#include "data-list.hh"
#include "file_picker_gui.h"
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <stdarg.h>
#include <iostream>
#include <fstream>

typedef unsigned int uint;

DataList::DataList()
{
    treeView_ = GTK_TREE_VIEW(gtk_tree_view_new());
    cellRenderer_ = gtk_cell_renderer_text_new();
    treeStore_ = gtk_tree_store_new(1, G_TYPE_INT);
}

void dump_callback(GtkButton* widget, gpointer data)
{

    GtkTreeView *treeView = GTK_TREE_VIEW(data);  
    DataList::dumpCsv(treeView);
}

int DataList::run()
{
    GtkDialogFlags flags = GTK_DIALOG_MODAL; //| GTK_DIALOG_DESTROY_WITH_PARENT;
    GtkWidget *vertical_box = NULL;
    GtkWidget            *scrolled_window = NULL;
    GtkTreeIter iter;
    GtkTreeSelection *selection;
    gint id = 0;
    window_ = gtk_dialog_new_with_buttons(title_.c_str(), NULL, flags, _("Open"), GTK_RESPONSE_ACCEPT,
                                          _("Cancel"), GTK_RESPONSE_REJECT, NULL);
    vertical_box = gtk_dialog_get_content_area(GTK_DIALOG(window_));
    gtk_tree_view_set_model(GTK_TREE_VIEW(treeView_), GTK_TREE_MODEL(treeStore_));
    scrolled_window = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(scrolled_window), 400);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window), GTK_POLICY_NEVER,
                                   GTK_POLICY_AUTOMATIC);
    gtk_container_add(GTK_CONTAINER(scrolled_window), GTK_WIDGET(treeView_));
    gtk_box_pack_start(GTK_BOX(vertical_box), scrolled_window, TRUE, TRUE, 0);
   
    GtkWidget* dump_button = gtk_button_new_with_label ("Dump CSV");

    g_signal_connect (G_OBJECT(dump_button), "clicked", G_CALLBACK (dump_callback), treeView_);

    gtk_box_pack_start(GTK_BOX(vertical_box), scrolled_window, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(vertical_box), dump_button, TRUE, TRUE, 0);


    gtk_widget_show_all(GTK_WIDGET(window_));
    gint result = gtk_dialog_run(GTK_DIALOG(window_));

    if (result == GTK_RESPONSE_ACCEPT)
    {
        selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeView_));

        if (gtk_tree_selection_get_selected(selection, NULL, &iter))
        {
            gtk_tree_model_get(GTK_TREE_MODEL(treeStore_), &iter, 0, &id, -1);
        }
    }

    return id;
}
int DataList::addRow(...)
{
    GtkTreeIter iter;
    GtkTreeIter *parent = NULL;
    va_list ap;
    va_start(ap, 0);

    if (parents_.size() > 0)
    {
        parent = &parents_.back();
    }

    gtk_tree_store_append(treeStore_, &iter, parent);
    gtk_tree_store_set_valist(treeStore_, &iter, ap);
    lastItem_ = iter;
    va_end(ap);                   /* Clean up. */
    return 0;
}

int DataList::setHeaders(std::vector<std::string>& text, std::vector<GType>& types)
{
    for (uint i = 0; i < text.size(); ++i)
    {
        gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(treeView_), -1, text[i].c_str(), cellRenderer_, "text", i,
                NULL);
    }

    treeStore_ = gtk_tree_store_newv(types.size(), types.data());
    return 0;
}



DataList::~DataList()
{
    if (window_)
    {
        gtk_widget_destroy(GTK_WIDGET(window_));
    }

    window_ = NULL;

    while (gtk_events_pending())
    {
        gtk_main_iteration();
    }

    free(window_);
}
int DataList::setTitle(std::string& title)
{
    title_ = title;
    return 0;
}

int DataList::popLevel()
{
    if (!parents_.empty())
    {
        parents_.pop_back();
    }

    return 0;
}

int DataList::pushLevel()
{
    parents_.push_back(lastItem_);
    return 0;
}

gboolean
foreach_func(GtkTreeModel *model,
             GtkTreePath  *path,
             GtkTreeIter  *iter,
             gpointer      user_data)
{
    gchar *column_char;
    gint column_int;
    gfloat column_float;
    gdouble column_double;

    int n_column = gtk_tree_model_get_n_columns(model); 
    std::ostream& myfile = *static_cast<std::ostream *>(user_data);

    char separator = ';';

    for (int i = 0; i < n_column; ++i)
    {
        GType type = gtk_tree_model_get_column_type (model, i);

        if (type == G_TYPE_STRING)
        {
            gtk_tree_model_get(model, iter,
                               i, &column_char,
                               -1);
            myfile << "\"" << column_char << "\"" << separator;
        }
        else if (type == G_TYPE_INT)
        {
            gtk_tree_model_get(model, iter,
                               i, &column_int,
                               -1);
            myfile << "\"" << column_int << "\"" << separator;
        }
        else if (type == G_TYPE_FLOAT)
        {
            gtk_tree_model_get(model, iter,
                               i, &column_float,
                               -1);
            myfile << "\"" << column_float << "\"" << separator;
        }
        else if (type == G_TYPE_DOUBLE)
        {
            gtk_tree_model_get(model, iter,
                               i, &column_double,
                               -1);
            myfile << "\"" << column_double << "\"" << separator;
        }
    }

    myfile << std::endl;
    return FALSE; /* do not stop walking the store, call us with next row */
}

int DataList::dumpCsv(GtkTreeView *treeView)
{
    char file[512] = "data.csv";
    char *filepath = nullptr;
    std::ofstream myfile;
    auto datas = &myfile;
    char separator = ';';

    GtkTreeModel *treeStore = GTK_TREE_MODEL(gtk_tree_view_get_model(GTK_TREE_VIEW(treeView)));
    int n_column = gtk_tree_model_get_n_columns(GTK_TREE_MODEL(treeStore)); 

    filepath = save_one_file_config("Choose where to save CSV", NULL, file, "csv File\0*.csv\0\0", "DATALIST", "last_saved_csv");
    myfile.open(filepath);

    for (int i = 0; i < n_column; ++i)
    {
        GtkTreeViewColumn *column = gtk_tree_view_get_column (GTK_TREE_VIEW(treeView), i);

        myfile << "\"" << gtk_tree_view_column_get_title(column) << "\"" << separator;
    }

    myfile << std::endl;
    gtk_tree_model_foreach(GTK_TREE_MODEL(treeStore), foreach_func, (gpointer) datas);
    myfile.close();
    return 0;
}



