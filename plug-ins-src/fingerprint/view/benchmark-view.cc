#include "benchmark-view.hh"

namespace benchmark
{
template <>
int View<chiSquareMethod::ChiSquareSearch>::drawScore(Params<chiSquareMethod::ChiSquareSearch>& params,
        std::vector<chiSquareMethod::ChiSquareSearch::results_type> results)
{
    if (scoreOp_ == nullptr)
    {
        warning_message("no score op."); 
        return 0;
    }

    d_s *truePosDs = scoreOp_->dat[0];
    truePosDs->nx = 1;
    truePosDs->ny = 1;

    for (uint i = 0; i < results.size(); ++i)
    {
        create_and_attach_one_ds(scoreOp_, 1, 1, 0);
        d_s *curDs = scoreOp_->dat[i + 1];
        curDs->nx = 0;
        curDs->ny = 0;
        auto sols = results[i].solutions();
        add_new_point_to_ds(truePosDs, results[i].firstTruePositiveRank(),
                            results[i].firstTruePositiveRank() >= 0 ?
                            sols[results[i].firstTruePositiveRank()].chiSquareValue() / sols[results[i].firstTruePositiveRank()].chiSquareValue()  : -1);

        for (uint j = 0; j < sols.size(); ++j)
        {
            add_new_point_to_ds(curDs, j, sols[j].chiSquareValue() / sols[j].criticalValue());
        }
    }

    return 0;
}

template <>
int View<DistOligoSequenceFinder>::drawScore(Params<DistOligoSequenceFinder>&,
        std::vector<DistOligoSequenceFinder::results_type>)
{
    warning_message("Not implemented.");
    return 0;
}

template <>
int View<BasicFinder>::drawScore(Params<BasicFinder>&,
        std::vector<BasicFinder::results_type>)
{
    warning_message("Not implemented.");
    return 0;
}



}
