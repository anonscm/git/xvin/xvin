#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif
#include <xvin.h>
#include "../sequence/pos-fingerprint.hh"
#include "../results/chisquare-candidate-solution.hh"

#include <iostream>
namespace chiSquareMethod
{
class ChiSquareSolutionViewer
{
  public:
    ChiSquareSolutionViewer(double avgBaseNmRate,
                            double stdDevBaseNmRate, 
                            double stretchingConfidence,
                            double expectedNoiseDeviation,
                            double noiseConfidenceLevel);

    ChiSquareSolutionViewer();
    virtual ~ChiSquareSolutionViewer();

    int draw(const PosFingerprint& refce, const PosFingerprint& needle, const ChiSquareCandidateSolution& solution);
    int reset();

  private:

    BaseNmRateDistribution baseNmRateDistribution_ = BaseNmRateDistribution(AVG_BASE_NM_RATE, STDDEV_BASE_NM_RATE);
    double stretchingConfidenceLevel_ = STRETCHING_CONFIDENCE;
    double noiseConfidenceLevel_ = NOISE_CONFIDENCE;
    double expectedNoiseDeviation_ = NOISE_DEVIATION;
    double minBaseNmRate_;
    double maxBaseNmRate_;
    double lowerNoiseInterv__ = 0;
    double upperNoiseInterv__ = 0;

    std::string label_;

    pltreg *outputPr_;
    O_p *outputOp_;


    int drawReference(const PosFingerprint& refce, const PosFingerprint& needle,
                      const ChiSquareCandidateSolution& solution);
    int drawNeedle(const PosFingerprint& needle, const ChiSquareCandidateSolution& solution);
    int drawStretchingRange(const PosFingerprint& needle, const ChiSquareCandidateSolution& solution);
    int drawMatchingHybs(const PosFingerprint& needle, const ChiSquareCandidateSolution& solution);
    int drawChoosenHybs(const PosFingerprint& needle, const ChiSquareCandidateSolution& solution);
    int drawFit(const PosFingerprint& needle, const ChiSquareCandidateSolution& solution);
    int drawROI(const PosFingerprint& needle, const ChiSquareCandidateSolution& solution);

};
}
