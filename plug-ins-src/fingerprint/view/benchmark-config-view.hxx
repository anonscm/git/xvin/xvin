#include "json.hpp"
#include "fstream"
#include "progress_builder.h"
using json = nlohmann::json;

template<typename T>
benchmark::BenchmarkConfigView<T>::BenchmarkConfigView()
{
}

template<typename T>
benchmark::BenchmarkConfigView<T>::~BenchmarkConfigView()
{
    form_free(form_);
}

template<typename T>
int benchmark::BenchmarkConfigView<T>::prepareGeneral()
{
    form_push_label(form_, "Keep All Results");
    form_push_bool(form_, &params_->keepAllResults);
    form_newline(form_);
    form_push_label(form_, "Trucates solutions");
    form_push_bool(form_, &params_->truncateRes);
    form_push_label(form_, "keepScores");
    form_push_bool(form_, &params_->keepScores);
    form_newline(form_);
    form_push_label(form_, "Sample per step");
    form_push_int(form_, &params_->samplesPerStep);
    form_push_label(form_, "Overwrite by « Mapping genom »");
    form_newline(form_);
    form_push_label(form_, "or Map genom ");
    form_push_bool(form_, &params_->mapGenom);
    form_push_label(form_, " (Only with sequencial locus)");
    form_newline(form_);
    //form_push_label(form_, "Needle fingerprint size");
    //form_push_int(form_, &params_->fgpSize);
    //form_newline(form_);
    //form_push_label(form_, "Needle fingerprint size deviation");
    //form_push_int(form_, &params_->sizeStdev);
    //form_newline(form_);
    //form_push_label(form_, "Replace needle list by not found fingerprint");
    //form_push_bool(form_, &keep_poor_results);
    //form_newline(form_);
    form_push_label(form_, "Locus selection method :");
    form_newline(form_);
    form_newline(form_);
    form_push_label(form_, "Use random generated locus :");
    form_push_bool(form_, &params_->randomGenerated);
    form_newline(form_);
    form_push_label(form_, "\t\tGenerate random seed");
    form_push_bool(form_, &genSeed_);
    form_push_label(form_, " Seed ");
    form_push_int(form_, &params_->randSeed);
    form_newline(form_);
    form_push_label(form_, "or\t\tSequencial locus");
    form_push_int(form_, &params_->locusInterval);
    form_newline(form_);
    return 0;
}

template<typename T>
int benchmark::BenchmarkConfigView<T>::prepareNoiseVariant()
{
    // FIXME default values should not be there.
//    params_->minVal =  0;
//    params_->maxVal = THEORICAL_STANDARD_DEVIATION * 1.75;
//    params_->step = 0.5;
//    form_push_label(form_, "Min sigma");
//    form_push_float(form_, 0.2 , &params_->minVal);
//    form_newline(form_);
//    form_push_label(form_, "Max sigma");
//    form_push_float(form_, 0.2, &params_->maxVal);
//    form_newline(form_);
//    form_push_label(form_, "Base->nm Rate");
//    form_push_float(form_, 0.05 , &params_->fixStretchingVal);
//    form_newline(form_);
//    form_push_label(form_, "sigma step size");
//    form_push_float(form_, 0.01, &params_->step);
//    form_newline(form_);
    return 0;
}


template<typename T>
int benchmark::BenchmarkConfigView<T>::prepareNoiseStretchVariant()
{
    //form_push_label(form, "Min Base->nm Rate");
    //form_push_float(form, 0.05 , &bgp.minVal);
    //form_newline(form);
    //form_push_label(form, "Max Base->nm Rate");
    //form_push_float(form, 0.05, &bgp.maxVal);
    //form_newline(form);
    //form_push_label(form, "baseRate step size");
    //form_push_float(form, 0.01, &bgp.step);
    //form_newline(form);
    //form_push_label(form, "Min sigma");
    //form_push_float(form, 0.2 , &minSigma);
    //form_newline(form);
    //form_push_label(form, "Max sigma");
    //form_push_float(form, 0.2, &maxSigma);
    //form_newline(form);
    //form_push_label(form, "sigma step size");
    //form_push_float(form, 0.1, &sigmaStep);
    //form_newline(form);
    return 0;
}
template<typename T>
int benchmark::BenchmarkConfigView<T>::prepareStretchVariant()
{
//    form_push_label(form_, "Min Base->nm Rate");
//    form_push_float(form_, 0.05 , &params_->minVal);
//    form_newline(form_);
//    form_push_label(form_, "Max Base->nm Rate");
//    form_push_float(form_, 0.05, &params_->maxVal);
//    form_newline(form_);
//    form_push_label(form_, "hybridization Sigma noise");
//    form_push_float(form_, 0.2 , &params_->fixNoiseVal);
//    form_newline(form_);
//    form_push_label(form_, "baserate step size");
//    form_push_float(form_, 0.01, &params_->step);
//    form_newline(form_);
//    return 0;
}

template<typename T>
int benchmark::BenchmarkConfigView<T>::draw(Params<T>& params, const char *benchmark_file)
{
    params_ = &params;
    typename T::params_type search_params;
    int form_ret = WIN_OK;

    if (benchmark_file == nullptr)
    {
        form_ret = drawVariantForm();
    }

    if (form_ret != WIN_OK)
    {
        // FIXME error handling
        return D_O_K;
    }

    form_ = form_create("Benchmark");

    if (configMode_ == JsonFile)
    {
        if (benchmark_file == nullptr)
        {
            benchmark_file = open_one_file_config("Load configuration file", "", "Json file\0*.json\0All file\0*.*\0",
                                                  "FINGERPRINT",
                                                  "benchmark_config_file");
        }

        std::ifstream istm;
        istm.open(benchmark_file);
        json jsonConfig;

        try
        {
            progress_window_t *progress = progress_create("Import json and gen haystack");
            progress_run(progress);
            progress_pulse(progress);
            progress_update_window(progress);
            istm >> jsonConfig;
            if (!params_->loadJson(jsonConfig))
                return -1;

            progress_free(progress);
        }
        catch (std::exception& e)
        {
            error_message("Exception in benchmark-config-view.cc: %s", e.what());
            istm.close();
            return -1;
        }

        istm.close();
    }
    else if (configMode_ == Form)
    {
        prepareSearchEngine(search_params);
    }
    else
    {
        warning_message("Benchmark config mode not implemented");
    }

    prepareGeneral();

    if (form_run(form_) != WIN_OK)
    {
        form_free(form_);
        return -1;
    }

    if (configMode_ == Form)
    {
        // TODO generate by step
        params_->searchParamsPerStep.push_back(search_params);
        postTreatSearchEngine(search_params);
    }

    if (genSeed_)
    {
        std::random_device rd;
        params_->randSeed = rd();
    }

    if (configMode_ == Form)
    {
        params_->haystack = getHaystack();
    }

    if (!params_->randomGenerated && params_->mapGenom)
    {
        params_->samplesPerStep = params_->haystack->size() / params_->locusInterval;
    }

    std::shared_ptr<typename T::input_type> hay = params_->haystack;

    for (int i = 0; i < params_->searchParamsPerStep.size(); ++i)
    {
        params_->searchParamsPerStep[i].generateMissingParams(*hay);
    }

    //std::cout << params_->dumpJson().dump(4) << std::endl;
    assert(params_->haystack != nullptr);
    return D_O_K;
}

template<typename T>
int benchmark::BenchmarkConfigView<T>::drawVariantForm()
{
    form_window_t *form = form_create("Select base parameters");
//    form_push_label(form, "Make variation on ");
//    form_push_combo_option(form, "Stretching", (int *) &params_->variant);
//    form_push_combo_option(form, "Noise", (int *)&params_->variant);
//    form_newline(form);
    form_push_label(form, "Benchmark parameter source");
    form_push_combo_option(form, "Json File", (int *)&configMode_);
    form_push_combo_option(form, "Configuration form", (int *)&configMode_);
    form_newline(form);
    form_push_label(form, "Aggregate data ");
    form_push_combo_option(form, "step by Step", (int *)&params_->aggregator);
    form_push_combo_option(form, "per hybridization count", (int *)&params_->aggregator);
    form_push_combo_option(form, "No aggregation", (int *)&params_->aggregator);
    int form_ret =  form_run(form);
    form_free(form);
    return form_ret;
}
