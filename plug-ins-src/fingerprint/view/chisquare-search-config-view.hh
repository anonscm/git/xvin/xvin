#pragma once
#include "../search/chisquare-search.hh"

namespace chiSquareMethod 
{

class ConfigView
{
public:
    ConfigView ();
    virtual ~ConfigView ();

    int draw(ChiSquareSearch &searchEngine);

private:

};
}
