#pragma once

namespace benchmark
{

template <typename T>
int View<T>::drawTitle(Params<T>& params)
{

        set_plot_title(outputOp_, "\\stack{{Benchmark - %s }}",
                       std::is_same<T, chiSquareMethod::ChiSquareSearch>::value ? "ChiSquareSearch" : std::is_same<T, DistOligoSequenceFinder>::value ? "DistOligo" : "Basic Finder");
        set_plot_x_title(outputOp_, "Steps");

    if (params.aggregator == Aggregator::HybCount)
    {
        set_plot_x_title(outputOp_, "Hybridization Count");
    
    }

    set_plot_y_title(outputOp_, "Average rank  of the correct solution");
    return 0;
}

template <typename T>
View<T>::View()
{
    ac_grep(cur_ac_reg, "%pr", &outputPr_);
    outputOp_ = create_and_attach_one_plot(outputPr_, 1, 1, 0);
    scoreOp_ = nullptr;
    //scoreOp_ = create_and_attach_one_plot(outputPr_, 1, 1, 0);
    //scoreOp_->user_id = 0xBECACE;
    //scoreOp_->read_only = true;
    //set_ds_point_symbol(scoreOp_->dat[0], "\\fd");
    outputOp_->read_only = true;
    outputOp_->user_id = 0xBECA;
    create_and_attach_one_ds(outputOp_, 16, 16, 0);
    create_and_attach_one_ds(outputOp_, 16, 16, 0);
    create_and_attach_one_ds(outputOp_, 16, 16, 0);
    create_and_attach_one_ds(outputOp_, 16, 16, 0);
    un_s *un = build_unit_set(IS_SECOND, 1, 1, IS_MILLI, IS_T_UNIT_SET, "ms");
    add_y_unit_set_to_op(outputOp_, un);
    set_ds_point_symbol(outputOp_->dat[DsIndex::DURATION], "\\fd");
    switch_plot(outputPr_, outputPr_->n_op - 1);
    outputOp_->iopt2 |= X_LIM;
    outputOp_->iopt2 |= Y_LIM;
    outputOp_->need_to_refresh = 1;
    refresh_plot(outputPr_, UNCHANGED);
}

template <typename T>
View<T>::~View()
{
}

template <typename T>
int View<T>::configParamView(float stepIntervalValue, int numberOfStep)
{
    d_s *boxplot_ds = outputOp_->dat[DsIndex::BOXPLOT];
    d_s *notfound_ds = outputOp_->dat[DsIndex::NOTFOUND];
    d_s *found_ds = outputOp_->dat[DsIndex::FOUND];
    d_s *duration_ds = outputOp_->dat[DsIndex::DURATION];
    set_ds_source(boxplot_ds, "Boxs");
    set_ds_source(notfound_ds, "Not found");
    set_ds_source(found_ds, "found");
    set_ds_source(duration_ds, "Duration (us)");
    boxplot_ds->nx = 0;
    boxplot_ds->ny = 0;
    notfound_ds->nx = 0;
    notfound_ds->ny = 0;
    found_ds->nx = 0;
    found_ds->ny = 0;
    duration_ds->nx = 0;
    duration_ds->ny = 0;
    boxplot_ds->boxplot_width = (stepIntervalValue / (numberOfStep * 2));
    alloc_data_set_y_error(boxplot_ds);
    alloc_data_set_y_down_error(boxplot_ds);
    alloc_data_set_y_box(boxplot_ds);
    return 0;
}

template <typename T>
int View<T>::draw(Params<T>& params, AggregatedResults<T>& stepResults)
{
    configParamView(0.1, 1); // TODO new interval configurator
    drawTitle(params);
    drawData(params, stepResults);
    refresh_plot(outputPr_, UNCHANGED);
    return 0;
}

template <typename T>
int View<T>::drawData(Params<T>& /*params*/, AggregatedResults<T>& stepResults)
{
    d_s *boxplot_ds = outputOp_->dat[DsIndex::BOXPLOT];
    d_s *notfound_ds = outputOp_->dat[DsIndex::NOTFOUND];
    d_s *found_ds = outputOp_->dat[DsIndex::FOUND];
    d_s *duration_ds = outputOp_->dat[DsIndex::DURATION];

    for (auto sr : stepResults)
    {
        add_new_point_to_ds(notfound_ds, sr.aggrValue, sr.notFoundCount);
        add_new_point_to_ds(found_ds, sr.aggrValue, sr.foundCount);
        add_new_point_to_ds(duration_ds, sr.aggrValue,
                            (std::chrono::duration_cast<std::chrono::microseconds> (sr.durationMean)).count() /  1000.); // milli seconds
        add_new_point_to_ds(boxplot_ds, sr.aggrValue, sr.mean);
        boxplot_ds->ye[boxplot_ds->nx - 1] = sr.max;
        boxplot_ds->yed[boxplot_ds->nx - 1] = sr.min;
        boxplot_ds->ybu[boxplot_ds->nx - 1] = std::sqrt(sr.variance);
        boxplot_ds->ybd[boxplot_ds->nx - 1] = std::sqrt(sr.variance);
    }

    outputOp_->need_to_refresh = 1;
    return 0;
}

}
