#include "basic-solution-viewer.hh"
#include "form_builder.h"
#include "../sequence/fingerprint.hh"
#include "../../ImOliSeq/ImOliSeq.h"

int solViewHayIdx = 0;
int solViewNeeIdx = 0;
int solViewNeeRefPeakIdx = 0;
int solViewHayRefPeakIdx = 0;
FingerprintVector *solViewHaystacks = 0;
FingerprintVector *solViewNeedles = 0;
std::vector<SearchMatch> *solViewSolutions = 0;

int solViewStartPos = 0;
float solViewMinNmBaseRate = MIN_NM_BASE_RATE;
float solViewMaxNmBaseRate = MAX_NM_BASE_RATE;
int solViewDir = 1;
O_p *solViewOp = NULL;
bool solViewOnlyMatch = false;
int solViewSolutionIndex = -1;
float solViewFitCoef = 0.0;
char *solViewLabel = NULL;

std::ostream& print_solution(
    std::ostream& os, Fingerprint& haystack, Fingerprint& needle, SearchMatch& searchMatch)
{
    const char *rev = searchMatch.reversed() ? "[reversed]" : "";
    os << rev << "\n"
       << "sequence: " << searchMatch.chromosome() << "\n"
       << "needle: " << searchMatch.needle() << "\n"
       << "pos: " << searchMatch.firstReferencePosition() << "\n"
       << "nm to base rate: " << searchMatch.nmBaseRate() << "\n";
    return os;
}
int fingerprint_solviewer_index_check()
{
    if ((uint) solViewHayIdx >= solViewHaystacks->size())
    {
        warning_message("Haystack index out of range %d > %u\n", solViewHayIdx, solViewHaystacks->size() - 1);
        solViewHayIdx = 0;
    }

    if ((uint) solViewNeeIdx >= solViewNeedles->size())
    {
        warning_message("Needle index out of range %d > %u\n", solViewNeeIdx, solViewNeedles->size() - 1);
        solViewNeeIdx = 0;
    }

    if ((uint) solViewSolutionIndex >= solViewSolutions->size())
    {
        warning_message("Solution index out of range %d > %u\n", solViewHayIdx, solViewHaystacks->size() - 1);
        solViewNeeIdx = 0;
    }

    return 0;
}
bool fingerprint_solviewer_is_empty_check()
{
    return solViewNeedles->empty() || solViewHaystacks->empty();
}
int fingerprint_solviewer_configure(FingerprintVector& haystacks,
                                    int haystack_idx,
                                    FingerprintVector& needles,
                                    int needle_idx,
                                    std::vector<SearchMatch>& lastSolutions,
                                    int dir,
                                    bool onlyMatch)
{
    solViewSolutions = &lastSolutions;
    solViewHaystacks = &haystacks;
    solViewNeedles = &needles;
    solViewOnlyMatch = onlyMatch;
    solViewHayIdx = haystack_idx;
    solViewNeeIdx = needle_idx;
    solViewDir = dir;

    if (solViewOnlyMatch && solViewSolutions->size() > 0)
    {
        solViewSolutionIndex = 0;
        SearchMatch& lsol = (*solViewSolutions)[solViewSolutionIndex];
        solViewStartPos = lsol.firstReferencePosition();
        solViewDir = lsol.reversed() ? -1 : 1;
        solViewHayIdx = lsol.chromosome();
        // solViewMinNmBaseRate = lsol.nmBaseRate() - 0.05;
        // solViewMaxNmBaseRate = lsol.nmBaseRate() + 0.05;
        solViewNeeIdx = lsol.needle();
        solViewFitCoef = lsol.nmBaseRate();
        std::stringstream ss;
        print_solution(ss, *(haystacks[solViewHayIdx]), *(needles[solViewNeeIdx]), lsol);
        std::free(solViewLabel);
        solViewLabel = my_strdup(ss.str().c_str());
    }

    return fingerprint_solviewer_display();
}

int fingerprint_solviewer_display()
{
    std::shared_ptr<Fingerprint>haystack = 0;
    std::shared_ptr<Fingerprint>needle = 0;
    pltreg *pr = NULL;
    ac_grep(cur_ac_reg, "%pr", &pr);

    if (!pr || fingerprint_solviewer_is_empty_check())
    {
        warning_message("Fingerprint is empty, can't open viewer");
        return D_O_K;
    }

    fingerprint_solviewer_index_check();

    if (!solViewOp)
    {
        solViewOp = create_and_attach_one_plot(pr, 16, 16, 0);
    }

    if (!solViewOp)
    {
        warning_message("Can't create OP");
        return D_O_K;
    }

    if (solViewOp->n_dat == 1)
    {
        create_and_attach_one_ds(solViewOp, 16, 16, 0);
    }

    haystack = (*solViewHaystacks)[solViewHayIdx];
    needle = (*solViewNeedles)[solViewNeeIdx];
    haystack->generateDs(solViewOp->dat[0], solViewStartPos,
                         solViewStartPos + needle->size() * solViewMaxNmBaseRate);

    if (solViewDir > 0)
    {
        needle->generateDs(solViewOp->dat[1], needle->firstHyb(), needle->lastHyb());
    }
    else
    {
        needle->reverse()->generateDs(solViewOp->dat[1], needle->firstHyb(), needle->lastHyb());
    }

    swap_ds_x_y(solViewOp->dat[1]);
    solViewStartPos = solViewOp->dat[0]->xd[0]; // get actual start
    solViewOp->user_id = 4242;
    error_message("disabled");
    //generate_Plot_Oligo_Seq(solViewOp, 1, 0, solViewNeeRefPeakIdx, solViewHayRefPeakIdx, solViewMinNmBaseRate, solViewMaxNmBaseRate,
    //                        solViewDir /*solViewDir*/, NULL /*&solViewFitCoef*/, solViewLabel);
    return refresh_plot(pr, UNCHANGED);
}

int fingerprint_solviewer_go_next_pos()
{
    if (fingerprint_solviewer_is_empty_check())
    {
        warning_message("Fingerprint is empty, can't open viewer");
        return D_O_K;
    }

    fingerprint_solviewer_index_check();

    if (solViewOnlyMatch && !solViewSolutions->empty())
    {
        solViewSolutionIndex++;
        SearchMatch& lsol = (*solViewSolutions)[solViewSolutionIndex];
        solViewStartPos = lsol.firstReferencePosition();
        solViewDir = lsol.reversed() ? -1 : 1;
        solViewHayIdx = lsol.chromosome();
        // solViewMinNmBaseRate = lsol.nmBaseRate() - 0.05;
        // solViewMaxNmBaseRate = lsol.nmBaseRate() + 0.05;
        solViewNeeIdx = lsol.needle();
        solViewFitCoef = lsol.nmBaseRate();
        std::stringstream ss;
        print_solution(ss, *((*solViewHaystacks)[solViewHayIdx]), *((*solViewNeedles)[solViewNeeIdx]), lsol);
        std::free(solViewLabel);
        solViewLabel = my_strdup(ss.str().c_str());
    }
    else
    {
        std::shared_ptr<Fingerprint>haystack = (*solViewHaystacks)[solViewHayIdx];
        solViewStartPos = haystack->nextHybridizationPos(solViewStartPos);
    }

    return fingerprint_solviewer_display();
}

int fingerprint_solviewer_go_prev_pos()
{
    if (fingerprint_solviewer_is_empty_check())
    {
        warning_message("Fingerprint is empty, can't open viewer");
        return D_O_K;
    }

    fingerprint_solviewer_index_check();

    if (solViewOnlyMatch && solViewSolutions->size() > 0)
    {
        solViewSolutionIndex--;

        if (solViewSolutionIndex < 0)
        {
            solViewSolutionIndex = solViewSolutions->size() - 1;
        }

        SearchMatch& lsol = (*solViewSolutions)[solViewSolutionIndex];
        solViewStartPos = lsol.firstReferencePosition();
        solViewDir = lsol.reversed() ? -1 : 1;
        solViewHayIdx = lsol.chromosome();
        // solViewMinNmBaseRate = lsol.nmBaseRate() - 0.05;
        // solViewMaxNmBaseRate = lsol.nmBaseRate() + 0.05;
        solViewNeeIdx = lsol.needle();
        solViewFitCoef = lsol.nmBaseRate();
        std::stringstream ss;
        print_solution(ss, *((*solViewHaystacks)[solViewHayIdx]), *((*solViewNeedles)[solViewNeeIdx]), lsol);
        std::free(solViewLabel);
        solViewLabel = my_strdup(ss.str().c_str());
    }
    else
    {
        std::shared_ptr<Fingerprint>haystack = (*solViewHaystacks)[solViewHayIdx];
        solViewStartPos = haystack->previousHybridizationPos(solViewStartPos);
    }

    return fingerprint_solviewer_display();
}
int fingerprint_solviewer_inc_ref_peak()
{

    if (fingerprint_solviewer_is_empty_check())
    {
        warning_message("Fingerprint is empty, can't open viewer");
        return D_O_K;
    }
    fingerprint_solviewer_index_check();

    solViewNeeRefPeakIdx = (solViewNeeRefPeakIdx + 1) % (*solViewNeedles)[solViewNeeIdx]->nbHybridation();
    solViewHayRefPeakIdx = (solViewHayRefPeakIdx + 1) % (*solViewHaystacks)[solViewHayIdx]->nbHybridation();
    
    return fingerprint_solviewer_display();
}
int fingerprint_solviewer_dec_ref_peak()
{
    if (fingerprint_solviewer_is_empty_check())
    {
        warning_message("Fingerprint is empty, can't open viewer");
        return D_O_K;
    }
    fingerprint_solviewer_index_check();

    solViewNeeRefPeakIdx = (solViewNeeRefPeakIdx <= 0 ? (*solViewNeedles)[solViewNeeIdx]->nbHybridation(): solViewNeeRefPeakIdx) - 1;
    solViewHayRefPeakIdx = (solViewHayRefPeakIdx <= 0 ? (*solViewHaystacks)[solViewHayIdx]->nbHybridation(): solViewHayRefPeakIdx) - 1;

    return fingerprint_solviewer_display();
}
