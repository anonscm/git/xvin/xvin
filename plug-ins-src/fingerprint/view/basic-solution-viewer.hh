#pragma once
#include <xvin.h>
#include "../sequence/fingerprint.hh"
#include <iostream> 


int fingerprint_solviewer_go_next_pos();
int fingerprint_solviewer_go_prev_pos();
int fingerprint_solviewer_display();
int fingerprint_solviewer_index_check();
bool fingerprint_solviewer_is_empty_check();
int fingerprint_solviewer_inc_ref_peak();
int fingerprint_solviewer_dec_ref_peak();
int fingerprint_solviewer_configure(FingerprintVector& haystacks,
                                    int haystack_idx,
                                    FingerprintVector& needles,
                                    int needle_idx,
                                    std::vector<SearchMatch>& lastSolutions,
                                    int dir,
                                    bool onlyMatch);

std::ostream& print_solution(
    std::ostream& os,
    Fingerprint& haystack,
    Fingerprint& needle,
    SearchMatch& searchMatch);
