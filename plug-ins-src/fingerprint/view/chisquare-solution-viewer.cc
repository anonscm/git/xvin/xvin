#include "chisquare-solution-viewer.hh"
#include "form_builder.h"
#include "../sequence/pos-fingerprint.hh"
//#include "../../ImOliSeq/ImOliSeq.h"

#include "../results/chisquare-candidate-solution.hh"

#define CSM_REFERENCE_DS_IDX 0
#define CSM_NEEDLE_DS_IDX 1
#define CSM_STRETCHING_RANGE_DS_IDX 2
#define CSM_NOISE_RANGE_DS_IDX 3
#define CSM_MATCHING_HYBS_DS_IDX 4
#define CSM_FIT_DS_IDX 5
#define CSM_CHOOSEN_HYBS_DS_IDX 6
#define CSM_CHOOSEN_REFERENCE_DS_IDX 7
#define CSM_CHOOSEN_NEEDLE_DS_IDX 8

namespace chiSquareMethod
{

ChiSquareSolutionViewer::ChiSquareSolutionViewer(double avgBaseNmRate,
        double stdDevBaseNmRate, double stretchingConfidenceLevel, double expectedNoiseDeviation, double noiseConfidenceLevel):
    baseNmRateDistribution_(avgBaseNmRate, stdDevBaseNmRate),
    stretchingConfidenceLevel_(stretchingConfidenceLevel),
    noiseConfidenceLevel_(noiseConfidenceLevel),
    expectedNoiseDeviation_(expectedNoiseDeviation),
    outputPr_(0),
    outputOp_(0)
{
    boost::math::normal_distribution<double> dist = boost::math::normal_distribution<double>(baseNmRateDistribution_.mean(),
            baseNmRateDistribution_.stddev());
    minBaseNmRate_ =  boost::math::quantile(boost::math::complement(dist, stretchingConfidenceLevel_));
    maxBaseNmRate_ = boost::math::quantile(dist, stretchingConfidenceLevel_);
    boost::math::normal_distribution<double> dist2 = boost::math::normal_distribution<double>(0, expectedNoiseDeviation_);
    lowerNoiseInterv__ =  boost::math::quantile(boost::math::complement(dist2, noiseConfidenceLevel_));
    upperNoiseInterv__ = boost::math::quantile(dist2, noiseConfidenceLevel_);
    ac_grep(cur_ac_reg, "%pr", &outputPr_);
    outputOp_ = create_and_attach_one_plot(outputPr_, 16, 16, 0);

    if (outputOp_ == nullptr)
    {
        error_message("Unable to allocate ds %s:%s", __FILE__, __LINE__);
    }

    outputOp_->read_only = true;
    outputOp_->user_id = 0xCAE;
    set_plot_title(outputOp_, "\\stack{{ChiSquare Fit View a=%f-%f}}", minBaseNmRate_, maxBaseNmRate_);
    outputOp_->filename = strdup("chisquare-fit.gr");
    create_and_attach_one_ds(outputOp_, 16, 16, 0); //needle
    create_and_attach_one_ds(outputOp_, 5, 5, 0); // stretching factor
    create_and_attach_one_ds(outputOp_, 5, 5, 0); // noise factor
    create_and_attach_one_ds(outputOp_, 16, 16, 0); // matching hybs
    create_and_attach_one_ds(outputOp_, 16, 16, 0); // fit
    create_and_attach_one_ds(outputOp_, 16, 16, 0); // choosen
    create_and_attach_one_ds(outputOp_, 16, 16, 0); // choosen ref
    create_and_attach_one_ds(outputOp_, 16, 16, 0); // choosen needle
    set_dash_line(outputOp_->dat[CSM_STRETCHING_RANGE_DS_IDX]);
    set_dash_line(outputOp_->dat[CSM_NOISE_RANGE_DS_IDX]);
    set_dash_line(outputOp_->dat[CSM_REFERENCE_DS_IDX]);
    set_dash_line(outputOp_->dat[CSM_NEEDLE_DS_IDX]);
    set_dash_line(outputOp_->dat[CSM_CHOOSEN_REFERENCE_DS_IDX]);
    set_dash_line(outputOp_->dat[CSM_CHOOSEN_NEEDLE_DS_IDX]);
    set_ds_point_symbol(outputOp_->dat[CSM_CHOOSEN_HYBS_DS_IDX], "\\fd");
    outputOp_->dat[CSM_REFERENCE_DS_IDX]->source = strdup("Reference lines");
    outputOp_->dat[CSM_NEEDLE_DS_IDX]->source = strdup("Experimental lines");
    outputOp_->dat[CSM_STRETCHING_RANGE_DS_IDX]->source = strdup("Stretching selection threshold");
    outputOp_->dat[CSM_NOISE_RANGE_DS_IDX]->source = strdup("Noise selection threshold");
    outputOp_->dat[CSM_MATCHING_HYBS_DS_IDX]->source = strdup("Matching Hybridization");
    outputOp_->dat[CSM_FIT_DS_IDX]->source = strdup("Fit");
    outputOp_->dat[CSM_CHOOSEN_HYBS_DS_IDX]->source = strdup("Choosen Hybridization Match");
    outputOp_->dat[CSM_CHOOSEN_REFERENCE_DS_IDX]->source = strdup("Choosen Reference Hybridization");
    outputOp_->dat[CSM_CHOOSEN_NEEDLE_DS_IDX]->source = strdup("Choosen needle Hybridization");
    outputOp_->dat[CSM_REFERENCE_DS_IDX]->color = Brown;
    outputOp_->dat[CSM_NEEDLE_DS_IDX]->color = Green;
    outputOp_->dat[CSM_NOISE_RANGE_DS_IDX]->color = orange;
    outputOp_->dat[CSM_CHOOSEN_HYBS_DS_IDX]->color = Lightgreen;
    outputOp_->dat[CSM_CHOOSEN_REFERENCE_DS_IDX]->color = purple;
    outputOp_->dat[CSM_CHOOSEN_NEEDLE_DS_IDX]->color = purple;
    outputOp_->dat[CSM_FIT_DS_IDX]->color = Yellow;
    outputOp_->dat[CSM_MATCHING_HYBS_DS_IDX]->color = blue;
    outputOp_->iopt2 |= X_LIM;
    outputOp_->iopt2 |= Y_LIM;
    set_plot_x_title(outputOp_, "Reference (Bp)");
    set_plot_y_title(outputOp_, "Experimental (Nm)");
    switch_plot(outputPr_, outputPr_->n_op - 1);
    outputOp_->need_to_refresh = 1;
    refresh_plot(outputPr_, UNCHANGED);
    reset();
}

ChiSquareSolutionViewer::ChiSquareSolutionViewer()
    : ChiSquareSolutionViewer(AVG_BASE_NM_RATE, STDDEV_BASE_NM_RATE, STRETCHING_CONFIDENCE, NOISE_DEVIATION,
                              NOISE_CONFIDENCE)
{
}

ChiSquareSolutionViewer::~ChiSquareSolutionViewer()
{
    //remove_data(outputPr_, IS_ONE_PLOT, outputOp_);
}

int ChiSquareSolutionViewer::draw(const PosFingerprint& refce, const PosFingerprint& needle,
                                  const ChiSquareCandidateSolution& solution)
{
    drawReference(refce, needle, solution);
    drawNeedle(needle, solution);
    drawStretchingRange(needle, solution);
    drawMatchingHybs(needle, solution);
    set_plot_title(outputOp_, "\\stack{{ChiSquare Fit View a=%f-%f}{Pivot: %d [%d->%d]%s %s}{[X^2 / GammaQ =%lf]}}",
                   minBaseNmRate_, maxBaseNmRate_,
                   solution.refPivotPosition(),
                   solution.firstReferencePosition(),
                   solution.lastReferencePosition(),
                   solution.chiSquareValue() / solution.criticalValue(),
                   abs(solution.refPivotPosition() - (needle.originPosition() + solution.needlePivotPosition())) < needle.size() ?
                   "[Origin]" : "", solution.direction() == CandidateSolution::FORWARD ? "" : "[Backward]");
    drawChoosenHybs(needle, solution);
    drawFit(needle, solution);
    drawROI(needle, solution);
    return refresh_plot(outputPr_, UNCHANGED);
}

int ChiSquareSolutionViewer::drawROI(const PosFingerprint& needle, const ChiSquareCandidateSolution& solution)
{
    int offset = solution.refPivotPosition() + needle.firstHyb() - solution.needlePivotPosition() * (1 / minBaseNmRate_);
    outputOp_->cur_dat = CSM_NEEDLE_DS_IDX;
    outputOp_->x_lo = offset;
    outputOp_->x_hi = offset + needle.lastHyb() * (1 / minBaseNmRate_);
    outputOp_->y_lo = - 100;
    outputOp_->y_hi = needle.lastHyb() + 100;
    outputOp_->need_to_refresh = 1;
    return 0;
}
int ChiSquareSolutionViewer::drawNeedle(const PosFingerprint& needle, const ChiSquareCandidateSolution& solution)
{
    d_s *dataset = outputOp_->dat[CSM_NEEDLE_DS_IDX];
    int offset = solution.refPivotPosition() - solution.needlePivotPosition() * (1 / minBaseNmRate_);
    int lineSize = needle.lastHyb() * (1 / minBaseNmRate_);

    outputOp_->dat[CSM_NEEDLE_DS_IDX]->source = strdup(needle.description().c_str());

    if (lineSize == 0)
    {
        lineSize = 1000;
    }

    needle.generateLineDs(dataset, needle.firstHyb() + lowerNoiseInterv__, needle.lastHyb() + upperNoiseInterv__, offset,
                          lineSize,
                          PosFingerprint::Orientation::VERTICAL);
    return 0;
}

int ChiSquareSolutionViewer::drawReference(const PosFingerprint& refce, const PosFingerprint& needle,
        const ChiSquareCandidateSolution& solution)
{
    outputOp_->dat[CSM_REFERENCE_DS_IDX]->source = strdup(refce.description().c_str());
    int lineSize = needle.lastHyb();

    if (lineSize == 0)
    {
        lineSize =  1000;
    }

    //refce.generateLineDs(outputOp_->dat[CSM_REFERENCE_DS_IDX], solution.firstReferencePosition() - 200,
    //                     solution.lastReferencePosition() * maxBaseNmRate_ + 200, 0, lineSize);
    refce.generateLineDs(outputOp_->dat[CSM_REFERENCE_DS_IDX], 0,
                         refce.size(), 0, lineSize);
    return 0;
}
int ChiSquareSolutionViewer::drawStretchingRange(const PosFingerprint& needle,
        const ChiSquareCandidateSolution& solution)
{
    int refPivotPosition = solution.refPivotPosition();
    int needlePivotPosition = solution.needlePivotPosition();
    int extendpl = needle.lastHyb() - needlePivotPosition;
    int extendms = needle.firstHyb() - needlePivotPosition;
    d_s *ds = outputOp_->dat[CSM_STRETCHING_RANGE_DS_IDX];
    ds->xd[0] = refPivotPosition + extendms;
    ds->yd[0] = minBaseNmRate_ * extendms + needlePivotPosition;
    ds->xd[1] = refPivotPosition + extendpl;
    ds->yd[1] = minBaseNmRate_ * extendpl + needlePivotPosition;
    ds->xd[2] = refPivotPosition + extendms;
    ds->yd[2] = maxBaseNmRate_ * extendms + needlePivotPosition;
    ds->xd[3] = refPivotPosition + extendpl;
    ds->yd[3] = maxBaseNmRate_ * extendpl + needlePivotPosition;
    ds->xd[4] = refPivotPosition;
    ds->yd[4] = needlePivotPosition;
    ds->nx =  5;
    ds->ny = 5;
    return 0;
}
int ChiSquareSolutionViewer::drawMatchingHybs(const PosFingerprint& needle, const ChiSquareCandidateSolution& solution)
{
    d_s *mh_ds = outputOp_->dat[CSM_MATCHING_HYBS_DS_IDX];
    const MatchingHybVector& matchingHybs = solution.matchingHybs();
    mh_ds->nx = 0;
    mh_ds->ny = 0;
    int offset = solution.refPivotPosition() - solution.needlePivotPosition();

    for (uint i = 0; i < matchingHybs.size(); ++i)
    {
        auto hybs = matchingHybs[i];
        int pos = needle.positions()[i];

        for (uint j = 0; j < hybs.size(); ++j)
        {
            add_new_point_to_ds(mh_ds, hybs[j] + offset + solution.needlePivotPosition(), pos);
        }
    }

    return 0;
}

int ChiSquareSolutionViewer::drawChoosenHybs(const PosFingerprint& needle,
        const ChiSquareCandidateSolution& solution)
{
    d_s *mh_ds = outputOp_->dat[CSM_CHOOSEN_HYBS_DS_IDX];
    d_s *crh_ds = outputOp_->dat[CSM_CHOOSEN_REFERENCE_DS_IDX];
    d_s *cnh_ds = outputOp_->dat[CSM_CHOOSEN_NEEDLE_DS_IDX];
    int lineSize = needle.lastHyb();
    int offset = solution.refPivotPosition() - solution.needlePivotPosition();
    int offset_scale = solution.refPivotPosition() - solution.needlePivotPosition() * (1 / minBaseNmRate_);
    mh_ds->nx = 0;
    mh_ds->ny = 0;
    crh_ds->nx = 0;
    crh_ds->ny = 0;
    cnh_ds->nx = 0;
    cnh_ds->ny = 0;

    for (uint i = 0; i < solution.choosenHybs().size(); ++i)
    {
        int needle_pos  = needle.positions()[solution.choosenHybs()[i].first];
        int ref_pos     = solution.choosenHybs()[i].second + offset + solution.needlePivotPosition();
        add_new_point_to_ds(mh_ds, ref_pos, needle_pos);
        // Ref
        add_new_point_to_ds(crh_ds, ref_pos , 0);
        add_new_point_to_ds(crh_ds, ref_pos, lineSize);
        // Needle
        add_new_point_to_ds(cnh_ds, offset_scale, needle_pos);
        add_new_point_to_ds(cnh_ds, offset_scale + lineSize * (1/ minBaseNmRate_), needle_pos);
    }

    return 0;
}

int ChiSquareSolutionViewer::drawFit(const PosFingerprint& needle, const ChiSquareCandidateSolution& solution)

{
    int offset = solution.refPivotPosition();
    d_s *ds = outputOp_->dat[CSM_FIT_DS_IDX];
    int needlePivotPosition = solution.needlePivotPosition();
    int firstNeedleDist = needle.firstHyb() - needlePivotPosition;
    int lastNeedleDist = needle.lastHyb() - needlePivotPosition;
    const LinearParams& linFit = solution.linearFit();
    float firstPos = firstNeedleDist *  linFit.first;
    float lastPos = lastNeedleDist  * linFit.first;
    ds->xd[0] = firstNeedleDist + offset;
    ds->yd[0] = firstPos + needlePivotPosition;
    ds->xd[1] = lastNeedleDist + offset;
    ds->yd[1] = lastPos + needlePivotPosition;
    ds->nx = 2;
    ds->ny = 2;
    d_s *ds2 = outputOp_->dat[CSM_NOISE_RANGE_DS_IDX];
    ds2->nx = 4;
    ds2->ny = 4;
    ds2->xd[0] = ds->xd[0] + lowerNoiseInterv__;
    ds2->yd[0] = ds->yd[0];
    ds2->xd[1] = ds->xd[1] + lowerNoiseInterv__;
    ds2->yd[1] = ds->yd[1];
    ds2->xd[2] = ds->xd[0] + upperNoiseInterv__;
    ds2->yd[2] = ds->yd[0];
    ds2->xd[3] = ds->xd[1] + upperNoiseInterv__;
    ds2->yd[3] = ds->yd[1];
    return 0;
}
int ChiSquareSolutionViewer::reset()
{
    set_plot_title(outputOp_, "\\stack{{ChiSquare Fit View a=%f;%f}{No solution viewed}}", minBaseNmRate_, maxBaseNmRate_);
    outputOp_->dat[CSM_REFERENCE_DS_IDX]->nx = 0;
    outputOp_->dat[CSM_REFERENCE_DS_IDX]->ny = 0;
    outputOp_->dat[CSM_NEEDLE_DS_IDX]->nx = 0;
    outputOp_->dat[CSM_NEEDLE_DS_IDX]->ny = 0;
    outputOp_->dat[CSM_STRETCHING_RANGE_DS_IDX]->nx = 0;
    outputOp_->dat[CSM_STRETCHING_RANGE_DS_IDX]->ny = 0;
    outputOp_->dat[CSM_NOISE_RANGE_DS_IDX]->nx = 0;
    outputOp_->dat[CSM_NOISE_RANGE_DS_IDX]->ny = 0;
    outputOp_->dat[CSM_FIT_DS_IDX]->nx = 0;
    outputOp_->dat[CSM_FIT_DS_IDX]->ny = 0;
    outputOp_->dat[CSM_MATCHING_HYBS_DS_IDX]->nx = 0;
    outputOp_->dat[CSM_MATCHING_HYBS_DS_IDX]->ny = 0;
    outputOp_->dat[CSM_CHOOSEN_HYBS_DS_IDX]->nx = 0;
    outputOp_->dat[CSM_CHOOSEN_HYBS_DS_IDX]->ny = 0;
    outputOp_->need_to_refresh = 1;
    refresh_plot(outputPr_, outputPr_->n_op - 1);
    return 0;
}

}
