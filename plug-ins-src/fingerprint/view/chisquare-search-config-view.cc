#include "chisquare-search-config-view.hh"

chiSquareMethod::ConfigView::ConfigView()
{
}

chiSquareMethod::ConfigView::~ConfigView()
{
}

int chiSquareMethod::ConfigView::draw(ChiSquareSearch& se)
{
    form_window_t *form = form_create("ChiSquareConfig");
    float stretchingMean = se.nmBaseRateDistribution().mean();
    float stretchingDev = se.nmBaseRateDistribution().standard_deviation();
    double stretchingConfidenceLevel = se.stretchingConfidenceLevel();
    int minUmDegreeOfFreedom = se.minUmDegreeOfFreedom();
    double minFoundRate = se.minFoundRate();
    double gammaSignificanceLevel = se.gammaSignificanceLevel();
    double expectedNoiseDeviation = se.expectedNoiseDeviation();
    double noiseConfidenceLevel = se.noiseConfidenceLevel();

    form_push_label(form, "\t NmBaseRate mean value");
    form_push_float(form, 0.1, &stretchingMean);
    form_newline(form);
    form_push_label(form, "\t NmbaseRate standard deviation");
    form_push_float(form, 0.1, &stretchingDev);
    form_newline(form);
    form_push_label(form, "\t Confidence Level (stetching)");
    form_push_double(form, 0.1, &stretchingConfidenceLevel);
    form_newline(form);
    form_push_label(form, "\t Min Univoque match degree of freedom");
    form_push_int(form, &minUmDegreeOfFreedom);
    form_newline(form);
    form_push_label(form, "\t Min found rate");
    form_push_double(form, 0.1, &minFoundRate);
    form_newline(form);
    form_push_label(form, "\t chiSquare(gamma) significance level");
    form_push_double(form, 0.1, &gammaSignificanceLevel);
    form_newline(form);
    form_push_label(form, "\t expected noise deviation");
    form_push_double(form, 0.1, &expectedNoiseDeviation);
    form_newline(form);
    form_push_label(form, "\t noise confidence interval");
    form_push_double(form, 0.1, &noiseConfidenceLevel);
    form_newline(form);

    if (form_run(form) != WIN_OK)
    {
        form_free(form);
        return -1;
    }

    se.nmBaseRateDistribution(NmBaseRateDistribution(stretchingMean, stretchingDev));
    se.stretchingConfidenceLevel(stretchingConfidenceLevel);
    se.minUmDegreeOfFreedom(minUmDegreeOfFreedom);
    se.minFoundRate(minFoundRate);
    se.gammaSignificanceLevel(gammaSignificanceLevel);
    se.expectedNoiseDeviation(expectedNoiseDeviation);
    se.noiseConfidenceLevel(noiseConfidenceLevel);

    return 0;
}
