#pragma once
#include "xvin.h"
#include "../benchmark/benchmark-generator.hh"
#include "../results/benchmark-step-results.hh"

namespace benchmark
{

template <typename SearchEngine>
class View
{
  public:
    View();
    virtual ~View();

    int draw(Params<SearchEngine>& params, AggregatedResults<SearchEngine>& stepResults);
    int drawScore(Params<SearchEngine>& params, std::vector<typename SearchEngine::results_type> results);



  private:
    O_p *outputOp_;
    O_p *scoreOp_;
    pltreg *outputPr_;

    int configParamView(float stepIntervalValue, int numberOfStep);
    int drawTitle(Params<SearchEngine>& bengen);
    int drawData(Params<SearchEngine>& bengen, AggregatedResults<SearchEngine>& stepResults);

    enum DsIndex
    {
        BOXPLOT = 0,
        FOUND = 1,
        NOTFOUND = 2,
        DURATION = 3
    };
};

}
#include "benchmark-view.hxx"
