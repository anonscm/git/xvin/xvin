#pragma once
#include "candidate-solution.hh"
/**
 * @brief Candidat solutions from OligoSequenceFinder
 *
 * Current Status : Not implemented (see .cc file) / Work in Progress.
 *
 */
class OligoSequenceSolution:
public CandidateSolution
{
public:
    OligoSequenceSolution (int position);
    virtual ~OligoSequenceSolution ();

    unsigned int size() const;
    virtual double score() const;
    int startPosition() const;

    int next();
    int addScore(float score, bool invert);
    unsigned int cursor() const;

private:
    float currentScore_;
    int startPosition_;
    int currentSize_;
    unsigned int cursor_;

};
