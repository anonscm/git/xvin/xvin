#pragma once
#include <vector>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <chrono>
#include "json.hpp"
using json = nlohmann::json;

namespace benchmark
{

namespace bacc = boost::accumulators;
using Accu =  bacc::accumulator_set < double,
      bacc::stats<
      bacc::tag::count,
      bacc::tag::mean(bacc::lazy),
      bacc::tag::max,
      bacc::tag::min,
      bacc::tag::variance(bacc::lazy)>>;

template<typename SearchEngine>
struct AggregateData
{
    float aggrValue = 0.;
    Accu rankAccu;
    int notFoundCount = 0;
    std::chrono::high_resolution_clock::duration foundDuration = std::chrono::high_resolution_clock::duration::zero();
    std::chrono::high_resolution_clock::duration notFoundDuration = std::chrono::high_resolution_clock::duration::zero();
    std::vector<typename SearchEngine::results_type> results;

    json haystackInfos;
    json dumpJson(void);
};

template<typename SearchEngine>
using AggregateDatas = std::vector<AggregateData<SearchEngine>>;

template<typename SearchEngine>
struct AggregatedResult
{
  public:
    int step = 0;
    float aggrValue = 0.;
    int foundCount = 0;
    int notFoundCount = 0;
    std::chrono::high_resolution_clock::duration durationMean =  std::chrono::high_resolution_clock::duration::zero();;
    std::chrono::high_resolution_clock::duration notFoundDurationMean = std::chrono::high_resolution_clock::duration::zero();
    float mean = 0.;
    float min = 0.;
    float max = 0.;
    float variance = 0.;
    std::vector<typename SearchEngine::results_type> results;

    json haystackInfos;
    json dumpJson(void);

};

template<typename SearchEngine>
using AggregatedResults = std::vector<AggregatedResult<SearchEngine>> ;

}

#include "benchmark-step-results.hxx"
