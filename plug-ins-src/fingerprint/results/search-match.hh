#pragma once
#include "candidate-solution.hh"

#include <iostream>
#include <map>
#include <set>

class SearchMatchBuilder;
typedef std::map<int, int> HaystackNeedleMatchMap;
typedef std::map<int, double> DeviationMap;
//typedef std::map<int, int> HaystackMatchCountMap;
/**
 * @brief Candidat solution from BasicFinder
 */
#include <json.hpp>
using json = nlohmann::json;

class SearchMatch:
                  public CandidateSolution
{
    friend SearchMatchBuilder;
    public:
        SearchMatch ();
        virtual ~SearchMatch ();

        virtual int firstReferencePosition(void) const;
        int lastReferencePosition(void) const;
        int originNeedlePosition(void) const;
        int lastNeedlePosition(void) const;
        double error(void) const;
        double errorRate(void) const;
        double nmBaseRate(void) const;
        double chiSquare(void) const;
        int chromosome(void) const;
        int needle(void) const;
        bool reversed(void) const;

        virtual double score() const;

        const HaystackNeedleMatchMap& haystackNeedleMatchMap(void) const;

        void needle(int ndl);
        void chromosome(int chr);
        void reversed(bool rev);
        void reverse();

        bool operator< (const SearchMatch &sm1) const;
        friend std::ostream& operator<< (std::ostream& ostr, const SearchMatch& o);
        json dumpJson() const;

    private:
        int originHaystackPosition_;
        int lastHaystackPosition_;
        int originNeedlePosition_;
        int lastNeedlePosition_;
        bool reversed_;
        double error_;
        double errorRate_;
        double nmBaseRate_;
        double chiSquare_;
        double chiSquareProbability_;
        int chromosome_;
        int needle_;
        HaystackNeedleMatchMap haystackNeedleMatchMap_;
        //HaystackMatchCountMap  haystackMatchCountMap_;
        DeviationMap deviationMap_;
        std::set<int> notFoundInHaystackHyb_;
        std::set<int> notFoundInNeedleHyb_;
};
/*
struct SearchMatchCmp {
  bool operator() (const SearchMatch* lhs, const SearchMatch* rhs) const
  {
      return *lhs < *rhs;
  }
};
*/
