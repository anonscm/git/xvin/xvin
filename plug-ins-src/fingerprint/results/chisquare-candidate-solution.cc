#include "chisquare-candidate-solution.hh"
namespace chiSquareMethod
{
ChiSquareCandidateSolution::ChiSquareCandidateSolution()
{
}

ChiSquareCandidateSolution::~ChiSquareCandidateSolution()
{
}

int ChiSquareCandidateSolution::needlePivotPosition() const
{
    return needlePivotPosition_;
}

const MatchingHybVector& ChiSquareCandidateSolution::matchingHybs(void) const
{
    return matchingHybs_;
}

const LinearParams& ChiSquareCandidateSolution::linearFit(void) const
{
    return linearFit_;
}

int ChiSquareCandidateSolution::firstReferencePosition() const
{
    if (matchingHybs_.empty())
    {
        return -1;
    }

    for (auto hybs : matchingHybs_)
    {
        if (!hybs.empty())
        {
            return hybs.front() + refPivotPosition_;
        }
    }

    return  -1;
}

int ChiSquareCandidateSolution::lastReferencePosition() const
{
    if (matchingHybs_.empty())
    {
        return -1;
    }

    for (auto hybs = matchingHybs_.crbegin(); hybs != matchingHybs_.crend(); ++hybs)
    {
        if (!hybs->empty())
        {
            return hybs->front() + refPivotPosition_;
        }
    }

    return  -1;
}
double ChiSquareCandidateSolution::umChiSquareValue(void) const
{
    return umChiSquareValue_;
}

double ChiSquareCandidateSolution::umCriticalValue(void) const
{
    return umCriticalValue_;
}

int ChiSquareCandidateSolution::umDegreeOfFreedom(void) const
{
    return umDegreeOfFreedom_;
}


double ChiSquareCandidateSolution::chiSquareValue(void) const
{
    return chiSquareValue_;
}

double ChiSquareCandidateSolution::score() const
{
    return chiSquareValue_;
}

double ChiSquareCandidateSolution::criticalValue(void) const
{
    return criticalValue_;
}

int ChiSquareCandidateSolution::degreeOfFreedom(void) const
{
    return degreeOfFreedom_;
}
int ChiSquareCandidateSolution::refPivotPosition() const
{
    return refPivotPosition_;
}

const std::vector<std::pair<int, int>>& ChiSquareCandidateSolution::choosenHybs() const
{
    return choosenHybs_;
}
double ChiSquareCandidateSolution::refHybCount(void) const
{
    return refHybCount_;
}

json ChiSquareCandidateSolution::dumpJson() const
{
    json j;
    j["origNeeIdx"] = originNeedleIdx_;
    j["origRefIdx"] = originReferenceIdx_;
    j["dir"] = direction_ ==  Direction::FORWARD ? "FWD" : "BWD";
    j["neePivotPos"] = needlePivotPosition_;
    j["refPivotPos"] = refPivotPosition_;
    j["linFit"]["slope"] = linearFit_.first;
    j["linFit"]["offset"] = linearFit_.second;
    j["umChiSqVal"] = umChiSquareValue_;
    j["umCritVal"] = umCriticalValue_;
    j["umDegFree"] = umDegreeOfFreedom_;
    j["chiSqVal"] = chiSquareValue_;
    j["critVal"] = criticalValue_;
    j["degFree"] = degreeOfFreedom_;
    j["refHybCnt"] = refHybCount_;
    j["mtchHybs"] = json::object();
    j["mtchHybsFit"] = json::object();
    j["unvqMatchHybs"] = json::object();
    j["choosenHybs"] = json::object();

    for (int i = 0; i <  matchingHybs_.size(); ++i)
    {
        auto mh = matchingHybs_[i];
        j["matchHybs"][std::to_string(i)] = mh;
    }

    for (int i = 0; i <  matchingHybsFit_.size(); ++i)
    {
        auto mhf = matchingHybsFit_[i];
        j["matchHybsFit"][std::to_string(i)] = mhf;
    }

    for (int i = 0; i <  univoqueMatchingHybs_.size(); ++i)
    {
        auto umh = univoqueMatchingHybs_[i];
        j["unvqMatchHybs"][std::to_string(umh.first)] = umh.second;
    }

    for (int i = 0; i <  choosenHybs_.size(); ++i)
    {
        auto ch = choosenHybs_[i];
        j["choosenHybs"][std::to_string(ch.first)] = ch.second;
    }

    return j;
}



}

#ifdef FX_PYTHON_BOOST
BOOST_PYTHON_MODULE(fingerprint)
{
    namespace csm = chiSquareMethod;
    typedef csm::ChiSquareCandidateSolution cscs;
    bp::class_<csm::LinearParams>("LinearParams")
    .def_readwrite("first", &csm::LinearParams::first)
    .def_readwrite("second", &csm::LinearParams::second);
    bp::class_<cscs>("ChiSquareCandidateSolution")
    .add_property("needlePivotPosition", &cscs::needlePivotPosition)
    .add_property("linearFit", &cscs::linearFit)
    //.add_property("matchingHybs", &chiSquareMethod::ChiSquareCandidateSolution::matchingHybs)
    ;
}
#endif
