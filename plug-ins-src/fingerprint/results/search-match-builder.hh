#pragma once
#include "search-match.hh"
#include "../sequence/hybridation.hh"
#include "../sequence/dist-fingerprint.hh"
class SearchMatchBuilder
{
    public:
        SearchMatchBuilder(const DistFingerprint& hay, const DistFingerprint& needle);
        SearchMatchBuilder(const SearchMatchBuilder& smb);
        void create(int originHaystackPosition);
        void create(int originHaystackPosition,
                    int originNeedlePosition,
                    double nmBaseRate);
        void abort();
        virtual ~SearchMatchBuilder ();
        double addNotFoundInHaystackHyb(int pos, const Hybridation& hyb);
        double addNotFoundInNeedleHyb(int pos, const Hybridation& hyb);
        double addHaystackNeedleMatch(int hayPos, int needlePos);
        void originNeedlePosition(int pos);
        void nmBaseRate(double rate);
        SearchMatch* searchMatch();



    private:
        SearchMatch* searchMatch_;
        const DistFingerprint& haystack_;
        const DistFingerprint& needle_;
};
