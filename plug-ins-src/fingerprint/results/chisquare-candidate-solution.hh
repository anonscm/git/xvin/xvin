#pragma once
#ifdef FX_PYTHON_BOOST
#include <boost/python.hpp>
namespace bp = boost::python;
#endif
#include "candidate-solution.hh"
#include "../sequence/fingerprint.hh"
#include "../define.hh"
#include <json.hpp>
using json = nlohmann::json;

namespace chiSquareMethod
{
class ChiSquareCandidateSolution : public CandidateSolution
{
  public:

    friend class ChiSquareSearch;
    ChiSquareCandidateSolution();
    virtual ~ChiSquareCandidateSolution();
    virtual int firstReferencePosition() const;
    int lastReferencePosition() const;
    int needlePivotPosition() const;
    int refPivotPosition() const;
    const MatchingHybVector& matchingHybs(void) const;
    const LinearParams& linearFit(void) const;
    double chiSquareValue(void) const;
    double criticalValue(void) const;
    int degreeOfFreedom(void) const;
    double umChiSquareValue(void) const;
    double umCriticalValue(void) const;
    int umDegreeOfFreedom(void) const;
    const std::vector<std::pair<int, int>>& choosenHybs(void) const;
    double refHybCount(void) const;
    virtual double score() const;

    json dumpJson() const;

  private:
    int needlePivotPosition_ = 0;
    int refPivotPosition_ = 0;
    LinearParams linearFit_ = LinearParams(0,0);
    double umChiSquareValue_ = 0;
    double umCriticalValue_ = 0;
    int umDegreeOfFreedom_ = 0;
    double chiSquareValue_ = 0; 
    double criticalValue_ = 0;
    int degreeOfFreedom_ = 0;
    MatchingHybVector matchingHybs_ = MatchingHybVector();
    MatchingHybVector matchingHybsFit_ = MatchingHybVector();
    std::vector<std::pair<int,int>> univoqueMatchingHybs_;
    std::vector<std::pair<int,int>> choosenHybs_;
    int refHybCount_ = 0;

};
}
