#pragma once

#include "dist-oligo-solution.hh"
#include "oligo-sequence-solution.hh"
#include "chisquare-candidate-solution.hh"
#include "../sequence/fingerprint.hh"
#include "../sequence/dist-oligo-sequence.hh"
#include "../define.hh"
#include <chrono>
#include <vector>

#include <json.hpp>
using json = nlohmann::json;
/**
 * @brief Result Summary for benchmarking purpose
 *
 * @tparam SolutionType
 */
template <typename SolutionType, typename FingerprintType = Fingerprint>
class SearchResult
{
  public:
    using value_type = SolutionType;
    using fgp_type = FingerprintType;

    SearchResult();
    SearchResult(
        std::vector<SolutionType> solutions,
        std::chrono::high_resolution_clock::duration duration);
    virtual ~SearchResult();

    SearchSolutions<value_type>& solutions();
    const SearchSolutions<value_type>& solutions() const;
    const std::chrono::high_resolution_clock::duration duration() const;
    void duration(std::chrono::high_resolution_clock::duration duration);
    int firstTruePositiveRank() const;

    void firstTruePositiveRank(int rank);

    std::shared_ptr<const FingerprintType> needle() const;
    int needle(std::shared_ptr<const FingerprintType> needle);

    const value_type& operator[](std::size_t idx) const;

    json dumpJson(const std::string& folder, int resIdx) const;

  //private:
    std::vector<value_type> solutions_ = std::vector<value_type> ();
    std::chrono::high_resolution_clock::duration duration_ = std::chrono::high_resolution_clock::duration::zero();
    int firstTruePositiveRank_ = -1; // during origin checking
    std::shared_ptr<const FingerprintType> needle_ = nullptr;
    std::vector<float> scores_ = std::vector<float> ();

    void extractScores();
    void truncateSolutions();
};


typedef SearchResult<DistOligoSolution, DistOligoSequence> DistSearchResult;
typedef SearchResult<OligoSequenceSolution> OligoSequenceResult;
typedef SearchResult<chiSquareMethod::ChiSquareCandidateSolution> ChiSquareSearchResult;
typedef SearchResult<SearchMatch> BasicSearchResult;

#include "search-result.hxx"
