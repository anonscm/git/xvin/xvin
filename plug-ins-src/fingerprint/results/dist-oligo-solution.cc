#include "dist-oligo-solution.hh"
#include "../define.hh"

DistOligoSolution::DistOligoSolution(int startPosition)
    : startPosition_(startPosition),
    curHayIntervalStart (startPosition)
{
}

DistOligoSolution::DistOligoSolution()
    : DistOligoSolution(0)
{
}

DistOligoSolution::~DistOligoSolution()
{
}


double DistOligoSolution::score() const
{
    return currentScore;
}

int DistOligoSolution::firstReferencePosition() const
{
    return startPosition_;
}

unsigned int DistOligoSolution::size() const
{
    return currentSize;
}

bool DistOligoSolution::operator <(const DistOligoSolution& rhs)
{
    return (this->currentScore < rhs.currentScore) || (this->currentScore == rhs.currentScore
            && this->currentSize > rhs.currentSize);
}

bool DistOligoSolution::operator <(const DistOligoSolution& rhs) const
{
    return (this->currentScore < rhs.currentScore) || (this->currentScore == rhs.currentScore
            && this->currentSize > rhs.currentSize);
}


void DistOligoSolution::addMatchPair(int ref, int nee)
{
    matchPair_.push_back(std::pair<int, int>(ref, nee));
}
void DistOligoSolution::addMatchDist(int ref, int nee)
{
    matchDist_.push_back(std::pair<int, int>(ref, nee));
}

json DistOligoSolution::dumpJson() const
{
    json j;
    j["oriNeeIdx"] = originNeedleIdx_;
    j["oriRefIdx"] = originReferenceIdx_;
    j["dir"] = direction_ ==  Direction::FORWARD ? "FORWARD" : "BACKWARD";
    j["score"] = currentScore;
    j["size"] = currentSize;
    j["startPos"] = startPosition_;
    j["matchPair"] = json::array();
    j["totalGapSize"] = totalGapSize;

    if (!matchPair_.empty())
    {
        for (uint i = 0; i < matchPair_.size(); ++i)
        {
            json jpair = json::array();
            jpair.push_back(matchPair_[i].first);
            jpair.push_back(matchPair_[i].second);
            j["matchPair"].push_back(jpair);
        }
    }
    if (!matchDist_.empty())
    {
        for (uint i = 0; i < matchDist_.size(); ++i)
        {
            json jpair = json::array();
            jpair.push_back(matchDist_[i].first);
            jpair.push_back(matchDist_[i].second);
            j["matchDist"].push_back(jpair);
        }
    }

    return j;
}
