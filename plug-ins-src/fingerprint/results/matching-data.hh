#pragma once

#ifndef XV_WIN32
# include "config.h"
#endif

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>


/**
 * @brief Result summary from BasicFinder
 */
class MatchingData
{
    friend class boost::serialization::access;
    public:
    MatchingData ();
    MatchingData (int matchCount,
                  int needleSize,
                  int needleHybridationCount,
                  int coperture,
                  bool foundAtOrigin,
                  int originalPosRank,
                  int chromosomeCount,
                  int oligoCount);
    virtual ~MatchingData ();

    double matchCount(void) const;
    double needleHybridationCount(void) const;
    double coperture(void) const;
    double needleSize(void) const;
    double foundAtOrigin(void) const;
    double originalPosRank(void) const;
    double chromosomeCount(void) const;
    double oligoCount(void) const;

    double uniqueMatch(void) const;// Not a getter.

    // Archive
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version);

    typedef double (MatchingData::* GetterFunc)(void) const;

    private:
    int matchCount_;
    int needleSize_;
    int needleHybridationCount_;
    int foundAtOrigin_;
    int originalPosRank_;
    int coperture_;
    int chromosomeCount_;
    int oligoCount_;

};

BOOST_CLASS_VERSION(MatchingData, 5)
#include "matching-data.hxx"
