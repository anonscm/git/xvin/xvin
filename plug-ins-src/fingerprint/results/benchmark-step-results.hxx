namespace benchmark
{
template<typename SearchEngine>
json AggregateData<SearchEngine>::dumpJson(void)
{
    json j;
    j["step"] = aggrValue;
    j["results"] = json::array();

    for (auto result : results)
    {
        j["results"].push_back(result.dumpJson());
    }
}
template<typename SearchEngine>
json AggregatedResult<SearchEngine>::dumpJson(void)
{
    json j;
    j["step"] = step;
    j["haystack"] = haystackInfos;
    j["results"] = json::array();

    for (auto result : results)
    {
        j["results"].push_back(result.dumpJson());
    }
}
}
