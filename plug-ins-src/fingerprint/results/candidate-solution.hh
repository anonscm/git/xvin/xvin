#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif
#include <json.hpp>
using json = nlohmann::json;
/**
 * @brief Abstract class representing Solutions returned by *Finder algorithm family
 */


class CandidateSolution
{
  public:

    CandidateSolution();
    virtual ~CandidateSolution();

    typedef enum {
        FORWARD, // 5' -> 3'
        BACKWARD // 3' -> 5'
    
    } Direction;

    virtual int firstReferencePosition() const = 0;
    virtual  Direction direction() const;
    virtual void direction(Direction dir);
    int originNeedleIdx() const;
    int originReferenceIdx() const;

    virtual json dumpJson() const = 0;
    virtual double score() const = 0;

  protected:
    /**
     * @brief Needle from which came this candidate solution
     */
    int originNeedleIdx_ = -1;
    /**
     * @brief Reference from which came this candidate solution
     */
    int originReferenceIdx_ = -1;

    Direction direction_ = Direction::FORWARD;

};


#include "candidate-solution.hxx"
