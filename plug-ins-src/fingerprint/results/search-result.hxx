#pragma once
#include <iostream>
#include <fstream>

template<typename SolutionType, typename FingerprintType>
SearchResult<SolutionType, FingerprintType>::SearchResult(
    SearchSolutions<SolutionType> solutions,
    std::chrono::high_resolution_clock::duration duration)
    : solutions_(solutions),
      duration_(duration)
{
}

template<typename SolutionType, typename FingerprintType>
SearchResult<SolutionType, FingerprintType>::SearchResult()
{
}

template<typename SolutionType, typename FingerprintType>
SearchResult<SolutionType, FingerprintType>::~SearchResult()
{
}

template<typename SolutionType, typename FingerprintType>
const std::chrono::high_resolution_clock::duration SearchResult<SolutionType, FingerprintType>::duration() const
{
    return duration_;
}

template<typename SolutionType, typename FingerprintType>
SearchSolutions<SolutionType>& SearchResult<SolutionType, FingerprintType>::solutions()
{
    return solutions_;
}
template<typename SolutionType, typename FingerprintType>
const SearchSolutions<SolutionType>& SearchResult<SolutionType, FingerprintType>::solutions() const
{
    return solutions_;
}

template<typename SolutionType, typename FingerprintType>
int SearchResult<SolutionType, FingerprintType>::firstTruePositiveRank() const
{
    return firstTruePositiveRank_;
}

template<typename SolutionType, typename FingerprintType>
void SearchResult<SolutionType, FingerprintType>::firstTruePositiveRank(int rank)
{
    firstTruePositiveRank_ = rank;
}

template<typename SolutionType, typename FingerprintType>
const SolutionType& SearchResult<SolutionType, FingerprintType>::operator[](std::size_t idx) const
{
    return solutions_[idx];
}

template<typename SolutionType, typename FingerprintType>
std::shared_ptr<const FingerprintType> SearchResult<SolutionType, FingerprintType>::needle() const
{
    return needle_;
}

template<typename SolutionType, typename FingerprintType>
int SearchResult<SolutionType, FingerprintType>::needle(std::shared_ptr<const FingerprintType> needle)
{
    needle_ = needle;
    return 0;
}
template<typename SolutionType, typename FingerprintType>
void SearchResult<SolutionType, FingerprintType>::duration(std::chrono::high_resolution_clock::duration duration)
{
    duration_ = duration;
}

template<typename SolutionType, typename FingerprintType>
json SearchResult<SolutionType, FingerprintType>::dumpJson(const std::string& folder, int resIdx) const
{
    std::ofstream myfile;
    json j;
    json solj;
    j["idx"] = resIdx;

    if (needle_ != nullptr)
    {
        j["needle"] = needle_->dumpJson();
    }

    j["duration"] = std::chrono::duration_cast<std::chrono::microseconds> (duration_).count() /  1000.;
    j["fstTruePos"] = firstTruePositiveRank_;
    j["fstSols"] = json::array();
    solj["sols"] = json::array();

    for (int i = 0; i < solutions_.size(); ++i)
    {
        if (i <= (firstTruePositiveRank_ + 1) || folder == "")
        {
            j["fstSols"].push_back(solutions_[i].dumpJson());
        }

        if ((solutions_.size() > firstTruePositiveRank_ + 2) && folder != "")
        {
            solj["sols"].push_back(solutions_[i].dumpJson());
        }
    }

    // TODO remove
    //j["allSols"] = solj["sols"];

    if ((solutions_.size() > firstTruePositiveRank_ + 2) && folder != "")
    {
        std::string filepath = folder + std::string("/res") + std::to_string(resIdx) + ".json";
        myfile.open(filepath);
        myfile << std::setprecision(4) << solj.dump()  << std::endl;
        myfile.flush();
        myfile.close();
    }

    if (scores_.size() > 0)
    {
        j["scores"] = scores_;
    }

    return j;
}
template<typename SolutionType, typename FingerprintType>
void SearchResult<SolutionType, FingerprintType>::extractScores()
{
    for (auto sol : solutions_)
    {
        scores_.push_back(sol.score());
    }
}

template<typename SolutionType, typename FingerprintType>
void SearchResult<SolutionType, FingerprintType>::truncateSolutions()
{
    if (firstTruePositiveRank_ >= 0)
    {
        if (firstTruePositiveRank_ + 1 == solutions_.size())
        {
            solutions_.resize(firstTruePositiveRank_ + 1);
        }
        else
        {
            solutions_.resize(firstTruePositiveRank_ + 2);
        }
    }
}
