#pragma once
#include "../define.hh"
#include "candidate-solution.hh"
#include <json.hpp>
using json = nlohmann::json;

/**
 * @brief Internal object used by the RankFinder to construct Rank of an hybridization looking at
 * his neighboors. Best rated FingerprintRankVector objects are outputed by RankFinder as best
 * canditat for a mapping.
 */
template<typename T>
class FingerprintRankVector:
public CandidateSolution
{
  public:
    FingerprintRankVector(int refHybCount, int needleHybCount, const T& value);
    virtual ~FingerprintRankVector();

    const T& getRanking(int hybIdx, int needleHybIdx) const;
    int setRanking(int hybIdx, int needleHybIdx, const T& value);
    int addRanking(int hybIdx, int needleHybIdx, const T& value);
    int addDistRanking(int hybIdx, int origin, int target, const T& value);
    std::pair<int, const T&> maxRanking(int hybIdx) const;
    int setRanks(const T& value);
    int threshold(int hybIdx, int threshold, int value);
    int binarize(int hybIdx, int threshold, int valueBelow, int valueAbove);
    int updateRanksFromDistMatrices(int hayIdx);
    int firstReferencePosition() const;
    double score() const;

    json dumpJson() const;

  private:
    int haystackHybCount_;
    int needleHybCount_;
    RankMatrix<T> ranks_;
    std::vector<DistMatrix<T>*> distMatrices_;
};

#include "fingerprint-rank-vector.hxx"
