#pragma once

template<class Archive>
void MatchingData::serialize(Archive & ar, const unsigned int version)
{
    ar & needleSize_;
    ar & needleHybridationCount_;
    ar & matchCount_;
    ar & foundAtOrigin_;
    if (version >= 3)
    {
        ar & originalPosRank_;
    }
    ar & coperture_;
    if (version >= 2)
    {
        ar & chromosomeCount_;
    }
    if (version >= 4)
    {
        ar & oligoCount_;
    } 
}

//template
//void MatchingData::serialize(boost::archive::text_oarchive & ar, const unsigned int version);
//
//template
//void MatchingData::serialize(boost::archive::text_iarchive & ar, const unsigned int version);
