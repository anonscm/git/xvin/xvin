#include "candidate-solution.hh"

CandidateSolution::CandidateSolution()
{
}

CandidateSolution::~CandidateSolution()
{
}
int CandidateSolution::originNeedleIdx() const
{
	return originNeedleIdx_;
}

int CandidateSolution::originReferenceIdx() const
{
	return originReferenceIdx_;
}

CandidateSolution::Direction CandidateSolution::direction() const
{
	return direction_;
}

 void CandidateSolution::direction(CandidateSolution::Direction dir) 
{
	direction_ = dir;
}
