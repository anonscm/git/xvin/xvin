#include "search-match-builder.hh"
#include "../sequence/fingerprint.hh"
#include <cmath>
#include <gsl/gsl_randist.h>
SearchMatchBuilder::SearchMatchBuilder(const SearchMatchBuilder& smb)
:searchMatch_ (new SearchMatch(*smb.searchMatch_)),
    haystack_ (smb.haystack_),
    needle_ (smb.needle_)
{
}

SearchMatchBuilder::SearchMatchBuilder(const DistFingerprint& hay, const DistFingerprint& needle)
:searchMatch_ (0),
    haystack_ (hay),
    needle_ (needle)
{
}

SearchMatchBuilder::~SearchMatchBuilder()
{
}


double SearchMatchBuilder::addHaystackNeedleMatch(int hayPos, int needlePos)
{
    HaystackNeedleMatchMap& hnmp = searchMatch_->haystackNeedleMatchMap_;
    DeviationMap&  dm = searchMatch_->deviationMap_;
    DeviationMap::iterator lastDevIt = dm.find(hayPos);
    double curDeviation = Fingerprint::stretchedPositiond(needlePos - searchMatch_->originNeedlePosition_,
                                                          searchMatch_->nmBaseRate_)
        - (hayPos - searchMatch_->originHaystackPosition_); 

    if (lastDevIt == dm.end() || std::abs(lastDevIt->second) > std::abs(curDeviation))
    {
        //std::cout << searchMatch_->originHaystackPosition_ << ": needlePos->" << needlePos << "nmBaseRate->" << searchMatch_->nmBaseRate_ << "deviation->" << curDeviation << std::endl;
        double lastDevValue = dm[hayPos];
        hnmp[hayPos] = needlePos;
        dm[hayPos] = curDeviation;
        searchMatch_->chiSquare_ += -std::pow( (double) lastDevValue / THEORICAL_STANDARD_DEVIATION, 2.) 
            + std::pow( (double) curDeviation / THEORICAL_STANDARD_DEVIATION, 2.);
        searchMatch_->errorRate_ = searchMatch_->error_ / needle_.nbHybridation();
    }

    searchMatch_->lastHaystackPosition_ = hnmp.rbegin()->first;
    searchMatch_->lastNeedlePosition_ = needlePos;
    if ( hnmp.size() > 0)
        searchMatch_->chiSquareProbability_ = gsl_ran_chisq_pdf (searchMatch_->chiSquare(), hnmp.size());
    return searchMatch_->chiSquare_;
}

double SearchMatchBuilder::addNotFoundInHaystackHyb(int pos, const Hybridation& hyb)
{
    std::set<int> &nfihh = searchMatch_->notFoundInHaystackHyb_;
    if (nfihh.find(pos) == nfihh.end())
    {
        nfihh.insert(pos);
        searchMatch_->error_ += hyb.probability();
        searchMatch_->errorRate_ = searchMatch_->error_ / needle_.nbHybridation();
    }
    return searchMatch_->error_;
}

double SearchMatchBuilder::addNotFoundInNeedleHyb(int pos, const Hybridation& hyb)
{
    std::set<int> &nfinh = searchMatch_->notFoundInNeedleHyb_;
    if (nfinh.find(pos) == nfinh.end())
    {
        nfinh.insert(pos);
        searchMatch_->error_ += hyb.probability();
        searchMatch_->errorRate_ = searchMatch_->error_ / needle_.nbHybridation();
    }
    return searchMatch_->error_;
}

SearchMatch* SearchMatchBuilder::searchMatch()
{
    return searchMatch_;
}

void SearchMatchBuilder::abort()
{
    delete searchMatch_;
    searchMatch_ = 0;
}

void SearchMatchBuilder::create(int originHaystackPosition,
                                int originNeedlePosition,
                                double nmBaseRate)
{
    searchMatch_ = new SearchMatch();

    searchMatch_->originHaystackPosition_ = originHaystackPosition;
    searchMatch_->originNeedlePosition_ = originNeedlePosition;
    searchMatch_->nmBaseRate_ = nmBaseRate;

}

void SearchMatchBuilder::create(int originHaystackPosition)
{
    searchMatch_ = new SearchMatch();
    searchMatch_->originHaystackPosition_ = originHaystackPosition;
}

void SearchMatchBuilder::nmBaseRate(double rate)
{
    searchMatch_->nmBaseRate_ = rate;
}

void SearchMatchBuilder::originNeedlePosition(int pos)
{
    searchMatch_->originNeedlePosition_ = pos;
}
