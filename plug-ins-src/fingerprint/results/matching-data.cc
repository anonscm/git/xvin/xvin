#include "matching-data.hh"

MatchingData::MatchingData()
:matchCount_ (0),
    needleSize_ (0),
    needleHybridationCount_ (0),
    foundAtOrigin_ (0),
    originalPosRank_ (0),
    coperture_ (0),
    chromosomeCount_ (0),
    oligoCount_ (0)
{
}

MatchingData::MatchingData(int matchCount,
                           int needleSize,
                           int needleHybridationCount,
                           int coperture,
                           bool foundAtOrigin,
                           int originalPosRank,
                           int chromosomeCount,
                           int oligoCount)
:matchCount_ (matchCount),
    needleSize_ (needleSize),
    needleHybridationCount_ (needleHybridationCount),
    foundAtOrigin_ (foundAtOrigin),
    originalPosRank_ (originalPosRank),
    coperture_ (coperture),
    chromosomeCount_ (chromosomeCount),
    oligoCount_ (oligoCount)
{
}

MatchingData::~MatchingData()
{
}

double MatchingData::coperture(void) const
{
    return coperture_;
}

double MatchingData::matchCount(void) const
{
    return matchCount_;
}

double MatchingData::needleHybridationCount(void) const
{
    return needleHybridationCount_;
}

double MatchingData::needleSize(void) const
{
    return needleSize_;
}
double MatchingData::foundAtOrigin(void) const
{
    return foundAtOrigin_;
}
double MatchingData::originalPosRank(void) const
{
    return originalPosRank_;
}
double MatchingData::chromosomeCount(void) const
{
    return chromosomeCount_;
}

double MatchingData::oligoCount(void) const
{
    return oligoCount_;
}

double MatchingData::uniqueMatch(void) const
{
    return matchCount_ == 1;
}
