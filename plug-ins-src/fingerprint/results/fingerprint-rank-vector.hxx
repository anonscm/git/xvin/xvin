#pragma once
#include <cassert>
#include <iostream>
#include <algorithm>
template<typename T>
FingerprintRankVector<T>::FingerprintRankVector(int refHybCount, int needleHybCount, const T& value)
    : haystackHybCount_(refHybCount),
      needleHybCount_(needleHybCount),
      ranks_(refHybCount, needleHybCount),
      distMatrices_(refHybCount, NULL)
{
    ranks_.setConstant(value);
}

template<typename T>
FingerprintRankVector<T>::~FingerprintRankVector()
{
}

template<typename T>
const T& FingerprintRankVector<T>::getRanking(int hybIdx, int needleHybIdx) const
{
    assert(hybIdx < haystackHybCount_ && needleHybIdx < needleHybCount_);
    return ranks_(hybIdx, needleHybIdx);
}

template<typename T>
int FingerprintRankVector<T>::setRanks(const T& value)
{
    ranks_.setConstant(value);

    return value;
}

template<typename T>
int FingerprintRankVector<T>::setRanking(int hybIdx, int needleHybIdx, const T& value)
{
    assert(hybIdx < haystackHybCount_ && needleHybIdx < needleHybCount_);
    return ranks_(hybIdx, needleHybIdx) = value;
}

template<typename T>
int FingerprintRankVector<T>::addRanking(int hybIdx, int needleHybIdx, const T& value)
{
    assert(hybIdx < haystackHybCount_ && needleHybIdx < needleHybCount_);
    return ranks_(hybIdx, needleHybIdx) += value;

}

template<typename T>
int FingerprintRankVector<T>::addDistRanking(int hybIdx, int idx, int neightIdx, const T& value)
{
    if (distMatrices_[hybIdx] == 0)
    {
        distMatrices_[hybIdx] = new DistMatrix<T>(needleHybCount_, needleHybCount_);

        distMatrices_[hybIdx]->setConstant(0);

    }

    auto distMat = distMatrices_[hybIdx];

    if ((*distMat)(idx, neightIdx) < value)
    {
        std::cout << "hybIdx" << hybIdx << ": " << idx << "->" << neightIdx << " value: " << value << std::endl;
        (*distMat)(idx, neightIdx) = value;
        return 0;
    }

    return 1;
}

template<typename T>
std::pair<int, const T&> FingerprintRankVector<T>::maxRanking(int hybIdx) const
{
    assert(hybIdx < haystackHybCount_);

    return ranks_.row(hybIdx).maxCoeff();
}

template<typename T>
int FingerprintRankVector<T>::threshold(int hybIdx, int threshold, int value)
{
    for (int i = 0; i < needleHybCount_; ++i)
    {
        if (ranks_(hybIdx, i) < threshold)
        {
            ranks_(hybIdx, i) = value;
        }
    }

    return 0;
}

template<typename T>
int FingerprintRankVector<T>::binarize(int hybIdx, int threshold, int valueBelow, int valueAbove)
{
    for (int i = 0; i < needleHybCount_; ++i)
    {
        ranks_(hybIdx, i) = ranks_(hybIdx, i) < threshold ? valueBelow : valueAbove ;
    }

    return 0;
}

template<typename T>
int FingerprintRankVector<T>::updateRanksFromDistMatrices(int hayIdx)
{
    auto distMat = distMatrices_[hayIdx];
    RankMatrix<int> col(1, needleHybCount_);

    if (distMat != 0)
    {
        std::cout << "matrix n" << hayIdx << std::endl;
        std::cout << *(distMatrices_[hayIdx]) << std::endl;

        col = distMat->rowwise().sum();
        col.transposeInPlace();
        ranks_.row(hayIdx) = col;
        std::cout << "ranks" << std::endl;
        std::cout << ranks_.row(hayIdx) << std::endl;

        delete distMat;

        distMatrices_[hayIdx] = 0;

        return 0;
    }

    return 1;
}

template<typename T>
int FingerprintRankVector<T>::firstReferencePosition() const
{
    //NOT IMPLEMENTED
    return -1;

}

template<typename T>
json FingerprintRankVector<T>::dumpJson() const
{
    json j;
    // NOT implemented
    return j;
}

template<typename T>
double FingerprintRankVector<T>::score() const
{
    return 0; //TODO
}
