#pragma once
#include "candidate-solution.hh"

#include <vector>
#include <json.hpp>

using json = nlohmann::json;
/**
 * @brief class representing a candidat solution for DistOligoSequenceFinder.
 *
 */
class DistOligoSolution:
    public CandidateSolution
{

  public:
    DistOligoSolution();
    DistOligoSolution(int startPosition);
    ~DistOligoSolution();

    void addMatchPair(int ref, int nee);
    void addMatchDist(int ref, int nee);
    //void addDistance(float dist);
    //float distanceSum() const;
    unsigned int size() const;
    virtual double score() const;
    virtual int firstReferencePosition() const;


    json dumpJson() const;

    bool operator<(const DistOligoSolution& rhs);
    bool operator<(const DistOligoSolution& rhs) const;

    int cursor = 0;

    int currentSize = 0;
    float currentScore = 0;
    int startPosition_ = 0;

    // Gap
    int curHayIntervalStart = 0;
    int curHayGapSize = 0;
    int curHayInterval = 0;

    int totalGapSize = 0;
    //int curHayDistStart = -1;
    //int curHayGapSize = 0;
    //int curHayDist = 0;

  private:
    std::vector<std::pair<int, int>> matchPair_;
    std::vector<std::pair<int, int>> matchDist_;
};
