#include "search-match.hh"
#include "../sequence/fingerprint.hh"
#include <cmath>
SearchMatch::SearchMatch()
    : originHaystackPosition_(0),
      lastHaystackPosition_(0),
      originNeedlePosition_(0),
      reversed_(false),
      error_(0.),
      errorRate_(0.),
      nmBaseRate_(1.),
      chiSquare_(0.),
      chiSquareProbability_(1.),
      chromosome_(-1),
      needle_(-1)
{
}

SearchMatch::~SearchMatch()
{
}

double SearchMatch::chiSquare(void) const
{
    return chiSquare_;
}

double SearchMatch::score() const
{
    return chiSquare_;
}

double SearchMatch::errorRate(void) const
{
    return errorRate_;
}

double SearchMatch::error(void) const
{
    return error_;
}

int SearchMatch::firstReferencePosition(void) const
{
    return originHaystackPosition_;
}
int SearchMatch::lastReferencePosition(void) const
{
    return lastHaystackPosition_;
}

int SearchMatch::originNeedlePosition(void) const
{
    return originNeedlePosition_;
}

double SearchMatch::nmBaseRate(void) const
{
    return nmBaseRate_;
}

int SearchMatch::chromosome(void) const
{
    return chromosome_;
}

void SearchMatch::chromosome(int chr)
{
    chromosome_ = chr;
}
bool SearchMatch::reversed(void) const
{
    return reversed_;
}
void SearchMatch::reversed(bool rev)
{
    reversed_ = rev;
}
int SearchMatch::needle(void) const
{
    return needle_;
}

void SearchMatch::needle(int ndl)
{
    needle_ = ndl;
}

bool SearchMatch::operator<(const SearchMatch& sm1) const
{
    return !(this->chromosome_ == sm1.chromosome_ &&
             ((sm1.originHaystackPosition_ >= this->originHaystackPosition_
               && sm1.lastHaystackPosition_ <= this->lastHaystackPosition_) ||
              (sm1.originHaystackPosition_ <= this->originHaystackPosition_
               && sm1.lastHaystackPosition_ >= this->lastHaystackPosition_))) // Check for duplicated solution
           && (
               (this->error_ == sm1.error_) ?
               (this->chiSquareProbability_ == sm1.chiSquareProbability_) ?
               (this->chromosome_ == sm1.chromosome_ ?
                (this->originHaystackPosition_ == sm1.originHaystackPosition_ ?
                 this->nmBaseRate_ < sm1.nmBaseRate_ :
                 this->originHaystackPosition_ < sm1.originHaystackPosition_) :
                this->chromosome_ < sm1.chromosome_) :
               this->chiSquareProbability_ < sm1.chiSquareProbability_ :
               this->error_ < sm1.error_
           );
}
std::ostream& operator<< (std::ostream& ostr, const SearchMatch& searchMatch)
{
    std::string rev;

    if (searchMatch.reversed_)
    {
        rev = "[REVERSED]";
    }
    else
    {
        rev = "          ";
    }

    ostr << "position: " << searchMatch.originHaystackPosition_ << "-" << searchMatch.lastHaystackPosition_
         << " nm->base rate: " << searchMatch.nmBaseRate_ << rev << std::endl
         << "chi square: " << searchMatch.chiSquare_ << "/" << searchMatch.haystackNeedleMatchMap_.size() << std::endl
         << "error count: " << searchMatch.error_ << "]" << std::endl
         << "-------------" << std::endl
         << "error rate: " << searchMatch.errorRate_ << std::endl
         << " chi-Square proba: " << searchMatch.chiSquareProbability_ << std::endl
         //<< "chrId: " << searchMatch.chromosome_
         //<< "first hybridation in hairpin: " << searchMatch.originNeedlePosition_ << std::endl
         << "not found experimental hybridation in reference: " << searchMatch.notFoundInHaystackHyb_.size() << std::endl
         << "not found reference hybridation in hairpin: " << searchMatch.notFoundInNeedleHyb_.size() << std::endl
         //<< "deviation map: " << std::endl
         ;
    //for (auto dev : searchMatch.deviationMap_)
    //{
    //   ostr << dev.first << ":" << dev.second << std::endl;
    //}
    return ostr;
}
const HaystackNeedleMatchMap& SearchMatch::haystackNeedleMatchMap(void) const
{
    return haystackNeedleMatchMap_;
}

json SearchMatch::dumpJson() const
{
    json j;
    j["originNeedleIdx"] = originNeedleIdx_;
    j["originReferenceIdx"] = originReferenceIdx_;
    j["direction"] = direction_ ==  Direction::FORWARD ? "FORWARD" : "BACKWARD";
    j["originHaystackPosition"] = originHaystackPosition_;
    j["lastHaystackPosition"] = lastHaystackPosition_;
    j["originNeedlePosition"] = originNeedlePosition_;
    j["lastNeedlePosition"] = lastNeedlePosition_;
    j["reversed"] = reversed_;
    j["error"] = error_;
    j["errorRate"] = errorRate_;
    j["nmBaseRate"] = nmBaseRate_;
    j["chiSquare"] = chiSquare_;
    j["chiSquareProbability"] = chiSquareProbability_;
    j["chromosome"] = chromosome_;
    j["needle"] = needle_;
    return j;
}
