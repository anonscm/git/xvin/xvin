#include "basic-finder.hh"
#include <json.hpp>

BasicFinder::BasicFinder()

{
}

BasicFinder::~BasicFinder()
{
}

const char * BasicFinder::engine_name = "Basic";

SearchMatch *BasicFinder::find(const DistFingerprint& haystack, DistFingerprint& needle, int start)
{
    //assert(haystack.nbHybridation() > 0);
    //assert(needle.nbHybridation() > 0);
    DistFingerprint *needleRev = needle.reverse();
    assert(needleRev->nbHybridation() > 0);
    SearchMatch *res = findOneStrand(haystack, needle, start);
    SearchMatch *res2 = findOneStrand(haystack, *needleRev, start);

    if (res2 != 0)
        res2->reversed(true);

    if (res == 0 || (res2 != 0 && res2 <= res))
        return res2;

    return res;

}

SearchMatch *BasicFinder::findOneStrand(const DistFingerprint& haystack, const DistFingerprint& needle, int start)
{
    //const int needleHybridationCount = needle.nbHybridation();

    HybridationMap::const_iterator needleItBegin = needle.hybridationMap_.cbegin();
    HybridationMap::const_reverse_iterator needleItRBegin = needle.hybridationMap_.crbegin();
    HybridationMap::const_iterator needleItEnd = needle.hybridationMap_.cend();
    HybridationMap::const_reverse_iterator needleItREnd = needle.hybridationMap_.crend();
    HybridationMap::const_iterator haystackItEnd = haystack.hybridationMap_.cend();
    SearchMatchBuilder curSmb = SearchMatchBuilder(haystack, needle);

    if (!isFingerprintSignificant(needle) || !haystack.isReferenceFingerprint())
    {
        std::cerr << __FILE__ << ":" << __LINE__ << " Fatal error, not significant fingerprint." << std::endl;
        throw std::exception(); //(std::string("trying to search with non significant fingerprint"));
    }

    // Iterate through all haystack hybridation position
    for (HybridationMap::const_iterator haystackIt = haystack.hybridationMap_.lower_bound(start);
            haystackIt != haystackItEnd; ++haystackIt)
    {
        const HybridationPair haystackPair = *haystackIt;
        assert(haystackPair.second.empty() == false);
        ////assert(haystackIt->first < 2952532);
        curSmb.create(haystackPair.first);

        // Iterate through all needle hybidation position
        for (HybridationMap::const_iterator needleIt = needleItBegin;
                isViableSolution(needle, *(curSmb.searchMatch())) &&
                needleIt != needleItEnd;
                ++needleIt)
        {
            const HybridationPair needlePair = *needleIt;

            // Verify that we can determinate a stretching for current configuration
            if (needlePair.second.theoricalOligo() == haystackPair.second.theoricalOligo())
            {
                curSmb.originNeedlePosition(needlePair.first);
                SearchMatchBuilder curRevSmb = SearchMatchBuilder(curSmb);

                for (HybridationMap::const_reverse_iterator revNeedleIt = needleItRBegin;
                        isViableSolution(needle, *(curRevSmb.searchMatch())) &&
                        revNeedleIt != needleItREnd &&
                        needlePair.first < revNeedleIt->first;
                        ++revNeedleIt)
                {
                    int minPosOk = haystackPair.first + needle.stretchedPosition(revNeedleIt->first - needlePair.first -
                                   params_.localSearchPrecision, params_.minNmBaseRate);

                    if (minPosOk < 0)
                        minPosOk = 0;

                    int maxPosOk = haystackPair.first + needle.stretchedPosition(revNeedleIt->first - needlePair.first +
                                   params_.localSearchPrecision, params_.maxNmBaseRate);
                    HybridationMap::const_iterator haystackLastNeedleMatchIt = haystack.hybridationMap_.lower_bound(minPosOk);

                    // Try to strech needle to fit with the haystack
                    for (; haystackLastNeedleMatchIt != haystack.hybridationMap_.end() &&
                            haystackLastNeedleMatchIt->first <= maxPosOk;
                            ++haystackLastNeedleMatchIt)
                    {
                        if (haystackLastNeedleMatchIt->second.theoricalOligo() == revNeedleIt->second.theoricalOligo())
                        {
                            double curNmBaseRate = (haystackLastNeedleMatchIt->first - haystackPair.first) / (double)(
                                                       revNeedleIt->first - needlePair.first);
                            curNmBaseRate = curNmBaseRate < params_.minNmBaseRate ? params_.minNmBaseRate : curNmBaseRate > params_.maxNmBaseRate ? params_.maxNmBaseRate :
                                            curNmBaseRate; // For little needle, in which stretching can't be significant.
                            curRevSmb.nmBaseRate(curNmBaseRate);
                            SearchMatch *match = findStretched(haystack, needle, needleIt, haystackIt, curNmBaseRate, curRevSmb);

                            if (match)
                            {
                                return match;
                            }

                        }
                    }

                    if (!(skipMissingOligoInHaystack(haystack, needlePair.second)))
                    {
                        curRevSmb.addNotFoundInHaystackHyb(needlePair.first, needlePair.second);
                    }

                }

                curRevSmb.abort();
            }

            if (!(skipMissingOligoInHaystack(haystack, needlePair.second)))
            {
                curSmb.addNotFoundInHaystackHyb(needlePair.first, needlePair.second);
            }
        }

        curSmb.abort();
    }

    return 0;
}

SearchMatch *BasicFinder::findStretched(const DistFingerprint& haystack,
        const DistFingerprint& needle,
        HybridationMap::const_iterator& originNeedleHybIt,
        HybridationMap::const_iterator& originHaystackHybIt,
        double nmBaseRate,
        SearchMatchBuilder smb)
{
    assert(nmBaseRate > 0);
    HybridationMap::const_iterator needleIt = originNeedleHybIt;
    const int originNeedleHybPos = originNeedleHybIt->first;
    const int originHaystackHybPos = originHaystackHybIt->first;
    const HybridationMap::const_iterator needleEndIt = needle.hybridationMap_.end();
    const HybridationMap::const_iterator haystackReachableEndIt = haystack.hybridationMap_.upper_bound(
                originHaystackHybPos + needle.stretchedPosition(needle.hybridationMap_.crbegin()->first - originNeedleHybPos +
                        params_.localSearchPrecision, nmBaseRate));

    // iterate throw needle to verify
    for (; needleIt != needleEndIt && isViableSolution(needle, *(smb.searchMatch())); ++needleIt)
    {
        const HybridationMap::value_type& needlePair = (*needleIt);
        bool match = false;
        int minSearchPos = originHaystackHybPos + needle.stretchedPosition(needlePair.first - originNeedleHybPos -
                           params_.localSearchPrecision, nmBaseRate);
        int maxSearchPos = originHaystackHybPos + needle.stretchedPosition(needlePair.first - originNeedleHybPos +
                           params_.localSearchPrecision, nmBaseRate);
        HybridationMap::const_iterator haystackIt = haystack.hybridationMap_.lower_bound(minSearchPos);

        for (; haystackIt != haystackReachableEndIt && haystackIt->first <= maxSearchPos;
                ++haystackIt)
        {
            const HybridationMap::value_type& haystackPair = (*haystackIt);

            if ((haystackPair.second.theoricalOligo() == needlePair.second.theoricalOligo()))
            {
                match = true;
                smb.addHaystackNeedleMatch(haystackPair.first, needlePair.first);
            }
        }

        // if there is a hybritation on needle that is not in haystack, we add an error
        if (!match && !(skipMissingOligoInHaystack(haystack, needlePair.second)))
        {
            assert(needlePair.second.probability() > 0);
            smb.addNotFoundInHaystackHyb(needlePair.first, needlePair.second);
        }
    }

    const HaystackNeedleMatchMap& hnm = smb.searchMatch()->haystackNeedleMatchMap();
    HaystackNeedleMatchMap::const_iterator hnmIt = hnm.begin();
    HybridationMap::const_iterator haystackIt = haystack.hybridationMap_.lower_bound(hnmIt->first);
    HybridationMap::const_iterator haystackLastFoundIt = haystack.hybridationMap_.upper_bound(
                smb.searchMatch()->lastReferencePosition());

    while (haystackIt != haystackLastFoundIt && hnmIt != hnm.end() && isViableSolution(needle, *(smb.searchMatch())))
    {
        if (hnmIt->first > haystackIt->first)
        {
            if (!skipMissingOligoInNeedle(needle, haystackIt->second))
            {
                assert(haystackIt->second.probability() > 0);
                smb.addNotFoundInNeedleHyb(haystackIt->first, haystackIt->second);
            }

            ++haystackIt;
        }
        else
        {
            assert(!(hnmIt->first < haystackIt->first));
            ++hnmIt;
            ++haystackIt;
        }
    }

    if (isViableSolution(needle, *(smb.searchMatch())))
    {
        return smb.searchMatch();
    }
    else
    {
        smb.abort();
        return 0;
    }
}


BasicSearchResult BasicFinder::findAll(const DistFingerprint& haystack, const DistFingerprint& needle)
{
    SearchMatch *res = 0;
    DistFingerprint *revNeedle = needle.reverse();
    BasicSearchResult matchSet;
    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
    start = std::chrono::high_resolution_clock::now();

    int curPos = -1;


    do
    {
        delete res;
        res = findOneStrand(haystack, needle, curPos + 1);

        if (res != 0)
        {
            matchSet.solutions().push_back(*res);
            curPos = res->firstReferencePosition();
        }
    }
    while (res != 0);

    curPos = -1;
    res = 0;

    do
    {
        delete res;
        res = findOneStrand(haystack, *revNeedle, curPos + 1);

        if (res != 0)
        {
            res->reversed(true);
            matchSet.solutions().push_back(*res);
            curPos = res->firstReferencePosition();
        }
    }
    while (res != 0);

    std::sort(matchSet.solutions().begin(), matchSet.solutions().end());
    end = std::chrono::high_resolution_clock::now();
    matchSet.duration(end - start);


    return matchSet;
}

BasicSearchResult BasicFinder::findAllByOligo(const Fingerprint& haystack, Fingerprint& needle)
{
    SearchMatch *res = 0;
    const DistFingerprint& distHaystack = dynamic_cast<const DistFingerprint&>(haystack);
    DistFingerprint& distNeedle = dynamic_cast<DistFingerprint&>(needle);
    DistFingerprint *revNeedle = 0;
    BasicSearchResult matchSet;
    std::cout << "find oligo one by one" << std::endl;

    for (auto oligo : distNeedle.oligos_)
    {
        OligoSet subset;
        subset.insert(oligo);
        DistFingerprint partNeedle = DistFingerprint(distNeedle, subset);

        if (isFingerprintSignificant(partNeedle))
        {
            int curPos = -1;

            do
            {
                delete res;
                res = findOneStrand(distHaystack, partNeedle, curPos + 1);

                if (res != 0)
                {
                    matchSet.solutions().push_back(*res);
                    curPos = res->firstReferencePosition();
                }
            }
            while (res != 0);

            revNeedle = partNeedle.reverse();
            curPos = -1;
            res = 0;

            do
            {
                delete res;
                res = findOneStrand(distHaystack, *revNeedle, curPos + 1);

                if (res != 0)
                {
                    res->reversed(true);
                    matchSet.solutions().push_back(*res);
                    curPos = res->firstReferencePosition();
                }
            }
            while (res != 0);
        }
    }

    std::sort(matchSet.solutions().begin(), matchSet.solutions().end());

    return matchSet;
}

const BasicSearchParams& BasicFinder::params() const
{
    return params_;
}

void BasicFinder::params(const BasicSearchParams& params)
{
params_ = params;
}

