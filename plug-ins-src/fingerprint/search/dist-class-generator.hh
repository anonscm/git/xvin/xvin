#pragma once
# include "../define.hh"
#include "../sequence/fingerprint.hh"

/**
  * @brief This class take the fingerprint of reference genome, count the distance between each
  * hybridization, and try to cluster similar distance, making class of distances.
  *
  * This cluster generator generate DistClassVector which are used in DistOligoSequenceFinder 
 */

class DistClassGenerator
{
  public:

      /**
       * @brief This class take the fingerprint of reference genome, count the distance between each
       * hybridization, and try to cluster similar distance, making class of distances.
       *
       * @param fgp Reference genom fingerprint
       * @param maxFractionInClass maximum size of a «similar distance» cluster, in portion of the
       * total different distance found in the reference genome.
       * @param minBaseNmRate minimum stretching of a experimental hairpin use with the
       * DistClassVector outputed with computeDistClasses
       * @param maxBaseNmRate maximum stretching
       * @param classWidthMultiplifier
       * @param maxDistance
       */
    DistClassGenerator(Fingerprint& fgp, float maxFractionInClass, float minBaseNmRate, float maxBaseNmRate,
                       float classWidthMultiplifier, int maxDistance);
    virtual ~DistClassGenerator();

    /**
     * @brief  compute distance cluster from the reference genome distance between hybridization distribution
     *
     * @param dists
     *
     * @return 
     */
    DistClasses *computeDistClasses(std::vector<int> *dists);

    /**
     * @brief Compute the distribution of distances between hybridizations
     *
     * @return 
     */
    std::vector<int> *computeDistDistribution();

    int maxFractionInClass(void) const;
    void maxFractionInClass(int maxFractionInClass);

    int maxDistance(void) const;
    void maxDistance(int maxDistance);



  private:
    Fingerprint& ref_;
    float maxFractionInClass_;
    float minBaseNmRate_;
    float maxBaseNmRate_;
    float classWidthMultiplifier_;
    int maxDistance_;
};
