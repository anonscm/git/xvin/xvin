#pragma once
#include "abstract-search.hh"
#include "../sequence/fingerprint.hh"
#include "../sequence/pos-fingerprint.hh"
#include "../define.hh"
#include "../results/search-result.hh"
#include "../params/chisquare-search-params.hh"
#include <json.hpp>
#include <vector>
#include <map>

// FIXME use a param class
#include "../view/benchmark-config-view.hh"

namespace chiSquareMethod
{

class ChiSquareSearch : public AbstractSearch
{
  public:
    friend benchmark::BenchmarkConfigView<ChiSquareSearch>;
    ChiSquareSearch();
    virtual ~ChiSquareSearch();

    typedef PosFingerprint input_type;
    typedef ChiSquareCandidateSolution result_type;
    typedef ChiSquareSearchResult results_type;
    typedef ChiSquareSearchParams params_type;
    const static char * engine_name;

    /**
     * @brief Search a experimental fingerprint into a reference genom fingerprint
     *
     * @param ref reference genom
     * @param needle experimental data
     *
     * @return a ordered set of canditate solutions
     */

    bool rejectSolution(result_type& cs);
    bool acceptSolution(result_type& cs);
    bool rejectNonOriginSolution(result_type& cs);
    bool acceptAllSolution(result_type& cs);
    bool rejectNone(ChiSquareCandidateSolution& cs);
    static bool gammaSortOperator(ChiSquareCandidateSolution& a, ChiSquareCandidateSolution& b);
    static bool pivotSortOperator(ChiSquareCandidateSolution& a, ChiSquareCandidateSolution& b);

    //std::vector<ChiSquareCandidateSolution> search(const PosFingerprint& ref, const PosFingerprint& needle);
    results_type search(std::shared_ptr<const input_type> refce,
                        std::shared_ptr<const input_type> expt,
                        decltype(&ChiSquareSearch::rejectSolution) rejectFunc = &ChiSquareSearch::rejectSolution,
                        decltype(&ChiSquareSearch::rejectSolution) acceptFunc =  &ChiSquareSearch::acceptSolution,
                        decltype(&ChiSquareSearch::gammaSortOperator) sortOperator = &ChiSquareSearch::gammaSortOperator
                        );

    results_type search(FingerprintVector& refces,
                        std::shared_ptr<const input_type> expt,
                        decltype(&ChiSquareSearch::rejectSolution) rejectFunc = &ChiSquareSearch::rejectSolution,
                        decltype(&ChiSquareSearch::rejectSolution) acceptFunc =  &ChiSquareSearch::acceptSolution,
                        decltype(&ChiSquareSearch::gammaSortOperator) sortOperator = &ChiSquareSearch::gammaSortOperator
                        );

    //ChiSquareCandidateSolution searchAtPos(const PosFingerprint& ref, const PosFingerprint& needle, const int refPos);

    // Params settings

    const NmBaseRateDistribution& nmBaseRateDistribution() const;
    void nmBaseRateDistribution(NmBaseRateDistribution nmbrDist);
    void minUmDegreeOfFreedom(int minumdof);
    int minUmDegreeOfFreedom() const;
    void minFoundRate(double mfr);
    double minFoundRate() const;

    double gammaSignificanceLevel() const;
    void gammaSignificanceLevel(double gsl);

    double expectedNoiseDeviation() const;
    void expectedNoiseDeviation(double exnde);

    double stretchingConfidenceLevel() const;
    void stretchingConfidenceLevel(double scl);

    double noiseConfidenceLevel() const;
    void noiseConfidenceLevel(double ncl);

    const ChiSquareSearchParams& params() const;
    void params(const ChiSquareSearchParams& params);

  private:

    ChiSquareSearchParams params_ = ChiSquareSearchParams();
    /**
     * @brief the minimum possible stretching of DNA
     */
    double minNmBaseRate__;
    /**
     * @brief the maximum allowed stretching of DNA
     */
    double maxNmBaseRate__;

    double lowerNoiseInterv__ = 0;
    double upperNoiseInterv__ = 0;

    std::shared_ptr<const PosFingerprint> expt_ = 0;

    /**
     * @brief Choose the best needle experimental peak to be the reference hybridization peak for
     * search.
     *
     * Current implementation just take the nearest hybridization from the center of the needle
     * @param needle
     *
     * @return the position of the reference peak choosen
     */
    int chooseRefHybridization(const PosFingerprint& needle);

    /**
     * @brief Selects all matchs between reference at a position and needle in a certain stretching range taking the stretching reference as needlePivot
     *
     * @param refce
     * @param needle
     * @param refPosIdx
     * @param needlePivotIdx
     *
     * @return
     */
    MatchingHybVector getHybridizationMatch(const PosFingerprint& refce, const PosFingerprint& needle, int refPosIdx,
                                            int needlePivotIdx, double fixStretching = -1);

    
    /**
     * @brief Compute the linear regression for all the point how have a univoque match between
     * reference and exp.
     * NO NOISE HANDLING
     *
     * @param matchs
     *
     I* @return
     */
    LinearParams linRegression(const PosFingerprint& needle,
                               const MatchingPairVector& matchs);

    /**
     * @brief Compute chi square of linear fit over data
     * @param needle
     * @param matchs
     * @param lin
     *
     * @return
     */
    double chiSquare(const PosFingerprint& needle, const MatchingPairVector& matchs,
                     const LinearParams& lin);

    /**
     * @brief
     *
     * @param needle
     * @param matchs
     * @param lin
     *
     * @return
     */
    MatchingPairVector bestMatches(const PosFingerprint& needle,
                                   const MatchingHybVector& matchs,
                                   const LinearParams& lin);
    /**
     * @brief Count match (on an)
     *
     * @param matchs
     *
     * @return
     */
    int matchCount(const MatchingHybVector& match, auto cond);


    /**
     * @brief Get the vector of matching hybs
     *
     * @param matchs
     *
     * @return A vecor of pair <needle pos, ref pos>
     */
    MatchingPairVector getUnivoqueMatchingHybs(const MatchingHybVector& matchs);

    double criticalValue(int degreeOfFreedom);

    std::pair<int, double> bestMatch(const PosFingerprint& needle, const MatchingHybVector& matchs, const LinearParams& lin,
                                     int posNeedleIdx, std::map<int, int>& choosenMatch,
                                     std::map<int, std::pair<int, double>>& closedList);

};

}

#include "chisquare-search.hxx"
