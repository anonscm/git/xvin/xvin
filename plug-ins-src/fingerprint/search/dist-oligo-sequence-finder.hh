#pragma once
#include "../define.hh"
#include "../sequence/dist-oligo-sequence.hh"
#include "../results/dist-oligo-solution.hh"
#include "../results/search-result.hh"
#include "abstract-search.hh"
#include "../params/dist-oligo-search-params.hh"
#include <json.hpp>
// FIXME use a param class
#include "../view/benchmark-config-view.hh"
#include<queue>

using json = nlohmann::json;
/**
 * @brief A search algorithm allowing to map a DNA fragment (needle)
 * in a reference genome (haystack) -> Find the origin fragment.
 *
 * This algorithm use fingerprint represented as a succession of distance between hybridizations.
 * Those distance are repesented by the cluster of distance they belongs to (DistClassVector)
 *
 *  Current status : Implemented / Not fully tested.
 *
 */

class DistOligoSequenceFinder:
    public AbstractSearch
{
    friend benchmark::BenchmarkConfigView<DistOligoSequenceFinder>;
  public:

    typedef DistOligoSolution result_type;
    typedef DistSearchResult results_type;
    typedef DistOligoSequence input_type;
    typedef DistOligoSearchParams params_type;
    const static char * engine_name;

    DistOligoSequenceFinder();
    DistOligoSequenceFinder( const DistOligoSearchParams& params);
    ~DistOligoSequenceFinder();

    results_type find(const DistOligoSequence& haystack, const DistOligoSequence& needle);

    const DistOligoSearchParams& params() const;
    void params(const DistOligoSearchParams& params);
    const DistClasses& distClasses() const;
    void distClasses(DistClasses& distClasses_);
  private:
    //std::vector<int> distClassCenter_;
    DistOligoSearchParams params_;

    bool reject(const DistOligoSequence& needle, const DistOligoSolution& dos);
    bool accept(const DistOligoSequence& needle, const DistOligoSolution& dos);
};
