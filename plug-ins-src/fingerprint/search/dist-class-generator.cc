#include "dist-class-generator.hh"

DistClassGenerator::DistClassGenerator(Fingerprint& fgp, float maxFractionInClass, float minBaseNmRate,
                                       float maxBaseNmRate, float classWidthMultiplifier, int maxDistance)
    :
    ref_(fgp),
    maxFractionInClass_(maxFractionInClass),
    minBaseNmRate_(minBaseNmRate),
    maxBaseNmRate_(maxBaseNmRate),
    classWidthMultiplifier_(classWidthMultiplifier),
    maxDistance_(maxDistance)
{
}

DistClassGenerator::~DistClassGenerator()
{
}

std::vector<int> *DistClassGenerator::computeDistDistribution()
{
    std::vector<int> *dists = new std::vector<int> (maxDistance_, 0);
    int prevPos = ref_.firstHyb();

    for (int i = 0; i < ref_.nbHybridation() - 1; ++i)
    {
        int curPos = ref_.nextHybridizationPos(prevPos);
        int dist = curPos - prevPos;

        if (dist < maxDistance_)
        {
            (*dists)[dist]++;
        }
        else
        {
            (*dists)[maxDistance_ - 1]++;
        }

        prevPos = curPos;
    }

    return dists;
}

DistClasses *DistClassGenerator::computeDistClasses(std::vector<int> *dists = nullptr)
{
    DistClasses *distClasses = new DistClasses();
    DistClass currentDistClass = DistClass(0, 0);
    int maxClassCount = ref_.nbHybridation() * maxFractionInClass_;
    int curClassCount = 0;
    std::vector<int> *idists = dists;

    /*
     * Classer les distances entre tout les pics d'un genome
     * Selectionner les classes ?
     *
     */

    if (idists == nullptr)
    {
        idists = computeDistDistribution();
    }

    for (int i = 0; i < maxDistance_; ++i)
    {
        curClassCount += (*idists)[i];
        int minClassWidth = std::max(
                                     std::abs(i - i * minBaseNmRate_) * classWidthMultiplifier_, 
                                     std::abs(i - i * maxBaseNmRate_) * classWidthMultiplifier_); 

        int classWidth = i - currentDistClass.first;
        //std::cout << "min class width: " << minClassWidth << ", classWidth: " << classWidth << std::endl;

        if (curClassCount >= maxClassCount || (distClasses->range.size() > 0 && classWidth >= minClassWidth))
        {
            currentDistClass.second = i;
            distClasses->range.push_back(currentDistClass);
            currentDistClass.first = i + 1;
            curClassCount = 0;
        }
    }

    if ((distClasses->range)[distClasses->range.size() - 1].second != maxDistance_)
    {
            currentDistClass.second = maxDistance_ + 1;
            distClasses->range.push_back(currentDistClass);
    }
    
    distClasses->map.resize(maxDistance_ + 1);
    for(uint i = 0; i < distClasses->range.size();++i)
    {
        auto curRange = distClasses->range[i];
        for(int j = curRange.first; j <= curRange.second; ++j)
        {
            distClasses->map[j] = i;
        }
    
    }


//#ifndef NDEBUG
//    std::cout << "Distance classes" << std::endl;
//
//    for (auto distclass : *distClasses)
//    {
//        std::cout << distclass.first << "--" << distclass.second << std::endl;
//    }
//
//#endif

    if (dists == nullptr)
    {
        delete idists;
    }

    distClasses->maxDistance = maxDistance_;
    distClasses->maxFractionInClass = maxFractionInClass_;
    distClasses->minBaseNmRate = minBaseNmRate_;
    distClasses->maxBaseNmRate = maxBaseNmRate_;
    distClasses->classWidthMultiplifier = classWidthMultiplifier_;
    return distClasses;
}
int DistClassGenerator::maxFractionInClass(void) const
{
    return maxFractionInClass_;
}

int DistClassGenerator::maxDistance(void) const
{
    return maxDistance_;
}

void DistClassGenerator::maxFractionInClass(int maxFractionInClass)
{
    if (maxFractionInClass > 0)
        maxFractionInClass_ = maxFractionInClass;
}

void DistClassGenerator::maxDistance(int maxDistance)
{
    if (maxDistance > 0)
        maxDistance_ = maxDistance;
}

