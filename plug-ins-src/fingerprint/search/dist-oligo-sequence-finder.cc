#include "dist-oligo-sequence-finder.hh"
#include<cmath>
#include <vector>

#define RECORD_MATCH_PAIR

const char *DistOligoSequenceFinder::engine_name = "DistOligoSequence";

DistOligoSequenceFinder::DistOligoSequenceFinder()
{
}
DistOligoSequenceFinder::DistOligoSequenceFinder(const DistOligoSearchParams& params_)
{
}

DistOligoSequenceFinder::~DistOligoSequenceFinder() {}

DistOligoSequenceFinder::results_type DistOligoSequenceFinder::find(
    const DistOligoSequence& haystack, const DistOligoSequence& needle)
{
    assert(params_.scoreMatrix.size() > 0);
    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
    start = std::chrono::high_resolution_clock::now();
    auto results = DistSearchResult();
    auto& acceptedSolutions = results.solutions();
    auto searchOneStrand = [this, &haystack, &acceptedSolutions](const DistOligoSequence & needle, bool needleForward)
    {
        std::vector<DistOligoSolution> ps1;
        std::vector<DistOligoSolution> ps2;
        std::vector<DistOligoSolution>& pendingSolutions = ps1;
        std::vector<DistOligoSolution>& nextPendingSolutions = ps2;
        std::vector<DistOligoSolution>& swapps = ps1;

        for (uint i = 0; i < haystack.oligoSeqSize() - 1; ++i)
        {
            swapps = pendingSolutions;
            pendingSolutions = nextPendingSolutions;
            nextPendingSolutions = swapps;
            nextPendingSolutions.clear();
            DistOligoSolution pdos = DistOligoSolution(haystack.position(i));

            if (needleForward)
            {
                pdos.direction(DistOligoSolution::Direction::FORWARD);
            }
            else
            {
                pdos.direction(DistOligoSolution::Direction::BACKWARD);
            }

            pdos.curHayInterval =  abs(haystack.position(i) -  haystack.position(i + 1));
            pendingSolutions.push_back(pdos);

            for (uint j = 0; j < pendingSolutions.size(); ++j)
            {
                // Close gap
                DistOligoSolution& dos = pendingSolutions[j];

                if (!reject(needle, dos))
                {
                    // Continue gap
                    int dist = abs(haystack.position(i + 1) -  haystack.position(i + 2));

                    if (params_.allowGap && haystack.distClasses_.map[dos.curHayInterval] <= needle[dos.cursor].value)
                    {
                        DistOligoSolution dosm = dos;
                        dosm.curHayInterval += dist;
                        dosm.curHayGapSize++;
                        dosm.currentScore += params_.gapCost;
                        nextPendingSolutions.push_back(dosm);
                    }

#ifdef RECORD_MATCH_PAIR
                    dos.addMatchPair(haystack.distClasses_.map[dos.curHayInterval], needle[dos.cursor].value);
                    int ndist = needle.position(dos.cursor + 1) - needle.position(dos.cursor);
                    dos.addMatchDist(dos.curHayInterval, ndist);
#endif
                    float mscore = params_.scoreMatrix(haystack.distClasses_.map[dos.curHayInterval], needle[dos.cursor].value);
                    dos.currentScore += mscore;
                    dos.currentSize += 1;
                    dos.cursor++;
                    dos.curHayInterval = dist;
                    dos.curHayIntervalStart = i;
                    dos.totalGapSize += dos.curHayGapSize;
                    dos.curHayGapSize = 0;

                    if (accept(needle, dos))
                    {
                        acceptedSolutions.push_back(dos);
                        std::push_heap(acceptedSolutions.begin(), acceptedSolutions.end());
                    }
                    else
                    {
                        nextPendingSolutions.push_back(dos);
                    }
                }
            }
        }
    };

    if (params_.dirForward)
    {
        //printf("forward: %i\n", needle.nbHybridation());
        //std::cout << needle.dumpJson(true) << std::endl;
        searchOneStrand(needle, true);
    }

    if (params_.dirBackward)
    {
        auto revNeedle = needle.reverse();
        //printf("backward: %i\n", revNeedle->nbHybridation());
        //std::cout << revNeedle->dumpJson(true) << std::endl;
        searchOneStrand(*revNeedle, false);
    }

    std::sort(results.solutions_.begin(), results.solutions_.end(), [](auto a, auto b)
    {
        return a.score() / a.size()  > b.score() / b.size();
    });
    end = std::chrono::high_resolution_clock::now();
    results.duration(end - start);
    return results;
}

// TODO put in DistOligoSearchParams

bool DistOligoSequenceFinder::reject(const DistOligoSequence& needle, const DistOligoSolution& dos)
{
    return ((uint) dos.currentSize >= needle.oligoSeqSize()) ||
           ((dos.curHayGapSize > params_.maxGapSize) ||
            (dos.totalGapSize + dos.curHayGapSize) > params_.globalMaxGapSize  ||
            dos.curHayInterval > params_.distClasses.maxDistance
            || (dos.currentSize > 3
                && dos.currentScore / dos.currentSize < params_.rejectThreshold)
           );
}

bool DistOligoSequenceFinder::accept(const DistOligoSequence& needle, const DistOligoSolution& dos)
{
    return ((uint) dos.currentSize >= needle.oligoSeqSize() && dos.currentScore / dos.size() > params_.acceptThreshold);
}

const DistClasses& DistOligoSequenceFinder::distClasses() const
{
    return params_.distClasses;
}

void DistOligoSequenceFinder::distClasses(DistClasses& distClasses)
{
    params_.distClasses = distClasses;
    //if (params_.scoreMatrix == 0)
    //{
    //    buildDefaultScoreMatrix();
    //    //build2DExpScoreMatrix();
    //    sumMatrix_ = DistClassSumMatrix<char>(distClasses->size(), distClasses->size());
    //    buildSumMatrix();
    //}
}


void DistOligoSequenceFinder::params(const DistOligoSearchParams& params)
{
    params_ = params;
}

const DistOligoSearchParams& DistOligoSequenceFinder::params() const
{
    return params_;
}
