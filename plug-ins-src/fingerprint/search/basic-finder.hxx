#pragma once
#include <gsl/gsl_randist.h>

inline bool BasicFinder::skipMissingOligoInHaystack(const Fingerprint& haystack, const Hybridation& needleOligo)
{
    bool res = params_.skipMissingOligoInHaystack && haystack.oligos_.find(needleOligo.theoricalOligo()) == haystack.oligos_.end();
    //std::cout << "hay: " << res << std::endl;
    return res;
}

inline bool BasicFinder::skipMissingOligoInNeedle(const Fingerprint& needle, const Hybridation& haystackOligoBF)
{
    Oligo haystackOligo = haystackOligoBF.theoricalOligo();
    bool res = params_.skipMissingOligoInNeedle && needle.oligos_.find(haystackOligo) == needle.oligos_.cend() && needle.outcastOligos_.find(haystackOligo) == needle.outcastOligos_.cend();
    //std::cout << "needle: " << res << std::endl;
    return res;
}

inline bool BasicFinder::skipMatch(Fingerprint& haystack, const Hybridation& hOligo, Fingerprint& needle, const Hybridation& nOligo)
{
    return skipMissingOligoInNeedle(needle, hOligo) || skipMissingOligoInHaystack(haystack, nOligo);
}

inline double BasicFinder::maxNmBaseRate() const
{
    return params_.maxNmBaseRate;
}

inline double BasicFinder::minNmBaseRate() const
{
    return params_.minNmBaseRate;
}


inline void BasicFinder::maxNmBaseRate(double maxSR)
{
    params_.maxNmBaseRate = maxSR;
}

inline void BasicFinder::minNmBaseRate(double minSR)
{
    params_.minNmBaseRate = minSR;
}

inline void BasicFinder::stretchingRates(double minSR, double maxSR)
{
    params_.minNmBaseRate = minSR;
    params_.maxNmBaseRate = maxSR;
}
inline bool BasicFinder::isViableSolution(const
                                                Fingerprint& /*needle*/,
                                                SearchMatch& searchMatch) const
{
    return searchMatch.errorRate() <=  params_.maxSearchHybridationError /*&& chiSquareProbability_ >= 0.01*/;
}

inline double BasicFinder::noiseDeviation() const
{
    return params_.noiseDeviation;
}

inline void BasicFinder::noiseDeviation(double noiseDeviation)
{
    params_.noiseDeviation = noiseDeviation;
    params_.localSearchPrecision = noiseDeviation * 3;
}

inline double BasicFinder::maxSearchHybridationError() const
{
    return params_.maxSearchHybridationError;
}

inline void BasicFinder::maxSearchHybridationError(double maxHER)
{
    params_.maxSearchHybridationError = maxHER + std::numeric_limits<double>::epsilon();
}
inline bool BasicFinder::skipMissingOligoInHaystack() const
{
    return params_.skipMissingOligoInHaystack;
}

inline bool BasicFinder::skipMissingOligoInNeedle() const
{
    return params_.skipMissingOligoInNeedle;
}

inline void BasicFinder::skipMissingOligoInHaystack(bool skipMissing)
{
    params_.skipMissingOligoInHaystack = skipMissing;
}

inline void BasicFinder::skipMissingOligoInNeedle(bool skipMissing)
{
    params_.skipMissingOligoInNeedle = skipMissing;
}

inline bool BasicFinder::isFingerprintSignificant(const Fingerprint& fgp) const
{
    if (fgp.nbHybridation() < 3)
    {
        return false;
    }

    return true;
}
