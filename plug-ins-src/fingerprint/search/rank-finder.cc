#include "rank-finder.hh"
#include "../results/fingerprint-rank-vector.hh"

RankFinder::RankFinder()
    : maxSearchHybridationError_(MAX_HYBRIDATION_ERROR),
      noiseDeviation_(NOISE_DEVIATION),
      localSearchPrecision_(NOISE_DEVIATION * 3),
      minNmBaseRate_(MIN_NM_BASE_RATE),
      maxNmBaseRate_(MAX_NM_BASE_RATE),
      minCorrelatedPeak_(MIN_CORRELATED_PEAK),
      rankingIteration_(RANKING_ITERATION)
{

}

RankFinder::~RankFinder()
{
}


SearchMatch *RankFinder::buildRankVector(const PosFingerprint& haystack,
        const PosFingerprint& needle)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    //d_s *peak_idx_ds = NULL;
    //d_s *rev_peak_idx_ds = NULL;
    //d_s *rev_ds = NULL;
    FingerprintRankVector<int> *prevRankVector = new FingerprintRankVector<int>(haystack.positions_.size(),
            needle.positions_.size(), 1);
    FingerprintRankVector<int> *prevReverseRankVector = new FingerprintRankVector<int>(haystack.positions_.size(),
            needle.positions_.size(), 1);
    FingerprintRankVector<int> *curRankVector = new         FingerprintRankVector<int>(haystack.positions_.size(),
            needle.positions_.size(), 0);
    FingerprintRankVector<int> *curReverseRankVector = new  FingerprintRankVector<int>(haystack.positions_.size(),
            needle.positions_.size(), 0);
    FingerprintRankVector<int> *tmpVector = NULL;

    std::cout << "min : " << minNmBaseRate_ << std::endl;
    std::cout << "max : " << maxNmBaseRate_ << std::endl;
    int hayHybCount = haystack.positions_.size();
    int needleHybCount = needle.positions_.size();

    //const int needleHybridationCount = needle.nbHybridation();
    PosFingerprint *revNeedle = needle.reverse();

    //SearchMatchBuilder curSmb = SearchMatchBuilder(haystack, needle);
    OligoPosDistMap *oligoPosDistMap = needle.oligoPosDistMap(minNmBaseRate_, maxNmBaseRate_, noiseDeviation_);
    OligoPosDistMap *revOligoPosDistMap = revNeedle->oligoPosDistMap(minNmBaseRate_, maxNmBaseRate_, noiseDeviation_);

#ifndef NDEBUG

    for (OligoPosDistMap::iterator it = (*oligoPosDistMap).begin(); it != (*oligoPosDistMap).end(); it++)
    {
        DistIntervalSet dis = (*it).second;

        std::cout << "Map: " << (*it).first.first << ", " << (*it).first.second  << std::endl;

        for (DistIntervalSet::iterator dit = dis.begin(); dit != dis.end(); ++dit)
        {
            std::cout << "interv: " << (*dit).first << ": ";
            std::set<std::pair<int, int>>& peakSet = (*dit).second;

            for (auto setIt = peakSet.begin(); setIt != peakSet.end(); ++setIt)
            {
                std::cout << "(" << (*setIt).first << ", " << (*setIt).second << ");";
            }

            std::cout << std::endl;
        }
    }

#endif


    if (!isFingerprintSignificant(needle) || !haystack.isReferenceFingerprint())
    {
        std::cerr << __FILE__ << ":" << __LINE__ << " Fatal error, not significant fingerprint." << std::endl;
        throw std::exception(); //(std::string("trying to search with non significant fingerprint"));
    }

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1) // we get the plot region
        return (SearchMatch *) win_printf_ptr("cannot find data");

    if ((op = create_and_attach_one_plot(pr, hayHybCount, hayHybCount , 0)) == NULL)
        return (SearchMatch *) win_printf_ptr("cannot create plot!");

    for (int iteration = 0; iteration < rankingIteration_; ++iteration)
    {
        // Iterate through all haystack hybridation position
        for (int hayIdx = 0; hayIdx < hayHybCount; ++hayIdx)
        {
            int hybPos = haystack.positions_[hayIdx];
            const Hybridation& hyb = haystack.hybridations_[hayIdx];

            // assert hybridation is correctly set !
            assert(hyb.empty() == false);

            ////assert(haystackIt->first < 2952532);
            
            // we start preparing searchMatch
            //curSmb.create(hayIdx);

            // Iterate through all needle hybidation position
            for (int nextHayIdx = hayIdx + 1; nextHayIdx < hayHybCount; ++nextHayIdx)
            {
                int nextHybPos = haystack.positions_[nextHayIdx];

                if (nextHybPos > hybPos + needle.underlyingSequenceSize_)
                {
                    break;
                }

                const Hybridation& nextHyb = haystack.hybridations_[nextHayIdx];

                // getting range compatible with
                OligoPair olp = OligoPair(hyb.theoricalOligo(), nextHyb.theoricalOligo());

                DistIntervalSet distIntervalSet = (*oligoPosDistMap)[olp];
                distIntervalSet =  distIntervalSet & (nextHybPos - hybPos);

                DistIntervalSet revDistIntervalSet = (*revOligoPosDistMap)[olp];
                revDistIntervalSet = revDistIntervalSet & (nextHybPos - hybPos);

                for (DistIntervalSet::iterator it = distIntervalSet.begin(); it != distIntervalSet.end(); ++it)
                {
                    std::set<std::pair<int, int>>& peakSet = (*it).second;

                    for (auto setIt = peakSet.begin(); setIt != peakSet.end(); ++setIt)
                    {
                        (*curRankVector).addDistRanking(hayIdx, (*setIt).first, (*setIt).second, (*prevRankVector).getRanking(nextHayIdx,
                                                        (*setIt).second));
                        (*curRankVector).addDistRanking(nextHayIdx, (*setIt).second, (*setIt).first, (*prevRankVector).getRanking(hayIdx,
                                                        (*setIt).first));
                        //(*curRankVector).addRanking(hayIdx, (*setIt).first, (*prevRankVector).getRanking(nextHayIdx, (*setIt).second));
                        //(*curRankVector).addRanking(nextHayIdx, (*setIt).second, (*prevRankVector).getRanking(hayIdx, (*setIt).first));
                    }
                }

                for (DistIntervalSet::iterator it = distIntervalSet.begin(); it != distIntervalSet.end(); ++it)
                {
                    std::set<std::pair<int, int>>& peakSet = (*it).second;

                    for (auto setIt = peakSet.begin(); setIt != peakSet.end(); ++setIt)
                    {
                        (*curReverseRankVector).addDistRanking(hayIdx, (*setIt).first, (*setIt).second,
                                                               (*prevReverseRankVector).getRanking(nextHayIdx,
                                                                       (*setIt).second));
                        (*curReverseRankVector).addDistRanking(nextHayIdx, (*setIt).second, (*setIt).first,
                                                               (*prevReverseRankVector).getRanking(hayIdx,
                                                                       (*setIt).first));
                        //(*curReverseRankVector).addRanking(hayIdx, (*setIt).first, (*prevReverseRankVector).getRanking(nextHayIdx,
                        //(*setIt).second));
                        //(*curReverseRankVector).addRanking(nextHayIdx, (*setIt).second, (*prevReverseRankVector).getRanking(hayIdx,
                        //(*setIt).first));
                    }
                }

            }

            (*curRankVector).updateRanksFromDistMatrices(hayIdx);
            (*curReverseRankVector).updateRanksFromDistMatrices(hayIdx);

            if (iteration != rankingIteration_ - 1)
            {
                (*curRankVector).binarize(hayIdx, minCorrelatedPeak_, 0, 1);
                (*curReverseRankVector).binarize(hayIdx, minCorrelatedPeak_, 0, 1);
            }

        }


        tmpVector = prevRankVector;
        prevRankVector = curRankVector;
        curRankVector = tmpVector;

        tmpVector = prevReverseRankVector;
        prevReverseRankVector = curReverseRankVector;
        curReverseRankVector = tmpVector;

        curRankVector->setRanks(0);
        curReverseRankVector->setRanks(0);
    }


    un_s *un = build_unit_set(IS_RAW_U, 0, 1 / (float) needleHybCount , 0, 0, NULL);
    add_x_unit_set_to_op(op, un);
    set_op_x_unit_set(op, 1);

    for (int j = 0; j < needleHybCount; ++j)
    {
        ds = create_and_attach_one_ds(op, haystack.positions_.size() * 3, haystack.positions_.size() * 3, 0);

        set_ds_source(ds, "Peak ranking for needle peak n°%d", j);

        for (int i = 0; i < hayHybCount; ++i)
        {
            //std::pair<int, const int&> maxPair = curRankVector->maxRanking(i);
            ds->xd[i * 3] =  haystack.positions_[i] * needleHybCount + j;
            ds->xd[i * 3 + 1] = haystack.positions_[i] * needleHybCount + j;
            ds->xd[i * 3 + 2] = haystack.positions_[i] * needleHybCount + j;
            //peak_idx_ds->xd[i] = haystack.positions_[i];


            ds->yd[i * 3] = 0;
            ds->yd[i * 3 + 1]  = prevRankVector->getRanking(i, j);
            ds->yd[i * 3 + 2] = 0;

        }
    }


    set_plot_title(op, strdup(needle.description().c_str()));
    op->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);

    return NULL;
}
