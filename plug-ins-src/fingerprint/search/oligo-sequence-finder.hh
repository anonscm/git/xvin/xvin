#pragma once
#include "../results/search-result.hh"
#include "../sequence/oligo-sequence.hh"
#include "abstract-search.hh"

/**
 * @brief A search algorithm allowing to map a DNA fragment (needle)
 * in a reference genome (haystack) -> Find the origin fragment. 
 *
 * This algorithm use fingerprint represented as a succession oligonucleotide, regardeless of the
 * distance between each hybridization: Each oligo is concidered as a letter in a sequence.
 *
 *  Current status : Not implemented / Work in progress
 *
 */

class OligoSequenceFinder:
public AbstractSearch
{
  public:
    OligoSequenceFinder();
    virtual ~OligoSequenceFinder();


    OligoSequenceResult *find(const OligoSequence& haystack, const OligoSequence& needle);


  private:
    float deletionCost_;
    float missingCost_;
    float additionCost_;
    float inversionCost_;

    bool reject(const OligoSequence& needle, const OligoSequenceSolution& dos);
    bool accept(const OligoSequence& needle, const OligoSequenceSolution& dos);

};
