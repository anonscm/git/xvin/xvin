#pragma once
#include "../sequence/fingerprint.hh"
#include "../sequence/dist-fingerprint.hh"
#include "../results/search-match.hh"
#include "../results/search-match-builder.hh"
#include "abstract-search.hh"
#include "../results/search-result.hh"
#include "../params/basic-search-params.hh"
#include <json.hpp>
using json = nlohmann::json;


// FIXME use a param class
#include "../view/benchmark-config-view.hh"

/**
 * @brief A basic search algorithm allowing to map a DNA fragment (needle)
 * in a reference genome (haystack) -> Find the origin fragment. 
 *
 *  Current status : Working.
 *
 */
class BasicFinder: public AbstractSearch
{
    public:

    typedef DistFingerprint input_type;
    typedef SearchMatch result_type;
    typedef BasicSearchResult results_type;
    const static char * engine_name;
    friend benchmark::BenchmarkConfigView<BasicFinder>;
    typedef BasicSearchParams params_type;

        BasicFinder ();
        virtual ~BasicFinder ();

        json parameters(void);
        // Getter

        /**
         * @brief maximum error allow for a solution to be accepted
         *
         * @return 
         */
        double maxSearchHybridationError() const;

        /**
         * @brief BasePair Noise concidered for the search
         *
         * @return 
         */
        double noiseDeviation() const;

        /**
         * @brief Accept to skip a hybridization during search if the hybridized oligo is not
         * referenced into the needle
         *
         * @return 
         */
        bool skipMissingOligoInNeedle() const;

        /**
         * @brief Accept to skip a hybridization during search if the hybridized oligo is not
         * referenced into the haystack
         *
         * @return 
         */
        bool skipMissingOligoInHaystack() const;


        /**
         * @brief Minimum stretching regarding the reference 
         *
         * @return 
         */
        double minNmBaseRate() const;

        /**
         * @brief Maximum stretching regarding the reference
         *
         * @return 
         */
        double maxNmBaseRate() const;


        //Setter
        void maxSearchHybridationError(double maxHER);
        void noiseDeviation(double noiseDeviation);
        void skipMissingOligoInNeedle(bool skipMissing);
        void skipMissingOligoInHaystack(bool skipMissing);
        void minNmBaseRate(double minSR);
        void maxNmBaseRate(double maxSR);
        void stretchingRates(double minSR, double maxSR);

		const BasicSearchParams& params() const;
    	void params(const BasicSearchParams& params);
		

        /**
         * @brief Search using a needle fingerprint (generally an experimental or pseudo
         * experimental) the first matching location on both directionin to the haystack (generally a reference genome)
         *
         * @param haystack Reference Genome
         * @param needle Experimental fragment
         * @param start At which locus start the search.
         *
         * @return  The first match.
         */
        SearchMatch* find(const DistFingerprint& haystack, DistFingerprint& needle, int start);

        /**
         * @brief Find all location where needle and haystack are matching. (both direction)
         *
         * @param haystack
         * @param needle
         *
         * @return 
         */
        BasicSearchResult findAll(const DistFingerprint& haystack, const DistFingerprint& needle);
        /**
         * @brief Find all location where needle and haystack are matching. (both direction)
         * Search using each oligo present in the needle object one by one.
         *
         * @param haystack
         * @param needle
         *
         * @return 
         */
        BasicSearchResult findAllByOligo(const Fingerprint& haystack, Fingerprint& needle);

        /**
         * @brief Return true if the fingerprint contains enough information to be searched on.
         *
         * @param fgp 
         *
         * @return 
         */
        bool isFingerprintSignificant(const Fingerprint& fgp) const;


        /**
         * @brief Return true if a solution is good enougth to be kept.
         *
         * @param needle
         * @param searchMatch
         *
         * @return 
         */
        bool isViableSolution(const Fingerprint& needle, SearchMatch& searchMatch) const;

    private:
        BasicSearchParams params_;
        SearchMatch* findOneStrand(const DistFingerprint& haystack,const DistFingerprint& needle, int start);
        SearchMatch* findStretched(const DistFingerprint& haystack,
                                   const DistFingerprint &needle,
                                   HybridationMap::const_iterator& beginNeedleIt,
                                   HybridationMap::const_iterator& haystackStartPos,
                                   double nmBaseRate,
                                   SearchMatchBuilder smb);
        bool skipMatch(Fingerprint& haystack,const Hybridation& hOligo, Fingerprint& needle,const Hybridation& nOligo);
        bool skipMissingOligoInNeedle(const Fingerprint& needle,const Hybridation& haystackOligo);
        bool skipMissingOligoInHaystack(const Fingerprint& haystack, const Hybridation& needleOligo);

};

#include "basic-finder.hxx"
