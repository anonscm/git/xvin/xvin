#include "oligo-sequence-finder.hh"

OligoSequenceFinder::OligoSequenceFinder()
{
}

OligoSequenceFinder::~OligoSequenceFinder()
{
}

// FIXME -- WORK IN PROGRESS -- Oligo without length mesurement
OligoSequenceResult *OligoSequenceFinder::find(const OligoSequence& haystack, const OligoSequence& needle)
{
//    std::vector<OligoSequenceSolution> ps1;
//    std::vector<OligoSequenceSolution> ps2;
//    std::vector<OligoSequenceSolution>& pendingSolutions = ps1;
//    std::vector<OligoSequenceSolution>& nextPendingSolutions = ps2;
//    std::vector<OligoSequenceSolution>& swapps = ps1;
//    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
//    start = std::chrono::high_resolution_clock::now();
//    std::vector<OligoSequenceSolution> *acceptedSolutions = new std::vector<OligoSequenceSolution>();
//
//    for (uint i = 0; i < haystack.size(); ++i)
//    {
//        swapps = pendingSolutions;
//        pendingSolutions = nextPendingSolutions;
//        nextPendingSolutions = swapps;
//        nextPendingSolutions.clear();
//        pendingSolutions.push_back(OligoSequenceSolution(i));
//
//        for (uint j = 0; j < pendingSolutions.size(); ++j)
//        {
//            OligoSequenceSolution& os = pendingSolutions[j];
//            OligoSequenceSolution osm = os;
//
//            if (!reject(needle, os))
//            {
//                // differents cases:
//                // - Match between haystack and needle
//                // - Missmatch between haystack and needle
//                //      - Missing: Haystack peak is missing in Needle
//                //      - Deletion: 
//                //      - Addition: 
//
//
//                // Everything is ok
//                if (needle[os.cursor()] == haystack[i])
//                {
//                    os.addScore(1, false);
//                    os.next();
//                }
//
//                // Missing: Haystack peak is missing in needle
//                if (needle[os.cursor()] == hays)
//
//                    // Inversion: peak are inverted between haystack and needle
//                    if (os.cursor() + 1 < needle.size() && i + 1 < haystack.size() && (needle[os.cursor()] == haystack[i + 1])
//                            && (needle[os.cursor() + 1] == haystack[i]))
//                    {
//                        OligoSequenceSolution osi = osm;
//                        osi.addScore(1 - inversionCost_, true);
//                        osi.next();
//                    }
//
//                //
//                if (needle[os.cursor()] == haystack[i + 1])
//                {
//                    osm.addScore(deletionCost_, false);
//                }
//
//                // inversion - addition - deletion
//            }
//
//            dos.addScore(score, false);
//
//            if (accept(needle, dos))
//            {
//                acceptedSolutions->push_back(dos);
//                std::push_heap(acceptedSolutions->begin(), acceptedSolutions->end());
//            }
//            else
//            {
//                nextPendingSolutions.push_back(dos);
//            }
//        }
//    }
//
//    end = std::chrono::high_resolution_clock::now();
//    return new OligoSequenceResult(acceptedSolutions, end - start);
//}
//return results;
}


bool OligoSequenceFinder::reject(const OligoSequence& needle, const OligoSequenceSolution& dos)
{
    return false;
}

bool OligoSequenceFinder::accept(const OligoSequence& needle, const OligoSequenceSolution& dos)
{
    return  dos.size() >= needle.size();
}
