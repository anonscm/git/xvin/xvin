#include "chisquare-search.hh"
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/covariance.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/variates/covariate.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/normal.hpp>
#include <json.hpp>
using json = nlohmann::json;

#ifdef NDEBUG
#undef NDEBUG
#endif

namespace chiSquareMethod
{

namespace bacc = boost::accumulators;

const char *ChiSquareSearch::engine_name = "ChiSquare";

ChiSquareSearch::ChiSquareSearch()
{
    stretchingConfidenceLevel(params_.stretchingConfidenceLevel);
    noiseConfidenceLevel(params_.noiseConfidenceLevel);
}

ChiSquareSearch::~ChiSquareSearch()
{
}

ChiSquareSearch::results_type ChiSquareSearch::search(FingerprintVector& refces,
        std::shared_ptr<const PosFingerprint> expt,
        decltype(&ChiSquareSearch::rejectSolution) rejectFunc,
        decltype(&ChiSquareSearch::rejectSolution) acceptFunc,
        decltype(&ChiSquareSearch::gammaSortOperator) sortOperator)

{
    ChiSquareSearch::results_type ress;
    ress.needle_ = expt;

    if (ress.needle_->nbHybridation() < 2)
    {
        return ress;
    }

    for (uint i = 0; i < refces.size(); ++i)
    {
        auto refce = std::static_pointer_cast<const PosFingerprint>(refces[i]);
        auto curRes = search(refce, expt, rejectFunc, acceptFunc);

        for (uint j = 0; j < curRes.solutions_.size(); ++j)
        {
            curRes.solutions_[j].originReferenceIdx_ = i;
        }

        ress.duration_ += curRes.duration_;
        ress.solutions_.insert(
            ress.solutions_.end(),
            std::make_move_iterator(curRes.solutions_.begin()),
            std::make_move_iterator(curRes.solutions_.end())
        );
    }

    std::sort(ress.solutions_.begin(), ress.solutions_.end(), [sortOperator](auto a, auto b)
    {
        return sortOperator(a, b);
    });
    return std::move(ress);
}

ChiSquareSearch::results_type ChiSquareSearch::search(std::shared_ptr<const PosFingerprint> refce,
        std::shared_ptr<const PosFingerprint> expt, decltype(&ChiSquareSearch::rejectSolution) rejectFunc,
        decltype(&ChiSquareSearch::acceptSolution) acceptFunc, decltype(&ChiSquareSearch::gammaSortOperator) sortOperator)
{
    expt_ = expt;
    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
    start = std::chrono::high_resolution_clock::now();
    auto cssr = ChiSquareSearchResult();
    auto& res = cssr.solutions();
    cssr.needle(expt_);

    if (expt->size() == 0 || refce->size() == 0)
    {
        warning_message("Unable to search : Empty fingerprint, refce: %d, expt: %d", refce->size(), expt->size());
        return cssr;
    }

    auto searchOneStrand = [this, refce, &res, &rejectFunc, &acceptFunc](CandidateSolution::Direction dir, auto needle)
    {
        int needlePivotIdx = -1;
        needlePivotIdx = chooseRefHybridization(*needle);
        #pragma omp parallel for

        for (int i = 0; i < refce->nbHybridation(); ++i)
        {
            ChiSquareSearch::result_type sol;
            // Get candidat hybridization matchs
            sol.matchingHybs_ = getHybridizationMatch(*refce, *needle, i, needlePivotIdx);
            sol.univoqueMatchingHybs_ = getUnivoqueMatchingHybs(sol.matchingHybs_);
            sol.umDegreeOfFreedom_ = sol.univoqueMatchingHybs_.size();
            // Least square on univoques matchs
            sol.linearFit_ = linRegression(*needle, sol.univoqueMatchingHybs_);
            // ChiSquare fit
            sol.umChiSquareValue_ = chiSquare(*needle, sol.univoqueMatchingHybs_, sol.linearFit_);
            // Check solution quality
            sol.umCriticalValue_ = criticalValue(sol.umDegreeOfFreedom_);
            sol.needlePivotPosition_ = needle->positions()[needlePivotIdx];
            sol.refPivotPosition_ = refce->positions()[i];

            if (!(this->*rejectFunc)(sol))
            {
                sol.direction_ = dir;
                sol.matchingHybsFit_ = getHybridizationMatch(*refce, *needle, i, needlePivotIdx, 1 / sol.linearFit_.first);
                sol.choosenHybs_ = bestMatches(*needle, sol.matchingHybsFit_, sol.linearFit_);
                sol.degreeOfFreedom_ = sol.choosenHybs_.size();
                sol.criticalValue_ = criticalValue(sol.degreeOfFreedom_);
                sol.chiSquareValue_ = chiSquare(*needle, sol.choosenHybs_, sol.linearFit_);
                sol.refHybCount_ = refce->countHyb(
                                       (needle->firstHyb() - sol.needlePivotPosition_) / sol.linearFit_.first + sol.refPivotPosition_,
                                       (needle->lastHyb() - sol.needlePivotPosition_) / sol.linearFit_.first + sol.refPivotPosition_
                                   );

                if ((this->*acceptFunc)(sol))
                {
                    #pragma omp critical
                    {
                        res.push_back(std::move(sol));
                    }
                }
            }
        }
    };

    if (params_.dirForward)
    {
        searchOneStrand(CandidateSolution::FORWARD, expt);
    }

    if (params_.dirBackward)
    {
        auto needle = expt->reverse();
        searchOneStrand(CandidateSolution::BACKWARD, needle);
    }

    std::sort(res.begin(), res.end(), [sortOperator](auto a, auto b)
    {
        return sortOperator(a, b);
    });
    end = std::chrono::high_resolution_clock::now();
    cssr.duration(end - start);
    expt_ = 0;
    return cssr;
}

int ChiSquareSearch::chooseRefHybridization(const PosFingerprint& needle)
{
    int centerIdx = 0;
    int firstMatch = needle.firstHyb();
    int lastMatch = needle.lastHyb();
    int theoricCenter = (lastMatch - firstMatch) / 2;
    int center1 = needle.nextHybridizationPos(theoricCenter);
    int center2 = needle.previousHybridizationPos(theoricCenter);
    std::pair<PositionVector::const_iterator, PositionVector::const_iterator> posPair;

    if (theoricCenter - center1 < center2 - theoricCenter)
    {
        posPair = std::equal_range(needle.positions_.cbegin(), needle.positions_.cend(), center1);
    }
    else
    {
        posPair = std::equal_range(needle.positions_.cbegin(), needle.positions_.cend(), center2);
    }

    if (posPair.first == needle.positions_.cend())
    {
        posPair.first--;
    }

    centerIdx = std::distance(needle.positions_.cbegin(), posPair.first);
    return centerIdx;
}

MatchingHybVector ChiSquareSearch::getHybridizationMatch(const PosFingerprint& refce, const PosFingerprint& needle,
        int refPosIdx, int needlePivotIdx, double fixStretching)
{
    MatchingHybVector res(needle.nbHybridation());
    const PositionVector& npositions = needle.positions_;
    const PositionVector& rpositions = refce.positions_;
    int needlePivotPos = npositions[needlePivotIdx];
    int refPos = rpositions[refPosIdx];
    double minNmBaseRate;
    double maxNmBaseRate;
    //if (fixStretching > 0)
    //{
    //    minNmBaseRate = fixStretching;
    //    maxNmBaseRate = fixStretching;
    //}
    //else
    //{
    minNmBaseRate = minNmBaseRate__;
    maxNmBaseRate = maxNmBaseRate__;
    //}

    for (uint i = 0; i < npositions.size(); ++i)
    {
        int curNeedlePos = npositions[i] - needlePivotPos; // putting in the same referentiel
        double minPos;
        double maxPos;

        if (curNeedlePos < 0)
        {
            maxPos = curNeedlePos * minNmBaseRate + upperNoiseInterv__;
            minPos = curNeedlePos * maxNmBaseRate + lowerNoiseInterv__;
        }
        else
        {
            minPos = curNeedlePos * minNmBaseRate + lowerNoiseInterv__;
            maxPos = curNeedlePos * maxNmBaseRate + upperNoiseInterv__;
        }

        //backward search
        for (uint j = refPosIdx; j > 0; --j)
        {
            int curRefPos = rpositions[j] - refPos; // putting in the same referentiel

            if (curRefPos < minPos)
            {
                break;
            }

            if (curRefPos <= maxPos)
            {
                res[i].push_back(curRefPos);
            }
        }

        //forward search
        for (uint j = refPosIdx; j < rpositions.size(); ++j)
        {
            int curRefPos = rpositions[j] - refPos; // putting in the same referentiel

            if (curRefPos > maxPos)
            {
                break;
            }

            if (curRefPos >= minPos)
            {
                res[i].push_back(curRefPos);
            }
        }
    }

    return res;
}

LinearParams ChiSquareSearch::linRegression(const PosFingerprint& needle,
        const MatchingPairVector& matchs)
{
    double a = 0;
    double b = 0;
    // Be careful about overflow
    bacc::accumulator_set < double,
         bacc::stats<
         bacc::tag::mean(bacc::lazy),
         bacc::tag::mean_of_variates<double, bacc::tag::covariate1>,
         bacc::tag::variance(bacc::lazy),
         bacc::tag::covariance<double, bacc::tag::covariate1>>> acc;
    const PositionVector& positions = needle.positions();

    for (uint i = 0; i < matchs.size(); ++i)
    {
        int curNeedlePos = positions[matchs[i].first];
        int refMatchPos = matchs[i].second;
        acc(refMatchPos, bacc::covariate1 = curNeedlePos);
    }

    a = bacc::variance(acc) == 0 ? 0 : bacc::covariance(acc) / bacc::variance(acc);
    b = bacc::mean_of_variates<double, bacc::tag::covariate1>(acc) - a * bacc::mean(acc);
    return LinearParams(a, b);
}

double ChiSquareSearch::chiSquare(const PosFingerprint& needle, const MatchingPairVector& matchs,
                                  const LinearParams& lin)
{
    const PositionVector& positions = needle.positions();
    bacc::accumulator_set < double,
         bacc::stats<
         bacc::tag::moment<1>
         >> acc;

    for (uint i = 0; i < matchs.size(); ++i)
    {
        int curNeedlePos = positions[matchs[i].first];
        int refMatchPos = matchs[i].second;
        acc(std::pow(((curNeedlePos - lin.second - lin.first * (refMatchPos)) / params_.expectedNoiseDeviation), 2));
    }

    return bacc::moment<1>(acc);
}

std::pair<int, double> ChiSquareSearch::bestMatch(const PosFingerprint& needle, const MatchingHybVector& matchs,
        const LinearParams& lin, int posNeedleIdx, std::map<int, int>& choosenMatch,
        std::map<int, std::pair<int, double>>& closedList)
{
    const PositionVector& positions = needle.positions();
    int curNeedlePos = positions[posNeedleIdx]; /*pos needle*/
    const std::vector<int>& refMatchPoss = matchs[posNeedleIdx]; /*pos ref*/
    double min_residue = FLT_MAX;
    int pos = -1;
    int needleIdx = -1;

    // Take match with minimum residue
    for (uint j = 0; j < refMatchPoss.size(); ++j)
    {
        double residue = std::pow(((curNeedlePos - lin.second - lin.first * (refMatchPoss[j])) /
                                   params_.expectedNoiseDeviation), 2);

        if (residue < min_residue)
        {
            auto it = closedList.find(refMatchPoss[j]);

            if (it == closedList.end() || residue < it->second.second)
            {
                min_residue = residue;
                pos = refMatchPoss[j];
            }
        }
    }

    if (pos == -1)
    {
        auto it = choosenMatch.find(posNeedleIdx);

        if (it != choosenMatch.end())
        {
            choosenMatch.erase(it);
        }
    }
    else if (closedList.find(pos) != closedList.end())
    {
        std::pair<int, double> oldVal = closedList[pos];
        assert(oldVal.first != posNeedleIdx);
        closedList[pos] = std::make_pair(posNeedleIdx, min_residue);
        choosenMatch[posNeedleIdx] = pos;
        auto bm = bestMatch(needle, matchs, lin, oldVal.first, choosenMatch, closedList);
    }
    else
    {
        choosenMatch[posNeedleIdx] = pos;
        closedList[pos] =  std::make_pair(posNeedleIdx, min_residue);
    }
}

MatchingPairVector ChiSquareSearch::bestMatches(const PosFingerprint& needle,
        const MatchingHybVector& matchs,
        const LinearParams& lin)
{
    std::map<int /*pos needle*/, int /*pos ref*/> choosenMatch;
    std::map<int /*pos ref*/, std::pair<int /*pos needle*/, double /*residue*/>> closedList;

    for (uint i = 0; i < matchs.size(); ++i)
    {
        //closedList.clear();
        bestMatch(needle, matchs, lin, i /* index for ref */, choosenMatch, closedList);
    }

    MatchingPairVector res;

    // std::copy is segv O_o
    for (auto choosen : choosenMatch)
    {
        res.push_back(choosen);
    }

    return std::move(res);
}


bool ChiSquareSearch::rejectSolution(ChiSquareCandidateSolution& cs)
{
    return (cs.umDegreeOfFreedom_ < params_.minUmDegreeOfFreedom || cs.umChiSquareValue_ > cs.umCriticalValue());
}

bool ChiSquareSearch::rejectNonOriginSolution(ChiSquareCandidateSolution& cs)
{
    return !(abs(cs.refPivotPosition() - (expt_->originPosition() + cs.needlePivotPosition())) < expt_->size());
}

bool ChiSquareSearch::acceptSolution(ChiSquareCandidateSolution& cs)
{
    return (cs.choosenHybs_.size() / (double) cs.matchingHybs_.size() > params_.minFoundRate
            && cs.choosenHybs_.size() / (double) cs.refHybCount_ > params_.minFoundRate);
}

bool ChiSquareSearch::acceptAllSolution(ChiSquareCandidateSolution& cs)
{
    return true;
}
bool ChiSquareSearch::rejectNone(ChiSquareCandidateSolution& cs)
{
    return false;
}

double ChiSquareSearch::criticalValue(int degreeOfFreedom)
{
    if (degreeOfFreedom == 0)
    {
        return -1;
    }

    boost::math::chi_squared_distribution<double> distribution(degreeOfFreedom);
    const double critical_value = boost::math::quantile(boost::math::complement(distribution,
                                  params_.gammaSignificanceLevel));
    return critical_value;
}

MatchingPairVector ChiSquareSearch::getUnivoqueMatchingHybs(const MatchingHybVector& matchs)
{
    MatchingPairVector res;

    for (uint i = 0; i < matchs.size(); ++i)
    {
        if (matchs[i].size() == 1)
        {
            res.push_back(std::make_pair(i, matchs[i][0]));
        }
    }

    return std::move(res);
}
int ChiSquareSearch::matchCount(const MatchingHybVector& matchs, auto cond)
{
    int count = 0;

    for (uint i = 0; i < matchs.size(); ++i)
    {
        if (cond(matchs[i]))
        {
            count++;
        }
    }

    return count;
}
const NmBaseRateDistribution& ChiSquareSearch::nmBaseRateDistribution() const
{
    return params_.nmBaseRateDistribution;
}
void ChiSquareSearch::nmBaseRateDistribution(NmBaseRateDistribution nmbrDist)
{
    params_.nmBaseRateDistribution = nmbrDist;
    boost::math::normal_distribution<double> dist = boost::math::normal_distribution<double>
            (params_.nmBaseRateDistribution.mean(),
             params_.nmBaseRateDistribution.standard_deviation());
    minNmBaseRate__ = boost::math::quantile(boost::math::complement(dist, params_.stretchingConfidenceLevel));
    maxNmBaseRate__ = boost::math::quantile(dist, params_.stretchingConfidenceLevel);
}
void ChiSquareSearch::minUmDegreeOfFreedom(int minumdof)
{
    params_.minUmDegreeOfFreedom = minumdof;
}
int ChiSquareSearch::minUmDegreeOfFreedom() const
{
    return params_.minUmDegreeOfFreedom;
}
void ChiSquareSearch::minFoundRate(double mfr)
{
    params_.minFoundRate = mfr;
}
double ChiSquareSearch::minFoundRate() const
{
    return params_.minFoundRate;
}

double ChiSquareSearch::gammaSignificanceLevel() const
{
    return params_.gammaSignificanceLevel;
}
void ChiSquareSearch::gammaSignificanceLevel(double gsl)
{
    params_.gammaSignificanceLevel = gsl;
}

double ChiSquareSearch::expectedNoiseDeviation() const
{
    return params_.expectedNoiseDeviation;
}
void ChiSquareSearch::expectedNoiseDeviation(double exnde)
{
    params_.expectedNoiseDeviation = exnde;
    noiseConfidenceLevel(params_.noiseConfidenceLevel);
}


double ChiSquareSearch::stretchingConfidenceLevel() const
{
    return params_.stretchingConfidenceLevel;
}
void ChiSquareSearch::stretchingConfidenceLevel(double scl)
{
    params_.stretchingConfidenceLevel = scl;
    nmBaseRateDistribution(params_.nmBaseRateDistribution);
}

bool ChiSquareSearch::gammaSortOperator(ChiSquareCandidateSolution& a, ChiSquareCandidateSolution& b)
{
    return a.chiSquareValue() / a.criticalValue() < b.chiSquareValue() / b.criticalValue();
}

bool ChiSquareSearch::pivotSortOperator(ChiSquareCandidateSolution& a, ChiSquareCandidateSolution& b)
{
    return a.refPivotPosition() < b.refPivotPosition();
}
double ChiSquareSearch::noiseConfidenceLevel() const
{
    return params_.noiseConfidenceLevel;
}
void ChiSquareSearch::noiseConfidenceLevel(double ncl)
{
    params_.noiseConfidenceLevel = ncl;
    boost::math::normal_distribution<double> dist = boost::math::normal_distribution<double>(0,
            params_.expectedNoiseDeviation);
    lowerNoiseInterv__ = boost::math::quantile(boost::math::complement(dist, params_.noiseConfidenceLevel));
    upperNoiseInterv__ = boost::math::quantile(dist, params_.noiseConfidenceLevel);
}

void ChiSquareSearch::params(const ChiSquareSearchParams& params)
{
    params_ = params;
}

const ChiSquareSearchParams& ChiSquareSearch::params() const
{
    return params_;
}


}
