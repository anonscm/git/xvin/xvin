#pragma once
#include <gsl/gsl_randist.h>


inline double RankFinder::maxNmBaseRate() const
{
    return maxNmBaseRate_;
}

inline double RankFinder::minNmBaseRate() const
{
    return minNmBaseRate_;
}
inline int RankFinder::rankingIteration() const
{
    return rankingIteration_;
}

inline int RankFinder::minCorrelatedPeak() const
{
    return minCorrelatedPeak_;
}


inline void RankFinder::maxNmBaseRate(double maxSR)
{
    maxNmBaseRate_ = maxSR;
}

inline void RankFinder::minNmBaseRate(double minSR)
{
    minNmBaseRate_ = minSR;
}
inline void RankFinder::rankingIteration(int rankIter)
{
    rankingIteration_ = rankIter;
}
inline void RankFinder::minCorrelatedPeak(int mcp)
{
    minCorrelatedPeak_ = mcp;
}

inline void RankFinder::stretchingRates(double minSR, double maxSR)
{
    minNmBaseRate_ = minSR;
    maxNmBaseRate_ = maxSR;
}
inline bool RankFinder::isViableSolution(const
                                                Fingerprint& /*needle*/,
                                                SearchMatch& searchMatch) const
{
    return searchMatch.errorRate() <=  maxSearchHybridationError_ /*&& chiSquareProbability_ >= 0.01*/;
}

inline double RankFinder::noiseDeviation() const
{
    return noiseDeviation_;
}

inline void RankFinder::noiseDeviation(double noiseDeviation)
{
    noiseDeviation_ = noiseDeviation;
    localSearchPrecision_ = noiseDeviation_ * 3;
}

inline double RankFinder::maxSearchHybridationError() const
{
    return maxSearchHybridationError_;
}

inline void RankFinder::maxSearchHybridationError(double maxHER)
{
    maxSearchHybridationError_ = maxHER + std::numeric_limits<double>::epsilon();
}

inline bool RankFinder::isFingerprintSignificant(const Fingerprint& fgp) const
{
    return fgp.nbHybridation() >= 3;
}
