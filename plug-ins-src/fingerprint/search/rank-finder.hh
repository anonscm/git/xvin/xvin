#pragma once
#include "../sequence/fingerprint.hh"
#include "../sequence/pos-fingerprint.hh"
#include "../results/search-match.hh"
#include "../results/search-match-builder.hh"
#include "abstract-search.hh"
/**
 * @brief A search algorithm allowing to map a DNA fragment (needle)
 * in a reference genome (haystack) -> Find the origin fragment. 
 *
 * This algorithm try to rank hybridization looking at if their neighbourhood is matching between
 * the haystack and the needle.
 *
 * An area with a lot of well rank hybridization should be a good match for mapping
 *
 * Current status : Implemented / Not tested extensivally / Abandoned.
 */

class RankFinder : 
    public AbstractSearch
{
  public:
    RankFinder();
    virtual ~RankFinder();

    // Getter
    double maxSearchHybridationError() const;
    double noiseDeviation() const;
    double minNmBaseRate() const;
    double maxNmBaseRate() const;
    int rankingIteration() const;
    int minCorrelatedPeak() const;



    //Setter
    void maxSearchHybridationError(double maxHER);
    void noiseDeviation(double noiseDeviation);
    void minNmBaseRate(double minSR);
    void maxNmBaseRate(double maxSR);
    void stretchingRates(double minSR, double maxSR);
    void rankingIteration(int rankingIteration);
    void minCorrelatedPeak(int minCorrelatedPeak);

    bool isFingerprintSignificant(const Fingerprint& fgp) const;
    bool isViableSolution(const Fingerprint& needle, SearchMatch& searchMatch) const;

    SearchMatch *buildRankVector(const PosFingerprint& haystack,
                                 const PosFingerprint& needle);
  private:
    /**
     * @brief
     */
    double maxSearchHybridationError_;

    /**
     * @brief
     */
    double noiseDeviation_;
    double localSearchPrecision_;

    /**
     * @brief the minimum possible stretching of DNA with picoSeq
     */
    double minNmBaseRate_;
    /**
     * @brief the maximum possible stretching of DNA with picoSeq
     */
    double maxNmBaseRate_;

    int minCorrelatedPeak_;
    int rankingIteration_;

};

#include "rank-finder.hxx"
