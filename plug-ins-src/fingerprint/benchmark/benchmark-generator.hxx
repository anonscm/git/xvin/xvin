#include "progress_builder.h"
#include "../search/dist-class-generator.hh"
#include <memory>

namespace bacc = boost::accumulators;
namespace benchmark
{
template<typename SearchEngine, Aggregator A = Aggregator::Step>
struct Policy
{
    using engine_type = SearchEngine;
    static const Aggregator aggregator = A;

    // No way to specialize outsize class definition...

    template <typename ... Dummy, typename SE = SearchEngine>
    static typename
    std::enable_if<std::is_same<SE, chiSquareMethod::ChiSquareSearch>::value, typename engine_type::results_type>::type
    search(SearchEngine& searchEngine, Params<SearchEngine>& params,
           std::shared_ptr<typename engine_type::input_type> haystack,
           std::shared_ptr<const Fingerprint> needle)
    {
        static_assert(sizeof...(Dummy) == 0, "Do not specify template arguments!");
        std::shared_ptr<const typename engine_type::input_type> posNeedle =
            std::static_pointer_cast<const typename engine_type::input_type>
            (needle);

            if (posNeedle != nullptr && haystack != nullptr)
            {
                auto results = searchEngine.search(haystack, posNeedle);
                results.needle(needle);
                return results;
            }

        return typename SE::results_type();
    }

    template <typename ... Dummy, typename SE = SearchEngine>
    static typename
    std::enable_if<std::is_same<SE, BasicFinder>::value, typename engine_type::results_type>::type
    search(SearchEngine& searchEngine, Params<SearchEngine>& params,
           std::shared_ptr<typename engine_type::input_type> haystack,
           std::shared_ptr<const Fingerprint> needle)
    {
        static_assert(sizeof...(Dummy) == 0, "Do not specify template arguments!");
        std::shared_ptr<const typename engine_type::input_type> posNeedle =
            std::static_pointer_cast<const typename engine_type::input_type>
            (needle);

        if (posNeedle != nullptr && haystack != nullptr)
        {
            auto results = searchEngine.findAll(*haystack, *posNeedle);
            results.needle(needle);
            return results;
        }

        return typename SE::results_type();
    }

    template <typename ... Dummy, typename SE = SearchEngine>
    static typename
    std::enable_if<std::is_same<SE, DistOligoSequenceFinder>::value, typename engine_type::results_type>::type
    search(SearchEngine& searchEngine, Params<SearchEngine>& params,
           std::shared_ptr<typename engine_type::input_type> haystack,
           std::shared_ptr<const Fingerprint> needle)
    {
        static_assert(sizeof...(Dummy) == 0, "Do not specify template arguments!");

        if (haystack == nullptr || needle == nullptr)
        {
            warning_message("pb with needle or haystack");
            return typename SE::results_type();
        }

        std::shared_ptr<typename engine_type::input_type> cur_dos = std::shared_ptr<DistOligoSequence> (new DistOligoSequence(
                    needle,
                    haystack->distClasses(), searchEngine.params().distClassBaseNmRate));
// TODO Choose how are generated distClass
        searchEngine.distClasses(haystack->distClasses());
        typename engine_type::results_type results = searchEngine.find(*haystack, *cur_dos);
        results.needle(cur_dos);
        return results;
    }

    template <typename ... Dummy, typename SE = SearchEngine>
    static typename
    std::enable_if<std::is_same<SE, chiSquareMethod::ChiSquareSearch>::value, std::shared_ptr<chiSquareMethod::ChiSquareSearch::input_type>>::type
            importHaystack(std::string sequencePath, oligo_t **oligoSet, int oligoSetSize)
    {
        static_assert(sizeof...(Dummy) == 0, "Do not specify template arguments!");
        sqd *seq = read_fasta_seq(sequencePath.c_str(), true);    // read fasta file accepting "N" nucleotides

        for (int j = 0; j < oligoSetSize; ++j)
        {
            Oligo o = Oligo(BiDirOligo(*(oligoSet[j])));
        }

        auto haystack = std::shared_ptr<PosFingerprint>(new PosFingerprint(seq, oligoSet, oligoSetSize, 3));
        free(seq);
        return haystack;
    }
    template <typename ... Dummy, typename SE = SearchEngine>
    static typename
    std::enable_if<std::is_same<SE, BasicFinder>::value, std::shared_ptr<BasicFinder::input_type>>::type
            importHaystack(std::string sequencePath, oligo_t **oligoSet, int oligoSetSize)
    {
        static_assert(sizeof...(Dummy) == 0, "Do not specify template arguments!");
        sqd *seq = read_fasta_seq(sequencePath.c_str(), true);    // read fasta file accepting "N" nucleotides

        for (int j = 0; j < oligoSetSize; ++j)
        {
            Oligo o = Oligo(BiDirOligo(*(oligoSet[j])));
        }

        auto haystack = std::shared_ptr<DistFingerprint>(new DistFingerprint(seq, oligoSet, oligoSetSize, 3));
        free(seq);
        return haystack;
    }
    template <typename ... Dummy, typename SE = SearchEngine>
    static typename
    std::enable_if<std::is_same<SE, DistOligoSequenceFinder>::value, std::shared_ptr<DistOligoSequenceFinder::input_type>>::type
            importHaystack(std::string sequencePath, oligo_t **oligoSet, int oligoSetSize)
    {
        static_assert(sizeof...(Dummy) == 0, "Do not specify template arguments!");
        sqd *seq = read_fasta_seq(sequencePath.c_str(), true);    // read fasta file accepting "N" nucleotides
        assert(oligoSetSize > 0);

        for (int j = 0; j < oligoSetSize; ++j)
        {
            Oligo o = Oligo(BiDirOligo(*(oligoSet[j])));
        }

        auto fgp = std::shared_ptr<PosFingerprint>(new PosFingerprint(seq, oligoSet, oligoSetSize, 3));
        assert(fgp->positions().size() > 0);
        //TODO : Set parameters via json
        auto dcg = DistClassGenerator(*fgp, 0.1, MIN_BASE_NM_RATE, MAX_BASE_NM_RATE, 2.5, 3000);
        std::vector<int> *dists = dcg.computeDistDistribution();
        DistClasses *dcv = dcg.computeDistClasses(dists);
        auto dos = std::shared_ptr<DistOligoSequence> (new DistOligoSequence(fgp, *dcv));
        assert(dos->nbHybridation() > 0);
        delete dists;
        delete dcv;
        free(seq);
        return dos;
    }
    template <typename ... Dummy, typename SE = SearchEngine>
    static typename
    std::enable_if<std::is_same<SE, chiSquareMethod::ChiSquareSearch>::value, const Fingerprint&>::type
    getFgp(typename SearchEngine::input_type& haystack)
    {
        static_assert(sizeof...(Dummy) == 0, "Do not specify template arguments!");
        return haystack;
    }

    template <typename ... Dummy, typename SE = SearchEngine>
    static typename
    std::enable_if<std::is_same<SE, BasicFinder>::value, const Fingerprint&>::type
    getFgp(typename SearchEngine::input_type& haystack)
    {
        static_assert(sizeof...(Dummy) == 0, "Do not specify template arguments!");
        return haystack;
    }

    template <typename ... Dummy, typename SE = SearchEngine>
    static typename
    std::enable_if<std::is_same<SE, DistOligoSequenceFinder>::value, const Fingerprint&>::type
    getFgp(typename SearchEngine::input_type& haystack)
    {
        static_assert(sizeof...(Dummy) == 0, "Do not specify template arguments!");
        return *haystack.parentFingerprint();
    }

    template <typename ... Dummy, Aggregator AG = A>
    static typename
    std::enable_if<AG == Aggregator::Step, void>::type
    accumulate(AggregateDatas<SearchEngine>& ar, typename SearchEngine::results_type& result, int step)
    {
        static_assert(sizeof...(Dummy) == 0, "Do not specify template arguments!");

        if (step >= (int) ar.size())
        {
            ar.resize(step + 1);
        }

        int cur_val = result.firstTruePositiveRank();

        if (cur_val == -1)
        {
            ar[step].notFoundCount++;
            ar[step].notFoundDuration += result.duration();
        }
        else
        {
            ar[step].rankAccu(cur_val);
            ar[step].foundDuration += result.duration();
        }

        ar[step].aggrValue = step;
    }

    template <typename ... Dummy, Aggregator AG = A>
    static typename
    std::enable_if<AG == Aggregator::None, void>::type
    accumulate(AggregateDatas<SearchEngine>& ar, typename SearchEngine::results_type& result, int step)
    {
        static_assert(sizeof...(Dummy) == 0, "Do not specify template arguments!");

        if (step >= (int) ar.size())
        {
            ar.resize(step + 1);
        }

        int cur_val = result.firstTruePositiveRank();
        ar[step].results.push_back(result);

        if (cur_val == -1)
        {
            ar[step].notFoundCount++;
            ar[step].notFoundDuration += result.duration();
        }
        else
        {
            ar[step].rankAccu(cur_val);
            ar[step].foundDuration += result.duration();
        }

        ar[step].aggrValue = step;
    }

    template <typename ... Dummy, Aggregator AG = A>
    static typename
    std::enable_if<AG == Aggregator::HybCount, void>::type
    accumulate(AggregateDatas<SearchEngine>& ar, typename SearchEngine::results_type& result, int step)
    {
        static_assert(sizeof...(Dummy) == 0, "Do not specify template arguments!");
        int cur_val = result.firstTruePositiveRank();
        int nbHyb = 0;

        if (result.needle())
        {
            nbHyb = result.needle()->nbHybridation();
        }

        if ((nbHyb) >= (int) ar.size())
        {
            ar.resize(nbHyb + 1);
        }

        if (cur_val == -1)
        {
            ar[nbHyb].notFoundCount++;
            ar[nbHyb].notFoundDuration += result.duration();
        }
        else
        {
            ar[nbHyb].rankAccu(cur_val);
            ar[nbHyb].foundDuration += result.duration();
        }
    }

};


template<typename T>
BenchmarkGenerator<T>::BenchmarkGenerator(param_type& params)
    :
    poorResults_(0),
    allResults_(0),
    params_(params)
{
}

template<typename T>
BenchmarkGenerator<T>::~BenchmarkGenerator()
{
}

template<typename T>
int BenchmarkGenerator<T>::samplesPerStep(void)
{
    return params_.samplesPerStep;
}
template<typename T>

int BenchmarkGenerator<T>::genSequencialLocus(int currentStep, int currentSample)
{
    return currentSample * params_.locusInterval % (int)(params_.haystack->size() -
            params_.generatorParamsPerStep[currentStep].sizeDistribution.mean());
}

template<typename T>
int BenchmarkGenerator<T>::genRandomLocus(int currentStep, int currentSample)
{
    std::mt19937 mt_rand(params_.randSeed);
    mt_rand.discard(currentSample);
    std::uniform_int_distribution<> unif_dist(0,
            abs(params_.haystack->size() - params_.generatorParamsPerStep[currentStep].sizeDistribution(mt_rand)));
    return unif_dist(mt_rand);
}

template<typename T>
std::shared_ptr<Fingerprint> BenchmarkGenerator<T>::genSample(int step, int locus)
{
    int size = params_.generatorParamsPerStep[step].sizeDistribution.mean();
//    std::cerr << currentStep << ": "<< cur_locus << std::endl;
    auto cur_fgp = stepHaystack_ != nullptr ? fgpsg_.generate(1, &T::getFgp(*stepHaystack_), locus) : fgpsg_.generate(1,
                   &T::getFgp(*params_.haystack), locus);

    if (cur_fgp.size() > 0)
    {
        return cur_fgp[0];
    }
    else
    {
        return nullptr;
    }
}

template<typename T>
bool BenchmarkGenerator<T>::isMatchingLocus(Fingerprint& needle,
        const typename engine_type::result_type& doSol)
{
    return (abs(doSol.firstReferencePosition() - needle.originPosition()) < (int) needle.size() / 2);
}

template<typename T>
int BenchmarkGenerator<T>::poorResults(std::vector <typename engine_type::results_type> *poorResults)
{
    poorResults_ = poorResults;
    return 0;
}

template<typename T>
int BenchmarkGenerator<T>::allResults(std::vector <typename engine_type::results_type> *allResults)
{
    allResults_ = allResults;
    return 0;
}



template<typename T>
typename T::engine_type::results_type BenchmarkGenerator<T>::run(int currentStep, int currentSample)
{
    bool accepted = false;
    int cur_val = 0;
    //float paramVal = params_.minVal + currentStep * params_.step;
    int cur_locus = params_.randomGenerated ?
                    genRandomLocus(currentStep, currentSample) :
                    genSequencialLocus(currentStep, currentSample);
    std::shared_ptr<Fingerprint> cur_fgp = genSample(currentStep, cur_locus);
    auto results = stepHaystack_ ? T::search(searchEngine_, params_, stepHaystack_, cur_fgp) :
                   T::search(searchEngine_, params_, params_.haystack, cur_fgp) ;

    for (auto solution : results.solutions())
    {
        if (isMatchingLocus(*cur_fgp, solution))
        {
            accepted = true;
            break;
        }

        cur_val++;
    }

    if (!accepted)
    {
        cur_val = -1;
    }

    results.firstTruePositiveRank(cur_val);

    if (params_.truncateRes)
    {
        if (params_.keepScores)
        {
            results.extractScores();
        }

        results.truncateSolutions();
    }

    if (cur_val != 0 && poorResults_)
    {
        poorResults_->push_back(results);
    }

    if (allResults_)
    {
        allResults_->push_back(results);
    }

    return results;
}


template <typename T>
AggregatedResults<typename T::engine_type> BenchmarkGenerator<T>::aggregate(AggregateDatas<typename T::engine_type>&
        ads, float firstAggr, float aggrStep)
{
    AggregatedResults<typename T::engine_type> stepResults;

    for (uint i = 0; i < ads.size(); ++i)
    {
        AggregateData<typename T::engine_type>& ad = ads[i];
        AggregatedResult<typename T::engine_type> sr;
        sr.foundCount = bacc::count(ad.rankAccu);
        sr.notFoundCount = ad.notFoundCount;
        sr.aggrValue = firstAggr + aggrStep * i;
        sr.step = sr.aggrValue;

        if (sr.foundCount > 0)
        {
            sr.mean = bacc::mean(ad.rankAccu);
            sr.variance = bacc::variance(ad.rankAccu);
            sr.max = bacc::max(ad.rankAccu);
            sr.min = bacc::min(ad.rankAccu);
            sr.durationMean = ad.foundDuration / sr.foundCount;
        }

        if (sr.notFoundCount != 0)
        {
            sr.notFoundDurationMean = ad.notFoundDuration / sr.notFoundCount;
        }

        sr.haystackInfos = std::move(ad.haystackInfos);
        sr.results = std::move(ad.results);
        stepResults.push_back(std::move(sr));
    }

    return std::move(stepResults);
}

template<typename T>
AggregatedResults<typename T::engine_type> BenchmarkGenerator<T>::runSteps()
{
    progress_window_t *progress = NULL;
    char buf[1024] = {0};
    char buf2[1024] = {0};
    AggregateDatas<typename T::engine_type> accus = AggregateDatas<typename T::engine_type>();
    progress = progress_create("Benchmark progress");
    progress_run(progress);
    bool canceled = false;
    bool hasHaystackPerStep = false;
    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
    std::chrono::high_resolution_clock::duration elapsed_time =
        std::chrono::high_resolution_clock::duration::zero();
    std::chrono::high_resolution_clock::duration estimated_step_time =
        std::chrono::high_resolution_clock::duration::zero();
    start = std::chrono::high_resolution_clock::now();

    if (params_.randomGenerated)
    {
        std::random_device rd;
        params_.randSeed = rd();
    }

    if (!params_.randomGenerated && params_.mapGenom)
    {
        params_.samplesPerStep = params_.haystack->size() / params_.locusInterval;
    }

    for (int i = 0; i < params_.generatorParamsPerStep.size() && !canceled; ++i)
    {
        end = std::chrono::high_resolution_clock::now();

        if (i != 0)
        {
            elapsed_time = end - start;
            estimated_step_time = elapsed_time / i;
        }

        auto remaining = estimated_step_time * params_.generatorParamsPerStep.size() - estimated_step_time * i;
        snprintf(buf, sizeof(buf), "current step %i of %i\nelapsed: ~%imin \nremaining: ~%imin\nstep: ~%imin", i + 1,
                 params_.generatorParamsPerStep.size(), std::chrono::duration_cast<std::chrono::minutes>(elapsed_time),
                 std::chrono::duration_cast<std::chrono::minutes>(remaining),
                 std::chrono::duration_cast<std::chrono::minutes>(estimated_step_time));
        // WARNING : pas parallelisable
        hasHaystackPerStep = (params_.generatorParamsPerStep[i].oligoSetSize > 0);

        if (hasHaystackPerStep)
        {
            assert(params_.generatorParamsPerStep[i].oligoSetSize > 0);
            snprintf(buf2, sizeof(buf2), "Generating haystack... \n%s", buf);
            progress_set_info_text(progress, buf2);
            stepHaystack_ = T::importHaystack(params_.sequencePath, params_.generatorParamsPerStep[i].oligoSet,
                                              params_.generatorParamsPerStep[i].oligoSetSize);
        }
        else
        {
            stepHaystack_ = nullptr;
        }

        progress_set_info_text(progress, buf);
        fgpsg_ = FingerprintSampleGenerator();
        fgpsg_.params(params_.generatorParamsPerStep[i]);
        fgpsg_.randomSeed(params_.randSeed);
        searchEngine_ = typename T::engine_type();
        auto par = params_.searchParamsPerStep[i];

        if (hasHaystackPerStep)
        {
            par.generateMissingParams(*stepHaystack_);
        }
        else
        {
            par.generateMissingParams(*params_.haystack);
        }

        searchEngine_.params(par);

        //#pragma omp parallel for
        for (int j = 0; j < samplesPerStep() && !canceled; ++j)
        {
            typename T::engine_type::results_type result = run(i, j);
            T::accumulate(accus, result, i);
            progress_set_fraction(progress, (i * samplesPerStep() + j) / (float)(params_.generatorParamsPerStep.size()*
                                  samplesPerStep()));

            if (progress_is_canceled(progress))
            {
                canceled = true;
            }
        }
    if (params_.aggregator != Aggregator::HybCount && stepHaystack_)
    {
        accus[i].haystackInfos = stepHaystack_->dumpJson(false);
    }
    }

    if (poorResults_)
    {
        poorResults_->shrink_to_fit();
    }

    if (allResults_)
    {
        allResults_->shrink_to_fit();
    }

    progress_free(progress);
    progress = NULL;
    return std::move(aggregate(accus));
}
}
