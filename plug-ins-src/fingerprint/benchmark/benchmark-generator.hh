#pragma once

#include "../results/dist-oligo-solution.hh"
#include "../results/search-result.hh"
#include "../sequence/dist-oligo-sequence.hh"
#include "../sequence/fingerprint.hh"
#include "../search/dist-oligo-sequence-finder.hh"
#include "../search/chisquare-search.hh"
#include "../search/basic-finder.hh"
#include "../results/benchmark-step-results.hh"
#include "../params/benchmark-params.hh"
#include "../sequence/fingerprint-sample-generator.hh"
#include <queue>
#include <map>


/**
 * @brief This abstract class define a family of class that allow to benchmark fingerprint mapping
 * algorithm.
 * It generate sample from a reference algorithm, and output performance of the tested algorithm.
 * with those sample.
 */

namespace benchmark
{


template<typename Policy>
class BenchmarkGenerator
{

  public:
    using engine_type = typename Policy::engine_type;
    using param_type = Params<engine_type>;

    BenchmarkGenerator(param_type& params);
    ~BenchmarkGenerator();

    typename Policy::engine_type::results_type run(int currentStep, int currentSample);

    AggregatedResults<engine_type> runSteps();
    std::vector<typename engine_type::results_type> runStepsFull();

    int samplesPerStep(void);
    int locusInterval(int locusInterval);
    int randSeed(int randSeed);

    int poorResults(std::vector <typename engine_type::results_type> *poorResults);
    int allResults(std::vector <typename engine_type::results_type> *allResults);
    std::shared_ptr<Fingerprint> genSample(int step, int locus);

  protected:
    std::vector <typename engine_type::results_type> *poorResults_ = nullptr;
    std::vector <typename engine_type::results_type> *allResults_ = nullptr;

    param_type params_;
    FingerprintSampleGenerator fgpsg_;
    engine_type searchEngine_;
    std::shared_ptr<typename engine_type::input_type> stepHaystack_ = nullptr;

    int genSequencialLocus(int currentStep, int currentSample);
    int genRandomLocus(int currentStep, int currentSample);
    bool isMatchingLocus(Fingerprint& needle, const typename engine_type::result_type& doSol);
    AggregatedResults<engine_type> aggregate(AggregateDatas<engine_type>& ads, float firstAggr = 0., float aggrStep = 1.);
};
}

#include "benchmark-generator.hxx"
