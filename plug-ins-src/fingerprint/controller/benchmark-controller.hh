#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif
#include "../view/benchmark-view.hh"
#include "../results/benchmark-step-results.hh"
#include "../params/benchmark-params.hh"

namespace benchmark
{

template <typename SearchEngine>
class Controller
{
  public:
    Controller();
    virtual ~Controller();

    Params<SearchEngine> config(const char *benchmark_file = nullptr);
    int benchmark(Params<SearchEngine>& params);
    int listData();
    int listPoorResults();
    int displayPoorResult(int idx);
    int dumpJson(const std::string& folder) const;

  private:
    Params<SearchEngine> params_;
    AggregatedResults<SearchEngine> aggrResults_;
    View<SearchEngine> benchmarkView_;
    BenchmarkConfigView<SearchEngine> benchmarkConfigView_;
    std::shared_ptr<SearchEngine> searchEngine_ = std::shared_ptr<SearchEngine>(new SearchEngine());
    std::vector <typename SearchEngine::results_type> poorResults_;
    std::vector <typename SearchEngine::results_type> allResults_;

};

void put_git_info(json &j);

}

#include "benchmark-controller.hxx"
