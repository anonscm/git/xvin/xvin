#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif
#include "../view/chisquare-solution-viewer.hh"
#include "../results/chisquare-candidate-solution.hh"
#include "../search/chisquare-search.hh"

namespace chiSquareMethod
{
class ChiSquareSolutionController
{
  public:
    ChiSquareSolutionController();
    virtual ~ChiSquareSolutionController();

    int searchAll(std::shared_ptr<const PosFingerprint> expt, std::shared_ptr<chiSquareMethod::ChiSquareSearch> searchEngine = nullptr);
    int search(std::shared_ptr<const PosFingerprint> refce, std::shared_ptr<const PosFingerprint> expt, std::shared_ptr<chiSquareMethod::ChiSquareSearch> searchEngine = nullptr);
    int searchOrigin(std::shared_ptr<const PosFingerprint> refce, std::shared_ptr<const PosFingerprint> expt, std::shared_ptr<chiSquareMethod::ChiSquareSearch> searchEngine = nullptr);
    int config();
    int candidateSolutions(ChiSquareSearch::results_type);
    int nextCandidate();
    int prevCandidate();
    int selectCandidate();
    int viewCandidate(int id);
    int listMatchingOfCandidate(int id);
    int selectAndViewCandidate();
    int selectAndListMatchingOfCandidate();
    std::weak_ptr<const PosFingerprint> expt();

  private:
    ChiSquareSolutionViewer view_;

    std::weak_ptr<const PosFingerprint> reference_;
    std::weak_ptr<const PosFingerprint> expt_;
    ChiSquareSearch::results_type candidateSolutions_ = ChiSquareSearch::results_type();
    int candidatIdx_;
    std::shared_ptr<ChiSquareSearch> searchEngine_ = std::shared_ptr<ChiSquareSearch>(new ChiSquareSearch());

    bool checkIdxAndView(int idx);

};
}
