#include "benchmark-controller.hh"
#include "chisquare-solution-controller.hh"
#include "../storage/environnement.hh"
#include "../view/dist-oligo-solution-viewer.hh"
// FIXME REMOVE THAT
#include "../gui/cssv-menu.hh"
#include "../gui/menu.hh"

#include "gitinfo.hh"

typedef fingerprint::Environnement Env;

namespace benchmark
{
template<>
int Controller<chiSquareMethod::ChiSquareSearch>::displayPoorResult(int idx)
{
    static chiSquareMethod::ChiSquareSolutionController cssc = chiSquareMethod::ChiSquareSolutionController();

    fingerprint_cssv_set_controller(&cssc);
    //FIXME search on all fgp
    std::shared_ptr<const PosFingerprint> refce = std::static_pointer_cast<const PosFingerprint>
            (Env::instance().references()[0]);
    std::shared_ptr<const PosFingerprint> needle = std::static_pointer_cast<const PosFingerprint>
            (poorResults_[idx].needle());

    if (poorResults_[idx].firstTruePositiveRank() == -1)
    {
        cssc.searchOrigin(refce, needle, searchEngine_);
    }
    else
    {
        cssc.search(refce, needle, searchEngine_);
    }

    cssc.selectAndViewCandidate();
    return 0;
}
template<>
int Controller<BasicFinder>::displayPoorResult(int idx)
{
    warning_message("Not implemented");
    return 0;
}

template<>
int Controller<DistOligoSequenceFinder>::displayPoorResult(int idx)
{
    error_message("Deactivated code, please review %s:%s", __FILE__,__LINE__);
  //  std::shared_ptr<const DistOligoSequence> refce_dos = Env::instance().referenceDsos()[0];
  //  std::shared_ptr<const Fingerprint> refce = refce_dos->parentFingerprint(); 

  //  std::shared_ptr<const PosFingerprint> needle = std::static_pointer_cast<const PosFingerprint>
  //          (poorResults_[idx].needle());

  //  DistOligoSequence needle_dos = DistOligoSequence(needle, Env::instance().distClasses());
  // DistSearchResult result = searchEngine_->find(*refce_dos, needle_dos);

  //  DistOligoSolutionViewer *dosv = new DistOligoSolutionViewer(Env::instance().distClasses(), refce, needle, std::move(result), 2);
  //  
  // set_dosv(dosv); 
  // return 0;

}

void put_git_info(json &j)
{
    j["git"] = {git::hashtag(), git::branch(), git::version(), git::author(), git::date()};
}
}



