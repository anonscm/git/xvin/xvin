#include "chisquare-solution-controller.hh"
#include "../search/chisquare-search.hh"
#include "../view/data-list.hh"
#include "../view/chisquare-search-config-view.hh"
#include "../storage/environnement.hh"

typedef fingerprint::Environnement Env;

chiSquareMethod::ChiSquareSolutionController::ChiSquareSolutionController()
    : view_(ChiSquareSolutionViewer()),
      reference_(),
      expt_(),
      candidateSolutions_(),
      candidatIdx_(-1)
{
}

chiSquareMethod::ChiSquareSolutionController::~ChiSquareSolutionController()
{
}

int chiSquareMethod::ChiSquareSolutionController::nextCandidate()
{
    int id = -1;

    if (candidateSolutions_.solutions().size() > 0)
    {
        id = (candidatIdx_ + 1) % candidateSolutions_.solutions().size();
    }

    return checkIdxAndView(id);
}

int chiSquareMethod::ChiSquareSolutionController::prevCandidate()
{
    candidatIdx_--;
    candidatIdx_ = candidatIdx_ < 0 ? candidateSolutions_.solutions().size() - 1 : candidatIdx_;
    return checkIdxAndView(candidatIdx_);
}

int chiSquareMethod::ChiSquareSolutionController::searchAll(std::shared_ptr<const PosFingerprint> expt,
        std::shared_ptr<chiSquareMethod::ChiSquareSearch> searchEngine)
{
    Env& env = Env::instance();

    if (searchEngine == nullptr)
    {
        searchEngine = searchEngine_;
    }

    expt_ = expt;
    candidateSolutions_ = searchEngine->search(env.references(), expt);

    if (candidateSolutions_.solutions().empty())
    {
        if (win_printf("No solution found, do you want to display origin solution / all solutions ?") == WIN_OK)
        {
            if (expt->originPosition() != -1)
                candidateSolutions_ = searchEngine->search(env.references(), expt, &ChiSquareSearch::rejectNonOriginSolution,
                                      &ChiSquareSearch::acceptAllSolution);

            if (expt->originPosition() == -1 || candidateSolutions_.solutions().size() == 0)
            {
                candidateSolutions_ = searchEngine->search(env.references(), expt, &ChiSquareSearch::rejectNone,
                                      &ChiSquareSearch::acceptAllSolution, &ChiSquareSearch::pivotSortOperator);
            }
        }
    }

    // FIXME : do not create everytime a new op in viewer
    view_ = ChiSquareSolutionViewer(1 / searchEngine->nmBaseRateDistribution().mean(),
                                    searchEngine->nmBaseRateDistribution().standard_deviation(), 0.95, searchEngine->expectedNoiseDeviation(), searchEngine->noiseConfidenceLevel());
    return 0;
}
int chiSquareMethod::ChiSquareSolutionController::search(std::shared_ptr<const PosFingerprint> refce,
        std::shared_ptr<const PosFingerprint> expt, std::shared_ptr<chiSquareMethod::ChiSquareSearch> searchEngine)
{
    if (searchEngine == nullptr)
    {
        searchEngine = searchEngine_;
    }

    reference_ = refce;
    expt_ = expt;
    candidateSolutions_ = searchEngine->search(refce, expt);

    if (candidateSolutions_.solutions().empty())
    {
        if (win_printf("No solution found, do you want to display origin solution / all solutions ?") == WIN_OK)
        {
            if (expt->originPosition() != -1)
                candidateSolutions_ = searchEngine->search(refce, expt, &ChiSquareSearch::rejectNonOriginSolution,
                                      &ChiSquareSearch::acceptAllSolution);

            if (expt->originPosition() == -1 || candidateSolutions_.solutions().size() == 0)
            {
                candidateSolutions_ = searchEngine->search(refce, expt, &ChiSquareSearch::rejectNone,
                                      &ChiSquareSearch::acceptAllSolution, &ChiSquareSearch::pivotSortOperator);
            }
        }
    }

    // FIXME : do not create everytime a new op in viewer
    view_ = ChiSquareSolutionViewer(1 / searchEngine->nmBaseRateDistribution().mean(),
                                    searchEngine->nmBaseRateDistribution().standard_deviation(), 0.95, searchEngine->expectedNoiseDeviation(), searchEngine->noiseConfidenceLevel());
    return 0;
}

int chiSquareMethod::ChiSquareSolutionController::searchOrigin(std::shared_ptr<const PosFingerprint> refce,
        std::shared_ptr<const PosFingerprint> expt, std::shared_ptr<chiSquareMethod::ChiSquareSearch> searchEngine)
{
    if (searchEngine == nullptr)
    {
        searchEngine = searchEngine_;
    }

    reference_ = refce;
    expt_ = expt;

    if (expt->originPosition() != -1)
        candidateSolutions_ = searchEngine->search(refce, expt, &ChiSquareSearch::rejectNonOriginSolution,
                              &ChiSquareSearch::acceptAllSolution);

    if (expt->originPosition() == -1 || candidateSolutions_.solutions().size() == 0)
    {
        candidateSolutions_ = searchEngine->search(refce, expt, &ChiSquareSearch::rejectNone,
                              &ChiSquareSearch::acceptAllSolution, &ChiSquareSearch::pivotSortOperator);
    }

    // FIXME : do not create everytime a new op in viewer
    view_ = ChiSquareSolutionViewer(1 / searchEngine->nmBaseRateDistribution().mean(),
                                    searchEngine->nmBaseRateDistribution().standard_deviation(), 0.95, searchEngine->expectedNoiseDeviation(), searchEngine->noiseConfidenceLevel());
    return 0;
}

int chiSquareMethod::ChiSquareSolutionController::candidateSolutions(chiSquareMethod::ChiSquareSearch::results_type
        candidates)
{
    candidateSolutions_ = candidates;
    return 0;
}

int chiSquareMethod::ChiSquareSolutionController::selectCandidate()
{
    DataList listView;
    auto expt = expt_.lock();
    int id = -1;
    Env& env = Env::instance();

    if (candidateSolutions_.solutions().empty())
    {
        error_message("No Candidate solution to show in the list !");
        return -1;
    }

    std::string title = std::string("Chi Square Solutions");
    std::vector<std::string> headerTexts =
    {
        "Id",
        "Ref size",
        "Expt size",
        "HybCount",
        "Expt origin",
        "Orientation",
        "Ref Pivot",
        "ax",
        "+ b",
        "uM°freedom",
        "uM-X²",
        "uM-GammaQ",
        "uM-Ratio",
        "°freedom",
        "X²",
        "GammaQ",
        "Ratio",
        "ExptHybRate",
        "RefHybRate"
    };
    std::vector<GType> headerTypes =
    {
        G_TYPE_INT,
        G_TYPE_INT,
        G_TYPE_INT,
        G_TYPE_INT,
        G_TYPE_STRING,
        G_TYPE_STRING,
        G_TYPE_INT,
        G_TYPE_DOUBLE,
        G_TYPE_DOUBLE,
        G_TYPE_INT,
        G_TYPE_DOUBLE,
        G_TYPE_DOUBLE,
        G_TYPE_DOUBLE,
        G_TYPE_INT,
        G_TYPE_DOUBLE,
        G_TYPE_DOUBLE,
        G_TYPE_DOUBLE,
        G_TYPE_DOUBLE,
        G_TYPE_DOUBLE
    };
    listView.setTitle(title);
    listView.setHeaders(headerTexts, headerTypes);
    auto mainRefce = reference_.lock();

    for (uint i = 0; i < candidateSolutions_.solutions().size(); ++i)
    {
        int missingCount = 0;
        auto fcn = [&missingCount](auto it)
        {
            missingCount += !!it.empty();
        };
        const ChiSquareCandidateSolution& curSol = candidateSolutions_[i];
        auto refce = curSol.originReferenceIdx() >= 0 && (uint) curSol.originReferenceIdx() < env.references().size() ?
                     std::static_pointer_cast<const PosFingerprint> (env.references()[curSol.originReferenceIdx()]) : mainRefce;
        std::for_each(curSol.matchingHybs().begin(), curSol.matchingHybs().end(), fcn);
        listView.addRow(
            0, i,
            1, refce->size(),
            2, expt->size(),
            3, curSol.matchingHybs().size(),
            4, expt->originPosition() >= 0 && abs(curSol.firstReferencePosition() - expt->originPosition()) < expt->size() ? "*" : " ",
            5, curSol.direction() == CandidateSolution::FORWARD ? "Forward" : "Backward",
            6, curSol.refPivotPosition(),
            7, curSol.linearFit().first,
            8, curSol.linearFit().second,
            9, curSol.umDegreeOfFreedom(),
            10, curSol.umChiSquareValue(),
            11, curSol.umCriticalValue(),
            12, curSol.umChiSquareValue() / curSol.umCriticalValue(),
            13, curSol.degreeOfFreedom(),
            14, curSol.chiSquareValue(),
            15, curSol.criticalValue(),
            16, curSol.chiSquareValue() / curSol.criticalValue(),
            17, (double) curSol.choosenHybs().size() / curSol.matchingHybs().size(),
            18, (double) curSol.choosenHybs().size() / curSol.refHybCount(),
            -1);
    }

    id = listView.run();
    return id;
}
int chiSquareMethod::ChiSquareSolutionController::selectAndListMatchingOfCandidate()
{
    int id = selectCandidate();

    if (id >= 0 && (uint) id < candidateSolutions_.solutions().size())
    {
        viewCandidate(id);
        listMatchingOfCandidate(id);
    }
    else
    {
        warning_message("Invalid index");
    }

    return 0;
}
int chiSquareMethod::ChiSquareSolutionController::listMatchingOfCandidate(int id)
{
    DataList listView;
    ChiSquareCandidateSolution sol = candidateSolutions_[id];
    auto refce = reference_.lock();
    auto expt = expt_.lock();

    if (sol.matchingHybs().empty())
    {
        error_message("No Match in this solution!");
        return -1;
    }

    if (sol.direction() == CandidateSolution::BACKWARD)
    {
        expt = std::shared_ptr<PosFingerprint>(expt->reverse());
    }

    std::string title = std::string("Chi Square Solution Matching");
    std::vector<std::string> headerTexts = {"Id", "Expt Pos", "Ref Pos"};
    std::vector<GType> headerTypes = {G_TYPE_INT, G_TYPE_INT, G_TYPE_INT};
    listView.setTitle(title);
    listView.setHeaders(headerTexts, headerTypes);

    for (uint i = 0; i < sol.matchingHybs().size(); ++i)
    {
        listView.addRow(
            0, i,
            1, expt->positions()[i],
            -1);
        listView.pushLevel();
        auto matchs = sol.matchingHybs()[i];

        for (uint j = 0; j < matchs.size(); ++j)
        {
            listView.addRow(
                0, j,
                2, matchs[j] + sol.needlePivotPosition(),
                -1);
        }

        listView.popLevel();
    }

    id = listView.run();
    return id;
}

int chiSquareMethod::ChiSquareSolutionController::selectAndViewCandidate()
{
    int id = selectCandidate();
    return viewCandidate(id);
}

int chiSquareMethod::ChiSquareSolutionController::viewCandidate(int id)
{
    return checkIdxAndView(id);
}

bool chiSquareMethod::ChiSquareSolutionController::checkIdxAndView(int id)
{
    Env& env = Env::instance();

    if (id >= 0 && (uint) id < candidateSolutions_.solutions().size())
    {
        candidatIdx_ = id;
        auto refce = reference_.lock();
        auto expt = expt_.lock();
        auto curSol = candidateSolutions_[candidatIdx_];

        if (curSol.direction() == CandidateSolution::BACKWARD)
        {
            expt = std::shared_ptr<PosFingerprint>(expt->reverse());
        }

        if (curSol.originReferenceIdx() >= 0)
        {
            if (curSol.originReferenceIdx() < (int) env.references().size())
            {
                refce = std::static_pointer_cast<const PosFingerprint> (env.references()[curSol.originReferenceIdx()]);
            }
        }

        if (refce && expt)
        {
            view_.draw(*refce, *expt, curSol);
        }
        else
        {
            view_.reset();
            warning_message("fingerprint used in the search are no longer available");
        }
    }
    else
    {
        view_.reset();
        warning_message("No candidate solution to display (id = %d , max = %d) ", id, candidateSolutions_.solutions().size());
        candidatIdx_ = -1;
    }

    return true;
}

int chiSquareMethod::ChiSquareSolutionController::config()
{
    ConfigView cv;
    return cv.draw(*searchEngine_);
}

std::weak_ptr<const PosFingerprint> chiSquareMethod::ChiSquareSolutionController::expt()
{
    return expt_;
}
