#include "../view/data-list.hh"
#include <regex>
#include "json.hpp"
#include <boost/filesystem.hpp>
using json = nlohmann::json;
namespace benchmark
{
template<typename T>
Controller<T>::Controller()
{
}

template<typename T>
Controller<T>::~Controller()
{
}

template<typename T>
int Controller<T>::benchmark(Params<T>& params)
{
    params_ = params;
    poorResults_.clear();
    allResults_.clear();
    auto bg = [this, params](auto bengen) -> AggregatedResults<T>
    {
        bengen.poorResults(&poorResults_);

        if (params.keepAllResults)
        {
            bengen.allResults(&allResults_);
        }

        return bengen.runSteps();
    };

    // FIXME : search for a better dispatching
    switch (params.aggregator)
    {
    case Aggregator::Step:
        aggrResults_ = bg(BenchmarkGenerator<Policy<T, Aggregator::Step>>(params));
        break;

    case Aggregator::HybCount:
        aggrResults_ = bg(BenchmarkGenerator<Policy<T, Aggregator::HybCount>>(params));
        break;

    case Aggregator::None:
        aggrResults_ = bg(BenchmarkGenerator<Policy<T, Aggregator::None>>(params));
    }

    benchmarkView_.draw(params, aggrResults_);

    if (params.keepAllResults)
    {
        benchmarkView_.drawScore(params, allResults_);
    }

    if (params.output.size() > 0)
    {
        std::regex e = std::regex("\\{\\{date\\}\\}");
        time_t rawtime;
        struct tm *timeinfo;
        char buffer[80];
        time(&rawtime);
        timeinfo = localtime(&rawtime);

        strftime(buffer, 80, "%Y-%m-%d_%H:%M", timeinfo);
        std::string str(buffer);
        std::string fol = std::regex_replace(params.output, e, str);
        dumpJson(fol);
    }

    return 0;
}

template<typename T>
Params<T> Controller<T>::config(const char *benchmark_file)
{
    if (benchmarkConfigView_.draw(params_, benchmark_file) != D_O_K)
    {
        error_message("Configuration failed !\n");
    }

    return params_;
}

template<typename T>
int Controller<T>::dumpJson(const std::string& folder) const
{
    json j;
    time_t t;
    char timec[100] = {0};
    std::ofstream myfile;

    boost::filesystem::create_directories(folder);
    if (params_.aggregator != Aggregator::None)
    {
        return 0;
    }

    std::string filepath = folder + std::string(".json");
    myfile.open(filepath);
    time(&t);
    strftime(timec, sizeof(timec), "%c %Z", std::gmtime(&t));
    j["date"] = timec;
    put_git_info(j); 
    j["params"] = params_.dumpJson();
    auto haystack = params_.haystack;

    if (haystack)
    {
        j["reference"] = haystack->dumpJson(false);
    }

    for (int i = 0; i < aggrResults_.size(); ++i)
    {
        auto ress = aggrResults_[i].results;
        json jress;
        jress["value"] = aggrResults_[i].step;
        jress["results"] = json::array();

        for (int k = 0; k < ress.size(); ++k)
        {
            jress["results"].push_back(ress[k].dumpJson(folder, k));
        }

        j["steps"].push_back(jress);
    }

    myfile << std::setprecision(4) << j.dump()  << std::endl;
    myfile.flush();
    myfile.close();
    return 0;
}
template<typename T>
int Controller<T>::listPoorResults()
{
    DataList listView;
    std::string title = std::string("Poor results list");
    std::vector<std::string> headerTexts = {"Id", "Name", "Oligos", "Size", "Hybridizations", "Origin", "true positive rank"};
    std::vector<GType> headerTypes = {G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT, G_TYPE_INT} ;
    listView.setTitle(title);
    listView.setHeaders(headerTexts, headerTypes);

    for (uint i = 0; i < poorResults_.size(); ++i)
    {
        const auto curNeedle = poorResults_[i].needle();
        listView.addRow(
            0, i,
            1, curNeedle->description().c_str(),
            //TODO extract oligos
            2, curNeedle->oligoSet().begin()->get().oligo().c_str(),
            3, curNeedle->size(),
            4, curNeedle->nbHybridation(),
            5, curNeedle->originPosition(),
            6, poorResults_[i].firstTruePositiveRank(),
            -1);
    }

    int id = listView.run();
    return id;
}



}
