#pragma once

#include "../sequence/fingerprint.hh"
#include "../sequence/dist-oligo-sequence.hh"
#include "../define.hh"
#include <vector>

namespace fingerprint {
class Environnement
{
  public:
    virtual ~Environnement();
    static Environnement& instance();

    //FIXME quick and dirty RW accessor, need to be updated
    FingerprintVector& references();
    FingerprintVector& experimentals();
    DistClasses& distClasses();
    void distClasses(DistClasses& dcv);
    std::vector<std::shared_ptr<DistOligoSequence>>& referenceDsos();
    std::vector<std::shared_ptr<DistOligoSequence>>& experimentalDsos();



  protected:
    Environnement();
    FingerprintVector references_;
    FingerprintVector experimentals_;
    DistClasses distClasses_;// Not sure this should be there
    std::vector<std::shared_ptr<DistOligoSequence>> referenceDsos_;
    std::vector<std::shared_ptr<DistOligoSequence>> experimentalDsos_;
};
}
