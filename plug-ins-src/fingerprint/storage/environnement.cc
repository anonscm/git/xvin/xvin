#include "environnement.hh"
#include "../define.hh"
fingerprint::Environnement& fingerprint::Environnement::instance()
{
    static Environnement instance__;
    return instance__;
}
std::vector<std::shared_ptr<DistOligoSequence>>& fingerprint::Environnement::experimentalDsos()
{
	return experimentalDsos_; 
}

FingerprintVector& fingerprint::Environnement::experimentals()
{
	return experimentals_;
}

DistClasses& fingerprint::Environnement::distClasses()
{
	return distClasses_;
}

std::vector<std::shared_ptr<DistOligoSequence>>& fingerprint::Environnement::referenceDsos()
{
	return referenceDsos_;
}

FingerprintVector& fingerprint::Environnement::references()
{
	return references_;
}

fingerprint::Environnement::Environnement()
{
}

fingerprint::Environnement::~Environnement()
{
}

void fingerprint::Environnement::distClasses(DistClasses& dcv)
{
    distClasses_ = dcv;
 } 
