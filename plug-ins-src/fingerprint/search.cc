#include "search.hh"

void cluster_fingerprint(int fingerp_idx_a, int fingerp_idx_b,int ***clusters, int *nb_clusters,int **clusters_size)
{

    int i = 0;
    bool is_a_in_cluster = false;
    bool is_b_in_cluster = false;

    for (i = 0; i < *nb_clusters; ++i)
    {     

        for (int j = 0; j < (*clusters_size)[i] && !(is_a_in_cluster && is_b_in_cluster); ++j)
        {
            if ((*clusters)[i][j] == fingerp_idx_a)
            {
                is_a_in_cluster = true;
            }
            if ((*clusters)[i][j] == fingerp_idx_b)
            {
                is_b_in_cluster = true;
            }
        }
        if (is_a_in_cluster)
        {
            if (!is_b_in_cluster)
            {
                (*clusters)[i] = (int *) realloc((*clusters)[i], ((*clusters_size)[i] + 1) * sizeof(int));
                (*clusters)[i][(*clusters_size)[i]] = fingerp_idx_b;
                ((*clusters_size)[i])++;
            }
            break;
        }
        else
        {
            if (is_b_in_cluster)
           {
                (*clusters)[i] = (int *) realloc((*clusters)[i], ((*clusters_size)[i] + 1) * sizeof(int));
                (*clusters)[i][(*clusters_size)[i]] = fingerp_idx_a;
                ((*clusters_size)[i])++;
                break;
            }
        }
    }
    if (i == *nb_clusters)
    {
        (*nb_clusters)++;
        *clusters = (int **) realloc(*clusters, *nb_clusters * sizeof(int *));
        *clusters_size = (int *) realloc(*clusters_size, *nb_clusters * sizeof(int));
        (*clusters)[i] = (int *) malloc(2 * sizeof(int));
        (*clusters_size)[i] = 2;

        (*clusters)[i][0] = fingerp_idx_a;
        (*clusters)[i][1] = fingerp_idx_b;
    }
}

void cluster_alone_fingerprint(int fingerp_idx_a,int ***clusters, int *nb_clusters,int **clusters_size)
{

    int i = 0;
    bool is_a_in_cluster = false;

    for (i = 0; i < *nb_clusters; ++i)
    {     
        for (int j = 0; j < (*clusters_size)[i] && !(is_a_in_cluster); ++j)
        {
            if ((*clusters)[i][j] == fingerp_idx_a)
            {
                is_a_in_cluster = true;
            }
        }
        if (is_a_in_cluster)
        {
            break;
        }
    }
    if (i == *nb_clusters)
    {
        (*nb_clusters)++;
        *clusters = (int **) realloc(*clusters, *nb_clusters * sizeof(int *));
        *clusters_size = (int *) realloc(*clusters_size, *nb_clusters * sizeof(int));
        (*clusters)[i] = (int *) malloc(sizeof(int));
        (*clusters_size)[i] = 1;

        (*clusters)[i][0] = fingerp_idx_a;
    }
}
