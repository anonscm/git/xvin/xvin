#pragma once

# include "../findSeq/findSeq.h"
#include <xvin.h>
# include "sequence/fingerprint.hh"
# include "sequence/pos-fingerprint.hh"


double fingerprint_coperture(sqd* sequence,char **oligos, int nb_oligos);
int fingerprint_search_all(PosFingerprint& haystack, PosFingerprint& needle, int mismatch_tolerence, int offset_range, int** match_positions);
/**
 * @brief search a fingerprint into a other (a needle in a haystack)
 *
 * @param haystack big fingerprint sequence in which the function search
 * @param needle search pattern
 * @param start where to begin search
 * @param mismatch_tolerence tolerence to missmatch, when hybridation don't match between needle and
 * haystack
 * @param offset_range range in which the algorithm consider that an hybridation haystack near an
 * hybridation in needle match  
 *
 * @return position of the first occurence of needle
 */

void cluster_fingerprint(int fingerp_idx_a, int fingerp_idx_b,int ***clusters, int *nb_clusters,int **clusters_size);
void cluster_alone_fingerprint(int fingerp_idx_a,int ***clusters, int *nb_clusters,int **clusters_size);
