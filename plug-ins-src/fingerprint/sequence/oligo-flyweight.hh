#pragma once

#include "oligo-key-extractor.hh"

class BiDirOligo;
/**
 * @brief Implementation of the flyweight design pattern to decrease footprint of oligo structure in
 * a Fingerprint implementations 
 *
 * @tparam OligoType
 */
template<class OligoType>
class OligoFlyweight
{
    friend class boost::serialization::access;

    public:
        OligoFlyweight ();
        OligoFlyweight (OligoType val);
        virtual ~OligoFlyweight ();

        bool operator== (const OligoFlyweight &o1) const;
        bool operator!= (const OligoFlyweight &o1) const;
        bool operator< (const OligoFlyweight &o1) const;
        bool operator> (const OligoFlyweight &o1) const;

        friend std::ostream& operator<< (std::ostream& ostr, const OligoFlyweight<BiDirOligo>& o);
        

        const OligoType& get() const;
        int get_key() const;
        void set_key(int key);

        template<class Archive>
        void save(Archive& ar, const unsigned int /*version*/) const;
        template<class Archive>
        void load(Archive& ar,const unsigned int /*version*/);
        template <class Archive>
        void serialize(Archive & ar, const unsigned int version);


    private:
        int key_;
};

#include "oligo-flyweight.hxx"
