#include "pos-fingerprint.hh"
#include <algorithm>
#include <cmath>
int PosFingerprint::size() const
{
    return underlyingSequenceSize_;
}

bool PosFingerprint::size(int size)
{
    if (size >= 0)
    {
        underlyingSequenceSize_ = size;
        return true;
    }

    return false;
}

bool PosFingerprint::hybridationValue(int pos, Hybridation& oligo)
{
    assert(!oligo.empty());
    Oligo theoricalOligo = oligo.theoricalOligo();

    if (pos == -1 && oligos_.find(theoricalOligo) == oligos_.end())
    {
        outcastOligos_.insert(theoricalOligo);
    }
    else
    {
        assert(pos >= 0);

        if (positions_.size() > 0 && pos <= *(positions_.rbegin()))
        {
            PositionVector::iterator posIt = std::lower_bound(positions_.begin(), positions_.end(), pos);
            int dist = std::distance(positions_.begin(), posIt);

            if (*posIt == pos)
            {
                hybridations_[dist] = oligo;
            }
            else
            {
                positions_.insert(posIt, pos);
                hybridations_.insert(hybridations_.begin() + dist, oligo);
            }
        }
        else
        {
            positions_.push_back(pos);
            hybridations_.push_back(oligo);
        }

        //std::cout << pos << std::endl;
        //std::cout << oligo << std::endl;
        outcastOligos_.erase(theoricalOligo);
        oligos_.insert(theoricalOligo);
    }

    return true;
}

int PosFingerprint::nbHybridation() const
{
    if (positions_.empty())
    {
        return 0; // bug about bad init of position_ object...
    }

    return positions_.size();
}

Hybridation PosFingerprint::hybridationValue(int pos) const
{
    auto posPair
        = std::equal_range(positions_.cbegin(), positions_.cend(), pos);

    if (posPair.first != posPair.second)
    {
        int dist = std::distance(positions_.cbegin(), posPair.first);
        return hybridations_[dist];
    }
    else
    {
        return Hybridation();
    }
}

PosFingerprint::PosFingerprint(json& j, oligo_t **oligos, int nb_oligos)
{
    assert(nb_oligos > 0);
    Oligo theoricalOligo = Oligo(BiDirOligo(*(oligos[0])));
    Oligo actualOligo = Oligo(BiDirOligo(*(oligos[0])));

    for (uint i = 0; i < j.size(); i++)
    {
        json jo = j[i];
        Oligo cto = theoricalOligo;
        Oligo cao = actualOligo;

        if (!jo["theoricalOligo"].is_null())
        {
            oligo_t oligo;
            oligo.seq = strdup(jo["theoricalOligo"].get<std::string>().c_str());
            oligo.probability = 1.;
            oligo.behaviors_size = 0;
            oligo.behaviors = nullptr;
            cto = Oligo(BiDirOligo((oligo)));
            free(oligo.seq);
        }

        if (!jo["actualOligo"].is_null())
        {
            oligo_t oligo;
            oligo.seq = strdup(jo["actualOligo"].get<std::string>().c_str());
            oligo.probability = 1.;
            oligo.behaviors_size = 0;
            oligo.behaviors = nullptr;
            cao = Oligo(BiDirOligo((oligo)));
            free(oligo.seq);
        }

        float proba = jo["proba"];
        int pos = jo["pos"];
        Hybridation hyb = Hybridation(cto, actualOligo, proba);
        hybridationValue(pos, hyb);
    }

    underlyingSequenceSize_ = -1;
    isReferenceFingerprint_ = false;
}

PosFingerprint::PosFingerprint(sqd *sequence, oligo_t **oligos, int nb_oligos, int resolution)
    : Fingerprint(resolution),
      positions_(),
      hybridations_()
{
    description_  = std::string(sequence->description);
    PositionVector tmpPos;
    HybridationVector tmpHyb;
    assert(nb_oligos > 0);
    assert(strlen(oligos[0]->seq) > 0);

    for (int i = 0; i < nb_oligos; ++i)
    {
        Oligo theoricalOligo = Oligo(BiDirOligo(*(oligos[i])));
        Oligo actualOligo = Oligo();
        double oligoProba = oligos[i]->probability;

        //std::cout << oligos[i] << " bdo " << bdo << " olig " << olig.get_key() << std::endl;
        if (oligos_.find(theoricalOligo) == oligos_.end())
        {
            int bidx = -1;

            do
            {
                bidx++;
                Hybridation hyb = Hybridation(theoricalOligo, actualOligo, oligoProba);
                assert(!hyb.empty());
                oligos_.insert(theoricalOligo);
                int cur_oligo_pos = -1;

                do
                {
                    cur_oligo_pos = find_oligo_in_sequence(sequence, hyb.actualOligo().get().oligo().c_str(), cur_oligo_pos + 1,
                                                           1);

                    if (cur_oligo_pos != -1)
                    {
                        hybridationValue(cur_oligo_pos, hyb);
                        //tmpPos.push_back(cur_oligo_pos);
                        //tmpHyb.push_back(hyb);
                    }
                    else
                    {
                        //std::cout << "merge" << std::endl;
                    }
                }
                while (cur_oligo_pos > 0);

                do
                {
                    cur_oligo_pos = find_oligo_in_sequence(sequence, hyb.actualOligo().get().reverseOligo().c_str(),
                                                           cur_oligo_pos + 1, 1);

                    if (cur_oligo_pos != -1)
                    {
                        hybridationValue(cur_oligo_pos, hyb);
                        //tmpPos.push_back(cur_oligo_pos);
                        //tmpHyb.push_back(hyb);
                    }
                    else
                    {
                        //std::cout << "merge" << std::endl;
                    }
                }
                while (cur_oligo_pos > 0);

                if (bidx < oligos[i]->behaviors_size)
                {
                    theoricalOligo = Oligo(BiDirOligo(*(oligos[i])));
                    actualOligo = Oligo(BiDirOligo(oligos[i]->behaviors[bidx]));
                    oligoProba = oligos[i]->behaviors_proba[bidx];
                }
            }
            while (bidx < oligos[i]->behaviors_size);
        }
    }

    int changes = 1;
    PositionVector newpos;
    HybridationVector newhybs;

    while (changes > 0)
    {
        changes = 0;
        int cur_pos = 0;
        int prev_pos = positions_[0];

        for (int i = 1; i < positions_.size(); ++i)
        {
            cur_pos = positions_[i];
            int nhybs = 0;
            int sum_pos = 0;

            while ((cur_pos - prev_pos) <= resolution_ && (i + nhybs + 1) < positions_.size())
            {
      //          printf("merging %d and %d \n", prev_pos, cur_pos);
                sum_pos += cur_pos;
                nhybs++;
                cur_pos = positions_[i + nhybs];
                ++changes;
            }

            newpos.push_back((sum_pos + prev_pos) / (nhybs + 1));
            newhybs.push_back(hybridations_[i - 1]);
            i = i + nhybs;
            prev_pos = cur_pos;
        }

        positions_ = std::move(newpos);
        hybridations_ = std::move(newhybs);
        newpos.clear();
        newhybs.clear();
    //printf("%d CHANGES \n", changes);
    }

//std::vector<int> ord(tmpPos.size());
//std::iota(ord.begin(), ord.end(), 0);
//std::sort(ord.begin(), ord.end(),
//          [&](int i, int j)
//{
//    return tmpPos[i] < tmpPos[j];
//});
//positions_.resize(tmpPos.size(), 0);
//std::transform(ord.begin(), ord.end(), positions_.begin(),
//               [&](int i)
//{
//    return tmpPos[i];
//});
//tmpPos.clear();
//tmpPos.shrink_to_fit();
//hybridations_.resize(tmpHyb.size(), Hybridation());
//std::transform(ord.begin(), ord.end(), hybridations_.begin(),
//               [&](int i)
//{
//    return tmpHyb[i];
//});
    underlyingSequenceSize_ = sequence->totalSize;
//std::cout << sequence->totalSize << std::endl;
    isReferenceFingerprint_ = true;
}
PosFingerprint::~PosFingerprint()
{
}
PosFingerprint::PosFingerprint()
    : Fingerprint(),
      underlyingSequenceSize_(0),
      positions_(),
      hybridations_()
{
}
PosFingerprint::PosFingerprint(const OligoSet& oligos)
    : Fingerprint(oligos),
      positions_(),
      hybridations_()
{
}


PosFingerprint::PosFingerprint(const PosFingerprint& posfgp)
    : Fingerprint(posfgp),
      underlyingSequenceSize_(posfgp.underlyingSequenceSize_),
      positions_(posfgp.positions_),
      hybridations_(posfgp.hybridations_)
{
}

PosFingerprint *PosFingerprint::subfgp(int begin, int size) const
{
    //FIXME enable new to use posfingerprint again
    PosFingerprint *res = new PosFingerprint();
    res->isReferenceFingerprint_ = isReferenceFingerprint_;
    auto subPosBeginIt =  std::lower_bound(positions_.begin(), positions_.end(), begin);
    auto beginIdx = std::distance(positions_.begin(), subPosBeginIt);
    auto subHybBeginIt = hybridations_.begin() + beginIdx;
    auto subPosEndIt =  std::upper_bound(subPosBeginIt, positions_.end(), begin + size);
    auto endIdx = std::distance(positions_.begin(), subPosEndIt);
    auto subHybEndIt = hybridations_.begin() + endIdx;
    std::copy(subPosBeginIt, subPosEndIt, std::back_inserter(res->positions_));
    std::copy(subHybBeginIt, subHybEndIt, std::back_inserter(res->hybridations_));
    int firstPos = *subPosBeginIt;

    for (uint i = 0; i < res->hybridations_.size(); ++i)
    {
        auto oligo = res->hybridations_[i];
        Oligo theoricalOligo = oligo.theoricalOligo();
        res->outcastOligos_.erase(theoricalOligo);
        res->oligos_.insert(theoricalOligo);
        res->positions_[i] = res->positions_[i] - firstPos;
    }

    //if (res->positions().empty())
    //{
    //    delete res;
    //    return 0;
    //}
    res->underlyingSequenceSize_ = res->positions_.size() > 0 ? res->positions_[res->positions_.size() - 1] : 0;
    res->originPosition(firstPos);
    res->sourceFgp_ = shared_from_this();
    return res;
}


HybridationVector& PosFingerprint::hybridationsVector()
{
    return hybridations_;
}


int PosFingerprint::coperture(void) const
{
    int nbCovered = 0;

    //#pragma omp parallel for reduction(+:nbCovered)
    for (auto hyb : hybridations_)
    {
        nbCovered += hyb.actualOligo().get().size();
    }

    return nbCovered;
}
// TODO : Here we can add a way to know if distance from on peak always refers to the same peak.
OligoPosDistMap *PosFingerprint::oligoPosDistMap(float minNmToBaseRate, float maxNmToBaseRate,
        float noiseDeviation) const
{
    OligoPosDistMap *oligoPosDistMap = new OligoPosDistMap();

    for (uint i = 0; i < positions_.size(); ++i)
    {
        for (uint j = i + 1; j < positions_.size(); ++j)
        {
            OligoPair olp = OligoPair(hybridations_[i].actualOligo(), hybridations_[j].actualOligo());
            int dist = positions_[j] - positions_[i];
            int minDist = minNmToBaseRate * dist; //(dist - 3 * noiseDeviation);  // TODO : configurable confidence
            int maxDist = maxNmToBaseRate * dist; //(dist + 3 * noiseDeviation);
            DistIntervalSet& distIntervalSet = (*oligoPosDistMap)[olp];
            distIntervalSet += std::make_pair<DistInterval, std::set<std::pair<int, int>>>(DistInterval::closed(minDist, maxDist),
                               std::set<std::pair<int, int>> {std::make_pair<>(i, j)});
        }
    }

    return oligoPosDistMap;
}


PosFingerprint *PosFingerprint::reverse(void) const
{
    int nbHyb = positions_.size();
    PosFingerprint *res = new PosFingerprint(*this);
    res->oligos_ = this->oligos_;
    res->outcastOligos_ = this->outcastOligos_;
    res->positions_.resize(nbHyb);
    res->hybridations_.resize(nbHyb);
    int firstPos = positions_[0];
    int lastPos = positions_[nbHyb - 1];

    for (uint i = 0; i < nbHyb; ++i)
    {
        res->positions_[i] = (lastPos) - positions_[nbHyb - i - 1];
        res->hybridations_[i] = hybridations_[nbHyb - i - 1];
    }

    res->underlyingSequenceSize_ = underlyingSequenceSize_;
    return res;
}

//PosFingerprint *PosFingerprint::reverse(void)
//{
//    const PosFingerprint *current = this;
//    return current->reverse();
//}


std::ostream& PosFingerprint::dumpSfgp(std::ostream& ostr) const
{
    ostr << description_ << std::endl;
    ostr << underlyingSequenceSize_ << std::endl;

    for (uint i = 0; i < positions_.size(); ++i)
    {
        ostr << positions_[i] << "\t" << hybridations_[i] << std::endl;
    }

    return ostr;
}


PosFingerprint *PosFingerprint::stretch(double stretching) const
{
    PosFingerprint *res = new PosFingerprint(*this);

    if (res->positions_.size() == 0)
    {
        free(res);
        return 0;
    }

    for (uint i = 0; i < res->positions_.size(); ++i)
    {
        res->positions_[i] = positions_[i] * stretching;
    }

    return res;
}

PosFingerprint *PosFingerprint::gaussianNoise(NoiseDistribution& noiseDistribution,
        UniformRandomEngine& randomEngine) const
{
    PosFingerprint *res = new PosFingerprint(*this);

    if (res->positions_.size() == 0)
    {
        free(res);
        return 0;
    }

    for (uint i = 0; i < res->positions_.size(); ++i)
    {
        res->positions_[i] = positions_[i] + std::round(noiseDistribution(randomEngine));
    }

    return res;
}

PosFingerprint *PosFingerprint::randomRemoveHybs(MissingHybDistribution mhd, double thres,
        UniformRandomEngine& randomEngine) const
{
    PosFingerprint *res = new PosFingerprint(*this);
    uint i = 0;

    while (i < res->positions_.size())
    {
        float hybRatio = mhd(randomEngine);
        //printf("%f < %f ?\n", hybRatio, thres);

        if (hybRatio < thres)
        {
            //printf("yes, rm !\n");
            res->positions_.erase(res->positions_.begin() + i);
            res->hybridations_.erase(res->hybridations_.begin() + i);
        }
        else
        {
            ++i;
        }
    }

    //printf("%d\n", res->positions_.size());

    if (res->positions_.size() == 0)
    {
        free(res);
        return 0;
    }

    return res;
}



int PosFingerprint::firstHyb() const
{
    if (positions_.empty())
    {
        return -1;
    }

    return *(positions_.begin());
}

int PosFingerprint::lastHyb() const
{
    if (positions_.empty())
    {
        return -1;
    }

    return *(positions_.rbegin());
}

void PosFingerprint::generateDs(d_s *dsd, int beginPos, int endPos) const
{
    PositionVector::const_iterator posIt = std::lower_bound(positions_.begin(), positions_.end(), beginPos);
    PositionVector::const_iterator endIt = std::lower_bound(positions_.begin(), positions_.end(), endPos);
    int dsSize = std::distance(posIt, endIt);
    build_adjust_data_set(dsd, dsSize, dsSize);
    dsd->nx = dsSize;
    dsd->ny = dsSize;

    for (int i = 0; posIt != endIt; ++posIt, ++i)
    {
        dsd->xd[i] = *posIt;
        dsd->yd[i] = 0;
    }
}

int PosFingerprint::countHyb(int beginPos, int endPos) const
{
    PositionVector::const_iterator posIt = std::lower_bound(positions_.cbegin(), positions_.cend(), beginPos);
    PositionVector::const_iterator endIt = std::lower_bound(positions_.cbegin(), positions_.cend(), endPos);
    return endIt - posIt;
}
void PosFingerprint::generateLineDs(d_s *dsd, int beginPos, int endPos, int offset, int lineLength,
                                    Orientation orient) const
{
    PositionVector::const_iterator posIt = std::lower_bound(positions_.begin(), positions_.end(), beginPos);
    PositionVector::const_iterator endIt = std::lower_bound(positions_.begin(), positions_.end(), endPos);
    int dsSize = std::distance(posIt, endIt) * 2;
    build_adjust_data_set(dsd, dsSize, dsSize);
    dsd->nx = dsSize;
    dsd->ny = dsSize;
    float *axe1 = 0;
    float *axe2 = 0;

    if (orient == Orientation::HORIZONTAL)
    {
        axe1 = dsd->xd;
        axe2 = dsd->yd;
    }
    else
    {
        axe1 = dsd->yd;
        axe2 = dsd->xd;
    }

    for (int i = 0; posIt != endIt; ++posIt, ++i)
    {
        axe1[i * 2] = *posIt;
        axe2[i * 2] = offset;
        axe1[i * 2 + 1] = *posIt;
        axe2[i * 2 + 1] = offset + lineLength;
    }
}
int PosFingerprint::nextHybridizationPos(int pos) const
{
    PositionVector::const_iterator posIt = std::upper_bound(positions_.begin(), positions_.end(), pos);

    if (posIt != positions_.end())
    {
        return *posIt;
    }

    return -1;
}

int PosFingerprint::previousHybridizationPos(int pos) const
{
    PositionVector::const_iterator posIt = std::lower_bound(positions_.begin(), positions_.end(), pos);

    if (posIt != positions_.begin() && --posIt != positions_.begin())
    {
        return *posIt;
    }

    return -1;
    return *posIt;
}

const PositionVector& PosFingerprint::positions(void) const
{
    return positions_;
}

json PosFingerprint::dumpJson(bool dumpHybs) const
{
    json j;
    j["type"] = "PosFingerprint";
    j["desc"] = description_;
    j["isRef"] = isReferenceFingerprint_;
    j["origPos"] = originPosition_;
    j["resolution"] = resolution_;
    j["seqSize"] = underlyingSequenceSize_;
    j["hybCount"] = positions_.size();
    j["hybs"] = json::array();

    if (generatorParams_)
    {
        j["genParams"]["size"] =  generationParams_->size;
        j["genParams"]["locus"] =  generationParams_->locus;
        j["genParams"]["noiseStdev"] =  generationParams_->noiseDistribution.stddev();
        //j["genParams"]["fromDesc"] =  generationParams_->fromDesc()
        j["genParams"]["baseNmRate"] =  generationParams_->baseNmRate;
        j["genParams"]["isReverse"] =  generationParams_->isReverse;
        j["genParams"]["missingHybs"] = generationParams_->missingHybs;
        // TODO missing / added distribution (not used right now)
    }

    if (dumpHybs)
    {
        for (uint i = 0; i < positions_.size(); ++i)
        {
            json hybj;
            hybj["pos"] = positions_[i];
            hybj["theoOligo"] = hybridations_[i].theoricalOligo().get().oligo();
            hybj["actOligo"] = hybridations_[i].actualOligo().get().oligo();
            hybj["proba"] = hybridations_[i].probability();
            j["hybs"].push_back(hybj);
        }
    }
    else
    {
        j["oligoBatchs"] = json::array();

        for (auto biol : oligos_)
        {
            auto oligo = biol.get();
            j["oligoBatchs"].push_back(json::array());
            j["oligoBatchs"].back().push_back(oligo.oligo());

            for (auto subOligo : oligo.behaviors())
            {
                j["oligoBatchs"].back().push_back(subOligo.get().oligo());
            }
        }
    }

    return j;
}
