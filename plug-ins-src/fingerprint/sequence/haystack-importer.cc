#include "haystack-importer.hh"

#include "../search/chisquare-search.hh"
#include "../search/dist-oligo-sequence-finder.hh"
#include "../search/basic-finder.hh"
#include "../storage/environnement.hh"
#include "../search/dist-class-generator.hh"
#include "../define.hh"
typedef fingerprint::Environnement Env;

template<>
std::shared_ptr<PosFingerprint> HaystackImporter<chiSquareMethod::ChiSquareSearch>::importHaystack(bool addToEnv)
{
    oligo_t **oligos = nullptr;
    int nb_oligos = 0;
    sqd *seq = read_fasta_seq(sequencePath.c_str(), true);    // read fasta file accepting "N" nucléotides
    bool res = true;

    if (oligoSetSize > 0)
    {
        oligos = oligoSet;
        nb_oligos = oligoSetSize;
    }
    else
    {
        res &= parse_oligo_file(oligoSetPath.c_str(), &oligos, &nb_oligos); // read oligo file loading also reverse oligo
    }

    if (res == false)
    {
        return nullptr;
    }

    for (int j = 0; j < nb_oligos; ++j)
    {
        Oligo o = Oligo(BiDirOligo(*(oligos[j])));
    }

    auto fgp = std::shared_ptr<PosFingerprint>(new PosFingerprint(seq, oligos, nb_oligos, resolution));

    if (addToEnv)
    {
        Env::instance().references().push_back(fgp);
    }

    free(seq);
    return fgp;
}

template<>
std::shared_ptr<DistFingerprint> HaystackImporter<BasicFinder>::importHaystack(bool addToEnv)
{
    oligo_t **oligos = nullptr;
    int nb_oligos = 0;
    sqd *seq = read_fasta_seq(sequencePath.c_str(), true);    // read fasta file accepting "N" nucléotides
    bool res = true;

    if (oligoSetSize > 0)
    {
        oligos = oligoSet;
        nb_oligos = oligoSetSize;
    }
    else
    {
        res &= parse_oligo_file(oligoSetPath.c_str(), &oligos, &nb_oligos); // read oligo file loading also reverse oligo
    }

    if (res == false)
    {
        return nullptr;
    }

    for (int j = 0; j < nb_oligos; ++j)
    {
        Oligo o = Oligo(BiDirOligo(*(oligos[j])));
    }

    auto fgp = std::shared_ptr<DistFingerprint>(new DistFingerprint(seq, oligos, nb_oligos, resolution));

    if (addToEnv)
    {
        Env::instance().references().push_back(fgp);
    }

    free(seq);
    return fgp;
}

template<>
std::shared_ptr<DistOligoSequence> HaystackImporter<DistOligoSequenceFinder>::importHaystack(bool addToEnv)
{
    oligo_t **oligos = nullptr;
    int nb_oligos = 0;
    sqd *seq = read_fasta_seq(sequencePath.c_str(), true);    // read fasta file accepting "N" nucléotides
    bool res = true;

    if (oligoSetSize > 0)
    {
        oligos = oligoSet;
        nb_oligos = oligoSetSize;
    }
    else
    {
        res &= parse_oligo_file(oligoSetPath.c_str(), &oligos, &nb_oligos); // read oligo file loading also reverse oligo
    }

    if (res == false)
    {
        return nullptr;
    }

    for (int j = 0; j < nb_oligos; ++j)
    {
        Oligo o = Oligo(BiDirOligo(*(oligos[j])));
    }

    auto fgp = std::shared_ptr<PosFingerprint>(new PosFingerprint(seq, oligos, nb_oligos, resolution));

    if (addToEnv)
    {
        Env::instance().references().push_back(fgp);
    }

    //TODO : Set parameters via json
    auto dcg = DistClassGenerator(*fgp,
                                  distClassParams.maxFractionInClass,
                                  distClassParams.minBaseNmRate,
                                  distClassParams.maxBaseNmRate,
                                  distClassParams.classWidthMultiplifier,
                                  distClassParams.maxDistance);
    std::vector<int> *dists = dcg.computeDistDistribution();
    DistClasses *dcv = dcg.computeDistClasses(dists);
    auto dos = std::shared_ptr<DistOligoSequence> (new DistOligoSequence(fgp, *dcv));
    //Env::instance().referenceDsos().push_back(dos);
    delete dists;
    delete dcv;
    free(seq);
    return dos;
}
