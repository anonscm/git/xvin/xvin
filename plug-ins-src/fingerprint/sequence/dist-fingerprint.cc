#include "dist-fingerprint.hh"
#include "../../dna_utils/utils.h"
#include <limits>
#include <vector>
#ifndef NDEBUG
#include <iostream>
#endif

DistFingerprint::DistFingerprint()
    : Fingerprint(),
      size_(0),
      reverse_(0)
{
}
DistFingerprint::DistFingerprint(const DistFingerprint& other)
    : Fingerprint(other),
      size_(other.size_),
      reverse_(0)
{
}

DistFingerprint::DistFingerprint(const DistFingerprint& other, const OligoSet& oligos)
    : DistFingerprint(other)
{
    this->oligos_.clear();
    this->hybridationMap_.clear();

    for (Oligo oligo : oligos)
    {
        this->outcastOligos_.insert(oligo);
    }

    for (HybridationPair hyb : other.hybridationMap_)
    {
        if (oligos.find(hyb.second.theoricalOligo()) != oligos.end())
        {
            this->hybridationValue(hyb.first, hyb.second);
        }
    }
}

DistFingerprint::DistFingerprint(const OligoSet& oligos)
    : Fingerprint(oligos),
      size_(0),
      reverse_(0)
{
}

DistFingerprint::~DistFingerprint()
{
}

DistFingerprint *DistFingerprint::reverse(void) const
{
    DistFingerprint *rfgp = new DistFingerprint();
    #pragma omp critical (reverseChange)
    {
        if (reverse_ == 0)
        {
            rfgp->size_ = size_;
            rfgp->oligos_ = oligos_;
            rfgp->description_ = description_;
            rfgp->isReferenceFingerprint_ = false;
            int lastPos = hybridationMap_.rbegin()->first;

            for (auto hybridation : hybridationMap_)
            {
                HybridationPair newHyb = HybridationPair(lastPos - hybridation.first, hybridation.second);
                assert(newHyb.first >= 0);
                assert(!newHyb.second.empty());
                rfgp->hybridationMap_.insert(newHyb);
            }
        }
    }
    return rfgp;
}

bool DistFingerprint::hybridationValue(int pos, Hybridation& oligo)
{
    assert(!oligo.empty());
    Oligo theoricalOligo = oligo.theoricalOligo();

    if (pos == -1 && oligos_.find(theoricalOligo) == oligos_.end())
    {
        outcastOligos_.insert(theoricalOligo);
    }
    else
    {
        assert(pos >= 0);
        hybridationMap_.insert(HybridationPair(pos, oligo));
        outcastOligos_.erase(theoricalOligo);
        oligos_.insert(theoricalOligo);
        #pragma omp critical (reverseChange)
        {
            delete reverse_;
            reverse_ = 0;
        }
    }

    return true;
}


int DistFingerprint::nbHybridation() const
{
    return hybridationMap_.size();
}

Hybridation DistFingerprint::hybridationValue(int pos) const
{
    auto res = hybridationMap_.find(pos);

    if (res != hybridationMap_.end())
    {
        return (*res).second;
    }

    return Hybridation();
}

int DistFingerprint::size() const
{
    return size_;
}

bool DistFingerprint::size(int size)
{
    //FIXME test that there is no element in the map greater that the new size
    if (size > 0)
    {
        size_ = size;
        return true;
    }

    return false;
}
const HybridationMap& DistFingerprint::hybridationMap() const
{
    return hybridationMap_;
}

DistFingerprint  *DistFingerprint::subfgp(int begin, int size) const
{
    DistFingerprint *res = new DistFingerprint();
    HybridationMap::const_iterator beginIt = hybridationMap_.lower_bound(begin);
    HybridationMap::const_iterator endIt = hybridationMap_.lower_bound(begin + size - 1);
    res->size_ = endIt->first - beginIt->first; //size - ((*beginIt).first - begin);
    res->isReferenceFingerprint_ = isReferenceFingerprint_;

    for (HybridationMap::const_iterator It = beginIt; It != endIt; ++It)
    {
        HybridationPair hyb = *It;
        hyb.first = It->first - begin; //(*beginIt).first;
        assert(hyb.first >= 0 && hyb.first < size);
        res->hybridationValue(hyb.first, hyb.second);
    }

    res->originPosition(beginIt->first);
    res->sourceFgp_ = shared_from_this();
    //std::cerr << "begin "<< begin << " first hyb in fgp " << (*beginIt).first << " needlesize " << res->size_ << " nb hybridation " << res->nbHybridation() << std::endl;
    return res;
}

DistFingerprint::DistFingerprint(sqd *sequence, oligo_t **oligos, int nb_oligos)
    : DistFingerprint(sequence, oligos, nb_oligos, 1)
{
}

DistFingerprint::DistFingerprint(sqd *sequence, oligo_t **oligos, int nb_oligos, int resolution)
    : Fingerprint(resolution),
      hybridationMap_()
{
    isReferenceFingerprint_ = true;
    description_  = std::string(sequence->description);
    description_.erase(description_.find_last_not_of(" \n\r\t") + 1);
    size_ = sequence->totalSize;

    for (int i = 0; i < nb_oligos; ++i)
    {
        //std::cout << "oligo: " << (oligos[i])->seq << std::endl;
        //for (int j = 0; j < oligos[i]->behaviors_size; ++j)
        //{
        //    std::cout << "\t behavior: " << (oligos[i])->behaviors[j] << std::endl;
        //}
        Oligo theoricalOligo = Oligo(BiDirOligo(*(oligos[i])));
        Oligo actualOligo = Oligo();
        double oligoProba = oligos[i]->probability;

        if (oligos_.find(theoricalOligo) == oligos_.end())
        {
            int bidx = -1;

            do
            {
                bidx++;
                Hybridation hyb = Hybridation(theoricalOligo, actualOligo, oligoProba);
                assert(!hyb.empty());
                oligos_.insert(theoricalOligo);
                int cur_oligo_pos = -1;
                int prev_oligo_pos = -resolution_;

                do
                {
                    cur_oligo_pos = find_oligo_in_sequence(sequence, hyb.actualOligo().get().oligo().c_str(), cur_oligo_pos + 1,
                                                           1);

                    if (cur_oligo_pos != -1 && abs(cur_oligo_pos - prev_oligo_pos) >= resolution)
                    {
                        assert(cur_oligo_pos >= 0 && cur_oligo_pos < size_);
                        const HybridationMap::iterator curPosHybIt = hybridationMap_.find(cur_oligo_pos);

                        if (curPosHybIt == hybridationMap_.end() || hyb.actualOligo().get().isSubOligo(curPosHybIt->second.actualOligo().get()))
                        {
                            hybridationMap_.insert(HybridationPair(cur_oligo_pos, hyb));
                        }
                    }

                    prev_oligo_pos = cur_oligo_pos;
                }
                while (cur_oligo_pos > 0);

                do
                {
                    cur_oligo_pos = find_oligo_in_sequence(sequence, hyb.actualOligo().get().reverseOligo().c_str(),
                                                           cur_oligo_pos + 1, 1);

                    if (cur_oligo_pos != -1 && abs(cur_oligo_pos - prev_oligo_pos) >= resolution)
                    {
                        assert(cur_oligo_pos >= 0 && cur_oligo_pos < size_);
                        const HybridationMap::iterator curPosHybIt = hybridationMap_.find(cur_oligo_pos);

                        if (curPosHybIt == hybridationMap_.end() || hyb.actualOligo().get().isSubOligo(curPosHybIt->second.actualOligo().get()))
                        {
                            hybridationMap_.insert(HybridationPair(cur_oligo_pos, hyb));
                        }
                    }

                    prev_oligo_pos = cur_oligo_pos;
                }
                while (cur_oligo_pos > 0);

                if (bidx < oligos[i]->behaviors_size)
                {
                    theoricalOligo = Oligo(BiDirOligo(*(oligos[i])));
                    actualOligo = Oligo(BiDirOligo(oligos[i]->behaviors[bidx]));
                    oligoProba = oligos[i]->behaviors_proba[bidx];
                }
            }
            while (bidx < oligos[i]->behaviors_size);
        }
    }
    warning_message("Resolution handling buggy using DistFingerprint");
}

//HybridationMap& DistFingerprint::hybridationMap()
//{
//    return hybridationMap_;
//}

int DistFingerprint::coperture(void) const
{
    std::vector<bool> coperture;
    int nbCovered = 0;
    int oligoMaxSize = 0;

    for (auto oligo : oligos_)
    {
        if ((int) oligo.get().size() > oligoMaxSize)
        {
            oligoMaxSize = oligo.get().size();
        }
    }

    coperture.assign(size_ + oligoMaxSize, false);

    for (auto hybridation : hybridationMap_)
    {
        for (int i = 0; i < (int) hybridation.second.actualOligo().get().size(); i++)
        {
            if (hybridation.first + i < (int) coperture.size())
            {
                coperture[hybridation.first + i] = true;
            }
        }
    }

    #pragma omp parallel for reduction(+:nbCovered)

    for (int i = 0; i < (int) coperture.size(); i++)
    {
        nbCovered += coperture[i];
    }

    return nbCovered;
}

std::ostream& DistFingerprint::dumpSfgp(std::ostream& ostr) const
{
    ostr << description_ << std::endl;
    ostr << size_ << std::endl;

    for (auto hyb : hybridationMap_)
    {
        ostr << hyb.first << "\t" << hyb.second << std::endl;
    }

    return ostr;
}


DistFingerprint *DistFingerprint::stretch(double stretching) const
{
    DistFingerprint *res = new DistFingerprint(*this);
    res->hybridationMap_.clear();
    res->size_ = size_ * stretching;

    for (auto hyb : hybridationMap_)
    {
        HybridationPair hybStretch = HybridationPair(hyb.first * stretching, hyb.second);
        assert(hybStretch.first >= 0);
        res->hybridationMap_.insert(hybStretch);
    }

    res->isReferenceFingerprint_ = false;
    return res;
}
DistFingerprint *DistFingerprint::gaussianNoise(NoiseDistribution& noiseDistribution,
        UniformRandomEngine& randomEngine) const
{
    DistFingerprint *res = new DistFingerprint(*this);
    res->hybridationMap_.clear();

    for (auto hyb : hybridationMap_)
    {
        HybridationPair hybNoise = HybridationPair(hyb.first + std::round(noiseDistribution(randomEngine)), hyb.second);

        if (hybNoise.first < 0)
        {
            hybNoise.first = 0;
        }
        else if (hybNoise.first >= size_)
        {
            hybNoise.first = size_ - 1;
        }

        res->hybridationMap_.insert(hybNoise);
    }

    return res;
}
DistFingerprint *DistFingerprint::randomRemoveHybs(MissingHybDistribution mhd, double thres,
        UniformRandomEngine& randomEngine) const
{
    DistFingerprint *res = new DistFingerprint(*this);

    HybridationMap::iterator itr = res->hybridationMap_.begin();

    while (itr != res->hybridationMap_.end())
    {
        float hybRatio = mhd(randomEngine);
        if (hybRatio < thres)
        {
            itr = res->hybridationMap_.erase(itr);
        }
        else
        {
            ++itr;
        }
    }

    if (res->hybridationMap_.size() == 0)
    {
        free(res);
        return nullptr;
    }

    return res;
}

int DistFingerprint::firstHyb() const
{
    if (hybridationMap_.empty())
    {
        return -1;
    }

    return hybridationMap_.begin()->first;
}

int DistFingerprint::lastHyb() const
{
    if (hybridationMap_.empty())
    {
        return -1;
    }

    return hybridationMap_.rbegin()->first;
}


std::ostream& operator<< (std::ostream& ostr, const DistFingerprint& o)
{
    ostr << o.description_ << std::endl;
    ostr << "size: " << o.size_ << std::endl;
    ostr << "hybridation Count: " << o.nbHybridation() << std::endl;
    ostr << "{";

    for (auto hyb : o.hybridationMap_)
    {
        ostr << "[" << hyb.first << ": " << hyb.second << "],";
    }

    ostr << "}" << std::endl;
    return ostr;
}

void DistFingerprint::generateDs(d_s *dsd, int beginPos, int endPos) const
{
    HybridationMap::const_iterator posIt = hybridationMap_.lower_bound(beginPos);
    HybridationMap::const_iterator endIt = hybridationMap_.lower_bound(endPos);
    int dsSize = std::distance(posIt, endIt);
    build_adjust_data_set(dsd, dsSize, dsSize);
    dsd->nx = dsSize;
    dsd->ny = dsSize;

    for (int i = 0; posIt != endIt; ++posIt, ++i)
    {
        dsd->xd[i] = posIt->first;
        dsd->yd[i] = 0;
    }
}

int DistFingerprint::nextHybridizationPos(int pos) const
{
    HybridationMap::const_iterator posIt = hybridationMap_.upper_bound(pos);
    return posIt->first;
}

int DistFingerprint::previousHybridizationPos(int pos) const
{
    HybridationMap::const_iterator posIt = hybridationMap_.lower_bound(pos);
    --posIt;
    return posIt->first;
}
json DistFingerprint::dumpJson(bool dumpHybs) const
{
    json j;
    j["type"] = "DistFingerprint";
    j["desc"] = description_;
    j["isRef"] = isReferenceFingerprint_;
    j["origPos"] = originPosition_;
    j["resolution"] = resolution_;
    j["seqSize"] = size_;
    j["hybCount"] = hybridationMap_.size();
    j["hybs"] = json::array();

    if (generatorParams_)
    {
        j["genParams"]["size"] =  generationParams_->size;
        j["genParams"]["locus"] =  generationParams_->locus;
        j["genParams"]["noiseStdev"] =  generationParams_->noiseDistribution.stddev();
        //j["genParams"]["fromDesc"] =  generationParams_->fromDesc()
        j["genParams"]["baseNmRate"] =  generationParams_->baseNmRate;
        j["genParams"]["isReverse"] =  generationParams_->isReverse;
        j["genParams"]["missingHybs"] = generationParams_->missingHybs;
        // TODO missing / added distribution (not used right now)
    }

    if (dumpHybs)
    {
        for (auto it = hybridationMap_.cbegin(); it != hybridationMap_.cend(); ++it)
        {
            json hybj;
            hybj["pos"] = it->first;
            hybj["theoOligo"] = it->second.theoricalOligo().get().oligo();
            hybj["actOligo"] = it->second.actualOligo().get().oligo();
            hybj["proba"] = it->second.probability();
            j["hybs"].push_back(hybj);
        }
    }
    else
    {
        j["oligoBatchs"] = json::array();

        for (auto biol : oligos_)
        {
            auto oligo = biol.get();
            j["oligoBatchs"].push_back(json::array());
            j["oligoBatchs"].back().push_back(oligo.oligo());

            for (auto subOligo : oligo.behaviors())
            {
                j["oligoBatchs"].back().push_back(subOligo.get().oligo());
            }
        }
    }
    return j;
}
