#pragma once
#include "../define.hh"
#include "fingerprint.hh"

class OligoSequence
{
  public:
    OligoSequence(Fingerprint& fingerprint);
    virtual ~OligoSequence();

    char operator[] (int pos) const;
    char operator[] (int pos);
    size_t size() const;

  private:
    CharOligoMap charoligos_;
    std::vector<char> sequence_;
    bool isReference_;
    static const std::string oligosIds;
};
