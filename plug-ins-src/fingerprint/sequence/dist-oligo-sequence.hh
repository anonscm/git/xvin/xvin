#pragma once

#include "../define.hh"
#include "dist-oligo-alphabet.hh"
#include "fingerprint.hh"
#include <vector>

/**
 * @brief A fingerprint representation, considering distance between hybridization instead of pair
 * of hybridizations / positions
 */

class DistOligoSequenceFinder;

class DistOligoSequence
{

  public:
    friend DistOligoSequenceFinder;
    DistOligoSequence(std::shared_ptr<const Fingerprint> fingerprint, const DistClasses& distClassVector, float distClassBaseNmRate = 1.);
    ~DistOligoSequence();
    DistOligoAlphabet& operator[](const int index);
    const DistOligoAlphabet& operator[](const int index) const;
    unsigned int size() const;

    const std::string& description() const;
    std::ostream& dump(std::ostream& ostr);
    std::ostream& dumpClasses(std::ostream& ostr);
    int position(int index) const;
    //int underlyingSize() const;
    int originPosition() const;
    const OligoSet& oligoSet(void) const;
    std::shared_ptr<const Fingerprint>  parentFingerprint() const;
    DistClasses& distClasses();

    unsigned int oligoSeqSize() const;
    virtual json dumpJson(bool dumpHybs = true) const;
    DistOligoSequence stretch(float baseNmRate);
    int nbHybridation() const;
    std::shared_ptr<DistOligoSequence> reverse(void) const;


 
  private:
    float distClassBaseNmRate_ = 1.;
    OligoSet oligos_ = OligoSet();
    OligoSet outcastOligos_ = OligoSet();
    std::string description_ = "";
    bool isReference_ = true;
    int resolution_ = 0;
    int underlyingSize_ = 0;
    std::vector<DistOligoAlphabet> sequence_ = std::vector<DistOligoAlphabet>();
    std::vector<int> positions_ = std::vector<int> ();
    DistClasses distClasses_ = DistClasses();
    static std::string classChars_;
    int originPosition_ = 0;
    std::shared_ptr<const Fingerprint> parentFingerprint_ = nullptr;
};
