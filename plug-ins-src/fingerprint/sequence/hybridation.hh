#pragma once
#include "bidirectional-oligo.hh"
/**
 * @brief Represent a hybridization with notion of « Koff » (to be improved)
 */
class Hybridation
{
public:
    Hybridation ();
    Hybridation (const Hybridation& other);
    Hybridation (const Oligo& theoricalOligo);
    Hybridation (const Oligo& theoricalOligo, double probability);
    Hybridation (const Oligo& theoricalOligo, const Oligo& actualOligo, double probability);
    virtual ~Hybridation ();
    double probability() const;
    const Oligo& theoricalOligo() const;
    const Oligo& actualOligo() const;
    bool empty() const;
    
    bool operator< (const Hybridation &o1) const;

    friend std::ostream& operator<< (std::ostream& ostr, const Hybridation& o);
   // Archive
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version);

protected:
    Oligo theoricalOligo_;
    Oligo actualOligo_;
    double probability_;
    bool isEmpty_;
};


#include "hybridation.hxx"
