#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif
#include "../sequence/fingerprint.hh"
#include "../sequence/dist-oligo-sequence.hh"
#include "../search/abstract-search.hh"
#include "json.hpp"

#include<memory>
template<typename SearchEngine>
struct HaystackImporter
{
    std::string sequencePath = "";
    oligo_t **oligoSet = nullptr;
    int oligoSetSize = 0;
    std::string oligoSetPath = "";
    float resolution = 3;

    struct DistClassParams
    {
        // distClass
        float maxFractionInClass = 0.1;
        float minBaseNmRate = MIN_BASE_NM_RATE;
        float maxBaseNmRate = MAX_BASE_NM_RATE;
        float classWidthMultiplifier = 2.5;
        int maxDistance = 3000;
    };

    DistClassParams distClassParams;

    std::shared_ptr<typename SearchEngine::input_type> importHaystack(bool addToEnv);
};
