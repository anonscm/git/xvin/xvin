/**
 * @file dist-fingerprint.hh
 * @brief 
 * @author François-Xavier Lyonnet du Moutier
 * @version 
 * @date 2016-02-15
 */

#pragma once
#include "../define.hh"
#include "fingerprint.hh"
//#include "../serialization.hh"
#include <map>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/utility.hpp>


/**
 * @brief A fingerprint representation using an Hybridization map.
 */
class DistFingerprint : public Fingerprint, public std::enable_shared_from_this<Fingerprint>
{
    friend class boost::serialization::access;
    friend class BasicFinder;

  public:
    DistFingerprint();
    DistFingerprint(const DistFingerprint& other);
    DistFingerprint(const DistFingerprint& other, const OligoSet& oligo);
    DistFingerprint(const OligoSet& oligo);
    DistFingerprint(sqd *sequence, oligo_t **oligos, int nb_oligos);
    DistFingerprint(sqd *sequence, oligo_t **oligos, int nb_oligos, int resolution);
    virtual ~DistFingerprint();
    virtual bool hybridationValue(int pos, Hybridation& oligo);

    virtual Hybridation hybridationValue(int pos) const;
    virtual int nextHybridizationPos(int pos) const;
    virtual int previousHybridizationPos(int pos) const;
    virtual int firstHyb() const;
    virtual int lastHyb() const;
    virtual int nbHybridation() const;
    virtual int size() const;
    bool size(int size);
    const HybridationMap& hybridationMap() const;
    //HybridationMap& hybridationMap(void);

    virtual DistFingerprint *subfgp(int begin, int size) const;
    virtual DistFingerprint *stretch(double streching) const;
    virtual DistFingerprint *reverse(void) const;
    virtual DistFingerprint *gaussianNoise(NoiseDistribution& noiseDistribution, UniformRandomEngine &randomEngine) const;
DistFingerprint *randomRemoveHybs(MissingHybDistribution mhd, double thres,
        UniformRandomEngine& randomEngine) const;
    virtual json dumpJson(bool dumpHybs = true) const;
    virtual void generateDs(d_s *dsd, int beginPos, int endPos) const;
    virtual int  coperture(void) const;
    bool isMatchEqual(int haystackPos, Fingerprint& needle, int needlePos);
    virtual std::ostream& dumpSfgp (std::ostream& ostr) const;

    friend std::ostream& operator<< (std::ostream& ostr, const DistFingerprint& o);

    // Archive
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version);
    //template<class Archive>
    //void save(Archive& ar, const unsigned int version)const;
    //template<class Archive>
    //void load(Archive& ar, const unsigned int version);

    //BOOST_SERIALIZATION_SPLIT_MEMBER();

  private:
    int size_;
    HybridationMap hybridationMap_;
    DistFingerprint *reverse_;
//    OligoPosDistSet oligoPosDistMap_;
//    float oligoPosDistMinStretching_;
//    float oligoPosDistMaxStretching_;
};

#include "dist-fingerprint.hxx"

