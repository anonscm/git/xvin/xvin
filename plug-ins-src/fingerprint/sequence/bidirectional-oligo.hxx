#pragma once
#include <algorithm>
inline BiDirOligo::BiDirOligo()
    :oligo_ (""),
    reverseOligo_ ("")
{
}

inline BiDirOligo::BiDirOligo(const BiDirOligo& oligo)
:oligo_ (oligo.oligo_),
    reverseOligo_ (oligo.reverseOligo_),
    behaviors_(oligo.behaviors_),
    meltingTemp_(oligo.meltingTemp_)
{

}
inline BiDirOligo::BiDirOligo(int /*dummy*/)
:BiDirOligo()
{
}

inline BiDirOligo::BiDirOligo(const char* oligo)
{
    std::string o = std::string(oligo);
    o.erase(o.begin(),find_if_not(o.begin(),o.end(),[](int c){return isspace(c);}));
    o.erase(find_if_not(o.rbegin(),o.rend(),[](int c){return isspace(c);}).base(), o.end());

    char *rev = dna_complementary(o.c_str());
    std::string o2 = std::string(rev);
    free(rev);
    rev = 0;
    oligo_ = std::max(o, o2);
    reverseOligo_ = std::min(o, o2);
}

inline BiDirOligo::BiDirOligo(std::string& oligo)
: BiDirOligo(oligo.c_str())
{
}

inline BiDirOligo::BiDirOligo (oligo_t& oligo)
:BiDirOligo(oligo.seq)
{
    for (int i = 0; i < oligo.behaviors_size; ++i)
    {
        behaviors_.push_back(Oligo(BiDirOligo(oligo.behaviors[i])));
    }
}

inline BiDirOligo::~BiDirOligo()
{
}

inline bool BiDirOligo::operator<(const BiDirOligo &o1) const
{
    return (oligo_ < o1.oligo_);
}
inline bool BiDirOligo::operator>(const BiDirOligo &o1) const
{
    return (oligo_ > o1.oligo_);
}

inline bool BiDirOligo::operator==(const BiDirOligo &o1) const
{
    return (this->oligo_ == o1.oligo_ || this->oligo_ == o1.reverseOligo_);
}

inline BiDirOligo& BiDirOligo::operator =(const BiDirOligo& other)
{
    oligo_ = other.oligo_;
    reverseOligo_ = other.reverseOligo_;
    return *this;
}

inline  BiDirOligo& BiDirOligo::operator =(const std::string& oligo)
{
    std::string o = oligo;
    o.erase(o.begin(),find_if_not(o.begin(),o.end(),[](int c){return isspace(c);}));
    o.erase(find_if_not(o.rbegin(),o.rend(),[](int c){return isspace(c);}).base(), o.end());

    char *rev = dna_complementary(o.c_str());
    std::string o2 = std::string(rev);
    free(rev);
    oligo_ = std::max(o, o2);
    reverseOligo_ = std::min(o, o2);
    return *this;

}

inline BiDirOligo& BiDirOligo::operator =(const char* oligo)
{
    std::string o = std::string(oligo);
    o.erase(o.begin(),find_if_not(o.begin(),o.end(),[](int c){return isspace(c);}));
    o.erase(find_if_not(o.rbegin(),o.rend(),[](int c){return isspace(c);}).base(), o.end());

    char *rev = dna_complementary(o.c_str());
    std::string o2 = std::string(rev);
    free(rev);
    oligo_ = std::max(o, o2);
    reverseOligo_ = std::min(o, o2);
    return *this;
}
inline const std::string& BiDirOligo::oligo(void) const
{
    return oligo_;
}

inline std::vector<Oligo>& BiDirOligo::behaviors()
{
    return behaviors_;
}
inline const std::string& BiDirOligo::reverseOligo(void) const
{
    return reverseOligo_;
}

inline int BiDirOligo::size(void)
{
    return (int) oligo_.size();
}

inline int BiDirOligo::size(void) const
{
    return (int) oligo_.size();
}
inline bool BiDirOligo::empty(void) const
{
    return oligo_ == "";
}
inline bool BiDirOligo::isSubOligo(const BiDirOligo& other) const
{
    return oligo_.find(other.oligo_) != std::string::npos || oligo_.find(other.reverseOligo_) != std::string::npos;
}

template<class Archive>
inline void BiDirOligo::save(Archive& ar, const unsigned int /*version*/) const
{
    ar << oligo_;
}

template<class Archive>
inline void BiDirOligo::load(Archive& ar, const unsigned int /*version*/)
{
    std::string s;
    ar >> s;

    char *rev = dna_complementary(s.c_str());
    std::string o2 = std::string(rev);
    free(rev);
    oligo_ = s;
    reverseOligo_ = o2;
}

template<class Archive>
inline void BiDirOligo::serialize(Archive & ar, const unsigned int version)
{
    boost::serialization::split_member(ar, *this, version);
}

namespace std
{
    template<> struct hash<BiDirOligo>
    {
        size_t operator()(const BiDirOligo& s) const 
        {
            return hash<string>()(s.oligo());
        }
    };
}
namespace std
{
    template<> struct hash<Oligo>
    {
        size_t operator()(const Oligo& s) const 
        {
            return s.get_key(); 
        }
    };
}
