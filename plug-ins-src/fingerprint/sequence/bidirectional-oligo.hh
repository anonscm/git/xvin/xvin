#pragma once
#include "../../dna_utils/utils.h"
#include "oligo-flyweight.hh"
#include <string>
#include <iostream>
#include <functional>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>


class BiDirOligo;
typedef OligoFlyweight<BiDirOligo> Oligo;

/**
 * @brief A representation of an oligo and its interaction with an opening and closing hairpin. In
 * this representation, the oligo DNA direction is not taken into account from the outside the
 * object. since the oligo can hybridize with the two strand of the hairpin.
 *
 * Since hybridization with missmatch is possible with high melting temperature oligos, we considers
 * those possible missmatch with oligo « behaviours », with is a list of missmatched sequence which
 * can allow hybridization.
 * This representation of missmatch is weak since it doesn't not take into consideration the actual
 * experiment temperature, salt condition, etc. which impact drastically the missmatched hybridization.
 */
class BiDirOligo
{
    public:
        friend class boost::serialization::access;
        /**
         * @brief Dummy empty constructor for serialization.
         *
         * @param dummy
         */
        BiDirOligo (int dummy);
        BiDirOligo ();
        BiDirOligo (std::string& oligo);
        BiDirOligo (oligo_t& oligo);
        BiDirOligo (const char* oligo);
        BiDirOligo (const BiDirOligo& oligo);

        virtual ~BiDirOligo ();
        bool operator== (const BiDirOligo &o1) const;
        bool operator< (const BiDirOligo &o1) const;
        bool operator> (const BiDirOligo &o1) const;
        BiDirOligo& operator= (const BiDirOligo& other);
        BiDirOligo& operator= (const std::string& other);
        BiDirOligo& operator= (const char* other);
        
        const std::string& oligo(void) const;
        const std::string& reverseOligo(void) const;
        int size(void);
        int size(void) const;
        bool empty(void) const;
        bool isSubOligo(const BiDirOligo& other) const;
        float meltingTemp(void);
        float meltingTemp(void) const;
        std::vector<Oligo>& behaviors();

        friend std::ostream& operator<< (std::ostream& ostr, const BiDirOligo& o);

        template<class Archive>
        void serialize(Archive & ar, const unsigned int version);
        template<class Archive>
        void save(Archive& ar, const unsigned int /*version*/) const;
        template<class Archive>
        void load(Archive& ar, const unsigned int /*version*/);

    protected:
        std::string oligo_;
        std::string reverseOligo_;
        std::vector<Oligo> behaviors_; 
        float meltingTemp_;
};

namespace std 
{
    template<> struct hash<Oligo>;
    template<> struct hash<BiDirOligo>;
}

#include "bidirectional-oligo.hxx"
