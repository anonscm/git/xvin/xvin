#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif
#include "fingerprint.hh"
#include <vector>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>

namespace chiSquareMethod
{
class ChiSquareSearch;
};

class PosFingerprint : public Fingerprint, public std::enable_shared_from_this<PosFingerprint>
{
    friend class boost::serialization::access;
    friend class RankFinder;
    friend class chiSquareMethod::ChiSquareSearch;

  public:
    enum class Orientation
    {
        HORIZONTAL,
        VERTICAL
    };
    PosFingerprint();
    PosFingerprint(const PosFingerprint& posfgp);
    PosFingerprint(const OligoSet& oligo);
    /**
     * @brief Create a fingerprint from a sequence and oligos
     *
     * @param sequence
     * @param oligos oligos selected to create fingerprint
     * @param num_oligos number of oligos selected
     *
     */
    PosFingerprint(sqd *sequence, oligo_t **oligos, int nb_oligos);
    PosFingerprint(sqd *sequence, oligo_t **oligos, int nb_oligos, int resolution);
    PosFingerprint(json& j, oligo_t **oligos, int nb_oligos);
    virtual ~PosFingerprint();
    virtual int nbHybridation() const;
    //virtual bool nbHybridation(int nbHybridation);
    virtual bool hybridationValue(int pos, Hybridation& oligo);
    virtual int firstHyb() const;
    virtual int lastHyb() const;
    virtual Hybridation hybridationValue(int pos) const;
    virtual int nextHybridizationPos(int pos) const;
    virtual int previousHybridizationPos(int pos) const;
    //virtual Hybridation operator[] (const int pos);
    virtual int size() const;
    bool size(int size);

    virtual PosFingerprint *subfgp(int begin, int size) const;
    //virtual PosFingerprint *reverse(void);
    virtual PosFingerprint *reverse(void) const;
    virtual PosFingerprint *stretch(double streching) const;
    virtual PosFingerprint *gaussianNoise(NoiseDistribution& noiseDistribution, UniformRandomEngine& randomEngine) const;
    virtual PosFingerprint *randomRemoveHybs(MissingHybDistribution mhd, double thres, UniformRandomEngine& randomEngine) const;
    virtual void generateDs(d_s *dsd, int beginPos, int endPos) const;
    virtual void generateLineDs(d_s *dsd, int beginPos, int endPos, int offset, int lineLength,
                                Orientation orient = Orientation::HORIZONTAL) const;
    virtual json dumpJson(bool dumpHybs = true) const;
    virtual int coperture(void) const;
    virtual std::ostream& dumpSfgp(std::ostream& ostr) const;

    int countHyb(int beginPos, int endPos) const;
    OligoPosDistMap *oligoPosDistMap(float minStretching, float maxStretching, float noiseDeviation) const;

    HybridationVector& hybridationsVector();

    const PositionVector& positions(void) const;

    // Archive
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version);

    // explicit instanciation of function for dlopen
    //template
    //void serialize<boost::archive::text_oarchive>(boost::archive::text_oarchive & ar, const unsigned int version);

  private:
    int underlyingSequenceSize_ = 0;
    PositionVector positions_;
    HybridationVector hybridations_;
};

#include "pos-fingerprint.hxx"
