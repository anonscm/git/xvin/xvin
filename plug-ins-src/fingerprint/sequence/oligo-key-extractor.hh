#pragma once
#include "../../dna_utils/utils.h"
#include <string>
#include <iostream>
#include <boost/bimap.hpp>

class BiDirOligo;
class Hybridation;

template<class OligoType>
class OligoGlobalIndex
{

    typedef boost::bimap<OligoType, int> OligoBimap;

    public:
    virtual ~OligoGlobalIndex();

    static OligoGlobalIndex& instance();
    const int& getIndex(const OligoType& oligo) ;
    bool oligoExists(int index) const;
    const OligoType& getOligo(int index);

    
    friend std::ostream& operator<< (std::ostream& ostr, const OligoGlobalIndex<BiDirOligo>& o);
    friend std::ostream& operator<< (std::ostream& ostr, const OligoGlobalIndex<Hybridation>& o);

    private:
    OligoGlobalIndex();

    static OligoGlobalIndex* instance_;
    OligoBimap oligoGlobalMap_;
    int lastIndex_;
};

template<class OligoType>
struct OligoKeyExtractor
{
    const int& operator()(OligoType x) const;
};

#include "oligo-key-extractor.hxx"
