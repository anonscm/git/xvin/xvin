#include "fingerprint-sample-generator.hh"
#include "../params/sample-generation-params.hh"
#include <chrono>

FingerprintSampleGenerator::FingerprintSampleGenerator()
    : params_(),
      randomSeed_(std::chrono::high_resolution_clock::now().time_since_epoch().count())

{
    randomEngine_ = UniformRandomEngine(randomSeed_);
}

FingerprintSampleGenerator::~FingerprintSampleGenerator()
{
}
FingerprintVector FingerprintSampleGenerator::generate(int numSample, const Fingerprint* ref, int locus)
{
    std::uniform_int_distribution<int> fgpDistribution = std::uniform_int_distribution<int> (0, references_.size() - 1);
    FingerprintVector res;
    std::shared_ptr<Fingerprint> curFgp = 0;
    int retrieve = 0;
    int i = 0;

    while (i < numSample)
    {
        const Fingerprint *fgp;

        if (ref == nullptr)
        {
            int fgpIdx = fgpDistribution(randomEngine_);
            fgp = references_[fgpIdx].get();
        }
        else
        {
            fgp = ref;
        }


        if (locus == -1)
        {
            LocusDistribution locusDistribution(0, fgp->size() - 1);
            locus = locusDistribution(randomEngine_);
        }

        int size = params_.sizeDistribution(randomEngine_);
        bool isReverse = !params_.directionDistribution(randomEngine_);
        float baseNmRate = params_.baseNmRateDistribution(randomEngine_);
        curFgp = generateOne(*fgp, locus, size, isReverse, baseNmRate);

        if (!curFgp || curFgp->nbHybridation() < params_.minHybridization)
        {
            retrieve++;

            if (retrieve > maxRetrieve_)
            {
                warning_message("Unable to generate with %d hybridization", params_.minHybridization);
                return FingerprintVector();
            }
        }
        else
        {
            i++;
            retrieve = 0;
            res.push_back(curFgp);
        }
    }

    return res;
}

std::shared_ptr<Fingerprint> FingerprintSampleGenerator::generateOne(const Fingerprint& ref, int locus, int size,
        bool isReverse,
        float baseNmRate)
{
    Fingerprint *resFgp = 0;
    Fingerprint *tmp = 0;
    resFgp = ref.subfgp(locus, size);
    SampleGenerationParams sgp;
    sgp.locus = locus;
    sgp.size = size;
    sgp.isReverse = isReverse;
    sgp.baseNmRate = baseNmRate;
    sgp.noiseDistribution = params_.noiseDistribution;
    sgp.fromDesc = ref.description();


    if (resFgp == nullptr || resFgp->nbHybridation() <= 0)
    {
        return nullptr;
    }

    if ((params_.missingHybDistribution.p() != 1) && params_.missingHybDetectionThreshold > 0)
    {
        tmp = resFgp->randomRemoveHybs(params_.missingHybDistribution, params_.missingHybDetectionThreshold, randomEngine_);
        sgp.missingHybs = resFgp->nbHybridation() - tmp->nbHybridation();    
        delete resFgp;
        resFgp = tmp;

    }
    if (resFgp == nullptr)
        return nullptr;
    if (!(params_.noiseDistribution.mean() == 0.) || !(params_.noiseDistribution.stddev() == 0.))
    {
        tmp = resFgp->gaussianNoise(params_.noiseDistribution, randomEngine_);
        delete resFgp;
        resFgp = tmp;
    }

    assert(resFgp != nullptr);

    if (baseNmRate != 1. && baseNmRate > 0)
    {
        tmp = resFgp->stretch(baseNmRate);
        delete resFgp;
        resFgp = tmp;
    }

    assert(resFgp != nullptr);

    if (isReverse)
    {
        tmp = resFgp->reverse();
        delete resFgp;
        resFgp = tmp;
    }

    assert(resFgp != nullptr);

    //TODO add / remove peaks
    std::string desc = ref.description();
    desc.erase(std::remove(desc.begin(), desc.end(), '\n'), desc.end());
    std::stringstream ss;
    ss << "barcode from :" << desc << " locus: " << resFgp->originPosition()
       << " isReversed: " << isReverse << " noise amplitude: " << params_.noiseDistribution.mean()
       << " baseNmRate: " << baseNmRate << std::endl;
    resFgp->description(ss.str());
    resFgp->generatorParams(std::shared_ptr<SampleGeneratorParams>(new SampleGeneratorParams(params_)));
    resFgp->generationParams(std::shared_ptr<SampleGenerationParams>(new SampleGenerationParams(sgp)));
    return std::shared_ptr<Fingerprint> (resFgp);
}
