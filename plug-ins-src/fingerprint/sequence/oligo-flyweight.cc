#include "oligo-flyweight.hh"
#include "hybridation.hh"
template<class OligoType>
OligoFlyweight<OligoType>::OligoFlyweight()
{
    key_ = OligoGlobalIndex<OligoType>::instance().getIndex(OligoType());
}

template<class OligoType>
OligoFlyweight<OligoType>::OligoFlyweight(OligoType val)
{
    key_ = OligoGlobalIndex<OligoType>::instance().getIndex(val);
}

template<class OligoType>
OligoFlyweight<OligoType>::~OligoFlyweight()
{
}

template<class OligoType>
bool OligoFlyweight<OligoType>::operator<(const OligoFlyweight &o1) const
{
    return key_ < o1.key_;
}

template<class OligoType>
bool OligoFlyweight<OligoType>::operator==(const OligoFlyweight &o1) const
{
    return key_ == o1.key_;
}
template<class OligoType>

bool OligoFlyweight<OligoType>::operator!=(const OligoFlyweight &o1) const
{
    return key_ != o1.key_;
}

template<class OligoType>
bool OligoFlyweight<OligoType>::operator>(const OligoFlyweight &o1) const
{
    return key_ > o1.key_;
}

template<class OligoType>
int OligoFlyweight<OligoType>::get_key() const
{
    return key_;
}
template<class OligoType>
void OligoFlyweight<OligoType>::set_key(int key)
{
    if (OligoGlobalIndex<OligoType>::instance().oligoExists(key))
        key_ = key;
}

template<class OligoType>
const OligoType& OligoFlyweight<OligoType>::get() const
{
    return OligoGlobalIndex<OligoType>::instance().getOligo(key_);
}

template class OligoFlyweight<BiDirOligo>;
template class OligoFlyweight<Hybridation>; //Force instanciation

std::ostream& operator<< (std::ostream& ostr, const OligoFlyweight<BiDirOligo>& o)
{
    ostr << o.get();
    return ostr;
}
