#include "oligo-sequence.hh"

const std::string OligoSequence::oligosIds = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

OligoSequence::OligoSequence(Fingerprint& fingerprint)
    : isReference_(fingerprint.isReferenceFingerprint())
{
    OligoSet fgpOligos = fingerprint.oligoSet();
    std::map<Oligo, char> oligoChar = std::map<Oligo, char>();
    int i = 0;

    for (Oligo oligo : fgpOligos)
    {
        charoligos_[i] = oligo;
        oligoChar[oligo] = i;
        ++i;
    }

    int pos = -1;
    pos = fingerprint.nextHybridizationPos(pos);

    while (pos != -1)
    {
        Oligo oligo = fingerprint.hybridationValue(pos).actualOligo();
        sequence_.push_back(oligoChar[oligo]);
        pos = fingerprint.nextHybridizationPos(pos);
    }
}

OligoSequence::~OligoSequence()
{
}

char OligoSequence::operator[](int pos)
{
    if (pos > 0 && pos < (int) sequence_.size())
    {
        return sequence_[pos];
    }
    return -1;
}

size_t OligoSequence::size() const
{
	return sequence_.size();
}

