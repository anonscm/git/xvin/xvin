#pragma once
#include <iostream>

template<class Archive>
void DistFingerprint::serialize(Archive& ar, const unsigned int /*version*/)
{
    //std::map<int, boost::flyweight<std::string>> lol;
    ar & boost::serialization::base_object<Fingerprint>(*this);
    ar & size_;
    ar & hybridationMap_;

    std::cout << description_ << std::endl;
}


//template<class Archive>
//void DistFingerprint::save(Archive& ar, const unsigned int /*version*/) const
//{
//    ar & boost::serialization::base_object<Fingerprint>(*this);
//    ar & size_;
//
//    int sz = hybridationMap_.size();
//    ar << sz;
//    for (HybridationPair hybridation : hybridationMap_)
//    {
//        ar << hybridation.first;
//        ar << hybridation.second.get().oligo();
//    }
//}
//
//template<class Archive>
//void DistFingerprint::load(Archive& ar, const unsigned int /*version*/)
//{
//    ar & boost::serialization::base_object<Fingerprint>(*this);
//    ar & size_;
//    int size = 0;
//    int pos = 0;
//    BiDirOligo oligo = BiDirOligo("");
//
//    ar >> size;
//    std::cout << size << std::endl;
//
//    for (int i = 0; i < size; ++i)
//    {
//        ar >> pos;
//        ar >> oligo;
//        std::cout << pos << " " << oligo << std::endl;
//        hybridationMap_.insert(HybridationPair(pos,Oligo(oligo)));
//    }
//}
