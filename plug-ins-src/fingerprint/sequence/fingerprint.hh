#pragma once
#ifndef XV_WIN32
# include "config.h"
#endif
#include "../define.hh"
#include <xvin.h>
#include "../../fasta_utils/fasta_utils.h"
#include "hybridation.hh"
#include "../params/sample-generator-params.hh"
#include "../params/sample-generation-params.hh"
//#include "../serialization.hh"
#include <string>
#include <map>
#include <vector>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/string.hpp>
#include "json.hpp"
using json = nlohmann::json;

/**
 * @brief abstract class to represent oligo hybridization on a DNA molecule.
 */
class Fingerprint 
{
    friend class boost::serialization::access;
    friend class BasicFinder;
    friend class RankFinder;

    public: 
    Fingerprint();
    Fingerprint(const Fingerprint& fgp);
    Fingerprint(int resolution);
    Fingerprint(const OligoSet& oligos);
    virtual ~Fingerprint();
 
    // Getter
    virtual int nbHybridation() const = 0;
    virtual Hybridation hybridationValue(int pos) const = 0; 
    virtual int nextHybridizationPos(int pos) const = 0;
    virtual int previousHybridizationPos(int pos) const = 0;
    virtual int firstHyb() const = 0;
    virtual int lastHyb() const = 0;
    virtual int size() const = 0;
    bool isReferenceFingerprint() const;
    const std::string& description() const;
    const OligoSet& oligoSet(void) const;
    int oligosCount() const;
    bool hasOligo(Oligo& o) const;
    int originPosition() const;
    virtual int coperture(void) const = 0;

    // Setter
    virtual bool hybridationValue(int pos, Hybridation& oligo) = 0;
    //void oligo(int index, std::string& sequence);
    void description(const std::string& desc);
    void description(const char * desc);
    float resolution() const;
    void resolution(float resolution);
    void originPosition(int originPosition);
    void generatorParams(std::shared_ptr<const SampleGeneratorParams> params);
    void generationParams(std::shared_ptr<const SampleGenerationParams> params);
    std::shared_ptr<const SampleGenerationParams> generationParams() const;
    std::shared_ptr<const SampleGeneratorParams> generatorParams() const;

    // Methods
    virtual Fingerprint *subfgp(int begin, int size) const = 0;
    virtual Fingerprint *reverse(void) const = 0;
    virtual Fingerprint *stretch(double streching) const = 0;
    virtual Fingerprint *gaussianNoise(NoiseDistribution& noiseDistribution, UniformRandomEngine& randomEngine) const = 0;
    virtual Fingerprint *randomRemoveHybs(MissingHybDistribution mhd, double thres, UniformRandomEngine& randomEngine) const = 0;
    virtual void generateDs(d_s *dsd, int beginPos, int endPos) const = 0;
    virtual json dumpJson(bool dumpHybs = true) const = 0;
    static int stretchedPosition(int pos, double stretch);
    static double stretchedPositiond(int pos, double stretch);

    // Archive
    virtual std::ostream& dumpSfgp (std::ostream& ostr) const = 0;
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version);
    //template<class Archive>
    //void save(Archive& ar, const unsigned int version)const;
    //template<class Archive>
    //void load(Archive& ar, const unsigned int version);

    //BOOST_SERIALIZATION_SPLIT_MEMBER();


    protected:
    OligoSet oligos_;
    OligoSet outcastOligos_; 
    std::string description_;
    bool isReferenceFingerprint_;
    int originPosition_ = -1;
    float resolution_;
    std::weak_ptr<const Fingerprint> sourceFgp_;
    std::shared_ptr<const SampleGeneratorParams> generatorParams_ = 0;
    std::shared_ptr<const SampleGenerationParams> generationParams_ = 0;



};

typedef std::vector<std::shared_ptr<Fingerprint>> FingerprintVector;

BOOST_SERIALIZATION_ASSUME_ABSTRACT(Fingerprint)
//BOOST_SERIALIZATION_SPLIT_FREE(Fingerprint)
BOOST_CLASS_VERSION(Fingerprint, 3)
#include "fingerprint.hxx"
