#pragma once

inline int FingerprintSampleGenerator::minHybridization() const
{
	return params_.minHybridization;
}

inline void FingerprintSampleGenerator::minHybridization(int minHyb)
{
    params_.minHybridization = minHyb;
}

inline void FingerprintSampleGenerator::references(FingerprintVector& references)
{
    references_ = references;
}

inline const BaseNmRateDistribution &FingerprintSampleGenerator::baseNmRateDistribution() const
{
	return params_.baseNmRateDistribution;
}

inline const NoiseDistribution &FingerprintSampleGenerator::noiseDistribution() const
{
	return params_.noiseDistribution;
}

inline const SizeDistribution &FingerprintSampleGenerator::sizeDistribution() const
{
	return params_.sizeDistribution;
}

inline const AddedHybDistribution &FingerprintSampleGenerator::addedHybDistribution() const
{
	return params_.addedHybDistribution;
}

inline const MissingHybDistribution &FingerprintSampleGenerator::missingHybDistribution() const
{
	return params_.missingHybDistribution;
}

inline float FingerprintSampleGenerator::probReverse() const
{
	return (1. - params_.directionDistribution.p());
}

inline void FingerprintSampleGenerator::addedHybDistribution(float meanAdded)
{
    params_.addedHybDistribution = AddedHybDistribution (meanAdded);
}

inline void FingerprintSampleGenerator::baseNmRateDistribution(float meanDistribution, float stdDevDistribution)
{
    params_.baseNmRateDistribution = BaseNmRateDistribution (meanDistribution, stdDevDistribution);
}

inline void FingerprintSampleGenerator::directionDistribution(float prob)
{
    params_.directionDistribution = DirectionDistribution(prob);
}

inline void FingerprintSampleGenerator::missingHybDistribution(float meanMissing)
{
    params_.missingHybDistribution = MissingHybDistribution (meanMissing);
}

inline void FingerprintSampleGenerator::noiseDistribution(float meanNoise, float stdDevNoise)
{
    params_.noiseDistribution = NoiseDistribution (meanNoise, stdDevNoise);
}

inline void FingerprintSampleGenerator::sizeDistribution(int meanSize, int stdDevSize)
{
    params_.sizeDistribution = SizeDistribution (meanSize, stdDevSize);
}
    

inline void FingerprintSampleGenerator::randomSeed(int randomSeed)
{
    randomSeed_ = randomSeed;
    randomEngine_ = UniformRandomEngine (randomSeed);
}

inline int FingerprintSampleGenerator::randomSeed() const
{
    return randomSeed_;

}

inline void FingerprintSampleGenerator::params(SampleGeneratorParams& params)
{
    params_ = params;
}
inline SampleGeneratorParams &FingerprintSampleGenerator::params()
{
    return params_;
}
