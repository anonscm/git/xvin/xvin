#pragma once
#include <cmath>
#include <limits>
inline int Fingerprint::stretchedPosition(int pos, double stretch)
{
    return std::round(pos * stretch);
}

inline double Fingerprint::stretchedPositiond(int pos, double stretch)
{
    return pos * stretch;
}

inline const std::string& Fingerprint::description() const
{
    return description_;
}

inline void Fingerprint::description(const std::string& desc)
{
    description_ = desc;
}

inline void Fingerprint::description(const char *desc)
{
    if (desc != 0)
    {
        description_ = std::string(desc);
    }
}

inline int Fingerprint::originPosition() const
{
    return originPosition_;
}

//inline void Fingerprint::oligo(Oligo oligo)
//{
//    oligos_[index] = sequence;
//}

inline int Fingerprint::oligosCount() const
{
    return oligos_.size();
}


inline const OligoSet& Fingerprint::oligoSet(void) const
{
    return oligos_;
}

inline bool Fingerprint::hasOligo(Oligo& o) const
{
    return oligos_.find(o) != oligos_.end();
}

inline bool Fingerprint::isReferenceFingerprint(void) const
{
    return isReferenceFingerprint_;
}

template<class Archive>
void Fingerprint::serialize(Archive& ar, const unsigned int version)
{
    ar& description_;
    ar& oligos_;

    if (version > 1)
    {
        ar& outcastOligos_;
    }

    ar& isReferenceFingerprint_;

    if (version > 2)
    {
        ar& originPosition_;
        ar& resolution_;
    }
}

inline float Fingerprint::resolution() const
{
    return resolution_;
}
inline void Fingerprint::resolution(float resolution)
{
    if (resolution >= 1)
    {
        resolution_ = resolution;
    }
}

inline void Fingerprint::generatorParams(std::shared_ptr<const SampleGeneratorParams> params)
{
    generatorParams_ = params;
}

inline void Fingerprint::generationParams(std::shared_ptr<const SampleGenerationParams> params)
{
    generationParams_ = params;
}


inline std::shared_ptr<const SampleGenerationParams> Fingerprint::generationParams() const
{
    return generationParams_;
}

inline std::shared_ptr<const SampleGeneratorParams> Fingerprint::generatorParams() const
{
    return generatorParams_;
}

//template<class Archive>
//void Fingerprint::save(Archive& ar, const unsigned int /*version*/) const
//{
//    int s = oligos_.size();
//    ar & description_;
//    ar & minStretchingRate_;
//    ar & maxStretchingRate_;
//    ar & s;
//
//    for (Oligo oligo : oligos_)
//    {
//        ar << oligo;
//    }
//}
//
//template<class Archive>
//void Fingerprint::load(Archive& ar, const unsigned int /*version*/)
//{
//    std::string oligo = "";
//    int size = 0;
//
//    ar & description_;
//    ar & minStretchingRate_;
//    ar & maxStretchingRate_;
//    ar & size;
//
//    for (int i = 0; i < size; ++i)
//    {
//        ar >> oligo;
//        oligos_.insert(Oligo(BiDirOligo(oligo)));
//    }
//}
