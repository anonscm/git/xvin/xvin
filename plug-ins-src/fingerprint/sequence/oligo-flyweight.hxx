#pragma once
template<class OligoType>
template<class Archive>
void OligoFlyweight<OligoType>::save(Archive& ar, const unsigned int /*version*/) const
{
    ar << get();
}

template<class OligoType>
template<class Archive>
void OligoFlyweight<OligoType>::load(Archive& ar,const unsigned int /*version*/)
{
    OligoType bdo = OligoType(); 
    ar >> bdo;
    key_ = OligoGlobalIndex<OligoType>::instance().getIndex(bdo);
}

template<class OligoType>
template<class Archive>
void OligoFlyweight<OligoType>::serialize(Archive & ar, const unsigned int version)
{
    boost::serialization::split_member(ar, *this, version);
}

