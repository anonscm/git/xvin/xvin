
// Archive
template<class Archive>
void Hybridation::serialize(Archive & ar, const unsigned int /*version*/)
{
    ar & theoricalOligo_;
    ar & actualOligo_;
    ar & probability_;
    if (Archive::is_loading::value)
        isEmpty_ = theoricalOligo_.get().empty();
}
