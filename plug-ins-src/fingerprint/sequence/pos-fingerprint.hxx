#pragma once


template<class Archive>
void PosFingerprint::serialize(Archive & ar, const unsigned int /*version*/)
{
    // serialize base class information
    ar & boost::serialization::base_object<Fingerprint>(*this);
    ar & underlyingSequenceSize_;
    ar & positions_;
    ar & hybridations_;
}


//template
//void PosFingerprint::serialize(boost::archive::text_oarchive & ar, const unsigned int version);

//template
//void PosFingerprint::serialize(boost::archive::text_iarchive & ar, const unsigned int version);
