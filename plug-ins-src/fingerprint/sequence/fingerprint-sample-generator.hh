#pragma once
#include "../define.hh"
#include "fingerprint.hh"
#include "../params/sample-generator-params.hh"
#include <vector>

/**
 * @brief generate sample simulating experimentales fingerprint from hairpin
 */
class FingerprintSampleGenerator
{
  public:
    FingerprintSampleGenerator();
    virtual ~FingerprintSampleGenerator();

    /**
     * @brief generate sample simulating experimentales fingerprint from hairpin
     *
     * @param numSample number of sample to generate.
     *
     * @return vector of generated fingerprints
     */
    FingerprintVector generate(int numSample, const Fingerprint* ref = nullptr, int locus = -1);

    std::shared_ptr<Fingerprint> generateOne(const Fingerprint& ref, int locus, int size, bool isReverse, float baseNmRate);

    /**
     * @brief replace the current seed used to generate random fingerprints.
     * This action reset the random number generator
     *
     * @param randomSeed
     */
    void randomSeed(int randomSeed);

    /**
     * @brief get the current seed.
     *
     * @return
     */
    int randomSeed() const;

    void references(FingerprintVector& references);

    void sizeDistribution(int meanSize, int stdDevSize);
    const SizeDistribution& sizeDistribution() const;

    void noiseDistribution(float meanNoise, float stdDevNoise);
    const NoiseDistribution& noiseDistribution() const;

    void baseNmRateDistribution(float meanBaseNmRate, float stddevBaseNmRate);
    const BaseNmRateDistribution& baseNmRateDistribution() const;

    void missingHybDistribution(float meanMissing);
    const MissingHybDistribution& missingHybDistribution() const;

    void addedHybDistribution(float meanAdded);
    const AddedHybDistribution& addedHybDistribution() const;

    void directionDistribution(float prob);
    float probReverse() const;

    void minHybridization(int minHyb);
    int minHybridization() const;

    SampleGeneratorParams &params();
    void params(SampleGeneratorParams& params);

  private:

    SampleGeneratorParams params_;
    FingerprintVector references_;
    int randomSeed_;
    std::default_random_engine randomEngine_;
    int maxRetrieve_ = 100;
};

#include "fingerprint-sample-generator.hxx"
