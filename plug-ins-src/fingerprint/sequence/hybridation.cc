#include "hybridation.hh"

Hybridation::Hybridation()
:Hybridation(Oligo())
{
}

Hybridation::Hybridation(const Oligo& theoricalOligo)
:Hybridation(theoricalOligo, 1)
{
} 

Hybridation::Hybridation(const Oligo& theoricalOligo, double probability)
:Hybridation(theoricalOligo, Oligo(), probability)
{    
}

Hybridation::Hybridation(const Oligo& theoricalOligo, const Oligo& actualOligo, double probability)
: theoricalOligo_ (theoricalOligo),
    actualOligo_ (actualOligo),
    probability_ (probability),
    isEmpty_ (theoricalOligo.get().empty())
{
    if (probability <= std::numeric_limits<double>::epsilon())
    {
        throw std::exception();
        std::cerr << __FILE__ << ":" << __LINE__ << " Fatal error, get oligo fail." << std::endl;
    }
}

Hybridation::Hybridation (const Hybridation& o)
:Hybridation(o.theoricalOligo_, o.actualOligo_, o.probability_)
{
}

const Oligo& Hybridation::actualOligo() const
{
    if (actualOligo_.get().empty())
        return theoricalOligo_;
    return actualOligo_;
}

Hybridation::~Hybridation()
{
}

double Hybridation::probability() const
{

    return probability_;
}

const Oligo& Hybridation::theoricalOligo() const
{
    return theoricalOligo_;
}

bool Hybridation::empty() const
{
    return isEmpty_;
}

bool Hybridation::operator<(const Hybridation &o1) const
{
    //return (this->theoricalOligo_ == o1.theoricalOligo_ ? this->actualOligo_ < o1.actualOligo_ :
    //        this->theoricalOligo_ < o1.theoricalOligo_);
    return probability_ < o1.probability_;
}

std::ostream& operator<< (std::ostream& ostr, const Hybridation& o)
{
    ostr << o.theoricalOligo_ << ":" << o.actualOligo_ << "~" << o.probability_;
    return ostr;
}
