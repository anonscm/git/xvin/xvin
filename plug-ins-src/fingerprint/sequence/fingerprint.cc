#include "fingerprint.hh"
#ifndef NDEBUG
#include <stdio.h>
#endif
#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif

Fingerprint::Fingerprint()
    :Fingerprint(1)
{
}

Fingerprint::Fingerprint(int resolution)
    : description_ (""),
      isReferenceFingerprint_ (false),
      resolution_(resolution)
{
}

Fingerprint::Fingerprint(const OligoSet& oligos)
    : oligos_ (oligos),
      description_ (""),
      isReferenceFingerprint_ (false),
      resolution_(1)
{
}

Fingerprint::~Fingerprint()
{
}


void Fingerprint::originPosition(int originPosition)
{
    originPosition_ = originPosition;
}
    
Fingerprint::Fingerprint(const Fingerprint& fgp)
    :oligos_ (fgp.oligos_),
    outcastOligos_ (fgp.outcastOligos_),
    description_ (fgp.description_),
    isReferenceFingerprint_ (fgp.isReferenceFingerprint_),
    originPosition_ (fgp.originPosition_),
    resolution_ (fgp.resolution_),
    sourceFgp_ (fgp.sourceFgp_),
    generatorParams_ (fgp.generatorParams_),
    generationParams_ (fgp.generationParams_)
{

}
