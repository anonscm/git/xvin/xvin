#include "dist-oligo-sequence.hh"

std::string DistOligoSequence::classChars_ = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

DistOligoAlphabet& DistOligoSequence::operator[](const int index)
{
    return sequence_[index];
}

const DistOligoAlphabet& DistOligoSequence::operator[](const int index) const
{
    return sequence_[index];
}

DistOligoSequence::~DistOligoSequence()
{
}

const std::string& DistOligoSequence::description() const
{
    return description_;
}
unsigned int DistOligoSequence::size() const
{
    return underlyingSize_;
}

int DistOligoSequence::position(int index) const
{
    return positions_[index];
}

DistOligoSequence::DistOligoSequence(std::shared_ptr<const Fingerprint> fingerprint,
                                     const DistClasses& distClasses, float distClassBaseNmRate)
    : distClassBaseNmRate_(distClassBaseNmRate),
      description_(fingerprint->description()),
      isReference_(fingerprint->isReferenceFingerprint()),
      resolution_(fingerprint->resolution()),
      underlyingSize_(fingerprint->size()),
      distClasses_(distClasses),
      originPosition_(fingerprint->originPosition()),
      parentFingerprint_(fingerprint)
{
    if (fingerprint->nbHybridation() > 0)
    {
        int curPos = fingerprint->firstHyb();
        int nextPos = 0;
        int dist = 0;

        for (int j = 0; j < fingerprint->nbHybridation(); ++j)
        {
            nextPos = fingerprint->nextHybridizationPos(curPos);
            positions_.push_back(curPos);

            if (nextPos == -1)
            {
                break;
            }

            curPos = nextPos;
        }

        for (int j = 0; j < positions_.size() - 1; ++j)
        {
            dist = (positions_[j + 1] - positions_[j]) * (1 / distClassBaseNmRate_);
            DistOligoAlphabet doa;

            if (dist < distClasses.maxDistance)
            {
                doa.value = distClasses.map[dist];
            }
            else
            {
                doa.value = distClasses.range.size() - 1;
            }

            sequence_.push_back(doa);
        }
    }
}

std::ostream& DistOligoSequence::dumpClasses(std::ostream& ostr)
{
    if (distClasses_.range.size() > classChars_.size())
    {
        ostr << "Can dump sequence, not enough char to represent classes";
    }

    for (int i = 0; i < (int) distClasses_.range.size(); ++i)
    {
        ostr << "[" << distClasses_.range[i].first << "," << distClasses_.range[i].second <<  "]" << "==>" << classChars_[i] <<
             std::endl;
    }

    return ostr;
}

std::ostream& DistOligoSequence::dump(std::ostream& ostr)
{
    for (int i = 0; i < (int) sequence_.size(); ++i)
    {
        if (i != 0 && i % 80 == 0)
        {
            ostr << std::endl;
        }

        ostr << classChars_[sequence_[i].value];
    }

    ostr << std::endl;
    return ostr;
}
int DistOligoSequence::nbHybridation() const
{
    return positions_.size();
}
//int DistOligoSequence::underlyingSize() const
//{
//    return underlyingSize_;
//}
int DistOligoSequence::originPosition() const
{
    return originPosition_;
}

const OligoSet& DistOligoSequence::oligoSet() const
{
    return oligos_;
}

std::shared_ptr<const Fingerprint>  DistOligoSequence::parentFingerprint() const
{
    return parentFingerprint_;
}

DistClasses& DistOligoSequence::distClasses()
{
    return distClasses_;
}

unsigned int DistOligoSequence::oligoSeqSize() const
{
    return  sequence_.size();
}
json DistOligoSequence::dumpJson(bool dumpHybs) const
{
    json j;
    j["type"] = "DistOligoSequence";
    j["desc"] = description_;
    j["isRef"] = isReference_;
    j["origPos"] = originPosition_;
    j["resolution"] = resolution_;
    j["seqSize"] = underlyingSize_;
    j["hybCount"] = sequence_.size();
    j["distClassBaseNmRate"] = distClassBaseNmRate_;

    if (parentFingerprint_ && parentFingerprint_->generationParams())
    {
        auto gp = parentFingerprint_->generationParams();
        j["genParams"]["size"] =  gp->size;
        j["genParams"]["locus"] =  gp->locus;
        j["genParams"]["noiseStdev"] =  gp->noiseDistribution.stddev();
        //j["genParams"]["fromDesc"] =  generationParams_->fromDesc()
        j["genParams"]["baseNmRate"] =  gp->baseNmRate;
        j["genParams"]["isReverse"] =  gp->isReverse;
        j["genParams"]["missingHybs"] = gp->missingHybs;
        // TODO missing / added distribution (not used right now)
    }

    if (dumpHybs)
    {
        j["seq"] = json::string_t();
        j["pos"] = json::array();

        for (uint i = 0; i < positions_.size(); ++i)
        {
            j["pos"].push_back(positions_[i]);
        }

        for (uint i = 0; i < sequence_.size(); ++i)
        {
            j["sequence"].push_back(sequence_[i].value);
        }
    }
    else
    {
        j["oligoBatchs"] = json::array();

        for (auto biol : oligos_)
        {
            auto oligo = biol.get();
            j["oligoBatchs"].push_back(json::array());
            j["oligoBatchs"].back().push_back(oligo.oligo());

            for (auto subOligo : oligo.behaviors())
            {
                j["oligoBatchs"].back().push_back(subOligo.get().oligo());
            }
        }
    }

    return j;
}

std::shared_ptr<DistOligoSequence> DistOligoSequence::reverse(void) const
{
    assert(this->positions_.size() > 0);
    auto res = std::shared_ptr<DistOligoSequence>(new DistOligoSequence(*this));
    auto firstPos = res->positions_[0];
    auto lastPos = res->positions_[res->positions_.size() - 1];
    res->positions_.clear();
    res->sequence_.clear();

    for (auto pit = this->positions_.crbegin(); pit != this->positions_.crend(); ++pit)
    {
        res->positions_.push_back((-*pit) + lastPos);
    }

    for (auto sit = this->sequence_.crbegin(); sit != this->sequence_.crend(); ++sit)
    {
        res->sequence_.push_back(*sit);
    }

    return res;
}
