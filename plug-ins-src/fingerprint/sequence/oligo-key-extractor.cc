
#include "oligo-key-extractor.hh"
#include <string>
#include <iostream>
#include <algorithm>
#include "bidirectional-oligo.hh"
#include "hybridation.hh"
template<class OligoType>
OligoGlobalIndex<OligoType> *OligoGlobalIndex<OligoType>::instance_ = 0;

template<class OligoType>
OligoGlobalIndex<OligoType>& OligoGlobalIndex<OligoType>::instance()
{
    if(!instance_)
        instance_ = new OligoGlobalIndex();
    return *instance_;
}

template<class OligoType>
OligoGlobalIndex<OligoType>::OligoGlobalIndex()
: lastIndex_ (0)
{
    oligoGlobalMap_.insert(typename OligoBimap::value_type(OligoType(), 0));
}

template<class OligoType>
OligoGlobalIndex<OligoType>::~OligoGlobalIndex()
{
    delete instance_;
}

template<class OligoType>
const int& OligoGlobalIndex<OligoType>::getIndex(const OligoType& oligo)
{
    const int *res;
#pragma omp critical
    {
        //std::cout << oligo << std::endl;
        typename OligoBimap::left_iterator It = oligoGlobalMap_.left.find(oligo);

        if (It != oligoGlobalMap_.left.end())
        {
            //std::cout  << "found: " << (*It).first << (*It).second<< std::endl;
            res = &(It->second);
        }
        else
        {
            lastIndex_++;
            oligoGlobalMap_.insert(typename OligoBimap::value_type (oligo,lastIndex_));
            res = &(oligoGlobalMap_.left.find(oligo)->second);
        }
    }
    return *res;
}

template<class OligoType>
const OligoType& OligoGlobalIndex<OligoType>::getOligo(int index)
{
    typename OligoBimap::right_iterator It = oligoGlobalMap_.right.find(index);

    if (It != oligoGlobalMap_.right.end())
        return It->second;

    std::cerr << __FILE__ << ":" << __LINE__ << " Fatal error, get n° "<< index << " oligo fail." << std::endl;
    throw std::exception();
}

template<class OligoType>
bool OligoGlobalIndex<OligoType>::oligoExists(int index) const
{
    typename OligoBimap::right_const_iterator It = oligoGlobalMap_.right.find(index);
    return (It != oligoGlobalMap_.right.end());
}

template<class OligoType>
const int& OligoKeyExtractor<OligoType>::operator()(OligoType x) const
{
    return OligoGlobalIndex<OligoType>::instance()->getIndex(x);
}

template class OligoGlobalIndex<BiDirOligo>;
template class OligoGlobalIndex<Hybridation>; //Force instanciation

std::ostream& operator<< (std::ostream& ostr,const OligoGlobalIndex<BiDirOligo>& o)
{
    for (typename OligoGlobalIndex<BiDirOligo>::OligoBimap::value_type oligo : o.oligoGlobalMap_)
        ostr << oligo.right << " " << oligo.left << std::endl;
    return ostr;
}

std::ostream& operator<< (std::ostream& ostr, const OligoGlobalIndex<Hybridation>& o)
{
    for (typename OligoGlobalIndex<Hybridation>::OligoBimap::value_type oligo : o.oligoGlobalMap_)
        ostr << oligo.right << " " << oligo.left << std::endl;
   return ostr;
}
