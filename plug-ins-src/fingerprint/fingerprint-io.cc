#include "fingerprint-io.hh"
#include "../histo_utils/histo_utils.hh"
#include <cmath>
#include <clocale>
PosFingerprint *loadSimplePosFingerprint(std::istream& is)
{
    std::string description = "";
    double size = 0;
    std::string curOligoStruct;
    char *curOligo = NULL;
    double curProba = 1;
    double currentPos = 0;
    PosFingerprint *fgprint = new PosFingerprint();
    std::getline(is, description);
    is >> size;

    while (is.good())
    {
        is >> currentPos;
        is >> curOligoStruct;
        setlocale(LC_ALL, "C");
        parse_oligo_struct(curOligoStruct.c_str(),  &curOligo, &curProba);
        int roundCurPos = std::round(currentPos);
        Hybridation obf = Hybridation(Oligo(BiDirOligo(curOligo)), curProba);
        fgprint->hybridationValue(roundCurPos, obf);
    }

    fgprint->description(description);
    fgprint->size(fgprint->lastHyb() - fgprint->firstHyb());
    return fgprint;
}
DistFingerprint *loadSimpleDistFingerprint(std::istream& is)
{
    std::string description = "";
    double size = 0;
    std::string curOligoStruct;
    char *curOligo = NULL;
    double curProba = 1;
    double currentPos = 0;
    DistFingerprint *fgprint = new DistFingerprint();
    std::getline(is, description);
    is >> size;

    while (is.good())
    {
        is >> currentPos;
        is >> curOligoStruct;
        setlocale(LC_ALL, "C");
        parse_oligo_struct(curOligoStruct.c_str(),  &curOligo, &curProba);
        int roundCurPos = std::round(currentPos);
        Hybridation obf = Hybridation(Oligo(BiDirOligo(curOligo)), curProba);
        fgprint->hybridationValue(roundCurPos, obf);
        free(curOligo);
    }

    fgprint->description(description);
    fgprint->size(std::round(size));
    fgprint->originPosition(-1);
    return fgprint;
}

void dump_clusters(FILE *fd, FingerprintVector& fgps, int **clusters, int nb_clusters, int *cluster_size)
{
    for (int i = 0; i < nb_clusters; ++i)
    {
        fprintf(fd, "Cluster :%d\n\n", i);

        for (int j = 0; j < cluster_size[i]; ++j)
        {
            fprintf(fd, "%s\n", fgps[clusters[i][j]]->description().c_str());
        }

        fprintf(fd, "\n\n");
    }
}

DistFingerprint *histogramToDistFingerprint(d_s *histogram, Oligo oligo, float threashold)
{
    DistFingerprint *fgprint = new DistFingerprint();
    fgprint->description("");//histogram->source);
    fgprint->size(histogram->xd[histogram->nx - 1]);
    std::vector<std::pair<float, float>> *maxvect = get_local_maxima_pos_and_val_sorted_data(histogram, threashold);

    for (std::pair<float, float> curMax : *maxvect)
    {
        Hybridation obf = Hybridation(oligo, curMax.second);
        fgprint->hybridationValue(std::round(curMax.first), obf);
    }

    return fgprint;
}


json oligo2json(oligo_t **oligos, int nb_oligos)
{

    json res;

    res["oligoBehaviors"] = json::array();

    for (int i = 0; i < nb_oligos; ++i)
    {
        json cur_oligo;

        cur_oligo["seq"] = oligos[i]->seq;
        cur_oligo["proba"] = oligos[i]->probability;
        cur_oligo["behaviors"] = json::array();
        for (int j = 0; j < oligos[i]->behaviors_size; ++j)
        {
            json cur_bh;

            cur_bh["seq"] = oligos[i]->behaviors[j];
            cur_bh["proba"] = oligos[i]->behaviors_proba[j];
            cur_oligo["behaviors"].push_back(cur_bh);
        }

        res["oligoBehaviors"].push_back(cur_oligo);
    }

    return res;
}

bool json2oligo(json jsin, oligo_t ***oligos, int *nb_oligos)
{
    if (jsin["oligoBatch"].is_array() && jsin["oligoBatch"].size() > 0)
    {
        json::array_t jsbatch = jsin["oligoBatch"];
        *nb_oligos = 1;
        *oligos = (oligo_t **) calloc(sizeof(oligo_t *), (*nb_oligos));
        (*oligos)[0] = (oligo_t *) malloc(sizeof(oligo_t));
        oligo_t *cur_oligo = (*oligos)[0];

        cur_oligo->seq = strdup(jsbatch[0].get<std::string>().c_str());
        cur_oligo->probability = jsin["oligoBatchProba"].is_array() ? jsin["oligoBatchProba"][0].get<float>() : 1.;
        cur_oligo->behaviors_size = jsbatch.size() - 1;
        cur_oligo->behaviors =  (char **) calloc(sizeof(char *), cur_oligo->behaviors_size);
        cur_oligo->behaviors_proba =  (double *) calloc(sizeof(double), cur_oligo->behaviors_size);

        for (uint i = 1; i < jsbatch.size(); ++i)
        {
            cur_oligo->behaviors[i - 1] = strdup(jsbatch[i].get<std::string>().c_str());
            cur_oligo->behaviors_proba[i - 1] = jsin["oligoBatchProba"].is_array() ? jsin["oligoBatchProba"][i].get<float>() : 1.;
        }

    }
    else if (jsin["oligoSet"].is_array() && jsin["oligoSet"].size() > 0)
    {
        warning_message("Oligoset import not implemented");
        return false;
    }
    else if (jsin["oligoBehaviors"].is_array() && jsin["oligoBehaviors"].size() > 0)
    {
        json::array_t js = jsin["oligoBehaviors"];

        for (uint i = 0; i < js.size(); ++i)
        {
            int oligo_idx = *nb_oligos;
            (*nb_oligos)++;
            *oligos = (oligo_t **) realloc(*oligos, sizeof(oligo_t *) * (*nb_oligos));
            (*oligos)[oligo_idx] = (oligo_t *) malloc(sizeof(oligo_t));
            oligo_t *cur_oligo = (*oligos)[oligo_idx];
            cur_oligo->seq = strdup(js[i]["seq"].get<std::string>().c_str());
            cur_oligo->probability = js[i]["proba"].is_number_float() ? js[i]["proba"].get<float>() : 1;
            cur_oligo->behaviors = NULL;
            cur_oligo->behaviors_proba = NULL;
            cur_oligo->behaviors_size = 0;

            if (js[i]["behaviors"].is_array() && js[i]["behaviors"].size() > 0)
            {
                cur_oligo->behaviors_size = js[i]["behaviors"].size();
                json::array_t bh = js[i]["behaviors"];

                for (uint j = 0; j < bh.size(); ++i)
                {
                    int behavior_idx = cur_oligo->behaviors_size;
                    cur_oligo->behaviors_size++;
                    cur_oligo->behaviors =
                        (char **) realloc(cur_oligo->behaviors, sizeof(char *) * cur_oligo->behaviors_size);
                    cur_oligo->behaviors_proba =
                        (double *) realloc(cur_oligo->behaviors_proba, sizeof(double) * cur_oligo->behaviors_size);
                    cur_oligo->behaviors[behavior_idx] = strdup(bh[i]["seq"].get<std::string>().c_str());
                    cur_oligo->behaviors_proba[behavior_idx] = bh[i]["proba"].is_number_float() ? bh[i]["proba"].get<float>() : 1;
                }
            }
        }
    }
    return true;
}
