/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _TM_DNA_C_
#define _TM_DNA_C_

# include "xvin.h"
# include "allegro.h"
# include "float.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "Tm_DNA.h"
//place here other headers of this plugin 
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "Tm_DNA.h"



# define A 0
# define C 1
# define G 2
# define T 3

# define AA 0
# define AC 1
# define AG 2
# define AT 3

# define CA 4
# define CC 5
# define CG 6
# define CT 7

# define GA 8
# define GC 9
# define GG 10
# define GT 11

# define TA 12
# define TC 13
# define TG 14
# define TT 15

# define IN 16 // initialization
# define TP 17 // Terminal AT penalty
# define SC 18 // Symmetry correction

# define INGC 19  // initialization with G or C
# define INAT 17  // initialization with A or T
# define SC 18    // Symmetry correction
# define ATPEN 20 // oligo starting with 5'-TA or complementry ending with TA-3'    

# define AA_	0
# define AC_ 	1
# define AG_ 	2
# define AT_ 	3

# define A_A_	4
# define A_C_	5
# define A_G_	6
# define A_T_	7

# define CA_ 	8
# define CC_ 	9
# define CG_ 	10
# define CT_ 	11

# define C_A_	12
# define C_C_	13
# define C_G_	14
# define C_T_	15

# define GA_ 	16
# define GC_ 	17
# define GG_ 	18
# define GT_ 	19

# define G_A_	20
# define G_C_	21
# define G_G_	22
# define G_T_	23

# define TA_ 	24
# define TC_ 	25
# define TG_ 	26
# define TT_ 	27

# define T_A_	28
# define T_C_	29
# define T_G_	30
# define T_T_	31

# define _AA_	32
# define _AC_	33
# define _AG_	34
# define _AT_	35

# define _A_A_	36
# define _A_C_	37
# define _A_G_	38
# define _A_T_	39

# define _CA_	40
# define _CC_	41
# define _CG_	42
# define _CT_	43

# define _C_A_	44
# define _C_C_	45
# define _C_G_	46
# define _C_T_	47

# define _GA_	48
# define _GC_	49
# define _GG_	50
# define _GT_	51

# define _G_A_	52
# define _G_C_	53
# define _G_G_	54
# define _G_T_	55

# define _TA_	56
# define _TC_	57
# define _TG_	58
# define _TT_	59

# define _T_A_	60
# define _T_C_	61
# define _T_G_	62
# define _T_T_	63




# define IN_ 64 // initialization
# define TP_ 65 // Terminal AT penalty
# define SC_ 66 // Symmetry correction

# define INGC_ 64 // initialization with GC
# define INAT_ 65 // initialization with AT
# define SC_ 66 // Symmetry correction

typedef struct _oligo_Tm
{
  char *seq;
  float Gc;
  float Conc;
  float Na[5];
  float Tmx[5];
  float TmU[5];
  float TmR[5];
}oligo_Tm;

oligo_Tm owz[92] = {{"ATCAATCATA",0.20,2,69,119,220,621,1020,21.3,24.5,27.9,32.4,33.6,0,0,0,0,0,0,0,0,0,0},
		    {"TTGTAGTCAT",0.30,2,69,119,220,621,1020,24.7,28.2,31.2,34.8,36.0,0,0,0,0,0,0,0,0,0,0},
		    {"GAAATGAAAG",0.30,2,69,119,220,621,1020,22.1,25.3,29.1,33.1,34.4,0,0,0,0,0,0,0,0,0,0},
		    {"CCAACTTCTT",0.40,2,69,119,220,621,1020,29.0,32.1,35.9,39.6,40.6,0,0,0,0,0,0,0,0,0,0},
		    {"ATCGTCTGGA",0.50,2,69,119,220,621,1020,33.8,37.4,40.5,44.5,44.9,0,0,0,0,0,0,0,0,0,0},
		    {"AGCGTAAGTC",0.50,2,69,119,220,621,1020,27.4,31.2,34.6,39.5,40.3,0,0,0,0,0,0,0,0,0,0},
		    {"CGATCTGCGA",0.60,2,69,119,220,621,1020,39.2,42.3,45.6,48.4,49.1,0,0,0,0,0,0,0,0,0,0},
		    {"TGGCGAGCAC",0.70,2,69,119,220,621,1020,44.4,47.8,51.3,55.0,55.3,0,0,0,0,0,0,0,0,0,0},
		    {"GATGCGCTCG",0.70,2,69,119,220,621,1020,44.2,47.0,50.1,53.6,53.5,0,0,0,0,0,0,0,0,0,0},
		    {"GGGACCGCCT",0.80,2,69,119,220,621,1020,46.7,50.3,53.1,56.5,57.0,0,0,0,0,0,0,0,0,0,0},
		    {"CGTACACATGC",0.55,2,69,119,220,621,1020,40.4,43.5,46.1,49.6,49.9,0,0,0,0,0,0,0,0,0,0},
		    {"CCATTGCTACC",0.55,2,69,119,220,621,1020,38.0,41.7,44.5,47.9,48.9,0,0,0,0,0,0,0,0,0,0},
		    {"TACTAACATTAACTA",0.20,2,69,119,220,621,1020,35.3,40.4,44.1,49.3,51.1,0,0,0,0,0,0,0,0,0,0},
		    {"ATACTTACTGATTAG",0.27,2,69,119,220,621,1020,38.1,41.4,45.0,49.9,51.5,0,0,0,0,0,0,0,0,0,0},
		    {"GTACACTGTCTTATA",0.33,2,69,119,220,621,1020,41.0,44.8,48.3,52.9,54.8,0,0,0,0,0,0,0,0,0,0},
		    {"GTATGAGAGACTTTA",0.33,2,69,119,220,621,1020,39.9,44.2,47.9,53.3,55.4,0,0,0,0,0,0,0,0,0,0},
		    {"TTCTACCTATGTGAT",0.33,2,69,119,220,621,1020,40.6,44.6,48.1,52.3,53.7,0,0,0,0,0,0,0,0,0,0},
		    {"AGTAGTAATCACACC",0.40,2,69,119,220,621,1020,44.3,47.8,51.6,56.2,57.1,0,0,0,0,0,0,0,0,0,0},
		    {"ATCGTCTCGGTATAA",0.40,2,69,119,220,621,1020,45.5,49.4,52.9,57.4,58.6,0,0,0,0,0,0,0,0,0,0},
		    {"ACGACAGGTTTACCA",0.47,2,69,119,220,621,1020,47.8,51.2,55.5,59.8,61.3,0,0,0,0,0,0,0,0,0,0},
		    {"CTTTCATGTCCGCAT",0.47,2,69,119,220,621,1020,49.9,53.9,57.1,61.4,62.8,0,0,0,0,0,0,0,0,0,0},
		    {"TGGATGTGTGAACAC",0.47,2,69,119,220,621,1020,46.5,51.6,54.6,59.1,60.4,0,0,0,0,0,0,0,0,0,0},
		    {"ACCCCGCAATACATG",0.53,2,69,119,220,621,1020,51.3,55.2,58.5,62.4,62.9,0,0,0,0,0,0,0,0,0,0},
		    {"GCAGTGGATGTGAGA",0.53,2,69,119,220,621,1020,51.2,54.8,58.0,61.7,63.3,0,0,0,0,0,0,0,0,0,0},
		    {"GGTCCTTACTTGGTG",0.53,2,69,119,220,621,1020,47.8,51.6,55.1,59.1,60.3,0,0,0,0,0,0,0,0,0,0},
		    {"CGCCTCATGCTCATC",0.60,2,69,119,220,621,1020,52.8,56.7,60.1,63.6,65.8,0,0,0,0,0,0,0,0,0,0},
		    {"AAATAGCCGGGCCGC",0.67,2,69,119,220,621,1020,59.0,62.2,65.3,69.0,70.4,0,0,0,0,0,0,0,0,0,0},
		    {"CCAGCCAGTCTCTCC",0.67,2,69,119,220,621,1020,54.1,58.0,61.5,65.1,66.7,0,0,0,0,0,0,0,0,0,0},
		    {"GACGACAAGACCGCG",0.67,2,69,119,220,621,1020,57.9,61.5,64.4,67.6,68.6,0,0,0,0,0,0,0,0,0,0},
		    {"CAGCCTCGTCGCAGC",0.73,2,69,119,220,621,1020,60.8,64.1,67.4,70.1,72.0,0,0,0,0,0,0,0,0,0,0},
		    {"CTCGCGGTCGAAGCG",0.73,2,69,119,220,621,1020,61.5,64.6,67.1,70.0,70.7,0,0,0,0,0,0,0,0,0,0},
		    {"GCGTCGGTCCGGGCT",0.80,2,69,119,220,621,1020,64.9,67.7,70.5,73.9,74.1,0,0,0,0,0,0,0,0,0,0},
		    {"TATGTATATTTTGTAATCAG",0.20,2,69,119,220,621,1020,44.4,47.7,52.6,57.6,61.2,0,0,0,0,0,0,0,0,0,0},
		    {"TTCAAGTTAAACATTCTATC",0.25,2,69,119,220,621,1020,45.7,49.5,53.9,59.4,61.5,0,0,0,0,0,0,0,0,0,0},
		    {"TGATTCTACCTATGTGATTT",0.30,2,69,119,220,621,1020,49.1,53.5,57.4,62.3,64.4,0,0,0,0,0,0,0,0,0,0},
		    {"GAGATTGTTTCCCTTTCAAA",0.35,2,69,119,220,621,1020,49.3,52.8,57.6,62.6,65.3,0,0,0,0,0,0,0,0,0,0},
		    {"ATGCAATGCTACATATTCGC",0.40,2,69,119,220,621,1020,55.2,59.5,62.9,67.0,68.9,0,0,0,0,0,0,0,0,0,0},
		    {"CCACTATACCATCTATGTAC",0.40,2,69,119,220,621,1020,51.1,54.6,58.4,62.2,64.4,0,0,0,0,0,0,0,0,0,0},
		    {"CCATCATTGTGTCTACCTCA",0.45,2,69,119,220,621,1020,55.6,59.5,63.1,67.3,68.5,0,0,0,0,0,0,0,0,0,0},
		    {"CGGGACCAACTAAAGGAAAT",0.45,2,69,119,220,621,1020,53.7,57.7,61.7,66.7,68.5,0,0,0,0,0,0,0,0,0,0},
		    {"TAGTGGCGATTAGATTCTGC",0.45,2,69,119,220,621,1020,57.0,60.6,64.8,69.1,71.2,0,0,0,0,0,0,0,0,0,0},
		    {"AGCTGCAGTGGATGTGAGAA",0.50,2,69,119,220,621,1020,59.7,63.5,67.6,71.3,73.1,0,0,0,0,0,0,0,0,0,0},
		    {"TACTTCCAGTGCTCAGCGTA",0.50,2,69,119,220,621,1020,60.3,64.4,67.7,71.6,73.6,0,0,0,0,0,0,0,0,0,0},
		    {"CAGTGAGACAGCAATGGTCG",0.55,2,69,119,220,621,1020,59.8,63.5,67.0,71.1,72.5,0,0,0,0,0,0,0,0,0,0},
		    {"CGAGCTTATCCCTATCCCTC",0.55,2,69,119,220,621,1020,56.0,60.2,64.1,68.5,70.3,0,0,0,0,0,0,0,0,0,0},
		    {"CGTACTAGCGTTGGTCATGG",0.55,2,69,119,220,621,1020,59.6,63.1,66.6,70.5,71.1,0,0,0,0,0,0,0,0,0,0},
		    {"AAGGCGAGTCAGGCTCAGTG",0.60,2,69,119,220,621,1020,64.5,67.7,71.4,75.1,76.3,0,0,0,0,0,0,0,0,0,0},
		    {"ACCGACGACGCTGATCCGAT",0.60,2,69,119,220,621,1020,66.0,69.1,72.6,76.8,77.3,0,0,0,0,0,0,0,0,0,0},
		    {"AGCAGTCCGCCACACCCTGA",0.65,2,69,119,220,621,1020,66.5,69.9,74.0,76.9,78.5,0,0,0,0,0,0,0,0,0,0},
		    {"CAGCCTCGTTCGCACAGCCC",0.70,2,69,119,220,621,1020,67.2,70.7,74.0,77.7,78.1,0,0,0,0,0,0,0,0,0,0},
		    {"GTGGTGGGCCGTGCGCTCTG",0.75,2,69,119,220,621,1020,69.2,72.7,76.2,79.6,81.0,0,0,0,0,0,0,0,0,0,0},
		    {"GTCCACGCCCGGTGCGACGG",0.80,2,69,119,220,621,1020,70.9,73.9,77.3,79.8,81.1,0,0,0,0,0,0,0,0,0,0},
		    {"GATATAGCAAAATTCTAAGTTAATA",0.20,2,69,119,220,621,1020,49.1,53.5,57.7,63.3,66.1,0,0,0,0,0,0,0,0,0,0},
		    {"ATAACTTTACGTGTGTGACCTATTA",0.32,2,69,119,220,621,1020,56.6,60.7,64.7,69.6,71.8,0,0,0,0,0,0,0,0,0,0},
		    {"GTTCTATACTCTTGAAGTTGATTAC",0.32,2,69,119,220,621,1020,52.7,56.1,60.6,66.1,67.7,0,0,0,0,0,0,0,0,0,0},
		    {"CCCTGCACTTTAACTGAATTGTTTA",0.36,2,69,119,220,621,1020,57.4,61.4,65.6,70.1,72.5,0,0,0,0,0,0,0,0,0,0},
		    {"TAACCATACTGAATACCTTTTGACG",0.36,2,69,119,220,621,1020,56.5,60.2,64.3,68.9,71.3,0,0,0,0,0,0,0,0,0,0},
		    {"TCCACACGGTAGTAAAATTAGGCTT",0.40,2,69,119,220,621,1020,59.3,63.1,67.3,71.8,73.8,0,0,0,0,0,0,0,0,0,0},
		    {"TTCCAAAAGGAGTTATGAGTTGCGA",0.40,2,69,119,220,621,1020,59.1,63.0,67.2,71.6,73.8,0,0,0,0,0,0,0,0,0,0},
		    {"AATATCTCTCATGCGCCAAGCTACA",0.44,2,69,119,220,621,1020,62.1,65.7,70.3,75.1,76.5,0,0,0,0,0,0,0,0,0,0},
		    {"TAGTATATCGCAGCATCATACAGGC",0.44,2,69,119,220,621,1020,61.2,64.7,69.1,72.8,75.0,0,0,0,0,0,0,0,0,0,0},
		    {"TGGATTCTACTCAACCTTAGTCTGG",0.44,2,69,119,220,621,1020,59.0,63.1,67.1,71.3,73.6,0,0,0,0,0,0,0,0,0,0},
		    {"CGGAATCCATGTTACTTCGGCTATC",0.48,2,69,119,220,621,1020,60.9,64.7,68.7,73.3,74.8,0,0,0,0,0,0,0,0,0,0},
		    {"CTGGTCTGGATCTGAGAACTTCAGG",0.52,2,69,119,220,621,1020,62.1,65.8,69.6,74.2,75.6,0,0,0,0,0,0,0,0,0,0},
		    {"ACAGCGAATGGACCTACGTGGCCTT",0.56,2,69,119,220,621,1020,68.1,72.1,76.0,79.4,81.0,0,0,0,0,0,0,0,0,0,0},
		    {"AGCAAGTCGAGCAGGGCCTACGTTT",0.56,2,69,119,220,621,1020,68.3,72.6,76.3,80.0,81.5,0,0,0,0,0,0,0,0,0,0},
		    {"GCGAGCGACAGGTTACTTGGCTGAT",0.56,2,69,119,220,621,1020,67.0,70.8,74.7,78.6,80.1,0,0,0,0,0,0,0,0,0,0},
		    {"AAAGGTGTCGCGGAGAGTCGTGCTG",0.60,2,69,119,220,621,1020,69.6,73.6,77.4,81.2,82.4,0,0,0,0,0,0,0,0,0,0},
		    {"ATGGGTGGGAGCCTCGGTAGCAGCC",0.68,2,69,119,220,621,1020,70.7,74.5,78.2,81.6,83.4,0,0,0,0,0,0,0,0,0,0},
		    {"CAGTGGGCTCCTGGGCGTGCTGGTC",0.72,2,69,119,220,621,1020,72.0,75.6,79.2,82.6,83.4,0,0,0,0,0,0,0,0,0,0},
		    {"GCCAACTCCGTCGCCGTTCGTGCGC",0.72,2,69,119,220,621,1020,73.5,76.5,80.6,83.2,84.6,0,0,0,0,0,0,0,0,0,0},
		    {"ACGGGTCCCCGCACCGCACCGCCAG",0.80,2,69,119,220,621,1020,76.8,79.9,84.0,87.1,88.3,0,0,0,0,0,0,0,0,0,0},
		    {"TTATGTATTAAGTTATATAGTAGTAGTAGT",0.20,2,69,119,220,621,1020,50.7,55.0,59.3,65.1,66.6,0,0,0,0,0,0,0,0,0,0},
		    {"ATTGATATCCTTTTCTATTCATCTTTCATT",0.23,2,69,119,220,621,1020,53.5,58.3,62.3,68.6,70.4,0,0,0,0,0,0,0,0,0,0},
		    {"AAAGTACATCAACATAGAGAATTGCATTTC",0.30,2,69,119,220,621,1020,58.3,61.9,66.1,71.3,73.2,0,0,0,0,0,0,0,0,0,0},
		    {"CTTAAGATATGAGAACTTCAACTAATGTGT",0.30,2,69,119,220,621,1020,56.8,61.3,64.9,70.5,71.8,0,0,0,0,0,0,0,0,0,0},
		    {"CTCAACTTGCGGTAAATAAATCGCTTAATC",0.37,2,69,119,220,621,1020,60.9,64.8,68.7,74.4,75.5,0,0,0,0,0,0,0,0,0,0},
		    {"TATTGAGAACAAGTGTCCGATTAGCAGAAA",0.37,2,69,119,220,621,1020,61.3,65.4,69.6,74.8,76.4,0,0,0,0,0,0,0,0,0,0},
		    {"GTCATACGACTGAGTGCAACATTGTTCAAA",0.40,2,69,119,220,621,1020,62.7,66.8,70.8,75.9,76.9,0,0,0,0,0,0,0,0,0,0},
		    {"AACCTGCAACATGGAGTTTTTGTCTCATGC",0.43,2,69,119,220,621,1020,64.5,68.3,72.5,77.7,78.7,0,0,0,0,0,0,0,0,0,0},
		    {"CCGTGCGGTGTGTACGTTTTATTCATCATA",0.43,2,69,119,220,621,1020,63.9,68.3,71.8,76.5,77.6,0,0,0,0,0,0,0,0,0,0},
		    {"GTTCACGTCCGAAAGCTCGAAAAAGGATAC",0.47,2,69,119,220,621,1020,64.3,68.2,72.1,77.1,78.7,0,0,0,0,0,0,0,0,0,0},
		    {"AGTCTGGTCTGGATCTGAGAACTTCAGGCT",0.50,2,69,119,220,621,1020,66.3,70.4,74.5,78.8,80.6,0,0,0,0,0,0,0,0,0,0},
		    {"TCGGAGAAATCACTGAGCTGCCTGAGAAGA",0.50,2,69,119,220,621,1020,66.3,70.4,74.1,79.0,80.9,0,0,0,0,0,0,0,0,0,0},
		    {"CTTCAACGGATCAGGTAGGACTGTGGTGGG",0.57,2,69,119,220,621,1020,67.6,71.7,74.7,78.8,80.1,0,0,0,0,0,0,0,0,0,0},
		    {"ACGCCCACAGGATTAGGCTGGCCCACATTG",0.60,2,69,119,220,621,1020,71.3,74.7,78.5,82.7,84.0,0,0,0,0,0,0,0,0,0,0},
		    {"GTTATTCCGCAGTCCGATGGCAGCAGGCTC",0.60,2,69,119,220,621,1020,70.6,74.9,78.1,82.4,84.1,0,0,0,0,0,0,0,0,0,0},
		    {"TCAGTAGGCGTGACGCAGAGCTGGCGATGG",0.63,2,69,119,220,621,1020,72.2,75.7,79.3,83.3,84.6,0,0,0,0,0,0,0,0,0,0},
		    {"CGCGCCACGTGTGATCTACAGCCGTTCGGC",0.67,2,69,119,220,621,1020,72.7,76.3,79.5,83.4,84.5,0,0,0,0,0,0,0,0,0,0},
		    {"GACCTGACGTGGACCGCTCCTGGGCGTGGT",0.70,2,69,119,220,621,1020,74.4,78.4,81.5,85.2,86.4,0,0,0,0,0,0,0,0,0,0},
		    {"GCCCCTCCACTGGCCGACGGCAGCAGGCTC",0.77,2,69,119,220,621,1020,76.3,79.8,83.6,87.1,87.7,0,0,0,0,0,0,0,0,0,0},
		    {"CGCCGCTGCCGACTGGAGGAGCGCGGGACG",0.80,2,69,119,220,621,1020,77.8,81.6,84.6,87.7,88.6,0,0,0,0,0,0,0,0,0,0}};

float DH[32] = {0}, DS[32] = {0}, DG[32] = {0};
float DHU98[32] = {0}, DSU98[32] = {0}, DGU98[32] = {0}; // unified parameter santaLucia 1998 PNAS
float DHU[32] = {0}, DSU[32] = {0}, DGU[32] = {0}; // unified parameter santaLucia 2004
float DHR[32] = {0}, DSR[32] = {0}, MIR[32] = {0}; // Ritort parameter Huguet 2010 PNAS
float DHR2[32] = {0}, DSR2[32] = {0}, MIR2[32] = {0}; // Ritort 2 parameters Huguet 2010 PNAS

float DHMF[32] = {0}, DSMF[32] = {0}, DGMF[32] = {0}; // Mfold 3.1
int array_init = 0;

float mulH = 1, mulS = 1; // for Ritort modified 2


float D_H[70] = {0}, D_S[70] = {0}, D_G[70] = {0}; // unified parameter santaLucia 1998 PNAS + LNA


void  init_Unified_array(void);
void  init_Unified_98_array(void);
void  init_Ritort_array(void);
void init_Ritort_2_array(void);
void init_Mfold31_array(void);
void  init_LNA_array(void);

void init_array(void)
{
  if (array_init) return;
  DH[AA] = DH[TT] = -7.6; DS[AA] = DS[TT] = -21.3; DG[AA] = DG[TT] = -1.00;
  DH[AT] = -7.2; DS[AT] = -20.4; DG[AT] = -0.88; // DH[AT] = DH[AT] = -7.2; DS[AT] = DS[AT] = -20.4; DG[AT] = DG[AT] = -0.88;
  DH[TA] = -7.2; DS[TA] = -21.3; DG[TA] = -0.58; // DH[TA] = DH[TA] = -7.2; DS[TA] = DS[TA] = -21.3; DG[TA] = DG[TA] = -0.58;
  DH[CA] = DH[TG] = -8.5; DS[CA] = DS[TG] = -22.7; DG[CA] = DG[TG] = -1.45;
  DH[GT] = DH[AC] = -8.4; DS[GT] = DS[AC] = -22.4; DG[GT] = DG[AC] = -1.44;
  DH[CT] = DH[AG] = -7.8; DS[CT] = DS[AG] = -21.0; DG[CT] = DG[AG] = -1.28;
  DH[GA] = DH[TC] = -8.2; DS[GA] = DS[TC] = -22.2; DG[GA] = DG[TC] = -1.30;
  DH[CG] = -10.6; DS[CG] = -27.2; DG[CG] = -2.17; // DH[CG] = DH[CG] = -10.6; DS[CG] = DS[CG] = -27.2; DG[CG] = DG[CG] = -2.17;
  DH[GC] = -9.8; DS[GC] = -24.4; DG[GC] = -2.24; // DH[GC] = DH[GC] = -9.8; DS[GC] = DS[GC] = -24.4; DG[GC] = DG[GC] = -2.24;
  DH[GG] = DH[CC] = -8.0; DS[GG] = DS[CC] = -19.9; DG[GG] = DG[CC] = -1.84;

  DH[IN] = 0.2; DS[IN] = -5.7; DG[IN] = 1.96; 
  DH[TP] = 2.2; DS[TP] = 6.9; DG[TP] = 0.05; 
  DH[SC] = 0.0; DS[SC] = -1.4; DG[SC] = 0.43; 
  init_Unified_array();
  init_Unified_98_array();
  init_Ritort_array();
  init_Ritort_2_array();
  init_LNA_array();
  init_Mfold31_array();
  array_init = 1;
}

# ifdef ENCOURS

Stability and Mismatch Discrimination of Locked Nucleic Acid-DNA
Duplexes
Richard Owczarzy,* Yong You, Christopher L. Groth, and Andrey V. Tataurov

Table 1. Nearest-Neighbor Parameters for Differences between LNA.DNA and DNA.DNA Base Pairs
+A+A/TT -2.091 +/- 0.8 -4.975 +/- 2.3 -0.6 +/- 0.1 1.5
+A+C/TG -2.989 +/- 2.0 -6.563 +/- 5.3 -1.0 +/- 0.4 2.0
+A+G/TC -4.993 +/- 1.5 -10.607 +/- 4.2 -1.8 +/- 0.3 3.3
+A+T/TA -7.503 +/- 1.7 -20.350 +/- 4.5 -1.2 +/- 0.3 6.3
+C+A/GT -5.677 +/- 2.0 -12.798 +/- 5.4 -1.7 +/- 0.4 4.0
+C+C/GG -7.399 +/- 2.6 -16.475 +/- 6.5 -2.3 +/- 0.5 5.1
+C+G/GC -3.958 +/- 2.7 -8.039 +/- 7.4 -1.5 +/- 0.4 2.5
+C+T/GA -7.937 +/- 2.3 -20.218 +/- 6.0 -1.6 +/- 0.5 6.3
+G+A/CT -5.759 +/- 1.6 -12.897 +/- 4.4 -1.7 +/- 0.3 4.0
+G+C/CG -6.309 +/- 2.5 -16.338 +/- 7.0 -1.2 +/- 0.4 5.1
+G+G/CC -5.022 +/- 1.5 -9.773 +/- 4.2 -2.0 +/- 0.3 3.0
+G+T/CA -8.961 +/- 1.7 -23.458 +/- 4.7 -1.7 +/- 0.3 7.3
+T+A/AT -3.118 +/- 1.8 -4.808 +/- 4.9 -1.6 +/- 0.4 1.5
+T+C/AG -0.966 +/- 2.2 0.665 +/- 5.8 -1.2 +/- 0.5 -0.2
+T+G/AC -1.546 +/- 1.6 0.109 +/- 4.4 -1.6 +/- 0.3 0.0
+T+T/AA -2.519 +/- 1.4 -5.483 +/- 3.5 -0.8 +/- 0.3 1.7



Table 2. Full Thermodynamic Parameters for Perfectly
Matched LNA.DNA Base Pairs in 1 M Na+
sequence \Delta H� (kcal/mol)a \Delta S� (cal mol-1 K-1)a \Delta G�37 (kcal/mol)
Consecutive LNA Modifications
+A+A/TT -9.991 -27.175 -1.57
+A+C/TG -11.389 -28.963 -2.44
+A+G/TC -12.793 -31.607 -3.07
+A+T/TA -14.703 -40.750 -2.12
+C+A/GT -14.177 -35.498 -3.11
+C+C/GG -15.399 -36.375 -4.15
+C+G/GC -14.558 -35.239 -3.65
+C+T/GA -15.737 -41.218 -2.92
+G+A/CT -13.959 -35.097 -3.03
+G+C/CG -16.109 -40.738 -3.48
+G+G/CC -13.022 -29.673 -3.82
+G+T/CA -17.361 -45.858 -3.12
+T+A/AT -10.318 -26.108 -2.19
+T+C/AG -9.166 -21.535 -2.48
+T+G/AC -10.046 -22.591 -3.08
+T+T/AA -10.419 -27.683 -1.83


  D_H[AA] = D_H[TT] = -7.9; D_S[AA] = D_S[TT] = -22.2; D_G[AA] = D_G[TT] = -1.00;
  D_H[AT] = -7.2; D_S[AT] = -20.4; D_G[AT] = -0.88;
  D_H[TA] = -7.2; D_S[TA] = -21.3; D_G[TA] = -0.58;
  D_H[CA] = D_H[TG] = -8.5; D_S[CA] = D_S[TG] = -22.7; D_G[CA] = D_G[TG] = -1.45;
  D_H[GT] = D_H[AC] = -8.4; D_S[GT] = D_S[AC] = -22.4; D_G[GT] = D_G[AC] = -1.44;
  D_H[CT] = D_H[AG] = -7.8; D_S[CT] = D_S[AG] = -21.0; D_G[CT] = D_G[AG] = -1.28;
  D_H[GA] = D_H[TC] = -8.2; D_S[GA] = D_S[TC] = -22.2; D_G[GA] = D_G[TC] = -1.30;
  D_H[CG] = -10.6; D_S[CG] = -27.2; D_G[CG] = -2.17;
  D_H[GC] = -9.8; D_S[GC] = -24.4; D_G[GC] = -2.24;
  D_H[GG] = D_H[CC] = -8.0; D_S[GG] = D_S[CC] = -19.9; D_G[GG] = D_G[CC] = -1.84;

  D_H[_INGC] = 0.1; D_S[_INGC] = -2.8; D_G[_INGC] = 0.98; 
  D_H[_INAT] = 2.3; D_S[_INAT] = 4.1; D_G[_INAT] = 1.03; 
  D_H[_SC] = 0.0; D_S[_SC] = -1.4; D_G[_SC] = 0.43; 



D_H[_A_A] = -9.991; D_G[_A_A] = -27.175; D_S[_A_A] = -1.57;
D_H[_A_C] = -11.389; D_G[_A_C] = -28.963; D_S[_A_A] = -2.44;
D_H[_A_G] = -12.793; D_G[_A_G] = -31.607; D_S[_A_G] = -3.07;
D_H[_A_T] = -14.703; D_G[_A_T] = -40.750; D_S[_A_T] = -2.12;
D_H[_C_A] = -14.177; D_G[_C_A] = -35.498; D_S[_C_A] = -3.11;
D_H[_C_C] = -15.399; D_G[_C_C] = -36.375; D_S[_C_C] = -4.15;
D_H[_C_G] = -14.558; D_G[_C_G] = -35.239; D_S[_C_G] = -3.65;
D_H[_C_T] = -15.737; D_G[_C_T] = -41.218; D_S[_C_T] = -2.92;
D_H[_G_A] = -13.959; D_G[_G_A] = -35.097; D_S[_G_A] = -3.03;
D_H[_G_C] = -16.109; D_G[_G_C] = -40.738; D_S[_G_C] = -3.48;
D_H[_G_G] = -13.022; D_G[_G_G] = -29.673; D_S[_G_G] = -3.82;
D_H[_G_T] = -17.361; D_G[_G_T] = -45.858; D_S[_G_T] = -3.12;
D_H[_T_A] = -10.318; D_G[_T_A] = -26.108; D_S[_T_A] = -2.19;
D_H[_T_C] = -9.166; D_G[_T_C] = -21.535; D_S[_T_C] = -2.48;
D_H[_T_G] = -10.046; D_G[_T_G] = -22.591; D_S[_T_G] = -3.08;
D_H[_T_T] = -10.419; D_G[_T_T] = -27.683; D_S[_T_T] = -1.83;



Isolated LNA Modificationsb
D_H[_AA] = -7.193; D_G[_AA] = -19.723; D_S[_AA] = -1.09;
D_H[_AC] = -7.269; D_G[_AC] = -18.336; D_S[_AC] = -1.56;
D_H[_AG] = -7.536; D_G[_AG] = -18.387; D_S[_AG] = -1.84;
D_H[_AT] = -4.918; D_G[_AT] = -12.943; D_S[_AT] = -0.89;
D_H[_CA] = -7.451; D_G[_CA] = -18.380; D_S[_CA] = -1.72;
D_H[_CC] = -5.904; D_G[_CC] = -11.904; D_S[_CC] = -2.30;
D_H[_CG] = -9.815; D_G[_CG] = -23.491; D_S[_CG] = -2.50;
D_H[_CT] = -7.092; D_G[_CT] = -16.825; D_S[_CT] = -1.95;
D_H[_GA] = -5.038; D_G[_GA] = -11.656; D_S[_GA] = -1.37;
D_H[_GC] = -10.160; D_G[_GC] = -24.651; D_S[_GC] = -2.65;
D_H[_GG] = -10.844; D_G[_GG] = -26.580; D_S[_GG] = -2.54;
D_H[_GT] = -8.612; D_G[_GT] = -22.327; D_S[_GT] = -1.63;
D_H[_TA] = -7.246; D_G[_TA] = -19.738; D_S[_TA] = -1.14;
D_H[_TC] = -6.307; D_G[_TC] = -15.515; D_S[_TC] = -1.51;
D_H[_TG] = -10.040; D_G[_TG] = -25.744; D_S[_TG] = -2.00;
D_H[_TT] = -6.372; D_G[_TT] = -16.902; D_S[_TT] = -1.13;
D_H[A_A] = -6.908; D_G[A_A] = -18.135; D_S[A_A] = -1.40;
D_H[A_C] = -5.510; D_G[A_C] = -11.824; D_S[A_C] = -1.83;
D_H[A_G] = -9.000; D_G[A_G] = -22.826; D_S[A_G] = -1.88;
D_H[A_T] = -5.384; D_G[A_T] = -13.537; D_S[A_T] = -1.19;
D_H[C_A] = -7.142; D_G[C_A] = -18.333; D_S[C_A] = -1.40;
D_H[C_C] = -5.937; D_G[C_C] = -12.335; D_S[C_C] = -2.24;
D_H[C_G] = -10.876; D_G[C_G] = -27.918; D_S[C_G] = -2.17;
D_H[C_T] = -9.471; D_G[C_T] = -25.070; D_S[C_T] = -1.69;
D_H[G_A] = -7.756; D_G[G_A] = -19.302; D_S[G_A] = -1.74;
D_H[G_C] = -10.725; D_G[G_C] = -25.511; D_S[G_C] = -2.78;
D_H[G_G] = -8.943; D_G[G_G] = -20.833; D_S[G_G] = -2.51;
D_H[G_T] = -9.035; D_G[G_T] = -22.742; D_S[G_T] = -1.96;
D_H[T_A] = -5.609; D_G[T_A] = -16.019; D_S[T_A] = -0.58;
D_H[T_C] = -7.591; D_G[T_C] = -19.031; D_S[T_C] = -1.70;
D_H[T_G] = -6.335; D_G[T_G] = -15.537; D_S[T_G] = -1.56;
D_H[T_T] = -5.574; D_G[T_T] = -14.149; D_S[T_T] = -1.21;

# endif


int grab_ologos_from_file(char ***loligo)
{
  register int  j;
  int  size;
  char seqfilename[2048],  line[256], oligo[64], ch;
  int n_oligo = 0, m_oligo = 0, read, ic;
  FILE *fp;

  if (loligo == NULL) return -1;

  seqfilename[0] = 0;

  if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to load oligo set", 1))
    return win_printf_OK("Cannot select input file");
  fp = fopen (seqfilename,"r");
  if (fp == NULL)  
    {
      win_printf("Cannot open file:\n%s",backslash_to_slash(seqfilename));
      return -1;
    }
  (*loligo) = (char **)calloc(m_oligo = 256,sizeof(char *));
  if ((*loligo) == NULL) { win_printf("Cannot allocate loligo"); return -1;}
  for (n_oligo = 0, read = 1, size = -1; read == 1; )
    {
      read = fscanf(fp,"%s",line);
      if (read == 1)
	{
	  if (n_oligo >= m_oligo)
	    {
	      m_oligo += 256;
	      (*loligo) = (char **)realloc((*loligo),m_oligo*sizeof(char*));
	      if ((*loligo) == NULL) { win_printf("Cannot reallocate loligo at size %d",m_oligo); return -1;}
	    }
	  for (j = 0, ic = 0; (ch = line[j]) != 0; j++)
	    {  // loop scanning characters in line
	      switch (ch) 
		{   // we test character
		case 'a': case 't': case 'g': case 'c': case 'n':
		case 'A': case 'T': case 'G': case 'C': case 'N':
		case 's': case 'S': case 'w': case 'W':
		  oligo[ic++] = ch;      break;
		default:	      break;
		}
	    } // end of loop scanning characters in line
	  if (ic > 0)
	    {
	      oligo[ic] = 0;
	      if (size < 0) size = ic;
	      if (size != ic) { win_printf_OK("Your oligos differ in size"); return -1;}
	      (*loligo)[n_oligo] = strdup(oligo);
	      n_oligo++;
	    }
	}
    } 
  fclose(fp);
  return n_oligo;
}


int compute_Tm(char *oligo, float conc, float *Tm, float *deltaH, float *deltaS, float *deltaG)
{
  register int i;
  int len, nonselfcomp, index;
  char *coligo = NULL;
  float  dg, ds, dh, tmp, T0 = 273.15, R = 1.9872; // cal/K-mol

  if (oligo == NULL) return 1;
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();
  coligo = strdup(oligo);
  nonselfcomp = 0;
  for (i = 0; i < len; i++)
    {
      if ((oligo[i]&0x5f) == 'A')         coligo[i] = 'T'; 
      else if ((oligo[i]&0x5f) == 'C')    coligo[i] = 'G'; 
      else if ((oligo[i]&0x5f) == 'G')    coligo[i] = 'C'; 
      else if ((oligo[i]&0x5f) == 'T')    coligo[i] = 'A'; 
    }
  // we look for selfcomplementarity
  if (len%2)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < (len/2); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }

  dg = DG[IN]; ds = DS[IN]; dh = DH[IN]; // initialization step
  if (nonselfcomp == 0)  dg += DG[SC]; ds += DS[SC]; dh += DH[SC]; // selfcomplementarity
  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0; 
      if ((oligo[i-1]&0x5F) == 'C')  index = 4; 
      if ((oligo[i-1]&0x5F) == 'G')  index = 8; 
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0; 
      if ((oligo[i]&0x5F) == 'C')  index += 1; 
      if ((oligo[i]&0x5F) == 'G')  index += 2; 
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dg += DG[index]; ds += DS[index]; dh += DH[index];
    }
  if ((oligo[len-1]&0x5F) == 'A' || (oligo[len-1]&0x5F) == 'T')
    {
      dg += DG[TP]; ds += DS[TP]; dh += DH[TP];
    }
  if ((oligo[0]&0x5F) == 'A' || (oligo[0]&0x5F) == 'T')
    {
      dg += DG[TP]; ds += DS[TP]; dh += DH[TP];
    }
  if (deltaH) *deltaH = dh;
  if (deltaS) *deltaS = ds;
  if (deltaG) *deltaG = dg;
  // TM = \Delta H/(\Delta S + R ln CT),
  tmp = 0.000001*conc*2; // we are in micoM, there is two strands
  if (nonselfcomp != 0) tmp /= 4;
  tmp = R * log(tmp);
  tmp += ds;
  tmp = ((float)1000)/tmp;
  if (Tm) *Tm = (dh * tmp) - T0;
  free (coligo);
  return 0;
}
// in nm per bp
float ss_minus_ds_above_5pN(float force)
{
  float a0 = -1.954224e-2, a1= 4.599937e-3;
  float a2 = -1.027389e-4, a3 = 1.176450e-6, z = 0;

  if (force < 4.7391) return 0;
  z = a3 * force * force * force;
  z += a2 * force * force;
  z += a1 * force;
  z += a0;
  return (float)sqrt(z);
}

float integral_ss_minus_ds_above_5pN(float force)
{
  float a0 = -1.337267e-1, a1= -2.274341e-2;
  float a2 = 1.041174e-2, a3 = -2.251318e-4;
  float a4 = 2.812870e-6, a5 = -1.382517e-8, e = 0;

  if (force < 5.15) return 0;
  e = a5 * force * force * force * force * force;
  e += a4 * force * force * force * force;
  e += a3 * force * force * force;
  e += a2 * force * force;
  e += a1 * force;
  e += a0;
  return e; // in pN.nm = 10e-21 J
}

double DeltaG_ss_minus_ds(double force)
{
  double a0 = 2.967220e-2, a1= -2.728794e-1;
  double a2 = 2.893267e-2, a3 = 1.267162e-3;
  double a4 = -2.936617e-4, a5 = 1.806779e-5;
  double a6 = -5.414613e-7, a7 = 8.087451e-9;
  double a8 = -4.817162e-11, e = 0;

  force =  (force < 0) ? -force : force;
  e = force * a8;
  e = force * (e + a7);
  e = force * (e + a6);
  e = force * (e + a5);
  e = force * (e + a4);
  e = force * (e + a3);
  e = force * (e + a2);
  e = force * (e + a1);
  e += a0;
  return e; // in pN.nm = 10e-21 J
}



float ss_minus_ds_above_5pN_old(float force)
{
  float a0 = 1.954246e-2, a1= 4.599937e-3;
  float a2 = -1.027407e-4, a3 = 1.176468e-6, z = 0;

  if (force < 5) return 0;
  z = a3 * force * force * force;
  z += a2 * force * force;
  z += a1 * force;
  z += a0;
  return (float)sqrt(z);
}

void init_Unified_array(void)
{
  // correction March 26th 2014
// unified parameter santaLucia 2004 ANNUEl REVIEW

  DHU[AA] = DHU[TT] = -7.6; DSU[AA] = DSU[TT] = -21.3; DGU[AA] = DGU[TT] = -1.00;
  DHU[AT] = -7.2; DSU[AT] = -20.4; DGU[AT] = -0.88;
  DHU[TA] = -7.2; DSU[TA] = -21.3; DGU[TA] = -0.58;
  DHU[CA] = DHU[TG] = -8.5; DSU[CA] = DSU[TG] = -22.7; DGU[CA] = DGU[TG] = -1.45;
  DHU[GT] = DHU[AC] = -8.4; DSU[GT] = DSU[AC] = -22.4; DGU[GT] = DGU[AC] = -1.44;
  DHU[CT] = DHU[AG] = -7.8; DSU[CT] = DSU[AG] = -21.0; DGU[CT] = DGU[AG] = -1.28;
  DHU[GA] = DHU[TC] = -8.2; DSU[GA] = DSU[TC] = -22.2; DGU[GA] = DGU[TC] = -1.30;
  DHU[CG] = -10.6; DSU[CG] = -27.2; DGU[CG] = -2.17;
  DHU[GC] = -9.8; DSU[GC] = -24.4; DGU[GC] = -2.24;
  DHU[GG] = DHU[CC] = -8.0; DSU[GG] = DSU[CC] = -19.9; DGU[GG] = DGU[CC] = -1.84;

  DHU[IN] = 0.2; DSU[IN] = -5.7; DGU[IN] = 1.96;
  DHU[INAT] = 2.2; DSU[INAT] = 6.9; DGU[INAT] = 0.05;
  DHU[SC] = 0.0; DSU[SC] = -1.4; DGU[SC] = 0.43;   

}


void init_Mfold31_array(void)
{

  DHMF[AA] = DHMF[TT] = -7.9; DSMF[AA] = DSMF[TT] = -22.25; DGMF[AA] = DGMF[TT] = DHMF[AA]-(DSMF[AA]*(273.15+37))/1000;
  DHMF[AT] = -7.2; DSMF[AT] = -20.375; DGMF[AT] = DHMF[AT]-(DSMF[AT]*(273.15+37))/1000;
  DHMF[TA] = -7.2; DSMF[TA] = -21.35; DGMF[TA] = DHMF[TA]-(DSMF[TA]*(273.15+37))/1000;
  DHMF[CA] = DHMF[TG] = -8.5; DSMF[CA] = DSMF[TG] = -22.725; DGMF[CA] = DGMF[TG] = DHMF[TG]-(DSMF[TG]*(273.15+37))/1000;
  DHMF[GT] = DHMF[AC] = -8.4; DSMF[GT] = DSMF[AC] = -22.45; DGMF[GT] = DGMF[AC] = DHMF[GT]-(DSMF[GT]*(273.15+37))/1000;
  DHMF[CT] = DHMF[AG] = -7.8; DSMF[CT] = DSMF[AG] = -21.025; DGMF[CT] = DGMF[AG] = DHMF[CT]-(DSMF[CT]*(273.15+37))/1000;
  DHMF[GA] = DHMF[TC] = -8.2; DSMF[GA] = DSMF[TC] = -22.25; DGMF[GA] = DGMF[TC] = DHMF[GA]-(DSMF[GA]*(273.15+37))/1000;
  DHMF[CG] = -10.6; DSMF[CG] = -27.2; DGMF[CG] = DHMF[CG]-(DSMF[CG]*(273.15+37))/1000;
  DHMF[GC] = -9.8; DSMF[GC] = -24.375; DGMF[GC] = DHMF[GC]-(DSMF[GC]*(273.15+37))/1000;
  DHMF[GG] = DHMF[CC] = -8.0; DSMF[GG] = DSMF[CC] = -19.85; DGMF[GG] = DGMF[CC] = DHMF[GG]-(DSMF[GG]*(273.15+37))/1000;


  DHMF[INGC] = 0.1; DSMF[INGC] = -2.8; DGMF[INGC] = DHMF[INGC]-(DSMF[INGC]*(273.15+37))/1000;
  DHMF[INAT] = 2.3; DSMF[INAT] = 4.1; DGMF[INAT] = DHMF[INAT]-(DSMF[INAT]*(273.15+37))/1000;
  DHMF[SC] = 0.0; DSMF[SC] = -1.4; DGMF[SC] = DHMF[SC]-(DSMF[SC]*(273.15+37))/1000;

}



int compute_Tm_unified(char *oligo,  // The oligo sequence "ACGTAC..." 
		       float conc,   // The oligo concentration in microM
		       float Na,     // The Na+ concentration in mM
		       float *Tm,    // The melting temperature found
		       float *deltaH, float *deltaS, float *deltaG)
{
  register int i;
  int len, nonselfcomp, index;
  float  tmp, T0 = 273.15, R = 1.9872, dgu, dsu, dhu; // cal/K-mol
  char* coligo = NULL;
  
  if (oligo == NULL) return 1;
  coligo = strdup(oligo);
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();
  for (i = 0; i < len; i++)
    {
      if ((oligo[i] & 0x5F) == 'A') coligo[i]='T';
      else if ((oligo[i] & 0x5F) == 'T') coligo[i]='A';
      else if ((oligo[i] & 0x5F) == 'G') coligo[i]='C';
      else if ((oligo[i] & 0x5F) == 'C') coligo[i]='G';
    }
  nonselfcomp = 0;
  // we look for selfcomplementarity
  if (len%2)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < (len/2); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }

  dgu = dsu = dhu = 0; // initialization step
  dgu += DGU[IN]; dsu = DSU[IN]; dhu = DHU[IN];
  if (nonselfcomp == 0)  
    {
      dgu += DGU[SC]; dsu += DSU[SC]; dhu += DHU[SC]; // selfcomplementarity
    }
  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0; 
      if ((oligo[i-1]&0x5F) == 'C')  index = 4; 
      if ((oligo[i-1]&0x5F) == 'G')  index = 8; 
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0; 
      if ((oligo[i]&0x5F) == 'C')  index += 1; 
      if ((oligo[i]&0x5F) == 'G')  index += 2; 
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dgu += DGU[index]; dsu += DSU[index]; dhu += DHU[index];
    }
  if ((oligo[len-1]&0x5F) == 'A' || (oligo[len-1]&0x5F) == 'T')
    {
      dgu += DGU[INAT]; dsu += DSU[INAT]; dhu += DHU[INAT];
    }
  //else     {       dgu += DGU[INGC]; dsu += DSU[INGC]; dhu += DHU[INGC];    }
  if ((oligo[0]&0x5F) == 'A' || (oligo[0]&0x5F) == 'T')
    {
      dgu += DGU[INAT]; dsu += DSU[INAT]; dhu += DHU[INAT];
    }
  //else     {      dgu += DGU[INGC]; dsu += DSU[INGC]; dhu += DHU[INGC];    }

  Na = 0.001*Na; // we are in mM
  // \Delta S[Na+] = \Delta S[Na+ = 1M] + 0.368 * N * ln[Na+]

  dsu += 0.368 * len * log(Na);

  if (deltaH) *deltaH = dhu;
  if (deltaS) *deltaS = dsu;
  if (deltaG) *deltaG = dgu;

  // TM = \Delta H/(\Delta S + R ln CT),

  tmp = 0.000001*conc*2; // we are in microM
  if (nonselfcomp != 0) tmp /= 4;
  tmp = R * log(tmp);
  tmp += dsu;
  tmp = ((float)1000)/tmp;
  if (Tm) *Tm = (dhu * tmp) - T0;
  free(coligo);
  return 0;
}


void init_Unified_98_array(void)
{

// unified parameter santaLucia 1998 PNAS
// overwritten

  DHU98[AA] = DHU98[TT] = -7.9; DSU98[AA] = DSU98[TT] = -22.2; DGU98[AA] = DGU98[TT] = -1.00;
  DHU98[AT] = -7.2; DSU98[AT] = -20.4; DGU98[AT] = -0.88;
  DHU98[TA] = -7.2; DSU98[TA] = -21.3; DGU98[TA] = -0.58;
  DHU98[CA] = DHU98[TG] = -8.5; DSU98[CA] = DSU98[TG] = -22.7; DGU98[CA] = DGU98[TG] = -1.45;
  DHU98[GT] = DHU98[AC] = -8.4; DSU98[GT] = DSU98[AC] = -22.4; DGU98[GT] = DGU98[AC] = -1.44;
  DHU98[CT] = DHU98[AG] = -7.8; DSU98[CT] = DSU98[AG] = -21.0; DGU98[CT] = DGU98[AG] = -1.28;
  DHU98[GA] = DHU98[TC] = -8.2; DSU98[GA] = DSU98[TC] = -22.2; DGU98[GA] = DGU98[TC] = -1.30;
  DHU98[CG] = -10.6; DSU98[CG] = -27.2; DGU98[CG] = -2.17;
  DHU98[GC] = -9.8; DSU98[GC] = -24.4; DGU98[GC] = -2.24;
  DHU98[GG] = DHU98[CC] = -8.0; DSU98[GG] = DSU98[CC] = -19.9; DGU98[GG] = DGU98[CC] = -1.84;

  DHU98[INGC] = 0.1; DSU98[INGC] = -2.8; DGU98[INGC] = 0.98; 
  DHU98[INAT] = 2.3; DSU98[INAT] = 4.1; DGU98[INAT] = 1.03; 
  DHU98[SC] = 0.0; DSU98[SC] = -1.4; DGU98[SC] = 0.43; 

}

int compute_Tm_unified_98(char *oligo,  // The oligo sequence "ACGTAC..." 
			 float conc,   // The oligo concentration in microM
			 float Na,     // The Na+ concentration in mM
			 float *Tm,    // The melting temperature found
			 float *deltaH, float *deltaS, float *deltaG)
{
  register int i;
  int len, nonselfcomp, index;
  float  tmp, T0 = 273.15, R = 1.9872, dgu, dsu, dhu; // cal/K-mol
  char* coligo = NULL;
  
  if (oligo == NULL) return 1;
  coligo = strdup(oligo);
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();
  for (i = 0; i < len; i++)
    {
      if ((oligo[i] & 0x5F) == 'A') coligo[i]='T';
      else if ((oligo[i] & 0x5F) == 'T') coligo[i]='A';
      else if ((oligo[i] & 0x5F) == 'G') coligo[i]='C';
      else if ((oligo[i] & 0x5F) == 'C') coligo[i]='G';
    }
  nonselfcomp = 0;
  // we look for selfcomplementarity
  if (len%2)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < (len/2); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }

  dgu = dsu = dhu = 0; // initialization step
  //dgu += DGU98[IN]; dsu = DSU98[IN]; dhu = DHU98[IN];
  if (nonselfcomp == 0)  
    {
      dgu += DGU98[SC]; dsu += DSU98[SC]; dhu += DHU98[SC]; // selfcomplementarity
    }
  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0; 
      if ((oligo[i-1]&0x5F) == 'C')  index = 4; 
      if ((oligo[i-1]&0x5F) == 'G')  index = 8; 
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0; 
      if ((oligo[i]&0x5F) == 'C')  index += 1; 
      if ((oligo[i]&0x5F) == 'G')  index += 2; 
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dgu += DGU98[index]; dsu += DSU98[index]; dhu += DHU98[index];
    }
  if ((oligo[len-1]&0x5F) == 'A' || (oligo[len-1]&0x5F) == 'T')
    {      dgu += DGU98[INAT]; dsu += DSU98[INAT]; dhu += DHU98[INAT];    }
  else     
    {       dgu += DGU98[INGC]; dsu += DSU98[INGC]; dhu += DHU98[INGC];    }
  if ((oligo[0]&0x5F) == 'A' || (oligo[0]&0x5F) == 'T')
    {      dgu += DGU98[INAT]; dsu += DSU98[INAT]; dhu += DHU98[INAT];    }
  else     
    {      dgu += DGU98[INGC]; dsu += DSU98[INGC]; dhu += DHU98[INGC];    }

  Na = 0.001*Na; // we are in mM
  // \Delta S[Na+] = \Delta S[Na+ = 1M] + 0.368 * N * ln[Na+]

  dsu += 0.368 * len * log(Na);

  if (deltaH) *deltaH = dhu;
  if (deltaS) *deltaS = dsu;
  if (deltaG) *deltaG = dgu;

  // TM = \Delta H/(\Delta S + R ln CT),

  tmp = 0.000001*conc*2; // we are in microM
  if (nonselfcomp != 0) tmp /= 4;
  tmp = R * log(tmp);
  tmp += dsu;
  tmp = ((float)1000)/tmp;
  if (Tm) *Tm = (dhu * tmp) - T0;
  free(coligo);
  return 0;
}


int compute_Tm_unified_with_F(char *oligo,  // The oligo sequence "ACGTAC..." 
			      float conc,   // The oligo concentration in microM
			      float Na,     // The Na+ concentration in mM
			      float force,  // the stretching force
			      float *Tm,    // The melting temperature found
			      float *deltaH, float *deltaS, float *deltaG)
{
  register int i;
  int len, nonselfcomp;
  float  tmp, T0 = 273.15, R = 1.9872, dgu, dsu, dhu, dx; // cal/K-mol
  char* coligo = NULL;


  if (oligo == NULL) return 1;
  coligo = strdup(oligo);
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();
  for (i = 0; i < len; i++)
    {
      if ((oligo[i] & 0x5F) == 'A') coligo[i]='T';
      else if ((oligo[i] & 0x5F) == 'T') coligo[i]='A';
      else if ((oligo[i] & 0x5F) == 'G') coligo[i]='C';
      else if ((oligo[i] & 0x5F) == 'C') coligo[i]='G';
    }
  nonselfcomp = 0;
  // we look for selfcomplementarity
  if (len%2)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < (len/2); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }

  compute_Tm_unified(oligo, conc, Na, Tm, &dhu, &dsu, &dgu);
  win_printf("First Tm %g, dH %g dS %g dG %g",Tm,dhu,dsu,dgu);

  dx = ss_minus_ds_above_5pN(force);
  dx *= len * force; // energy in pN.nm = 10^{-21}j
  dx *= 6.024e2; // j /mole
  dx /= 4.18;   // cal/mole
  dx /= 1000; // kcal/mole
  dhu += dx;

  if (deltaH) *deltaH = dhu;
  if (deltaS) *deltaS = dsu;
  if (deltaG) *deltaG = dgu;

  tmp = 0.000001*conc*2; // we are in microM
  if (nonselfcomp != 0) tmp /= 4;
  tmp = R * log(tmp);
  tmp += dsu;
  tmp = ((float)1000)/tmp;
  if (Tm) *Tm = (dhu * tmp) - T0;

  free(coligo);
  return 0;
}


int compute_Cc_from_Tm_unified(char *oligo, float *conc, float Na, float Tm, float *deltaH, float *deltaS, float *deltaG)
{
  register int i;
  int len, nonselfcomp, index;
  float  tmp, T0 = 273.15, R = 1.9872, dgu, dsu, dhu; // cal/K-mol
  char* coligo = NULL;

  if (oligo == NULL) return 1;
  coligo = strdup(oligo);
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();
  for (i = 0; i < len; i++)
    {
      if ((oligo[i] & 0x5F) == 'A') coligo[i]='T';
      else if ((oligo[i] & 0x5F) == 'T') coligo[i]='A';
      else if ((oligo[i] & 0x5F) == 'G') coligo[i]='C';
      else if ((oligo[i] & 0x5F) == 'C') coligo[i]='G';
    }
  nonselfcomp = 0;
  // we look for selfcomplementarity
  if (len%2)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < (len/2); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }

  dgu = dsu = dhu = 0; // initialization step
  if (nonselfcomp == 0)  
    {
      dgu += DGU[SC]; dsu += DSU[SC]; dhu += DHU[SC]; // selfcomplementarity
    }
  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0; 
      if ((oligo[i-1]&0x5F) == 'C')  index = 4; 
      if ((oligo[i-1]&0x5F) == 'G')  index = 8; 
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0; 
      if ((oligo[i]&0x5F) == 'C')  index += 1; 
      if ((oligo[i]&0x5F) == 'G')  index += 2; 
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dgu += DGU[index]; dsu += DSU[index]; dhu += DHU[index];
    }
  if ((oligo[len-1]&0x5F) == 'A' || (oligo[len-1]&0x5F) == 'T')
    {
      dgu += DGU[INAT]; dsu += DSU[INAT]; dhu += DHU[INAT];
    }
  else
    {
      dgu += DGU[INGC]; dsu += DSU[INGC]; dhu += DHU[INGC];
    }
  if ((oligo[0]&0x5F) == 'A' || (oligo[0]&0x5F) == 'T')
    {
      dgu += DGU[INAT]; dsu += DSU[INAT]; dhu += DHU[INAT];
    }
  else
    {
      dgu += DGU[INGC]; dsu += DSU[INGC]; dhu += DHU[INGC];
    }

  Na = 0.001*Na; // we are in mM
  // \Delta S[Na+] = \Delta S[Na+ = 1M] + 0.368 * N * ln[Na+]

  dsu += 0.368 * len * log(Na);

  if (deltaH) *deltaH = dhu;
  if (deltaS) *deltaS = dsu;
  if (deltaG) *deltaG = dgu;


  Tm += T0;
  tmp = (dhu * 1000)/Tm;
  tmp -= dsu;
  tmp /= R;
  tmp = exp(tmp);
  if (nonselfcomp == 1) tmp *= 4;  
  if (conc) *conc = tmp * 1000000;
  free(coligo);
  return 0;
}


int compute_Kd_from_Tm_unified_with_F(char *oligo,  // The oligo sequence "ACGTAC..." 
				      float *conc,   // The oligo concentration in microM
				      float Na,     // The Na+ concentration in mM
				      float force,  // the stretching force
				      float Tm,    // The melting temperature found
				      float *deltaH, float *deltaS, float *deltaG)
{
  register int i;
  int len, nonselfcomp;
  float  Tmo = 0, T0 = 273.15, R = 1.9872, dgu, dsu, dhu, dx; // cal/K-mol
  char* coligo = NULL;
  double tmp;

  if (oligo == NULL) return 1;
  coligo = strdup(oligo);
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();
  for (i = 0; i < len; i++)
    {
      if ((oligo[i] & 0x5F) == 'A') coligo[i]='T';
      else if ((oligo[i] & 0x5F) == 'T') coligo[i]='A';
      else if ((oligo[i] & 0x5F) == 'G') coligo[i]='C';
      else if ((oligo[i] & 0x5F) == 'C') coligo[i]='G';
    }
  nonselfcomp = 0;
  // we look for selfcomplementarity
  if (len%2)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < (len/2); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }

  compute_Tm_unified(oligo, 1.0, Na, &Tmo, &dhu, &dsu, &dgu);
  win_printf("First Tm %g, dH %g dS %g dG %g",Tmo,dhu,dsu,dgu);
  dx = ss_minus_ds_above_5pN(force);
  //win_printf("dx = %g",dx);
  dx *= len * force; // energy in pN.nm = 10^{-21}j
  dx *= 6.024e2; // j /mole
  dx /= 4.18;   // cal/mole
  dx /= 1000; // kcal/mole
  //win_printf("ddH = %g",dx);
  dhu += dx;

  if (deltaH) *deltaH = dhu;
  if (deltaS) *deltaS = dsu;
  if (deltaG) *deltaG = dgu;


  Tm += T0;
  win_printf("T K = %g dhu %g",Tm,dhu);
  tmp = (dhu * 1000)/Tm;
  win_printf("2 tmp = %g",tmp);
  tmp -= dsu;
  tmp /= R;
  win_printf("3 tmp = %g",tmp);
  tmp = exp(tmp);
  win_printf("4 tmp = %g",tmp);
  if (nonselfcomp == 1) tmp *= 4;
  win_printf("tmp = %g",tmp);
  if (conc) *conc = tmp * 1000000;
  free(coligo);
  return 0;
}


int compute_Tm_Mfold3_1(char *oligo,  // The oligo sequence "ACGTAC..." 
			float conc,   // The oligo concentration in microM
			float Na,     // The Na+ concentration in mM
			float Mg,     // The Na+ concentration in mM
			float *Tm,    // Initial value, the T to compute energies, return value The melting temperature found
			float *deltaH, float *deltaS, float *deltaG)
{
  register int i;
  int len, nonselfcomp, index;
  float  tmp, T0 = 273.15, R = 1.9872, dgu, dsu, dhu, Ti; // cal/K-mol
  char* coligo = NULL;
  
  if (oligo == NULL) return 1;
  coligo = strdup(oligo);
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();
  if (Tm) Ti = *Tm; 
  for (i = 0; i < len; i++)
    {
      if ((oligo[i] & 0x5F) == 'A') coligo[i]='T';
      else if ((oligo[i] & 0x5F) == 'T') coligo[i]='A';
      else if ((oligo[i] & 0x5F) == 'G') coligo[i]='C';
      else if ((oligo[i] & 0x5F) == 'C') coligo[i]='G';
    }
  nonselfcomp = 0;
  // we look for selfcomplementarity
  if (len%2)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < (len/2); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }

  dgu = dsu = dhu = 0; // initialization step
  //dgu += DGMF[IN]; dsu = DSMF[IN]; dhu = DHMF[IN];
  if (nonselfcomp == 0)  
    {
      dgu += DGMF[SC]; dsu += DSMF[SC]; dhu += DHMF[SC]; // selfcomplementarity
    }
  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0; 
      if ((oligo[i-1]&0x5F) == 'C')  index = 4; 
      if ((oligo[i-1]&0x5F) == 'G')  index = 8; 
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0; 
      if ((oligo[i]&0x5F) == 'C')  index += 1; 
      if ((oligo[i]&0x5F) == 'G')  index += 2; 
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dgu += DGMF[index]; dsu += DSMF[index]; dhu += DHMF[index];
    }
  if ((oligo[len-1]&0x5F) == 'A' || (oligo[len-1]&0x5F) == 'T')
    {      dgu += DGMF[INAT]; dsu += DSMF[INAT]; dhu += DHMF[INAT];    }
  else     
    {       dgu += DGMF[INGC]; dsu += DSMF[INGC]; dhu += DHMF[INGC];    }
  if ((oligo[0]&0x5F) == 'A' || (oligo[0]&0x5F) == 'T')
    {      dgu += DGMF[INAT]; dsu += DSMF[INAT]; dhu += DHMF[INAT];    }
  else     
    {      dgu += DGMF[INGC]; dsu += DSMF[INGC]; dhu += DHMF[INGC];    }

  Na = 0.001*Na; // we are in mM
  Mg = 0.001*Mg; // we are in mM
  // \Delta S[Na+] = \Delta S[Na+ = 1M] + 0.368 * N * ln[Na+]
  //dsu += 0.368 * len * log(Na);

  dsu += 0.368*len*log(Na+3.2*sqrt(Mg));

  dgu = dhu - (dsu *(Ti+273.15))/1000;
  if (deltaH) *deltaH = dhu;
  if (deltaS) *deltaS = dsu;
  if (deltaG) *deltaG = dgu;

  // TM = \Delta H/(\Delta S + R ln CT),

  tmp = 0.000001*conc*2; // we are in microM
  if (nonselfcomp != 0) tmp /= 4;
  tmp = R * log(tmp);
  tmp += dsu;
  tmp = ((float)1000)/tmp;
  if (Tm) *Tm = (dhu * tmp) - T0;
  free(coligo);
  return 0;
}

int compute_Kd_from_Mfold_with_F(char *oligo,  // The oligo sequence "ACGTAC..." 
				 float *conc,   // The oligo concentration in microM
				 float Na,     // The Na+ concentration in mM
				 float Mg,     // The Mg2++ concentration in mM
				 float force,  // the stretching force
				 float Tm,    // The melting temperature found
				 float *deltaH, float *deltaS, float *deltaG)
{
  register int i;
  int len, nonselfcomp;
  float  Tmo = 0, T0 = 273.15, R = 1.9872, dgu, dsu, dhu, dx, mu = 1; // cal/K-mol
  char* coligo = NULL;
  double tmp;

  if (oligo == NULL) return 1;
  coligo = strdup(oligo);
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();
  for (i = 0; i < len; i++)
    {
      if ((oligo[i] & 0x5F) == 'A') coligo[i]='T';
      else if ((oligo[i] & 0x5F) == 'T') coligo[i]='A';
      else if ((oligo[i] & 0x5F) == 'G') coligo[i]='C';
      else if ((oligo[i] & 0x5F) == 'C') coligo[i]='G';
    }
  nonselfcomp = 0;
  // we look for selfcomplementarity
  if (len%2)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < (len/2); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  Tmo = Tm;
  compute_Tm_Mfold3_1(oligo, 1.0, Na, Mg, &Tmo, &dhu, &dsu, &dgu);
  win_printf("First Tm %g, dH %g dS %g dG %g",Tmo,dhu,dsu,dgu);
  dx = ss_minus_ds_above_5pN(force);
  win_printf("dx = %g nm per base",dx);
  dx *= len * force; // energy in pN.nm = 10^{-21}j
  dx *= 602.214; // j /mole
  dx /= 4.18;   // cal/mole
  dx /= 1000; // kcal/mole
  win_printf("ddH = %g Kcal/mole",dx);
  dhu += dx;

  dgu += dx;

  if (deltaH) *deltaH = dhu;
  if (deltaS) *deltaS = dsu;
  if (deltaG) *deltaG = dgu;


  Tm += T0;
  //win_printf("T K = %g dhu %g",Tm,dhu);
  tmp = (dhu * 1000)/Tm;
  //win_printf("2 tmp = %g",tmp);
  tmp -= dsu;
  tmp /= R;
  //win_printf("3 tmp = %g",tmp);
  tmp = exp(tmp);
  //win_printf("4 tmp = %g",tmp);
  mu = 1;
  if (nonselfcomp != 0) 
    {
      tmp *= 4;
      mu = 4;
    }
  win_printf("Kd1 = %g \\mu M,  Kd2 = %g"
	     ,tmp* 1000000, 1000000*mu*exp(1000*dgu/(R*(Tm))));

 

  if (conc) *conc = tmp * 1000000;
  free(coligo);
  return 0;
}


int compute_Kd_from_Mfold_with_F_ener2(char *oligo,  // The oligo sequence "ACGTAC..." 
				 float *conc,   // The oligo concentration in microM
				 float Na,     // The Na+ concentration in mM
				 float Mg,     // The Mg2++ concentration in mM
				 float force,  // the stretching force
				 float Tm,    // The melting temperature found
				 float *deltaH, float *deltaS, float *deltaG)
{
  register int i;
  int len, nonselfcomp;
  float  Tmo = 0, T0 = 273.15, R = 1.9872, dgu, dsu, dhu, dx, mu = 1; // cal/K-mol
  char* coligo = NULL;
  double tmp;

  if (oligo == NULL) return 1;
  coligo = strdup(oligo);
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();
  for (i = 0; i < len; i++)
    {
      if ((oligo[i] & 0x5F) == 'A') coligo[i]='T';
      else if ((oligo[i] & 0x5F) == 'T') coligo[i]='A';
      else if ((oligo[i] & 0x5F) == 'G') coligo[i]='C';
      else if ((oligo[i] & 0x5F) == 'C') coligo[i]='G';
    }
  nonselfcomp = 0;
  // we look for selfcomplementarity
  if (len%2)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < (len/2); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  Tmo = Tm;
  compute_Tm_Mfold3_1(oligo, 1.0, Na, Mg, &Tmo, &dhu, &dsu, &dgu);
  win_printf("First Tm %g, dH %g dS %g dG %g",Tmo,dhu,dsu,dgu);

  //dx = integral_ss_minus_ds_above_5pN(force);
  dx = DeltaG_ss_minus_ds(force);

  //dx = ss_minus_ds_above_5pN(force);
  win_printf("dx = %g E(pN.nm) per base -> %g kBT",dx,dx/4.1);
  //dx *= len * force; // energy in pN.nm = 10^{-21}j
  dx *= len; // energy in pN.nm = 10^{-21}j
  dx *= 602.214; // j /mole
  dx /= 4.18;   // cal/mole
  dx /= 1000; // kcal/mole
  win_printf("ddH = %g Kcal/mole",dx);
  dhu += dx;

  dgu += dx;

  if (deltaH) *deltaH = dhu;
  if (deltaS) *deltaS = dsu;
  if (deltaG) *deltaG = dgu;


  Tm += T0;
  //win_printf("T K = %g dhu %g",Tm,dhu);
  tmp = (dhu * 1000)/Tm;
  //win_printf("2 tmp = %g",tmp);
  tmp -= dsu;
  tmp /= R;
  //win_printf("3 tmp = %g",tmp);
  tmp = exp(tmp);
  //win_printf("4 tmp = %g",tmp);
  mu = 1;
  if (nonselfcomp != 0) 
    {
      tmp *= 4;
      mu = 4;
    }
  win_printf("Kd1 = %g \\mu M,  Kd2 = %g"
	     ,tmp* 1000000, 1000000*mu*exp(1000*dgu/(R*(Tm))));

 

  if (conc) *conc = tmp * 1000000;
  free(coligo);
  return 0;
}



int display_Mfold3_1_DeltaG(void)
{
  register int i;
  double deltaG[20], deltaS[20], deltaH[20];
  static float Na = 1000;     // The Na+ concentration in mM
  static float Mg = 0;     // The Na+ concentration in mM
  static float Ti = 37;    // value, the T to compute energies
  double T0 = 273.15, R = 1.9872, cor; // cal/K-mol

  if(updating_menu_state != 0)	return D_O_K;

  i = win_scanf("Display enerniies of NN dinucleotides\n"
		"at T = %6f C, [Na^+] = %6f mM, [Mg^{2+}] = %6f mM\n",&Ti,&Na,&Mg); 
  if (i == WIN_CANCEL) return 0;

  if (array_init == 0) init_array();
  cor = (double)1000/(R*T0);

  for (i = 0; i < 16; i++)
    {
      deltaH[i] = DHMF[i];
      deltaS[i] = DSMF[i] + 0.368*log(0.001*Na+3.2*sqrt(0.001*Mg));
      deltaG[i] = deltaH[i] - (deltaS[i] *(Ti+273.15))/1000;
    }

  win_printf("T = %g C, [Na^+] = %g mM, [Mg^{2+}] = %g mM\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT  \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT  \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     "%s \\Delta G = %6.3f Kcal/mole %6.3f k_BT \\Delta S = %g \\Delta H = %g\n"
	     ,Ti,Na,Mg
	     ,"AA",deltaG[AA],cor*deltaG[AA],deltaS[AA],deltaH[AA]
	     ,"AC",deltaG[AC],cor*deltaG[AC],deltaS[AC],deltaH[AC]
	     ,"AG",deltaG[AG],cor*deltaG[AG],deltaS[AG],deltaH[AG]
	     ,"AT",deltaG[AT],cor*deltaG[AT],deltaS[AT],deltaH[AT]
	     ,"CA",deltaG[CA],cor*deltaG[CA],deltaS[CA],deltaH[CA]
	     ,"CC",deltaG[CC],cor*deltaG[CC],deltaS[CC],deltaH[CC]
	     ,"CG",deltaG[CG],cor*deltaG[CG],deltaS[CG],deltaH[CG]
	     ,"CT",deltaG[CT],cor*deltaG[CT],deltaS[CT],deltaH[CT]
	     ,"GA",deltaG[GA],cor*deltaG[GA],deltaS[GA],deltaH[GA]
	     ,"GC",deltaG[GC],cor*deltaG[GC],deltaS[GC],deltaH[GC]
	     ,"GG",deltaG[GG],cor*deltaG[GG],deltaS[GG],deltaH[GG]
	     ,"GT",deltaG[GT],cor*deltaG[GT],deltaS[GT],deltaH[GT]
	     ,"TA",deltaG[TA],cor*deltaG[TA],deltaS[TA],deltaH[TA]
	     ,"TC",deltaG[TC],cor*deltaG[TC],deltaS[TC],deltaH[TC]
	     ,"TG",deltaG[TG],cor*deltaG[TG],deltaS[TG],deltaH[TG]
	     ,"TT",deltaG[TT],cor*deltaG[TT],deltaS[TT],deltaH[TT]);
  return 0;
}




 void  init_LNA_array(void)
 {
  // cgttga
  // Dh = 0.2 + 0 -10.6 -8.4 -7.6 -8.5 -8.2 +2.2 = -40.9
  // dS = -5.7 +0  -27.2 -22.4 -21.3 -22.7 -22.2 +6.9 = -114.6 
  // for LNA
  D_H[AA_] = D_H[TT_] = -7.9; D_S[AA_] = D_S[TT_] = -22.2; D_G[AA_] = D_G[TT_] = -1.00;
  D_H[AT_] = -7.2; D_S[AT_] = -20.4; D_G[AT_] = -0.88;
  D_H[TA_] = -7.2; D_S[TA_] = -21.3; D_G[TA_] = -0.58;
  D_H[CA_] = D_H[TG_] = -8.5; D_S[CA_] = D_S[TG_] = -22.7; D_G[CA_] = D_G[TG_] = -1.45;
  D_H[GT_] = D_H[AC_] = -8.4; D_S[GT_] = D_S[AC_] = -22.4; D_G[GT_] = D_G[AC_] = -1.44;
  D_H[CT_] = D_H[AG_] = -7.8; D_S[CT_] = D_S[AG_] = -21.0; D_G[CT_] = D_G[AG_] = -1.28;
  D_H[GA_] = D_H[TC_] = -8.2; D_S[GA_] = D_S[TC_] = -22.2; D_G[GA_] = D_G[TC_] = -1.30;
  D_H[CG_] = -10.6; D_S[CG_] = -27.2; D_G[CG_] = -2.17;
  D_H[GC_] = -9.8; D_S[GC_] = -24.4; D_G[GC_] = -2.24;
  D_H[GG_] = D_H[CC_] = -8.0; D_S[GG_] = D_S[CC_] = -19.9; D_G[GG_] = D_G[CC_] = -1.84;


  D_H[_A_A_] = -9.991; D_S[_A_A_] = -27.175; D_G[_A_A_] = -1.57;
  D_H[_A_C_] = -11.389; D_S[_A_C_] = -28.963; D_G[_A_A_] = -2.44;
  D_H[_A_G_] = -12.793; D_S[_A_G_] = -31.607; D_G[_A_G_] = -3.07;
  D_H[_A_T_] = -14.703; D_S[_A_T_] = -40.750; D_G[_A_T_] = -2.12;
  D_H[_C_A_] = -14.177; D_S[_C_A_] = -35.498; D_G[_C_A_] = -3.11;
  D_H[_C_C_] = -15.399; D_S[_C_C_] = -36.375; D_G[_C_C_] = -4.15;
  D_H[_C_G_] = -14.558; D_S[_C_G_] = -35.239; D_G[_C_G_] = -3.65;
  D_H[_C_T_] = -15.737; D_S[_C_T_] = -41.218; D_G[_C_T_] = -2.92;
  D_H[_G_A_] = -13.959; D_S[_G_A_] = -35.097; D_G[_G_A_] = -3.03;
  D_H[_G_C_] = -16.109; D_S[_G_C_] = -40.738; D_G[_G_C_] = -3.48;
  D_H[_G_G_] = -13.022; D_S[_G_G_] = -29.673; D_G[_G_G_] = -3.82;
  D_H[_G_T_] = -17.361; D_S[_G_T_] = -45.858; D_G[_G_T_] = -3.12;
  D_H[_T_A_] = -10.318; D_S[_T_A_] = -26.108; D_G[_T_A_] = -2.19;
  D_H[_T_C_] = -9.166; D_S[_T_C_] = -21.535; D_G[_T_C_] = -2.48;
  D_H[_T_G_] = -10.046; D_S[_T_G_] = -22.591; D_G[_T_G_] = -3.08;
  D_H[_T_T_] = -10.419; D_S[_T_T_] = -27.683; D_G[_T_T_] = -1.83;


  D_H[_AA_] = -7.193; D_S[_AA_] = -19.723; D_G[_AA_] = -1.09;
  D_H[_AC_] = -7.269; D_S[_AC_] = -18.336; D_G[_AC_] = -1.56;
  D_H[_AG_] = -7.536; D_S[_AG_] = -18.387; D_G[_AG_] = -1.84;
  D_H[_AT_] = -4.918; D_S[_AT_] = -12.943; D_G[_AT_] = -0.89;
  D_H[_CA_] = -7.451; D_S[_CA_] = -18.380; D_G[_CA_] = -1.72;
  D_H[_CC_] = -5.904; D_S[_CC_] = -11.904; D_G[_CC_] = -2.30;
  D_H[_CG_] = -9.815; D_S[_CG_] = -23.491; D_G[_CG_] = -2.50;
  D_H[_CT_] = -7.092; D_S[_CT_] = -16.825; D_G[_CT_] = -1.95;
  D_H[_GA_] = -5.038; D_S[_GA_] = -11.656; D_G[_GA_] = -1.37;
  D_H[_GC_] = -10.160; D_S[_GC_] = -24.651; D_G[_GC_] = -2.65;
  D_H[_GG_] = -10.844; D_S[_GG_] = -26.580; D_G[_GG_] = -2.54;
  D_H[_GT_] = -8.612; D_S[_GT_] = -22.327; D_G[_GT_] = -1.63;
  D_H[_TA_] = -7.246; D_S[_TA_] = -19.738; D_G[_TA_] = -1.14;
  D_H[_TC_] = -6.307; D_S[_TC_] = -15.515; D_G[_TC_] = -1.51;
  D_H[_TG_] = -10.040; D_S[_TG_] = -25.744; D_G[_TG_] = -2.00;
  D_H[_TT_] = -6.372; D_S[_TT_] = -16.902; D_G[_TT_] = -1.13;
  D_H[A_A_] = -6.908; D_S[A_A_] = -18.135; D_G[A_A_] = -1.40;
  D_H[A_C_] = -5.510; D_S[A_C_] = -11.824; D_G[A_C_] = -1.83;
  D_H[A_G_] = -9.000; D_S[A_G_] = -22.826; D_G[A_G_] = -1.88;
  D_H[A_T_] = -5.384; D_S[A_T_] = -13.537; D_G[A_T_] = -1.19;
  D_H[C_A_] = -7.142; D_S[C_A_] = -18.333; D_G[C_A_] = -1.40;
  D_H[C_C_] = -5.937; D_S[C_C_] = -12.335; D_G[C_C_] = -2.24;
  D_H[C_G_] = -10.876; D_S[C_G_] = -27.918; D_G[C_G_] = -2.17;
  D_H[C_T_] = -9.471; D_S[C_T_] = -25.070; D_G[C_T_] = -1.69;
  D_H[G_A_] = -7.756; D_S[G_A_] = -19.302; D_G[G_A_] = -1.74;
  D_H[G_C_] = -10.725; D_S[G_C_] = -25.511; D_G[G_C_] = -2.78;
  D_H[G_G_] = -8.943; D_S[G_G_] = -20.833; D_G[G_G_] = -2.51;
  D_H[G_T_] = -9.035; D_S[G_T_] = -22.742; D_G[G_T_] = -1.96;
  D_H[T_A_] = -5.609; D_S[T_A_] = -16.019; D_G[T_A_] = -0.58;
  D_H[T_C_] = -7.591; D_S[T_C_] = -19.031; D_G[T_C_] = -1.70;
  D_H[T_G_] = -6.335; D_S[T_G_] = -15.537; D_G[T_G_] = -1.56;
  D_H[T_T_] = -5.574; D_S[T_T_] = -14.149; D_G[T_T_] = -1.21;
  
  D_H[INGC_] = 0.1; D_S[INGC_] = -2.8; D_G[INGC_] = 0.98; 
  D_H[INAT_] = 2.3; D_S[INAT_] = 4.1; D_G[INAT_] = 1.03; 
  D_H[SC_] = 0.0; D_S[SC_] = -1.4; D_G[SC_] = 0.43; 

}


int compute_Tm_unified_DNA_LNA(char *oligo,  // The oligo sequence "acCgTaC..."
			      //lowercase DNA, Uppercase LNA 
			      float conc,   // The oligo concentration in microM
			      float Na,     // The Na+ concentration in mM
			      float *Tm,    // The melting temperature found
			      float *deltaH, float *deltaS, float *deltaG)
{
  register int i;
  int len, nonselfcomp, index;
  float  tmp, T0 = 273.15, R = 1.9872, dgu, dsu, dhu; // cal/K-mol
  char* coligo = NULL;
  
  if (oligo == NULL) return 1;
  coligo = strdup(oligo);
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();
  for (i = 0; i < len; i++)
    {
      if ((oligo[i] & 0x5F) == 'A') coligo[i]='T';
      else if ((oligo[i] & 0x5F) == 'T') coligo[i]='A';
      else if ((oligo[i] & 0x5F) == 'G') coligo[i]='C';
      else if ((oligo[i] & 0x5F) == 'C') coligo[i]='G';
    }
  nonselfcomp = 0;
  // we look for selfcomplementarity
  if (len%2)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < (len/2); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }

  dgu = dsu = dhu = 0; // initialization step
  if (nonselfcomp == 0)  
    {
      dgu += D_G[SC_]; dsu += D_S[SC_]; dhu += D_H[SC_]; // selfcomplementarity
    }
  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]) == 'a')  index = 0; 
      if ((oligo[i-1]) == 'c')  index = 8; 
      if ((oligo[i-1]) == 'g')  index = 16; 
      if ((oligo[i-1]) == 't')  index = 24;
      if ((oligo[i-1]) == 'A')  index = 32; 
      if ((oligo[i-1]) == 'C')  index = 40; 
      if ((oligo[i-1]) == 'G')  index = 48; 
      if ((oligo[i-1]) == 'T')  index = 56;
      if ((oligo[i]) == 'a')  index += 0; 
      if ((oligo[i]) == 'c')  index += 1; 
      if ((oligo[i]) == 'g')  index += 2; 
      if ((oligo[i]) == 't')  index += 3;
      if ((oligo[i]) == 'A')  index += 4; 
      if ((oligo[i]) == 'C')  index += 5; 
      if ((oligo[i]) == 'G')  index += 6; 
      if ((oligo[i]) == 'T')  index += 7;
      dgu += D_G[index]; dsu += D_S[index]; dhu += D_H[index];
    }
  if ((oligo[len-1]&0x5F) == 'A' || (oligo[len-1]&0x5F) == 'T')
    {
      dgu += D_G[INAT_]; dsu += D_S[INAT_]; dhu += D_H[INAT_];
    }
  else
    {
      dgu += D_G[INGC_]; dsu += D_S[INGC_]; dhu += D_H[INGC_];
    }
  if ((oligo[0]&0x5F) == 'A' || (oligo[0]&0x5F) == 'T')
    {
      dgu += D_G[INAT_]; dsu += D_S[INAT_]; dhu += D_H[INAT_];
    }
  else
    {
      dgu += D_G[INGC_]; dsu += D_S[INGC_]; dhu += D_H[INGC_];
    }

  Na = 0.001*Na; // we are in mM
  // \Delta S[Na+] = \Delta S[Na+ = 1M] + 0.368 * N * ln[Na+]

  dsu += 0.368 * len * log(Na);

  if (deltaH) *deltaH = dhu;
  if (deltaS) *deltaS = dsu;
  if (deltaG) *deltaG = dgu;

  // TM = \Delta H/(\Delta S + R ln CT),

  tmp = 0.000001*conc*2; // we are in microM
  if (nonselfcomp != 0) tmp /= 4;
  tmp = R * log(tmp);
  tmp += dsu;
  tmp = ((float)1000)/tmp;
  if (Tm) *Tm = (dhu * tmp) - T0;
  free(coligo);
  return 0;
}




int compute_Cc_from_Tm(char *oligo, float *conc, float Tm, float *deltaH, float *deltaS, float *deltaG)
{
  register int i;
  int len, nonselfcomp, index;
  float  dg, ds, dh, tmp, T0 = 273.15, R = 1.9872; // cal/K-mol

  if (oligo == NULL) return 1;
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();

  nonselfcomp = 0;
  // we look for selfcomplementarity
  if (len%1)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == oligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < ((len/2) - 1); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == oligo[len-i-1]) ? 0 : 1;
    }

  dg = DG[IN]; ds = DS[IN]; dh = DH[IN]; // initialization step
  if (nonselfcomp == 0)  dg += DG[SC]; ds += DS[SC]; dh += DH[SC]; // selfcomplementarity
  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0; 
      if ((oligo[i-1]&0x5F) == 'C')  index = 4; 
      if ((oligo[i-1]&0x5F) == 'G')  index = 8; 
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0; 
      if ((oligo[i]&0x5F) == 'C')  index += 1; 
      if ((oligo[i]&0x5F) == 'G')  index += 2; 
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dg += DG[index]; ds += DS[index]; dh += DH[index];
    }
  if ((oligo[len-1]&0x5F) == 'A' || (oligo[len-1]&0x5F) == 'T')
    {
      dg += DG[TP]; ds += DS[TP]; dh += DH[TP];
    }
  if ((oligo[0]&0x5F) == 'A' || (oligo[0]&0x5F) == 'T')
    {
      dg += DG[TP]; ds += DS[TP]; dh += DH[TP];
    }
  if (deltaH) *deltaH = dh;
  if (deltaS) *deltaS = ds;
  if (deltaG) *deltaG = dg;

  Tm += T0;
  tmp = (dh * 1000)/Tm;
  tmp -= ds;
  tmp /= R;
  tmp = exp(tmp);
  if (nonselfcomp == 1) tmp *= 4;  
  if (conc) *conc = tmp * 1000000;
  return 0;
}


void init_Ritort_array(void)
{
  //huguet PNAS 2010
  /*
Method UO values Force measurements
NNBP \epsilon_i 25 �C \Delta hi \Delta si mi \epsilon_i 25 �C \Delta hi \Delta si mi
AA/TT -1.28 -7.9 -22.2 0.114 -1.23 -7.28 (0.3) -20.28 (1.2) 0.145
AC/TG -1.72 -8.4 -22.4 0.114 -1.49 -5.80 (0.3) -14.46 (1.3) 0.099
AG/TC -1.54 -7.8 -21.0 0.114 -1.36 -5.21 (0.3) -12.89 (1.2) 0.070
AT/TA -1.12 -7.2 -20.4 0.114 -1.17 -4.63 (0.6) -11.62 (2.1) 0.117
CA/GT -1.73 -8.5 -22.7 0.114 -1.66 -8.96 (0.3) -24.48 (1.2) 0.091
CC/GG -2.07 -8.0 -19.9 0.114 -1.93 -8.57 (0.3) -22.30 (1.2) 0.063
CG/GC -2.49 -10.6 -27.2 0.114 -2.37 -9.66 (0.5) -24.43 (2.1) 0.132
GA/CT -1.58 -8.2 -22.2 0.114 -1.47 -8.16 (0.3) -22.46 (1.3) 0.155
GC/CG -2.53 -9.8 -24.4 0.114 -2.36 -10.10 (0.5) -25.96 (1.8) 0.079
TA/AT -0.85 -7.2 -21.3 0.114 -0.84 -8.31 (0.6) -25.06 (2.1) 0.091
  */

  DHR[AA] = DHR[TT] = -7.28; DSR[AA] = DSR[TT] = -20.28; MIR[AA] = MIR[TT] = 0.145; 
  DHR[AT] = -4.63; DSR[AT] = -11.62; MIR[AT] = 0.117;
  DHR[TA] = -8.31; DSR[TA] = -25.06; MIR[TA] = 0.091;
  DHR[CA] = DHR[TG] = -8.96; DSR[CA] = DSR[TG] = -24.48; MIR[CA] = MIR[TG] = 0.091;
  DHR[GT] = DHR[AC] = -5.80; DSR[GT] = DSR[AC] = -14.46; MIR[GT] = MIR[AC] = 0.099;
  DHR[CT] = DHR[AG] = -5.21; DSR[CT] = DSR[AG] = -12.89; MIR[CT] = MIR[AG] = 0.070;
  DHR[GA] = DHR[TC] = -8.16; DSR[GA] = DSR[TC] = -22.46; MIR[GA] = MIR[TC] = 0.155;
  DHR[CG] = -9.66; DSR[CG] = -24.43; MIR[CG] = 0.132;
  DHR[GC] = -10.10; DSR[GC] = -25.96; MIR[GC] = 0.079;
  DHR[GG] = DHR[CC] = -8.57; DSR[GG] = DSR[CC] = -22.30; MIR[GG] = MIR[CC] = 0.063;

// from unified parameter santaLucia 1998 PNAS

//  DHR[INGC] = 0.1; DSR[INGC] = -2.8; //DGU[INGC] = 1.96;
//  DHR[INAT] = 2.3; DSR[INAT] = 4.1; //DGU[INAT] = 0.05;
//  DHR[SC] = 0.0; DSR[SC] = -1.4; //DGU[SC] = 0.43;   
// from table S1 Huguet (pb Delta S is in cal/mol/K not kcal/mol/K
  DHR[IN] = 0.2; DSR[IN] = -5.6; //DGU[INAT] = 0.05;
  DHR[INAT] = 2.2; DSR[INAT] = 6.9; //DGU[INAT] = 0.05; pb de signe !!! -6.9 in Huguet
  DHR[ATPEN] = -0.4; DSR[ATPEN] = 0.5; //DGU[INAT] = 0.05;

}



int compute_Tm_Ritort(char *oligo,  // The oligo sequence "ACGTAC..." 
		       float conc,   // The oligo concentration in microM
		       float Na,     // The Na+ concentration in mM
		       float *Tm,    // The melting temperature found
		       float *deltaH, float *deltaS, float *deltaG)
{
  register int i;
  int len, nonselfcomp, index;
  float  tmp, T0 = 273.15, R = 1.9872, dgr, dsr, dhr, mir; // cal/K-mol
  char* coligo = NULL;
  
  if (oligo == NULL) return 1;
  coligo = strdup(oligo);
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();
  for (i = 0; i < len; i++)
    {
      if ((oligo[i] & 0x5F) == 'A') coligo[i]='T';
      else if ((oligo[i] & 0x5F) == 'T') coligo[i]='A';
      else if ((oligo[i] & 0x5F) == 'G') coligo[i]='C';
      else if ((oligo[i] & 0x5F) == 'C') coligo[i]='G';
    }
  nonselfcomp = 0;
  // we look for selfcomplementarity
  if (len%2)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < (len/2); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }

  dgr = dsr = dhr = mir = 0; // initialization step
  dsr = DSR[IN]; dhr = DHR[IN];  // constant contribution
  if (nonselfcomp == 0)  
    {
      dsr += DSR[SC]; dhr += DHR[SC]; // selfcomplementarity
    }
  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0; 
      if ((oligo[i-1]&0x5F) == 'C')  index = 4; 
      if ((oligo[i-1]&0x5F) == 'G')  index = 8; 
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0; 
      if ((oligo[i]&0x5F) == 'C')  index += 1; 
      if ((oligo[i]&0x5F) == 'G')  index += 2; 
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dsr += DSR[index]; dhr += DHR[index]; mir += MIR[index];
    }
  if ((oligo[len-1]&0x5F) == 'A' || (oligo[len-1]&0x5F) == 'T')
    {
      dsr += DSR[INAT]; dhr += DHR[INAT];
    }
  //else     {    dsr = DSR[IN]; dhr = DHR[IN];    }

  if ((oligo[0]&0x5F) == 'A' || (oligo[0]&0x5F) == 'T')
    {
      dsr += DSR[INAT]; dhr += DHR[INAT];
    }
  //else    {      dsr = DSR[IN]; dhr = DHR[IN];     }
  
  if ((oligo[0]&0x5F) == 'T' && (oligo[1]&0x5F) == 'A')
    {
      dsr += DSR[ATPEN]; dhr += DHR[ATPEN];
    }
  if ((coligo[len-2]&0x5F) == 'T' && (coligo[len-1]&0x5F) == 'A')
    {
      dsr += DSR[ATPEN]; dhr += DHR[ATPEN];
    }
  mir /= 298;
  mir *= 1000; 
  // Tm = DeltaH0/(DeltaS0+R.ln[Ct/4])
  // DeltaH0 enthalpy DeltaS0 entropy at 1M Na+
  // Ct is the total oligo strand conc (1/4 for non complementary)

  Na = 0.001*Na; // we are in mM
  // \Delta S[Na+] = \Delta S[Na+ = 1M] + 0.368 * N * ln[Na+]

  dsr += mir  * log(Na); // * (len-1)

  if (deltaH) *deltaH = dhr;
  if (deltaS) *deltaS = dsr;
  dgr = dhr - 0.298 * dsr; // at 25 degrees C
  if (deltaG) *deltaG = dgr;

  // TM = \Delta H/(\Delta S + R ln CT),

  tmp = 0.000001*conc*2; // we are in microM
  if (nonselfcomp != 0) tmp /= 4;
  tmp = R * log(tmp);
  tmp += dsr;
  tmp = ((float)1000)/tmp;
  if (Tm) *Tm = (dhr * tmp) - T0;
  free(coligo);
  return 0;
}





void init_Ritort_2_array(void)
{
  //huguet PNAS 2010
  /*
Method UO values Force measurements
NNBP \epsilon_i 25 �C \Delta hi \Delta si mi \epsilon_i 25 �C \Delta hi \Delta si mi
AA/TT -1.28 -7.9 -22.2 0.114 -1.23 -7.28 (0.3) -20.28 (1.2) 0.145
AC/TG -1.72 -8.4 -22.4 0.114 -1.49 -5.80 (0.3) -14.46 (1.3) 0.099
AG/TC -1.54 -7.8 -21.0 0.114 -1.36 -5.21 (0.3) -12.89 (1.2) 0.070
AT/TA -1.12 -7.2 -20.4 0.114 -1.17 -4.63 (0.6) -11.62 (2.1) 0.117
CA/GT -1.73 -8.5 -22.7 0.114 -1.66 -8.96 (0.3) -24.48 (1.2) 0.091
CC/GG -2.07 -8.0 -19.9 0.114 -1.93 -8.57 (0.3) -22.30 (1.2) 0.063
CG/GC -2.49 -10.6 -27.2 0.114 -2.37 -9.66 (0.5) -24.43 (2.1) 0.132
GA/CT -1.58 -8.2 -22.2 0.114 -1.47 -8.16 (0.3) -22.46 (1.3) 0.155
GC/CG -2.53 -9.8 -24.4 0.114 -2.36 -10.10 (0.5) -25.96 (1.8) 0.079
TA/AT -0.85 -7.2 -21.3 0.114 -0.84 -8.31 (0.6) -25.06 (2.1) 0.091
  */

  DHR2[AA] = DHR2[TT] = -7.28; DSR2[AA] = DSR2[TT] = -20.28; MIR2[AA] = MIR2[TT] = 0.145; 
  DHR2[AT] = -4.63; DSR2[AT] = -11.62; MIR2[AT] = 0.117;
  DHR2[TA] = -8.31; DSR2[TA] = -25.06; MIR2[TA] = 0.091;
  DHR2[CA] = DHR2[TG] = -8.96; DSR2[CA] = DSR2[TG] = -24.48; MIR2[CA] = MIR2[TG] = 0.091;
  DHR2[GT] = DHR2[AC] = -5.80; DSR2[GT] = DSR2[AC] = -14.46; MIR2[GT] = MIR2[AC] = 0.099;
  DHR2[CT] = DHR2[AG] = -5.21; DSR2[CT] = DSR2[AG] = -12.89; MIR2[CT] = MIR2[AG] = 0.070;
  DHR2[GA] = DHR2[TC] = -8.16; DSR2[GA] = DSR2[TC] = -22.46; MIR2[GA] = MIR2[TC] = 0.155;
  DHR2[CG] = -9.66; DSR2[CG] = -24.43; MIR2[CG] = 0.132;
  DHR2[GC] = -10.10; DSR2[GC] = -25.96; MIR2[GC] = 0.079;
  DHR2[GG] = DHR2[CC] = -8.57; DSR2[GG] = DSR2[CC] = -22.30; MIR2[GG] = MIR2[CC] = 0.063;

// from unified parameter santaLucia 1998 PNAS

//  DHR2[INGC] = 0.1; DSR2[INGC] = -2.8; //DGU[INGC] = 1.96;
//  DHR2[INAT] = 2.3; DSR2[INAT] = 4.1; //DGU[INAT] = 0.05;
//  DHR2[SC] = 0.0; DSR2[SC] = -1.4; //DGU[SC] = 0.43;   
// from table S1 Huguet (pb Delta S is in cal/mol/K not kcal/mol/K
//  DHR2[IN] = 0.2; DSR2[IN] = -5.6; //DGU[INAT] = 0.05;
//  DHR2[INAT] = 2.2; DSR2[INAT] = 6.9; //DGU[INAT] = 0.05; pb de signe !!! -6.9 in Huguet
  DHR2[ATPEN] = -0.4; DSR2[ATPEN] = 0.5; //DGU[INAT] = 0.05;
  DHR2[INGC] = 0.1; DSR2[INGC] = -2.8; 
  DHR2[INAT] = 2.3; DSR2[INAT] = 4.1;

}



int compute_Tm_Ritort_2(char *oligo,  // The oligo sequence "ACGTAC..." 
		       float conc,   // The oligo concentration in microM
		       float Na,     // The Na+ concentration in mM
		       float *Tm,    // The melting temperature found
		       float *deltaH, float *deltaS, float *deltaG,
			float mulHl, float mulSl) // multipicative factor on H and S
{
  register int i;
  int len, nonselfcomp, index;
  float  tmp, T0 = 273.15, R = 1.9872, dgr, dsr, dhr, mir; // cal/K-mol
  char* coligo = NULL;
  
  if (oligo == NULL) return 1;
  coligo = strdup(oligo);
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();
  for (i = 0; i < len; i++)
    {
      if ((oligo[i] & 0x5F) == 'A') coligo[i]='T';
      else if ((oligo[i] & 0x5F) == 'T') coligo[i]='A';
      else if ((oligo[i] & 0x5F) == 'G') coligo[i]='C';
      else if ((oligo[i] & 0x5F) == 'C') coligo[i]='G';
    }
  nonselfcomp = 0;
  // we look for selfcomplementarity
  if (len%2)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < (len/2); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }

  dgr = dsr = dhr = mir = 0; // initialization step
  //dsr = DSR2[IN]; dhr = DHR2[IN];  // constant contribution
  if (nonselfcomp == 0)  
    {
      dsr += DSR2[SC]; dhr += DHR2[SC]; // selfcomplementarity
    }
  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0; 
      if ((oligo[i-1]&0x5F) == 'C')  index = 4; 
      if ((oligo[i-1]&0x5F) == 'G')  index = 8; 
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0; 
      if ((oligo[i]&0x5F) == 'C')  index += 1; 
      if ((oligo[i]&0x5F) == 'G')  index += 2; 
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dsr += DSR2[index]; dhr += DHR2[index]; mir += MIR2[index];
    }
  if ((oligo[len-1]&0x5F) == 'A' || (oligo[len-1]&0x5F) == 'T')
    {    dsr += DSR2[INAT]; dhr += DHR2[INAT];    }
  else     
    {    dsr += DSR2[INGC]; dhr += DHR2[INGC];    }

  if ((oligo[0]&0x5F) == 'A' || (oligo[0]&0x5F) == 'T')
    {    dsr += DSR2[INAT]; dhr += DHR2[INAT];    }
  else    
    {    dsr += DSR2[INGC]; dhr += DHR2[INGC];     }
  
  if ((oligo[0]&0x5F) == 'T' && (oligo[1]&0x5F) == 'A')
    {
      dsr += DSR2[ATPEN]; dhr += DHR2[ATPEN];
    }
  if ((oligo[len-2]&0x5F) == 'T' && (oligo[len-1]&0x5F) == 'A')
    {
      dsr += DSR2[ATPEN]; dhr += DHR2[ATPEN];
    }
  dhr *= mulHl;
  dsr *= mulSl;
  mir /= 298;
  mir *= 1000; 
  // Tm = DeltaH0/(DeltaS0+R.ln[Ct/4])
  // DeltaH0 enthalpy DeltaS0 entropy at 1M Na+
  // Ct is the total oligo strand conc (1/4 for non complementary)

  Na = 0.001*Na; // we are in mM
  // \Delta S[Na+] = \Delta S[Na+ = 1M] + 0.368 * N * ln[Na+]

  dsr += mir  * log(Na); // * (len-1)

  if (deltaH) *deltaH = dhr;
  if (deltaS) *deltaS = dsr;
  dgr = dhr - 0.298 * dsr; // at 25 degrees C
  if (deltaG) *deltaG = dgr;

  // TM = \Delta H/(\Delta S + R ln CT),

  tmp = 0.000001*conc*2; // we are in microM
  if (nonselfcomp != 0) tmp /= 4;
  tmp = R * log(tmp);
  tmp += dsr;
  tmp = ((float)1000)/tmp;
  if (Tm) *Tm = (dhr * tmp) - T0;
  free(coligo);
  return 0;
}



int compute_kd_Ritort_2_w_force(char *oligo,  // The oligo sequence "ACGTAC..." 
				float *kd,   // The oligo concentration in microM
				float Na,     // The Na+ concentration in mM
				float Tm,    // The melting temperature found
				float *deltaH, float *deltaS, float *deltaG,
				float force) // multipicative factor on H and S
{
  register int i;
  int len, nonselfcomp, index;
  float  tmp, T0 = 273.15, R = 1.9872, dgr, dsr, dhr, mir, dx; // cal/K-mol
  char* coligo = NULL;
  
  if (oligo == NULL) return 1;
  coligo = strdup(oligo);
  len = strlen(oligo);
  if (len < 2) return 1;
  if (array_init == 0) init_array();
  for (i = 0; i < len; i++)
    {
      if ((oligo[i] & 0x5F) == 'A') coligo[i]='T';
      else if ((oligo[i] & 0x5F) == 'T') coligo[i]='A';
      else if ((oligo[i] & 0x5F) == 'G') coligo[i]='C';
      else if ((oligo[i] & 0x5F) == 'C') coligo[i]='G';
    }
  nonselfcomp = 0;
  // we look for selfcomplementarity
  if (len%2)   // odd length
    {
      for (i = 0, nonselfcomp = 0; i < len/2; i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }
  else
    {
      for (i = 0, nonselfcomp = 0; i < (len/2); i++)
	nonselfcomp += ((oligo[i] & 0x5F) == coligo[len-i-1]) ? 0 : 1;
    }

  dgr = dsr = dhr = mir = 0; // initialization step
  if (nonselfcomp == 0)  
    {
      dsr += DSR2[SC]; dhr += DHR2[SC]; // selfcomplementarity
    }
  for (i = 1; i < len; i++)
    {
      index = 0;
      if ((oligo[i-1]&0x5F) == 'A')  index = 0; 
      if ((oligo[i-1]&0x5F) == 'C')  index = 4; 
      if ((oligo[i-1]&0x5F) == 'G')  index = 8; 
      if ((oligo[i-1]&0x5F) == 'T')  index = 12;
      if ((oligo[i]&0x5F) == 'A')  index += 0; 
      if ((oligo[i]&0x5F) == 'C')  index += 1; 
      if ((oligo[i]&0x5F) == 'G')  index += 2; 
      if ((oligo[i]&0x5F) == 'T')  index += 3;
      dsr += DSR2[index]; dhr += DHR2[index]; mir += MIR2[index];
    }
  if ((oligo[len-1]&0x5F) == 'A' || (oligo[len-1]&0x5F) == 'T')
    {    dsr += DSR2[INAT]; dhr += DHR2[INAT];    }
  else     
    {    dsr += DSR2[INGC]; dhr += DHR2[INGC];    }

  if ((oligo[0]&0x5F) == 'A' || (oligo[0]&0x5F) == 'T')
    {    dsr += DSR2[INAT]; dhr += DHR2[INAT];    }
  else    
    {    dsr += DSR2[INGC]; dhr += DHR2[INGC];     }
  
  if ((oligo[0]&0x5F) == 'T' && (oligo[1]&0x5F) == 'A')
    {
      dsr += DSR2[ATPEN]; dhr += DHR2[ATPEN];
    }
  if ((oligo[len-2]&0x5F) == 'T' && (oligo[len-1]&0x5F) == 'A')
    {
      dsr += DSR2[ATPEN]; dhr += DHR2[ATPEN];
    }
  dhr *= mulH;
  dsr *= mulS;
  mir /= 298;
  mir *= 1000; 
  // Tm = DeltaH0/(DeltaS0+R.ln[Ct/4])
  // DeltaH0 enthalpy DeltaS0 entropy at 1M Na+
  // Ct is the total oligo strand conc (1/4 for non complementary)

  Na = 0.001*Na; // we are in mM
  // \Delta S[Na+] = \Delta S[Na+ = 1M] + 0.368 * N * ln[Na+]

  dsr += mir  * log(Na); // * (len-1)


  dx = ss_minus_ds_above_5pN(force);
  //win_printf("dx = %g",dx);
  dx *= len * force; // energy in pN.nm = 10^{-21}j
  dx *= 6.024e2; // j /mole
  dx /= 4.18;   // cal/mole
  dx /= 1000; // kcal/mole
  //win_printf("ddH = %g",dx);
  dhr += dx;



  if (deltaH) *deltaH = dhr;
  if (deltaS) *deltaS = dsr;

  Tm += T0;

  dgr = dhr - dsr * (Tm/1000); 
  if (deltaG) *deltaG = dgr;

  tmp = (dhr * 1000)/Tm;
  tmp -= dsr;
  tmp /= R;
  tmp = exp(tmp);
  tmp /= 2; // two strands
  if (nonselfcomp) tmp *= 4;  
  if (kd) *kd = tmp * 1000000;
  free(coligo);
  return 0;
}


int do_compute_Tm_DNA_oligo(void)
{
  register int i;
  static float conc = 2, Na = 119, Mg = 0, Ti = 25;
  static char oligo[256] = {0};
  float Tm, dg, ds, dh;
  float Tmu, dgu, dsu, dhu;
  float Tmr, dgr, dsr, dhr;
  float TmM, dgM, dsM, dhM;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  if (oligo[0] == 0)
    {
      sprintf(oligo,"TACTAACATTAACTA");
    }

  i = win_scanf("Define your Oligo 5'xxxx3'%s\nand its concentration %12f in \\mu M\n"
		"Specify Temperature to compute \\Delta S T = %6f\n"
		"Give [Na^+] %8f in mM\n"
		"Give [Mg^{2+}] %8f in mM \n(will only be used in Mfold)",oligo,&conc,&Ti,&Na,&Mg);
  if (i == WIN_CANCEL) return D_O_K;
  init_array();
  Tm = Tmu = Tmr = TmM = Ti; 
  i = compute_Tm(oligo, conc, &Tm, &dh, &ds, &dg);
  if (i) win_printf_OK("error in computing Tm");
  i = compute_Tm_unified(oligo, conc, Na, &Tmu, &dhu, &dsu, &dgu);
  if (i) win_printf_OK("error in computing Tm");
  i = compute_Tm_Ritort(oligo, conc, Na, &Tmr, &dhr, &dsr, &dgr);
  if (i) win_printf_OK("error in computing Tm");
  i = compute_Tm_Mfold3_1(oligo, conc, Na, Mg, &TmM, &dhM, &dsM, &dgM);
  win_printf("Tm of %s\nat %f \\mu M equals %6.2f degrees\n"
	     "\\Delta G = %g\n"
	     "\\Delta H = %g\n"
	     "\\Delta S = %g\n"
	     "Using SantaLucia 1998 (PNAS) unified model\n"
	     "Tm of %s\nat %f \\mu M with [Na^+] %g mM\nequals %6.2f degrees\n"
	     "\\Delta G = %g\n"
	     "\\Delta H = %g\n"
	     "\\Delta S = %g\n"
	     "Using Huguet & Ritort 2010 (PNAS) model\n"
	     "Tm of %s\nat %f \\mu M with [Na^+] %g mM\nequals %6.2f degrees\n"
	     "\\Delta G = %g (25 degrees C)\n"
	     "\\Delta H = %g\n"
	     "\\Delta S = %g\n"
	     "Using Mfold 3.1\n"
	     "Tm of %s\nat %f \\mu M with [Na^+] = %g mM and [Mg^{2+}] = %g mM\nequals %6.2f degrees\n"
	     "\\Delta G = %g (%g degrees C)\n"
	     "\\Delta H = %g\n"
	     "\\Delta S = %g\n"
	     ,oligo,conc,Tm,dg,dh,ds,oligo,conc,Na,Tmu,dgu,dhu,dsu,oligo,conc,Na,Tmr,dgr,dhr,dsr,oligo,conc,Na,Mg,TmM,dgM,Ti,dhM,dsM);
  return D_O_K;  
}



int do_compute_Tm_DNA_92_oligos(void)
{
  register int i, j, k;
  float dgu, dsu, dhu;
  float dgr, dsr, dhr;
  static double chi2 = 0, chi2_1 = 0;
  pltreg *pr = NULL;
  O_p *op = NULL, *op2 = NULL, *opv = NULL;
  d_s *ds = NULL, *ds2 = NULL, *dsv = NULL;
  int len = 0, typeA = 0;
  static int Na69 = 1,Na119 = 1,Na220 = 1,Na621 = 1,Na1020 = 1;
  static int drawu = 1, noAT = 0, noGorC = 0, var = 0;//, first = 1; 
  //static float DHRIN = 0,DSRIN = 0,DHRNAT = 0,DSRINAT = 0,DHRATPEN = 0,DSRATPEN = 0; 
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
    return win_printf_OK("cannot find plot region");


  init_array();

  /*
  DHR[IN] = 0.2; DSR[IN] = -5.6; //DGU[INAT] = 0.05;
  DHR[INAT] = 2.2; DSR[INAT] = 6.9; //DGU[INAT] = 0.05; pb de signe !!! -6.9 in Huguet
  DHR[ATPEN] = -0.4; DSR[ATPEN] = 0.5; //DGU[INAT] = 0.05;
  */

  i = win_scanf("define end conditions for Ritort\n"
		"Draw unified %b Draw Tm variation %b\n"
		"[Na^+] %b69mM %b119mM %b220nM %b621mM %b1020mM\n"
		"A or T ends : %Rnone, %r1 %r2 \n"
		"Initiation \\Delta H %6f \\Delta S %6f\n"
		"No A or T penality \\Delta H %6f \\Delta S %6f\n"
		"%bAT penality \\Delta H %6f \\Delta S %6f\n"
		,&drawu,&var,&Na69,&Na119,&Na220,&Na621,&Na1020,&noGorC,DHR+IN,DSR+IN
		,DHR+INAT,DSR+INAT,&noAT,DHR+ATPEN,DSR+ATPEN);

  if (i == WIN_CANCEL) return D_O_K;

  if (drawu)
    {
      op = create_and_attach_one_plot(pr, 5, 5, 0);
      if (op == NULL)     return win_printf_OK("cannot create plot");
      ds = op->dat[0];
      op->need_to_refresh = 1; 
      set_plot_title(op, "\\stack{{Tm for oligos}");
      set_plot_x_title(op, "Tm experimental");
      set_plot_y_title(op, "Tm Unified");
    }

  if (var)
    {
      opv = create_and_attach_one_plot(pr, 5, 5, 0);
      if (opv == NULL)     return win_printf_OK("cannot create plot");
      dsv = opv->dat[0];
      opv->need_to_refresh = 1; 
      set_plot_title(opv, "\\stack{{Tm for oligos}");
      set_plot_x_title(opv, "Tm experimental - Tm0");
      set_plot_y_title(opv, "Tm Unified - Tm0");
    }

  op2 = create_and_attach_one_plot(pr, 5, 5, 0);
  if (op2 == NULL)     return win_printf_OK("cannot create plot");
  ds2 = op2->dat[0];
  op2->need_to_refresh = 1; 
  ds2->nx = ds2->ny = 0;

  set_plot_x_title(op2, "Tm experimental");
  set_plot_y_title(op2, "Tm Ritort");



  for(i = 0, chi2_1 = chi2, k = 0; i < 92; i++)
    {
      typeA = 0;
      len = strlen(owz[i].seq);
      if (((owz[i].seq[0] & 0x5F) == 'A') || ((owz[i].seq[0] & 0x5F) == 'T')) typeA++;
      if (((owz[i].seq[len-1] & 0x5F) == 'A') || ((owz[i].seq[len-1] & 0x5F) == 'T')) typeA++;

      if (typeA != noGorC) continue;
      /*
      if (noAorT)
	{
	  if (((owz[i].seq[0] & 0x5F) == 'A') || ((owz[i].seq[0] & 0x5F) == 'T'))
	    continue;
	  len = strlen(owz[i].seq);
	  if (((owz[i].seq[len-1] & 0x5F) == 'A') || ((owz[i].seq[len-1] & 0x5F) == 'T'))
	    continue;
	}
      if (noGorC)
	{
	  if (((owz[i].seq[0] & 0x5F) == 'C') || ((owz[i].seq[0] & 0x5F) == 'G'))
	    continue;
	  len = strlen(owz[i].seq);
	  if (((owz[i].seq[len-1] & 0x5F) == 'C') || ((owz[i].seq[len-1] & 0x5F) == 'G'))
	    continue;
	}
      */
      if (noAT)
	{
	  if (((owz[i].seq[0] & 0x5F) == 'T') && ((owz[i].seq[1] & 0x5F) == 'A'))
	    continue;
	  len = strlen(owz[i].seq);
	  if (((owz[i].seq[len-2] & 0x5F) == 'A') && ((owz[i].seq[len-1] & 0x5F) == 'T'))
	    continue;
	}
      if (drawu)
	{
	  if (ds == NULL) ds = create_and_attach_one_ds(op, 5, 5, 0);
	  if (ds == NULL)     return win_printf_OK("cannot create plot");
	  set_ds_source(ds,"Tm Unified for oligo %s at %g \\mu M",owz[i].seq,owz[i].Conc);
	  set_dot_line(ds);
	  set_ds_point_symbol(ds, "\\oc");
	}
      if (var)
	{
	  if (dsv == NULL) dsv = create_and_attach_one_ds(opv, 5, 5, 0);
	  if (dsv == NULL)     return win_printf_OK("cannot create plot");
	  set_ds_source(dsv,"Tm Ritort for oligo %s at %g \\mu M",owz[i].seq,owz[i].Conc);
	  set_dot_line(dsv);
	  set_ds_point_symbol(dsv, "\\oc");
	}

      if (ds2 == NULL) ds2 = create_and_attach_one_ds(op2, 5, 5, 0);
      if (ds2 == NULL)     return win_printf_OK("cannot create plot");
      ds2->nx = ds2->ny = 0;
      set_dot_line(ds2);
      set_ds_point_symbol(ds2, "\\di");

      set_ds_source(ds2,"Tm Rotort for oligo %s at %g \\mu M",owz[i].seq,owz[i].Conc);
      for (j = 0; j < 5; j++)
	{
	  if (compute_Tm_unified(owz[i].seq, owz[i].Conc, owz[i].Na[j], owz[i].TmU + j, &dhu, &dsu, &dgu))
	    win_printf_OK("error in computing Tm");
	  if (drawu)
	    {
	      ds->yd[j] = owz[i].TmU[j];
	      ds->xd[j] = owz[i].Tmx[j];
	    }
	  if (compute_Tm_Ritort(owz[i].seq, owz[i].Conc, owz[i].Na[j], owz[i].TmR + j, &dhr, &dsr, &dgr))
	    win_printf_OK("error in computing Tm");
	  if ((Na69 && j == 0) || (Na119 && j == 1) || (Na220 && j == 2) 
	      || (Na621 && j == 3) || (Na1020 && j == 4))
	    {
	      ds2->yd[ds2->ny++] = owz[i].TmR[j];
	      ds2->xd[ds2->nx++] = owz[i].Tmx[j];
	    }
	  if (dsv)
	    {
	      dsv->yd[j] = owz[i].TmR[j] - owz[i].TmR[0];
	      dsv->xd[j] = owz[i].Tmx[j] - owz[i].Tmx[0];
	    }
	  chi2 += (owz[i].TmR[j] - owz[i].Tmx[j])*(owz[i].TmR[j] - owz[i].Tmx[j]);
	  k++;
	}
      ds = NULL;
      ds2 = NULL;
      dsv = NULL;
    }
  chi2 /= k;
  chi2 = sqrt(chi2);
  
  set_plot_title(op2, "\\stack{{Tm for oligos \\chi^2 = %g %g}"		
		 "{\\pt6 Initiation \\Delta H %g \\Delta S %g}"
		"{\\pt6 A or T penality \\Delta H %g \\Delta S %g}"
		"{\\pt6 AT penality \\Delta H %g \\Delta S %g}}"
		 ,chi2,chi2 - chi2_1,DHR[IN],DSR[IN],DHR[INAT],DSR[INAT],DHR[ATPEN],DSR[ATPEN]);
  return refresh_plot(pr, pr->n_op-1);
}

int find_chi2_R2(int noGorC, int noAT, float mulHl, float mulSl, int Na[5], double *E, int *np)
{
  register int i, j, k;
  int typeA, len;
  double chi2;
  float dhr, dsr, dgr;

  for(i = 0, chi2 = 0, k = 0; i < 92; i++)
    {
      typeA = 0;
      len = strlen(owz[i].seq);
       if (((owz[i].seq[0] & 0x5F) == 'A') || ((owz[i].seq[0] & 0x5F) == 'T')) typeA++;
      if (((owz[i].seq[len-1] & 0x5F) == 'A') || ((owz[i].seq[len-1] & 0x5F) == 'T')) typeA++;

      if (noGorC != 3 && typeA != noGorC) continue;
      if (noAT)
	{
	  if (((owz[i].seq[0] & 0x5F) == 'T') && ((owz[i].seq[1] & 0x5F) == 'A'))
	    continue;
	  len = strlen(owz[i].seq);
	  if (((owz[i].seq[len-2] & 0x5F) == 'A') && ((owz[i].seq[len-1] & 0x5F) == 'T'))
	    continue;
	}
      for (j = 0; j < 5; j++)
	{
	  if (compute_Tm_Ritort_2(owz[i].seq, owz[i].Conc, owz[i].Na[j], owz[i].TmR + j, &dhr, &dsr, &dgr, mulHl, mulSl))
	    win_printf_OK("error in computing Tm");

	  if (Na[j])
	    {
	      chi2 += (owz[i].TmR[j] - owz[i].Tmx[j])*(owz[i].TmR[j] - owz[i].Tmx[j]);
	      k++;
	    }
	}
    }
  chi2 /= k;
  chi2 = sqrt(chi2);
  if (E) *E = chi2;
  if (np) *np = k;
  return 0;
}
int adjust_NN_Ritort_2(int index, float ratio, int noGorC, int noAT, int Na[5])
{
  register int i, k;
  int minmin = 0, np, km, index2;
  double E[9], Em, Em_1;
  float dhr2, dsr2;

  index2 = index;
  if (index == AA) index2 = TT; 
  else if (index == TT) index2 = AA; 
  else if (index == CA) index2 = TG; 
  else if (index == TG) index2 = CA; 
  else if (index == GT) index2 = AC; 
  else if (index == AC) index2 = GT; 
  else if (index == CT) index2 = AG; 
  else if (index == AG) index2 = CT; 
  else if (index == GA) index2 = TC; 
  else if (index == TC) index2 = GA; 
  else if (index == GG) index2 = CC; 
  else if (index == CC) index2 = GG; 

  find_chi2_R2(noGorC, noAT, mulH, mulS, Na, &Em_1, &np);
  for (i = 0, minmin = 0; minmin == 0 && i < 10000000; i++)
    {
      dsr2 = DSR2[index]; dhr2 = DHR2[index];
      find_chi2_R2(noGorC, noAT, mulH, mulS, Na, &Em, &np);
      for (k = 0, km = 4; k < 9; k++)
	{
	  if (k == 0)      
	    {
	      DHR2[index] = dhr2/ratio; DSR2[index] = dsr2/ratio;
	      DHR2[index2] = dhr2/ratio; DSR2[index2] = dsr2/ratio;
	    }
	  else if (k == 1) 
	    {
	      DHR2[index] = dhr2/ratio; DSR2[index] = dsr2;
	      DHR2[index2] = dhr2/ratio; DSR2[index2] = dsr2;
	    }
	  else if (k == 2) 
	    {
	      DHR2[index] = dhr2/ratio; DSR2[index] = dsr2*ratio;
	      DHR2[index2] = dhr2/ratio; DSR2[index2] = dsr2*ratio;
	    }
	  else if (k == 3) 
	    {
	      DHR2[index] = dhr2; DSR2[index] = dsr2/ratio;
	      DHR2[index2] = dhr2; DSR2[index2] = dsr2/ratio;
	    }
	  else if (k == 4) 
	    {
	      DHR2[index] = dhr2; DSR2[index] = dsr2;
	      DHR2[index2] = dhr2; DSR2[index2] = dsr2;
	    }
	  else if (k == 5) 
	    {
	      DHR2[index] = dhr2; DSR2[index] = dsr2*ratio;
	      DHR2[index2] = dhr2; DSR2[index2] = dsr2*ratio;
	    }
	  else if (k == 6) 
	    {
	      DHR2[index] = dhr2*ratio; DSR2[index] = dsr2/ratio;
	      DHR2[index2] = dhr2*ratio; DSR2[index2] = dsr2/ratio;
	    }
	  else if (k == 7) 
	    {
	      DHR2[index] = dhr2*ratio; DSR2[index] = dsr2;
	      DHR2[index2] = dhr2*ratio; DSR2[index2] = dsr2;
	    }
	  else if (k == 8) 
	    {
	      DHR2[index] = dhr2*ratio; DSR2[index] = dsr2*ratio;
	      DHR2[index2] = dhr2*ratio; DSR2[index2] = dsr2*ratio;
	    }
	  find_chi2_R2(noGorC, noAT, mulH, mulS, Na, E+k, &np);
	  if (E[k] <= Em)
	    {
	      Em = E[k];
	      km = k;
	    }
	}
      if (km == 0)      {dhr2 = dhr2/ratio; dsr2 = dsr2/ratio;}
      else if (km == 1) {dhr2 = dhr2/ratio; dsr2 = dsr2;}
      else if (km == 2) {dhr2 = dhr2/ratio; dsr2 = dsr2*ratio;}
      else if (km == 3) {dhr2 = dhr2; dsr2 = dsr2/ratio;}
      else if (km == 4) {minmin = 1; dhr2 = dhr2; dsr2 = dsr2;}
      else if (km == 5) {dhr2 = dhr2; dsr2 = dsr2*ratio;}
      else if (km == 6) {dhr2 = dhr2*ratio; dsr2 = dsr2/ratio;}
      else if (km == 7) {dhr2 = dhr2*ratio; dsr2 = dsr2;}
      else if (km == 8) {dhr2 = dhr2*ratio; dsr2 = dsr2*ratio;}

      DSR2[index] = dsr2; DHR2[index] = dhr2;
      if (Em > Em_1) break;
      Em_1 = Em; 
      display_title_message("iter %d km %d minmin %d H %d = %g S %d %g E %g", i,km,minmin, index,dhr2,index,dsr2,Em);
    }
  //win_printf("Optimized after %d iter km = %d",i,km);
  return 0;
}
  



int do_compute_Tm_DNA_92_oligos_2(void)
{
  register int i, j, k;
  float dgu, dsu, dhu;
  float dgr, dsr, dhr;
  static double chi2 = 0, chi2_1 = 0;
  double E[9] = {0}, Em;
  pltreg *pr = NULL;
  O_p *op = NULL, *op2 = NULL, *opv = NULL;
  d_s *ds = NULL, *ds2 = NULL, *dsv = NULL;
  int len = 0, typeA = 0, km, minmin = 0, np;
  static int Na[5] = {1};
  static int drawu = 1,  noAT = 0, noGorC = 0, var = 0;//, first = 1; 
  float mH[9] = {0}, mS[9] = {0}, ratio = 1.00001, dsr2, dhr2;

  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
    return win_printf_OK("cannot find plot region");


  init_array();
  i = win_scanf("define end conditions for Ritort\n"
		"Draw unified %b Draw Tm variation %b\n"
		"[Na^+] %b69mM %b119mM %b220nM %b621mM %b1020mM\n"
		"A or T ends : %Rnone, %r1 %r2 %r all cases\n"
		"Multiplification factor on \\Delta H %6f\n"
		"Multiplification factor on \\Delta S %6f\n"
		"Initiation G or C \\Delta H %6f \\Delta S %6f\n"
		"Initiation A or T  \\Delta H %6f \\Delta S %6f\n"
		"%bAT penality \\Delta H %6f \\Delta S %6f\n"
		,&drawu,&var,Na,Na+1,Na+2,Na+3,Na+4,&noGorC,&mulH,&mulS
		,DHR2+INGC,DSR2+INGC,DHR2+INAT,DSR2+INAT,&noAT,DHR2+ATPEN,DSR2+ATPEN);

  if (i == WIN_CANCEL) return D_O_K;

  if (drawu)
    {
      op = create_and_attach_one_plot(pr, 5, 5, 0);
      if (op == NULL)     return win_printf_OK("cannot create plot");
      ds = op->dat[0];
      op->need_to_refresh = 1; 
      set_plot_title(op, "\\stack{{Tm for oligos}");
      set_plot_x_title(op, "Tm experimental");
      set_plot_y_title(op, "Tm Unified");
    }

  if (var)
    {
      opv = create_and_attach_one_plot(pr, 5, 5, 0);
      if (opv == NULL)     return win_printf_OK("cannot create plot");
      dsv = opv->dat[0];
      opv->need_to_refresh = 1; 
      set_plot_title(opv, "\\stack{{Tm for oligos}");
      set_plot_x_title(opv, "Tm experimental - Tm0");
      set_plot_y_title(opv, "Tm Unified - Tm0");
    }

  op2 = create_and_attach_one_plot(pr, 5, 5, 0);
  if (op2 == NULL)     return win_printf_OK("cannot create plot");
  ds2 = op2->dat[0];
  op2->need_to_refresh = 1; 
  ds2->nx = ds2->ny = 0;

  set_plot_x_title(op2, "Tm experimental");
  set_plot_y_title(op2, "Tm Ritort");


  for (i = 0, minmin = 0; minmin == 0 && i < 10000000; i++)
    {
      mH[0] = mulH/ratio; mS[0] = mulS/ratio;
      mH[1] = mulH/ratio; mS[1] = mulS;
      mH[2] = mulH/ratio; mS[2] = mulS*ratio;

      mH[3] = mulH; mS[3] = mulS/ratio;
      mH[4] = mulH; mS[4] = mulS;
      mH[5] = mulH; mS[5] = mulS*ratio;

      mH[6] = mulH*ratio; mS[6] = mulS/ratio;
      mH[7] = mulH*ratio; mS[7] = mulS;
      mH[8] = mulH*ratio; mS[8] = mulS*ratio;
      
      for (k = 0, Em = FLT_MAX; k < 9; k++)
	{
	  find_chi2_R2(noGorC, noAT, mH[k], mS[k], Na, E+k, &np);
	  if (E[k] <= Em)
	    {
	      Em = E[k];
	      km = k;
	    }
	}
      if (km == 4) minmin = 1;
      mulH = mH[km];
      mulS = mS[km];
      display_title_message("iter %d minmin %d mutH %g mutS %g E %g", i,minmin, mulH,mulS,Em);
    }

  for (i = 0, minmin = 0; noGorC != 2 && minmin == 0 && i < 10000000; i++)
    {
      dsr2 = DSR2[INGC]; dhr2 = DHR2[INGC];
      for (k = 0, Em = FLT_MAX; k < 9; k++)
	{
	  if (k == 0)      {DHR2[INGC] = dhr2/ratio; DSR2[INGC] = dsr2/ratio;}
	  else if (k == 1) {DHR2[INGC] = dhr2/ratio; DSR2[INGC] = dsr2;}
	  else if (k == 2) {DHR2[INGC] = dhr2/ratio; DSR2[INGC] = dsr2*ratio;}
	  else if (k == 3) {DHR2[INGC] = dhr2; DSR2[INGC] = dsr2/ratio;}
	  else if (k == 4) {DHR2[INGC] = dhr2; DSR2[INGC] = dsr2;}
	  else if (k == 5) {DHR2[INGC] = dhr2; DSR2[INGC] = dsr2*ratio;}
	  else if (k == 6) {DHR2[INGC] = dhr2*ratio; DSR2[INGC] = dsr2/ratio;}
	  else if (k == 7) {DHR2[INGC] = dhr2*ratio; DSR2[INGC] = dsr2;}
	  else if (k == 8) {DHR2[INGC] = dhr2*ratio; DSR2[INGC] = dsr2*ratio;}
	  find_chi2_R2(noGorC, noAT, mulH, mulS, Na, E+k, &np);
	  if (E[k] <= Em)
	    {
	      Em = E[k];
	      km = k;
	    }
	}
      if (km == 4) minmin = 1;
      if (km == 0)      {dhr2 = dhr2/ratio; dsr2 = dsr2/ratio;}
      else if (km == 1) {dhr2 = dhr2/ratio; dsr2 = dsr2;}
      else if (km == 2) {dhr2 = dhr2/ratio; dsr2 = dsr2*ratio;}
      else if (km == 3) {dhr2 = dhr2; dsr2 = dsr2/ratio;}
      else if (km == 4) {dhr2 = dhr2; dsr2 = dsr2;}
      else if (km == 5) {dhr2 = dhr2; dsr2 = dsr2*ratio;}
      else if (km == 6) {dhr2 = dhr2*ratio; dsr2 = dsr2/ratio;}
      else if (km == 7) {dhr2 = dhr2*ratio; dsr2 = dsr2;}
      else if (km == 8) {dhr2 = dhr2*ratio; dsr2 = dsr2*ratio;}
      display_title_message("iter %d minmin %d H INGC %g S INGC %g E %g", i,minmin, dhr2,dsr2,Em);
      DSR2[INGC] = dsr2; DHR2[INGC] = dhr2;
    }
  //win_printf("Optimized after %d iter km = %d",i,km);


  for (i = 0, minmin = 0; noGorC != 0 && minmin == 0 && i < 10000000; i++)
    {
      dsr2 = DSR2[INAT]; dhr2 = DHR2[INAT];
      for (k = 0, Em = FLT_MAX; k < 9; k++)
	{
	  if (k == 0)      {DHR2[INAT] = dhr2/ratio; DSR2[INAT] = dsr2/ratio;}
	  else if (k == 1) {DHR2[INAT] = dhr2/ratio; DSR2[INAT] = dsr2;}
	  else if (k == 2) {DHR2[INAT] = dhr2/ratio; DSR2[INAT] = dsr2*ratio;}
	  else if (k == 3) {DHR2[INAT] = dhr2; DSR2[INAT] = dsr2/ratio;}
	  else if (k == 4) {DHR2[INAT] = dhr2; DSR2[INAT] = dsr2;}
	  else if (k == 5) {DHR2[INAT] = dhr2; DSR2[INAT] = dsr2*ratio;}
	  else if (k == 6) {DHR2[INAT] = dhr2*ratio; DSR2[INAT] = dsr2/ratio;}
	  else if (k == 7) {DHR2[INAT] = dhr2*ratio; DSR2[INAT] = dsr2;}
	  else if (k == 8) {DHR2[INAT] = dhr2*ratio; DSR2[INAT] = dsr2*ratio;}
	  find_chi2_R2(noGorC, noAT, mulH, mulS, Na, E+k, &np);
	  if (E[k] <= Em)
	    {
	      Em = E[k];
	      km = k;
	    }
	}
      if (km == 4) minmin = 1;
      if (km == 0)      {dhr2 = dhr2/ratio; dsr2 = dsr2/ratio;}
      else if (km == 1) {dhr2 = dhr2/ratio; dsr2 = dsr2;}
      else if (km == 2) {dhr2 = dhr2/ratio; dsr2 = dsr2*ratio;}
      else if (km == 3) {dhr2 = dhr2; dsr2 = dsr2/ratio;}
      else if (km == 4) {dhr2 = dhr2; dsr2 = dsr2;}
      else if (km == 5) {dhr2 = dhr2; dsr2 = dsr2*ratio;}
      else if (km == 6) {dhr2 = dhr2*ratio; dsr2 = dsr2/ratio;}
      else if (km == 7) {dhr2 = dhr2*ratio; dsr2 = dsr2;}
      else if (km == 8) {dhr2 = dhr2*ratio; dsr2 = dsr2*ratio;}
      display_title_message("iter %d minmin %d H INAT %g S INAT %g E %g", i,minmin, dhr2,dsr2,Em);
      DSR2[INAT] = dsr2;  DHR2[INAT] = dhr2;
    }
  //win_printf("Optimized after %d iter km = %d",i,km);

  adjust_NN_Ritort_2(ATPEN, ratio, noGorC, noAT, Na);

  adjust_NN_Ritort_2(AA, ratio, noGorC, noAT, Na);
  adjust_NN_Ritort_2(AT, ratio, noGorC, noAT, Na);
  adjust_NN_Ritort_2(TA, ratio, noGorC, noAT, Na);
  adjust_NN_Ritort_2(CA, ratio, noGorC, noAT, Na);
  adjust_NN_Ritort_2(GT, ratio, noGorC, noAT, Na);
  adjust_NN_Ritort_2(CT, ratio, noGorC, noAT, Na);
  adjust_NN_Ritort_2(GA, ratio, noGorC, noAT, Na);
  adjust_NN_Ritort_2(CG, ratio, noGorC, noAT, Na);
  adjust_NN_Ritort_2(GC, ratio, noGorC, noAT, Na);
  adjust_NN_Ritort_2(GG, ratio, noGorC, noAT, Na);

  for(i = 0, chi2_1 = chi2, k = 0; i < 92; i++)
    {
      typeA = 0;
      len = strlen(owz[i].seq);
      if (((owz[i].seq[0] & 0x5F) == 'A') || ((owz[i].seq[0] & 0x5F) == 'T')) typeA++;
      if (((owz[i].seq[len-1] & 0x5F) == 'A') || ((owz[i].seq[len-1] & 0x5F) == 'T')) typeA++;

      if (noGorC != 3 && typeA != noGorC) continue;
      if (noAT)
	{
	  if (((owz[i].seq[0] & 0x5F) == 'T') && ((owz[i].seq[1] & 0x5F) == 'A'))
	    continue;
	  len = strlen(owz[i].seq);
	  if (((owz[i].seq[len-2] & 0x5F) == 'A') && ((owz[i].seq[len-1] & 0x5F) == 'T'))
	    continue;
	}
      if (drawu)
	{
	  if (ds == NULL) ds = create_and_attach_one_ds(op, 5, 5, 0);
	  if (ds == NULL)     return win_printf_OK("cannot create plot");
	  set_ds_source(ds,"Tm Unified for oligo %s at %g \\mu M",owz[i].seq,owz[i].Conc);
	  set_dot_line(ds);
	  set_ds_point_symbol(ds, "\\oc");
	}
      if (var)
	{
	  if (dsv == NULL) dsv = create_and_attach_one_ds(opv, 5, 5, 0);
	  if (dsv == NULL)     return win_printf_OK("cannot create plot");
	  set_ds_source(dsv,"Tm Ritort for oligo %s at %g \\mu M",owz[i].seq,owz[i].Conc);
	  set_dot_line(dsv);
	  set_ds_point_symbol(dsv, "\\oc");
	}

      if (ds2 == NULL) ds2 = create_and_attach_one_ds(op2, 5, 5, 0);
      if (ds2 == NULL)     return win_printf_OK("cannot create plot");
      ds2->nx = ds2->ny = 0;
      set_dot_line(ds2);
      set_ds_point_symbol(ds2, "\\di");

      set_ds_source(ds2,"Tm Rotort for oligo %s at %g \\mu M",owz[i].seq,owz[i].Conc);
      for (j = 0; j < 5; j++)
	{
	  if (compute_Tm_unified(owz[i].seq, owz[i].Conc, owz[i].Na[j], owz[i].TmU + j, &dhu, &dsu, &dgu))
	    win_printf_OK("error in computing Tm");
	  if (drawu)
	    {
	      ds->yd[j] = owz[i].TmU[j];
	      ds->xd[j] = owz[i].Tmx[j];
	    }
	  if (compute_Tm_Ritort_2(owz[i].seq, owz[i].Conc, owz[i].Na[j], owz[i].TmR + j, &dhr, &dsr, &dgr, mulH, mulS))
	    win_printf_OK("error in computing Tm");
	  if (Na[j])
	    {
	      ds2->yd[ds2->ny++] = owz[i].TmR[j];
	      ds2->xd[ds2->nx++] = owz[i].Tmx[j];
	      chi2 += (owz[i].TmR[j] - owz[i].Tmx[j])*(owz[i].TmR[j] - owz[i].Tmx[j]);
	      k++;
	    }
	  if (dsv)
	    {
	      dsv->yd[j] = owz[i].TmR[j] - owz[i].TmR[0];
	      dsv->xd[j] = owz[i].Tmx[j] - owz[i].Tmx[0];
	    }

	}
      ds = NULL;
      ds2 = NULL;
      dsv = NULL;
    }
  chi2 /= k;
  chi2 = sqrt(chi2);
  
  set_plot_title(op2, "\\stack{{Tm for oligos \\chi^2 = %g %g}"		
		 "{\\pt6 Initiation \\Delta H %g \\Delta S %g mulH = %g mulS = %g}"
		"{\\pt6 A or T penality \\Delta H %g \\Delta S %g}"
		"{\\pt6 AT penality \\Delta H %g \\D\elta S %g}}"
		 ,chi2,chi2 - chi2_1,DHR2[INGC],DSR2[INGC],mulH,mulS,DHR2[INAT],DSR2[INAT],DHR2[ATPEN],DSR2[ATPEN]);
  return refresh_plot(pr, pr->n_op-1);
}


int do_compute_Tm_DNA_oligo_R2(void)
{
  register int i;
  static float conc = 2, Na = 119;
  static char oligo[256] = {0};
  float Tmr, dgr, dsr, dhr;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  if (oligo[0] == 0)
    {
      sprintf(oligo,"TACTAACATTAACTA");
    }

  i = win_scanf("Define your Oligo 5'xxxx3'%s\nand its concentration %12f in \\mu M\n"
		"Give [Na^+] %8f in mM\n",oligo,&conc,&Na);
  if (i == WIN_CANCEL) return D_O_K;
  init_array();
  i = compute_Tm_Ritort_2(oligo, conc, Na, &Tmr, &dhr, &dsr, &dgr, mulH, mulS);
  if (i) win_printf_OK("error in computing Tm");
  win_printf("Using Huguet & Ritort 2010 (PNAS) model modified\n"
	     "Tm of %s\nat %f \\mu M with [Na^+] %g mM\nequals %6.2f degrees\n"
	     "\\Delta G = %g (25 degrees C) %g (37 degrees)\n"
	     "\\Delta H = %g\n"
	     "\\Delta S = %g\n"
	     "mulH %g mulS %g\n"
	     ,oligo,conc,Na,Tmr,dgr, dhr-0.31*dsr,dhr,dsr,mulH, mulS);
  return D_O_K;  
}



int do_compute_Kd_DNA_oligo_Mfold_w_force_ener2(void)
{
  register int i;
  static float kd = 2, Na = 119, Mg = 10, force = 18, Tmr = 25;//pN;
  static char oligo[256] = {0};
  float  dgr, dsr, dhr;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  if (oligo[0] == 0)
    {
      sprintf(oligo,"ACAGCCAGCCGA");
      //sprintf(oligo,"TGATGATGAGTGGATTCGCGGTATTCAAATTGATGGAATTAAGCAAGCGATAGAATATTCTAAGAAAAATGGAATTACTACCTGGATTC");
    }

  i = win_scanf("Define your Oligo 5'xxxx3'%s\nand the temperature %12f in degree C\n"
		"Give [Na^+] %8f in mM\nGive [Mg^{2+}] %8f in mM\nForce %8f\n",oligo,&Tmr,&Na,&Mg,&force);
  if (i == WIN_CANCEL) return D_O_K;
  init_array();
  i = compute_Kd_from_Mfold_with_F_ener2(oligo, &kd, Na, Mg, force, Tmr, &dhr, &dsr, &dgr);
  if (i) win_printf_OK("error in computing Tm");
  win_printf("Using Mfold 3.1 model\n"
	     "Kd of %s\nat %g degress is %f \\mu M with [Na^+] %g mM, [Mg^{2+}] %g mM\nfor F = %6.2f pN\n"
	     "\\Delta G = %g (%g degrees C) %g (37 degrees)\n"
	     "\\Delta H = %g\n"
	     "\\Delta S = %g\n"
	     "mulH %g mulS %g\n"
	     ,oligo,Tmr,kd,Na,Mg,force,dgr, Tmr,dhr-0.31*dsr,dhr,dsr,mulH, mulS);
  return D_O_K;  
}

int do_compute_Kd_DNA_oligo_Mfold_w_force(void)
{
  register int i;
  static float kd = 2, Na = 119, Mg = 10, force = 18, Tmr = 25;//pN;
  static char oligo[256] = {0};
  float  dgr, dsr, dhr;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  if (oligo[0] == 0)
    {
      sprintf(oligo,"ACAGCCAGCCGA");
      sprintf(oligo,"TGATGATGAGTGGATTCGCGGTATTCAAATTGATGGAATTAAGCAAGCGATAGAATATTCTAAGAAAAATGGAATTACTACCTGGATTC");
    }

  i = win_scanf("Define your Oligo 5'xxxx3'%s\nand the temperature %12f in degree C\n"
		"Give [Na^+] %8f in mM\nGive [Mg^{2+}] %8f in mM\nForce %8f\n",oligo,&Tmr,&Na,&Mg,&force);
  if (i == WIN_CANCEL) return D_O_K;
  init_array();
  i = compute_Kd_from_Mfold_with_F(oligo, &kd, Na, Mg, force, Tmr, &dhr, &dsr, &dgr);
  if (i) win_printf_OK("error in computing Tm");
  win_printf("Using Mfold 3.1 model\n"
	     "Kd of %s\nat %g degress is %f \\mu M with [Na^+] %g mM, [Mg^{2+}] %g mM\nfor F = %6.2f pN\n"
	     "\\Delta G = %g (%g degrees C) %g (37 degrees)\n"
	     "\\Delta H = %g\n"
	     "\\Delta S = %g\n"
	     "mulH %g mulS %g\n"
	     ,oligo,Tmr,kd,Na,Mg,force,dgr, Tmr,dhr-0.31*dsr,dhr,dsr,mulH, mulS);
  return D_O_K;  
}




int do_compute_Kd_DNA_oligo_R2_w_force(void)
{
  register int i;
  static float kd = 2, Na = 119, force = 18, Tmr = 25;//pN;
  static char oligo[256] = {0};
  float  dgr, dsr, dhr;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  if (oligo[0] == 0)
    {
      sprintf(oligo,"ACAGCCAGCCGA");
    }

  i = win_scanf("Define your Oligo 5'xxxx3'%s\nand the temperature %12f in degree C\n"
		"Give [Na^+] %8f in mM\nForce %8f\n",oligo,&Tmr,&Na,&force);
  if (i == WIN_CANCEL) return D_O_K;
  init_array();
  i = compute_kd_Ritort_2_w_force(oligo, &kd, Na, Tmr, &dhr, &dsr, &dgr, force);
  if (i) win_printf_OK("error in computing Tm");
  win_printf("Using Huguet & Ritort 2010 (PNAS) model modified\n"
	     "Kd of %s\nat %g degress is %f \\mu M with [Na^+] %g mM\nfor F = %6.2f pN\n"
	     "\\Delta G = %g (%g degrees C) %g (37 degrees)\n"
	     "\\Delta H = %g\n"
	     "\\Delta S = %g\n"
	     "mulH %g mulS %g\n"
	     ,oligo,Tmr,kd,Na,force,dgr, Tmr,dhr-0.31*dsr,dhr,dsr,mulH, mulS);
  return D_O_K;  
}


int do_compute_Tm_DNA_LNA_oligo(void)
{
  register int i;
  static float conc = 5, Na = 115;
  static char oligo[256];
  float Tm, dg, ds, dh;
  float Tmu, dgu, dsu, dhu;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  i = win_scanf("Define your DNA-LNA Oligo 5'xxxx3'\nUppercase for LNA, lowercase DNA%s\nand its concentration %12f in \\mu M\n"
		"Give [Na^+] %8f in mM\n",oligo,&conc,&Na);
  if (i == WIN_CANCEL) return D_O_K;
  init_array();

  i = compute_Tm_unified(oligo, conc, Na, &Tmu, &dhu, &dsu, &dgu);
  if (i) win_printf_OK("error in computing Tm");

  i = compute_Tm_unified_DNA_LNA(oligo, conc, Na, &Tm, &dh, &ds, &dg);
  if (i) win_printf_OK("error in computing Tm");


  win_printf("Tm of %s with LNA bases\nat %f \\mu M equals %6.2f degrees\n"
	     "\\Delta G = %g\n"
	     "\\Delta H = %g\n"
	     "\\Delta S = %g\n"
	     "Using SantaLucia 1998 (PNAS) unified model\n"
	     "Tm of %s with DNA only\nat %f \\mu M with [Na^+] %g mM\nequals %6.2f degrees\n"
	     "\\Delta G = %g\n"
	     "\\Delta H = %g\n"
	     "\\Delta S = %g\n"
	     ,oligo,conc,Tm,dg,dh,ds,oligo,conc,Na,Tmu,dgu,dhu,dsu);
  return D_O_K;  
}


int do_compute_Tm_DNA_oligo_with_SW_wooble(void)
{
  register int i, j;
  static float conc = 5, Na = 115;
  static char oligo[128];
  static int doplot = 0;
  float Tm, dg, dS, dh;
  float Tmm, dgm, dSm, dhm;
  int len, nw, nws;
  char toligo[128], wob[16], wobs[16], wobt[16], *basesAndWooble[128];
  pltreg *pr;
  O_p *op;
  d_s *ds = NULL;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
    return win_printf_OK("cannot find plot region");

  i = win_scanf("Define your Oligo 5'xxxx3'with S and W wooble\n%32s \n"
		"and [Oligo] %6f \\mu M [Na^+] %6f mM\n"
		"Draw plot %b",oligo,&conc,&Na,&doplot);
  if (i == WIN_CANCEL) return D_O_K;
  len = strlen(oligo);
  for (i = nw = j = 0; i < len; i++)
    {
      if ((oligo[i]&0x5F) == 'A')      basesAndWooble[i] = oligo + i;
      else if ((oligo[i]&0x5F) == 'C') basesAndWooble[i] = oligo + i;
      else if ((oligo[i]&0x5F) == 'G') basesAndWooble[i] = oligo + i;
      else if ((oligo[i]&0x5F) == 'T') basesAndWooble[i] = oligo + i;
      else if ((oligo[i]&0x5F) == 'S') 
	{
	  basesAndWooble[i] = wob + nw;
	  wobs[nw] = 1;
	  wobt[nw] = (oligo[i] == 'S') ? 1 : 0;
	  nw++;
	}
      else if ((oligo[i]&0x5F) == 'W') 
	{
	  basesAndWooble[i] = wob + nw;
	  wobs[nw] = 0;
	  wobt[nw] = (oligo[i] == 'W') ? 1 : 0;
	  nw++;
	}
      else return win_printf_OK("wrong base type %c in oligo\n%s",oligo[i],oligo);
    }

  init_array();
  for (j = 1, i = 0; i < nw; i++, j*=2);
  nws = j;
  if (doplot)
    {
      op = create_and_attach_one_plot(pr, nws, nws, 0);
      if (op == NULL)     return win_printf_OK("cannot create plot");
      ds = op->dat[0];
      op->need_to_refresh = 1; 
      set_plot_title(op, "\\stack{{Tm for %d oligos %s}",nws,oligo);
      set_dot_line(ds);
      set_ds_point_symbol(ds, "\\oc");
      set_ds_source(ds,"Tm for %d oligos %s",nws, oligo);
      set_plot_x_title(op, "index");
      set_plot_y_title(op, "Tm");
    }

  for (i = 0, Tmm = dgm = dSm = dhm = 0; i < nws; i++)
    {
      for (j = 0; j < nw; j++)
	{
	  if (i&(0x01<<j))
	    {
	      if (wobt[j] == 1)
		{
		  wob[j] = (wobs[j] == 1) ? 'G': 'T'; 
		}
	      else
		{
		  wob[j] = (wobs[j] == 1) ? 'g': 't'; 
		}
	    }
	  else
	    {
	      if (wobt[j] == 1)
		{
		  wob[j] = (wobs[j] == 1) ? 'C': 'A'; 
		}
	      else
		{
		  wob[j] = (wobs[j] == 1) ? 'c': 'a'; 
		}
	    }
	}
      for (j = 0; j < len; j++)
	toligo[j] = *basesAndWooble[j];
      toligo[len] = 0;
      //      if (compute_Tm(toligo, conc, &Tm, &dh, &dS, &dg))
      if (compute_Tm_unified_DNA_LNA(toligo, conc, Na, &Tm, &dh, &dS, &dg))
	win_printf_OK("error in computing Tm");
      if (ds != NULL)
	{
	  ds->xd[i] = i;
	  ds->yd[i] = Tm;
	}
      Tmm += Tm;
      dgm += dg;
      dSm += dS;
      dhm += dh;
    }
  Tmm /= nws;
  dgm /= nws;
  dSm /= nws;
  dhm /= nws;
  win_printf("Tm of %s\nat %f \\mu M equals %6.2f degrees\n"
	     "\\Delta G = %g\n"
	     "\\Delta H = %g\n"
	     "\\Delta S = %g\n"
	     "%d configurations"
	     ,oligo,conc,Tmm,dgm,dhm,dSm,nws);
  return refresh_plot(pr, pr->n_op-1);
} 
int do_draw_double_oligo_concentration(void)
{
  register int i;
  static float maxconc = 5, Kd = 10;
  static int Nb = 1024;
  float tmp;
  pltreg *pr;
  O_p *op;
  d_s *ds = NULL;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
    return win_printf_OK("cannot find plot region");

  i = win_scanf("Define your Oligo affinity constant %10f nM\n"
		"Oligo concentration max %10f nM\n"
		"Nb of points %8d\n",&Kd,&maxconc,&Nb);
  if (i == WIN_CANCEL) return D_O_K;

  op = create_and_attach_one_plot(pr, Nb, Nb, 0);
  if (op == NULL)     return win_printf_OK("cannot create plot");
  ds = op->dat[0];
  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{Free [oligo] vs added [oligo] (Kd = %g nM) }}",Kd);
  set_ds_source(ds,"Free [oligo] vs added [oligo] (Kd = %g nM)",Kd);
  set_plot_x_title(op, "[Oligo] (nM)");
  set_plot_y_title(op, "Free [Oligo] [nM)");
  for (i = 0, tmp = (float)1/Kd; i < Nb; i++)
    {
      ds->xd[i] = (maxconc * i)/Nb;
      ds->yd[i] =  tmp*(sqrt(1+((4*ds->xd[i])/tmp)) - 1)/2; 
    }
  return refresh_plot(pr, pr->n_op-1);
}  
# ifdef ENCOURS

int do_compute_Tm_DNA_oligo_with_LNA_substitution(void)
{
  register int i, j;
  static float conc = 5, Na = 115;
  static char oligo[128];
  static int doplot = 0;
  float Tm, dg, dS, dh;
  float Tmm, dgm, dSm, dhm;
  int len, nw, nws;
  char toligo[128], wob[16], wobs[16], wobt[16], *basesAndWooble[128];
  pltreg *pr;
  O_p *op;
  d_s *ds = NULL;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
    return win_printf_OK("cannot find plot region");

  i = win_scanf("Define your Oligo 5'xxxx3' with A,C,G,T only\n%32s \n"
		"and [Oligo] %6f \\mu M [Na^+] %6f mM\n"
		"Draw plot %b",oligo,&conc,&Na,&doplot);
  if (i == WIN_CANCEL) return D_O_K;
  len = strlen(oligo);
  for (i = nw = j = 0; i < len; i++)
    {
      if ((oligo[i]&0x5F) == 'A')      basesAndWooble[i] = oligo + i;
      else if ((oligo[i]&0x5F) == 'C') basesAndWooble[i] = oligo + i;
      else if ((oligo[i]&0x5F) == 'G') basesAndWooble[i] = oligo + i;
      else if ((oligo[i]&0x5F) == 'T') basesAndWooble[i] = oligo + i;
      else if ((oligo[i]&0x5F) == 'S') 
	{
	  basesAndWooble[i] = wob + nw;
	  wobs[nw] = 1;
	  wobt[nw] = (oligo[i] == 'S') ? 1 : 0;
	  nw++;
	}
      else if ((oligo[i]&0x5F) == 'W') 
	{
	  basesAndWooble[i] = wob + nw;
	  wobs[nw] = 0;
	  wobt[nw] = (oligo[i] == 'W') ? 1 : 0;
	  nw++;
	}
      else return win_printf_OK("wrong base type %c in oligo\n%s",oligo[i],oligo);
    }

  init_array();
  for (j = 1, i = 0; i < nw; i++, j*=2);
  nws = j;
  if (doplot)
    {
      op = create_and_attach_one_plot(pr, nws, nws, 0);
      if (op == NULL)     return win_printf_OK("cannot create plot");
      ds = op->dat[0];
      op->need_to_refresh = 1; 
      set_plot_title(op, "\\stack{{Tm for %d oligos %s}",nws,oligo);
      set_dot_line(ds);
      set_ds_point_symbol(ds, "\\oc");
      set_ds_source(ds,"Tm for %d oligos %s",nws, oligo);
      set_plot_x_title(op, "index");
      set_plot_y_title(op, "Tm");
    }

  for (i = 0, Tmm = dgm = dSm = dhm = 0; i < nws; i++)
    {
      for (j = 0; j < nw; j++)
	{
	  if (i&(0x01<<j))
	    {
	      if (wobt[j] == 1)
		{
		  wob[j] = (wobs[j] == 1) ? 'G': 'T'; 
		}
	      else
		{
		  wob[j] = (wobs[j] == 1) ? 'g': 't'; 
		}
	    }
	  else
	    {
	      if (wobt[j] == 1)
		{
		  wob[j] = (wobs[j] == 1) ? 'C': 'A'; 
		}
	      else
		{
		  wob[j] = (wobs[j] == 1) ? 'c': 'a'; 
		}
	    }
	}
      for (j = 0; j < len; j++)
	toligo[j] = *basesAndWooble[j];
      toligo[len] = 0;
      //      if (compute_Tm(toligo, conc, &Tm, &dh, &dS, &dg))
      if (compute_Tm_unified_DNA_LNA(toligo, conc, Na, &Tm, &dh, &dS, &dg))
	win_printf_OK("error in computing Tm");
      if (ds != NULL)
	{
	  ds->xd[i] = i;
	  ds->yd[i] = Tm;
	}
      Tmm += Tm;
      dgm += dg;
      dSm += dS;
      dhm += dh;
    }
  Tmm /= nws;
  dgm /= nws;
  dSm /= nws;
  dhm /= nws;
  win_printf("Tm of %s\nat %f \\mu M equals %6.2f degrees\n"
	     "\\Delta G = %g\n"
	     "\\Delta H = %g\n"
	     "\\Delta S = %g\n"
	     "%d configurations"
	     ,oligo,conc,Tmm,dgm,dhm,dSm,nws);
  return refresh_plot(pr, pr->n_op-1);
}
  
# endif

int do_compute_Tm_DNA_oligo_with_SW_wooble_from_file(void)
{
  register int i, j;
  static float conc = 5, Na = 115;
  static int doplot = 0, ritort = 0;
  float Tm, dg, dS, dh;
  float Tmm, dgm, dSm, dhm;
  int len, nw, nws;
  char toligo[128], wob[16], wobs[16], *basesAndWooble[128];
  pltreg *pr;
  O_p *op;
  d_s *ds = NULL;
  char **loligo = NULL;
  char seqfilename[2048];
  FILE *fp;
  int n_oligo = 0, ioli;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
    return win_printf_OK("cannot find plot region");

  i = win_scanf("Compute Tm of Oligo 5'xxxx3'with S and W wooble\ngiven in a file \n"
		"and [Oligo] %6f \\mu M [Na^+] %6f mM\n"
		"Draw plot %b\n"
		"SantaLucia Unified %R or Ritort %r"
		"You will next specify first the input file\n"
		"and then the output file\n"
		,&conc,&Na,&doplot,&ritort);
  if (i == WIN_CANCEL) return D_O_K;


  n_oligo = grab_ologos_from_file(&loligo);
  if (n_oligo < 0)  win_printf_OK("could not load oligo list!");
  if (loligo == NULL) return -1;

  seqfilename[0] = 0;

  if (do_select_file(seqfilename, sizeof(seqfilename), "TXT-FILE", "*.txt", "File to save oligo set with Tm", 0))
    return win_printf_OK("Cannot select input file");
  fp = fopen (seqfilename,"w");
  if (fp == NULL)  return win_printf_OK("Cannot open file:\n%s",backslash_to_slash(seqfilename));
  fprintf(fp,"[Oligo]=%gmicroM([Na+]=%gmM\tTm(degrees)\tDeltaG\tDeltaH\tDeltaS\t#configurations\n",conc,Na);

  for (ioli = 0; ioli < n_oligo; ioli++)
    {
      len = strlen(loligo[ioli]);
      for (i = nw = j = 0; i < len; i++)
	{
	  if ((loligo[ioli][i]&0x5F) == 'A')      basesAndWooble[i] = loligo[ioli] + i;
	  else if ((loligo[ioli][i]&0x5F) == 'C') basesAndWooble[i] = loligo[ioli] + i;
	  else if ((loligo[ioli][i]&0x5F) == 'G') basesAndWooble[i] = loligo[ioli] + i;
	  else if ((loligo[ioli][i]&0x5F) == 'T') basesAndWooble[i] = loligo[ioli] + i;
	  else if ((loligo[ioli][i]&0x5F) == 'S') 
	    {
	      basesAndWooble[i] = wob + nw;
	      wobs[nw] = 1;
	      nw++;
	    }
	  else if ((loligo[ioli][i]&0x5F) == 'W') 
	    {
	      basesAndWooble[i] = wob + nw;
	      wobs[nw] = 0;
	      nw++;
	    }
	  else return win_printf_OK("wrong base type %c in loligo[ioli]\n%s",loligo[ioli][i],loligo[ioli]);
	}

      
      init_array();
      for (j = 1, i = 0; i < nw; i++, j*=2);
      nws = j;
      if (doplot)
	{
	  if (ioli == 0)
	    {
	      op = create_and_attach_one_plot(pr, nws, nws, 0);
	      if (op == NULL)     return win_printf_OK("cannot create plot");
	      ds = op->dat[0];
	      op->need_to_refresh = 1; 
	      set_plot_title(op, "\\stack{{Tm for %d loligo[ioli]s %s}",nws,loligo[ioli]);
	      set_plot_x_title(op, "index");
	      set_plot_y_title(op, "Tm");
	      set_ds_source(ds,"Tm for %d loligo[ioli]s %s",nws, loligo[ioli]);
	      set_dot_line(ds);
	      set_ds_point_symbol(ds, "\\oc");
	    }
	  else if (op != NULL)
	    {
	      ds = create_and_attach_one_ds(op, nws, nws, 0);
	      if (ds == NULL)     return win_printf_OK("cannot create plot");
	      set_dot_line(ds);
	      set_ds_point_symbol(ds, "\\oc");
	      set_ds_source(ds,"Tm for %d loligo[ioli]s %s",nws, loligo[ioli]);
	    }
	}
      
      for (i = 0, Tmm = dgm = dSm = dhm = 0; i < nws; i++)
	{
	  for (j = 0; j < nw; j++)
	    {
	      if (i&(0x01<<j))
		{
		  wob[j] = (wobs[j] == 1) ? 'G': 'T'; 
		}
	      else
		{
		  wob[j] = (wobs[j] == 1) ? 'C': 'A'; 
		}
	    }
	  for (j = 0; j < len; j++)
	    toligo[j] = *basesAndWooble[j];
	  toligo[len] = 0;
	  //      if (compute_Tm(tloligo[ioli], conc, &Tm, &dh, &dS, &dg))
	  if (ritort == 0)
	    {
	      if (compute_Tm_unified(toligo, conc, Na, &Tm, &dh, &dS, &dg))
		win_printf_OK("error in computing Tm");
	    }
	  else
	    {
	      if (compute_Tm_Ritort(toligo, conc, Na, &Tm, &dh, &dS, &dg))
		win_printf_OK("error in computing Tm");
	    }
	  if (ds != NULL)
	    {
	      ds->xd[i] = i;
	      ds->yd[i] = Tm;
	    }
	  Tmm += Tm;
	  dgm += dg;
	  dSm += dS;
	  dhm += dh;
	}
      Tmm /= nws;
      dgm /= nws;
      dSm /= nws;
      dhm /= nws;

      if (ds != NULL)
	set_ds_source(ds,"[%s]=%gmicroM([Na+]=%gmM, Tm = %6.2f(degrees) \\Delta G = %g \\Delta H = %g "
		      "\\Delta S = %g %d configurations",loligo[ioli],conc,Na,Tmm,dgm,dhm,dSm,nws);
      fprintf(fp,"%s\t%6.2f\t%g\t%g\t%g\t%d\n",loligo[ioli],Tmm,dgm,dhm,dSm,nws);
    }
  fclose(fp);
  return refresh_plot(pr, pr->n_op-1);
}
  



int do_compute_Cc_DNA_oligo(void)
{
  register int i;
  static float Tm = 30;
  static char oligo[256];
  float dg, ds, dh, conc;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  i = win_scanf("Define your Oligo 5'xxxx3'%s\nand melting temp. %12f degrees\n",oligo,&Tm);
  if (i == WIN_CANCEL) return D_O_K;
  init_array();

  i = compute_Cc_from_Tm(oligo, &conc, Tm, &dh, &ds, &dg);
  if (i) win_printf_OK("error in computing Cc");
  win_printf("Cc of %s\nat %f degrees equals %8.6f \\mu M\n",oligo,Tm,conc);
  return D_O_K;  
}



int do_compute_Kd_DNA_oligo_unified(void)
{
  register int i;
  static float Tm = 30, Na = 150, force = 20;
  static char oligo[256];
  float dg, ds, dh, conc;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  i = win_scanf("Define your Oligo 5'xxxx3'%s\nand melting temp. %12f degrees\n"
		"[Na^+](mM) %10f Force %10f (pN)\n",oligo,&Tm,&Na,&force);
  if (i == WIN_CANCEL) return D_O_K;
  init_array();

  i = compute_Kd_from_Tm_unified_with_F(oligo, &conc, Na, force, Tm, &dh, &ds, &dg);

  if (i) win_printf_OK("error in computing Cc");
  win_printf("Kd of %s\nin [Na^+] = %g mM at F = %g pN and at %g degrees\nequals %g nM\n",oligo,Na,force,Tm,1000*conc);
  return D_O_K;  
}


int do_compute_Tm_DNA_oligo_unified_with_F(void)
{
  register int i;
  static float Tm = 30, Na = 150, force = 20, conc = 1;
  static char oligo[256];
  float dg, ds, dh;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  i = win_scanf("Define your Oligo 5'xxxx3'%s\nand its concentration. %12f \\mu M\n"
		"[Na^+](mM) %10f Force %10f (pN)\n",oligo,&conc,&Na,&force);
  if (i == WIN_CANCEL) return D_O_K;
  init_array();

  i = compute_Tm_unified_with_F(oligo, conc, Na, force, &Tm, &dh, &ds, &dg);

  if (i) win_printf_OK("error in computing Cc");
  win_printf("Tm of %s\nin [Na^+] = %g mM at F = %g pN and at %g \\mu M\nequals %g degrees \n",oligo,Na,force,conc, Tm);
  return D_O_K;  
}





int do_draw_Tm_DNA_oligo_vs_GC(void)
{
  register int i, j, k;
  static int len = 7, nc, gc;
  static float conc = 5, ca = 0.25, cc = 0.25, cg = 0.25, ct = 0.25;
  static char oligo[256];
  float Tm, dg, ds, dh, cra, crc, crg, crt, c;
  pltreg *pr;
  O_p *op;
  d_s *dso;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
    return win_printf_OK("cannot find plot region");


  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number");
    }

  i = win_scanf("Define your Oligo length %8d \n"
		"The mean concentration of A %12f \\mu M\n"
		"and relative concentration of A %12f \n"
		"and relative concentration of C %12f \n"
		"and relative concentration of G %12f \n"
		"and relative concentration of T %12f \n"
		,&len,&conc,&ca,&cc,&cg,&ct);
  if (i == WIN_CANCEL) return D_O_K;
  init_array();


  op = create_and_attach_one_plot(pr, 16, 16, 0);
  if (op == NULL)     return win_printf_OK("cannot create plot");
  dso = op->dat[0];
  dso->nx = dso->ny = 0;
  for (i = 0, nc = 1; i < len; i++, nc *= 4);

  //win_printf("nc = %d",nc);
  cra = ca/0.25;
  crc = cc/0.25;
  crg = cg/0.25;
  crt = ct/0.25;

  for (i = 0; i < nc; i++)
    {
      gc = 0;
      c = conc; 
      for (j = 0; j < len; j++)
	{
	  k = i >> (2*j);
	  k &= 0x03;
	  if (k == 0) {oligo[j] = 'A';  c *= cra;} 
	  else if (k == 1) {oligo[j] = 'C'; gc++; c *= crc;} 
	  else if (k == 2) {oligo[j] = 'G'; gc++; c *= crg;}
	  else if (k == 3) {oligo[j] = 'T';  c *= crt;} 
	  else win_printf("pb base");
	} 
      oligo[j] = 0;
      //win_printf("testing %s, i = %d ",oligo,i);
      if (compute_Tm(oligo, c, &Tm, &dh, &ds, &dg))
	  win_printf_OK("error in computing Tm");
      add_new_point_to_ds(dso, gc, Tm);
    }

  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{Tm for %d oligos versus GC}"
		 "{\\pt7 Conc = %g \\mu M, ca = %g cc = %g cg = %g ct = %g}}"
		 ,len, conc,ca,cc,cg,ct);
  set_dot_line(dso);
  set_ds_point_symbol(dso, "\\oc");
  set_ds_source(dso,"Tm for %d oligos versus GC Conc = %g \\mu M, ca = %g cc = %g cg = %g ct = %g"
		 ,len, conc,ca,cc,cg,ct);
  set_plot_x_title(op, "GC");
  set_plot_y_title(op, "Tm");
  return refresh_plot(pr, pr->n_op-1);
}

int do_draw_ss_minus_ds(void)
{
  int i;
  pltreg *pr;
  O_p *op;
  d_s *ds;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	
    return win_printf_OK("cannot find plot region");


  op = create_and_attach_one_plot(pr, 256, 256, 0);
  if (op == NULL)     return win_printf_OK("cannot create plot");
  ds = op->dat[0];
  for (i = 0; i < 256; i++)
    {
      ds->yd[i] = (float)i/10;
      ds->xd[i] = ss_minus_ds_above_5pN(ds->yd[i]);
    }
  set_plot_x_title(op, "ss - ds (nm)");
  set_plot_y_title(op, "F (pN)");
  return refresh_plot(pr, pr->n_op-1);
} 
 

MENU *Tm_DNA_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Tm of oligo", do_compute_Tm_DNA_oligo,NULL,0,NULL);
	add_item_to_menu(mn,"Tm of oligo with LNA", do_compute_Tm_DNA_LNA_oligo,NULL,0,NULL);

	add_item_to_menu(mn,"Tm of oligo with SW",do_compute_Tm_DNA_oligo_with_SW_wooble ,NULL,0,NULL);

	add_item_to_menu(mn,"Tm of oligo with SW in file",do_compute_Tm_DNA_oligo_with_SW_wooble_from_file ,NULL,0,NULL);

	add_item_to_menu(mn,"Cc of oligo", do_compute_Cc_DNA_oligo,NULL,0,NULL);
	add_item_to_menu(mn,"Kd of oligo unified", do_compute_Kd_DNA_oligo_unified,NULL,0,NULL);
	add_item_to_menu(mn,"Draw Tm vs GC", do_draw_Tm_DNA_oligo_vs_GC,NULL,0,NULL);
	add_item_to_menu(mn,"Draw double [Oligo]", do_draw_double_oligo_concentration,NULL,0,NULL);
	add_item_to_menu(mn,"Tm of oligo unified with F", do_compute_Tm_DNA_oligo_unified_with_F,NULL,0,NULL);
	add_item_to_menu(mn,"Tm of 92 oligos",do_compute_Tm_DNA_92_oligos,NULL,0,NULL);
	add_item_to_menu(mn,"Tm of 92 oligos R2",do_compute_Tm_DNA_92_oligos_2,NULL,0,NULL);
	add_item_to_menu(mn,"Tm oligo R2",do_compute_Tm_DNA_oligo_R2,NULL,0,NULL);
	add_item_to_menu(mn,"Kd oligo R2 w force",do_compute_Kd_DNA_oligo_R2_w_force,NULL,0,NULL);
	add_item_to_menu(mn,"Mfold 3.1 \\Delta G",display_Mfold3_1_DeltaG,NULL,0,NULL);
	add_item_to_menu(mn,"Kd oligo Mfold w F",do_compute_Kd_DNA_oligo_Mfold_w_force,NULL,0,NULL);
	add_item_to_menu(mn,"Kd oligo Mfold w F E2",do_compute_Kd_DNA_oligo_Mfold_w_force_ener2,NULL,0,NULL);
	add_item_to_menu(mn,"Draw ss - ds",do_draw_ss_minus_ds,NULL,0,NULL);
	return mn;
}

int	Tm_DNA_main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  add_plot_treat_menu_item ( "Tm_DNA", NULL, Tm_DNA_plot_menu(), 0, NULL);
  return D_O_K;
}

int	Tm_DNA_unload(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  remove_item_to_menu(plot_treat_menu, "Tm_DNA", NULL, NULL);
  return D_O_K;
}
#endif

