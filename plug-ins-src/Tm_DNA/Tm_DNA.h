#ifndef _TM_DNA_H_
#define _TM_DNA_H_

PXV_FUNC(int, compute_Tm, (char *oligo, float conc, float *Tm, float *deltaH, float *deltaS, float *deltaG));
PXV_FUNC(int, compute_Tm_unified, (char *oligo, float conc, float Na, float *Tm, float *deltaH, float *deltaS, float *deltaG));
PXV_FUNC(int, do_Tm_DNA_rescale_plot, (void));
PXV_FUNC(MENU*, Tm_DNA_plot_menu, (void));
PXV_FUNC(int, do_Tm_DNA_rescale_data_set, (void));
PXV_FUNC(int, Tm_DNA_main, (int argc, char **argv));
#endif

