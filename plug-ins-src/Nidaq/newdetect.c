int new_detect_board(void)
{   
# ifdef PB
    char *modName=NULL;
	char configFile[80];
	pCITIMods itimod,itimod1;
	ITI_PARENT_MOD mod,mod1;
	pCICamera cam1;
	
	
	itimod=IFC_IfxCreateITIMods();
	CITIMods_GetFirst(itimod,&mod);
	CITIMods_GetNext(itimod1,&mod1);
	win_printf("Board detected \n %s \n%s",mod.name,mod1.name);



	void CICapMod::ProcessCameraFilesInDir(const pSTRING szPath, BOOL bRecurse = FALSE);
	void CICapMod::ProcessCameraFileList(char *szListFileName);

	if ((&mod.name = "P2V"))
	{
	if (!(capmod=IFC_IfxCreateCaptureModule(mod.name,0,configFile))) {
		if (!(capmod=IFC_IfxCreateCaptureModule(mod.name,0,NULL))) {
			win_printf("No Image Capture Module detected");
			exit(0);
		}
		
	cam = CICapMod_GetCam(capmod,0);
	cam1 = CICapMod_GetCam(capmod,1);
	if (cam1 = NULL) win_printf("Only one camera detected");
 
 	}
	
	modName = "P2V";
	if ( modName!=NULL ) {
		
		sprintf(configFile,"%stest.txt",modName);
	}
	else {
		strcpy(configFile,"ifctest.txt");
	}
	 win_printf("No Image %s",configFile);
	 
	if (!(capmod=IFC_IfxCreateCaptureModule(modName,0,configFile))) {
		if (!(capmod=IFC_IfxCreateCaptureModule(modName,0,NULL))) {
			win_printf("No Image Capture Module detected");
			exit(0);
		}
		// If user config file not found, add all camera definitions in camera database directory
	CICapMod_ProcessCameraFilesInDir(capmod,"camdb",TRUE);
	}
	cam = CICapMod_GetCam(capmod,0);
	// Get the camera's basic attributes
	itimod=IFC_IfxCreateITIMods();
    CITIMods_GetFirst(itimod,&mod);
    CICamera_GetAttr(cam,&attr,TRUE);
    win_printf("Board name= %s\n Revision %d\n Camera name %s\n",mod.name ,CModule_Revision(capmod),attr.camName);
# endif    
    return 0;

}

