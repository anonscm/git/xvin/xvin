#define homeDAC_C
#ifndef DAQ_6503_H
#include "daq6503.h"
#endif

#ifndef homeDAC_H
#include "homeDAC.H"
#endif

#ifndef homeADC_H
#include "homeADC.h"
#endif



int init_CNA(void)
{
    DIG_Prt_Config (PCI6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
    DIG_Prt_Config (PCI6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
    DIG_Prt_Config (PCI6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
    DIG_Out_Prt (PCI6503,PORT_C_AD,0xFF);
    IO_write(CNA_1,0);
    IO_write(CNA_2,0);
    IO_write(CNA_3,0);
    IO_read(CAN_1);
    return 0;
}

int IO_read(unsigned char canal)
{
    char voie;
    int mesure=0;
    char read_1=0;
    char read_2=0;
    unsigned char port_C_value=0;
    
    
    voie = (canal-1) * 2;
    DIG_Prt_Config (PCI6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
    DIG_Prt_Config (PCI6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
    DIG_Prt_Config (PCI6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
    {DIG_Out_Prt (PCI6503,PORT_C_AD,0xCF);}
    
    DIG_Out_Prt (PCI6503,PORT_B_AD,voie+57);
    DIG_Out_Line (PCI6503,PORT_C_AD,6,1);
    DIG_Out_Prt (PCI6503,PORT_B_AD,voie+0);
    DIG_Out_Prt (PCI6503,PORT_B_AD,voie+8);
    DIG_In_Prt (PCI6503,PORT_A_AD,&read_1);
    
    DIG_Out_Prt (PCI6503,1,voie+9);
    DIG_In_Prt (PCI6503,PORT_A_AD,&read_2);
    
    DIG_Out_Prt (PCI6503,PORT_B_AD,voie+57);
    DIG_Out_Line (PCI6503,PORT_C_AD,6,0);
    DIG_Prt_Config (PCI6503,PORT_B_AD,1,1);
    
    mesure=(read_1<<8)+read_2;
    DIG_Prt_Config (PCI6503,PORT_A_AD,NO_HANDSHAKING,OUT_SIGNAL);
    DIG_Prt_Config (PCI6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
    DIG_Prt_Config (PCI6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
    
    return mesure;
}
int Read_ADC_value(void)
{   static char canal;
    
    
    win_scanf("Channel to read %d",&canal);
    win_printf("ADC value %d",IO_read(canal));
    return 0;

}


