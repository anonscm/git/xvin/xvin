#ifndef DAQ6602_C
#define DAQ6602_C

# include "allegro.h"
# include "winalleg.h"

# include "xvin.h"
#include "nidaqcns.h"
#include "nidaq.h"
#include "daq6602.h"







/*
typedef struct counter_parameters
{	
	int counter_timebase;
	int counter_gate;
	int counter_source;
	int counter_application;
	int edgeMode;
 	int gateMode;
  	int outType;
   	int outPolarity;
	
} counter_param;

counter_param counters[8];

int set_defautls_param(void)
{
	for (i=0;i<8;i++)
	{
		counters[i].timebase = 4;
		counter[i].gate = 4;
		counter[i].source = 11;
*/
int detect_NI_boards(void)
{	
	short int infovalue;
	int i;
	
	for (i=1;i<3;i++)
	{
		status = Init_DA_Brds (i,&infovalue);
		if (infovalue == PCI_6503)
  		{
    	 	NI_6503=i;
    	 	win_printf(" Board %d is DAQ6503",i);
 	 	}
 	 	else if (infovalue == PCI_6602)
  		{
    	 	NI_6602=i;
    	 	win_printf(" Board %d is Counter 6602",i);
 	 	}
 	 	else win_printf("No board detected");
 
 	}
 /*	for (i=0;i<8;i++)
 	{
 	      status = GPCTR_Control(NI_6602, i, ND_RESET);

          if (status ==0 ) win_printf( "GPCTR_Control/RESET of counter %d",i);
          else win_printf("Failed in %d",i);
    }
*/
 	return 0;

}




int init_boards_6602(void)
{	
	short int board;
 	
 	status = Init_DA_Brds (NI_6602,&board);
 	if (board == PCI_6602)
	win_printf("Initialised Board 6602");
	
	/*if (board == PCI_6503)
	win_printf("Initialised Board 6503");*/

	return 0;	
}
/*
int configure_buffer_for_continuous(void)
{   static long int numPoints = 16384;
    int counter_number=0;
    
    win_scanf("HOW MANY POINTS DO YOU WANT? \n %d",&numPoints);
    config_buff(counter_number,numPoints);
    return 0;
}
*/
int config_buff(int counter_number, int numPoints)
{
        if (buffer ==NULL) buffer=(long int *) calloc(numPoints,sizeof(long int));
        
        buffer=( long int *)realloc(buffer,numPoints*sizeof(long int));
        status = GPCTR_Config_Buffer (NI_6602, counter_number, 0 , numPoints, buffer);
        if (status !=0)
        {    
                win_printf("Error in the buffer making!");
                return 0;
        }
        return 0;
}

/*


int start_buffered_count(void)
{       
    long int entityValue;
    long int numPts=1024;
    long int data[1024];
    long int read_points=0;

    status = GPCTR_Watch (NI_6602, COUNTER_0, ND_BUFFER_SIZE, &entityValue);
    if (status !=0)
        {    
                win_printf("Error in asking the buffer size!");
                return 0;
        }
    else if (entityValue ==0) 
        {
                win_printf("It might no be a buffered application! Buffe size is 0!");
                return 0;
        }
        
    configure_buffer_for_continuous();
    status = GPCTR_Control (NI_6602, COUNTER_0, ND_ARM	) ;
    
    
    for (read_points=0,i=0;read_points<131072;i++)
    {
    status = GPCTR_Read_Buffer (NI_6602, COUNTER_0, ND_READ_MARK, numPts, 0, 0, data);
    read_points+=numPts;
    
    }
    return 0;
}*/


int generate_square_wave(void)
{
    /*
     * Local Variable Declarations: 
     */

    
    
    
    long int counter_number = ND_COUNTER_1;
    long int ulGpctrOutput = ND_GPCTR1_OUTPUT;
    long int ulLOWcount = 50;
    long int ulHIGHcount = 50;
    

    status = GPCTR_Control(NI_6602, counter_number, ND_RESET);
    
    status = GPCTR_Set_Application(NI_6602, counter_number,
     ND_PULSE_TRAIN_GNR);
     
    /* If you want to use a different GPCTR source, change the last
     parameter in the following function. */

    status = GPCTR_Change_Parameter(NI_6602, counter_number, ND_SOURCE,
     ND_INTERNAL_100_KHZ);
    
    /* Load the low count for the square wave. */

    status = GPCTR_Change_Parameter(NI_6602, counter_number, ND_COUNT_1,
     ulLOWcount);
     
    /* Load the high count for the square wave. */

    status = GPCTR_Change_Parameter(NI_6602, counter_number, ND_COUNT_2,
     ulHIGHcount);

    /* Enable GPCTR output. */

    status = Select_Signal(NI_6602, ulGpctrOutput, ulGpctrOutput, ND_LOW_TO_HIGH);

    status = GPCTR_Control(NI_6602, counter_number, ND_PROGRAM);

    win_printf(" Variable frequency squarewave generation started...\n");

  /*  while (keypressed()!= TRUE) {;}*/



    return 0;
}    

int stop_square_wave(void)
{   
    long int counter_number = ND_COUNTER_1;
    long int ulGpctrOutput = ND_GPCTR1_OUTPUT;
    
    /* CLEANUP - Don't check for errors on purpose. */

    /* Disconnect the GPCTR output from the IO Connector. */
    status = Select_Signal(NI_6602, ulGpctrOutput, ND_NONE, ND_DONT_CARE);

    /* Reset GPCTR. */

    status = GPCTR_Control(NI_6602, counter_number, ND_RESET);

    win_printf(" Variable frequency squarewave generation done!\n");

    return 0;
}     
 
int test_program(void)
{		
		int base_time = 1;
        int i,j;
		int numPoints = 65536;
		int numPts_read = 0;
		int numPts_to_read = 1024;
		clock_t counting_time = 5;
		clock_t start=0;
		int data_counted=0;
		int data_to_count = 131072;
		long unsigned int read_buffer[1024];
		long int param;
		O_p *op=NULL;
		d_s *ds_counts=NULL;
    	pltreg *pr = NULL;
		
		
  		win_scanf("How long do you want to count (in ms)%d",&counting_time);
  		
  		
  		
  		/*read_buffer = ( long int *) calloc(numPts_to_read+1,sizeof(long int));*/
  		
  		
  		pr = find_pr_in_current_dialog(NULL);
        if (pr == NULL)	return 1;
	    op = create_one_plot(data_to_count, data_to_count, 0);
	    ds_counts = get_op_cur_ds(op);
	    add_data( pr, IS_ONE_PLOT, (void*)op);
  	    
		/* Reset the counter*/
		status = GPCTR_Control (NI_6602, ND_COUNTER_0, ND_RESET);
		
		/* Set application->period measurement*/
		status = GPCTR_Set_Application (NI_6602, ND_COUNTER_0, ND_BUFFERED_PERIOD_MSR );
		
  		/* We take PFI line 0*/
		status = GPCTR_Change_Parameter (NI_6602, ND_COUNTER_0, ND_SOURCE, ND_DEFAULT_PFI_LINE);
		
		/* In order to gate we have to send a square wave from output counter 1*/
  		status = GPCTR_Change_Parameter (NI_6602, ND_COUNTER_0, ND_GATE, ND_OTHER_GPCTR_OUTPUT);
		
		/*We set a continuous acquisition*/
		status = GPCTR_Change_Parameter (NI_6602, ND_COUNTER_0, ND_BUFFER_MODE, ND_CONTINUOUS);
		
		/* So we need to configure the buffer*/
		
		config_buff(ND_COUNTER_0,numPoints);
  		/*status = GPCTR_Config_Buffer (NI_6602, COUNTER_0, 0, numPoints, buffer);*/
  		
  		/* We prepare the counter*/
  		
  		status = GPCTR_Control (NI_6602, ND_COUNTER_0, ND_PREPARE);
		
		
		/*status = GPCTR_Watch (NI_6602, ND_COUNTER_0, ND_BUFFER_SIZE , &param);*/
		/*win_printf("Buffer size = %d",param);*/
		/* counting starts*/
  		status = GPCTR_Control (NI_6602, ND_COUNTER_0, ND_ARM);
  		if (status == 0) win_printf("we started");
  		/*start=clock();
  		while ( (clock()-start) < 100);
    	status = GPCTR_Watch (NI_6602, ND_COUNTER_0, ND_AVAILABLE_POINTS  , &param);
		win_printf("Available points = %d",param);*/
		start=clock(); 
		while ( (clock()-start) < counting_time) /*& (keypressed != TRUE)*/
		
		{
			i = data_counted;			
			status = GPCTR_Read_Buffer (NI_6602, ND_COUNTER_0, ND_READ_MARK, 0/*offset*/,numPts_to_read,  1, &numPts_read, read_buffer);
			
   			data_counted+=numPts_read;
   			
   			if (data_to_count < (data_counted+numPts_to_read)) ds_counts = build_adjust_data_set(ds_counts, data_to_count+2*numPts_to_read, data_to_count+2*numPts_to_read);
   			for ( /*i = data_counted,*/j=0; (data_counted-i)>0;i++,j++)
   			{ 		
                  ds_counts->xd[i] = i*base_time;
                  ds_counts->yd[i] = read_buffer[j];
            }
  		}
  		
  		status = GPCTR_Control (NI_6602, ND_COUNTER_0, ND_DISARM);
  		
    	    win_printf("read %d",data_counted);
    	    /*sprintf(bin_time,"Binning time %f \\mu s",binning_time);
    	    set_plot_title(op, bin_time);*/
    	    set_plot_x_title(op, "Time");	
    	    set_plot_y_title(op, "Count ");
    	    set_ds_plain_line(ds_counts);
    	    broadcast_dialog_message(MSG_DRAW,0);
    	    

		return 0;


}  


int continuous_buff_event(void)
{
    /*
     * Local Variable Declarations: 
     */

    unsigned int i,j;
    clock_t counting_time = 500;
	clock_t start=0;
	unsigned int 	data_counted=0;
	unsigned int 	data_to_count = 131072;
	
 	long unsigned int read_buffer[1024];
    long int last_buffer_value =0;
    long int Start_Count = 0;
    long int ReadCount = 50;
    
    long int Init_Count = 0;
    int 	 numPoints = 65536;
    int      iGateFasterThanSource = 0;
    long int numPts_to_read = 50;
    long int numPts_read = 0;
    long int ReadOffset = 0;
    long int TimeOut = 0;
    O_p *op=NULL;
	d_s *ds_counts=NULL;
    pltreg *pr = NULL;
		
		
  	win_scanf("How long do you want to count (in ms)%d",&counting_time);
  		
  		
  		
  		/*read_buffer = ( long int *) calloc(numPts_to_read+1,sizeof(long int));*/
  		
  		
  	pr = find_pr_in_current_dialog(NULL);
    if (pr == NULL)	return 1;
	op = create_one_plot(data_to_count, data_to_count, 0);
	ds_counts = get_op_cur_ds(op);
	add_data( pr, IS_ONE_PLOT, (void*)op);
  	    
    

    status = GPCTR_Control(NI_6602, ND_COUNTER_2, ND_RESET);

    

    /* Set up for a buffered event counting application. */

    status = GPCTR_Set_Application(NI_6602, ND_COUNTER_2,ND_BUFFERED_EVENT_CNT);

    

    /* If you want to use a different GPCTR source, change the last
     parameter in the following function. */

    status = GPCTR_Change_Parameter(NI_6602, ND_COUNTER_2, ND_SOURCE, ND_INTERNAL_MAX_TIMEBASE);

    
    /* Each time a pulse arrives in the gate, a new value will be
     latched into the counter and sent to the data buffer. */

    status = GPCTR_Change_Parameter(NI_6602, ND_COUNTER_2, ND_GATE, ND_DEFAULT_PFI_LINE);

    
    /* Load initial count. */

    status = GPCTR_Change_Parameter(NI_6602, ND_COUNTER_2,ND_INITIAL_COUNT, Init_Count);

    
    /* Enable double-buffer mode. */

    status = GPCTR_Change_Parameter(NI_6602, ND_COUNTER_2,ND_BUFFER_MODE, ND_DOUBLE);

    
    /* Turn on synchronous counting mode if gate is faster than
     source. */

    if (iGateFasterThanSource == 1) {


        status = GPCTR_Change_Parameter(NI_6602, ND_COUNTER_2,ND_COUNTING_SYNCHRONOUS, ND_YES);

        

    }

    /* So we need to configure the buffer*/
		
    config_buff(ND_COUNTER_2,numPoints);
    /*status = GPCTR_Config_Buffer(NI_6602, ND_COUNTER_2, 0, Start_Count, Buffer);*/

    /*prepare and arm the counter*/
    status = GPCTR_Control(NI_6602, ND_COUNTER_2, ND_PROGRAM);

    start = clock();
    while ( (clock()-start) < counting_time) /*& (keypressed != TRUE)*/
		
		{    
			i = data_counted;			
			status = GPCTR_Read_Buffer(NI_6602, ND_COUNTER_2, ND_READ_MARK,ReadOffset, numPts_to_read, TimeOut, &numPts_read, read_buffer);
   			data_counted+=numPts_read;
   			
   			if (data_to_count < (data_counted+numPts_to_read)) ds_counts = build_adjust_data_set(ds_counts, data_to_count+2*numPts_to_read, data_to_count+2*numPts_to_read);
   				for ( /*i = data_counted,*/j=0; (data_counted-i)>0;i++,j++)
   			{ 		
			      ds_counts->xd[i] = (float) (read_buffer[j])/*+last_buffer_value))/80000000.80MHz clock*/;
                  ds_counts->yd[i] = (float) i;
                  if (j == (numPts_read-1)) last_buffer_value = read_buffer[j];
                  
            }
  		}
  		
  		
    	    win_printf("read %d",data_counted);
    	    /*sprintf(bin_time,"Binning time %f \\mu s",binning_time);
    	    set_plot_title(op, bin_time);*/
    	    set_plot_x_title(op, "Time");	
    	    set_plot_y_title(op, "Count ");
    	    set_ds_plain_line(ds_counts);
    	    broadcast_dialog_message(MSG_DRAW,0);


        
        

       

    
    /* CLEANUP - Don't check for errors on purpose. */

    /* Reset GPCTR. */

    status = GPCTR_Control(NI_6602, ND_COUNTER_2, ND_RESET);

    return 0;
}
/*

MENU Zero_counter_menu[32] =
{
	{ "Select Application",NULL, select_application, 0, NULL  },
	{" Parameters",NULL, select_parameters,0, NULL},
    { "Control", NULL, select_control,0, NULL},
    { "Start buffered application", start_buffered_count,NULL, 0,NULL},
    { NULL,                                  NULL,             NULL,       0, NULL  }

}

MENU select_control[32] =
{
	{ "Prepare the general-purpose counter",ND_PREPARE, NULL, 0, NULL  },
	{ "Prepares the counter to start",ND_ARM, NULL, 0, NULL  },
	{ "Restores the state of the counter",ND_DISARM, NULL, 0, NULL  },
	{ "ND_PREPARE and then ND_ARM the counter",ND_PROGRAM, NULL, 0, NULL  },
	{ "Returns the counter to the power-on state",ND_RESET, NULL, 0, NULL  },
	{ "Change the counting direction to up",ND_COUNT_UP, NULL, 0, NULL  },
	{ "Change the counting direction to down",ND_COUNT_DOWN, NULL, 0, NULL  },
	{ "Change the properties of a continuous pulse started",ND_SWITCH_CYCLE, NULL, 0, NULL  },
	{ "sets several buffer-related readable parameters",ND_SNAPSHOT, NULL, 0, NULL  },
	{ NULL,                                  NULL,             NULL,       0, NULL  }
}

MENU select_parameters[32]=
{
	
	{"ND_AUTOINCREMENT_COUNT", ND_AUTOINCREMENT_COUNT ,NULL, 0, NULL},
	{"ND_AUX_LINE", ND_AUX_LINE ,NULL, 0, NULL},	
	{"ND_AUX_LINE_POLARITY", ND_AUX_LINE_POLARITY ,NULL, 0, NULL},	
	{"ND_BUFFER_MODE ", ND_BUFFER_MODE  ,NULL, 0, NULL},
	{"ND_COUNT_1", ND_COUNT_1 , NULL,0, NULL},
	{"ND_COUNT_2", ND_COUNT_2 , NULL,0, NULL},
	{"ND_COUNT_3", ND_COUNT_3 , NULL,0, NULL},
	{"ND_COUNT_4", ND_COUNT_4  ,NULL, 0, NULL},	
  	{"ND_COUNTING_SYNCHRONOUS", ND_COUNTING_SYNCHRONOUS , NULL,0, NULL},	
   	{"ND_ENCODER_TYPE", ND_ENCODER_TYPE , NULL,0, NULL},
	{"ND_GATE ", ND_GATE  , NULL,0, NULL},
	{"ND_GATE_POLARITY", ND_GATE_POLARITY , NULL,0, NULL},
	{"ND_INITIAL_COUNT", ND_INITIAL_COUNT , NULL,0, NULL},
	{"ND_OUTPUT_MODE ", ND_OUTPUT_MODE  , NULL,0, NULL},
	{"ND_OUTPUT_POLARITY ", ND_OUTPUT_POLARITY  , NULL,0, NULL},
	{"ND_PRESCALE_VALUE ", ND_PRESCALE_VALUE  , NULL,0, NULL},
	{"ND_SOURCE", ND_SOURCE , NULL,0, NULL},
	{"ND_SOURCE_POLARITY ", ND_SOURCE_POLARITY  , NULL,0, NULL},
	{"ND_START_TRIGGER", ND_START_TRIGGER , NULL,0, NULL},
	{"ND_START_TRIGGER_POLARITY", ND_START_TRIGGER_POLARITY , NULL,0, NULL},
	{"ND_UP_DOWN", ND_UP_DOWN , NULL,0, NULL},
	{"ND_Z_INDEX_ACTIVE", ND_Z_INDEX_ACTIVE , NULL,0, NULL},
	{"ND_Z_INDEX_PHASE", ND_Z_INDEX_PHASE , NULL,0, NULL},
	{"ND_Z_INDEX_VALUE", ND_Z_INDEX_VALUE , NULL,0, NULL},
	{"ND SYNCHRONIZATION LINE", ND_SYNCHRONIZATION_LINE , NULL,0, NULL},
	{"ND SYNCHRONIZATION METHOD", ND_SYNCHRONIZATION_METHOD , NULL,0, NULL},
	{"ND TRANSFER MODE ", ND_TRANSFER_MODE  , NULL,0, NULL},
	{"ND INITIAL SECONDS", ND_INITIAL_SECONDS , NULL,0, NULL},
	{"ND PRECISION", ND_PRECISION , NULL,0, NULL},
    { NULL,          NULL,       NULL,       0, NULL  }
}
MENU select_application[32]=
{
	
	{"Buffered, asynchronous time measurement.", ND_BUFFERED_TIME_MSR , NULL,0, NULL},
	{"Buffered position tracking of quadrature encoders", ND_BUFFERED_POSITION_MSR, NULL,0, NULL},
	{"asynchronous pulse-width measurement on separate gates", ND_TWO_SIGNAL_EDGE_SEPARATION_MSR 	,NULL, 0, NULL},
	{"Buffered, asynchronous pulse-width measurement", ND_BUFFERED_PULSE_WIDTH_MSR, NULL,0, NULL},
	{"Buffered, asynchronous semi period measurement", ND_BUFFERED_SEMI_PERIOD_MSR, 0, NULL},
	{"Buffered, asynchronous period measurement", ND_BUFFERED_PERIOD_MSR 	, NULL,0, NULL},
	{"Buffered, asynchronous event counting", ND_BUFFERED_EVENT_CNT, NULL,0, NULL},
	{"Frequency Shift-Keying", ND_FSK, NULL,0, NULL},
	{"Generation of a pulse train", ND_PULSE_TRAIN_GNR, NULL,0, NULL},
	{"Generation of a retriggerable single pulse", ND_RETRIG_PULSE_GNR, NULL,0, NULL},
	{"Generation of a single triggered pulse", ND_SINGLE_TRIG_PULSE_GNR, NULL,0, NULL},
	{"Simple Pulse and Pulse-Train Generation", ND_SINGLE_PULSE_GNR, NULL,0, NULL},
	{"Single time measurement triggered with a gate pulse", ND_SINGLE_TRIGGERED_TIME_MSR, NULL,0, NULL},
	{"Simple time measurements", ND_SIMPLE_TIME_MSR, NULL,0, NULL},
	{"Tracks position for motion encoder", ND_POSITION_MSR, NULL,0, NULL},
	{"Pulse-width measurement on two gates", ND_TWO_SIGNAL_EDGE_SEPARATION_MSR, NULL,0, NULL},
	{"Pulse-width measurement", ND_TRIG_PULSE_WIDTH_MSR, NULL,0, NULL},
	{"Simple single pulse-width measurement", ND_SINGLE_PULSE_WIDTH_MSR, NULL,0, NULL},
	{"Simple single period measurement", ND_SINGLE_PERIOD_MSR, NULL,0, NULL},
	{"Simple event counting", ND_SIMPLE_EVENT_CNT, NULL,0, NULL},
	{ NULL,                                  NULL,             NULL,       0, NULL  }
}
*/

MENU PCI_6602_menu[32] =
{
   
   { "Init boards",                	init_boards_6602, NULL,       0, NULL  },
   { "generate_square_wave",        generate_square_wave, NULL,       0, NULL  },
   { "stop_square_wave",            stop_square_wave, NULL,       0, NULL  },
   { "continuous_buff_event",            continuous_buff_event, NULL,       0, NULL  },
   
   { "Detect NI boards",       		detect_NI_boards,	NULL,       0, NULL  },
   { "Test",						test_program, NULL, 0, NULL				},
   /*{ "Counter ZERO",				NULL,   Zero_counter_menu,       0, NULL  },
   { "Counter ONE",               	NULL,   One_counter_menu,       0, NULL  },
   { "Counter TWO",                	NULL,   Two_counter_menu,       0, NULL  },
   { "Counter THREE",               NULL,   Three_counter_menu,       0, NULL  },
   { "Counter FOUR",                NULL,   Four_counter_menu,       0, NULL  },
   { "Counter FIVE",                NULL,   Five_counter_menu,       0, NULL  },
   { "Counter SIX",                	NULL,   Six_counter_menu,       0, NULL  },
   { "Counter SEVEN",                				NULL,    		Trigger_menu,       0, NULL  },*/
   
   { NULL,                                  NULL,             NULL,       0, NULL  }
};

int daq6602_main(int argc, char **argv)
{
    detect_NI_boards();
    add_plot_edit_menu_item("PCI_6602_menu", NULL, PCI_6602_menu, 0, NULL);
    
	broadcast_dialog_message(MSG_DRAW,0);
	


	return 0;    
}

#endif

