#define homeDAC_c
#ifndef DAQ_6503_H
#include "daq6503.h"
#endif

int init_CAN(void)
{	DIG_Prt_Config (PCI6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
	DIG_Prt_Config (PCI6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
	DIG_Prt_Config (PCI6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
	DIG_Out_Prt (PCI6503,PORT_C_AD,0xFF);
	IO_write (CNA_1, 0);
	IO_write (CNA_2, 0);
	IO_write (CNA_3, 0);
	IO_read (CAN_1);
    
    return 0;
}

int IO_write(unsigned char canal, short int data)
{
    char voie;
    unsigned char port_C_value=0;
    
    voie=canal*64;
    DIG_Prt_Config (PCI6503,PORT_A_AD,NO_HANDSHAKING,OUT_SIGNAL);
    DIG_Prt_Config (PCI6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
    DIG_Prt_Config (PCI6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
    /*DIG_Out_Prt (PCI6503,PORT_C_AD,0xC0);*/
    DIG_Out_Prt (PCI6503,PORT_A_AD,(0xFF&&data));
    DIG_Out_Prt (PCI6503,PORT_B_AD,63+voie);
    DIG_Out_Line (PCI6503,PORT_C_AD,6,1);
    DIG_Out_Prt (PCI6503,PORT_B_AD,9+voie);
    DIG_Out_Prt (PCI6503,PORT_B_AD,12+voie);
    DIG_Out_Prt (PCI6503,PORT_A_AD,((0xFF00&&data)>>8));
    DIG_Out_Prt (PCI6503,PORT_B_AD,5+voie);
    DIG_Out_Line (PCI6503,PORT_C_AD,6,0);
    DIG_Prt_Config (PCI6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
    DIG_Prt_Config (PCI6503,PORT_B_AD,HANDSHAKING,OUT_SIGNAL);
     

    return 0;
}

int Write_DAC_value(void)
{   static char canal=0;
    static short int value=0;
    
    
    win_scanf("Value to Write %d",&value);
    IO_write(canal,value);
    return 0;
}

