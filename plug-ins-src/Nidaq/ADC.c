unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,nidaq,
  StdCtrls, Spin, ExtCtrls;
procedure iowrite (canal : byte;data : word);
function ioread (canal : byte):smallint;

type
  TForm1 = class(TForm)
    Edit2: TEdit;
    Button2: TButton;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    Label2: TLabel;
    Timer1: TTimer;
    Button1: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);

end;


var
  Form1: TForm1;



  
implementation
 {$R *.DFM}



procedure initialisation;
begin
DIG_Prt_Config (1,0,0,0);
DIG_Prt_Config (1,1,0,1);
DIG_Prt_Config (1,2,0,1);
DIG_Out_Prt (1,2,$FF);
iowrite (1, 0);
iowrite (2, 0);
iowrite (3, 0);
ioread (1);
end;


function ioread (canal : byte):smallint;
var
voie : byte;
ptr1 : longint;
mesure : smallint;
begin
DIG_Prt_Config (1,0,0,0);
DIG_Prt_Config (1,1,0,1);
DIG_Prt_Config (1,2,0,1);
{DIG_Out_Prt (1,2,$CF);}
voie := (canal-1) * 2;
DIG_Out_Prt (1,1,voie+57);
DIG_Out_Line (1,2,6,1);
DIG_Out_Prt (1,1,voie+0);
DIG_Out_Prt (1,1,voie+8);
DIG_In_Prt (1,0,@ptr1);
mesure := ptr1 * 256;
DIG_Out_Prt (1,1,voie+9);
DIG_In_Prt (1,0,@ptr1);
ioread := ptr1 + mesure;
DIG_Out_Prt (1,1,voie+57);
DIG_Out_Line (1,2,6,0);
DIG_Prt_Config (1,1,1,1);
end;


procedure iowrite (canal : byte;data : word);
type
    pt1 = array [0..3] of byte;
var
voie : byte;
p1 : ^pt1;
begin
DIG_Prt_Config (1,0,0,1);
DIG_Prt_Config (1,1,0,1);
DIG_Prt_Config (1,2,0,1);
{DIG_Out_Prt (1,2,$C0);
}
p1 := @data;
voie := canal * 64;
DIG_Out_Prt (1,0,p1^[0]);
DIG_Out_Prt (1,1,63+voie);
DIG_Out_Line (1,2,6,1);
DIG_Out_Prt (1,1,9+voie);
DIG_Out_Prt (1,1,12+voie);
DIG_Out_Prt (1,0,p1^[1]);
DIG_Out_Prt (1,1,5+voie);
DIG_Out_Line (1,2,6,0);
DIG_Prt_Config (1,0,0,0);
{DIG_Prt_Config (1,1,1,1);
 }
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
initialisation;
end;


procedure TForm1.Button2Click(Sender: TObject);
begin
iowrite (spinedit1.value, spinedit2.value)
end;




procedure TForm1.Timer1Timer(Sender: TObject);
var Lvaleur : longint;

begin
Lvaleur := ioread (spinedit1.value) + 32768;
edit2.text := inttostr (Lvaleur);
end;


procedure TForm1.Button1Click(Sender: TObject);
var n,courbe : word;
    t : real;

begin
t := 0;
for n:= 0 to 10000 do
    begin
    courbe := round ((sin (t) + 1) * 30000);
    spinedit2.value := courbe;
    iowrite (spinedit1.value,courbe) ;
    t:= t+(pi/100);
 end;
end;

end.

