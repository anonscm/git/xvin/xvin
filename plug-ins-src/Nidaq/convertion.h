#ifndef CONVERTION_H
#define CONVERTION_H




#define CNA_1   1
#define CNA_2   2
#define CNA_3   3
#define CAN_1   3 /*1*/

#ifndef PORT_A_AD
#define PORT_A_AD     0x00
#endif

#ifndef PORT_B_AD
#define PORT_B_AD     0x01
#endif

#ifndef PORT_C_AD
#define PORT_C_AD     0x02
#endif

#define PIEZZO_100	100
#define	PIEZZO_250	250


XV_ARRAY(MENU,  project_menu);



#define PCI_6503   256
#define PCI_6602   232 

PXV_FUNC(int, do_quit, (void));
PXV_FUNC(int, init_CAN,(void));
PXV_FUNC(int, IO_write,(unsigned char canal, short int data));
PXV_FUNC(int, Write_DAC_value,(void));
PXV_FUNC(int, init_CNA,(void));
PXV_FUNC(int, IO_read,(unsigned char canal));
PXV_FUNC(int, Read_ADC_value,(void));


PXV_FUNC(int, set_piezzo_in_micron,(int canal,float piezzo_pos,int Piezzo_type));
PXV_FUNC(float, read_piezzo_in_micron,(int canal, int Piezzo_type));

PXV_FUNC(int, set_step,(void));


PXV_FUNC(int, inc_of_nstep,(void));


PXV_FUNC(int, inc_Z_value,(int step));

PXV_FUNC(int, set_Z_step,(float z));





#endif
