/*******************************************************************
				 Program for driving PIFOC with C. Herrmann controller
				 				november 03
				 				JF Allemand
	 				No responsability for any bug recognized by the author
************************************************************************/


#define CONVERTION_C

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"
/*# include "menus/image/im_mn.h"*/

#ifndef motor_H
#include "motor.h"
#endif 

#ifndef homeADC_H
#include "homeADC.h"
#endif 

#include "nidaqcns.h"
#include "nidaq.h"

XV_ARRAY(MENU,  project_menu);
XV_FUNC(int, do_quit, (void));

#ifndef CONVERTION_H
#include "convertion.h"
#endif

int Piezzo_type = PIEZZO_100;

int		small_step = 2, big_step = 20;
float   obj_position = 0, Z_step = 0.1;
extern float n_rota;
extern float n_magnet_z;


int NI_6602, NI_6503;
/*************************************************************************/
/*				First determine piezzo type								*/
int what_is_piezzo(void)
{
    	int i=0;
	if(updating_menu_state != 0)	return D_O_K;
	
	win_printf("We are in piezzo type");
	Piezzo_type = get_config_int("PIFOC","last_piezzo",0);
	if (Piezzo_type != PIEZZO_100 && Piezzo_type != PIEZZO_250) 
 		{
  	        i =win_scanf("What is your PIFOC range (in micron)?%d",&Piezzo_type);
  	        if (i ==  CANCEL) return -1;
  	        set_config_int("PIFOC","last_piezzo",Piezzo_type);
  	        Piezzo_type = get_config_int("PIFOC","last_piezzo",PIEZZO_100);
       		
	        if (Piezzo_type != PIEZZO_100 && Piezzo_type != PIEZZO_250) 
            {        win_printf("I do not know you Pifoc type");
            		  return -1;
	        }    
 		} 		
	
	return Piezzo_type;
} 


/**********check NI boards on the computer*********************************************/

int detect_NI_boards(void)
{	
	short int infovalue;
	
	int i;
	int status;
	
 if(updating_menu_state != 0)	return D_O_K;	
 
    for (i=0;i<3;i++)
	{
		status = Init_DA_Brds (i,&infovalue);
		if (infovalue==PCI_6503)
  		{
    	 	NI_6503=i;
    	 	win_printf(" Board %d is DAQ6503",i);
 	 	}
 	 	else if (infovalue==PCI_6602)
  		{
    	 	NI_6602=i;
    	 	win_printf(" Board %d is Counter 6602",i);
 	 	}
 
 	   else 
       { 
              win_printf("No board number %d",i);
              NI_6602 = 1;
       }
 	    
 	

   }
   if (NI_6503 == 0) 
   {
      win_printf("No motor available");
      return 0;
   }
   /*status = Get_DAQ_Device_Info (NI_6602, ND_DEVICE_TYPE_CODE		, &infoValue);
   win_printf("Counter on board=%d (ND_AM9513 ?)",infovalue);*/
   return 0;
}

/************************Initialise the converter********************************/
int init_CNA(void)
{
	if(updating_menu_state != 0)	return D_O_K;	
    DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
	DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
	DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
	DIG_Out_Prt (NI_6503,PORT_C_AD,0xFF);
    IO_write(CNA_1,0);
    IO_write(CNA_2,0);
    IO_write(CNA_3,0);
    IO_read(CAN_1);
    IO_read(CAN_1);
    /*win_printf("CNA_1 %d CNA_2 %d CNA_3 %d CAN_1%d",CNA_1,CNA_2,CNA_3,CAN_1);*/
    return 0;
}

int init_CAN(void)
{	
	if(updating_menu_state != 0)	return D_O_K;	
 	DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
	DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
	DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
	DIG_Out_Prt (NI_6503,PORT_C_AD,0xFF);
	IO_write (CNA_1, 0);
	IO_write (CNA_2, 0);
	IO_write (CNA_3, 0);
	IO_read (CAN_1);
	IO_read (CAN_1);
    
    return 0;
}


/******************Write value routine**************************************/
int IO_write(unsigned char canal, short int data)
{
    unsigned char voie;
    
    voie=canal*64;
    DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,OUT_SIGNAL);
    DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
    DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
    DIG_Out_Prt (NI_6503,PORT_A_AD,(0xFF&data));
    DIG_Out_Prt (NI_6503,PORT_B_AD,63+voie);
    DIG_Out_Line (NI_6503,PORT_C_AD,6,1);
    DIG_Out_Prt (NI_6503,PORT_B_AD,9+voie);
    DIG_Out_Prt (NI_6503,PORT_B_AD,12+voie);
    DIG_Out_Prt (NI_6503,PORT_A_AD,((0xFF00&data)>>8));
    DIG_Out_Prt (NI_6503,PORT_B_AD,5+voie);
    DIG_Out_Line (NI_6503,PORT_C_AD,6,0);
    DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
     

    return 0;
}

/********************************Read value routine***************************/
int IO_read(unsigned char canal)
{
    long int voie;
    int i;
    unsigned long mesure=0;
    unsigned long read_1=0;
    unsigned long read_2=0;
    
    
    voie = (canal-1) * 2;
    
    for (i=0;i<2;i++)/* we need to read twice!!!!!!*/
    {
    DIG_Prt_Config (NI_6503,PORT_A_AD,NO_HANDSHAKING,IN_SIGNAL);
    DIG_Prt_Config (NI_6503,PORT_B_AD,NO_HANDSHAKING,OUT_SIGNAL);
    DIG_Prt_Config (NI_6503,PORT_C_AD,NO_HANDSHAKING,OUT_SIGNAL);
    
    DIG_Out_Prt (NI_6503,PORT_B_AD,voie+57);
    DIG_Out_Line (NI_6503,PORT_C_AD,6,1);
    DIG_Out_Prt (NI_6503,PORT_B_AD,voie+0);
    DIG_Out_Prt (NI_6503,PORT_B_AD,voie+8);
    DIG_In_Prt (NI_6503,PORT_A_AD,&read_1);
    
    DIG_Out_Prt (NI_6503,PORT_B_AD,voie+9);
    DIG_In_Prt (NI_6503,PORT_A_AD,&read_2);
    
    DIG_Out_Prt (NI_6503,PORT_B_AD,voie+57);
    DIG_Out_Line (NI_6503,PORT_C_AD,6,0);
    DIG_Prt_Config (NI_6503,PORT_B_AD,HANDSHAKING,OUT_SIGNAL);
    }

    mesure = ((((0xff&read_1)<<8)+(0xff&read_2)+32768) & 0xffff);
    draw_bubble(screen,0,750,50,"canal %d read 1 %d  read 2 %d  mesure %d ",(int) canal, read_1, read_2,mesure);
    
    return (unsigned int)mesure;
}




/******************Read and set value in ADC value (0-65535)***************/
int Read_ADC_value(void)
{   static int  canal = CAN_1;

	if(updating_menu_state != 0)	return D_O_K;	
    
    win_scanf("canal to read %d",&canal);
   /* win_scanf("Channel to read %d",&canal);*/
   draw_bubble(screen,0,750,100,"ADC value %d",IO_read((unsigned char) canal));	
    //win_printf("ADC value %d",IO_read((unsigned char) canal));
    return 0;

}

int Write_DAC_value(void)
{   static int canal=2;
    static int value=0;
    	
   	if(updating_menu_state != 0)	return D_O_K;	
    
    win_scanf("Channel to write %d \n Value to Write %d",&canal,&value);
    IO_write((unsigned char)canal, (short int)value);
    return 0;
}
/*************************************************************************/


/*********************************Set positions of the PIFOC (microns)************/



int set_Z_value(float z)  /* in microns */
{
    int canal=2;
	set_piezzo_in_micron(canal,z,Piezzo_type);
	obj_position = read_piezzo_in_micron(CAN_1,Piezzo_type);
	return 0;
}



int set_piezzo_in_micron(int canal,float piezzo_pos,int Piezzo_type)
{
    
    return IO_write((unsigned char)canal, (short int)(piezzo_pos/Piezzo_type*65535));
}

int set_piezzo(void)
{   int canal=2;
    static float piezzo_pos=0.;
    if(updating_menu_state != 0)	return D_O_K;
    
    win_scanf("Desired piezzo position (in micron)? %f",&piezzo_pos);
    
    return set_piezzo_in_micron(canal,piezzo_pos,Piezzo_type);
}


/*********************************Read positions of the PIFOC (microns)************/
float read_piezzo_in_micron(int canal, int Piezzo_type)
{
    return (float)(Piezzo_type*IO_read((unsigned char) canal)/65535);
}

int read_piezzo(void)
{   int  canal = CAN_1;
    
    if(updating_menu_state != 0)	return D_O_K;
   
    win_printf("Piezzo position in microns %f",read_piezzo_in_micron(canal,Piezzo_type));
    return 0;

}











/********************COMFORT ADJUSTMENT*******************************/


int adjust_focus(void)
{   int  canal = CAN_1;
	int key_read = 0;
    
    if(updating_menu_state != 0)	return D_O_K;
   
    while (key_read != 'q')
    {
       key_read = readkey();
       
           if ((key_read & 0xff) == 'p')   IO_write((unsigned char)canal, (short int)(1+(float)(Piezzo_type*IO_read((unsigned char) canal)/65535)));
           if ((key_read & 0xff) == 'm')   IO_write((unsigned char)canal, (short int)(-1+(float)(Piezzo_type*IO_read((unsigned char) canal)/65535)));
           if ((key_read & 0xff) == 16)   IO_write((unsigned char)canal, (short int)(.1+(float)(Piezzo_type*IO_read((unsigned char) canal)/65535)));
           if ((key_read & 0xff) == 13)   IO_write((unsigned char)canal, (short int)(-.1+(float)(Piezzo_type*IO_read((unsigned char) canal)/65535)));
    
    
    
    
    }
    
    
    return 0;

}

int		do_change_focus_p_one_micron(void)
{
	int canal = 2;
	O_p *op;
	d_s *dsi;
	pltreg *pr;
	
	
	if(updating_menu_state != 0)	
	{
	    add_keyboard_short_cut(0, KEY_PLUS_PAD, 0, do_change_focus_p_one_micron);
		return D_O_K;	
	}
    
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
	return win_printf("cannot find data");
	IO_write((unsigned char)canal, (short int)((float)65535/Piezzo_type+(float)(Piezzo_type*IO_read((unsigned char) canal)/65535)));
			
	return 0;
}

int		do_change_focus_m_one_micron(void)
{
	int canal = 2;
	O_p *op;
	d_s *dsi;
	pltreg *pr;
	
	
	if(updating_menu_state != 0)	
	{
	    add_keyboard_short_cut(0, KEY_MINUS_PAD, 0, do_change_focus_m_one_micron);
		return D_O_K;	
	}
    
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
	return win_printf("cannot find data");
	IO_write((unsigned char)canal, (short int)(-(float)65535/Piezzo_type+(float)(Piezzo_type*IO_read((unsigned char) canal)/65535)));
			
	return 0;
}
/********************from old version********************************/

int set_step(void)
{
	register int i;
	static float z = 0.1;
	
    if(updating_menu_state != 0)	return D_O_K;
	i = win_scanf("What step size do you want (\\mu m) ?\n %f",&z);
	if (i == CANCEL) return OFF;
	
	set_Z_step(z);
	
	return OFF;
}

int inc_of_nstep(void)
{
	register int i;
	static int n = 0;
	
    if(updating_menu_state != 0)	return D_O_K;
	i = win_scanf("How many steps? %d",&n);
	if (i == CANCEL)	return OFF;
	inc_Z_value(n);

	return OFF;
}


int inc_Z_value(int step)  /* in micron */
{
	set_Z_value(obj_position + Z_step*step);
		
	return 0;
}

int set_Z_step(float z)  /* in micron */
{
	Z_step = z;
	return 0;
}




/***********************MENU**************************************/

MENU convertion_menu[32] = 
{  
   { "&Write_DAC_value",           	Write_DAC_value, NULL ,  0, NULL  },  
   { "&Read_ADC_value",           	Read_ADC_value, NULL ,  0, NULL  },  
   { "& Set Piezzo",           		set_piezzo, NULL ,  0, NULL  },  
   { "&Read Piezzo",           		read_piezzo, NULL ,  0, NULL  },
   { "&Adjust focus",           	adjust_focus, NULL ,  0, NULL  }, 
   { "&+ focus",           			do_change_focus_p_one_micron, NULL ,  0, NULL  }, 
   { "&- focus",           			do_change_focus_m_one_micron, NULL ,  0, NULL  }, 
   	
	{ "Set step Z",    				set_step, NULL ,  0, NULL  },
	{"n steps",						inc_of_nstep, NULL ,  0, NULL  },
//	{"Set Z time",					set_Z_check, NULL ,  0, NULL  },
	
     
   { NULL,               NULL,             NULL,       0, NULL  }
};




int convertion_main(int argc, char **argv)
{
	detect_NI_boards();
    init_CNA();
    what_is_piezzo();
    if (what_is_piezzo()==-1) return win_printf("Piezzo not known");
    
    
    add_plot_edit_menu_item("Convertion menu", NULL, convertion_menu, 0, NULL);
    add_image_treat_menu_item("Convertion menu", NULL, convertion_menu, 0, NULL);
    
	broadcast_dialog_message(MSG_DRAW,0);
	


	return 0;    
}











