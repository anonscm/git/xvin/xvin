/**
\file motor.c
\brief Program for driving DC motors with C Herrmann controller
\version 11/03
\author JF Allemand	
*/				 				
 
 
#define motor_C

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

#include "motor.h"

#include "nidaqcns.h"
#include "nidaq.h"

XV_ARRAY(MENU,  project_menu);
XV_FUNC(int, do_quit, (void));

#define PCI_6503   256
#define PCI_6602   232 
int NI_6602, NI_6503;

int detect_NI_boards(void)
{	
	short int infovalue;
	long int infoValue;
	int i;
	int status;
	
    if(updating_menu_state != 0)	return D_O_K;	
    for (i=0;i<3;i++)
	{
		status = Init_DA_Brds (i,&infovalue);
		if (infovalue==PCI_6503)
  		{
    	 	NI_6503=i;
    	 	win_printf(" Board %d is DAQ6503",i);
 	 	}
 	 	else if (infovalue==PCI_6602)
  		{
    	 	NI_6602=i;
    	 	win_printf(" Board %d is Counter 6602",i);
 	 	}
 
 	   else 
       { 
              win_printf("No board number %d",i);
              //NI_6602 = 1;
       }
 	    
 	

   }
   if (NI_6503 == 0) 
   {
      win_printf("No motor available");
      return 0;
   }
   /*status = Get_DAQ_Device_Info (NI_6602, ND_DEVICE_TYPE_CODE		, &infoValue);
   win_printf("Counter on board=%d (ND_AM9513 ?)",infovalue);*/
   return 0;
}
/*int device(void)
{	

	long int infoValue;
	short int deviceNumberCode;
	int i,status;
	
	for (i=0;i<5;i++)
	{
		status = Init_DA_Brds (i, &deviceNumberCode);
		if (deviceNumberCode == PCI6503) NI_6503=i;
		else win_printf("6503 is not card number %d", i);
	}
	
	status = Get_DAQ_Device_Info (NI_6503, ND_BASE_ADDRESS	, &infoValue);
	win_printf("Adresse de la 6503 %d", infoValue);
	
	status = Get_DAQ_Device_Info (NI_6503, ND_DEVICE_TYPE_CODE	, &infoValue);
	win_printf("Device type %d", infoValue);
	
 	return 0;


}
*/

/************************general interface routines*************************/
int send_command_to_controller (unsigned char commande,unsigned char adr,unsigned char data)
{
	char n=0;
    short int hands= 0;
    /*char *value=NULL;*/
    int status;
  
    status= DIG_Prt_Config(NI_6503,PORT_A,NO_HANDSHAKING,OUT_SIGNAL);

   if (adr < 4)
   {
   
   		status = DIG_Out_Prt (NI_6503,PORT_A,data);
   		
      	status = DIG_Out_Prt (NI_6503,PORT_B,commande+adr);
      	while ((hands!=1)& (n < 100)) 
      	{
         n = n + 1;
         status = DIG_Prt_Status (NI_6503,PORT_B,&hands);
        }
        if (hands == 1) 
        {
                status = DIG_Out_Prt (NI_6503,PORT_B,0xFF);/**1 donc on �crit sur B pour avoir un accuse de reception je crois**/
                hands = 0;
                n=0;
                while ((hands != 1) & (n < 100))
                        {
                                      n = n + 1;
                                      status = DIG_Prt_Status (NI_6503,PORT_B,&hands);
                        }
        }
       } 
        
        status = DIG_Prt_Config(NI_6503,PORT_A,NO_HANDSHAKING,IN_SIGNAL);/***on remet la ligne en etat***/
   
   	return 0;
}



unsigned char read_data_from_controller (unsigned char commande,unsigned char  adr)
{
	int n=0;

	long int value;
	int status;
    short int hands=0;
        
    
    if (adr < 4) 
    {
       status = DIG_Out_Prt (NI_6503,PORT_B,commande+adr);
          while ((hands != 1) & (n < 1000))
          {
    	          n = n + 1;
    	          status = DIG_Prt_Status (NI_6503,PORT_B,&hands);
          }
       if (hands == 1)
          {
          	status = DIG_In_Prt (NI_6503,PORT_A, &value);
          	status = DIG_Out_Prt (NI_6503,PORT_B,0xFF);
      	  }
 	   status = DIG_Prt_Config(NI_6503,PORT_B,HANDSHAKING,OUT_SIGNAL);
    }

return  value;
}



int  read_card_type(void)
{        unsigned char card_type[4];
		 unsigned char i;
		 
		 if(updating_menu_state != 0)	return D_O_K;	

         for (i=0;i<4;i++)
		{
			card_type[i]=read_data_from_controller(CARD_TYPE,CARD_ADRESS+i);
			win_printf ("card type %0x i=%0x ",card_type[i],i);
         	if (card_type[0]!=0x02) win_printf("T'as un soucis! Je ne suis pas qui tu crois");
         	
		}
		return (int)card_type[0];
}




/****************************EXCHANGE OF PID PARAMETERS**************************/


int set_Vmax_moteur_1(unsigned char v_max_1)
{  
  send_command_to_controller(READ_V_MAX_1,CARD_ADRESS,v_max_1);
   
  return 0;
}

int set_Vmax_moteur_2(unsigned char v_max_2)
{   
  send_command_to_controller(READ_V_MAX_2,CARD_ADRESS,v_max_2);
  
  return 0;
}



int set_speed_feedback_1(unsigned char feedback_speed)
{
  send_command_to_controller(SPEED_FEEDBACK_VMAX_MOT_1,CARD_ADRESS,feedback_speed);
    
  return 0;
}

int set_speed_feedback_2(unsigned char feedback_speed)
{	
  send_command_to_controller(SPEED_FEEDBACK_VMAX_MOT_2,CARD_ADRESS,feedback_speed);
  
  return 0;
}


int set_acc_pos_feedback_1(unsigned char acc)
{   
  int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,ACCELERATION_1);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,acc);
    
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
        
  return 0;
}

int set_acc_pos_feedback_2(unsigned char acc)
{  
  int i;
  clock_t start;

  send_command_to_controller(COMMAND_PID,CARD_ADRESS,ACCELERATION_2);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,acc);
    
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
  return 0;
}

int set_consigne_pos_moteur_1(long cons_pos_1)
{    
  unsigned char pos1=0;
  unsigned char pos2=0;
  unsigned char pos3=0;
  unsigned char pos4=0;

  pos4=(unsigned char)(cons_pos_1&0xFF);
  pos3=(unsigned char)((cons_pos_1&0xFF00)>>8);
  pos2=(unsigned char)((cons_pos_1&0xFF0000)>>16);
  pos1=(unsigned char)((cons_pos_1&0xFF000000)>>24);
  send_command_to_controller(CONSIGNE_POS_1_1,CARD_ADRESS,pos1);
  send_command_to_controller(CONSIGNE_POS_1_2,CARD_ADRESS,pos2);
  send_command_to_controller(CONSIGNE_POS_1_3,CARD_ADRESS,pos3);
  send_command_to_controller(CONSIGNE_POS_1_4,CARD_ADRESS,pos4);

  return 0;
}

int set_consigne_pos_moteur_2(long cons_pos_2)
{     
  unsigned char pos1=0;
  unsigned char pos2=0;
  unsigned char pos3=0;
  unsigned char pos4=0;
	
  pos4=(unsigned char)(cons_pos_2&0xFF);
  pos3=(unsigned char)((cons_pos_2&0xFF00)>>8);
  pos2=(unsigned char)((cons_pos_2&0xFF0000)>>16);
  pos1=(unsigned char)((cons_pos_2&0xFF000000)>>24);
  send_command_to_controller(CONSIGNE_POS_2_1,CARD_ADRESS,pos1);
  send_command_to_controller(CONSIGNE_POS_2_2,CARD_ADRESS,pos2);
  send_command_to_controller(CONSIGNE_POS_2_3,CARD_ADRESS,pos3);
  send_command_to_controller(CONSIGNE_POS_2_4,CARD_ADRESS,pos4);

  return 0;
}


/*int set_speed_feedback_v_lim_1(unsigned char vlim)
{
    send_command_to_controller(SPEED_FEEDBACK_VLIM_MOT_1,CARD_ADRESS,vlim);
    return 0;
}

int set_speed_feedback_v_lim_2(unsigned char vlim)
{
    send_command_to_controller(SPEED_FEEDBACK_VLIM_MOT_2,CARD_ADRESS,vlim);
    return 0;
}
*/

int set_lim_dec_feedback_pos_1(short int lim_pos_feedback)
{  
  unsigned char pos0=0;
  unsigned char pos1=0;
	
  pos0=(unsigned char)(lim_pos_feedback&0xFF);
  pos1=(unsigned char)((lim_pos_feedback&0xFF00)>>8);
   
  send_command_to_controller(POS_FEEDBACK_LIM_1_MOT_1,CARD_ADRESS,pos1);
  send_command_to_controller(POS_FEEDBACK_LIM_2_MOT_1,CARD_ADRESS,pos0);
    
  return 0;
}

int set_lim_dec_feedback_pos_2(short int lim_pos_feedback)
{  	
  unsigned char pos0=0;
  unsigned char pos1=1;

  pos0=(unsigned char)(lim_pos_feedback&0xFF);
  pos1=(unsigned char)((lim_pos_feedback&0xFF00)>>8);
  send_command_to_controller(POS_FEEDBACK_LIM_1_MOT_2,CARD_ADRESS,pos1);
  send_command_to_controller(POS_FEEDBACK_LIM_2_MOT_2,CARD_ADRESS,pos0);
   
  return 0;
}



long int read_pos_motor_1(void)
{    
  unsigned char pos1=0;
  unsigned char pos2=0;
  unsigned char pos3=0;
  unsigned char pos4=0;
  long int position_motor_1=0;

  pos1=   read_data_from_controller(LECT_POS_1_1,CARD_ADRESS);
  pos2=   read_data_from_controller(LECT_POS_1_2,CARD_ADRESS);
  pos3=   read_data_from_controller(LECT_POS_1_3,CARD_ADRESS);
  pos4=   read_data_from_controller(LECT_POS_1_4,CARD_ADRESS);
  draw_bubble(screen,0,750,50,"1 %d pos2 %d  3 %d  4 %d ",(int)pos1, (int)pos2, (int)pos3,(int)pos4);
    
  position_motor_1=pos4+(pos3<<8)+(pos2<<16)+(pos1<<24);

  return position_motor_1;
}

long int read_pos_motor_2(void)
{    
  unsigned char pos1=0;
  unsigned char pos2=0;
  unsigned char pos3=0;
  unsigned char pos4=0;
  long int position_motor_2=0;

  pos1=   read_data_from_controller(LECT_POS_2_1,CARD_ADRESS);
  pos2=   read_data_from_controller(LECT_POS_2_2,CARD_ADRESS);
  pos3=   read_data_from_controller(LECT_POS_2_3,CARD_ADRESS);
  pos4=   read_data_from_controller(LECT_POS_2_4,CARD_ADRESS);
  position_motor_2=pos4+(pos3<<8)+(pos2<<16)+(pos1<<24);
    
  return position_motor_2;
}




int new_P_motor_1(unsigned char new_P_1)
{		
  int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,P_1);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_P_1); 
    
    
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
    
  return 0;
}

int new_P_motor_2(unsigned char new_P_2)
{		   
  int i;
  clock_t start;
  send_command_to_controller(COMMAND_PID,CARD_ADRESS,P_2);
  send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_P_2); 
       
    
  for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
   
  return 0;
}


int new_I_motor_1(unsigned char new_I_1)
{        int i;
    clock_t start;
	send_command_to_controller(COMMAND_PID,CARD_ADRESS,I_1);
        send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_I_1); 
       
    
    for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
    return 0;
}

int new_I_motor_2(unsigned char new_I_2)
{
         int i;
    clock_t start;
		send_command_to_controller(COMMAND_PID,CARD_ADRESS,I_2);
        send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_I_2);
        
    
    for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
    return 0;
}

int new_D_motor_1(unsigned char new_D_1)
{
        int i;
    	clock_t start;
		send_command_to_controller(COMMAND_PID,CARD_ADRESS,D_1);
        send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_D_1);
        
    
    for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
    return 0;
}

int new_D_motor_2(unsigned char new_D_2)
{		int i;
    	clock_t start;
        send_command_to_controller(COMMAND_PID,CARD_ADRESS,D_2);
        send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_D_2);
        
    
    for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
    return 0;
}

int new_R_motor_1(unsigned char new_R_1)
{	int i;
    clock_t start;

        send_command_to_controller(COMMAND_PID,CARD_ADRESS,R_1);
        send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_R_1);
        
    
        for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
        return 0;
}

int new_R_motor_2(unsigned char new_R_2)
{		int i;
        clock_t start;
        send_command_to_controller(COMMAND_PID,CARD_ADRESS,R_2);
        send_command_to_controller(PARAMETER_PID,CARD_ADRESS,new_R_2);
        
    
    for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
    return 0;
}



/*****************************RESET THE MOTORS******************************/

int new_res_motor_1(void)
{		int i;
    	clock_t start;
    	
    	if(updating_menu_state != 0)	return D_O_K;	
        send_command_to_controller(COMMAND_PID,CARD_ADRESS,RESET_MOT_1);
        send_command_to_controller(PARAMETER_PID,CARD_ADRESS,RESET_MOT_1);
        
    for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
    return 0;

}

int new_res_motor_2(void)
{			int i;
    		clock_t start;
    		
    		if(updating_menu_state != 0)	return D_O_K;	
           	send_command_to_controller(COMMAND_PID,CARD_ADRESS,RESET_MOT_2);
        	send_command_to_controller(PARAMETER_PID,CARD_ADRESS,RESET_MOT_2);
        
    for (i=0,start=clock();(clock()-start<(2*CLOCKS_PER_SEC))&&(read_data_from_controller(TEST_COMMAND_PID,CARD_ADRESS)!=255);i++);
    return 0;
}	
  



/**********************TRADUCTION EN BOUTONS **************************/

int new_set_P_1(void)
{       static int new_P_1;
        
        
        if(updating_menu_state != 0)	return D_O_K;	
	    win_scanf("Proportional factor motor 1 %d ?",&new_P_1);
        new_P_motor_1((unsigned char)new_P_1);
        return 0;

}

int new_set_P_2(void)
{       static int new_P_2;
        
        if(updating_menu_state != 0)	return D_O_K;	
	    win_scanf("Proportional factor motor 2 %d ?",&new_P_2);
        new_P_motor_2((unsigned char)new_P_2);
        return 0;


}


int new_set_I_1(void)
{       static int new_I_1;
         
	    
	    if(updating_menu_state != 0)	return D_O_K;	
 		win_scanf("Integral factor motor 1 %d ?",&new_I_1);
        new_I_motor_1((unsigned char)new_I_1);
        return 0;
}

int new_set_I_2(void)
{       static int new_I_2;
         
         
        if(updating_menu_state != 0)	return D_O_K;	
	    win_scanf("Integral factor motor 2 %d ?",&new_I_2);
        new_I_motor_2((unsigned char)new_I_2);
        return 0;
}

int new_set_D_1(void)
{       static int new_D_1;
        
        if(updating_menu_state != 0)	return D_O_K;	
	    win_scanf("Derivative factor motor 1 %d ?",&new_D_1);
        new_D_motor_1((unsigned char)new_D_1);
        return 0;
}

int new_set_D_2(void)
{       static int new_D_2;
         
	    
	    if(updating_menu_state != 0)	return D_O_K;	
        win_scanf("Derivative factor motor 2 %d ?",&new_D_2);
        new_D_motor_2((unsigned char)new_D_2);
        return 0;
}

int new_set_ratio_motor_1(void)
{       static int new_R_1;
        
        if(updating_menu_state != 0)	return D_O_K;	
	    win_scanf("Ratio factor motor 1 %d ?",&new_R_1);
        new_R_motor_1((unsigned char)new_R_1);
        return 0;
}

int new_set_ratio_motor_2(void)
{       static int new_R_2;
         
	    
	    if(updating_menu_state != 0)	return D_O_K;	
 		win_scanf("Ratio factor motor 2 %d ?",&new_R_2);
        new_R_motor_2((unsigned char)new_R_2);
        return 0;
}

int new_reset_motor_1(void)
{
        
        if(updating_menu_state != 0)	return D_O_K;	
	    new_res_motor_1();
        return 0;

}

int new_reset_motor_2(void)
{
 
	   
	   if(updating_menu_state != 0)	return D_O_K;	
 	   new_res_motor_2();
        return 0;
}	

int set_cons_max_speed_pos_1(void)
{    static int v_max=0;


	if(updating_menu_state != 0)	return D_O_K;	
      
	    win_scanf("Max Speed %d ?",&v_max);
        set_speed_feedback_1((unsigned char)v_max);
      return 0;
}

int set_cons_max_speed_pos_2(void)
{    static int v_max=0;


	if(updating_menu_state != 0)	return D_O_K;	
      
	    win_scanf("Max Speed %d ?",&v_max);
        set_speed_feedback_2((unsigned char)v_max);
      return 0;
}
	
int Read_pos_mot_1(void)
{	
	
	 
	if(updating_menu_state != 0)	return D_O_K;		
	win_printf("Pos Mot 1 %ld",read_pos_motor_1());
	
	return 0;
}

int Read_pos_mot_2(void)
{	
	 
	if(updating_menu_state != 0)	return D_O_K;		
	win_printf("Pos Mot 2 %ld",read_pos_motor_2());
	
	return 0;
}

int set_cons_pos_mot_1(void)
{	static  long cons1=0;
	 	
	
	if(updating_menu_state != 0)	return D_O_K;	
	win_scanf("Position %d ?",&cons1);
	set_consigne_pos_moteur_1(cons1);
	
	return 0;
}
int set_cons_pos_mot_2(void)
{	static  long  cons2=0;
	 	
	
	if(updating_menu_state != 0)	return D_O_K;	
	win_scanf("Position %d ?",&cons2);
	set_consigne_pos_moteur_2(cons2);
	
	return 0;
}


int set_vitesse_mot_1(void)
{	 
  static int vmax=0;
  
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Speed %d ?(0-255)",&vmax);
  set_Vmax_moteur_1((unsigned char)vmax);
  
  return 0;
}

int set_acc_pos_1(void)
{ 
  static int acc=0;
  
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Acceleration position feedback %d ?",&acc);
  set_acc_pos_feedback_1((unsigned char)acc);
  
  return 0;
}

int set_acc_pos_2(void)
{  
  static int acc=0;
	 
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Acceleration position feedback %d ?",&acc);
  set_acc_pos_feedback_2((unsigned char)acc);
  
  return 0;
}

int set_vitesse_mot_2(void)
{
  static int vmax=0;
     	 
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Speed %d ?",&vmax);
  set_Vmax_moteur_2((unsigned char)vmax);
  
  return 0;
}

int set_cons_pos_lim_1(void)
{  
  static int pos_lim=0;
     
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Pos lim %d ?",&pos_lim);
  set_lim_dec_feedback_pos_1((short int)pos_lim);
	
  return 0;
}

int set_cons_pos_lim_2(void)
{   
  static int pos_lim=0;
     
  if(updating_menu_state != 0)	return D_O_K;	
  win_scanf("Pos lim %d ?",&pos_lim);
  set_lim_dec_feedback_pos_2((short int)pos_lim);
	
  return 0;
}




MENU DAQMOTORmenu[32] = 
{  
  { "&Read_pos_mot_1",           Read_pos_mot_1, NULL ,  0, NULL  }, 
  { "set_cons_pos_mot_1",     set_cons_pos_mot_1, NULL,  0, NULL  }, 
  { "Reset motor 1",new_reset_motor_1, NULL,  0, NULL  },
  { "set_cons_max_speed_pos_1",           set_cons_max_speed_pos_1, NULL ,  0, NULL  },  
  { "set_acc_pos_1",     set_acc_pos_1, NULL,  0, NULL  },
  { "set_cons_pos_lim_1",     set_cons_pos_lim_1, NULL,  0, NULL  },
  { "P motor 1",new_set_P_1, NULL,  0, NULL  },
  { "I motor 1",new_set_I_1, NULL,  0, NULL  },
  { "D motor 1",new_set_D_1, NULL,  0, NULL  },
  { "Ratio motor 1",new_set_ratio_motor_1, NULL,  0, NULL  },
  { "set_vitesse_mot_1",     set_vitesse_mot_1, NULL,  0, NULL  },
  
  { "Read_pos_mot_2",     Read_pos_mot_2, NULL,  0, NULL  },
  { "set_cons_pos_mot_2",     set_cons_pos_mot_2, NULL,  0, NULL  },
  { "Reset motor 2",new_reset_motor_2, NULL, 0,  NULL},
  { "set_cons_max_speed_pos_2",           set_cons_max_speed_pos_2, NULL ,  0, NULL  },  
  { "set_acc_pos_2",     set_acc_pos_2, NULL,  0, NULL  },
  { "set_cons_pos_lim_2",     set_cons_pos_lim_2, NULL,  0, NULL  },
  { "P motor 2",new_set_P_2, NULL, 0,  NULL},
  { "I motor 2",new_set_I_2, NULL, 0,  NULL},
  { "D motor 2",new_set_D_2, NULL, 0,  NULL},
  { "Ratio motor 2",new_set_ratio_motor_2, NULL, 0,  NULL},
  { "set_vitesse_mot_2",     set_vitesse_mot_2, NULL,  0, NULL  },
  { "read_card_type",           read_card_type, NULL ,  0, NULL  }, 
  /* { "do_initialisation",   do_initialisation, NULL, 0,  NULL},*/
  { NULL,               NULL,             NULL,       0, NULL  }
};


int init_motors(void)
{
  set_lim_dec_feedback_pos_1(200);
  set_lim_dec_feedback_pos_2(200);
  new_P_motor_1(20);
  new_P_motor_2(40);
  new_I_motor_1(0);
  new_I_motor_2(3);
  new_D_motor_1(255);
  new_D_motor_2(255);
  new_R_motor_1(6);
  new_R_motor_2(6);
  set_acc_pos_feedback_1(1);
  set_acc_pos_feedback_2(1);
  set_speed_feedback_1(75);
  set_speed_feedback_2(127);
  set_Vmax_moteur_1(128);
  set_Vmax_moteur_2(128);
  
  return 0;
}


int main(int argc, char **argv)
{
  detect_NI_boards();
  init_motors();
  
  add_plot_treat_menu_item("Moteurs menu", NULL, DAQMOTORmenu, 0, NULL);
  add_image_treat_menu_item("Moteurs menu", NULL, DAQMOTORmenu, 0, NULL);
    
  broadcast_dialog_message(MSG_DRAW,0);
	
  return 0;    
}







