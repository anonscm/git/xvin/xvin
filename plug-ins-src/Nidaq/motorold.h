short int DEVICE_6503 ;

#define PORT_A 0
#define PORT_B 1
#define PORT_C 2
#define NO_HANDSHAKING 0
#define HANDSHAKING 1
#define IN_SIGNAL 0
#define OUT_SIGNAL 1
#define PCI6503                     256
#define MOTOR_1                        0
#define MOTOR_2                        1
#define MOTOR_1_2                      2  /********************A verifier******************/
#define READ_V_MAX_1 0x00
#define READ_V_MAX_2 0x08

#define LECT_POS_1_1 0x10
#define LECT_POS_1_2 0x18
#define LECT_POS_1_3 0x20
#define LECT_POS_1_4 0x28

#define LECT_POS_2_1 0x30
#define LECT_POS_2_2 0x38
#define LECT_POS_2_3 0x40
#define LECT_POS_2_4 0x48

#define CONSIGNE_POS_1_1 0x50
#define CONSIGNE_POS_1_2 0x58
#define CONSIGNE_POS_1_3 0x60
#define CONSIGNE_POS_1_4 0x68

#define CONSIGNE_POS_2_1 0x70
#define CONSIGNE_POS_2_2 0x78
#define CONSIGNE_POS_2_3 0x80
#define CONSIGNE_POS_2_4 0x88

#define RESET_POS        0x90
#define RESET_MOTOR_1    0xB1
#define RESET_MOTOR_2    0xB2
#define RESET_MOTOR_1_2  0xB3

#define SPEED_FEEDBACK_ACC   		0x98

#define SPEED_FEEDBACK_VMAX_MOT_1  		0xA0
#define SPEED_FEEDBACK_VLIM_MOT_1  		0xA8
#define POS_FEEDBACK_ACC_MOT_1	     	0xB0
#define POS_FEEDBACK_LIM_1_MOT_1     	0xB8
#define POS_FEEDBACK_LIM_2_MOT_1     	0xC0

#define SPEED_FEEDBACK_VMAX_MOT_2  		0xC8
#define SPEED_FEEDBACK_VLIM_MOT_2  		0xD0
#define POS_FEEDBACK_ACC_MOT_2	     	0xD8
#define POS_FEEDBACK_LIM_1_MOT_2     	0xE0
#define POS_FEEDBACK_LIM_2_MOT_2     	0xE8

#define INITIALISATION       0xF8

#define CARD_TYPE            0xF0
#define CARD_ADRESS			 0X01


/****************************declaration of functions*************************************/
int detect_NI_boards(void);
int device(void);
int send_command_to_controller (unsigned char commande,unsigned char adr,unsigned char data);

int  read_card_type(void);
unsigned char read_data_from_controller (unsigned char commande,unsigned char adr);

int set_Vmax_moteur_1(unsigned char v_max_1);
int set_Vmax_moteur_2(unsigned char v_max_2);
long int read_pos_motor_1(void);

long int read_pos_motor_2(void);
int set_speed_feedback_1(unsigned char feedback_speed);
int set_speed_feedback_2(unsigned char feedback_speed);
int set_speed_feedback_v_lim_1(unsigned char vlim);
int set_speed_feedback_v_lim_2(unsigned char vlim);
int set_acc_speed_feedback(unsigned char acc);
int set_acc_pos_feedback_1(unsigned char acc);
int set_acc_pos_feedback_2(unsigned char acc);

int set_consigne_pos_moteur_1(long cons_pos_1);
int set_consigne_pos_moteur_2(long cons_pos_2);

int set_lim_dec_feedback_pos_1(short int lim_pos_feedback);
int set_lim_dec_feedback_pos_2(short int lim_pos_feedback);

void initialisation(void);
void reset_pos(unsigned char motor);
int set_cons_max_speed_pos_1(void);
	
int Read_pos_mot_1(void);

int Read_pos_mot_2(void);

int set_cons_pos_mot_1(void);
int set_cons_pos_mot_2(void);

int set_vitesse_mot_1(void);

int set_acc_speed(void);

int set_acc_pos_1(void);

int set_acc_pos_2(void);

int set_vitesse_mot_2(void);

int set_cons_pos_lim_1(void);

int set_cons_pos_lim_2(void);

int set_cons_max_speed_lim_1(void);

int set_cons_max_speed_lim_2(void);

int reset_pos_mot_1(void);

int reset_pos_mot_2(void);

int do_initialisation(void);



