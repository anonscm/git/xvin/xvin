#ifndef DAQ6602_H
#define DAQ6602_H

#define PCI_6503   256
#define PCI_6602   232 

# ifndef DAQ6602_C
PXV_VAR(int ,status);
PXV_VAR(int ,NI_6503);
PXV_VAR(int ,NI_6602);
PXV_VAR(long unsigned int * , buffer);
#else
int status;
int NI_6503;
int NI_6602;
long unsigned int *buffer=NULL;
#endif

PXV_FUNC(int, detect_NI_boards,(void));
PXV_FUNC(int, init_boards_6602,(void));
PXV_FUNC(int, config_buff,(int counter_number, int numPoints));
PXV_FUNC(int, generate_square_wave,(void));
PXV_FUNC(int, stop_square_wave,(void));
 
PXV_FUNC(int, test_program,(void));
PXV_FUNC(int, continuous_buff_event,(void));



PXV_FUNC(int, daq6602_main,(int argc, char **argv));

PXV_FUNC(MENU, PCI_6602_menu,[32]) ;

#endif


