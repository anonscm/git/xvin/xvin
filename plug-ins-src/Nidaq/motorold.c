# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"
# include "menus/image/im_mn.h"
# include "menus/plot/plot_mn.h"
#include "motor.h"
#include "nidaqcns.h"
#include "nidaq.h"

XV_ARRAY(MENU,  project_menu);
XV_FUNC(int, do_quit, (void));



#define PCI_6503   256
#define PCI_6602   232 
int NI_6602, NI_6503;

int detect_NI_boards(void)
{	
	short int infovalue;
	long int infoValue;
	int i;
	int status;
	
 
    for (i=0;i<3;i++)
	{
		status = Init_DA_Brds (i,&infovalue);
		if (infovalue==PCI_6503)
  		{
    	 	NI_6503=i;
    	 	win_printf(" Board %d is DAQ6503",i);
 	 	}
 	 	else if (infovalue==PCI_6602)
  		{
    	 	NI_6602=i;
    	 	win_printf(" Board %d is Counter 6602",i);
 	 	}
 
 	   else 
       { 
              win_printf("No board number %d",i);
              NI_6602 = 1;
       }
 	    
 	

   }
   if (NI_6503 == 0) 
   {
      win_printf("No motor available");
      return 0;
   }
   /*status = Get_DAQ_Device_Info (NI_6602, ND_DEVICE_TYPE_CODE		, &infoValue);
   win_printf("Counter on board=%d (ND_AM9513 ?)",infovalue);*/
   status = DIG_Prt_Config(NI_6503,PORT_A,NO_HANDSHAKING,IN_SIGNAL);

   status = DIG_Prt_Config(NI_6503,PORT_B,HANDSHAKING,OUT_SIGNAL);

   return 0;
}
int device(void)
{	

	long int infoValue;
	short int deviceNumberCode;
	int i,status;
	
	for (i=0;i<5;i++)
	{
		status = Init_DA_Brds (i, &deviceNumberCode);
		if (deviceNumberCode == PCI6503) DEVICE_6503=i;
		else win_printf("6503 is not card number %d", i);
	}
	
	status = Get_DAQ_Device_Info (NI_6503, ND_BASE_ADDRESS	, &infoValue);
	win_printf("Adresse de la 6503 %d", infoValue);
	
	status = Get_DAQ_Device_Info (NI_6503, ND_DEVICE_TYPE_CODE	, &infoValue);
	win_printf("Device type %d", infoValue);
	
 	return 0;


}

int send_command_to_controller (unsigned char commande,unsigned char adr,unsigned char data)
{
	char n=0;
    short int hands= 0;
    /*char *value=NULL;*/
    int status;
    
    
    
    
    
    
    status= DIG_Prt_Config(NI_6503,PORT_A,NO_HANDSHAKING,OUT_SIGNAL);
    win_printf("status = %d",status);

   if (adr < 4)
   {
   
   		status = DIG_Out_Prt (NI_6503,PORT_A,data);
   		    win_printf("status = %d",status);
   		
      	status = DIG_Out_Prt (NI_6503,PORT_B,commande+adr);
      	    win_printf("status = %d",status);
      	while ((hands!=1)& (n < 100)) 
      	{
         n = n + 1;
         status = DIG_Prt_Status (NI_6503,PORT_B,&hands);
             win_printf("status = %d",status);
        }
        if (hands == 1) 
        {
                status = DIG_Out_Prt (NI_6503,PORT_B,0xFF);/**1 donc on �crit sur B pour avoir un accuse de reception je crois**/
                    win_printf("status = %d",status);
                hands = 0;
                n=0;
                while ((hands != 1) & (n < 100))
                        {
                                      n = n + 1;
                                      status = DIG_Prt_Status (NI_6503,PORT_B,&hands);
                                          win_printf("status = %d",status);
                        }
        }
       } 
        
        status = DIG_Prt_Config(NI_6503,PORT_A,NO_HANDSHAKING,IN_SIGNAL);/***on remet la ligne en etat***/
               win_printf("status = %d",status);
   	return 0;
}



unsigned char read_data_from_controller (unsigned char commande,unsigned char adr)
{
	int n=0;

	long int value;
	int status;
    short int hands=0;
        
    if (adr < 4) 
    { 
  		  status = DIG_Out_Prt (NI_6503,PORT_B,commande+adr);
          while ((hands != 1) & (n < 100))
          {
    	          n = n + 1;
    	          status = DIG_Prt_Status (NI_6503,PORT_B,&hands);
          }
       if (hands == 1)
          {	status = DIG_In_Prt (NI_6503,PORT_A, &value);
           	status = DIG_Out_Prt (NI_6503,PORT_B,0xFF);
              	
      	  }
 	   status = DIG_Prt_Config(NI_6503,PORT_B,HANDSHAKING,OUT_SIGNAL);
 	   
    }

return  value;
}

int set_Vmax_moteur_1(unsigned char v_max_1)
{  send_command_to_controller(READ_V_MAX_1,CARD_ADRESS,v_max_1);
   return 0;
}

int set_Vmax_moteur_2(unsigned char v_max_2)

{   send_command_to_controller(READ_V_MAX_2,CARD_ADRESS,v_max_2);
   	return 0;
}

long int read_pos_motor_1(void)
{    unsigned char pos1=0;
     unsigned char pos2=0;
     unsigned char pos3=0;
     unsigned char pos4=0;
     long int position_motor_1=0;

     pos1=   read_data_from_controller(LECT_POS_1_1,CARD_ADRESS);
     pos2=   read_data_from_controller(LECT_POS_1_2,CARD_ADRESS);
     pos3=   read_data_from_controller(LECT_POS_1_3,CARD_ADRESS);
     pos4=   read_data_from_controller(LECT_POS_1_4,CARD_ADRESS);
     position_motor_1=pos4+(pos3<<8)+(pos2<<16)+(pos1<<24);
     return position_motor_1;

}

long int read_pos_motor_2(void)
{     unsigned char pos1=0;
      unsigned char pos2=0;
      unsigned char pos3=0;
      unsigned char pos4=0;
      long int position_motor_2=0;

     pos1=   read_data_from_controller(LECT_POS_2_1,CARD_ADRESS);
     pos2=   read_data_from_controller(LECT_POS_2_2,CARD_ADRESS);
     pos3=   read_data_from_controller(LECT_POS_2_3,CARD_ADRESS);
     pos4=   read_data_from_controller(LECT_POS_2_4,CARD_ADRESS);
     position_motor_2=pos4+(pos3<<8)+(pos2<<16)+(pos1<<24);
     return position_motor_2;

}


int set_speed_feedback_1(unsigned char feedback_speed)
{	win_printf("Speed %0x",feedback_speed);
    send_command_to_controller(SPEED_FEEDBACK_VMAX_MOT_1,CARD_ADRESS,feedback_speed);
    return 0;
}

int set_speed_feedback_2(unsigned char feedback_speed)
{	win_printf("Speed %0x",feedback_speed);
    send_command_to_controller(SPEED_FEEDBACK_VMAX_MOT_2,CARD_ADRESS,feedback_speed);
    return 0;
}

int set_speed_feedback_v_lim_1(unsigned char vlim)
{
    send_command_to_controller(SPEED_FEEDBACK_VLIM_MOT_1,CARD_ADRESS,vlim);
    return 0;
}

int set_speed_feedback_v_lim_2(unsigned char vlim)
{
    send_command_to_controller(SPEED_FEEDBACK_VLIM_MOT_2,CARD_ADRESS,vlim);
    return 0;
}

int set_acc_speed_feedback(unsigned char acc)
{   send_command_to_controller(SPEED_FEEDBACK_ACC,CARD_ADRESS,acc);
   	return 0;
}

int set_acc_pos_feedback_1(unsigned char acc)
{   
	win_printf("acceleration %0x",acc);
	send_command_to_controller(POS_FEEDBACK_ACC_MOT_1,CARD_ADRESS,acc);
   	return 0;
}

int set_acc_pos_feedback_2(unsigned char acc)
{   
	win_printf("acceleration %0x",acc);
	send_command_to_controller(POS_FEEDBACK_ACC_MOT_2,CARD_ADRESS,acc);
   	return 0;
}

int set_consigne_pos_moteur_1(long cons_pos_1)
{    unsigned char pos1=0;
     unsigned char pos2=0;
     unsigned char pos3=0;
     unsigned char pos4=0;

   pos4=(unsigned char)(cons_pos_1&0xFF);
   pos3=(unsigned char)((cons_pos_1&0xFF00)>>8);
   pos2=(unsigned char)((cons_pos_1&0xFF0000)>>16);
   pos1=(unsigned char)((cons_pos_1&0xFF000000)>>24);
   send_command_to_controller(CONSIGNE_POS_1_1,CARD_ADRESS,pos1);
   send_command_to_controller(CONSIGNE_POS_1_2,CARD_ADRESS,pos2);
   send_command_to_controller(CONSIGNE_POS_1_3,CARD_ADRESS,pos3);
   send_command_to_controller(CONSIGNE_POS_1_4,CARD_ADRESS,pos4);

   return 0;
}

int set_consigne_pos_moteur_2(long cons_pos_2)
{     unsigned char pos1=0;
      unsigned char pos2=0;
      unsigned char pos3=0;
      unsigned char pos4=0;
	
   pos4=(unsigned char)(cons_pos_2&0xFF);
   pos3=(unsigned char)((cons_pos_2&0xFF00)>>8);
   pos2=(unsigned char)((cons_pos_2&0xFF0000)>>16);
   pos1=(unsigned char)((cons_pos_2&0xFF000000)>>24);
   send_command_to_controller(CONSIGNE_POS_2_1,CARD_ADRESS,pos1);
   send_command_to_controller(CONSIGNE_POS_2_2,CARD_ADRESS,pos2);
   send_command_to_controller(CONSIGNE_POS_2_3,CARD_ADRESS,pos3);
   send_command_to_controller(CONSIGNE_POS_2_4,CARD_ADRESS,pos4);

   return 0;
}

int set_lim_dec_feedback_pos_1(short int lim_pos_feedback)
{  unsigned char pos0=0;
   unsigned char pos1=0;

   pos0=(unsigned char)(lim_pos_feedback&0xFF);
   pos1=(unsigned char)((lim_pos_feedback&0xFF00)>>8);
   send_command_to_controller(POS_FEEDBACK_LIM_1_MOT_1,CARD_ADRESS,pos1);
   send_command_to_controller(POS_FEEDBACK_LIM_2_MOT_1,CARD_ADRESS,pos0);

   return 0;
}

int set_lim_dec_feedback_pos_2(short int lim_pos_feedback)
{  unsigned char pos0=0;
   unsigned char pos1=0;

   pos0=(unsigned char)(lim_pos_feedback&0xFF);
   pos1=(unsigned char)((lim_pos_feedback&0xFF00)>>8);
   send_command_to_controller(POS_FEEDBACK_LIM_1_MOT_2,CARD_ADRESS,pos1);
   send_command_to_controller(POS_FEEDBACK_LIM_2_MOT_2,CARD_ADRESS,pos0);

   return 0;
}



int  read_card_type(void)
{        unsigned char card_type[4];
		 unsigned char i;

		 for (i=0;i<3;i++)
		{
			card_type[i]=read_data_from_controller(CARD_TYPE,CARD_ADRESS+i);
			win_printf ("card type %d i=%0x ",card_type[i],i);
         	if (card_type[0]!=0x02) win_printf("T'as un soucis! Je ne suis pas qui tu crois");
         	
		}
		return 0;
}

void initialisation(void)
{        read_data_from_controller(INITIALISATION,CARD_ADRESS);
         return;
}

void reset_pos(unsigned char motor)
{
         switch(motor){
         case MOTOR_1:
         send_command_to_controller(RESET_POS,CARD_ADRESS,RESET_MOTOR_1);
         return;
         case MOTOR_2:
         send_command_to_controller(RESET_POS,CARD_ADRESS,RESET_MOTOR_2);
         return;
         case MOTOR_1_2:
         send_command_to_controller(RESET_POS,CARD_ADRESS,RESET_MOTOR_1_2);/*A verifier*/
         return;
         }
         win_printf("Tu t'es tromp� de moteur!");
         return;
}




/**********************TRADUCTION EN BOUTONS XV**************************/

int set_cons_max_speed_pos_1(void)
{    static int v_max=0;
     
	    win_scanf("Max Speed %d ?",&v_max);
        set_speed_feedback_1((unsigned char)v_max);
      return 0;
}
int set_cons_max_speed_pos_2(void)
{    static int v_max=0;
     
	    win_scanf("Max Speed %d ?",&v_max);
        set_speed_feedback_2((unsigned char)v_max);
      return 0;
}

	
int Read_pos_mot_1(void)
{	
	
		
	win_printf("Pos Mot 1 %ld",read_pos_motor_1());
	
	return 0;
}

int Read_pos_mot_2(void)
{	
	
	win_printf("Pos Mot 2 %ld",read_pos_motor_2());
	
	return 0;
}

int set_cons_pos_mot_1(void)
{	static  long cons1=0;
	
	
	win_scanf("Position %d ?",&cons1);
	set_consigne_pos_moteur_1(cons1);
	
	return 0;
}
int set_cons_pos_mot_2(void)
{	static  long  cons2=0;
	
	
	win_scanf("Position %d ?",&cons2);
	set_consigne_pos_moteur_2(cons2);
	
	return 0;
}


int set_vitesse_mot_1(void)
{	 
	static int vmax=0;
	
	win_scanf("Speed %d ?(0-255)",&vmax);
	set_Vmax_moteur_1((unsigned char)vmax);
	
	return 0;
}

int set_acc_speed(void)
{   static int acc=0;
	
	win_scanf("Acceleration Speed Feedback %d (0-127)?",&acc);
	set_acc_speed_feedback((unsigned char)acc);

    return 0;
}

int set_acc_pos_1(void)
{   static int acc=0;
	
	win_scanf("Acceleration position feedback %d ?",&acc);
	set_acc_pos_feedback_1((unsigned char)acc);

    return 0;
}

int set_acc_pos_2(void)
{   static int acc=0;
	
	win_scanf("Acceleration position feedback %d ?",&acc);
	set_acc_pos_feedback_2((unsigned char)acc);

    return 0;
}

int set_vitesse_mot_2(void)
{	static int vmax=0;
	
	
	win_scanf("Speed %d ?",&vmax);
	set_Vmax_moteur_2((unsigned char)vmax);
	
	return 0;
}

int set_cons_pos_lim_1(void)
{   static int pos_lim=0;

    
	win_scanf("Pos lim %d ?",&pos_lim);
    set_lim_dec_feedback_pos_1((short int)pos_lim);
	
	return 0;
}

int set_cons_pos_lim_2(void)
{   static int pos_lim=0;

    
	win_scanf("Pos lim %d ?",&pos_lim);
    set_lim_dec_feedback_pos_2((short int)pos_lim);
	
	return 0;
}

int set_cons_max_speed_lim_1(void)
{   static int speed_lim=0;

    
	win_scanf("Speed lim %d ?",&speed_lim);
    set_speed_feedback_v_lim_1((unsigned char)speed_lim);
	
	return 0;
}

int set_cons_max_speed_lim_2(void)
{   static int speed_lim=0;

    
	win_scanf("Speed lim %d ?",&speed_lim);
    set_speed_feedback_v_lim_2((unsigned char)speed_lim);
	
	return 0;
}

int reset_pos_mot_1(void)
{         
             reset_pos(MOTOR_1);
          return 0;
}

int reset_pos_mot_2(void)
{         
             reset_pos(MOTOR_2);
          return 0;
}

int do_initialisation(void)
{   
             initialisation();
    return 0;
}

MENU DAQMOTORmenu[32] = 
{  
	{ "read_card_type",           read_card_type, NULL ,  0, NULL  },  
   
   { "set_cons_max_speed_pos_1",           set_cons_max_speed_pos_1, NULL ,  0, NULL  },  
   { "&Read_pos_mot_1",           Read_pos_mot_1, NULL ,  0, NULL  },    
   { "Read_pos_mot_2",     Read_pos_mot_2, NULL,  0, NULL  },
   { "set_cons_pos_mot_1",     set_cons_pos_mot_1, NULL,  0, NULL  },
   { "set_cons_pos_mot_2",     set_cons_pos_mot_2, NULL,  0, NULL  },
   { "set_vitesse_mot_1",     set_vitesse_mot_1, NULL,  0, NULL  },
   { "set_acc_speed",     set_acc_speed, NULL,  0, NULL  },
   { "set_acc_pos_1",     set_acc_pos_1, NULL,  0, NULL  },
   { "set_acc_pos_2",     set_acc_pos_2, NULL,  0, NULL  },
   { "set_vitesse_mot_2",     set_vitesse_mot_2, NULL,  0, NULL  },
   { "set_cons_pos_lim_1",     set_cons_pos_lim_1, NULL,  0, NULL  },
   { "set_cons_pos_lim_2",     set_cons_pos_lim_2, NULL,  0, NULL  },
   { "set_cons_max_speed_lim_1",     set_cons_max_speed_lim_1, NULL,  0, NULL  },
   { "set_cons_max_speed_lim_2",     set_cons_max_speed_lim_2, NULL,  0, NULL  },
   { "reset_pos_mot_1",     reset_pos_mot_1, NULL,  0, NULL  },
   {" reset_pos_mot_2",   reset_pos_mot_2, NULL, 0,  NULL},
   {" do_initialisation",   do_initialisation, NULL, 0,  NULL},
   { NULL,               NULL,             NULL,       0, NULL  }
};


int motor_main(int argc, char **argv)
{
    detect_NI_boards();
    add_plot_edit_menu_item("Moteurs menu", NULL, DAQMOTORmenu, 0, NULL);
    
	broadcast_dialog_message(MSG_DRAW,0);
	


	return 0;    
}









