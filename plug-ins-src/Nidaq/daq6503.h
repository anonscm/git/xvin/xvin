#define DAQ_6503_H

#ifndef PCI_VENDOR_ID_CBOARDS
#define PCI_VENDOR_ID_CBOARDS 0x1093
#endif

#ifndef PCI_DEVICE_ID_CBOARDS_DAQ_6503
#define PCI_DEVICE_ID_CBOARDS_DAQ_6503 0x17d0
#endif

/********************************NiDaq 6503 registers*************************/

#define CONFIG_REG_AD 0x03
#define PORT_A_AD     0x00
#define PORT_B_AD     0x01
#define PORT_C_AD     0x02

/***************************CONFIGURATION INPUT-OUTPUT*********************/


/**********************mode 0 I/O*****************************************/
#define IO_OUT_A_OUT_C1_OUT_B_OUT_C2 0x80
#define IO_OUT_A_OUT_C1_OUT_B_IN_C2  0x81
#define IO_OUT_A_OUT_C1_IN_B_OUT_C2  0x82
#define IO_OUT_A_OUT_C1_IN_B_IN_C2   0x83
#define IO_OUT_A_IN_C1_OUT_B_OUT_C2  0x88
#define IO_OUT_A_IN_C1_OUT_B_IN_C2   0x89
#define IO_OUT_A_IN_C1_IN_B_OUT_C2   0x8A
#define IO_OUT_A_IN_C1_IN_B_IN_C2    0x8B
#define IO_IN_A_OUT_C1_OUT_B_OUT_C2  0x90
#define IO_IN_A_OUT_C1_OUT_B_IN_C2   0x91
#define IO_IN_A_OUT_C1_IN_B_OUT_C2   0x92
#define IO_IN_A_OUT_C1_IN_B_IN_C2    0x93
#define IO_IN_A_IN_C1_OUT_B_OUT_C2   0x98
#define IO_IN_A_IN_C1_OUT_B_IN_C2    0x99
#define IO_IN_A_IN_C1_IN_B_OUT_C2    0x9A
#define IO_IN_A_IN_C1_IN_B_IN_C2     0x9B


/*****************write and read mode for the motor controller of C. Herrmann************/
#define WRITE_MODE_MOTOR	0x84 /*mode 0 output pour A et mode 1 output pour B*/
#define READ_MODE_MOTOR		0x94 /*mode 0 input pour A et mode 1 output pour B*/

/******************************mode 1 STROBED***********************/


#define MODE_1_A_IN_C_HIGH_IN_B_IN_C_LOW_IN                 0xBF
#define MODE_1_A_IN_C_HIGH_IN_B_IN_C_LOW_OUT                0xBE
#define MODE_1_A_IN_C_HIGH_IN_B_OUT_C_LOW_IN                0xBD
#define MODE_1_A_IN_C_HIGH_IN_B_OUT_C_LOW_OUT               0xBC
#define MODE_1_A_IN_C_HIGH_OUT_B_IN_C_LOW_IN                0xB7
#define MODE_1_A_IN_C_HIGH_OUT_B_IN_C_LOW_OUT               0xB6
#define MODE_1_A_IN_C_HIGH_OUT_B_OUT_C_LOW_IN               0xB5
#define MODE_1_A_IN_C_HIGH_OUT_B_OUT_C_LOW_OUT              0xB4
#define MODE_1_A_OUT_C_HIGH_IN_B_IN_C_LOW_IN                0xAF
#define MODE_1_A_OUT_C_HIGH_IN_B_IN_C_LOW_OUT               0xAE
#define MODE_1_A_OUT_C_HIGH_IN_B_OUT_C_LOW_IN               0xAD
#define MODE_1_A_OUT_C_HIGH_IN_B_OUT_C_LOW_OUT              0xAC
#define MODE_1_A_OUT_C_HIGH_OUT_B_IN_C_LOW_IN               0xA7
#define MODE_1_A_OUT_C_HIGH_OUT_B_IN_C_LOW_OUT              0xA6
#define MODE_1_A_OUT_C_HIGH_OUT_B_OUT_C_LOW_IN              0xA5
#define MODE_1_A_OUT_C_HIGH_OUT_B_OUT_C_LOW_OUT             0xA4


/************************Handshaking registers********************/

#define INTRA_IN                     0x08
#define IBFA_IN                      0x20
#define RESET_IBFA_IN				 0x0A
#define SET_OBFA_ON                  0x0F
#define SET_OBFB_ON                  0x03
#define INTEA_IN                     0x10
#define SET_INTEA_IN                 0x09
#define INTRB_IN                     0x01
#define IBFB_IN                      0x02
#define SET_INTEB_IN                 0x05
#define INTEB_IN                 	 0x04
#define INTRA_OUT                    0x08
#define OBFA_OUT                     0x80
#define INTEA_OUT                    0x40
#define SET_INTEA_OUT                0x0D
#define INTRB_OUT                    0x01
#define OBFB_OUT                     0x02
#define INTEB_OUT                    0x04
#define SET_INTEB_OUT                0x05




int 			Port_Config(unsigned char config);
int 			configure_PCI_MITE(void);
int 			init_module(void) ;
void			write_daq_6503_control_registers(unsigned long reg, unsigned char val);
unsigned char	read_daq_6503_interface_registers(unsigned long reg);
void			write_daq_6503_interface_registers(unsigned long reg, unsigned char val);
unsigned short	set_daq_6503_control_registers_selector(void);
unsigned short	set_daq_6503_interface_registers_selector(void);
int 			set_IO_mode(unsigned char type);
int 			enable_INTRA_output_status(void);
int 			enable_INTRB_output_status(void);
int				enable_INTRA_input_status(void);
int 			enable_INTRB_input_status(void);
void			write_daq_6503_PCI_MITE(unsigned int reg, unsigned long val);
unsigned char	read_daq_6503_control_registers(unsigned long reg);



