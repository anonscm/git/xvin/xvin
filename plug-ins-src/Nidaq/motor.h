#ifndef motor_H
#define motor_H

#define PORT_A 0
#define PORT_B 1
#define PORT_C 2
#define NO_HANDSHAKING 0
#define HANDSHAKING 1
#define IN_SIGNAL 0
#define OUT_SIGNAL 1
#define PCI6503                     256

/****************************INTERFACE CONTROLER REGISTERS*********************/

#define MOTOR_1                        0
#define MOTOR_2                        1
#define MOTOR_1_2                      2  /********************A verifier******************/
#define READ_V_MAX_1 0x00
#define READ_V_MAX_2 0x08

#define LECT_POS_1_1 0x10
#define LECT_POS_1_2 0x18
#define LECT_POS_1_3 0x20
#define LECT_POS_1_4 0x28

#define LECT_POS_2_1 0x30
#define LECT_POS_2_2 0x38
#define LECT_POS_2_3 0x40
#define LECT_POS_2_4 0x48

#define CONSIGNE_POS_1_1 0x50
#define CONSIGNE_POS_1_2 0x58
#define CONSIGNE_POS_1_3 0x60
#define CONSIGNE_POS_1_4 0x68

#define CONSIGNE_POS_2_1 0x70
#define CONSIGNE_POS_2_2 0x78
#define CONSIGNE_POS_2_3 0x80
#define CONSIGNE_POS_2_4 0x88

#define TEST_COMMAND_PID      0xA0
#define COMMAND_PID      0x90
#define PARAMETER_PID    0x98
#define SPEED_FEEDBACK_VMAX_MOT_1  		0xC0
#define SPEED_FEEDBACK_VMAX_MOT_2  		0xC8
#define POS_FEEDBACK_LIM_1_MOT_1     	0xD0
#define POS_FEEDBACK_LIM_2_MOT_1     	0xD8
#define POS_FEEDBACK_LIM_1_MOT_2     	0xE0
#define POS_FEEDBACK_LIM_2_MOT_2     	0xE8
#define ACCELERATION_1   0x01
#define ACCELERATION_2   0x02
#define P_1              0x10
#define I_1              0x11
#define D_1              0x12
#define R_1              0x13
#define P_2              0x20
#define I_2              0x21
#define D_2              0x22
#define R_2              0x23
#define RESET_MOT_1    0xB1
#define RESET_MOT_2    0xB2
#define RESET_MOT_1_2  0xB3

#define INITIALISATION       0xF8

#define CARD_TYPE            0xF0
#define CARD_ADRESS			 0X02

PXV_FUNC(int, send_command_to_controller, (unsigned char command,unsigned char card_adress,unsigned char data));
PXV_FUNC(unsigned char, read_data_from_controller, (unsigned char command,unsigned char card_adress));
PXV_FUNC(int, Write,(unsigned char port, unsigned char val));
PXV_FUNC(unsigned char, Read,(unsigned char port));
PXV_FUNC(int, set_Vmax_moteur_1,(unsigned char v_max_1));
PXV_FUNC(int, set_Vmax_moteur_2,(unsigned char v_max_2));
PXV_FUNC(long int, read_pos_motor_1,(void));
PXV_FUNC(long int, read_pos_motor_2,(void));
PXV_FUNC(int, set_speed_feedback_1,(unsigned char feedback_speed));
PXV_FUNC(int, set_speed_feedback_2,(unsigned char feedback_speed));
PXV_FUNC(int, set_acc_pos_feedback_1,(unsigned char acc));
PXV_FUNC(int, set_acc_pos_feedback_2,(unsigned char acc));
PXV_FUNC(int, set_consigne_pos_moteur_1,(long cons_pos_1));
PXV_FUNC(int, set_consigne_pos_moteur_2,(long cons_pos_2));
PXV_FUNC(int, set_lim_dec_feedback_pos_1,(short int lim_pos_feedback));
PXV_FUNC(int, set_lim_dec_feedback_pos_2,(short int lim_pos_feedback));
PXV_FUNC(int,  read_card_type,(void));
PXV_FUNC(int, new_P_motor_1,(unsigned char new_P_1));
PXV_FUNC(int, new_P_motor_2,(unsigned char new_P_2));
PXV_FUNC(int, new_I_motor_1,(unsigned char new_I_1));
PXV_FUNC(int, new_I_motor_2,(unsigned char new_I_2));
PXV_FUNC(int, new_D_motor_1,(unsigned char new_D_1));
PXV_FUNC(int, new_D_motor_2,(unsigned char new_D_2));
PXV_FUNC(int, new_R_motor_1,(unsigned char new_R_1));
PXV_FUNC(int, new_R_motor_2,(unsigned char new_R_2));
PXV_FUNC(int, new_res_motor_1,(void));
PXV_FUNC(int, new_res_motor_2,(void));
PXV_FUNC(int, new_set_P_1,(void));
PXV_FUNC(int, new_set_P_2,(void));
PXV_FUNC(int, new_set_I_1,(void));
PXV_FUNC(int, new_set_I_2,(void));
PXV_FUNC(int, new_set_D_1,(void));
PXV_FUNC(int, new_set_D_2,(void));
PXV_FUNC(int, new_set_ratio_motor_1,(void));
PXV_FUNC(int, new_set_ratio_motor_2,(void));
PXV_FUNC(int, new_reset_motor_1,(void));
PXV_FUNC(int, new_reset_motor_2,(void));
PXV_FUNC(int, set_cons_max_speed_pos_1,(void));
PXV_FUNC(int, set_cons_max_speed_pos_2,(void));
PXV_FUNC(int, Read_pos_mot_1,(void));
PXV_FUNC(int, Read_pos_mot_2,(void));
PXV_FUNC(int, set_cons_pos_mot_1,(void));
PXV_FUNC(int, set_cons_pos_mot_2,(void));
PXV_FUNC(int, set_vitesse_mot_1,(void));
PXV_FUNC(int, set_acc_pos_1,(void));
PXV_FUNC(int, set_acc_pos_2,(void));
PXV_FUNC(int, set_vitesse_mot_2,(void));
PXV_FUNC(int, set_cons_pos_lim_1,(void));
PXV_FUNC(int, set_cons_pos_lim_2,(void));
PXV_FUNC(int, set_cons_max_speed_lim_1,(void));
PXV_FUNC(int, set_cons_max_speed_lim_2,(void));
PXV_FUNC(int, do_detect_card_type,(void));
PXV_FUNC(int, read_status,(void));
PXV_FUNC(void, reset_pos,(unsigned char motor));
PXV_FUNC(int, init_motors,(void));

#endif
