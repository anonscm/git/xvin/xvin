unit ioconv;

interface

uses
  Nidaq, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TForm2 = class(TForm)
  end;

var
  Form2: TForm2;

function convread (canal : byte):smallint;  export;
procedure convwrite (canal : byte;data : word);  export;


implementation
function convread (canal : byte):smallint;
var
voie : byte;
ptr1 : longint;
mesure : smallint;
begin
DIG_Prt_Config (1,0,0,0);
DIG_Prt_Config (1,1,0,1);
DIG_Prt_Config (1,2,0,1);
{DIG_Out_Prt (1,2,$CF);}
voie := (canal-1) * 2;
DIG_Out_Prt (1,1,voie+57);
DIG_Out_Line (1,2,6,1);
DIG_Out_Prt (1,1,voie+0);
DIG_Out_Prt (1,1,voie+8);
DIG_In_Prt (1,0,@ptr1);
mesure := ptr1 * 256;
DIG_Out_Prt (1,1,voie+9);
DIG_In_Prt (1,0,@ptr1);
convread := ptr1 + mesure;
DIG_Out_Prt (1,1,voie+57);
DIG_Out_Line (1,2,6,0);
DIG_Prt_Config (1,1,1,1);
end;


procedure convwrite (canal : byte;data : word);
type
    pt1 = array [0..3] of byte;
var
voie : byte;
p1 : ^pt1;
begin
DIG_Prt_Config (1,0,0,1);
DIG_Prt_Config (1,1,0,1);
DIG_Prt_Config (1,2,0,1);
{DIG_Out_Prt (1,2,$C0);
}
p1 := @data;
voie := canal * 64;
DIG_Out_Prt (1,0,p1^[0]);
DIG_Out_Prt (1,1,63+voie);
DIG_Out_Line (1,2,6,1);
DIG_Out_Prt (1,1,9+voie);
DIG_Out_Prt (1,1,12+voie);
DIG_Out_Prt (1,0,p1^[1]);
DIG_Out_Prt (1,1,5+voie);
DIG_Out_Line (1,2,6,0);
DIG_Prt_Config (1,0,0,0);
{DIG_Prt_Config (1,1,1,1);
 }
end;

{$R *.DFM}

end.
