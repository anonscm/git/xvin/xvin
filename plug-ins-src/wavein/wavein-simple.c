/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _WAVEIN_C_
#define _WAVEIN_C_

# include "allegro.h"
# include "winalleg.h"
# include "fftl32n.h"
# include "fillib.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin 

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "wavein.h"


# define B_SIZE 4096
# define TB_SIZE 65536
int sampleRate = 96000;//44100;
short int waveIn[B_SIZE];   // 'short int' is a 16-bit type; I request 16-bit samples below
short int waveIn2[B_SIZE];   // 'short int' is a 16-bit type; I request 16-bit samples below
                             // for 8-bit capture, you'd use 'unsigned char' or 'BYTE' 8-bit types

int nChannels = 1;
float spe_avg = 1.0;

short int *rollingWave = NULL;
int rollingWaveNx = 0;
int rollingWavePos = 0;
int rollingWavePos_1 = 0;
int scopeSize = 0;
int do_apo = 1;
int sub_sample = 1;

d_s *to_be_filled = NULL;
pltreg *pr_WAVE = NULL;
O_p *op_scope = NULL;
O_p *op_fft_scope = NULL;


short int waveBig[TB_SIZE];
int i_wavebig = 0;  // length in bytes
int buf_0_call = 0;
int buf_1_call = 0;
int buf_read = 0;
HWAVEIN      hWaveIn;
WAVEHDR      WaveInHdr, WaveInHdr2;
MMRESULT result;

WAVEFORMATEX pFormat;
WAVEINCAPS waveInCaps;

unsigned long t_0, dt0, dt1;

void CALLBACK waveInProcRoll(HWAVEIN hwi,       
			       UINT uMsg,         
			       DWORD dwInstance,  
			       DWORD dwParam1,    
			       DWORD dwParam2)     
{
int cpos, i, j, np;
short int *si;
if (uMsg == WIM_OPEN) return;
 else if (uMsg == WIM_DATA && rollingWave != NULL)
   {
cpos = rollingWavePos%rollingWaveNx;
np = ((PWAVEHDR)dwParam1)->dwBufferLength>>1;
si = (short int*)((PWAVEHDR)dwParam1)->lpData;
for (i = 0, j = cpos; i < np; i += sub_sample, j++)
  rollingWave[j] = si[i]; 
rollingWavePos += np/sub_sample;;
buf_read = ((PWAVEHDR)dwParam1)->dwBytesRecorded>>1;
if (((PWAVEHDR)dwParam1)->lpData == (LPSTR)waveIn) 
  {
dt0 =  my_uclock() - t_0;
buf_0_call++;
}
if (((PWAVEHDR)dwParam1)->lpData == (LPSTR)waveIn2) 
  {
dt1 =  my_uclock() - t_0;
buf_1_call++;
}
MMRESULT i4 = waveInAddBuffer(hWaveIn,(PWAVEHDR)dwParam1,sizeof(WAVEHDR));
      
switch (i4)
  {
 case MMSYSERR_INVALHANDLE:
MessageBox(NULL,"Specified device handle is invalid. ",NULL,MB_OK);
break;
 case MMSYSERR_NODRIVER:
MessageBox(NULL,"No device driver is present. ",NULL,MB_OK);
break;
 case MMSYSERR_NOMEM:
MessageBox(NULL,"Unable to allocate or lock memory. ",NULL,MB_OK);
break;
 case WAVERR_UNPREPARED:
MessageBox(NULL,"The buffer pointed to by the pwh parameter hasn't been prepared. ",NULL,MB_OK);
break;
}
return;
}
 else  if (uMsg == WIM_CLOSE)
   {
}
}


int set_apo(void)
{
   if(updating_menu_state != 0) return D_O_K;     
  do_apo = 1;
  return 0;
}
int rm_apo(void)
{
   if(updating_menu_state != 0) return D_O_K;     
  do_apo = 0;
  return 0;
}

int set_avg(void)
{
  if(updating_menu_state != 0) return D_O_K;     
  spe_avg = 0.05;
  win_scanf("Rebew factor %12f",&spe_avg);
  return 0;
}
int rm_avg(void)
{
   if(updating_menu_state != 0) return D_O_K;     
  spe_avg = 1;
  return 0;
}

int dec_scope_size(void)
{
  int i;
  if(updating_menu_state != 0)   
    {
      add_keyboard_short_cut(0, KEY_F8, 0, dec_scope_size);
      return D_O_K;  
    }
  i = scopeSize;
  i = i/2;
  if (i < 256) i = 256;
  scopeSize = i;
  return 0;
}

int inc_scope_size(void)
{
  int i;
  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut(0, KEY_F5 , 0, inc_scope_size);
      return D_O_K;  
    }
  i = scopeSize;
  i = i*2;
  if (i > rollingWaveNx/2) i = rollingWaveNx/2;
  scopeSize = i;
  return 0;
}
int fill_scope_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  d_s *dsx;
  int last,i, j, size, rWP, min = 0, max = 0, scale;

  if (rollingWave == NULL) return 0;
  dsx = find_source_specific_ds_in_op(op_scope,"Scope of wavein ");
  if (dsx == NULL) return D_O_K;	
  if (rollingWavePos_1 >= rollingWavePos) return 0;
  rWP = rollingWavePos; // volatile variable
  last = rWP - scopeSize;
  last = (last < 0) ? 0 : last;
  size = rWP - last;
  for (i = 0; i < size; i++)
    {
      j = last + i;
      dsx->xd[i] = sub_sample*i;
      j %= rollingWaveNx;
      dsx->yd[i] = (float)rollingWave[j];
      min = (min < rollingWave[j]) ? min : rollingWave[j];
      max = (max > rollingWave[j]) ? max : rollingWave[j];
    }
  dsx->nx = dsx->ny = size;
  op_scope->need_to_refresh = 1;
  max = (max > -min) ? max : -min;
  if (max > 20000) scale =  50000;
  else if (max > 10000) scale =  20000;
  else if (max > 5000) scale =  10000;
  else if (max > 2000) scale =  5000;
  else if (max > 1000) scale =  2000;
  else if (max > 500) scale =  1000;
  else scale = 500;
  op_scope->y_lo = -scale;
  op_scope->y_hi = scale;
  op->ax = last * op->dx;
  set_plot_y_fixed_range(op);
 
  rollingWavePos_1 = rWP;
  i = size/8;
  j = ((int)dsx->xd[i])/i;
  op_scope->x_lo = (j-1)*i;
  op_scope->x_hi = (j+6)*i;
  set_plot_title(op_scope, "Scope at %d over %d",last,size);
  refresh_plot(pr_WAVE, UNCHANGED);
  return 0;
}

int fill_fft_scope_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  d_s *dsx, *dsf;
  int last,i, j, size, rWP;

  if (rollingWave == NULL) return 0;
  dsx = find_source_specific_ds_in_op(op_scope,"Scope of wavein ");
  dsf = find_source_specific_ds_in_op(op_fft_scope,"FFT Scope of wavein ");
  if (dsx == NULL) return D_O_K;	
  if (rollingWavePos_1 >= rollingWavePos) return 0;
  rWP = rollingWavePos; // volatile variable
  last = rWP - scopeSize;
  last = (last < 0) ? 0 : last;
  size = rWP - last;
  for (i = 0; i < size; i++)
    {
      j = last + i;
      dsx->xd[i] = sub_sample*j;
      j %= rollingWaveNx;
      dsf->xd[i] = dsx->yd[i] = (float)rollingWave[j];
    }
  dsx->nx = dsx->ny = size;
  dsf->nx = dsf->ny = size;

  if (fft_init(size)) return 0; //win_printf("Cannot init fft\n%d not a power of 2!",size);


  if (do_apo) fftwindow(size, dsf->xd);
  realtr1(size, dsf->xd);
  fft(size, dsf->xd, 1);
  realtr2(size, dsf->xd, 1);
  spec_real (size, dsf->xd, dsf->xd);
  dsf->nx = dsf->ny = 1 + (size/2);

  for (i = 0; i < size; i++)
    {
      dsf->yd[i] = (1-spe_avg) * dsf->yd[i] + spe_avg*dsf->xd[i];
      dsf->xd[i] = ((float)i)/(size * sub_sample); 
      dsf->xd[i] *= sampleRate;
    }


  op_fft_scope->need_to_refresh = 1;
  rollingWavePos_1 = rWP;
  set_plot_title(op_fft_scope, "Scope at %d over %d",last,size);
  return refresh_plot(pr_WAVE, UNCHANGED);
}



int do_wavein_scope(void)
{
  register int i, j;
  O_p *op = NULL, *opn = NULL;
  int nf, ndev; 
  static int idev = 0, ifreq = 0;
  static int started = 0;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;
  char message[2048];
  
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  if (started) return D_O_K;
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  nf = 32768;
  
  if ((ndev = waveInGetNumDevs()) == 0)
    return win_printf_OK("No sound card installed !");
  snprintf(message,sizeof(message),"Length of scope displayed in time (s) %%10f\n"
	   "Sampling rate %%8R11.025, %%r22.05, %%r44.1, %%r48, %%r96, %%r192 kHz\n");
  snprintf(message+strlen(message),sizeof(message)-strlen(message),"Choose your input device in the list\n");
  for (j = 0; j < ndev; j++)
    {
      if ((i = waveInGetDevCaps (j, &waveInCaps, sizeof(WAVEINCAPS))) != MMSYSERR_NOERROR)
	return win_printf_OK("No sound card installed !");
      if (j == 0) snprintf(message+strlen(message),sizeof(message)-strlen(message),
			   "%%R id %d, Device %s\n",j, waveInCaps.szPname);
      else snprintf(message+strlen(message),sizeof(message)-strlen(message),
		    "%%r id %d, Device %s\n",j, waveInCaps.szPname);
    }
  snprintf(message+strlen(message),sizeof(message)-strlen(message),
	   "%%r id %d, default Device \n",j);
  i = win_scanf(message,&factor,&ifreq,&idev);
  if (i == CANCEL) return 0;
  
  if (ifreq == 0) sampleRate = 11025;
  else if (ifreq == 1) sampleRate = 22050;
  else if (ifreq == 2) sampleRate = 44100;
  else if (ifreq == 3) sampleRate = 48000;
  else if (ifreq == 4) sampleRate = 96000;
  else if (ifreq == 5) sampleRate = 192000;
  else return win_printf_OK("Wrong sampling frequency !");
  
  nf = sampleRate * factor;
  for (i = 2; i < nf; i*=2);
  nf = i;
  rollingWave = (short int*)calloc(2*nf,sizeof(short int));
  if (rollingWave == NULL) return win_printf_OK("cannot create rolling buffer !");
  rollingWaveNx = 2*nf;
  rollingWavePos = 0;
  scopeSize = nf;
  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  ds->nx = 0;
  pr_WAVE = pr;
  
  /* now we must do some house keeping */
  set_ds_source(ds,"Scope of wavein %d pts. %f seconds",ds->nx,factor);
  opn->op_idle_action = fill_scope_rolling_buffer_idle_action;
  set_plot_title(opn, "Scope over %f",factor);
  set_plot_x_title(opn, "Time");
  set_plot_y_title(opn, "Signal");
  create_attach_select_x_un_to_op(opn, IS_SECOND, 0 ,(float)1/sampleRate, 0, 0, "s");
  opn->width = 1.7;
  /* redisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  
  op_scope = opn;
  
  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  ds->nx = 0;
  opn->width = 1.7;
  op_fft_scope = opn;
  
  
  /* now we must do some house keeping */
  set_ds_source(ds,"FFT Scope of wavein %d pts. %f seconds",ds->nx,factor);
  opn->op_idle_action = fill_fft_scope_rolling_buffer_idle_action;
  set_plot_title(opn, "Scope over %f",factor);
  set_plot_x_title(opn, "Frequency");
  set_plot_y_title(opn, "Amplitude");
  set_plot_y_log(opn);
  
  pFormat.wFormatTag=WAVE_FORMAT_PCM;     // simple, uncompressed format
  pFormat.nChannels=1;                    //  1=mono, 2=stereo
  pFormat.nSamplesPerSec=sampleRate;      // 44100
  pFormat.nAvgBytesPerSec=sampleRate*2;   // = nSamplesPerSec * n.Channels * wBitsPerSample/8
  pFormat.nBlockAlign=2;                  // = n.Channels * wBitsPerSample/8
  pFormat.wBitsPerSample=16;              //  16 for high quality, 8 for telephone-grade
  pFormat.cbSize=0;
  
  if (idev < ndev) 
    {
      result = waveInOpen(&hWaveIn,idev ,&pFormat,(DWORD)waveInProcRoll,0, 
			  CALLBACK_FUNCTION); //WAVE_MAPPER
    }
  else
    {
      result = waveInOpen(&hWaveIn,WAVE_MAPPER ,&pFormat,(DWORD)waveInProcRoll,0, 
			  CALLBACK_FUNCTION); //WAVE_MAPPER
    }
  if (result)
    {
      char fault[256];
      waveInGetErrorText(result, fault, 256);
      return win_printf_OK("Could not initiate Wave in: %s",fault);
    }
  
  // Set up and prepare header for input
  WaveInHdr.lpData = (LPSTR)waveIn;
  WaveInHdr.dwBufferLength = B_SIZE*2;
  WaveInHdr.dwBytesRecorded=0;
  WaveInHdr.dwUser = 0L;
  WaveInHdr.dwFlags = 0L;
  WaveInHdr.dwLoops = 0L;
  waveInPrepareHeader(hWaveIn, &WaveInHdr, sizeof(WAVEHDR));
  
  WaveInHdr2.lpData = (LPSTR)waveIn2;
  WaveInHdr2.dwBufferLength = B_SIZE*2;
  WaveInHdr2.dwBytesRecorded=0;
  WaveInHdr2.dwUser = 0L;
  WaveInHdr2.dwFlags = 0L;
  WaveInHdr2.dwLoops = 0L;  
  waveInPrepareHeader(hWaveIn, &WaveInHdr2, sizeof(WAVEHDR));
  // Insert a wave input buffer
  result = waveInAddBuffer(hWaveIn, &WaveInHdr, sizeof(WAVEHDR));
  if (result)  win_printf("failed to read block from device 1");
  result = waveInAddBuffer(hWaveIn, &WaveInHdr2, sizeof(WAVEHDR));
  if (result)  win_printf("failed to read block from device 1");
  // Commence sampling input
  
  result = waveInStart(hWaveIn);
  if (result)  win_printf("failed to start recording");
  t_0 = my_uclock();
  started = 1;
  
 return D_O_K;
}

MENU *wavein_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Wave in Scope", do_wavein_scope,NULL,0,NULL);
  add_item_to_menu(mn,"dec scope", dec_scope_size,NULL,0,NULL);
  add_item_to_menu(mn,"inc scope", inc_scope_size,NULL,0,NULL);
  add_item_to_menu(mn,"Windowing", set_apo,NULL,0,NULL);
  add_item_to_menu(mn,"No windowing", rm_apo,NULL,0,NULL);
  add_item_to_menu(mn,"Average Spectre", set_avg,NULL,0,NULL);
  add_item_to_menu(mn,"No averaging", rm_avg,NULL,0,NULL);
  return mn;
}

int	wavein_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "wavein", NULL, wavein_plot_menu(), 0, NULL);
  return D_O_K;
}

int	wavein_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "wavein", NULL, NULL);
  return D_O_K;
}
#endif
