#ifndef _WAVEIN_H_
#define _WAVEIN_H_

PXV_FUNC(int, do_wavein_rescale_plot, (void));
PXV_FUNC(MENU*, wavein_plot_menu, (void));
PXV_FUNC(int, do_wavein_rescale_data_set, (void));
PXV_FUNC(int, wavein_main, (int argc, char **argv));
#endif

