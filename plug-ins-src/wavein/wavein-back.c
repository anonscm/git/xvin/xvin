/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _WAVEIN_C_
#define _WAVEIN_C_

# include "allegro.h"
# include "winalleg.h"
# include "fftl32n.h"
# include "fillib.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin 

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "wavein.h"


# define B_SIZE 4096
# define TB_SIZE 65536
//const int NUMPTS = 441000;   // 10 seconds
int sampleRate = 96000;//44100;
short int waveIn[B_SIZE];   // 'short int' is a 16-bit type; I request 16-bit samples below
short int waveIn2[B_SIZE];   // 'short int' is a 16-bit type; I request 16-bit samples below
                             // for 8-bit capture, you'd use 'unsigned char' or 'BYTE' 8-bit types

int nChannels = 1;
float spe_avg = 1.0;

short int *rollingWave = NULL;
int rollingWaveNx = 0;
int rollingWavePos = 0;
int rollingWavePos_1 = 0;
int scopeSize = 0;
int do_apo = 1;
int sub_sample = 1;

d_s *to_be_filled = NULL;
pltreg *pr_WAVE = NULL;
O_p *op_scope = NULL;
O_p *op_fft_scope = NULL;


short int waveBig[TB_SIZE];
int i_wavebig = 0;  // length in bytes
int buf_0_call = 0;
int buf_1_call = 0;
int buf_read = 0;
HWAVEIN      hWaveIn;
WAVEHDR      WaveInHdr, WaveInHdr2;
MMRESULT result;

WAVEFORMATEX pFormat;
WAVEINCAPS waveInCaps;

unsigned long t_0, dt0, dt1;



/*
HANDLE wave_hThread = NULL;
DWORD wave_dwThreadId;


// function executed in the tracking thread
int create_wave_thread(O_p *op)
{
    
    wave_hThread = CreateThread( 
            NULL,              // default security attributes
            0,                 // use default stack size  
            waveThreadProc,// thread function 
            (void*)op,       // argument to thread function 
            0,                 // use default creation flags 
            &wave_dwThreadId);      // returns the thread identifier 

    if (track_hThread == NULL) 	win_printf_OK("No thread created");
    SetThreadPriority(wave_hThread,  THREAD_PRIORITY_NORMAL);
    return 0;
}



// function executed in the tracking thread
VOID CALLBACK dummyAPCProc(LPVOID lpArgToCompletionRoutine,
  DWORD dwTimerLowValue,  DWORD dwTimerHighValue)
{
  return;
}


// function executed in the tracking thread
DWORD WINAPI TrackingThreadProc( LPVOID lpParam ) 
{
    DIALOG *d;
    O_i *oi;
    static long idum = 87;
    int nf, im_n = 0, i, j, l, norm;
    unsigned long dt = 0, t0 = 0, t1 = 0, dtmax = 0;
    long long t;
    DIALOG *starting_one = NULL;
    float tmp;

    if (op_wave == NULL)	return 1;
    op = (op*)lpParam;

    while (go_track)
      {

	// Wait for the timer.
	t1 = my_uclock();
	// Set a timer to wait for 10 seconds.

	dt = t1 - t0;	    
	if (dt > dtmax) dtmax = dt;
	t0 = t1;
	if (go_track == TRACK_FREEZE) continue;
	oi->im.c_f++;
	if (oi->im.c_f >= nf) oi->im.c_f = 0;
	im_n++;      
	oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;

      }
    source_running = 0;
    return 0;
}

*/

void CALLBACK waveInProcRoll(HWAVEIN hwi,       
			     UINT uMsg,         
			     DWORD dwInstance,  
			     DWORD dwParam1,    
			     DWORD dwParam2)     
{
  int cpos, i, j, np;
  short int *si;
  if (uMsg == WIM_OPEN) return;
  else if (uMsg == WIM_DATA && rollingWave != NULL)
    {
      cpos = rollingWavePos%rollingWaveNx;
      //cpos <<= 1;
      //CopyMemory(((char *)rollingWave) + cpos, ((PWAVEHDR)dwParam1)->lpData,((PWAVEHDR)dwParam1)->dwBufferLength );
      //rollingWavePos += ((PWAVEHDR)dwParam1)->dwBytesRecorded>>1;
      np = ((PWAVEHDR)dwParam1)->dwBufferLength>>1;
      si = (short int*)((PWAVEHDR)dwParam1)->lpData;
      for (i = 0, j = cpos; i < np; i += sub_sample, j++)
	rollingWave[j] = si[i]; 
      rollingWavePos += np/sub_sample;;
      buf_read = ((PWAVEHDR)dwParam1)->dwBytesRecorded>>1;
      //cpos >>= 1;
      if (((PWAVEHDR)dwParam1)->lpData == (LPSTR)waveIn) 
	{
	  dt0 =  my_uclock() - t_0;
	  buf_0_call++;
	  //for (i = 0; i < ((PWAVEHDR)dwParam1)->dwBufferLength>>1; i++)    rollingWave[cpos+i] = buf_0_call; 
	}
      if (((PWAVEHDR)dwParam1)->lpData == (LPSTR)waveIn2) 
	{
	  dt1 =  my_uclock() - t_0;
	  buf_1_call++;
	  //for (i = 0; i < ((PWAVEHDR)dwParam1)->dwBufferLength>>1; i++)    rollingWave[cpos+i] = -buf_1_call; 
	}
      MMRESULT i4 = waveInAddBuffer(hWaveIn,(PWAVEHDR)dwParam1,sizeof(WAVEHDR));
      
      switch (i4)
	{
	case MMSYSERR_INVALHANDLE:
	  MessageBox(NULL,"Specified device handle is invalid. ",NULL,MB_OK);
	  break;
	case MMSYSERR_NODRIVER:
	  MessageBox(NULL,"No device driver is present. ",NULL,MB_OK);
	  break;
	case MMSYSERR_NOMEM:
	  MessageBox(NULL,"Unable to allocate or lock memory. ",NULL,MB_OK);
	  break;
	case WAVERR_UNPREPARED:
	  MessageBox(NULL,"The buffer pointed to by the pwh parameter hasn't been prepared. ",NULL,MB_OK);
	  break;
	  
	}
      
      
      
      return;
    }
  
  else 
    
    if (uMsg == WIM_CLOSE)
      
      {
      
      }
}



void CALLBACK waveInProc(HWAVEIN hwi,       
			 UINT uMsg,         
			 DWORD dwInstance,  
			 DWORD dwParam1,    
			 DWORD dwParam2)     
{
  if (uMsg == WIM_OPEN) return;
  else if (uMsg == WIM_DATA && i_wavebig < 2*TB_SIZE)
    {
      CopyMemory(((char *)waveBig) + i_wavebig, ((PWAVEHDR)dwParam1)->lpData,((PWAVEHDR)dwParam1)->dwBufferLength );
      i_wavebig +=((PWAVEHDR)dwParam1)->dwBytesRecorded ;
      if (((PWAVEHDR)dwParam1)->lpData == (LPSTR)waveIn) 
	{
	  dt0 =  my_uclock() - t_0;
	  buf_0_call++;
	}
      if (((PWAVEHDR)dwParam1)->lpData == (LPSTR)waveIn2) 
	{
	  dt1 =  my_uclock() - t_0;
	  buf_1_call++;
	}
      MMRESULT i4 = waveInAddBuffer(hWaveIn,(PWAVEHDR)dwParam1,sizeof(WAVEHDR));
      
      switch (i4)
	{
	case MMSYSERR_INVALHANDLE:
	  MessageBox(NULL,"Specified device handle is invalid. ",NULL,MB_OK);
	  break;
	case MMSYSERR_NODRIVER:
	  MessageBox(NULL,"No device driver is present. ",NULL,MB_OK);
	  break;
	case MMSYSERR_NOMEM:
	  MessageBox(NULL,"Unable to allocate or lock memory. ",NULL,MB_OK);
	  break;
	case WAVERR_UNPREPARED:
	  MessageBox(NULL,"The buffer pointed to by the pwh parameter hasn't been prepared. ",NULL,MB_OK);
	  break;
	  
	}
      
      
      
      return;
    }
  
  else 
    
    if (uMsg == WIM_CLOSE)
      
      {
      
      }
}



void CALLBACK waveInProcDs(HWAVEIN hwi,       
			 UINT uMsg,         
			 DWORD dwInstance,  
			 DWORD dwParam1,    
			 DWORD dwParam2)     
{
  int i, n;
  short int *data;

  if (uMsg == WIM_OPEN) return;
  else if (uMsg == WIM_DATA && to_be_filled != NULL)
    {
      data = (short int*)(((PWAVEHDR)dwParam1)->lpData);
      for (i = 0, n = ((PWAVEHDR)dwParam1)->dwBufferLength/2; i < n && to_be_filled->nx < to_be_filled->mx; i++)
	{
	  to_be_filled->xd[to_be_filled->nx] = to_be_filled->nx;
	  to_be_filled->yd[to_be_filled->nx] = (float)data[i];
	  to_be_filled->nx++;
	}
      if (((PWAVEHDR)dwParam1)->lpData == (LPSTR)waveIn) 
	{
	  dt0 =  my_uclock() - t_0;
	  buf_0_call++;
	}
      if (((PWAVEHDR)dwParam1)->lpData == (LPSTR)waveIn2) 
	{
	  dt1 =  my_uclock() - t_0;
	  buf_1_call++;
	}
      MMRESULT i4 = waveInAddBuffer(hWaveIn,(PWAVEHDR)dwParam1,sizeof(WAVEHDR));
      
      switch (i4)
	{
	case MMSYSERR_INVALHANDLE:
	  MessageBox(NULL,"Specified device handle is invalid. ",NULL,MB_OK);
	  break;
	case MMSYSERR_NODRIVER:
	  MessageBox(NULL,"No device driver is present. ",NULL,MB_OK);
	  break;
	case MMSYSERR_NOMEM:
	  MessageBox(NULL,"Unable to allocate or lock memory. ",NULL,MB_OK);
	  break;
	case WAVERR_UNPREPARED:
	  MessageBox(NULL,"The buffer pointed to by the pwh parameter hasn't been prepared. ",NULL,MB_OK);
	  break;
	  
	}
      
      
      
      return;
    }
  
  else 
    
    if (uMsg == WIM_CLOSE)
      
      {
      
      }
}


int do_wavein_hello(void)
{
  register int i;

  if(updating_menu_state != 0)	return D_O_K;

  /*
  i = win_printf("Hello from wavein");
  if (i == CANCEL) win_printf_OK("you have press CANCEL");
  else win_printf_OK("you have press OK");
  */

 // Specify recording parameters


  if ((i = waveInGetNumDevs()) == 0)
    return win_printf_OK("No sound card installed !");
  win_printf("%d device(s) installed",i);

  if ((i = waveInGetDevCaps (0, &waveInCaps, sizeof(WAVEINCAPS))) != MMSYSERR_NOERROR)
    return win_printf_OK("No sound card installed !");

  switch (waveInCaps.dwFormats)
    {
    case WAVE_FORMAT_1M08 : win_printf("11.025 kHz, mono, 8-bit with %d chan",waveInCaps.wChannels);break;
    case WAVE_FORMAT_1M16 : win_printf("11.025 kHz, mono, 16-bit with %d chan",waveInCaps.wChannels);break;
    case WAVE_FORMAT_1S08 : win_printf("11.025 kHz, stereo, 8-bit with %d chan",waveInCaps.wChannels);break;
    case WAVE_FORMAT_1S16 : win_printf("11.025 kHz, stereo, 16-bit with %d chan",waveInCaps.wChannels);break;
    case WAVE_FORMAT_2M08 : win_printf("22.05 kHz, mono, 8-bit with %d chan",waveInCaps.wChannels);break;
    case WAVE_FORMAT_2M16 : win_printf("22.05 kHz, mono, 16-bit with %d chan",waveInCaps.wChannels);break;
    case WAVE_FORMAT_2S08 : win_printf("22.05 kHz, stereo, 8-bit with %d chan",waveInCaps.wChannels);break;
    case WAVE_FORMAT_2S16 : win_printf("22.05 kHz, stereo, 16-bit with %d chan",waveInCaps.wChannels);break;
    case WAVE_FORMAT_4M08 : win_printf("44.1 kHz, mono, 8-bit with %d chan",waveInCaps.wChannels);break;
    case WAVE_FORMAT_4M16 : win_printf("44.1 kHz, mono, 16-bit with %d chan",waveInCaps.wChannels);break;
    case WAVE_FORMAT_4S08 : win_printf("44.1 kHz, stereo, 8-bit with %d chan",waveInCaps.wChannels);break;
    case WAVE_FORMAT_4S16 : win_printf("44.1 kHz, stereo, 16-bit with %d chan",waveInCaps.wChannels);break;
    default : win_printf("unknown mode %x with %d chan",waveInCaps.dwFormats,waveInCaps.wChannels);
    }


// waveInCaps.dwFormats contains information 
// about available wave formats.




  pFormat.wFormatTag=WAVE_FORMAT_PCM;     // simple, uncompressed format
  pFormat.nChannels=1;                    //  1=mono, 2=stereo
  pFormat.nSamplesPerSec=sampleRate;      // 44100
  pFormat.nAvgBytesPerSec=sampleRate*2;   // = nSamplesPerSec * n.Channels * wBitsPerSample/8
  pFormat.nBlockAlign=2;                  // = n.Channels * wBitsPerSample/8
  pFormat.wBitsPerSample=16;              //  16 for high quality, 8 for telephone-grade
  pFormat.cbSize=0;

  result = waveInOpen(&hWaveIn, WAVE_MAPPER,&pFormat,(DWORD)waveInProc,0, 
                          CALLBACK_FUNCTION);
  if (result)
    {
      char fault[256];
      waveInGetErrorText(result, fault, 256);
      return win_printf_OK("Could not initiate Wave in: %s",fault);
    }
  
  win_printf_OK("initiate Wave in OK");

  // Set up and prepare header for input
  WaveInHdr.lpData = (LPSTR)waveIn;
  WaveInHdr.dwBufferLength = B_SIZE*2;
  WaveInHdr.dwBytesRecorded=0;
  WaveInHdr.dwUser = 0L;
  WaveInHdr.dwFlags = 0L;
  WaveInHdr.dwLoops = 0L;
  waveInPrepareHeader(hWaveIn, &WaveInHdr, sizeof(WAVEHDR));

  WaveInHdr2.lpData = (LPSTR)waveIn2;
  WaveInHdr2.dwBufferLength = B_SIZE*2;
  WaveInHdr2.dwBytesRecorded=0;
  WaveInHdr2.dwUser = 0L;
  WaveInHdr2.dwFlags = 0L;
  WaveInHdr2.dwLoops = 0L;  
  waveInPrepareHeader(hWaveIn, &WaveInHdr2, sizeof(WAVEHDR));
  // Insert a wave input buffer
  result = waveInAddBuffer(hWaveIn, &WaveInHdr, sizeof(WAVEHDR));
  if (result)  win_printf("failed to read block from device 1");
  result = waveInAddBuffer(hWaveIn, &WaveInHdr2, sizeof(WAVEHDR));
  if (result)  win_printf("failed to read block from device 1");
  // Commence sampling input

  result = waveInStart(hWaveIn);
  if (result)  win_printf("failed to start recording");
  t_0 = my_uclock();

  Sleep(1000);
  win_printf("buff 0 %d, %g ms buf 1 %d %g ms",buf_0_call,
	     (double)((1000*dt0))/get_my_uclocks_per_sec(),
	     buf_1_call,(double)((1000*dt1))/get_my_uclocks_per_sec());
  Sleep(1000);
  win_printf("buff 0 %d, %g ms buf 1 %d %g ms",buf_0_call,
	     (double)((1000*dt0))/get_my_uclocks_per_sec(),
	     buf_1_call,(double)((1000*dt1))/get_my_uclocks_per_sec());
  waveInUnprepareHeader(hWaveIn, &WaveInHdr, sizeof(WAVEHDR));

  waveInClose(hWaveIn);
  return 0;
}

int do_wavein_rescale_data_set(void)
{
  register int i, j;
  O_p *op = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == CANCEL)	return OFF;

  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_wavein_rescale_plot(void)
{
  register int i, j;
  O_p *op = NULL, *opn = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  nf = 32768;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  ds->nx = 0;
  to_be_filled = ds;



  if ((i = waveInGetNumDevs()) == 0)
    return win_printf_OK("No sound card installed !");
  win_printf("%d device(s) installed",i);

  if ((i = waveInGetDevCaps (0, &waveInCaps, sizeof(WAVEINCAPS))) != MMSYSERR_NOERROR)
    return win_printf_OK("No sound card installed !");
  win_printf("unknown mode %x with %d chan",waveInCaps.dwFormats,waveInCaps.wChannels);

  pFormat.wFormatTag=WAVE_FORMAT_PCM;     // simple, uncompressed format
  pFormat.nChannels=1;                    //  1=mono, 2=stereo
  pFormat.nSamplesPerSec=sampleRate;      // 44100
  pFormat.nAvgBytesPerSec=sampleRate*2;   // = nSamplesPerSec * n.Channels * wBitsPerSample/8
  pFormat.nBlockAlign=2;                  // = n.Channels * wBitsPerSample/8
  pFormat.wBitsPerSample=16;              //  16 for high quality, 8 for telephone-grade
  pFormat.cbSize=0;


  result = waveInOpen(&hWaveIn, WAVE_MAPPER,&pFormat,(DWORD)waveInProcDs,0, 
                          CALLBACK_FUNCTION);
  if (result)
    {
      char fault[256];
      waveInGetErrorText(result, fault, 256);
      return win_printf_OK("Could not initiate Wave in: %s",fault);
    }
  
  win_printf_OK("initiate Wave in OK");

  // Set up and prepare header for input
  WaveInHdr.lpData = (LPSTR)waveIn;
  WaveInHdr.dwBufferLength = B_SIZE*2;
  WaveInHdr.dwBytesRecorded=0;
  WaveInHdr.dwUser = 0L;
  WaveInHdr.dwFlags = 0L;
  WaveInHdr.dwLoops = 0L;
  waveInPrepareHeader(hWaveIn, &WaveInHdr, sizeof(WAVEHDR));

  WaveInHdr2.lpData = (LPSTR)waveIn2;
  WaveInHdr2.dwBufferLength = B_SIZE*2;
  WaveInHdr2.dwBytesRecorded=0;
  WaveInHdr2.dwUser = 0L;
  WaveInHdr2.dwFlags = 0L;
  WaveInHdr2.dwLoops = 0L;  
  waveInPrepareHeader(hWaveIn, &WaveInHdr2, sizeof(WAVEHDR));
  // Insert a wave input buffer
  result = waveInAddBuffer(hWaveIn, &WaveInHdr, sizeof(WAVEHDR));
  if (result)  win_printf("failed to read block from device 1");
  result = waveInAddBuffer(hWaveIn, &WaveInHdr2, sizeof(WAVEHDR));
  if (result)  win_printf("failed to read block from device 1");
  // Commence sampling input

  result = waveInStart(hWaveIn);
  if (result)  win_printf("failed to start recording");
  t_0 = my_uclock();

  Sleep(1000);
  win_printf("buff 0 %d, %g ms buf 1 %d %g ms",buf_0_call,
	     (double)((1000*dt0))/get_my_uclocks_per_sec(),
	     buf_1_call,(double)((1000*dt1))/get_my_uclocks_per_sec());
  Sleep(1000);
  win_printf("buff 0 %d, %g ms buf 1 %d %g ms",buf_0_call,
	     (double)((1000*dt0))/get_my_uclocks_per_sec(),
	     buf_1_call,(double)((1000*dt1))/get_my_uclocks_per_sec());
  waveInUnprepareHeader(hWaveIn, &WaveInHdr, sizeof(WAVEHDR));

  waveInClose(hWaveIn);

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  set_plot_title(opn, "Multiply by %f",factor);
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

      //int do_apo = 1;
      //int sub_sample = 1;


int set_apo(void)
{
   if(updating_menu_state != 0) return D_O_K;     
  do_apo = 1;
  return 0;
}
int rm_apo(void)
{
   if(updating_menu_state != 0) return D_O_K;     
  do_apo = 0;
  return 0;
}

int set_sub_sample(void)
{
   if(updating_menu_state != 0) return D_O_K;     
  sub_sample = 4;
  return 0;
}
int rm_sub_sample(void)
{
   if(updating_menu_state != 0) return D_O_K;     
  sub_sample = 1;
  return 0;
}

int set_avg(void)
{
   if(updating_menu_state != 0) return D_O_K;     
  spe_avg = 0.05;
  return 0;
}
int rm_avg(void)
{
   if(updating_menu_state != 0) return D_O_K;     
  spe_avg = 1;
  return 0;
}

int dec_scope_size(void)
{
  int i;
  if(updating_menu_state != 0)   
    {
      //remove_short_cut_from_dialog(0, KEY_DOWN);
      //add_keyboard_short_cut(0, KEY_DOWN, 0, dec_scope_size);
      //remove_short_cut_from_dialog(0, KEY_Z);
      add_keyboard_short_cut(0, KEY_Z, 0, dec_scope_size);
      return D_O_K;  
    }
  i = scopeSize;
  i = i/2;
  if (i < 256) i = 256;
  scopeSize = i;
  return 0;
}

int inc_scope_size(void)
{
  int i;
  if(updating_menu_state != 0)	
    {
      //remove_short_cut_from_dialog(0, KEY_UP);
      //add_keyboard_short_cut(0, KEY_UP, 0, inc_scope_size);
      //remove_short_cut_from_dialog(0, KEY_A);
      add_keyboard_short_cut(0, KEY_A, 0, inc_scope_size);
      return D_O_K;  
    }
  i = scopeSize;
  i = i*2;
  if (i > rollingWavePos/2) i = rollingWavePos/2;
  scopeSize = i;
  return 0;
}
int check_timing(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  win_printf("buff 0 %d, %g ms buf 1 %d %g ms\n read %d",buf_0_call,
	     (double)((1000*dt0))/get_my_uclocks_per_sec(),
	     buf_1_call,(double)((1000*dt1))/get_my_uclocks_per_sec(),buf_read);
  return D_O_K;
}
int fill_scope_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  d_s *dsx;
  int last,i, j, size, rWP, min = 0, max = 0, scale;

  if (rollingWave == NULL) return 0;
  dsx = find_source_specific_ds_in_op(op_scope,"Scope of wavein ");
  if (dsx == NULL) return D_O_K;	
  if (rollingWavePos_1 >= rollingWavePos) return 0;
  rWP = rollingWavePos; // volatile variable
  last = rWP - scopeSize;
  last = (last < 0) ? 0 : last;
  size = rWP - last;
  for (i = 0; i < size; i++)
    {
      j = last + i;
      dsx->xd[i] = sub_sample*j;
      j %= rollingWaveNx;
      dsx->yd[i] = (float)rollingWave[j];
      min = (min < rollingWave[j]) ? min : rollingWave[j];
      max = (max > rollingWave[j]) ? max : rollingWave[j];
    }
  dsx->nx = dsx->ny = size;
  op_scope->need_to_refresh = 1;
  max = (max > -min) ? max : -min;
  if (max > 20000) scale =  50000;
  else if (max > 10000) scale =  20000;
  else if (max > 5000) scale =  10000;
  else if (max > 2000) scale =  5000;
  else if (max > 1000) scale =  2000;
  else if (max > 500) scale =  1000;
  else scale = 500;
  op_scope->y_lo = -scale;
  op_scope->y_hi = scale;
  set_plot_y_fixed_range(op);
 
  rollingWavePos_1 = rWP;
  i = size/8;
  j = ((int)dsx->xd[i])/i;
  op_scope->x_lo = (j-1)*i;
  op_scope->x_hi = (j+6)*i;
  //set_plot_x_fixed_range(op);
  set_plot_title(op_scope, "Scope at %d over %d",last,size);
  refresh_plot(pr_WAVE, UNCHANGED);
  add_keyboard_short_cut(0, KEY_Z, 0, dec_scope_size);
  add_keyboard_short_cut(0, KEY_A, 0, inc_scope_size);
  return 0;
}

int fill_fft_scope_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  d_s *dsx, *dsf;
  int last,i, j, size, rWP;

  if (rollingWave == NULL) return 0;
  dsx = find_source_specific_ds_in_op(op_scope,"Scope of wavein ");
  dsf = find_source_specific_ds_in_op(op_fft_scope,"FFT Scope of wavein ");
  if (dsx == NULL) return D_O_K;	
  if (rollingWavePos_1 >= rollingWavePos) return 0;
  rWP = rollingWavePos; // volatile variable
  last = rWP - scopeSize;
  last = (last < 0) ? 0 : last;
  size = rWP - last;
  for (i = 0; i < size; i++)
    {
      j = last + i;
      dsx->xd[i] = sub_sample*j;
      j %= rollingWaveNx;
      dsf->xd[i] = dsx->yd[i] = (float)rollingWave[j];
    }
  dsx->nx = dsx->ny = size;
  dsf->nx = dsf->ny = size;

  if (fft_init(size)) return 0; //win_printf("Cannot init fft\n%d not a power of 2!",size);


  if (do_apo) fftwindow(size, dsf->xd);
  realtr1(size, dsf->xd);
  fft(size, dsf->xd, 1);
  realtr2(size, dsf->xd, 1);
  spec_real (size, dsf->xd, dsf->xd);
  dsf->nx = dsf->ny = 1 + (size/2);

  for (i = 0; i < size; i++)
    {
      dsf->yd[i] = (1-spe_avg) * dsf->yd[i] + spe_avg*dsf->xd[i];
      dsf->xd[i] = ((float)i)/(size * sub_sample); 
      dsf->xd[i] *= sampleRate;
    }


  op_fft_scope->need_to_refresh = 1;
  rollingWavePos_1 = rWP;
  set_plot_title(op_fft_scope, "Scope at %d over %d",last,size);
  return refresh_plot(pr_WAVE, UNCHANGED);
}


int do_wavein_scope(void)
{
  register int i;
  O_p *op = NULL, *opn = NULL;
  int nf, ndev; 
  static int idev = 0;
  static int started = 0;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;
  char message[1024];


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  if (started) return D_O_K;
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  nf = 32768;

  i = win_scanf("Length of scope in time (s) %10f\nSampling rate %8d\n",&factor,&sampleRate);
  if (i == CANCEL) return 0;

  nf = sampleRate * factor;
  for (i = 2; i < nf; i*=2);
  nf = i;
  //nf = 4096*(nf/4096) + ((nf%4096) ? 4096 : 0);
  rollingWave = (short int*)calloc(2*nf,sizeof(short int));
  if (rollingWave == NULL) return win_printf_OK("cannot create rolling buffer !");
  rollingWaveNx = 2*nf;
  rollingWavePos = 0;
  scopeSize = nf;
  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  ds->nx = 0;
  pr_WAVE = pr;

  /* now we must do some house keeping */
  set_ds_source(ds,"Scope of wavein %d pts. %f seconds",ds->nx,factor);
  opn->op_idle_action = fill_scope_rolling_buffer_idle_action;
  set_plot_title(opn, "Scope over %f",factor);
  set_plot_x_title(opn, "Time");
  set_plot_y_title(opn, "Signal");
  create_attach_select_x_un_to_op(opn, IS_SECOND, 0 ,(float)1/sampleRate, 0, 0, "s");
  opn->width = 1.7;
  /* redisplay the entire plot */
  refresh_plot(pr, UNCHANGED);

  op_scope = opn;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  ds->nx = 0;
  opn->width = 1.7;
  op_fft_scope = opn;


  /* now we must do some house keeping */
  set_ds_source(ds,"FFT Scope of wavein %d pts. %f seconds",ds->nx,factor);
  opn->op_idle_action = fill_fft_scope_rolling_buffer_idle_action;
  set_plot_title(opn, "Scope over %f",factor);
  set_plot_x_title(opn, "Frequency");
  set_plot_y_title(opn, "Amplitude");
  set_plot_y_log(opn);
  //create_attach_select_x_un_to_op(opn, 0, 0, (float)44100/nf, 0, 0, "no_name");




  if ((i = waveInGetNumDevs()) == 0)
    return win_printf_OK("No sound card installed !");
  //win_printf("%d device(s) installed",i);

  if ((i = waveInGetDevCaps (0, &waveInCaps, sizeof(WAVEINCAPS))) != MMSYSERR_NOERROR)
    return win_printf_OK("No sound card installed !");
  //win_printf("unknown mode %x with %d chan",waveInCaps.dwFormats,waveInCaps.wChannels);

  pFormat.wFormatTag=WAVE_FORMAT_PCM;     // simple, uncompressed format
  pFormat.nChannels=1;                    //  1=mono, 2=stereo
  pFormat.nSamplesPerSec=sampleRate;      // 44100
  pFormat.nAvgBytesPerSec=sampleRate*2;   // = nSamplesPerSec * n.Channels * wBitsPerSample/8
  pFormat.nBlockAlign=2;                  // = n.Channels * wBitsPerSample/8
  pFormat.wBitsPerSample=16;              //  16 for high quality, 8 for telephone-grade
  pFormat.cbSize=0;


  result = waveInOpen(&hWaveIn, WAVE_MAPPER,&pFormat,(DWORD)waveInProcRoll,0, 
                          CALLBACK_FUNCTION);
  if (result)
    {
      char fault[256];
      waveInGetErrorText(result, fault, 256);
      return win_printf_OK("Could not initiate Wave in: %s",fault);
    }
  
  //win_printf_OK("initiate Wave in OK");

  // Set up and prepare header for input
  WaveInHdr.lpData = (LPSTR)waveIn;
  WaveInHdr.dwBufferLength = B_SIZE*2;
  WaveInHdr.dwBytesRecorded=0;
  WaveInHdr.dwUser = 0L;
  WaveInHdr.dwFlags = 0L;
  WaveInHdr.dwLoops = 0L;
  waveInPrepareHeader(hWaveIn, &WaveInHdr, sizeof(WAVEHDR));

  WaveInHdr2.lpData = (LPSTR)waveIn2;
  WaveInHdr2.dwBufferLength = B_SIZE*2;
  WaveInHdr2.dwBytesRecorded=0;
  WaveInHdr2.dwUser = 0L;
  WaveInHdr2.dwFlags = 0L;
  WaveInHdr2.dwLoops = 0L;  
  waveInPrepareHeader(hWaveIn, &WaveInHdr2, sizeof(WAVEHDR));
  // Insert a wave input buffer
  result = waveInAddBuffer(hWaveIn, &WaveInHdr, sizeof(WAVEHDR));
  if (result)  win_printf("failed to read block from device 1");
  result = waveInAddBuffer(hWaveIn, &WaveInHdr2, sizeof(WAVEHDR));
  if (result)  win_printf("failed to read block from device 1");
  // Commence sampling input

  result = waveInStart(hWaveIn);
  if (result)  win_printf("failed to start recording");
  t_0 = my_uclock();
  started = 1;

  /*
  Sleep(1000);
  win_printf("buff 0 %d, %g ms buf 1 %d %g ms",buf_0_call,
	     (double)((1000*dt0))/get_my_uclocks_per_sec(),
	     buf_1_call,(double)((1000*dt1))/get_my_uclocks_per_sec());
  Sleep(1000);
  win_printf("buff 0 %d, %g ms buf 1 %d %g ms",buf_0_call,
	     (double)((1000*dt0))/get_my_uclocks_per_sec(),
	     buf_1_call,(double)((1000*dt1))/get_my_uclocks_per_sec());
  waveInUnprepareHeader(hWaveIn, &WaveInHdr, sizeof(WAVEHDR));

  waveInClose(hWaveIn);
  */


  return D_O_K;
}



MENU *wavein_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Hello example", do_wavein_hello,NULL,0,NULL);
  add_item_to_menu(mn,"data set rescale in Y", do_wavein_rescale_data_set,NULL,0,NULL);
  add_item_to_menu(mn,"plot rescale in Y", do_wavein_rescale_plot,NULL,0,NULL);
  add_item_to_menu(mn,"Wave in Scope", do_wavein_scope,NULL,0,NULL);
  add_item_to_menu(mn,"Timing", check_timing,NULL,0,NULL);
  add_item_to_menu(mn,"dec scope", dec_scope_size,NULL,0,NULL);
  add_item_to_menu(mn,"inc scope", inc_scope_size,NULL,0,NULL);
  add_item_to_menu(mn,"sub sample 4", set_sub_sample,NULL,0,NULL);
  add_item_to_menu(mn,"sub sample 1", rm_sub_sample,NULL,0,NULL);
  add_item_to_menu(mn,"Windowing", set_apo,NULL,0,NULL);
  add_item_to_menu(mn,"No windowing", rm_apo,NULL,0,NULL);
  add_item_to_menu(mn,"Average Spectre", set_avg,NULL,0,NULL);
  add_item_to_menu(mn,"No averaging", rm_avg,NULL,0,NULL);



  return mn;
}

int	wavein_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "wavein", NULL, wavein_plot_menu(), 0, NULL);
  return D_O_K;
}

int	wavein_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "wavein", NULL, NULL);
  return D_O_K;
}
#endif

# ifdef NOTTOCOMPIL

24-bit audio recording?
Tech-Archive recommends: Repair Windows Errors & Optimize Windows Performance

    From: Jan Wagner <no_spam@xxxxxxxxxx>
    Date: Wed, 01 Feb 2006 16:46:36 +0200

Hi,


anyone have or know of example source code for 24-bit 2..n channel audio recording?

I couldn't find anything actually useful on MSDN either (even the description of WAVEFORMATEXTENSIBLE is very badly written). I can't use DirectSound because the project is in VC6.0 and the DirectX SDK is not available for VC6.

Also I'm a bit puzzled as to why 24-bit recording does not work properly. WinXP Pro, Audigy 2 NX external 24-bit and on-board cheapo 16-bit. With my code 16-bit recording works fine on both, but with 24-bit only on the cheapo 16-bit but not the Audigy.

In GoldWave, both cards record fine with 24-bit (apparently using WDM/WinMM and not directx).

This is the waveformatextensible used:


/* Init Extensible wave-format structure (>16-bit) */
sWaveFormatPcmEx.Format.wFormatTag = WAVE_FORMAT_EXTENSIBLE;
sWaveFormatPcmEx.Format.nChannels = NUM_IN_OUT_CHANNELS;
// container size:
sWaveFormatPcmEx.Format.wBitsPerSample = sizeof(_SAMPLE) * 8;
sWaveFormatPcmEx.Format.nSamplesPerSec = SOUNDCRD_SAMPLE_RATE;
sWaveFormatPcmEx.Format.nBlockAlign = NUM_IN_OUT_CHANNELS *
    sizeof(_SAMPLE);
sWaveFormatPcmEx.Format.nAvgBytesPerSec = sWaveFormatEx.nBlockAlign
    * sWaveFormatEx.nSamplesPerSec;
sWaveFormatPcmEx.Format.cbSize = 22;
sWaveFormatPcmEx.dwChannelMask = SPEAKER_FRONT_LEFT |
    SPEAKER_FRONT_RIGHT;
sWaveFormatPcmEx.Samples.wValidBitsPerSample = BITS_PER_SAMPLE;
sWaveFormatPcmEx.SubFormat = KSDATAFORMAT_SUBTYPE_PCM;


where


#define SOUNDCRD_SAMPLE_RATE 48000
#define NUM_IN_OUT_CHANNELS  2
#define BITS_PER_SAMPLE      24
#define NUM_SOUND_BUFFERS_IN 24
typedef signed long          _SAMPLE;
_SAMPLE* psSoundcardBuffer[NUM_SOUND_BUFFERS_IN];


Nothing wrong so far, or..? I also tried unsigned long for _SAMPLE, no difference for the Audigy. Only when BITS_PER_SAMPLE is set to 16 then it will start working on the Audigy too, still with typedef unsigned long _SAMPLE.

lpwavehdrs set up like


/* Set struct entries */
m_WaveInHeader[iBufNum].lpData = (LPSTR) &
   psSoundcardBuffer[iBufNum][0];
m_WaveInHeader[iBufNum].dwBufferLength = iBufferSizeIn *
   BYTES_PER_SAMPLE;
m_WaveInHeader[iBufNum].dwFlags = 0;


/* Prepare wave-header */
waveInPrepareHeader(m_WaveIn, &m_WaveInHeader[iBufNum],
   sizeof(WAVEHDR));


and wave device is opened like


result = waveInOpen(&m_WaveIn, iCurInDev,
   (WAVEFORMATEX*)&sWaveFormatPcmEx,
   (DWORD) m_WaveInEvent, NULL,
   CALLBACK_EVENT);


The actual code is zipped at
http://drm.sourceforge.net/forums/index.php?act=Attach&type=post&id=747
and in .\windows\Source\Sound.cpp, Sound.h


Any ideas? Or any example code suggestions?


Or can there somehow be a difference in WAVEFORMATEXTENSIBLE in WinMM vs DirectX!?

 - Jan
# endif
