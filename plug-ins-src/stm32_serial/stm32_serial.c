/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
# ifndef _STM32_SERIAL_C_
# define _STM32_SERIAL_C_

# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"




/* If you include other plug-ins header do it here*/

/* But not below this define */
# define BUILDING_PLUGINS_DLL

# undef _PXV_DLL
# define _PXV_DLL   __declspec(dllexport)
# include "stm32_serial.h"

unsigned long t_rs = 0, dt_rs;
#define READ_ONLY 0
#define WRITE_ONLY 1
#define WRITE_READ 2

UINT timeout = 200;  // ms
#ifdef XV_WIN32 
typedef HANDLE com_port_t;
#else
typedef int com_port_t;
#endif

char last_answer_2[128];
char str_com[32];
com_port_t hCom = NULL;
COMMTIMEOUTS lpTo;
COMMCONFIG lpCC;


int CloseSerialPort(HANDLE lhCom);

HANDLE init_serial_port(char *port_str, int baudrate, int n_bits, int parity, int stops, int RTSCTS)
{
  int k, pb = 0;
  HANDLE hCom_port;
  DWORD dwErrors;
  COMSTAT comStat;


  //win_printf("entering init ");
  t_rs = my_uclock();
  dt_rs = get_my_uclocks_per_sec() / 1000;

  strcpy(str_com, port_str);
  //sprintf( str_com, "\\\\.\\COM%d\0", port_number);
  hCom_port = CreateFile(str_com, GENERIC_READ | GENERIC_WRITE, 0, NULL,
			 OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
  PurgeComm(hCom_port, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
  if (GetCommState(hCom_port, &lpCC.dcb) == 0)
    {
      win_printf("GetCommState FAILED\n port %s baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
		 , port_str, baudrate, n_bits, parity, stops, RTSCTS);
      return NULL;
    }
  /* Initialisation des parametres par defaut */
  /*http://msdn.microsoft.com/library/default.asp?url=/library/en-us/devio/base/dcb_str.asp*/
  //win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
  lpCC.dcb.BaudRate = baudrate;//CBR_57600;//CBR_115200;
  //lpCC.dcb.BaudRate = CBR_115200;
  //win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
  lpCC.dcb.ByteSize = n_bits; //8;
  lpCC.dcb.StopBits = (stops == 1) ? ONESTOPBIT : TWOSTOPBITS;//stops;//ONESTOPBIT;
  lpCC.dcb.Parity = parity;//NOPARITY;
  lpCC.dcb.fOutX = FALSE;
  lpCC.dcb.fInX = FALSE;
  if (RTSCTS == 0)
    {
      lpCC.dcb.fDtrControl = DTR_CONTROL_DISABLE;//DTR_CONTROL_HANDSHAKE or DTR_CONTROL_DISABLE;
      lpCC.dcb.fRtsControl = RTS_CONTROL_DISABLE;//RTS_CONTROL_HANDSHAKE
    }
  else
    {
      lpCC.dcb.fDtrControl = DTR_CONTROL_HANDSHAKE;// or DTR_CONTROL_DISABLE;
      lpCC.dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
      lpCC.dcb.fOutxCtsFlow = TRUE;
    }
  if (SetCommState(hCom_port, &lpCC.dcb) == 0)
    {
      win_printf("SetCommState FAILED\n port %s baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
		 , port_str, baudrate, n_bits, parity, stops, RTSCTS);
      return NULL;
    }
  if (GetCommTimeouts(hCom_port, &lpTo) == 0)
    {
      win_printf("GetCommTimeouts FAILED\n port %s baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
		 , port_str, baudrate, n_bits, parity, stops, RTSCTS);
      return NULL;
    }
  lpTo.ReadIntervalTimeout = timeout;   // 100 ms max entre characters
  lpTo.ReadTotalTimeoutMultiplier = 1;
  lpTo.ReadTotalTimeoutConstant = 1;
  lpTo.WriteTotalTimeoutMultiplier = 1;
  lpTo.WriteTotalTimeoutConstant = 1;

  if (SetCommTimeouts(hCom_port, &lpTo) == 0)
    {
      win_printf("SetCommTimeouts FAILED\n port %s baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
		 , port_str, baudrate, n_bits, parity, stops, RTSCTS);
      return NULL;
    }
  //    we check that the rs232 port is ready for writing
  if (SetupComm(hCom_port, 2048, 2048) == 0)
    {
      win_printf("Init Serial port %s FAILED", port_str);
      CloseSerialPort(hCom_port);
      hCom_port = NULL;
      return NULL;
    }

  k = ClearCommError(hCom, &dwErrors, &comStat);
  if (k && comStat.fCtsHold)  pb = 1;//win_printf(" Tx waiting for CTS signal");
  if (k && comStat.fDsrHold)  pb = 2;
  if (k && comStat.fDsrHold)  pb = 3;
  if (k && comStat.fRlsdHold) pb = 4;
  if (k && comStat.fXoffHold) pb = 5;
  if (k && comStat.fXoffSent) pb = 6;
  if (k && comStat.fEof)      pb = 7;
  if (k && comStat.fTxim)     pb = 8;
  if (k && comStat.cbInQue)   pb = 9;
  if (k && comStat.cbOutQue)  pb = 10;
  if (pb != 0)
    {
      win_printf("Init Serial port %d went Ok but the port in not ready for Writting\n"
		 "Check if the rs232 cable is correctly connected pb %s", port_str, pb);
    }
  my_set_window_title("Init Serial port %s OK", port_str);
  return hCom_port;
}


int Write_serial_port(HANDLE handle, char *data, DWORD length)
{
  int success = 0, k, pb = 0;
  DWORD dwWritten = 0, dwWritten2 = 0;
  OVERLAPPED o = {0};
  DWORD dwErrors;
  COMSTAT comStat;


  k = ClearCommError(hCom, &dwErrors, &comStat);
  if (k && comStat.fCtsHold)  pb = 1;//win_printf(" Tx waiting for CTS signal");
  if (k && comStat.fDsrHold)  pb = 2;
  if (k && comStat.fDsrHold)  pb = 3;
  if (k && comStat.fRlsdHold) pb = 4;
  if (k && comStat.fXoffHold) pb = 5;
  if (k && comStat.fXoffSent) pb = 6;
  if (k && comStat.fEof)      pb = 7;
  if (k && comStat.fTxim)     pb = 8;
  if (k && comStat.cbInQue)   pb = 9;
  if (k && comStat.cbOutQue)  pb = 10;
  /*
    if (k && comStat.fCtsHold)  pb = 1;//win_printf(" Tx waiting for CTS signal");
    if (k && comStat.fDsrHold)  win_printf(" Tx waiting for DSR signal");
    if (k && comStat.fRlsdHold) win_printf(" Tx waiting for RLSD signal");
    if (k && comStat.fXoffHold) win_printf(" Tx waiting, XOFF char rec'd");
    if (k && comStat.fXoffSent) win_printf(" Tx waiting, XOFF char sent");
    if (k && comStat.fEof)      win_printf(" EOF character received");
    if (k && comStat.fTxim)     win_printf(" Character waiting for Tx; char queued with TransmitCommChar");
    if (k && comStat.cbInQue)   win_printf(" comStat.cbInQue bytes have been received, but not read");
    if (k && comStat.cbOutQue)  win_printf(" comStat.cbOutQue bytes are awaiting transfer");
  */
  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  if (!WriteFile(handle, (LPCVOID)data, length, &dwWritten, &o))
    {
      if (GetLastError() == ERROR_IO_PENDING)
	if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	  if (GetOverlappedResult(handle, &o, &dwWritten2, FALSE))
	    success = 1;
    }
  else    success = 1;
  if (dwWritten != length)     success = 0;
  if (dwWritten2 == length)     success = 1;
  CloseHandle(o.hEvent);
  return (success == 0) ? -1 : ((dwWritten < dwWritten2) ? dwWritten2 : dwWritten);
}


int ReadData(HANDLE handle, BYTE *data, DWORD length, DWORD *dwRead)
{
  int success = 0;
  OVERLAPPED o = {0};

  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  if (!ReadFile(handle, data, length, dwRead, &o))
    {
      if (GetLastError() == ERROR_IO_PENDING)
	if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	  success = 1;
      GetOverlappedResult(handle, &o, dwRead, FALSE);
    }
  else    success = 1;
  CloseHandle(o.hEvent);
  return success;
}

int Read_serial_port(HANDLE handle, char *stuff, DWORD max_size, DWORD *dwRead)
{
  register int i, j, k;
  unsigned long t0;
  //  int len;
  BYTE lch[2];//, chtmp[128];
  int ltimeout = 0;
  DWORD dwErrors;
  COMSTAT comStat;

  if (handle == NULL) return -2;


  t0 = get_my_uclocks_per_sec() / 20; // we cannot wait more than 50 ms
  t0 += my_uclock();
  for (i = j = 0; j == 0 && i <(int) max_size - 1 && ltimeout == 0;)
    {
      *dwRead = 0;

      if (ReadData(handle, lch, 1, dwRead) == 0)
        {
	  stuff[i] = 0;
	  *dwRead = i;
	  k = ClearCommError(hCom, &dwErrors, &comStat);

	  //if (k && comStat.cbInQue)   Read_serial_port(hCom,chtmp,127,&len);
	  //if (j && comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	  //if (j && comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	  //if (j && comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	  //if (j && comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	  //if (j && comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	  //if (j && comStat.fEof)      my_set_window_title(" EOF character received");
	  if (k && comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
	  //if (j && comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	  if (k && comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
        }
      if (*dwRead == 1)
        {
	  stuff[i] = lch[0];
	  j = (lch[0] == '\n') ? 1 : 0;
	  i++;
        }
      ltimeout = (t0 > my_uclock()) ? 0 : 1;
    }
  stuff[i] = 0;
  *dwRead = i;
  return (ltimeout) ? -2 : i;
}
int sent_cmd_and_read_answer(HANDLE lhCom, char *Command, char *answer, DWORD n_answer, DWORD *n_written,
                             DWORD *dwErrors, unsigned long *t0)
{
  int rets, ret, i, j;
  COMSTAT comStat;
  char l_command[128], chtmp[128]; // , ch[2]
  DWORD len = 0;

  if (lhCom == NULL || Command == NULL || answer == NULL || strlen(Command) < 1) return 1;

  for (i = 0, j = 1; i < 5 && j != 0; i++)
    {
      j = ClearCommError(lhCom, dwErrors, &comStat);
      if (j && comStat.cbInQue)   Read_serial_port(lhCom, chtmp, 127, &len);
      //if (j && comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
      //if (j && comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
      //if (j && comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
      //if (j && comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
      //if (j && comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
      //if (j && comStat.fEof)      my_set_window_title(" EOF character received");
      if (j && comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
      //if (j && comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
      if (j && comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
    }
  for (i = 0; i < 126 && Command[i] != 0 && Command[i] != 13; i++)
    l_command[i] = Command[i]; // we copy until CR

  l_command[i++] = 13;         // we add it
  l_command[i] = 0;            // we end string

  if (t0) *t0 = my_uclock();
  rets = Write_serial_port(lhCom, l_command, strlen(l_command));
  if (rets <= 0)
    {
      if (t0) *t0 = my_uclock() - *t0;
      // Get and clear current errors on the port.
      if (ClearCommError(lhCom, dwErrors, &comStat))
        {
	  if (comStat.cbInQue)  Read_serial_port(lhCom, chtmp, 127, &len);
	  //if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	  //if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	  //if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	  //if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	  //if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	  //if (comStat.fEof)      my_set_window_title(" EOF character received");
	  if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");

	  //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	  if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
	  return -2;
        }
      return -1;
    }
  *n_written = 0;
  ret = Read_serial_port(lhCom, answer, n_answer, n_written);
  for (; ret == 0;)
    ret = Read_serial_port(lhCom, answer, n_answer, n_written);
  if (t0) *t0 = my_uclock() - *t0;
  answer[*n_written] = 0;
  strncpy(last_answer_2, answer, 127);
  if (ret < 0)
    {
      // Get and clear current errors on the port.
      if (ClearCommError(lhCom, dwErrors, &comStat))
        {
	  if (comStat.cbInQue)  Read_serial_port(lhCom, chtmp, 127, &len);
	  //if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	  //if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	  //if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	  //if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	  //if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	  //if (comStat.fEof)      my_set_window_title(" EOF character received");
	  if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");

	  //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	  if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
	  return 2;
        }
      return 1;
    }
  return 0;
}


int talk_serial_port(HANDLE hCom_port, char *command, int wait_answer, char *answer, DWORD NumberOfBytesToWrite,
                     DWORD nNumberOfBytesToRead)
{
  //int NumberOfBytesWrittenOnPort = -1;
  unsigned long NumberOfBytesWritten = -1;


  if (hCom == NULL) return win_printf_OK("No serial port init");

  if (wait_answer != READ_ONLY)
    {
      //NumberOfBytesWrittenOnPort =
      Write_serial_port(hCom_port, command, NumberOfBytesToWrite);
    }
  if (wait_answer != WRITE_ONLY)
    {
      Read_serial_port(hCom_port, answer, nNumberOfBytesToRead, &NumberOfBytesWritten);
    }
  return 0;
}

/*
  Attemp to read the stm32 response from serial port
  if a full answer is available then transfer it
  otherwise grab characters and respond that full message is not yet there

*/

char *catch_pending_request_answer(int *size, int im_n, int *error, unsigned long *t0)
{
  static char answer[128], request_answer[128];
  unsigned long dwRead = 0;
  static int i = 0;
  int j, max_size = 128, lf_found = 0;
  BYTE lch[2];

  (void)im_n;
  if (t0 != NULL) *t0 = my_uclock();

  if (hCom == NULL)
    {
      *error = -3;
      if (t0 != NULL) *t0 = my_uclock() - *t0;
      return NULL;   // no serial port !
    }

  for (j = 0; j == 0 && i < max_size - 1;)
    {
      // we grab rs232 char already arrived
      dwRead = 0;
      if (ReadData(hCom, lch, 1, &dwRead) == 0)
        {
	  // something wrong
	  answer[i] = 0;
	  dwRead = i;
	  *error = -2;
	  if (t0 != NULL) *t0 = my_uclock() - *t0;
	  return NULL;    // serial port read error !
        }
      if (dwRead == 1)
        {
	  answer[i] = lch[0];
	  j = lf_found = (lch[0] == '\n') ? 1 : 0;
	  i++;
        }
      else if (dwRead == 0) j = 1;
    }
  answer[i] = 0;
  *size = i;
  if (lf_found)
    {
      *error = 0;
      strncpy(request_answer, answer, i);
      if (t0 != NULL) *t0 = my_uclock() - *t0;
      i = 0;
      return request_answer;
    }
  *error = 1;
  if (t0 != NULL) *t0 = my_uclock() - *t0;
  return NULL;
}




int n_write_read_serial_port(void)
{
  static char Command[128] = "dac16?";
  static int ntimes = 1, i;
  unsigned long t0;
  double dt, dtm, dtmax;
  char resu[128], *ch;
  int ret = 0;
  unsigned long lpNumberOfBytesWritten = 0;
  DWORD nNumberOfBytesToRead = 127;

  if (updating_menu_state != 0)    return D_O_K;

  if (hCom == NULL) return win_printf_OK("No serial port init");
  for (ch = Command; *ch != 0; ch++)
    if (*ch == '\r') *ch = 0;
  win_scanf("Command to send? %snumber of times %d", &Command, &ntimes);
  strncat(Command, "\r", (sizeof(Command) > strlen(Command)) ? sizeof(Command) - strlen(Command) : 0);

  for (i = 0, dtm = dtmax = 0; i < ntimes; i++)
    {
      t0 = my_uclock();
      Write_serial_port(hCom, Command, strlen(Command));

      ret = Read_serial_port(hCom, resu, nNumberOfBytesToRead, &lpNumberOfBytesWritten);
      t0 = my_uclock() - t0;
      dt = 1000 * (double)(t0) / get_my_uclocks_per_sec();
      resu[lpNumberOfBytesWritten] = 0;
      dtm += dt;

      if (dt > dtmax) dtmax = dt;
    }
  dtm /= ntimes;
  win_printf("command read = %s \n ret %d lpNumberOfBytesWritten = %d\nin %g mS in avg %g max", resu, ret,
	     lpNumberOfBytesWritten, dtm, dtmax);
  return D_O_K;
}



int n_read_serial_port(void)
{
  static int ntimes = 1, i;
  unsigned long t0;
  double dt;
  char resu[128];
  int ret = 0, tot = 0;
  unsigned long lpNumberOfBytesWritten = 0;
  DWORD nNumberOfBytesToRead = 127;

  if (updating_menu_state != 0)    return D_O_K;

  if (hCom == NULL) return win_printf_OK("No serial port init");
  win_scanf("Number of times read %d", &ntimes);

  for (i = 0, t0 = my_uclock(); i < ntimes; i++)
    {
      ret = Read_serial_port(hCom, resu, nNumberOfBytesToRead, &lpNumberOfBytesWritten);
      tot += lpNumberOfBytesWritten;
    }

  t0 = my_uclock() - t0;
  dt = 1000 * (double)(t0) / get_my_uclocks_per_sec();
  win_printf("%d reads of %d characters took %g ms",ntimes, tot, dt);
  return D_O_K;
}

int purge_com(void)
{
  if (hCom == NULL) return 1;
  return PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
}
int init_stm32_serial(void)
{
  int i;
  static int port_number = 4, baud = 9600, hand = 0, parity = 0, stops = 1, n_bits = 8;
  static int first_init = 1;
  char port_str[256] = {0};

  if (hCom != NULL) return 0;
  if (first_init == 0) return 1;
  first_init = 0;
  // we only get here once

  for (; hCom == NULL;)
    {
      i = win_scanf("{\\color{yellow}Your Stm32twist serial interface did not open properly on last trial!} Check:\n"
		    " - that another program is not using the same serial port\n"
		    " - Another instance of Stm32jai.exe may still be running\n"
		    "    If this is the case open the Windows task manager and kill the process\n"
		    " - Last but not least your configuration may not be correct\n"
		    "If this is the case select a new one. {\\color{yellow}Port number:} ?%5d\n"
		    "{\\color{yellow}Baudrate:} %5d\n"
		    "{\\color{yellow}Number of bits:} %4d  (default 8)\n"
		    "{\\color{yellow}Parity:} No %R even %r odd %r (default No)\n"
		    "{\\color{yellow}Number of stop bits:} 0->%R 1->%r 2->%r (default 1)\n"
		    "{\\color{yellow}HandShaking:} None %R Cts/Rts %r (default Cts/Rts)\n"
		    "{\\color{lightred}If you hit cancel a dummy interface will be used\n"
		    "This only allows you to check your camera}\n"
		    , &port_number, &baud, &n_bits, &parity, &stops, &hand);
	
      if (i == CANCEL) return 0;
      sprintf(port_str, "\\\\.\\COM%d\0", port_number);
      hCom = init_serial_port(port_str, baud , n_bits, parity , stops, hand);
    }
  if (hCom == NULL)
    {
      win_printf("the serial interface did not stated properly\n"
		 "We switch to Dummy\n");
      return 1;
    }
  purge_com();
  add_plot_treat_menu_item("stm32_serial", NULL, stm32_serial_plot_menu(), 0, NULL);
  return 0;
}

int init_port(void)
{
  if (updating_menu_state != 0)    return D_O_K;

  if (hCom == NULL)
    {
      //add_item_to_menu(trackBead_ex_image_menu, "stm32_serial", NULL, stm32_serial_plot_menu(), 0, NULL);
      add_plot_treat_menu_item("stm32_serial", NULL, stm32_serial_plot_menu(), 0, NULL);
      add_image_treat_menu_item("stm32_serial", NULL, stm32_serial_plot_menu(), 0, NULL);
    }
  init_stm32_serial();
  return D_O_K;
}



int write_command_on_serial_port(void)
{
  static char Command[128] = "test";
  //char *test="\r";

  if (updating_menu_state != 0)    return D_O_K;

  win_scanf("Command to send? %s", &Command);
  strncat(Command, "\r", (sizeof(Command) > strlen(Command)) ? sizeof(Command) - strlen(Command) : 0);

  //win_printf("deja cela %d\n Command %s",strlen(Command), Command);
  Write_serial_port(hCom, Command, strlen(Command));
  //snprintf(Command,128,"?nm\r");
  //strcat(Command,'\r');
  //win_printf("deja cela %d\n Command %s",strlen(Command), Command);

  //win_printf("%s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
  return D_O_K;
}


int read_on_serial_port(void)
{
  char Command[128];
  unsigned long lpNumberOfBytesWritten = 0;
  int ret = 0;
  DWORD nNumberOfBytesToRead = 127;
  unsigned long t0;

  if (updating_menu_state != 0)    return D_O_K;

  t0 = my_uclock();
  //win_scanf("Command to send? %s",Command);
  ret = Read_serial_port(hCom, Command, nNumberOfBytesToRead, &lpNumberOfBytesWritten);
  t0 = my_uclock() - t0;
  Command[lpNumberOfBytesWritten] = 0;
  win_printf("command read = %s \n ret %d lpNumberOfBytesWritten = %d\n in %g mS", Command, ret, lpNumberOfBytesWritten,
	     1000 * (double)(t0) / get_my_uclocks_per_sec());

  //win_printf("command read = %s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
  return D_O_K;
}



int write_read_serial_port(void)
{
  if (updating_menu_state != 0)    return D_O_K;

  write_command_on_serial_port();
  read_on_serial_port();
  return D_O_K;
}




int CloseSerialPort(HANDLE lhCom)
{
  if (CloseHandle(lhCom) == 0) return win_printf("Close Handle FAILED!");

  return D_O_K;
}

int close_serial_port(void)
{
  if (updating_menu_state != 0)    return D_O_K;

  CloseSerialPort(hCom);
  hCom = NULL;
  return D_O_K;
}




MENU *stm32_serial_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL) return mn;

  add_item_to_menu(mn, "Init serial port", init_port, NULL, 0, NULL);
  add_item_to_menu(mn, "Write serial port", write_command_on_serial_port, NULL, 0, NULL);
  add_item_to_menu(mn, "Read serial port", read_on_serial_port, NULL, 0, NULL);
  add_item_to_menu(mn, "Write read serial port", write_read_serial_port, NULL, 0, NULL);
  add_item_to_menu(mn, "Write read n times", n_write_read_serial_port, NULL, 0, NULL);
  add_item_to_menu(mn, "Read n times", n_read_serial_port, NULL, 0, NULL);
  add_item_to_menu(mn, "Close serial port", close_serial_port, NULL, 0, NULL);
  //  add_item_to_menu(mn,"Read Objective position", stm32_read_Z,NULL,0,NULL);
  //add_item_to_menu(mn,"Set Objective position", stm32_set_Z,NULL,0,NULL);

  return mn;
}

int stm32_serial_main(int argc, char **argv)
{
  init_port();

  if (hCom != NULL) return D_O_K;
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item("stm32_serial", NULL, stm32_serial_plot_menu(), 0, NULL);
  add_image_treat_menu_item("stm32_serial", NULL, stm32_serial_plot_menu(), 0, NULL);
  return D_O_K;
}

int stm32_serial_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "stm32_serial", NULL, NULL);
  return D_O_K;
}
#endif

