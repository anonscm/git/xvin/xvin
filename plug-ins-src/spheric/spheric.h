#ifndef _SPHERIC_H_
#define _SPHERIC_H_

PXV_FUNC(int, do_spheric_average_along_y, (void));
PXV_FUNC(MENU*, spheric_image_menu, (void));
PXV_FUNC(int, spheric_main, (int argc, char **argv));
#endif

