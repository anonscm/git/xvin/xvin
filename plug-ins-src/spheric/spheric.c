/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _SPHERIC_C_
#define _SPHERIC_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "spheric.h"



O_i	*max_histo_tile(O_i *ois, int nx)
{
	register int i, j;
	char c1[256];
	int k, c, l;
	unsigned char z[256];
	O_i *oit = NULL, *oid = NULL;
	union pix *ps, *pd;
	int onx, ony, onxt, onyt, ix, iy;
	
	if (ois == NULL || ois->im.data_type != IS_CHAR_IMAGE)	
	  {
	    win_printf("Cannot handle images which are not unsigned char!");
	    return NULL;
	  }
	
	onx = ois->im.nx;
	ony = ois->im.ny;
	onxt = (onx % nx) ? onx/nx + 1 : onx/nx; 
	onyt = (ony % nx) ? ony/nx + 1 : ony/nx; 	
	oit = create_one_image(onxt, onyt, IS_CHAR_IMAGE);
	if (oit == NULL)	return NULL;
	for (ix = 0, ps = ois->im.pixel; ix < onxt; ix++)
	{
		for (iy = 0; iy < onyt; iy++)
		{
			for(k = 0; k < 256; z[k++] = 0);
			for (i = ix*nx; i < (ix+1)*nx; i++)
			{
				for (j = iy*nx;j < (iy+1)*nx; j++)
				{
					c = ps[(j<ony)?j:ony-1].ch[(i<onx)?i:onx-1];
					z[c]++;
				}
			}
			c = nx * nx;
			c /= 2;
			for(k = 0, l = 0; k < 256 && l < c; l += z[k] , k++);
			oit->im.pixel[iy].ch[ix] = k;
		}
	}

	oid = create_one_image(onx, ony, IS_CHAR_IMAGE);
	if (oid == NULL)	return NULL;	
	for(j = 0, pd = oid->im.pixel; j < ony; j++)
	{
		for(i = 0; i < onx; i++)
		{
			k = ps[j].ch[i];
			k <<= 8;

			k -= fast_interpolate_image_point(oit,((float)i)/nx -.5,((float)j)/nx -.5,255);
		
			k >>= 8;
			k  += 128;
			k %= 256;
			pd[j].ch[i] = k;
		}
	}
	free_one_image(oit);
	if (ois->im.source != NULL) 	strcpy(c1, ois->im.source);
	else				c1[0] = 0;
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	if (ois->title != NULL)	set_im_title(oid, "%s", ois->title);
	set_formated_string(&oid->im.treatement,"Image flaten by max histogram of %s",c1);
	return oid;
}
O_i	*max_histo_tile_RGB_pb(O_i *ois, int nx)
{
  register int i, j;
  char c1[256];
  int k, c, l, m = 0;
  unsigned char zrc[256], zgc[256], zbc[256];
  O_i *oit = NULL, *oid = NULL;
  union pix *ps, *pd;
  int onx, ony, onxt, onyt, ix, iy;
  float zr, zg, zb, za;
	
  if (ois == NULL || ois->im.data_type != IS_RGB_PICTURE)	
    {
      win_printf("Cannot handle images which are not RGB 8bits!");
      return NULL;
    }
	
  onx = ois->im.nx;
  ony = ois->im.ny;
  onxt = (onx % nx) ? onx/nx + 1 : onx/nx; 
  onyt = (ony % nx) ? ony/nx + 1 : ony/nx; 	
  oit = create_one_image(onx, ony, IS_RGB_PICTURE);
  if (oit == NULL)	return NULL;
  for (ix = 0, ps = ois->im.pixel; ix < onx-nx; ix++)
    {
      for (iy = 0; iy < ony-nx; iy++)
	{
	  for(k = 0; k < 256; k++)
	    zrc[k] = zgc[k] = zbc[k] = 0;
	  for (i = ix; i < ix+nx; i++)
	  {
	    for (j = iy;j < iy+nx; j++)
	      {
		c = ps[(j<ony)?j:ony-1].rgb[(i<onx)?i:onx-1].r;
		zrc[c]++;
		c = ps[(j<ony)?j:ony-1].rgb[(i<onx)?i:onx-1].g;
		zgc[c]++;
		c = ps[(j<ony)?j:ony-1].rgb[(i<onx)?i:onx-1].b;
		zbc[c]++;
	      }
	  }
	  c = nx * nx;
	  c /= 2;
	  for(m = k = 0, l = 0; k < 256; k++)
	    {
	      if (zrc[k] > l)
		{
		  l = zrc[k];
		  m = k;
		}
	    }
	  oit->im.pixel[iy+nx/2].rgb[ix+nx/2].r = m;
	  for(m = k = 0, l = 0; k < 256; k++)
	    {
	      if (zgc[k] > l)
		{
		  l = zgc[k];
		  m = k;
		}
	    }
	  oit->im.pixel[iy+nx/2].rgb[ix+nx/2].g = m;
	  for(m = k = 0, l = 0; k < 256; k++)
	    {
	      if (zbc[k] > l)
		{
		  l = zbc[k];
		  m = k;
		}
	    }
	  oit->im.pixel[iy+nx/2].rgb[ix+nx/2].b = m;
	}
    }
  
  for (ix = nx/2, ps = ois->im.pixel; ix < onx-nx/2; ix++)
    {
      for (iy = 0; iy < nx/2; iy++)
	{
	  oit->im.pixel[iy].rgb[ix] = oit->im.pixel[nx/2].rgb[ix]; 
	  oit->im.pixel[ony-1-iy].rgb[ix] = oit->im.pixel[ony-1-nx/2].rgb[ix]; 
	}
    }
  
  for (iy = 0, ps = ois->im.pixel; iy < ony; iy++)
    {
      for (ix = 0; ix < nx/2; ix++)
	{
	  oit->im.pixel[iy].rgb[ix] = oit->im.pixel[iy].rgb[nx/2]; 
	  oit->im.pixel[iy].rgb[onx-1-ix] = oit->im.pixel[iy].rgb[onx-1-nx/2]; 
	}
    }

  inherit_from_im_to_im(oit,ois);
  return oit;
      /*
      oid = create_one_image(onx, ony, IS_RGB_PICTURE);
      if (oid == NULL)	return NULL;	
      for(j = 0, pd = oid->im.pixel; j < ony; j++)
	{
	  for(i = 0; i < onx; i++)
	    {
	      //interpolate_RGB_picture_point( oit,((float)i/nx),((float)j/nx), &zr, &zg, &zb, &za);

	      //k -= fast_interpolate_image_point(oit,((float)i)/nx -.5,((float)j)/nx -.5,255);
	      zr = oit->im.pixel[j/nx].rgb[i/nx].r;
	      zg = oit->im.pixel[j/nx].rgb[i/nx].g;
	      zb = oit->im.pixel[j/nx].rgb[i/nx].b;
	      zr -= ps[j].rgb[i].r;
	      zr = -zr;
	      zr += 200;
	      pd[j].rgb[i].r = (unsigned char)((zr < 0) ? 0 : ((zr>254.5) ? 255 : zr+0.5));
	      zg -= ps[j].rgb[i].g;
	      zg = -zg;
	      zg += 200;
	      pd[j].rgb[i].g = (unsigned char)((zg < 0) ? 0 : ((zg>254.5) ? 255 : zg+0.5));
	      zb -= ps[j].rgb[i].b;
	      zb = -zb;
	      zb += 200;
	      pd[j].rgb[i].b = (unsigned char)((zb < 0) ? 0 : ((zb>254.5) ? 255 : zb+0.5));
	    }
	}
      free_one_image(oit);
      if (ois->im.source != NULL) 	strcpy(c1, ois->im.source);
      else				c1[0] = 0;
      inherit_from_im_to_im(oid,ois);
      uns_oi_2_oi(oid,ois);
      if (ois->title != NULL)	set_im_title(oid, "%s", ois->title);
      set_formated_string(&oid->im.treatement,"Image flaten by max histogram of %s",c1);
      return oid;
      */
    }
O_i	*max_histo_tile_RGB(O_i *ois, int nx)
{
	register int i, j;
	char c1[256];
	int k, c, l, m = 0;
	unsigned char zrc[256], zgc[256], zbc[256];
	O_i *oit = NULL, *oid = NULL;
	union pix *ps, *pd;
	int onx, ony, onxt, onyt, ix, iy;
	float zr, zg, zb, za;
	
	if (ois == NULL || ois->im.data_type != IS_RGB_PICTURE)	
	  {
	    win_printf("Cannot handle images which are not RGB 8bits!");
	    return NULL;
	  }
	
	onx = ois->im.nx;
	ony = ois->im.ny;
	onxt = (onx % nx) ? onx/nx + 1 : onx/nx; 
	onyt = (ony % nx) ? ony/nx + 1 : ony/nx; 	
	oit = create_one_image(onxt, onyt, IS_RGB_PICTURE);
	if (oit == NULL)	return NULL;
	for (ix = 0, ps = ois->im.pixel; ix < onxt; ix++)
	{
		for (iy = 0; iy < onyt; iy++)
		{
			for(k = 0; k < 256; k++)
			  zrc[k] = zgc[k] = zbc[k] = 0;
			for (i = ix*nx; i < (ix+1)*nx; i++)
			{
				for (j = iy*nx;j < (iy+1)*nx; j++)
				{
					c = ps[(j<ony)?j:ony-1].rgb[(i<onx)?i:onx-1].r;
					zrc[c]++;
					c = ps[(j<ony)?j:ony-1].rgb[(i<onx)?i:onx-1].g;
					zgc[c]++;
					c = ps[(j<ony)?j:ony-1].rgb[(i<onx)?i:onx-1].b;
					zbc[c]++;
				}
			}
			c = nx * nx;
			c /= 2;
			for(m = k = 0, l = 0; k < 256; k++)
			  {
			    if (zrc[k] > l)
			      {
				l = zrc[k];
				m = k;
			      }
			  }
			oit->im.pixel[iy].rgb[ix].r = m;
			for(m = k = 0, l = 0; k < 256; k++)
			  {
			    if (zgc[k] > l)
			      {
				l = zgc[k];
				m = k;
			      }
			  }
			oit->im.pixel[iy].rgb[ix].g = m;
			for(m = k = 0, l = 0; k < 256; k++)
			  {
			    if (zbc[k] > l)
			      {
				l = zbc[k];
				m = k;
			      }
			  }
			oit->im.pixel[iy].rgb[ix].b = m;
		}
	}
	//	inherit_from_im_to_im(oit,ois);
	//return oit;
	oid = create_one_image(onx, ony, IS_RGB_PICTURE);
	if (oid == NULL)	return NULL;	
	for(j = 0, pd = oid->im.pixel; j < ony; j++)
	{
		for(i = 0; i < onx; i++)
		{
		  interpolate_RGB_picture_point( oit,((float)i/nx),((float)j/nx), &zr, &zg, &zb, &za);

		  //k -= fast_interpolate_image_point(oit,((float)i)/nx -.5,((float)j)/nx -.5,255);
		  //zr = oit->im.pixel[j/nx].rgb[i/nx].r;
		  //zg = oit->im.pixel[j/nx].rgb[i/nx].g;
		  //zb = oit->im.pixel[j/nx].rgb[i/nx].b;
		  zr -= ps[j].rgb[i].r;
		  zr = -zr;
		  zr += 200;
		  pd[j].rgb[i].r = (unsigned char)((zr < 0) ? 0 : ((zr>254.5) ? 255 : zr+0.5));
		  zg -= ps[j].rgb[i].g;
		  zg = -zg;
		  zg += 200;
		  pd[j].rgb[i].g = (unsigned char)((zg < 0) ? 0 : ((zg>254.5) ? 255 : zg+0.5));
		  zb -= ps[j].rgb[i].b;
		  zb = -zb;
		  zb += 200;
		  pd[j].rgb[i].b = (unsigned char)((zb < 0) ? 0 : ((zb>254.5) ? 255 : zb+0.5));
		}
	}
	free_one_image(oit);
	if (ois->im.source != NULL) 	strcpy(c1, ois->im.source);
	else				c1[0] = 0;
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	if (ois->title != NULL)	set_im_title(oid, "%s", ois->title);
	set_formated_string(&oid->im.treatement,"Image flaten by max histogram of %s",c1);
	return oid;
}

int do_histo_flaten_image(void)
{
	static int nx = 16;
	register int i;
	O_i *ois, *oid;
	imreg *imr;
	
	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
	  return win_printf_OK("This routine flatens an image using its \n"
			       "local histogram");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
	i = win_scanf("size to perform histogramm :%d",&nx);
	if (i == CANCEL)	return OFF;
	if (ois->im.data_type == IS_RGB_PICTURE)	
	  oid = max_histo_tile_RGB(ois, nx);
	else oid = max_histo_tile(ois, nx);
	if (oid == NULL)	return win_printf("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));		
}


int do_spheric_average_along_y(void)
{
	register int i, j;
	int l_s, l_e;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average the image intensity"
			"of several lines of an image");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	return D_O_K;
	/* we grap the data that we need, the image and the screen region*/
	l_s = 0; l_e = ois->im.ny;
	/* we ask for the limit of averaging*/
	i = win_scanf("Average Between lines%d and %d",&l_s,&l_e);
	if (i == CANCEL)	return D_O_K;
	/* we create a plot to hold the profile*/
	op = create_one_plot(ois->im.nx,ois->im.nx,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0]; /* we grab the data set */
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	op->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;
	inherit_from_im_to_ds(ds, ois);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"spheric averaged profile from line %d to %d",l_s, l_e);
	set_formated_string(&ds->treatement,"%s averaged profile from line %d to %d",l_s, l_e);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	for (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, ds->xd);
		for (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];
	}
	for (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)
	{
		ds->xd[i] = i;
		ds->yd[i] /= j;
	}
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}

O_i	*spheric_image_multiply_by_a_scalar(O_i *ois, float factor)
{
	register int i, j;
	O_i *oid;
	float tmp;
	int onx, ony, data_type;
	union pix *ps, *pd;

	onx = ois->im.nx;	ony = ois->im.ny;	data_type = ois->im.data_type;
	oid =  create_one_image(onx, ony, data_type);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	if (data_type == IS_CHAR_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_INT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].in[j];
				pd[i].in[j] = (tmp < -32768) ? -32768 :
					((tmp > 32767) ? 32767 : (short int)tmp);
			}
		}
	}
	else if (data_type == IS_UINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ui[j];
				pd[i].ui[j] = (tmp < 0) ? 0 :
					((tmp > 65535) ? 65535 : (unsigned short int)tmp);
			}
		}
	}
	else if (data_type == IS_LINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].li[j];
				pd[i].li[j] = (tmp < 0x10000000) ? 0x10000000 :
					((tmp > 0x7FFFFFFF) ? 0x7FFFFFFF : (int)tmp);
			}
		}
	}
	else if (data_type == IS_FLOAT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[j] = factor * ps[i].fl[j];
			}
		}
	}
	else if (data_type == IS_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[j] = factor * ps[i].db[j];
			}
		}
	}
	else if (data_type == IS_COMPLEX_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[2*j] = factor * ps[i].fl[2*j];
				pd[i].fl[2*j+1] = factor * ps[i].fl[2*j+1];
			}
		}
	}
	else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[2*j] = factor * ps[i].db[2*j];
				pd[i].db[2*j+1] = factor * ps[i].db[2*j+1];
			}
		}
	}
	else if (data_type == IS_RGB_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 3*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_RGBA_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 4*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s rescaled by %g",
		 ois->title,factor);
	set_formated_string(&oid->im.treatement,
		"Image %s rescaled by %g", ois->filename,factor);
	return oid;
}

int do_spheric_image_multiply_by_a_scalar(void)
{
	O_i *ois, *oid;
	imreg *imr;
	int i;
	static float factor = 2.0;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("define the factor of mutiplication %f",&factor);
	if (i == CANCEL)	return D_O_K;
	oid = spheric_image_multiply_by_a_scalar(ois,factor);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}


MENU *spheric_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"average profile", do_spheric_average_along_y,NULL,0,NULL);
	add_item_to_menu(mn,"image z rescaled", do_spheric_image_multiply_by_a_scalar,NULL,0,NULL);
	add_item_to_menu(mn,"Flaten image", do_histo_flaten_image,NULL,0,NULL);

	return mn;
}

int	spheric_main(int argc, char **argv)
{
	add_image_treat_menu_item ( "spheric", NULL, spheric_image_menu(), 0, NULL);
	return D_O_K;
}

int	spheric_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "spheric", NULL, NULL);
	return D_O_K;
}
#endif

