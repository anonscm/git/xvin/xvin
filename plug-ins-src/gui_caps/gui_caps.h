#ifndef GUI_CAPS_H_
#define GUI_CAPS_H_

/*colors*/
#define BLACK 0
#define BLUE 255
#define GREEN 65280
#define RED 16711680
#define WHITE 16777215
#define PINK 16737480

#define GREY_32 2105376
#define GREY_64 4210752
#define GREY_96 6316128
#define GREY_128 8421504
#define GREY_160 10526880
#define GREY_192 12632256
#define GREY_224 14737632

/*fons sizes*/
#define SMALLSIZE 8
#define MEDIUMSIZE 12
#define NORMALSIZE 16
#define BIGSIZE 24
#define HUGESIZE 36

PXV_FUNC(int, bubble_test, (void));
PXV_FUNC(BITMAP*, new_button, (int x_pos, int y_pos, int n_lines, char *example_test));
PXV_FUNC(int, rgb2c, (int red, int green, int blue));
PXV_FUNC(int, special_bubble, (int x,int y,int color,int size,char *format, ...));
PXV_FUNC(MENU*, gui_caps_image_menu, (void));
PXV_FUNC(int, gui_caps_main, (int argc, char **argv));

#endif

