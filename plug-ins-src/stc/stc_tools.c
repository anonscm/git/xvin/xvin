#ifndef _STC_TOOLS_C_
#define _STC_TOOLS_C_

#include <stddef.h> 		/* for size_t */
#include <string.h>		/* for strlen */
#include "stc_tools.h"

/************************************************************************/
/* to convert an array of float into an array of double			*/
/************************************************************************/
int f_to_d(float *in, double *out, int N)
{	register int i;

	for (i=0; i<N; i++)
	{	out[i] = (double)in[i];
	}
	
	return(0);
}




/************************************************************************/
/* concatenate two chains of char : name=prefix+name			*/
/* (like strcat, but output is in name instead of prefix)		*/
/************************************************************************/
size_t add_prefix(char *prefix, char *name)
{	size_t	pl, nl;
	int 	i;
	
	pl = strlen(prefix);
	nl = strlen(name);

	for (i=(int)nl; i>=0; i--)	name[i+pl] = name[i];
	memcpy(name, prefix, pl*sizeof(char));

	return(pl+nl);
}



#endif
