/************************************************************************/
/* Gallavotti-Cohen tests						*/
/*									*/
/* functions that computed global quantities over the PDE solution	*/
/*									*/
/* version 4.0.0	11/26/2004 					*/
/************************************************************************/
/* Nicolas Garnier	nicolas.garnier@ens-lyon.fr			*/
/************************************************************************/


extern char	time_data_rho_mean_filename[128],
	time_data_e1_mean_filename[128],
	time_data_e2_mean_filename[128],
	time_data_rho_local_filename[128],
	time_data_e1_local_filename[128],
	time_data_e2_local_filename[128],
	time_data_jA_local_filename[128],
	time_data_jA_mean_filename[128],
	time_data_v2_mean_filename[128],
	time_data_D_e1_local_filename[128],
	time_data_D_e1_mean_filename[128],
	time_data_D_e2_local_filename[128],
	time_data_D_e2_mean_filename[128],
	time_data_dissip_local_filename[128],
	time_data_dissip_mean_filename[128],
	time_data_rho2_mean_filename[128],
	time_data_rho3_mean_filename[128],
	time_data_tTT_mean_filename[128],
	time_data_rhov2_mean_filename[128],
	time_data_d1_mean_filename[128],
	time_data_d2_mean_filename[128],
	time_data_gradrho_mean_filename[128],
	time_data_D_u_mean_filename[128],
	time_data_D_k_mean_filename[128]	;


/****************************************************************************************/
int	GC_init(int ny, char *prefix_filename);	// init counter, alloc memory blocks
							// and prepare filenames
int	GC_reset(void);			// reset counter to zero, keeping same memory blocks
int GC_compute(double *A);	// compute quantity and increase counter
int GC_save(void);			// save computed quantities
int	GC_free(void);			// free pointers initialized with GC_init();
