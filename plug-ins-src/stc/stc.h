#ifndef _STC_H_
#define _STC_H_

PXV_FUNC(int, stc_main, 			(int argc, char **argv));
PXV_FUNC(int, stc_unload,			(int argc, char **argv));

PXV_FUNC(int, do_load_simulation_parameters,	(void));
PXV_FUNC(int, do_edit_simulation_parameters,	(void));
PXV_FUNC(int, do_set_filenames,			(void));
PXV_FUNC(int, do_print_simulation_infos,	(void));

#define real_dt 	auto_dt*auto_nbpas
#define real_npts	auto_ny*auto_nbloop

// global variables used by all subfiles:
extern char	simulation_parameters_fullfilename[512];
extern char	simulation_filename_prefix[128];

extern int		is_first_load_dissipation; 

// note that all CGL parameters and numerical simulation parameters are defined in CGL_parameters.h
// which is taken directly from the CGL code (same file ! good!!!).

#endif

