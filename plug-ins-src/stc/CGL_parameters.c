/************************************************************************/
/* interface with file and command line 					*/
/* 													*/
/* version 2.0.0 may 9th 2003							*/
/* version 1.1.0 march 2003							*/
/* version 3.0.0 november 2006, Warsaw, with XVin inclusion	*/
/************************************************************************/
/* Nicolas Garnier	nicolas.garnier@ens-lyon.fr			*/
/************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef XV_WIN32
#define XVIN
#else 
#ifdef XV_UNIX
#define XVIN
#else
#ifdef XV_MAC
#define  XVIN
#endif
#endif
#endif

#ifdef XVIN
#include "xvin.h"
#include "../inout/inout.h"
#else 
#include "inout.h"
#define win_printf printf
#endif

#include "CGL_parameters.h"
#include "lyaps_common.h"


/****************************************************************************************/
/* following variables can be used in the main (e.g., to set them up)			*/
/* so they are defined as extern in the cgl_xxx.h headers, but defined for real here:	*/
/****************************************************************************************/
int 	auto_nx=256,		/* size of image region = size of destination image 	*/
	auto_ny=4096;

float 	auto_c1=1.5,	auto_c2=-1.5, /* coefficeints for the equation			*/
	auto_vg=0.875,	auto_epsilon=1., auto_c0=0.,
	auto_lambda=1,	auto_mu=1, auto_alpha=0, auto_beta=0, auto_L=35.2;

float	auto_gamma=0, auto_gammac3=0, auto_delta=0, auto_deltac4=0,
	auto_eta=0, auto_etac5=0, auto_nu=0, auto_nuc6=0;
				/* additional coefficients for the integrated equation	*/
int	auto_sign=-1;		/* -1 for CGL 3 and +1 for CGL5				*/
float	auto_hr=0, auto_hi=0,	/* additional coefficients for the integrated equation	*/			
	auto_noise_amp=0;	/* noise level						*/
float	auto_dt=0.05;		/* time step 						*/
int	auto_nbpas=50;
				/* number of intergation steps between two saved lines	*/
				/* and before the first line				*/
int	auto_nbloop=1;		/* number of big loops 					*/ 
int	auto_nb_save=1;		/* number of saved time (out of nbloop)			*/
				/* 	if nbloop=100 and nb_save=25, 			*/
				/*	then lyaps will be saved every 4 steps		*/
				/*	at the end of the step				*/
float	auto_precision=2.5;	/* dt = dx*dx/(2*auto_precision) : auto_precision >=1 	*/
				/* some tests showed that 5 is always enough		*/
/****************************************************************************************/



#ifdef XVIN
// following is a copy/paste from the code main.c, in order to use the CGL_parameter.c in XVin :

// definitions for selecting the integration method and the equation:
#define CGL3_GSCN	2
#define CGL3_PS	4
#define NLS_GSCN	8
#define CGL5_GSCN	16

int 	verbosity = 1;
int	  integration_function = CGL3_GSCN;

int 	finite_time_tau = 1; 	// integration time for the computation of finite time lyaps.
				// it is counted in units of loops 

/********************************************************************************/
int   auto_go_on=0;	// boolean if 1, then resume previous computation	*/
int   do_save_st=1;	// boolean if 1, then st diagram is saved		*/
int   do_GC_tracing=0;// boolean, 0 for not computing dissipation and other quantities, 1 to compute them

/********************************************************************************/
/* parameters for Lyapunov computation:						*/
int	do_lyaps=0;	// 0 for not computing Lyaps, 
			// 1 for computing Lyaps using stupid matrix method
			// 2 for computing Lyaps using Benettin method.
			// 4 for computing Lyaps using Ruelle-Eckmann (QR decompositions) method.
int	do_lyaps_go_on=0; // boolean. if 1, then matrix Q in Ruelle-Eckmann scheme is saved
int	do_lyaps_RE_save_matrices=1;	// boolean; do we save intermediate matrices ? (same as do_lyaps_go_on)
int do_lyaps_RE_finite_time=0;	  // boolean; do we compute the product of R_i ? (slower, used for finite time lyaps)
int	do_lyaps_space=LYAPS_IN_REAL_SPACE;	// LYAPS_IN_REAL_SPACE or LYAPS_IN_FOURIER_SPACE
int	finite_time_lyaps_number=30;		    // number of local_lyaps to trace in time
int	do_lyaps_RE_finite_time_reset=1;	  // do we reset the computation of local lyaps when getting them ?
int do_lyaps_RE_continuity_vectors=0;   // do we compute right-eigenvectors of R to disentangle the local lyaps ?

// end of the copy/paste
#endif


void show_options(void){
  
  fprintf(stderr,"Integration of CGLE and calculation of its global and local Lyapunov exponents\n");
  fprintf(stderr,"Usage: run.exe [options]\n");
  fprintf(stderr,"Note that the command line options update the values read from the params.txt file\n");
  fprintf(stderr,"List of command-line options:\n");  
  fprintf(stderr,"\n\tParameters in equation:\n\n");  
  fprintf(stderr,"-e  epsilon, float (1.)\n");
  fprintf(stderr,"-c0 c0, float (0.)\n");
  fprintf(stderr,"-c1 c1, float (1.5)\n");
  fprintf(stderr,"-c2 c2, float (-1.5)\n");
  fprintf(stderr,"-vg vg, float (0.875)\n");
  fprintf(stderr,"-si sign (-1)\n");
  fprintf(stderr,"-hr hr, float (0.)\n");
  fprintf(stderr,"-hi hi, float (0.)\n");
    
  fprintf(stderr,"\n\tSpace-related parameters:\n\n"); 
  fprintf(stderr,"-nx nx, integer - number of points in the spatial direction(256)\n");     
  fprintf(stderr,"-L L, float - size of the spatial domain (35.2)\n");

  fprintf(stderr,"\n\tTime-related parameters:\n\n"); 
  fprintf(stderr,"-ny ny, integer - number of points in the time direction (4096)\n"); 
  fprintf(stderr,"-dt dt, float - time step (0.05)\n");
  fprintf(stderr,"-np nbpas, integer - number of time-steps between two points in time (50)\n");
  fprintf(stderr,"-nl nbloop, integer - number of loops (1) - a loop is n_y*nb_pas*d_t long\n");
  fprintf(stderr,"-ns nb_save, integer - number of saved lines of lyaps (1)\n");

  fprintf(stderr,"\n\tOther parameters:\n\n"); 
  fprintf(stderr,"-v verbosity, integer (1)\n"
			" 0: mute\n"
			" 1: normal, basic messages\n"
			" 2: more messages\n"
			" 3: even more messages (step numbers of external loop)\n"
			" 4: max messages (step numbers of integration routines)\n");
  fprintf(stderr,"-i integration function and scheme, integer (2) \n %s\n %s\n %s\n",
	  " 2 : CGL3, with Gauss-Siedel, Crank-Nickolson scheme",
	  " 4 : CGL3, with pseudo-spectral method", 
	  " 8 : NLS, with Gauss-Siedel, Crank-Nickolson scheme");  
  fprintf(stderr,"-go go_on, boolean - use last last of previous run as init. cond. (0)\n %s\n %s\n %s\n %s\n",
	  " -1 : random initial conditions",
	  " 0 : initial conditions in file \"init_filename\"",
	  " 1 : continue last run",
	  " 2 : continue last run, and append data treatments to previous files");
  fprintf(stderr,"-s save_st, integer - save spatiotemporal diagram in file (1)\n %s\n %s\n %s\n",
	  " 0 : no saving",
	  " 1 : save (but erase old file)",
	  " 2 : append (keep old file)");
  fprintf(stderr,"-gc do_GC_tracing, integer - compute energies and dissipation (0)\n %s\n",
	  " 0 : no, 1 : yes");

  fprintf(stderr,"\n\tLyapunov parameters:\n\n"); 
  fprintf(stderr,"-lyaps do_lyaps, integer do you want to compute Lyapunov spectrum ? (0)\n %s\n %s\n %s\n %s\n",
	  " 0 : nothing is computed",
	  " 1 : Lyaps using matrix method",
	  " 2 : Lyaps using Benettin et al method",
	  " 4 : Lyaps using Eckman-Ruelle method (from their Rev.Mod.Phys. paper)");
	fprintf(stderr,"-lsp do_lyaps_space, integer 0 for real space, 1 for Fourier space (0)\n");
  fprintf(stderr,"-lgo do_lyaps_go_on, integer continue or not the Lyapunov computation (0)\n "
    " 0 : no continuation, new files are output\n "
	  " 1 : continue last run, but new files are output\n "
	  " 2 : continue last run, and append results to previous files\n");
  fprintf(stderr,"-lsm do_lyaps_RE_save_matrices, boolean save matrices\n");
  fprintf(stderr,"-lft do_lyaps_RE_finite_time, boolean compute finite time lyaps\n");
  fprintf(stderr,"-lfn finite_time_lyaps_number, integer the lfn first lyaps are saved\n");
  fprintf(stderr,"-lfr do_lyaps_RE_finite_time_reset, integer reset R each loop lyaps are got\n");
  fprintf(stderr,"-lcv do_lyaps_RE_continuity_vectors, integer compute right-ev of R\n"
    "\t and disentangle local lyaps with this information (a file is saved)\n");

  fprintf(stderr,"-fi root_filename, string - the resulting files names will start with this root (run0)\n");
  fprintf(stderr,"-h help, print this information\n");
  fprintf(stderr,"\n");
  exit(0);
  
} /* show_options */



int read_options(int argc,char **argv){
  
  int i;
  
  i=1;
  while (i<argc)
    {
      
      if (strcmp(argv[i],"-h")==0)		(void) show_options();
      
      else if (strcmp(argv[i],"-e")==0)		auto_epsilon = (float) atof(argv[++i]);
      else if (strcmp(argv[i],"-c0")==0)	auto_c0 =      (float) atof(argv[++i]);
      else if (strcmp(argv[i],"-c1")==0)	auto_c1 =      (float) atof(argv[++i]);
      else if (strcmp(argv[i],"-c2")==0)	auto_c2 =      (float) atof(argv[++i]);
      else if (strcmp(argv[i],"-vg")==0)	auto_vg =      (float) atof(argv[++i]);
      else if (strcmp(argv[i],"-si")==0)	auto_sign =    (int)   atoi(argv[++i]);
      else if (strcmp(argv[i],"-hr")==0)	auto_hr =      (float) atof(argv[++i]);
      else if (strcmp(argv[i],"-hi")==0)	auto_hi =      (float) atof(argv[++i]);
      
      else if (strcmp(argv[i],"-nx")==0)	auto_nx =      (int)   atoi(argv[++i]);
      else if (strcmp(argv[i],"-L")==0)		auto_L =       (float) atof(argv[++i]);

      else if (strcmp(argv[i],"-ny")==0)	auto_ny =      (int)   atoi(argv[++i]);
      else if (strcmp(argv[i],"-dt")==0)	auto_dt =      (float)   atof(argv[++i]);
      else if (strcmp(argv[i],"-np")==0)	auto_nbpas =   (int)   atoi(argv[++i]);
      else if (strcmp(argv[i],"-nl")==0)	auto_nbloop  =  (int)   atoi(argv[++i]);
      else if (strcmp(argv[i],"-ns")==0)	auto_nb_save =  (int)   atoi(argv[++i]);
 
      else if (strcmp(argv[i],"-v")==0)		verbosity =    (int)   atoi(argv[++i]);
      else if (strcmp(argv[i],"-i")==0)		integration_function = atoi(argv[++i]);
      else if (strcmp(argv[i],"-go")==0)	auto_go_on =   (int)   atoi(argv[++i]);
      else if (strcmp(argv[i],"-s")==0)		do_save_st =   (int)   atoi(argv[++i]);
      else if (strcmp(argv[i],"-gc")==0)	do_GC_tracing= (int)   atoi(argv[++i]);

      else if (strcmp(argv[i],"-lyaps")==0)	do_lyaps  =    (int) atoi(argv[++i]);
      else if (strcmp(argv[i],"-lsp")==0)	do_lyaps_space = (int) atoi(argv[++i]);
      else if (strcmp(argv[i],"-lgo")==0)	do_lyaps_go_on = (int) atoi(argv[++i]);
      else if (strcmp(argv[i],"-lsm")==0)	do_lyaps_RE_save_matrices = (int)atoi(argv[++i]);
      else if (strcmp(argv[i],"-lft")==0)	do_lyaps_RE_finite_time   = (int)atoi(argv[++i]);
      else if (strcmp(argv[i],"-lfn")==0)	finite_time_lyaps_number  = (int)atoi(argv[++i]);
      else if (strcmp(argv[i],"-lfr")==0)	do_lyaps_RE_finite_time_reset  = (int)atoi(argv[++i]);   
      else if (strcmp(argv[i],"-lcv")==0)	do_lyaps_RE_continuity_vectors = (int)atoi(argv[++i]);

      else if (strncmp(argv[i],"-fi",3)==0)	{ // there is an argument which first 3 characters are '-fi'
						  // this will be a filename, treated elsewhere,
						  // but not generating an error here
						i++;
						}

      else return(printf("error, unknown option %s, type -h for help.\n", argv[i]));

      i++;
    }
  
  return 0;
} // read_options 



/************************************************************************/
/* read values of the parameters used in the equation  from a file	*/
/************************************************************************/
int CGL_general_load_parameters(char *filename)
{	int	error=0;

	if (load_parameter(filename,"epsilon",&auto_epsilon) !=1) 
		{ win_printf("error loading parameter epsilon, using default value = %f\n",auto_epsilon);
		  error+=1; }
	if (load_parameter(filename,"c0",&auto_c0) !=1) 
		{ win_printf("error loading parameter c0, using default value = %f\n",auto_c0);
		  error+=1; }
	if (load_parameter(filename,"c1",&auto_c1) !=1) 
		{ win_printf("error loading parameter c1, using default value = %f\n",auto_c1);
		  error+=1; }
	if (load_parameter(filename,"c2",&auto_c2) !=1) 
		{ win_printf("error loading parameter c2, using default value = %f\n",auto_c2);
		  error+=1; }
	if (load_parameter(filename,"vg",&auto_vg) !=1) 
		{ win_printf("error loading parameter vg, using default value = %f\n",auto_vg);
		  error+=1; }
	if (load_parameter(filename,"L",&auto_L) !=1) 
		{ win_printf("error loading parameter L, using default value = %f\n",auto_L);
		  error+=1; }
	
	return(error); /* error is the number of unread parameters */
}



/************************************************************************/
/* read values of the parameters of integration scheme from a file	*/
/************************************************************************/
int numerics_general_load_parameters(char *filename)
{	float 	x;
	int	error=0;

	if (load_parameter(filename,"dt",&auto_dt) !=1) 
		{ win_printf("error loading parameter dt, using default value = %f\n",auto_dt);
		  error+=1; }
	if (load_parameter(filename,"nx",&x) !=1) 
		{ win_printf("error loading parameter nx, using default value = %d\n",auto_nx);
		  error+=1; }
		else auto_nx=(int)x;
	if (load_parameter(filename,"nbpas",&x) !=1) 
		{ win_printf("error loading parameter nbpas, using default value = %d\n",auto_nbpas);
		  error+=1; }
		else auto_nbpas=(int)x;
	if (load_parameter(filename,"ny",&x) !=1) 
		{ win_printf("error loading parameter ny, using default value = %d\n",auto_ny);
		  error+=1; }
		else auto_ny=(int)x;
	if (load_parameter(filename,"nbloop",&x) !=1) 
		{ win_printf("error loading parameter nbloop, using default value = %d\n",auto_nbloop);
		  error+=1; }
		else auto_nbloop=(int)x;
	if (load_parameter(filename,"nb_save",&x) !=1) 
		{ win_printf("error loading parameter nb_save, using default value = %d\n",auto_nb_save);
		  error+=1; }
		else auto_nb_save=(int)x;

	return(error); /* error is the number of unread parameters */
}




/************************************************************************/
/* read values of the parameters of integration scheme from a file	*/
/************************************************************************/
int numerics_extra_load_parameters(char *filename)
{	float 	x;
	int	error=0;

	// extern	int verbosity, auto_go_on, do_save_st, do_GC_tracing, do_lyaps;

	if (load_parameter(filename,"verbosity",&x) !=1) 
		{ win_printf("verbosity assigned default value = %d\n",verbosity); }
		else verbosity=(int)x;

	if (load_parameter(filename,"integration_function",&x) !=1) 
		{ win_printf("Equation and method not defined!\n"
				"(use variable 'integration_function' in parameters file)\nexiting...\n");
		  return(++error);
		}
		else integration_function=(int)x;

	if (load_parameter(filename,"go_on",&x) !=1) 
		{ win_printf("go_on couldn't be read from file, assigned default value = %d\n",auto_go_on);
		  error+=1; }
		else auto_go_on=(int)x;
	if (load_parameter(filename,"save_st",&x) ==1) 
		do_save_st=(int)x;

	if (load_parameter(filename,"do_GC_tracing",&x) !=1) 
		win_printf("do_GC_tracing assigned default value = %d\n",do_GC_tracing);
		else do_GC_tracing=(int)x;

	return(error); /* error is the number of unread parameters */
}





/************************************************************************/
/* read values of the parameters of Lyaps computation from a file	*/
/************************************************************************/
int lyaps_load_parameters(char *filename)
{	float 	x;
	int	error=0;

	if (load_parameter(filename,"do_lyaps",&x) !=1) 
		printf("do_lyaps assigned default value = %d\n",do_lyaps);
		else do_lyaps=(int)x;
	if (do_lyaps<=0) return(error);	// no need to load further parameters.

  if (load_parameter(filename,"do_lyaps_space",&x) !=1)
		printf("do_lyaps_space assigned default value = %d\n",do_lyaps_space);
		else do_lyaps_space=(int)x;

	if (load_parameter(filename,"do_lyaps_go_on",&x) !=1) 
		printf("do_lyaps_go_on assigned default value = %d\n",do_lyaps_go_on);
		else do_lyaps_go_on=(int)x;
	if (load_parameter(filename,"do_lyaps_RE_save_matrices",&x) !=1) 
		printf("do_lyaps_RE_save_matrices assigned default value = %d\n",do_lyaps_RE_save_matrices);
		else do_lyaps_RE_save_matrices=(int)x;
	if (load_parameter(filename,"do_lyaps_RE_finite_time",&x) !=1) 
		printf("do_lyaps_RE_finite_time assigned default value = %d\n",do_lyaps_RE_finite_time);
		else do_lyaps_RE_finite_time=(int)x;

	if (load_parameter(filename,"finite_time_lyaps_number",&x) !=1) 
		printf("finite_time_lyaps_number assigned default value = %d\n",finite_time_lyaps_number);
		else finite_time_lyaps_number=(int)x;
	if (load_parameter(filename,"do_lyaps_RE_finite_time_reset",&x) !=1) 
		printf("do_lyaps_RE_finite_time_reset assigned default value = %d\n",do_lyaps_RE_finite_time_reset);
		else do_lyaps_RE_finite_time_reset=(int)x;
	if (load_parameter(filename,"do_lyaps_RE_continuity_vectors",&x) !=1)
		printf("do_lyaps_RE_continuity_vectors assigned default value = %d\n",do_lyaps_RE_continuity_vectors);
		else do_lyaps_RE_finite_time_reset=(int)x;

	return(error); // error is the number of unread parameters
}



/************************************************************************/
/* read values of the additional parameters used in CGL5 from a file	*/
/************************************************************************/
int CGL_CGL5_load_parameters(char *filename)
{	float 	x;
	int	error=0;

	if (load_parameter(filename,"sign",&x) !=1) 
		{ printf("error loading parameter sign, using default value = %d\n",auto_sign);
		  error+=1; }
		else auto_sign=(int)x;
	if (load_parameter(filename,"hr",&auto_hr) !=1) 
		{ printf("error loading parameter hr, using default value = %f\n",auto_hr);
		  error+=1; }
	if (load_parameter(filename,"hi",&auto_hi) !=1) 
		{ printf("error loading parameter hi, using default value = %f\n",auto_hi);
		  error+=1; }

	return(error); /* error is the number of unread parameters */
}


#ifndef XVIN
/************************************************************************/
/* printf the values of the parameters used in the integration		*/
/************************************************************************/
void scheme_print_parameters(char *filename)
{
	printf_and_log_in_file(filename, "nx : %d     dx : %8.5f  (L : %6.3f)", auto_nx, auto_L/auto_nx, auto_L );
	printf_and_log_in_file(filename, "dt : %8.5f  nbpas : %d", 		auto_dt, auto_nbpas);
	printf_and_log_in_file(filename, "ny : %d     nbloop : %d   nb_save : %d", auto_ny, auto_nbloop, auto_nb_save);
	return;
}



/************************************************************************/
/* printf the values of the parameters used in the integration		*/
/************************************************************************/
void CGL_print_parameters(char *filename)
{
	printf_and_log_in_file(filename, "\nCGL equation:");
	printf_and_log_in_file(filename, "L  : %6.3f  ", auto_L);
	printf_and_log_in_file(filename, "vg : %6.3f  epsilon : %5.2f", auto_vg,auto_epsilon);
	printf_and_log_in_file(filename, "c0 : %6.3f  c1 : %6.3f  c2 : %6.3f",auto_c0,auto_c1,auto_c2);
	printf_and_log_in_file(filename, "hr : %6.3f  hi : %6.3f  sign : %d", auto_hr,auto_hi,auto_sign);
	return;
}


/************************************************************************/
/* printf the values of the parameters used in the integration		*/
/************************************************************************/
void code_print_parameters(char *filename)
{
	printf_and_log_in_file(filename, "\nSelected options for the run:");
	printf_and_log_in_file(filename, "verbosity     %d \t integration_function %d", verbosity, integration_function);
	printf_and_log_in_file(filename, "go_on         %d \t save_st              %d", auto_go_on, do_save_st);
	printf_and_log_in_file(filename, "do_GC_tracing %d", 			do_GC_tracing);
	printf_and_log_in_file(filename, "do_lyaps      %d \t do_lyaps_space       %d", do_lyaps, do_lyaps_space); 
	printf_and_log_in_file(filename, "do_lyaps_go_on                  %d",	do_lyaps_go_on);
	printf_and_log_in_file(filename, "do_lyaps_RE_save_matrices       %d",	do_lyaps_RE_save_matrices);
	printf_and_log_in_file(filename, "do_lyaps_RE_finite_time         %d",	do_lyaps_RE_finite_time);
	printf_and_log_in_file(filename, "do_lyaps_RE_finite_time_reset   %d",	do_lyaps_RE_finite_time_reset);
	printf_and_log_in_file(filename, "finite_time_lyaps_number        %d",	finite_time_lyaps_number);
	return;
}

/************************************************************************/
/* concatenate two chains of char : name=prefix+name			*/
/* (like strcat, but output is in name instead of prefix)		*/
/************************************************************************/
size_t add_prefix(char *prefix, char *name)
{	size_t	pl, nl;
	int 	i;
	
	pl = strlen(prefix);
	nl = strlen(name);

	for (i=(int)nl; i>=0; i--)	name[i+pl] = name[i];
	memcpy(name, prefix, pl*sizeof(char));

	return(pl+nl);
}
#endif
