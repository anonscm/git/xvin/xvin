#ifndef _STC_TOOLS_C_
#define _STC_TOOLS_C_

int f_to_d(float *in, double *out, int N);
size_t add_prefix(char *prefix, char *name);

#endif
