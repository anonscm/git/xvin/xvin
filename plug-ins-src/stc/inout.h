/************************************************************************/
/* input/output functions for numerical simulations			*/
/*									*/
/* version 1.0.0 02/05/2003 from cgl5_fd.c (the CGL5 finite difference)	*/
/************************************************************************/
/* Nicolas Garnier	nicolas.garnier@ens-lyon.fr			*/
/************************************************************************/



/************************************************************************/
/* Procedures :								*/
/************************************************************************/
size_t	load_dataset_f  (char *filename, float  **x1);			/* load floats in a binary file of floats	*/
size_t	load_dataset_f2d(char *filename, double **x1);			/* load doubles from a binary file of floats	*/
size_t	load_dataset_d  (char *filename, double **x1);
size_t 	save_dataset_f  (char *filename, float  *x1, size_t N); 		/* save floats in a binary file of floats	*/
size_t 	save_dataset_d2f(char *filename, double *x1, size_t N); 		/* save doubles in a binary file of floats 	*/
size_t 	save_dataset_d  (char *filename, double *x1, size_t N);
int 	save_dataset_ascii(char *filename, float *x1, int N);		/* save floats in a ascii file  		*/
size_t 	append_dataset_f  (char *filename, float  *x1, size_t N);
size_t 	append_dataset_d2f(char *filename, double *x1, size_t N);		/* append doubles in a binary files, saving them as floats */
size_t	append_dataset_d  (char *filename, double *x1, size_t N);


int	load_parameter(char *filename, char *param, float *x);		/* load a parameter (number) from a text file */
int	load_parameter_string(char *filename, char *param, char *x);	/* load a parameter (string) from a text file */

int	log_in_file(char *filename, const char *fmt, ...);		/* prints a line of text in a log file	*/
int	printf_and_log_in_file(char *filename, const char *fmt, ...);	/* prints a line of text on the console and in the file at the same time */

double 	*float2double(float  *Af, int n);				/* converts floats into doubles		*/
float  	*double2float(double *Ad, int n);				/* converts doubles into floats		*/



