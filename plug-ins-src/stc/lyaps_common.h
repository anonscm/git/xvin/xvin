/************************************************************************/
/* numerical computation of Lyapunov'exponents				*/
/* general definitions							*/
/*									*/
/************************************************************************/
/* version 1.0.0   06/02/2003 with matrix method			*/
/* version 2.0.0   10/06/2003 with Benettin method			*/
/* version 3.0.0   11/15/2004 with Ruelle-Eckmann method		*/
/************************************************************************/
/* Nicolas Garnier	nicolas.garnier@ens-lyon.fr			*/
/*			nicolasgarnier@free.fr				*/
/************************************************************************/


// #include <f2c.h>
// #include <cblas.h>
// #include <clapack.h>

#include "CGL_parameters.h"	// parameters of the equation to study

#define IS_COMPLEX 1
#define IS_REAL 2

#define DO_LYAPS_USING_MATRIX	1
#define DO_LYAPS_USING_BENETTIN 2
#define DO_LYAPS_USING_RUELLE	4

#define LYAPS_CGL3			1
#define LYAPS_CGL3_FOURIER	4
#define LYAPS_LORENZ		8

// definitions for selecting what Lyaps are computed:
#define	LYAPS_IN_REAL_SPACE      0
#define LYAPS_IN_FOURIER_SPACE   1

#define small_thing 1.e-10
#define medium_thing 1.e-7

#ifndef M_PI 
#define M_PI 3.1415926535897932384626433832795
#endif

/****************************************************************************************/
extern 	int 	verbosity;

			


/************************************************************************/
/* Procedures :								*/
/************************************************************************/

/* for writing matrices corresponding to linear evolution:		*/
int CGL_Lyapunov_fill_matrix_regular         (double *A, double dx, double *Mat);
int CGL_Lyapunov_fill_matrix_linear_only     (double *A, double dx, double *Mat);
int CGL_Lyapunov_fill_matrix_Laplacian_order3(double *A, double dx, double *Mat);
int CGL_Lyapunov_fill_matrix_Fourier         (double *A, double L,  double *Mat);

/* of general interest:							*/
int matrix_transpose(double *M, int n);
double matrix_compute_norm(double *M, int n);
int print_matrix(double *M, int n, int k);
