/**********************************************************************/
/* Plug-in for analyse of numerical simulations of pde, and lyaps	*/
/**********************************************************************/
#ifndef _STC_IMAGE_C_
#define _STC_IMAGE_C_

#include "allegro.h"
#include "xvin.h"

/* If you include other plug-ins header do it here*/ 
#include "../inout/inout.h"
	
/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "stc.h"
#include "stc_image.h"
#include "CGL_parameters.h"
#include "CGL_dissip.h"

int do_plot_st_data(void)
{   register int i, j;
	O_i	     *oid= NULL;
	imreg 	*imr = NULL;
    char 	fullfilename[512];
	char	filename[256], pathname[512];
	float	*tampon_float=NULL;
	union  pix *pd;
	float  x;
	int    index, n, nxl, nx=auto_nx, ny=auto_ny; /* nx and ny are the expected dimensions, not the actual dimensions ! */
	int    bool_decimate=0, n_decimate=1;
	
	if (updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine plots spatio-temporal data\n"
					"a new image is created");
	}
	if (ac_grep(cur_ac_reg,"%imr",&imr) != 1)		return(win_printf_OK("No image region!"));
	
	if (index & STC_IMAGE_ST_DATA)
	{  load_parameter(simulation_parameters_fullfilename, "save_st", &x);
	   if ((int)x<1) 
       {  i=win_printf("From the parameters file,\n"
                          "spatio-temporal data wasn't saved\n\n"
                          "click OK to go on, or CANCEL to exit.");
          if (i==CANCEL) return(D_O_K);
       }
	   i=load_parameter_string(simulation_parameters_fullfilename, "st_filename", fullfilename);
	   nx = 2*auto_nx;             // expected, 2 is for complex pts
       ny = auto_ny*auto_nb_save; // expected
    }
	else if ( (index & STC_IMAGE_LYAPS_RE) 
           || (index & STC_IMAGE_LYAPS_RE_INC) 
           || (index & STC_IMAGE_LYAPS_RE_LOCAL))
	{  load_parameter(simulation_parameters_fullfilename, "do_lyaps", &x);
	   if ((int)x<1) 
       {  i=win_printf("From the parameters file,\n"
                          "Lyapunov exponents were not computed\n\n"
                          "click OK to go on, or CANCEL to exit.");
          if (i==CANCEL) return(D_O_K);
       }
       if (index & STC_IMAGE_LYAPS_RE)
       {    sprintf(fullfilename,"lyaps_RE.dat");
            nx = 2*auto_nx; // 2 lyaps per spatial point
            ny = auto_nbloop;
       }
       else if (index & STC_IMAGE_LYAPS_RE_INC)
       {    sprintf(fullfilename,"lyaps_RE_inc.dat");
            nx = 2*auto_nx; // 2 lyaps per spatial point
            ny = auto_nbloop;
       }
       else if (index & STC_IMAGE_LYAPS_RE_LOCAL)
	   {    sprintf(fullfilename,"lyaps_RE_inc.dat");
	        load_parameter(simulation_parameters_fullfilename, "finite_time_lyaps_number", &x);
	        nx = (int)x*2;
            ny = auto_nb_save;
       }
    }
    else return(D_O_K);
    		
// here, we add the prefix (run0_) to the filename:
    (void)add_prefix(simulation_filename_prefix, fullfilename);

// 	if ( file_select_ex("Load raw image", fullfilename, "", 512, 0, 0) == 0) return D_O_K;

    n=get_data_length(fullfilename);
	if (n<1) return(win_printf_OK("error opening file %s", filename));
	if (n!=nx*ny) 
    {  i=win_printf("%d pts loaded, I was expecting %d pts!\n\n"
                        "cf: nx = %d pts  / ny = %d pts\n"
                        "  nloop = %d pts / nsave = %d pts\n\n"
                        "click OK to continue, and CANCEL to abort",
                              n, nx*ny, auto_nx, auto_ny, auto_nbloop, auto_nb_save);
	   if (i==CANCEL) 
       {  free(tampon_float);
          return(D_O_K);
       }
    }
    if (n>2e6) 
    {  n_decimate = (int)n/1e6;
       i=win_scanf("there are more than 2 millions points\n"
                          "do you want to decimate ? (CANCEL if no)"
                          "keep 1 point every %4d ones in time", &n_decimate);
       if (i!=CANCEL)
       {  bool_decimate=1;
          if (bool_decimate<1) return(win_printf_OK("Hey! decimate by at least 1!!!"));
       }
    }
    if (index & STC_IMAGE_ST_DATA)
         oid = create_and_attach_oi_to_imr(imr, auto_nx, n/(2*auto_nx)/n_decimate, IS_COMPLEX_IMAGE);
    else oid = create_and_attach_oi_to_imr(imr, nx,      n/nx/n_decimate,          IS_FLOAT_IMAGE);
	if (oid == NULL)	return(win_printf_OK("cannot create destination image!"));
	pd = oid->im.pixel;	

    if (index & STC_IMAGE_ST_DATA)
	{  for (i=0; i<(n/(2*auto_nx*n_decimate)); i++)
	   {	nxl = load_data_chunk(fullfilename, &tampon_float, 2*auto_nx, i*2*auto_nx*n_decimate);
	        memcpy (pd[i].fl, tampon_float, 2*auto_nx*sizeof(float)); 

       }
       set_unit_offset   (oid->yu[0], 0.);
       set_unit_increment(oid->yu[0], real_dt*n_decimate);
       set_oi_y_unit_set (oid, 0);
         
       set_unit_offset   (oid->xu[0], 0.);
       set_unit_increment(oid->xu[0], auto_L/(float)auto_nx);
       set_oi_x_unit_set (oid, 0);
       
   //   set_im_x_title      (oid, "x (points)");
       set_im_x_top_title  (oid, "x");
       set_im_y_right_title(oid, "t");
   //   set_im_y_title      (oid, "t (points)");

       set_oi_source(oid, "st data from %s",  filename);
	   set_im_title (oid, "A(x,t) from %s", filename);
       oid->im.mode = AMP;
    }
    if ( (index & STC_IMAGE_LYAPS_RE)
      || (index & STC_IMAGE_LYAPS_RE_INC)
      || (index & STC_IMAGE_LYAPS_RE_LOCAL) )
	{  for (i=0; i<(n/(nx*n_decimate)); i++)
	   {	nxl = load_data_chunk(fullfilename, &tampon_float, nx, i*nx*n_decimate);
            for (j=0; j<nx; j++)
	   	    {	pd[i].fl[j]   = tampon_float[j];
	        }
       }
       set_unit_offset   (oid->yu[0], 0.);
       set_unit_increment(oid->yu[0], real_dt*auto_ny*n_decimate);
       set_oi_y_unit_set (oid, 0);
         
       set_im_x_title      (oid, "lyap number i");
       set_im_y_right_title(oid, "t");
   //   set_im_y_title      (oid, "t (points)");

       if (index & STC_IMAGE_LYAPS_RE)
       {    set_oi_source(oid, "lyaps RE from %s", filename);
	        set_im_title (oid, "lyaps RE from %s", filename);
       }
       else if (index & STC_IMAGE_LYAPS_RE_INC)
       {    set_oi_source(oid, "lyaps RE increments from %s", filename);
	        set_im_title (oid, "lyaps RE increments from %s", filename);
       }
       else if (index & STC_IMAGE_LYAPS_RE_LOCAL)
	   {    set_oi_source(oid, "lyaps RE local from %s", filename);
	        set_im_title (oid, "lyaps RE local from %s", filename);
       }
    }
	
    free(tampon_float);	
	
	extract_file_name( filename, 256, fullfilename); oid->filename = strdup(filename);
	extract_file_path( pathname, 512, fullfilename); oid->dir 	 = Mystrdup(pathname);		
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}



MENU *stc_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
	add_item_to_menu(mn,"Load simulation parameters",		do_load_simulation_parameters,	NULL, 0, NULL);
	add_item_to_menu(mn,"Edit simulation parameters",		do_edit_simulation_parameters,	NULL, 0, NULL);
	add_item_to_menu(mn,"Print simulation infos",		    do_print_simulation_infos,		NULL, 0, NULL);
	add_item_to_menu(mn,"Set names of files to look at",	do_set_filenames,				NULL, 0, NULL);
	add_item_to_menu(mn, "\0", 			 	               NULL,  				NULL, 0,  NULL);
	add_item_to_menu(mn,"Plot st-data (image)",	           do_plot_st_data,		NULL, STC_IMAGE_ST_DATA,      NULL);
	add_item_to_menu(mn,"Plot lyaps",	                   do_plot_st_data,		NULL, STC_IMAGE_LYAPS_RE,     NULL);
	add_item_to_menu(mn,"Plot lyaps increments",	       do_plot_st_data,		NULL, STC_IMAGE_LYAPS_RE_INC, NULL);
	add_item_to_menu(mn,"Plot lyaps local",	               do_plot_st_data,		NULL, STC_IMAGE_LYAPS_RE_LOCAL, NULL);
	add_item_to_menu(mn, "\0", 			 	               NULL,  				NULL, 0,                      NULL);
	add_item_to_menu(mn, "Load raw image", 		           do_inout_load_raw_image, NULL, 0,                  NULL);

	return mn;
}


#endif
