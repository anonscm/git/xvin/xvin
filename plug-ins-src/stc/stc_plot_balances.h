#ifndef _STC_PLOT_BALANCE_H_
#define _STC_PLOT_BALANCE_H_

/* plots balance terms for evolution of rho					      */
int do_plot_balance_rho(void);
/* plots balance terms for evolution of u					      */
int do_plot_balance_u(void);
/* plots balance terms for evolution of k=|v|^2				      */
int do_plot_balance_k(void);
/* plots balance terms for evolution of e1 (RGL lyapunov functional)   */
int do_plot_balance_e1(void);
/* plots balance terms for evolution of e2 (NLS conserved quantity)    */
int do_plot_balance_e2(void);

int stc_substract_mean_integrate(void);

#endif
