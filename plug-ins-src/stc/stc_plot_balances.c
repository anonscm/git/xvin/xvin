#ifndef _STC_PLOT_BALANCE_C_
#define _STC_PLOT_BALANCE_C_

#include "allegro.h"
#include "xvin.h"
#include "xvin/xv_tools_lib.h"

/* If you include other plug-ins header do it here	*/
#include "../inout/inout.h"
#include "../diff/diff.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "stc.h"
#include "stc_plot.h"
#include "stc_tools.h"
#include "CGL_parameters.h"
#include "CGL_dissip.h"

#include "stc_plot_balances.h"


/**********************************************************************/
/* plots balance terms for evolution of rho					      */
/* NG, 2006/11/09, Warsaw								      */
/**********************************************************************/
int do_plot_balance_rho(void)
{	register int j;
	O_p 	*op1=NULL, *op2=NULL;
	int		n=0, nx;
	d_s 		*ds2, *ds1, *ds_product=NULL, *ds_destruct=NULL;
	pltreg	*pr = NULL;
	float 	*x=NULL, *y=NULL, *drho=NULL;
	
	if(updating_menu_state != 0)	return D_O_K;	

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine plots several .dat files\n"
					"associated with the evolution equation for \\rho.\n");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");

// here, we add the prefix (run0_) to the filenames; this has to be done once only :
	if (is_first_load_dissipation==1) { GC_init(auto_ny, simulation_filename_prefix); is_first_load_dissipation=0; }
	
// first dataset:	
	op2 = create_and_attach_one_plot_from_datafile(pr, time_data_rho_mean_filename, "<\\rho>");
	nx=op2->dat[0]->nx; // nb of pts loaded
	if (nx!=real_npts) if (win_printf("nb of points in file of <\\rho> is %d"
					"which is different from expected %d", nx, real_npts)==CANCEL) return(D_O_K);
// second dataset : drho/dt
	n=load_data_bin(time_data_rho_mean_filename, &x);
	drho = (float*)calloc(n, sizeof(float));
	nx= diff_splines_float(op2->dat[0]->xd, x, n, drho);
	stc_check_for_data_size(n, "(splines diff of \\rho)");
	if ((ds2 = create_and_attach_one_ds(op2, nx, nx, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "d\\rho /dt");
	for (j=0; j<nx; j++) 
	{	ds2->xd[j] = (float)j*real_dt;
		ds2->yd[j] = drho[j]; 
	}
	free(drho);
// 3 dataset : Production of rho
	if ((ds2 = create_and_attach_one_ds(op2, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "P_\\rho");
	for (j=0; j<n; j++) 
	{	ds2->xd[j] = (float)j*real_dt;
		ds2->yd[j] = (float)2.*auto_epsilon*x[j]; // x was filled just above; it contains rho
	}
	free(x); ds_product=ds2;
// 4 dataset : destruction of rho
	n=load_data_bin(time_data_v2_mean_filename, &x); 
	nx=load_data_bin(time_data_rho2_mean_filename, &y); 
	if (n!=nx) return(win_printf_OK("some data don't have the same size... (|v|^2 and \\rho^2)"));
	if ((ds2 = create_and_attach_one_ds(op2, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "D_\\rho");
	for (j=0; j<n; j++) 
	{	ds2->xd[j] = (float)j*real_dt;
		ds2->yd[j] = (float)2.*(x[j]+y[j]); 
	}
	free(x); free(y); ds_destruct=ds2;
// 5 dataset : production - destruction of rho
	if (ds_product->nx!=ds_destruct->nx) return(win_printf_OK("P_\\rho has %d pts but D_\\rho has %d pts\n"
										"I cannot substract them.", ds_product->nx, ds_destruct->nx));
	if ((ds2 = create_and_attach_one_ds(op2, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "P_\\rho - D_\\rho");
	for (j=0; j<n; j++) 
	{	ds2->xd[j] = ds_product->xd[j];
		ds2->yd[j] = ds_product->yd[j] - ds_destruct->yd[j];
	}

// cosmetics:
	set_plot_title(op2,"Balance eq. for <\\rho>");
	set_plot_x_title(op2, "\\color{white}time (%d pts, dt=%g)", real_npts, real_dt);
	set_plot_y_title(op2, "\\color{white}\\rho, d\\rho/dt, P_\\rho, D_\\rho");
	set_op_filename(op2, "%sbalance_rho.gr", simulation_filename_prefix);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
}
/* end of 'do_plot_balance_rho' */








/**********************************************************************/
/* plots balance terms for evolution of u					      */
/* NG, 2006/11/10, Warsaw								      */
/**********************************************************************/
int do_plot_balance_u(void)
{	register int j;
	O_p 		*op1=NULL, *op2=NULL;
	int		n=0, ny=0;
	d_s 		*ds2, *ds1, *ds_product=NULL, *ds_destruct=NULL, *ds_diff, *ds_P_D;
	pltreg	*pr = NULL;
	float 	*x=NULL, *y=NULL, *z=NULL, *du=NULL;
//	char 	*s=NULL;
	
	if(updating_menu_state != 0)	return D_O_K;	

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine plots several .dat files\n"
					"associated with the evolution equation for \\rho.\n");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");

// here, we add the prefix (run0_) to the filenames; this has to be done once only :
	if (is_first_load_dissipation==1) { GC_init(auto_ny, simulation_filename_prefix); is_first_load_dissipation=0; }
	
// first dataset: u = 1/2 \rho^2	
	op2 = create_and_attach_one_plot_from_datafile(pr, time_data_rho2_mean_filename, "u = 1/2<\\rho^2>");
	for (j=0; j<op2->dat[0]->nx; j++) { op2->dat[0]->yd[j] /= 2.; } // u is half rho^2
	
// second dataset : du/dt
	n=load_data_bin(time_data_rho2_mean_filename, &x); // this is rho^2 (so 2*u)
	du = (float*)calloc(n, sizeof(float));
	ny = diff_splines_float(op2->dat[0]->xd, x, n, du);
	stc_check_for_data_size(n, "(splines diff of u)");
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "du/dt");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = (float)j*real_dt;
		ds2->yd[j] = du[j]/2.; 
	}
	free(du);
	ds_diff=ds2;
// 3 dataset : Production of u
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "P_u");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = (float)j*real_dt;
		ds2->yd[j] = (float)2.*auto_epsilon*x[j]; 
	}
	free(x); ds_product=ds2;
// 4 dataset : destruction of u
	n=load_data_bin(time_data_rhov2_mean_filename, &x); 
	ny=load_data_bin(time_data_rho3_mean_filename, &y); 
	if (n!=ny) return(win_printf_OK("some data don't have the same size... (\\rho|v|^2 and \\rho^3)"));
	n=load_data_bin(time_data_gradrho_mean_filename, &z); 
	if (n!=ny) return(win_printf_OK("some data don't have the same size... (<|\\partial_x \\rho|^2> and <\\rho^3>)"));
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "D_u");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = (float)j*real_dt;
		ds2->yd[j] = (float)2.*(x[j]+y[j])+z[j]; 
	}
	free(x); free(y); free(z); ds_destruct=ds2;
// 5 dataset : production - destruction of u
	if (ds_product->nx!=ds_destruct->nx) return(win_printf_OK("some data don't have the same size... (P_u and D_u)"));
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "P_u-D_u");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = ds_product->xd[j];
		ds2->yd[j] = ds_product->yd[j] - ds_destruct->yd[j];
	}
	ds_P_D=ds2;
	set_ds_plot_label(ds2, ds2->xd[ny*1/4], ds2->yd[ny*1/4], USR_COORD, "{\\color{%d}P_u-D_u}", get_ds_line_color(ds2));
// 6 dataset : funny term c1*<d_2> = du/dt - (P_u-D_u)
	if (ds_diff->nx!=ds_P_D->nx) return(win_printf_OK("dataset with du/dt asn't the same nb of pts as dataset with (P_u-D_u)"));
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "du/dt - (P_u-D_u)");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = ds_diff->xd[j];
		ds2->yd[j] = ds_diff->yd[j] - ds_P_D->yd[j];
	}
	set_ds_plot_label(ds2, ds2->xd[ny*3/4], ds2->yd[ny*3/4], USR_COORD, 
				"{\\color{%d}\\frac{\\partial u}{\\partial t}-(P_u-D_u)=c_1<d_2> ?}", get_ds_line_color(ds2));
	

// cosmetics:
	set_plot_title(op2,"Balance eq. for <u>=\\frac{1}{2}<\\rho^2>");
	set_plot_x_title(op2, "\\color{white}time (%d pts, dt=%g)", real_npts, real_dt);
	set_plot_y_title(op2, "\\color{white}u, du/dt, P_u, D_u");
	set_op_filename(op2, "%sbalance_u.gr", simulation_filename_prefix);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
}
/* end of 'do_plot_balance_u' */






/**********************************************************************/
/* plots balance terms for evolution of k=|v|^2				      */
/* NG, 2006/11/09, Warsaw								      */
/**********************************************************************/
int do_plot_balance_k(void)
{	register int j;
	O_p 		*op1=NULL, *op2=NULL;
	int		n=0, ny=0;
	d_s 		*ds2, *ds1, *ds_product=NULL, *ds_destruct=NULL, *ds_diff, *ds_P_D;
	pltreg	*pr = NULL;
	float 	*x=NULL, *y=NULL, *z=NULL, *dk=NULL;
//	char 	*s=NULL;
	
	if(updating_menu_state != 0)	return D_O_K;	

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine plots several .dat files\n"
					"associated with the evolution equation of <k> = <|v|^2>.\n");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");

// here, we add the prefix (run0_) to the filenames; this has to be done once only :
	if (is_first_load_dissipation==1) { GC_init(auto_ny, simulation_filename_prefix); is_first_load_dissipation=0; }
	
// first dataset: k = v^2	
	op2 = create_and_attach_one_plot_from_datafile(pr, time_data_v2_mean_filename, "k = <|v|^2>");
	
// second dataset : dk/dt
	n=load_data_bin(time_data_v2_mean_filename, &x);
	dk = (float*)calloc(n, sizeof(float));
	ny = diff_splines_float(op2->dat[0]->xd, x, n, dk);
	stc_check_for_data_size(n, "(splines diff of k)");
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "dk/dt");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = (float)j*real_dt;
		ds2->yd[j] = dk[j]; 
	}
	free(dk);
	ds_diff=ds2;
// 3 dataset : Production of k
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "P_k");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = (float)j*real_dt;
		ds2->yd[j] = (float)2.*auto_epsilon*x[j]; 
	}
	free(x); ds_product=ds2;
// 4 dataset : destruction of k
	n=load_data_bin(time_data_rhov2_mean_filename, &x); 
	ny=load_data_bin(time_data_tTT_mean_filename, &y); 
	if (n!=ny) return(win_printf_OK("some data don't have the same size... (\\rho|v|^2 and tr(T^+T)"));
	n=load_data_bin(time_data_gradrho_mean_filename, &z); 
	if (n!=ny) return(win_printf_OK("some data don't have the same size... (<|\\partial_x \\rho|^2> and <tr(T^+T)>)"));
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "D_k");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = (float)j*real_dt;
		ds2->yd[j] = (float)2.*(x[j]+y[j])+z[j]; 
	}
	free(x); free(y); free(z); ds_destruct=ds2;
// 5 dataset : production - destruction of k
	if (ds_product->nx!=ds_destruct->nx) return(win_printf_OK("some data don't have the same size... (P_k and D_k)"));
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "P_k-D_k");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = ds_product->xd[j];
		ds2->yd[j] = ds_product->yd[j] - ds_destruct->yd[j];
	}
	ds_P_D=ds2;
	set_ds_plot_label(ds2, ds2->xd[ny*1/4], ds2->yd[ny*1/4], USR_COORD, "{\\color{%d}P_k-D_k}", get_ds_line_color(ds2));
// 6 dataset : funny term c2*<d_2> = dk/dt - (P_k-D_k)
	if (ds_diff->nx!=ds_P_D->nx) return(win_printf_OK("dataset with dk/dt hasn't the same nb of pts as dataset with (P_k-D_k)"));
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "dk/dt - (P_k-D_k)");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = ds_diff->xd[j];
		ds2->yd[j] = ds_diff->yd[j] - ds_P_D->yd[j];
	}
	set_ds_plot_label(ds2, ds2->xd[ny*3/4], ds2->yd[ny*3/4], USR_COORD, 
				"{\\color{%d}\\frac{\\partial k}{\\partial t}-(P_k-D_k)=-c_2<d_2> ?}", get_ds_line_color(ds2));
	

// cosmetics:
	set_plot_title(op2,"Balance eq. for <k>=<|v|^2> (v=\\frac{\\partial A}{\\partial x}");
	set_plot_x_title(op2, "\\color{white}time (%d pts, dt=%g)", real_npts, real_dt);
	set_plot_y_title(op2, "\\color{white}k, dk/dt, P_k, D_k");
	set_op_filename(op2, "%sbalance_k.gr", simulation_filename_prefix);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
}
/* end of 'do_plot_balance_k' */




/**********************************************************************/
/* plots balance terms for evolution of e1 (RGL lyapunov functional)   */
/* NG, 2006/13/09, Warsaw								      */
/**********************************************************************/
int do_plot_balance_e1(void)
{	register int j;
	O_p 		*op1=NULL, *op2=NULL;
	int		n=0, ny=0;
	d_s 		*ds2, *ds1, *ds_product=NULL, *ds_destruct=NULL, *ds_diff, *ds_P_D;
	pltreg	*pr = NULL;
	float 	*x=NULL, *y=NULL, *de=NULL;
//	char 	*s=NULL;
	
	if(updating_menu_state != 0)	return D_O_K;	

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine plots several .dat files\n"
					"associated with the evolution equation of <e_1> = <\\frac{1}{2}\\rho^2 + |v|^2>.\n");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");

// here, we add the prefix (run0_) to the filenames; this has to be done once only :
	if (is_first_load_dissipation==1) { GC_init(auto_ny, simulation_filename_prefix); is_first_load_dissipation=0; }
	
// first dataset: e1	
	op2 = create_and_attach_one_plot_from_datafile(pr, time_data_e1_mean_filename, "e_1 = <\\frac{1}{2}\\rho^2 + |v|^2>");
	
// second dataset : de1/dt
	n=load_data_bin(time_data_e1_mean_filename, &x);
	de = (float*)calloc(n, sizeof(float));
	ny = diff_splines_float(op2->dat[0]->xd, x, n, de);
	stc_check_for_data_size(n, "(splines diff of e_1)");
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "de_1/dt");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = (float)j*real_dt;
		ds2->yd[j] = de[j]; 
	}
	free(de); free(x);
	ds_diff=ds2;
// 3 dataset : Production of e1
	n=load_data_bin(time_data_rho2_mean_filename, &x);
	stc_check_for_data_size(n, "\\rho^2");
	n=load_data_bin(time_data_v2_mean_filename, &y);
	stc_check_for_data_size(n, "v^2");
	if ((ds2 = create_and_attach_one_ds(op2, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "P_{e1}");
	for (j=0; j<n; j++) 
	{	ds2->xd[j] = (float)j*real_dt;
		ds2->yd[j] = (float)(x[j]+y[j])*2.*auto_epsilon; 
	}
	free(x); free(y);
	ds_product=ds2;
// 4 dataset : destruction of e1
	ds_destruct = create_and_attach_one_ds_from_datafile(op2, time_data_D_e1_mean_filename, "D'_{e_1} (positive part only)");
	
// 5 dataset : production - destruction of k
	if (ds_product->nx!=ds_destruct->nx) return(win_printf_OK("some data don't have the same size... (P_{e1} and D'_{e1})"));
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "P_{e1}-D'_{e1}");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = ds_product->xd[j];
		ds2->yd[j] = ds_product->yd[j] - ds_destruct->yd[j];
	}
	ds_P_D=ds2;
	set_ds_plot_label(ds2, ds2->xd[ny*1/4], ds2->yd[ny*1/4], USR_COORD, "{\\color{%d}P_{e1}-D'_{e1}}", get_ds_line_color(ds2));
// 6 dataset : funny term c2*<d_2> = de1/dt - (P_k-D'_k)
	if (ds_diff->nx!=ds_P_D->nx) return(win_printf_OK("dataset with de_1/dt hasn't the same nb of pts as dataset with (P_{e1}-D_{e1})"));
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "de_1/dt - (P_{e1}-D'_{e1})");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = ds_diff->xd[j];
		ds2->yd[j] = ds_diff->yd[j] - ds_P_D->yd[j];
	}
	set_ds_plot_label(ds2, ds2->xd[ny*3/4], ds2->yd[ny*3/4], USR_COORD, 
				"{\\color{%d}\\frac{\\partial e_1}{\\partial t}-(P_{e1}-D_{e1})= ?}", get_ds_line_color(ds2));
// 7 dataset : funny term (c2-c1)*<d_2> should be equal to = de1/dt - (P_k-D'_k)
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_d2_mean_filename, "(c_1-c_2)<d_2>");
	for (j=0; j<ds2->nx; j++) 
	{	ds2->yd[j] *= (auto_c1-auto_c2);
	}
	

// cosmetics:
	set_plot_title(op2,"Balance eq. for <e_1>=<\\frac{1}{2}\\rho^2+|v|^2>");
	set_plot_x_title(op2, "\\color{white}time (%d pts, dt=%g)", real_npts, real_dt);
	set_plot_y_title(op2, "\\color{white}e_1, de_1/dt, P_{e1}, D'_{e1}, P-D', etc.");
	set_op_filename(op2, "%sbalance_e1.gr", simulation_filename_prefix);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
}
/* end of 'do_plot_balance_e1' */





/**********************************************************************/
/* plots balance terms for evolution of e2 (NLS conserved quantity)    */
/* NG, 2006/13/09, Warsaw								      */
/**********************************************************************/
int do_plot_balance_e2(void)
{	register int j;
	O_p 		*op1=NULL, *op2=NULL;
	int		n=0, ny=0;
	d_s 		*ds2, *ds1, *ds_product=NULL, *ds_destruct=NULL, *ds_diff, *ds_P_D;
	pltreg	*pr = NULL;
	float 	*x=NULL, *y=NULL, *de=NULL;
	
	if(updating_menu_state != 0)	return D_O_K;	

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine plots several .dat files\n"
					"associated with the evolution equation of <e_2> = <\\frac{1}{2}c_2\\rho^2 + c_1|v|^2>.\n");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");

// here, we add the prefix (run0_) to the filenames; this has to be done once only :
	if (is_first_load_dissipation==1) { GC_init(auto_ny, simulation_filename_prefix); is_first_load_dissipation=0; }
	
// first dataset: e2	
	op2 = create_and_attach_one_plot_from_datafile(pr, time_data_e2_mean_filename, "e_2 = <\\frac{1}{2}c_2\\rho^2 + c_1|v|^2>");
	
// second dataset : de2/dt
	n=load_data_bin(time_data_e2_mean_filename, &x);
	de = (float*)calloc(n, sizeof(float));
	ny = diff_splines_float(op2->dat[0]->xd, x, n, de);
	stc_check_for_data_size(n, "(splines diff of e_1)");
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "de_1/dt");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = (float)j*real_dt;
		ds2->yd[j] = de[j]; 
	}
	free(de);
	ds_diff=ds2;
// 3 dataset : Production of e2
	n=load_data_bin(time_data_rho2_mean_filename, &x);
	stc_check_for_data_size(n, "\\rho^2");
	n=load_data_bin(time_data_v2_mean_filename, &y);
	stc_check_for_data_size(n, "v^2");
	if ((ds2 = create_and_attach_one_ds(op2, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "P_{e2}");
	for (j=0; j<n; j++) 
	{	ds2->xd[j] = (float)j*real_dt;
		ds2->yd[j] = (float)(auto_c2*x[j] + auto_c1*y[j])*2.*auto_epsilon; 
	}
	free(x); free(y);
	ds_product=ds2;

// 4 dataset : destruction of e2
	ds_destruct = create_and_attach_one_ds_from_datafile(op2, time_data_D_e2_mean_filename, "D_{e_2} (complete, sign unknown)");
	
// 5 dataset : production - destruction of e2
	if (ds_product->nx!=ds_destruct->nx) return(win_printf_OK("some data don't have the same size... (P_{e2} and D_{e2})"));
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "P_{e2}-D'_{e2}");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = ds_product->xd[j];
		ds2->yd[j] = ds_product->yd[j] - ds_destruct->yd[j];
	}
	ds_P_D=ds2;
	set_ds_plot_label(ds2, ds2->xd[ny*1/4], ds2->yd[ny*1/4], USR_COORD, "{\\color{%d}P_{e2}-D_{e2}}", get_ds_line_color(ds2));
// 6 dataset : funny term : de2/dt - (P_e2-D_e2)
	if (ds_diff->nx!=ds_P_D->nx) return(win_printf_OK("dataset with de_2/dt hasn't the same nb of pts as dataset with (P_{e2}-D_{e2})"));
	if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	return(win_printf_OK("cannot create dataset !"));
	set_ds_history(ds2, "de_2/dt - (P_{e2}-D_{e2})");
	for (j=0; j<ny; j++) 
	{	ds2->xd[j] = ds_diff->xd[j];
		ds2->yd[j] = ds_diff->yd[j] - ds_P_D->yd[j];
	}
	set_ds_plot_label(ds2, ds2->xd[ny*3/4], ds2->yd[ny*3/4], USR_COORD, 
				"{\\color{%d}\\frac{\\partial e_2}{\\partial t}-(P_{e2}-D_{e2})}", get_ds_line_color(ds2));
/*
// 7 dataset : funny term (c2-c1)*<d_2> should be equal to = de1/dt - (P_k-D'_k)
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_d2_mean_filename, "(c_1-c_2)<d_2>");
	for (j=0; j<ds2->nx; j++) 
	{	ds2->yd[j] *= (auto_c1-auto_c2);
	}
*/	

// cosmetics:
	set_plot_title(op2,"Balance eq. for <e_1>=<\\frac{1}{2}\\rho^2+|v|^2>");
	set_plot_x_title(op2, "\\color{white}time (%d pts, dt=%g)", real_npts, real_dt);
	set_plot_y_title(op2, "\\color{white}e_1, de_1/dt, P_{e2}, D'_{e2}");
	set_op_filename(op2, "%sbalance_e2.gr", simulation_filename_prefix);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
}
/* end of 'do_plot_balance_e2' */



/**********************************************************************/
/* integrate after subtracting the mean to avoid a drift               */
/* NG, 2007/01/26, Lyon								      */
/**********************************************************************/
int stc_substract_mean_integrate(void)
{	register int j;
	O_p 	*op1=NULL;
	int		 n=0;
	d_s 	*ds2=NULL, *ds1;
	pltreg	*pr = NULL;
	double   m=0, dt=1.;
	
	if(updating_menu_state != 0)	return D_O_K;	
 
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine plots several .dat files\n"
					"associated with the evolution equation of <e_2> = <\\frac{1}{2}c_2\\rho^2 + c_1|v|^2>.\n");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot find data");
    n=ds1->nx;
    
    ds2 = duplicate_data_set(ds1, NULL);
    if (ds2==NULL)			return win_printf_OK("cannot create dataset !");
	
	dt = (double)(ds1->yd[n-1]-ds1->yd[0])/(double)(n-1);
	
	m=0.;
    for (j=0; j<n; j++) m += (double)ds2->yd[j];
    m /= (double)n;
    
    for (j=0; j<n; j++) ds2->yd[j] -= (float)m;
    for (j=1; j<n; j++) ds2->yd[j] *= (float)dt;
	for (j=1; j<n; j++) ds2->yd[j] += ds2->yd[j-1];
	
	inherit_from_ds_to_ds(ds2, ds1);
    set_formated_string(&ds2->treatement,"integration, after mean substraction");        
        
	return refresh_plot(pr, UNCHANGED);
}
/* end of 'stc_substract_mean_integrate' */



#endif

