/************************************************************************/
/* input/output functions for numerical simulations			*/
/*									*/
/* version 1.0.0 02/05/2003 from cgl5_fd.c (the CGL5 finite difference)	*/
/************************************************************************/
/* Nicolas Garnier	nicolas.garnier@ens-lyon.fr			*/
/************************************************************************/


#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>	// for malloc
#include "inout.h"


/************************************************************************/
/* Implementation							*/
/************************************************************************/
int load_parameter(char *filename, char *param, float *x)
/* load parameter "param" from the text file named "filename" 		*/
/* very usefull for batch-runs						*/
{	FILE 	*param_file;
	int	err=0;
	char	*ch;
	char 	c;
	int 	go_on=1;

	param_file=fopen(filename, "rt");
	if (param_file==NULL) printf("load_parameter: error opening file %s\n",filename);

	ch=(char*)calloc(128,sizeof(char));
	while ( (!feof(param_file)) && go_on)
	{	err=fscanf(param_file,"%s",ch);
		if ( (strncmp(ch,"%",1)==0) || (strncmp(ch,"#",1)==0) || (strncmp(ch,"//",2)==0) ) 
		/* commentaire detecte : on ignore toute la fin de la ligne */
		{	c=ch[strlen(ch)-1]; 
			while ( (!feof(param_file)) && (c!='\n') )
			{	c = fgetc(param_file);
			}
		}
		if (strcmp(param,ch)==0) go_on=0;
	}
	if (err==EOF) 
	{ 	fclose(param_file); 
		return err; 
	}
	err=fscanf(param_file,"%f",x);
	fclose(param_file);
	free(ch);
	return err;
}


int load_parameter_string(char *filename, char *param, char *x)
/* load parameter "param" from the text file named "filename" 		*/
/* very usefull for batch-runs						*/
{	FILE 	*param_file;
	int	err=0;
	char	*ch;
	char 	c;
	int 	go_on=1;

	param_file=fopen(filename, "rt");
	if (param_file==NULL) printf("load_parameter: error opening file %s\n",filename);

	ch=(char*)calloc(128,sizeof(char));
	while ( (!feof(param_file)) && go_on)
	{	err=fscanf(param_file,"%s",ch);
		if ( (strncmp(ch,"%",1)==0) || (strncmp(ch,"#",1)==0) || (strncmp(ch,"//",2)==0) ) 
		/* commentaire detecte : on ignore toute la fin de la ligne */
		{	c=ch[strlen(ch)-1]; 
			while ( (!feof(param_file)) && (c!='\n') )
			{	c = fgetc(param_file);
			}
		}
		if (strcmp(param,ch)==0) go_on=0;
	}
	if (err==EOF)
	{ 	fclose(param_file); 
		return err; 
	}
	err=fscanf(param_file,"%s",x);
	fclose(param_file);
	free(ch);
	return err;
}




/************************************************************************/
size_t load_dataset_f(char *filename, float **x1)
/* to load an array of float as a variable				*/
/* complex number are stored as 2 consecutive real numbers for real and */
/*	imaginary part. So a n-dimensionnal vector of complex contains  */
/*	2*n real numbers.						*/
/************************************************************************/
{	FILE 	*data_file;
	int	N;
	size_t i;
	float 	*x;
	
	data_file=fopen(filename, "rb");
	if (data_file==NULL) printf("load_data_set: error opening file %s\n",filename);
	
	x =(float*)calloc(1,sizeof(float)); 
	N=-1;
	while (!feof(data_file)) 
	{ 	i=fread(x,sizeof(float),1,data_file); 
//		if ( (i!=1) && (verbosity>2) ) 
//			printf("for the %dth element, value=%f with error %d\n",N,x[0],i);
		N++; 
	}
	free(x);
	rewind(data_file);
//	if (verbosity>1) printf("load_data_set: file %s contains %d floats.\n",filename,N);
	*x1=(float*)calloc((size_t)N,sizeof(float));
	i=fread(*x1,sizeof(float),(size_t)N,data_file);
	
	fclose(data_file);

	return i;
}


/************************************************************************/
size_t load_dataset_f2d(char *filename, double **x1)
/* to load an array of double as a variable				*/
/* complex number are stored as 2 consecutive real numbers for real and */
/*	imaginary part. So a n-dimensionnal vector of complex contains  */
/*	2*n real numbers.						*/
/************************************************************************/
/* as you can see, the last argument is a single pointer ! 		*/
/* so this is different from load_data_set() for floats !!!		*/
/************************************************************************/
{	FILE 	*data_file;
	int	N;
	size_t i;
	float 	*x;
	
	data_file=fopen(filename, "rb");
	if (data_file==NULL) printf("load_data_set_d: error opening file %s\n",filename);
	
	x =(float*)calloc(1,sizeof(float)); 
	N=-1;
	while (!feof(data_file)) 
	{ 	i=fread(x,sizeof(float),1,data_file); 
		N++; 
	}
	free(x);

	rewind(data_file);
	x=(float*)calloc((size_t)N,sizeof(float));
	i=fread(x,sizeof(float),(size_t)N,data_file);
	fclose(data_file);

	if (i!=(size_t)N) printf("load_data_set_d: number of elements loaded is %d, while expected number was %d\n",(int)i,N);

	(*x1)=float2double(x,N);
	free(x);	

	return(i);
}



/************************************************************************/
size_t load_dataset_d(char *filename, double **x1)
/* to load an array of float as a variable				*/
/* complex number are stored as 2 consecutive real numbers for real and */
/*	imaginary part. So a n-dimensionnal vector of complex contains  */
/*	2*n real numbers.						*/
/************************************************************************/
{	FILE 	*data_file;
	int	N;
	size_t i;
	double 	*x;
	
	data_file=fopen(filename, "rb");
	if (data_file==NULL) printf("load_data_set: error opening file %s\n",filename);
	
	x =(double*)calloc(1,sizeof(float)); 
	N=-1;
	while (!feof(data_file)) 
	{ 	i=fread(x,sizeof(double),1,data_file); 
		N++; 
	}
	free(x);
	rewind(data_file);
	*x1=(double*)calloc((size_t)N,sizeof(double));
	i=fread(*x1,sizeof(double),(size_t)N,data_file);
	
	fclose(data_file);
	return i;
}




/************************************************************************/
/* to save an array of float in a binary file				*/
/* see comments of load_dataset_f for more information			*/
/************************************************************************/
size_t save_dataset_f(char *filename, float *x1, size_t N)
{	FILE 	*data_file;
	size_t	err;
	
	data_file=fopen(filename, "wb");
	err=fwrite(x1,sizeof(float),(size_t)N,data_file);
//	if (verbosity>1) printf("save_data_set: file %s save with %d/%d floats.\n",filename,err,N);
	fclose(data_file);

	return err;
}


/************************************************************************/
/* to save an array of doubles as floats in a binary file		*/
/* see comments of load_dataset_f2d for more information			*/
/************************************************************************/
size_t save_dataset_d2f(char *filename, double *x1, size_t N)
{	FILE 	*data_file;
	size_t	err=0;
	register int i;
	float	*tmp;

	tmp=(float*)calloc(N,sizeof(float));
	for (i=0; i<(int)N; i++) tmp[i]=(float)(x1[i]);
	data_file=fopen(filename, "wb");
	err=fwrite(tmp,sizeof(float),N,data_file);
//	if (verbosity>1) printf("save_data_set_d: file %s save with %d/%d floats.\n",filename,err,N);
	fclose(data_file);
	free(tmp);

	return err;
}


/************************************************************************/
/* to save an array of doubles in a binary file				*/
/* see comments of save_dataset_f for more information			*/
/************************************************************************/
size_t save_dataset_d(char *filename, double *x1, size_t N)
{	FILE 	*data_file;
	size_t	err;
	
	data_file=fopen(filename, "wb");
	err=fwrite(x1,sizeof(double),(size_t)N,data_file);
//	if (verbosity>1) printf("save_data_set: file %s save with %d/%d floats.\n",filename,err,N);
	fclose(data_file);

	return err;
}




/************************************************************************/
/* to save (append mode) an array of float in a binary file		*/
/* see comments of load_data_set for more information			*/
/************************************************************************/
size_t append_dataset_f(char *filename, float *x1, size_t N)
{	FILE 	*data_file;
	size_t	err;
	
	data_file=fopen(filename, "a+b");
	err=fwrite(x1,sizeof(float),N,data_file);
//	if (verbosity>1) printf("append_data_set: file %s saved with %d/%d floats.\n",filename,err,N);
	fclose(data_file);

	return err;
}



/************************************************************************/
/* to save (append mode) an array of doubles in a binary file of floats	*/
/* see comments of load_dataset_f for more information			*/
/************************************************************************/
size_t append_dataset_d2f(char *filename, double *x1, size_t N)
{	FILE 	*data_file;
	size_t	err;
	register int i;
	float	*tmp;
	
	tmp=(float*)calloc(N,sizeof(float));
	for (i=0; i<(int)N; i++) tmp[i]=(float)(x1[i]);
	data_file=fopen(filename, "a+b");
	err=fwrite(tmp,sizeof(float),N,data_file);
//	if (verbosity>1) printf("append_data_set: file %s saved with %d/%d floats.\n",filename,err,N);
	fclose(data_file);
	free(tmp);

	return err;
}


/************************************************************************/
/* to save (append mode) an array of doubles in a binary file		*/
/* see comments of load_dataset_f for more information			*/
/************************************************************************/
size_t append_dataset_d(char *filename, double *x1, size_t N)
{	FILE 	*data_file;
	size_t	err;
	
	data_file=fopen(filename, "a+b");
	err=fwrite(x1,sizeof(double),N,data_file);
//	if (verbosity>1) printf("append_data_set: file %s saved with %d/%d floats.\n",filename,err,N);
	fclose(data_file);

	return err;
}



/************************************************************************/
/* to save an array of float in a text file (ascii)			*/
/* see comments of load_data_set for more information			*/
/************************************************************************/
int save_dataset_ascii(char *filename, float *x1, int N)
{	FILE 	*data_file;
	register int i;
	int	err=0;
	
	data_file=fopen(filename, "wt");
	for (i=0; i<N; i++)
	{	err=fprintf(data_file, "%10.8f\n",x1[i]);
	}
	fclose(data_file);

	return err;
}


/************************************************************************/
/* to print a line of text in a file 					*/
/* the first argument is the file name					*/
/* the text can be a regular composition as a C-formated string		*/
/* a \n is appended 							*/
/************************************************************************/
int	log_in_file(char *filename, const char *fmt, ...)
{	va_list ap;
	FILE *fic;
	char c[2048];
        
	if (filename == NULL)	return(1);
	if (fmt == NULL)	return(1);
    	fic = fopen(filename,"a");
	if (fic == NULL)	return(1);
	va_start(ap, fmt);
	vsprintf(c,fmt,ap);
	va_end(ap);
	fprintf(fic,"%s\n",c);        
	return fclose(fic);    
} 


/************************************************************************/
/* to print a line of text in a file 					*/
/* the first argument is the file name					*/
/* the text can be a regular composition as a C-formated string		*/
/* a \n is appended 							*/
/************************************************************************/
int	printf_and_log_in_file(char *filename, const char *fmt, ...)
{	va_list ap;
	FILE *fic;
	char c[2048];
        
	if (filename == NULL)	return(1);
	if (fmt == NULL)	return(1);
    	fic = fopen(filename,"a");
	if (fic == NULL)	return(1);
	va_start(ap, fmt);
	vsprintf(c,fmt,ap);
	va_end(ap);
	fprintf(fic,"%s\n",c); 
	printf("%s\n",c);       
	return fclose(fic);    
} 



/************************************************************************/
/* to convert a pointer on floats into a pointer on doubles		*/
/* a new pointer of doubles is returned, 				*/
/* this new pointer should not be already allocated!			*/
/************************************************************************/
double *float2double(float *Af, int n)
{	register int i;
	double *Ad;
	
	Ad=(double*)calloc((size_t)n,sizeof(double));
	for (i=0; i<n; i++) Ad[i]=(double)Af[i];
	return(Ad);
}


/************************************************************************/
/* to convert a pointer on doubles into a pointer on floats		*/
/* a new pointer of floats is returned, 				*/
/* this new pointer should not be already allocated!			*/
/************************************************************************/
float *double2float(double *Ad, int n)
{	register int i;
	float *Af;

	Af=(float*)calloc((size_t)n,sizeof(float));
	for (i=0; i<n; i++) Af[i]=(float)Ad[i];
	return(Af);
}
