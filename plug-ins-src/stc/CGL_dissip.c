/************************************************************************/
/* for computing the dissipation and other global quantities		*/
/* 									*/
/* can be applied to any integration scheme to track the evolution	*/
/* of given quantities along time						*/
/* version 2.0.0 may 9th 2003							*/
/* version 1.1.0 march 2003							*/
/* version 3.0.0 november 2006, Warsaw, with XVin inclusion	*/
/************************************************************************/
/* Nicolas Garnier	nicolas.garnier@ens-lyon.fr			*/
/************************************************************************/
#ifdef XV_WIN32
#define XVIN
#else 
#ifdef XV_UNIX
#define XVIN
#else
#ifdef XV_MAC
#define  XVIN
#endif
#endif
#endif


#include <stdio.h>
#include <stdlib.h>	// replace malloc.h
#include <string.h>	// for strlen()

#ifndef XVIN
#include "inout.h"
#else
#include "xvin.h"
#include "../inout/inout.h"
#endif

#include "CGL_parameters.h"
#include "CGL_dissip.h"


/****************************************************************************************/	
char	time_data_rho_mean_filename[128]	= "GC_rho_mean.dat",
	time_data_rho_local_filename[128]  = "GC_rho_local.dat",
	time_data_rho2_mean_filename[128]  = "GC_rho2_mean.dat",
	time_data_rho3_mean_filename[128]  = "GC_rho3_mean.dat",
	time_data_v2_mean_filename[128]  	= "GC_v2_mean.dat",
	time_data_e1_mean_filename[128]   	= "GC_e1_mean.dat",
	time_data_e2_mean_filename[128]	= "GC_e2_mean.dat",
	time_data_e1_local_filename[128]  	= "GC_e1_local.dat",
	time_data_e2_local_filename[128]  	= "GC_e2_local.dat",
	time_data_jA_local_filename[128]  	= "GC_jA_local.dat",
	time_data_jA_mean_filename[128]  	= "GC_jA_mean.dat",
	time_data_D_e1_local_filename[128] = "GC_D_e1_local.dat",
	time_data_D_e1_mean_filename[128]  = "GC_D_e1_mean.dat",
	time_data_D_e2_local_filename[128] = "GC_D_e2_local.dat",
	time_data_D_e2_mean_filename[128]  = "GC_D_e2_mean.dat",
	time_data_D_u_mean_filename[128]  	= "GC_D_u_mean.dat",
	time_data_D_k_mean_filename[128]  	= "GC_D_k_mean.dat",
	time_data_dissip_local_filename[128] 	= "GC_dissip_local.dat",
	time_data_dissip_mean_filename[128]  	= "GC_dissip_mean.dat",
	time_data_tTT_mean_filename[128]     = "GC_tTT_mean.dat",
	time_data_rhov2_mean_filename[128]     = "GC_rhov2_mean.dat",
	time_data_d1_mean_filename[128]     = "GC_d1_mean.dat",
	time_data_d2_mean_filename[128]     = "GC_d2_mean.dat",
	time_data_gradrho_mean_filename[128]     = "GC_gradrho_mean.dat";

char	st_data_dissipation_1_filename[128]	= "dissip_1_st.dat";
char	st_data_dissipation_2_filename[128]	= "dissip_2_st.dat";

/************************************************************************/
#ifndef XVIN
static double	
	*temp_rho_local, 	/* time-serie for local squared modulus of the  amplitude	*/
	*temp_rho_mean,	/* time-serie for space-averaged amplitude modulus		*/
	*temp_e1_mean,		/* time-serie for space-averaged e=1/2|A|^4 + |\nabla(A)|^2	*/
	*temp_e1_local,	/* time-serie for local e							*/
	*temp_e2_mean,
	*temp_e2_local,
	*temp_v2_mean,		/* time-serie for space-averaged |v|^2					*/
	*temp_Du_mean,		/* time-serie for destruction of u (complete 4 terms)		*/
	*temp_Dk_mean,		/* time-serie for destruction of k (complete 4 terms)		*/
	*temp_De1_local,	/* time-serie for destruction of e De					*/
	*temp_De1_mean,	/* time-serie for destruction of e De					*/
	*temp_De2_local,	/* time-serie for destruction of e_2 De_2				*/
	*temp_De2_mean,	/* time-serie for destruction of e_2 De_2				*/
	*temp_dissip_local,	/* time-serie for "dissipation term"					*/
	*temp_dissip_mean,	/* time_serie for "dissipation term 					*/
	*temp_jA_local,	/* time-serie for "flux of A" (local)					*/
	*temp_jA_mean,		/* time_serie for "flux of A" (mean) / mean flux of rho=|A|^2	*/
  	*temp_rho3_mean,
  	*temp_rho2_mean,
  	*temp_tTT_mean,
  	*temp_rhov2_mean,
  	*temp_d1_mean,
	*temp_d2_mean,
	*temp_gradrho_mean;
  
  
static float *GC_dissip_1_st;	/* the spatio-temporal distribution of dissipation d1	*/
static float *GC_dissip_2_st;	/* the spatio-temporal distribution of dissipation d2	*/

static size_t GC_ny;		/* size in the time direction for output data files	*/
static int GC_position_index; /* loop index									*/
#endif


/************************************************************************/
/* Implementation							*/
/************************************************************************/


/****************************************************************************************/
int	GC_init(int ny, char *prefix_filename)
{	
	if ( (prefix_filename!=NULL) && (strlen(prefix_filename)>0) )
        {	
		(void)add_prefix(prefix_filename, time_data_rho_local_filename);
		(void)add_prefix(prefix_filename, time_data_rho_mean_filename);
		(void)add_prefix(prefix_filename, time_data_e1_local_filename);
		(void)add_prefix(prefix_filename, time_data_e1_mean_filename);
		(void)add_prefix(prefix_filename, time_data_e2_local_filename);
		(void)add_prefix(prefix_filename, time_data_e2_mean_filename);
		(void)add_prefix(prefix_filename, time_data_v2_mean_filename);
		(void)add_prefix(prefix_filename, time_data_D_e1_local_filename);
		(void)add_prefix(prefix_filename, time_data_D_e1_mean_filename);
		(void)add_prefix(prefix_filename, time_data_D_e2_local_filename);
		(void)add_prefix(prefix_filename, time_data_D_e2_mean_filename);
		(void)add_prefix(prefix_filename, time_data_D_u_mean_filename);
		(void)add_prefix(prefix_filename, time_data_D_k_mean_filename);
		(void)add_prefix(prefix_filename, time_data_dissip_local_filename);
		(void)add_prefix(prefix_filename, time_data_dissip_mean_filename);
		(void)add_prefix(prefix_filename, time_data_jA_local_filename);
		(void)add_prefix(prefix_filename, time_data_jA_mean_filename);
		(void)add_prefix(prefix_filename, time_data_rho2_mean_filename);
		(void)add_prefix(prefix_filename, time_data_rho3_mean_filename);
		(void)add_prefix(prefix_filename, time_data_tTT_mean_filename);
		(void)add_prefix(prefix_filename, time_data_rhov2_mean_filename);
		(void)add_prefix(prefix_filename, time_data_d1_mean_filename);
		(void)add_prefix(prefix_filename, time_data_d2_mean_filename);
		(void)add_prefix(prefix_filename, time_data_gradrho_mean_filename);
	}
#ifndef XVIN // if compiling this file under XVin environment, following is not compiled	
	GC_ny=(size_t)ny;

	if (auto_go_on<2) // no appending : we have to erase previous files
	{	/* erase previous files (dangerous if correct backups are not made !!!) : */
		fclose(fopen(time_data_rho_local_filename, "wb"));
		fclose(fopen(time_data_rho_mean_filename,  "wb"));
		fclose(fopen(time_data_e1_mean_filename,  "wb"));
		fclose(fopen(time_data_e1_local_filename, "wb"));
		fclose(fopen(time_data_e2_mean_filename,  "wb"));
		fclose(fopen(time_data_e2_local_filename, "wb"));
		fclose(fopen(time_data_v2_mean_filename, "wb"));
		fclose(fopen(time_data_D_e1_local_filename,"wb"));
		fclose(fopen(time_data_D_e1_mean_filename, "wb"));
		fclose(fopen(time_data_D_e2_local_filename,"wb"));
		fclose(fopen(time_data_D_e2_mean_filename, "wb"));
		fclose(fopen(time_data_D_u_mean_filename, "wb"));
		fclose(fopen(time_data_D_k_mean_filename, "wb"));
		fclose(fopen(time_data_dissip_local_filename,"wb"));
		fclose(fopen(time_data_dissip_mean_filename, "wb"));
		fclose(fopen(time_data_jA_local_filename,"wb"));
		fclose(fopen(time_data_jA_mean_filename, "wb"));
		fclose(fopen(time_data_rho2_mean_filename, "wb"));
		fclose(fopen(time_data_rho3_mean_filename, "wb"));
		fclose(fopen(time_data_tTT_mean_filename, "wb"));
		fclose(fopen(time_data_rhov2_mean_filename, "wb"));
		fclose(fopen(time_data_d1_mean_filename, "wb"));
		fclose(fopen(time_data_d2_mean_filename, "wb"));
		fclose(fopen(time_data_gradrho_mean_filename, "wb"));
		
	}

	temp_rho_local	= (double*)calloc((size_t)(ny),sizeof(double));
	temp_rho_mean	= (double*)calloc((size_t)(ny),sizeof(double));
	temp_e1_mean	= (double*)calloc((size_t)(ny),sizeof(double));
	temp_e1_local	= (double*)calloc((size_t)(ny),sizeof(double));
	temp_e2_mean	= (double*)calloc((size_t)(ny),sizeof(double));
	temp_e2_local	= (double*)calloc((size_t)(ny),sizeof(double));
	temp_v2_mean	= (double*)calloc((size_t)(ny),sizeof(double));
	temp_De1_local	= (double*)calloc((size_t)(ny),sizeof(double));
	temp_De1_mean	= (double*)calloc((size_t)(ny),sizeof(double));
	temp_De2_local	= (double*)calloc((size_t)(ny),sizeof(double));
	temp_De2_mean	= (double*)calloc((size_t)(ny),sizeof(double));
	temp_Du_mean	= (double*)calloc((size_t)(ny),sizeof(double));
	temp_Dk_mean	= (double*)calloc((size_t)(ny),sizeof(double));
	temp_dissip_local=(double*)calloc((size_t)(ny),sizeof(double));
	temp_dissip_mean =(double*)calloc((size_t)(ny),sizeof(double));
	temp_jA_local   = (double*)calloc((size_t)(ny),sizeof(double));
	temp_jA_mean    = (double*)calloc((size_t)(ny),sizeof(double));
	temp_rho2_mean    = (double*)calloc((size_t)(ny),sizeof(double));
	temp_rho3_mean    = (double*)calloc((size_t)(ny),sizeof(double));
	temp_tTT_mean    = (double*)calloc((size_t)(ny),sizeof(double));
	temp_rhov2_mean    = (double*)calloc((size_t)(ny),sizeof(double));
	temp_d1_mean    = (double*)calloc((size_t)(ny),sizeof(double));
	temp_d2_mean    = (double*)calloc((size_t)(ny),sizeof(double));
	temp_gradrho_mean    = (double*)calloc((size_t)(ny),sizeof(double));
	if ( (temp_rho_local==NULL) || (temp_rho_mean==NULL) )
		return( printf("memory allocation failed for temp variables.\n"));
	if ( (temp_jA_local==NULL) || (temp_jA_mean==NULL) ) 
		return( printf("memory allocation failed for temp variables.\n"));
	if ( (temp_e1_mean==NULL) || (temp_e1_local==NULL) || (temp_v2_mean==NULL) ) 
		return( printf("memory allocation failed for temp variables.\n"));
	if ( (temp_De1_mean==NULL) || (temp_De1_local==NULL) || (temp_dissip_mean==NULL) || (temp_dissip_local==NULL) ) 
		return( printf("memory allocation failed for temp variables.\n"));

	if (do_save_st>=1)
	{	GC_dissip_1_st = (float*)calloc((size_t)auto_nx*ny, sizeof(float));
		GC_dissip_2_st = (float*)calloc((size_t)auto_nx*ny, sizeof(float));
		if (do_save_st==1) 
		{	fclose(fopen(st_data_dissipation_1_filename, "wb")); // do not continue, create a new one
			fclose(fopen(st_data_dissipation_2_filename, "wb")); // do not continue, create a new one
		}
	}	

	GC_position_index=0;

	if (verbosity>1) printf("memory OK... ");
#endif	
	return(0);
}

#ifndef XVIN
/****************************************************************************************/
int GC_compute(double *A)
{	
//	extern float 	auto_c1, auto_c2, auto_L;

	register int i, i1, i2, i3, k;
	
	int j=GC_position_index;
	float	dx;
	double	
		p=0, e1=0, e2=0, De1_l=0, De2_l=0, Du_l=0., Dk_l=0., ijA=0, ijA1=0, d_1=0, d_2=0, trace=0,
		Pm=0.,	// will store the mean of squared amplitude \rho=|A|^2
		em=0., 	// will store the mean of e
		e2m=0.,	// will store the mean of e_2 (second energy in CGLe)
		v2m=0.,	// will store the mean of |A_x|^2 = |v|^2
		De1_m=0., // will store the mean of De
		De2_m=0.,	// will store the mean of De_2
		Du_m=0., // mean of destruction of u
		Dk_m=0., // mean of destruction of u
		dism=0., // will store the mean of dissipation (funny term)
		ijAm=0., // will store the mean of i*flux of A
		rho2_m=0.,
		rho3_m=0.,
		rhov2_m=0.,
		trace_m=0.,
		gradrho_m=0.,
		d_1_m=0.,
		d_2_m=0.;
	double 	p1, v2;


	dx=auto_L/auto_nx;	// needed to compute spatial derivatives
	
	for (i=0; i<auto_nx; i++) 
	{	i1 = (i==0) ? auto_nx-1 : i-1;	i2 = (i==auto_nx-1) ? 0 : i+1;
		i3 = (i1==0) ? auto_nx-1 : i1-1;

		p  = A[2*i]*A[2*i] + A[2*i+1]*A[2*i+1] ; 	// local squared amplitude, ie local rho
		p1 = A[2*i1]*A[2*i1] + A[2*i1+1]*A[2*i1+1] ;	// local squared amplitude at previous point in space
		Pm     += p;							// rho averaged in space
		rho2_m += p*p;
		rho3_m += p*p*p;
		v2 = ((A[2*i2]-A[2*i1])*(A[2*i2]-A[2*i1]) 
		   +  (A[2*i2+1]-A[2*i1+1])*(A[2*i2+1]-A[2*i1+1]))/(4*dx*dx) ;  // local |v|^2
		e1 = v2	+ p*p/2;						// local energy e
		e2 = auto_c1*v2 + auto_c2*p*p/2;
		rhov2_m += p*v2;
		em += e1;
		e2m += e2;
		v2m += v2;
		trace = ( (A[2*i2]+A[2*i1]-2*A[2*i])*(A[2*i2]+A[2*i1]-2*A[2*i])
			 +(A[2*i2+1]+A[2*i1+1]-2*A[2*i+1])*(A[2*i2+1]+A[2*i1+1]-2*A[2*i+1]) )/(dx*dx*dx*dx);
			// trace is the squared modulus of the laplacian of A
    		trace_m += trace;

    		gradrho_m += (p-p1)*(p-p1)/(dx*dx);
    
		De1_l  = 4*p*e1 + 2*(p-p1)*(p-p1)/(dx*dx) + 2*trace;
		De1_m += De1_l;
		De2_l = 2*p*(auto_c2*p*p + (auto_c1+auto_c2)*v2) + (auto_c1+auto_c2)*(p-p1)*(p-p1)/(dx*dx)
								 + 2*auto_c1*trace;
		De2_m+= De2_l;
		
		
		ijA  = -2*( A[2*i]*(A[2*i2+1]-A[2*i1+1]) - A[2*i+1]*(A[2*i2]-A[2*i1]) ) / (2*dx) ;
		ijA1 = -2*( A[2*i1]*(A[2*i+1]-A[2*i3+1]) - A[2*i1+1]*(A[2*i]-A[2*i3]) ) / (2*dx) ;
		ijAm+= auto_c1*(ijA-ijA1)/dx;
		d_1  = ((auto_c1-auto_c2)/2) * ( p*(ijA-ijA1) - (p-p1)*(ijA) )/dx ;
//		d_2  = ( (auto_c2-auto_c1)*( (p-p1)*(ijA) )/dx);  // note: on 2006/11/14, factor (c2-c1) was removed from definition of d_2 !!!
		d_2  = (p-p1) * ijA / dx;  	// note: on 2006/11/14, factor (c2-c1) was removed from definition of d_2 !!!
		d_1_m += d_1;
		d_2_m += d_2;

		Du_l = 2.*p*p*p + 2.*p*v2 + (p-p1)*(p-p1)/(dx*dx) - auto_c1*d_2;
		Du_m += Du_l;
		Dk_l = 2.*p*v2 + 2.*trace + (p-p1)*(p-p1)/(dx*dx) + auto_c2*d_2;
		Dk_m += Du_l;

		if (do_save_st>=1)
		for (k=0; k<auto_nx; k++)
    		{	GC_dissip_1_st[j*auto_nx + i] = (float)d_1;
        		GC_dissip_2_st[j*auto_nx + i] = (float)d_2;
    		}
	
	}

		temp_rho_mean[j] = Pm   /auto_nx;
		temp_v2_mean[j]  = v2m  /auto_nx;
		temp_e1_mean[j]  = em   /auto_nx;
		temp_e2_mean[j]  = e2m  /auto_nx;
		temp_De1_mean[j] = De1_m/auto_nx;
		temp_De2_mean[j] = De2_m/auto_nx;
		temp_Du_mean[j]  = Du_m /auto_nx;
		temp_Dk_mean[j]  = Dk_m /auto_nx;
		temp_dissip_mean[j] = dism/auto_nx;
		temp_jA_mean[j]  = ijAm/auto_nx;

    temp_rho2_mean[j]  =    rho2_m/auto_nx;
    temp_rho3_mean[j]  =    rho3_m/auto_nx;
    temp_tTT_mean[j]   =    trace_m/auto_nx;
    temp_rhov2_mean[j] =    rhov2_m/auto_nx;
    temp_gradrho_mean[j] =  gradrho_m/auto_nx;
    temp_d1_mean[j] =       d_1_m/auto_nx;
    temp_d2_mean[j] =       d_2_m/auto_nx;

	/* local values : they are taken at x=auto_nx-1 (last point computed in the loop !) */ 
		temp_rho_local[j] = p;
		temp_e1_local[j]  = e1;
		temp_e2_local[j]  = e2;
		temp_De1_local[j] = De1_l;
		temp_De2_local[j] = De2_l;
		temp_dissip_local[j]= d_1;
		temp_jA_local[j]=auto_c1*(ijA-ijA1)/auto_nx;

	GC_position_index++;	// very important : GC_position_index is static !!!
	
	return(0);
}


int GC_reset(void)
{	
	GC_position_index=0;	
	return(0);
}


int GC_save(void)
{
	(void)append_dataset_d2f(time_data_rho_local_filename,		temp_rho_local, 	GC_ny);		
	(void)append_dataset_d2f(time_data_rho_mean_filename, 		temp_rho_mean,  	GC_ny);		
	(void)append_dataset_d2f(time_data_e1_mean_filename, 		temp_e1_mean,  	GC_ny); 	
	(void)append_dataset_d2f(time_data_e1_local_filename,		temp_e1_local, 	GC_ny);
	(void)append_dataset_d2f(time_data_e2_mean_filename, 		temp_e2_mean,  	GC_ny); 	
	(void)append_dataset_d2f(time_data_e2_local_filename,		temp_e2_local, 	GC_ny);		
	(void)append_dataset_d2f(time_data_v2_mean_filename,		temp_v2_mean,		GC_ny);
	(void)append_dataset_d2f(time_data_D_e1_local_filename,	temp_De1_local,	GC_ny);	
	(void)append_dataset_d2f(time_data_D_e1_mean_filename, 	temp_De1_mean, 	GC_ny);	
	(void)append_dataset_d2f(time_data_D_e2_local_filename,	temp_De2_local,	GC_ny);	
	(void)append_dataset_d2f(time_data_D_e2_mean_filename, 	temp_De2_mean, 	GC_ny);	
	(void)append_dataset_d2f(time_data_D_u_mean_filename, 		temp_Du_mean, 		GC_ny);	
	(void)append_dataset_d2f(time_data_D_k_mean_filename, 		temp_Du_mean, 		GC_ny);	
	(void)append_dataset_d2f(time_data_dissip_local_filename,	temp_dissip_local,	GC_ny);
	(void)append_dataset_d2f(time_data_dissip_mean_filename, 	temp_dissip_mean,	GC_ny);
	(void)append_dataset_d2f(time_data_jA_local_filename,  	temp_jA_local,		GC_ny);
	(void)append_dataset_d2f(time_data_jA_mean_filename,   	temp_jA_mean,		GC_ny);
	(void)append_dataset_d2f(time_data_rho2_mean_filename,		temp_rho2_mean,	GC_ny);
	(void)append_dataset_d2f(time_data_rho3_mean_filename,  	temp_rho3_mean,	GC_ny);
	(void)append_dataset_d2f(time_data_rhov2_mean_filename,	temp_rhov2_mean,	GC_ny);
	(void)append_dataset_d2f(time_data_tTT_mean_filename,		temp_tTT_mean,      GC_ny);
	(void)append_dataset_d2f(time_data_gradrho_mean_filename,   temp_gradrho_mean,  GC_ny);
	(void)append_dataset_d2f(time_data_d1_mean_filename,     	temp_d1_mean,     	GC_ny);
	(void)append_dataset_d2f(time_data_d2_mean_filename,     	temp_d2_mean,     	GC_ny);

	if (do_save_st>=1)
  { (void)append_dataset_f(st_data_dissipation_1_filename,	GC_dissip_1_st, auto_nx*GC_ny);
    (void)append_dataset_f(st_data_dissipation_2_filename,	GC_dissip_2_st, auto_nx*GC_ny);
  }
	return(0);
}




int GC_free(void)
{
	free(temp_rho_local);	free(temp_rho_mean);
	free(temp_v2_mean);
	free(temp_e1_local);	free(temp_e1_mean);
	free(temp_e2_local);	free(temp_e2_mean);
 	free(temp_De1_local);	free(temp_De1_mean);	
 	free(temp_De2_local);	free(temp_De2_mean);
 	free(temp_Du_mean);
 	free(temp_Dk_mean);
	free(temp_dissip_local);	free(temp_dissip_mean);	
	free(temp_jA_local);	free(temp_jA_mean); 

	free(temp_rho2_mean); 	free(temp_rho3_mean);
	free(temp_tTT_mean); 	free(temp_rhov2_mean);
	free(temp_gradrho_mean);
	free(temp_d1_mean); 	free(temp_d2_mean);

	if (do_save_st>0)
	{	free(GC_dissip_1_st);
		free(GC_dissip_2_st);
 	}

	return(0);
}
#endif






