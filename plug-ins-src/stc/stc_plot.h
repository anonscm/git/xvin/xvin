#ifndef _STC_PLOT_H_
#define _STC_PLOT_H_

PXV_FUNC(int, do_plot_initial_conditions,	(void));
PXV_FUNC(int, do_plot_complex_line,		(void));
PXV_FUNC(int, do_plot_lyaps,				(void));
PXV_FUNC(int, do_plot_dissipation_files,	(void));
PXV_FUNC(int, do_plot_balance_rho,		 	(void));
PXV_FUNC(int, do_plot_balance_u,			(void));
PXV_FUNC(int, do_plot_balance_k,			(void));

PXV_FUNC(MENU*, stc_plot_menu, 			(void));
PXV_FUNC(MENU*, stc_plot_quantities_submenu, (void));
PXV_FUNC(MENU*, stc_plot_balances_submenu,	(void));
	
PXV_FUNC(int,   stc_check_for_data_size,   (int n, char *filename));
PXV_FUNC(O_p*,  create_and_attach_one_plot_from_datafile, (pltreg *pr, char *filename, char *history));
PXV_FUNC(d_s*,  create_and_attach_one_ds_from_datafile,   (O_p *op, char *filename, char *history));


// definitions for selecting what to plot:
#define STC_PLOT_SIGMA        0x00000100
#define STC_PLOT_RHO          0x00000200
#define STC_PLOT_U            0x00000400
#define STC_PLOT_P_U          0x00000800
#define STC_PLOT_D_U          0x00001000
#define STC_PLOT_K            0x00002000
#define STC_PLOT_P_K          0x00004000
#define STC_PLOT_D_K          0x00008000
#define STC_PLOT_D2           0x00010000
#define STC_PLOT_E1           0x00020000
#define STC_PLOT_E2           0x00040000
#define STC_PLOT_TRACE        0x00080000

#endif

