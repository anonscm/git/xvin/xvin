#ifndef _STC_IMAGE_H_
#define _STC_IMAGE_H_

#define STC_IMAGE_ST_DATA        0x0000100
#define STC_IMAGE_LYAPS_RE       0x0000200
#define STC_IMAGE_LYAPS_RE_INC   0x0000400
#define STC_IMAGE_LYAPS_RE_LOCAL 0x0000800

PXV_FUNC(int,	do_plot_st_data,	(void));

PXV_FUNC(MENU*, stc_image_menu,		(void));

#endif
