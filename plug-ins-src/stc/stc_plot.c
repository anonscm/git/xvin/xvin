#ifndef _STC_PLOT_C_
#define _STC_PLOT_C_

#include "allegro.h"
#include "xvin.h"
#include "xvin/xv_tools_lib.h"

/* If you include other plug-ins header do it here	*/
#include "../inout/inout.h"
#include "../diff/diff.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "stc.h"
#include "stc_plot.h"
#include "stc_plot_balances.h"
#include "stc_tools.h"
#include "CGL_parameters.h"
#include "CGL_dissip.h"

int		is_first_load_dissipation=1; // to track if we need to add the prefix to filenames, or not




/**********************************************************************/
/* plots the initial conditions of the simulation					*/
/* NG, 27/06/2002											*/
/**********************************************************************/
int do_plot_initial_conditions(void)
{
register int 	j;
	O_p 	*op2, *op1 = NULL;
	int	n=0;
	d_s 	*ds2, *ds1;
	pltreg	*pr = NULL;
	float *x=NULL;
	char initial_conditions_filename[128]="lastline.dat";
	char *tmp;

	if(updating_menu_state != 0)	return D_O_K;	
//	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine plots the initial conditions"
					"that were used by the numerical simulations");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");

	tmp = calloc(128,sizeof(char));
	tmp = extract_file_name( tmp, 1, simulation_parameters_fullfilename);
	
	load_parameter_string(simulation_parameters_fullfilename, "init_filename", initial_conditions_filename);
	n=load_data_bin(initial_conditions_filename, &x);
	n/=2;
	if (n!=auto_nx) 
	{	j=win_printf("%d complex points in file %s\n but nx=%d (from %s or imposed)\n"
					"\npress OK to set nx=%d, or CANCEL to abort", 
					n, initial_conditions_filename, auto_nx, tmp, n);	
		if (j==CANCEL) return(D_O_K);
	}

	auto_nx=n;
	if ((op2 = create_and_attach_one_plot(pr, n, n, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	
	ds2 = op2->dat[0];
	for (j=0; j<n; j++)
	{	ds2->xd[j] = (float)j*auto_L/(float)auto_nx; 
		ds2->yd[j] = x[2*j];
	}
	if ((ds2 = create_and_attach_one_ds(op2, n, n, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	for (j=0; j<n; j++)
	{	ds2->xd[j] = (float)j*auto_L/(float)auto_nx; 
		ds2->yd[j] = x[2*j+1];
	}
    set_plot_x_title(op2, "\\color{white}x (CGLe units)");
    set_plot_y_title(op2, "\\color{white}initial conditions");
		
	free(tmp);
	return refresh_plot(pr, UNCHANGED);
}
/* end of 'do_plot_initial_conditions' */





/**********************************************************************/
/* plots the initial conditions of the simulation					*/
/* NG, 2006/11/09											*/
/**********************************************************************/
int do_plot_complex_line(void)
{
register int 	j;
	O_p 	*op2, *op1 = NULL;
	int	n=0;
	d_s 	*ds2, *ds1;
	pltreg	*pr = NULL;
	float *x=NULL;
	char		filename[512]="lastline.dat";

	if(updating_menu_state != 0)	return D_O_K;	
//	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine plots 1-d complex data"
					"such as the initial conditions, or the last line");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return(win_printf_OK("cannot find plot region"));

	j = file_select_ex("Load complex .dat file", filename, "dat", 512, 0, 0);
	if (j==0) return(OFF);
	
	n=load_data_bin(filename, &x);
	n/=2; // because complexes !
	if (n!=auto_nx) 
	{	j=win_printf("%d complex points in file %s\n but nx=%d (from parameters loaded)\n"
					"\npress OK to set nx=%d, or CANCEL to abort", 
					n, filename, auto_nx, n);	
		if (j==CANCEL) return(D_O_K);
	}

	auto_nx=n;
	if ((op2 = create_and_attach_one_plot(pr, n, n, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	
	ds2 = op2->dat[0];
	for (j=0; j<n; j++)
	{	ds2->xd[j] = (float)j*auto_L/(float)auto_nx; 
		ds2->yd[j] = x[2*j];
	}
	set_ds_source(ds2, "real part of %s", backslash_to_slash(filename));

	if ((ds2 = create_and_attach_one_ds(op2, n, n, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	for (j=0; j<n; j++)
	{	ds2->xd[j] = (float)j*auto_L/(float)auto_nx; 
		ds2->yd[j] = x[2*j+1];
	}
	set_ds_source(ds2, "imaginary part of %s", backslash_to_slash(filename));
		
	set_plot_title(op2, "%s", backslash_to_slash(filename));

	return refresh_plot(pr, UNCHANGED);
}
/* end of 'do_plot_complex_line' */






/**********************************************************************/
/* plots the Lyaps. 						      */
/* NG, 27/06/2006						      */
/**********************************************************************/
int do_plot_lyaps(void)
{
register int 	j, k;
	O_p 	*op1=NULL, *op2=NULL;
	int	n=0, nx=0, ny=0;
	d_s 	*ds2=NULL, *ds1;
	pltreg	*pr = NULL;
	double	y=0.;	
	float 	*x=NULL, x_tmp;
	char 	lyaps_filename[128]="lyaps_RE_inc.dat";
static char 	lyaps_string  [128]="0:10";
	int	*lyaps_index=NULL, n_lyaps, ind;
static int	 bool_plot_what=0, bool_what_file=2;

	if(updating_menu_state != 0)	return D_O_K;	
//	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine plots the lyaps"
					"that were computed in the numerical simulations");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");

	j=D_O_K;
	if (load_parameter(simulation_parameters_fullfilename,"do_lyaps",&x_tmp)!=1) 
		j=CANCEL; 
	else if ((int)x_tmp<1) j=win_printf("from params.txt, no Lyaps were computed.\n"
						"click OK to continue, or CANCEL to exit");
	if (j==CANCEL) return(D_O_K);
	
	j=win_scanf("what file to plot ?\n%R lyaps_RE.dat\n%rlyaps_RE_inc.dat\n%rlyaps_RE_local.dat", &bool_what_file);
	if (j==CANCEL) return(D_O_K);
	
	if      (bool_what_file==0) 
    {               sprintf(lyaps_filename,"lyaps_RE.dat"); 
                    nx = auto_nx*2;		// twice as mode number in space
    }
	else if (bool_what_file==1) 
    {               sprintf(lyaps_filename,"lyaps_RE_inc.dat");
                    nx = auto_nx*2;		// twice as mode number in space
    }
	else if (bool_what_file==2) 
    {               sprintf(lyaps_filename,"lyaps_RE_local.dat");
             if (load_parameter(simulation_parameters_fullfilename, "finite_time_lyaps_number", &x_tmp)!=1)
                return(win_printf_OK("couldn't load local lyaps number from params.txt"));
                    if (x_tmp<auto_nx) nx=(int)x_tmp;   
    }
	
//	tmp = calloc(128,sizeof(char));
//	tmp = extract_file_name( tmp, 1, simulation_parameters_fullfilename);
/*	j=win_scanf(" prefix : %s lyaps filename %s", &simulation_filename_prefix, &lyaps_filename);
		if (j==CANCEL) return(D_O_K);
*/	add_prefix(simulation_filename_prefix, lyaps_filename);
	
	n=load_data_bin(lyaps_filename, &x);
	ny = n/nx;		// number of points in time
	j=win_scanf("%6d lyaps in file and %6d points in time\n"
				"%R plot time-averaged spectrum\n"
				"%r plot lyaps %s versus time",
				&nx, &ny, &bool_plot_what, &lyaps_string);	
	if (j==CANCEL) return(D_O_K);
	if (ny==0) return(win_printf_OK("Nothing to plot"));
	if (nx*ny>n) return(win_printf_OK("n=%d pts but nx=%d and ny=%d so nx.ny=%d", n,nx,ny,nx*ny));
	
	if (bool_plot_what==0) // we plot the time averaged spectrum		
	{	if ((op2 = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
			return win_printf_OK("cannot create plot !");
	
		ds2 = op2->dat[0];
		for (j=0; j<nx; j++)
		{	ds2->xd[j] = (float)j;
	 	
			y = (double)0.;
			for (k=0; k<ny; k++)
			{	y += x[j + k*nx]; 
			}
			ds2->yd[j] = (float)(y/(double)ny);
		}
		set_ds_point_symbol(ds2, "\\oc");
		set_ds_dot_line(ds2);
		set_plot_x_title(op2, "Lyap number i");

	}
	else if (bool_plot_what==1) // we plot the lyaps versus time		
	{	lyaps_index = str_to_index(lyaps_string, &n_lyaps);	// malloc is done by str_to_index
		if ( (lyaps_index==NULL) || (n_lyaps<1) || (lyaps_index[n_lyaps-1]>=nx) )	
			return(win_printf_OK("bad values for the lyaps numbers !"));

		if ((op2 = create_and_attach_one_plot(pr, ny, ny, 0)) == NULL)
			return(win_printf_OK("cannot create plot !"));
		ds2 = op2->dat[0];
	
		for (j=0; j<n_lyaps; j++)
		{	ind=lyaps_index[j];

			if (j!=0)
			if ((ds2 = create_and_attach_one_ds(op2, ny, ny, 0)) == NULL)	
				return win_printf_OK("cannot create dataset !");
				
			for (k=0; k<ny; k++)
			{	ds2->xd[k] = (float)k*real_dt;
				ds2->yd[k] = x[ind + k*nx];
			}	 
			ds2->history = my_sprintf(ds2->history,"Lyapunov exponent #%d", ind);
		}	
		free(lyaps_index);
		set_plot_x_title(op2, "time (%d pts, dt=%g)", real_npts, real_dt);
	}
	
	free(x);
	
	set_plot_title(op2,"Lyapunov exponents");
	set_plot_y_title(op2, "\\lambda_i");
	set_op_filename(op2, "%s.gr", lyaps_filename);
	set_ds_source(ds2, "%s", lyaps_filename);
	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
}
/* end of 'do_plot_lyaps' */


/**********************************************************************/
/* used by several functions to check if the size of data loaded		*/
/* is the one expected from the params.txt file					*/
/* it only print a warning									*/
/* this warning can be disabled								*/
/*														*/
/* returns 0 if no pb, or 1 if pb								*/
/* NG, 2006/11/09, Warsaw								     */
/**********************************************************************/
int	stc_check_for_data_size(int n, char *filename)
{	int		bool_warning=1, j, k=0;

	if ( (n!=real_npts) && (bool_warning==1) )
	{	j=win_printf("nb of points in file %s is %d\n"
					"which is different from expected %d from parameters file.\n\n"
					"(click OK to still have those warning, or CANCEL to discard them)", 
					backslash_to_slash(filename), n, real_npts);
		if (j==CANCEL) bool_warning=0;
		k=1;
	}
	return(k);
}



/**********************************************************************/
/* used by "do_plot_dissipation_files" 							*/
/* NG, 2006/11/09, Warsaw								     */
/**********************************************************************/
O_p *create_and_attach_one_plot_from_datafile(pltreg *pr, char *filename, char *history)
{	int		n, j;
	float	*x=NULL;
	O_p		*op=NULL;
	char    text[128];
	
	n=load_data_bin(filename, &x);
	stc_check_for_data_size(n, filename);
	if ((op = create_and_attach_one_plot(pr, n, n, 0)) == NULL)
	{	win_printf_OK("cannot create plot !");
		return(NULL);
	}
	if (extract_file_name(text,128,filename)!=NULL)	set_ds_source (op->dat[0], "%s", text);
	else                                            set_ds_source (op->dat[0], "unknown source");    
	set_ds_history(op->dat[0], "%s", history);
	for (j=0; j<n; j++) 
	{	op->dat[0]->xd[j] = (float)j*real_dt;
		op->dat[0]->yd[j] = x[j]; 
	}
	free(x);
	return(op);
}


/**********************************************************************/
/* used by "do_plot_dissipation_files" 							*/
/* NG, 2006/11/09, Warsaw								     */
/**********************************************************************/
d_s *create_and_attach_one_ds_from_datafile(O_p *op, char *filename, char *history)
{	int 		n, j;
	float 	*x=NULL;
	d_s		*ds=NULL;
	char    text[128];

	n=load_data_bin(filename, &x); 
	stc_check_for_data_size(n, filename);
	if ((ds = create_and_attach_one_ds(op, n, n, 0)) == NULL)	
	{	win_printf_OK("cannot create dataset !");
		return(NULL);
	}
	if (extract_file_name(text,128,filename)!=NULL)	set_ds_source (ds, "%s", text);
	else                                            set_ds_source (ds, "unknown source");          
	set_ds_history(ds, "%s", history);
	for (j=0; j<n; j++) 
	{	ds->xd[j] = (float)j*real_dt;
		ds->yd[j] = x[j]; 
	}
	free(x);
	return(ds);
}


/**********************************************************************/
/* plots dissipation data, ie, a lot of datasets on the same plot	*/
/* NG, 2006/11/09, Warsaw								     */
/**********************************************************************/
int do_plot_dissipation_files(void)
{
	O_p 	*op1=NULL, *op2=NULL;
	int	n=0, nx=0, ny=0;
	d_s 	*ds2, *ds1;
	pltreg	*pr = NULL;
//	float 	*x=NULL;
//	char 	*s=NULL;
	
	if(updating_menu_state != 0)	return D_O_K;	
//	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine plots several .dat files\n"
					"eventually associated to dissipation and other\n"
					"terms in balance equations.\n"
					"It can also be used to load anything computed\n"
					"by the numerical simulation.\n"
					"a new plot is created");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");

//	tmp = calloc(128,sizeof(char));
//	tmp = extract_file_name( tmp, 1, simulation_parameters_fullfilename);
//	j=win_scanf(" prefix : %s lyaps filename %s", &simulation_filename_prefix, &lyaps_filename);
//		if (j==CANCEL) return(D_O_K);
//	add_prefix(simulation_filename_prefix, lyaps_filename);
	
//	load_parameter_string(simulation_parameters_fullfilename, "init_filename", initial_conditions_filename);
//	n=load_data_bin(lyaps_filename, &x);
	nx = auto_nx*2;		// twice as mode number in space
	ny = n/nx;		// number of points in time

// here, we add the prefix (run0_) to the filenames; this has to be done once only :
	if (is_first_load_dissipation==1) { GC_init(auto_ny, simulation_filename_prefix); is_first_load_dissipation=0; }
	
// local quantities: 	
// first dataset:	
	op2 = create_and_attach_one_plot_from_datafile(pr, time_data_rho_local_filename, "local \\rho");
// other datasets:
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_e1_local_filename, "e1 local");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_e2_local_filename, "e2 local");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_jA_local_filename, "jA local");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_D_e1_local_filename, "D_{e1} local");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_D_e2_local_filename, "D_{e2} local");
// cosmetics:
	set_plot_title(op2,"Dissipation data : local quantities");
	set_plot_x_title(op2, "time");
	set_op_filename(op2, "%sdissipation_local.gr", simulation_filename_prefix);
    	op2->dir = Mystrdup(op1->dir);

// global quantities: 	
	op2 = create_and_attach_one_plot_from_datafile(pr, time_data_rho_mean_filename, "<\\rho>");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_e1_mean_filename, "<e_1>");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_e2_mean_filename, "<e_2>");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_jA_mean_filename, "flux <jA>");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_v2_mean_filename, "<v^2>");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_D_e1_mean_filename, "<D_{e1}>");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_D_e2_mean_filename, "<D_{e2}>");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_dissip_local_filename, "GC_dissip_local.dat");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_dissip_mean_filename, "GC_dissip_mean.dat");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_rho2_mean_filename, "<\\rho^2>");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_rho3_mean_filename, "<\\rho^3>");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_tTT_mean_filename, "<tTT>");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_rhov2_mean_filename, "<\\rho.v^2>");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_d1_mean_filename, "<d1>");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_d2_mean_filename, "<d2>");
	ds2 = create_and_attach_one_ds_from_datafile(op2, time_data_gradrho_mean_filename, "<\\partial_x \\rho>");
// cosmetics:
	set_plot_title(op2,"Dissipation data : global quantities");
	set_plot_x_title(op2, "\\color{white}time");
	set_op_filename(op2, "%sdissipation_global.gr", simulation_filename_prefix);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
}
/* end of 'do_plot_dissipation_files' */




int number_of_active_modes(float L)
{	return( 2 + 4* floor((L-2.)/4.) );
}




/**********************************************************************/
/* plots some quantity, out of the code							 */
/* NG, 2006/14/09, Warsaw								      */
/**********************************************************************/
int do_plot_something(void)
{	register int j;
	O_p 		*op1=NULL;
	int		index;
	d_s 		*ds2, *ds1;
	pltreg	*pr = NULL;
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine plots some quantity\n"
					"only a dataset is created");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");

// here, we add the prefix (run0_) to the filenames; this has to be done once only :
	if (is_first_load_dissipation==1) { GC_init(auto_ny, simulation_filename_prefix); is_first_load_dissipation=0; }

	if (index & STC_PLOT_SIGMA)
	{	ds2 = create_and_attach_one_ds_from_datafile(op1, time_data_rho_mean_filename, "phase space contraction \\sigma=\\epsilon-2<\\rho>-something");
		for (j=0; j<ds2->nx; j++)
		{	ds2->yd[j] *= (float)(-2.); // -2<rho>
			ds2->yd[j] += auto_epsilon - M_PI*M_PI*( number_of_active_modes(auto_L)*number_of_active_modes(auto_L) - 4.)/(12.*auto_L*auto_L);
		}
	}	
	else if (index & STC_PLOT_RHO)
	{	ds2 = create_and_attach_one_ds_from_datafile(op1, time_data_rho_mean_filename, "<\\rho>");
	}	
	else	if (index & STC_PLOT_U)
	{	ds2 = create_and_attach_one_ds_from_datafile(op1, time_data_rho2_mean_filename, "<u>=\\frac{1}{2}<\\rho^2>");
		for (j=0; j<ds2->nx; j++) ds2->yd[j] /= (float)2.; 
	}	
	else	if (index & STC_PLOT_D2)
	{	ds2 = create_and_attach_one_ds_from_datafile(op1, time_data_d2_mean_filename, "<d_2>=<\\partial_x\\rho.j(A)>");
	}	
	else	if (index & STC_PLOT_P_U)
	{	ds2 = create_and_attach_one_ds_from_datafile(op1, time_data_rho2_mean_filename, "<P_u>=4\\epsilon <u>=2\\epsilon<\\rho^2>");
		for (j=0; j<ds2->nx; j++)
		{	ds2->yd[j] *= (float)2.*auto_epsilon; 
		}
	}	
	else if (index & STC_PLOT_D_U)
	{	ds2 = create_and_attach_one_ds_from_datafile(op1, time_data_D_u_mean_filename, "<D_u>=2<\\rho^3>+2<\\rho|v|^2>+<|\\partial_x\\rho|^2>-c_1<d_2>");
	}	
	else	if (index & STC_PLOT_K)
	{	ds2 = create_and_attach_one_ds_from_datafile(op1, time_data_v2_mean_filename, "<k>=<v^2>=<|\\partial_xA|^2>");
	}	
	else	if (index & STC_PLOT_P_K)
	{	ds2 = create_and_attach_one_ds_from_datafile(op1, time_data_rho2_mean_filename, "<P_k>=2\\epsilon <v^2>");
		for (j=0; j<ds2->nx; j++)
		{	ds2->yd[j] *= (float)2.*auto_epsilon; 
		}
	}	
	else if (index & STC_PLOT_D_K)
	{	ds2 = create_and_attach_one_ds_from_datafile(op1, time_data_D_k_mean_filename, "<D_k>=2<\\rho|v|^2>+2<Tr(T^+T)>+<|\\partial_x\\rho|^2>+c_2<d_2>");
	}	
	else if (index & STC_PLOT_E1)
	{	ds2 = create_and_attach_one_ds_from_datafile(op1, time_data_e1_mean_filename, "<e_1>");
	}	
	else if (index & STC_PLOT_E2)
	{	ds2 = create_and_attach_one_ds_from_datafile(op1, time_data_e2_mean_filename, "<e_2>");
	}	
	else if (index & STC_PLOT_TRACE)
	{	ds2 = create_and_attach_one_ds_from_datafile(op1, time_data_tTT_mean_filename, "<tr(T^+T)>");
	}	

	return refresh_plot(pr, UNCHANGED);
}
/* end of 'do_plot_balance_e2' */


MENU *stc_plot_quantities_submenu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
	add_item_to_menu(mn,"sigma",					do_plot_something,		 		NULL, STC_PLOT_SIGMA, NULL);
	add_item_to_menu(mn,"rho",					do_plot_something,		 		NULL, STC_PLOT_RHO, NULL);
	add_item_to_menu(mn,"u",						do_plot_something,		 		NULL, STC_PLOT_U, 	NULL);
	add_item_to_menu(mn,"production of u",			do_plot_something,		 		NULL, STC_PLOT_P_U, NULL);
	add_item_to_menu(mn,"destruction of u",			do_plot_something,		 		NULL, STC_PLOT_D_U, NULL);
	add_item_to_menu(mn,"k=rho^2",				do_plot_something,		 		NULL, STC_PLOT_K, 	NULL);
	add_item_to_menu(mn,"production of k",			do_plot_something,		 		NULL, STC_PLOT_P_K, NULL);
	add_item_to_menu(mn,"destruction of k",			do_plot_something,		 		NULL, STC_PLOT_D_K, NULL);
	add_item_to_menu(mn,"e_1",					do_plot_something,		 		NULL, STC_PLOT_E1, 	NULL);
	add_item_to_menu(mn,"e_2",					do_plot_something,		 		NULL, STC_PLOT_E2, 	NULL);
	add_item_to_menu(mn,"d_2",					do_plot_something,		 		NULL, STC_PLOT_D2, 	NULL);
	add_item_to_menu(mn,"tr(TT)",					do_plot_something,		 		NULL, STC_PLOT_D2, 	NULL);
	
	return mn;
}
	

MENU *stc_plot_balances_submenu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
	add_item_to_menu(mn,"Plot balance for rho",			do_plot_balance_rho,		 	NULL, 0, NULL);
	add_item_to_menu(mn,"Plot balance for u",			do_plot_balance_u,		 		NULL, 0, NULL);
	add_item_to_menu(mn,"Plot balance for k",			do_plot_balance_k,		 		NULL, 0, NULL);
	add_item_to_menu(mn,"Plot balance for e1 (!)",		do_plot_balance_e1,		 		NULL, 0, NULL);
	add_item_to_menu(mn,"Plot balance for e2 (!)",		do_plot_balance_e2,		 		NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						    0,		 					NULL, 0, NULL);
	add_item_to_menu(mn,"Integrate with no mean",		stc_substract_mean_integrate,    NULL, 0, NULL);
	
	return mn;
}


MENU *stc_plot_menu(void)
{
	static MENU mn[32];
	static MENU *submn = NULL;

	if (mn[0].text != NULL)	return mn;
	
	add_item_to_menu(mn,"Load simulation parameters",	do_load_simulation_parameters,	NULL, 0, NULL);
	add_item_to_menu(mn,"Edit simulation parameters",	do_edit_simulation_parameters,	NULL, 0, NULL);
	add_item_to_menu(mn,"Print simulation infos",		do_print_simulation_infos,		NULL, 0, NULL);
	add_item_to_menu(mn,"Set names of files to look at",	do_set_filenames,				NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		 					NULL, 0, NULL);
	add_item_to_menu(mn,"Plot initial condition",		do_plot_initial_conditions,	 	NULL, 0, NULL);
	add_item_to_menu(mn,"Plot complex line .dat file",	do_plot_complex_line,	 		NULL, 0, NULL);
	add_item_to_menu(mn,"Plot all .dat files",			do_plot_dissipation_files,	 	NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		 					NULL, 0, NULL);
	add_item_to_menu(mn,"Plot lyaps",					do_plot_lyaps,	 				NULL, 0, NULL);
	submn = stc_plot_quantities_submenu();
	add_item_to_menu(mn,"Plot an average quantity",		0,							submn,0, NULL);
	submn = stc_plot_balances_submenu();
	add_item_to_menu(mn,"Plot balance equations",		0,							submn,0, NULL);
	
	return mn;
}
	
#endif

