#ifndef _STC_C_
#define _STC_C_

#include "allegro.h"
#include "xvin.h"
#include "xvin/xv_tools_lib.h"

/* If you include other plug-ins header do it here	*/
#include "../inout/inout.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "stc.h"
#include "stc_plot.h"
#include "stc_image.h"
#include "CGL_parameters.h"
#include "lyaps_common.h"


// following varaibles are defined extern in stc.h:
char  simulation_parameters_fullfilename[512]="params.txt";
char  simulation_filename_prefix[128]="run0_";



/**********************************************************************/
/* loads parameters of the simulation and lyaps					*/
/* NG, 29/06/2005											*/
/**********************************************************************/
int do_load_simulation_parameters(void)
{
	pltreg	*pr = NULL;
	int	i=0;
	char    *fullfilename;
	
	if(updating_menu_state != 0)	return D_O_K;	
//	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine load the parameters from a text file\n"
					"which usually reads params.txt");
	}
	
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)		return win_printf_OK("cannot find plot region");
	
	i = file_select_ex("Load parameters file", simulation_parameters_fullfilename, "txt", 512, 0, 0);
 	if (i == 0) return D_O_K;
	fullfilename=simulation_parameters_fullfilename;

	i=CGL_general_load_parameters(fullfilename) ;
	if (i!=0) if (win_scanf("error loading CGL general parameters")==CANCEL) return(D_O_K);
	i=numerics_general_load_parameters(fullfilename);
	if (i!=0) if (win_scanf("error loading numerical simulation general parameters")==CANCEL) return(D_O_K);
	i=numerics_extra_load_parameters(fullfilename);
	if (i!=0) if (win_scanf("error loading numerical simulation extra parameters")==CANCEL) return(D_O_K);
/*	i=lyaps_load_parameters(fullfilename);
	if (i!=0) if (win_scanf("error loading Lyaps parameters")==CANCEL) return(D_O_K);
*/	
	return refresh_plot(pr, UNCHANGED);
}

/**********************************************************************/
/* edits parameters of the simulation and lyaps					*/
/* NG, 29/06/2005, working well on 2006-11-09					*/
/**********************************************************************/
int do_edit_simulation_parameters(void)
{
register int 	i;
	pltreg	*pr = NULL;

	if(updating_menu_state != 0)	return D_O_K;	
//	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine load the parameters from a text file\n"
					"which usually reads params.txt");
	}
	
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)		return win_printf_OK("cannot find plot region");
	
	i=win_scanf("{\\color{yellow}\\pt14 CGL general parameters}\n"
			"\\epsilon %8f    L %8f\n"
			"c_1 %8f   c_2 %8f\n   v_g %8f\n\n"
			"{\\color{yellow}\\pt14 Numerical simulation general parameters}\n"
			"dt %8f   n_x %6d   n_y %8d\n"
			"nbpas %8d    nbloop %8d    nb save %8d\n",
			&auto_epsilon, &auto_L, &auto_c1, &auto_c2, &auto_vg,
			&auto_dt, &auto_nx, &auto_ny, &auto_nbpas, &auto_nbloop, &auto_nb_save);
	
	return refresh_plot(pr, UNCHANGED);
}


/**********************************************************************/
/* sets the filenames of the simulation and lyaps					*/
/* NG, 01/07/2005											*/
/**********************************************************************/
int do_set_filenames(void)
{
register int 	i;
	pltreg	*pr = NULL;

	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine sets the filenames\n"
					"which usually start with run01_");
	}
	
	i = win_scanf("prefix for filenames of the simulation : %s", simulation_filename_prefix);

	return refresh_plot(pr, UNCHANGED);
}


int do_print_simulation_infos(void)
{	
	if(updating_menu_state != 0)	return D_O_K;	

	win_printf("simulation used : \n"
			"dt = %g with nbpas = %d\n"
			"ny = %d and nbloop = %d\n\n"
			"so saved data and plots will have\n"
			"{\\color{red}dt = %g} and {\\color{red}%d points}", 
			auto_dt, auto_nbpas, auto_ny, auto_nbloop, real_dt, real_npts);
	
	return(D_O_K);
}




int	stc_main(int argc, char **argv)
{	int i;
  FILE *fic;

	if (argc>1) 
	{	for (i=1; i<argc; i++)
		{	if ( (strcasecmp(argv[i], "auto")==0) || (strcasecmp(argv[i], "-auto")==0) )
			{	fic=fopen(simulation_parameters_fullfilename, "rt");
                if (fic!=NULL)
			    {   fclose(fic);
                    CGL_general_load_parameters(simulation_parameters_fullfilename) ;
				    numerics_general_load_parameters(simulation_parameters_fullfilename);
				    numerics_extra_load_parameters(simulation_parameters_fullfilename);
                }
			}
		}
	}
	
	add_plot_treat_menu_item ("stc", NULL, stc_plot_menu(),  0, NULL);
	add_image_treat_menu_item("stc", NULL, stc_image_menu(), 0, NULL);
	return D_O_K;
}


int	stc_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,  "stc", NULL, NULL);
     	remove_item_to_menu(image_treat_menu, "stc", NULL, NULL);
     	return D_O_K;
}


#endif
