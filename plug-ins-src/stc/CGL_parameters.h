/************************************************************************/
/* management of parameters of the CGLe 				*/
/* this file should be imported whenever the global parameters are used	*/
/*									*/
/* version 4.0.0	11/26/2004 externalized integration		*/
/************************************************************************/
/* Nicolas Garnier	nicolas.garnier@ens-lyon.fr			*/
/************************************************************************/



/****************************************************************************************/
/* following variables are extern, and defined for real in CGL_parameters.c		*/
/****************************************************************************************/
extern  int auto_nx,		/* size of image region = size of destination image 	*/
	auto_ny;
extern float auto_c1, auto_c2, 	/* coefficients of the equation				*/
	auto_vg, auto_epsilon, auto_c0,
	auto_lambda, auto_mu, auto_alpha, auto_beta, auto_L,
	auto_hr, auto_hi,	/* order 5 : (a+ib) |A|^4 A 				*/
	auto_gamma, auto_gammac3, auto_delta, auto_deltac4,
	auto_eta, auto_etac5, auto_nu, auto_nuc6,
	auto_noise_amp;		/* noise level						*/
				/* ou de la derniere equation integree			*/
extern float	auto_dt;	/* pas de temps par defaut				*/
extern int	auto_nbpas;	/* nombre de pas d'integration entre chaque ligne	*/
				/* sauvergardee et avant la premiere ligne		*/
extern int	auto_nbloop;
extern int	auto_nb_save;
extern int	auto_sign;	/* -1 for CGL 3 and +1 for CGL5				*/

extern float	auto_precision;	/* dt = dx*dx/(2*auto_precision) : auto_precision >=1 	*/
				/* une serie de test a montre que 5 suffisait toujours  */
/****************************************************************************************/



/****************************************************************************************/
/* following variables are extern, and defined for real in main.c			*/
/****************************************************************************************/
extern int verbosity,
  integration_function,
  do_save_st,
  auto_go_on,
  do_GC_tracing;

/****************************************************************************************/
/* following variables are extern, and defined for real in main.c (lyaps_Ruelle later)	*/
/****************************************************************************************/
extern int do_lyaps;
extern int do_lyaps_go_on;		// boolean. 
extern int do_lyaps_RE_save_matrices;	// boolean. if 1, then matrix Q in Ruelle-Eckmann scheme is saved,
					// and so is the product of R_i
extern int do_lyaps_RE_finite_time;	// boolean. Do we compute the product of R_i ?
extern int do_lyaps_space;		// real or Fourier space (for definitions of modes) ?
extern int finite_time_lyaps_number;	// number of finite time lyaps to save
extern int do_lyaps_RE_finite_time_reset;
extern int do_lyaps_RE_continuity_vectors;  // do we compute right-eigenvectors of R to disentangle the local lyaps ?


/************************************************************************/
/* declarations of functions : 						*/
/************************************************************************/
int	CGL_general_load_parameters(char *filename);		// CGL3 parameters
int 	CGL_CGL5_load_parameters(char *filename);			// CGL5 parameters
int	numerics_general_load_parameters(char *filename);		// computer projection parameters
int 	numerics_extra_load_parameters(char *filename);		// extra simulation parameters
int 	lyaps_load_parameters(char *filename);			// extra simulation parameters

void	CGL_print_parameters(char *filename);				// print on console the parameters of the CGLe
void	scheme_print_parameters(char *filename);			// print on console the parameters of the GSCN scheme
void code_print_parameters(char *filename);				// print on console the parameters of the main code

void	show_options(void);
int	read_options(int argc,char **argv);

size_t	add_prefix(char *prefix, char *name);
