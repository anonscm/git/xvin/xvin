/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _MKMOVIE_C_
#define _MKMOVIE_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "mkmovie.h"


# define EVENT_N  56

O_i *local_movie_im = NULL;  // the image that is build 
int lmi_nx = 0;              // the point x and y position
int lmi_ny = 0;
unsigned long t0 = 0;

void CALLBACK movie_filling(HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime)
{
  O_i *oi;
  static unsigned long dt = 0, t1;
  float dtm = 0;

  if (local_movie_im == NULL) return;
  t1 = my_uclock();
  if (t0)  
    {
      dt = t1 - t0;
      dtm = 1000*((float)dt)/get_my_uclocks_per_sec();
    }
  t0 = t1;
  oi = local_movie_im;
  if (oi->im.data_type != IS_FLOAT_IMAGE) return;
  if (oi->im.c_f >= oi->im.n_f -1 && lmi_nx >= oi->im.nx && lmi_ny >= oi->im.ny)
    {
      lmi_nx = 0;
      lmi_ny = 0;
      KillTimer(win_get_window(),25);
      local_movie_im = NULL;
    }
  oi->im.pixel[lmi_ny].fl[lmi_nx++] = dtm;
  oi->need_to_refresh |= BITMAP_NEED_REFRESH;
  if (lmi_nx >= oi->im.nx) 
    {
      lmi_nx = 0;
      lmi_ny++;
      if (lmi_ny >= oi->im.ny) 
	{
	  lmi_ny = 0;
	  if (oi->im.c_f >= oi->im.n_f -1)  // we are done
	    {
	      lmi_nx = 0;
	      KillTimer(win_get_window(),25);
	      local_movie_im = NULL;
	    }
	  else switch_frame(oi,oi->im.c_f+1);
	}
    }
  return;
}

int real_time_display(struct one_image *oi, DIALOG *d)
{
  imreg *imr, *ac_imr;
  BITMAP *imb;

  if (win_get_window()!= GetForegroundWindow())       return 0;
  imr = d->dp;
  display_image_stuff_16M(imr,d);
  ac_imr = find_imr_in_current_dialog(NULL);
  if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL) && (ac_imr == imr))
    {
      imb = (BITMAP*)oi->bmp.stuff;
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off - 
	   imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);	
      display_title_message("im %d",oi->im.c_f);
    }
  return 0;
}        


int acquire_new_movie(void)
{
  register int i;
  imreg *imr;
  O_i *oi;
  static int nf = 16, nx = 32, ny = 32, ms = 2;
  

  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  
  i = win_scanf("This routine create a movie measuring timer execution\n"
		"time\n nb. of frames in movie %5d\n"
		"size in x %5d size in y %5d\n"
		"time delay between pixels (ms) %5d\n",&nf,&nx,&ny,&ms);
  if (i == CANCEL) return OFF;

  oi = create_and_attach_movie_to_imr(imr, nx, ny, IS_FLOAT_IMAGE, nf);
  if (oi == NULL)      return win_printf_OK("Can't create dest movie");

  switch_frame(oi,0);
  lmi_nx = 0;
  lmi_ny = 0;
  local_movie_im = oi;
  oi->oi_idle_action = real_time_display;        
  set_oi_horizontal_extend(oi, ((float)8*nx)/512);
  set_oi_vertical_extend(oi, ((float)8*ny)/512);
  //set_zmin_zmax_values(oi, 0, 20);
  set_z_black_z_white_values(oi, 0, 10*ms);
  SetTimer( win_get_window(), EVENT_N, ms, movie_filling);
  return (refresh_image(imr, imr->n_oi - 1));
}




O_i	*mkmovie_image(imreg *imr)
{
	register int i,  k;
	O_i *oid, *oi, *ois = NULL;
	int onx, ony, data_type, nf, size;


	for (i = nf = 0, onx = -1, ony = -1, data_type = -1; i < imr->n_oi; i++)
	  {
	    oi = imr->o_i[i];
	    if (oi->im.movie_on_disk) continue;
	    if (oi->im.n_f > 0) continue;
	    if (onx < 0) 
	      {
		onx = oi->im.nx;
		ois = oi;
	      }
	    else if (onx != oi->im.nx) continue;
	    if (ony < 0) ony = oi->im.ny; 
	    else if (ony != oi->im.ny) continue;
	    if (data_type < 0) data_type = oi->im.data_type; 
	    else if (data_type != oi->im.data_type) continue;
	    nf++;
	  }

	if (onx < 0 || ony < 0 || data_type < 0)
	  {
	    win_printf("No images found for movie");
	    return NULL;
	  }


	if (data_type == IS_CHAR_IMAGE)	                  size = onx * ony;
	else if (data_type == IS_INT_IMAGE)               size = onx * ony * 2;
	else if (data_type == IS_UINT_IMAGE)              size = onx * ony * 2;
	else if (data_type == IS_LINT_IMAGE)              size = onx * ony * 4;
	else if (data_type == IS_FLOAT_IMAGE)             size = onx * ony * 4;
	else if (data_type == IS_DOUBLE_IMAGE)            size = onx * ony * 8;
	else if (data_type == IS_COMPLEX_IMAGE)           size = onx * ony * 8;
	else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)    size = onx * ony * 16;
	else if (data_type == IS_RGB_PICTURE)             size = onx * ony * 3;
	else if (data_type == IS_RGBA_PICTURE)            size = onx * ony * 4;
	else 
	  {
	    win_printf("Unsuported image type");
	    return NULL;
	  }

	oid = create_one_movie(onx, ony, data_type, nf);
	if (oid == NULL)
	{
		win_printf("Can't create dest movie");
		return NULL;
	}

	for (i = 0, k = 0; i < imr->n_oi && k < nf; i++)
	  {
	    oi = imr->o_i[i];
	    if (oi->im.movie_on_disk) continue;
	    if (oi->im.n_f > 0) continue;
	    if (onx != oi->im.nx) continue;
	    if (ony != oi->im.ny) continue;
	    if (data_type != oi->im.data_type) continue;
	    memcpy (oid->im.mem[k],oi->im.mem[0],size);
	  }

	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	return oid;
}

int do_mkmovie_image(void)
{
	O_i *ois, *oid;
	imreg *imr;

	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine make o movie"
		"from identical images in an imr");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	oid = mkmovie_image(imr);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}


MENU *mkmovie_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Make movie from images", do_mkmovie_image,NULL,0,NULL);
	add_item_to_menu(mn,"acquire movie", acquire_new_movie,NULL,0,NULL);

	return mn;
}

int	mkmovie_main(int argc, char **argv)
{
	add_image_treat_menu_item ( "mkmovie", NULL, mkmovie_image_menu(), 0, NULL);
	return D_O_K;
}

int	mkmovie_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "mkmovie", NULL, NULL);
	return D_O_K;
}
#endif

