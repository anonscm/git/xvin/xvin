#ifndef _MKMOVIE_H_
#define _MKMOVIE_H_

PXV_FUNC(int, do_mkmovie_average_along_y, (void));
PXV_FUNC(MENU*, mkmovie_image_menu, (void));
PXV_FUNC(int, mkmovie_main, (int argc, char **argv));
#endif

