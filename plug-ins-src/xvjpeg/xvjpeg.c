/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _JPEG_C_
#define _JPEG_C_

# include "allegro.h"
# include "xvin.h"


#define BIG_FONT                         0        /* FONT */
# define BUILDING_PLUGINS_DLL
# include "xvjpeg.h"

int load_jpeg_file_in_imreg(imreg *imr, char *file, char *path);

int	do_load_jpeg(void)
{
	register int i;
	char path[512], file[256];
	static char fullfile[512], *fu = NULL;	
	imreg *imr = NULL;
	imreg	*create_and_register_new_image_project(int x, int y, int w, int h);
	int d_draw_Im_proc(int msg, DIALOG *d, int c);
	
	if(updating_menu_state != 0)	return D_O_K;		
	if (fu == NULL)
	{
		my_getcwd(fullfile, 512);
		strcat(fullfile,"\\");
	}	

	imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)	
		imr = create_and_register_new_image_project( 0,   32,  900,  668);
/*		imr = build_image_region ( 0,   32,  900,  668, 0);*/
	if (imr == NULL)	return win_printf("could not create imreg");		
	switch_allegro_font(1);
	i = file_select_ex("Load file", fullfile, "jpg;jpeg", 512, 0, 0);
	switch_allegro_font(0);

	if (i != 0) 	
	{
		fu = fullfile;	
		extract_file_name(file, 256, fullfile);
		extract_file_path(path, 512, fullfile);
		strcat(path,"\\");
		load_jpeg_file_in_imreg(imr, file, path);
/*		win_printf("loaded %s\n from %s",pr->one_p->filename,
			backslash_to_slash(pr->one_p->dir));*/
	}
	switch_allegro_font(0);
	broadcast_dialog_message(MSG_DRAW,0);
	return 0;
}


int save_jpeg_image(void)
{
	register int i, j;
	char s[512], c[256], c_dir[256];
	static char *im_dir = NULL;	
	imreg *imr = NULL;
	static int quality = -1;
	int	display_saving_win(char *path, char *pattern, char *file, char *purpose);
	char *my_getcwd(char *path, int size);
	void write_JPEG_file (O_i *oi, char *filename, int quality);

	if(updating_menu_state != 0)	return D_O_K;	
	imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL || imr->one_i == NULL || imr->one_i->im.pixel == NULL)
		return 1;
	if (imr->one_i->filename != NULL) 
	{
		strcpy(c,imr->one_i->filename);
		for (i = 0; i < 256 && c[i] != 0 && c[i] != '.'; i++);
		if (i < 252) sprintf(c+i,".jpg");
		
	}
	else 				strcpy(c,"untitled.jpg");	
	if (imr->one_i->dir != NULL) 	strcpy(c_dir,imr->one_i->dir);
	else 				
	{
		if (im_dir == NULL)
		{
			my_getcwd(c_dir ,DIR_LEN);			
			slash_to_backslash(c_dir);
			im_dir = (char *)calloc(256,sizeof(char));
			strcpy(im_dir,c_dir);
		}
		else strcpy(c_dir,im_dir);
	}
	sprintf(s,"%s\\%s",c_dir,c);
			
	
	switch_allegro_font(1);	
	i = file_select_ex("Save image in Jpeg", s, "jpg", 512, 0, 0);
	switch_allegro_font(0);
	if (i != 0) 	
	{
		extract_file_name(c, 256, s);
		extract_file_path(c_dir, 256, s);
		strcat(c_dir,"\\");

		i =	do_you_want_to_overwrite(s, NULL,NULL);
		if (i == CANCEL) return D_O_K;
		i = win_scanf("Quality (0-100) (<0 -> default) %d",&quality);
		if (i == CANCEL) return D_O_K;
		if (imr->one_i->im.n_f > 1)
		{
			for (i=0 ; c[i] != 0 && c[i] != '.'; i++);
			c[i] = 0;
			for (j = 0; j < imr->one_i->im.n_f; j++)
			{
				display_title_message("image %d",j);
				switch_frame(imr->one_i,j);
				sprintf(s,"%s%s%04d.jpg",c_dir,c,j);
				write_JPEG_file (imr->one_i, s,  quality);
			}
		}
		else write_JPEG_file (imr->one_i, s,  quality);
	}
	switch_allegro_font(0);
	broadcast_dialog_message(MSG_DRAW,0);
	return D_O_K;	
}



XV_FUNC(int, xvjpeg_main, (int argc, char** argv))
{

  add_item_to_menu(image_file_import_menu,"Load Jpeg\t(ctrl+I)", do_load_jpeg, NULL, 0, NULL);
  add_item_to_menu(image_file_export_menu,"Save as Jpeg\t(ctrl+I)", save_jpeg_image, NULL, 0, NULL);	
	return 0;
}

int	xvjpeg_unload(int argc, char **argv)
{ 
        remove_item_to_menu(image_file_import_menu,"Load Jpeg\t(ctrl+I)", NULL, NULL);
        remove_item_to_menu(image_file_export_menu,"Save as Jpeg\t(ctrl+I)", NULL, NULL);
	return D_O_K;
}



#endif
