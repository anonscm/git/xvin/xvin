#ifndef _JPEG_H_
#define _JPEG_H_

PXV_FUNC(int, do_jpeg_average_along_y, (void));
PXV_FUNC(MENU*, jpeg_image_menu, (void));
PXV_FUNC(int, xvjpeg_main, (int argc, char **argv));
PXV_FUNC(int, xvjpeg_unload, (int argc, char **argv));
#endif
