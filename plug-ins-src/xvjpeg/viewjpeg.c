
#include <allegro.h>
#ifdef WIN32
#include <winalleg.h>
#endif
#include <stdio.h>
#include <fcntl.h>
# include "xvin.h"

#include <setjmp.h>


#include "jpeglib.h"

struct my_error_mgr {
  struct jpeg_error_mgr pub;	/* "public" fields */

  jmp_buf setjmp_buffer;	/* for return to caller */
};

typedef struct my_error_mgr * my_error_ptr;

/*
 * Here's the routine that will replace the standard error_exit method:
 */

METHODDEF(void)
my_error_exit (j_common_ptr cinfo)
{
  /* cinfo->err really points to a my_error_mgr struct, so coerce pointer */
  my_error_ptr myerr = (my_error_ptr) cinfo->err;

  /* Always display the message. */
  /* We could postpone this until after returning, if we chose. */
  (*cinfo->err->output_message) (cinfo);

  /* Return control to the setjmp point */
  longjmp(myerr->setjmp_buffer, 1);
}
/*	oi is created but not allocated */

int jpegreadfile(O_i *oi, char *file)
{
	register int i, j;
	struct jpeg_decompress_struct *cinfo = NULL;
	FILE *infile = NULL;
	struct my_error_mgr jerr;
	int onx, ony;
	union pix *pd;
	JSAMPARRAY buffer = NULL;		/* Output row buffer */
	int row_stride;		/* physical row width in output buffer */
	

	if ((infile = fopen(file, "rb")) == NULL) 
	{
	  	fprintf(stderr, "can't open %s\n", file);
		return 0;
	}
	/* Now we can initialize the JPEG decompression object. */
	/* We set up the normal JPEG error routines, then override error_exit. */
	cinfo = (struct jpeg_decompress_struct *)calloc(1,sizeof(struct jpeg_decompress_struct));
	if (cinfo == NULL) return 0;
	cinfo->err = jpeg_std_error(&jerr.pub);
	jerr.pub.error_exit = my_error_exit;
	/* Establish the setjmp return context for my_error_exit to use. */
	if (setjmp(jerr.setjmp_buffer)) {
		/* If we get here, the JPEG code has signaled an error.
		 * We need to clean up the JPEG object, close the input file, and return.
		 */
		jpeg_destroy_decompress(cinfo);
		fclose(infile);
		return 0;
	}


	jpeg_create_decompress(cinfo);

	/* Step 2: specify data source (eg, a file) */

	jpeg_stdio_src(cinfo, infile);

	/* Step 3: read file parameters with jpeg_read_header() */

	(void) jpeg_read_header(cinfo, TRUE);
/*	
	fprintf(stderr,"%s\nimage is %d by %d\noutput is %d by %d\n",file,cinfo.image_width,cinfo.image_height,
	cinfo.output_width,cinfo.output_height);
	fprintf(stderr,"image has %d components\n",cinfo.output_components);
	if (cinfo.jpeg_color_space == JCS_UNKNOWN)			fprintf(stderr,"unknown color\n");
	else if (cinfo.jpeg_color_space == JCS_GRAYSCALE)	fprintf(stderr,"monochromer\n");
	else if (cinfo.jpeg_color_space == JCS_RGB)			fprintf(stderr,"red/green/blue\n");
	else if (cinfo.jpeg_color_space == JCS_YCbCr)		fprintf(stderr,"Y/Cb/Cr (also known as YUV)\n");
	else if (cinfo.jpeg_color_space == JCS_CMYK)		fprintf(stderr,"C/M/Y/K\n");
	else if (cinfo.jpeg_color_space == JCS_YCCK)		fprintf(stderr,"Y/Cb/Cr/K\n");
	else fprintf(stderr,"unknown mode\n");
*/	
	if (cinfo->jpeg_color_space == JCS_GRAYSCALE)
	{
		cinfo->out_color_space = JCS_GRAYSCALE;  
		alloc_one_image (oi, cinfo->image_width, cinfo->image_height, IS_CHAR_IMAGE);
	}
	else
	{
		cinfo->out_color_space = JCS_RGB;
		alloc_one_image (oi, cinfo->image_width, cinfo->image_height, IS_RGB_PICTURE);
	}
	pd = oi->im.pixel;
	onx = oi->im.nx;	ony = oi->im.ny;
/*	set_cursor_glass();	 */

	(void) jpeg_start_decompress(cinfo);
	/* We can ignore the return value since suspension is not possible
	 * with the stdio data source.
	 */

	/* We may need to do some setup of our own at this point before reading
	 * the data.	After jpeg_start_decompress() we have the correct scaled
	 * output image dimensions available, as well as the output colormap
	 * if we asked for color quantization.
	 * In this example, we need to make an output work buffer of the right size.
	 */ 
	/*
	fprintf(stderr,"%s\nimage is %d by %d\noutput is %d by %d\n",file,cinfo.image_width,cinfo.image_height,
	cinfo.output_width,cinfo.output_height);
	fprintf(stderr,"image has %d components\n",cinfo.output_components);
	if (cinfo.jpeg_color_space == JCS_UNKNOWN)			fprintf(stderr,"unknown color\n");
	else if (cinfo.jpeg_color_space == JCS_GRAYSCALE)	fprintf(stderr,"monochromer\n");
	else if (cinfo.jpeg_color_space == JCS_RGB)			fprintf(stderr,"red/green/blue\n");
	else if (cinfo.jpeg_color_space == JCS_YCbCr)		fprintf(stderr,"Y/Cb/Cr (also known as YUV)\n");
	else if (cinfo.jpeg_color_space == JCS_CMYK)		fprintf(stderr,"C/M/Y/K\n");
	else if (cinfo.jpeg_color_space == JCS_YCCK)		fprintf(stderr,"Y/Cb/Cr/K\n");
	else fprintf(stderr,"unknown mode\n");
	*/	
	
	/* JSAMPLEs per row in output buffer */
	row_stride = cinfo->output_width * cinfo->output_components;
	/* Make a one-row-high sample array that will go away when done with image */
	buffer = (*cinfo->mem->alloc_sarray)
		((j_common_ptr) cinfo, JPOOL_IMAGE, row_stride, 1);	
	while (cinfo->output_scanline < cinfo->output_height) {
		/* jpeg_read_scanlines expects an array of pointers to scanlines.
	 	* Here the array is only one element long, but you could ask for
	 	* more than one scanline at a time if that's more convenient.
	 	*/
		(void) jpeg_read_scanlines(cinfo,buffer , 1);
		
		/* Assume put_scanline_someplace wants a pointer and sample count. */

		if (cinfo->jpeg_color_space == JCS_GRAYSCALE)
		{
			for (i = 0; i < onx; i++)
				pd[ony - cinfo->output_scanline].ch[i] = (unsigned char)buffer[0][i];
			j++;
		}
		else
		{					
			for (i = 0; i < 3*onx; i++)
				pd[ony - cinfo->output_scanline].ch[i] = (unsigned char)buffer[0][i];
			j++;
		}
		
	}

	/* Step 7: Finish decompression */

	(void) jpeg_finish_decompress(cinfo);
	/* We can ignore the return value since suspension is not possible
	 * with the stdio data source.
	 */
	
	
	
	jpeg_destroy_decompress(cinfo);
	free (cinfo);
	cinfo = NULL;
	/* After finish_decompress, we can close the input file.
	 * Here we postpone it until after no more JPEG errors are possible,
	 * so as to simplify the setjmp error logic above.  (Actually, I don't
	 * think that jpeg_destroy can do an error exit, but why assume anything...)
	 */
	fclose(infile);
/*	reset_cursor();		*/
	oi->width = (float)onx/512;
	oi->height = (float)ony/512;
	oi->iopt2 &= ~X_NUM;		oi->iopt2 &= ~Y_NUM;		
	oi->im.source = Mystrdupre(oi->im.source,file);	
	find_zmin_zmax(oi);
	return 0;
}



int load_jpeg_file_in_imreg(imreg *imr, char *file, char *path)
{
	O_i *oi = NULL;
	char f_in[256];

	if (imr == NULL || file == NULL || path == NULL)		return 1;	
	if (imr->n_oi == 1 && imr->one_i->im.pixel == NULL) 	oi = imr->one_i;
	else
	{
		oi = (O_i *)calloc(1,sizeof(O_i));
		init_one_image(oi, 0);
		add_image (imr, IS_CHAR_IMAGE, (void *)oi);
	}
	oi->iopt |= NOAXES;
	sprintf(f_in,"%s%s",path,file);
	imr->cur_oi = imr->n_oi - 1;
	imr->one_i = imr->o_i[imr->cur_oi];
	if ( jpegreadfile(oi,f_in) == 0)
	{
		oi->filename = Mystrdupre(oi->filename,file);
		oi->dir = Mystrdupre(oi->dir,path);
/*		win_printf("source %s",(oi->im.source == NULL)?"null":oi->im.source);*/
		if (oi->im.source == NULL)		oi->im.source = strdup(file);
/*		if (image_special_2_use != NULL) image_special_2_use(oi);*/
		do_one_image(imr);		
/*		refresh_image(imr, imr->n_oi - 1);	 */
	}
	else
	{	
		remove_from_image (imr, oi->im.data_type, (void *)oi);
		win_printf("file %s \n not loaded",backslash_to_slash(f_in));
		return 1;
	}
	return 0;
}
void write_JPEG_file (O_i *oi, char * filename, int quality)
{
  /* This struct contains the JPEG compression parameters and pointers to
   * working space (which is allocated as needed by the JPEG library).
   * It is possible to have several such structures, representing multiple
   * compression/decompression processes, in existence at once.  We refer
   * to any one struct (and its associated working data) as a "JPEG object".
   */
  struct jpeg_compress_struct cinfo;
  /* This struct represents a JPEG error handler.  It is declared separately
   * because applications often want to supply a specialized error handler
   * (see the second half of this file for an example).  But here we just
   * take the easy way out and use the standard error handler, which will
   * print a message on stderr and call exit() if compression fails.
   * Note that this struct must live as long as the main JPEG parameter
   * struct, to avoid dangling-pointer problems.
   */
  struct jpeg_error_mgr jerr;
  /* More stuff */
  FILE * outfile;		/* target file */
  JSAMPROW row_pointer[1];	/* pointer to JSAMPLE row[s] */
  int row_stride;		/* physical row width in image buffer */
  unsigned char *uc;
  register int i, j;
	int onx, ony;
	union pix *pd;
	pd = oi->im.pixel;

	onx = oi->im.nx;	ony = oi->im.ny;
/*	set_cursor_glass();	*/

  /* Step 1: allocate and initialize JPEG compression object */

  /* We have to set up the error handler first, in case the initialization
   * step fails.  (Unlikely, but it could happen if you are out of memory.)
   * This routine fills in the contents of struct jerr, and returns jerr's
   * address which we place into the link field in cinfo.
   */
  cinfo.err = jpeg_std_error(&jerr);
  /* Now we can initialize the JPEG compression object. */
  jpeg_create_compress(&cinfo);

  /* Step 2: specify data destination (eg, a file) */
  /* Note: steps 2 and 3 can be done in either order. */

  /* Here we use the library-supplied code to send compressed data to a
   * stdio stream.  You can also write your own code to do something else.
   * VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
   * requires it in order to write binary files.
   */
  if ((outfile = fopen(filename, "wb")) == NULL) {
    fprintf(stderr, "can't open %s\n", filename);
    exit(1);
  }
  jpeg_stdio_dest(&cinfo, outfile);

  /* Step 3: set parameters for compression */

  /* First we supply a description of the input image.
   * Four fields of the cinfo struct must be filled in:
   */
  cinfo.image_width = onx; 	/* image width and height, in pixels */
  cinfo.image_height = ony;
  cinfo.input_components = 1;		/* # of color components per pixel */
  if (oi->im.data_type == IS_RGB_PICTURE)
	cinfo.in_color_space = JCS_RGB;
  else if (oi->im.data_type == IS_CHAR_IMAGE)		
	cinfo.in_color_space = JCS_GRAYSCALE;
  else
  {
	win_printf("Cannot handle this type of image!");
	return;
  }
 	/* colorspace of input image */
  /* Now use the library's routine to set default compression parameters.
   * (You must set at least cinfo.in_color_space before calling this,
   * since the defaults depend on the source color space.)
   */
  jpeg_set_defaults(&cinfo);
  /* Now you can set any non-default parameters you wish to.
   * Here we just illustrate the use of quality (quantization table) scaling:
   */
	if (quality >= 0)
	  jpeg_set_quality(&cinfo, quality, TRUE /* limit to baseline-JPEG values */);

  /* Step 4: Start compressor */

  /* TRUE ensures that we will write a complete interchange-JPEG file.
   * Pass TRUE unless you are very sure of what you're doing.
   */
  jpeg_start_compress(&cinfo, TRUE);

  /* Step 5: while (scan lines remain to be written) */
  /*           jpeg_write_scanlines(...); */

  /* Here we use the library's state variable cinfo.next_scanline as the
   * loop counter, so that we don't have to keep track ourselves.
   * To keep things simple, we pass one scanline per call; you can pass
   * more if you wish, though.
   */
  row_stride = onx;	/* JSAMPLEs per row in image_buffer */
	row_stride = (oi->im.data_type == IS_RGB_PICTURE) ? 3*onx : onx;

	uc = (unsigned char*)calloc(row_stride,sizeof(unsigned char));
	if (uc == NULL) return 1;

	
  while (cinfo.next_scanline < cinfo.image_height) {
    /* jpeg_write_scanlines expects an array of pointers to scanlines.
     * Here the array is only one element long, but you could pass
     * more than one scanline at a time if that's more convenient.
     */
    for (i = 0; i < row_stride; i++)
    {
      j = 256*(pd[ony - cinfo.next_scanline - 1].ch[i] - oi->z_min)/(oi->z_max-oi->z_min);
      uc[i] = (j < 0) ? 0 : (j > 255) ? 255 : j;
    }
    row_pointer[0] = uc; // (pd[ony - cinfo.next_scanline - 1].ch);
    (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
  }

  /* Step 6: Finish compression */

  jpeg_finish_compress(&cinfo);
  /* After finish_compress, we can close the output file. */
  fclose(outfile);

  /* Step 7: release JPEG compression object */

  /* This is an important step since it will release a good deal of memory. */
  jpeg_destroy_compress(&cinfo);
  free(uc);
  /* And we're done! */
}


void write_BW_JPEG_file (O_i *oi, char *filename, int quality)
{
  struct jpeg_compress_struct cinfo;
  struct jpeg_error_mgr jerr;
  /* More stuff */
  FILE * outfile;		/* target file */
  JSAMPROW row_pointer[1];	/* pointer to JSAMPLE row[s] */
  int row_stride;		/* physical row width in image buffer */
	register int i, j;
	int onx, ony, nf = 0, z, black, white;
	union pix *pd;
	O_i *oid = NULL;
		


	if (oi->im.data_type != IS_CHAR_IMAGE)
	{
		win_printf("I can only treat BW images !");
		return;
	}
	black = (int)oi->z_black;
	white = (int)oi->z_white;
	if ((white - black) == 0)
	{	
		win_printf("the black level equal the white one!");
		return;
	}
	if ( oi->im.n_f > 0)
	{
		nf = oi->im.n_f;
		oi->im.n_f = 0;
	}
	oid = duplicate_image(oi, NULL);
	if (nf > 0) oi->im.n_f = nf;
	onx = oi->im.nx;	ony = oi->im.ny;
	
	
	for (i = 0; i< ony ; i++)
	{
		for (j = 0; j < onx ; j++)
		{
			z = oi->im.pixel[i].ch[j];
			z = (256*(z -  black))/(white-black);
			z = (z > 255) ? 255 : z;
			z = (z < 0) ? 0 : z;
			oid->im.pixel[i].ch[j] = z;
		}
	}	
	pd = oid->im.pixel;
	
/*	set_cursor_glass();	*/
  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_compress(&cinfo);
  if ((outfile = fopen(filename, "wb")) == NULL) {
    fprintf(stderr, "can't open %s\n", filename);
    exit(1);
  }
  jpeg_stdio_dest(&cinfo, outfile);
  cinfo.image_width = onx; 	/* image width and height, in pixels */
  cinfo.image_height = ony;
  cinfo.input_components = 1;		/* # of color components per pixel */
  cinfo.in_color_space = JCS_GRAYSCALE;; 	/* colorspace of input image */
  jpeg_set_defaults(&cinfo);
	if (quality >= 0)
	  jpeg_set_quality(&cinfo, quality, TRUE /* limit to baseline-JPEG values */);
  jpeg_start_compress(&cinfo, TRUE);
  row_stride = onx;	/* JSAMPLEs per row in image_buffer */

  while (cinfo.next_scanline < cinfo.image_height) {
    row_pointer[0] = (pd[ony - cinfo.next_scanline - 1].ch);
    (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
  }
  jpeg_finish_compress(&cinfo);
  fclose(outfile);
  jpeg_destroy_compress(&cinfo);
	free_one_image(oid);
  /* And we're done! */
}

