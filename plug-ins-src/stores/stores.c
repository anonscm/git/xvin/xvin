/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _STORES_C_
#define _STORES_C_

# include "allegro.h"
# include "xvin.h"


/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "fft32d.h"
# include "stores.h"

resosto	*find_resosto_in_op(O_p *op) 
{
	register int i;
	d_s *ds;
	
	if (op == NULL) return NULL;
	for ( i = 0; i < op->n_dat; i++)
	{
		ds = op->dat[i];
		if ( ds->use.to == IS_STORES )	return (resosto *)ds->use.stuff;
	}
	return NULL;
}


int	simulate_one_time_step(resosto *r, int niter)
{
	register int i, re, ii;
	int q2, q;
	double rec = 0, imc = 0, req = 0, imq = 0, nq2, nq, t;

/*	win_printf("nb pts %d kc1d %g Dc1 %g iter %d\nh %g c1[0] %g",r->nx,r->kc1d,
	r->Dc1,r->niter,r->h,r->c1[0]); */

	for (; niter > 0; niter--)
	{

		for (i= 0; i < r->nx; i++)
		{
			re = 2*i;
			ii = re+1;
			q = (i < r->nx/2) ? i : (i - r->nx);
			q2 = q * q;
			nq2 = ((double)q2*64)/(r->nx*r->nx);
			nq = ((double)q*8)/r->nx;
			t = r->h * r->niter;
			t /= r->period;
			t -= (int)t;
			if (t >= 0.25 && t < 0.75) nq *= -1;
			
			rec = r->c1[re];
			imc = r->c1[ii];
			req = r->q1[re];
			imq = r->q1[ii];
				
			r->k1c1r = r->h * (r->kc1f * req - r->kc1d * rec);
			r->k1q1r = -r->k1c1r;
			r->k1c1r += r->h * (- nq2 * r->Dc1 * rec - nq * r->vc1 * imc);
			r->k1q1r += r->h * (- nq2 * r->Dq1 * req - nq * r->vq1 * imq);
		
			r->k1c1i = r->h * ( r->kc1f * imq - r->kc1d * imc);
			r->k1q1i = -r->k1c1i;
			r->k1c1i += r->h * (- nq2 * r->Dc1 * imc + nq * r->vc1 * rec);
			r->k1q1i += r->h * (- nq2 * r->Dq1 * imq + nq * r->vq1 * req);

			rec = r->c1[re] + r->k1c1r/2;
			imc = r->c1[ii] + r->k1c1i/2;
			req = r->q1[re] + r->k1q1r/2;
			imq = r->q1[ii] + r->k1q1i/2;
				
			r->k2c1r = r->h * (r->kc1f * req - r->kc1d * rec);
			r->k2q1r = -r->k2c1r;
			r->k2c1r += r->h * (- nq2 * r->Dc1 * rec - nq * r->vc1 * imc);
			r->k2q1r += r->h * (- nq2 * r->Dq1 * req - nq * r->vq1 * imq);
		
			r->k2c1i = r->h * (r->kc1f * imq - r->kc1d * imc);
			r->k2q1i = -r->k2c1i;
			r->k2c1i += r->h * (- nq2 * r->Dc1 * imc + nq * r->vc1 * rec);
			r->k2q1i += r->h * (- nq2 * r->Dq1 * imq + nq * r->vq1 * req);

			rec = r->c1[re] + r->k2c1r/2;
			imc = r->c1[ii] + r->k2c1i/2;
			req = r->q1[re] + r->k2q1r/2;
			imq = r->q1[ii] + r->k2q1i/2;
				
			r->k3c1r = r->h * (r->kc1f * req - r->kc1d * rec);
			r->k3q1r = -r->k3c1r;
			r->k3c1r += r->h * (- nq2 * r->Dc1 * rec - nq * r->vc1 * imc);
			r->k3q1r += r->h * (- nq2 * r->Dq1 * req - nq * r->vq1 * imq);
		
			r->k3c1i = r->h * (r->kc1f * imq - r->kc1d * imc );
			r->k3q1i = -r->k3c1i;
			r->k3c1i += r->h * (- nq2 * r->Dc1 * imc + nq * r->vc1 * rec);
			r->k3q1i += r->h * (- nq2 * r->Dq1 * imq + nq * r->vq1 * req);
		
			rec = r->c1[re] + r->k3c1r;
			imc = r->c1[ii] + r->k3c1i;
			req = r->q1[re] + r->k3q1r;
			imq = r->q1[ii] + r->k3q1i;
				
			r->k4c1r = r->h * (r->kc1f * req - r->kc1d * rec);
			r->k4q1r = -r->k4c1r;
			r->k4c1r += r->h * (- nq2 * r->Dc1 * rec - nq * r->vc1 * imc);
			r->k4q1r += r->h * (- nq2 * r->Dq1 * req - nq * r->vq1 * imq);
		
			r->k4c1i = r->h * (r->kc1f * imq - r->kc1d * imc);
			r->k4q1i = -r->k4c1i;
			r->k4c1i += r->h * (- nq2 * r->Dc1 * imc + nq * r->vc1 * rec);
			r->k4q1i += r->h * (- nq2 * r->Dq1 * imq + nq * r->vq1 * req);

			r->c1[re] +=  (r->k1c1r + 2*r->k2c1r + 2*r->k3c1r + r->k4c1r)/6;
			r->c1[ii] +=  (r->k1c1i + 2*r->k2c1i + 2*r->k3c1i + r->k4c1i)/6;
			r->q1[re] +=  (r->k1q1r + 2*r->k2q1r + 2*r->k3q1r + r->k4q1r)/6;
			r->q1[ii] +=  (r->k1q1i + 2*r->k2q1i + 2*r->k3q1i + r->k4q1i)/6;

		}
		r->niter++;
	}
	return r->niter;
}


int do_simulate_fronts(void)
{
	register int i, j;
	O_p  *opn = NULL;
	static int  n_sto = 0, nx = 256, nt = 16;
	static float kc1f = .001, kc1d = .001, vc1 = 1,	vq1 = 2, Dc1 = 0.01;
	static float Dq1 = 0.02, h = 0.02, per = 5;	
	d_s *dsc1, *dsq1;
	pltreg *pr = NULL;
	resosto *r = NULL;

	if(updating_menu_state != 0)	return D_O_K;	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine simulate fronts in an alternating"
	"electric field and place it in a new plot");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
		return win_printf_OK("cannot find data");
	
	r = (resosto*)calloc(1,sizeof(resosto));	
	if (r == NULL)	return win_printf_OK("cannot create structure");

	i = win_scanf("Stochastic resonance enhanced diffusion"
		"Number of points %d"
		"kf_{C1}^f %f Kb_{C1}^d %f V_{C1} %f Diffusion coefficient D_{C1} %f"
		"V_{Q1} %f Diffusion coefficient D_{Q1} %fperiod of modulation %f"
		"step in time %fnb. of time steps between display%d",&nx,&kc1f,
		&kc1d,&vc1,&Dc1,&vq1,&Dq1,&per,&h,&nt);
	if (i == CANCEL)	return OFF;

	r->nx = nx;
	r->kc1f = kc1f;
	r->kc1d = kc1d;
	r->vc1 = vc1;
	r->vq1 = vq1;
	r->Dc1 = Dc1;
	r->Dq1 = Dq1;
	r->h = h;
	r->period = per;
	r->nboucle = nt;

	if ((opn = create_and_attach_one_plot(pr, r->nx, r->nx, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	dsc1 = opn->dat[0];
	if ((dsq1 = create_and_attach_one_ds(opn, r->nx, r->nx, 0)) == NULL)
		return win_printf_OK("cannot create plot !");

	r->c1 = (double*)calloc(2*r->nx,sizeof(double));
	r->q1 = (double*)calloc(2*r->nx,sizeof(double));
	r->ft = (double*)calloc(2*r->nx,sizeof(double));
				
	if (r->c1 == NULL || r->q1 == NULL || r->ft == NULL)
		return win_printf_OK("cannot create double arrays !");
	/* we create the initial condition */
	
	for (j = 0; j < r->nx; j++)
	{
		if ((2*j) == nx-6)			r->c1[2*j] = r->q1[2*j] =  .2;		
		else if ((2*j) == nx-4)		r->c1[2*j] = r->q1[2*j] =  .5;
		else if ((2*j) == nx-2)		r->c1[2*j] = r->q1[2*j] =  .8;
		else if ((2*j) == nx)		r->c1[2*j] = r->q1[2*j] =  1;
		else if ((2*j) == nx+2)		r->c1[2*j] = r->q1[2*j] =  .8;		
		else if ((2*j) == nx+4)		r->c1[2*j] = r->q1[2*j] =  .5;
		else if ((2*j) == nx+6)		r->c1[2*j] = r->q1[2*j] =  .2;		
		else r->c1[2*j] = r->q1[2*j] =  0;		
		dsc1->yd[j] = dsq1->yd[j] = r->c1[2*j];
		r->c1[2*j+1] = r->q1[2*j+1] = 0;
		dsc1->xd[j] = dsq1->xd[j] = j;
	}
	fft_init_d(2*r->nx);
	/* we fft the initial conditions */
    fft_d(2*r->nx, r->c1, 1);
    fft_d(2*r->nx, r->q1, 1);
	
	dsc1->use.stuff = (void*)r;
	dsc1->use.to = IS_STORES;


	fft_init_d(2*r->nx);

	for (i = 0; i < 2*r->nx; i++) r->ft[i] = r->c1[i];
    fft_d(2*r->nx, r->ft, -1);
	for (i = 0; i < r->nx; i++) dsc1->yd[i] = r->ft[2*i];
		
	for (i = 0; i < 2*r->nx; i++) r->ft[i] = r->q1[i];
    fft_d(2*r->nx, r->ft, -1);
	for (i = 0; i < r->nx; i++) dsq1->yd[i] = r->ft[2*i];
			

	/* now we must do some house keeping */
	dsc1->source = my_sprintf(dsc1->source,"Stochastic resonance diffusion C1");
	dsq1->source = my_sprintf(dsq1->source,"Stochastic resonance diffusion Q1");	
	set_plot_title(opn, "Stochastic resonance diffusion");
	set_plot_x_title(opn, "Position X along profile");
	set_plot_y_title(opn, "C1 and Q1");
	set_op_filename(opn, "Sto-diff%03d.gr", n_sto++);
	opn->y_lo = -0.2;
	opn->y_hi = 1.1;
	opn->width = 1.3;
	opn->iopt2 |= Y_LIM;
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

int	iterate_stores_diff(void)
{
	register int i, j, k;
	O_p *op = NULL;
	d_s *dsc1, *dsq1;
	pltreg *pr = NULL;
	resosto *r = NULL;
	float min = 0, max = 1;
	int index = 0;
	static int ni = 128;

	if(updating_menu_state != 0)	return D_O_K;		
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine simulate by n iterates fronts in an\n"
		"alternating electric field and place it in a new plot");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
		return win_printf_OK("cannot find data");
	
	r = find_resosto_in_op(op);
	if (r == NULL) return win_printf_OK("cannot find data");

	dsc1 = find_source_specific_ds_in_op(op, "Stochastic resonance diffusion C1");
	if (dsc1 == NULL) return win_printf_OK("cannot find data set C1");
	dsq1 = find_source_specific_ds_in_op(op, "Stochastic resonance diffusion Q1");		if (dsq1 == NULL) return win_printf_OK("cannot find data set Q1");
	
	k = 1;
	index = RETRIEVE_MENU_INDEX;
	if (index)
	{
		i = win_scanf("Number of ierations %d",&ni);
		if (i == CANCEL)	return D_O_K; 
		k = ni;
	}

	
	fft_init_d(2*r->nx);

	for (j = 0; j < k; j++)
	{
		simulate_one_time_step(r, r->nboucle);
		for (i = 0; i < 2*r->nx; i++) r->ft[i] = r->c1[i];
    	fft_d(2*r->nx, r->ft, -1);
		for (i = 0; i < r->nx; i++) 
		{
			dsc1->yd[i] = r->ft[2*i];
			if (i == 0 || min > dsc1->yd[i])  min = dsc1->yd[i];
			if (i == 0 || max < dsc1->yd[i])  max = dsc1->yd[i];
		}
		
		for (i = 0; i < 2*r->nx; i++) r->ft[i] = r->q1[i];
    	fft_d(2*r->nx, r->ft, -1);
		for (i = 0; i < r->nx; i++) 
		{
			dsq1->yd[i] = r->ft[2*i];
			if (min > dsq1->yd[i])  min = dsq1->yd[i];
			if (max < dsq1->yd[i])  max = dsq1->yd[i];			
		}
		if ((max - min) < ((op->y_hi - op->y_lo)*.66) 
			|| min < op->y_lo || max > op->y_hi )
		{	
			op->y_lo = min - 0.03*(max-min);
			op->y_hi = max + 0.03*(max-min);
			op->iopt2 |= Y_LIM;			
		}
			

		set_plot_title(op, "Stochastic resonance diffusion at %g",r->h*r->niter);

	/* refisplay the entire plot */
		refresh_plot(pr, UNCHANGED);
	}
	return D_O_K;	
	
}

MENU *stores_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"create simulation", do_simulate_fronts,NULL,0,NULL);
	add_item_to_menu(mn,"iterate diffusion", iterate_stores_diff,NULL,0,NULL);
	add_item_to_menu(mn,"iterate N diffusion", iterate_stores_diff,NULL,MENU_INDEX(1),NULL);

	return mn;
}

int	stores_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "Stochastic Diffusion", NULL, stores_plot_menu(), 0, NULL);
	do_simulate_fronts();
	return D_O_K;
}
int	stores_unload(int argc, char **argv)
{ 
        remove_item_to_menu(image_treat_menu, "Stochastic Diffusion", NULL, NULL);
	return D_O_K;
}

#endif
