#ifndef _STORES_H_
#define _STORES_H_

#define IS_STORES		5010


typedef struct _resosto
{
	int 	niter;			/* nb of time iteration in a set 			*/
	int 	nboucle;		/* the nb of iter between profile display 	*/
	double 	h;				/* the step 								*/
	double 	period;			/* the forcing period 						*/
	int		nx;				/* nb of points along the profile 			*/
	double 	*c1, *q1, *ft;	/* The profile for C1 and Q1 + a temp array	*/
	float 	kc1f;			/* The forward chemical constant 			*/
	float 	kc1d;			/* The backward chemical constant 			*/
	float	vc1;			/* The electrophorese velocity of c1		*/
	float	vq1;			/* The electrophorese velocity of q1		*/
	float	Dc1;			/* The Diffusion coefficient of C1 			*/
	float 	Dq1;			/* The Diffusion coefficient of Q1			*/
	double k1c1r, k2c1r, k3c1r, k4c1r;
	double k1q1r, k2q1r, k3q1r, k4q1r;
	double k1c1i, k2c1i, k3c1i, k4c1i;
	double k1q1i, k2q1i, k3q1i, k4q1i;		
} resosto;

PXV_FUNC(resosto*, find_resosto_in_op, (O_p *op));
PXV_FUNC(int, simulate_one_time_step, (resosto *r, int niter));
PXV_FUNC(int, do_simulate_fronts, (void));
PXV_FUNC(int, iterate_stores_diff, (void));
PXV_FUNC(MENU*, stores_plot_menu, (void));
PXV_FUNC(int, stores_main, (int argc, char **argv));

#endif
