#ifndef _FFT32D_H_
#define _FFT32D_H_
/*	fftl32n.h  
 *	
 *	5 fev 1992	JMF
 *
 *	header for fftl32n.c
 *		set of routines for fast Fourier transforms
 *		in 32 bits with the DOS EXTENDER
 */

# include "platform.h"

# ifndef _FFT32D_C_
PXV_ARRAY(double, fftsin_d);
PXV_ARRAY(int, mix_d);
# endif

PXV_FUNC(int, fft_init_d, (int npts));
PXV_FUNC(int, fftmixing_d, (int npts, double *x));
PXV_FUNC(int, fft_d, (int npts, double *x, int df));
PXV_FUNC(void, realtr1_d, (int npts, double *x));
PXV_FUNC(void, realtr2_d, (int npts, double *x, int df));
PXV_FUNC(int, fftwindow_d, (int npts, double *x));
PXV_FUNC(int, fftwindow1_d, (int npts, double *x, double smp));
PXV_FUNC(int, defftwindow1_d, (int npts, double *x, double smp));
PXV_FUNC(int, fftwc_d, (int npts, double *x));
PXV_FUNC(int, fftwc1_d, (int npts, double *x, double smp));
PXV_FUNC(int, defftwc_d, (int npts, double *x));
PXV_FUNC(int, defftwc1_d, (int npts, double *x, double smp));
PXV_FUNC(void, spec_real_d, (int npts, double *x, double *spe));
PXV_FUNC(void, spec_comp_d, (int npts, double *x, double *spe));
PXV_FUNC(int, demodulate_d, (int npts, double *x, int freq));
PXV_FUNC(void, derive_real_d, (int npts, double *x, double *y));
PXV_FUNC(void, amp_d, (int npts, double *x, double *y));

# endif
