/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _GIFAVG_C_ 
#define _GIFAVG_C_

# include "allegro.h"

# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "GifAvg.h"
#include "../../src/menus/plot/treat/p_treat_basic.h"
//place here other headers of this plugin 

//PXV_FUNC(int, 	sort_ds_along_x, (d_s *ds));
int oi_mouse_GifAvg_action(struct one_image *oi, int x0, int y0, int mode, DIALOG *d);
O_i 	*convert_float_image_to_expanded_RGB_char(O_i *dest, O_i *ois1, int exp, float inR, float inG, float inB);
O_i 	*convert_float_image_extremum_to_expanded_RGB_char(O_i *dest, O_i *ois1, int expa, double sigma, float inR, float inG, float inB);
int	find_best_fit_param_on_9x9_LM(double **zd, int verbose, float error_a, float error_b, gaussian_fit_params *g_fit_prm, double *sigma, int n_sig  )  ;


# define IS_IM_EXREMUM 357951

static double thres_s = 1000, tolerance_s = 500, sigma_s = 1.4;
static float err_a_s = 0.15, err_b_s = 200;

int find_in_im_extrema(im_ext_ar *iea, int x, int y, int tolerence,int frame_number)/*find if there is a maximum in image frame number close to xy with a tolerence  tolerence*/
{
  int i;
  if (iea == NULL || iea->ie == NULL) return -1;
  if (iea->n_ie == 0) return 0;
  for (i = iea->n_ie - 1; i >= 0; i--)
    {  // we found a maximum already existing
      if ((x > iea->ie[i].x0 - tolerence) && (x < iea->ie[i].x1 + tolerence) && 
	  (y > iea->ie[i].y0 - tolerence) && (y < iea->ie[i].y1 + tolerence) && (iea->ie[i].inb == frame_number))
	return i;
    } 
  // no maximum known
  return iea->n_ie;
}

int do_GifAvg_average_along_y(void)
{
	int i, j;
	int l_s, l_e;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average the image intensity"
			"of several lines of an image");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	return D_O_K;
	/* we grap the data that we need, the image and the screen region*/
	l_s = 0; l_e = ois->im.ny;
	/* we ask for the limit of averaging*/
	i = win_scanf("Average Between lines%d and %d",&l_s,&l_e);
	if (i == CANCEL)	return D_O_K;
	/* we create a plot to hold the profile*/
	op = create_one_plot(ois->im.nx,ois->im.nx,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0]; /* we grab the data set */
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	op->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;
	inherit_from_im_to_ds(ds, ois);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"GifAvg averaged profile from line %d to %d",l_s, l_e);
	set_formated_string(&ds->treatement,"%s averaged profile from line %d to %d",l_s, l_e);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	for (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, ds->xd);
		for (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];
	}
	for (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)
	{
		ds->xd[i] = i;
		ds->yd[i] /= j;
	}
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}

O_i	*GifAvg_image_multiply_by_a_scalar(O_i *ois, float factor)
{
	int i, j;
	O_i *oid;
	float tmp;
	int onx, ony, data_type;
	union pix *ps, *pd;

	onx = ois->im.nx;	ony = ois->im.ny;	data_type = ois->im.data_type;
	oid =  create_one_image(onx, ony, data_type);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	if (data_type == IS_CHAR_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_INT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].in[j];
				pd[i].in[j] = (tmp < -32768) ? -32768 :
					((tmp > 32767) ? 32767 : (short int)tmp);
			}
		}
	}
	else if (data_type == IS_UINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ui[j];
				pd[i].ui[j] = (tmp < 0) ? 0 :
					((tmp > 65535) ? 65535 : (unsigned short int)tmp);
			}
		}
	}
	else if (data_type == IS_LINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].li[j];
				pd[i].li[j] = (tmp < 0x10000000) ? 0x10000000 :
					((tmp > 0x7FFFFFFF) ? 0x7FFFFFFF : (int)tmp);
			}
		}
	}
	else if (data_type == IS_FLOAT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[j] = factor * ps[i].fl[j];
			}
		}
	}
	else if (data_type == IS_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[j] = factor * ps[i].db[j];
			}
		}
	}
	else if (data_type == IS_COMPLEX_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[2*j] = factor * ps[i].fl[2*j];
				pd[i].fl[2*j+1] = factor * ps[i].fl[2*j+1];
			}
		}
	}
	else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[2*j] = factor * ps[i].db[2*j];
				pd[i].db[2*j+1] = factor * ps[i].db[2*j+1];
			}
		}
	}
	else if (data_type == IS_RGB_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 3*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_RGBA_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 4*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s rescaled by %g",
		 ois->title,factor);
	set_formated_string(&oid->im.treatement,
		"Image %s rescaled by %g", ois->filename,factor);
	return oid;
}

int do_GifAvg_image_multiply_by_a_scalar(void)
{
	O_i *ois, *oid;
	imreg *imr;
	int i;
	static float factor = 2.0;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("define the factor of mutiplication %f",&factor);
	if (i == CANCEL)	return D_O_K;
	oid = GifAvg_image_multiply_by_a_scalar(ois,factor);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}




/*
  fit parameter a in y = a*exp(-x/tau) + b
  by minimisation of chi^2

 */

int	find_best_a_an_b_of_exp_fit(d_s *dsi,    // input data set
				    int verbose, // issue explicit error message
				    int error_type, // the type of error in data
				    double tau,  // the imposed tau value
				    double *a,   // the best a value
				    double *b,   // the best b value
				    double *E)   // the chi^2
{	
  int i, j;
  double fy, f, x, y, fx, er, la, lb;
  double sy = 0, sy2 = 0, sfx = 0, sfx2 = 0, sfxy = 0, sig_2, ny = 0;

  if (dsi == NULL) 
    {
      if (verbose) win_printf("No valid data \n"); 
      return 1;
    }
  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = exp(-x/tau);
      if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
      else if (error_type == 1) er = fx;
      else er = 1;
      if (er > 0)
	{
	  sig_2 = (double)1/(er*er);
	  sy += y*sig_2;
	  sy2 += y*y*sig_2;
	  sfx += fx*sig_2;
	  sfx2 += fx*fx*sig_2;
	  sfxy += y*fx*sig_2;
	  ny += sig_2;
	}
    }
  lb = (sfx2 * ny) - sfx * sfx;
  if (lb == 0) 
    {
      if (verbose) win_printf_OK("\\Delta  = 0 \n can't fit that!");
      return 3;
    }
  la = sfxy * ny - sfx * sy;
  la /= lb;
  lb = (sfx2 * sy - sfx * sfxy)/lb;
  if (a) *a = la;
  if (b) *b = lb;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = la*exp(-x/tau) + lb;
      if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
      else if (error_type == 1) er = fx;
      else er = 1;
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }	
  return 0;
}




/*
  fit parameter a in y = a*exp(-x/tau) + b
  by minimisation of chi^2

 */

int	find_best_a_an_b_of_exp_fit_er_affine(d_s *dsi,    // input data set
					      int verbose, // issue explicit error message
					      double error_a, // affine coef for error in data
					      double error_b, // the offset  error in data
					      double tau,  // the imposed tau value
					      double *a,   // the best a value
					      double *b,   // the best b value
					      double *E)   // the chi^2
{	
  int i, j;
  double fy, f, x, y, fx, er, la, lb;
  double sy = 0, sy2 = 0, sfx = 0, sfx2 = 0, sfxy = 0, sig_2, ny = 0;

  if (dsi == NULL) 
    {
      if (verbose) win_printf("No valid data \n"); 
      return 1;
    }
  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = exp(-x/tau);
      er = error_b + error_a * fabs(y);
      if (er > 0)
	{
	  sig_2 = (double)1/(er*er);
	  sy += y*sig_2;
	  sy2 += y*y*sig_2;
	  sfx += fx*sig_2;
	  sfx2 += fx*fx*sig_2;
	  sfxy += y*fx*sig_2;
	  ny += sig_2;
	}
    }
  lb = (sfx2 * ny) - sfx * sfx;
  if (lb == 0) 
    {
      if (verbose) win_printf_OK("\\Delta  = 0 \n can't fit that!");
      return 3;
    }
  la = sfxy * ny - sfx * sy;
  la /= lb;
  lb = (sfx2 * sy - sfx * sfxy)/lb;
  if (a) *a = la;
  if (b) *b = lb;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = la*exp(-x/tau) + lb;
      er = error_b + error_a * fabs(y);
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }	
  return 0;
}



int	do_fit_a_exp_t_over_tau_plus_b_with_er(void)
{
  int i;
  char st[256];
  pltreg *pr;
  O_p *op;
  d_s *dsi, *dsd;
  float x, tmp, xmin, xmax;
  int nf = 1024;
  double a, b, dt, E = 0, E1 = 0, ert, tauu;
  static int match_x = 0, error_type = 0;
  static double tau = 0.1, err = 0.0001;
  
  if(updating_menu_state != 0)	return D_O_K;	
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	
    return win_printf_OK("cannot find data");
  if (dsi->ye != NULL)
    {
      error_type = 0;
      i = win_scanf("Fit y = a.exp (-x/\\tau) +b \n"
		    "\\tau  starting value %12lf\n"
		    "Relative error on \\tau  %12lf\n"
		    "Error type in data: error bars %R f(x) %r constant %r\n"
		    "fitted curve x matching data %b\n"
		    ,&tau,&err,&error_type,&match_x);
        if (i == CANCEL)	return OFF;
    }
  else
    {
      i = win_scanf("Fit y = a.exp (-x/\\tau) +b \n"
		    "\\tau  starting value %12lf\n"
		    "Relative error on \\tau  %12lf\n"
		    "Error type in data: f(x) %R constant %r\n"
		    "fitted curve x matching data %b\n"
		    ,&tau,&err,&error_type,&match_x);
      if (i == CANCEL)	return OFF;
      error_type++;
    }


  for (i = 0, xmin = xmax = dsi->xd[0]; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      xmin = (x < xmin) ? x : xmin;
      xmax = (x > xmax) ? x : xmax;
    }	

  tauu = (op->dx > 0) ? tau/op->dx : tau;


  i = find_best_a_an_b_of_exp_fit(dsi, 1, error_type, tauu, &a, &b, &E);
  if (i)
    {
      win_printf("Error %d",i);
      return D_O_K;
    }
  dt = tauu/10;
  ert = tauu*err;
  for (i = 0; fabs(dt) > ert && i < 1024; i++)
    {
      tauu += dt;
      find_best_a_an_b_of_exp_fit(dsi, 1, error_type, tauu, &a, &b, &E1);
      if (E1 > E)   dt = -dt/2;
      E = E1;
    }
  if (i > 1023)
    win_printf("%d iter \\tau = %g, a = %g b = %g \\chi^2 = %g",i,tauu,a,b,E);
  if (match_x) nf = dsi->nx;
  if ((dsd = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create data set");
  tau = tauu*op->dx;
  win_printf("\\tau = %g, \\tau_u %g",tau, tauu);
  if (match_x)
    {
      for (i = 0; i < dsi->nx; i++)
	{
	  x = dsi->xd[i];
	  dsd->xd[i] = x;
	  dsd->yd[i] = a*exp(-x/tauu) +b;
	}	
    }
  else
    {
      tmp = (xmax-xmin)/40;
      xmin -= tmp;
      xmax += tmp;
      for (i = 0, tmp = xmin ; i < dsd->nx; i++, tmp += (xmax-xmin)/dsd->nx)	
	{
	  dsd->xd[i] = (float)(tmp);
	  dsd->yd[i] = a*exp(-tmp/tauu) +b;
	}		
    }
  b = op->ay+(op->dy*b);
  a = a*op->dy;
  
  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl y = a.exp(-t/\\tau)}"
	  "{a =  %g%s}{b = %g%s}{\\tau  = %g%s}{1/\\tau  = %g}"
	  "{\\chi^2 = %g, n = %d}}}"
	  ,a,(op->y_unit) ? op->y_unit:"",b,(op->y_unit) ? op->y_unit:" ",tau
	  ,(op->x_unit) ? op->x_unit:"",((tau!=0)?(double)1/tau:0),E,dsi->nx-3);
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4, 
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD,st);	 
  return refresh_plot(pr,pr->cur_op);			
}






int do_movie_avg_starting_at_every(void)
{
  O_i *ois, *oid;
  imreg *imr;
  int nx, ny, nf, i, im, j, k;
  static int s_a = 0, n_a = 2;
  float *z = NULL, avg;
  union pix *pd; 

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average in time a movie 0ne frame every n frames starting at a specific frame");
    }
  s_a = ois->im.c_f;
  i = win_scanf("This routine average in time a movie\n"
		"0ne frame every %5d frames\n"
		"starting at frame %4d\n",&n_a,&s_a);
  if (i == CANCEL)	return D_O_K;

  oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE);
  if (oid == NULL)      
    return win_printf_OK("Can't create dest movie");


  for (i = 0, pd = oid->im.pixel; i < ny ; i++)
    {
      for (j=0; j< nx; j++)
	{
	  z = extract_z_profile_from_movie(ois, j, i, z);
	  for (im = s_a, avg = 0, k = 0; im < nf; im += n_a, k++)
	      avg += z[im]; 
	  avg /= k;
	  pd[i].fl[j] = avg;
	}
    }
  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);
  set_formated_string(&oid->im.treatement,"movie avg starting at %d every %d of %s ",s_a,n_a, ois->filename);
  find_zmin_zmax(oid);
  free(z);
  return (refresh_image(imr, imr->n_oi - 1));
}




int do_movie_avg_starting_at_every_fit_exp(void)
{
  O_i *ois, *oid;
  imreg *imr;
  O_p *opi;
  d_s *ds, *dsi, *dsa;
  int nx, ny, nf, i, im, j, k, nhi;
  static int s_a = 0, n_a = 2, fillb = 0, error_type = 0, bin = 100, maxZ = 65536;
  float *z = NULL;
  union pix *pd; 
  double a, b,  E = 0, moy = 0;
  static double tau = 30;
 


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;
  s_a = ois->im.c_f;
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average in time a movie\n"
			   "0ne frame every n frames starting at a specific frame\n"
			   "and fit data to a*exp(t/tau) + b");
    }

  i = win_scanf("This routine average in time a movie\n"
		"0ne frame every %5d frames\n"
		"starting at frame %4d\n"
		"Then fit a and b in expression :\n"
		" y = a.exp (-x/\\tau) + b \n"
		"\\tau   value %12lf\n"
		"Error type in data: error bars %R f(x) %r constant %r\n"
		"fill resulting image with %R->a or %r->b\n"
		"bin size to measure noise %8d max value %12d\n"
		,&n_a,&s_a,&tau,&error_type,&fillb,&bin,&maxZ);
  if (i == CANCEL)	return D_O_K;
  nhi = 1+(maxZ/bin);
  ds = build_data_set(nf,nf);
  oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE);
  if (ds == NULL || oid == NULL)      
    return win_printf_OK("Can't create dest movie");
  opi = create_and_attach_op_to_oi(oid, nhi, nhi, 0, 0);
  if (opi == NULL)    return win_printf_OK("Can't create histo plot");
  dsi = opi->dat[0];
  dsa = create_and_attach_one_ds(opi, nhi, nhi, 0);

  for (i = 0, pd = oid->im.pixel; i < ny ; i++)
    {
      for (j=0; j< nx; j++)
	{
	  z = extract_z_profile_from_movie(ois, j, i, z);
	  for (im = s_a+1, k = 0, moy = 0; im < nf; im += n_a, k++)
	    moy += z[im]; 
	  moy = (k) ? moy/k: moy;
	  for (im = s_a, k = 0; im < nf; im += n_a, ds->xd[k] = k, k++)
	    {
	      ds->yd[k] = z[im] - moy; 
	      ds->xd[k] = im;
	    }
	  ds->nx = ds->ny = k;
	  if (find_best_a_an_b_of_exp_fit(ds, 1, error_type, tau, &a, &b, &E) == 0)
	      pd[i].fl[j] = (fillb) ? (float)b : (float)a;
	  for (im = 1; im < ds->nx; im++)
	    {
	      k = (int)(0.5 + (ds->yd[im-1] + ds->yd[im])/(2*bin));
	      if (k>= 0 && k < dsi->nx)
		{
		  dsi->yd[k] += (ds->yd[im-1] - ds->yd[im])*(ds->yd[im-1] - ds->yd[im])/2;
		  dsi->xd[k] += 1;
		}
	    }
	}
    }
  for (im = dsi->nx-1; dsi->xd[im] < 10; im--);
  dsi->nx = dsi->ny = dsa->nx = dsa->ny = im+1;
  for (im = 0; im < dsi->nx; im++)
    {
      dsa->yd[im] = dsi->xd[im];
      dsi->yd[im] /= (dsi->xd[im] > 0) ? dsi->xd[im] : 1; 
      dsa->xd[im] = dsi->xd[im] = (0.5 + im) *bin; 
    }
  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);
  set_formated_string(&oid->im.treatement,"Exp projection (y = a.exp(t/%g) + b), parameter %c "
		      "on movie avg starting at %d every %d of %s ",tau,(fillb)?'b':'a',s_a,n_a, ois->filename);
  find_zmin_zmax(oid);
  free(z);
  return (refresh_image(imr, imr->n_oi - 1));
}




int do_movie_avg_starting_at_every_fit_exp_RGB16(void)
{
  O_i *ois, *oid;
  imreg *imr;
  O_p *opi = NULL;
  d_s *ds = NULL, *dsi = NULL, *dsa = NULL;
  int nx, ny, nf, i, im, j, k, nhi;
  static int s_a = 0, n_a = 2, fillb = 0,  bin = 100, maxZ = 65536, offset = 0, draw_noise = 1;
  float *z = NULL;
  union pix *pd; 
  double a, b,  E = 0, moy = 0;
  short int usi; 
  static double tau = 30, error_a = 0.15, error_b = 200;
 


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;
  s_a = ois->im.c_f;
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average in time a movie\n"
			   "0ne frame every n frames starting at a specific frame\n"
			   "and fit data to a*exp(t/tau) + b");
    }

  i = win_scanf("This routine average in time a movie\n"
		"0ne frame every %5d frames\n"
		"starting at frame %4d\n"
		"Then fit a and b in expression :\n"
		" y = a.exp (-x/\\tau) + b \n"
		"\\tau   value %12lf\n"
		"Error = er_b + er_a * y ; er_a = %10lf er_b = %10lf\n"
		"fill resulting image with %R->a or %r->b\n"
		"Add this offset to RGB16 image %10d\n"
		"Add noise plot %b\n"
		"bin size to measure noise %8d max value %12d\n"
		,&n_a,&s_a,&tau,&error_a,&error_b,&fillb,&offset,&draw_noise,&bin,&maxZ);
  if (i == CANCEL)	return D_O_K;
  nhi = 1+(maxZ/bin);
  ds = build_data_set(nf,nf);
  oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_RGB16_PICTURE);
  if (ds == NULL || oid == NULL)      
    return win_printf_OK("Can't create dest movie");
  oid->im.mode = TRUE_RGB;
  if (draw_noise)
    {
      opi = create_and_attach_op_to_oi(oid, nhi, nhi, 0, 0);
      if (opi == NULL)    return win_printf_OK("Can't create histo plot");
      dsi = opi->dat[0];
      dsa = create_and_attach_one_ds(opi, nhi, nhi, 0);
    }

  for (i = 0, pd = oid->im.pixel; i < ny ; i++)
    {
      for (j=0; j< nx; j++)
	{
	  z = extract_z_profile_from_movie(ois, j, i, z);
	  for (im = s_a+1, k = 0, moy = 0; im < nf; im += n_a, k++)
	    moy += z[im]; 
	  moy = (k) ? moy/k: moy;
	  for (im = s_a, k = 0; im < nf; im += n_a, ds->xd[k] = k, k++)
	    {
	      ds->yd[k] = z[im] - moy; 
	      ds->xd[k] = im;
	    }
	  ds->nx = ds->ny = k;
	  if (find_best_a_an_b_of_exp_fit_er_affine(ds, 1, error_a, error_b, tau, &a, &b, &E) == 0)
	    {
	      usi = (fillb) ? (short int)(b + offset) : (short int)(a + offset);
	      pd[i].rgb16[j].r = usi;
	      pd[i].rgb16[j].g = usi;
	      pd[i].rgb16[j].b = usi;
	    }
	  for (im = 1; im < ds->nx; im++)
	    {
	      k = (int)(0.5 + (ds->yd[im-1] + ds->yd[im])/(2*bin));
	      if (draw_noise && k >= 0 && dsi != NULL && k < dsi->nx)
		{
		  dsi->yd[k] += (ds->yd[im-1] - ds->yd[im])*(ds->yd[im-1] - ds->yd[im])/2;
		  dsi->xd[k] += 1;
		}
	    }
	}
    }
  if (draw_noise)
    {
      for (im = dsi->nx-1; dsi->xd[im] < 10; im--);
      dsi->nx = dsi->ny = dsa->nx = dsa->ny = im+1;
      for (im = 0; im < dsi->nx; im++)
	{
	  dsa->yd[im] = dsi->xd[im];
	  dsi->yd[im] /= (dsi->xd[im] > 0) ? dsi->xd[im] : 1; 
	  dsa->xd[im] = dsi->xd[im] = (0.5 + im) *bin; 
	}
    }
  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);
  set_formated_string(&oid->im.treatement,"Exp projection (y = a.exp(t/%g) + b), parameter %c "
		      "on movie avg starting at %d every %d of %s ",tau,(fillb)?'b':'a',s_a,n_a, ois->filename);
  find_zmin_zmax(oid);
  free(z);
  free_data_set(ds);
  return (refresh_image(imr, imr->n_oi - 1));
}




int do_movie_avg_starting_at_every_fit_tau_exp(void)
{
  O_i *ois, *oid;
  imreg *imr;
  d_s *ds;
  int nx, ny, nf, i, im, j, k, l;
  static int s_a = 0, n_a = 2, fillb = 0, error_type = 0;
  float *z = NULL;
  union pix *pd; 
  double a, b, dt, E = 0, E1, ert, tauf;
  static double tau = 30, thres = 1000, err = 0.0001, mint = 10, maxt = 50;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }		
  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  else   active_menu->flags &= ~D_DISABLED;

  if(updating_menu_state != 0)	return D_O_K;
  i = ois->im.c_f;
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)	
    {
      win_printf("I do not handle complexe images ! yet");
      return D_O_K;
    }
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average in time a movie\n"
			   "0ne frame every n frames starting at a specific frame\n"
			   "and fit data to a*exp(t/tau) + b");
    }
  s_a = ois->im.c_f;
  i = win_scanf("This routine average in time a movie\n"
		"0ne frame every %5d frames\n"
		"starting at frame %4d\n"
		"Then fit \\tau, a or b in expression :\n"
		" y = a.exp (-x/\\tau) + b \n"
		"For pixel where I_s - I_{last} > %8lf\n"
		"\\tau   starting value %12lf\n"
		"Error type in data: error bars %R f(x) %r constant %r\n"
		"fill resulting image with %R->a or %r->b or %r->\\tau\n"
		"keep data if \\tau in [%8lf,%8lf]\n"
		,&n_a,&s_a,&thres,&tau,&error_type,&fillb,&mint,&maxt);
  if (i == CANCEL)	return D_O_K;
  ds = build_data_set(nf,nf);
  oid = create_and_attach_oi_to_imr(imr, nx, ny, IS_FLOAT_IMAGE);
  if (ds == NULL || oid == NULL)      
    return win_printf_OK("Can't create dest movie");


  for (i = 0, pd = oid->im.pixel; i < ny ; i++)
    {
      for (j=0; j< nx; j++)
	{
	  z = extract_z_profile_from_movie(ois, j, i, z);
	  for (im = s_a, k = 0; im < nf; im += n_a, ds->xd[k] = k, k++)
	    {
	      ds->yd[k] = z[im]; 
	      ds->xd[k] = im;
	    }
	  ds->nx = ds->ny = k;
	  if ((ds->yd[0] - ds->yd[k-1]) > thres)
	    {
	      if (find_best_a_an_b_of_exp_fit(ds, 1, error_type, tau, &a, &b, &E) != 0)
		my_set_window_title("error in fit at poitnt %d, %d",i,j);
	      dt = tau/10;
	      ert = tau*err;
	      for (l = 0, tauf = tau; fabs(dt) > ert && l < 1024; l++)
		{
		  //tauf += dt;
		  if ((tauf + dt) < 1 || (tauf + dt) > (double)ds->nx) break;
		  if (find_best_a_an_b_of_exp_fit(ds, 1, error_type, tauf + dt, &a, &b, &E1) != 0)
		    my_set_window_title("error in fit at poitnt %d, %d",i,j);
		  if (E1 > E)   dt = -dt/2;
		  else 
		    {
		      tauf += dt;
		      E = E1;
		    }     
		}
	      if (l > 1023)		
		my_set_window_title("%d iter \\tau = %g, a = %g b = %g \\chi^2 = %g at pixel %d, %d",l,tauf,a,b,E,i,j);
	      if (tauf > mint && tauf < maxt)
		{ 
		  if (fillb == 0) 		pd[i].fl[j] = (float)a;
		  else if (fillb == 1)      pd[i].fl[j] = (float)b;
		  else if (fillb == 2)      pd[i].fl[j] = (float)tauf;
		}
	    }
	}
    }
  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s ", ois->title);
  set_formated_string(&oid->im.treatement,"Exp projection (y = a.exp(t/%g) + b), parameter %c "
		      "on movie avg starting at %d every %d of %s ",tau,(fillb)?'b':'a',s_a,n_a, ois->filename);
  find_zmin_zmax(oid);
  free(z);
  return (refresh_image(imr, imr->n_oi - 1));
}

int extract_x_y_avg_profiles_in_oi(O_i *oi, int ix, int iy, double xp[], int size_x, int wx, double yp[], int size_y, int wy)
{
  int mode, *li, ixp, iyp;
  unsigned char *ch;
  rgb_t *rgb;
  rgb16_t *rgb16;
  rgba_t *rgba;
  rgba16_t *rgba16;
  short int *in;
  unsigned short int *ui;
  float *fl, zr, zi;
  double *db, t, zdr, zdi;

  
  if (oi == NULL)	return 2;

  if (ix < 0 || iy < 0) return 1;
  if ((ix + size_x >  oi->im.nx) || (iy + size_y >  oi->im.ny)) return 1;

  mode = oi->im.mode;

  for (iyp = 0; iyp < size_y; iyp++) yp[iyp] = 0;
  for (ixp = 0; ixp < size_x; ixp++) xp[ixp] = 0;

  if ( oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  ch = oi->im.pixel[iy+iyp].ch;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += (double) (ch[ix+ixp]);
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += (double) (ch[ix+ixp]);
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGB_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgb = oi->im.pixel[iy+iyp].rgb;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    xp[ixp] += ((double)(rgb[ix+ixp].r + rgb[ix+ixp].g + rgb[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	xp[ixp] += (double)rgb[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	xp[ixp] += (double)rgb[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	xp[ixp] += (double)rgb[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  xp[ixp] += (double)255;
		  else return 1;
		}
	      if (abs(2*ixp - size_x) <= wx)  
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    yp[iyp] += ((double)(rgb[ix+ixp].r + rgb[ix+ixp].g + rgb[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	yp[iyp] += (double)rgb[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	yp[iyp] += (double)rgb[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	yp[iyp] += (double)rgb[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  yp[iyp] += (double)255;
		  else return 1;
		}
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGBA_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgba = oi->im.pixel[iy+iyp].rgba;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    xp[ixp] += ((double)(rgba[ix+ixp].r + rgba[ix+ixp].g + rgba[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	xp[ixp] += (double)rgba[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	xp[ixp] += (double)rgba[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	xp[ixp] += (double)rgba[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  xp[ixp] += (double)rgba[ix+ixp].a;
		  else return 1;
		}
	      if (abs(2*ixp - size_x) <= wx)  
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    yp[iyp] += ((double)(rgba[ix+ixp].r + rgba[ix+ixp].g + rgba[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	yp[iyp] += (double)rgba[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	yp[iyp] += (double)rgba[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	yp[iyp] += (double)rgba[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  yp[iyp] += (double)rgba[ix+ixp].a;
		  else return 1;
		}
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGB16_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgb16 = oi->im.pixel[iy+iyp].rgb16;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    xp[ixp] += ((double)(rgb16[ix+ixp].r + rgb16[ix+ixp].g + rgb16[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	xp[ixp] += (double)rgb16[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	xp[ixp] += (double)rgb16[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	xp[ixp] += (double)rgb16[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  xp[ixp] += (double)255;
		  else return 1;
		}
	      if (abs(2*ixp - size_x) <= wx)  
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    yp[iyp] += ((double)(rgb16[ix+ixp].r + rgb16[ix+ixp].g + rgb16[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	yp[iyp] += (double)rgb16[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	yp[iyp] += (double)rgb16[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	yp[iyp] += (double)rgb16[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  yp[iyp] += (double)255;
		  else return 1;
		}
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGBA16_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgba16 = oi->im.pixel[iy+iyp].rgba16;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    xp[ixp] += ((double)(rgba16[ix+ixp].r + rgba16[ix+ixp].g + rgba16[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	xp[ixp] += (double)rgba16[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	xp[ixp] += (double)rgba16[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	xp[ixp] += (double)rgba16[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  xp[ixp] += (double)rgba16[ix+ixp].a;
		  else return 1;
		}
	      if (abs(2*ixp - size_x) <= wx)  
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    yp[iyp] += ((double)(rgba16[ix+ixp].r + rgba16[ix+ixp].g + rgba16[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	yp[iyp] += (double)rgba16[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	yp[iyp] += (double)rgba16[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	yp[iyp] += (double)rgba16[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  yp[iyp] += (double)rgba16[ix+ixp].a;
		  else return 1;
		}
	    }
	}
    }
  else if ( oi->im.data_type == IS_INT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  in = oi->im.pixel[iy+iyp].in;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += (double) (in[ix+ixp]);
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += (double) (in[ix+ixp]);
	    }
	}
    }
  else if ( oi->im.data_type == IS_UINT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  ui = oi->im.pixel[iy+iyp].ui;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += (double) (ui[ix+ixp]);
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += (double) (ui[ix+ixp]);
	    }
	}
    }
  else if ( oi->im.data_type == IS_LINT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  li = oi->im.pixel[iy+iyp].li;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += (double) (li[ix+ixp]);
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += (double) (li[ix+ixp]);
	    }
	}
    }
  else if ( oi->im.data_type == IS_FLOAT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  fl = oi->im.pixel[iy+iyp].fl;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += (double) (fl[ix+ixp]);
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += (double) (fl[ix+ixp]);
	    }
	}
    }
  else if ( oi->im.data_type == IS_DOUBLE_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  db = oi->im.pixel[iy+iyp].db;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += (double) (db[ix+ixp]);
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += (double) (db[ix+ixp]);
	    }
	}
    }
  else if ( oi->im.data_type == IS_COMPLEX_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  fl = oi->im.pixel[iy+iyp].fl;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      zr = fl[2*(ix+ixp)];
	      zi = fl[2*(ix+ixp)+1];
	      if (mode == RE)		t = (double)zr;
	      else if (mode == IM)	t = (double)zi;
	      else if (mode == AMP)	t = sqrt(zr*zr+zi*zi);
	      else if (mode == AMP_2)	t = (double)(zr*zr+zi*zi);
	      else if (mode == LOG_AMP)	t = (zr*zr+zi*zi > 0) ? log10(zr*zr+zi*zi) : -40.0;	    
	      else return 3;
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += t;
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += t;
	    }
	}
    }
  else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  db = oi->im.pixel[iy+iyp].db;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      zdr = db[2*(ix+ixp)];
	      zdi = db[2*(ix+ixp)+1];
	      if (mode == RE)		t = zdr;
	      else if (mode == IM)	t = zdi;
	      else if (mode == AMP)	t = sqrt(zdr*zdr+zdi*zdi);
	      else if (mode == AMP_2)	t = (zdr*zdr+zdi*zdi);
	      else if (mode == LOG_AMP)	t = (zdr*zdr+zdi*zdi > 0) ? log10(zdr*zdr+zdi*zdi) : -40.0;	    
	      else return 3;
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += t;
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += t;
	    }
	}
    }
  else return 1;
  return 0;
}


int extract_2D_double_array_in_oi(O_i *oi, int ix, int iy, double **zd, int size_x, int size_y)
{
  int mode, *li, ixp, iyp;
  unsigned char *ch;
  rgb_t *rgb;
  rgb16_t *rgb16;
  rgba_t *rgba;
  rgba16_t *rgba16;
  short int *in;
  unsigned short int *ui;
  float *fl, zr, zi;
  double *db, t, zdr, zdi;

  
  if (oi == NULL)	return 2;

  if (ix < 0 || iy < 0) return 1;
  if ((ix + size_x >  oi->im.nx) || (iy + size_y >  oi->im.ny)) return 1;

  mode = oi->im.mode;

  if ( oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  ch = oi->im.pixel[iy+iyp].ch;
	  for (ixp = 0; ixp < size_x; ixp++)
	    zd[iyp][ixp] = (double) (ch[ix+ixp]);
	}
    }
  else 	if ( oi->im.data_type == IS_RGB_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgb = oi->im.pixel[iy+iyp].rgb;
	  for (ixp = 0; ixp < size_x; ixp++)
	    {
	      if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		zd[iyp][ixp] = ((double)(rgb[ix+ixp].r + rgb[ix+ixp].g + rgb[ix+ixp].b))/3;
	      else if (oi->im.mode & R_LEVEL)	    zd[iyp][ixp] = (double)rgb[ix+ixp].r;
	      else if (oi->im.mode & G_LEVEL)	    zd[iyp][ixp] = (double)rgb[ix+ixp].g;
	      else if (oi->im.mode & B_LEVEL)	    zd[iyp][ixp] = (double)rgb[ix+ixp].b;
	      else if (oi->im.mode == ALPHA_LEVEL)  zd[iyp][ixp] = (double)255;
	      else return 1;
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGBA_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgba = oi->im.pixel[iy+iyp].rgba;
	  for (ixp = 0; ixp < size_x; ixp++)
	    {
	      if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		zd[iyp][ixp] = ((double)(rgba[ix+ixp].r + rgba[ix+ixp].g + rgba[ix+ixp].b))/3;
	      else if (oi->im.mode & R_LEVEL)	     zd[iyp][ixp] = (double)rgba[ix+ixp].r;
	      else if (oi->im.mode & G_LEVEL)	     zd[iyp][ixp] = (double)rgba[ix+ixp].g;
	      else if (oi->im.mode & B_LEVEL)	     zd[iyp][ixp] = (double)rgba[ix+ixp].b;
	      else if (oi->im.mode == ALPHA_LEVEL)   zd[iyp][ixp] = (double)rgba[ix+ixp].a;
	      else return 1;
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGB16_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgb16 = oi->im.pixel[iy+iyp].rgb16;
	  for (ixp = 0; ixp < size_x; ixp++)
	    {
	      if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		zd[iyp][ixp] = ((double)(rgb16[ix+ixp].r + rgb16[ix+ixp].g + rgb16[ix+ixp].b))/3;
	      else if (oi->im.mode & R_LEVEL)	    zd[iyp][ixp] = (double)rgb16[ix+ixp].r;
	      else if (oi->im.mode & G_LEVEL)	    zd[iyp][ixp] = (double)rgb16[ix+ixp].g;
	      else if (oi->im.mode & B_LEVEL)	    zd[iyp][ixp] = (double)rgb16[ix+ixp].b;
	      else if (oi->im.mode == ALPHA_LEVEL)  zd[iyp][ixp] = (double)255;
	      else return 1;
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGBA16_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgba16 = oi->im.pixel[iy+iyp].rgba16;
	  for (ixp = 0; ixp < size_x; ixp++)
	    {
	      if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		zd[iyp][ixp] = ((double)(rgba16[ix+ixp].r + rgba16[ix+ixp].g + rgba16[ix+ixp].b))/3;
	      else if (oi->im.mode & R_LEVEL)	     zd[iyp][ixp] = (double)rgba16[ix+ixp].r;
	      else if (oi->im.mode & G_LEVEL)	     zd[iyp][ixp] = (double)rgba16[ix+ixp].g;
	      else if (oi->im.mode & B_LEVEL)	     zd[iyp][ixp] = (double)rgba16[ix+ixp].b;
	      else if (oi->im.mode == ALPHA_LEVEL)   zd[iyp][ixp] = (double)rgba16[ix+ixp].a;
	      else return 1;
	    }
	}
    }
  else if ( oi->im.data_type == IS_INT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  in = oi->im.pixel[iy+iyp].in;
	  for (ixp = 0; ixp < size_x; ixp++)
	    zd[iyp][ixp] = (double) (in[ix+ixp]);
	}
    }
  else if ( oi->im.data_type == IS_UINT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  ui = oi->im.pixel[iy+iyp].ui;
	  for (ixp = 0; ixp < size_x; ixp++)
	    zd[iyp][ixp] = (double) (ui[ix+ixp]);
	}
    }
  else if ( oi->im.data_type == IS_LINT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  li = oi->im.pixel[iy+iyp].li;
	  for (ixp = 0; ixp < size_x; ixp++)
	    zd[iyp][ixp] = (double) (li[ix+ixp]);
	}
    }
  else if ( oi->im.data_type == IS_FLOAT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  fl = oi->im.pixel[iy+iyp].fl;
	  for (ixp = 0; ixp < size_x; ixp++)
	    zd[iyp][ixp] = (double) (fl[ix+ixp]);
	}
    }
  else if ( oi->im.data_type == IS_DOUBLE_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  db = oi->im.pixel[iy+iyp].db;
	  for (ixp = 0; ixp < size_x; ixp++)
	    zd[iyp][ixp] = (double) (db[ix+ixp]);
	}
    }
  else if ( oi->im.data_type == IS_COMPLEX_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  fl = oi->im.pixel[iy+iyp].fl;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      zr = fl[2*(ix+ixp)];
	      zi = fl[2*(ix+ixp)+1];
	      if (mode == RE)		t = (double)zr;
	      else if (mode == IM)	t = (double)zi;
	      else if (mode == AMP)	t = sqrt(zr*zr+zi*zi);
	      else if (mode == AMP_2)	t = (double)(zr*zr+zi*zi);
	      else if (mode == LOG_AMP)	t = (zr*zr+zi*zi > 0) ? log10(zr*zr+zi*zi) : -40.0;	    
	      else return 3;
	      zd[iyp][ixp] = t;
	    }
	}
    }
  else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  db = oi->im.pixel[iy+iyp].db;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      zdr = db[2*(ix+ixp)];
	      zdi = db[2*(ix+ixp)+1];
	      if (mode == RE)		t = zdr;
	      else if (mode == IM)	t = zdi;
	      else if (mode == AMP)	t = sqrt(zdr*zdr+zdi*zdi);
	      else if (mode == AMP_2)	t = (zdr*zdr+zdi*zdi);
	      else if (mode == LOG_AMP)	t = (zdr*zdr+zdi*zdi > 0) ? log10(zdr*zdr+zdi*zdi) : -40.0;	    
	      else return 3;
	      zd[iyp][ixp] = t;
	    }
	}
    }
  else return 1;
  return 0;
}



int add_im_extremum(im_ext_ar *iea, double x, double y, double data, double derivate, double chi2, double sigma, int frame_numb)
{
  im_ext *ie;
  //  static int verbose = D_O_K;

  if (iea == NULL || iea->ie == NULL) return -1;
  //if (verbose != CANCEL)
  //  verbose = win_printf("adding pixel to maximun i_ie %d n_ie %d\n x %d y %d, val %g\n"
  //			 "Pressing CANCEL will supress further message\n",i_ie,iea->n_ie,x,y,data);
  if (iea->n_ie >= iea->m_ie)
    {
      iea->m_ie = alloc_im_ext_array(iea, 2*iea->m_ie);
      if (iea->m_ie <= 0) return -2;
    }
  ie = iea->ie + iea->n_ie;
  if (data > ie->zmax)
    {
      ie->zmax = data;
      ie->dnb = derivate;
      ie->chi2 = chi2;
      ie->sigma = sigma;
      ie->xm = x;
      ie->ym = y;
    } 
  ie->xpos = x;
  ie->ypos = y;
  ie->np++;
  ie->x0 = (int)(x+0.5) - 2;
  ie->x1 = (int)(x+0.5) + 2;
  ie->y0 = (int)(y+0.5) - 2;
  ie->y1 = (int)(y+0.5) + 2;
  ie->inb = frame_numb;
  iea->n_ie++;
  //win_printf("max num %d \nframe max %d\n %d",iea->n_ie,iea->ie[iea->n_ie-1].inb,frame_numb);
  return (iea->n_ie - 1);
}


# define	INDEX_OUT_OF_RANGE	3
# define	PB_WITH_EXTREMUM	2
# define	TOO_MUCH_NOISE		4
# define	GETTING_REF_PROFILE	1


/*	Find maximum position in a 1d array of 5 points with x[2] greater than its neighbours
 *	return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int	find_local_max(double *x, double *Max_pos, double *Max_val, double *Max_deriv)
{
  int   ret = 0, delta;
  double a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0;

  f_2 = x[0];
  f_1 = x[1];
  f0 = x[2];
  f1 = x[3];
  f2 = x[4];
  if (f_1 < f1)
    {
      a = -f_1/6 + f0/2 - f1/2 + f2/6;
      b = f_1/2 - f0 + f1/2;
      c = -f_1/3 - f0/2 + f1 - f2/6;
      d = f0;
      delta = 0;
    }	
  else
    {
      a = b = c = 0;	
      a = -f_2/6 + f_1/2 - f0/2 + f1/6;
      b = f_2/2 - f_1 + f0/2;
      c = -f_2/3 - f_1/2 + f0 - f1/6;
      d = f_1;
      delta = -1;
    }
  if (fabs(a) < 1e-8)
    {
      if (b != 0)	xm =  - c/(2*b) - (3 * a * c * c)/(4 * b * b * b);
      else
	{
	  xm = 0;
	  ret |= PB_WITH_EXTREMUM;
	}
    }
  else if ((b*b - 3*a*c) < 0)
    {
      ret |= PB_WITH_EXTREMUM;
      xm = 0;
    }
  else
    xm = (-b - sqrt(b*b - 3*a*c))/(3*a);
  *Max_pos = (float)(xm + delta); 
  *Max_val = (float)((a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d);
  if (Max_deriv)    *Max_deriv = (float)((6 * a * xm) + (2 * b));
  return ret;
}

int project_on_gaussian(double **zd,    // input data set
			int verbose, // issue explicit error message
			float error_a, // the error proportional to the signal
			float error_b, // the error offset
			double x0,     // center position 
			double y0,     // should be close to 0 < 2
			double sigma,  // size of the gaussian
			double *a,   // the best a value
			double *b,   // the best b value
			double *E)   // the chi^2
{
  int i, j;
  int size_x = 9, off_x = 4, size_y = 9, off_y = 4;
  double  f, fx, er, la, lb, gau[9][9], r2, tmpx, tmpy, lE, sigma2;
  double sy = 0, sy2 = 0, sfx = 0, sfx2 = 0, sfxy = 0, sig_2, ny = 0;

  sigma2 = sigma*sigma;
  for (i = 0; i < size_y; i++)
    {
      tmpy = -y0 + i - off_y;
      tmpy *= tmpy;
      for (j = 0; j < size_x; j++)
	{
	  tmpx = -x0 + j - off_x;
	  tmpx *= tmpx;
	  r2 = tmpx + tmpy;
	  r2 /= (2*sigma2);
	  gau[i][j] = exp(-r2);
	  er = error_b + error_a * zd[i][j];
	  if (er > 0)
	    {
	      sig_2 = (double)1/(er*er);
	      sy += zd[i][j]*sig_2;
	      sy2 += zd[i][j]*zd[i][j]*sig_2;
	      sfx += gau[i][j]*sig_2;
	      sfx2 += gau[i][j]*gau[i][j]*sig_2;
	      sfxy += zd[i][j]*gau[i][j]*sig_2;
	      ny += sig_2;
	    }
	  else if (verbose) win_printf("at point %d %d error negative %g",i,j,er);
	}
    }
  lb = (sfx2 * ny) - sfx * sfx;
  if (lb == 0) 
    {
      if (verbose) win_printf_OK("\\Delta  = 0 \n can't fit that!");
      return 3;
    }
  la = sfxy * ny - sfx * sy;
  la /= lb;
  lb = (sfx2 * sy - sfx * sfxy)/lb;
  for (i = 0, lE = 0; i < size_y; i++)
    {
      for (j = 0; j < size_x; j++)
	{
	  fx = la*gau[i][j] + lb;
	  f = zd[i][j] - fx;
	  er = error_b + error_a * zd[i][j];
	  if (er > 0)
	    {
	      lE += (f*f)/(er*er);
	      j++;
	    }
	}
    }
  if (a) *a = la;
  if (b) *b = lb;
  if (E) *E = lE;
  return 0;
}


int	find_best_a_an_b_and_sigma_of_gaussian_fit_on_9x9(double **zd,    // input data set
							  int verbose, // issue explicit error message
							  float error_a, // the error proportional to the signal
							  float error_b, // the error offset
							  double x0,     // center position 
							  double y0,     // should be close to 0 < 2
							  double *sigma,  // an array of imposed size of the gaussian
							  int n_sig, // the number of sigma
							  double *a,   // the best a value
							  double *b,   // the best b value
							  double *sig,
							  double *E)   // the chi^2
{	
  int i, i1, k;
  double  *la, *lb, *lE, lEmax;
  int  imax, ret;
  d_s *ds;
  float Max_pos, Max_val, tmpx, p;  

  if (zd == NULL || n_sig < 1) 
    {
      if (verbose) win_printf("No valid data \n"); 
      return 1;
    }
  lE = (double*)calloc(n_sig,sizeof(double));
  la = (double*)calloc(n_sig,sizeof(double));
  lb = (double*)calloc(n_sig,sizeof(double));
  ds = build_data_set(n_sig,n_sig);
  if (lE == NULL || lb == NULL || la == NULL || ds == NULL)
    {
      if (verbose) win_printf("nomore memory\n"); 
      return 1;
    }
  for (k = 0, lEmax = 0; k < n_sig; k++)
    {
      ds->xd[k] = (float)sigma[k];
      project_on_gaussian(zd, verbose, error_a, error_b, x0, y0, sigma[k], la+k, lb+k, lE+k);
      ds->yd[k] = -(float)lE[k];
      if (k == 0 || lE[k] < lEmax)
	{
	  lEmax = lE[k];
	  imax = k;
	}
    }

  ret = find_max_around(ds->yd, ds->nx, imax, &Max_pos, &Max_val, NULL);
  if (ret == 0)
    {
      i = (int)Max_pos;
      i1 = i + 1;
      p = (float)i1 - Max_pos;
      i = (i < 0) ? 0 : i;
      i = (i < ds->nx) ? i : ds->nx-1;
      i1 = (i1 < 0) ? 0 : i1;
      i1 = (i1 < ds->nx) ? i1 : ds->nx-1;
      tmpx = p * ds->xd[i] + (1-p) * ds->xd[i1];
      project_on_gaussian(zd, verbose, error_a, error_b, x0, y0, tmpx, a, b, E);
      if (sig) *sig = tmpx;
    }
  else
    {
      if (a) *a = la[imax];
      if (b) *b = lb[imax];
      if (E) *E = lEmax;
      if (sig) *sig = sigma[imax];
    }
  //find_local_max(double *x, double *Max_pos, double *Max_val, double *Max_deriv)
  free(lE);
  free(la);
  free(lb);
  free_data_set(ds);
  return 0;
}




int	find_best_a_an_b_of_gaussian_fit_on_9x9(double **zd,    // input data set
						int verbose, // issue explicit error message
						int error_type, // the type of error in data
						double x0,     // center position 
						double y0,     // should be close to 0 < 2
						double sigma,  // the imposed size of the gaussian
						double *a,   // the best a value
						double *b,   // the best b value
						double *E,   // the chi^2
						int add_im)  // add images to check
{	
  int i, j;
  double  f, fx, er, la, lb, gau[9][9], r2, tmpx, tmpy;
  double sy = 0, sy2 = 0, sfx = 0, sfx2 = 0, sfxy = 0, sig_2, ny = 0;
  O_i *ois, *oig, *oi;
  imreg *imr;
  int size_x = 9, off_x = 4, size_y = 9, off_y = 4;
    

  if (zd == NULL) 
    {
      if (verbose) win_printf("No valid data \n"); 
      return 1;
    }
  sigma *= 2*sigma;
  for (i = 0; i < size_y; i++)
    {
      tmpy = -y0 + i - off_y;
      tmpy *= tmpy;
      for (j = 0; j < size_x; j++)
	{
	  tmpx = -x0 + j - off_x;
	  tmpx *= tmpx;
	  r2 = tmpx + tmpy;
	  r2 /= sigma;
	  gau[i][j] = exp(-r2);
	  if (error_type == 0) er = gau[i][j];
	  else er = 1;
	  if (er > 0)
	    {
	      sig_2 = (double)1/(er*er);
	      sy += zd[i][j]*sig_2;
	      sy2 += zd[i][j]*zd[i][j]*sig_2;
	      sfx += gau[i][j]*sig_2;
	      sfx2 += gau[i][j]*gau[i][j]*sig_2;
	      sfxy += zd[i][j]*gau[i][j]*sig_2;
	      ny += sig_2;
	    }

	}
    }
  lb = (sfx2 * ny) - sfx * sfx;
  if (lb == 0) 
    {
      if (verbose) win_printf_OK("\\Delta  = 0 \n can't fit that!");
      return 3;
    }
  la = sfxy * ny - sfx * sy;
  la /= lb;
  lb = (sfx2 * sy - sfx * sfxy)/lb;
  if (a) *a = la;
  if (b) *b = lb;

  if (add_im)
    {
      if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) == 2)
	{
	  ois = create_and_attach_oi_to_imr(imr, size_x, size_y, IS_FLOAT_IMAGE);
	  oig = create_and_attach_oi_to_imr(imr, size_x, size_y, IS_FLOAT_IMAGE);
	  if (ois != NULL && oig != NULL)
	    {
	      for (i = 0; i < size_y; i++)
		{
		  for (j = 0; j < size_x; j++)
		    {
		      oig->im.pixel[i].fl[j] = (float)(la*gau[i][j] + lb);
		      ois->im.pixel[i].fl[j] = (float)(zd[i][j]);
		    }
		}
	    }    
	  inherit_from_im_to_im(ois,oi);
	  uns_oi_2_oi(ois,oi);
	  set_im_title(ois, "Signal x %g y %g",x0,y0);
	  find_zmin_zmax(ois);
	  inherit_from_im_to_im(oig,oi);
	  uns_oi_2_oi(oig,oi);
	  set_im_title(oig, "Gaussian x %g y %g",x0,y0);
	  find_zmin_zmax(oig);
	}		
    }
  for (i = 0; i < size_y; i++)
    {
      for (j = 0; j < size_x; j++)
	{
	  fx = la*gau[i][j] + lb;
	  f = zd[i][j] - fx;
	  if (error_type == 0) er = gau[i][j];
	  else er = 1;
	  if (er > 0)
	    {
	      *E += (f*f)/(er*er);
	      j++;
	    }
	}
    }
  return 0;
}


int detect_maxima_from_avg_profiles(double xp[5], double yp[5], double threshold_z, double threshold_dz, double max[6])
{
  double Maxx_pos, Maxx_val, Maxx_deriv, Maxy_pos, Maxy_val, Maxy_deriv;
  int ret;
  
  if ((yp[2] > yp[1]) && (yp[2] >= yp[3]) && (xp[2] > xp[1]) 
      && (xp[2] >= xp[3]) && (yp[2] > threshold_z) && (xp[2] > threshold_z))
    { 
      ret = find_local_max(yp, &Maxy_pos, &Maxy_val, &Maxy_deriv);
      if (ret == 0)
	{
	  ret = find_local_max(xp, &Maxx_pos, &Maxx_val, &Maxx_deriv);
	  if ((ret == 0) && (Maxx_val > threshold_z) && (Maxy_val > threshold_z) 
	      && (fabs(Maxx_deriv) > threshold_dz) && (fabs(Maxy_deriv) > threshold_dz))	
	    {
	      max[0] = Maxx_pos;
	      max[1] = Maxy_pos;
	      max[2] = Maxx_val;
	      max[3] = Maxy_val;
	      max[4] = Maxx_deriv;
	      max[5] = Maxy_deriv;
	      return 1;
	    }
	}
    }
  return 0;
}




/*

  ROI = NULL -> complete image
  x0 = ROI[0],   x1 = ROI[1],   
  y0 = ROI[2],   y1 = ROI[3],   

 */


im_ext_ar *find_maxima_positions(O_i *oi, im_ext_ar *iea, double threshold_z,  double threshold_dz, int max_number, int *ROI, int multi, double sigma, float err_a, float err_b)
{
  int i, j, k;
  int  nmax, nys, nye, nxs, nxe, keep_mode = 0;
  static int np;
  double  **zdp = NULL, *zdpl[9], zdpl1[81], max[6], xp[5], yp[5], amp, background, E, /*x0, y0,*/ sigmaf = 0;// zd[9][9],
  double sig[15] = {0.7,0.75,0.8,0.85,0.9,0.95,1.0,1.05,1.1,1.15,1.2,1.25,1.3,1.35,1.4};

  (void)sigma;
  for (zdp = zdpl, i = 0; i < 9; i++)
    zdpl[i] = zdpl1 + 9*i;
  // zd[i] = calloc(9, sizeof(double));
  
  if (oi == NULL)	{xvin_error(Wrong_Argument); return NULL;}
  nmax = (max_number > 0) ? max_number : 16;
  if (iea == NULL) 
    {
      iea = (im_ext_ar *)calloc(1,sizeof(im_ext_ar));
      if (iea == NULL)  return NULL;
      if (alloc_im_ext_array(iea, nmax) < 0) return NULL;
    }
  if (iea == NULL)  return NULL;

  //mode = oi->im.mode;
  nxs = (ROI) ? ROI[0] : 0;
  nys = (ROI) ? ROI[2] : 0;
  nxe = (ROI) ? ROI[1] : oi->im.nx;
  nye = (ROI) ? ROI[3] : oi->im.ny;
  nxs = (nxs > 3) ? nxs : 4;
  nxe = (nxe < oi->im.nx-4) ? nxe : oi->im.nx-5;
  nys = (nys > 3) ? nys : 4;
  nye = (nye < oi->im.ny-4) ? nye : oi->im.ny-5;

  if ((oi->im.data_type == IS_RGBA16_PICTURE) || (oi->im.data_type == IS_RGB16_PICTURE)
      || (oi->im.data_type == IS_RGBA_PICTURE) || (oi->im.data_type == IS_RGB_PICTURE))
    {
      keep_mode = oi->im.mode;
      oi->im.mode = G_LEVEL;
    }
  

  for (i = nys ; i< nye ; i++)
    {
      for (j = nxs ; j< nxe ; j++)
	{
	  extract_x_y_avg_profiles_in_oi(oi, j-2, i-2, xp, 5, 3, yp, 5, 3);
	  if (detect_maxima_from_avg_profiles(xp, yp, 2*threshold_z, 2*threshold_dz, max) == 1) 
	    {  // multiplication by 2 of thresholds compensates ~integration on 3 value arround a max
	      extract_2D_double_array_in_oi(oi, j-4, i-4, zdp, 9, 9);
	      //find_best_a_an_b_of_gaussian_fit_on_9x9(zdp, 1, 1, max[0], max[1], sigma, &amp, &background, &E, 0);
	      find_best_a_an_b_and_sigma_of_gaussian_fit_on_9x9(zdp,1,err_a,err_b,max[0],max[1],sig,15, &amp, &background, &sigmaf,&E);

	      //win_printf("max found in %d %g %d %g zdp %g, amp %g bkg %g",j,max[1],i,max[0],zdp[4][4],amp,background);
	      k = find_in_im_extrema(iea, j, i, 2,oi->im.c_f);//k = numero du pic dans la zone i, j +-2 et image im_cf
	      if ((amp > threshold_z) && (k == (int)iea->n_ie)/*no previous maximum known*/)//we have a new maximum with a required amplitude
		{
		  k = (iea->n_ie > 0) ? iea->n_ie - 1 : 0;
		  if ((multi == 0) && (np > 0) && (amp > iea->ie[k].zmax)) 
		    {
		      iea->n_ie = (iea->n_ie > 0) ? iea->n_ie - 1 : 0; // we erase the last peak because amplitude is bigger
		      add_im_extremum(iea, max[0] + j, max[1] + i, amp, background, E, sigmaf,oi->im.c_f);
		      np++;
		    }
		  else if (multi || (np == 0))//np==0  au premier passage...le r�le du param�tre multi? multi = 1 au premier appel quand lance le plugin pour tout chercher et 0 ensuite
		    {
		      add_im_extremum(iea, max[0] + j, max[1] + i, amp, background, E, sigmaf,oi->im.c_f);
		      np++;
		    }
		}
	    }
	}
    }
  /* if (multi == 0)  */
  /*   { */
  /*     k = (iea->n_ie > 0) ? iea->n_ie - 1 : 0; */
  /*     extract_2D_double_array_in_oi(oi, iea->ie[k].x0-4, iea->ie[k].y0-4, zdp, 9, 9); */
  /*     x0 = iea->ie[k].xpos - (int)(iea->ie[k].xpos + 0.5); */
  /*     y0 = iea->ie[k].ypos - (int)(iea->ie[k].ypos + 0.5); */
  /*     //find_best_a_an_b_of_gaussian_fit_on_9x9(zdp, 1, 1, x0, y0, sigma, &amp, &background, &E, 0); */
  /*     find_best_a_an_b_and_sigma_of_gaussian_fit_on_9x9(zdp,1,err_a,err_b,x0,y0,sig,15, &amp, &background, &sigmaf,&E); */
  /*     //on trouve le maximum et on n'en fait rien.... */
  //}

  if ((oi->im.data_type == IS_RGBA16_PICTURE) || (oi->im.data_type == IS_RGB16_PICTURE)
      || (oi->im.data_type == IS_RGBA_PICTURE) || (oi->im.data_type == IS_RGB_PICTURE))
    oi->im.mode = keep_mode;

  my_set_window_title("%d pixels in %d maxima",np,iea->n_ie);
  oi->need_to_refresh |= BITMAP_NEED_REFRESH;
  oi->im.use.to = IS_IM_EXREMUM;
  oi->im.use.stuff = (void*)iea;
  oi->oi_mouse_action = oi_mouse_GifAvg_action;
  return iea;
}

int convert_maxima_search_info_in_plot(im_ext_ar *iea, O_i *ois, double thres, double tolerance) 
{
  int i, ip;
  d_s *ds = NULL;
  O_p *op = NULL;

  if (iea == NULL || iea->n_ie < 1) return 1;
  ip = find_op_nb_of_source_specific_ds_in_oi(ois, "multi max found with threshold");
  if (ip < 0 || ip >= ois->n_op)
    {
      op = create_and_attach_op_to_oi(ois, iea->n_ie, iea->n_ie, 0, IM_SAME_Y_AXIS|IM_SAME_X_AXIS);
      if (op == NULL) return D_O_K;
      ds = op->dat[0];
      set_ds_dot_line(ds);
      set_ds_point_symbol(ds, "\\pt4\\oc");
      ds->nx = ds->ny = 0;
    }
  else
    {
      op = ois->o_p[ip];
      ds = op->dat[0];
      ds->nx = ds->ny = 0;
    }
  set_ds_source(ds, "multi max found with threshold %g tolerence %d",thres,tolerance);
  for (i = 0; i < (int)iea->n_ie; i++)
    add_new_point_to_ds(ds, iea->ie[i].xpos, iea->ie[i].ypos);

  ois->need_to_refresh = PLOT_NEED_REFRESH;	
  return 0;
}


/*
 *		
 *
 */
int convert_maxima_search_info_in_GREEN(im_ext_ar *iea, O_i *ois, double thres, double tolerance) 
{
  int i, j, im;
  int  k, jm;
  union pix *pd;



  if (iea == NULL || iea->n_ie < 1) return 1;
  if (ois == NULL || ois->im.pixel == NULL) return 1;
  (void)thres;
  (void)tolerance;
  if (ois->im.data_type != IS_RGB16_PICTURE) return 1;
  pd = ois->im.pixel;	
  for (i = 0; i < ois->im.ny; i++)
    {
      for (j = 0; j < ois->im.nx; j++)	
	pd[i].rgb16[j].r = pd[i].rgb16[j].b = pd[i].rgb16[j].g; 
    }
  for (k = 0; k < (int)iea->n_ie; k++)
    {
      jm = (int)(iea->ie[k].xpos+0.5);       
      im = (int)(iea->ie[k].ypos+0.5);       
      for (i = -1; i <= 1; i++)
	{
	  if (im + i < 0 || im + i > ois->im.ny) continue; 
	  for (j=-1; j <= 1; j++)	
	    {
	      if (jm + j < 0 || jm + j > ois->im.nx) continue;
	      if (abs(i) == 1 && abs(j) == 1) continue;
	      pd[im+i].rgb16[jm+j].r = pd[im+i].rgb16[jm+j].b = 0;
	    }
	}
    }
  ois->need_to_refresh |= ALL_NEED_REFRESH;
  return 0;
}

int rank_maxima_search_info_in_plot(void)
{
  imreg *imr = NULL;
  O_i *oi = NULL;
  im_ext_ar *iea;
  int i, ip;
  d_s *ds = NULL;
  O_p *op = NULL;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;

  if (imr->one_i == NULL)        return D_O_K;
  oi = imr->one_i;
  iea = (oi->im.use.to != IS_IM_EXREMUM || oi->im.use.stuff == NULL) ? NULL : (im_ext_ar *)oi->im.use.stuff;

  if(updating_menu_state != 0)
    {
      if (iea == NULL) active_menu->flags |=  D_DISABLED;
      else   active_menu->flags &= ~D_DISABLED;
      return D_O_K;		
    }

  if (iea == NULL || iea->n_ie < 1) return D_O_K;
  ip = find_op_nb_of_source_specific_ds_in_oi(oi, "histo multi max found with threshold");
  if (ip < 0 || ip >= oi->n_op)
    {
      op = create_and_attach_op_to_oi(oi, iea->n_ie, iea->n_ie, 0, 0);
      if (op == NULL) return D_O_K;
      ds = op->dat[0];
      set_ds_dot_line(ds);
      set_ds_point_symbol(ds, "\\pt4\\oc");
      ds->nx = ds->ny = 0;
    }
  else
    {
      op = oi->o_p[ip];
      ds = op->dat[0];
      ds->nx = ds->ny = 0;
    }
  set_ds_source(ds, "histo multi max found with threshold %g tolerence %d",thres_s,tolerance_s);
  for (i = 0; i < (int)iea->n_ie; i++)
    add_new_point_to_ds(ds, iea->ie[i].zmax, i); 
  sort_ds_along_x(ds);
  for (i = 0; i < (int)iea->n_ie && i < ds->nx; i++)
    {
      ds->yd[i] = ds->xd[i];
      ds->xd[i] = i;
    }
  oi->need_to_refresh |= PLOT_NEED_REFRESH;	
  return 0;
}




int dump_peaks_to_csv(void)
{
  int i;
  imreg *imr = NULL;
  O_i *oi = NULL;
  FILE *fp = NULL;
  char cfilename[1024];
  im_ext_ar *iea;
  int k;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;

  if (imr->one_i == NULL)        return D_O_K;
  oi = imr->one_i;
  iea = (oi->im.use.to != IS_IM_EXREMUM || oi->im.use.stuff == NULL) ? NULL : (im_ext_ar *)oi->im.use.stuff;

  if(updating_menu_state != 0)
    {
      if (iea == NULL) active_menu->flags |=  D_DISABLED;
      else   active_menu->flags &= ~D_DISABLED;
      return D_O_K;		
    }

  if (iea == NULL || iea->n_ie < 1) return D_O_K;
  append_filename(cfilename, oi->dir, oi->filename, 1024);
  replace_extension(cfilename, cfilename, "csv", sizeof(cfilename));
  if (do_select_file(cfilename, sizeof(cfilename), "CSV-FILE", "*.csv", "File to save peaks", 0))
    return win_printf_OK("Cannot select input file");
  fp = fopen (cfilename,"w");
  if (fp == NULL)  
    {
      win_printf("Cannot open file:\n%s",backslash_to_slash(cfilename));
      return -1;
    }
  fprintf(fp,"Peak_Nb; ");
  fprintf(fp,"Amplitude; ");
  fprintf(fp,"Background; ");
  fprintf(fp,"\\chi^2; ");
  fprintf(fp,"\\sigma; ");
  fprintf(fp,"X; ");
  fprintf(fp,"Y;");
  fprintf(fp,"Image number\n");
  if (iea != NULL)
    {
      for (i = 0; i < (int)iea->n_ie; i++)
	{
	  //	  win_printf(" i = %d \n inb %d",i,iea->ie[i].inb);
	  fprintf(fp,"%d; ",i);
	  fprintf(fp,"%g; ",iea->ie[i].zmax);
	  fprintf(fp,"%g; ",iea->ie[i].dnb);
	  fprintf(fp,"%g; ",iea->ie[i].chi2);
	  fprintf(fp,"%g; ",iea->ie[i].sigma);
	  fprintf(fp,"%g; ",iea->ie[i].xpos);
	  fprintf(fp,"%g;",iea->ie[i].ypos);
	  fprintf(fp,"%d\n",iea->ie[i].inb);
	}
    }
  
  fclose(fp);
  
  return 0;
}



int oi_mouse_GifAvg_action(struct one_image *oi, int x0, int y0, int mode, DIALOG *d)
{
  int x, y, ip, i, ROI[4], n0, keep_mode = 0;
  static int old_x = 0, old_y = 0, remove = 0;
  float px1, py1;
  static double reduce = 0.5, sig[15] = {0.7,0.75,0.8,0.85,0.9,0.95,1.0,1.05,1.1,1.15,1.2,1.25,1.3,1.35,1.4};
  static float err_a = 0.15, err_b = 200;
  static int w = 8;
  imreg *imr;
  im_ext_ar *iea = NULL;
  char question[1024];
  double  **zdp = NULL, *zdpl[9], zdpl1[81], amp = 0, background = 0, E = 0, sigma = 0;
  static int fit_meth=0;
  gaussian_fit_params gfp;
  
  for (zdp = zdpl, i = 0; i < 9; i++)
    zdpl[i] = zdpl1 + 9*i;

  (void)x0;
  (void)y0;
  (void)mode;
  
  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  
  if (imr->one_i == NULL)        return win_printf_OK("Could not find image data");
  oi = imr->one_i;
  if (oi->im.use.to != IS_IM_EXREMUM || oi->im.use.stuff == NULL) 
    return win_printf_OK("Could not find extremum info");
  iea = (im_ext_ar *)oi->im.use.stuff;
  
  x = mouse_x; y = mouse_y;
  px1 = x_imr_2_imdata(imr, mouse_x);    
  py1 = y_imr_2_imdata(imr, mouse_y);    
  
  if(x != old_x || x != old_y)
    {
      ip = find_in_im_extremum(iea, px1, py1, 2);//gives the number of the peak on pixel px1, py1+-2 pixels return iea->n_ie if no maximum found
      if (ip < (int)iea->n_ie)
	{
	  snprintf(question,sizeof(question),"Image %d\n Extremum found at:\nx = %g y = %g\n"
		   "Amplitude %g Background %g \n\\chi^2 = %g \\sigma = %g\n"
		   "To do nothing click here->%%R\n"
		   "To remove extremum->%%r\n"
		   "to fit sigma in Gaussian->%%r \n"
		   "For this last case give the error expression\n"
		   "Noise offset %%8f noise factor %%8f\n"
		   "Fitting method (Levenberg Marquart %%R Chi square minimization 0) %%r",
		   oi->im.c_f,iea->ie[ip].xpos, iea->ie[ip].ypos, iea->ie[ip].zmax, iea->ie[ip].dnb, iea->ie[ip].chi2, iea->ie[ip].sigma);
	  i = win_scanf(question,&remove,&err_b, &err_a,&fit_meth);
	  if (i == CANCEL) return D_O_K;
	  if (remove == 1)
	    {
	      for (i = ip; i < (int)(iea->n_ie - 1); i++)
		iea->ie[i] = iea->ie[i+1]; 
	      iea->n_ie = (iea->n_ie > 1) ? iea->n_ie - 1 : 1;//removes the peak

	      if (oi->im.data_type == IS_RGB16_PICTURE) 
		convert_maxima_search_info_in_GREEN(iea, oi, thres_s, tolerance_s);
	      else 
		{
		  convert_maxima_search_info_in_plot(iea, oi, thres_s, tolerance_s);
		  oi->need_to_refresh |= PLOT_NEED_REFRESH;	  
		}
	    }
	  else if (remove == 2)
	    {
	      if ((oi->im.data_type == IS_RGBA16_PICTURE) || (oi->im.data_type == IS_RGB16_PICTURE)
		  || (oi->im.data_type == IS_RGBA_PICTURE) || (oi->im.data_type == IS_RGB_PICTURE))
		{
		  keep_mode = oi->im.mode;
		  oi->im.mode = G_LEVEL;
		}
	      extract_2D_double_array_in_oi(oi, iea->ie[ip].x0-4, iea->ie[ip].y0-4, zdp, 9, 9);
	      //win_printf("center %g pos %g %g ",zdp[4][4],iea->ie[ip].xpos - (int)(iea->ie[ip].xpos+0.5), 
	      //	 iea->ie[ip].ypos - (int)(iea->ie[ip].ypos + 0.5));
	      if (fit_meth == 1) find_best_a_an_b_and_sigma_of_gaussian_fit_on_9x9(zdp,1,err_a,err_b,iea->ie[ip].xpos - (int)(iea->ie[ip].xpos+0.5), 
					      iea->ie[ip].ypos - (int)(iea->ie[ip].ypos + 0.5),sig,15, &amp, &background, &sigma,&E);
	      else 
		{
		  
		  gfp.a = (double)/* 1000;// */iea->ie[ip].zmax;
  		  gfp.x_c = (double)4.5;//(iea->ie[ip].xpos - (int)(iea->ie[ip].xpos+0.5));
		  gfp.y_c = (double) 4.5;//(iea->ie[ip].ypos - (int)(iea->ie[ip].ypos + 0.5));
  		  gfp.sigma_x=(double)/* 2;// */iea->ie[ip].sigma;
		  gfp.sigma_y=(double)/* 2;// */iea->ie[ip].sigma;
		  gfp.b = (double)/* 3000;// */iea->ie[ip].dnb;
		  
		  find_best_fit_param_on_9x9_LM(zdp,1,err_a,err_b,&gfp,sig,15);
		  
		  /*iea->ie[ip].zmax*/amp = gfp.a;
  		  ///*iea->ie[ip].xpos*/xpos = gfp.x_c; //(iea->ie[ip].xpos - (int)(iea->ie[ip].xpos+0.5));
		  //*iea->ie[ip].ypos*/ypos = gfp.y_c ; /*(iea->ie[ip].ypos - (int)(iea->ie[ip].ypos + 0.5))*/
  		  /*iea->ie[ip].sigma = gfp.sigma_x;*/
		  /*iea->ie[ip].sigma*/sigma = gfp.sigma_y;
		  /*iea->ie[ip].dnb*/background = gfp.b ;
		  /*iea->ie[ip].chi2*/E = gfp.chi2;


		
		}
	      if ((oi->im.data_type == IS_RGBA16_PICTURE) || (oi->im.data_type == IS_RGB16_PICTURE)
		  || (oi->im.data_type == IS_RGBA_PICTURE) || (oi->im.data_type == IS_RGB_PICTURE))
		  oi->im.mode = keep_mode;

	      win_printf("Initial Extremum found at:\nx = %g y = %g\n"
			 "Amplitude %g Background %g \n\\chi^2 %g sigma %g\n"
			 "New fit sigma = %g amp %g Background %g\n"
			 "\\chi^2 = %g",iea->ie[ip].xpos, iea->ie[ip].ypos, 
			 iea->ie[ip].zmax, iea->ie[ip].dnb, iea->ie[ip].chi2, iea->ie[ip].sigma, sigma,amp,background,E);

	      // we should ask if we want to replace the old by the new fitting parameters!
	    }
	}
      else
	{
	  ROI[0] = (int)(0.5+px1) - w;	  ROI[1] = (int)(0.5+px1) + w;
	  ROI[2] = (int)(0.5+py1) - w;	  ROI[3] = (int)(0.5+py1) + w;
	  n0 = iea->n_ie;//on compte le nombre de pics avant la recherche de maxima
	  iea = find_maxima_positions(oi, iea, thres_s*reduce, tolerance_s*reduce, 256, ROI, 0, sigma_s, err_a_s, err_b_s);
	  if ((int)(iea->n_ie) > n0)
	    {
	      i = win_printf("New peak found at x = %g y = %g\n amp %g background %g\n\\chi^2 = %g \\sigma  = %g\n"
			     "Press OK to keep CANCEL to refuse this maximum\n"
			     ,iea->ie[n0].xpos,iea->ie[n0].ypos,iea->ie[n0].zmax,iea->ie[n0].dnb, iea->ie[n0].chi2, iea->ie[n0].sigma);
	      if (i == CANCEL) iea->n_ie = n0;
	      else 
		{
		  if (oi->im.data_type == IS_RGB16_PICTURE) 
		    convert_maxima_search_info_in_GREEN(iea, oi, thres_s, tolerance_s);
		  else 
		    {
		      convert_maxima_search_info_in_plot(iea, oi, thres_s, tolerance_s);
		      oi->need_to_refresh |= PLOT_NEED_REFRESH;	  
		    }
		}
	    }
	  else
	    {
	      i = win_scanf("No peak found! you can reduce the theshold\n"
			    "Reduction factor %10lf\n"
			    "Extend the search arround the mouse position\n"
			    "New extend - and + arround position %8d\n",&reduce,&w);
	      if (i == CANCEL) return D_O_K;
	    }
	}
    }
  old_x = x;
  old_y = y;
  oi->need_to_refresh |= PLOT_NEED_REFRESH;	
  return (refresh_image(imr, UNCHANGED));
}


int refresh_RGB_expand_im(imreg *imr, O_i *oi)
{
  int ni1, ni2, expa;
  float iR1, iG1, iB1, iR2, iG2, iB2, sigma;
  O_i *oid = NULL;

  if (oi == NULL) return 1;
  if (oi->im.user_nipar < 8 || oi->im.user_nfpar < 8)   return 1;
  ni1 = oi->im.user_ispare[0][0];
  expa = oi->im.user_ispare[0][1];
  ni2 = oi->im.user_ispare[0][2];
  if (ni1 == ni2 || ni1 < 0 || ni1 >= imr->n_oi || ni2 < 0 || ni2 >= imr->n_oi) return 1;
  iR1 = oi->im.user_fspare[0][0];
  iG1 = oi->im.user_fspare[0][1];
  iB1 = oi->im.user_fspare[0][2];
  iR2 = oi->im.user_fspare[0][3];
  iG2 = oi->im.user_fspare[0][4];
  iB2 = oi->im.user_fspare[0][5];
  sigma = oi->im.user_fspare[0][6];
  oid = convert_float_image_to_expanded_RGB_char(oi, imr->o_i[ni1], expa, iR1, iG1, iB1);
  if (oid == NULL)	return D_O_K;
  oid = convert_float_image_extremum_to_expanded_RGB_char(oid, imr->o_i[ni2], expa, sigma, iR2, iG2, iB2);
  if (oid == NULL)	return D_O_K;
  find_zmin_zmax(oid);
  oid->need_to_refresh |= ALL_NEED_REFRESH;		
  return 0;
}



int oi_mouse_RGB_GifAvg_action(struct one_image *oi, int x0, int y0, int mode, DIALOG *d)
{
  int x, y, ip, i, remove = 0;
  static int old_x = 0, old_y = 0;
  float px1, py1;
  imreg *imr;
  im_ext_ar *iea = NULL;
  char question[1024];
  int  ni1, ni2, expa; // 
  O_i  *oifm = NULL;; // *oib = NULL
  
  (void)x0;
  (void)y0;
  (void)mode;
  
  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  
  if (oi == NULL) return 1;
  if (oi->im.user_nipar < 8 || oi->im.user_nfpar < 8)   return 1;
  ni1 = oi->im.user_ispare[0][0];
  expa = oi->im.user_ispare[0][1];
  ni2 = oi->im.user_ispare[0][2];
  if (ni1 == ni2 || ni1 < 0 || ni1 >= imr->n_oi || ni2 < 0 || ni2 >= imr->n_oi) return 1;
  //sigma = oi->im.user_fspare[0][6];
  //oib = imr->o_i[ni1];  // bacteria image
  oifm = imr->o_i[ni2]; // Max fluo image


  if (oifm->im.use.to != IS_IM_EXREMUM || oifm->im.use.stuff == NULL) 
    return win_printf_OK("Could not find extremum info");
  iea = (im_ext_ar *)oifm->im.use.stuff;
  
  x = mouse_x; y = mouse_y;
  px1 = x_imr_2_imdata(imr, mouse_x);    
  py1 = y_imr_2_imdata(imr, mouse_y);    
  
  if(x != old_x || x != old_y)
    {
      ip = find_in_im_extremum(iea, (int)(0.5 + px1/expa), (int)(0.5 + py1/expa), 2);
      if (ip < (int)iea->n_ie)
	{
	  snprintf(question,sizeof(question),"Extremum found at:\nx = %g y = %g\n"
		   "Amplitude %g Background %g\n\\chi^2 = %g \\sigma  = %g\nTo remove extremum click here %%b\n",
		   iea->ie[ip].xpos, iea->ie[ip].ypos, iea->ie[ip].zmax, iea->ie[ip].dnb, iea->ie[ip].chi2, iea->ie[ip].sigma);
	  i = win_scanf(question,&remove);
	  if (i == CANCEL) return D_O_K;
	  if (remove)
	    {
	      for (i = ip; i < (int)(iea->n_ie - 1); i++)
		iea->ie[i] = iea->ie[i+1]; 
	      iea->n_ie = (iea->n_ie > 1) ? iea->n_ie - 1 : 1;
	      if (oifm->im.data_type == IS_RGB16_PICTURE) 
		convert_maxima_search_info_in_GREEN(iea, oifm, thres_s, tolerance_s);
	      else 
		{
		  convert_maxima_search_info_in_plot(iea, oifm, thres_s, tolerance_s);
		  oifm->need_to_refresh |= PLOT_NEED_REFRESH;	  
		}
	    }
	  refresh_RGB_expand_im(imr, oi);
	}
    }
  old_x = x;
  old_y = y;
  oi->need_to_refresh |= PLOT_NEED_REFRESH;	
  return (refresh_image(imr, UNCHANGED));
}







//int oi_idle_GifAvg_action(struct one_image *oi, DIALOG *d){}        

int  do_find_fluo_maxima(void)
{
  O_i   *ois;
  imreg *imr;
  static int i, nmax = 256, with_amp = 1;//, dumpcsv = 1;
  im_ext_ar *iea = NULL;
  static double thres = 2000,  tolerance = 1000, sigma = 0.9;
  static float err_a = 0.15, err_b = 200;
	
  if(updating_menu_state != 0)	return D_O_K;		

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  iea = (ois->im.use.to != IS_IM_EXREMUM || ois->im.use.stuff == NULL) ? NULL : (im_ext_ar *)ois->im.use.stuff;

  i = win_scanf("Find bright spots in image\n"
		"Sigma of gaussian %12lf\n"
		"Specify the threshold in Z %12lf\n"
		"Specify the threshold in dZ %12lf\n"
		"Define the Signal error as er(Y) = a*y +b\n"
		"Enter a %8f and b %8f\n"
		"Maximum number of spots %8d \n"
		"generate amp ds %b\n"
		,&sigma,&thres, &tolerance,&err_a,&err_b,&nmax,&with_amp);
  if (i == CANCEL) return D_O_K;

  iea = find_maxima_positions(ois, iea/*NULL*/, thres, tolerance, nmax, NULL, 1,sigma, err_a, err_b);
  thres_s = thres;
  tolerance_s = tolerance;
  sigma_s = sigma;
  err_a_s = err_a;
  err_b_s = err_b;

  if (ois->im.data_type == IS_RGB16_PICTURE) 
    convert_maxima_search_info_in_GREEN(iea, ois, thres, tolerance);
  else 
    {
      convert_maxima_search_info_in_plot(iea, ois, thres, tolerance);
      ois->need_to_refresh = PLOT_NEED_REFRESH;	  
    }
  return (refresh_image(imr, UNCHANGED));
}



/*
 *		dest = ois1 - ois2
 *
 */
O_i 	*convert_float_image_to_RGB_char(O_i *dest, O_i *ois1, float inR, float inG, float inB)
{
  int i, j, im;
  int  onx, ony, itmp, nf, nfd;
  int	dest_type = 0;
  float tmp;
  double val;
  union pix *pd;

  if (ois1 == NULL || ois1->im.pixel == NULL)
      return (O_i *) win_printf_ptr("No image found");

  ony = ois1->im.ny;	onx = ois1->im.nx;	
  nf = ois1->im.n_f;
  if (nf < 1) nf = 1;

  if (dest != NULL)
    {
      if (dest->im.data_type != IS_RGB_PICTURE) 			
	return (O_i *) win_printf_ptr ("The destination image is not of unsigned char type\n"
			"Cannot perform conversion!");
      if (ony != dest->im.ny || onx != dest->im.nx)
	return (O_i *) win_printf_ptr ("The destination size differs from the source one \n"
			"Cannot perform conversion!");
      nfd = dest->im.n_f;
      if (nfd < 1) nfd = 1;
      if (nf != nfd)	return (O_i *) win_printf_ptr ("The destination nb of frame differs from the source one \n"
					"Cannot perform conversion!");
      
    }
  dest_type = IS_RGB_PICTURE;

  if (nf == 1)    dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
  else            dest = (dest != NULL) ? dest : create_one_movie(onx, ony, dest_type, nf);
  if (dest == NULL)    return (O_i *) win_printf_ptr("Can't create dest image");
  dest->im.mode = TRUE_RGB;


  for (im = 0; im < nf; im++)
    {
    switch_frame(ois1,im);
    switch_frame(dest,im);
    pd = dest->im.pixel;	
    for (i=0; i< ony; i++)
      {
	for (j=0; j<onx; j++)	
	  {
	    get_raw_pixel_value(ois1, j, i, &val);
	    tmp = val - ois1->z_min;
	    tmp /= ois1->z_max - ois1->z_min;
	    tmp *= 256;
	    itmp = (int)(inR*tmp+0.5);
	    itmp = (itmp < 0) ? 0 : itmp;
	    itmp = (itmp > 255) ? 255 : itmp;
	    pd[i].rgb[j].r += (unsigned char)itmp;
	    itmp = (int)(inG*tmp+0.5);
	    itmp = (itmp < 0) ? 0 : itmp;
	    itmp = (itmp > 255) ? 255 : itmp;
	    pd[i].rgb[j].g += (unsigned char)itmp;
	    itmp = (int)(inB*tmp+0.5);
	    itmp = (itmp < 0) ? 0 : itmp;
	    itmp = (itmp > 255) ? 255 : itmp;
	    pd[i].rgb[j].b += (unsigned char)itmp;
	  }
      }
    }
  inherit_from_im_to_im(dest,ois1);	
  uns_oi_2_oi(dest,ois1);
  dest->im.win_flag = ois1->im.win_flag;
  dest->need_to_refresh = ALL_NEED_REFRESH;		
  return dest;
}



int  do_convert_float_image_to_RGB_char(void)
{
  O_i   *oid, *ois;
  imreg *imr;
  static int i, ni1 = 0, ni2 = 1; 
  static float iR1 =0.5, iG1 = 0.5, iB1 = 0.5, iR2 = 0, iG2 = 0.5, iB2 = 0;
	
  if(updating_menu_state != 0)	return D_O_K;		

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  
  i = win_scanf("Combine two images in an RGB\n"
		"Specify the first image nb %8d scaled by %5f R %5f G %5f B\n"
		"Specify the second image nb %8d scaled by %5f R %5f G %5f B\n"
		,&ni1,&iR1,&iG1,&iB1,&ni2,&iR2,&iG2,&iB2);
  if (i == CANCEL) return D_O_K;

  oid = convert_float_image_to_RGB_char(NULL, imr->o_i[ni1], iR1, iG1, iB1);
  if (oid == NULL)	return D_O_K;
  oid = convert_float_image_to_RGB_char(oid, imr->o_i[ni2], iR2, iG2, iB2);
  if (oid == NULL)	return D_O_K;
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  oid->need_to_refresh = ALL_NEED_REFRESH;		
  return (refresh_image(imr, imr->n_oi - 1));
}


O_i *substract_float_image_extremum_to_RGB16_image(O_i *dest, O_i *ois)
{
  int i, j, im;
  int  onx, ony, nf, nfd, k, jm;
  int	dest_type = 0;
  float  tmpx, tmpy;
  float val, sigma2;
  union pix *pd, *ps;
  im_ext_ar *iea;


  if (ois == NULL || ois->im.pixel == NULL)
    return (O_i *) win_printf_ptr("No image found");

  ony = ois->im.ny;	onx = ois->im.nx;	
  nf = (ois->im.n_f < 1) ? 1 : ois->im.n_f;

  if (dest != NULL)
    {
      if (dest->im.data_type != IS_INT_IMAGE) 			
	  return (O_i *) win_printf_ptr("The destination image is not of RGB16 type\n"
				"Cannot perform conversion!");
      if ((ony != dest->im.ny) || (onx != dest->im.nx))
	return (O_i *) win_printf_ptr("The destination size differs from the source one \n"
			      "Cannot perform conversion!");
      nfd = (dest->im.n_f < 1) ? 1 : dest->im.n_f;
      if (nf > 1 || nfd > 1)	return (O_i *) win_printf_ptr("Cannot treat movies");
      
    }
  dest_type = IS_INT_IMAGE;

  iea = (ois->im.use.to != IS_IM_EXREMUM || ois->im.use.stuff == NULL) ? NULL : (im_ext_ar *)ois->im.use.stuff;
  if (iea == NULL || iea->n_ie < 1) return D_O_K;

  dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
  if (dest == NULL)    return (O_i *) win_printf_ptr("Can't create dest image");

  pd = dest->im.pixel;	
  ps = ois->im.pixel;	
  for (i = 0; i < ony; i++)
    {
      for (j = 0; j < onx; j++)	
	pd[i].in[j] = ps[i].rgb16[j].g; 
    }
  for (k = 0; k < (int)iea->n_ie; k++)
    {
      jm = (int)(iea->ie[k].xpos+0.5);       
      im = (int)(iea->ie[k].ypos+0.5);
      sigma2 = iea->ie[k].sigma * iea->ie[k].sigma * 2;
      for (i = -4; i <= 4; i++)
	{
	  if (im + i < 0 || im + i > dest->im.ny) continue; 
	  for (j=-4; j <= 4; j++)	
	    {
	      if (jm + j < 0 || jm + j > dest->im.nx) continue; 
	      tmpx = iea->ie[k].xpos - j - jm;
	      tmpx *= tmpx;
	      tmpy = iea->ie[k].ypos - i - im;
	      tmpy *= tmpy;
	      val = tmpx + tmpy;
	      val /= sigma2;
	      val = exp(-val);
	      val *= iea->ie[k].zmax;
	      pd[im+i].in[jm+j] = ps[im+i].rgb16[jm+j].g - (short int)(0.5+val); 
	    }
	}
    }
  inherit_from_im_to_im(dest,ois);	
  uns_oi_2_oi(dest,ois);
  dest->im.win_flag = ois->im.win_flag;
  dest->need_to_refresh = ALL_NEED_REFRESH;		
  return dest;
}


O_i 	*substract_float_image_extremum_to_float_image(O_i *dest, O_i *ois)
{
  int i, j, im;
  int  onx, ony, nf, nfd, k, jm;
  int	dest_type = 0;
  float  tmpx, tmpy;
  float val, sigma2;
  union pix *pd, *ps;
  im_ext_ar *iea;


  if (ois == NULL || ois->im.pixel == NULL)
    return (O_i *) win_printf_ptr("No image found");

  ony = ois->im.ny;	onx = ois->im.nx;	
  nf = (ois->im.n_f < 1) ? 1 : ois->im.n_f;

  if (dest != NULL)
    {
      if (dest->im.data_type != IS_FLOAT_IMAGE) 			
	  return (O_i *) win_printf_ptr("The destination image is not of RGB16 type\n"
				"Cannot perform conversion!");
      if ((ony != dest->im.ny) || (onx != dest->im.nx))
	return (O_i *) win_printf_ptr("The destination size differs from the source one \n"
			      "Cannot perform conversion!");
      nfd = (dest->im.n_f < 1) ? 1 : dest->im.n_f;
      if (nf > 1 || nfd > 1)	return (O_i *) win_printf_ptr("Cannot treat movies");
      
    }
  dest_type = IS_FLOAT_IMAGE;

  iea = (ois->im.use.to != IS_IM_EXREMUM || ois->im.use.stuff == NULL) ? NULL : (im_ext_ar *)ois->im.use.stuff;
  if (iea == NULL || iea->n_ie < 1) return D_O_K;

  dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
  if (dest == NULL)    return (O_i *) win_printf_ptr("Can't create dest image");

  pd = dest->im.pixel;	
  ps = ois->im.pixel;	
  for (i = 0; i < ony; i++)
    {
      for (j = 0; j < onx; j++)	
	pd[i].fl[j] = ps[i].fl[j]; 
    }
  for (k = 0; k < (int)iea->n_ie; k++)
    {
      jm = (int)(iea->ie[k].xpos+0.5);       
      im = (int)(iea->ie[k].ypos+0.5);
      sigma2 = iea->ie[k].sigma * iea->ie[k].sigma * 2;
      for (i = -4; i <= 4; i++)
	{
	  if (im + i < 0 || im + i > dest->im.ny) continue; 
	  for (j=-4; j <= 4; j++)	
	    {
	      if (jm + j < 0 || jm + j > dest->im.nx) continue; 
	      tmpx = iea->ie[k].xpos - j - jm;
	      tmpx *= tmpx;
	      tmpy = iea->ie[k].ypos - i - im;
	      tmpy *= tmpy;
	      val = tmpx + tmpy;
	      val /= sigma2;
	      val = exp(-val);
	      val *= iea->ie[k].zmax;
	      pd[im+i].fl[jm+j] = ps[im+i].fl[jm+j] - val; 
	    }
	}
    }
  inherit_from_im_to_im(dest,ois);	
  uns_oi_2_oi(dest,ois);
  dest->im.win_flag = ois->im.win_flag;
  dest->need_to_refresh = ALL_NEED_REFRESH;		
  return dest;
}


O_i 	*substract_float_image_extremum_to_u_int_image(O_i *dest, O_i *ois)
{
  int i, j, im;
  int  onx, ony, nf, nfd, k, jm;
  int	dest_type = 0;
  float  tmpx, tmpy;
  float val, sigma2;
  union pix *pd, *ps;
  im_ext_ar *iea;


  if (ois == NULL || ois->im.pixel == NULL)
    return (O_i *) win_printf_ptr("No image found");

  ony = ois->im.ny;	onx = ois->im.nx;	
  nf = (ois->im.n_f < 1) ? 1 : ois->im.n_f;

  if (dest != NULL)
    {
      if (dest->im.data_type != IS_UINT_IMAGE) 			
	  return (O_i *) win_printf_ptr("The destination image is not of UINT type\n"
				"Cannot perform conversion!");
      if ((ony != dest->im.ny) || (onx != dest->im.nx))
	return (O_i *) win_printf_ptr("The destination size differs from the source one \n"
			      "Cannot perform conversion!");
      nfd = (dest->im.n_f < 1) ? 1 : dest->im.n_f;
      if (nf > 1 || nfd > 1)	return (O_i *) win_printf_ptr("Cannot treat movies");
      
    }
  dest_type = IS_UINT_IMAGE;

  iea = (ois->im.use.to != IS_IM_EXREMUM || ois->im.use.stuff == NULL) ? NULL : (im_ext_ar *)ois->im.use.stuff;
  if (iea == NULL || iea->n_ie < 1) return D_O_K;

  dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
  if (dest == NULL)    return (O_i *) win_printf_ptr("Can't create dest image");

  pd = dest->im.pixel;	
  ps = ois->im.pixel;	
  for (i = 0; i < ony; i++)
    {
      for (j = 0; j < onx; j++)	
	pd[i].ui[j] = ps[i].ui[j]; 
    }
  for (k = 0; k < (int)iea->n_ie; k++)
    {
      jm = (int)(iea->ie[k].xpos+0.5);       
      im = (int)(iea->ie[k].ypos+0.5);
      sigma2 = iea->ie[k].sigma * iea->ie[k].sigma * 2;
      for (i = -4; i <= 4; i++)
	{
	  if (im + i < 0 || im + i > dest->im.ny) continue; 
	  for (j=-4; j <= 4; j++)	
	    {
	      if (jm + j < 0 || jm + j > dest->im.nx) continue; 
	      tmpx = iea->ie[k].xpos - j - jm;
	      tmpx *= tmpx;
	      tmpy = iea->ie[k].ypos - i - im;
	      tmpy *= tmpy;
	      val = tmpx + tmpy;
	      val /= sigma2;
	      val = exp(-val);
	      val *= iea->ie[k].zmax;
	      pd[im+i].ui[jm+j] = ps[im+i].ui[jm+j] - (unsigned int)val; 
	    }
	}
    }
  inherit_from_im_to_im(dest,ois);	
  uns_oi_2_oi(dest,ois);
  dest->im.win_flag = ois->im.win_flag;
  dest->need_to_refresh = ALL_NEED_REFRESH;		
  return dest;
}




int  do_substract_peaks_to_image(void)
{
  O_i   *oid, *oi;
  imreg *imr;
  im_ext_ar *iea;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;

  if (imr->one_i == NULL)        return D_O_K;
  oi = imr->one_i;
  iea = (oi->im.use.to != IS_IM_EXREMUM || oi->im.use.stuff == NULL) ? NULL : (im_ext_ar *)oi->im.use.stuff;

  if(updating_menu_state != 0)
    {
      if (iea == NULL) active_menu->flags |=  D_DISABLED;
      else   active_menu->flags &= ~D_DISABLED;
      return D_O_K;		
    }

  if (iea == NULL || iea->n_ie < 1) return D_O_K;

  if (oi->im.data_type == IS_RGB16_PICTURE) 			
    oid = substract_float_image_extremum_to_RGB16_image(NULL, oi);
  else if (oi->im.data_type == IS_FLOAT_IMAGE) 			
    oid = substract_float_image_extremum_to_float_image(NULL, oi);
  else if (oi->im.data_type == IS_UINT_IMAGE) 			
    oid = substract_float_image_extremum_to_u_int_image(NULL, oi);
  else return win_printf_OK("Unsupported image type");

  add_image(imr, oid->type, (void*)oid);
  oid->oi_mouse_action = oi_mouse_RGB_GifAvg_action;
  find_zmin_zmax(oid);
  oid->need_to_refresh |= ALL_NEED_REFRESH;		
  return (refresh_image(imr, imr->n_oi - 1));

}


O_i 	*substract_x_profile_to_image(O_i *ois, d_s *ds)
{
  int i, j;
  int  onx, ony, nf;
  int	dest_type = 0;
  double val;
  union pix *pd;
  O_i *dest = NULL;


  if (ois == NULL || ois->im.pixel == NULL)
    return (O_i *) win_printf_ptr("No image found");

  ony = ois->im.ny;	onx = ois->im.nx;	
  nf = (ois->im.n_f < 1) ? 1 : ois->im.n_f;
  if (nf > 1)	return (O_i *) win_printf_ptr("Cannot treat movies");
  dest_type = IS_FLOAT_IMAGE;

  if (ds->nx != onx) return (O_i *) win_printf_ptr("Profile and image have incomptible size!");

  dest = create_one_image(onx, ony, dest_type);
  if (dest == NULL)    return (O_i *) win_printf_ptr("Can't create dest image");

  pd = dest->im.pixel;	
  for (i=0; i< ony; i++)
    {
      for (j=0; j<onx; j++)	
	{
	  get_raw_pixel_value(ois, j, i, &val);
	  pd[i].fl[j] = (float)val - ds->yd[j];
	}
    }
  inherit_from_im_to_im(dest,ois);	
  uns_oi_2_oi(dest,ois);
  dest->im.win_flag = ois->im.win_flag;
  dest->need_to_refresh = ALL_NEED_REFRESH;		
  return dest;
}

int  do_substract_x_profile_to_image(void)
{
  O_i   *oid, *ois;
  O_p *op;
  imreg *imr;
	
  if(updating_menu_state != 0)	return D_O_K;		

  if (ac_grep(cur_ac_reg,"%im%oi%oiop",&imr,&ois,&op) != 3)	return OFF;
  oid = substract_x_profile_to_image(ois, op->dat[0]);
  if (oid == NULL)	return D_O_K;
  add_image(imr, oid->type, (void*)oid);
  oid->oi_mouse_action = oi_mouse_RGB_GifAvg_action;
  find_zmin_zmax(oid);
  oid->need_to_refresh |= ALL_NEED_REFRESH;		
  return (refresh_image(imr, imr->n_oi - 1));
}


O_i 	*substract_y_profile_to_image(O_i *ois, d_s *ds)
{
  int i, j;
  int  onx, ony, nf;
  int	dest_type = 0;
  double val;
  union pix *pd;
  O_i *dest = NULL;


  if (ois == NULL || ois->im.pixel == NULL)
    return (O_i *) win_printf_ptr("No image found");

  ony = ois->im.ny;	onx = ois->im.nx;	
  nf = (ois->im.n_f < 1) ? 1 : ois->im.n_f;
  if (nf > 1)	return (O_i *) win_printf_ptr("Cannot treat movies");
  dest_type = IS_FLOAT_IMAGE;

  if (ds->nx != ony) return (O_i *) win_printf_ptr("Profile and image have incomptible size!");

  dest = create_one_image(onx, ony, dest_type);
  if (dest == NULL)    return (O_i *) win_printf_ptr("Can't create dest image");

  pd = dest->im.pixel;	
  for (i=0; i< ony; i++)
    {
      for (j=0; j<onx; j++)	
	{
	  get_raw_pixel_value(ois, j, i, &val);
	  pd[i].fl[j] = (float)val - ds->yd[i];
	}
    }
  inherit_from_im_to_im(dest,ois);	
  uns_oi_2_oi(dest,ois);
  dest->im.win_flag = ois->im.win_flag;
  dest->need_to_refresh = ALL_NEED_REFRESH;		
  return dest;
}


int  do_substract_y_profile_to_image(void)
{
  O_i   *oid, *ois;
  O_p *op;
  imreg *imr;
	
  if(updating_menu_state != 0)	return D_O_K;		

  if (ac_grep(cur_ac_reg,"%im%oi%oiop",&imr,&ois,&op) != 3)	return OFF;
  oid = substract_y_profile_to_image(ois, op->dat[0]);
  if (oid == NULL)	return D_O_K;
  add_image(imr, oid->type, (void*)oid);
  oid->oi_mouse_action = oi_mouse_RGB_GifAvg_action;
  find_zmin_zmax(oid);
  oid->need_to_refresh |= ALL_NEED_REFRESH;		
  return (refresh_image(imr, imr->n_oi - 1));
}






/* /\* */
/*  *		 */
/*  * */
/*  *\/ */
/* O_i 	*convert_float_image_to_expanded_RGB_char(O_i *dest, O_i *ois1, int exp, float inR, float inG, float inB) */
/* { */
/*   int i, j, im; */
/*   int  onx, ony, itmp, nf, nfd; */
/*   int	dest_type = 0; */
/*   float tmp; */
/*   float val, zi; */
/*   union pix *pd; */

/*   if (ois1 == NULL || ois1->im.pixel == NULL) */
/*     { */
/*       win_printf("No image found"); */
/*       return (NULL);	 */
/*     } */
/*   ony = ois1->im.ny;	onx = ois1->im.nx;	 */
/*   nf = ois1->im.n_f; */
/*   if (nf < 1) nf = 1; */

/*   if (dest != NULL) */
/*     { */
/*       if (dest->im.data_type != IS_RGB_PICTURE) 			 */
/* 	  return win_printf_ptr ("The destination image is not of unsigned char type\n" */
/* 		      "Cannot perform conversion!"); */
/*       if (((exp*ony) != dest->im.ny) || ((exp*onx) != dest->im.nx)) */
/* 	  return win_printf_ptr ("The destination size differs from the source one \n" */
/* 		      "Cannot perform conversion!"); */

/*       nfd = dest->im.n_f; */
/*       if (nfd < 1) nfd = 1; */
/*       if (nf != nfd)	  return win_printf_ptr ("The destination nb of frame differs from the source one \n" */
/* 						 "Cannot perform conversion!"); */
      
/*     } */
/*   dest_type = IS_RGB_PICTURE; */

/*   if (nf == 1) */
/*     dest = (dest != NULL) ? dest : create_one_image(exp*onx, exp*ony, dest_type); */
/*   else dest = (dest != NULL) ? dest : create_one_movie(exp*onx, exp*ony, dest_type, nf); */
/*   if (dest == NULL)      return win_printf_ptr("Can't create dest image"); */

/*   for (im = 0; im < nf; im++) */
/*     { */
/*     switch_frame(ois1,im); */
/*     switch_frame(dest,im); */
/*     pd = dest->im.pixel;	 */
/*     for (i = 0; i < dest->im.ny; i++) */
/*       { */
/* 	for (j=0; j< dest->im.nx; j++)	 */
/* 	  { */
/* 	    interpolate_image_point(ois1, ((float)j)/exp, ((float)i)/exp, &val, &zi); */
/* 	    //get_raw_pixel_value(ois1, j, i, &val); */
/* 	    tmp = val - ois1->z_min; */
/* 	    tmp /= ois1->z_max - ois1->z_min; */
/* 	    tmp *= 256; */
/* 	    itmp = (int)(inR*tmp+0.5); */
/* 	    itmp = (itmp < 0) ? 0 : itmp; */
/* 	    itmp = (itmp > 255) ? 255 : itmp; */
/* 	    pd[i].rgb[j].r = (unsigned char)itmp; */
/* 	    itmp = (int)(inG*tmp+0.5); */
/* 	    itmp = (itmp < 0) ? 0 : itmp; */
/* 	    itmp = (itmp > 255) ? 255 : itmp; */
/* 	    pd[i].rgb[j].g = (unsigned char)itmp; */
/* 	    itmp = (int)(inB*tmp+0.5); */
/* 	    itmp = (itmp < 0) ? 0 : itmp; */
/* 	    itmp = (itmp > 255) ? 255 : itmp; */
/* 	    pd[i].rgb[j].b = (unsigned char)itmp; */
/* 	  } */
/*       } */
/*     } */
/*   inherit_from_im_to_im(dest,ois1);	 */
/*   uns_oi_2_oi(dest,ois1); */
/*   dest->im.win_flag = ois1->im.win_flag; */
/*   dest->need_to_refresh = ALL_NEED_REFRESH;		 */
/*   return dest; */
/* } */



/*
 *		
 *
 */
O_i 	*convert_float_image_extremum_to_expanded_RGB_char(O_i *dest, O_i *ois1, int expa, double sigma, float inR, float inG, float inB)
{
  int i, j, im;
  int  onx, ony, itmp, nf, nfd, k, jm;
  int	dest_type = 0;
  float tmp, tmpx, tmpy;
  float val, sigma2;
  union pix *pd;
  im_ext_ar *iea;


  if (ois1 == NULL || ois1->im.pixel == NULL)
    return (O_i *) win_printf_ptr("No image found");

  ony = ois1->im.ny;	onx = ois1->im.nx;	
  nf = (ois1->im.n_f < 1) ? 1 : ois1->im.n_f;

  if (dest != NULL)
    {
      if (dest->im.data_type != IS_RGB_PICTURE) 			
	  return (O_i *) win_printf_ptr("The destination image is not of unsigned char type\n"
				"Cannot perform conversion!");
      if (((expa*ony) != dest->im.ny) || ((expa*onx) != dest->im.nx))
	return (O_i *) win_printf_ptr("The destination size differs from the source one \n"
			      "Cannot perform conversion!");
      nfd = (dest->im.n_f < 1) ? 1 : dest->im.n_f;
      if (nf > 1 || nfd > 1)	return (O_i *) win_printf_ptr("Cannot treat movies");
      
    }
  dest_type = IS_RGB_PICTURE;


  iea = (ois1->im.use.to != IS_IM_EXREMUM || ois1->im.use.stuff == NULL) ? NULL : (im_ext_ar *)ois1->im.use.stuff;
  if (iea == NULL || iea->n_ie < 1) return D_O_K;

  dest = (dest != NULL) ? dest : create_one_image(expa*onx, expa*ony, dest_type);
  if (dest == NULL)    return (O_i *) win_printf_ptr("Can't create dest image");

  sigma2 = (float)(sigma*sigma);
  pd = dest->im.pixel;	
  for (k = 0; k < (int)iea->n_ie; k++)
    {
      jm = (int)((iea->ie[k].xpos*expa)+0.5);       
      im = (int)((iea->ie[k].ypos*expa)+0.5);       
      for (i = -4; i <= 4; i++)
	{
	  if (im + i < 0 || im + i > dest->im.ny) continue; 
	  for (j=-4; j <= 4; j++)	
	    {
	      if (jm + j < 0 || jm + j > dest->im.nx) continue; 
	      tmpx = (iea->ie[k].xpos * expa) - j - jm;
	      tmpx *= tmpx;
	      tmpy = (iea->ie[k].ypos * expa) - i - im;
	      tmpy *= tmpy;
	      val = tmpx + tmpy;
	      val /= sigma2;
	      val = exp(-val);
	      val *= iea->ie[k].zmax;
	      tmp = val - ois1->z_min;
	      tmp /= ois1->z_max - ois1->z_min;
	      tmp *= 256;
	      itmp = (int)(inR*tmp+0.5);
	      itmp = (itmp < 0) ? 0 : itmp;
	      itmp = (itmp > 255) ? 255 : itmp;
	      pd[im+i].rgb[jm+j].r += (unsigned char)itmp;
	      itmp = (int)(inG*tmp+0.5);
	      itmp = (itmp < 0) ? 0 : itmp;
	      itmp = (itmp > 255) ? 255 : itmp;
	      pd[im+i].rgb[jm+j].g += (unsigned char)itmp;
	      itmp = (int)(inB*tmp+0.5);
	      itmp = (itmp < 0) ? 0 : itmp;
	      itmp = (itmp > 255) ? 255 : itmp;
	      pd[im+i].rgb[jm+j].b += (unsigned char)itmp;
	    }
	}
    }
  inherit_from_im_to_im(dest,ois1);	
  uns_oi_2_oi(dest,ois1);
  dest->im.win_flag = ois1->im.win_flag;
  dest->need_to_refresh = ALL_NEED_REFRESH;		
  return dest;
}



int  do_generate_image(void)
{
  O_i   *oid, *ois;
  imreg *imr;
  union pix *pd;
  static int i, num_spots = 10, image_dimension_x =128, image_dimension_y =128;
  static float Amplitude  = 2000., Mean_background = 8000., Noise_amplitude= 200.;
  static float sigma = 1.4, noise, signal;
  double *  x_c, *y_c;
  const gsl_rng_type * T_1;
  const gsl_rng_type * T_2;
  gsl_rng * r_1;
  gsl_rng * r_2;

  int j, k;
	
  if(updating_menu_state != 0)	return D_O_K;		

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  
  i = win_scanf("Generate an image with noise\n"
        "Number of bright spots %d  Amplitude %5f Mean background %5f \n"
        "Image noise amplitude %5f\n"
        "Image dimension in x %3d \n Image dimension in y %3d \n"
        "Sigma of the gaussian %5f\n"
		,&num_spots,&Amplitude,&Mean_background, &Noise_amplitude,&image_dimension_x,&image_dimension_y,&sigma);
  if (i == CANCEL) return D_O_K;

  x_c = (double *)calloc(num_spots, sizeof(double));
  y_c = (double *)calloc(num_spots, sizeof(double));


    oid = create_and_attach_oi_to_imr(imr, image_dimension_x, image_dimension_y, IS_UINT_IMAGE);
    if (oid == NULL)
      return win_printf_OK("Can't create dest movie");

  /* create a generator chosen by the
       environment variable GSL_RNG_TYPE */

  
  
    T_1 = gsl_rng_mt19937;//gsl_rng_default;
    r_1 = gsl_rng_alloc (T_1);
    T_2 = gsl_rng_mt19937;//gsl_rng_default;
    r_2 = gsl_rng_alloc (T_2);
    
  gsl_rng_set (r_1, (unsigned long int)clock());
  gsl_rng_set (r_2, (unsigned long int)clock()+2);
  
  //  gsl_rng_env_setup ();  
    //  if (!getenv("GSL_RNG_SEED")) gsl_rng_default_seed = clock();
  
  for (k= 0 ; k<num_spots ; k++)
    {
      x_c[k]= gsl_ran_flat (r_2, (double) 0, (double) oid->im.nx);
      y_c[k]= gsl_ran_flat (r_2, (double) 0, (double) oid->im.ny);

    }
  for (i = 0, pd = oid->im.pixel; i < oid->im.ny ; i++)
          {
              for (j=0; j< oid->im.nx; j++)
              {
                  signal = 0;
                  noise = gsl_ran_gaussian (r_1, (double) Noise_amplitude)+ (double)Mean_background;
  
		  for (k= 0 ; k<num_spots ; k++)
		    {
		      signal += Amplitude * exp (-(j-x_c[k])*(j-x_c[k])/2/sigma)* exp (-(i-y_c[k])*(i-y_c[k])/2/sigma) ;
		    }       
                    pd[i].ui[j] = ((noise+signal > 65535) ? 65535 : (unsigned short int)(noise+signal));
              }
           }



  gsl_rng_free (r_1);
  gsl_rng_free (r_2);

//  oid->oi_mouse_action = oi_mouse_RGB_GifAvg_action;
  find_zmin_zmax(oid);
  oid->need_to_refresh |= ALL_NEED_REFRESH;		
  return (refresh_image(imr, imr->n_oi - 1));
}


int  do_generate_movie(void)
{
  O_i   *oid, *ois;
  imreg *imr;
  union pix *pd;
  static int i, num_spots = 10, image_dimension_x =128, image_dimension_y =128, num_frames = 10;
  static float Amplitude  = 500., Mean_background = 2000., Noise_amplitude= 200.;
  static float sigma = 1.4, noise, signal, noise_signal, noise_background, sampling, photobleach=.9;
  double *  x_c, *y_c, *intensity_c;
  const gsl_rng_type * T_1;
  const gsl_rng_type * T_2;
  const gsl_rng_type * T_3;
  gsl_rng * r_1;
  gsl_rng * r_2;
  gsl_rng * r_3;

  int j, k, l;
	
  if(updating_menu_state != 0)	return D_O_K;		

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  
  i = win_scanf("Generate a movie with noise\n"
        "Number of frames %d\n Number of bright spots %d  Amplitude %5f Mean background %5f \n"
        "Image noise amplitude %5f\n"
        "Image dimension in x %3d \n Image dimension in y %3d \n"
        "Sigma of the gaussian %5f\n"
	"Photobleaching constant %5f\n"
		,&num_frames,&num_spots,&Amplitude,&Mean_background, &Noise_amplitude,&image_dimension_x,&image_dimension_y,&sigma,&photobleach);
  if (i == CANCEL) return D_O_K;

  x_c = (double *)calloc(num_spots, sizeof(double));
  y_c = (double *)calloc(num_spots, sizeof(double));
  intensity_c = (double *)calloc(num_spots, sizeof(double));
  for (i = 0 ; i < num_spots ; i++)
    {
      intensity_c[i]= 3;
    }

  oid = create_and_attach_movie_to_imr(imr, image_dimension_x, image_dimension_y, IS_UINT_IMAGE,num_frames);
  if (oid == NULL)
    return win_printf_OK("Can't create dest movie");
  set_oi_filename(oid, "generated_movie.gr");
  set_oi_path(oid,".");
  /* create a generator chosen by the
     environment variable GSL_RNG_TYPE */
  T_1 = gsl_rng_mt19937;//gsl_rng_default;
  r_1 = gsl_rng_alloc (T_1);
  T_2 = gsl_rng_mt19937;//gsl_rng_default;
  r_2 = gsl_rng_alloc (T_2);
  T_3 = gsl_rng_mt19937;//gsl_rng_default;
  r_3 = gsl_rng_alloc (T_3);
  
  gsl_rng_set (r_1, (unsigned long int)clock());
  gsl_rng_set (r_2, (unsigned long int)clock()+2);
  gsl_rng_set (r_3, (unsigned long int)clock()+3);
  
  //  gsl_rng_env_setup ();  
  //  if (!getenv("GSL_RNG_SEED")) gsl_rng_default_seed = clock();
  //position of the spots  
  for (k= 0 ; k<num_spots ; k++)
    {
      x_c[k]= gsl_ran_flat (r_2, (double) 0, (double) oid->im.nx);
      y_c[k]= gsl_ran_flat (r_2, (double) 0, (double) oid->im.ny);
    }


  for (k = 0; k < num_frames ; k++)
    {
      //on s'occupe du photobleaching
      for (i = 0 ; i < num_spots ; i++)
	{
	  sampling = gsl_ran_flat (r_3, (double) 0, (double) 1);
	  if ( (sampling > photobleach) && (intensity_c[i]>0) ) 
	    intensity_c[i]--;
	}
      //we fill frame k
      pd = (num_frames > 1) ? oid->im.pxl[k] : NULL;
      if (pd == NULL) return win_printf_OK("Not a movie");
      for (i = 0 ; i< /*num_frames */ oid->im.ny ; i++)
	{
	  for (j = 0 ; j < oid->im.nx ; j++)
	    {
	      signal = 0;
	      for (l = 0; l < num_spots ; l++)
		{
		  signal += intensity_c[l]*Amplitude * exp (-(j-x_c[l])*(j-x_c[l])/2/sigma)* exp (-(i-y_c[l])*(i-y_c[l])/2/sigma) ;
		}
	      noise_signal = gsl_ran_gaussian (r_1, (double) (sqrt(signal)));
	      noise_background = gsl_ran_gaussian (r_1, (double) (sqrt(Mean_background)));
	      signal+=noise_signal+noise_background+Mean_background;
	      noise = gsl_ran_gaussian (r_1, (double) (sqrt(signal)));
	      pd[i].ui[j] = ((noise+signal > 65535) ? 65535 : (unsigned short int)(noise+signal));		  
	    }
	  
	}
    }
  



  gsl_rng_free (r_1);
  gsl_rng_free (r_2);

//  oid->oi_mouse_action = oi_mouse_RGB_GifAvg_action;
  find_zmin_zmax(oid);
  oid->need_to_refresh |= ALL_NEED_REFRESH;		
  return (refresh_image(imr, imr->n_oi - 1));
}












//void print_state (size_t iter, gsl_multifit_fdfsolver * s);




int	find_best_fit_param_on_9x9_LM(double **zd,    // input data set
				      int verbose, // issue explicit error message
				      float error_a, // the error proportional to the signal
				      float error_b, // the error offset
				      gaussian_fit_params *g_fit_prm,
				      double *sigma,  // an array of imposed size of the gaussian
				      int n_sig // the number of sigma
				      )  
{	
  int i;
  const gsl_multifit_fdfsolver_type *T;
  gsl_multifit_fdfsolver *s;
  int status,j;
  unsigned int /*i,*/ iter = 0;
  const size_t n_x = 9;
  const size_t n_y = 9;
  const size_t p = 6;
  gsl_matrix *covar = gsl_matrix_alloc (p, p);
  double y[81], sigma_f[81];
  data_for_gaussian_fit d = {81, y, sigma_f};
  gsl_multifit_function_fdf f;
  double x_init[6];
  gsl_vector_view x ;
  
  
  if (zd == NULL || n_sig < 1) 
    {
      if (verbose) win_printf("No valid data \n"); 
      return 1;
    }
  
  //win_printf("we are in\n g_fit_prm a=%g\n g_fit_parm b %g \n sigma_x%g",g_fit_prm->a,g_fit_prm->b,g_fit_prm->sigma_x);
  for (i= 0; i<9; i++)
    {
      for (j= 0; j<9; j++)
	{
	  y[i*9+j] =zd[i][j];
	  sigma_f[i*9+j]=error_a*y[i*9+j]+error_b;
	}
    }
  //win_printf("No memory problem yet");
  x_init[0] = g_fit_prm->a;
  x_init[1] = g_fit_prm->x_c;
  x_init[2] = g_fit_prm->y_c;
  x_init[3] = g_fit_prm->sigma_x;//initial guess for sigma_x_in_pixel
  x_init[4] = g_fit_prm->sigma_y;//initial guess for sigma_y_in_pixel
  x_init[5] = g_fit_prm->b;
  
  win_printf("No soucis so far \n x_init_0 %g \n error_sigma %g",x_init[0],d.sigma[0]);
  x = gsl_vector_view_array (x_init, p);
  
  f.f = &gaussian_f;
  //win_printf("gaussian 1");
  f.df = &gaussian_df;
  //win_printf("gaussian 2");
  f.fdf = &gaussian_fdf;
  //win_printf("gaussian 3");
  f.n = n_x*n_y;
  f.p = p;
  //  win_printf("n %d \n p%d",f.n,f.p);
  f.params = &d;
  //win_printf("Yop");
  T = gsl_multifit_fdfsolver_lmsder;
  s = gsl_multifit_fdfsolver_alloc (T, 81, p);
  gsl_multifit_fdfsolver_set (s, &f, &x.vector);
  do
    {
      iter++;
      status = gsl_multifit_fdfsolver_iterate (s);
      
      //win_printf ("status = %s\n", gsl_strerror (status));
      gsl_strerror (status); 
      if (status)
	break;
      
      status = gsl_multifit_test_delta (s->dx, s->x, 1e-4, 1e-4);
    }
  while (status == GSL_CONTINUE && iter < 500);
  gsl_multifit_covar (s->J, 0.0, covar);
  {
    double chi = gsl_blas_dnrm2(s->f);
    double dof = 81 - p;
    double c = GSL_MAX_DBL(1, chi / sqrt(dof));
    //win_printf("on attaque le resultat");   
    g_fit_prm->a=FIT(0);
    g_fit_prm->error_a= c*ERR(0);
    g_fit_prm->x_c=FIT(1);
    g_fit_prm->error_x_c= c*ERR(1);
    g_fit_prm->sigma_x=FIT(2);
    g_fit_prm->error_sigma_x= c*ERR(2);
    g_fit_prm->y_c=FIT(3);
    g_fit_prm->error_y_c= c*ERR(3);
    g_fit_prm->sigma_y=FIT(4);
    g_fit_prm->error_sigma_y= c*ERR(4);
    g_fit_prm->b=FIT(5);    
    g_fit_prm->error_b= c*ERR(5);
    g_fit_prm->chi2 = chi;
    /* win_printf("chisq/dof = %g\n",  pow(chi, 2.0) / dof); */
    //win_printf("fit \n A=  %g \n B= %g \n xc=  %g \n yc %g \n sigmax %g \n sigmay %g",FIT(0),FIT(5),FIT(1),FIT(2),FIT(3),FIT(4));
    /* win_printf ("A      = %.5f +/- %.5f\n", FIT(0), c*ERR(0)); */
    /* win_printf ("lambda = %.5f +/- %.5f\n", FIT(1), c*ERR(1)); */
    /* win_printf ("b      = %.5f +/- %.5f\n", FIT(2), c*ERR(2)); */
  }
  
  //  win_printf ("status = %s\n", gsl_strerror (status));
  
  gsl_multifit_fdfsolver_free (s);
  gsl_matrix_free (covar);
  
  return 0;
}

 
int do_gaussian_fit_2D (void)
{
  const gsl_multifit_fdfsolver_type *T;
  gsl_multifit_fdfsolver *s;
  int status;
  unsigned int /*i,*/ iter = 0;
  const size_t n_x = N;
  const size_t n_y=N;
  const size_t p = N_params;
  gsl_matrix *covar = gsl_matrix_alloc (p, p);
  double y[N*N], sigma[N*N];
  data_for_gaussian_fit d = { n_x, y, sigma};
  gsl_multifit_function_fdf f;
  double x_init[N_params] = { 1.0, 0.0, 0.0,1,1,0.0 };
  gsl_vector_view x = gsl_vector_view_array (x_init, p);
  const gsl_rng_type * type;
  gsl_rng * r;
  
  gsl_rng_env_setup();
  
  type = gsl_rng_default;
  r = gsl_rng_alloc (type);
  
  f.f = &gaussian_f;
  f.df = &gaussian_df;
  f.fdf = &gaussian_fdf;
  f.n = n_x*n_y;
  f.p = p;
  f.params = &d;
  
  T = gsl_multifit_fdfsolver_lmsder;
  s = gsl_multifit_fdfsolver_alloc (T, n_x-n_y, p);
  gsl_multifit_fdfsolver_set (s, &f, &x.vector);
  do
    {
      iter++;
      status = gsl_multifit_fdfsolver_iterate (s);
      
      win_printf ("status = %s\n", gsl_strerror (status));
      if (status)
	break;
      
      status = gsl_multifit_test_delta (s->dx, s->x, 1e-4, 1e-4);
    }
  while (status == GSL_CONTINUE && iter < 500);
  
  gsl_multifit_covar (s->J, 0.0, covar);
  
  
  
  {
    double chi = gsl_blas_dnrm2(s->f);
    double dof = n_x*n_y - p;
    double c = GSL_MAX_DBL(1, chi / sqrt(dof));
    
    win_printf("chisq/dof = %g\n",  pow(chi, 2.0) / dof);
    win_printf ("A      = %.5f +/- %.5f\n", FIT(0), c*ERR(0));
    win_printf ("lambda = %.5f +/- %.5f\n", FIT(1), c*ERR(1));
    win_printf ("b      = %.5f +/- %.5f\n", FIT(2), c*ERR(2));
  }
  
  win_printf ("status = %s\n", gsl_strerror (status));
  
  gsl_multifit_fdfsolver_free (s);
  gsl_matrix_free (covar);
  gsl_rng_free (r);
  return 0;
}


/*
 *		
 *
 */
O_i 	*convert_float_image_to_expanded_RGB_char(O_i *dest, O_i *ois1, int exp, float inR, float inG, float inB)
{
  int i, j, im;
  int  onx, ony, itmp, nf, nfd;
  int	dest_type = 0;
  float tmp;
  float val, zi;
  union pix *pd;

  if (ois1 == NULL || ois1->im.pixel == NULL)
    {
      win_printf("No image found");
      return (NULL);	
    }
  ony = ois1->im.ny;	onx = ois1->im.nx;	
  nf = ois1->im.n_f;
  if (nf < 1) nf = 1;

  if (dest != NULL)
    {
      if (dest->im.data_type != IS_RGB_PICTURE) 			
	  return (O_i *) win_printf_ptr ("The destination image is not of unsigned char type\n"
		      "Cannot perform conversion!");
      if (((exp*ony) != dest->im.ny) || ((exp*onx) != dest->im.nx))
	  return (O_i *) win_printf_ptr ("The destination size differs from the source one \n"
		      "Cannot perform conversion!");

      nfd = dest->im.n_f;
      if (nfd < 1) nfd = 1;
      if (nf != nfd)	  return (O_i *) win_printf_ptr ("The destination nb of frame differs from the source one \n"
						 "Cannot perform conversion!");
      
    }
  dest_type = IS_RGB_PICTURE;

  if (nf == 1)
    dest = (dest != NULL) ? dest : create_one_image(exp*onx, exp*ony, dest_type);
  else dest = (dest != NULL) ? dest : create_one_movie(exp*onx, exp*ony, dest_type, nf);
  if (dest == NULL)      return (O_i *) win_printf_ptr("Can't create dest image");

  for (im = 0; im < nf; im++)
    {
    switch_frame(ois1,im);
    switch_frame(dest,im);
    pd = dest->im.pixel;	
    for (i = 0; i < dest->im.ny; i++)
      {
	for (j=0; j< dest->im.nx; j++)	
	  {
	    interpolate_image_point(ois1, ((float)j)/exp, ((float)i)/exp, &val, &zi);
	    //get_raw_pixel_value(ois1, j, i, &val);
	    tmp = val - ois1->z_min;
	    tmp /= ois1->z_max - ois1->z_min;
	    tmp *= 256;
	    itmp = (int)(inR*tmp+0.5);
	    itmp = (itmp < 0) ? 0 : itmp;
	    itmp = (itmp > 255) ? 255 : itmp;
	    pd[i].rgb[j].r = (unsigned char)itmp;
	    itmp = (int)(inG*tmp+0.5);
	    itmp = (itmp < 0) ? 0 : itmp;
	    itmp = (itmp > 255) ? 255 : itmp;
	    pd[i].rgb[j].g = (unsigned char)itmp;
	    itmp = (int)(inB*tmp+0.5);
	    itmp = (itmp < 0) ? 0 : itmp;
	    itmp = (itmp > 255) ? 255 : itmp;
	    pd[i].rgb[j].b = (unsigned char)itmp;
	  }
      }
    }
  inherit_from_im_to_im(dest,ois1);	
  uns_oi_2_oi(dest,ois1);
  dest->im.win_flag = ois1->im.win_flag;
  dest->need_to_refresh = ALL_NEED_REFRESH;		
  return dest;
}
int  do_convert_float_image_to_expand_RGB_char(void)
{
  O_i   *oid, *ois;
  imreg *imr;
  static int i, ni1 = 0, ni2 = 1, expa = 4; 
  static float iR1 =0.5, iG1 = 0.5, iB1 = 0.5, iR2 = 0, iG2 = 0.5, iB2 = 0;
  static double sigma = 1.4;
	
  if(updating_menu_state != 0)	return D_O_K;		

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  
  i = win_scanf("Combine images and maxima in an RGB\n"
		"Specify the first image nb %8d \nscaled by %5f R %5f G %5f B\n"
		"Expand image by factor %8d\n"
		"Specify the second image nb %8d \nscaled by %5f R %5f G %5f B\n"
		"Sigma of the gaussian %10lf\n"
		,&ni1,&iR1,&iG1,&iB1,&expa,&ni2,&iR2,&iG2,&iB2,&sigma);
  if (i == CANCEL) return D_O_K;

  oid = convert_float_image_to_expanded_RGB_char(NULL, imr->o_i[ni1], expa, iR1, iG1, iB1);
  if (oid == NULL)	return D_O_K;
  oid->im.mode  = TRUE_RGB;
  if (oid->im.user_nipar < 8)
    {
      oid->im.user_nipar = 8;
      free(oid->im.user_ispare[0]);
      oid->im.user_ispare[0] = (int*)calloc(oid->im.user_nipar,sizeof(int));	
      if (oid->im.user_ispare[0] == NULL) return D_O_K;
    }

  if (oid->im.user_nfpar < 8)
    {
      oid->im.user_nfpar = 8;
      free(oid->im.user_fspare[0]);
      oid->im.user_fspare[0] = (float*)calloc(oid->im.user_nipar,sizeof(float));	
      if (oid->im.user_fspare[0] == NULL) return D_O_K;
    }
  oid->im.user_ispare[0][0] = ni1;
  oid->im.user_ispare[0][1] = expa;
  oid->im.user_fspare[0][0] = iR1;
  oid->im.user_fspare[0][1] = iG1;
  oid->im.user_fspare[0][2] = iB1;
  oid = convert_float_image_extremum_to_expanded_RGB_char(oid, imr->o_i[ni2], expa, sigma, iR2, iG2, iB2);
  //  oid = convert_float_image_to_RGB_char(oid, imr->o_i[ni2], iR2, iG2, iB2);
  if (oid == NULL)	return D_O_K;
  oid->im.user_ispare[0][2] = ni2;
  oid->im.user_fspare[0][3] = iR2;
  oid->im.user_fspare[0][4] = iG2;
  oid->im.user_fspare[0][5] = iB2;
  oid->im.user_fspare[0][6] = sigma;
  add_image(imr, oid->type, (void*)oid);
  oid->oi_mouse_action = oi_mouse_RGB_GifAvg_action;
  find_zmin_zmax(oid);
  oid->need_to_refresh |= ALL_NEED_REFRESH;		
  return (refresh_image(imr, imr->n_oi - 1));
}


int oi_mouse_follow_spot_action(struct one_image *oi, int x0, int y0, int mode, DIALOG *d)
{
  int x, y, ip, i, ROI[4], n0=0, keep_mode = 0,j;
  static int old_x = 0, old_y = 0, remove = 0;
  float px1, py1;
  static double reduce = .5, sig[15] = {0.7,0.75,0.8,0.85,0.9,0.95,1.0,1.05,1.1,1.15,1.2,1.25,1.3,1.35,1.4};
  static float err_a = 0.15, err_b = 200;
  static int w = 5;
  imreg *imr;
  im_ext_ar *iea = NULL;
  char question[1024];
  double  **zdp = NULL, *zdpl[9], zdpl1[81], amp = 0, background = 0, E = 0, sigma = 0;
  static int fit_meth=0;
  gaussian_fit_params gfp;
  O_p *op = NULL;
  d_s *ds = NULL;
  //static double reduce=.5;

  

  for (zdp = zdpl, i = 0; i < 9; i++)
    zdpl[i] = zdpl1 + 9*i;

  (void)x0;
  (void)y0;
  (void)mode;
  
  if (d->dp == NULL)    return win_printf_OK("Could not find dialog");
  imr = (imreg*)d->dp;        /* the image region is here */
  
  if (imr->one_i == NULL)        return win_printf_OK("Could not find image data");
  oi = imr->one_i;
  iea = (oi->im.use.to != IS_IM_EXREMUM || oi->im.use.stuff == NULL) ? NULL : (im_ext_ar *)oi->im.use.stuff;	  


  i = win_scanf("Find bright spots in image\n"
		"Sigma of gaussian %12lf\n"
		"Specify the threshold in Z %12lf\n"
		"Specify the threshold in dZ %12lf\n"
		"Define the Signal error as er(Y) = a*y +b\n"
		"Enter a %8f and b %8f\n"
		    ,&sigma_s,&thres_s, &tolerance_s,&err_a_s,&err_b_s);



  ip = find_op_nb_of_source_specific_ds_in_oi(oi, "Max_time_trace");
  if (ip < 0 || ip >= oi->n_op)
    {
      op = create_and_attach_op_to_oi(oi, oi->im.n_f, oi->im.n_f, 0, 0);
      if (op == NULL) return D_O_K;
      ds = op->dat[0];
      set_ds_dot_line(ds);
      set_ds_point_symbol(ds, "\\pt4\\oc");
      ds->nx = ds->ny = 0;
    }
  else
    {
      op = oi->o_p[ip];
      ds = op->dat[0];
      ds->nx = ds->ny = 0;
    }
  set_ds_source(ds, "histo multi max found with threshold %g tolerence %d",thres_s,tolerance_s);
  for (i = 0; i < (int)iea->n_ie; i++)
    add_new_point_to_ds(ds, iea->ie[i].zmax, i); 
  sort_ds_along_x(ds);
  for (i = 0; i < (int)iea->n_ie && i < ds->nx; i++)
    {
      ds->yd[i] = ds->xd[i];
      ds->xd[i] = i;
    }
  oi->need_to_refresh |= PLOT_NEED_REFRESH;
  



  x = mouse_x; y = mouse_y;
  px1 = x_imr_2_imdata(imr, mouse_x);    
  py1 = y_imr_2_imdata(imr, mouse_y);    
  //win_printf("We are in");
  if(x != old_x || x != old_y)
    {
     
      for (j = oi->im.c_f ; j < oi->im.n_f; j++) 
       	{
	  switch_frame(oi,j);
	  refresh_image(imr, UNCHANGED);
	  
	  ROI[0] = (int)(0.5+px1) - w;	  ROI[1] = (int)(0.5+px1) + w;
	  ROI[2] = (int)(0.5+py1) - w;	  ROI[3] = (int)(0.5+py1) + w;
	  if (iea != NULL) n0 = iea->n_ie;
	  iea = find_maxima_positions(oi, iea/*NULL*/, thres_s*reduce, tolerance_s*reduce, 1, ROI, 1/*0*/, sigma_s, err_a_s, err_b_s);
	  //win_printf("We are in %d \n n0 %d",iea->n_ie, n0);
	  if ((int)(iea->n_ie) >/*=*/ n0)
	    {
	      i = win_printf("Image number %d\n Peak found at x = %g y = %g on image %d\n amp %g background %g\n\\chi^2 = %g \\sigma  = %g\n"
			     "Press OK to keep CANCEL to refuse this maximum\n"
			     ,oi->im.c_f,iea->ie[iea->n_ie-1].xpos,iea->ie[iea->n_ie-1].ypos,oi->im.c_f, iea->ie[iea->n_ie-1].zmax,iea->ie[iea->n_ie-1].dnb, iea->ie[iea->n_ie-1].chi2, iea->ie[iea->n_ie-1].sigma);
	      if (i == CANCEL) iea->n_ie = n0;
	      else 
		{
		  if (oi->im.data_type == IS_RGB16_PICTURE) 
		    convert_maxima_search_info_in_GREEN(iea, oi, thres_s, tolerance_s);
		  else 
		    {
		      convert_maxima_search_info_in_plot(iea, oi, thres_s, tolerance_s);
		      oi->need_to_refresh |= PLOT_NEED_REFRESH;	  
		    }
		}
	    }
	  else
	    {
	      i = win_scanf("No peak found! you can reduce the theshold\n"
			    "Reduction factor %10lf\n"
			    "Extend the search arround the mouse position\n"
			    "New extend - and + arround position %8d\n",&reduce,&w);
	      //if (i == CANCEL) return D_O_K;
	    }
	 }
    }
  old_x = x;
  old_y = y;
  oi->oi_mouse_action = NULL;
  oi->need_to_refresh |= PLOT_NEED_REFRESH;	
  return (refresh_image(imr, UNCHANGED));
}

int do_follow_spot_in_time(void)
{
  O_i   *oi;
  imreg *imr;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;
  if(updating_menu_state != 0)	return D_O_K;

  
 
  oi->oi_mouse_action = oi_mouse_follow_spot_action;		
  return D_O_K;
}

MENU *GifAvg_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
	add_item_to_menu(mn,"Movie average decime", do_movie_avg_starting_at_every,NULL,0,NULL);
	add_item_to_menu(mn,"Fit Movie avg. to Exp", do_movie_avg_starting_at_every_fit_exp,NULL,0,NULL);
	add_item_to_menu(mn,"Fit Movie avg. to Exp RGB16", do_movie_avg_starting_at_every_fit_exp_RGB16,NULL,0,NULL);
	add_item_to_menu(mn,"Fit Movie Exp. \\tau", do_movie_avg_starting_at_every_fit_tau_exp,NULL,0,NULL);
	add_item_to_menu(mn,"Combine two images in RGB", do_convert_float_image_to_RGB_char,NULL,0,NULL);
	add_item_to_menu(mn,"Find fluo Maxima",do_find_fluo_maxima ,NULL,0,NULL);
	add_item_to_menu(mn,"Dump peaks in CSV",dump_peaks_to_csv,NULL,0,NULL);
	add_item_to_menu(mn,"Dump ranked peaks plot",rank_maxima_search_info_in_plot,NULL,0,NULL);
	add_item_to_menu(mn,"Expand and convert image in RGB",do_convert_float_image_to_expand_RGB_char,NULL,0,NULL);
	add_item_to_menu(mn,"Substract peaks to image",do_substract_peaks_to_image,NULL,0,NULL);
	add_item_to_menu(mn,"Substract x profile",do_substract_x_profile_to_image,NULL,0,NULL);
	add_item_to_menu(mn,"Substract y profile",do_substract_y_profile_to_image,NULL,0,NULL);
	add_item_to_menu(mn,"Generate image",do_generate_image,NULL,0,NULL);
	add_item_to_menu(mn,"Generate movie",do_generate_movie,NULL,0,NULL);
	add_item_to_menu(mn,"Follow spot in time",do_follow_spot_in_time,NULL,0,NULL);


	//	add_item_to_menu(mn,"average profile", do_GifAvg_average_along_y,NULL,0,NULL);
	//add_item_to_menu(mn,"image z rescaled", do_GifAvg_image_multiply_by_a_scalar,NULL,0,NULL);
	return mn;
}

int	GifAvg_main(int argc, char **argv)
{
  add_image_treat_menu_item ( "GifAvg", NULL, GifAvg_image_menu(), 0, NULL);
  (void)argc;
  (void)argv;
  
  return D_O_K;
}

int	GifAvg_unload(int argc, char **argv)
{
  remove_item_to_menu(image_treat_menu, "GifAvg", NULL, NULL);
  (void)argc;
  (void)argv;
  return D_O_K;
}
#endif

