#ifndef _GIFAVG_H_
#define _GIFAVG_H_


#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_matrix.h>
#include "xvin.h"

//typedef struct im_max
//{
//  int x0, x1, y0, y1, xm, ym, np/*peak number--nbre pixel au dessus du seuil?*/, inb/*image number?*/;            // a rectangle containing the max in pixels
//  double xpos, ypos, weight, zmax, dnb/*  background*/, chi2, sigma;      // trap size
//} im_ext;


//typedef struct im_max_array
//{
//    struct im_max *ie;
//  unsigned int n_ie, m_ie, c_ie/* number, allocated, current ie*/;
//} im_ext_ar;


typedef struct Data {
    size_t n_x;
  double * y;
  double * sigma;
}data_for_gaussian_fit;

typedef struct gaussian_fit_parameters{
  double a;
  double error_a;
  double x_c;
  double error_x_c;
  double y_c;
  double error_y_c;
  double sigma_x;
  double error_sigma_x;
  double sigma_y;
  double error_sigma_y;
  double b;
  double error_b;
  double chi2;
}gaussian_fit_params;

#define FIT(i) gsl_vector_get(s->x, i)
#define ERR(i) sqrt(gsl_matrix_get(covar,i,i))
#define N 25
#define N_params 6

int gaussian_f (const gsl_vector * x, void *data, gsl_vector * f)
{
 data_for_gaussian_fit * data_to_treat=NULL;
 data_to_treat = (data_for_gaussian_fit *) data;
 size_t n_x = data_to_treat->n_x;
 //size_t n_y = data_to_treat->n_y;
 //size_t n_y=n_x;
 double * y = data_to_treat->y;
 double * sigma = data_to_treat->sigma;
 double A = gsl_vector_get (x, 0);
 double x_c = gsl_vector_get (x, 1);
 double y_c = gsl_vector_get (x, 2);
 double sigma_x = gsl_vector_get (x, 3);
 double sigma_y = gsl_vector_get (x, 4);
 double B = gsl_vector_get (x, 5);
 static int first =0;
 size_t i;
 size_t j;

 if (first == 0) 
   {
     win_printf("A %g\n n_x in %d, \n x_c %g",A,n_x,x_c);
     first++;
   }
 for (i = 0; i < 9; i++)
 {
     for (j = 0; j < 9; j++)
       {
           /* Model Yi = A * exp(-x^2/2\sigma_x^2)*exp(-y^2/2\sigma_y^2) + b */
           double x = j;
           double z = i;
           if (sigma_x ==0 || sigma_y == 0) return GSL_FAILURE;
           double Yij = A * exp (-(x-x_c)*(x-x_c)/2/sigma_x)* exp (-(z-y_c)*(z-y_c)/2/sigma_y) + B;
           gsl_vector_set (f, i*9+j, (Yij - y[i*9+j])/sigma[i*9+j]);
       }
 }
 // win_printf("out gauss");
 return GSL_SUCCESS;
}

int gaussian_df (const gsl_vector * x, void *data, gsl_matrix * J)
{
  // size_t n_x = ((data_for_gaussian_fit *)data)->n_x;
 // size_t n_y = ((data_for_gaussian_fit *)data)->n_x;
 double *sigma = ((data_for_gaussian_fit *) data)->sigma;
 //double *y = ((data_for_gaussian_fit *)data)->y;
 
 double A = gsl_vector_get (x, 0);
 double x_c = gsl_vector_get (x, 1);
 double y_c = gsl_vector_get (x, 2);
 double sigma_x = gsl_vector_get (x, 3);
 double sigma_y = gsl_vector_get (x, 4);
 //double B = gsl_vector_get (x, 5);

 size_t i;
 size_t j;
 for (i = 0; i < 9; i++)
   {
     for (j = 0; j < 9; j++)

         {
           /* Jacobian matrix J(i,j) = dfi / dxj, */
           /* where fi = (Yi - yi)/sigma[i],      */
	   /* and the xj are the parameters  */
           double x = j;
           double z = i;
           double s = sigma[i*9+j];
           double e = A * exp (-(x-x_c)*(x-x_c)/2/sigma_x)* exp (-(z-y_c)*(z-y_c)/2/sigma_y) ;
           gsl_matrix_set (J, i*9+j, 0, exp (-(x-x_c)*(x-x_c)/2/sigma_x)* exp (-(z-y_c)*(z-y_c)/2/sigma_y)/s);
           gsl_matrix_set (J, i*9+j, 1, (x-x_c)*e/s/sigma_x);
           gsl_matrix_set (J, i*9+j, 2, (z-y_c)*e/s/sigma_y);
           gsl_matrix_set (J, i*9+j, 3, (x-x_c)*(x-x_c)*e/s/sigma_x/sigma_x/2);
           gsl_matrix_set (J, i*9+j, 4, (z-y_c)*(z-y_c)*e/s/sigma_y/sigma_y/2);
           gsl_matrix_set (J, i*9+j, 5, 1/s);
        }
       }
 // win_printf("df out");
 return GSL_SUCCESS;
}

int gaussian_fdf (const gsl_vector * x, void *data,gsl_vector * f, gsl_matrix * J)
{
 gaussian_f (x, data, f);
 gaussian_df (x, data, J);

 return GSL_SUCCESS;
}

PXV_FUNC(int, do_GifAvg_average_along_y, (void));

PXV_FUNC(MENU*, GifAvg_image_menu, (void));
PXV_FUNC(int, GifAvg_main, (int argc, char **argv));
#endif

