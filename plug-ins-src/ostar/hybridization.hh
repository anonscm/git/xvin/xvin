#pragma once
class Hybridization
{
public:
    Hybridization ();
    virtual ~Hybridization ();

    int index();
    float position();
    
    bool closed();
    float score();
    bool opened();
    int closed(bool closed);
    int score(float score);
    int opened(bool opened);

private:
    float position_;
    int index_;
    bool closed_;
    float score_;
    int predIndex_;
};
