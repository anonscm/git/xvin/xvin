#pragma once
//#include "oligo-heuristic.hh"
#include "hybridization.hh"
#include <queue>
#include <string>
class OligoAStar
{
public:
    OligoAStar (std::vector<Hybridization&>& hybData, float neighbourThreshold);
    virtual ~OligoAStar ();

    int shortestPath(void);
    
    float cost(Hybridization& a, Hybridization& b);
    float heuristic(Hybridization& a, Hybridization& b);
    bool solutionFound(Hybridization& hyb);
    std::string constructPath(Hybridization& hyb);


private:
    std::vector<Hybridization&> openList_;
    std::set<Hybridization&> closedList_;
    std::set<Hybridization&> lostHyb_;
    std::vector<Hybridization&>& hybData_;
    float neighbourThreshold_;
    int fillOpenList(Hybridization& hyb);
};
