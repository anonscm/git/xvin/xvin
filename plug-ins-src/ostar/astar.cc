#include "astar.hh"
#include <cfloat>

OligoAStar::OligoAStar(std::vector<Hybridization>& hybData, float neighbourThreshold)
    : hybData_(hybData),
      neighbourThreshold_(neighbourThreshold)
{
    for (Hybridization& hyb : hybData)
    {
        hyb.closed(false);
        hyb.score(FLT_MAX);
    }

    openList_.push(hybData[0]);
    hybData[0].score(0);
}

OligoAStar::~OligoAStar()
{
}

float OligoAStar::cost(Hybridization& a, Hybridization& b)
{
    return 0;
}

float OligoAStar::heuristic(Hybridization& a, Hybridization& b)
{
    return 0;
}

int OligoAStar::fillOpenList(Hybridization& hyb)
{
    bool stop = false;

    for (int i = 0; i < hybData_ && !stop; ++i)
    {
        int pidx = hyb.index() - i;
        int nidx = hyb.index() + i;
        stop = true;

        if (pidx >= 0)
        {
            Hybridization& phyb = hybData_[pidx];

            if (!phyb.closed() && (hyb.position() - phyb.position()) < neighbourThreshold_)
            {
                //Change score
                //phyb.score();
                if (phyb.opened())
                {
                    std::make_heap(openList_.begin(), openList_.end());
                }

                openList_.push_back(phyb);
                std::push_heap(openList_.begin(), openList_.end());
                stop = false;
            }
        }

        if (nidx < hybData_.size())
        {
            Hybridization& nhyb = hybData_[nidx];

            if (!nhyb.closed() && (nhyb.position() - hyb.position()) < neighbourThreshold_)
            {
                //Change score
                //phyb.score();
                if (nhyb.opened())
                {
                    std::make_heap(openList_.begin(), openList_.end());
                }

                openList_.push_back(nhyb);
                std::push_heap(openList_.begin(), openList_.end());
                openList_.push(nhyb);
                stop = false
            }
        }
    }

    return 0;
}

int OligoAStar::shortestPath(void)
{
    while (!openList_.empty())
    {
        Hybridization& current = openList_.top();
        openList_.pop();
        current.closed(true);

        if (solutionFound(current))
        {
            constructPath(current)
            // Record solution
        }

        fillOpenList(current);
    }

    return 0;
}
