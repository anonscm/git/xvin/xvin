#ifndef _FIND_H_
#define _FIND_H_

#define	FIND_LOWER		0x0100
#define	FIND_UPPER		0x0200
#define	FIND_BEGIN		0x1000
#define	FIND_MIDDLE	    0x2000
#define	FIND_END		0x4000

extern int FIND_MINIMAL_INTERVAL_LENGTH;


PXV_FUNC(int, find_main, 			(int argc, char **argv));
PXV_FUNC(int, find_unload,			(int argc, char **argv));

PXV_FUNC(int*, impose_indices,		(float dt, float cycle_period, float start_time, int N, int *n_ind));
PXV_FUNC(int, ask_for_parameters_to_search_for_indices, 
									(int *bool_search_indices, int *point_position, int *plateau_position, 
									int *bool_remove_bounds, int *n_last, int *bool_shift_indices, int *n_shift, 
									int *bool_add_more_indices, int *n_more, int *N_shift_auto) );
	
PXV_FUNC(int, find_search_domain,	(float *x, int nx, int plateau_position, float *y_min, float *y_max));
						
PXV_FUNC(int*, find_set_of_indices, (float *x, int nx, int *n_ind,
						int bool_search_indices, int point_position, int plateau_position, 
						int bool_remove_bounds, int n_last, 
						int bool_shift_indices, int n_shift, 
						int bool_add_more_indices, int n_more,
						int N_shift_auto,
						float y_min, float y_max) );


PXV_FUNC(int*, find_and_keep_long_intervals, (int *index, int n_in, int *n_out, int N) ); // added 2007/10/02
PXV_FUNC(int, do_find_set_interval_length, (void) ); // added 2007/10/02, used in plugin cycles

#endif

