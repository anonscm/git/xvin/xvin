#ifndef _FIND_C_
#define _FIND_C_

#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"

/* If you include other plug-ins header do it here*/ 

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "find.h"


int FIND_MINIMAL_INTERVAL_LENGTH=1; // longueur mini des intervalles a considerer
// par defaut, c'est 1, mais c'est une variable que l'on peut modifier avec la fonction
// do_find_set_interval_length().

/************************************************************************************************************/
/* this function is the one used by all functions in this plug'in											*/
/************************************************************************************************************/
int *find_set_of_indices(float *x, int nx, int *n_ind,
						int bool_search_indices, int point_position, int plateau_position, 
						int bool_remove_bounds, int n_last, 
						int bool_shift_indices, int n_shift, 
						int bool_add_more_indices, int n_more,
						int N_shift_auto,
						float y_min, float y_max)
{	int		i, k2; // slow loops, no need of registers, let's save some cache memory 
	int		*ind=NULL, *ind1=NULL, *ind2=NULL; // indices
	int		n_ind0, n_ind1, n_ind2;
	
	if (bool_search_indices==1)
	{	
		// search for indices: intervals first
		ind = find_interval_float (x, nx-1, y_min, y_max, &n_ind0);
		
		// then only intervals which are large enough:
        ind1 = find_and_keep_long_intervals(ind, n_ind0, &n_ind1, FIND_MINIMAL_INTERVAL_LENGTH);
        free(ind);
                
        // and then the first or last or middle indices:
		if (point_position==0)	ind2  = find_and_keep_firsts_int(ind1,  n_ind1, &n_ind2);
		if (point_position==1)	ind2  = find_and_keep_middles_int(ind1, n_ind1, &n_ind2);
		if (point_position==2)	ind2  = find_and_keep_lasts_int(ind1,   n_ind1, &n_ind2);
		free(ind1);
		ind=ind2;	
			
		k2=0;	// a shift on index if the first one is removed
		if (bool_remove_bounds==1)
		{	n_ind2 -= (1+n_last);  // there will be n_last points less : the first and the last are removed.
			for (i=0; i<n_ind2; i++)		ind[i]=ind[i+1];
		}
		if (n_ind2<=0) return(NULL); 
				
		if (bool_shift_indices==1)
		{	for (k2=0; k2<n_ind2; k2++)
			{	ind[k2] += n_shift;
			}
		}
			
		if (bool_add_more_indices==1)
		{	ind2 = calloc(n_ind2*n_more, sizeof(float));
			for (i=0; i<n_more; i++)
			{	for (k2=0; k2<n_ind2; k2++)
				{	ind2[k2*n_more+i] = ind[k2] + i;
				}
			}
			free(ind);
			ind=ind2;
			n_ind2 *= n_more;
		}
		
		n_ind[0] = n_ind2;
	}		
		
	else	// we impose the indices
	{	// n_ind = (int)((nx - tau_index[n_tau-1]) / N_shift);
		n_ind[0] = (int)(nx/N_shift_auto);
		ind=(int*)calloc(n_ind[0], sizeof(int));
		for (i=0; i<n_ind[0]; i++)
		{	ind[i] = (int)(i*N_shift_auto);
		}
	}

	return ind;
} /* end of function 'find_set_of_indices' */


int do_find_set_interval_length(void)
{	int	 n;
	char s[512];
	
	if(updating_menu_state != 0)	return D_O_K;	

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine sets an internal variable \n"
				"that is used to discriminate in the intervals to search points in\n"
                "are large/long enough");
	}

    n=FIND_MINIMAL_INTERVAL_LENGTH;
	sprintf(s, "{\\pt14\\color{yellow}current interval length is %d}\n"
			"which means that only intervals that long will be used to search indices\n"
            "\n\n"
            "enter new value : %%6d", n);
	if (win_scanf(s, &n)!=CANCEL) FIND_MINIMAL_INTERVAL_LENGTH=n;
	
	return(D_O_K);
} /* end of function 'do_plot_psd_from_ds' */




int find_search_domain(float *x, int nx, int plateau_position, float *y_min, float *y_max)
{   int i;
static int	 relative_or_absolute=0;
static float epsilon=5e-14;
	float 	amp;
	
	i=win_scanf("There is a criterium to find indices.\n\n"
				"%R with absolute precision\n"
				"%r with relative precision\n"
				" \\epsilon = %8f", 
				&relative_or_absolute, &epsilon);
	if (i==CANCEL) return(CANCEL);
	
	y_min[0] = y_max[0] = x[0];
					
	for (i=1; i<nx; i++)
	{	if (x[i]<y_min[0]) y_min[0]=x[i];
		if (x[i]>y_max[0]) y_max[0]=x[i];
	}
	amp = y_max[0]-y_min[0];
	
	if (plateau_position==0) // minimum
	{	y_max[0] = y_min[0];
	}
	if (plateau_position==1) // maximum
	{ 	y_min[0] = y_max[0];
	}
	
	if (relative_or_absolute==0) // absolute
	{	y_min[0] -= epsilon;
	  	y_max[0] += epsilon;
    }
    if (relative_or_absolute==1) // relative
	{	y_min[0] -= (amp*epsilon);
	  	y_max[0] += (amp*epsilon);
    }
    
	return(D_O_K);	
}






int ask_for_parameters_to_search_for_indices(int *bool_search_indices, int *point_position, int *plateau_position, 
							int *bool_remove_bounds, int *n_last, 
							int *bool_shift_indices, int *n_shift, 
							int *bool_add_more_indices, int *n_more,
							int *N_shift_auto)
{	int		i;

	i=win_scanf("{\\pt14\\color{yellow} Indices and cycles...}\n"
			"%b search for indices (if not, you will be ask how to impose them)\n"
			"How to find the points t_i where to start from ?\n"
			"%R first points\n"
			"%r middle points\n"
			"%r last points\n"
			"of\n"
			"%R lower plateau\n"
			"%r upper plateau\n\n"
			"%b remove first and last %4d indices\n"
			"%b shift indices by %5d\n (to account for relaxation time)\n"
			"%b add manually %5d more indices\n (total duration of ramp)", 
			bool_search_indices,
			point_position, plateau_position, bool_remove_bounds, n_last, 
			bool_shift_indices, n_shift, bool_add_more_indices, n_more);
	if (i==CANCEL) return(OFF);
			
	if (bool_search_indices[0]==0)
	{
		i=win_scanf("{\\pt14\\color{yellow} Imposing indices...}\n"
			"Instead of searching for indices, I will impose them.\n\n"
			"Skip %d between two consecutive averaging windows\n"
			"(note: if files are big, and integration times \\tau are large, I recommend\n"
			"to use set the number above to a significant fraction of \\tau to avoid \n"
			"diverging computation times)",
			N_shift_auto);
	}
	else
	{	if (n_more[0]<1) return(win_printf_OK("Adding manually more indices\n %d should >=1", n_more[0]));
	}
		
	return(0);
} /* end of function 'ask_for_parameters_to_search_for_indices' */





int *impose_indices(float dt, float cycle_period, float start_time, int N, int *n_ind)
{	int	*ind, first_ind, i;

	if ((start_time>dt*(N-1)) || (start_time<0) )
	{	win_printf_OK("imposing indices: starting time is too large...");
		return(NULL);
	}
	if (cycle_period<=dt) 
	{	win_printf_OK("imposing indices: cycle period is too small!");
		return(NULL);
	}
	first_ind = (int)(start_time/dt);
	*n_ind=(int)(((float)dt*(N-1)-start_time)/cycle_period); 
	ind=(int*)calloc(*n_ind, sizeof(int));
	for (i=0; i<*n_ind; i++) 
	{	ind[i]=first_ind + (int)(cycle_period*i/dt);
	}
	return(ind);
}






/* when index is an array of int, this function reduces index in such a way 	*/
/* that only indexes of a serie of M>N continuous points is kept		*/
int *find_and_keep_long_intervals(int *index, int n_in, int *n_out, int N)
{	int *index_tmp=NULL, *index_out;
	int	m,p;
	register int i, j;

	index_tmp=(int*)calloc(n_in, sizeof(int));
	index_tmp[0]=index[0];
	m=1; // count the number of points in the current compact
	p=0; // count the number of points in the output set of indices
	for (i=1; i<n_in; i++)
	{	if (index[i]==(index[i-1]+1)) // contiguous
		{	m++;
		}
		else 
        {   if (m>=N) // then we keep the previous interval
            {  for (j=0; j<m; j++)
                  index_tmp[p] = index[i-m+j];
                  p++;
            }
            m=1;
        }
	}
	
	index_out = (int*)calloc(m, sizeof(int));
	memcpy(index_out, index_tmp, m*sizeof(int));
	if (index_tmp!=NULL)  free(index_tmp);

	*n_out=m;
	return(index_out);
}



int	find_main(int argc, char **argv)
{
	return D_O_K;
}


int	find_unload(int argc, char **argv)
{ 
	return D_O_K;
}
#endif

