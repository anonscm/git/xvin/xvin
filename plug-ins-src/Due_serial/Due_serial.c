/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _DUE_SERIAL_C_
#define _DUE_SERIAL_C_

#include <allegro.h>
#ifdef XV_WIN32
# include "winalleg.h"
#endif

# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "Due_serial.h"




unsigned long t_rs = 0, dt_rs;

UINT timeout = 200;  // ms

char last_answer_2[128];

int im_ci(void);


HANDLE init_serial_port(char *port_str, int baudrate, int n_bits, int parity, int stops, int RTSCTS)
{
    int k, pb = 0;
    HANDLE hCom_port;
    DWORD dwErrors;
    COMSTAT comStat;


    //win_printf("entering init ");
    t_rs = my_uclock();
    dt_rs = get_my_uclocks_per_sec() / 1000;

    strcpy(str_com, port_str);
    //sprintf( str_com, "\\\\.\\COM%d\0", port_number);
    hCom_port = CreateFile(str_com, GENERIC_READ | GENERIC_WRITE, 0, NULL,
                           OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);
    PurgeComm(hCom_port, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

    if (GetCommState(hCom_port, &lpCC.dcb) == 0)
    {
        win_printf("GetCommState FAILED\n port %s baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
                   , port_str, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }

    /* Initialisation des parametres par defaut */
    /*http://msdn.microsoft.com/library/default.asp?url=/library/en-us/devio/base/dcb_str.asp*/
    //win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.BaudRate = baudrate;//CBR_57600;//CBR_115200;
    //lpCC.dcb.BaudRate = CBR_115200;
    //win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.ByteSize = n_bits; //8;
    lpCC.dcb.StopBits = (stops == 1) ? ONESTOPBIT : TWOSTOPBITS;//stops;//ONESTOPBIT;
    lpCC.dcb.Parity = parity;//NOPARITY;
    lpCC.dcb.fOutX = FALSE;
    lpCC.dcb.fInX = FALSE;


    if (RTSCTS == 0)
    {
        lpCC.dcb.fDtrControl = DTR_CONTROL_DISABLE;//DTR_CONTROL_HANDSHAKE or DTR_CONTROL_DISABLE;
        lpCC.dcb.fRtsControl = RTS_CONTROL_DISABLE;//RTS_CONTROL_HANDSHAKE
    }
    else
    {
        lpCC.dcb.fDtrControl = DTR_CONTROL_HANDSHAKE;// or DTR_CONTROL_DISABLE;
        lpCC.dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
        lpCC.dcb.fOutxCtsFlow = TRUE;
    }


    if (SetCommState(hCom_port, &lpCC.dcb) == 0)
    {
        win_printf("SetCommState FAILED\n port %s baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
                   , port_str, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }

    //Delay(60);

    if (GetCommTimeouts(hCom_port, &lpTo) == 0)
    {
        win_printf("GetCommTimeouts FAILED\n port %s baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
                   , port_str, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }

    lpTo.ReadIntervalTimeout = timeout;   // 100 ms max entre characters
    lpTo.ReadTotalTimeoutMultiplier = 1;
    lpTo.ReadTotalTimeoutConstant = 1;
    lpTo.WriteTotalTimeoutMultiplier = 1;
    lpTo.WriteTotalTimeoutConstant = 1;

    if (SetCommTimeouts(hCom_port, &lpTo) == 0)
    {
        win_printf("SetCommTimeouts FAILED\n port %s baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
                   , port_str, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }

    //    we check that the rs232 port is ready for writing

    if (SetupComm(hCom_port, 2048, 2048) == 0)
    {
        win_printf("Init Serial port %s FAILED", port_str);
        CloseSerialPort(hCom_port);
        hCom_port = NULL;
        return NULL;
    }

    k = ClearCommError(hCom, &dwErrors, &comStat);

    if (k && comStat.fCtsHold)  pb = 1;//win_printf(" Tx waiting for CTS signal");

    if (k && comStat.fDsrHold)  pb = 2;

    if (k && comStat.fDsrHold)  pb = 3;

    if (k && comStat.fRlsdHold) pb = 4;

    if (k && comStat.fXoffHold) pb = 5;

    if (k && comStat.fXoffSent) pb = 6;

    if (k && comStat.fEof)      pb = 7;

    if (k && comStat.fTxim)     pb = 8;

    if (k && comStat.cbInQue)   pb = 9;

    if (k && comStat.cbOutQue)  pb = 10;

    if (pb != 0)
    {
        win_printf("Init Serial port %d went Ok but the port in not ready for Writting\n"
                   "Check if the rs232 cable is correctly connected pb %s", port_str, pb);
    }
    my_set_window_title("Init Serial port %s OK", port_str);
    return hCom_port;
}


int Write_serial_port(HANDLE handle, char *data, DWORD length)
{
    int success = 0, k, pb = 0;
    DWORD dwWritten = 0, dwWritten2 = 0;
    OVERLAPPED o = {0};
    DWORD dwErrors;
    COMSTAT comStat;


    k = ClearCommError(hCom, &dwErrors, &comStat);

    if (k && comStat.fCtsHold)  pb = 1;//win_printf(" Tx waiting for CTS signal");

    if (k && comStat.fDsrHold)  pb = 2;

    if (k && comStat.fDsrHold)  pb = 3;

    if (k && comStat.fRlsdHold) pb = 4;

    if (k && comStat.fXoffHold) pb = 5;

    if (k && comStat.fXoffSent) pb = 6;

    if (k && comStat.fEof)      pb = 7;

    if (k && comStat.fTxim)     pb = 8;

    if (k && comStat.cbInQue)   pb = 9;

    if (k && comStat.cbOutQue)  pb = 10;

# ifdef JF_DEBUG

    if (pb) win_printf("Error writting %d", pb);

# endif


    /*
    if (k && comStat.fCtsHold)  pb = 1;//win_printf(" Tx waiting for CTS signal");
    if (k && comStat.fDsrHold)  win_printf(" Tx waiting for DSR signal");
    if (k && comStat.fRlsdHold) win_printf(" Tx waiting for RLSD signal");
    if (k && comStat.fXoffHold) win_printf(" Tx waiting, XOFF char rec'd");
    if (k && comStat.fXoffSent) win_printf(" Tx waiting, XOFF char sent");
    if (k && comStat.fEof)      win_printf(" EOF character received");
    if (k && comStat.fTxim)     win_printf(" Character waiting for Tx; char queued with TransmitCommChar");
    if (k && comStat.cbInQue)   win_printf(" comStat.cbInQue bytes have been received, but not read");
    if (k && comStat.cbOutQue)  win_printf(" comStat.cbOutQue bytes are awaiting transfer");
    */


    o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

    if (!WriteFile(handle, (LPCVOID)data, length, &dwWritten, &o))
    {
        if (GetLastError() == ERROR_IO_PENDING)
            if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
                if (GetOverlappedResult(handle, &o, &dwWritten2, FALSE))
                    success = 1;
    }
    else    success = 1;

    if (dwWritten != length)     success = 0;

    if (dwWritten2 == length)     success = 1;

    CloseHandle(o.hEvent);

    return (success == 0) ? -1 : ((dwWritten < dwWritten2) ? dwWritten2 : dwWritten);
}


int ReadData(HANDLE handle, BYTE *data, DWORD length, DWORD *dwRead)
{
    int success = 0;
    OVERLAPPED o = {0};

    o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

    if (!ReadFile(handle, data, length, dwRead, &o))
    {
        if (GetLastError() == ERROR_IO_PENDING)
            if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
                success = 1;

        GetOverlappedResult(handle, &o, dwRead, FALSE);
    }
    else    success = 1;

    CloseHandle(o.hEvent);
    return success;
}

int Read_serial_port(HANDLE handle, char *stuff, DWORD max_size, DWORD *dwRead)
{
    register int i, j, k;
    unsigned long t0;
    //  int len;
    BYTE lch[2];//, chtmp[128];
    int ltimeout = 0;
    DWORD dwErrors;
    COMSTAT comStat;

    if (handle == NULL) return -2;


    t0 = get_my_uclocks_per_sec() / 20; // we cannot wait more than 50 ms
    t0 += my_uclock();

    for (i = j = 0; j == 0 && i < max_size - 1 && ltimeout == 0;)
    {
        *dwRead = 0;

        if (ReadData(handle, lch, 1, dwRead) == 0)
        {
            stuff[i] = 0;
            *dwRead = i;
            k = ClearCommError(hCom, &dwErrors, &comStat);

            //if (k && comStat.cbInQue)   Read_serial_port(hCom,chtmp,127,&len);
            //if (j && comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
            //if (j && comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
            //if (j && comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
            //if (j && comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
            //if (j && comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
            //if (j && comStat.fEof)      my_set_window_title(" EOF character received");
            if (k && comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");

            //if (j && comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
            if (k && comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");



            //return -1;
        }
        if (*dwRead == 1)
        {
            stuff[i] = lch[0];
            j = (lch[0] == '\n') ? 1 : 0;
            i++;
        }

        ltimeout = (t0 > my_uclock()) ? 0 : 1;
    }

    stuff[i] = 0;
    *dwRead = i;
    return (ltimeout) ? -2 : i;
}




int sent_cmd_and_read_answer(HANDLE lhCom, char *Command, char *answer, DWORD n_answer, DWORD *n_written,
                             DWORD *dwErrors, unsigned long *t0)
{
    int rets, ret, i, j;
    COMSTAT comStat;
    char l_command[128], chtmp[128]; // , ch[2]
    DWORD len = 0;

    if (lhCom == NULL || Command == NULL || answer == NULL || strlen(Command) < 1) return 1;

    for (i = 0, j = 1; i < 5 && j != 0; i++)
    {
        j = ClearCommError(lhCom, dwErrors, &comStat);

        if (j && comStat.cbInQue)   Read_serial_port(lhCom, chtmp, 127, &len);

        //if (j && comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
        //if (j && comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
        //if (j && comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
        //if (j && comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
        //if (j && comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
        //if (j && comStat.fEof)      my_set_window_title(" EOF character received");
        if (j && comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");

        //if (j && comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
        if (j && comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
    }


    for (i = 0; i < 126 && Command[i] != 0 && Command[i] != 13; i++)
        l_command[i] = Command[i]; // we copy until CR

    l_command[i++] = 13;         // we add it
    l_command[i] = 0;            // we end string

    if (t0) *t0 = my_uclock();

    /*
    for (i = 0; i < 126 && l_command[i] != 0; i++)
      {
        ch[0] = l_command[i];
        ch[1] = 0;
        rets = Write_serial_port(hCom,ch,1);
        if (rets <= 0)
    {
      if (t0) *t0 = my_uclock() - *t0;
      // Get and clear current errors on the port.
      if (ClearCommError(hCom, dwErrors, &comStat))
        {
          if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
          return -2;
        }
      return -1;
    }
      }
    */

    rets = Write_serial_port(lhCom, l_command, strlen(l_command));

    if (rets <= 0)
    {
        if (t0) *t0 = my_uclock() - *t0;

        // Get and clear current errors on the port.
        if (ClearCommError(lhCom, dwErrors, &comStat))
        {
            if (comStat.cbInQue)  Read_serial_port(lhCom, chtmp, 127, &len);

            //if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
            //if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
            //if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
            //if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
            //if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
            //if (comStat.fEof)      my_set_window_title(" EOF character received");
            if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");

            //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
            if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");

            return -2;
        }

        return -1;
    }

    *n_written = 0;
    ret = Read_serial_port(lhCom, answer, n_answer, n_written);

    for (; ret == 0;)
        ret = Read_serial_port(lhCom, answer, n_answer, n_written);

    if (t0) *t0 = my_uclock() - *t0;

    answer[*n_written] = 0;
    strncpy(last_answer_2, answer, 127);

    if (ret < 0)
    {
        // Get and clear current errors on the port.
        if (ClearCommError(lhCom, dwErrors, &comStat))
        {
            if (comStat.cbInQue)  Read_serial_port(lhCom, chtmp, 127, &len);

            //if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
            //if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
            //if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
            //if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
            //if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
            //if (comStat.fEof)      my_set_window_title(" EOF character received");
            if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");

            //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
            if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");

            return 2;
        }

        return 1;
    }

    return 0;
}


int talk_serial_port(HANDLE hCom_port, char *command, int wait_answer, char *answer, DWORD NumberOfBytesToWrite,
                     DWORD nNumberOfBytesToRead)
{
    //int NumberOfBytesWrittenOnPort = -1;
    unsigned long NumberOfBytesWritten = -1;


    if (hCom_port == NULL) return win_printf_OK("No serial port init");

    if (wait_answer != READ_ONLY)
    {
        //NumberOfBytesWrittenOnPort =
        Write_serial_port(hCom_port, command, NumberOfBytesToWrite);
    }

    if (wait_answer != WRITE_ONLY)
    {
        Read_serial_port(hCom_port, answer, nNumberOfBytesToRead, &NumberOfBytesWritten);

    }

    return 0;
}

int n_write_read_serial_port(void)
{
    static char Command[128] = "dac16?";
    static int ntimes = 1;
    unsigned long t0;
    double dt, dtm, dtmax;
    char resu[128], *ch;
    int ret = 0, i;
    unsigned long lpNumberOfBytesWritten = 0;
    DWORD nNumberOfBytesToRead = 127;

    if (updating_menu_state != 0)    return D_O_K;

    if (hCom == NULL) return win_printf_OK("No serial port init");

    for (ch = Command; *ch != 0; ch++)
        if (*ch == '\r') *ch = 0;

    win_scanf("Command to send? %snumber of times %d", &Command, &ntimes);

    strncat(Command, "\r", (sizeof(Command) > strlen(Command)) ? sizeof(Command) - strlen(Command) : 0);

    for (i = 0, dtm = dtmax = 0; i < ntimes; i++)
    {
        t0 = my_uclock();
        Write_serial_port(hCom, Command, strlen(Command));

        ret = Read_serial_port(hCom, resu, nNumberOfBytesToRead, &lpNumberOfBytesWritten);
        t0 = my_uclock() - t0;
        dt = 1000 * (double)(t0) / get_my_uclocks_per_sec();
        resu[lpNumberOfBytesWritten] = 0;
        dtm += dt;

        if (dt > dtmax) dtmax = dt;
    }

    dtm /= ntimes;
    win_printf("command read = %s \n ret %d lpNumberOfBytesWritten = %d\nin %g mS in avg %g max", resu, ret,
               lpNumberOfBytesWritten, dtm, dtmax);
    return D_O_K;
}
int read_Due_param(char *cmd, int *val)
{
  char cmd2[128], cmdc[128];
    char resu[128];
    int ret = 0, i;
    unsigned long lpNumberOfBytesWritten = 0;
    DWORD nNumberOfBytesToRead = 127;

 
    if (hCom == NULL) return win_printf_OK("No serial port init");

    for (i = 0; cmd[i] != 0; i++)
      {
	cmdc[i] = cmd[i];
        if (cmd[i] == '?' || cmd[i] == '=' || cmd[i] == '\n' || cmd[i] == '\r') 
	  {
	    cmdc[i] = 0;
	    break;
	  }
      }
    cmdc[i+1] = 0;
    snprintf(cmd2,sizeof(cmd2),"%s?\r",cmdc);
    ret = Read_serial_port(hCom, resu, nNumberOfBytesToRead, &lpNumberOfBytesWritten);
    resu[lpNumberOfBytesWritten] = 0;
    Write_serial_port(hCom, cmd2, strlen(cmd2));
    Sleep(50);
    ret = Read_serial_port(hCom, resu, nNumberOfBytesToRead, &lpNumberOfBytesWritten);
    resu[lpNumberOfBytesWritten] = 0;
    snprintf(cmd2,sizeof(cmd2),"%s=%%d",cmdc);
    if (sscanf(resu,cmd2,val) != 1)       
      {
	win_printf_OK("pb reading value:in\n%s\nseed:%s",resu,cmd2);
	return 1;
      }
    return 0;
}

int set_Due_param(char *cmd, int val, int *val_set)
{
  char cmd2[128], cmdc[128];;
    char resu[128];
    int i;
    unsigned long lpNumberOfBytesWritten = 0;
    DWORD nNumberOfBytesToRead = 127;

 
    if (hCom == NULL) return win_printf_OK("No serial port init");

    for (i = 0; cmd[i] != 0; i++)
      {
	cmdc[i] = cmd[i];
        if (cmd[i] == '?' || cmd[i] == '=' || cmd[i] == '\n' || cmd[i] == '\r') 
	  {
	    cmdc[i] = 0;
	    break;
	  }
      }
    cmdc[i+1] = 0;
    snprintf(cmd2,sizeof(cmd2),"%s=%d\r",cmdc,val);
    Read_serial_port(hCom, resu, nNumberOfBytesToRead, &lpNumberOfBytesWritten);
    resu[lpNumberOfBytesWritten] = 0;
    Write_serial_port(hCom, cmd2, strlen(cmd2));
    Sleep(50);
    Read_serial_port(hCom, resu, nNumberOfBytesToRead, &lpNumberOfBytesWritten);
    resu[lpNumberOfBytesWritten] = 0;
    snprintf(cmd2,sizeof(cmd2),"%s=%%d",cmdc);
    if (sscanf(resu,cmd2,val_set) != 1)       
      {
	win_printf_OK("pb reading value:in\n%s\nseed:%s",resu,cmd2);
	return 1;
      }
    return 0;
}

int do_set_Due_param(void)
{
    static char Command[128] = "M0=";
    int  i;
    static int val = 0;
    int tmp = -1;

    if (updating_menu_state != 0)    return D_O_K;

    if (hCom == NULL) return win_printf_OK("No serial port init");

    i = win_scanf("Command %12s\n val %5d\n",Command,&val);
    if (i == WIN_CANCEL) return 0;

    if (set_Due_param(Command, val, &tmp))
            win_printf_OK("Could not exec %s",Command);
    if (tmp != val) win_printf_OK("set %d and set %d differ",val,tmp);
    return 0;
}

int do_read_Due_param(void)
{
    static char Command[128] = "M0?";
    int i;
    static int val = 0;

    if (updating_menu_state != 0)    return D_O_K;

    if (hCom == NULL) return win_printf_OK("No serial port init");

    i = win_scanf("Command %12s",Command);
    if (i == WIN_CANCEL) return 0;

    if (read_Due_param(Command, &val))
            win_printf_OK("Could not exec %s",Command);
    win_printf_OK("%s -> val %d",Command,val);
    return 0;
}
int start_modulation(void)
{
  int val = -1;
    if (updating_menu_state != 0)    return D_O_K;
    if (read_Due_param("S=", &val))
            win_printf_OK("Could not exec S=");
    win_printf_OK("Modulation started at %d",val);
    return 0;
}

char *Get_Due_cycle(void)
{
    int M0 = 0, M1 = 0, A0 = 0, A1 = 0, N0 = 0, N1 = 0, O0 = 0;
    int O1 = 0, B0 =0, B1 = 0, P = 0; 
    char stuff[2048];


    if (hCom == NULL) return win_printf_ptr("No serial port init");
    read_Due_param("M0?", &M0);
    read_Due_param("M1?", &M1);
    read_Due_param("N0?", &N0);
    read_Due_param("N1?", &N1);
    read_Due_param("O0?", &O0);
    read_Due_param("O1?", &O1);
    read_Due_param("A0?", &A0);
    read_Due_param("A1?", &A1);
    read_Due_param("B0?", &B0);
    read_Due_param("B1?", &B1);
    read_Due_param("P?", &P);

    snprintf(stuff,sizeof(stuff),"Led Cycle: %5d camera frames for one period of modulation\n"
	     "DAC constant signal LED0 %d ; LED1 %d\n"
	     "number of modulation periods %d\n"
	     "DAC modulation amplitude LED0 %d ; LED1 %d\n"
	     "number of periods %d of signal\n"
	     "DAC stand by signal LED0 %d ; LED1 %d\n",P,M0,M1,O0,A0,A1,N0,B0,B1);
    return strdup(stuff);

}
int Set_Due_cycle(void)
{
    int  i;
    int M0 = 0, M1 = 0, A0 = 0, A1 = 0, N0 = 0, N1 = 0, O0 = 0;
    int O1 = 0, B0 =0, B1 = 0, P = 0, tmp; 
    int M0v, M1v, A0v, A1v, N0v, N1v, O0v, O1v, B0v, B1v, Pv; 

    if (updating_menu_state != 0)    return D_O_K;

    if (hCom == NULL) return win_printf_OK("No serial port init");
    read_Due_param("M0?", &M0v);
    read_Due_param("M1?", &M1v);
    read_Due_param("N0?", &N0v);
    read_Due_param("N1?", &N1v);
    read_Due_param("O0?", &O0v);
    read_Due_param("O1?", &O1v);
    read_Due_param("A0?", &A0v);
    read_Due_param("A1?", &A1v);
    read_Due_param("B0?", &B0v);
    read_Due_param("B1?", &B1v);
    read_Due_param("P?", &Pv);
    /*
    M0v = M0; M1v = M1; A0v = A0; A1v = A1; N0v = N0; N1v = N1; 
    O0v = O0; O1v = O1; B0v = B0; B1v = B1; Pv = P; 
    */
    P = get_config_int("DUE-fluo-modulation", "Cam-Period", 20);
    M0 = get_config_int("DUE-fluo-modulation", "LE0-CST", 2000);
    M1 = get_config_int("DUE-fluo-modulation", "LE1-CST", 1200);
    O0 = get_config_int("DUE-fluo-modulation", "n-CST", 2);
    A0 = get_config_int("DUE-fluo-modulation", "LE0-Amp", 1999);
    A1 = get_config_int("DUE-fluo-modulation", "LE0-Amp", -1199);
    N0 = get_config_int("DUE-fluo-modulation", "n-Mod", 8);
    B0 = get_config_int("DUE-fluo-modulation", "LE0-STDBY", 0);
    B1 = get_config_int("DUE-fluo-modulation", "LE0-STDBY", 0);
    char message[4096] = {0};

    snprintf(message,sizeof(message),"Led Cycle definition:\n"
	      "Choose the number %%5d (%d) of camera frames defining one period of modulation\n"
	      "Define the DAC constant signal before modulation for :\n"
	      "LED0 %%5d (%d); LED1 %%5d (%d) in [0,4095]\n"
	      "Define the number of modulation periods %%4d (%d) for this constant signal\n"
	      "Define the DAC modulation amplitude for :\n"
	      "LED0 %%5d (%d); LED1 %%5d  '%d)(can be < 0 but abs value < constant signal)\n"
	      "Define the number of periods %%4d (%d) for the modulation signal\n"
	      "Define the DAC stand by signal after modulation for :\n"
             "LED0 %%5d (%d); LED1 %%5d (%d)in [O, 4095]\n",Pv,M0v,M1v,O0v,A0v,A1v,N0v,B0v,B1v);
      
    i = win_scanf(message,&P,&M0,&M1,&O0,&A0,&A1,&N0,&B0,&B1);

    if (i == WIN_CANCEL) return 0;

    set_config_int("DUE-fluo-modulation", "Cam-Period", P);
    set_config_int("DUE-fluo-modulation", "LE0-CST", M0);
    set_config_int("DUE-fluo-modulation", "LE1-CST", M1);
    set_config_int("DUE-fluo-modulation", "n-CST", O0);
    set_config_int("DUE-fluo-modulation", "LE0-Amp", A0);
    set_config_int("DUE-fluo-modulation", "LE0-Amp", A1);
    set_config_int("DUE-fluo-modulation", "n-Mod", N0);
    set_config_int("DUE-fluo-modulation", "LE0-STDBY", B0);
    set_config_int("DUE-fluo-modulation", "LE0-STDBY", B1);


    
    N1 = N0;
    O1 = O0;

    if (M0 != M0v)
      {
	set_Due_param("M0=", M0, &tmp);
	if (tmp != M0) win_printf_OK("read M0 error %d",tmp);
      }
    if (M1 != M1v)
      {
	set_Due_param("M1=", M1, &tmp);
	if (tmp != M1) win_printf_OK("read M1 error %d",tmp);
      }

    if (O0 != O0v)
      {
	set_Due_param("O0=", O0, &tmp);
	if (tmp != O0) win_printf_OK("read O0 error %d",tmp);
      }
    if (O1 != O1v)
      {
	set_Due_param("O1=", O1, &tmp);
	if (tmp != O1) win_printf_OK("read O1 error %d",tmp);
      }

    if (N0 != N0v)
      {
	set_Due_param("N0=", N0, &tmp);
	if (tmp != N0) win_printf_OK("read N0 error %d",tmp);
      }
    if (N1 != N1v)
      {
	set_Due_param("N1=", N1, &tmp);
	if (tmp != N1) win_printf_OK("read N1 error %d",tmp);
      }

    if (A0 != A0v)
      {
	set_Due_param("A0=", A0, &tmp);
	if (tmp != A0) win_printf_OK("read A0 error %d",tmp);
      }
    if (A1 != A1v)
      {
	set_Due_param("A1=", A1, &tmp);
	if (tmp != A1) win_printf_OK("read A1 error %d",tmp);
      }

    if (B0 != B0v)
      {
	set_Due_param("B0=", B0, &tmp);
	if (tmp != B0) win_printf_OK("read B0 error %d",tmp);
      }
    if (B1 != B1v)
      {
	set_Due_param("B1=", B1, &tmp);
	if (tmp != B1) win_printf_OK("read B1 error %d",tmp);
      }

    if (P != Pv)
      {
	set_Due_param("P=", P, &tmp);
	if (tmp != P) win_printf_OK("read P error %d",tmp);
      }

    return D_O_K;
}




int purge_com(void)
{
    if (hCom == NULL) return 1;

    return PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
}
int init_due_serial(void)
{
    int i, j;
    static int port_number = 14, sbaud = 0, hand = 0, parity = 0, stops = 1, n_bits = 8;
    char command[128], answer[128], *gr;
    int  ret, iret;
    unsigned long len = 0;
    static int first_init = 1;
    char port_str[256] = {0};

# ifdef JF_DEBUG
    win_printf("Entering init_due_serial");
# endif

    if (hCom != NULL) return 0;

    if (first_init == 0) return 1;

    first_init = 0;
    // we only get here once
    answer[0] = 0;

    sbaud = 1;

    for (; hCom == NULL;)
      {
	i = win_scanf("{\\color{yellow}Your Arduino serial interface did not open properly on last trial!} Check:\n"
		      " - that another program is not using the same serial port\n"
		      " - Another instance of a program may still be running\n"
		      "    If this is the case open the Windows task manager and kill the process\n"
		      " - Last but not least your configuration may not be correct\n"
		      "If this is the case select a new one. {\\color{yellow}Port number:} ?%5d\n"
		      "{\\color{yellow}Baudrate:} 57.6k %R 115.2k %r 230.4k %r 460.8k %r 921.6k %r (default 57.6k)\n"
		      "{\\color{yellow}Number of bits:} %4d  (default 8)\n"
		      "{\\color{yellow}Parity:} No %R even %r odd %r (default No)\n"
		      "{\\color{yellow}Number of stop bits:} 0->%R 1->%r 2->%r (default 1)\n"
		      "{\\color{yellow}HandShaking:} None %R Cts/Rts %r (default Cts/Rts)\n"
		      "{\\color{lightred}If you hit cancel a dummy interface will be used\n"
		      "This only allows you to check your camera}\n"
		      , &port_number, &sbaud, &n_bits, &parity, &stops, &hand);

	if (i == WIN_CANCEL)
	  {
	    return 0;
	  }
	
	sprintf(port_str, "\\\\.\\COM%d\0", port_number);
	hCom = init_serial_port(port_str, (sbaud + 1) * 57600, n_bits, parity , stops, hand);
      }


    Sleep(50);

    for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
        ret = Read_serial_port(hCom, answer, 127, &len);

    sprintf(command, "\r");
    j = Write_serial_port(hCom, command, strlen(command));
    Sleep(50);
    
    for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
      ret = Read_serial_port(hCom, answer, 127, &len);
    
    answer[len] = 0;
    sprintf(command, "E0\r");
	//TV tmp : to debug pico
	//sprintf(command, "zmagi?");
    j = Write_serial_port(hCom, command, strlen(command));
	//win_printf_OK("write %d",j);
    Sleep(250);
    
    for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
      ret = Read_serial_port(hCom, answer, 127, &len);

	//win_printf_OK("answer %s", answer);

    answer[len] = 0;
    gr = strstr(answer, "Echo set OFF");

    if (gr == NULL)
    {
      for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
	ret = Read_serial_port(hCom, answer, 127, &len);
	//win_printf_OK("answer %s", answer);
	
      gr = strstr(answer, "Echo set OFF");
      if (gr == NULL)
        win_printf("You have a problem while reading the reply to the first\n"
                   "command 'E0' your device replied %d characters:\n>%s<\n"
                   "We shall try again initializing arduino device\n", len, answer);
    }
    //add_plot_treat_menu_item("pico_serial", NULL, pico_serial_plot_menu(), 0, NULL);
    return (gr != NULL) ? 0 : 1;
}

int init_port(void)
{
    if (updating_menu_state != 0)    return D_O_K;


    init_due_serial();
    return D_O_K;
}



int write_command_on_serial_port(void)
{
    static char Command[128] = "test";
    //char *test="\r";

    if (updating_menu_state != 0)    return D_O_K;

    win_scanf("Command to send? %s", &Command);
    strncat(Command, "\r", (sizeof(Command) > strlen(Command)) ? sizeof(Command) - strlen(Command) : 0);

    //win_printf("deja cela %d\n Command %s",strlen(Command), Command);
    Write_serial_port(hCom, Command, strlen(Command));
    //snprintf(Command,128,"?nm\r");
    //strcat(Command,'\r');
    //win_printf("deja cela %d\n Command %s",strlen(Command), Command);

    //win_printf("%s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
    return D_O_K;
}


int read_on_serial_port(void)
{
    char Command[128];
    unsigned long lpNumberOfBytesWritten = 0;
    int ret = 0;
    DWORD nNumberOfBytesToRead = 127;
    unsigned long t0;

    if (updating_menu_state != 0)    return D_O_K;

    t0 = my_uclock();
    //win_scanf("Command to send? %s",Command);
    ret = Read_serial_port(hCom, Command, nNumberOfBytesToRead, &lpNumberOfBytesWritten);
    t0 = my_uclock() - t0;
    Command[lpNumberOfBytesWritten] = 0;

    win_printf("command read = %s \n ret %d lpNumberOfBytesWritten = %d\n in %g mS", Command, ret, lpNumberOfBytesWritten,
               1000 * (double)(t0) / get_my_uclocks_per_sec());

    //win_printf("command read = %s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
    return D_O_K;
}



int write_read_serial_port(void)
{
    if (updating_menu_state != 0)    return D_O_K;

    write_command_on_serial_port();
    read_on_serial_port();
    return D_O_K;
}




int CloseSerialPort(HANDLE lhCom)
{
    if (CloseHandle(lhCom) == 0) return win_printf("Close Handle FAILED!");

    return D_O_K;
}

int close_serial_port(void)
{
    if (updating_menu_state != 0)    return D_O_K;

    CloseSerialPort(hCom);
    hCom = NULL;
    return D_O_K;
}




MENU *due_serial_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn, "Init serial port", init_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Write serial port", write_command_on_serial_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Read serial port", read_on_serial_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Write read serial port", write_read_serial_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Write read n times", n_write_read_serial_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Close serial port", close_serial_port, NULL, 0, NULL);
    add_item_to_menu(mn, "Define modulation", Set_Due_cycle, NULL, 0, NULL);
    add_item_to_menu(mn, "Read param",do_read_Due_param , NULL, 0, NULL);
    add_item_to_menu(mn, "Set param", do_set_Due_param, NULL, 0, NULL);
    add_item_to_menu(mn, "Start modulation", start_modulation, NULL, 0, NULL);



    //  add_item_to_menu(mn,"Read Objective position", due_read_Z,NULL,0,NULL);
    //add_item_to_menu(mn,"Set Objective position", due_set_Z,NULL,0,NULL);

    return mn;
}

int Due_serial_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;

    init_port();



    add_plot_treat_menu_item("due_serial", NULL, due_serial_plot_menu(), 0, NULL);
    add_image_treat_menu_item("due_serial", NULL, due_serial_plot_menu(), 0, NULL);
    if (hCom != NULL) return D_O_K;

    return D_O_K;
}

int Due_serial_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
    remove_item_to_menu(plot_treat_menu, "due_serial", NULL, NULL);
    return D_O_K;
}
#endif

