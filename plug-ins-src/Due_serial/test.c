#include <DueTimer.h>

# define TABLESIZE  512

struct _Sig_dac
{
  uint16_t pre_sig;          // the constant value before modulation
  int n_mean;                // the number of camera frame the mean signal is held
  int i_mean;                // the current index
  int mod_amp;          // the modulation amplitude
  int n_sig;                 // the number of period of applied modulation
  int i_per;                 // the index inside the table
  int i_sig;                 // the index of period
  uint16_t std_by;           // the stand by value of the led
  int p_running;               // flag state
  int start_on_next_frame;
  uint16_t table[TABLESIZE]; // the table containing one period of modulation
};

typedef struct _Sig_dac s_dac;

s_dac sdac0, sdac1;

int freq_in_camera_fr = 10;  // the modulation frequency in camera frame
int ficf = 0;
int dac_per = 500;
int echo = 1;


int fill_table(struct _Sig_dac *sdac, uint16_t sig0, int amp)
{
  int i;
  if (sdac == NULL) return -1;
  sdac->pre_sig = sig0;
  sdac->mod_amp = amp;
  for (i = 0; i < TABLESIZE; i++)
    sdac->table[i] = sig0 + (int)(0.5 + amp * sin((2 * M_PI * i) / TABLESIZE));
  return 0;
}


char cmd[256] = {0};         // a string to hold incoming data
int n_cmd = 0;
boolean stringComplete = false;  // whether the string is complete

boolean meanReady = false;
int myLed = 13;
int gpio_cam = 11; // pin to set state via camera
int sstep = 0;
int cstep = 90;
bool ledOn = false;
unsigned long  dt = 0, t0 = 0;
float dtm = 0;
int sensorPin = A0;    // select the input pin for the potentiometer
int sensorValue[TABLESIZE] = {0};  // variable to store the value coming from the sensor

const byte interruptPin = 12;
volatile byte state = LOW;
volatile  unsigned long  idt = 0, camera_per = 0,  it0 = 0, imn = 0, last_im = 0;
unsigned long  camera_per_1 = 0;
volatile uint16_t vdac0, vdac1, lvdac0, lvdac1;
int i_handler = 0, camera_alert = 0;

void myHandler()
{ // Timer 3 routine
  t0 = micros();
  i_handler++;
  if (sdac0.p_running)
  {
    sensorValue[sdac0.i_per] = analogRead(sensorPin);

    if (sdac0.i_mean < sdac0.n_mean)
    {
      vdac0 = sdac0.pre_sig;
      vdac1 = sdac1.pre_sig;
      sdac0.i_per++;
      if (sdac0.i_per >= TABLESIZE)
      {
        sdac0.i_mean++;
        sdac0.i_per = 0;
      }
    }
    else if (sdac0.i_sig < sdac0.n_sig)
    {
      vdac0 = sdac0.table[sdac0.i_per];
      vdac1 = sdac1.table[sdac0.i_per];
      sdac0.i_per++;
      if (sdac0.i_per >= TABLESIZE)
      {
        sdac0.i_sig++;
        sdac0.i_per = 0;
      }
    }
    else
    {
      vdac0 = sdac0.std_by;
      vdac1 = sdac1.std_by;
      sdac0.i_per++;
      if (sdac0.i_per >= TABLESIZE)
      {
        sdac0.i_per = 0;
        sdac0.i_mean = 0;
        sdac0.i_sig = 0;
        sdac0.p_running = 0;
      }
    }
    analogWrite(DAC0, vdac0);
    analogWrite(DAC1, vdac1);
    // Led on, off, on, off...
    dt += micros() - t0;
  }
}


void flash_camera() {
  if (digitalRead(interruptPin) == 0)
  {
    it0 = micros();
    if ((camera_alert > 0) && (i_handler < (freq_in_camera_fr + 10)))
      camera_alert = -1;
    i_handler = 0;
    camera_per = it0 - idt;
    idt = it0;
    imn++;
    if (sdac0.start_on_next_frame)
    {
      sdac0.p_running = 1;
      sdac0.start_on_next_frame = 0;
    }
    if (sdac0.p_running)
    {
      lvdac0 = vdac0;
      lvdac1 = vdac1;
      if (sdac0.i_sig < sdac0.n_sig)
      {
        if (2 * sdac0.i_per < TABLESIZE)  digitalWrite(gpio_cam, true);
        else    digitalWrite(gpio_cam, false);
      }
      else digitalWrite(gpio_cam, false);
    }
  }
}


  void setup() {
    // initialize serial:
    Serial.begin(115200);
    // prepare LED
    pinMode(myLed, OUTPUT);
    // prepare camera gpio_1
    pinMode(gpio_cam, OUTPUT);
    digitalWrite(gpio_cam, false);
    // set analog stuff
    analogWriteResolution(12);
    analogReadResolution(12);
    // define Timer 3 job
    Timer3.attachInterrupt(myHandler);
    // we attach the flash pulse of the camera
    pinMode(interruptPin, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(interruptPin), flash_camera, CHANGE);
    sdac0.pre_sig = sdac1.pre_sig = 2048;
    sdac0.n_mean = sdac1.n_mean = 1;
    sdac0.mod_amp = 2000;
    sdac1.mod_amp = -2000;
    sdac0.n_sig = sdac1.n_sig = 1;
    sdac0.std_by = sdac1.std_by = 200;
    fill_table(&sdac0, sdac0.pre_sig, sdac0.mod_amp);
    fill_table(&sdac1, sdac1.pre_sig, sdac1.mod_amp);
    Timer3.start(dac_per); // Calls every dac_period in micros
  }

  /*
    SerialEvent occurs whenever a new data comes in the
   hardware serial RX.  This routine is run between each
   time loop() runs, so using delay inside loop can delay
   response.  Multiple bytes of data may be available.
   */
  void serialEvent() {
    while (Serial.available()) {
      // get the new byte:
      char inChar = (char)Serial.read();
      // add it to the inputString:
      cmd[n_cmd++] = inChar;
      if (echo) Serial.print(inChar);
      n_cmd = (n_cmd > 254) ? 254 : n_cmd;
      cmd[n_cmd] = 0;
      // if the incoming character is a newline, set a flag
      // so the main loop can do something about it:
      if (inChar == '\n' || inChar == '\r') {
        stringComplete = true;
      }
    }
  }

  void M_command(void)
  {
    int tmp;
    // setting mean value in first phase?
    if (cmd[1] == '0')
    {
      if (cmd[2] == '=')
      {
        if (sscanf(cmd + 3, "%d", &tmp) == 1)
          sdac0.pre_sig = tmp ;
        Serial.print("M0=");
        Serial.print(sdac0.pre_sig);
        Serial.println(" : ADC0 pre signal new value");
        fill_table(&sdac0, sdac0.pre_sig, sdac0.mod_amp);
      }
      if (cmd[2] == '?' || cmd[2] == '\n' || cmd[2] == '\r')
      {
        Serial.print("M0=");
        Serial.print(sdac0.pre_sig);
        Serial.println(" : ADC0 pre signal value");
      }
    }
    else if (cmd[1] == '1')
    {
      if (cmd[2] == '=')
      {
        if (sscanf(cmd + 3, "%d", &tmp) == 1)
          sdac1.pre_sig = tmp;
        Serial.print("M1=");
        Serial.print(sdac1.pre_sig);
        Serial.println(" : ADC1 pre signal new value");
        fill_table(&sdac1, sdac1.pre_sig, sdac1.mod_amp);
      }
      if (cmd[2] == '?' || cmd[2] == '\n' || cmd[2] == '\r')
      {
        Serial.print("M1=");
        Serial.print(sdac1.pre_sig);
        Serial.println(" : ADC1 pre signal value");
      }
    }
  }

  void N_command(void)
  {
    int tmp;
    // setting the number of modulation period
    if (cmd[1] == '0')
    {
      if (cmd[2] == '=')
      {
        if (sscanf(cmd + 3, "%d", &tmp) == 1)
          sdac0.n_sig = tmp;
        Serial.print("N0=");
        Serial.print(sdac0.n_sig);
        Serial.println(" : ADC0 # of period in signal set");
      }
      if (cmd[2] == '?' || cmd[2] == '\n' || cmd[2] == '\r')
      {
        Serial.print("N0=");
        Serial.print(sdac0.n_sig);
        Serial.println(" : ADC0 # of period in signal value");
      }
    }
    else if (cmd[1] == '1')
    {
      if (cmd[2] == '=')
      {
        if (sscanf(cmd + 3, "%d", &tmp) == 1)
          sdac1.n_sig = tmp;
        Serial.print("N1=");
        Serial.print(sdac1.n_sig);
        Serial.println(" : ADC1 # of period in signal set");
      }
      if (cmd[2] == '?' || cmd[2] == '\n' || cmd[2] == '\r')
      {
        Serial.print("N1=");
        Serial.print(sdac1.n_sig);
        Serial.println(" : ADC1 # of period in signal value");
      }
    }
  }
  void A_command(void)
  {
    int tmp;
    // setting the modulation amplitude
    if (cmd[1] == '0')
    {
      if (cmd[2] == '=')
      {
        if (sscanf(cmd + 3, "%d", &tmp) == 1)
          sdac0.mod_amp = tmp;
        Serial.print("A0=");
        Serial.print(sdac0.mod_amp);
        Serial.println(" : ADC0 amplitude of modulation in signal set");
        fill_table(&sdac0, sdac0.pre_sig, sdac0.mod_amp);
      }
      if (cmd[2] == '?' || cmd[2] == '\n' || cmd[2] == '\r')
      {
        Serial.print("A0=");
        Serial.print(sdac0.mod_amp);
        Serial.println(" : ADC0 amplitude of modulation in signal value");
      }
    }
    else if (cmd[1] == '1')
    {
      if (cmd[2] == '=')
      {
        if (sscanf(cmd + 3, "%d", &tmp) == 1)
          sdac1.mod_amp = tmp;
        Serial.print("A1=");
        Serial.print(sdac1.mod_amp);
        Serial.println(" : ADC1 amplitude of modulation in signal set");
        fill_table(&sdac1, sdac1.pre_sig, sdac1.mod_amp);
      }
      if (cmd[2] == '?' || cmd[2] == '\n' || cmd[2] == '\r')
      {
        Serial.print("A1=");
        Serial.print(sdac1.mod_amp);
        Serial.println(" : ADC1 amplitude of modulation in signal value");
      }
    }
  }
  void B_command(void)
  {
    int tmp;
    // setting the modulation amplitude
    if (cmd[1] == '0')
    {
      if (cmd[2] == '=')
      {
        if (sscanf(cmd + 3, "%d", &tmp) == 1)
          sdac0.std_by = tmp;
        Serial.print("B0=");
        Serial.print(sdac0.std_by);
        Serial.println(" : ADC0 stand-by signal set");
      }
      if (cmd[2] == '?' || cmd[2] == '\n' || cmd[2] == '\r')
      {
        Serial.print("B0=");
        Serial.print(sdac0.std_by);
        Serial.println(" : ADC0 stand-by signal value");
      }
    }
    else if (cmd[1] == '1')
    {
      if (cmd[2] == '=')
      {
        if (sscanf(cmd + 3, "%d", &tmp) == 1)
          sdac1.std_by = tmp;
        Serial.print("B1=");
        Serial.print(sdac1.std_by);
        Serial.println(" : ADC1 stand-by signal set");
      }
      if (cmd[2] == '?' || cmd[2] == '\n' || cmd[2] == '\r')
      {
        Serial.print("B1=");
        Serial.print(sdac1.std_by);
        Serial.println(" : ADC1 stand-by signal value");
      }
    }
  }
  void O_command(void)
  {
    int tmp;
    // setting the modulation amplitude
    if (cmd[1] == '0')
    {
      if (cmd[2] == '=')
      {
        if (sscanf(cmd + 3, "%d", &tmp) == 1)
          sdac0.n_mean = tmp;
        Serial.print("O0=");
        Serial.print(sdac0.n_mean);
        Serial.println(" : ADC0 # period of constant signal set");
      }
      if (cmd[2] == '?' || cmd[2] == '\n' || cmd[2] == '\r')
      {
        Serial.print("O0=");
        Serial.print(sdac0.n_mean);
        Serial.println(" : ADC0 # period of constant signal value");
      }
    }
    else if (cmd[1] == '1')
    {
      if (cmd[2] == '=')
      {
        if (sscanf(cmd + 3, "%d", &tmp) == 1)
          sdac1.n_mean = tmp;
        Serial.print("O1=");
        Serial.print(sdac1.n_mean);
        Serial.println(" : ADC1 # period of constant signal set");
      }
      if (cmd[2] == '?' || cmd[2] == '\n' || cmd[2] == '\r')
      {
        Serial.print("O1=");
        Serial.print(sdac1.n_mean);
        Serial.println(" : ADC1 # period of constant signal value");
      }
    }
  }
  void D_command(void)
  {
    int i;
    // setting the modulation amplitude
    if (cmd[1] == '0' && echo)
    {
      if (cmd[2] == '?' || cmd[2] == '\n' || cmd[2] == '\r')
      {
        Serial.print("ADC0 signal : ");
        for (i = 0; i < TABLESIZE; i++)
        {
          Serial.print(i);
          Serial.print("\t");
          Serial.println(sdac0.table[i]);
        }
      }
    }
    if (cmd[1] == '1')
    {
      if (cmd[2] == '?' || cmd[2] == '\n' || cmd[2] == '\r')
      {
        Serial.print("ADC1 signal : ");
        for (i = 0; i < TABLESIZE; i++)
        {
          Serial.print(i);
          Serial.print("\t");
          Serial.println(sdac1.table[i]);
        }
      }
    }
  }
  void P_command(void)
  {
    int tmp;
    // setting the number of camera period from one modulation
    if (cmd[1] == '=')
    {
      if (sscanf(cmd + 2, "%d", &tmp) == 1)
        freq_in_camera_fr = tmp;
      Serial.print("P=");
      Serial.print(freq_in_camera_fr);
      Serial.print(" : Number of camera period from one modulation set to : ");
      dac_per = (camera_per * freq_in_camera_fr) / TABLESIZE;
      Serial.print("Im ");
      Serial.print(imn, DEC);
      Serial.print(" per ");
      Serial.print(camera_per, DEC);
      Serial.print(" timer3 ");
      Serial.println(dac_per, DEC);
      Timer3.stop();
      Timer3.start(dac_per); // Calls every dac_period in micros
    }
    if (cmd[1] == '?' || cmd[1] == '\n' || cmd[1] == '\r')
    {
      Serial.print("P=");
      Serial.print(freq_in_camera_fr);
      Serial.print(" : Number of camera period from one modulation set to : ");
      dac_per = (camera_per * freq_in_camera_fr) / TABLESIZE;
      Serial.print("Im ");
      Serial.print(imn, DEC);
      Serial.print(" per ");
      Serial.print(camera_per, DEC);
      Serial.print(" timer3 ");
      Serial.println(dac_per, DEC);
    }
  }

  void S_command(void)
  {
    // start next modulation
    if (sdac0.p_running == 0)
    {
      sdac0.i_mean = 0;
      sdac0.i_sig = 0;
      sdac0.start_on_next_frame = 1;
      Serial.print("S=");
      Serial.print(imn, DEC);
      Serial.println(" New modulation started ");
    }
  }
  void E_command(void)
  {
    // start next modulation
    if (cmd[1] == '1')
    {
      echo = 1;
      Serial.println("Echo set ON");
    }
    else if (cmd[1] == '0')
    {
      echo = 0;
      Serial.println("Echo set OFF");
    }
    else
    {
      Serial.print("Wrong command: ");
      Serial.println(cmd);
    }
  }

  void G_command(void)
  {

    if (cmd[1] == '1')
    {
      digitalWrite(gpio_cam, true);
      Serial.println("GPIO 1 set to 1");
    }
    else if (cmd[1] == '0')
    {
      digitalWrite(gpio_cam, false);
      Serial.println("GPIO 1 set to 0");
    }
    else
    {
      Serial.print("Wrong command: ");
      Serial.println(cmd);
    }
  }

  void loop() {

    serialEvent(); //call the function
    // print the string when a newline arrives:
    if (stringComplete)
    {
      if (echo) Serial.println(cmd);
      if (cmd[0] == 'M')       M_command();
      else if (cmd[0] == 'N')  N_command();
      else if (cmd[0] == 'A')  A_command();
      else if (cmd[0] == 'B')  B_command();
      else if (cmd[0] == 'P')  P_command();
      else if (cmd[0] == 'O')  O_command();
      else if (cmd[0] == 'S')  S_command();
      else if (cmd[0] == 'E')  E_command();
      else if (cmd[0] == 'G')  G_command();
      // clear the string:
      cmd[0] = 0;
      n_cmd = 0;
      stringComplete = false;
    }
    //if (meanReady) {
    //Serial.println(dtm,DEC);
    //}
    if ((camera_alert == 0) && (i_handler > (freq_in_camera_fr + 10)))
    {
      if (echo)
      {
        Serial.print("Camera sync Pb ");
        Serial.print(i_handler, DEC);
        Serial.print(" instead of ");
        Serial.println(freq_in_camera_fr, DEC);
      }
      i_handler = 0;
      camera_alert = 1;
    }
    if (camera_alert == -1)
    {
      if (echo) Serial.println("Camera sync normal ");
      camera_alert = 0;
    }
    if (imn > last_im)
    {
      if  (abs((int)camera_per - (int)camera_per_1) > 50)
      {
        dac_per = (camera_per * freq_in_camera_fr) / TABLESIZE;
        if (echo)
        {
          Serial.print("Im ");
          Serial.print(imn, DEC);
          Serial.print(" per ");
          Serial.print(camera_per, DEC);
          Serial.print(" timer3 ");
          Serial.println(dac_per, DEC);
        }
        Timer3.stop();
        Timer3.start(dac_per); // Calls every dac_period in micros
        camera_per_1 = camera_per;
      }

      if (sdac0.p_running)
      {
        if (echo)
        {
          Serial.print("Im ");
          Serial.print(imn, DEC);
          Serial.print(" iter ");
          Serial.print(sdac0.i_per, DEC);
          Serial.print(" first ");
          Serial.print(sdac0.i_mean, DEC);
          Serial.print("/");
          Serial.print(sdac0.n_mean, DEC);
          Serial.print(" mod ");
          Serial.print(sdac0.i_sig, DEC);
          Serial.print("/");
          Serial.print(sdac0.n_sig, DEC);
          Serial.print(" Dac0 ");
          Serial.print(lvdac0, DEC);
          Serial.print(" Dac1 ");
          Serial.println(lvdac1, DEC);
        }
      }
      last_im = imn;
    }



  }


