#pragma once
#ifndef _DUE_SERIAL_H_
#define _DUE_SERIAL_H_
#include "xvin.h"

# include "allegro.h"
#ifdef XV_WIN32 
# include "winalleg.h"
#endif

#define READ_ONLY 0
#define WRITE_ONLY 1
#define WRITE_READ 2

#ifdef XV_WIN32 
typedef HANDLE com_port_t;
#else
typedef int com_port_t;
#endif

# ifndef _DUE_SERIAL_C_


PXV_VAR(com_port_t, hCom);
#ifdef XV_WIN32 
PXV_VAR(COMMTIMEOUTS, lpTo);
PXV_VAR(COMMCONFIG, lpCC);
#endif
PXV_VAR(char, str_com[32]);

# endif

# ifdef _DUE_SERIAL_C_
#ifdef XV_WIN32 
com_port_t hCom = NULL;
COMMTIMEOUTS lpTo;
COMMCONFIG lpCC;
#else
com_port_t hCom = -1;
#endif

char str_com[32];

char *debug_file = "serial_debug.txt";
int debug = 0;
FILE *fp_debug = NULL;

# endif

PXV_FUNC(com_port_t, init_serial_port, (char *port_str, int baudrate, int n_bits, int parity, int stops, int RTSCTS));
PXV_FUNC(int,Write_serial_port,(com_port_t hCom_port, char *ch, unsigned long NumberOfBytesToWrite));
PXV_FUNC(int,Read_serial_port,(com_port_t hCom_port, char *ch, unsigned long nNumberOfBytesToRead, unsigned long *lpNumberOfBytesWritten));
PXV_FUNC(int,talk_serial_port,(com_port_t hCom_port, char *command, int wait_answer, char * answer, unsigned long NumberOfBytesToWrite, unsigned long nNumberOfBytesToRead));
PXV_FUNC(int, CloseSerialPort,(com_port_t hCom));
//PXV_FUNC(HANDLE,init_serial_port,(unsigned short port_number));

PXV_FUNC(int, sent_cmd_and_read_answer, (com_port_t hCom, char *Command, char *answer, unsigned long n_answer, 
					 unsigned long *n_written, unsigned long *dwErrors, unsigned long *t0));

PXV_FUNC(int, write_command_on_serial_port,(void));
PXV_FUNC(int, read_on_serial_port,(void));
PXV_FUNC(int, purge_com,(void));
PXV_FUNC(int, close_serial_port,(void));
PXV_FUNC(MENU*, serialw_menu, (void));
PXV_FUNC(int, serialw32_main, (int argc, char **argv));

PXV_FUNC(int, init_due_serial, (void));


PXV_FUNC(int, do_Due_serial_rescale_plot, (void));
PXV_FUNC(MENU*, Due_serial_plot_menu, (void));
PXV_FUNC(int, do_Due_serial_rescale_data_set, (void));
PXV_FUNC(int, Due_serial_main, (int argc, char **argv));


PXV_VAR(char*, debug_file);
PXV_VAR(int, debug);
PXV_VAR(FILE *, fp_debug);

PXV_FUNC(int, Set_Due_cycle, (void));
PXV_FUNC(int, start_modulation, (void));

#endif



