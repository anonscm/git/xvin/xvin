/*
 *    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _FFT2D_C_
#define _FFT2D_C_

# include "allegro.h"
# include "xvin.h"
# include "float.h"
# include "fftl32n.h"
# include "fillib.h"
# include "fftbtl32n.h"
//# include "fillibbt.h"

# include <fftw3.h>		// include the fftw v3.0.1 or v3.1.1 library for computing spectra:

/* If you include other plug-ins header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "fft2d.h"

/*
   shfit pixels by nx/2 and ny/2
   if dest == NULL, dest will be created
   dest may be equal to ois, in this case image is modified
 */

O_i 	*shift_2d_im(O_i *dest, O_i *ois)
{
    union pix *pd;//, *ps;
    int i, j, ii, jj, onx, ony, onx_2, ony_2, im, nfi;
    unsigned char ch;
    short int in;
    unsigned int ui;
    int li;
    float fl;
    rgb_t rgb_;
    rgba_t rgba_;
    rgb16_t rgb_16;
    rgba16_t rgba_16;
    double db;
    mcomplex mc;
    mdcomplex mdc;

    if (ois == NULL) return NULL;
    if (dest == NULL) dest = duplicate_image(ois, NULL);
    if (dest == NULL) return NULL;

    //ps = ois->im.pixel;
    onx = ois->im.nx;
    ony = ois->im.ny;
    onx_2 = onx/2;
    ony_2 = ony/2;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;

    for(im = 0; im < nfi; im++)
       {
	 switch_frame(dest,im);
	 pd = dest->im.pixel;
	 if (ois->im.data_type == IS_CHAR_IMAGE)
	   {
	     for (i = 0; i< ony ; i++)
	       {
		 for (j = 0, jj = onx_2; j < onx_2 ; j++, jj++)
		   {
		     ch = pd[i].ch[j];
		     pd[i].ch[j] = pd[i].ch[jj];
		     pd[i].ch[jj] = ch;
		   }
	       }
	     for (i = 0, ii = ony_2; i< ony_2 ; i++, ii++)
	       {
		 for (j = 0; j < onx ; j++)
		   {
		     ch = pd[i].ch[j];
		     pd[i].ch[j] = pd[ii].ch[j];
		     pd[ii].ch[j] = ch;
		   }
	       }
	   }
	 else if (ois->im.data_type == IS_RGB_PICTURE)
	   {
	     for (i = 0; i< ony ; i++)
	       {
		 for (j = 0, jj = onx_2; j < onx_2 ; j++, jj++)
		   {
		     rgb_ = pd[i].rgb[j];
		     pd[i].rgb[j] = pd[i].rgb[jj];
		     pd[i].rgb[jj] = rgb_;
		   }
	       }
	     for (i = 0, ii = ony_2; i< ony_2 ; i++, ii++)
	       {
		 for (j = 0; j < onx ; j++)
		   {
		     rgb_ = pd[i].rgb[j];
		     pd[i].rgb[j] = pd[ii].rgb[j];
		     pd[ii].rgb[j] = rgb_;
		   }
	       }
	   }
	 else if (ois->im.data_type == IS_RGBA_PICTURE)
	   {
	     for (i = 0; i< ony ; i++)
	       {
		 for (j = 0, jj = onx_2; j < onx_2 ; j++, jj++)
		   {
		     rgba_ = pd[i].rgba[j];
		     pd[i].rgba[j] = pd[i].rgba[jj];
		     pd[i].rgba[jj] = rgba_;
		   }
	       }
	     for (i = 0, ii = ony_2; i< ony_2 ; i++, ii++)
	       {
		 for (j = 0; j < onx ; j++)
		   {
		     rgba_ = pd[i].rgba[j];
		     pd[i].rgba[j] = pd[ii].rgba[j];
		     pd[ii].rgba[j] = rgba_;
		   }
	       }
	   }
	 else if (ois->im.data_type == IS_RGB16_PICTURE)
	   {
	     for (i = 0; i< ony ; i++)
	       {
		 for (j = 0, jj = onx_2; j < onx_2 ; j++, jj++)
		   {
		     rgb_16 = pd[i].rgb16[j];
		     pd[i].rgb16[j] = pd[i].rgb16[jj];
		     pd[i].rgb16[jj] = rgb_16;
		   }
	       }
	     for (i = 0, ii = ony_2; i< ony_2 ; i++, ii++)
	       {
		 for (j = 0; j < onx ; j++)
		   {
		     rgb_16 = pd[i].rgb16[j];
		     pd[i].rgb16[j] = pd[ii].rgb16[j];
		     pd[ii].rgb16[j] = rgb_16;
		   }
	       }
	   }
	 else if (ois->im.data_type == IS_RGBA16_PICTURE)
	   {
	     for (i = 0; i< ony ; i++)
	       {
		 for (j = 0, jj = onx_2; j < onx_2 ; j++, jj++)
		   {
		     rgba_16 = pd[i].rgba16[j];
		     pd[i].rgba16[j] = pd[i].rgba16[jj];
		     pd[i].rgba16[jj] = rgba_16;
		   }
	       }
	     for (i = 0, ii = ony_2; i< ony_2 ; i++, ii++)
	       {
		 for (j = 0; j < onx ; j++)
		   {
		     rgba_16 = pd[i].rgba16[j];
		     pd[i].rgba16[j] = pd[ii].rgba16[j];
		     pd[ii].rgba16[j] = rgba_16;
		   }
	       }
	   }
	 else if (ois->im.data_type == IS_INT_IMAGE)
	   {
	     for (i = 0; i< ony ; i++)
	       {
		 for (j = 0, jj = onx_2; j < onx_2 ; j++, jj++)
		   {
		     in = pd[i].in[j];
		     pd[i].in[j] = pd[i].in[jj];
		     pd[i].in[jj] = in;
		   }
	       }
	     for (i = 0, ii = ony_2; i< ony_2 ; i++, ii++)
	       {
		 for (j = 0; j < onx ; j++)
		   {
		     in = pd[i].in[j];
		     pd[i].in[j] = pd[ii].in[j];
		     pd[ii].in[j] = in;
		   }
	       }
	   }
	 else if (ois->im.data_type == IS_UINT_IMAGE)
	   {
	     for (i = 0; i< ony ; i++)
	       {
		 for (j = 0, jj = onx_2; j < onx_2 ; j++, jj++)
		   {
		     ui = pd[i].ui[j];
		     pd[i].ui[j] = pd[i].ui[jj];
		     pd[i].ui[jj] = ui;
		   }
	       }
	     for (i = 0, ii = ony_2; i< ony_2 ; i++, ii++)
	       {
		 for (j = 0; j < onx ; j++)
		   {
		     ui = pd[i].ui[j];
		     pd[i].ui[j] = pd[ii].ui[j];
		     pd[ii].ui[j] = ui;
		   }
	       }
	   }
	 else if (ois->im.data_type == IS_LINT_IMAGE)
	   {
	     for (i = 0; i< ony ; i++)
	       {
		 for (j = 0, jj = onx_2; j < onx_2 ; j++, jj++)
		   {
		     li = pd[i].li[j];
		     pd[i].li[j] = pd[i].li[jj];
		     pd[i].li[jj] = li;
		   }
	       }
	     for (i = 0, ii = ony_2; i< ony_2 ; i++, ii++)
	       {
		 for (j = 0; j < onx ; j++)
		   {
		     li = pd[i].li[j];
		     pd[i].li[j] = pd[ii].li[j];
		     pd[ii].li[j] = li;
		   }
	       }
	   }
	 else if (ois->im.data_type == IS_FLOAT_IMAGE)
	   {
	     for (i = 0; i< ony ; i++)
	       {
		 for (j = 0, jj = onx_2; j < onx_2 ; j++, jj++)
		   {
		     fl = pd[i].fl[j];
		     pd[i].fl[j] = pd[i].fl[jj];
		     pd[i].fl[jj] = fl;
		   }
	       }
	     for (i = 0, ii = ony_2; i< ony_2 ; i++, ii++)
	       {
		 for (j = 0; j < onx ; j++)
		   {
		     fl = pd[i].fl[j];
		     pd[i].fl[j] = pd[ii].fl[j];
		     pd[ii].fl[j] = fl;
		   }
	       }
	   }
	 else if (ois->im.data_type == IS_COMPLEX_IMAGE)
	   {
	     for (i = 0; i< ony ; i++)
	       {
		 for (j = 0, jj = onx_2; j < onx_2 ; j++, jj++)
		   {
		     mc = pd[i].cp[j];
		     pd[i].cp[j] = pd[i].cp[jj];
		     pd[i].cp[jj] = mc;
		   }
	       }
	     for (i = 0, ii = ony_2; i< ony_2 ; i++, ii++)
	       {
		 for (j = 0; j < onx ; j++)
		   {
		     mc = pd[i].cp[j];
		     pd[i].cp[j] = pd[ii].cp[j];
		     pd[ii].cp[j] = mc;
		   }
	       }
	   }
	 else if (ois->im.data_type == IS_DOUBLE_IMAGE)
	   {
	     for (i = 0; i< ony ; i++)
	       {
		 for (j = 0, jj = onx_2; j < onx_2 ; j++, jj++)
		   {
		     db = pd[i].db[j];
		     pd[i].db[j] = pd[i].db[jj];
		     pd[i].db[jj] = db;
		   }
	       }
	     for (i = 0, ii = ony_2; i< ony_2 ; i++, ii++)
	       {
		 for (j = 0; j < onx ; j++)
		   {
		     db = pd[i].db[j];
		     pd[i].db[j] = pd[ii].db[j];
		     pd[ii].db[j] = db;
		   }
	       }
	   }
	 else if (ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
	   {
	     for (i = 0; i< ony ; i++)
	       {
		 for (j = 0, jj = onx_2; j < onx_2 ; j++, jj++)
		   {
		     mdc = pd[i].dcp[j];
		     pd[i].dcp[j] = pd[i].dcp[jj];
		     pd[i].dcp[jj] = mdc;
		   }
	       }
	     for (i = 0, ii = ony_2; i< ony_2 ; i++, ii++)
	       {
		 for (j = 0; j < onx ; j++)
		   {
		     mdc = pd[i].dcp[j];
		     pd[i].dcp[j] = pd[ii].dcp[j];
		     pd[ii].dcp[j] = mdc;
		   }
	       }
	   }
       }
    return dest;
}
// Determine the number and position of small images covering the original one with minimum overlap
int compute_nb_of_min_subset_ROI(O_i *ois, int sub_w, int sub_h, int x0, int y0, int w0, int w1)
{
    //union pix *ps;
    int cx, cy, nx0, ny0;//, onx, ony;

    (void)x0;
    (void)y0;
    if (ois == NULL)            return -1;
    //ps = ois->im.pixel;
    //onx = ois->im.nx;   ony = ois->im.ny;
    if (sub_w > w0)            return -2;
    if (sub_h > w1)            return -3;
    cx = 4*(w0 - sub_w);
    nx0 = cx/(3*sub_w);
    nx0 += (cx%(3*sub_w)) ? 2 : 1;

    cy = 4*(w1 - sub_h);
    ny0 = cy/(3*sub_h);
    ny0 += (cy%(3*sub_h)) ? 2 : 1;
    return nx0 * ny0;
}

int compute_nb_of_min_subset_and_border_ROI(O_i *ois, int sub_w, int sub_h, int x0, int y0, int w0, int w1, int *border_x, int *border_y)
{
    //union pix *ps;
    int cx, cy, nx0, ny0;//, onx, ony;

    (void)x0;
    (void)y0;
    if (ois == NULL)            return -1;
    //ps = ois->im.pixel;
    //onx = ois->im.nx;   ony = ois->im.ny;
    if (sub_w > w0)            return -2;
    if (sub_h > w1)            return -3;
    cx = 4*(w0 - sub_w);
    nx0 = cx/(3*sub_w);
    nx0 += (cx%(3*sub_w)) ? 2 : 1;
    if (border_x) *border_x = sub_w - (cx/(4 * (nx0-1)));

    cy = 4*(w1 - sub_h);
    ny0 = cy/(3*sub_h);
    ny0 += (cy%(3*sub_h)) ? 2 : 1;
    if (border_x) *border_y = sub_h - (cy/(4 * (ny0-1)));
    return nx0 * ny0;
}


int find_min_subset_pos_ROI(O_i *ois, int sub_w, int sub_h, int n, int *x, int *y, int x0, int y0, int w0, int w1)
{
    int cx, cy, nx0, ny0, ix, iy;

    if (ois == NULL)            return -1;

    if (sub_w > w0)            return -2;
    if (sub_h > w1)            return -3;
    cx = 4*(w0 - sub_w);
    nx0 = cx/(3*sub_w);
    nx0 += (cx%(3*sub_w)) ? 2 : 1;

    cy = 4*(w1 - sub_h);
    ny0 = cy/(3*sub_h);
    ny0 += (cy%(3*sub_h)) ? 2 : 1;

    if (n > nx0 * ny0)         return -4;
    ix = n%nx0;
    iy = n/nx0;
    *x = x0 + (cx * ix)/(4 * (nx0-1));
    *y = y0 + (cy * iy)/(4 * (ny0-1));
    /*
       win_printf("onx = %d cx = %d nx0 = %d ix %d x %d\n"
       "ony = %d cy = %d ny0 = %d iy %d y %d\n n %d\n"
       ,onx,cx/2,nx0,ix,*x,ony,cy/2,ny0,iy,*y,n);
       */
    return nx0 * ny0;
}

int find_min_subset_good_pixel_pos_ROI(O_i *ois, int sub_w, int sub_h, int n, int *x, int *y, int *w, int *h, int w0, int w1)
{

    int cx, cy, nx0, ny0, ix, iy;

    if (ois == NULL)            return -1;


    if (sub_w > w0)            return -2;
    if (sub_h > w1)            return -3;
    cx = 4*(w0 - sub_w);
    nx0 = cx/(3*sub_w);
    nx0 += (cx%(3*sub_w)) ? 2 : 1;


    cy = 4*(w1 - sub_h);
    ny0 = cy/(3*sub_h);
    ny0 += (cy%(3*sub_h)) ? 2 : 1;

    *w = 3*sub_w/4;
    *h = 3*sub_h/4;
    if (n > nx0 * ny0)         return -4;
    ix = n%nx0;
    iy = n/nx0;
    *x = (cx * ix)/(4 * (nx0-1));
    if (ix != 0)       *x += sub_w/8;
    else               *w += sub_w/8;
    if (ix == (nx0-1))   *w += sub_w/8;
    *y = (cy * iy)/(4 * (ny0-1));
    if (iy != 0)       *y += sub_h/8;
    else               *h += sub_h/8;
    if (iy == (ny0-1))   *h += sub_w/8;
    return nx0 * ny0;
}


// Determine the number and position of small images covering the original one
int compute_nb_of_subset_ROI(O_i *ois, int sub_w, int sub_h, int x0, int y0, int w0, int w1)
{

    int cx, cy, nx0, ny0;
    (void)x0;
    (void)y0;
    if (ois == NULL)            return -1;


    if (sub_w > w0)            return -2;
    if (sub_h > w1)            return -3;
    cx = 2*(w0 - sub_w);
    nx0 = cx/sub_w;
    nx0 += (cx%sub_w) ? 2 : 1;


    cy = 2*(w1 - sub_h);
    ny0 = cy/sub_h;
    ny0 += (cy%sub_h) ? 2 : 1;

    return nx0 * ny0;
}

int find_subset_pos_ROI(O_i *ois, int sub_w, int sub_h, int n, int *x, int *y, int x0, int y0, int w0, int w1)
{

    int cx, cy, nx0, ny0, ix, iy;

    if (ois == NULL)            return -1;


    if (sub_w > w0)            return -2;
    if (sub_h > w1)            return -3;
    cx = 2*(w0 - sub_w);
    nx0 = cx/sub_w;
    nx0 += (cx%sub_w) ? 2 : 1;


    cy = 2*(w1 - sub_h);
    ny0 = cy/sub_h;
    ny0 += (cy%sub_h) ? 2 : 1;


    if (n > nx0 * ny0)         return -4;
    ix = n%nx0;
    iy = n/nx0;
    *x = x0 + (cx * ix)/(2 * (nx0-1));
    *y = y0 + (cy * iy)/(2 * (ny0-1));
    /*
       win_printf("onx = %d cx = %d nx0 = %d ix %d x %d\n"
       "ony = %d cy = %d ny0 = %d iy %d y %d\n n %d\n"
       ,onx,cx/2,nx0,ix,*x,ony,cy/2,ny0,iy,*y,n);
       */
    return nx0 * ny0;
}

int find_subset_good_pixel_pos_ROI(O_i *ois, int sub_w, int sub_h, int n, int *x, int *y, int *w, int *h, int w0, int w1)
{
    //union pix *ps;
    int cx, cy, nx0, ny0, ix, iy;// , onx, ony

    if (ois == NULL)            return -1;
    //ps = ois->im.pixel;
    //onx = ois->im.nx;   ony = ois->im.ny;
    if (sub_w > w0)            return -2;
    if (sub_h > w1)            return -3;
    cx = 2*(w0 - sub_w);
    nx0 = cx/sub_w;
    nx0 += (cx%sub_w) ? 2 : 1;


    cy = 2*(w1 - sub_h);
    ny0 = cy/sub_h;
    ny0 += (cy%sub_h) ? 2 : 1;

    *w = sub_w/2;
    *h = sub_h/2;
    if (n > nx0 * ny0)         return -4;
    ix = n%nx0;
    iy = n/nx0;
    *x = (cx * ix)/(2 * (nx0-1));
    if (ix != 0)       *x += sub_w/4;
    else               *w += sub_w/4;
    if (ix == (nx0-1))   *w += sub_w/4;
    *y = (cy * iy)/(2 * (ny0-1));
    if (iy != 0)       *y += sub_h/4;
    else               *h += sub_h/4;
    if (iy == (ny0-1))   *h += sub_w/4;

    /*
       win_printf("onx = %d cx = %d nx0 = %d ix %d x %d\n"
       "ony = %d cy = %d ny0 = %d iy %d y %d\n n %d\n"
       ,onx,cx/2,nx0,ix,*x,ony,cy/2,ny0,iy,*y,n);
       */
    return nx0 * ny0;
}





// Determine the number and position of small images covering the original one
int compute_nb_of_subset(O_i *ois, int sub_w, int sub_h, int *nx, int *ny)
{

    int i, nx0, ny0, onx, ony; // , cx, cy

    if (ois == NULL)            return -1;

    onx = ois->im.nx;   ony = ois->im.ny;
    if (sub_w > onx)            return -2;
    if (sub_h > ony)            return -3;
    for (i = 2; ((1+(i*3))*sub_w) < (4*onx); i++);
    //cx = (4*onx) - (3*sub_w);
    //cx = ((1 + (i*3))*sub_w)/4;
    nx0 = i;
    //nx0 = cx/(2*sub_w);
    //if nx0 += 2;
    //nx0 += (cx%sub_w) ? 2 : 1;
    //nx0 = (nx0 < 1) ? 1 : nx0;

    for (i = 2; ((1+(i*3))*sub_h) < (4*ony); i++);
    //cy = (2*ony) - (3*sub_h);
    //cy = ((1 + (i*3))*sub_h)/4;
    ny0 = i;
    //ny0 = cy/(2*sub_h);
    //ny0 += (cy%sub_h) ? 2 : 1;
    //ny0 = (ny0 < 1) ? 1 : ny0;
    //win_printf("Image in %dx%d => %dx%d (%dx%d)",onx,ony,cx,cy,nx0,ny0);
    if (nx) *nx = nx0;
    if (nx) *ny = ny0;
    return nx0 * ny0;
}

int find_subset_pos(O_i *ois, int sub_w, int sub_h, int n, int *x, int *y)
{

  int i, cx, cy, nx0, ny0, onx, ony, ix, iy, xoff, yoff;

    if (ois == NULL)            return -1;

    onx = ois->im.nx;   ony = ois->im.ny;
    if (sub_w > onx)            return -2;
    if (sub_h > ony)            return -3;
    for (i = 2; ((1+(i*3))*sub_w) < (4*onx); i++);
    //cx = (2*onx) - (3*sub_w);
    cx = ((1 + (i*3))*sub_w);
    nx0 = i;
    //nx0 = cx/(2*sub_w);
    //nx0 += (cx%sub_w) ? 2 : 1;
    //nx0 = (nx0 < 1) ? 1 : nx0;

    for (i = 2; ((1+(i*3))*sub_h) < (4*ony); i++);
    cy = ((1 + (i*3))*sub_h);
    ny0 = i;
    //cy = (2*ony) - (3*sub_h);
    //ny0 = cy/(2*sub_h);
    //ny0 += (cy%sub_h) ? 2 : 1;
    //ny0 = (ny0 < 1) ? 1 : ny0;

    if (n > nx0 * ny0)         return -4;
    ix = n%nx0;
    iy = n/nx0;
    //*x = (nx0 > 1) ? (cx * ix)/(2 * (nx0-1)) : xoff;
    //*y = (ny0 > 1) ? (cy * iy)/(2 * (ny0-1)) : yoff;
    xoff = -(cx - (4 * onx))/2;
    yoff = -(cy - (4 * ony))/2;
    if (x) *x = (xoff + (ix * 3 * sub_w))/4;
    if (y) *y = (yoff + (iy * 3 * sub_h))/4;
    /*
    win_printf("Image in %dx%d => %dx%d (%d:%dx%d%d)"
    	       "xoff = %d yoff = %d; x %d y %d\n"
    	       ,onx,ony,cx/4,cy/4,ix,nx0,iy,ny0,xoff/4,yoff/4,*x,*y);
       win_printf("onx = %d cx = %d nx0 = %d ix %d x %d\n"
       "ony = %d cy = %d ny0 = %d iy %d y %d\n n %d\n"
       ,onx,cx/2,nx0,ix,*x,ony,cy/2,ny0,iy,*y,n);
       */
    return nx0 * ny0;
}


int 	copy_one_subset_of_image(O_i *dest, O_i *ois, int x_start, int y_start)
{
  union pix *pd, *ps;
  int i, j, onx, ony, donx, dony;

  if (ois == NULL || dest == NULL)                       return 1;
  if (ois->im.data_type != dest->im.data_type)           return 2;
  pd = dest->im.pixel;
  ps = ois->im.pixel;
  onx = ois->im.nx;
  ony = ois->im.ny;
  donx = dest->im.nx;
  dony = dest->im.ny;
  //if (x_start < 0 || y_start < 0)                        return 3;
  //if ((x_start + donx > onx) || (y_start + dony > ony))  return 4;
  dest->im.user_ispare[dest->im.c_f][0] = x_start;
  dest->im.user_ispare[dest->im.c_f][1] = y_start;
  if (ois->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = 0; i< dony ; i++)
        {
	  if ((i+y_start < 0) || (i+y_start >= ony)) continue;
	  for (j = 0; j < donx ; j++)
	    {
	      if ((j+x_start < 0) || (j+x_start >= onx)) continue;
	      pd[i].ch[j] = ps[i+y_start].ch[j+x_start];
	    }
        }
    }
  else if (ois->im.data_type == IS_RGB_PICTURE)
    {
      for (i = 0; i< dony ; i++)
        {
	  if ((i+y_start < 0) || (i+y_start >= ony)) continue;
	  for (j = 0; j < donx ; j++)
	    {
	      if ((j+x_start < 0) || (j+x_start >= onx)) continue;
	      pd[i].rgb[j] = ps[i+y_start].rgb[j+x_start];
	    }
        }
    }
  else if (ois->im.data_type == IS_RGBA_PICTURE)
    {
      for (i = 0; i< dony ; i++)
        {
	  if ((i+y_start < 0) || (i+y_start >= ony)) continue;
	  for (j = 0; j < donx ; j++)
	    {
	      if ((j+x_start < 0) || (j+x_start >= onx)) continue;
	      pd[i].rgba[j] = ps[i+y_start].rgba[j+x_start];
	    }
        }
    }
  else if (ois->im.data_type == IS_RGB16_PICTURE)
    {
      for (i = 0; i< dony ; i++)
        {
	  if ((i+y_start < 0) || (i+y_start >= ony)) continue;
	  for (j = 0; j < donx ; j++)
	    {
	      if ((j+x_start < 0) || (j+x_start >= onx)) continue;
	      pd[i].rgb16[j] = ps[i+y_start].rgb16[j+x_start];
	    }
        }
    }
  else if (ois->im.data_type == IS_RGBA16_PICTURE)
    {
      for (i = 0; i< dony ; i++)
        {
	  if ((i+y_start < 0) || (i+y_start >= ony)) continue;
	  for (j = 0; j < donx ; j++)
	    {
	      if ((j+x_start < 0) || (j+x_start >= onx)) continue;
	      pd[i].rgba16[j] = ps[i+y_start].rgba16[j+x_start];
	    }
        }
    }
  else if (ois->im.data_type == IS_INT_IMAGE)
    {
      for (i = 0; i< dony ; i++)
        {
	  if ((i+y_start < 0) || (i+y_start >= ony)) continue;
	  for (j = 0; j < donx ; j++)
	    {
	      if ((j+x_start < 0) || (j+x_start >= onx)) continue;
	      pd[i].in[j] = ps[i+y_start].in[j+x_start];
	    }
        }
    }
  else if (ois->im.data_type == IS_UINT_IMAGE)
    {
      for (i = 0; i< dony ; i++)
	{
	  if ((i+y_start < 0) || (i+y_start >= ony)) continue;
	  for (j = 0; j < donx ; j++)
	    {
	      if ((j+x_start < 0) || (j+x_start >= onx)) continue;
	      pd[i].ui[j] = ps[i+y_start].ui[j+x_start];
	    }
        }
    }
  else if (ois->im.data_type == IS_LINT_IMAGE)
    {
      for (i = 0; i< dony ; i++)
        {
	  if ((i+y_start < 0) || (i+y_start >= ony)) continue;
	  for (j = 0; j < donx ; j++)
	    {
	      if ((j+x_start < 0) || (j+x_start >= onx)) continue;
	      pd[i].li[j] = ps[i+y_start].li[j+x_start];
	    }
        }
    }
  else if (ois->im.data_type == IS_FLOAT_IMAGE)
    {
      for (i = 0; i< dony ; i++)
	{
	  if ((i+y_start < 0) || (i+y_start >= ony)) continue;
	  for (j = 0; j < donx ; j++)
	    {
	      if ((j+x_start < 0) || (j+x_start >= onx)) continue;
	      pd[i].fl[j] = ps[i+y_start].fl[j+x_start];
	    }
        }
    }
  else if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
      for (i = 0; i< dony ; i++)
        {
	  if ((i+y_start < 0) || (i+y_start >= ony)) continue;
	  for (j = 0; j < donx ; j++)
	    {
	      if ((j+x_start < 0) || (j+x_start >= onx)) continue;
	      pd[i].cp[j] = ps[i+y_start].cp[j+x_start];
	    }
        }
    }
  else if (ois->im.data_type == IS_DOUBLE_IMAGE)
    {
      for (i = 0; i< dony ; i++)
        {
	  if ((i+y_start < 0) || (i+y_start >= ony)) continue;
	  for (j = 0; j < donx ; j++)
	    {
	      if ((j+x_start < 0) || (j+x_start >= onx)) continue;
	      pd[i].db[j] = ps[i+y_start].db[j+x_start];
	    }
        }
    }
  else if (ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
      for (i = 0; i< dony ; i++)
        {
	  if ((i+y_start < 0) || (i+y_start >= ony)) continue;
	  for (j = 0; j < donx ; j++)
	    {
	      if ((j+x_start < 0) || (j+x_start >= onx)) continue;
	      pd[i].dcp[j] = ps[i+y_start].dcp[j+x_start];
	    }
        }
    }
  return 0;
}


int 	copy_one_subset_of_image_and_border(O_i *dest, O_i *ois, int x_start, int y_start)
{
  union pix *pd, *ps;
  int i, j, onx, ony, donx, dony, it, jt;

  if (ois == NULL || dest == NULL)                       return 1;
  if (ois->im.data_type != dest->im.data_type)           return 2;
  pd = dest->im.pixel;
  ps = ois->im.pixel;
  onx = ois->im.nx;
  ony = ois->im.ny;
  donx = dest->im.nx;
  dony = dest->im.ny;
  //if (x_start < 0 || y_start < 0)                        return 3;
  //if ((x_start + donx > onx) || (y_start + dony > ony))  return 4;
  dest->im.user_ispare[dest->im.c_f][0] = x_start;
  dest->im.user_ispare[dest->im.c_f][1] = y_start;
  if (ois->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = 0; i< dony ; i++)
        {
	  it = i+y_start;
	  it = (it < 0) ? 0 : it;
	  it = (it >= ony) ? ony-1 : it;
	  for (j = 0; j < donx ; j++)
	    {
	      jt = j+x_start;
	      jt = (jt < 0) ? 0 : jt;
	      jt = (jt >= onx) ? onx-1 : jt;
	      pd[i].ch[j] = ps[it].ch[jt];
	    }
        }
    }
  else if (ois->im.data_type == IS_RGB_PICTURE)
    {
      for (i = 0; i< dony ; i++)
        {
	  it = i+y_start;
	  it = (it < 0) ? 0 : it;
	  it = (it >= ony) ? ony-1 : it;
	  for (j = 0; j < donx ; j++)
	    {
	      jt = j+x_start;
	      jt = (jt < 0) ? 0 : jt;
	      jt = (jt >= onx) ? onx-1 : jt;
	      pd[i].rgb[j] = ps[it].rgb[jt];
	    }
        }
    }
  else if (ois->im.data_type == IS_RGBA_PICTURE)
    {
      for (i = 0; i< dony ; i++)
        {
	  it = i+y_start;
	  it = (it < 0) ? 0 : it;
	  it = (it >= ony) ? ony-1 : it;
	  for (j = 0; j < donx ; j++)
	    {
	      jt = j+x_start;
	      jt = (jt < 0) ? 0 : jt;
	      jt = (jt >= onx) ? onx-1 : jt;
	      pd[i].rgba[j] = ps[it].rgba[jt];
	    }
        }
    }
  else if (ois->im.data_type == IS_RGB16_PICTURE)
    {
      for (i = 0; i< dony ; i++)
        {
	  it = i+y_start;
	  it = (it < 0) ? 0 : it;
	  it = (it >= ony) ? ony-1 : it;
	  for (j = 0; j < donx ; j++)
	    {
	      jt = j+x_start;
	      jt = (jt < 0) ? 0 : jt;
	      jt = (jt >= onx) ? onx-1 : jt;
	      pd[i].rgb16[j] = ps[it].rgb16[jt];
	    }
        }
    }
  else if (ois->im.data_type == IS_RGBA16_PICTURE)
    {
      for (i = 0; i< dony ; i++)
        {
	  it = i+y_start;
	  it = (it < 0) ? 0 : it;
	  it = (it >= ony) ? ony-1 : it;
	  for (j = 0; j < donx ; j++)
	    {
	      jt = j+x_start;
	      jt = (jt < 0) ? 0 : jt;
	      jt = (jt >= onx) ? onx-1 : jt;
	      pd[i].rgba16[j] = ps[it].rgba16[jt];
	    }
        }
    }
  else if (ois->im.data_type == IS_INT_IMAGE)
    {
      for (i = 0; i< dony ; i++)
        {
	  it = i+y_start;
	  it = (it < 0) ? 0 : it;
	  it = (it >= ony) ? ony-1 : it;
	  for (j = 0; j < donx ; j++)
	    {
	      jt = j+x_start;
	      jt = (jt < 0) ? 0 : jt;
	      jt = (jt >= onx) ? onx-1 : jt;
	      pd[i].in[j] = ps[it].in[jt];
	    }
        }
    }
  else if (ois->im.data_type == IS_UINT_IMAGE)
    {
      for (i = 0; i< dony ; i++)
	{
	  it = i+y_start;
	  it = (it < 0) ? 0 : it;
	  it = (it >= ony) ? ony-1 : it;

	  for (j = 0; j < donx ; j++)
	    {
	      jt = j+x_start;
	      jt = (jt < 0) ? 0 : jt;
	      jt = (jt >= onx) ? onx-1 : jt;
	      pd[i].ui[j] = ps[it].ui[jt];
	    }
        }
    }
  else if (ois->im.data_type == IS_LINT_IMAGE)
    {
      for (i = 0; i< dony ; i++)
        {
	  it = i+y_start;
	  it = (it < 0) ? 0 : it;
	  it = (it >= ony) ? ony-1 : it;
	  for (j = 0; j < donx ; j++)
	    {
	      jt = j+x_start;
	      jt = (jt < 0) ? 0 : jt;
	      jt = (jt >= onx) ? onx-1 : jt;
	      pd[i].li[j] = ps[it].li[jt];
	    }
        }
    }
  else if (ois->im.data_type == IS_FLOAT_IMAGE)
    {
      for (i = 0; i< dony ; i++)
	{
	  it = i+y_start;
	  it = (it < 0) ? 0 : it;
	  it = (it >= ony) ? ony-1 : it;
	  for (j = 0; j < donx ; j++)
	    {
	      jt = j+x_start;
	      jt = (jt < 0) ? 0 : jt;
	      jt = (jt >= onx) ? onx-1 : jt;
	      pd[i].fl[j] = ps[it].fl[jt];
	    }
        }
    }
  else if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
      for (i = 0; i< dony ; i++)
        {
	  it = i+y_start;
	  it = (it < 0) ? 0 : it;
	  it = (it >= ony) ? ony-1 : it;
	  for (j = 0; j < donx ; j++)
	    {
	      jt = j+x_start;
	      jt = (jt < 0) ? 0 : jt;
	      jt = (jt >= onx) ? onx-1 : jt;
	      pd[i].cp[j] = ps[it].cp[jt];
	    }
        }
    }
  else if (ois->im.data_type == IS_DOUBLE_IMAGE)
    {
      for (i = 0; i< dony ; i++)
        {
	  it = i+y_start;
	  it = (it < 0) ? 0 : it;
	  it = (it >= ony) ? ony-1 : it;
	  for (j = 0; j < donx ; j++)
	    {
	      jt = j+x_start;
	      jt = (jt < 0) ? 0 : jt;
	      jt = (jt >= onx) ? onx-1 : jt;
	      pd[i].db[j] = ps[it].db[jt];
	    }
        }
    }
  else if (ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
      for (i = 0; i< dony ; i++)
        {
	  it = i+y_start;
	  it = (it < 0) ? 0 : it;
	  it = (it >= ony) ? ony-1 : it;
	  for (j = 0; j < donx ; j++)
	    {
	      jt = j+x_start;
	      jt = (jt < 0) ? 0 : jt;
	      jt = (jt >= onx) ? onx-1 : jt;
	      pd[i].dcp[j] = ps[it].dcp[jt];
	    }
        }
    }
  return 0;
}





/*
 *      -ask_movie is a flag, if set and ois is a movie and dest = NULL the function will ask the user to
 *      either treat all movie or a single image. If set to 0 treat all movie.
 *	"lp" is the 1/2 low pass cut_off, "hp" is the high pass cutt_off
 *	gain is 0 at f < "lp - lw" and f > "hp + hw"
 *	gain is 1 at "lp + lw" < f < "hp - hw"
 */

O_i 	*band_pass_filter_fft_2d_im(O_i *dest, O_i *ois, int ask_movie, float lp, float lw, float hp, float hw, int amp_save_in_user_spare, int house_keeping)
{
    int i, j, ims;
    int  onx, ony, nfi, nfo, ia;
    float a, re, im;
    float k, hma = hp + hw, hmi = hp - hw, lma = lp + lw, lmi = lp - lw;
    //union pix *pd;//, *ps;
    O_i  *oid = NULL;
    static int curim = 0, same_im = 1;


    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;  nfi = abs(ois->im.n_f);// cf = ois->im.c_f;
    nfo = nfi = (nfi == 0) ? 1 : nfi;
    if (ois->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }

    if (dest != NULL)		/*	if dest exists check that destination is OK */
    {
        if (dest->im.data_type != IS_COMPLEX_IMAGE ||
            dest->im.nx != onx || dest->im.ny != ony)
        {
            free_one_image(dest);
            dest = NULL;
        }
    }
    if (dest != NULL)
    {
        nfo = abs(dest->im.n_f);
        nfo = (nfo == 0) ? 1 : nfo;
        oid = dest;
    }
    else
    {
        if (nfi < 2)	nfo = 1;
        else
        {
            if (ask_movie)
            {
  	        double size = onx*ony;
		char message[256] = {0};
		size *= nfo;
		size *= 8;
		size /= 1024*1024;
		snprintf(message,sizeof(message),"You are filtering the FFT of a movie!\n"
			 "%%RApply filtering on all image (%8.2f Mo)\n"
			 "%%rApply filtering only on the current image\n"
			 "%%bModify the input image\n"
                         ,size);
                i = win_scanf(message,&curim,&same_im);
		if (i == WIN_CANCEL) return 0;
		nfo = (curim)  ? 1 : nfi;
            }
            else nfo = nfi;
        }
    }
    if (dest == NULL)
    {
        if (same_im) oid = ois;
        else if (nfo < 2) oid = create_one_image(onx, ony, IS_COMPLEX_IMAGE);
        else oid = create_one_movie(onx, ony, IS_COMPLEX_IMAGE, nfo);
    }

    if (oid == NULL || oid->im.nx != onx || oid->im.ny != ony)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }
    if (amp_save_in_user_spare >= oid->im.user_nfpar)
        amp_save_in_user_spare = -1;
    for (ims = 0; amp_save_in_user_spare >= 0 && ims < nfo; ims++)
        oid->im.user_fspare[ims][amp_save_in_user_spare] = 0;

    //  pd = oid->im.pixel;	ps = ois->im.pixel;
    for (j=1; j< ony; j++)
    {
        for (i = -onx/2; i< onx/2 ; i++)
        {
            ia = amp_save_in_user_spare;
            k = sqrt ( (double)j*j + (double)i*i);
            if (k <= hmi && k >= lma)	a = 1;
            else	if (k < hma && k > hmi)	a = (1 - sin(M_PI_2*(k-hp)/hw))/2;
            else	if (k < lma && k > lmi)	a = (1 + sin(M_PI_2*(k-lp)/lw))/2;
            else				{a = 0; ia = -1;}
            for (ims = 0; ims < nfo; ims++)
            {
                re = oid->im.pxl[ims][j].fl[onx+2*i] = a * ois->im.pxl[ims][j].fl[onx+2*i];
                im = oid->im.pxl[ims][j].fl[onx+2*i+1] = a * ois->im.pxl[ims][j].fl[onx+2*i+1];
                if (ia >= 0)	    oid->im.user_fspare[ims][ia] += re * re + im * im;
                //my_set_window_title("im %d j %d i %d",ims,j,i);
            }
        }
    }
    for (i =0; i< onx/2 ; i++)
    {
        ia = amp_save_in_user_spare;
        k = sqrt ((double)i*i);
        if (k <= hmi && k >= lma)		a = 1;
        else	if (k < hma && k > hmi)	a = (1 - sin(M_PI_2*(k-hp)/hw))/2;
        else	if (k < lma && k > lmi)	a = (1 + sin(M_PI_2*(k-lp)/lw))/2;
        else				{a = 0; ia = -1;}
        for (ims = 0; ims < nfo; ims++)
        {
            re = oid->im.pxl[ims][0].fl[onx+2*i] = a * ois->im.pxl[ims][0].fl[onx+2*i];
            im = oid->im.pxl[ims][0].fl[onx+2*i+1] = a * ois->im.pxl[ims][0].fl[onx+2*i+1];
            if (ia >= 0)	    oid->im.user_fspare[ims][ia] += re * re + im * im;
            oid->im.pxl[ims][0].fl[2*i] = oid->im.pxl[ims][0].fl[2*i+1] = 0;
            //my_set_window_title("im %d i %d",ims,i);
        }
    }
    if (house_keeping) inherit_from_im_to_im(oid,ois);
    oid->im.mode = LOG_AMP;
    if (oid != ois)
      {
	set_im_x_title(oid,"wavenumber k_x");
	set_im_y_title(oid,"frequency \\omega");
	uns_oi_2_oi(oid,ois);
      }
    oid->ax = -onx/2;
    oid->im.win_flag = ois->im.win_flag;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    set_im_title(oid, "\\stack{{filtred fft 2D of image}{%s}}",
                 (ois->title != NULL) ? ois->title : ((ois->im.source != NULL) ?
                                                      ois->im.source : "untitled"));
    update_oi_treatment(oid,"sloppy fft2d image of %s band pas %f %f %f %f",
                        (ois->title != NULL) ? ois->title : ((ois->im.source != NULL)
                         ? ois->im.source : "untitled"),lmi,lma,hmi,hma);
    return oid;
}

/*
 *      -ask_movie is a flag, if set and ois is a movie and dest = NULL the function will ask the user to
 *      either treat all movie or a single image. If set to 0 treat all movie.
 *	"lp" is the 1/2 low pass cut_off, "hp" is the high pass cutt_off
 *	gain is 0 at f < "lp - lw" and f > "hp + hw"
 *	gain is 1 at "lp + lw" < f < "hp - hw"
 */

O_i 	*derivative_filter_fft_2d_im(O_i *dest, O_i *ois, int ask_movie, float lp, float lw, float hp, float hw, int amp_save_in_user_spare, int house_keeping)
{
    int i, j, ims;
    int  onx, ony, nfi, nfo, ia;
    float a, re, im;
    float k, hma = hp + hw, hmi = hp - hw, lma = lp + lw, lmi = lp - lw;
    //union pix *pd;//, *ps;
    O_i  *oid = NULL;


    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;  nfi = abs(ois->im.n_f);// cf = ois->im.c_f;
    nfi = (nfi == 0) ? 1 : nfi;
    if (ois->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }

    if (dest != NULL)		/*	if dest exists check that destination is OK */
    {
        if (dest->im.data_type != IS_COMPLEX_IMAGE ||
            dest->im.nx != onx || dest->im.ny != ony)
        {
            free_one_image(dest);
            dest = NULL;
        }
    }
    if (dest != NULL)
    {
        nfo = abs(dest->im.n_f);
        nfo = (nfo == 0) ? 1 : nfo;
        oid = dest;
    }
    else
    {
        if (nfi < 2)	nfo = 1;
        else
        {
            if (ask_movie)
            {
                i = win_scanf("You are filtering the FFT of a movie!\n"
                              "To apply filtering on all image press OK\n"
                              "To apply filtering only on the current image\n"
                              "Press CANCEL");
                if (i == WIN_CANCEL)	nfo = 1;
                else 		nfo = nfi;
            }
            else nfo = nfi;
        }
    }
    if (dest == NULL)
    {
        if (nfo < 2) oid = create_one_image(onx, ony, IS_COMPLEX_IMAGE);
        else         oid = create_one_movie(onx, ony, IS_COMPLEX_IMAGE, nfo);
    }

    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }
    if (amp_save_in_user_spare >= oid->im.user_nfpar)
        amp_save_in_user_spare = -1;
    for (ims = 0; amp_save_in_user_spare >= 0 && ims < nfo; ims++)
        oid->im.user_fspare[ims][amp_save_in_user_spare] = 0;

    //  pd = oid->im.pixel;	ps = ois->im.pixel;
    for (j=1; j< ony; j++)
    {
        for (i = -onx/2; i< onx/2 ; i++)
        {
            ia = amp_save_in_user_spare;
            k = sqrt ( (double)j*j + (double)i*i);
            if (k <= hmi && k >= lma)	a = 1;
            else	if (k < hma && k > hmi)	a = (1 - sin(M_PI_2*(k-hp)/hw))/2;
            else	if (k < lma && k > lmi)	a = (1 + sin(M_PI_2*(k-lp)/lw))/2;
            else				{a = 0; ia = -1;}
            for (ims = 0; ims < nfo; ims++)
            {
                re = oid->im.pxl[ims][j].fl[onx+2*i] = a * ois->im.pxl[ims][j].fl[onx+2*i];
                im = oid->im.pxl[ims][j].fl[onx+2*i+1] = a * ois->im.pxl[ims][j].fl[onx+2*i+1];
                if (ia >= 0)	    oid->im.user_fspare[ims][ia] += re * re + im * im;
                //my_set_window_title("im %d j %d i %d",ims,j,i);
            }
        }
    }
    for (i =0; i< onx/2 ; i++)
    {
        ia = amp_save_in_user_spare;
        k = sqrt ((double)i*i);
        if (k <= hmi && k >= lma)		a = 1;
        else	if (k < hma && k > hmi)	a = (1 - sin(M_PI_2*(k-hp)/hw))/2;
        else	if (k < lma && k > lmi)	a = (1 + sin(M_PI_2*(k-lp)/lw))/2;
        else				{a = 0; ia = -1;}
        for (ims = 0; ims < nfo; ims++)
        {
            re = oid->im.pxl[ims][0].fl[onx+2*i] = a * ois->im.pxl[ims][0].fl[onx+2*i];
            im = oid->im.pxl[ims][0].fl[onx+2*i+1] = a * ois->im.pxl[ims][0].fl[onx+2*i+1];
            if (ia >= 0)	    oid->im.user_fspare[ims][ia] += re * re + im * im;
            oid->im.pxl[ims][0].fl[2*i] = oid->im.pxl[ims][0].fl[2*i+1] = 0;
            //my_set_window_title("im %d i %d",ims,i);
        }
    }
    if (house_keeping) inherit_from_im_to_im(oid,ois);
    oid->im.mode = LOG_AMP;
    set_im_x_title(oid,"wavenumber k_x");
    set_im_y_title(oid,"frequency \\omega");
    uns_oi_2_oi(oid,ois);
    oid->ax = -onx/2;
    oid->im.win_flag = ois->im.win_flag;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    set_im_title(oid, "\\stack{{filtred fft 2D of image}{%s}}",
                 (ois->title != NULL) ? ois->title : ((ois->im.source != NULL) ?
                                                      ois->im.source : "untitled"));
    set_formated_string(&oid->im.treatement,
                        "sloppy fft2d image of %s band pas %f %f %f %f",
                        (ois->title != NULL) ? ois->title : ((ois->im.source != NULL)
                                                             ? ois->im.source : "untitled"),lmi,lma,hmi,hma);
    return oid;
}




/*	compute the forward fft of a real image
 *	-dest is the result, if dest == NULL then it is created
 *	-ois is the source image
 *	-bmp is the bitmap to print message
 *		if NULL, no message print during process
 *	-smp is a small number tipically .05 in the Hanning window
 *		if smp = 0 the reverse operation is impossible
 */
O_i 	*forward_fft_2d_im(O_i *dest, O_i *ois, BITMAP *bmp, float smp)
{
    int i, j;
    int nout = 0, onx, ony, no;
    float *tmp;
    union pix *pd;
    O_i  *oid;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;
    if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        win_printf ("Cannot treat a complex image yet!");
        return(NULL);
    }
    if (dest != NULL)		/*	if dest exists check that destination is OK */
    {
        if (dest->im.data_type != IS_COMPLEX_IMAGE ||
            dest->im.nx != onx || dest->im.ny != ony/2)
        {
            free_one_image(dest);
            dest = NULL;
        }
    }
    oid = (dest != NULL) ? dest : create_one_image(onx, ony/2, IS_COMPLEX_IMAGE);
    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }

    pd = oid->im.pixel;
    tmp = (float*)calloc(ony,sizeof(float));	/* fft on row */
    if ( tmp == NULL )
    {
        win_printf ("Can't create tmp");
        return (NULL);
    }
    if (fft_init(ony))
    {
        free(tmp);
        free_one_image(oid);
        win_printf("Can't initialize fft");
        return (NULL);
    }
    for (j=0; j< ois->im.nx; j++)
    {
        no = ony * j;

        if ((bmp != NULL) && (no >= nout + 16384))
        {	/* say how much done ~ every second*/
            display_title_message("fft row %d",j);
        }
        extract_raw_row (ois, j, tmp);
        if (!(ois->im.win_flag & Y_PER))	fftwindow1(ony, tmp, smp);
        realtr1(ony, tmp);
        fft(ony, tmp, 1);
        realtr2(ony, tmp, 1);
        for (i = 0; i< ony/2 ; i++)
        {
            pd[i].fl[2*j] = tmp[2*i];		/* real part */
            pd[i].fl[2*j+1] = tmp[2*i+1];	/* imaginary part */
        }
    }
    if (fft_init(onx) || fft_init(2*onx)) 	/* fft on line */
    {
        free(tmp);
        free_one_image(oid);
        win_printf("Can't initialize fft");
        return (NULL);
    }
    tmp = (float*)realloc(tmp,2*onx*sizeof(float));
    if ( tmp == NULL )
    {
        free_one_image(oid);
        win_printf ("Can't create tmp");
        return (NULL);
    }
    for (j=1, nout = 0; j< ony/2; j++)	/* ! start on mode k = 1	*/
    {
        no = 2*onx * j;
        if ((bmp != NULL) && (no >= nout + 16384))
        {	/* say how much done ~ every second*/
            display_title_message("fft line %d",j);
        }

        for (i = 0; i< onx ; i++)
        {
            tmp[2*i] = pd[j].fl[2*i];
            tmp[2*i+1] = pd[j].fl[2*i+1];
        }
        if (!(ois->im.win_flag & X_PER))	fftwc1(2*onx, tmp,  smp);
        fft(2*onx, tmp, 1);
        for (i = 0; i< onx/2 ; i++)		/* order from -Nx/2 to Nx/2 */
        {
            pd[j].fl[onx+2*i] = tmp[2*i];
            pd[j].fl[onx+2*i+1] = tmp[2*i+1];
            pd[j].fl[2*i] = tmp[onx+2*i];
            pd[j].fl[2*i+1] = tmp[onx+2*i+1];
        }
    }
    fft_init(onx);		/* treat ky = 0 and ky = Ny/2 */
    for (i = 0; i< onx ; i++)
    {
        tmp[onx+i] = pd[0].fl[2*i];	/* all real part ky = 0		*/
        tmp[i] = pd[0].fl[2*i+1];	/* all real part ky = ny/2 	*/
    }
    if (!(ois->im.win_flag & X_PER))
    {
        fftwc1(onx, tmp,  smp);
        fftwc1(onx, tmp + onx,  smp);
    }
    realtr1(onx, tmp);
    fft(onx, tmp, 1);
    realtr2(onx, tmp, 1);
    realtr1(onx, tmp + onx);
    fft(onx, tmp + onx, 1);
    realtr2(onx, tmp + onx, 1);
    for (i = 0; i< onx ; i++)
    {
        pd[0].fl[onx + i] 	= tmp[onx+i];	/* all real part ky = 0		*/
        pd[0].fl[i] 		= tmp[i];		/* all real part ky = ny/2 	*/
    }
    /*	the ky = 0 mode are nearly well ordered except (kx = 0, kx = Nx/2) */
    /*	the ky = Ny/2 mode are ordered in inverse order */
    free(tmp);
    inherit_from_im_to_im(oid,ois);
    uns_oi_2_oi(oid,ois);
    /*
       un = build_unit_set(0, xs, 1, 0, 0, NULL);
       if (un == NULL)		return 1;
       add_to_one_image (oid, IS_X_UNIT_SET, (void *)un);
       un = build_unit_set(0, ys, 1, 0, 0, NULL);
       if (un == NULL)		return 1;
       add_to_one_image (oid, IS_Y_UNIT_SET, (void *)un);
       oid->c_xu = oid->n_xu - 1;
       oid->c_yu = oid->n_yu - 1;
       */
    oid->im.win_flag = ois->im.win_flag;
    oid->im.mode = LOG_AMP;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    oid->ax = -onx/2;
    set_im_x_title(oid,"wavenumber k_x");
    set_im_y_title(oid,"frequency \\omega");
    set_im_title(oid, "\\stack{{fft 2D of image}{%s}}",(ois->title != NULL)
                 ? ois->title : ((ois->im.source != NULL) ? ois->im.source : "untitled"));
    set_formated_string(&oid->im.treatement,"sloppy fft2d image of %s",
                        (ois->title != NULL) ? ois->title : ((ois->im.source != NULL)
                                                             ? ois->im.source : "untitled"));
    return oid;
}



/*	compute the forward fft of a real image
 *	-dest is the result, if dest == NULL then it is created
 *	-ois is the source image
 *      -ask_movie is a flag, if set and ois is a movie and dest = NULL the function will ask the user to
 *      either treat all movie or a single image. If set to 0 treat all movie.
 *	-bmp is the bitmap to print message
 *		if NULL, no message print during process
 *	-smp is a small number tipically .05 in the Hanning window
 *		if smp = 0 the reverse operation is impossible
 */
O_i 	*forward_fftbt_2d_im(O_i *dest, O_i *ois, int ask_movie, BITMAP *bmp, float smp)
{
    int i, j;
    int nout = 0, onx, ony, no, nfi = 0, nfo = 0, cf, im;
    union pix *pd;
    O_i  *oid = NULL;
    static fft_plan *fpy = NULL, *fpx = NULL, *fpx2 = NULL;
    static float *tmpx = NULL, *tmpy = NULL;
    static int ntmpx = 0, ntmpy = 0;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;  nfi = abs(ois->im.n_f); cf = ois->im.c_f;
    nfi = (nfi == 0) ? 1 : nfi;
    if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        win_printf ("Cannot treat a complex image yet!");
        return(NULL);
    }
    if (dest != NULL)		/*	if dest exists check that destination is OK */
    {
        if (dest->im.data_type != IS_COMPLEX_IMAGE ||
            dest->im.nx != onx || dest->im.ny != ony/2)
        {
            free_one_image(dest);
            dest = NULL;
        }
    }
    if (dest != NULL)
    {
        nfo = abs(dest->im.n_f);
        nfo = (nfo == 0) ? 1 : nfo;
        oid = dest;
    }
    else
    {
        if (nfi < 2)	nfo = 1;
        else
        {
            if (ask_movie)
            {
                i = win_scanf("You are taking the FFT of a movie!\n"
                              "To apply FFT on all image press OK\n"
                              "To apply FFT only on the current image\n"
                              "Press CANCEL");
                if (i == WIN_CANCEL)	nfo = 1;
                else 		nfo = nfi;
            }
            else nfo = nfi;
        }
    }
    if (dest == NULL)
    {
        if (nfo < 2) oid = create_one_image(onx, ony/2, IS_COMPLEX_IMAGE);
        else         oid = create_one_movie(onx, ony/2, IS_COMPLEX_IMAGE, nfo);
    }
    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }

    fpy = fftbt_init(fpy, ony);
    fpx = fftbt_init(fpx,onx);
    fpx2 = fftbt_init(fpx2, 2*onx);
    if (fpy == NULL || fpx2 == NULL || fpx == NULL)
    {
        win_printf("Can't initialize fft nx %d ny %d",onx, ony);
        return(NULL);
    }
    if (tmpx == NULL || ntmpx != 2*onx)
    {
        tmpx = (float*)realloc(tmpx,2*onx*sizeof(float));
        if ( tmpx == NULL )
        {
            win_printf ("Can't create tmpx");
            return (NULL);
        }
        ntmpx = 2*onx;
    }
    if (tmpy == NULL || ntmpy != ony)
    {
        tmpy = (float*)realloc(tmpy,ony*sizeof(float));
        if ( tmpy == NULL )
        {
            win_printf ("Can't create tmpy");
            return (NULL);
        }
        ntmpy = ony;
    }
    for (im = 0; im < nfo; im++)
    {
        if (nfo > 1)
        {
            switch_frame(ois,im);
            switch_frame(oid,im);
        }
        pd = oid->im.pixel;
        /* fft on row */
        for (j=0; j< ois->im.nx; j++)
        {
            no = ony * j;

            if ((bmp != NULL) && (no >= nout + 16384))
            {	/* say how much done ~ every second*/
                display_title_message("fft row %d",j);
            }
            extract_raw_row (ois, j, tmpy);
            if (!(ois->im.win_flag & Y_PER))	fftwindow1(ony, tmpy, smp);
            realtr1bt(fpy, tmpy);
            fftbt(fpy, tmpy, 1);
            realtr2bt(fpy, tmpy, 1);
            for (i = 0; i< ony/2 ; i++)
            {
                pd[i].fl[2*j] = tmpy[2*i];		/* real part */
                pd[i].fl[2*j+1] = tmpy[2*i+1];	/* imaginary part */
            }
        }
        /* fft on line */
        for (j=1, nout = 0; j< ony/2; j++)	/* ! start on mode k = 1	*/
        {
            no = 2*onx * j;
            if ((bmp != NULL) && (no >= nout + 16384))
            {	/* say how much done ~ every second*/
                display_title_message("fft line %d",j);
            }

            for (i = 0; i< onx ; i++)
            {
                tmpx[2*i] = pd[j].fl[2*i];
                tmpx[2*i+1] = pd[j].fl[2*i+1];
            }
            if (!(ois->im.win_flag & X_PER))	fftbtwc1(fpx2, tmpx,  smp);
            fftbt(fpx2, tmpx, 1);
            for (i = 0; i< onx/2 ; i++)		/* order from -Nx/2 to Nx/2 */
            {
                pd[j].fl[onx+2*i] = tmpx[2*i];
                pd[j].fl[onx+2*i+1] = tmpx[2*i+1];
                pd[j].fl[2*i] = tmpx[onx+2*i];
                pd[j].fl[2*i+1] = tmpx[onx+2*i+1];
            }
        }
        for (i = 0; i< onx ; i++)
        {
            tmpx[onx+i] = pd[0].fl[2*i];	/* all real part ky = 0		*/
            tmpx[i] = pd[0].fl[2*i+1];	/* all real part ky = ny/2 	*/
        }
        if (!(ois->im.win_flag & X_PER))
        {
            fftbtwc1(fpx, tmpx,  smp);
            fftbtwc1(fpx, tmpx + onx,  smp);
        }
        realtr1bt(fpx, tmpx);
        fftbt(fpx, tmpx, 1);
        realtr2bt(fpx, tmpx, 1);
        realtr1bt(fpx, tmpx + onx);
        fftbt(fpx, tmpx + onx, 1);
        realtr2bt(fpx, tmpx + onx, 1);
        for (i = 0; i< onx ; i++)
        {
            pd[0].fl[onx + i] 	= tmpx[onx+i];	/* all real part ky = 0		*/
            pd[0].fl[i] 		= tmpx[i];		/* all real part ky = ny/2 	*/
        }
    }
    /*	the ky = 0 mode are nearly well ordered except (kx = 0, kx = Nx/2) */
    /*	the ky = Ny/2 mode are ordered in inverse order */
    switch_frame(ois,cf);  // we restore current image
    if (nfo > 1) switch_frame(oid,cf);
    inherit_from_im_to_im(oid,ois);
    uns_oi_2_oi(oid,ois);
    /*
       un = build_unit_set(0, xs, 1, 0, 0, NULL);
       if (un == NULL)		return 1;
       add_to_one_image (oid, IS_X_UNIT_SET, (void *)un);
       un = build_unit_set(0, ys, 1, 0, 0, NULL);
       if (un == NULL)		return 1;
       add_to_one_image (oid, IS_Y_UNIT_SET, (void *)un);
       oid->c_xu = oid->n_xu - 1;
       oid->c_yu = oid->n_yu - 1;
       */
    oid->im.win_flag = ois->im.win_flag;
    oid->im.mode = LOG_AMP;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    oid->ax = -onx/2;
    set_im_x_title(oid,"wavenumber k_x");
    set_im_y_title(oid,"frequency \\omega");
    set_im_title(oid, "\\stack{{fft 2D of image}{%s}}",(ois->title != NULL)
                 ? ois->title : ((ois->im.source != NULL) ? ois->im.source : "untitled"));
    set_formated_string(&oid->im.treatement,"sloppy fft2d image of %s",
                        (ois->title != NULL) ? ois->title : ((ois->im.source != NULL)
                                                             ? ois->im.source : "untitled"));
    return oid;
}




/*	compute the forward fft of a real image
 *	-dest is the result, if dest == NULL then it is created
 *	-ois is the source image
 *      -ask_movie is a flag, if set and ois is a movie and dest = NULL the function will ask the user to
 *      either treat all movie or a single image. If set to 0 treat all movie.
 *	-bmp is the bitmap to print message
 *		if NULL, no message print during process
 *	-smp is a small number tipically .05 in the Hanning window
 *		if smp = 0 the reverse operation is impossible
 *       mean the offset to substract
 */
O_i 	*forward_fftbt_2d_im_minus_mean(O_i *dest, O_i *ois, int ask_movie, BITMAP *bmp, float smp)
{
    int i, j;
    int nout = 0, onx, ony, no, nfi = 0, nfo = 0, cf, im;
    float mean = 0;
    union pix *pd;
    O_i  *oid = NULL;
    static fft_plan *fpy = NULL, *fpx = NULL, *fpx2 = NULL;
    static float *tmpx = NULL, *tmpy = NULL;
    static int ntmpx = 0, ntmpy = 0;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;  nfi = abs(ois->im.n_f); cf = ois->im.c_f;
    nfi = (nfi == 0) ? 1 : nfi;
    if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        win_printf ("Cannot treat a complex image yet!");
        return(NULL);
    }
    if (dest != NULL)		/*	if dest exists check that destination is OK */
    {
        if (dest->im.data_type != IS_COMPLEX_IMAGE ||
            dest->im.nx != onx || dest->im.ny != ony/2)
        {
            free_one_image(dest);
            dest = NULL;
        }
    }
    if (dest != NULL)
    {
        nfo = abs(dest->im.n_f);
        nfo = (nfo == 0) ? 1 : nfo;
        oid = dest;
    }
    else
    {
        if (nfi < 2)	nfo = 1;
        else
        {
            if (ask_movie)
            {
                i = win_scanf("You are taking the FFT of a movie!\n"
                              "To apply FFT on all image press OK\n"
                              "To apply FFT only on the current image\n"
                              "Press CANCEL");
                if (i == WIN_CANCEL)	nfo = 1;
                else 		nfo = nfi;
            }
            else nfo = nfi;
        }
    }
    if (dest == NULL)
    {
        if (nfo < 2) oid = create_one_image(onx, ony/2, IS_COMPLEX_IMAGE);
        else         oid = create_one_movie(onx, ony/2, IS_COMPLEX_IMAGE, nfo);
    }
    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }

    fpy = fftbt_init(fpy, ony);
    fpx = fftbt_init(fpx,onx);
    fpx2 = fftbt_init(fpx2, 2*onx);
    if (fpy == NULL || fpx2 == NULL || fpx == NULL)
    {
        win_printf("Can't initialize fft nx %d ny %d",onx, ony);
        return(NULL);
    }
    if (tmpx == NULL || ntmpx != 2*onx)
    {
        tmpx = (float*)realloc(tmpx,2*onx*sizeof(float));
        if ( tmpx == NULL )
        {
            win_printf ("Can't create tmpx");
            return (NULL);
        }
        ntmpx = 2*onx;
    }
    if (tmpy == NULL || ntmpy != ony)
    {
        tmpy = (float*)realloc(tmpy,ony*sizeof(float));
        if ( tmpy == NULL )
        {
            win_printf ("Can't create tmpy");
            return (NULL);
        }
        ntmpy = ony;
    }
    for (im = 0; im < nfo; im++)
    {
        if (nfo > 1)
        {
            switch_frame(ois,im);
            switch_frame(oid,im);
        }
        pd = oid->im.pixel;
        /* find mean value */
        for (j=0, mean = 0; j< ois->im.nx; j++)
        {
            extract_raw_row (ois, j, tmpy);
            for (i = 0; i< ony ; i++)
                    mean += tmpy[i];
        }
        mean /= ((ois->im.nx * ois->im.ny) > 0) ? ois->im.nx * ois->im.ny : 1;
        /* fft on row */
        for (j=0; j< ois->im.nx; j++)
        {
            no = ony * j;

            if ((bmp != NULL) && (no >= nout + 16384))
            {	/* say how much done ~ every second*/
                display_title_message("fft row %d",j);
            }
            extract_raw_row (ois, j, tmpy);
            for (i = 0; i< ony ; i++) tmpy[i] -= mean;
            if (!(ois->im.win_flag & Y_PER))	fftwindow1(ony, tmpy, smp);
            realtr1bt(fpy, tmpy);
            fftbt(fpy, tmpy, 1);
            realtr2bt(fpy, tmpy, 1);
            for (i = 0; i< ony/2 ; i++)
            {
                pd[i].fl[2*j] = tmpy[2*i];		/* real part */
                pd[i].fl[2*j+1] = tmpy[2*i+1];	/* imaginary part */
            }
        }
        /* fft on line */
        for (j=1, nout = 0; j< ony/2; j++)	/* ! start on mode k = 1	*/
        {
            no = 2*onx * j;
            if ((bmp != NULL) && (no >= nout + 16384))
            {	/* say how much done ~ every second*/
                display_title_message("fft line %d",j);
            }

            for (i = 0; i< onx ; i++)
            {
                tmpx[2*i] = pd[j].fl[2*i];
                tmpx[2*i+1] = pd[j].fl[2*i+1];
            }
            if (!(ois->im.win_flag & X_PER))	fftbtwc1(fpx2, tmpx,  smp);
            fftbt(fpx2, tmpx, 1);
            for (i = 0; i< onx/2 ; i++)		/* order from -Nx/2 to Nx/2 */
            {
                pd[j].fl[onx+2*i] = tmpx[2*i];
                pd[j].fl[onx+2*i+1] = tmpx[2*i+1];
                pd[j].fl[2*i] = tmpx[onx+2*i];
                pd[j].fl[2*i+1] = tmpx[onx+2*i+1];
            }
        }
        for (i = 0; i< onx ; i++)
        {
            tmpx[onx+i] = pd[0].fl[2*i];	/* all real part ky = 0		*/
            tmpx[i] = pd[0].fl[2*i+1];	/* all real part ky = ny/2 	*/
        }
        if (!(ois->im.win_flag & X_PER))
        {
            fftbtwc1(fpx, tmpx,  smp);
            fftbtwc1(fpx, tmpx + onx,  smp);
        }
        realtr1bt(fpx, tmpx);
        fftbt(fpx, tmpx, 1);
        realtr2bt(fpx, tmpx, 1);
        realtr1bt(fpx, tmpx + onx);
        fftbt(fpx, tmpx + onx, 1);
        realtr2bt(fpx, tmpx + onx, 1);
        for (i = 0; i< onx ; i++)
        {
            pd[0].fl[onx + i] 	= tmpx[onx+i];	/* all real part ky = 0		*/
            pd[0].fl[i] 		= tmpx[i];		/* all real part ky = ny/2 	*/
        }
    }
    /*	the ky = 0 mode are nearly well ordered except (kx = 0, kx = Nx/2) */
    /*	the ky = Ny/2 mode are ordered in inverse order */
    switch_frame(ois,cf);  // we restore current image
    if (nfo > 1) switch_frame(oid,cf);
    inherit_from_im_to_im(oid,ois);
    uns_oi_2_oi(oid,ois);
    /*
       un = build_unit_set(0, xs, 1, 0, 0, NULL);
       if (un == NULL)		return 1;
       add_to_one_image (oid, IS_X_UNIT_SET, (void *)un);
       un = build_unit_set(0, ys, 1, 0, 0, NULL);
       if (un == NULL)		return 1;
       add_to_one_image (oid, IS_Y_UNIT_SET, (void *)un);
       oid->c_xu = oid->n_xu - 1;
       oid->c_yu = oid->n_yu - 1;
       */
    oid->im.win_flag = ois->im.win_flag;
    oid->im.mode = LOG_AMP;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    oid->ax = -onx/2;
    set_im_x_title(oid,"wavenumber k_x");
    set_im_y_title(oid,"frequency \\omega");
    set_im_title(oid, "\\stack{{fft 2D of image}{%s}{minus mean value %g}}",(ois->title != NULL)
                 ? ois->title : ((ois->im.source != NULL) ? ois->im.source : "untitled"), mean);
    set_formated_string(&oid->im.treatement,"sloppy fft2d image of %s minus mean value %g",
                        (ois->title != NULL) ? ois->title : ((ois->im.source != NULL)
                                                             ? ois->im.source : "untitled"),mean);
    return oid;
}


/*----------------------------------------------------------------
 *	compute the backward fft of a real image
 *	-dest is the result, if dest == NULL then it is created
 *	-ois is the source image
 *      -ask_movie is a flag, if set and ois is a movie and dest = NULL the function will ask the user to
 *      either treat all movie or a single image. If set to 0 treat all movie.
 *	-bmp is the BITMAP to print message
 *		if NULL, no message print during process
 *	-smp is a small number tipically .05 in the Hanning window
 *		if smp = 0 operation is impossible
 *-----------------------------------------------------------------
 */
O_i 	*backward_fftbt_2d_im(O_i *dest, O_i *ois, int ask_movie, BITMAP *bmp, float smp)
{
    int i, j;
    int nout = 0, onx, ony, no, nfi, nfo, cf, im;
    union pix *pd, *ps;
    O_i  *oid = NULL;
    static fft_plan *fpy2 = NULL, *fpx = NULL, *fpx2 = NULL;
    static float *tmpx = NULL, *tmpy = NULL;
    static int ntmpx = 0, ntmpy = 0;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;  nfi = abs(ois->im.n_f); cf = ois->im.c_f;
    nfi = (nfi == 0) ? 1 : nfi;
    if (ois->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if (dest != NULL)
    {
        if (dest->im.data_type != IS_FLOAT_IMAGE ||
            dest->im.nx != onx || dest->im.ny != ony*2)
        {
            free_one_image(dest);
            dest = NULL;
        }
    }
    if (dest != NULL)
    {
        nfo = abs(dest->im.n_f);
        nfo = (nfo == 0) ? 1 : nfo;
        oid = dest;
    }
    else
    {
        if (nfi < 2)	nfo = 1;
        else
        {
            if (ask_movie)
            {
                i = win_scanf("You are taking the FFT^{-1} of a movie!\n"
                              "To apply FFT on all image press OK\n"
                              "To apply FFT only on the current image\n"
                              "Press CANCEL");
                if (i == WIN_CANCEL)	nfo = 1;
                else 		nfo = nfi;
            }
            else nfo = nfi;
        }
    }
    if (dest == NULL)
    {
        if (nfo < 2) oid = create_one_image(onx, 2*ony, IS_FLOAT_IMAGE);
        else         oid = create_one_movie(onx, 2*ony, IS_FLOAT_IMAGE, nfo);
    }



    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }

    fpy2 = fftbt_init(fpy2, 2*ony);
    fpx = fftbt_init(fpx,onx);
    fpx2 = fftbt_init(fpx2, 2*onx);
    if (fpy2 == NULL || fpx2 == NULL || fpx == NULL)
    {
        win_printf("Can't initialize fft nx %d ny %d",onx, ony);
        return(NULL);
    }
    if (tmpx == NULL || ntmpx != 2*onx)
    {
        tmpx = (float*)realloc(tmpx,2*onx*sizeof(float));
        if ( tmpx == NULL )
        {
            win_printf ("Can't create tmpx");
            return (NULL);
        }
        ntmpx = 2*onx;
    }
    if (tmpy == NULL || ntmpy != 2*ony)
    {
        tmpy = (float*)realloc(tmpy,2*ony*sizeof(float));
        if ( tmpy == NULL )
        {
            win_printf ("Can't create tmpy");
            return (NULL);
        }
        ntmpy = 2*ony;
    }
    for (im = 0; im < nfo; im++)
    {
        if (nfo > 1)
        {
            switch_frame(ois,im);
            switch_frame(oid,im);
        }
        pd = oid->im.pixel;	ps = ois->im.pixel;
        /* treat ky = 0 and ky = Ny/2 */
        for (i = 0; i< 2*onx ; i++)	tmpx[i] = ps[0].fl[i];
        realtr2bt(fpx, tmpx, -1);
        fftbt(fpx, tmpx, -1);
        realtr1bt(fpx, tmpx);
        realtr2bt(fpx, tmpx + onx, -1);
        fftbt(fpx, tmpx + onx, -1);
        realtr1bt(fpx, tmpx + onx);
        if (!(ois->im.win_flag & X_PER))
        {
            defftbtwc1(fpx, tmpx,  smp);
            defftbtwc1(fpx, tmpx + onx,  smp);
        }
        for (i = 0; i< onx ; i++)
        {
            pd[0].fl[i] = tmpx[onx+i];	/* all real part ky = 0		*/
            pd[1].fl[i] = tmpx[i];	/* all real part ky = ny/2 	*/
        }
        /* now treat normal mode */
        for (j=1, nout = 0; j< ony; j++)
        {
            no = 2*onx * j;
            if ((bmp != NULL) && (no >= nout + 16384))
            {	/* say how much done ~ every second*/
                display_title_message("fft line %d",j);
            }
            for (i = 0; i< onx/2 ; i++)
            {
                tmpx[2*i] = ps[j].fl[onx+2*i];
                tmpx[2*i+1] = ps[j].fl[onx+2*i+1];
                tmpx[onx+2*i] = ps[j].fl[2*i];
                tmpx[onx+2*i+1] = ps[j].fl[2*i+1];
            }
            fftbt(fpx2, tmpx, -1);
            if (!(ois->im.win_flag & X_PER))	defftbtwc1(fpx2, tmpx,  smp);
            for (i = 0; i< onx ; i++)
            {
                pd[2*j].fl[i] = tmpx[2*i];
                pd[2*j+1].fl[i] = tmpx[2*i+1];
            }

        }
        for (j=0; j< ois->im.nx; j++)
        {
            no = ony * j;
            if ((bmp != NULL) && (no >= nout + 16384))
            {	/* say how much done ~ every second*/
                display_title_message("fft row %d",j);
            }
            for (i = 0; i< 2*ony ; i++)			tmpy[i] = pd[i].fl[j];
            realtr2bt(fpy2, tmpy, -1);
            fftbt(fpy2, tmpy, -1);
            realtr1bt(fpy2, tmpy);
            if (!(ois->im.win_flag & Y_PER))	defftbtwindow1(fpy2, tmpy, smp);
            for (i = 0; i< 2*ony ; i++)			pd[i].fl[j] = tmpy[i];
        }
    }
    switch_frame(ois,cf);  // we restore current image
    if (nfo > 1) switch_frame(oid,cf);
    inherit_from_im_to_im(oid,ois);
    uns_oi_2_oi(oid,ois);
    /*
       un = build_unit_set(0, xs, 1, 0, 0, NULL);
       if (un == NULL)		return 1;
       add_to_one_image (oid, IS_X_UNIT_SET, (void *)un);
       un = build_unit_set(0, ys, 1, 0, 0, NULL);
       if (un == NULL)		return 1;
       add_to_one_image (oid, IS_Y_UNIT_SET, (void *)un);
       oid->c_xu = oid->n_xu - 1;
       oid->c_yu = oid->n_yu - 1;
       */
    oid->im.win_flag = ois->im.win_flag;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    set_im_title(oid, "\\stack{{fft^{-1} 2D of image}{%s}}",
                 (ois->title != NULL) ? ois->title : ((ois->im.source != NULL) ?
                                                      ois->im.source : "untitled"));
    set_formated_string(&oid->im.treatement,"restored image after fft2d of %s",
                        (ois->title != NULL) ? ois->title : ((ois->im.source != NULL)
                                                             ? ois->im.source : "untitled"));
    return oid;
}




/*	compute the forward fft of a real image
 *	-dest is the result, if dest == NULL then it is created
 *	-ois is the source image
 *      -ask_movie is a flag, if set and ois is a movie and dest = NULL the function will ask the user to
 *      either treat all movie or a single image. If set to 0 treat all movie.
 *	-bmp is the bitmap to print message
 *		if NULL, no message print during process
 *	-smp is a small number tipically .05 in the Hanning window
 *		if smp = 0 the reverse operation is impossible
 */
O_i 	*forward_fftbt_2d_im_appo(O_i *dest, O_i *ois, int ask_movie, BITMAP *bmp, O_i *apo, int house_keeping)
{
    int i, j;
    int nout = 0, onx, ony, no, nfi = 0, nfo = 0, cf, im;
    double mean = 0;
    union pix *pd, *po = NULL;
    O_i  *oid = NULL;
    static fft_plan *fpy = NULL, *fpx = NULL, *fpx2 = NULL;
    static float *tmpx = NULL, *tmpy = NULL;
    static int ntmpx = 0, ntmpy = 0;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;  nfi = abs(ois->im.n_f); cf = ois->im.c_f;
    nfi = (nfi == 0) ? 1 : nfi;
    if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        win_printf ("Cannot treat a complex image yet!");
        return(NULL);
    }
    if (apo != NULL)
    {
        if (apo->im.nx != onx)
            return (O_i *) win_printf_ptr ("Inappropriate number of point in x \n"
                                           "for apodisation image ! %d instead of %d",apo->im.nx, onx);
        if (apo->im.ny != ony)
            return (O_i *) win_printf_ptr ("Inappropriate number of point in y \n"
                                           "for apodisation image ! %d instead of %d",apo->im.ny, ony);
        if (apo->im.data_type != IS_FLOAT_IMAGE)
            return (O_i *) win_printf_ptr ("Inappropriate apodisation image type");
    }
    if (dest != NULL)		/*	if dest exists check that destination is OK */
    {
        if (dest->im.data_type != IS_COMPLEX_IMAGE ||
            dest->im.nx != onx || dest->im.ny != ony/2)
        {
            free_one_image(dest);
            dest = NULL;
        }
    }
    if (dest != NULL)
    {
        nfo = abs(dest->im.n_f);
        nfo = (nfo == 0) ? 1 : nfo;
        oid = dest;
    }
    else
    {
        if (nfi < 2)	nfo = 1;
        else
        {
            if (ask_movie)
            {
                i = win_scanf("You are taking the FFT of a movie!\n"
                              "To apply FFT on all image press OK\n"
                              "To apply FFT only on the current image\n"
                              "Press CANCEL");
                if (i == WIN_CANCEL)	nfo = 1;
                else 		nfo = nfi;
            }
            else nfo = nfi;
        }
    }
    if (dest == NULL)
    {
        if (nfo < 2) oid = create_one_image(onx, ony/2, IS_COMPLEX_IMAGE);
        else         oid = create_one_movie(onx, ony/2, IS_COMPLEX_IMAGE, nfo);
    }
    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }

    fpy = fftbt_init(fpy, ony);
    fpx = fftbt_init(fpx,onx);
    fpx2 = fftbt_init(fpx2, 2*onx);
    if (fpy == NULL || fpx2 == NULL || fpx == NULL)
    {
        win_printf("Can't initialize fft nx %d ny %d",onx, ony);
        return(NULL);
    }
    if (tmpx == NULL || ntmpx != 2*onx)
    {
        tmpx = (float*)realloc(tmpx,2*onx*sizeof(float));
        if ( tmpx == NULL )
        {
            win_printf ("Can't create tmpx");
            return (NULL);
        }
        ntmpx = 2*onx;
    }
    if (tmpy == NULL || ntmpy != ony)
    {
        tmpy = (float*)realloc(tmpy,ony*sizeof(float));
        if ( tmpy == NULL )
        {
            win_printf ("Can't create tmpy");
            return (NULL);
        }
        ntmpy = ony;
    }
    for (im = 0; im < nfo; im++)
    {
        if (nfo > 1)
        {
            switch_frame(ois,im);
            switch_frame(oid,im);
        }
        pd = oid->im.pixel;
        if (apo) po = apo->im.pixel;
        if (!(ois->im.win_flag & (Y_PER|X_PER)))
        {  // we substract mean value before apodisation
            for (j=0, mean = 0; j< onx; j++)
            {
                extract_raw_row (ois, j, tmpy);
                for (i = 0; i< ony ; i++) mean += tmpy[i];
            }
            mean /= (onx*ony);
        }

        for (j=0; j< ois->im.nx; j++)
        {	/* fft on row */
            no = ony * j;
            if ((bmp != NULL) && (no >= nout + 16384))
            {	/* say how much done ~ every second*/
                display_title_message("fft row %d",j);
            }
            extract_raw_row (ois, j, tmpy);
            if (!(ois->im.win_flag & (Y_PER|X_PER)))
            {
                for (i = 0; i< ony ; i++) tmpy[i] -= mean;
                if (apo) for (i = 0; i< ony ; i++) tmpy[i] *= po[i].fl[j];
            }
            realtr1bt(fpy, tmpy);
            fftbt(fpy, tmpy, 1);
            realtr2bt(fpy, tmpy, 1);
            for (i = 0; i< ony/2 ; i++)
            {
                pd[i].fl[2*j] = tmpy[2*i];		/* real part */
                pd[i].fl[2*j+1] = tmpy[2*i+1];	/* imaginary part */
            }
        }
        /* fft on line */
        for (j=1, nout = 0; j< ony/2; j++)	/* ! start on mode k = 1	*/
        {
            no = 2*onx * j;
            if ((bmp != NULL) && (no >= nout + 16384))
            {	/* say how much done ~ every second*/
                display_title_message("fft line %d",j);
            }

            for (i = 0; i< onx ; i++)
            {
                tmpx[2*i] = pd[j].fl[2*i];
                tmpx[2*i+1] = pd[j].fl[2*i+1];
            }
            fftbt(fpx2, tmpx, 1);
            for (i = 0; i< onx/2 ; i++)		/* order from -Nx/2 to Nx/2 */
            {
                pd[j].fl[onx+2*i] = tmpx[2*i];
                pd[j].fl[onx+2*i+1] = tmpx[2*i+1];
                pd[j].fl[2*i] = tmpx[onx+2*i];
                pd[j].fl[2*i+1] = tmpx[onx+2*i+1];
            }
        }
        for (i = 0; i< onx ; i++)
        {
            tmpx[onx+i] = pd[0].fl[2*i];	/* all real part ky = 0		*/
            tmpx[i] = pd[0].fl[2*i+1];	/* all real part ky = ny/2 	*/
        }
        realtr1bt(fpx, tmpx);
        fftbt(fpx, tmpx, 1);
        realtr2bt(fpx, tmpx, 1);
        realtr1bt(fpx, tmpx + onx);
        fftbt(fpx, tmpx + onx, 1);
        realtr2bt(fpx, tmpx + onx, 1);
        for (i = 0; i< onx ; i++)
        {
            pd[0].fl[onx + i] 	= tmpx[onx+i];	/* all real part ky = 0		*/
            pd[0].fl[i] 		= tmpx[i];		/* all real part ky = ny/2 	*/
        }
        if (!(ois->im.win_flag & (Y_PER|X_PER)))
            pd[0].fl[onx] += mean;
    }
    /*	the ky = 0 mode are nearly well ordered except (kx = 0, kx = Nx/2) */
    /*	the ky = Ny/2 mode are ordered in inverse order */
    switch_frame(ois,cf);  // we restore current image
    if (nfo > 1) switch_frame(oid,cf);
    if (house_keeping) inherit_from_im_to_im(oid,ois);
    uns_oi_2_oi(oid,ois);
    oid->im.win_flag = ois->im.win_flag;
    oid->im.mode = LOG_AMP;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    oid->ax = -onx/2;
    set_im_x_title(oid,"wavenumber k_x");
    set_im_y_title(oid,"wavenumber k_y");
    set_im_title(oid, "\\stack{{fft 2D of image}{%s}}",(ois->title != NULL)
                 ? ois->title : ((ois->im.source != NULL) ? ois->im.source : "untitled"));
    set_formated_string(&oid->im.treatement,"sloppy fft2d image of %s",
                        (ois->title != NULL) ? ois->title : ((ois->im.source != NULL)
                                                             ? ois->im.source : "untitled"));
    return oid;
}




/*	compute appodize real image
 *	-dest is the result, if dest == NULL then it is created
 *	-ois is the source image
 *      -ask_movie is a flag, if set and ois is a movie and dest = NULL the function will ask the user to
 *      either treat all movie or a single image. If set to 0 treat all movie.
 *	-bmp is the bitmap to print message
 *		if NULL, no message print during process
 */
O_i 	*d2_im_appo(O_i *dest, O_i *ois, int ask_movie, BITMAP *bmp, O_i *apo, int house_keeping, int keep_dc)
{
    int i, j;
    int  onx, ony, nfi = 0, nfo = 0, cf, im;
    double mean = 0;
    union pix *pd, *po = NULL;
    O_i  *oid = NULL;
    static float *tmpx = NULL, *tmpy = NULL;
    static int ntmpx = 0, ntmpy = 0;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;  nfi = abs(ois->im.n_f); cf = ois->im.c_f;
    nfi = (nfi == 0) ? 1 : nfi;
    if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        win_printf ("Cannot treat a complex image yet!");
        return(NULL);
    }
    if (apo != NULL)
    {
        if (apo->im.nx != onx)
            return (O_i *) win_printf_ptr ("Inappropriate number of point in x \n"
                                           "for apodisation image ! %d instead of %d",apo->im.nx, onx);
        if (apo->im.ny != ony)
            return (O_i *) win_printf_ptr ("Inappropriate number of point in y \n"
                                           "for apodisation image ! %d instead of %d",apo->im.ny, ony);
        if (apo->im.data_type != IS_FLOAT_IMAGE)
            return (O_i *) win_printf_ptr ("Inappropriate apodisation image type");
    }
    if (dest != NULL)		/*	if dest exists check that destination is OK */
    {
        if (dest->im.data_type != IS_FLOAT_IMAGE ||
            dest->im.nx != onx || dest->im.ny != ony)
        {
            free_one_image(dest);
            dest = NULL;
        }
    }
    if (dest != NULL)
    {
        nfo = abs(dest->im.n_f);
        nfo = (nfo == 0) ? 1 : nfo;
        oid = dest;
    }
    else
    {
        if (nfi < 2)	nfo = 1;
        else
        {
            if (ask_movie)
            {
                i = win_scanf("This is a movie!\n"
                              "To apply appodisation to all image press OK\n"
                              "To apply appodisition only on the current image\n"
                              "Press CANCEL");
                if (i == WIN_CANCEL)	nfo = 1;
                else 		nfo = nfi;
            }
            else nfo = nfi;
        }
    }
    if (dest == NULL)
    {
        if (nfo < 2) oid = create_one_image(onx, ony, IS_FLOAT_IMAGE);
        else         oid = create_one_movie(onx, ony, IS_FLOAT_IMAGE, nfo);
    }
    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }

    if (tmpx == NULL || ntmpx != 2*onx)
    {
        tmpx = (float*)realloc(tmpx,2*onx*sizeof(float));
        if ( tmpx == NULL )
        {
            win_printf ("Can't create tmpx");
            return (NULL);
        }
        ntmpx = 2*onx;
    }
    if (tmpy == NULL || ntmpy != ony)
    {
        tmpy = (float*)realloc(tmpy,ony*sizeof(float));
        if ( tmpy == NULL )
        {
            win_printf ("Can't create tmpy");
            return (NULL);
        }
        ntmpy = ony;
    }
    for (im = 0; im < nfo; im++)
    {
        if (nfo > 1)
        {
            switch_frame(ois,im);
            switch_frame(oid,im);
	    for (j = 0; j < ois->im.user_nipar && j < oid->im.user_nipar; j++)
	      oid->im.user_ispare[im][j] = ois->im.user_ispare[im][j];
	    for (j = 0; j < ois->im.user_nipar && j < oid->im.user_nipar; j++)
	      oid->im.user_fspare[im][j] = ois->im.user_fspare[im][j];

        }
	if (bmp != NULL)
	  {	/* say how much done ~ every second*/
	    display_title_message("image %d",im);
	  }
        pd = oid->im.pixel;
        if (apo) po = apo->im.pixel;
        if (!(ois->im.win_flag & (Y_PER|X_PER)))
        {  // we substract mean value before apodisation
            for (j=0, mean = 0; j< onx; j++)
            {
                extract_raw_row (ois, j, tmpy);
                for (i = 0; i< ony ; i++) mean += tmpy[i];
            }
            mean /= (onx*ony);
        }
	if (keep_dc) mean = 0;
        for (j=0; j< ois->im.nx; j++)
        {
            extract_raw_row (ois, j, tmpy);
            if (!(ois->im.win_flag & (Y_PER|X_PER)))
            {
                for (i = 0; i< ony ; i++) tmpy[i] -= mean;
                if (apo) for (i = 0; i< ony ; i++) tmpy[i] *= po[i].fl[j];
            }
            for (i = 0; i< ony ; i++)
            {
                pd[i].fl[j] = tmpy[i];
            }
        }
    }
    switch_frame(ois,cf);  // we restore current image
    if (nfo > 1) switch_frame(oid,cf);
    if (house_keeping) inherit_from_im_to_im(oid,ois);
    uns_oi_2_oi(oid,ois);
    oid->im.win_flag = ois->im.win_flag;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return oid;
}



/*----------------------------------------------------------------
 *	compute the backward fft of a real image
 *	-dest is the result, if dest == NULL then it is created
 *	-ois is the source image
 *      -ask_movie is a flag, if set and ois is a movie and dest = NULL the function will ask the user to
 *      either treat all movie or a single image. If set to 0 treat all movie.
 *	-bmp is the BITMAP to print message
 *		if NULL, no message print during process
 *	-smp is a small number tipically .05 in the Hanning window
 *		if smp = 0 operation is impossible
 *       house_keeping = 1 transcibe historical information
 *-----------------------------------------------------------------
 */
O_i 	*backward_fftbt_2d_im_desappo(O_i *dest, O_i *ois, int ask_movie, BITMAP *bmp, O_i *apo, int house_keeping)
{
    int i, j;
    int nout = 0, onx, ony, no, nfi, nfo, cf, im;
    union pix *pd, *ps, *po = NULL;
    O_i  *oid = NULL;
    static fft_plan *fpy2 = NULL, *fpx = NULL, *fpx2 = NULL;
    static float *tmpx = NULL, *tmpy = NULL;
    static int ntmpx = 0, ntmpy = 0;
    float mean = 0;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;  nfi = abs(ois->im.n_f); cf = ois->im.c_f;
    nfi = (nfi == 0) ? 1 : nfi;
    if (ois->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if (apo != NULL)
    {
        if (apo->im.nx != onx)
            return (O_i *) win_printf_ptr ("Inappropriate number of point in x \n"
                                           "for apodisation image ! %d instead of %d", apo->im.nx,onx);
        if (apo->im.ny != 2*ony)
            return (O_i *) win_printf_ptr ("Inappropriate number of point in y \n"
                                           "for apodisation image ! %d instead of %d" ,apo->im.ny, ony);
        if (apo->im.data_type != IS_FLOAT_IMAGE)
            return (O_i *) win_printf_ptr ("Inappropriate apodisation image type");
    }
    if (dest != NULL)
    {
        if (dest->im.data_type != IS_FLOAT_IMAGE ||
            dest->im.nx != onx || dest->im.ny != ony*2)
        {
            free_one_image(dest);
            dest = NULL;
        }
    }
    if (dest != NULL)
    {
        nfo = abs(dest->im.n_f);
        nfo = (nfo == 0) ? 1 : nfo;
        oid = dest;
    }
    else
    {
        if (nfi < 2)	nfo = 1;
        else
        {
            if (ask_movie)
            {
                i = win_scanf("You are taking the FFT^{-1} of a movie!\n"
                              "To apply FFT on all image press OK\n"
                              "To apply FFT only on the current image\n"
                              "Press CANCEL");
                if (i == WIN_CANCEL)	nfo = 1;
                else 		nfo = nfi;
            }
            else nfo = nfi;
        }
    }
    if (dest == NULL)
    {
        if (nfo < 2) oid = create_one_image(onx, 2*ony, IS_FLOAT_IMAGE);
        else         oid = create_one_movie(onx, 2*ony, IS_FLOAT_IMAGE, nfo);
    }



    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }

    fpy2 = fftbt_init(fpy2, 2*ony);
    fpx = fftbt_init(fpx,onx);
    fpx2 = fftbt_init(fpx2, 2*onx);
    if (fpy2 == NULL || fpx2 == NULL || fpx == NULL)
    {
        win_printf("Can't initialize fft nx %d ny %d",onx, ony);
        return(NULL);
    }
    if (tmpx == NULL || ntmpx != 2*onx)
    {
        tmpx = (float*)realloc(tmpx,2*onx*sizeof(float));
        if ( tmpx == NULL )
        {
            win_printf ("Can't create tmpx");
            return (NULL);
        }
        ntmpx = 2*onx;
    }
    if (tmpy == NULL || ntmpy != 2*ony)
    {
        tmpy = (float*)realloc(tmpy,2*ony*sizeof(float));
        if ( tmpy == NULL )
        {
            win_printf ("Can't create tmpy");
            return (NULL);
        }
        ntmpy = 2*ony;
    }
    for (im = 0; im < nfo; im++)
    {
        if (nfo > 1)
        {
            switch_frame(ois,im);
            switch_frame(oid,im);
        }
        pd = oid->im.pixel;	ps = ois->im.pixel;
        if (apo) po = apo->im.pixel;
        if (!(ois->im.win_flag & (Y_PER|X_PER)))
        {             // remove dc component
            mean = ps[0].fl[onx];
            ps[0].fl[onx] = 0;
        }	      // treat ky = 0 and ky = Ny/2
        for (i = 0; i< 2*onx ; i++)	tmpx[i] = ps[0].fl[i];
        realtr2bt(fpx, tmpx, -1);
        fftbt(fpx, tmpx, -1);
        realtr1bt(fpx, tmpx);
        realtr2bt(fpx, tmpx + onx, -1);
        fftbt(fpx, tmpx + onx, -1);
        realtr1bt(fpx, tmpx + onx);
        for (i = 0; i< onx ; i++)
        {
            pd[0].fl[i] = tmpx[onx+i];	/* all real part ky = 0		*/
            pd[1].fl[i] = tmpx[i];	/* all real part ky = ny/2 	*/
        }
        /* now treat normal mode */
        for (j=1, nout = 0; j< ony; j++)
        {
            no = 2*onx * j;
            if ((bmp != NULL) && (no >= nout + 16384))   /* say how much done ~ every second*/
                display_title_message("fft line %d",j);

            for (i = 0; i< onx/2 ; i++)
            {
                tmpx[2*i] = ps[j].fl[onx+2*i];
                tmpx[2*i+1] = ps[j].fl[onx+2*i+1];
                tmpx[onx+2*i] = ps[j].fl[2*i];
                tmpx[onx+2*i+1] = ps[j].fl[2*i+1];
            }
            fftbt(fpx2, tmpx, -1);
            for (i = 0; i< onx ; i++)
            {
                pd[2*j].fl[i] = tmpx[2*i];
                pd[2*j+1].fl[i] = tmpx[2*i+1];
            }

        }
        for (j=0; j< ois->im.nx; j++)
        {
            no = ony * j;
            if ((bmp != NULL) && (no >= nout + 16384))	/* say how much done ~ every second*/
                display_title_message("fft row %d",j);
            for (i = 0; i< 2*ony ; i++)			tmpy[i] = pd[i].fl[j];
            realtr2bt(fpy2, tmpy, -1);
            fftbt(fpy2, tmpy, -1);
            realtr1bt(fpy2, tmpy);
            for (i = 0; i< 2*ony ; i++)			pd[i].fl[j] = tmpy[i];
        }
        if (!(ois->im.win_flag & (Y_PER|X_PER)))
        {
            if (apo != NULL)
            {
                for (i = 0; i< 2*ony ; i++)
                {
                    for (j = 0; j< onx ; j++)
                        pd[i].fl[j] = mean + pd[i].fl[j]*po[i].fl[j];
                }
            }
            else
            {
                for (i = 0; i< 2*ony ; i++)
                {
                    for (j = 0; j< onx ; j++)
                        pd[i].fl[j] += mean;
                }
            }
        }

    }
    switch_frame(ois,cf);  // we restore current image
    if (nfo > 1) switch_frame(oid,cf);
    if (house_keeping) inherit_from_im_to_im(oid,ois);
    uns_oi_2_oi(oid,ois);
    /*
       un = build_unit_set(0, xs, 1, 0, 0, NULL);
       if (un == NULL)		return 1;
       add_to_one_image (oid, IS_X_UNIT_SET, (void *)un);
       un = build_unit_set(0, ys, 1, 0, 0, NULL);
       if (un == NULL)		return 1;
       add_to_one_image (oid, IS_Y_UNIT_SET, (void *)un);
       oid->c_xu = oid->n_xu - 1;
       oid->c_yu = oid->n_yu - 1;
       */
    oid->im.win_flag = ois->im.win_flag;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    set_im_title(oid, "\\stack{{fft^{-1} 2D of image}{%s}}",
                 (ois->title != NULL) ? ois->title : ((ois->im.source != NULL) ?
                                                      ois->im.source : "untitled"));
    set_formated_string(&oid->im.treatement,"restored image after fft2d of %s",
                        (ois->title != NULL) ? ois->title : ((ois->im.source != NULL)
                                                             ? ois->im.source : "untitled"));
    return oid;
}





/*----------------------------------------------------------------
 *	compute the backward fft of a real image
 *	-dest is the result, if dest == NULL then it is created
 *	-ois is the source image
 *	-bmp is the BITMAP to print message
 *		if NULL, no message print during process
 *	-smp is a small number tipically .05 in the Hanning window
 *		if smp = 0 operation is impossible
 *-----------------------------------------------------------------
 */
O_i 	*backward_fft_2d_im(O_i *dest, O_i *ois, BITMAP *bmp, float smp)
{
    int i, j;
    int nout = 0, onx, ony, no;
    float *tmp;
    union pix *pd, *ps;
    O_i  *oid;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;
    if (ois->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if (dest != NULL)
    {
        if (dest->im.data_type != IS_FLOAT_IMAGE ||
            dest->im.nx != onx || dest->im.ny != ony*2)
        {
            free_one_image(dest);
            dest = NULL;
        }
    }
    oid = (dest != NULL) ? dest : create_one_image(onx, 2*ony, IS_FLOAT_IMAGE);
    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }
    pd = oid->im.pixel;	ps = ois->im.pixel;

    tmp = (float*)calloc(2*onx,sizeof(float));	/* fft-1 along x */
    if ( tmp == NULL )
    {
        free_one_image(oid);
        win_printf ("Can't create tmp");
        return (NULL);
    }
    if (fft_init(2*onx) || fft_init(onx))
    {
        free(tmp);
        free_one_image(oid);
        win_printf("Can't initialize fft");
        return (NULL);
    }
    /* treat ky = 0 and ky = Ny/2 */
    for (i = 0; i< 2*onx ; i++)
        tmp[i] = ps[0].fl[i];
    realtr2(onx, tmp, -1);
    fft(onx, tmp, -1);
    realtr1(onx, tmp);
    realtr2(onx, tmp + onx, -1);
    fft(onx, tmp + onx, -1);
    realtr1(onx, tmp + onx);
    if (!(ois->im.win_flag & X_PER))
    {
        defftwc1(onx, tmp,  smp);
        defftwc1(onx, tmp + onx,  smp);
    }
    for (i = 0; i< onx ; i++)
    {
        pd[0].fl[i] = tmp[onx+i];	/* all real part ky = 0		*/
        pd[1].fl[i] = tmp[i];	/* all real part ky = ny/2 	*/
    }
    fft_init(2*onx);		/* now treat normal mode */
    for (j=1, nout = 0; j< ony; j++)
    {
        no = 2*onx * j;


        if ((bmp != NULL) && (no >= nout + 16384))
        {	/* say how much done ~ every second*/
            display_title_message("fft line %d",j);
        }


        for (i = 0; i< onx/2 ; i++)
        {
            tmp[2*i] = ps[j].fl[onx+2*i];
            tmp[2*i+1] = ps[j].fl[onx+2*i+1];
            tmp[onx+2*i] = ps[j].fl[2*i];
            tmp[onx+2*i+1] = ps[j].fl[2*i+1];
        }
        fft(2*onx, tmp, -1);
        if (!(ois->im.win_flag & X_PER))	defftwc1(2*onx, tmp,  smp);
        for (i = 0; i< onx ; i++)
        {
            pd[2*j].fl[i] = tmp[2*i];
            pd[2*j+1].fl[i] = tmp[2*i+1];
        }

    }
    tmp = (float*)realloc(tmp,2*ony*sizeof(float));
    if ( tmp == NULL )
    {
        win_printf ("Can't create tmp");
        return (NULL);
    }
    if (fft_init(2*ony))
    {
        free(tmp);
        free_one_image(oid);
        win_printf("Can't initialize fft");
        return (NULL);
    }
    for (j=0; j< ois->im.nx; j++)
    {
        no = ony * j;
        if ((bmp != NULL) && (no >= nout + 16384))
        {	/* say how much done ~ every second*/
            display_title_message("fft row %d",j);
        }


        for (i = 0; i< 2*ony ; i++)			tmp[i] = pd[i].fl[j];
        realtr2(2*ony, tmp, -1);
        fft(2*ony, tmp, -1);
        realtr1(2*ony, tmp);
        if (!(ois->im.win_flag & Y_PER))	defftwindow1(2*ony, tmp, smp);
        for (i = 0; i< 2*ony ; i++)			pd[i].fl[j] = tmp[i];
    }
    free(tmp);
    inherit_from_im_to_im(oid,ois);
    uns_oi_2_oi(oid,ois);
    /*
       un = build_unit_set(0, xs, 1, 0, 0, NULL);
       if (un == NULL)		return 1;
       add_to_one_image (oid, IS_X_UNIT_SET, (void *)un);
       un = build_unit_set(0, ys, 1, 0, 0, NULL);
       if (un == NULL)		return 1;
       add_to_one_image (oid, IS_Y_UNIT_SET, (void *)un);
       oid->c_xu = oid->n_xu - 1;
       oid->c_yu = oid->n_yu - 1;
       */
    oid->im.win_flag = ois->im.win_flag;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    set_im_title(oid, "\\stack{{fft^{-1} 2D of image}{%s}}",
                 (ois->title != NULL) ? ois->title : ((ois->im.source != NULL) ?
                                                      ois->im.source : "untitled"));
    set_formated_string(&oid->im.treatement,"restored image after fft2d of %s",
                        (ois->title != NULL) ? ois->title : ((ois->im.source != NULL)
                                                             ? ois->im.source : "untitled"));
    return oid;
}


/*----------------------------------------------------------------
 *	compute the radial distribution of a fft of a real image
 *	-ois is the source image, create a plot with a ds
 *-----------------------------------------------------------------
 */
d_s 	*radial_wavenumber_of_fft_2d_im(O_i *ois)
{
    int i, j, im;
    int nout = 0, onx, ony, ire, iim, ik, nf;
    union pix  *ps;
    float tmp, fr, k;
    O_p  *op;
    d_s *ds;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;
    if (ois->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    nout = 1 + (int)sqrt((onx/2)*(onx/2) + ony * ony);
    op = create_and_attach_op_to_oi(ois, nout, nout, 0, 0);
    if (op == NULL) return NULL;
    ds= op->dat[0];



    nf = abs(ois->im.n_f);
    nf = (nf == 0) ? 1 : nf;
    if (nf > 1)   set_plot_x_title(op,"Radial wavenumber k_x of movie with %d im",nf);
    else   set_plot_x_title(op,"Radial wavenumber k_x");
    set_plot_y_title(op,"A^2");



    for (im = 0; im < nf; im++)
    {
        switch_frame(ois,im);
        ps = ois->im.pixel;

        ire = onx;
        iim = onx+1;
        ik = 0;
        ds->yd[0] += ps[0].fl[onx]*ps[0].fl[onx];
        ds->xd[0] += 1;
        ds->yd[onx/2] += ps[0].fl[onx+1]*ps[0].fl[onx+1];
        ds->xd[onx/2] += 1;
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = onx+2*i;
            iim = ire+1;
            ds->yd[i] += ps[0].fl[ire]*ps[0].fl[ire] + ps[0].fl[iim]*ps[0].fl[iim];
            ds->xd[i] += 1;
        }
        ire = 0;
        iim = 1;
        ik = ony;
        ds->yd[ik] += ps[0].fl[ire]*ps[0].fl[ire];
        ds->xd[ik] += 1;
        k = sqrt(onx*onx/4 + ony*ony);
        ik = (int)k;
        fr = k - ik;
        if (ik >= 0 && ik < ds->nx)
        {
            ds->yd[ik] += (1-fr)*ps[0].fl[iim]*ps[0].fl[iim];
            ds->xd[ik] += (1-fr);
            ds->yd[ik+1] += fr*ps[0].fl[iim]*ps[0].fl[iim];
            ds->xd[ik+1] += fr;
        }
        else win_printf("K out of range k = %g ik = %d",k,ik);
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = 2*i;
            iim = ire+1;
            k = sqrt(i*i + ony*ony/4);
            ik = (int)k;
            fr = k - ik;
            tmp = ps[0].fl[ire]*ps[0].fl[ire] + ps[0].fl[iim]*ps[0].fl[iim];
            if (ik >= 0 && ik < ds->nx)
            {
                ds->yd[ik] += (1-fr)*tmp;
                ds->xd[ik] += (1-fr);
                ds->yd[ik+1] += fr*tmp;
                ds->xd[ik+1] += fr;
            }
            else win_printf("K out of range k = %g ik = %d",k,ik);
        }

        for (j=1; j< ony; j++)
        {
            for (i = 0; i< onx ; i++)
            {
                ire = 2*i;
                iim = ire+1;
                k = sqrt(j*j + (i-(onx/2))*(i-(onx/2)));
                ik = (int)k;
                fr = k - ik;
                tmp = ps[j].fl[ire]*ps[j].fl[ire] + ps[j].fl[iim]*ps[j].fl[iim];
                if (ik >= 0 && ik < ds->nx)
                {
                    ds->yd[ik] += (1-fr)*tmp;
                    ds->xd[ik] += (1-fr);
                    ds->yd[ik+1] += fr*tmp;
                    ds->xd[ik+1] += fr;
                }
                else win_printf("K out of range k = %g ik = %d",k,ik);
            }
        }
    }
    for (i = 0; i< ds->nx ; i++)
    {
        ds->yd[i] = (ds->xd[i] > 0) ? ds->yd[i]/ds->xd[i] : 0;
        ds->xd[i] = i;
    }
    ois->need_to_refresh |= PLOTS_NEED_REFRESH;
    return ds;
}




O_i 	*radial_wavenumber_filter_xray_2d_im(O_i *ois, int ikx, int iky)
{
    int i, j, im;
    int  onx, ony, ire, iim, ik, nf;
    union pix *pd;
    float  k;
    O_i *oid = NULL;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;
    if (ois->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    oid = duplicate_image_or_movie(ois, NULL, 0);


    nf = abs(ois->im.n_f);
    nf = (nf == 0) ? 1 : nf;


    for (im = 0; im < nf; im++)
    {
        switch_frame(ois,im);
        switch_frame(oid,im);
	pd = oid->im.pixel;

        ire = onx;
        iim = onx+1;
        ik = 0;
	pd[0].fl[onx+1] = 0;
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = onx+2*i;
            iim = ire+1;
	    if (i%ikx != 0)
	      {
		pd[0].fl[ire] = 0;
		pd[0].fl[iim] = 0;
	      }
        }
        ire = 0;
        iim = 1;
        ik = ony;
	if (ik%iky != 0)
	  {
	    pd[0].fl[ire] = 0;
	  }
        k = sqrt(onx*onx/4 + ony*ony);
        ik = (int)k;
        if ((onx/2)%ikx != 0 || ony%iky != 0)
        {
            pd[0].fl[iim] = 0;
        }
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = 2*i;
            iim = ire+1;
            k = sqrt(i*i + ony*ony/4);
            ik = (int)k;
	    if (i%ikx != 0 || (ony/2)%iky != 0)
	      {
		pd[0].fl[ire] = 0;
		pd[0].fl[iim] = 0;
	      }
        }

        for (j=1; j< ony; j++)
        {
            for (i = 0; i< onx ; i++)
            {
                ire = 2*i;
                iim = ire+1;
                k = sqrt(j*j + (i-(onx/2))*(i-(onx/2)));
                ik = (int)k;
		if ((i-(onx/2))%ikx != 0 || j%iky != 0)
		  {
		    pd[j].fl[ire] = 0;
		    pd[j].fl[iim] = 0;
		  }
            }
        }
    }
    oid->need_to_refresh |= PLOTS_NEED_REFRESH;
    return oid;
}




/*----------------------------------------------------------------
 *	compute the radial distribution of a fft of a real image
 *	-ois is the source image, create an image
 *-----------------------------------------------------------------
 */
O_i 	*radial_wavenumber_of_fft_2d_movie(O_i *ois)
{
    int i, j, im, nfi;
    int nout = 0, onx, ony, ire, iim, ik;
    union pix  *ps = NULL;
    float tmp, fr, k;
    O_i *oid = NULL;
    d_s *ds = NULL;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;
    if (nfi < 2)
      {
        win_printf ("this is not a moviee!\n");
	return NULL;
      }

    if (ois->im.data_type != IS_COMPLEX_IMAGE)
      {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
      }
    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
      {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
      }
    nout = 2 + (int)sqrt((onx/2)*(onx/2) + ony * ony);
    oid = create_one_image(nout, nfi, IS_FLOAT_IMAGE);
    if (oid == NULL) return NULL;
    ds = build_data_set(nout,nout);
    if (ds == NULL) return NULL;
    set_im_x_title(oid,"Radial wavenumber k_x");
    set_im_y_title(oid,"A^2");

    for (im = 0; im < nfi; im++)
    {
        switch_frame(ois,im);
        ps = ois->im.pixel;
	for (i = 0; i < nout; i++)
	  ds->xd[i] = ds->yd[i] = 0;
        ire = onx;
        iim = onx+1;
        ik = 0;
        ds->yd[0] += ps[0].fl[onx]*ps[0].fl[onx];
        ds->xd[0] += 1;
        ds->yd[onx/2] += ps[0].fl[onx+1]*ps[0].fl[onx+1];
        ds->xd[onx/2] += 1;
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = onx+2*i;
            iim = ire+1;
	    if (i < ds->nx)
	      {
		ds->yd[i] += ps[0].fl[ire]*ps[0].fl[ire] + ps[0].fl[iim]*ps[0].fl[iim];
		ds->xd[i] += 1;
	      }
        }
        ire = 0;
        iim = 1;
        ik = ony;
	if (ik < ds->nx)
	  {
	    ds->yd[ik] += ps[0].fl[ire]*ps[0].fl[ire];
	    ds->xd[ik] += 1;
	  }
        k = sqrt(onx*onx/4 + ony*ony);
        ik = (int)k;
        fr = k - ik;
        if (ik >= 0 && ik < ds->nx)
        {
            ds->yd[ik] += (1-fr)*ps[0].fl[iim]*ps[0].fl[iim];
            ds->xd[ik] += (1-fr);
        }
        if (ik+1 >= 0 && ik+1 < ds->nx)
        {
            ds->yd[ik+1] += fr*ps[0].fl[iim]*ps[0].fl[iim];
            ds->xd[ik+1] += fr;
	}
        else win_printf("K out of range k = %g ik = %d",k,ik);
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = 2*i;
            iim = ire+1;
            k = sqrt(i*i + ony*ony/4);
            ik = (int)k;
            fr = k - ik;
            tmp = ps[0].fl[ire]*ps[0].fl[ire] + ps[0].fl[iim]*ps[0].fl[iim];
            if (ik >= 0 && ik < ds->nx)
            {
                ds->yd[ik] += (1-fr)*tmp;
                ds->xd[ik] += (1-fr);
            }
            if (ik >= 0 && ik+1 < ds->nx)
            {
                ds->yd[ik+1] += fr*tmp;
                ds->xd[ik+1] += fr;
            }
            else win_printf("K out of range k = %g ik = %d",k,ik);
        }

        for (j=1; j< ony; j++)
        {
            for (i = 0; i< onx ; i++)
            {
                ire = 2*i;
                iim = ire+1;
                k = sqrt(j*j + (i-(onx/2))*(i-(onx/2)));
                ik = (int)k;
                fr = k - ik;
                tmp = ps[j].fl[ire]*ps[j].fl[ire] + ps[j].fl[iim]*ps[j].fl[iim];
                if (ik >= 0 && ik < ds->nx)
                {
                    ds->yd[ik] += (1-fr)*tmp;
                    ds->xd[ik] += (1-fr);
                }
                if (ik >= 0 && ik+1 < ds->nx)
                {
                    ds->yd[ik+1] += fr*tmp;
                    ds->xd[ik+1] += fr;
                }
                else win_printf("K out of range k = %g ik = %d",k,ik);
            }
        }
	for (i = 0; i< ds->nx ; i++)
	  {
	    oid->im.pixel[im].fl[i] = ds->yd[i] = (ds->xd[i] > 0) ? ds->yd[i]/ds->xd[i] : 0;
	    ds->xd[i] = i;
	  }
    }
    oid->need_to_refresh = ALL_NEED_REFRESH;
    if (ds != NULL) free_data_set(ds);
    return oid;
}



/*  Find maximum position in a 1d array, the array is supposed periodic
 *  return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int find_min(double *x, float *Min_pos, float *Min_val, float *Min_deriv)
{
    int  ret = 0, delta;
    double a, b, c, d, f_2, f_1, f_0, f1, f2, xm = 0;

    // get neighbourhood
    f_2 = -x[0];
    f_1 = -x[1];
    f_0 = -x[2];
    f1 = -x[3];
    f2 = -x[4];

    // polinomiale interpolation
    if (f_1 < f1)
    {
        a = -f_1 / 6 + f_0 / 2 - f1 / 2 + f2 / 6;
        b = f_1 / 2 - f_0 + f1 / 2;
        c = -f_1 / 3 - f_0 / 2 + f1 - f2 / 6;
        d = f_0;
        delta = 0;
    }
    else
    {
        a = b = c = 0;
        a = -f_2 / 6 + f_1 / 2 - f_0 / 2 + f1 / 6;
        b = f_2 / 2 - f_1 + f_0 / 2;
        c = -f_2 / 3 - f_1 / 2 + f_0 - f1 / 6;
        d = f_1;
        delta = -1;
    }


    if (fabs(a) < 1e-8)
    {
        if (b != 0)
        {
            xm =  - c / (2 * b) - (3 * a * c * c) / (4 * b * b * b);
        }
        else
        {
            xm = 0;
            ret |= 0x1;
        }
    }
    else if ((b * b - 3 * a * c) < 0)
    {
      ret |= 0x2;
      xm = 0;
    }
    else
      xm = (-b - sqrt(b * b - 3 * a * c)) / (3 * a);

    *Min_pos = (float)(xm + 2 + delta);
    *Min_val = -(float)((a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d);

    if (Min_deriv)    *Min_deriv = -(float)((6 * a * xm) + (2 * b));

    return ret;
}



/*----------------------------------------------------------------
 *	compute the radial distribution of a fft of a real image
 *	-ois is the source image, create an image
 *-----------------------------------------------------------------
 */
O_i 	*histo_small_amp_of_fft_2d_movie(O_i *ois, float dA, int nA)
{
    int i, j, im, nfi;
    int  onx, ony, ire, iim, ik;
    union pix  *ps = NULL;
    float tmp, k;
    O_p *op = NULL;
    O_i *oid = NULL;
    d_s *ds = NULL, *dsop = NULL;
    double E[5] = {0}, a = 0, b = 0;
    float Min_pos = 0, Min_val = 0;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;
    if (nfi < 2)
      {
        win_printf ("this is not a moviee!\n");
	return NULL;
      }

    if (ois->im.data_type != IS_COMPLEX_IMAGE)
      {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
      }
    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
      {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
      }
    //nout = 2 + (int)sqrt((onx/2)*(onx/2) + ony * ony);
    oid = create_one_image(nA, nfi, IS_FLOAT_IMAGE);
    if (oid == NULL) return NULL;
    op = create_and_attach_op_to_oi(oid, nfi, nfi, 0, 0);
    if (op != NULL)	dsop = op->dat[0];
    ds = build_data_set(nA,nA);
    if (ds == NULL) return NULL;
    alloc_data_set_y_error(ds);
    if (ds == NULL) return NULL;
    set_im_x_title(oid,"Mode amplitude");
    set_im_y_title(oid,"Nb. of modes");


    for (im = 0; im < nfi; im++)
    {
        switch_frame(ois,im);
        ps = ois->im.pixel;
	for (i = 0; i < ds->nx; i++)
	  ds->xd[i] = ds->yd[i] = 0;
        ire = onx;
        iim = onx+1;
        ik = 0;
	tmp = (ps[0].fl[onx]*ps[0].fl[onx])/dA;
	i = (int)tmp;
	if (i >= 0 && i < ds->nx) ds->yd[i] += 1;
	tmp = (ps[0].fl[onx+1]*ps[0].fl[onx+1])/dA;
	ik = (int)tmp;
	ik = (ik < 0) ? 0 : ik;
	ik = (ik < ds->nx) ? ik : ds->nx - 1;
	ds->yd[ik] += 1;
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = onx+2*i;
            iim = ire+1;
	    tmp = ps[0].fl[ire]*ps[0].fl[ire] + ps[0].fl[iim]*ps[0].fl[iim]/dA;
	    ik = (int)tmp;
	    ik = (ik < 0) ? 0 : ik;
	    ik = (ik < ds->nx) ? ik : ds->nx - 1;
	    ds->yd[ik] += 1;
        }
        ire = 0;
        iim = 1;
        ik = ony;
	tmp = (ps[0].fl[ire]*ps[0].fl[ire])/dA;
	ik = (int)tmp;
	if (ik >= 0 && ik < ds->nx) ds->yd[ik] += 1;
	tmp = (ps[0].fl[iim]*ps[0].fl[iim])/dA;
	ik = (int)tmp;
	if (ik >= 0 && ik < ds->nx) ds->yd[ik] += 1;
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = 2*i;
            iim = ire+1;
            k = sqrt(i*i + ony*ony/4);
            ik = (int)k;
            //fr = k - ik;
            tmp = ps[0].fl[ire]*ps[0].fl[ire] + ps[0].fl[iim]*ps[0].fl[iim];
	    tmp /= dA;
	    ik = (int)tmp;
	    ik = (ik < 0) ? 0 : ik;
	    ik = (ik < ds->nx) ? ik : ds->nx - 1;
	    ds->yd[ik] += 1;
        }

        for (j=1; j< ony; j++)
        {
            for (i = 0; i< onx ; i++)
            {
                ire = 2*i;
                iim = ire+1;
                k = sqrt(j*j + (i-(onx/2))*(i-(onx/2)));
                ik = (int)k;
                //fr = k - ik;
                tmp = ps[j].fl[ire]*ps[j].fl[ire] + ps[j].fl[iim]*ps[j].fl[iim];
		tmp /= dA;
		ik = (int)tmp;
		ik = (ik < 0) ? 0 : ik;
		ik = (ik < ds->nx) ? ik : ds->nx - 1;
		ds->yd[ik] += 1;
            }
        }
	for (i = 0; i< ds->nx ; i++)
	  {
	    oid->im.pixel[im].fl[i] = ds->yd[i];
	    ds->xd[i] = i;
	    ds->ye[i] = 1 + sqrt(fabs(ds->yd[i]));
	  }
	if (nA > 20) ds->nx = ds->ny = 20;
	find_best_a_an_b_of_exp_fit(ds, 0, 0, .6,&a,&b,E);
	find_best_a_an_b_of_exp_fit(ds, 0, 0, .8,&a,&b,E+1);
	find_best_a_an_b_of_exp_fit(ds, 0, 0, 1.0,&a,&b,E+2);
	find_best_a_an_b_of_exp_fit(ds, 0, 0, 1.2,&a,&b,E+3);
	find_best_a_an_b_of_exp_fit(ds, 0, 0, 1.4,&a,&b,E+4);
	find_min(E, &Min_pos, &Min_val, NULL);
	Min_pos = (1.0 + (Min_pos - 2) * 0.2);
	find_best_a_an_b_of_exp_fit(ds, 0, 0, Min_pos,&a,&b,E);
	dsop->yd[im] = Min_pos;
	ds->nx = ds->ny = nA;
	for (i = 0, tmp = 0; i < ds->nx; i++)
	  tmp += ds->yd[i] - a*exp(-(double)i/Min_pos);
	dsop->yd[im] = tmp;
	dsop->xd[im] = im;
    }
    oid->need_to_refresh = ALL_NEED_REFRESH;
    if (ds != NULL) free_data_set(ds);
    return oid;
}





/*----------------------------------------------------------------
 *	compute the radial distribution of a fft of a real image
 *	-ois is the source image, create an image
 *-----------------------------------------------------------------
 */
O_p 	*Nb_of_mode_radial_wavenumber_of_fft_2d_movie(O_i *ois, float thres, float kmax)
{
    int nout = 0,i, j, im, nfi;
    int  onx, ony, ire, iim, ik;
    union pix  *ps = NULL;
    float tmp, k;
    d_s *ds = NULL;
    O_p *op = NULL;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;
    if (nfi < 2)
      {
        win_printf ("this is not a moviee!\n");
	return NULL;
      }

    if (ois->im.data_type != IS_COMPLEX_IMAGE)
      {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
      }
    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
      {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
      }
    nout = 1 + (int)sqrt((onx/2)*(onx/2) + ony * ony);
    op = create_one_plot(nfi, nfi, 0);
    if (op == NULL) return NULL;
    ds = op->dat[0];
    for (im = 0; im < nfi; im++)
    {
        switch_frame(ois,im);
        ps = ois->im.pixel;
        ire = onx;
        iim = onx+1;
        ik = 0;
        ds->yd[im] += (ps[0].fl[onx]*ps[0].fl[onx] > thres) ? 1 : 0;
        ds->xd[im] = im;
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = onx+2*i;
            iim = ire+1;
	    if (((float)i < kmax) && (ps[0].fl[ire]*ps[0].fl[ire] + ps[0].fl[iim]*ps[0].fl[iim] > thres))
	      ds->yd[im] += 1;
        }
        ire = 0;
        iim = 1;
        ik = ony;
	if (((float)ik < kmax) && (ps[0].fl[ire]*ps[0].fl[ire] > thres))
	  ds->yd[im] += 1;

        k = sqrt(onx*onx/4 + ony*ony);
	if ((k < kmax) && (ps[0].fl[iim]*ps[0].fl[iim] > thres))
	  ds->yd[im] += 1;
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = 2*i;
            iim = ire+1;
            k = sqrt(i*i + ony*ony/4);
	    tmp = ps[0].fl[ire]*ps[0].fl[ire] + ps[0].fl[iim]*ps[0].fl[iim];
	    if ((k < kmax) && (tmp > thres))
	      ds->yd[im] += 1;
        }

        for (j=1; j< ony; j++)
        {
            for (i = 0; i< onx ; i++)
            {
                ire = 2*i;
                iim = ire+1;
                k = sqrt(j*j + (i-(onx/2))*(i-(onx/2)));
                tmp = ps[j].fl[ire]*ps[j].fl[ire] + ps[j].fl[iim]*ps[j].fl[iim];
		if ((k < kmax) && (tmp > thres))
		  ds->yd[im] += 1;
            }
        }
    }
    return op;
}

/*
float grab_noise_at_high_k(O_i *ois, float kmin)
{
  int i, j, ire, iim, k, kx, np, onx, ony;
  float tmp;
  union pix  *ps = NULL;

  if (ois == NULL || kmin < 0) return 0;
  ony = ois->im.ny;	onx = ois->im.nx;
  ps = ois->im.pixel;
  for (j=1, tmp = 0, np =0; j< ony-1; j++)
    {
      for (i = 1; i< onx-1 ; i++)
	{
	  kx = i-(onx/2);
	  k = sqrt(j*j + kx*kx);
	  if (k < kmin) continue;
	  ire = 2*i;
	  iim = ire+1;
	  tmp += ps[j].fl[ire]*ps[j].fl[ire] + ps[j].fl[iim]*ps[j].fl[iim];
	  np++;
	}
    }
  return (np > 0) ? tmp/np : tmp;
}
*/
float grab_noise_at_high_k(O_i *ois, float kmin)
{
    int i, j, ire, iim, k, kx, np, onx, ony, imi, nfi = 0;
    float tmp, re, im;

    if (ois == NULL || kmin < 0) return 0;
    ony = ois->im.ny;	onx = ois->im.nx;
    nfi = nb_of_frames_in_movie(ois);
    for (j=1, tmp = 0, np =0; j< ony-1; j++)
        {
            for (i = 1; i< onx-1 ; i++)
                {
                    kx = i-(onx/2);
                    k = sqrt(j*j + kx*kx);
                    if (k < kmin) continue;
                    ire = 2*i;
                    iim = ire+1;
                    for (imi = 0; imi < nfi; imi++)
                        {
                            re = ois->im.pxl[imi][j].fl[ire];
                            im = ois->im.pxl[imi][j].fl[iim];
                            tmp += re*re+im*im;
                            np++;
                        }
                }
        }
    return (np > 0) ? tmp/np : tmp;
}

int do_grab_noise_at_high_k(void)
{
    int i;
    O_i  *ois = NULL;
    imreg *imr;
    static float kmin = 32;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("grab noise at high k\n"
                  "kmin %5f\n",&kmin);
    if (i == WIN_CANCEL) return 0;
    win_printf("Noise is equal to %g",grab_noise_at_high_k(ois, kmin));
    return 0;
}

/*----------------------------------------------------------------
 *	compute the radial distribution of a fft of a real image
 *	-ois is the source image, create an image
 *-----------------------------------------------------------------
 */
d_s 	*Nb_modes_above_noise_by_histo_small_amp_of_fft_2d_movie(O_i *ois, d_s *dsd)
{
    int i, j, im, nfi;
    int  onx, ony, ire, iim, ik;
    union pix  *ps = NULL;
    float tmp, k;
    O_p *op = NULL;
    static d_s *ds = NULL;
    d_s *dsop = NULL;
    double E[5] = {0}, a = 0, b = 0;
    float Min_pos = 0, Min_val = 0;
    float dA = 0;
    int nA = 20;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;
    if (nfi < 2)
        {
            win_printf ("this is not a moviee!\n");
            return NULL;
        }
    
    if (ois->im.data_type != IS_COMPLEX_IMAGE)
        {
            win_printf ("the image is not of complex type!\n"
                        "Cannot perform backward fft!");
            return(NULL);
        }
    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
        {
            win_printf ("the image does not correspond to an fft!\n"
                        "Cannot perform backward fft!");
            return(NULL);
        }
    //nout = 2 + (int)sqrt((onx/2)*(onx/2) + ony * ony);
    if (ds == NULL || ds->nx != nA || ds->ny != nA)
        ds = build_adjust_data_set(ds,nA,nA);
    if (ds == NULL) return NULL;
    alloc_data_set_y_error(ds);
    if (ds == NULL) return NULL;

    if (dsd == NULL)
        {
            op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, 0);
            if (op != NULL)	dsop = op->dat[0];
        }
    else
        {
            if (dsd->nx != nfi || dsd->ny != nfi)
                dsd = build_adjust_data_set(dsd, nfi, nfi);
            if (dsd == NULL) return NULL;
            dsop = dsd;
        }
    switch_frame(ois,nfi/2);
    dA = grab_noise_at_high_k(ois, (float)(2*ony)/3);
    //win_printf("Noise at %g",dA);
    for (im = 0; im < nfi; im++)
        {
            switch_frame(ois,im);
            ps = ois->im.pixel;
            dsop->xd[im] = dsop->yd[im] = 0;
            for (i = 0; i < ds->nx; i++)
                ds->xd[i] = ds->yd[i] = 0;
            ire = onx;
            iim = onx+1;
            ik = 0;
            tmp = (ps[0].fl[onx]*ps[0].fl[onx])/dA;
            ik = (int)tmp;
            ik = (ik < 0) ? 0 : ik;
            ik = (ik < ds->nx) ? ik : ds->nx - 1;
            ds->yd[ik] += 1;
            tmp = (ps[0].fl[onx+1]*ps[0].fl[onx+1])/dA;
            ik = (int)tmp;
            ik = (ik < 0) ? 0 : ik;
            ik = (ik < ds->nx) ? ik : ds->nx - 1;
            ds->yd[ik] += 1;
            for (i = 1; i< onx/2 ; i++)
                {  // all real part ky = 0
                    ire = onx+2*i;
                    iim = ire+1;
                    tmp = (ps[0].fl[ire]*ps[0].fl[ire] + ps[0].fl[iim]*ps[0].fl[iim])/dA;
                    ik = (int)tmp;
                    ik = (ik < 0) ? 0 : ik;
                    ik = (ik < ds->nx) ? ik : ds->nx - 1;
                    ds->yd[ik] += 1;
                }
            ire = 0;
            iim = 1;
            ik = ony;
            tmp = (ps[0].fl[ire]*ps[0].fl[ire])/dA;
            ik = (int)tmp;
            ik = (ik < 0) ? 0 : ik;
            ik = (ik < ds->nx) ? ik : ds->nx - 1;
            ds->yd[ik] += 1;            
            tmp = (ps[0].fl[iim]*ps[0].fl[iim])/dA;
            ik = (int)tmp;
            ik = (ik < 0) ? 0 : ik;
            ik = (ik < ds->nx) ? ik : ds->nx - 1;
            ds->yd[ik] += 1;            
            for (i = 1; i< onx/2 ; i++)
                {  // all real part ky = 0
                    ire = 2*i;
                    iim = ire+1;
                    k = sqrt(i*i + ony*ony/4);
                    ik = (int)k;
                    //fr = k - ik;
                    tmp = ps[0].fl[ire]*ps[0].fl[ire] + ps[0].fl[iim]*ps[0].fl[iim];
                    tmp /= dA;
                    ik = (int)tmp;
                    ik = (ik < 0) ? 0 : ik;
                    ik = (ik < ds->nx) ? ik : ds->nx - 1;
                    ds->yd[ik] += 1;
                }
            
            for (j=1; j< ony; j++)
                {
                    for (i = 0; i< onx ; i++)
                        {
                            ire = 2*i;
                            iim = ire+1;
                            k = sqrt(j*j + (i-(onx/2))*(i-(onx/2)));
                            ik = (int)k;
                            //fr = k - ik;
                            tmp = ps[j].fl[ire]*ps[j].fl[ire] + ps[j].fl[iim]*ps[j].fl[iim];
                            tmp /= dA;
                            ik = (int)tmp;
                            ik = (ik < 0) ? 0 : ik;
                            ik = (ik < ds->nx) ? ik : ds->nx - 1;
                            ds->yd[ik] += 1;
                        }
                }
            for (i = 0; i< ds->nx ; i++)
                {
                    ds->xd[i] = i;
                    ds->ye[i] = 1 + sqrt(fabs(ds->yd[i]));
                }
            if (nA > 20) ds->nx = ds->ny = 20;
            find_best_a_an_b_of_exp_fit(ds, 0, 0, .6,&a,&b,E);
            find_best_a_an_b_of_exp_fit(ds, 0, 0, .8,&a,&b,E+1);
            find_best_a_an_b_of_exp_fit(ds, 0, 0, 1.0,&a,&b,E+2);
            find_best_a_an_b_of_exp_fit(ds, 0, 0, 1.2,&a,&b,E+3);
            find_best_a_an_b_of_exp_fit(ds, 0, 0, 1.4,&a,&b,E+4);
            find_min(E, &Min_pos, &Min_val, NULL);
            Min_pos = (1.0 + (Min_pos - 2) * 0.2);
            find_best_a_an_b_of_exp_fit(ds, 0, 0, Min_pos,&a,&b,E);
            dsop->yd[im] = Min_pos;
            ds->nx = ds->ny = nA;
            for (i = 0, tmp = 0; i < ds->nx; i++)
                tmp += ds->yd[i] - a*exp(-(double)i/Min_pos);
            dsop->yd[im] = tmp;
            dsop->xd[im] = im;
        }
    return dsop;
}



int do_grab_Nb_modes_above_noise(void)
{
    O_i  *ois = NULL;
    imreg *imr;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    Nb_modes_above_noise_by_histo_small_amp_of_fft_2d_movie(ois, NULL);
    ois->need_to_refresh = ALL_NEED_REFRESH;
    return refresh_image(imr, UNCHANGED);
}






# ifdef NOTUSED
/*----------------------------------------------------------------
 *	compute the radial distribution of a fft of a real image
 *	-ois is the source image, create a plot with a ds
 *-----------------------------------------------------------------
 */
d_s *keep_mode_close_to_grid_inf_fft_2d_im(O_i *ois, float k0x, float k0y, float k1x, float k1y)
{
    int i, j, im;
    int nout = 0, onx, ony, ire, iim, ik, nf;
    union pix  *ps;
    float tmp, fr, k;
    O_p  *op;
    d_s *ds;

    (void)k0x;
    (void)k0y;
    (void)k1x;
    (void)k1y:
    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;
    if (ois->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    nout = 1 + (int)sqrt((onx/2)*(onx/2) + ony * ony);
    op = create_and_attach_op_to_oi(ois, nout, nout, 0, 0);
    if (op == NULL) return NULL;
    ds= op->dat[0];



    nf = abs(ois->im.n_f);
    nf = (nf == 0) ? 1 : nf;

    for (im = 0; im < nf; im++)
    {
        switch_frame(ois,im);
        ps = ois->im.pixel;

        ire = onx;
        iim = onx+1;
        ik = 0;
        ds->yd[0] += ps[0].fl[onx]*ps[0].fl[onx];
        ds->xd[0] += 1;
        ds->yd[onx/2] += ps[0].fl[onx+1]*ps[0].fl[onx+1];
        ds->xd[onx/2] += 1;
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = onx+2*i;
            iim = ire+1;
            ds->yd[i] += ps[0].fl[ire]*ps[0].fl[ire] + ps[0].fl[iim]*ps[0].fl[iim];
            ds->xd[i] += 1;
        }
        ire = 0;
        iim = 1;
        ik = ony;
        ds->yd[ik] += ps[0].fl[ire]*ps[0].fl[ire];
        ds->xd[ik] += 1;
        k = sqrt(onx*onx/4 + ony*ony);
        ik = (int)k;
        fr = k - ik;
        if (ik >= 0 && ik < ds->nx)
        {
            ds->yd[ik] += (1-fr)*ps[0].fl[iim]*ps[0].fl[iim];
            ds->xd[ik] += (1-fr);
            ds->yd[ik+1] += fr*ps[0].fl[iim]*ps[0].fl[iim];
            ds->xd[ik+1] += fr;
        }
        else win_printf("K out of range k = %g ik = %d",k,ik);
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = 2*i;
            iim = ire+1;
            k = sqrt(i*i + ony*ony/4);
            ik = (int)k;
            fr = k - ik;
            tmp = ps[0].fl[ire]*ps[0].fl[ire] + ps[0].fl[iim]*ps[0].fl[iim];
            if (ik >= 0 && ik < ds->nx)
            {
                ds->yd[ik] += (1-fr)*tmp;
                ds->xd[ik] += (1-fr);
                ds->yd[ik+1] += fr*tmp;
                ds->xd[ik+1] += fr;
            }
            else win_printf("K out of range k = %g ik = %d",k,ik);
        }

        for (j=1; j< ony; j++)
        {
            for (i = 0; i< onx ; i++)
            {
                ire = 2*i;
                iim = ire+1;
                k = sqrt(j*j + (i-(onx/2))*(i-(onx/2)));
                ik = (int)k;
                fr = k - ik;
                tmp = ps[j].fl[ire]*ps[j].fl[ire] + ps[j].fl[iim]*ps[j].fl[iim];
                if (ik >= 0 && ik < ds->nx)
                {
                    ds->yd[ik] += (1-fr)*tmp;
                    ds->xd[ik] += (1-fr);
                    ds->yd[ik+1] += fr*tmp;
                    ds->xd[ik+1] += fr;
                }
                else win_printf("K out of range k = %g ik = %d",k,ik);
            }
        }
    }
    for (i = 0; i< ds->nx ; i++)
    {
        ds->yd[i] = (ds->xd[i] > 0) ? ds->yd[i]/ds->xd[i] : 0;
        ds->xd[i] = i;
    }
    ois->need_to_refresh |= PLOTS_NEED_REFRESH;
    return ds;
}

#endif




/*----------------------------------------------------------------
 *	compute the projection  profile of an image on a defined angle
 *	-ois is the source image, create a plot
 *      if n_ds is in [0, n_dat[ use the existing ds, else creta a new one
 *-----------------------------------------------------------------
 */
O_p 	*image_projection_along_an_angle(O_i *ois, O_p *opd, int n_ds, float angle)
{
    int i, j;
    int nout = 0, nout_2, onx, ony, onx_2, ony_2, ik;
    //union pix  *ps;
    float fr, k, co, si;
    O_p  *op;
    d_s *ds;
    double val;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;
    ony_2 = ony/2;   onx_2 = onx/2;        // image center
    nout_2 = 1 + (int)sqrt(onx_2 * onx_2 + ony_2 * ony_2);
    nout = 2 * nout_2;
    //ps = ois->im.pixel;
    if (opd == NULL)
    {
        op = create_one_plot( nout, nout, 0);
        if (op == NULL) return NULL;
    }
    else op = opd;
    if (n_ds < 0 || n_ds >= op->n_dat)
    {
        if ((ds = create_and_attach_one_ds(op, nout, nout, 0)) == NULL)
            return (O_p *) win_printf_ptr("I can't create plot !");
    }
    else ds= op->dat[0];
    if (ds->mx < nout) ds = build_adjust_data_set(ds, nout, nout);
    ds->nx = ds->ny = nout;
    co = cos(M_PI*angle/180);
    si = sin(M_PI*angle/180);

    if (ds == NULL)
    {
        win_printf("Data set allocation pb");
        return (NULL);
    }
    for (i = 0; i< ds->nx ; i++)
    {
        ds->yd[i] = 0;
        ds->xd[i] = 0;
    }

    for (j = 0; j< ony; j++)
    {
        for (i = 0; i< onx ; i++)
        {
            get_raw_pixel_value(ois, i, j, &val);
            k = co * (i - onx_2) + si * (j - ony_2);
            k += nout_2;
            ik = (int)k;
            fr = k - ik;
            ds->yd[ik] += (1-fr)*val;
            ds->xd[ik] += (1-fr);
            ds->yd[ik+1] += fr*val;
            ds->xd[ik+1] += fr;
        }
    }
    for (i = 0; i< ds->nx ; i++)
    {
        //ds->yd[i] = (ds->xd[i] > 0)  ? ds->yd[i] /ds->xd[i]: 0;
        ds->xd[i] = i-nout_2;
        ds->yd[i] /= onx*ony;

    }
    inherit_from_im_to_ds(op->dat[0], ois);
    set_ds_treatement(ds, "Angular profile at %g degrees", angle);
    ois->need_to_refresh |= PLOTS_NEED_REFRESH;
    return op;
}



/*----------------------------------------------------------------
 *	filter an image by mulriplying fft by the radial wavenumber profile
 *	-ois is the source image dest is modified by the operation,
 *      if dest = NULL a new image is created, if dest == ois, ois is modified
 *-----------------------------------------------------------------
 */
O_i 	*filter_fft_2d_im_by_radial_wavenumber(O_i *ois, O_i *dest, d_s *ds)
{
    int i, j;
    int nout = 0, onx, ony, ire, iim, ik, im, nf;
    union pix  *ps, *pd;
    float  k;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    if (ds == NULL)
    {
        win_printf("No data-set specified found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;
    if (ois->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    nout = 1 + (int)sqrt((onx/2)*(onx/2) + ony * ony);

    if (nout > ds->nx || nout > ds->ny)
    {
        win_printf("your data-set does not have enough points %d to filter %d!",ds->nx,nout);
        return (NULL);
    }
    if (dest != NULL)		/*	if dest exists check that destination is OK */
    {
        if (dest->im.data_type != IS_COMPLEX_IMAGE ||
            dest->im.nx != onx || dest->im.ny != ony/2)
        {
            free_one_image(dest);
            dest = NULL;
        }
    }
    dest = (dest != NULL) ? dest : duplicate_image(ois, NULL);
    if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }


    nf = abs(ois->im.n_f);
    nf = (nf == 0) ? 1 : nf;

    ire = onx;
    iim = onx+1;
    ik = 0;
    for (im = 0; im < nf; im++)
    {
        switch_frame(ois,im);
        switch_frame(dest,im);
        ps = ois->im.pixel;
        pd = dest->im.pixel;

        pd[0].fl[onx] = ds->yd[0] * ps[0].fl[onx];
        pd[0].fl[onx+1] = ds->yd[onx/2] * ps[0].fl[onx+1];
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = onx+2*i;
            iim = ire+1;
            pd[0].fl[ire] = ds->yd[i] * ps[0].fl[ire];
            pd[0].fl[iim] = ds->yd[i] * ps[0].fl[iim];
        }
        ire = 0;
        iim = 1;
        ik = ony;
        pd[0].fl[ire] = ds->yd[ik] * ps[0].fl[ire];
        k = sqrt(onx*onx/4 + ony*ony);
        ik = (int)(k+0.5);
        pd[0].fl[iim] = ds->yd[ik] * ps[0].fl[iim];
        for (i = 1; i< onx/2 ; i++)
        {  // all real part ky = 0
            ire = 2*i;
            iim = ire+1;
            k = sqrt(i*i + ony*ony/4);
            ik = (int)(k+1);
            pd[0].fl[ire] = ds->yd[ik] * ps[0].fl[ire];
            pd[0].fl[iim] = ds->yd[ik] * ps[0].fl[iim];
        }

        for (j=1; j< ony; j++)
        {
            for (i = 0; i< onx ; i++)
            {
                ire = 2*i;
                iim = ire+1;
                k = sqrt(j*j + (i-(onx/2))*(i-(onx/2)));
                ik = (int)(k+0.5);
                pd[j].fl[ire] = ds->yd[ik] * ps[j].fl[ire];
                pd[j].fl[iim] = ds->yd[ik] * ps[j].fl[iim];
            }
        }
    }
    //update_oi_treatment(dest, "filter an image by mulriplying fft by the radial wavenumber profile");
    dest->need_to_refresh |= PLOTS_NEED_REFRESH;
    return dest;
}



/*
 *		destination may be NULL or one of the images
 *
 */
O_i 	*corel_2d_im(O_i *dest, O_i *ois1, O_i *ois2)
{
    int i, j;
    int  onx, ony;
    float re1, re2, im1, im2, m1, m2;
    float re0_0_1, renx2_0_1, re0_ny2_1, renx2_ny2_1;
    float re0_0_2, renx2_0_2, re0_ny2_2, renx2_ny2_2;
    union pix *pd, *ps1, *ps2;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No source 1 image found");
        return (NULL);
    }
    if ( ois2 == NULL|| ois2->im.pixel == NULL)
    {
        win_printf("No source 2 image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    if (ois1->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image 1 is not of complex type!\n"
                    "Cannot perform corelation!");
        return(NULL);
    }
    if ((ois1->im.treatement == NULL) || strncmp(ois1->im.treatement,
                                                 "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image 1 does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if (ois2->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image 2 is not of complex type!\n"
                    "Cannot perform corelation!");
        return(NULL);
    }
    if (ony != ois2->im.ny || onx != ois2->im.nx)
    {
        win_printf ("The size of the second image differs from the source one \n"
                    "Cannot perform corelation!");
        return(NULL);
    }
    if ((ois2->im.treatement == NULL) || strncmp(ois2->im.treatement,
                                                 "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image 2 does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }

    dest = (dest != NULL) ? dest : create_one_image(onx, ony, IS_COMPLEX_IMAGE);	if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }
    if (ois2->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the destination image is not of complex type!\n"
                    "Cannot perform corelation!");
        return(NULL);
    }
    if (ony != dest->im.ny || onx != dest->im.nx)
    {
        win_printf ("The destination size differs from the source one \n"
                    "Cannot perform corelation!");
        return(NULL);
    }

    pd = dest->im.pixel;	ps1 = ois1->im.pixel;	ps2 = ois2->im.pixel;
    re0_0_1 	= ps1[0].fl[onx];
    renx2_0_1 	= ps1[0].fl[onx + 1];
    re0_ny2_1 	= ps1[0].fl[0];
    renx2_ny2_1 = ps1[0].fl[1];
    re0_0_2 	= ps2[0].fl[onx];
    renx2_0_2 	= ps2[0].fl[onx + 1];
    re0_ny2_2 	= ps2[0].fl[0];
    renx2_ny2_2 = ps2[0].fl[1];
    for (i=0, m1 = 0, m2 = 0; i< ony; i++)
    {
        for (j=0; j< onx; j++)
        {
            re1 = ps1[i].fl[2*j];	re2 = ps2[i].fl[2*j];
            im1 = ps1[i].fl[2*j+1];	im2 = ps2[i].fl[2*j+1];
            m1 += re1 * re1 + im1 * im1;
            m2 += re2 * re2 + im2 * im2;
        }
    }
    m1 = sqrt(m1);
    m2 = sqrt(m2);
    m1 *= m2;
    for (i=0; i< ony; i++)
    {
        for (j=0; j< onx; j++)
        {
            re1 = ps1[i].fl[2*j];	re2 = ps2[i].fl[2*j];
            im1 = ps1[i].fl[2*j+1];	im2 = ps2[i].fl[2*j+1];
            pd[i].fl[2*j] = (re1 * re2 + im1 * im2)/m1;
            pd[i].fl[2*j+1] = (im1 * re2 - re1 * im2)/m1;
        }
    }
    pd[0].fl[onx] 		= 2*(re0_0_1 * re0_0_2)/m1 ;
    pd[0].fl[onx + 1] 	= 2*(renx2_0_1 * renx2_0_2)/m1;
    pd[0].fl[0] 		= 2*(re0_ny2_1 * re0_ny2_2 )/m1;
    pd[0].fl[1] 		= 2*(renx2_ny2_1 * renx2_ny2_2)/m1;

    inherit_from_im_to_im(dest,ois1);
    dest->im.mode = LOG_AMP;
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;
    set_im_title(dest, "\\stack{{corelation of images}{%s and %s}}",
                 (ois1->im.source != NULL) ? 	ois1->im.source : "untitled",
                 (ois2->im.source != NULL) ? 	ois2->im.source : "untitled");
    set_formated_string(&dest->im.treatement,
                        "sloppy fft2d image : corelation of images %s and %s",
                        (ois1->im.source != NULL) ? 	ois1->im.source : "untitled",
                        (ois2->im.source != NULL) ? 	ois2->im.source : "untitled");
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}


/*  correlate a movie using two int array to determine the two images to correlate
 *		destination may be NULL or one of the images
 *
 */
O_i 	*corel_2d_movie_in_time_from_arrays(O_i *dest, O_i *ois1, int *first_im, int *second_im, int size, int normalize)
{
    int i, j, im;
    int  onx, ony, nfi;
    float re1, re2, im1, im2, m1, m2;
    float re0_0_1, renx2_0_1, re0_ny2_1, renx2_ny2_1;
    float re0_0_2, renx2_0_2, re0_ny2_2, renx2_ny2_2;
    union pix *pd, *ps1, *ps2;
    int imi1, imi2;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No source 1 image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    nfi = abs(ois1->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;
    if (nfi < 2) return win_printf_ptr("the image 1 is not a movie!\n");
    if (ois1->im.data_type != IS_COMPLEX_IMAGE)
        return win_printf_ptr ("the image 1 is not of complex type!\n"
                    "Cannot perform corelation!");
    if ((ois1->im.treatement == NULL) || strncmp(ois1->im.treatement,
                                                 "sloppy fft2d image",18) != 0)
        return win_printf_ptr ("the image 1 does not correspond to an fft!\n"
                    "Cannot perform backward fft!");

    //if (ref_im >= nfi) return win_printf_ptr("ref frame %d out of range [-1,%d[",ref_im,nfi);
    dest = (dest != NULL) ? dest : create_one_movie(onx, ony, IS_COMPLEX_IMAGE, size);
    if (dest == NULL)
        return win_printf_ptr("Can't create dest image");
    if (dest->im.data_type != IS_COMPLEX_IMAGE)
        return win_printf_ptr("the destination image is not of complex type!\n"
                    "Cannot perform corelation!");
    if (ony != dest->im.ny || onx != dest->im.nx)
        return win_printf_ptr ("The destination size differs from the source one \n"
                    "Cannot perform corelation!");

    for (im = 0; im < size; im++)
       {
           switch_frame(dest,im);
	   imi1 = first_im[im];
	   imi1 = (imi1 < 0) ? 0 : imi1;
	   imi1 = (imi1 >= nfi) ? nfi-1 : imi1;
	   imi2 = second_im[im];
	   imi2 = (imi2 < 0) ? 0 : imi2;
	   imi2 = (imi2 >= nfi) ? nfi-1 : imi2;
           switch_frame(ois1,imi1);
           pd = dest->im.pixel;
           ps1 = ois1->im.pixel;

           ps2 = ois1->im.pxl[imi2];
           re0_0_1 	= ps1[0].fl[onx];
           renx2_0_1 	= ps1[0].fl[onx + 1];
           re0_ny2_1 	= ps1[0].fl[0];
           renx2_ny2_1 = ps1[0].fl[1];
           re0_0_2 	= ps2[0].fl[onx];
           renx2_0_2 	= ps2[0].fl[onx + 1];
           re0_ny2_2 	= ps2[0].fl[0];
           renx2_ny2_2 = ps2[0].fl[1];
           for (i=0, m1 = 0, m2 = 0; i< ony; i++)
              {
                 for (j=0; j< onx; j++)
                    {
                        re1 = ps1[i].fl[2*j];	re2 = ps2[i].fl[2*j];
                        im1 = ps1[i].fl[2*j+1];	im2 = ps2[i].fl[2*j+1];
                        m1 += re1 * re1 + im1 * im1;
                        m2 += re2 * re2 + im2 * im2;
                    }
              }
           m1 = sqrt(m1);
           m2 = sqrt(m2);
           m1 *= m2;
           if (normalize == 0) m1 = 1;
           for (i=0; i< ony; i++)
              {
                   for (j=0; j< onx; j++)
                      {
                        re1 = ps1[i].fl[2*j];	re2 = ps2[i].fl[2*j];
                        im1 = ps1[i].fl[2*j+1];	im2 = ps2[i].fl[2*j+1];
                        pd[i].fl[2*j] = (re1 * re2 + im1 * im2)/m1;
                        pd[i].fl[2*j+1] = (im1 * re2 - re1 * im2)/m1;
                      }
              }
           pd[0].fl[onx] 	= 2*(re0_0_1 * re0_0_2)/m1 ;
           pd[0].fl[onx + 1] 	= 2*(renx2_0_1 * renx2_0_2)/m1;
           pd[0].fl[0] 		= 2*(re0_ny2_1 * re0_ny2_2 )/m1;
           pd[0].fl[1] 		= 2*(renx2_ny2_1 * renx2_ny2_2)/m1;
	   dest->im.user_ispare[im][2] = imi1;
	   dest->im.user_ispare[im][3] = imi2;
       }
    inherit_from_im_to_im(dest,ois1);
    dest->im.mode = LOG_AMP;
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;
    set_im_title(dest, "\\stack{{corelation of movie}{%s im from arrays}}",
                 (ois1->im.source != NULL) ? 	ois1->im.source : "untitled");
    set_formated_string(&dest->im.treatement,
                        "sloppy fft2d image : corelation of movie %s from arrays",
                        (ois1->im.source != NULL) ? 	ois1->im.source : "untitled");
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}



/*  correlate a movie either to one specific frame ref_im or between neighbour if ref_im < 0
 *		destination may be NULL or one of the images
 *
 */
O_i 	*corel_2d_movie_in_time(O_i *dest, O_i *ois1, int ref_im, int normalize)
{
    int i, j, im;
    int  onx, ony, nfi, nfid;
    float re1, re2, im1, im2, m1, m2;
    float re0_0_1, renx2_0_1, re0_ny2_1, renx2_ny2_1;
    float re0_0_2, renx2_0_2, re0_ny2_2, renx2_ny2_2;
    union pix *pd, *ps1, *ps2;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No source 1 image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    nfi = abs(ois1->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;
    if (nfi < 2) return win_printf_ptr("the image 1 is not a movie!\n");
    if (ois1->im.data_type != IS_COMPLEX_IMAGE)
        return win_printf_ptr ("the image 1 is not of complex type!\n"
                    "Cannot perform corelation!");
    if ((ois1->im.treatement == NULL) || strncmp(ois1->im.treatement,
                                                 "sloppy fft2d image",18) != 0)
        return win_printf_ptr ("the image 1 does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
    if (ref_im >= nfi) return win_printf_ptr("ref frame %d out of range [-1,%d[",ref_im,nfi);
    dest = (dest != NULL) ? dest : create_one_movie(onx, ony, IS_COMPLEX_IMAGE, (ref_im < 0) ? nfi-1 : nfi);
    if (dest == NULL)
        return win_printf_ptr("Can't create dest image");
    if (dest->im.data_type != IS_COMPLEX_IMAGE)
        return win_printf_ptr("the destination image is not of complex type!\n"
                    "Cannot perform corelation!");
    if (ony != dest->im.ny || onx != dest->im.nx)
        return win_printf_ptr ("The destination size differs from the source one \n"
                    "Cannot perform corelation!");
    nfid = abs(dest->im.n_f);
    nfid = (nfid == 0) ? 1 : nfid;
    if (nfid < nfi - ((ref_im < 0) ? 1 : 0))
        return win_printf_ptr ("The destination movie size differs from the source one \n"
                    "Cannot perform corelation!");

    for (im = 0; im < nfi; im++)
       {
	   if (ref_im < 0 && im >= nfi - 2) continue;
           switch_frame(dest,im);
           switch_frame(ois1,im);
           pd = dest->im.pixel;
           ps1 = ois1->im.pixel;
           ps2 = (ref_im < 0) ? ois1->im.pxl[im+1] : ois1->im.pxl[ref_im];
           re0_0_1 	= ps1[0].fl[onx];
           renx2_0_1 	= ps1[0].fl[onx + 1];
           re0_ny2_1 	= ps1[0].fl[0];
           renx2_ny2_1 = ps1[0].fl[1];
           re0_0_2 	= ps2[0].fl[onx];
           renx2_0_2 	= ps2[0].fl[onx + 1];
           re0_ny2_2 	= ps2[0].fl[0];
           renx2_ny2_2 = ps2[0].fl[1];
           for (i=0, m1 = 0, m2 = 0; i< ony; i++)
              {
                 for (j=0; j< onx; j++)
                    {
                        re1 = ps1[i].fl[2*j];	re2 = ps2[i].fl[2*j];
                        im1 = ps1[i].fl[2*j+1];	im2 = ps2[i].fl[2*j+1];
                        m1 += re1 * re1 + im1 * im1;
                        m2 += re2 * re2 + im2 * im2;
                    }
              }
           m1 = sqrt(m1);
           m2 = sqrt(m2);
           m1 *= m2;
           if (normalize == 0) m1 = 1;
           for (i=0; i< ony; i++)
              {
                   for (j=0; j< onx; j++)
                      {
                        re1 = ps1[i].fl[2*j];	re2 = ps2[i].fl[2*j];
                        im1 = ps1[i].fl[2*j+1];	im2 = ps2[i].fl[2*j+1];
                        pd[i].fl[2*j] = (re1 * re2 + im1 * im2)/m1;
                        pd[i].fl[2*j+1] = (im1 * re2 - re1 * im2)/m1;
                      }
              }
           pd[0].fl[onx] 	= 2*(re0_0_1 * re0_0_2)/m1 ;
           pd[0].fl[onx + 1] 	= 2*(renx2_0_1 * renx2_0_2)/m1;
           pd[0].fl[0] 		= 2*(re0_ny2_1 * re0_ny2_2 )/m1;
           pd[0].fl[1] 		= 2*(renx2_ny2_1 * renx2_ny2_2)/m1;
       }
    inherit_from_im_to_im(dest,ois1);
    dest->im.mode = LOG_AMP;
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;
    set_im_title(dest, "\\stack{{corelation of movie}{%s im and %s%d}}",
                 (ois1->im.source != NULL) ? 	ois1->im.source : "untitled",
		 (ref_im < 0) ? "im+":"", (ref_im < 0) ? 1: ref_im );
    set_formated_string(&dest->im.treatement,
                        "sloppy fft2d image : corelation of movie %s im an  %s%d",
                        (ois1->im.source != NULL) ? 	ois1->im.source : "untitled",
			(ref_im < 0) ? "im+":"", (ref_im < 0) ? 1: ref_im );
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}




# define COREL_NORM_SQ1_SQ2 0
# define COREL_NORM_S1 1
# define COREL_NORM_S2 2
# define COREL_NORM_NONE 3

/*
 *		destination may be NULL or one of the images
 *
 */
O_i 	*corel_2d_im_norm(O_i *dest, O_i *ois1, O_i *ois2, int norm)
{
    int i, j;
    int  onx, ony;
    float re1, re2, im1, im2, m1, m2;
    float re0_0_1, renx2_0_1, re0_ny2_1, renx2_ny2_1;
    float re0_0_2, renx2_0_2, re0_ny2_2, renx2_ny2_2;
    union pix *pd, *ps1, *ps2;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No source 1 image found");
        return (NULL);
    }
    if ( ois2 == NULL|| ois2->im.pixel == NULL)
    {
        win_printf("No source 2 image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    if (ois1->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image 1 is not of complex type!\n"
                    "Cannot perform corelation!");
        return(NULL);
    }
    if ((ois1->im.treatement == NULL) || strncmp(ois1->im.treatement,
                                                 "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image 1 does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if (ois2->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image 2 is not of complex type!\n"
                    "Cannot perform corelation!");
        return(NULL);
    }
    if (ony != ois2->im.ny || onx != ois2->im.nx)
    {
        win_printf ("The size of the second image differs from the source one \n"
                    "Cannot perform corelation!");
        return(NULL);
    }
    if ((ois2->im.treatement == NULL) || strncmp(ois2->im.treatement,
                                                 "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image 2 does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }

    dest = (dest != NULL) ? dest : create_one_image(onx, ony, IS_COMPLEX_IMAGE);	if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }
    if (ois2->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the destination image is not of complex type!\n"
                    "Cannot perform corelation!");
        return(NULL);
    }
    if (ony != dest->im.ny || onx != dest->im.nx)
    {
        win_printf ("The destination size differs from the source one \n"
                    "Cannot perform corelation!");
        return(NULL);
    }

    pd = dest->im.pixel;	ps1 = ois1->im.pixel;	ps2 = ois2->im.pixel;
    re0_0_1 	= ps1[0].fl[onx];
    renx2_0_1 	= ps1[0].fl[onx + 1];
    re0_ny2_1 	= ps1[0].fl[0];
    renx2_ny2_1 = ps1[0].fl[1];
    re0_0_2 	= ps2[0].fl[onx];
    renx2_0_2 	= ps2[0].fl[onx + 1];
    re0_ny2_2 	= ps2[0].fl[0];
    renx2_ny2_2 = ps2[0].fl[1];
    for (i=0, m1 = 0, m2 = 0; i< ony; i++)
    {
        for (j=0; j< onx; j++)
        {
            re1 = ps1[i].fl[2*j];	re2 = ps2[i].fl[2*j];
            im1 = ps1[i].fl[2*j+1];	im2 = ps2[i].fl[2*j+1];
            m1 += re1 * re1 + im1 * im1;
            m2 += re2 * re2 + im2 * im2;
        }
    }
    if (norm == 0)
    {
        m1 = sqrt(m1);
        m2 = sqrt(m2);
        m1 *= m2;
    }
    else if (norm == 1)
    {
        m1 = sqrt(m1);
        m1 *= m1;
    }
    else if (norm == 2)
    {
        //m1 = sqrt(m1);
        //m2 = sqrt(m2);
        m1 = m2;
    }
    else if (norm == 3)
    {
        m1 = 1;
    }

    for (i=0; i< ony; i++)
    {
        for (j=0; j< onx; j++)
        {
            re1 = ps1[i].fl[2*j];	re2 = ps2[i].fl[2*j];
            im1 = ps1[i].fl[2*j+1];	im2 = ps2[i].fl[2*j+1];
            pd[i].fl[2*j] = (re1 * re2 + im1 * im2)/m1;
            pd[i].fl[2*j+1] = (im1 * re2 - re1 * im2)/m1;
        }
    }
    pd[0].fl[onx] 		= 2*(re0_0_1 * re0_0_2)/m1 ;
    pd[0].fl[onx + 1] 	= 2*(renx2_0_1 * renx2_0_2)/m1;
    pd[0].fl[0] 		= 2*(re0_ny2_1 * re0_ny2_2 )/m1;
    pd[0].fl[1] 		= 2*(renx2_ny2_1 * renx2_ny2_2)/m1;

    inherit_from_im_to_im(dest,ois1);
    dest->im.mode = LOG_AMP;
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;
    set_im_title(dest, "\\stack{{corelation of images}{%s and %s}}",
                 (ois1->im.source != NULL) ? 	ois1->im.source : "untitled",
                 (ois2->im.source != NULL) ? 	ois2->im.source : "untitled");
    set_formated_string(&dest->im.treatement,
                        "sloppy fft2d image : corelation of images %s and %s",
                        (ois1->im.source != NULL) ? 	ois1->im.source : "untitled",
                        (ois2->im.source != NULL) ? 	ois2->im.source : "untitled");
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}


/*	compute the forward fft of a real image
 *	-dest is the result, if dest == NULL then it is created
 *	-ois is the source image
 *	-bmp is the BITMAP to print message
 *		if NULL, no message print during process
 *	-smp is a small number tipically .05 in the Hanning window
 *		if smp = 0 the reverse operation is impossible
 *	fx low_pass cut_off in x fy lowpass filter in y
 */
O_i 	*forward_fft_2d_im_filter(O_i *dest, O_i *ois, BITMAP *bmp, float smp, int fx, int fy)
{
    int i, j;
    int nout = 0, onx, ony, no;
    float *tmp;
    union pix *pd;
    O_i  *oid;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;
    if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        win_printf ("Cannot treat a complex image yet!");
        return(NULL);
    }
    if (dest != NULL)		/*	if dest exists check that destination is OK */
    {
        if (dest->im.data_type != IS_COMPLEX_IMAGE ||
            dest->im.nx != onx || dest->im.ny != ony/2)
        {
            free_one_image(dest);
            dest = NULL;
        }
    }
    oid = (dest != NULL) ? dest : create_one_image(onx, ony/2, IS_COMPLEX_IMAGE);
    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }

    pd = oid->im.pixel;
    tmp = (float*)calloc(ony,sizeof(float));	/* fft on row */
    if ( tmp == NULL )
    {
        win_printf ("Can't create tmp");
        return (NULL);
    }
    if (fft_init(ony) || filter_init(ony))
    {
        free(tmp);
        free_one_image(oid);
        win_printf("Can't initialize fft");
        return (NULL);
    }
    for (j=0; j< ois->im.nx; j++)
    {
        no = ony * j;
        if ((bmp != NULL) && (no >= nout + 16384))
        {	/* say how much done ~ every second*/
            display_title_message("fft row %d",j);
        }


        extract_raw_row (ois, j, tmp);
        if (!(ois->im.win_flag & Y_PER))	fftwindow1(ony, tmp, smp);
        realtr1(ony, tmp);
        fft(ony, tmp, 1);
        realtr2(ony, tmp, 1);
        lowpass_smooth_half (ony, tmp, fy);
        for (i = 0; i< ony/2 ; i++)
        {
            pd[i].fl[2*j] = tmp[2*i];		/* real part */
            pd[i].fl[2*j+1] = tmp[2*i+1];	/* imaginary part */
        }
    }
    if (fft_init(onx) || fft_init(2*onx) || filter_init(onx) || filter_init(2*onx))
    {	/* fft on line */
        free(tmp);
        free_one_image(oid);
        win_printf("Can't initialize fft");
        return (NULL);
    }
    tmp = (float*)realloc(tmp,2*onx*sizeof(float));
    if ( tmp == NULL )
    {
        free_one_image(oid);
        win_printf ("Can't create tmp");
        return (NULL);
    }
    for (j=1, nout = 0; j< ony/2; j++)	/* ! start on mode k = 1	*/
    {
        no = 2*onx * j;
        if ((bmp != NULL) && (no >= nout + 16384))
        {	/* say how much done ~ every second*/
            display_title_message("fft line %d",j);
        }

        for (i = 0; i< onx ; i++)
        {
            tmp[2*i] = pd[j].fl[2*i];
            tmp[2*i+1] = pd[j].fl[2*i+1];
        }
        if (!(ois->im.win_flag & X_PER))	fftwc1(2*onx, tmp,  smp);
        fft(2*onx, tmp, 1);
        lowpass_smooth_sym (2*onx, tmp, fx);
        for (i = 0; i< onx/2 ; i++)		/* order from -Nx/2 to Nx/2 */
        {
            pd[j].fl[onx+2*i] = tmp[2*i];
            pd[j].fl[onx+2*i+1] = tmp[2*i+1];
            pd[j].fl[2*i] = tmp[onx+2*i];
            pd[j].fl[2*i+1] = tmp[onx+2*i+1];
        }
    }
    fft_init(onx);		/* treat ky = 0 and ky = Ny/2 */
    filter_init(onx);
    for (i = 0; i< onx ; i++)
    {
        tmp[onx+i] = pd[0].fl[2*i];	/* all real part ky = 0		*/
        tmp[i] = pd[0].fl[2*i+1];	/* all real part ky = ny/2 	*/
    }
    if (!(ois->im.win_flag & X_PER))
    {
        fftwc1(onx, tmp,  smp);
        fftwc1(onx, tmp + onx,  smp);
    }
    realtr1(onx, tmp);
    fft(onx, tmp, 1);
    realtr2(onx, tmp, 1);
    realtr1(onx, tmp + onx);
    fft(onx, tmp + onx, 1);
    realtr2(onx, tmp + onx, 1);
    lowpass_smooth_half (onx, tmp, fx);
    lowpass_smooth_half (onx, tmp + onx, fx);
    for (i = 0; i< onx ; i++)
    {
        pd[0].fl[onx + i] 	= tmp[onx+i];	/* all real part ky = 0		*/
        pd[0].fl[i] 		= tmp[i];		/* all real part ky = ny/2 	*/
    }
    /*	the ky = 0 mode are nearly well ordered except (kx = 0, kx = Nx/2) */
    /*	the ky = Ny/2 mode are ordered in inverse order */
    free(tmp);
    inherit_from_im_to_im(oid,ois);
    uns_oi_2_oi(oid,ois);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    oid->im.win_flag = ois->im.win_flag;
    oid->im.mode = LOG_AMP;
    oid->ax = -onx/2;
    set_im_x_title(oid,"wavenumber k_x");
    set_im_y_title(oid,"frequency \\omega");
    set_im_title(oid, "\\stack{{fft 2D of image}{%s}}",(ois->title != NULL)
                 ? ois->title : ((ois->im.source != NULL) ? ois->im.source : "untitled"));
    set_formated_string(&oid->im.treatement,"sloppy fft2d image of %s lowpass %d in x %d in y",
                        (ois->title != NULL) ? ois->title : ((ois->im.source != NULL)
                                                             ? ois->im.source : "untitled"),fx,fy);
    return oid;
}
O_i 	*multiply_im(O_i *dest, O_i *ois, double factor)
{
    int i, j;
    int  onx, ony;
    union pix *pd, *ps;
    float fac = (float)factor;
    O_i  *oid;

    if (ois == NULL || ois->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois->im.ny;	onx = ois->im.nx;
    if (dest != NULL)		/*	if dest exists check that destination is OK */
    {
        if (ois->im.data_type != IS_COMPLEX_IMAGE)
        {
            if (dest->im.data_type != IS_FLOAT_IMAGE ||
                dest->im.nx != onx || dest->im.ny != ony)
            {
                free_one_image(dest);
                dest = NULL;
            }
        }
        else
        {
            if (dest->im.data_type != IS_COMPLEX_IMAGE ||
                dest->im.nx != onx || dest->im.ny != ony)
            {
                free_one_image(dest);
                dest = NULL;
            }
        }
    }
    if (ois->im.data_type == IS_COMPLEX_IMAGE)
        oid = (dest != NULL) ? dest : create_one_image(onx, ony, IS_COMPLEX_IMAGE);
    else
        oid = (dest != NULL) ? dest : create_one_image(onx, ony, IS_FLOAT_IMAGE);
    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }

    pd = oid->im.pixel;
    ps = ois->im.pixel;
    if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        for (i=0; i< ony; i++)
        {
            for (j=0; j< 2*onx; j++)
                pd[i].fl[j] = ps[i].fl[j] * fac;
        }
    }
    else if (ois->im.data_type == IS_FLOAT_IMAGE)
    {
        for (i=0; i< ony; i++)
        {
            for (j=0; j< onx; j++)
                pd[i].fl[j] = ps[i].fl[j] * fac;
        }
    }
    else	if (ois->im.data_type == IS_INT_IMAGE)
    {
        for (i=0; i< ony; i++)
        {
            for (j=0; j< onx; j++)
                pd[i].fl[j] = (float)ps[i].in[j] * fac;
        }
    }
    else	if (ois->im.data_type == IS_CHAR_IMAGE)
    {
        for (i=0; i< ony; i++)
        {
            for (j=0; j< onx; j++)
                pd[i].fl[j] = (float)ps[i].ch[j] * fac;
        }
    }
    else
    {
        win_printf("wrong type of image");
        return NULL;
    }
    inherit_from_im_to_im(oid,ois);
    uns_oi_2_oi(oid,ois);
    oid->im.win_flag = ois->im.win_flag;
    set_im_title(oid, "\\stack{{image multiplied}{%s}}",(ois->title != NULL)
                 ? ois->title : ((ois->im.source != NULL) ? ois->im.source : "untitled"));
    set_formated_string(&oid->im.treatement,"%s image multiplied by %f",
                        (ois->title != NULL) ? ois->title : ((ois->im.source != NULL)
                                                             ? ois->im.source : "untitled"),fac);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return oid;
}


float find_min_modulus(O_i *ois1)
{
    int i, j;
    int  onx, ony;
    float min = 0, tmp = 0, re, im;
    union pix *ps1;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No image found");
        return -1;
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    if (ois1->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return -1;
    }
    if ((ois1->im.treatement == NULL) || strncmp(ois1->im.treatement,
                                                 "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return -1;
    }

    ps1 = ois1->im.pixel;

    for (i=0, min = FLT_MAX; i< ony; i++)
    {
        for (j=0; j<onx; j++)
        {
            if (j == 0 && i == 0)
            {
                tmp = ps1[i].fl[2*j];
                tmp *= tmp;
                min = (tmp < min) ? tmp : min;
                tmp = ps1[i].fl[2*j+1];
                tmp *= tmp;
                min = (tmp < min) ? tmp : min;
            }
            if (j == onx/2 && i == 0)
            {
                tmp = ps1[i].fl[2*j];
                tmp *= tmp;
                min = (tmp < min) ? tmp : min;
                tmp = ps1[i].fl[2*j+1];
                tmp *= tmp;
                min = (tmp < min) ? tmp : min;
            }
            else
            {
                re = ps1[i].fl[2*j];
                im = ps1[i].fl[2*j+1];
                tmp = re * re + im * im;
                min = (tmp < min) ? tmp : min;
            }
        }
    }
    return sqrt(min);

}


// find the max modulus for modes with k > high_pass_radius
float find_max_modulus(O_i *ois1, float high_pass_radius)
{
    int i, j;
    int  onx, ony;
    float max = 0, tmp = 0, re, im, k;
    union pix *ps1;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No image found");
        return -1;
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    if (ois1->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return -1;
    }
    if ((ois1->im.treatement == NULL) || strncmp(ois1->im.treatement,
                                                 "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return -1;
    }

    ps1 = ois1->im.pixel;

    for (i=0, max = 0; i< ony; i++)
    {
        for (j=0; j<onx; j++)
        {
            if (j == 0 && i == 0)
            {
                tmp = ps1[i].fl[2*j];
                tmp *= tmp;
                if (high_pass_radius <= 0)
                    max = (tmp < max) ? tmp : max;
                tmp = ps1[i].fl[2*j+1];
                tmp *= tmp;
                if (high_pass_radius <= 0)
                    max = (tmp < max) ? tmp : max;
            }
            if (j == onx/2 && i == 0)
            {
                tmp = ps1[i].fl[2*j];
                tmp *= tmp;
                k = sqrt(ony*ony + j*j);
                if (high_pass_radius <= k)
                    max = (tmp < max) ? tmp : max;
                tmp = ps1[i].fl[2*j+1];
                tmp *= tmp;
                k = sqrt(ony*ony + j*j);
                if (high_pass_radius <= k)
                    max = (tmp > max) ? tmp : max;
            }
            else
            {
                re = ps1[i].fl[2*j];
                im = ps1[i].fl[2*j+1];
                tmp = re * re + im * im;
                k = sqrt(i*i + ((onx/2-j)*(onx/2-j)));
                if (high_pass_radius <= k)
                    max = (tmp > max) ? tmp : max;
            }
        }
    }
    return sqrt(max);

}

/*
 *		dest = ois1 - ois2
 *
 */
O_i 	*multiply_2_fft_images(O_i *dest, O_i *ois1, O_i *ois2)
{
    int i, j;
    int  onx, ony;
    int	dest_type = 0;
    float re, im, ren, imn;
    union pix *pd, *ps1, *ps2;



    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    if (ois1->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if ((ois1->im.treatement == NULL) || strncmp(ois1->im.treatement,
                                                 "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }

    if (ois2 == NULL || ois2->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    if (ois2->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if ((ois2->im.treatement == NULL) || strncmp(ois2->im.treatement,
                                                 "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }


    if (ony != ois2->im.ny || onx != ois2->im.nx)
    {
        win_printf ("The size of the second image differs from the source one \n"
                    "Cannot perform multiplication!");
        return(NULL);
    }
    if (dest != NULL)
    {
        if (dest->im.data_type != IS_COMPLEX_IMAGE)
        {
            win_printf ("The destination image is not of complex type\n"
                        "Cannot perform multiplication!");
            return(NULL);
        }
        if (ony != dest->im.ny || onx != dest->im.nx)
        {
            win_printf ("The destination size differs from the source one \n"
                        "Cannot perform multiplication!");
            return(NULL);
        }
    }
    else
    {
        dest_type = IS_COMPLEX_IMAGE;
    }
    dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
    if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }


    pd = dest->im.pixel;	ps1 = ois1->im.pixel;	ps2 = ois2->im.pixel;


    for (i=0; i< ony; i++)
    {
        for (j=0; j<onx; j++)
        {
            if (j == 0 && i == 0)
            {
                pd[i].fl[2*j] = 2*ps1[i].fl[2*j]*ps2[i].fl[2*j];
                pd[i].fl[2*j+1] = 2*ps1[i].fl[2*j+1]*ps2[i].fl[2*j+1];
            }
            if (j == onx/2 && i == 0)
            {
                pd[i].fl[2*j] = 2*ps1[i].fl[2*j]*ps2[i].fl[2*j];
                pd[i].fl[2*j+1] = 2*ps1[i].fl[2*j+1]*ps2[i].fl[2*j+1];							}
            else
            {
                re = ps2[i].fl[2*j];
                im = ps2[i].fl[2*j+1];
                ren = ps1[i].fl[2*j];
                imn = ps1[i].fl[2*j+1];
                pd[i].fl[2*j] = (ren * re - im * imn);
                pd[i].fl[2*j+1] = (imn * re + im * ren);
            }
        }
    }

    inherit_from_im_to_im(dest,ois1);
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;

    dest->im.mode = LOG_AMP;
    dest->ax = -onx/2;
    set_im_x_title(dest,"wavenumber k_x");
    set_im_y_title(dest,"frequency \\omega");
    set_formated_string(&dest->im.treatement,"sloppy fft2d image of %s",
                        (ois1->title != NULL) ? ois1->title : ((ois1->im.source != NULL)
                                                               ? ois1->im.source : "untitled"));
    set_im_title(dest, "\\stack{{images multiplication}{%s and %s}}",
                 (ois1->im.source != NULL) ? 	ois1->im.source : "untitled",
                 (ois2->im.source != NULL) ? 	ois2->im.source : "untitled");
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}

/*
 *		dest = ois1 - ois2
 *
 */
O_i 	*divide_2_fft_images(O_i *dest, O_i *ois1, O_i *ois2, float min_div)
{
    int i, j;
    int  onx, ony;
    int	dest_type = 0;
    float re, im, ren, imn, tmp, norm_div;
    union pix *pd, *ps1, *ps2;



    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    if (ois1->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if ((ois1->im.treatement == NULL) || strncmp(ois1->im.treatement,
                                                 "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }

    if (ois2 == NULL || ois2->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    if (ois2->im.data_type != IS_COMPLEX_IMAGE)
    {
        win_printf ("the image is not of complex type!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }
    if ((ois2->im.treatement == NULL) || strncmp(ois2->im.treatement,
                                                 "sloppy fft2d image",18) != 0)
    {
        win_printf ("the image does not correspond to an fft!\n"
                    "Cannot perform backward fft!");
        return(NULL);
    }

    if (fabs(min_div) == 0)
    {
        win_printf("Min div cannot be set to zero");
        return (NULL);
    }

    if (ony != ois2->im.ny || onx != ois2->im.nx)
    {
        win_printf ("The size of the second image differs from the source one \n"
                    "Cannot perform division!");
        return(NULL);
    }
    if (dest != NULL)
    {
        if (dest->im.data_type != IS_COMPLEX_IMAGE)
        {
            win_printf ("The destination image is not of complex type\n"
                        "Cannot perform division!");
            return(NULL);
        }
        if (ony != dest->im.ny || onx != dest->im.nx)
        {
            win_printf ("The destination size differs from the source one \n"
                        "Cannot perform division!");
            return(NULL);
        }
    }
    else
    {
        dest_type = IS_COMPLEX_IMAGE;
    }
    dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
    if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }


    pd = dest->im.pixel;	ps1 = ois1->im.pixel;	ps2 = ois2->im.pixel;


    norm_div = min_div * min_div;
    for (i=0; i< ony; i++)
    {
        for (j=0; j<onx; j++)
        {
            if (j == 0 && i == 0)
            {
                tmp = ps2[i].fl[2*j];
                tmp = (fabs(tmp) < fabs(min_div)) ? min_div : tmp;
                pd[i].fl[2*j] = 2*ps1[i].fl[2*j]/tmp;
                tmp = ps2[i].fl[2*j+1];
                tmp = (fabs(tmp) < fabs(min_div)) ? min_div : tmp;
                pd[i].fl[2*j+1] = 2*ps1[i].fl[2*j+1]/tmp;
            }
            if (j == onx/2 && i == 0)
            {
                tmp = ps2[i].fl[2*j];
                tmp = (fabs(tmp) < fabs(min_div)) ? min_div : tmp;
                pd[i].fl[2*j] = 2*ps1[i].fl[2*j]/tmp;
                tmp = ps2[i].fl[2*j+1];
                tmp = (fabs(tmp) < fabs(min_div)) ? min_div : tmp;
                pd[i].fl[2*j+1] = 2*ps1[i].fl[2*j+1]/tmp;
            }
            else
            {
                re = ps2[i].fl[2*j];
                im = ps2[i].fl[2*j+1];
                ren = ps1[i].fl[2*j];
                imn = ps1[i].fl[2*j+1];
                tmp = re * re + im * im;
                tmp = (tmp < norm_div) ? norm_div : tmp;
                pd[i].fl[2*j] = (ren * re + im * imn)/tmp;
                pd[i].fl[2*j+1] = (imn * re - im * ren)/tmp;
            }
        }
    }

    inherit_from_im_to_im(dest,ois1);
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;

    dest->im.mode = LOG_AMP;
    dest->ax = -onx/2;
    set_im_x_title(dest,"wavenumber k_x");
    set_im_y_title(dest,"frequency \\omega");
    set_formated_string(&dest->im.treatement,"sloppy fft2d image of %s",
                        (ois1->title != NULL) ? ois1->title : ((ois1->im.source != NULL)
                                                               ? ois1->im.source : "untitled"));
    set_im_title(dest, "\\stack{{images division}{%s and %s}}",
                 (ois1->im.source != NULL) ? 	ois1->im.source : "untitled",
                 (ois2->im.source != NULL) ? 	ois2->im.source : "untitled");
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}

int		do_divide_2_fft_im(void)
{
    int i;
    static float min = 0.003, min_i, max_i, gain = 1000;
    O_i   *oid, *ois;
    int n_den;
    imreg *imr;
    char question[1024];

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    if ((imr->cur_oi - 1) < 0)
        return win_printf_OK("I need at least two images!");
    n_den = imr->cur_oi + 1;
    i = win_scanf("I will divide two fft images\n"
                  "select the denominator image %d",&n_den);
    n_den = (n_den < 0) ? 0 : n_den;
    n_den %= imr->n_oi;
    min_i = find_min_modulus(imr->o_i[n_den]);
    if (min_i < 0)	return win_printf_OK("something went wrong in min modulus!");
    max_i = find_max_modulus(imr->o_i[n_den],2.0);
    if (max_i < 0)	return win_printf_OK("something went wrong in max modulus!");
    sprintf (question,"Minimum modulus of denominator image %d = %g\n"
             "maximum modulus of denominator image = %g max ratio %g\n"
             "define the maximum gain value to saturate division"
             "%%f",
             n_den,min_i,max_i,(min_i>0)?max_i/min_i:1);

    i = win_scanf(question,&gain);
    if (i == WIN_CANCEL)	return D_O_K;
    if (gain <= 0) win_printf_OK("Impossible gain value");
    min = max_i/gain;
    oid = divide_2_fft_images(NULL, ois, imr->o_i[n_den],  min);
    if (oid == NULL)	return D_O_K;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}

int		do_multiply_2_fft_im(void)
{
    int i;
    O_i   *oid, *ois;
    int n_den;
    imreg *imr;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    if ((imr->cur_oi - 1) < 0)
        return win_printf_OK("I need at least two images!");
    n_den = imr->cur_oi + 1;
    i = win_scanf("I will multiply two fft images\n"
                  "select the second image %d",&n_den);
    if (i == WIN_CANCEL)	return D_O_K;
    n_den = (n_den < 0) ? 0 : n_den;
    n_den %= imr->n_oi;

    oid = multiply_2_fft_images(NULL, ois, imr->o_i[n_den]);
    if (oid == NULL)	return D_O_K;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}


//# ifdef EN_COURS


/*    generates a spot in an image oi with a given radial profile specified in a dataset ds
      the spot position may be fractional.
      The profile should be given with an enhanced resolution than the image so that
      the linear interpolation will be fine. (dx = 1/8 is a good choice)
      */



int add_spot_of_profile_to_image(O_i *oi,       // the image to put the spot in
                                 float xcf, float ycf, // the spot position in pixels
                                 float dx,             // the extend of one spot pixel in image pixel
                                 d_s *ds)              // the spot profile
{
    int i, j, k;
    float x, y, x2, y2, r, fr, dx_1, val;
    int onx, ony, pnx, k1;
    union pix  *pd;

    if (oi == NULL) return 1;
    if (oi->im.data_type != IS_FLOAT_IMAGE && oi->im.data_type != IS_INT_IMAGE
        && oi->im.data_type != IS_INT_IMAGE)
        return win_printf_OK("Wrong image type! cannot add spot");

    onx = oi->im.nx;   ony = oi->im.ny;
    pnx = 1 + (int)ds->nx*dx;
    dx_1 = (dx > 0) ? (float)1/dx : 1;


    for (i = 0, pd = oi->im.pixel; i < ony ; i++)
    {
        y = fabs(ycf - i);
        if (y > pnx) continue;
        y2 = y*y;
        for (j = 0; j< onx; j++)
        {
            x = fabs(xcf - j);
            if (x > pnx) continue;
            x2 = x*x;
            r = sqrt(x2 + y2);     // r is distance from pixel to bead center in microns
            if (r > pnx) continue;
            r *= dx_1; // this is the distance in pixels
            k = (int)r;
            k = (k < ds->nx) ? k : ds->nx - 1;
            k1 = k + 1;
            k1 = (k1 < ds->nx) ? k1 : ds->nx - 1;
            fr = r - k;
            val = (1-fr)*ds->yd[k] + fr*ds->yd[k1];
            if (oi->im.data_type == IS_FLOAT_IMAGE) pd[i].fl[j] += val;
            else if (oi->im.data_type == IS_INT_IMAGE) pd[i].in[j] += (short int)val;
            else if (oi->im.data_type == IS_INT_IMAGE) pd[i].ch[j] += (unsigned char)val;
        }
    }
    oi->need_to_refresh = ALL_NEED_REFRESH;
    return 0;
}



int	do_add_spot_of_profile_to_image(void)
{
    int i;
    O_i   *ois;
    O_p *op;
    imreg *imr;
    static float  xc = 0, yc = 0, dx = 0.1;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
        return win_printf_OK("This routine add a spot to an image\n"
                             "it requires an image with a plot containing\n"
                             "the spot profile (expanded)\n");

    if (ois->cur_op < 0)
        return win_printf_OK("This routine add a spot to an image\n"
                             "it requires an image with a plot containing\n"
                             "the spot profile (expanded)\n");
    op = ois->o_p[imr->one_i->cur_op];

    i = win_scanf("Add a new spot to an imagen"
                  "Position of the spot x %8f y %8f\n"
                  "distance between two consecutive points in profile"
                  "in image pixel unit dx = %10f\n"
                  ,&xc,&yc,&dx);
    if (i == WIN_CANCEL) return  D_O_K;
    add_spot_of_profile_to_image(ois, xc, yc, dx,	op->dat[op->cur_dat]);
    ois->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}


int	create_blank_image(void)
{
    int i, j;
    O_i   *oid, *ois;
    imreg *imr;
    static int onx = 512, ony = 512, type = 0;
    static float val = 127, mod = 127;
    union pix  *pd;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
    if (ois != NULL)
    {
        onx = ois->im.nx;
        ony = ois->im.ny;
        if (ois->im.data_type == IS_CHAR_IMAGE) type = 0;
        else       if (ois->im.data_type == IS_INT_IMAGE) type = 1;
        else       if (ois->im.data_type == IS_FLOAT_IMAGE) type = 2;
    }

    i = win_scanf("Construction of a Blank image\n"
                  "Dtata type 8 bits %R 16 bits %r float %r\n"
                  "Size nx %8d ny %8d value %12f\n"
                  "High modulation amp %12f\n"
                  ,&type,&onx,&ony,&val,&mod);
    if (i == WIN_CANCEL) return  D_O_K;
    if (type == 0)
    {
        oid = create_and_attach_oi_to_imr(imr, onx, ony, IS_CHAR_IMAGE);
        if (oid == NULL)	return win_printf_OK("cannot create profile !");
        for (i = 0, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j< onx; j++)
                pd[i].ch[j] = ((i&0x01)^(j&0x01)) ? (unsigned char)(val - mod) : (unsigned char)(val + mod);
        }
    }
    else if (type == 1)
    {
        oid = create_and_attach_oi_to_imr(imr, onx, ony, IS_INT_IMAGE);
        if (oid == NULL)	return win_printf_OK("cannot create profile !");
        for (i = 0, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j< onx; j++)
                pd[i].in[j] = ((i&0x01)^(j&0x01)) ? (short int)(val - mod) : (short int)(val + mod);
        }
    }
    else if (type == 2)
    {
        oid = create_and_attach_oi_to_imr(imr, onx, ony, IS_FLOAT_IMAGE);
        if (oid == NULL)	return win_printf_OK("cannot create profile !");
        for (i = 0, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j< onx; j++)
                pd[i].fl[j] = ((i&0x01)^(j&0x01)) ? (val - mod) : (val + mod);
        }
    }
    else return 0;
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}



int	create_image_with_gaussian_spot(void)
{
  int i, j, k;
    O_i   *oid, *ois;
    O_p *op = NULL;
    d_s *ds = NULL;
    imreg *imr;
    float tmp;
    static int onx = 512, ony = 512;
    static float val = 0, mod = 127, sig = 3.5, x0 = 256, y0 = 256;
    union pix  *pd;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;

    if (ois != NULL)
    {
        onx = ois->im.nx;
        ony = ois->im.ny;
    }

    if (ois->n_op > 0 && ois->cur_op < ois->n_op)
      {
	op = ois->o_p[ois->cur_op];
	ds = op->dat[op->cur_dat];
	i = win_scanf("Construction of a Blank image with a Gaussian spots\n"
		      "Image Size nx %8d ny %8d \nvalue of zero %12f\n"
		      "Amplitude of Gaussian %12f\n"
		      "\\sigma  size %8f in pixels\n"
		      "Spots position taken from ds\n"
		      ,&onx,&ony,&val,&mod,&sig);
	if (i == WIN_CANCEL) return  D_O_K;
	oid = create_and_attach_oi_to_imr(imr, onx, ony, IS_FLOAT_IMAGE);
	if (oid == NULL)	return win_printf_OK("cannot create profile !");
	for (i = 0, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j = 0; j< onx; j++)
		pd[i].fl[j] = val;
	  }
	for (k = 0; k < ds->nx; k++)
	  {
	    x0 = ds->xd[k];
	    y0 = ds->yd[k];
	    int xs, xe, ys, ye;
	    ys = (int)(y0-8*sig);
	    ye = 1+(int)(y0+8*sig);
	    for (i = ys, pd = oid->im.pixel; i < ye ; i++)
	      {
		if (i < 0 || i >= ony) continue;
		xs = (int)(x0-8*sig);
		xe = 1+(int)(x0+8*sig);
		for (j = xs; j< xe; j++)
		  {
		    if (j < 0 || j >= onx) continue;
		    tmp = (y0 - i) * (y0 - i) + (x0 - j) * (x0 - j);
		    tmp /= sig * sig * 2;
		    pd[i].fl[j] +=  mod * exp(-tmp);
		  }
	      }
	  }
	set_oi_source(oid,"Gaussian spots (%d) with sigma = %g, amp = %g offset %g",ds->nx,sig,mod,val);


      }
    else
      {

	i = win_scanf("Construction of a Blank image with a Gaussian spot\n"
		      "Size nx %8d ny %8d \nvalue of zero %12f\n"
		      "Amplitude of Gaussian %12f\n"
		      "\\sigma  size %8f in pixels\n"
		      "Spot position x_0 %8f y_0 %8f\n"
		      ,&onx,&ony,&val,&mod,&sig,&x0,&y0);
	if (i == WIN_CANCEL) return  D_O_K;
	oid = create_and_attach_oi_to_imr(imr, onx, ony, IS_FLOAT_IMAGE);
	if (oid == NULL)	return win_printf_OK("cannot create profile !");
	for (i = 0, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j = 0; j< onx; j++)
	      {
		tmp = (y0 - i) * (y0 - i) + (x0 - j) * (x0 - j);
		tmp /= sig * sig * 2;
		pd[i].fl[j] = val + mod * exp(-tmp);
	      }
	  }
	set_oi_source(oid,"Gaussian spot in x_0 = %g, y_0 = %g with sigma = %g, amp = %g offset %g",x0,y0,sig,mod,val);
      }

    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}

O_i *create_appodisation_variable_size_of_float_image(int onx, int ony, int border_x, int border_y, float smp, int desapo)
{
    int i, j;
    int nx_2, ny_2, x, y; // , nxy
    float smp_1;//, rxy;
    O_i *oid;
    union pix *pd;

    oid = create_one_image(onx, ony, IS_FLOAT_IMAGE);
    if (oid == NULL)	return (O_i *) win_printf_ptr("cannot create image !");
    nx_2 = onx/2; ny_2 = ony/2;
    //nxy = nx_2 + ny_2;
    //rxy = sqrt(nx_4 * nx_4 +ny_4 * ny_4);
    smp_1 = 1 - (2 * smp);
    for (i = 0, pd = oid->im.pixel; i < ony ; i++)
    {
        y = (i < ny_2) ? i : ony - i;
        for (j = 0; j< onx; j++)
        {
            x = (j < nx_2) ? j : onx - j;
            if (x < border_x && y < border_y)
                pd[i].fl[j] = 0.25 * (1 - smp_1 * cos((M_PI * y)/border_y)) * (1 - smp_1 * cos((M_PI * x)/border_x));
            else if (x < border_x)   pd[i].fl[j] = 0.5 * (1 - smp_1 * cos((M_PI * x)/border_x));
            else if (y < border_y)   pd[i].fl[j] = 0.5 * (1 - smp_1 * cos((M_PI * y)/border_y));
            else	                   pd[i].fl[j] = 1 - smp;
        }
    }
    if (desapo)
    {
        for (i = 0, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j< onx; j++)
                pd[i].fl[j] = (pd[i].fl[j] != 0) ? (float)1/pd[i].fl[j] : pd[i].fl[j];
        }
    }
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return oid;
}


O_i *create_appodisation_float_image(int onx, int ony, float smp, int desapo)
{
    int i, j;
    int nx_2, ny_2, nx_4, ny_4, x, y; // , nxy
    float smp_1;//, rxy;
    O_i *oid;
    union pix *pd;

    oid = create_one_image(onx, ony, IS_FLOAT_IMAGE);
    if (oid == NULL)	return (O_i *) win_printf_ptr("cannot create image !");
    nx_2 = onx/2; ny_2 = ony/2;
    nx_4 = onx/4; ny_4 = ony/4;
    //nxy = nx_2 + ny_2;
    //rxy = sqrt(nx_4 * nx_4 +ny_4 * ny_4);
    smp_1 = 1 - (2 * smp);
    for (i = 0, pd = oid->im.pixel; i < ony ; i++)
    {
        y = (i < ny_2) ? i : ony - i;
        for (j = 0; j< onx; j++)
        {
            x = (j < nx_2) ? j : onx - j;
	    pd[i].fl[j] = (x < nx_4) ? 0.5 * (1 - (smp_1 * cos((M_PI * x)/nx_4))) : 0.5*(1+smp_1);
	    pd[i].fl[j] *= (y < ny_4) ? 0.5 * (1 - (smp_1 * cos((M_PI * y)/ny_4))) : 0.5*(1+smp_1);
	    /*
            if (x < nx_4)
            {
                if (y < x)  pd[i].fl[j] = 0.5 * (1 - smp_1 * cos((M_PI * y)/ny_4));
                else  pd[i].fl[j] = 0.5 * (1 - (smp_1 * cos((M_PI * x)/nx_4)));
            }
            else
            {
                if (y < ny_4)  pd[i].fl[j] = 0.5 * (1 - (smp_1 * cos((M_PI * y)/ny_4)));
                else           pd[i].fl[j] = 1 - smp;

            }
	    */
            /*
               if ((j -i) >= 0)
               {
               if ((j + i - nxy) >= 0)  pd[i].fl[j] = 0.5 * (1 - smp_1 * cos((M_PI * j)/nx_2));
               else                     pd[i].fl[j] = 0.5 * (1 - smp_1 * cos((M_PI * i)/ny_2));
               }
               else
               {
               if ((j + i - nxy) >= 0)  pd[i].fl[j] = 0.5 * (1 - smp_1 * cos((M_PI * i)/ny_2));
               else                     pd[i].fl[j] = 0.5 * (1 - smp_1 * cos((M_PI * j)/nx_2));
               }
               */

        }
    }
    if (desapo)
    {
        for (i = 0, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j< onx; j++)
                pd[i].fl[j] = (pd[i].fl[j] != 0) ? (float)1/pd[i].fl[j] : pd[i].fl[j];
        }
    }
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return oid;
}



int	do_create_appodisation_float_image(void)
{
    int i;
    O_i   *oid, *ois;
    imreg *imr;
    static int onx = 512, ony = 512;
    static float smp = 0.05;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
    if (ois != NULL)
    {
        onx = ois->im.nx;
        ony = ois->im.ny;
    }

    i = win_scanf("Construction of an appodisation image\n"
                  "Size nx %8d ny %8d\n non vanishing border value %12f\n",&onx,&ony,&smp);
    if (i == WIN_CANCEL) return  D_O_K;
    oid = create_appodisation_float_image(onx, ony, smp, 0);
    add_image(imr, oid->im.data_type, (void*)oid);
    if (oid == NULL)	return win_printf_OK("cannot create image !");
    return (refresh_image(imr, imr->n_oi - 1));
}

//# endif




/*
 *		dest = ois1 - ois2
 *
 */
O_i 	*undersample_image_of_char(O_i *dest, O_i *ois1, int bin)
{
    int i, j, im, ii, jj;
    int  onx, ony, nf, nfd, nx, ny;
    int	dest_type = IS_UINT_IMAGE;
    union pix *pd = NULL, *ps1 = NULL;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No image found in undersample_image_of_char");
        return (NULL);
    }
    if (bin == 0)
    {
        win_printf("bin = 0 forbidden");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    nx = onx/bin;
    ny = ony/bin;
    nf = ois1->im.n_f;
    if (nf < 1) nf = 1;
    if (ois1->im.data_type != IS_CHAR_IMAGE)
    {
        win_printf ("the image is not of float type!\n"
                    "Cannot transform!");
        return(NULL);
    }

    if (dest != NULL)
    {
        if (dest->im.data_type != IS_UINT_IMAGE)
        {
            win_printf ("The destination image is not of unsigned short int type\n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        if (ny != dest->im.ny || nx != dest->im.nx)
        {
            win_printf ("The destination size differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        nfd = dest->im.n_f;
        if (nfd < 1) nfd = 1;
        if (nf != nfd)
        {
            win_printf ("The destination nb of frame differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }

    }
    else
        dest_type = IS_UINT_IMAGE;

    if (dest == NULL)
      {
	if (nf == 1)     dest =  create_one_image(nx, ny, dest_type);
	else dest = create_one_movie(nx, ny, dest_type, nf);
      }
    if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }
    for (im = 0; im < nf; im++)
    {
        switch_frame(ois1,im);
        switch_frame(dest,im);
        pd = dest->im.pixel;	ps1 = ois1->im.pixel;
        for (i=0; i< ny; i++)
        {
            for (j=0; j<nx; j++)
            {
	      pd[i].ui[j] = 0;
            }
        }
        for (i=0; i< ony; i++)
        {
	    ii = i/bin;
            for (j=0; j<onx; j++)
            {
	        jj = j/bin;
		pd[ii].ui[jj] += ps1[i].ch[j];
            }
        }
    }
    inherit_from_im_to_im(dest,ois1);
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}



/*
 *		dest = ois1 - ois2
 *
 */
O_i 	*undersample_image_of_char_in_y(O_i *dest, O_i *ois1, int bin)
{
    int i, j, im, ii, jj;
    int  onx, ony, nf, nfd, nx, ny;
    int	dest_type = IS_UINT_IMAGE;
    union pix *pd = NULL, *ps1 = NULL;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No image found in undersample_image_of_char_in_y");
        return (NULL);
    }
    if (bin == 0)
    {
        win_printf("bin = 0 forbidden");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    nx = onx;
    ny = ony/bin;
    nf = ois1->im.n_f;
    if (nf < 1) nf = 1;
    if (ois1->im.data_type != IS_CHAR_IMAGE)
    {
        win_printf ("the image is not of float type!\n"
                    "Cannot transform!");
        return(NULL);
    }

    if (dest != NULL)
    {
        if (dest->im.data_type != IS_UINT_IMAGE)
        {
            win_printf ("The destination image is not of unsigned short int type\n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        if (ny != dest->im.ny || nx != dest->im.nx)
        {
            win_printf ("The destination size differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        nfd = dest->im.n_f;
        if (nfd < 1) nfd = 1;
        if (nf != nfd)
        {
            win_printf ("The destination nb of frame differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }

    }
    else
        dest_type = IS_UINT_IMAGE;

    if (dest == NULL)
      {
	if (nf == 1)    dest = create_one_image(nx, ny, dest_type);
	else dest = create_one_movie(nx, ny, dest_type, nf);
      }
    if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }
    for (im = 0; im < nf; im++)
    {
        switch_frame(ois1,im);
        switch_frame(dest,im);
        pd = dest->im.pixel;	ps1 = ois1->im.pixel;
        for (i=0; i< ny; i++)
        {
            for (j=0; j<nx; j++)
            {
	      pd[i].ui[j] = 0;
            }
        }
        for (i=0; i< ony; i++)
        {
	    ii = i/bin;
            for (j=0; j<onx; j++)
            {
	        jj = j;
		pd[ii].ui[jj] += ps1[i].ch[j];
            }
        }
    }
    inherit_from_im_to_im(dest,ois1);
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}


int		do_undersample_image_of_char(void)
{
    O_i   *oid, *ois;
    imreg *imr;
    int i;
    static int bin = 2, yonly = 0;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("%R->undersample in X and Y or %r->in Y only\n"
		  "Define your undersampling %4d\n",&yonly,&bin);
    if (i == WIN_CANCEL) return 0;
    if (yonly) oid = undersample_image_of_char_in_y(NULL, ois, bin);
    else oid = undersample_image_of_char(NULL, ois, bin);
    if (oid == NULL)	return D_O_K;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}




/*
 *		dest = ois1 - ois2
 *
 */
O_i 	*convert_float_image_to_char(O_i *dest, O_i *ois1)
{
    int i, j, im;
    int  onx, ony, itmp, nf, nfd;
    int	dest_type = 0;
    float tmp;
    union pix *pd, *ps1;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    nf = ois1->im.n_f;
    if (nf < 1) nf = 1;
    if (ois1->im.data_type != IS_FLOAT_IMAGE)
    {
        win_printf ("the image is not of float type!\n"
                    "Cannot transform!");
        return(NULL);
    }

    if (dest != NULL)
    {
        if (dest->im.data_type != IS_CHAR_IMAGE)
        {
            win_printf ("The destination image is not of unsigned char type\n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        if (ony != dest->im.ny || onx != dest->im.nx)
        {
            win_printf ("The destination size differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        nfd = dest->im.n_f;
        if (nfd < 1) nfd = 1;
        if (nf != nfd)
        {
            win_printf ("The destination nb of frame differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }

    }
    else
        dest_type = IS_CHAR_IMAGE;

    if (nf == 1)
        dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
    else dest = (dest != NULL) ? dest : create_one_movie(onx, ony, dest_type, nf);
    if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }


    for (im = 0; im < nf; im++)
    {
        switch_frame(ois1,im);
        switch_frame(dest,im);
        pd = dest->im.pixel;	ps1 = ois1->im.pixel;
        for (i=0; i< ony; i++)
        {
            for (j=0; j<onx; j++)
            {
                tmp = ps1[i].fl[j] - ois1->z_min;
                tmp /= ois1->z_max - ois1->z_min;
                tmp *= 256;
                itmp = (int)(tmp+0.5);
                itmp = (itmp < 0) ? 0 : itmp;
                itmp = (itmp > 255) ? 255 : itmp;
                pd[i].ch[j] = (unsigned char)itmp;
            }
        }
    }
    inherit_from_im_to_im(dest,ois1);
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}

int		do_convert_float_image_to_char(void)
{
    O_i   *oid, *ois;
    imreg *imr;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    oid = convert_float_image_to_char(NULL, ois);
    if (oid == NULL)	return D_O_K;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}




int		do_radial_wavenumber_filter_xray_2d_im(void)
{
    O_i   *oid, *ois;
    imreg *imr;
    static int ikx = 16, iky = 16;
    int i;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

    i = win_scanf("Ikx %4d iky %4d\n",&ikx,&iky);
    if (i == WIN_CANCEL) return 0;
    oid = radial_wavenumber_filter_xray_2d_im(ois, ikx, iky);
    if (oid == NULL)	return D_O_K;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}

int		do_histo_small_amp_of_fft_2d_movie(void)
{
    O_i   *oid, *ois;
    imreg *imr;
    static int nA = 16;
    float dA = 0.0005;
    int i;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

    i = win_scanf("Na %4d dA %6f\n",&nA,&dA);
    if (i == WIN_CANCEL) return 0;
    oid = histo_small_amp_of_fft_2d_movie(ois, dA, nA);
    if (oid == NULL)	return D_O_K;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}




/*
 *		dest = ois1 - ois2
 *
 */
O_i 	*divide_2_image(O_i *dest, O_i *ois1, O_i *ois2, float min_div, float min_divi)
{
    int i, j;
    int  onx, ony;
    int	im1_type = 0, im2_type = 0, dest_type = 0;
    float re, im, ren, imn, norm_div, tmp;
    union pix *pd, *ps1, *ps2;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No source 1 image found");
        return (NULL);
    }
    if ( ois2 == NULL|| ois2->im.pixel == NULL)
    {
        win_printf("No source 2 image found");
        return (NULL);
    }
    if (fabs(min_div) == 0)
    {
        win_printf("Min div cannot be set to zero");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    if (ois2->im.data_type == IS_COMPLEX_IMAGE) 	im2_type = 4;
    else if (ois2->im.data_type == IS_FLOAT_IMAGE)	im2_type = 3;
    else if (ois2->im.data_type == IS_INT_IMAGE)	im2_type = 2;
    else if (ois2->im.data_type == IS_CHAR_IMAGE)	im2_type = 1;
    else	win_printf("wrong image type %d!",ois2->im.data_type);
    if (ois1->im.data_type == IS_COMPLEX_IMAGE) 	im1_type = 4;
    else if (ois1->im.data_type == IS_FLOAT_IMAGE)	im1_type = 3;
    else if (ois1->im.data_type == IS_INT_IMAGE)	im1_type = 2;
    else if (ois1->im.data_type == IS_CHAR_IMAGE)	im1_type = 1;
    else	win_printf("wrong image type %d!",ois1->im.data_type);
    i = (im2_type >= im1_type) ? im2_type : im1_type;
    if (ony != ois2->im.ny || onx != ois2->im.nx)
    {
        win_printf ("The size of the second image differs from the source one \n"
                    "Cannot perform division!");
        return(NULL);
    }
    if (dest != NULL)
    {
        if (dest->im.data_type == IS_COMPLEX_IMAGE) 	dest_type = 4;
        else if (dest->im.data_type == IS_FLOAT_IMAGE)	dest_type = 3;
        else if (dest->im.data_type == IS_INT_IMAGE)	dest_type = 3;
        else if (dest->im.data_type == IS_CHAR_IMAGE)	dest_type = 3;
        else	win_printf("wrong image type %d!",dest->im.data_type);
        if (ony != dest->im.ny || onx != dest->im.nx)
        {
            win_printf ("The destination size differs from the source one \n"
                        "Cannot perform division!");
            return(NULL);
        }
        if (dest_type < i || dest_type < 2)
        {
            win_printf ("The destination image type cannot contain the source"
                        "	data type\n Cannot perform division!");
            return(NULL);
        }
    }
    else
    {
        i = (im2_type >= im1_type) ? im2_type : im1_type;
        if (i == 4)			dest_type = IS_COMPLEX_IMAGE;
        else 				dest_type = IS_FLOAT_IMAGE;
    }
    dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
    if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }


    pd = dest->im.pixel;	ps1 = ois1->im.pixel;	ps2 = ois2->im.pixel;
    if (dest->im.data_type == IS_COMPLEX_IMAGE)
    {
        if (ois1->im.data_type == IS_COMPLEX_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j<2*onx; j++)	pd[i].fl[j] = ps1[i].fl[j];
        }
        else if (ois1->im.data_type == IS_FLOAT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[2*j] = ps1[i].fl[j];
        }
        else if (ois1->im.data_type == IS_INT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[2*j] = (float)ps1[i].in[j];
        }
        else if (ois1->im.data_type == IS_CHAR_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[2*j] = (float)ps1[i].ch[j];
        }
        if (ois2->im.data_type == IS_COMPLEX_IMAGE)
        {
            norm_div = min_div * min_div + min_divi * min_divi;
            for (i=0; i< ony; i++)
            {
                for (j=0; j<onx; j++)
                {
                    re = ps2[i].fl[2*j];
                    im = ps2[i].fl[2*j+1];
                    ren = pd[i].fl[2*j];
                    imn = pd[i].fl[2*j+1];
                    tmp = re * re + im * im;
                    tmp = (tmp < norm_div) ? norm_div : tmp;
                    pd[i].fl[2*j] = (ren * re - im * imn)/tmp;
                    pd[i].fl[2*j+1] = (imn * re - im * ren)/tmp;
                }
            }
        }
        else if (ois2->im.data_type == IS_FLOAT_IMAGE)
        {
            for (i=0; i< ony; i++)
            {
                for (j=0; j< onx; j++)
                {
                    tmp = ps2[i].fl[j];
                    tmp = (fabs(tmp) < fabs(min_div)) ? min_div : tmp;
                    pd[i].fl[2*j] /= tmp;
                    pd[i].fl[2*j+1] /= tmp;
                }
            }
        }
        else if (ois2->im.data_type == IS_INT_IMAGE)
        {
            for (i=0; i< ony; i++)
            {
                for (j=0; j< onx; j++)
                {
                    tmp = ps2[i].in[j];
                    tmp = (fabs(tmp) < fabs(min_div)) ? min_div : tmp;
                    pd[i].fl[2*j] /= tmp;
                    pd[i].fl[2*j+1] /= tmp;
                }
            }
        }
        else if (ois2->im.data_type == IS_CHAR_IMAGE)
        {
            for (i=0; i< ony; i++)
            {
                for (j=0; j< onx; j++)
                {
                    tmp = ps2[i].ch[j];
                    tmp = (fabs(tmp) < fabs(min_div)) ? min_div : tmp;
                    pd[i].fl[2*j] /= tmp;
                    pd[i].fl[2*j+1] /= tmp;
                }
            }
        }
    }
    else if (dest->im.data_type == IS_FLOAT_IMAGE)
    {
        if (ois1->im.data_type == IS_FLOAT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[j] = ps1[i].fl[j];
        }
        else if (ois1->im.data_type == IS_INT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[j] = (float)ps1[i].in[j];
        }
        else if (ois1->im.data_type == IS_CHAR_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[j] = (float)ps1[i].ch[j];
        }
        if (ois2->im.data_type == IS_FLOAT_IMAGE)
        {
            for (i=0; i< ony; i++)
            {
                for (j=0; j< onx; j++)
                {
                    tmp = ps2[i].fl[j];
                    tmp = (fabs(tmp) < fabs(min_div)) ? min_div : tmp;									pd[i].fl[j] /= tmp;
                }
            }
        }
        else if (ois2->im.data_type == IS_INT_IMAGE)
        {
            for (i=0; i< ony; i++)
            {
                for (j=0; j< onx; j++)
                {
                    tmp = ps2[i].in[j];
                    tmp = (fabs(tmp) < fabs(min_div)) ? min_div : tmp;
                    pd[i].fl[j] /= tmp;
                }
            }
        }
        else if (ois2->im.data_type == IS_CHAR_IMAGE)
        {
            for (i=0; i< ony; i++)
            {
                for (j=0; j< onx; j++)
                {
                    tmp = ps2[i].ch[j];
                    tmp = (fabs(tmp) < fabs(min_div)) ? min_div : tmp;
                    pd[i].fl[j] /= tmp;
                }
            }
        }
    }
    else
    {
        win_printf("wrong type of image");
        return NULL;
    }
    inherit_from_im_to_im(dest,ois1);
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;
    dest->need_to_refresh = ALL_NEED_REFRESH;
    set_im_title(dest, "\\stack{{images division}{%s and %s}}",
                 (ois1->im.source != NULL) ? 	ois1->im.source : "untitled",
                 (ois2->im.source != NULL) ? 	ois2->im.source : "untitled");
    set_formated_string(&dest->im.treatement,"images division %s and %s",
                        (ois1->im.source != NULL) ? 	ois1->im.source : "untitled",
                        (ois2->im.source != NULL) ? 	ois2->im.source : "untitled");
    return dest;
}


/*
 *		dest = ois1 - ois2
 *
 */
O_i 	*substract_2_image(O_i *dest, O_i *ois1, O_i *ois2)
{
    int i, j;
    int  onx, ony;
    int	im1_type = 0, im2_type = 0, dest_type = 0;
    union pix *pd, *ps1, *ps2;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No source 1 image found");
        return (NULL);
    }
    if ( ois2 == NULL|| ois2->im.pixel == NULL)
    {
        win_printf("No source 2 image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    if (ois2->im.data_type == IS_COMPLEX_IMAGE) 	im2_type = 4;
    else if (ois2->im.data_type == IS_FLOAT_IMAGE)	im2_type = 3;
    else if (ois2->im.data_type == IS_INT_IMAGE)	im2_type = 2;
    else if (ois2->im.data_type == IS_CHAR_IMAGE)	im2_type = 1;
    else	win_printf("wrong image type %d!",ois2->im.data_type);
    if (ois1->im.data_type == IS_COMPLEX_IMAGE) 	im1_type = 4;
    else if (ois1->im.data_type == IS_FLOAT_IMAGE)	im1_type = 3;
    else if (ois1->im.data_type == IS_INT_IMAGE)	im1_type = 2;
    else if (ois1->im.data_type == IS_CHAR_IMAGE)	im1_type = 1;
    else	win_printf("wrong image type %d!",ois1->im.data_type);
    i = (im2_type >= im1_type) ? im2_type : im1_type;
    if (ony != ois2->im.ny || onx != ois2->im.nx)
    {
        win_printf ("The size of the second image differs from the source one \n"
                    "Cannot perform corelation!");
        return(NULL);
    }
    if (dest != NULL)
    {
        if (dest->im.data_type == IS_COMPLEX_IMAGE) 	dest_type = 4;
        else if (dest->im.data_type == IS_FLOAT_IMAGE)	dest_type = 3;
        else if (dest->im.data_type == IS_INT_IMAGE)	dest_type = 2;
        else if (dest->im.data_type == IS_CHAR_IMAGE)	dest_type = 1;
        else	win_printf("wrong image type %d!",dest->im.data_type);
        if (ony != dest->im.ny || onx != dest->im.nx)
        {
            win_printf ("The destination size differs from the source one \n"
                        "Cannot perform corelation!");
            return(NULL);
        }
        if (dest_type < i || dest_type < 2)
        {
            win_printf ("The destination image type cannot contain the source"
                        "	data type\n Cannot perform substraction!");
            return(NULL);
        }
    }
    else
    {
        i = (im2_type >= im1_type) ? im2_type : im1_type;
        if (i == 4)			dest_type = IS_COMPLEX_IMAGE;
        else if (i == 3)	dest_type = IS_FLOAT_IMAGE;
        else if (i <= 2)	dest_type = IS_INT_IMAGE;
    }
    dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
    if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }


    pd = dest->im.pixel;	ps1 = ois1->im.pixel;	ps2 = ois2->im.pixel;
    if (dest->im.data_type == IS_COMPLEX_IMAGE)
    {
        if (ois1->im.data_type == IS_COMPLEX_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j<2*onx; j++)	pd[i].fl[j] = ps1[i].fl[j];
        }
        else if (ois1->im.data_type == IS_FLOAT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[2*j] = ps1[i].fl[j];
        }
        else if (ois1->im.data_type == IS_INT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[2*j] = (float)ps1[i].in[j];
        }
        else if (ois1->im.data_type == IS_CHAR_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[2*j] = (float)ps1[i].ch[j];
        }
        if (ois2->im.data_type == IS_COMPLEX_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j<2*onx; j++)	pd[i].fl[j] -= ps2[i].fl[j];
        }
        else if (ois2->im.data_type == IS_FLOAT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[2*j] -= ps2[i].fl[j];
        }
        else if (ois2->im.data_type == IS_INT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[2*j] -= (float)ps2[i].in[j];
        }
        else if (ois2->im.data_type == IS_CHAR_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[2*j] -= (float)ps2[i].ch[j];
        }
    }
    else if (dest->im.data_type == IS_FLOAT_IMAGE)
    {
        if (ois1->im.data_type == IS_FLOAT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[j] = ps1[i].fl[j];
        }
        else if (ois1->im.data_type == IS_INT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[j] = (float)ps1[i].in[j];
        }
        else if (ois1->im.data_type == IS_CHAR_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[j] = (float)ps1[i].ch[j];
        }
        if (ois2->im.data_type == IS_FLOAT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[j] -= ps2[i].fl[j];
        }
        else if (ois2->im.data_type == IS_INT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[j] -= (float)ps2[i].in[j];
        }
        else if (ois2->im.data_type == IS_CHAR_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].fl[j] -= (float)ps2[i].ch[j];
        }
    }
    else	if (dest->im.data_type == IS_INT_IMAGE)
    {
        if (ois1->im.data_type == IS_INT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].in[j] = ps1[i].in[j];
        }
        else if (ois1->im.data_type == IS_CHAR_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].in[j] = (short int)ps1[i].ch[j];
        }
        if (ois2->im.data_type == IS_INT_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].in[j] -= ps2[i].in[j];
        }
        else if (ois2->im.data_type == IS_CHAR_IMAGE)
        {
            for (i=0; i< ony; i++)
                for (j=0; j< onx; j++)	pd[i].in[j] -= (short int)ps2[i].ch[j];
        }
    }
    else
    {
        win_printf("wrong type of image");
        return NULL;
    }
    inherit_from_im_to_im(dest,ois1);
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;
    dest->need_to_refresh = ALL_NEED_REFRESH;
    set_im_title(dest, "\\stack{{substraction of images}{%s and %s}}",
                 (ois1->im.source != NULL) ? 	ois1->im.source : "untitled",
                 (ois2->im.source != NULL) ? 	ois2->im.source : "untitled");
    set_formated_string(&dest->im.treatement,"substraction of images %s and %s",
                        (ois1->im.source != NULL) ? 	ois1->im.source : "untitled",
                        (ois2->im.source != NULL) ? 	ois2->im.source : "untitled");
    return dest;
}
int		do_substract_2_im(void)
{
    O_i   *oid;
    imreg *imr;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return OFF;
    if ((imr->cur_oi - 1) < 0)	return(win_printf("I need at least two images!"));
    oid = substract_2_image(NULL, imr->o_i[imr->cur_oi], imr->o_i[imr->cur_oi - 1]);
    if (oid == NULL)	return win_printf_OK("Could not create image!");
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}
int	do_multiply(void)
{
    int i;
    imreg *imr;
    double x = 1;
    O_i *oid;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return OFF;

    i = win_scanf("facteur multiplicatif %lf",&x);
    if (i == WIN_CANCEL)	return OFF;
    oid = multiply_im(NULL,imr->one_i, (double)x);
    if (oid == NULL)	return win_printf_OK("Could not create image!");
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}

int	do_band_pass_filter_fft_2d(void)
{
  O_i  *oid = NULL, *ois = NULL;
    imreg *imr;
    static float lp = 20;
    static float lw = 15;
    static float hp = 60;
    static float hw = 20;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

    if (win_scanf("cutoff lowpass  %flowpass width %fcutoff highpass  %f"
                  "highpass width %f",&lp,&lw,&hp,&hw) == WIN_CANCEL)		return OFF;
    oid = band_pass_filter_fft_2d_im(NULL, imr->one_i,  1, lp,  lw,  hp,  hw, -1, 1);
    if (oid == NULL)	return win_printf_OK("Could not filter image");
    if (oid != ois) add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}
int	do_sloppy_fft2d(void)
{
    O_i  *oid;
    imreg *imr;
    static unsigned long t_c, to = 0;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return OFF;

    t_c = my_uclock();
    oid = forward_fftbt_2d_im(NULL, imr->one_i, 1, screen, .05);
    t_c = my_uclock() - t_c;

    if (oid == NULL)	return win_printf_OK("Could not create image!");
    oid->height *= 0.5;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    refresh_image(imr, imr->n_oi - 1);
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    //win_printf("dt = %g",(double)t_c);
    my_set_window_title("FFT took %g ms",(double)(t_c*1000)/to);
    //win_printf("dt = %g ms",(double)(t_c*1000)/to);
    return D_O_K;
}


int	do_sloppy_fft2d_apo(void)
{
    O_i  *ois, *oid, *apo;
    imreg *imr;
    static unsigned long t_c, to = 0;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

    apo = create_appodisation_float_image(ois->im.nx, ois->im.ny, 0.05, 0);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");

    t_c = my_uclock();
    oid = forward_fftbt_2d_im_appo(NULL, ois, 1, screen, apo, 1);
    t_c = my_uclock() - t_c;

    if (oid == NULL)	return win_printf_OK("Could not create image!");
    oid->height *= 0.5;
    free_one_image(apo);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    refresh_image(imr, imr->n_oi - 1);
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    //win_printf("dt = %g",(double)t_c);
    my_set_window_title("FFT took %g ms",(double)(t_c*1000)/to);
    //win_printf("dt = %g ms",(double)(t_c*1000)/to);
    return D_O_K;
}

int	do_2d_apo(void)
{
    O_i  *ois = NULL, *oid = NULL, *apo = NULL;
    imreg *imr = NULL;
    int i;
    static unsigned long t_c, to = 0;
    static int keep_dc = 0;
    static float sm = 0.05;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

    i = win_scanf("Keep DC component %b\nsmall param %6f\n",&keep_dc,&sm);
    if (i == WIN_CANCEL) return 0;
    apo = create_appodisation_float_image(ois->im.nx, ois->im.ny, sm, 0);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");

    t_c = my_uclock();
    oid = d2_im_appo(NULL, ois, 1, screen, apo, 1, keep_dc);
    t_c = my_uclock() - t_c;

    if (oid == NULL)	return win_printf_OK("Could not create image!");
    free_one_image(apo);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    refresh_image(imr, imr->n_oi - 1);
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    //win_printf("dt = %g",(double)t_c);
    my_set_window_title("Appodisation took %g ms",(double)(t_c*1000)/to);
    //win_printf("dt = %g ms",(double)(t_c*1000)/to);
    return D_O_K;
}




int	do_sloppy_fft2d_filter(void)
{
    O_i  *oid;
    imreg *imr;
    int fx, fy, i;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return OFF;

    fx = imr->one_i->im.nx/4;
    fy = imr->one_i->im.ny/4;
    i = win_scanf("cutoff lowpass en x %dcutoff lowpass en y %d",&fx,&fy);
    if (i == WIN_CANCEL)    return OFF;
    oid = forward_fft_2d_im_filter(NULL, imr->one_i,screen, .05, fx, fy);
    if (oid == NULL)	return win_printf_OK("Could not create image!");
    oid->height = 0.5;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    refresh_image(imr, imr->n_oi - 1);
    return D_O_K;
}
int	do_sloppy_fft2d_inv(void)
{
    O_i  *oid;
    imreg *imr;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return OFF;

    oid = backward_fftbt_2d_im(NULL, imr->one_i, 1, screen, .05);
    if (oid == NULL)	return win_printf_OK("Could not create image!");
    oid->height *= 2;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}

int	do_sloppy_fft2d_inv_apo(void)
{
    O_i  *ois, *oid, *apo;
    imreg *imr;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

    apo = create_appodisation_float_image(ois->im.nx, 2*ois->im.ny, 0.05, 1);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");

    oid = backward_fftbt_2d_im_desappo(NULL, imr->one_i, 1, screen, apo, 1);
    if (oid == NULL)	return win_printf_OK("Could not create image!");
    oid->height *= 2;
    free_one_image(apo);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}

int	do_sloppy_fft2d_inv_desapo(void)
{
    O_i  *ois, *oid;//, *apo;
    imreg *imr;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

    oid = backward_fftbt_2d_im_desappo(NULL, imr->one_i, 1, screen, NULL, 1);
    if (oid == NULL)	return win_printf_OK("Could not create image!");
    oid->height *= 2;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}



int		do_sloppy_correlate(void)
{
    O_i  *oid;
    imreg *imr;
    int i, n_den = 0;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return OFF;

    if ((imr->cur_oi - 1) < 0)	return win_printf_OK("I need at least two images!");
    n_den = imr->cur_oi + 1;
    i = win_scanf("I will correlate two fft images\n"
                  "select the second image %d",&n_den);
    if (i == WIN_CANCEL) return 0;
    n_den = (n_den < 0) ? 0 : n_den;
    n_den %= imr->n_oi;

    oid = corel_2d_im(NULL, imr->o_i[imr->cur_oi], imr->o_i[n_den]);
    if (oid == NULL)	return win_printf_OK("Could not create image!");
    add_image(imr, oid->type, (void*)oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}


int   do_sloppy_correlate_movie_in_time(void)
{
    O_i  *oid;
    imreg *imr;
    int nfi, ref;
    static int n1 = 0, normalize = 1, ref_im = 0;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return OFF;

    nfi = abs(imr->o_i[imr->cur_oi]->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;
    if (nfi < 2)	return win_printf_OK("I need at least two images!");
    ref = ref_im;
    if (win_scanf("Correlate movie frames:\n"
		  "%R to a specific one\n"
		  "%r or between n and n+1\n"
		  "In first case specify the reference frame %4d\n"
		  "%b Normalize correlation\n",&n1,&ref,&normalize) == WIN_CANCEL)
            return 0;
    ref_im = ref;
    oid = corel_2d_movie_in_time(NULL, imr->o_i[imr->cur_oi],(n1==0)?ref:-1,normalize);
    if (oid == NULL)	return win_printf_OK("Could not create image!");
    add_image(imr, oid->type, (void*)oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}



O_i	*upside_down_and_left_to_right( O_i *ois)
{
    int i, j, k;
    O_i *oid;
    char  c1[128];
    int onx, ony;
    union pix  *ps, *pd;

    if(updating_menu_state != 0)	return D_O_K;

    //c[0] = 0;
    onx = ois->im.nx;	ony = ois->im.ny;
    oid = duplicate_image(ois, NULL);
    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return NULL;
    }
    for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < onx ; i++)
    {
        k = onx - i -1;
        if (ois->im.data_type == IS_CHAR_IMAGE)
            for (j=0; j< ony; j++)
                pd[ony - j - 1].ch[k] = ps[j].ch[i];
        else if (ois->im.data_type == IS_INT_IMAGE)
            for (j=0; j< ony; j++)
                pd[ony - j - 1].fl[k] = ps[j].in[i];
        else if (ois->im.data_type == IS_FLOAT_IMAGE)
            for (j=0; j< ony; j++) pd[ony - j - 1].fl[k] = ps[j].fl[i];
        else if (ois->im.data_type == IS_COMPLEX_IMAGE)
        {
            for (j=0; j< ony; j++)
            {
                pd[ony - j - 1].fl[2*k] = ps[j].fl[2*i];
                pd[ony - j - 1].fl[2*k+1] = ps[j].fl[2*i+1];
            }
        }
    }
    if (ois->im.source != NULL) 	strcpy(c1, ois->im.source);
    else				c1[0] = 0;
    inherit_from_im_to_im(oid,ois);
    uns_oi_2_oi(oid,ois);
    oid->im.win_flag = ois->im.win_flag;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    if (ois->title != NULL)	set_im_title(oid, "%s", ois->title);
    set_formated_string(&oid->im.treatement,"upside down and right to left %s",c1);
    return oid;
}

int upside_down_and_right_to_left(void)
{
    O_i *ois, *oid;
    imreg *imr;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    oid = upside_down_and_left_to_right(ois);
    if (oid == NULL)	return win_printf_OK("unable to create image!");
    add_image(imr, oid->type, (void*)oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}

int half_shift_x_y(void)
{
    O_i *ois, *oid;
    imreg *imr;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    oid = shift_2d_im(NULL, ois);
    if (oid == NULL)	return win_printf_OK("unable to create image!");
    add_image(imr, oid->type, (void*)oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}




int do_Nb_of_mode_radial_wavenumber(void)
{
    O_i *ois;
    imreg *imr;
    int nfi, i;
    O_p *op = NULL;
    static float thres = 0005, kmax = 64;


    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;
    i = win_scanf("Define thres above which mode are counted %6f\n"
		  "Max wavenumber below which mode are counted %6f\n"
		  ,&thres,&kmax);
    if (i == WIN_CANCEL) return 0;

    op = Nb_of_mode_radial_wavenumber_of_fft_2d_movie(ois, thres, kmax);
    if (op== NULL)	return win_printf_OK("unable to create image!");
    add_to_one_image(ois, IS_ONE_PLOT, (void*)op);
    ois->need_to_refresh = ALL_NEED_REFRESH;
    return refresh_image(imr, UNCHANGED);
}



int radial_wavenumber(void)
{
  O_i *ois, *oid;
    imreg *imr;
    int nfi;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;
    if (nfi < 2)
      {
	radial_wavenumber_of_fft_2d_im(ois);
	return refresh_image(imr, UNCHANGED);
      }
    oid = radial_wavenumber_of_fft_2d_movie(ois);
    if (oid == NULL)	return win_printf_OK("unable to create image!");
    add_image(imr, oid->type, (void*)oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}






int 	erase_low_mode_of_fft_2d_im(O_i *ois, float ampmin)
{
    int i, j;
    int  onx, ony, nm = 0;
    float amp, re, im;
    union pix  *ps;
    ps = ois->im.pixel;

    if (ois == NULL || ois->im.pixel == NULL)
        win_printf_OK("No image found");

    ony = ois->im.ny;	onx = ois->im.nx;
    if (ois->im.data_type != IS_COMPLEX_IMAGE)
        win_printf_OK ("the image is not of complex type!\n"
                       "Cannot perform backward fft!");

    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
        win_printf_OK ("the image does not correspond to an fft!\n"
                       "Cannot perform erase mode!");


    for (j=0, nm = 0; j< ony; j++)
    {
        for (i = 0; i< onx ; i++)
        {
            re = ps[j].fl[2*i];
            im = ps[j].fl[2*i+1];
            amp =  re*re + im*im;
            if (amp < ampmin)
            {
                ps[j].fl[2*i] = 0;
                ps[j].fl[2*i+1] = 0;
            }
            else nm++;
        }
    }
    ois->need_to_refresh = ALL_NEED_REFRESH;
    return nm;
}




int 	equalize_module_or_erase_low_mode_of_fft_2d_im(O_i *ois, float ampmin)
{
    int i, j;
    int  onx, ony, nm = 0;
    float amp, re, im;
    union pix  *ps;
    ps = ois->im.pixel;

    if (ois == NULL || ois->im.pixel == NULL)
        win_printf_OK("No image found");

    ony = ois->im.ny;	onx = ois->im.nx;
    if (ois->im.data_type != IS_COMPLEX_IMAGE)
        win_printf_OK ("the image is not of complex type!\n"
                       "Cannot perform backward fft!");

    if ((ois->im.treatement == NULL) || strncmp(ois->im.treatement,
                                                "sloppy fft2d image",18) != 0)
        win_printf_OK ("the image does not correspond to an fft!\n"
                       "Cannot perform erase mode!");


    for (j=0, nm = 0; j< ony; j++)
    {
        for (i = 0; i< onx ; i++)
        {
            re = ps[j].fl[2*i];
            im = ps[j].fl[2*i+1];
            amp =  re*re + im*im;
            if (amp < ampmin)
            {
                ps[j].fl[2*i] = 0;
                ps[j].fl[2*i+1] = 0;
            }
            else
            {
                amp = sqrt(amp);
                ps[j].fl[2*i] /= amp;
                ps[j].fl[2*i+1] /= amp;
                nm++;
            }
        }
    }
    ois->need_to_refresh = ALL_NEED_REFRESH;
    return nm;
}




int	do_erase_low_mode_of_fft_2d_im(void)
{
    imreg *imr;
    O_i *ois;
    static float ampmin = 1e-3;
    int nm, i;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("I am going to erase the small modes\n"
                  "amplitude threshold below \n which modes are set to 0 %f",&ampmin);
    if (i == WIN_CANCEL)	return D_O_K;
    nm = erase_low_mode_of_fft_2d_im(ois,ampmin);
    win_printf("%d modes were kept",nm);
    ois->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(ois);
    return (refresh_image(imr, UNCHANGED));
}


int	do_equalize_module_or_erase_low_mode_of_fft_2d_im(void)
{
    imreg *imr;
    O_i *ois;
    static float ampmin = 1e-3;
    int nm, i;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("I am going to set module of modes to 1 \n"
                  "or to erase them when their amplitude falls below an \n"
                  "amplitude threshold %12f\n",&ampmin);
    if (i == WIN_CANCEL)	return D_O_K;
    nm = equalize_module_or_erase_low_mode_of_fft_2d_im(ois,ampmin);
    win_printf("%d modes were kept",nm);
    ois->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(ois);
    return (refresh_image(imr, UNCHANGED));
}


int do_filter_fft_2d_im_by_radial_wavenumber(void)
{
    imreg *imr;
    O_i *ois, *oid;
    O_p *op;
    static int new_img = 1;
    int  i;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

    if (ois->cur_op < 0)
        return win_printf_OK("This routine filter a fft image by a radial profile\n"
                             "it requires an image with a plot containing\n"
                             "the radial profile which is missing\n");
    op = ois->o_p[imr->one_i->cur_op];

    i = win_scanf("Modify this image %R or create a new one %r\n"
                  ,&new_img);
    if (i == WIN_CANCEL) return  D_O_K;
    oid = filter_fft_2d_im_by_radial_wavenumber(ois, ((new_img) ? NULL : ois), op->dat[op->cur_dat]);
    if (oid == NULL)    win_printf_OK ("Operation failed!\n");
    if (new_img)
    {
        add_image(imr, oid->type, (void*)oid);
        oid->need_to_refresh = ALL_NEED_REFRESH;
    }
    find_zmin_zmax(oid);
    return (refresh_image(imr, (new_img)?imr->n_oi - 1: UNCHANGED));
}



int do_angular_projection_of_image(void)
{
    imreg *imr;
    O_i *ois;
    O_p *op;
    static float angle_de = 45;
    int  i;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("This routine perform an angular projection of an image\n"
                  "with an angle (in degrees) %12f\n",&angle_de);
    if (i == WIN_CANCEL) return 0;
    op = image_projection_along_an_angle(ois, NULL, 0, angle_de);
    if (op == NULL)    win_printf_OK ("Operation failed!\n");
    add_to_one_image(ois, IS_ONE_PLOT, op);
    ois->need_to_refresh |= PLOT_NEED_REFRESH;
    return (refresh_image(imr, UNCHANGED));
}




int do_reconstruct_image_from_angular_projection_nearly(void)
{
    imreg *imr;
    O_i *ois, *oid = NULL;
    static float angle_de = 45;
    static int n_angle = 12;
    int  i, i_angle, ikx, iky;
    int nout = 0, nout_2, onx, ony, onx_2, ony_2;
    union pix  *pd; // *ps
    float  co, si, *tmp;
    O_p  *op = NULL;
    d_s *ds;
    double  	*in=NULL;
    fftw_complex	*outc=NULL;
    fftw_plan     plan;     // fft plan for fftw



    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("This routine construct the image fft from\n"
                  "a set of an angular projection of an image\n"
                  "define the number of profile %d\n",&n_angle);
    if (i == WIN_CANCEL) return 0;

    if (ois == NULL || ois->im.pixel == NULL)
        win_printf_OK("No image found");
    ony = ois->im.ny;	onx = ois->im.nx;
    ony_2 = ony/2;   onx_2 = onx/2;        // image center
    nout_2 = 1 + (int)sqrt(onx_2 * onx_2 + ony_2 * ony_2);
    for (i = 2; i < nout_2; i*=2);
    nout_2 = i;
    nout = 2 * nout_2;
    //ps = ois->im.pixel;

    oid = create_and_attach_oi_to_imr(imr, nout, nout_2, IS_COMPLEX_IMAGE);
    if (oid == NULL)      win_printf_OK("Can't create dest image");
    inherit_from_im_to_im(oid,ois);
    oid->im.mode = LOG_AMP;
    oid->ax = -onx/2;
    set_im_x_title(oid,"wavenumber k_x");
    set_im_y_title(oid,"wavenumber k_y");
    uns_oi_2_oi(oid,ois);
    oid->im.win_flag = ois->im.win_flag;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    set_formated_string(&oid->im.treatement,"sloppy fft2d image ");
    set_oi_periodic_in_x_and_y(oid);

    in   = (double *) fftw_malloc( nout * sizeof(double));
    outc = (fftw_complex *) fftw_malloc((nout_2+1)*sizeof(fftw_complex));
    if (in == NULL || outc == NULL)
        return win_printf_OK("cannot create fftw arrays !");
    plan = fftw_plan_dft_r2c_1d(nout, in, outc, FFTW_ESTIMATE);
    pd = oid->im.pixel;
    tmp = (float*)calloc(nout,sizeof(float));
    if (tmp == NULL)
        return win_printf_OK("cannot create fftw arrays !");
    if (fft_init(nout))
        return win_printf_OK("cannot init fft");

    for (i_angle = 0; i_angle < n_angle; i_angle++)
    {
        angle_de = (180 * i_angle) /n_angle;
        op = image_projection_along_an_angle(ois, op, 0, angle_de);
        if (op == NULL)    win_printf_OK ("Operation failed!\n");
        ds = op->dat[0];
        for (i = 0; i < nout; i++) in[i] = 0;
        for (i = 0; i < nout; i++) tmp[i] = 0;
        for (i = 0; i < nout_2 && i < ds->nx/2; i++)
        {
            in[i] = (double)ds->yd[i+ds->nx/2];
            in[nout-i-1] = (double)ds->yd[(ds->nx/2)-i-1];
            //in[i+nout_2] = (double)ds->yd[i+ds->nx/2];
            //in[nout_2-i-1] = (double)ds->yd[(ds->nx/2)-i-1];
            tmp[i+nout_2] = ds->yd[i+ds->nx/2];
            tmp[nout_2-i-1] = ds->yd[(ds->nx/2)-i-1];
        }
        //for (i = 0; i < nout; i++) ds->yd[i] = (float)in[i]; // debug
        fftw_execute(plan);

        realtr1(nout, tmp);
        fft(nout, tmp, 1);
        realtr2(nout, tmp, 1);



        co = cos(M_PI*angle_de/180);
        si = sin(M_PI*angle_de/180);
        for (i = 0; i < nout_2; i++)
        {
            ikx = nout_2 + (int) (0.5+ (co * i));
            iky = (int) (0.5+ (si * i));
            if (ikx >= 0 && ikx < nout && iky >= 0 && iky < nout_2)
            {
                pd[iky].fl[2*ikx] += (float)outc[i][0] * ((float)i/nout_2);
                pd[iky].fl[2*ikx+1] += (float)outc[i][1] * ((float)i/nout_2);
                //pd[iky].fl[2*ikx] += tmp[2*i];
                //pd[iky].fl[2*ikx+1] += tmp[2*i+1];
            }
        }
    }
    add_to_one_image(ois, IS_ONE_PLOT, op);
    ois->need_to_refresh |= PLOT_NEED_REFRESH;

    oid->height *= 0.5;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oid);
    return refresh_image(imr, imr->n_oi - 1);
}




int do_reconstruct_image_from_angular_projection(void)
{
    imreg *imr;
    O_i *ois, *oid = NULL, *oid2 = NULL, *oip = NULL;
    O_i *oir = NULL; // radial fft image
    static float angle_de = 45;
    static int n_angle = 12;
    int  i, j, i_angle, ikx, iky, debug = WIN_OK;
    int nout = 0, nout_2, onx, ony, onx_2, ony_2, fil_f;
    union pix   *pd, *pr; // *ps,
    float  co, si, *tmp, re, im, k, theta, *fil;
    O_p  *op = NULL;
    d_s *ds;
    double  	*in=NULL;
    fftw_complex	*outc=NULL;
    fftw_plan     plan;     // fft plan for fftw



    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("This routine construct the image fft from\n"
                  "a set of an angular projection of an image\n"
                  "define the number of profile %d\n",&n_angle);
    if (i == WIN_CANCEL) return 0;

    if (ois == NULL || ois->im.pixel == NULL)
        win_printf_OK("No image found");
    ony = ois->im.ny;	onx = ois->im.nx;
    ony_2 = ony/2;   onx_2 = onx/2;        // image center
    nout_2 = 1 + (int)sqrt(onx_2 * onx_2 + ony_2 * ony_2);
    fil_f = nout_2;
    for (i = 2; i < nout_2; i*=2);
    nout_2 = i;
    nout = 2 * nout_2;
    //ps = ois->im.pixel;

    fft_init(nout);


    oip = create_and_attach_oi_to_imr(imr, nout, n_angle, IS_FLOAT_IMAGE);
    if (oip == NULL)      win_printf_OK("Can't create radial image");
    inherit_from_im_to_im(oip,ois);
    set_im_title(oip,"Sinogramme");
    set_im_y_title(oip,"Profile angle in \\pi/%d",n_angle);
    set_im_x_title(oip,"radial extend");
    uns_oi_2_oi(oip,ois);
    oip->im.win_flag = ois->im.win_flag;
    oip->need_to_refresh = ALL_NEED_REFRESH;
    set_formated_string(&oip->im.treatement,"radial profiles ");
    set_oi_periodic_in_x(oip);

    //win_printf_OK("oip created");

    oir = create_and_attach_oi_to_imr(imr, n_angle, nout_2, IS_COMPLEX_IMAGE);
    if (oir == NULL)      win_printf_OK("Can't create radial image");
    inherit_from_im_to_im(oir,ois);
    oir->im.mode = LOG_AMP;
    set_im_x_title(oir,"ortho radial wavenumber k_{\theta}");
    set_im_y_title(oir,"radial wavenumber k_r");
    uns_oi_2_oi(oir,ois);
    oir->im.win_flag = ois->im.win_flag;
    oir->need_to_refresh = ALL_NEED_REFRESH;
    set_formated_string(&oir->im.treatement,"sloppy radial fft2d image ");
    set_oi_periodic_in_x(oir);

    //win_printf_OK("2");

    oid2 = create_and_attach_oi_to_imr(imr, nout, nout_2, IS_COMPLEX_IMAGE);
    if (oid2 == NULL)      win_printf_OK("Can't create dest image");
    inherit_from_im_to_im(oid2,ois);
    oid2->im.mode = LOG_AMP;
    oid2->ax = -onx/2;
    set_im_x_title(oid2,"wavenumber k_x");
    set_im_y_title(oid2,"wavenumber k_y");
    uns_oi_2_oi(oid2,ois);
    oid2->im.win_flag = ois->im.win_flag;
    oid2->ax = -nout/2;
    oid2->need_to_refresh = ALL_NEED_REFRESH;
    set_formated_string(&oid2->im.treatement,"sloppy fft2d image ");
    set_oi_periodic_in_x_and_y(oid2);
    map_pixel_ratio_of_image_and_screen(oid2, 1.0, 1.0);

    //win_printf_OK("3");

    oid = create_and_attach_oi_to_imr(imr, nout, nout_2, IS_COMPLEX_IMAGE);
    if (oid == NULL)      win_printf_OK("Can't create dest image");
    inherit_from_im_to_im(oid,ois);
    oid->im.mode = LOG_AMP;
    oid->ax = -onx/2;
    set_im_x_title(oid,"wavenumber k_x");
    set_im_y_title(oid,"wavenumber k_y");
    uns_oi_2_oi(oid,ois);
    oid->im.win_flag = ois->im.win_flag;
    oid->ax = -nout/2;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    set_formated_string(&oid->im.treatement,"sloppy fft2d image ");
    set_oi_periodic_in_x_and_y(oid);
    map_pixel_ratio_of_image_and_screen(oid, 1.0, 1.0);


    //win_printf_OK("4");


    in   = (double *) fftw_malloc( nout * sizeof(double));
    outc = (fftw_complex *) fftw_malloc((nout_2+1)*sizeof(fftw_complex));
    if (in == NULL || outc == NULL)
        return win_printf_OK("cannot create fftw arrays !");
    plan = fftw_plan_dft_r2c_1d(nout, in, outc, FFTW_ESTIMATE);
    pd = oid->im.pixel;
    pr = oir->im.pixel;
    tmp = (float*)calloc(nout,sizeof(float));
    fil = (float*)calloc(nout_2,sizeof(float));
    if (tmp == NULL || fil == NULL)
        return win_printf_OK("cannot create fftw arrays !");
    if (fft_init(nout))
        return win_printf_OK("cannot init fft");

    //win_printf_OK("5");

    fil_f = fil_f/2;
    for (i = 0; i < nout_2; i++)
    {
        if (i < fil_f) 	                    fil[i] = 1;
        else if (i < 2*fil_f && i >= fil_f)   fil[i] = (1 - cos((M_PI*i)/fil_f))/2;
        else  	                            fil[i] = 0;
    }

    for (i_angle = 0; i_angle < n_angle; i_angle++)
    {
        angle_de = (180 * i_angle) /n_angle;
        if (op != NULL) for (i = 0; i < op->dat[0]->nx; i++) op->dat[0]->yd[i] = op->dat[0]->xd[i] = 0;
        op = image_projection_along_an_angle(ois, op, 0, angle_de);
        if (op == NULL)    win_printf_OK ("Operation failed!\n");
        ds = op->dat[0];
        for (i = 0; i < nout; i++) in[i] = 0;
        for (i = 0; i < nout; i++) tmp[i] = 0;
        for (i = 0; i < nout && i < ds->nx; i++) oip->im.pixel[i_angle].fl[nout_2-(ds->nx/2)+i] = ds->yd[i];

        for (i = 0; i < nout_2 && i < ds->nx/2; i++)
        {
            in[i] = (double)ds->yd[i+ds->nx/2];
            in[nout-i-1] = (double)ds->yd[(ds->nx/2)-i-1];
            //in[i+nout_2] = (double)ds->yd[i+ds->nx/2];
            //in[nout_2-i-1] = (double)ds->yd[(ds->nx/2)-i-1];
            //oip->im.pixel[i_angle].fl[i+nout_2] = tmp[i+nout_2] = ds->yd[i+ds->nx/2];
            //oip->im.pixel[i_angle].fl[nout_2-i-1] = tmp[nout_2-i-1] = ds->yd[(ds->nx/2)-i-1];
        }
        //for (i = 0; i < nout; i++) ds->yd[i] = (float)in[i]; // debug
        fftw_execute(plan);

        realtr1(nout, tmp);
        fft(nout, tmp, 1);
        realtr2(nout, tmp, 1);



        co = cos(M_PI*angle_de/180);
        si = sin(M_PI*angle_de/180);
        for (i = 0; i < nout_2; i++)
        {
            ikx = nout_2 + (int) (0.5+ (co * i));
            iky = (int) (0.5+ (si * i));
            if (ikx >= 0 && ikx < nout && iky >= 0 && iky < nout_2)
            {
                pr[i].fl[2*i_angle] = (float)outc[i][0] * fil[i];
                pr[i].fl[2*i_angle+1] = (float)outc[i][1] * fil[i];
                pd[iky].fl[2*ikx] += (float)outc[i][0] * fil[i] * ((float)i/nout_2);
                pd[iky].fl[2*ikx+1] += (float)outc[i][1] * fil[i] * ((float)i/nout_2);
                //pd[iky].fl[2*ikx] += tmp[2*i];
                //pd[iky].fl[2*ikx+1] += tmp[2*i+1];
            }
        }
    }
    //win_printf_OK("6");
    pd = oid2->im.pixel;
    for (j = 0; j < nout_2; j++)
    {
        for (i = 0; i < nout; i++)
        {
            k = j * j + (i - nout_2) * (i - nout_2);
            k = sqrt(k);
            if (k > 0)
            {
                angle_de = atan2(((double)j)/k,((double)(nout_2-i))/k);
                theta = n_angle*angle_de/M_PI;

                if (theta < 0 || theta > (float)n_angle)
                {
                    if (debug != WIN_CANCEL)
                        debug = win_printf("angle %f out of [0,%d[\ni %d j %d k %g\n"
                                           ,theta,n_angle,i,j,k);
                }
                else
                {
                    interpolate_image_point(oir, theta, k, &re, &im);
                    pd[j].fl[2*i] = re;
                    pd[j].fl[2*i+1] = im;
                }
                /*
                   if (j == nout_2/4 && i == nout_2)
                   win_printf("angle %f k %f \nre %g im %g\nre0 %g imO %g "
                   ,theta,k,re,im,pr[nout_2/4].fl[n_angle],pr[nout_2/4].fl[1+n_angle]);
                   */
            }
            else
            {
                pd[j].fl[2*i] = pr[0].fl[0];
                pd[j].fl[2*i+1] = pr[0].fl[1];
            }
        }
    }
    for (j = 0; j < nout; j++)
        pd[0].fl[j] = 0;

    //win_printf_OK("7");

    add_to_one_image(ois, IS_ONE_PLOT, op);
    ois->need_to_refresh |= PLOT_NEED_REFRESH;

    //oid->height *= 0.5;
    //oid2->height *= 0.5;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oid);
    find_zmin_zmax(oid2);
    find_zmin_zmax(oir);
    find_zmin_zmax(oip);
    return refresh_image(imr, imr->n_oi - 1);
}


int do_reconstruct_image_from_angular_projection_only(void)
{
    imreg *imr;
    O_i *ois, *oid = NULL, *oid2 = NULL;
    O_i *oir = NULL; // radial fft image
    static float angle_de = 45;
    static int all = 0, ang = 12;
    int n_angle;
    int  i, j, i_angle, ikx, iky, debug = WIN_OK;
    int nout = 0, nout_2, onx, ony, fil_f; // , onx_2, ony_2
    union pix  *pd, *pr; // *ps,
    float  co, si, *tmp, re, im, k, theta, *fil;
    double  	*in=NULL;
    fftw_complex	*outc=NULL;
    fftw_plan     plan;     // fft plan for fftw



    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

    if (ois == NULL || ois->im.pixel == NULL)
        win_printf_OK("No image found");


    if (ois->im.data_type != IS_FLOAT_IMAGE)
        return win_printf_OK("input image must be float");
    ony = ois->im.ny;	onx = ois->im.nx;


    //ony_2 = ony/2;   onx_2 = onx/2;        // image center
    nout_2 = onx/2;
    fil_f = nout_2;
    for (i = 2; i < nout_2; i*=2);
    nout_2 = i;
    nout = 2 * nout_2;
    //ps = ois->im.pixel;
    n_angle = ony;
    fft_init(nout);


    i = win_scanf("all angle %R or one %r\ndefine angle %8d\n",&all,&ang);
    if (i == WIN_CANCEL) return D_O_K;

    oir = create_and_attach_oi_to_imr(imr, ony, nout_2, IS_COMPLEX_IMAGE);
    if (oir == NULL)      win_printf_OK("Can't create radial image");
    inherit_from_im_to_im(oir,ois);
    oir->im.mode = LOG_AMP;
    set_im_x_title(oir,"ortho radial wavenumber k_{\theta}");
    set_im_y_title(oir,"radial wavenumber k_r");
    uns_oi_2_oi(oir,ois);
    oir->im.win_flag = ois->im.win_flag;
    oir->need_to_refresh = ALL_NEED_REFRESH;
    set_formated_string(&oir->im.treatement,"sloppy radial fft2d image ");
    set_oi_periodic_in_x(oir);

    //win_printf_OK("2");

    oid2 = create_and_attach_oi_to_imr(imr, nout, nout_2, IS_COMPLEX_IMAGE);
    if (oid2 == NULL)      win_printf_OK("Can't create dest image");
    inherit_from_im_to_im(oid2,ois);
    oid2->im.mode = LOG_AMP;
    oid2->ax = -onx/2;
    set_im_x_title(oid2,"wavenumber k_x");
    set_im_y_title(oid2,"wavenumber k_y");
    uns_oi_2_oi(oid2,ois);
    oid2->im.win_flag = ois->im.win_flag;
    oid2->ax = -nout/2;
    oid2->need_to_refresh = ALL_NEED_REFRESH;
    set_formated_string(&oid2->im.treatement,"sloppy fft2d image ");
    set_oi_periodic_in_x_and_y(oid2);
    map_pixel_ratio_of_image_and_screen(oid2, 1.0, 1.0);

    //win_printf_OK("3");

    oid = create_and_attach_oi_to_imr(imr, nout, nout_2, IS_COMPLEX_IMAGE);
    if (oid == NULL)      win_printf_OK("Can't create dest image");
    inherit_from_im_to_im(oid,ois);
    oid->im.mode = LOG_AMP;
    oid->ax = -onx/2;
    set_im_x_title(oid,"wavenumber k_x");
    set_im_y_title(oid,"wavenumber k_y");
    uns_oi_2_oi(oid,ois);
    oid->im.win_flag = ois->im.win_flag;
    oid->ax = -nout/2;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    set_formated_string(&oid->im.treatement,"sloppy fft2d image ");
    set_oi_periodic_in_x_and_y(oid);
    map_pixel_ratio_of_image_and_screen(oid, 1.0, 1.0);


    //win_printf_OK("4");


    in   = (double *) fftw_malloc( nout * sizeof(double));
    outc = (fftw_complex *) fftw_malloc((nout_2+1)*sizeof(fftw_complex));
    if (in == NULL || outc == NULL)
        return win_printf_OK("cannot create fftw arrays !");
    plan = fftw_plan_dft_r2c_1d(nout, in, outc, FFTW_ESTIMATE);
    pd = oid->im.pixel;
    pr = oir->im.pixel;
    tmp = (float*)calloc(nout,sizeof(float));
    fil = (float*)calloc(nout_2,sizeof(float));
    if (tmp == NULL || fil == NULL)
        return win_printf_OK("cannot create fftw arrays !");
    if (fft_init(nout))
        return win_printf_OK("cannot init fft");

    //win_printf_OK("5");

    fil_f = fil_f/2;
    for (i = 0; i < nout_2; i++)
    {
        if (i < fil_f) 	                    fil[i] = 1;
        else if (i < 2*fil_f && i >= fil_f)   fil[i] = (1 - cos((M_PI*i)/fil_f))/2;
        else  	                            fil[i] = 0;
    }

    for (i_angle = 0; i_angle < n_angle; i_angle++)
    {
        angle_de = (180 * i_angle) /n_angle;
        for (i = 0; i < nout; i++) in[i] = 0;
        for (i = 0; i < nout; i++) tmp[i] = 0;

        for (i = 0; i < nout_2; i++)
        {
            in[i] = (double)ois->im.pixel[i_angle].fl[i+nout_2];
            in[nout-i-1] = (double)ois->im.pixel[i_angle].fl[(nout_2)-i-1];
        }
        fftw_execute(plan);

        realtr1(nout, tmp);
        fft(nout, tmp, 1);
        realtr2(nout, tmp, 1);



        co = cos(M_PI*angle_de/180);
        si = sin(M_PI*angle_de/180);
        for (i = 0; i < nout_2; i++)
        {
            ikx = nout_2 + (int) (0.5+ (co * i));
            iky = (int) (0.5+ (si * i));
            if (ikx >= 0 && ikx < nout && iky >= 0 && iky < nout_2)
            {
                pr[i].fl[2*i_angle] = (float)outc[i][0] * fil[i];
                pr[i].fl[2*i_angle+1] = (float)outc[i][1] * fil[i];
                if (all == 0 || ang == i_angle)
                {
                    pd[iky].fl[2*ikx] += (float)outc[i][0] * fil[i] * ((float)i/nout_2);
                    pd[iky].fl[2*ikx+1] += (float)outc[i][1] * fil[i] * ((float)i/nout_2);
                }
                //pd[iky].fl[2*ikx] += tmp[2*i];
                //pd[iky].fl[2*ikx+1] += tmp[2*i+1];
            }
        }
    }
    //win_printf_OK("6");
    pd = oid2->im.pixel;
    for (j = 0; j < nout_2; j++)
    {
        for (i = 0; i < nout; i++)
        {
            k = j * j + (i - nout_2) * (i - nout_2);
            k = sqrt(k);
            if (k > 0)
            {
                angle_de = atan2(((double)j)/k,((double)(nout_2-i))/k);
                theta = n_angle*angle_de/M_PI;

                if (theta < 0 || theta > (float)n_angle)
                {
                    if (debug != WIN_CANCEL)
                        debug = win_printf("angle %f out of [0,%d[\ni %d j %d k %g\n"
                                           ,theta,n_angle,i,j,k);
                }
                else
                {
                    interpolate_image_point(oir, theta, k, &re, &im);
                    pd[j].fl[2*i] = re;
                    pd[j].fl[2*i+1] = im;
                }
                /*
                   if (j == nout_2/4 && i == nout_2)
                   win_printf("angle %f k %f \nre %g im %g\nre0 %g imO %g "
                   ,theta,k,re,im,pr[nout_2/4].fl[n_angle],pr[nout_2/4].fl[1+n_angle]);
                   */
            }
            else
            {
                pd[j].fl[2*i] = pr[0].fl[0];
                pd[j].fl[2*i+1] = pr[0].fl[1];
            }
        }
    }
    for (j = 0; j < nout; j++)
        pd[0].fl[j] = 0;

    oid->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oid);
    find_zmin_zmax(oid2);
    find_zmin_zmax(oir);
    return refresh_image(imr, imr->n_oi - 1);
}



int do_cut_image_in_small_parts(void)
{
    imreg *imr = NULL;
    O_i *ois = NULL, *oim = NULL, *oif = NULL;
    int i, j, x = 0, y = 0, nf;
    static int size = 256, dofft = 1;
    static unsigned long t_c, to = 0;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("This routine will cut an image in smaller ones\n"
                  "with 1/2 covering edge for correlation\n"
                  "define the size of the small image %8d\n"
		  "%b->DO FFT-2D\n",&size,&dofft);
    if (i == WIN_CANCEL) return 0;

    if (ois == NULL)      win_printf_OK("No image found");
    nf = compute_nb_of_subset(ois, size, size, NULL, NULL);
    //win_printf_OK("Nb of small images %d",nf);
    oim = create_and_attach_movie_to_imr(imr, size, size, ois->im.data_type, nf);
    if (dofft) oif = create_and_attach_movie_to_imr(imr, size, size/2, IS_COMPLEX_IMAGE, nf);
    t_c = my_uclock();
    for (j = 0; j < nf; j++)
    {
        switch_frame(oim,j);
        switch_frame(oif,j);
        find_subset_pos(ois, size, size, j, &x, &y);
        copy_one_subset_of_image_and_border(oim, ois, x, y);
        //win_printf_OK("small image %d\nx %d y %d\n",j,x,y);
        if (dofft) forward_fft_2d_im(oif, oim, NULL, 0.05);
    }
    t_c = my_uclock() - t_c;
    oim->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oim);
    if (dofft)
      {
	oif->need_to_refresh = ALL_NEED_REFRESH;
	find_zmin_zmax(oif);
	oif->height *= 0.5;
      }
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    //my_set_window_title
    win_printf("%s took %g ms",(dofft)?"FFT":"Cut",(double)(t_c*1000)/to);
    return refresh_image(imr, imr->n_oi - 1);
}
int do_reconstruct_image_from_small_parts(void)
{
    imreg *imr = NULL;
    O_i *ois = NULL, *oid = NULL;
    int i, nf, onx, ony, negx, negy;
    static unsigned long t_c, to = 0;
    static int replace = 0;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    nf = nb_of_frames_in_movie(ois);
    if(updating_menu_state != 0)
      {
	if (nf < 2) 	active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }
    if (ois == NULL || nf < 2)      win_printf_OK("No image found");
     for (i = 0, onx = ony = negx = negy = 0; i < nf; i++)
      {
	switch_frame(ois,i);
	onx = (ois->im.user_ispare[i][0] > onx) ? ois->im.user_ispare[i][0] : onx;
	negx = (ois->im.user_ispare[i][0] < negx) ? ois->im.user_ispare[i][0] : negx;
	ony = (ois->im.user_ispare[i][1] > ony) ? ois->im.user_ispare[i][1] : ony;
	negy = (ois->im.user_ispare[i][1] < negy) ? ois->im.user_ispare[i][1] : negy;
	//win_printf("i->%d x0 %d y0 %d",i,ois->im.user_ispare[i][0],ois->im.user_ispare[i][1]);
      }
    onx -= negx;
    ony -= negy;
    onx += ois->im.nx;
    ony += ois->im.ny;
    win_printf("onx %d ony %d",onx,ony);
    i = win_scanf("Reconstructing image from small images\n"
		  "%R->Add or %r->replace\n",&replace);
    if (i == WIN_CANCEL) return 0;
    oid = create_and_attach_oi_to_imr(imr, onx, ony, ois->im.data_type);
    t_c = my_uclock();
    for (i = 0; i < nf; i++)
    {
        switch_frame(ois,i);
	//copy_one_subset_of_image_in_a_bigger_one(oid, ois->im.user_ispare[i][0],
	//					 ois->im.user_ispare[i][1], ois,
	//					 0, 0, ois->im.nx, ois->im.ny);
	copy_or_add_one_subset_of_image_in_a_bigger_one(oid, ois->im.user_ispare[i][0]-negx,
							ois->im.user_ispare[i][1]-negy, ois,
							0, 0, ois->im.nx, ois->im.ny,(replace)?0:1);

    }
    t_c = my_uclock() - t_c;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oid);
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    win_printf("reconstruct took %g ms",(double)(t_c*1000)/to);
    return refresh_image(imr, imr->n_oi - 1);
}

int find_mean_of_edge(O_i *oi, int w, float *zm)
{
    int i, j;
    int mode, nmean = 0, *li, onx, ony;
    unsigned char *ch;
    rgba_t *rgba;
    rgba16_t *rgba16;
    short int *in;
    unsigned short int *ui;
    float *fl, zmean = 0, t, zr, zi;
    double *db;

    if (oi == NULL)		return xvin_error(Wrong_Argument);
    mode = oi->im.mode;

    ony = oi->im.ny;
    onx = oi->im.nx;
    if ( oi->im.data_type == IS_CHAR_IMAGE)
    {
        for (i = 0; i< ony ; i++)
        {
            ch = oi->im.pixel[i].ch;
            for (j=0 ; j< onx ; j++)
            {
                if (i < w || ony - i < w || j < w || onx -j < w)
                {
                    zmean += (float)(ch[j]);
                    nmean++;
                }
            }
        }
    }
    else 	if ( oi->im.data_type == IS_RGB_PICTURE)
    {
        for (i = 0; i< ony ; i++)
        {
            ch = oi->im.pixel[i].ch;
            for (j=0 ; j< onx ; j++)
            {
                if (i < w || ony - i < w || j < w || onx -j < w)
                {
                    zmean += (float) (ch[3*j]+ch[(3*j)+1]+ch[(3*j)+2]);
                    nmean++;
                }
            }
        }
    }
    else 	if ( oi->im.data_type == IS_RGBA_PICTURE)
    {
        for (i = 0; i< ony ; i++)
        {
            rgba = oi->im.pixel[i].rgba;
            for (j=0 ; j< onx ; j++)
            {
                if (i < w || ony - i < w || j < w || onx -j < w)
                {
                    zmean += (float) (rgba[j].b + rgba[j].g + rgba[j].r);
                    nmean++;
                }
            }
        }
    }
    else 	if ( oi->im.data_type == IS_RGB16_PICTURE)
    {
        for (i = 0; i< ony ; i++)
        {
            in = oi->im.pixel[i].in;
            for (j=0 ; j< onx ; j++)
            {
                if (i < w || ony - i < w || j < w || onx -j < w)
                {
                    zmean += (float) (in[3*j]+in[(3*j)+1]+in[(3*j)+2]);
                    nmean++;
                }
            }
        }
    }
    else 	if ( oi->im.data_type == IS_RGBA16_PICTURE)
    {
        for (i = 0; i< ony ; i++)
        {
            rgba16 = oi->im.pixel[i].rgba16;
            for (j=0 ; j< onx ; j++)
            {
                if (i < w || ony - i < w || j < w || onx -j < w)
                {
                    zmean += (float) (rgba16[j].b + rgba16[j].g + rgba16[j].r);
                    nmean++;
                }
            }
        }
    }
    else if ( oi->im.data_type == IS_INT_IMAGE)
    {
        for (i = 0; i< ony ; i++)
        {
            in = oi->im.pixel[i].in;
            for (j=0 ; j< onx ; j++)
            {
                if (i < w || ony - i < w || j < w || onx -j < w)
                {
                    zmean += (float) (in[j]);
                    nmean++;
                }
            }
        }
    }
    else if ( oi->im.data_type == IS_UINT_IMAGE)
    {
        for (i = 0; i< ony ; i++)
        {
            ui = oi->im.pixel[i].ui;
            for (j=0 ; j< onx ; j++)
            {
                if (i < w || ony - i < w || j < w || onx -j < w)
                {
                    zmean += (float) (ui[j]);
                    nmean++;
                }
            }
        }
    }
    else if ( oi->im.data_type == IS_LINT_IMAGE)
    {
        for (i = 0; i< ony ; i++)
        {
            li = oi->im.pixel[i].li;
            for (j=0 ; j< onx ; j++)
            {
                if (i < w || ony - i < w || j < w || onx -j < w)
                {
                    zmean += (float) (li[j]);
                    nmean++;
                }
            }
        }
    }
    else if ( oi->im.data_type == IS_FLOAT_IMAGE)
    {
        for (i = 0; i< ony ; i++)
        {
            fl = oi->im.pixel[i].fl;
            for (j=0 ; j< onx ; j++)
            {
                if (i < w || ony - i < w || j < w || onx -j < w)
                {
                    zmean += fl[j];
                    nmean++;
                }
            }
        }
    }
    else if ( oi->im.data_type == IS_DOUBLE_IMAGE)
    {
        for (i = 0; i< ony ; i++)
        {
            db = oi->im.pixel[i].db;
            for (j=0 ; j< onx ; j++)
            {
                if (i < w || ony - i < w || j < w || onx -j < w)
                {
                    zmean += (float)db[j];
                    nmean++;
                }
            }
        }
    }
    else if ( oi->im.data_type == IS_COMPLEX_IMAGE)
    {
        for (i = 0; i< ony ; i++)
        {
            fl = oi->im.pixel[i].fl;
            for (j = 0; j < onx; j++)
            {
                if (i < w || ony - i < w || j < w || onx -j < w)
                {
                    zr = fl[2*j];
                    zi = fl[2*j+1];
                    if (mode == RE)	    t = zr;
                    else if (mode == IM)	    t = zi;
                    else if (mode == AMP)	    t = sqrt(zr*zr+zi*zi);
                    else if (mode == AMP_2)   t = zr*zr+zi*zi;
                    else if (mode == LOG_AMP) t = (zr*zr+zi*zi > 0) ? log10(zr*zr+zi*zi) : -40.0;
                    else return 1;
                    zmean += t;
                    nmean++;
                }
            }
        }
    }
    else if ( oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        for (i = 0; i < ony; i++)
        {
            db = oi->im.pixel[i].db;
            for (j = 0; j < onx; j++)
            {
                if (i < w || ony - i < w || j < w || onx -j < w)
                {
                    zr = db[2*j];
                    zi = db[2*j+1];
                    if (mode == RE)	    t = zr;
                    else if (mode == IM)	    t = zi;
                    else if (mode == AMP)	    t = sqrt(zr*zr+zi*zi);
                    else if (mode == AMP_2)   t = zr*zr+zi*zi;
                    else if (mode == LOG_AMP) t = (zr*zr+zi*zi > 0) ? log10(zr*zr+zi*zi) : -40.0;
                    else return 1;
                    zmean += t;
                    nmean++;
                }
            }
        }
    }
    else return 1;
    zmean = (nmean > 0) ? zmean/nmean : zmean;
    if (zm) *zm = zmean;
    return 0;
}




// small overlap
int do_cut_ROI_of_image_in_small_parts(void)
{
    imreg *imr;
    O_i *ois, *oim, *oif, *oifb, *oid, *apo, *desapo;
    int i, j, x = 0, y = 0, nf, im, ims;
    int xb, yb, wb, hb, nfi;
    static int size = 256, x0 = 0, y0 = 0, w0 = 512, w1 = 128, noROI = 0;
    static float lp = 3, lw = 1, hp = 6, hw = 2;
    static unsigned long t_c, to = 0;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("This routine will cut the ROI of an image in smaller ones\n"
                  "with covering edge for correlation\n"
                  "define the size of the small image %8d\n"
                  "ROI start x0 %6d y0 %6d w0 %6d w1 %6d\n"
                  "Set ROI to entire image size %b\n"
                  "Band-pass filter cutoff lowpass  %10flowpass width %10f\n"
                  "cutoff highpass  %10fhighpass width %10f"
                  ,&size,&x0,&y0,&w0,&w1,&noROI,&lp,&lw,&hp,&hw);
    if (i == WIN_CANCEL) return 0;

    if (noROI)
    {
        x0 = y0 = 0;
        w0 = ois->im.nx;
        w1 = ois->im.ny;
    }


    if (ois == NULL)      win_printf_OK("No image found");

    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;

    apo = create_appodisation_float_image(size, size, 0.05, 0);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");

    desapo = create_appodisation_float_image(size, size, 0.05, 1);
    if (desapo == NULL)	return win_printf_OK("Could not create apodisation image!");

    nf = compute_nb_of_min_subset_ROI(ois, size, size, x0, y0, w0, w1);
    //win_printf_OK("Nb of small images %d",nf);
    oim = create_and_attach_movie_to_imr(imr, size, size, ois->im.data_type, nf);
    oif = create_and_attach_movie_to_imr(imr, size, size/2, IS_COMPLEX_IMAGE, nf);
    oifb = create_and_attach_movie_to_imr(imr, size, size, IS_FLOAT_IMAGE, nf);

    if (nfi < 2) oid = create_and_attach_oi_to_imr(imr, w0, w1, IS_FLOAT_IMAGE);
    else         oid = create_and_attach_movie_to_imr(imr, w0, w1, IS_FLOAT_IMAGE, nfi);


    t_c = my_uclock();
    for (ims = 0; ims < nfi; ims++)
    {
        switch_frame(ois,ims);
        switch_frame(oid,ims);
        my_set_window_title("Treating image %d",ims);
        for (j = 0; j < nf; j++)
        {
            switch_frame(oim,j);
            switch_frame(oif,j);
            find_min_subset_pos_ROI(ois, size, size, j, &x, &y, x0, y0, w0, w1);
            copy_one_subset_of_image(oim, ois, x, y);
            //win_printf_OK("small image %d\nx %d y %d\n",j,x,y);
        }
        forward_fftbt_2d_im_appo(oif, oim, 0, screen, apo, 0);
        band_pass_filter_fft_2d_im(oif, oif,  0, lp,  lw,  hp,  hw, 0, 0);
        backward_fftbt_2d_im_desappo(oifb, oif, 0, screen, desapo, 0);
        for (im = 0; im < nf; im++)
        {
            switch_frame(oifb, im);
            //switch_frame(oim, im);
            find_min_subset_pos_ROI(ois, size, size, im, &x, &y, 0, 0, w0, w1);
            find_min_subset_good_pixel_pos_ROI(ois, size, size, im, &xb, &yb, &wb, &hb, w0, w1);
            for (i = 0; i < hb; i++)
            {
                if (((yb + i) > w1) || ((yb + i) < 0))
                {
                    win_printf("yb %d + i %d > size %d at im %d",yb,i,w1,im);
                    continue;
                }
                if (((yb-y+i) > size) || ((yb-y+i) < 0))
                {
                    win_printf("yb %d - y %d + i %d > size %d at im %d",yb,y,i,size,im);
                    continue;
                }
                for (j = 0; j < wb; j++)
                {
                    if (((xb + j) > w0) || ((xb + j) < 0))
                    {
                        win_printf("xb %d + j %d > w0 %d at im %d",xb,j,w0,im);
                        continue;
                    }
                    if (((xb-x+j) > size) || ((xb-x+j) < 0))
                    {
                        win_printf("xb %d - x %d + j %d > size %d at im %d",xb,x,j,size,im);
                        continue;
                    }
                    oid->im.pixel[yb+i].fl[xb+j] = oifb->im.pixel[yb-y+i].fl[xb-x+j] ;
                    //oid->im.pixel[yb+i].fl[xb+j] = oim->im.pixel[yb-y+i].ch[xb-x+j] ;
                }
            }
        }
    }
    free_one_image(apo);
    free_one_image(desapo);
    t_c = my_uclock() - t_c;
    oim->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oim);
    oif->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oif);
    oif->height *= 0.5;
    find_zmin_zmax(oid);
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    //my_set_window_title
    win_printf("FFT took %g ms",(double)(t_c*1000)/to);
    return refresh_image(imr, imr->n_oi - 1);
}



int do_PIV(void)
{
    imreg *imr;
    O_i *ois, *oim, *oif, *oifb, *oim2, *oif2, *oid, *apo;
    int i, j, x = 0, y = 0, nf, im, ims;
    int xb, yb, wb, hb, nfi, size_4;
    static int size = 256, x0 = 0, y0 = 0, w0 = 512, w1 = 128, noROI = 0;
    static float lp = 3, lw = 1, hp = 6, hw = 2;
    static unsigned long t_c, to = 0;
    float zm;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;
    if (nfi < 2) return win_printf_OK("This is not a movie!");

    i = win_scanf("This routine will cut the ROI of an image in smaller ones\n"
                  "with covering edge for correlation and compute PIV\n"
                  "define the size of the small image %8d\n"
                  "ROI start x0 %6d y0 %6d w0 %6d w1 %6d\n"
                  "Set ROI to entire image size %b\n"
                  "Band-pass filter cutoff lowpass  %10flowpass width %10f\n"
                  "cutoff highpass  %10fhighpass width %10f"
                  ,&size,&x0,&y0,&w0,&w1,&noROI,&lp,&lw,&hp,&hw);
    if (i == WIN_CANCEL) return 0;
    size_4 = size/4;
    if (noROI)
    {
        x0 = y0 = 0;
        w0 = ois->im.nx;
        w1 = ois->im.ny;
    }


    if (ois == NULL)      win_printf_OK("No image found");

    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;

    apo = create_appodisation_float_image(size, size, 0.05, 0);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");


    nf = compute_nb_of_min_subset_ROI(ois, size, size, x0, y0, w0, w1);
    //win_printf_OK("Nb of small images %d",nf);
    oim = create_and_attach_movie_to_imr(imr, size, size, ois->im.data_type, nf);
    oif = create_and_attach_movie_to_imr(imr, size, size/2, IS_COMPLEX_IMAGE, nf);
    oifb = create_and_attach_movie_to_imr(imr, size, size, IS_FLOAT_IMAGE, nf);
    set_oi_periodic_in_x_and_y(oifb);

    oim2 = create_and_attach_movie_to_imr(imr, size, size, ois->im.data_type, nf);
    oif2 = create_and_attach_movie_to_imr(imr, size, size/2, IS_COMPLEX_IMAGE, nf);

    oid = create_and_attach_movie_to_imr(imr, w0, w1, IS_FLOAT_IMAGE, nfi-1);


    t_c = my_uclock();
    for (ims = 0; ims < nfi-1; ims++)
    {
        switch_frame(ois,ims);
        switch_frame(oid,ims);
        my_set_window_title("Treating image %d",ims);
        for (j = 0; j < nf; j++)
        {
            switch_frame(oim,j);
            find_min_subset_pos_ROI(ois, size, size, j, &x, &y, x0, y0, w0, w1);
            copy_one_subset_of_image(oim, ois, x, y);
        }
        forward_fftbt_2d_im_appo(oif, oim, 0, screen, apo, 0);
        //band_pass_filter_fft_2d_im(oif, oif,  0, lp,  lw,  hp,  hw, 0, 0);
        switch_frame(ois,ims+1);
        for (j = 0; j < nf; j++)
        {
            switch_frame(oim2,j);
            find_min_subset_pos_ROI(ois, size, size, j, &x, &y, x0, y0, w0, w1);
            copy_one_subset_of_image(oim2, ois, x, y);
        }
        forward_fftbt_2d_im_appo(oif2, oim2, 0, screen, apo, 0);
        //band_pass_filter_fft_2d_im(oif, oif,  0, lp,  lw,  hp,  hw, 0, 0);
        for (j = 0; j < nf; j++)
        {
            switch_frame(oif,j);
            switch_frame(oif2,j);
            //corel_2d_im(oif, oif, oif2);
            corel_2d_im_norm(oif, oif, oif2, COREL_NORM_NONE);
        }
        set_oi_periodic_in_x_and_y(oif);
        backward_fftbt_2d_im(oifb, oif, 0, screen, 0.05);
        for (im = 0; im < nf; im++)
        {
            switch_frame(oifb, im);
            shift_2d_im(oifb, oifb);
            find_min_subset_pos_ROI(ois, size, size, im, &x, &y, 0, 0, w0, w1);
            find_min_subset_good_pixel_pos_ROI(ois, size, size, im, &xb, &yb, &wb, &hb, w0, w1);
            find_mean_of_edge(oifb, size_4, &zm);
            //zm = 0;
            for (i = 0; i < hb; i++)
            {
                if (((yb + i) > w1) || ((yb + i) < 0))
                {
                    win_printf("yb %d + i %d > size %d at im %d",yb,i,w1,im);
                    continue;
                }
                if (((yb-y+i) > size) || ((yb-y+i) < 0))
                {
                    win_printf("yb %d - y %d + i %d > size %d at im %d",yb,y,i,size,im);
                    continue;
                }
                for (j = 0; j < wb; j++)
                {
                    if (((xb + j) > w0) || ((xb + j) < 0))
                    {
                        win_printf("xb %d + j %d > w0 %d at im %d",xb,j,w0,im);
                        continue;
                    }
                    if (((xb-x+j) > size) || ((xb-x+j) < 0))
                    {
                        win_printf("xb %d - x %d + j %d > size %d at im %d",xb,x,j,size,im);
                        continue;
                    }
                    oid->im.pixel[yb+i].fl[xb+j] = oifb->im.pixel[yb-y+i].fl[xb-x+j] -zm ;
                    //oid->im.pixel[yb+i].fl[xb+j] = oim->im.pixel[yb-y+i].ch[xb-x+j] ;
                }
            }
        }
    }
    free_one_image(apo);
    t_c = my_uclock() - t_c;
    oim->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oim);
    oim2->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oim2);
    oif->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oif);
    oif->height *= 0.5;
    oif2->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oif2);
    oif2->height *= 0.5;
    oifb->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oifb);
    find_zmin_zmax(oid);
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    //my_set_window_title
    win_printf("FFT took %g ms",(double)(t_c*1000)/to);
    return refresh_image(imr, imr->n_oi - 1);
}

int do_auto_bead_find(void)
{
    imreg *imr;
    O_i *ois, *oism, *oismf, *oismfb, *oiref, *oiref_f, *oid, *apo, *oiref_fc, *oiref_fcb, *oiaponorm;
    O_p *op;
    int i, j, x = 0, y = 0, nf, im, x_min, x_max, y_min, y_max;
    int xb, yb, wb, hb, border_x = 0, border_y = 0;
    static int size = 256, x0 = 0, y0 = 0, w0 = 512, w1 = 128, noROI = 0;
    static float lp = 3, lw = 1, hp = -1, hw = -1, dx = 1;
    static unsigned long t_c, to = 0;
    float zm;
    double val_z;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

    op = find_oi_cur_op(ois);
    if (op == NULL) 	return OFF;

    i = (op->dat[op->cur_dat]->nx);
    for (size = 32; size < i; size *= 2);
    size *= 2;
    if (hp < 0) hp = size/4;
    if (hw < 0) hw = size/8;



    i = win_scanf("This routine will cut the ROI of an image in smaller ones with covering edge\n"
                  "and compute correlation with the image of a single bead described by its profile\n"
                  "define the size of the small image (a power of 2) %8d\n"
                  "Analyse the entire image %R or just a Region Of Interest %r\n"
                  "In the last case, ROI starts at x0 %6d y0 %6d extends in x %6d and y %6d\n"
                  "Define the Band-pass filter cutoff lowpass  %10flowpass width %10f\n"
                  "cutoff highpass  %10fhighpass width %10f\n"
                  "Define the extend of one spot pixel in image pixel %8f\n"
                  ,&size,&x0,&y0,&w0,&w1,&noROI,&lp,&lw,&hp,&hw,&dx);
    if (i == WIN_CANCEL) return 0;
    if (noROI == 0)
    {
        x0 = y0 = 0;
        w0 = ois->im.nx;
        w1 = ois->im.ny;
    }

    if (ois == NULL)      win_printf_OK("No image found");


    //nf = compute_nb_of_min_subset_ROI(ois, size, size, x0, y0, w0, w1);
    nf = compute_nb_of_min_subset_and_border_ROI(ois, size, size, x0, y0, w0, w1, &border_x, &border_y);

    apo = create_appodisation_variable_size_of_float_image(size, size, border_x, border_y, 0.0, 0);
    //create_appodisation_float_image(size, size, 0.05, 0);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");

    add_image(imr, apo->im.data_type, (void*)apo);

    //win_printf_OK("Nb of small images %d",nf);
    oism = create_and_attach_movie_to_imr(imr, size, size, ois->im.data_type, nf);
    if (oism == NULL)      win_printf_OK("Can't create dest image");
    oismf = create_and_attach_movie_to_imr(imr, size, size/2, IS_COMPLEX_IMAGE, nf);
    if (oismf == NULL)      win_printf_OK("Can't create dest image");

    oismfb = create_and_attach_movie_to_imr(imr, size, size, IS_FLOAT_IMAGE, nf);
    if (oismfb == NULL)      win_printf_OK("Can't create dest image");

    oiref = create_and_attach_oi_to_imr(imr, size, size, IS_FLOAT_IMAGE);
    if (oiref == NULL)      win_printf_OK("Can't create dest image");

    oiref_f = create_and_attach_oi_to_imr(imr, size, size/2, IS_COMPLEX_IMAGE);
    if (oiref_f == NULL)      win_printf_OK("Can't create dest image");

    oiref_fc = create_and_attach_oi_to_imr(imr, size, size/2, IS_COMPLEX_IMAGE);
    if (oiref_fc == NULL)      win_printf_OK("Can't create dest image");

    oiref_fcb = create_and_attach_oi_to_imr(imr, size, size, IS_FLOAT_IMAGE);
    if (oiref_fcb == NULL)      win_printf_OK("Can't create dest image");

    set_oi_periodic_in_x_and_y(oismfb);
    oid = create_and_attach_oi_to_imr(imr, w0, w1, IS_FLOAT_IMAGE);
    oiaponorm  = create_and_attach_oi_to_imr(imr, w0, w1, IS_FLOAT_IMAGE);

    t_c = my_uclock();
    add_spot_of_profile_to_image(oiref, size/2, size/2, dx, op->dat[0]);
    forward_fftbt_2d_im_appo(oiref_f, oiref, 0, screen, apo, 0);
    corel_2d_im_norm(oiref_fc, oiref_f, oiref_f, COREL_NORM_S2);
    set_oi_periodic_in_x_and_y(oiref_fc);
    backward_fftbt_2d_im(oiref_fcb, oiref_fc, 0, screen, 0.05);
    find_zmin_zmax_and_pos(oiref_fcb, &x_min, &x_max, &y_min, &y_max);
    get_raw_pixel_value(oiref_fcb, x_max, y_max, &val_z);

    for (j = 0; j < nf; j++)
    {
        switch_frame(oism,j);
        find_min_subset_pos_ROI(ois, size, size, j, &x, &y, x0, y0, w0, w1);
        copy_one_subset_of_image(oism, ois, x, y);
    }
    forward_fftbt_2d_im_appo(oismf, oism, 0, screen, apo, 0);
    band_pass_filter_fft_2d_im(oismf, oismf,  0, lp,  lw,  hp,  hw, 0, 0);
    for (j = 0; j < nf; j++)
    {
        switch_frame(oismf,j);
        corel_2d_im_norm(oismf, oismf, oiref_f, COREL_NORM_S2);
        //corel_2d_im_norm(O_i *dest, O_i *ois1, O_i *ois2, int norm)
    }
    set_oi_periodic_in_x_and_y(oismf);
    backward_fftbt_2d_im(oismfb, oismf, 0, screen, 0.05);
    for (im = 0; im < nf; im++)
    {
        switch_frame(oismfb, im);
        shift_2d_im(oismfb, oismfb);
        find_min_subset_pos_ROI(ois, size, size, im, &x, &y, 0, 0, w0, w1);
        find_min_subset_good_pixel_pos_ROI(ois, size, size, im, &xb, &yb, &wb, &hb, w0, w1);
        //find_mean_of_edge(oismfb, size_4, &zm);
        zm = 0;
        hb = wb = size;
        xb = x;
        yb = y;
        for (i = 0; i < hb; i++)
        {
            if (((yb + i) > w1) || ((yb + i) < 0))
            {
                win_printf("yb %d + i %d > size %d at im %d",yb,i,w1,im);
                continue;
            }
            if (((yb-y+i) > size) || ((yb-y+i) < 0))
            {
                win_printf("yb %d - y %d + i %d > size %d at im %d",yb,y,i,size,im);
                continue;
            }
            for (j = 0; j < wb; j++)
            {
                if (((xb + j) > w0) || ((xb + j) < 0))
                {
                    win_printf("xb %d + j %d > w0 %d at im %d",xb,j,w0,im);
                    continue;
                }
                if (((xb-x+j) > size) || ((xb-x+j) < 0))
                {
                    win_printf("xb %d - x %d + j %d > size %d at im %d",xb,x,j,size,im);
                    continue;
                }
                oid->im.pixel[yb+i].fl[xb+j] += oismfb->im.pixel[yb-y+i].fl[xb-x+j] -zm ;
                oiaponorm->im.pixel[yb+i].fl[xb+j] += apo->im.pixel[yb-y+i].fl[xb-x+j];
                //oid->im.pixel[yb+i].fl[xb+j] = oism->im.pixel[yb-y+i].ch[xb-x+j] ;
            }
        }
    }
    for (i = border_y; (i < (oid->im.ny - border_y)) && (i < (oiaponorm->im.ny - border_y)); i++)
    {
        for (j = border_x; (j < (oid->im.nx - border_x)) && (j < (oiaponorm->im.nx - border_x)); j++)
            if (oiaponorm->im.pixel[i].fl[j] > 0) oid->im.pixel[i].fl[j] /= oiaponorm->im.pixel[i].fl[j];
    }
    //free_one_image(apo);
    t_c = my_uclock() - t_c;
    oism->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oism);
    oiref->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oiref);
    oismf->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oismf);
    oismf->height *= 0.5;
    oiref_f->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oiref_f);
    oiref_f->height *= 0.5;
    oismfb->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oismfb);
    find_zmin_zmax(oid);
    find_zmin_zmax(oiaponorm);
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    //my_set_window_title
    win_printf("FFT took %g ms max corel %g",(double)(t_c*1000)/to,val_z);
    return refresh_image(imr, imr->n_oi - 1);
}


int add_new_im_extremum(im_ext_ar *iea, int i_ie, int x, int y, double data, float w, float x0, float y0, int tolerance)
{
  im_ext *ie;
  float tmp, d;
  //  static int verbose = D_O_K;

  if (iea == NULL || iea->ie == NULL || i_ie < 0) return -1;
  if (i_ie > (int) iea->n_ie) return -1;
  if (iea->n_ie >= iea->m_ie)
    {
      iea->m_ie = alloc_im_ext_array(iea, 2*iea->m_ie);
      if (iea->m_ie <= 0) return -2;
    }
  ie = iea->ie + i_ie;

  if (ie->np > 0)
    {
      tmp = ie->xpos - (ie->x0 + ie->x1)/2;
      d = tmp * tmp;
      tmp = ie->ypos - (ie->y0 + ie->y1)/2;
      d += tmp * tmp;
      if (((x0 - x)*(x0 - x) + (y0 - y)*(y0 - y)) > d)
	{  // we keep the closest max only
	  if (i_ie == (int) iea->n_ie) iea->n_ie++;
	  return i_ie;
	}
    }

  ie->xpos = x0;
  ie->ypos = y0;
  ie->zmax = data;
  ie->weight = w;
  ie->x0 = x - tolerance;
  ie->y0 = y - tolerance;
  ie->x1 = x + tolerance;
  ie->y1 = y + tolerance;
  ie->xm = x;
  ie->ym = y;
  ie->np = tolerance * tolerance * 4;
  if (i_ie == (int) iea->n_ie) iea->n_ie++;
  return i_ie;
}



im_ext_ar *find_maxima_position_local(O_i *oi, double threshold,  int tolerence, int max_number, int useROI)
{
  int i, j, k, ix, iy, ixo, iyo;
  int mode, *li;
  unsigned char *ch;
  rgba_t *rgba;
  rgba16_t *rgba16;
  short int *in;
  unsigned short int *ui;
  float *fl, zr, zi, dx, dy, w;
  double *db, t, t1;
  int  nmax, nys, nye, nxs, nxe, np=0, nw;
  im_ext_ar *iea;
  //  static int verbose = D_O_K;


  if (oi == NULL)	{xvin_error(Wrong_Argument); return NULL;}
  nmax = (max_number > 0) ? max_number : 16;
  iea = (im_ext_ar *)calloc(1,sizeof(im_ext_ar));
  if (iea == NULL)  return NULL;
  if (alloc_im_ext_array(iea, nmax) < 0) return NULL;
  mode = oi->im.mode;
  nxs = (useROI) ? oi->im.nxs : 0;
  nys = (useROI) ? oi->im.nys : 0;
  nxe = (useROI) ? oi->im.nxe : oi->im.nx;
  nye = (useROI) ? oi->im.nye : oi->im.ny;
  if ( oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = nys ; i< nye ; i++)
	{
	  ch = oi->im.pixel[i].ch;
	  for (j = nxs ; j< nxe ; j++)
	    {
	      t = (double) (ch[j]);
	      if ( t > threshold)
		{
		  k = find_in_im_extremum(iea, j, i, tolerence);
		  if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);
		  np++;
		}
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGB_PICTURE)
    {
      for (i = nys ; i < nye ; i++)
	{
	  ch = oi->im.pixel[i].ch;
	  for (j = nxs ; j < nxe ; j++)
	    {
	      t = (double)(ch[3*j])+(double)(ch[(3*j)+1])+(double)(ch[(3*j)+2]);
	      if ( t > threshold)
		{
		  k = find_in_im_extremum(iea, j, i, tolerence);
		  if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);
		  np++;
		}
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGBA_PICTURE)
    {
      for (i = nys ; i < nye ; i++)
	{
	  rgba = oi->im.pixel[i].rgba;
	  for (j = nxs ; j < nxe ; j++)
	    {
	      t = (double) (rgba[j].b) + (double)(rgba[j].g) + (double)(rgba[j].r);
	      if ( t > threshold)
		{
		  k = find_in_im_extremum(iea, j, i, tolerence);
		  if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);
		  np++;
		}
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGB16_PICTURE)
    {
      for (i = nys ; i < nye ; i++)
	{
	  in = oi->im.pixel[i].in;
	  for (j = nxs ; j < nxe ; j++)
	    {
	      t = (double) (in[3*j])+(double)(in[(3*j)+1])+(double)(in[(3*j)+2]);
	      if ( t > threshold)
		{
		  k = find_in_im_extremum(iea, j, i, tolerence);
		  if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);
		  np++;
		}
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGBA_PICTURE)
    {
      for (i = nys ; i < nye ; i++)
	{
	  rgba16 = oi->im.pixel[i].rgba16;
	  for (j = nxs ; j < nxe ; j++)
	    {
	      t = (double) (rgba16[j].b) + (double)(rgba16[j].g) + (double)(rgba16[j].r);
	      if ( t > threshold)
		{
		  k = find_in_im_extremum(iea, j, i, tolerence);
		  if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);
		  np++;
		}
	    }
	}
    }
  else if ( oi->im.data_type == IS_INT_IMAGE)
    {
      for (i = nys ; i < nye ; i++)
	{
	  in = oi->im.pixel[i].in;
	  for (j = nxs ; j < nxe ; j++)
	    {
	      t = (double) (in[j]);
	      if ( t > threshold)
		{
		  k = find_in_im_extremum(iea, j, i, tolerence);
		  if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);
		  np++;
		}
	    }
	}
    }
  else if ( oi->im.data_type == IS_UINT_IMAGE)
    {
      for (i = nys ; i < nye ; i++)
	{
	  ui = oi->im.pixel[i].ui;
	  for (j = nxs ; j < nxe ; j++)
	    {
	      t = (double) (ui[j]);
	      if ( t > threshold)
		{
		  k = find_in_im_extremum(iea, j, i, tolerence);
		  if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);
		  np++;
		}
	    }
	}
    }
  else if ( oi->im.data_type == IS_LINT_IMAGE)
    {
      for (i = nys ; i < nye ; i++)
	{
	  li = oi->im.pixel[i].li;
	  for (j = nxs ; j < nxe ; j++)
	    {
	      t = (double) (li[j]);
	      if ( t > threshold)
		{
		  k = find_in_im_extremum(iea, j, i, tolerence);
		  if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);
		  np++;
		}
	    }
	}
    }
  else if ( oi->im.data_type == IS_FLOAT_IMAGE)
    {
      for (i = nys ; i < nye ; i++)
	{
	  fl = oi->im.pixel[i].fl;
	  for (j = nxs ; j < nxe ; j++)
	    {
	      t = (double)(fl[j]);
	      if ( t > threshold)
		{
		  for(iy = -tolerence, nw = 0, w = 0, dx = 0, dy = 0; iy <= tolerence; iy++)
		    {
		      iyo = i + iy;
		      iyo = (iyo < 0) ? 0 : iyo;
		      iyo = (iyo < oi->im.ny) ? iyo : oi->im.ny - 1;
		      fl = oi->im.pixel[iyo].fl;
		      for(ix = -tolerence; ix <= tolerence; ix++)
			{
			  ixo = j + ix;
			  ixo = (ixo < 0) ? 0 : ixo;
			  ixo = (ixo < oi->im.nx) ? ixo : oi->im.nx - 1;
			  t1 = fl[ixo];
			  w += t1;
			  dx += t1*(ixo-j);
			  dy += t1*(iyo-i);
			  nw++;
			}
		    }
		  if (w > 0)
		    {
		      dx /= w;
		      dy /= w;
		    }
		  if ((fabs(dx) < 2.0) && (fabs(dy) < 2.0))
		    {
		      k = find_in_im_extremum(iea, j, i, tolerence);
		      // if (verbose != CANCEL)
		      //  verbose = win_printf("point %d,%d val %g",j,i,t);
		      if (k >= 0) add_new_im_extremum(iea, k, j, i, t, w, dx + j, dy + i, tolerence);
		      np++;
		    }
		}
	    }
	}
    }
  else if ( oi->im.data_type == IS_DOUBLE_IMAGE)
    {
      for (i = nys ; i < nye ; i++)
	{
	  db = oi->im.pixel[i].db;
	  for (j = nxs ; j < nxe ; j++)
	    {
	      t = db[j];
	      if ( t > threshold)
		{
		  k = find_in_im_extremum(iea, j, i, tolerence);
		  if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);
		  np++;
		}
	    }
	}
    }
  else if ( oi->im.data_type == IS_COMPLEX_IMAGE)
    {
      zr = oi->im.pixel[oi->im.nys].fl[2*oi->im.nxs];
      zi = oi->im.pixel[oi->im.nys].fl[2*oi->im.nxs+1];
      for (i = nys ; i < nye ; i++)
	{
	  fl = oi->im.pixel[i].fl;
	  for (j = nxs ; j < nxe ; j++)
	    {
	      zr = fl[2*j];
	      zi = fl[2*j+1];
	      if (mode == RE)		t = (double)zr;
	      else if (mode == IM)	t = (double)zi;
	      else if (mode == AMP)	t = sqrt(zr*zr+zi*zi);
	      else if (mode == AMP_2)	t = (double)(zr*zr+zi*zi);
	      else if (mode == LOG_AMP)	t = (zr*zr+zi*zi > 0) ? log10(zr*zr+zi*zi) : -40.0;
	      else return NULL;
	      if ( t > threshold)
		{
		  k = find_in_im_extremum(iea, j, i, tolerence);
		  if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);
		  np++;
		}
	    }
	}
    }
  else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
      zr = oi->im.pixel[oi->im.nys].db[2*oi->im.nxs];
      zi = oi->im.pixel[oi->im.nys].db[2*oi->im.nxs+1];
      for (i = nys ; i < nye ; i++)
	{
	  db = oi->im.pixel[i].db;
	  for (j = nxs ; j < nxe ; j++)
	    {
	      zr = db[2*j];
	      zi = db[2*j+1];
	      if (mode == RE)		t = zr;
	      else if (mode == IM)	t = zi;
	      else if (mode == AMP)	t = sqrt(zr*zr+zi*zi);
	      else if (mode == AMP_2)	t = zr*zr+zi*zi;
	      else if (mode == LOG_AMP)	t = (zr*zr+zi*zi > 0) ? log10(zr*zr+zi*zi) : -40.0;
	      else return NULL;
	      if ( t > threshold)
		{
		  k = find_in_im_extremum(iea, j, i, tolerence);
		  if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);
		  np++;
		}

	    }
	}
    }
  else return NULL;
  if ( oi->im.data_type != IS_FLOAT_IMAGE) finalize_im_extremum(iea);
  my_set_window_title("%d pixels in %d maxima",np,iea->n_ie);
  oi->need_to_refresh |= BITMAP_NEED_REFRESH;
  return iea;
}




/*  take the maximum of intensity for each radius from an image with a different scaling in
 *  x and y, real x = i_image * ax,  y = i_image * ay
 *
 *
 *
 */


float find_distance_from_pixel_exceeding_thresholin_correlation_im(O_i *ois, float xc, float yc, float peaksize, int r_max, float ax,
							     float ay, float thres)
{
    int i, j;
    union pix *ps = NULL;
    int onx, ony, type;
    float z = 0, x, y, r, rmin;
    int imin, imax, jmin, jmax;

    if (ois == NULL)    return -1.0;

    ps = ois->im.pixel;
    ony = ois->im.ny;
    onx = ois->im.nx;
    type = ois->im.data_type;
    rmin = (float)(r_max + 1);

    imin = (int)(yc - (r_max / ay));
    imax = (int)(0.5 + yc + (r_max / ay));
    jmin = (int)(xc -  r_max / ax);
    jmax = (int)(0.5 + xc + r_max / ax);
    imin = (imin < 0) ? 0 : imin;
    imax = (imax > ony) ? ony : imax;
    jmin = (jmin < 0) ? 0 : jmin;
    jmax = (jmax > onx) ? onx : jmax;
    if (type == IS_FLOAT_IMAGE)
    {
        for (i = imin; i < imax; i++)
        {
            y = ay * ((float)i - yc);
            y *= y;
            for (j = jmin; j < jmax; j++)
            {
                z = ps[i].fl[j];
		if (z < thres) continue;
                x = ax * ((float)j - xc);
                x *= x;
                r = (float)sqrt(x + y);
		if (r <= peaksize) continue;
		rmin = (r < rmin) ? r : rmin;
            }
        }
    }
    return rmin;
}





im_ext_ar *do_auto_bead_find_from_sym_profile(O_i *ois, d_s *symprofile, bd_sc *bdsc, imreg *imr)
{
    O_i  *oism, *oismf, *oismfb, *oiref, *oiref_f, *oid, *apo, *oiaponorm;
    int i, j, x = 0, y = 0, nf, im;
    int border_x = 0, border_y = 0;
    static int size = 256, x0 = 0, y0 = 0, w0 = 512, w1 = 128;
    static float lp = 4, lw = 2, hp = -1, hw = -1, dx = 1;
    static unsigned long t_c, to = 0;
    im_ext_ar *iea = NULL;

    if (ois == NULL || symprofile == NULL || bdsc == NULL)      win_printf_ptr("No image found");

    size = bdsc->sim_nx;
    lp = (int)(bdsc->lpr*size + 0.5);
    lw = (int)(bdsc->lwr*size + 0.5);
    hp = (int)(bdsc->hpr*size + 0.5);
    hw = (int)(bdsc->hwr*size + 0.5);

    if (bdsc->useROI == 0)
    {
        x0 = y0 = 0;
        w0 = ois->im.nx;
        w1 = ois->im.ny;
    }
    else
    {
        x0 = ois->im.nxs;
        y0 = ois->im.nys;
        w0 = ois->im.nxe - ois->im.nxs;
        w1 = ois->im.nye - ois->im.nys;
    }

    //nf = compute_nb_of_min_subset_ROI(ois, size, size, x0, y0, w0, w1);
    nf = compute_nb_of_min_subset_and_border_ROI(ois, size, size, x0, y0, w0, w1, &border_x, &border_y);

    apo = create_appodisation_variable_size_of_float_image(size, size, border_x, border_y, 0.0, 0);
    if (apo == NULL)	return (im_ext_ar *) win_printf_ptr("Could not create apodisation image!");

    oism = create_one_image(size, size, ois->im.data_type);
    if (oism == NULL)      win_printf_ptr("Can't create dest image");
    oismf = create_one_image(size, size/2, IS_COMPLEX_IMAGE);
    if (oismf == NULL)      win_printf_ptr("Can't create dest image");

    oismfb = create_one_image(size, size, IS_FLOAT_IMAGE);
    if (oismfb == NULL)      win_printf_ptr("Can't create dest image");

    oiref = create_one_image(size, size, IS_FLOAT_IMAGE);
    if (oiref == NULL)      win_printf_ptr("Can't create dest image");

    oiref_f = create_one_image(size, size/2, IS_COMPLEX_IMAGE);
    if (oiref_f == NULL)      win_printf_ptr("Can't create dest image");

    set_oi_periodic_in_x_and_y(oismfb);
    oid = create_one_image(w0, w1, IS_FLOAT_IMAGE);
    oiaponorm  = create_one_image(w0, w1, IS_FLOAT_IMAGE);

    t_c = my_uclock();
    set_all_pixel_to_color(oiref, (int)symprofile->yd[symprofile->nx-1], (double)symprofile->yd[symprofile->nx-1]);
    add_spot_of_profile_to_image(oiref, size/2, size/2, dx, symprofile);
    //forward_fftbt_2d_im_appo(oiref_f, oiref, 0, screen, apo, 0);
    set_oi_periodic_in_x_and_y(oiref);
    forward_fftbt_2d_im(oiref_f, oiref, 0, screen, 0);
    band_pass_filter_fft_2d_im(oiref_f, oiref_f,  0, lp,  lw,  hp,  hw, 0, 0);
    for (im = 0; im < nf; im++)
    {
        find_min_subset_pos_ROI(ois, size, size, im, &x, &y, x0, y0, w0, w1);
        copy_one_subset_of_image(oism, ois, x, y);
        forward_fftbt_2d_im_appo(oismf, oism, 0, screen, apo, 0);
        band_pass_filter_fft_2d_im(oismf, oismf,  0, lp,  lw,  hp,  hw, 0, 0);
        corel_2d_im_norm(oismf, oismf, oiref_f, COREL_NORM_S2);
        set_oi_periodic_in_x_and_y(oismf);
        backward_fftbt_2d_im(oismfb, oismf, 0, screen, 0.05);
        shift_2d_im(oismfb, oismfb);
        find_min_subset_pos_ROI(ois, size, size, im, &x, &y, 0, 0, w0, w1);
        for (i = 0; i < size; i++)
        {
            for (j = 0; j < size; j++)
            {
                oid->im.pixel[y+i].fl[x+j] += oismfb->im.pixel[i].fl[j];
                oiaponorm->im.pixel[y+i].fl[x+j] += apo->im.pixel[i].fl[j];
            }
        }
    }
    for (i = border_y; (i < oid->im.ny - border_y) && (i < oiaponorm->im.ny - border_y); i++)
    {
        for (j = border_x; (j < oid->im.nx - border_x) && (j < oiaponorm->im.nx - border_x); j++)
            if (oiaponorm->im.pixel[i].fl[j] > 0) oid->im.pixel[i].fl[j] /= oiaponorm->im.pixel[i].fl[j];
    }
    t_c = my_uclock() - t_c;
    iea = find_maxima_position_local(oid, bdsc->thres, bdsc->tolerence, bdsc->nmax, bdsc->useROI);
    for (i = 0;iea != NULL && i < (int) iea->n_ie; i++) 	  iea->ie[i].inb = -1;
    for (i = 0;iea != NULL && i < (int) iea->n_ie; i++)
    {
      iea->ie[i].dnb = find_distance_from_pixel_exceeding_thresholin_correlation_im(oid, iea->ie[i].xpos, iea->ie[i].ypos,
										    bdsc->peaksize, fabs(bdsc->mindist), 1.0, 1.0, bdsc->thres);
      iea->ie[i].inb = 0;
      //win_printf("Ext %d min r %g",i,iea->ie[i].dnb);
      /*
       for (j = 0; j < (int) iea->n_ie; j++)
        {
            if (j == i) continue;
            tmp = iea->ie[j].xpos - iea->ie[i].xpos;
            d = tmp * tmp;
            tmp = iea->ie[j].ypos - iea->ie[i].ypos;
            d += tmp * tmp;
	    d = sqrt(d);
            if (iea->ie[j].inb == -1)
            {
                iea->ie[j].dnb = d;
                iea->ie[j].inb = i;
            }
            else if (d - iea->ie[j].dnb < 0)
            {
                iea->ie[j].dnb = d;
                iea->ie[j].inb = i;
            }
            if (d - iea->ie[i].dnb < 0)
            {
                iea->ie[i].dnb = d;
                iea->ie[i].inb = j;
            }
        }
      */
    }
    if (imr != NULL)
      {
	find_zmin_zmax(apo);
	add_image(imr, apo->im.data_type, (void*)apo);
	find_zmin_zmax(oism);
	add_image(imr, oism->im.data_type, (void*)oism);
	find_zmin_zmax(oismf);
	add_image(imr, oismf->im.data_type, (void*)oismf);
	find_zmin_zmax(oismfb);
	add_image(imr, oismfb->im.data_type, (void*)oismfb);
	find_zmin_zmax(oiref);
	add_image(imr, oiref->im.data_type, (void*)oiref);
	find_zmin_zmax(oiref_f);
	add_image(imr, oiref_f->im.data_type, (void*)oiref_f);
	find_zmin_zmax(oid);
	add_image(imr, oid->im.data_type, (void*)oid);
	map_pixel_ratio_of_image_and_screen(oid, 1.0, 1.0);
      }
    else
      {
	free_one_image(apo);
	free_one_image(oism);
	free_one_image(oiref);
	free_one_image(oismf);
	free_one_image(oiref_f);
	free_one_image(oid);
	free_one_image(oismfb);
      }
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    return iea;
}

int convert_bead_search_info_in_plot(im_ext_ar *iea, O_i *ois, bd_sc *bdsc)
{
    int i, j;
    d_s *ds = NULL;
    O_p *op = NULL;

    if (iea == NULL || iea->n_ie < 1) return 1;
    if (bdsc->mindist <= 0)
      op = create_and_attach_op_to_oi(ois, 2*iea->n_ie, 2*iea->n_ie, 0, IM_SAME_Y_AXIS|IM_SAME_X_AXIS);
    else op = create_and_attach_op_to_oi(ois, iea->n_ie, iea->n_ie, 0, IM_SAME_Y_AXIS|IM_SAME_X_AXIS);
    if (op == NULL) return D_O_K;
    ds = op->dat[0];
    set_ds_dot_line(ds);
    set_ds_dash_line(ds);
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "multi max found with threshold %g tolerence %d",bdsc->thres,bdsc->tolerence);
    if (bdsc->mindist <= 0)
      {
	for (i = j = 0; i < (int)iea->n_ie && j < ds->nx; i++)
	  {
	    ds->yd[j] = iea->ie[i].ypos;
	    ds->xd[j++] = iea->ie[i].xpos;
	    //k = iea->ie[i].inb;
	    ds->yd[j] = iea->ie[i].ypos;
	    ds->xd[j++] = iea->ie[i].xpos + iea->ie[i].dnb;
	  }
	ds->nx = ds->ny = j;
      }
    else
      {
	for (i = j = 0; i < (int)iea->n_ie; i++)
	 {
	   if (iea->ie[i].dnb > bdsc->mindist)
	     {
	       ds->xd[j] = iea->ie[i].xpos;
	       ds->yd[j++] = iea->ie[i].ypos;
	     }
	 }
	ds->nx = ds->ny = j;
	set_ds_dot_line(ds);
      }
    ois->need_to_refresh = PLOT_NEED_REFRESH;
    return 0;
}

int do_auto_bead_find_s(void)
{
    imreg *imr;
    O_i *ois;
    O_p *op;
    int i;
    static int size = 256, x0 = 0, y0 = 0, w0 = 512, w1 = 128;
    static float lp = 4, lw = 2, hp = -1, hw = -1, dx = 1;
    static int nmax = 256, tolerence = 3, useROI = 0;
    static float thres = 0.5, mindist = 15, peaksize = 5;
    bd_sc bdsc;
    im_ext_ar *iea;


    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

    op = find_oi_cur_op(ois);
    if (op == NULL) 	return win_printf_OK("You need a profile plot");


    i = (op->dat[op->cur_dat]->nx);
    for (size = 32; size < i; size *= 2);
    size *= 2;
    if (hp < 0) hp = size/4;
    if (hw < 0) hw = size/8;


    i = win_scanf("This routine will cut the ROI of an image in smaller ones with covering edge\n"
                  "and compute correlation with the image of a single bead described by its profile\n"
                  "define the size of the small image (a power of 2) %8d\n"
                  "Analyse the entire image %R or just a Region Of Interest %r\n"
                  "In the last case, ROI starts at x0 %6d y0 %6d extends in x %6d and y %6d\n"
                  "Define the Band-pass filter cutoff lowpass  %10flowpass width %10f\n"
                  "cutoff highpass  %10fhighpass width %10f\n"
                  "Define the extend of one spot pixel in image pixel %8f\n"
                  "looking for maximum in image exceeding a threshold\n"
                  "Define threshold %8f the typical peak width %8d\n"
                  "and max number of extremum found %8d\n"
                  "Define minimum distance between beads %10f\n"
		  "Define the half size of beads correlation peak %10f\n"
                  ,&size,&x0,&y0,&w0,&w1,&useROI,&lp,&lw,&hp,&hw,&dx,&thres,&tolerence,&nmax,&mindist,&peaksize);


    if (i == WIN_CANCEL) return 0;

    bdsc.sim_nx = size;
    bdsc.lpr = (float)lp/size;
    bdsc.lwr = (float)lw/size;
    bdsc.hpr = (float)hp/size;
    bdsc.hwr = (float)hw/size;
    bdsc.useROI = useROI;
    bdsc.thres = thres;
    bdsc.tolerence = tolerence;
    bdsc.nmax = nmax;
    bdsc.mindist = (double)mindist;
    bdsc.peaksize = (double)peaksize;
    iea = do_auto_bead_find_from_sym_profile(ois, op->dat[op->cur_dat], &bdsc, imr); // imr = NULL for no image
    convert_bead_search_info_in_plot(iea, ois, &bdsc);
    free_im_ext_array(iea);
    return refresh_image(imr, UNCHANGED);
}


int do_auto_bead_find_sold(void)
{
    imreg *imr;
    O_i *ois, *oism, *oismf, *oismfb, *oiref, *oiref_f, *oid, *apo, *oiaponorm;
    O_p *op;
    d_s *ds;
    int i, j, x = 0, y = 0, nf, im;
    int border_x = 0, border_y = 0;
    static int size = 256, x0 = 0, y0 = 0, w0 = 512, w1 = 128, noROI = 0;
    static float lp = 4, lw = 2, hp = -1, hw = -1, dx = 1;
    static unsigned long t_c, to = 0;
    double d, tmp, mindistd;
    static int nmax = 256, tolerence = 3, useROI = 0;
    static float thres = 0.5, mindist = 15;
    im_ext_ar *iea = NULL;



    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;

    op = find_oi_cur_op(ois);
    if (op == NULL) 	return win_printf_OK("You need a profile plot");


    i = (op->dat[op->cur_dat]->nx);
    for (size = 32; size < i; size *= 2);
    size *= 2;
    if (hp < 0) hp = size/4;
    if (hw < 0) hw = size/8;


    i = win_scanf("This routine will cut the ROI of an image in smaller ones with covering edge\n"
                  "and compute correlation with the image of a single bead described by its profile\n"
                  "define the size of the small image (a power of 2) %8d\n"
                  "Analyse the entire image %R or just a Region Of Interest %r\n"
                  "In the last case, ROI starts at x0 %6d y0 %6d extends in x %6d and y %6d\n"
                  "Define the Band-pass filter cutoff lowpass  %10flowpass width %10f\n"
                  "cutoff highpass  %10fhighpass width %10f\n"
                  "Define the extend of one spot pixel in image pixel %8f\n"
                  "looking for maximum in image exceeding a threshold\n"
                  "Define threshold %8f the typical peak width %8d\n"
                  "and max number of extremum found %8d\n"
                  "Define minimum distance between beads %10f\n"
                  ,&size,&x0,&y0,&w0,&w1,&noROI,&lp,&lw,&hp,&hw,&dx,&thres,&tolerence,&nmax,&mindist);


    if (i == WIN_CANCEL) return 0;
    if (noROI == 0)
    {
        x0 = y0 = 0;
        w0 = ois->im.nx;
        w1 = ois->im.ny;
    }

    if (ois == NULL)      win_printf_OK("No image found");



    //nf = compute_nb_of_min_subset_ROI(ois, size, size, x0, y0, w0, w1);
    nf = compute_nb_of_min_subset_and_border_ROI(ois, size, size, x0, y0, w0, w1, &border_x, &border_y);

    apo = create_appodisation_variable_size_of_float_image(size, size, border_x, border_y, 0.0, 0);
    //create_appodisation_float_image(size, size, 0.05, 0);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");

    oism = create_one_image(size, size, ois->im.data_type);
    if (oism == NULL)      win_printf_OK("Can't create dest image");
    oismf = create_one_image(size, size/2, IS_COMPLEX_IMAGE);
    if (oismf == NULL)      win_printf_OK("Can't create dest image");

    oismfb = create_one_image(size, size, IS_FLOAT_IMAGE);
    if (oismfb == NULL)      win_printf_OK("Can't create dest image");

    oiref = create_one_image(size, size, IS_FLOAT_IMAGE);
    if (oiref == NULL)      win_printf_OK("Can't create dest image");

    oiref_f = create_one_image(size, size/2, IS_COMPLEX_IMAGE);
    if (oiref_f == NULL)      win_printf_OK("Can't create dest image");

    set_oi_periodic_in_x_and_y(oismfb);
    oid = create_one_image(w0, w1, IS_FLOAT_IMAGE);
    oiaponorm  = create_one_image(w0, w1, IS_FLOAT_IMAGE);

    t_c = my_uclock();
    add_spot_of_profile_to_image(oiref, size/2, size/2, dx, op->dat[0]);
    forward_fftbt_2d_im_appo(oiref_f, oiref, 0, screen, apo, 0);
    for (im = 0; im < nf; im++)
    {
        find_min_subset_pos_ROI(ois, size, size, im, &x, &y, x0, y0, w0, w1);
        copy_one_subset_of_image(oism, ois, x, y);
        forward_fftbt_2d_im_appo(oismf, oism, 0, screen, apo, 0);
        band_pass_filter_fft_2d_im(oismf, oismf,  0, lp,  lw,  hp,  hw, 0, 0);
        corel_2d_im_norm(oismf, oismf, oiref_f, COREL_NORM_S2);
        set_oi_periodic_in_x_and_y(oismf);
        backward_fftbt_2d_im(oismfb, oismf, 0, screen, 0.05);
        shift_2d_im(oismfb, oismfb);
        find_min_subset_pos_ROI(ois, size, size, im, &x, &y, 0, 0, w0, w1);
        for (i = 0; i < size; i++)
        {
            for (j = 0; j < size; j++)
            {
                oid->im.pixel[y+i].fl[x+j] += oismfb->im.pixel[i].fl[j];
                oiaponorm->im.pixel[y+i].fl[x+j] += apo->im.pixel[i].fl[j];
            }
        }
    }
    for (i = border_y; (i < oid->im.ny - border_y) && (i < oiaponorm->im.ny - border_y); i++)
    {
        for (j = border_x; (j < oid->im.nx - border_x) && (j < oiaponorm->im.nx - border_x); j++)
            if (oiaponorm->im.pixel[i].fl[j] > 0) oid->im.pixel[i].fl[j] /= oiaponorm->im.pixel[i].fl[j];
    }
    t_c = my_uclock() - t_c;
    iea = find_maxima_position(oid, thres, tolerence, nmax, useROI);
    if (iea == NULL) return win_printf_OK("error cannot get extremum");
    if (iea->n_ie < 1) return win_printf_OK("0 extremum found");
    op = create_and_attach_op_to_oi(ois, iea->n_ie, iea->n_ie, 0, IM_SAME_Y_AXIS|IM_SAME_X_AXIS);
    if (op == NULL) return D_O_K;
    ds = op->dat[0];
    set_ds_dot_line(ds);
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "multi max found with threshold %g tolerence %d",thres,tolerence);
    for (i = 0; i < (int)iea->n_ie; i++) 	  iea->ie[i].inb = -1;
    for (i = 0; i < (int)iea->n_ie; i++)
    {
      for (j = 0; j < (int)iea->n_ie; j++)
        {
            if (j == i) continue;
            tmp = iea->ie[j].xpos - iea->ie[i].xpos;
            d = tmp * tmp;
            tmp = iea->ie[j].ypos - iea->ie[i].ypos;
            d += tmp * tmp;
            if (iea->ie[j].inb == -1)
            {
                iea->ie[j].dnb = d;
                iea->ie[j].inb = i;
            }
            else if (d - iea->ie[j].dnb < 0)
            {
                iea->ie[j].dnb = d;
                iea->ie[j].inb = i;
            }
            if (d - iea->ie[i].dnb < 0)
            {
                iea->ie[i].dnb = d;
                iea->ie[i].inb = j;
            }
        }
    }
    for (i = j = 0, mindistd = (double)(mindist*mindist); i < (int)iea->n_ie; i++)
    {
        if (iea->ie[i].dnb > mindistd)
        {
            ds->xd[j] = iea->ie[i].xpos;
            ds->yd[j++] = iea->ie[i].ypos;
        }
    }
    ds->nx = ds->ny = j;
    ois->need_to_refresh = PLOT_NEED_REFRESH;



    free_one_image(apo);
    free_one_image(oism);
    free_one_image(oiref);
    free_one_image(oismf);
    free_one_image(oiref_f);
    free_one_image(oid);
    free_one_image(oismfb);
    //find_zmin_zmax(oid);
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    //my_set_window_title
    //  win_printf("FFT took %g ms max corel",(double)(t_c*1000)/to);
    return refresh_image(imr, UNCHANGED);
}





// large overlap
int do_cut_ROI_of_image_in_small_parts_bp(void)
{
    imreg *imr;
    O_i *ois, *oim, *oif, *oid, *apo;
    int i, j, x = 0, y = 0, nf, im, ims;
    int nfi;
    static int size = 256, x0 = 0, y0 = 0, w0 = 512, w1 = 128, noROI = 0;
    static float lp = 3, lw = 1, hp = 6, hw = 2;
    static unsigned long t_c, to = 0;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("This routine will cut the ROI of an image in smaller ones\n"
                  "with covering edge for correlation\n"
                  "define the size of the small image %8d\n"
                  "ROI start x0 %6d y0 %6d w0 %6d w1 %6d\n"
                  "Set ROI to entire image size %b\n"
                  "Band-pass filter cutoff lowpass  %10flowpass width %10f\n"
                  "cutoff highpass  %10fhighpass width %10f"
                  ,&size,&x0,&y0,&w0,&w1,&noROI,&lp,&lw,&hp,&hw);
    if (i == WIN_CANCEL) return 0;

    if (noROI)
    {
        x0 = y0 = 0;
        w0 = ois->im.nx;
        w1 = ois->im.ny;
    }


    if (ois == NULL)      win_printf_OK("No image found");

    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;

    apo = create_appodisation_float_image(size, size, 0.05, 0);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");


    nf = compute_nb_of_subset_ROI(ois, size, size, x0, y0, w0, w1);
    oim = create_and_attach_movie_to_imr(imr, size, size, ois->im.data_type, nf);
    oif = create_and_attach_movie_to_imr(imr, size, size/2, IS_COMPLEX_IMAGE, nf);

    oid = create_and_attach_oi_to_imr(imr, nf, nfi, IS_FLOAT_IMAGE);

    t_c = my_uclock();
    for (ims = 0; ims < nfi; ims++)
    {
        switch_frame(ois,ims);
        my_set_window_title("Treating image %d",ims);
        for (j = 0; j < nf; j++)
        {
            switch_frame(oim,j);
            switch_frame(oif,j);
            find_subset_pos_ROI(ois, size, size, j, &x, &y, x0, y0, w0, w1);
            copy_one_subset_of_image(oim, ois, x, y);
        }
        forward_fftbt_2d_im_appo(oif, oim, 0, screen, apo, 0);
        band_pass_filter_fft_2d_im(oif, oif,  0, lp,  lw,  hp,  hw, 0, 0);
        for (im = 0; im < nf; im++)
        {
            switch_frame(oif, im);
            oid->im.pixel[ims].fl[im] = oif->im.user_fspare[im][0];
        }
    }
    free_one_image(apo);
    t_c = my_uclock() - t_c;
    oim->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oim);
    oif->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oif);
    oif->height *= 0.5;
    find_zmin_zmax(oid);
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    //my_set_window_title
    win_printf("FFT took %g ms",(double)(t_c*1000)/to);
    return refresh_image(imr, imr->n_oi - 1);
}

//if apo == NULL do not use apo
O_i *build_ortho_poly2x2(int nx, int ny, O_i *apo)
{
    O_i *oip = NULL;
    int i, j;
    float tmp, tmp2, ap;
    double norm, mean;
    union pix *ps = NULL, *po = NULL;

    po = (apo != NULL) ? apo->im.pixel : NULL;
    oip = create_one_movie(nx, ny, IS_FLOAT_IMAGE, 9);
    if (oip == NULL) return NULL;
    switch_frame(oip, 0);
    tmp = (float)1/sqrt(nx*ny);

    for (j = 0, ps = oip->im.pixel, norm = 0; j < nx; j++)
      {
	for (i = 0; i < ny; i++)
	  {
	    ap = (po != NULL) ? po[i].fl[j] : 1;
	    ps[i].fl[j] = tmp;
	    norm += ap*tmp*tmp;
	  }
      }
    norm = (float)1/sqrt(norm);
    for (j = 0, ps = oip->im.pixel; j < nx; j++)
      {
	for (i = 0; i < ny; i++)
	  ps[i].fl[j] *= norm;
      }
    switch_frame(oip, 1);
    for (j = 0, ps = oip->im.pixel, norm = 0; j < nx; j++)
      {
	tmp = j - (float)(nx-1)/2;
	for (i = 0; i < ny; i++)
	  {
	    ap = (po != NULL) ? po[i].fl[j] : 1;
	    ps[i].fl[j] = tmp;
	    norm += ap*tmp*tmp;
	  }
      }
    norm = (float)1/sqrt(norm);
    for (j = 0, ps = oip->im.pixel; j < nx; j++)
      {
	for (i = 0; i < ny; i++)
	  ps[i].fl[j] *= norm;
      }
    switch_frame(oip, 2);
    for (i = 0, ps = oip->im.pixel, norm = 0; i < ny; i++)
      {
	tmp = i - (float)(ny-1)/2;
	for (j = 0; j < nx; j++)
	  {
	    ap = (po != NULL) ? po[i].fl[j] : 1;
	    ps[i].fl[j] = tmp;
	    norm += ap*tmp*tmp;
	  }
      }
    norm = (float)1/sqrt(norm);
    for (j = 0, ps = oip->im.pixel; j < nx; j++)
      {
	for (i = 0; i < ny; i++)
	  ps[i].fl[j] *= norm;
      }
    switch_frame(oip, 3);
    for (i = 0, ps = oip->im.pixel, norm = 0; i < ny; i++)
      {
	tmp = i - (float)(ny-1)/2;
	for (j = 0; j < nx; j++)
	  {
	    tmp2 = j - (float)(nx-1)/2;
	    ap = (po != NULL) ? po[i].fl[j] : 1;
	    ps[i].fl[j] = tmp*tmp2;
	    norm += ap*tmp*tmp*tmp2*tmp2;
	  }
      }
    norm = (float)1/sqrt(norm);
    for (j = 0, ps = oip->im.pixel; j < nx; j++)
      {
	for (i = 0; i < ny; i++)
	  ps[i].fl[j] *= norm;
      }
    switch_frame(oip, 4);
    for (i = 0, mean = 0; i < ny; i++)
      {
	tmp = i - (float)(ny-1)/2;
	tmp *= tmp;
	mean += tmp;
      }
    mean /= ny;
    for (i = 0, ps = oip->im.pixel, norm = 0; i < ny; i++)
      {
	tmp = i - (float)(ny-1)/2;
	tmp *= tmp;
	tmp -= mean;
	for (j = 0; j < nx; j++)
	  {
	    ap = (po != NULL) ? po[i].fl[j] : 1;
	    ps[i].fl[j] = tmp;
	    norm += ap*tmp*tmp;
	  }
      }
    norm = (float)1/sqrt(norm);
    for (j = 0, ps = oip->im.pixel; j < nx; j++)
      {
	for (i = 0; i < ny; i++)
	  ps[i].fl[j] *= norm;
      }
    switch_frame(oip, 5);
    for (i = 0, mean = 0; i < ny; i++)
      {
	tmp = i - (float)(ny-1)/2;
	tmp *= tmp;
	mean += tmp;
      }
    mean /= ny;
    for (i = 0, ps = oip->im.pixel, norm = 0; i < ny; i++)
      {
	tmp = i - (float)(ny-1)/2;
	tmp *= tmp;
	tmp -= mean;
	for (j = 0; j < nx; j++)
	  {
	    tmp2 = j - (float)(nx-1)/2;
	    ap = (po != NULL) ? po[i].fl[j] : 1;
	    ps[i].fl[j] = tmp*tmp2;
	    norm += ap*tmp*tmp*tmp2*tmp2;
	  }
      }
    norm = (float)1/sqrt(norm);
    for (j = 0, ps = oip->im.pixel; j < nx; j++)
      {
	for (i = 0; i < ny; i++)
	  ps[i].fl[j] *= norm;
      }

    switch_frame(oip, 6);
    for (j = 0, mean = 0; j < nx; j++)
      {
	tmp = j - (float)(nx-1)/2;
	tmp *= tmp;
	mean += tmp;
      }
    mean /= nx;
    for (i = 0, ps = oip->im.pixel, norm = 0; i < ny; i++)
      {
	for (j = 0; j < nx; j++)
	  {
	    tmp = j - (float)(nx-1)/2;
	    tmp *= tmp;
	    tmp -= mean;
	    ap = (po != NULL) ? po[i].fl[j] : 1;
	    ps[i].fl[j] = tmp;
	    norm += ap*tmp*tmp;
	  }
      }
    norm = (float)1/sqrt(norm);
    for (j = 0, ps = oip->im.pixel; j < nx; j++)
      {
	for (i = 0; i < ny; i++)
	  ps[i].fl[j] *= norm;
      }
    switch_frame(oip, 7);
    for (j = 0, mean = 0; j < nx; j++)
      {
	tmp = j - (float)(nx-1)/2;
	tmp *= tmp;
	mean += tmp;
      }
    mean /= ny;
    for (i = 0, ps = oip->im.pixel, norm = 0; i < ny; i++)
      {
	tmp2 = i - (float)(ny-1)/2;
	for (j = 0; j < nx; j++)
	  {
	    tmp = j - (float)(nx-1)/2;
	    tmp *= tmp;
	    tmp -= mean;
	    ap = (po != NULL) ? po[i].fl[j] : 1;
	    ps[i].fl[j] = tmp*tmp2;
	    norm += ap*tmp*tmp*tmp2*tmp2;
	  }
      }
    norm = (float)1/sqrt(norm);
    for (j = 0, ps = oip->im.pixel; j < nx; j++)
      {
	for (i = 0; i < ny; i++)
	  ps[i].fl[j] *= norm;
      }
    switch_frame(oip, 8);
    for (i = 0, ps = oip->im.pixel, norm = 0; i < ny; i++)
      {
	tmp2 = i - (float)(ny-1)/2;
	tmp2 *= -tmp2;
	for (j = 0; j < nx; j++)
	  {
	    tmp = j - (float)(nx-1)/2;
	    tmp *= tmp;
	    ap = (po != NULL) ? po[i].fl[j] : 1;
	    ps[i].fl[j] = tmp*tmp2;
	    norm += ap*tmp*tmp*tmp2*tmp2;
	  }
      }
    norm = (float)1/sqrt(norm);
    for (j = 0, ps = oip->im.pixel; j < nx; j++)
      {
	for (i = 0; i < ny; i++)
	  ps[i].fl[j] *= norm;
      }
    return oip;
}




O_i *project_on_first_poly(O_i *dest, O_i *ois, O_i *oip, int substract, O_i *apo)
{
  float a[9] = {0}, ap;
  int i, j, im, onx, ony, im2, nf;
  O_i *oid = NULL;
  union pix *ps = NULL, *pp = NULL, *pd = NULL, *po = NULL;

  po = (apo != NULL) ? apo->im.pixel : NULL;
  if (ois == NULL || oip == NULL)
    {
      win_printf("Ois %p, oip %p !",ois,oip);
      return NULL;
    }
  if (ois->im.nx != oip->im.nx || ois->im.ny != oip->im.ny) return NULL;
  onx = ois->im.nx; ony = ois->im.ny;
  nf = nb_of_frames_in_movie(ois);
  if (dest != NULL)
    {
      if (dest->im.nx != ois->im.nx || dest->im.ny != ois->im.ny
	  || nb_of_frames_in_movie(dest) != nf
	  || dest->im.data_type != IS_FLOAT_IMAGE)
	{
	  free_one_image(dest);
	  dest = NULL;
	}
      else oid = dest;
    }
  if (dest == NULL)
    {
      if (nf == 1)    oid = create_one_image(onx, ony, IS_FLOAT_IMAGE);
      else oid = create_one_movie(onx, ony, IS_FLOAT_IMAGE, nf);
      if (oid == NULL) return NULL;
      //win_printf("image created in project %d %d %d",onx,ony,nf);
    }
  for (im2 = 0; im2 < nf; im2++)
    {
      switch_frame(ois, im2);
      switch_frame(oid, im2);
      if (substract)
	{
	  ps = ois->im.pixel;
	  pd = oid->im.pixel;
	  if (ois->im.data_type == IS_CHAR_IMAGE)
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    pd[i].fl[j] = ps[i].ch[j];
		}
	    }
	  else if (ois->im.data_type == IS_INT_IMAGE)
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    pd[i].fl[j] = ps[i].in[j];
	    }
	    }
	  else if (ois->im.data_type == IS_UINT_IMAGE)
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    pd[i].fl[j] = ps[i].ui[j];
		}
	    }
	  else if (ois->im.data_type == IS_LINT_IMAGE)
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    pd[i].fl[j] = ps[i].li[j];
		}
	    }
	  else if (ois->im.data_type == IS_FLOAT_IMAGE)
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    pd[i].fl[j] = ps[i].fl[j];
		}
	    }
	  else if (ois->im.data_type == IS_DOUBLE_IMAGE)
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    pd[i].fl[j] = ps[i].db[j];
		}
	    }
	  else
	    {
	      free_one_image(oid);
	      return NULL;
	    }
	}
      for (im = 0; im < 8 && im < oip->im.n_f; im++)
	{
	  switch_frame(oip, im);
	  a[im] = 0;
	  ps = ois->im.pixel;
	  pp = oip->im.pixel;
	  pd = oid->im.pixel;
	  if (ois->im.data_type == IS_CHAR_IMAGE)
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    {
		      ap = (po != NULL) ? po[i].fl[j] : 1;
		      a[im] += ap * pp[i].fl[j] * ps[i].ch[j];
		    }
		}
	    }
	  else if (ois->im.data_type == IS_INT_IMAGE)
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    {
		      ap = (po != NULL) ? po[i].fl[j] : 1;
		      a[im] += ap * pp[i].fl[j] * ps[i].in[j];
		    }
		}
	    }
	  else if (ois->im.data_type == IS_UINT_IMAGE)
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    {
		      ap = (po != NULL) ? po[i].fl[j] : 1;
		      a[im] += ap * pp[i].fl[j] * ps[i].ui[j];
		    }
		}
	    }
	  else if (ois->im.data_type == IS_LINT_IMAGE)
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    {
		      ap = (po != NULL) ? po[i].fl[j] : 1;
		      a[im] += ap * pp[i].fl[j] * ps[i].li[j];
		    }
		}
	    }
	  else if (ois->im.data_type == IS_FLOAT_IMAGE)
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    {
		      ap = (po != NULL) ? po[i].fl[j] : 1;
		      a[im] += ap * pp[i].fl[j] * ps[i].fl[j];
		    }
		}
	    }
	  else if (ois->im.data_type == IS_DOUBLE_IMAGE)
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    {
		      ap = (po != NULL) ? po[i].fl[j] : 1;
		      a[im] += ap * pp[i].fl[j] * ps[i].db[j];
		    }
		}
	    }
	  else
	    {
	      free_one_image(oid);
	      return NULL;
	    }
	  if (substract)
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    pd[i].fl[j] -= a[im] * pp[i].fl[j];
		}
	    }
	  else
	    {
	      for (i = 0; i< ony ; i++)
		{
		  for (j = 0; j < onx ; j++)
		    pd[i].fl[j] += a[im] * pp[i].fl[j];
		}
	    }
	  for (j = 0; j < ois->im.user_nipar && j < oid->im.user_nipar; j++)
            oid->im.user_ispare[im2][j] = ois->im.user_ispare[im2][j];
	  for (j = 0; j < ois->im.user_nipar && j < oid->im.user_nipar; j++)
            oid->im.user_fspare[im2][j] = ois->im.user_fspare[im2][j];

	}
    }
  return oid;
}

int do_project(void)
{
    imreg *imr = NULL;
    O_i *ois = NULL, *oip = NULL, *oid = NULL, *apo = NULL;
    int i;
    static unsigned long t_c, to = 0;
    static int sub = 1, keep_op = 0, useapo = 0, rmdc = 1;
    static float sm = 0.0;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    if (ois == NULL)      return win_printf_OK("No image found");

    i = win_scanf("%R->projection or %r->substract projection\n"
		  "%b keep polynome\n"
		  "%b use apodisation\n"
		  "If yes %bremove DC specify small value %5f\n"
		  ,&sub,&keep_op,&useapo,&rmdc,&sm);
    if (i == WIN_CANCEL) return 0;
    if (useapo) apo = create_appodisation_float_image(ois->im.nx, ois->im.ny, sm, 0);
    oip = build_ortho_poly2x2(ois->im.nx, ois->im.ny, apo);
    if (oip == NULL) return win_printf_OK("cannot build polynomial");
    t_c = my_uclock();
    oid = project_on_first_poly(NULL, ois, oip, sub, apo);
    t_c = my_uclock() - t_c;
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    add_image(imr, oid->im.data_type, (void*)oid);
    if (keep_op)
      {
	find_zmin_zmax(oip);
	oip->need_to_refresh = ALL_NEED_REFRESH;
	add_image(imr, oip->im.data_type, (void*)oip);
      }
    else free_one_image(oip);


    to = get_my_uclocks_per_sec();
    if (apo) free_one_image(apo);
    win_printf("Process took %g S",((double)t_c)/to);
    return refresh_image(imr, imr->n_oi - 1);
}








int do_flatten_image_by_cutting_in_small_parts(void)
{
    imreg *imr = NULL;
    O_i *ois = NULL, *oim = NULL, *oip = NULL, *oidp = NULL, *apo = NULL, *oid = NULL, *oidf = NULL;
    int i, j, x = 0, y = 0, nf, onx, ony, negx, negy;
    static int size = 256;
    static unsigned long t_c, to = 0;
    static int keep_dc = 0, sub = 1, polyapo = 0;
    static float sm = 0.05;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("This routine will cut an image in smaller ones\n"
                  "with 1/2 covering edge for correlation\n"
                  "define the size of the small image %8d\n"
		  "%R->projection or %r->substract projection\n"
		  "%bApply appodisation in polynome projection\n"
		  "For appodisation: %bkeep DC component\n"
		  "Decreasea appodisation to %6f \n"
		  ,&size,&sub,&polyapo,&keep_dc,&sm);
    if (i == WIN_CANCEL) return 0;

    if (ois == NULL)      win_printf_OK("No image found");
    nf = compute_nb_of_subset(ois, size, size, NULL, NULL);
    oim = create_and_attach_movie_to_imr(imr, size, size, ois->im.data_type, nf);
    t_c = my_uclock();
    for (j = 0; j < nf; j++)
    {
        switch_frame(oim,j);
        find_subset_pos(ois, size, size, j, &x, &y);
        copy_one_subset_of_image_and_border(oim, ois, x, y);
        //win_printf_OK("small image %d\nx %d y %d\n",j,x,y);
    }
    apo = create_appodisation_float_image(size, size, sm, 0);
    if (apo == NULL)	return win_printf_OK("Could not create apodisation image!");

    oip = build_ortho_poly2x2(size, size, (polyapo)? apo : NULL);
    if (oip == NULL) return win_printf_OK("cannot build polynomial");
    oidp = project_on_first_poly(NULL, oim, oip, sub, (polyapo)? apo : NULL);


    oid = d2_im_appo(NULL, oidp, 1, screen, apo, 1, keep_dc);


    for (i = 0, onx = ony = negx = negy = 0; i < nf; i++)
      {
	switch_frame(oid,i);
	onx = (oid->im.user_ispare[i][0] > onx) ? oid->im.user_ispare[i][0] : onx;
	negx = (oid->im.user_ispare[i][0] < negx) ? oid->im.user_ispare[i][0] : negx;
	ony = (oid->im.user_ispare[i][1] > ony) ? oid->im.user_ispare[i][1] : ony;
	negy = (oid->im.user_ispare[i][1] < negy) ? oid->im.user_ispare[i][1] : negy;
      }
    onx -= negx;
    ony -= negy;
    onx += oid->im.nx;
    ony += oid->im.ny;

    oidf = create_and_attach_oi_to_imr(imr, onx, ony, oid->im.data_type);
    for (i = 0; i < oid->im.n_f; i++)
    {
        switch_frame(oid,i);
	copy_or_add_one_subset_of_image_in_a_bigger_one(oidf, oid->im.user_ispare[i][0]-negx,
							oid->im.user_ispare[i][1]-negy, oid,
							0, 0, oid->im.nx, oid->im.ny,1);

    }
    t_c = my_uclock() - t_c;
    oim->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oim);
    if (to == 0) to = MY_UCLOCKS_PER_SEC;
    win_printf("Flatten took %g ms",(double)(t_c*1000)/to);
    free_one_image(oip);
    free_one_image(oid);
    free_one_image(oidp);
    free_one_image(apo);
    return refresh_image(imr, imr->n_oi - 1);
}






int do_show_ispare(void)
{
    imreg *imr = NULL;
    O_i *ois = NULL;
    int i = 0, id[6];


    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    if (ois == NULL)      return win_printf_OK("No image found");
    for (i = 0; i < 6; i++)
      id[i] = (i+ ois->im.c_f)%ois->im.n_f;
    win_printf("for images:\n"
	       "%d ispare 0 %d 1 %d\n"
	       "%d ispare 0 %d 1 %d\n"
	       "%d ispare 0 %d 1 %d\n"
	       "%d ispare 0 %d 1 %d\n"
	       "%d ispare 0 %d 1 %d\n"
	       "%d ispare 0 %d 1 %d\n"
	       ,id[0], ois->im.user_ispare[id[0]][0],ois->im.user_ispare[id[0]][1]
	       ,id[1], ois->im.user_ispare[id[1]][0],ois->im.user_ispare[id[1]][1]
	       ,id[2], ois->im.user_ispare[id[2]][0],ois->im.user_ispare[id[2]][1]
	       ,id[3], ois->im.user_ispare[id[3]][0],ois->im.user_ispare[id[3]][1]
	       ,id[4], ois->im.user_ispare[id[4]][0],ois->im.user_ispare[id[4]][1]
	       ,id[5], ois->im.user_ispare[id[5]][0],ois->im.user_ispare[id[5]][1]);
    return 0;
}



int do_find_max(void)
{
    imreg *imr;
    O_i *ois;
    O_p *op;
    d_s *ds;
    int i, j;
    double d, tmp, mindistd;
    static int nmax = 256, tolerence = 3, useROI = 0;
    static float thres = 0.5, mindist = 15;
    im_ext_ar *iea = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    i = win_scanf("looking for maximum in image exceeding a threshold\n"
                  "Define threshold %8f the typical peak width %8d\n"
                  "and max number of extremum found %8d\n"
                  "Scan all image %R or the define ROI %r\n"
                  "Define minimum distance between beads %10f\n"
                  ,&thres,&tolerence,&nmax,&useROI,&mindist);
    if (i == WIN_CANCEL) 	return D_O_K;
    iea = find_maxima_position(ois, thres, tolerence, nmax, useROI);
    if (iea == NULL) return win_printf_OK("error cannot get extremum");
    if (iea->n_ie < 1) return win_printf_OK("0 extremum found");
    op = create_and_attach_op_to_oi(ois, iea->n_ie, iea->n_ie, 0, IM_SAME_Y_AXIS|IM_SAME_X_AXIS);
    if (op == NULL) return D_O_K;
    ds = op->dat[0];
    set_ds_dot_line(ds);
    set_ds_point_symbol(ds, "\\oc");
    set_ds_source(ds, "multi max found with threshold %g tolerence %d",thres,tolerence);
    for (i = 0; i < (int)iea->n_ie; i++) 	  iea->ie[i].inb = -1;
    for (i = 0; i < (int)iea->n_ie; i++)
    {
      for (j = 0; j < (int)iea->n_ie; j++)
        {
            if (j == i) continue;
            tmp = iea->ie[j].xpos - iea->ie[i].xpos;
            d = tmp * tmp;
            tmp = iea->ie[j].ypos - iea->ie[i].ypos;
            d += tmp * tmp;
            if (iea->ie[j].inb == -1)
            {
                iea->ie[j].dnb = d;
                iea->ie[j].inb = i;
            }
            else if (d - iea->ie[j].dnb < 0)
            {
                iea->ie[j].dnb = d;
                iea->ie[j].inb = i;
            }
            if (d - iea->ie[i].dnb < 0)
            {
                iea->ie[i].dnb = d;
                iea->ie[i].inb = j;
            }
        }
    }
    for (i = j = 0, mindistd = (double)(mindist*mindist); i < (int)iea->n_ie; i++)
    {
        if (iea->ie[i].dnb > mindistd)
        {
            ds->xd[j] = iea->ie[i].xpos;
            ds->yd[j++] = iea->ie[i].ypos;
        }
    }
    ds->nx = ds->ny = j;
    ois->need_to_refresh = PLOT_NEED_REFRESH;
    return refresh_image(imr, UNCHANGED);
}

//int

MENU *fft2d_image_menu(void)
{
    static MENU mn[32], mn1[32];

    if (mn[0].text != NULL)	return mn;


    add_item_to_menu(mn1,"create appodisation image", do_create_appodisation_float_image,NULL,0,NULL);
    add_item_to_menu(mn1,"Direct FFT2D with apodisation", do_sloppy_fft2d_apo,NULL,0,NULL);
    add_item_to_menu(mn1,"Inverse FFT2D with apodisation", do_sloppy_fft2d_inv_apo,NULL,0,NULL);
    add_item_to_menu(mn1,"Inverse FFT2D with desapodisation", do_sloppy_fft2d_inv_desapo,NULL,0,NULL);
    add_item_to_menu(mn1,"cut image ROI in small ones band-pass",do_cut_ROI_of_image_in_small_parts_bp,NULL,0,NULL);
    add_item_to_menu(mn1,"Image apodisation", do_2d_apo,NULL,0,NULL);
    add_item_to_menu(mn1,"Undersample",do_undersample_image_of_char,NULL,0,NULL);
    add_item_to_menu(mn1,"X-ray filter",do_radial_wavenumber_filter_xray_2d_im,NULL,0,NULL);
    add_item_to_menu(mn1,"Project on 4",do_project,NULL,0,NULL);
    add_item_to_menu(mn1,"Reconstruc image",do_reconstruct_image_from_small_parts,NULL,0,NULL);
    add_item_to_menu(mn1,"Show ispare",do_show_ispare,NULL,0,NULL);
    add_item_to_menu(mn1,"Flaten image",do_flatten_image_by_cutting_in_small_parts,NULL,0,NULL);
    add_item_to_menu(mn1,"Nb of modes bigger than",do_Nb_of_mode_radial_wavenumber,NULL,0,NULL);

    add_item_to_menu(mn1,"Histo small amp",do_histo_small_amp_of_fft_2d_movie,NULL,0,NULL);
    add_item_to_menu(mn1,"Grab noise at high k",do_grab_noise_at_high_k,NULL,0,NULL);
    add_item_to_menu(mn1,"Grab Nb. of modes above noise",do_grab_Nb_modes_above_noise,NULL,0,NULL);





    add_item_to_menu(mn,"Direct FFT2D", do_sloppy_fft2d,NULL,0,NULL);
    add_item_to_menu(mn,"Inverse FFT2D", do_sloppy_fft2d_inv,NULL,0,NULL);
    add_item_to_menu(mn,"band_pass an FFT2D", do_band_pass_filter_fft_2d,NULL,0,NULL);
    add_item_to_menu(mn,"FFT2D and filter", do_sloppy_fft2d_filter,NULL,0,NULL);

    add_item_to_menu(mn,"Correlate by FFT2D", do_sloppy_correlate,NULL,0,NULL);
    add_item_to_menu(mn,"Correlate movie by FFT2D in time",do_sloppy_correlate_movie_in_time,NULL,0,NULL);


    add_item_to_menu(mn, "\0", 		NULL,  			NULL,       0, NULL  );
    add_item_to_menu(mn,"test functions", NULL, mn1, 0, NULL);
    add_item_to_menu(mn,"Turn by 180 degrees", upside_down_and_right_to_left,NULL,0,NULL);
    add_item_to_menu(mn,"substract 2 images", do_substract_2_im,NULL,0,NULL);
    add_item_to_menu(mn,"Divide 2 fft im", do_divide_2_fft_im,NULL,0,NULL);
    add_item_to_menu(mn,"Multiply 2 fft im", do_multiply_2_fft_im,NULL,0,NULL);
    add_item_to_menu(mn,"Multiply im a factor", do_multiply,NULL,0,NULL);
    add_item_to_menu(mn,"convert to char", do_convert_float_image_to_char,NULL,0,NULL);
    add_item_to_menu(mn,"erase low modes", do_erase_low_mode_of_fft_2d_im,NULL,0,NULL);
    add_item_to_menu(mn,"normalize  modes", do_equalize_module_or_erase_low_mode_of_fft_2d_im,NULL,0,NULL);

    add_item_to_menu(mn,"create blank image", create_blank_image,NULL,0,NULL);
    add_item_to_menu(mn,"do add spot", do_add_spot_of_profile_to_image,NULL,0,NULL);
    add_item_to_menu(mn,"Spot gaussian",create_image_with_gaussian_spot,NULL,0,NULL);


    add_item_to_menu(mn,"Shift by nx/2, ny/2", half_shift_x_y,NULL,0,NULL);
    add_item_to_menu(mn,"Radial wavenumber", radial_wavenumber,NULL,0,NULL);
    add_item_to_menu(mn,"Filter by radial profile", do_filter_fft_2d_im_by_radial_wavenumber,NULL,0,NULL);
    add_item_to_menu(mn,"Angular projection", do_angular_projection_of_image,NULL,0,NULL);
    add_item_to_menu(mn,"FFT from Angular projections", do_reconstruct_image_from_angular_projection,NULL,0,NULL);
    add_item_to_menu(mn,"FFT from Angular projections only", do_reconstruct_image_from_angular_projection_only,NULL,0,NULL);


    add_item_to_menu(mn,"cut image in small ones",do_cut_image_in_small_parts,NULL,0,NULL);
    add_item_to_menu(mn,"cut image ROI in small ones",do_cut_ROI_of_image_in_small_parts,NULL,0,NULL);
    add_item_to_menu(mn,"do PIV",do_PIV,NULL,0,NULL);
    add_item_to_menu(mn,"Autocorrel beads",do_auto_bead_find_s,NULL,0,NULL);
    add_item_to_menu(mn,"find max",do_find_max,NULL,0,NULL);
    return mn;
}

int	fft2d_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
    add_image_treat_menu_item ( "fft2d", NULL, fft2d_image_menu(), 0, NULL);
    return D_O_K;
}
int	fft2d_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
    remove_item_to_menu(image_treat_menu, "fft2d", NULL, NULL);
    return D_O_K;
}

#endif
