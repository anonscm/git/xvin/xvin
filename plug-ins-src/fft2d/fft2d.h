#ifndef _FFT2D_H_
#define _FFT2D_H_

#include "xvin.h"

typedef struct _bead_search
{
  int sim_nx;
  float lpr; 
  float lwr; 
  float hpr; 
  float hwr; 
  int useROI;
  float thres;
  int tolerence;
  int nmax;
  float mindist;
  float peaksize;
} bd_sc;

PXV_FUNC(O_i*, multiply_im, (O_i *dest, O_i *ois, double factor));
PXV_FUNC(O_i*, substract_2_image, (O_i *dest, O_i *ois1, O_i *ois2));
PXV_FUNC(O_i*, upside_down_and_left_to_right, ( O_i *ois));

//  shfit pixels by nx/2 and ny/2:   if dest == NULL, dest will be created, dest may be equal to ois, in this case image is modified
PXV_FUNC(O_i*, shift_2d_im, (O_i *dest, O_i *ois));

PXV_FUNC(O_i*, forward_fft_2d_im, (O_i *dest, O_i *ois, BITMAP *bmp, float smp));
PXV_FUNC(O_i*, forward_fftbt_2d_im_minus_mean, (O_i *dest, O_i *ois, int ask_movie, BITMAP *bmp, float smp));

// compute the forward fft of a real image dest is the result, if dest == NULL then it is created
// -ois is the source image bmp is the BITMAP to print message,	if NULL, no message print during process
// -smp is a small number tipically .05 in the Hanning window if smp = 0 the reverse operation is impossible
// fx low_pass cut_off in x fy lowpass filter in y
PXV_FUNC(O_i*, forward_fft_2d_im_filter, (O_i *dest, O_i *ois, BITMAP *bmp, float smp, int fx, int fy));

PXV_FUNC(O_i*, backward_fft_2d_im, (O_i *dest, O_i *ois, BITMAP *bmp, float smp));

// find the max modulus for modes with k > high_pass_radius
PXV_FUNC(float, find_max_modulus, (O_i *ois1, float high_pass_radius));

PXV_FUNC(float, find_min_modulus, (O_i *ois1));

PXV_FUNC(O_i*, corel_2d_movie_in_time, (O_i *dest, O_i *ois, int ref_im, int normalize));
PXV_FUNC(O_i*, corel_2d_movie_in_time_from_arrays, (O_i *dest, O_i *ois1, int *first_im, int *second_im, int size, int normalize));
// same as above except that these functions handle movies and use bt fft 
PXV_FUNC(O_i*, forward_fftbt_2d_im,(O_i *dest, O_i *ois, int ask_movie, BITMAP *bmp, float smp));
PXV_FUNC(O_i*, backward_fftbt_2d_im,(O_i *dest, O_i *ois, int ask_movie, BITMAP *bmp, float smp));

// same as above but use an image for apodisation
PXV_FUNC(O_i*, create_appodisation_float_image, (int onx, int ony, float smp, int desapo));
PXV_FUNC(O_i*, forward_fftbt_2d_im_appo, (O_i *dest, O_i *ois, int ask_movie, BITMAP *bmp, O_i *apo, int house_keeping));
PXV_FUNC(O_i*, backward_fftbt_2d_im_desappo, (O_i *dest, O_i *ois, int ask_movie, BITMAP *bmp, O_i *apo,  int house_keeping));

// do a band pass in fft space accept movies
PXV_FUNC(O_i*, band_pass_filter_fft_2d_im, (O_i *dest, O_i *ois, int ask_movie, float lp, 
					    float lw, float hp, float hw, int amp_save_in_user_spare, int house_keeping));
PXV_FUNC(O_i*, corel_2d_im, (O_i *dest, O_i *ois1, O_i *ois2));

PXV_FUNC(d_s*, radial_wavenumber_of_fft_2d_im, (O_i *ois));

// filter an image by mulriplying fft by the radial wavenumber profile ois is the source image, dest is modified 
// by the operation, if dest = NULL a new image is created, if dest == ois, ois is modified
PXV_FUNC(O_i*, filter_fft_2d_im_by_radial_wavenumber,(O_i *ois, O_i *dest, d_s *ds));

// compute the projection  profile of an image on a defined angle ois is the source image, create a plot 
// if n_ds is in [0, n_dat[ use the existing ds, else creta a new one
PXV_FUNC(O_p*, image_projection_along_an_angle, (O_i *ois, O_p *opd, int n_ds, float angle));

PXV_FUNC(O_i*, multiply_2_fft_images, (O_i *dest, O_i *ois1, O_i *ois2));
PXV_FUNC(O_i*, divide_2_fft_images,(O_i *dest, O_i *ois1, O_i *ois2, float min_div));
PXV_FUNC(O_i*, divide_2_image, (O_i *dest, O_i *ois1, O_i *ois2, float min_div, float min_divi));
PXV_FUNC(O_i*, convert_float_image_to_char,(O_i *dest, O_i *ois1));

PXV_FUNC(int, erase_low_mode_of_fft_2d_im,(O_i *ois, float ampmin));
PXV_FUNC(int, equalize_module_or_erase_low_mode_of_fft_2d_im, (O_i *ois, float ampmin));

PXV_FUNC(int, do_substract_2_im, (void));
PXV_FUNC(int, do_multiply, (void));
PXV_FUNC(int, do_band_pass_filter_fft_2d, (void));
PXV_FUNC(int, do_sloppy_fft2d, (void));
PXV_FUNC(int, do_sloppy_fft2d_filter, (void));
PXV_FUNC(int, do_sloppy_fft2d_inv, (void));
PXV_FUNC(int, do_sloppy_correlate, (void));
PXV_FUNC(int, upside_down_and_right_to_left, (void));
PXV_FUNC(O_i*, build_ortho_poly2x2, (int nx, int ny, O_i *apo));
PXV_FUNC(int, copy_one_subset_of_image_and_border, (O_i *dest, O_i *ois, int x_start, int y_start));
PXV_FUNC(O_i*, project_on_first_poly, (O_i *dest, O_i *ois, O_i *oip, int substract, O_i *apo));
PXV_FUNC(O_i*, d2_im_appo, (O_i *dest, O_i *ois, int ask_movie, BITMAP *bmp, O_i *apo, int house_keeping, int keep_dc));
PXV_FUNC(d_s*, Nb_modes_above_noise_by_histo_small_amp_of_fft_2d_movie, (O_i *ois, d_s *dsd));
PXV_FUNC(O_i*, undersample_image_of_char_in_y, (O_i *dest, O_i *ois1, int bin));
PXV_FUNC(O_i*, undersample_image_of_char, (O_i *dest, O_i *ois1, int bin));
// these function are used to cut an image in small ones which overlap by 1/2

// Determine the number of small images covering the original one
// sub_h and sub_w are the size of the small image with 1/2 coverage
// in the entire image
PXV_FUNC(int, compute_nb_of_subset, (O_i *ois, int sub_w, int sub_h, int *nx, int *ny));

// Determine the number of small images covering the original one with 1/2 coverage
// just in an ROI
PXV_FUNC(int, compute_nb_of_subset_ROI, (O_i *ois, int sub_w, int sub_h, int x0, int y0, int w0, int w1));

// allow to compute the location of the nth small image
// in the entire image
PXV_FUNC(int, find_subset_pos, (O_i *ois, int sub_w, int sub_h, int n, int *x, int *y));
// just in an ROI
PXV_FUNC(int, find_subset_pos_ROI, (O_i *ois, int sub_w, int sub_h, int n, int *x, int *y, int x0, int y0, int w0, int w1));

// recover good point in small images; (sub_w, sub_h) size of small images, n number of the small image
// *x, *y, *w, *h good points position and width, (w0, w1) size of the original ROI
PXV_FUNC(int, find_subset_good_pixel_pos_ROI,(O_i *ois, int sub_w, int sub_h, int n, int *x, int *y, int *w, int *h, int w0, int w1);)

// copy a small image from the original one
PXV_FUNC(int, copy_one_subset_of_image, (O_i *dest, O_i *ois, int x_start, int y_start));

// these function are used to cut an image in small ones which overlap by 1/4

// Determine the number and position of small images covering the original one with minimum overlap
// sub_h and sub_w are the size of the small image with 1/4 coverage
PXV_FUNC(int, compute_nb_of_min_subset_ROI, (O_i *ois, int sub_w, int sub_h, int x0, int y0, int w0, int w1));

// same as above but compute the overlap between image for appodisation
PXV_FUNC(int, compute_nb_of_min_subset_and_border_ROI, (O_i *ois, int sub_w, int sub_h, int x0, int y0, int w0, int w1, int *border_x, int *border_y));
// allow to compute the location of the nth small image
PXV_FUNC(int, find_min_subset_pos_ROI, (O_i *ois, int sub_w, int sub_h, int n, int *x, int *y, int x0, int y0, int w0, int w1));

// recover good point in small images; (sub_w, sub_h) size of small images, n number of the small image
// *x, *y, *w, *h good points position and width, (w0, w1) size of the original ROI
PXV_FUNC(int, find_min_subset_good_pixel_pos_ROI, (O_i *ois, int sub_w, int sub_h, int n, int *x, int *y, int *w, int *h, int w0, int w1));

PXV_FUNC(im_ext_ar*, do_auto_bead_find_from_sym_profile, (O_i *ois, d_s *symprofile, bd_sc *bdsc, imreg *imr));
PXV_FUNC(int, convert_bead_search_info_in_plot,(im_ext_ar *iea, O_i *ois, bd_sc *bdsc));

PXV_FUNC(MENU*, fft2d_image_menu, (void));
PXV_FUNC(int, fft2d_main, (int argc, char **argv));
#endif
