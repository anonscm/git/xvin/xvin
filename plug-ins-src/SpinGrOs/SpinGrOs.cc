/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _SPINGROS_C_
#define _SPINGROS_C_

#define HELENE_VERSION

#ifdef HELENE_VERSION
# include <allegro.h>
# include "xvin.h"
#endif 

/* If you include other regular header do it here*/ 

#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <complex>
#include <algorithm>

#include <Eigen/Dense>
#include <Eigen/Sparse>

#ifdef HELENE_VERSION
/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "Spingros.hh"
#endif


#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <complex>
#include <algorithm>

#include <Eigen/Dense>
#include <Eigen/Sparse>

using namespace Eigen;

using namespace std;

#include <stdlib.h>
#include <stdio.h>
// #include <time.h>
// #include <assert.h>

typedef complex<double> complexg;
typedef complex<float> complexf;
typedef vector<double> vectorg;
const complexg iii(0,1);
inline double sq(double x) { return x*x; }
inline double min(double x, double y) { return (y < x) ? y : x; }
inline double scalar(complexg a, complexg b) { return real(a)*real(b)+imag(a)*imag(b); }
inline double cross_product(complexg a, complexg b) { return real(a)*imag(b)-imag(a)*real(b); }
double myrand(void) { return (double) rand() / (double) RAND_MAX; }

typedef SparseMatrix<float> SparseMatrixXf;
typedef SparseMatrix<double> SparseMatrixXd;
typedef SparseMatrix<complexf> SparseMatrixXcf;
typedef SparseMatrix<complexg> SparseMatrixXcd;


// double get_runtime(void) { return ((double)clock())/((double)CLOCKS_PER_SEC); } 
double get_runtime(void) { return 0; }




class Lanczos { 
    double SQR( double a ) { return (a == 0.0) ? 0.0 : a*a; }
    double SIGN( double a, double b) { return ((b) >= 0.0 ? fabs(a) : -fabs(a)); }

    double pythag(double a, double b)
    {
       double absa,absb;
       absa=fabs(a);
       absb=fabs(b);
       if (absa > absb) return absa*sqrt(1.0+SQR(absb/absa));
       else return (absb == 0.0 ? 0.0 : absb*sqrt(1.0+SQR(absa/absb)));
    }


    void tqli(vectorg &d, vectorg &e, int n)
    {
        int m,l,iter,i;
	double s,r,p,g,f,dd,c,b;

	for (i=1;i<n;i++) e[i-1]=e[i];
	e[n-1]=0.0;
	for (l=0;l<n;l++) {
		iter=0;
		do {
			for (m=l;m<n-1;m++) {
				dd=fabs(d[m])+fabs(d[m+1]);
				if ((double)(fabs(e[m])+dd) == dd) break;
			}
			if (m != l) {
				g=(d[l+1]-d[l])/(2.0*e[l]);
				r=pythag(g,1.0);
				g=d[m]-d[l]+e[l]/(g+SIGN(r,g));
				s=c=1.0;
				p=0.0;
				for (i=m-1;i>=l;i--) {
					f=s*e[i];
					b=c*e[i];
					e[i+1]=(r=pythag(f,g));
					if (r == 0.0) {
						d[i+1] -= p;
						e[m]=0.0;
						break;
					}
					s=f/r;
					c=g/r;
					g=d[i+1]-p;
					r=(d[i]-g)*s+2.0*c*b;
					d[i+1]=g+(p=s*r);
					g=c*r-b;
 				}
				if (r == 0.0 && i >= l) continue;
				d[l] -= p;
				e[l]=g;
				e[m]=0.0;
			}
		} while (m != l);
	}
    }



    void eigen_tqli(VectorXd &d, VectorXd &e, int n)
    {
       int m,l,iter,i;
       double s,r,p,g,f,dd,c,b;

       for (i=1;i<n;i++) 
	  e(i-1) = e(i);
       e(n-1) = 0.0;
       for (l=0;l<n;l++) {
	 iter=0;
	 do {
	    for (m=l;m<n-1;m++) {
	       dd=fabs(d(m))+fabs(d(m+1));
	       if ((double)(fabs(e(m))+dd) == dd) break;
	    }
	    if (m != l) {
	       g = (d(l+1)-d(l))/(2.0*e(l));
	       r = pythag(g,1.0);
	       g = d(m)-d(l)+e(l)/(g+SIGN(r,g));
	       s = c =1.0;
	       p = 0.0;
	       for (i=m-1;i>=l;i--) {
		  f = s*e(i);
		  b = c*e(i);
		  e(i+1) = (r=pythag(f,g));
		  if (r == 0.0) {
		     d(i+1) -= p;
		     e(m)=0.0;
		     break;
		  }
		  s=f/r;
		  c=g/r;
		  g=d(i+1)-p;
		  r=(d(i)-g)*s+2.0*c*b;
		  d(i+1)=g+(p=s*r);
		  g=c*r-b;
	       }
	       if (r == 0.0 && i >= l) continue;
	       d(l) -= p;
	       e(l)=g;
	       e(m)=0.0;
	    }
	 } while (m != l);
       }
    }


    void eigen_load_rand_vector(VectorXd &v)
    {
       for (int i = 0; i < v.rows(); i++) { 
	  v(i) = myrand();
       }
       double norm = v.norm();
       v /= norm;
    }

    void eigen_load_rand_vector(VectorXcd &v)
    {
       for (int i = 0; i < v.rows(); i++) { 
	  v(i) = myrand() + iii * myrand();
       }
       double norm = v.norm();
       v /= norm;
    }

    complexg scalar_product(const VectorXcd &v1, const VectorXcd &v2) { 
       VectorXcd Z(1);
       Z =  v1.adjoint() * v2;
       return Z(0); 
    } 


    double scalar_product_real(const VectorXcd &v1, const VectorXcd &v2) { 
       double s = 0;
       for (int i=0; i < v1.size(); i++) { 
	  s += ( real(v1(i)) * real(v2(i)) + imag(v1(i)) * imag(v2(i)) );
       }
       return s; 
    } 

    double scalar_product(const VectorXd &v1, const VectorXd &v2) { 
       VectorXd Z(1);
       Z =  v1.transpose() * v2;
       return Z(0); 
    } 



    int find_evals(vectorg &evals, vectorg &d, double epsilon) { 
       int finsize = d.size();
       int found = 0;
       double foundval;
       for (int ic = 0; ic < finsize-1; ic++) {
	  if (!found && abs(d[ic]-d[ic+1]) < epsilon) { 
	     evals[found++] = foundval = d[ic];
	  } else if ( abs(d[ic]- foundval) > 10.0 * epsilon  && abs(d[ic]-d[ic+1]) < epsilon  ) {
	    if (found == evals.size()) cout << "Too many eigenvalues found !" << endl;
	    evals[found++] = foundval = d[ic];
	  }
       }
       return found;
    }



    int find_eval_abs(vectorg &evals, vectorg &d, double epsilon) { 
       int finsize = d.size();
       for (int i = 0; i < d.size(); i++) { 
	  d[i] = abs(d[i]);
       }

       sort(d.begin(), d.end());

       int found = 0;
       double foundval;
       for (int ic = 0; ic < finsize-1; ic++) {
	  if (!found && abs(d[ic]-d[ic+1]) < epsilon) { 
	     evals[found++] = foundval = d[ic];
	  } else if ( abs(d[ic]- foundval) > 10.0 * epsilon  && abs(d[ic]-d[ic+1]) < epsilon  ) {
	    evals[found++] = foundval = d[ic];

	    if (found > evals.size()/2) { 
	       cerr << "Too many eigenvalues found : " <<  found << " for size " << evals.size() << endl;	       

	       double mindiff = abs(evals[1] - evals[0]);
	       int minj = 0;
	       for (int j = 1; j < found-1; j++) { 
		  if ( abs(evals[j+1] - evals[j]) < mindiff ) { 
		     mindiff = abs(evals[j+1] - evals[j]);
		     minj = j;
		  }
	       }

	       cerr << "removing eigenvalue : " <<  evals[minj+1] << " with distance to next eigenvalue " << mindiff << endl;	       

	       for (int j = minj+1; j < found-1; j++) { 
		  evals[j] = evals[j+1];
	       }
	       found--;
	    }
	  }
       }
       for (int i = 0; i < found; i++) { 
	  evals[2 * found - i -1] = evals[i];
	  evals[i] = -evals[i];
       }
     
       return 2*found;
    }


public : 

    bool electron_hole_symmetry;

    Lanczos(void) {
        electron_hole_symmetry = false;
    }


  /*
   * Lanczos diagonalization function for symmetric matrixes ... 
   * Z   : pointer to a vector of lists containing the column number of all
   * non-zero elements for each line of the matrix.
   * Res : eigenvalue vector 
   * findration : minimal percentage of eigenvalues to be found 
   * returns: number of found eigenvales 
   */
    int eigenvalues(SparseMatrixXd &Z, vectorg &Res, double findratio, double epsilon) 
    {
       int n = Z.rows();


       VectorXd W(n); 
       eigen_load_rand_vector(W);
       VectorXd V = VectorXd::Zero(n);
       
       int phi = 4;
       int maxJ = phi * n;
       vectorg B (maxJ+1);
       B[0] = 1.0;
       vectorg A (maxJ+1);      
       vectorg d(maxJ+1);
       vectorg e(maxJ+1); 
       
       int j = 0;
       
       for (;;) {
	  while (j < maxJ) {
	     if (j) 
	        // W <- Q_{j+1} = r_j / \beta_j  and V <- \beta_j Q_{j}
	        for (int i = 0; i < n; i++) {
		   double t = W(i);
		   W(i) = V(i)/B[j];
		   V(i) = -B[j] * t;
		}
	     
	     // matrix vector multiplication : A Q_{j+1} 
	     // V <- A Q_{j+1} - \beta_j Q_{j}
	     V += Z * W;
	     
	     // \alpha_j = <Q_{j+1}, A Q_{j+1} - \beta_j Q_{j}> = <Q_{j+1}, A Q_{j+1}> 
	     A[j] = scalar_product(W,V);

	     // V <- (A - \alpha_j) Q_{j+1} - \beta_j Q_{j}
	     V -= A[j] * W;
	    	     
	     j++;

	     if (j >= B.size()) {
		 phi++;
		 A.resize(phi*n);
		 B.resize(phi*n);
	     }

	     if (j < B.size()) { 
	        B[j] = V.norm();
	     }
	  }

	  int finsize = j;
	  d.resize(finsize);
	  e.resize(finsize);
	  
	  for (int q=0; q<finsize; q++) { 
	     d[q] = A[q];
	     e[q] = B[q];
	  }
	  tqli (d , e, finsize);
	  
	  sort(d.begin(), d.begin()+finsize);

	  /*
	   * In the abscence of numerical errors the algorithm ends after one iteration  
	   * (see http://www.mat.uniroma1.it/~bertaccini/seminars/CS339/)
	   * However for large matrices the method is not stable, and has to be run several times. 
	   */ 

	  int neval_found;
	  if (electron_hole_symmetry == false) { 
	     neval_found = find_evals(Res, d, epsilon);
	  } else { 
	     neval_found = find_eval_abs(Res, d, epsilon);
	  }

	  if ( (double) neval_found / (double) n >= findratio || neval_found == n ) { 
	     return neval_found;
	  }
	  maxJ += 2 * n;

       }   
    }

    int eigenvalues(SparseMatrixXcd &Z, vectorg &Res, double findratio, double epsilon) 
    {
       int n = Z.rows();


       VectorXcd Vn(n); 
       eigen_load_rand_vector(Vn);
       VectorXcd Vp = VectorXcd::Zero(n);
       VectorXcd W(n); 
       
       int phi = 4;
       int maxJ = phi * n;
       vectorg B (maxJ+1);
       B[0] = 0.0;
       vectorg A (maxJ+1);      
       vectorg d(maxJ+1);
       vectorg e(maxJ+1); 
       
       int j = 0;
       
       for (;;) {
	  while (j < maxJ) {
	     W = Z * Vn;
	     A[j] = real( scalar_product(W, Vn) ); // the scalar product is real for self adjoint operators 	    	 	     
	     W -= A[j] * Vn;
	     W -= B[j] * Vp;

	     j++;

	     if (j >= B.size()) {
		 phi++;
		 A.resize(phi*n);
		 B.resize(phi*n);
	     }

	     B[j] = W.norm();
	     Vp = Vn;
	     Vn = W / B[j];
	  }

	  int finsize = j;
	  d.resize(finsize);
	  e.resize(finsize);
	  
	  for (int q=0; q<finsize; q++) { 
	     d[q] = A[q];
	     e[q] = B[q];
	  }
	  tqli (d , e, finsize);
	  
	  sort(d.begin(), d.begin()+finsize);

	  /*
	   * In the abscence of numerical errors the algorithm ends after one iteration  
	   * (see http://www.mat.uniroma1.it/~bertaccini/seminars/CS339/)
	   * However for large matrices the method is not stable, and has to be run several times. 
	   */ 

	  int neval_found;
	  if (electron_hole_symmetry == false) { 
	     neval_found = find_evals(Res, d, epsilon);
	  } else { 
	     neval_found = find_eval_abs(Res, d, epsilon);
	  }

	  cerr << get_runtime() << " : lanczos.eigenvalues found " << neval_found << " out of " << n << endl;

	  if ( (double) neval_found / (double) n >= findratio || neval_found == n ) { 
	     return neval_found;
	  }
	  maxJ += 2 * n;

       }   
    }



};



typedef Triplet<double> TMatrixXd;
typedef Triplet<complexg> TMatrixXcd;




class SNSsparseSO { 
public:
    double Delta; 
    double HtS; 
    double HtN; 
    double Phi;
    double Phi_orb;
    double W;
    double eigen_fraction;
    double lambda;
    double lso;
    double BzN;
    double BzS;
    double Tsn;
    double epsilonF;
    int NSupra;
    bool phase_in_delta;
    int first_site;
private:

    int NY;
    int NX;
    int size; 
    int n_found;

    enum { UP, DN, UPCC, DNCC, NS };
  //    enum { UP, DNCC, NS };

    SparseMatrixXcd Hsp;
    SparseMatrixXcd idm;
    Lanczos lanczos_diag;
    vectorg lanczos_eval;

    // applies periodic boundary conditions 
    int enc_xys(int x, int y, int s) {
       return NS * (NY * (x % NX) + (y % NY)) + s;
    }

    int dec_x(int n) { 
       return n / (NS * NY);
    }

    int dec_y(int n) { 
       return (n / NS) % NY;
    }

    int dec_s(int n) { 
       return n % (NS);
    }

    bool site_is_normal(int x) { 
       return (x >= NSupra/2) && (x < (NX - NSupra/2));
    }

    int normal_length(void) { 
       return NX - 2 * (NSupra/2);
    }





    complexg rxy_graph(int x, int y) { 
       double rx, ry;
       
       rx = (x/4) * 3;
       
       int row = ((x % 4) + 4)%4;
       if (row == 1) { rx += 0.5; }
       else if (row == 2) { rx += 1.5; }
       else if (row == 3) { rx += 2.0; }
       
       ry = sqrt(3.0) * (double) y;
       if (row == 1 || row == 2) { ry += sqrt(3.0)/2.0;  }  
       
       return rx + iii * ry;
    }
  

    complexg rxy_sample(int x, int y) { 
       int x_norm_first = NSupra/2;
       int x_norm_last = NX - NSupra/2 - 1;
       if (x >= x_norm_first && x <= x_norm_last) { 
	 return rxy_graph(x - x_norm_first + first_site, y);
       } else if (x < x_norm_first) { 
	 return rxy_graph(first_site, y) + sqrt(3.0) * (double) (x - x_norm_first);
       } else if (x > x_norm_last) { 
	 return rxy_graph(x_norm_last-x_norm_first+first_site, y) + sqrt(3.0) * (double) (x - x_norm_last);
       }
       
    }
  

    bool are_neighbors(int x1, int y1, int x2, int y2) { 
       int x_norm_first = NSupra/2;
       int x_norm_last = NX - NSupra/2 - 1;

       // we decide that a site is not its own neighbor 
       if (x1 == x2 && y1 == y2) {
	 return false; 
       }
       
       bool n = false;
       if (x1 <= x_norm_first && x2 <= x_norm_first || x1 >= x_norm_last && x2 >= x_norm_last) { 
	  if (abs(x1-x2) == 1 && y1==y2 || abs(y1-y2) == 1 && x1==x2 ) { 
	     n = true;
	  }
       } else if (site_is_normal(x1) && site_is_normal(x2)) { 
	  if ( abs( rxy_sample(x1, y1) - rxy_sample(x2, y2) ) < 1.01 ) { 
	     n = true;
	  }
       }
       return n;
    } 

    bool are_second_neighbors(int x1, int y1, int x2, int y2) { 
       bool n = false; 

       if (site_is_normal(x1) && site_is_normal(x2)) { 
	 double r12 = abs( rxy_sample(x1, y1) - rxy_sample(x2, y2) );
	 if (abs(r12 - sqrt(3.0)) < 0.01) {
	    n = true;
	 } 
       }
       return n;
    } 

    bool intermediate_site(int x1, int y1, int x2, int y2, int &xi, int &yi) { 
       bool found = false;
       for (int dx = -1; dx <= 1; dx++) { 
	  for (int dy = -1; dy <= 1; dy++) { 
	     if (are_neighbors(x1, y1, x1+dx, y1+dy) && are_neighbors(x2, y2, x1+dx, y1+dy)) { 
	        xi = x1+dx;
		yi = y1+dy;
		found = true;
	     }
	  }
       }
       return found;
    }


  
    complexg t_phase(int x1, int y1, int x2, int y2) { 
       complexg t = 1.0;

       int x_norm_first = NSupra/2;
       int x_norm_last = NX - NSupra/2 - 1;
       double normal_length = real(rxy_sample(x_norm_last, 0)) - real(rxy_sample(x_norm_first, 0));
       double Snorm = (double)(NX - 2*(NSupra/2)) * (double) NY;

       if ( site_is_normal(x1) && site_is_normal(x2) ) { 
	  complexg r1 = rxy_sample(x1, y1);
	  complexg r2 = rxy_sample(x2, y2);
	  
	  if (!phase_in_delta) { 
	     t *= exp(iii * M_PI * Phi * (real(r2) - real(r1))/normal_length);
	  }

	  if (site_is_normal(x1) && site_is_normal(x2))  { 
	     t *= exp(iii * M_PI * (real(r1) + real(r2)) * Phi_orb * (imag(r2) - imag(r1)) / Snorm);
	  } else if (x1 >= x_norm_last && x2 >= x_norm_last) { 
	     complexg lr1 = rxy_sample(x_norm_last, y1);
	     complexg lr2 = rxy_sample(x_norm_last, y2);
	     t *= exp(iii * M_PI * (real(lr1) + real(lr2)) * Phi_orb * (imag(lr2) - imag(lr1)) / Snorm);
	  } 
       }
       return t;
    }

  



    complexg Delta_xy(int x, int y) { 
       if (x < NSupra/2) { return Delta; }
       else if (x >= NX - NSupra/2) { 
	  if (phase_in_delta) { 
	     return Delta * exp(iii * 2.0 * M_PI * Phi); 
	  } else { 
  	     return Delta; 
	  }
       } else { return 0; } 
    }


public : 
  
    SNSsparseSO(int NX_new, int NY_new) { 
       NX = NX_new;
       NY = NY_new;
       size = NX * NY * NS;

       Hsp.resize(size, size);
       idm.resize(size, size);
       Hsp.setZero();
       idm.setIdentity();
       lanczos_eval.resize(size);
       n_found = 0;
       lanczos_diag.electron_hole_symmetry = true;
       phase_in_delta = false;
    }

    void fill_matrix(void) { 
       Hsp.setZero();

       std::vector< TMatrixXcd > coef; 
       coef.reserve(4 * NX * NY * NS);

       for (int x = 0; x < NX; x++) { 
	  for (int y = 0; y < NY; y++) { 
	     double Vxy = (myrand() - 0.5) * W;	     

	     for (int s = 0; s < NS; s++) { 
	        int n = enc_xys(x, y, s);

		for (int dx = -2; dx <= 2; dx++) { 
		   for (int dy = -2; dy <= 2; dy++) { 

		     if (x+dx < 0 || x+dx >= NX || y+dy < 0 || y+dy >= NY) {
		        continue;
		     }

		     double Htl;
		     if (site_is_normal(x) && site_is_normal(x+dx)) { 
		       Htl = HtN;
		     } else { 
		       Htl = HtS;
		     }

		     complexg tt = Htl * t_phase(x,y,x+dx,y+dy);

		     if ( !site_is_normal(x) && site_is_normal(x+dx) || site_is_normal(x) && !site_is_normal(x+dx) ) { 
		       tt *= Tsn;
		     }

		     double sigma_z;
		     if (s == UP || s == UPCC) { sigma_z = 1.0; } 
		     else { sigma_z = -1.0; }

		     double Bz = (site_is_normal(x)) ? BzN : BzS;

		     // on site energy 
		     double Hxy = Vxy + sigma_z * Bz - epsilonF;

		     if (s == DNCC || s == UPCC) { 
		       tt =  -conj(tt);
		       Hxy = -Hxy;
		     }
		
		     if (!dx && !dy) {
		        coef.push_back( TMatrixXcd(n, n, Hxy) );
		     } else if (are_neighbors(x, y, x+dx, y+dy)) { 
		        int nn = enc_xys(x+dx,y+dy,s);
			coef.push_back( TMatrixXcd(n, nn, tt) );
			/***
			cerr << "set arrow from " << real(rxy_sample(x,y)) << "," << imag(rxy_sample(x,y)) << " to " 
			     << real(rxy_sample(x+dx,y+dy)) << "," << imag(rxy_sample(x+dx,y+dy))
			  // << " as 1" 
			     << endl;
			***/
		     } else if (are_second_neighbors(x, y, x+dx, y+dy)) { 
		        int nn = enc_xys(x+dx,y+dy,s);

		        int xi, yi;
			intermediate_site(x, y, x+dx, y+dy, xi, yi);
			complexg d1 = rxy_sample(xi, yi) - rxy_sample(x, y);
			complexg d2 = rxy_sample(x+dx, y+dy) - rxy_sample(xi, yi);

			complexg tso = iii * lso;

			tso *= t_phase(x, y, xi, yi);
			tso *= t_phase(xi, yi, x+dx, y+dy);

			tso *= ((cross_product(d2, d1) > 0) ? 1.0 : -1.0);
			tso *= sigma_z;
			

			if (s == DNCC || s == UPCC) { 
			   tso =  -conj(tso);
			}
			coef.push_back( TMatrixXcd(n, nn, tso) );
		     }
		   }
		}
	     }
	     
	     
	     complexg delta = Delta_xy(x, y);

	     if (norm(delta) != 0) { 
	        int n_up = enc_xys(x, y, UP);
		int n_dncc = enc_xys(x, y, DNCC);
		
		int n_dn = enc_xys(x, y, DN);
		int n_upcc = enc_xys(x, y, UPCC);
	        coef.push_back( TMatrixXcd(n_up, n_dncc, delta) );
		coef.push_back( TMatrixXcd(n_dncc, n_up, conj(delta)));

		coef.push_back( TMatrixXcd(n_dn, n_upcc, -delta) );
		coef.push_back( TMatrixXcd(n_upcc, n_dn, -conj(delta)));
	     }

	     
	     if (lambda != 0) { 
	        int up0 = enc_xys(x, y, UP);
	        int dn0 = enc_xys(x, y, DN);
	        int upcc0 = enc_xys(x, y, UPCC);
	        int dncc0 = enc_xys(x, y, DNCC);

	        int up_yp1 = enc_xys(x, y+1, UP);
	        int up_ym1 = enc_xys(x, y-1, UP);
	        int dn_yp1 = enc_xys(x, y+1, DN);
	        int dn_ym1 = enc_xys(x, y-1, DN);

	        int upcc_yp1 = enc_xys(x, y+1, UPCC);
	        int upcc_ym1 = enc_xys(x, y-1, UPCC);
	        int dncc_yp1 = enc_xys(x, y+1, DNCC);
	        int dncc_ym1 = enc_xys(x, y-1, DNCC);

	        int up_xp1 = enc_xys(x+1, y, UP);
	        int up_xm1 = enc_xys(x-1, y, UP);
	        int dn_xp1 = enc_xys(x+1, y, DN);
	        int dn_xm1 = enc_xys(x-1, y, DN);

	        int upcc_xp1 = enc_xys(x+1, y, UPCC);
	        int upcc_xm1 = enc_xys(x-1, y, UPCC);
	        int dncc_xp1 = enc_xys(x+1, y, DNCC);
	        int dncc_xm1 = enc_xys(x-1, y, DNCC);
	
	     }
	  }
       }

       Hsp.setFromTriplets(coef.begin(), coef.end());
    }

    void diag(void) { 
       cerr << get_runtime() << " : launching lanczos_diag" << endl;
       n_found = lanczos_diag.eigenvalues(Hsp, lanczos_eval, eigen_fraction, 1e-10);
       cerr << get_runtime() << " : found " << n_found << " eigenvalues out of " << size << endl;
    }

    double eval(int i) { 
       return lanczos_eval[i];
    };

#ifdef HELENE_VERSION

  void print_eval(pltreg *pr, O_p **op) {
    d_s *ds = NULL;
    int i;
    if (*op == NULL)
      {
	*op = create_and_attach_one_plot(pr, 16, 16, 0);
	if (*op != NULL)
	  {
	    ds = (*op)->dat[0];
	    ds->nx = ds->ny = 0;
	    for (i = 1; i < n_found; i++)
	      {
		ds = create_and_attach_one_ds(*op, 16, 16, 0);
		if (ds != NULL) ds->nx = ds->ny = 0;
	      }
	    set_plot_title(*op,"\\stack{{Nx = %d; Ny = %d; Ns = %d; N_{supra} = %d}"
			   "{\\Delta = %g; Ht = %g; \\lambda = %g \\phi = %g}"
			   "{\\pt7 W = %g eigen fraction %g phase in delta %g}}"
			   ,NX,NY,NS,NSupra,Delta,HtS,lambda,Phi,W,eigen_fraction,phase_in_delta);

	  }
      }
    if (*op != NULL)
      {
       for (i = 0; i < n_found; i++) {
	 add_new_point_to_ds((*op)->dat[i], Phi, lanczos_eval[i]); 
	 //cout << Phi << "    " << lanczos_eval[i] << endl;
       }
      }
       //cout << endl;
    }
#else 
    void print_eval(void) { 
       for (int i = 0; i < n_found; i++) {
	 cout << Phi << "    " << lanczos_eval[i] << endl;
       }
       cout << endl;
    }
#endif 

    void print_wavefunction(void) { 
       int count = 0;
       while (lanczos_eval[count] < 0) { 
	  count++;
       } 
       cout << "# eigenvalue : " << lanczos_eval[count] << endl;

       VectorXcd xxx(size); 
       VectorXcd yyy = VectorXcd::Random(size);

       /***
       SparseMatrixXcd Hdiff = Hsp - lanczos_eval[count] * idm;
       ConjugateGradient<SparseMatrixXcd> cg;
       cg.compute(Hdiff);
       for (int k=0; k<10; k++) { 
	 x = cg.solve(y);
	 cerr << cg.iterations() << endl;
	 cerr << cg.error() << endl;

	 double n = x.norm();
	 x /= n;
	 y = x;
       } 
       ***/

       SparseMatrix<complexg, ColMajor> Hdiff = Hsp - lanczos_eval[count] * idm;
       SparseLU<SparseMatrix<complexg, ColMajor>, COLAMDOrdering<int> > solver;
       solver.analyzePattern(Hdiff); 
       solver.factorize(Hdiff); 

       for (int k=0; k<10; k++) { 
	 xxx = solver.solve(yyy);
	 double n = xxx.norm();
	 xxx /= n;
	 yyy = xxx;
       } 

       yyy = Hdiff * xxx;
       cerr << "error : " << yyy.norm() << "    " << xxx.norm() << endl;

       for (int x=0; x < NX; x++) { 
	  for (int y=0; y < NY; y++) { 
	     cout << x << "    " << y << "    " << norm(xxx(enc_xys(x, y, UP))) << "    " << norm(xxx(enc_xys(x, y, DNCC))) << "    " << norm(xxx(enc_xys(x, y, DN))) << "    " << norm(xxx(enc_xys(x, y, UPCC))) << endl;
	  }
	  cout << endl;
       }

       exit(0);
    }

    double self_adjoint_check(void) { 
       double err = 0;

       for (int i = 0; i < size; i++) { 
	  for (int j = 0; j < size; j++) { 
	    complexg a = Hsp.coeffRef(i, j);
	    complexg b = Hsp.coeffRef(j, i);
	    err += norm(a - conj(b));
	  }
       }
       return err;
    }

    void print_info(void) { 
       cout << "# NX " << NX << endl;
       cout << "# NY " << NY << endl;
       cout << "# NS " << NS << endl;
       cout << "# NSupra " << NSupra << endl;
       cout << "# Delta " << Delta << endl;
       cout << "# HtS " << HtS << endl;
       cout << "# HtN " << HtN << endl;
       cout << "# lambda " << lambda << endl;
       cout << "# lso " << lso << endl;
       cout << "# BzN " << BzN << endl;
       cout << "# BzS " << BzS << endl;
       cout << "# Phi " << Phi << endl;
       cout << "# Phi_orb " << Phi_orb << endl;
       cout << "# W " << W << endl;
       cout << "# Tsn " << Tsn << endl;
       cout << "# first_site " << first_site << endl;
       cout << "# chemical potential " << epsilonF << endl;
       cout << "# eigen_fraction " << eigen_fraction << endl;
       cout << "# phase_in_delta " << phase_in_delta << endl;    
    }
};




#ifdef HELENE_VERSION
int do_Spingros_hello(void)
{
  int i;

# else
// main version 
int  main(int argc, char **argv)
{
  bool p_in_d = false;
# endif

  static int Nx = 80;
  static int Ny = 10;
  static int Ns = 30;

  static double Ht = 4.0;
  static double Delta = 1.0;
  static double BzS = 1e-6;
  static double BzN = 1e-6;
  static double lambda = 0.1;
  static double drh = 10.0;  //not used
  static double phi0 = 0.01;
  static double phi_orb = 0.01;
  static double W = 2.0;
  static double eigen_fraction = 0.9998;
  static double epsilonF = 0.5;
  static double Tcontacts = 1.0;   //not used
  static int nph = 32;
  SNSsparseSO Hr(Nx, Ny);

#ifdef HELENE_VERSION
  static int p_in_d = 0;

  O_p *op = NULL;
  d_s *ds = NULL;
  pltreg *pr = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");


  i = win_scanf("Define the size Nx=%4d; Ny=%4d Ns=%4d\n"
		"Ht = %6lf, Delta = %6lf, W = %6lf\n"
		"eigen_fraction = %6lf, lambda = %6lf\n"
		"BzS = %6lf, BzN = %6lf\n"
		"phase_in_delta %b\n"
		"phi_value %6lf Nb. of points to compute phase %4d\n"
		"drh = %6lf, T_{contacts} %6lf\n"
		"\\phi_{orb} = %6lf \\epsilon_f = %6lf\n"
		,&Nx, &Ny, &Ns, &Ht, &Delta, &W, &eigen_fraction, &lambda, &BzS, &BzN, 
		&p_in_d, &phi0, &nph, &drh, &Tcontacts, &phi_orb, &epsilonF);

  if (i == CANCEL) return 0;

# endif

  Hr.NSupra = Ns;
  Hr.HtS = Ht;
  Hr.HtN = Ht;
  Hr.Delta = Delta;
  Hr.W = W;
  Hr.eigen_fraction = eigen_fraction;
  Hr.lambda = 0.0; 
  Hr.lso = lambda;
  Hr.BzS = BzS;
  Hr.BzN = BzN;
  Hr.Tsn = 1.0;
  Hr.first_site = 1;
  Hr.phase_in_delta = (p_in_d) ? true : false;
  Hr.Phi_orb = phi_orb;
  Hr.epsilonF = epsilonF;
  Hr.print_info();

  double Phi0 = phi0;
  cerr << Phi0 << endl;
  double Phi = Phi0;
  for (i = 0; i < nph; i++) {
    Phi = ((double)i + 0.5)/nph;
    Phi += Phi0;
    Hr.Phi = Phi;
    srand(1);
    Hr.fill_matrix();
    Hr.diag();


#ifdef HELENE_VERSION
    Hr.print_eval(pr, &op);
    op->need_to_refresh = 1;
    refresh_plot(pr, pr->n_op-1);
#else
    Hr.print_eval();
#endif


    //    Hr.print_wavefunction();
  }
  return 0;
}

#ifdef HELENE_VERSION

MENU *Spingros_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Diagonalization", do_Spingros_hello,NULL,0,NULL);
  return mn;
}

int	Spingros_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "Spingros", NULL, Spingros_plot_menu(), 0, NULL);
  return D_O_K;
}

int	Spingros_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "Spingros", NULL, NULL);
  return D_O_K;
}
#endif
 

#endif

