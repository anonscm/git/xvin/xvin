#pragma once
#include "xvin.h"

PXV_FUNC(int, do_SpinGrOs_rescale_plot, (void));
PXV_FUNC(MENU*, SpinGrOs_plot_menu, (void));
PXV_FUNC(int, do_SpinGrOs_rescale_data_set, (void));
PXV_FUNC(int, SpinGrOs_main, (int argc, char **argv));
