#ifndef ANDOR_2_XVIN_C
#define ANDOR_2_XVIN_C
#include "allegro.h"
#include "winalleg.h"
#include "xvin.h"

int ANDOR_oi_mouse_action(O_i *oi, int x0, int y0, int mode, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  BITMAP *imb;
  unsigned long t0 = 0;

  //  display_title_message("cou cou");
  //return D_O_K;
  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;


  while(mouse_b == 1)
    {
      if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
	{
	  t0 = my_uclock();
	  
	  display_image_stuff_16M(imr,d);
	  oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
	  t0 = my_uclock() - t0;
	  write_and_blit_im_box( plt_buffer, d, imr);
      

	  imb = (BITMAP*)oi->bmp.stuff;
	 
	  display_title_message("Mouse im %d dt %6.3f ms",oi->im.c_f,  1000*(double)(d->d1)/get_my_uclocks_per_sec());
	}
      if (general_idle_action) general_idle_action(d);
    }

  // this routine switches supress the image idle action, 
  // in particular info display by xvin on image by default 

  return 0;
}




int ANDOR_before_menu(int msg, DIALOG *d, int c)
{
  /* this routine switches the display flag so that no magic color is drawn 
     when menu are activated */
  if (msg == MSG_GOTMOUSE)
    {
      //do_refresh_overlay = 0;
      //display_title_message("menu got mouse %d",n_t++);
    }
  return 0;
}

int ANDOR_after_menu(int msg, DIALOG *d, int c)
{
  //display_title_message("menu exit mouse");
  return 0;
}

int ANDOR_oi_got_mouse(struct  one_image *oi, int xm_s, int ym_s, int mode)
{
  /* this routine switches the display flag so that the magic color is drawn again
     after menu are activated */
  //do_refresh_overlay = 1;
  //display_title_message("Image got mouse");
  return 0;
}

int prepare_image_overlay(O_i* oi)
{
  return 0;
}
#endif
