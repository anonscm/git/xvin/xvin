#ifndef _IXON_THREADS_C_
#define _IXON_THREADS_C_

#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>

#include "xvin.h"
#include "ATMCD32D.h"
#include "global_define.h"
#include "global_variables.h"
#include "global_functions.h"

//#include "iXon.h"
//#include "iXon_tape.h"
#include <time.h>

#define BUILDING_PLUGINS_DLL
#include "iXon_threads.h"

// SetTimer( win_get_window(),25,SAVING_INTERVAL,automatic_saving);/*sets a background saving*/

//void CALLBACK automatic_saving(HWND hwnd, UINT uMsg, UINT_PTR idEvent,  DWORD dwTime)
//  if (KillTimer(win_get_window(),  25) == FALSE) return win_printf_OK("Timer not  killed"); 
  
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Thread functions

////////////////////////////////////////////////////////////////////////////////

/** Standby thread.

After a specified timeout, the system sets the camera to standby mode, mainly by switching off the acquisition
and the gain amplifier.
\param int None.
\return int Error code.
\author Adrien Meglio
\version 21/09/07
*/ 
DWORD WINAPI StandbyThreadProc(LPVOID lpParam) 
{
  time_t begin_time;
  time_t current_time;

  time(&begin_time);
  time(&current_time);
  
  iXon.standby_action = STANDBY;

  while(((current_time-begin_time) < iXon.standby_timeout) && (iXon.standby_action == STANDBY))
    {
      Sleep(1000); /* Wait for 1 second */
      current_time = time(NULL);
      //win_event("%d < %d sec ? %d",(current_time-begin_time),iXon.standby_timeout,((current_time-begin_time) < iXon.standby_timeout));
    }
  
  if (iXon.standby_action == STANDBY)
    {
      standby_action(); /* What to do when timeout is reached */

      iXon.standby_action = WAIT_FOR_WAKEUP; /* Waits for an action to wake up */
    }

  //win_report("Standby exited");
    
  return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////

/** General display thread main operation.

This function is periodically called by the display control thread. 
It monitors acquisition status and reports them to the screen.
\param int None.
\return int Error code.
\return Screen output of acquisition status.
\author Adrien Meglio
\version 26/01/07
*/ 
DWORD WINAPI StatusThreadProc(LPVOID lpParam) 
{
  int current_temperature;
  float pifoc_position;
  float t_position;
  float r_position;
  int line_status;
  float line_level;
  unsigned int temperature_error = GetTemperature(&current_temperature);
  BITMAP *temp_button;
    
  temp_button = new_button(2,40,3,"TEMPERATURE : -69 C ; TARGET : -70 C BLABLABLABLABLA");
    
  while(1)
    {
      SleepEx(30000,FALSE);
      temperature_error = GetTemperature(&current_temperature);
                  
      switch (temperature_error)
        {
	case DRV_NOT_INITIALIZED : 
	  textprintf_ex(temp_button,font,0,0,GREY_160,-1,"TEMPERATURE :");
	  textprintf_ex(temp_button,font,0,text_height(font),RED,-1,"ERROR"); 
	  return D_O_K; 
	  break;
	case DRV_ACQUIRING : 
	  textprintf_ex(temp_button,font,0,0,GREY_160,-1,"TEMPERATURE : %d C (LAST READ)",iXon.temperature_lastread);
	  textprintf_ex(temp_button,font,0,text_height(font),BLUE,-1,"ACQUIRING"); 
	  break;
	case DRV_ERROR_ACK : 
	  textprintf_ex(temp_button,font,0,0,GREY_160,-1,"TEMPERATURE :");
	  textprintf_ex(temp_button,font,0,text_height(font),RED,-1,"ERROR"); 
	  return D_O_K; 
	  break;
	case DRV_TEMP_OFF : 
	  iXon.temperature_lastread = current_temperature;
	  textprintf_ex(temp_button,font,0,0,GREY_160,-1,"TEMPERATURE : %d C",current_temperature);
	  textprintf_ex(temp_button,font,0,text_height(font),BLUE,-1,"TEMP CONTROL OFF"); 
	  break;
	case DRV_TEMP_NOT_REACHED : 
	  iXon.temperature_lastread = current_temperature;
	  textprintf_ex(temp_button,font,0,0,GREY_160,-1,"TEMPERATURE : %d C ; TARGET : %d C",current_temperature,iXon.temperature_target);
	  textprintf_ex(temp_button,font,0,text_height(font),PINK,-1,"NOT STABILIZED"); 
	  break;
	case DRV_TEMP_STABILIZED : 
	  iXon.temperature_lastread = current_temperature;
	  textprintf_ex(temp_button,font,0,0,GREY_160,-1,"TEMPERATURE : %d C",current_temperature);
	  textprintf_ex(temp_button,font,0,text_height(font),GREEN,-1,"STABILIZED"); 
        }
        
      if (iXon.contrast == AUTOMATIC)
        {
	  textprintf_ex(temp_button,font,0,2*text_height(font),GREY_160,-1,"AUTOMATIC CONTRAST ON ");
        } else
        {
	  textprintf_ex(temp_button,font,0,2*text_height(font),GREY_128,-1,"AUTOMATIC CONTRAST OFF");
        }
    }  
    
  destroy_bitmap(temp_button);    
    
  return D_O_K;
} 

////////////////////////////////////////////////////////////////////////////////

/** Retrieves the images taken by the iXon camera and stored in the hardware circular buffer.

This function is called by a time-critical thread ; it retrieves the images in the camera circular buffer and sends them to the software 
circular buffer to be displayed
.
\param LPVOID A handle (usually NULL).
\return int Error code.
\author Adrien Meglio
\version 05/09/07
*/ 
DWORD WINAPI RetrieveImages(LPVOID lpParam) 
{
  long first = -1 , last = -1 , validfirst, validlast;
  BITMAP * imb = NULL;
  int i;

  /* Creates the buffer if it doesn't exist */
  if (oi_buffer == NULL)
    {
      oi_buffer = create_one_image(iXon.total_width,iXon.total_height,iXon.image_type);
    }

  iXon.present_image_number_aquired_on_PC = -1;
  if (StartAcquisition() != DRV_SUCCESS)
    {
      AbortAcquisition();
      win_report("Start acquisition error");
      iXon.is_live = FREEZE;
    } 

  while (iXon.is_live == LIVE)
    {
      /* While there is no new data */                                 
      while (GetNumberNewImages(&first,&last) == DRV_NO_NEW_DATA) ;
      {
	//Sleep(0.2*iXon.exposure_time); /* Waits 1/5 th of the exposure time till data arrive */
      }
        
      /* When new data arrive in the circular buffer, retrive images one by one, beginning with 'first'
	 which is "the first image to retrieve in buffer"
	 For an unknown reason, the following instruction is not working :
	 >GetImages16(first,last,oi_iXon->im.mem[0], iXon.total_pixels_number, &validfirst, &validlast); 
	 This is maybe because oi_iXon->im.mem[0] is only for ONE image */ 
      /* It is EXTREMELY IMPORTANT to retrieve ALL images in buffer before doing ANYTHING else ;
	 if not, display will be slowed down A LOT */
      for (i=0; i<last-first+1 ; i++)
	{
	  /* oi_buffer has just ONE image in it (mem[0]) : alwas the last acquired */
	  GetImages16(first+i,first+i,oi_buffer->im.mem[0], iXon.total_pixels_number, &validfirst, &validlast);
	  iXon.present_image_number_aquired_on_PC += 1;
	}
       
      /* Refreshes screen periodically every iXon.refresh_period images taken, but only if display is requested */ 
      if  ((iXon.present_image_number_aquired_on_PC % iXon.refresh_period == 0) && (flag__display_yes_no == YES))
	{
	  /* Copies the last acquired image into oi_iXon for display */
	  memcpy(oi_iXon->im.mem[0],oi_buffer->im.mem[0],iXon.total_pixels_number*2);

	  oi_iXon->need_to_refresh |= BITMAP_NEED_REFRESH;            
	  display_image_stuff_16M(imr_iXon,d_iXon);
	  
	  imb = (BITMAP*)oi_iXon->bmp.stuff;
	  acquire_bitmap(screen);
	  blit(imb,screen,0,0,imr_iXon->x_off + d_iXon->x, imr_iXon->y_off - imb->h + d_iXon->y, imb->w, imb->h); 
	  release_bitmap(screen);	
	}
      
      /* Automatically corrects the contrast of the display */ 
      if (iXon.contrast == AUTOMATIC)
        {
	  find_zmin_zmax(oi_iXon);
        }
    }  
      
  AbortAcquisition();
     
  return D_O_K;
}

DWORD WINAPI RetrieveImages_working_previous_verison(LPVOID lpParam) 
{
  long first = -1 , last = -1 , validfirst, validlast;
  BITMAP * imb = NULL;
  int count = 1;
  int i;
    
  iXon.present_image_number_aquired_on_PC = -1;
  if (StartAcquisition() != DRV_SUCCESS)
    {
      AbortAcquisition();
      //iXon.cam_status = IDLE;
      win_report("Start acquisition error");
      iXon.is_live = FREEZE;
    } 

  while (iXon.is_live == LIVE)
    {
      /* While there is no new data */                                 
      while (GetNumberNewImages(&first,&last) == DRV_NO_NEW_DATA) ;
      {
	//Sleep(0.2*iXon.exposure_time); /* Waits 1/5 th of the exposure time till data arrive */
      }
        
      //if (initial_image == -1) initial_image = first;

      /* When new data arrive in the circular buffer, retrive images one by one, beginning with 'first'
	 which is "the first image to retrieve in buffer"
	 For an unknown reason, the following instruction is not working :
	 >GetImages16(first,last,oi_iXon->im.mem[0], iXon.total_pixels_number, &validfirst, &validlast); 
	 This is maybe because oi_iXon->im.mem[0] is only for ONE image */ 
      /* It is EXTREMELY IMPORTANT to retrieve ALL images in buffer before doing ANYTHING else ;
	 if not, display will be slowed down A LOT */
      for (i=0; i<last-first+1 ; i++)
	{
	  GetImages16(first+i,first+i,oi_iXon->im.mem[0], iXon.total_pixels_number, &validfirst, &validlast);
	  iXon.present_image_number_aquired_on_PC += 1;
	}
      //iXon.present_image_number_aquired_on_PC = last;
     
      /* Refreshes screen periodically every iXon.refresh_period images taken */ 
      if  ((iXon.present_image_number_aquired_on_PC % iXon.refresh_period == 0) && (flag__display_yes_no == YES))
	{
	  oi_iXon->need_to_refresh |= BITMAP_NEED_REFRESH;            
	  display_image_stuff_16M(imr_iXon,d_iXon);
	  
	  imb = (BITMAP*)oi_iXon->bmp.stuff;
	  acquire_bitmap(screen);
	  blit(imb,screen,0,0,imr_iXon->x_off + d_iXon->x, imr_iXon->y_off - imb->h + d_iXon->y, imb->w, imb->h); 
	  release_bitmap(screen);	
	}
      
      /* Automatically corrects the contrast of the display */ 
      if (iXon.contrast == AUTOMATIC)
        {
	  /* It's useless to adjust contrast for more images than the ones that are actually displayed */
	  if (count > iXon.refresh_period)
	    {
	      find_zmin_zmax(oi_iXon);
	      count = 1;
	    }
	  count++;
        }
    }  
      
  AbortAcquisition();
    
  return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////

/** Movie acquisition thread main operation.

This function controls movie acquisition on RAM.
\param int None.
\return int Error code.
\return none.
\author Adrien Meglio
\version 22/01/07
*/
DWORD WINAPI thread__iXon_RAM_movie_old(LPVOID lpParam)
{
  /////////////////////////////
  // Variables declarations
    
  long  initial_index = -1; // Frame indexes used in acquisition control
  long first = 0 , last = 0 ,validfirst, validlast;
    
  int frames_acquired = -1;    
  FILE *ActionLogFile;
    
  /////////////////////////////
    // Initialization
     
    if (StartAcquisition() != DRV_SUCCESS)
      {
        AbortAcquisition();
        iXon.movie_status = IDLE;
        win_report("Start acquisition error");
        iXon.is_live = FREEZE;
      } 
   
    /////////////////////////////
      // Acquisition loop
      for (frames_acquired = 0; frames_acquired < iXon.number_images_in_movie; frames_acquired++)
	{   
	  /* While there is no new data */                                 
	  while (GetNumberNewImages(&first,&last) == DRV_NO_NEW_DATA) 
	    {
	      //draw_bubble(screen,0,450,175,"Image number %d/%d !!!%d ",first - initial_index,iXon.number_images_in_movie,frames_acquired);
	      //Sleep(0.2*iXon.exposure_time); /* Waits 1/5 th of the exposure time till data arrive */
	    }
	  if (initial_index == -1) initial_index = first;
            
	  /* When new data arrive in the circular buffer */ 
	  GetImages16(first,first,oi_iXon->im.mem[0], iXon.total_pixels_number, &validfirst, &validlast);
	  /* Retrieves the "oldest" acquired-but-not-saved image of the circular buffer and copies it to oi_iXon */
                   
	  /* Copies the image from oi_iXon to the movie (and tests memcpy success)
	     The frame index is adapted to take into account the possibility that some frames 
	     have been skipped at the beginning of the film (i.e. initial-index != 1) */
	  //if (iXon_movie->im.mem[first-initial_index] != memcpy(iXon_movie->im.mem[first-initial_index],oi_iXon->im.mem[0],iXon.total_pixels_number*2)) 
	  if (iXon_movie->im.mem[frames_acquired] != memcpy(iXon_movie->im.mem[frames_acquired],oi_iXon->im.mem[0],iXon.total_pixels_number*2)) 
	    {
	      return win_report("Error ! Bug in thread--iXon-RAM-movie : memcopy failed");
	    }
                
	  /* For an unknown reason, the following two lines slow considerably acquisition : display takes hours to be done */
	  //oi_iXon->need_to_refresh |= BITMAP_NEED_REFRESH;  
	  //refresh_image(imr_iXon, UNCHANGED);
	}
        
      /* When finished with acquisition : stop*/
      /*  iXon.movie_status = IDLE; */
    
      /* Freezes the display */
      AbortAcquisition();
      iXon.movie_status = IDLE;
    
      ActionLogFile = fopen__event();
      fprintf(ActionLogFile,"At : %s Ended movie acquisition\n",formatted_time());
      fprintf(ActionLogFile,"*****************************************\n");
      fclose(ActionLogFile);

      return D_O_K;
} 

DWORD WINAPI thread__iXon_RAM_movie(LPVOID lpParam)
{
  /////////////////////////////
  // Variables declarations
    
  long  initial_index = -1; // Frame indexes used in acquisition control
  long first = 0 , last = 0 ,validfirst, validlast;
    
  int frames_acquired = -1;   
  int i; 
  int count = 1;
  FILE *ActionLogFile;

  int moteur;
  int CA_TOURNE = 1;
  int COUPEZ = 0;
    
  /////////////////////////////
    /* Initialization */
     
    if (StartAcquisition() != DRV_SUCCESS)
      {
        AbortAcquisition();
        iXon.movie_status = IDLE;
        win_report("Start acquisition error");
        iXon.is_live = FREEZE;
      } 
   
    moteur = CA_TOURNE;
    iXon.present_image_number_aquired_on_PC = 0;

    /////////////////////////////
      /* Acquisition loop */
      while (moteur == CA_TOURNE)
	{   
	  /* While there is no new data */                                 
	  while (GetNumberNewImages(&first,&last) == DRV_NO_NEW_DATA) 
	    {
	      //Sleep(0.2*iXon.exposure_time); /* Waits 1/5 th of the exposure time till data arrive */
	    }
	              
	  /* When new data arrive in the circular buffer */ 
	  for (i=0; i<last-first+1 ; i++)
	    {
	      /* Checks that not too much images have been acquired : 
		 during the 'while (iXon.present_image_number_aquired_on_PC <  iXon.number_images_in_movie)' loop, 
		 more than one images may be retrieved from camera. Then even if the inequality was true at the beginning of the loop, 
		 the number of acquired images might end up to be greater than the set limit,  which would cause an overflow in iXon_movie */
	      if (iXon.present_image_number_aquired_on_PC <  iXon.number_images_in_movie-1)
		{
		  iXon.present_image_number_aquired_on_PC += 1;
		  //win_event("Images acquises : %d/",iXon.present_image_number_aquired_on_PC+1,iXon.number_images_in_movie);
		  /* Transfers data into 'iXon_movie' */
		  GetImages16(first+i,first+i,iXon_movie->im.mem[iXon.present_image_number_aquired_on_PC], iXon.total_pixels_number, &validfirst, &validlast);
		}
	      else 
		{
		  //win_event("Enough images have been acquired : %d/%d",iXon.present_image_number_aquired_on_PC+1, iXon.number_images_in_movie);
		  /* Exiting when all images have been acquired */
		  moteur = COUPEZ;
		}
	    }
	      	      
	  /* Copies the image from the movie to oi_iXon and tests memcpy success */
	  if (oi_iXon->im.mem[0] != memcpy(oi_iXon->im.mem[0],iXon_movie->im.mem[iXon.present_image_number_aquired_on_PC],iXon.total_pixels_number*2)) 
	    {
	      return win_report("Error ! Bug in thread--iXon-RAM-movie : memcopy failed");
	    }
	  
	  
	  /* Refreshes screen periodically every iXon.refresh_period images taken */ 
	  //if  (iXon.present_image_number_aquired_on_PC % iXon.refresh_period == 0)
	  //{
	  //oi_iXon->need_to_refresh |= BITMAP_NEED_REFRESH;    
	  //refresh_image(imr_iXon, UNCHANGED);
	      //display_image_stuff_16M(imr_iXon,d_iXon);
	      
	      //imb = (BITMAP*)oi_iXon->bmp.stuff;
	      //acquire_bitmap(screen);
	      //blit(imb,screen,0,0,imr_iXon->x_off + d_iXon->x, imr_iXon->y_off - imb->h + d_iXon->y, imb->w, imb->h); 
	      //release_bitmap(screen);	
	      //}
	}
    
      /* Ends acquisition */
      AbortAcquisition();
      iXon.movie_status = IDLE;
    
      //win_report("aaaaand....... cut !");

      /*  Writes in log file */
      ActionLogFile = fopen__event();
      fprintf(ActionLogFile,"At : %s Ended movie acquisition\n",formatted_time());
      fprintf(ActionLogFile,"*****************************************\n");
      fclose(ActionLogFile);

      return D_O_K;
} 



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Initialization functions

/** Creates the low-priorityl thread that will switch iXon camera to standby mode after a specified timeout.

\param None.
\return int Error code.
\author Adrien Meglio
\version 21/09/07
*/ 
int launcher_iXon_standby_thread(void)
{
  HANDLE hThread = NULL;
  DWORD dwThreadId;

  iXon.is_live = LIVE;

  hThread = CreateThread( 
			 NULL,              // default security attributes
			 0,                 // use default stack size  
			 StandbyThreadProc,// thread function 
			 NULL,        // argument to thread function 
			 0,                 // use default creation flags 
			 &dwThreadId);      // returns the thread identifier 

  if (hThread == NULL) win_report("No thread created");
    
  SetThreadPriority(hThread, THREAD_PRIORITY_NORMAL);

  return D_O_K;
}

/** Creates the time-critical thread that will retriev the images taken by the iXon.

\param None.
\return int Error code.
\author JF Allemand
*/ 
int create_iXon_retrieve_images_thread(void)
{
  HANDLE hThread = NULL;
  DWORD dwThreadId;

  iXon.is_live = LIVE;

  hThread = CreateThread( 
			 NULL,              // default security attributes
			 0,                 // use default stack size  
			 RetrieveImages,// thread function 
			 NULL,        // argument to thread function 
			 0,                 // use default creation flags 
			 &dwThreadId);      // returns the thread identifier 

  if (hThread == NULL) win_report("No thread created");
    
  SetThreadPriority(hThread, THREAD_PRIORITY_TIME_CRITICAL);

  return D_O_K;
}

/** Display control thread.

Initializes the display control thread.
\param int None.
\return int Error code.
\author Adrien Meglio
\version 26/01/07
*/ 
int launcher__iXon_display_thread(void)
{
  HANDLE hThread = NULL;
  DWORD dwThreadId;

   hThread = CreateThread( 
			 NULL,              // default security attributes
			 0,                 // use default stack size  
			 StatusThreadProc,// thread function 
			 NULL,        // argument to thread function 
			 0,                 // use default creation flags 
			 &dwThreadId);      // returns the thread identifier 

  if (hThread == NULL) win_report("No thread created");

  SetThreadPriority(hThread,THREAD_PRIORITY_LOWEST);

  return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////

/** Movie acquisition thread.

Initializes the movie acquisition thread.
\param int None.
\return int Error code.
\author Adrien Meglio
*/ 
int launcher__iXon_RAM_movie(void)
{
  HANDLE hThread = NULL;
  DWORD dwThreadId;

  hThread = CreateThread( 
			 NULL,              // default security attributes
			 0,                 // use default stack size  
			 thread__iXon_RAM_movie,// thread function 
			 NULL,        // argument to thread function 
			 0,                 // use default creation flags 
			 &dwThreadId);      // returns the thread identifier 

  if (hThread == NULL) win_report("No thread created");
    
  SetThreadPriority(hThread,THREAD_PRIORITY_TIME_CRITICAL);

  return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////

#endif




