#ifndef _GLOBAL_DEFINE_H_
#define _GLOBAL_DEFINE_H_

/** A structure to store iXon parameters

\author Adrien Meglio
*/
typedef struct IXON_PARAM_STRUCT
{       
  int standby_timeout; // Time in seconds before switching camera to standby mode
  int standby_action; // Should the camera be switched into standby mode ?

  int refresh_period; //During live the image is refreshed every refresh_period
  float screen_frequency; // The target refresh frequency on the screen
  int present_image_number_aquired_on_PC; //most recent image with data downloaded

  int image_type; //type of image...should be IS_INT_IMAGE
        
  int aquisition_mode; //Single or run till abort....
  int image_mode;
  int transfer_mode; //to be in normal CCD collection or tranfer mode...ie no dead time
        
  long size_circular_buffer; //size of the buffer where images are stored before DMA transfer
        
  int pixels_x; //number of pixels in x on the CCD
  int pixels_y; //number of pixels in x on the CCD
  int bin_x; //binning in x
  int bin_y; //binning in y
  int subregion_x1; //end of horizontal position
  int subregion_x2; //beginning of horizontal position
  int subregion_y1; //end of horizontal position
  int subregion_y2; //beginning of horizontal position
  int total_pixels_number; //total number of pixels 
  int total_width; //number of pixels in x in the image
  int total_height; //number of pixels in y in the image
        
  int gain; // Current gain
  int gain_mode; 
  int gain_min; // Minimum available gain
  int gain_max; // Maximum available gain

  float exposure_time; // Exposure time of a frame
  float accumulation_time;
  float kinetic_time;
        
  int trigger_mode;
  int shutter_mode;
  int shutter_TTL;
  int open_shutter;
        
  int num_images;
  int frames_in_tape_buffer;
        
  int is_live; // Tells whether the camera currently displays live image
  /* CAUTION ! It is a switch that triggers LIVE mode */
        
  int is_live_previous; /* Tells the status of the camera just before 
			   Useful to stop the cam for an operation and then go back to the previous status */
        
  int movie_status; // Tells wether a movie is acquired or not
  /* CAUTION ! It is a switch that triggers movie recording */
        
  int number_images_in_movie; // Number of images to record in the movie
        
  int AD_channel;
  int is_electron_multiplication;
        
  float HSSpeed; /* Speed of horizontal readout */
  float VSSpeed; /* Speed of vertical readout */

  int temperature_target; /* Target temperature (int format) */
  int temperature_lastread; /* Last value of the temperature read from the camera */

  int contrast; /* Defines whether set_zmin_zmax will be applied automatically or not */ 

} IXON_PARAM;  

#define YES 1
#define NO 0

// Keyboard shortcuts (ixon.c)

#define SHORTCUT_IXON_Z_AUTO KEY_W /* Key "Z" on azerty keyboards */
#define SHORTCUT_IXON_PAUSE KEY_SPACE

// Conversion factor

#define MICRONS_PER_PIXEL .08

// DEFINEs for the iXon structure

#define WAKEUP 3
#define WAIT_FOR_WAKEUP 2
#define STANDBY 1

#define IS_ELECTRON_MULTIPLICATION 0
#define IS_CONVENTIONAL 1

#define FULL_IMAGE 512
#define IMAGE_512 512
#define IMAGE_256 256
#define IMAGE_128 128
#define IMAGE_64 64
#define IMAGE_32 32
#define IMAGE_16 16

#define IS_COOLER_ON 1
#define IS_COOLER_OFF 0
#define IS_COOLER_ERROR 2

#define OUT_OF_RANGE 1000

#define SINGLE_SCAN 1
#define ACCUMULATE 2
#define KINETICS 3
#define FAST_KINETICS 4
#define RUN_TILL_ABORT 5

#define MODE_TRANSFER 1
#define NO_MODE_TRANSFER 0

#define TTL_LOW 0
#define TTL_HIGH 0

#define INTERNAL 0
#define EXTERNAL 1

#define OPEN 1
#define CLOSED 0

#define GAIN_STANDARD 0
#define GAIN_EXTENDED 1
#define GAIN_LINEAR 2
#define GAIN_REAL 3

#define SHUTTER_AUTO 0
#define SHUTTER_OPEN 1
#define SHUTTER_CLOSE 2

#define BIN_1_1 0x00
#define BIN_2_2 0x11
#define BIN_4_4 0x33
#define BIN_8_8 0x77
#define BIN_16_16 0xFF

#define LIVE 1
#define FREEZE 0

#define TEMP_MINUS_70 -70
#define TEMP_0 0
#define TEMP_PLUS_20 20

#define IMAGE_MODE 4

#define IDT_TIMER1 100
#define SAVING_INTERVAL iXon.exposure_time*5

#define IDLE DRV_IDLE
#define ACQUIRING DRV_ACQUIRING

#define BLACK 0
#define BLUE 255
#define GREEN 65280
#define RED 16711680
#define WHITE 16777215
#define PINK 16737480

#define GREY_32 2105376
#define GREY_64 4210752
#define GREY_96 6316128
#define GREY_128 8421504
#define GREY_160 10526880
#define GREY_192 12632256
#define GREY_224 14737632

#define SMALLSIZE 8
#define MEDIUMSIZE 12
#define NORMALSIZE 16
#define BIGSIZE 24
#define HUGESIZE 36

#define INT_NULL -999999
#define FLOAT_NULL (float) INT_NULL
#define DOUBLE_NULL (double) INT_NULL
#define LONG_NULL (long) INT_NULL

#define AUTOMATIC 1
#define MANUAL 0

#endif
