/*******************************************************************************
                         LPS ENS
--------------------------------------------------------------------------------

  Program      : iXon_Source
  Author       : JFA,AM
  Date         : 10.2007

  Purpose      : Andor iXon for XVin
  ANDOR        : SDK 2.74
  Hints        : 
      
  Updates      : 
********************************************************************************/
#ifndef _IXON_SOURCE_C_
#define _IXON_SOURCE_C_
//#define XV_WIN32
#include "allegro.h"
#include "winalleg.h"


#include "ATMCD32D.h"//ANDOR SPECIFIC HEADER

#include "xvin.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */

#define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
#include "trackBead.h"
#include "im_fluo.h"
#include "global_define.h"
#include "global_variables.h"
#include "menu_ixon.h"
#include "global_functions.h"
#include "ixon.h"
//#include "XVCVB.h"
BOOL allegro_display_on = TRUE;
double	absolute_image_number = 0;
BOOL overlay = TRUE;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/** Declaration of the initial camera parameters.
\param none
\return 0
\author Adrien Meglio
\version 09/05/06
*/
int init_parameters(void)
{
  iXon.standby_action = STANDBY;
  iXon.standby_timeout = 300; /* In seconds */

  iXon.refresh_period = 1;
  iXon.screen_frequency = 50;
  iXon.present_image_number_aquired_on_PC = 0;

  iXon.image_type = IS_INT_IMAGE;
    
  iXon.aquisition_mode = RUN_TILL_ABORT;
  iXon.image_mode = IMAGE_MODE;
  iXon.transfer_mode = MODE_TRANSFER;  
    
  iXon.size_circular_buffer = 2;
     
  iXon.pixels_x = 512;
  iXon.pixels_y = 512;
  iXon.bin_x = 1;
  iXon.bin_y = 1;
  iXon.subregion_x1 = 1;
  iXon.subregion_x2 = 512;
  iXon.subregion_y1 = 1;
  iXon.subregion_y2 = 512;
  iXon.total_pixels_number = 512*512;
  iXon.total_width = 512;
  iXon.total_height = 512;
    
  iXon.gain = 1;//previously4095;
  iXon.gain_mode = GAIN_STANDARD;
  iXon.gain_min = 1;
  iXon.gain_max = 4095;

  iXon.exposure_time = 0.001;
  iXon.accumulation_time = 0;
  iXon.kinetic_time = 0;

  iXon.trigger_mode = INTERNAL;
  iXon.shutter_mode = SHUTTER_AUTO;
  iXon.shutter_TTL = TTL_HIGH;
  iXon.open_shutter = OPEN;

  iXon.num_images = 0;  
  iXon.frames_in_tape_buffer = 128; 

  iXon.is_live = LIVE;
  iXon.is_live_previous = LIVE;
    
  iXon.movie_status = IDLE;
  iXon.number_images_in_movie = 0;

  iXon.AD_channel = 0;
  iXon.is_electron_multiplication = IS_ELECTRON_MULTIPLICATION;

  iXon.HSSpeed = 5.0;
  iXon.VSSpeed = 1.0;
    
  iXon.temperature_target = 20;
  iXon.temperature_lastread = 20;
    
  iXon.contrast = AUTOMATIC;
    
  //win_printf_OK("iXon parameters are set to default value");
  return 0;
}


////////////////////////////////////////////////////////////////////////////////

/** Loads the camera.

\param none
\return Error code.
\author Adrien Meglio
\version 09/05/06
*/


int initialize_ANDOR(void)
{   
  static char ini_file_directory[512];
  if(updating_menu_state != 0) return D_O_K;

  strcpy(ini_file_directory,"C:/Program Files/Andor iXon/");
  if (Initialize(ini_file_directory) != DRV_SUCCESS) return win_printf_OK("iXon initialization error !");
  init_parameters();
  
  return D_O_K;    
} 


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/** Sets Xvin in real-time imaging mode with iXon.

\param None.
\return int Error code.
\author JF Allemand
*/ 
int iXon_real_time_imaging(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    if (SetEMCCDGain(iXon.gain) != DRV_SUCCESS) win_printf_OK("Could not set gain to min in function 'iXon-real-time-imaging' !");

    /* Launches the acquisition and display thread */
    create_fluo_thread(&fluo_dtid);
        
    return D_O_K;   
}   

////////////////////////////////////////////////////////////////////////////////

/** Stops iXon image acquisition.

\param None.
\return int Error code.
\author JF Allemand
*/ 
int Stop_acquisition(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    iXon.is_live_previous = iXon.is_live;
    iXon.is_live = FREEZE;
    
    Sleep(200); 
    /* Quite necessary. Following functions might be lured by the camera telling them
    it is still acquiring whereas it has just ceased to be. */

    /* Free data memory of the camera to ensure better efficiency */
    if (FreeInternalMemory() != DRV_SUCCESS) win_printf_OK("Could not set free internal memory in function 'Stop-acquisition' !");
    
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/**Allocate or reallocate and attach movie for iXon (if not already done ) to im
JF
*/
int add_ANDOR_oi_2_imr (imreg *imr,O_i* oi)
{
  //long lInfo = -1;
  //long pix;
  //int type;
  long lNf = 1;
  int xpixels,ypixels;

  if (GetSizeOfCircularBuffer(&lNf) != DRV_SUCCESS) return win_printf("Pb with Size of circular buffer");
  iXon.size_circular_buffer = lNf;
  if ( imr == NULL ) return win_printf_OK("IMR NULL");

  if (GetDetector(&xpixels,&ypixels) != DRV_SUCCESS) return win_printf("Error in detector");  
  if (iXon.pixels_x > xpixels || iXon.pixels_x < 0 || iXon.pixels_y > xpixels || iXon.pixels_y < 0)
    {
      iXon.pixels_x = xpixels;
      iXon.pixels_y = ypixels;
    }
  if (oi_FLUO == NULL)
    {
      oi = create_and_attach_movie_to_imr (imr , iXon.pixels_x , iXon.pixels_y , IS_UINT_IMAGE , (int)iXon.size_circular_buffer);
      if (oi == NULL) return win_printf("Pb in allocating iXon movie");
      set_oi_source(oi , "iXon");
      oi_FLUO = oi;
    }
  else alloc_one_image (oi_FLUO, iXon.pixels_x , iXon.pixels_x , IS_UINT_IMAGE);//adapt type for other types!!!!!!

  oi->width  = (float)xpixels/512;
  oi->height = (float)ypixels/512;

  oi->iopt2 &= ~X_NUM;		oi->iopt2 &= ~Y_NUM;		
  set_zmin_zmax_values(oi, 0, 65535);
  set_z_black_z_white_values(oi, 0, 65535);

  return D_O_K;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int init_image_source_for_fluo(void)
{
  imreg *imr; 

  imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL) return win_printf_OK("Patate t'es mal barre!");
  add_image_treat_menu_item ( "ANDOR", NULL, fluo_image_menu(), 0, NULL);

  initialize_ANDOR(); 
  add_ANDOR_oi_2_imr (imr,NULL);
  remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);

  return D_O_K;
}


///////////////////BOF la suite////////////////////
int kill_fluo_source(void)
{
  imreg *imr;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |= D_DISABLED;
      return D_O_K;	
    }
  //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
  else active_menu->flags &= ~D_DISABLED;
  if(updating_menu_state != 0)	return D_O_K;
  go_FLUO = FLUO_STOP;
  //REGARDER LE ROLE EXACT DE LA FONCTION!!!!!!!!!!!!!!!!!!!!!!!
  return D_O_K;
}
//////////////////////////////////////////////////

int freeze_fluo_video(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  allegro_display_on = FALSE;
  Sleep(100);
  if (AbortAcquisition() != DRV_SUCCESS <0) return win_printf_OK("Freeze failed");
  if (imr_FLUO != NULL)
    {
      oi_FLUO->need_to_refresh &= BITMAP_NEED_REFRESH;  
      find_zmin_zmax(oi_FLUO);
      refresh_image(imr_FLUO, UNCHANGED); 
    }
  return D_O_K;
}

int freeze_fluo_source(void)
{
  //imreg *imr;
	
  if(updating_menu_state != 0)	return D_O_K;
  freeze_fluo_video();
  return 0;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
Timer for imaging....this is the central function to keep in time
JF adapted from trackBead10/2007
*/

VOID CALLBACK FLUO_TimerAPCProc(LPVOID lpArgToCompletionRoutine,
			   DWORD dwTimerLowValue,  DWORD dwTimerHighValue)
{
  long long t;
  static unsigned long dt, dt_1 = 0, dt2;
  int im_n,  i = 0;
  //static int j=0;
  Fluo_tid *ltid;
  imreg *imr;
  DIALOG *d;
  O_i *oi;
  Fluo_Bid *p = NULL;
  int nf;
  int n_func;
  long lIndex = -1;
  long lFirst,lLast;

  ltid = (Fluo_tid*)lpArgToCompletionRoutine;

  imr = ltid->imr;
  d = ltid->dimr;
  oi = ltid->oi;
  p = ltid->dbfid;
  nf = abs(oi->im.n_f);
  n_func = p->n_functions;

  if (p == NULL) win_printf("MERDE");
  dt = my_uclock();
  im_n = p->previous_fr_nb + 1; // the next frame

  GetTotalNumberImagesAcquired(&lIndex);
  
  if (GetNumberNewImages(&lFirst, &lLast)!= DRV_NO_NEW_DATA) // we are in advance
    {
      while (GetOldestImage16(oi_FLUO->im.mem[((int)lIndex)%oi_FLUO->im.n_f] , iXon.total_pixels_number) != DRV_SUCCESS || DRV_P1INVALID ||DRV_P2INVALID); 
      p->previous_in_time_fr = t = my_ulclock();
      dt2 = my_uclock() - dt;
      p->previous_in_time_fr_nb = im_n;
    }
  else   
    {
      GetOldestImage16( oi_FLUO->im.mem[((int)lIndex)%oi_FLUO->im.n_f] , iXon.total_pixels_number);
      dt2 = 0;
    }
  
  p->timer_dt = dt2;
  p->previous_fr_nb = im_n;

  if ( p->n_functions == 0 || p->function == NULL ) 
    {
      win_printf("timer_do NULL");
      return;
    }
  for (i = 0 ; i < (int)(lFirst-lLast) ; im_n++, i++)
    {
      if (i)     GetOldestImage16(oi_FLUO->im.mem[((int)lIndex)%oi_FLUO->im.n_f] , iXon.total_pixels_number);
      
      switch_frame( oi_FLUO , ((int)lIndex)%oi_FLUO->im.n_f );
      
      t = my_ulclock();
      dt = my_uclock();
      p->previous_fr_nb = im_n;

      for ( i = 0 ; i < p->n_functions ; i++)
	{

	  p->function[i].timer_do(imr, oi,im_n, d, t, dt, p->function[i].function_param, i);
        }
    }

  dt_1 = my_uclock() - dt;
  return;
}
# define AcquisitionPeriodInHundredsOfNanoSeconds   \
(int)((((double)AcquisitionPeriod())*10000000)/get_my_ulclocks_per_sec())




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**Gets again the timing for iXon...required for the Timer of the thread
JF
*/
int MyAcquisitionPeriod_in_ms(void)
{
  //  long long t0;
  //long lTotalImageNumber = 0;
  //long lIndex = 0;
  float fExposure,fAccumulate,fKinetic;


  if (GetAcquisitionTimings(&fExposure, &fAccumulate, &fKinetic) != DRV_SUCCESS) return win_printf ("Failed in timing determination");

  return (int) fExposure; 
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**Thread that pilots the timer
JF Adapted from trackBead 10/2007
*/

DWORD WINAPI FluoThreadProc(LPVOID lpParam) 
{
  HANDLE hTimer = NULL;
  LARGE_INTEGER liDueTime;
  imreg *imr;
  int j, im_n;
  long long t;
  long long AcqPer;
  //  int AcqPerNano;
  Fluo_tid *lftid;
  DIALOG *d;
  O_i *oi;
  Fluo_Bid *p;
  long lImageNumber;
  //BITMAP* imb = NULL;
  long lFirst,lLast;
  
  if (imr_FLUO == NULL || oi_FLUO == NULL)	return 1;
  liDueTime.QuadPart=-300000; // 30 ms
  lftid = (Fluo_tid*)lpParam;

  imr = lftid->imr;
  d = lftid->dimr;
  oi = lftid->oi;
  p = lftid->dbfid;
  
    
  AcqPer = MyAcquisitionPeriod_in_ms();

  hTimer = CreateWaitableTimer(NULL, TRUE, "WaitableTimer");
  if (!hTimer)   win_printf_OK("CreateWaitableTimer failed (%d)\n", GetLastError());

  if (StartAcquisition() != DRV_SUCCESS) return win_printf_OK("Failure"); 

  GetTotalNumberImagesAcquired(&lImageNumber);
  p->first_im = p->previous_fr_nb = (int) lImageNumber;
  im_n = p->previous_fr_nb + 1;

  while( im_n > (int) lImageNumber)
    {
      GetTotalNumberImagesAcquired(&lImageNumber);
    }
  p->previous_in_time_fr = my_ulclock();
  p->previous_in_time_fr_nb = im_n;
    
  while (StopFluoThread != STOP_FLUO_THREAD)
    {
      im_n = p->previous_fr_nb + 1;
		
      if (GetNumberNewImages(&lFirst, &lLast) != DRV_NO_NEW_DATA)  // Images are stored in the buffer
	{
	  t = 500;
	  j = 0;
	}
      else
	{
	  j = im_n - p->previous_in_time_fr_nb;
	  t = j*AcqPer;
	  t -= (my_ulclock() - p->previous_in_time_fr);
	  t = (long long)((((double)t)*10000000)/get_my_ulclocks_per_sec());
	  t -= 10000; // we aim 1000 micro second before image flip
	  t = (t < 0) ? 1000 : t;
	  t = (t > 150000) ? 150000 : t;
    	}
	
      liDueTime.QuadPart= -t; // 30 ms

      // Set a timer to wait for 10 seconds.
      if (!SetWaitableTimer(hTimer, &liDueTime, 0, FLUO_TimerAPCProc, lpParam, 0))
	win_printf_OK("SetWaitableTimer failed (%d)\n", GetLastError());

      // Wait for the timer.
      SleepEx (INFINITE, TRUE);
    }
      
  win_printf_OK("Timer was signaled.\n");
  return 0;
}

int live_fluo_source(void)
{
  if(updating_menu_state != 0)	return D_O_K;

  //is_live  = 1;
  return create_fluo_thread(&fluo_dtid);	
}


MENU *ANDOR_image_menu(void)
{
  static MENU mn[32];
    
  if (mn[0].text != NULL)	return mn;
  // launch bead tracking in 2D or 3D
  add_item_to_menu(mn,"Init Andor", initialize_ANDOR ,NULL,0,NULL);
  add_item_to_menu(mn,"Live iXon", live_fluo_source ,NULL,0,NULL);
  add_item_to_menu(mn,"Freeze iXon", freeze_fluo_video ,NULL,0,NULL);
  //add_item_to_menu(mn,"Tape iXon", save_tape_mode ,NULL,0,NULL);
    
  return mn;
    
}


int iXon_source_main(void)
{
  imreg *imr = NULL;
  //pltreg *pr = NULL;
  //O_p *op;
  //double pixel = -1;
    
  initialize_ANDOR(); // Initializes CVB
  imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL) win_printf("Patate t'es mal barre!");
  //Sleep(500);
  //win_printf("Before CVB"); 
  imr_FLUO = imr;
  d_FLUO = find_dialog_associated_to_imr(imr_FLUO, NULL);
  if (imr_FLUO == NULL) win_printf("NO GOOD");
  add_ANDOR_oi_2_imr (imr_FLUO,NULL);
  //broadcast_dialog_message(MSG_DRAW,0);
  refresh_image(imr, UNCHANGED);
  add_image_treat_menu_item ( "ANDOR", NULL, ANDOR_image_menu(), 0, NULL); 
       
  return D_O_K;
}

/** Unload function for the  CVB.c menu.*/
int	iXon_source_unload(void)
{
  remove_item_to_menu(image_treat_menu, "ANDOR", NULL, NULL);
  return D_O_K;
}


  
#endif

