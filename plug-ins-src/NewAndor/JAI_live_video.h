#ifndef _JAI_SOURCE_H_
#define _JAI_SOURCE_H_


#define BIT_PER_PIX_8  0x01080001
#define BIT_PER_PIX_10  0x01100003
#define ACQUIRE_INFINITY 0xFFFFFFFFFFFFFFFF

#define D_BAD 1

typedef struct Treatement_struct
{
  void *pInfoBase;
  //int k;
  uint64_t lTimeStamp;
  //uint64_t lNumberImageDelivered;
} TreatementStruct;


# ifndef _JAI_SOURCE_C_
//extern  __declspec(dllexport) int go_live_video ;             // flag on means live_videoing
PXV_VAR(int, iImages_in_JAI_Buffer);
//PXV_VAR(int, do_refresh_overlay);
PXV_VAR(imreg  *,imr_JAI);
PXV_VAR(pltreg  *,pr_JAI);
PXV_VAR(O_i  *, oi_JAI);
PXV_VAR(DIALOG *, d_JAI);
PXV_VAR(BOOL , allegro_display_on);
PXV_VAR(BOOL, overlay);
PXV_VAR(int, do_refresh_overlay);
PXV_VAR(VIEW_HANDLE, hView);
PXV_VAR(FACTORY_HANDLE, hFactory);
PXV_VAR(CAM_HANDLE , hCamera);
PXV_VAR(J_STATUS_TYPE, retval);
PXV_VAR(void ** , JAI_Im_buffer_ID);
PXV_VAR(STREAM_HANDLE, hDS);
PXV_VAR(BOOL , bEnableThread);
PXV_VAR(HANDLE , hReadyEvent);
PXV_VAR(uint64_t, lNumberFramesDelivered);
PXV_VAR(uint64_t, lNumberFramestreated);

# endif
# ifdef _JAI_SOURCE_C_

uint64_t lNumberFramesDelivered = 0;   // written only by camera thread
uint64_t lNumberFramestreated = 0;     // written only by treatment thread

HANDLE hReadyEvent = NULL;
HANDLE hFinishEvent = NULL;

imreg   	*imr_JAI = NULL;
pltreg  	*pr_JAI = NULL;
O_i     	*oi_JAI = NULL;
DIALOG  	*d_JAI = NULL;
int iImages_in_JAI_Buffer = 128;
int 		num_image_JAI = -1;
BOOL 		allegro_display_on = TRUE;
BOOL 		overlay = TRUE;
//int 		do_refresh_overlay = 1;
int absolute_image_number = 0;
int lNumBuff;
VIEW_HANDLE     hView = NULL;    // Global view handle
FACTORY_HANDLE  hFactory  = NULL;     // Factory Handle
CAM_HANDLE      hCamera = NULL;      // Camera Handle
J_STATUS_TYPE   retval;
void **JAI_Im_buffer_ID = NULL;
STREAM_HANDLE hDS = NULL;
BOOL bEnableThread;
char win_title[512];




// Initialize all handles
HANDLE hStreamThread = NULL;
HANDLE hStreamEvent = NULL;
HANDLE hEventFreeze = NULL;

int iStreamChannel = 0;
HANDLE hEventKill = NULL;


void StreamProcess(void);
void Terminate_JAI_Thread(void);
void WaitForThreadToTerminate(void);
void CloseThreadHandle(void);
int add_oi_JAI_2_imr (imreg *imr,O_i* oi);
#endif
#endif
