/*
*    Plug-in module allowing to track bead from a movie
 *
 *    V. Croquette
 *    JF Allemand
  */
#ifndef _GAME_SOURCE_C_
#define _GAME_SOURCE_C_



# include "allegro.h"
# include "winalleg.h"
# include <windowsx.h>
# include "ctype.h"
# include "xvin.h"
# include "../nrutil/nrutil.h"

XV_FUNC(int, do_load_im, (void));


# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../wlc/wlc.h"

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

//# include "movie_source.h"
# include "focus.h"
# include "trackBead.h"
# include "magnetscontrol.h"




/*
Pico_param.dna_molecule.dsDNA_molecule_extension;
Pico_param.dna_molecule.dsxi;

Pico_param.bead_param.radius;
*/


O_i *calibration_image = NULL;

float x_bead = 15, y_bead = 20, z_bead = 5, L0 = 5, z_obj = 0;
float  x_bn = 0, y_bn = 0, z_bn = 0, x_b0 = 15, y_b0= 20; // bead noise
float  x_bnm = 0, y_bnm = 0, z_bnm = 0; // noise averaged over one frame
float  x_bn2 = 0, y_bn2 = 0, z_bn2 = 0, x_b02 = 40, y_b02= 25; // bead noise
float  x_bn2m = 0, y_bn2m = 0, z_bn2m = 0; // noise averaged over one frame
int co_set = 0;
float co_dl_ld[2048];
float my_sqrt[2048];
float my_exp_1[512];

unsigned char im_grey_level = 128;
/*
float read_Z_value(void)
{
  return z_obj;
}
int set_Z_value(float z)
{
  z_obj = z;
  return 0;
}
*/



float DNA_extension(void)
{
  /*
  float x, y, T = 298;

  y = applied_force;
  y *= (Pico_param.dna_molecule.dsxi)/(1.38e-2 * T);
  x = extension_versus_alpha(y);
  return Pico_param.dna_molecule.dsDNA_molecule_extension * x;*/
  return mean_extension;
}


int freeze_source(void)
{
  imreg *imr;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }
  //else if (go_track == TRACK_FREEZE) active_menu->flags |=  D_DISABLED;
  else active_menu->flags &=  ~D_DISABLED;
  if(updating_menu_state != 0)	return D_O_K;
  go_track = TRACK_FREEZE;
  return D_O_K;
}


int live_source(void)
{
  imreg *imr;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |= D_DISABLED;
      return D_O_K;	
    }
  //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
  else active_menu->flags &= ~D_DISABLED;
  if(updating_menu_state != 0)	return D_O_K;
  go_track = TRACK_ON;
  return D_O_K;
}
int kill_source(void)
{
  imreg *imr;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |= D_DISABLED;
      return D_O_K;	
    }
  //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
  else active_menu->flags &= ~D_DISABLED;
  if(updating_menu_state != 0)	return D_O_K;
  go_track = TRACK_STOP;
  return D_O_K;
}

int prepare_image_overlay(O_i* oi)
{
  return 0;
}





int add_bead_talbot_image(O_i *oi,                         // the image to put the bead in 
			  float xcf, float ycf, float zcf, // bead position in microns
			  float dx,                        // the extend of one pixel in microns
			  float lambda,         // the light wavelentgth in microns
			  float l_d,            // the coherence decay length of light in microns
			  float Id,             // the ratio of diffuse light
			  float theta_max,      // the maximum angle that the light ray can have
			  int rc)               // the radius of the bead
{
  register int i, j;
  float x, y, x2, y2, z2, r, z, l, dl, It, Imin, Ic, pxl, rcp, lambda_100, l_d_64;
  int onx, ony, xci, yci, xis, xie, yis, yie, in;
  union pix  *pd;

  Imin = cos(M_PI*theta_max/180);
  if (oi == NULL) return 1;
  onx = oi->im.nx;   ony = oi->im.ny;


 if (co_set == 0)
   {
     for (i = 0; i < 512; i++) 
       my_exp_1[i] = exp(-((float)i)/64); 
     for (i = 0; i < 2048; i++) 
       co_dl_ld[i] = 2*cos((M_PI*(float)i)/100); 
     for (i = 0; i < 2048; i++)
       my_sqrt[i] = sqrt((float)i/4); 
   } 

  xci = (int)((xcf/dx)+.5);   
  xis = xci - 99;
  xis = (xis < 0) ? 0 : xis;
  xis = (xis < onx) ? xis : onx - 1;
  xie = xci + 100;
  xie = (xie < 0) ? 0 : xie;
  xie = (xie < onx) ? xie : onx;
  yci = (int)((ycf/dx)+.5);
  yis = yci - 99;
  yis = (yis < 0) ? 0 : yis;
  yis = (yis < ony) ? yis : ony - 1;
  yie = yci + 100;
  yie = (yie < 0) ? 0 : yie;
  yie = (yie < ony) ? yie : ony;

  rcp = rc*dx;
  lambda_100 = (float)100/lambda;
  l_d_64 = (float)64/l_d;
  z = zcf;
  z2 = 4*z*z; // 3*z*z is interesting
  for (i = yis, pd = oi->im.pixel; i < yie ; i++)
    {
      y = fabs(ycf - (i*dx));
      y2 = 4*y*y+0.5;
      for (j = xis; j< xie; j++)
	{
	  x = fabs(xcf - (j*dx));                 // x and y are from pixel to bead center in microns
	  //r = sqrt(x*x + y*y);                   // r is distance from pixel to bead center in microns
	  x2 = 4*x*x;
	  r = my_sqrt[(int)(x2 + y2)]; // r is distance from pixel to bead center in microns
	  //k = (int)(r/dx);                             // its integer part
	  Ic = (r < rcp) ? 0 : 1;                  //
	  //r = dx * r;                             // this is the distance in microns
	  //l = sqrt(z*z + r*r);
	  l = my_sqrt[(int)(z2+x2+y2)];  // this is the optical ray path
	  It = (l == 0) ? 1: fabs(z/l);           // Id*cos \theta, simulate the decay of diffusion   
	  It = (It < Imin) ? 0 : It;  
	  dl = l - fabs(z);                       // the difference of ray travel 
	  //It *= Id * exp(-dl/l_d);                // It now contains deifuse light level
	  It *= Id * my_exp_1[(int)((dl*l_d_64)+.5)];
	  pxl = Ic-It;
	  in = (int)(0.5+dl*lambda_100);
	  in = (in < 2048) ? in : 2047;
	  in = (in < 0) ? 0 : in;
	  It *= co_dl_ld[in];
	  //It *= 2 * cos(M_PI*dl/lambda);//co_dl_ld[in]; //cos(M_PI*dl/lambda);
	  pxl += It;
	  pd[i].ch[j] = (unsigned char)(pxl*130);
	}
    }
  return 0;
}







int add_bead_calib_image(O_i *oi,                         // the image to put the bead in 
			  float xcf, float ycf, float zcf, // bead position in microns
			  float dx,                        // the extend of one pixel in microns
			  O_i *oic)               // the bead calibration image
{
  register int i, j, k;
  float x, y, x2, y2, z2, r, z, dy, impf, fr;
  int onx, ony, xci, yci, xis, xie, yis, yie, ocnx, ocny, zc0, zc1;
  union pix  *pd, *pdc;

  if (oi == NULL) return 1;
  onx = oi->im.nx;   ony = oi->im.ny;
  ocnx = oic->im.nx;   ocny = oic->im.ny;
  dx = oic->dx;
  dy = oic->dy;

 if (co_set == 0)
   {
     for (i = 0; i < 512; i++) 
       my_exp_1[i] = exp(-((float)i)/64); 
     for (i = 0; i < 2048; i++) 
       co_dl_ld[i] = 2*cos((M_PI*(float)i)/100); 
     for (i = 0; i < 2048; i++)
       my_sqrt[i] = sqrt((float)i/4); 
   } 

  xci = (int)((xcf/dx)+.5);   
  xis = xci - ocnx/2;
  xis = (xis < 0) ? 0 : xis;
  xis = (xis < onx) ? xis : onx - 1;
  xie = xci + ocnx/2;
  xie = (xie < 0) ? 0 : xie;
  xie = (xie < onx) ? xie : onx;
  yci = (int)((ycf/dx)+.5);
  yis = yci - ocnx/2;
  yis = (yis < 0) ? 0 : yis;
  yis = (yis < ony) ? yis : ony - 1;
  yie = yci + ocnx/2;
  yie = (yie < 0) ? 0 : yie;
  yie = (yie < ony) ? yie : ony;

  z = (zcf < 0) ? 0 : zcf/dy; // the position in y in the calibration image
  zc0 = (int)z;
  fr = z - zc0;    // the fractional part
  zc0 = (zc0 < ocny) ? zc0 : ocny - 1;
  zc1 = zc0 + 1;
  zc1 = (zc1 < ocny) ? zc1 : ocny - 1;
  z2 = 4*z*z; // 3*z*z is interesting
  for (i = yis, pd = oi->im.pixel, pdc = oic->im.pixel; i < yie ; i++)
    {
      y = fabs(ycf - (i*dx));
      y2 = 4*y*y+0.5;
      for (j = xis; j< xie; j++)
	{
	  x = fabs(xcf - (j*dx));                 // x and y are from pixel to bead center in microns
	  //r = sqrt(x*x + y*y);                   // r is distance from pixel to bead center in microns
	  x2 = 4*x*x;
	  r = my_sqrt[(int)(x2 + y2)]; // r is distance from pixel to bead center in microns
	  r /= dx; // this is the distance in pixels 
	  k = (int)r;
	  if  (k < ocnx/2)
	    {
	      k += ocnx/2;
	      impf = (1-fr) * pdc[zc0].fl[k] + fr * pdc[zc1].fl[k];   
	      pd[i].ch[j] = (unsigned char)impf;
	    }
	  else continue; //pd[i].ch[j] = im_grey_level;
	}
    }
  return 0;
}




#define IA 16807
#define RIM 2147483647
//#define AM (1.0/RIM)
#define IQ 127773
#define IR 2836
#define MASK 123459876
long ran0(long idum) // Minimal random number generator of Park and Miller. 
                     // Returns a uniform random deviate between 0 and 2^31 - 1
{
  long k;
  idum ^= MASK;                 // XORing with MASK allows use of zero and other simple bit patterns for 
  k=(idum)/IQ;                  //idum.
  idum = IA*(idum-k*IQ)-IR*k;  // Compute idum=(IA*idum) % IM without over
  if (idum < 0) idum += RIM;     //flows by Schrage�s method.
                                //ans=AM*(*idum); Convert idum to a floating result.
  idum ^= MASK;                //Unmask before return.
  return idum;
}


long simulate_video_noise(O_i *oi, long idum)
{
  register int i, j;
  int onx, ony;
  long l = 0;

  if (oi == NULL)  return 1;
  onx = oi->im.nx; ony = oi->im.ny;
  for ( i = 0; i < ony; i++)
    {
      for ( j = 0; j < onx; j++)	    
	{
	  if (j%15 == 0) l = idum = ran0(idum);
	  oi->im.pixel[i].ch[j] = im_grey_level + ((l & 0x3)<<2);
	  l >>= 2;
	}
    }
  return idum;
}


VOID CALLBACK dummyAPCProc(LPVOID lpArgToCompletionRoutine,
  DWORD dwTimerLowValue,  DWORD dwTimerHighValue)
{
  return;
}



DWORD WINAPI TrackingThreadProc( LPVOID lpParam ) 
{
    HANDLE hTimer = NULL;
    LARGE_INTEGER liDueTime;
    tid *ltid;
    imreg *imr;
    DIALOG *d;
    O_i *oi;
    static long idum = 87;
    int nf, im_n = 0, i;
    unsigned long dt = 0, t0 = 0, t1 = 0, dtmax = 0;
    long long t;
    //double xnoisep, ynoisep, znoisep;
    DIALOG *starting_one = NULL;
    Bid *p;

    if (imr_TRACK == NULL || oi_TRACK == NULL)	return 1;
    liDueTime.QuadPart=-300000; // 30 ms
    ltid = (tid*)lpParam;

    imr = ltid->imr;
    d = ltid->dimr;
    oi = ltid->oi;
    p = ltid->dbid;
    nf = abs(oi->im.n_f);
    // Create a waitable timer.
    hTimer = CreateWaitableTimer(NULL, TRUE, "WaitableTimer2");
    if (!hTimer)   win_printf_OK("CreateWaitableTimer failed (%d)\n", GetLastError());
    t0 = my_uclock();
    starting_one = active_dialog; // to check if windows change
    source_running = 1;

    // Set a timer to wait for 10 seconds.
    if (!SetWaitableTimer(hTimer, &liDueTime, 0, dummyAPCProc, NULL, 0))
      win_printf_OK("SetWaitableTimer failed (%d)\n", GetLastError());

    while (go_track)
      {

	// Wait for the timer.
	SleepEx (INFINITE, TRUE);
	t1 = my_uclock();
	// Set a timer to wait for 10 seconds.
	if (!SetWaitableTimer(hTimer, &liDueTime, 0, dummyAPCProc, NULL, 0))
	  win_printf_OK("SetWaitableTimer failed (%d)\n", GetLastError());


	dt = t1 - t0;	    
	if (dt > dtmax) dtmax = dt;
	t0 = t1;
	if (go_track == TRACK_FREEZE) continue;
	oi->im.c_f++;
	if (oi->im.c_f >= nf) oi->im.c_f = 0;


	t0 = t1;
	if (IS_DATA_IN_USE(oi))   continue;
	SET_DATA_IN_USE(oi);
	dt_simul = my_uclock();
	//xnoisep = xnoise /oi->dx;
	//ynoisep = ynoise /oi->dy;

	idum = simulate_video_noise(oi, idum);
	for (i = 0, x_bnm = y_bnm = z_bnm = 0; i < 64; i++)
	  {
	    x_bn = x_bn * betax + (float)(xnoise *gasdev(&idum));
	    x_bnm += x_bn;
	    y_bn = y_bn * betay + (float)(ynoise *gasdev(&idum));
	    y_bnm += y_bn;
	    z_bn = z_bn * betaz + (float)(znoise *gasdev(&idum));
	    z_bnm += z_bn;
	  }
	x_bnm /= 64;
	y_bnm /= 64;
	z_bnm /= 64;

	//draw_bubble(screen, B_LEFT, 512, 100, "x %g y %g z %g",x_bnm,y_bnm,z_bnm);

	x_bead = x_b0 + x_bnm; 
	y_bead = y_b0 + y_bnm; 
	//z_bead = L0 + read_last_Z_value() + z_bn;  // z_obj
	z_bead = read_last_Z_value() + z_bnm - DNA_extension();  // z_obj

	//z_bead = L0 + z_obj + z_bn;  
	if (calibration_image == NULL)
	  add_bead_talbot_image(oi, x_bead, y_bead, z_bead, 0.1, 0.45, 1.5, 0.3, 70, 0);
	else add_bead_calib_image(oi, x_bead, y_bead, z_bead, 0.1, calibration_image);


	for (i = 0, x_bn2m = y_bn2m = z_bn2m = 0; i < 64; i++)
	  {
	    x_bn2 = x_bn2 * betax + (float)(xnoise *gasdev(&idum));
	    x_bn2m += x_bn2;
	    y_bn2 = y_bn2 * betay + (float)(ynoise *gasdev(&idum));
	    y_bn2m += y_bn2;
	    z_bn2 = z_bn2 * betaz + (float)(znoise *gasdev(&idum));
	    z_bn2m += z_bn2;
	  }
	x_bn2m /= 64;
	y_bn2m /= 64;
	z_bn2m /= 64;


	//x_bn2 = 0.95* x_bn2 + 0.2*gasdev(&idum);
	x_bead = x_b02;// + x_bn2m; 
	//y_bn2 = 0.95* y_bn2 + 0.2*gasdev(&idum);
	y_bead = y_b02;// + y_bn2m; 
	//z_bn2 = 0.9* z_bn2 + 0.01*gasdev(&idum);
	//z_bead = L0 + read_last_Z_value() + z_bn2;  // z_obj
	//z_bead = DNA_extension() + read_last_Z_value() + z_bn2m;  // z_obj
	//z_bead = L0 + z_obj + z_bn2;  
	z_bead = read_last_Z_value();  
	if (calibration_image == NULL)
	  add_bead_talbot_image(oi, x_bead, y_bead, z_bead, 0.1, 0.45, 1.5, 0.3, 70, 0);
	else add_bead_calib_image(oi, x_bead, y_bead, z_bead, 0.1, calibration_image);


	SET_DATA_NO_MORE_IN_USE(oi);
	dt_simul = my_uclock() - dt_simul;
	oi->need_to_refresh |= BITMAP_NEED_REFRESH;
	//display_image_stuff_16M(imr,d); done idle refresh
	//sprintf(running_message,"simul %6.3f ms",1000*(double)(dt_simul)/get_my_uclocks_per_sec());

	sprintf(running_message,"x_bnm %g",x_bnm);
	t0 = my_uclock();
	d->d1 = (int)dt;


	t = my_ulclock();
	dt = my_uclock();
	bid.previous_fr_nb = im_n;      
	for (p->timer_do(imr_TRACK, oi_TRACK, im_n, d_TRACK, t ,dt ,p->param, 0) ; 
	   p->next != NULL && p->next->timer_do != NULL; p = p->next)
	  p->next->timer_do(imr_TRACK, oi_TRACK,im_n, d_TRACK, t, dt, p->next->param, 0);
	p = ltid->dbid;
	im_n++;      
	oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;

      }
    source_running = 0;
    return 0;
}

int dna_ext(void)
{
  if(updating_menu_state != 0)	      return D_O_K;	
  win_printf("Force %f \nextension %f",applied_force,DNA_extension());
  return D_O_K;
}


/*
MENU *simulation_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Move up", move_obj_up,NULL,0,NULL);
	add_item_to_menu(mn,"Move down", move_obj_dwn,NULL,0,NULL);
	add_item_to_menu(mn,"DNA extension", dna_ext,NULL,0,NULL);
	return mn;
}
*/

int init_image_source(void)
{
  register int i, j = 0, k;
  imreg *imr;
  O_i *oi;
  int ony = 756, onx = 572, onx2;
  long idum = 45, l = 0;
  float dx = 0.1, tmp;
  char *cs = NULL;


  // add_image_treat_menu_item ( "Simulation", NULL, simulation_image_menu(), 0, NULL);  
  if ((imr = find_imr_in_current_dialog(NULL)) == NULL)
    {
      imr = create_and_register_new_image_project( 0,   32,  900,  668);
      j = 1;
    }
  else
    {
      for(i = 0; i < imr->n_oi; i++)
	{
	  if (imr->o_i[i] == NULL) continue;
	  cs = (imr->o_i[i]->im.source != NULL) ? imr->o_i[i]->im.source 
	    : imr->o_i[i]->im.treatement; 
	  if (cs == NULL) continue;
	  if (strncmp(cs,"equally spaced reference profile",32) == 0)
	    {
	      calibration_image = imr->o_i[i];
	      onx2 = imr->o_i[i]->im.nx - 1;
	      for (k = 0, tmp = 0; k < imr->o_i[i]->im.ny; k++)
		tmp += imr->o_i[i]->im.pixel[k].fl[onx2];
	      tmp /= imr->o_i[i]->im.ny;
	      im_grey_level = (unsigned char)(tmp+0.5);
	      //win_printf_OK("tmp %f grey %d",tmp,(int)im_grey_level);
	    }
	}
    }
  if (imr == NULL)	win_printf_OK("could not create imreg");		

  oi = create_and_attach_oi_to_imr(imr, onx, ony, IS_CHAR_IMAGE);
  if (oi == NULL)	return win_printf_OK("cannot create profile !");
  if (j == 1)  remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
  select_image_of_imreg(imr, imr->n_oi -1);
  oi->im.source = strdup("Dummy beads");
  for ( i = 0; i < ony; i++)
    {
      for ( j = 0; j < onx; j++)	    
	{
	  if (j%15 == 0) l = idum = ran0(idum);
	  oi->im.pixel[i].ch[j] = im_grey_level + (l & 0x3);
	  l >>= 2;
	}
    }
  oi->width = (float)onx/512;
  oi->height = (float)ony/512;
  oi->iopt2 &= ~X_NUM;		oi->iopt2 &= ~Y_NUM;		
  x_bead = 20; y_bead = 15; z_bead = 2;
  add_bead_talbot_image(oi, x_bead, y_bead, z_bead, 0.1, 0.6, 3, 0.1, 70, 0);
  //  find_zmin_zmax(oi);	
  if (calibration_image)
    {
      set_zmin_zmax_values(oi, calibration_image->z_min, calibration_image->z_max);
      set_z_black_z_white_values(oi, calibration_image->z_black, calibration_image->z_white);
    }
  else
    {
      set_zmin_zmax_values(oi, 50, 150);
      set_z_black_z_white_values(oi, 50, 150);
    }
  oi->need_to_refresh = ALL_NEED_REFRESH;	
  create_attach_select_x_un_to_oi(oi, IS_METER, 0, dx, -6, 0, "\\mu m");
  create_attach_select_y_un_to_oi(oi, IS_METER, 0, dx, -6, 0, "\\mu m");
  // select_image_of_imreg(imr, imr->n_oi -1);
  //broadcast_dialog_message(MSG_DRAW,0);    
  //switch_project_to_this_imreg(imr);
  broadcast_dialog_message(MSG_DRAW,0);    
  //  refresh_image(imr, imr->n_oi - 1);
  //  starting_one = active_dialog; // to check if windows change
  //index_menu_dialog = retrieve_item_from_dialog(d_menu_proc, NULL);
  //index_menu_dialog = 1;
  return 0;
}
int start_data_movie(imreg *imr)
{
  return 0;
}


# endif
