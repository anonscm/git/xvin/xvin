#ifndef _ANDOR_IMAGE_SIZE_UTIL_C
#define  _ANDOR_IMAGE_SIZE_UTIL_C

#include "allegro.h"
#include "winalleg.h"


#include "ATMCD32D.h"//ANDOR SPECIFIC HEADER

#include "xvin.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */

#define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
#include "trackBead.h"
#include "im_fluo.h"
#include "global_define.h"
#include "global_variables.h"
#include "menu_ixon.h"
#include "global_functions.h"
#include "Andor_utils.h"
#include "ANDOR_live_video.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Image size functions

/** Function to update the unit sets when they might have been modified.

Changes the unit sets according to the values stored in Andor.
\param O_i* oi The oi.
\return int Errot code.
\author Adrien Meglio
@version 10/05/06
*/
int update_unit_sets(O_i* src_oi)
{
    if(updating_menu_state != 0) return D_O_K;
        
    //win_report("Unit sets update");
    
    if (src_oi == NULL) return D_O_K;
    
    /* Sets the unit sets */
    if (src_oi->n_xu == 1 || src_oi->n_yu == 1)
    {
        create_and_attach_unit_set_to_oi(src_oi,IS_METER,0,MICRONS_PER_PIXEL*Andor.bin_x,-6,0,"\\mu m",IS_X_UNIT_SET); 
        set_oi_x_unit_set(src_oi,src_oi->n_xu-1);
        create_and_attach_unit_set_to_oi(src_oi,IS_METER,0,MICRONS_PER_PIXEL*Andor.bin_y,-6,0,"\\mu m",IS_Y_UNIT_SET);
        set_oi_y_unit_set(src_oi,src_oi->n_yu-1);
    }    
    else 
    {
        set_unit_increment(src_oi->yu[src_oi->n_yu-1],MICRONS_PER_PIXEL*Andor.bin_y); 
        set_unit_increment(src_oi->xu[src_oi->n_xu-1],MICRONS_PER_PIXEL*Andor.bin_x);    
    }  
    
    //win_report("Space unit sets done");
        
    if (src_oi->n_tu == 1) 
        {
            create_and_attach_unit_set_to_oi(src_oi,IS_SECOND,0,Andor.exposure_time,-3,0,"ms",IS_T_UNIT_SET); 
            set_oi_t_unit_set(src_oi,src_oi->n_tu-1);
        }
    else set_unit_increment(src_oi->tu[src_oi->n_tu-1],Andor.exposure_time);
    
    //win_report("Time unit sets done");
    /* */
    
    return D_O_K;
}

/** Clean function to set image size and binning

Sets image size and binning, changes the global Andor variables if successful and recalculates
image width, height and total pixels number
\param Same as SetImage.
\return unsigned int SetImage error code.
\author Adrien Meglio
*/
int set_image_size_on_Andor(int bin_x,int bin_y,int subregion_x1,int subregion_x2,int subregion_y1,int subregion_y2)
{
    unsigned int error_code; // The Andor-type error code returned by SetImage
    
    if(updating_menu_state != 0) return D_O_K;
    
    //win_report("<<set image size on ixon>>");
    
    /* Freezes the display */
    Stop_ANDOR_acquisition();     
    
    //win_report("<<set image size on ixon>> : stop acquisition passed");
    
    error_code = SetImage(bin_x,bin_y,subregion_x1,subregion_x2,subregion_y1,subregion_y2); 
    /* Changes the image parameters to the requested value
    NOTE : no error correction (like : is subregion_x1 < subregion_x2 ?) is performed at this point
    BEWARE : image begins at 1 and NOT at 0 */
    
    //win_report("<<set image size on ixon>> : SetImage passed");
    
    if (error_code != DRV_SUCCESS) // If the image settings have not been successfully changed
    {
      //error_report(error_code);
        return error_code;
    }
    
    /* The global variable Andor is refrshed */
    Andor.bin_x = bin_x;
    Andor.bin_y = bin_y;
    Andor.subregion_x1 = subregion_x1;
    Andor.subregion_x2 = subregion_x2;
    Andor.subregion_y1 = subregion_y1;
    Andor.subregion_y2 = subregion_y2;
        
    Andor.total_pixels_number = (Andor.subregion_x2 - Andor.subregion_x1 + 1)*(Andor.subregion_y2 - Andor.subregion_y1 + 1)/(Andor.bin_x*Andor.bin_y);
    Andor.total_width = (Andor.subregion_x2 - Andor.subregion_x1 + 1)/Andor.bin_x;
    Andor.total_height = (Andor.subregion_y2 - Andor.subregion_y1 + 1)/Andor.bin_y;
    /* */
    
    //win_report("<<set image size on ixon>> : Andor refresh passed");
    
    /* Timings will change when image size is changed */
    GetAcquisitionTimings(&Andor.exposure_time,&Andor.accumulation_time,&Andor.kinetic_time);
    
    //win_report("<<set image size on ixon>> : GetAcquisitionTimings passed");
    
    /* Unit sets update */
    update_unit_sets(oi_ANDOR);
       
    return error_code;
}

////////////////////////////////////////////////////////////////////////////////

/** Efficient way to set the binning using a single argument.

\param int bin Binning coded as a 2-digit hexadecimal number
\return int Error code.
\author Adrien Meglio
\version 09/05/06
*/
int set_binning(int bin)
{   
    if(updating_menu_state != 0) return D_O_K;
    
    // Freezes the display
    Stop_ANDOR_acquisition();
    
    /* Binning may span from 1 to 16 on each direction. So we may code it as a 2-digit hexadecimal number
    The first digit ("left" is x-binning and the second ("right") is y-binning */
    Andor.bin_x = (bin&0x0F)+1; 
    // Hexadecimal way to retrieve the left digit. Since F is 15 and 0 binning is useless, we add 1 to span the 1-16 range
    Andor.bin_y = (bin>>4)+1;
    // Hexadecimal way to retrieve the right digit. Since F is 15 and 0 binning is useless, we add 1 to span the 1-16 range
    
    //win_report("You requested %d x %d binning",Andor.bin_x,Andor.bin_y); 
    
    /* Prints into the log file */
    win_printf("Binning changed for : %d x %d",Andor.bin_x,Andor.bin_y);
    
    /* Reinitializes the display and camera settings (uses the new values of Andor.bin_x and bin_y) */
    //set_image_for_live();
    Set_ANDOR_parameters();
    
    /* Back to live */
    ANDOR_live();
    
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////

/** A zoom-out function.

Back to maximum image size.
\param None.
\return int Error code.
\author JF Allemand
@version 10/05/06
*/    
int zoom_out_Andor(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    Stop_ANDOR_acquisition();
       
    Andor.bin_x = 1;
    Andor.bin_y = 1;
    Andor.subregion_x1 = 1;
    Andor.subregion_x2 = Andor.pixels_x;
    Andor.subregion_y1 = 1;
    Andor.subregion_y2 = Andor.pixels_y;
    
    /* Prints into the log file */
    win_printf("Zoomed out and set binning to 1x1");
    
    /* Reinitializes the display and camera settings (uses the new values in Andor) */
    //set_image_for_live();
    Set_ANDOR_parameters();

    /* Back to live */
    ANDOR_live();
    
    return D_O_K;
}    

////////////////////////////////////////////////////////////////////////////////

/** A zoom-in function.

Defines a region of interest in a given image and resets image parameters to this new bounding box.
\param None.
\return int Error code.
\author JF Allemand
@version 09/05/06
*/
int zoom_in_Andor(void)
{
  int start_x; 
  int start_y;
  int present_x =0;
  int present_y=0;
  int start_mouse_x;
  int start_mouse_y;
  int dx;
  int dy;
  int ldx;
  int  ldy;
  int color;
  BITMAP* imb=NULL;
  
  if(updating_menu_state != 0) return D_O_K;
    
  if (imr_ANDOR == NULL)		return win_printf("Andor oi NULL");
	
  color = makecol(255, 64, 64); // Bright red	
		
  Stop_ANDOR_acquisition();
	
  /* Wait for mouse selection or escape */
  while (!( mouse_b & 1 ) || key[KEY_ESC])
    {
      Sleep(100); /* So as not to overuse the processor */
    }
	
  /* Escape */
  if (key[KEY_ESC])	
    {
      clear_keybuf();
      return D_REDRAWME;	//REFRESH
    }

  /* Copies the position of the mouse at the beginning of the rectangle */
  start_mouse_x = mouse_x;
  start_mouse_y = mouse_y;		
  /* Converts that position from screen in pixels units */
  start_x = (int) x_imr_2_imdata(imr_ANDOR, start_mouse_x);
  start_y = (int) y_imr_2_imdata(imr_ANDOR, start_mouse_y);

  ldx = 0;
  ldy = 0;
  while (mouse_b & 1)
    {
      if (key[KEY_ESC])	
	{
	  clear_keybuf();
	  return D_REDRAWME;	//REFRESH
	}

      dx = mouse_x - start_mouse_x;
      dy = mouse_y - start_mouse_y;
		
      if  (ldy != dy || ldx != dx)
	{
	  imb = (BITMAP*)oi_ANDOR->bmp.stuff;
	  blit(imb,screen,0,0,imr_ANDOR->x_off + d_ANDOR->x, imr_ANDOR->y_off - imb->h + d_ANDOR->y, imb->w, imb->h); 
	  rect(screen,start_mouse_x,start_mouse_y,start_mouse_x + dx,start_mouse_y + dy,color);		
	    
	  present_x = (int) x_imr_2_imdata(imr_ANDOR, mouse_x);
	  present_y = (int) y_imr_2_imdata(imr_ANDOR, mouse_y);	
	}
      else 
	{
	  Sleep(100); /* Avoids using too much processor time */
	}
      
      ldx = dx;
      ldy = dy;
    }
		
  //win_report("start_x %d \n start_y %d \n present_x %d \n present_y %d",start_x,start_y,present_x,present_y);
        
  /* Ensures a correct orientation of the subregion */
  Andor.subregion_x1 = (start_x < present_x) ? start_x : present_x;
  Andor.subregion_x2 = (start_x > present_x) ? start_x : present_x;
  Andor.subregion_y1 = (start_y < present_y) ? start_y : present_y;
  Andor.subregion_y2 = (start_y > present_y) ? start_y : present_y;
    
  /* Prints into the log file */
  win_printf("Zoomed to region x : %d - %d & y : %d - %d",Andor.subregion_x1,Andor.subregion_x2,Andor.subregion_y1,Andor.subregion_y2);
    
  /* Reinitializes the display and camera settings (uses the new values in Andor) */
  //set_image_for_live();
  Set_ANDOR_parameters();

  /* Back to live */
  ANDOR_live();
    
  return D_O_K;
}

#endif
