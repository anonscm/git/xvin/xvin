/** \file Andor_utils.c
    \brief Plug-in program for temperature control of Andor cameras.
    This is a subprogram that monitors temperature, displays related information and allows
    one to control it.
    \sa ixon.c
    \author A Meglio &JF Allemand
*/
#ifndef _ANDOR_UTILS_C_
#define _ANDOR_UTILS_C_ 

#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>
#include <stdlib.h>
#include <string.h>
#include "box2alfn.h"
#include "ctype.h"
#include "xvin.h"
#include "ATMCD32D.h"
#include "Andor_live_video.h"
#include "Andor_temperature_util.h"

#define BUILDING_PLUGINS_DLL

#include "Andor_utils.h"



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/** Declaration of the initial camera parameters.
\param none
\return 0
\author Adrien Meglio
\version 09/05/06
*/
int init_ANDOR_parameters(void)
{
  Andor.standby_action = STANDBY;
  Andor.standby_timeout = 300; /* In seconds */

  Andor.refresh_period = 1;
  Andor.screen_frequency = 50;
  Andor.present_image_number_aquired_on_PC = 0;

  Andor.image_type = IS_INT_IMAGE;
    
  Andor.aquisition_mode = RUN_TILL_ABORT;
  Andor.image_mode = IMAGE_MODE;
  Andor.transfer_mode = MODE_TRANSFER;  
    
  Andor.size_circular_buffer = 2;
     
  Andor.pixels_x = 512;
  Andor.pixels_y = 512;
  Andor.bin_x = 1;
  Andor.bin_y = 1;
  Andor.subregion_x1 = 1;
  Andor.subregion_x2 = 512;
  Andor.subregion_y1 = 1;
  Andor.subregion_y2 = 512;
  Andor.total_pixels_number = 512*512;
  Andor.total_width = 512;
  Andor.total_height = 512;
    
  Andor.gain = 1;//previously4095;
  Andor.gain_mode = GAIN_STANDARD;
  Andor.gain_min = 1;
  Andor.gain_max = 4095;

  Andor.exposure_time = 0.001;
  Andor.accumulation_time = 0;
  Andor.kinetic_time = 0;

  Andor.trigger_mode = INTERNAL;
  Andor.shutter_mode = SHUTTER_AUTO;
  Andor.shutter_TTL = TTL_HIGH;
  Andor.open_shutter = OPEN;

  Andor.num_images = 0;  
  Andor.frames_in_tape_buffer = 128; 

  Andor.is_live = LIVE;
  Andor.is_live_previous = LIVE;
    
  Andor.movie_status = IDLE;
  Andor.number_images_in_movie = 0;

  Andor.AD_channel = 0;
  Andor.is_electron_multiplication = IS_ELECTRON_MULTIPLICATION;

  Andor.HSSpeed = 5.0;
  Andor.VSSpeed = 1.0;
    
  Andor.temperature_target = 20;
  Andor.temperature_lastread = 20;
    
  Andor.contrast = AUTOMATIC;
    
  //win_printf_OK("ANDOR parameters are set to default value");
  return 0;
}




////////////////////////////////////////////////////////////////////////////////

/** Allows the user to set the Andor EMCCD gain.

\param None.
\return int Error code.
\author Adrien Meglio
@version 10/05/06
*/ 
int change_gain(void)
{
  int current_gain;
    int Andor_gain_target = Andor.gain;
    
    if(updating_menu_state != 0) return D_O_K;
           
    /* Freezes the display */
    Stop_ANDOR_acquisition();
              
    /* Retrieves the allowed gain range */   
    if (GetEMGainRange(&Andor.gain_min,&Andor.gain_max) != DRV_SUCCESS) return win_printf("Could not retrieve current gain range !");
    
    /* Retrieves the current gain */
    if (GetEMCCDGain(&current_gain) != DRV_SUCCESS) return win_printf("Could not retrive current gain !");
    
    /* Displays this information */
    win_printf("Current gain is %d \nRange spans from %d to %d",current_gain,Andor.gain_min,Andor.gain_max);  
    if (win_scanf("Gain target value : %d",&Andor_gain_target) == CANCEL) return D_O_K;;
    
    if (Andor_gain_target < Andor.gain_min) Andor_gain_target = Andor.gain_min;
    else if (Andor_gain_target > Andor.gain_max) Andor_gain_target = Andor.gain_max;
    
    /* Check compatibility of requested value. Not necessary since SetEMCCDGain would simply return an error */
    //if (Andor_gain_target > Andor.gain_max || Andor_gain_target < Andor.gain_min) return win_printf("Gain out of range %d:%d !",Andor.gain_min,Andor.gain_max);
    
    /* Tries to set gain to the specified value */
    if (SetEMCCDGain(Andor_gain_target) != DRV_SUCCESS) return win_printf("Could not set gain to specified value !");
    else Andor.gain = Andor_gain_target; // Refreshes the global variable Andor if successful
          
    /* Prints into the log file */
    win_printf("Changed gain to : %d",Andor.gain);
          
    /* Back to live */
    Stop_ANDOR_acquisition();
    //    Andor_real_time_imaging();
          
    return D_O_K;
}


/** Allows the user to set the Andor acquisition time.

\param None.
\return int Error code.
\author JF Allemand
@version 10/05/06
*/ 
int set_aq_time(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    /* Freezes the display */
    Stop_ANDOR_acquisition(); 
     
    win_scanf("Integration time %f",&Andor.exposure_time);
    
    SetExposureTime(Andor.exposure_time); /* Asks Andor to set exposure time as close as possible 
     to the requested value */
    
    GetAcquisitionTimings(&Andor.exposure_time,&Andor.accumulation_time,&Andor.kinetic_time); /*Retrieves the actual timings */
    
    /* Prints into the log file */
    win_printf("Changed acquision time to %f ms/frame",1000*Andor.exposure_time);
    
    /* Gets the ACTUAL exposure time (and other timings) matching the requested value */
    //set_image_for_live();
    Set_ANDOR_parameters();
    
    /* Back to live */
    ANDOR_live();

                
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////

/** Sets the vertical CCD speed to the maximum possible.

\param none
\return int Error code.
\author Adrien Meglio
\version 09/05/06
*/
int init_VSSpeed_to_max(void)
{   
    int index;
    float speed;
    
    if(updating_menu_state != 0) return D_O_K;
    
    GetFastestRecommendedVSSpeed(&index,&speed);
    SetVSSpeed(index);
    
    Andor.VSSpeed = speed;
    
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////

/** Sets the horizontal CCD speed to the maximum possible.

\param none
\return int Error code.
\author Adrien Meglio
@version 09/05/06
*/
int init_HSSpeed(void)
{
    float speed;
    
    if(updating_menu_state != 0) return D_O_K;
    
    SetHSSpeed(Andor.is_electron_multiplication,0);
    
    GetHSSpeed(Andor.AD_channel,Andor.is_electron_multiplication,0,&speed);
    
    Andor.HSSpeed = speed;
    
    return D_O_K;
} 

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// Base functions
      
/** Retrieves the \a imr (typically to get the film in the \a oi) in the current display */      
imreg* get_imr(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    imreg *imr = NULL;
    
    /* Finds the set of frames (i.e; the film) as loaded by the sif2gr function in the current window */
    imr = find_imr_in_current_dialog(NULL);
    
    return imr;
} 



////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Thread functions

////////////////////////////////////////////////////////////////////////////////

/** Standby thread.

After a specified timeout, the system sets the camera to standby mode, mainly by switching off the acquisition
and the gain amplifier.
\param int None.
\return int Error code.
\author Adrien Meglio
\version 21/09/07
*/ 
DWORD WINAPI StandbyThreadProc(LPVOID lpParam) 
{
  time_t begin_time;
  time_t current_time;

  time(&begin_time);
  time(&current_time);
  
  Andor.standby_action = STANDBY;

  while(((current_time-begin_time) < Andor.standby_timeout) && (Andor.standby_action == STANDBY))
    {
      Sleep(1000); /* Wait for 1 second */
      current_time = time(NULL);
      //win_event("%d < %d sec ? %d",(current_time-begin_time),Andor.standby_timeout,((current_time-begin_time) < Andor.standby_timeout));
    }
  
  if (Andor.standby_action == STANDBY)
    {
      win_printf("Stanby action not set for the moment");
      //standby_action(); /* What to do when timeout is reached */

      Andor.standby_action = WAIT_FOR_WAKEUP; /* Waits for an action to wake up */
    }

  //win_printf("Standby exited");
    
  return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////

/** General display thread main operation.

This function is periodically called by the display control thread. 
It monitors acquisition status and reports them to the screen.
\param int None.
\return int Error code.
\return Screen output of acquisition status.
\author Adrien Meglio
\version 26/01/07
*/ 
DWORD WINAPI StatusThreadProc(LPVOID lpParam) 
{
  int current_temperature;
  float pifoc_position;
  float t_position;
  float r_position;
  int line_status;
  float line_level;
  unsigned int temperature_error = GetTemperature(&current_temperature);
  BITMAP *temp_button;
    
  //  temp_button = new_button(2,40,3,"TEMPERATURE : -69 C ; TARGET : -70 C BLABLABLABLABLA");
    
  while(1)
    {
      SleepEx(30000,FALSE);
      temperature_error = GetTemperature(&current_temperature);
                  
      switch (temperature_error)
        {
	case DRV_NOT_INITIALIZED : 
	  textprintf_ex(temp_button,font,0,0,GREY_160,-1,"TEMPERATURE :");
	  textprintf_ex(temp_button,font,0,text_height(font),RED,-1,"ERROR"); 
	  return D_O_K; 
	  break;
	case DRV_ACQUIRING : 
	  textprintf_ex(temp_button,font,0,0,GREY_160,-1,"TEMPERATURE : %d C (LAST READ)",Andor.temperature_lastread);
	  textprintf_ex(temp_button,font,0,text_height(font),BLUE,-1,"ACQUIRING"); 
	  break;
	case DRV_ERROR_ACK : 
	  textprintf_ex(temp_button,font,0,0,GREY_160,-1,"TEMPERATURE :");
	  textprintf_ex(temp_button,font,0,text_height(font),RED,-1,"ERROR"); 
	  return D_O_K; 
	  break;
	case DRV_TEMP_OFF : 
	  Andor.temperature_lastread = current_temperature;
	  textprintf_ex(temp_button,font,0,0,GREY_160,-1,"TEMPERATURE : %d C",current_temperature);
	  textprintf_ex(temp_button,font,0,text_height(font),BLUE,-1,"TEMP CONTROL OFF"); 
	  break;
	case DRV_TEMP_NOT_REACHED : 
	  Andor.temperature_lastread = current_temperature;
	  textprintf_ex(temp_button,font,0,0,GREY_160,-1,"TEMPERATURE : %d C ; TARGET : %d C",current_temperature,Andor.temperature_target);
	  textprintf_ex(temp_button,font,0,text_height(font),PINK,-1,"NOT STABILIZED"); 
	  break;
	case DRV_TEMP_STABILIZED : 
	  Andor.temperature_lastread = current_temperature;
	  textprintf_ex(temp_button,font,0,0,GREY_160,-1,"TEMPERATURE : %d C",current_temperature);
	  textprintf_ex(temp_button,font,0,text_height(font),GREEN,-1,"STABILIZED"); 
        }
        
      if (Andor.contrast == AUTOMATIC)
        {
	  textprintf_ex(temp_button,font,0,2*text_height(font),GREY_160,-1,"AUTOMATIC CONTRAST ON ");
        } else
        {
	  textprintf_ex(temp_button,font,0,2*text_height(font),GREY_128,-1,"AUTOMATIC CONTRAST OFF");
        }
    }  
    
  destroy_bitmap(temp_button);    
    
  return D_O_K;
} 

////////////////////////////////////////////////////////////////////////////////

/** Retrieves the images taken by the ANDOR camera and stored in the hardware circular buffer.

This function is called by a time-critical thread ; it retrieves the images in the camera circular buffer and sends them to the software 
circular buffer to be displayed
.
\param LPVOID A handle (usually NULL).
\return int Error code.
\author Adrien Meglio
\version 05/09/07
*/ 
DWORD WINAPI RetrieveImages(LPVOID lpParam) 
{
  long first = -1 , last = -1 , validfirst, validlast;
  BITMAP * imb = NULL;
  int i;

  /* Creates the buffer if it doesn't exist */
  if (oi_buffer == NULL)
    {
      oi_buffer = create_one_image(Andor.total_width,Andor.total_height,Andor.image_type);
    }

  Andor.present_image_number_aquired_on_PC = -1;
  if (StartAcquisition() != DRV_SUCCESS)
    {
      AbortAcquisition();
      win_printf("Start acquisition error");
      Andor.is_live = FREEZE;
    } 

  while (Andor.is_live == LIVE)
    {
      /* While there is no new data */                                 
      while (GetNumberNewImages(&first,&last) == DRV_NO_NEW_DATA) ;
      {
	//Sleep(0.2*Andor.exposure_time); /* Waits 1/5 th of the exposure time till data arrive */
      }
        
      /* When new data arrive in the circular buffer, retrive images one by one, beginning with 'first'
	 which is "the first image to retrieve in buffer"
	 For an unknown reason, the following instruction is not working :
	 >GetImages16(first,last,oi_ANDOR->im.mem[0], Andor.total_pixels_number, &validfirst, &validlast); 
	 This is maybe because oi_ANDOR->im.mem[0] is only for ONE image */ 
      /* It is EXTREMELY IMPORTANT to retrieve ALL images in buffer before doing ANYTHING else ;
	 if not, display will be slowed down A LOT */
      for (i=0; i<last-first+1 ; i++)
	{
	  /* oi_buffer has just ONE image in it (mem[0]) : alwas the last acquired */
	  GetImages16(first+i,first+i,oi_buffer->im.mem[0], Andor.total_pixels_number, &validfirst, &validlast);
	  Andor.present_image_number_aquired_on_PC += 1;
	}
       
      /* Refreshes screen periodically every Andor.refresh_period images taken, but only if display is requested */ 
      if  ((Andor.present_image_number_aquired_on_PC % Andor.refresh_period == 0) && (flag__display_yes_no == YES))
	{
	  /* Copies the last acquired image into oi_ANDOR for display */
	  memcpy(oi_ANDOR->im.mem[0],oi_buffer->im.mem[0],Andor.total_pixels_number*2);

	  oi_ANDOR->need_to_refresh |= BITMAP_NEED_REFRESH;            
	  display_image_stuff_16M(imr_ANDOR,d_ANDOR);
	  
	  imb = (BITMAP*)oi_ANDOR->bmp.stuff;
	  acquire_bitmap(screen);
	  blit(imb,screen,0,0,imr_ANDOR->x_off + d_ANDOR->x, imr_ANDOR->y_off - imb->h + d_ANDOR->y, imb->w, imb->h); 
	  release_bitmap(screen);	
	}
      
      /* Automatically corrects the contrast of the display */ 
      if (Andor.contrast == AUTOMATIC)
        {
	  find_zmin_zmax(oi_ANDOR);
        }
    }  
      
  AbortAcquisition();
     
  return D_O_K;
}

DWORD WINAPI RetrieveImages_working_previous_verison(LPVOID lpParam) 
{
  long first = -1 , last = -1 , validfirst, validlast;
  BITMAP * imb = NULL;
  int count = 1;
  int i;
    
  Andor.present_image_number_aquired_on_PC = -1;
  if (StartAcquisition() != DRV_SUCCESS)
    {
      AbortAcquisition();
      //Andor.cam_status = IDLE;
      win_printf("Start acquisition error");
      Andor.is_live = FREEZE;
    } 

  while (Andor.is_live == LIVE)
    {
      /* While there is no new data */                                 
      while (GetNumberNewImages(&first,&last) == DRV_NO_NEW_DATA) ;
      {
	//Sleep(0.2*Andor.exposure_time); /* Waits 1/5 th of the exposure time till data arrive */
      }
        
      //if (initial_image == -1) initial_image = first;

      /* When new data arrive in the circular buffer, retrive images one by one, beginning with 'first'
	 which is "the first image to retrieve in buffer"
	 For an unknown reason, the following instruction is not working :
	 >GetImages16(first,last,oi_ANDOR->im.mem[0], Andor.total_pixels_number, &validfirst, &validlast); 
	 This is maybe because oi_ANDOR->im.mem[0] is only for ONE image */ 
      /* It is EXTREMELY IMPORTANT to retrieve ALL images in buffer before doing ANYTHING else ;
	 if not, display will be slowed down A LOT */
      for (i=0; i<last-first+1 ; i++)
	{
	  GetImages16(first+i,first+i,oi_ANDOR->im.mem[0], Andor.total_pixels_number, &validfirst, &validlast);
	  Andor.present_image_number_aquired_on_PC += 1;
	}
      //Andor.present_image_number_aquired_on_PC = last;
     
      /* Refreshes screen periodically every Andor.refresh_period images taken */ 
      if  ((Andor.present_image_number_aquired_on_PC % Andor.refresh_period == 0) && (flag__display_yes_no == YES))
	{
	  oi_ANDOR->need_to_refresh |= BITMAP_NEED_REFRESH;            
	  display_image_stuff_16M(imr_ANDOR,d_ANDOR);
	  
	  imb = (BITMAP*)oi_ANDOR->bmp.stuff;
	  acquire_bitmap(screen);
	  blit(imb,screen,0,0,imr_ANDOR->x_off + d_ANDOR->x, imr_ANDOR->y_off - imb->h + d_ANDOR->y, imb->w, imb->h); 
	  release_bitmap(screen);	
	}
      
      /* Automatically corrects the contrast of the display */ 
      if (Andor.contrast == AUTOMATIC)
        {
	  /* It's useless to adjust contrast for more images than the ones that are actually displayed */
	  if (count > Andor.refresh_period)
	    {
	      find_zmin_zmax(oi_ANDOR);
	      count = 1;
	    }
	  count++;
        }
    }  
      
  AbortAcquisition();
    
  return D_O_K;
}

/* //////////////////////////////////////////////////////////////////////////////// */

/* /\** Movie acquisition thread main operation. */

/* This function controls movie acquisition on RAM. */
/* \param int None. */
/* \return int Error code. */
/* \return none. */
/* \author Adrien Meglio */
/* \version 22/01/07 */
/* *\/ */
/* DWORD WINAPI thread__ANDOR_RAM_movie_old(LPVOID lpParam) */
/* { */
/*   ///////////////////////////// */
/*   // Variables declarations */
    
/*   long  initial_index = -1; // Frame indexes used in acquisition control */
/*   long first = 0 , last = 0 ,validfirst, validlast; */
    
/*   int frames_acquired = -1;     */
/*   FILE *ActionLogFile; */
    
/*   ///////////////////////////// */
/*     // Initialization */
     
/*     if (StartAcquisition() != DRV_SUCCESS) */
/*       { */
/*         AbortAcquisition(); */
/*         Andor.movie_status = IDLE; */
/*         win_printf("Start acquisition error"); */
/*         Andor.is_live = FREEZE; */
/*       }  */
   
/*     ///////////////////////////// */
/*       // Acquisition loop */
/*       for (frames_acquired = 0; frames_acquired < Andor.number_images_in_movie; frames_acquired++) */
/* 	{    */
/* 	  /\* While there is no new data *\/                                  */
/* 	  while (GetNumberNewImages(&first,&last) == DRV_NO_NEW_DATA)  */
/* 	    { */
/* 	      //draw_bubble(screen,0,450,175,"Image number %d/%d !!!%d ",first - initial_index,Andor.number_images_in_movie,frames_acquired); */
/* 	      //Sleep(0.2*Andor.exposure_time); /\* Waits 1/5 th of the exposure time till data arrive *\/ */
/* 	    } */
/* 	  if (initial_index == -1) initial_index = first; */
            
/* 	  /\* When new data arrive in the circular buffer *\/  */
/* 	  GetImages16(first,first,oi_ANDOR->im.mem[0], Andor.total_pixels_number, &validfirst, &validlast); */
/* 	  /\* Retrieves the "oldest" acquired-but-not-saved image of the circular buffer and copies it to oi_ANDOR *\/ */
                   
/* 	  /\* Copies the image from oi_ANDOR to the movie (and tests memcpy success) */
/* 	     The frame index is adapted to take into account the possibility that some frames  */
/* 	     have been skipped at the beginning of the film (i.e. initial-index != 1) *\/ */
/* 	  //if (ANDOR_movie->im.mem[first-initial_index] != memcpy(ANDOR_movie->im.mem[first-initial_index],oi_ANDOR->im.mem[0],Andor.total_pixels_number*2))  */
/* 	  if (ANDOR_movie->im.mem[frames_acquired] != memcpy(ANDOR_movie->im.mem[frames_acquired],oi_ANDOR->im.mem[0],Andor.total_pixels_number*2))  */
/* 	    { */
/* 	      return win_printf("Error ! Bug in thread--ANDOR-RAM-movie : memcopy failed"); */
/* 	    } */
                
/* 	  /\* For an unknown reason, the following two lines slow considerably acquisition : display takes hours to be done *\/ */
/* 	  //oi_ANDOR->need_to_refresh |= BITMAP_NEED_REFRESH;   */
/* 	  //refresh_image(imr_ANDOR, UNCHANGED); */
/* 	} */
        
/*       /\* When finished with acquisition : stop*\/ */
/*       /\*  Andor.movie_status = IDLE; *\/ */
    
/*       /\* Freezes the display *\/ */
/*       AbortAcquisition(); */
/*       Andor.movie_status = IDLE; */
    
/*       ActionLogFile = fopen__event(); */
/*       fprintf(ActionLogFile,"At : %s Ended movie acquisition\n",formatted_time()); */
/*       fprintf(ActionLogFile,"*****************************************\n"); */
/*       fclose(ActionLogFile); */

/*       return D_O_K; */
/* }  */

/* DWORD WINAPI thread__ANDOR_RAM_movie(LPVOID lpParam) */
/* { */
/*   ///////////////////////////// */
/*   // Variables declarations */
    
/*   long  initial_index = -1; // Frame indexes used in acquisition control */
/*   long first = 0 , last = 0 ,validfirst, validlast; */
    
/*   int frames_acquired = -1;    */
/*   int i;  */
/*   int count = 1; */
/*   FILE *ActionLogFile; */

/*   int moteur; */
/*   int CA_TOURNE = 1; */
/*   int COUPEZ = 0; */
    
/*   ///////////////////////////// */
/*     /\* Initialization *\/ */
     
/*     if (StartAcquisition() != DRV_SUCCESS) */
/*       { */
/*         AbortAcquisition(); */
/*         Andor.movie_status = IDLE; */
/*         win_printf("Start acquisition error"); */
/*         Andor.is_live = FREEZE; */
/*       }  */
   
/*     moteur = CA_TOURNE; */
/*     Andor.present_image_number_aquired_on_PC = 0; */

/*     ///////////////////////////// */
/*       /\* Acquisition loop *\/ */
/*       while (moteur == CA_TOURNE) */
/* 	{    */
/* 	  /\* While there is no new data *\/                                  */
/* 	  while (GetNumberNewImages(&first,&last) == DRV_NO_NEW_DATA)  */
/* 	    { */
/* 	      //Sleep(0.2*Andor.exposure_time); /\* Waits 1/5 th of the exposure time till data arrive *\/ */
/* 	    } */
	              
/* 	  /\* When new data arrive in the circular buffer *\/  */
/* 	  for (i=0; i<last-first+1 ; i++) */
/* 	    { */
/* 	      /\* Checks that not too much images have been acquired :  */
/* 		 during the 'while (Andor.present_image_number_aquired_on_PC <  Andor.number_images_in_movie)' loop,  */
/* 		 more than one images may be retrieved from camera. Then even if the inequality was true at the beginning of the loop,  */
/* 		 the number of acquired images might end up to be greater than the set limit,  which would cause an overflow in ANDOR_movie *\/ */
/* 	      if (Andor.present_image_number_aquired_on_PC <  Andor.number_images_in_movie-1) */
/* 		{ */
/* 		  Andor.present_image_number_aquired_on_PC += 1; */
/* 		  //win_event("Images acquises : %d/",Andor.present_image_number_aquired_on_PC+1,Andor.number_images_in_movie); */
/* 		  /\* Transfers data into 'ANDOR_movie' *\/ */
/* 		  GetImages16(first+i,first+i,ANDOR_movie->im.mem[Andor.present_image_number_aquired_on_PC], Andor.total_pixels_number, &validfirst, &validlast); */
/* 		} */
/* 	      else  */
/* 		{ */
/* 		  //win_event("Enough images have been acquired : %d/%d",Andor.present_image_number_aquired_on_PC+1, Andor.number_images_in_movie); */
/* 		  /\* Exiting when all images have been acquired *\/ */
/* 		  moteur = COUPEZ; */
/* 		} */
/* 	    } */
	      	      
/* 	  /\* Copies the image from the movie to oi_ANDOR and tests memcpy success *\/ */
/* 	  if (oi_ANDOR->im.mem[0] != memcpy(oi_ANDOR->im.mem[0],ANDOR_movie->im.mem[Andor.present_image_number_aquired_on_PC],Andor.total_pixels_number*2))  */
/* 	    { */
/* 	      return win_printf("Error ! Bug in thread--ANDOR-RAM-movie : memcopy failed"); */
/* 	    } */
	  
	  
/* 	  /\* Refreshes screen periodically every Andor.refresh_period images taken *\/  */
/* 	  //if  (Andor.present_image_number_aquired_on_PC % Andor.refresh_period == 0) */
/* 	  //{ */
/* 	  //oi_ANDOR->need_to_refresh |= BITMAP_NEED_REFRESH;     */
/* 	  //refresh_image(imr_ANDOR, UNCHANGED); */
/* 	      //display_image_stuff_16M(imr_ANDOR,d_ANDOR); */
	      
/* 	      //imb = (BITMAP*)oi_ANDOR->bmp.stuff; */
/* 	      //acquire_bitmap(screen); */
/* 	      //blit(imb,screen,0,0,imr_ANDOR->x_off + d_ANDOR->x, imr_ANDOR->y_off - imb->h + d_ANDOR->y, imb->w, imb->h);  */
/* 	      //release_bitmap(screen);	 */
/* 	      //} */
/* 	} */
    
/*       /\* Ends acquisition *\/ */
/*       AbortAcquisition(); */
/*       Andor.movie_status = IDLE; */
    
/*       //win_printf("aaaaand....... cut !"); */

/*       /\*  Writes in log file *\/ */
/*       ActionLogFile = fopen__event(); */
/*       fprintf(ActionLogFile,"At : %s Ended movie acquisition\n",formatted_time()); */
/*       fprintf(ActionLogFile,"*****************************************\n"); */
/*       fclose(ActionLogFile); */

/*       return D_O_K; */
/* }  */



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Initialization functions

/** Creates the low-priorityl thread that will switch ANDOR camera to standby mode after a specified timeout.

\param None.
\return int Error code.
\author Adrien Meglio
\version 21/09/07
*/ 
int launcher_ANDOR_standby_thread(void)
{
  HANDLE hThread = NULL;
  DWORD dwThreadId;

  Andor.is_live = LIVE;

  hThread = CreateThread( 
			 NULL,              // default security attributes
			 0,                 // use default stack size  
			 StandbyThreadProc,// thread function 
			 NULL,        // argument to thread function 
			 0,                 // use default creation flags 
			 &dwThreadId);      // returns the thread identifier 

  if (hThread == NULL) win_printf("No thread created");
    
  SetThreadPriority(hThread, THREAD_PRIORITY_NORMAL);

  return D_O_K;
}

/** Creates the time-critical thread that will retriev the images taken by the Andor.

\param None.
\return int Error code.
\author JF Allemand
*/ 
int create_ANDOR_retrieve_images_thread(void)
{
  HANDLE hThread = NULL;
  DWORD dwThreadId;

  Andor.is_live = LIVE;

  hThread = CreateThread( 
			 NULL,              // default security attributes
			 0,                 // use default stack size  
			 RetrieveImages,// thread function 
			 NULL,        // argument to thread function 
			 0,                 // use default creation flags 
			 &dwThreadId);      // returns the thread identifier 

  if (hThread == NULL) win_printf("No thread created");
    
  SetThreadPriority(hThread, THREAD_PRIORITY_TIME_CRITICAL);

  return D_O_K;
}

/** Display control thread.

Initializes the display control thread.
\param int None.
\return int Error code.
\author Adrien Meglio
\version 26/01/07
*/ 
int launcher__ANDOR_display_thread(void)
{
  HANDLE hThread = NULL;
  DWORD dwThreadId;

   hThread = CreateThread( 
			 NULL,              // default security attributes
			 0,                 // use default stack size  
			 StatusThreadProc,// thread function 
			 NULL,        // argument to thread function 
			 0,                 // use default creation flags 
			 &dwThreadId);      // returns the thread identifier 

  if (hThread == NULL) win_printf("No thread created");

  SetThreadPriority(hThread,THREAD_PRIORITY_LOWEST);

  return D_O_K;
}

/* //////////////////////////////////////////////////////////////////////////////// */

/* /\** Movie acquisition thread. */

/* Initializes the movie acquisition thread. */
/* \param int None. */
/* \return int Error code. */
/* \author Adrien Meglio */
/* *\/  */
/* int launcher__ANDOR_RAM_movie(void) */
/* { */
/*   HANDLE hThread = NULL; */
/*   DWORD dwThreadId; */

/*   hThread = CreateThread(  */
/* 			 NULL,              // default security attributes */
/* 			 0,                 // use default stack size   */
/* 			 thread__ANDOR_RAM_movie,// thread function  */
/* 			 NULL,        // argument to thread function  */
/* 			 0,                 // use default creation flags  */
/* 			 &dwThreadId);      // returns the thread identifier  */

/*   if (hThread == NULL) win_printf("No thread created"); */
    
/*   SetThreadPriority(hThread,THREAD_PRIORITY_TIME_CRITICAL); */

/*   return D_O_K; */
/* } */

/* //////////////////////////////////////////////////////////////////////////////// */




////////////////////////////////////////////////////////////////////////////////
// Housekeeping functions

/** Main function of \a Andor_acq_control.c . This function initializes the system, loads the menus. */
int	ANDOR_acq_main(void)
{   
    //atexit(Exit_Andor);
      
    return D_O_K;
}

/** Unload function for the \a Andor_acq_control.c menu.*/
int	ANDOR_acq_unload(void)
{
	return D_O_K;
}

#endif




