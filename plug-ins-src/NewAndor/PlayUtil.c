/** \file brown.c
    \brief Plug-in program for bead tracking 
    
    This is a subprogram that creates, displays and interfaces the menus with the plug-in functions.
    
    \author V. Croquette, J-F. Allemand
*/

#ifndef _PLAYUTIL_C_
#define _PLAYUTIL_C_

# include "allegro.h"
# include "winalleg.h"
# include "ctype.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "PlayItSam.h"
# include "playutil.h"
# include "action.h"
//# include "focus.h"






/** this function initialize the tracking info structure

This function is started as soon as the video acquisition is launch so that we can
check the timing

\param none
\return A tracking info structure.
\version 19/05/06
*/



int check_heap_raw(char *file, unsigned long line)
{
  int out;

  out = _heapchk();
  if (out != -2)  win_printf("File %s at line %d\nheap satus %d (-2 =>OK)\n",file,(int)line,out);
  return (out == -2) ? 0 : out;
}
	

int timing_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j, k;
  static int  immax = -1; // last_c_i = -1,
  d_s *dsx, *dsy, *dsz;
  int nf, last_fr = 0, lost_fr, lf;
  unsigned long   dtm, dtmax, dts;
  float   fdtm, fdtmax;
  long long last_ti = 0;

  if (track_info == NULL) return D_O_K;
  dts = get_my_uclocks_per_sec();
  dsx = find_source_specific_ds_in_op(op,"Timing rolling buffer");
  dsy = find_source_specific_ds_in_op(op,"Period rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Timer rolling buffer");
  if (dsx == NULL || dsy == NULL || dsz == NULL) return D_O_K;	
  /*
  if (sscanf(dsx->source,"X rolling buffer bead %d",&bead_num) != 1)
    return win_printf_OK("Cannot find bead number");
  */
  //if (track_info->n_b < 1)    return win_printf_OK("No bead to display");
  i = track_info->lc_i;
  if (track_info->limi[i] <= immax) return D_O_K;   // we are up todate
  if (track_info->local_lock & BUF_CHANGING) return D_O_K;   // we have to wait
  track_info->local_lock |= BUF_FREEZED; // we forbid buffer changes
  i = track_info->lc_i;
  immax = track_info->limi[i];               
  lf = immax - track_info->lac_i;
  if (track_info->lac_i > TRACKING_BUFFER_SIZE) 
    {
      nf = TRACKING_BUFFER_SIZE;
      j = track_info->lc_i + 1;  //last_c_i;
    }
  else
    {
      nf = track_info->lac_i-1;
      j = 1;
    }
  dsx->nx = dsx->ny = nf;
  dsy->nx = dsy->ny = nf - 1;
  dsz->nx = dsz->ny = nf;
  

  for (i = 0, lost_fr = 0, dtm = dtmax = 0, fdtm = fdtmax = 0, k = 0; i < nf; i++, j++, k++)
    {
      j = (j < TRACKING_BUFFER_SIZE) ? j : j - TRACKING_BUFFER_SIZE;
      dsx->xd[i] = track_info->limi[j];
      dsx->yd[i] = ((float)(1000*track_info->limdt[j]))/dts; 
      //dsx->yd[i] = track_info->limdt[j]; 
      dsz->xd[i] = 1000*(float)track_info->limt[j]/get_my_ulclocks_per_sec(); 
      dsz->yd[i] = track_info->limit[j];
      if (i > 0) 
	{
	  dsy->xd[i-1] = track_info->limi[j];
	  dsy->yd[i-1] = 1000*(double)(track_info->limt[j]-last_ti)/get_my_ulclocks_per_sec(); 
	  lost_fr += (track_info->limi[j] - last_fr) - 1;
	}
      last_ti = track_info->limt[j];
      last_fr = track_info->limi[j];
      dtm += track_info->limdt[j];
      fdtm += dsx->yd[i];
      dtmax = (track_info->limdt[j] > dtmax) ? track_info->limdt[j] : dtmax;
      fdtmax = (dsx->yd[i] > fdtmax) ? dsx->yd[i] : fdtmax;
    }

  op->need_to_refresh = 1; 
  if (k) set_plot_title(op, "\\stack{{%d fr position lac_i %d stat %d}{%d missed images}"
			"{\\pt8 dt mean %6.3f ms (%6.3f ms) max %6.3f (%6.3f)}}"
			,bid.first_im +track_info->lac_i 
			,immax,0,lf,fdtm/k,
			1000*((double)(dtm/k))/dts,
			fdtmax, 1000*((double)(dtmax))/dts);
  //,(int)dts,(int)track_info->limdt[1]);
  track_info->local_lock = BUF_UNLOCK; // we allow new buffer changes

  i = TRACKING_BUFFER_SIZE/4;
  j = ((int)dsx->xd[1])/i;
  op->x_lo = j*i;
  op->x_hi = (j+5)*i;
  set_plot_x_fixed_range(op);
  return refresh_plot(pr_TRACK, UNCHANGED);
}



int obj_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  static int last_c_i = -1, immax = -1;
  d_s *dsx;
  int nf;
  
  if (track_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"Obj. rolling buffer");
  if (dsx == NULL) return D_O_K;	


  i = track_info->c_i;
  if (track_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = track_info->imi[i];               
  nf = (track_info->ac_i > TRACKING_BUFFER_SIZE) ? TRACKING_BUFFER_SIZE : track_info->ac_i;
  nf = (nf > (TRACKING_BUFFER_SIZE>>1)) ? TRACKING_BUFFER_SIZE>>1 : nf;
  dsx->nx = dsx->ny = nf;
  last_c_i = (track_info->c_i > 0) ? track_info->c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? TRACKING_BUFFER_SIZE + j: j;
      j = (j < TRACKING_BUFFER_SIZE) ? j : TRACKING_BUFFER_SIZE;
      dsx->xd[i] = track_info->imi[j];
      dsx->yd[i] = track_info->obj_pos[j];
    }
  i = TRACKING_BUFFER_SIZE/8;
  j = ((int)dsx->xd[i])/i;
  op->x_lo = (j-1)*i;
  op->x_hi = (j+4)*i;
  set_plot_x_fixed_range(op);

  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d}{Obj start %g obj %g}}"
		 ,track_info->ac_i, track_info->z_obj_servo_start,
  		 track_info->obj_pos[last_c_i]);
  return refresh_plot(pr_TRACK, UNCHANGED);
}


int ask_to_update_menu(void)
{
  if (track_info == NULL)  return 1;
  last_im_who_asked_menu_update = track_info->imi[track_info->c_i];
  return 0;
}


// this running in the timer thread
int do_at_all_image(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow) 
{
  int cl, cw, cl2, ci, i, ci_1, c_b;
  int dis_filter = 0, *xi, *yi;
  int f_conv = 0;
  int page_n, i_page, abs_pos, status;
  float  *xf, *yf;
  BITMAP *imb = NULL;
  g_track *gt = NULL;
  static int rot_flag_im_start = 0;
  
  gt = (g_track*)p;

  gt->ac_i++;                     // we increment the image number and wrap it eventually
  ci_1 = ci = gt->c_i;
  ci++;
  ci = (ci < gt->n_i) ? ci : 0;


  gt->imi[ci] = n;                                 // we save image" number
  gt->imit[ci] = n_inarow;                                 
  gt->imt[ci] = t;                                 // image time
  gt->imdt[ci] = dt;
  gt->c_i = ci;

  if (oi->bmp.stuff != NULL) imb = (BITMAP*)oi->bmp.stuff;
  // we grab the video bitmap of the IFC image
  prepare_image_overlay(oi);


  i = find_remaining_action(n);
  gt->status_flag[ci] = gt->status_flag[ci_1];  
  gt->obj_pos_cmd[ci] = gt->obj_pos_cmd[ci_1];
  while ((i = find_next_action(n)) >= 0)
    {
      if (action_pending[i].type == MV_OBJ_ABS)
	{
	  //set_Z_value(action_pending[i].value);
	  gt->status_flag[ci] |= OBJ_MOVING;
	  gt->obj_pos_cmd[ci] = action_pending[i].value;
	}
      else if (action_pending[i].type == MV_OBJ_REL)
	{
	  gt->obj_pos_cmd[ci] = gt->obj_pos_cmd[ci_1] + action_pending[i].value;
	  //set_Z_value(gt->obj_pos_cmd[ci]);
	  gt->status_flag[ci] |= OBJ_MOVING;
	}
      action_pending[i].proceeded = 1;
    }

  if (fabs(gt->obj_pos_cmd[ci] - gt->obj_pos[ci_1]) > 0.005)
    {
      //gt->obj_pos[ci] = read_Z_value_accurate_in_micron();
      gt->status_flag[gt->c_i] |= OBJ_MOVING;
    }     
  else
    {
      gt->obj_pos[ci] = gt->obj_pos[ci_1];
      gt->status_flag[gt->c_i] &= ~OBJ_MOVING;
    }

  gt->imdt[ci] = my_uclock() - dt;
  if ((gt->local_lock & BUF_FREEZED) == 0)
    {
      gt->local_lock |= BUF_CHANGING;  // we prevent data changes
      for ( ;gt->lac_i < gt->ac_i; gt->lac_i++)
	{   // we update local buffer
	  i = gt->lac_i%TRACKING_BUFFER_SIZE;
	  gt->limi[i] = gt->imi[i];  
	  gt->limit[i] = gt->imit[i];  
	  gt->limt[i] = gt->imt[i];  
	  gt->limdt[i] = gt->imdt[i];
	  gt->lc_i = i;
	} 
      gt->local_lock = 0;
    }

  return 0; 
}
END_OF_FUNCTION(do_at_all_image)


int get_present_image_nb(void)
{
 return (track_info != NULL) ?  track_info->imi[track_info->c_i] : -1;
}                                 


g_track *creating_track_info(void)
{
  O_p *op;
  d_s *ds;

  if (track_info == NULL)
    {
      track_info = (g_track*)calloc(1,sizeof(g_track));
      if (track_info == NULL) return win_printf_ptr("cannot alloc track info!");	
      //starting_one = active_dialog; // to check if windows change
      track_info->user_tracking_action = NULL;

      // plots to check tracking
      op = pr_TRACK->o_p[0];
      set_op_filename(op, "Timing.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Timing rolling buffer");
      if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Period rolling buffer");
      if ((ds = create_and_attach_one_ds(op, TRACKING_BUFFER_SIZE, TRACKING_BUFFER_SIZE, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Timer rolling buffer");
      op->op_idle_action = timing_rolling_buffer_idle_action;

      track_info->m_i = track_info->n_i = TRACKING_BUFFER_SIZE;
      track_info->c_i = track_info->ac_i = track_info->lac_i = track_info->lc_i = 0;
      track_info->local_lock = 0;
      LOCK_FUNCTION(do_at_all_image);
      LOCK_VARIABLE(track_info);
      bid.param = (void*)track_info;
      bid.to_do = NULL;
      bid.timer_do = do_at_all_image;
    }
  return track_info;
}


int init_track_info(void)
{
  if (track_info == NULL) 
    {
      track_info = creating_track_info();
      m_action = 256;
      n_action = 0;
      action_pending = (f_action*)calloc(m_action,sizeof(f_action));
      if (action_pending == NULL)
	win_printf_OK("Could not allocate action_pendind!");
    }
  return 0;
}



int reset_mouse_rect(void)
{
    scare_mouse();    
    set_mouse_sprite(NULL);
    set_mouse_sprite_focus(0, 0);
    unscare_mouse();
    return 0;
}

int change_mouse_to_rect(int cl, int cw)
{
    BITMAP *ds_bitmap = NULL;
    int color, col;


    ds_bitmap = create_bitmap(cl+1,cw+1);
    col = Lightmagenta;
    color = makecol(EXTRACT_R(col), EXTRACT_G(col), EXTRACT_B(col));
    clear_to_color(ds_bitmap, color);

    color = makecol(255, 0, 0);                        

    line(ds_bitmap,0,0,cl,0,color);
    line(ds_bitmap,0,0,0,cw,color);
    line(ds_bitmap,0,cw,cl,cw,color);
    line(ds_bitmap,cl,0,cl,cw,color);
    scare_mouse();    
    set_mouse_sprite(ds_bitmap);
    set_mouse_sprite_focus(cl/2, cw/2);
    unscare_mouse();
    return 0;    
}


#endif
