/*******************************************************************************
                         LPS ENS
--------------------------------------------------------------------------------

  Program      : ANDOR SDK for bead live_videoing 
  Author       : A. Meglio & JFA
  Date         : 2009

  Purpose      : Andor Manager Functions for XVin
               : 
  Hints        : 
      
  Updates      : 
********************************************************************************/
#ifndef _ANDOR_LIVE_VIDEO_C_
#define _ANDOR_LIVE_VIDEO_C_

#include "allegro.h"
#include "winalleg.h"
#include "xvin.h"

/* If you include other plug-ins header do it here*/ 

#include "ATMCD32D.h"//ANDOR SPECIFIC HEADER

/* But not below this define */

# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

# include "fftbtl32n.h"
# include "fillibbt.h"
# include "PlayItSam.h"
//# include "track_util.h"
# include "ANDOR_utils.h"
# include "ANDOR_2_XVIN.h"
# include "Andor_image_size_util.h"
# include "ANDOR_live_video.h"

# include "../cfg_file/Pico_cfg.h"

//char title[512];
int k_test = 0;

int Delay_counter = 0;



int ANDOR_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// General camera setting functions

/** Proceeds to the camera parameters modification.

\input none
\return Error code.
\author Adrien Meglio
@version 09/05/06
*/
int Set_ANDOR_parameters(void)
{
    int VSSpeed_index;
        
    if(updating_menu_state != 0) return D_O_K;
 
    if (SetAcquisitionMode(Andor.aquisition_mode) != DRV_SUCCESS) return win_printf("Failed in setting acquisition mode");
    
    if (SetReadMode(Andor.image_mode) != DRV_SUCCESS) return win_printf("Failed in setting image mode");

    if (GetDetector(&Andor.pixels_x,&Andor.pixels_y) != DRV_SUCCESS) return win_printf("Could not read detector size !");

    if (set_image_size_on_Andor(Andor.bin_x,Andor.bin_y,Andor.subregion_x1,Andor.subregion_x2,Andor.subregion_y1,Andor.subregion_y2) != DRV_SUCCESS) return win_printf("Failed in setting image size");//modifier par la suite

    add_ANDOR_oi_2_imr ( imr_ANDOR , oi_ANDOR );//pas sur que cela soit deja cree

    if (SetFrameTransferMode(Andor.transfer_mode)!= DRV_SUCCESS) return win_printf("Failed in setting Transfer mode");

    if (GetEMGainRange(&Andor.gain_min,&Andor.gain_max) != DRV_SUCCESS) return win_printf("Could not retrieve current gain range !");
    Andor.gain = (Andor.gain > Andor.gain_max) ? Andor.gain_max : Andor.gain;
    if (SetEMCCDGain(Andor.gain) != DRV_SUCCESS) return win_printf("Failed in setting gain !");
    
    if (GetSizeOfCircularBuffer(&(Andor.size_circular_buffer)) != DRV_SUCCESS) return win_printf("Failed in getting circular buffer size");
    
    if (SetHSSpeed(Andor.is_electron_multiplication,0) != DRV_SUCCESS) return win_printf("Failed in setting HSSpeed !"); // Sets to max
    GetHSSpeed(Andor.AD_channel,Andor.is_electron_multiplication,0,&Andor.HSSpeed); // Retrieves HSSpeed value
    
    GetFastestRecommendedVSSpeed(&VSSpeed_index,&Andor.VSSpeed); // Retrieves max speed index and value
    if (SetVSSpeed(VSSpeed_index) != DRV_SUCCESS) return win_printf("Failed in setting VSSpeed !"); // Sets to max

    if (SetExposureTime(Andor.exposure_time) != DRV_SUCCESS) return win_printf("Failed in setting Exposure time");

    if (SetTriggerMode(Andor.trigger_mode) != DRV_SUCCESS) return win_printf("Failed in setting Trigger mode");

    if (GetAcquisitionTimings(&Andor.exposure_time,&Andor.accumulation_time,&Andor.kinetic_time) != DRV_SUCCESS) return win_printf("Failed in getting timings");  

    //    set_screen_refresh_freq(); // Adapts refreshing frequency to a predefined standard

    PrepareAcquisition();
    
    imr_ANDOR->screen_scale = PIXEL_1;
    set_oi_horizontal_extend(oi_ANDOR,1.0);//(float)Andor.total_width/512);
    set_oi_vertical_extend(oi_ANDOR,1.0);//(float)Andor.total_height/512);
    /* This makes the display size independent of Andor.total_width and total_height, 
    whatever the binning or the zoom level. If you prefer to take this into account replace 
    1.0 by (float)Andor.total_width/512) and (float)Andor.total_height/512) respectively. */
    oi_ANDOR->need_to_refresh |= ALL_NEED_REFRESH;
    broadcast_dialog_message(MSG_DRAW,0);
    return D_O_K;
}   

////////////////////////////////////////////////////////////////////////////////

//==============================================================////
// Stream Processing Function
//==============================================================////

void Andor_StreamProcess(void)
{
  float fExposure,fAccumulate,fKinetic;
  int status =-1;
  int temperature = 0;
  long  lImageNumber =-1, prev_fr_delivered,fr_delivered;
  long int first, last = 0;
  Bid *p = NULL;
  unsigned int result;
  long long t, dt;
  int i, im_n =0 ,imn =0;

  if (imr_LIVE_VIDEO == NULL || oi_LIVE_VIDEO == NULL)	return ;
  p = &bid;
  if (p == NULL) { win_printf("MERDE"); return;}
  p->first_im = 0;
  
  if (GetAcquisitionTimings(&fExposure, &fAccumulate, &fKinetic) != DRV_SUCCESS) 
    { 
      win_printf ("Failed in timing determination");
      return;
    }
  
  //  Pico_param.camera_param.camera_frequency_in_Hz = (float)1000/p->image_per_in_ms;  Frequence (int) fExposure; 
  //Rentrer la frequence
  // Start image acquisition
  if (StartAcquisition() != DRV_SUCCESS) 
    {
      win_printf_OK("Failure"); 
      return;
    } 
  Sleep(100);
  while(bEnableThread)
    {
      WaitForAcquisition();
      if (GetTemperature(&temperature) != DRV_ACQUIRING) win_printf("Acquisition not started");
      if (GetStatus(&status) != DRV_SUCCESS) win_printf("CAN NOT Communicate with te card"); 
      if (status != DRV_ACQUIRING)
	win_printf("Acquisistion stopped");
      
      GetTotalNumberImagesAcquired(&lImageNumber);
      if (p->first_im == 0) p->first_im = (int) lImageNumber;
      p->previous_fr_nb = (int) lImageNumber;
      
      result = GetNumberNewImages(&first, &last);
      if (DRV_SUCCESS != result && result != DRV_NO_NEW_DATA)
	{
	  win_printf_OK("Acquisition software problem");
	  return;
	}
      p->previous_in_time_fr_nb = (int) lImageNumber;//Ce ne doit etre que le buffer
      p->previous_in_time_fr = my_ulclock();
      p->previous_in_time_fr = t = my_ulclock();//modifier
      p->timer_dt = 0;//idem
      
      
      t = my_ulclock();// lTimeStamp
      dt = my_uclock();
      
      
      for (i = 0; (i<(int)(last-first+1)) && (DRV_NO_NEW_DATA != result); i++, lNumberFramestreated+=1)
	{
	  result = GetOldestImage16(oi_LIVE_VIDEO->im.mem[(int)lNumberFramestreated%oi_LIVE_VIDEO->im.n_f], (unsigned long)oi_LIVE_VIDEO->im.nx*oi_LIVE_VIDEO->im.ny);
	  switch_frame(oi_LIVE_VIDEO,(int)lNumberFramestreated%oi_LIVE_VIDEO->im.n_f);
	  t = my_ulclock();// lTimeStamp
	  dt = my_uclock();
	  p->previous_fr_nb = imn = (int)lNumberFramestreated;
	  
	  //  min_delay_camera_thread_in_ms = (1000*(double)t/get_my_ulclocks_per_sec()) 
	  // - (double)(llt-jai_origin)/50000;   
	  //if (min_delay_camera_thread_in_ms < p->min_delay_camera_thread_in_ms)
	  //  p->min_delay_camera_thread_in_ms = min_delay_camera_thread_in_ms;
	  
	  for (p->timer_do(imr_LIVE_VIDEO, oi_LIVE_VIDEO, im_n, d_LIVE_VIDEO, t ,dt ,p->param, last-first+1-i) ;
	       p->timer_do != NULL && p->next != NULL; p = p->next)
	    {
	      p->timer_do(imr_LIVE_VIDEO, oi_LIVE_VIDEO,im_n, d_LIVE_VIDEO, t, dt, p->param,  last-first+1-i);
	    }
	  
	  // lTimeStamp = my_ulclock();
	}
      prev_fr_delivered = fr_delivered;
      
    }

}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/**Allocate or reallocate and attach movie for ANDOR (if not already done ) to im
JF
*/
int add_ANDOR_oi_2_imr (imreg *imr,O_i* oi)
{
  //long lInfo = -1;
  //long pix;
  //int type;
  long lNf = 1;
  int xpixels,ypixels;

  if ( imr == NULL ){ win_printf("IMR NULL");  return 1;}

  //A FAIRE Pico_param.camera_param.camera_model = strdup(cname);

  if (GetSizeOfCircularBuffer(&lNf) != DRV_SUCCESS) return win_printf("Pb with Size of circular buffer");
    Andor.size_circular_buffer = lNf;
  if ( imr == NULL ) return win_printf_OK("IMR NULL");

  if (GetDetector(&xpixels,&ypixels) != DRV_SUCCESS) return win_printf("Error in detector");  
  if (Andor.pixels_x > xpixels || Andor.pixels_x < 0 || Andor.pixels_y > xpixels || Andor.pixels_y < 0)
    {
      Andor.pixels_x = xpixels;
      Andor.pixels_y = ypixels;
    }
  if (oi_ANDOR == NULL)
    {
      oi = create_and_attach_movie_to_imr (imr , Andor.pixels_x , Andor.pixels_y , IS_UINT_IMAGE , (int)Andor.size_circular_buffer);
      if (oi == NULL) return win_printf("Pb in allocating ANDOR image");
      set_oi_source(oi , "ANDOR");
      oi_ANDOR = oi;
    }

  else alloc_one_image (oi_ANDOR, Andor.pixels_x , Andor.pixels_x , IS_UINT_IMAGE);//adapt type for other types!!!!!!
  Pico_param.camera_param.nb_bits = 16;//A ecrire dans la config
  oi->width  = (float)xpixels/512;
  oi->height = (float)ypixels/512;

  oi->iopt2 &= ~X_NUM;		oi->iopt2 &= ~Y_NUM;		
  set_zmin_zmax_values(oi, 0, 65535);
  set_z_black_z_white_values(oi, 0, 65535);
  write_Pico_config_file();
  Close_Pico_config_file();
  return D_O_K;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/** Sets Xvin in real-time imaging mode with ANDOR.

\param None.
\return int Error code.
\author JF Allemand
*/ 
int ANDOR_real_time_imaging(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    //win_printf("Real-time imaging");
    
    if (SetEMCCDGain(Andor.gain) != DRV_SUCCESS) win_printf("Could not set gain to min in function 'iXon-real-time-imaging' !");
    
    /* Launches the acquisition and display thread */
    
    Create_ANDOR_Thread();
    return D_O_K;   
}   

////////////////////////////////////////////////////////////////////////////////

/** Stops iXon image acquisition.

\param None.
\return int Error code.
\author JF Allemand
*/ 
int Stop_ANDOR_acquisition(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    Andor.is_live_previous = Andor.is_live;
    Andor.is_live = FREEZE;
    bEnableThread = FALSE;

    
    Sleep(200); 
    /* Quite necessary. Following functions might be lured by the camera telling them
    it is still acquiring whereas it has just ceased to be. */

    /* Free data memory of the camera to ensure better efficiency */
    if (FreeInternalMemory() != DRV_SUCCESS) win_printf("Could not set free internal memory in function 'Stop-acquisition' !");
    
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
BOOL Create_ANDOR_Thread(void)
{	
  DWORD dwThreadId;
  
  // Is it already created?
  if(hAndor_Thread)
    return FALSE;
  bEnableThread = TRUE;
  hAndor_Thread = CreateThread( 
			       NULL,              // default security attributes
			       0,                 // use default stack size  
			       (LPTHREAD_START_ROUTINE)Andor_StreamProcess,// thread function 
			       NULL,       // argument to thread function 
			       0,                 // use default creation flags 
			       &dwThreadId);      // returns the thread identifier 
  if(hAndor_Thread == NULL) 
    {
      win_printf_OK("No thread created");
      return FALSE;
    }    
  SetThreadPriority(hAndor_Thread,  THREAD_PRIORITY_TIME_CRITICAL);
  return TRUE;
}





int initialize_ANDOR(void)
{   
  return 0;   

} 


int freeze_video(void)
{
  imreg *imr;
  if(updating_menu_state != 0)	return D_O_K;
  allegro_display_on = FALSE;


  Stop_ANDOR_acquisition();

  imr = find_imr_in_current_dialog(NULL);  
  imr->one_i->need_to_refresh &= BITMAP_NEED_REFRESH;  
  find_zmin_zmax(imr->one_i);
  refresh_image(imr, UNCHANGED); 
  return 0;
}


int freeze_source(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  freeze_video();
  return 0;
}

int ANDOR_live(void)
{
  int is_live = 0;
  if(updating_menu_state != 0)	return D_O_K;
  Set_ANDOR_parameters();
  Create_ANDOR_Thread();

  is_live  = 1;
  allegro_display_on = TRUE;

  return D_O_K;
}


///////////////////BOF la suite////////////////////
int kill_source(void)
{
  imreg *imr;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |= D_DISABLED;
      return D_O_K;	
    }
  //else if (go_live_video == LIVE_VIDEO_ON) active_menu->flags |=  D_DISABLED;
  else active_menu->flags &= ~D_DISABLED;
  if(updating_menu_state != 0)	return D_O_K;
  go_live_video = LIVE_VIDEO_STOP;
  return D_O_K;
}

int live_source(void)
{
  if(updating_menu_state != 0)	return D_O_K;


  return ANDOR_live();	
}


int start_data_movie(imreg *imr)
{
  return 0;
}


DWORD WINAPI LiveVideoThreadProc(LPVOID lpParam) 
{
  imreg *imr;
  tid *ltid;
  DIALOG *d;
  O_i *oi;
  Bid *p;
    
  if (imr_LIVE_VIDEO == NULL || oi_LIVE_VIDEO == NULL)	return 1;

  ltid = (tid*)lpParam;

  imr = ltid->imr;
  d = ltid->dimr;
  oi = ltid->oi;
  p = ltid->dbid;
 
  live_source();		

  return 0;
}

  
MENU *ANDOR_source_image_menu(void)
{
  static MENU mn[32];
    
  if (mn[0].text != NULL)	return mn;

  add_item_to_menu(mn,"Live ANDOR", ANDOR_live ,NULL,0,NULL);
  add_item_to_menu(mn,"Freeze ANDOR", freeze_video ,NULL,0,NULL);
    
  return mn;
    
}


int init_image_source(imreg *imr, int mode)
{
  int ret;
  O_i *oi = NULL;

  //if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
  if (imr == NULL)    imr = create_and_register_new_image_project( 0,   32,  900,  668);
  if (imr == NULL)    return win_printf_OK("Patate t'es mal barre!");
  imr_ANDOR = imr;
  oi = imr->one_i;
  init_ANDOR_parameters();
  Set_ANDOR_parameters();
  if ((ret = initialize_ANDOR())) 
    {
      win_printf_OK("ANdor camera did not start!");
      return ret; // Initializes JAI 
    }
  if (mode == 1)      remove_from_image (imr, oi->im.data_type, (void *)oi);
  add_image_treat_menu_item ( "ANDOR", NULL, ANDOR_source_image_menu(), 0, NULL);
  return 0;
}





////////////////////////////////////////////////////////////////////////////////////////////////////////
int ANDOR_source_main(void)
{
  imreg *imr = NULL;
  O_i *oi = NULL;
  
    

  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL)    init_image_source(NULL,0);
  else
    {
        if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
	  init_image_source(imr,0);
	if (oi->im.movie_on_disk == 0 || oi->im.n_f < 2)
	  init_image_source(imr,0);

    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    return win_printf_OK("You must load a movie in the active window");

  d_ANDOR = find_dialog_associated_to_imr(imr_ANDOR, NULL);
  before_menu_proc = ANDOR_before_menu;
  after_menu_proc = ANDOR_after_menu;
  oi_ANDOR->oi_got_mouse = ANDOR_oi_got_mouse;
  //  oi_JAI->oi_idle_action = JAI_oi_idle_action;
  oi_ANDOR->oi_mouse_action = ANDOR_oi_mouse_action;

  general_end_action = source_end_action;
  //  general_idle_action = source_idle_action;

   add_image_treat_menu_item ( "ANDOR", NULL, ANDOR_source_image_menu(), 0, NULL); 
       
  return D_O_K;
}

/** Unload function for the  JAI.c menu.*/
int	ANDOR_unload(void)
{
  remove_item_to_menu(image_treat_menu, "ANDOR", NULL, NULL);
  return D_O_K;
}



#endif

/*******************************************************************************/
