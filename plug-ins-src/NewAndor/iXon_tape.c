 /** \file ixon.c
    \brief Plug-in program for iXon movie control in Xvin.
    This is the master program that controls the iXon camera movie acquisition.
    \sa movie_statistics.c
    \author JF Allemand & A Meglio
*/
#ifndef _IXON_TAPE_C_
#define _IXON_TAPE_C_

#include "allegro.h"
#include "winalleg.h"
#include <windowsx.h>
#include "ctype.h"
#include "xvin.h"
#include "ATMCD32D.h"
#include "global_define.h"
#include "global_variables.h"
#include "global_functions.h"

//#include "iXon.h"
//#include "iXon_acq_control.h"

#define BUILDING_PLUGINS_DLL
#include "iXon_tape.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// RAM acquisition functions

/** Acquires a movie on RAM via a thread.

This new feature releases control to user when acquiring the movie 
(because it is acquired via a thread and not a function). It enables
the user to change acquisition parameters (such as PIFOC...) by launching 
functions during acquisition

BEWARE : this capability depends strongly on your RAM capacity ! 
Be sure to have enough RAM to save your film ; otherwise, 
the system will use swap ROM instead of RAM, causing a great 
decrease in acquisition speed !

\param None.
\return int Error code.
\author Adrien Meglio
\version 22/01/07
*/ 
int iXon_RAM_movie(void)
{
  /////////////////////////////
  // Variables declarations
    
  FILE *ActionLogFile;
  int line_status_1, line_status_2, line_status_3, line_status_4;
  float pifoc_percent;
  float pifoc_microns;

  /////////////////////////////
    // Initialization
    
    if(updating_menu_state != 0)	return D_O_K;
    
    /* Freezes the display */
    Stop_acquisition(); 
    
    /*Ensures that no movie is currently being acquired */
    if (iXon.movie_status == ACQUIRING)
      {
        iXon.movie_status = IDLE;
        win_report("Was already acquiring a movie !"); 
      }
    
    /////////////////////////////
      // Movie parameters
    
      if (win_scanf("How many images do you want to record ? %d",&iXon.number_images_in_movie) == CANCEL) return D_O_K;
    
      iXon_movie = create_and_attach_movie_to_imr(imr_iXon,iXon.total_width,iXon.total_height,IS_INT_IMAGE/*iXon.image_type*/,iXon.number_images_in_movie);
    
      if (iXon_movie == NULL) return win_report("Could not create iXon movie !");
    
      GetAcquisitionTimings(&iXon.exposure_time,&iXon.accumulation_time,&iXon.kinetic_time);
      set_oi_source(iXon_movie," iXon movie");

      /* Unit sets update */
      update_unit_sets(iXon_movie);  
    
      //iXon.cam_status = ACQUIRING;
      
      /*      Retrieves status */
      /*     IO__whatis__motor_p_position_in_microns(&pifoc_microns); */
      /*     IO__whatis__motor_p_position_in_percent(&pifoc_percent); */
      /*     IO__give_line_status(0,&line_status_1); */
      /*     IO__give_line_status(1,&line_status_2); */
      /*     IO__give_line_status(2,&line_status_3); */
      /*     IO__give_line_status(3,&line_status_4); */
      
      // Prints into the log file 
      
      ActionLogFile = fopen__event();
      fprintf(ActionLogFile,"*****************************************\n");
      fprintf(ActionLogFile,"At : %sBegan acquisition of a movie of %d frames at %f ms/frame\n",formatted_time(),iXon.number_images_in_movie,1000*iXon.exposure_time);
      //fprintf(ActionLogFile,"Parameters : PIFOC position %.3f �m (really %.1f/100)\n",pifoc_microns,pifoc_percent);
      fprintf(ActionLogFile,"Gain : %d\n", iXon.gain);
      fprintf(ActionLogFile,"Binning : %d x %d\n",iXon.bin_x,iXon.bin_y);
      fprintf(ActionLogFile,"Region x : %d - %d & y : %d - %d\n",iXon.subregion_x1,iXon.subregion_x2,iXon.subregion_y1,iXon.subregion_y2);
      //fprintf(ActionLogFile,"Laser lines status : 1->%d, 2->%d, 3->%d4->%d\n",line_status_1,line_status_2,line_status_3,line_status_4);
      fclose(ActionLogFile);
      
      iXon.movie_status = ACQUIRING;
      
      /* Once everything has been configured, the acquisition may be launched */
      launcher__iXon_RAM_movie(); // Opens the thread that enables movie acquisition on RAM
      
      return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////

/** Creates the adapted software circular buffer to handle images retrieved from the iXon.

\param None.
\return int Error code.
\author JF Allemand
\version 09/05/06
*/ 
int create_and_adapt_tape_iXon_buffer_image(void)
{
  if(updating_menu_state != 0)	return D_O_K;
	
  if (oi_iXon_tape == NULL)
    {
      if((oi_iXon_tape = (O_i *)calloc(1,sizeof(O_i))) == NULL)  return win_report("We are out of memory guy");
      if (init_one_image(oi_iXon_tape,iXon.image_type)) return win_report("You have a little problem guy");
      set_oi_source(oi_iXon_tape,"BUFFER TAPE iXon");
        
      /* Unit sets update */
      update_unit_sets(oi_iXon_tape);
    }
	
  else
    {
      duplicate_unit_set(oi_iXon_tape->xu[oi_iXon_tape->n_xu-1], oi_iXon->xu[oi_iXon->n_xu-1]);
      duplicate_unit_set(oi_iXon_tape->yu[oi_iXon_tape->n_yu-1], oi_iXon->yu[oi_iXon->n_yu-1]);
      duplicate_unit_set(oi_iXon_tape->tu[oi_iXon_tape->n_tu-1], oi_iXon->tu[oi_iXon->n_tu-1]);
    }    
	
  oi_iXon_tape->im.n_f = iXon.frames_in_tape_buffer;
  win_report("%d\n%d",iXon.total_width,iXon.total_height);
  if (alloc_one_image(oi_iXon_tape,iXon.total_width,iXon.total_height,iXon.image_type))
    return win_report("XVin merdouille");
	
  //draw_bubble(screen,0,550,250,"Tape buffer Allocation Performed");
  win_report("Tape buffer Allocation Performed");
	
  return 0;	
}

////////////////////////////////////////////////////////////////////////////////
/** A timer killer function. 

Specific to this application.
\param None.
\return int Error code.
\author JF Allemand
*/ 
int Kill_timer(void)
{
  if (KillTimer(win_get_window(),25) == FALSE) return win_report("Timer not killed");  //stops the background task 
    
  draw_bubble(screen,0,550,55,"Timer Killed !");
    
  return 0;   
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// ROM acquisition functions

/** Performs the saving action on ROM of the images into a specified file.

\return None.
\author JF Allemand
*/ 
void CALLBACK automatic_saving(HWND hwnd,UINT uMsg,UINT_PTR idEvent,DWORD dwTime)
{
  int n_current_images_saved_tape = 0;
  register int i;
  long test = 0;
     
  test = iXon.present_image_number_aquired_on_PC;
  n_images_to_be_saved = iXon.present_image_number_aquired_on_PC - previous_image_for_tape_saving;
  if (n_images_to_be_saved > iXon.frames_in_tape_buffer)
    {         
      Kill_timer();
      iXon.is_live = FREEZE;
      Sleep(500);
      fclose(fp_images);
      set_image_ending_time(oi_iXon_tape);
      close_movie_on_disk_JF(oi_iXon_tape,n_images_saved);
         			
      win_report("One image lost");//Verifier le test!!!!!!!!!!!!
      return ;         			
    }
 	 
  //GetTotalNumberImagesAcquired(&present_image_number_for_tape_saving);
     
  for (i = 0 ; i < n_images_to_be_saved ; i++)
    {
      fwrite(oi_iXon_tape->im.mem[(previous_image_for_tape_saving +i)%iXon.frames_in_tape_buffer],2,(unsigned long)iXon.total_pixels_number,fp_images);
      n_images_saved++;
      n_current_images_saved_tape++;
      draw_bubble(screen,0,150,200,"test   %d PC %d buff %d o %d saved %d",test,iXon.present_image_number_aquired_on_PC,previous_image_for_tape_saving,n_images_to_be_saved,n_images_saved);
    }
  previous_image_for_tape_saving += n_current_images_saved_tape;//VERIF OFFSET DE 1
      	  
  return;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// New file saving functions

////////////////////////////////////////////////////////////////////////////////

/** Modified .gr header saver.

IMPORTANT : superseds all previous (bugged) versions of save_image_header.
\param O_i The oi containing the information.
\param char A header text ??
\param int ??
\return int Error code.
\author JF Allemand
*/
int save_image_header_JF(O_i* oi, char *head, int size)
{
  FILE *fp;
  int nh;
  char filename[512];

  if (oi == NULL || oi->dir == NULL || oi->filename == NULL)	return 1;
  build_full_file_name(filename,512,oi->dir,oi->filename);

  fp = fopen(filename,"r+b");
  if (fp == NULL)
    {
      error_in_file("%s\nfile not found!...",filename);
      return 1;
    }
  nh = find_CTRL_Z_offset(oi,fp);

  if (nh != size)
    {
      win_report("the size of headers disagree!");
      return 1;
    }
	  
  if (fp == NULL)
    {
      error_in_file("%s\nfile not found!...",filename);
      return 1;
    }
	
  fseek(fp,0,SEEK_SET);
  if (fwrite (head,sizeof(char),size,fp) != size)
    {
      win_report("Pb while writing header!");
      return 1;
    }
  fclose(fp);
  return 0;
}

////////////////////////////////////////////////////////////////////////////////
/** Modified file saver.

IMPORTANT : superseds all previous (bugged) versions of close_movie_on_disk.
\param O_i The oi containing the information.
\param int The number of frames in the movie.
\return int Error code.
\author JF Allemand
*/
int  close_movie_on_disk_JF(O_i *oi, int nf)
{
  char *head, *found, tmp[256];
  int size = 0, nff = -1, cff = -1, i;
  extern char *read_image_header(O_i* oi, int *size);
  head = read_image_header(oi, &size);
  if (head == NULL)  return 1;
  found = strstr(head,"-nf");
  if (found != NULL)    
    {
      sprintf(tmp,"-nf %010d %010d\r\n",nf, oi->im.c_f);
      for (i = 0; (i < 256) && (tmp[i] != 0); i++) found[i] = tmp[i]; 
    }
  else win_report("did not found -nf symbol");
  sscanf(found,"-nf %d %d",&nff,&cff);

  if (oi->im.record_duration != 0)
    {
      found = strstr(head,"-duration");
      if (found != NULL) 	
	{
      	  sprintf(tmp,"-duration %016.6f \r\n",oi->im.record_duration);
      	  for (i = 0; (i < 256) && (tmp[i] != 0); i++) found[i] = tmp[i]; 
	}
      else win_report("did not found -duration symbol");
    }
  save_image_header_JF(oi, head,  size);
  if (head) {free(head); head = NULL;}
  return 0;
}

#endif



