#ifndef _IXON_TAPE_H_
#define _IXON_TAPE_H_

O_i *oi_iXon_tape = NULL;
O_i *iXon_movie = NULL;
int n_images_to_be_saved = 1;
long int previous_image_for_tape_saving;
long int offset_image_number_for_tape_saving= 0;
FILE *fp_images = NULL;
long int present_image_number_for_tape_saving = -1;
int n_images_saved = 0;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Function declarations

//// Functions
//PXV_FUNC(int, From_iXon_2_Xvin_Movie, (void));
//PXV_FUNC(int, create_and_adapt_tape_iXon_buffer_image, (void));
//PXV_FUNC(int, Kill_timer, (void));
//
//// Tape functions
//PXV_FUNC(void, CALLBACK automatic_saving, (HWND hwnd,UINT uMsg,UINT_PTR idEvent,DWORD dwTime));
//PXV_FUNC(int, save_tape_mode, (void));
//PXV_FUNC(int, save_image_header_JF, (O_i* oi, char *head, int size));
//PXV_FUNC(int,  close_movie_on_disk_JF, (O_i *oi, int nf));

#endif
