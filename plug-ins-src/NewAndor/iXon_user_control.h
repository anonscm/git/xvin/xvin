#ifndef _IXON_USER_CONTROL_H_
#define _IXON_USER_CONTROL_H_

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Global variables

extern unsigned int WINAPI give_me_binning(int* x_bin, int* y_bin);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Functions declaration

// Menu void functions
PXV_FUNC(int, change_gain,(void));

// Thread functions
PXV_FUNC(DWORD WINAPI, InfoThreadProc,(LPVOID lpParam));

// Initialization functions
PXV_FUNC(int, create_iXon_info_thread,(void));

// Housekeeping functions
PXV_FUNC (int	,ixon_user_main,(void));
PXV_FUNC (int,	ixon_user_unload,(void));




#endif
