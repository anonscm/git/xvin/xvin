#ifndef _TRACKBEAD_H_
#define _TRACKBEAD_H_


typedef struct before_image_display
{
  int (*to_do)(imreg *imr, O_i *oi, int n, DIALOG *d, long long t, unsigned long dt, void *p);
  void *param;
  int previous_fr_nb;
  unsigned long timer_dt;
  int previous_in_time_fr_nb;
  int first_im;                     // the nb of the first image
  long long previous_in_time_fr;
  int lost_fr;
  struct before_image_display *next;
  int (*timer_do)(imreg *imr, O_i *oi, int n, DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow);
} Bid;



typedef struct thread_image_data
{
  imreg *imr;
  pltreg *pr;
  O_p *op;
  O_i *oi;
  DIALOG *dpr, *dimr;
  Bid *dbid;
} tid;


# define TRACK_FREEZE   2
# define TRACK_ON       1
# define TRACK_STOP     0

// job type

# define GRAB_CALIBRATION_IMAGE     8

# define COLLECT_DATA   16
// this is lower priority routine done in the general_idle_action
// typically grabbing the calibration image, force measurements etc
typedef struct future_job
{
  int imi;             // the image nb where to start action, if < 0 the next possible frame
  int last_imi;        // an internal idex used to avoid calling several time this job for the same image
  int in_progress;    // 0 if finished, anything else if active
  int type;
  int bead_nb;
  int local;
  imreg *imr;
  O_i *oi;
  pltreg *pr;
  O_p *op;
  void *more_data ;                       // data structure for function
  int (*job_to_do)(int im, struct future_job *job);
} f_job;





#define IMAGE_CAL			1

#define DEF_CROSS_SIZE			2

# ifndef _TRACKBEAD_C_
//extern  __declspec(dllexport) int go_track ;             // flag on means tracking
PXV_VAR(int, go_track);
PXV_VAR(tid, dtid);
PXV_VAR(int, do_refresh_overlay);
PXV_VAR(Bid, bid);
PXV_VAR(imreg  *,imr_TRACK);
PXV_VAR(pltreg  *,pr_TRACK);
PXV_VAR(O_i  *, oi_TRACK);
PXV_VAR(DIALOG *, d_TRACK);
PXV_VAR(int, source_running);
PXV_VAR(char , running_message[]);
PXV_VAR(unsigned long, dt_simul);

PXV_VAR(f_job*, job_pending);
PXV_VAR(int, m_job);
PXV_VAR(int, n_job);

# endif
# ifdef _TRACKBEAD_C_
int go_track = 0;             // flag on means tracking
tid dtid;                     // the data passed to the thread proc
int do_refresh_overlay = 0;
Bid bid={NULL,NULL,0,0,0,0,0,0,NULL,NULL};

imreg  *imr_TRACK = NULL;
pltreg  *pr_TRACK = NULL;
O_i  *oi_TRACK = NULL;
DIALOG *d_TRACK = NULL;
int source_running = 0;
char running_message[128];
unsigned long dt_simul = 0;

f_job *job_pending = NULL;
int m_job = 0;
int n_job = 0;

# endif


PXV_FUNC(MENU*, trackBead_image_menu, (void));
PXV_FUNC(int, trackBead_main, (int argc, char **argv));
PXV_FUNC(int, init_image_source,(void));
PXV_FUNC(int, start_data_movie, (imreg *imr));
PXV_FUNC(DWORD WINAPI, TrackingThreadProc, (LPVOID lpParam)); 
PXV_FUNC(int, live_source, (void));
PXV_FUNC(int, freeze_source, (void));
PXV_FUNC(int, kill_source, (void));
PXV_FUNC(int, prepare_image_overlay, (O_i* oi));
PXV_FUNC(int, follow_bead_in_x_y,(void));
PXV_FUNC(int, track_oi_idle_action, (struct  one_image *oi, DIALOG *d));


//PXV_FUNC(float, read_Z_value, (void));
//PXV_FUNC(int, set_Z_value,(float z));


PXV_FUNC(int, fill_next_available_job_spot, (int in_progress, int im, int type, int bead_nb, imreg *imr, O_i *oi, pltreg *pr, O_p *op, void* more_data, int (*job_to_do)(int im, struct future_job *job),int local));

PXV_FUNC(f_job*, find_job_associated_to_image,(O_i *oi));
PXV_FUNC(f_job*, find_job_associated_to_plot,(O_p *op));

#endif

