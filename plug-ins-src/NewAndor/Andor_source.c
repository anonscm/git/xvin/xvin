////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/** Sets Xvin in real-time imaging mode with iXon.

\param None.
\return int Error code.
\author JF Allemand
*/ 
int iXon_real_time_imaging(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    //win_report("Real-time imaging");
    
    if (SetEMCCDGain(iXon.gain) != DRV_SUCCESS) win_report("Could not set gain to min in function 'iXon-real-time-imaging' !");
    
    /* Launches the acquisition and display thread */
    create_fluo_thread();
    
    return D_O_K;   
}   

////////////////////////////////////////////////////////////////////////////////

/** Stops iXon image acquisition.

\param None.
\return int Error code.
\author JF Allemand
*/ 
int Stop_acquisition(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    iXon.is_live_previous = iXon.is_live;
    iXon.is_live = FREEZE;
    
    Sleep(200); 
    /* Quite necessary. Following functions might be lured by the camera telling them
    it is still acquiring whereas it has just ceased to be. */

    /* Free data memory of the camera to ensure better efficiency */
    if (FreeInternalMemory() != DRV_SUCCESS) win_report("Could not set free internal memory in function 'Stop-acquisition' !");
    
    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
