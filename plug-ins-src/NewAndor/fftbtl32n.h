#ifndef _FFTBTL32N_H_
#define _FFTBTL32N_H_
/*	fftbtl32n.h  
 *	
 *	5 fev 1992	JMF
 *
 *	header for fftbtl32n.c
 *		set of routines for fast Fourier transforms
 *		in 32 bits with the DOS EXTENDER
 */

# include "platform.h"


typedef struct _fft_plan
{
  int nx;                  // the total number of frames requires to perform calibration
  int n_2, n_4;            // nx/2 and nx/4
  float    *fftbtsin;      // the sinus array
  int 	   *mixbt;         // the mixing array
  int pow_2[31];
} fft_plan;





XV_FUNC(fft_plan*, fftbt_init, (fft_plan *initial, int npts));
XV_FUNC(int, free_fft_plan_bt, (fft_plan *fp));
XV_FUNC(int, fftbtmixing, (fft_plan *fp, float *x));
XV_FUNC(int, fftbt, (fft_plan *fp, float *x, int df));
XV_FUNC(void, realtr1bt, (fft_plan *fp, float *x));
XV_FUNC(void, realtr2bt, (fft_plan *fp, float *x, int df));
XV_FUNC(int, fftbtwindow, (fft_plan *fp, float *x));
XV_FUNC(int, fftbtwindow1, (fft_plan *fp, float *x, float smp));
XV_FUNC(int, defftbtwindow1, (fft_plan *fp, float *x, float smp));
XV_FUNC(int, fftbtwc, (fft_plan *fp, float *x));
XV_FUNC(int, fftbtwc1, (fft_plan *fp, float *x, float smp));
XV_FUNC(int, defftbtwc, (fft_plan *fp, float *x));
XV_FUNC(int, defftbtwc1, (fft_plan *fp, float *x, float smp));
XV_FUNC(void, spec_realbt, (fft_plan *fp, float *x, float *spe));
XV_FUNC(void, spec_compbt, (fft_plan *fp, float *x, float *spe));
XV_FUNC(int, demodulatebt, (fft_plan *fp, float *x, int freq));
XV_FUNC(void, derive_realbt, (fft_plan *fp, float *x, float *y));
XV_FUNC(void, ampbt, (fft_plan *fp, float *x, float *y));

# endif
