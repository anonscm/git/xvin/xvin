#ifndef _MAGNETSCONTROL_C_
#define _MAGNETSCONTROL_C_


# include "ctype.h"
# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"
# include "xv_plugins.h"	// for inclusions of libraries that control dynamic loading of libraries.




# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../wlc/wlc.h"


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "magnetscontrol.h"
# include "trackBead.h"
# include "action.h"
# include "focus.h"



int    set_magnetscontrol_device(Translation_Motor_param *t_m, Rotation_Motor_param *r_m)
{
  int ret = 0, i;
  char *argvp[2];
  void*	hinstLib;
  char pl_name[256];



  if (strcmp(r_m->rotation_motor_model,t_m->translation_motor_model) != 0)
    {
      win_printf("Your motor translation model :\n%s\nDiffers from your motor rotation model:\n%s"
		 "We cannot handle this situation, please change your config",
		 t_m->translation_motor_model,r_m->rotation_motor_model);
      return 1; 
    }

  sprintf(pl_name,"%s_magnets",t_m->translation_motor_model);
  for (i=0; pl_name[i]; i++)
    pl_name[i] = tolower(pl_name[i]);
  
  argvp[0] = pl_name;
  argvp[1] = NULL;
  load_plugin(1, argvp);
  hinstLib = grep_plug_ins_hinstLib(argvp[0]);
  if (hinstLib == NULL)
    return win_printf_OK("Could not find plugins %s",argvp[0]);


  set_rot_value_raw = (int (*)(float rot))GetProcAddress(hinstLib,"_set_rot_value");
  if (set_rot_value_raw == NULL) 
    win_printf("Could not find: set_rot_value()\nin%s",argvp[0]); 

  set_rot_ref_value = (int (*)(float rot))GetProcAddress(hinstLib,"_set_rot_ref_value"); 
  if (set_rot_ref_value == NULL) 
    win_printf("Could not find: set_rot_ref_value()\nin%s",argvp[0]); 

  read_rot_value = (float (*)(void))GetProcAddress(hinstLib,"_read_rot_value");
  if (read_rot_value == NULL) 
    win_printf("Could not find: read_rot_value()\nin%s",argvp[0]); 

  set_magnet_z_value_raw = (int	(*)(float z))GetProcAddress(hinstLib,"_set_magnet_z_value");
  if (set_magnet_z_value_raw == NULL) 
    win_printf("Could not find: set_magnet_z_value()\nin%s",argvp[0]); 

  set_magnet_z_ref_value = (int (*)(float z))GetProcAddress(hinstLib,"_set_magnet_z_ref_value");
  if (set_magnet_z_ref_value == NULL) 
    win_printf("Could not find: set_magnet_z_ref_value()\nin%s",argvp[0]); 

  read_magnet_z_value = (float (*)(void))GetProcAddress(hinstLib,"_read_magnet_z_value");
  if (read_magnet_z_value == NULL) 
    win_printf("Could not find: read_magnet_z_value()\nin%s",argvp[0]); 

  set_magnet_z_value_and_wait_raw = (int (*)(float pos))GetProcAddress(hinstLib,"_set_magnet_z_value_and_wait");
  if (set_magnet_z_value_and_wait_raw == NULL) 
    win_printf("Could not find: set_magnet_z_value_and_wait()\nin%s",argvp[0]); 

  set_rot_value_and_wait_raw = (int (*)(float rot))GetProcAddress(hinstLib,"_set_rot_value_and_wait");
  if (set_rot_value_and_wait_raw == NULL) 
    win_printf("Could not find: set_rot_value_and_wait()\nin%s",argvp[0]); 
  
  set_motors_speed = (int (*)(float v))GetProcAddress(hinstLib,"_set_motors_speed");
  if (set_motors_speed == NULL) 
    win_printf("Could not find: set_motors_speed()\nin%s",argvp[0]); 

  get_motors_speed = (float	(*)(void))GetProcAddress(hinstLib,"_get_motors_speed");
  if (get_motors_speed == NULL) 
    win_printf("Could not find: get_motors_speed()\nin%s",argvp[0]); 
  


  /*
  go_and_dump_z_magnet = (int (*)(float z))GetProcAddress(hinstLib,"_go_and_dump_z_magnet");
  if (go_and_dump_z_magnet == NULL) 
    win_printf("Could not find: go_and_dump_z_magnet()\nin%s",argvp[0]); 
  go_wait_and_dump_z_magnet = (int (*)(float zmag))GetProcAddress(hinstLib,"_go_wait_and_dump_z_magnet");
  if (go_wait_and_dump_z_magnet == NULL) 
    win_printf("Could not find: go_wait_and_dump_z_magnet()\nin%s",argvp[0]); 
  go_and_dump_rot = (int (*)(float r))GetProcAddress(hinstLib,"_go_and_dump_rot");
  if (go_and_dump_rot == NULL) 
    win_printf("Could not find: go_and_dump_rot()\nin%s",argvp[0]); 
  go_wait_and_dump_rot = (int (*)(float r))GetProcAddress(hinstLib,"_go_wait_and_dump_rot");
  if (go_wait_and_dump_rot == NULL) 
    win_printf("Could not find: go_wait_and_dump_rot()\nin%s",argvp[0]); 
  
  go_wait_and_dump_log_specific_rot = (int (*)(float r, char *log))GetProcAddress(hinstLib,"_go_wait_and_dump_log_specific_rot");
  if (go_wait_and_dump_log_specific_rot == NULL) 
    win_printf("Could not find: go_wait_and_dump_log_specific_rot()\nin%s",argvp[0]); 
  go_wait_and_dump_log_specific_z_magnet = (int (*)(float zmag, char *log))GetProcAddress(hinstLib,"_go_wait_and_dump_log_specific_z_magnet");
  if (go_wait_and_dump_log_specific_z_magnet == NULL) 
    win_printf("Could not find: go_wait_and_dump_log_specific_z_magnet()\nin%s",argvp[0]); 
  */
      //}
  return ret;
}

float zmag_to_force(int nb, float zmag)
{
  float z, tmp;
  z = zmag - Pico_param.magnet_param.zmag_contact_sample;
  tmp = Pico_param.magnet_param.a1[nb] * z;
  if (Pico_param.magnet_param.nd[nb] > 2)
      tmp -= Pico_param.magnet_param.a2[nb] * z * z;
  if (Pico_param.magnet_param.nd[nb] > 3)
      tmp += Pico_param.magnet_param.a3[nb] * z * z * z;
  if (Pico_param.magnet_param.nd[nb] > 4)
      tmp -= Pico_param.magnet_param.a4[nb] * z * z * z * z;
  tmp = exp(tmp);
  return (Pico_param.magnet_param.f0[nb] * tmp);
}


float force_to_tau_x_in_fr(float force, int bead_type)
{
  float l_alpha, l_x, m_x, l_kx, l_taux; 
  double T = 298;

  l_alpha = force;
  l_alpha *= (Pico_param.dna_molecule.dsxi)/(1.38e-2 * T);
  l_x = extension_versus_alpha(l_alpha);
  m_x = Pico_param.dna_molecule.dsDNA_molecule_extension * l_x; 
  if (l_x > 0.1)       l_kx = force/m_x;  // pN/microns
  else 
    {
      l_kx = marko_siggia_improved_derivate(l_x);
      l_kx *= (1.38e-2 * T)/(Pico_param.dna_molecule.dsxi);
      l_kx /= Pico_param.dna_molecule.dsDNA_molecule_extension;
    }
  l_taux = 6 * M_PI * 1e-3 * Pico_param.bead_param.radius;

  if (l_kx > 0) l_taux /= l_kx;  else l_taux = 0;
  l_taux *= Pico_param.camera_param.camera_frequency_in_Hz;
  return (l_taux > 0) ? l_taux : 1;
}

float convert_zmag_to_force(int nb)
{
  float z, tmp;
  float x;
  double T = 298, dt, tmpd;

  z = n_magnet_z - Pico_param.magnet_param.zmag_contact_sample;
  tmp = Pico_param.magnet_param.a1[nb] * z;
  if (Pico_param.magnet_param.nd[nb] > 2)
      tmp -= Pico_param.magnet_param.a2[nb] * z * z;
  if (Pico_param.magnet_param.nd[nb] > 3)
      tmp += Pico_param.magnet_param.a3[nb] * z * z * z;
  if (Pico_param.magnet_param.nd[nb] > 4)
      tmp -= Pico_param.magnet_param.a4[nb] * z * z * z * z;
  tmp = exp(tmp);
  applied_force = Pico_param.magnet_param.f0[nb] * tmp;

  alpha = applied_force;
  alpha *= (Pico_param.dna_molecule.dsxi)/(1.38e-2 * T);
  x = extension_versus_alpha(alpha);
  mean_extension = Pico_param.dna_molecule.dsDNA_molecule_extension * x; 
  if (x > 0.1) 
    {
      kx = applied_force/mean_extension;  // pN/microns
      ky = applied_force/(mean_extension + Pico_param.bead_param.radius);  // pN/microns
    }
  else 
    {
      kx = marko_siggia_improved_derivate(x);
      kx *= (1.38e-2 * T)/(Pico_param.dna_molecule.dsxi);
      ky = kx;
      kx /= Pico_param.dna_molecule.dsDNA_molecule_extension;
      //      ky /= (mean_extension + Pico_param.bead_param.radius);  // pN/microns
      ky /= (Pico_param.dna_molecule.dsDNA_molecule_extension 
	     + Pico_param.bead_param.radius);  // pN/microns
    }
  mean_extension -= (n_rota * n_rota)/100;
  mean_extension = (mean_extension < 0) ? 0 : mean_extension;


  kz = marko_siggia_improved_derivate(x);
  kz *= (1.38e-2 * T)/(Pico_param.dna_molecule.dsxi);
  kz /= Pico_param.dna_molecule.dsDNA_molecule_extension;

  tauz = tauy = taux = 6 * M_PI * 1e-3 * Pico_param.bead_param.radius;

  if (kx > 0) taux /= kx;  else taux = 0;
  if (ky > 0) tauy /= ky;  else tauy = 0;
  if (kz > 0) tauz /= kz;  else tauz = 0;

  // oversampling at 64 x the camera rate
  dt = ((double)1)/(64*Pico_param.camera_param.camera_frequency_in_Hz);
  
  betax = exp(-dt/taux);
  betay = exp(-dt/tauy);
  betaz = exp(-dt/tauz);

  tmpd = 1;
  tmpd -= betax * betax;
  xnoise = sqrt((tmpd * 1.38e-5 * T)/kx);  // en microns

  tmpd = 1;
  tmpd -= betay * betay;
  ynoise = sqrt((tmpd * 1.38e-5 * T)/ky);  // en microns

  tmpd = 1;
  tmpd -= betaz * betaz;
  znoise = sqrt((tmpd * 1.38e-5 * T)/kz);  // en microns


  return applied_force;
}


int	set_rot_value(float r)
{
  if (n_rota == r) return 0;	
  set_rot_value_raw(r);
  convert_zmag_to_force(0);
  Open_Pico_config_file();
  set_config_float("STATE", "MagRot", r);
  Close_Pico_config_file();
  return 0;
}

int	set_rot_value_and_wait(float r)
{
  if (n_rota == r) return 0;	
  set_rot_value_and_wait_raw(r);
  convert_zmag_to_force(0);
  Open_Pico_config_file();
  set_config_float("STATE", "MagRot", r);
  Close_Pico_config_file();
  return 0;
}

int	set_magnet_z_value(float z)
{
  if (n_magnet_z == z) return 0;	
  set_magnet_z_value_raw(z);
  convert_zmag_to_force(0);
  Open_Pico_config_file();
  set_config_float("STATE", "MagZ", z);
  Close_Pico_config_file();
  return 0;
}

int	set_magnet_z_value_and_wait(float z)
{
  if (n_magnet_z == z) return 0;	
  set_magnet_z_value_and_wait_raw(z);
  convert_zmag_to_force(0);
  Open_Pico_config_file();
  set_config_float("STATE", "MagZ", z);
  Close_Pico_config_file();
  return 0;
}

int	go_and_dump_z_magnet(float z)
{
  if (n_magnet_z == z) return 0;	
  set_magnet_z_value(z);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);
}
int    go_wait_and_dump_z_magnet(float zmag)
{
  if (n_magnet_z == zmag)		return 0;
  set_magnet_z_value_and_wait(zmag);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);	
}

int    go_and_dump_rot(float r)
{
  if (n_rota == r)		return 0;
  set_rot_value(r);	
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);
}
int	go_wait_and_dump_rot(float r)
{
  if (n_rota == r)		return 0;
  set_rot_value_and_wait(r);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);	
}


int	go_wait_and_dump_log_specific_rot(float r, char *log)
{	
  if (n_rota == r)		return 0;
  set_rot_value_and_wait(r);
  //dump_to_specific_log_file_only(log,"Rotation number auto changed %g\n",n_rota);
  return 0;
}

int    go_wait_and_dump_log_specific_z_magnet(float zmag, char *log)
{
  if (n_magnet_z == zmag)		return 0;
  set_magnet_z_value_and_wait(zmag);
  //dump_to_specific_log_file_only(log,"Magnet Z changed auto to %g\n",n_magnet_z);	
  return 0;
}





int mag_z(void)
{
  int index, i;
  index = RETRIEVE_MENU_INDEX;

  if(updating_menu_state != 0)	
    {
      if ((fabs(n_magnet_z-(((float)index) /100))) < 0.02)
	active_menu->flags |=  D_SELECTED;
      else active_menu->flags &= ~D_SELECTED;
      return D_O_K;	
    }	
  i = immediate_next_available_action(MV_ZMAG_ABS, ((float)index) /100);
  if (i < 0)	win_printf_OK("Could not add pending action!");
  
  //go_and_dump_z_magnet( ((float)index) /100);
  //write_mag_value_to_disk();
  //do_update_menu();
  return OFF;
}



MENU *preset_mag_z(float z0, float z1, float dz)
{
  register int i, j;
  float tmp;
  char c[32];
  static MENU pmp[32];
  
  
  if (z0 == z1)
    {
      for (i = CANCEL; i == CANCEL; )
	i = win_scanf("You can define preset values to move Z position"
		     "of the magnets by a menu. Please define the maximum value (mm) %f"
		     "the minimum value (mm) %f"
		     "the step size (mm) (no more than 20 steps are possible %f",&z1,&z0,&dz);
    }
  if (z0 > z1) 
    {
      tmp = z1;
      z1 = z0;
      z0 = tmp;
    }	
  if (dz < 0) dz = -dz;
  if (dz > (z1 - z0)/20) dz = (z1 - z0)/20;
  
  for (i = (int)(100*z0), j = 0; i <= (int)(100*z1) && j < 31 ; i += (int)(100*dz), j++)
    {
      sprintf(c,"%5.2f mm",((float)i)/100);
      add_item_to_menu(pmp,strdup(c), mag_z,NULL,MENU_INDEX(i),NULL);
    }
  pmp[j].text = NULL;
  return pmp;
}
int	set_max_abs_zmag(void)
{
  register int i;
  float m = absolute_maximum_zmag;
  
  if(updating_menu_state != 0)	return D_O_K;  

  i = win_scanf("Define the absolute maximum Z mag %f",&m);
  if (i == CANCEL)	return OFF;
  absolute_maximum_zmag = m;
  
  Pico_param.translation_motor_param.translation_max_pos = m;
  write_Pico_config_file();
  return OFF;
}

float what_is_present_rot_value(void)
{
	return n_rota;
}
float what_is_present_z_mag_value(void)
{
	return n_magnet_z;
}
int read_magnets(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  win_printf("The magnets position \nin z is %f mm\n"
	     "Estimated force %6.2f pN \\alpha  %g\nrotation %f \n"
	     "mean extension %f \\mu m\n"
	     "Stiffness Kx = %g Ky = %g Kz = %g \n"
	     "\\tau_x = %g \\tau_y = %g \\tau_z = %g \n"
	     "\\beta_x = %g \\beta_y = %g \\beta_z = %g \n"
	     "x_n = %g y_n = %g z_n = %g \n"
	     ,n_magnet_z,applied_force,alpha,n_rota,
	     mean_extension,kx,ky,kz,taux,tauy,tauz,
	     betax,betay,betaz,xnoise,ynoise,znoise);
  return D_O_K;
}
int set_magnets_rot(void)
{
  register int i;
  static float z = 0;
	
  if(updating_menu_state != 0)	return D_O_K;
  
  z = n_rota;
  i = win_scanf("New magnets angular position in turns %f",&z);
  if (i == CANCEL)	return D_O_K;
  i = immediate_next_available_action(MV_ROT_ABS, z);
  if (i < 0)	win_printf_OK("Could not add pending action!");

  //go_and_dump_rot(z);
  return D_O_K;;
}

int set_magnets_z(void)
{
  register int i;
  static float z = 0;
	
  if(updating_menu_state != 0)	return D_O_K;
  
  z = n_magnet_z;
  i = win_scanf("New magnets position in mm %f",&z);
  if (i == CANCEL)	return D_O_K;

  i = immediate_next_available_action(MV_ZMAG_ABS, z);
  if (i < 0)	win_printf_OK("Could not add pending action!");

  //go_and_dump_z_magnet(z);
  //do_update_menu();

  return D_O_K;;
}


int move_mag(void)
{
  register int i;
  static float z = 0;
	
  if(updating_menu_state != 0)	return D_O_K;
  
  z = n_magnet_z;
  i = win_scanf("New magnets position in mm %f",&z);
  if (i == CANCEL)	return D_O_K;


  i = immediate_next_available_action(MV_ZMAG_ABS, z);
  if (i < 0)	win_printf_OK("Could not add pending action!");
  return D_O_K;;
}



int   ref_magnets_zmag(void)
{
  register int i;
  float z = n_magnet_z;
	
  if(updating_menu_state != 0)	return D_O_K;
  i = win_scanf("Enter the new Z reference magnet position %f",&z);
  if (i == CANCEL)	return OFF;
  i = win_printf("are you sure that you want to \n"
		 "change reference of Z magnet to %g",z); 
  if (i == CANCEL)	return OFF;	
  n_magnet_z = z;	
  set_magnet_z_ref_value(n_magnet_z);
  return D_O_K;;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Reference Magnet Z changed to %g\n",n_magnet_z);
}




int zmag_small_step_up(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  immediate_next_available_action(MV_ZMAG_ABS, n_magnet_z + 0.05);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z + 0.05,n_rota);
  set_window_title(buf);
  return D_O_K;
}
int zmag_small_step_dwn(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  immediate_next_available_action(MV_ZMAG_ABS, n_magnet_z - 0.05);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z - 0.05,n_rota);
  set_window_title(buf);
  return D_O_K;
}
int zmag_step_up(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  immediate_next_available_action(MV_ZMAG_ABS, n_magnet_z + 0.25);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z + 0.25,n_rota);
  set_window_title(buf);
  return D_O_K;
}
int zmag_step_dwn(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  immediate_next_available_action(MV_ZMAG_ABS, n_magnet_z - 0.25);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z - 0.25,n_rota);
  set_window_title(buf);
  return D_O_K;
}


int rot_small_step_up(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  immediate_next_available_action(MV_ROT_ABS, n_rota + 0.25);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z,n_rota + 0.25);
  set_window_title(buf);
  return D_O_K;
}
int rot_small_step_dwn(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  immediate_next_available_action(MV_ROT_ABS, n_rota - 0.25);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z,n_rota - 0.25);
  set_window_title(buf);
  return D_O_K;
}
int rot_step_up(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  immediate_next_available_action(MV_ROT_ABS, n_rota + 1);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z,n_rota + 1);
  set_window_title(buf);
  return D_O_K;
}
int rot_step_dwn(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  immediate_next_available_action(MV_ROT_ABS, n_rota - 1);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z,n_rota - 1);
  set_window_title(buf);
  return D_O_K;
}



int	ref_magnets_rot(void)
{
  register  int i;
  float r = n_rota;
	
  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut(0, KEY_F5, 0, zmag_step_up);
      add_keyboard_short_cut(0, KEY_F6, 0, zmag_small_step_up);
      add_keyboard_short_cut(0, KEY_F7, 0, zmag_small_step_dwn);
      add_keyboard_short_cut(0, KEY_F8, 0, zmag_step_dwn);
      add_keyboard_short_cut(0, KEY_F9, 0, rot_step_up);
      add_keyboard_short_cut(0, KEY_F10, 0, rot_small_step_up);
      add_keyboard_short_cut(0, KEY_F11, 0, rot_small_step_dwn);
      add_keyboard_short_cut(0, KEY_F12, 0, rot_step_dwn);
      return D_O_K;	
    }
  i = win_scanf("Enter the new reference \n of rotation number %f",&r);
  if (i == CANCEL)	return OFF;
  i = win_printf("are you sure that you want to \n"
		 "change reference of rotation number to %g",r); 
  if (i == CANCEL)	return OFF;
  n_rota = r;
  set_rot_ref_value(n_rota);
  return D_O_K;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"{\\bf Reference Rotation number changed %g}\n",n_rota);
}

int	inc_magnets_rot(void)
{
  register int i;
  float r = n_rota;
  static float dr = 10;
  char c[256];
	
  if(updating_menu_state != 0)	return D_O_K;
  sprintf(c,"the rotation number is presently %g {\\sl turns} \n"
	  "increment this value by %%f",r);
	
  i = win_scanf(c,&dr);
  if (i == CANCEL)	return OFF;
  if (r > 1000 || r < -1000)
    i = win_printf("the new rot position %f\n" 
		   "may take some time to reach !\n"
		   " do you really want to go there ?",r);
  if (i == CANCEL)	return OFF;	
  n_rota = r + dr;	
  set_rot_value(n_rota);
  return D_O_K;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number incremented to %g\n",n_rota);
}


int	do_search_coilable_bead(void)
{
  register int i;
  static float zlow = 14, zhigh = 16;
  int do_again = 1, tobe_turned = 0;
  imreg *imr;
  O_i *oi;
  //extern int		(*motion_action)(int mode);
  //static long noaction = 0;
  float key_rot = 0;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return OFF;	
  zlow = get_config_float("bead.search","zmaglow", zlow);
  zhigh = get_config_float("bead.search","zmaghigh", zhigh);
  
  
  i = win_scanf("enter low magnet position (right button) %6f\n "
		"enter high magnet position (left button) %6f\n",&zlow,&zhigh);
  
  if (i == CANCEL)	return OFF;
  set_config_float("bead.search","zmaglow", zlow);
  set_config_float("bead.search","zmaghigh", zhigh);
  //write_cfg(hard_cfg);			
  do 
    {
      if (mouse_b & 0x01)
	go_and_dump_z_magnet(zhigh);
      if (mouse_b & 0x02)
	go_and_dump_z_magnet(zlow);
      
      if (keypressed())
	{
	  do_again++;
	  
	  if (key[KEY_RIGHT])
	    {
	      key_rot = what_is_present_rot_value();
	      key_rot += ((key_shifts & KB_SHIFT_FLAG) == 0) ? 1 : 0.25;
	      tobe_turned = (fabs(what_is_present_rot_value() - key_rot)) > 0.005 ? 2 : 0;
	      set_rot_value(key_rot);
	      display_title_message("going to  = %g To be rot %d",key_rot,tobe_turned);
	    }
	  if (key[KEY_LEFT])
	    {
	      key_rot = what_is_present_rot_value();
	      key_rot -= ((key_shifts & KB_SHIFT_FLAG) == 0) ? 1 : 0.25;
	      tobe_turned = (fabs(what_is_present_rot_value() - key_rot)) > 0.005 ? 2 : 0;
	      set_rot_value(key_rot);
	      display_title_message("going to  = %g To be rot %d",key_rot,tobe_turned);
	    }
	  
	  if ((key[KEY_PLUS_PAD]) && ((key_shifts & KB_SHIFT_FLAG) == 0))
	    big_inc_Z_value(+1);
	  if ((key[KEY_MINUS_PAD]) && ((key_shifts & KB_SHIFT_FLAG) == 0))
	    big_inc_Z_value(-1);
	  if ((key[KEY_PLUS_PAD]) && (key_shifts & KB_SHIFT_FLAG))
	    small_inc_Z_value(1);
	  if ((key[KEY_MINUS_PAD]) && (key_shifts & KB_SHIFT_FLAG))
	    small_inc_Z_value(-1);
	  if (key[KEY_ESC]) do_again = 0;
	  clear_keybuf();
	}		
      /*
	if (event.flags & (M_MOTION | M_BUTTON_DOWN))
	if (motion_action != NULL)	motion_action(1);	
      */
      //refresh_pc_screen(CR);	
      track_oi_idle_action(oi, dtid.dimr);
	//display_title_message("Bead searching Rot %g Z mag %g mm",n_rota,n_magnet_z);			
    }while(do_again);
  return 0;	
}



MENU *magnetscontrol_plot_menu(void)
{
	static MENU mn[32], *pm = NULL;


	if (mn[0].text != NULL)	return mn;



	pm = preset_mag_z(0, 10, 0.5);
	add_item_to_menu(mn,"Read position", read_magnets,NULL,0,NULL);
	add_item_to_menu(mn,"Magnets z position", set_magnets_z,NULL,0,NULL);
	add_item_to_menu(mn,"Set Z magnet reference", ref_magnets_zmag,NULL,0,NULL);
	add_item_to_menu(mn,"Magnets rotation", set_magnets_rot,NULL,0,NULL);

	add_item_to_menu(mn,"Increment rotation", inc_magnets_rot,NULL,0,NULL);
	add_item_to_menu(mn,"Set rotation reference", ref_magnets_rot,NULL,0,NULL);


	add_item_to_menu(mn,"Preset Z",NULL, pm, 0,NULL);
	add_item_to_menu(mn,"set Z_{abs}", set_max_abs_zmag,NULL,0,NULL);
	add_item_to_menu(mn,"Search bead", do_search_coilable_bead,NULL,0,NULL);



	return mn;
}



int	magnetscontrol_main(int argc, char **argv)
{
	set_magnetscontrol_device(&(Pico_param.translation_motor_param),&(Pico_param.rotation_motor_param));
	add_plot_treat_menu_item ( "Magnets", NULL, magnetscontrol_plot_menu(), 0, NULL);
	add_image_treat_menu_item ( "Magnets", NULL, magnetscontrol_plot_menu(), 0, NULL);
	return D_O_K;
}



/*
to add

int	do_search_coilable_bead(void)
{
	register int i;
	static float zlow = 14, zhigh = 16;
	int do_again = 1, tobe_turned = 0;
	extern int		(*motion_action)(int mode);
	static long noaction = 0;
	float key_rot = 0;
	int	refresh_pc_screen(char ch);

	if(ch != CR) return OFF;	
	
	zlow = get_cfg_float(hard_cfg,"bead.search","zmaglow", zlow);
	zhigh = get_cfg_float(hard_cfg,"bead.search","zmaghigh", zhigh);
	
	
	i = win_scanf("enter low magnet pos (right button) %f "
		      "enter high magnet pos (left button) %f",&zlow,&zhigh);
	
	if (i == CANCEL)	return OFF;
	set_cfg_float(hard_cfg,"bead.search","zmaglow", zlow);
	set_cfg_float(hard_cfg,"bead.search","zmaghigh", zhigh);
	write_cfg(hard_cfg);			
	do 
	{
		MyGetEvent((M_MOTION | M_BUTTON_DOWN | M_KEYPRESS | M_POLL), &event);
		if (event.flags & (M_MOTION | M_BUTTON_DOWN | M_KEYPRESS))
			if (motion_action != NULL)	motion_action(0);
		if (event.flags & (M_LEFT_DOWN))
		{
			last_m_event = event;
			event.flags = 0;
			go_and_dump_z_magnet(zhigh);
		}	
		if (event.flags & (M_RIGHT_DOWN))
		{
			last_m_event = event;
			event.flags = 0;
			go_and_dump_z_magnet(zlow);
		}	

		if (event.flags & M_KEYPRESS)
		{
			do_again++;

			if (event.key == RIGHT_AR)
			  {
			    key_rot = what_is_present_rot_value();
			    key_rot += ((event.kbstat & KB_SHIFT) == 0) ? 1 : 0.25;
			    tobe_turned = (fabs(what_is_present_rot_value() - key_rot)) > 0.005 ? 2 : 0;
			    set_rot_value(key_rot);
			    display_title_message("going to  = %g To be rot %d",key_rot,tobe_turned);
			  }
			if (event.key == LEFT_AR)
			  {
			    key_rot = what_is_present_rot_value();
			    key_rot -= ((event.kbstat & KB_SHIFT) == 0) ? 1 : 0.25;
			    tobe_turned = (fabs(what_is_present_rot_value() - key_rot)) > 0.005 ? 2 : 0;
			    set_rot_value(key_rot);
			    display_title_message("going to  = %g To be rot %d",key_rot,tobe_turned);
			  }

			if (event.key == '+' && (event.kbstat & KB_SHIFT) == 0)
				big_inc_Z_value(+1);
			if (event.key == '-' && (event.kbstat & KB_SHIFT) == 0)
				big_inc_Z_value(-1);
			if (event.key == '+' && event.kbstat & KB_SHIFT)
				small_inc_Z_value(1);
			if (event.key == '-' && event.kbstat & KB_SHIFT)
				small_inc_Z_value(-1);
			delta_t = noaction + event.dtime;
			if (event.key == ESC ) do_again = 0;
			last_m_event = event;							
			event.flags = 0;		
		}		
		if (event.flags & (M_MOTION | M_BUTTON_DOWN))
			if (motion_action != NULL)	motion_action(1);	
		refresh_pc_screen(CR);	
		display_title_message("Bead searching Rot %g Z mag %g mm",n_rota,n_magnet_z);			
	}while(do_again);
	return 0;	
}

int	do_search_spike_bead(char ch)
{
	register int i;
	static float zlow = 0, zhigh = 1;
	int do_again = 1;
	extern int		(*motion_action)(int mode);
	static long noaction = 0;
	int	refresh_pc_screen(char ch);

	if(ch != CR) return OFF;	
	
	zlow = get_config_float("bead.search","rotleft", zlow);
	zhigh = get_config_float("bead.search","rotright", zhigh);
	
	
	i = win_scanf("enter first  rot magnet pos (left button) %f"
		"enter second rot  magnet pos (right button) %f",&zlow,&zhigh);
	
	if (i == CANCEL) return OFF;
	set_cfg_float(hard_cfg,"bead.search","rotleft", zlow);
	set_cfg_float(hard_cfg,"bead.search","rotright", zhigh);
	write_cfg(hard_cfg);			
	do 
	{
		MyGetEvent((M_MOTION | M_BUTTON_DOWN | M_KEYPRESS | M_POLL), &event);
		if (event.flags & (M_MOTION | M_BUTTON_DOWN | M_KEYPRESS))
			if (motion_action != NULL)	motion_action(0);
		if (event.flags & M_KEYPRESS)
		{
			if (event.key == ESC ) do_again = 0;
			last_m_event = event;							
			event.flags = 0;		
		}
		if (event.flags & (M_LEFT_DOWN))
		{
			last_m_event = event;
			event.flags = 0;
			go_and_dump_rot(zlow);
		}	
		if (event.flags & (M_RIGHT_DOWN))
		{
			last_m_event = event;
			event.flags = 0;
			go_and_dump_rot(zhigh);
		}	
		if (event.flags & M_KEYPRESS)
		{
			do_again++;
			if (event.key == '+' && (event.kbstat & KB_SHIFT) == 0)
				big_inc_Z_value(+1);
			if (event.key == '-' && (event.kbstat & KB_SHIFT) == 0)
				big_inc_Z_value(-1);
			if (event.key == '+' && event.kbstat & KB_SHIFT)
				small_inc_Z_value(1);
			if (event.key == '-' && event.kbstat & KB_SHIFT)
				small_inc_Z_value(-1);
			if (event.key == 's')
				return 0;
					
			delta_t = noaction + event.dtime;
			last_m_event = event;							
			event.flags = 0;		
		}		
		if (event.flags & (M_MOTION | M_BUTTON_DOWN))
			if (motion_action != NULL)	motion_action(1);	
		refresh_pc_screen(CR);	
		display_title_message("Bead spiking Rot %g Z mag %g mm",n_rota,n_magnet_z);			
	}while(do_again);
	return 0;	
}


 */

# endif 
