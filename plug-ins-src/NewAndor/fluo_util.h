#ifndef _FLUO_UTIL_H_
#define _FLUO_UTIL_H_


# define FLUO_BUFFER_SIZE 4096
# define PROFILE_BUFFER_SIZE 128

# define OBJ_MOVING 32
# define ZMAG_MOVING 64
# define ROT_MOVING 128

typedef struct bead_fluoing
{
  int xc, yc, x0, y0, not_lost, im_prof_ref;    // bead pixel position
  int in_image, mouse_draged, mx, my; 
  int start_im;                    // im number for fluoing start
  int bead_number;                 // a unique bead identifier
  float x[FLUO_BUFFER_SIZE], y[FLUO_BUFFER_SIZE], z[FLUO_BUFFER_SIZE];     // bead position
  float xt[FLUO_BUFFER_SIZE], yt[FLUO_BUFFER_SIZE], zt[FLUO_BUFFER_SIZE];  // bead true position
  char n_l[FLUO_BUFFER_SIZE];                       // not lost idicator
  float rad_prof_ref[PROFILE_BUFFER_SIZE];             // reference radial profile
  float rad_prof_ref_fft[PROFILE_BUFFER_SIZE];         // reference radial profile fourrier transform
  float rad_prof_fft[PROFILE_BUFFER_SIZE];             // radial profile fourrier transform
  float rad_prof[FLUO_BUFFER_SIZE][PROFILE_BUFFER_SIZE];                // radial profile
} b_fluo;


# define BUF_FREEZED 2
# define BUF_CHANGING 1
# define BUF_UNLOCK 0

typedef struct gen_fluoing
{
  int (*user_fluo_action)(struct gen_fluoing *gt, DIALOG *d, void *data);  // a user callback function       
  int c_i, n_i, m_i, lc_i;          // c_i, index of the current image, n_i size of buffer, m_i size allocated
  int ac_i, lac_i;                  // ac_i actual number of frame since acquisition started + local
  int cl, cw, dis_filter, bd_mul;   // cross size, filter def, and bd_mul define the loss of bead condition 
  int c_b, n_b, m_b;                // the current bead nb and the max bead nb
  float max_obj_mov;                // Maximum relative objective displacement in servo mode
  float z_obj_servo_start;          // objective position at the start of servo mode
  int bead_servo_nb;                // the bead used to servo the objective
  int n_bead_display;               // the bead number corresponding to the plot display
  int local_lock;                   // 0 unlock, 1 changing, 2 freezed
  int xi[256], yi[256];             // tmp int buffers to avg profile
  float xf[256], yf[256];           // tmp float buffers to do fft
  b_fluo **bd;                     // the bead pointer
  int imi[FLUO_BUFFER_SIZE];                         // the image nb
  int limi[FLUO_BUFFER_SIZE];                         // local buffer the image nb
  int imit[FLUO_BUFFER_SIZE];                        // the image nb by timer
  int limit[FLUO_BUFFER_SIZE];                        // the image nb by timer
  long long imt[FLUO_BUFFER_SIZE];                   // the image absolute time
  long long limt[FLUO_BUFFER_SIZE];                   // local the image absolute time
  long long imtt[FLUO_BUFFER_SIZE];                  // the image absolute time obtained by timer 
  unsigned long imdt[FLUO_BUFFER_SIZE];              // the time spent in the previous function call
  unsigned long limdt[FLUO_BUFFER_SIZE];              // local the time spent in the previous function call
  unsigned long imtdt[FLUO_BUFFER_SIZE];             // the time spent before image flip in timer mode
  float zmag[FLUO_BUFFER_SIZE], rot_mag[FLUO_BUFFER_SIZE], obj_pos[FLUO_BUFFER_SIZE];  // the magnet position, the objective position measured
  int status_flag[FLUO_BUFFER_SIZE];
  float zmag_cmd[FLUO_BUFFER_SIZE], rot_mag_cmd[FLUO_BUFFER_SIZE], obj_pos_cmd[FLUO_BUFFER_SIZE];  // the magnet position, the objective position asked for

} g_fluo;


# ifdef _FLUO_UTIL_C_
g_fluo *fluo_info = NULL;
int px0,px1,py0,py1;
float intensity[FLUO_BUFFER_SIZE];
# else
PXV_VAR(g_fluo*, fluo_info);
PXV_VAR(int, px0);
PXV_VAR(int, px1);
PXV_VAR(int, py0);
PXV_VAR(int, py1);
PXV_VAR(int,x_center);
PXV_VAR(int,y_center);
# endif



PXV_FUNC(int, grab_bead_info_from_calibration, (O_i *oi, float *xb, float *yb, float *zb, int *nx, int *cw));
PXV_FUNC(O_i*, do_load_calibration, (int n));
PXV_FUNC(int, show_bead_cross, (BITMAP *imb, imreg *imr, DIALOG *d));
PXV_FUNC(int, record_calibration, (void));

PXV_FUNC(int, get_present_image_nb, (void));
PXV_FUNC(int, move_bead_cross, (BITMAP *imb, imreg *imr, DIALOG *d, int x0, int y0));
# endif



