#ifndef _MAGNETSCONTROL_H_
#define _MAGNETSCONTROL_H_


# ifndef _MAGNETSCONTROL_C_
PXV_VAR(float, n_rota);
PXV_VAR(float, n_rot_offset);
PXV_VAR(float, n_magnet_z);
PXV_VAR(float, n_magnet_offset);
PXV_VAR(float, v_rota);
PXV_VAR(float, v_mag);
PXV_VAR(float, absolute_maximum_zmag);
PXV_VAR(double, applied_force);
PXV_VAR(double, alpha);
PXV_VAR(double, mean_extension);
PXV_VAR(double, kx);
PXV_VAR(double, ky);
PXV_VAR(double, kz);
PXV_VAR(double, taux);
PXV_VAR(double, tauy);
PXV_VAR(double, tauz);

PXV_VAR(double, betax);             // exp (-dt/tau)
PXV_VAR(double, betay);
PXV_VAR(double, betaz);
PXV_VAR(double, xnoise);                // the magnitude of noise
PXV_VAR(double, ynoise);
PXV_VAR(double, znoise);


PXV_FUNCPTR(int, set_rot_value_raw,(float rot));
PXV_FUNC(int, set_rot_value,(float rot));
PXV_FUNCPTR(int, set_rot_ref_value, (float rot)); 
PXV_FUNCPTR(float, read_rot_value, (void));
PXV_FUNCPTR(int, set_magnet_z_value_raw, (float z));
PXV_FUNC(int, set_magnet_z_value, (float z));
PXV_FUNCPTR(int, set_magnet_z_ref_value, (float z));
PXV_FUNCPTR(float, read_magnet_z_value, (void));
PXV_FUNCPTR(int, set_magnet_z_value_and_wait_raw, (float pos));
PXV_FUNC(int, set_magnet_z_value_and_wait, (float pos));
PXV_FUNCPTR(int, set_rot_value_and_wait_raw, (float rot));
PXV_FUNC(int, set_rot_value_and_wait, (float rot));
PXV_FUNCPTR( int, set_motors_speed, (float v));
PXV_FUNCPTR(float, get_motors_speed, (void));

/*
PXV_FUNCPTR( int, go_and_dump_z_magnet, (float z));
PXV_FUNCPTR(int, go_wait_and_dump_z_magnet, (float zmag));
PXV_FUNCPTR(int, go_and_dump_rot, (float r));
PXV_FUNCPTR(int, go_wait_and_dump_rot, (float r));
PXV_FUNCPTR( int, go_wait_and_dump_log_specific_rot, (float r, char *log));
PXV_FUNCPTR(int, go_wait_and_dump_log_specific_z_magnet, (float zmag, char *log));
*/

PXV_FUNC( int, go_and_dump_z_magnet, (float z));
PXV_FUNC(int, go_wait_and_dump_z_magnet, (float zmag));
PXV_FUNC(int, go_and_dump_rot, (float r));
PXV_FUNC(int, go_wait_and_dump_rot, (float r));
PXV_FUNC( int, go_wait_and_dump_log_specific_rot, (float r, char *log));
PXV_FUNC(int, go_wait_and_dump_log_specific_z_magnet, (float zmag, char *log));

PXV_FUNC(int, magnetscontrol_main, (int argc, char **argv));

PXV_FUNC(int, move_mag, (void));

PXV_FUNC(float, zmag_to_force,(int nb, float zmag));
PXV_FUNC(float, force_to_tau_x_in_fr,(float force, int bead_type));
# else

float n_rota = 0, n_rot_offset = 0;
float n_magnet_z = 0, n_magnet_offset = 0;
float v_rota = 1, v_mag = 1;
float absolute_maximum_zmag = 20;
double applied_force = 0;
double alpha;
double mean_extension = 0;
double kx;                // stiffness in pN/microns
double ky;
double kz;
double taux;              // characteristic time in s
double tauy;
double tauz;
double betax = 0;             // exp (-dt/tau)
double betay = 0;
double betaz = 0;
double xnoise = 0;            // the magnitude of noise
double ynoise = 0;
double znoise = 0;


int 	(*set_rot_value_raw)(float rot);
int 	set_rot_value(float rot);
int 	(*set_rot_ref_value)(float rot); 
float 	(*read_rot_value)(void);

int 	(*set_magnet_z_value_raw)(float z);
int     set_magnet_z_value(float z);



int 	(*set_magnet_z_ref_value)(float z);
float 	(*read_magnet_z_value)(void);

int	set_magnet_z_value_and_wait(float pos);
int	(*set_magnet_z_value_and_wait_raw)(float pos);

int	(*set_rot_value_and_wait_raw)(float rot);
int	set_rot_value_and_wait(float rot);

int 	(*set_motors_speed)(float v);
float 	(*get_motors_speed)(void);
/*
int	(*go_and_dump_z_magnet)(float z);
int	(*go_wait_and_dump_z_magnet)(float zmag);
int	(*go_and_dump_rot)(float r);
int	(*go_wait_and_dump_rot)(float r);

int	(*go_wait_and_dump_log_specific_rot)(float r, char *log);
int	(*go_wait_and_dump_log_specific_z_magnet)(float zmag, char *log);
*/


int	go_and_dump_z_magnet(float z);
int	go_wait_and_dump_z_magnet(float zmag);
int	go_and_dump_rot(float r);
int	go_wait_and_dump_rot(float r);

int	go_wait_and_dump_log_specific_rot(float r, char *log);
int	go_wait_and_dump_log_specific_z_magnet(float zmag, char *log);


# endif

float 	what_is_present_rot_value(void);
float 	what_is_present_z_mag_value(void);

# endif
