
#ifndef _ACTION_H_
#define _ACTION_H_


# define MV_OBJ_ABS   2
# define MV_OBJ_REL   3
# define MV_ZMAG_ABS  4
# define MV_ZMAG_REL  5
# define MV_ROT_ABS   8
# define MV_ROT_REL   9


PXV_FUNC(int, immediate_next_available_action, (int type, float value));
PXV_FUNC(int, fill_next_available_action, (int im, int type, float value));
PXV_FUNC(int, find_next_action, (int imi));
PXV_FUNC(int, find_remaining_action, (int imi));
PXV_FUNC(int, find_next_available_action, (void));


// this describe real time action to perform like moving objective, magnets etc
typedef struct future_action
{
  int imi;                           // the image nb where to act, if < 0 the next possible frame
  int proceeded;                     //  1 if treated, 0 if pending
  int type;
  float value;
} f_action;


# ifndef _ACTION_C_

# else
f_action *action_pending = NULL;
int m_action = 0;
int n_action = 0;

# endif
PXV_VAR(f_action*, action_pending);
PXV_VAR(int, m_action);
PXV_VAR(int, n_action);

# endif
