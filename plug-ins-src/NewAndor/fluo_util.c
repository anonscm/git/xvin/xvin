/** 
    \brief Plug-in program for monitoring image properties in a thread system as in trackBead plugin
    
    This is a subprogram that creates, displays and interfaces the menus with the plug-in functions.
    
    \author V. J-F. Allemand
*/

#ifndef _FLUO_UTIL_C_
#define _FLUO_UTIL_C_

# include "allegro.h"
# include "winalleg.h"

# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "focus.h"
//# include "magnetscontrol.h"
# include "trackBead.h"
# include "brown_util.h"
# include "im_fluo.h"
# include "fluo_util.h"
# include "action.h"
# include "calibration.h"



MENU bead_active_menu[32];

/** this function initialize the tracking info structure

This function is started as soon as the video acquisition is launch so that we can
check the timing

\param none
\return A tracking info structure.
\version 19/05/06
*/



int timing_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j, k;
  static int  immax = -1; // last_c_i = -1,
  d_s *dsx, *dsy, *dsz;
  int nf, last_fr = 0, lost_fr, lf;
  unsigned long   dtm, dtmax, dts;
  float   fdtm, fdtmax;
  long long last_ti = 0;

  if (fluo_info == NULL) return D_O_K;
  dts = get_my_uclocks_per_sec();
  dsx = find_source_specific_ds_in_op(op,"Timing rolling buffer");
  dsy = find_source_specific_ds_in_op(op,"Period rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Timer rolling buffer");
  if (dsx == NULL || dsy == NULL || dsz == NULL) return D_O_K;	
  /*
  if (sscanf(dsx->source,"X rolling buffer bead %d",&bead_num) != 1)
    return win_printf_OK("Cannot find bead number");
  */
  //if (fluo_info->n_b < 1)    return win_printf_OK("No bead to display");
  i = fluo_info->lc_i;
  if (fluo_info->limi[i] <= immax) return D_O_K;   // we are up todate
  if (fluo_info->local_lock & BUF_CHANGING) return D_O_K;   // we have to wait
  fluo_info->local_lock |= BUF_FREEZED; // we forbid buffer changes
  i = fluo_info->lc_i;
  immax = fluo_info->limi[i];               
  lf = immax - fluo_info->lac_i;
  if (fluo_info->lac_i > FLUO_BUFFER_SIZE) 
    {
      nf = FLUO_BUFFER_SIZE;
      j = fluo_info->lc_i + 1;  //last_c_i;
    }
  else
    {
      nf = fluo_info->lac_i-1;
      j = 1;
    }
  dsx->nx = dsx->ny = nf;
  dsy->nx = dsy->ny = nf - 1;
  dsz->nx = dsz->ny = nf;
  

  for (i = 0, lost_fr = 0, dtm = dtmax = 0, fdtm = fdtmax = 0, k = 0; i < nf; i++, j++, k++)
    {
      j = (j < FLUO_BUFFER_SIZE) ? j : j - FLUO_BUFFER_SIZE;
      dsx->xd[i] = fluo_info->limi[j];
      dsx->yd[i] = ((float)(1000*fluo_info->limdt[j]))/dts; 
      //dsx->yd[i] = fluo_info->limdt[j]; 
      dsz->xd[i] = 1000*(float)fluo_info->limt[j]/get_my_ulclocks_per_sec(); 
      dsz->yd[i] = fluo_info->limit[j];
      if (i > 0) 
	{
	  dsy->xd[i-1] = fluo_info->limi[j];
	  dsy->yd[i-1] = 1000*(double)(fluo_info->limt[j]-last_ti)/get_my_ulclocks_per_sec(); 
	  lost_fr += (fluo_info->limi[j] - last_fr) - 1;
	}
      last_ti = fluo_info->limt[j];
      last_fr = fluo_info->limi[j];
      dtm += fluo_info->limdt[j];
      fdtm += dsx->yd[i];
      dtmax = (fluo_info->limdt[j] > dtmax) ? fluo_info->limdt[j] : dtmax;
      fdtmax = (dsx->yd[i] > fdtmax) ? dsx->yd[i] : fdtmax;
    }

  op->need_to_refresh = 1; 
/*   if (k) set_plot_title(op, "\\stack{{%d fr position lac_i %d stat %d}{%d missed images}" */
/* 			"{\\pt8 dt mean %6.3f ms (%6.3f ms) max %6.3f (%6.3f)}" */
/* 			"{MY_UCLOCKS_PER_SEC %d 0->%d}}",fluobid.first_im +fluo_info->lac_i  */
/* 			,immax,0,lf,fdtm/k, */
/* 			1000*((double)(dtm/k))/dts, */
/* 			fdtmax, 1000*((double)(dtmax))/dts,(int)dts,(int)fluo_info->limdt[1]); */
  fluo_info->local_lock = BUF_UNLOCK; // we allow new buffer changes

  i = FLUO_BUFFER_SIZE/4;
  j = ((int)dsx->xd[1])/i;
  op->x_lo = j*i;
  op->x_hi = (j+5)*i;
  set_plot_x_fixed_range(op);
  return refresh_plot(pr_FLUO, UNCHANGED);
}



int obj_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  static int last_c_i = -1, immax = -1;
  d_s *dsx, *dsz;
  int nf;
  
  if (fluo_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"Obj. rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Z rolling buffer");
  if (dsx == NULL || dsz == NULL) return D_O_K;	

  if (fluo_info->n_b < 1)    return D_O_K;	//win_printf_OK("No bead to display");
  i = fluo_info->c_i;
  if (fluo_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = fluo_info->imi[i];               
  nf = (fluo_info->ac_i > FLUO_BUFFER_SIZE) ? FLUO_BUFFER_SIZE : fluo_info->ac_i;
  nf = (nf > (FLUO_BUFFER_SIZE>>1)) ? FLUO_BUFFER_SIZE>>1 : nf;
  dsx->nx = dsx->ny = dsz->nx = dsz->ny = nf;
  last_c_i = (fluo_info->c_i > 0) ? fluo_info->c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? FLUO_BUFFER_SIZE + j: j;
      j = (j < FLUO_BUFFER_SIZE) ? j : FLUO_BUFFER_SIZE;
      dsx->xd[i] = dsz->xd[i] = fluo_info->imi[j];
      dsx->yd[i] = fluo_info->obj_pos[j];
      dsz->yd[i] = fluo_info->bd[fluo_info->n_bead_display]->z[j];
    }
  i = FLUO_BUFFER_SIZE/8;
  j = ((int)dsx->xd[i])/i;
  op->x_lo = (j-1)*i;
  op->x_hi = (j+4)*i;
  set_plot_x_fixed_range(op);

  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d bead servo %d}{Obj start %g obj %g}}"
		 ,fluo_info->ac_i,  fluo_info->bead_servo_nb,
  		 fluo_info->z_obj_servo_start,
  		 fluo_info->obj_pos[last_c_i]);
  return refresh_plot(pr_FLUO, UNCHANGED);
}


int	x_imdata_2_imr_nl(imreg *imr, int x, DIALOG *d)
{
	int pw;
	if (imr == NULL || imr->one_i == NULL)	return -xvin_error(Wrong_Argument);
	pw = imr->one_i->im.nxe - imr->one_i->im.nxs;
	if (imr->s_nx == 0 || pw == 0) return 0;
	return (imr->x_off + d->x + ((x - imr->one_i->im.nxs) * imr->s_nx)/pw);
}
int	y_imdata_2_imr_nl(imreg *imr, int y, DIALOG *d)
{
	int ph;
	if (imr == NULL || imr->one_i == NULL)	return -xvin_error(Wrong_Argument);
	ph = imr->one_i->im.nye - imr->one_i->im.nys;		
	if (imr->s_ny == 0 || ph == 0) return 0;	
	return (d->h - imr->y_off  + ((y - imr->one_i->im.nys) * imr->s_ny)/ph); // + 
}



int show_bead_cross(BITMAP *imb, imreg *imr, DIALOG *d)
{
  int cl, cw, pxl, pyl, redraw = 0;
  b_fluo *bt = NULL;
  int red = makecol(255,0,0);
  int greeen = makecol(0,255,0);

  if (fluo_info == NULL || imb == NULL || imr == NULL) return 0;
  if (fluo_info->n_b < 1) return 0;

  for (fluo_info->c_b = 0; fluo_info->c_b < fluo_info->n_b; fluo_info->c_b++)
    {
      cl = fluo_info->cl;                   // cross arm length	
      cw = fluo_info->cw;                   // cross arm width	
      bt = fluo_info->bd[fluo_info->c_b];
      if (bt->not_lost > 0) //== NB_FRAMES_BEF_LOST)
	draw_cross_in_screen_unit(bt->xc, bt->yc, cl, cw, greeen, imb, imr->screen_scale);
      else
	{
	  pxl = (int)(0.5+x_imr_2_imdata(imr, mouse_x));    
	  pyl = (int)(0.5+y_imr_2_imdata(imr, mouse_y));    
	  if ((mouse_b & 0x1) 
	      && (pxl > bt->xc - cw)
	      && (pxl < bt->xc + cw)
	      && (pyl > bt->yc - cw)
	      && (pyl < bt->yc + cw))

	    {
	      draw_bubble(screen, B_LEFT, 512, 100, "x %d(%d) y %d(%d)",pxl,bt->xc,pyl,bt->yc);
	      bt->mouse_draged = 1;
	      //	      bt->mx = bt->xc = bt->x0 = pxl;
	      //bt->my = bt->yc = bt->y0 = pyl;
	    }
	  else 
	    {
	      if (mouse_b == 0)		  bt->mouse_draged = 0;
	    }
	  draw_cross_in_screen_unit(bt->xc, bt->yc, cl, cw, red, imb, imr->screen_scale);
	}
      if  (bt->mouse_draged) redraw = 1;
    }
  return redraw;
}



// this running in the timer thread
int track_in_x_y_timer(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow) 
{
/*   int cl, cw, cl2, ci, i, ci_1; */
/*   int dis_filter = 0, *xi, *yi; */
/*   float  *xf, *yf; */
/*   BITMAP *imb = NULL; */
/*   g_fluo *gt = NULL; */
/*   b_fluo *bt = NULL; */
/* 	/\* we grab the general structure *\/ */
  
/*   gt = (g_fluo*)p; */

/*   gt->ac_i++;                     // we increment the image number and wrap it eventually */
/*   //gt->c_i++;                     // we increment the image number and wrap it eventually */
/*   ci_1 = ci = gt->c_i; */
/*   ci++; */
/*   ci = (ci < gt->n_i) ? ci : 0; */



/*   gt->imi[ci] = n;                                 // we save image" number */
/*   gt->imit[ci] = n_inarow;                                  */
/*   gt->imt[ci] = t;                                 // image time */
/*   gt->imdt[ci] = dt; */
/*   gt->c_i = ci; */

/*   if (oi->bmp.stuff != NULL) imb = (BITMAP*)oi->bmp.stuff; */
/*   // we grab the video bitmap of the IFC image */
/*   prepare_image_overlay(oi); */

/*   gt->obj_pos[ci] = read_last_Z_value();	     */
/*   if (gt->n_b > 0) */
/*     { */
/*       cl = gt->cl;                   // cross arm length	 */
/*       cl2 = cl/2; */
/*       cw = gt->cw;                   // cross arm width	 */
/*       xi = gt->xi;	yi = gt->yi;     // these are temporary int buffers for averaged profile */
/*       xf = gt->xf;	yf = gt->yf;     // these are temporary float buffers for averaged profile */
/*       dis_filter = gt->dis_filter;   // this is the filter description */
  

/*       // loop scanning beads tracked */
/*       for (gt->c_b = 0; gt->c_b < gt->n_b; gt->c_b++) */
/* 	{ */
/* 	  bt = gt->bd[gt->c_b]; */
/* 	  proceed_bead(bt, gt, imb, oi); */
/* 	  bt->z[ci] = gt->obj_pos[ci] - bt->z[ci]; // new */
/* 	  // we draw bead in green if not lost in red otherwise */
/* 	  //if (gt->c_b == gt->n_b -1) draw_bubble(screen,0,550,50,"%d xc %f yc %f",gt->c_b,xcf,ycf);	     */
/* 	} */
/*       /\* */
/*       if (gt->obj_pos_cmd[gt->c_i] != gt->obj_pos[gt->c_i])     */
/* 	{ */
/* 	  set_Z_value(gt->obj_pos_cmd[gt->c_i]); */
/* 	  gt->status_flag[gt->c_i] |= OBJ_MOVING; */
/* 	} */
/*       *\/ */
/*     } */

/*   i = find_remaining_action(n); */
/*   //draw_bubble(screen,0,1000,450,"    %d actions to do     ",i); */

/*   while ((i = find_next_action(n)) >= 0) */
/*     { */
/*       //draw_bubble(screen,0,1000,390,"action ");	     */
/*       if (action_pending[i].type == MV_OBJ_ABS) */
/* 	{ */
/* 	  set_Z_value(action_pending[i].value); */
/* 	  gt->status_flag[ci] |= OBJ_MOVING; */
/* 	  gt->obj_pos[ci] = action_pending[i].value; */
/* 	  //draw_bubble(screen,0,1000,390,"setting abs Z to %f",gt->obj_pos[ci]);	     */
/* 	} */
/*       else if (action_pending[i].type == MV_OBJ_REL) */
/* 	{ */
/* 	  gt->obj_pos[ci] = gt->obj_pos[ci] + action_pending[i].value; */
/* 	  set_Z_value(gt->obj_pos[ci]); */
/* 	  gt->status_flag[ci] |= OBJ_MOVING; */
/* 	  //draw_bubble(screen,0,1000,390,"setting rel Z to %f",gt->obj_pos[ci]);	     */
/* 	} */
/*       else if (action_pending[i].type == MV_ZMAG_REL) */
/* 	{ */
/* 	  gt->zmag_cmd[ci] = gt->zmag_cmd[ci_1] + action_pending[i].value; */
/* 	  set_magnet_z_value(gt->zmag_cmd[ci]); */
/* 	  gt->status_flag[ci] |= ZMAG_MOVING; */
/* 	  do_update_menu(); */
/* 	  //draw_bubble(screen,0,1000,390,"setting rel Z to %f",gt->obj_pos[ci]);	     */
/* 	} */
/*       else if (action_pending[i].type == MV_ZMAG_ABS) */
/* 	{ */
/* 	  gt->zmag_cmd[ci] = action_pending[i].value; */
/* 	  set_magnet_z_value(gt->zmag_cmd[ci]); */
/* 	  gt->status_flag[ci] |= ZMAG_MOVING; */
/* 	  do_update_menu(); */
/* 	  //draw_bubble(screen,0,900,390,"setting rel Z to %f",gt->zmag_cmd[ci]);	     */
/* 	} */
/*       else if (action_pending[i].type == MV_ROT_REL) */
/* 	{ */
/* 	  gt->rot_mag_cmd[ci] = gt->rot_mag_cmd[ci_1] + action_pending[i].value; */
/* 	  set_rot_value(gt->rot_mag_cmd[ci]); */
/* 	  gt->status_flag[ci] |= ROT_MOVING; */
/* 	  do_update_menu(); */
/* 	  //draw_bubble(screen,0,1000,390,"setting rel Z to %f",gt->obj_pos[ci]);	     */
/* 	} */
/*       else if (action_pending[i].type == MV_ROT_ABS) */
/* 	{ */
/* 	  gt->rot_mag_cmd[ci] = action_pending[i].value; */
/* 	  set_rot_value(gt->rot_mag_cmd[ci]); */
/* 	  gt->status_flag[ci] |= ROT_MOVING; */
/* 	  do_update_menu(); */
/* 	  //draw_bubble(screen,0,1000,390,"setting rel Z to %f",gt->obj_pos[ci]);	     */
/* 	} */
/*       action_pending[i].proceeded = 1; */
/*     } */


/*   gt->zmag[ci] = (gt->status_flag[ci] & ZMAG_MOVING)  */
/*     ? read_magnet_z_value() : what_is_present_z_mag_value();  */
/*   gt->rot_mag[ci] = (gt->status_flag[ci] & ROT_MOVING)  */
/*     ? read_rot_value() : what_is_present_rot_value(); */
  
/*   if (fabs(gt->zmag_cmd[ci] - gt->zmag[ci]) < 0.005)     */
/*       gt->status_flag[ci] &= ~ZMAG_MOVING; */
/*   if (fabs(gt->rot_mag_cmd[ci] - gt->rot_mag[ci]) < 0.005)     */
/*     gt->status_flag[gt->c_i] &= ~ROT_MOVING; */



/*   gt->imdt[ci] = my_uclock() - dt; */
/*   if ((gt->local_lock & BUF_FREEZED) == 0) */
/*     { */
/*       gt->local_lock |= BUF_CHANGING;  // we prevent data changes */
/*       for ( ;gt->lac_i < gt->ac_i; gt->lac_i++) */
/* 	{   // we update local buffer */
/* 	  i = gt->lac_i%FLUO_BUFFER_SIZE; */
/* 	  gt->limi[i] = gt->imi[i];   */
/* 	  gt->limit[i] = gt->imit[i];   */
/* 	  gt->limt[i] = gt->imt[i];   */
/* 	  gt->limdt[i] = gt->imdt[i]; */
/* 	  gt->lc_i = i; */
/* 	}  */
/*       gt->local_lock = 0; */
/*     } */


/*   // no bead to track */

  return 0; 
}
END_OF_FUNCTION(track_in_x_y_timer)


int get_present_image_nb(void)
{
 return (fluo_info != NULL) ?  fluo_info->imi[fluo_info->c_i] : -1;
}                                 


g_fluo *creating_fluo_info(void)
{
  O_p *op;
  d_s *ds;

  if (fluo_info == NULL)
    {
      fluo_info = (g_fluo*)calloc(1,sizeof(g_fluo));
      if (fluo_info == NULL) return win_printf_ptr("cannot alloc track info!");	
      fluo_info->cl = -1;	    // so that we know it was not initialized
      fluo_info->m_b = 16;	    
      fluo_info->n_b = fluo_info->c_b = 0;
      fluo_info->bead_servo_nb = -1;
      fluo_info->n_bead_display = -1;
      fluo_info->bd = (b_fluo**)calloc(fluo_info->m_b,sizeof(b_fluo*));
      if (fluo_info->bd == NULL) return win_printf_ptr("cannot alloc track info!");	
      //starting_one = active_dialog; // to check if windows change
      

      // plots to check tracking
      op = pr_FLUO->o_p[0];
      set_op_filename(op, "Timing.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Timing rolling buffer");
      if ((ds = create_and_attach_one_ds(op, FLUO_BUFFER_SIZE, FLUO_BUFFER_SIZE, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Period rolling buffer");
      if ((ds = create_and_attach_one_ds(op, FLUO_BUFFER_SIZE, FLUO_BUFFER_SIZE, 0)) == NULL)
		return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Timer rolling buffer");
      op->op_idle_action = timing_rolling_buffer_idle_action;

      fluo_info->m_i = fluo_info->n_i = FLUO_BUFFER_SIZE;
      fluo_info->c_i = fluo_info->ac_i = fluo_info->lac_i = fluo_info->lc_i = 0;
      fluo_info->local_lock = 0;
      //      LOCK_FUNCTION(track_in_x_y_IFC);
      LOCK_FUNCTION(track_in_x_y_timer);
      LOCK_VARIABLE(fluo_info);
      
      fluo_bid.function = (fit *)calloc(1,sizeof(fit));
      fluo_bid.n_functions = 1;
      fluo_bid.function[0].function_param = (void*)fluo_info;
      fluo_bid.function[0].timer_do = track_in_x_y_timer;
      fluo_bid.function[0].source = (char*)calloc(64,sizeof(char));
      fluo_bid.function[0].source = "track_x_y_timer";
    }
  return fluo_info;
}


int init_fluo_info(void)
{
  if (fluo_info == NULL) 
    {
      fluo_info = creating_fluo_info();
      m_action = 256;
      n_action = 0;
      action_pending = (f_action*)calloc(m_action,sizeof(f_action));
      if (action_pending == NULL)
	win_printf_OK("Could not allocate action_pendind!");
    }
  return 0;
}


int switch_bead_of_interest(void)
{
  int tmp = 0;

  if (active_menu->text == NULL) return D_O_K;	
  sscanf((char*)active_menu->text,"Bead %d",&tmp); // != 1) return D_O_K;	
  if(updating_menu_state != 0)	
    {
      if (tmp == fluo_info->n_bead_display)
	active_menu->flags |=  D_SELECTED;
      else active_menu->flags &= ~D_SELECTED;			
      return D_O_K;	
    }

  if (tmp >= fluo_info->n_b || tmp < 0) return D_O_K;	
  fluo_info->n_bead_display = tmp;    
  return D_O_K;
}

int prepare_bead_menu(void)
{
  char st[128];
  register int i;
  for (i = 0; i < fluo_info->n_b; i++)
    { 
      if (bead_active_menu[i].text)
	{
	  free(bead_active_menu[i].text);
	  bead_active_menu[i].text = NULL;
	}
      sprintf(st,"Bead %d",i);
      bead_active_menu[i].text = strdup(st); 
      bead_active_menu[i].proc = switch_bead_of_interest;

    }
  if (bead_active_menu[i].text)
      free(bead_active_menu[i].text);
  bead_active_menu[i].text = NULL;
  return 0;
}

int follow_bead_in_x_y(void)
{
  register int i, di;
  imreg *imrs;	
  O_p *op;
  d_s *ds;
  O_i *ois, *oi = NULL;
  int xc = 256, yc = 256, color, bnx = 0, bcw = 0, dis_state;
  static int cl = 128, cw = 16, nf = 2048, imstart = 10, prof = 0, cl_size_selected = 0;
  b_fluo *bd = NULL;
  int mode = 0;
  float xb = 0, yb = 0, zb = 0, zobj;
  DIALOG *d;
	
  if(updating_menu_state != 0)	return D_O_K;
  di = find_dialog_focus(active_dialog);	
  mode = RETRIEVE_MENU_INDEX;
  if (ac_grep(NULL,"%im%oi",&imrs,&ois) != 2)	
    return win_printf_OK("cannot find image in acreg!");

  //if (ois != oi_IFC) return win_printf_OK("this is not the IFC image!");
  
  dis_state = do_refresh_overlay;   do_refresh_overlay = 0;
  //freeze_video();
  color = makecol(255,64,64);
  /* check for menu in win_scanf */
/*   if (mode == IMAGE_CAL) */
/*     { */
/*       //freeze_video(); */
/*       oi = do_load_calibration(0); */
/*       if (grab_bead_info_from_calibration(oi, &xb, &yb, &zb, &bnx, &bcw)) */
/* 	win_printf("error in retrieving bead data"); */
/*       cl = bnx; */
/*       cw = bcw; */
/*       //live_video(0); */
/*     } */
/*   else */ if (cl_size_selected == 0)
    {
      i = win_scanf("this routine track a bead in X,Y\n"
		    " using a cross shaped pattern \n"
		    "arm length %3d arm width %3d\n"
		    "how many frames %6d imstart %2d\n"
		    "profile %b\nkey {\\it b} black circle {\\it w} to stop\n"
		    ,&cl,&cw,&nf,&imstart,&prof); // %m ,select_power_of_2() 
      if (i == CANCEL)	return OFF; 
      cl_size_selected = 1;
    }
  if (cl > 128)	return win_printf_OK("cannot use arm bigger\nthan 128");
  if (cw > 32)	return win_printf_OK("cannot use width bigger\nthan 32");
  
  if (fft_init(cl) || filter_init(cl))	return win_printf_OK("cannot init fft!");	
  do_refresh_overlay = dis_state; 
  
  //live_video(0);
  xc = yc = 0;
  if ( mode == IMAGE_CAL)
    {
      xc = (int)(xb - oi->ax)/oi->dx;
      yc = (int)(yb - oi->ay)/oi->dy;
    }
  else
    {
      /*
      for(change_mouse_to_cross(cl, cw); !key[KEY_ENTER]; )
	{	
	  xc = mouse_x;
	  yc = mouse_y;
	  //d_draw_Im_proc(MSG_IDLE,d_FLUO,0);
	}
      reset_mouse();
      xc = (int)x_imr_2_imdata(imrs, xc);	
      yc = (int)y_imr_2_imdata(imrs, yc);	
      clear_keybuf();
      */
      xc = ois->im.nx + 2*cl/3;
      yc = cl/2 + fluo_info->n_b*cl;
      ois->need_to_refresh |= BITMAP_NEED_REFRESH;
    }					
  
  if (fluo_info == NULL) fluo_info = creating_fluo_info();
  if (fluo_info == NULL) return win_printf_OK("cannot alloc track info!");	
  if (fluo_info->cl == -1)	    
    {
      // we define tracking parameters
      fluo_info->cl = cl;	    
      fluo_info->cw = cw;	    
      fluo_info->bd_mul = 16;	    
      fluo_info->dis_filter = cl>>2;
      fluo_info->m_b = 16;	    
      fluo_info->n_b = fluo_info->c_b = 0;
      fluo_info->bead_servo_nb = -1;
      //starting_one = active_dialog; // to check if windows change
      //index_menu_dialog = retrieve_item_from_dialog(d_menu_proc, NULL);
      //index_menu_dialog = 1;


      // second plots to check tracking
      op = create_and_attach_one_plot(pr_FLUO,  FLUO_BUFFER_SIZE, FLUO_BUFFER_SIZE, 0);
      ds = op->dat[0];
      set_ds_source(ds, "X rolling buffer");
      if ((ds = create_and_attach_one_ds(op, FLUO_BUFFER_SIZE, FLUO_BUFFER_SIZE, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Y rolling buffer");
      if ((ds = create_and_attach_one_ds(op, FLUO_BUFFER_SIZE, FLUO_BUFFER_SIZE, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Z rolling buffer");
      uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);

/*       op->op_idle_action = z_rolling_buffer_idle_action; */
      set_op_filename(op, "X(t)Y(t)Z(t).gr");

      // third plot
      //win_printf("index %d %d",index_menu_dialog,di);
      op = create_and_attach_one_plot(pr_FLUO, fluo_info->cl, fluo_info->cl, 0);
      ds = op->dat[0];
      set_ds_source(ds, "Instantaneous radial profile");
      if ((ds = create_and_attach_one_ds(op,  fluo_info->cl, fluo_info->cl, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Reference radial profile");
/*       op->op_idle_action = profile_rolling_buffer_idle_action; */
      set_op_filename(op, "Profiles.gr");
      // fouth plot

      op = create_and_attach_one_plot(pr_FLUO,  FLUO_BUFFER_SIZE, FLUO_BUFFER_SIZE, 0);
      ds = op->dat[0];
      set_ds_source(ds, "Obj. rolling buffer");
      if ((ds = create_and_attach_one_ds(op, FLUO_BUFFER_SIZE, FLUO_BUFFER_SIZE, 0)) == NULL)
		return win_printf_OK("I can't create plot !");
      set_ds_source(ds, "Z rolling buffer");
      op->op_idle_action = obj_rolling_buffer_idle_action;
      set_op_filename(op, "Objective.gr");
      //fifth plot
      op = create_and_attach_one_plot(pr_FLUO,  FLUO_BUFFER_SIZE, FLUO_BUFFER_SIZE, 0);
      ds = op->dat[0];
      set_ds_source(ds, "X VS Y rolling buffer");
/*       op->op_idle_action = x_y_rolling_buffer_idle_action; */
      uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
      uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
      set_op_filename(op, "X VS Y.gr");

      op = create_and_attach_one_plot(pr_FLUO,  FLUO_BUFFER_SIZE, FLUO_BUFFER_SIZE, 0);
      ds = op->dat[0];
      set_ds_source(ds, "X VS Z rolling buffer");
/*       op->op_idle_action = x_z_rolling_buffer_idle_action; */
      uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
      uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_Y_UNIT_SET);
      set_op_filename(op, "X VS Z.gr");

      op = create_and_attach_one_plot(pr_FLUO,  FLUO_BUFFER_SIZE, FLUO_BUFFER_SIZE, 0);
      ds = op->dat[0];
      set_ds_source(ds, "Y VS Z rolling buffer");
/*       op->op_idle_action = y_z_rolling_buffer_idle_action; */
      uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
      uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
      set_op_filename(op, "Y VS Z.gr");


      for (i = 0, zobj = read_Z_value_accurate_in_micron(); i < FLUO_BUFFER_SIZE; i++)
	fluo_info->obj_pos_cmd[i] = zobj;

      fluo_info->m_i = fluo_info->n_i = FLUO_BUFFER_SIZE;
      //fluo_info->c_i = 0;
      //LOCK_FUNCTION(track_in_x_y_IFC);
      //LOCK_FUNCTION(track_in_x_y_timer);
      //LOCK_VARIABLE(fluo_info);
    }
  else
    {
      if (fluo_info->cl != cl || fluo_info->cw != cw)
	{
	  win_printf("Your arms do not have the right size!\n");
	  if (oi) free_one_image(oi);
	  return D_O_K;
	}	    
    }
  bd = (b_fluo*)calloc(1,sizeof(b_fluo));
  if (bd == NULL) return win_printf_OK("cannot alloc bead info!");	
  if (fluo_info->n_b >= fluo_info->m_b)
    {
      fluo_info->m_b += 16;	    
      fluo_info->bd = (b_fluo**)realloc(fluo_info->bd,fluo_info->m_b*sizeof(b_fluo*));
      if (fluo_info->bd == NULL) return win_printf_OK("cannot alloc track info!");	
    }
  for (i = 0; i < fluo_info->cl; i++) bd->rad_prof_ref[i] = -1; // cannot be a profile
  
  if (mode == IMAGE_CAL)
    {
      bd->calib_im = oi;
      bd->calib_im_fil = NULL;
    }
  else 
    {
      bd->calib_im = NULL;
      bd->calib_im_fil = NULL;
      bd->not_lost = -1;
    }
  bd->bp_center = 16;
  bd->bp_width = 12;
  bd->rc = 12;
  fluo_info->n_bead_display = fluo_info->n_b;    
  fluo_info->bd[fluo_info->n_b++] = bd;
  prepare_bead_menu();
  // win_printf("bead added %d",fluo_info->n_b);
  LOCK_VARIABLE(bd);
  bd->xc = bd->x0 = xc;
  bd->yc = bd->y0 = yc;
  bd->im_prof_ref = -1;
  bd->mouse_draged = 0;
  bd->not_lost = NB_FRAMES_BEF_LOST;
  bd->start_im = 0;
  if (fluo_bid.function == NULL) fluo_bid.function = (fit *)calloc(1, sizeof(fit));
  fluo_bid.function[0].function_param = (void*)fluo_info;
  //  bid.to_do = track_in_x_y_IFC;
  fluo_bid.function[0].timer_do = track_in_x_y_timer;

  d = find_dialog_associated_to_imr(imrs, NULL);
  write_and_blit_im_box( plt_buffer, d, imrs);
  //move_bead_cross(screen, imrs, d, xc, yc);

  return 0;
}



#endif
