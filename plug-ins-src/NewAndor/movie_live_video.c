#ifndef _MOVIE_LIVE_VIDEO_C_
#define _MOVIE_LIVE_VIDEO_C_

# include "allegro.h"
# include "winalleg.h"

# include "xvin.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//# include "fftbtl32n.h"
//# include "fillibbt.h"
//# include "focus.h"
//# include "magnetscontrol.h"
# include "PlayItSam.h"
# include "lockin_util.h"
//# include "brown_util.h"
//# include "track_util.h"
//# include "action.h"
//# include "calibration.h"
# include "movie_live_video.h"

char title[512];
char fullfile_time_lapse[512];
int iNumber_of_images_to_save = 128,iNumber_of_s_between_frames = 60, images_saved_for_time_lapse = 0;
O_i *time_lapse_movie_oi = NULL;
////////////////////////////////////////////////////////////////////////////////////////////////////
/////                 Save finite movie synchronized in the live_video thread through bid          ///
///////////////////////////////////////////////////////////////////////////////////////////////////

int save_finite_movie_in_live_video_thread(imreg *imr, O_i *oi, int n, DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow)
{
  static int i=0;
  O_i *oi_movie = (O_i*)p;

  snprintf(title,512,"saving image %d on %d",i,oi_movie->im.n_f);
  set_window_title(title);
  
  if (i < oi_movie->im.n_f)
    {
      memcpy(oi_movie->im.mem[i],oi_LIVE_VIDEO->im.mem[live_video_info->imi[live_video_info->c_i]%oi_LIVE_VIDEO->im.n_f],oi_LIVE_VIDEO->im.nx*oi_LIVE_VIDEO->im.ny*((oi_LIVE_VIDEO->im.data_type == IS_CHAR_IMAGE)?1:2)); 
      i++;
      snprintf(title,512,"saving image %d on %d",i,oi_movie->im.n_f);
      set_window_title(title);
    }
  else bid.next = NULL;

  return D_O_K;
}


int save_finite_movie(void)
{
  static int n_images_to_save = 256;
  O_i *oi = NULL;

  if(updating_menu_state != 0)	return D_O_K;
  win_scanf("You want to save how many images?%d\n Think about RAM size...\n Recording starts when mouse button is pressed",&n_images_to_save);
  
  oi = create_and_attach_movie_to_imr (imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx , oi_LIVE_VIDEO->im.ny, oi_LIVE_VIDEO->im.data_type , n_images_to_save);
  if (oi == NULL) return win_printf_OK("Oi could not be created");
  if (bid.timer_do == NULL) return win_printf_OK("Bid NULL");

  while (!(mouse_b & 0x3));
  
  if (bid.next == NULL)
    {
      win_printf("Bid allocation");
      bid.next = (Bid *)calloc(1, sizeof(Bid));
      bid.next->param = (void *)oi;
      bid.next->timer_do = save_finite_movie_in_live_video_thread;
    }

  return D_O_K;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////                Finite recording asynchronized                                                        /////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

DWORD WINAPI RecordMovieThreadProc( LPVOID lpParam )
{
  O_i* movie_oi= NULL;
  int i, k , iStarting_image;
  int iImages_in_movie;
  O_p *op = NULL;
  int one_image_on = 1;  // was 2 !
  //  char title[64];
  int bit_per_pixel;


  movie_oi = (O_i *)lpParam;

  if (movie_oi == NULL) return win_printf_OK("Movie could not be created");
  iImages_in_movie = movie_oi->im.n_f;
  snprintf(title,512,"Number of images %d",movie_oi->im.n_f);
  set_window_title(title);
  op = movie_oi->o_p[0];
  iStarting_image = live_video_info->imi[live_video_info->c_i];
  bit_per_pixel = (oi_LIVE_VIDEO->im.data_type == IS_CHAR_IMAGE) ? 1 : 2;

  for (i = iStarting_image , k = 0; i < iStarting_image + iImages_in_movie ; i++ , k++ )
    {
      while (live_video_info->imi[live_video_info->c_i] < iStarting_image + one_image_on*k)
	Sleep(2*one_image_on);
      memcpy(movie_oi->im.mem[k]/*i-iStarting_image]*/,
	     oi_LIVE_VIDEO->im.mem[(iStarting_image + one_image_on*k)%oi_LIVE_VIDEO->im.n_f],
	     oi_LIVE_VIDEO->im.nx*oi_LIVE_VIDEO->im.ny*bit_per_pixel);//Correct for data type
      op->dat[0]->xd[i-iStarting_image] = i-iStarting_image;
      op->dat[0]->yd[i-iStarting_image] = live_video_info->imi[live_video_info->c_i];
      snprintf(title,512,"Saved image %d on %d Saving one every %d image",k,iImages_in_movie,one_image_on);
      set_window_title(title); 
    }

  return D_O_K;
}

int create_record_movie_thread(O_i*oi)
{
  HANDLE hThread = NULL;
  DWORD dwThreadId;

  hThread = CreateThread( 
			 NULL,              // default security attributes
			 0,                 // use default stack size  
			 RecordMovieThreadProc,// thread function 
			 (void*)oi,       // argument to thread function 
			 0,                 // use default creation flags 
			 &dwThreadId);      // returns the thread identifier 

  if (hThread == NULL) 	return win_printf_OK("No thread created");
  SetThreadPriority(hThread,  THREAD_PRIORITY_TIME_CRITICAL);
  return 0;
}

int record_movie(void)
{
  int i;
  static int iImages_in_movie = 128;
  O_i *movie_oi = NULL;
 
  if(updating_menu_state != 0)	return D_O_K;

  i = win_scanf("You want to save a movie of how many images?%d" ,&iImages_in_movie);
  if (i == CANCEL) return D_O_K;
  if (oi_LIVE_VIDEO == NULL) return win_printf_OK("No tracking image");
  if (live_video_info == NULL)return win_printf_OK("No tracking info");

  movie_oi = create_and_attach_movie_to_imr (imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny, oi_LIVE_VIDEO->im.data_type, iImages_in_movie);
  if (movie_oi == NULL) return win_printf_OK("Movie could not be created");

 
  //  uns_oi_2_oi(movie_oi, oi_LIVE_VIDEO);
  //  inherit_from_im_to_im(movie_oi, oi_LIVE_VIDEO);

  create_and_attach_op_to_oi (movie_oi , iImages_in_movie ,iImages_in_movie , 0 , 0);
  win_printf("Created");  
  create_record_movie_thread(movie_oi);
  return D_O_K;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////                InFinite recording asynchronized                                                        ///////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// New file saving functions

////////////////////////////////////////////////////////////////////////////////
/** Modified .gr header saver.

IMPORTANT : superseds all previous (bugged) versions of save_image_header.
\param O_i The oi containing the information.
\param char A header text ??
\param int ??
\return int Error code.
\author JF Allemand
*/
int save_image_header_JF(O_i* oi, char *head, int size)
{
  FILE *fp;
  int nh;
  char filename[512];

  if (oi == NULL || oi->dir == NULL || oi->filename == NULL)	return 1;
  build_full_file_name(filename,512,oi->dir,oi->filename);

  fp = fopen(oi->filename,"r+b");
  if (fp == NULL)
    {
      error_in_file("%s\nfile not found!...",filename);
      return 1;
    }
  nh = find_CTRL_Z_offset(oi,fp);

  if (nh != size)
    {
      win_printf("the size of headers disagree!");
      return 1;
    }
	  
  if (fp == NULL)
    {
      error_in_file("%s\nfile not found!...",filename);
      return 1;
    }
	
  fseek(fp,0,SEEK_SET);
  if (fwrite (head,sizeof(char),size,fp) != size)
    {
      win_printf("Pb while writing header!");
      return 1;
    }
  fclose(fp);
  return 0;
}

////////////////////////////////////////////////////////////////////////////////
/** Modified file saver.

IMPORTANT : superseds all previous (bugged) versions of close_movie_on_disk.
\param O_i The oi containing the information.
\param int The number of frames in the movie.
\return int Error code.
\author JF Allemand
*/


char *read_image_header_JF(O_i* oi, int *size)
{
	FILE *fp;
	char filename[512], *buf;
	
	

	if (oi == NULL || oi->dir == NULL || oi->filename == NULL)
	  {
	    snprintf(title,512,"name namenamenamenamenamenamenamenamenamename");
	    set_window_title(title);
	    return NULL;
	  }

	build_full_file_name(filename, 512, oi->dir, oi->filename);
	fp = fopen (oi->filename, "rb");
	if ( fp == NULL )
	{
	  //error_in_file("%s\nfile not found!...",filename);
	  snprintf(title,512,"FilenameFilenameFilenameFilenameFilenameFilenameFilename");
	  set_window_title(title);
	  return NULL;
	}
	*size = find_CTRL_Z_offset(oi, fp);
	fseek(fp, 0, SEEK_SET);
	buf = (char*)calloc(*size+1,sizeof(char));
	if (buf == NULL)
	  {
	    snprintf(title,512,"FilenameFilename %d",*size);
	    set_window_title(title);
	    return NULL;
	  }
	if (fread (buf, sizeof (char), *size, fp) != *size)
	  {
	    //win_printf("Va donc chez speedy!",*size);
		if (buf)  {free(buf);  buf = NULL;}
		snprintf(title,512,"FilenameFilename %d",*size);
		set_window_title(title);

	    return NULL;
	  }
	 // win_printf("%s",buf);
	return buf;
}

int  close_movie_on_disk_JF(O_i *oi, int nf)
{
  char *head, *found, tmp[256];
  int size = 0, nff = -1, cff = -1, i;
  extern char *read_image_header(O_i* oi, int *size);

  
  head = read_image_header_JF(oi, &size);
  if (head == NULL)  return 1;
  found = strstr(head,"-nf");
 
  if (found != NULL)    
    {
     snprintf(tmp,256,"-nf %010d %010d\r\n",nf, oi->im.c_f);
      for (i = 0; (i < 256) && (tmp[i] != 0); i++) found[i] = tmp[i]; 
    }
  else win_printf("did not found -nf symbol");
  sscanf(found,"-nf %d %d",&nff,&cff);
  // win_printf("%s",found);
  if (oi->im.record_duration != 0)
    {
      found = strstr(head,"-duration");
      if (found != NULL) 	
	{
      	  snprintf(tmp,256,"-duration %016.6f \r\n",oi->im.record_duration);
      	  for (i = 0; (i < 256) && (tmp[i] != 0); i++) found[i] = tmp[i]; 
	}
      else win_printf("did not found -duration symbol");
    }
  save_image_header_JF(oi, head,  size);
  if (head) {free(head); head = NULL;}
  return 0;
}




DWORD WINAPI RecordSaveTapeModeThreadProc( LPVOID lpParam )
{
  char fullfile[512];
  int num_files;
  char path[512];
  char file[512];
  char common_name[64];
  O_i *oi = NULL;
  FILE *fp_images = NULL; 
  Save_Tape_Mode_Param SaveTapeModeParam;
  int n_images_saved = 1;
  int previous_grab_stat = 0;  
  int n_images_to_be_saved = 0;
  int previous_image_for_saving;

  SaveTapeModeParam = *((Save_Tape_Mode_Param*)(lpParam));
  num_files = SaveTapeModeParam.num_files;
  snprintf ( path , 512,SaveTapeModeParam.path ) ;
  snprintf ( common_name , 64,SaveTapeModeParam.common_name);
  oi = create_one_image (oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny , oi_LIVE_VIDEO->im.data_type);

  while (keypressed()!=TRUE)
    {   if (mouse_b & 1)
	{
	  fullfile[0] = '\0';
	  num_files = get_config_int("TAPE_SAVE_MODE","filenumber",-1);
	  if (num_files < 0) num_files = 0;
	  snprintf(file,512,"%s%04d.gr",common_name,num_files++);//on rajoute le nombre a incrementer et le format
	  strncat(fullfile,path,512-strlen(fullfile));
	  strncat(fullfile,file,512-strlen(fullfile));
	  
	  oi->im.movie_on_disk = 1;
	  oi->dir = strdup(path);
	  oi->filename = strdup(file);
	  oi->im.time = 0;
	  oi->im.n_f= 0;
	  set_image_starting_time(oi);
	  uns_oi_2_oi(oi, oi_LIVE_VIDEO);
	  
	  
	  set_image_ending_time (oi);
	  save_one_image(oi, fullfile);/**sauve en tete et les parametres**/
	  
	  set_config_int("TAPE_SAVE_MODE","filenumber",num_files);
	  fp_images = fopen (fullfile, "ab");
	  if ( fp_images == NULL )
	    {
	      win_printf("Cannot open the file!");
	      movie_buffer_on_disk = 0;
	      return 1;
	    }
	    
	  previous_image_for_saving = 0;
	  previous_grab_stat = live_video_info->imi[live_video_info->c_i];
	  n_images_saved = 0;
	  set_image_starting_time(oi);
	  
	  while (!(mouse_b & 2))//stop saving with the mouse right button
	    {
	      n_images_to_be_saved = live_video_info->imi[live_video_info->c_i] - previous_grab_stat - n_images_saved;
	      if (n_images_to_be_saved > 0)
		{
		  fwrite (oi_LIVE_VIDEO->im.mem[( live_video_info->imi[live_video_info->c_i] - n_images_to_be_saved)%oi_LIVE_VIDEO->im.n_f + 1],(oi_LIVE_VIDEO->im.data_type == IS_CHAR_IMAGE) ? 1 : 2 ,oi_LIVE_VIDEO->im.nx * oi_LIVE_VIDEO->im.ny,fp_images);
		  n_images_saved++;
		  snprintf(title,512,"Currently saving image %d",n_images_saved+1);
		  set_window_title(title);
		  
		  if (n_images_to_be_saved > oi_LIVE_VIDEO->im.n_f)
		    {
		      fclose(fp_images);
		      set_image_ending_time (oi);
		      close_movie_on_disk_JF(oi,n_images_saved);
		      return win_printf("One image lost...Stop real time saving");
		    }
		}
	    }
	  
	  fclose(fp_images);
	  set_image_ending_time (oi);
	  close_movie_on_disk_JF(oi,n_images_saved);
	}
    }
  movie_buffer_on_disk = 0;
  return D_O_K;
  
}


int create_save_tape_mode_live_video_thread(char path[512], char common_name[20], int num_files)
{
  HANDLE hThread = NULL;
  DWORD dwThreadId;
  Save_Tape_Mode_Param SaveTapeModeParam;

  SaveTapeModeParam.num_files = num_files;
  snprintf(SaveTapeModeParam.path,512,path);
  snprintf(SaveTapeModeParam.common_name,64,common_name);

  hThread = CreateThread( 
			 NULL,              // default security attributes
			 0,                 // use default stack size  
			 RecordSaveTapeModeThreadProc,// thread function 
			 (void*)&SaveTapeModeParam,       // argument to thread function 
			 0,                 // use default creation flags 
			 &dwThreadId);      // returns the thread identifier 

  if (hThread == NULL) 	return win_printf_OK("No thread created");
  SetThreadPriority(hThread,  THREAD_PRIORITY_TIME_CRITICAL);
  return 0;
}




int save_tape_mode_from_live_video(void)
{
  //  FILE *fp_images = NULL;
  //int n_images_saved = 1;
  imreg *imr;
  O_i *oi;
  //unsigned char *buf;
  int num_files = 0;
  char filenamend[64];
  char common_name[20]="movie";
  //int previous_grab_stat = 0;  
  char fullfile[512];
  register int i;
  char path[512], file[256], *pa; 
  //int n_images_to_be_saved = 0;
    
  if(updating_menu_state != 0)	return D_O_K;
	
  if (imr_LIVE_VIDEO == NULL)	return 1;
  if (oi_LIVE_VIDEO == NULL) return 1;
	
  imr = imr_LIVE_VIDEO;
  oi = oi_LIVE_VIDEO;

  movie_buffer_on_disk = 1;    		
  switch_allegro_font(1);
  pa = (char*)get_config_string("IMAGE-GR-FILE","last_saved",NULL);	
  if (pa != NULL)		extract_file_path(fullfile, 512, pa);
  else				my_getcwd(fullfile, 512);
  strncat(fullfile,"\\",512);//Jusqu'ici c'est le path 
		
  num_files = get_config_int("TAPE_SAVE_MODE","filenumber",-1);
  win_scanf("Characteristic string for the files? %s\n Initial number for file%d?",common_name,&num_files);
		
  strncat(fullfile,common_name,512);//on rajoute le debut du nom specifique
		
  snprintf(filenamend,64,"%04d.gr",num_files);//on rajoute le nombre a incrementer et le format
  strncat(fullfile,filenamend,512-strlen(fullfile));
    	
				
  i = file_select_ex("Save real time movie", fullfile, "gr", 512, 0, 0);
  switch_allegro_font(0);

  if (i != 0) 	
    {
      set_config_string("IMAGE-GR-FILE","last_saved",fullfile);			
      extract_file_name(file, 256, fullfile);
      extract_file_path(path, 512, fullfile);
      strncat(path,"\\",512-strlen(path));
    }
  else 
    {	
      movie_buffer_on_disk = 1;
      return 0;
    }
	
  set_config_int("TAPE_SAVE_MODE","filenumber",num_files);
  
   create_save_tape_mode_live_video_thread(path,common_name,num_files);	
 
  return 0;
}


int start_aquisition_tape(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  save_tape_mode_from_live_video();
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
/** A timer killer function. 

Specific to this application.
\param None.
\return int Error code.
\author JF Allemand
*/ 
int Kill_timer(void)
{
  if (KillTimer(win_get_window(),25) == FALSE) return win_printf_OK("Timer not killed");  //stops the background task 
  return 0;   
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// ROM acquisition functions

////////////////////////////////////////////////////////////////////////////////
/** A timer killer function. 

Specific to this application.
\param None.
\return int Error code.
\author JF Allemand
*/ 
int Kill_time_lapse_timer(void)
{
  if (KillTimer(win_get_window(),26) == FALSE) return win_printf_OK("Timer not killed");  //stops the background task 
  return D_O_K;//win_printf_OK("Timer killed");   
}


void CALLBACK automatic_time_lapse_saving(HWND hwnd, UINT uMsg, UINT_PTR idEvent,  DWORD dwTime)
{
    FILE *fp = NULL;
    int nw;

    fp = fopen (fullfile_time_lapse, "ab");
    if ( fp == NULL ) 	
    {    
      Kill_time_lapse_timer();  
      return ;
    }
    snprintf(title,512,"before write");
    set_window_title(title);
    nw = fwrite (oi_LIVE_VIDEO->im.mem[(oi_LIVE_VIDEO->im.n_f != 0) ? ((live_video_info->imi[live_video_info->c_i]-1)%oi_LIVE_VIDEO->im.n_f) : 0],(oi_LIVE_VIDEO->im.data_type == IS_CHAR_IMAGE) ? 1 : 2 , oi_LIVE_VIDEO->im.nx*oi_LIVE_VIDEO->im.ny,fp);
    fclose(fp);

    images_saved_for_time_lapse++;
    close_movie_on_disk_JF(time_lapse_movie_oi ,images_saved_for_time_lapse);
    snprintf(title,512,"Currently saving image %d expecting %d",images_saved_for_time_lapse, iNumber_of_images_to_save);
    set_window_title(title);
     if (images_saved_for_time_lapse >= iNumber_of_images_to_save)
    {
	  Kill_time_lapse_timer();
    }
     // set_image_ending_time(time_lapse_movie_oi);

    return;
}

int time_lapse(void)
{
  int i;
  char common_name[20]="movie";
  int num_files = 0;
  char path[512], filenamend[256], file[256];//, *pa; 
  //  FILE *fp = NULL;
  //int nw;
  //char add_source[256];
  if(updating_menu_state != 0)	return D_O_K;
  
  i = win_scanf("You want to save a movie of how many images?%d \n Number of seconds between 2 frames? %d",&iNumber_of_images_to_save,&iNumber_of_s_between_frames);
  if (i == CANCEL) return D_O_K;
    
  if (oi_LIVE_VIDEO == NULL) return win_printf_OK("No image to save");
  if (live_video_info == NULL)return win_printf_OK("No tracking info");

///////////////////////////SELECT FILENAME////////////////////////////////////////////  
   movie_buffer_on_disk = 1;  
     		
  num_files = get_config_int("TIME_LAPSE_SAVE_MODE","filenumber",1);
  win_scanf("Name for the files? %s\n Initial number for file%d?",common_name,&num_files);

  //fullfile_time_lapse[0] = '\0';		
  //strcat(fullfile_time_lapse,common_name);//on rajoute le debut du nom specifique
  //sprintf(filenamend,"%04d.gr",num_files);//on rajoute le nombre a incrementer et le format
  //strcat(fullfile_time_lapse,filenamend);

  snprintf(fullfile_time_lapse,512,"%s%04d.gr",common_name,num_files);
  i = file_select_ex("Save time lapse movie", fullfile_time_lapse, "gr", 512, 0, 0);
  switch_allegro_font(0);
  if (i != 0)
    {
      set_config_string("IMAGE-GR-FILE","last_saved",fullfile_time_lapse);
      extract_file_name(file, 256, fullfile_time_lapse);
      extract_file_path(path, 512, fullfile_time_lapse);
      strncat(path,"\\",512-strlen(path));
    }
  else
    {
      movie_buffer_on_disk = 1;
      return 0;
    }
  set_config_int("TIME_LAPSE_SAVE_MODE","filenumber",++num_files);
  /////////////////////////////////////////////////////////////////////////////////
  if (time_lapse_movie_oi == NULL)
    {
      if((time_lapse_movie_oi = (O_i *)calloc(1,sizeof(O_i))) == NULL)
	return win_printf("Our are out of memory guy");
      if (init_one_image(time_lapse_movie_oi, IS_CHAR_IMAGE)) 		return win_printf("You have a little problem guy");

      uns_oi_2_oi(time_lapse_movie_oi, oi_LIVE_VIDEO);
      inherit_from_im_to_im(time_lapse_movie_oi, oi_LIVE_VIDEO);
    }

 		
 		
/* 	set_image_starting_time(oi_buffer_tape_CVB); */
/* 	set_image_ending_time (oi_buffer_tape_CVB); */
		
  time_lapse_movie_oi->im.movie_on_disk = 1;
  time_lapse_movie_oi->dir = strdup(path);
  time_lapse_movie_oi->filename = strdup(fullfile_time_lapse);//pb here
  time_lapse_movie_oi->im.time = 0;
  time_lapse_movie_oi->im.n_f= 0;//iNumber_of_images_to_save;
  time_lapse_movie_oi->im.data_type = (oi_LIVE_VIDEO->im.data_type == IS_CHAR_IMAGE) ? IS_CHAR_IMAGE : IS_UINT_IMAGE;
  time_lapse_movie_oi->im.nx = oi_LIVE_VIDEO->im.nx;
  time_lapse_movie_oi->im.ny = oi_LIVE_VIDEO->im.ny;
  //set_image_starting_time(time_lapse_movie_oi);

 /*  sprintf(add_source," Time lapse interval %d s",iNumber_of_s_between_frames); */
/*   strcat(time_lapse_movie_oi->im.source,add_source); */
  set_oi_source(time_lapse_movie_oi," Time lapse interval %d s",iNumber_of_s_between_frames);
  //set_image_starting_time(time_lapse_movie_oi);
  //set_image_ending_time (time_lapse_movie_oi);
  save_one_image(time_lapse_movie_oi, fullfile_time_lapse);/**sauve en tete et les parametres**/

  set_config_int("TIME_LAPSE_SAVE_MODE","filenumber",num_files);

  images_saved_for_time_lapse = 0;
  snprintf(title,512,"before timer");
  set_window_title(title);
 

  SetTimer( win_get_window(),26,iNumber_of_s_between_frames*1000,automatic_time_lapse_saving);/*sets a background saving*/
  
  return D_O_K;
}


MENU *save_movie_in_live_video_image_menu(void)
{
	static MENU mn[32];
	

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Save Tape Mode", save_tape_mode_from_live_video,NULL,0 ,NULL);
	add_item_to_menu(mn,"Record Movie", record_movie,NULL,0 ,NULL);
	add_item_to_menu(mn,"Record movie within Track thread", save_finite_movie,NULL,0 ,NULL);
	add_item_to_menu(mn,"Time Lapse saving", time_lapse,NULL,0 ,NULL);

	return mn;
}


#endif

