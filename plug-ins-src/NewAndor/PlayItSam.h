#ifndef _PLAYITSAM_H_
#define _PLAYITSAM_H_


typedef struct before_image_display
{
  int (*to_do)(imreg *imr, O_i *oi, int n, DIALOG *d, long long t, unsigned long dt, void *p);
  void *param;
  int previous_fr_nb;
  unsigned long timer_dt;
  int previous_in_time_fr_nb;
  int first_im;                     // the nb of the first image
  long long previous_in_time_fr;
  int lost_fr;
  struct before_image_display *next;
  int (*timer_do)(imreg *imr, O_i *oi, int n, DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow);
  double image_per_in_ms;
  double min_delay_camera_thread_in_ms;
} Bid;



typedef struct thread_image_data
{
  imreg *imr;
  pltreg *pr;
  O_p *op;
  O_i *oi;
  DIALOG *dpr, *dimr;
  Bid *dbid;
} tid;


# define LIVE_VIDEO_FREEZE   2
# define LIVE_VIDEO_ON       1
# define LIVE_VIDEO_STOP     0

// job type

// this is lower priority routine done in the general_idle_action
// typically grabbing the calibration image, force measurements etc
typedef struct future_job
{
  int imi;             // the image nb where to start action, if < 0 the next possible frame
  int last_imi;        // an internal idex used to avoid calling several time this job for the same image
  int in_progress;    // 0 if finished, anything else if active
  int type;
  int bead_nb;
  int local;
  imreg *imr;
  O_i *oi;
  pltreg *pr;
  O_p *op;
  void *more_data ;                       // data structure for function
  int (*job_to_do)(int im, struct future_job *job);
} f_job;



# ifndef _PLAYITSAM_C_
//extern  __declspec(dllexport) int go_live_video ;             // flag on means tracking
PXV_VAR(int, go_live_video);
PXV_VAR(tid, dtid);
PXV_VAR(int, do_refresh_overlay);
PXV_VAR(Bid, bid);
PXV_VAR(imreg  *,imr_LIVE_VIDEO);
PXV_VAR(pltreg  *,pr_LIVE_VIDEO);
PXV_VAR(O_i  *, oi_LIVE_VIDEO);
PXV_VAR(DIALOG *, d_LIVE_VIDEO);
PXV_VAR(int, source_running);
PXV_VAR(char , running_message[]);
PXV_VAR(unsigned long, dt_simul);

PXV_VAR(f_job*, job_pending);
PXV_VAR(int, m_job);
PXV_VAR(int, n_job);
PXV_VAR(int, mouse_selected_bead);

PXV_VAR(int, last_im_who_asked_menu_update);  // switch in the tracking thread
PXV_VAR(int, last_im_where_menu_was_updated); // done in the general idle action 
PXV_VAR(int, imr_and_pr);  // image and plot display together
PXV_VAR(int, imr_LIVE_VIDEO_redraw_background);
PXV_VAR(int, imr_LIVE_VIDEO_title_change_asked);
PXV_FUNCPTR(char*, generate_imr_LIVE_VIDEO_title, (void));
# endif
# ifdef _PLAYITSAM_C_

char* 	(*generate_imr_LIVE_VIDEO_title)(void);
int imr_LIVE_VIDEO_title_change_asked = 0;
int imr_LIVE_VIDEO_title_displayed = 0;

int go_live_video = 0;             // flag on means tracking
tid dtid;                     // the data passed to the thread proc
int do_refresh_overlay = 0;
Bid bid={NULL,NULL,0,0,0,0,0,0,NULL,NULL};

imreg  *imr_LIVE_VIDEO = NULL;
pltreg  *pr_LIVE_VIDEO = NULL;
O_i  *oi_LIVE_VIDEO = NULL;
DIALOG *d_LIVE_VIDEO = NULL;
int source_running = 0;
char running_message[128];
unsigned long dt_simul = 0;

int imr_and_pr = 0;

f_job *job_pending = NULL;
int m_job = 0;
int n_job = 0;
int last_im_who_asked_menu_update = 0;  // switch in the tracking thread
int last_im_where_menu_was_updated = 0; // done in the general idle action 


int imr_LIVE_VIDEO_redraw_background = 0;


# endif


PXV_FUNC(MENU*, playitsam_image_menu, (void));
//PXV_FUNC(MENU*, playitsam_ex_image_menu, (void));
PXV_FUNC(MENU*, playitsam_plot_menu, (void));
PXV_FUNC(int, playitsam_main, (int argc, char **argv));
PXV_FUNC(int, init_image_source,(imreg *imr, int mode));
PXV_FUNC(int, start_data_movie, (imreg *imr));
PXV_FUNC(DWORD WINAPI, LiveVideoThreadProc, (LPVOID lpParam)); 
PXV_FUNC(int, live_source, (void));
PXV_FUNC(int, freeze_source, (void));
PXV_FUNC(int, kill_source, (void));
PXV_FUNC(int, prepare_image_overlay, (O_i* oi));
PXV_FUNC(int, follow_bead_in_x_y,(void));
PXV_FUNC(int, live_video_oi_idle_action, (struct  one_image *oi, DIALOG *d));


PXV_FUNC(int, fill_next_available_job_spot, (int in_progress, int im, int type, int bead_nb, imreg *imr, O_i *oi, pltreg *pr, O_p *op, void* more_data, int (*job_to_do)(int im, struct future_job *job),int local));

PXV_FUNC(f_job*, find_job_associated_to_image,(O_i *oi));
PXV_FUNC(f_job*, find_job_associated_to_plot,(O_p *op));

PXV_FUNC(int, init_live_video_info, (void));

#endif

