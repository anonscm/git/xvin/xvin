#ifndef _LOCKIN_UTIL_C_
#define _LOCKIN_UTIL_C_



# include "allegro.h"
# include "winalleg.h"
# include "ctype.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"





/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../phaseindex/phaseindex.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

# include "PlayItSam.h"
# include "lockin_util.h"
# include "action_live_video.h"
//# include "focus.h"

int vco_phase_pid(long long t,      // the time stamp to determine phase
		  float error,      // error signal
		  int mode,         // mode = 0, return P and I, else change their value
		  float *P,         // proportionnal 
		  float *I,         // integralal
		  int *per,         // if > 0 define the new period of the vco else
                                    // return perl the present vco period
		  int *n_cycle,     // the number of cycles since started
		  int *cycl_ph      // the position inside cycle [0 to perl]
		  );

void phase_timer(void)
{
  int ci, phi1, phi2;
  
  ci = ph_lock.c_i;
  ci++;
  if (ci >= ph_lock.n_i) ci = 0;
  phi1 = ph_lock.phase2[ci] = do_read_db25();  
  ph_lock.imt[ci] =  my_ulclock(); 
  phi2 = do_read_db25();  
  if (ph_lock.ac_i == 0) 
    {
      ph_lock.phase2[0] = phi1;
      ph_lock.imt[0] = ph_lock.imt[1];
      ph_lock.imt0 = ph_lock.imt[ci];
    }
  if (phi1 == phi2)
    {
      ph_lock.c_i = ci;
      ph_lock.ac_i++;
    }
   
}
END_OF_FUNCTION(phase_timer)

int check_heap_raw(char *file, unsigned long line)
{
  int out;

  out = _heapchk();
  if (out != -2)  win_printf("File %s at line %d\nheap satus %d (-2 =>OK)\n",file,(int)line,out);
  return (out == -2) ? 0 : out;
}
	
# ifdef OLD
int timing_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j, k;
  static int  immax = -1; // last_c_i = -1,
  d_s *dsx, *dsy, *dsz;
  int nf, lost_fr, lf, jstart = 0, j1 = 0;
  unsigned long   dtm, dtmax, dts;
  float   fdtm, fdtmax;
  double  tmp, tmp2;
  long long llt;

  if (lockin_info == NULL) return D_O_K;
  dts = get_my_uclocks_per_sec();
  dsx = find_source_specific_ds_in_op(op,"Timing rolling buffer");
  dsy = find_source_specific_ds_in_op(op,"Period rolling buffer");
  dsz = find_source_specific_ds_in_op(op,"Timer rolling buffer");
  if (dsx == NULL || dsy == NULL || dsz == NULL) return D_O_K;	

  i = lockin_info->c_i;
  if (lockin_info->imi[i] <= immax) 
    {
        my_set_window_title("waiting %d %d",lockin_info->imi[i], immax);
	return D_O_K;   // we are up todate
    }


  i = lockin_info->c_i;
  immax = lockin_info->imi[i];               
  lf = immax - lockin_info->ac_i;
  if (lockin_info->ac_i > (LOCKIN_BUFFER_SIZE>>1)) 
    {
      nf = LOCKIN_BUFFER_SIZE>>1;
      j = jstart = (lockin_info->ac_i - (LOCKIN_BUFFER_SIZE>>1))%LOCKIN_BUFFER_SIZE;  //last_c_i;
    }
  else
    {
      nf = lockin_info->ac_i-1;
      j = jstart = 1;
    }
  dsx->nx = dsx->ny = nf;
  dsy->nx = dsy->ny = nf;
  dsz->nx = dsz->ny = nf;
  j = jstart + nf - 1;
  j = (j < LOCKIN_BUFFER_SIZE) ? j : j - LOCKIN_BUFFER_SIZE;
  llt = lockin_info->imt[j];
  j1 = j;
  j = jstart;
  j = (j < LOCKIN_BUFFER_SIZE) ? j : j - LOCKIN_BUFFER_SIZE;
  llt -= lockin_info->imt[j];
  llt /= (nf - 1);
  
  for (i = 0, j = jstart, lost_fr = 0, dtm = dtmax = 0, fdtm = fdtmax = 0, k = 0, tmp2 = 0, lf = 0; i < nf; i++, j++, k++)
    {
      j = (j < LOCKIN_BUFFER_SIZE) ? j : j - LOCKIN_BUFFER_SIZE;
      dsx->xd[i] = dsy->xd[i] = dsz->xd[i] = lockin_info->imi[j];
      if (i > 0)
	lf += lockin_info->imi[j] - lockin_info->imi[j1] - 1;
      dsx->yd[i] = ((float)(1000*lockin_info->imdt[j]))/dts;
      tmp = lockin_info->imt[j];
      tmp -= (i == 0) ? lockin_info->imt[j] : lockin_info->imt[j1];
      tmp = 1000*(double)(tmp)/get_my_ulclocks_per_sec();
      //tmp -= bid.image_per_in_ms * i;
      tmp2 += tmp;
      dsy->yd[i] = (float)tmp;
      dsz->yd[i] = lockin_info->imit[j];

      dtm += lockin_info->imdt[j];
      fdtm += dsx->yd[i];
      dtmax = (lockin_info->imdt[j] > dtmax) ? lockin_info->imdt[j] : dtmax;
      fdtmax = (dsx->yd[i] > fdtmax) ? dsx->yd[i] : fdtmax;
      j1 = j;
    }
  dsy->yd[0] =  dsy->yd[1];
  op->need_to_refresh = 1; 
  if (k) set_plot_title(op, "\\stack{{fr position lac_i %d }{%d missed images}"
			"{\\pt8 dt mean %6.3f ms (%6.3f ms) max %6.3f (%6.3f)}}"
			,bid.first_im +lockin_info->ac_i 
			,lf,fdtm/k,
			1000*((double)(dtm/k))/dts,
			fdtmax, 1000*((double)(dtmax))/dts);

  i = LOCKIN_BUFFER_SIZE/8;
  j = ((int)dsx->xd[1])/i;
  op->x_lo = j*i;
  op->x_hi = (j+5)*i;
  set_plot_x_fixed_range(op);
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}
# endif
//# ifdef OLD
int phase_in_imagethread_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  static int last_c_i = -1, immax = -1;
  d_s *dsx;
  int nf;
  
 
  if (lockin_info == NULL) return D_O_K;
  dsx = find_source_specific_ds_in_op(op,"Phase expected");
  if (dsx == NULL) return D_O_K;	
  

  i = lockin_info->c_i;
  if (lockin_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = lockin_info->imi[i];   
              
  nf = (lockin_info->ac_i > LOCKIN_BUFFER_SIZE) ? LOCKIN_BUFFER_SIZE : lockin_info->ac_i;      
  nf = (nf > (LOCKIN_BUFFER_SIZE>>1)) ? LOCKIN_BUFFER_SIZE>>1 : nf;
  dsx->nx = dsx->ny = nf;
  
  
  last_c_i = (lockin_info->c_i > 0) ? lockin_info->c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? LOCKIN_BUFFER_SIZE + j: j;
      
      //dsx->yd[i]= (float)(1000*(double)(lockin_info->imt[j] - lockin_info->imt0)/get_my_ulclocks_per_sec() - lockin_info->delta[j]); 
      dsx->yd[i]= lockin_info->phase[j]; 
     
      dsx->xd[i] = lockin_info->imi[j];
     
      
    }
   
  i = LOCKIN_BUFFER_SIZE/8;
  j = ((int)dsx->xd[i])/i;
  op->x_lo = (j-1)*i;
  op->x_hi = (j+4)*i;
  set_plot_x_fixed_range(op);

  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d}{period %8f}{phi start %d}}"
  		 ,lockin_info->ac_i, (float)(1000*(double)lockin_info->per_i/get_my_ulclocks_per_sec()), lockin_info->ph_start);
  
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}

//# endif

int phase_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j   ;
  static int last_c_i = -1, immax = -1;
  d_s *dsx, *dsy, *dsz;
  int nf, b_size;
  double tmpd;
    
  if (lockin_info == NULL) return D_O_K;
 
  dsx = find_source_specific_ds_in_op(op,"Modulation phase measured");
  if (dsx == NULL) return D_O_K;
  dsy = find_source_specific_ds_in_op(op,"Modulation phase simulated"); 
  if (dsy == NULL) return D_O_K; 
  dsz = find_source_specific_ds_in_op(op,"Error");
  if (dsz == NULL) return D_O_K;
 
  i = ph_lock.c_i;
  if (ph_lock.ac_i <= immax) return D_O_K;   // we are uptodate
  immax = ph_lock.ac_i;   

  b_size = 50*LOCKIN_BUFFER_SIZE/3;            
  nf = (ph_lock.ac_i > b_size) ? b_size : ph_lock.ac_i;      
  nf = (nf > (b_size>>4)) ? (b_size>>4) : nf;
  dsx->nx = dsx->ny = nf;
  dsy->nx = dsy->ny = nf;
  dsz->nx = dsz->ny = nf;
  
  
  last_c_i = (ph_lock.c_i > 0) ? ph_lock.c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--) 
    {
      j = (j < 0) ? b_size + j: j;
      tmpd = 1000*((double)(ph_lock.imt[j] - ph_lock.imt0))/get_my_ulclocks_per_sec();
      dsx->xd[i] = (float)tmpd;
      dsx->yd[i] = (float)ph_lock.phase2[j];
      dsy->xd[i] = (float)tmpd;
      dsy->yd[i] = (float)ph_lock.phase1[j];
      dsz->xd[i] = (float)tmpd;
      dsz->yd[i] = (float)ph_lock.dephi[j];
    }
  op->x_lo = dsx->xd[0] - (dsx->xd[dsx->nx-1] - dsx->xd[0])/20;  
  op->x_hi = dsx->xd[dsx->nx-1] + (dsx->xd[dsx->nx-1] - dsx->xd[0])/20;
  set_plot_x_fixed_range(op);
   
  
  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d}{Period %d}{Period in ms %8f}{d\\phi %g}}"
		 ,ph_lock.ac_i, lockin_info->per_i,(float)(1000*(double)lockin_info->per_i/get_my_ulclocks_per_sec()),lockin_info->dphi_t);
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


int obj_rolling_buffer_idle_action(O_p *op, DIALOG *d)
{
  register int i, j;
  static int last_c_i = -1, immax = -1;
  d_s *dsx;
  int nf;
  
  if (lockin_info == NULL) return D_O_K;

  dsx = find_source_specific_ds_in_op(op,"Obj. rolling buffer");
  if (dsx == NULL) return D_O_K;	

  i = lockin_info->c_i;
  if (lockin_info->imi[i] <= immax) return D_O_K;   // we are uptodate
  immax = lockin_info->imi[i];               
  nf = (lockin_info->ac_i > LOCKIN_BUFFER_SIZE) ? LOCKIN_BUFFER_SIZE : lockin_info->ac_i;
  nf = (nf > (LOCKIN_BUFFER_SIZE>>1)) ? LOCKIN_BUFFER_SIZE>>1 : nf;
  dsx->nx = dsx->ny = nf;
  last_c_i = (lockin_info->c_i > 0) ? lockin_info->c_i - 1 : 0;  // c_i is not yet valid
  for (i = nf-1, j = last_c_i; i >= 0; i--, j--)
    {
      j = (j < 0) ? LOCKIN_BUFFER_SIZE + j: j;
      j = (j < LOCKIN_BUFFER_SIZE) ? j : LOCKIN_BUFFER_SIZE;
      dsx->xd[i] = lockin_info->imi[j];
      dsx->yd[i] = lockin_info->obj_pos[j];
    }
  i = LOCKIN_BUFFER_SIZE/8;
  j = ((int)dsx->xd[i])/i;
  op->x_lo = (j-1)*i;
  op->x_hi = (j+4)*i;
  set_plot_x_fixed_range(op);

  op->need_to_refresh = 1; 
  set_plot_title(op, "\\stack{{image %d}{Obj %g obj}}"
		 ,lockin_info->ac_i, lockin_info->obj_pos[last_c_i]);
  return refresh_plot(pr_LIVE_VIDEO, UNCHANGED);
}


int refold_phase(void)
{
  int i, j;
  pltreg *pr;
  O_p *op;
  d_s *dsi, *dsd;
  float per = (float)200/3;
  double org, tmpd, perd;

  if (lockin_info == NULL)  return 0;
  if(updating_menu_state != 0)	      return 0;  


  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("Not enough data");  

  i = win_scanf("refolding period %f",&per);
  if (i == CANCEL)     return 0;  

  dsd = create_and_attach_one_ds(op,dsi->nx,dsi->nx,0);
  if (dsd == NULL)      return win_printf_OK("Pas de ds creer");  
  org = dsi->xd[0];
  perd = per;
  for (i = 0; i < dsi->nx; i++)
    {
      tmpd = dsi->xd[i] - org;
      for (j = 0; tmpd < 0 || tmpd >= perd; j++ )
	{
	  if (tmpd >= perd) tmpd -= perd;
	  if (tmpd < 0) tmpd += perd;
	}
      dsd->xd[i] = tmpd;
      dsd->yd[i] = dsi->yd[i];
    } 
  return refresh_plot(pr, UNCHANGED);
}

int simul_phase(void)
{
  int i, j, nf, orgi, ndphi = 0;
  pltreg *pr;
  O_p *op;
  d_s *dsi, *dsd;
  static float per = (float)200/3, phi = 0;
  double tmpd, perd, dphi;

  if (lockin_info == NULL)  return 0;
  if(updating_menu_state != 0)	      return 0;  


  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("Not enough data");  

  i = win_scanf("refolding period %f\n phase phi %f",&per, &phi);
  if (i == CANCEL)     return 0;  


  nf = 1 + (int)dsi->xd[dsi->nx-1] - (int)dsi->xd[0];
  dsd = create_and_attach_one_ds(op,nf,nf,0);
  if (dsd == NULL)      return win_printf_OK("Pas de ds crerer");  
  orgi = (int)dsi->xd[0];
  perd = per;
  for (i = 0; i < nf; i++)
    {
      tmpd = i + orgi;
      dsd->xd[i] = (float)tmpd;
      tmpd += phi;
      for (j = 0; tmpd < 0 || tmpd >= perd; j++ )
	{
	  if (tmpd >= perd) tmpd -= perd;
	  if (tmpd < 0) tmpd += perd;
	}
      if (tmpd < perd/4)	dsd->yd[i] = 0;
      else if (tmpd < perd/2)	dsd->yd[i] = 90;
      else if (tmpd < 3*perd/4)	dsd->yd[i] = 180;
      else dsd->yd[i] = 270;
    } 


  dsd = create_and_attach_one_ds(op,dsi->nx,dsi->nx,0);
  if (dsd == NULL)      return win_printf_OK("Pas de ds crerer");
  orgi = (int)dsi->xd[0];  
  perd = per;
  for (i = 0, dphi = 0, ndphi = 0; i < nf; i++)
    {
      tmpd = dsi->xd[i];
      dsd->xd[i] = dsi->xd[i];
      tmpd += phi;
      for (j = 0; tmpd < 0 || tmpd >= perd; j++ )
	{
	  if (tmpd >= perd) tmpd -= perd;
	  if (tmpd < 0) tmpd += perd;
	}
      if (tmpd < perd/4)	dsd->yd[i] = 0;
      else if (tmpd < perd/2)	
	{
	  dsd->yd[i] = 90;
	  dphi += 90 - dsi->yd[i]; 
	  ndphi++;
	}
      else if (tmpd < 3*perd/4)	
	{
	  dsd->yd[i] = 180;
	  dphi += 180 - dsi->yd[i]; 
	  ndphi++;
	}
      else dsd->yd[i] = 270;
    } 
  dphi = (ndphi > 0) ? dphi/ndphi : -1000;
  set_plot_title(op,"dphi %g",dphi);
  return refresh_plot(pr, UNCHANGED);
}

int slidding_phase(void)
{
  int i,k, nf, ndphi = 0, nsld = 8, isld, peri, phase, nd;
  pltreg *pr;
  static int  niter=10;
  O_p *op;
  d_s *dsi, *dsd;
  static float per = 66.7965;
  double dphi;

  if (lockin_info == NULL)  return 0;
  if(updating_menu_state != 0)	      return 0;  


  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("Not enough data");  
  nd = op->cur_dat;
  i = win_scanf("iteration number %d\n", &niter);
  if (i == CANCEL)     return 0;  
  
  
  nsld *= (int)per;
  nf =  (int)per * 8;


  peri = 16666;
  vco_phase_pid(1000*dsi->xd[0],0,0,NULL,NULL,&peri,NULL,NULL); 


  dsd = create_and_attach_one_ds(op,nf,nf,0);
  if (dsd == NULL)      return win_printf_OK("Pas de ds crerer");  

  if ( (nf+niter*nsld) > dsi->nx) return win_printf_OK("niter trop important");

  for (k = 0; k < niter; k++)
    {
          
           isld = k*nsld; 
           for (i = 0, dphi = 0, ndphi = 0; i < nf; i++)
               {
                 vco_phase_pid(1000*dsi->xd[i+isld],0,0,NULL,NULL,NULL,&phase,NULL);
                 dsd->xd[i] = dsi->xd[i+isld];
		 if (phase%4 == 0)	
		   {
		     dsd->yd[i] = 0;
		     dphi += (dsi->yd[i+isld] <= 180) ? -dsi->yd[i+isld] : 90; 
		     ndphi++;
		   }
		 else if (phase%4 == 1)	
		   {
		     dsd->yd[i] = 90;
		     dphi += 90 - dsi->yd[i+isld]; 
		     ndphi++;
		   }
		 else if (phase%4 == 2)	
		   {
		     dsd->yd[i] = 180;
		     dphi += 180 - dsi->yd[i+isld]; 
		     ndphi++;
		   }
		 else 
		   {
		     dsd->yd[i] = 270;
		     dphi += (dsi->yd[i+isld] <= 90) ? -90 - dsi->yd[i+isld] : 270 - dsi->yd[i+isld]; 
		     ndphi++;
		   }
		 // dsd->yd[i] = phase;
               } 
           dphi = (ndphi > 0) ? dphi/ndphi : -1000;
	   peri = 0;
	   vco_phase_pid(1000*dsd->xd[0],dphi,0,NULL,NULL,&peri,NULL,NULL);
	   // i = win_printf("k = %d phi %f dphi %g",k,phi, dphi); 
           //if (i == CANCEL)     return 0;  
	  
     }
  set_plot_title(op,"dphi %g periode %d", dphi, peri);
  op->cur_dat = nd;
  return refresh_plot(pr, UNCHANGED); 

}

# ifdef OLD
int vco_phase(long long t,      //the time stamp to determine phase
          int *per,         // if > 0 define the new period of the vco else
                            // return perl the present vco period
          int *n_cycle,     // the number of cycles since started
          int *cycl_ph      // the position inside cycle [0 to perl]
          )
{
  static long long org = 0, perl = (long long)((double)0.0667965/4 * 
  get_my_ulclocks_per_sec());
  int ph;
 
  if (per != NULL)
    {
      if (*per > 0) perl = (long long)*per;  else *per = (int)perl;
    }
  org = t + org;
  ph = (int)(org / perl);
  org -= perl * ph;
  if (n_cycle) *n_cycle = ph;
  if(cycl_ph) *cycl_ph = (int) org;
  return 0;
}
#endif

int vco_phase_pid(long long t,      // the time stamp to determine phase
		  float error,      // error signal
		  int mode,         // mode = 0, return P and I, else change their value
		  float *P,         // proportionnal 
		  float *I,         // integralal
		  int *per,         // if > 0 define the new period of the vco else
                                    // return perl the present vco period
		  int *n_cycle,     // the number of cycles since started
		  int *cycl_ph      // the position inside cycle [0 to perl]
          )
{
  static long long org = 0, perl = 16666, t0 = 0, tmpl;
  static float Pl = 500, Il = 0.0001;
  static double Ival = 0;
  static int ph0, first = 1;
  int ph;
 


  if (mode == 0 && P != NULL) *P = Pl;
  else if (mode != 0 && P != NULL) Pl = *P;
  if (mode == 0 && I != NULL) *I = Il;
  else if (mode != 0 && I != NULL) Il = *I;
  if (per != NULL)
    {
      if (*per > 0) 
	{
	  tmpl = (t - t0 + org) / perl;
	  ph0 = (int)tmpl + ph0;
	  org = (t - t0 + org) % perl;
	  if (first)
	    {
	      first = 0;
	      ph0 = 0;
	    }
	  perl = (long long)*per; 
	  t0 = t;
	}
      else *per = (int)perl;
    }
  if (error != 0.0)
    {
      tmpl = (t - t0 + org) / perl;
      ph0 = (int)tmpl + ph0;
      org = (t - t0 + org) % perl;
      t0 = t;
      Ival += error;
      perl = perl + Pl * error + Il * Ival;
    }  
  if (t > 0)
    {
      //org = (t - t0) + org;
      tmpl = (t - t0 + org) / perl;
      ph = (int)tmpl + ph0;
      //org -= perl * ph;
      if (n_cycle) *n_cycle = ph;
      if(cycl_ph) *cycl_ph = (int) (t - t0 + org - ph* perl);
    }
  return 0;
}

double do_dphi_old(int n)
{
  int i, j, k, ndphi, b_size, last_ci;
  double tmpd, dphi;
  if (lockin_info == NULL) return D_O_K;

  b_size = 50*LOCKIN_BUFFER_SIZE/3;
  last_ci = ph_lock.c_i;
  for (i = 0, j = last_ci, dphi = 0, ndphi = 0; i < n; i++, j--)
     {
       j = (j < 0) ? b_size + j: j;
       tmpd = 1000*((double)(ph_lock.imt[j] - ph_lock.imt0))/get_my_ulclocks_per_sec();
       tmpd += lockin_info->phi_t;
       for (k = 0; tmpd < 0 || tmpd >= lockin_info->per_t; k++)
          {
	     if (tmpd >= lockin_info->per_t) tmpd -= lockin_info->per_t;
	     if (tmpd < 0) tmpd += lockin_info->per_t;
          }
       if (tmpd >= lockin_info->per_t/4 && tmpd < lockin_info->per_t/2)
	  {
             dphi += 90 - ph_lock.phase2[j];
             ndphi++;
	  }
       if (tmpd >= lockin_info->per_t/2 && tmpd < 3*lockin_info->per_t/4)  
	  {
             dphi += 180 - ph_lock.phase2[j];
             ndphi++;
	  }
      }
  dphi = (ndphi > 0) ? dphi/ndphi : -1000;
  return dphi;
}

double do_dphi(int n)
{
  int i, j, ndphi, b_size, last_ci, phase = 0;
  double dphi;
  if (lockin_info == NULL) return D_O_K;

  b_size = 50*LOCKIN_BUFFER_SIZE/3;
  last_ci = ph_lock.c_i;
  for (i = 0, j = last_ci, dphi = 0, ndphi = 0; i < n; i++, j--)
     {
       j = (j < 0) ? b_size + j: j;
       vco_phase_pid(ph_lock.imt[j],0,0,NULL,NULL,NULL,&phase,NULL);
       ph_lock.phase1[j] = 90*(phase%4);
       if (phase%4 == 0)	
	 {
	   // dphi += (ph_lock.phase2[j] <= 180) ? -ph_lock.phase2[j] : 90; 
	   //ndphi++;
	 }
       else if (phase%4 == 1)	
	 {
	   dphi += 90 - ph_lock.phase2[j]; 
	   ndphi++;
	 }
       else if (phase%4 == 2)	
	 {
	   dphi += 180 - ph_lock.phase2[j]; 
	   ndphi++;
	 }
       else 
	 {
	   //dphi += (ph_lock.phase2[j] <= 90) ? -90 - ph_lock.phase2[j] : 270 - ph_lock.phase2[j]; 
	   //ndphi++;
	 }
     } 
  dphi = (ndphi > 0) ? dphi/ndphi : -1000;
  for (i = 0, j = last_ci; i < n; i++, j--)
     {
       j = (j < 0) ? b_size + j: j;
       ph_lock.dephi[j] = dphi;
     }
  return dphi;
}

int set_alpha(void)
{
  int i,n;
  float per,alpha;

  if (lockin_info == NULL)  return 0;
  if(updating_menu_state != 0)	      return 0;  

  per = lockin_info->per_t;
  alpha = lockin_info->alpha;
  n= ph_lock.n_phi/ph_lock.peri;
  i = win_scanf("periode %f\n alpha %f\n n steps = n * periode %d",&per, &alpha, &n);
  if (i == CANCEL)     return 0;
  
  ph_lock.n_phi = n * ph_lock.peri;
  lockin_info->per_t = per;
  lockin_info->alpha = alpha;
  return 0;
}


//commands to operate on the averaged movies/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int adj_toogle_frame(void)
{
  int fr;

  if (lockin_info == NULL)  return 0;
  fr = lockin_info->toogle_frame;
  if(updating_menu_state != 0)	
    {
      if (fr == RETRIEVE_MENU_INDEX)
	active_menu->flags |= D_SELECTED;	
      else active_menu->flags &= ~D_SELECTED;	
      return 0;  
    }
  lockin_info->toogle_frame = (fr) ? 0 : 1;
  return 0;  
}

int adj_odd_even(void)
{
  int oe;

  if (lockin_info == NULL)  return 0;
  oe = lockin_info->odd_even;
  if(updating_menu_state != 0)	
    {
      if (oe == RETRIEVE_MENU_INDEX)
	active_menu->flags |= D_SELECTED;	
      else active_menu->flags &= ~D_SELECTED;	
      return 0;  
    }
  lockin_info->odd_even = (oe) ? 0 : 1;
  return 0;  
}

int adj_navg(void)
{
  int na, i;

  if (lockin_info == NULL)  return 0;
  na = lockin_info->navg;
  if(updating_menu_state != 0)	return 0;  
  i = win_scanf("Navg %d",&na);
  if (i == CANCEL)  	return 0;  
  lockin_info->navg = 2*na/2;
  return 0;  
}


int adj_vco(void)
{
  int i;
  float P, I;
  int per = 0;

  if (lockin_info == NULL)  return 0;

  if(updating_menu_state != 0)	return 0;  
  vco_phase_pid(0,0,0,&P,&I,&per,NULL,NULL);
  i = win_scanf("VCO P %12f I %12f \nper %20d\n",&P,&I,&per);
  if (i == CANCEL)  	return 0;  
  vco_phase_pid(0,0,1,&P,&I,&per,NULL,NULL);
  return 0;  
}



int ask_to_update_menu(void)
{
  if (lockin_info == NULL)  return 1;
  last_im_who_asked_menu_update = lockin_info->imi[lockin_info->c_i];
  return 0;
}

int oi_avg_idle_action(O_i *oi, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (oi->need_to_refresh)      d->proc(MSG_DRAW,d,0);	
  return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// this running in the timer thread
int do_at_all_image(imreg *imr, O_i *oi, int n,  DIALOG *d, long long t, unsigned long dt, void *p, int n_inarow) 
{
  register int k, j;
  int ci, i, ci_1, ac_i, ac_istart;
  BITMAP *imb = NULL;
  g_lockin *gt = NULL;
  short int *pi_avg1, *pi_avg2, *pi_avg3;
  int  i_avg3=0, i_avg1=0, i_avg2=0, peri, tmpi, phase;
  //int i_avg;
  unsigned char *pc;
  O_i *oia1, *oia2, *oia3;
  
  
  gt = (g_lockin*)p;

  ac_i = lockin_info->ac_i;
  ac_i++;
  ci_1 = ci = lockin_info->c_i;
  ci++;
  ci = (ci < lockin_info->n_i) ? ci : 0;
  lockin_info->imi[ci] = n;                                 // we save image" number
  lockin_info->imit[ci] = n_inarow;                                 
  lockin_info->imt[ci] = t;                                 // image time
   
  // lockin_info->phase[ci] = (n_inarow == 0) ? do_read_db25() : -1; 
  //lockin_info->phase[ci] =  do_read_db25();
 
  if (lockin_info->ac_i == 0)  
    {
      lockin_info->imt0 = lockin_info->imt[ci]; 
      peri = (int)(((double)get_my_ulclocks_per_sec())*0.016674);
      vco_phase_pid(lockin_info->imt0,0,0,NULL,NULL,&peri,NULL,NULL); 
    }
 
  if (ph_lock.ac_i < ph_lock.n_phi)       lockin_info->per_i = 0;
  else if ((ac_i%4) == 0)
    {
      lockin_info->dphi_t =  do_dphi(ph_lock.n_phi);
      peri = 0;
      tmpi = ph_lock.c_i - ph_lock.n_phi;
      tmpi = (tmpi < 0) ? tmpi + 50*LOCKIN_BUFFER_SIZE/3 : tmpi;
      vco_phase_pid(ph_lock.imt[tmpi],lockin_info->dphi_t,0,NULL,NULL,&peri,NULL,NULL);
      lockin_info->per_i = peri;
      vco_phase_pid(lockin_info->imt[ci],0,0,NULL,NULL,NULL,&phase,NULL);
    }
  //lockin_info->delta[ci] = ac_i * 1000*(double)lockin_info->per_i/get_my_ulclocks_per_sec();
  //lockin_info->delta[ci] = ac_i *16.699062;

  //vco_phase_pid(lockin_info->imt[ci]-33365228,0,0,NULL,NULL,NULL,&phase,NULL);
  vco_phase_pid(lockin_info->imt[ci]-lockin_info->per_i,0,0,NULL,NULL,NULL,&phase,NULL);
  lockin_info->phase[ci] = 90*(phase%4);
  
  if (oi->bmp.stuff != NULL) imb = (BITMAP*)oi->bmp.stuff;
  // we grab the video bitmap of the IFC image
  prepare_image_overlay(oi);


  O_i *oi_synchro;
  short int *pi_sync;
  O_p *op;
 
  oi_synchro = lockin_info->oid;
  op = oi_synchro->o_p[0];

  ac_istart =  ph_lock.n_phi/2;
  if (ac_i >= ac_istart && ac_i < ac_istart + 64)   
     {
       op->dat[0]->xd[ac_i-ac_istart] = ac_i-ac_istart;
       op->dat[0]->yd[ac_i-ac_istart] = lockin_info->phase[ci];

       for (k = lockin_info->y0; k < lockin_info->y1; k++)
	{
          pi_sync = oi_synchro->im.pxl[ac_i-ac_istart][k].in;
	  pc = oi->im.pixel[k].ch;
	  for (j = lockin_info->x0; j < lockin_info->x1; j++)   pi_sync[j] = pc[j];
	} 
     }    

////image treatment///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  oia1 = lockin_info->oi_avg1;
  oia2 = lockin_info->oi_avg2;
  oia3 = lockin_info->oi_avg3;
  //oia = lockin_info->oi_avg1; 
 
  // we put here the lockin averaging

  //tmpf = (ci%2 == 0) ? 0.05 : -0.05;
//  
//     
//  if (live_video_info->oi_avg != NULL)
//    {
//      for (k = lockin_info->y0; k < lockin_info->y1; k++)
//	{
//	  pf = live_video_info->oi_avg->im.pixel[k].fl;
//	  pc = oi->im.pixel[k].ch;
//	  for (j = lockin_info->x0; j < lockin_info->x1; j++)
//	    {
//	      pf[j] = 0.95 * pf[j] + tmpf * pc[j]; 
//	    } 
//	}
//      live_video_info->oi_avg->need_to_refresh |= BITMAP_NEED_REFRESH;
//    }
 

/* //we create an avg image over navg frames by changing phase every frame with 2 phases */
 
/*   i_avg = (oia->im.c_f == 0) ? 1 : 0; */

/*   if ((ac_i%lockin_info->navg) == 0) */
/*      { */
/*       for (k = lockin_info->y0; k < lockin_info->y1; k++) */
/* 	{ */
/*           pi_avg = oia->im.pxl[i_avg][k].in; */
/* 	  pc = oi->im.pixel[k].ch; */
/* 	  for (j = lockin_info->x0; j < lockin_info->x1; j++)        pi_avg[j] = pc[j]; */
/* 	} */
/*      } */
/*   else if ((ac_i%2) == 0) */
/*      { */
/*       for (k = lockin_info->y0; k < lockin_info->y1; k++) */
/* 	{ */
/* 	  pi_avg = oia->im.pxl[i_avg][k].in; */
/* 	  pc = oi->im.pixel[k].ch; */
/* 	  for (j = lockin_info->x0; j < lockin_info->x1; j++)     pi_avg[j] += pc[j]; */
/*         } */
/*      } */
/*   else  */
/*      { */
/*       for (k = lockin_info->y0; k < lockin_info->y1; k++) */
/* 	{ */
/* 	  pi_avg = oia->im.pxl[i_avg][k].in; */
/* 	  pc = oi->im.pixel[k].ch; */
/* 	  for (j = lockin_info->x0; j < lockin_info->x1; j++)     */
/* 	    { */
/*                  pi_avg[j] -= pc[j]; */
/*                  if ((ac_i%lockin_info->navg) == (lockin_info->navg -1)) pi_avg[j] *= pi_avg[j]; */
/*             }   */
/*         } */
/*      } */
  
/*   if ((ac_i%lockin_info->navg) == (lockin_info->navg -1)) */
/*      {  */
/*        switch_frame(oia,i_avg); */
/*        // find_zmin_zmax(oia); */
/*        oia->need_to_refresh |= BITMAP_NEED_REFRESH; */
/*        set_im_title(oia, "Image avg %d", ac_i/lockin_info->navg); */
/*      } */

/* /\* //we create 2 avg image over navg frames with real and imaginary part *\/ */
/* /\* //phase rolling = 0, 90, 180, 270 *\/ */

/*   i_avg1 = (oia1->im.c_f == 0) ? 1 : 0; */
/*   i_avg2 = (oia2->im.c_f == 0) ? 1 : 0; */
/*   i_avg3 = (oia3->im.c_f == 0) ? 1 : 0; */



/*   if ((ac_i%lockin_info->navg) == 0) */
/*     { */
/*       for (k = lockin_info->y0; k < lockin_info->y1; k++) */
/* 	{ */
/*           pi_avg1 = oia1->im.pxl[i_avg1][k].in; */
/* 	  pc = oi->im.pixel[k].ch; */
/* 	  if (lockin_info->odd_even == 1 && k%2 == 1) */
/* 	    { */
/* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++) */
/* 		     pi_avg1[j] = -pc[j]; */
/* 	    } */
/* 	  else */
/* 	    { */
/* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++) */
/* 	         pi_avg1[j] = pc[j]; */
/* 	    } */
/* 	} */
/*     } */
/*   if (((ac_i%lockin_info->navg)%4) == 2) */
/*      { */
/*       for (k = lockin_info->y0; k < lockin_info->y1; k++) */
/* 	 { */
/* 	  pi_avg1 = oia1->im.pxl[i_avg1][k].in; */
/* 	  pc = oi->im.pixel[k].ch; */
/* 	  if (lockin_info->odd_even == 1 && k%2 == 1) */
/* 	    { */
/* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++) */
/* 	         pi_avg1[j] += pc[j]; */
/* 	    } */
/* 	  else */
/* 	    { */
/* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++) */
/* 		     pi_avg1[j] -= pc[j]; */
/* 	    } */

/*          } */
/*      } */
      
/*   if (((ac_i%lockin_info->navg)%4) == 0 && (ac_i%lockin_info->navg) != 0) */
/*      { */
/*       for (k = lockin_info->y0; k < lockin_info->y1; k++) */
/* 	 { */
/* 	  pi_avg1 = oia1->im.pxl[i_avg1][k].in; */
/* 	  pc = oi->im.pixel[k].ch; */
/* 	  if (lockin_info->odd_even == 1 && k%2 == 1) */
/* 	    { */
/* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++) */
/* 		     pi_avg1[j] -= pc[j]; */
/* 	    } */
/* 	  else */
/* 	    { */
/* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++) */
/* 		     pi_avg1[j] += pc[j]; */
/* 	    } */
/*          } */
/*      } */
   
/*   if ((ac_i%lockin_info->navg) == 1) */
/*      { */
/*       for (k = lockin_info->y0; k < lockin_info->y1; k++) */
/* 	 { */
/*       pi_avg2 = oia2->im.pxl[i_avg2][k].in; */
/* 	  pc = oi->im.pixel[k].ch; */
/* 	  if (lockin_info->odd_even == 1 && k%2 == 1) */
/* 	    { */
/* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++) */
/*              pi_avg2[j] = -pc[j]; */
/* 	    } */
/* 	  else */
/* 	    { */
/* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++) */
/*              pi_avg2[j] = pc[j]; */
/*         } */
/* 	 } */
/*      } */
/*   if (((ac_i%lockin_info->navg)%4) == 3) */
/*      { */
/*       for (k = lockin_info->y0; k < lockin_info->y1; k++) */
/* 	 { */
/* 	  pi_avg2 = oia2->im.pxl[i_avg2][k].in; */
/* 	  pc = oi->im.pixel[k].ch; */
/* 	  if (lockin_info->odd_even == 1 && k%2 == 1) */
/* 	    { */
/* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++) */
/* 	         pi_avg2[j] += pc[j]; */
/* 	    } */
/* 	  else */
/* 	    { */
/* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++) */
/* 		     pi_avg2[j] -= pc[j]; */
/* 	    } */
/*          } */
/*      } */
/*   if (((ac_i%lockin_info->navg)%4) == 1 && (ac_i%lockin_info->navg) != 1) */
/*      { */
/*       for (k = lockin_info->y0; k < lockin_info->y1; k++) */
/* 	 { */
/* 	  pi_avg2 = oia2->im.pxl[i_avg2][k].in; */
/* 	  pc = oi->im.pixel[k].ch; */
/* 	  if (lockin_info->odd_even == 1 && k%2 == 1) */
/* 	    { */
/* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++) */
/* 	       	 pi_avg2[j] -= pc[j]; */
/* 	    } */
/* 	  else */
/* 	    { */
/* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++) */
/* 		     pi_avg2[j] += pc[j]; */
              
/* 	    } */
/*          } */
/*      } */
  
/*   if ((ac_i%lockin_info->navg) == (lockin_info->navg -1)) */
/*     { */
/*       for (k = lockin_info->y0; k < lockin_info->y1; k++) */
/* 	 { */
/* 	  pi_avg1 = oia1->im.pxl[i_avg1][k].in; */
/*           pi_avg2 = oia2->im.pxl[i_avg2][k].in; */
/*           pi_avg3 = oia3->im.pxl[i_avg3][k].in; */
/* 	      for (j = lockin_info->x0; j < lockin_info->x1; j++) */
/* 	       	pi_avg3[j] = pi_avg2[j]*pi_avg2[j] + pi_avg1[j]*pi_avg1[j]; */
/*          } */
/*     } */
/*   if ((ac_i%lockin_info->navg) == (lockin_info->navg -1)) */
/*      { */
       
/*        switch_frame(oia1,i_avg1); */
/*        switch_frame(oia2,i_avg2); */
/*        switch_frame(oia3,i_avg3); */

/*        // find_zmin_zmax(oia); */
/*        oia1->need_to_refresh |= BITMAP_NEED_REFRESH; */
/*        oia2->need_to_refresh |= BITMAP_NEED_REFRESH; */
/*        oia3->need_to_refresh |= BITMAP_NEED_REFRESH; */
/*        //set_im_title(oia, "Image avg %d", ac_i/lockin_info->navg); */
/*      } */

 /* //we create 2 avg image over navg frames with real and imaginary part */
/* //phase rolling = 0, 90, 180, 270 */

  i_avg1 = (oia1->im.c_f == 0) ? 1 : 0;
  i_avg2 = (oia2->im.c_f == 0) ? 1 : 0;
  i_avg3 = (oia3->im.c_f == 0) ? 1 : 0;

  if ((ac_i%lockin_info->navg) == 0) lockin_info->ph_start = lockin_info->phase[ci]; 
  if (lockin_info->ph_start == 0)
    {
      if ((ac_i%lockin_info->navg) == 0)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] = -pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 2)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] += pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 0)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] -= pc[j];
	     }
	}

      if ((ac_i%lockin_info->navg) == 1)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] = -pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 3)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] += pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 1)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] -= pc[j];
	     }
	}
    }
 
  if (lockin_info->ph_start == 90)
    {

      if ((ac_i%lockin_info->navg) == 0)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] = -pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 2)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] += pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 0)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] -= pc[j];
	     }
	}

      if ((ac_i%lockin_info->navg) == 1)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] = pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 3)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] -= pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 1)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] += pc[j];
	     }
	}
    }

   if (lockin_info->ph_start == 180)
    {

      if ((ac_i%lockin_info->navg) == 0)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] = pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 2)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] -= pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 0)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] += pc[j];
	     }
	}

      if ((ac_i%lockin_info->navg) == 1)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] = pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 3)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] -= pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 1)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] += pc[j];
	     }
	}
    }

  if (lockin_info->ph_start == 270)
    {

      if ((ac_i%lockin_info->navg) == 0)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] = pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 2)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] -= pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 0)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg2 = oia2->im.pxl[i_avg2][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg2[j] += pc[j];
	     }
	}

      if ((ac_i%lockin_info->navg) == 1)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] = -pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 3)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] += pc[j];
	     }
	}
      else if (((ac_i%lockin_info->navg)%4) == 1)
        {
	  for (k = lockin_info->y0; k < lockin_info->y1; k++)
	     {
                pi_avg1 = oia1->im.pxl[i_avg1][k].in;
	        pc = oi->im.pixel[k].ch;
	        for (j = lockin_info->x0; j < lockin_info->x1; j++)       pi_avg1[j] -= pc[j];
	     }
	}
    }

  if ((ac_i%lockin_info->navg) == (lockin_info->navg - 1))
    {
      for (k = lockin_info->y0; k < lockin_info->y1; k++)
	 {
	  pi_avg1 = oia1->im.pxl[i_avg1][k].in;
          pi_avg2 = oia2->im.pxl[i_avg2][k].in;
          pi_avg3 = oia3->im.pxl[i_avg3][k].in;
	  for (j = lockin_info->x0; j < lockin_info->x1; j++)             pi_avg3[j] = pi_avg2[j]*pi_avg2[j] + pi_avg1[j]*pi_avg1[j];
         }
       switch_frame(oia1,i_avg1);
       switch_frame(oia2,i_avg2);
       switch_frame(oia3,i_avg3);

       // find_zmin_zmax(oia);
       oia1->need_to_refresh |= BITMAP_NEED_REFRESH;
       oia2->need_to_refresh |= BITMAP_NEED_REFRESH;
       oia3->need_to_refresh |= BITMAP_NEED_REFRESH;
    }
//image treatment end////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  i = find_remaining_action(n);
  lockin_info->status_flag[ci] = lockin_info->status_flag[ci_1];  
  lockin_info->obj_pos_cmd[ci] = lockin_info->obj_pos_cmd[ci_1];
  while ((i = find_next_action(n)) >= 0)
    {
      if (action_pending[i].type == MV_OBJ_ABS)
	{
	  //set_Z_value(action_pending[i].value);
	  lockin_info->status_flag[ci] |= OBJ_MOVING;
	  lockin_info->obj_pos_cmd[ci] = action_pending[i].value;
	}
      else if (action_pending[i].type == MV_OBJ_REL)
	{
	  lockin_info->obj_pos_cmd[ci] = lockin_info->obj_pos_cmd[ci_1] + action_pending[i].value;
	  //set_Z_value(lockin_info->obj_pos_cmd[ci]);
	  lockin_info->status_flag[ci] |= OBJ_MOVING;
	}
      action_pending[i].proceeded = 1;
    }

  if (fabs(lockin_info->obj_pos_cmd[ci] - lockin_info->obj_pos[ci_1]) > 0.005)
    {
      //lockin_info->obj_pos[ci] = read_Z_value_accurate_in_micron();
      lockin_info->status_flag[lockin_info->c_i] |= OBJ_MOVING;
    }     
  else
    {
      lockin_info->obj_pos[ci] = lockin_info->obj_pos[ci_1];
      lockin_info->status_flag[lockin_info->c_i] &= ~OBJ_MOVING;
    }

  lockin_info->imdt[ci] = my_uclock() - dt;
  lockin_info->c_i = ci;
  lockin_info->ac_i = ac_i;

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
  
  return 0; 
}
END_OF_FUNCTION(do_at_all_image)


MENU *phase_in_live_video_image_menu(void)
{
	static MENU mn[32];
	

	if (mn[0].text != NULL)	return mn;
        add_item_to_menu(mn,"frame 0->1", adj_toogle_frame ,NULL,MENU_INDEX(0) ,NULL);
	add_item_to_menu(mn,"frame 1->0", adj_toogle_frame ,NULL,MENU_INDEX(1) ,NULL);
	add_item_to_menu(mn,"Odd even",adj_odd_even ,NULL,MENU_INDEX(2) ,NULL);
	add_item_to_menu(mn,"Even Odd",adj_odd_even ,NULL,MENU_INDEX(3) ,NULL);
        add_item_to_menu(mn,"navg",adj_navg ,NULL,MENU_INDEX(4) ,NULL);
        add_item_to_menu(mn,"refold phase",refold_phase ,NULL,0 ,NULL);
	return mn;
}


int get_present_image_nb(void)
{
 return (lockin_info != NULL) ?  lockin_info->imi[lockin_info->c_i] : -1;
}                                 


g_lockin *creating_lockin_info(void)
{
  O_p *op;
  d_s *ds;
  O_i *oi_rel, *oi_im, *oi_mod;
  O_i *oi_sync;
 

  if (lockin_info == NULL)
    {
      lockin_info = (g_lockin*)calloc(1,sizeof(g_lockin));
      if (lockin_info == NULL) return win_printf_ptr("cannot alloc lockin info!");	

      lockin_info->im_focus_check = 16;
      lockin_info->temp_cycle = lockin_info->itemp_mes = 0;
      lockin_info->T0 = lockin_info->T1 = lockin_info->T2 = lockin_info->T3 = lockin_info->I0 = 0;   
      lockin_info->temp_mes[0] = 0;
      lockin_info->user_lockin_action = NULL;

     /*  // plots to check lockin */
/*       op = pr_LIVE_VIDEO->o_p[0]; */
/*       set_op_filename(op, "Timing.gr"); */
/*       ds = op->dat[0]; // the first data set was already created */
/*       set_ds_source(ds, "Timing rolling buffer"); */
/*       if ((ds = create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0)) == NULL) */
/* 		return win_printf_ptr("I can't create plot !"); */
/*       set_ds_source(ds, "Period rolling buffer"); */
/*       if ((ds = create_and_attach_one_ds(op, LOCKIN_BUFFER_SIZE, LOCKIN_BUFFER_SIZE, 0)) == NULL) */
/* 		return win_printf_ptr("I can't create plot !"); */
/*       set_ds_source(ds, "Timer rolling buffer"); */
/*       op->op_idle_action = timing_rolling_buffer_idle_action; */
/*       //      op->op_post_display = general_op_post_display; */
/*       set_plot_x_title(op, "Time"); */
/*       set_plot_y_title(op, "{\\color{yellow} Lockining duration} {\\color{lightgreen} Frame duration} (ms)");			 */

/*       create_attach_select_x_un_to_op(op, IS_SECOND, 0 */
/* 				       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s"); */


      //plot to check phase in image thread
       op = pr_LIVE_VIDEO->o_p[0];
      set_op_filename(op, "Timing.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Phase expected");
      op->op_idle_action = phase_in_imagethread_rolling_buffer_idle_action;
      set_plot_x_title(op, "Time");
	    //set_plot_y_title(op, "{\\color{yellow} Lockining duration} {\\color{lightgreen} Frame duration} (ms)");			
	    // create_attach_select_x_un_to_op(op, IS_SECOND, 0
	    //		       ,(float)1/Pico_param.camera_param.camera_frequency_in_Hz, 0, 0, "s");



      // plots to check phase index in phase-timer
      if ((op = create_and_attach_one_plot(pr_LIVE_VIDEO, 50*LOCKIN_BUFFER_SIZE/3, 50*LOCKIN_BUFFER_SIZE/3, 0)) == NULL)
	return win_printf_ptr("I can't create plot !");
      set_op_filename(op, "Lock_phase.gr");
      ds = op->dat[0]; // the first data set was already created
      set_ds_source(ds, "Modulation phase measured");
      if ((ds = create_and_attach_one_ds(op, 50*LOCKIN_BUFFER_SIZE/3, 50*LOCKIN_BUFFER_SIZE/3, 0)) == NULL)
      return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Modulation phase simulated");
      if ((ds = create_and_attach_one_ds(op, 50*LOCKIN_BUFFER_SIZE/3, 50*LOCKIN_BUFFER_SIZE/3, 0)) == NULL)
      return win_printf_ptr("I can't create plot !");
      set_ds_source(ds, "Error");
      
      op->op_idle_action = phase_rolling_buffer_idle_action;
      set_plot_x_title(op, "Time");
      set_plot_y_title(op, "{\\color{yellow} Measured}{\\color{lightgreen} Simulated} phase");
      //create_attach_select_x_un_to_op(op, IS_SECOND, 0 ,1, 0, 0, "s");


      
      //if ((oi = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, 
      //				       oi_LIVE_VIDEO->im.ny, IS_INT_IMAGE, 2)) == NULL)
      //	return win_printf_ptr("I can't create image !");
      
      if ((oi_rel = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny, IS_INT_IMAGE, 2)) == NULL)
      	return win_printf_ptr("I can't create image !");
      if ((oi_im = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny, IS_INT_IMAGE, 2)) == NULL)
      	return win_printf_ptr("I can't create image !");
       //set_oi_filename(oi, "Im_avg.gr");
      if ((oi_mod = create_and_attach_movie_to_imr(imr_LIVE_VIDEO, oi_LIVE_VIDEO->im.nx, oi_LIVE_VIDEO->im.ny, IS_INT_IMAGE, 2)) == NULL)
      	return win_printf_ptr("I can't create image !");
       

       // plot and movie for synchronisation
      if ((oi_sync = create_and_attach_movie_to_imr(imr_LIVE_VIDEO,oi_LIVE_VIDEO->im.nx , oi_LIVE_VIDEO->im.ny, IS_INT_IMAGE,64)) == NULL)
	 return win_printf_ptr("I can't create image !");
      if ((op = create_and_attach_op_to_oi(oi_sync , 64 ,64 , 0, 0)) == NULL)
	 return win_printf_ptr("I can't create op !");
      //ds = op->dat[0]; // the first data set was already created
      // set_ds_source(ds, "Sync dataset");

      lockin_info->oi_avg1 = oi_rel;
      lockin_info->oi_avg2 = oi_im;
      lockin_info->oi_avg3 = oi_mod;
      lockin_info->oid = oi_sync;
      
      lockin_info->x0 =0; // oi_LIVE_VIDEO->im.nx/4;
      lockin_info->y0 = 0;//oi_LIVE_VIDEO->im.ny/4;
      lockin_info->x1 = oi_LIVE_VIDEO->im.nx; //3*oi_LIVE_VIDEO->im.nx/4;
      lockin_info->y1 = oi_LIVE_VIDEO->im.ny; //3*oi_LIVE_VIDEO->im.ny/4;

      oi_rel->oi_idle_action = oi_avg_idle_action;
      oi_im->oi_idle_action = oi_avg_idle_action;
      oi_mod->oi_idle_action = oi_avg_idle_action;
      //oi_sync->oi_idle_action = oi_avg_idle_action;
      
      map_pixel_ratio_of_image_and_screen(oi_rel, 1, 1);
      map_pixel_ratio_of_image_and_screen(oi_im, 1, 1);
      map_pixel_ratio_of_image_and_screen(oi_mod, 1, 1);
      map_pixel_ratio_of_image_and_screen(oi_sync, 1, 1);
      
      lockin_info->m_i = lockin_info->n_i = LOCKIN_BUFFER_SIZE;
      lockin_info->c_i = lockin_info->ac_i = 0;
      lockin_info->navg = 8;
      lockin_info->alpha = -0.1;
      lockin_info->per_t = 66.7965;

      LOCK_FUNCTION(do_at_all_image);
      LOCK_VARIABLE(lockin_info);

      ph_lock.n_i = 50*LOCKIN_BUFFER_SIZE/3;
      ph_lock.ac_i = ph_lock.c_i = 0;
      ph_lock.peri = (int)66.7965;
      ph_lock.n_phi = (int)(128 * 66.7965);
      LOCK_FUNCTION(phase_timer);
      LOCK_VARIABLE(ph_lock);
      install_int(phase_timer,1);

      bid.param = (void*)lockin_info;
      bid.to_do = NULL;
      bid.timer_do = do_at_all_image;

      add_image_treat_menu_item ( "Avg", NULL, phase_in_live_video_image_menu(), 0, NULL);
      add_plot_treat_menu_item ( "refold phase",refold_phase ,NULL,0 ,NULL);
      add_plot_treat_menu_item ("simul phase",simul_phase ,NULL,0 ,NULL);
      add_plot_treat_menu_item ("slide phase",slidding_phase ,NULL,0 ,NULL);
      add_plot_treat_menu_item ("adjust pid",set_alpha ,NULL,0 ,NULL);
      add_plot_treat_menu_item ("adjust vco",adj_vco ,NULL,0 ,NULL);
    }
  return lockin_info;
}


int init_live_video_info(void)
{
  if (lockin_info == NULL) 
    {
      lockin_info = creating_lockin_info();
      m_action = 256;
      n_action = 0;
      action_pending = (f_action*)calloc(m_action,sizeof(f_action));
      if (action_pending == NULL)
	     win_printf_OK("Could not allocate action_pendind!");
      phaseindex_main(0, NULL);
    }
  return 0;
}




#endif
