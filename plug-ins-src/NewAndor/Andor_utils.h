#ifndef _ANDOR_UTILS_H_
#define _ANDOR_UTILS_H_
/** A structure to store ANDOR parameters

\author Adrien Meglio
*/
/* typedef struct ANDOR_PARAM_STRUCT */
/* {        */
/*   int standby_timeout; // Time in seconds before switching camera to standby mode */
/*   int standby_action; // Should the camera be switched into standby mode ? */
/*   int refresh_period; //During live the image is refreshed every refresh_period */
/*   float screen_frequency; // The target refresh frequency on the screen */
/*   int present_image_number_aquired_on_PC; //most recent image with data downloaded */
/*   int image_type; //type of image...should be IS_INT_IMAGE */
/*   int aquisition_mode; //Single or run till abort.... */
/*   int image_mode; */
/*   int transfer_mode; //to be in normal CCD collection or tranfer mode...ie no dead time */
/*   long size_circular_buffer; //size of the buffer where images are stored before DMA transfer */
/*   int pixels_x; //number of pixels in x on the CCD */
/*   int pixels_y; //number of pixels in x on the CCD */
/*   int bin_x; //binning in x */
/*   int bin_y; //binning in y */
/*   int subregion_x1; //end of horizontal position */
/*   int subregion_x2; //beginning of horizontal position */
/*   int subregion_y1; //end of horizontal position */
/*   int subregion_y2; //beginning of horizontal position */
/*   int total_pixels_number; //total number of pixels  */
/*   int total_width; //number of pixels in x in the image */
/*   int total_height; //number of pixels in y in the image */
/*   int gain; // Current gain */
/*   int gain_mode;  */
/*   int gain_min; // Minimum available gain */
/*   int gain_max; // Maximum available gain */
/*   float exposure_time; // Exposure time of a frame */
/*   float accumulation_time; */
/*   float kinetic_time; */
/*   int trigger_mode; */
/*   int shutter_mode; */
/*   int shutter_TTL; */
/*   int open_shutter; */
/*   int num_images; */
/*   int frames_in_tape_buffer; */
/*   int is_live; // Tells whether the camera currently displays live image */
/*   /\* CAUTION ! It is a switch that triggers LIVE mode *\/ */
/*   int is_live_previous; /\* Tells the status of the camera just before  */
/* 			   Useful to stop the cam for an operation and then go back to the previous status *\/ */
        
/*   int movie_status; // Tells wether a movie is acquired or not */
/*   /\* CAUTION ! It is a switch that triggers movie recording *\/ */
/*   int number_images_in_movie; // Number of images to record in the movie */
/*   int AD_channel; */
/*   int is_electron_multiplication; */
/*   float HSSpeed; /\* Speed of horizontal readout *\/ */
/*   float VSSpeed; /\* Speed of vertical readout *\/ */
/*   int temperature_target; /\* Target temperature (int format) *\/ */
/*   int temperature_lastread; /\* Last value of the temperature read from the camera *\/ */
/*   int contrast; /\* Defines whether set_zmin_zmax will be applied automatically or not *\/  */
/* } ANDOR_PARAM;   */

#define YES 1
#define NO 0

// Keyboard shortcuts (ixon.c)

#define SHORTCUT_ANDOR_Z_AUTO KEY_W /* Key "Z" on azerty keyboards */
#define SHORTCUT_ANDOR_PAUSE KEY_SPACE

// Conversion factor

#define MICRONS_PER_PIXEL .08

// DEFINEs for the iXon structure

#define WAKEUP 3
#define WAIT_FOR_WAKEUP 2
#define STANDBY 1

#define IS_ELECTRON_MULTIPLICATION 0
#define IS_CONVENTIONAL 1

#define FULL_IMAGE 512
#define IMAGE_512 512
#define IMAGE_256 256
#define IMAGE_128 128
#define IMAGE_64 64
#define IMAGE_32 32
#define IMAGE_16 16

#define IS_COOLER_ON 1
#define IS_COOLER_OFF 0
#define IS_COOLER_ERROR 2

#define OUT_OF_RANGE 1000

#define SINGLE_SCAN 1
#define ACCUMULATE 2
#define KINETICS 3
#define FAST_KINETICS 4
#define RUN_TILL_ABORT 5

#define MODE_TRANSFER 1
#define NO_MODE_TRANSFER 0

#define TTL_LOW 0
#define TTL_HIGH 0

#define INTERNAL 0
#define EXTERNAL 1

#define OPEN 1
#define CLOSED 0

#define GAIN_STANDARD 0
#define GAIN_EXTENDED 1
#define GAIN_LINEAR 2
#define GAIN_REAL 3

#define SHUTTER_AUTO 0
#define SHUTTER_OPEN 1
#define SHUTTER_CLOSE 2

#define BIN_1_1 0x00
#define BIN_2_2 0x11
#define BIN_4_4 0x33
#define BIN_8_8 0x77
#define BIN_16_16 0xFF

#define LIVE 1
#define FREEZE 0

#define TEMP_MINUS_70 -70
#define TEMP_0 0
#define TEMP_PLUS_20 20

#define IMAGE_MODE 4

#define IDT_TIMER1 100
#define SAVING_INTERVAL iXon.exposure_time*5

#define IDLE DRV_IDLE
#define ACQUIRING DRV_ACQUIRING

#define BLACK 0
#define BLUE 255
#define GREEN 65280
#define RED 16711680
#define WHITE 16777215
#define PINK 16737480

#define GREY_32 2105376
#define GREY_64 4210752
#define GREY_96 6316128
#define GREY_128 8421504
#define GREY_160 10526880
#define GREY_192 12632256
#define GREY_224 14737632

#define SMALLSIZE 8
#define MEDIUMSIZE 12
#define NORMALSIZE 16
#define BIGSIZE 24
#define HUGESIZE 36

#define INT_NULL -999999
#define FLOAT_NULL (float) INT_NULL
#define DOUBLE_NULL (double) INT_NULL
#define LONG_NULL (long) INT_NULL

#define AUTOMATIC 1
#define MANUAL 0
//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

// Config file functions
PXV_FUNC(void, create_config_file, (void));
PXV_FUNC(int, write_me, (char* buffer, char* string, ...));

// General camera functions
PXV_FUNC(int, set_image_size_on_ANDOR, (int bin_x,int bin_y,int subregion_x1,int subregion_x2,int subregion_y1,int subregion_y2));
PXV_FUNC(int, set_aq_time, (void));
PXV_FUNC(int, change_gain, (void));
PXV_FUNC(int, binning_test, (void));
PXV_FUNC(int, set_binning, (int bin));
PXV_FUNC(int, init_VSSpeed_to_max, (void));
PXV_FUNC(int, init_HSSpeed, (void));

// Error function
PXV_FUNC(int, error_report, (unsigned int error_code));
PXV_FUNC(int, test_error, (void));
PXV_FUNC(int, action__create_log_file, (void));
PXV_FUNC(int, error__create_log_file, (void));
PXV_FUNC(char*, formatted_time, (void));

// Initialization functions
PXV_FUNC(void, erase_ANDOR_parameters, (void));
PXV_FUNC(int, init_ANDOR_parameters, (void));
PXV_FUNC(int, Set_ANDOR_parameters, (void));
PXV_FUNC(int, initialize_ANDOR, (void));

// Shortcut functions

PXV_FUNC(int, short__set_z_auto, (void));
PXV_FUNC(int, short__pause, (void));


PXV_FUNC(int, Exit_ANDOR, (void));

// Housekeeping functions
PXV_FUNC(int, Andor_main, (void));
PXV_FUNC(int, Andor_unload, (void));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From _image_settings.c

// Image size functions
PXV_FUNC(int, update_unit_sets, (O_i* src_oi));

// Image acquisition functions
PXV_FUNC(int, remove_background_from_video__void, (void));
PXV_FUNC(int, remove_background_from_current_oi__void, (void));
PXV_FUNC(int, black_reference, (O_i* oi, float weight));
PXV_FUNC(int, adjust_pixels, (void));

// Base functions 
PXV_FUNC(imreg*, get_imr, (void));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From iXon_acq_control.c

// Live functions
PXV_FUNC(int, change_display_refreshing_period,(void));
PXV_FUNC(int, set_screen_refresh_freq, (void));
PXV_FUNC(int, allocate_live_buffer, (int sizeX,int sizeY,int pixelDepth));
PXV_FUNC(int, iXon_real_time_imaging, (void));
//PXV_FUNC(int, set_image_for_live, (void));

// Image functions
PXV_FUNC(int, zoom_out_iXon, (void));
PXV_FUNC(int, zoom_in_iXon, (void));

// Acquisition functions
PXV_FUNC(int, Stop_acquisition, (void));

// Single-image functions
PXV_FUNC(int, iXon_2_Xvin, (void));

// Housekeeping functions
PXV_FUNC(int, ixon_acq_main, (void));
PXV_FUNC(int, ixon_acq_unload, (void));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From iXon_tape.c

PXV_FUNC(int, iXon_RAM_movie, (void));

// Functions
PXV_FUNC(int, From_iXon_2_Xvin_Movie, (void));
PXV_FUNC(int, create_and_adapt_tape_iXon_buffer_image, (void));
PXV_FUNC(int, Kill_timer, (void));

// Tape functions
PXV_FUNC(void, CALLBACK automatic_saving, (HWND hwnd,UINT uMsg,UINT_PTR idEvent,DWORD dwTime));
PXV_FUNC(int, save_tape_mode, (void));
PXV_FUNC(int, save_image_header_JF, (O_i* oi, char *head, int size));
PXV_FUNC(int, close_movie_on_disk_JF, (O_i *oi, int nf));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From iXon_temp_control.c

// Temperature return functions
PXV_FUNC (int, give_me_temp_in,(void));
PXV_FUNC (float, give_me_temp_fl,(void));

// Temperature user functions
PXV_FUNC (int, set_temperature,(int temperature_target));
PXV_FUNC (int ,emergency_temperature_control,(void));

// Menu void functions
PXV_FUNC (int, give_me_temp_void,(void));
PXV_FUNC (int, give_me_temperature_range_void,(void));
PXV_FUNC (int, set_temperature_void,(void));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From iXon_threads.c

// Thread functions
PXV_FUNC(DWORD WINAPI, StandbyThreadProc, (LPVOID lpParam));
PXV_FUNC(DWORD WINAPI, RetrieveImages, (LPVOID lpParam));
PXV_FUNC(DWORD WINAPI, TempThreadProc, (LPVOID lpParam));
PXV_FUNC(DWORD WINAPI, InfoThreadProc, (LPVOID lpParam));
PXV_FUNC(DWORD WINAPI, StatusThreadProc, (LPVOID lpParam));
PXV_FUNC(DWORD WINAPI, thread__iXon_RAM_movie, (LPVOID lpParam));

// Initialization functions
PXV_FUNC(int, launcher_iXon_standby_thread, (void));
PXV_FUNC(int, create_iXon_retrieve_images_thread, (void));
PXV_FUNC(int, create_iXon_info_thread, (void));
PXV_FUNC(int, create_iXon_temp_thread, (void));
PXV_FUNC(int, launcher__iXon_display_thread, (void));
PXV_FUNC(int, launcher__iXon_RAM_movie, (void));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From menu_iXon.c

PXV_FUNC(int, create_HSpeed_submenu, (void));
PXV_FUNC(int, create_VSpeed_submenu, (void));
PXV_FUNC(int, create_gain_submenu,   (void));

// Menus minifunctions
PXV_FUNC(int, set_HSpeed_submenu_void, (void));
PXV_FUNC(int, set_VSpeed_submenu_void, (void));
PXV_FUNC(int, set_temperature_submenu_void, (void));
PXV_FUNC(int, set_binning_submenu_void, (void));
PXV_FUNC(int, set_gain_submenu_void, (void));

// Menus
PXV_FUNC(MENU, *ixon_acq_menu, (void));
PXV_FUNC(MENU, *ixon_img_menu, (void));
PXV_FUNC(MENU, *ixon_temp_menu, (void));
PXV_FUNC(int, menu_ixon_main, (void));


//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From gui_caps.c

PXV_FUNC(int, bubble_test, (void));
PXV_FUNC(BITMAP*, new_button, (int x_pos, int y_pos, int n_lines, char *example_test));
PXV_FUNC(int, rgb2c, (int red, int green, int blue));
PXV_FUNC(int, special_bubble, (int x,int y,int color,int size,char *format, ...));

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// Imported from plugin "logfile"

PXV_FUNC(FILE*, fopen__event, (void));
PXV_FUNC(int, win_event, (char *message, ...));
PXV_FUNC(int, win_report, (char *message, ...));
PXV_FUNC(int, action__create_log_file, (void));
PXV_FUNC(int, message__create_log_file, (void));

// Imported from plugin "nidaq_commander_adrien"

PXV_FUNC(void, IO__give_line_level, (int line, float *level));
PXV_FUNC(void, IO__give_line_status, (int line, int *status));

// Imported from plugin "motor_adrien"

//PXV_FUNC(int, IO__whatis__motor_p_position_in_microns, (float* position_in_microns));
//PXV_FUNC(int, IO__whatis__motor_p_position_in_percent, (float* position_in_percent));
PXV_FUNC(int, IO__whatis__motor_p_consigne_in_microns, (float* position_in_microns));
PXV_FUNC(int, IO__whatis__motor_r_consigne_in_turns, (float* position_in_turns));
PXV_FUNC(int, IO__whatis__motor_t_consigne_in_mm, (float* position_in_mm));

// Imported from plugin "shortcuts_adrien"

PXV_FUNC(int,  shortcut_main, (void));

#ifdef _ANDOR_UTILS_C

int flag__display_yes_no;
O_i *oi_buffer;

AndorCapabilities caps; 
//ANDOR_PARAM Andor;
pltreg, *pr_iXon;
long, *Img_buff;

int, n_images_to_be_saved;
long int, previous_image_for_tape_saving;
FILE, *fp_images;
long int present_image_number_for_tape_saving;
int n_images_saved;


MENU binning_submenu[32];
MENU temperature_submenu[32];
MENU HSpeed_submenu[32];
MENU VSpeed_submenu[32];
MENU gain_submenu[32];


#else

PXV_VAR(int, flag__display_yes_no);
PXV_VAR(O_i, *oi_buffer);

PXV_VAR(AndorCapabilities, caps); 
//PXV_VAR(ANDOR_PARAM, Andor);

PXV_VAR(pltreg, *pr_iXon);
PXV_VAR(long, *Img_buff);

PXV_VAR(int, n_images_to_be_saved);
PXV_VAR(long int, previous_image_for_tape_saving);
PXV_VAR(FILE, *fp_images);
PXV_VAR(long int, present_image_number_for_tape_saving);
PXV_VAR(int, n_images_saved);


PXV_VAR(MENU, binning_submenu[32]);
PXV_VAR(MENU, temperature_submenu[32]);
PXV_VAR(MENU, HSpeed_submenu[32]);
PXV_VAR(MENU, VSpeed_submenu[32]);
PXV_VAR(MENU, gain_submenu[32]);

#endif
#endif
