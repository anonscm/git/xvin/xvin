/*
*    Plug-in module allowing to track bead from a movie
 *
 *    V. Croquette
 *    JF Allemand
  */
#ifndef _MOVIE_SOURCE_C_
#define _MOVIZ_SOURCE_C_


# include "allegro.h"
# include "winalleg.h"
# include <windowsx.h>
# include "ctype.h"
# include "xvin.h"

XV_FUNC(int, do_load_im, (void));






# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

//# include "movie_source.h"
# include "trackBead.h"



float read_Z_value(void)
{
  return 0.0;
}
int set_Z_value(float z)
{
  return 0;
}




int freeze_source(void)
{
  imreg *imr;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;	
    }
  //else if (go_track == TRACK_FREEZE) active_menu->flags |=  D_DISABLED;
  else active_menu->flags &=  ~D_DISABLED;
  if(updating_menu_state != 0)	return D_O_K;
  go_track = TRACK_FREEZE;
  return D_O_K;
}


int live_source(void)
{
  imreg *imr;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |= D_DISABLED;
      return D_O_K;	
    }
  //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
  else active_menu->flags &= ~D_DISABLED;
  if(updating_menu_state != 0)	return D_O_K;
  go_track = TRACK_ON;
  return D_O_K;
}
int kill_source(void)
{
  imreg *imr;
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    {
      active_menu->flags |= D_DISABLED;
      return D_O_K;	
    }
  //else if (go_track == TRACK_ON) active_menu->flags |=  D_DISABLED;
  else active_menu->flags &= ~D_DISABLED;
  if(updating_menu_state != 0)	return D_O_K;
  go_track = TRACK_STOP;
  return D_O_K;
}

int prepare_image_overlay(O_i* oi)
{
  return 0;
}

VOID CALLBACK dummyAPCProc(LPVOID lpArgToCompletionRoutine,
  DWORD dwTimerLowValue,  DWORD dwTimerHighValue)
{
  return;
}



DWORD WINAPI TrackingThreadProc( LPVOID lpParam ) 
{
    HANDLE hTimer = NULL;
    LARGE_INTEGER liDueTime;
    tid *ltid;
    imreg *imr;
    DIALOG *d;
    O_i *oi;
    int nf, im_n = 0;;
    unsigned long dt = 0, t0 = 0, t1 = 0, dtmax = 0;
    long long t;
    DIALOG *starting_one = NULL;
    Bid *p;

    if (imr_TRACK == NULL || oi_TRACK == NULL)	return 1;
    liDueTime.QuadPart=-300000; // 30 ms
    ltid = (tid*)lpParam;

    imr = ltid->imr;
    d = ltid->dimr;
    oi = ltid->oi;
    p = ltid->dbid;
    nf = abs(oi->im.n_f);		
    // Create a waitable timer.
    hTimer = CreateWaitableTimer(NULL, TRUE, "WaitableTimer2");
    if (!hTimer)   win_printf_OK("CreateWaitableTimer failed (%d)\n", GetLastError());
    t0 = my_uclock();
    starting_one = active_dialog; // to check if windows change
    source_running = 1;
    while (go_track)
      {
	// Set a timer to wait for 10 seconds.
	if (!SetWaitableTimer(hTimer, &liDueTime, 0, dummyAPCProc, NULL, 0))
	  win_printf_OK("SetWaitableTimer failed (%d)\n", GetLastError());

	// Wait for the timer.
	SleepEx (INFINITE, TRUE);
	t1 = my_uclock();
	dt = t1 - t0;	    
	if (dt > dtmax) dtmax = dt;
	t0 = t1;
	if (go_track == TRACK_FREEZE) continue;
	oi->im.c_f++;
	if (oi->im.c_f >= nf) oi->im.c_f = 0;
	switch_frame(oi,oi->im.c_f);
	//tim[oi->im.c_f] = t1;
	oi->need_to_refresh |= BITMAP_NEED_REFRESH;
	display_image_stuff_16M(imr,d);
	d->d1 = (int)dt;


	t = my_ulclock();
	dt = my_uclock();
	bid.previous_fr_nb = im_n;      
	for (p->timer_do(imr_TRACK, oi_TRACK, im_n, d_TRACK, t ,dt ,p->param, 0) ; 
	   p->timer_do != NULL && p->next != NULL; p = p->next)
	  p->timer_do(imr_TRACK, oi_TRACK,im_n, d_TRACK, t, dt, p->param, 0);
	im_n++;      
	oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;

	//dtim[oi->im.c_f] = my_uclock() - t1;
      }
    //dt_max = 1000*((double)dtmax)/get_my_uclocks_per_sec();
    source_running = 0;
    return 0;
}

int init_image_source(void)
{
  win_printf("Load a movie that will be used as a tracking source");
  do_load_im();
  return broadcast_dialog_message(MSG_DRAW,0);    
}
int start_data_movie(imreg *imr)
{
  return 0;
}


# endif
