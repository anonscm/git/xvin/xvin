Plug-in "ixon"

This hardware plug-in has been developed to be used with the iXon Andor camera. 

It enables at this day :
- live display of the camera
- RAM and ROM movies acquisition
- zooming
- gain control
- binning control

It has been written by JF Allemand and Adrien Meglio

This README file has been written May 09, 2006 by AM