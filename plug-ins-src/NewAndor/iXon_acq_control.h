#ifndef _IXON_ACQ_CONTROL_H_
#define _IXON_ACQ_CONTROL_H_

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Global variables declaration

//extern IXON_PARAM iXon; // iXon parameters
//extern pltreg *pr_iXon;
//extern long *Img_buff;
//extern O_i *oi_iXon;
//extern imreg *imr_iXon;
//extern DIALOG *d_iXon;
//extern O_i *oi_iXon_tape ;
//extern int n_images_to_be_saved ;
//extern long int previous_image_for_tape_saving;
//extern FILE *fp_images ;
//extern long int present_image_number_for_tape_saving ;
//extern int n_images_saved ;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Function declarations

//// Image functions
//PXV_FUNC(int, set_image_iXon, (int bin_x,int bin_y,int subregion_x1,int subregion_x2,int subregion_y1,int subregion_y2));
//PXV_FUNC(int, set_image_and_allocate_iXon, (int bin_x,int bin_y,int subregion_x1,int subregion_x2,int subregion_y1,int subregion_y2));
//PXV_FUNC(int, Set_iXon_Xvin_image_size, (void));
//PXV_FUNC(int, zoom_out_iXon, (void));
//PXV_FUNC(int, zoom_in_iXon, (void));
//PXV_FUNC(int, set_image_and_allocate_iXon, (int bin_x,int bin_y,int subregion_x1,int subregion_x2,int subregion_y1,int subregion_y2));
//
//// Acquisition functions
//PXV_FUNC(int, set_aq_time, (void));
//PXV_FUNC(int, iXon_real_time_imaging, (void));
//PXV_FUNC(int, Stop_acquisition, (void));
//PXV_FUNC(void, Exit_iXon, (void));
//PXV_FUNC(int, iXon_2_Xvin, (void));
//
//// Housekeeping functions
//PXV_FUNC(int, ixon_acq_main, (void);
//PXV_FUNC(int, ixon_acq_unload, (void);

#endif

