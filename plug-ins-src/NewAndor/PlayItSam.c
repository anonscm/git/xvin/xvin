/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _PLAYITSAM_C_
#define _PLAYITSAM_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"
# include "xv_main.h"

/* If you include other plug-ins header do it here*/ 



# define BUILDING_PLUGINS_DLL
# include "../cfg_file/Pico_cfg.h"
# include "../cfg_file/microscope.h"




XV_FUNC(int, do_load_im, (void));

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "playitsam.h"
//# include "fftbtl32n.h"
//# include "fillibbt.h"
//# include "track_util.h"
# include "action_live_video.h"
# include "movie_live_video.h"

PXV_FUNC(int, get_present_image_nb, (void));

HANDLE live_video_hThread = NULL;
DWORD live_video_dwThreadId;

int   playitsam_init = 0;

int mouse_selected_bead = -1;

int d_draw_LIVE_VIDEO_Im_proc(int msg, DIALOG *d, int c)
{
  return d_draw_Im_proc(msg, d, c);
}

int live_video_oi_mouse_action(O_i *oi, int x0, int y0, int mode, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  BITMAP *imb;
  unsigned long t0 = 0;
  int cl, cw, pxl, pyl, c_b;
  int (*post_display)(struct one_image *oi, DIALOG *d);
  static float zmag_asked = 0;
  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;


  if(mouse_b == 2)
    {

    }

  while(mouse_b == 1)
    {
      if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
	{
	  t0 = my_uclock();
	  post_display = oi->oi_post_display;
	  oi->oi_post_display = NULL;
	  display_image_stuff_16M(imr,d);
	  oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
	  t0 = my_uclock() - t0;
	  write_and_blit_im_box( plt_buffer, d, imr);
      	  oi->oi_post_display = post_display; 

	  imb = (BITMAP*)oi->bmp.stuff;

	  //move_bead_cross(screen, imr, d, mouse_x, mouse_y);

	}
      if (general_idle_action) general_idle_action(d);
    }
  return 0;
}




int live_video_before_menu(int msg, DIALOG *d, int c)
{
  /* this routine switches the display flag so that no magic color is drawn 
     when menu are activated */
  if (msg == MSG_GOTMOUSE)
    {
      do_refresh_overlay = 0;
    }
  return 0;
}
int live_video_after_menu(int msg, DIALOG *d, int c)
{
  return 0;
}

int live_video_oi_got_mouse(struct  one_image *oi, int xm_s, int ym_s, int mode)
{
  /* this routine switches the display flag so that the magic color is drawn again
     after menu are activated */
  do_refresh_overlay = 1;
  return 0;
}


int live_video_oi_post_display(struct one_image *oi, DIALOG *d)
{
  imreg *imr;

  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;

  imr = (imreg*)d->dp;
  //show_bead_cross(plt_buffer, imr, d);
  return 0;
}

int live_video_oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  BITMAP *imb;
  unsigned long t0 = 0;
  float zo, zm;
  struct box *b = NULL;

  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

  t0 = my_uclock();
  if (oi->need_to_refresh & BITMAP_NEED_REFRESH)
    {
      display_image_stuff_16M(imr,d);
      oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
    }

  imb = (BITMAP*)oi->bmp.stuff;
  //imr_LIVE_VIDEO_redraw_background = show_bead_cross(imb, imr, d);


  if (generate_imr_LIVE_VIDEO_title && imr_LIVE_VIDEO_title_change_asked > imr_LIVE_VIDEO_title_displayed)
    {// display video image when title change
      set_im_title(oi_LIVE_VIDEO, generate_imr_LIVE_VIDEO_title());
      imr_LIVE_VIDEO_title_displayed = imr_LIVE_VIDEO_title_change_asked;
      imr_LIVE_VIDEO_redraw_background = 1;
      ttf_enable = 0;
      b = &imr_LIVE_VIDEO->stack;
      do_one_image (imr_LIVE_VIDEO);
      oi_LIVE_VIDEO->need_to_refresh &= BITMAP_NEED_REFRESH;
      adj_image_pos(imr_LIVE_VIDEO, d_LIVE_VIDEO);
      restore_image_parameter(imr_LIVE_VIDEO,  d_LIVE_VIDEO);
    }


  if (imr_LIVE_VIDEO_redraw_background)  // slow redraw
    {
      write_and_blit_im_box( plt_buffer, d, imr);
      t0 = my_uclock() - t0;

    }
  else  if (oi->need_to_refresh & INTERNAL_BITMAP_NEED_REFRESH)
    {
      SET_BITMAP_IN_USE(oi);
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off //+ d->x
	   , imr->y_off - imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);
      oi->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
      t0 = my_uclock() - t0;
      SET_BITMAP_NO_MORE_IN_USE(oi);
    }
  return 0;
}

void stop_source_thread(void)
{
  DWORD lpExitCode;

  go_live_video = LIVE_VIDEO_STOP;
  //win_printf("Stopping thread");
  broadcast_dialog_message(MSG_DRAW,0); // needed to prevent freezing dialog!
  GetExitCodeThread(live_video_hThread, &lpExitCode);
  TerminateThread(live_video_hThread, lpExitCode);
  CloseHandle(live_video_hThread);

  while (source_running);
}


int source_end_action(DIALOG *d)
{
  stop_source_thread();
  return 0;
}



int find_next_available_job_spot(void)
{
  int i;

  if (job_pending == NULL) return -1;
  // we look for used job to recycle 
  for (i = 0; i < n_job; i++)  
    if (job_pending[i].in_progress == 0) break; 
  if (i == n_job)
        n_job = (n_job < m_job) ? n_job + 1 : n_job;
  if (i >= m_job) return -2;                 // the buffer is full
  return i;
}


f_job* find_job_associated_to_plot(O_p *op)
{
  int i;

  if (job_pending == NULL) return NULL;
  // we look for used job to recycle 
  for (i = 0; i < n_job; i++)  
    {
      if (job_pending[i].in_progress == 0) continue; 
      if (job_pending[i].op == op) return job_pending + i; 
    }
  return NULL;
}

f_job* find_job_associated_to_image(O_i *oi)
{
  int i;

  if (job_pending == NULL) return NULL;
  // we look for used job to recycle 
  for (i = 0; i < n_job; i++)  
    {
      if (job_pending[i].in_progress == 0) continue; 
      if (job_pending[i].oi == oi) return job_pending + i; 
    }
  return NULL;
}


int fill_next_available_job_spot(int in_progress, int im, int type, int bead_nb, 
				 imreg *imr, O_i *oi, pltreg *pr, 
				  O_p *op, void* more_data, 
				 int (*job_to_do)(int im, struct future_job *job)
				 ,int local)
{
  int i;

  i = find_next_available_job_spot();
  if (i < 0) return -1;
  job_pending[i].in_progress = in_progress;
  job_pending[i].imi = im;
  job_pending[i].last_imi = im - 1;
  job_pending[i].type = type;
  job_pending[i].bead_nb = bead_nb;
  job_pending[i].imr = imr;
  job_pending[i].oi = oi;
  job_pending[i].pr = pr;
  job_pending[i].op = op;
  job_pending[i].more_data = more_data;
  job_pending[i].job_to_do = job_to_do;
  job_pending[i].local = local;
  return i;
}


int fill_local_job_value(int jobid, int local)
{
  if (jobid < 0 || jobid >= n_job) return  1;
  job_pending[jobid].local = local;
  return 0;
}


int find_next_job(int imi)
{
  int i, j;

  if (job_pending == NULL) return -1;
  for (i = 0, j = -1; i < n_job; i++)
    {
      if (job_pending[i].in_progress == 0) continue;
      else j = i;
      if (job_pending[i].last_imi < imi) 
	{
	  if (job_pending[i].imi < 0) break;
	  if (job_pending[i].imi < imi) break;
	}
    }
  if (i < n_job) return i;
  else  n_job = j+1;
  return -2;
}

int find_remaining_job(int imi)
{
  int i, j;

  if (job_pending == NULL) return -1;
  for (i = 0, j = 0; i < n_job; i++)
      j += (job_pending[i].in_progress) ? 1 : 0;
  return j;
}





int source_idle_action(DIALOG *d)
{
  return 0;
}



// function executed in the live_video thread
int create_live_video_thread(tid *dtid)
{
    
    live_video_hThread = CreateThread( 
            NULL,              // default security attributes
            0,                 // use default stack size  
            LiveVideoThreadProc,// thread function 
            (void*)dtid,       // argument to thread function 
            0,                 // use default creation flags 
            &live_video_dwThreadId);      // returns the thread identifier 

    if (live_video_hThread == NULL) 	win_printf_OK("No thread created");
    SetThreadPriority(live_video_hThread,  THREAD_PRIORITY_NORMAL);
    return 0;
}

int display_imr_and_pr(void)
{
  int i;
  DIALOG *di, *dip, *dim = NULL;

  if(updating_menu_state != 0)	return D_O_K;


  if (pr_LIVE_VIDEO == NULL || imr_LIVE_VIDEO == NULL) return D_O_K;

  i = retrieve_index_of_menu_from_dialog();
  if (i < 0)     win_printf_OK("Cannot find menu dialog");
  di = the_dialog + i;

  i = find_imr_index_in_current_dialog(the_dialog);
  if (i >= 0)   dim = the_dialog + i;
  else 	        win_printf_OK("Cannot find Im dialog");
  set_dialog_size_and_color(dim, SCREEN_W/2, 19, SCREEN_W/2-1, dim->h, 255, 0);
  imr_and_pr = 1;


  remove_all_keyboard_proc();	
  dip = attach_new_plot_region_to_dialog( 0, 0, 0, 0);
  if (dip == NULL)        allegro_message("dialog pb");
  set_dialog_size_and_color(dip, 0, 19, SCREEN_W/2-1, dim->h, 255, 0);
  dip->dp = (void*)imr_LIVE_VIDEO;	
  dip->proc = d_draw_Im_proc;
  dip->fg = makecol(0, 0, 0);
  dip->bg = makecol(128, 128, 128);	

  scan_and_update((MENU*)di->dp);

  switch_project_to_this_pltreg(pr_LIVE_VIDEO);

  broadcast_dialog_message(MSG_DRAW,0);
  return D_REDRAWME;
}



int check_remaining_job(void)
{

  if(updating_menu_state != 0)	return D_O_K;

  return win_printf_OK("number of remaining jobs %d",find_remaining_job(get_present_image_nb()));
}



MENU *playitsam_image_menu(void)
{
	static MENU mn[32];
	extern int save_finite_movie(void);
	

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"freeze video", freeze_source,NULL,0,NULL);
	add_item_to_menu(mn,"live video", live_source,NULL,0,NULL);
	add_item_to_menu(mn,"kill video", kill_source,NULL,0,NULL);
	add_item_to_menu(mn,"Load Image\t(Ctrl-I)",do_load_im, NULL, 0, NULL);
	add_item_to_menu(mn,"check jobs", check_remaining_job,NULL,0 ,NULL);
	add_item_to_menu(mn,"plot and image", display_imr_and_pr,NULL,0 ,NULL);


	return mn;
}



// playitsam 3D bead live_video software wich can be compile to use different image sources
// simulate a bead : "game"
// use a movie :  "movie"
// use IFC data acquisition "ifc"
// use CVB data acquisition "cvb"
  
// source have specific functions sharing the same names
// int init_image_source();
// int start_data_movie(imreg *imr);
// DWORD WINAPI Live_VideoThreadProc( LPVOID lpParam ); 

// playitsam creates an image region withe the special image to live_video
// it defines the image idle action which typically consists in screen refresh
// it also creates a plot region where timing plots are gathered
// it also fills up a structure with all these variables
// it adds a general menu to images: playitsam_image_menu()
// it lauches a thread for live_video with a specific function Live_VideoThreadProc
// this function trigger a timer function used to live_video 


int	playitsam_main(int argc, char **argv)
{
  imreg *imr = NULL;
  pltreg *pr = NULL;
  O_p *op;
  d_s *ds;
  O_i *oi;
  DIALOG *di_mn, *di;
  float r, z;
  int i, ret = 0;
  int init_live_video_info(void);
  extern MENU   *save_movie_in_track_image_menu(void);
  XV_ARRAY(MENU,  help_menu);

  XVcfg_main();
  imr = find_imr_in_current_dialog(NULL);

  // if a movie is load already we use this movie as a data source 
  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL)    ret = init_image_source(NULL,1);
  else
    {
        if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)   ret = init_image_source(imr,1);
	if (oi->im.movie_on_disk == 0 || oi->im.n_f < 2)  ret = init_image_source(imr,1);
	if (oi->im.nx == 0) 	                          ret = init_image_source(imr,1); 
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    return win_printf_OK("You must load a movie in the active window");
  if (oi->filename == NULL) set_oi_filename(oi, "LIVE VIDEO");

  // We create a plot region to display bead position, timing  etc.
  pr = create_hidden_pltreg_with_op(&op, 4096, 4096, 0,"Live_Video status");
  if (pr == NULL)  win_printf_OK("Could not find or allocte plot region!");
  pr_LIVE_VIDEO = pr;
  add_item_to_menu(plot_menu, "Help", NULL, help_menu, 0, NULL);
  
  if (op == NULL)  win_printf_OK("Could not find or allocate plot !");

  op->dat[0]->xd[1] = op->dat[0]->yd[1] = 1; 
  pr->one_p->need_to_refresh = 1;
  switch_project_to_this_imreg(imr);
  broadcast_dialog_message(MSG_DRAW,0);    

  m_job = 256;
  n_job = 0;
  job_pending = (f_job*)calloc(m_job,sizeof(f_job));
  if (job_pending == NULL)
    win_printf_OK("Could not allocate job_pending!");


  //we attach image menu
  if (imr_LIVE_VIDEO == NULL)
    {
      add_image_treat_menu_item ( "playitsam", NULL, playitsam_image_menu(), 0, NULL);
      di_mn = the_dialog + 1;
      rectfill(screen,di_mn->x,di_mn->y,di_mn->x+di_mn->w-1,
	       di_mn->y+di_mn->h-1, makecol(255,255,255));
      di_mn->w = 0; 	   di_mn->h = 0;
      xvin_d_menu_proc(MSG_END,the_dialog + 1,0);
      add_item_to_menu(image_menu, "Help", NULL, help_menu, 0, NULL);
      xvin_d_menu_proc(MSG_START,the_dialog + 1,0); 
      xvin_d_menu_proc(MSG_DRAW,the_dialog + 1,0);
      start_data_movie(imr); 
    }

  imr_LIVE_VIDEO = imr;
  oi_LIVE_VIDEO = imr->one_i;
  add_image_treat_menu_item ( "Save movie", NULL, save_movie_in_live_video_image_menu(), 0, NULL);
  d_LIVE_VIDEO = find_dialog_associated_to_imr(imr_LIVE_VIDEO, NULL);
  dtid.imr = imr;
  dtid.pr = pr;
  dtid.oi = oi_LIVE_VIDEO;
  dtid.op = op;
  dtid.dimr = d_LIVE_VIDEO;
  dtid.dpr = find_dialog_associated_to_pr(pr_LIVE_VIDEO, NULL);
  dtid.dbid = &bid;
  before_menu_proc = live_video_before_menu;
  after_menu_proc = live_video_after_menu;
  oi_LIVE_VIDEO->oi_got_mouse = live_video_oi_got_mouse;
  oi_LIVE_VIDEO->oi_idle_action = live_video_oi_idle_action;
  oi_LIVE_VIDEO->oi_mouse_action = live_video_oi_mouse_action;
  oi_LIVE_VIDEO->oi_post_display = live_video_oi_post_display;
  init_live_video_info();
  go_live_video = LIVE_VIDEO_ON;
  create_live_video_thread(&dtid);
  general_end_action = source_end_action;
  general_idle_action = source_idle_action;

  if (SCREEN_W > 1800) 
    display_imr_and_pr();
  return D_O_K;
}

int	playitsam_unload(int argc, char **argv)
{
  remove_item_to_menu(image_treat_menu, "playitsam", NULL, NULL);
  return D_O_K;
}
#endif

