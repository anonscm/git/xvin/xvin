#ifndef _IXON_H_
#define _IXON_H_

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Global variables declaration

AndorCapabilities caps; // AndorCapabilities structure
IXON_PARAM iXon; // iXon parameters
pltreg *pr_iXon = NULL;
long *Img_buff = NULL;
O_i *oi_iXon = NULL;
imreg *imr_iXon = NULL;
DIALOG *d_iXon = NULL;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Functions declaration

//// General camera functions
//PXV_FUNC(int, change_gain, (void));
//PXV_FUNC(int, binning_test, (void));
//PXV_FUNC(int, set_binning, (int bin));
//PXV_FUNC(int, init_VSSpeed_to_max, (void));
//PXV_FUNC(int, init_HSSpeed, (void));
//
//// Error function
//PXV_FUNC(int, error_report, (unsigned int error_code));
//PXV_FUNC(int, test_error, (void));
//
//// Initialization functions
//PXV_FUNC(int, init_parameters, (void));
//PXV_FUNC(int, reinit_image_size, (void));
//PXV_FUNC(int, Set_iXon_parameters, (void));
//PXV_FUNC(int, initialize_ixon, (void));
//PXV_FUNC(int, Allocate_buffer, (int sizeX,int sizeY,int pixelDepth));
//
//// Housekeeping functions
//PXV_FUNC(int, ixon_main, (void));
//PXV_FUNC(int, ixon_unload, (void));

#endif

