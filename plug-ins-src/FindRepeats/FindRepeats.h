#ifndef _FINDREPEATS_H_
#define _FINDREPEATS_H_

PXV_FUNC(int, do_FindRepeats_rescale_plot, (void));
PXV_FUNC(MENU*, FindRepeats_plot_menu, (void));
PXV_FUNC(int, do_FindRepeats_rescale_data_set, (void));
PXV_FUNC(int, FindRepeats_main, (int argc, char **argv));
#endif

