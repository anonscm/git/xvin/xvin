/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _FINDREPEATS_C_
#define _FINDREPEATS_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin 
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "FindRepeats.h"

int	FindRepeats_unload(int argc, char **argv);
int use_visible_points_of_one_ds_to_remove_points_of_other_ds_with_same_x(void);
int use_invisible_points_of_one_ds_to_remove_points_of_other_ds_with_same_x(void);
int use_visible_points_of_one_ds_to_remove_points_of_other_ds_with_same_x(void);
int use_invisible_points_of_one_ds_to_remove_points_of_other_ds_with_same_x(void);
int do_FindRepeats_in_data_set_2(void);
int mean_and_sigma_on_array(float *yd, int nx, float *meany, float *sig);
int least_square_fit_on_ds2(d_s *dsi1, int bool_not_affine, float c_error, double *ar, double *br, double *chi2r);
int find_HF_chi2on_ds2(d_s *dsi1, float c_error, double ar, double br, double *hfchi2r);
int do_FindRepeats_hello(void);

int do_FindRepeats_hello(void)
{
  register int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from FindRepeats");
  if (i == WIN_CANCEL) win_printf_OK("you have press CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}


int find_HF_chi2on_ds2(d_s *dsi1, float c_error, double ar, double br, double *hfchi2r)
{
  register int i;
  int  nx1, has_good_error_bars = 0;
  double tmp, sig2, sig_2, chi2;
  
  
  // by default, we compute an affine fit y=ax+b; but if ALT is pressed, the fit is linear y=ax

  if (dsi1 == NULL) 	return -1;
  nx1 = dsi1->nx;
  if (nx1 < 2)   	return -2;
  
  if (dsi1->ye == NULL)    has_good_error_bars = 0;
  else
    {
      for (i = 0, has_good_error_bars = 1; i < nx1 && has_good_error_bars == 1; i++)
	if (dsi1->ye[i] == 0) has_good_error_bars = 0;
    }

  if (has_good_error_bars == 0)
    {
      for (i=1, chi2 = 0; i< nx1 ; i++)
    	{      
	  sig2 = 2 * c_error * c_error;
	  sig_2 = (double)1/sig2;
	  tmp = (ar * dsi1->xd[i] + br - dsi1->yd[i]) - (ar * dsi1->xd[i-1] + br - dsi1->yd[i-1]);
	  chi2 += tmp * tmp * sig_2;
	}
    }
  else  // with error bars in y
    {
      for (i=0, chi2 = 0; i< nx1 ; i++)
    	{      
	  sig2 = dsi1->ye[i] * dsi1->ye[i] + dsi1->ye[i-1] * dsi1->ye[i-1];
	  sig_2 = (double)1/sig2;
	  tmp = (ar * dsi1->xd[i] + br - dsi1->yd[i]) - (ar * dsi1->xd[i-1] + br - dsi1->yd[i-1]);
	  chi2 += tmp * tmp * sig_2;
	}
    }      
  if (hfchi2r != NULL) *hfchi2r = chi2;
  return 0;
}

int least_square_fit_on_ds2(d_s *dsi1, int bool_not_affine, float c_error, double *ar, double *br, double *chi2r)
{
  register int i;
  int  nx1, has_good_error_bars = 0;
  double a, b;
  double sy2 = 0, sy = 0, sx = 0, sx2 = 0, sxy = 0, sig2, sig_2, chi2, ny = 0;
  
  
  // by default, we compute an affine fit y=ax+b; but if ALT is pressed, the fit is linear y=ax

  if (dsi1 == NULL) 	return -1;
  nx1 = dsi1->nx;
  if (nx1 < 2)   	return -2;
  
  if (dsi1->ye == NULL)    has_good_error_bars = 0;
  else
    {
      for (i = 0, has_good_error_bars = 1; i < nx1 && has_good_error_bars == 1; i++)
	if (dsi1->ye[i] == 0) has_good_error_bars = 0;
    }

  if (has_good_error_bars == 0)
    {
      for (i=0; i< nx1 ; i++)
    	{
	  sy += dsi1->yd[i];
	  sy2 += dsi1->yd[i] * dsi1->yd[i];
	  sx += dsi1->xd[i];
	  sx2 += dsi1->xd[i] * dsi1->xd[i];
	  sxy += dsi1->xd[i] * dsi1->yd[i];
	}
      if (bool_not_affine==1)
	{	
	  if (sx2==0) return -3; //(win_printf_OK("\\Sigma x_i^2 = 0   => cannot perform fit."));
	  a = (sxy/sx2);
	  b= 0.;
	}
      else
    	{	
	  b = (sx2 * nx1) - sx * sx;
	  if (b == 0) return -4; //(win_printf_OK("\\Delta  = 0 \n can't fit that!"));
	  a = sxy * nx1 - sx * sy;
	  a /= b;
	  b = (sx2 * sy - sx * sxy)/b;
	}
      for (i=0, chi2 = 0; i< nx1 ; i++)
    	{      
	  sig2 = c_error * c_error;
	  sig_2 = (double)1/sig2;
	  chi2 += (a * dsi1->xd[i] + b - dsi1->yd[i]) * (a * dsi1->xd[i] + b - dsi1->yd[i]) * sig_2;
	}
    }
  else  // with error bars in y
    {
      for (i=0; i< nx1 ; i++)
    	{
	  sig2 = dsi1->ye[i] * dsi1->ye[i];
	  sig_2 = (double)1/sig2;
	  sy += dsi1->yd[i] * sig_2;
	  sy2 += dsi1->yd[i] * dsi1->yd[i] * sig_2;
	  sx += dsi1->xd[i] * sig_2;
	  sx2 += dsi1->xd[i] * dsi1->xd[i] * sig_2;
	  sxy += dsi1->xd[i] * dsi1->yd[i] * sig_2;
	  ny += sig_2;
	}
      if (bool_not_affine==1)
	{	
	  if (sx2==0) return -3; //(win_printf_OK("\\Sigma x_i^2 = 0   => cannot perform fit."));
	  a = (sxy/sx2);
	  b= 0.;
	}
      else
    	{	
	  b = (sx2 * ny) - sx * sx;
	  if (b == 0) return -4; // (win_printf_OK("\\Delta  = 0 \n can't fit that!"));
	  a = sxy * ny - sx * sy;
	  a /= b;
	  b = (sx2 * sy - sx * sxy)/b;
	}
      
      for (i=0, chi2 = 0; i< nx1 ; i++)
    	{      
	  sig2 = dsi1->ye[i] * dsi1->ye[i];
	  sig_2 = (double)1/sig2;
	  chi2 += (a * dsi1->xd[i] + b - dsi1->yd[i]) * (a * dsi1->xd[i] + b - dsi1->yd[i]) * sig_2;
	}
    }      

  if (ar != NULL) *ar = a;
  if (br != NULL) *br = b;
  if (chi2r != NULL) *chi2r = chi2;
  return 0;
}



int do_FindRepeats_in_data_set(void)
{
  register int i, j, k;
  O_p *op = NULL, *opn = NULL, *opc = NULL, *oper = NULL;
  int nf, ids, sds, nds;
  static float c_error = 3;
  d_s *ds, *dsi, *dsc, *dsN, *dsa, *dsb, *dser;
  pltreg *pr = NULL;
  char question[4096], c[256];
  static int loc = 0, rmin = 0, rmax = 200, allds = 0, newplotfit = 0, error = 0, same = 0;
  int best_k;
  double best_a = 1, ar = 1;
  double best_b = 0, br = 0;
  double best_chi2 = 0, chi2r = 0;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine fits data from hybridization experiments\n"
			 "and adjust the number of bases between two oligos\n"
			 "to minimize \\chi^2 of a data set");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  snprintf(question,sizeof(question), "This routine fits data from hybridization experiments\n"
	   "and adjust the number of bases between two oligos\n"
	   "to minimize \\chi^2 of a data set\n"
	   "Y should be the hybridization position (in nm)\n"
	   "and X the expected oligo (in bp) with 0 repeats\n"
	   "place fit(s) in new plot->%%b Treat all datasets->%%b in the same plot %%b\n"
	   "Draw fitted data%%R or error signal %%r\n"
	   "You have %d oligo positions\n"
	   "Specify where the repeats are located\n",nf);
  if (nf <= 10)
    {
      for(k = 0, i = 0; k < nf-1; k++)
	{
	  if (i == 0)    snprintf(question+strlen(question),sizeof(question)-strlen(question),"%g < %%R <",dsi->xd[k]);
	  else	     snprintf(question+strlen(question),sizeof(question)-strlen(question),"%g < %%r ",dsi->xd[k]);
	  if (i%5 == 4) snprintf(question+strlen(question),sizeof(question)-strlen(question),"\n");
	  i++;
	}
      snprintf(question+strlen(question),sizeof(question)-strlen(question)," < %g\n",dsi->xd[nf-1]);
    }
  else snprintf(question+strlen(question),sizeof(question)-strlen(question),"in point index 0 < %%8d < %d\n",nf-1);
  snprintf(question+strlen(question),sizeof(question)-strlen(question),"Specify the range in bps of the repeats\n"
	   "[%%8d, %%8d] \n");
  if (dsi->ye == NULL)
    {
      snprintf(question+strlen(question),sizeof(question)-strlen(question),"Specify the error in Y %%8f\n");
      i = win_scanf(question,&newplotfit,&allds,&same,&error,&loc,&rmin,&rmax,&c_error);
    }
  else
    {
      i = win_scanf(question,&newplotfit,&allds,&same,&error,&loc,&rmin,&rmax);
    }
  if (i == CANCEL)	return OFF;


  if (op->n_dat < 2) allds = 0;
  if (allds)
    {
      opc = create_and_attach_one_plot(pr, op->n_dat, op->n_dat, 0);
      dsc = opc->dat[0];
      dsN = create_and_attach_one_ds(opc, op->n_dat, op->n_dat, 0);
      dsa = create_and_attach_one_ds(opc, op->n_dat, op->n_dat, 0);
      dsb = create_and_attach_one_ds(opc, op->n_dat, op->n_dat, 0);
      set_plot_title(opc,"Quality versus repeats");
      set_plot_y_title(opc, "\\chi^2, N, a, b");			
      set_plot_x_title(opc, "dataset index");			

      oper = create_and_attach_one_plot(pr, op->n_dat, op->n_dat, 0);
      dser = oper->dat[0];
      for (i = 1; i < nf; i++)
	dser = create_and_attach_one_ds(oper, op->n_dat, op->n_dat, 0);
      set_plot_title(oper,"Quality");
      set_plot_y_title(oper, "Error");			
      set_plot_x_title(oper, "dataset index");			
    }
  else dsc = NULL;

  sds = (allds == 0) ? op->cur_dat : 0;
  nds = (allds == 0) ? op->cur_dat + 1 : op->n_dat;
  for (ids = sds; ids < nds; ids++)
    { 
      dsi = op->dat[ids];
      if (newplotfit == 0)
	{
	  opn = op;
	  ds = create_and_attach_one_ds(opn, nf, nf, 0);
	}
      else if (ids > sds && same)
	{
	  ds = create_and_attach_one_ds(opn, nf, nf, 0);
	}
      else 
	{
	  opn = create_and_attach_one_plot(pr, nf, nf, 0);
	  ds = opn->dat[0];
	  set_plot_title(opn,"Fit of ds %d",ids);
	  set_plot_y_title(opn, "Position");			
	  set_plot_x_title(opn, "bp");			
	}
      if (ds == NULL) return win_printf_OK("cannot create plot !");

      for (k = rmin; k <= rmax; k++)
	{
	  for (j = 0; j < nf; j++)
	    {
	      ds->yd[j] = dsi->yd[j];
	      ds->xd[j] = (j <= loc) ? dsi->xd[j] : dsi->xd[j] + k;
	    }
	  i = least_square_fit_on_ds2(ds, 0, c_error, &ar, &br, &chi2r);
	  if (i < 0) return win_printf("least square fit error!");
	  if ((k == rmin) || (chi2r < best_chi2))
	    {
	      best_k = k;
	      best_a = ar;
	      best_b = br;
	      best_chi2 = chi2r;
	    }
	}
      for (j = 0; j < nf; j++)
	{
	  ds->yd[j] = dsi->yd[j];
	  ds->xd[j] = (j <= loc) ? dsi->xd[j] : dsi->xd[j] + best_k;
	  if (oper != NULL)
	    {
	      dser = oper->dat[j];
	      dser->xd[ids] = ids;
	      dser->yd[ids] = ds->yd[j] - best_a * ds->xd[j] + best_b;;
	    }
	}
  /* now we must do some house keeping */
      inherit_from_ds_to_ds(ds, dsi);
      if (error == 0)
	{
	  set_ds_treatement(ds,"Best repeats %d",best_k);
      
	  if ((ds = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
	    return win_printf_OK("cannot create plot !");
	  
	  for (i=0; i< nf ; i++)
	    {      
	      ds->xd[i] = (i <= loc) ? dsi->xd[i] : dsi->xd[i] + best_k;
	      ds->yd[i] = best_a * ds->xd[i] + best_b;
	    }
      
	  inherit_from_ds_to_ds(ds,dsi);
	  set_ds_treatement(ds,"%d bases added - least square fit y=ax+b, a = %g and b = %g "
			"\\chi^2 = %g, n = %d", best_k, best_a, best_b, best_chi2, nf-2);
	  sprintf(c,"\\fbox{\\stack{{%d bases added}{\\sl y = a x + b}{a = %g}{b = %g}{\\chi^2 = %g}{n = %d}}}"
		  ,best_k,best_a,best_b,best_chi2,nf-2);
	  set_ds_plot_label(ds, (ds->xd[nf-1]+ds->xd[nf-1])/2, best_a*(ds->xd[nf-1]+ds->xd[nf-1])/2 + best_b, USR_COORD, "%s", c);
	}
      else
	{
	  for (i=0; i< nf ; i++)
	    {      
	      ds->yd[i] -= best_a * ds->xd[i] + best_b;
	    }
      
	  set_ds_treatement(ds,"%d bases added - least square fit y=ax+b, a = %g and b = %g "
			"\\chi^2 = %g, n = %d", best_k, best_a, best_b, best_chi2, nf-2);
	  sprintf(c,"\\fbox{\\stack{{%d bases added}{\\sl y = a x + b}{a = %g}{b = %g}{\\chi^2 = %g}{n = %d}}}"
		  ,best_k,best_a,best_b,best_chi2,nf-2);
	  set_ds_plot_label(ds, (ds->xd[nf-1]+ds->xd[nf-1])/2, best_a*(ds->xd[nf-1]+ds->xd[nf-1])/2 + best_b, USR_COORD, "%s", c);

	}
      if (dsc != NULL)
	{
	  dsc->xd[ids] = dsN->xd[ids] = dsa->xd[ids] = dsb->xd[ids] = ids;
	  dsN->yd[ids] = best_k;
	  dsc->yd[ids] = best_chi2;
	  dsa->yd[ids] = best_a;
	  dsb->yd[ids] = best_b;
	}
    }
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}


int mean_and_sigma_on_array(float *yd, int nx, float *meany, float *sig)
{
  int i;
  double tmp, y2, mean;
  
  for (i=0, mean = 0; i< nx ; i++)     mean += yd[i];
  if (nx < 1) return 1;
  mean /= nx;
  for (i=0 ,y2 = 0; i< nx ; i++)
    {
      tmp = yd[i] - mean;
      y2 += tmp * tmp;
    }
  if (nx > 1) y2 /= (nx-1);
  if (meany != NULL)	*meany = (float)mean;
  if (sig != NULL)	*sig = (float)sqrt(y2);
  return 0;
}


int do_FindRepeats_in_data_set_2(void)
{
  register int i, j, k;
  int nf, ids, sds, nds, idsr, base_index_nx = 0;
  static float c_error = 3, maxchi2 = 8.5, min_a = 0.97, max_a = 1.045, zero_shift = 10;
  pltreg *pr = NULL;
  O_p *op = NULL, *opn = NULL, *opc = NULL, *oper = NULL, *oper2 = NULL;
  d_s *ds = NULL, *dsi = NULL, *dsc = NULL, *dsN = NULL, *dsa = NULL, *dsb = NULL, *dser = NULL, *dser2 = NULL;
  char question[4096], c[512];
  static int loc = 0, allds = 0, newplotfit = 0, error = 0, same = 0, filteronchi2 = 0;
  static int filterona = 0;
  int best_k, bases_added = 0, jloc;
  float tmp, meany = 0, sig = 0, base_index[256], loc_x_val;
  double best_a = 1, ar = 1;
  double best_b = 0, br = 0;
  double best_chi2 = 0, chi2r = 0, hfchi2r = 0;
  //int QuickSort(float *a, int l, int r);

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine fits data from hybridization experiments\n"
			 "and adjust the number of bases between two oligos\n"
			 "to minimize \\chi^2 of a data set");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */

  for (ids = 0; ids < op->n_dat; ids++)
    {
      dsi = op->dat[ids];
      for (j = 0; j < dsi->nx; j++)
	{
	  for (k = 0; k < base_index_nx; k++)
	    if (dsi->xd[j] == base_index[k]) break;
	  if (k >= base_index_nx) 
	    {
	      base_index[base_index_nx] = dsi->xd[j];
	      if (base_index_nx < 256) base_index_nx++;
	      else return win_printf("You have too many oligo position > %u",base_index_nx);
	    }
	}
    }
  QuickSort(base_index, 0, base_index_nx-1);



  
  snprintf(question,sizeof(question), "This routine fits data from hybridization experiments\n"
	   "It fits a line first excluding repeats, compute the scaling factor\n" 
	   "to minimize \\chi^2 of a data set\n"
	   "and then adjust the number of bases between two oligos\n"
	   "Y should be the hybridization position (in nm)\n"
	   "and X the expected oligo (in bp) with 0 repeats\n"
	   "place fit(s) in new plot->%%b Treat all datasets->%%b in the same plot %%b\n"
	   "Draw fitted data%%R or error signal %%r (in that case shift ds by %%6f)\n"
	   "if select %%b disregard data with \\chi^2 > %%6f\n"
	   "if select %%b disregard data with a outside [%%6f, %%6f] \n"
	   "You have %d oligo positions\n"
	   "Specify where the repeats are located\n"
	   ,base_index_nx);
  if (base_index_nx <= 24)
    {
      for(k = 0, i = 0; k < base_index_nx-1; k++)
	{
	  if (i == 0)    snprintf(question+strlen(question),sizeof(question)-strlen(question),"%g < %%R <",base_index[k]);
	  else	     snprintf(question+strlen(question),sizeof(question)-strlen(question),"%g < %%r ",base_index[k]);
	  if (i%5 == 4) snprintf(question+strlen(question),sizeof(question)-strlen(question),"\n");
	  i++;
	}
      snprintf(question+strlen(question),sizeof(question)-strlen(question)," < %g or No repeats %%r\n",base_index[base_index_nx-1]);
    }
  else snprintf(question+strlen(question),sizeof(question)-strlen(question),"in point index 0 < %%8d < %d (if == no repeats are fitted)\n",nf-1);
  if (dsi->ye == NULL)
    {
      snprintf(question+strlen(question),sizeof(question)-strlen(question),"Specify the error in Y %%8f\n");
      i = win_scanf(question,&newplotfit,&allds,&same,&error,&zero_shift,&filteronchi2,&maxchi2
		    ,&filterona,&min_a,&max_a,&loc,&c_error);
    }
  else
    {
      i = win_scanf(question,&newplotfit,&allds,&same,&error,&zero_shift,&filteronchi2,&maxchi2
		    ,&filterona,&min_a,&max_a,&loc);
    }
  if (i == WIN_CANCEL)	return OFF;

  loc_x_val = (loc >= base_index_nx - 1) ? (base_index[base_index_nx-1] + 100000) : base_index[loc];

  if (op->n_dat < 2) allds = 0;
  sds = (allds == 0) ? op->cur_dat : 0;
  nds = (allds == 0) ? op->cur_dat + 1 : op->n_dat;
  
  if (allds)
    {
      opc = create_and_attach_one_plot(pr, op->n_dat, op->n_dat, 0);
      dsc = opc->dat[0];
      dsN = create_and_attach_one_ds(opc, op->n_dat, op->n_dat, 0);
      dsa = create_and_attach_one_ds(opc, op->n_dat, op->n_dat, 0);
      dsb = create_and_attach_one_ds(opc, op->n_dat, op->n_dat, 0);
      set_plot_title(opc,"Quality versus repeats");
      set_plot_y_title(opc, "\\chi^2, N, a, b");			
      set_plot_x_title(opc, "dataset index");			

      oper = create_and_attach_one_plot(pr, op->n_dat, op->n_dat, 0);
      dser = oper->dat[0];
      dser->nx = dser->ny = 0;
      for (i = 1; i < base_index_nx; i++)
	{
	  dser = create_and_attach_one_ds(oper, op->n_dat, op->n_dat, 0);
	  dser->nx = dser->ny = 0;
	  set_ds_point_symbol(dser, "\\pt5\\oc");
	}
      set_plot_title(oper,"Quality");
      set_plot_y_title(oper, "Deduced base position");			
      set_plot_x_title(oper, "dataset index");			
      if (loc < base_index_nx - 1)
	{
	  oper2 = create_and_attach_one_plot(pr, op->n_dat, op->n_dat, 0);
	  dser2 = oper2->dat[0];
	  dser2->nx = dser2->ny = 0;
	  for (i = 1; i < base_index_nx; i++)
	    {
	      dser2 = create_and_attach_one_ds(oper2, op->n_dat, op->n_dat, 0);
	      dser2->nx = dser2->ny = 0;
	      set_ds_point_symbol(dser2, "\\pt5\\oc");
	    }
	  set_plot_title(oper2,"Quality");
	  set_plot_y_title(oper2, "Deduced base position - repeats");			
	  set_plot_x_title(oper2, "dataset index");			
	}
    }
  else dsc = NULL;

  for (ids = sds, idsr = 0; ids < nds; ids++)
    { 
      dsi = op->dat[ids];
      if (newplotfit == 0)
	{
	  opn = op;
	  ds = create_and_attach_one_ds(opn, dsi->nx, dsi->nx, 0);
	}
      else if (ids > sds && same)
	{
	  ds = create_and_attach_one_ds(opn, dsi->nx, dsi->nx, 0);
	}
      else 
	{
	  opn = create_and_attach_one_plot(pr, dsi->nx, dsi->nx, 0);
	  ds = opn->dat[0];
	  set_plot_title(opn,"Fit of ds %d",ids);
	  set_plot_y_title(opn, "Position");			
	  set_plot_x_title(opn, "bp");			
	}
      if (ds == NULL) return win_printf_OK("cannot create plot !");

      if (ds->ye == NULL) ds = alloc_data_set_y_error(ds);

      for (j = 0, jloc = 0; j < dsi->nx; j++)
	jloc = (dsi->xd[j] <= loc_x_val) ? j : jloc;
      for (j = 0; j < dsi->nx - 1; j++)
	{
	  if (j <= jloc)
	    {
	      ds->yd[j] = dsi->yd[j];
	      tmp = (dsi->ye != NULL) ? dsi->ye[j] : c_error;
	      if (ds->ye != NULL) ds->ye[j] = tmp;
	      ds->xd[j] = dsi->xd[j];
	    }
	  else
	    {
	      ds->yd[j] = dsi->yd[j+1] - (dsi->yd[jloc+1] - dsi->yd[jloc]);
	      tmp = (dsi->ye != NULL) ? dsi->ye[j+1] : c_error;
	      if (ds->ye != NULL) ds->ye[j] = tmp;
	      ds->xd[j] = dsi->xd[j+1] - (dsi->xd[jloc+1] - dsi->xd[jloc]);
	    }
	}
      ds->nx = ds->ny = dsi->nx - 1;
      i = least_square_fit_on_ds2(ds, 0, c_error, &ar, &br, &chi2r);
      if (i < 0) return win_printf("least square fit error!");
      find_HF_chi2on_ds2(ds, c_error, ar, br, &hfchi2r);
      if (jloc < dsi->nx - 1)
	{
	  best_k = (ar > 0) ? (int)(0.5+(dsi->yd[jloc+1] - dsi->yd[jloc])/ar) : 0;
	  bases_added = best_k - dsi->xd[jloc+1] + dsi->xd[jloc];
	}
      else
	{
	  best_k = 0;
	  bases_added = 0;
	}
      ds->nx = ds->ny = dsi->nx;
      for (j = 0; j < dsi->nx; j++)
	{
	  ds->yd[j] = dsi->yd[j];
	  tmp = (dsi->ye != NULL) ? dsi->ye[j] : c_error;
	  if (ds->ye != NULL) ds->ye[j] = tmp;
	  ds->xd[j] = (j <= jloc) ? dsi->xd[j] : dsi->xd[j] + bases_added;
	}
      i = least_square_fit_on_ds2(ds, 0, c_error, &ar, &br, &chi2r);
      if (i < 0) return win_printf("least square fit error!");
      best_a = ar;
      best_b = br;
      best_chi2 = chi2r;
      find_HF_chi2on_ds2(ds, c_error, best_a, best_b, &hfchi2r);
      for (j = 0; j < dsi->nx; j++)
	{
	  ds->yd[j] = dsi->yd[j];
	  ds->xd[j] = (j <= jloc) ? dsi->xd[j] : dsi->xd[j] + bases_added;
	  if ((best_chi2 < maxchi2 || filteronchi2 == 0) 
	      && ((best_a > min_a && best_a < max_a) || filteronchi2 == 0)) 
	    {

	      for (k = 0; k < base_index_nx; k++)
		if (dsi->xd[j] == base_index[k]) break;
	      if (oper != NULL && k < base_index_nx)
		{
		  dser = oper->dat[k];
		  add_new_point_to_ds(dser, ids, (ds->yd[j] - best_b)/best_a);
		}
	      if (oper2 != NULL)
		{
		  dser2 = oper2->dat[k];
		  tmp = (ds->yd[j] - best_b)/best_a;
		  tmp -= (j <= jloc) ? 0 : bases_added;
		  add_new_point_to_ds(dser2, ids, tmp);
		}
	    }
	}
      inherit_from_ds_to_ds(ds, dsi);
      set_ds_point_symbol(ds, "\\pt5\\di");
      if (error == 0)  // we draw data
	{
	  set_ds_treatement(ds,"Best repeats %d",best_k);
      
	  if ((ds = create_and_attach_one_ds(opn, dsi->nx, dsi->nx, 0)) == NULL)
	    return win_printf_OK("cannot create plot !");
	  
	  for (i=0; i< dsi->nx ; i++)
	    {      
	      ds->xd[i] = (i <= jloc) ? dsi->xd[i] : dsi->xd[i] + bases_added;
	      ds->yd[i] = best_a * ds->xd[i] + best_b;
	    }
      
	  inherit_from_ds_to_ds(ds,dsi);
	  set_ds_treatement(ds,"%d bases added - least square fit y=ax+b, a = %g and b = %g "
			"\\chi^2 = %g, n = %d", best_k, best_a, best_b, best_chi2, dsi->nx-2);
	  sprintf(c,"\\fbox{\\stack{{%d bases added}{\\sl y = a x + b}{a = %g}{b = %g}{\\chi^2 = %g}{n = %d}{\\chi_{HF}^2 = %g}}}"
		  ,bases_added,best_a,best_b,best_chi2,dsi->nx-2,hfchi2r);
	  set_ds_plot_label(ds, (ds->xd[dsi->nx-1]+ds->xd[dsi->nx-1])/2, best_a*(ds->xd[dsi->nx-1]+ds->xd[dsi->nx-1])/2 + best_b, USR_COORD, "%s", c);
	}
      else // we draw error (data - fit)
	{
	  for (i=0; i< dsi->nx ; i++)
	    {      
	      ds->yd[i] -= best_a * ds->xd[i] + best_b;
	      ds->yd[i] += ids * zero_shift * same;
	    }
      
	  set_ds_treatement(ds,"%d bases added - least square fit y=ax+b, a = %g and b = %g "
			"\\chi^2 = %g, n = %d", best_k, best_a, best_b, best_chi2, dsi->nx-2);
	  sprintf(c,"\\fbox{\\stack{{%d bases added}{\\sl y = a x + b}{a = %g}{b = %g}{\\chi^2 = %g}{n = %d}{\\chi_{HF}^2 = %g}}}"
		  ,bases_added,best_a,best_b,best_chi2,dsi->nx-2,hfchi2r);
	  set_ds_plot_label(ds, (ds->xd[dsi->nx-1]+ds->xd[dsi->nx-1])/2, ids * zero_shift * same, USR_COORD, "%s", c);

	}
      if ((best_chi2 < maxchi2 || filteronchi2 == 0) 
	  && ((best_a > min_a && best_a < max_a) || filteronchi2 == 0)) 
	{
	  if (dsc != NULL)
	    {
	      dsc->xd[idsr] = dsN->xd[idsr] = dsa->xd[idsr] = dsb->xd[idsr] = ids;
	      dsN->yd[idsr] = bases_added;
	      dsc->yd[idsr] = best_chi2;
	      dsa->yd[idsr] = best_a;
	      dsb->yd[idsr] = best_b;
	      idsr++;
	    }
	}
    }

  for (j = 0; j < base_index_nx; j++)
    {
      if (oper != NULL)
	{
	  dser = oper->dat[j];
	  if (mean_and_sigma_on_array(dser->yd, dser->nx, &meany, &sig) == 0)
	    {
	      sprintf(c,"\\pt8 %7.2f -> <y> %7.2f bp \\sigma  = %7.2f on %d pts.",base_index[j], meany, sig,dser->nx);
	      set_ds_plot_label(dser, dser->xd[0], base_index[j]+10, USR_COORD, "%s", c);
	    }

	}
      if (oper2 != NULL)
	{
	  dser2 = oper2->dat[j];
	  if (mean_and_sigma_on_array(dser2->yd, dser2->nx, &meany, &sig) == 0)
	    {
	      sprintf(c,"\\pt8 %7.2f -> <y> = %7.2f bp \\sigma  = %7.2f on %d pts.",base_index[j], meany, sig, dser2->nx);
	      set_ds_plot_label(dser2, dser2->xd[0], base_index[j]+10, USR_COORD, "%s", "%s", c);
	    }
	}
    }
  




  if (dsc != NULL) dsc->nx = dsc->ny = idsr;
  if (dsN != NULL) dsN->nx = dsN->ny = idsr;
  if (dsa != NULL) dsa->nx = dsa->ny = idsr;
  if (dsb != NULL) dsb->nx = dsb->ny = idsr;
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int use_invisible_points_of_one_ds_to_remove_points_of_other_ds_with_same_x(void)
{
  int i,  k, jj, j;
  int  n_out;
  pltreg *pr;
  d_s *ds, *dsi;
  O_p *op;
  
  if (updating_menu_state != 0)	return D_O_K;		
  
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return D_O_K;					


  for (i = dsi->nx - 1; i >= 0; i--)
    {
      // INVISIBLE
      n_out = (dsi->xd[i] >= op->x_hi || dsi->xd[i] < op->x_lo  
	       || dsi->yd[i] >= op->y_hi || dsi->yd[i] < op->y_lo ) ? 1 : 0;
      if (n_out)
	{
	  for (k = 0; k < op->n_dat; k++)
	    {
	      if (k == op->cur_dat) continue;
	      ds = op->dat[k]; // we work on dataset k
	      for (j = ds->nx - 1; j >= 0; j--)
		{
		  if (ds->xd[j] == dsi->xd[i])
		    {
		      for (jj = j; jj < ds->nx-1; jj++)
			{
			  ds->xd[jj] = ds->xd[jj+1];
			  ds->yd[jj] = ds->yd[jj+1];
			  if (ds->xe) ds->xe[jj] = ds->xe[jj+1];
			  if (ds->ye) ds->ye[jj] = ds->ye[jj+1];
			}
		      ds->nx -= (ds->nx > 0) ? 1 : 0;
		    }
		}
	    }
	  for (jj = i; jj < dsi->nx-1; jj++)
	    {
	      dsi->xd[jj] = dsi->xd[jj+1];
	      dsi->yd[jj] = dsi->yd[jj+1];
	      if (dsi->xe) dsi->xe[jj] = dsi->xe[jj+1];
	      if (dsi->ye) dsi->ye[jj] = dsi->ye[jj+1];
	    }
	  dsi->nx -= (dsi->nx > 0) ? 1 : 0;
	}
    }
    op->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);	
}


int use_visible_points_of_one_ds_to_remove_points_of_other_ds_with_same_x(void)
{
  int i,  k, jj, j;
  int  n_out;
  pltreg *pr;
  d_s *ds, *dsi;
  O_p *op;
  
  if (updating_menu_state != 0)	return D_O_K;		
  
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return D_O_K;					


  for (i = dsi->nx - 1; i >= 0; i--)
    {
      // INVISIBLE
      n_out = (dsi->xd[i] < op->x_hi && dsi->xd[i] >= op->x_lo  
	       && dsi->yd[i] < op->y_hi && dsi->yd[i] >= op->y_lo ) ? 1 : 0;
      if (n_out)
	{
	  for (k = 0; k < op->n_dat; k++)
	    {
	      if (k == op->cur_dat) continue;
	      ds = op->dat[k]; // we work on dataset k
	      for (j = ds->nx - 1; j >= 0; j--)
		{
		  if (ds->xd[j] == dsi->xd[i])
		    {
		      for (jj = j; jj < ds->nx-1; jj++)
			{
			  ds->xd[jj] = ds->xd[jj+1];
			  ds->yd[jj] = ds->yd[jj+1];
			  if (ds->xe) ds->xe[jj] = ds->xe[jj+1];
			  if (ds->ye) ds->ye[jj] = ds->ye[jj+1];
			}
		      ds->nx -= (ds->nx > 0) ? 1 : 0;
		    }
		}
	    }
	  for (jj = i; jj < dsi->nx-1; jj++)
	    {
	      dsi->xd[jj] = dsi->xd[jj+1];
	      dsi->yd[jj] = dsi->yd[jj+1];
	      if (dsi->xe) dsi->xe[jj] = dsi->xe[jj+1];
	      if (dsi->ye) dsi->ye[jj] = dsi->ye[jj+1];
	    }
	  dsi->nx -= (dsi->nx > 0) ? 1 : 0;
	}
    }
    op->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);	
}



MENU *FindRepeats_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Find repeats nb", do_FindRepeats_in_data_set,NULL,0,NULL);
  add_item_to_menu(mn,"Find repeats nb 2", do_FindRepeats_in_data_set_2,NULL,0,NULL);
  add_item_to_menu(mn,"Rm points of all ds same x that invisible points", 
		   use_invisible_points_of_one_ds_to_remove_points_of_other_ds_with_same_x,NULL,0,NULL);
  add_item_to_menu(mn,"Rm points of all ds same x that visible points", 
		   use_visible_points_of_one_ds_to_remove_points_of_other_ds_with_same_x,NULL,0,NULL);




  return mn;
}

int	FindRepeats_main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  add_plot_treat_menu_item ( "FindRepeats", NULL, FindRepeats_plot_menu(), 0, NULL);
  return D_O_K;
}

int	FindRepeats_unload(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  remove_item_to_menu(plot_treat_menu, "FindRepeats", NULL, NULL);
  return D_O_K;
}
#endif

