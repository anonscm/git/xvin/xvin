/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _DUFFIN_C_
#define _DUFFIN_C_

#include <allegro.h>
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"


/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "Duffin.h"




# define  	IS_DUFFIN_DATA    3266



typedef struct _Duffin
{
  int 	  niter;	// nb of time iteration 			
  int 	  i_iter;	// iteration 			
  int     n_asked;      // nb of asked point
  int 	  niter_npc;	// niter%npc (phase in omega) 		      
  double  h;	        // the step
  double  hpi;	        // the step * 2 pi
  double  x;            // the position in x	
  double  y;	        // the velocity	in x	
  double  v;            // the phse of the rotating field
  double  x_1;          // the position in x of n-1	
  double  y_1;	        // the velocity	in x of n-1		
  double  v_1;          // the phse of the rotating field of n-1	
  double  x0;           // the position in x0	
  double  y0;	        // the velocity	in x0	
  double  gamma1;	// dissipation factor
  double  pot;	        // potential strength
  int	  npc;		// nb de point pour faire 1 periode 
  double  P;		// B1  amplitude	
  int	  nph;		// phase of poincare [0, npc[		
  int n_turn;
  int buf_size;
  float *xt;
  float *yt;
  float *vt;
  int     live;         // continuous refresh  
} Duffin;

int 		oscillate = 0;

# define ONCE 		1
# define PERMANENT 	2


Duffin 		*find_Duffin(O_p *op);
Duffin 		*find_Duffin_in_ds(d_s *ds);
Duffin 		*create_Duffin( double x0, double y0, double pot, double v0, double P, double gamma1, int npc, int n_turn);


Duffin *find_Duffin(O_p *op)
{
    if (op == NULL)				return NULL;
    if (op->dat[0]->use.to != IS_DUFFIN_DATA)	return NULL;
    return ((Duffin*)op->dat[0]->use.stuff);
}
Duffin *find_Duffin_in_ds(d_s *ds)
{
	if (ds == NULL)				return NULL;
	if (ds->use.to != IS_DUFFIN_DATA)	return NULL;
	return ((Duffin*)ds->use.stuff);
}


Duffin *find_Duffin_in_pr(pltreg *pr)
{
    if (pr == NULL)				return NULL;
    if (pr->use.to != IS_DUFFIN_DATA)	return NULL;
    return (Duffin*)pr->use.stuff;
}

Duffin *create_Duffin( double x0, double y0, double pot, double v0, double P, double gamma1, int npc, int n_turn)
{
  Duffin *c;
  
  if (npc == 0) return NULL;
  c = (Duffin *)calloc(1,sizeof(Duffin));
  if (c == NULL)		return NULL;
  c->niter = c->niter_npc = 0;		
  c->npc = npc;		
  c->h = (double)1/c->npc;
  c->hpi = M_PI * 2 * c->h;
  c->x0 = c->x_1 = c->x = x0;			
  c->y0 = c->y_1 = c->y = y0;			
  c->v_1 = c->v = v0;			
  c->P =  P;
  c->pot =  pot;  
  c->gamma1 = gamma1;
  c->n_turn = n_turn;
  c->buf_size = npc*n_turn;
  c->i_iter = 0;
  c->xt = (float*)calloc(c->buf_size, sizeof(float));
  if (c->xt == NULL) return NULL;
  c->yt = (float*)calloc(c->buf_size, sizeof(float));
  if (c->yt == NULL) return NULL;
  c->vt = (float*)calloc(c->buf_size, sizeof(float));
  if (c->vt == NULL) return NULL;
  c->live = 1;
  return c;
}



int do_simulate(Duffin *bo)
{
  double k1x,k2x,k3x,k4x,k1y,k2y,k3y,k4y;
  double k1v, k2v, k3v, k4v;


  if (bo == NULL) return -1;
  if (bo->live == 0) return 0;
  bo->h = (double)1/bo->npc;
  bo->hpi = M_PI * 2 * bo->h;
  bo->x_1 = bo->x;
  bo->y_1 = bo->y;
  bo->v_1 = bo->v;
  if (bo->niter_npc >= bo->npc)
    {
      bo->niter_npc = 0;
      bo->v = 0;	
    }
  k1v = bo->hpi;
  k1x = bo->h * bo->y;
  k1y = -bo->h * bo->pot * ( -bo->x + bo->x*bo->x*bo->x + bo->P * sin(bo->x-bo->v) - bo->gamma1 *bo->y);
  
  k2v = bo->hpi;
  k2x = bo->h * (bo->y+k1y/2);
  k2y = -bo->h * bo->pot * ( -(bo->x+k1x/2) +(bo->x+k1x/2)*(bo->x+k1x/2)*(bo->x+k1x/2)+ bo->P * sin ((bo->x+k1x/2) - (bo->v+k1v/2))- bo->gamma1 *(bo->y+k1y/2));
  
  k3v = bo->hpi;
  k3x = bo->h * (bo->y+k2y/2);
  k3y = -bo->h * bo->pot * ( -(bo->x+k2x/2) + (bo->x+k2x/2)*(bo->x+k2x/2)*(bo->x+k2x/2)+ bo->P * sin ((bo->x+k2x/2) - (bo->v+k2v/2)) - bo->gamma1 *(bo->y+k2y/2));
  
  k4v = bo->hpi;			
  k4x = bo->h * (bo->y+k3y);
  k4y = -bo->h * bo->pot * ( -(bo->x+k3x) + (bo->x+k3x)*(bo->x+k3x)*(bo->x+k3x)+ bo->P * sin ((bo->x+k3x) - (bo->v+k3v)) - bo->gamma1 *(bo->y+k3y));
  
  bo->x = bo->x + ((k1x+2*k2x+2*k3x+k4x)/6);
  bo->y = bo->y + ((k1y+2*k2y+2*k3y+k4y)/6);
  bo->v = bo->v + ((k1v+2*k2v+2*k3v+k4v)/6);
  bo->niter_npc++;
  bo->niter++;
  if (bo->niter_npc >= bo->npc)
    {
      bo->niter_npc = 0;
      bo->v = 0;	
    }
  while (bo->i_iter >= bo->buf_size) bo->i_iter -= bo->buf_size; 
  bo->xt[bo->i_iter] = bo->x;
  bo->yt[bo->i_iter] = bo->y;
  bo->vt[bo->i_iter++] = bo->v;
  return 0;
}

int sim_traject_ds(Duffin *bou, d_s *ds, int niter)
{
    register int j;

    if (bou == NULL || ds == NULL)return 1;
    for (j = 0; j < niter; j++)
	{
	    do_simulate(bou);
	    if (bou->v < bou->v_1)
	      {
		add_new_point_to_ds(ds, bou->x_1, bou->y_1);
		add_new_point_to_ds(ds, bou->x, bou->y);
	      }
	    else
	      {
		add_new_point_to_ds(ds, bou->x_1, bou->y_1);
		add_new_point_to_ds(ds, bou->x, bou->y);
	      }
	}
    return 0;
}


int bou_trajectories_op_idle_action(O_p *op, DIALOG *d)
{
  int x_m, y_m, dx = 0, dy = 0, niter, ok = 0, j;//, dxp = 0, dyp = 0;
    pltreg *pr;
    d_s *ds;
    Duffin *bou = NULL;
    float x, y;
    
    (void)d;
    if(updating_menu_state != 0)	return D_O_K;		
    if ((pr = find_pr_in_current_dialog(NULL)) == NULL)		
	return win_printf_OK("cannot find pr");	
    bou = find_Duffin_in_pr(pr);
    if (bou == NULL)  	return win_printf_OK("cannot find Duffin");
    x_m = mouse_x;
    y_m = mouse_y;
  display_title_message("x_m %d ym %d",x_m,y_m);
  
  while (mouse_b & 0x3)
    {
      ok = mouse_b & 0x3;
      dx = mouse_x - x_m;
      dy = mouse_y - y_m;
      display_title_message("x0 = %g y0 %g button %d", x_pr_2_pltdata(pr,mouse_x),y_pr_2_pltdata(pr,mouse_y),mouse_b & 3);	
    }

   if (ok) 
    {
      x = x_pr_2_pltdata(pr,x_m + dx);
      y = y_pr_2_pltdata(pr,y_m + dy);
      if ((x - op->x_lo) < (op->x_hi - op->x_lo)/100) return 0;
      if ((op->x_hi - x) < (op->x_hi - op->x_lo)/100) return 0;
      if ((y - op->y_lo) < (op->y_hi - op->y_lo)/100) return 0;
      if ((op->y_hi - y) < (op->y_hi - op->y_lo)/100) return 0;
      bou->x0 = bou->x = x_pr_2_pltdata(pr,x_m + dx);
      bou->y0 = bou->y = y_pr_2_pltdata(pr,y_m + dy);
    }
    if (bou->live) 
    {
      niter = bou->buf_size;
      ds = op->dat[0];
      ds->nx = ds->ny = 0;
      for (j = 0; j < niter; j++)
	{
	  do_simulate(bou);
	  add_new_point_to_ds(ds, bou->x, bou->y);
	}
      op->need_to_refresh = 1;
    }
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);

  return 0;
}


int bou_x_de_t_op_idle_action(O_p *op, DIALOG *d)
{
  int niter, j;//, dxp = 0, dyp = 0;
    pltreg *pr;
    d_s *ds;
    Duffin *bou = NULL;
    
    (void)d;
    if(updating_menu_state != 0)	return D_O_K;		
    if ((pr = find_pr_in_current_dialog(NULL)) == NULL)		
	return win_printf_OK("cannot find pr");	
    bou = find_Duffin_in_pr(pr);
    if (bou == NULL)  	return win_printf_OK("cannot find Duffin");	
    niter = bou->buf_size;
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    for (j = 0; j < niter; j++)
      {
	do_simulate(bou);
	add_new_point_to_ds(ds, j, bou->x);
      }
    op->need_to_refresh = 1;    
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);

  return 0;
}

int bou_fft_op_idle_action(O_p *op, DIALOG *d)
{
  int niter, j;//, dxp = 0, dyp = 0;
    pltreg *pr;
    d_s *ds;
    Duffin *bou = NULL;
    
    (void)d;
    if(updating_menu_state != 0)	return D_O_K;		
    if ((pr = find_pr_in_current_dialog(NULL)) == NULL)		
	return win_printf_OK("cannot find pr");	
    bou = find_Duffin_in_pr(pr);
    if (bou == NULL)  	return win_printf_OK("cannot find Duffin");	
    niter = bou->buf_size;
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    for (j = 0; j < niter; j++)
      {
	do_simulate(bou);
	add_new_point_to_ds(ds, j, bou->x);
      }

    fftwindow(niter, ds->yd);
    realtr1(niter, ds->yd);
    fft(niter, ds->yd, 1);
    realtr2(niter, ds->yd, 1);
    spec_real (niter, ds->yd, ds->yd);
    ds->nx = ds->ny = 1 + (niter/2);


    op->need_to_refresh = 1;    
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);

  return 0;
}


int bou_poincare_op_idle_action(O_p *op, DIALOG *d)
{
    int j, niter;//, dxp = 0, dyp = 0;
    pltreg *pr;
    d_s *ds;
    Duffin *bou = NULL;

    (void)d;
    if(updating_menu_state != 0)	return D_O_K;		
    if ((pr = find_pr_in_current_dialog(NULL)) == NULL)		
	return win_printf_OK("cannot find pr");	
    bou = find_Duffin_in_pr(pr);
    if (bou == NULL)  	return win_printf_OK("cannot find Duffin");	
    niter = bou->n_turn;
    ds = op->dat[0];
    ds->nx = ds->ny = 0;
    for (j = 0; j < niter; )
      {
	do_simulate(bou);
	if (bou->niter_npc != bou->nph) continue;
	add_new_point_to_ds(ds, bou->x, bou->y);
	j++;
      }
    op->need_to_refresh = 1;    
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
  return 0;
}



int do_Duffin_trajectories_plot(void)
{
    int i;
    static int niter = 4096, npc = 32;
    O_p *op = NULL, *opn = NULL;
    d_s *ds = NULL;
    pltreg *pr = NULL;
    Duffin *bou = NULL;
    static float p = 0.1, gam = 0, x0 = 1.1, y0 = 0, pot = 1;

    if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
	{
	    return win_printf_OK("This routine multiply the y coordinate"
				 "of a data set by a number and place it in a new plot");
	}
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
	return win_printf_OK("cannot find data");
    
    bou = find_Duffin_in_pr(pr);
    
    
    if (bou == NULL)
	{
	    i = win_scanf("Create a Duffin in this plot region\n"
			  "Define P = %6f\n"
			  "Potential strength = %6f (10)\n"
			  "Viscous friction %6f (-0.2)\n"
			  "Number of iter during one turn %6d (128)\n"
			  "X0 %6f V0 %6f Nb. of turn %6d (1024)\n"
			  "SC avec cascade \\gamma = -0.2, pot = 10 P = 1.16693\n"
			  ,&p,&pot, &gam,&npc,&x0,&y0,&niter);
	    if (i == WIN_CANCEL) return 0;
	    bou = create_Duffin( x0, y0, pot, 0, p, gam, npc, niter);
	    if (bou == NULL) return win_printf_OK("cannot create boussol");
	    bou->n_asked = niter;
	    pr->use.to = IS_DUFFIN_DATA;
	    pr->use.stuff = (void*)bou;
	    if ((opn = create_and_attach_one_plot(pr, niter*npc, niter*npc, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	    ds = opn->dat[0];
	    ds->nx = ds->ny = 0;
	    set_ds_dash_line(ds);
	    set_plot_title(opn, "Duffin");
	    set_plot_x_title(opn, "x");
	    set_plot_y_title(opn, "\\partial x /\\partial t");
	    opn->user_id = 4096;
	    opn->op_idle_action = bou_trajectories_op_idle_action;

	    set_ds_source(ds,"Duffin trajectory (x,dx /dt) with P = %g, x0 = %g, v0 = %g"
			  ,bou->P,x0,y0);



	    if ((opn = create_and_attach_one_plot(pr, npc*niter, npc*niter, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	    ds = opn->dat[0];
	    ds->nx = ds->ny = 0;
	    set_plot_title(opn, "x(t)");
	    set_plot_x_title(opn, "x");
	    set_plot_y_title(opn, "t");
	    opn->op_idle_action = bou_x_de_t_op_idle_action;
	    opn->user_id = 4098;



	    if ((opn = create_and_attach_one_plot(pr, npc*niter, npc*niter, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	    ds = opn->dat[0];
	    ds->nx = ds->ny = 0;
	    set_plot_title(opn, "FFT");
	    set_plot_x_title(opn, "Power");
	    set_plot_y_title(opn, "frequence");
	    opn->op_idle_action = bou_fft_op_idle_action;
	    opn->user_id = 4099;




	    if ((opn = create_and_attach_one_plot(pr, niter, niter, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	    ds = opn->dat[0];
	    ds->nx = ds->ny = 0;
	    set_ds_dot_line(ds);
	    set_plot_title(opn, "Poincar� Duffin");
	    set_plot_x_title(opn, "x");
	    set_plot_y_title(opn, "\\partial x /\\partial t");
	    opn->op_idle_action = bou_poincare_op_idle_action;
	    opn->user_id = 4097;



	}
    else
	{
	    p = bou->P;
	    pot = bou->pot;
	    gam = bou->gamma1;
	    i = win_scanf("Add a data set to Duffin simulation\n"
			  "X0 %6f V0 %6f Nb. od iter %6d\n"
			  "Define P = %6f\n"
			  "Potential strength = %6f\n"
			  "Viscous friction %6f\n"
			  "SC avec cascade \\gamma = -0.2, pot = 10 P = 1.16693\n"	// 5 -> 1.82625		  
			  ,&x0,&y0,&niter,&p,&pot,&gam);
	    if (i == WIN_CANCEL) return 0;
	    if ((ds = create_and_attach_one_ds(op, niter, niter, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	    ds->nx = ds->ny = 0;
	    bou->x0 = bou->x = x0;
	    bou->y0 = bou->y = y0;
	    bou->P =  p;
	    bou->pot =  pot;
	    bou->gamma1 = gam;
	    
	    bou->niter = niter;
	    set_ds_dash_line(ds);
	    set_ds_source(ds,"Duffin trajectory (\\theta,d\\theta /dt) with P = %g, x0 = %g, v0 = %g"
			  ,bou->P,x0,y0);
	}
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}




int do_Duffin_poicare(void)
{
    int i, j;
    static int niter = 1024, npc = 32, nph = 0;
    O_p *op = NULL, *opn = NULL;
    d_s *ds = NULL;
    pltreg *pr = NULL;
    Duffin *bou = NULL;
    static float  p = 0.2, gam = 0, x0 = 0.1, y0 = 0, pot = 1;

    if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
	{
	    return win_printf_OK("This routine multiply the y coordinate"
				 "of a data set by a number and place it in a new plot");
	}
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
	return win_printf_OK("cannot find data");
    
    bou = find_Duffin_in_pr(pr);
    
    
    if (bou == NULL)
	{
	    i = win_scanf("Create a Duffin in this plot region\n"
			  "Define P = %6f\n"
			  "Potential strength = %6f\n"
			  "Viscous friction %6f\n"
			  "Number of iter during one turn %6d\n"
			  "X0 %6f V0 %6f Nb. od iter %6d\n"
			  "Poincare cut index %6d\n"
			  "SC avec cascade \\gamma = -0.2, pot = 10 P = 1.16693\n"
			  ,&p,&pot,&gam,&npc,&x0,&y0,&niter,&nph);
	    if (i == WIN_CANCEL) return 0;
	    bou = create_Duffin( x0, y0, pot, 0, p, gam, npc, niter);
	    if (bou == NULL) return win_printf_OK("cannot create boussol");
	    bou->nph = nph;
	    bou->niter = niter;
	    bou->n_asked = niter;
	    pr->use.to = IS_DUFFIN_DATA;
	    pr->use.stuff = (void*)bou;
	    if ((opn = create_and_attach_one_plot(pr, niter, niter, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	    ds = opn->dat[0];
	    ds->nx = ds->ny = 0;
	    set_ds_dot_line(ds);
	    set_plot_title(opn, "\\partial\\theta /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)");
	    set_plot_x_title(opn, "\\theta");
	    set_plot_y_title(opn, "\\partial\\theta /\\partial t");
	    opn->op_idle_action = bou_poincare_op_idle_action;
	    opn->user_id = 4097;
	}
    else
	{
	    i = win_scanf("Add a data set to Duffin simulation\n"
			  "X0 %6f V0 %6f Nb. od iter %6d\n"
			  "SC avec cascade \\gamma = -0.2, pot = 10 P = 1.16693\n"
			  ,&x0,&y0,&niter);
	    if (i == WIN_CANCEL) return 0;
	    if ((ds = create_and_attach_one_ds(op, niter, niter, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	    ds->nx = ds->ny = 0;
	    bou->x0 = bou->x = x0;
	    bou->y0 = bou->y = y0;
	    bou->niter = niter;
	    set_ds_dot_line(ds);
	}
    set_ds_source(ds,"P = %g",bou->P);


    for (j = 0; j < niter; )
	{
	    do_simulate(bou);
	    if (bou->niter_npc != bou->nph) continue;
	    add_new_point_to_ds(ds, bou->x, bou->y);
	    j++;
	}
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

int do_move_phase(void)
{
    pltreg *pr;
    O_p *op;
    d_s *ds;
    Duffin *bou = NULL;
    
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
	return D_O_K;		

    //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
    if(updating_menu_state != 0)	
	{
	    if (op != NULL && op->user_id >= 4096 && op->user_id < 4101)
		{
		    remove_short_cut_from_dialog(0, KEY_RIGHT);
		    add_keyboard_short_cut(0, KEY_RIGHT, 0, do_move_phase);
		}
	    return D_O_K;		
	}

    bou = find_Duffin_in_pr(pr);
    if (bou == NULL) return D_O_K;

    bou->nph++;
    if (bou->nph >= bou->npc) bou->nph = 0;
    ds->nx = ds->ny = 0;
    bou->x = bou->x0;
    bou->y = bou->y0;
    bou->v = 0;
    /*
    for (j = 0; j < niter; )
	{
	    do_simulate(bou);
	    if (bou->niter_npc != bou->nph) continue;
	    add_new_point_to_ds(ds, bou->x, bou->y);
	    j++;
	}
    */
    op->need_to_refresh = 1;    
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

int do_inc_S(void)
{
    pltreg *pr;
    O_p *op;
    d_s *ds;
    Duffin *bou = NULL;
    
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
	return D_O_K;		

    //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
    if(updating_menu_state != 0)	
	{
	    if (op != NULL && op->user_id >= 4096 && op->user_id < 4101)
		{
		    remove_short_cut_from_dialog(0, KEY_UP);
		    add_keyboard_short_cut(0, KEY_UP, 0, do_inc_S);
		}
	    return D_O_K;		
	}

    bou = find_Duffin_in_pr(pr);
    if (bou == NULL) return D_O_K;

    bou->P *= 1.0001;
    set_plot_title(op, "\\stack{{\\partial\\theta /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)}{\\pt8 P = %g}}",bou->P);

    /*
    if (op->user_id == 4097)
	{
	    niter = ds->nx;
	    ds->nx = ds->ny = 0;
	    for (j = 0; j < niter; )
		{
		    do_simulate(bou);
		    if (bou->niter_npc != bou->nph) continue;
		    add_new_point_to_ds(ds, bou->x, bou->y);
		    j++;
		}
	}
    else if (op->n_dat > 1)
	{
	    ds = op->dat[0];
	    dsp = op->dat[1];
	    niter = bou->n_asked;
	    ds->nx = ds->ny = 0;
	    dsp->nx = dsp->ny = 0;
	    sim_traject_ds(bou, ds, niter);
	}
    */
    op->need_to_refresh = 1;    
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}



int do_dec_S(void)
{
    pltreg *pr;
    O_p *op;
    d_s *ds;
    Duffin *bou = NULL;
    
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
	return D_O_K;		

    //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
    if(updating_menu_state != 0)	
	{
	    if (op != NULL && op->user_id >= 4096 && op->user_id < 4101)
		{
		    remove_short_cut_from_dialog(0, KEY_DOWN);
		    add_keyboard_short_cut(0, KEY_DOWN, 0, do_dec_S);
		}
	    return D_O_K;		
	}

    bou = find_Duffin_in_pr(pr);
    if (bou == NULL) return D_O_K;

    bou->P *= 0.9999;
    set_plot_title(op, "\\stack{{\\partial\\theta /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)}{\\pt8 P = %g}}",bou->P);

    /*
    if (op->user_id == 4097)
	{
	    niter = ds->nx;
	    ds->nx = ds->ny = 0;
	    for (j = 0; j < niter; )
		{
		    do_simulate(bou);
		    if (bou->niter_npc != bou->nph) continue;
		    add_new_point_to_ds(ds, bou->x, bou->y);
		    j++;
		}
	}
    else if (op->n_dat > 1)
	{
	    ds = op->dat[0];
	    dsp = op->dat[1];
	    niter = bou->n_asked;
	    ds->nx = ds->ny = 0;
	    dsp->nx = dsp->ny = 0;
	    sim_traject_ds(bou, ds, dsp, niter);
	}
    */
    op->need_to_refresh = 1;    
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}



int do_big_inc_S(void)
{
    pltreg *pr;
    O_p *op;
    d_s *ds;
    Duffin *bou = NULL;
    
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
	return D_O_K;		

    //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
    if(updating_menu_state != 0)	
	{
	    if (op != NULL && op->user_id >= 4096 && op->user_id < 4101)
		{
		    remove_short_cut_from_dialog(0, KEY_PGUP);
		    add_keyboard_short_cut(0, KEY_PGUP, 0, do_big_inc_S);
		}
	    return D_O_K;		
	}

    bou = find_Duffin_in_pr(pr);
    if (bou == NULL) return D_O_K;

    bou->P *= 1.005;
    set_plot_title(op, "\\stack{{\\partial\\theta /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)}{\\pt8 M = %g}}",bou->P);
    /*
    if (op->user_id == 4097)
	{
	    niter = ds->nx;
	    ds->nx = ds->ny = 0;
	    for (j = 0; j < niter; )
		{
		    do_simulate(bou);
		    if (bou->niter_npc != bou->nph) continue;
		    add_new_point_to_ds(ds, bou->x, bou->y);
		    j++;
		}
	}
    else if (op->n_dat > 1)
	{
	    ds = op->dat[0];
	    dsp = op->dat[1];
	    niter = bou->n_asked;
	    ds->nx = ds->ny = 0;
	    dsp->nx = dsp->ny = 0;
	    sim_traject_ds(bou, ds, dsp, niter);
	}
    */
    op->need_to_refresh = 1;    
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}



int do_big_dec_S(void)
{
    pltreg *pr;
    O_p *op;
    d_s *ds;
    Duffin *bou = NULL;
    
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
	return D_O_K;		

    //if (strncmp(op->dat[0]->source,"M = ",4)) return D_O_K;
    if(updating_menu_state != 0)	
	{
	    if (op != NULL && op->user_id >= 4096 && op->user_id < 4101)
		{
		    remove_short_cut_from_dialog(0, KEY_PGDN);
		    add_keyboard_short_cut(0, KEY_PGDN, 0, do_big_dec_S);
		}
	    return D_O_K;		
	}

    bou = find_Duffin_in_pr(pr);
    if (bou == NULL) return D_O_K;

    bou->P *= 0.995;
    set_plot_title(op, "\\stack{{\\partial\\theta /\\partial t^2 = M sin\\theta + P sin(\\theta  -t)}{\\pt8 P = %g}}",bou->P);
    /*
    if (op->user_id == 4097)
	{
	    niter = ds->nx;
	    ds->nx = ds->ny = 0;
	    for (j = 0; j < niter; )
		{
		    do_simulate(bou);
		    if (bou->niter_npc != bou->nph) continue;
		    add_new_point_to_ds(ds, bou->x, bou->y);
		    j++;
		}
	}
    else if (op->n_dat > 1)
	{
	    ds = op->dat[0];
	    dsp = op->dat[1];
	    niter = bou->n_asked;
	    ds->nx = ds->ny = 0;
	    dsp->nx = dsp->ny = 0;
	    sim_traject_ds(bou, ds, dsp, niter);
	}
    */
    op->need_to_refresh = 1;    
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}




int	convert_3D_bou_op(O_p *op)
{
    register int i, j, k;
    static float xc[32], yc[32], zc[32];
    float sit, cot, sip, cop;
    float thetal, phil;
    pltreg *pr;
    O_p *opb = NULL;
    d_s *ds = NULL, *dst = NULL, *dsp = NULL;
    
    if (op == NULL) return 1;

    if ((pr = find_pr_in_current_dialog(NULL)) == NULL)		
	return win_printf("cannot find pr");	


    for (i = 0, j = -1; j < 0 && i < pr->n_op; i++)
	j = (pr->o_p[i]->user_id == IS_DUFFIN_DATA) ? i : j;

    j = find_op_nb_of_source_specific_ds_in_pr(pr, "Duffin trajectory");
    
    if (j < 0) return win_printf("wrong Duffin data");

    opb = pr->o_p[j];
    //win_printf("plot found %d # ds %d ",j,opb->n_dat);
    //win_printf("plot found %d # ds %d source \n%s",j,opb->n_dat,opb->dat[0]->source);
    thetal = op->user_fspare[0];
    phil = op->user_fspare[1];

    sit = sin(thetal);
    cot = cos(thetal);
    sip = sin(phil);
    cop = cos(phil);	

    i = 0;
    xc[i] = 0; yc[i] = 1; zc[i++] = 0;
    xc[i] = 0; yc[i] = 0; zc[i++] = 0;
    xc[i] = -1; yc[i] = 0; zc[i++] = 0;
    xc[i] = -1; yc[i] = 0; zc[i++] = 2;
    xc[i] = 1; yc[i] = 0; zc[i++] = 2;
    xc[i] = 1; yc[i] = 0; zc[i++] = 0;
    xc[i] = -1; yc[i] = 0; zc[i++] = 0;

    xc[i] = -1; yc[i] = 1; zc[i++] = 0;
    xc[i] = -1; yc[i] = 1; zc[i++] = 2;
    xc[i] = 1; yc[i] = 1; zc[i++] = 2;	
    xc[i] = 1; yc[i] = 1; zc[i++] = 0;			
    xc[i] = -1; yc[i] = 1; zc[i++] = 0;			
    xc[i] = -1; yc[i] = 1; zc[i++] = 2;
    xc[i] = -1; yc[i] = 0; zc[i++] = 2;
    xc[i] = 1; yc[i] = 0; zc[i++] = 2;	    pr->use.to = IS_DUFFIN_DATA;
    xc[i] = 1; yc[i] = 1; zc[i++] = 2;
    xc[i] = 1; yc[i] = 1; zc[i++] = 0;
    xc[i] = 1; yc[i] = 0; zc[i++] = 0;

    ds = op->dat[0];
    ds->nx = ds->ny = i;
    for (i = 0 ;ds != NULL && i < 32 && i < ds->mx; i++)
	{
	    ds->xd[i] = -xc[i] * sit + (zc[i] - 1) * cot;
	    ds->yd[i] = -xc[i] * cot * sip - (zc[i] - 1) * sit * sip + yc[i] * cop;
	}
    //win_printf("Bef op %d opb %d",op->n_dat,opb->n_dat);
    for (i = 0, j = 1; i+1 < opb->n_dat && j < op->n_dat; j++, i += 2)
	{
	    ds = op->dat[j];
	    dst = opb->dat[i];
	    dsp = opb->dat[i+1];
	    if (ds == NULL || dst == NULL || dsp == NULL) continue;
	    for (k = 0 ;k < ds->nx && k < dst->nx && k < dsp->nx; k++)
		{
		    ds->xd[k] = -(dst->xd[k] * sit) + ((dsp->yd[k] -M_PI)* cot)/M_PI;
		    ds->yd[k] = -(dst->xd[k] * cot * sip) - ((dsp->yd[k] - M_PI) * sit * sip)/M_PI + dst->yd[k] * cop;
		}
	    //win_printf("ds %d k %d",j,k);	
	    
	}
    op->need_to_refresh = 1;    
    return 0;	
}


int change_angle_in_3D_plot_region(void)
{
    int x_m, y_m, dx, dy, dxp = 0, dyp = 0;
    pltreg *pr;
    O_p *op;
    float theta_0, phi_0;
    
    if(updating_menu_state != 0)	return D_O_K;		
    if ((pr = find_pr_in_current_dialog(NULL)) == NULL)		
	return win_printf("cannot find pr");	

    op = pr->one_p;
    if (strncmp(op->dat[0]->source,"box axis display",16)) 
	return win_printf_OK("Cannot find 3D plot");

    theta_0 = op->user_fspare[0];
    phi_0 = op->user_fspare[1];

    x_m = mouse_x;
    y_m = mouse_y;
	
    display_title_message("x_m %d ym %d",x_m,y_m);
	
    while (mouse_b & 0x3)
	{
	    dx = mouse_x - x_m;
	    dy = mouse_y - y_m;
	    if (dx != dxp || dy != dyp)
		{
		    dxp = dx;
		    dyp = dy;
		    op->user_fspare[0] = theta_0 + (float)(3*dx)/menu_x0;
		    op->user_fspare[1] = phi_0 + (float)(3*dy)/max_y;

		    convert_3D_bou_op(op);
		    refresh_plot(pr,UNCHANGED);	
		    display_title_message("\\theta %g \\phi %g",op->user_fspare[0],op->user_fspare[1]);
		}
	    if (key[KEY_PGDN]) 
		{
		    op->y_lo *= M_SQRT2;
		    op->y_hi *= M_SQRT2;
		    op->x_lo *= M_SQRT2;
		    op->x_hi *= M_SQRT2;
		    convert_3D_bou_op(op);	
		    refresh_plot(pr,UNCHANGED);				
		} 
	    if (key[KEY_PGUP])    
		{
		    op->y_lo *= M_SQRT1_2;
		    op->y_hi *= M_SQRT1_2;
		    op->x_lo *= M_SQRT1_2;
		    op->x_hi *= M_SQRT1_2;
		    convert_3D_bou_op(op);
		    refresh_plot(pr,UNCHANGED);									
		}
	}
    clear_keybuf();
    return 0;
}


int	do_3D_bou_draw(void)
{
    pltreg *pr;
    O_p *op;
    d_s *ds;
    
    if(updating_menu_state != 0)	return D_O_K;		
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
	return win_printf("cannot find data");

    if (strncmp(op->dat[0]->source,"box axis display",16)) return D_O_K;

    convert_3D_bou_op(op);
    return refresh_plot(pr, UNCHANGED);
}



int bou_3D_op_idle_action(O_p *op, DIALOG *d)
{
  (void)d;
  (void)op;

  change_angle_in_3D_plot_region();
  return 0;
}




int create_3D_plot(void)
{
    pltreg  *pr = NULL;
    int i, max;
    O_p	*op = NULL, *opbox = NULL;
    d_s *dsbox= NULL, *dsp = NULL;
    
	
    if(updating_menu_state != 0)	return D_O_K;		
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
	return win_printf_OK("cannot find data");
    
    if (op->n_dat < 2) 	return D_O_K;		
    if (op->dat[0]->source == NULL || op->dat[1]->source == NULL)  return win_printf_OK("cannot find ds source");
    /*
    if (strncmp(op->dat[0]->source,"Duffin trajectory (\\theta,d\\theta /dt) with M =",50) ) 
	return win_printf_OK("Wrong ds source\n%s\ninstead of \n"
			     "Duffin trajectory (\\theta,d\\theta /dt) with M ="
			     ,op->dat[0]->source);
    if (strncmp(op->dat[1]->source,"Duffin trajectory (\\theta, \\phi ) with M =",46))
	return win_printf_OK("Wrong ds source\n%s\ninstead of \n"
			     "Duffin trajectory (\\theta, \\phi ) with M ="
			     ,op->dat[1]->source);

    */
    if ((opbox = create_and_attach_one_plot(pr, 32, 32, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
    dsbox = opbox->dat[0];
    //dsbox->nx = dsbox->ny = 0;
    opbox->user_fspare[0] = opbox->user_fspare[1] = 0;
    set_ds_source(dsbox,"box axis display");	

    for(i = 0; i < op->n_dat; i += 2)
	{
	    /*
	    if (strncmp(op->dat[i]->source,"Duffin trajectory (\\theta,d\\theta /dt) with M =",50)) 
		continue;
	    if (strncmp(op->dat[i+1]->source,"Duffin trajectory (\\theta, \\phi ) with M =",50))
		continue;
	    */
	    max = op->dat[i]->nx;
	    max = (op->dat[i+1]->nx > max) ? op->dat[i+1]->nx : max;
	    if ((dsp = create_and_attach_one_ds(opbox, max, max, 0)) == NULL)
		return win_printf_OK("cannot create plot");	
	    set_ds_source(dsp,"3D vue of %s",op->dat[i]->source);	
	    set_ds_dash_line(dsp);
	}
    convert_3D_bou_op(opbox);
    opbox->y_lo = -0.6;
    opbox->y_hi = 1.6;
    opbox->x_lo = -1.5;
    opbox->x_hi = 1.5;
		
    set_plot_x_fixed_range(opbox);
    set_plot_y_fixed_range(opbox);
    
    opbox->iopt |= NOAXES;
    opbox->op_idle_action = bou_3D_op_idle_action;
    do_one_plot(pr);			
    broadcast_dialog_message(MSG_DRAW,0);
    return 0;
}


int set_bou_live(void)
{
  pltreg *pr;
  Duffin *bou = NULL;

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return D_O_K;		

  if(updating_menu_state != 0)
    {
      remove_short_cut_from_dialog(0, KEY_L);
      add_keyboard_short_cut(0, KEY_L, 0, set_bou_live);      
      return D_O_K;
    }
  bou = find_Duffin_in_pr(pr);
  if (bou == NULL) return D_O_K;

  bou->live = (bou->live) ? 0 : 1;
  return refresh_plot(pr, UNCHANGED);
}

int set_limits_to_op(void)
{
  pltreg *pr;
  O_p *op;
    
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return D_O_K;		

  if(updating_menu_state != 0)
    {
      remove_short_cut_from_dialog(0, KEY_B);
      add_keyboard_short_cut(0, KEY_B, 0, set_limits_to_op);
      
      return D_O_K;
    }
  op->x_lo = -1.8;
  op->x_hi = 1.8;
  op->y_lo = -5.2;
  op->y_hi = 5.2;
  set_plot_x_fixed_range(op);
  set_plot_y_fixed_range(op);
  op->need_to_refresh = 1;    
  return refresh_plot(pr, UNCHANGED);
}



MENU *Duffin_plot_menu(void)
{
    static MENU mn[32];
    
    if (mn[0].text != NULL)	return mn;
    add_item_to_menu(mn,"Trajectories", do_Duffin_trajectories_plot,NULL,0,NULL);
    add_item_to_menu(mn,"Poincare", do_Duffin_poicare,NULL,0,NULL);
    add_item_to_menu(mn,"3D", create_3D_plot,NULL,0,NULL);
    add_item_to_menu(mn,"Advance phase (right)",do_move_phase,NULL,0,NULL);
    add_item_to_menu(mn,"Small Inc S (up)",do_inc_S,NULL,0,NULL);
    add_item_to_menu(mn,"Small Decc S (Dwn)",do_dec_S,NULL,0,NULL);
    add_item_to_menu(mn,"Big Inc S (PgUp)",do_big_inc_S,NULL,0,NULL);
    add_item_to_menu(mn,"Big Dec S (PgDwn)",do_big_dec_S,NULL,0,NULL);
    add_item_to_menu(mn,"Set limits (B)",set_limits_to_op,NULL,0,NULL);
    add_item_to_menu(mn,"Set live (L)",set_bou_live,NULL,0,NULL);
    




    return mn;
}

int	Duffin_main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    add_plot_treat_menu_item ( "Duffin", NULL, Duffin_plot_menu(), 0, NULL);
  return D_O_K;
}

int	Duffin_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    remove_item_to_menu(plot_treat_menu, "Duffin", NULL, NULL);
    return D_O_K;
}
#endif

