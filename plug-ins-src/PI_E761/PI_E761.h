#ifndef _PI_E761_H_
#define _PI_E761_H_

PXV_FUNC(int, do_PI_E761_rescale_plot, (void));
PXV_FUNC(MENU*, PI_E761_plot_menu, (void));
PXV_FUNC(int, do_PI_E761_rescale_data_set, (void));
PXV_FUNC(int, PI_E761_main, (int argc, char **argv));
#endif

