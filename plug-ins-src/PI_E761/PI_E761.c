/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _PI_E761_C_
#define _PI_E761_C_

#include "allegro.h"
#include "winalleg.h"
#include "xvin.h"


/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "PI_E761.h"
//place here other headers of this plugin 

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "E7XX_GCS_DLL.h"
# include "PI_E761.h"

char PI_E761_axes[10];
int PI_E761_ID = -1;



int PI_E761_start(void)
{

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  /* we first find the data that we need to transform */
  // connect to the E-761 over PCI (boardnumber 1) 
  if (PI_E761_ID >= 0) return win_printf_OK("PI E761 board! already connected"); 
  PI_E761_ID = E7XX_ConnectPciBoard (1); 
  if (PI_E761_ID<0)     return win_printf_OK("Cannot init PI E761 board!"); 
    
  if (!E7XX_qSAI(PI_E761_ID, PI_E761_axes, 9)) 
    return win_printf_OK("Cannot read PI E761 axes!"); 
 
  // the output should be "12" - if axes 1 and 2 were configured 
  win_printf("qSAI() returned \"%s\"", PI_E761_axes); 
  return D_O_K;
}

int PI_E761_close(void)
{

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  /* we first find the data that we need to transform */
  // connect to the E-761 over PCI (boardnumber 1) 
  if (PI_E761_ID >= 0)  
    {
      E7XX_CloseConnection (PI_E761_ID); 
      win_printf_OK("PI E761 board disconnected"); 
    }
  else return win_printf_OK("PI E761 board is not connected"); 
  return D_O_K;
}

int PI_E761_read_Z(void)
{
  double pos[4];


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  if (PI_E761_ID<0) PI_E761_ID = E7XX_ConnectPciBoard (1); 
  if (PI_E761_ID<0)     return win_printf_OK("Cannot init PI E761 board!"); 
  E7XX_qPOS(PI_E761_ID, "3", pos);
  win_printf("Z position %g", pos[0]); 
  return D_O_K;
}


int PI_E761_set_Z(void)
{
  double pos[4];
  float posfl[4];
  


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the y coordinate"
			   "of a data set by a number and place it in a new plot");
    }
  if (PI_E761_ID<0) PI_E761_ID = E7XX_ConnectPciBoard (1); 
  if (PI_E761_ID<0)     return win_printf_OK("Cannot init PI E761 board!"); 
  E7XX_qPOS(PI_E761_ID, "3", pos);
  posfl[0] = pos[0];
  win_scanf("New Z position %f", posfl);
  pos[0] = posfl[0];
  E7XX_MOV(PI_E761_ID, "3", pos);
  return D_O_K;
}



MENU *PI_E761_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Start PI_E761", PI_E761_start,NULL,0,NULL);
  add_item_to_menu(mn,"Stop PI_E761", PI_E761_close,NULL,0,NULL);
  add_item_to_menu(mn,"PI_E761 Read Z", PI_E761_read_Z,NULL,0,NULL);
  add_item_to_menu(mn,"PI_E761 Set Z", PI_E761_set_Z,NULL,0,NULL);
  return mn;
}

int	PI_E761_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "PI_E761", NULL, PI_E761_plot_menu(), 0, NULL);
  return D_O_K;
}

int	PI_E761_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "PI_E761", NULL, NULL);
  return D_O_K;
}
#endif

