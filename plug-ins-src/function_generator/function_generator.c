#ifndef _function_generator_C_
#define _function_generator_C_

#include "allegro.h"
#include "xvin.h"

/* If you include other plug-ins header do it here*/ 

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "function_generator.h"







int do_ramps_and_plateaux(void)
{
	int 		i;
static float 	amplitude=1.;
static float    offset=0.;
static int		Ntot=1e6;
static float 		t_ramps = 100;
	int N_ramps;
static float 		freq=1e6;
	pltreg	*pr = NULL;
	O_p 	*op2, *op1 = NULL;
	d_s 	*ds2, *ds1;
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routines builds a signal with ramps and plateaux"
					"and place it in a new plot");
	}
	
	i=win_scanf("{\\pt14\\color{yellow} ramps - Parameters}\n\n"
			"number of points N : %10d\n"
			"amplitude ? %f\n"
			"offset ?   %f \n"
			"duree d'une rampe (ms)? %f \n"
			"frequence d'echantillonage (Hz)? %f\n",
			&Ntot, &amplitude, &offset, &t_ramps, &freq);
	if (i==CANCEL) return(D_O_K);
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	if ((op2 = create_and_attach_one_plot(pr, Ntot, Ntot, 0)) == NULL) return win_printf_OK("cannot create plot !");	
	ds2 = op2->dat[0];
	for (i=0; i<Ntot; i++)
	{	
		ds2->xd[i] = (float)i;
	}
	N_ramps = (int) t_ramps*1e-3*freq;
	if (N_ramps>Ntot/2)	return win_printf_OK("ramp is too long...");
	for (i=0; i<N_ramps; i++)
	{	
		ds2->yd[i]= (float)offset-(float)amplitude+ (float)2.*amplitude*(float)i/(float)N_ramps; 		
	}
	for (i=N_ramps; i <Ntot/2;i++)
	{
		ds2->yd[i] = (float) amplitude+offset;
	}
	for (i=Ntot/2; i<Ntot/2+N_ramps; i++)
	{	
		ds2->yd[i]= (float)offset+(float)amplitude- (float)2.*amplitude*(float)(i-Ntot/2.)/(float)N_ramps;	
	}
	for (i=Ntot/2+N_ramps;i <Ntot;i++)
	{
		ds2->yd[i] = (float) offset-amplitude;
	}
	set_plot_title(op2,"generated signal with ramps and plateaux");
	op2->filename = Transfer_filename(op1->filename);
    op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
}
 /* end of function 'do_ramps_and_plateaux' */

 int do_ramps(void)
{
	int 		i;
static float 	amplitude=1.;
static float    offset=0.;
static int		Ntot=1e6;
	pltreg	*pr = NULL;
	O_p 	*op2, *op1 = NULL;
	d_s 	*ds2, *ds1;
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routines builds a triangular signal"
					"and place it in a new plot");
	}
	
	i=win_scanf("{\\pt14\\color{yellow} ramps - Parameters}\n\n"
			"number of points N : %10d\n"
			"amplitude ? %f\n"
			"offset ?   %f \n",
			&Ntot, &amplitude, &offset);
	if (i==CANCEL) return(D_O_K);
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	if ((op2 = create_and_attach_one_plot(pr, Ntot, Ntot, 0)) == NULL) return win_printf_OK("cannot create plot !");	
	ds2 = op2->dat[0];
	for (i=0; i<Ntot; i++)
	{	
		ds2->xd[i] = (float)i;
	}
	
	for (i=0; i<Ntot/2; i++)
	{	
		ds2->yd[i]= (float)offset-(float)amplitude+ (float)4*amplitude*(float)i/(float)Ntot; 		
	}
	for (i=Ntot/2; i<Ntot; i++)
	{	
		ds2->yd[i]= (float)offset+(float)3*amplitude- (float)4*amplitude*(float)i/(float)Ntot;	
	}
	set_plot_title(op2,"generated triangular signal");
	op2->filename = Transfer_filename(op1->filename);
    op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
}
 /* end of function 'do_ramps' */



MENU *function_generator_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"ramps",	do_ramps,		NULL, 0, NULL);
	add_item_to_menu(mn,"ramps and plateaux",	do_ramps_and_plateaux,		NULL, 0, NULL);
	return(mn);
}

int	function_generator_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("function generator", NULL, function_generator_plot_menu(), 0, NULL);
	return D_O_K;
}


int	function_generator_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"function generator",	NULL, NULL);
	return D_O_K;
}
#endif

