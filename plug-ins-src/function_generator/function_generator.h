#ifndef _function_generator_H_
#define _function_generator_H_

#define	function_generator_LOWER		0x0100
#define	function_generator_UPPER		0x0200
#define	function_generator_BEGIN		0x1000
#define	function_generator_MIDDLE		0x2000
#define	function_generator_END			0x4000

#define	FILE_BINARY		0x0100
#define	FILE_NI4472		0x0200


PXV_FUNC(MENU*, function_generator_plot_menu, 		(void));
PXV_FUNC(int, function_generator_main, 				(int argc, char **argv));
PXV_FUNC(int, graph_unload,				(int argc, char **argv));


PXV_FUNC(int, do_ramps,	(void));
PXV_FUNC(int, do_ramps_and_plateaux,	(void));

#endif

