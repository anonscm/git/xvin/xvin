/****************************************************************/
/*    Plug-in program for random number generation in XVin	*/
/*								*/
/* 	V. Croquette, N. Garnier				*/
/*	18 novembre 2003					*/
/****************************************************************/
#ifndef _KK_C_
#define _KK_C_

#include "allegro.h"
#include "xvin.h"

#include "../diff/diff.h"

#define BUILDING_PLUGINS_DLL
#include "KK.h"


/* to check if a dataset is such that the limit for large time is 0 or not */
/* returns 1 if OK, or 0 if not OK                                         */
int check_decrease_2pts(float *x, int n, int p1, int p2)
{   int i;
    double m1, m2;

    i=p1; m1 = log(fabs(x[i])); 
    i=p2; m2 = log(fabs(x[i])); 
    
    if (m2<m1) return(1);
    return(0);
}

/* to check if a dataset is such that the limit for large time is 0 or not */
/* returns 1 if OK, or 0 if not OK                                         */
int check_decrease_average(float *x, int n, int p)
{   int i;
    double m1=0.;

    for (i=n-p; i<n; i++)
    {   m1 += log(fabs(x[i])); 
    }
    if (m1<0) return(1);
    return(0);
}


int do_KK_RE_IM(void)
{	register int i, j;
	char	*info=NULL, *message_warning=NULL;
	double  dt, m, r1=0., r2=0., log_factor=0.0;
	float   *deriv;
	pltreg	*pr;
	O_p	    *op;
	d_s	    *ds, *ds2=NULL;
	int 	index, n;
	static int bool_correct=0;


	if (updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	
	if (key[KEY_LSHIFT]) 	
	{ 	if (index == KK_RE_IM_2)
		{ info = my_sprintf(info,"Kramers-Kronig\n\n"
                 "We compute the imaginary part from the real part\n"
                 "We assume response function is real and imaginary part is even\n"
                 "(no memory effects)."); }
        else if (index == KK_IM_RE_2)
		{ info = my_sprintf(info,"Kramers-Kronig\n"
                 "We compute the real part from the imaginary part"
                 "We assume response function is real and imaginary part is even\n"
                 "(no memory effects)."); }
        else if (index == KK_RE_IM_1)
		{ info = my_sprintf(info,"Kramers-Kronig\n"
                 "We compute the imaginary part from the real part"
                 "We do not assume response function is real, imaginary part can be non-even\n"
                 "(we allow memory effects)."); }
        else if (index == KK_IM_RE_1)
		{ info = my_sprintf(info,"Kramers-Kronig\n"
                 "We compute the real part from the imaginary part"
                 "We do not assume response function is real, imaginary part can be non-even\n"
                 "(we allow memory effects)."); }

        else info = my_sprintf(info,"???");
		
		return(win_printf_OK(info));
	}

    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)  return win_printf_OK("cannot find data!");
	if (ds == NULL)   return (win_printf_OK("can't find data set"));
	n = ds->nx;

    if (check_decrease_2pts(ds->yd, n, n-2, n-1)==0)
    if (check_decrease_2pts(ds->yd, n, n-2, n-1)==0)
    if (win_printf("dataset X is such that lim X(t) is not 0 when t->\\infty\n\n"
                   "Your response function may not be causal !!!\n"
                   "In that case, inverse the response function, and try again.\n\n"
                   "Or my test is simply too severe...\n"
                   "click OK to go on or CANCEL to abort")==CANCEL)
       return(D_O_K);

    dt=1.;
	// a tentative measure of dt:
    message_warning=my_sprintf(message_warning, "{\\color{yellow}\\pt14 Kramers-Kronig}\n\n");
	if ( (ds->xd[2]-ds->xd[1])!=0 )
	{	dt=(float)(ds->xd[1]-ds->xd[0]);
	    if ( (ds->xd[2]-ds->xd[1])!=(ds->xd[1]-ds->xd[0]) )
		message_warning=my_sprintf(message_warning, "\n{\\color{lightred}dt is not constant !}");
        else  message_warning=my_sprintf(message_warning, "d\\omega = %g}", dt);
	}
	else message_warning=my_sprintf(message_warning, "\n{\\color{lightred}dt=0 at t=0 ?}");
	message_warning=my_sprintf(message_warning, "\n%%b use additional correction (proportional to d\\omega)\n");
	if (win_scanf(message_warning, &bool_correct)==CANCEL)
       return(D_O_K);
    free(message_warning); message_warning=NULL;   

    if ((ds2 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	
   	   return(win_printf_OK("cannot create dataset !"));

    deriv=(float*)calloc(n,sizeof(float));
    if (deriv==NULL)
       return(win_printf_OK("cannot allocate memory !"));;
    if (diff_splines_float(ds->xd, ds->yd, n, deriv)!=n) 
       return(win_printf_OK("pb with splines to differentiate !"));


    for (i=0; i<n; i++)
    {
        m=0.;
        for (j=1; j<n; j++)
        {   if(i!=j)
            {       if ( (index==KK_RE_IM_2) || (index==KK_RE_IM_1) )
                       r1 = -ds->xd[i]*ds->yd[j];
                    else if ( (index==KK_IM_RE_2) || (index==KK_IM_RE_1) )
                       r1 = +ds->xd[j]*ds->yd[j];
                    r2 = ds->xd[j]*ds->xd[j] - ds->xd[i]*ds->xd[i];
            }
            else // j=i
            {       if (index == KK_RE_IM_2)
                       r1 = 0.0;
                    else if (index == KK_IM_RE_2)
                       r1 = + deriv[j];
                    r2 = 1.0;
            }
            m += r1/r2;
            
        }
        m *= dt;
        if ( (i>0) && (i<n-1) && (bool_correct==1) ) // correction due to controled divergence (cf cahier VI, p187)
        {    log_factor = log(fabs(2.0*ds->xd[i] + dt/2.0)) - log(fabs(2.0*ds->xd[i] - dt/2.0));
             m         -= (double)(ds->xd[i]*deriv[i] - 0.5*ds->yd[i]) * log_factor;
        }
        m *= (2.0/M_PI);
        
        ds2->xd[i] = ds->xd[i];
        ds2->yd[i] = (float)m;
        
        if ( (index==KK_RE_IM_1) || (index==KK_RE_IM_1) )
        {  /* in that case, a second integral has to be computed, because Imaginary part
              of the response function is not even (this is for example the case when some 
              visco-elasticity is present, etc. */
              
            m=0.;
            for (j=1; j<n; j++)
            {   r1=0.0;
                r2=1.0;
            }
            m += r1/r2;
            
        ds2->yd[i] += (float)m;
        
        }
    }

    switch(index)
    {      case 	KK_RE_IM_2 : message_warning=my_sprintf(message_warning,"Re->Im, regular"); break;
           case 	KK_IM_RE_2 : message_warning=my_sprintf(message_warning,"Im->Re, regular"); break;
           case 	KK_RE_IM_1 : message_warning=my_sprintf(message_warning,"Re->Im, with memory"); break;
           case 	KK_IM_RE_1 : message_warning=my_sprintf(message_warning,"Im->Re, with memory"); break;
           default             : message_warning=my_sprintf(message_warning,"???"); break;
           
    }
    set_ds_source(ds2, "Kramers-Kronig : %s", message_warning);
    free(message_warning); message_warning=NULL;
    
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

MENU *KK_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Re -> Im", 	        do_KK_RE_IM, NULL, KK_RE_IM_2, NULL);
	add_item_to_menu(mn,"Im -> Re", 	        do_KK_RE_IM, NULL, KK_IM_RE_2, NULL);
	add_item_to_menu(mn,"\0", 			        NULL,        NULL, 0,          NULL);
	add_item_to_menu(mn,"Re -> Im (memory)", 	do_KK_RE_IM, NULL, KK_RE_IM_1, NULL);
		
	return mn;
}


int KK_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "Kramers Kronig", NULL, KK_plot_menu(), 0, NULL);
	return D_O_K;
}


int KK_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"Kramers Kronig",	NULL, NULL);
	return D_O_K;
}
#endif

