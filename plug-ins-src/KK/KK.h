#ifndef _KK_H_
#define _KK_H_

// definitions
#define KK_RE_IM_1 0x000001
#define KK_IM_RE_1 0x000010
#define KK_RE_IM_2 0x001000
#define KK_IM_RE_2 0x000100

XV_FUNC(int, check_decrease_2pts, (float *x, int n, int p1, int p2));
XV_FUNC(int, check_decrease_average, (float *x, int n, int p));

XV_FUNC(MENU*, KK_plot_menu, (void));
XV_FUNC(int, KK_main, (int argc, char **argv));
XV_FUNC(int, KK_unload, (int argc, char **argv));

#endif

