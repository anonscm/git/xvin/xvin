#ifndef _SHIFT_DATA_H_
#define _SHIFT_DATA_H_

PXV_FUNC(int, do_shift_data_rescale_plot, (void));
PXV_FUNC(MENU*, shift_data_plot_menu, (void));
PXV_FUNC(int, do_shift_data_rescale_data_set, (void));
PXV_FUNC(int, shift_data_main, (int argc, char **argv));
#endif

