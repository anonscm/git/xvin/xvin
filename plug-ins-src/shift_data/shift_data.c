/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _SHIFT_DATA_C_
#define _SHIFT_DATA_C_

# include "allegro.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"
#include <fftw3.h>      // include the fftw v3.0.1 or v3.1.1 library for computing spectra:

/* If you include other regular header do it here*/
/* introduction to numerical analysis by Hildebrand p 350 */

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "shift_data.h"
//place here other headers of this plugin
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "shift_data.h"




double poly_4_in_x_1(float *x, float *y, float pos_x);
double poly_4_in_x0(float *x, float *y, float pos_x);
double poly_4_in_x1(float *x, float *y, float pos_x);
double poly_4_in_x2(float *x, float *y, float pos_x);

double poly_3_in_x_1(float *x, float *y, float pos_x);
double poly_3_in_x0(float *x, float *y, float pos_x);
double poly_3_in_x1(float *x, float *y, float pos_x);

double poly_2_in_x0(float *x, float *y, float pos_x);
double poly_2_in_x1(float *x, float *y, float pos_x);





int m_points = 0;       /* the number of points is actually 2*m +1 */

# include "stdio.h"
# include "math.h"
# include "malloc.h"

double  *p1 = NULL;
double  *p2 = NULL;
double  *p3 = NULL;
double  *p4 = NULL;
double  *p5 = NULL;


double **po = NULL;
int n_po_order = 0;            // maximum order of polynome
int n_po_m = 0, n_po_nx = 0;   // nx = 2 * m +1


int     poly_init(int m);
int     init_p1(double *p1, int m);
int     init_p2(double *p2, int m);
int     init_p3(double *p3, int m);
int     init_p4(double *p4, int m);
int     init_p5(double *p5, int m);
int     poly_fit4(float *y, int nx, double *y0, double *y1, double *y2, double *y3, double *y4);


int     poly_fit4(float *y, int nx, double *y0, double *y1, double *y2, double *y3, double *y4)
{
    register int i;
    double tmp;

    if (nx % 2 == 0)              return -1;

    if (poly_init((nx - 1) / 2))    return -1;

    tmp = (double)1 / nx;

    if (y0 != NULL)
    {
        for (i = 0, *y0 = 0; i < nx; i++)  *y0 += y[i];

        *y0 *= tmp;
    }

    if (y1 != NULL)
    {
        for (i = 0, *y1 = 0; i < nx; i++)  *y1 += y[i] * p1[i];
    }

    if (y2 != NULL)
    {
        for (i = 0, *y2 = 0; i < nx; i++)  *y2 += y[i] * p2[i];
    }

    if (y3 != NULL)
    {
        for (i = 0, *y3 = 0; i < nx; i++)  *y3 += y[i] * p3[i];
    }

    if (y4 != NULL)
    {
        for (i = 0, *y4 = 0; i < nx; i++)  *y4 += y[i] * p4[i];
    }

    return 0;
}


int     poly_fit5(float *y, int nx, double *yt)
{
    register int i;
    double tmp;

    if (nx % 2 == 0)              return -1;

    if (poly_init((nx - 1) / 2))    return -1;

    tmp = (double)1 / nx;

    for (i = 0, yt[0] = 0; i < nx; i++)  yt[0] += y[i];

    yt[0] *= tmp;

    for (i = 0, yt[1] = 0; i < nx; i++)  yt[1] += y[i] * p1[i];

    for (i = 0, yt[2] = 0; i < nx; i++)  yt[2] += y[i] * p2[i];

    for (i = 0, yt[3] = 0; i < nx; i++)  yt[3] += y[i] * p3[i];

    for (i = 0, yt[4] = 0; i < nx; i++)  yt[4] += y[i] * p4[i];

    for (i = 0, yt[5] = 0; i < nx; i++)  yt[5] += y[i] * p5[i];

    return 0;
}



int     poly_init(int m)
{
    if (m <= 0)         return -1;

    if (m == m_points)  return 0;

    p1 = (double *)realloc(p1, (2 * m + 1) * sizeof(double));
    p2 = (double *)realloc(p2, (2 * m + 1) * sizeof(double));
    p3 = (double *)realloc(p3, (2 * m + 1) * sizeof(double));
    p4 = (double *)realloc(p4, (2 * m + 1) * sizeof(double));
    p5 = (double *)realloc(p5, (2 * m + 1) * sizeof(double));
    m_points = m;

    if (p1 == NULL || p2 == NULL || p3 == NULL || p4 == NULL || p5 == NULL)
        return 1;

    init_p1(p1, m);
    init_p2(p2, m);
    init_p3(p3, m);
    init_p4(p4, m);
    init_p5(p5, m);
    return 0;
}

# ifdef ENCOURS


int     poly_ortho_init(int npts, int norder)
{
    int i, nf;
    double **po = NULL;
    int n_po_order = 0;            // maximum order of polynome
    int n_po_m = 0, n_po_nx = 0;   // nx = 2 * m +1



    if (npts <= 0)          return -1;

    if (npts == n_po_nx && norder == n_po_order)    return 0;

    n_po_m = (npts / 2);

    if (norder != n_po_order)
    {
        po = (double **)realloc(po, norder * sizeof(double *));

        if (po == NULL) return 1;
    }

    n_po_order = norder;

    for (i = 0; i < norder, i++)
    {
        po[i] = (double *)realloc(po[i], npts * sizeof(double));

        if (po[i] == NULL) return 1;
    }

    p2 = (double *)realloc(p2, (2 * m + 1) * sizeof(double));
    p3 = (double *)realloc(p3, (2 * m + 1) * sizeof(double));
    p4 = (double *)realloc(p4, (2 * m + 1) * sizeof(double));
    p5 = (double *)realloc(p5, (2 * m + 1) * sizeof(double));
    m_points = m;

    if (p1 == NULL || p2 == NULL || p3 == NULL || p4 == NULL || p5 == NULL)
        return 1;

    init_p1(p1, m);
    init_p2(p2, m);
    init_p3(p3, m);
    init_p4(p4, m);
    init_p5(p5, m);
    return 0;
}

# endif

int     init_p1(double *p1, int m)
{
    register int i, j;
    double s;

    if (m <= 0) return -1;

    for (i = -m, j = 0, s = 0; i <= m; i++, j++)
    {
        p1[j] = ((double)i) / m;
        s += p1[j] * p1[j];
    }

    for (j = 0, s = sqrt(s); j <= 2 * m; j++)  p1[j] /= s;

    return 0;
}
int     init_p2(double *p2, int m)
{
    register int i, j;
    double tmp, tmp1, x, s;

    if (m <= 0) return -1;

    tmp = ((double)1) / (m * (2 * m - 1));
    tmp1 = m * (m + 1);

    for (i = -m, j = 0, s = 0; i <= m; i++, j++)
    {
        x = (double)i;
        p2[j] = (3 * x * x - tmp1) * tmp;
        s += p2[j] * p2[j];
    }

    for (j = 0, s = sqrt(s); j <= 2 * m; j++)  p2[j] /= s;

    return 0;
}
int     init_p3(double *p3, int m)
{
    register int i, j;
    double tmp, tmp1, x, s;

    if (m <= 0) return -1;

    tmp = ((double)1) / (m * (2 * m - 1) * (m - 1));
    tmp1 = 3 * m * m + 3 * m - 1;

    for (i = -m, j = 0, s = 0; i <= m; i++, j++)
    {
        x = (double)i;
        p3[j] = (5 * x * x - tmp1) * x * tmp;
        s += p3[j] * p3[j];
    }

    for (j = 0, s = sqrt(s); j <= 2 * m; j++)  p3[j] /= s;

    return 0;
}
int     init_p4(double *p4, int m)
{
    register int i, j;
    double tmp, tmp1, tmp2, x, s;

    if (m <= 0) return -1;

    tmp = ((double)1) / (2 * m * (2 * m - 1) * (m - 1) * (2 * m - 3));
    tmp1 = 3 * m * (m * m - 1) * (m + 2);
    tmp2 = 6 * m * m + 6 * m - 5;

    for (i = -m, j = 0, s = 0; i <= m; i++, j++)
    {
        x = (double)i;
        p4[j] = (35 * x * x * x * x - 5 * tmp2 * x * x + tmp1) * tmp;
        s += p4[j] * p4[j];
    }

    for (j = 0, s = sqrt(s); j <= 2 * m; j++)  p4[j] /= s;

    return 0;
}
int     init_p5(double *p5, int m)
{
    register int i, j;
    double tmp, tmp1, tmp2, x, s;

    if (m <= 0) return -1;

    tmp = ((double)1) / (2 * m * (2 * m - 1) * (m - 1) * (2 * m - 3) * (m - 2));
    tmp1 = 15 * m * m * m * m + 30 * m * m * m - 35 * m * m - 50 * m + 12;
    tmp2 = 2 * m * m + 2 * m - 3;

    for (i = -m, j = 0, s = 0; i <= m; i++, j++)
    {
        x = (double)i;
        p5[j] = (65 * x * x * x * x - 35 * tmp2 * x * x + tmp1) * x * tmp;
        s += p5[j] * p5[j];
    }

    for (j = 0, s = sqrt(s); j <= 2 * m; j++)  p5[j] /= s;

    return 0;
}





int do_project_on_polynome_ds(void)
{
    register int i, j;
    O_p *op = NULL;
    int nf;
    d_s *ds, *dsi;
    pltreg *pr = NULL;
    static int order = 3;
    double  yt[6];


    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    nf = dsi->nx; /* this is the number of points in the data set */

    if (nf % 2 == 0)
    {
        nf = nf - 1;
        i = win_printf("Projection can only be done on an Odd number\n"
                       "of points! do you agree to use %d", nf);

        if (i == CANCEL)  return OFF;
    }

    poly_fit5(dsi->yd, nf, yt);
    //poly_fit4(ds->yd, nf, yt, yt + 1, yt + 2, yt + 3, yt + 4);
    i = win_scanf("Order to use (<= 5) %d", &order);

    if (i == CANCEL)  return OFF;


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    for (j = 0; j < nf; j++)
    {
        ds->xd[j] = dsi->xd[j];
        ds->yd[j] = yt[0];

        if (order >= 1) ds->yd[j] += yt[1] * p1[j];

        if (order >= 2) ds->yd[j] += yt[2] * p2[j];

        if (order >= 3) ds->yd[j] += yt[3] * p3[j];

        if (order >= 4) ds->yd[j] += yt[4] * p4[j];

        if (order >= 5) ds->yd[j] += yt[5] * p5[j];
    }

    /* now we must do some house keeping */
    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "Projection on poly 5");
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}








int do_draw_ortho_polynome_op(void)
{
    register int  j;
    O_p *op = NULL;
    int nf;
    d_s *ds;
    pltreg *pr = NULL;


    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
        return win_printf_OK("cannot find data");

    if (m_points == 0)  return win_printf_OK("No polynome defined");

    nf = 2 * m_points + 1;    /* this is the number of points in the data set */


    if ((op = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    ds = op->dat[0];

    for (j = 0; j < nf; j++)
    {
        ds->yd[j] = sqrt((float)1 / nf);
        ds->xd[j] = j;
    }

    /* now we must do some house keeping */
    set_ds_source(ds, "Poly 0 of %d points", nf);

    set_plot_title(op, "5 first othogonal Polynomes of %d points", nf);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    for (j = 0; j < nf; j++)
    {
        ds->xd[j] = j;
        ds->yd[j] = p1[j];
    }

    /* now we must do some house keeping */
    set_ds_source(ds, "Poly 1 of %d points", nf);

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    for (j = 0; j < nf; j++)
    {
        ds->xd[j] = j;
        ds->yd[j] = p2[j];
    }

    /* now we must do some house keeping */
    set_ds_source(ds, "Poly 2 of %d points", nf);

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    for (j = 0; j < nf; j++)
    {
        ds->xd[j] = j;
        ds->yd[j] = p3[j];
    }

    /* now we must do some house keeping */
    set_ds_source(ds, "Poly 3 of %d points", nf);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    for (j = 0; j < nf; j++)
    {
        ds->xd[j] = j;
        ds->yd[j] = p4[j];
    }

    /* now we must do some house keeping */
    set_ds_source(ds, "Poly 4 of %d points", nf);


    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    for (j = 0; j < nf; j++)
    {
        ds->xd[j] = j;
        ds->yd[j] = p5[j];
    }

    /* now we must do some house keeping */
    set_ds_source(ds, "Poly 5 of %d points", nf);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}




int do_expand_periodic_ds(void)
{
    register int i, j;
    O_p *op = NULL;
    int nf;
    d_s *ds, *dsi;
    pltreg *pr = NULL;
    float  *yt = NULL;


    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    if (fft_init(dsi->nx))
        return win_printf_OK("Cannot init fft");

    nf = 32 * dsi->nx; /* this is the number of points in the data set */

    yt = (float *)calloc(nf, sizeof(float));

    if (yt == NULL)       return win_printf_OK("Cannot alloc");

    if ((ds = create_and_attach_one_ds(op, nf / 2, nf / 2, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    for (j = 0; j < dsi->nx; j++)
    {
        yt[2 * j] = dsi->yd[j];
    }

    fft(2 * dsi->nx, yt, 1);

    for (j = 2 * dsi->nx - 1, i = nf - 1; j >= dsi->nx; j--, i--)
    {
        yt[i] = yt[j];
        yt[j] = 0;
    }

    if (fft_init(nf))
        return win_printf_OK("Cannot init fft");

    fft(nf, yt, -1);

    for (j = 0; j < ds->nx; j++)
    {
        ds->yd[j] = yt[2 * j];
        ds->xd[j] = (float)j / 16;
    }

    /* now we must do some house keeping */
    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "Projection on poly 5");
    free(yt);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


int do_simple_correlation_on_ds(void)
{
    register int i, j;
    O_p *op = NULL, *opn;
    int nf, lauto;
    d_s *ds, *dsi, *dsi2;
    pltreg *pr = NULL;
    static int ndsi2 = 0, nodc = 1, norm = 1, no_auto = 1;
    char question[2048], *tmpc = NULL;
    float m1, m2;


    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine computes correlation of data set in real space");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    dsi2 = dsi;

    if (op->n_dat > 1)
    {
        i = win_scanf("Do you want to perform auto-correlation %R\n"
                      "or cross-correlation with a second data set %r\n"
                      "Do you want to remove DC %b\nTo normalize by C(0) %b\n", &no_auto, &nodc, &norm);

        if (i == CANCEL) return 0;

        lauto = (no_auto) ? 0 : 1;

        if (lauto == 0)
        {
            ndsi2 = (op->cur_dat + 1) % op->n_dat;

            if (op->n_dat < 8)
            {
                snprintf(question, sizeof(question), "First selected data set number = %d \n"
                         "specify the number of the second data set "
                         "to cross-correlate\n0-> %%R 1->%%r", op->cur_dat);

                for (i = 2; i < op->n_dat; i++)
                {
                    tmpc = strdup(question);
                    snprintf(question, sizeof(question), "%s %d->%%r", tmpc, i);
                    free(tmpc);
                }

                i = win_scanf(question, &ndsi2);

                if (i == CANCEL) return 0;
            }
            else
            {
                snprintf(question, sizeof(question), "First selected data set number = %d \n"
                         "specify the number of the second data set "
                         "to cross-correlate%%d", op->cur_dat);
                i = win_scanf(question, &ndsi2);

                if (i == CANCEL) return 0;
            }

            dsi2 = op->dat[ndsi2];
        }
    }
    else
    {
        snprintf(question, sizeof(question), "Auto-correlation of data set %d\n"
                 "Do you want to remove DC %%b\nTo normalize by C(0) %%b\n", op->cur_dat);
        i = win_scanf(question, &nodc, &norm);

        if (i == CANCEL) return 0;
    }


    if (dsi2->nx != dsi->nx || dsi2->ny != dsi->ny)
        win_printf_OK("Cannot correlate\nYour data sets have different number of points !");

    nf = dsi->nx;

    if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    ds = opn->dat[0];

    for (j = 0, m1 = m2 = 0; j < nf && nodc > 0; j++)
    {
        m1 += dsi->yd[j];
        m2 += dsi2->yd[j];
    }

    if (nf > 0)
    {
        m1 /= nf;
        m2 /= nf;
    }

    for (i = 0; i < nf; i++)
    {
        for (j = 0; j < nf; j++)
        {
            ds->yd[i] += (dsi->yd[j] - m1) * (dsi2->yd[(nf + j - i) % nf] - m2) ;
        }

        ds->xd[i] = dsi->xd[i];
    }

    for (i = nf - 1; i >= 0 && norm > 0 && ds->yd[0] > 0; i--)
        ds->yd[i] /= ds->yd[0];

    set_plot_y_title(opn, "Correlation");
    set_plot_x_title(opn, "Delay");
    opn->filename = Transfer_filename(op->filename);
    uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_X_UNIT_SET);

    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "Correlation of ds %d with %d", op->cur_dat, ndsi2);
    /* refisplay the entire plot */
    refresh_plot(pr, pr->n_op - 1);
    return D_O_K;
}



int do_simple_convolution_on_ds(void)
{
    register int i, j;
    O_p *op = NULL;
    int nf, lauto;
    d_s *ds, *dsi, *dsi2;
    pltreg *pr = NULL;
    static int ndsi2 = 0, no_auto = 1;
    char question[2048], *tmpc = NULL;


    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine computes convolution of data sets in real space");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    dsi2 = dsi;

    if (op->n_dat > 1)
    {
        i = win_scanf("Do you want to perform auto-convolution %R\n"
                      "or convolution with a second data set %r\n", &no_auto);

        if (i == CANCEL) return 0;

        lauto = (no_auto) ? 0 : 1;

        if (lauto == 0)
        {
            ndsi2 = (op->cur_dat + 1) % op->n_dat;

            if (op->n_dat < 8)
            {
                snprintf(question, sizeof(question), "First selected data set number = %d \n"
                         "specify the number of the second data set "
                         "for convolution\n0-> %%R 1->%%r", op->cur_dat);

                for (i = 2; i < op->n_dat; i++)
                {
                    tmpc = strdup(question);
                    snprintf(question, sizeof(question), "%s %d->%%r", tmpc, i);
                    free(tmpc);
                }

                i = win_scanf(question, &ndsi2);

                if (i == CANCEL) return 0;
            }
            else
            {
                snprintf(question, sizeof(question), "First selected data set number = %d \n"
                         "specify the number of the second data set "
                         "for convolution%%d", op->cur_dat);
                i = win_scanf(question, &ndsi2);

                if (i == CANCEL) return 0;
            }

            dsi2 = op->dat[ndsi2];
        }
    }
    else
    {
        lauto = 1;
    }

    if (dsi2->nx != dsi->nx || dsi2->ny != dsi->ny)
        win_printf_OK("Cannot preform convolution\nYour data sets have different number of points !");

    nf = dsi->nx;

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");


    for (i = 0; i < nf; i++)
    {
        for (j = 0; j < nf; j++)
        {
            ds->yd[i] += dsi->yd[j] * dsi2->yd[(nf - j + i) % nf] ;
        }

        ds->xd[i] = dsi->xd[i];
    }

    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "Convolution of ds %d with %d", op->cur_dat, ndsi2);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}




int do_fft_convolution_on_ds(void)
{
    register int i;
    O_p *op = NULL;
    int nf, ldeconv, n_max = 0, lauto, lnorma;
    d_s *ds, *dsi, *dsi2;
    pltreg *pr = NULL;
    static int ndsi2 = 0, deconv = 1, no_auto = 1, norma = 1;
    static float max_gain = 100;
    char question[2048], *tmpc = NULL;
    double    *in1 = NULL, *in2 = NULL, re, im, norm, max, min;
    fftw_complex  *outc1 = NULL, *outc2 = NULL;
    fftw_plan     plan1;     // fft plan for fftw3

    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine computes convolution or deconvolution \nof data sets in Fourier space");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    dsi2 = dsi;

    if (op->n_dat > 1)
    {
        i = win_scanf("Do you want to perform auto-convolution %R\n"
                      "or convolution with a second data set %r\n"
                      "Do you want to perform  convolution %R deconvolution %r\n"
                      "For deconvolution specify the maximum amplification %12f\n", &no_auto, &deconv, &max_gain);

        if (i == CANCEL)    return 0;

        lauto = (no_auto) ? 0 : 1;

        if (lauto == 0)
        {
            ndsi2 = (op->cur_dat + 1) % op->n_dat;

            if (op->n_dat < 8)
            {
                snprintf(question, sizeof(question), "First selected data set number = %d \n"
                         "specify the number of the second data set "
                         "for convolution\n0-> %%R 1->%%r", op->cur_dat);

                for (i = 2; i < op->n_dat; i++)
                {
                    tmpc = strdup(question);
                    snprintf(question, sizeof(question), "%s %d->%%r", tmpc, i);
                    free(tmpc);
                }

                tmpc = strdup(question);
                snprintf(question, sizeof(question), "%s\nNormalizes second data set %%b\n", tmpc);
                free(tmpc);
                i = win_scanf(question, &ndsi2, &norma);

                if (i == CANCEL) return 0;
            }
            else
            {
                snprintf(question, sizeof(question), "First selected data set number = %d \n"
                         "specify the number of the second data set "
                         "for convolution%%d"
                         "\nNormalizes second data set %%b\n", op->cur_dat);
                i = win_scanf(question, &ndsi2, &norma);

                if (i == CANCEL) return 0;
            }

            dsi2 = op->dat[ndsi2];
        }

        ldeconv = deconv;
        lnorma = norma;
    }
    else
    {
        ldeconv = 0;
        lauto = 1;
        lnorma = 1;
    }

    if (dsi2->nx != dsi->nx || dsi2->ny != dsi->ny)
        win_printf_OK("Cannot preform convolution\nYour data sets have different number of points !");

    nf = dsi->nx;

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");


    in1   = (double *) fftw_malloc(nf * sizeof(double));
    outc1 = (double(*)[2]) fftw_malloc((nf / 2 + 1) * sizeof(fftw_complex));
    in2   = (double *) fftw_malloc(nf * sizeof(double));
    outc2 = (double(*)[2]) fftw_malloc((nf / 2 + 1) * sizeof(fftw_complex));

    if (in1 == NULL || in2 == NULL || outc1 == NULL || outc2 == NULL)
        return win_printf_OK("cannot create fftw arrays !");

    plan1 = fftw_plan_dft_r2c_1d(nf, in1, outc1, FFTW_ESTIMATE);

    for (i = 0, norm = 0; i < nf; i++)
    {
        in1[i] = dsi2->yd[i];
        norm += in1[i];
    }

    for (i = 0; lnorma > 0 && i < nf; i++)
        in1[i] /= norm;

    fftw_execute(plan1);

    for (i = 0; i < (nf / 2 + 1); i++)
    {
        outc2[i][0] = outc1[i][0];
        outc2[i][1] = outc1[i][1];
    }

    //win_printf("fft1");
    for (i = 0; i < nf; i++) in1[i] = dsi->yd[i];

    fftw_execute(plan1);

    //win_printf("fft2");
    if (deconv == 0)
    {
        outc1[0][0] = outc1[0][0] * outc2[0][0];
        outc1[0][1] = 0;

        for (i = 1; i < nf / 2; i++)
        {
            re = outc1[i][0] * outc2[i][0] - outc1[i][1] * outc2[i][1];
            im = outc1[i][0] * outc2[i][1] + outc1[i][1] * outc2[i][0];
            outc1[i][0] = re;
            outc1[i][1] = im;
        }

        outc1[nf / 2][0] = outc1[nf / 2][0] * outc2[nf / 2][0];
        outc1[nf / 2][1] = 0;
    }
    else  // deconvolution
    {
        n_max = 0;

        for (i = 1, max = 0; i < (nf / 2 + 1); i++)
        {
            // we find the maximum of the fft aside DC component
            norm = outc2[i][0] * outc2[i][0] + outc2[i][1] * outc2[i][1];

            if (i == 1 || norm > max) max = norm;
        }

        min = max / (max_gain * max_gain);

        for (i = 1; i < nf / 2; i++)
        {
            re = outc1[i][0] * outc2[i][0] + outc1[i][1] * outc2[i][1];
            im = outc1[i][1] * outc2[i][0] - outc1[i][0] * outc2[i][1];
            norm = outc2[i][0] * outc2[i][0] + outc2[i][1] * outc2[i][1];

            if (norm < min)
            {
                outc1[i][0] = re / min;
                outc1[i][1] = im / min;
                n_max++;
            }
            else
            {
                outc1[i][0] = re / norm;
                outc1[i][1] = im / norm;
            }
        }

        min = sqrt(min);

        if (fabs(outc2[0][0]) < min)      n_max++;

        outc1[0][0] = (fabs(outc2[0][0]) < min) ? outc1[0][0] / min : outc1[0][0] / outc2[0][0];
        outc1[0][1] = 0;

        if (fabs(outc2[nf / 2][0]) < min)   n_max++;

        outc1[nf / 2][0] = (fabs(outc2[nf / 2][0]) < min) ? outc1[nf / 2][0] / min : outc1[nf / 2][0] / outc2[nf / 2][0];
        outc1[nf / 2][1] = 0;
    }


    //win_printf("fft *");
    fftw_destroy_plan(plan1);
    plan1 = fftw_plan_dft_c2r_1d(nf, outc1, in1, FFTW_ESTIMATE);
    fftw_execute(plan1);

    for (i = 0; i < nf; i++)
    {
        ds->yd[i] = (float)(in1[i] / nf);
        ds->xd[i] = dsi->xd[i];
    }

    //win_printf("fft-1");
    fftw_destroy_plan(plan1);
    fftw_free(in1);
    fftw_free(outc1);
    fftw_free(in2);
    fftw_free(outc2);
    inherit_from_ds_to_ds(ds, dsi);

    if (deconv == 0)  set_ds_treatement(ds, "Convolution by fft of ds %d with %d", op->cur_dat, ndsi2);
    else  set_ds_treatement(ds, "De-Convolution by fft of ds %d with %d with %d modes minimized", op->cur_dat, ndsi2,
                                n_max);

    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


int do_fftw_derivate_on_ds(void)
{
    register int i;
    O_p *op = NULL;
    int nf;
    d_s *ds, *dsi;
    pltreg *pr = NULL;
    double    *in1 = NULL, re, im, norm1;
    fftw_complex  *outc1 = NULL;
    fftw_plan     plan1;     // fft plan for fftw3

    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine computes correlation  \nof data sets in Fourier space");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    nf = dsi->nx;

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");


    in1   = (double *) fftw_malloc(nf * sizeof(double));
    outc1 = (double (*) [2]) fftw_malloc((nf / 2 + 1) * sizeof(fftw_complex));

    if (in1 == NULL || outc1 == NULL)
        return win_printf_OK("cannot create fftw arrays !");

    plan1 = fftw_plan_dft_r2c_1d(nf, in1, outc1, FFTW_ESTIMATE);

    for (i = 0; i < nf; i++) in1[i] = dsi->yd[i];

    fftw_execute(plan1);

    norm1 = (M_PI * 2) / nf;

    for (i = 1; i < nf / 2; i++)
    {
        re = outc1[i][0];
        im = outc1[i][1];
        outc1[i][0] = -im * i * norm1;
        outc1[i][1] = re * i * norm1;
    }

    outc1[nf / 2][0] = outc1[nf / 2][1] = 0;
    outc1[0][0] = outc1[0][1] = 0;

    //win_printf("fft *");
    fftw_destroy_plan(plan1);
    plan1 = fftw_plan_dft_c2r_1d(nf, outc1, in1, FFTW_ESTIMATE);
    fftw_execute(plan1);

    for (i = 0; i < nf; i++)
    {
        ds->yd[i] = (float)(in1[i] / nf);
        ds->xd[i] = dsi->xd[i];
    }

    //win_printf("fft-1");
    fftw_destroy_plan(plan1);
    fftw_free(in1);
    fftw_free(outc1);
    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "Derivative by fftw of ds %d", op->cur_dat);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;

}




int do_fft_derivate_on_ds(void)
{
    register int i;
    O_p *op = NULL;
    int nf;
    d_s *ds, *dsi;
    pltreg *pr = NULL;
    float  re, im, norm1, tmpf, ms, md;


    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine computes correlation  \nof data sets in Fourier space");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    nf = dsi->nx;

    if (fft_init(nf)) return win_printf_OK("cannot init fft");

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    for (i = 0; i < nf; i++) ds->yd[i] = dsi->yd[i];

    realtr1(nf, ds->yd);
    fft(nf, ds->yd, 1);
    realtr2(nf, ds->yd, 1);

    for (i = 0, ms = md = 0; i < nf; i += 2)
    {
        tmpf = ds->yd[i] * ds->yd[i] + ds->yd[i + 1] * ds->yd[i + 1];
        ms += tmpf;
        md += tmpf * i * i;  // we compute derivative amplitude
    }

    md *= (M_PI * M_PI) / (nf * nf);


    norm1 = (M_PI * 2) / nf;

    for (i = 1; i < nf / 2; i++)
    {
        re = ds->yd[2 * i];
        im = ds->yd[2 * i + 1];
        ds->yd[2 * i] = -im * i * norm1;
        ds->yd[2 * i + 1] = re * i * norm1;
    }

    ds->yd[0] = ds->yd[1] = 0;
    realtr2(nf, ds->yd, -1);
    fft(nf, ds->yd, -1);
    realtr1(nf, ds->yd);
    //win_printf("fft *");

    for (i = 0; i < nf; i++)
        ds->xd[i] = dsi->xd[i];

    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "Derivative by fft of ds %d ms = %g md = %g", op->cur_dat, ms, md);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;

}


int do_fft_correlation_on_ds(void)
{
    register int i;
    O_p *op = NULL;
    int nf,  lauto, lnorma;
    d_s *ds, *dsi, *dsi2;
    pltreg *pr = NULL;
    static int ndsi2 = 0,  no_auto = 1, norma = 1;
    char question[2048], *tmpc = NULL;
    double    *in1 = NULL, *in2 = NULL, re, im, norm1, norm2;
    fftw_complex  *outc1 = NULL, *outc2 = NULL;
    fftw_plan     plan1;     // fft plan for fftw3

    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine computes correlation  \nof data sets in Fourier space");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    dsi2 = dsi;

    if (op->n_dat > 1)
    {
        i = win_scanf("Do you want to perform auto-correlation %R\n"
                      "or correlation with a second data set %r\n"
                      , &no_auto);

        if (i == CANCEL)    return 0;

        lauto = (no_auto) ? 0 : 1;

        if (lauto == 0)
        {
            ndsi2 = (op->cur_dat + 1) % op->n_dat;

            if (op->n_dat < 8)
            {
                snprintf(question, sizeof(question), "First selected data set number = %d \n"
                         "specify the number of the second data set "
                         "for convolution\n0-> %%R 1->%%r", op->cur_dat);

                for (i = 2; i < op->n_dat; i++)
                {
                    tmpc = strdup(question);
                    snprintf(question, sizeof(question), "%s %d->%%r", tmpc, i);
                    free(tmpc);
                }

                tmpc = strdup(question);
                snprintf(question, sizeof(question), "%s\nNormalizes second data set %%b\n", tmpc);
                free(tmpc);
                i = win_scanf(question, &ndsi2, &norma);

                if (i == CANCEL) return 0;
            }
            else
            {
                snprintf(question, sizeof(question), "First selected data set number = %d \n"
                         "specify the number of the second data set "
                         "for correlation%%d"
                         "\nNormalizes second data set %%b\n", op->cur_dat);
                i = win_scanf(question, &ndsi2, &norma);

                if (i == CANCEL) return 0;
            }

            dsi2 = op->dat[ndsi2];
        }

        lnorma = norma;
    }
    else
    {
        lauto = 1;
        lnorma = 1;
    }

    if (dsi2->nx != dsi->nx || dsi2->ny != dsi->ny)
        win_printf_OK("Cannot preform correlation\nYour data sets have different number of points !");

    nf = dsi->nx;

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");


    in1   = (double *) fftw_malloc(nf * sizeof(double));
    outc1 = (double (*)[2]) fftw_malloc((nf / 2 + 1) * sizeof(fftw_complex));
    in2   = (double *) fftw_malloc(nf * sizeof(double));
    outc2 = (double (*)[2]) fftw_malloc((nf / 2 + 1) * sizeof(fftw_complex));

    if (in1 == NULL || in2 == NULL || outc1 == NULL || outc2 == NULL)
        return win_printf_OK("cannot create fftw arrays !");

    plan1 = fftw_plan_dft_r2c_1d(nf, in1, outc1, FFTW_ESTIMATE);

    for (i = 0; i < nf; i++)  in1[i] = dsi2->yd[i];

    fftw_execute(plan1);

    for (i = 0; i < (nf / 2 + 1); i++)
    {
        outc2[i][0] = outc1[i][0];
        outc2[i][1] = outc1[i][1];
    }

    norm2 = outc2[0][0] * outc2[0][0];

    for (i = 1; i < (nf / 2 + 1); i++)
        norm2 += outc2[i][0] * outc2[i][0] + outc2[i][1] * outc2[i][1];

    norm2 = sqrt(norm2);

    //win_printf("fft1");
    for (i = 0; i < nf; i++) in1[i] = dsi->yd[i];

    fftw_execute(plan1);
    //win_printf("fft2");
    norm1 = outc1[0][0] * outc1[0][0];

    for (i = 1; i < (nf / 2 + 1); i++)
        norm1 += outc1[i][0] * outc1[i][0] + outc1[i][1] * outc1[i][1];

    norm1 = sqrt(norm1);

    outc1[0][0] = outc1[0][0] * outc2[0][0];
    outc1[0][1] = 0;

    for (i = 1; i < nf / 2; i++)
    {
        re = outc1[i][0] * outc2[i][0] + outc1[i][1] * outc2[i][1];
        im = outc1[i][0] * outc2[i][1] - outc1[i][1] * outc2[i][0];
        outc1[i][0] = re;
        outc1[i][1] = im;
    }

    outc1[nf / 2][0] = outc1[nf / 2][0] * outc2[nf / 2][0];
    outc1[nf / 2][1] = 0;
    outc1[0][0] /= norm1 * norm2;
    outc1[0][1] /= norm1 * norm2;

    for (i = 1; i < (nf / 2 + 1); i++)
    {
        outc1[i][0] /= norm1 * norm2;
        outc1[i][1] /= norm1 * norm2;
    }

    //win_printf("fft *");
    fftw_destroy_plan(plan1);
    plan1 = fftw_plan_dft_c2r_1d(nf, outc1, in1, FFTW_ESTIMATE);
    fftw_execute(plan1);

    for (i = 0; i < nf / 2; i++)
    {
        ds->yd[i + nf / 2] = (float)(in1[i] / 2);
        ds->xd[i + nf / 2] = dsi->xd[i];
        ds->yd[i] = (float)(in1[nf / 2 + i] / 2);
        ds->xd[i] = dsi->xd[i] - dsi->xd[nf / 2];

    }

    //win_printf("fft-1");
    fftw_destroy_plan(plan1);
    fftw_free(in1);
    fftw_free(outc1);
    fftw_free(in2);
    fftw_free(outc2);
    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "Correlation by fft of ds %d with %d", op->cur_dat, ndsi2);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}








int do_spectre_to_impulse_response_on_ds(void)
{
    register int i;
    O_p *op = NULL;
    int nf;
    d_s *ds, *dsi;
    pltreg *pr = NULL;
    double    *in1 = NULL;
    fftw_complex  *outc1 = NULL;
    fftw_plan     plan1;     // fft plan for fftw3

    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine computes the inpulse response from a spectrum");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    nf = 2 * (dsi->nx / 2);
    nf = 2 * nf;

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    in1   = (double *) fftw_malloc(nf * sizeof(double));
    outc1 = (double (*)[2]) fftw_malloc((dsi->nx) * sizeof(fftw_complex));

    if (in1 == NULL || outc1 == NULL)
        return win_printf_OK("cannot create fftw arrays !");

    plan1 = fftw_plan_dft_c2r_1d(nf, outc1, in1, FFTW_ESTIMATE);

    for (i = 0; i < dsi->nx; i++)
    {
        outc1[i][0] = dsi->yd[i];
        outc1[i][1] = 0;
    }

    fftw_execute(plan1);

    for (i = 0; i < nf; i++)
    {
        ds->yd[i] = 2 * (float)in1[i] / nf;
        ds->xd[i] = i;
    }

    fftw_destroy_plan(plan1);
    fftw_free(in1);
    fftw_free(outc1);
    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "Finite inpulse response from spectrum");
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}







int translate_fft(float *x, int nx, float dx, float *xt)
{
    register int i, j;
    double phase, co, si, re, im;

    phase = -2 * M_PI * dx / nx;

    for (i = 1 ; i < nx / 2 ; i++)
    {
        j = 2 * i;
        co = cos(phase * i);
        si = sin(phase * i);
        re = (double) x[j];
        im = (double) x[j + 1];
        xt[j]   = (float)(co * re - si * im);
        xt[j + 1] = (float)(co * im + si * re);
    }

    xt[0] = x[0];
    xt[1] = x[1];
    return 0;
}


int do_translate_periodic_ds(void)
{
    register int i, j;
    O_p *op = NULL;
    int nf;
    d_s *ds, *dsi;
    pltreg *pr = NULL;
    float  *yt = NULL;
    static float dx = 0.5;
    double phase, co, si, re, im;

    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    if (fft_init(dsi->nx))
        return win_printf_OK("Cannot init fft");

    nf = 2 * dsi->nx; /* this is the number of points in the data set */


    i = win_scanf("Decalage %f", &dx);

    if (i == CANCEL) return D_O_K;

    yt = (float *)calloc(nf, sizeof(float));

    if (yt == NULL)       return win_printf_OK("Cannot alloc");

    if ((ds = create_and_attach_one_ds(op, nf / 2, nf / 2, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    for (j = 0; j < dsi->nx; j++)
    {
        yt[2 * j] = dsi->yd[j];
    }

    fft(nf, yt, 1);
    phase = -2 * M_PI * dx / dsi->nx;

    for (i = 1 ; i < dsi->nx / 2 ; i++)
    {
        j = 2 * i;
        co = cos(phase * i);
        si = sin(phase * i);
        re = (double) yt[j];
        im = (double) yt[j + 1];
        yt[j]   = (float)(co * re - si * im);
        yt[j + 1] = (float)(co * im + si * re);
    }

    for (i = 1 ; i <= dsi->nx / 2 ; i++)
    {
        j = nf - 2 * i;
        co = cos(phase * (-i));
        si = sin(phase * (-i));
        re = (double) yt[j];
        im = (double) yt[j + 1];
        yt[j]   = (float)(co * re - si * im);
        yt[j + 1] = (float)(co * im + si * re);
    }

    if (fft_init(nf))
        return win_printf_OK("Cannot init fft");

    fft(nf, yt, -1);

    for (j = 0; j < ds->nx; j++)
    {
        ds->yd[j] = yt[2 * j];
        ds->xd[j] = dsi->xd[j];
    }

    /* now we must do some house keeping */
    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "Projection on poly 5");
    free(yt);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


int do_expand_mode_of_periodic_ds(void)
{
    register int j, k;
    O_p *op = NULL;
    int nf;
    d_s *ds, *dsi;
    pltreg *pr = NULL;
    float  *yts = NULL, *yt = NULL;


    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    if (fft_init(dsi->nx))
        return win_printf_OK("Cannot init fft");

    nf = 32 * dsi->nx; /* this is the number of points in the data set */

    yts = (float *)calloc(2 * dsi->nx, sizeof(float));
    yt = (float *)calloc(nf, sizeof(float));

    if (yt == NULL)       return win_printf_OK("Cannot alloc");

    if ((ds = create_and_attach_one_ds(op, nf / 2, nf / 2, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    for (j = 0; j < dsi->nx; j++)
    {
        yts[2 * j] = dsi->yd[j];
    }

    win_printf("bef fft");
    fft(2 * dsi->nx, yts, 1);

    if (fft_init(nf))
        return win_printf_OK("Cannot init fft");

    yt[0] = yts[0];
    yt[1] = yts[1];

    fft(nf, yt, -1);

    for (j = 0; j < ds->nx; j++)
    {
        ds->yd[j] = yt[2 * j];
        ds->xd[j] = (float)j / 16;
    }

    /* now we must do some house keeping */
    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "Mode 0");

    for (k = 1; k < 16 && k < dsi->nx / 2; k++)
    {
        //win_printf("mode %d",k);
        for (j = 0; j < nf; j++) yt[j] = 0;

        yt[2 * k] = yts[2 * k];
        yt[2 * k + 1] = yts[2 * k + 1];
        yt[2 * nf - 2 * k] = yts[2 * dsi->nx - 2 * k];
        yt[2 * nf - 2 * k + 1] = yts[2 * dsi->nx - 2 * k + 1];
        fft(nf, yt, -1);

        if ((ds = create_and_attach_one_ds(op, nf / 2, nf / 2, 0)) == NULL)
            return win_printf_OK("cannot create plot !");

        inherit_from_ds_to_ds(ds, dsi);
        set_ds_treatement(ds, "Mode %d", k);

        for (j = 0; j < ds->nx; j++)
        {
            ds->yd[j] = yt[2 * j];
            ds->xd[j] = (float)j / 16;
        }
    }

    //free(yt);
    //free(yts);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}






int do_shift_data_poly1_ds(void)
{
    register int i, j;
    O_p *op = NULL;
    int nf;
    static float factor = .5;
    static int per = 1;
    d_s *ds, *dsi;
    pltreg *pr = NULL;
    float xt[4], yt[4], f;


    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine shift the x coordinate"
                             "of a data set by a number");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    nf = dsi->nx; /* this is the number of points in the data set */
    i = win_scanf("By what amount do you want to shift your data %8f\n"
                  "Periodic data %R yes %r no", &factor, &per);

    if (i == CANCEL)  return OFF;

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    f = -factor;

    for (j = 0; j < nf; j++)
    {
        ds->xd[j] = dsi->xd[j] ;
        yt[0] = dsi->yd[j % nf];

        if (f >= 0)
        {
            xt[0] = 0;
            xt[1] = 1;
            yt[1] = dsi->yd[(j + 1) % nf];
        }
        else
        {
            xt[0] = 0;
            xt[1] = -1;
            yt[1] = dsi->yd[(j + nf - 1) % nf];
        }

        ds->yd[j] = poly_2_in_x0(xt, yt, f);
        ds->yd[j] += poly_2_in_x1(xt, yt, f);
    }

    /* now we must do some house keeping */
    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "y shifted by %f by poly 1", factor);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}



int do_shift_data_poly2_ds(void)
{
    register int i, j;
    O_p *op = NULL;
    int nf;
    static float factor = .5;
    static int per = 1;
    d_s *ds, *dsi;
    pltreg *pr = NULL;
    float xt[4], yt[4], f;


    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine shift the x coordinate"
                             "of a data set by a number");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    nf = dsi->nx; /* this is the number of points in the data set */
    i = win_scanf("By what amount do you want to shift your data %8f\n"
                  "Periodic data %R yes %r no", &factor, &per);

    if (i == CANCEL)  return OFF;

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    for (j = 0; j < nf; j++)
    {
        ds->xd[j] = dsi->xd[j] ;
        xt[0] = -1;
        xt[1] = 0;
        xt[2] = 1;
        yt[0] = dsi->yd[(j + nf - 1) % nf];
        yt[1] = dsi->yd[j % nf];
        yt[2] = dsi->yd[(j + 1) % nf];
        f = (factor < 0) ? -factor : factor - 1;
        ds->yd[j] = poly_3_in_x_1(xt, yt, f);
        ds->yd[j] += poly_3_in_x0(xt, yt, f);
        ds->yd[j] += poly_3_in_x1(xt, yt, f);
    }

    /* now we must do some house keeping */
    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "y shifted by %f by poly 2", factor);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


int do_shift_data_poly3_ds(void)
{
    register int i, j;
    O_p *op = NULL;
    int nf;
    static float factor = .5;
    static int per = 1;
    d_s *ds, *dsi;
    pltreg *pr = NULL;
    float xt[4], yt[4], f;


    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine shift the x coordinate"
                             "of a data set by a number");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    nf = dsi->nx; /* this is the number of points in the data set */
    i = win_scanf("By what amount do you want to shift your data %8f\n"
                  "Periodic data %R yes %r no", &factor, &per);

    if (i == CANCEL)  return OFF;

    if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    for (j = 0; j < nf; j++)
    {
        ds->xd[j] = dsi->xd[j] ;
        xt[0] = -1;
        xt[1] = 0;
        xt[2] = 1;
        xt[3] = 2;

        if (factor < 0)
        {
            yt[0] = dsi->yd[(j + nf - 1) % nf];
            yt[1] = dsi->yd[j % nf];
            yt[2] = dsi->yd[(j + 1) % nf];
            yt[3] = dsi->yd[(j + 2) % nf];
            f =  -factor;
            ds->yd[j] = poly_3_in_x_1(xt, yt, f);
            ds->yd[j] += poly_3_in_x0(xt, yt, f);
            ds->yd[j] += poly_3_in_x1(xt, yt, f);
        }
        else
        {
            yt[0] = dsi->yd[(j + nf - 2) % nf];
            yt[1] = dsi->yd[(j + nf - 1) % nf];
            yt[2] = dsi->yd[j % nf];
            yt[3] = dsi->yd[(j + 1) % nf];
            f =  1 - factor;
            ds->yd[j] = poly_3_in_x_1(xt, yt, f);
            ds->yd[j] += poly_3_in_x0(xt, yt, f);
            ds->yd[j] += poly_3_in_x1(xt, yt, f);
        }
    }

    /* now we must do some house keeping */
    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "y shifted by %f by poly 3", factor);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}



int do_shift_data_rescale_plot(void)
{
    register int i, j;
    O_p *op = NULL, *opn = NULL;
    int nf;
    static float factor = 1;
    d_s *ds, *dsi;
    pltreg *pr = NULL;


    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number and place it in a new plot");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        return win_printf_OK("cannot find data");

    nf = dsi->nx; /* this is the number of points in the data set */
    i = win_scanf("By what factor do you want to multiply y %f", &factor);

    if (i == CANCEL)  return OFF;

    if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    ds = opn->dat[0];

    for (j = 0; j < nf; j++)
    {
        ds->yd[j] = factor * dsi->yd[j];
        ds->xd[j] = dsi->xd[j];
    }

    /* now we must do some house keeping */
    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "y multiplied by %f", factor);
    set_plot_title(opn, "Multiply by %f", factor);

    if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);

    if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);

    opn->filename = Transfer_filename(op->filename);
    uns_op_2_op(opn, op);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}



int read_file_and_do_histo(void)
{
    register int i, j;
    O_p  *opn = NULL;
    static int start = 0, size = 65536;
    d_s *ds;
    pltreg *pr = NULL;
    static char fullfile[512], *fu = NULL;
    FILE *fp;
    unsigned char uch;

    if (updating_menu_state != 0)  return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number and place it in a new plot");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
        return win_printf_OK("cannot find data");


    fu = open_one_file_config("Load file", NULL, "(*.*)\0*.*\0\0", "SHIFT_DATA", "last_loaded");

    if (fu == NULL)
        return 0;

    fp = fopen(fullfile, "rb+");

    if (fp == NULL) return win_printf_OK("cannot open file:\n%s", backslash_to_slash(fullfile));


    i = win_scanf("Character histogram starting at %d over %d", &start, &size);

    if (i == CANCEL)  return OFF;

    if ((opn = create_and_attach_one_plot(pr, 256, 256, 0)) == NULL)
        return win_printf_OK("cannot create plot !");

    ds = opn->dat[0];


    fseek(fp, start, SEEK_SET);      // we go to file end

    for (j = 0; j < size; j++)
    {
        fread(&uch, sizeof(char), 1, fp);
        ds->yd[uch] += 1 ;
    }

    fclose(fp);

    for (j = 0; j < 256; j++)       ds->xd[j] = j;

    /* now we must do some house keeping */
    set_plot_title(opn, "Histo from %d to %d", start, size);
    /* refisplay the entire plot */
    refresh_plot(pr, UNCHANGED);
    return D_O_K;

}



MENU *shift_data_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)   return mn;

    add_item_to_menu(mn, "data set shifted in X (poly 1)", do_shift_data_poly1_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "data set shifted in X (poly 2)", do_shift_data_poly2_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "data set shifted in X (poly 3)", do_shift_data_poly3_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "Projection on poly 5", do_project_on_polynome_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "Draw ortho poly", do_draw_ortho_polynome_op, NULL, 0, NULL);
    add_item_to_menu(mn, "16x expand", do_expand_periodic_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "Draw modes", do_expand_mode_of_periodic_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "Translate ds", do_translate_periodic_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "correlation in real space", do_simple_correlation_on_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "convolution in real space", do_simple_convolution_on_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "correlation by FFT", do_fft_correlation_on_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "(de)convolution in Fourier space", do_fft_convolution_on_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "Derivate by FFTW", do_fftw_derivate_on_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "Derivate by FFT", do_fft_derivate_on_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "Spectre -> Inpulse Response", do_spectre_to_impulse_response_on_ds, NULL, 0, NULL);
    add_item_to_menu(mn, "Histo of ch", read_file_and_do_histo, NULL, 0, NULL);

    //add_item_to_menu(mn,"plot rescale in Y", do_shift_data_rescale_plot,NULL,0,NULL);
    return mn;
}

int shift_data_main(int argc, char **argv)
{
    add_plot_treat_menu_item("shift_data", NULL, shift_data_plot_menu(), 0, NULL);


    return D_O_K;
}

int shift_data_unload(int argc, char **argv)
{
    remove_item_to_menu(plot_treat_menu, "shift_data", NULL, NULL);
    return D_O_K;
}
#endif


