typedef struct _tracking_data
{
    int image_num;
    long long image_t;                                  // the image absolute time
    unsigned long image_dt;                               // the time spent in the previous function call
    float image_zmag, image_rot_mag, image_obj_pos;     // the magnet position, the objective position
    float   *x;
    float   *y;
    float   *z;
    char    *n_l;           // not lost indicator
                                              
} tracking_data;


typedef struct _acquisition_plt
{
	int		n_bead;		/* number of data sets	*/
	int		n;			/* number of points in each data set	*/
	int		m;			/* size allocated for each data set */
	int		x_index;	/* current x_index */
	int		y_index;	/* current y_index */
	char	*source;	/* name of the file	*/
	char	**name;		/* names of data sets */
	char    **calib_image_name; /*calibration image file*/
	tracking_data	*data;		/* data	*/
	un_s	*x_unit;			/*  unit sets */
	un_s	*y_unit;			/*  unit sets */
	un_s	*z_unit;			/*  unit sets */
	un_s	*t_unit;			/*  unit sets */
	int     *n_x_unit, *m_x_unit, *c_x_unit;
	int     *n_y_unit, *m_y_unit, *c_y_unit;
	int     *n_z_unit, *m_z_unit, *c_z_unit;
	unsigned long time;		/* date of creation */
} acq_plt;


#define BUFFER_PACK_SIZE  64//mettre multiple lie aqusition rate
#define SAVING_INTERVAL 100 //lie aquisition rate


int init_absolute_counter = 0, saving_absolute_counter = 0;
int init_pos_in_buffer = 0, saving_pos_in_buffer = 0;
char  filename_tr[512];
int number_of_images = 1024;
extern g_track *track_info;
//g_track *track_info;


int save_header(char *filename, acq_plt *acqdata, int number_of_images);
int save_data_to_disk_from_rolling_buffer(int starting_index,int end_index,char *filename,g_track *track_information);
int prepare_header(char *filename, acq_plt *acqdata, int num_points);
char *return_acq_file_name(void);
int aquire_and_save_data_in_real_time(void);
int save_data_to_disk_from_rolling_buffer_tr_format(int starting_index,int end_index,char *filename_tr,g_track *track_information);
int test_background_saving(void);
int kill_timer(void);
void CALLBACK automatic_saving(HWND hwnd, UINT uMsg, UINT_PTR idEvent,  DWORD dwTime);
int start_save_acq_ds(void);
int save_header(char *filename, acq_plt *acqdata, int number_of_images);
int prepare_header(char *filename, acq_plt *acqdata, int num_points);
int do_load_track_data(void);
int push_bin_track_data(FILE *fp, acq_plt* acqdata);
int read_acq_file(void);
acq_plt *read_acq_data(acq_plt **acq_data,char *filename);
char *return_acq_file_name(void);
multi_d_s *convert_acq_plt_to_xvplt(acq_plt *acqplt);
int plot_acqplt(void);

