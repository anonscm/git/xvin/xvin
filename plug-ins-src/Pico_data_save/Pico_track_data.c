/*
 *    Plug-in program for data treatement and saving in Xvin.
 *
 *    V. Croquette
 *    JF Allemand
 */
#ifndef _PICO_TRACK_DATA_C_
#define _PICO_TRACK_DATA_C_


# include "allegro.h"
# include "winalleg.h"
//# include "ifcapi.h"
# include <windowsx.h>
# include "ctype.h"
# include "xvin.h"
//# include "brown.h"
# include "track_util.h"
# include "trackBead.h"
# include  "xvplot.h"


# define BUILDING_PLUGINS_DLL
# include "Pico_track_data.h"


//changer reference au fichier de config

extern g_track *track_info ;
int init_absolute_counter = 0, saving_absolute_counter = 0;
int init_pos_in_buffer = 0, saving_pos_in_buffer = 0;
char  filename_tr[512];
int number_of_images = 1024;
//static int num_of_files = 0;



void *rearrange_data_for_saving(int starting_index,int *end_index, int *size)
{
  int  i=0 , j=0 , k=0; 
  int size_of_track_data =0;
  void *track_data_to_save = NULL;
    
  int im_i_shift = 0;
  int im_t_shift = sizeof(int);
  int im_dt_shift = sizeof(int)+sizeof(long long int);
  int im_zmag_shift = sizeof(int)+sizeof(long long int)+sizeof(unsigned long );
  int im_rot_mag_shift = sizeof(int)+sizeof(long long int)+sizeof(unsigned long )+sizeof(float);
  int im_obj_pos_shift = sizeof(int)+sizeof(long long int)+sizeof(unsigned long )+2*sizeof(float);
  int im_pos_shift = sizeof(int)+sizeof(long long int)+sizeof(unsigned long )+3*sizeof(float);
  int i_shift = sizeof(float)*3+sizeof(char);
   
  if (starting_index > *end_index) *end_index = *end_index + TRACKING_BUFFER_SIZE;/*in principle OK*/
  if (track_info == NULL)
    {
      win_printf_OK("We lack track info to save");
      return NULL;
    }
  if (track_info->n_b <= 0) 
    {
      win_printf_OK("We lack track info to save");
      return NULL;
    }
  
  size_of_track_data = sizeof(int) + sizeof(long long int) + sizeof(unsigned long ) + sizeof(float)*3 + track_info->n_b*(3*sizeof(float)+sizeof(char));
  *size = size_of_track_data;
  draw_bubble(screen,0,250,50,"difference %d ",*end_index-starting_index);
  track_data_to_save = (void *)realloc(track_data_to_save,(*end_index-starting_index)*size_of_track_data);
  if (track_data_to_save == NULL) 
    {
      win_printf_OK("Memory alloc pb!");
      return NULL;
    }

  for ( k = 0 , j = starting_index; k < (*end_index-starting_index) && j < *end_index; k++, j++)
    {
      memcpy(track_data_to_save + im_i_shift + k*size_of_track_data,&(track_info->imi[j%TRACKING_BUFFER_SIZE]),sizeof(int));
      memcpy(track_data_to_save + im_t_shift + k*size_of_track_data,&(track_info->imt[j%TRACKING_BUFFER_SIZE]),sizeof(long long int));
      memcpy(track_data_to_save + im_dt_shift + k*size_of_track_data ,&(track_info->imdt[j%TRACKING_BUFFER_SIZE]),sizeof(unsigned long ));
      memcpy(track_data_to_save + im_zmag_shift + k*size_of_track_data,&(track_info->zmag[j%TRACKING_BUFFER_SIZE]),sizeof(float));
      memcpy(track_data_to_save + im_rot_mag_shift + k*size_of_track_data,&track_info->rot_mag[j%TRACKING_BUFFER_SIZE],sizeof(float));
      memcpy(track_data_to_save + im_obj_pos_shift + k*size_of_track_data,&track_info->obj_pos[j%TRACKING_BUFFER_SIZE],sizeof(float));
      for (i = 0; i < track_info->n_b ; i++)
	{
	  memcpy(track_data_to_save + im_pos_shift + i*i_shift + k*size_of_track_data,&(track_info->bd[i]->x[j%TRACKING_BUFFER_SIZE]),sizeof(float));
	  memcpy(track_data_to_save + im_pos_shift + i*i_shift + sizeof(float) + k*size_of_track_data,&(track_info->bd[i]->y [j%TRACKING_BUFFER_SIZE]),sizeof(float));
	  memcpy(track_data_to_save + im_pos_shift + i*i_shift + 2*sizeof(float) +k*size_of_track_data,&(track_info->bd[i]->z[j%TRACKING_BUFFER_SIZE]),sizeof(float));
	  memcpy(track_data_to_save + im_pos_shift + i*i_shift + 3*sizeof(float) + k*size_of_track_data,&(track_info->bd[i]->n_l[j%TRACKING_BUFFER_SIZE]),sizeof(char));
	}
    }	        
  return track_data_to_save;
}

int save_data_to_disk_from_rolling_buffer_tr_format(int starting_index,int end_index,char *filename_tr)
{
  FILE *fp;
  int size_of_track_data =0;
  void *track_data_to_save = NULL;
  int nw = -1;


  track_data_to_save = rearrange_data_for_saving( starting_index, &end_index, &size_of_track_data);
  if (track_data_to_save == NULL) return win_printf_OK("Rearrange data failed");
     
  fp = fopen (filename_tr, "ab");
  if ( fp == NULL ) 	
  {    
    return win_printf_OK("Cannot open the file!");
  }

  nw = fwrite (track_data_to_save, size_of_track_data, end_index - starting_index, fp)	;
  if (fclose(fp) != 0) 
    {
      return win_printf_OK("Failure in closure!");
    }    
  //if (nw !=  end_index - starting_index)
  //  {
  //      return win_printf_OK("error in saving nb = %d\n  %d",track_info->n_b,nw);
  // }
      
  /*Check the closure*/
  fp = fopen (filename_tr, "r+b");
  if ( fp == NULL ) 	
  {    
    return win_printf_OK("Cannot open the file!");
  }

  rewind(fp); 
  //test = fseek(fp,0L,SEEK_SET);
  //win_printf("seek %d",fseek(fp,0L,SEEK_SET));
  fprintf(fp, "-n points %d\n",   saving_absolute_counter-init_absolute_counter + end_index - starting_index);//Check
  if (fclose(fp) != 0) 
    {
      return win_printf_OK("Failure in closure!");
    }
  if (track_data_to_save != NULL) free(track_data_to_save);
  return D_O_K;
  
}

int save_acq_from_image(char *filename, int starting_image, int number_of_images_to_save)
{
  acq_plt *acqdata = NULL;

  init_absolute_counter = saving_absolute_counter = track_info->imi[track_info->c_i];
  init_pos_in_buffer = saving_pos_in_buffer = track_info->c_i;
  save_header(filename_tr,acqdata,number_of_images);
  SetTimer( win_get_window(),25,SAVING_INTERVAL,automatic_saving);/*sets a background saving*/
  return D_O_K;  
}

int test_background_saving(void)
{
  acq_plt *acqdata = NULL;
  static int number_of_files = 0;
  static char common_name[64]="tracking_data";
  
  
  if(updating_menu_state != 0)	return D_O_K;
  
  //adapter cfg avec filename aussi
  number_of_files = get_config_int("SAVING PicoTwist TRACKING DATA","filenumber",0);
  
  win_scanf("Characteristic string for the filename? %s\n Initial number for file?%6d\n Save how any frames %6d\n",common_name,&number_of_files,&number_of_images);
  sprintf(filename_tr,"%s%d.acq",common_name,number_of_files);
  number_of_files++;
  set_config_int("SAVING PicoTwist TRACKING DATA","filenumber",number_of_files);
  init_absolute_counter = saving_absolute_counter = track_info->imi[track_info->c_i];
  init_pos_in_buffer = saving_pos_in_buffer = track_info->c_i;
  save_header(filename_tr,acqdata,number_of_images);
  SetTimer( win_get_window(),25,SAVING_INTERVAL,automatic_saving);/*sets a background saving*/
  return D_O_K;  
}

int kill_timer(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  if (KillTimer(win_get_window(),  25) == FALSE) return win_printf_OK("Timer not  killed"); 
  return D_O_K;   
}

void CALLBACK automatic_saving(HWND hwnd, UINT uMsg, UINT_PTR idEvent,  DWORD dwTime)
{
  if (( track_info->imi[track_info->c_i] - saving_absolute_counter) > BUFFER_PACK_SIZE )//enough data arrived for writing
    {
      save_data_to_disk_from_rolling_buffer_tr_format(saving_pos_in_buffer,(saving_pos_in_buffer+ BUFFER_PACK_SIZE)%TRACKING_BUFFER_SIZE,filename_tr);
      saving_absolute_counter = saving_absolute_counter + BUFFER_PACK_SIZE;
      saving_pos_in_buffer = (saving_pos_in_buffer + BUFFER_PACK_SIZE) %  TRACKING_BUFFER_SIZE ;
      if ( init_absolute_counter + number_of_images - saving_absolute_counter == 0)//to avoid  a useless new loop
	{
	  kill_timer();
	  saving_absolute_counter = 0;
	  saving_pos_in_buffer = 0;
	}
    }
  
  else if (init_absolute_counter + number_of_images - saving_absolute_counter < BUFFER_PACK_SIZE && track_info->imi[track_info->c_i] > init_absolute_counter +number_of_images && number_of_images > 0)  //not a lot of data to finish saving  and aquisition finished
    {
      save_data_to_disk_from_rolling_buffer_tr_format(saving_pos_in_buffer,( number_of_images  + init_pos_in_buffer)% TRACKING_BUFFER_SIZE,filename_tr);
      saving_absolute_counter = 0;
      saving_pos_in_buffer = 0;
      kill_timer(); //in principle saving is finished here

    }
  return;
}



int start_save_acq_ds(void)
{
  acq_plt *acqdata = NULL;
  static int number_of_files = 0;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  win_scanf("You want to save positions for how many images?%d",&number_of_images);
  
  init_absolute_counter = saving_absolute_counter = track_info->imi[track_info->c_i];
  init_pos_in_buffer = saving_pos_in_buffer = track_info->c_i;
  
  sprintf(filename_tr,"BEAD%d.acq",number_of_files++);
  save_header(filename_tr,acqdata,number_of_images);
  SetTimer( win_get_window(),25,SAVING_INTERVAL,automatic_saving);
  return D_O_K;
}

int save_header(char *filename, acq_plt *acqdata, int number_of_images)
{
  FILE *fp = NULL;
  int i = 0;
  un_s *un;
  
  if (acqdata == NULL)    acqdata = (acq_plt *) calloc(1, sizeof(acq_plt));
  if (acqdata == NULL) return win_printf_OK("Cannot allocate acqdata");
  
  prepare_header(filename,acqdata,number_of_images);
  
  fp = fopen (filename, "w");
  if ( fp == NULL ) 	return win_printf("Cannot open %s!",backslash_to_slash(filename));
  if (acqdata->n > 0)
    fprintf(fp, "-n points %d\n",   acqdata->n);
  else fprintf(fp, "-n points %d\n",   0);//METTRE QUELQUE CHOSE DANS LA TACHE DE FOND
  
  if (acqdata->n_bead >= 0)
    fprintf(fp, "-n beads %d \n",   acqdata->n_bead); 
  /* for (i = 0 ; i < acqdata->n_bead; i++) */
/*     { */
/*     if (acqdata->calib_image_name[i] != NULL) fprintf(fp, "-calib_image_filename %d %s\n",i,acqdata->calib_image_name[i]);     } */
  
  if (acqdata->source  != NULL) fprintf(fp, "-src \"%s\"\n",    acqdata->source);
  
  /*un_s part*/
  /* plutot chercher dans im_TRACK*/  
  if (acqdata->x_unit != NULL)
    {
      un = acqdata->x_unit;			
      if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
	fprintf(fp,"-xus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
      else if (un != NULL && un->type != IS_RAW_U)
	fprintf(fp,"-xus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
      
    }	
  if (acqdata->y_unit != NULL)
    {
      un = acqdata->y_unit;			
      if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
	fprintf(fp,"-yus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
      else if (un != NULL && un->type != IS_RAW_U)
	fprintf(fp,"-yus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
      
    }
  if (acqdata->z_unit != NULL)
    {
    
    un = acqdata->z_unit;			
    if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
    fprintf(fp,"-zus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
    else if (un != NULL && un->type != IS_RAW_U)
    fprintf(fp,"-zus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
    
    }
  if (acqdata->t_unit != NULL)
    {
      
      un = acqdata->t_unit;			
      if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
	fprintf(fp,"-tus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
      else if (un != NULL && un->type != IS_RAW_U)
	fprintf(fp,"-tus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
      
    }
  
  if (acqdata->name[0] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[0]);
  if (acqdata->name[1] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[1]);
  if (acqdata->name[2] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[2]);
  if (acqdata->name[3] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[3]);
  if (acqdata->name[4] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[4]);
  if (acqdata->name[5] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[5]);
  
  for (i = 0 ; i < acqdata->n_bead*4; i++)
    {
      if (acqdata->name[6+i] != NULL) fprintf(fp, "-name \"%s\"\n",acqdata->name[6+i]);
    }
  fprintf (fp, "%c",26);/*separator where data start*/
  if (fclose(fp) != 0) return win_printf("Failure in closure!");
  
  return D_O_K;
}

int prepare_header(char *filename, acq_plt *acqdata, int num_points)
{   
  int i = 0;
  time_t timer;
  //  int k = -1;
  
  
  if (acqdata == NULL) return win_printf("Acq data NULL");
  if (track_info == NULL) return win_printf("Track info NULL");
  acqdata->n_bead = track_info->n_b;		/* number of data sets	*/
  acqdata->n = TRACKING_BUFFER_SIZE;//num_points;   		    	/* number of points in each data set	*/
  /*PREVOIR LECTURE EN COURS AVEC PARAMETRE A -1*/
  
  //acqdata->m = num_points;   			    /* size allocated for each data set */
  acqdata->source = (char *) calloc(strlen(filename+1),sizeof(char));
  acqdata->source = filename;	            /* name of the file	*/
  
  acqdata->time = time(&timer);
  
  acqdata->data = (tracking_data *) calloc(acqdata->n , sizeof(tracking_data ));
  if (acqdata->data == NULL) return 1;
  
  acqdata->data->x = (float *) calloc(acqdata->n_bead , sizeof(float)) ;
  if (acqdata->data->x == NULL) return 1;
  acqdata->data->y = (float *) calloc(acqdata->n_bead , sizeof(float)) ;
  if (acqdata->data->y == NULL) return 1;
  acqdata->data->z = (float *) calloc(acqdata->n_bead , sizeof(float)) ;
  if (acqdata->data->z == NULL) return 1;
  acqdata->data->n_l = (char *) calloc(acqdata->n_bead , sizeof(char)) ;
  if (acqdata->data->n_l == NULL) return 1;
  acqdata->name = (char **) calloc(6+acqdata->n_bead*4, sizeof(char *));
  if (acqdata->name == NULL) return 1;
  for (i = 0; i < 6; i++)
    {
      acqdata->name[i] = (char *) calloc(32, sizeof(char));    
    }
  acqdata->name[0] = "image number";
  acqdata->name[1] = "absolute time";
  acqdata->name[2] = "absolute differential time";
  acqdata->name[3] = "z mag";
  acqdata->name[4] = "rot mag";
  acqdata->name[5] = "obj pos";

/*   for (i = 0 ; i < acqdata->n_bead ; i++)  */
/*     {  */
/*       if (track_info->bd[i]->calib_im_fil->filename != NULL) */
/* 	k++; */
/*     } */
/*   if (k !=0) */
/*     { */
/*       acqdata->calib_image_name = (char **)calloc(k/\*acqdata->n_bead*\/, sizeof(char *));  */
/*       for (i = 0 ; i < acqdata->n_bead ; i++) */
/* 	{ */
/* 	  if (track_info->bd[i]->calib_im_fil->filename != NULL) */
/* 	    { */
/* 	      acqdata->calib_image_name[i] = (char *) calloc(strlen(track_info->bd[i]->calib_im_fil->filename)+1,sizeof(char *)); */
/* 	      strcpy(acqdata->calib_image_name[i],track_info->bd[i]->calib_im_fil->filename); */
/* 	      /\* treat the case where some beads have calibration and not others      *\/ */
/* 	    }  */
/* 	} */
/*     } */

  for (i = 0 ; i < acqdata->n_bead*4 ; i++)
    {
      acqdata->name[6+i] = (char *)calloc(64,sizeof(char));
      
      if(i%4 == 0)    sprintf(acqdata->name[6+i],"xbead%d",(int)(i/4));
      if(i%4 == 1)    sprintf(acqdata->name[6+i],"ybead%d",(int)(i/4));
      if(i%4 == 2)    sprintf(acqdata->name[6+i],"zbead%d",(int)(i/4));
      if(i%4 == 3)    sprintf(acqdata->name[6+i],"nlbead%d",(int)(i/4));
    }
  /* on verra apres char    *x_unit, *y_unit, *t_unit;*/		
  //float   *ax, *dx;
  //un_s	***xu;			/*  unit set */
  //int     *n_xu, *m_xu, *c_xu,  *c_xu_p;
  
  return 0;
}


/**********************************************************************************************************************/
/*                                             LOADING PART                                                          */ 
/**********************************************************************************************************************/ 
int do_load_track_data(void)
{
  extern int parse_track_datalex(acq_plt *acqdata,char *fullfile);
  static char fullfile[512];
  acq_plt *acqdata = NULL;
  extern FILE *parse_track_datain;
  //char test[16];
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("Test for load track data parser");
    }
  /* we first find the data that we need to transform */
  //if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
  //	return win_printf_OK("cannot find data");
  
  //op_new = create_and_attach_one_plot(pr,1024, 1024, 0);
  //if (op_new == NULL) return 1;
  
  acqdata = (acq_plt *)calloc(1,sizeof(acq_plt));
  if (acqdata == NULL) return win_printf("Problem in memory allocation");
  switch_allegro_font(1);
  file_select_ex("Load tracking data file (*.acq)", fullfile, "acq", 512, 0, 0);
  switch_allegro_font(0);
  parse_track_datain = fopen(fullfile,"r");
  if (parse_track_datain == NULL) return win_printf("cannot open file");
  parse_track_datalex(acqdata,fullfile);
  push_bin_track_data(parse_track_datain , acqdata);
  return D_O_K;
}

int push_bin_track_data(FILE *fp, acq_plt* acqdata)
{ 
  int i, j;
  int nw = 0;
  char ch;

  if (fp == NULL) return win_printf("file pointer null");
  if (acqdata == NULL) return win_printf("acq data pointer null");
  rewind(fp);
  while (fread (&ch, sizeof (char), 1, fp) == 1 && ch != CRT_Z);
    
  if (acqdata->data == NULL) win_printf("So far so good\n  n bead %d \n n %d",acqdata->n_bead,acqdata->n);
  
  acqdata->data = realloc(acqdata->data,acqdata->n*sizeof(tracking_data));
  if (acqdata->data == NULL) return win_printf("Mem alloc pb");
  for (i = 0 ; i < acqdata->n ; i++)
    {
      acqdata->data[i].x =(float *)calloc(acqdata->n_bead, sizeof(float));
      if (acqdata->data[i].x == NULL) return win_printf("Mem alloc pb");	
      acqdata->data[i].y =(float *)calloc(acqdata->n_bead, sizeof(float));
      if (acqdata->data[i].y == NULL) return win_printf("Mem alloc pb");	
      acqdata->data[i].z =(float *)calloc(acqdata->n_bead, sizeof(float));
      if (acqdata->data[i].z == NULL) return win_printf("Mem alloc pb");	
      acqdata->data[i].n_l =(char *)calloc(acqdata->n_bead, sizeof(char));
      if (acqdata->data[i].n_l == NULL) return win_printf("Mem alloc pb");
    }
  for (i = 0; i < acqdata->n; i++)
    {	
      nw += fread (&(acqdata->data[i].image_num) , sizeof(int), 1, fp);
      nw += fread (&(acqdata->data[i].image_t), sizeof(long long int), 1, fp);
      nw += fread (&(acqdata->data[i].image_dt), sizeof(unsigned long ), 1, fp);
      nw += fread (&(acqdata->data[i].image_zmag), sizeof(float), 1, fp);
      nw += fread (&acqdata->data[i].image_rot_mag, sizeof(float), 1, fp);
      nw += fread (&acqdata->data[i].image_obj_pos, sizeof(float), 1, fp);
      
      for (j = 0; j <acqdata->n_bead; j++)
	{
	  nw += fread (&acqdata->data[i].x[j] , sizeof(float), 1, fp);
	  nw += fread (&acqdata->data[i].y[j] , sizeof(float), 1, fp);
	  nw += fread (&acqdata->data[i].z[j] , sizeof(float), 1, fp);
	  nw += fread (&acqdata->data[i].n_l[j] , sizeof(char), 1, fp);
	}
    }       
  if (nw != ((6+4*acqdata->n_bead)*acqdata->n )) 
    {
      return win_printf("BUG detected %d", nw);
    }
  return D_O_K;
  
} 
void free_acq_plt(acq_plt *acqds)
{
  register int i;
	
  if (!acqds) return;
  for ( i=0 ; i < 5+(*acqds).n_bead*3 ; i++)	    free((*acqds).name[i]);
  free((*acqds).data);
  free(acqds);
  return;
}

acq_plt *read_acq_data(acq_plt **acqdata, char *filename)
{
  extern int parse_track_datalex(acq_plt *acqdata,char *fullfile);
  static char fullfile[512];
  extern FILE *parse_track_datain;
		
  if(updating_menu_state != 0)	return D_O_K;
	
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      win_printf_OK("Test for load track data parser");
      return NULL;
    }
  if (*acqdata == NULL) *acqdata = (acq_plt *)calloc(1,sizeof(acq_plt));
  if (*acqdata == NULL)
    {
      win_printf("Problem in memory allocation");
      return NULL;
    }
  parse_track_datain = fopen(filename,"r");
  if (parse_track_datain == NULL) return NULL;//win_printf("cannot open file");
  parse_track_datalex(*acqdata,fullfile);
  fclose(parse_track_datain);
  parse_track_datain = fopen(filename,"rb");
  push_bin_track_data(parse_track_datain , *acqdata);
  fclose(parse_track_datain);
  return *acqdata;   
}

	
char *return_acq_file_name(void)
{
  register int i = 0;
  static char fullfile[512], *fu = NULL;
  
  if (fu == NULL)
    {
      fu = (char*)get_config_string("ACQ","last_loaded",NULL);
      if (fu != NULL)	
	{
	  strcpy(fullfile,fu);
	  fu = fullfile;
	}
      else
	{
	  my_getcwd(fullfile, 512);
	  strcat(fullfile,"\\");
	}
    }
  switch_allegro_font(1);
  i = file_select_ex("Load ACQ (*.acq)", fullfile, "acq", 512, 0, 0);
  switch_allegro_font(0);
  if (i != 0) 	
    {
      fu = fullfile;
      set_config_string("ACQ","last_loaded",fu);
      return fullfile;;
    }
  return NULL;
}

multi_d_s *convert_acq_plt_to_xvplt(acq_plt *acqplt)
{
  multi_d_s *mds = NULL;
  int  i = 0, k=0;
  float *tmp = NULL /*, *tmp1 = NULL, *tmp2 = NULL*/;
  char *chtmp = NULL;
  
  if (acqplt == NULL) return NULL;
  
  //    chtmp = (char *) calloc(128, sizeof(char));
  
  //win_printf("We get in %d \n %d",(acqplt->n_bead*4+6),acqplt->n);
  if ((mds = create_multi_d_s(acqplt->n_bead*4+6)) == NULL) 
    {
      win_printf_OK("No memory");
			    return NULL;
    }
  //win_printf("ola!");
  //mds->name = (char **) calloc(acqplt->n_bead*3+5,sizeof(char *)); 
  for (i = 0 ; i < (acqplt->n_bead*4+6) ; i++)
    {
      if (acqplt->name[i] != NULL)
	{
	  mds->name[i] = (char *) calloc(strlen(acqplt->name[i])+1, sizeof(char));
	  //                 sprintf(chtmp,"%d",i);
	  strcpy(mds->name[i],acqplt->name[i]);
	  //win_printf("%s",mds->name[i]);
	}
      else
	{
	  mds->name[i] = (char *) calloc(4, sizeof(char));
	  chtmp = (char *) calloc(128, sizeof(char));
	  sprintf(chtmp,"%d",i);
	  strcpy(mds->name[i],chtmp);
	}   
    }
  
  
  if (acqplt->source != NULL)    mds->source = acqplt->source;
  tmp = (float*)calloc(acqplt->n_bead*4+6, sizeof(float));
  for (i = 0; i < acqplt->n ; i++)
    {
      
      tmp[0] = (float) acqplt->data[i].image_num;
      tmp[1] = (float) acqplt->data[i].image_dt;
      tmp[2] = (float) acqplt->data[i].image_dt;
      tmp[3] = (float) acqplt->data[i].image_zmag;
      tmp[4] = (float) acqplt->data[i].image_rot_mag;
      tmp[5] = (float) acqplt->data[i].image_obj_pos;
      for (k = 0; k < acqplt->n_bead ; k++)
	{
	  tmp[6+3*k] = acqplt->data[i].x[k];
	  tmp[7+3*k] = acqplt->data[i].y[k];
	  tmp[8+3*k] = acqplt->data[i].z[k];
	  tmp[9+3*k] = acqplt->data[i].n_l[k];
	  
        }   
      add_data_to_multi_d_s(mds, tmp);
    }
  
  return mds;
}

int plot_acqplt(void)
{
  acq_plt *acqplt = NULL;
  multi_d_s *mds = NULL;
  char *filename = NULL;
  pltreg *pr = NULL;
  static int first = 1;
  
    
  if(updating_menu_state != 0)	return D_O_K;
  
  if (first)
    {				
      add_plot_treat_menu_item("Xv Plots",NULL,xvplot_plot_menu(NULL),0,NULL);
      first = 0;
    }
  filename = (char *)calloc(512, sizeof(char));
  filename = return_acq_file_name();
  //win_printf("%s", backslash_to_slash(filename));
  read_acq_data(&acqplt,filename);
  if (acqplt == NULL) return win_printf("Patate");
  mds = convert_acq_plt_to_xvplt(acqplt);
  if (mds == NULL) return win_printf("Patate 2");
  
  
  if ((pr=create_multi_d_s_pltreg(mds))==NULL)
    return win_printf_OK("cannot create good plot reg");
  return D_O_K; 
}


int test(void)
{
  //  FILE *fp = NULL;
  //int k = 100;
  //int test;
  if(updating_menu_state != 0)	return D_O_K;

  //  is_tracking_on = (is_tracking_on) ? 0 : 1;    

  return D_O_K;
}
MENU *track_data_menu(void)
{
  static MENU mn[32];
	
  int do_load_track_data(void);

  if (mn[0].text != NULL)	return mn;
  
  add_item_to_menu(mn,"Load Track",plot_acqplt,NULL,0,NULL);
  add_item_to_menu(mn,"Background saving",  test_background_saving,NULL,0,NULL);
  add_item_to_menu(mn,"Stop Background saving",  kill_timer,NULL,0,NULL);
  add_item_to_menu(mn,"File test",  test,NULL,0,NULL);
  return mn;
}

int	Pico_track_data_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "track_data", NULL, track_data_menu(), 0, NULL);
  add_image_treat_menu_item ( "track_data", NULL, track_data_menu(), 0, NULL);
  return D_O_K;
}


#endif
