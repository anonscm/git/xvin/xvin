/*
 *    Plug-in program for data treatement and saving in Xvin.
 *
 *    V. Croquette
 *    JF Allemand
 */
#ifndef _TRACK_DATA_C_
#define _TRACK_DATA_C_


# include "allegro.h"
# include "winalleg.h"
# include "ifcapi.h"
# include <windowsx.h>
# include "ctype.h"
# include "xvin.h"
# include "brown.h"
#include  "xvplot.h"

# define BUILDING_PLUGINS_DLL
# include "track_data.h"


//changer reference au fichier de config


int aquire_and_save_data_in_real_time(void)
{
  int number_of_images = 0;
  static int num_of_files = 0;
  int init_absolute_counter = 0, saving_absolute_counter = 0;
  int init_pos_in_buffer = 0, saving_pos_in_buffer = 0;
  char common_name[64]="tracking";
  char filename[128];
  
  
  if(updating_menu_state != 0)	return D_O_K;

  num_of_files = get_config_int("SAVING PicoTwist TRACKING DATA","filenumber",0);
  win_scanf("Characteristic string for the files? %s\n Initial number for file%d?",common_name,&num_of_files);
  
  sprintf(filename,"%s%d.gr",common_name,num_of_files);
  
  num_of_files++;
  set_config_int("SAVING PicoTwist TRACKING DATA","filenumber",num_of_files);


  win_scanf("You want to save positions for how many images?%d",&number_of_images);
  init_absolute_counter = track_info->imi[track_info->c_i];
  init_pos_in_buffer = saving_pos_in_buffer = track_info->c_i;
  saving_absolute_counter = track_info->imi[track_info->c_i];
  
  
  //win_printf("init saving counter %d \n init pos_in_buffer %d", saving_counter, ini_pos_in_buffer);
  while ( saving_absolute_counter < (init_absolute_counter + number_of_images) || (track_info->imi[track_info->c_i] < init_absolute_counter + number_of_images))
    {
      draw_bubble(screen,0,750,50,"saving counter %d pos_in_buffer %d c_i %d mi %d ", saving_absolute_counter, saving_pos_in_buffer,track_info->c_i,track_info->imi[track_info->c_i]);
      
      if (( track_info->imi[track_info->c_i] - saving_absolute_counter) > BUFFER_PACK_SIZE )//enough data arrived
	{
	  save_data_to_disk_from_rolling_buffer(saving_pos_in_buffer,
						(saving_pos_in_buffer+ BUFFER_PACK_SIZE)%TRACKING_BUFFER_SIZE,filename,track_info);
	  saving_absolute_counter += BUFFER_PACK_SIZE;
	  saving_pos_in_buffer = (saving_pos_in_buffer + BUFFER_PACK_SIZE) %  TRACKING_BUFFER_SIZE ;
	}
      else if (number_of_images < BUFFER_PACK_SIZE && track_info->imi[track_info->c_i] > init_absolute_counter +number_of_images)//aquisition finished and not many data required
	{
	  save_data_to_disk_from_rolling_buffer(saving_pos_in_buffer,
						(init_pos_in_buffer+number_of_images) % TRACKING_BUFFER_SIZE,filename,track_info);
	  saving_absolute_counter += ((init_pos_in_buffer + number_of_images) % TRACKING_BUFFER_SIZE-saving_pos_in_buffer) ? (init_pos_in_buffer + number_of_images)% TRACKING_BUFFER_SIZE-saving_pos_in_buffer : (init_pos_in_buffer + number_of_images)% TRACKING_BUFFER_SIZE + TRACKING_BUFFER_SIZE - saving_pos_in_buffer;
	  saving_pos_in_buffer = (saving_pos_in_buffer + BUFFER_PACK_SIZE) %  TRACKING_BUFFER_SIZE  ;//Not the best thing to do here but should work
	}
      else if (init_absolute_counter + number_of_images -saving_absolute_counter < BUFFER_PACK_SIZE)    
	{
	  save_data_to_disk_from_rolling_buffer(saving_pos_in_buffer,
						(init_absolute_counter+number_of_images - saving_absolute_counter) % TRACKING_BUFFER_SIZE,filename,track_info);
	  saving_absolute_counter += ((init_pos_in_buffer + number_of_images) % TRACKING_BUFFER_SIZE-saving_pos_in_buffer) ? (init_pos_in_buffer + number_of_images)% TRACKING_BUFFER_SIZE-saving_pos_in_buffer : (init_pos_in_buffer + number_of_images)% TRACKING_BUFFER_SIZE + TRACKING_BUFFER_SIZE - saving_pos_in_buffer;
	  saving_pos_in_buffer = (saving_pos_in_buffer + BUFFER_PACK_SIZE) %  TRACKING_BUFFER_SIZE  ;//Not the best thing to do here but should work
	}
      else win_printf("Find your way!");
    }
  
  return 0;
}

int save_data_to_disk_from_rolling_buffer(int starting_index,int end_index,char *filename,g_track *track_information)
{
  int nw =0, i=0; 
  FILE *fp;
  
  //win_printf(" starting %d \n end %d", starting_index, end_index);
  
  if (track_information == NULL) return win_printf("We lack track info to save");
  if (track_info->n_b <= 0) return win_printf("We lack track info to save");
  
  for (i = 0; i < track_info->n_b; i++)
    {
      
      fp = fopen (filename, "ab");
      if ( fp == NULL ) 	
	{        
	  win_printf("Cannot open the file!");
	  return 1;
	}
      if (starting_index < end_index)
	{
	  // Set also other parameters      
	  nw = fwrite (track_information->imi+starting_index, sizeof(int), end_index-starting_index, fp);
	  if (nw !=  end_index-starting_index)	return win_printf("error in saving");
	  
	  nw = fwrite (track_information->zmag+starting_index, sizeof(float), end_index-starting_index, fp);
	  if (nw !=  end_index-starting_index)	return win_printf("error in saving");
	  
	  nw = fwrite (track_information->rot_mag+starting_index, sizeof(float), end_index-starting_index, fp);
	  if (nw !=  end_index-starting_index)	return win_printf("error in saving");
	  
	  nw = fwrite (track_information->bd[i]->x+starting_index, sizeof(float), end_index-starting_index, fp);
	  if (nw !=  end_index-starting_index)	return win_printf("error in saving");
	  
	  nw = fwrite (track_information->bd[i]->y+starting_index, sizeof(float), end_index-starting_index, fp);
	  if (nw !=  end_index-starting_index)	return win_printf("error in saving");
	  
	  nw = fwrite (track_information->bd[i]->z+starting_index, sizeof(float), end_index-starting_index, fp);
	  if (nw !=  end_index-starting_index)	return win_printf("error in saving");
	}
      else if (end_index < starting_index)
	
	{
	  nw = fwrite (track_information->bd[i]->x+starting_index, sizeof(float), TRACKING_BUFFER_SIZE -starting_index, fp);
	  if (nw !=  TRACKING_BUFFER_SIZE-starting_index)	return win_printf("error in saving");
	  nw = fwrite (track_information->bd[i]->y+starting_index, sizeof(float), TRACKING_BUFFER_SIZE -starting_index, fp);
	  if (nw !=  TRACKING_BUFFER_SIZE-starting_index)	return win_printf("error in saving");
	  nw = fwrite (track_information->bd[i]->z+starting_index, sizeof(float), TRACKING_BUFFER_SIZE-starting_index, fp);
	  if (nw !=  TRACKING_BUFFER_SIZE-starting_index)	return win_printf("error in saving");
	  nw = fwrite (track_information->bd[i]->x, sizeof(float), end_index, fp);
	  if (nw !=  TRACKING_BUFFER_SIZE-starting_index)	return win_printf("error in saving");
	  nw = fwrite (track_information->bd[i]->y, sizeof(float), end_index, fp);
	  if (nw !=  TRACKING_BUFFER_SIZE-starting_index)	return win_printf("error in saving");
	  nw = fwrite (track_information->bd[i]->z, sizeof(float), end_index, fp);
	  
	}
      fclose(fp);
      /*Check the closure*/
      
      
    }
  
  return D_O_K;
  
}

int save_data_to_disk_from_rolling_buffer_tr_format(int starting_index,int end_index,char *filename_tr,g_track *track_information)
{
  int  i=0 , j=0 , k=0; 
  FILE *fp;
  static int test = 0;
  //tracking_data *track = NULL; 
  int size_of_track_data =0;
  void *track_data_to_save = NULL;
  //int croissant = 0 , decroissant=0;
  
  int im_i_shift = 0;
  int im_t_shift = sizeof(int);
  int im_dt_shift = sizeof(int)+sizeof(long long int);
  int im_zmag_shift = sizeof(int)+sizeof(long long int)+sizeof(unsigned long );
  int im_rot_mag_shift = sizeof(int)+sizeof(long long int)+sizeof(unsigned long )+sizeof(float);
  int im_obj_pos_shift = sizeof(int)+sizeof(long long int)+sizeof(unsigned long )+2*sizeof(float);
  int im_pos_shift = sizeof(int)+sizeof(long long int)+sizeof(unsigned long )+3*sizeof(float);
  int i_shift = sizeof(float)*3+sizeof(char);
  
  test++;  
  
  if (starting_index > end_index) end_index = end_index + TRACKING_BUFFER_SIZE;/*in principle OK*/
  
  //win_printf("We lack track");
  
  if (track_information == NULL) return win_printf("We lack track info to save");
  if (track_info->n_b <= 0) return win_printf("We lack track info to save");
  
  size_of_track_data = sizeof(int) + sizeof(long long int) + sizeof(unsigned long ) + sizeof(float)*3 + track_information->n_b*(3*sizeof(float)+sizeof(char));

  //  draw_bubble(screen,0,350,75,"Alloc size %d end %d",size_of_track,end_index-starting_index);
  track_data_to_save = (void *)calloc(end_index-starting_index,size_of_track);
  
  if (track_data_to_save == NULL) return win_printf("Memory alloc pb!");
  //draw_bubble(screen,0,350,85,"Alloc done %d",test);
  
  for ( k = 0 , j = starting_index; k < (end_index-starting_index) && j < end_index; k++, j++)
    {
      //draw_bubble(screen,0,150,95,"croissant %d decroissant %d",croissant,decroissant);
      if (memcpy(track_data_to_save + im_i_shift + k*size_of_track,&(track_information->imi[j%TRACKING_BUFFER_SIZE]),sizeof(int)) == NULL) return win_printf("pb!");
      memcpy(track_data_to_save + im_t_shift + k*size_of_track,&(track_information->imt[j%TRACKING_BUFFER_SIZE]),sizeof(long long int));
      memcpy(track_data_to_save + im_dt_shift + k*size_of_track ,&(track_information->imdt[j%TRACKING_BUFFER_SIZE]),sizeof(unsigned long ));
      memcpy(track_data_to_save + im_zmag_shift + k*size_of_track,&(track_information->zmag[j%TRACKING_BUFFER_SIZE]),sizeof(float));
      memcpy(track_data_to_save + im_rot_mag_shift + k*size_of_track,&track_information->rot_mag[j%TRACKING_BUFFER_SIZE],sizeof(float));
      memcpy(track_data_to_save + im_obj_pos_shift + k*size_of_track,&track_information->obj_pos[j%TRACKING_BUFFER_SIZE],sizeof(float));
      for (i = 0; i < track_information->n_b ; i++)
	{
	  memcpy(track_data_to_save + im_pos_shift + i*i_shift + k*size_of_track,&(track_information->bd[i]->x[j%TRACKING_BUFFER_SIZE]),sizeof(float));
	  memcpy(track_data_to_save + im_pos_shift + i*i_shift + sizeof(float) + k*size_of_track,&(track_information->bd[i]->y [j%TRACKING_BUFFER_SIZE]),sizeof(float));
	  memcpy(track_data_to_save + im_pos_shift + i*i_shift + 2*sizeof(float) +k*size_of_track,&(track_information->bd[i]->z[j%TRACKING_BUFFER_SIZE]),sizeof(float));
	  memcpy(track_data_to_save + im_pos_shift + i*i_shift + 3*sizeof(float) + k*size_of_track,&(track_information->bd[i]->n_l[j%TRACKING_BUFFER_SIZE]),sizeof(char));
	}
    }	        
  
  

  /*track = (tracking_data *)calloc(end_index-starting_index,sizeof(tracking_data));
    if (track == NULL) return win_printf("calloc pb!");
    for (i = 0; i < end_index-starting_index ; i++)
    {
    track[i].x = (float *)calloc(track_info->n_b,sizeof(float));
    if (track[i].x == NULL) return win_printf("calloc pb!");
    track[i].y = (float *)calloc(track_info->n_b,sizeof(float));
    if (track[i].y == NULL) return win_printf("calloc pb!");
    track[i].z = (float *)calloc(track_info->n_b,sizeof(float));
    if (track[i].z == NULL) return win_printf("calloc pb!");
    track[i].n_l = (char *)calloc(track_info->n_b,sizeof(char));
    if (track[i].n_l == NULL) return win_printf("calloc pb!");
    }
    
    //draw_bubble(screen,0,350,75,"Memory alloc OK %d",test);
    
    for ( j=starting_index; j < end_index; j++)
    {        
    
    track[j - starting_index].image_num = track_information->imi[j%TRACKING_BUFFER_SIZE];
    draw_bubble(screen,0,350,85,"Track copy %d track %d info %d",test,track[j - starting_index].image_num,track_information->imi[j%TRACKING_BUFFER_SIZE]);
    track[j - starting_index].image_t = track_information->imt[j%TRACKING_BUFFER_SIZE];
    track[j - starting_index].image_dt = track_information->imdt[j%TRACKING_BUFFER_SIZE];
    track[j - starting_index].image_zmag = track_information->zmag[j%TRACKING_BUFFER_SIZE];
    track[j - starting_index].image_rot_mag = track_information->rot_mag[j%TRACKING_BUFFER_SIZE];
    track[j - starting_index].image_obj_pos = track_information->obj_pos[j%TRACKING_BUFFER_SIZE];
    for (i = 0; i < track_information->n_b ; i++)
    {
    track[j - starting_index].x[i] = track_information->bd[i]->x[j%TRACKING_BUFFER_SIZE];
    track[j - starting_index].y[i] = track_information->bd[i]->y [j%TRACKING_BUFFER_SIZE];
    track[j - starting_index].z[i] = track_information->bd[i]->z[j%TRACKING_BUFFER_SIZE];
    track[j - starting_index].n_l[i] = track_information->bd[i]->n_l[j%TRACKING_BUFFER_SIZE];
    }
    }*/
  
  
  //draw_bubble(screen,0,350,105,"Before fwrite OK %d",test);
  
  fp = fopen (filename_tr, "ab");
  if ( fp == NULL ) 	
    {    
      //draw_bubble(screen,0,350,95,"OUT BAD %d",test);    
      win_printf("Cannot open the file!");
      return 1;
    }
  //draw_bubble(screen,0,350,120,"Before size %d",test);    
  
  
  //draw_bubble(screen,0,250,120,"After size %d",test);    
  
  
  //nw += 
  //  draw_bubble(screen,0,250,50,"fwrite %d and %d     ",test,fwrite (track_data_to_save, size_of_track, end_index - starting_index, fp));
  /*nw = fwrite (track_data_to_save, size_of_track, end_index - starting_index, fp)	;    */
  //draw_bubble(screen,0,250,75,"Write %d pote %f %f %f",test,*(track_information->bd[i]->x + j%TRACKING_BUFFER_SIZE),*(track_information->bd[i]->y + j%TRACKING_BUFFER_SIZE),*(track_information->bd[i]->z + j%TRACKING_BUFFER_SIZE));			    
  
  if (fclose(fp) != 0) 
    {
      //draw_bubble(screen,0,150,65,"fp %d",test);
      return win_printf("Failure in closure!");
    }
  //if (nw !=  0)/*end_index - starting_index)*/	
  //{
  //draw_bubble(screen,0,150,65,"nw %d",test);
  //  return win_printf("error in saving nb = %d\n  %d",track_information->n_b,nw);
  //}
  
  /*Check the closure*/
  
  
  
  //draw_bubble(screen,0,150,65,"OUT OK %d",test);
  if (track_data_to_save != NULL) free(track_data_to_save);
  return D_O_K;
  
}

int test_background_saving(void)
{
  acq_plt *acqdata = NULL;
  static int number_of_files = 0;
  static char common_name[64]="tracking_data";
  
  
  if(updating_menu_state != 0)	return D_O_K;
  
  //adapter cfg avec filename aussi
  number_of_files = get_config_int("SAVING IFC TRACKING DATA","filenumber",0);
  
  win_scanf("Characteristic string for the filename? %s\n Initial number for file%d?",common_name,&number_of_files);
  sprintf(filename_tr,"%s%d.tr",common_name,number_of_files);
  
  number_of_files++;
  set_config_int("SAVING IFC TRACKING DATA","filenumber",number_of_files);
  
  
  win_scanf("You want to save positions for how many images?%d",&number_of_images);
  
  init_absolute_counter = saving_absolute_counter = track_info->imi[track_info->c_i];
  
  init_pos_in_buffer = saving_pos_in_buffer = track_info->c_i;
  save_header(filename_tr,acqdata,number_of_images);
  SetTimer( win_get_window(),25,SAVING_INTERVAL,automatic_saving);/*sets a background saving*/
  return 0;  
  
}

int kill_timer(void)
{
  draw_bubble(screen,0,550,55,"I KILL YOU %d ",saving_absolute_counter);
  if (KillTimer(win_get_window(),  25) == FALSE) return win_printf("Timer not killed");  //stops the background task 
  return 0;   
}

/*DWORD WINAPI DisplayThreadProc(LPVOID lpParam) 
{
  return D_O_K;
}
int create_display_thread(void)
{
    HANDLE hThread = NULL;
    DWORD dwThreadId;

    hThread = CreateThread( 
            NULL,             // default security attributes
            0,                // use default stack size  
            DisplayThreadProc,// thread function 
            NULL,       	// argument to thread function 
            0,                // use default creation flags 
            &dwThreadId);     // returns the thread identifier 

    if (hThread == NULL) 	win_printf_OK("No thread created");
    SetThreadPriority(hThread,  THREAD_PRIORITY_TIME_CRITICAL);
    return 0;
}*/





void CALLBACK automatic_saving(HWND hwnd, UINT uMsg, UINT_PTR idEvent,  DWORD dwTime)
{
  
  static int counter= 0;
      
      
  //draw_bubble(screen,0,650,50,"saving %d counter %d pos_in_buffer %d c_i %d mi %d Buff %d", counter,saving_absolute_counter, saving_pos_in_buffer,track_info->c_i,track_info->imi[track_info->c_i],BUFFER_PACK_SIZE);
      
  if (( track_info->imi[track_info->c_i] - saving_absolute_counter) > BUFFER_PACK_SIZE )//enough data arrived for writing
	{
	  counter++;
	  //            draw_bubble(screen,0,950,50,"MERDE %d ",counter);
	  save_data_to_disk_from_rolling_buffer_tr_format(saving_pos_in_buffer,
							  (saving_pos_in_buffer+ BUFFER_PACK_SIZE)%TRACKING_BUFFER_SIZE,filename_tr,track_info);
	  saving_absolute_counter = saving_absolute_counter + BUFFER_PACK_SIZE;
	  //        draw_bubble(screen,0,650,25,"saving abs%d ",saving_absolute_counter);
	  saving_pos_in_buffer = (saving_pos_in_buffer + BUFFER_PACK_SIZE) %  TRACKING_BUFFER_SIZE ;
	}
  
  else if (init_absolute_counter + number_of_images - saving_absolute_counter < BUFFER_PACK_SIZE && track_info->imi[track_info->c_i] > init_absolute_counter +number_of_images)  //not a lot of data to finish saving  and aquisition finished
    {
      //     draw_bubble(screen,0,550,35,"OUT LOOP %d and %d",saving_pos_in_buffer,(( number_of_images  + init_pos_in_buffer)% BUFFER_PACK_SIZE) % TRACKING_BUFFER_SIZE);
      save_data_to_disk_from_rolling_buffer_tr_format(saving_pos_in_buffer,
						      ( number_of_images  + init_pos_in_buffer)% TRACKING_BUFFER_SIZE,filename_tr,track_info);
      saving_absolute_counter = 0;
      saving_pos_in_buffer = 0;
      
      kill_timer(); //in principle the aquisition is finished here
      
    }
  else draw_bubble(screen,0,750,100,"Not yet finished saving");
  return;
}



int start_save_acq_ds(void)
{
  // char filename[128];
  acq_plt *acqdata = NULL;
  static int number_of_files = 0;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  win_scanf("You want to save positions for how many images?%d",&number_of_images);
  
  init_absolute_counter = saving_absolute_counter = track_info->imi[track_info->c_i];
  init_pos_in_buffer = saving_pos_in_buffer = track_info->c_i;
  
  sprintf(filename_tr,"BEAD%d.acq",number_of_files++);
  //win_printf("IN %s",filename_tr);
  save_header(filename_tr,acqdata,number_of_images);
  //win_printf("header written");
  SetTimer( win_get_window(),25,SAVING_INTERVAL,automatic_saving);
  //win_printf("OUT");
  
  return 0;
}

int save_header(char *filename, acq_plt *acqdata, int number_of_images)
{
  FILE *fp = NULL;
  int i = 0;
  un_s *un;
  if (acqdata == NULL)    acqdata = (acq_plt *) calloc(1, sizeof(acq_plt));
  if (acqdata == NULL) return 1;
  
  //win_printf("Next %s \n %d",filename,acqdata->n_bead);
  prepare_header(filename,acqdata,number_of_images);
  //win_printf("Next step %s \n %d",filename,acqdata->n_bead);
  //win_printf("Next step %s \n %d",filename,(*acqdata).n);
  fp = fopen (filename, "w");
  if ( fp == NULL ) 	return win_printf("Cannot open %s!",backslash_to_slash(filename));
  
  if (acqdata->n > 0)
    fprintf(fp, "-n points %d\n",   acqdata->n);
  else fprintf(fp, "-n points %d\n",   0);//METTRE QUELQUE CHOSE DANS LA TACHE DE FOND
  
  if (acqdata->n_bead >= 0)
    fprintf(fp, "-n beads %d \n",   acqdata->n_bead); 
  
  /*for (i = 0 ; i < acqdata->n_bead; i++)
    {
    if (acqdata->calib_image_name[i] != NULL) fprintf(fp, "-calib_image_filename %d %s\n",i,acqdata->calib_image_name[i]); 
    }
  */
  if (acqdata->source  != NULL) fprintf(fp, "-src \"%s\"\n",    acqdata->source);
  
  /*un_s part*/
  //if (acqdata->x_unit  != NULL) fprintf(fp, "-axp %d\n",op->c_xu_p,    acqdata->source);
  if (acqdata->x_unit != NULL)
    {
      
      un = acqdata->x_unit;			
      if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
	fprintf(fp,"-xus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
      else if (un != NULL && un->type != IS_RAW_U)
	fprintf(fp,"-xus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
      
    }	
  if (acqdata->y_unit != NULL)
    {
      
      un = acqdata->y_unit;			
      if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
	fprintf(fp,"-yus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
      else if (un != NULL && un->type != IS_RAW_U)
	fprintf(fp,"-yus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
      
    }
  /*if (acqdata->z_unit != NULL)
    {
    
    un = acqdata->z_unit;			
    if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
    fprintf(fp,"-zus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
    else if (un != NULL && un->type != IS_RAW_U)
    fprintf(fp,"-zus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
    
    }*/
  if (acqdata->t_unit != NULL)
    {
      
      un = acqdata->t_unit;			
      if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
	fprintf(fp,"-tus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
      else if (un != NULL && un->type != IS_RAW_U)
	fprintf(fp,"-tus %g %g \"%s\" %d %d %d\n",un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
      
    }
  
  if (acqdata->name[0] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[0]);
  if (acqdata->name[1] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[1]);
  if (acqdata->name[2] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[2]);
  if (acqdata->name[3] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[3]);
  if (acqdata->name[4] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[4]);
  if (acqdata->name[5] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[5]);
  
  
  for (i = 0 ; i < acqdata->n_bead*4; i++)
    {
      if (acqdata->name[6+i] != NULL) fprintf(fp, "-name \"%s\"\n",acqdata->name[6+i]);
    }
  
  //unit set a voir + image de calibration a rajouter
  fprintf (fp, "%c",26);
  if (fclose(fp) != 0) return win_printf("Failure in closure!");
  return 0;
  
}

int prepare_header(char *filename, acq_plt *acqdata, int num_points)
{   
  int i = 0;
  time_t timer;
  
  
  //win_printf("we get in");
  if (acqdata == NULL) return win_printf("Acq data NULL");
  if (track_info == NULL) return win_printf("Track info NULL");
  acqdata->n_bead = track_info->n_b;		/* number of data sets	*/
  //win_printf("Next step %s \n %d",filename,acqdata->n_bead);
  
  acqdata->n = num_points;   		    	/* number of points in each data set	*/
  /*PREVOIR LECTURE EN COURS AVEC PARAMETRE A -1*/
  
  //acqdata->m = num_points;   			    /* size allocated for each data set */
  acqdata->source = (char *) calloc(strlen(filename+1),sizeof(char));
  acqdata->source = filename;	            /* name of the file	*/
  
  acqdata->time = time(&timer);
  /****UNIT SET SECTION*******************/
  acqdata->x_unit = track_info->bd[0]->calib_im_fil->xu[track_info->bd[0]->calib_im_fil->c_xu];
  acqdata->y_unit = track_info->bd[0]->calib_im_fil->yu[track_info->bd[0]->calib_im_fil->c_yu];
  acqdata->t_unit = track_info->bd[0]->calib_im_fil->tu[track_info->bd[0]->calib_im_fil->c_tu];
  
  acqdata->data = (tracking_data *) calloc(acqdata->n , sizeof(tracking_data ));
  if (acqdata->data == NULL) return 1;
  
  acqdata->data->x = (float *) calloc(acqdata->n_bead , sizeof(float)) ;
  if (acqdata->data->x == NULL) return 1;
  acqdata->data->y = (float *) calloc(acqdata->n_bead , sizeof(float)) ;
  if (acqdata->data->y == NULL) return 1;
  acqdata->data->z = (float *) calloc(acqdata->n_bead , sizeof(float)) ;
  if (acqdata->data->z == NULL) return 1;
  acqdata->data->n_l = (char *) calloc(acqdata->n_bead , sizeof(char)) ;
  if (acqdata->data->n_l == NULL) return 1;
  //win_printf("before");
  acqdata->name = (char **) calloc(6+acqdata->n_bead*4, sizeof(char *));
  if (acqdata->name == NULL) return 1;
  for (i = 0; i < 6; i++)
    {
      acqdata->name[i] = (char *) calloc(32, sizeof(char));    
      
    }
  acqdata->name[0] = "image number";
  acqdata->name[1] = "absolute time";
  acqdata->name[2] = "absolute differential time";
  acqdata->name[3] = "z mag";
  acqdata->name[4] = "rot mag";
  acqdata->name[5] = "obj pos";
  //win_printf("%s \n",acqdata->name[4]);
  //win_printf("after");
  
  /*acqdata->calib_image_name = (char **)calloc(acqdata->n_bead, sizeof(char *));
    for (i = 0 ; i < acqdata->n_bead ; i++)
    {
    if (track_info->bd[i]->calib_im_fil->filename != NULL)
    {
    win_printf("What are you doing here?");
    acqdata->calib_image_name[i] = (char *) calloc(strlen(track_info->bd[i]->calib_im_fil->filename)+1,sizeof(char *));
    strcpy(acqdata->calib_image_name[i],track_info->bd[i]->calib_im_fil->filename);
    
    }
    }*/
  //win_printf("%s \n",acqdata->name[4]);
  for (i = 0 ; i < acqdata->n_bead*4 ; i++)
    {
      acqdata->name[6+i] = (char *)calloc(64,sizeof(char));
      
      if(i%4 == 0)    sprintf(acqdata->name[6+i],"xbead%d",(int)(i/4));
      if(i%4 == 1)    sprintf(acqdata->name[6+i],"ybead%d",(int)(i/4));
      if(i%4 == 2)    sprintf(acqdata->name[6+i],"zbead%d",(int)(i/4));
      if(i%4 == 3)    sprintf(acqdata->name[6+i],"nlbead%d",(int)(i/4));
      
      
    }
  /* on verra apres char    *x_unit, *y_unit, *t_unit;*/		
  //float   *ax, *dx;
  //un_s	***xu;			/*  unit set */
  //int     *n_xu, *m_xu, *c_xu,  *c_xu_p;
  
  return 0;
  
  
}
  
int do_load_track_data(void)
{
  extern int parse_track_datalex(acq_plt *acqdata,char *fullfile);
	
  static char fullfile[512];
  acq_plt *acqdata = NULL;
  extern FILE *parse_track_datain;
  //char test[16];
  
  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("Test for load track data parser");
    }
  /* we first find the data that we need to transform */
  //if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
  //	return win_printf_OK("cannot find data");
  
  //op_new = create_and_attach_one_plot(pr,1024, 1024, 0);
  //if (op_new == NULL) return 1;
  
  acqdata = (acq_plt *)calloc(1,sizeof(acq_plt));
  if (acqdata == NULL) return win_printf("Problem in memory allocation");    
  switch_allegro_font(1);
  file_select_ex("Load tracking data file (*.tr)", fullfile, "acq", 512, 0, 0);
  switch_allegro_font(0);
  parse_track_datain = fopen(fullfile,"r");
  
  if (parse_track_datain == NULL) return win_printf("cannot open file");
  //win_printf("IN");
  parse_track_datalex(acqdata,fullfile);
  //win_printf("EXIT");
  push_bin_track_data(parse_track_datain , acqdata);    
  //rentrer les donnees
  //win_printf("EXIT");
  return D_O_K;
}

int push_bin_track_data(FILE *fp, acq_plt* acqdata)
{ 
  int i, j, k=0;
  int nw = 0;
  char ch;
  //int test = 0;
  //float test_float = 0;
  
  //win_printf("Hello");
  if (fp == NULL) return win_printf("file pointer null");
  if (acqdata == NULL) return win_printf("acq data pointer null");
  rewind(fp);
  while (fread (&ch, sizeof (char), 1, fp) == 1 && ch != CRT_Z)k++;
  draw_bubble(screen,0,550,75,"k = %d,acqn %d bead %d",k,acqdata->n,acqdata->n_bead);
  
  if (acqdata->data == NULL) win_printf("So far so good");
  
  acqdata->data = realloc(acqdata->data,acqdata->n*sizeof(tracking_data));
  if (acqdata->data == NULL) return win_printf("Mem alloc pb");
  //win_printf("So far so good888");
  for (i = 0 ; i < acqdata->n ; i++)
    {
      acqdata->data[i].x =(float *)calloc(acqdata->n_bead, sizeof(float));
      if (acqdata->data[i].x == NULL) return win_printf("Mem alloc pb");	
      //win_printf("So far so good999");
      acqdata->data[i].y =(float *)calloc(acqdata->n_bead, sizeof(float));
      if (acqdata->data[i].y == NULL) return win_printf("Mem alloc pb");	
      acqdata->data[i].z =(float *)calloc(acqdata->n_bead, sizeof(float));
      if (acqdata->data[i].z == NULL) return win_printf("Mem alloc pb");	
      acqdata->data[i].n_l =(char *)calloc(acqdata->n_bead, sizeof(char));
      if (acqdata->data[i].n_l == NULL) return win_printf("Mem alloc pb");
    }
  //win_printf("alloc done");
  //win_printf("acqdata->n %d\n acqdata->n_bead %d",acqdata->n,acqdata->n_bead);
  
  for (i = 0; i < acqdata->n; i++)
    {	
      nw += fread (&(acqdata->data[i].image_num) , sizeof(int), 1, fp);
      //win_printf("test %d\n nw %d ",test,nw);
      nw += fread (&(acqdata->data[i].image_t), sizeof(long long int), 1, fp);
      //win_printf("nw %d ",nw);
      nw += fread (&(acqdata->data[i].image_dt), sizeof(unsigned long ), 1, fp);
      //win_printf("nw %d ",nw);
      
      nw += fread (&(acqdata->data[i].image_zmag), sizeof(float), 1, fp);
      //win_printf("test float %f\n nw %d",test_float,nw);
      nw += fread (&acqdata->data[i].image_rot_mag, sizeof(float), 1, fp);
      nw += fread (&acqdata->data[i].image_obj_pos, sizeof(float), 1, fp);
      
      for (j = 0; j <acqdata->n_bead; j++)
	{
	  nw += fread (&acqdata->data[i].x[j] , sizeof(float), 1, fp);
	  nw += fread (&acqdata->data[i].y[j] , sizeof(float), 1, fp);
	  nw += fread (&acqdata->data[i].z[j] , sizeof(float), 1, fp);
	  nw += fread (&acqdata->data[i].n_l[j] , sizeof(char), 1, fp);
	}
    }       
  if (nw != ((6+4*acqdata->n_bead)*acqdata->n )) 
    {
      
      return win_printf("BUG detected %d", nw);
    }
  return 0;
  
} 
void free_acq_plt(acq_plt *acqds)
{
	register int i;
	
	if (!acqds) return;
	for ( i=0 ; i < 5+(*acqds).n_bead*3 ; i++)	    free((*acqds).name[i]);
	
	//for ( i=0 ; i < (*acqds)->n_bead*3 ; i++)   free((*acqds).data->x[i]);
	free((*acqds).data);
	free(acqds);
	return;
}






acq_plt *read_acq_data(acq_plt **acqdata, char *filename)
{
  extern int parse_track_datalex(acq_plt *acqdata,char *fullfile);
	static char fullfile[512];
	
	extern FILE *parse_track_datain;
	
	
	if(updating_menu_state != 0)	return D_O_K;
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	  {
	    win_printf_OK("Test for load track data parser");
	    return NULL;
	  }
	
	//win_printf("NULL %p ",*acqdata);
	
   	if (*acqdata == NULL) *acqdata = (acq_plt *)calloc(1,sizeof(acq_plt));
   	//**acqdata = {0,0,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0};
   	if (*acqdata == NULL) 
	  {
   	    win_printf("Problem in memory allocation");    
   	    return NULL;
   	}
   	
   	//win_printf("NULL %p \n %d",(*acqdata)->data,(*acqdata)->n);
	parse_track_datain = fopen(filename,"r");
	
	if (parse_track_datain == NULL) return NULL;//win_printf("cannot open file");
	//win_printf("2");
    parse_track_datalex(*acqdata,fullfile);
    
    //win_printf("3");
    
    if ((*acqdata)->data == NULL) win_printf("It should be NULL");
    fclose(parse_track_datain);
    
    parse_track_datain = fopen(filename,"rb");
    
    push_bin_track_data(parse_track_datain , *acqdata);  
    fclose(parse_track_datain);  
    
    
    return *acqdata;   
}

	
char *return_acq_file_name(void)
{
  register int i = 0;
  //char path[512], file[256];
  static char fullfile[512], *fu = NULL;
  
  if (fu == NULL)
	  {
	    fu = (char*)get_config_string("ACQ","last_loaded",NULL);
	    if (fu != NULL)	
	      {
		strcpy(fullfile,fu);
		fu = fullfile;
	      }
	    else
	      {
		my_getcwd(fullfile, 512);
		strcat(fullfile,"\\");
	      }
	  }
  switch_allegro_font(1);
  i = file_select_ex("Load ACQ (*.acq)", fullfile, "acq", 512, 0, 0);
  switch_allegro_font(0);
  if (i != 0) 	
	{
	  fu = fullfile;
	  set_config_string("ACQ","last_loaded",fu);
	  return fullfile;;
	}
  return NULL;
}

multi_d_s *convert_acq_plt_to_xvplt(acq_plt *acqplt)
{
  multi_d_s *mds = NULL;
  int  i = 0, k=0;
  float *tmp = NULL /*, *tmp1 = NULL, *tmp2 = NULL*/;
  char *chtmp = NULL;
  
  if (acqplt == NULL) return NULL;
  
  //    chtmp = (char *) calloc(128, sizeof(char));
  
  //win_printf("We get in %d \n %d",(acqplt->n_bead*4+6),acqplt->n);
  if ((mds = create_multi_d_s(acqplt->n_bead*4+6)) == NULL) 
    {
      win_printf_OK("No memory");
			    return NULL;
    }
  //win_printf("ola!");
  //mds->name = (char **) calloc(acqplt->n_bead*3+5,sizeof(char *)); 
  for (i = 0 ; i < (acqplt->n_bead*4+6) ; i++)
    {
      if (acqplt->name[i] != NULL)
	{
	  mds->name[i] = (char *) calloc(strlen(acqplt->name[i])+1, sizeof(char));
	  //                 sprintf(chtmp,"%d",i);
	  strcpy(mds->name[i],acqplt->name[i]);
	  //win_printf("%s",mds->name[i]);
	}
      else
	{
	  mds->name[i] = (char *) calloc(4, sizeof(char));
	  chtmp = (char *) calloc(128, sizeof(char));
	  sprintf(chtmp,"%d",i);
	  strcpy(mds->name[i],chtmp);
	}   
    }
  
  
  if (acqplt->source != NULL)    mds->source = acqplt->source;
  
  tmp = (float*)calloc(acqplt->n_bead*4+6, sizeof(float));
  /*tmp1 = (float*)calloc(acqplt->n, sizeof(float));
    tmp2 = (float*)calloc(acqplt->n, sizeof(float));
  */
  for (i = 0; i < acqplt->n ; i++)
    {
      
      tmp[0] = (float) acqplt->data[i].image_num;
      tmp[1] = (float) acqplt->data[i].image_dt;
      tmp[2] = (float) acqplt->data[i].image_dt;
      tmp[3] = (float) acqplt->data[i].image_zmag;
      tmp[4] = (float) acqplt->data[i].image_rot_mag;
      tmp[5] = (float) acqplt->data[i].image_obj_pos;
      for (k = 0; k < acqplt->n_bead ; k++)
	{
	  tmp[6+3*k] = acqplt->data[i].x[k];
	  tmp[7+3*k] = acqplt->data[i].y[k];
	  tmp[8+3*k] = acqplt->data[i].z[k];
	  tmp[9+3*k] = acqplt->data[i].n_l[k];
	  
        }   
      //win_printf("%f\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n",tmp[0],tmp[1],tmp[2],tmp[3],tmp[4],tmp[5],tmp[6],tmp[7],tmp[8],tmp[9],tmp[10]);
      //win_printf("ola TER!");
      add_data_to_multi_d_s(mds, tmp);
    }
  
  //win_printf("Out convertion");        
  return mds;
}

int plot_acqplt(void)
{
  acq_plt *acqplt = NULL;
  multi_d_s *mds = NULL;
  char *filename = NULL;
  pltreg *pr = NULL;
  static int first = 1;
  
    
  if(updating_menu_state != 0)	return D_O_K;
  
  if (first)
    {				
      add_plot_treat_menu_item("Xv Plots",NULL,xvplot_plot_menu(NULL),0,NULL);
      first = 0;
    }
  
  filename = (char *)calloc(512, sizeof(char));
  
  filename = return_acq_file_name();
  //win_printf("%s", backslash_to_slash(filename));
  read_acq_data(&acqplt,filename);
  if (acqplt == NULL) return win_printf("Patate");
  mds = convert_acq_plt_to_xvplt(acqplt);
  if (mds == NULL) return win_printf("Patate 2");
  
  
  if ((pr=create_multi_d_s_pltreg(mds))==NULL)
    return win_printf_OK("cannot create good plot reg");
  return D_O_K; 
}

int generate_track_file(void)
{
  char filename[128] = "track001.acq";
  acq_plt *acqdata = NULL;
  int number_of_images = 256;
  int k,i, j, nw = 0;
  FILE *fp = NULL;
  float f =0;
  long long int lli;
  unsigned long ulong;
  char ch;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  
  win_scanf("number_of_images ?%d",&number_of_images);
  acqdata = (acq_plt *) calloc(1, sizeof(acq_plt));
  if (acqdata == NULL) return win_printf("acqdata memory alloc pb");
  
  if (acqdata->data != NULL) win_printf("Not initialised");
  
  acqdata->n_bead = 2;
  acqdata->n = number_of_images;
  
    //win_printf("acqdata->n_bead %d",acqdata->n_bead);
  acqdata->source = (char *) calloc(strlen(filename+1),sizeof(char));
  acqdata->source = filename;	            /* name of the file	*/
  //acqdata->time = time(&timer);
  //win_printf("acqdata->source %s",acqdata->source);
  if (acqdata->data != NULL) win_printf("Not initialised");
  (*acqdata).data = (tracking_data *) calloc(acqdata->n , sizeof(tracking_data));
  if (acqdata->data == NULL) return 1;
  
  //win_printf("acqdata->data %p",acqdata->data);
  
  (*acqdata).data->x = (float *) calloc(acqdata->n_bead , sizeof(float)) ;
  if (acqdata->data->x == NULL) return 1;
  //win_printf("acqdata->data->x %p",acqdata->data->x);
  
  acqdata->data->y = (float *) calloc(acqdata->n_bead , sizeof(float)) ;
  if (acqdata->data->y == NULL) return 1;
  acqdata->data->z = (float *) calloc(acqdata->n_bead , sizeof(float)) ;
  if (acqdata->data->z == NULL) return 1;
  acqdata->data->n_l = (char *) calloc(acqdata->n_bead , sizeof(char)) ;
  if (acqdata->data->n_l == NULL) return 1;
  
  
  acqdata->name = (char **) calloc(6+acqdata->n_bead*4, sizeof(char *));
  if (acqdata->name == NULL) return 1;
  for (i = 0; i < 6; i++)
    {
      acqdata->name[i] = (char *) calloc(32, sizeof(char));    
      
    }
  acqdata->name[0] = "image number";
  acqdata->name[1] = "absolute time";
  acqdata->name[2] = "absolute differential time";
  acqdata->name[3] = "z mag";
  acqdata->name[4] = "rot mag";
  acqdata->name[5] = "obj pos";
  //win_printf("%s \n",acqdata->name[4]);
  
  for (i = 0 ; i < acqdata->n_bead*4 ; i++)
    {
      acqdata->name[6+i] = (char *)calloc(64,sizeof(char));
      
      if(i%4 == 0)    sprintf(acqdata->name[6+i],"xbead%d",(int)(i/4));
      if(i%4 == 1)    sprintf(acqdata->name[6+i],"ybead%d",(int)(i/4));
      if(i%4 == 2)    sprintf(acqdata->name[6+i],"zbead%d",(int)(i/4));
      if(i%4 == 3)    sprintf(acqdata->name[6+i],"nlbead%d",(int)(i/4));
      
      
    }
  
  //if (acqdata == NULL)    acqdata = (acq_plt *) calloc(1, sizeof(acq_plt));
  if (acqdata == NULL) return 1;
  
  
  fp = fopen (filename, "w");
  if ( fp == NULL ) 	return win_printf("Cannot open %s!",backslash_to_slash(filename));
  
  fprintf(fp, "-n points %d\n",   acqdata->n);
  
  if (acqdata->n_bead >= 0)
    fprintf(fp, "-n beads %d \n",   acqdata->n_bead); 
  
  if (acqdata->source  != NULL) fprintf(fp, "-src \"%s\"\n",    acqdata->source);
  if (acqdata->name[0] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[0]);
  if (acqdata->name[1] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[1]);
  if (acqdata->name[2] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[2]);
  if (acqdata->name[3] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[3]);
  if (acqdata->name[4] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[4]);
  if (acqdata->name[5] != NULL) fprintf(fp, "-name \"%s\"\n",    acqdata->name[5]);
  
    
  for (i = 0 ; i < acqdata->n_bead*4; i++)
    {
      if (acqdata->name[6+i] != NULL) fprintf(fp, "-name \"%s\"\n",acqdata->name[6+i]);
    }
  
  fprintf (fp, "%c",26);
  if (fclose(fp) != 0) return win_printf("Failure in closure!");
  
  
  
    //header prepared
  //win_printf("header prepared");
  
  fp = fopen (filename, "ab");
  if ( fp == NULL ) 	
    {        
    	win_printf("Cannot open the file!");
	return 1;
    }
  
  //win_printf("acqdata->n %d\n acqdata->n_bead %d",acqdata->n,acqdata->n_bead);
   for ( j=0 ; j < acqdata->n ; j++)
     {    
       k = j;
       nw += fwrite (&j, sizeof(int), 1, fp);
       lli = (long long int)(j*10);
       nw += fwrite (&lli, sizeof(long long int), 1, fp);
       ulong = (unsigned long)(j*100);
       nw += fwrite (&ulong, sizeof(unsigned long ), 1, fp);
       f = (float)j;
       nw += fwrite (&f, sizeof(float), 1, fp);
       f += (float)10;
       nw += fwrite (&f, sizeof(float), 1, fp);
       
       f += (float)10;
       nw += fwrite (&f, sizeof(float), 1, fp);
       
       for (i = 0; i < acqdata->n_bead; i++)
	 {
	   f += (float)2;
	   nw += fwrite (&f, sizeof(float), 1, fp);
	   f += (float)2;
	   nw += fwrite (&f, sizeof(float), 1, fp);
	   f += (float)2;
	   nw += fwrite (&f, sizeof(float), 1, fp);
	   ch += (char)2;
	   nw += fwrite (&ch, sizeof(char), 1, fp);
	   
	   //    draw_bubble(screen,0,750,50,"%f %f %f",*(track_information->bd[i]->x + j%TRACKING_BUFFER_SIZE),*(track_information->bd[i]->y + j%TRACKING_BUFFER_SIZE),*(track_information->bd[i]->z + j%TRACKING_BUFFER_SIZE));			    
	 }
       
     }
   
   if (fclose(fp) != 0) return win_printf("Failure in closure!");
   if (nw !=  (6+acqdata->n_bead*4)*acqdata->n)	return win_printf("error in saving nb = %d",nw);
   
    
   win_printf("nw %d",nw);
   
   return 0;
   
}


MENU *track_data_menu(void)
{
  static MENU mn[32];
	
  int do_load_track_data(void);
  
  if (mn[0].text != NULL)	return mn;
  
  add_item_to_menu(mn,"Load Track",plot_acqplt,NULL,0,NULL);
  add_item_to_menu(mn,"Generate Track",generate_track_file,NULL,0,NULL);
  
  
  //add_item_to_menu(mn,"Parser",test_parser_track_data,NULL,0,NULL);
  
  return mn;
}

int	track_data_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "track_data", NULL, track_data_menu(), 0, NULL);
  return D_O_K;
}


#endif
