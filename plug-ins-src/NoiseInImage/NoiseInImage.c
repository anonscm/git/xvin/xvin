//still needs to add :
// 1- phase correlation en fonction de la distance
// 2- variation correlation en fonction de la distance


/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _NOISEINIMAGE_C_
#define _NOISEINIMAGE_C_
//#include <sys/types>
 #include "allegro.h"
# include "xvin.h"
# include "math.h"
# include <time.h>



//# include "fillibbt.h"

/*
XV_VAR(un_s*, to_modify_un);
XV_FUNC(int, pop_unit_decade_mn, (void));
XV_FUNC(int, pop_unit_type_mn, (void));

 */

/* If you include other regular header do it here*/

#define _TRK_C_
# include "float.h"
# include "unitset.h"
#include "../trackBead/record.h"
#include "../../src/menus/plot/treat/p_treat_math.h"



/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled
# include "../nrutil/nrutil.h"
#include "../fft2d/fftbtl32n.h"
#include "../stat/stat.h"


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "NoiseInImage.h"
float yratio = 3;


int	draw_moving_shear_rect_vga_screen_unit_NI(int xt, int yt, // top fringes center
					       int xb, int yb, // bottom fringes center
					       int nfx, int nfy, // fringes size
					       int maxdx,    // maxium dx allowed
					       int dfy0, int dym,// fringes distance, typical and max dy
					       int color, int scale, int bead_nb,  BITMAP* bm, float *quality_factor)
{
    int x1, x2, x3, x4, y1, y2, y3, y4;
    //int tmp;
    int xcs, ycs;
    int l = (28 * (nfx >> 1)) / scale;
    int w = (28 * (nfy >> 1)) / scale;
    (void)maxdx;
    (void)dfy0;
    (void)dym;
  //
  //   if ((xt - xb) > maxdx)
  //     {
	// tmp = (xt + xb + maxdx)/2;
	// xb = (xt + xb - maxdx)/2;
	// xt = tmp;
  //     }
  //   else if ((xb - xt) > maxdx)
  //     {
	// tmp = (xt + xb + maxdx)/2;
	// xt = (xt + xb - maxdx)/2;
	// xb = tmp;
  //     }
  //   if ((yt - yb) > dfy0 + dym)
  //     {
	// tmp = (yt + yb + dfy0 + dym)/2;
	// yb = (yt + yb - dfy0 - dym)/2;
	// yt = tmp;
  //     }
  //   if ((yt - yb) < dfy0 - dym)
  //     {
	// tmp = (yt + yb + dfy0 - dym)/2;
	// yb = (yt + yb - dfy0 + dym)/2;
	// yt = tmp;
  //     }

    xcs = (28 * xt) / scale;
    ycs = bm->h - (28 * yt) / scale;
    x1 =  xcs - l;
    x2 =  xcs + l;
    y1 =  ycs - w;
    y2 =  ycs + w;

    line(bm, x1, y1, x2, y1, color);
    line(bm, x2, y1, x2, y2, color);
    line(bm, x2, y2, x1, y2, color);
    line(bm, x1, y2, x1, y1, color);
    line(bm, xcs, y1, xcs, y2, color);

    xcs = (28 * xb) / scale;
    ycs = bm->h - (28 * yb) / scale;
    x3 =  xcs - l;
    x4 =  xcs + l;
    y3 =  ycs - w;
    y4 =  ycs + w;

    line(bm, x3, y3, x4, y3, color);
    line(bm, x4, y3, x4, y4, color);
    line(bm, x2, y4, x3, y4, color);
    line(bm, x3, y4, x3, y3, color);
    line(bm, xcs, y3, xcs, y4, color);

    line(bm, (x1+x4)/2, y3, (x1+x4)/2, y2, color);
  //  textprintf_centre_ex(bm, large_font, xcs, ycs+30, color, -1, "%d", bead_nb);




    return 0;
}


int do_bg_minimum(void)
{
  O_i *oibg = NULL;
  O_i *oi = NULL;
  imreg *imr = NULL;
  int ii = 1;
  int framestart  = 1, frameend = -1;
  if (updating_menu_state != 0) return D_O_K;
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2) return D_O_K;

  oibg = duplicate_image_or_movie(oi, NULL, 2);
  if (oibg == NULL)     return D_O_K;
  add_to_image(imr, IS_ONE_IMAGE, (void*)oibg);
  imr->cur_oi = imr->n_oi-1;
  imr->one_i = imr->o_i[imr->cur_oi];
  imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
  switch_frame(oi,0);
  ii = win_scanf("Find minimum image between frame %d and frame %d (-1 to go to the end)\n",&framestart,&frameend);
  if (ii == WIN_CANCEL) return D_O_K;

  if (oi->im.data_type != IS_CHAR_IMAGE) return (win_printf_OK("Please write the code for images that are not 8 bits\n"));

  for (int i =0;i<oibg->im.ny;i++)
  {
    for (int j =0;j<oibg->im.nx;j++)
    {
      oi->im.pixel[i].ch[j] = 255;
    }
  }
  ii = framestart;
  while (switch_frame(oi,ii) != 0)
  {
    for (int i =0;i<oibg->im.ny;i++)
    {
      for (int j =0;j<oibg->im.nx;j++)
      {
        if (oi->im.pixel[i].ch[j] < oibg->im.pixel[i].ch[j])
        oibg->im.pixel[i].ch[j] = oi->im.pixel[i].ch[j];
      }
    }


    ii++;
    if (ii == frameend) break;
  }

  oi->rm_background = 1;
  oi->oi_bg = oibg;
  return D_REDRAWME;


}



int do_automatize_noise_analysis_and_photons_numbers(void)
{


    O_p  *opd, *opx, *opmean, *op;
    d_s *dssighf, *dssig, *dstemp, *dsmean, *dsd, *dsi, *dsphotonsw, *dsphotonsblack, *dsphotscorrect;
    double mean, meansq;
    double sig = 0;
    double sighf = 0;
    pltreg *pr;
    imreg *imr;
    int ii = 0;
    O_i *oic = NULL;





      if (updating_menu_state != 0)
      {
          return D_O_K;
      }



  if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
  {
      return (win_printf_OK("Could not find plot data"));
  }




  win_printf_OK("Please dump X trajectories\n");
  draw_track_XYZ_trajectories();
  do_average_all_ds();
  opd = pr->o_p[pr->n_op-2];
  opmean = pr->o_p[pr->n_op-1];
  dsmean = opmean->dat[0];
  set_ds_source(dsmean,"Mean data set");
  opx = create_and_attach_one_plot(pr,opd->n_dat,opd->n_dat,0);
  set_plot_title(opx,"Sigma and sigma HF X");
  set_plot_x_title(opx,"dataset");
  set_plot_y_title(opx,"Sigmas ");
  uns_op_2_op_by_type(opd, IS_Y_UNIT_SET, opx, IS_Y_UNIT_SET);


  dssighf = opx->dat[0];
  dssig = create_and_attach_one_ds(opx,opd->n_dat,opd->n_dat,0);
  dssig ->nx = dssig->ny = dssighf ->nx = dssighf ->ny = 0;

  set_ds_source(dssighf,"Sigma HF of substracted datasets\n");
  set_ds_source(dssig,"Sigma normal of substracted datasets\n");



  for (int ids = 0;ids<opd->n_dat;ids++)
  {
      dstemp = create_and_attach_one_ds(opmean,16,16,0);
      set_ds_source(dstemp,"Bead %d minus data set\n",ids);

      dsd = opd->dat[ids];
      if (dsmean->nx != dsd->nx) return(win_printf_OK("Problem length\n"));
      dstemp->nx = dstemp->ny = 0;
      for (int i = 0;i<dsmean->nx;i++)
      {
        add_new_point_to_ds(dstemp,dsd->xd[i],dsd->yd[i] - dsmean->yd[i]);
      }
      get_sigma_of_derivative_of_partial_ds(dstemp, 0, dstemp->nx, DBL_MAX,&sighf);
      mean = meansq = 0;
      for (int i = 0;i<dstemp->nx;i++)
      {
        mean += (double)dstemp->yd[i];
        meansq += (double)dstemp->yd[i]*dstemp->yd[i];
      }
      mean/= (double) dstemp->nx;
      meansq/= (double) dstemp->nx;
      sig = sqrt(meansq - mean*mean);
      add_new_point_to_ds(dssighf,ids,(float)sighf);
      add_new_point_to_ds(dssig,ids,(float)sig);
  }


  win_printf_OK("Please dump Y trajectories\n");
  draw_track_XYZ_trajectories();
  do_average_all_ds();
  opd = pr->o_p[pr->n_op-2];
  opmean = pr->o_p[pr->n_op-1];
  dsmean = opmean->dat[0];
  opx = create_and_attach_one_plot(pr,opd->n_dat,opd->n_dat,0);
  set_plot_title(opx,"Sigma and sigma HF Y");
  set_plot_x_title(opx,"dataset");
  set_plot_y_title(opx,"Sigmas ");
  uns_op_2_op_by_type(opd, IS_Y_UNIT_SET, opx, IS_Y_UNIT_SET);


  dssighf = opx->dat[0];
  dssig = create_and_attach_one_ds(opx,opd->n_dat,opd->n_dat,0);
  dssig ->nx = dssig->ny = dssighf ->nx = dssighf ->ny = 0;

  set_ds_source(dssighf,"Sigma HF of substracted datasets\n");
  set_ds_source(dssig,"Sigma normal of substracted datasets\n");



  for (int ids = 0;ids<opd->n_dat;ids++)
  {
      dstemp = create_and_attach_one_ds(opmean,16,16,0);
      set_ds_source(dstemp,"Bead %d minus data set\n",ids);
      dsd = opd->dat[ids];
      if (dsmean->nx != dsd->nx) return(win_printf_OK("Problem length\n"));
      dstemp->nx = dstemp->ny = 0;
      for (int i = 0;i<dsmean->nx;i++)
      {
        add_new_point_to_ds(dstemp,dsd->xd[i],dsd->yd[i] - dsmean->yd[i]);
      }
      get_sigma_of_derivative_of_partial_ds(dstemp, 0, dstemp->nx, DBL_MAX,&sighf);
      mean = meansq = 0;
      for (int i = 0;i<dstemp->nx;i++)
      {
        mean +=(double) dstemp->yd[i];
        meansq += (double)dstemp->yd[i]*dstemp->yd[i];
      }
      mean/= (double) dstemp->nx;
      meansq/= (double) dstemp->nx;
      sig = sqrt(meansq - mean*mean);
      add_new_point_to_ds(dssighf,ids,(float)sighf);
      add_new_point_to_ds(dssig,ids,(float)sig);
  }

  win_printf_OK("Please dump Z trajectories\n");
  draw_track_XYZ_trajectories();
  do_average_all_ds();
  opd = pr->o_p[pr->n_op-2];
  opmean = pr->o_p[pr->n_op-1];
  dsmean = opmean->dat[0];
  opx = create_and_attach_one_plot(pr,opd->n_dat,opd->n_dat,0);
  set_plot_title(opx,"Sigma and sigma HF Z");
  set_plot_x_title(opx,"dataset");
  set_plot_y_title(opx,"Sigmas ");
  uns_op_2_op_by_type(opd, IS_Y_UNIT_SET, opx, IS_Y_UNIT_SET);


  dssighf = opx->dat[0];
  dssig = create_and_attach_one_ds(opx,opd->n_dat,opd->n_dat,0);
  dssig ->nx = dssig->ny = dssighf ->nx = dssighf ->ny = 0;

  set_ds_source(dssighf,"Sigma HF of substracted datasets\n");
  set_ds_source(dssig,"Sigma normal of substracted datasets\n");



  for (int ids = 0;ids<opd->n_dat;ids++)
  {
      dstemp = create_and_attach_one_ds(opmean,16,16,0);
      set_ds_source(dstemp,"Bead %d minus data set\n",ids);
      dsd = opd->dat[ids];
      if (dsmean->nx != dsd->nx) return(win_printf_OK("Problem length\n"));
      dstemp->nx = dstemp->ny = 0;
      for (int i = 0;i<dsmean->nx;i++)
      {
        add_new_point_to_ds(dstemp,dsd->xd[i],dsd->yd[i] - dsmean->yd[i]);
      }
      get_sigma_of_derivative_of_partial_ds(dstemp, 0, dstemp->nx, DBL_MAX,&sighf);
      mean = meansq = 0;
      for (int i = 0;i<dstemp->nx;i++)
      {
        mean += (double)dstemp->yd[i];
        meansq +=(double) dstemp->yd[i]*dstemp->yd[i];
      }
      mean/= (double) dstemp->nx;
      meansq/= (double) dstemp->nx;
      sig = sqrt(meansq - mean*mean);
      add_new_point_to_ds(dssighf,ids,(float)sighf);
      add_new_point_to_ds(dssig,ids,(float)sig);
  }





ii = win_printf_OK("Please dump the first image of all beads\n");





imr = find_bead_image_in_record_file_data();


opx = create_and_attach_one_plot(pr,imr->n_oi-1,imr->n_oi-1,0);
set_plot_title(opx,"Bead photons number");
set_plot_x_title(opx,"Beads");
set_plot_y_title(opx,"Integrated numerical level");
dsphotonsw = opx->dat[0];
set_ds_source(dsphotonsw,"Mean black level on first lines");
dsphotonsw->nx = dsphotonsw->ny = 0;

dsphotonsblack = create_and_attach_one_ds(opx,imr->n_oi-1,imr->n_oi-1,0);
set_ds_source(dsphotonsblack,"Mean black level on first lines");
dsphotonsblack->nx = dsphotonsblack->ny = 0;


dsphotscorrect = create_and_attach_one_ds(opx,imr->n_oi-1,imr->n_oi-1,0);
set_ds_source(dsphotscorrect,"integrated level minus black level ");
dsphotscorrect->nx = dsphotscorrect->ny = 0;

dstemp = build_data_set(16,16);

for (int im =1;im<imr->n_oi;im++)
{
  oic = imr->o_i[im];


  dstemp->nx = dstemp->ny = 1;
  integrate_photons_on_area(oic,dstemp,0,oic->im.nx-1,0,oic->im.ny-1,0,0,1,1); // compute total photon number
  add_new_point_to_ds(dsphotonsw,im-1,dstemp->yd[0]);

  dstemp->nx = dstemp->ny = 1;
  integrate_photons_on_area(oic,dstemp,0,oic->im.nx-1,0,9,0,0,1,1); //compute black on first 10 lines
  add_new_point_to_ds(dsphotonsblack,im-1,dstemp->yd[0]/(10*oic->im.nx));

  add_new_point_to_ds(dsphotscorrect,im-1,dsphotonsw->yd[dsphotonsw->ny-1]-oic->im.nx*oic->im.ny*dsphotonsblack->yd[dsphotonsblack->ny-1]);

}

free_data_set(dstemp);


return refresh_plot(pr,pr->n_op-1);





}





int SDI_2_fill_x_av_y_profile_from_im(O_i *oi, int xco, int xend, int yco, int yend, float *y, int *ym,int *yg, int *yd)

{
    int j, i;
    int ly=yend-yco+1;
    int lx=xend-xco+1;

    float ymax=0;
    union pix *pd=NULL;


    pd  = oi->im.pixel;


    if (oi == NULL  || y == NULL)   return 1;


    for (i = 0; i < ly; i++)      y[i] = 0;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {

        for (i = 0; i < ly; i++)
        {
            for (j = 0; j < lx; j++)
                y[i] += (float) pd[yco+i].ch[xco+j];
            y[i]=y[i]/lx;
        }



    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
      for (i = 0; i < ly; i++)
      {
          for (j = 0; j < lx; j++)
              y[i] += (float) pd[yco+i].in[xco+j];
          y[i]=y[i]/lx;
      }


    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {
      for (i = 0; i < ly; i++)
      {
          for (j = 0; j < lx; j++)
              y[i] += (float) pd[yco+i].ui[xco+j];
          y[i]=y[i]/lx;
      }

    }

    else return 1;

    for (i=0;i<ly;i++)
    {
      if (y[i]>ymax)
      {
        *ym=i;
        ymax=y[i];
      }
    }

    (*yg)=(*ym);
    (*yd)=(*ym);
    //find yg limit given yratio
    while ( (y[*yg]>ymax/yratio) && (*yg>0))
    {
      *yg-=1;
    }

    //find yd limit given y ratio
    while ( (y[*yd]>ymax/yratio) && (*yd<ly-1))
    {
      *yd+=1;
    }

    return 0;
}

int SDI_2_fill_y_av_x_profile_from_im(O_i *oi, int xco, int xend, int yco, int yend, float *x, double *fit_param,double *xm)

{
    int j, i;
    int ly=yend-yco+1;
    int lx=xend-xco+1;
    double *a = NULL;


    union pix *pd=NULL;


    pd  = oi->im.pixel;


    if (oi == NULL  || x == NULL)   return 1;


    for (i = 0; i < lx; i++)      x[i] = 0;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {

        for (i = 0; i < lx; i++)
        {
            for (j = 0; j < ly; j++)
                x[i] += (float) pd[yco+j].ch[xco+i];
            x[i]=x[i]/ly;
        }



    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {

              for (i = 0; i < lx; i++)
              {
                  for (j = 0; j < ly; j++)
                      x[i] += (float) pd[yco+j].in[xco+i];
                  x[i]=x[i]/ly;
              }



    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {

              for (i = 0; i < lx; i++)
              {
                  for (j = 0; j < ly; j++)
                      x[i] += (float) pd[yco+j].ui[xco+i];
                  x[i]=x[i]/ly;
              }


    }
    else return 1;




    d_s *ds = build_data_set(5,5);
    ds->nx=ds->ny=5;

    for (i=lx/2-10;i<lx/2+10;i++)
    {
      if (x[i]>ds->yd[2])
      {
        ds->xd[2]=(double)i;
        ds->yd[2]=x[i];
      }
    }

    int deb =(int) (ds->xd[2]+5);
    int fin =(int) (ds->xd[2]+15);
    ds->yd[3] = 0;
    for (i=deb;i<fin;i++)
    {
      if (x[i]>ds->yd[3])
      {
        ds->xd[3]=(double)i;
        ds->yd[3]=x[i];
      }
    }


    deb =(int) (ds->xd[3]+5);
    fin =(int) (ds->xd[3]+15);
    ds->yd[4] = 0;
    for (i=deb;i<fin;i++)
    {
      if (x[i]>ds->yd[4])
      {
        ds->xd[4]=(double)i;
        ds->yd[4]=x[i];
      }
    }

    deb =(int) (ds->xd[2]-5);
    fin =(int) (ds->xd[2]-15);
    ds->yd[1] = 0;
    for (i=deb;i>=fin;i--)
    {
      if (x[i]>ds->yd[1])
      {
        ds->xd[1]=(double)i;
        ds->yd[1]=x[i];
      }
    }

    deb =(int) (ds->xd[1]-5);
    fin =(int) (ds->xd[1]-15);
    ds->yd[0] = 0;
    for (i=deb;i>=fin;i--)
    {
      if (x[i]>ds->yd[0])
      {
        ds->xd[0]=(double)i;
        ds->yd[0]=x[i];
      }
    }


    if (fit_ds_to_xn_polynome(ds, 3, ds->xd[0]-1, ds->xd[4]+1, 0, 256, &a) < 0) *xm = ds->xd[2];
    else *xm = -a[1]/(2*a[2]);

    if (!((*xm>ds->xd[0]) && (*xm<ds->xd[4])))  *xm = ds->xd[2];
    free_data_set(ds);


    if (a!=NULL && fit_param != NULL)
    {
      fit_param[0] = a[0];
      fit_param[1] = a[1];
      fit_param[2] = a[2];
      free(a);
    }





    return 0;
}

int find_barycenter_window(O_i *oi,int fxs,int fxe,int fys,int fye, float *xmfn, float *ymfn, int *tot, float threshold)
{
  if (oi == NULL) return 1;
  int lx = fxe - fxs +1;
  int ly = fye - fys +1;
  union pix *pd=NULL;
  pd = oi->im.pixel;
  float intxt = 0, intyt = 0, intt = 0;
  *tot = 0;
  for (int i = 0; i<ly;i++ )
  {
    for (int j =0;j<lx;j++)
    {
      intyt += (float) pd[fys+i].ch[fxs+j]*(fys+i);
      intxt += (float) pd[fys+i].ch[fxs+j]*(fxs+j);
      intt  += (float) pd[fys+i].ch[fxs+j];
      if (pd[fys+i].ch[fxs+j] > threshold) *tot += 1;

    }
  }
  *xmfn = intxt / intt;
  *ymfn = intyt/intt;

  return 0;

}




d_s *find_fringes_in_partial_OI_old(O_i *oi, int xco, int xend, int yco, int yend, d_s *dsfringes, int threshold, int xw, int yw)
{

  if (oi == NULL) return 1;
  if (oi->im.data_type != IS_CHAR_IMAGE) return(2);
  if (dsfringes == NULL) dsfringes = build_data_set(16,16);
  dsfringes ->nx = dsfringes->ny = 0;

  if (xco < 0 ) xco = 0;
  if (xend >=oi->im .nx) xend =oi->im .nx - 1;
  if (yco <0) yco = 0;
  if (yend >=oi->im .ny) yend =oi->im .ny - 1;
  if (xco >= xend) return 1;
  if (yco >= yend) return 1;

  int ly = yend - yco + 1;
  int lx = xend - xco + 1;
  union pix *pd=NULL;

  float *xp = calloc(lx,sizeof(float));
  float *yp = calloc(ly,sizeof(float));


  pd  = oi->im.pixel;

 int stop = 0;

 int ymax = 0;
 double xm = 0;
 int dummy = 0;
  for (int i= yw/2; i < ly-yw/2; i++)
  {
      for (int j = xw/2; j < lx-xw/2; j++)
      {
         if (pd[yco+i].ch[xco+j] <  threshold) continue; // si en dessous du trehold ne nous intéresse pas
         stop = 0;
         for (int k = 0 ; k< dsfringes->nx;k++)
         {
           if ((fabs(dsfringes->xd[k] - xco -j  ) < xw) && (fabs(dsfringes->yd[k] - yco - i ) < yw)) // si près d'une autre frange ne nous intéresse pas
           {
             stop = 1;

             continue;
           }
         }
           if (stop) continue;

           if (SDI_2_fill_x_av_y_profile_from_im(oi, xco+j-xw/2,  xco+j+xw/2,  yco+i-yw/2, yco+ i+yw/2,yp,&ymax,&dummy,&dummy)==1) return 1;
           if (SDI_2_fill_y_av_x_profile_from_im(oi,  xco+j-xw/2,  xco+j+xw/2, yco+ i-yw/2,  yco+i+yw/2, xp , NULL,&xm)) return 1;
           add_new_point_to_ds(dsfringes,xco+j-xw/2+(float) xm, yco+i-yw/2+(float) ymax);
       }
     }
     free(xp);
     free(yp);
     return dsfringes;
}

d_s *find_fringes_in_partial_OI(O_i *oi, int xco, int xend, int yco, int yend, d_s *dsfringes, int threshold, int xw, int yw)
{

  if (oi == NULL) return 1;
  if (oi->im.data_type != IS_CHAR_IMAGE) return(2);
  if (dsfringes == NULL) dsfringes = build_data_set(16,16);
  dsfringes ->nx = dsfringes->ny = 0;

  if (xco < 0 ) xco = 0;
  if (xend >=oi->im .nx) xend =oi->im .nx - 1;
  if (yco <0) yco = 0;
  if (yend >=oi->im .ny) yend =oi->im .ny - 1;
  if (xco >= xend) return 1;
  if (yco >= yend) return 1;

  int ly = yend - yco + 1;
  int lx = xend - xco + 1;
  union pix *pd=NULL;

  float *xp = calloc(lx,sizeof(float));
  float *yp = calloc(ly,sizeof(float));


pd  = oi->im.pixel;

 int stop = 0;

 //int ymax = 0;
 float ymaxf = 0;
 //double xm = 0;
 float xmf = 0;
 float ymf = 0;
 float xmfn = 0;
 float ymfn = 0;
 int fxs, fxe, fys, fye;
 int dummy = 0;
 int iter = 0;
 int converged = 0;
 int tot = 0;
 double dummyd = 0;

 d_s *dsi = NULL;
 dsi = build_data_set(xw,xw);
 for (int i = 0 ; i<dsi->nx;i++) dsi->xd[i] = i;
  for (int i= yw/2; i < ly-yw/2; i++)
  {
      for (int j = xw/2; j < lx-xw/2; j++)
      {
         if (pd[yco+i].ch[xco+j] <  threshold) continue; // si en dessous du trehold ne nous intéresse pas
         stop = 0;
         for (int k = 0 ; k< dsfringes->nx;k++)
         {
           if ((fabs(dsfringes->xd[k] - xco -j  ) < xw) && (fabs(dsfringes->yd[k] - yco - i ) < yw)) // si près d'une autre frange ne nous intéresse pas
           {
             stop = 1;

             continue;
           }
         }
           if (stop) continue;

           // if (SDI_2_fill_x_av_y_profile_from_im(oi, xco+j-xw/2,  xco+j+xw/2,  yco+i-yw/2, yco+ i+yw/2,yp,&ymax,&dummy,&dummy)==1) return 1;
           // if (SDI_2_fill_y_av_x_profile_from_im(oi,  xco+j-xw/2,  xco+j+xw/2, yco+ i-yw/2,  yco+i+yw/2, xp , NULL,&xm)) return 1;
           // ymaxf = (float) ymax;
           // xmf = (float) xm;
           fxs = xco + j -xw/2;
           fxe = xco + j +xw/2;
           fys = yco + i -yw/2;
           fye = yco + i +yw/2;


           converged =  0;
           iter = 0;
           xmf = ymf = -1;
           tot = 0;
           for (int r =0;r<3;r++)
           {
           find_barycenter_window(oi,fxs,fxe,fys,fye, &xmfn, &ymfn,&tot,threshold);
           fxs = (int) (xmfn - xw / 2 + 0.5);
           fxe = fxs + xw;
           fys = (int) (ymfn - yw / 2 + 0.5);
           fye = fys + yw;
           if (fxs < 0 || fys < 0 || fxe >= oi->im.nx || fye >= oi->im.ny)
           {
             stop = 1;
             break;
           }
         }

         // if (stop == 0 ) SDI_2_fill_y_av_x_profile_from_im(oi, fxs, fxe-1, fys, fye-1, dsi->yd, NULL,&dummyd);
         //
         //
         // if (stop == 0) fit_Y_equal_a_gaussian(dsi,&xmfn,30);
         //
         // xmfn = fxs + xmfn;

           // while (converged == 0)
           // {
           //   find_barycenter_window(oi,fxs,fxe,fys,fye, &xmfn, &ymfn);
           //   printf("fxs %d fxe %d fys %d fye %d xmfn %f, ymfn %f\n",fxs,fxe,fys,fye,xmfn,ymfn);
           //   fxs = (int) (xmfn - xw / 2 + 0.5);
           //   fxe = fxs + xw;
           //   fys = (int) (ymfn - yw / 2 + 0.5);
           //   fye = fys + yw;
             if (fxs < 0 || fys < 0 || fxe >= oi->im.nx || fye >= oi->im.ny)
             {
               stop = 1;
               break;
             }
           //   if ((xmfn == xmf) && (ymfn == ymf)) converged = 1;
           //   if (iter >= 6) converged = 1;
           //   iter++;
           //   ymf = ymfn;
           //   xmf = xmfn;
           // }
           if (stop) continue;
           if (tot > 6) add_new_point_to_ds(dsfringes,xmfn, ymfn);
       }
     }
     free(xp);
     free(yp);
     free_data_set(dsi);
     return dsfringes;
}



d_s *find_beads_in_partial_OI(O_i *oi, int xco, int xend, int yco, int yend, int threshold, int xw, int yw, int distance_x_max, int distance_y_max, int old)
{
  d_s *dsbeads = NULL;
  d_s *dsfringes = NULL;
  if (old)   dsfringes = find_fringes_in_partial_OI_old(oi,xco,xend, yco, yend, NULL, threshold, xw, yw);
  else    dsfringes = find_fringes_in_partial_OI(oi,xco,xend, yco, yend, NULL, threshold, xw, yw);


  if (dsfringes == NULL) return 1;
  dsbeads = build_data_set(16,16);
  dsbeads->nx = dsbeads->ny = 0;
  alloc_data_set_y_error(dsbeads);
  alloc_data_set_x_error(dsbeads);
  int invert = 0;
  for (int i = 0;i<dsfringes->nx;i++)
  {
    if (dsfringes->xd[i] == 0) continue;
    for (int j = i+1;j<dsfringes->nx;j++)
    {
      if (dsfringes->xd[j] == 0) continue;
      if ( (fabs(dsfringes->xd[i] - dsfringes->xd[j]) < distance_x_max) && (fabs(dsfringes->yd[i] - dsfringes->yd[j]) < distance_y_max))
      {
        invert = (dsfringes->yd[i] - dsfringes->yd[j] > 0) ? 1:0;
        add_new_point_to_ds(dsbeads,invert ? dsfringes->xd[i] : dsfringes->xd[j],invert ? dsfringes->yd[i] : dsfringes->yd[j]) ;
        dsbeads->xe[dsbeads->nx-1] = invert ? dsfringes->xd[j] : dsfringes->xd[i];
        dsbeads->ye[dsbeads->nx-1] = invert ? dsfringes->yd[j] : dsfringes->yd[i];
        dsfringes->xd[i] = dsfringes->xd[j] = 0;
        break;

      }
    }
  }

  free_data_set(dsfringes);

     return dsbeads;
   }

int compute_im_log(O_i *ois,char *max)
{
  *max = 0;
  char new1;
  for (int i = 0;i<ois->im.nx;i++)
  {
    for (int j = 0;j<ois->im.ny;j++)
    {
      new1 = (char)((int)36*log(1+(float)ois->im.pixel[j].ch[i]));
      ois->im.pixel[j].ch[i] = new1;
      if (new1>*max) *max=new1;
    }
  }
  return 0;
}

int compute_im_sqrt(O_i *ois,char *max)
{
  *max = 0;
  char new1;
  for (int i = 0;i<ois->im.nx;i++)
  {
    for (int j = 0;j<ois->im.ny;j++)
    {
      new1 = (char)((int)15*sqrt((float)ois->im.pixel[j].ch[i]));
      ois->im.pixel[j].ch[i] = new1;
      if (new1>*max) *max=new1;
    }
  }
  return 0;
}

int compute_im_max(O_i *ois,char *max)
{
  *max = 0;
  char new1;
  for (int i = 0;i<ois->im.nx;i++)
  {
    for (int j = 0;j<ois->im.ny;j++)
    {
      new1 = ois->im.pixel[j].ch[i];
      if (new1>*max) *max=new1;
    }
  }
  return 0;
}

int compute_partial_im_max(O_i *ois,char *max, int xmin, int xmax, int ymin, int ymax)
{
  if (xmin < 0) xmin = 0;
  if (xmax >= ois->im.nx) xmax = ois->im.nx-1;
  if (ymin < 0 ) ymin = 0;
  if (ymax >= ois->im.ny) ymax = ois->im.ny-1;
  if (xmin >= xmax || ymin>=ymax) return 1;
  *max = 0;
  char new1;
  for (int i = xmin;i<=xmax;i++)
  {
    for (int j = ymin;j<=ymax;j++)
    {
      new1 = ois->im.pixel[j].ch[i];
      if (new1>*max) *max=new1;
    }
  }
  return 0;
}

int multiply_area(O_i *ois, int xmin, int xmax, int ymin, int ymax, float factor)
{
  if (xmin < 0) xmin = 0;
  if (xmax >= ois->im.nx) xmax = ois->im.nx-1;
  if (ymin < 0 ) ymin = 0;
  if (ymax >= ois->im.ny) ymax = ois->im.ny-1;
  if (xmin >= xmax || ymin>=ymax) return 1;
  char new1;
  for (int i = xmin;i<=xmax;i++)
  {
    for (int j = ymin;j<=ymax;j++)
    {
      new1 = ois->im.pixel[j].ch[i];
      ois->im.pixel[j].ch[i] = (char) ((int) (new1 * factor));
    }
  }
  return 0;

}

int equalize_fringe(O_i *ois,int x1, int y1,int x2, int y2, int xw, int yw)
{
  char max = 0;
  if (compute_partial_im_max(ois,&max,x1-xw/2,x1+xw/2,y1-yw/2,y1+yw/2)) return 1;
  if (multiply_area(ois,x1-xw/2,x1+xw/2,y1-yw/2,y1+yw/2,40.0/(float)max)) return 1;

  if (compute_partial_im_max(ois,&max,x2-xw/2,x2+xw/2,y2-yw/2,y2+yw/2)) return 1;
  if (multiply_area(ois,x2-xw/2,x2+xw/2,y2-yw/2,y2+yw/2,40.0/(float)max)) return 1;
  return 0;


}

int do_draw_SDI_trajectories(void)
{

     if (updating_menu_state != 0) return 0;
   O_i *ois = NULL;
   d_s *dsfringes = NULL;
   imreg *imr = NULL;
   pltreg *pr = NULL;
   static int newplot = 0;
   O_p *opx = NULL, *opy = NULL, *opz = NULL, *op = NULL;
   d_s *dsx = NULL, *dsy = NULL, *dsz = NULL;
   d_s **dstrajx = NULL, **dstrajy = NULL, **dstrajz = NULL;
   d_s *dsbeads = NULL;
   static int threshold = 15, xw = 256, yw = 76, distance_x_max = 256, distance_y_max = 128;
   static int max_lost_images = 4, stich_distance = 200;
   float x=0, y=0, z = 0;
   int added = 0;
   static int frames = 0, framee = -1;
   int ntraj = 0;
   int mtraj = 0;
   DIALOG *d = NULL;
   PALETTE pal;
   static int operation = 0;
   static int autoscale = 0;
   static int autoscale_fr = 50;
   static int save = 0;


   int ii =0;
   if (updating_menu_state != 0) return D_O_K;

   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2) return D_O_K;

   ii = win_scanf("This routine will dump trajectories of the movie \n"
  "From frame %d to frame %d (-1 :until the end)\n"
 "Lum treshold for fringe detection %d fringe length %d fringe width %d\n"
 "Maximal x distance %d Maximal y distance %d \n"
  "Maximal number of missing images in a trajectory %d \n"
  "Stiching distance %d \n"
  "Draw a new plot for each trajectory %b \n"
  "Save image %d \n"
  "Operation %R None %r log %r Sqrt %r fringe equalization \n"
  "Autoscale %b on %d first frames \n"

  ,&frames,&framee,&threshold,&xw,&yw,&distance_x_max,&distance_y_max,&max_lost_images,&stich_distance,&newplot,&save,&operation,&autoscale,&autoscale_fr);

  if (ii == WIN_CANCEL) return 0;
  pr = create_and_register_new_plot_project(0,   32,  900,  668);
    if (pr == NULL)
    {
        win_printf("cannot create plot region");
        return D_O_K;
    }
   if (newplot == 0)
   {
     opx = create_and_attach_one_plot(pr,16,16,0);
     set_plot_title(opx,"All trajectories, coordinate X");
     set_plot_x_title(opx,"Frame");
     set_plot_y_title(opx,"X coordinate");
     opy = create_and_attach_one_plot(pr,16,16,0);
     set_plot_title(opy,"All trajectories, coordinate Y");
     set_plot_x_title(opy,"Frame");
     set_plot_y_title(opy,"Y coordinate");
     opz = create_and_attach_one_plot(pr,16,16,0);
     set_plot_title(opz,"All trajectories, coordinate Z");
     set_plot_x_title(opz,"Frame");
     set_plot_y_title(opz,"Z coordinate");
   }

   ntraj = 0;
   mtraj = 16;
   dstrajx = calloc(mtraj,sizeof(d_s*));
   dstrajy = calloc(mtraj,sizeof(d_s*));
   dstrajz = calloc(mtraj,sizeof(d_s*));
   map_pixel_ratio_of_image_and_screen(ois, 1, 1);
   imr->screen_scale = 56;

int frameer = framee;
int n = 0;
int icurmin = -1;
float mindis = FLT_MAX;
float dis = 0;
if (framee == -1 ) frameer = INT_MAX;
BITMAP *bmp = NULL;
switch_project_to_this_imreg(imr);
d = find_dialog_associated_to_imr(imr, NULL);

char fullfile[512];
char basename[512];
if (save) do_select_file(basename, sizeof(basename), "IMAGE-BMP_FILE\0", "bmp;pcx;tga", "Exporting image\0", 0);
char max = 0,mmax = 0;

if (save && autoscale && operation != 3)
{
  for (int i = 0;i<autoscale_fr;i++)
  {
    mmax = 0;
    n = switch_frame(ois,i);
    if (operation == 1) compute_im_log(ois,&max);
    else if (operation == 2) compute_im_sqrt(ois,&max);
    else compute_im_max(ois,&max);
    if (max > mmax) mmax = max;

  }
  ois->z_black = 0;
  ois->z_white = 1.2*mmax;

}


for (int i =frames;i<frameer;i++)
   {
     n = switch_frame(ois,i);



     if (n!=i) break;

     dsbeads = find_beads_in_partial_OI(ois,0,ois->im.nx-1,0,ois->im.ny-1,threshold, xw, yw, distance_x_max, distance_y_max,0);
     if (save && (operation == 1)) compute_im_log(ois,&max);
     if (save && (operation == 2)) compute_im_sqrt(ois,&max);
     if (save && (operation == 3))
     {
       for (int j = 0;j<dsbeads->nx;j++)
       {
         equalize_fringe(ois,dsbeads->xd[j], dsbeads->yd[j],dsbeads->xe[j], dsbeads->ye[j], xw, yw);

         }
     }

     refresh_image(imr,UNCHANGED);
     acquire_bitmap(screen);

     for (int j = 0;j<dsbeads->nx;j++)
     {
       draw_moving_shear_rect_vga_screen_unit_NI(dsbeads->xd[j], dsbeads->yd[j],dsbeads->xe[j], dsbeads->ye[j], xw, yw,
              0, 0,0, lightgreen, imr->screen_scale, j, imr->one_i->bmp.stuff,0);
       }
       bmp = (BITMAP *)imr->one_i->bmp.stuff;

       blit(imr->one_i->bmp.stuff,screen,0,0,imr->x_off + d->x, imr->y_off - bmp->h + d->y, bmp->w, bmp->h);
       release_bitmap(screen);
       if (save)
       {
         get_palette(pal);
       sprintf(fullfile, "%s%04d.bmp",basename,i);
       save_bitmap(fullfile, bmp, pal);
     }
     for (int ib = 0;ib<dsbeads->nx;ib++)
     {
       x = 0.5*dsbeads->xe[ib] + 0.5*dsbeads->xd[ib];
       y = 0.5*dsbeads->ye[ib] + 0.5*dsbeads->yd[ib];
       z = (dsbeads->xe[ib] - dsbeads->xd[ib]);
       added = 0;
       mindis = FLT_MAX;
       for (int icur = 0; icur <ntraj;icur++)
       {
         if (i-dstrajx[icur]->xd[dstrajx[icur]->nx - 1] <= max_lost_images)
         {
           dis = sqrt((x-dstrajx[icur]->yd[dstrajx[icur]->nx - 1])*(x-dstrajx[icur]->yd[dstrajx[icur]->nx - 1])+(y-dstrajy[icur]->yd[dstrajx[icur]->nx - 1])*(y-dstrajy[icur]->yd[dstrajx[icur]->nx - 1]));
         if ((dis < mindis) && (dis <(i-dstrajx[icur]->xd[dstrajx[icur]->nx - 1])*stich_distance))
         {
           icurmin = icur;
           mindis = dis;
           added = 1;
         }
       }
     }

      if (added)
      {
           add_new_point_to_ds(dstrajx[icurmin],i,x);
           add_new_point_to_ds(dstrajy[icurmin],i,y);
           add_new_point_to_ds(dstrajz[icurmin],i,z);
         }


       else
       {
         if (newplot)
         {
           op = create_and_attach_one_plot(pr,16,16,0);
           dsx = op->dat[0];
           dsy = create_and_attach_one_ds(op,16,16,0);
           dsz = create_and_attach_one_ds(op,16,16,0);

           set_plot_title(op,"Trajectory %d, X,Y,Z",ntraj);
           set_plot_x_title(op,"Frame");
           set_plot_y_title(op,"Position");

         }


         else
         {
            dsx = create_and_attach_one_ds(opx,16,16,0);
            dsy = create_and_attach_one_ds(opy,16,16,0);
            dsz = create_and_attach_one_ds(opz,16,16,0);
         }

         set_ds_source(dsx,"Trajectory %d, Coordinate X ", ntraj);
         set_ds_source(dsy,"Trajectory %d, Coordinate Y ", ntraj);
         set_ds_source(dsz,"Trajectory %d, Coordinate Z ", ntraj);


         dsx->nx = dsx->ny = dsy->nx = dsy->ny = dsz->nx = dsz->ny = 1;
         dsx->xd[0] = dsy->xd[0] = dsz->xd[0] = i;
         dsx->yd[0] = x;
         dsy->yd[0] = y;
         dsz->yd[0] = z;

         dstrajx[ntraj] = dsx;
         dstrajy[ntraj] = dsy;
         dstrajz[ntraj] = dsz;

         ntraj++;
         if (ntraj >= mtraj-1)
         {
           mtraj+=16;
           dstrajx = realloc(dstrajx,mtraj*sizeof(d_s*));
           dstrajy = realloc(dstrajy,mtraj*sizeof(d_s*));
           dstrajz = realloc(dstrajz,mtraj*sizeof(d_s*));
         }

       }

       }



     free_data_set(dsbeads);
   }

   return 0;


 }



int merge_all_projects_plots(void)
{


    XV_ARRAY(pltreg*,project);
    XV_VAR(int,project_n);


  if (updating_menu_state != 0 ) return D_O_K;
  int ii = 0;
  ii = win_printf_OK("I see %d projects. Continue ? \n",project_n);
  if (ii == WIN_CANCEL) return D_O_K;


  pltreg *pr = NULL;
  O_p *opx, *opy, *opz, *Opxm, *Opym, *Opzm;
  float Xdx, XYdy, Zdy;
  d_s *dds = NULL;


  pr = project[1];
  opx = pr->o_p[0];
  opz = pr->o_p[2];
  Xdx = opx->xu[opx->c_xu] -> dx;
  XYdy = opx->yu[opx->c_yu] -> dx;
  Zdy = opz->yu[opz->c_yu] -> dx;

  Opxm = pr->o_p[0];
  Opym = pr->o_p[1];
  Opzm = pr->o_p[2];


  for (int j = 2; j<= project_n;j++)
  {
    pr = project[j];
    opx = pr->o_p[1];
    if (opx->xu[opx->c_xu] -> dx != Xdx || opx->yu[opx->c_yu] -> dx != XYdy) return(win_printf_OK("Problem Y unit with project %d\n",j));
    opx = pr->o_p[0];
    if (opx->xu[opx->c_xu] -> dx != Xdx || opx->yu[opx->c_yu] -> dx != XYdy) return(win_printf_OK("Problem X unit with project %d\n",j));
    opx = pr->o_p[2];
    if (opx->xu[opx->c_xu] -> dx != Xdx || opx->yu[opx->c_yu] -> dx != Zdy) return(win_printf_OK("Problem  Z unit with project %d\n",j));
    opx = pr->o_p[0];
    opy = pr->o_p[1];
    opz = pr->o_p[2];

    if (opx->n_dat != opy->n_dat)return (win_printf_OK("Problèmes d'OP"));
    if (opx->n_dat != opz->n_dat) return (win_printf_OK("Problèmes d'OP"));


    for (int i = 0;i<opx->n_dat;i++)
    {
      if (opx->dat[i]->nx != opy->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));
      if (opx->dat[i]->nx != opz->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));

      dds = duplicate_data_set(opx->dat[i], NULL);
      if (dds == NULL)    return D_O_K;
      for (int k = 0; k<dds->nx;k++)
      {
        dds->xd[k] += 1500 * (j-1);
      }
      add_one_plot_data(Opxm, IS_DATA_SET, (void *)dds);


      dds = duplicate_data_set(opy->dat[i], NULL);
      if (dds == NULL)    return D_O_K;
      for (int k = 0; k<dds->nx;k++)
      {
        dds->xd[k] += 1500 * (j-1);
      }
      add_one_plot_data(Opym, IS_DATA_SET, (void *)dds);


      dds = duplicate_data_set(opz->dat[i], NULL);
      if (dds == NULL)    return D_O_K;
      for (int k = 0; k<dds->nx;k++)
      {
        dds->xd[k] += 1500 * (j-1);
      }
      add_one_plot_data(Opzm, IS_DATA_SET, (void *)dds);
  }
}

Opxm->need_to_refresh=1;
Opzm->need_to_refresh=1;
Opym->need_to_refresh=1;

return 0;

}








 int remove_point_using_range_in_multiple_ds(O_p *src_op, O_p **other_op_list, int n_other_op,int ds_index, d_s **src_ds_ptr,
                              float range_x_min, float range_x_max,
                              float range_y_min, float range_y_max,
                              bool remove_inside)
 {
     d_s *src_ds = *src_ds_ptr;
     int min = (src_ds->nx > src_ds->ny) ? src_ds->nx : src_ds->ny;
     int n_keep = 0;
     float **xl = NULL;
     float **yl = NULL;

     xl = (float **)calloc(n_other_op+1,sizeof(float*));
     yl = (float **)calloc(n_other_op+1,sizeof(float*));



       if (remove_inside)
       {
           for (int i = min - 1 ; i >= 0 ; i--)
           {
               if (src_ds->xd[i] >= range_x_max || src_ds->xd[i] < range_x_min
                       || src_ds->yd[i] >= range_y_max || src_ds->yd[i] < range_y_min)
               {
                   n_keep++;
               }
           }
       }
       else
       {
           for (int i = min - 1 ; i >= 0 ; i--)
           {
               if (src_ds->xd[i] < range_x_max && src_ds->xd[i] >= range_x_min
                       && src_ds->yd[i] < range_y_max && src_ds->yd[i] >= range_y_min)
               {
                   n_keep++;
               }
           }
       }

       if (n_keep >0)
       {

       for (int i = 0;i<n_other_op+1;i++)
       {
           xl[i] = (float *)calloc(n_keep, sizeof(float));
           yl[i] = (float *)calloc(n_keep, sizeof(float));
         }

         if (remove_inside)
         {
             for (int i = min - 1, k = n_keep - 1 ; i >= 0 ; i--)
             {
                 if (src_ds->xd[i] >= range_x_max || src_ds->xd[i] < range_x_min
                         || src_ds->yd[i] >= range_y_max || src_ds->yd[i] < range_y_min)
                 {
                   xl[0][k] = src_ds->xd[i];
                  yl[0][k] = src_ds->yd[i];

                          for (int j = 1;j<n_other_op+1;j++)
                          {
                                          xl[j][k] = other_op_list[j-1]->dat[ds_index]->xd[i];
                                         yl[j][k] = other_op_list[j-1]->dat[ds_index]->yd[i];
                                       }
                                       k--;
                 }
             }
         }
         else
         {
             for (int i = min - 1, k = n_keep - 1  ; i >= 0 ; i--)
             {
                 if (src_ds->xd[i] < range_x_max && src_ds->xd[i] >= range_x_min
                         && src_ds->yd[i] < range_y_max && src_ds->yd[i] >= range_y_min)
                 {
                   xl[0][k] = src_ds->xd[i];
                  yl[0][k] = src_ds->yd[i];

                          for (int j = 1;j<n_other_op+1;j++)
                          {
                                          xl[j][k] = other_op_list[j-1]->dat[ds_index]->xd[i];
                                         yl[j][k] = other_op_list[j-1]->dat[ds_index]->yd[i];
                                       }
                                       k--;
                 }
             }
         }

         free(src_ds->xd);
         free(src_ds->yd);
         for (int i = 1;i<n_other_op+1;i++)
         {
           free(other_op_list[i-1]->dat[ds_index]->xd);
           free(other_op_list[i-1]->dat[ds_index]->yd);
          }
         src_ds->xd = xl[0];
         src_ds->yd = yl[0];

         for (int i = 1;i<n_other_op+1;i++)
         {
           other_op_list[i-1]->dat[ds_index]->xd = xl[i];
           other_op_list[i-1]->dat[ds_index]->yd = yl[i];

          }



         src_ds->mx = src_ds->my = src_ds->nx = src_ds->ny = n_keep;
         for (int i = 1;i<n_other_op+1;i++)
         {
           other_op_list[i-1]->dat[ds_index]->mx = other_op_list[i-1]->dat[ds_index]->my  = other_op_list[i-1]->dat[ds_index]->nx = other_op_list[i-1]->dat[ds_index]->ny  = n_keep;

          }

      free(xl);
      free(yl);
    }

    else
    {
      remove_ds_from_op(src_op,src_ds);
      for (int i = 1;i<n_other_op+1;i++)
      {
        remove_ds_from_op(other_op_list[i-1],other_op_list[i-1]->dat[ds_index]);

       }
    }


      src_op->need_to_refresh = 1;
      for (int i = 1;i<n_other_op+1;i++)
      {
        other_op_list[i-1]->need_to_refresh = 1;

       }
       if (n_keep == 0) return 1;
       return 0;
 }

 int remove_visible_invisible_points_in_3ds(void)
 {
     int k;
     int  index, n_start, n_end;
     pltreg *pr = NULL;
     d_s *ds = NULL;
     O_p *op = NULL;
     bool remove_inside = false;
     O_p *other_op_list[2];

     if (updating_menu_state != 0)
     {
         return D_O_K;
     }

     if (active_menu->dp == NULL)
     {
         return D_O_K;
     }

     index = active_menu->flags & 0xFFFFFF00; // one ds or all ds ?

     if (strstr((const char *) active_menu->dp, "INVISIBLE") != NULL)
     {
         remove_inside = false;
     }
     else if (strstr((const char *) active_menu->dp, "VISIBLE") != NULL)
     {
         remove_inside = true;
     }
     else
     {
         return D_O_K;
     }

     if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)
     {
         return D_O_K;
     }

     if (index & DS_ONE)  // we work on active dataset
     {
         n_start = op->cur_dat;
         n_end   = op->cur_dat + 1;
     }
     else if (index & DS_ALL)  // we work on all datasets
     {
         n_start = 0;
         n_end   = op->n_dat;
     }
     else
     {
         return (D_O_K);
     }
     int j = 0;
 // win_printf("index = %d = %x\ni will work on datasets in [%d , %d[", index, index, n_start, n_end);
      for (int i = 0 ;i<3;i++)
      {
        if (i!=pr->cur_op)
        {
          other_op_list[j] = pr->o_p[i];
          j++;

        }
      }
     for (k = n_start;k<n_end;)
     {
         ds = op->dat[k]; // we work on dataset k
         if (remove_point_using_range_in_multiple_ds(op, other_op_list,2,k, &ds, op->x_lo, op->x_hi, op->y_lo, op->y_hi, remove_inside))
         {
           n_end --;
         }
         else k++;
     }

     if (remove_inside && (index & DS_ALL))
     {
         auto_x_limit();
         auto_y_limit();

     }
     op->need_to_refresh = 1;
     return refresh_plot(pr, UNCHANGED);
 }

 int correct_cell_angle(O_p *opx,O_p *opy,O_p *opz)
 {
   d_s *dsxz, *dsyz;
   double *axz = NULL;
   double *ayz;
   if (opx->n_dat != opy->n_dat)return (win_printf_OK("Problèmes d'OP"));
   if (opx->n_dat != opz->n_dat) return (win_printf_OK("Problèmes d'OP"));

   dsxz = build_data_set(16,16);
   dsxz->nx = dsxz-> ny = 0;

   dsyz = build_data_set(16,16);
   dsyz->nx = dsyz-> ny = 0;

   for (int i = 0;i<opx->n_dat;i++)
   {
     if (opx->dat[i]->nx != opy->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));
     if (opx->dat[i]->nx != opz->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));
     for (int j = 0;j<opx->dat[i]->nx;j++)
     {
       add_new_point_to_ds(dsxz,opx->dat[i]->yd[j],opz->dat[i]->yd[j]);
       add_new_point_to_ds(dsyz,opy->dat[i]->yd[j],opz->dat[i]->yd[j]);
     }
   }

   if (fit_ds_to_xn_polynome(dsxz, 2, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, &axz) < 0) return(win_printf_OK("Problem of fit"));
   if (fit_ds_to_xn_polynome(dsyz, 2, -FLT_MAX, FLT_MAX, -FLT_MAX, FLT_MAX, &ayz) < 0) return(win_printf_OK("Problem of fit"));


   for (int i = 0;i<opx->n_dat;i++)
   {
     if (opx->dat[i]->nx != opy->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));
     if (opx->dat[i]->nx != opz->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));
     for (int j = 0;j<opx->dat[i]->nx;j++)
     {
       opz->dat[i]->yd[j] -= axz[0] + axz[1] * opx->dat[i] ->yd[j];
       opz->dat[i]->yd[j] -= ayz[0] + ayz[1] * opy->dat[i] ->yd[j];

     }
   }

   free(axz);
   free(ayz);


   return 0;
 }

 d_s *least_square_affit_fit_on_ds_between_indexes_with_sig2(const d_s *src_ds,  int min_idx, int max_idx,
         double *out_a, double *out_b, float *out_sig2, float *max_sig_2)
 {
     d_s *dest_ds = NULL;
     int nx;
     float minx = FLT_MAX;
     float maxx = -FLT_MAX;
     float miny = FLT_MAX;
     float maxy = -FLT_MAX;
     float sig2m = 0;
     float tmp = 0;
     double sy2 = 0, sy = 0, sx = 0, sx2 = 0, sxy = 0, sig2 = 0, sig_2 = 0, chi2 = 0, ny = 0;
     double a = 0, b = 0;
     float af,bf = 0;
     char  c[256] = {0};
     if (out_sig2 == NULL) return(win_printf_OK("No sig2\n"));
     *out_sig2 = 0;

     if (min_idx < 0)
     {
         min_idx = 0;
     }

     if (max_idx < 0)
     {
         max_idx = src_ds->nx;
     }

     if (max_idx >= src_ds->nx) max_idx = src_ds->nx - 1;

     nx = max_idx - min_idx;

     if (max_idx - min_idx < 2)
     {
         (win_printf_OK("no fit on less than 2 points"));
         return NULL;
     }



         minx = FLT_MAX;
         maxx = -FLT_MAX;
         miny = FLT_MAX;
         maxy = -FLT_MAX;

         for (int i = min_idx; i < max_idx ; i++)
         {
             sy += src_ds->yd[i];
             sy2 += src_ds->yd[i] * src_ds->yd[i];
             sx += src_ds->xd[i];
             sx2 += src_ds->xd[i] * src_ds->xd[i];
             sxy += src_ds->xd[i] * src_ds->yd[i];
             minx = (src_ds->xd[i] < minx) ? src_ds->xd[i] : minx;
             maxx = (src_ds->xd[i] > maxx) ? src_ds->xd[i] : maxx;
             miny = (src_ds->yd[i] < miny) ? src_ds->yd[i] : miny;
             maxy = (src_ds->yd[i] > maxy) ? src_ds->yd[i] : maxy;
         }

             b = (sx2 * nx) - sx * sx;

             if (b == 0)
             {
                 win_printf_OK("\\Delta  = 0 \n can't fit that!");
                 return NULL;
             }

             a = sxy * nx - sx * sy;
             a /= b;
             b = (sx2 * sy - sx * sxy) / b;


         dest_ds = build_data_set(2, 2); //create_and_attach_one_ds(dest_op, 2, 2, 0);

         if (dest_ds == NULL)
         {
             win_printf_OK("can't create data set");
             return  NULL;
         }
         *max_sig_2 = 0;
         af = (float) a;
         bf = (float) b;
         for (int i = min_idx ; i<max_idx;i++)
         {
           sig2m = (src_ds->yd[i] - af*src_ds->xd[i] - bf)*(src_ds->yd[i] - af*src_ds->xd[i] - bf);
           if (sig2m > *max_sig_2) *max_sig_2 = sig2m;
           *out_sig2 += sig2m;
         }


         tmp = maxx - minx;
         minx = minx - (tmp * .05);
         maxx = maxx + (tmp * .05);
         dest_ds->xd[0] = minx;
         dest_ds->xd[1] = maxx;
         dest_ds->yd[0] = a * minx + b;
         dest_ds->yd[1] = a * maxx + b;
         inherit_from_ds_to_ds(dest_ds, src_ds);

    *out_sig2 /= max_idx-min_idx;


     if (out_a)
     {
         *out_a = a;
     }

     if (out_b)
     {
         *out_b = b;
     }

     return dest_ds;
 }



int find_good_linear_fit(O_p *op, int frame_range, float chi2_max, float close_to_surf, float min_amp_z)
{
  double a =0;
  double b =0;
  float sig2 = 0;
  d_s *dstemp = NULL;
  d_s *src = NULL;
  float max_delta = 0;

  for (int i =0;i<op->n_dat;i++)
  {
    src = op->dat[i];
    for (int j = 0;j<src->nx-frame_range;j++)
    {
      if (src->yd[j]>close_to_surf) continue;
      if (fabs(src->yd[j+frame_range]-src->yd[j])<min_amp_z) continue;

      dstemp = least_square_affit_fit_on_ds_between_indexes_with_sig2(src,j, j+frame_range,&a,&b,&sig2,&max_delta);
      if (max_delta > chi2_max)
      {
        free_data_set(dstemp);
        dstemp = NULL;
      }
      else {
        j += frame_range -1;
        add_data_to_one_plot(op,IS_DATA_SET,(void *)dstemp);
        set_ds_point_symbol(op->dat[op->n_dat-1], "\\pt8 \\di");
      }

    }
  }

  return 0;


}


int do_find_good_linear_fit(void)
{
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL;
  int i = 0;
  static int frame_range = 8;
  static float sig2_max = 2;
  if (updating_menu_state != 0) return D_O_K;
  static float close_to_surf = 42;
  static float min_amp_z = 50;
  if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
  {
      return (win_printf_OK("Could not find plot data"));
  }
  close_to_surf /= op->yu[op->c_yu]->dx;
  min_amp_z /= op->yu[op->c_yu]->dx;
  i = win_scanf("Find good fits over %d points with mx sig 2 per point %f\n"
  "starting point close smaller than %f\n"
  "Min amplitude z %f \n",&frame_range,&sig2_max,&close_to_surf,&min_amp_z);
  find_good_linear_fit(op, frame_range, sig2_max,close_to_surf,min_amp_z);
  close_to_surf *= op->yu[op->c_yu]->dx;
  min_amp_z *= op->yu[op->c_yu]->dx;


  return 0;
}


 int draw_velocity_XYZ(pltreg *pr, O_p *opx, O_p *opy, O_p *opz, int max_frame_lost, int absz)
 {

   if (opx->n_dat != opy->n_dat)return (win_printf_OK("Problèmes d'OP"));
   if (opx->n_dat != opz->n_dat) return (win_printf_OK("Problèmes d'OP"));
   float vxh = 0, vyh = 0, vzh = 0;
   O_p *opvx=NULL, *opvy = NULL, *opvz = NULL, *opvt = NULL, *opvxy = NULL, *opvlat = NULL;
   d_s *dsvx = NULL, *dsvy = NULL, *dsvz = NULL, *dsvt = NULL, *dsvxy = NULL;
   d_s *dsvlat = NULL;
   float xf = 0;

   opvx = create_and_attach_one_plot(pr,16,16,INSERT_HERE);
   opvy = create_and_attach_one_plot(pr,16,16,INSERT_HERE);
   opvz = create_and_attach_one_plot(pr,16,16,INSERT_HERE);
   opvt = create_and_attach_one_plot(pr,16,16,INSERT_HERE);
   opvxy = create_and_attach_one_plot(pr,16,16,INSERT_HERE);
   opvlat = create_and_attach_one_plot(pr,16,16,INSERT_HERE);


  set_plot_title(opvx,"V_x");
  set_plot_title(opvy,"V_y");
  set_plot_title(opvz,"V_z");
  set_plot_title(opvt,"V");
  set_plot_title(opvxy,"Vx vs Vy");
  set_plot_title(opvlat,"V lateral");



  set_plot_x_title(opvx,"Time");
  set_plot_y_title(opvx,"Velocity");
  set_plot_x_title(opvy,"Time");
  set_plot_y_title(opvy,"Velocity");
  set_plot_x_title(opvz,"Time");
  set_plot_y_title(opvz,"Velocity");
  set_plot_x_title(opvlat,"Time");
  set_plot_y_title(opvlat,"Velocity lateral");
  set_plot_x_title(opvxy,"Velocity X");
  set_plot_y_title(opvxy,"Velocity Y");

  if (absz)
  {
    set_plot_x_title(opvx,"Z");
    set_plot_x_title(opvy,"Z");
    set_plot_x_title(opvz,"Z");
    set_plot_x_title(opvlat,"Z");
    set_plot_x_title(opvt,"Z");


  }

  create_attach_select_x_un_to_op(opvx, IS_SECOND, 0,(float) opx->xu[opx->c_xu]->dx, 0, 0, "s");
  create_attach_select_x_un_to_op(opvy, IS_SECOND, 0,(float) opx->xu[opx->c_xu]->dx, 0, 0, "s");
  create_attach_select_x_un_to_op(opvz, IS_SECOND, 0,(float) opx->xu[opx->c_xu]->dx, 0, 0, "s");
  create_attach_select_x_un_to_op(opvt, IS_SECOND, 0,(float) opx->xu[opx->c_xu]->dx, 0, 0, "s");
  create_attach_select_x_un_to_op(opvlat, IS_SECOND, 0,(float) opx->xu[opx->c_xu]->dx, 0, 0, "s");


  create_attach_select_y_un_to_op(opvx, 0, 0,(float) opx->yu[opx->c_yu]->dx/opx->xu[opx->c_xu]->dx, -6, 0, "\\mu{}m/s");
  create_attach_select_y_un_to_op(opvy, 0, 0,(float) opy->yu[opy->c_yu]->dx/opy->xu[opy->c_xu]->dx, -6, 0, "\\mu{}m/s");
  create_attach_select_y_un_to_op(opvz, 0, 0,(float) opz->yu[opz->c_yu]->dx/opz->xu[opz->c_xu]->dx, -6, 0, "\\mu{}m/s");
  create_attach_select_y_un_to_op(opvxy, 0, 0,(float) opx->yu[opx->c_yu]->dx/opx->xu[opx->c_xu]->dx, -6, 0, "\\mu{}m/s");
  create_attach_select_y_un_to_op(opvlat, 0, 0,(float) opx->yu[opx->c_yu]->dx/opx->xu[opx->c_xu]->dx, -6, 0, "\\mu{}m/s");

  create_attach_select_x_un_to_op(opvxy, 0, 0,(float) opx->yu[opx->c_yu]->dx/opx->xu[opx->c_xu]->dx, -6, 0, "\\mu{}m/s");


  create_attach_select_y_un_to_op(opvt, 0, 0,(float)1, -6, 0, "\\mu{}m/s");


  if (absz)
    {


        create_attach_select_x_un_to_op(opvx, IS_METER, 0,(float) opz->yu[opz->c_yu]->dx, -6, 0, "\\mu{}m");
        create_attach_select_x_un_to_op(opvy, IS_METER, 0,(float) opz->yu[opz->c_yu]->dx, -6, 0, "\\mu{}m");
        create_attach_select_x_un_to_op(opvz, IS_METER, 0,(float) opz->yu[opz->c_yu]->dx, -6, 0, "\\mu{}m");
        create_attach_select_x_un_to_op(opvt, IS_METER, 0,(float) opz->yu[opz->c_yu]->dx, -6, 0, "\\mu{}m");
        create_attach_select_x_un_to_op(opvlat, IS_METER, 0,(float) opz->yu[opz->c_yu]->dx, -6, 0, "\\mu{}m");



    }


   for (int i = 0;i<opx->n_dat;i++)
   {

               if (opx->dat[i]->nx != opy->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));
               if (opx->dat[i]->nx != opz->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));

               if ( i >= opvx->n_dat)
               {
                dsvx = create_and_attach_one_ds(opvx,16,16,0);
               }
               else
               {
                 dsvx = opvx->dat[i];
               }
               if ( i >= opvy->n_dat)
               {
                dsvy = create_and_attach_one_ds(opvy,16,16,0);
               }
               else
               {
                 dsvy = opvy->dat[i];
               }
               if ( i >= opvz->n_dat)
               {
                dsvz = create_and_attach_one_ds(opvz,16,16,0);
               }
               else
               {
                 dsvz = opvz->dat[i];
               }
               if ( i >= opvt->n_dat)
               {
                dsvt = create_and_attach_one_ds(opvt,16,16,0);
               }
               else
               {
                 dsvt = opvt->dat[i];
               }
               if ( i >= opvxy->n_dat)
               {
                dsvxy = create_and_attach_one_ds(opvxy,16,16,0);
               }
               else
               {
                 dsvxy = opvxy->dat[i];
               }
               if ( i >= opvlat->n_dat)
               {
                dsvlat = create_and_attach_one_ds(opvlat,16,16,0);
               }
               else
               {
                 dsvlat = opvlat->dat[i];
               }

               dsvz->nx = dsvz->ny = dsvx->nx = dsvx->ny = dsvy->nx = dsvy->ny = dsvt->nx = dsvt->ny = 0;
               dsvlat->nx = dsvlat->ny = 0;
               dsvxy->nx = dsvxy->ny = 0;

               for (int j = 0;j<opx->dat[i]->nx-1;j++)
               {
                  if (opx->dat[i]->xd[j+1]-opx->dat[i]->xd[j] <= max_frame_lost+1)
                  {
                    vxh = (opx->dat[i]->yd[j+1]-opx->dat[i]->yd[j])/(opx->dat[i]->xd[j+1]-opx->dat[i]->xd[j]);
                    vyh = (opy->dat[i]->yd[j+1]-opy->dat[i]->yd[j])/(opy->dat[i]->xd[j+1]-opy->dat[i]->xd[j]);
                    vzh = (opz->dat[i]->yd[j+1]-opz->dat[i]->yd[j])/(opz->dat[i]->xd[j+1]-opz->dat[i]->xd[j]);
                    if (absz)
                    {
                      xf = opz->dat[i]->yd[j];
                    }
                    else
                    {
                      xf = opx->dat[i]->xd[j];
                    }
                    add_new_point_to_ds(dsvx,xf,vxh);
                    add_new_point_to_ds(dsvy,xf,vyh);
                    add_new_point_to_ds(dsvz,xf,vzh);
                    add_new_point_to_ds(dsvlat,xf,sqrt(vxh*vxh+vyh*vyh));

                    //if (vxh*vxh+vyh*vyh != 0) add_new_point_to_ds(dsvxy,vxh/sqrt(vxh*vxh+vyh*vyh),vyh/sqrt(vxh*vxh+vyh*vyh));
                    //if (vxh != 0) add_new_point_to_ds(dsvxy,i,atan2(vyh,vxh));

                    add_new_point_to_ds(dsvt,xf,sqrt((vxh*vxh)*(opx->yu[opx->c_yu]->dx*opx->yu[opx->c_yu]->dx)+vyh*vyh*(opy->yu[opy->c_yu]->dx*opy->yu[opy->c_yu]->dx)+vzh*vzh
                  *(opz->yu[opz->c_yu]->dx*opz->yu[opz->c_yu]->dx))/opx->xu[opx->c_xu]->dx);


                  }


               }
 }

 return 0;
}

int correct_lost_frame_number_using_velocity(O_p *opx, O_p *opy, O_p *opz, O_p *vx, O_p *vy, float ratio, float ratiotraj)
{
  if (opx->n_dat != opy->n_dat)return (win_printf_OK("Problèmes d'OP"));
  if (opx->n_dat != opz->n_dat) return (win_printf_OK("Problèmes d'OP"));
  int *jumpsnb = NULL;
  int *trajnb = NULL;
  int *shifts = NULL;
  float frmax = 0;
  for (int i = 0;i<opx->n_dat-1;i++)
  {
    for (int j = 0;j<opx->dat[i]->nx;j++)
    {
       if (opx->dat[i]->xd[j] > frmax) frmax = opx->dat[i]->xd[j];
    }
  }

  jumpsnb = (int *) calloc((int) frmax+1,sizeof(int));
  trajnb = (int *) calloc((int) frmax+1, sizeof(int));
  shifts = (int *) calloc((int) frmax+1, sizeof(int));


  for (int i = 1;i<opx->n_dat-1;i++)
  {

              if (opx->dat[i]->nx != opy->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));
              if (opx->dat[i]->nx != opz->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));
              if (opx->dat[i]->nx != vy->dat[i]->nx+1) return(win_printf_OK("Some trajectories with different number of points"));
              if (opx->dat[i]->nx != vx->dat[i]->nx+1) return(win_printf_OK("Some trajectories with different number of points"));

              for (int j = 0;j<opx->dat[i]->nx;j++)
              {
              if (fabs(vx->dat[i]->yd[j]/vx->dat[i]->yd[j-1]) > ratio || fabs(vy->dat[i]->yd[j]/vy->dat[i]->yd[j-1]) > ratio) jumpsnb[ (int) opx->dat[i]->xd[j]] +=1;
              trajnb[(int) opx->dat[i]->xd[j]] += 1;
            }
}

for (int i = 0;i<(int)frmax;i++)
{
  if (((float)jumpsnb[i])/((float)trajnb[i]) > ratiotraj) shifts[i+1] = shifts[i] + 1;
  else shifts[i+1] = shifts[i];

}

for (int i = 0;i<opx->n_dat-1;i++)
{
  for (int j = 0;j<opx->dat[i]->nx;j++)
  {
     opx->dat[i]->xd[j] += shifts[(int) opx->dat[i]->xd[j]];
     opy->dat[i]->xd[j] += shifts[(int) opy->dat[i]->xd[j]];
     opz->dat[i]->xd[j] += shifts[(int) opz->dat[i]->xd[j]];

  }
}

free(shifts);
free(jumpsnb);
free(trajnb);

return 0;
}

int do_correct_frame_lost(void)
{

       O_p *op;
       d_s  *dsi;
       pltreg *pr;
       int i = 0;
       float ratio_velocity = 1.7;
       float ratio_traj = 0.8;




      if (updating_menu_state != 0)
           {
               return D_O_K;
           }



       if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
       {
           return (win_printf_OK("Could not find plot data"));
       }
       if (pr->n_op < 3) return(win_printf_OK("Not entough plots\n"));


      i =  win_scanf("This routine will remove lost frames by shifting the number of frames in x\n"
      "Ratio of successive velocities to find a lost frame %f\n"
      "Ratio of trajectories having this hjump to confirm lost frame %f\n",&ratio_velocity, &ratio_traj);

   if (i==WIN_CANCEL) return 0;

  correct_lost_frame_number_using_velocity(pr->o_p[0],pr->o_p[1],pr->o_p[2], pr->o_p[3], pr->o_p[4], ratio_velocity, ratio_traj);
  return 0;

}


int remove_short_trajectories(O_p *opx, O_p *opy, O_p *opz, int nx_min, float delta_z_min)
{
  float minz = 0, maxz=0;
  if (opx->n_dat != opy->n_dat)return (win_printf_OK("Problèmes d'OP"));
  if (opx->n_dat != opz->n_dat) return (win_printf_OK("Problèmes d'OP"));

  for (int i = 0;i<opx->n_dat-1;)
  {

              if (opx->dat[i]->nx != opy->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));
              if (opx->dat[i]->nx != opz->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));

              if (opx->dat[i]->nx < nx_min)
              {
                remove_one_plot_data (opx, IS_DATA_SET, (void *)opx->dat[i]);
                remove_one_plot_data (opy, IS_DATA_SET, (void *)opy->dat[i]);
                remove_one_plot_data (opz, IS_DATA_SET, (void *)opz->dat[i]);
                continue;
              }

              else
              {
                minz = FLT_MAX;
                maxz = FLT_MIN;
                for (int j = 0; j <opz->dat[i]->nx;j++)
                {
                    if (opz->dat[i]->yd[j] < minz) minz = opz->dat[i]->yd[j];
                    if (opz->dat[i]->yd[j] > maxz) maxz = opz->dat[i]->yd[j];
                }
                if (maxz-minz < delta_z_min)
                {
                  remove_one_plot_data (opx, IS_DATA_SET, (void *)opx->dat[i]);
                  remove_one_plot_data (opy, IS_DATA_SET, (void *)opy->dat[i]);
                  remove_one_plot_data (opz, IS_DATA_SET, (void *)opz->dat[i]);
                  continue;
                }
              }
              i++;
}


return 0;
}




int do_remove_short_trajectories(void)
{

       O_p *op;
       d_s  *dsi;
       pltreg *pr;
       int i = 0;
       int nx_min = 50;
       float delta_z_min = 40;


      if (updating_menu_state != 0)
           {
               return D_O_K;
           }



       if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
       {
           return (win_printf_OK("Could not find plot data"));
       }
       if (pr->n_op < 3) return(win_printf_OK("Not entough plots\n"));




      i =  win_scanf("This routine will remove short trajectories for three first plots (X,Y,Z)\n"
      "Minimum number of point for each trajectory %d \n"
      "Minimum delta %f\n",&nx_min, &delta_z_min);

   if (i==WIN_CANCEL) return 0;
   O_p *opz = pr->o_p[2];

  remove_short_trajectories(pr->o_p[0],pr->o_p[1],pr->o_p[2],nx_min,delta_z_min / opz->yu[opz->c_yu]->dx);
  return 0;

}





 int remove_traj_points_outside_range(O_p *opx, O_p *opy, O_p *opz, float xmin, float xmax, float ymin, float ymax)
 {

       int min = 0;
       int n_keep = 0;
       float *Xx, *Xy, *Yx, *Yy, *Zx, *Zy = NULL;
       if (opx->n_dat != opy->n_dat)return (win_printf_OK("Problèmes d'OP"));
       if (opx->n_dat != opz->n_dat) return (win_printf_OK("Problèmes d'OP"));


          for (int i = 0;i<opx->n_dat;0)
          {
            if (opx->dat[i]->nx != opy->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));
            if (opx->dat[i]->nx != opz->dat[i]->nx) return(win_printf_OK("Some trajectories with different number of points"));
            min = opx->dat[i]->nx;
            n_keep = 0;
           for (int j = 0;j<opx->dat[i]->nx;j++)
           {
               if (opx->dat[i]->yd[j] < xmax && opx->dat[i]->yd[j] >= xmin
                       && opy->dat[i]->yd[j] < ymax && opy->dat[i]->yd[j] >= ymin)
               {
                   n_keep++;
               }
           }

           if (n_keep <= 0)
           {
               remove_one_plot_data(opx, IS_DATA_SET, (void *)opx->dat[i]);
               remove_one_plot_data(opy, IS_DATA_SET, (void *)opy->dat[i]);
               remove_one_plot_data(opz, IS_DATA_SET, (void *)opz->dat[i]);
               //  n_end--; // there is one dataset less in the plot
           }
           else

            {

              Xx = (float *)calloc(n_keep, sizeof(float));
              Xy = (float *)calloc(n_keep, sizeof(float));
              Yx = (float *)calloc(n_keep, sizeof(float));
              Yy = (float *)calloc(n_keep, sizeof(float));
              Zx = (float *)calloc(n_keep, sizeof(float));
              Zy = (float *)calloc(n_keep, sizeof(float));

              for (int j = min - 1, k = n_keep - 1 ; j >= 0 ; j--)
              {
                 if (opx->dat[i]->yd[j] < xmax && opx->dat[i]->yd[j] >= xmin
                       && opy->dat[i]->yd[j] < ymax && opy->dat[i]->yd[j] >= ymin)
                  {
                      Xx[k] = opx->dat[i]->xd[j];
                      Xy[k] = opx->dat[i]->yd[j];
                      Yx[k] = opy->dat[i]->xd[j];
                      Yy[k] = opy->dat[i]->yd[j];
                      Zx[k] = opz->dat[i]->xd[j];
                      Zy[k--] = opz->dat[i]->yd[j];
                  }
              }

              free(opx->dat[i]->xd);
              free(opx->dat[i]->yd);
              free(opy->dat[i]->xd);
              free(opy->dat[i]->yd);
              free(opz->dat[i]->xd);
              free(opz->dat[i]->yd);
              opx->dat[i]->xd = Xx;
              opx->dat[i]->yd = Xy;
              opy->dat[i]->xd = Yx;
              opy->dat[i]->yd = Yy;
              opz->dat[i]->xd = Zx;
              opz->dat[i]->yd = Zy;

                opx->dat[i]->nx =   opx->dat[i]->ny =   opy->dat[i]->nx =   opy->dat[i]->ny =   opz->dat[i]->nx =   opz->dat[i]->ny =   n_keep;
                opx->dat[i]->mx =   opx->dat[i]->my =   opy->dat[i]->mx =   opy->dat[i]->my =   opz->dat[i]->mx =   opz->dat[i]->my =   n_keep;

                i++;


              }
         }



   // win_printf("on dataset %d, i will keep %d points", k, n_keep);






        opx->need_to_refresh = 1;
        opy->need_to_refresh = 1;
        opz->need_to_refresh = 1;

       return D_O_K;

 }

 int do_draw_velocities(void)
 {

        O_p *op;
        d_s  *dsi;
        pltreg *pr;
        int i = 0;
        static int nb_frames_lost = 1;
        static int absz = 0;





       if (updating_menu_state != 0)
            {
                return D_O_K;
            }



        if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
        {
            return (win_printf_OK("Could not find plot data"));
        }
        if (pr->n_op < 3) return(win_printf_OK("Not entough plots\n"));


       i =  win_scanf("This routine will only keep points of trajectories in this part of the screen\n"
       "will plot velocities. Be careful to give correct unit to the graphs in each direction\n"
     "Don't compute velocities if more than %d frames lost\n"
     "Draw as a function of z %b \n",&nb_frames_lost,&absz);

    if (i==WIN_CANCEL) return 0;

   draw_velocity_XYZ(pr,pr->o_p[0],pr->o_p[1],pr->o_p[2],nb_frames_lost,absz);
   return 0;

 }

int do_keep_center_of_screen(void)
{

         O_p *op;
         d_s  *dsi;
         pltreg *pr;
         int i = 0;
         float xmin = 1000.0, xmax = 3000.0, ymin =1000.0, ymax = 2000.0;





      if (updating_menu_state != 0)
           {
               return D_O_K;
           }



       if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
       {
           return (win_printf_OK("Could not find plot data"));
       }
       if (pr->n_op < 3) return(win_printf_OK("Not entough plots\n"));


      i =  win_scanf("This routine will only keep points of trajectories in this part of the screen\n"
      "xmin %f xmax %f \n"
    "ymin %f ymax %f\n",&xmin,&xmax,&ymin,&ymax);

   if (i==WIN_CANCEL) return 0;

  remove_traj_points_outside_range(pr->o_p[0],pr->o_p[1],pr->o_p[2],xmin,xmax,ymin,ymax);
  return 0;

}

 int do_correct_cell_angle(void)
 {

       O_p *op;
       d_s  *dsi;
       pltreg *pr;
       int i = 0;





    if (updating_menu_state != 0)
         {
             return D_O_K;
         }



     if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
     {
         return (win_printf_OK("Could not find plot data"));
     }

    i =  win_scanf("This routine will correct the three first plots considering that they correspond to X,Y,and Z plots\n"
   "It will correct the Z to remove the correlation between X,Y,Z due to the cell angle\n");

   if (pr->n_op < 3) return("Not entough plots\n");
 if (i==WIN_CANCEL) return 0;
correct_cell_angle(pr->o_p[0],pr->o_p[1],pr->o_p[2]);
correct_cell_angle(pr->o_p[0],pr->o_p[1],pr->o_p[2]);
correct_cell_angle(pr->o_p[0],pr->o_p[1],pr->o_p[2]);

return 0;



 }





int do_find_fringes_in_image(void)
{
    if (updating_menu_state != 0) return 0;
  O_i *ois = NULL;
  d_s *dsfringes = NULL;
  imreg *imr = NULL;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2) return D_O_K;


    static int xco=0,xend=2,yco=0,yend=200,threshold=50,xw=64,yw=32;
    xend = ois->im.nx;

  int i = win_scanf("Find fringes between xco %d and xend %d \n"
                "Between yco %d and yend %d \n"
                "Using intensity threshold %d \n"
                  "Width x %d\n"
                "Width y %d\n",&xco,&xend,&yco,&yend,&threshold,&xw,&yw);
  clock_t begin = clock();
  dsfringes = find_fringes_in_partial_OI(ois,  xco,  xend,  yco,  yend, dsfringes,  threshold,  xw,  yw);
  clock_t end = clock();
  double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  for (i = 0;i<dsfringes->nx;i++)
  {
    draw_moving_shear_rect_vga_screen_unit_NI(dsfringes->xd[i], dsfringes->yd[i],dsfringes->xd[i], dsfringes->yd[i], xw, yw,
           0, 0,0, lightgreen, imr->screen_scale, i, imr->one_i->bmp.stuff,0);
    }
  free_data_set(dsfringes);
  win_printf_OK("Function took %lf seconds\n",time_spent);

  return 0;
}


int do_find_beads_in_image(void)
{
    if (updating_menu_state != 0) return 0;
  O_i *ois = NULL;
  d_s *dsbeads = NULL;
  imreg *imr = NULL;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return D_O_K;
  static  int old = 0;


  static int xco=0,xend=2,yco=0,yend=200,threshold=50,xw=256,yw=64,max_distance_x = 256,max_distance_y = 128;
  xend = ois->im.nx;

  int i = win_scanf("Find fringes between xco %d and xend %d \n"
                "Between yco %d and yend %d \n"
                "Using intensity threshold %d \n"
                "Width x %d \n"
                "Width y %d \n"
                "Max distance x %d \n"
                "Max distance y %d \n"
                "old %b\n",&xco,&xend,&yco,&yend,&threshold,&xw,&yw,&max_distance_x,&max_distance_y,&old);
  clock_t begin = clock();
  dsbeads = find_beads_in_partial_OI(ois,  xco,  xend,  yco,  yend,  threshold,  xw,  yw,max_distance_x,max_distance_y,old);
  clock_t end = clock();
  double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
  for (i = 0;i<dsbeads->nx;i++)
  {
    draw_moving_shear_rect_vga_screen_unit_NI(dsbeads->xd[i], dsbeads->yd[i],dsbeads->xe[i], dsbeads->ye[i], xw, yw,
           0, 0,0, lightgreen, imr->screen_scale, i, imr->one_i->bmp.stuff,0);
    }
  free_data_set(dsbeads);
  win_printf_OK("Function took %lf seconds\n",time_spent);

  return 0;
}


int do_compute_variance_of_photons(void)

{

  if (updating_menu_state != 0) return 0;

O_i *ois= NULL;
O_p *op;
d_s *ds=NULL;
imreg *imr;
int nfi;
int size;
int i;
static int average_range=1024;
static int frame_shift=128;
int number_of_averages=0;

if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
  return D_O_K;
nfi = abs(ois->im.n_f);
nfi = (nfi == 0) ? 1 : nfi;


int pxg = 0,pxd = 0, pxb=0 ,pxh = 0 ,t1=0 ,t2=0;

pxd = (pxd<=0) ? ois->im.nx : pxd;
pxh = (pxh<=0) ? ois->im.ny : pxh;
t2 = (t2<=0) ? nfi -1: t2;

average_range = (average_range<=0) ? nfi : average_range;

i = win_scanf("Draw variance as a function of mean value\n"
  "From frame %6d to frame %6d\n"
  "From line %6d to line %6d\n"
  "From row %6d to raw %6d \n"
  "Average on %6d points, frame shift %6d\n"
  , &t1,&t2,&pxb,&pxh,&pxg,&pxd,&average_range,&frame_shift);


if (i==WIN_CANCEL) return 0;

number_of_averages = (t2-t1+1)/frame_shift;
while ((number_of_averages-1)*frame_shift+average_range>(t2-t1) + 1) number_of_averages--;
if (number_of_averages<=0) return 0;
size=number_of_averages*(pxh-pxb+1)*(pxd-pxg+1);


if ((size<=0) || (pxd>ois->im.nx) || (pxh>ois->im.ny)  || (t2 > nfi) )
{
  win_printf_OK("Please check your indices\n");
  return 0;
}



op = create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
if (op == NULL) return (win_printf_OK("cant create plot!"));
ds = op->dat[0];
set_plot_title(op,"Variance vs mean\n");
set_ds_treatement(ds,"Variance vs mean\n");
inherit_from_im_to_ds(ds, ois);
uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
alloc_data_set_x_error(ds);
alloc_data_set_y_error(ds);

variance_function_of_mean_value(ois,ds,pxg,pxd,pxb,pxh,t1,t2,frame_shift,average_range);
return 0;
}

int draw_z_angles(d_s *dsr, O_p *opx, O_p *opy, O_p *opz)
{
  double vz = 0, vx = 0, vy = 0, db = 0;
  if (opx->n_dat != opy->n_dat ) return (win_printf_OK("Pb op angles XY"));
  if (opx->n_dat != opz->n_dat ) return (win_printf_OK("Pb op angles XZ"));
  dsr->nx = dsr->ny = 0;
  for (int i = 0;i < opx->n_dat;i++)
  {
    if (opx->dat[i]->nx != opy->dat[i]->nx) return (win_printf_OK("Pb ds"));
    if (opx->dat[i]->nx != opz->dat[i]->nx) return (win_printf_OK("Pb ds"));

    compute_least_square_fit_on_ds(opx->dat[i],1, &vx,&db);
    compute_least_square_fit_on_ds(opy->dat[i],1, &vy,&db);
    compute_least_square_fit_on_ds(opz->dat[i],1, &vz,&db);


    vx *= opx->yu[opx->c_yu]->dx / opx->xu[opx->c_xu]->dx;
    vy *= opy->yu[opy->c_yu]->dx / opy->xu[opy->c_xu]->dx;
    vz *= opz->yu[opz->c_yu]->dx / opz->xu[opz->c_xu]->dx;
    if ((vx*vx+vy*vy) ==  0) continue;
    add_new_point_to_ds(dsr,opx->dat[i]->xd[opx->dat[i]->nx/2],(float)atan(vz/(sqrt(vx*vx+vy*vy))));


  }
  return 0;

}

int cut_traj_in_z(O_p *opx, O_p *opy, O_p *opz, float xmin, float xmax, float ymin, float ymax)
{
  O_p* other_op_list[2];
  other_op_list[0] = opx;
  other_op_list[1] = opy;
  if (opx->n_dat != opy->n_dat ) return (win_printf_OK("Pb op 1"));
  if (opx->n_dat != opz->n_dat ) return (win_printf_OK("Pb op 2"));
  d_s *ds = NULL;
  for (int k =0; k < opz->n_dat;)
  {
    ds = opz->dat[k];
    if (remove_point_using_range_in_multiple_ds(opz, other_op_list,2,k, &ds, xmin, xmax, ymin, ymax, 0) == 0)
    {
      k++;
    }
}

if (opx->n_dat != opy->n_dat ) return (win_printf_OK("Pb op 3"));
if (opx->n_dat != opz->n_dat ) return (win_printf_OK("Pb op 4"));
for (int k = 0 ;k<opz->n_dat;k++)
{
  for (int j = 0;j<opz->dat[k]->nx;j++)
  {
  if (opx->dat[k]->xd[j] != opz->dat[k]->xd[j]) return (win_printf_OK("Pb XZ xd at ds %d index %d",k,j));
  if (opx->dat[k]->xd[j] != opy->dat[k]->xd[j]) return (win_printf_OK("Pb XY xd at ds %d index %d",k,j));

  }
}
return 0;

}

int do_draw_z_angles(void)
{

  O_p *op;
  d_s *dsi = NULL;

  pltreg *pr;
  int i = 0;
  O_p *opd = NULL;
  static float zmin = -10;
  static float zmax = 600;
  static float cut_frame = 1;
  O_p *opx = NULL, *opy = NULL , *opz = NULL;

 if (updating_menu_state != 0)
      {
          return D_O_K;
      }

  if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
  {
      return (win_printf_OK("Could not find plot data"));
  }
  if (pr->n_op < 3) return(win_printf_OK("Not entough plots\n"));

  i = win_scanf("Angles will be computed for each trajectories for z between %f and %f \n"
    "after having cut trajectories separated by more than %f frames \n",&zmin,&zmax,&cut_frame);

opx = pr->o_p[0];
opy = pr->o_p[1];
opz = pr->o_p[2];
cut_traj_in_z(opx,opy,opz,-FLT_MAX,FLT_MAX, zmin, zmax);
split_ds_of_op(opx, 1,0, cut_frame);
split_ds_of_op(opy, 1,0, cut_frame);
split_ds_of_op(opz, 1,0, cut_frame);

remove_short_trajectories(opx,opy,opz,2,0);


opd = create_and_attach_one_plot(pr,16,16,0);
set_plot_title(opd,"Mean Z angle");
set_plot_x_title(opd,"Time");
set_plot_y_title(opd,"Mean Z angle of trajectory");
uns_op_2_op_by_type(pr->o_p[0], IS_X_UNIT_SET, opd, IS_X_UNIT_SET);

if (i==WIN_CANCEL) return 0;

draw_z_angles(opd->dat[0], pr->o_p[0],pr->o_p[1],pr->o_p[2]);
return 0;


}


int do_draw_integrated_signal_on_area(void)
{

    if (updating_menu_state != 0) return 0;

  O_i *ois= NULL;
  O_p *op;
  d_s *ds=NULL;
  imreg *imr;
  int nfi;
  int size;
  int i;
  static int average_range=1024;
  static int frame_shift=128;
  int number_of_averages=0;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return D_O_K;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;


  int pxg = 0,pxd = 0, pxb=0 ,pxh = 0 ,t1=0 ,t2=0;

  pxd = (pxd<=0) ? ois->im.nx : pxd;
  pxh = (pxh<=0) ? ois->im.ny : pxh;
  t2 = nfi - 1;

  average_range = (average_range<=0) ? nfi : average_range;

  i = win_scanf("Draw time variation of integrated photon number\n"
    "From frame %6d to frame %6d\n"
    "From line %6d to line %6d\n"
    "From row %6d to row %6d \n"
    "Average on %6d points, frame shift %6d\n"
    , &t1,&t2,&pxb,&pxh,&pxg,&pxd,&average_range,&frame_shift);

    number_of_averages = (t2-t1+1)/frame_shift;
    while ((number_of_averages-1)*frame_shift+average_range>(t2-t1) + 1) number_of_averages--;
    if (number_of_averages<=0) return 0;
    size=number_of_averages;


    if ((size<=0) || (pxd>ois->im.nx) || (pxh>ois->im.ny)  || (t2 > nfi) )
    {
      win_printf_OK("Please check your indices\n");
      return 0;
    }



    op = create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
    if (op == NULL) return (win_printf_OK("cant create plot!"));
    ds = op->dat[0];
    set_plot_title(op,"Variance vs mean\n");
    set_ds_treatement(ds,"Variance vs mean\n");
    inherit_from_im_to_ds(ds, ois);
    uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
    uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
    alloc_data_set_x_error(ds);
    alloc_data_set_y_error(ds);

    integrate_photons_on_area(ois,ds,pxg,pxd,pxb,pxh,t1,t2,frame_shift,average_range);

    return 0;



}

int do_show_temporal_phase_x_y(void)

{

  if (updating_menu_state != 0) return 0;

O_i *ois= NULL;
O_p *op;
d_s *ds=NULL, *ds2=NULL;
imreg *imr;
int nfi;
int size;
int i;
int average_range=1024;
int frame_shift=128;
int frame_number=0;
int number_of_averages=0;
int mode_left=0,mode_right=0;
float freq;
float freq_l,freq_r;

if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
  return D_O_K;
nfi = abs(ois->im.n_f);
nfi = (nfi == 0) ? 1 : nfi;


int pxg = 0,pxd = 0, pxb=0 ,pxh = 0 ,t1=0 ,t2=0;

pxd = (pxd<=0) ? ois->im.nx : pxd;
pxh = (pxh<=0) ? ois->im.ny : pxh;
t2 = (t2<=0) ? nfi-1 : t2;

average_range = (average_range<=0) ? nfi : average_range;

i = win_scanf("Draw temporal phase as a function of mean value\n"
  "From frame %6d to frame %6d (power of 2)\n"
  "From line %6d to line %6d\n"
  "From row %6d to raw %6d \n"
  "Looking for Fourier peak bewteen %6d and %6d (Hz)\n"
  "Frequency %6d(Hz)\n"
  , &t1,&t2,&pxb,&pxh,&pxg,&pxd,&freq_l,&freq_r,&freq);

frame_number=(t2-t1+1);
mode_left=(int) (freq_l*frame_number/freq);
mode_right=(int) (freq_r*frame_number/freq);



if (i==WIN_CANCEL) return 0;

size=(pxd-pxg+1)*(pxh-pxb+1);

if ((size<=0) || (pxd>ois->im.nx) || (pxh>ois->im.ny)  || (t2 > nfi) )
{
  win_printf_OK("Please check your indices\n");
  return 0;
}

op = create_and_attach_op_to_oi(ois, size, size, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
if (op == NULL) return (win_printf_OK("cant create plot!"));
ds = op->dat[0];
inherit_from_im_to_ds(ds, ois);
uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);


ds2 = create_and_attach_one_ds(op, size, size, 0);
inherit_from_im_to_ds(ds, ois);
uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);

if(draw_specific_mode_phase_pixel(ois,op,pxg,pxd,pxb,pxh,t1,t2,mode_left,mode_right,ds->xd,ds2->xd,ds2->yd,freq)) return 0;
for (i=0;i<size;i++)
{
  ds->yd[i]=ds2->yd[i];
}

set_ds_treatement(ds,"Phase vs x\n");
set_ds_treatement(ds2,"Phase vs y\n");

return 0;
}



int draw_specific_mode_phase_pixel(O_i *ois,O_p *op, int pxg, int pxd, int pxb, int pxh, int t1, int t2, int mode_left, int mode_right, float *x_array, float *y_array, float *phi_array, float freq)
{
  int i,j,l;
  int lx= pxd-pxg+1;
  int ly = pxh-pxb+1;
  fft_plan *fp=NULL;
  int frame_numb=t2-t1+1;
  fp=fftbt_init(NULL, frame_numb);
  int index=0;
  float im,re;
  int taille = lx*ly;
  int mode_main=-1;
  d_s *dstemp=NULL;
  dstemp=create_and_attach_one_ds(op,frame_numb,frame_numb,0);
  for (i=0;i<frame_numb;i++) dstemp->xd[i]=i*freq/frame_numb;


  for (i=0;i<ly && index<taille;i++)
  {
    for (j=0;j<lx && index<taille;j++)
    {
        if (extract_z_partial_profile_from_movie(ois, pxg+j, pxb+i, dstemp->yd,t1,t2)) return(win_printf("data set has null pointer i=%6d j=%6d\n",i,j));
        realtr1bt(fp, dstemp->yd);  fftbt(fp, dstemp->yd, 1);  realtr2bt(fp, dstemp->yd, 1);
        if (mode_main==-1)
        {
          for (l=mode_left;l<=mode_right;l++)
          {
            if (dstemp->yd[2*l]*dstemp->yd[2*l]+dstemp->yd[2*l+1]*dstemp->yd[2*l+1]>dstemp->yd[2*mode_main]*dstemp->yd[2*mode_main]+dstemp->yd[2*mode_main+1]*dstemp->yd[2*mode_main+1])
              mode_main=l;
          }
        }
        re=dstemp->yd[2*mode_main];
        im=dstemp->yd[2*mode_main+1];
        phi_array[index]=(im == 0.0 && re == 0.0) ? 0.0 : atan2(im, re);
        x_array[index]=j;
        y_array[index]=i;
        index++;

     }


 }

 for (i=0;i<frame_numb/2;i++)
 {
   dstemp->yd[i]=dstemp->yd[2*i]*dstemp->yd[2*i]+dstemp->yd[2*i+1]*dstemp->yd[2*i+1];

 }




 return 0;
}


int variance_function_of_mean_value(O_i *ois,d_s *dsr,int pxg,int pxd,int pxb,int pxh,int t1, int t2,int frame_shift, int average_range)
  {
    int i,j,t;
    int lx= pxd-pxg+1;
    int ly = pxh-pxb+1;
    int deltat=t2-t1+1;
    int tb,te;
    d_s *dstmp=build_data_set(lx,lx);

    int number_of_averages = deltat/frame_shift;
    while ((number_of_averages-1)*frame_shift+average_range>deltat) number_of_averages--;
    int taille=number_of_averages*lx*ly;
    if (dsr==NULL)
    {
      win_printf_OK("No dataset found\n");
      return 0;
    }
    if ((dsr->nx<taille) || (dsr->ny<taille))
    {
      win_printf_OK("Variance computation : dataset is too small\n");
      return 0;
    }

    for (int kav=0;kav<number_of_averages;kav++)
     {
       tb=kav*frame_shift;
       te=kav*frame_shift+average_range-1;


       for (t=tb;t<=te;t++)
       {
         switch_frame(ois,t);
         for (i=0;i<ly;i++)
         {
           extract_raw_partial_line(ois,pxb+i,dstmp->yd,pxg,lx);
           for (j=0;j<lx;j++)
           {
                dsr->yd[kav*lx*ly+i*lx+j]+=(dstmp->yd[j])*(dstmp->yd[j]);
                dsr->xd[kav*lx*ly+i*lx+j]+=(dstmp->yd[j]);
                dsr->xe[kav*lx*ly+i*lx+j] = j;
                dsr->ye[kav*lx*ly+i*lx+j] = i;
              }

        }
    }
  }

    for (i=0;i<=taille;i++)
      {
        dsr->yd[i]/=(float)average_range;
        dsr->xd[i]/=(float)average_range;
        dsr->yd[i]=dsr->yd[i]-dsr->xd[i]*dsr->xd[i];

      }

    free_data_set(dstmp);
    return 0;

  }


  int integrate_photons_on_area(O_i *ois,d_s *dsr,int pxg,int pxd,int pxb,int pxh,int t1, int t2,int frame_shift, int average_range)
    {
      int i,j,t;
      int lx= pxd-pxg+1;
      int ly = pxh-pxb+1;
      int deltat=t2-t1+1;
      int tb,te;
      d_s *dstmp=build_data_set(lx,lx);

      int number_of_averages = deltat/frame_shift;
      while ((number_of_averages-1)*frame_shift+average_range>deltat) number_of_averages--;
      int taille=number_of_averages;
      if (dsr==NULL)
      {
        win_printf_OK("No dataset found\n");
        return 0;
      }
      if ((dsr->nx<taille) || (dsr->ny<taille))
      {
        win_printf_OK("Integrate computation : dataset is too small\n");
        return 0;
      }
      dsr->nx = 0;
      dsr->ny = 0;

      for (int kav=0;kav<number_of_averages;kav++)
       {
         tb=kav*frame_shift;
         te=kav*frame_shift+average_range-1;
         dsr->xd[dsr->nx] = 0.5*tb+0.5*te;
         dsr->yd[dsr->nx] = 0;

         for (t=tb;t<=te;t++)
         {
           switch_frame(ois,t);
           for (i=0;i<ly;i++)
           {
             extract_raw_partial_line(ois,pxb+i,dstmp->yd,pxg,lx);
             for (j=0;j<lx;j++)
             {

                  dsr->yd[dsr->nx] += dstmp->yd[j];
                }

          }
      }
          dsr->yd[dsr->nx] /= (te-tb+1);
          dsr->nx += 1;
          dsr->ny += 1;
      }


      free_data_set(dstmp);
      return 0;

    }



MENU *NoiseInImage_image_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;

  add_item_to_menu (mn,"Draw variance as a function of intensity", do_compute_variance_of_photons,NULL,0,NULL);
  add_item_to_menu (mn,"Draw phase of a specific Fourier temporal mode ", do_show_temporal_phase_x_y,NULL,0,NULL);
  add_item_to_menu (mn,"Draw integrated signal", do_draw_integrated_signal_on_area,NULL,0,NULL);
  add_item_to_menu(mn,"Fringe detect",do_find_fringes_in_image,NULL,0,NULL);
  add_item_to_menu(mn,"Beads detect",do_find_beads_in_image,NULL,0,NULL);
  add_item_to_menu(mn,"Automatize sigma computation",do_automatize_noise_analysis_and_photons_numbers,NULL,0,NULL);
  add_item_to_menu(mn,"Create background minimum",do_bg_minimum,NULL,0,NULL);
  add_item_to_menu(mn,"Draw SDI trajectories",do_draw_SDI_trajectories,NULL,0,NULL);







  return mn;
}

MENU *NoiseInImage_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;

  add_item_to_menu(mn,"Automatize sigma computation",do_automatize_noise_analysis_and_photons_numbers,NULL,0,NULL);
  add_item_to_menu(mn, "Remove Visible points (all ds)",	remove_visible_invisible_points_in_3ds,	NULL,	DS_ALL, (char *) "VISIBLE");
  add_item_to_menu(mn, "Remove Invisible points (all ds)",	remove_visible_invisible_points_in_3ds,	NULL,	DS_ALL, (char *) "INVISIBLE");
  add_item_to_menu(mn, "Remove Visible points (one ds)",	remove_visible_invisible_points_in_3ds,	NULL,	DS_ONE, (char *) "VISIBLE");
  add_item_to_menu(mn, "Remove Invisible points (one ds)",	remove_visible_invisible_points_in_3ds,	NULL,	DS_ONE, (char *) "INVISIBLE");
  add_item_to_menu(mn,"Correct cell angle",do_correct_cell_angle,NULL,0,NULL);
  add_item_to_menu(mn,"Remove points outside a ROI",do_keep_center_of_screen,NULL,0,NULL);
  add_item_to_menu(mn,"Draw velocities",do_draw_velocities,NULL,0,NULL);
  add_item_to_menu(mn,"Correct lost frames",do_correct_frame_lost,NULL,0,NULL);
  add_item_to_menu(mn, "Erase short trajectories", do_remove_short_trajectories, NULL,0,NULL);
  add_item_to_menu(mn,"Merge all project plots",merge_all_projects_plots,NULL,0,NULL);
  add_item_to_menu(mn,"Find good linear fits",do_find_good_linear_fit,NULL,0,NULL);
  add_item_to_menu(mn,"Dump mean angle of each traj",do_draw_z_angles,NULL,0,NULL);





  return mn;
}



int	NoiseInImage_main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  add_image_treat_menu_item ( "Noise in image", NULL, NoiseInImage_image_menu(), 0, NULL);
  add_plot_treat_menu_item ( "Noise in image", NULL, NoiseInImage_plot_menu(), 0, NULL);

  return D_O_K;
}

int	NoiseInImage_unload(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  remove_item_to_menu(plot_treat_menu, "Noise in image", NULL, NULL);
  return D_O_K;
}

#endif
