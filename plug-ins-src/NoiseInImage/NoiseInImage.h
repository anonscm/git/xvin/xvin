#ifndef _NOISEINIMAGE_H_
#define _NOISEINIMAGE_H_
#define VISIBLE		0x00000100
#define INVISIBLE	0x00001000
#define DS_ONE      0x00010000
#define DS_ALL      0x00100000
PXV_FUNC(MENU*, NoiseInImage_image_menu, (void));
PXV_FUNC(int, do_compute_variance_of_photons,(void));
PXV_FUNC(int, variance_function_of_mean_value,(O_i *ois,d_s *dsr,int pxg,int pxd,int pxb,int pxh,int t1, int t2, int frame_shift, int average_range));
PXV_FUNC(int, draw_specific_mode_phase_pixel,(O_i *ois, O_p *op,int pxg, int pxd, int pxb, int pxh, int t1, int t2, int mode_left, int mode_right, float *x_array, float *y_array, float *phi_array,float freq));
PXV_FUNC(int, do_show_temporal_phase_x_y,(void));
PXV_FUNC(int, integrate_photons_on_area,(O_i *ois,d_s *dsr,int pxg,int pxd,int pxb,int pxh,int t1, int t2,int frame_shift, int average_range));
PXV_FUNC(int, do_draw_integrated_signal_on_area,(void));

XV_FUNC(int, split_ds_of_op,(O_p *op, int ds_all, int j , int ar));


#endif
