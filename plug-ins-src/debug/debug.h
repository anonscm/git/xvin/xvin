#ifndef _DEBUG_H_
#define _DEBUG_H_

PXV_FUNC(int, do_kb_reinit, (void));
PXV_FUNC(MENU*, debug_plot_menu, (void));
PXV_FUNC(int, debug_main, (int argc, char **argv));
#endif

