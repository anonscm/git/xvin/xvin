/* restore the keyborad if it diseappear...
  */
#ifndef _DEBUG_C_
#define _DEBUG_C_

# include "allegro.h"
# include "xvin.h"
#include "xv_tools_lib.h" // tfor do_test str_to_index/float

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "debug.h"


int do_kb_reinit(void)
{   int i;

	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine tries to restore keyboard keys\n"
                                   "if they don't respond...");
	}
	
    // while (keypressed()) win_printf("a key was previously pressed : #%d", readkey());
    //   clear_keybuf();
    
    i=win_printf("I detected that the keyboard %s poll.\n"
    			"Click OK to see reinit keyboard, or CANCEL to skip",
    			keyboard_needs_poll() ? "needs" : "doesn't");
    if (i!=CANCEL)
    {//  poll_keyboard();
	     remove_keyboard();
	     install_keyboard(); 
    }

  	return D_O_K;
}




int do_check_io(void)
{
//	GFX_MODE_LIST *modes;
	int 	x, y;
	char	*message=NULL;

	if(updating_menu_state != 0)	return D_O_K;

	
	message=my_sprintf(message, "{\\color{lightgreen}Allegro version %d.%d.%d} (WIP %d, named %s)", 
						ALLEGRO_VERSION, ALLEGRO_SUB_VERSION, ALLEGRO_WIP_VERSION, ALLEGRO_WIP_VERSION, ALLEGRO_VERSION_STR);
	message=my_sprintf(message, " dated : %d (%s)\n", ALLEGRO_DATE, ALLEGRO_DATE_STR);
	message=my_sprintf(message, "{\\color{lightgreen}operating system : ");
	
	switch (os_type)
    {
    	case OSTYPE_UNKNOWN:	message=my_sprintf(message, "unknown, or regular MSDOS");
      		break;
      	case OSTYPE_WIN3:		message=my_sprintf(message, "Windows 3.1 or earlier");
      		break;
      	case OSTYPE_WIN95:		message=my_sprintf(message, "Windows 95");
      		break;
    	case OSTYPE_WIN98:		message=my_sprintf(message, "Windows 98");
    		break;
    	case OSTYPE_WINME:		message=my_sprintf(message, "Windows ME");
    		break;
    	case OSTYPE_WINNT:		message=my_sprintf(message, "Windows NT");
    		break;
    	case OSTYPE_WIN2000:	message=my_sprintf(message, "Windows 2000");
    		break;
    	case OSTYPE_WINXP:		message=my_sprintf(message, "Windows XP");
    		break;
    	case OSTYPE_OS2:		message=my_sprintf(message, "OS/2");
    		break;
    	case OSTYPE_WARP:		message=my_sprintf(message, "OS/2 Warp 3");
    		break;
    	case OSTYPE_DOSEMU:		message=my_sprintf(message, "Linux DOSEMU");
    		break;
    	case OSTYPE_OPENDOS:	message=my_sprintf(message, "Caldera OpenDOS");
    		break;
    	case OSTYPE_LINUX:		message=my_sprintf(message, "Linux");
    		break;
    	case OSTYPE_SUNOS:		message=my_sprintf(message, "SunOS/Solaris");
    		break;
		case OSTYPE_FREEBSD:	message=my_sprintf(message, "FreeBSD");
    		break;
    	case OSTYPE_NETBSD :	message=my_sprintf(message, "NetBSD");
    		break;
    	case OSTYPE_IRIX:		message=my_sprintf(message, "IRIX");
    		break;
    	case OSTYPE_DARWIN:		message=my_sprintf(message, "Darwin");
    		break;
    	case OSTYPE_QNX:		message=my_sprintf(message, "QNX");
    		break;
    	case OSTYPE_UNIX:		message=my_sprintf(message, "Unknown Unix variant");
    		break;
    	case OSTYPE_BEOS:		message=my_sprintf(message, "BeOS");
    		break;
    	case OSTYPE_MACOS:		message=my_sprintf(message, "MacOS");
    		break;
    	case OSTYPE_MACOSX:		message=my_sprintf(message, "MacOS X");
    		break;
    	default:                message=my_sprintf(message, "operating system unknown");
    }
    message=my_sprintf(message, "}");
    if (os_multitasking) 		message=my_sprintf(message, " (multitasking OK)");
	 
    message = my_sprintf(message, "\nCPU vendor : %s\nprocessor : ", cpu_vendor);
    
    switch (cpu_family)
    {
    	case CPU_FAMILY_UNKNOWN:	message=my_sprintf(message, "unknown");
      		break;
    	case CPU_FAMILY_I386:	message=my_sprintf(message, "Intel-compatible 386");
      		break;
    	case CPU_FAMILY_I486:	message=my_sprintf(message, "Intel-compatible 486");
      		break;
    	case CPU_FAMILY_I586:	message=my_sprintf(message, "Pentium ");
             switch (cpu_family)
             {
                 case CPU_MODEL_PENTIUM:                    message=my_sprintf(message, "(generic Intel)"); break;
                 case CPU_MODEL_PENTIUMP54C:                message=my_sprintf(message, "(Intel P54C)"); break;
                 case CPU_MODEL_PENTIUMOVERDRIVE:           message=my_sprintf(message, "(Intel overdrive)"); break;
                 case CPU_MODEL_PENTIUMOVERDRIVEDX4:        message=my_sprintf(message, "(Intel overdrive DX4)"); break;
                 case CPU_MODEL_CYRIX:                      message=my_sprintf(message, "(Cyrix)"); break;
                 case CPU_MODEL_K5:                         message=my_sprintf(message, "(AMD K5)"); break;
                 case CPU_MODEL_K6:                         message=my_sprintf(message, "(AMD K6)"); break;
                 // default:
             }
      		break;
    	case CPU_FAMILY_I686:                               
             switch (cpu_family)
             {
                 case CPU_MODEL_PENTIUMPROA:                message=my_sprintf(message, "Intel Pentium Pro A"); break;
                 case CPU_MODEL_PENTIUMPRO:                 message=my_sprintf(message, "Intel Pentium Pro"); break;
//                 case CPU_MODEL_PENTIUMIIKLAMATH:           message=my_sprintf(message, "Intel Pentium II Klamath"); break;
                 case CPU_MODEL_PENTIUMII:                  message=my_sprintf(message, "Intel Pentium II"); break;
                 case CPU_MODEL_CELERON:                    message=my_sprintf(message, "Intel Celeron"); break;
                 case CPU_MODEL_PENTIUMIIIKATMAI:           message=my_sprintf(message, "Intel Pentium III Katmai"); break;
                 case CPU_MODEL_PENTIUMIIICOPPERMINE:       message=my_sprintf(message, "Intel Pentium III Coppermine"); break;
                 case CPU_MODEL_PENTIUMIIIMOBILE:           message=my_sprintf(message, "Intel Pentium III Mobile"); break;
                 case CPU_MODEL_ATHLON:                     message=my_sprintf(message, "AMD Athlon"); break;
                 case CPU_MODEL_DURON:                      message=my_sprintf(message, "AMD Duron"); break;
    	         default:                                   message=my_sprintf(message, "unknown Pentium Pro, II, III or equivalent");
             }
      		break;
    	case CPU_FAMILY_ITANIUM:	message=my_sprintf(message, "Itanium");
      		break;
    	case CPU_FAMILY_POWERPC:	message=my_sprintf(message, "PowerPC ");
    	     switch (cpu_family)
             {
                 case CPU_MODEL_POWERPC_601:                message=my_sprintf(message, "601"); break;
                 case CPU_MODEL_POWERPC_602:                message=my_sprintf(message, "602"); break;
                 case CPU_MODEL_POWERPC_603:                message=my_sprintf(message, "603"); break;
                 case CPU_MODEL_POWERPC_603e:               message=my_sprintf(message, "603e"); break;
                 case CPU_MODEL_POWERPC_603ev:              message=my_sprintf(message, "603ev"); break;
                 case CPU_MODEL_POWERPC_604:                message=my_sprintf(message, "604"); break;
                 case CPU_MODEL_POWERPC_604e:               message=my_sprintf(message, "604e"); break;
                 case CPU_MODEL_POWERPC_620:                message=my_sprintf(message, "620"); break;                                                                                                      
                 case CPU_MODEL_POWERPC_750:                message=my_sprintf(message, "750"); break;
                 case CPU_MODEL_POWERPC_7400:               message=my_sprintf(message, "7400"); break;
                 case CPU_MODEL_POWERPC_7450:               message=my_sprintf(message, "7450"); break;                                  
                 default:                                   message=my_sprintf(message, "(unknown)");
             }
      		break;
    	case CPU_FAMILY_EXTENDED:
             switch (cpu_family)
             {
                 case CPU_MODEL_PENTIUMIV:                  message=my_sprintf(message, "Intel Pentium IV"); break;
                 case CPU_MODEL_XEON:                       message=my_sprintf(message, "Intel Xeon"); break;
                 case CPU_MODEL_ATHLON64:                   message=my_sprintf(message, "AMD Athlon"); break;                                       
                 case CPU_MODEL_OPTERON:                    message=my_sprintf(message, "AMD Opteron"); break;
                 default:                                   message=my_sprintf(message, "Extended CPU model unknow");
             }
      		break;
        default:                message=my_sprintf(message, "unknown");
    }  		
  
    if (cpu_capabilities & CPU_ID)
    { message=my_sprintf(message, "\nfollowing capabilities are available: {\\color{lightred}");
      if (cpu_capabilities & CPU_FPU)      message=my_sprintf(message, "FPU, ");
      if (cpu_capabilities & CPU_IA64)     message=my_sprintf(message, "Intel 64 bit CPU, ");
      if (cpu_capabilities & CPU_AMD64)    message=my_sprintf(message, "AMD 64 bit CPU, ");
      message=my_sprintf(message, "}\nfollowing instruction sets are available: {\\color{lightred}");
      if (cpu_capabilities & CPU_MMX)      message=my_sprintf(message, "MMX, ");
      if (cpu_capabilities & CPU_MMXPLUS)  message=my_sprintf(message, "MMX+, ");
      if (cpu_capabilities & CPU_SSE)      message=my_sprintf(message, "SSE, ");
      if (cpu_capabilities & CPU_SSE2)     message=my_sprintf(message, "SSE2, ");
      if (cpu_capabilities & CPU_SSE3)     message=my_sprintf(message, "SSE3");
      if (cpu_capabilities & CPU_3DNOW)    message=my_sprintf(message, "AMD 3DNow!, ");
      if (cpu_capabilities & CPU_ENH3DNOW) message=my_sprintf(message, "Enhanced 3DNow!, ");
      if (cpu_capabilities & CPU_CMOV)     message=my_sprintf(message, "Pentium Pro ""cmov""");
      message=my_sprintf(message, "}");
    }

    
    message = my_sprintf(message, "\n\n\ndesktop color depth is {\\color{lightred}%d}, resolution {\\color{lightred}", desktop_color_depth());
    if (get_desktop_resolution(&x, &y)==0) message=my_sprintf(message, "%dx%d", x, y);
    else message=my_sprintf(message, "unknown");
    message = my_sprintf(message, "}");
    
    message=my_sprintf(message, "\nXVin color depth is {\\color{lightred}%d}, {\\color{lightred}%s}, refresh rate %d Hz\n\n\n\n", 
    				get_color_depth(), (is_windowed_mode()) ? "windowed" : "full_screen", get_refresh_rate());
    				
    if (mouse_needs_poll())		message=my_sprintf(message, "mouse needs poll, ");
    else 						message=my_sprintf(message, "mouse doesn't need poll, ");
    if (keyboard_needs_poll())	message=my_sprintf(message, "keyboard needs poll\n");
    else 						message=my_sprintf(message, "keyboard doesn't need poll\n");

//	modes 				= get_gfx_mode_list(card);
	
    win_printf(message);
    free(message);
    
	return(D_O_K);
}




MENU *debug_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn, "reinit keyboard",		do_kb_reinit,	NULL,	0,	NULL);
	add_item_to_menu(mn, "system infos",		do_check_io,	NULL,	0,	NULL);
	add_item_to_menu(mn, "string to index",	 	do_try_str_to_index,		NULL,0,	NULL);
	
	return mn;
}

int	debug_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "debug", 	NULL, debug_plot_menu(), 0, NULL);
	return D_O_K;
}

int	debug_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "debug", NULL, NULL);
	return D_O_K;
}
#endif

