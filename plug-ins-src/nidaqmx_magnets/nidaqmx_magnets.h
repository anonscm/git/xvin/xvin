#ifndef _NIDAQMX_MAGNETS_H_
#define _NIDAQMX_MAGNETS_H_

PXV_FUNC(MENU*, nidaqmx_magnets_plot_menu, (void));
PXV_FUNC(int, nidaqmx_magnets_main, (int argc, char **argv));
#endif

