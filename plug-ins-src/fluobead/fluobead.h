#ifndef _FLUOBEAD_H_
#define _FLUOBEAD_H_

PXV_FUNC(int, do_fluobead_average_along_y, (void));
PXV_FUNC(int, draw_ecoli_label, (S_l *s_l, O_i *oi, void *imr));
PXV_FUNC(int, get_number_screen_labels, (O_i *oi));
PXV_FUNC(int, draw_ecoli_label, (S_l *s_l, O_i *oi, void *imr));
PXV_FUNC(int, scatter_plot_bp_peaks, (O_i *oi, d_s *dssc, d_s *dssci, int rot, int debug));

PXV_FUNC(MENU*, fluobead_image_menu, (void));
PXV_FUNC(int, fluobead_main, (int argc, char **argv));
#endif

