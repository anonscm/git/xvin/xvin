/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _FLUOBEAD_C_
#define _FLUOBEAD_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 
# include "../fft2d/fft2d.h"
# include "../trackBead/real_time_avg.h"


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "fluobead.h"
//place here other headers of this plugin 


# define 	WRONG_ARGUMENT		-1
# define 	OUT_MEMORY			-2
# define 	FFT_NOT_SUPPORTED	-15
# define	PB_WITH_EXTREMUM	2
# define	TOO_MUCH_NOISE		4
# define	GETTING_REF_PROFILE	1

int draw_square_label(S_l *s_l, O_i *oi, void *imr)
{
  int xp, yp;

  xp = x_imdata_2_imr((imreg*)imr, (int)(s_l->xla+0.5));
  yp = y_imdata_2_imr((imreg*)imr, (int)(s_l->yla+0.5));
  hline(screen, xp-1, yp+1, xp+1, Lightred);
  hline(screen, xp-1, yp-1, xp+1, Lightred);
  vline(screen, xp-1, yp-1, yp+1, Lightred);
  vline(screen, xp+1, yp-1, yp+1, Lightred);
  return 0;
}

int draw_ecoli_label(S_l *s_l, O_i *oi, void *imr)
{
  int xp, yp, xoff, yoff, sc, itmp;
  DIALOG *di;
  

  di = find_dialog_associated_to_imr((imreg*)imr, NULL);
  sc = ((imreg*)imr)->screen_scale;
  xp = x_imdata_2_imr((imreg*)imr, (int)(s_l->xla+0.5));
  yp = y_imdata_2_imr((imreg*)imr, (int)(s_l->yla+0.5));
  xoff = ((imreg*)imr)->x_off;
  yoff = ((imreg*)imr)->y_off - ((BITMAP*)((imreg*)imr)->one_i->bmp.stuff)->h + di->y;
  //xp += xoff;
  //yp += yoff;
  itmp = (3*28)/sc;
  hline(screen, xp-itmp, yp+itmp, xp+itmp, makecol(0,0,255));
  hline(screen, xp-itmp, yp-itmp, xp+itmp, makecol(0,0,255));
  vline(screen, xp-itmp, yp-itmp, yp+itmp, makecol(0,0,255));
  vline(screen, xp+itmp, yp-itmp, yp+itmp, makecol(0,0,255));
  return 0;
}



O_i	*max_histo_tile_16bits(O_i *ois, int nx)
{
  register int i, j;
  char c1[256];
  int k, c, l, ih;
  unsigned int min, max, ui, tmp, fac;
  unsigned int z[1024];
  float zv[1024], tmpf = 0;
  O_i *oit = NULL, *oid = NULL;
  union pix *ps, *pd;
  int onx, ony, onxt, onyt, ix, iy, tmpi = 1;
	
  if (ois == NULL || ois->im.data_type != IS_UINT_IMAGE)	
    {
      win_printf("Cannot handle images which are not unsigned char!");
      return NULL;
    }
	
  onx = ois->im.nx;
  ony = ois->im.ny;
  onxt = (onx % nx) ? onx/nx + 1 : onx/nx; 
  onyt = (ony % nx) ? ony/nx + 1 : ony/nx; 	
  oit = create_one_image(onxt, onyt, IS_UINT_IMAGE);
  if (oit == NULL)	return NULL;
  for (ix = 0, ps = ois->im.pixel; ix < onxt; ix++)
    {
      for (iy = 0; iy < onyt; iy++)
	{
	  for(k = 0; k < 1024; k++)
	    {
	      zv[k] = 0;
	      z[k] = 0;
	    }
	  for (i = ix*nx, min = 65535, max = 0; i < (ix+1)*nx; i++)
	    {
	      for (j = iy*nx;j < (iy+1)*nx; j++)
		{
		  ui = ps[(j<ony)?j:ony-1].ui[(i<onx)?i:onx-1];
		  min = (ui < min) ? ui : min; 
		  max = (ui > max) ? ui : max; 
		}
	    }
	  tmp = max - min;
	  fac = 1 + tmp/1024;
	  for (i = ix*nx; i < (ix+1)*nx; i++)
	    {
	      for (j = iy*nx;j < (iy+1)*nx; j++)
		{
		  ui = ps[(j<ony)?j:ony-1].ui[(i<onx)?i:onx-1];
		  ih = (ui - min)/fac;
		  z[ih]++;
		  zv[ih] += (float)ui;
		}
	    }
	  c = nx * nx;
	  c /= 2;
	  for(k = 0, l = 0; k < 1024 && l < c; l += z[k] , k++);
	  for (i = 0, tmpf = 0, tmpi = 0; i < k; i++)
	    {
	      tmpf += zv[i]; 
	      tmpi += z[i]; 
	    }
	  oit->im.pixel[iy].ui[ix] = (unsigned short int) (0.5+tmpf/tmpi);
	}
    }
  oid = create_one_image(onx, ony, IS_UINT_IMAGE);
  if (oid == NULL)	return NULL;	
  for(j = 0, pd = oid->im.pixel, ps = ois->im.pixel; j < ony; j++)
    {
      for(i = 0; i < onx; i++)
	{
	  tmpf = ps[j].ui[i] - interpolate_image_point(oit,((float)i)/nx -.5,((float)j)/nx -.5, NULL,NULL);
	  tmpf += 32768.5;
	  pd[j].ui[i] = (unsigned short int)tmpf;
	}
    }
  free_one_image(oit);
  if (ois->im.source != NULL) 	strcpy(c1, ois->im.source);
  else				c1[0] = 0;
  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  if (ois->title != NULL)	set_im_title(oid, "%s", ois->title);
  set_formated_string(&oid->im.treatement,"Image flaten by max histogram of %s",c1);
  return oid;
}


O_i	*convert_to_16bits(O_i *ois)
{
  register int i, j, k;
  O_i *oid;
  int onx, ony, data_type, nf;
  union pix *ps, *pd;
  
  if (ois->im.data_type == IS_COMPLEX_IMAGE 
      || ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE
      || ois->im.data_type == IS_RGB_PICTURE
      || ois->im.data_type == IS_RGBA_PICTURE)
    {
      win_printf("Cannot convert complex images");
      return NULL;
    }
  onx = ois->im.nx;	ony = ois->im.ny;	data_type = IS_UINT_IMAGE;
  nf = (ois->im.n_f > 0) ? ois->im.n_f : 1;
  if (nf == 1) oid =  create_one_image(onx, ony, data_type);
  else oid = create_one_movie( onx, ony, data_type, nf);
  
  
  if (oid == NULL)
    {
      win_printf("Can't create dest image");
      return NULL;
    }
  for (k = 0; k < nf; k++)
    {
      switch_frame(ois,k);
      switch_frame(oid,k);
      ps = ois->im.pixel; pd = oid->im.pixel;
      if (ois->im.data_type == IS_CHAR_IMAGE)
	{
	  for (i = 0; i < ony ; i++)
	    {
	      for (j=0; j< onx; j++)
		pd[i].ui[j] = (unsigned short int)ps[i].ch[j];
	    }
	}
      else if (ois->im.data_type == IS_INT_IMAGE)
	{
	  for (i = 0; i < ony ; i++)
	    {
	      for (j=0; j< onx; j++)
		pd[i].ui[j] = (unsigned short int)ps[i].in[j];
	    }
	}
      else if (ois->im.data_type == IS_UINT_IMAGE)
	{
	  for (i = 0; i < ony ; i++)
	    {
	      for (j=0; j< onx; j++)
		pd[i].ui[j] = (unsigned short int)ps[i].ui[j];
	    }
	}
      else if (ois->im.data_type == IS_LINT_IMAGE)
	{
	  for (i = 0; i < ony ; i++)
	    {
	      for (j=0; j< onx; j++)
		pd[i].ui[j] = (unsigned short int)ps[i].li[j];
	    }
	}
      else if (ois->im.data_type == IS_FLOAT_IMAGE)
	{
	  for (i = 0; i < ony ; i++)
	    {
	      for (j=0; j< onx; j++)
		pd[i].ui[j] = (unsigned short int)ps[i].fl[j];
	    }
	}
      else if (ois->im.data_type == IS_DOUBLE_IMAGE)
	{
	  for (i = 0; i < ony ; i++)
	    {
	      for (j=0; j< onx; j++)
		pd[i].ui[j] = (unsigned short int)ps[i].db[j];
	    }
	}
    }
  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	
    set_im_title(oid, "%s converted 16bits", ois->title);
  set_formated_string(&oid->im.treatement,"%s converted 16bits",
					     ois->filename);
  return oid;
}

int do_conv_float_usint(void)
{
  O_i *ois, *oid;
  imreg *imr;


  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  oid = convert_to_16bits(ois);
  if (oid == NULL)	return win_printf_OK("unable to create image!");
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  return (refresh_image(imr, imr->n_oi - 1));
}

int	find_max1(float *x, int nx, float *Max_pos, float *Max_val)
{
	register int i;
	int  nmax, ret = 0, delta;
	double a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0;

	for (i = 0, nmax = 0; i < nx; i++) nmax = (x[i] > x[nmax]) ? i : nmax;
	f_2 = x[(nx + nmax - 2) % nx];
	f_1 = x[(nx + nmax - 1) % nx];
	f0 = x[nmax];
	f1 = x[(nmax + 1) % nx];
	f2 = x[(nmax + 2) % nx];
	if (f_1 < f1)
	{
		a = -f_1/6 + f0/2 - f1/2 + f2/6;
		b = f_1/2 - f0 + f1/2;
		c = -f_1/3 - f0/2 + f1 - f2/6;
		d = f0;
		delta = 0;
	}	
	else
	{
		a = b = c = 0;	
		a = -f_2/6 + f_1/2 - f0/2 + f1/6;
		b = f_2/2 - f_1 + f0/2;
		c = -f_2/3 - f_1/2 + f0 - f1/6;
		d = f_1;
		delta = -1;
	}
	if (fabs(a) < 1e-8)
	{
		if (b != 0)	xm =  - c/(2*b) - (3 * a * c * c)/(4 * b * b * b);
		else
		{
			xm = 0;
			ret |= PB_WITH_EXTREMUM;
		}
	}
	else if ((b*b - 3*a*c) < 0)
	{
		ret |= PB_WITH_EXTREMUM;
		xm = 0;
	}
	else
		xm = (-b - sqrt(b*b - 3*a*c))/(3*a);
	/*
	for (p = -2; p < 1; p++)
	{
		if (6*a*p+b > 0) ret |= TOO_MUCH_NOISE;
	}	
	*/
	*Max_pos = (float)(xm + nmax + delta); 
	*Max_val = (a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d;
	return ret;
}


int tell_screen_labe_nb(void)
{
  int i, nl = 0;
  struct screen_label* s_l = NULL;
  O_i *ois;
  imreg *imr;


  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  win_printf("Nb of labels %d",ois->im.n_sl[ois->im.c_f]);
  
  nl = 0;
  for (i = 0;  i != CANCEL; )
    {
      i = win_scanf("label number %d",&nl);
      s_l = ois->im.s_l[ois->im.c_f][nl];
      win_printf("label %d\n xc %f yc %f\nval %g",nl,s_l->xla,s_l->yla,s_l->user_val);
    }
  return 0;  
}        

int delete_all_labels(void)
{
  O_i *ois;
  imreg *imr;


  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine deletes all labels\n"
			   "in the current movie.");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }

  remove_all_screen_label_from_movie(ois);

  ois->need_to_refresh = ALL_NEED_REFRESH;

  return 0;
}


int do_find_fluobead(void)
{
  register int i, j, ix, iy;
  O_i *ois;
  imreg *imr;
  //int  itx,imx;
  int onx, ony, cl, cl2a, cl2b, cw2, display = 1, q = 0;
  float x[256], y[256];
  float xf[256], yf[256];
  union pix *ps;
  static float thres = 9000;
  static int nb = 4;
  //float mx, tx, my, ty;
  float x_pos, x_val, y_pos, y_val;
  struct screen_label* s_l = NULL;  
	
  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  if (ois == NULL || ois->im.data_type != IS_UINT_IMAGE)	
    {
      win_printf("images must be unsigned short int");
      return D_O_K;
    }
  onx = ois->im.nx;	ony = ois->im.ny;
  i = win_scanf("Size of bead image %6d\n"
		"threshold %6f",&nb,&thres);
  if (i == CANCEL)   return 0;

  ps = ois->im.pixel;
  
  cl = 4 * nb;
  cw2 = (nb-1)/2;
  cl2a = (3*nb)/2;
  cl2b = (5*nb)/2;
  for (i = 0; i < ony - cl ; i++)
    {
      for (j=0; j< onx - cl; j++)
	{ 
	  // we erase array
	  for (ix = 0; ix < cl; ix++) x[ix] = y[ix] = 0;
	  // we grab x profile
	  for (iy = (3*nb)/2; iy < (5*nb)/2; iy++)
	    {
	      for (ix = 0; ix < cl; ix++)
		  x[ix] += (float)ps[i+iy].ui[j+ix] - 32768;
	    }
	  // we grab y profile
	  for (ix = cl2a; ix < cl2b; ix++)
	    {
	      for (iy = 0; iy < cl; iy++)
		  y[iy] += (float)ps[i+iy].ui[j+ix] - 32768;
	    }
	  for (ix = 0; ix < cl; ix++) 
	  {
	    for (iy = -cw2; iy <= cw2; iy++)
	      {
		xf[ix] += x[(cl+ix+iy)%cl];  
		yf[ix] += y[(cl+ix+iy)%cl];  
	      }
	    xf[ix] /= (1+2*cw2)*nb;
	    yf[ix] /= (1+2*cw2)*nb;
	  }
	  find_max1(xf, cl, &x_pos, &x_val);  
   	  find_max1(yf, cl, &y_pos, &y_val); 
	  /* 
	  for (ix = 0, mx = tx = my = ty = 0, imx = itx = 0; ix < 4*nb; ix++)
	    {
	      if (ix < (5*nb)/2 && ix >= (3*nb)/2)
		{
		  tx += x[ix];
		  ty += y[ix];
		  itx++;
		} 
	      else
		{
		  mx += x[ix];
		  my += y[ix];
		  imx++;
		} 
	    }
	  mx = (imx) ? mx/imx : mx;
	  my = (imx) ? my/imx : my;
	  tx = (itx) ? tx/itx : tx;
	  ty = (itx) ? ty/itx : ty;*/
	  if (display && x_val > thres && y_val > thres)
	    {
	      q = win_printf("i %d j %d x val %f y val %f pos x %f y %f",i,j,x_val,y_val,x_pos,y_pos);
	      if (q == CANCEL) display = 0;
	    }
	  
      	  if ((x_val > thres) && (y_val > thres) && (fabs(x_pos - 2*nb) <= 1) && (fabs(y_pos - 2*nb) <= 1))
	    {
	      s_l = add_screen_label(ois, ((float)j+x_pos), ((float)i+y_pos), 0, NULL, draw_square_label);
	      s_l->user_val = (x_val + y_val)/2;

              //push_image_label(ois, ((float)j+x_pos), ((float)i+y_pos), "\\center{\\oc}", USR_COORD); 
	    }
              
	}
      draw_im_screen_label(imr, ois, ois->im.c_f);
    }

push_image_label(ois, 100, 100, "\\center{\\oc}", USR_COORD);
/*
    for (i = 0; i < ony - 5*nb ; i++)
     {
      for (j=0; j< onx - 5*nb; j++)
	 {
	  tx = 0; mx =0;
       
	  for (iy = 2*nb; iy < 3*nb; iy++)
	    {
	      for (ix = 1; ix < 4*nb; ix++)
		  tx += ps[i+iy].ui[j+ix];
	    }
      for (ix = 2*nb; ix < 3*nb; ix++)
	    {
	      for (iy = 1; iy < 4*nb; iy++)
		  tx += ps[i+iy].ui[j+ix];
	    }    
	  tx /=6*nb*nb;
	  
      for (iy = nb; iy < 4*nb; iy++)
	    {
	      for (ix = 0; ix < nb; ix++)
		  mx += ps[i+iy].ui[j+ix];
	    }
      for (ix = nb; ix < 4*nb; ix++)
	    {
	      for (iy = 0; iy < nb; iy++)
		  mx += ps[i+iy].ui[j+ix];
	    }
      for (iy = nb; iy < 4*nb; iy++)
	    {
	      for (ix = 4*nb; ix < 5*nb; ix++)
		  mx += ps[i+iy].ui[j+ix];
	    }
      for (ix = nb; ix < 4*nb; ix++)
	    {
	      for (iy = 4*nb; iy < 5*nb; iy++)
		  mx += ps[i+iy].ui[j+ix];
	    } 
      mx /=12*nb*nb;
	    
      if ((tx - mx) > thres )
	    push_image_label(ois, (float)(j+nb*5/2), (float)(i+nb*5/2), "\\oc", USR_COORD);
     }
     }
*/
  return (refresh_image(imr, imr->n_oi - 1));
}

int do_histo_flaten_image_16bits(void)
{
	static int nx = 16;
	register int i;
	O_i *ois, *oid;
	imreg *imr;
	
	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
	  return win_printf_OK("This routine flatens an image using its \n"
			       "local histogram");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
	i = win_scanf("size to perform histogramm :%d",&nx);
	if (i == CANCEL)	return OFF;
	oid = max_histo_tile_16bits(ois, nx);
	if (oid == NULL)	return win_printf("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));		
}


int find_ecoli_in_cur_frame(O_i *ois, int thres, int display)
{
  int np = 0;
  union pix *ps;

  int i, j, x[3], y[3], itmp;
  int onx, ony, q = 0; 
  int i00, i10, i20, i01, i11, i21, i02, i12, i22;
  float x_pos, x_val, y_pos, y_val, fthres;

  struct screen_label* s_l = NULL;  

  if (ois == NULL || ois->im.data_type != IS_CHAR_IMAGE)	
    {
      win_printf("images must be unsigned char");
      return D_O_K;
    }

  onx = ois->im.nx;	ony = ois->im.ny;
  fthres = thres;
  for (i = 1, ps = ois->im.pixel; i < ony - 1 ; i++)
	{
	  for (j=1; j< onx - 1; j++)
	    {
	      itmp = 0;
	      i00 = ps[i-1].ch[j-1];
	      itmp += (i00) < thres ? 1 : 0;
	      i10 = ps[i-1].ch[j];
	      itmp += (i10) < thres ? 1 : 0;
	      i20 = ps[i-1].ch[j+1];
	      itmp += (i20) < thres ? 1 : 0;
	      i01 = ps[i].ch[j-1];
	      itmp += (i01) < thres ? 1 : 0;
	      i11 = ps[i].ch[j];
	      itmp += (i11) < thres ? 1 : 0;
	      i21 = ps[i].ch[j+1];
	      itmp += (i21) < thres ? 1 : 0;
	      i02 = ps[i+1].ch[j-1];
	      itmp += (i02) < thres ? 1 : 0;
	      i12 = ps[i+1].ch[j];
	      itmp += (i12) < thres ? 1 : 0;
	      i22 = ps[i+1].ch[j+1];
	      itmp += (i22) < thres ? 1 : 0;
	      if (itmp >= 9) continue;
	      x[0] = i00 + i01 + i02;
	      x[1] = i10 + i11 + i12;
	      x[2] = i20 + i21 + i22;
	      if (x[1] < x[0] || x[1] < x[2]) continue;
	      x_pos = x[1] - x[0];
	      itmp = 2*x[1] - x[0] - x[2];
	      if (itmp <= 0) continue;
	      x_pos /= itmp;
	      x_pos += 0.5;
	      x_val = (x_pos - 1) * (x_pos - 2) * x[0]/2;
	      x_val -= x_pos * (x_pos - 2) * x[1];
	      x_val += x_pos * (x_pos - 1) * x[2]/2;

	      x_pos += j - 1;
	      y[0] = i00 + i10 + i20;
	      y[1] = i01 + i11 + i21;
	      y[2] = i02 + i12 + i22;
	      if (y[1] < y[0] || y[1] < y[2]) continue;
	      y_pos = y[2] - y[1];
	      itmp = 2*y[1] - y[0] - y[2];
	      if (itmp <= 0) continue;
	      y_pos /= itmp;
	      y_pos += 0.5;
	      y_val = (y_pos - 1) * (y_pos - 2) * y[0]/2;
	      y_val -= y_pos * (y_pos - 2) * y[1];
	      y_val += y_pos * (y_pos - 1) * y[2]/2;
	      y_pos += i - 1;
	      if (display && x_val > fthres && y_val > fthres)
		{
		  q = win_printf("i %d j %d x val %f y val %f pos x %f y %f",i,j,x_val,y_val,x_pos,y_pos);
		  if (q == CANCEL) display = 0;
		}
	  
	      if ((x_val > fthres) && (y_val > fthres))
		{
		  s_l = add_screen_label(ois, x_pos, y_pos, 0, NULL, draw_ecoli_label);
		  s_l->user_val = (x_val + y_val)/2;
		  np++;
		  //push_image_label(ois, ((float)j+x_pos), ((float)i+y_pos), "\\center{\\oc}", USR_COORD); 
		}
	    }
	  //draw_im_screen_label(imr, ois, ois->im.c_f);
	}

  return np;
}

int do_find_ecoli(void)
{
  register int i, im;
  O_i *ois;
  imreg *imr;
  //int  itx,imx;
  int nf, np = 0;
  static int thres = 15, display = 0;
	
  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine finds light intensity peaks");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  if (ois == NULL || ois->im.data_type != IS_CHAR_IMAGE)	
    {
      win_printf("images must be unsigned char");
      return D_O_K;
    }
  i = win_scanf("Light level threshold %6d\ndebug %b",&thres,&display);
  if (i == CANCEL)   return 0;

  nf = abs(ois->im.n_f);		
  if (nf <= 0) nf = 1;	

  for (im = 0, np = 0; im < nf; im++)
    {
      switch_frame(ois,im);
      np += find_ecoli_in_cur_frame(ois, thres, display);

    }
  win_printf("%d peak found in %d images with threshold = %d",np,nf,thres);
  return 0;
  //push_image_label(ois, 100, 100, "\\center{\\oc}", USR_COORD);
  //return (refresh_image(imr, imr->n_oi - 1));
}

int copy_one_image_with_labels_only(O_i *ois, O_i *oid, int bg, int lw, int keep_intensity, int verbose)
{
  int i, j, l;
  int xmin, ymin;

  // check that source and dest img have same size
  if ((ois->im.nx != oid->im.nx) || (ois->im.nx != oid->im.nx))
    {
      if (verbose) win_printf_OK("Error: images with different size");
      return 1;
    }
  if ((ois->im.data_type != IS_CHAR_IMAGE) || (ois->im.data_type != IS_CHAR_IMAGE))
    {
      if (verbose) win_printf_OK("Error: images with different type");
      return 1;
    }

  for (j = 0; j < oid->im.ny; j++)
    for (i = 0; i < oid->im.nx; i++)
      {
	oid->im.pixel[j].ch[i] = (unsigned char) bg;
      }

  for (l = 0 ; l < ois->im.n_sl[ois->im.c_f] ; l++)
    {
      xmin = (int)ois->im.s_l[ois->im.c_f][l]->xla - lw/2;
      ymin = (int)ois->im.s_l[ois->im.c_f][l]->yla - lw/2;
      for (j = ymin; j < ymin + lw; j++)
	for (i = xmin; i < xmin + lw; i++)
	  {
	    if (keep_intensity)
	      oid->im.pixel[j].ch[i] = ois->im.pixel[j].ch[i];
	    else
	      oid->im.pixel[j].ch[i] = 255;
	  }
    }

  return 0;
}

int do_copy_one_image_with_labels_only(void)
{
  imreg *imr;
  O_i *oi, *oid;
  int i, w = 3;
  int bg = 2;

  if(updating_menu_state != 0)	return D_O_K;

/*   if (key[KEY_LSHIFT]) */
/*     { */
/*       return win_printf_OK("This routine finds light intensity peaks"); */
/*     } */
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
      win_printf_OK("Cannot find image");

  i = win_scanf("Bg level  %5d\nLabel width  %5d", &bg, &w);
  if (i == CANCEL)   return OFF;

  oid = create_and_attach_oi_to_imr(imr, oi->im.nx, oi->im.ny, IS_CHAR_IMAGE);
  copy_one_image_with_labels_only(oi, oid, bg, w, 1, 1);

  refresh_image(imr, UNCHANGED);
  return 0;
}

int do_movie_average_labels_only_along_x(void)
{
	register int i, j, k;
	static int l_s = -1, l_e = -1;
	static int do_line_avg = 1;
	int bg = 2, lw = 3, keep_intensity = 0;
	O_i *ois = NULL, *oid = NULL, *oitmp = NULL;
	float *tmp;
	int nf;
	union pix *pd;
	imreg *imr = NULL;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;	
	}		
	if(updating_menu_state != 0)	
	{
		if (ois->im.n_f == 1 || ois->im.n_f == 0)	
			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
		return D_O_K;	
	}	
	nf = abs(ois->im.n_f);		
	if (nf <= 0)	
	{
		win_printf("This is not a movie");
		return D_O_K;
	}
	i = ois->im.c_f;

	if (l_s == -1) l_s = 0; 
	if (l_e == -1) l_e = ois->im.nx;
	i = win_scanf("Average Between row %5d and %5d\n"
		      "Bg level %5d   Label width %5d\n\n"
		      "%b do line averaging (img to plot)\n"
		      "%b keep peaks intensity in filtered image",
		      &l_s, &l_e, &bg, &lw, &do_line_avg, &keep_intensity);
	if (i == CANCEL)	return D_O_K;

	oid = create_and_attach_oi_to_imr(imr, ois->im.ny ,nf, IS_FLOAT_IMAGE);	
	if (oid == NULL) return (win_printf_OK("cant create image!"));
	oitmp = create_one_image(ois->im.nx ,ois->im.ny, IS_CHAR_IMAGE);
	//oitmp = create_and_attach_oi_to_imr(imr, ois->im.nx ,ois->im.ny, IS_CHAR_IMAGE);
	if (oitmp == NULL) return (win_printf_OK("cant create tmp image!"));

	tmp = (float*)calloc(ois->im.ny,sizeof(float));
	if (tmp == NULL) return (win_printf_OK("cant create tmp array!"));
	pd  = oid->im.pixel;		

	set_im_title(oid,"Average row profile. (%d, %d)", l_s,l_e);
	set_formated_string(&oid->im.treatement,"Average row profile. (%d, %d)", l_s,l_e);

	inherit_from_im_to_im(oid,ois);
	// set_oi_history(oid, "Y-profile from movie with labels");

	uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid, IS_X_UNIT_SET);
	uns_oi_2_oi_by_type(ois, IS_T_UNIT_SET, oid, IS_Y_UNIT_SET);
	for (k = 0; k < nf; k++)
	  {
	    switch_frame(ois,k);
	    copy_one_image_with_labels_only(ois, oitmp, bg, lw, keep_intensity, 0);

	    display_title_message("im %d",k);
	    for (i=0 ; i< oitmp->im.ny ; i++)	tmp[i] = 0;
	    for (i = (l_s>= 0) ? l_s : 0; i< l_e && i < oitmp->im.nx; i++)
	      {
		extract_raw_row(oitmp, i, tmp);
		for (j=0 ; j< oitmp->im.ny ; j++)	pd[k].fl[j] += tmp[j];	
	      }
	    for (i=0, j = l_e - l_s ; i < oitmp->im.ny && j !=0 ; i++)
	      {
		pd[k].fl[i] /= j;		
	      }
	  }
	free(tmp);
	free_one_image(oitmp);
	find_zmin_zmax(oid);
	oid->need_to_refresh = ALL_NEED_REFRESH;
	set_oi_horizontal_extend(oid, 1);
	set_oi_vertical_extend(oid, 1);
	select_image_of_imreg(imr, imr->n_oi-1);
	broadcast_dialog_message(MSG_DRAW,0);	
	
	if (do_line_avg)
	  do_average_along_y();

	return D_REDRAWME;		
}

int grab_ecoli_peak(void)
{
  register int i, j;
  O_i *ois;
  O_p *op;
  d_s *dsy, *dsi, *dsm;
  imreg *imr;
  int xm, ym, max=0, sum=0;

	
  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine finds light intensity peaks");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      win_printf_OK("Cannot find image");


  if (ois->im.s_l == NULL || ois->im.m_sl == NULL || ois->im.n_sl == NULL)
      win_printf_OK("No screen label");

  op = create_and_attach_op_to_oi(ois, 16, 16,0, 0);  
  if (op == NULL)	return win_printf_OK("cannot create plot !");
  dsy = op->dat[0];
  set_ds_source(dsy, "Intensity of ecoli peaks against y coordinates");
  dsy->nx = dsy->ny = 0;
  set_ds_point_symbol(dsy, "\\pt4\\gamma");
  set_ds_dot_line(dsy);

  op = create_and_attach_op_to_oi(ois, 16, 16,0, 0);  
  if (op == NULL)	return win_printf_OK("Cannot create plot !");
  dsi = op->dat[0];
  set_ds_source(dsi, "Max pixel intensity against interpolated intensity");
  dsi->nx = dsi->ny = 0;
  if ((dsm = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  set_ds_source(dsm, "Integrated intensity against interpolated value");
  dsm->nx = dsm->ny = 0;

  for (i=0 ; i< ois->im.n_f; i++)
    {
      for (j=0 ; j < ois->im.n_sl[i] ; j++)
	{
	  //tmp = i + (float)j/ois->im.n_sl[i];
	  add_new_point_to_ds(dsy, ois->im.s_l[i][j]->yla, ois->im.s_l[i][j]->user_val);

	  xm = (int)round(ois->im.s_l[i][j]->xla);
	  ym = (int)round(ois->im.s_l[i][j]->yla);
	  max = ois->im.pxl[i][ym].ch[xm];

	  sum = ois->im.pxl[i][ym-1].ch[xm-1] + ois->im.pxl[i][ym-1].ch[xm] + ois->im.pxl[i][ym-1].ch[xm+1];
	  sum += ois->im.pxl[i][ym].ch[xm-1] + ois->im.pxl[i][ym].ch[xm] + ois->im.pxl[i][ym].ch[xm+1];
	  sum = ois->im.pxl[i][ym+1].ch[xm-1] + ois->im.pxl[i][ym+1].ch[xm] + ois->im.pxl[i][ym+1].ch[xm+1];

	  add_new_point_to_ds(dsm, ois->im.s_l[i][j]->user_val, sum);  
	  add_new_point_to_ds(dsi, ois->im.s_l[i][j]->user_val, max);
	}

    }
  return refresh_im_plot(imr, UNCHANGED);  
}

int do_fir_constant_width(void)
{
  pltreg *pr;
  O_p *op;
  d_s *ds, *dsf;
  int i = 0;
  static float width = 8, dx = 1;
  int j, itmp;
  float min, max;
  float i_f, tmp;

  if(updating_menu_state != 0)
    {
      //add_keyboard_short_cut(0, KEY_H, 0, do_fir_constant_width);
      return D_O_K;
    }

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("cannot find plot");
  if (ds == NULL)
    return win_printf_OK("cannot find dataset");

  for (j = 0, min = max = ds->xd[0]; j < ds->nx; j++)
    {
      min = (ds->xd[j] < min) ?  ds->xd[j] : min;
      max = (ds->xd[j] > max) ?  ds->xd[j] : max;
    }

  i = win_scanf("Fir window width %8f and shift dx %8f\n", &width, &dx);
  if (i == CANCEL) return OFF;

  if ((dsf = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  dsf->nx = 0;
  set_ds_source(dsf, "FIR with constant width window %g shift %g",width,dx);



  for (i_f = min; i_f < max-width; i_f += dx)
    {
      for (j = 0, tmp = 0, itmp = 0; j < ds->nx; j++)
	{
	  if (ds->xd[j] > i_f &&  ds->xd[j] < (i_f+width))
	    {
	      tmp += ds->yd[j];
	      itmp++;
	    }
  	}
      if (itmp > 0)  add_new_point_to_ds(dsf, i_f + width/2,tmp/itmp);
    }
  
  op->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);
  return 0;
}

int do_x_histo_constant_width(void)
{
  pltreg *pr;
  O_p *op;
  d_s *ds, *dsf;
  int i = 0;
  static float width = 8, dx = 1;
  int j, itmp;
  float min, max;
  float i_f, tmp;

  if(updating_menu_state != 0)
    {
      //add_keyboard_short_cut(0, KEY_H, 0, do_fir_constant_width);
      return D_O_K;
    }

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("cannot find plot");
  if (ds == NULL)
    return win_printf_OK("cannot find dataset");

  for (j = 0, min = max = ds->xd[0]; j < ds->nx; j++)
    {
      min = (ds->xd[j] < min) ?  ds->xd[j] : min;
      max = (ds->xd[j] > max) ?  ds->xd[j] : max;
    }

  i = win_scanf("Fir window width %8f and shift dx %8f\n", &width, &dx);
  if (i == CANCEL) return OFF;

  //win_printf_OK("min %d max %d\nw %d ni %d", min, max, width, ni);
  if ((dsf = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
    return win_printf_OK("I can't create plot !");
  dsf->nx = 0;
  set_ds_source(dsf, "X histo with constant width window %g shift %g",width,dx);

  for (i_f = min; i_f < max-width; i_f += dx)
    {
      for (j = 0, tmp = 0, itmp = 0; j < ds->nx; j++)
	{
	  if (ds->xd[j] > i_f &&  ds->xd[j] < (i_f+width))
	    {
	      //tmp += ds->yd[j];
	      itmp++;
	    }
  	}
      if (itmp > 0)  add_new_point_to_ds(dsf, i_f + width/2, (float) itmp);
    }
  
  op->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);
  return 0;
}

int get_number_screen_labels(O_i *oi)
{
  int f, nlab;

  nlab = 0;
  for (f = 0; f < oi->im.n_f; f++)
    {
      switch_frame(oi, f);
      nlab += oi->im.n_sl[oi->im.c_f];
    }
  //win_printf_OK("# labels  %d", nlab);

  return nlab;
}

int scatter_plot_bp_peaks(O_i *oi, d_s *dssc, d_s *dssci, int rot, int debug)
// add in dssc (resp dssci) the (resp interpolated) value 
// of the peak after bp vs before bp
{
  struct screen_label *s_ld;
  int i, f, l, dsc = 0, wsc = 0, nlab;
  int xm, ym, min, max;
  float tmp, x[3], y[3], x_pos = 0, y_pos = 0, x_val, y_val;

  if ((nlab=get_number_screen_labels(oi)) <= 0)
    return win_printf_OK("No label to plot");

  if ((dssc->mx < nlab) || (dssc->mx < nlab))
    return win_printf_OK("Error: more labels in the image\n"
			 "than points allocated in the datasets");
  
  // Reset ds
  for (i = 0; i < dssc->mx; i++)
      dssc->xd[i] = dssc->yd[i] = 0;
  dssc->nx = 0;
  for (i = 0; i < dssci->mx; i++)
      dssci->xd[i] = dssci->yd[i] = 0;
  dssci->nx = 0;

  wsc = (rot == 0) ? oi->im.ny : oi->im.nx;
  win_scanf("Distance to center %5d\nWidth %5d\n"
	    "%b debug", &dsc, &wsc, &debug);
  min = (rot == 0) ? oi->im.ny/2 + dsc - wsc/2 : oi->im.nx/2 + dsc - wsc/2;
  max = (rot == 0) ? oi->im.ny/2 + dsc + wsc/2 : oi->im.nx/2 + dsc + wsc/2;
  nlab = 0;

  //  win_printf_OK("min %d  max %d", min, max);

  for (f = 0; f < oi->im.n_f; f++)
    {
      switch_frame(oi, f);
      for (l = 0; l < oi->im.n_sl[oi->im.c_f]; l++)
	{
	  s_ld = oi->im.s_l[oi->im.c_f][l];
	  xm = (int)round(s_ld->xla);
	  ym = (int)round(s_ld->yla);
	  //win_printf_OK("xm %d  ym %d", xm, ym);


	  // check if in roi
	  if ((rot == 0) && ((ym < min) || (ym > max)))
	    continue;
	  if ((rot == 1) && ((xm < min) || (xm > max)))
	    continue;

	  // check if not too close to the borders
	  if ((xm <= 0) || (xm >= oi->im.nx-1) || (ym <= 0) || (ym >= oi->im.ny-1))
	    {
	      tmp = -10;
	      win_printf_OK("xm %d ym %d\nxla %3f yla %3f", xm, ym, s_ld->xla, s_ld->yla);
	    }
	  else
	    {
	      //interpolate z value over 3 points in dest image
	      x[0] = oi->im.pixel[ym-1].fl[xm-1] 
		+ oi->im.pixel[ym].fl[xm-1] 
		+ oi->im.pixel[ym+1].fl[xm-1];
	      x[1] = oi->im.pixel[ym-1].fl[xm] 
		+ oi->im.pixel[ym].fl[xm] 
		+ oi->im.pixel[ym+1].fl[xm];
	      x[2] = oi->im.pixel[ym-1].fl[xm+1] 
		+ oi->im.pixel[ym].fl[xm+1] 
		+ oi->im.pixel[ym+1].fl[xm+1];
	      x_pos = x[1] - x[0];
	      tmp = 2*x[1] - x[0] - x[2];
	      x_pos /= tmp;
	      x_pos += 0.5;
	      x_val = (x_pos - 1) * (x_pos - 2) * x[0]/2;
	      x_val -= x_pos * (x_pos - 2) * x[1];
	      x_val += x_pos * (x_pos - 1) * x[2]/2;
	      x_pos += xm - 1;

	      y[0] = oi->im.pixel[ym-1].fl[xm-1] 
		+ oi->im.pixel[ym-1].fl[xm] 
		+ oi->im.pixel[ym-1].fl[xm+1];
	      y[1] = oi->im.pixel[ym].fl[xm-1] 
		+ oi->im.pixel[ym].fl[xm] 
		+ oi->im.pixel[ym].fl[xm+1];
	      y[2] = oi->im.pixel[ym+1].fl[xm-1] 
		+ oi->im.pixel[ym+1].fl[xm] 
		+ oi->im.pixel[ym+1].fl[xm+1];
	      y_pos = y[2] - y[1];
	      tmp = 2*y[1] - y[0] - y[2];
	      y_pos /= tmp;
	      y_pos += 0.5;
	      y_val = (y_pos - 1) * (y_pos - 2) * y[0]/2;
	      y_val -= y_pos * (y_pos - 2) * y[1];
	      y_val += y_pos * (y_pos - 1) * y[2]/2;
	      y_pos += ym - 1;

	      tmp = (x_val + y_val)/2;
	    }

	  dssc->xd[dssc->nx] = s_ld->user_val;
	  dssci->xd[dssci->nx] = s_ld->user_val;
	  dssc->yd[dssc->nx] = oi->im.pixel[ym].fl[xm];
	  dssci->yd[dssci->nx] = tmp;
	  dssc->nx++;
	  dssci->nx++;
	  nlab ++;

	  if (debug)
	    win_printf_OK("xla %4f  yla %4f\n"
			  "xpos %4f  ypos %4f\n\n"
			  "init val %5f\nuninterpol val %5f\ninterpol val %5f",
			  s_ld->xla, s_ld->yla, x_pos, 
			  y_pos, s_ld->user_val, oi->im.pixel[ym].fl[xm], tmp);
	}
    }
  win_printf_OK("# labels processed  %d", nlab);

  return 0;
}

int do_plot_scatter_plot_bp_peaks(void)
{
  imreg *imr;
  O_i *oi;
  d_s *ds, *dsi;
  int i = -1;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine plots the value before and\n"
			   "after bp filtering in the image plot");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    {
      win_printf("Cannot find image");
      return D_O_K;
    }
  if (oi == NULL || oi->im.data_type != IS_FLOAT_IMAGE)	
    {
      win_printf("images must be float");
      return D_O_K;
    }

  i = find_op_nb_of_source_specific_ds_in_oi(oi, "Scatter plot of intensity before and after bp filtering");
  if (i < 0)
    return win_printf_OK("Dataset does not have the right source");
  ds = oi->o_p[i]->dat[0];
  dsi = oi->o_p[i]->dat[1];

  scatter_plot_bp_peaks(oi, ds, dsi, 0, 0); // menu for horiz beam
  oi->o_p[1]->need_to_refresh = 1;
  oi->need_to_refresh = ALL_NEED_REFRESH;

  return 0;
}


MENU *fluobead_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Float -> 16bits", do_conv_float_usint,NULL,0,NULL);
	add_item_to_menu(mn,"Flaten image", do_histo_flaten_image_16bits,NULL,0,NULL);
	add_item_to_menu(mn,"\0",NULL,NULL,0,NULL);

	add_item_to_menu(mn,"Find bead", do_find_fluobead,NULL,0,NULL);
	add_item_to_menu(mn,"Find ecoli", do_find_ecoli,NULL,0,NULL);
	add_item_to_menu(mn,"Grab ecoli", grab_ecoli_peak,NULL,0,NULL);
	add_item_to_menu(mn,"New image with labelled area only", do_copy_one_image_with_labels_only,NULL,0,NULL);
	add_item_to_menu(mn,"Scatter plot of labels (horiz beam)", do_plot_scatter_plot_bp_peaks,NULL,0,NULL);
	add_item_to_menu(mn,"Movie to image (Y profile) with labelled area only", do_movie_average_labels_only_along_x,NULL,0,NULL);
	add_item_to_menu(mn,"\0",NULL,NULL,0,NULL);

	add_item_to_menu(mn,"Find labels", tell_screen_labe_nb,NULL,0,NULL);
	add_item_to_menu(mn,"Delete all labels", delete_all_labels,NULL,0,NULL);
	return mn;
}

MENU *fluobead_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"x histogram (constant width)", do_x_histo_constant_width,NULL,0,NULL);
	add_item_to_menu(mn,"FIR filter (constant width)", do_fir_constant_width,NULL,0,NULL);
	return mn;
}


int	fluobead_main(int argc, char **argv)
{
	add_image_treat_menu_item ( "fluobead", NULL, fluobead_image_menu(), 0, NULL);
	add_plot_treat_menu_item ( "fluobead",  NULL, fluobead_plot_menu(), 0, NULL);
	return D_O_K;
}

int	fluobead_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "fluobead", NULL, NULL);
	remove_item_to_menu(plot_treat_menu,  "fluobead", NULL, NULL);
	return D_O_K;
}
#endif

