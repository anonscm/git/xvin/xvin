
#include <windows.h>
#include <conio.h>
#include <stdio.h>
#include "Pi_gcs2_dll.h"
//#include "NIDAQmx.h"
#include "math.h"


int Controller_ID;
char szAxes[129];
double Piezzo_position;
int iError;
char szErrorMessage[1025];
BOOL bTest = FALSE;
float actual_temperature=0;
int error=0;
char	errBuff[2048]={'\0'};
float   data_out[1] = {1.0};
#define BUFFER_SIZE 30000
#define PIFOC_RANGE 2500/*4000*/

LRESULT CALLBACK WndProc     (HWND, UINT, WPARAM, LPARAM) ;
INT_PTR CALLBACK ColorScrDlg (HWND, UINT, WPARAM, LPARAM) ;
HWND hDlgModeless ;
#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else
void Cleanup(void);


float		data_in[BUFFER_SIZE];
int		totalRead=0;

int Init_PI(void)
{
  
  //char szDescription[1025];
  BOOL bReturn_SVO;

  Controller_ID = PI_InterfaceSetupDlg ("");
  bTest = PI_IsConnected(Controller_ID);
  if (bTest == FALSE) MessageBox (NULL, TEXT ("Connected"), "Not Connected", MB_ICONERROR) ;

  /////////////////////////////////////////
  // Get the name of the connected axis. //
  /////////////////////////////////////////

  if (!PI_qSAI(Controller_ID, szAxes, 16))
    {
      iError = PI_GetError(Controller_ID);
      PI_TranslateError(iError, szErrorMessage, 1024);
      /* printf("SAI?: ERROR %d: %s\n", iError, szErrorMessage); */
      /* MessageBox (NULL, TEXT ("SAI est NULL!"), */
      /* 		   "SAI", MB_ICONERROR) ; */
      PI_CloseConnection(Controller_ID);
      return 0;
    }


 /*  bTest = PI_qPOS(Controller_ID, szAxes, &dPosition); */
 /*  if (bTest == FALSE) MessageBox (NULL, TEXT("Merde"), "Merde", MB_ICONERROR) ; */
 /*  StringCbPrintf(szErrorMessage,1024,TEXT("Position: %f "), (float)dPosition); */
 /*  MessageBox (NULL, szErrorMessage, "Position", MB_ICONERROR) ; */

 /* bTest = PI_qTMX(Controller_ID, szAxes, &dMaxpos); */
 /*  if (bTest == FALSE) MessageBox (NULL, TEXT("Merde"), "Merde", MB_ICONERROR) ; */
 /*  StringCbPrintf(szErrorMessage,1024,TEXT("Max Position: %f "), (float)dMaxpos); */
 /*  MessageBox (NULL, szErrorMessage, "Maximum Position", MB_ICONERROR) ; */

 /*   bTest = PI_qSVO(Controller_ID, szAxes, &bReturn_SVO); */
 /*  if (bTest == FALSE) MessageBox (NULL, TEXT("Merde"), "Merde", MB_ICONERROR) ; */
 /*  if (bReturn_SVO == TRUE)  MessageBox (NULL, TEXT("TRUE"), "TRUE", MB_ICONERROR) ; */
 /*  else  MessageBox (NULL, TEXT("FALSE"), "FALSE", MB_ICONERROR); */

 
  bReturn_SVO = TRUE;
  bTest = PI_SVO(Controller_ID, szAxes, &bReturn_SVO);
  if (bTest == FALSE) MessageBox (NULL, TEXT("Merde"), "Merde", MB_ICONERROR) ;
 
  bTest = PI_qSVO(Controller_ID, szAxes, &bReturn_SVO);
  if (bTest == FALSE) MessageBox (NULL, TEXT("Merde"), "Merde", MB_ICONERROR) ;
  /* if (bReturn_SVO == TRUE)  MessageBox (NULL, TEXT("TRUE"), "TRUE", MB_ICONERROR) ; */
  /* else  MessageBox (NULL, TEXT("FALSE"), "FALSE", MB_ICONERROR); */
  
	/* bTest = PI_MOV(Controller_ID, szAxes, &dMaxpos); */
	/* if (bTest == FALSE) */
	/*   { */
	/*     iError = PI_GetError(Controller_ID); */
	/*     PI_TranslateError(iError, szErrorMessage, 1024); */
	/*     printf("MOV: ERROR %d: %s\n", iError, szErrorMessage); */
	/*     MessageBox (NULL, TEXT ("MOV error!"), */
	/* 	   szErrorMessage, MB_ICONERROR) ; */
	/*     PI_CloseConnection(Controller_ID); */
	/*     return (INT_PTR) 0; */
	/*   } */
	     

	/* bTest = PI_qPOS(Controller_ID, szAxes, &dPosition); */
	/* if (bTest == FALSE) MessageBox (NULL, TEXT("Merde"), "Merde", MB_ICONERROR) ; */
	/* StringCbPrintf(szErrorMessage,1024,TEXT("Position: %f "), (float)dPosition); */
	/* MessageBox (NULL, szErrorMessage, "Position", MB_ICONERROR) ;  */
  
  return 1;
}





