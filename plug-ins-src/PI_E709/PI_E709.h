#ifndef _PI_E709_H_
#define _PI_E709_H_

PXV_FUNC(int, do_PI_E709_rescale_plot, (void));
PXV_FUNC(MENU*, PI_E709_plot_menu, (void));
PXV_FUNC(int, do_PI_E709_rescale_data_set, (void));
PXV_FUNC(int, PI_E709_main, (int argc, char **argv));
#endif
