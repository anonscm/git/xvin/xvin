/*(**
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _PI_E709_C_
#define _PI_E709_C_

# include "util.h"
# include "stdio.h"
#include <stdio.h>
# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"

/* If you include other regular header do it here*/
#include "PI_GCS2_DLL.h"
#include "pthread.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin

PXV_FUNC(float, (*grab_PI_z_value),(void));

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "PI_E709.h"
#include "../xvuEye/xvuEye.h"

#ifdef _WIN32
    #include <windows.h>

    void psleep(unsigned long int milliseconds)
    {
        Sleep(milliseconds);
    }
#else
    #include <unistd.h>

    void psleep(unsigned long int milliseconds)
    {
        usleep(milliseconds * 1000); // takes microseconds
    }
#endif



int Controller_ID = -1;
char szAxes[129];
double Piezzo_position = 0;
int iError;
char szErrorMessage[1025];
BOOL bTest = FALSE;
float actual_temperature=0;
int error=0;
char	errBuff[2048]={'\0'};
float   data_out[1] = {1.0};
#define BUFFER_SIZE 30000
#define PIFOC_RANGE 2500/*4000*/

struct Rampe{
  double start;
  double end;
  double step;
  double x;
  int nb_steps;
  double time_step;
  pthread_t* thread;
} Rampe;


float PI_read_aut(void)
{
  static double pos;
  PI_qPOS(Controller_ID, szAxes, &pos);
  return (float)pos;
 }

int Init_PI(void)
{
  //char szDescription[1025];
  BOOL bReturn_SVO;

  char *szBuffer = NULL;

  szBuffer = malloc(100);
  int nb_controllers = 0;

  nb_controllers = PI_EnumerateUSB(szBuffer, 100,"E-709");

  if (nb_controllers < 0)
    {
      win_printf_OK("The E-709 isn't connecte");
      return D_O_K;
    }

  Controller_ID = PI_ConnectUSB(szBuffer);

  bTest = PI_IsConnected(Controller_ID);
  if (bTest == FALSE) win_printf ("PI E709 Not Connected") ;

  /////////////////////////////////////////
  // Get the name of the connected axis. //
  /////////////////////////////////////////

  if (!PI_qSAI(Controller_ID, szAxes, 16))
    {
      iError = PI_GetError(Controller_ID);
      PI_TranslateError(iError, szErrorMessage, 1024);
      win_printf("SAI?: ERROR %d: %s\n", iError, szErrorMessage);
      PI_CloseConnection(Controller_ID);
      return 0;
    }

  bReturn_SVO = TRUE;
  bTest = PI_SVO(Controller_ID, szAxes, &bReturn_SVO);
  if (bTest == FALSE) win_printf ("PI_SVO failed\n");

  bTest = PI_qSVO(Controller_ID, szAxes, &bReturn_SVO);
  if (bTest == FALSE) win_printf ("PI_qSVO failed\n");



  free(szBuffer);

grab_PI_z_value = PI_read_aut;


  /* if (bReturn_SVO == TRUE)  MessageBox (NULL, TEXT("TRUE"), "TRUE", MB_ICONERROR) ; */
  /* else  MessageBox (NULL, TEXT("FALSE"), "FALSE", MB_ICONERROR); */

	/* bTest = PI_MOV(Controller_ID, szAxes, &dMaxpos); */
	/* if (bTest == FALSE) */
	/*   { */
	/*     iError = PI_GetError(Controller_ID); */
	/*     PI_TranslateError(iError, szErrorMessage, 1024); */
	/*     printf("MOV: ERROR %d: %s\n", iError, szErrorMessage); */
	/*     MessageBox (NULL, TEXT ("MOV error!"), */
	/* 	   szErrorMessage, MB_ICONERROR) ; */
	/*     PI_CloseConnection(Controller_ID); */
	/*     return (INT_PTR) 0; */
	/*   } */


	/* bTest = PI_qPOS(Controller_ID, szAxes, &dPosition); */
	/* if (bTest == FALSE) MessageBox (NULL, TEXT("Merde"), "Merde", MB_ICONERROR) ; */
	/* StringCbPrintf(szErrorMessage,1024,TEXT("Position: %f "), (float)dPosition); */
	/* MessageBox (NULL, szErrorMessage, "Position", MB_ICONERROR) ;  */

  return 1;
}


int PI_bouge(double pos)
{
  int bTest = FALSE;
  bTest =  PI_MOV(Controller_ID, szAxes, (const double*)&pos);
  if (bTest == FALSE)
    {
      win_printf_OK("PI_MOV failed\n");
      return 0;
    }
  return 1;
}


int PI_bouge_rel(double pos)
{
  int bTest = FALSE;
  bTest =  PI_MVR(Controller_ID, szAxes, (const double*)&pos);
  if (bTest == FALSE)
    {
      win_printf_OK("PI_MVR failed\n");
      return 0;
    }
  return 1;
}

int PI_pos(double* pos)
{
 int bTest = FALSE;
  if (Controller_ID == -1) Init_PI();

  if (Controller_ID == -1)
    {
      win_printf_OK("cannot init PI");
      return 0;
    }
  if (Controller_ID == -1) return 0;
  bTest = PI_qPOS(Controller_ID, szAxes,pos);
  if (bTest == FALSE)
    {
      win_printf_OK("PI_qPOS failed\n");
      return 0;
    }
  return 1;
}


void *rampe(void *param)
{
  struct Rampe *r = NULL;
  r = param;
  unsigned long time = 0;
  static int i = 0;
  //time = my_uclock();
  double step = r->step;
  double pos = 0;
  if (r == NULL) return NULL;
  if (step*r->x < step*r->end)
    {
      if (PI_bouge_rel(step) == 0)
	{
	  return NULL;
	}
      r->x += step;
      i++;
      //time = my_uclock() - time;

      /* if( r->time_step - time < 0)  */
      /* 	{ */
      /* 	  win_printf_OK("The Pi doesn't have the time to react, too short time step %f", r->time_step - time); */
      /* 	  return NULL; */
      /* 	  */
      /* 	} */
      psleep(r->time_step);
      pthread_create(r->thread,NULL,rampe, r);

    }
  else
    free(r);

  return NULL;
}


int do_PI_E709_ramp(void)
{
  static double pos_start = 0;
  static double pos_end = 250;
  static int nb_steps = 50;
  static double time_btw_steps = 100;
  struct Rampe* r;
  pthread_t ramp_thread;

  if(updating_menu_state != 0)	return D_O_K;


  if (Controller_ID == -1) Init_PI();
  if (Controller_ID == -1) return win_printf_OK("cannot init PI");


  if ( win_scanf("Start of the ramp (�m) %8lf \n"
		 "End of the ramp (�m) %8lf \n"
		 "Number of steps %d \n"
		 "Time between steps (ms) : %8lf",&pos_start,&pos_end,&nb_steps,&time_btw_steps) == CANCEL)  return D_O_K;



  r = malloc(sizeof(Rampe));
  r->start = pos_start;
  r->end = pos_end;
  r->x = pos_start;
  r->step = (pos_end-pos_start)/nb_steps;
  r->time_step = time_btw_steps;
  r->nb_steps = nb_steps;
  r->thread = &ramp_thread;
  pthread_create(&ramp_thread, NULL, rampe, (void *)r);
  return 0;
}




int do_PI_E709_record_ramp_movie(void)
{
  int i, ii, jj, im;
  static double pos_start = 0;
  static double pos_step = 1;
  static int nb_steps = 50;
  static double time_btw_steps = 100;
  imreg *imr = NULL;
  union pix *pd, *ps;
  O_i *oid = NULL, *ois = NULL;

  if(updating_menu_state != 0)	return D_O_K;


  if (Controller_ID == -1) Init_PI();
  if (Controller_ID == -1) return win_printf_OK("cannot init PI");


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return win_printf_OK("cannot find image region");

  pos_start = PI_read_aut();

  i = win_scanf("Start of the ramp (�m) %8lf \n"
		 "step in ramp (�m) %8lf \n"
		 "Number of steps %d \n"
		 "Time between steps (ms) : %8lf"
		,&pos_start,&pos_step,&nb_steps,&time_btw_steps);

  if (i == CANCEL)  return D_O_K;


  oid = create_and_attach_movie_to_imr(imr, ois->im.nx, ois->im.ny, ois->im.data_type, nb_steps);
  if (oid == NULL)	  return win_printf_OK("Can't create dest image");

  for (im = 0; im < nb_steps; im++)
    {
      PI_bouge(pos_start+im*(pos_step));
      psleep(time_btw_steps);
      switch_frame(oid,im);
      pd = oid->im.pixel;
      ps = ois->im.pxl[ois->im.c_f];
      for (ii = 0 ; ii< ois->im.ny ; ii++)
	  {
	    for (jj = 0 ; jj< ois->im.nx; jj++)
	      {
		switch (ois->im.data_type)
		  {
		  case  IS_CHAR_IMAGE:
		    pd[ii].ch[jj] = ps[ii].ch[jj];
		    break;
		  case  IS_RGB_PICTURE:
		    pd[ii].ch[3*jj] = ps[ii].ch[3*jj];
		    pd[ii].ch[(3*jj)+1] = ps[ii].ch[(3*jj)+1];
		    pd[ii].ch[(3*jj)+2] = ps[ii].ch[(3*jj)+2];
		    break;
		  case  IS_RGBA_PICTURE:
		    pd[ii].ch[3*jj] = ps[ii].ch[3*jj];
		    pd[ii].ch[(3*jj)+1] = ps[ii].ch[(3*jj)+1];
		    pd[ii].ch[(3*jj)+2] = ps[ii].ch[(3*jj)+2];
		    pd[ii].ch[(3*jj)+3] = ps[ii].ch[(3*jj)+3];
		    break;
		  case  IS_RGB16_PICTURE:
		    pd[ii].in[3*jj] = ps[ii].in[3*jj];
		    pd[ii].in[(3*jj)+1] = ps[ii].in[(3*jj)+1];
		    pd[ii].in[(3*jj)+2] = ps[ii].in[(3*jj)+2];
		    break;
		  case  IS_RGBA16_PICTURE:
		    pd[ii].in[3*jj] = ps[ii].in[3*jj];
		    pd[ii].in[(3*jj)+1] = ps[ii].in[(3*jj)+1];
		    pd[ii].in[(3*jj)+2] = ps[ii].in[(3*jj)+2];
		    pd[ii].in[(3*jj)+3] = ps[ii].in[(3*jj)+3];
		    break;
		  case  IS_INT_IMAGE:
		    pd[ii].in[jj] = ps[ii].in[jj];
		    break;
		  case  IS_UINT_IMAGE:
		    pd[ii].ui[jj] = ps[ii].ui[jj];
		    break;
		  case  IS_LINT_IMAGE:
		    pd[ii].li[jj] = ps[ii].li[jj];
		    break;
		  case  IS_FLOAT_IMAGE:
		    pd[ii].fl[jj] = ps[ii].fl[jj];
		    break;
		  case  IS_COMPLEX_IMAGE:
		    pd[ii].fl[jj] = ps[ii].fl[jj];
		    pd[ii].fl[jj+ois->im.nx] = ps[ii].fl[jj+ois->im.nx];
		    break;
		  case  IS_DOUBLE_IMAGE:
		    pd[ii].db[jj] = ps[ii].db[jj];
		    break;
		  case  IS_COMPLEX_DOUBLE_IMAGE:
		    pd[ii].db[jj] = ps[ii].db[jj];
		    pd[ii].db[jj+ois->im.nx] = ps[ii].db[jj+ois->im.nx];
		    break;
		  }
	      }
	  }
      oid->im.user_fspare[im][0] = PI_read_aut();
    }
  set_oi_source(oid,"Movie Zobj ramp starting %g over %d steps of %g dt %g",pos_start,nb_steps,pos_step,time_btw_steps);
  return (refresh_image(imr, imr->n_oi - 1));
}


int do_PI_E709_record_steps_movie(void)
{
  int i, ii, jj, im;
  static double pos_start = 0;
  static double pos_step = 1;
  static int nb_steps = 50;
  static int nb_stop = 10 ;
  static double time_btw_steps = 100;
  imreg *imr = NULL;
  union pix *pd, *ps;
  O_i *oid = NULL, *ois = NULL;
  double *trajectory=NULL;
  int nb_points;
  int constant_i,step_i;

  if(updating_menu_state != 0)	return D_O_K;


  if (Controller_ID == -1) Init_PI();
  if (Controller_ID == -1) return win_printf_OK("cannot init PI");


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return win_printf_OK("cannot find image region");

  pos_start = PI_read_aut();

  i = win_scanf("Position min in the trajectory %8lf \n"
		 "step %8lf \n"
		 "Number of steps %d \n"
		 "Time between orders (ms) : %8lf \n"
     "Size of plateaux %d"
		,&pos_start,&pos_step,&nb_steps,&time_btw_steps,&nb_stop);

  if (i == CANCEL)  return D_O_K;
  nb_points = nb_stop*nb_steps;
  trajectory=calloc(nb_stop*nb_steps,sizeof(double));
  for (step_i=0;step_i<nb_steps;step_i++)
  {
    for (constant_i=0;constant_i<nb_stop;constant_i++)
      {
        trajectory[step_i*nb_stop + constant_i]=(double) step_i*pos_step ;
      }
  }

  oid = create_and_attach_movie_to_imr(imr, ois->im.nx, ois->im.ny, ois->im.data_type, nb_steps*nb_stop);
  if (oid == NULL)	  return win_printf_OK("Can't create dest image");

  for (im = 0; im < nb_steps*nb_stop; im++)
    {
      PI_bouge(pos_start+trajectory[im]);
      psleep(time_btw_steps);
      switch_frame(oid,im);
      pd = oid->im.pixel;
      ps = ois->im.pxl[ois->im.c_f];
      for (ii = 0 ; ii< ois->im.ny ; ii++)
	  {
	    for (jj = 0 ; jj< ois->im.nx; jj++)
	      {
		switch (ois->im.data_type)
		  {
		  case  IS_CHAR_IMAGE:
		    pd[ii].ch[jj] = ps[ii].ch[jj];
		    break;
		  case  IS_RGB_PICTURE:
		    pd[ii].ch[3*jj] = ps[ii].ch[3*jj];
		    pd[ii].ch[(3*jj)+1] = ps[ii].ch[(3*jj)+1];
		    pd[ii].ch[(3*jj)+2] = ps[ii].ch[(3*jj)+2];
		    break;
		  case  IS_RGBA_PICTURE:
		    pd[ii].ch[3*jj] = ps[ii].ch[3*jj];
		    pd[ii].ch[(3*jj)+1] = ps[ii].ch[(3*jj)+1];
		    pd[ii].ch[(3*jj)+2] = ps[ii].ch[(3*jj)+2];
		    pd[ii].ch[(3*jj)+3] = ps[ii].ch[(3*jj)+3];
		    break;
		  case  IS_RGB16_PICTURE:
		    pd[ii].in[3*jj] = ps[ii].in[3*jj];
		    pd[ii].in[(3*jj)+1] = ps[ii].in[(3*jj)+1];
		    pd[ii].in[(3*jj)+2] = ps[ii].in[(3*jj)+2];
		    break;
		  case  IS_RGBA16_PICTURE:
		    pd[ii].in[3*jj] = ps[ii].in[3*jj];
		    pd[ii].in[(3*jj)+1] = ps[ii].in[(3*jj)+1];
		    pd[ii].in[(3*jj)+2] = ps[ii].in[(3*jj)+2];
		    pd[ii].in[(3*jj)+3] = ps[ii].in[(3*jj)+3];
		    break;
		  case  IS_INT_IMAGE:
		    pd[ii].in[jj] = ps[ii].in[jj];
		    break;
		  case  IS_UINT_IMAGE:
		    pd[ii].ui[jj] = ps[ii].ui[jj];
		    break;
		  case  IS_LINT_IMAGE:
		    pd[ii].li[jj] = ps[ii].li[jj];
		    break;
		  case  IS_FLOAT_IMAGE:
		    pd[ii].fl[jj] = ps[ii].fl[jj];
		    break;
		  case  IS_COMPLEX_IMAGE:
		    pd[ii].fl[jj] = ps[ii].fl[jj];
		    pd[ii].fl[jj+ois->im.nx] = ps[ii].fl[jj+ois->im.nx];
		    break;
		  case  IS_DOUBLE_IMAGE:
		    pd[ii].db[jj] = ps[ii].db[jj];
		    break;
		  case  IS_COMPLEX_DOUBLE_IMAGE:
		    pd[ii].db[jj] = ps[ii].db[jj];
		    pd[ii].db[jj+ois->im.nx] = ps[ii].db[jj+ois->im.nx];
		    break;
		  }
	      }
	  }
      oid->im.user_fspare[im][0] = PI_read_aut();
    }
  //set_oi_source(oid,"Movie steps starting %g over %d steps of %g dt %g and with plateaux ofoints",pos_start,nb_steps,pos_step,time_btw_steps);
  free(trajectory);
  trajectory=NULL;
  return (refresh_image(imr, imr->n_oi - 1));
}


int do_PI_E709_init(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  Init_PI();
  return 0;
}



int do_PI_E709_mov(void)
{
  int i;
  double pos = Piezzo_position;
  if(updating_menu_state != 0)	return D_O_K;
  i = win_scanf("Where do you want to go %8lf",&pos);
  if (i == CANCEL) return D_O_K;
  if(PI_bouge(pos) == 0) return 0;
  Piezzo_position = pos;
  return 0;
}


int up_move(void)
{
  const double a = 10.;
  if (PI_MVR(Controller_ID, szAxes, &a) == FALSE)
    win_printf_OK("PI_MOV failed\n"); return 0;
  win_printf_OK("bouh");
  return 1;
}


int down_move(void)
{
  const double a = -10.;
  if (PI_MVR(Controller_ID, szAxes,  &a) == FALSE)
    win_printf_OK("PI_MOV failed\n"); return 0;
  return 1;
}

int f1_move(void)
{
  const double a = -1;
  if (PI_MVR(Controller_ID, szAxes,  &a) == FALSE)
    win_printf_OK("PI_MOV failed\n"); return 0;
  return 1;
}

int f2_move(void)
{
  const double a = -0.1;
  if (PI_MVR(Controller_ID, szAxes,  &a) == FALSE)
    win_printf_OK("PI_MOV failed\n"); return 0;
  return 1;
}

int f3_move(void)
{
  const double a = 0.1;
  if (PI_MVR(Controller_ID, szAxes,  &a) == FALSE)
    win_printf_OK("PI_MOV failed\n"); return 0;
  return 1;
}

int f4_move(void)
{
  const double a = +1.;
  if (PI_MVR(Controller_ID, szAxes,  &a) == FALSE)
    win_printf_OK("PI_MOV failed\n"); return 0;
  return 1;
}

int f5_move(void)
{
  const double a = -0.005;
  if (PI_MVR(Controller_ID, szAxes,  &a) == FALSE)
    win_printf_OK("PI_MOV failed\n"); return 0;
  return 1;
}

int f6_move(void)
{
  const double a = +0.005;
  if (PI_MVR(Controller_ID, szAxes,  &a) == FALSE)
    win_printf_OK("PI_MOV failed\n"); return 0;
  return 1;
}

int f7_move(void)
{
  const double a = -0.01;
  if (PI_MVR(Controller_ID, szAxes,  &a) == FALSE)
    win_printf_OK("PI_MOV failed\n"); return 0;
  return 1;
}

int f8_move(void)
{
  const double a = +0.01;
  if (PI_MVR(Controller_ID, szAxes,  &a) == FALSE)
    win_printf_OK("PI_MOV failed\n"); return 0;
  return 1;
}

int do_PI_E709_moves(void)
{
  if(updating_menu_state != 0)
    {
      if (Controller_ID == -1) return D_O_K;
      else
	{
	  add_keyboard_short_cut(0, KEY_UP, 0, up_move );
	  add_keyboard_short_cut(0, KEY_DOWN, 0, down_move);
	  add_keyboard_short_cut(0, KEY_F1, 0, f1_move );
	  add_keyboard_short_cut(0, KEY_F2, 0, f2_move);
	  add_keyboard_short_cut(0, KEY_F3, 0, f3_move );
	  add_keyboard_short_cut(0, KEY_F4, 0, f4_move);
    add_keyboard_short_cut(0, KEY_F5, 0, f5_move );
	  add_keyboard_short_cut(0, KEY_F6, 0, f6_move);
    add_keyboard_short_cut(0, KEY_F7, 0, f7_move );
	  add_keyboard_short_cut(0, KEY_F8, 0, f8_move);
	}
    }

  if (Controller_ID == -1) Init_PI();
  if (Controller_ID == -1)
    {
      win_printf_OK("cannot init PI");
      return 0;
    }

  add_keyboard_short_cut(0, KEY_UP, 0, up_move );
  add_keyboard_short_cut(0, KEY_DOWN, 0, down_move);
  add_keyboard_short_cut(0, KEY_F1, 0, f1_move );
  add_keyboard_short_cut(0, KEY_F2, 0, f2_move);
  add_keyboard_short_cut(0, KEY_F3, 0, f3_move );
  add_keyboard_short_cut(0, KEY_F4, 0, f4_move);
  return D_O_K;
}

int do_PI_E709_read(void)
{
  double pos;
  if(updating_menu_state != 0)	return D_O_K;

  if (Controller_ID == -1) Init_PI();
  if (Controller_ID == -1) return win_printf_OK("cannot init PI");

  bTest =  PI_qPOS(Controller_ID, szAxes, &pos);
  if (bTest == FALSE) return win_printf_OK("PI_qPOS failed\n");
  win_printf_OK("Position : %f", pos);
  Piezzo_position = pos;

  return 0;
}

MENU *PI_E709_image_menu(void)
{
  static MENU mn[32];
  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Init PI_E709", do_PI_E709_init,NULL,0,NULL);
  add_item_to_menu(mn,"Read position", do_PI_E709_read,NULL,0,NULL);
  add_item_to_menu(mn,"Move to position", do_PI_E709_mov ,NULL,0,NULL);
  add_item_to_menu(mn,"Do a ramp", do_PI_E709_ramp ,NULL,0,NULL);
  add_item_to_menu(mn,"Do movie ramp", do_PI_E709_record_ramp_movie,NULL,0,NULL);
  add_item_to_menu(mn,"Do movie step", do_PI_E709_record_steps_movie,NULL,0,NULL);
  add_item_to_menu(mn,"Activate keyboard displacement", do_PI_E709_moves ,NULL,0,NULL);
  return mn;
}

int PI_E709_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_image_treat_menu_item ( "PI_E709", NULL, PI_E709_image_menu(), 0, NULL);
  do_PI_E709_moves();
  return D_O_K;
}

int	PI_E709_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "PI_E709", NULL, NULL);
  return D_O_K;
}
#endif
