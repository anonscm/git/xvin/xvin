#ifndef _DUMMY_FOCUS_H_
#define _DUMMY_FOCUS_H_



#ifdef _DUMMY_FOCUS_C_
float   dummy_obj_position = 0, dummy_Z_step = 0.1;
#endif

#ifndef _DUMMY_FOCUS_C_
PXV_VAR(float, dummy_obj_position); 
PXV_VAR(float, dummy_Z_step);
#endif

PXV_FUNC(MENU*, dummy_focus_plot_menu, (void));
PXV_FUNC(int, dummy_focus_main, (int argc, char **argv));
#endif

