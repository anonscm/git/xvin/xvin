/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _DUMMY_FOCUS_C_
#define _DUMMY_FOCUS_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 


//# include "../cfg_file/focus.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "dummy_focus.h"




//extern float n_rota;
//extern float n_magnet_z;



int _read_Z_value(void)
{
	return (int)(10*dummy_obj_position);	
}
int _set_Z_step(float z)  /* in micron */
{
	dummy_Z_step = z;
	return 0;
}



float _read_Z_value_accurate_in_micron(void)
{
	return dummy_obj_position;	
}

float _read_Z_value_OK(int *error) /* in microns */
{
	return dummy_obj_position;	
}

float _read_last_Z_value(void) /* in microns */
{
	return dummy_obj_position;	
}

int _set_Z_value(float z)  /* in microns */
{
	dummy_obj_position = z;
	return 0;
}
int	_set_Z_obj_accurate(float zstart)
{
	int er = 0;
	
	_set_Z_step(0.045);
	_set_Z_value(zstart);
	return er;
}
int _set_Z_value_OK(float z) 
{
	dummy_obj_position = z;
	return 0;
}

int _inc_Z_value(int step)  /* in micron */
{
	_set_Z_value(dummy_obj_position + dummy_Z_step*step);
	return 0;
}

int _small_inc_Z_value(int up)  
{
	int nstep;
	
	_set_Z_step(0.1);
	nstep = (up>=0)? 1:-1;
	_set_Z_value(dummy_obj_position + dummy_Z_step*nstep);
	
	//display_title_message("Z obj = %g \\mu m Rot %g Z mag %g mm",
        //  (float)dummy_read_Z_value()/10,n_rota,n_magnet_z);
	return 0;
}

int _big_inc_Z_value(int up)  
{
	int nstep;
	
	_set_Z_step(0.1);
	nstep = (up>=0)? 10:-10;
	_set_Z_value(dummy_obj_position + dummy_Z_step*nstep);
	//	display_title_message("Z obj = %g \\mu m Rot %g Z mag %g mm",
	//  (float)dummy_read_Z_value()/10,n_rota,n_magnet_z);

	return 0;
}




/*



int set_dummy_objective_device(void)
{
  //  focus_position = &dummy_obj_position;
  //focus_Z_step = &dummy_Z_step;

  read_Z_value = dummy_read_Z_value;
  read_Z_value_accurate_in_micron = dummy_read_Z_value_accurate_in_micron;
  read_Z_value_OK = dummy_read_Z_value_OK;
  
  set_Z_value = dummy_set_Z_value;
  set_Z_obj_accurate = dummy_set_Z_obj_accurate;
  set_Z_value_OK = dummy_set_Z_value_OK;
  
  set_Z_step = dummy_set_Z_step;
  inc_Z_value = dummy_inc_Z_value;
  
  small_inc_Z_value = dummy_small_inc_Z_value;
  big_inc_Z_value = dummy_big_inc_Z_value;

    what_is_objective_device = this_is_dummy_objective_device;
    describe_objective_device = describe_dummy_objective_device;
    z_objective_menu = dumobj_menu;

  return 0;
}


int dummy_read_Z(char ch)
{
	int z;
	
	if (ch != CR)	return OFF;
	z = dummy_read_Z_value();
	return win_printf("The objective position in z is %f \\mu m",dummy_obj_position);
}

int dummy_set_Z(char ch)
{
	register int i;
	static float z = 0;
	
	if (ch != CR)	return OFF;
	
	z = dummy_obj_position;
	i = win_scanf("Where do you want to go in \\mu m %f",&z);
	if (i == CANCEL)	return OFF;
	
	dummy_set_Z_value(z);
	
	return OFF;
}

int dummy_set_Z_accu(char ch)
{
	register int i;
	static float z = 0;
	
	if (ch != CR)	return OFF;
	
	z = dummy_obj_position;
	i = win_scanf("Where do you want to go (\\mu m) ?\n  %f",&z);
	if (i == CANCEL)	return OFF;
	
	dummy_set_Z_value(z);
	dummy_obj_position = z;
	
	return OFF;
}

int dummy_set_step(char ch)
{
	register int i;
	static float z = 0.1;
	
	if (ch != CR)	return OFF;
	i = win_scanf("What step size do you want (\\mu m) ?\n %f",&z);
	if (i == CANCEL) return OFF;
	
	dummy_set_Z_step(z);
	
	return OFF;
}

int dummy_inc_of_nstep(char ch)
{
	register int i;
	static int n = 0;
	
	if (ch != CR)	return OFF;
	i = win_scanf("How many steps? %d",&n);
	if (i == CANCEL)	return OFF;
	dummy_inc_Z_value(n);

	return OFF;
}

int dummy_back_forth_Z_check(char ch)
{
	register int i;
	static int n = 100;
	float z, z1;

	
	if (ch != CR)	return OFF;
	
	z = dummy_read_Z_value();
	z1 = z + 1;
	i = win_scanf("Alternate objectif from %f to %f over %d cycles",&z,&z1,&n);
	if (i == CANCEL)	return OFF;
	
	for(i = 0; i < n; i++)
	{
		dummy_set_Z_value(z);
		dummy_set_Z_value(z1);
	}
	
	return OFF;
}

int dummy_back_forth_Z_step(char ch)
{
	register int i;
	static int n = 100, step = 1;
	float z;

	
	if (ch != CR)	return OFF;
	z = dummy_read_Z_value();
	
	i = win_scanf("switch from in \\mu m %fstep of %dfor n cycles %d",&z,&step,&n);
	if (i == CANCEL)	return OFF;
	
	for(i = 0; i < n; i++)
	{
		dummy_inc_Z_value(step);
		dummy_inc_Z_value(-step);
	}
	
	return OFF;
}



acreg 	*dumobj_menu(void)
{
	static acreg *mo_mn = NULL;
	

	if (mo_mn != NULL)	return mo_mn;

	mo_mn = build_menu("DUM-OBJ",1);	
	add_menu_item(mo_mn,"read Z",dummy_read_Z,ONE_BOUNCE,1);
	add_menu_item(mo_mn,"Set Z",dummy_set_Z,ONE_BOUNCE,1);
	add_menu_item(mo_mn,"Set Z accu",dummy_set_Z_accu,ONE_BOUNCE,1);
	add_menu_item(mo_mn,"Set step Z",dummy_set_step,ONE_BOUNCE,1);
	add_menu_item(mo_mn,"n steps",dummy_inc_of_nstep,ONE_BOUNCE,1);
	add_menu_item(mo_mn,"Set Z  to Z1",dummy_back_forth_Z_check,ONE_BOUNCE,1);
	add_menu_item(mo_mn,"Set Z step",dummy_back_forth_Z_step,ONE_BOUNCE,1);
		
	return mo_mn;
}

*/


MENU *dummy_focus_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//	add_item_to_menu(mn,"data set rescale in Y", do_dummy_focus_rescale_data_set,NULL,0,NULL);
	//add_item_to_menu(mn,"plot rescale in Y", do_dummy_focus_rescale_plot,NULL,0,NULL);
	return mn;
}

int	dummy_focus_main(int argc, char **argv)
{
  //	add_plot_treat_menu_item ( "dummy_focus", NULL, dummy_focus_plot_menu(), 0, NULL);
  //set_dummy_objective_device();
  return D_O_K;
}

int	dummy_focus_unload(int argc, char **argv)
{
  //	remove_item_to_menu(plot_treat_menu, "dummy_focus", NULL, NULL);
	return D_O_K;
}
#endif

