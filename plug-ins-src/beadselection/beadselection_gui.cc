#include <regex>
#include <allegro.h>
#include <algorithm>
#include <fstream>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/median.hpp>
#include "xvin.h"
#include "float.h"
#include "memorized_win_scan.hh"
#include "menu_template.hh"
#include "beadselection.h"
#include "../trackBead/indexing.h"
#include "../filters/accumulators.h"
#define BS_CKEY "BEAD-SELECTION"

/* But not below this define */
#define BUILDING_PLUGINS_DLL
XV_VAR(int, CAN_KEY_PRESS);

namespace beadselection
{
    namespace
    {
        namespace ba = boost::accumulators;
        namespace bt = boost::accumulators::tag;

        std::string _trim(std::string reas)
        {
            auto x = reas.find_last_not_of(" \t\n");
            if(x == std::string::npos)
                return "";
            reas.resize(x+1);
            x = reas.find_first_not_of(" \t\n");
            if(x != std::string::npos)
                return reas.substr(x);
            else
                return reas;
        }

        int _find(Args const & cf, one_plot const * op)
        {
            std::string fname = indexing::filename(op);
            std::smatch val;
            if(std::any_of(cf.options.begin(), cf.options.end(),
                           [&](auto const & opt)
                           { return std::regex_match(fname, val, opt); }))
                return std::stoi(val[1].str());
            return std::numeric_limits<int>::max();
        }

        auto _stats(O_p const * op)
        {
            using     bmed_t = ba::accumulator_set<float, ba::stats<bt::median>>;

            ba::accumulator_set<float, ba::stats<bt::median, bt::min, bt::max, bt::count>> info;
            for(auto ds = op->dat, e = ds + op->n_dat; ds != e; ++ds)
            {
                if((*ds)->nx  == 0)
                    continue;

                bmed_t med;
                for(auto x = (*ds)->yd, y = x+(*ds)->nx-1; x != y; ++x)
                    med(x[1]-x[0]);
                auto m = ba::median(med);

                bmed_t meddev;
                for(auto x = (*ds)->yd, y = x+(*ds)->nx-1; x != y; ++x)
                    meddev(std::abs(x[1]-x[0]-m));

                info(ba::median(meddev));
            };

            return info;
        }

        auto _height(O_p const * op)
        {
            ba::accumulator_set<float, ba::stats<bt::mean>> info;
            for(auto ds = op->dat, e = ds + op->n_dat; ds != e; ++ds)
            {
                if((*ds)->nx  <= 10)
                    continue;

                ba::accumulator_set<float, ba::stats<bt::min, bt::max>> curr;
                float const * yd = (*ds)->yd;
                for(int ix = 0, e = (*ds)->nx-10; ix < e; ++ix)
                    curr(stats::compute<bt::median>(10, yd+ix));

                info(ba::max(curr)-ba::min(curr));
            };

            return ba::mean(info);
        }
    };

    std::string query(one_plot const * op, std::vector<std::string> & reasons)
    {
        auto info   = _stats(op);
        auto height = _height(op);
        char tmp[2048];
        snprintf(tmp, sizeof(tmp),
                 "{\\color{yellow}Plot key shortcuts:}\n"
                 "  - <Control-Left>  pan left\n"
                 "  - <Control-Right> pan right\n"
                 "  - <Control-Up>    zoom in\n"
                 "  - <Control-Down>  zoom out\n"
                 "\n{\\color{yellow}Plot stats:}\n"
                 "  - %d cycles on this bead\n"
                 "  - %.5f is their mean extension\n"
                 "  - %.5f is their median deviation (~ median level of noise)\n"
                 "  - %.5f is their min median deviation\n"
                 "  - %.5f is their max median deviation\n"
                 "\n{\\color{yellow}What to do with plot ",
                 int   (ba::count(info)),  double(height),
                 double(ba::median(info)), double(ba::min(info)),
                 double(ba::max(info)));

        std::string dialog = tmp+indexing::filename(op);

        dialog += "?}\n\n %R Keep it\n\nOr discard it:\n";
        for(auto const & r: reasons)
            dialog += " %r " + r + "\n";
        dialog += "\n    %s";

        std::string user;
        int         val = 0;

        char cdiag[2024]; strcpy(cdiag, dialog.c_str());
        CAN_KEY_PRESS = 1;
        if(config::query(nullptr, cdiag, val, user))
        {
            CAN_KEY_PRESS = 0;
            return "###EXIT";
        }
        CAN_KEY_PRESS = 0;

        std::string x = _trim(user);
        if(x.size() > 0)
        {
            reasons.push_back(x);
            return x;
        }
        return val == 0 ? std::string("") : reasons[val-1];
    }

    bool write(std::string fname, std::vector<Discarded> const & disc)
    {
        std::ofstream stream(fname.c_str());
        if(!stream.good())
            return true;

        stream << "Bead Id;\tPath;\tReason" << std::endl;
        for(auto const & x: disc)
            stream << x.id << ";\t" << x.path << ";\t" << x.reason << std::endl;
        stream.close();
        return false;
    }

    void read(std::string fname, std::vector<Discarded> & disc)
    {
        std::ifstream stream(fname.c_str());
        if(!stream.good())
            return;

        std::smatch val;
        std::regex  patt("(\\d+)\\s*;\\s*([^;]*)\\s*;\\s*([^;]*)\\s*");

        std::string line;
        while(std::getline(stream, line))
            if(std::regex_match(line, val, patt) && val.size() == 4)
            {
                Discarded cur = {0, _trim(val[2].str()), _trim(val[3].str())};
                try { cur.id = std::stoi(val[1].str()); }
                catch(...) { continue; }

                disc.push_back(cur);
            }
        stream.close();
    }

    bool update(Args                    const & cf,
                plot_region                   * plt,
                one_plot                      * op,
                std::vector<Discarded>        & disc,
                std::vector<std::string>      & reasons)
    {
        auto id = _find(cf, op);
        if(id != std::numeric_limits<int>::max())
        {
            menu::refresh(plt, op);
            auto res = query(op, reasons);
            if(res == "###EXIT")
                return true;
            if(res.size())
                disc.emplace_back(Discarded{id, "", res});
        }
        return false;
    }

    void update(Args const & cf, plot_region * plt)
    {
        auto fname = indexing::filename(plt);
        auto sfile = fname;
        if (sfile == "")
            return;

        std::string creas;
        config::get("BEAD-SELECTION", "REASONS", creas);

        std::vector<std::string> reasons;
        if(creas.size() == 0)
            reasons = {"Fixed bead",
                       "Not my hairpin",
                       "Structural bindings",
                       "Too few cycles",
                       "Too noisy"
                      };
        else
        {
            std::istringstream stream(creas);
            std::string line;
            while(std::getline(stream, line, ';'))
                if(line.size() > 1)
                    reasons.push_back(line);
        }

        std::vector<Discarded>  disc;
        std::vector<one_plot *> ops(plt->o_p, plt->o_p+plt->n_op);

        std::map<one_plot *, decltype(_stats(NULL))> stats;
        for(auto op: ops)
            stats[op] = _stats(op);

        std::sort(ops.begin(), ops.end(),
                  [&](auto o1, auto o2)
                  {
                      auto m1 = ba::median(stats[o1]);
                      auto m2 = ba::median(stats[o2]);
                      if(m1 != m2)
                          return m1 > m2;

                      auto n1 = ba::count(stats[o1]);
                      auto n2 = ba::count(stats[o2]);
                      return n1 < n2;
                  });

        for(auto op: ops)
            if(update(cf, plt, op, disc, reasons))
                return;

        if(disc.size() == 0)
            return;

        creas = "";
        for(auto l: reasons)
            creas += l+";";
        config::set("BEAD-SELECTION", "REASONS", creas);

        for(auto & x: disc)
            x.path = fname;

        sfile.resize(sfile.rfind('.'));
        sfile += "_discarded.csv";

        char file[2048];
        strcpy(file, sfile.c_str());

        auto f = save_one_file_config("Save discarded beads list",
                                      nullptr, file,
                                      "csv File\0*.csv\0txt File\0*.txt\0\0",
                                      BS_CKEY, "DISCARDED-LAST-SAVE");
        if(f == nullptr || strlen(f) == 0)
            return;

        std::vector<Discarded> tmp;
        read(f, tmp);
        for(auto const & x: disc)
            tmp.push_back(x);
        if(write(f, tmp))
            win_printf_OK("Could not write in file");

        for(auto op = plt->o_p, eop = op+plt->n_op; op != eop; ++op)
            apply(cf, disc, plt, *op);
        if(plt->n_op >= 1)
            plt->o_p[0]->need_to_refresh = 1;
        refresh_plot(plt, 0);
    }

    void apply(Args            const & cf,
               std::vector<Discarded>   const & disc,
               plot_region                    * plt,
               one_plot                       * op)
    {
        auto id = _find(cf, op);
        if(id == std::numeric_limits<int>::max())
            return;

        if(std::any_of(disc.begin(), disc.end(),
                       [&](auto const & x) { return x.id == id; }))
            remove_data_from_pltreg(plt, IS_ONE_PLOT, (void*) op);
    }

    void apply(Args const & cf, plot_region * plt)
    {
        auto f = open_one_file_config("Load discarded beads list",
                                      nullptr, "csv File\0*.csv\0txt File\0*.txt\0\0",
                                      BS_CKEY, "DISCARDED-LAST-SAVE");
        if(f == nullptr || strlen(f) == 0)
            return;

        std::vector<Discarded> disc;
        read(f, disc);
        for(auto op: std::vector<one_plot*>(plt->o_p, plt->o_p+plt->n_op))
            apply(cf, disc, plt, op);
        if(plt->n_op >= 1)
            plt->o_p[0]->need_to_refresh = 1;
        refresh_plot(plt, 0);
    }
}

#define BEADSELECTION_MENU                                   \
    ((false, "update discarded plots", do_update_selection)) \
    ((false, "apply selection",        do_apply_selection))
MENU_DECLARE_ALL(beadselection, BEADSELECTION_MENU)


int do_update_selection()
{
    if (updating_menu_state != 0)
        return D_O_K;

    beadselection::Args args;
    beadselection::update(args, indexing::currentpltreg());
    return D_O_K;
}

int do_apply_selection ()
{
    if (updating_menu_state != 0)
        return D_O_K;

    beadselection::Args args;
    beadselection::apply(args, indexing::currentpltreg());
    return D_O_K;
}

MENU_IMPL_ALL(beadselection, BEADSELECTION_MENU)
