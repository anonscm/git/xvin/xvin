#pragma once
#include <vector>
#include <regex>
extern "C" { struct plot_region; struct one_plot; };
namespace beadselection
{
    struct Args
    {
        std::vector<std::regex> options = {std::regex(".*bd(\\d+)track\\d+.*")};
    };

    struct Discarded
    {
        int             id;
        std::string     path;
        std::string     reason;
    };

    std::string query(one_plot const *);
    bool write (std::string, std::vector<Discarded> const &);
    void read  (std::string, std::vector<Discarded> &);
    bool update(Args                    const & cf,
                plot_region                   * plt,
                one_plot                      * op,
                std::vector<Discarded>        & disc,
                std::vector<std::string>      & reasons);
    void update(Args const &, plot_region * = nullptr);

    void apply(Args                   const &,
               std::vector<Discarded> const &,
               plot_region                  *,
               one_plot                     *);
    void apply(Args const &, plot_region * = nullptr);
}
