#ifndef _SELECT_H_
#define _SELECT_H_

PXV_FUNC(int, do_zoom_in_x_on_op, (void));
PXV_FUNC(int, do_unzoom_in_x_on_op, (void));
PXV_FUNC(int, do_move_in_x_on_op, (void));
PXV_FUNC(int, do_adjust_y_lim_of_cur_ds_visi, (void));
PXV_FUNC(int, find_best_y_half, (d_s *data, d_s *poly, int i0, int i1, double *a, 
			double *b, double *c));
PXV_FUNC(int, find_best_y, (d_s *data, d_s *poly, int i0, int i1, double *a, 
			double *b, double *c));
PXV_FUNC(double, compute_chi2, (d_s *data, d_s *poly));
PXV_FUNC(int, do_simple_poly_on_segment, (pltreg *pr, O_p *op, d_s *dsi, float x0, 
			float y0, float x1, float y1));
PXV_FUNC(int, do_burst_segment, (pltreg *pr, O_p *op, d_s *dsd, float x0, float y0, 			float x1, float y1, int mode));
PXV_FUNC(int, find_selected_pr_segment, (pltreg *pr, int x_0, int y_0, DIALOG *d));
PXV_FUNC(int, find_selected_pr_burst_segment, (pltreg *pr, int x_0, int y_0, 
			DIALOG *d));
PXV_FUNC(int, 	do_select_pr_segment, (void));
PXV_FUNC(int, 	do_select_pr_burst_segment, (void));


PXV_FUNC(int, do_select_rescale_plot, (void));
PXV_FUNC(MENU*, select_plot_menu, (void));
PXV_FUNC(int, do_select_rescale_data_set, (void));
PXV_FUNC(int, select_main, (int argc, char **argv));
#endif
