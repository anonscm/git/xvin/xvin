/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _SELECT_C_
#define _SELECT_C_

# include "allegro.h"
# include "xvin.h"
# include "float.h"
# include "ctype.h"

/* If you include other plug-ins header do it here*/ 
# include "../nrutil/nrutil.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "select.h"

int 	n_burst = 0;
float N_minv = -5, N_maxv = 5; /* nm/s */
float Z_v = 60, H_v = -60; /* nm/s */

/* 	given a data set of data fit a polygone line to it; this routine 
	compute the best y value at one of the end
	the fit is made for i1
*/			
double	chi2_of_bump(d_s *dsi, int start, int bump0, int bump1, int end, 
		     double *mean, double *bump);

int	find_x_selected_bounds(d_s *dsi, float x_lo, float x_hi, int *x_start, int *x_end);

int	find_best_y_half(d_s *data, d_s *poly, int i0, int i1, double *a, double *b, double *c)
{
  register int i;
  int *ipoly;
  double X1, Y1, X0, Y0, dX, dxX0, dxX1, tmp;
  int id0, id1, di;
	
  if (i0 < 0 || i1 < 0 || i0 >= poly->nx || i1 >= poly->nx)	return 1;
	
  ipoly = (int*)poly->xd;
  X0 = data->xd[ipoly[i0]];
  Y0 = poly->yd[i0];
  X1 = data->xd[ipoly[i1]];
  Y1 = poly->yd[i1];	
  dX = X1 - X0;
  if (dX == 0)	return 1;
  id0 = ipoly[i0];
  id1 = ipoly[i1];
  if (id0 == id1)	return 1;
  *a = *b = *c = 0;
  for (i = id0, di = (id1-id0)/abs(id1-id0);i != id1; i += di)
    {
      dxX0 = data->xd[i] - X0;
      dxX1 = X1 - data->xd[i];
      tmp = dxX0/dX;
      *a += tmp*tmp;
      *b += data->yd[i]*tmp;
      *c += tmp*dxX1/dX;
    }  
	
  *c *= Y0;
  return 0;
}


int		find_best_y(d_s *data, d_s *poly, int i0, int i1, double *a, double *b, double *c)
{
  register int i;
  int *ipoly;
  double X1, Y1, X0, Y0, dX, dxX0, dxX1, tmp;
  int id0, id1, di;
	
  if (i0 < 0 || i1 < 0 || i0 >= poly->nx || i1 >= poly->nx)	return 1;
	
  ipoly = (int*)poly->xd;
  X0 = data->xd[ipoly[i0]];
  Y0 = poly->yd[i0];
  X1 = data->xd[ipoly[i1]];
  Y1 = poly->yd[i1];	
  dX = X1 - X0;
  if (dX == 0)	return 1;
  id0 = ipoly[i0];
  id1 = ipoly[i1];
  if (id0 == id1)	return 1;
  *a = *b = *c = 0;
  for (i = id0, di = (id1-id0)/abs(id1-id0);i != id1; i += di)
    {
      dxX0 = data->xd[i] - X0;
      dxX1 = X1 - data->xd[i];
      tmp = dxX0/dX;
      *a += tmp*tmp;
      *b += data->yd[i]*tmp;
      *c += tmp*dxX1/dX;
    }
	
  dxX0 = data->xd[id1] - X0;
  dxX1 = X1 - data->xd[id1];
  tmp = dxX0/dX;
  *a += tmp*tmp;
  *b += data->yd[id1]*tmp;
  *c += tmp*dxX1/dX;
	  
  *c *= Y0;
  return 0;
}

double compute_chi2(d_s *data, d_s *poly)
{
  register int i, j;
  double  tmp, dX, chi2 = 0;
  int *ipoly, i0, i1;

  ipoly = (int*)poly->xd;
  for(i = 0; i < poly->nx-1; i++)
    {
      i0 = ipoly[i];
      i1 = ipoly[i+1];
      dX = data->xd[i1] - data->xd[i0];
      for (j = i0; j < i1; j++)
	{
	  tmp = data->yd[j] + poly->yd[i]*(data->xd[j]-data->xd[i1])/dX;
	  tmp -= poly->yd[i+1]*(data->xd[j]-data->xd[i0])/dX;
	  chi2 += tmp*tmp;			
	}
    }
  i1 = ipoly[poly->nx-1];

  chi2 += ((data->yd[i1] - poly->yd[poly->nx-1])*(data->yd[i1] - poly->yd[poly->nx-1]));
	
  return chi2;
}


int	recover_points_index_in_ds(float x0, float x1, d_s *dsi, int *imin, int *imax)
{
  float min, max, dmin, dmax;
  register int i;
	
  if (dsi == NULL)	return -1;
	
  min = (x0 < x1) ? x0 : x1;
  max = (x0 < x1) ? x1 : x0;

  for (*imin = *imax = i = 0, dmax = dmin = FLT_MAX; i < dsi->nx; i++) // *imin = dsi->nx -1,
    {
      if (dsi->xd[i] <= max && (max - dsi->xd[i]) < dmax)
	{
	  *imax = i;			
	  dmax = max - dsi->xd[i];
	}
      if (dsi->xd[i] >= min && (dsi->xd[i] - min) < dmin)
	{
	  *imin = i;		
	  dmin = dsi->xd[i] - min; 
	}
    }
  if (*imax <= *imin)	return 1;
  return 0;				
}

int	find_ds_y_limits(d_s *dsi, int start, int end, float *min_y, float *max_y )
{
  register int i;
	
  if (dsi == NULL)	return -1;
	
  start = (start < 0) ? 0 : start;
  start = (start < dsi->nx) ? start : dsi->nx;
  end = (end < 0) ? 0 : end;
  end = (end < dsi->nx) ? end : dsi->nx;
  for (i = start, *min_y = *max_y = dsi->yd[start]; i < end ; i++)
    {
      *max_y = (dsi->yd[i] > *max_y) ? dsi->yd[i] : *max_y;
      *min_y = (dsi->yd[i] < *min_y) ? dsi->yd[i] : *min_y;
    }
  return 0;
}
d_s	*add_polyline_ds(O_p *op, d_s *dsi, int imin, int imax, int nptp, int iter)
{ 
  register int i, j;
  int nf, *ipoly;
  double a,b,c, a1, b1, c1;	
  d_s *ds;
	
  if (nptp > imax-imin) return NULL;
	
  nf = 1 + (1+imax-imin)/nptp;
	
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return NULL;

  ds->symb = strdup("\\pt5\\oc ");
  ds->m = 1;				
			
	
  for (j = 0, ipoly = (int*)ds->xd; j < nf; j++)
    {
      ipoly[j] = imin + ((imax - imin)*j)/(nf-1);
      ds->yd[j] = dsi->yd[ipoly[j]];
    }
  display_title_message("\\chi^2 = %g",(float)compute_chi2(dsi, ds));
  for (i = 0; i < iter; i++)
    {
      find_best_y(dsi, ds, 1, 0, &a, &b, &c);
      ds->yd[0] = (a != 0) ? (b - c)/a : ds->yd[0];
      for (j = 1; j < nf-1; j++)
	{
	  find_best_y(dsi, ds, j-1, j, &a, &b, &c);
	  find_best_y_half(dsi, ds, j+1, j, &a1, &b1, &c1);
	  ds->yd[j] = ((a + a1) != 0) ? (b + b1 - c - c1)/(a + a1):ds->yd[j];
	}
      find_best_y(dsi, ds, nf-2, nf-1, &a, &b, &c);
      ds->yd[nf-1] = (a != 0) ? (b - c)/a : ds->yd[nf-1];	
    }
  ipoly = (int*)ds->xd;

  for (j = 0, ipoly = (int*)ds->xd; j < nf; j++)	
    ds->xd[j] = dsi->xd[ipoly[j]];
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  ds->treatement = my_sprintf(ds->treatement,
			      "polygon fit of %d pts and %d iterations",nf,iter);
		
  return ds;
}
d_s	*adjust_polyline_ds(d_s *dsi, int imin, int imax, int nptp, int iter)
{ 
  register int i, j;
  int nf, *ipoly;
  double a,b,c, a1, b1, c1;	
  d_s *ds;
	
  if (nptp > imax-imin) return NULL;
	
  nf = 1 + (1+imax-imin)/nptp;
	
  if ((ds = build_data_set(nf, nf)) == NULL)
    return NULL;
  ds->nx = nf;	ds->ny = nf;	
  ds->symb = strdup("\\pt5\\oc ");
  ds->m = 1;				
			
	
  for (j = 0, ipoly = (int*)ds->xd; j < nf; j++)
    {
      ipoly[j] = imin + ((imax - imin)*j)/(nf-1);
      ds->yd[j] = dsi->yd[ipoly[j]];
    }
  display_title_message("\\chi^2 = %g",(float)compute_chi2(dsi, ds));
  for (i = 0; i < iter; i++)
    {
      find_best_y(dsi, ds, 1, 0, &a, &b, &c);
      ds->yd[0] = (a != 0) ? (b - c)/a : ds->yd[0];
      for (j = 1; j < nf-1; j++)
	{
	  find_best_y(dsi, ds, j-1, j, &a, &b, &c);
	  find_best_y_half(dsi, ds, j+1, j, &a1, &b1, &c1);
	  ds->yd[j] = ((a + a1) != 0) ? (b + b1 - c - c1)/(a + a1):ds->yd[j];
	}
      find_best_y(dsi, ds, nf-2, nf-1, &a, &b, &c);
      ds->yd[nf-1] = (a != 0) ? (b - c)/a : ds->yd[nf-1];	
    }
  ipoly = (int*)ds->xd;

  for (j = 0, ipoly = (int*)ds->xd; j < nf; j++)	
    ds->xd[j] = dsi->xd[ipoly[j]];
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  ds->treatement = my_sprintf(ds->treatement,
			      "polygon fit of %d pts and %d iterations",nf,iter);
		
  return ds;
}

int retrieve_basename_and_index(char *filename, int n_file, char *basename, int n_base)
{
  int k, index;

  if (filename == NULL || basename == NULL)  return -1;
  for (k = 0; k < n_file && filename[k] != '.' && filename[k] != 0 ; k++);
  basename[(k < n_base) ? k : n_base-1] = 0;
  k = (k > 0) ? k-1 : k;
  for ( ; k >= 0 && isdigit(filename[k]); k--);
  if (sscanf(filename+k+1,"%d",&index) != 1) index = -1;
  basename[(k+1 < n_base) ? k+1 : n_base-1] = 0;
  for ( ; k >= 0; k--)
    basename[(k < n_base) ? k : n_base-1] = filename[k];
  return index;
}

int reload_hat_data(void)
{
  register int i, j, k;
  char *st, basename[256], file[1024];
  int findex;
  pltreg *pr;
  O_p *op, *opn;
  d_s *ds, *dsy, *dsz, *dsn;
  int nf, nstep;
  float rots, step, zmag;

 if(updating_menu_state != 0)	return D_O_K;		
	
	
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf("cannot find data");

  if (ds->source == NULL) return win_printf_OK("this is not a hat curve");
  if (strstr(ds->source,"Chapeau 2 beads") != NULL)
    {
      st = strstr(ds->source,"Z mag start");
      if (st == NULL) 
	return win_printf_OK("this is not a good hat curve");
      if (sscanf(st,"Z mag start = %f for %d frames scan by steps of %f for %d"
		 ,&rots,&nf,&step,&nstep) != 4)
	return win_printf_OK("problem recovering parameters from hat curve");
	
    }
  st = strstr(op->title,", rot");
  if (st == NULL) 
    return win_printf_OK("this is not a good hat curve title");
  if (sscanf(st,", rot %f",&zmag) != 1)
    return win_printf_OK("problem recovering zmag from hat curve");
  findex = retrieve_basename_and_index(op->filename, strlen(op->filename), basename, 256);

  sprintf(file,"traca%03d.gr",findex);
  opn = create_plot_from_gr_file(file, op->dir);
  if (opn == NULL) win_printf_OK("Could not loaded %s\n from %s",
				 backslash_to_slash(file), backslash_to_slash(op->dir));
  add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)opn);
  for (i = 0; i < opn->n_dat; i++)
    {
      dsy = opn->dat[0];
      if (dsy->source == NULL) continue;
      st = strstr(dsy->source,"Y coordinate");
      if (st != NULL) break;
    }
  if (st == NULL) 
    return win_printf_OK("problem recovering Y coordinate");

  for (i = 0; i < opn->n_dat; i++)
    {
      dsz = opn->dat[0];
      if (dsz->source == NULL) continue;
      st = strstr(dsy->source,"Z coordinate");
      if (st != NULL) break;
    }
  if (st == NULL) 
    return win_printf_OK("problem recovering Y coordinate");


  for (i = 0; i < ds->nx; i++)
    {
      j = (int)(0.5 + (ds->xd[i] - rots)/step);
      dsn = create_and_attach_one_ds(op, nf, nf, 0);
      if (dsn == NULL) 	 return win_printf("cannot create ds !");
      grey = 224-(j*192)/nstep;
      set_ds_line_color(dsn, makecol(grey,grey,grey));
      set_ds_source(dsn,"Partial trajectory at Zmag = %g Rot %g fileindex %d",
		    zmag,ds->xd[i],findex);
      set_ds_dot_line(dsn);
      for(k = 0; k < nf; k++)
	{
	  dsn->xd[k] = dsy->xd[j+nf];
	}
    }
  op->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);		
  return 0;

}

int separate_trajectories_by_zmag(void)
{
  register int i, j, k;
  pltreg *pr;
  O_p *op;
  d_s *ds, *dsz, *dsn;
  float zm[256], zmmax;
  int nzm[256], izm = 0, ndsz = -1, cnzm[256], c2nzm[256], max, jmax;
  static int ndsc = 8;
  char buf[512];

  if(updating_menu_state != 0)	return D_O_K;		
	
	
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf("cannot find data");


  dsz = find_source_specific_ds_in_op(op, "Zmag position in Bead tracking");
  if (dsz != NULL)
    {
      for (i = 0; op->dat[i] != dsz && i < op->n_dat; i++);
      ndsz = (i < op->n_dat) ? i : -1;
    }
  i = win_scanf("indicate the data set number \ncorresponding to Zmag%d",&ndsz);
  if (i == CANCEL) return D_O_K;
  if (ndsz < 0 || ndsz >= op->n_dat)
    win_printf_OK("ndsz %d out of range !");
  dsz = op->dat[ndsz];

  for (j = 0; j < 256; j++)
    {
      zm[j] = 0;
      nzm[j] = 0;
      cnzm[j] = -1;
      c2nzm[j] = -1;
    }

  for (i = izm = 0; i < dsz->nx; i++)
    {
      for (j = 0; (j < izm) && (dsz->yd[i] != zm[j]); j++);
      if (j < izm)	  nzm[j]++;
      else if (izm < 256)
	{
	  zm[izm] = dsz->yd[i];
	  nzm[izm] = 1;	  
	  izm++;
	}
    }

  sprintf(buf,"I have found %d values of zmag\nAmong the zmag values corresponding\n"
	      "to the largest number of points\n"
	      "Specify how many coloured data set you want %%d ",izm);

  i = win_scanf(buf,&ndsc);
  if (i == CANCEL) return D_O_K;

  for (i = 0; i < ndsc; i++)
    {
      for (j = 0, max = 0, jmax = -1; j < izm; j++)
	{
	  if (cnzm[j] < 0)
	    {
	      if (max < nzm[j])
		{
		  max = nzm[j];
		  jmax = j;
		}
	    }
	}
      if (jmax >= 0)
	{
	  cnzm[jmax] = i+1;
	}
      win_printf("%d selected jmax %d zmag %g with %d points",i,jmax,(jmax<0)?-1:zm[jmax],(jmax<0)?0:nzm[jmax]); 
    }



  for (i = 0; i < ndsc; i++)
    {
      for (j = 0, jmax = -1, zmmax = -FLT_MAX; j < izm; j++)
	{
	  if (cnzm[j] > 0 && c2nzm[j] < 0)
	    {
	      if (zmmax < zm[j])
		{
		  zmmax = zm[j];
		  jmax = j;
		}
	    }
	}
      if (jmax >= 0)
	{
	  c2nzm[jmax] = i+1;
	}
    }

  for (i = 0; i < ndsc; i++)
    {
      for (j = 0; j < izm; j++)
	{
	  if (cnzm[j] == i+1)
	    {
	      dsn = create_and_attach_one_ds(op, nzm[j], nzm[j], 0);
	      if (dsn == NULL) 		return win_printf("cannot create ds !");
	      grey = 224-(c2nzm[j]*192)/ndsc;
	      set_ds_line_color(dsn, makecol(grey,grey,grey));
	      set_ds_source(dsn,"Partial trajectory at Zmag = %g", zm[j]);
	      set_ds_dot_line(dsn);
	      //win_printf("Creating %d with %d points zmag = %g gey = %d",i,nzm[j],zm[j],grey);

	      for (k = 0, dsn->nx = 0; k < ds->nx && k < dsz->nx; k++)
		{
		  if (dsz->yd[k] == zm[j])
		    {
		      if (dsn->nx < dsn->mx)
			{
			  dsn->yd[dsn->nx] = ds->yd[k];
			  dsn->xd[dsn->nx] = ds->xd[k];
			  dsn->nx++;
			}
		    }
		}
	    }
	}
    }
  op->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);		
  return 0;
}

int separate_trajectories_by_zmag_cte(void)
{
  register int i, j, k;
  pltreg *pr;
  O_p *op;
  d_s *ds, *dsz, *dsn = NULL, *dsm, *dsmm;
  float *zm, zmmax, last_zmag, tmp;
  int *nzm, izm = 0, nizm, ndsz = -1, *cnzm, *c2nzm, max, jmax, *istart, *iend, last_zi;
  static int ndsc = 8, nmin = 16, dsmin = 256, no_create = 0;
  int xis, xie;
  char buf[512];

  if(updating_menu_state != 0)	return D_O_K;		
	
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf("cannot find data");

  dsz = find_source_specific_ds_in_op(op, "Zmag position in Bead tracking");
  if (dsz != NULL)
    {
      for (i = 0; op->dat[i] != dsz && i < op->n_dat; i++);
      ndsz = (i < op->n_dat) ? i : -1;
    }

  if (find_x_selected_bounds(ds, op->x_lo, op->x_hi, &xis, &xie))
    return win_printf("cannot find boundary in data set");


  sprintf(buf,"I am going to isolate signal stretches recorded at constant Zmag\n"
	  "sarting at point %d (%g%s) and ending at point %d (%g%s)\n"
	  "indicate the data set number corresponding to Zmag %%4d\n"
	  "the minimum number of consecutive points to keep a data set %%4d "
	  ,xis,op->dx*ds->xd[xis],op->x_unit,xie,op->dx*ds->xd[xie],op->x_unit);
	  

  i = win_scanf(buf,&ndsz,&nmin);
  if (i == CANCEL) return D_O_K;

  if (ndsz < 0 || ndsz >= op->n_dat)
    win_printf_OK("ndsz %d out of range !");
  dsz = op->dat[ndsz];

  for (i = xis, izm = -1, last_zi = 0, tmp = last_zmag = dsz->yd[0]; i < xie; i++)
    {
      if (dsz->yd[i] != last_zmag)
	{
	  last_zi = i; 
	  last_zmag = dsz->yd[i];
	}
      else if (i - last_zi < nmin)	continue;
      if (izm >= 0 && dsz->yd[i] == tmp) 	continue;
      else 	  
	{
	  izm++;
	  tmp = dsz->yd[i];
	}
    }
  nizm = izm + 1;

  zm = (float*)calloc(nizm,sizeof(float));
  nzm = (int*)calloc(nizm,sizeof(int));
  cnzm = (int*)calloc(nizm,sizeof(int));
  c2nzm = (int*)calloc(nizm,sizeof(int));
  istart = (int*)calloc(nizm,sizeof(int));
  iend = (int*)calloc(nizm,sizeof(int));

  if (zm == NULL || nzm == NULL || cnzm == NULL || c2nzm == NULL 
      || istart == NULL || iend == NULL)   return win_printf_OK("cannot alloc mem !");

  for (j = 0; j < nizm; j++)
    {
      zm[j] = -1;
      nzm[j] = 0;
      cnzm[j] = -1;
      c2nzm[j] = -1;
      istart[j] = -1;
      iend[j] = -1;
    }

  for (i = xis, izm = -1, last_zi = 0, last_zmag = dsz->yd[0]; i < xie; i++)
    {
      if (dsz->yd[i] != last_zmag)
	{
	  last_zi = i; 
	  last_zmag = dsz->yd[i];
	}
      else if (i - last_zi < nmin)	continue;
      if (izm >= 0 && dsz->yd[i] == zm[izm])
	{
	  nzm[izm]++;
	  iend[izm] = i;	  
	}
      else if (izm < nizm)
	{
	  izm++;
	  istart[izm] = i;
	  iend[izm] = i;	  
	  zm[izm] = dsz->yd[i];
	  nzm[izm] = 1;	  
	} 
    }
  izm++;

  sprintf(buf,"Between point %d (%g%s) and point %d (%g%s)\n"
	  "I have found {\\color{yellow}%d} values of zmag,"
	  "Among the zmag \nvalues corresponding"
	  "to the largest number of points\n"
	  "Specify the maximum number of coloured data set you want %%4d\n"
	  "and the mininum number of points in a data set %%6d\n"
	  "Create trajectory datasets %%R or just average values %%r\n",
	  xis,op->dx*dsz->xd[xis],op->x_unit,xie,op->dx*dsz->xd[xie],op->x_unit,izm);

  i = win_scanf(buf,&ndsc,&dsmin,&no_create);
  if (i == CANCEL) return D_O_K;

  for (i = 0; i < ndsc; i++)
    {
      for (j = 0, max = 0, jmax = -1; j < izm; j++)
	{
	  if (cnzm[j] < 0)
	    {
	      if (max < nzm[j])
		{
		  max = nzm[j];
		  jmax = j;
		}
	    }
	}
      if (jmax >= 0)	  cnzm[jmax] = i+1;
      //win_printf("%d selected jmax %d zmag %g with %d points",i,jmax,(jmax<0)?-1:zm[jmax],(jmax<0)?0:nzm[jmax]); 
    }

  for (i = 0; i < ndsc; i++)
    {
      for (j = 0, jmax = -1, zmmax = -FLT_MAX; j < izm; j++)
	{
	  if (cnzm[j] > 0 && c2nzm[j] < 0)
	    {
	      if (zmmax < zm[j])
		{
		  zmmax = zm[j];
		  jmax = j;
		}
	    }
	}
      if (jmax >= 0)	  c2nzm[jmax] = i+1;
    }

  dsm = build_data_set(ndsc, ndsc);
  if (dsm == NULL)       return win_printf("cannot create ds !");
  dsm->nx = ndsc;
  alloc_data_set_x_error(dsm);
  set_ds_line_color(dsm, Yellow);
  set_ds_source(dsm,"Mean of partial continuous trajectory");
  set_ds_dot_line(dsm);
  dsm->nx = 0;
  set_plot_symb(dsm,"\\pt5\\oc ");
  dsmm = build_data_set(ndsc, ndsc);
  if (dsmm == NULL) 	 return win_printf("cannot create ds !");
  dsmm->nx = ndsc;
  alloc_data_set_x_error(dsmm);
  set_ds_line_color(dsmm, Lightred);
  set_ds_source(dsmm,"Zmag of partial continuous trajectory");
  set_ds_dot_line(dsmm);
  dsmm->nx = 0;
  set_plot_symb(dsmm,"\\pt5\\oc ");

  for (i = 0; i < ndsc; i++)
    {
      for (j = 0; j < izm; j++)
	{
	  if (cnzm[j] == i+1 && nzm[j] >= dsmin)
	    {

	      if (no_create == 0)
		{
		  dsn = create_and_attach_one_ds(op, nzm[j], nzm[j], 0);
		  if (dsn == NULL) 		
		    return win_printf("cannot create ds !");
		  grey = 224-(c2nzm[j]*192)/ndsc;
		  set_ds_line_color(dsn, makecol(grey,grey,grey));
		  set_ds_source(dsn,"Partial continuous trajectory at Zmag = %g", zm[j]);
		  set_ds_dot_line(dsn);
		  dsn->nx = 0;
		  //win_printf("Creating %d with %d points zmag = %g gey = %d",i,nzm[j],zm[j],grey);
		}

	      for (k = istart[j]; k < iend[j]; k++)
		{
		  if (no_create == 0 && dsn->nx < dsn->mx)
		    {
		      dsn->yd[dsn->nx] = ds->yd[k];
		      dsn->xd[dsn->nx] = ds->xd[k];
		      dsn->nx++;
		    }
		  dsm->xd[dsm->nx] += ds->xd[k];
		  dsm->yd[dsm->nx] += ds->yd[k];

		}
	      if (iend[j] > istart[j])
		{
		  dsm->xd[dsm->nx] /= (iend[j] - istart[j]);
		  dsm->yd[dsm->nx] /= (iend[j] - istart[j]);
		}
	      dsm->xe[dsm->nx] = dsmm->xe[dsm->nx] = (ds->xd[iend[j]] - ds->xd[istart[j]])/2;
	      dsmm->yd[dsmm->nx] = zm[j];
	      dsmm->xd[dsmm->nx] = dsm->xd[dsm->nx];
	      dsm->nx = dsm->ny = dsm->nx + 1;
	      dsmm->nx = dsmm->ny = dsmm->nx + 1;
	    }
	}
    }
  add_data_to_one_plot(op, IS_DATA_SET, (void*)dsm);
  add_data_to_one_plot(op, IS_DATA_SET, (void*)dsmm);	
  op->cur_dat = op->n_dat - 1;

  free(zm);
  free(nzm);
  free(cnzm);
  free(c2nzm);
  free(istart);
  free(iend);

  op->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);		
  return 0;
}

# ifdef PB

ds *adjust_polyline_to_ds(d_s *dsi, int nptp, int iter, float x_start, float x_end, int *nfo, float *chi2)
{
  register int i, j;
  int imax, imin, nf, *ipoly;
  double a,b,c, a1, b1, c1;

  if (dsi == NULL || nptp <= 0) return NULL;


  //we find the extremety points indexes
  for(i = imin = imax = 0; i < dsi->nx; i++)
    {
      if (dsi->xd[i] < x_start) imin = i;
      if (dsi->xd[i] <= x_end) imax = i;      
      else break;
    }
  //wr check that we have at least one seg
  if (nptp > imax-imin) 
    {
      *nfo = 0;
      return NULL;
    }
	
  nf = 1 + (1+imax-imin)/nptp;
	
  if ((dsp = build_data_set (nf, nf, 0)) == NULL) return NULL;

  dsp->symb = strdup("\\pt5\\oc ");
  dsp->m = 1;				
			
	
  for (j = 0, ipoly = (int*)dsp->xd; j < nf; j++)
    {
      ipoly[j] = imin + ((imax - imin)*j)/(nf-1);
      dsp->yd[j] = dsi->yd[ipoly[j]];
    }
  *chi2 = (float)compute_chi2(dsi, dsp);
  for (i = 0; i < iter; i++)
    {
      find_best_y(dsi, dsp, 1, 0, &a, &b, &c);
      dsp->yd[0] = (a != 0) ? (b - c)/a : dsp->yd[0];
      for (j = 1; j < nf-1; j++)
	{
	  find_best_y(dsi, dsp, j-1, j, &a, &b, &c);
	  find_best_y_half(dsi, dsp, j+1, j, &a1, &b1, &c1);
	  dsp->yd[j] = ((a + a1) != 0) ? (b + b1 - c - c1)/(a + a1):dsp->yd[j];
	}
      find_best_y(dsi, dsp, nf-2, nf-1, &a, &b, &c);
      dsp->yd[nf-1] = (a != 0) ? (b - c)/a : dsp->yd[nf-1];	
    }
  ipoly = (int*)dsp->xd;

  for (j = 0, ipoly = (int*)dsp->xd; j < nf; j++)	
    dsp->xd[j] = dsi->xd[ipoly[j]];
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(dsp, dsi);
  *nfo = nf;
  return dsp;
}

# endif

int grab_simple_segment_at_cst_zmag(void)
{
  register int i, j, k, l, m;
  pltreg *pr;
  O_p *op, *opd, *opm;
  d_s *ds, *ds_zmag, *ds_seg, *dsn, *dsp, *dsi, *dsderi, *dsm;
  float zm[256], zmmax;
  int nzm[256], izm = 0, nds_zmag = -1, nds_seg = -1, n_dsi;
  int cnzm[256], c2nzm[256], max, jmax, imax, imin;
  static int iter = 10, nptp = 50;
  static int ndsc = 8;
  char buf[512];
  float ax, dx, ay, dy, tmp;
  int QuickSort_double(float *xd, float *yd, int l, int r);
  static int do_cor = 0;
  static float obj_cor = 0.878, cor = 1;



  if(updating_menu_state != 0)	
    {
      if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
	  active_menu->flags |=  D_DISABLED;
      else
	{
	  ds_seg = find_source_specific_ds_in_op(op, "Helicase simple velocity");
	  if (ds_seg == NULL)	      active_menu->flags |=  D_DISABLED;
	  else active_menu->flags &=  ~D_DISABLED;
	}
      return D_O_K;
    }
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("could not find data");
  ds_seg = find_source_specific_ds_in_op(op, "Helicase simple velocity");
  if (ds_seg != NULL)
    {
      for (i = 0; op->dat[i] != ds_seg && i < op->n_dat; i++);
      nds_seg = (i < op->n_dat) ? i : -1;
    }
  dsi = find_source_specific_ds_in_op(op, "Bead tracking at");
  if (dsi != NULL)
    {
      for (i = 0; op->dat[i] != dsi && i < op->n_dat; i++);
      n_dsi = (i < op->n_dat) ? i : -1;
    }
  ds_zmag = find_source_specific_ds_in_op(op, "Zmag position in Bead tracking");
  if (ds_zmag != NULL)
    {
      for (i = 0; op->dat[i] != ds_zmag && i < op->n_dat; i++);
      nds_zmag = (i < op->n_dat) ? i : -1;
    }
  i = win_scanf("indicate the data set number \ncorresponding to Zmag%d"
		"corresponding to bead signal%d"
		"the number points in small segments %6d\n"
		"the number of iteration %6d\n"
                "do you want to apply the obj. correction yes %R, no %r\n"
	      "correction factor %7f" ,&nds_zmag,&n_dsi,&nptp,&iter,&do_cor,&obj_cor);
  if (i == CANCEL) return D_O_K;
  if (nds_zmag < 0 || nds_zmag >= op->n_dat)
    win_printf_OK("nds_zmag %d out of range !");
  ds_zmag = op->dat[nds_zmag];
  if (n_dsi < 0 || n_dsi >= op->n_dat)
    win_printf_OK("n_dsi %d out of range !");
  dsi = op->dat[n_dsi];
  cor = (do_cor < 0) ? 1 : obj_cor; 

  QuickSort_double(ds_seg->xd, ds_seg->yd, 0, ds_seg->nx-1);

  for (j = 0; j < 256; j++)
    {
      zm[j] = 0;
      nzm[j] = 0;
      cnzm[j] = -1;
      c2nzm[j] = -1;
    }

  for (i = izm = k = 0; i < ds_zmag->nx && k < ds_seg->nx; )
    {
      //win_printf("Zmag point %d seg %d\nseg %g < %g < %g",
      //	 i,k,ds_seg->xd[k]), ds_zmag->xd[i], ds_seg->xd[k+1]);
      if ((ds_zmag->xd[i] >= ds_seg->xd[k]) && (ds_zmag->xd[i] <= ds_seg->xd[k+1]))
	{
	  for (j = 0; (j < izm) && (fabs(ds_zmag->yd[i] - zm[j]) > 0.001); j++);
	  if (j < izm)	  nzm[j]++;
	  else if (izm < 256)
	    {
	      zm[izm] = ds_zmag->yd[i];
	      nzm[izm] = 1;	  
	      //win_printf("new zmag %g k %d i %d ",zm[izm],k,i);
	      //win_printf("Zmag[%d] %g point %d seg %d\nseg %g < %g < %g",
	      //	 izm, zm[izm],i,k,op->dx * ds_seg->xd[k], 
	      //	 op->dx * ds_zmag->xd[i], op->dx * ds_seg->xd[k+1]);
	      izm++;
	    }
	  i++;
	}
      else if (ds_zmag->xd[i] > ds_seg->xd[k+1]) 
	{
	  k += 2;
	  //win_printf("new seg k %d i %d ",k,i);
	}
      else i++;
    }
  if (ndsc > izm) ndsc = izm;
  sprintf(buf,"I have found %d values of zmag among the zmag values\n"
	      "corresponding to the largest number of points.\n"
	      "Specify how many Zmag values you want to analyze %%d ",izm);

  i = win_scanf(buf,&ndsc);
  if (i == CANCEL) return D_O_K;


  //ordering versus nb of points
  for (i = 0; i < ndsc; i++)
    {
      for (j = 0, max = 0, jmax = -1; j < izm; j++)
	{
	  if (cnzm[j] < 0)
	    {
	      if (max < nzm[j])
		{
		  max = nzm[j];
		  jmax = j;
		}
	    }
	}
      if (jmax >= 0)
	{
	  cnzm[jmax] = i+1;
	  //win_printf("%d selected zmag %g with %d points %d zmag",i,zm[jmax],nzm[jmax],izm); 
	}
    }


  //ordering versus Zmag
  for (i = 0; i < ndsc; i++)
    {
      for (j = 0, jmax = -1, zmmax = -FLT_MAX; j < izm; j++)
	{
	  if (cnzm[j] > 0 && c2nzm[j] < 0)
	    {
	      if (zmmax < zm[j])
		{
		  zmmax = zm[j];
		  jmax = j;
		}
	    }
	}
      if (jmax >= 0)
	{
	  c2nzm[jmax] = i+1;
	  // win_printf("2 %d selected zmag %g with %d points",i,zm[jmax],c2nzm[jmax]); 
	}
    }

  opm = create_and_attach_one_plot(pr, ndsc, ndsc, 0);
  if (opm == NULL)		return win_printf("Cannot allocate plot");
  dsm = opm->dat[0];

  set_plot_title(opm, "Helicase velocity vs Zmag");
  set_plot_y_title(opm, "Velocity");
  set_plot_x_title(opm, "Zmag");
  set_plot_symb(dsm,"\\pt5\\oc ");
  set_dot_line(dsm);				


  for (i = 0; i < ndsc; i++)
    {
      //win_printf("treating zmag %d",i);
      for (j = 0; j < izm; j++)
	{
	  if (cnzm[j] == i+1)
	    {
	      dsn = create_and_attach_one_ds(op, nzm[j], nzm[j], 0);
	      if (dsn == NULL) 		return win_printf("cannot create ds !");
	      grey = 224-(c2nzm[j]*192)/ndsc;
	      set_ds_line_color(dsn, makecol(grey,grey,grey));
	      set_ds_source(dsn,"Partial trajectory at Zmag = %g", zm[j]);
	      set_ds_dot_line(dsn);
	      //win_printf("Creating %d with %d points zmag = %g gey = %d",i,nzm[j],zm[j],grey);

	      for (k = l = 0, dsn->nx = 0; k < ds->nx && k < ds_zmag->nx; )
		{
		  if ((ds_zmag->xd[k] >= ds_seg->xd[l]) && (ds_zmag->xd[k] <= ds_seg->xd[l+1]))
		    {
		      if (ds_zmag->yd[k] == zm[j])
			{
			  if (dsn->nx < dsn->mx)
			    {
			      dsn->yd[dsn->nx] = ds->yd[k];
			      dsn->xd[dsn->nx] = ds->xd[k];
			      dsn->nx++;
			    }
			}
		      k++;
		    }
		  else if (ds_zmag->xd[k] > ds_seg->xd[l+1]) l += 2;
		  else k++;
		}

	      opd = create_and_attach_one_plot(pr, 32, 32, 0);
	      if (opd == NULL)		return win_printf("Cannot allocate plot");
	      dsderi = opd->dat[0];
	      if (dsderi == NULL)		return win_printf("cannot create plot !");
	      dsderi->source = Mystrdupre(dsderi->source,"Helicase velocity");
	      opd->filename = Transfer_filename(op->filename);
	      uns_op_2_op_by_type(op, IS_X_UNIT_SET, opd, IS_X_UNIT_SET);
	      get_afine_param_from_op(op, IS_X_UNIT_SET, &ax, &dx);
	      get_afine_param_from_op(op, IS_Y_UNIT_SET, &ay, &dy);
	      create_attach_select_y_un_to_op(opd, 0,0,1,0,0, "nm/s");
	      set_plot_title(opd, "\\stack{{velocity at Zmag %g}"
			     "{avg. over %d cor %g}}",zm[j],nptp,cor);
	      set_plot_y_title(opd, "Velocity");
	      set_plot_x_title(opd, "Real time");
	      set_plot_symb(dsderi,"\\pt5\\oc ");
	      set_dot_line(dsderi);				
	      dsderi->nx = dsderi->ny = 0; 

	      for (l = 0; l < ds_seg->nx; l += 2)
		{
		  recover_points_index_in_ds(ds_seg->xd[l], ds_seg->xd[l+1], dsi, &imin, &imax);
		  if ((ds_zmag->yd[imin] == zm[j]) && (imax-imin > nptp))
		    {
		      //win_printf("Plot %d imin %d nx %d nptp %d",j,imin,imax-imin,nptp);

		      //dsp = add_polyline_ds(op, dsi, imin, imax, nptp, iter);
		      dsp = adjust_polyline_ds( dsi, imin, imax, nptp, iter);
		      if (dsp != NULL)
			set_ds_treatement(dsp,"polygon fit of %d pts and %d iterations at Zmag %g"
					  ,imax-imin,iter,zm[j]);

		      for (m = 0; m < dsp->nx-1; m++)
			{
			  tmp = dsp->xd[m+1] - dsp->xd[m];
			  tmp = (tmp > 0) ? (dsp->yd[m+1] - dsp->yd[m])/tmp : (dsp->yd[m+1] - dsp->yd[m]);
			  tmp *= cor*1e9*dy/dx;
			  add_new_point_to_op_and_ds(opd, dsderi, (dsp->xd[m] + dsp->xd[m+1])/2, tmp);
			  dsm->yd[i] += tmp;
			  dsm->xd[i] += 1;
			}

			      
		      free_data_set(dsp);
		    }
		}
	      if (dsm->xd[i] > 0) dsm->yd[i] /= dsm->xd[i];
	      dsm->xd[i] = zm[j];	      
	    }
	}
    }
  op->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);		
  return 0;
}

int grab_single_segment_at_cst_zmag(void)
{
  register int i, k;
  pltreg *pr;
  O_p *op;
  d_s *ds, *ds_zmag, *ds_seg, *dsn, *dsi, *dsm, *dsmm;
  int  izm = 0, nds_zmag = -1, nds_seg = -1, n_dsi;
  int  imax, imin;
  static int ndsc = 8;
  char buf[512];
  int QuickSort_double(float *xd, float *yd, int l, int r);
  static int do_cor = 0;
  static float obj_cor = 0.878, cor = 1;



  if(updating_menu_state != 0)	
    {
      if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
	  active_menu->flags |=  D_DISABLED;
      else
	{
	  ds_seg = find_source_specific_ds_in_op(op, "Helicase simple velocity");
	  if (ds_seg == NULL)	      active_menu->flags |=  D_DISABLED;
	  else active_menu->flags &=  ~D_DISABLED;
	}
      return D_O_K;
    }
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("could not find data");
  ds_seg = find_source_specific_ds_in_op(op, "Helicase simple velocity");
  if (ds_seg != NULL)
    {
      for (i = 0; op->dat[i] != ds_seg && i < op->n_dat; i++);
      nds_seg = (i < op->n_dat) ? i : -1;
    }
  dsi = find_source_specific_ds_in_op(op, "Bead tracking at");
  if (dsi != NULL)
    {
      for (i = 0; op->dat[i] != dsi && i < op->n_dat; i++);
      n_dsi = (i < op->n_dat) ? i : -1;
    }
  ds_zmag = find_source_specific_ds_in_op(op, "Zmag position in Bead tracking");
  if (ds_zmag != NULL)
    {
      for (i = 0; op->dat[i] != ds_zmag && i < op->n_dat; i++);
      nds_zmag = (i < op->n_dat) ? i : -1;
    }
  i = win_scanf("indicate the data set number \ncorresponding to Zmag%d"
		"corresponding to bead signal%d"
                "do you want to apply the obj. correction yes %R, no %r\n"
	      "correction factor %7f" ,&nds_zmag,&n_dsi,&do_cor,&obj_cor);
  if (i == CANCEL) return D_O_K;
  if (nds_zmag < 0 || nds_zmag >= op->n_dat)
    win_printf_OK("nds_zmag %d out of range !");
  ds_zmag = op->dat[nds_zmag];
  if (n_dsi < 0 || n_dsi >= op->n_dat)
    win_printf_OK("n_dsi %d out of range !");
  dsi = op->dat[n_dsi];
  cor = (do_cor < 0) ? 1 : obj_cor; 

  QuickSort_double(ds_seg->xd, ds_seg->yd, 0, ds_seg->nx-1);

  izm = ds_seg->nx/2;
  if (ndsc > izm) ndsc = izm;
  sprintf(buf,"I have found %d values of zmag among the zmag values\n"
	      "corresponding to the largest number of points.\n"
	      "Specify how many Zmag values you want to analyze %%d ",izm);

  i = win_scanf(buf,&ndsc);
  if (i == CANCEL) return D_O_K;


  dsm = build_data_set(ndsc, ndsc);
  if (dsm == NULL)       return win_printf("cannot create ds !");
  dsm->nx = ndsc;
  alloc_data_set_x_error(dsm);
  set_ds_line_color(dsm, Yellow);
  set_ds_source(dsm,"Mean of partial continuous trajectory");
  set_ds_dot_line(dsm);
  dsm->nx = 0;
  set_plot_symb(dsm,"\\pt5\\oc ");
  dsmm = build_data_set(ndsc, ndsc);
  if (dsmm == NULL) 	 return win_printf("cannot create ds !");
  dsmm->nx = ndsc;
  alloc_data_set_x_error(dsmm);
  set_ds_line_color(dsmm, Lightred);
  set_ds_source(dsmm,"Zmag of partial continuous trajectory");
  set_ds_dot_line(dsmm);
  dsmm->nx = 0;
  set_plot_symb(dsmm,"\\pt5\\oc ");


  for (i = 0; i < ndsc; i += 1)
    {
      recover_points_index_in_ds(ds_seg->xd[2*i], ds_seg->xd[2*i+1], dsi, &imin, &imax);
      //win_printf("treating zmag %d",i);
      if (imax - imin > 2)
	{
	  dsn = create_and_attach_one_ds(op, imax - imin, imax - imin, 0);
	  if (dsn == NULL) 		return win_printf("cannot create ds !");
	  grey = 224-(i*192)/ndsc;
	  set_ds_line_color(dsn, makecol(grey,grey,grey));
	  set_ds_source(dsn,"Partial trajectory at Zmag = %g", ds_zmag->yd[imin]);
	  set_ds_dot_line(dsn);
      //win_printf("Creating %d with %d points zmag = %g gey = %d",i,nzm[j],zm[j],grey);
	  for (k = 0, dsn->nx = 0; k < dsn->mx; k++)
	    {
	      dsn->yd[dsn->nx] = ds->yd[imin+k];
	      dsn->xd[dsn->nx] = ds->xd[imin+k];
	      dsn->nx++;
	      dsm->xd[dsm->nx] += ds->xd[imin+k];
	      dsm->yd[dsm->nx] += ds->yd[imin+k];
	    }
	  dsm->xd[dsm->nx] /= dsn->nx;
	  dsm->yd[dsm->nx] /= dsn->nx;
	  dsm->xe[dsm->nx] = dsmm->xe[dsm->nx] = (ds->xd[imin] - ds->xd[imax])/2;
	  dsmm->yd[dsmm->nx] = ds_zmag->yd[imin];
	  dsmm->xd[dsmm->nx] = dsm->xd[dsm->nx];
	  dsm->nx = dsm->ny = dsm->nx + 1;
	  dsmm->nx = dsmm->ny = dsmm->nx + 1;
	}
    }
  add_data_to_one_plot(op, IS_DATA_SET, (void*)dsm);
  add_data_to_one_plot(op, IS_DATA_SET, (void*)dsmm);	
  op->cur_dat = op->n_dat - 1;
  op->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);		
  return 0;
}



int grab_simple_segment_duration(void)
{
  register int i;
  pltreg *pr;
  O_p *op, *opd;
  d_s *ds, *dsd, *ds_zmag, *ds_seg, *dsi;
  int  izm = 0, nds_zmag = -1, nds_seg = -1, n_dsi;

  if(updating_menu_state != 0)	
    {
      if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
	  active_menu->flags |=  D_DISABLED;
      else
	{
	  ds_seg = find_source_specific_ds_in_op(op, "Helicase simple velocity");
	  if (ds_seg == NULL)	      active_menu->flags |=  D_DISABLED;
	  else active_menu->flags &=  ~D_DISABLED;
	}
      return D_O_K;
    }
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("could not find data");
  ds_seg = find_source_specific_ds_in_op(op, "Helicase simple velocity");
  if (ds_seg != NULL)
    {
      for (i = 0; op->dat[i] != ds_seg && i < op->n_dat; i++);
      nds_seg = (i < op->n_dat) ? i : -1;
    }
  dsi = find_source_specific_ds_in_op(op, "Bead tracking at");
  if (dsi != NULL)
    {
      for (i = 0; op->dat[i] != dsi && i < op->n_dat; i++);
      n_dsi = (i < op->n_dat) ? i : -1;
    }
  ds_zmag = find_source_specific_ds_in_op(op, "Zmag position in Bead tracking");
  if (ds_zmag != NULL)
    {
      for (i = 0; op->dat[i] != ds_zmag && i < op->n_dat; i++);
      nds_zmag = (i < op->n_dat) ? i : -1;
    }
  i = win_scanf("indicate the data set number \ncorresponding to Zmag%d"
		"corresponding to bead signal%d"
	       ,&nds_zmag,&n_dsi);
  if (i == CANCEL) return D_O_K;
  if (nds_zmag < 0 || nds_zmag >= op->n_dat)
    win_printf_OK("nds_zmag %d out of range !");
  ds_zmag = op->dat[nds_zmag];
  if (n_dsi < 0 || n_dsi >= op->n_dat)
    win_printf_OK("n_dsi %d out of range !");
  dsi = op->dat[n_dsi];
  izm = ds_seg->nx/2;


  opd = create_and_attach_one_plot(pr, izm, izm, 0);
  if (opd == NULL)		return win_printf("Cannot allocate plot");
  dsd = opd->dat[0];
  if (dsd == NULL)		return win_printf("cannot create plot !");
  dsd->source = Mystrdupre(dsd->source,"Helicase loading time");
  for (i = 0; i < ds_seg->nx /2; i++) 
    {
      dsd->xd[i] = i;
      dsd->yd[i] = ds_seg->xd[2*i+1] - ds_seg->xd[2*i];
    }
  opd->filename = Transfer_filename(op->filename);
  uns_op_2_op_by_type(op, IS_X_UNIT_SET, opd, IS_Y_UNIT_SET);
  set_plot_title(opd, "Helicasz loading time");
  set_plot_y_title(opd, "Loading time");
  set_plot_x_title(opd, "segment index");
  set_plot_symb(dsd,"\\pt5\\oc ");
  set_dot_line(dsd);				

  opd->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);		
  return 0;
}




int		do_simple_segment(pltreg *pr, O_p *op, d_s *dsi, float x0, 
					  float y0, float x1, float y1)
{
  register int i;
  d_s  *dsd = NULL, *ds_zmag = NULL;
  int  imin, imax, dsn, op_n;
  static int  n_ds_zmag = -1, choice = 1;
  double  min, max, tmp, *ap;
  char ch[1024];
  float max_y, min_y, max_diff_zmag;
  int fit_ds_to_xn_polynome(d_s *ds, int n, float x_lo, float x_hi, 
			    float y_lo, float y_hi, double **a);


  /* "This routine fits a polygone to a data set using\n least quare fit
     data should be well ordered in x"); */
  min = (x0 < x1) ? x0 : x1;
  max = (x0 < x1) ? x1 : x0;
  dsn = op->cur_dat;
  op_n = pr->cur_op;
  for (imin = dsi->nx -1, imax = i = 0;i < dsi->nx; i++)
    {
      if (dsi->xd[i] <= max && i > imax) 	  imax = i;			
      if (dsi->xd[i] >= min && i < imin) 	  imin = i;			
    }
  if (imax <= imin)	
    return win_printf("imin = %d imax = %d impossible",imin,imax);
  for (i = 1, min_y = max_y = dsi->yd[0]; i < dsi->nx; i++)
    {
      max_y = (dsi->yd[i] > max_y) ? dsi->yd[i] : max_y;
      min_y = (dsi->yd[i] < min_y) ? dsi->yd[i] : min_y;
    }
  dsd = find_source_specific_ds_in_pr(pr, "Helicase simple velocity");
  if (dsd == NULL)
    {
      n_ds_zmag = -1;
      ds_zmag = find_source_specific_ds_in_op(op, "Zmag position in Bead tracking");
      if (ds_zmag != NULL)
	{
	  for (i = 0; op->dat[i] != ds_zmag && i < op->n_dat; i++);
	  n_ds_zmag = (i < op->n_dat) ? i : -1;
	}
      sprintf(ch,
	      "I am going to adjust a simple segment defined\n"
	      "starting at point %d"
	      "at x = %g\n and extending over %d points at x %g\n"
	      "if there is a zmag data set give its number (-1 otherwise) %%d"
	      ,imin,op->ax+op->dx*dsi->xd[imin%dsi->nx],
	      imax-imin,op->ax+op->dx*dsi->xd[imax%dsi->nx]);
      i = win_scanf(ch,&n_ds_zmag);
      if (i == CANCEL)	return OFF;
      if (n_ds_zmag >= 0)
	{
	  ds_zmag = op->dat[n_ds_zmag%op->n_dat];
	  
	  for (i = imin, tmp = ds_zmag->yd[imin], max_diff_zmag = 0; i <= imax; i++)
	    max_diff_zmag = (fabs(tmp - ds_zmag->yd[i]) > max_diff_zmag) ? 
	      fabs(tmp - ds_zmag->yd[i]) : max_diff_zmag;
	  
	  
	  if (max_diff_zmag > 0) 
	    {
	      sprintf(ch,"Zmag is not constant over the segment you selected\n"
		      "The maximug difference in zmag is %g\n"
		      "Do you want to keep this segment yes %%R np %%r\n",
		      max_diff_zmag);
	      
	      win_scanf(ch,&choice);
	      if (choice == 1) return 0;
	    }


	  //for (i = imin, tmp = ds_zmag->yd[imin]; i <= imax; i++)
	  //if (tmp != ds_zmag->yd[i])	break;
	  //if (i != imax+1) 
	  // return win_printf("Zmag is not constant over your segment");

	  //coco



	}	

      dsd = create_and_attach_one_ds(op, 32, 32, 0);
      if (dsd == NULL)		return win_printf("cannot create plot !");
      dsd->source = Mystrdupre(dsd->source,"Helicase simple velocity");
      dsd->nx = dsd->ny = 0;
      set_dash_line(dsd);				
      dsd->symb = strdup("\\pt4\\oc ");
      if (dsi->color == Lightred) set_ds_line_color(dsd, Yellow);
      else set_ds_line_color(dsd, Lightred);
      //win_printf("ds created");
    }
  if (n_ds_zmag >= 0)
    {
      ds_zmag = op->dat[n_ds_zmag%op->n_dat];

      for (i = imin, tmp = ds_zmag->yd[imin], max_diff_zmag = 0; i <= imax; i++)
	max_diff_zmag = (fabs(tmp - ds_zmag->yd[i]) > max_diff_zmag) ? fabs(tmp - ds_zmag->yd[i]) : max_diff_zmag;
      
      
      if (max_diff_zmag > 0) 
	{
	    sprintf(ch,"Zmag is not constant over the segment you selected\n"
		    "The maximug difference in zmag is %g\n"
		    "Do you want to keep this segment yes %%R np %%r\n",
		    max_diff_zmag);
	  
	  win_scanf(ch,&choice);
	  if (choice == 1) return 0;
	}

      //      for (i = imin, tmp = ds_zmag->yd[imin]; i <= imax; i++)
      //if (tmp != ds_zmag->yd[i])	break;
      //if (i != imax+1) 
      //return win_printf("Zmag is not constant over your segment");
    }

  //win_printf("ready to fit min %g -> %g %g -> %g",min, max, min_y - 1, max_y + 1);

  fit_ds_to_xn_polynome(dsi, 2, min, max, min_y - 1, max_y + 1, &ap);

  add_new_point_to_op_and_ds(op, dsd, dsi->xd[imin], ap[0] + ap[1] * dsi->xd[imin]);
  add_new_point_to_op_and_ds(op, dsd, dsi->xd[imax-1], ap[0] + ap[1] * dsi->xd[imax-1]);

  op->cur_dat = dsn;
  free(ap);
  refresh_plot(pr, op_n);		
  return 0;
}


int		do_simple_poly_on_segment(pltreg *pr, O_p *op, d_s *dsi, float x0, 
					  float y0, float x1, float y1)
{
  register int i, j;
  O_p *opd, *opp, *opt, *opmv, *opnoc;
  d_s *ds, *dsd = NULL, *dsp = NULL, *dst = NULL, *ds_zmag = NULL;
  d_s *dsno = NULL, *dsmv = NULL, *dsnoc;
  int *ipoly, imin, imax, dsn, op_n;
  static int nf = 10, iter = 10, nptp = 50, n_ds_zmag = -1, do_cor = -1;
  static float zmag = -1, obj_cor = 0.878, cor = 1;
  double a,b,c, a1, b1, c1, min, max, tmp, *ap;
  char ch[1024];
  float max_y, min_y;
  int fit_ds_to_xn_polynome(d_s *ds, int n, float x_lo, float x_hi, 
			    float y_lo, float y_hi, double **a);


  /* "This routine fits a polygone to a data set using\n least quare fit
     data should be well ordered in x"); */
  min = (x0 < x1) ? x0 : x1;
  max = (x0 < x1) ? x1 : x0;
  dsn = op->cur_dat;
  op_n = pr->cur_op;
  for (imin = dsi->nx -1, imax = i = 0;i < dsi->nx; i++)
    {
      if (dsi->xd[i] <= max && i > imax) 
	{
	  imax = i;			
	}
      if (dsi->xd[i] >= min && i < imin) 
	{
	  imin = i;			
	}		
    }
  if (imax <= imin)	
    return win_printf("imin = %d imax = %d impossible",imin,imax);
  for (i = 1, min_y = max_y = dsi->yd[0]; i < dsi->nx; i++)
    {
      max_y = (dsi->yd[i] > max_y) ? dsi->yd[i] : max_y;
      min_y = (dsi->yd[i] < min_y) ? dsi->yd[i] : min_y;
    }
  dsd = find_source_specific_ds_in_pr(pr, "Helicase velocity");
  if (dsd == NULL)
    {
      n_ds_zmag = -1;
      zmag = -1;
      /*		win_printf("imin %d imax %d",imin,imax);*/
      sprintf(ch,
	      "I am going to adjust a polygone to the segment defined\n"
	      "starting at point %d"
	      "at x = %g\n and extending over %d points at x %g\n"
	      "Enter the number of points in one segment\n"
	      "of polygone (averaging size) %%d"
	      "and the number of iteration %%d"
	      "if there is a zmag data set give its number (-1 otherwise) %%d"
	      "do you want to apply the obj. correction (yes ->0, no -> -1)%%d"
	      "correction factor %%f",imin,op->ax+op->dx*dsi->xd[imin%dsi->nx],
	      imax-imin,op->ax+op->dx*dsi->xd[imax%dsi->nx]);
      i = win_scanf(ch,&nptp,&iter,&n_ds_zmag,&do_cor,&obj_cor);
      if (i == CANCEL)	return OFF;
      /*		win_printf("do_cor %d cor %g dsmag %d imin %d",do_cor,obj_cor,n_ds_zmag%op->n_dat,imin); */
      if (n_ds_zmag >= 0)
	{
	  /*			win_printf("bef zmag imin %d",imin); */
	  ds_zmag = op->dat[n_ds_zmag%op->n_dat];
	  /*			win_printf("zmag n %d zmag 0 %g at %d",n_ds_zmag%op->n_dat,ds_zmag->yd[imin%ds_zmag->nx],imin);			*/

	  for (i = imin, tmp = ds_zmag->yd[imin]; i <= imax; i++)
	    if (tmp != ds_zmag->yd[i])	break;
	  if (i != imax+1) 
	    return win_printf("Zmag is not constant over your segment");
	  if (zmag >= 0 && zmag != tmp)
	    return win_printf("Zmag is not equal to %g over your segment\n"
			      "as it should but equals %g",zmag,tmp);
	  if (zmag < 0)	zmag = tmp;
	}	
      cor = (do_cor < 0) ? 1 : obj_cor; 
      opd = create_and_attach_one_plot(pr, dsi->nx, dsi->nx, 0);
      if (opd == NULL)		return win_printf("Cannot allocate plot");
      dsd = opd->dat[0];
      if (dsd == NULL)		return win_printf("cannot create plot !");
      dsd->source = Mystrdupre(dsd->source,"Helicase velocity");
      for (i = 0; i < dsi->nx; i++) dsd->xd[i] = dsi->xd[i];
      opd->filename = Transfer_filename(op->filename);
      uns_op_2_op_by_type(op, IS_X_UNIT_SET, opd, IS_X_UNIT_SET);
      create_attach_select_y_un_to_op(opd, 0,0,op->dy/op->dx,0,0, "\\mu{}m/s");
      set_plot_title(opd, (zmag <0) ?"velocity" :"\\stack{{velocity at Zmag %g}"
		     "{avg. over %d cor %g}}",zmag,nptp,cor);
      set_plot_y_title(opd, "Velocity");
      set_plot_symb(dsd,"\\pt5\\oc ");
      set_dot_line(dsd);				
    }
  dsp = find_source_specific_ds_in_pr(pr, "Helicase processivity");
  if (dsp == NULL)
    {
      opp = create_and_attach_one_plot(pr, dsi->nx, dsi->nx, 0);
      if (opp == NULL)		return win_printf("Cannot allocate plot");
      dsp = opp->dat[0];
      if (dsp == NULL)		return win_printf("cannot create plot !");
      dsp->source = Mystrdupre(dsp->source,"Helicase processivity");
      for (i = 0; i < dsi->nx; i++) dsp->xd[i] = dsi->xd[i];
      opp->filename = Transfer_filename(op->filename);
      uns_op_2_op(opp, op);	
      set_plot_title(opp, (zmag <0) ? "Processivity" :
		     "Processivity at Zmag %g cor %g",zmag,cor);
      set_plot_symb(dsp,"\\pt5\\oc ");
      set_dot_line(dsp);		
    }
  dst = find_source_specific_ds_in_pr(pr, "Helicase action time");
  if (dst == NULL)
    {
      opt = create_and_attach_one_plot(pr, dsi->nx, dsi->nx, 0);
      if (opt == NULL)		return win_printf("Cannot allocate plot");
      dst = opt->dat[0];
      if (dst == NULL)		return win_printf("cannot create plot !");
      dst->source = Mystrdupre(dst->source,"Helicase action time");
      for (i = 0; i < dsi->nx; i++) dst->xd[i] = dsi->xd[i];
      opt->filename = Transfer_filename(op->filename);
      uns_op_2_op_by_type(op, IS_X_UNIT_SET, opt, IS_X_UNIT_SET);
      uns_op_2_op_by_type(op, IS_X_UNIT_SET, opt, IS_Y_UNIT_SET);
      set_plot_title(opt, (zmag <0) ? "Helicase action time" :
		     "Helicase action time at Zmag %g",zmag);			
      set_plot_symb(dst,"\\pt5\\oc ");
      set_dot_line(dst);
    }
  dsmv = find_source_specific_ds_in_pr(pr, "Helicase mean velocity");
  dsno = find_source_specific_ds_in_pr(pr, "Helicase bursts noise");	
  if (dsmv == NULL || dsno == NULL)
    {
      opmv = create_and_attach_one_plot(pr, dsi->nx, dsi->nx, 0);
      if (opmv == NULL)		return win_printf("Cannot allocate plot");
      dsmv = opmv->dat[0];
      if (dsmv == NULL)		return win_printf("cannot create plot !");
      dsmv->source = Mystrdupre(dsmv->source,"Helicase mean velocity");
      dsno = create_and_attach_one_ds(opmv, dsi->nx, dsi->nx, 0);
      if (dsno == NULL) 		return win_printf("cannot create ds !");
      dsno->source = Mystrdupre(dsno->source,"Helicase bursts noise");
      opmv->filename = Transfer_filename(op->filename);
      uns_op_2_op_by_type(op, IS_X_UNIT_SET, opmv, IS_X_UNIT_SET);
      uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opmv, IS_Y_UNIT_SET);
      set_plot_title(opmv, (zmag <0) ? "Helicase mean velocity" :
		     "Helicase mean velocity at Zmag %g",zmag);			
      set_dot_line(dsno);
      set_dot_line(dsmv);
    }	
  i =	find_op_nb_of_source_specific_ds_in_pr( pr, "Helicase noise bursts");
  if (i < 0)
    {
      opnoc = create_and_attach_one_plot(pr, imax-imin+1, imax-imin+1, 0);
      if (opnoc == NULL)		return win_printf("Cannot allocate plot");
      dsnoc = opnoc->dat[0];
      if (dsnoc == NULL)		return win_printf("cannot create plot !");
      opnoc->filename = Transfer_filename(op->filename);
      uns_op_2_op(opnoc, op);	
      set_plot_title(opnoc, (zmag <0) ? "Noise bursts" :
		     "Noise bursts at Zmag %g cor %g",zmag,cor);
      set_dot_line(dsnoc);		
    }
  else
    {
      dsnoc = create_and_attach_one_ds(pr->o_p[i],imax-imin+1, imax-imin+1, 0);
      if (dsnoc == NULL) 		return win_printf("cannot create ds !");
      set_dot_line(dsnoc);			
    }	
  if (n_ds_zmag >= 0)
    {
      ds_zmag = op->dat[n_ds_zmag%op->n_dat];
      for (i = imin, tmp = ds_zmag->yd[imin]; i <= imax; i++)
	if (tmp != ds_zmag->yd[i])	break;
      if (i != imax+1) 
	return win_printf("Zmag is not constant over your segment");
      if (zmag >= 0 && zmag != tmp)
	return win_printf("Zmag is not equal to %g over your segment\n"
			  "as it should but equals %g",zmag,tmp);
      if (zmag < 0)	zmag = tmp;
    }


  cor = (do_cor < 0) ? 1 : obj_cor; 
  /*	win_printf("2 do_cor %d cor %g",do_cor,cor);*/
			
  if (nptp > imax-imin) return win_printf("not enought points selected \n"
					  " at = %d nb. of selected points %d \n your ask too much points in your \n"
					  "polygone segment %d",imin,imax-imin,nptp);
	
  nf = 1 + (1+imax-imin)/nptp;
	
  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf("cannot create plot !");

  ds->symb = strdup("\\pt5\\oc ");
  ds->m = 1;				
			
	
  for (j = 0, ipoly = (int*)ds->xd; j < nf; j++)
    {
      ipoly[j] = imin + ((imax - imin)*j)/(nf-1);
      ds->yd[j] = dsi->yd[ipoly[j]];
    }
  display_title_message("\\chi^2 = %g",(float)compute_chi2(dsi, ds));
  for (i = 0; i < iter; i++)
    {
      find_best_y(dsi, ds, 1, 0, &a, &b, &c);
      ds->yd[0] = (a != 0) ? (b - c)/a : ds->yd[0];
      for (j = 1; j < nf-1; j++)
	{
	  find_best_y(dsi, ds, j-1, j, &a, &b, &c);
	  find_best_y_half(dsi, ds, j+1, j, &a1, &b1, &c1);
	  ds->yd[j] = ((a + a1) != 0) ? (b + b1 - c - c1)/(a + a1):ds->yd[j];
	}
      find_best_y(dsi, ds, nf-2, nf-1, &a, &b, &c);
      ds->yd[nf-1] = (a != 0) ? (b - c)/a : ds->yd[nf-1];	
    }
  ipoly = (int*)ds->xd;
  dsp->yd[(ipoly[nf-1]+ipoly[0])/2] = cor*(ds->yd[nf-1] - ds->yd[0]);
  dst->yd[(ipoly[nf-1]+ipoly[0])/2] = dsi->xd[ipoly[nf-1]] - dsi->xd[ipoly[0]];

  for (j = 1, ipoly = (int*)ds->xd; j < nf; j++)	
    {
      tmp = dsi->xd[ipoly[j]] - dsi->xd[ipoly[j-1]];
      if (tmp != 0)	tmp = cor*(ds->yd[j] - ds->yd[j-1])/tmp; 
      dsd->yd[((ipoly[j]+ipoly[j-1])/2)%dsd->nx] = (float)tmp;
      /*		win_printf("j = %d dy %g dx %g \n tmp %g index %d",j,ds->yd[j] - ds->yd[j-1],*/
    }
	
  for (j = 0, ipoly = (int*)ds->xd; j < nf; j++)	
    ds->xd[j] = dsi->xd[ipoly[j]];
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  ds->treatement = my_sprintf(ds->treatement,
			      "polygon fit of %d pts and %d iterations",nf,iter);


  fit_ds_to_xn_polynome(dsi, 2, min, max, min_y - 1, max_y + 1, &ap);

  for (j = imin; j < imax; j++)	
    {
      dsno->xd[j] = dsmv->xd[j] = dsi->xd[j];
      dsnoc->xd[j-imin] = dsi->xd[j] - dsi->xd[imin];
      dsmv->yd[j] = ap[0] + ap[1] * dsi->xd[j];
      dsnoc->yd[j-imin] = dsno->yd[j] = dsi->yd[j] - dsmv->yd[j]; 
    }
  set_formated_string(&dsnoc->source, "Helicase noise bursts %d pts v = %f "
		      "at %f",imax-imin+1,ap[1] * op->dy /op->dx, op->dx*dsi->xd[imin]);
  /* refisplay the entire plot */
  op->cur_dat = dsn;
  free(ap);
  refresh_plot(pr, op_n);		
  return 0;
}

int		do_burst_segment(pltreg *pr, O_p *op, d_s *dsd, float x0, float y0, 
				 float x1, float y1, int mode)
{
  register int i, j;
  d_s *dsi = NULL, *ds_zmag = NULL;
  int imin, imax, dsn, op_n;
  static int n_ds_zmag = -1, do_cor = 0, first = 1, dead_frac = 4;
  static float zmag = -1, obj_cor = 0.878, cor = 1, atp = 500;
  double min, max, tmp, *ap = NULL;
  float max_y, min_y;
  char ch[1024], *cs, bead[512];
  static char seg_type[64], heli[64];
  time_t time;	

  /* "This routine fits a segment to a data set using\n least quare fit
     data should be well ordered in x"); */
  if (first == 1)
    {
      sprintf(seg_type,"N");
      sprintf(heli,"UvrD");
      first = 0;
    }
  min = (x0 < x1) ? x0 : x1;
  max = (x0 < x1) ? x1 : x0;
  dsn = op->cur_dat;
  op_n = pr->cur_op;
		
  /* we switch unit to nm otherwise we create the unit set */
  if (op->yu != NULL)
    {
      for (i=0 ; i< op->n_yu ; i++)
	{
	  if (op->yu[i] != NULL && op->yu[i]->type == IS_METER)
	    break;
	}
      if (i != op->n_yu && op->yu[i]->decade != IS_NANO)
	{
	  if (op->yu[i]->name != NULL)	free(op->yu[i]->name);
	  op->yu[i]->name = generate_units(IS_METER,IS_NANO);
	  op->yu[i]->dx *= pow(10,op->yu[i]->decade - IS_NANO);
	  op->yu[i]->decade = IS_NANO;
	  set_plt_y_unit_set(pr, i);
	}
    }	
  if ((op->yu == NULL) || i == op->n_yu)
    {
      create_attach_select_y_un_to_op(op, IS_METER, 0,1000,IS_NANO,0, "nm");
    }	
	
	
  /* we grab the dz data set */
  for (i = 0, dsi = NULL; i < op->n_dat; i++)
    {
      if (strstr(op->dat[i]->source,"Z bead1 - Z bead2") != NULL)
	{
	  dsi = op->dat[i];
	  break;
	}
    }	
  if (dsi == NULL)	
    return win_printf("cannot find 2 beads data set!");
	
  /* we find the min and max index of points in view */				
  for (imin = dsi->nx -1, imax = i = 0;i < dsi->nx; i++)
    {
      if (dsi->xd[i] <= max && i > imax) 		imax = i;			
      if (dsi->xd[i] >= min && i < imin) 		imin = i;			
    }
  /*	
	if (imax <= imin)	
	return win_printf("imin = %d imax = %d impossible",imin,imax);*/
  /* we find the min and max in Y of points in view */						
  for (i = 1, min_y = max_y = dsi->yd[0]; i < dsi->nx; i++)
    {
      max_y = (dsi->yd[i] > max_y) ? dsi->yd[i] : max_y;
      min_y = (dsi->yd[i] < min_y) ? dsi->yd[i] : min_y;
    }
	
  fit_ds_to_xn_polynome(dsi, 2, min, max, min_y - 1, max_y + 1, &ap);
	
  /*
    sprintf(ch,"Helicase segments burst %d",n_burst);
    dsd = find_source_specific_ds_in_pr(pr,ch); 
  */
  if (mode == -1) /* we create a new burst */
    {
      if (strstr(dsi->source,"Z bead1 - Z bead2") != NULL)
	{
	  cs = strstr(dsi->source,"Calibration from");
	  if (cs != NULL) 	sscanf(cs+16,"%s",bead);
	}
      time = dsi->time;
      zmag = -1;
		
      tmp = obj_cor*op->dy*(y1-y0)/((x1-x0)*op->dx);
      if (tmp < -60) sprintf(seg_type,"H");
      else if ((-60 <= tmp) && (tmp < -5)) sprintf(seg_type,"Z");
      else if ((-5 <= tmp) && (tmp < 5)) sprintf(seg_type,"N");
      else if ((5 <= tmp) && (tmp < 60)) sprintf(seg_type,"U");
      else sprintf(seg_type,"O");
		
      /*		win_printf("imin %d imax %d",imin,imax);*/
      sprintf(ch,
	      "I am going to adjust a polyline to an helicase burst "
	      "starting at point\n index %d -> %g %s  ending at %d -> %g %s\n"
	      "if there is a zmag data set give its number (-1 otherwise) %%4d\n"
	      "do you want to apply the obj. correction yes %%R, no %%r\n"
	      "correction factor %%7f type of segment (N noise,U unzipping, \n"
	      " Z zipping, R rehibridization ,H hairpin, O other) %%3s \n"
	      " [ATP] \\mu M %%8f helicase %%10s\n Burst number%%8ddead frac%%6d\n"
	      ,imin,op->ax+op->dx*dsi->xd[imin%dsi->nx],op->x_unit,imax-imin,
	      op->ax+op->dx*dsi->xd[imax%dsi->nx],op->x_unit);
      i = win_scanf(ch,&n_ds_zmag,&do_cor,&obj_cor,seg_type,&atp,heli,
		    &n_burst,&dead_frac);
      if (i == CANCEL)	return OFF;
      /*		win_printf("do_cor %d cor %g dsmag %d imin %d",do_cor,obj_cor,n_ds_zmag%op->n_dat,imin); */
      if (n_ds_zmag >= 0)
	{
	  ds_zmag = op->dat[n_ds_zmag%op->n_dat];
	  for (i = imin, tmp = ds_zmag->yd[imin]; i <= imax; i++)
	    if (tmp != ds_zmag->yd[i])	break;
	  if (i != imax+1) 
	    return win_printf("Zmag is not constant over your segment");
	  if (zmag >= 0 && zmag != tmp)
	    return win_printf("Zmag is not equal to %g over your segment\n"
			      "as it should but equals %g",zmag,tmp);
	  if (zmag < 0)	zmag = tmp;
	}	
      cor = (do_cor < 0) ? 1 : obj_cor;
      dsd = create_and_attach_one_ds(op, 65, 65, 0);
      if (dsd == NULL) 		return win_printf("cannot create ds !");

      dsd->nx = dsd->ny = 5;
      dsd->source =my_sprintf(NULL,"Helicase segments burst %d time %u\n"					"Bead %s\npath %s file %s\nZmag %g mm helicase %s [ATP] %g \\mu M"
			      " Zcor %g\nstarting at %d -> %g Type"
			      ,n_burst,time,backslash_to_slash(bead),
			      backslash_to_slash(op->dir),op->filename,
			      zmag,heli,atp,obj_cor,imin,op->ax+op->dx*dsi->xd[imin%dsi->nx]);
      dsd->treatement =my_sprintf(NULL,"%s",seg_type);		
      set_plot_symb(dsd,"\\pt5\\oc ");
      set_dash_line(dsd);	
      /*		win_printf_OK("we create a new burst ds %d\n%s",op->n_dat-1,dsd->source); */						
      dsd->xd[0] = dsd->xd[1] = dsd->xd[2] = dsd->xd[3] = x0;
      dsd->yd[0] = dsd->yd[1] = dsd->yd[2] = dsd->yd[3] = y0;
      dsd->xd[2] += (x1 - x0)/dead_frac;
      dsd->yd[2] += (y1 - y0)/dead_frac;
      dsd->xd[3] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->yd[3] += (dead_frac-1)*(y1 - y0)/dead_frac;		
      dsd->xd[4] = x1;
      dsd->yd[4] = y1;					
		
      x0 = dsd->xd[2];
      x1 = dsd->xd[3];
		
      min = (x0 < x1) ? x0 : x1;
      max = (x0 < x1) ? x1 : x0;
		
      if (ap != NULL)	 free(ap);
      ap = NULL;
      for (imin = dsi->nx -1, imax = i = 0;i < dsi->nx; i++)
	{
	  if (dsi->xd[i] <= max && i > imax) 		imax = i;			
	  if (dsi->xd[i] >= min && i < imin) 		imin = i;			
	}
      if (imax <= imin)	
	return win_printf("imin = %d imax = %d impossible",imin,imax);
      for (i = 1, min_y = max_y = dsi->yd[0]; i < dsi->nx; i++)
	{
	  max_y = (dsi->yd[i] > max_y) ? dsi->yd[i] : max_y;
	  min_y = (dsi->yd[i] < min_y) ? dsi->yd[i] : min_y;
	}
	
      fit_ds_to_xn_polynome(dsi, 2, min, max, min_y - 1, max_y + 1, &ap);		
		
      dsd->yd[2] = ap[0] + ap[1] * dsd->xd[2];				
      dsd->yd[3] = ap[0] + ap[1] * dsd->xd[3];				
      display_title_message("<V> = %4.3g %s/%s dt = %6.2g %s <P> = %4.3g %s",
			    obj_cor*ap[1]*op->dy/op->dx, (op->y_unit != NULL) ? op->y_unit : " ",
			    (op->x_unit != NULL) ? op->x_unit : " ",op->dx*(dsd->xd[4]-dsd->xd[1]),
			    (op->x_unit != NULL) ? op->x_unit : " ",obj_cor*op->dy*(dsd->yd[4]-dsd->yd[1]),
			    (op->y_unit != NULL) ? op->y_unit : " ");
      op->need_to_refresh = 1;
    }
  /* we check that Z mag is fine */
  if (n_ds_zmag >= 0)
    {
      ds_zmag = op->dat[n_ds_zmag%op->n_dat];
      for (i = imin, tmp = ds_zmag->yd[imin]; i <= imax; i++)
	if (tmp != ds_zmag->yd[i])	break;
      if (i != imax+1) 
	return win_printf("Zmag is not constant over your segment");
      if (zmag >= 0 && zmag != tmp)
	return win_printf("Zmag is not equal to %g over your segment\na"
			  "s it should but equals %g",zmag,tmp);
      if (zmag < 0)	zmag = tmp;
    }

  cor = (do_cor < 0) ? 1 : obj_cor; 
  if ((mode >= 0) && (mode < dsd->nx)) /* we just move points*/
    {
      if ((key[KEY_LCONTROL]) && (mode == (dsd->nx-1)))
	{
	  if (dsd->nx > 5)
	    {
	      dsd->nx = dsd->nx - 4; 
	      dsd->ny = dsd->ny - 4; 
	      i = strlen(dsd->treatement);
	      if (i > 0)		dsd->treatement[i-1] = 0;
	    }
	}
      else
	{
	  i = (mode - 1)%4;
	  i = (i == 0) ?  mode - 1 : mode;
	  if (i < 0) i = 0; 
	  dsd->xd[i] = dsd->xd[mode] = x1;
	  dsd->yd[i] = dsd->yd[mode] = y1;
	  i = (mode - 1)%4;
	  if (i == 1 || i == 2)
	    {
	      i = (mode-1)/4;
	      i = 4*i;
	      i += 1;
	      x0 = dsd->xd[i+1];
	      x1 = dsd->xd[i+2];
			
	      min = (x0 < x1) ? x0 : x1;
	      max = (x0 < x1) ? x1 : x0;
	      j = i;
	      if (ap != NULL)	 free(ap);
	      ap = NULL;
	      for (imin = dsi->nx -1, imax = i = 0;i < dsi->nx; i++)
		{
		  if (dsi->xd[i] <= max && i > imax) 		imax = i;			
		  if (dsi->xd[i] >= min && i < imin) 		imin = i;			
		}

	      for (i = 1, min_y = max_y = dsi->yd[0]; i < dsi->nx; i++)
		{
		  max_y = (dsi->yd[i] > max_y) ? dsi->yd[i] : max_y;
		  min_y = (dsi->yd[i] < min_y) ? dsi->yd[i] : min_y;
		}
	
	      fit_ds_to_xn_polynome(dsi, 2, min, max, min_y - 1, max_y + 1,&ap);
	      i = j;
	      dsd->yd[i+1] = ap[0] + ap[1] * dsd->xd[i+1];				
	      dsd->yd[i+2] = ap[0] + ap[1] * dsd->xd[i+2];				
	      display_title_message(
				    "<V> = %4.3g %s/%s dt = %6.2g %s <P> = %4.3g %s",
				    obj_cor*ap[1]*op->dy/op->dx, (op->y_unit != NULL) ? 
				    op->y_unit : " ", (op->x_unit != NULL) ? op->x_unit : " ",
				    op->dx*(dsd->xd[i+3]-dsd->xd[i]), (op->x_unit != NULL) ? 
				    op->x_unit : " ",obj_cor*op->dy*(dsd->yd[i+3]-dsd->yd[i]),
				    (op->y_unit != NULL) ? op->y_unit : " ");			
	    }
	}
      op->need_to_refresh = 1;		
    }		
  if (mode == -2) /* we extend the burst data set */
    {
      /*		return win_printf_OK("we add points");*/
      tmp = obj_cor*op->dy*(y1-y0)/((x1-x0)*op->dx);
      if (tmp < -60) sprintf(seg_type,"H");
      else if ((-60 <= tmp) && (tmp < -5)) sprintf(seg_type,"Z");
      else if ((-5 <= tmp) && (tmp < 5)) sprintf(seg_type,"N");
      else if ((5 <= tmp) && (tmp < 60)) sprintf(seg_type,"U");
      else sprintf(seg_type,"O");
      i = win_scanf("Type of segment added (N noise,U unzipping,\n"
		    " Z zipping, R rehibridization ,H hairpin, O other) %s"
		    "dead fraction %d",seg_type,&dead_frac);	
      if (i == CANCEL)	return OFF;
      i = dsd->nx;
      if (dsd->nx + 4 >= dsd->mx)
	build_adjust_data_set(dsd, dsd->mx + 64, dsd->mx + 64);
      dsd->xd[i] = dsd->xd[i+1] = dsd->xd[i+2] = dsd->xd[i+3] = x0;
      dsd->yd[i] = dsd->yd[i+1] = dsd->yd[i+2] = dsd->yd[i+3] = y0;
      dsd->xd[i+1] += (x1 - x0)/dead_frac;
      dsd->yd[i+1] += (y1 - y0)/dead_frac;
      dsd->xd[i+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->yd[i+2] += (dead_frac-1)*(y1 - y0)/dead_frac;		
      dsd->xd[i+3] = x1;
      dsd->yd[i+3] = y1;
      x0 = dsd->xd[i+1];
      x1 = dsd->xd[i+2];
      dsd->nx += 4;
      dsd->ny = dsd->nx;	
      min = (x0 < x1) ? x0 : x1;
      max = (x0 < x1) ? x1 : x0;
      j = i;
      if (ap != NULL)	 free(ap);
      ap = NULL;
      for (imin = dsi->nx -1, imax = i = 0;i < dsi->nx; i++)
	{
	  if (dsi->xd[i] <= max && i > imax) 		imax = i;			
	  if (dsi->xd[i] >= min && i < imin) 		imin = i;			
	}
      if (imax <= imin)	
	return win_printf("imin = %d imax = %d impossible",imin,imax);
      for (i = 1, min_y = max_y = dsi->yd[0]; i < dsi->nx; i++)
	{
	  max_y = (dsi->yd[i] > max_y) ? dsi->yd[i] : max_y;
	  min_y = (dsi->yd[i] < min_y) ? dsi->yd[i] : min_y;
	}
      i = j;
      fit_ds_to_xn_polynome(dsi, 2, min, max, min_y - 1, max_y + 1, &ap);		
      if (seg_type[0] == 'U' || seg_type[0] == 'u' || seg_type[0] == 'Z'
	  || seg_type[0] == 'z' || seg_type[0] == 'n' || seg_type[0] == 'N')
	{
	  dsd->yd[i+1] = ap[0] + ap[1] * dsd->xd[i+1];				
	  dsd->yd[i+2] = ap[0] + ap[1] * dsd->xd[i+2];				
	}
      dsd->treatement = my_sprintf(dsd->treatement, seg_type);
      display_title_message("<V> = %4.3g %s/%s dt = %6.2g %s <P> = %4.3g %s",
			    obj_cor*ap[1]*op->dy/op->dx, (op->y_unit != NULL) ? op->y_unit : " ",
			    (op->x_unit != NULL) ? op->x_unit : " ",op->dx*(dsd->xd[i+3]-dsd->xd[i]),
			    (op->x_unit != NULL) ? op->x_unit : " ",
			    obj_cor*op->dy*(dsd->yd[i+3]-dsd->yd[i]),
			    (op->y_unit != NULL) ? op->y_unit : " ");
      op->need_to_refresh = 1;
    }
							
  /* refisplay the entire plot */
  if (ap != NULL)	 free(ap);
  op->cur_dat = dsn;
  refresh_plot(pr, op_n);		
  return 0;
}

double 	adjust_bump(d_s *dsi, d_s *dsb, int first)
{
  register int i, j;
  int imin, ibump0, icenter, ibump1, imax, ib0b, ib1b, wi, ibmi, ibma, ii, it;
  double chi2 = 0, mean, bump, meanb = 0, bumpb = 0, chi2b = 0;

  recover_points_index_in_ds(dsb->xd[0], dsb->xd[5], dsi, &imin, &imax);
  recover_points_index_in_ds(dsb->xd[2], dsb->xd[3], dsi, &ibump0, &ibump1);
  if (ibump0 < imin || ibump0 >= imax)
    ibump0 = ((imin + imax)/2) - (imax - imin)/20;
  if (ibump1 < imin || ibump1 >= imax)
    ibump1 = ((imin + imax)/2) + (imax - imin)/20;
				 	
  icenter = (ibump0 + ibump1)/2;
  ibmi = imin + (imax - imin)/20;
  ibma = imax - (imax - imin)/20;
  wi = ibump1 - ibump0;
  ib0b = imin + wi/2;
  if (first) //we find center of bump having a fixed size
    {
      for(i = imin + wi/2, j = 0; (i < (imax -(3*wi)/2)) && (i < imax); i++, j++)		{ 
	chi2 = chi2_of_bump(dsi, imin, i, i+wi, imax, &mean, &bump);
	if ((j == 0) || chi2 < chi2b)
	  {
	    chi2b = chi2;
	    ib0b = i;
	    meanb = mean;
	    bumpb = bump;
	  }
      } 	
      ibump0 = ib0b;
      ibump1 = ib0b + wi;
      icenter = (ibump0 + ibump1)/2;
    }
  it = ((imin+imax)/2) + 1;
  ii = icenter + (ibump1 - icenter)/2;
  ib1b = ii = (ii < it) ? it : ii;
  //we adjust ib1b (second edge)
  for (i = ii, j = 0; (i < (icenter + ibump1 - ibump0)) && (i < ibma); i++, j++)
    { 
      chi2 = chi2_of_bump(dsi, imin, ibump0, i, imax, &mean, &bump);
      if ((j == 0) || chi2 < chi2b)
	{
	  chi2b = chi2;
	  ib1b = i;
	  meanb = mean;
	  bumpb = bump;
	}
    } 
  ii = icenter - ibump1 + ibump0;
  ii = (ii < ibmi) ? ibmi : ii;
  it = icenter - (ibump1 - icenter)/2;	
  //we adjust ib0b (first edge)
  for (ib0b = i = ii, j = 0; i < it && (i < ibma); i++, j++)
    { 
      chi2 = chi2_of_bump(dsi, imin, i, ib1b, imax, &mean, &bump);
      if ((j == 0) || chi2 < chi2b)
	{
	  chi2b = chi2;
	  ib0b = i;
	  meanb = mean;
	  bumpb = bump;
	}
    }
  dsb->xd[1] = dsb->xd[2] = dsi->xd[ib0b];
  dsb->xd[3] = dsb->xd[4] = dsi->xd[ib1b]; 	
  dsb->yd[0] = dsb->yd[1] = dsb->yd[4] = dsb->yd[5] = meanb;
  dsb->yd[2] = dsb->yd[3] = bumpb;	
  return chi2b;	
}


int		do_bump_segment(pltreg *pr, O_p *op, d_s *dsd, float x0, float y0, 
				float x1, float y1, int mode)
{
  register int i;
  d_s *dsi = NULL, *ds_zmag = NULL;
  int imin, imax, dsn, op_n;
  static int n_ds_zmag = -1, do_cor = 0; 
  static float zmag = -1, obj_cor = 0.878, cor = 1, atp = 500, peak_frac = .1;
  double min, max, tmp, chi2;
  float max_y, min_y, tmpf;
  char ch[1024], *cs, bead[512];
  static char heli[64];
  time_t time;	

  /* "This routine fits a segment to a data set using\n least quare fit
     data should be well ordered in x"); */

  min = (x0 < x1) ? x0 : x1;
  max = (x0 < x1) ? x1 : x0;
  if (x1 < x0)
    {
      tmpf = x1; x1 = x0; x0 = tmpf;
      tmpf = y1; y1 = y0; y0 = tmpf;
    }
  dsn = op->cur_dat;  op_n = pr->cur_op;
	
  if (op->yu != NULL)
    {
      for (i=0 ; i< op->n_yu ; i++)
	{
	  if (op->yu[i] != NULL && op->yu[i]->type == IS_METER)
	    break;
	}
      if (i != op->n_yu && op->yu[i]->decade != IS_NANO)
	{
	  if (op->yu[i]->name != NULL)	free(op->yu[i]->name);
	  op->yu[i]->name = generate_units(IS_METER,IS_NANO);
	  op->yu[i]->dx *= pow(10,op->yu[i]->decade - IS_NANO);
	  op->yu[i]->decade = IS_NANO;
	  set_plt_y_unit_set(pr, i);
	}
    }	
  if ((op->yu == NULL) || i == op->n_yu)
    create_attach_select_y_un_to_op(op, IS_METER, 0,1000,IS_NANO,0, "nm");
	
  for (i = 0, dsi = NULL; i < pr->one_p->n_dat; i++)
    {
      if (strstr(pr->one_p->dat[i]->source,"Z bead1 - Z bead2") != NULL)
	{dsi = pr->one_p->dat[i];	  break;}
    }	
  if (dsi == NULL)	return win_printf_OK("cannot find 2 beads data set!");


  for (i = 0; n_ds_zmag < 0 && i < pr->one_p->n_dat; i++)
    {
      if (strstr(pr->one_p->dat[i]->source,"Zmag position in Bead tracking") != NULL)
	{ds_zmag = pr->one_p->dat[n_ds_zmag = i];	  break;}
    }	
  if (ds_zmag == NULL)	
    {
      for (i = 0; n_ds_zmag < 0 && i < pr->one_p->n_dat; i++)
	{
	  if (strstr(pr->one_p->dat[i]->source,"rotation real time coordinate") != NULL)
	    {ds_zmag = pr->one_p->dat[n_ds_zmag = i];	  break;}
	}	
    }



  recover_points_index_in_ds(min, max, dsi, &imin, &imax);					
  find_ds_y_limits(dsi, 0, dsi->nx-1, &min_y, &max_y);
	
  /*
    sprintf(ch,"Helicase segments burst %d",n_burst);
    dsd = find_source_specific_ds_in_pr(pr,ch); 
  */
  if (mode == -1) /* we create a new burst */
    {
      if (strstr(dsi->source,"Z bead1 - Z bead2") != NULL)
	{
	  cs = strstr(dsi->source,"Calibration from");
	  if (cs != NULL) 	sscanf(cs+16,"%s",bead);
	}
      time = dsi->time;
      zmag = -1;
      sprintf(heli,"UvrD");
      tmp = obj_cor*op->dy*(y1-y0)/((x1-x0)*op->dx);
      sprintf(ch,"I am going to adjust a bump to Giuseppe data\n"
	      "starting at %d -> %g  extending over %d -> %g\n"
	      "if there is a zmag data set give its number \n(-1 otherwise) %%5d"
	      "do you want to apply the obj. correction\n(yes %%R, %%r) "
	      "correction factor %%10f\n [ATP] \\mu M %%8f enzyme %%12s\n"
	      "Bump number%%10d relative bump size%%12f\n"
	      ,imin,op->ax+op->dx*dsi->xd[imin%dsi->nx],imax-imin,
	      op->ax+op->dx*(dsi->xd[imax%dsi->nx]-dsi->xd[imin%dsi->nx]));
      i = win_scanf(ch,&n_ds_zmag,&do_cor,&obj_cor,&atp,heli,&n_burst,&peak_frac);
      if (i == CANCEL)	return OFF;
      if (n_ds_zmag >= 0)
	{
	  ds_zmag = op->dat[n_ds_zmag%op->n_dat];
	  for (i = imin, tmp = ds_zmag->yd[imin]; i <= imax; i++)
	    if (tmp != ds_zmag->yd[i])	break;
	  if (i != imax+1) 
	    return win_printf_OK("Zmag is not constant over your segment");
	  if (zmag >= 0 && zmag != tmp)
	    return win_printf_OK("Zmag is not equal to %g over your segment\n"
				 "as it should but equals %g",zmag,tmp);
	  if (zmag < 0)	zmag = tmp;
	}	
      cor = (do_cor < 0) ? 1 : obj_cor; 
      dsd = create_and_attach_one_ds(pr->one_p, 6, 6, 0);
      if (dsd == NULL) 		return win_printf_OK("cannot create ds !");
      dsd->source =my_sprintf(NULL,"Signal segment bump %d time %u\n"	
			      "Bead %s\npath %s file %s\nZmag %g mm enzyme %s [ATP] %g \\mu M"
			      " Zcor %g\nstarting at %d -> %g Type"
			      ,n_burst,time,backslash_to_slash(bead),
			      backslash_to_slash(op->dir),op->filename,
			      zmag,heli,atp,obj_cor,imin,op->ax+op->dx*dsi->xd[imin%dsi->nx]);
      set_plot_symb(dsd,"\\pt5\\oc ");
      set_dash_line(dsd);	
      set_ds_line_color(dsd, Lightred);
      dsd->treatement = my_sprintf(NULL,"B");		
      dsd->xd[0] = x0;
      dsd->xd[1] = dsd->xd[2] = ((x0 + x1)/2) - ((x1 - x0)*peak_frac);
      dsd->xd[3] = dsd->xd[4] = ((x0 + x1)/2) + ((x1 - x0)*peak_frac);
      dsd->xd[5] = x1;
      dsd->yd[0] = dsd->yd[1] = y0;
      dsd->yd[2] = dsd->yd[3] = y0;
      dsd->yd[4] = dsd->yd[5] = y0;
		
      chi2 = adjust_bump(dsi,dsd,1);		
      display_title_message("\\chi^2 = %4.3g dt = %6.2g %s <\\delta z> = %4.3g %s",
			    chi2, op->dx*(dsd->xd[3]-dsd->xd[2]),
			    (op->x_unit != NULL) ? op->x_unit : " ",
			    obj_cor*op->dy*(dsd->yd[2]-dsd->yd[1]),
			    (op->y_unit != NULL) ? op->y_unit : " ");
      dsd->history = my_sprintf(NULL,"\\chi^2 = %4.3g dt = %6.2g %s <\\delta z> = %4.3g %s\n",
				chi2, op->dx*(dsd->xd[3]-dsd->xd[2]),
				(op->x_unit != NULL) ? op->x_unit : " ",
				obj_cor*op->dy*(dsd->yd[2]-dsd->yd[1]),
				(op->y_unit != NULL) ? op->y_unit : " ");
    }
  if (n_ds_zmag >= 0)
    {
      ds_zmag = op->dat[n_ds_zmag%op->n_dat];
      for (i = imin, tmp = ds_zmag->yd[imin]; i <= imax; i++)
	if (tmp != ds_zmag->yd[i])	break;
      if (i != imax+1) 
	return win_printf_OK("Zmag is not constant over your segment");
      if (zmag >= 0 && zmag != tmp)
	return win_printf_OK("Zmag is not equal to %g over your segment\n"
			     "nas it should but equals %g",zmag,tmp);
      if (zmag < 0)	zmag = tmp;
    }
  cor = (do_cor < 0) ? 1 : obj_cor; 
  if ((mode >= 0) && (mode < dsd->nx)) /* we just move points*/
    {
      /*
      if (mode == 0 || mode == dsd->nx-1)	i = mode;
      else if (mode == 1 || mode == 3)			
	{
	  x1 = (x1 < dsd->xd[5]) ? x1 : dsd->xd[5]; 
	  x1 = (x1 > dsd->xd[0]) ? x1 : dsd->xd[0]; 
	  i = mode = i+1;
	}
      else if (mode == 2 || mode == 4)			
	{
	  x1 = (x1 < dsd->xd[5]) ? x1 : dsd->xd[5]; 
	  x1 = (x1 > dsd->xd[0]) ? x1 : dsd->xd[0]; 		
	  i = mode = i-1;
	}
      dsd->xd[i] = dsd->xd[mode] = x1;
      */	
      if (mode < 3) dsd->xd[0] = x0; 
      else dsd->xd[5] = x1; 

      chi2 = adjust_bump(dsi,dsd,0);		
      display_title_message("\\chi^2 = %4.3g dt = %6.2g %s <\\delta z> = %4.3g %s",
			    chi2, op->dx*(dsd->xd[3]-dsd->xd[2]),
			    (op->x_unit != NULL) ? op->x_unit : " ",
			    obj_cor*op->dy*(dsd->yd[2]-dsd->yd[1]),
			    (op->y_unit != NULL) ? op->y_unit : " ");
      if (dsd->history)	free(dsd->history);
      dsd->history =my_sprintf(NULL,"\\chi^2 = %4.3g dt = %6.2g %s <\\delta z> = %4.3g %s\n",
			       chi2, op->dx*(dsd->xd[3]-dsd->xd[2]),
			       (op->x_unit != NULL) ? op->x_unit : " ",
			       obj_cor*op->dy*(dsd->yd[2]-dsd->yd[1]),
			       (op->y_unit != NULL) ? op->y_unit : " ");		
		

    }		
  /* refisplay the entire plot */
  op->cur_dat = dsn;
  op->need_to_refresh = 1;
  refresh_plot(pr, op_n);		
  return 0;
}


						
# ifdef DOUBLON
	
int find_selected_pr_bump_segment(pltreg *pr, int x_0, int y_0)
{
  register int i, j, k;
  char c[256];
  int  x_m, y_m, dx = 0, dy = 0, ldx, ldy, dxmax, dxmin, dymax, dymin, px, py, color;
  float x, y, xa, ya;
  int px0, py0;
  int inds, ipt, mode = 0;
  O_p *op;
  d_s *ds;
  int		do_bump_segment(pltreg *pr, O_p *op, d_s *dsi, float x0, float y0, 
				float x1, float y1, int mode);

  if (pr->one_p == NULL)		return 1;
  op = pr->one_p;
	
	
  y = ya = y_pr_2_pltdata_raw(pr, y_0);
  x = xa = x_pr_2_pltdata_raw(pr, x_0);	




  ldy = 0;
  ldx = 0;			
  color = makecol(255, 64, 64);	





  ldy = event.y - y_m;
  ldx = event.x - x_m;	

  px0 = event.x - cur_ac_reg->area.x0;	
  py0 = event.y - cur_ac_reg->area.y0;		
  for (ipt = 0, inds = - 1, k = 0; k < op->n_dat && inds == -1; k++)
    {
      ds = op->dat[k];
      if (strstr(ds->source,"Signal segment bump") != NULL)
	{
			
	  for (i = 0; i < ds->nx && i < ds->ny && inds == -1; i++)
	    {	/* remenber point 0 and 1 are the same */
	      if ((abs(px0 - x_pltdata_2_pr(pr,ds->xd[i])) < 5 ) 
		  && (abs(py0 - y_pltdata_2_pr(pr,ds->yd[i])) < 5 ))
		{
		  inds = k;
		  ipt = i;
		  /*					win_printf("pt found %d in ds%d",i,k); */
		}
	    }
	}
    }
  if (inds != -1 && ipt >= 0)
    {
      ds = op->dat[inds];
      op->cur_dat = inds;
      j = (ipt < 6) ? ipt : 5;
      j = (ipt < 0) ? 0 : ipt; 
      i = (j%2) ? 2*(j/2) : j+1;
      xa = ds->xd[i];
      ya = ds->yd[i];
      x_m = cur_ac_reg->area.x0 + x_pltdata_2_pr(pr, ds->xd[i]);
      y_m = cur_ac_reg->area.y0 + y_pltdata_2_pr(pr, ds->yd[i]);		
      mode = ipt;
    }
  else
    {
      n_burst++;	
      mode = -1;
    }	


	
  while (mouse_b & 0x03)
    {
      dx = mouse_x - x_0;
      dy = mouse_y - y_0;
      if  (ldy != dy || dx != ldx)
	{
	  blit_plot_reg(d);
	  line(screen,x_0,y_0,x_0 + dx,y_0 + dy,color);		

	  draw_bubble(screen, B_LEFT, 20, 64, "x0 = %g y0 %g",									x_pr_2_pltdata(pr,x_0),y_pr_2_pltdata(pr,y_0));	
	  draw_bubble(screen, B_RIGHT, SCREEN_W-64, 64, "x1 = %g y2 %g", 	 					x_pr_2_pltdata(pr,x_0 + dx),y_pr_2_pltdata(pr,y_0 + dy));	
	  y = y_pr_2_pltdata_raw(pr, y_0 + dy);
	  x = x_pr_2_pltdata_raw(pr, x_0 + dx);											}
      if (key[KEY_ESC])	return D_REDRAWME;	
      ldx = dx;
      ldy = dy;
    }
  if ( abs(dy) > 5 || abs(dx) > 5) 
    {
      do_simple_poly_on_segment(pr, op, op->dat[op->cur_dat], xa, ya, x, y);
      return 0;
    }
  return D_O_K;	


  dxmax =  pr->db.x1 - x_m + cur_ac_reg->area.x0;
  dxmin = cur_ac_reg->area.x0 + pr->db.x0 - x_m;	
  dymax = pr->db.y1 - y_m + cur_ac_reg->area.y0;
  dymin = cur_ac_reg->area.y0  + pr->db.x0 - y_m;									
																			
  MouseSetCursorMode(M_CUR_LINE,x_m,y_m,RED_0);
  while (1)
    {
      MyGetEvent((M_POLL | M_BUTTON_DOWN | M_MOTION | M_BUTTON_UP), &event);
      if (event.flags & M_MOTION)
	{	
	  dx = event.x - x_m;
	  dy = event.y - y_m;
	  last_m_event = event;
	  dy = (dy < dymin) ? dymin : dy;
	  dy = (dy >= dymax) ? dymax : dy;
	  dx = (dx < dxmin) ? dxmin : dx;
	  dx = (dx >= dxmax) ? dxmax : dx;			
	  if (dy != ldy || dx != ldx)	MouseWarp(x_m + dx, y_m + dy);
	  y = y_pr_2_pltdata_raw(pr, y_m + dy - cur_ac_reg->area.y0);
	  x = x_pr_2_pltdata_raw(pr, x_m + dx - cur_ac_reg->area.x0);						}
      else if ( ti != NULL)
	{
	  if  (ldy != dy || dx != ldx)
	    {
	      /*				sprintf(c,"%g < x < %g and %g < y < %g",xa,x,y,ya);
						if (op->n_dat > 2)
						{
						ds = op->dat[2];
						sprintf(c,"mouse %d %d pt %d %d",px0,py0,
						x_pltdata_2_pr(pr,ds->xd[4]),y_pltdata_2_pr(pr,ds->yd[4]));	
						}
						else 		*/
	      if (dy != ldy)	sprintf(c,"mouse dy %d min %d max %d",last_m_event.y - y_m,dymin,dymax);
	      else sprintf(c,"%g < x < %g mode %d",xa,x,mode);
	      modify_title(ti,c);
	      ldy = dy;
	      ldx = dx;
	    }
	}
      if (event.flags & M_BUTTON_UP)
	{
	  MouseSetCursorMode(M_CUR_NORMAL);
	  if ( abs(dy) > 2 || abs(dx) > 2) 
	    {
	      do_bump_segment(pr, op, op->dat[op->cur_dat], xa, ya, x, y, mode);
	      return 0;
	    }
	  else return 1;
	}
    }	
  return 0;	
}
		
# endif

int find_selected_pr_segment(pltreg *pr, int x_0, int y_0, DIALOG *d)
{
  int   dx = 0, dy = 0, ldx, ldy, color, sc;
  float x, y, xa, ya;
  struct box *b;
  O_p *op;
		
  if (pr->one_p == NULL)		return D_O_K;
  op = pr->one_p;
  sc = pr->screen_scale;	
  b = pr->stack;
  /*	
	dxmax =  SCREEN_W - x_m - 32 ;
	dxmin =  -x_m;	
	dymax = SCREEN_H - y_m - 32;
	dymin =  - y_m;	
  */	
  y = ya = y_pr_2_pltdata_raw(pr, y_0);
  x = xa = x_pr_2_pltdata_raw(pr, x_0);	
	
  /*
    px = x_m - cur_ac_reg->area.x0;
    py = cur_ac_reg->area.y0 - y_m;
  */	
  ldy = 0;
  ldx = 0;			
  color = makecol(255, 64, 64);	
	
  while (mouse_b & 0x03)
    {
      dx = mouse_x - x_0;
      dy = mouse_y - y_0;
      if  (ldy != dy || dx != ldx)
	{
	  blit_plot_reg(d);
	  line(screen,x_0,y_0,x_0 + dx,y_0 + dy,color);		

	  draw_bubble(screen, B_LEFT, 20, 64, "x0 = %g y0 %g", x_pr_2_pltdata(pr,x_0),y_pr_2_pltdata(pr,y_0));	
	  draw_bubble(screen, B_RIGHT, SCREEN_W-64, 64, "x1 = %g y2 %g",x_pr_2_pltdata(pr,x_0 + dx),
		      y_pr_2_pltdata(pr,y_0 + dy));	
	  y = y_pr_2_pltdata_raw(pr, y_0 + dy);
	  x = x_pr_2_pltdata_raw(pr, x_0 + dx);
	}
      if (key[KEY_ESC])	return D_REDRAWME;	
      ldx = dx;
      ldy = dy;
    }
  if ( abs(dy) > 5 || abs(dx) > 5) 
    {
      do_simple_poly_on_segment(pr, op, op->dat[op->cur_dat], xa, ya, x, y);
      return 0;
    }
  return D_O_K;	
}		


int find_selected_pr_simple_segment(pltreg *pr, int x_0, int y_0, DIALOG *d)
{
  int   dx = 0, dy = 0, ldx, ldy, color, sc;
  float x, y, xa, ya;
  struct box *b;
  O_p *op;
		
  if (pr->one_p == NULL)		return D_O_K;
  op = pr->one_p;
  sc = pr->screen_scale;	
  b = pr->stack;
  /*	
	dxmax =  SCREEN_W - x_m - 32 ;
	dxmin =  -x_m;	
	dymax = SCREEN_H - y_m - 32;
	dymin =  - y_m;	
  */	
  y = ya = y_pr_2_pltdata_raw(pr, y_0);
  x = xa = x_pr_2_pltdata_raw(pr, x_0);	
	
  /*
    px = x_m - cur_ac_reg->area.x0;
    py = cur_ac_reg->area.y0 - y_m;
  */	
  ldy = 0;
  ldx = 0;			
  color = makecol(255, 64, 64);	
	
  while (mouse_b & 0x03)
    {
      dx = mouse_x - x_0;
      dy = mouse_y - y_0;
      if  (ldy != dy || dx != ldx)
	{
	  blit_plot_reg(d);
	  line(screen,x_0,y_0,x_0 + dx,y_0 + dy,color);		

	  draw_bubble(screen, B_LEFT, 20, 64, "x0 = %g y0 %g", x_pr_2_pltdata(pr,x_0),y_pr_2_pltdata(pr,y_0));	
	  draw_bubble(screen, B_RIGHT, SCREEN_W-64, 64, "x1 = %g y2 %g",x_pr_2_pltdata(pr,x_0 + dx),
		      y_pr_2_pltdata(pr,y_0 + dy));	
	  y = y_pr_2_pltdata_raw(pr, y_0 + dy);
	  x = x_pr_2_pltdata_raw(pr, x_0 + dx);
	}
      if (key[KEY_ESC])	return D_REDRAWME;	
      ldx = dx;
      ldy = dy;
    }
  if ( abs(dy) > 5 || abs(dx) > 5) 
    {
      do_simple_segment(pr, op, op->dat[op->cur_dat], xa, ya, x, y);
      return 0;
    }
  return D_O_K;	
}		
						
				
int find_selected_pr_burst_segment_old(pltreg *pr, int x_0, int y_0, DIALOG *d)
{
  register int i, j, k;
  int   dx = 0, dy = 0, ldx, ldy, color, sc, dxmax, dxmin, dymax, dymin;	
  float x, y, xa, ya, dtmin = FLT_MAX, tmp;
  int inds, ipt, dsview, ptview, mode = 0;
  int px0, py0, x_m, y_m;
  O_p *op;
  d_s *ds;
  struct box *b;

  if (pr->one_p == NULL)		return D_O_K;
  op = pr->one_p;
  sc = pr->screen_scale;	
  b = pr->stack;

  x_m = x_0;
  y_m = y_0;

  y = ya = y_pr_2_pltdata_raw(pr, y_0 - d->y);
  x = xa = x_pr_2_pltdata_raw(pr, x_0 - d->x);	


  ldy = 0;
  ldx = 0;			
  color = makecol(255, 64, 64);	
  px0 = x_0 - d->x;	
  py0 = y_0 - d->y;		
  for(ipt = 0, ptview=dsview=inds= - 1, k = 0; k < op->n_dat && inds == -1; k++)
    {
      ds = op->dat[k];
      if (strstr(ds->source,"Helicase segments burst") != NULL)
	{
	  for (i = ds->nx-1; i > 0 && inds == -1; i--)
	    {	/* remenber point 0 and 1 are the same */
	      if ((abs(px0 - x_pltdata_2_pr(pr,ds->xd[i])) < 5 ) 
		  && (abs(py0 - y_pltdata_2_pr(pr,ds->yd[i])) < 5 ))
		{
		  inds = k; /* the ds is found*/
		  ipt = i; /* the point index is found*/
		}
	      if ((ds->xd[i] >= op->x_lo) && (ds->xd[i] < op->x_hi)
		  && (ds->yd[i] >= op->y_lo) && (ds->yd[i] < op->y_hi))
		{ /* the burst is in view */
		  dsview = k; 
		  tmp = op->dx * (x_pr_2_pltdata_raw(pr,px0)- ds->xd[i]);
		  /*		if (i == ds->nx - 1) win_printf("looking ds%d view %d tmp %g",k,i,tmp); */
		  if (tmp > 0 && tmp < dtmin)
		    {	/* we find the point in view having the smallest x*/
		      ptview = i; 
		      dtmin = tmp;
		    }
		}
	    }
	}
    }
  if (key[KEY_LSHIFT] || key[KEY_RSHIFT])
    {
      n_burst++;	
      mode = -1;
    }
  else if (inds < 0 && dsview < 0)	
    {
      n_burst++;	
      mode = -1;
    }
  else if (inds != -1 && ipt > 0)
    {
      ds = op->dat[inds];
      op->cur_dat = inds;
      j = (ipt - 1)%4;
      i = (ipt - 1)/4;
      i *= 4;
      i += 1;		
      if (j == 0)			i += 3;
      else if (j == 1)	i += 2;
      else if (j == 2)	i += 1;
      else if (j == 3)	i += 0;
      xa = ds->xd[i];
      ya = ds->yd[i];
      x_m = d->x + x_pltdata_2_pr(pr, ds->xd[i]);
      y_m = d->y + y_pltdata_2_pr(pr, ds->yd[i]);		
      mode = ipt;
    }
  else if (dsview >= 0 && (ptview == op->dat[dsview]->nx-1))
    {
      /*		win_printf("dsview %d ptview %d",dsview,ptview);	*/
      ds = op->dat[dsview];
      op->cur_dat = dsview;
      i = ds->nx-1;
      xa = ds->xd[i];
      ya = ds->yd[i];
      x_m = d->x + x_pltdata_2_pr(pr, ds->xd[i]);
      y_m = d->y + y_pltdata_2_pr(pr, ds->yd[i]);		
      mode = -2;		
    }
  dxmax =  d->w + d->x - x_m - 32 ;
  dxmin =  -x_m + d->x;	
  dymax = d->h + d->y - y_m - 32;
  dymin =  -y_m + d->y;		
  /*	display_title_message("%d < x < %d, %d < y < %d = %g y = %g",dxmin,dxmax,dymin,dymax,x,y);		*/
  while (mouse_b & 0x03)
    {		
      dx = mouse_x - x_m;
      dy = mouse_y - y_m;
      dy = (dy < dymin) ? dymin : dy;
      dy = (dy >= dymax) ? dymax : dy;
      dx = (dx < dxmin) ? dxmin : dx;
      dx = (dx >= dxmax) ? dxmax : dx;			
      if (dy != ldy || dx != ldx)
	{
	  mouse_x = x_m + dx;
	  mouse_y = y_m + dy;
	  y = y_pr_2_pltdata_raw(pr, y_m + dy - d->y); /*   */
	  x = x_pr_2_pltdata_raw(pr, x_m + dx - d->x);
	  blit_plot_reg(d);			
	  if (dy != ldy)	
	    display_title_message("mouse dy %d min %d max %d",
				  dy,dymin,dymax);
	  else display_title_message("%g < x < %g mode %d",xa,x,mode);
	  line(screen,x_m,y_m,x_m + dx,y_m + dy,color);		
	  ldy = dy;
	  ldx = dx;	
	  if (key[KEY_ESC])	return D_REDRAWME;			
	}
    }
  if ( abs(dy) > 2 || abs(dx) > 2) 
    {
      /*		win_printf("y0 %g y1 %g",ya,y);*/
      do_burst_segment(pr, op, op->dat[op->cur_dat], xa, ya, x, y, mode);
      return 0;
    }
  else return D_O_K;
  return 0;	
}																					
				
int find_selected_pr_burst_segment(pltreg *pr, int x_0, int y_0, DIALOG *d)
{
  register int i, j, k;
  int   dx = 0, dy = 0, ldx, ldy, color, sc, dxmax, dxmin, dymax, dymin;	
  float x, y, xa, ya, dtmin = FLT_MAX, tmp;
  int inds, ipt, dsview, ptview, mode = 0, ndscur;
  int px0, py0, x_m, y_m, dsinx, imin, imax;
  float *dsixd, *dsiyd;
  O_p *op;
  d_s *ds = NULL, *dsi;
  struct box *b;

  if (pr->one_p == NULL)		return D_O_K;
  op = pr->one_p;
  sc = pr->screen_scale;	
  b = pr->stack;
  dsi = op->dat[op->cur_dat];
  ndscur = op->cur_dat;
	
  x_m = x_0;
  y_m = y_0;

  y = ya = y_pr_2_pltdata_raw(pr, y_0 - d->y);
  x = xa = x_pr_2_pltdata_raw(pr, x_0 - d->x);	


  ldy = 0;
  ldx = 0;			
  color = makecol(255, 64, 64);	
  px0 = x_0;// - d->x;	
  py0 = y_0;// - d->y;		
  for(ipt = 0, ptview=dsview=inds= - 1, k = 0; k < op->n_dat && inds == -1; k++)
    {
      ds = op->dat[k];
      if (strstr(ds->source,"Helicase segments burst") != NULL)
	{
	  for (i = ds->nx-1; i > 0 && inds == -1; i--)
	    {	/* remenber point 0 and 1 are the same */
	      if ((abs(px0 - x_pltdata_2_pr(pr,ds->xd[i])) < 5 ) 
		  && (abs(py0 - y_pltdata_2_pr(pr,ds->yd[i])) < 5 ))
		{
		  inds = k; /* the ds is found*/
		  ipt = i; /* the point index is found*/
		}
	      if ((ds->xd[i] >= op->x_lo) && (ds->xd[i] < op->x_hi)
		  && (ds->yd[i] >= op->y_lo) && (ds->yd[i] < op->y_hi))
		{ /* the burst is in view */
		  dsview = k; 
		  tmp = op->dx * (x_pr_2_pltdata_raw(pr,px0)- ds->xd[i]);
		  /*		if (i == ds->nx - 1) win_printf("looking ds%d view %d tmp %g",k,i,tmp); */
		  if (tmp > 0 && tmp < dtmin)
		    {	/* we find the point in view having the smallest x*/
		      ptview = i; 
		      dtmin = tmp;
		    }
		}
	    }
	}
    }
	
  /* we find the min and max index of points in view */				
  for (imin = dsi->nx -1, imax = i = 0;i < dsi->nx; i++)
    {
      if (dsi->xd[i] <= op->x_hi && i > imax) 		imax = i;			
      if (dsi->xd[i] >= op->x_lo && i < imin) 		imin = i;			
    }	
  dsinx = dsi->nx;
  dsi->nx = dsi->ny = imax - imin;
  dsixd = dsi->xd;
  dsiyd = dsi->yd;
  dsi->xd = dsi->xd + imin;
  dsi->yd = dsi->yd + imin;
	
  if (key[KEY_LSHIFT] || key[KEY_RSHIFT])
    {
      n_burst++;	
      mode = -1;
    }
  else if (inds < 0 && dsview < 0)	
    {
      n_burst++;	
      mode = -1;
    }
  else if (inds != -1 && ipt > 0)
    {	/* we modify a point */
      ds = op->dat[inds];
      op->cur_dat = inds;
      j = (ipt - 1)%4;
      i = (ipt - 1)/4;
      i *= 4;
      i += 1;		
      if (j == 0)			i += 3;
      else if (j == 1)	i += 2;
      else if (j == 2)	i += 1;
      else if (j == 3)	i += 0;
      xa = ds->xd[i];
      ya = ds->yd[i];
      x_m = d->x + x_pltdata_2_pr(pr, ds->xd[i]);
      y_m = d->y + y_pltdata_2_pr(pr, ds->yd[i]);		
      mode = ipt;
      dxmax =  d->w + d->x - x_m - 32 ;
      dxmin =  -x_m + d->x;	
      dymax = d->h + d->y - y_m - 32;
      dymin =  -y_m + d->y;				
      while (mouse_b & 0x03)
	{		
	  dx = mouse_x - x_m;
	  dy = mouse_y - y_m;
	  dy = (dy < dymin) ? dymin : dy;
	  dy = (dy >= dymax) ? dymax : dy;
	  dx = (dx < dxmin) ? dxmin : dx;
	  dx = (dx >= dxmax) ? dxmax : dx;			
	  if (dy != ldy || dx != ldx)
	    {
	      mouse_x = x_m + dx;
	      mouse_y = y_m + dy;
	      ds->yd[ipt] = y = y_pr_2_pltdata_raw(pr, y_m + dy - d->y); 
	      ds->xd[ipt] = x = x_pr_2_pltdata_raw(pr, x_m + dx - d->x);
	      if (ipt == 1)
		{
		  ds->yd[0] = y;
		  ds->xd[0] = x;
		}
	      do_one_plot (pr); 
	      write_and_blit_box(b, sc, plt_buffer, the_dialog+2, pr);	
	      display_title_message("index %d vis a vis %d ",ipt,i);		
	    }
	}	
      op->cur_dat = ndscur;
    }
  else if (dsview >= 0 && (ptview == op->dat[dsview]->nx-1))
    {	/* we will add a new segment */
      /*		win_printf("dsview %d ptview %d",dsview,ptview);	*/
      ds = op->dat[dsview];
      op->cur_dat = dsview;
      i = ds->nx-1;
      xa = ds->xd[i];
      ya = ds->yd[i];
      x_m = d->x + x_pltdata_2_pr(pr, ds->xd[i]);
      y_m = d->y + y_pltdata_2_pr(pr, ds->yd[i]);		
      mode = -2;		
    }
  dxmax =  d->w + d->x - x_m - 32 ;
  dxmin =  -x_m + d->x;	
  dymax = d->h + d->y - y_m - 32;
  dymin =  -y_m + d->y;		
  /*	display_title_message("%d < x < %d, %d < y < %d = %g y = %g",dxmin,dxmax,dymin,dymax,x,y);		*/


  while (mouse_b & 0x03 && mode <= 0)
    {		
      dx = mouse_x - x_m;
      dy = mouse_y - y_m;
      dy = (dy < dymin) ? dymin : dy;
      dy = (dy >= dymax) ? dymax : dy;
      dx = (dx < dxmin) ? dxmin : dx;
      dx = (dx >= dxmax) ? dxmax : dx;			
      if (dy != ldy || dx != ldx)
	{
	  mouse_x = x_m + dx;
	  mouse_y = y_m + dy;
	  y = y_pr_2_pltdata_raw(pr, y_m + dy - d->y); 
	  x = x_pr_2_pltdata_raw(pr, x_m + dx - d->x);

	  blit_plot_reg(d);			
	  /*			if (dy != ldy)	
				display_title_message("mouse dy %d min %d max %d",
				dy,dymin,dymax);
				else */display_title_message("%g < x < %g mode %d",xa,x,mode);

	  if (mode == -2)
	    {
	      line(screen,x_m,y_m,x_m + dx,y_m + dy,dsi->color);		/* possible pb dsi */
	    }
	  else line(screen,x_m,y_m,x_m + dx,y_m + dy,color);		
	  ldy = dy;
	  ldx = dx;	
	  if (key[KEY_ESC])	
	    {
	      dsi->ny = dsi->nx = dsinx;
	      dsi->xd = dsixd;
	      dsi->yd = dsiyd;
	      return D_REDRAWME;			
	    }
	}
    }
  dsi->ny = dsi->nx = dsinx;
  dsi->xd = dsixd;
  dsi->yd = dsiyd;
  op->cur_dat = ndscur;
  if ( abs(dy) > 2 || abs(dx) > 2) 
    {
      /*		win_printf("x0 %g y0 %g x1 %g y1 %g",xa,ya,x,y);*/
      if (ds != NULL) do_burst_segment(pr, op, ds, xa, ya, x, y, mode);
      return 0;
    }
  else return D_O_K;
  return 0;	
}																					
	
int find_selected_pr_bump_segment(pltreg *pr, int x_0, int y_0, DIALOG *d)
{
  register int i, j, k;
  int  x_m, y_m, dx = 0, dy = 0, ldx, ldy, color;
  float x, y, xa, ya;
  int px0, py0;
  int inds, ipt, mode = 0;
  O_p *op;
  d_s *ds;
  int do_bump_segment(pltreg *pr, O_p *op, d_s *dsi, float x0, float y0, float x1, float y1, int mode);

  if (pr->one_p == NULL)		return D_O_K;
  op = pr->one_p;
	
  y_m = y_0;
  x_m = x_0;
  y = ya = y_pr_2_pltdata_raw(pr, y_0);
  x = xa = x_pr_2_pltdata_raw(pr, x_0);	

  ldy = 0;
  ldx = 0;			
  color = makecol(255, 64, 64);	

  px0 = x_0;//- d->x;	
  py0 = y_0;// - d->y;
  for (ipt = 0, inds = - 1, k = 0; k < op->n_dat && inds == -1; k++)
    {
      ds = op->dat[k];
      if (strstr(ds->source,"Signal segment bump") != NULL)
	{
	  for (i = 0; i < ds->nx && i < ds->ny && inds == -1; i++)
	    {	/* remenber point 0 and 1 are the same */
	      if ((abs(px0 - x_pltdata_2_pr(pr,ds->xd[i])) < 5 ) 
		  && (abs(py0 - y_pltdata_2_pr(pr,ds->yd[i])) < 5 ))
		{
		  inds = k;
		  ipt = i;
		  		//win_printf("pt found %d in ds%d",i,k); 
		}
	      else display_title_message("found ds%d but no point",k);
	      //win_printf("px0 %d py0 %d x[%d] = %d y[%d] %d",px0,py0,i,x_pltdata_2_pr(pr,ds->xd[i])
	      //	 ,i,y_pltdata_2_pr(pr,ds->yd[i]));
	    }
	}
      else if (k == op->n_dat-1) display_title_message("last ds%d",k); 

    }
  if (inds != -1 && ipt >= 0)
    {
      display_title_message("editing %d %d",inds,ipt); 
      ds = op->dat[inds];
      op->cur_dat = inds;
      j = (ipt < 6) ? ipt : 5;
      j = (ipt < 0) ? 0 : ipt; 
      //i = (j%2) ? 2*(j/2) : j+1;
      i = (j < 3) ? 5 : 0;
      xa = ds->xd[i];
      ya = ds->yd[i];
      x_m =  x_pltdata_2_pr(pr, ds->xd[i]); // d->x +
      y_m =  y_pltdata_2_pr(pr, ds->yd[i]); // d->y +		
      mode = (j < 3) ? 0 : 5; //ipt;
    }
  else
    {
      display_title_message("new bump"); 
      n_burst++;	
      mode = -1;
    }	
	
  while (mouse_b & 0x03)
    {
      dx = mouse_x - x_0;
      dy = mouse_y - y_0;
      if  (ldy != dy || dx != ldx)
	{
	  blit_plot_reg(d);
	  line(screen,x_m,y_m,x_0 + dx,y_0 + dy,color);		

	  draw_bubble(screen, B_LEFT, 20, 64, "x0 = %g y0 %g",x_pr_2_pltdata(pr,x_m),y_pr_2_pltdata(pr,y_m));	
	  draw_bubble(screen, B_RIGHT, SCREEN_W-64, 64, "x1 = %g y2 %g",x_pr_2_pltdata(pr,x_0 + dx),
		      y_pr_2_pltdata(pr,y_0 + dy));	
	  y = y_pr_2_pltdata_raw(pr, y_0 + dy);
	  x = x_pr_2_pltdata_raw(pr, x_0 + dx);
	}
      if (key[KEY_ESC])	
	{
	  clear_keybuf();
	  return D_REDRAWME;	
	}
      ldx = dx;
      ldy = dy;
    }

  if ( abs(dy) > 5 || abs(dx) > 5) 
    {
      //win_printf("bump %d, mode %d,\n xa %g x %g ya %g y %g", n_burst,mode,xa, x, ya, y);
      do_bump_segment(pr, op, op->dat[op->cur_dat], xa, ya, x, y, mode);
      return 0;
    }
  return D_O_K;	
}
		


int 	do_select_pr_simple_segment(void)
{
  if(updating_menu_state != 0)	return D_O_K;	
  prev_general_pr_action = general_pr_action;
  general_pr_action = find_selected_pr_simple_segment;
  return 0;		
}

int 	do_select_pr_segment(void)
{
  if(updating_menu_state != 0)	return D_O_K;	
  prev_general_pr_action = general_pr_action;
  general_pr_action = find_selected_pr_segment;
  return 0;		
}

int 	do_select_pr_burst_segment(void)
{
  if(updating_menu_state != 0)	return D_O_K;	
  prev_general_pr_action = general_pr_action;
  general_pr_action = find_selected_pr_burst_segment;
  return 0;		
}
int 	do_select_pr_bump_segment(void)
{
  if(updating_menu_state != 0)	return D_O_K;	
  prev_general_pr_action = general_pr_action;
  general_pr_action = find_selected_pr_bump_segment;
  return 0;		
}


int	dump_bump_signal_to_files(void)
{
  register int i, k;
  static char file[256], *cs, save_dir[256];
  static int space = 32;
  int dsn, opi_n, cur_ds, dsnx, n_dat, imin, imax, nburst, start, end;
  pltreg *pr;
  O_p *opi, *opa = NULL, *opb = NULL, *opt;
  d_s *dsi, *dsbu, *dsza = NULL, *dszb = NULL, **datt, *dat2[2], *dsbuc, *dsic, *dsp;
  float max_y, min_y, x0, x1;
  float *xds, *yds, obj_cor = 1;
  int load_plt_file_in_pltreg(pltreg *pr, char *file, char *path);
	
	
  if(updating_menu_state != 0)	return D_O_K;		
	
	
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&opi) != 2)
    return win_printf("cannot find data");
  dsn = opi->cur_dat;
  opi_n = pr->cur_op;
  if (opi->dir != NULL)   sprintf(save_dir,opi->dir);

  i = win_scanf("Define extra space arround bump %d",&space);			
  if (i == CANCEL)	return OFF;
					
  for (i = 0, dsi = NULL; i < opi->n_dat; i++)
    {
      if (strstr(opi->dat[i]->source,"Z bead1 - Z bead2") != NULL)
	{
	  dsi = opi->dat[i];
	  break;
	}
    }	
  if (dsi == NULL)   return win_printf_OK("cannot find 2 beads data set!");
	
  for (cs = opi->filename; *cs != 0 && isdigit(*cs) == 0; cs++);
  sprintf(file,"traca%s",cs);				
  if (load_plt_file_in_pltreg(pr, file, opi->dir) == 0)
    {
      opa = pr->one_p;
      for (i = 0, dsza = NULL; i < opa->n_dat; i++)
	{
	  if (strstr(opa->dat[i]->source,"Z coordinate") != NULL)
	    {
	      dsza = opa->dat[i];
	      break;
	    }
	}	
    }
  sprintf(file,"tracb%s",cs);				
  if (load_plt_file_in_pltreg(pr, file, opi->dir) == 0)
    {
      opb = pr->one_p;
      for (i = 0, dszb = NULL; i < opb->n_dat; i++)
	{
	  if (strstr(opb->dat[i]->source,"Z coordinate") != NULL)
	    {
	      dszb = opb->dat[i];
	      break;
	    }
	}		
    }			
  refresh_plot(pr,opi_n);			
  for (k = 0; k < opi->n_dat; k++)
    {
      dsbu = opi->dat[k];
      if (strstr(dsbu->source,"Signal segment bump") != NULL)
	{
	  i = sscanf (dsbu->source,"Signal segment bump %d",&nburst); 
	  if (i != 1)	win_printf("could not find bump number!");
	  cs = strstr(dsbu->source,"Zcor ");
	  if (cs != NULL) i = sscanf (cs+4,"%f",&obj_cor);
	  if (i != 1)	win_printf("could not find objective correction!");
					
	  x0 = dsbu->xd[0];
	  x1 = dsbu->xd[dsbu->nx-1];
	  recover_points_index_in_ds(x0, x1, dsi, &imin, &imax);
	  find_ds_y_limits(dsi, imin, imax, &min_y, &max_y);
	  dat2[0] = dsi;
	  dat2[1] = dsbu;
	  cur_ds = opi->cur_dat;
	  n_dat = opi->n_dat;
	  datt = opi->dat;
	  opi->dat = dat2;
	  opi->n_dat = 2; 
	  opi->cur_dat = 0;
	  xds = dsi->xd;
	  yds = dsi->yd;
	  dsnx = dsi->nx;
	  opi->x_lo = dsi->xd[imin] - (dsi->xd[imax]-dsi->xd[imin])/10;
	  opi->x_hi = dsi->xd[imax] + (dsi->xd[imax]-dsi->xd[imin])/10;
	  opi->y_lo = min_y - (max_y - min_y)/10;
	  opi->y_hi =	max_y + (max_y - min_y)/10;		
	  start = ((imin - space) >= 0) ? imin - space : 0;
	  dsi->xd = dsi->xd + start;
	  dsi->yd = dsi->yd + start;
	  end = ((imax + space) > dsnx) ? dsnx - start : (imax + space) - start;
	  dsi->nx = dsi->ny = end;
	  opt = duplicate_plot_data(opi, NULL);

	  dsi->xd = xds;
	  dsi->yd = yds;
	  dsi->nx = dsi->ny = dsnx;
	  opi->dat = datt;
	  opi->n_dat = n_dat;
	  opi->cur_dat = cur_ds;
			
	  if (opt == NULL)	 return win_printf("cannot duplicate plot");
	  if (opt->filename)  free(opt->filename);
	  opt->filename = NULL;
	  opt->filename = my_sprintf(NULL, "bum%05d.gr",nburst);			
	  if (opt->dir) free(opt->dir);
	  opt->dir = NULL;
	  opt->dir = my_sprintf(NULL,save_dir);						
	  dsbuc = opt->dat[1];
	  dsic = opt->dat[0];
	  strupr(dsbuc->treatement);
	  dsbuc->history = NULL;
	  dsp = create_and_attach_one_ds(opt, dsic->nx, dsic->nx, 0);
	  if (dsp != NULL && dsza != NULL)
	    {
	      for (i = 0; i < dsp->nx; i++)
		{ 
		  dsp->xd[i] = dsi->xd[start+i];
		  dsp->yd[i] = 1000*opa->dy*dsza->yd[start+i]/opi->dy;
		}
	      inherit_from_ds_to_ds(dsp, dsza);
	      dsp->treatement = my_sprintf(NULL,"Start index %d",start);
	    }
	  dsp = create_and_attach_one_ds(opt, dsic->nx, dsic->nx, 0);
	  if (dsp != NULL && dszb != NULL)
	    {
	      for (i = 0; i < dsp->nx; i++)
		{ 
		  dsp->xd[i] = dsi->xd[start+i];
		  dsp->yd[i] = 1000*opb->dy*dszb->yd[start+i]/opi->dy;
		}
	      inherit_from_ds_to_ds(dsp, dszb);
	      dsp->treatement = my_sprintf(NULL,"Start index %d",start);
	    }

	  //add_data( pr, IS_ONE_PLOT, (void*)opt);	
	  //refresh_plot(pr,pr->n_op-1);			
	  //win_printf("aft dupli");		
	  //remove_data( pr, IS_ONE_PLOT, (void*)opt);	
	  //refresh_plot(pr,pr->n_op-1);			

	  //win_printf("bef save %s %s",opt->filename,backslash_to_slash(opt->dir));					
	  save_one_plot_data_bin(opt);
	  if (opi->dir != NULL)   sprintf(save_dir,opt->dir);	  

	  //win_printf("after save");					
	  free_one_plot(opt);
	  opt = NULL;
	}
    }
  return 0;
}



int	dump_burst_signal_to_files(void)
{
  register int i, j, k;
  char file[256], *cs, fullfile[512], path[512];
  const char *pa;
  static char save_dir[256];
  static int space = 32, nptp = 10, iter = 10, first = 1;
  int dsn, opi_n, cur_ds, dsnx, n_dat, imin, imax, nburst, start, end;
  pltreg *pr;
  O_p *opi, *opa = NULL, *opb = NULL, *opt;
  d_s *dsi, *dsbu, *dsza = NULL, *dszb = NULL, **datt, *dat2[2], *dsbuc, *dsic, *dsp;
  float max_y, min_y, x0, x1, tmp;
  double *ap = NULL, mean_y2;
  float *xds, *yds, obj_cor = 1, speed;
  int load_plt_file_in_pltreg(pltreg *pr, char *file, char *path);
	
  if(updating_menu_state != 0)	return D_O_K;		
	
  if (first == 1)
    {
      sprintf(save_dir,"p:\\thimothee\\select\\");
      first = 0;
    }	
	
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&opi) != 2)
    return win_printf_OK("cannot find data");
  dsn = opi->cur_dat;
  opi_n = pr->cur_op;
		
  i = win_scanf("nb of points in one polyline to measure velocity %d"
		"nb. of iter %d extra space arround burst %d saving directory %s"
		,&nptp,&iter,&space,save_dir);			
  if (i == CANCEL)	return OFF;
					
  for (i = 0, dsi = NULL; i < opi->n_dat; i++)
    {
      if (strstr(opi->dat[i]->source,"Z bead1 - Z bead2") != NULL)
	{
	  dsi = opi->dat[i];
	  break;
	}
    }	
  if (dsi == NULL)	
    return win_printf_OK("cannot find 2 beads data set!");
	
  sprintf(file,"\\traca%s",opi->filename+5);				
  if (load_plt_file_in_pltreg(pr, file, opi->dir) == 0)
    {
      opa = pr->one_p;
      for (i = 0, dsza = NULL; i < opa->n_dat; i++)
	{
	  if (strstr(opa->dat[i]->source,"Z coordinate") != NULL)
	    {
	      dsza = opa->dat[i];
	      break;
	    }
	}	
    }
  sprintf(file,"\\tracb%s",opi->filename+5);				
  if (load_plt_file_in_pltreg(pr, file, opi->dir) == 0)
    {
      opb = pr->one_p;
      for (i = 0, dszb = NULL; i < opb->n_dat; i++)
	{
	  if (strstr(opb->dat[i]->source,"Z coordinate") != NULL)
	    {
	      dszb = opb->dat[i];
	      break;
	    }
	}		
    }			
  refresh_plot(pr,opi_n);			
  for (k = 0; k < opi->n_dat; k++)
    {
      dsbu = opi->dat[k];
      if (strstr(dsbu->source,"Helicase segments burst") != NULL)
	{
	  i = sscanf (dsbu->source,"Helicase segments burst %d",&nburst); 
	  if (i != 1)	win_printf_OK("could not find burst number!");
	  cs = strstr(dsbu->source,"Zcor ");
	  if (cs != NULL) i = sscanf (cs+4,"%f",&obj_cor);
	  if (i != 1)	win_printf_OK("could not find objective correction!");
					
	  x0 = dsbu->xd[0];
	  x1 = dsbu->xd[dsbu->nx-1];
	  recover_points_index_in_ds(x0, x1, dsi, &imin, &imax);
	  find_ds_y_limits(dsi, imin, imax, &min_y, &max_y);
	  dat2[0] = dsi;
	  dat2[1] = dsbu;
	  cur_ds = opi->cur_dat;
	  n_dat = opi->n_dat;
	  datt = opi->dat;
	  opi->dat = dat2;
	  opi->n_dat = 2; 
	  opi->cur_dat = 0;
	  xds = dsi->xd;
	  yds = dsi->yd;
	  dsnx = dsi->nx;
	  opi->x_lo = dsi->xd[imin] - (dsi->xd[imax]-dsi->xd[imin])/10;
	  opi->x_hi = dsi->xd[imax] + (dsi->xd[imax]-dsi->xd[imin])/10;
	  opi->y_lo = min_y - (max_y - min_y)/10;
	  opi->y_hi =	max_y + (max_y - min_y)/10;		
	  start = ((imin - space) >= 0) ? imin - space : 0;
	  dsi->xd = dsi->xd + start;
	  dsi->yd = dsi->yd + start;
	  end = ((imax + space) > dsnx) ? dsnx - start : (imax + space) - start;
	  dsi->nx = dsi->ny = end;
	  opt = duplicate_plot_data(opi, NULL);
	  dsi->xd = xds;
	  dsi->yd = yds;
	  dsi->nx = dsi->ny = dsnx;
	  opi->dat = datt;
	  opi->n_dat = n_dat;
	  opi->cur_dat = cur_ds;
			
	  if (opt == NULL)	 return win_printf_OK("cannot duplicate plot");
	  free(opt->filename);
	  opt->filename = NULL;
	  opt->filename = my_sprintf(NULL, "hbu%05d.gr",nburst);			
	  free(opt->dir);
	  opt->dir = NULL;
	  opt->dir = my_sprintf(NULL,save_dir);						
	  dsbuc = opt->dat[1];
	  dsic = opt->dat[0];
	  strupr(dsbuc->treatement);
	  dsbuc->history = NULL;
	  dsp = create_and_attach_one_ds(opt, dsic->nx, dsic->nx, 0);
	  if (dsp != NULL && dsza != NULL)
	    {
	      for (i = 0; i < dsp->nx; i++)
		{ 
		  dsp->xd[i] = dsi->xd[start+i];
		  if (opa != NULL)    dsp->yd[i] = 1000*opa->dy*dsza->yd[start+i]/opi->dy;
		  else dsp->yd[i] = 1000*dsi->yd[start+i];
		}
	      if (dsza)  inherit_from_ds_to_ds(dsp, dsza);
	      else inherit_from_ds_to_ds(dsp, dsi);
	      dsp->treatement = my_sprintf(NULL,"Start index %d",start);
	    }
	  dsp = create_and_attach_one_ds(opt, dsic->nx, dsic->nx, 0);
	  if (dsp != NULL && dszb != NULL)
	    {
	      for (i = 0; i < dsp->nx; i++)
		{ 
		  dsp->xd[i] = dsi->xd[start+i];
		  if (opb != NULL)    dsp->yd[i] = 1000*opb->dy*dszb->yd[start+i]/opi->dy;
		  else dsp->yd[i] = 1000*dsi->yd[start+i];
		}
	      if (dszb)  inherit_from_ds_to_ds(dsp, dszb);
	      else inherit_from_ds_to_ds(dsp, dsi);
	      dsp->treatement = my_sprintf(NULL,"Start index %d",start);
	    }
			
	  for (j = 0, start = -1, end = 0; j < (dsbuc->nx/4); j++)
	    {
	      if (strlen(dsbuc->treatement) < j)
		return win_printf_OK("treatement is too short");
	      x0 = dsbuc->xd[(4*j)+2];
	      x1 = dsbuc->xd[(4*j)+3];
	      if (ap != NULL) free(ap);
	      ap = NULL;
	      recover_points_index_in_ds(x0, x1, dsic, &imin, &imax);
	      find_ds_y_limits(dsic, imin, imax, &min_y, &max_y);
	      fit_ds_to_xn_polynome(dsic, 2,dsic->xd[imin],dsic->xd[imax], 
				    min_y-1, max_y+1, &ap);	
	      for (i = imin, mean_y2 = 0; i < imax; i++)
		{
		  tmp = opt->dy * (dsic->yd[i] - ap[0] - ap[1] * dsic->xd[i]);
		  mean_y2 += tmp * tmp; 
		}
	      mean_y2 = ((imax - imin) > 0) ? mean_y2/(imax - imin) : mean_y2;
				
	      if ((dsbuc->treatement[j] == 'U') || (dsbuc->treatement[j] == 'N')
		  || (dsbuc->treatement[j] == 'Z'))
		{
		  dsbuc->yd[(4*j)+2] = ap[0] + ap[1] * dsbuc->xd[(4*j)+2];	
		  dsbuc->yd[(4*j)+3] = ap[0] + ap[1] * dsbuc->xd[(4*j)+3];	
		  speed = ap[1];
		}
	      else
		{
		  speed = dsbuc->yd[(4*j)+3]-dsbuc->yd[(4*j)+2];
		  if ((dsbuc->xd[(4*j)+3]-dsbuc->xd[(4*j)+2]) != 0)
		    speed /= dsbuc->xd[(4*j)+3]-dsbuc->xd[(4*j)+2];
		}


	      dsbuc->history = my_sprintf(dsbuc->history,
					  "\nSeg %c%d <V> %5.3g %s/%s dt %5.3g %s <P> = %5.3g %s\n<dy^2> %g %s^2"
					  ,dsbuc->treatement[j],j,obj_cor*speed*opi->dy/opi->dx, 
					  (opi->y_unit != NULL) ? opi->y_unit : " ",
					  (opi->x_unit != NULL) ? opi->x_unit : " ",
					  opi->dx*(dsbuc->xd[(4*j)+4]-dsbuc->xd[(4*j)+1]),
					  (opi->x_unit != NULL) ? opi->x_unit : " ",
					  obj_cor*opi->dy*(dsbuc->yd[(4*j)+4]-dsbuc->yd[(4*j)+1]),
					  (opi->y_unit != NULL) ? opi->y_unit : " ",mean_y2,
					  (opi->y_unit != NULL) ? opi->y_unit : " ");	
	      if ((dsbuc->treatement[j] != 'N') && (dsbuc->treatement[j] != 'n')
		  && (dsbuc->treatement[j] != 'O') && (dsbuc->treatement[j] != 'o'))
		{
		  start = (start < 0) ? (4*j) + 1 : start;
		  end = (4*j) + 4;
		}
	      if ((dsbuc->treatement[j] == 'U') || (dsbuc->treatement[j] == 'u')
		  || (dsbuc->treatement[j] == 'Z') || (dsbuc->treatement[j] == 'z'))
		{
		  dsp = add_polyline_ds(opt, dsic, imin, imax, nptp, iter);
		  if (dsp != NULL)
		    dsp->treatement = my_sprintf(NULL,"Segment %c%d polyline"
						 " fit over %d pts and %d iterations",dsbuc->treatement[j]
						 ,j,	nptp,iter);
		}				
	    }
	  dsbuc->history = my_sprintf(dsbuc->history,"\nBurst total duration %6.2g %s\n", 
				      opi->dx*(dsbuc->xd[end]-dsbuc->xd[start]),
				      (opi->x_unit != NULL) ? opi->x_unit : " ");
			
			
	  if (opt->dir == NULL)
	    {
	      pa = get_config_string("PLOT-GR-FILE","last_saved",NULL);	
	      if (pa != NULL)		extract_file_path(fullfile, 512, (char*)pa);
	      else			my_getcwd(fullfile, 512);
	      strcat(fullfile,"\\");
	    }
	  else 
	    {
	      strcpy(fullfile,opt->dir);
	      strcat(fullfile,"\\");
	    }
	  if (opt->filename == NULL)
	    {	
	      strcat(fullfile,"Untitled.gr");
	    }
	  else strcat(fullfile,opt->filename);
		
	  /*if (win_printf("filename :\n %s",backslash_to_slash(fullfile))==CANCEL) return D_O_K;*/
			
	  /*save_one_plot_data_bin(opt);
	   */
	  switch_allegro_font(1);
	  i = file_select_ex("Save plot (*.gr) in binary format", fullfile, "gr", 512, 0, 0);
	  switch_allegro_font(0);
	  set_config_string("PLOT-GR-FILE","last_saved",fullfile);		
	  extract_file_name(file, 256, fullfile);
	  extract_file_path(path, 512, fullfile);
	  strcat(path,"\\");
	  if (do_you_want_to_overwrite(fullfile, NULL, "Saving plot File") == OK)
	    {
	      save_one_plot_bin(opt, fullfile);
	      set_op_filename(opt, file);
	      set_op_path(opt, path);
	    }
	  free_one_plot(opt);
	  opt = NULL;
	}
    }
  return 0;
}

int	catch_burst_directory(char *fullpattern, int n_char)
{
  register int i;
  static char c_dir[512], *c = NULL;

  switch_allegro_font(1);
  if (c == NULL)
    {
      my_getcwd(c_dir, 512);
      strcat(c_dir,"\\");
      c = c_dir;
    }
  i = file_select_ex("Load plot (*.gr)", c_dir, "gr", 512, 0, 0);
  switch_allegro_font(0);
  if (i != 0) 	
    {
      sprintf(fullpattern,"%s",c_dir);	
      return 0;
    }
  else return 1;
  return 0;
}

double	chi2_of_bump(d_s *dsi, int start, int bump0, int bump1, int end, 
		     double *mean, double *bump)
{
  register int i, j, k;
  double chi2 = 0, m, mb, tmp;

  for (i = start, j = k = 0, m = mb = 0; i < end; i++)
    {
      if (i >= bump0 && i < bump1)
	{
	  mb += dsi->yd[i];
	  k++;
	}
      else
	{
	  m += dsi->yd[i];
	  j++;		
	}
    }
  mb = (k != 0) ? mb/k : mb;
  m = (j != 0) ? m/j : m;
  if (mean != NULL)	*mean = m;
  if (bump != NULL)	*bump = mb;
  for (i = start, j = 0, chi2 = 0; i < end; i++, j++)
    {
      tmp = dsi->yd[i];
      tmp -= (i >= bump0 && i < bump1) ?  mb : m;
      tmp *= tmp;
      chi2 += tmp;	
    }
  chi2 /= (j != 0) ? j : 1;
  return chi2;
} 


double 	exhaustive_bump_adjust2(d_s *dsi, d_s *dsb)
{
  register int i, j;
  int imin, imax, ib0b, ib1b, first, is, ie, ibump0, ibump1;
  double chi2 = 0, mean, bump, meanb = 0, bumpb = 0, chi2b = 0;

  recover_points_index_in_ds(dsb->xd[0], dsb->xd[5], dsi, &imin, &imax);
  recover_points_index_in_ds(dsb->xd[2], dsb->xd[3], dsi, &ibump0, &ibump1);	

  is = ibump0 - (ibump1 - ibump0);
  is = (is < imin) ? imin : is;
  ie = ibump1 + (ibump1 - ibump0);		
  ie = (ie > imax) ? imax : ie;
	
  ib0b = is + 1;
  ib1b = is + 1;
  for(i = is + 1, first = 1; i < ie - 2; i++)	
    { 
      display_title_message("ex bump i = %d max %d",i,ie-2);
      for(j = i + 1; j < ie-1; j++)	
	{ 	
	  chi2 = chi2_of_bump(dsi, imin, i, j, imax, &mean, &bump);
	  if ((first == 1) || chi2 < chi2b)
	    {
	      first = 0;
	      chi2b = chi2;
	      ib0b = i;
	      ib1b = j;
	      meanb = mean;
	      bumpb = bump;
	    }
	}
    }	
  dsb->xd[1] = dsb->xd[2] = dsi->xd[ib0b];
  dsb->xd[3] = dsb->xd[4] = dsi->xd[ib1b]; 	
  dsb->yd[0] = dsb->yd[1] = dsb->yd[4] = dsb->yd[5] = meanb;
  dsb->yd[2] = dsb->yd[3] = bumpb;	
  return chi2b;	
}

int	fit_giuseppe_bump(d_s *ds, int xi0, int xi1, int xi2, int xi3, int xi4, 
			  int xi5, double *y1,  double *y2, double *chi2)
{
  register int i;
  double ay1, ay2, by1, by2, a0, b0, dx, tmp1, tmp2, delta, tmp;
	
  if (ds == NULL)	return 1;
  if (xi0 < 0 || xi0 >= ds->nx) return 2;
  if (xi1 < xi0 || xi1 > ds->nx) return 2;
  if (xi2 < xi1 || xi2 > ds->nx) return 2;
  if (xi3 < xi2 || xi3 > ds->nx) return 2;
  if (xi4 < xi3 || xi4 > ds->nx) return 2;
  if (xi5 < xi4 || xi5 > ds->nx) return 2;
		
  /* a for d/dy1, b for d/dy2 */
  a0 = ay1 = ay2 = b0 = by1 = by2 = 0;
  for (i = xi0; i < xi1; i++)
    {
      a0 += -2 * ds->yd[i];
      ay1 += 2;  
    }		
  if (xi2 != xi1)
    {
      dx = ds->xd[xi2] - ds->xd[xi1];
      if (dx != 0)	dx = (float)1/dx;
      else return 3;
      for (i = xi1; i < xi2; i++)
	{
	  tmp1 = (ds->xd[i] - ds->xd[xi1]) * dx;
	  tmp2 = (ds->xd[xi2] - ds->xd[i]) * dx;
		
	  a0 += -2 * ds->yd[i] * tmp2;
	  b0 += -2 * ds->yd[i] * tmp1;
		
	  by1 += 2 * tmp2 * tmp1;
	  by2 += 2 * tmp1 * tmp1;
		
	  ay2 += 2 * tmp2 * tmp1;
	  ay1 += 2 * tmp2 * tmp2;		  
	}
    }

  for (i = xi2; i < xi3; i++)
    {
      b0 += -2 * ds->yd[i];
      by2 += 2;  
    }		
	
  if (xi4 != xi3)
    {	
      dx = ds->xd[xi4] - ds->xd[xi3];
      if (dx != 0)	dx = (float)1/dx;
      else return 3;
      for (i = xi3; i < xi4; i++)
	{
	  tmp1 = (ds->xd[i] - ds->xd[xi3]) * dx;
	  tmp2 = (ds->xd[xi4] - ds->xd[i]) * dx;
		
	  a0 += -2 * ds->yd[i] * tmp1;
	  b0 += -2 * ds->yd[i] * tmp2;
		
	  by1 += 2 * tmp2 * tmp1;
	  by2 += 2 * tmp2 * tmp2;
		
	  ay2 += 2 * tmp2 * tmp1;
	  ay1 += 2 * tmp1 * tmp1;		  
	}
    }	
	
  for (i = xi4; i < xi5; i++)
    {
      a0 += -2 * ds->yd[i];
      ay1 += 2;  
    }			
  delta = by2 * ay1 - ay2 * by1;
  if (delta == 0)	return 4;
  *y1 = -(by2 * a0 - ay2 * b0)/delta;
  *y2 = -(ay1 * b0 - by1 * a0)/delta;
  *chi2 = 0;
  for (i = xi0; i < xi1; i++)
    {
      tmp = *y1 - ds->yd[i];
      *chi2 += tmp * tmp;
    }	
  if (xi2 != xi1)
    {	
      dx = ds->xd[xi2] - ds->xd[xi1];
      if (dx != 0)	dx = (float)1/dx;
      else return 3;
      for (i = xi1; i < xi2; i++)
	{
	  tmp1 = (ds->xd[i] - ds->xd[xi1]) * dx;
	  tmp2 = (ds->xd[xi2] - ds->xd[i]) * dx;
	  tmp = ds->yd[i] - (*y2) * tmp1 - (*y1) * tmp2; 
	  *chi2 += tmp * tmp;
	}
    }
  for (i = xi2; i < xi3; i++)
    {
      tmp = *y2 - ds->yd[i];
      *chi2 += tmp * tmp;
    }	
  if (xi4 != xi3)
    {			
      dx = ds->xd[xi4] - ds->xd[xi3];
      if (dx != 0)	dx = (float)1/dx;
      else return 3;
      for (i = xi3; i < xi4; i++)
	{
	  tmp1 = (ds->xd[i] - ds->xd[xi3]) * dx;
	  tmp2 = (ds->xd[xi4] - ds->xd[i]) * dx;
	  tmp = ds->yd[i] - (*y1) * tmp1 - (*y2) * tmp2; 
	  *chi2 += tmp * tmp;
	}
    }	
  for (i = xi4; i < xi5; i++)
    {
      tmp = *y1 - ds->yd[i];
      *chi2 += tmp * tmp;
    }				
  return 0;
}


d_s		*adjust_bump_edge_on_op(O_p *op, int extend)
{
  register int i, j, k;
  d_s *ds, *dsi, *dsb = NULL; /*  *dsc; */
  char bead[256], ppath[256], pfile[256], heli[128];
  int nf, xi[6], nburst, np, np1, np2;
  float atp = 0, zmag = 0, obj_cor = 1;	
  time_t time;		
  static float factor = 0.1;
  double y2, y1, chi2, chi2_0;

	
  if (op == NULL)	return NULL;
  np = extend;	
	
  for (i = 0, dsi = NULL; i < op->n_dat; i++)
    {
      if (strstr(op->dat[i]->source,"Z bead1 - Z bead2") != NULL)
	{
	  dsi = op->dat[i];
	  break;
	}
    }	
  if (dsi == NULL)	
    {
      win_printf("cannot find 2 beads data set!");		
      return NULL;
    }
	
  for (k = 0, i = -1; k < op->n_dat && i == -1; k++)
    {
      dsb = op->dat[k];
      if (strstr(dsb->source,"Signal segment bump") != NULL)
	{
	  sscanf (dsb->source,"Signal segment bump %d time %lu\n"	
		  "Bead %s\npath %s file %s\nZmag %f mm enzyme %s [ATP] %f \\mu M"
		  " Zcor %f",&nburst,&time,bead,ppath,pfile,
		  &zmag,heli,&atp,&obj_cor); 
	  i = k;
	}
    }	
  nf = dsi->nx;	/* this is the number of points in the data set */
	
  if ((ds = create_and_attach_one_ds(op, 6, 6, 0)) == NULL)
    {
      win_printf("cannot create plot !");
      return NULL;
    }
		
# ifdef DEBUG		
  if ((dsc = create_and_attach_one_ds(op, 1024, 1024, 0)) == NULL)
    return win_printf("cannot create plot !");		
# endif
	
  recover_points_index_in_ds(dsb->xd[0], dsb->xd[5], dsi, xi, xi+5);	
  recover_points_index_in_ds(dsb->xd[1], dsb->xd[4], dsi, xi+1, xi+4);		
  recover_points_index_in_ds(dsb->xd[2], dsb->xd[3], dsi, xi+2, xi+3);
  if (xi[2] <= xi[1])	xi[1]--;
  if (xi[4] <= xi[3])	xi[3]--;		

	
  i = fit_giuseppe_bump(dsi, xi[0], xi[1], xi[2], xi[3], xi[4], xi[5], &y1, 
			&y2, &chi2_0);
  if (i)	win_printf("error in fiting bump");
  /*	win_printf("iter %d, xi[1] = %d xi[2] = %d\n\\chi_0^2 %g"
	"y1 %g y2 %g",-1, xi[1], xi[2],chi2_0,y1,y2);	*/
  np = xi[3] - xi[2];
  np = (int)(factor*2*np);
  for (j = 1, k = 0; (xi[1]-(j+1)/2) > (xi[0] + 10) && 
	 (xi[2]+j/2) < (xi[3] - 10) && j < np; j++)
    {
      i = fit_giuseppe_bump(dsi, xi[0], xi[1]-(j+1)/2, xi[2]+j/2, xi[3], xi[4], 
			    xi[5], &y1, &y2, &chi2);
      if (i)	win_printf("error in fiting bump");	
      /*		win_printf("iter %d, xi[1] = %d xi[2] = %d\n\\chi^2 %g  \\chi_0^2 %g"
			"\ny1 %g y2 %g",j, xi[1]-(j+1)/2, xi[2]+j/2,chi2,chi2_0,y1,y2);	*/
      if (chi2 <= chi2_0)		
	{
	  chi2_0 = chi2;
	  k = j;
	}
    }	
  xi[1] -= (k+1)/2;
  xi[2] += +k/2;
  np1 = k;
  i = fit_giuseppe_bump(dsi, xi[0], xi[1], xi[2], xi[3], xi[4], xi[5], &y1, 
			&y2, &chi2_0);
  if (i)	win_printf("error in fiting bump");		
	
  for (j = xi[0] + 4 - xi[1], k = 0; j < xi[3] - 4 - xi[2]; j++)
    {
      i = fit_giuseppe_bump(dsi, xi[0], xi[1]+j, xi[2]+j, xi[3], xi[4], 
			    xi[5], &y1, &y2, &chi2);
      if (i)	win_printf("error in fiting bump");	
      /*		win_printf("iter %d, xi[1] = %d xi[2] = %d\n\\chi^2 %g  \\chi_0^2 %g"
			"\ny1 %g y2 %g",j, xi[1]+j, xi[2]+j,chi2,chi2_0,y1,y2);			*/	
      if (chi2 <= chi2_0)		
	{
	  chi2_0 = chi2;
	  k = j;
	}
    }	
  xi[1] += k;
  xi[2] += k;
  i = fit_giuseppe_bump(dsi, xi[0], xi[1], xi[2], xi[3], xi[4], xi[5], &y1, 
			&y2, &chi2_0);
  if (i)	win_printf("error in fiting bump");			
	

  for (j = 1, k = 0; xi[3]-(j+1)/2 > xi[2] + 10 && 
	 xi[4]+j/2 < xi[5] - 10 && j < np; j++)
    {
      i = fit_giuseppe_bump(dsi, xi[0], xi[1], xi[2], xi[3]-(j+1)/2, xi[4]+j/2, 
			    xi[5], &y1, &y2, &chi2);
      if (i)	win_printf("error in fiting bump");	
      /*		win_printf("iter %d, xi[2] %d xi[3] = %d xi[4] = %d xi[5] %d\n"
			"\\chi^2 %g  \\chi_0^2 %g \ny1 %g y2 %g",j, xi[2],xi[3]-(j+1)/2, 
			xi[4]+j/2,xi[5],chi2,chi2_0,y1,y2);				*/
      if (chi2 <= chi2_0)		
	{
	  chi2_0 = chi2;
	  k = j;
	}
# ifdef DEBUG 		
      if (j < 1024)
	{
	  dsc->xd[j-1] = j;
	  dsc->yd[j-1] = chi2;
	  dsc->nx = dsc->ny = j;
	}
# endif
    }	
  xi[3] -= (k+1)/2;
  xi[4] += +k/2;
  np2 = k;		
  i = fit_giuseppe_bump(dsi, xi[0], xi[1], xi[2], xi[3], xi[4], xi[5], &y1, 
			&y2, &chi2_0);
  if (i)	win_printf("error in fiting bump");				
	

	
  for (j = xi[2] + 10 - xi[3], k = 0; j < xi[5] - 10 - xi[4]; j++)
    {
      i = fit_giuseppe_bump(dsi, xi[0], xi[1], xi[2], xi[3]+j, xi[4]+j, 
			    xi[5], &y1, &y2, &chi2);
      if (i)	win_printf("error in fiting bump");	
      /*		win_printf("iter %d, xi[1] = %d xi[2] = %d\n\\chi^2 %g  \\chi_0^2 %g"
			"\ny1 %g y2 %g",j, xi[1]+j, xi[2]+j,chi2,chi2_0,y1,y2);			*/
      if (chi2 <= chi2_0)		
	{
	  chi2_0 = chi2;
	  k = j;
	}
    }	
  xi[3] += k;
  xi[4] += k;
	
  i = fit_giuseppe_bump(dsi, xi[0], xi[1], xi[2], xi[3], xi[4], xi[5], &y1, 
			&y2, &chi2_0);
  if (i)	win_printf("error in fiting bump");			
	
	
  for (j = 0; j < 6; j++)
    {	
      ds->xd[j] = dsi->xd[xi[j]];
    }		
  ds->yd[0] = ds->yd[1] = y1;
  ds->yd[2] = ds->yd[3] = y2;
  ds->yd[4] = ds->yd[5] = y1;
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  ds->treatement = my_sprintf(ds->treatement,
			      "Rising and falling edges adjustment of bump %d extending over %d and %d",
			      nburst,np1,np2);

  return ds;
}




int	analyze_bump_signal_from_files(void)
{
  register int i, j, k;
  struct al_ffblk fb;	
  static char fullpattern[256], seg_pat[128];
  char file[256], path[256];
  char bead[256], prev_bead[256], ppath[256], pfile[256], heli[128];
  static int  first = 1, verbose = 1, rising = 32, adj_bump = 0, norm_bump = 0;
  static int extend = 32;
  int imin, imax, nburst, done, ib0, ib1, dtn = 0, w = 0, mid;
  int nfiles = 0, tfiles = 0, nsegs = 0, i_file = 0, nmol = 0, debug_lia = OK;
  pltreg *pr;
  double chi2;
  O_p *opi, *opt = NULL, *opp, *optt, *opr;
  O_p *oped, *opedt, *opebs; /* rising and falling edges */
  d_s *dsi, *dsbu = NULL, *dst, *dsp, *dstt, *dsr, *dsf, *dsrer, *dsfer;
  d_s *dsab, *dsam, *dsae;
  d_s *dsre, *dsfe, *dsrfb, *dsrt, *dsft, *dsbt, *dsbs; /* rising and falling edges and bump*/
  float x0, x1, tmp, atp = 0, zmag = 0, tmp2, dz, az;
  float obj_cor = 1, dt = 0;
  double *srer = NULL, *sfer = NULL, *sr = NULL, *sf = NULL;
  time_t time;	
  static float minb = 50, maxb = 150;
	
  if(updating_menu_state != 0)	return D_O_K;		
	
  if (first == 1)
    {
      sprintf(fullpattern,"v:\\giuseppe\\2003\\bump\\res\\*.gr");
      sprintf(seg_pat,"U");
      first = 0;
    }

  catch_burst_directory(fullpattern, 256);
  //minb *= 1000;
  //maxb *= 1000;
  i = win_scanf("Histogram grabber verbose mode %3d\n"
		"size in frames for averaging rising and fallig edge %5d\n"	
		"minimum bump size for averaging (in nm) %8f\n"
		"maximum bump size for averaging (in nm) %8f\n"
		"readjust bump %b "
		"normalize avg bump  %b\n"
		"extend for burst reshaping %6d\n"
		,&verbose,&rising,&minb,&maxb,&adj_bump,&norm_bump,&extend);
  if (i == CANCEL) return OFF;		
	
  //minb /= 1000;
  //maxb /= 1000;
	
  verbose = (verbose) ? OK : OFF;

  pr = create_pltreg_with_op(&opt, 1024, 1024, 0);
  dst = opt->dat[0];
  dst->source = Mystrdupre(dst->source,"bump action time");
  opt->filename = my_sprintf(NULL,"bump-time.gr");
  create_attach_select_y_un_to_op(opt, IS_SECOND, 0, 1, 0, 0, "s");		
  dst->nx = dst->ny = 0;	
  set_plot_symb(dst,"\\pt5\\oc ");
  set_dot_line(dst);			
  set_plot_y_title(opt, "working time");
  set_plot_x_title(opt, "events");
	
	
  opp = create_and_attach_one_plot(pr, 1024, 1024, 0);
  if (opp == NULL)		return win_printf("Cannot allocate plot");
  dsp = opp->dat[0];
  if (dsp == NULL)		return win_printf("cannot create plot !");
  dsp->source = Mystrdupre(dsp->source,"bump size");
  opp->filename = my_sprintf(NULL,"bump-size.gr");	
  dsp->nx = dsp->ny = 0;	
  create_attach_select_y_un_to_op(opp, IS_METER, 0, 1, -9, 0, "nm");
  set_plot_symb(dsp,"\\pt5\\oc ");
  set_dot_line(dsp);	
  set_plot_y_title(opp, "Bump size");
  set_plot_x_title(opp, "events");
	
  optt = create_and_attach_one_plot(pr, 1024, 1024, 0);
  if (optt == NULL)		return win_printf("Cannot allocate plot");
  dstt = optt->dat[0];
  if (dstt == NULL)		return win_printf("cannot create plot !");
  dstt->source = Mystrdupre(dstt->source,"\\chi^2");
  optt->filename = my_sprintf(NULL,"bump-quality.gr");
  dstt->nx = dstt->ny = 0;	
  set_plot_symb(dstt,"\\pt5\\oc ");
  set_dot_line(dstt);			
  set_plot_y_title(optt, "bump quality");
  set_plot_x_title(optt, "events");		
	
  opr = create_and_attach_one_plot(pr, rising, rising, 0);
  if (opr == NULL)		return win_printf("Cannot allocate plot");
  dsr = opr->dat[0];
  if (dsr == NULL)		return win_printf("cannot create plot !");
  dsr->source = Mystrdupre(dsr->source,"bump average rising edge");
  opr->filename = my_sprintf(NULL,"bump-shape.gr");	
  create_attach_select_y_un_to_op(opr, IS_METER, 0, 1, -9, 0, "nm");
  create_attach_select_x_un_to_op(opr, IS_SECOND, 0, 1, 0, 0, "s");			
  set_plot_y_title(opr, "Bump shape");
  set_plot_x_title(opr, "time");
  dsf = create_and_attach_one_ds(opr, rising, rising, 0);
  if (dsf == NULL)		return win_printf("cannot create plot !");
  dsf->source = Mystrdupre(dsf->source,"bump average falling edge");
  dsrer = create_and_attach_one_ds(opr, rising, rising, 0);
  if (dsrer == NULL)		return win_printf("cannot create plot !");
  dsrer->source = Mystrdupre(dsrer->source,"bump average rising edge error");
  dsfer = create_and_attach_one_ds(opr, rising, rising, 0);
  if (dsfer == NULL)		return win_printf("cannot create plot !");
  dsfer->source = Mystrdupre(dsfer->source,"bump average falling edge error");
  dsab = create_and_attach_one_ds(opr, 4*rising, 4*rising, 0);
  if (dsab == NULL)		return win_printf("cannot create plot !");
  dsab->source = Mystrdupre(dsab->source,"bump average beginning");
  dsam = create_and_attach_one_ds(opr, rising, rising, 0);
  if (dsam == NULL)		return win_printf("cannot create plot !");
  dsam->source = Mystrdupre(dsam->source,"bump average medium");
  dsae = create_and_attach_one_ds(opr, 4*rising, 4*rising, 0);
  if (dsae == NULL)		return win_printf("cannot create plot !");
  dsae->source = Mystrdupre(dsae->source,"bump average beginning");
				
  sfer = (double*)calloc(rising,sizeof(double));		
  srer = (double*)calloc(rising,sizeof(double));				
  sf = (double*)calloc(rising,sizeof(double));		
  sr = (double*)calloc(rising,sizeof(double));
					
  if (sfer == NULL || srer == NULL || sr == NULL || sf == NULL )		
    win_printf("cannot allocate memory !");	
				
				
  oped = create_and_attach_one_plot(pr, 1024, 1024, 0);
  if (oped == NULL)		return win_printf("Cannot allocate plot");
  dsre = oped->dat[0];
  if (dsre == NULL)		return win_printf("cannot create plot !");
  dsre->source = Mystrdupre(dsre->source,"Rising edge velocity");
  oped->filename = my_sprintf(NULL,"bump-edges.gr");
  dsre->nx = dsre->ny = 0;	
  set_plot_symb(dsre,"\\pt5\\oc ");
  set_dot_line(dsre);			
  set_plot_y_title(oped, "bump edges velocity");
  set_plot_x_title(oped, "events");						
  dsfe = create_and_attach_one_ds(oped, 1024, 1024, 0);
  if (dsfe == NULL)		return win_printf("cannot create plot !");
  dsfe->source = Mystrdupre(dsfe->source,"Falling edge velocity");	
  dsfe->nx = dsfe->ny = 0;	
  set_plot_symb(dsfe,"\\pt5\\oc ");
  set_dot_line(dsfe);				
	
  opedt = create_and_attach_one_plot(pr, 1024, 1024, 0);
  if (opedt == NULL)		return win_printf("Cannot allocate plot");
  dsrt = opedt->dat[0];
  if (dsrt == NULL)		return win_printf("cannot create plot !");
  dsrt->source = Mystrdupre(dsrt->source,"Bump Rising time");
  opedt->filename = my_sprintf(NULL,"bump-timing.gr");
  dsrt->nx = dsrt->ny = 0;	
  set_plot_symb(dsrt,"\\pt5\\oc ");
  set_dot_line(dsrt);			
  set_plot_y_title(opedt, "Bump Rising time");
  set_plot_x_title(opedt, "events");						
  dsft = create_and_attach_one_ds(opedt, 1024, 1024, 0);
  if (dsft == NULL)		return win_printf("cannot create plot !");
  dsft->source = Mystrdupre(dsft->source,"Bump Falling time");	
  dsft->nx = dsft->ny = 0;	
  set_plot_symb(dsft,"\\pt5\\oc ");
  set_dot_line(dsft);				
  dsbt = create_and_attach_one_ds(opedt, 1024, 1024, 0);
  if (dsbt == NULL)		return win_printf("cannot create plot !");
  dsbt->source = Mystrdupre(dsbt->source,"Bump Up time");	
  dsbt->nx = dsbt->ny = 0;	
  set_plot_symb(dsbt,"\\pt5\\oc ");
  set_dot_line(dsbt);						
	
	
  opebs = create_and_attach_one_plot(pr, 1024, 1024, 0);
  if (opebs == NULL)		return win_printf("Cannot allocate plot");
  dsbs = opebs->dat[0];
  if (dsbs == NULL)		return win_printf("cannot create plot !");
  dsbs->source = Mystrdupre(dsbs->source,"Bump size");
  opebs->filename = my_sprintf(NULL,"bump-size.gr");
  dsbs->nx = dsbs->ny = 0;	
  set_plot_symb(dsbs,"\\pt5\\oc ");
  set_dot_line(dsbs);			
  set_plot_y_title(opebs, "Bump size");
  set_plot_x_title(opebs, "events");		

  refresh_plot(pr,pr->cur_op);
  //d_draw_Op_proc(MSG_DRAW,the_dialog + 1,0);					
	
  //win_printf("looking for\n %s",backslash_to_slash(fullpattern));
  done = al_findfirst(fullpattern,&fb,0xFF); /* FA_DIREC|FA_RDONLY);*/
  while (done == 0)
    {
      // win_printf("treating %s",fb.name);
      if (extract_file_path(path, 256, fb.name) == NULL)
	extract_file_path(path, 256, fullpattern); 
      extract_file_name(file, 256, fb.name);
      display_title_message("loading %s\n in %s",backslash_to_slash(file),
			    backslash_to_slash(path));
      //win_printf("loading %s\n in %s",backslash_to_slash(file),
      //	backslash_to_slash(path));
      for (i = 0; path[i] != 0; i++);
      if (i)
	{
	  if (path[i-1] != '/' && path[i-1] != 92 && i < 255)
	    {
	      path[i] = 92; path[i+1] = 0;
	    }
	}	
      if (load_plt_file_in_pltreg(pr, file, path))
	return win_printf("error loading %s",file);
      opi = pr->one_p;
      //win_printf("loaded %s",opi->filename);
      tfiles++;

      for (i = 0, dsi = NULL; i < opi->n_dat && dsi == NULL; i++)
	{
	  if (opi->dat[i]->source != NULL && strstr(opi->dat[i]->source,"Z bead1 - Z bead2") != NULL)
	      dsi = opi->dat[i];
	}	
      if (dsi == NULL)	return win_printf("cannot find 2 beads data set!");		
      //else win_printf("find 2 beads data set %d!",i);		
						
      for (i = 0, dsrfb = NULL; i < opi->n_dat && dsrfb == NULL; i++)
	{
	  if (opi->dat[i]->treatement != NULL && strstr(opi->dat[i]->treatement,
							"Rising and falling edges adjustment of bump") != NULL)
	      dsrfb = opi->dat[i];
	}			
		
      if (dsrfb == NULL)   dsrfb = adjust_bump_edge_on_op(opi, extend);
	
      get_afine_param_from_unit(opi->yu[opi->c_yu], &az, &dz);
      dz *= 1e9;

      for (k = 0, i = -1; k < opi->n_dat && i == -1; k++)
	{
	  dsbu = opi->dat[k];
	  if (dsbu->source != NULL && strstr(dsbu->source,"Signal segment bump") != NULL)
	    {
	      sscanf (dsbu->source,"Signal segment bump %d time %lu\n"	
		      "Bead %s\npath %s file %s\nZmag %f mm enzyme %s [ATP] %f \\mu M"
		      " Zcor %f",&nburst,&time,bead,ppath,pfile,
		      &zmag,heli,&atp,&obj_cor); 
	      i = k;
	    }

	}
      if (i != -1)
	{
	  if (adj_bump)     chi2 = exhaustive_bump_adjust2(dsi, dsbu);		
	  strupr(bead);
	  if ((nmol == 0) || strcmp(bead,prev_bead) != 0)
	    {
	      strcpy(prev_bead,bead);
	      nmol++;
	    }
	  nsegs++;
	  i_file++;
	  /* we had bump height */
	  dsp->xd[dsp->nx] = dsp->nx;
	  //dsp->yd[dsp->nx] = opi->dy * obj_cor * (dsbu->yd[2] - dsbu->yd[0]);
	  dsp->yd[dsp->nx] = dz * obj_cor * (dsbu->yd[2] - dsbu->yd[0]);
	  dsp->nx++; dsp->ny++;
	  if (dsp->nx >= dsp->mx)
	    dsp = build_adjust_data_set(dsp, dsp->mx + 256, dsp->mx + 256); 

	  dsp->history = my_sprintf(dsp->history,
				    "pt. %d %s bump mol %d\n",dsp->nx-1,file,nmol);
	  /* we had bump duration */	
	  dst->xd[dst->nx] = dst->nx;
	  dst->yd[dst->nx] = opi->dx * (dsbu->xd[3] - dsbu->xd[2]);
	  dst->nx++; dst->ny++;
	  if (dst->nx >= dst->mx)
	    dst = build_adjust_data_set(dst, dst->mx + 256, dst->mx + 256); 			
	  dst->history = my_sprintf(dst->history,
				    "pt.%d %s bump mol %d\n",dst->nx-1,file,nmol);
			

	  x0 = dsbu->xd[0];
	  x1 = dsbu->xd[5];
	  recover_points_index_in_ds(x0, x1, dsi, &imin, &imax);
	  x0 = dsbu->xd[2];
	  x1 = dsbu->xd[3];
	  recover_points_index_in_ds(x0, x1, dsi, &ib0, &ib1);						
	  for (j = ib0, k = 0, tmp = 0;  j < ib1; j++)
	    {
	      tmp += (dsi->yd[j] - dsbu->yd[2])*(dsi->yd[j] - dsbu->yd[2]);
	      k++;
	    }
	  if (k > 0) tmp /= k;
	  tmp = sqrt(tmp);
	  if (k > 1) tmp /= sqrt(k-1);

	  dstt->xd[dstt->nx] = dstt->nx;
	  dstt->yd[dstt->nx] = dz*fabs(dsbu->yd[2] - dsbu->yd[0]);
	  if (tmp != 0) dstt->yd[dstt->nx] /= tmp; 
	  dstt->nx++; dstt->ny++;
	  if (dstt->nx >= dstt->mx)
	    dstt = build_adjust_data_set(dstt, dstt->mx + 256, dstt->mx + 256);
	  dstt->history = my_sprintf(dstt->history,
				     "pts. %d  %s bump mol %d\n",dstt->nx-1,file,nmol);		
						
	  if ((dz*(dsbu->yd[2] - dsbu->yd[0]) >= minb) && 
	      (dz*(dsbu->yd[2] - dsbu->yd[0]) < maxb) && ib1-ib0 > rising)
	    {
	      if (verbose == OK)
		verbose = win_printf("Bump %g found %g %g",
				     dsbu->yd[2] - dsbu->yd[0],minb,maxb);			
	      w = rising/2;
	      mid = (ib0+ib1)/2;
	      for( j = ib0 - w, k = 0; (j < ib0 + w) && (j < mid); j++, k++)
		{
		  if (j >= imin && j < imax)
		    {
		      tmp2 = dsi->yd[j] - dsbu->yd[0];
		      if (norm_bump && (dsbu->yd[2] - dsbu->yd[0]) != 0)
			tmp2 /=  dsbu->yd[2] - dsbu->yd[0];
		      dsr->yd[k] += tmp2;
		      dsr->xd[k] += 1;
		      sr[k] += tmp2;
		      srer[k] += tmp2*tmp2;
		    } 
		  if (j > imin && j < imax)
		    {
		      dt += dsi->xd[j] - dsi->xd[j-1];
		      dtn++;
		    } 					
		}  
	      for( j = ib1 - w, k = 0; (j < ib1 + w) && (j < imax); j++, k++)
		{
		  if (j >= mid && j >= imin && j < imax)
		    {
		      tmp2 = dsi->yd[j] - dsbu->yd[0];
		      if (norm_bump && (dsbu->yd[2] - dsbu->yd[0]) != 0)
			tmp2 /=  dsbu->yd[2] - dsbu->yd[0];
		      dsf->yd[k] += tmp2;
		      dsf->xd[k] += 1;
		      sf[k] += tmp2;
		      sfer[k] += tmp2*tmp2;
		    } 
		  if (j >= mid && j > imin && j < imax)
		    {
		      dt += dsi->xd[j] - dsi->xd[j-1];
		      dtn++;
		    } 					
		} 
	      for (j = 0; j < 4 * rising; j++)
		{
		  k = imin + j;
		  if (k < ib0)
		    {
		      tmp2 = dsi->yd[k] - dsbu->yd[0];
		      if (norm_bump && (dsbu->yd[2] - dsbu->yd[0]) != 0)
			tmp2 /=  dsbu->yd[2] - dsbu->yd[0];	
		      dsab->yd[j] += tmp2;
		      dsab->xd[j] += 1;						
		    }
		  k = j + imax - 4*rising;
		  if (k > ib1 && k < imax)
		    {
		      tmp2 = dsi->yd[k] - dsbu->yd[0];
		      if (norm_bump && (dsbu->yd[2] - dsbu->yd[0]) != 0)
			tmp2 /=  dsbu->yd[2] - dsbu->yd[0];	
		      dsae->yd[j] += tmp2;
		      dsae->xd[j] += 1;						
		    }									
				
		}				
	      for (j = 0; j < rising; j++)
		{
		  k = mid - w + j;
		  if (k > ib0 && k < ib1)
		    {
		      tmp2 = dsi->yd[k] - dsbu->yd[0];
		      if (norm_bump && (dsbu->yd[2] - dsbu->yd[0]) != 0)
			tmp2 /=  dsbu->yd[2] - dsbu->yd[0];
		      dsam->yd[j] += tmp2;
		      dsam->xd[j] += 1;						
		    }								
		}  				
	    }
	  else
	    {
	      if (verbose == OK)
		verbose = win_printf("Bump %g out of range %g %g",dz*(dsbu->yd[2] - dsbu->yd[0]),
				     minb,maxb);
	    }
	  if (dsrfb != NULL)
	    {
	      dsre->xd[dsre->nx] = dsre->nx;
	      //dsre->yd[dsre->nx] = opi->dy * obj_cor * (dsrfb->yd[2] - dsrfb->yd[1]);
	      dsre->yd[dsre->nx] = dz * obj_cor * (dsrfb->yd[2] - dsrfb->yd[1]);
	      if (dsrfb->xd[2] - dsrfb->xd[1] != 0)
		dsre->yd[dsre->nx] /= (dsrfb->xd[2] - dsrfb->xd[1]) * opi->dx;
	      else win_printf("zero denominator in rising edge");
	      dsre->nx++; dsre->ny++;
	      if (dsre->nx >= dsre->mx)
		dsre = build_adjust_data_set(dsre, dsre->mx + 256, dsre->mx + 256); 

	      dsre->history = my_sprintf(dsre->history,
					 "pt. %d %s bump mol %d\n",dsre->nx-1,file,nmol);	
				
	      dsrt->xd[dsrt->nx] = dsrt->nx;
	      dsrt->yd[dsrt->nx] = (dsrfb->xd[2] - dsrfb->xd[1]) * opi->dx;
	      dsrt->nx++; dsrt->ny++;
	      if (dsrt->nx >= dsrt->mx)
		dsrt = build_adjust_data_set(dsrt, dsrt->mx + 256, dsrt->mx + 256); 	    
	      dsrt->history = my_sprintf(dsrt->history,
					 "pt. %d %s bump mol %d\n",dsrt->nx-1,file,nmol);					
				
	      dsfe->xd[dsfe->nx] = dsfe->nx;
	      //dsfe->yd[dsfe->nx] = opi->dy * obj_cor * (dsrfb->yd[4] - dsrfb->yd[3]);
	      dsfe->yd[dsfe->nx] = dz * obj_cor * (dsrfb->yd[4] - dsrfb->yd[3]);
	      if (dsrfb->xd[4] - dsrfb->xd[3] != 0)
		dsfe->yd[dsfe->nx] /= (dsrfb->xd[4] - dsrfb->xd[3]) * opi->dx;
	      else win_printf("zero denominator in fallining edge");		
	      dsfe->nx++; dsfe->ny++;
	      if (dsfe->nx >= dsfe->mx)
		dsfe = build_adjust_data_set(dsfe, dsfe->mx + 256, dsfe->mx + 256); 

	      dsfe->history = my_sprintf(dsfe->history,
					 "pt. %d %s bump mol %d\n",dsfe->nx-1,file,nmol);		
				
	      dsft->xd[dsft->nx] = dsft->nx;
	      dsft->yd[dsft->nx] = (dsrfb->xd[4] - dsrfb->xd[3]) * opi->dx;
	      dsft->nx++; dsft->ny++;
	      if (dsft->nx >= dsft->mx)
		dsft = build_adjust_data_set(dsft, dsft->mx + 256, dsft->mx + 256);
	      dsft->history = my_sprintf(dsft->history,
					 "pt. %d %s bump mol %d\n",dsft->nx-1,file,nmol);
				
	      dsbt->xd[dsbt->nx] = dsbt->nx;
	      dsbt->yd[dsbt->nx] = (dsrfb->xd[3] - dsrfb->xd[2]) * opi->dx;
	      dsbt->nx++; dsbt->ny++;
	      if (dsbt->nx >= dsbt->mx)
		dsbt = build_adjust_data_set(dsbt, dsbt->mx + 256, dsbt->mx + 256);
	      dsbt->history = my_sprintf(dsbt->history,
					 "pt. %d %s bump mol %d\n",dsbt->nx-1,file,nmol);					
	      dsbs->xd[dsbs->nx] = dsbs->nx;
	      dsbs->yd[dsbs->nx] = opi->dy * obj_cor * (dsrfb->yd[2] - dsrfb->yd[1]);
	      dsbs->nx++; dsbs->ny++;
	      if (dsbs->nx >= dsbs->mx)
		dsbs = build_adjust_data_set(dsbs, dsbs->mx + 256, dsbs->mx + 256);
	      dsbs->history = my_sprintf(dsbs->history,
					 "pt. %d %s bump mol %d\n",dsbs->nx-1,file,nmol);					

				
	      if (debug_lia == OK)
		{
		  debug_lia = win_printf("Bump %s\n \\delta z = %g\n dt_r %g"
					 "dt_f %g\n v_r %g v_f %g\nsize %g",file,
					 opi->dy*obj_cor*(dsrfb->yd[2] - dsrfb->yd[1]),
					 (dsrfb->xd[2] - dsrfb->xd[1]) * opi->dx,
					 (dsrfb->xd[4] - dsrfb->xd[3]) * opi->dx,
					 dsre->yd[dsre->nx-1], dsfe->yd[dsre->nx-1],
					 dsbs->yd[dsbs->nx-1]);
		}
				
	    }	 			 			
	  nfiles++;	
	}
      /* refresh_plot(pr,0); */
      remove_data(pr, IS_ONE_PLOT,(void*)opi);
      done = al_findnext(&fb);
    }
  //win_printf("Closing file search");
  al_findclose(&fb);

  opp->title = my_sprintf(NULL, "\\stack{{Analysis of %s}"
			  "{\\pt8 with %d/%d files and %d mol}}", 
			  backslash_to_slash(fullpattern), nfiles,tfiles,nmol);
	
  opt->title = my_sprintf(NULL, "\\stack{{Analysis of %s}"
			  "{\\pt8 with %d/%d files and %d mol}}", 
			  backslash_to_slash(fullpattern), nfiles,tfiles,nmol);
	
  optt->title = my_sprintf(NULL, "\\stack{{Analysis of %s}"
			   "{\\pt8 with %d/%d files and %d mol}}", 
			   backslash_to_slash(fullpattern), nfiles,tfiles,nmol);
  dt = (dtn != 0) ? dt/dtn : dt;
  dt = (dt == 0) ? 0.04 : dt;
  for (j = 0; j < rising; j++)
    {
      if (verbose == OK)
	verbose = win_printf("pt %f nb of avg %g",j,dsr->xd[j]);
      dsr->yd[j] /= (dsr->xd[j] != 0) ? dsr->xd[j] : 1;
      dsf->yd[j] /= (dsf->xd[j] != 0) ? dsf->xd[j] : 1;
      sr[j] /= (dsr->xd[j] != 0) ? dsr->xd[j] : 1;
      sf[j] /= (dsf->xd[j] != 0) ? dsf->xd[j] : 1;
      srer[j] /= (dsr->xd[j] != 0) ? dsr->xd[j] : 1;
      sfer[j] /= (dsf->xd[j] != 0) ? dsf->xd[j] : 1;		
      srer[j] -= sr[j]*sr[j];
      sfer[j] -= sf[j]*sf[j];
      srer[j] /= (dsr->xd[j] > 1) ? dsr->xd[j]-1 : 1;
      sfer[j] /= (dsf->xd[j] > 1) ? dsf->xd[j]-1 : 1;			
      dsrer->yd[j] = sqrt(srer[j]);
      dsfer->yd[j] = sqrt(sfer[j]);
      dsam->yd[j] /= (dsam->xd[j] != 0) ? dsam->xd[j] : 1;
      dsam->xd[j] = dt * (j + rising);
      dsrer->xd[j] = dsr->xd[j] = dt * (j - w);
      dsfer->xd[j] = dsf->xd[j] = dt * (j + w + 2*rising);
    }
  for (j = 0; j < 4*rising; j++)
    {
      dsab->yd[j] /= (dsab->xd[j] != 0) ? dsab->xd[j] : 1;
      dsae->yd[j] /= (dsae->xd[j] != 0) ? dsae->xd[j] : 1;
      dsab->xd[j] = dt * (j - 5*rising);
      dsae->xd[j] = dt * (j + 4*rising);							
    }	
  free(sf); free(sr); free(sfer); free(srer);	
   pr->one_p->need_to_refresh = 1;
  return refresh_plot(pr,pr->cur_op);
}	
int	check_spe(O_p *opbn, float f0, float spea, float sprethres)
{
  register int i, j;
  d_s *ds;
  float tmp, s, r = 0;
		
	
  ds = opbn->dat[opbn->n_dat - 1];
  for (i = j = 0; i < ds->nx; i++)
    {
      if (ds->xd[i] > 0)
	{ 
	  s = spea * ( 1 + (f0*f0)/(ds->xd[i]*ds->xd[i]));
	  tmp = ds->yd[i]/s;
	  r += tmp;
	  j++;
	}
    } 
  if (j > 0)	r /= j;
  return (r > sprethres) ? 1 : 0;
}


int	analyze_burst_signal_from_files(void)
{
  register int i, j, k;
  struct al_ffblk fb;	
  static char fullpattern[512], seg_pat[128];
  char file[256], *cs, path[256], search[128], ch1;
  char bead[256], prev_bead[256], ppath[256], pfile[256], heli[128];
  static int occ = 1, seg_pos = 0, first = 1, verbose = 0, molnb = -1, do_check = 1;
  int imin, imax, nburst, done, seg_i, nptp, iter, ns, pb, uzu = 0;
  static int anptp = 8, aiter = 10, donoise = 1, cam_fil = 1, frame = 0, w_flag = 1, doanomalous = 1, keep_all = 0;
  int nfiles = 0, tfiles = 0, nsegs = 0, i_file = 0, nmol = 0, doit = 0, filter;
  pltreg *pr;
  static float factor = 0.97, vmin_burst = 15, vmax_burst = 45, vb_min = 10, vb_max = 60;
  O_p *opi = NULL, *opv = NULL, *opt = NULL, *opp, *opvr = NULL, *optt, *opbn = NULL, *optmp, *opbf = NULL;
  d_s *dsi, *dsbu = NULL, *dsv = NULL, *dst, *dsp, *dss, *dsvr = NULL, *dstt, *dsi1, *dsi2, *dsbf = NULL, *dsf;
  d_s *dshn = NULL, *dsvru = NULL, *dsvrz = NULL; 
  float x0, x1, tmp, atp = 0, zmag = 0, t0, t1, vb, dtb, bpb, dt, meanf, meanb;
  float obj_cor = 1,Vm, Vp, Vpp, Vmp, max_y, min_y;
  static float vpmp = 30, ffilter = 1.0, f0 = 2 , spea = .3, sprethres = 3, max_fix_noise = 1.5;
  double *ap = NULL;
  float v_burst_avg = 0, n_burst_avg = 0, v_burst_tmp;
  time_t time;	
  int n_opi, keep_plot = 0;
  int load_plt_file_in_pltreg1(pltreg *pr, char *file, char *path);
  int load_plt_file_in_pltreg(pltreg *pr, char *file, char *path);	
  int	sft_real_noise_op(pltreg *pr, O_p *op, d_s *dsi, O_p **ops, int start, 
			  int end, int cam_fil, int frame, float factor, int w_flag, int verbose);
  d_s *sft_low_pass_filer_real(O_p *op, d_s *dsi, int filter, int w_flag);
			
  	

  if(updating_menu_state != 0)	return D_O_K;		

  /*if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");*/
  
  if (first == 1)
    {
      sprintf(fullpattern,"f:\\helicase\\burst\\*.gr");
      sprintf(seg_pat,"U");
      first = 0;
    }
	

  catch_burst_directory(fullpattern, 512);	
  
  /*if(win_printf("path :\n %s",backslash_to_slash(fullpattern))==CANCEL) return D_O_K;*/

  i = win_scanf("Histogram grabber segment pattern %10s\n"
		"position of segment of interest in pattern %4d\n"	
		"find first occurence only (otherwise all occurences) %b\n verbose %b"
		"nb. of pts to find speed %4d nb of iter %5d\n"
		"mol nb. (-1 all) %5d check burst (No %R, Yes %r,"
		" recompute dZ %r)\n",
		seg_pat,&seg_pos,&occ,&verbose,&anptp,&aiter,
		&molnb,&do_check);
  if (i == CANCEL) return D_O_K;		
	
  verbose = (verbose) ? OK : OFF;
	
  if (seg_pos >= strlen(seg_pat))
    return win_printf_OK("your segment position is impossible!");
  	
  pr = create_and_register_new_plot_project(0, 32, 900, 700);	
  opv = create_and_attach_one_plot(pr, 1024, 1024, 0);
  remove_data(pr,IS_ONE_PLOT,(void*)(pr->o_p[0]));
  dsv = opv->dat[0];
  dsv->source = Mystrdupre(dsv->source,"Helicase velocity");
  dsv->nx = dsv->ny = 0;	
  opv->filename = my_sprintf(NULL,"heli-velo.gr");
  create_attach_select_y_un_to_op(opv, 0, 0, 1, 0, 0, "nm/s");
  set_plot_symb(dsv,"\\pt5\\oc ");
  set_dot_line(dsv);			
  set_plot_y_title(opv, "velocity");
  set_plot_x_title(opv, "events");
	
	
  opt = create_and_attach_one_plot(pr, 1024, 1024, 0);
  if (opt == NULL)		return win_printf_OK("Cannot allocate plot");
  dst = opt->dat[0];
  if (dst == NULL)		return win_printf_OK("cannot create plot !");
  dst->source = Mystrdupre(dst->source,"Helicase action time");
  opt->filename = my_sprintf(NULL,"heli-time.gr");
  create_attach_select_y_un_to_op(opt, IS_SECOND, 0, 1, 0, 0, "s");		
  dst->nx = dst->ny = 0;	
  set_plot_symb(dst,"\\pt5\\oc ");
  set_dot_line(dst);			
  set_plot_y_title(opt, "working time");
  set_plot_x_title(opt, "events");
	
	

	
		
  opp = create_and_attach_one_plot(pr, 1024, 1024, 0);
  if (opp == NULL)		return win_printf_OK("Cannot allocate plot");
  dsp = opp->dat[0];
  if (dsp == NULL)		return win_printf_OK("cannot create plot !");
  dsp->source = Mystrdupre(dsp->source,"Helicase processivity");
  opp->filename = my_sprintf(NULL,"heli-pro.gr");	
  dsp->nx = dsp->ny = 0;	
  create_attach_select_y_un_to_op(opp, IS_METER, 0, 1, -9, 0, "nm");
  set_plot_symb(dsp,"\\pt5\\oc ");
  set_dot_line(dsp);	
  set_plot_y_title(opp, "processivity");
  set_plot_x_title(opp, "events");
	
  optt = create_and_attach_one_plot(pr, 1024, 1024, 0);
  if (optt == NULL)		return win_printf_OK("Cannot allocate plot");
  dstt = optt->dat[0];
  if (dstt == NULL)		return win_printf_OK("cannot create plot !");
  dstt->source = Mystrdupre(dstt->source,"Helicase total action time");
  optt->filename = my_sprintf(NULL,"heli-ttime.gr");
  create_attach_select_y_un_to_op(optt, IS_SECOND, 0, 1, 0, 0, "s");		
  dstt->nx = dstt->ny = 0;	
  set_plot_symb(dstt,"\\pt5\\oc ");
  set_dot_line(dstt);			
  set_plot_y_title(optt, "total working time");
  set_plot_x_title(optt, "events");		
	
  if (strstr(seg_pat,"UZU") != NULL)
    {
      i = win_scanf("Minimum processivity required for UZU burst %f",&vpmp);
      opvr = create_and_attach_one_plot(pr, 1024, 1024, 0);
      if (opvr == NULL)		return win_printf_OK("Cannot allocate plot");
      dsvr = opvr->dat[0];
      if (dsvr == NULL)		return win_printf_OK("cannot create plot !");
      dsvr->source = Mystrdupre(dsvr->source,"Helicase V_u/V_u in UZU bursts");
      opvr->filename = my_sprintf(NULL,"heli-pro.gr");	
      dsvr->nx = dsvr->ny = 0;	
      set_plot_symb(dsvr,"\\pt5\\oc ");
      set_dot_line(dsvr);	
      set_plot_y_title(opvr, "V_u/V_u");
      set_plot_x_title(opvr, "events");	
      uzu = 1;		
    }
  else if (strstr(seg_pat,"UZ") != NULL)
    {
      i = win_scanf("Minimum processivity required for UZ burst %f",&vpmp);
      opvr = create_and_attach_one_plot(pr, 1024, 1024, 0);
      if (opvr == NULL)		return win_printf_OK("Cannot allocate plot");
      dsvr = opvr->dat[0];
      if (dsvr == NULL)		return win_printf_OK("cannot create plot !");
      dsvr->source = Mystrdupre(dsvr->source,"Helicase processivity");
      opvr->filename = my_sprintf(NULL,"heli-pro.gr");	
      dsvr->nx = dsvr->ny = 0;	
      dsvru = create_and_attach_one_ds(opvr, 1024, 1024, 0);
      dsvrz = create_and_attach_one_ds(opvr, 1024, 1024, 0);
      dsvru->nx = dsvru->ny = 0;	dsvrz->nx = dsvrz->ny = 0;	
      set_plot_symb(dsvr,"\\pt5\\oc ");
      set_dot_line(dsvr);	
      set_plot_symb(dsvru,"\\pt5\\oc ");
      set_dot_line(dsvru);
      set_plot_symb(dsvrz,"\\pt5\\oc ");
      set_dot_line(dsvrz);						
      set_plot_y_title(opvr, "V+/V-");
      set_plot_x_title(opvr, "events");	
    }	
  else if (strstr(seg_pat,"ZU") != NULL)
    {
      i = win_scanf("Minimum processivity required for ZU burst %f",&vpmp);
      opvr = create_and_attach_one_plot(pr, 1024, 1024, 0);
      if (opvr == NULL)		return win_printf_OK("Cannot allocate plot");
      dsvr = opvr->dat[0];
      if (dsvr == NULL)		return win_printf_OK("cannot create plot !");
      dsvr->source = Mystrdupre(dsvr->source,"Helicase processivity");
      opvr->filename = my_sprintf(NULL,"heli-pro.gr");	
      dsvr->nx = dsvr->ny = 0;	
      set_plot_symb(dsvr,"\\pt5\\oc ");
      set_dot_line(dsvr);	
      set_plot_y_title(opvr, "V+/V-");
      set_plot_x_title(opvr, "events");		
    }	
	

  if (seg_pat[0] == 'U' || seg_pat[0] == 'Z' || seg_pat[0] == 'N')
    {
      i = win_scanf("do burst noise analysis %b\n" 
		    "camera correction %b"
		    "mode (selected frame, Field otherwise) %b\n"
		    "camera field factor %6f windowing  %b\n"
		    "velocity to analyze burst Minimum %6f"
		    "Maximum %6f\n"
		    "Max noise variance for ref beead (in nm)%6f\nkeep all plots %b\n"
		    ,&donoise,&cam_fil,&frame,&factor,&w_flag,&vmin_burst,&vmax_burst,
		    &max_fix_noise,&keep_all);  
      if (donoise)
	{
	  win_scanf("checking for anomalous noise (1-> yes, 0-> No)%d"
		    "Noise predicted to be A(f) = a(1 + {f/f_0}^2)\n"
		    "indicate f_0 %f possible a %f threshold ratio %f"
		    ,&doanomalous,&f0,&spea,&sprethres);
	  opbf = create_and_attach_one_plot(pr, 1024, 1024, 0);
	  if (opbf == NULL)		return win_printf_OK("Cannot allocate plot");
	  dsbf = opbf->dat[0];
	  if (dsbf == NULL)		return win_printf_OK("cannot create plot !");
	  dsbf->source = Mystrdupre(dsbf->source,"Helicase fixed bead noise");
	  opbf->filename = my_sprintf(NULL,"heli-fnoise.gr");
	  dsbf->nx = dsbf->ny = 0;	
	  set_plot_symb(dsbf,"\\pt5\\oc ");
	  set_dot_line(dsbf);			
	  set_plot_y_title(opbf, "noise (nm)");
	  set_plot_x_title(opbf, "events");	
	  dshn = create_and_attach_one_ds(opbf, 1024, 1024, 0);
	  if (dshn == NULL)		return win_printf_OK("Cannot allocate plot");	
	  dshn->source = Mystrdupre(dshn->source,"Helicase noise");
	  dshn->nx = dshn->ny = 0;	
	  set_plot_symb(dshn,"\\pt5\\oc ");
	  set_dot_line(dshn);										
	}				
				
    }
  if (do_check == 1)
    {
      i = win_scanf("do burst check\nMinimum burst velocity abs. value%f"
		    "Maximum burst velocity abs. value%f",&vb_min,&vb_max);
      if (i == CANCEL)	return OFF;	
    }
  if (do_check == 2)
    {
      i = win_scanf("Recompute \\Delta z between moving bead and fixed one\n"
		    "using a low-pass filter on fixed bead at frequency (in Hz) %f",
		    &ffilter);
      if (i == CANCEL)	return OFF;	
    }

	
  do_one_plot(pr);			
			
  done = al_findfirst(fullpattern,&fb,0xFF); /* FA_DIREC|FA_RDONLY);*/
  while ( !done )
    {
      keep_plot = keep_all;
      dsf = NULL;
      if (extract_file_path(path, 256, fb.name) == NULL)
	extract_file_path(path, 256, fullpattern); 
      extract_file_name(file, 256, fb.name);
      display_title_message("loading %s\n in %s",backslash_to_slash(file),
			    backslash_to_slash(path));
      for (i = 0; path[i] != 0; i++);
      if (i)
	{
	  if (path[i-1] != '/' && path[i-1] != 92 && i < 255)
	    {
	      path[i] = 92; path[i+1] = 0;
	    }
	}	
      if (load_plt_file_in_pltreg(pr, file, path)) // was load_plt_file_in_pltreg1
	return win_printf_OK("error loading %s",file);
      opi = pr->one_p;
      tfiles++;
      n_opi = pr->cur_op;
      for (i = 0, dsi = NULL; i < opi->n_dat; i++)
	{
	  if (strstr(opi->dat[i]->source,"Z bead1 - Z bead2") != NULL)
	    {
	      dsi = opi->dat[i];
	      break;
	    }
	}	
      if (dsi == NULL)	return win_printf_OK("cannot find 2 beads data set!");		
      else if (verbose == OK) verbose = win_printf("find 2 beads data set!");		
      for (i = j = 0; i < opi->n_dat && j != 4; i++)
	{
	  if (opi->dat[i]->treatement != NULL)
	    {
	      j = sscanf(opi->dat[i]->treatement,"Segment %c%d polyline"
			 " fit over %d pts and %d",&ch1,&nburst,&nptp,&iter);
	      //if (j == 4) win_printf("i %d Segment %c%d polyline"
	      //	 " fit over %d pts and %d",i,ch1,nburst,nptp,iter);
	    }
	}			
      if (j < 4) win_printf_OK("could not find bust parameters");
      else if (verbose == OK) verbose = win_printf("find bust parameters");
      for (i = 0, dsi1 = dsi2 = NULL; i < opi->n_dat; i++)
	{
	  if (strstr(opi->dat[i]->source,"Z coordinate") != NULL)
	    {
	      if (dsi1 == NULL) dsi1 = opi->dat[i];
	      dsi2 = opi->dat[i];
	    }
	}	
							
      if (dsi1 == NULL || dsi2 == NULL) win_printf_OK("could not find Z coordinate");
      else if (verbose == OK) verbose = win_printf("find Z coordinate");	
						
      for (k = 0, i = -1; k < opi->n_dat && i == -1; k++)
	{
	  dsbu = opi->dat[k];
	  if (strstr(dsbu->source,"Helicase segments burst") != NULL)
	    {
	      sscanf (dsbu->source,"Helicase segments burst %d time %lu\n"
		      "Bead %s\npath %s file %s\n"
		      "Zmag %f mm helicase %s [ATP] %f \\mu M Zcor %f"
		      ,&nburst,&time,bead,ppath,pfile,
		      &zmag,heli,&atp,&obj_cor); 
	      i = k;
	    }
	}
      if (i == -1) win_printf_OK("Helicase segments burst not found");
      if (i != -1)
	{
	  strupr(bead);
	  if ((nmol == 0) || strcmp(bead,prev_bead) != 0)
	    {
	      strcpy(prev_bead,bead);
	      nmol++;
	    }
	  cs = strstr(dsbu->history,"Seg ");
	  for(k = 0; (cs != NULL) && (do_check == 1); k++)
	    {
	      pb = 0;
	      if (sscanf(cs,"Seg %c%d <V> %f nm/s dt  %f s <P> = %f nm",
			 &ch1,&ns,&vb,&dtb,&bpb) == 5)
		{
		  if (ch1 == 'U') 
		    pb = ((vb < vb_min) || (vb > vb_max)) ? pb + 1 : pb;	
		  if (ch1 == 'Z') 
		    pb = ((vb < -vb_max) || (vb > -vb_min)) ? pb + 1 : pb;	
		  if (ch1 == 'N') 
		    pb = ((vb < -vb_min) || (vb > vb_min)) ? pb + 1 : pb;	
		  if (pb != 0)
		    {	
		      //activate_acreg(Term);
		      //text_printf(txterr,"strange burst in file %s "
		      win_printf("strange burst in file %s "
				 "seg %c%d <V> %f nm/s dt  %f s <P> = %f nm\n",
				 backslash_to_slash(file),ch1,ns,vb,dtb,bpb);			
		    }
		}
	      else win_printf("pb in burst in file");
	      if (verbose == OK) verbose = win_printf("burst in file %s "
						      "seg %c%d <V> %f nm/s dt  %f s <P> = %f nm\n",
						      backslash_to_slash(file),ch1,ns,vb,dtb,bpb);			
	      cs = strstr(cs+3,"Seg ");
	    }
	  if (do_check == 2)
	    {
	      dt = opi->dx * (dsi->xd[dsi->nx-1] - dsi->xd[0]);
	      filter = (int)(ffilter * dt);
	      if (filter < 5) filter = 5;
	      display_title_message("slow fft over %d filter at %d",dsi->nx,filter);
	      dsf = NULL;
	      dsf = sft_low_pass_filer_real(opi, dsi2, filter, 1); 
	      for (j = 0; j < dsi->nx; j++)
		dsi->yd[j] = dsi1->yd[j] - dsf->yd[j];
	      /*				free_data_set(dss);
						dss = NULL;*/
	 
	    }				
															
	}
      if (molnb  < 0) doit = 1;
      else if (nmol == molnb) doit = 1;
      else doit = 0;
		 
		
      /*		refresh_plot(pr,n_opi);*/
		
      if (i != -1 && doit == 1)
	{
			
	  for (k = 0, i_file = 0, t0 = t1 = -1;  k < strlen(dsbu->treatement); k++)
	    {
	      //win_printf("%d looking for %s to %g t1 %g",k, dsbu->treatement+k,t0,t1);
	      cs = dsbu->treatement + k;
	      if (t0 == -1)
		{	
		  if (dsbu->treatement[k] == 'U' || dsbu->treatement[k] == 'Z' )
		    {
		      //win_printf("DSBU nx %d %g",dsbu->nx,dsbu->xd[0]);
		      t0 = dsbu->xd[((4*k)+1)%dsbu->nx]; 
		      //win_printf("DSBU nx %d k %d %g",dsbu->nx,k,dsbu->xd[((4*k)+1)%dsbu->nx]);
		      //win_printf_OK("file %s t0 %g",file,t0);
		      if (verbose == OK)
			verbose = win_printf("file %s t0 %g",file,t0);
		      //win_printf_OK("after verbose file %s t0 %g \nOK = %d verbose %d",file,t0,OK,verbose);
		    }
		}
	      if (t1 == -1 && t0 != -1)
		{	
		  if (dsbu->treatement[k] == 'H' || dsbu->treatement[k] == 'N' )
		    {
		      t1 = dsbu->xd[((4*k)+1)%dsbu->nx];
		      if (verbose == OK)
			verbose = win_printf("file %s t1 %g t1-t0 %g"
					     ,file,t1,t1-t0);
		    }						
		}				
	      if (verbose == OK)
		verbose = win_printf("begin comparison cs %s with seg_pat %s",cs,seg_pat);
	      if (strncmp(cs,seg_pat,strlen(seg_pat)) == 0)
		{
		  if (verbose == OK)
		    verbose = win_printf("comparison %s with %s OK",cs,seg_pat);
		  nsegs++;
		  i_file++;
		  seg_i = 4*(seg_pos+k) + 1;
		  dsp->xd[dsp->nx] = dsp->nx;
		  dsp->yd[dsp->nx] = opi->dy * obj_cor *
		    (dsbu->yd[seg_i + 3] - dsbu->yd[seg_i]);
		  dsp->nx++; dsp->ny++;
		  if (dsp->nx >= dsp->mx)
		    dsp = build_adjust_data_set(dsp, dsp->mx + 256, dsp->mx + 256); 

		  dsp->history = my_sprintf(dsp->history,"pt. %d %s seg %c%d mol %d\n",dsp->nx-1,file,
					    dsbu->treatement[k+seg_pos],seg_pos+k,nmol);
		  dst->xd[dst->nx] = dst->nx;
		  dst->yd[dst->nx] = opi->dx * 
		    (dsbu->xd[seg_i + 3] - dsbu->xd[seg_i]);
		  dst->nx++; dst->ny++;
		  if (dst->nx >= dst->mx)
		    dst = build_adjust_data_set(dst, dst->mx + 256, dst->mx + 256); 				
		  dst->history = my_sprintf(dst->history,"pt.%d %s seg %c%d mol %d\n",dst->nx-1,file,	
					    dsbu->treatement[k+seg_pos],seg_pos+k,nmol);
		  // win_printf("2");	
		  if ((dsbu->treatement[k+seg_pos] == 'U') || 
		      (dsbu->treatement[k+seg_pos] == 'Z'))
		    {
		      sprintf(search,"Segment %c%d polyline fit over",
			      dsbu->treatement[k+seg_pos],k+seg_pos);
		      //win_printf("searching for %s",search);
		      for (j = 0, dss = NULL; j < opi->n_dat; j++)
			{
			  if (opi->dat[j]->treatement != NULL && 
			      strncmp(opi->dat[j]->treatement,search,strlen(search)) == 0)
			    {
			      dss = opi->dat[j];
			      sscanf(dss->treatement+strlen(search),
				     "%d pts and %d",&nptp,&iter);
			      break;
			    }
			}	
		      if (dss == NULL) win_printf("searching for %s failed",search);	//------------------------		
		      x0 = dsbu->xd[seg_i + 1];
		      x1 = dsbu->xd[seg_i + 2];
		      recover_points_index_in_ds(x0, x1, dsi, &imin, &imax);		
		      if (nptp != anptp || iter != aiter)
			{
			  dss = add_polyline_ds(opi, dsi, imin, imax, anptp, aiter);
			  if (dss != NULL)
			    dss->treatement = my_sprintf(NULL,"Segment %c%d polyline"
							 " fit over %d pts and %d iterations",
							 dsbu->treatement[k+seg_pos],j,	anptp,aiter);
			  else win_printf("polyline ds not generated");
			}
		      if (dss != NULL)
			{
			  if (verbose == OK)
			    verbose = win_printf("found polyline ds\n%s",dss->treatement);
			  for (j = 0;  j < dss->nx - 1; j++)
			    {
			      tmp = opi->dx * (dss->xd[j+1] - dss->xd[j]);
			      if (tmp != 0)	tmp = (dss->yd[j+1] - dss->yd[j])/tmp;
			      dsv->xd[dsv->nx] = dsv->nx;
			      dsv->yd[dsv->nx] = opi->dy * tmp * obj_cor;
			      dsv->nx++; dsv->ny++;
			      if (dsv->nx >= dsv->mx)
				dsv = build_adjust_data_set(dsv,  dsv->mx + 256, dsv->mx + 256);
			    }
			  dsv->history = my_sprintf(dsv->history,
						    "pts. %d to %d %s seg %c%d mol %d\n",dsv->nx-dss->nx,
						    dsv->nx-1,file,dsbu->treatement[k+seg_pos],seg_pos+k,
						    nmol);					
			}	
		      else if (verbose == OK)
			verbose = win_printf_OK("could not find polyline ds %s\n%s"
						,search,dss->treatement);

		      Vp = dsbu->yd[seg_i + 2] - dsbu->yd[seg_i + 1];
		      Vp *= opi->dy * obj_cor;
		      tmp = opi->dx * (dsbu->xd[seg_i + 2] - dsbu->xd[seg_i + 1]);	
		      Vp /= (tmp != 0) ? tmp : 1;
						 	
		      if (ap != NULL) free(ap);
		      ap = NULL;		
		      if (dsf != NULL)
			{				
			  find_ds_y_limits(dsf, imin, imax, &min_y, &max_y);
			  fit_ds_to_xn_polynome(dsf, 2,dsf->xd[imin],
						dsf->xd[imax], min_y-1, max_y+1, &ap);	
			  for (j = imin, meanf = 0;  j < imax; j++)
			    {
			      tmp = dsf->yd[j];
			      tmp -= ap[0] + ap[1] * dsi2->xd[j];
			      tmp *= obj_cor;
			      meanf += tmp*tmp;						
			    }										
			}	
		      else
			{				
			  find_ds_y_limits(dsi2, imin, imax, &min_y, &max_y);
			  fit_ds_to_xn_polynome(dsi2, 2,dsi2->xd[imin],
						dsi2->xd[imax], min_y-1, max_y+1, &ap);		
			  for (j = imin, meanf = 0;  j < imax; j++)
			    {
			      tmp = dsi2->yd[j];
			      tmp -= ap[0] + ap[1] * dsi2->xd[j];
			      tmp *= obj_cor;
			      meanf += tmp*tmp;						
			    }
			}
		      if (imax - imin > 0) meanf /= (imax - imin);
		      meanf = opi->dy * sqrt(meanf);
		      if ((donoise == 1) && (Vp >= vmin_burst) && 
			  (Vp < vmax_burst) && (meanf < max_fix_noise))
			{
			  dsbf->xd[dsbf->nx] = nburst;
			  dsbf->yd[dsbf->nx] = meanf;
			  dsbf->nx++; dsbf->ny++;
			  if (dsbf->nx >= dsbf->mx)
			    dsbf = build_adjust_data_set(dsbf, dsbf->mx + 256, dsbf->mx + 256);
			  for (j = imin;  j < imax; j++)
			    {
			      dsi->yd[j] -= dsbu->yd[seg_i + 1];
			      tmp = ((imax-imin-1) > 0) ? (float)(j-imin)/(imax-imin-1) : 1;
			      dsi->yd[j] -=  tmp * (dsbu->yd[seg_i + 2] - dsbu->yd[seg_i + 1]);
			      dsi->yd[j] *= obj_cor;
			    }
			  v_burst_tmp = (dsbu->yd[seg_i + 2] - dsbu->yd[seg_i + 1]);
			  v_burst_tmp *= obj_cor;
			  tmp = (dsbu->xd[seg_i + 2] - dsbu->xd[seg_i + 1]);
			  if (tmp > 0) v_burst_tmp /= tmp;
			  sft_real_noise_op(pr, opi, dsi, &opbn, imin, imax, 
					    cam_fil, frame, factor, w_flag, 0);	
			  (opbn->dat[opbn->n_dat-1])->treatement = 
			    my_sprintf((opbn->dat[opbn->n_dat-1])->treatement,
				       "\nBurst %d %c %d",nburst,dsbu->treatement[k+seg_pos],k+seg_pos);
			  for (j = imin, meanb = 0;  j < imax; j++)
			    {
			      tmp = dsi->yd[j];
			      tmp *= obj_cor;
			      meanb += tmp*tmp;						
			    }	
			  if (imax - imin > 0) meanb /= (imax - imin);	
			  dshn->xd[dshn->nx] = nburst;
			  dshn->yd[dshn->nx] = opi->dy * sqrt(meanb);
			  dshn->nx++; dshn->ny++;
			  if (dshn->nx >= dshn->mx)
			    dshn = build_adjust_data_set(dshn, dshn->mx + 256,	dshn->mx + 256);     
			  if (doanomalous)
			    {
			      if (check_spe(opbn, f0, spea,sprethres))
				{
				  keep_plot = win_printf("Strange spectrum at burst %d\n"
							 "do you want to keep this plot aside",nburst);
				  keep_plot = (keep_plot == OK) ? 1 : 0;
				  if (keep_plot)
				    {
				      remove_one_plot_data (opbn, IS_DATA_SET, 
							    (void *)opbn->dat[opbn->n_dat-1]);
				      optmp = NULL;
				      sft_real_noise_op(pr, opi, dsi, &optmp, 
							imin, imax, cam_fil, frame, factor, w_flag, 0);
				    }
				  else 
				    {
				      v_burst_avg += v_burst_tmp * (imax - imin);
				      n_burst_avg += (imax - imin);
				    }
				}
			      else 
				{
				  v_burst_avg += v_burst_tmp * (imax - imin);
				  n_burst_avg += (imax - imin);
				}
			    }
			  else
			    {
			      if (keep_plot)
				{
				  remove_one_plot_data (opbn, IS_DATA_SET, 
							(void *)opbn->dat[opbn->n_dat-1]);
				  optmp = NULL;
				  sft_real_noise_op(pr, opi, dsi, &optmp, 
						    imin, imax, cam_fil, frame, factor, w_flag, 0);
				}
			      v_burst_avg += v_burst_tmp * (imax - imin);
			      n_burst_avg += (imax - imin);
			    }			
			}	
	
		      if ((donoise == 2) && (Vp >= vmin_burst) && (Vp < vmax_burst))
			{
			  if (ap != NULL) free(ap);
			  ap = NULL;						
			  find_ds_y_limits(dsi1, imin, imax, &min_y, &max_y);
			  fit_ds_to_xn_polynome(dsi1, 2,dsi1->xd[imin],
						dsi1->xd[imax], min_y-1, max_y+1, &ap);
			  for (j = imin;  j < imax; j++)
			    {
			      dsi1->yd[j] -= ap[0] + ap[1] * dsi1->xd[j];
			      dsi1->yd[j] *= obj_cor;
			    }
			  sft_real_noise_op(pr, opi, dsi1, &opbn, imin, imax, 
					    cam_fil, frame, factor, w_flag, 0);
			}						
		      if ((donoise == 3) && (Vp >= vmin_burst) && (Vp < vmax_burst))
			{
			  if (ap != NULL) free(ap);
			  ap = NULL;						
			  find_ds_y_limits(dsi2, imin, imax, &min_y, &max_y);
			  fit_ds_to_xn_polynome(dsi2, 2,dsi2->xd[imin],
						dsi2->xd[imax], min_y-1, max_y+1, &ap);
			  for (j = imin;  j < imax; j++)
			    {
			      dsi2->yd[j] -= ap[0] + ap[1] * dsi2->xd[j];
			      dsi2->yd[j] *= obj_cor;
			    }
			  sft_real_noise_op(pr, opi, dsi2, &opbn, imin, imax, 
					    cam_fil, frame, factor, w_flag, 0);
			}						
						
		      if ((dsbu->treatement[k+seg_pos] == 'U') && dsvr != NULL)
			{
			  Vp = Vm = Vpp = Vmp = 0;
			  j = 0;
			  sprintf(search,"Seg U%d <V>",k+seg_pos);	
			  cs = strstr(dsbu->history,search);
			  if (cs != NULL) 
			    {
			      j = sscanf(cs+strlen(search),"%f",&Vp);			
			      cs = strstr(cs,"<P> =");
			      sscanf(cs+5,"%f",&Vpp);	
			      Vpp = fabs(Vpp);
			    }					
			  if (uzu)
			    {
			      sprintf(search,"Seg U%d <V>",k+seg_pos+2);
			      cs = strstr(dsbu->history,search);
			      if (cs == NULL)
				{
				  sprintf(search,"Seg U%d <V>",k+seg_pos-2);
				  cs = strstr(dsbu->history,search);				
				}
			      if (cs != NULL) 
				{
				  j += sscanf(cs+strlen(search),"%f",&Vm);		
				  cs = strstr(cs,"<P> =");
				  sscanf(cs+5,"%f",&Vmp);	
				  Vmp = fabs(Vmp);
				}
			    }	
			  else
			    {
			      sprintf(search,"Seg Z%d <V>",k+seg_pos+1);
			      cs = strstr(dsbu->history,search);
			      if (cs == NULL)
				{
				  sprintf(search,"Seg Z%d <V>",k+seg_pos-1);
				  cs = strstr(dsbu->history,search);				
				}
			      if (cs != NULL) 
				{
				  j += sscanf(cs+strlen(search),"%f",&Vm);		
				  cs = strstr(cs,"<P> =");
				  sscanf(cs+5,"%f",&Vmp);
				  Vmp = fabs(Vmp);
				}									
			    }
			  if ((j == 2) && (Vm != 0) && (Vpp > vpmp) && (Vmp > vpmp))
			    {
			      dsvr->xd[dsvr->nx] = dsvr->nx;
			      dsvr->yd[dsvr->nx] = Vp/Vm;
			      dsvr->nx++; dsvr->ny++;
			      if (dsvr->nx >= dsvr->mx)
				dsvr = build_adjust_data_set(dsvr, dsvr->mx + 256, dsvr->mx + 256);	
			      dsvr->history = my_sprintf(dsvr->history,
							 "pt. %d %s seg %c%d mol %d\n",dsvr->nx-1,file,
							 dsbu->treatement[k+seg_pos],seg_pos+k,nmol);
			      if (dsvru != NULL)
				{
				  dsvru->xd[dsvru->nx] = dsvru->nx;
				  dsvru->yd[dsvru->nx] = Vp;
				  dsvru->nx++; dsvru->ny++;
				  if (dsvru->nx >= dsvru->mx)
				    dsvru = build_adjust_data_set(dsvru,dsvru->mx + 256, dsvru->mx + 256);
				  dsvru->history = my_sprintf(dsvru->history,
							      "pt. %d %s seg %c%d mol %d\n",dsvru->nx-1,file,
							      dsbu->treatement[k+seg_pos],seg_pos+k,nmol);
				}	
			      if (dsvrz != NULL)
				{								
				  dsvrz->xd[dsvrz->nx] = dsvrz->nx;
				  dsvrz->yd[dsvrz->nx] = Vm;
				  dsvrz->nx++; dsvrz->ny++;
				  if (dsvrz->nx >= dsvrz->mx)
				    dsvrz = build_adjust_data_set(dsvrz, dsvrz->mx + 256, dsvrz->mx + 256);
				  dsvrz->history = my_sprintf(dsvrz->history,
							      "pt. %d %s seg %c%d mol %d\n",dsvrz->nx-1,file,
							      dsbu->treatement[k+seg_pos],seg_pos+k,nmol);
				}
			    }
			}
		    }
		  else if (dsbu->treatement[k+seg_pos] == 'H')
		    {
		      x0 = dsbu->xd[seg_i + 1];
		      x1 = dsbu->xd[seg_i + 2];
		      recover_points_index_in_ds(x0, x1, dsi, &imin, &imax);
		      for (j = imin; j < imax - 1 && j < dsi->nx - 1; j++)
			{
			  dsv->xd[dsv->nx] = dsv->nx;
			  tmp = opi->dx * (dsi->xd[j+1] - dsi->xd[j]);
			  if (tmp != 0)	    tmp = (dsi->yd[j+1] - dsi->yd[j])/tmp;
			  dsv->yd[dsv->nx] = opi->dy * obj_cor * tmp;
			  dsv->nx++; dsv->ny++;
			  if (dsv->nx >= dsv->mx)
			    dsv = build_adjust_data_set(dsv, dsv->mx + 256, dsv->mx + 256);
			}				
		      dsv->history = my_sprintf(dsv->history,
						"pts. %d to %d %s seg %c%d mol %d\n",dsv->nx-imax+imin,
						dsv->nx-1,file,dsbu->treatement[k+seg_pos],seg_pos+k,nmol);
		    }
		  else if (dsbu->treatement[k+seg_pos] == 'N')
		    {
		      x0 = dsbu->xd[seg_i + 1];
		      x1 = dsbu->xd[seg_i + 2];
		      recover_points_index_in_ds(x0, x1, dsi, &imin, &imax);
		      dss = add_polyline_ds(opi, dsi, imin, imax, anptp, aiter);
		      if (dss != NULL)
			dss->treatement = my_sprintf(NULL,"Segment %c%d polyline"
						     " fit over %d pts and %d iterations",
						     dsbu->treatement[k+seg_pos],j,anptp,aiter);
		      if (verbose == OK)
			verbose = win_printf("Noise polyline data set added with"
					     " %d points\nimin %d imax %d nptp %d iter %d",
					     dss->nx-1,imin, imax, anptp, aiter);
		      for (j = 0;  j < dss->nx - 1; j++)
			{
			  tmp = opi->dx * (dss->xd[j+1] - dss->xd[j]);
			  if (tmp != 0)	    tmp = (dss->yd[j+1] - dss->yd[j])/tmp;
			  dsv->xd[dsv->nx] = dsv->nx;
			  dsv->yd[dsv->nx] = opi->dy * tmp * obj_cor;
			  dsv->nx++; dsv->ny++;
			  if (dsv->nx >= dsv->mx)
			    dsv = build_adjust_data_set(dsv, dsv->mx + 256, dsv->mx + 256);
			}						
		      dsv->history = my_sprintf(dsv->history,
						"pts. %d to %d %s seg %c%d mol %d\n",dsv->nx-dss->nx,
						dsv->nx-1,file,dsbu->treatement[k+seg_pos],seg_pos+k,nmol);
		      Vp = dsbu->yd[seg_i + 2] - dsbu->yd[seg_i + 1];
		      Vp *= opi->dy * obj_cor;
		      tmp = opi->dx * (dsbu->xd[seg_i + 2] - dsbu->xd[seg_i + 1]);
		      Vp /= (tmp != 0) ? tmp : 1;
						
		      if ((donoise == 1) && (Vp >= vmin_burst) && (Vp < vmax_burst))
			{
			  for (j = imin;  j < imax; j++)
			    {
			      dsi->yd[j] -= dsbu->yd[seg_i + 1];
			      tmp = ((imax-imin-1) > 0) ? (float)(j-imin)/(imax-imin-1) : 1;
			      dsi->yd[j] -= tmp * (dsbu->yd[seg_i + 2] - dsbu->yd[seg_i + 1]);
			      dsi->yd[j] *= obj_cor;
			    }
			  sft_real_noise_op(pr, opi, dsi, &opbn, imin, imax, 
					    cam_fil, frame, factor, w_flag, 0);	
			  (opbn->dat[opbn->n_dat-1])->treatement = 
			    my_sprintf((opbn->dat[opbn->n_dat-1])->treatement,
				       "\nBurst %d N %d",nburst,k+seg_pos);			
			  if (doanomalous)
			    {
			      if (check_spe(opbn, f0, spea,sprethres))
				win_printf("Strange spectrum at burst %d",nburst); 
			    }
			}	
	
		      else if ((donoise == 2) && (Vp >= vmin_burst) && (Vp < vmax_burst))
			{
			  if (ap != NULL) free(ap);
			  ap = NULL;						
			  find_ds_y_limits(dsi1, imin, imax, &min_y, &max_y);
			  fit_ds_to_xn_polynome(dsi1, 2,dsi1->xd[imin],
						dsi1->xd[imax], min_y-1, max_y+1, &ap);
			  for (j = imin;  j < imax; j++)
			    {
			      dsi1->yd[j] -= ap[0] + ap[1] * dsi1->xd[j];
			      dsi1->yd[j] *= obj_cor;
			    }
			  sft_real_noise_op(pr, opi, dsi1, &opbn, imin, imax, 
					    cam_fil, frame, factor, w_flag, 0);
			}						
		      else if ((donoise == 3) && (Vp >= vmin_burst) && (Vp < vmax_burst))
			{
			  if (ap != NULL) free(ap);
			  ap = NULL;						
			  find_ds_y_limits(dsi2, imin, imax, &min_y, &max_y);
			  fit_ds_to_xn_polynome(dsi2, 2,dsi2->xd[imin],
						dsi2->xd[imax], min_y-1, max_y+1, &ap);	
			  for (j = imin;  j < imax; j++)
			    {
			      dsi2->yd[j] -= ap[0] + ap[1] * dsi2->xd[j];
			      dsi2->yd[j] *= obj_cor;
			    }
			  sft_real_noise_op(pr, opi, dsi2, &opbn, imin, imax, 
					    cam_fil, frame, factor, w_flag, 0);
			}						
		      else
			{
			  if (verbose == OK)
			    verbose = win_printf("Rejected noise analysis\n V %g"
						 "min %g max %g",Vp,vmin_burst,vmax_burst);
			}

		    }					
		  if (occ == 1) break;
		}
	      else if (verbose == OK)
		verbose = win_printf("comparison %s with %s failed", cs,seg_pat);
	    }
	  if (t1 == -1 && t0 != -1)
	    {	
	      t1 = dsbu->xd[dsbu->nx-1];
	    }	
	  if (t1 != -1 && t0 != -1)
	    {
	      dstt->xd[dstt->nx] = dstt->nx;
	      dstt->yd[dstt->nx] = opi->dx * (t1 - t0);
	      dstt->nx++; dstt->ny++;
	      if (dstt->nx >= dstt->mx)
		dstt = build_adjust_data_set(dstt, dstt->mx + 256, dstt->mx + 256); 			
	      dstt->history = my_sprintf(dstt->history,"pt. %d %s seg %c%d mol %d\n",
					 dstt->nx-1,file,dsbu->treatement[k+seg_pos],seg_pos+k,nmol);
	    }
	  if (i_file)	nfiles++;	
	} 
      /* refresh_plot(pr,0); */
      if (keep_plot == 0) remove_data(pr, IS_ONE_PLOT,(void*)opi);
      done = al_findnext(&fb);
      /*		if (nfiles > 375) done=1; */
    }
  if (verbose == OK) verbose = win_printf("end of file loop");
  al_findclose(&fb);
  opv->title = my_sprintf(NULL, "\\stack{{Analysis of %s}"
			  "{\\pt8 pattern  %s pos %d}{\\pt8 with %d/%d files and %d segments"
			  " %d mol}}", 
			  backslash_to_slash(fullpattern), seg_pat, seg_pos, nfiles,tfiles,nsegs,nmol);
	
  opp->title = my_sprintf(NULL, "\\stack{{Analysis of %s}"
			  "{\\pt8 pattern  %s pos %d}{\\pt8 with %d/%d files and %d segments"
			  " %d mol}}", 
			  backslash_to_slash(fullpattern), seg_pat, seg_pos, nfiles,tfiles,nsegs,nmol);
	
  opt->title = my_sprintf(NULL, "\\stack{{Analysis of %s}"
			  "{\\pt8 pattern  %s pos %d}{\\pt8 with %d/%d files and %d segments"
			  " %d mol}}", 
			  backslash_to_slash(fullpattern), seg_pat, seg_pos, nfiles,tfiles,nsegs,nmol);
	
  optt->title = my_sprintf(NULL, "\\stack{{Analysis of %s}"
			   "{\\pt8 pattern  %s pos %d}{\\pt8 with %d/%d files and %d segments"
			   " %d mol}}", 
			   backslash_to_slash(fullpattern), seg_pat, seg_pos, nfiles,tfiles,nsegs,nmol);

  if (dsvr != NULL)
    {
      opvr->title = my_sprintf(NULL, "\\stack{{Analysis of %s}"
			       "{\\pt8 pattern  %s pos %d}{\\pt8 with %d/%d files and %d segments"
			       " %d mol min processivity %g}}", 
			       backslash_to_slash(fullpattern), seg_pat, seg_pos, nfiles,tfiles,nsegs,
			       nmol,vpmp);	
    }		
		
  if (opbn != NULL)
    {
      v_burst_avg /= (n_burst_avg != 0) ? n_burst_avg : 1;
      if (opbn->title != NULL) 	free(opbn->title);
      v_burst_avg *= opi->dy;
      v_burst_avg /= opi->dx;
      opbn->title = my_sprintf(NULL, "Noise with avg V = %g",v_burst_avg);
    }	
		
		
  do_one_plot(pr);
  //return 	display_plot_region(pr);		
  return refresh_plot(pr,pr->cur_op);
}	


int	analyze_NUNZN_burst_signal_from_files(void)
{
  register int i, j, k;
  struct al_ffblk fb;	
  static char fullpattern[256], seg_pat[128];
  char file[256], path[256];
  char bead[256], prev_bead[256], ppath[256], pfile[256], heli[128];
  static int  first = 1, verbose = 1, rising = 32, adj_bump = 0, norm_bump = 0;
  static int extend = 32;
  int imin, imax, nburst, done, ib0, ib1, dtn = 0, w = 0, mid;
  int nfiles = 0, tfiles = 0, nsegs = 0, i_file = 0, nmol = 0, debug_lia = OK;
  int i_ud, i_ue, i_zd, i_ze, i_u, i_z;
  pltreg *pr;
  O_p *opi, *opt = NULL, *opp, *optt, *opr;
  O_p *oped, *opedt, *opebs; /* rising and falling edges */
  d_s *dsi, *dsbu = NULL, *dst, *dsp, *dstt, *dsr, *dsf, *dsrer, *dsfer;
  d_s *dsab, *dsam, *dsae;
  d_s *dsre, *dsfe, *dsrt, *dsft, *dsbt, *dsbs; /* rising and falling edges and bump*/
  float x0, x1, tmp, atp = 0, zmag = 0, tmp2, az, dz;
  float obj_cor = 1, dt = 0;
  double *srer = NULL, *sfer = NULL, *sr = NULL, *sf = NULL;
  time_t time;	
  static float minb = 50, maxb = 150, minrft = 80;
	
  if(updating_menu_state != 0)	return D_O_K;		
	
  if (first == 1)
    {
      sprintf(fullpattern,"v:\\giuseppe\\2003\\bump\\res\\*.gr");
      sprintf(seg_pat,"U");
      first = 0;
    }

  catch_burst_directory(fullpattern, 256);
  //	minb *= 1000;
  //maxb *= 1000;
  i = win_scanf("Histogram grabber verbose mode %3d\n"
		"size in frames for averaging rising and fallig edge %5d\n"	
		"minimum bump size for averaging (in nm) %8f\n"
		"maximum bump size for averaging (in nm) %8f\n"
		"minimum bump rising or falling time (in ms) %8f\n"
		"readjust bump %b "
		"normalize avg bump  %b\n"
		"extend for burst reshaping %6d\n"
		,&verbose,&rising,&minb,&maxb,&minrft,&adj_bump,&norm_bump,&extend);
  if (i == CANCEL) return OFF;		
	
  //minb /= 1000;
  //maxb /= 1000;
	
  verbose = (verbose) ? OK : OFF;

  pr = create_and_register_new_plot_project(0, 32, 900, 700);	
  opt = create_and_attach_one_plot(pr, 1024, 1024, 0);
  remove_data(pr,IS_ONE_PLOT,(void*)(pr->o_p[0]));
  dst = opt->dat[0];
  dst->source = Mystrdupre(dst->source,"bump action time");
  opt->filename = my_sprintf(NULL,"bump-time.gr");
  create_attach_select_y_un_to_op(opt, IS_SECOND, 0, 1, 0, 0, "s");		
  dst->nx = dst->ny = 0;	
  set_plot_symb(dst,"\\pt5\\oc ");
  set_dot_line(dst);			
  set_plot_y_title(opt, "working time");
  set_plot_x_title(opt, "events");
	
	
  opp = create_and_attach_one_plot(pr, 1024, 1024, 0);
  if (opp == NULL)		return win_printf_OK("Cannot allocate plot");
  dsp = opp->dat[0];
  if (dsp == NULL)		return win_printf_OK("cannot create plot !");
  dsp->source = Mystrdupre(dsp->source,"bump size");
  opp->filename = my_sprintf(NULL,"bump-size.gr");	
  dsp->nx = dsp->ny = 0;	
  create_attach_select_y_un_to_op(opp, IS_METER, 0, 1, -9, 0, "nm");
  set_plot_symb(dsp,"\\pt5\\oc ");
  set_dot_line(dsp);	
  set_plot_y_title(opp, "Bump size");
  set_plot_x_title(opp, "events");

  optt = create_and_attach_one_plot(pr, 1024, 1024, 0);
  if (optt == NULL)		return win_printf_OK("Cannot allocate plot");
  dstt = optt->dat[0];
  if (dstt == NULL)		return win_printf_OK("cannot create plot !");
  dstt->source = Mystrdupre(dstt->source,"toto");
  optt->filename = my_sprintf(NULL,"bump-quality.gr");
  dstt->nx = dstt->ny = 0;	
  set_plot_symb(dstt,"\\pt5\\oc ");
  set_dot_line(dstt);			
  set_plot_y_title(optt, "bump quality");
  set_plot_x_title(optt, "events");		
	
	
  opr = create_and_attach_one_plot(pr, rising, rising, 0);
  if (opr == NULL)		return win_printf_OK("Cannot allocate plot");
  dsr = opr->dat[0];
  if (dsr == NULL)		return win_printf_OK("cannot create plot !");
  dsr->source = Mystrdupre(dsr->source,"bump average rising edge");
  opr->filename = my_sprintf(NULL,"bump-shape.gr");	
  create_attach_select_y_un_to_op(opr, IS_METER, 0, 1, -9, 0, "nm");
  create_attach_select_x_un_to_op(opr, IS_SECOND, 0, 1, 0, 0, "s");			
  set_plot_y_title(opr, "Bump shape");
  set_plot_x_title(opr, "time");

  dsf = create_and_attach_one_ds(opr, rising, rising, 0);
  if (dsf == NULL)		return win_printf_OK("cannot create plot !");
  dsf->source = Mystrdupre(dsf->source,"bump average falling edge");
  dsrer = create_and_attach_one_ds(opr, rising, rising, 0);
  if (dsrer == NULL)		return win_printf_OK("cannot create plot !");
  dsrer->source = Mystrdupre(dsrer->source,"bump average rising edge error");
  dsfer = create_and_attach_one_ds(opr, rising, rising, 0);
  if (dsfer == NULL)		return win_printf_OK("cannot create plot !");
  dsfer->source = Mystrdupre(dsfer->source,"bump average falling edge error");
  dsab = create_and_attach_one_ds(opr, 4*rising, 4*rising, 0);
  if (dsab == NULL)		return win_printf_OK("cannot create plot !");
  dsab->source = Mystrdupre(dsab->source,"bump average beginning");
  dsam = create_and_attach_one_ds(opr, rising, rising, 0);
  if (dsam == NULL)		return win_printf_OK("cannot create plot !");
  dsam->source = Mystrdupre(dsam->source,"bump average medium");
  dsae = create_and_attach_one_ds(opr, 4*rising, 4*rising, 0);
  if (dsae == NULL)		return win_printf_OK("cannot create plot !");
  dsae->source = Mystrdupre(dsae->source,"bump average beginning");
				
  sfer = (double*)calloc(rising,sizeof(double));		
  srer = (double*)calloc(rising,sizeof(double));				
  sf = (double*)calloc(rising,sizeof(double));		
  sr = (double*)calloc(rising,sizeof(double));
					
  if (sfer == NULL || srer == NULL || sr == NULL || sf == NULL )		
    win_printf("cannot allocate memory !");	
				
				
  oped = create_and_attach_one_plot(pr, 1024, 1024, 0);
  if (oped == NULL)		return win_printf_OK("Cannot allocate plot");
  dsre = oped->dat[0];
  if (dsre == NULL)		return win_printf_OK("cannot create plot !");
  dsre->source = Mystrdupre(dsre->source,"Rising edge velocity");
  oped->filename = my_sprintf(NULL,"bump-edges.gr");
  dsre->nx = dsre->ny = 0;	
  set_plot_symb(dsre,"\\pt5\\oc ");
  set_dot_line(dsre);			
  set_plot_y_title(oped, "bump edges velocity");
  set_plot_x_title(oped, "events");						
  dsfe = create_and_attach_one_ds(oped, 1024, 1024, 0);
  if (dsfe == NULL)		return win_printf_OK("cannot create plot !");
  dsfe->source = Mystrdupre(dsfe->source,"Falling edge velocity");	
  dsfe->nx = dsfe->ny = 0;	
  set_plot_symb(dsfe,"\\pt5\\oc ");
  set_dot_line(dsfe);				
	
  opedt = create_and_attach_one_plot(pr, 1024, 1024, 0);
  if (opedt == NULL)		return win_printf_OK("Cannot allocate plot");
  dsrt = opedt->dat[0];
  if (dsrt == NULL)		return win_printf_OK("cannot create plot !");
  dsrt->source = Mystrdupre(dsrt->source,"Bump Rising time");
  opedt->filename = my_sprintf(NULL,"bump-timing.gr");
  dsrt->nx = dsrt->ny = 0;	
  set_plot_symb(dsrt,"\\pt5\\oc ");
  set_dot_line(dsrt);			
  set_plot_y_title(opedt, "Bump Rising time");
  set_plot_x_title(opedt, "events");						
  dsft = create_and_attach_one_ds(opedt, 1024, 1024, 0);
  if (dsft == NULL)		return win_printf_OK("cannot create plot !");
  dsft->source = Mystrdupre(dsft->source,"Bump Falling time");	
  dsft->nx = dsft->ny = 0;	
  set_plot_symb(dsft,"\\pt5\\oc ");
  set_dot_line(dsft);				
  dsbt = create_and_attach_one_ds(opedt, 1024, 1024, 0);
  if (dsbt == NULL)		return win_printf_OK("cannot create plot !");
  dsbt->source = Mystrdupre(dsbt->source,"Bump Up time");	
  dsbt->nx = dsbt->ny = 0;	
  set_plot_symb(dsbt,"\\pt5\\oc ");
  set_dot_line(dsbt);						
	
	
  opebs = create_and_attach_one_plot(pr, 1024, 1024, 0);
  if (opebs == NULL)		return win_printf_OK("Cannot allocate plot");
  dsbs = opebs->dat[0];
  if (dsbs == NULL)		return win_printf_OK("cannot create plot !");
  dsbs->source = Mystrdupre(dsbs->source,"Bump size");
  opebs->filename = my_sprintf(NULL,"bump-size.gr");
  dsbs->nx = dsbs->ny = 0;	
  set_plot_symb(dsbs,"\\pt5\\oc ");
  set_dot_line(dsbs);			
  set_plot_y_title(opebs, "Bump size");
  set_plot_x_title(opebs, "events");		

  //d_draw_Op_proc(MSG_DRAW,the_dialog + 1,0);					
  refresh_plot(pr,pr->cur_op);	
  done = al_findfirst(fullpattern,&fb,0xFF); /* FA_DIREC|FA_RDONLY);*/
  while (done == 0)
    {
      if (extract_file_path(path, 256, fb.name) == NULL)
	extract_file_path(path, 256, fullpattern); 
      extract_file_name(file, 256, fb.name);
      display_title_message("loading %s\n in %s",backslash_to_slash(file),
			    backslash_to_slash(path));

      for (i = 0; path[i] != 0; i++);
      if (i)
	{
	  if (path[i-1] != '/' && path[i-1] != 92 && i < 255)
	    {
	      path[i] = 92; path[i+1] = 0;
	    }
	}	
      if (load_plt_file_in_pltreg(pr, file, path))
	return win_printf_OK("error loading %s",file);
      opi = pr->one_p;
      tfiles++;

      for (i = 0, dsi = NULL; i < opi->n_dat; i++)
	{
	  if (opi->dat[i]->source != NULL && strstr(opi->dat[i]->source,"Z bead1 - Z bead2") != NULL)
	    {
	      dsi = opi->dat[i];
	      break;
	    }
	}	
      if (dsi == NULL)	
	return win_printf_OK("cannot find 2 beads data set!");		
						
      for (k = 0, i = -1; k < opi->n_dat && i == -1; k++)
	{
	  dsbu = opi->dat[k];
	  if (dsbu->source != NULL && strstr(dsbu->source,"Helicase segments burst") != NULL)
	    {
	      sscanf (dsbu->source,"Helicase segments burst %d time %lu\n"	
		      "Bead %s\npath %s file %s\nZmag %f mm enzyme %s [ATP] %f \\mu M"
		      " Zcor %f",&nburst,&time,bead,ppath,pfile,
		      &zmag,heli,&atp,&obj_cor); 
	      i = k;
	    }

	}
      i_u = i_z = -1;
	
      get_afine_param_from_unit(opi->yu[opi->c_yu], &az, &dz);
      dz *= 1e9;

      if (dsbu->treatement != NULL)
	{
	  for (i_u = -1, j = 0; dsbu->treatement[j] != 0 && i_u == -1; j++)
	    if (dsbu->treatement[j] == 'U') i_u = j;
	  for (i_z = -1, j = 0; dsbu->treatement[j] != 0 && i_z == -1; j++)
	    if (dsbu->treatement[j] == 'Z') i_z = j;
	}
      if ((i != -1) && (i_u != -1) && (i_z != -1))
	{
	  /*			if (adj_bump)
				chi2 = exhaustive_bump_adjust2(dsi, dsbu);		*/
	  strupr(bead);
	  if ((nmol == 0) || strcmp(bead,prev_bead) != 0)
	    {
	      strcpy(prev_bead,bead);
	      nmol++;
	    }
	  nsegs++;
	  i_file++;
	  i_ud = 1 + 4*i_u;
	  i_ue = 4 + 4*i_u;
	  i_zd = 1 + 4*i_z;
	  i_ze = 4 + 4*i_z;
	  /* we had bump height */
	  dsp->xd[dsp->nx] = dsp->nx;
	  //dsp->yd[dsp->nx] = opi->dy * obj_cor * (dsbu->yd[i_ue] - dsbu->yd[i_ud]);
	  dsp->yd[dsp->nx] = dz * obj_cor * (dsbu->yd[i_ue] - dsbu->yd[i_ud]);
	  dsp->nx++; dsp->ny++;
	  if (dsp->nx >= dsp->mx)
	    dsp = build_adjust_data_set(dsp, dsp->mx + 256, dsp->mx + 256); 

	  dsp->history = my_sprintf(dsp->history,
				    "pt. %d %s bump mol %d\n",dsp->nx-1,file,nmol);
	  /* we had bump duration */	
	  dst->xd[dst->nx] = dst->nx;
	  dst->yd[dst->nx] = opi->dx * (dsbu->xd[i_ze] - dsbu->xd[i_ud]);
	  dst->nx++; dst->ny++;
	  if (dst->nx >= dst->mx)
	    dst = build_adjust_data_set(dst, dst->mx + 256, dst->mx + 256); 			
	  dst->history = my_sprintf(dst->history,
				    "pt.%d %s burst mol %d\n",dst->nx-1,file,nmol);
			

	  x0 = dsbu->xd[0];
	  x1 = dsbu->xd[5];
	  recover_points_index_in_ds(x0, x1, dsi, &imin, &imax);
	  x0 = dsbu->xd[2];
	  x1 = dsbu->xd[3];
	  recover_points_index_in_ds(x0, x1, dsi, &ib0, &ib1);						
	  for (j = ib0, k = 0, tmp = 0;  j < ib1; j++)
	    {
	      tmp += (dsi->yd[j] - dsbu->yd[2])*(dsi->yd[j] - dsbu->yd[2]);
	      k++;
	    }
	  if (k > 0) tmp /= k;
	  tmp = sqrt(tmp);
	  if (k > 1) tmp /= sqrt(k-1);

	  dstt->xd[dstt->nx] = dstt->nx;
	  dstt->yd[dstt->nx] = fabs(dsbu->yd[2] - dsbu->yd[0]);
	  if (tmp != 0) dstt->yd[dstt->nx] /= tmp; 
	  dstt->nx++; dstt->ny++;
	  if (dstt->nx >= dstt->mx)
	    dstt = build_adjust_data_set(dstt, dstt->mx + 256, dstt->mx + 256);
	  dstt->history = my_sprintf(dstt->history,
				     "pts. %d  %s bump mol %d\n",dstt->nx-1,file,nmol);		
						
	  if ((dz*(dsbu->yd[i_ue] - dsbu->yd[i_ud]) >= minb) && 
	      (dz*(dsbu->yd[i_ue] - dsbu->yd[i_ud]) < maxb) && ib1-ib0 > rising)
	    {
	      if (verbose == OK)
		verbose = win_printf_OK("Bump %g found %g %g",
					dsbu->yd[i_ue] - dsbu->yd[i_ud],minb,maxb);			
	      w = rising/2;
	      mid = (ib0+ib1)/2;
	      for( j = ib0 - w, k = 0; (j < ib0 + w) && (j < mid); j++, k++)
		{
		  if (j >= imin && j < imax)
		    {
		      tmp2 = dsi->yd[j] - dsbu->yd[0];
		      if (norm_bump && (dsbu->yd[2] - dsbu->yd[0]) != 0)
			tmp2 /=  dsbu->yd[2] - dsbu->yd[0];
		      dsr->yd[k] += tmp2;
		      dsr->xd[k] += 1;
		      sr[k] += tmp2;
		      srer[k] += tmp2*tmp2;
		    } 
		  if (j > imin && j < imax)
		    {
		      dt += dsi->xd[j] - dsi->xd[j-1];
		      dtn++;
		    } 					
		}  
	      for( j = ib1 - w, k = 0; (j < ib1 + w) && (j < imax); j++, k++)
		{
		  if (j >= mid && j >= imin && j < imax)
		    {
		      tmp2 = dsi->yd[j] - dsbu->yd[0];
		      if (norm_bump && (dsbu->yd[2] - dsbu->yd[0]) != 0)
			tmp2 /=  dsbu->yd[2] - dsbu->yd[0];
		      dsf->yd[k] += tmp2;
		      dsf->xd[k] += 1;
		      sf[k] += tmp2;
		      sfer[k] += tmp2*tmp2;
		    } 
		  if (j >= mid && j > imin && j < imax)
		    {
		      dt += dsi->xd[j] - dsi->xd[j-1];
		      dtn++;
		    } 					
		} 
	      for (j = 0; j < 4 * rising; j++)
		{
		  k = imin + j;
		  if (k < ib0)
		    {
		      tmp2 = dsi->yd[k] - dsbu->yd[0];
		      if (norm_bump && (dsbu->yd[2] - dsbu->yd[0]) != 0)
			tmp2 /=  dsbu->yd[2] - dsbu->yd[0];	
		      dsab->yd[j] += tmp2;
		      dsab->xd[j] += 1;						
		    }
		  k = j + imax - 4*rising;
		  if (k > ib1 && k < imax)
		    {
		      tmp2 = dsi->yd[k] - dsbu->yd[0];
		      if (norm_bump && (dsbu->yd[2] - dsbu->yd[0]) != 0)
			tmp2 /=  dsbu->yd[2] - dsbu->yd[0];	
		      dsae->yd[j] += tmp2;
		      dsae->xd[j] += 1;						
		    }									
				
		}				
	      for (j = 0; j < rising; j++)
		{
		  k = mid - w + j;
		  if (k > ib0 && k < ib1)
		    {
		      tmp2 = dsi->yd[k] - dsbu->yd[0];
		      if (norm_bump && (dsbu->yd[2] - dsbu->yd[0]) != 0)
			tmp2 /=  dsbu->yd[2] - dsbu->yd[0];
		      dsam->yd[j] += tmp2;
		      dsam->xd[j] += 1;						
		    }								
		}  				
	    }
	  else
	    {
	      if (verbose == OK)
		verbose = win_printf("Bump %g out of range %g %g",
				     dsbu->yd[2] - dsbu->yd[0],minb,maxb);
	    }
	  if ((opi->dx*(dsbu->xd[i_ue] - dsbu->xd[i_ud]) >= 0.001*minrft))
	    {
	      dsre->xd[dsre->nx] = dsre->nx;
	      // dsre->yd[dsre->nx] = opi->dy * obj_cor * (dsbu->yd[i_ue] - dsbu->yd[i_ud]);
	      dsre->yd[dsre->nx] = dz * obj_cor * (dsbu->yd[i_ue] - dsbu->yd[i_ud]);
	      if (dsbu->xd[i_ue] - dsbu->xd[i_ud] != 0)
		dsre->yd[dsre->nx] /= (dsbu->xd[i_ue] - dsbu->xd[i_ud]) * opi->dx;
	      else win_printf("zero denominator in rising edge");
	      dsre->nx++; dsre->ny++;
	      if (dsre->nx >= dsre->mx)
		dsre = build_adjust_data_set(dsre, dsre->mx + 256, dsre->mx + 256); 
	      dsre->history = my_sprintf(dsre->history,"pt. %d %s burst mol %d\n",
					 dsre->nx-1,file,nmol);	
			
	      dsrt->xd[dsrt->nx] = dsrt->nx;
	      dsrt->yd[dsrt->nx] = (dsbu->xd[i_ue] - dsbu->xd[i_ud]) * opi->dx;
	      dsrt->nx++; dsrt->ny++;
	      if (dsrt->nx >= dsrt->mx)
		dsrt = build_adjust_data_set(dsrt, dsrt->mx + 256, dsrt->mx + 256); 	    
	      dsrt->history = my_sprintf(dsrt->history,
					 "pt. %d %s burst mol %d\n",dsrt->nx-1,file,nmol);					
	    }	
	  if ((opi->dx*(dsbu->xd[i_ze] - dsbu->xd[i_zd]) >= 0.001*minrft))
	    {
	      dsfe->xd[dsfe->nx] = dsfe->nx;
	      //dsfe->yd[dsfe->nx] = opi->dy * obj_cor * (dsbu->yd[i_ze] - dsbu->yd[i_zd]);
	      dsfe->yd[dsfe->nx] = dz * obj_cor * (dsbu->yd[i_ze] - dsbu->yd[i_zd]);
	      if (dsbu->xd[i_ze] - dsbu->xd[i_zd] != 0)
		dsfe->yd[dsfe->nx] /= (dsbu->xd[i_ze] - dsbu->xd[i_zd]) * opi->dx;
	      else win_printf("zero denominator in fallining edge");		
	      dsfe->nx++; dsfe->ny++;
	      if (dsfe->nx >= dsfe->mx)
		dsfe = build_adjust_data_set(dsfe, dsfe->mx + 256, dsfe->mx + 256); 
	      dsfe->history = my_sprintf(dsfe->history,
					 "pt. %d %s burst mol %d\n",dsfe->nx-1,file,nmol);
			
				
	      dsft->xd[dsft->nx] = dsft->nx;
	      dsft->yd[dsft->nx] = (dsbu->xd[i_ze] - dsbu->xd[i_zd]) * opi->dx;
	      dsft->nx++; dsft->ny++;
	      if (dsft->nx >= dsft->mx)
		dsft = build_adjust_data_set(dsft, dsft->mx + 256, dsft->mx + 256);
	      dsft->history = my_sprintf(dsft->history,
					 "pt. %d %s bump mol %d\n",dsft->nx-1,file,nmol);
	    }
	  dsbt->xd[dsbt->nx] = dsbt->nx;
	  dsbt->yd[dsbt->nx] = (dsbu->xd[i_zd] - dsbu->xd[i_ue]) * opi->dx;
	  dsbt->nx++; dsbt->ny++;
	  if (dsbt->nx >= dsbt->mx)
	    dsbt = build_adjust_data_set(dsbt, dsbt->mx + 256, dsbt->mx + 256);
	  dsbt->history = my_sprintf(dsbt->history,
				     "pt. %d %s bump mol %d\n",dsbt->nx-1,file,nmol);					
	  dsbs->xd[dsbs->nx] = dsbs->nx;
	  //dsbs->yd[dsbs->nx] = opi->dy * obj_cor * (dsbu->yd[i_ue] - dsbu->yd[i_ud]);
	  dsbs->yd[dsbs->nx] = dz * obj_cor * (dsbu->yd[i_ue] - dsbu->yd[i_ud]);
	  dsbs->nx++; dsbs->ny++;
	  if (dsbs->nx >= dsbs->mx)
	    dsbs = build_adjust_data_set(dsbs, dsbs->mx + 256, dsbs->mx + 256);
	  dsbs->history = my_sprintf(dsbs->history,
				     "pt. %d %s bump mol %d\n",dsbs->nx-1,file,nmol);					
				
	  if (debug_lia == OK)
	    {
	      debug_lia = win_printf("Bump %s\n \\delta z = %g\n dt_r %g"
				     "dt_f %g\n v_r %g v_f %g\nsize %g",file,
				     dz*obj_cor*(dsbu->yd[2] - dsbu->yd[1]),
				     (dsbu->xd[2] - dsbu->xd[1]) * opi->dx,
				     (dsbu->xd[4] - dsbu->xd[3]) * opi->dx,
				     dsre->yd[dsre->nx-1], dsfe->yd[dsre->nx-1],
				     dsbs->yd[dsbs->nx-1]);
	    }
	  nfiles++;	
	}
      /* refresh_plot(pr,0); */
      remove_data(pr, IS_ONE_PLOT,(void*)opi);
      done = al_findnext(&fb);
    }
  //win_printf("Closing file search");
  al_findclose(&fb);

  opp->title = my_sprintf(NULL, "\\stack{{Analysis of %s}"
			  "{\\pt8 with %d/%d files and %d mol}}", 
			  backslash_to_slash(fullpattern), nfiles,tfiles,nmol);
	
  opt->title = my_sprintf(NULL, "\\stack{{Analysis of %s}"
			  "{\\pt8 with %d/%d files and %d mol}}", 
			  backslash_to_slash(fullpattern), nfiles,tfiles,nmol);
	
  optt->title = my_sprintf(NULL, "\\stack{{Analysis of %s}"
			   "{\\pt8 with %d/%d files and %d mol}}", 
			   backslash_to_slash(fullpattern), nfiles,tfiles,nmol);
  dt = (dtn != 0) ? dt/dtn : dt;
  dt = (dt == 0) ? 0.04 : dt;
  for (j = 0; j < rising; j++)
    {
      if (verbose == OK)
	verbose = win_printf("pt %f nb of avg %g",j,dsr->xd[j]);
      dsr->yd[j] /= (dsr->xd[j] != 0) ? dsr->xd[j] : 1;
      dsf->yd[j] /= (dsf->xd[j] != 0) ? dsf->xd[j] : 1;
      sr[j] /= (dsr->xd[j] != 0) ? dsr->xd[j] : 1;
      sf[j] /= (dsf->xd[j] != 0) ? dsf->xd[j] : 1;
      srer[j] /= (dsr->xd[j] != 0) ? dsr->xd[j] : 1;
      sfer[j] /= (dsf->xd[j] != 0) ? dsf->xd[j] : 1;		
      srer[j] -= sr[j]*sr[j];
      sfer[j] -= sf[j]*sf[j];
      srer[j] /= (dsr->xd[j] > 1) ? dsr->xd[j]-1 : 1;
      sfer[j] /= (dsf->xd[j] > 1) ? dsf->xd[j]-1 : 1;			
      dsrer->yd[j] = sqrt(srer[j]);
      dsfer->yd[j] = sqrt(sfer[j]);
      dsam->yd[j] /= (dsam->xd[j] != 0) ? dsam->xd[j] : 1;
      dsam->xd[j] = dt * (j + rising);
      dsrer->xd[j] = dsr->xd[j] = dt * (j - w);
      dsfer->xd[j] = dsf->xd[j] = dt * (j + w + 2*rising);
    }
  for (j = 0; j < 4*rising; j++)
    {
      dsab->yd[j] /= (dsab->xd[j] != 0) ? dsab->xd[j] : 1;
      dsae->yd[j] /= (dsae->xd[j] != 0) ? dsae->xd[j] : 1;
      dsab->xd[j] = dt * (j - 5*rising);
      dsae->xd[j] = dt * (j + 4*rising);							
    }	
  free(sf); free(sr); free(sfer); free(srer);	
  pr->one_p->need_to_refresh = 1;
  return refresh_plot(pr,pr->cur_op);
}	
int	do_histo_noise_in_time(void)
{
  register int i, j, k;
  static float binu = 1.0;
  float tmp, maxx;
  int nf;
  pltreg *pr;
  d_s *dsi, *dsd, *dser;
  O_p *op, *ops;
  char s[256];
	
  if(updating_menu_state != 0)	return D_O_K;		


  if (key[KEY_LSHIFT])   
    return win_printf_OK("This routine compute histo of bursts noise in real time");
	
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&ops,&dsi) != 3)
    return win_printf_OK("cannot find data");

				
  i =	find_op_nb_of_source_specific_ds_in_pr( pr, "Helicase noise bursts");
  if (i < 0)	return win_printf_OK("cannot find helicase data");
	
  ops = pr->o_p[i];
	
  if (ops->x_unit == NULL)
    sprintf(s,"compute histo of %d bursts noise\nBin size %%f",ops->n_dat);
  else 	
    sprintf(s,"compute histo of %d bursts noise\nBin size in %s %%f",ops->n_dat,ops->x_unit);
  if (win_scanf(s, &binu)==CANCEL) return 0;
	
  for (i = 0, maxx = 0; i < ops->n_dat; i++)
    {
      dsi = ops->dat[i];
      for (j = 0; j < dsi->nx; j++)
	maxx = (dsi->xd[j] > maxx) ? dsi->xd[j] : maxx;
    }
  nf = 1 + (maxx*ops->dx)/binu;
	
	
  op = create_and_attach_one_plot(pr, nf, nf, 0);
  if (op == NULL)		return win_printf_OK("Cannot allocate plot");
  dsd = op->dat[0];
  if (dsd == NULL)	return win_printf_OK("cannot create plot !");
  dsd->source = Mystrdupre(dsd->source,"Average Helicase noise bursts");

  if ((dser = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");	
			

  for (i = 0, maxx = 0; i < ops->n_dat; i++)
    {
      dsi = ops->dat[i];
      for (j = 0; j < dsi->nx; j++)
	maxx = (dsi->xd[j] > maxx) ? dsi->xd[j] : maxx;
    }
  nf = 1 + (maxx*ops->dx)/binu;	
  op->filename = Transfer_filename(ops->filename);
  uns_op_2_op_by_type(ops, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  create_attach_select_y_un_to_op(op, 0, 0, 1, 0, 0, "\\mu{}m^2");
		
  set_plot_title(op, "\\stack{{Average Helicase noise over %d bursts}{bin %g}}"
		 ,ops->n_dat,binu);
  set_plot_y_title(op, "<\\delta z^2 >");
  set_plot_x_title(op, "Time in burst");
  set_dot_line(dsd);	
  set_dot_line(dser);				
	
  for (i = 0; i < ops->n_dat; i++)
    {
      dsi = ops->dat[i];
      for (j = 0; j < dsi->nx; j++)
	{
	  k = (int)((dsi->xd[j]*ops->dx)/binu);
	  k %= dsd->nx;
	  dsd->yd[k] += dsi->yd[j] *  dsi->yd[j];
	  dsd->xd[k]++;
	}
    }
  for (k = 0; k < dsd->nx; k++)
    dsd->yd[k] = (dsd->xd[k] != 0) ? dsd->yd[k]/dsd->xd[k] : dsd->yd[k];
		
  for (i = 0; i < ops->n_dat; i++)
    {
      dsi = ops->dat[i];
      for (j = 0; j < dsi->nx; j++)
	{
	  k = (int)((dsi->xd[j]*ops->dx)/binu);
	  k %= dsd->nx;
	  tmp =  dsi->yd[j] *  dsi->yd[j];
	  tmp -= dsd->yd[k];
	  tmp *= tmp;
	  dser->yd[k] += tmp;
	  dser->xd[k]++;
	}
    }				
  for (k = 0; k < dsd->nx; k++)
    {
      dser->yd[k] = sqrt(dser->yd[k]);
      dser->yd[k] = (dser->xd[k] > 1) ? dser->yd[k]/(dser->xd[k]-1) : dser->yd[k];
      dsd->yd[k] *= op->dy*op->dy;
      dser->yd[k] *= op->dy*op->dy;
      dsd->xd[k] = dser->xd[k] = k*binu/ops->dx;
    }
														
										
  refresh_plot(pr, pr->cur_op);		
  return D_O_K;	
}


int simulate_helicase(void)
{
  register int i, j, k;
  char pattern[1024];
  pltreg *pr;
  float tmp;
  O_p *opuz, *opt = NULL, *opp, *optt, *opuzt, *opuh;
  d_s *dsuz, *dsuzt, *dsuh, *dst, *dsp, *dstt;
  static float puph = 0.005, pups = 0.05, pdnh = 0.0005, pdns = 0.005;
  static int niter = 1024, iidum = 0;
  long idum;
  int z, dz, state, t, ev[6], zt;
	
  if(updating_menu_state != 0)	return D_O_K;		


  if (key[KEY_LSHIFT])   
    return win_printf_OK("This routine simulate helicase signal");


  puph = (float)1/puph;
  pups = (float)1/pups;
  pdnh = (float)1/pdnh;
  pdns = (float)1/pdns;
  i = win_scanf("Simulate helicase, P_^{-1}{up}(hybridation) %f"
		"P_^{-1}{up}(switch) %fP_^{-1}{dwn}(hybridation) %f"
		"P_^{-1}{dwn}(switch) %fnb. of iteration %dseed %d",
		&puph,&pups,&pdnh,&pdns,&niter,&iidum);
	
  puph = (float)1/puph;
  pups = (float)1/pups;
  pdnh = (float)1/pdnh;
  pdns = (float)1/pdns;	
	
  for (i = 0; i < 6; i++)	ev[i] = 0;
	
  pr = create_and_register_new_plot_project(0,   32,  900,  668);
  if (pr == NULL) return win_printf_OK("cannot find data");
  opt = create_and_attach_one_plot(pr, 4096, 4096, 0);
  if (opt == NULL) return win_printf_OK("cannot find data");
  dst = opt->dat[0];
    
  dst->source = Mystrdupre(dst->source,"burst simulation");
  opt->filename = my_sprintf(NULL,"burst-shape.gr");
  create_attach_select_x_un_to_op(opt, IS_SECOND, 0, 1, 0, 0, "s");		
  dst->nx = dst->ny = 0;	
  set_dot_line(dst);			
  set_plot_y_title(opt, "Z");
  set_plot_x_title(opt, "time");
		
  opp = create_and_attach_one_plot(pr, niter, niter, 0);
  if (opp == NULL)	return win_printf_OK("Cannot allocate plot");
  dsp = opp->dat[0];
  if (dsp == NULL)	return win_printf_OK("cannot create plot !");
  dsp->source = Mystrdupre(dsp->source,"burst action time");
  opp->filename = my_sprintf(NULL,"burst-time.gr");	
  set_plot_symb(dsp,"\\pt5\\oc ");
  set_dot_line(dsp);	
  set_plot_y_title(opp, "Burst working time");
  set_plot_x_title(opp, "events");		
		
  optt = create_and_attach_one_plot(pr, niter, niter, 0);
  if (optt == NULL)	return win_printf_OK("Cannot allocate plot");
  dstt = optt->dat[0];
  if (dstt == NULL)	return win_printf_OK("cannot create plot !");
  dstt->source = Mystrdupre(dstt->source,"Total helicase working time");
  optt->filename = my_sprintf(NULL,"heli-total.gr");
  set_plot_symb(dstt,"\\pt5\\oc ");
  set_dot_line(dstt);			
  set_plot_y_title(optt, "total action time");
  set_plot_x_title(optt, "events");		
			


  opuh = create_and_attach_one_plot(pr, niter, niter, 0);
  if (opuh == NULL)		return win_printf_OK("Cannot allocate plot");
  dsuh = opuh->dat[0];
  if (dsuh == NULL)		return win_printf_OK("cannot create plot !");
  dsuh->source = Mystrdupre(dsuh->source,"UH working time");
  opuh->filename = my_sprintf(NULL,"heli-uh.gr");
  set_plot_symb(dsuh,"\\pt5\\oc ");
  set_dot_line(dsuh);			
  set_plot_y_title(opuh, "action time of UH");
  set_plot_x_title(opuh, "events");		
  dsuh->nx = dsuh->ny = 0;			

  opuz = create_and_attach_one_plot(pr, niter, niter, 0);
  if (opuz == NULL)		return win_printf_OK("Cannot allocate plot");
  dsuz = opuz->dat[0];
  if (dsuz == NULL)		return win_printf_OK("cannot create plot !");
  dsuz->source = Mystrdupre(dsuz->source,"UZ working time");
  opuz->filename = my_sprintf(NULL,"heli-uz.gr");
  set_plot_symb(dsuz,"\\pt5\\oc ");
  set_dot_line(dsuz);			
  set_plot_y_title(opuz, "action time of UH");
  set_plot_x_title(opuz, "events");		
  dsuz->nx = dsuz->ny = 0;			


  opuzt = create_and_attach_one_plot(pr, niter, niter, 0);
  if (opuzt == NULL)		return win_printf_OK("Cannot allocate plot");
  dsuzt = opuzt->dat[0];
  if (dsuzt == NULL)		return win_printf_OK("cannot create plot !");
  dsuzt->source = Mystrdupre(dsuzt->source,"Z working time");
  opuzt->filename = my_sprintf(NULL,"heli-uzt.gr");
  set_plot_symb(dsuzt,"\\pt5\\oc ");
  set_dot_line(dsuzt);			
  set_plot_y_title(opuzt, "action time of Z");
  set_plot_x_title(opuzt, "events");		
  dsuzt->nx = dsuzt->ny = 0;			
	
			
  do_one_plot(pr);	
  refresh_plot(pr, pr->cur_op);		
			
  idum = (long)iidum;	
  for( i = 0; i < niter; i++)
    {
      for (j = z = t = state = k = zt = 0, dz = 1; k == 0; j++)
	{
	  tmp = fabs(ran1(&idum));
	  if (state == 1)	zt++;
	  if ((dz == 1) && (tmp < puph)) 
	    {
	      k = 1;
	      state++;
	    }
	  else  if ((dz == -1) && (tmp < pdnh)) 
	    {
	      k = 1;
	      state++;
	    }
	  else
	    {
	      tmp = fabs(ran1(&idum));
	      if ((dz == 1) && (tmp < pups)) 
		{
		  dz = -1;
		  state++;
		  if (state == 1)	zt = 0;
		}
	      else  if ((dz == -1) && (tmp < pdns)) 
		{
		  dz = 1;				
		  state++;
		}
	    }
	  z += dz;
	  dst->xd[j] = j;
	  dst->yd[j] = z;
	  dst->nx = dst->ny = j+1;
	  if (dst->nx >= dst->mx)
	    dst = build_adjust_data_set(dst, dst->mx + 4096, dst->mx + 4096);
	  if (state == 0)	t = z;

	  if ((j != 0) && (z <= 0)) k = 3;
	  if ((dz == 1) && (k == 0) && (state < 1023)) pattern[state] ='U';
	  if ((dz == -1) && (k == 0) && (state < 1023)) pattern[state] ='Z';
	  if (k == 1 && state < 1023) pattern[state] ='H';
	}
      state = (state > 1022) ? 1023 : state+1;
      pattern[state] = 0;
      if (state >= 1)
	{
	  dsuzt->xd[dsuzt->nx] = dsuzt->nx;
	  dsuzt->yd[dsuzt->nx] = zt;
	  dsuzt->nx++;			
	  dsuzt->ny = dsuzt->nx;			
	}
      dstt->xd[i] = i;
      dstt->yd[i] = j-1;
      dsp->xd[i] = i;
      dsp->yd[i] = t;		
      if (strcmp(pattern,"UH") == 0)
	{
	  ev[0]++;
	  dsuh->xd[dsuh->nx] = dsuh->nx;
	  dsuh->yd[dsuh->nx] = t;
	  dsuh->nx++;
	  dsuh->ny = dsuh->nx;
	}
      else if (strcmp(pattern,"UZ") == 0)	
	{	
	  ev[1]++;
	  dsuz->xd[dsuz->nx] = dsuz->nx;
	  dsuz->yd[dsuz->nx] = t;
	  dsuz->nx++;			
	  dsuz->ny = dsuz->nx;
	}
      else if (strcmp(pattern,"UZH") == 0)	ev[2]++;		
      else if (strcmp(pattern,"UZUH") == 0)	ev[3]++;
      else if (strcmp(pattern,"UZUZ") == 0)	ev[4]++;
      else ev[5]++;
				
    }	
  set_plot_title(opt,pattern);
  set_plot_title(optt, "\\stack{{%d UH %d UZ %d UZH %d UZUH}"
		 "{%d UZUZ %d Other total %d}}",ev[0],ev[1],ev[2],ev[3],ev[4],ev[5]
		 ,ev[0]+ev[1]+ev[2]+ev[3]+ev[4]+ev[5]);			
	
  set_plot_title(opp,"\\stack{{Helicase simulation %d iter seed %d}"
		 "{P_^{-1}{up}(hyb.) %g "
		 "P_^{-1}{up}(switch) %g}{P_^{-1}{dwn}(hyb.) %g "
		 "P_^{-1}{dwn}(switch) %g}}",niter,iidum,(float)1/puph,(float)1/pups,
		 (float)1/pdnh,(float)1/pdns);	
	
	
  iidum =(int)idum;
  refresh_plot(pr,0);
  return 0;
}


int nearly_exhaustive_fit_of_NHNZN_phase(d_s *ds, int xi0, int *xi1, int *xi2, 
					 int *xi3, int *xi4, int xi5, double *y1, double *y2, double *chi2)
{
  register int i, j, k, l;
  double y1b, y2b, chi20, tt, ti;
  int xi1bk = 0, xi2bk = 0, xi3bk = 0, xi4bk = 0;
  int first = 1, t, bin, xi1b = 0, xi2b = 0, xi3b = 0, xi4b = 0, bin2;
  clock_t start = 0;

  bin = (xi5 - xi0)/32;
  bin = (bin > 0) ? bin : 1;
  tt = (xi5  - xi0)/bin;
  tt *= tt;
  tt *= tt;
  tt += 8*8*8*8;
  bin2 = bin/4;
  bin2 = (bin2 > 0) ? bin2 : 1;	
  ti = 2*bin2;
  ti *= ti;
  ti *= ti;
  tt += ti;
	
  for(i = xi0, start = clock(), ti = 0; i < xi5 - 1; i += bin)
    {
      for(j = i+1; j < xi5 - 1; j += bin) /* helicase phase must be there */
	{
	  for(k = j; k < xi5; k += bin)
	    {
	      for(l = k; l < xi5; l += bin)
		{
		  ti += 1;
		  t =	fit_giuseppe_bump(ds, xi0, i, j, k, l, xi5, &y1b, &y2b, &chi20);
		  if (t != 0 && t != 4) 
		    {
		      display_title_message 
			("fit pb err. %d %d %d %d %d %d %d",t,xi0, i, j, k, l, xi5);
		      return t;	
		    }
		  if (t != 4)
		    {
		      if (first == 1 || chi20 < *chi2)
			{
			  first = 0;
			  xi1b = i;
			  xi2b = j;
			  xi3b = k;
			  xi4b = l;
			  *y1 = y1b;
			  *y2 = y2b;
			  *chi2 = chi20;
			  k = j;
			}
		    }
		  if (clock() - start > CLOCKS_PER_SEC/4)
		    {
		      display_title_message("bin %d %4f %%",bin,(ti*100)/tt);
		      start = clock();
		    }
		}
	    }
	}
    }
  if (bin == 1)
    {
      *xi1 = xi1b;
      *xi2 = xi2b;
      *xi3 = xi3b;
      *xi4 = xi4b;
      return 0;	
    }
  xi1bk = xi1b;
  xi2bk = xi2b;
  xi3bk = xi3b;
  xi4bk = xi4b;
  for(i = (xi1bk - bin > xi0) ? xi1bk - bin : xi0, start = clock(); i < xi5 - 1 && i < xi1bk + bin; i+= bin2)
    {
      for(j = (xi2bk - bin > i) ? xi2bk - bin : i; j < xi2bk + bin && j < xi5 - 1; j+= bin2) /* helicase phase must be there */
	{
	  for(k = (xi3bk - bin > j) ? xi3bk - bin : j; k < xi5 && k < xi3bk + bin; k+= bin2)
	    {
	      for(l = (xi4bk - bin > k) ? xi4bk - bin : k; l < xi5 && l < xi4bk + bin; l+= bin2)
		{
		  ti += 1;
		  t =	fit_giuseppe_bump(ds, xi0, i, j, k, l, xi5, &y1b, &y2b, &chi20);
		  if (t != 0 && t != 4) 
		    {
		      display_title_message 
			("fit pb err. %d %d %d %d %d %d %d",t,xi0, i, j, k, l, xi5);
		      return t;	
		    }
		  if (t != 4)
		    {
		      if (first == 1 || chi20 < *chi2)
			{
			  first = 0;
			  xi1b = i;
			  xi2b = j;
			  xi3b = k;
			  xi4b = l;
			  *y1 = y1b;
			  *y2 = y2b;
			  *chi2 = chi20;
			  k = j;
			}
		    }
		  if (clock() - start > CLOCKS_PER_SEC/4)
		    {
		      display_title_message("%4f %%",(ti*100)/tt);
		      start = clock();
		    }
		}
	    }
	}
    }	

  if (bin2 == 1)
    {
      *xi1 = xi1b;
      *xi2 = xi2b;
      *xi3 = xi3b;
      *xi4 = xi4b;
      return 0;	
    }
  xi1bk = xi1b;
  xi2bk = xi2b;
  xi3bk = xi3b;
  xi4bk = xi4b;
  for(i = (xi1bk - bin2 > xi0) ? xi1b - bin2 : xi0, start = clock(); i < xi5 - 1 && i < xi1bk + bin2; i++)
    {
      for(j = (xi2bk - bin2 > i) ? xi2bk - bin2 : i; j < xi2bk + bin2 && j < xi5 - 1; j++) /* helicase phase must be there */
	{
	  for(k = (xi3bk - bin2 > j) ? xi3bk - bin2 : j; k < xi5 && k < xi3bk + bin2; k++)
	    {
	      for(l = (xi4bk - bin2 > k) ? xi4bk - bin2 : k; l < xi5 && l < xi4bk + bin2; l++)
		{
		  ti += 1;
		  t =	fit_giuseppe_bump(ds, xi0, i, j, k, l, xi5, &y1b, &y2b, &chi20);
		  if (t != 0 && t != 4) 
		    {
		      display_title_message 
			("fit pb err. %d %d %d %d %d %d %d",t,xi0, i, j, k, l, xi5);
		      return t;	
		    }
		  if (t != 4)
		    {
		      if (first == 1 || chi20 < *chi2)
			{
			  first = 0;
			  *xi1 = i;
			  *xi2 = j;
			  *xi3 = k;
			  *xi4 = l;
			  *y1 = y1b;
			  *y2 = y2b;
			  *chi2 = chi20;
			  k = j;
			}
		    }
		  if (clock() - start > CLOCKS_PER_SEC/4)
		    {
		      display_title_message("%4f %%",(ti*100)/tt);
		      start = clock();
		    }
		}
	    }
	}
    }	
  return 0;
}



// fit for gp41


int	fit_gp41_burst(d_s *ds, int xi0, int xi1, int xi2, int xi3, int xi4, 
		       int xi5, double *y1, double *y2, double *yu, 
		       double *yz, double *chi2)
{
  register int i;
  double dx, tmp1, delta, tmp;
  int S = 0, U = 0;
  double M = 0, N = 0, O = 0, P = 0, Q = 0, R = 0, T = 0, V = 0;
  double au, az, bu, bz, a0, b0;
	
  if (ds == NULL)	return 1;
  if (xi0 < 0 || xi0 >= ds->nx) return 2;
  if (xi1 < xi0 || xi1 > ds->nx) return 2;
  if (xi2 < xi1 || xi2 > ds->nx) return 2;
  if (xi3 < xi2 || xi3 > ds->nx) return 2;
  if (xi4 < xi3 || xi4 > ds->nx) return 2;
  if (xi5 < xi4 || xi5 > ds->nx) return 2;
		
  /* a for d/dy1, b for d/dy2, c for d/dyu and d for d/dyz */
  for (i = xi0; i < xi1; i++)
    {
      T += ds->yd[i];
      S += 1;  
    }		
  if (xi2 != xi1)
    {
      dx = ds->xd[xi2] - ds->xd[xi1];
      if (dx != 0)	dx = (float)1/dx;
      else return 3;
      for (i = xi1; i < xi2; i++)
	{
	  tmp1 = (ds->xd[i] - ds->xd[xi1]) * dx;
	  O += tmp1;
	  N += ds->yd[i] * tmp1;
	  tmp1 *= tmp1;
	  M += tmp1;
	  T += ds->yd[i];
	  S += 1;  
	}
    }
  for (i = xi2; i < xi3; i++)
    {
      V += ds->yd[i];
      U += 1;  
    }		
	
  if (xi4 != xi3)
    {	
      dx = ds->xd[xi4] - ds->xd[xi3];
      if (dx != 0)	dx = (float)1/dx;
      else return 3;
      for (i = xi3; i < xi4; i++)
	{
	  tmp1 = (ds->xd[xi4] - ds->xd[i]) * dx;
	  R += tmp1;
	  Q += ds->yd[i] * tmp1;
	  tmp1 *= tmp1;
	  P += tmp1;
	  T += ds->yd[i];
	  S += 1;  
	}
    }	
  for (i = xi4; i < xi5; i++)
    {
      T += ds->yd[i];
      S += 1;  
    }	
  au = (M * S) - (O * N);
  az = -O * Q;
  a0 = (N * S) - (O * T);
  bu = - (N * R);
  bz = (P * S) - (R * Q);
  b0 = (O * S) - (T * R);
  delta = (au * bz) - (az * bu);
  if (delta == 0)	return 4;
  *yu = (a0 * bz - b0 * az)/delta;
  *yz = (au * b0 - a0 * bu)/delta;
  if (S == 0)	return 5;
  *y1 = (T - (N * *yu) - (Q * *yz))/S;
  *y2 = (U == 0) ? *y1 : V/U;

  *chi2 = 0;
  for (i = xi0; i < xi1; i++)
    {
      tmp = *y1 - ds->yd[i];
      *chi2 += tmp * tmp;
    }	
  if (xi2 != xi1)
    {	
      dx = ds->xd[xi2] - ds->xd[xi1];
      if (dx != 0)	dx = (float)1/dx;
      else return 3;
      for (i = xi1; i < xi2; i++)
	{
	  tmp1 = (ds->xd[i] - ds->xd[xi1]) * dx;
	  tmp1 *= *yu;
	  tmp = ds->yd[i] - *y1 - tmp1; 
	  *chi2 += tmp * tmp;
	}
    }
  for (i = xi2; i < xi3; i++)
    {
      tmp = *y2 - ds->yd[i];
      *chi2 += tmp * tmp;
    }	
  if (xi4 != xi3)
    {			
      dx = ds->xd[xi4] - ds->xd[xi3];
      if (dx != 0)	dx = (float)1/dx;
      else return 3;
      for (i = xi3; i < xi4; i++)
	{
	  tmp1 = (ds->xd[xi4] - ds->xd[i]) * dx;
	  tmp1 *= *yz;
	  tmp = ds->yd[i] - *y1 - tmp1; 
	  *chi2 += tmp * tmp;
	}
    }	
  for (i = xi4; i < xi5; i++)
    {
      tmp = *y1 - ds->yd[i];
      *chi2 += tmp * tmp;
    }				
  return 0;
}



int nearly_exhaustive_fit_of_gp41_burst(d_s *ds, int xi0, int *xi1, int *xi2, 
					 int *xi3, int *xi4, int xi5, double *y1, double *y2, 
					double *yu, double *yz, double *chi2)
{
  register int i, j, k, l;
  double y1b, y2b, yub, yzb, chi20, tt, ti;
  int first = 1, t, bin, xi1b = 0, xi2b = 0, xi3b = 0, xi4b = 0, bin2;
  int xi1bk = 0, xi2bk = 0, xi3bk = 0, xi4bk = 0;
  clock_t start = 0;

  bin = (xi5 - xi0)/32;
  bin = (bin > 0) ? bin : 1;
  tt = (xi5  - xi0)/bin;
  tt *= tt;
  tt *= tt;
  tt += 8*8*8*8;
  bin2 = bin/4;
  bin2 = (bin2 > 0) ? bin2 : 1;	
  ti = 2*bin2;
  ti *= ti;
  ti *= ti;
  tt += ti;
	
  for(i = xi0, start = clock(), ti = 0; i < xi5 - 1; i += bin)
    {
      for(j = i+1; j < xi5 - 1; j += bin) /* helicase phase must be there */
	{
	  for(k = j; k < xi5; k += bin)
	    {
	      for(l = k; l < xi5; l += bin)
		{
		  ti += 1;
		  t = fit_gp41_burst(ds, xi0, i, j, k, l, xi5, &y1b, &y2b, &yub, &yzb, &chi20);
		  if (t != 0 && t != 4) 
		    {
		      display_title_message 
			("fit pb err. %d %d %d %d %d %d %d",t,xi0, i, j, k, l, xi5);
		      return t;	
		    }
		  if (t != 4)
		    {
		      if (first == 1 || chi20 < *chi2)
			{
			  first = 0;
			  xi1b = i;
			  xi2b = j;
			  xi3b = k;
			  xi4b = l;
			  *y1 = y1b;
			  *y2 = y2b;
			  *yu = yub;
			  *yz = yzb;
			  *chi2 = chi20;
			  k = j;
			}
		    }
		  if (clock() - start > CLOCKS_PER_SEC/4)
		    {
		      display_title_message("bin %d %4f %% \\chi^2 = %g                  ",bin,(ti*100)/tt,*chi2);
		      start = clock(); 
		    }
		}
	    }
	}
    }
  if (bin == 1)
    {
      *xi1 = xi1b;
      *xi2 = xi2b;
      *xi3 = xi3b;
      *xi4 = xi4b;
      return 0;	
    }
  xi1bk = xi1b;
  xi2bk = xi2b;
  xi3bk = xi3b;
  xi4bk = xi4b;

  for(i = (xi1bk - bin > xi0) ? xi1bk - bin : xi0, start = clock(); i < xi5 - 1 && i < xi1bk + bin; i+= bin2)
    {
      for(j = (xi2bk - bin > i) ? xi2bk - bin : i; j < xi2bk + bin && j < xi5 - 1; j+= bin2) /* helicase phase must be there */
	{
	  for(k = (xi3bk - bin > j) ? xi3bk - bin : j; k < xi5 && k < xi3bk + bin; k+= bin2)
	    {
	      for(l = (xi4bk - bin > k) ? xi4bk - bin : k; l < xi5 && l < xi4bk + bin; l+= bin2)
		{
		  ti += 1;
		  t = fit_gp41_burst(ds, xi0, i, j, k, l, xi5, &y1b, &y2b, &yub, &yzb, &chi20);
		  if (t != 0 && t != 4) 
		    {
		      display_title_message 
			("fit pb err. %d %d %d %d %d %d %d",t,xi0, i, j, k, l, xi5);
		      return t;	
		    }
		  if (t != 4)
		    {
		      if (first == 1 || chi20 < *chi2)
			{
			  first = 0;
			  xi1b = i;
			  xi2b = j;
			  xi3b = k;
			  xi4b = l;
			  *y1 = y1b;
			  *y2 = y2b;
			  *yu = yub;
			  *yz = yzb;
			  *chi2 = chi20;
			  k = j;
			}
		    }
		  if (clock() - start > CLOCKS_PER_SEC/4)
		    {
		      display_title_message("%4f %% \\chi^2 = %g                ",(ti*100)/tt,*chi2);
		      start = clock();
		    }
		}
	    }
	}
    }	

  if (bin2 == 1)
    {
      *xi1 = xi1b;
      *xi2 = xi2b;
      *xi3 = xi3b;
      *xi4 = xi4b;
      return 0;	
    }
  xi1bk = xi1b;
  xi2bk = xi2b;
  xi3bk = xi3b;
  xi4bk = xi4b;
  for(i = (xi1bk - bin2 > xi0) ? xi1bk - bin2 : xi0, start = clock(); i < xi5 - 1 && i < xi1bk + bin2; i++)
    {
      for(j = (xi2bk - bin2 > i) ? xi2bk - bin2 : i; j < xi2bk + bin2 && j < xi5 - 1; j++) /* helicase phase must be there */
	{
	  for(k = (xi3bk - bin2 > j) ? xi3bk - bin2 : j; k < xi5 && k < xi3bk + bin2; k++)
	    {
	      for(l = (xi4bk - bin2 > k) ? xi4bk - bin2 : k; l < xi5 && l < xi4bk + bin2; l++)
		{
		  ti += 1;
		  t = fit_gp41_burst(ds, xi0, i, j, k, l, xi5, &y1b, &y2b, &yub, &yzb, &chi20);
		  if (t != 0 && t != 4) 
		    {
		      display_title_message 
			("fit pb err. %d %d %d %d %d %d %d",t,xi0, i, j, k, l, xi5);
		      return t;	
		    }
		  if (t != 4)
		    {
		      if (first == 1 || chi20 < *chi2)
			{
			  first = 0;
			  *xi1 = i;
			  *xi2 = j;
			  *xi3 = k;
			  *xi4 = l;
			  *y1 = y1b;
			  *y2 = y2b;
			  *yu = yub;
			  *yz = yzb;
			  *chi2 = chi20;
			  k = j;
			}
		    }
		  if (clock() - start > CLOCKS_PER_SEC/4)
		    {
		      display_title_message("%4f %% \\chi^2 = %g                  ",(ti*100)/tt, *chi2);
		      start = clock();
		    }
		}
	    }
	}
    }	
  return 0;
}




int	fit_gp41_burst_xm(d_s *ds, int xi0, int xi1, int xi2, int xi3, int xi4, 
			  int xi5, int xm, double *y1, double *y2, double *chi2)
{
  register int i;
  double ay1, ay2, by1, by2, a0, b0, dx, tmp1, tmp2, delta, tmp;
	
  if (ds == NULL)	return 1;
  if (xi0 < 0 || xi0 >= ds->nx) return 2;
  if (xi1 < xi0 || xi1 > ds->nx) return 2;
  if (xi2 < xi1 || xi2 > ds->nx) return 2;
  if (xi3 < xi2 || xi3 > ds->nx) return 2;
  if (xi4 < xi3 || xi4 > ds->nx) return 2;
  if (xi5 < xi4 || xi5 > ds->nx) return 2;
  if (xm > xi3 || xm < xi2) return 2;
		
  /* a for d/dy1, b for d/dy2 */
  a0 = ay1 = ay2 = b0 = by1 = by2 = 0;
  for (i = xi0; i < xi1; i++)
    {
      a0 += -2 * ds->yd[i];
      ay1 += 2;  
    }		
  if (xi2 != xi1)
    {
      dx = ds->xd[xm] - ds->xd[xi1];
      if (dx != 0)	dx = (float)1/dx;
      else return 3;
      for (i = xi1; i < xi2; i++)
	{
	  tmp1 = (ds->xd[i] - ds->xd[xi1]) * dx;
	  tmp2 = (ds->xd[xm] - ds->xd[i]) * dx;
		
	  a0 += -2 * ds->yd[i] * tmp2;
	  b0 += -2 * ds->yd[i] * tmp1;
		
	  by1 += 2 * tmp2 * tmp1;
	  by2 += 2 * tmp1 * tmp1;
		
	  ay2 += 2 * tmp2 * tmp1;
	  ay1 += 2 * tmp2 * tmp2;		  
	}
    }

  for (i = xi2; i < xi3; i++)
    {
      b0 += -2 * ds->yd[i];
      by2 += 2;  
    }		
	
  if (xi4 != xi3)
    {	
      dx = ds->xd[xi4] - ds->xd[xm];
      if (dx != 0)	dx = (float)1/dx;
      else return 3;
      for (i = xi3; i < xi4; i++)
	{
	  tmp1 = (ds->xd[i] - ds->xd[xm]) * dx;
	  tmp2 = (ds->xd[xi4] - ds->xd[i]) * dx;
		
	  a0 += -2 * ds->yd[i] * tmp1;
	  b0 += -2 * ds->yd[i] * tmp2;
		
	  by1 += 2 * tmp2 * tmp1;
	  by2 += 2 * tmp2 * tmp2;
		
	  ay2 += 2 * tmp2 * tmp1;
	  ay1 += 2 * tmp1 * tmp1;		  
	}
    }	
	
  for (i = xi4; i < xi5; i++)
    {
      a0 += -2 * ds->yd[i];
      ay1 += 2;  
    }			
  delta = by2 * ay1 - ay2 * by1;
  if (delta == 0)	return 4;
  *y1 = -(by2 * a0 - ay2 * b0)/delta;
  *y2 = -(ay1 * b0 - by1 * a0)/delta;
  *chi2 = 0;
  for (i = xi0; i < xi1; i++)
    {
      tmp = *y1 - ds->yd[i];
      *chi2 += tmp * tmp;
    }	
  if (xi2 != xi1)
    {	
      dx = ds->xd[xm] - ds->xd[xi1];
      if (dx != 0)	dx = (float)1/dx;
      else return 3;
      for (i = xi1; i < xi2; i++)
	{
	  tmp1 = (ds->xd[i] - ds->xd[xi1]) * dx;
	  tmp2 = (ds->xd[xm] - ds->xd[i]) * dx;
	  tmp = ds->yd[i] - (*y2) * tmp1 - (*y1) * tmp2; 
	  *chi2 += tmp * tmp;
	}
    }
  for (i = xi2; i < xi3; i++)
    {
      tmp = *y2 - ds->yd[i];
      *chi2 += tmp * tmp;
    }	
  if (xi4 != xi3)
    {			
      dx = ds->xd[xi4] - ds->xd[xm];
      if (dx != 0)	dx = (float)1/dx;
      else return 3;
      for (i = xi3; i < xi4; i++)
	{
	  tmp1 = (ds->xd[i] - ds->xd[xm]) * dx;
	  tmp2 = (ds->xd[xi4] - ds->xd[i]) * dx;
	  tmp = ds->yd[i] - (*y1) * tmp1 - (*y2) * tmp2; 
	  *chi2 += tmp * tmp;
	}
    }	
  for (i = xi4; i < xi5; i++)
    {
      tmp = *y1 - ds->yd[i];
      *chi2 += tmp * tmp;
    }				
  return 0;
}


int nearly_exhaustive_fit_of_gp41_burst_xm(d_s *ds, int xi0, int *xi1, int *xi2, 
					   int *xi3, int *xi4, int xi5, int *xm, double *y1, 
					   double *y2, double *chi2)
{
  register int i, j, k, l, m;
  double y1b, y2b, chi20, tt, ti;
  int first = 1, t, bin, xi1b = 0, xi2b = 0, xi3b = 0, xi4b = 0, bin2, xmb = 0;
  int xi1bk = 0, xi2bk = 0, xi3bk = 0, xi4bk = 0, xmbk = 0;
  clock_t start = 0;

  bin = (xi5 - xi0)/32;
  bin = (bin > 0) ? bin : 1;
  tt = (xi5  - xi0)/bin;
  tt *= tt;
  tt *= tt;
  tt += 8*8*8*8;
  bin2 = bin/4;
  bin2 = (bin2 > 0) ? bin2 : 1;	
  ti = 2*bin2;
  ti *= ti;
  ti *= ti;
  tt += ti;
	
  for(i = xi0, start = clock(), ti = 0; i < xi5 - 1; i += bin)
    {  // moving xi1
      for(j = i+1; j < xi5 - 1; j += bin) /* helicase phase must be there */
	{  // moving xi2
	  for(k = j; k < xi5; k += bin)
	    {  // moving xi3
	      for(l = k; l < xi5; l += bin)
		{  // moving xi4
		  for(m = j; m < k; m += bin)
		    {
		      ti += 1;
		      t = fit_gp41_burst_xm(ds, xi0, i, j, k, l, xi5, m, &y1b, &y2b, &chi20);
		      if (t != 0 && t != 4) 
			{
			  display_title_message 
			    ("fit pb err. %d %d %d %d %d %d %d %d",t,xi0, i, j, k, l, xi5, m);
			  return t;	
			}
		      if (t != 4)
			{
			  if (first == 1 || chi20 < *chi2)
			    {
			      first = 0;
			      xi1b = i;
			      xi2b = j;
			      xi3b = k;
			      xi4b = l;
			      xmb = m;
			      *y1 = y1b;
			      *y2 = y2b;
			      *chi2 = chi20;
			      k = j;
			    }
			}
		      if (clock() - start > CLOCKS_PER_SEC/4)
			{
			  display_title_message("bin %d %4f %% \\chi^2 %g             ",bin,(ti*100)/tt,*chi2);
			  start = clock();
			}
		    }
		}
	    }
	}
    }
  if (bin == 1)
    {
      *xi1 = xi1b;
      *xi2 = xi2b;
      *xi3 = xi3b;
      *xi4 = xi4b;
      *xm = xmb;
      return 0;	
    }

  xi1bk = xi1b;
  xi2bk = xi2b;
  xi3bk = xi3b;
  xi4bk = xi4b;
  xmbk = xmb;
  for(i = (xi1bk - bin > xi0) ? xi1bk - bin : xi0, start = clock(); i < xi5 - 1 && i < xi1bk + bin; i+= bin2)
    {
      for(j = (xi2bk - bin > i) ? xi2bk - bin : i; j < xi2bk + bin && j < xi5 - 1; j+= bin2) /* helicase phase must be there */
	{
	  for(k = (xi3bk - bin > j) ? xi3bk - bin : j; k < xi5 && k < xi3bk + bin; k+= bin2)
	    {
	      for(l = (xi4bk - bin > k) ? xi4bk - bin : k; l < xi5 && l < xi4bk + bin; l+= bin2)
		{
		  for(m = j; m < k; m += bin2)
		    {
		      ti += 1;
		      t =  fit_gp41_burst_xm(ds, xi0, i, j, k, l, xi5, m, &y1b, &y2b, &chi20);
		      if (t != 0 && t != 4) 
			{
			  display_title_message 
			    ("fit pb err. %d %d %d %d %d %d %d",t,xi0, i, j, k, l, xi5);
			  return t;	
			}
		      if (t != 4)
			{
			  if (first == 1 || chi20 < *chi2)
			    {
			      first = 0;
			      xi1b = i;
			      xi2b = j;
			      xi3b = k;
			      xi4b = l;
			      xmb = m;
			      *y1 = y1b;
			      *y2 = y2b;
			      *chi2 = chi20;
			      k = j;
			    }
			}
		      if (clock() - start > CLOCKS_PER_SEC/4)
			{
			  display_title_message("%4f %% \\chi^2 %g              ",(ti*100)/tt,*chi2);
			  start = clock();
			}
		    }
		}
	    }
	}
    }	

  if (bin2 == 1)
    {
      *xi1 = xi1b;
      *xi2 = xi2b;
      *xi3 = xi3b;
      *xi4 = xi4b;
      *xm = xmb;
      return 0;	
    }
  xi1bk = xi1b;
  xi2bk = xi2b;
  xi3bk = xi3b;
  xi4bk = xi4b;
  xmbk = xmb;
  for(i = (xi1bk - bin2 > xi0) ? xi1bk - bin2 : xi0, start = clock(); i < xi5 - 1 && i < xi1bk + bin2; i++)
    {
      for(j = (xi2bk - bin2 > i) ? xi2bk - bin2 : i; j < xi2bk + bin2 && j < xi5 - 1; j++) /* helicase phase must be there */
	{
	  for(k = (xi3bk - bin2 > j) ? xi3bk - bin2 : j; k < xi5 && k < xi3bk + bin2; k++)
	    {
	      for(l = (xi4bk - bin2 > k) ? xi4bk - bin2 : k; l < xi5 && l < xi4bk + bin2; l++)
		{
		  for(m = j; m < k; m ++)
		    {
		      ti += 1;
		      t = fit_gp41_burst_xm(ds, xi0, i, j, k, l, xi5, m, &y1b, &y2b, &chi20);
		      if (t != 0 && t != 4) 
			{
			  display_title_message 
			    ("fit pb err. %d %d %d %d %d %d %d",t,xi0, i, j, k, l, xi5);
			  return t;	
			}
		      if (t != 4)
			{
			  if (first == 1 || chi20 < *chi2)
			    {
			      first = 0;
			      *xi1 = i;
			      *xi2 = j;
			      *xi3 = k;
			      *xi4 = l;
			      *xm = m;
			      *y1 = y1b;
			      *y2 = y2b;
			      *chi2 = chi20;
			      k = j;
			    }
			}
		      if (clock() - start > CLOCKS_PER_SEC/4)
			{
			  display_title_message("%4f %% \\chi2 %g     ",(ti*100)/tt,*chi2);
			  start = clock();
			}
		    }
		}
	    }
	}
    }	
  return 0;
}


/*	find the index x_start et x_end of a data set according to the 
 *	boundary x_lo, x_hi:
 *
 */
int	find_x_selected_bounds(d_s *dsi, float x_lo, float x_hi, int *x_start, int *x_end)
{
  register int i;
  int nx, min, start, end;
	
  if (dsi == NULL)	return 1;
  nx = dsi->nx;
  min = (dsi->nx > dsi->ny) ? dsi->nx : dsi->ny;	
  for (start = i = min-1; i >= 0 ; i--)
    {
      if (dsi->xd[i] < x_hi && dsi->xd[i] >= x_lo)
	{
	  start = (i < start) ? i : start;
	}
    }	
  for ( end = i = 0; i < min ; i++)
    {
      if (dsi->xd[i] < x_hi && dsi->xd[i] >= x_lo)
	{
	  end = (i > end) ? i : end;
	}
    }		
  *x_start = start;
  *x_end = end;
  return 0;
}


int	create_burst_NHNZN_ds(O_p *op, int nburst, int xi0, int xi1, int xi2, int xi3,
			      int xi4,  int xi5, double y0, double y1, int dead_frac)
{	
  register int i;
  d_s *dsd, *dsi;
  char *cs, bead[512];
  time_t time;	
  float x0, x1,  zmag = -1;
  int ni;	 

	
  if (op->yu != NULL)
    {
      for (i=0 ; i< op->n_yu ; i++)
	{
	  if (op->yu[i] != NULL && op->yu[i]->type == IS_METER)
	    break;
	}
      if (i != op->n_yu && op->yu[i]->decade != IS_NANO)
	{
	  if (op->yu[i]->name != NULL)	free(op->yu[i]->name);
	  op->yu[i]->name = generate_units(IS_METER,IS_NANO);
	  op->yu[i]->dx *= pow(10,op->yu[i]->decade - IS_NANO);
	  op->yu[i]->decade = IS_NANO;
	  set_op_y_unit_set(op, i);
	}
    }	
  if ((op->yu == NULL) || i == op->n_yu)
      create_attach_select_y_un_to_op(op, IS_METER, 0,1000,IS_NANO,0, "nm");
	
	
	
  for (i = 0, dsi = NULL; i < op->n_dat && dsi == NULL; i++)
    {
      if (strstr(op->dat[i]->source,"Z bead1 - Z bead2") != NULL)
	  dsi = op->dat[i];
    }	
  if (dsi == NULL)      return win_printf("cannot find 2 beads data set!");
				
  if (strstr(dsi->source,"Z bead1 - Z bead2") != NULL)
    {
      cs = strstr(dsi->source,"Calibration from");
      if (cs != NULL) 	sscanf(cs+16,"%s",bead);
    }
  time = dsi->time;
  zmag = -1;	
	
	
  dsd = create_and_attach_one_ds(op, 32, 32, 0);
  if (dsd == NULL) 		return win_printf("cannot create ds !");
  dsd->nx = dsd->ny = 5;
  dsd->source =my_sprintf(NULL,"Helicase segments burst %d time %u\n"
			  "Bead %s\npath %s file %s\nZmag %g mm helicase FtsK [ATP] 0 \\mu M"
			  " Zcor 0.878\nstarting at %d -> %g Type"
			  ,nburst,time,backslash_to_slash(bead),
			  backslash_to_slash(op->dir),op->filename,
			  zmag,xi0,op->ax+op->dx*dsi->xd[xi0]);
	
  set_plot_symb(dsd,"\\pt5\\oc ");
  set_dash_line(dsd);		

  ni = 1;
  if (xi1 > xi0)
    {
      x0 = dsi->xd[xi0];
      x1 = dsi->xd[xi1];
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y0;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y0;			
      dsd->treatement =my_sprintf(dsd->treatement,"N");	
      ni += 4;				
    }				
  if (xi2 > xi1)
    {
      x0 = dsi->xd[xi1];
      x1 = dsi->xd[xi2];
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y0;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->yd[ni+1] += (y1 - y0)/dead_frac;
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->yd[ni+2] += (dead_frac-1)*(y1 - y0)/dead_frac;		
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y1;			
      dsd->treatement =my_sprintf(dsd->treatement,"U");	
      ni += 4;				
    }		
  if (xi3 > xi2)
    {
      x0 = dsi->xd[xi2];
      x1 = dsi->xd[xi3];
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y1;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y1;			
      dsd->treatement =my_sprintf(dsd->treatement,"N");	
      ni += 4;				
    }		
  if (xi4 > xi3)
    {
      x0 = dsi->xd[xi3];
      x1 = dsi->xd[xi4];
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y1;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->yd[ni+1] += (y0 - y1)/dead_frac;
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->yd[ni+2] += (dead_frac-1)*(y0 - y1)/dead_frac;		
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y0;			
      dsd->treatement =my_sprintf(dsd->treatement,"Z");	
      ni += 4;				
    }			
  if (xi5 > xi4)
    {
      x0 = dsi->xd[xi4];
      x1 = dsi->xd[xi5];
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y0;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y0;			
      dsd->treatement =my_sprintf(dsd->treatement,"N");	
      ni += 4;				
    }		
  dsd->nx = dsd->ny = ni;
  dsd->xd[0] = dsd->xd[1];		
  dsd->yd[0] = dsd->yd[1];			
  return 0;
}


int	fit_NHNZN_bump(void)
{
  register int i, dsni;
  O_p *op;
  d_s *dsi;
  pltreg *pr;
  int nf, xi0, xi1, xi2, xi3, xi4, xi5, bu, de;
  double y1, y2,  chi2x;
  static int nburst = -1, dead_frac = 1000;



  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut(0, KEY_F, 0, fit_NHNZN_bump);
      return D_O_K;	
    }

  if (key[KEY_LSHIFT])   
    if (win_printf_OK("This routine fit a NHNZN bump") == CANCEL)
      return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf("cannot find data");
	
  nf = dsi->nx;
  dsni = op->cur_dat;
  if (find_x_selected_bounds(dsi, op->x_lo, op->x_hi, &xi0, &xi5))
    return win_printf("cannot find boundary in data set");
			
			
  i = nearly_exhaustive_fit_of_NHNZN_phase(dsi, xi0, &xi1, &xi2, &xi3, &xi4, xi5,
					   &y1, &y2, &chi2x);
			

  if (i == 0)
    {
      if (nburst == -1 || key[KEY_LSHIFT])
	{
	  bu = (nburst < 0) ? 0 : nburst;
	  de = dead_frac;
	  i = win_scanf("Nb of burst %ddead fraction in burst %d",&bu,&de);
	  if (i == CANCEL)  return D_O_K;
	  dead_frac = de;
	  nburst = bu;
	}
      create_burst_NHNZN_ds(op, nburst++, xi0, xi1, xi2, xi3, xi4, xi5, y1, y2, dead_frac);
      op->cur_dat = dsni;			
    }	
  return refresh_plot(pr, UNCHANGED);		
}


int	create_gp41_burst(O_p *op, int nburst, int xi0, int xi1, int xi2, int xi3,
				int xi4,  int xi5, double y0, double y1, double yu, 
				double yz, int dead_frac)
{	
  register int i;
  d_s *dsd, *dsi;
  char *cs, bead[512];
  time_t time;	
  float x0, x1,  zmag = -1;
  int ni;	 

	
  if (op->yu != NULL)
    {
      for (i=0 ; i< op->n_yu ; i++)
	{
	  if (op->yu[i] != NULL && op->yu[i]->type == IS_METER)
	    break;
	}
      if (i != op->n_yu && op->yu[i]->decade != IS_NANO)
	{
	  if (op->yu[i]->name != NULL)	free(op->yu[i]->name);
	  op->yu[i]->name = generate_units(IS_METER,IS_NANO);
	  op->yu[i]->dx *= pow(10,op->yu[i]->decade - IS_NANO);
	  op->yu[i]->decade = IS_NANO;
	  set_op_y_unit_set(op, i);
	}
    }	
  if ((op->yu == NULL) || i == op->n_yu)
      create_attach_select_y_un_to_op(op, IS_METER, 0,1000,IS_NANO,0, "nm");
	
	
	
  for (i = 0, dsi = NULL; i < op->n_dat && dsi == NULL; i++)
    {
      if (strstr(op->dat[i]->source,"Z bead1 - Z bead2") != NULL)
	  dsi = op->dat[i];
    }	
  if (dsi == NULL)      return win_printf("cannot find 2 beads data set!");
				
  if (strstr(dsi->source,"Z bead1 - Z bead2") != NULL)
    {
      cs = strstr(dsi->source,"Calibration from");
      if (cs != NULL) 	sscanf(cs+16,"%s",bead);
    }
  time = dsi->time;
  zmag = -1;	
	
	
  dsd = create_and_attach_one_ds(op, 32, 32, 0);
  if (dsd == NULL) 		return win_printf("cannot create ds !");
  dsd->nx = dsd->ny = 5;
  dsd->source =my_sprintf(NULL,"Helicase segments burst %d time %u\n"
			  "Bead %s\npath %s file %s\nZmag %g mm helicase gp41 [ATP] 0 \\mu M"
			  " Zcor 0.878\nstarting at %d -> %g Type"
			  ,nburst,time,backslash_to_slash(bead),
			  backslash_to_slash(op->dir),op->filename,
			  zmag,xi0,op->ax+op->dx*dsi->xd[xi0]);
	
  set_plot_symb(dsd,"\\pt5\\oc ");
  set_dash_line(dsd);		

  ni = 1;
  if (xi1 > xi0)
    {
      x0 = dsi->xd[xi0];
      x1 = dsi->xd[xi1];
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y0;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y0;			
      dsd->treatement =my_sprintf(dsd->treatement,"N");	
      ni += 4;				
    }				
  if (xi2 > xi1)
    {
      x0 = dsi->xd[xi1];
      x1 = dsi->xd[xi2];
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y0;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->yd[ni+1] += (yu)/dead_frac;
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->yd[ni+2] += (dead_frac-1)*(yu)/dead_frac;		
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y0 + yu;			
      dsd->treatement =my_sprintf(dsd->treatement,"U");	
      ni += 4;				
    }		
  if (xi3 > xi2)
    {
      x0 = dsi->xd[xi2];
      x1 = dsi->xd[xi3];
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y1;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y1;			
      dsd->treatement =my_sprintf(dsd->treatement,"T");	
      ni += 4;				
    }		
  if (xi4 > xi3)
    {
      x0 = dsi->xd[xi3];
      x1 = dsi->xd[xi4];
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y0;
      dsd->yd[ni] += yz;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->yd[ni+1] += (dead_frac-1)*(yz)/dead_frac;		
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->yd[ni+2] += (yz)/dead_frac;
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y0;			
      dsd->treatement =my_sprintf(dsd->treatement,"Z");	
      ni += 4;				
    }			
  if (xi5 > xi4)
    {
      x0 = dsi->xd[xi4];
      x1 = dsi->xd[xi5];
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y0;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y0;			
      dsd->treatement =my_sprintf(dsd->treatement,"N");	
      ni += 4;				
    }		
  dsd->nx = dsd->ny = ni;
  dsd->xd[0] = dsd->xd[1];		
  dsd->yd[0] = dsd->yd[1];			
  return 0;
}


int	create_gp41_burst_xm(O_p *op, int nburst, int xi0, int xi1, int xi2, int xi3,
			      int xi4,  int xi5, int xim, double y0, double y1, int dead_frac)
{	
  register int i;
  d_s *dsd, *dsi;
  char *cs, bead[512];
  time_t time;	
  float x0, x1,  zmag = -1, yt, xm;
  int ni;	 

	
  if (op->yu != NULL)
    {
      for (i=0 ; i< op->n_yu ; i++)
	{
	  if (op->yu[i] != NULL && op->yu[i]->type == IS_METER)
	    break;
	}
      if (i != op->n_yu && op->yu[i]->decade != IS_NANO)
	{
	  if (op->yu[i]->name != NULL)	free(op->yu[i]->name);
	  op->yu[i]->name = generate_units(IS_METER,IS_NANO);
	  op->yu[i]->dx *= pow(10,op->yu[i]->decade - IS_NANO);
	  op->yu[i]->decade = IS_NANO;
	  set_op_y_unit_set(op, i);
	}
    }	
  if ((op->yu == NULL) || i == op->n_yu)
      create_attach_select_y_un_to_op(op, IS_METER, 0,1000,IS_NANO,0, "nm");
	
	
	
  for (i = 0, dsi = NULL; i < op->n_dat && dsi == NULL; i++)
    {
      if (strstr(op->dat[i]->source,"Z bead1 - Z bead2") != NULL)
	  dsi = op->dat[i];
    }	
  if (dsi == NULL)      return win_printf("cannot find 2 beads data set!");
				
  if (strstr(dsi->source,"Z bead1 - Z bead2") != NULL)
    {
      cs = strstr(dsi->source,"Calibration from");
      if (cs != NULL) 	sscanf(cs+16,"%s",bead);
    }
  time = dsi->time;
  zmag = -1;	
	
	
  dsd = create_and_attach_one_ds(op, 32, 32, 0);
  if (dsd == NULL) 		return win_printf("cannot create ds !");
  dsd->nx = dsd->ny = 5;
  dsd->source =my_sprintf(NULL,"Helicase segments burst %d time %u\n"
			  "Bead %s\npath %s file %s\nZmag %g mm helicase gp41 [ATP] 0 \\mu M"
			  " Zcor 0.878\nstarting at %d -> %g Type xm = %d"
			  ,nburst,time,backslash_to_slash(bead),
			  backslash_to_slash(op->dir),op->filename,
			  zmag,xi0,op->ax+op->dx*dsi->xd[xi0],xim);
	
  set_plot_symb(dsd,"\\pt5\\oc ");
  set_dash_line(dsd);		

  ni = 1;
  if (xi1 > xi0)
    {
      x0 = dsi->xd[xi0];
      x1 = dsi->xd[xi1];
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y0;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y0;			
      dsd->treatement =my_sprintf(dsd->treatement,"N");	
      ni += 4;				
    }				
  if (xi2 > xi1)
    {
      x0 = dsi->xd[xi1];
      x1 = dsi->xd[xi2];
      xm = dsi->xd[xim];
      yt = (y1 - y0)*(x1 - x0)/(xm - x0);
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y0;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->yd[ni+1] += yt/dead_frac;
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->yd[ni+2] += (dead_frac-1)*yt/dead_frac;		
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y0 + yt;			
      dsd->treatement =my_sprintf(dsd->treatement,"U");	
      ni += 4;				
    }		
  if (xi3 > xi2)
    {
      x0 = dsi->xd[xi2];
      x1 = dsi->xd[xi3];
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y1;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y1;			
      dsd->treatement =my_sprintf(dsd->treatement,"T");	
      ni += 4;				
    }		
  if (xi4 > xi3)
    {
      x0 = dsi->xd[xi3];
      x1 = dsi->xd[xi4];
      xm = dsi->xd[xim];
      yt = (y1 - y0)*(x1 - x0)/(x1 - xm);
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y0;
      dsd->yd[ni] += yt;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->yd[ni+1] += (dead_frac-1)*yt/dead_frac;		
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->yd[ni+2] += yt/dead_frac;
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y0;			
      dsd->treatement =my_sprintf(dsd->treatement,"Z");	
      ni += 4;				
    }			
  if (xi5 > xi4)
    {
      x0 = dsi->xd[xi4];
      x1 = dsi->xd[xi5];
      dsd->xd[ni] = dsd->xd[ni+1] = dsd->xd[ni+2] = x0;
      dsd->yd[ni] = dsd->yd[ni+1] = dsd->yd[ni+2] = y0;
      dsd->xd[ni+1] += (x1 - x0)/dead_frac;
      dsd->xd[ni+2] += (dead_frac-1)*(x1 - x0)/dead_frac;
      dsd->xd[ni+3] = x1;
      dsd->yd[ni+3] = y0;			
      dsd->treatement =my_sprintf(dsd->treatement,"N");	
      ni += 4;				
    }
  x1 = dsi->xd[xi5];
  dsd->xd[ni] = x1;
  dsd->yd[ni] = y0;	
  x0 = dsi->xd[xi4];
  dsd->xd[ni+1] = x0;
  dsd->yd[ni+1] = y0;	
  xm = dsi->xd[xim];	
  dsd->xd[ni+2] = xm;
  dsd->yd[ni+2] = y1;		
  dsd->xd[ni+3] = xm;
  dsd->yd[ni+3] = y1;		
  x0 = dsi->xd[xi1];
  dsd->xd[ni+4] = x0;
  dsd->yd[ni+4] = y0;		
  ni += 5;						
  dsd->nx = dsd->ny = ni;
  dsd->xd[0] = dsd->xd[1];		
  dsd->yd[0] = dsd->yd[1];			
  return 0;
}


int	do_fit_gp41_burst_xm(void)
{
  register int i, dsni;
  O_p *op;
  d_s *dsi;
  pltreg *pr;
  int nf, xi0, xi1, xi2, xi3, xi4, xi5, bu, de, xm;
  double y1, y2,  chi2x;
  static int nburst = -1, dead_frac = 1000;



  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut(0, KEY_G, 0, do_fit_gp41_burst_xm);
      return D_O_K;	
    }

  if (key[KEY_LSHIFT])   
    if (win_printf_OK("This routine fit a gp41 burst with xm") == CANCEL)
      return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf("cannot find data");
	
  nf = dsi->nx;
  dsni = op->cur_dat;
  if (find_x_selected_bounds(dsi, op->x_lo, op->x_hi, &xi0, &xi5))
    return win_printf("cannot find boundary in data set");
			
			
  i = nearly_exhaustive_fit_of_gp41_burst_xm(dsi, xi0, &xi1, &xi2, &xi3, &xi4, xi5, &xm,
					   &y1, &y2, &chi2x);
			

  if (i == 0)
    {
      if (nburst == -1 || key[KEY_LSHIFT])
	{
	  bu = (nburst < 0) ? 0 : nburst;
	  de = dead_frac;
	  i = win_scanf("Nb of burst %ddead fraction in burst %d",&bu,&de);
	  if (i == CANCEL)  return D_O_K;
	  dead_frac = de;
	  nburst = bu;
	}
      create_gp41_burst_xm(op, nburst++, xi0, xi1, xi2, xi3, xi4, xi5, xm, y1, y2, dead_frac);
      op->cur_dat = dsni;			
    }	
  return refresh_plot(pr, UNCHANGED);		
}

int	do_fit_gp41_burst(void)
{
  register int i, dsni;
  O_p *op;
  d_s *dsi;
  pltreg *pr;
  int nf, xi0, xi1, xi2, xi3, xi4, xi5, bu, de;
  double y1, y2, yu, yt, chi2x;
  static int nburst = -1, dead_frac = 1000;



  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut(0, KEY_H, 0, do_fit_gp41_burst);
      return D_O_K;	
    }

  if (key[KEY_LSHIFT])   
    if (win_printf_OK("This routine fit a simple gp41 burst") == CANCEL)
      return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf("cannot find data");
	
  nf = dsi->nx;
  dsni = op->cur_dat;
  if (find_x_selected_bounds(dsi, op->x_lo, op->x_hi, &xi0, &xi5))
    return win_printf("cannot find boundary in data set");
			
			
  i = nearly_exhaustive_fit_of_gp41_burst(dsi, xi0, &xi1, &xi2, &xi3, &xi4, xi5,
					   &y1, &y2, &yu, &yt, &chi2x);
			

  if (i == 0)
    {
      if (nburst == -1 || key[KEY_LSHIFT])
	{
	  bu = (nburst < 0) ? 0 : nburst;
	  de = dead_frac;
	  i = win_scanf("Nb of burst %ddead fraction in burst %d",&bu,&de);
	  if (i == CANCEL)  return D_O_K;
	  dead_frac = de;
	  nburst = bu;
	}
      create_gp41_burst(op, nburst++, xi0, xi1, xi2, xi3, xi4, xi5, y1, y2, yu, yt, dead_frac);
      op->cur_dat = dsni;			
    }	
  return refresh_plot(pr, UNCHANGED);		
}

int	find_previous_burst(void)
{
  register int i, j, k;
  O_p *op;
  d_s *dsi;
  pltreg *pr;
  int nf, xi0, xi5, dsni, i_1, i1;
  float thres;
  static int idir = 0, dir = 1;
  static float threshold = FLT_MAX;

  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut(0, KEY_P, 0, find_previous_burst);
      return D_O_K;	
    }

  if (key[KEY_LSHIFT])   
    if (win_printf_OK("This routine try to catch the next burst") == CANCEL)
      return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf("cannot find data");
	
  nf = dsi->nx;
  dsni = op->cur_dat;
  if (find_x_selected_bounds(dsi, op->x_lo, op->x_hi, &xi0, &xi5))
    return win_printf("cannot find boundary in data set");
  j = xi5 - xi0;
  if (threshold == FLT_MAX || key[KEY_LSHIFT])
    {
      thres = 0;
      i = win_scanf("Enter the threshold value for burst %8f\n"
		    "rising %R or falling %r edge\n",&thres, &idir);
      if (i == CANCEL)  return D_O_K;
      threshold = thres;
    }
  dir = (idir == 0) ? 1 : -1;
  for (i = xi0, k = 1; i > 0 && k == 1; i--)
    {
      i_1 = (i > 0) ? i - 1 : i;
      i1 = (i < dsi->nx - 1) ? i + 1 : i;
      k = (((dsi->yd[i1] - threshold) * (threshold - dsi->yd[i_1]) >= 0)
	   && ((dsi->yd[i1] - dsi->yd[i_1]) * dir >= 0)) ? -1 : 1;
    }
  xi0 = i - j/2;
  xi0 = (xi0 > 0) ? xi0 : 0;
  xi5 = xi0 + j;
  xi5 = (xi5 < dsi->nx) ? xi5 : dsi->nx - 1;
  op->x_lo = dsi->xd[xi0];
  op->x_hi = dsi->xd[xi5];
  op->need_to_refresh = 1;
  return refresh_plot(pr, UNCHANGED);			
}


int	find_next_burst(void)
{
  register int i, j, k;
  O_p *op;
  d_s *dsi;
  pltreg *pr;
  int nf, xi0, xi5, dsni, i_1, i1;
  float thres;
  static int idir = 0, dir = 1;
  static float threshold = FLT_MAX;

  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut(0, KEY_N, 0, find_next_burst);
      add_keyboard_short_cut(0, KEY_P, 0, find_previous_burst);
      return D_O_K;	
    }

  if (key[KEY_LSHIFT])   
    if (win_printf_OK("This routine try to catch the next burst") == CANCEL)
      return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf("cannot find data");
	
  nf = dsi->nx;
  dsni = op->cur_dat;
  if (find_x_selected_bounds(dsi, op->x_lo, op->x_hi, &xi0, &xi5))
    return win_printf("cannot find boundary in data set");
  j = xi5 - xi0;
  if (threshold == FLT_MAX || key[KEY_LSHIFT])
    {
      thres = 0;
      i = win_scanf("Enter the threshold value for burst %8f\n"
		    "rising %R or falling %r edge\n",&thres, &idir);
      if (i == CANCEL)  return D_O_K;
      threshold = thres;
    }
  dir = (idir == 0) ? 1 : -1;
  for (i = xi5, k = 1; i < dsi->nx && k == 1; i++)
    {
      i_1 = (i > 0) ? i - 1 : i;
      i1 = (i < dsi->nx - 1) ? i + 1 : i;
      k = (((dsi->yd[i1] - threshold) * (threshold - dsi->yd[i_1]) >= 0)
	   && ((dsi->yd[i1] - dsi->yd[i_1]) * dir >= 0)) ? -1 : 1;
    }
  xi0 = i - j/2;
  xi0 = (xi0 > 0) ? xi0 : 0;
  xi5 = xi0 + j;
  xi5 = (xi5 < dsi->nx) ? xi5 : dsi->nx - 1;
  op->x_lo = dsi->xd[xi0];
  op->x_hi = dsi->xd[xi5];
  op->need_to_refresh = 1;
  return refresh_plot(pr, UNCHANGED);			
}


MENU *select_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;



  add_item_to_menu(mn,"separate vs Zmag", separate_trajectories_by_zmag,NULL,0,NULL);
  add_item_to_menu(mn,"continuous trajectories vs Zmag", separate_trajectories_by_zmag_cte,NULL,0,NULL);

  add_item_to_menu(mn,"\0",	    NULL,  NULL,	0, NULL);
  add_item_to_menu(mn,"simple segment", do_select_pr_simple_segment,NULL,0,NULL);
  add_item_to_menu(mn,"grab simple segment", grab_simple_segment_at_cst_zmag,NULL,0,NULL);
  add_item_to_menu(mn,"grab single segment", grab_single_segment_at_cst_zmag,NULL,0,NULL);
  add_item_to_menu(mn,"grab segment duration", grab_simple_segment_duration,NULL,0,NULL);

  add_item_to_menu(mn,"\0",	    NULL,  NULL,	0, NULL);
  add_item_to_menu(mn,"select segment", do_select_pr_segment,NULL,0,NULL);
  add_item_to_menu(mn,"select bump", do_select_pr_bump_segment,NULL,0,NULL);
  add_item_to_menu(mn,"Dump bump",dump_bump_signal_to_files,NULL,0,NULL);
  add_item_to_menu(mn,"Analyze bump",analyze_bump_signal_from_files,NULL,0,NULL);
  add_item_to_menu(mn,"\0",	    NULL,  NULL,	0, NULL);
  add_item_to_menu(mn,"select burst", do_select_pr_burst_segment,NULL,0,NULL);
  add_item_to_menu(mn,"Dump burst",dump_burst_signal_to_files,NULL,0,NULL);
  add_item_to_menu(mn,"Analyze burst",analyze_burst_signal_from_files,NULL,0,NULL);
  add_item_to_menu(mn,"\0",	    NULL,  NULL,	0, NULL);
  add_item_to_menu(mn,"simulate helicase", simulate_helicase,NULL,0,NULL);

# ifdef OLD	
  add_item_to_menu(mn,"zoom in X",do_zoom_in_x_on_op,NULL,0,NULL);
  add_item_to_menu(mn,"zoom out X",do_unzoom_in_x_on_op,NULL,0,NULL);
  add_item_to_menu(mn,"<< x <<",do_move_in_x_left2,NULL,MENU_INDEX(-2),NULL);
  add_item_to_menu(mn,"< x <",do_move_in_x_left,NULL,MENU_INDEX(-1),NULL);
  add_item_to_menu(mn,"> x >",do_move_in_x_right,NULL,MENU_INDEX(1),NULL);

  add_item_to_menu(mn,">> x >>",do_move_in_x_right2,NULL,MENU_INDEX(2),NULL);
  add_item_to_menu(mn,"adj. y lim.",do_adjust_y_lim_of_cur_ds_visi,NULL,0,NULL);
# endif





  add_item_to_menu(mn,"Analyze NUNZN",analyze_NUNZN_burst_signal_from_files,NULL,0,NULL);
  add_item_to_menu(mn,"Noise heli",do_histo_noise_in_time,NULL,0,NULL);
  add_item_to_menu(mn,"fit NHNZN bump",fit_NHNZN_bump,NULL,0,NULL);
  add_item_to_menu(mn,"fit gp41 xm",do_fit_gp41_burst_xm,NULL,0,NULL);
  add_item_to_menu(mn,"fit gp41",do_fit_gp41_burst,NULL,0,NULL);
  add_item_to_menu(mn,"next burst",find_next_burst,NULL,0,NULL);
  return mn;
}

int	select_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "select", NULL, select_plot_menu(), 0, NULL);
  /*	plot_wheel_action = select_plot_wheel_action;*/
  return D_O_K;
}
int	select_unload(int argc, char **argv)
{ 
  remove_item_to_menu(image_treat_menu, "select", NULL, NULL);
  return D_O_K;
}

#endif
