#ifndef _XVFFMPEG_H_
#define _XVFFMPEG_H_

PXV_FUNC(int, do_xvFfmpeg_average_along_y, (void));
PXV_FUNC(MENU*, xvFfmpeg_image_menu, (void));
PXV_FUNC(int, xvFfmpeg_main, (int argc, char **argv));
#endif

