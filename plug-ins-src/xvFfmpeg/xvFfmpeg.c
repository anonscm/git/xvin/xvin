/*
 *    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _XVFFMPEG_C_
#define _XVFFMPEG_C_

# include "allegro.h"
# include "xvin.h"
# include <stdio.h>

/* If you include other regular header do it here*/

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>

#include <libavutil/opt.h>
#include <libavutil/channel_layout.h>
#include <libavutil/common.h>
#include <libavutil/imgutils.h>
#include <libavutil/mathematics.h>
#include <libavutil/samplefmt.h>



#include <libavutil/avassert.h>
#include <libavutil/channel_layout.h>
#include <libavutil/opt.h>
#include <libavutil/timestamp.h>
#include <libswresample/swresample.h>


// a wrapper around a single output AVStream
typedef struct OutputStream {
    AVStream *st;
    AVCodecContext *enc;
    /* pts of the next frame that will be generated */
    int64_t next_pts;
    int samples_count;
    AVFrame *frame;
    AVFrame *tmp_frame;
    float t, tincr, tincr2;
    struct SwsContext *sws_ctx;
    struct SwrContext *swr_ctx;
} OutputStream;



/* Needed dll

avcodec-56.dll
libcelt0-2.dll
libdcadec-0.dll
libgsm.dll
libopencore-amrnb-0.dll
libopencore-amrwb-0.dll
libopenjpeg-5.dll
libopus-0.dll
libschroedinger-1.0-0.dll
liborc-0.4-0.dll
libspeex-1.dll
libtheoradec-1.dll
libtheoraenc-1.dll
libvorbis-0.dll
libvorbisenc-2.dll
libvpx-1.dll
libwavpack-1.dll
libx264-146.dll
libx265.dll
xvidcore.dll
avcodec-56.dll
avutil-54.dll
swresample-1.dll
avformat-56.dll
libbluray-1.dll
libxml2-2.dll
libgnutls-30.dll
libgmp-10.dll
libhogweed-4-1.dll
libnettle-6-1.dll
libidn-11.dll
libp11-kit-0.dll
libtasn1-6.dll
libmodplug-1.dll
librtmp-1.dll
swscale-3.dll
*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "xvFfmpeg.h"
//place here other headers of this plugin
int do_xvFfmpeg_average_along_y(void)
{
  register int i, j;
  int l_s, l_e;
  O_i *ois = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  imreg *imr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average the image intensity"
			   "of several lines of an image");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return D_O_K;
  /* we grap the data that we need, the image and the screen region*/
  l_s = 0; l_e = ois->im.ny;
  /* we ask for the limit of averaging*/
  i = win_scanf("Average Between lines%d and %d",&l_s,&l_e);
  if (i == WIN_CANCEL)	return D_O_K;
  /* we create a plot to hold the profile*/
  op = create_one_plot(ois->im.nx,ois->im.nx,0);
  if (op == NULL) return (win_printf_OK("cant create plot!"));
  ds = op->dat[0]; /* we grab the data set */
  op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
  op->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;
  inherit_from_im_to_ds(ds, ois);
  op->filename = Transfer_filename(ois->filename);
  set_plot_title(op,"xvFfmpeg averaged profile from line %d to %d",l_s, l_e);
  set_formated_string(&ds->treatement,"averaged profile from line %d to %d",l_s, l_e);
  uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
  uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  for (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;
  for (i=l_s ; i< l_e ; i++)
    {
      extract_raw_line(ois, i, ds->xd);
      for (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];
    }
  for (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)
    {
      ds->xd[i] = i;
      ds->yd[i] /= j;
    }
  add_one_image (ois, IS_ONE_PLOT, (void *)op);
  ois->cur_op = ois->n_op - 1;
  broadcast_dialog_message(MSG_DRAW,0);
  return D_REDRAWME;
}

O_i	*xvFfmpeg_image_multiply_by_a_scalar(O_i *ois, float factor)
{
  register int i, j;
  O_i *oid = NULL;
  float tmp;
  int onx, ony, data_type;
  union pix *ps = NULL, *pd = NULL;

  onx = ois->im.nx;	ony = ois->im.ny;	data_type = ois->im.data_type;
  oid =  create_one_image(onx, ony, data_type);
  if (oid == NULL)
    {
      win_printf("Can't create dest image");
      return NULL;
    }
  if (data_type == IS_CHAR_IMAGE)
    {
      for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      tmp = factor * ps[i].ch[j];
	      pd[i].ch[j] = (tmp < 0) ? 0 :
		((tmp > 255) ? 255 : (unsigned char)tmp);
	    }
	}
    }
  else if (data_type == IS_INT_IMAGE)
    {
      for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      tmp = factor * ps[i].in[j];
	      pd[i].in[j] = (tmp < -32768) ? -32768 :
		((tmp > 32767) ? 32767 : (short int)tmp);
	    }
	}
    }
  else if (data_type == IS_UINT_IMAGE)
    {
      for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      tmp = factor * ps[i].ui[j];
	      pd[i].ui[j] = (tmp < 0) ? 0 :
		((tmp > 65535) ? 65535 : (unsigned short int)tmp);
	    }
	}
    }
  else if (data_type == IS_LINT_IMAGE)
    {
      for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      tmp = factor * ps[i].li[j];
	      pd[i].li[j] = (tmp < 0x10000000) ? 0x10000000 :
		((tmp > 0x7FFFFFFF) ? 0x7FFFFFFF : (int)tmp);
	    }
	}
    }
  else if (data_type == IS_FLOAT_IMAGE)
    {
      for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      pd[i].fl[j] = factor * ps[i].fl[j];
	    }
	}
    }
  else if (data_type == IS_DOUBLE_IMAGE)
    {
      for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      pd[i].db[j] = factor * ps[i].db[j];
	    }
	}
    }
  else if (data_type == IS_COMPLEX_IMAGE)
    {
      for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      pd[i].fl[2*j] = factor * ps[i].fl[2*j];
	      pd[i].fl[2*j+1] = factor * ps[i].fl[2*j+1];
	    }
	}
    }
  else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
      for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	{
	  for (j=0; j< onx; j++)
	    {
	      pd[i].db[2*j] = factor * ps[i].db[2*j];
	      pd[i].db[2*j+1] = factor * ps[i].db[2*j+1];
	    }
	}
    }
  else if (data_type == IS_RGB_PICTURE)
    {
      for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	{
	  for (j=0; j< 3*onx; j++)
	    {
	      tmp = factor * ps[i].ch[j];
	      pd[i].ch[j] = (tmp < 0) ? 0 :
		((tmp > 255) ? 255 : (unsigned char)tmp);
	    }
	}
    }
  else if (data_type == IS_RGBA_PICTURE)
    {
      for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	{
	  for (j=0; j< 4*onx; j++)
	    {
	      tmp = factor * ps[i].ch[j];
	      pd[i].ch[j] = (tmp < 0) ? 0 :
		((tmp > 255) ? 255 : (unsigned char)tmp);
	    }
	}
    }
  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s rescaled by %g",
					     ois->title,factor);
  set_formated_string(&oid->im.treatement,
		      "Image %s rescaled by %g", ois->filename,factor);
  return oid;
}

int do_xvFfmpeg_read_movie(void)
{
  char movfilename[512] = {0};
  O_i *oid = NULL;
  imreg *imr = NULL;
  int i;
  //static float factor = 2.0;
  //static int first = 1;
  AVFormatContext *pFormatCtx = NULL;
  int videoStream;
  AVCodecContext *pCodecCtx = NULL;
  AVCodec *pCodec = NULL;
  AVFrame *pFrame = NULL;
  AVFrame *pFrameRGB = NULL;
  AVPacket packet;
  int frameFinished;
  //int numBytes;
  //uint8_t *buffer = NULL;
  AVDictionary *optionsDict = NULL;
  struct SwsContext *sws_ctx = NULL;

   if(updating_menu_state != 0)	return D_O_K;
   if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  movfilename[0] = 0;
     if (do_select_file(movfilename, sizeof(movfilename), "Movie file", "*.avi;*.mpg;*.mov;*.mp4", "File to load movie", 1))
    return win_printf_OK("Cannot select input file");

  i = win_printf("File selected %s",backslash_to_slash(movfilename));
  if (i == WIN_CANCEL)	return D_O_K;

  // Register all formats and codecs
  av_register_all();
  // Open video file
  if(avformat_open_input(&pFormatCtx, movfilename, NULL, NULL)!=0)
    return win_printf("Cannot open File:\n %s",backslash_to_slash(movfilename));
  // Retrieve stream information
  if(avformat_find_stream_info(pFormatCtx, NULL)<0)
    return win_printf("Cannot open stream in File:\n %s",backslash_to_slash(movfilename));
  // Dump information about file onto standard error
  //av_dump_format(pFormatCtx, 0, argv[1], 0);
  // Find the first video stream
  videoStream=-1;
  for(i=0; i<(int)pFormatCtx->nb_streams; i++)
    if(pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO) {
      videoStream=i;
      break;
    }
  if(videoStream==-1)
    return win_printf("Cannot find a video stream in File:\n %s",backslash_to_slash(movfilename));
  // Get a pointer to the codec context for the video stream
  pCodecCtx=pFormatCtx->streams[videoStream]->codec;
  // Find the decoder for the video stream
  pCodec=avcodec_find_decoder(pCodecCtx->codec_id);
  if(pCodec==NULL)    return win_printf("Unsupported Codec in File:\n %s",backslash_to_slash(movfilename));
  // Open codec
  if(avcodec_open2(pCodecCtx, pCodec, &optionsDict)<0)
    return win_printf("could not open Codec in File:\n %s",backslash_to_slash(movfilename));
  win_printf("Movie %d x %d",pCodecCtx->width,pCodecCtx->height);
  // pCodecCtx->duration
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
      return win_printf_OK("Cannot find image region");
  oid =  create_and_attach_oi_to_imr(imr,pCodecCtx->width,pCodecCtx->height, IS_RGB_PICTURE);
  if (oid == NULL)	return win_printf_OK("unable to create image!");
  map_pixel_ratio_of_image_and_screen(oid, 1.0, 1.0);
  set_zmin_zmax_values(oid, 0, 255);
  set_z_black_z_white_values(oid, 0, 255);

  pFrame=av_frame_alloc();
  // Allocate an AVFrame structure
  pFrameRGB=av_frame_alloc();
  if(pFrameRGB == NULL)
    return win_printf_OK("Cannot allocate pFrameRGB");
  // Determine required buffer size and allocate buffer
  /*
  numBytes=avpicture_get_size(PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);
  buffer=(uint8_t *)av_malloc(numBytes*sizeof(uint8_t));
  */
  sws_ctx = sws_getContext(pCodecCtx->width, pCodecCtx->height,pCodecCtx->pix_fmt,
			   pCodecCtx->width, pCodecCtx->height, AV_PIX_FMT_RGB24,
			   SWS_BILINEAR,     NULL,      NULL,     NULL    );
  // Assign appropriate parts of buffer to image planes in pFrameRGB
  // Note that pFrameRGB is an AVFrame, but AVFrame is a superset
  // of AVPicture
  avpicture_fill((AVPicture *)pFrameRGB, oid->im.mem[0], AV_PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);
  // Read frames and save first five frames to disk
  i=0;
  while(av_read_frame(pFormatCtx, &packet)>=0) {
    // Is this a packet from the video stream?
    if(packet.stream_index==videoStream) {
      // Decode video frame
      avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished,  &packet);
      // Did we get a video frame?
      if(frameFinished) {
	// Convert the image from its native format to RGB
	sws_scale
	  (
	   sws_ctx,
	   (uint8_t const * const *)pFrame->data,
	   pFrame->linesize,
	   0,
	   pCodecCtx->height,
	   pFrameRGB->data,
	   pFrameRGB->linesize
	   );
	oid->need_to_refresh |= ALL_NEED_REFRESH;
	refresh_image(imr, imr->n_oi - 1);
	i++;
      }
    }
    // Free the packet that was allocated by av_read_frame
    av_free_packet(&packet);
  }
  win_printf("movie has %d frames\n",i);
  // Free the RGB image
  //av_free(buffer);
  av_free(pFrameRGB);
  // Free the YUV frame
  av_free(pFrame);
  // Close the codec
  avcodec_close(pCodecCtx);
  // Close the video file
  avformat_close_input(&pFormatCtx);

  //find_zmin_zmax(oid);
  return (refresh_image(imr, imr->n_oi - 1));
}


int do_xvFfmpeg_read_movie_2(void)
{
  char movfilename[512] = {0};
  O_i *oid = NULL, *oitmp = NULL;
  imreg *imr = NULL;
  int i;
  static int start = 0, end = 100, rgb = 1;
  //static float factor = 2.0;
  //static int first = 1;
  AVFormatContext *pFormatCtx = NULL;
  int videoStream;
  AVCodecContext *pCodecCtx = NULL;
  AVCodec *pCodec = NULL;
  AVFrame *pFrame = NULL;
  AVFrame *pFrameRGB = NULL;
  AVPacket packet;
  int frameFinished;
  //int numBytes;
  //uint8_t *buffer = NULL;
  AVDictionary *optionsDict = NULL;
  struct SwsContext *sws_ctx = NULL;
  static int partial_size = 0, xc = 512,yc = 512, w = 256, h = 256;

   if(updating_menu_state != 0)	return D_O_K;
   if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  movfilename[0] = 0;
     if (do_select_file(movfilename, sizeof(movfilename), "Movie file", "*.avi;*.mpg;*.mov;*.mp4", "File to load movie", 1))
    return win_printf_OK("Cannot select input file");

  //i = win_printf("File selected %s",backslash_to_slash(movfilename));
  //if (i == WIN_CANCEL)	return D_O_K;

  // Register all formats and codecs
  av_register_all();
  // Open video file
  if(avformat_open_input(&pFormatCtx, movfilename, NULL, NULL)!=0)
    return win_printf("Cannot open File:\n %s",backslash_to_slash(movfilename));
  // Retrieve stream information
  if(avformat_find_stream_info(pFormatCtx, NULL)<0)
    return win_printf("Cannot open stream in File:\n %s",backslash_to_slash(movfilename));
  // Dump information about file onto standard error
  //av_dump_format(pFormatCtx, 0, argv[1], 0);
  // Find the first video stream
  videoStream=-1;
  for(i=0; i<(int)pFormatCtx->nb_streams; i++)
    if(pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO) {
      videoStream=i;
      break;
    }
  if(videoStream==-1)
    return win_printf("Cannot find a video stream in File:\n %s",backslash_to_slash(movfilename));
  // Get a pointer to the codec context for the video stream
  pCodecCtx=pFormatCtx->streams[videoStream]->codec;
  // Find the decoder for the video stream
  pCodec=avcodec_find_decoder(pCodecCtx->codec_id);
  if(pCodec==NULL)    return win_printf("Unsupported Codec in File:\n %s",backslash_to_slash(movfilename));
  // Open codec
  if(avcodec_open2(pCodecCtx, pCodec, &optionsDict)<0)
    return win_printf("could not open Codec in File:\n %s",backslash_to_slash(movfilename));
  win_printf("Movie %d x %d",pCodecCtx->width,pCodecCtx->height);
  // pCodecCtx->duration
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
      return win_printf_OK("Cannot find image region");

  i = win_scanf("Store in ram from frame %6d to %6d\n"
		"BW %R or RGB %r\n"
                "%R full size\n"
                "%r partial size\n"
                "xc %4d yc %4d\n"
                "Width %4d height %4d\n"
                ,&start,&end,&rgb,&partial_size,&xc,&yc,&w,&h);
  if (i == WIN_CANCEL) return 0;

  if (partial_size == 0)
    {
       oid = create_and_attach_movie_to_imr(imr,pCodecCtx->width,pCodecCtx->height,					((rgb)?IS_RGB_PICTURE:IS_CHAR_IMAGE),end-start);
    }
  else 
    {
       oid = create_and_attach_movie_to_imr(imr,w,h,
                            (rgb)?IS_RGB_PICTURE:IS_CHAR_IMAGE,end-start);
       oitmp = create_one_image(pCodecCtx->width,pCodecCtx->height,					((rgb)?IS_RGB_PICTURE:IS_CHAR_IMAGE));
    }
  if (oid == NULL)	return win_printf_OK("unable to create movie!");
  if (partial_size  > 0 && oitmp == NULL) 
          return win_printf_OK("unable to create tmp image!");
  map_pixel_ratio_of_image_and_screen(oid, 1.0, 1.0);
  set_zmin_zmax_values(oid, 0, 255);
  set_z_black_z_white_values(oid, 0, 255);

  pFrame=av_frame_alloc();
  // Allocate an AVFrame structure
  pFrameRGB=av_frame_alloc();
  if(pFrameRGB == NULL)
    return win_printf_OK("Cannot allocate pFrameRGB");
  // Determine required buffer size and allocate buffer
  /*
  numBytes=avpicture_get_size(PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);
  buffer=(uint8_t *)av_malloc(numBytes*sizeof(uint8_t));
  */
  sws_ctx = sws_getContext(pCodecCtx->width, pCodecCtx->height,pCodecCtx->pix_fmt,
			   pCodecCtx->width, pCodecCtx->height,			   ((rgb)?AV_PIX_FMT_RGB24:AV_PIX_FMT_GRAY8),
			   SWS_BILINEAR,     NULL,      NULL,     NULL    );
  // Assign appropriate parts of buffer to image planes in pFrameRGB
  // Note that pFrameRGB is an AVFrame, but AVFrame is a superset
  // of AVPicture
  avpicture_fill((AVPicture *)pFrameRGB, oid->im.mem[0], ((rgb)?AV_PIX_FMT_RGB24:AV_PIX_FMT_GRAY8), pCodecCtx->width, pCodecCtx->height);
  // Read frames and save first five frames to disk
  i=0;
  while(av_read_frame(pFormatCtx, &packet) >= 0 && i < end) {
    // Is this a packet from the video stream?
    if(packet.stream_index==videoStream) {
      // Decode video frame
      avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished,  &packet);
      // Did we get a video frame?
      if(frameFinished) {
	// Convert the image from its native format to RGB
	sws_scale
	  (
	   sws_ctx,
	   (uint8_t const * const *)pFrame->data,
	   pFrame->linesize,
	   0,
	   pCodecCtx->height,
	   pFrameRGB->data,
	   pFrameRGB->linesize
	   );
	if (i > start && i < end) switch_frame(oid,i - start);
	oid->need_to_refresh |= ALL_NEED_REFRESH;
	refresh_image(imr, imr->n_oi - 1);
	i++;
	if (i >= start && i < end)
          {
             if (partial_size == 0)
                avpicture_fill((AVPicture *)pFrameRGB, oid->im.mem[i-start], ((rgb)?AV_PIX_FMT_RGB24:AV_PIX_FMT_GRAY8), pCodecCtx->width, pCodecCtx->height);
             else
               {
                  avpicture_fill((AVPicture *)pFrameRGB, oitmp->im.mem[0], ((rgb)?AV_PIX_FMT_RGB24:AV_PIX_FMT_GRAY8), pCodecCtx->width, pCodecCtx->height);
                  switch_frame(oid,i-start);
                  copy_one_subset_of_image_in_a_bigger_one(oid, 0, 0, oitmp, xc - w/2, yc - h/2, w, h);
               }
             display_title_message("loading image %d",i);
          }
      }
    }
    // Free the packet that was allocated by av_read_frame
    av_free_packet(&packet);
  }
  fprintf(stderr,"movie has %d frames\n",i);
  // Free the RGB image
  //av_free(buffer);
  av_free(pFrameRGB);
  // Free the YUV frame
  av_free(pFrame);
  // Close the codec
  avcodec_close(pCodecCtx);
  // Close the video file
  avformat_close_input(&pFormatCtx);
  if (partial_size  > 0 && oitmp == NULL) 
        free_one_image(oitmp);
  //find_zmin_zmax(oid);
  return (refresh_image(imr, imr->n_oi - 1));
  }


int do_encode_h264_movie(void)
{
   // Output file
    const char  fullfilename[512] = {0};
    imreg *imr = NULL;
    O_i *ois = NULL;
    int nfi, im;
    float tmp;
// Audio and Video Timestamps (presentation time stamp)
    int codec_id = AV_CODEC_ID_H264, codec_id2 = AV_CODEC_ID_MPEG4;
    AVCodec *codec = NULL;
    AVCodecContext *c= NULL;
    int i, ret, x, y, got_output;
    FILE *f;
    AVFrame *frame = NULL;
    AVPacket pkt;
    uint8_t endcode[] = { 0, 0, 1, 0xb7 };
    float compression_factor = 0.1;
    static int mp4 = 0;


    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
        return D_O_K;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;     if(updating_menu_state != 0)      {
        if (nfi == 1) 	active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

   if (ois->im.data_type != IS_RGB_PICTURE && ois->im.data_type != IS_CHAR_IMAGE)
        return win_printf_OK("I can only handle RGB or BW movies!");


   //i = win_scanf("FOrmat %Rh264 or %rMP4",&mp4);
   i = win_scanf("Compression factor %4f",&compression_factor);
   if (i == WIN_CANCEL) return 0;

   codec_id = (mp4) ? AV_CODEC_ID_MPEG4  : AV_CODEC_ID_H264;
   codec = avcodec_find_encoder(codec_id);
   if (!codec) {
           return win_printf_OK("Codec not found\n");
   }

   build_full_file_name(fullfilename, sizeof(fullfilename), (const char*)ois->dir, (const char*)ois->filename);
   replace_extension(fullfilename, fullfilename, ((mp4) ? "mp4" : "h264"), sizeof(fullfilename));

   win_printf_OK("openning:\n %s", backslash_to_slash((char*)fullfilename));
   if (mp4) {
       if (do_select_file(fullfilename, sizeof(fullfilename), "Save MP4 movie", "mp4", "Save MP4 movie", 0))
           return win_printf_OK("Cannot select ouput file");
   }
   else  {
       if (do_select_file(fullfilename, sizeof(fullfilename), "Save H264 movie", "h264", "Save H264 movie", 0))
           return win_printf_OK("Cannot select ouput file");
   }

   f = fopen(fullfilename, "wb");
   if (!f) {
           return win_printf_OK("Could not open:\n %s", backslash_to_slash((char*)fullfilename));
    }

   c = avcodec_alloc_context3(codec);
   if (!c) {
           return win_printf_OK("Could not allocate video codec context\n");
   }
     tmp = (float)(ois->im.nx * ois->im.nx)/(352*288);
   /* put sample parameters */
   c->bit_rate = (int)(0.5+tmp)*400000*20*compression_factor;
   /* resolution must be a multiple of two */
   c->width = ois->im.nx;
   c->height = ois->im.ny;

   /*
    c->bit_rate = 400000;
    c->width = 352;
    c->height = 288;
   */

   /* frames per second */
   c->time_base = (AVRational){1,25};
   /* emit one intra frame every ten frames
    * check frame pict_type before passing frame
    * to encoder, if frame->pict_type is AV_PICTURE_TYPE_I
    * then gop_size is ignored and the output of encoder
    * will always be I frame irrespective to gop_size
    */
   c->gop_size = 10;
   c->max_b_frames = 1;
   c->pix_fmt = (ois->im.data_type == IS_RGB_PICTURE) ? AV_PIX_FMT_YUV444P : AV_PIX_FMT_GRAY8;
   c->pix_fmt = AV_PIX_FMT_YUV444P;
   //AV_PIX_FMT_RGB24;// AV_PIX_FMT_YUV420P;

   if (codec_id == AV_CODEC_ID_H264)
           av_opt_set(c->priv_data, "preset", "slow", 0);
   /* open it */
   if (avcodec_open2(c, codec, NULL) < 0) {
           return win_printf_OK("Could not open codec\n");
   }
     frame = av_frame_alloc();
   if (!frame) {
           win_printf_OK("Could not allocate video frame\n");
   }
   frame->format = c->pix_fmt;
   frame->width  = c->width;
   frame->height = c->height;
   /* the image can be allocated by any means and av_image_alloc() is
    * just the most convenient way if av_malloc() is to be used */
   ret = av_image_alloc(frame->data, frame->linesize, c->width, c->height,
                        c->pix_fmt, 32);
   if (ret < 0) {
           return win_printf_OK("Could not allocate raw picture buffer\n");
   }
     unsigned char R, G, B;
     int Y, U, V, yo, im2;
     int zwi, zbi;
     int zi, zgi;

    zwi = (ois->dz != 0) ? (int)((ois->z_white - ois->az)/ois->dz) :
        (int)(ois->z_white - ois->az);
    zbi = (ois->dz != 0) ? (int)((ois->z_black - ois->az)/ois->dz) :
        (int)(ois->z_black - ois->az);
    zgi = 65536 * 255;
    if ( (zwi - zbi) != 0)     zgi /= (zwi - zbi);
   
   for (im2 = 0; im2 < nfi; im2++) {
	im = (ois->im.save_using_this_frame_as_first)
	  ? im2 + ois->im.save_using_this_frame_as_first : im2;
	im = (im < nfi) ? im : im - nfi;
        av_init_packet(&pkt);
        pkt.data = NULL;    // packet data will be allocated by the encoder
        pkt.size = 0;
        fflush(stdout);
        switch_frame(ois,im);
        /* prepare the image */

        if (ois->im.data_type == IS_RGB_PICTURE) {
	  for (y = 0; y < c->height; y++) {
              for (x = 0; x < c->width; x++) {
                 yo = ois->im.ny - 1 - y;
		 zi = (ois->im.pixel[yo].rgb[x].r - zbi)* zgi;
		 zi = (zi < 0 ) ? 0 : (zi >> 16);
		 R = (zi > 255) ? 255 :(unsigned char)zi;
		 zi = (ois->im.pixel[yo].rgb[x].g - zbi)* zgi;
		 zi = (zi < 0 ) ? 0 : (zi >> 16);
		 G = (zi > 255) ? 255 :(unsigned char)zi;
		 zi = (ois->im.pixel[yo].rgb[x].b - zbi)* zgi;
		 zi = (zi < 0 ) ? 0 : (zi >> 16);
		 B = (zi > 255) ? 255 :(unsigned char)zi;		 
                 Y = ((66 * R + 129 * G +  25 * B + 128) >> 8) +  16;
                 U = (( -38 * R -  74 * G + 112 * B + 128) >> 8) + 128;
                 V = (( 112 * R -  94 * G -  18 * B + 128) >> 8) + 128;
                 /*
                 Y = (0.299*r + 0.587*g + 0.114*b);
                 U = 0.492 * (b - Y);
                 V = 0.877 * (r - Y);
                 */
                 Y = (Y < 0) ? 0 : ((Y < 256) ? Y : 255);
                 U = (U < 0) ? 0 : ((U < 256) ? U : 255);
                 V = (V < 0) ? 0 : ((V < 256) ? V : 255);
                 frame->data[0][y * frame->linesize[0] + x] = (unsigned char)Y;
                 frame->data[1][y * frame->linesize[1] + x] = (unsigned char)U;
                 frame->data[2][y * frame->linesize[2] + x] = (unsigned char)V;
              }
           }
        }
        else if (ois->im.data_type == IS_CHAR_IMAGE) {
	  for (y = 0; y < c->height; y++) {
              for (x = 0; x < c->width; x++) {
                 yo = ois->im.ny - 1 - y;
		 zi = (ois->im.pixel[yo].ch[x] - zbi)* zgi;
		 zi = (zi < 0 ) ? 0 : (zi >> 16);
		 frame->data[0][y * frame->linesize[0] + x] = (zi > 255) ? 255 :(unsigned char)zi;
                 frame->data[1][y * frame->linesize[1] + x] = 128;
                 frame->data[2][y * frame->linesize[2] + x] = 128;
              }
           }
        }
        /*
        for (y = 0; y < c->height; y++) {
            for (x = 0; x < c->width; x++) {
                frame->data[0][y * frame->linesize[0] + x] = x + y + i * 3;
            }
        }
        // Cb and Cr        for (y = 0; y < c->height/2; y++) {
            for (x = 0; x < c->width/2; x++) {
                frame->data[1][y * frame->linesize[1] + x] = 128 + y + i * 2;
                frame->data[2][y * frame->linesize[2] + x] = 64 + x + i * 5;
            }
        }
        */

        frame->pts = im2;
        /* encode the image */
        ret = avcodec_encode_video2(c, &pkt, frame, &got_output);
        if (ret < 0) {
                return win_printf_OK("Error encoding frame\n");
        }
        if (got_output) {
	  my_set_window_title("Write frame %3d (in movie %d) (size=%5d)\n", im2, im, pkt.size);
            fwrite(pkt.data, 1, pkt.size, f);
            av_packet_unref(&pkt);
        }
    }

    /* get the delayed frames */
   for (i = 0, got_output = 1; got_output; i++) {
       fflush(stdout);
       ret = avcodec_encode_video2(c, &pkt, NULL, &got_output);
       if (ret < 0) {
               win_printf_OK("Error encoding frame\n");
       }
       if (got_output) {
           my_set_window_title("Write frame %3d (size=%5d)\n", i, pkt.size);
           fwrite(pkt.data, 1, pkt.size, f);
           av_packet_unref(&pkt);
       }
   }
   /* add sequence end code to have a real MPEG file */
   fwrite(endcode, 1, sizeof(endcode), f);
   fclose(f);
   avcodec_close(c);
   av_free(c);
   av_freep(&frame->data[0]);
   av_frame_free(&frame);
   return 0;
}




int do_encode_h264_plot_animation(void)
{
   // Output file
    const char  fullfilename[512] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    static int x0 = 0, y0 = 0, x1 = 256, y1 = 256;
    //imreg *imr = NULL;
    O_i *ois = NULL;
    int nfi, im;
    float tmp;
// Audio and Video Timestamps (presentation time stamp)
    int codec_id = AV_CODEC_ID_H264, codec_id2 = AV_CODEC_ID_MPEG4;
    AVCodec *codec = NULL;
    AVCodecContext *c= NULL;
    int i, ret, x, y, got_output;
    FILE *f = NULL;
    AVFrame *frame = NULL;
    AVPacket pkt;
    uint8_t endcode[] = { 0, 0, 1, 0xb7 };
    //static int mp4 = 0;


    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
        return D_O_K;
    nfi = ds->nx;
    if(updating_menu_state != 0)      {
        if (nfi == 1) 	active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }



    i = win_scanf("Define the Scrren area to copy:\n"
                  "X0 = %4d Y0 = %4d\n"
                  "X1 = %4d Y1 = %4d\n",&x0, &y0, &x1, &y1);
    if (i == WIN_CANCEL) return 0;

    x0 = (x0 < 0) ? 0 : x0;
    y0 = (y0 < 0) ? 0 : y0;
    x1 = (x1 <= screen->w) ? x1 : screen->w;
    y1 = (y1 <= screen->h) ? y1 : screen->h;


   codec_id = AV_CODEC_ID_H264;
   codec = avcodec_find_encoder(codec_id);
   if (!codec) {
           return win_printf_OK("Codec not found\n");
   }

   build_full_file_name(fullfilename, sizeof(fullfilename), (const char*)op->dir, (const char*)op->filename);
   replace_extension(fullfilename, fullfilename, "h264", sizeof(fullfilename));

   win_printf_OK("openning:\n %s", backslash_to_slash((char*)fullfilename));
   if (do_select_file(fullfilename, sizeof(fullfilename), "Save H264 movie", "h264", "Save H264 movie", 0))
        return win_printf_OK("Cannot select ouput file");


   f = fopen(fullfilename, "wb");
   if (!f) {
           return win_printf_OK("Could not open:\n %s", backslash_to_slash((char*)fullfilename));
    }

   c = avcodec_alloc_context3(codec);
   if (!c) {
           return win_printf_OK("Could not allocate video codec context\n");
   }
   tmp = (float)((x1-x0) * (y1-y0))/(352*288);
   /* put sample parameters */
   c->bit_rate = (int)(0.5+tmp)*400000;
   /* resolution must be a multiple of two */
   c->width = x1 - x0;
   c->height = y1 - y0;

   /*
    c->bit_rate = 400000;
    c->width = 352;
    c->height = 288;
   */

   /* frames per second */
   c->time_base = (AVRational){1,25};
   /* emit one intra frame every ten frames
    * check frame pict_type before passing frame
    * to encoder, if frame->pict_type is AV_PICTURE_TYPE_I
    * then gop_size is ignored and the output of encoder
    * will always be I frame irrespective to gop_size
    */
   c->gop_size = 10;
   c->max_b_frames = 1;
   c->pix_fmt = AV_PIX_FMT_YUV444P;
   c->pix_fmt = AV_PIX_FMT_YUV444P;
   //AV_PIX_FMT_RGB24;// AV_PIX_FMT_YUV420P;

   if (codec_id == AV_CODEC_ID_H264)
           av_opt_set(c->priv_data, "preset", "slow", 0);
   /* open it */
   if (avcodec_open2(c, codec, NULL) < 0) {
           return win_printf_OK("Could not open codec\n");
   }
     frame = av_frame_alloc();
   if (!frame) {
           win_printf_OK("Could not allocate video frame\n");
   }
   frame->format = c->pix_fmt;
   frame->width  = c->width;
   frame->height = c->height;
   /* the image can be allocated by any means and av_image_alloc() is
    * just the most convenient way if av_malloc() is to be used */
   ret = av_image_alloc(frame->data, frame->linesize, c->width, c->height,
                        c->pix_fmt, 32);
   if (ret < 0) {
           return win_printf_OK("Could not allocate raw picture buffer\n");
   }
   unsigned char R, G, B;
   int Y, U, V, yo;
   for (im = 0; im < nfi; im++) {
        av_init_packet(&pkt);
        pkt.data = NULL;    // packet data will be allocated by the encoder
        pkt.size = 0;
        fflush(stdout);

	ds->nx = im;
	op->need_to_refresh = 1;
	refresh_plot(pr, UNCHANGED);

	ois = convert_bitmap_to_oi(screen, ois, x0, y0, x1, y1);
	if (ois == NULL)      return (win_printf("Can't create dest image"));


	for (y = 0; y < c->height; y++) {
	  for (x = 0; x < c->width; x++) {
	    yo = ois->im.ny - 1 - y;
	    R = ois->im.pixel[yo].rgb[x].r;
	    G = ois->im.pixel[yo].rgb[x].g;
	    B = ois->im.pixel[yo].rgb[x].b;
	    
	    Y = ((66 * R + 129 * G +  25 * B + 128) >> 8) +  16;
	    U = (( -38 * R -  74 * G + 112 * B + 128) >> 8) + 128;
	    V = (( 112 * R -  94 * G -  18 * B + 128) >> 8) + 128;
	    Y = (Y < 0) ? 0 : ((Y < 256) ? Y : 255);
	    U = (U < 0) ? 0 : ((U < 256) ? U : 255);
	    V = (V < 0) ? 0 : ((V < 256) ? V : 255);
	    frame->data[0][y * frame->linesize[0] + x] = (unsigned char)Y;
	    frame->data[1][y * frame->linesize[1] + x] = (unsigned char)U;
                 frame->data[2][y * frame->linesize[2] + x] = (unsigned char)V;
	  }
	}
        frame->pts = im;
        /* encode the image */
        ret = avcodec_encode_video2(c, &pkt, frame, &got_output);
        if (ret < 0) {
                return win_printf_OK("Error encoding frame\n");
        }
        if (got_output) {
            my_set_window_title("Write frame %3d (size=%5d)\n", im, pkt.size);
            fwrite(pkt.data, 1, pkt.size, f);
            av_packet_unref(&pkt);
        }
    }

    /* get the delayed frames */
   for (got_output = 1; got_output; i++) {
       fflush(stdout);
       ret = avcodec_encode_video2(c, &pkt, NULL, &got_output);
       if (ret < 0) {
               win_printf_OK("Error encoding frame\n");
       }
       if (got_output) {
           my_set_window_title("Write frame %3d (size=%5d)\n", im, pkt.size);
           fwrite(pkt.data, 1, pkt.size, f);
           av_packet_unref(&pkt);
       }
   }
   /* add sequence end code to have a real MPEG file */
   fwrite(endcode, 1, sizeof(endcode), f);
   fclose(f);
   avcodec_close(c);
   av_free(c);
   av_freep(&frame->data[0]);
   av_frame_free(&frame);
   free_one_image(ois);
   return 0;
}


int do_encode_h264_plot_animation_with_image(void)
{
   // Output file
    const char  fullfilename[512] = {0}, file[256] = {0}, path[512] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    static int x0 = 0, y0 = 0, x1 = 256, y1 = 256, xl = 0, yt = 0;
    //imreg *imr = NULL;
    O_i *ois = NULL, *oim = NULL;
    int nfi, im;
    float tmp;
// Audio and Video Timestamps (presentation time stamp)
    int codec_id = AV_CODEC_ID_H264;//, codec_id2 = AV_CODEC_ID_MPEG4;
    AVCodec *codec = NULL;
    AVCodecContext *c= NULL;
    int i, ret, x, y, got_output;
    FILE *f = NULL;
    AVFrame *frame = NULL;
    AVPacket pkt;
    uint8_t endcode[] = { 0, 0, 1, 0xb7 };
    //static int mp4 = 0;


    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
        return D_O_K;
    nfi = ds->nx;
    if(updating_menu_state != 0)      {
        if (nfi == 1) 	active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }


    if (do_select_file(fullfilename, sizeof(fullfilename), "load GR movie", "gr", "Gr movie", 1))
        return win_printf_OK("Cannot select ouput file");
    extract_file_name(file, 256, fullfilename);
    extract_file_path(path, 512, fullfilename);

    oim = load_im_file_in_oi(file, path);
    if (oim == NULL) return win_printf_OK("movie  not found\n");

    
    i = win_scanf("Define the Screen area to copy:\n"
                  "X0 = %4d Y0 = %4d\n"
                  "X1 = %4d Y1 = %4d\n"
		  "Specify the top left position of the movie image\n"
		  "xl = %4d yt = %4d\n"
		  ,&x0, &y0, &x1, &y1, &xl, &yt);
    if (i == WIN_CANCEL) return 0;

    x0 = (x0 < 0) ? 0 : x0;
    y0 = (y0 < 0) ? 0 : y0;
    x1 = (x1 <= screen->w) ? x1 : screen->w;
    y1 = (y1 <= screen->h) ? y1 : screen->h;

    
    codec_id = AV_CODEC_ID_H264;
    codec = avcodec_find_encoder(codec_id);
    if (!codec) {
      return win_printf_OK("Codec not found\n");
    }
    
    build_full_file_name(fullfilename, sizeof(fullfilename), (const char*)op->dir, (const char*)op->filename);
    replace_extension(fullfilename, fullfilename, "h264", sizeof(fullfilename));
    
    win_printf_OK("openning:\n %s", backslash_to_slash((char*)fullfilename));
    if (do_select_file(fullfilename, sizeof(fullfilename), "Save H264 movie", "h264", "Save H264 movie", 0))
      return win_printf_OK("Cannot select ouput file");

    
    f = fopen(fullfilename, "wb");
    if (!f) {
      return win_printf_OK("Could not open:\n %s", backslash_to_slash((char*)fullfilename));
    }
    
    c = avcodec_alloc_context3(codec);
    if (!c) {
      return win_printf_OK("Could not allocate video codec context\n");
    }
    tmp = (float)((x1-x0) * (y1-y0))/(352*288);
    /* put sample parameters */
    c->bit_rate = (int)(0.5+tmp)*400000;
    /* resolution must be a multiple of two */
    c->width = x1 - x0;
    c->height = y1 - y0;
    
    /*
      c->bit_rate = 400000;
      c->width = 352;
      c->height = 288;
    */
    
    /* frames per second */
    c->time_base = (AVRational){1,25};
    /* emit one intra frame every ten frames
     * check frame pict_type before passing frame
     * to encoder, if frame->pict_type is AV_PICTURE_TYPE_I
     * then gop_size is ignored and the output of encoder
     * will always be I frame irrespective to gop_size
     */
    c->gop_size = 10;
    c->max_b_frames = 1;
    c->pix_fmt = AV_PIX_FMT_YUV444P;
    c->pix_fmt = AV_PIX_FMT_YUV444P;
    //AV_PIX_FMT_RGB24;// AV_PIX_FMT_YUV420P;
    
    if (codec_id == AV_CODEC_ID_H264)
      av_opt_set(c->priv_data, "preset", "slow", 0);
    /* open it */
    if (avcodec_open2(c, codec, NULL) < 0) {
      return win_printf_OK("Could not open codec\n");
    }
    frame = av_frame_alloc();
    if (!frame) {
      win_printf_OK("Could not allocate video frame\n");
    }
    frame->format = c->pix_fmt;
    frame->width  = c->width;
    frame->height = c->height;
    /* the image can be allocated by any means and av_image_alloc() is
     * just the most convenient way if av_malloc() is to be used */
    ret = av_image_alloc(frame->data, frame->linesize, c->width, c->height,
			 c->pix_fmt, 32);
    if (ret < 0) {
      return win_printf_OK("Could not allocate raw picture buffer\n");
    }
    unsigned char R, G, B;
    int Y, U, V, yo;
    for (im = 0; im < nfi; im++) {
      av_init_packet(&pkt);
      pkt.data = NULL;    // packet data will be allocated by the encoder
      pkt.size = 0;
      fflush(stdout);
      
      ds->nx = im;
      op->need_to_refresh = 1;
      refresh_plot(pr, UNCHANGED);
      
      ois = convert_bitmap_to_oi(screen, ois, x0, y0, x1, y1);
      if (ois == NULL)      return (win_printf("Can't create dest image"));
      switch_frame(oim,im);
      copy_one_subset_of_image_in_a_bigger_one(ois, xl, yt, oim, 0, 0, oim->im.nx, oim->im.ny);
      for (y = 0; y < c->height; y++) {
	for (x = 0; x < c->width; x++) {
	  yo = ois->im.ny - 1 - y;
	  R = ois->im.pixel[yo].rgb[x].r;
	  G = ois->im.pixel[yo].rgb[x].g;
	  B = ois->im.pixel[yo].rgb[x].b;
	  
	  Y = ((66 * R + 129 * G +  25 * B + 128) >> 8) +  16;
	  U = (( -38 * R -  74 * G + 112 * B + 128) >> 8) + 128;
	  V = (( 112 * R -  94 * G -  18 * B + 128) >> 8) + 128;
	  Y = (Y < 0) ? 0 : ((Y < 256) ? Y : 255);
	  U = (U < 0) ? 0 : ((U < 256) ? U : 255);
	  V = (V < 0) ? 0 : ((V < 256) ? V : 255);
	  frame->data[0][y * frame->linesize[0] + x] = (unsigned char)Y;
	  frame->data[1][y * frame->linesize[1] + x] = (unsigned char)U;
	  frame->data[2][y * frame->linesize[2] + x] = (unsigned char)V;
	}
      }
      frame->pts = im;
      /* encode the image */
      ret = avcodec_encode_video2(c, &pkt, frame, &got_output);
      if (ret < 0) {
                return win_printf_OK("Error encoding frame\n");
      }
      if (got_output) {
	my_set_window_title("Write frame %3d (size=%5d)\n", im, pkt.size);
	fwrite(pkt.data, 1, pkt.size, f);
	av_packet_unref(&pkt);
      }
    }
    
    /* get the delayed frames */
    for (got_output = 1; got_output; i++) {
      fflush(stdout);
      ret = avcodec_encode_video2(c, &pkt, NULL, &got_output);
      if (ret < 0) {
               win_printf_OK("Error encoding frame\n");
      }
      if (got_output) {
	my_set_window_title("Write frame %3d (size=%5d)\n", im, pkt.size);
	fwrite(pkt.data, 1, pkt.size, f);
	av_packet_unref(&pkt);
      }
    }
    /* add sequence end code to have a real MPEG file */
   fwrite(endcode, 1, sizeof(endcode), f);
   fclose(f);
   avcodec_close(c);
   av_free(c);
   av_freep(&frame->data[0]);
   av_frame_free(&frame);
   free_one_image(ois);
   return 0;
}

# ifdef ENCOURS

# define STREAM_FRAME_RATE 25
/* Add an output stream. */
static int add_stream(OutputStream *ost, AVFormatContext *oc,
                       AVCodec **codec,
                       enum AVCodecID codec_id)
{
    AVCodecContext *c = NULL;
    int i;
    /* find the encoder */
    *codec = avcodec_find_encoder(codec_id);
    if (!(*codec)) {
         return win_printf_OK("Could not find encoder for '%s'\n",
                avcodec_get_name(codec_id));
    }
    ost->st = avformat_new_stream(oc, NULL);
    if (!ost->st) {
        return win_printf_OK( "Could not allocate stream\n");
    }
    ost->st->id = oc->nb_streams-1;
    c = avcodec_alloc_context3(*codec);
    if (!c) {
        return win_printf_OK("Could not alloc an encoding context\n");
    }
    ost->enc = c;
    switch ((*codec)->type) {
    case AVMEDIA_TYPE_AUDIO:
        c->sample_fmt  = (*codec)->sample_fmts ?
            (*codec)->sample_fmts[0] : AV_SAMPLE_FMT_FLTP;
        c->bit_rate    = 64000;
        c->sample_rate = 44100;
        if ((*codec)->supported_samplerates) {
            c->sample_rate = (*codec)->supported_samplerates[0];
            for (i = 0; (*codec)->supported_samplerates[i]; i++) {
                if ((*codec)->supported_samplerates[i] == 44100)
                    c->sample_rate = 44100;
            }
        }
        c->channels        = av_get_channel_layout_nb_channels(c->channel_layout);
        c->channel_layout = AV_CH_LAYOUT_STEREO;
        if ((*codec)->channel_layouts) {
            c->channel_layout = (*codec)->channel_layouts[0];
            for (i = 0; (*codec)->channel_layouts[i]; i++) {
                if ((*codec)->channel_layouts[i] == AV_CH_LAYOUT_STEREO)
                    c->channel_layout = AV_CH_LAYOUT_STEREO;
            }
        }
        c->channels        = av_get_channel_layout_nb_channels(c->channel_layout);
        ost->st->time_base = (AVRational){ 1, c->sample_rate };
        break;
    case AVMEDIA_TYPE_VIDEO:
        c->codec_id = codec_id;
        c->bit_rate = 400000;
        /* Resolution must be a multiple of two. */
        c->width    = 352;
        c->height   = 288;
        /* timebase: This is the fundamental unit of time (in seconds) in terms
         * of which frame timestamps are represented. For fixed-fps content,
         * timebase should be 1/framerate and timestamp increments should be
         * identical to 1. */
        ost->st->time_base = (AVRational){ 1, STREAM_FRAME_RATE };
        c->time_base       = ost->st->time_base;
        c->gop_size      = 12; /* emit one intra frame every twelve frames at most */
        c->pix_fmt       = STREAM_PIX_FMT;
        if (c->codec_id == AV_CODEC_ID_MPEG2VIDEO) {
            /* just for testing, we also add B-frames */
            c->max_b_frames = 2;
        }
        if (c->codec_id == AV_CODEC_ID_MPEG1VIDEO) {
            /* Needed to avoid using macroblocks in which some coeffs overflow.
             * This does not happen with normal video, it just happens here as
             * the motion of the chroma plane does not match the luma plane. */
            c->mb_decision = 2;
        }
    break;
    default:
        break;
    }
    /* Some formats want stream headers to be separate. */
    if (oc->oformat->flags & AVFMT_GLOBALHEADER)
        c->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
}

int do_encode_movie(void)
{
   // Output file
    const char  fullfilename[512] = {0};
    imreg *imr = NULL;
    O_i *ois = NULL;
    int nfi, im;
    float tmp;
// Audio and Video Timestamps (presentation time stamp)
    int codec_id = AV_CODEC_ID_H264, codec_id2 = AV_CODEC_ID_MPEG4;
    AVCodec *video_codec = NULL;
    AVCodecContext *c= NULL;
    OutputStream video_st = { 0 }, audio_st = { 0 };
    AVOutputFormat *fmt;
    AVFormatContext *oc;
    int have_video = 0;
    int encode_video = 0;
    AVDictionary *opt = NULL;
    int i, ret, x, y, got_output;
    FILE *f = NULL;
    AVFrame *frame = NULL;
    AVPacket pkt;
    uint8_t endcode[] = { 0, 0, 1, 0xb7 };
    static int mp4 = 0;


    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
        return D_O_K;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;     if(updating_menu_state != 0)      {
        if (nfi == 1) 	active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

   if (ois->im.data_type != IS_RGB_PICTURE && ois->im.data_type != IS_CHAR_IMAGE)
        return win_printf_OK("I can only handle RGB or BW movies!");


   i = win_scanf("FOrmat %Rh264 or %rMP4",&mp4);
   if (i == WIN_CANCEL) return 0;


   build_full_file_name(fullfilename, sizeof(fullfilename), (const char*)ois->dir, (const char*)ois->filename);
   replace_extension(fullfilename, fullfilename, "mkv", sizeof(fullfilename));

   win_printf_OK("openning:\n %s", backslash_to_slash((char*)fullfilename));
   if (do_select_file(fullfilename, sizeof(fullfilename), "Save MKV movie", "mkv", "Save MKV movie", 0))
           return win_printf_OK("Cannot select ouput file");


    /* allocate the output media context */
    avformat_alloc_output_context2(&oc, NULL, NULL, fullfilename);
    if (!oc) {
        win_printf("Could not deduce output format from file extension: using MPEG.\n");
        avformat_alloc_output_context2(&oc, NULL, "mpeg", fullfilename);
    }
    if (!oc)
        return win_printf_OK("Cannot select ouput codec");

    fmt = oc->oformat;

    if (fmt->video_codec != AV_CODEC_ID_NONE) {
        add_stream(&video_st, oc, &video_codec, fmt->video_codec);
        have_video = 1;
        encode_video = 1;
    }

    /* Now that all the parameters are set, we can open the audio and
     * video codecs and allocate the necessary encode buffers. */
    if (have_video)   open_video(oc, video_codec, &video_st, opt);


    av_dump_format(oc, 0, fullfilename, 1);
    /* open the output file, if needed */
    if (!(fmt->flags & AVFMT_NOFILE)) {
        ret = avio_open(&oc->pb, fullfilename, AVIO_FLAG_WRITE);
        if (ret < 0) 
             return   win_printf("Could not open '%s': %s\n"
                                 , backslash_to_slash(fullfilename), av_err2str(ret));
    }
    /* Write the stream header, if any. */
    ret = avformat_write_header(oc, &opt);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file: %s\n",
                av_err2str(ret));
        return 1;
    }


    while (encode_video) {
        /* select the stream to encode */
            encode_video = !write_video_frame(oc, &video_st);
        }
    /* Write the trailer, if any. The trailer must be written before you
     * close the CodecContexts open when you wrote the header; otherwise
     * av_write_trailer() may try to use memory that was freed on
     * av_codec_close(). */
    av_write_trailer(oc);
    /* Close each codec. */
    if (have_video)        close_stream(oc, &video_st);
        /* Close the output file. */
    if (!(fmt->flags & AVFMT_NOFILE))        avio_closep(&oc->pb);
    /* free the stream */
    avformat_free_context(oc);
    return 0;
}
# endif

MENU *xvFfmpeg_image_menu(void)
{
  static MENU mn[32] = {0};

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"average profile", do_xvFfmpeg_average_along_y,NULL,0,NULL);
  add_item_to_menu(mn,"Read movie", do_xvFfmpeg_read_movie,NULL,0,NULL);
  add_item_to_menu(mn,"Load movie", do_xvFfmpeg_read_movie_2,NULL,0,NULL);
  add_item_to_menu(mn,"Save H264 movie", do_encode_h264_movie ,NULL,0,NULL);

  return mn;
}

MENU *xvFfmpeg_plot_menu(void)
{
  static MENU mn[32] = {0};//, mn1[32] = {0}, mn2[32] = {0};

    if (mn[0].text != NULL)    return mn;
    add_item_to_menu(mn,"Plot animation -> h264", do_encode_h264_plot_animation,NULL,MENU_INDEX(0),NULL);
    add_item_to_menu(mn,"Plot animation with image -> h264", do_encode_h264_plot_animation_with_image,NULL,MENU_INDEX(0),NULL);
    
    return mn;
}

int	xvFfmpeg_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  avcodec_register_all();
  add_image_treat_menu_item ( "xvFfmpeg", NULL, xvFfmpeg_image_menu(), 0, NULL);
  add_plot_treat_menu_item ( "xvFfmpeg", NULL, xvFfmpeg_plot_menu(), 0, NULL);
  return D_O_K;
}

int	xvFfmpeg_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(image_treat_menu, "xvFfmpeg", NULL, NULL);
  return D_O_K;
}


#endif

