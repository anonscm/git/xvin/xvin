#ifndef _JARZYsNSKI_C_
#define _JARZYNSKI_C_

#include "allegro.h"
#include "xvin.h"
#include "fft_filter_lib.h"
#include "xv_tools_lib.h"


#include <gsl/gsl_spline.h>
#include <gsl/gsl_statistics.h>
#include <fftw3.h>		
#include <gsl/gsl_histogram.h>

/* If you include other plug-ins header do it here*/ 
#include "../hist/hist.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "Jarzynski.h"

#define k_b 1.3806503e-23	// m2 kg s-2 K-1
#define Temperature 300.		// K

double  kT = k_b*Temperature;


int f_to_d(float *in, double *out, int N)
{	register int i;

	for (i=0; i<N; i++)
	{	out[i] = (double)in[i];
	}
	
	return(0);
}






int do_compute_Jarzynski_estimate(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny;
	d_s 	*ds1;
	pltreg *pr = NULL;
	int	index;
	double	dt=1;
	double	*w;
	float	f_acq;
	double	mean_w, mean_exp_w, var_w, var_exp_w;	
	char	bool_is_kT_units=0;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the Jarzynski estimate for the variation of\n"
					"free energy, using actual work performed on the system.\n\n"
					"{\\pt14 \\Delta F = - kT log( < exp(-W/kT) > )}\n\n"
					"active dataset is used as the work W");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->ny;	// number of points in time series 
	if (ny<4) 						return(win_printf_OK("not enough points !"));

	if (win_scanf("This routine computes the Jarzynski estimate for the variation of\n"
					"free energy, using actual work performed on the system.\n\n"
					"{\\pt14 \\Delta F = - kT log( < exp(-W/kT) > )}\n\n"
					"active dataset is used as the work W\n"
					"%b signal is already in kT units\n   (if not checked, units are Joules)", 
					&bool_is_kT_units)==CANCEL) return(D_O_K);

	dt = (double)(ds1->xd[ny-1]-ds1->xd[0])/(ny-1);
	if ( (ds1->xd[2]-ds1->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)				return(win_printf_OK("dt is zero !"));

	f_acq=(float)(1/dt);

	w  = fftw_malloc((ny)*sizeof(double));
	if (bool_is_kT_units==1) for (i=0; i<ny; i++)	w[i]=(double)ds1->yd[i];
	else			 for (i=0; i<ny; i++)	w[i]=(double)ds1->yd[i]/kT;
	
	mean_w  = gsl_stats_mean(w, 1, ny);
	var_w  	= gsl_stats_sd_m(w, 1, ny, mean_w);

	for (i=0; i<ny; i++)	w[i] = exp(-w[i]);
	
	mean_exp_w = gsl_stats_mean(w, 1, ny);
	var_exp_w  = gsl_stats_sd_m(w, 1, ny, mean_w);

	fftw_free(w);

	if (mean_exp_w<=0) return(win_printf_OK("<exp(-W/kT)> is not positive !!!"));

	win_printf_OK("{\\color{yellow}\\pt14 For the instantaneous work in current dataset:}\n"
			"          <w> = %8.4g kT = %8.4g J   \\sigma_w = %8.4g kT = %g J\n\n"
			" <exp(-w/kT)> = %g   \\sigma  = %8.4g\n\n"
			" <w> - \\frac{\\sigma^2}{2kT} = %g kT = %g J\n"
			" \\Delta F_{Jarzynski} = %g kT = %g J", 
			mean_w, mean_w*kT, var_w,  var_w*kT, 
			mean_exp_w, var_exp_w, (mean_w - var_w*var_w/2), kT*(mean_w - var_w*var_w/2), 
			-log(mean_exp_w), -kT*log(mean_exp_w));

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_compute_Jarzynski_estimate





int do_compute_delayed_diff(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny;
static	int	tau=10;
	d_s 	*ds1, *ds2=NULL;
	pltreg *pr = NULL;
	int	index;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the following quantity (using f from active dataset)\n\n"
					"f(t+\\tau) - f(t)\n\n"
					"then, you can compute its pdf.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	if (win_scanf("if f(t) is the active dataset, I will compute\n\n"
			"f(t+\\tau) - f(t)\n\n"
			"\\tau = %d", &tau)==CANCEL) return D_O_K;
	ny = ds1->ny;	// number of points in time series 
	if (ny<(tau+1)) 			return(win_printf_OK("not enough points !"));

	ds2 = create_and_attach_one_ds(op1, ny-tau, ny-tau, 0);

	for (i=0; i<ny-tau; i++)	
	{	ds2->xd[i] = ds1->xd[i];
		ds2->yd[i] = (ds1->yd[i+tau] - ds1->yd[i]);
	}

	inherit_from_ds_to_ds(ds2, ds1);
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_compute_delayed_diff






int do_compute_Jarzynski_work(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny;
static	int	tau=10;
	d_s 	*ds1, *ds2=NULL;
	pltreg *pr = NULL;
	int	index;
static	int	dipole=0;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the following quantity (using f from active dataset)\n\n"
					"{\\pt14 W_J = \\int_t^{t+\\tau} \\partial U / \\partial t . q . dt}\n\n"
					"then, you can compute its pdf.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	if (win_scanf("This routine computes the following quantity (using f from active dataset)\n\n"
					"{\\pt14 W_J = \\int_t^{t+\\tau} \\partial U / \\partial t . q . dt}\n\n"
					"\\tau = %d %R work with R\n %r work with RC", &tau, &dipole)==CANCEL) return D_O_K;
	ny = ds1->ny;	// number of points in time series 
	if (ny<(tau+1)) 			return(win_printf_OK("not enough points !"));

	ds2 = create_and_attach_one_ds(op1, ny-tau, ny-tau, 0);

	if (dipole==0) // dipole = R
	{	for (i=0; i<ny-tau; i++)	
		{	ds2->xd[i] = ds1->xd[i];
			ds2->yd[i] = (ds1->yd[i+tau] - ds1->yd[i]);
		}
	}
	else // dipole = RC
	{	for (i=0; i<ny-tau; i++)	
		{	ds2->xd[i] = ds1->xd[i];
			ds2->yd[i] = (ds1->yd[i+tau] - ds1->yd[i]);
		}
	}

	inherit_from_ds_to_ds(ds2, ds1);
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_compute_delayed_diff






int do_plot_BHP(void)
{
	register int i;
	O_p 	*op=NULL;
	int	ny;
	d_s 	*ds1, *ds2=NULL;
	pltreg *pr = NULL;
	int	index;
	double	x;
static	float	K=1;
static float s=0;
static float a=1;
static float b=1;


	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine plots a BHP/Gumnble distribution\n"
					"A new dataset is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->nx;	// number of points in time series 
	if (ny<4) 						return(win_printf_OK("not enough points !"));

	if (win_scanf("{\\color{yellow}\\pt14 BHP}\n\n"
		"I will draw p(x) = K exp [ a [ ( b(y-s) - exp(b(y-s)) ] ] \n\n"
		"K=%8f s=%8f\na=%8f b=%8f ", &K, &s, &a, &b)==CANCEL) return(D_O_K);

	ds2 = create_and_attach_one_ds(op, ny, ny, 0);
			
	for (i=0; i<ny; i++)	
	{	x = (double)ds1->xd[i];
		ds2->xd[i] = (float)x;
		x = ((double)b*(x-s));
		ds2->yd[i] = (float)((double)K*exp((double)a*( x - exp(x) )));
	}

//	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(NULL,"BHP shape, with K=%6.3f s=%6.3f a=%6.3f b=%6.3f", K,s,a,b);

	set_ds_plot_label(ds2, ds2->xd[ds2->nx*2/3-1], ds2->yd[ds2->ny-1], USR_COORD, 
				 "\\stack{{K=%6.3f s=%6.3f}{a=%6.3f b=%6.3f}}", K,s,a,b);
	
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_plot_BHP






MENU *Jarzynski_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Estimate DF", 			do_compute_Jarzynski_estimate,		NULL, 0, NULL);	
	add_item_to_menu(mn,"average diff. tau",		do_compute_delayed_diff,		NULL, 0, NULL);	
	add_item_to_menu(mn,"Jarzynski work",			do_compute_Jarzynski_work,		NULL, 0, NULL);	
	add_item_to_menu(mn,"Plot theor. BHP", 			do_plot_BHP,				NULL, 0, NULL);	
	
	return mn;
}

int	Jarzynski_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("Jarzynski", NULL, Jarzynski_plot_menu(), 0, NULL);
	return D_O_K;
}


int	Jarzynski_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"Jarzynski",	NULL, NULL);
	return D_O_K;
}
#endif

