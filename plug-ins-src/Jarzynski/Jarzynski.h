#ifndef _JARZYNSKI_H_
#define _JARZYNSKI_H_

#define	DERIVATION_METHOD_FINITE_DIFFERENCES	0x0100
#define	DERIVATION_METHOD_FOURIER		0x0200
#define	DERIVATION_METHOD_SPLINE		0x0400

PXV_FUNC(MENU*, Jarzynski_plot_menu, (void));
PXV_FUNC(int, Jarzynski_main, 			(int argc, char **argv));
PXV_FUNC(int, Jarzynski_unload,		(int argc, char **argv));


#endif

