#ifndef _TILTED_H_
#define _TILTED_H_

PXV_FUNC(int, do_tilted_average_along_y, (void));
PXV_FUNC(MENU*, tilted_image_menu, (void));
PXV_FUNC(int, tilted_main, (int argc, char **argv));
#endif

