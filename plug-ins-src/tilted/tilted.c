/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _TILTED_C_
#define _TILTED_C_

# include "allegro.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"


/* If you include other plug-ins header do it here*/ 

# define 	WRONG_ARGUMENT		-1
# define 	OUT_MEMORY			-2
# define 	FFT_NOT_SUPPORTED	-15
# define	PB_WITH_EXTREMUM	2
# define	TOO_MUCH_NOISE		4

# define PROFILE_SIZE  512

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "tilted.h"

int flatten(float *x, int nx)
{
	register int i, j;
	float ad, af, xd, xf;
	
	for (i = 0, j = (3*nx)/4, ad = af = 0; i < nx/4 ; i++, j++)
	{
		ad += x[i];
		af += x[j];
	}
	ad *= 4;
	af *= 4;
	ad /= nx;
	af /= nx;
	xd = (float)(nx)/8;
	xf = nx - xd;
	ad /= (xf - xd);
	af /= (xf - xd);	
	for (i = 0; i < nx ; i++)
		x[i] -= ad * (xf - i) + af * (i - xd) ;
	return 0;
}
int check_array_valid(float *x1, int nx)
{
	register int i;
	for (i = 0; i < nx; i++)
		if (x1[i] != 0) return 1;
	return 0;
}

int deplace(float *x, int nx)
{
	register int i, j;
	float tmp;
	
	for (i = 0, j = nx/2; j < nx ; i++, j++)
	{
		tmp = x[j];
		x[j] = x[i];
		x[i] = tmp;
	}
	return 0;
}


int	find_specific_plot_in_oi(O_i *oi, char *spe_treat)
{
	register int i, j, k;
	O_p *op;
	d_s *ds;
	int len = 0;
	
	if (oi == NULL || spe_treat == NULL)	return -1;
	len = strlen(spe_treat);
	for (i = 0, j = -1; j == -1 && i < oi->n_op; i++)
	{
		op = oi->o_p[i];
		if (op == NULL)		return -1;
		for (k = 0; k < op->n_dat && j == -1; k++)
		{
			ds = op->dat[k];
			j = (strncmp(ds->treatement,spe_treat,len) == 0) ? i : -1;
		}
	}
	return j;
}


int extract_raw_line_segment (O_i *oi, int nl, float *z, int nz, int nstart, int nend)
{
	register int i, j, nx;
	float zr, zi;
	unsigned char *ch;
	short int *in;
	float *fl;
	
	if (oi == NULL || z == NULL)		return 1;
	/* if (nl >= oi->im.ny || nl < 0)		return 1; */
	nl = (nl < 0) ? 0 : nl;
	nl = (nl >= oi->im.ny) ? oi->im.ny-1 : nl;
	nx = oi->im.nx;
	if (nend < nstart)
	{
		i = nend;
		nend = nstart;
		nstart = i;
	}
	if (nend - nstart > nz)	nend = nstart + nz;
	ch = oi->im.pixel[nl].ch;
	in = oi->im.pixel[nl].in;
	fl = oi->im.pixel[nl].fl;
	if (oi->im.data_type == IS_CHAR_IMAGE)
		for (j = 0, i = nstart; i< nend; i++, j++)
		  z[j] = (float)ch[(nx+i)%nx];//(float)ch[(i<0)?0:((i<nx)?i:nx-1)];
	else if (oi->im.data_type == IS_INT_IMAGE)
		for (j = 0, i = nstart; i< nend; i++, j++)
		  z[j] = (float)in[(nx+i)%nx];//(float)in[(i<0)?0:((i<nx)?i:nx-1)];
	else if (oi->im.data_type == IS_FLOAT_IMAGE)
		for (j = 0, i = nstart; i< nend; i++, j++)
		  z[j] = fl[(nx+i)%nx];//fl[(i<0)?0:((i<nx)?i:nx-1)];
	else if (oi->im.data_type == IS_COMPLEX_IMAGE)
	{
		for (j = 0, i = nstart; i< nend; i++, j++)
		{
		  //zr = fl[2*((i<0)?0:((i<nx)?i:nx-1))];
		  //zi = fl[2*((i<0)?0:((i<nx)?i:nx-1))+1];
		  zr = fl[(2*(nx+i))%(2*nx)];
		  zi = fl[(2*(nx+i)+1)%(2*nx)];
			switch (oi->im.mode)
			{
				case AMP:
				z[j] = sqrt(zr*zr+zi*zi);
				break;
				case LOG_AMP:
				z[j] = zr*zr+zi*zi;
				z[j] = (z[j] > 0) ? log10(z[j]) : -40;
				break;				
				case AMP_2:
				z[j] = zr*zr+zi*zi;
				break;
				case RE:
				z[j] = zr;
				break;
				case IM:
				z[j] = zi;
				break;
			};
		}
	}
	else	return 1;
	return 0;
}
int extract_raw_row_segment(O_i *oi, int nr, float *z, int nz, int nstart, int nend)
{
	register int i, j, ny;
	float zr, zi;
	union pix *ps;
	
	if (oi == NULL || z == NULL)		return win_printf("wrong arg");
	if (nr >= oi->im.nx || nr < 0)		return win_printf("x out of im");
	ny = oi->im.ny;
	if (nend < nstart)
	{
		i = nend;
		nend = nstart;
		nstart = i;
	}
	if (nend - nstart > nz)	nend = nstart + nz;
	ps = oi->im.pixel;
	if (oi->im.data_type == IS_CHAR_IMAGE)
		for (j = 0, i = nstart; i< nend; i++, j++) 
			z[j] = (float)(ps[(i<0)?0:((i<ny)?i:ny-1)].ch[nr]);
	else if (oi->im.data_type == IS_INT_IMAGE)
		for (j = 0, i = nstart; i< nend; i++, j++) 
			z[j] = (float)ps[(i<0)?0:((i<ny)?i:ny-1)].in[nr];
	else if (oi->im.data_type == IS_FLOAT_IMAGE)
		for (j = 0, i = nstart; i< nend; i++, j++) 
			z[j] = ps[(i<0)?0:((i<ny)?i:ny-1)].fl[nr];
	else if (oi->im.data_type == IS_COMPLEX_IMAGE)
	{
		for (j = 0, i = nstart; i< nend; i++, j++) 
		{
			zr = ps[(i<0)?0:((i<ny)?i:ny-1)].fl[2*nr];
			zi = ps[(i<0)?0:((i<ny)?i:ny-1)].fl[2*nr+1];
			switch (oi->im.mode)
			{
				case AMP:
				z[j] = sqrt(zr*zr+zi*zi);
				break;
				case LOG_AMP:
				z[j] = zr*zr+zi*zi;
				z[j] = (z[j] > 0) ? log10(z[j]) : -40;
				break;				
				case AMP_2:
				z[j] = zr*zr+zi*zi;
				break;
				case RE:
				z[j] = zr;
				break;
				case IM:
				z[j] = zi;
				break;
			};
		}
	}
	else	return win_printf("wrong image type");
	return 0;
}
# ifdef PB
float *extract_raw_tilted_segment_of_anle(O_i *oi, float *z, int nz, float xc, float yc, float angle, int len)
{
	float x1l, x2l, y1l, y2l;
	float co, si;

	if (oi == NULL)		return NULL;
	len /=2;
	co = cos(angle);
	si = sin(angle);	
	x1l = xc - len*co;
	x2l = xc + len*co;
	y1l = yc - len*si;
	y2l = yc + len*si;
	return extract_raw_tilted_segment(oi, z, nz, x1l, y1l, x2l, y2l);
}
# endif

float *extract_raw_tilted_segment(O_i *oi, float *z, int nz, float x0, float y0, float x1, float y1)
{
	register int i, nx, ny;
	float zr, zi;
	int type, n;
	float *zb, x, y, ratio, dx, dy, nf;
		
	if (oi == NULL)		return NULL;
	nx = oi->im.nx;
	ny = oi->im.ny;
	ratio = (oi->ay == 0) ? 1 : oi->ax / oi->ay;
	nf = ratio * ratio * (x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0);
	nf = sqrt(nf);
	n = (int)nf + 1;
	if ((zb = z) == NULL)	zb = (float*)calloc(n,sizeof(float));
	if (zb == NULL)	return NULL;
	if (nz < n)		zb = (float*)realloc(z,n*sizeof(float));
	if (zb == NULL)	return NULL; 
	x = x0;
	y = y0;
	dx = (x1 - x0)/nf;
	dy = (y1 - y0)/nf;
	type = oi->im.data_type;
	if (type == IS_CHAR_IMAGE)
		for (i = 0; i< n; i++, x += dx, y += dy) 
			zb[i] = ((float)fast_interpolate_image_point(oi,x,y,0))/256;
	else if (type == IS_INT_IMAGE || type == IS_FLOAT_IMAGE)
		for (i = 0; i< n; i++, x += dx, y += dy) 
			zb[i] = interpolate_image_point(oi,x,y,&zr,NULL);
	else if (type == IS_COMPLEX_IMAGE)
	{
		for (i = 0; i< n; i++, x += dx, y += dy)
		{
			interpolate_image_point(oi,x,y,&zr,&zi);
			switch (oi->im.mode)
			{
				case AMP:
				zb[i] = sqrt(zr*zr+zi*zi);
				break;
				case LOG_AMP:
				zb[i] = zr*zr+zi*zi;
				zb[i] = (zb[i] > 0) ? log10(zb[i]) : -40;
				break;				
				case AMP_2:
				zb[i] = zr*zr+zi*zi;
				break;
				case RE:
				zb[i] = zr;
				break;
				case IM:
				zb[i] = zi;
				break;
			};
		}
	}
	else	return NULL;
	return zb;
}


/*		correlate "x1" with "x2" having "nx" points, returns the correlation
 *		in "fx1". "fx1" and "fx2" will be used to compute fft and result 
 *		setting them to NULL will allocate space. "window" will enable
 *		a windowing process if != 0 a double windowing if = to 2.
 */
int	correlate_1d(float *x1, float *x2, int nx, int window, float *fx1, float *fx2, int filter, int remove_dc)
{
	register int i, j;
	float moy, smp = 0.05, *f = NULL;
	float m1, m2, re1, re2, im1, im2;
	
	if (x1 == NULL || x2 == NULL)		return WRONG_ARGUMENT;
	if (fft_init(nx))			return FFT_NOT_SUPPORTED;
	if (fx1 == NULL)	fx1 = (float*)calloc(nx,sizeof(float));
	if (fx2 == NULL)	f = fx2 = (float*)calloc(nx,sizeof(float));
	if (fx1 == NULL || fx2 == NULL)		return OUT_MEMORY;

	/*	compute fft of x1 */
	for(i = 0; i < nx; i++)		fx1[i] = x1[i];
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += fx1[i] + fx1[j];	
	for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)	
		fx1[i] = x1[i] - moy;
	if (window)	fftwindow1(nx, fx1, smp);
	realtr1(nx, fx1);
	fft(nx, fx1, 1);
	realtr2(nx, fx1, 1);
	
	/*	compute fft of x2 */
	for(i = 0; i < nx; i++)		fx2[i] = x2[i];	
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += fx2[i] + fx2[j];	
	for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)	
		fx2[i] = x2[i] - moy;
	if (window == 2)	fftwindow1(nx, fx2, smp);
	realtr1(nx, fx2);
	fft(nx, fx2, 1);
	realtr2(nx, fx2, 1);

	/*	compute normalization */
	for (i=0, m1 = 0, m2 = 0; i< nx; i+=2)
	{
		m1 += fx1[i] * fx1[i] + fx1[i+1] * fx1[i+1];
		m2 += fx2[i] * fx2[i] + fx2[i+1] * fx2[i+1];
	}
	
	/*	compute correlation in Fourier space */
	for (m1 = sqrt(m1)*sqrt(m2), i=2; i< nx; i+=2)
	{
		re1 = fx1[i];		re2 = fx2[i];
		im1 = fx1[i+1];		im2 = fx2[i+1];
		fx1[i] 		= (re1 * re2 + im1 * im2)/m1;
		fx1[i+1] 	= (im1 * re2 - re1 * im2)/m1;
	}
	fx1[0]	= 2*(fx1[0] * fx2[0])/m1;	/* these too mode are special */
	fx1[1] 	= 2*(fx1[1] * fx2[1])/m1;
	if (filter> 0)				lowpass_smooth_half (nx, fx1, filter);
	
	/*	get back to real world */
	realtr2(nx, fx1, -1);
	fft(nx, fx1, -1);
	realtr1(nx, fx1);
	deplace(fx1, nx);
	if (f != NULL)	free(f);
	return 0;
}	

/*		correlate "x1" with its invert over "nx" points, returns the correlation
 *		in "fx1". "fx1" will be used to compute fft and result 
 *		setting them to NULL will allocate space. "window" will enable
 *		a windowing process if != 0 a double windowing if = to 2.
 */
int	correlate_1d_sig_and_invert(float *x1, int nx, int window, float *fx1, int filter, int remove_dc)
{
	register int i, j;
	float moy, smp = 0.05;
	float m1, re1, re2, im1, im2;
	
	if (x1 == NULL)		return WRONG_ARGUMENT;
	if (fft_init(nx))			return FFT_NOT_SUPPORTED;
	if (fx1 == NULL)	fx1 = (float*)calloc(nx,sizeof(float));
	if (fx1 == NULL)		return OUT_MEMORY;

	/*	compute fft of x1 */
	for(i = 0; i < nx; i++)		fx1[i] = x1[i];
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += fx1[i] + fx1[j];	
	for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)	
		fx1[i] = x1[i] - moy;
	if (window)	fftwindow1(nx, fx1, smp);
	realtr1(nx, fx1);
	fft(nx, fx1, 1);
	realtr2(nx, fx1, 1);
	
	/*	compute normalization */
	for (i=0, m1 = 0; i< nx; i+=2)
		m1 += fx1[i] * fx1[i] + fx1[i+1] * fx1[i+1];
	m1 /= 2;	
	m1 = (m1 == 0) ? 1 : m1;	
	/*	compute correlation in Fourier space */
	for (i=2; i< nx; i+=2)
	{
		re1 = fx1[i];		re2 = fx1[i];
		im1 = fx1[i+1];		im2 = -fx1[i+1];
		fx1[i] 		= (re1 * re2 + im1 * im2)/m1;
		fx1[i+1] 	= (im1 * re2 - re1 * im2)/m1;
	}
	fx1[0]	= 2*(fx1[0] * fx1[0])/m1;	/* these too mode are special */
	fx1[1] 	= 2*(fx1[1] * fx1[1])/m1;
	if (filter> 0)				lowpass_smooth_half (nx, fx1, filter);
	
	/*	get back to real world */
	realtr2(nx, fx1, -1);
	fft(nx, fx1, -1);
	realtr1(nx, fx1);
	deplace(fx1, nx);
	return 0;
}	

int	correlate_1d_sig_and_invert_new(float *x1, int nx, int window, float *fx1, int filter, int remove_dc)
{
	register int i, j;
	float moy, smp = 0.05;
	float m1, re1, re2, im1, im2;
	
	if (x1 == NULL)		return WRONG_ARGUMENT;
	if (fft_init(nx))			return FFT_NOT_SUPPORTED;
	if (fx1 == NULL)	fx1 = (float*)calloc(nx,sizeof(float));
	if (fx1 == NULL)		return OUT_MEMORY;

	/*	compute fft of x1 */
	for(i = 0; i < nx; i++)		fx1[i] = x1[i];
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += fx1[i] + fx1[j];	
	for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)	
		fx1[i] = x1[i] - moy;
	if (window)	fftwindow1(nx, fx1, smp);
	realtr1(nx, fx1);
	fft(nx, fx1, 1);
	realtr2(nx, fx1, 1);
	
	/*	compute correlation in Fourier space */
	for (i=2; i< nx; i+=2)
	{
		re1 = fx1[i];		re2 = fx1[i];
		im1 = fx1[i+1];		im2 = -fx1[i+1];
		fx1[i] 		= (re1 * re2 + im1 * im2);
		fx1[i+1] 	= (im1 * re2 - re1 * im2);
	}
	fx1[0]	= 2*(fx1[0] * fx1[0]);	/* these too mode are special */
	fx1[1] 	= 2*(fx1[1] * fx1[1]);
	if (filter> 0)				lowpass_smooth_half (nx, fx1, filter);

	/*	compute normalization */
	for (i=0, m1 = 0; i< nx; i+=2)
		m1 += fx1[i] * fx1[i] + fx1[i+1] * fx1[i+1];
	for (i=0, m1 = 1/m1; i< nx; i++)	fx1[i] *= m1;
	
			
	/*	get back to real world */
	realtr2(nx, fx1, -1);
	fft(nx, fx1, -1);
	realtr1(nx, fx1);
	deplace(fx1, nx);
	return 0;
}	

/*	Find maximum position in a 1d array, the array is supposed periodic
 *	return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int	find_max1(float *x, int nx, float *Max_pos, float *Max_val)
{
	register int i;
	int  nmax, ret = 0, delta;
	double a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0;

	for (i = 0, nmax = 0; i < nx; i++) nmax = (x[i] > x[nmax]) ? i : nmax;
	f_2 = x[(nx + nmax - 2) % nx];
	f_1 = x[(nx + nmax - 1) % nx];
	f0 = x[nmax];
	f1 = x[(nmax + 1) % nx];
	f2 = x[(nmax + 2) % nx];
	if (f_1 < f1)
	{
		a = -f_1/6 + f0/2 - f1/2 + f2/6;
		b = f_1/2 - f0 + f1/2;
		c = -f_1/3 - f0/2 + f1 - f2/6;
		d = f0;
		delta = 0;
	}	
	else
	{
		a = b = c = 0;	
		a = -f_2/6 + f_1/2 - f0/2 + f1/6;
		b = f_2/2 - f_1 + f0/2;
		c = -f_2/3 - f_1/2 + f0 - f1/6;
		d = f_1;
		delta = -1;
	}
	if (fabs(a) < 1e-8)
	{
		if (b != 0)		xm =  - c/(2*b) - (3 * a * c * c)/(4 * b * b * b);
		else
		{
			xm = 0;
			ret |= PB_WITH_EXTREMUM;
		}
	}
	else if ((b*b - 3*a*c) < 0)
	{
		ret |= PB_WITH_EXTREMUM;
		xm = 0;
	}
	else
		xm = (-b - sqrt(b*b - 3*a*c))/(3*a);
	/*
	for (p = -2; p < 1; p++)
	{
		if (6*a*p+b > 0) ret |= TOO_MUCH_NOISE;
	}	
	*/
	*Max_pos = (float)(xm + nmax + delta); 
	*Max_val = (a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d;
	return ret;
}

int	follow_horizontal_line(void)
{
  register int i, j, k;
	static int cl = 32;
	static float min_corel = .5;
	BITMAP *ds_bitmap = NULL;
	imreg *imrs;	
	O_p *op;
	O_i *ois;
	d_s *ds;
	int not_done, xc = 0, yc = 0, oldyc, onx, ony, nu, nd, xc0;
	float fy1[PROFILE_SIZE], *xu, *xd, *yu, *yd;
	float y1[PROFILE_SIZE], y2[PROFILE_SIZE], *y1p = y1, *y2p = y2;	
	float ycf, dy, max;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	
	if (ac_grep(cur_ac_reg,"%im%oi",&imrs,&ois) != 2)	
		return win_printf("cannot find image in acreg!");
	
	i = win_scanf("this routine tracks a semi-horizontal line along increasing x\n"
		      "using a correlation schems with a vertical segment\n"
		      "segment length (2^n <= 128) %d\nutill corelation falls to %f",&cl,&min_corel);
	if (i == CANCEL)	return D_O_K;
	if (cl > PROFILE_SIZE)	return win_printf_OK("cannot use arm bigger\nthan 128");

	if (fft_init(cl) || filter_init(cl))	return win_printf("cannot init fft!");	
	onx = ois->im.nx;	ony = ois->im.ny;
	xu = (float*)calloc(2*onx,sizeof(float));
	xd = (float*)calloc(2*onx,sizeof(float));
	yu = (float*)calloc(2*onx,sizeof(float));	
	yd = (float*)calloc(2*onx,sizeof(float));	
	if (xu == NULL || yu == NULL || xd == NULL || yd == NULL)		
		return win_printf("cannot allocate mem!");


	ds_bitmap = create_bitmap(3,cl);
	clear_to_color(ds_bitmap, Lightmagenta);
	rect(ds_bitmap, 0, 0, 3, cl, Lightred);
	scare_mouse();    
	set_mouse_sprite(ds_bitmap);
	set_mouse_sprite_focus(1, cl/2);
	unscare_mouse();    

	while ((mouse_b & 0x3) == 0);
	while (mouse_b & 0x3)
	  {
	    xc = x_imr_2_imdata(imrs,mouse_x);
	    yc = y_imr_2_imdata(imrs,mouse_y) -cl/2;
	  }

	scare_mouse();    
	set_mouse_sprite(NULL);
	set_mouse_sprite_focus(0, 0);
	unscare_mouse();    
	if (ds_bitmap != NULL)    
	  {
	    destroy_bitmap(ds_bitmap);
	    ds_bitmap = NULL;
	  }    


	ycf = yc;
	xc0 = xc;

	//extract_raw_tilted_segment(ois, y1, PROFILE_SIZE, xc, yc - cl/2, xc, yc + cl/2);
	extract_raw_row_segment(ois, xc, y1, PROFILE_SIZE, yc - cl/2, yc + cl/2);
	/*
	op = create_and_attach_op_to_oi(ois, cl,cl, 0,IM_SAME_X_AXIS+IM_SAME_Y_AXIS);
	if (op == NULL)		return win_printf("cannot create plot!");		
	ds = op->dat[0];
	ds->treatement = Mystrdupre(ds->treatement, "horizontal profile tracking");
	ds->source = Mystrdupre(ds->source, ois->im.source);
	for (i = 0; i < cl; i++)
	  {
	    ds->xd[i] = i;
	    ds->yd[i] = y1[i];
	  }
	return refresh_im_plot(imrs, ois->n_op - 1);
	*/
	correlate_1d_sig_and_invert(y1, cl, 0, fy1, cl>>2, 1);
	find_max1(fy1, cl, &dy,&max);
	dy -= cl/2;
	dy /=2;
	ycf = dy + yc;
	oldyc = yc = (int)ycf;
	xu[0] = xc;
	yu[0] = ycf;
	//k = win_printf("xc %d yc %d dy = %g\nMax = %g min_corel %g \nto set not_done to 0 press cancel",xc,yc, dy,max,min_corel);

	for(not_done = 1, i = 1, xc = xc0 + 1; not_done == 1 && xc < onx; i++, xc++)
	{
		y1p = (i%2) ? y1 : y2;
		y2p = (i%2) ? y2 : y1;
		extract_raw_row_segment(ois, xc, y2p, PROFILE_SIZE, yc - cl/2, yc + cl/2);
		//extract_raw_tilted_segment(ois, y2p, PROFILE_SIZE, xc, yc - cl/2, xc, yc + cl/2);
		correlate_1d_sig_and_invert(y2p, cl, 0, fy1, cl>>2, 1);
		find_max1(fy1, cl, &dy,&max);
		dy -= cl/2;
		dy /=2;
		ycf = dy + yc;
		oldyc = yc;
		yc = (int)ycf;
		not_done = (max < min_corel) ? 0 : 1;
		//k = win_printf("xc %d yc %d dy = %g\nMax = %g min_corel %g \nto set not_done to 0 press cancel",xc,yc, dy,max,min_corel);
		//not_done = (k == CANCEL) ? 0 : 1;
		if (not_done)
		{
			xu[i] = xc;
			yu[i] = ycf;
		}
	}
	nu = (i > 0) ? i - 1 : 0;
	ycf = yu[0];
	oldyc = yc = (int)ycf;
	for(not_done = 1, i = 0, xc = xc0 - 1; not_done == 1 && xc >=0; i++, xc--)
	{
		y1p = (i%2) ? y1 : y2;
		y2p = (i%2) ? y2 : y1;
		extract_raw_row_segment(ois, xc, y2p, PROFILE_SIZE, yc - cl/2, yc + cl/2);
		//extract_raw_tilted_segment(ois, y2p, PROFILE_SIZE, xc, yc - cl/2, xc, yc + cl/2);
		correlate_1d_sig_and_invert(y2p, cl, 0, fy1, cl>>2, 1);
		find_max1(fy1, cl, &dy,&max);
		dy -= cl/2;
		dy /=2;
		ycf = dy + yc;
		oldyc = yc;
		yc = (int)ycf;
		not_done = (max < min_corel) ? 0 : 1;
		if (not_done)
		{
			xd[i] = xc;
			yd[i] = ycf;
		}
	}
	nd = (i > 0) ? i - 1 : 0;	
	i = find_specific_plot_in_oi(ois, "horizontal profile tracking");
	if (i != -1 && i != ois->cur_op)	refresh_im_plot(imrs, i);
	if (i == -1)
	{
		op = create_and_attach_op_to_oi(ois, nu+nd,nu+nd, 0,IM_SAME_X_AXIS+IM_SAME_Y_AXIS);
		if (op == NULL)		return win_printf("cannot create plot!");		
		op->type |= IM_SAME_Y_AXIS;
		op->type |= IM_SAME_X_AXIS;
		ds = op->dat[0];
		ds->treatement = Mystrdupre(ds->treatement, "horizontal profile tracking");
		ds->source = Mystrdupre(ds->source, ois->im.source);
		op->filename = Transfer_filename(ois->filename);
		inherit_from_im_to_ds(ds, ois);	
		uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
		uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	}
	else
	{
		ds = create_and_attach_ds_to_oi(ois,nu+nd,nu+nd,0);
		if (ds == NULL)		return win_printf("cannot create ds!");
		ds->treatement = Mystrdupre(ds->treatement, "horizontal profile tracking");
		ds->source = Mystrdupre(ds->source, ois->im.source);
		inherit_from_im_to_ds(ds, ois);	
	}				
	for (i = 0; i < nd; i++)
	{
		ds->xd[i] = xd[nd-1-i];
		ds->yd[i] = yd[nd-1-i];
	}					
	for (i = 0, j = nd; i < nu; i++, j++)
	{
		ds->xd[j] = xu[i];
		ds->yd[j] = yu[i];
	}								
	free(xu);
	free(xd);
	free(yu);
	free(yd);	
	return refresh_im_plot(imrs, ois->n_op - 1);
}

int	follow_horizontal_line_local_min(void)
{
  register int i, j, k;
	static int cl = 32;
	static float min_corel = .5;
	BITMAP *ds_bitmap = NULL;
	imreg *imrs;	
	O_p *op;
	O_i *ois;
	d_s *ds;
	int not_done, xc = 0, yc = 0, oldyc, onx, ony, nu, nd, xc0;
	float fy1[PROFILE_SIZE], *xu, *xd, *yu, *yd;
	float y1[PROFILE_SIZE], y2[PROFILE_SIZE], *y1p = y1, *y2p = y2;	
	float ycf, dy, max;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	
	if (ac_grep(cur_ac_reg,"%im%oi",&imrs,&ois) != 2)	
		return win_printf("cannot find image in acreg!");
	
	i = win_scanf("this routine tracks a semi-horizontal line along increasing x\n"
		      "using a correlation schems with a vertical segment\n"
		      "segment length (2^n <= 128) %d\nutill corelation falls to %f",&cl,&min_corel);
	if (i == CANCEL)	return D_O_K;
	if (cl > PROFILE_SIZE)	return win_printf_OK("cannot use arm bigger\nthan 128");

	if (fft_init(cl) || filter_init(cl))	return win_printf("cannot init fft!");	
	onx = ois->im.nx;	ony = ois->im.ny;
	xu = (float*)calloc(2*onx,sizeof(float));
	xd = (float*)calloc(2*onx,sizeof(float));
	yu = (float*)calloc(2*onx,sizeof(float));	
	yd = (float*)calloc(2*onx,sizeof(float));	
	if (xu == NULL || yu == NULL || xd == NULL || yd == NULL)		
		return win_printf("cannot allocate mem!");


	ds_bitmap = create_bitmap(3,cl);
	clear_to_color(ds_bitmap, Lightmagenta);
	rect(ds_bitmap, 0, 0, 3, cl, Lightred);
	scare_mouse();    
	set_mouse_sprite(ds_bitmap);
	set_mouse_sprite_focus(1, cl/2);
	unscare_mouse();    

	while ((mouse_b & 0x3) == 0);
	while (mouse_b & 0x3)
	  {
	    xc = x_imr_2_imdata(imrs,mouse_x);
	    yc = y_imr_2_imdata(imrs,mouse_y);
	  }

	scare_mouse();    
	set_mouse_sprite(NULL);
	set_mouse_sprite_focus(0, 0);
	unscare_mouse();    
	if (ds_bitmap != NULL)    
	  {
	    destroy_bitmap(ds_bitmap);
	    ds_bitmap = NULL;
	  }    


	ycf = yc;
	xc0 = xc;

	extract_raw_tilted_segment(ois, y1, PROFILE_SIZE, xc, yc - cl/2, xc, yc + cl/2);
	correlate_1d_sig_and_invert(y1, cl, 0, fy1, cl>>2, 1);
	find_max1(fy1, cl, &dy,&max);
	dy -= cl/2;
	dy /=2;
	ycf = dy + yc;
	oldyc = yc = (int)ycf;
	xu[0] = xc;
	yu[0] = ycf;

	for(not_done = 1, i = 1, xc = xc0 + 1; not_done == 1 && xc < onx; i++, xc++)
	{
		y1p = (i%2) ? y1 : y2;
		y2p = (i%2) ? y2 : y1;
		extract_raw_tilted_segment(ois, y2p, PROFILE_SIZE, xc, yc - cl/2, xc, yc + cl/2);
		correlate_1d_sig_and_invert(y2p, cl, 0, fy1, cl>>2, 1);
		find_max1(fy1, cl, &dy,&max);
		dy -= cl/2;
		dy /=2;
		ycf = dy + yc;
		oldyc = yc;
		yc = (int)ycf;
		not_done = (max < min_corel) ? 0 : 1;
		if (not_done)
		{
			xu[i] = xc;
			yu[i] = ycf;
		}
	}
	nu = (i > 0) ? i - 1 : 0;
	ycf = yu[0];
	oldyc = yc = (int)ycf;
	for(not_done = 1, i = 0, xc = xc0 - 1; not_done == 1 && xc >=0; i++, xc--)
	{
		y1p = (i%2) ? y1 : y2;
		y2p = (i%2) ? y2 : y1;
		extract_raw_tilted_segment(ois, y2p, PROFILE_SIZE, xc, yc - cl/2, xc, yc + cl/2);
		correlate_1d_sig_and_invert(y2p, cl, 0, fy1, cl>>2, 1);
		find_max1(fy1, cl, &dy,&max);
		dy -= cl/2;
		dy /=2;
		ycf = dy + yc;
		oldyc = yc;
		yc = (int)ycf;
		not_done = (max < min_corel) ? 0 : 1;
		if (not_done)
		{
			xd[i] = xc;
			yd[i] = ycf;
		}
	}
	nd = (i > 0) ? i - 1 : 0;	
	i = find_specific_plot_in_oi(ois, "horizontal profile tracking");
	if (i != -1 && i != ois->cur_op)	refresh_im_plot(imrs, i);
	if (i == -1)
	{
		op = create_and_attach_op_to_oi(ois, nu+nd,nu+nd, 0,IM_SAME_X_AXIS+IM_SAME_Y_AXIS);
		if (op == NULL)		return win_printf("cannot create plot!");		
		op->type |= IM_SAME_Y_AXIS;
		op->type |= IM_SAME_X_AXIS;
		ds = op->dat[0];
		ds->treatement = Mystrdupre(ds->treatement, "horizontal profile tracking");
		ds->source = Mystrdupre(ds->source, ois->im.source);
		op->filename = Transfer_filename(ois->filename);
		inherit_from_im_to_ds(ds, ois);	
		uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
		uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	}
	else
	{
		ds = create_and_attach_ds_to_oi(ois,nu+nd,nu+nd,0);
		if (ds == NULL)		return win_printf("cannot create ds!");
		ds->treatement = Mystrdupre(ds->treatement, "horizontal profile tracking");
		ds->source = Mystrdupre(ds->source, ois->im.source);
		inherit_from_im_to_ds(ds, ois);	
	}				
	for (i = 0; i < nd; i++)
	{
		ds->xd[i] = xd[nd-1-i];
		ds->yd[i] = yd[nd-1-i];
	}					
	for (i = 0, j = nd; i < nu; i++, j++)
	{
		ds->xd[j] = xu[i];
		ds->yd[j] = yu[i];
	}								
	free(xu);
	free(xd);
	free(yu);
	free(yd);	
	return refresh_im_plot(imrs, ois->n_op - 1);
}


int	follow_vertical_line(void)
{
	register int i, j, k;
	static int cl = 32;
	static int navg = 1;
	static float min_corel = .5;
	BITMAP *ds_bitmap = NULL;
	imreg *imrs;	
	O_p *op;
	O_i *ois;
	d_s *ds;
	int not_done, xc = 0, yc = 0, oldxc, onx, ony, nu, nd, yc0;
	float fx1[PROFILE_SIZE], *xu, *xd, *yu, *yd;
	float x1[PROFILE_SIZE], x2[PROFILE_SIZE];	
	float xcf, dx, max;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}

	
	if (ac_grep(cur_ac_reg,"%im%oi",&imrs,&ois) != 2)	
		return win_printf("cannot find image in acreg!");
	
	i = win_scanf("this routine tracks a semi-vertical line using a correlation schems\n"
		      "with a horizontal segment segment length (2^n <= 256) %d"
		      "utill corelation falls to %fnavg %d",&cl,&min_corel,&navg);
	if (i == CANCEL)	return OFF; 
	if (cl > PROFILE_SIZE)	return win_printf("cannot use arm bigger\nthan 256");

	if (fft_init(cl) || filter_init(cl))	return win_printf("cannot init fft!");	
	onx = ois->im.nx;	ony = ois->im.ny;
	xu = (float*)calloc(2*onx,sizeof(float));
	xd = (float*)calloc(2*onx,sizeof(float));
	yu = (float*)calloc(2*onx,sizeof(float));	
	yd = (float*)calloc(2*onx,sizeof(float));	
	if (xu == NULL || yu == NULL || xd == NULL || yd == NULL)		
		return win_printf("cannot allocate mem!");



	ds_bitmap = create_bitmap(cl,3);
	clear_to_color(ds_bitmap, Lightmagenta);
	rect(ds_bitmap, 0, 0, cl, 3, Lightred);
	scare_mouse();    
	set_mouse_sprite(ds_bitmap);
	set_mouse_sprite_focus(cl/2,1);
	unscare_mouse();    

	while ((mouse_b & 0x3) == 0);
	while (mouse_b & 0x3)
	  {
	    xc = x_imr_2_imdata(imrs,mouse_x);
	    yc = y_imr_2_imdata(imrs,mouse_y);
	  }

	scare_mouse();    
	set_mouse_sprite(NULL);
	set_mouse_sprite_focus(0, 0);
	unscare_mouse();    
	if (ds_bitmap != NULL)    
	  {
	    destroy_bitmap(ds_bitmap);
	    ds_bitmap = NULL;
	  }    


	yc0 = yc;
	xcf = xc;


	if (extract_raw_line_segment(ois, yc, x1, PROFILE_SIZE, xc - cl/2, xc + cl/2))
		win_printf("Pb extracting profile");

	flatten(x1, cl);
	correlate_1d_sig_and_invert(x1, cl, 0, fx1, cl>>2, 1);
	find_max1(fx1, cl, &dx,&max);
	dx -= cl/2;
	dx /=2;
	xcf = dx + xc;
	oldxc = xc = (int)xcf;
	yu[0] = yc;
	xu[0] = xcf;
	for(not_done = 1, i = 1, yc = yc0 + 1; not_done == 1 && yc < ony; i++, yc++)
	{

		for (k = 0; k < cl; k++)	x1[k] = 0;
		for (j = -(navg/2); j <= navg/2; j++)
		{
			if (extract_raw_line_segment(ois, yc+j, x2, PROFILE_SIZE, xc - cl/2, xc + cl/2))
			{
				win_printf("Pb extracting profile at line %d j = %d",yc,j);
				not_done = 0;
			}
			for (k = 0; k < cl; k++)	x1[k] += x2[k];
		}
		if (check_array_valid(x1, cl))
		{
			flatten(x1, cl);	
			correlate_1d_sig_and_invert(x1, cl, 0, fx1, cl>>2, 1);
			find_max1(fx1, cl, &dx,&max);
			dx -= cl/2;
			dx /=2;
		}
		else win_printf("zero filled array ! at line %d",yc);
		xcf = dx + xc;
		oldxc = xc;
		xc = (int)xcf;
		not_done = (not_done == 0 || max < min_corel) ? 0 : 1;
		if (not_done)
		{
			yu[i] = yc;
			xu[i] = xcf;
		}
	}
	nu = (i > 0) ? i - 1 : 0;
	xcf = xu[0];
	oldxc = xc = (int)xcf;
	
	for(not_done = 1, i = 0, yc = yc0 - 1; not_done == 1 && yc >= 0; i++, yc--)
	{
		for (k = 0; k < cl; k++)	x1[k] = 0;
		for (j = -(navg/2); j <= navg/2; j++)
		{
			if (extract_raw_line_segment(ois, yc+j, x2, PROFILE_SIZE, xc - cl/2, xc + cl/2))
			{
				win_printf("Pb extracting profile at %d j = %d",yc,j);
				not_done = 0;
			}
			for (k = 0; k < cl; k++)	x1[k] += x2[k];
		}	
		if (check_array_valid(x1, cl))
		{
			flatten(x1, cl);	
			correlate_1d_sig_and_invert(x1, cl, 0, fx1, cl>>2, 1);
			find_max1(fx1, cl, &dx,&max);
			dx -= cl/2;
			dx /=2;
		}
		else win_printf("zero filled array ! at %d",yc);		
		xcf = dx + xc;
		oldxc = xc;
		xc = (int)xcf;
		not_done = (not_done == 0 || max < min_corel) ? 0 : 1;
		if (not_done)
		{
			yd[i] = yc;
			xd[i] = xcf;
		}
	}
	
	nd = (i > 0) ? i - 1 : 0;	
	
	i = find_specific_plot_in_oi(ois, "vertical profile tracking");
	if (i != -1 && i != ois->cur_op)	refresh_im_plot(imrs, i);
	if (i == -1)
	{
		op = create_and_attach_op_to_oi(ois, nu+nd,nu+nd, 0,IM_SAME_X_AXIS+IM_SAME_Y_AXIS);
		if (op == NULL)		return win_printf("cannot create plot!");
		op->type |= IM_SAME_Y_AXIS;
		op->type |= IM_SAME_X_AXIS;
		ds = op->dat[0];
		ds->treatement = Mystrdupre(ds->treatement, "vertical profile tracking");
		ds->source = Mystrdupre(ds->source, ois->im.source);
		op->filename = Transfer_filename(ois->filename);
		inherit_from_im_to_ds(ds, ois);	
		uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
		uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	}
	else
	{
		ds = create_and_attach_ds_to_oi(ois,nu+nd,nu+nd,0);
		if (ds == NULL)		return win_printf("cannot create ds!");
		ds->treatement = Mystrdupre(ds->treatement, "vertical profile tracking");
		ds->source = Mystrdupre(ds->source, ois->im.source);
		inherit_from_im_to_ds(ds, ois);	
	}				
	for (i = 0; i < nd; i++)
	{
		ds->xd[i] = xd[nd-1-i];
		ds->yd[i] = yd[nd-1-i];
	}					
	for (i = 0, j = nd; i < nu; i++, j++)
	{
		ds->xd[j] = xu[i];
		ds->yd[j] = yu[i];
	}								
	free(yu);
	free(xd);
	free(yu);
	free(yd);	
	return refresh_im_plot(imrs, ois->n_op - 1);
}


int do_tilted_average_along_y(void)
{
	register int i, j;
	int l_s, l_e;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average the image intensity"
			"of several lines of an image");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	return D_O_K;
	/* we grap the data that we need, the image and the screen region*/
	l_s = 0; l_e = ois->im.ny;
	/* we ask for the limit of averaging*/
	i = win_scanf("Average Between lines%d and %d",&l_s,&l_e);
	if (i == CANCEL)	return D_O_K;
	/* we create a plot to hold the profile*/
	op = create_one_plot(ois->im.nx,ois->im.nx,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0]; /* we grab the data set */
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	op->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;
	inherit_from_im_to_ds(ds, ois);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"tilted averaged profile from line %d to %d",l_s, l_e);
	set_formated_string(&ds->treatement,"%s averaged profile from line %d to %d",l_s, l_e);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	for (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, ds->xd);
		for (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];
	}
	for (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)
	{
		ds->xd[i] = i;
		ds->yd[i] /= j;
	}
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}

O_i	*tilted_image_multiply_by_a_scalar(O_i *ois, float factor)
{
	register int i, j;
	O_i *oid;
	float tmp;
	int onx, ony, data_type;
	union pix *ps, *pd;

	onx = ois->im.nx;	ony = ois->im.ny;	data_type = ois->im.data_type;
	oid =  create_one_image(onx, ony, data_type);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	if (data_type == IS_CHAR_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_INT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].in[j];
				pd[i].in[j] = (tmp < -32768) ? -32768 :
					((tmp > 32767) ? 32767 : (short int)tmp);
			}
		}
	}
	else if (data_type == IS_UINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ui[j];
				pd[i].ui[j] = (tmp < 0) ? 0 :
					((tmp > 65535) ? 65535 : (unsigned short int)tmp);
			}
		}
	}
	else if (data_type == IS_LINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].li[j];
				pd[i].li[j] = (tmp < 0x10000000) ? 0x10000000 :
					((tmp > 0x7FFFFFFF) ? 0x7FFFFFFF : (int)tmp);
			}
		}
	}
	else if (data_type == IS_FLOAT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[j] = factor * ps[i].fl[j];
			}
		}
	}
	else if (data_type == IS_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[j] = factor * ps[i].db[j];
			}
		}
	}
	else if (data_type == IS_COMPLEX_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[2*j] = factor * ps[i].fl[2*j];
				pd[i].fl[2*j+1] = factor * ps[i].fl[2*j+1];
			}
		}
	}
	else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[2*j] = factor * ps[i].db[2*j];
				pd[i].db[2*j+1] = factor * ps[i].db[2*j+1];
			}
		}
	}
	else if (data_type == IS_RGB_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 3*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_RGBA_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 4*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s rescaled by %g",
		 ois->title,factor);
	set_formated_string(&oid->im.treatement,
		"Image %s rescaled by %g", ois->filename,factor);
	return oid;
}

int do_tilted_image_multiply_by_a_scalar(void)
{
	O_i *ois, *oid;
	imreg *imr;
	int i;
	static float factor = 2.0;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("define the factor of mutiplication %f",&factor);
	if (i == CANCEL)	return D_O_K;
	oid = tilted_image_multiply_by_a_scalar(ois,factor);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}


MENU *tilted_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"follow horizontal profile", follow_horizontal_line,NULL,0,NULL);
	add_item_to_menu(mn,"follow vertical profile", follow_vertical_line,NULL,0,NULL);
	add_item_to_menu(mn,"image z rescaled", do_tilted_image_multiply_by_a_scalar,NULL,0,NULL);
	return mn;
}

int	tilted_main(int argc, char **argv)
{
	add_image_treat_menu_item ( "tilted", NULL, tilted_image_menu(), 0, NULL);
	return D_O_K;
}

int	tilted_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "tilted", NULL, NULL);
	return D_O_K;
}
#endif

