#ifndef _IMOLISEQ_H_
#define _IMOLISEQ_H_
#include "xvin.h"

PXV_FUNC(int, do_ImOliSeq_average_along_y, (void));
PXV_FUNC(MENU*, ImOliSeq_image_menu, (void));
PXV_FUNC(int, ImOliSeq_main, (int argc, char **argv));
PXV_FUNC(int, generate_Plot_Oligo_Seq, (O_p *op, int idsol, int idssl, int idshisto, int pol, int psl, float stretch_minl, float stretch_maxl, int dir, float *linear_fit_coef, const char *label));
#endif

