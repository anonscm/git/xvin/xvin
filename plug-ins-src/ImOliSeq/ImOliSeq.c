/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _IMOLISEQ_C_
#define _IMOLISEQ_C_

# include "allegro.h"
# include "xvin.h"
# include "float.h"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "ImOliSeq.h"
//place here other headers of this plugin

static float dshisto_offest = 0;
static int idso = 2,  idss = 1, idshisto = 0, po = 15, ps = 50, dirr = 0;
static float stretch_min = 0.8, stretch_max = 0.95;



/*
O_i     *create_Im_Oligo_Seq(O_i *oi, d_s *dso, d_s *dss, int po, int ps, float stretch_min, float stretch_max)
{
    register int i, j;
    O_i *oid;
    float tmp, stretch;
    int onx, ony, ory, orx, data_type;
    union pix *psx;


    if (dso == NULL || dss == NULL )  return NULL;
    po = (po < dso->nx) ? po : dso->nx - 1;
    po = (po < 0) ? 0 : po;
    ps = (ps < dss->nx) ? ps : dss->nx - 1;
    ps = (ps < 0) ? 0 : ps;
    stretch = (stretch_min + stretch_max)/2;
    ony = (int) ((dso->yd[dso->nx-1] - dso->yd[0]) * 1.2 + 0.5);
    ory = (int) (dso->yd[0] + 0.5 - (dso->yd[dso->nx-1] - dso->yd[0]) * 0.1);

    onx = (int) ((dso->yd[dso->nx-1] - dso->yd[0]) * stretch_max * 1.2 + 0.5);
    omy = dso->yd[po] - ory;
    orx = dss->xd[ps] - omy * stretch_max;
    oex = orx + onx;
    to_create = 0;
    if (oi != NULL)
      {
        if (oi->im.data_type != IS_CHAR_IMAGE) to_create = 1;
        if (onx != oi->im.nx || ony != ois->im.ny) to_create = 1;
      }
    if (to_create) oid =  create_one_image(onx, ony, IS_CHAR_IMAGE);
    else oid = oi;
    if (oid == NULL) return NULL;
    set_all_pixel_to_color(oid, 0, 0);
    for (i = 0; i < dso->nx; i++)
      {
        ky = dso->yd[i] - ory;
        if (ky < 0 || ky >= ony) continue;
        ch = oid->im.pixel[ky].ch;
        for (j = 0; j < onx; j++)   ch[j] = 5;
        for (j = 0; j < dss->nx; j++)
          {
        kx = dss->xd[j] - orx;
        if (kx < 0 || kx >= onx) continue;
        for (k = 0; k < ony; k++) oid->im.pixel[k].ch[kx] = 5;
        x = dss->xd[j] - dss->xd[ps];
        y = dso->yd[i] - dso->xd[po];

          }
      }

    return oid;
}

*/



typedef struct _hit
{
    int oliindnm;
    int seqindbp;
    float oliposnm;
    float seqposbp;
    int n_oliind;
    int n_seqind;
    int non_ambiguous;
    int pivot;
} hit;

typedef struct _hit_array
{
    hit* hit;
    int n_hit;
    int m_hit;
    int i_hit;
    int i_pivot;
} hitar;

int find_n_oliind(hitar *a)
{
   int i, j, k;
   if (a == NULL || a->hit == NULL) return 1;
   for(i = 0; i < a->n_hit; i++)
           a->hit[i].n_oliind = 0;
   for(j = 0; j < a->n_hit; j++)
      {
          if (a->hit[j].n_oliind > 0) continue;
          a->hit[j].n_oliind = 1;
          for(i = 0; i < a->n_hit; i++)
            {
                if (i == j) continue;
                if (a->hit[i].oliindnm != a->hit[j].oliindnm) continue;
                a->hit[j].n_oliind++;
                a->hit[i].n_oliind = -1;
            }
          k = a->hit[j].n_oliind;
          for(i = 0; i < a->n_hit; i++)
            {
                if (a->hit[i].n_oliind >= 0) continue;
                a->hit[i].n_oliind = k;
            }
      }
   return 0;
}
int find_n_seqind(hitar *a)
{
   int i, j, k;
   if (a == NULL || a->hit == NULL) return 1;
   for(i = 0; i < a->n_hit; i++)
           a->hit[i].n_seqind = 0;
   for(j = 0; j < a->n_hit; j++)
      {
          if (a->hit[j].n_seqind > 0) continue;
          a->hit[j].n_seqind = 1;
          for(i = 0; i < a->n_hit; i++)
            {
                if (i == j) continue;
                if (a->hit[i].seqindbp != a->hit[j].seqindbp) continue;
                a->hit[j].n_seqind++;
                a->hit[i].n_seqind = -1;
            }
          k = a->hit[j].n_seqind;
          for(i = 0; i < a->n_hit; i++)
            {
                if (a->hit[i].n_seqind >= 0) continue;
                a->hit[i].n_seqind = k;
            }
      }
   return 0;
}

hitar *create_hit_array(int size)
{
    hitar *ha = NULL;
    ha = (hitar*)calloc(1,sizeof(struct _hit_array));
    if (ha == NULL) return NULL;
    ha->hit = (hit*)calloc(size,sizeof(struct _hit));
    if (ha->hit == NULL) return NULL;
    ha->n_hit = 0;
    ha->m_hit = size;
    ha->i_hit = 0;
    return ha;
}
int destroy_hit_array(hitar *ha)
{
  if (ha == NULL || ha->hit == NULL) return 1;
  if (ha->hit != NULL && ha->n_hit > 0) free(ha->hit);
  ha->hit = NULL;
  free(ha);
  return 0;
}
int removeHitFromArray(hitar *a, int l)
{
   int i;

   if (a == NULL || a->hit == NULL) return 1;
   if (l < 0 || l >= a->n_hit) return 2;
   for (i = l; i < a->n_hit - 1; i++)
           a->hit[i] = a->hit[i+1];
   a->n_hit = (a->n_hit > 0) ? a->n_hit-1 : a->n_hit;
   find_n_seqind(a);
   find_n_oliind(a);
   return 0;
}
int InsertInHitArray(hitar *ha, hit *a, int pos)
{
   int i;
   if (ha == NULL || ha->hit == NULL || a == NULL) return 1;
   if (pos < 0) return 2;
   pos = (pos >= ha->n_hit) ? ha->n_hit : pos;
   if (ha->n_hit+1 >= ha->m_hit)
      {
         i = ha->n_hit+1;
         ha->hit = (hit*)realloc(ha->hit,i*sizeof(struct _hit));
         if (ha->hit == NULL) return 3;
         ha->m_hit = i;
      }
   for (i = ha->n_hit; i > pos; i--)
           ha->hit[i+1] = ha->hit[i];
   ha->n_hit++;
   ha->hit[pos] = *a;
   find_n_seqind(ha);
   find_n_oliind(ha);
   return 0;
}


int InsertInHitArraByVal(hitar *ha, int pos, int oliindnm, int seqindbp, float oliposnm, float seqposbp, int pivot)
{
   int i;
   if (ha == NULL || ha->hit == NULL) return 1;
   if (pos < 0) return 2;
   pos = (pos >= ha->n_hit) ? ha->n_hit : pos;
   if (ha->n_hit+1 >= ha->m_hit)
      {
         i = ha->n_hit+1;
         ha->hit = (hit*)realloc(ha->hit,i*sizeof(struct _hit));
         if (ha->hit == NULL) return 3;
         ha->m_hit = i;
      }
   for (i = ha->n_hit; i > pos; i--)
           ha->hit[i+1] = ha->hit[i];
   ha->n_hit++;
   ha->hit[pos].oliindnm = oliindnm;
   ha->hit[pos].seqindbp = seqindbp;
   ha->hit[pos].oliposnm = oliposnm;
   ha->hit[pos].seqposbp = seqposbp;
   ha->hit[pos].pivot = pivot;
   if (pivot) ha->i_pivot = pos;
   find_n_seqind(ha);
   find_n_oliind(ha);
   return 0;
}


int QuickSortHitArraySeq(hitar *a, int l, int r)
{
    int  i, j;
    hit  x, y;
    i = l;
    j = r;
    x = a->hit[(int)((l + r) / 2)];

    while (i < j)
    {
        while (a->hit[i].seqindbp < x.seqindbp)
        {
            i = i + 1;
        }
        while (x.seqindbp < a->hit[j].seqindbp)
        {
            j = j - 1;
        }
        if (i <= j)
        {
            y = a->hit[i];
            a->hit[i] = a->hit[j];
            a->hit[j] = y;
            i = i + 1;
            j = j - 1;
        }
    }/*while*/
    if (l < j)
    {
        QuickSortHitArraySeq(a, l, j);
    }

    if (i < r)
    {
        QuickSortHitArraySeq(a, i, r);
    }
    return 0;
}

int QuickSortHitArrayOli(hitar *a, int l, int r)
{
    int  i, j;
    hit  x, y;
    i = l;
    j = r;
    x = a->hit[(int)((l + r) / 2)];

    while (i < j)
    {
        while (a->hit[i].oliindnm < x.oliindnm)
        {
            i = i + 1;
        }
        while (x.oliindnm < a->hit[j].oliindnm)
        {
            j = j - 1;
        }
        if (i <= j)
        {
            y = a->hit[i];
            a->hit[i] = a->hit[j];
            a->hit[j] = y;
            i = i + 1;
            j = j - 1;
        }
    }/*while*/
    if (l < j)
    {
        QuickSortHitArrayOli(a, l, j);
    }

    if (i < r)
    {
        QuickSortHitArrayOli(a, i, r);
    }
    return 0;
}
int fill_initial_hit_array(hitar *ha, d_s *dso, d_s *dss, int pol, int psl, int dir, int orx, int onx,  float stretch_minl, float stretch_maxl)
{
     int i, j;
     float kx, ky, x, y, sgn;

     for (i = 0; i < dso->nx; i++)
        {  // we scan the oligo blocking position in nm
            ky = dso->yd[i];
            for (j = 0; j < dss->nx; j++)
                {  // we scan the oligo expected blocking position in bp
                     kx = dss->xd[j];
                     if (kx < orx || kx > orx + onx)      continue;
                     x = kx - dss->xd[psl];
                     if (dir < 0) x = -x;
                     y = ky - dso->yd[pol];
                     sgn = (x >= 0) ? 1 : -1;
                     if ((sgn*x >= sgn*stretch_minl*y) && (sgn*x <= sgn*stretch_maxl*y))
                             InsertInHitArraByVal(ha, ha->n_hit, i, j, ky, kx, (i == pol && j == psl));
                }
        }
     return 0;
}

int draw_boundaries(d_s *dsor, float kx, float ky, int orx, int onx, int ory, int ony, int orxm, int onxm, int dir)
{
    if (dsor == NULL) return 1;
    if (dir < 0)
       {
           add_new_point_to_ds(dsor, orx, ory + ony);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, orx + onx, ory);
           add_new_point_to_ds(dsor, orxm, ory + ony);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, orxm + onxm, ory);
           //add_new_point_to_ds(dsor, kx, ky);
           //add_new_point_to_ds(dsor, orxm + onxm, ory);
       }
    else
       {
           add_new_point_to_ds(dsor, orx, ory);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, orx + onx, ory + ony);
           add_new_point_to_ds(dsor, orxm, ory);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, orxm + onxm, ory + ony);
       }
    return 0;
}
int draw_up_boundaries(d_s *dsor, float kx, float ky, int orx, int onx, int ory, int ony, int orxm, int onxm, int dir)
{
    if (dsor == NULL) return 1;
    if (dir < 0)
       {
               //add_new_point_to_ds(dsor, orx, ory + ony);
               //add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, orx + onx, ory);
           //add_new_point_to_ds(dsor, orxm, ory + ony);
           //add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, orxm + onxm, ory);
           //add_new_point_to_ds(dsor, kx, ky);
           //add_new_point_to_ds(dsor, orxm + onxm, ory);
       }
    else
       {
               //add_new_point_to_ds(dsor, orx, ory);
               //add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, orx + onx, ory + ony);
           //add_new_point_to_ds(dsor, orxm, ory);
           //add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, orxm + onxm, ory + ony);
       }
    return 0;
}


int find_nb_of_unambiguous_hit(hitar *ha)
{
   int i, j;
   for (i = j = 0; i < ha->n_hit; i++)
       j += (ha->hit[i].n_oliind == 1 && ha->hit[i].n_seqind == 1) ? 1 : 0;
   return j;
}

int rm_from_hit_array_ambiguous_hit(hitar *ha, int dir, float stretch_minl, float stretch_maxl)
{
    int i, j, sgn, ok;
    float y, x;
    hit *hi;

    hi = ha->hit;
    for (i = ha->n_hit - 1; i >= 0 ; i--)
         hi[i].non_ambiguous = (hi[i].n_oliind == 1 && hi[i].n_seqind == 1) ? 1 : 0;
    // when we delete hit the ambiguous hits may become non_ambiguous because t
    // heir neigbors have been deleted

    for (i = ha->n_hit - 1; i >= 0 ; i--)
       {
           if (hi[i].non_ambiguous)
               continue; // i -> only ambiguous hits are treated
           for (j = 0, ok = 1; ok != 0 && j < ha->n_hit; j++)
               {
                    if (i == j) continue;
                    if (hi[i].non_ambiguous == 0)
                         continue; // j -> only non ambiguous hits are ued
                    x = hi[j].seqposbp - hi[i].seqposbp;
                    if (dir < 0) x = -x;
                    y = hi[j].oliposnm - hi[i].oliposnm;
                    sgn = (x >= 0) ? 1 : -1;
                    x *= sgn;
                    y *= sgn;
                    ok = (x >= (stretch_minl*y) && x <= (stretch_maxl*y)) ? ok : 0;
               }
           if (ok == 0) removeHitFromArray(ha, i);
       }

   return find_nb_of_unambiguous_hit(ha);
}

/**
 * @brief
 *
 * @param op Plot in which are putted data
 * @param idsol index of needle dataset
 * @param idssl index of the haystack dataset
 * @param idshisto index of the histogram of blocking position (-1 none)
 * @param pol index of needle reference peak
 * @param psl index of haystack reference peak
 * @param stretch_minl minimum stretching
 * @param stretch_maxl maximun stretching
 * @param dir direction of the solution (positive or negative)
 *
 * @return
 */
int generate_Plot_Oligo_Seq(O_p *op, int idsol, int idssl, int idshistol, int pol, int psl, float stretch_minl, float stretch_maxl,
                            int dir, float *linear_fit_coef, const char *label)
{
    int i = 0, j = 0;
    d_s *dso = NULL;   // needle dataset
    d_s *dss = NULL;   // haystack dataset
    d_s *dsh = NULL;   // histo dataset
    d_s *dsbp = NULL;  // Oligo_Seq_bp_lines;
    d_s *dsbpi = NULL; // Oligo_Seq_bp_lines not matched
    d_s *dsnm = NULL;  // Oligo_Seq_nm_lines;
    d_s *dsnmi = NULL; // Oligo_Seq_nm_lines not matched
    d_s *dshit = NULL; // Oligo_Seq_hits;
    d_s *dshit2 = NULL;// Oligo_Seq_hits filter 
    d_s *dsor = NULL;  // Oligo_Seq_origin bounding lines defining acceptable streching
    d_s *dsfit = NULL; // Oligo_Seq_fit
    int onx = 0;
    int ony = 0;
    int ory = 0;
    int orx = 0;
    int omy = 0;
    int orxm = 0;
    int onxm = 0;
    static hitar* ha = NULL;

    if (idsol < 0 || idsol >= op->n_dat || idssl < 0 || idssl >= op->n_dat)
        return 1;

    (void)linear_fit_coef;
    dso = op->dat[idsol];
    dss = op->dat[idssl];
    pol = (pol < dso->nx) ? pol : dso->nx - 1;
    pol = (pol < 0) ? 0 : pol;
    psl = (psl < dss->nx) ? psl : dss->nx - 1;
    psl = (psl < 0) ? 0 : psl;
    ony = round((dso->yd[dso->nx - 1] - dso->yd[0]) * 1.2);



    onx = round((dso->yd[dso->nx - 1] - dso->yd[0]) * stretch_maxl * 1.2);
    onxm = round((dso->yd[dso->nx - 1] - dso->yd[0]) * stretch_minl * 1.2);

    if (dir < 0)
    {
        ory = round(dso->yd[0] - (dso->yd[dso->nx - 1] - dso->yd[0]) * 0.1);
        omy = dso->yd[pol] - ory;
        orx = dss->xd[psl] - (ony - omy) * stretch_maxl;
        orxm = dss->xd[psl] - (ony - omy) * stretch_minl;
    }
    else
    {
        ory = round(dso->yd[0] - (dso->yd[dso->nx - 1] - dso->yd[0]) * 0.1);
        omy = dso->yd[pol] - ory;
        orx = dss->xd[psl] - omy * stretch_maxl;
        orxm = dss->xd[psl] - omy * stretch_minl;
    }


    //win_printf("ory %d ony %d\norx %d onx %d\n",ory,ony,orx,onx);


    dsbp = find_source_specific_ds_in_op(op, "Oligo_Seq_bp_lines");

    if (dsbp == NULL)
    {
        if ((dsbp = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_ds_source(dsbp, "Oligo_Seq_bp_lines");
        set_dash_line(dsbp);
        set_ds_line_color(dsbp, Green);
        fine_grid = 0;
    }

    dsbp->nx = dsbp->ny = 0;

    dsbpi = find_source_specific_ds_in_op(op, "Oligo_Seq_unmatched_bp_lines");

    if (dsbpi == NULL)
    {
        if ((dsbpi = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_ds_source(dsbpi, "Oligo_Seq_unmatched_bp_lines");
        set_dash_line(dsbpi);
        set_ds_line_color(dsbpi, Red);
        fine_grid = 0;
    }

    dsbpi->nx = dsbpi->ny = 0;

    dsnm = find_source_specific_ds_in_op(op, "Oligo_Seq_nm_lines");

    if (dsnm == NULL)
    {
        if ((dsnm = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_ds_source(dsnm, "Oligo_Seq_nm_lines");
        set_ds_line_color(dsnm, Green);
        set_dash_line(dsnm);
    }

    dsnm->nx = dsnm->ny = 0;

    dsnmi = find_source_specific_ds_in_op(op, "Oligo_Seq_unmatched_nm_lines");

    if (dsnmi == NULL)
    {
        if ((dsnmi = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_ds_source(dsnmi, "Oligo_Seq_unmatched_nm_lines");
        set_ds_line_color(dsnmi, Red);
        set_dash_line(dsnmi);
    }

    dsnmi->nx = dsnmi->ny = 0;


    dshit = find_source_specific_ds_in_op(op, "Oligo_Seq_hits");

    if (dshit == NULL)
    {
        if ((dshit = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_plot_symb(dshit, "\\pt5\\di");
        set_ds_line_color(dshit, Yellow);
        set_ds_source(dshit, "Oligo_Seq_hits");
        set_dot_line(dshit);
    }
    dshit->nx = dshit->ny = 0;


    dshit2 = find_source_specific_ds_in_op(op, "Oligo_Seq_filter_hits");

    if (dshit2 == NULL)
    {
        if ((dshit2 = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_plot_symb(dshit2, "\\pt5\\di");
        set_ds_line_color(dshit2, Lightgray);
        set_ds_source(dshit2, "Oligo_Seq_filter_hits");
        set_dot_line(dshit2);
    }
    dshit2->nx = dshit2->ny = 0;

    d_s *dshit3 = find_source_specific_ds_in_op(op, "Oligo_Seq_filter_3_hits");

    if (dshit3 == NULL)
    {
        if ((dshit3 = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_plot_symb(dshit3, "\\pt5\\di");
        set_ds_line_color(dshit3, Lightmagenta);
        set_ds_source(dshit3, "Oligo_Seq_filter_3_hits");
        set_plain_line(dshit3);
    }
    dshit3->nx = dshit3->ny = 0;

    d_s *dshit4 = find_source_specific_ds_in_op(op, "Oligo_Seq_filter_4_hits");

    if (dshit4 == NULL)
    {
        if ((dshit4 = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_plot_symb(dshit4, "\\pt5\\di");
        set_ds_line_color(dshit4, Lightred);
        set_ds_source(dshit4, "Oligo_Seq_filter_4_hits");
        set_plain_line(dshit4);
    }
    dshit4->nx = dshit4->ny = 0;


    dsor = find_source_specific_ds_in_op(op, "Oligo_Seq_origin");

    if (dsor == NULL)
    {
        if ((dsor = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_ds_source(dsor, "Oligo_Seq_origin");
        set_dash_line(dsor);
        set_ds_line_color(dsor, Lightblue);
        //set_plot_symb(dsor, "\\pt5\\oc");
    }

    dsor->nx = dsor->ny = 0;

    dsfit = find_source_specific_ds_in_op(op, "Oligo_Seq_fit");

    if (dsfit == NULL)
    {
        if ((dsfit = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsfit !");

        set_ds_source(dsfit, "Oligo_Seq_fit");
        set_dash_line(dsfit);
        //set_plot_symb(dsor, "\\pt5\\oc");
    }

    dsfit->nx = dsfit->ny = 0;

    remove_all_plot_label_from_ds(dsfit);

    if (label)
    {
      set_ds_plot_label(dsfit, op->x_hi, op->y_lo + (op->y_hi - op->y_lo) / 2, USR_COORD, "%s",label);
    }

    int kx, ky;
    op->x_lo = orx;
    op->x_hi = orx + onx;
    op->y_lo = ory;
    op->y_hi = ory + ony;

    if (idshistol >= 0)
       {   // we adjust the histogram to field of view
           dsh = op->dat[idshistol];
           for (i = 0; i < dsh->nx; i++)
               dsh->xd[i] += orx + 50 - dshisto_offest;
           dshisto_offest = orx + 50;
       }

    if (ha == NULL) ha = create_hit_array(128);
    if (ha == NULL) return win_printf_OK("I can't create hit array !");

    ha->n_hit = 0;
    if (ha->n_hit == 0)
       fill_initial_hit_array(ha, dso, dss, pol, psl, dir, orx, onx, stretch_minl, stretch_maxl);

    kx = dss->xd[psl]; // we draw the two boundaries 
    ky = dso->yd[pol];
    draw_boundaries(dsor, kx, ky, orx, onx, ory, ony, orxm, onxm, dir);

    find_n_seqind(ha);
    find_n_oliind(ha);
    QuickSortHitArraySeq(ha, 0, ha->n_hit-1);

    for (j = 0; j < ha->n_hit; j++)
        add_new_point_to_ds(dshit, ha->hit[j].seqposbp, ha->hit[j].oliposnm);





    for (j = find_nb_of_unambiguous_hit(ha), i = j-1; j > i; )
        {
            i = j;
            j = rm_from_hit_array_ambiguous_hit(ha, dir, stretch_minl, stretch_maxl);
        }

    for (j = 0; j < ha->n_hit; j++)
        add_new_point_to_ds(dshit2, ha->hit[j].seqposbp, ha->hit[j].oliposnm);
    int omyl, orxl, orxml, k;
    for (j = k = 0; j < ha->n_hit; j++)
       {
           if (ha->hit[j].n_oliind == 1 && ha->hit[j].n_seqind == 1)
              {
                 add_new_point_to_ds(dshit3, ha->hit[j].seqposbp, ha->hit[j].oliposnm);
                 kx = ha->hit[j].seqposbp; // we draw the two boundaries 
                 ky = ha->hit[j].oliposnm;

                 if (dir < 0)
                   {
                     omyl = ky - ory;
                     orxl = kx - (ony - omyl) * stretch_maxl;
                     orxml = kx - (ony - omyl) * stretch_minl;
                   }
                 else
                   {
                    omyl = ky - ory;
                    orxl = kx - omyl * stretch_maxl;
                    orxml = kx - omyl * stretch_minl;
                   }
                 if (ha->hit[j].oliindnm > pol)
                     draw_up_boundaries(dsor, kx, ky, orxl, onx, ory, ony, orxml, onxm, dir);
              }

       }

    double out_a,  out_b;
    d_s* ds = least_square_fit_on_ds(dshit2, 1, &out_a, &out_b);
    if (ds == NULL) return win_printf_OK("I cannot least quare fit !");
    duplicate_data_set(ds, dsfit);
    set_ds_source(dsfit, "Oligo_Seq_fit");
    set_ds_line_color(dsfit, White);
    free_data_set(ds);
    float best = 0, tmp = 0;
    int ibest = 0;
    for (j = 0; j < ha->n_hit; j++)
       {
           if (ha->hit[j].n_oliind == 1 && ha->hit[j].n_seqind == 1)
                add_new_point_to_ds(dshit4, ha->hit[j].seqposbp, ha->hit[j].oliposnm);
           else if (ha->hit[j].n_oliind == 1)
             {
                for (i = j; i < j + ha->hit[j].n_seqind; i++)
                   {
                        tmp = fabs(ha->hit[i].oliposnm - out_b + out_a * ha->hit[i].seqposbp);
                        if ((i == j) || (tmp < best))
                            {
                                best = tmp;
                                ibest = i;
                            }
                   }
                 add_new_point_to_ds(dshit4, ha->hit[ibest].seqposbp, ha->hit[ibest].oliposnm);
                 j += ha->hit[j].n_seqind - 1;
             }
           else if (ha->hit[j].n_seqind == 1)
             {
                for (i = j; i < j + ha->hit[j].n_oliind; i++)
                   {
                        tmp = fabs(ha->hit[i].oliposnm - out_b + out_a * ha->hit[i].seqposbp);
                        if ((i == j) || (tmp < best))
                            {
                                best = tmp;
                                ibest = i;
                            }
                   }
                 add_new_point_to_ds(dshit4, ha->hit[ibest].seqposbp, ha->hit[ibest].oliposnm);
                 j += ha->hit[j].n_oliind - 1;
             }

       }



    for (i = 0; i < dso->nx; i++)
        {  // we scan the oligo blocking position in bp
            for (j = 0; j < ha->n_hit; j++)
                if (ha->hit[j].oliindnm == i) break;
            ky = dso->yd[i];
            if (ha->hit[j].oliindnm == i )
               {
                   add_new_point_to_ds(dsbp, orx, ky);
                   add_new_point_to_ds(dsbp, orx + onx, ky);
               }
            else
               {
                   add_new_point_to_ds(dsbpi, orx, ky);
                   add_new_point_to_ds(dsbpi, orx + onx, ky);
               }
        }

    for (j = 0; j < dss->nx; j++)
        {  // we scan the oligo expected blocking position in bp
            kx = dss->xd[j];
            if (kx < orx || kx > orx + onx)      continue;
            for (i = 0; i < ha->n_hit; i++)
                if (ha->hit[i].seqindbp == j) break;
            if (ha->hit[i].seqindbp == j)
               {
                   add_new_point_to_ds(dsnm, kx, ory);
                   add_new_point_to_ds(dsnm, kx, ory + ony);
               }
            else
               {
                   add_new_point_to_ds(dsnmi, kx, ory);
                   add_new_point_to_ds(dsnmi, kx, ory + ony);
               }
        }
    op->need_to_refresh = 1;
    return 0;
}



/**
 * @brief
 *
 * @param op Plot in which are putted data
 * @param idsol index of needle dataset
 * @param idssl index of the haystack dataset
 * @param idshisto index of the histogram of blocking position (-1 none)
 * @param pol index of needle reference peak
 * @param psl index of haystack reference peak
 * @param stretch_minl minimum stretching
 * @param stretch_maxl maximun stretching
 * @param dir direction of the solution (positive or negative)
 *
 * @return
 */
int generate_Plot_Oligo_Seq1(O_p *op, int idsol, int idssl, int idshistol, int pol, int psl, float stretch_minl, float stretch_maxl,
                            int dir, float *linear_fit_coef, const char *label)
{
    int i = 0, j = 0;
    d_s *dso = NULL;   // needle dataset
    d_s *dss = NULL;   // haystack dataset
    d_s *dsh = NULL;   // histo dataset
    d_s *dsbp = NULL;  // Oligo_Seq_bp_lines;
    d_s *dsbpi = NULL; // Oligo_Seq_bp_lines not matched
    d_s *dsnm = NULL;  // Oligo_Seq_nm_lines;
    d_s *dsnmi = NULL; // Oligo_Seq_nm_lines not matched
    d_s *dshit = NULL; // Oligo_Seq_hits;
    d_s *dshit2 = NULL;// Oligo_Seq_hits filter 
    d_s *dsor = NULL;  // Oligo_Seq_origin bounding lines defining acceptable streching
    d_s *dsfit = NULL; // Oligo_Seq_fit
    float x = .0, y = 0;
    int onx = 0;
    int ony = 0;
    int ory = 0;
    int orx = 0;
    int omy = 0;
    int orxm = 0;
    int onxm = 0;
    static int *dssmatch = NULL, *dsomatch = NULL;
    static int n_dssmatch = 0, n_dsomatch = 0;

    if (idsol < 0 || idsol >= op->n_dat || idssl < 0 || idssl >= op->n_dat)
        return 1;
    
    (void)linear_fit_coef;
    dso = op->dat[idsol];
    dss = op->dat[idssl];
    pol = (pol < dso->nx) ? pol : dso->nx - 1;
    pol = (pol < 0) ? 0 : pol;
    psl = (psl < dss->nx) ? psl : dss->nx - 1;
    psl = (psl < 0) ? 0 : psl;
    ony = round((dso->yd[dso->nx - 1] - dso->yd[0]) * 1.2);


    if (n_dssmatch < dss->nx)
      {
         dssmatch = (int*)realloc(dssmatch,dss->nx * sizeof(int));
         if (dssmatch == NULL) return 1;
         n_dssmatch = dss->nx;
      }
    for (i = 0; i < n_dssmatch; i++) dssmatch[i] = 0;


    if (n_dsomatch < dso->nx)
      {
         dsomatch = (int*)realloc(dsomatch,dso->nx * sizeof(int));
         if (dsomatch == NULL) return 1;
         n_dsomatch = dso->nx;
      }
    for (i = 0; i < n_dsomatch; i++) dsomatch[i] = 0;


    onx = round((dso->yd[dso->nx - 1] - dso->yd[0]) * stretch_maxl * 1.2);
    onxm = round((dso->yd[dso->nx - 1] - dso->yd[0]) * stretch_minl * 1.2);

    if (dir < 0)
    {
        ory = round(dso->yd[0] - (dso->yd[dso->nx - 1] - dso->yd[0]) * 0.1);
        omy = dso->yd[pol] - ory;
        orx = dss->xd[psl] - (ony - omy) * stretch_maxl;
        orxm = dss->xd[psl] - (ony - omy) * stretch_minl;
    }
    else
    {
        ory = round(dso->yd[0] - (dso->yd[dso->nx - 1] - dso->yd[0]) * 0.1);
        omy = dso->yd[pol] - ory;
        orx = dss->xd[psl] - omy * stretch_maxl;
        orxm = dss->xd[psl] - omy * stretch_minl;
    }


    //win_printf("ory %d ony %d\norx %d onx %d\n",ory,ony,orx,onx);


    dsbp = find_source_specific_ds_in_op(op, "Oligo_Seq_bp_lines");

    if (dsbp == NULL)
    {
        if ((dsbp = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_ds_source(dsbp, "Oligo_Seq_bp_lines");
        set_dash_line(dsbp);
        set_ds_line_color(dsbp, Green);
        fine_grid = 0;
    }

    dsbpi = find_source_specific_ds_in_op(op, "Oligo_Seq_unmatched_bp_lines");

    if (dsbpi == NULL)
    {
        if ((dsbpi = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_ds_source(dsbpi, "Oligo_Seq_unmatched_bp_lines");
        set_dash_line(dsbpi);
        set_ds_line_color(dsbpi, Red);
        fine_grid = 0;
    }

    dsbpi->nx = dsbpi->ny = 0;

    dsnm = find_source_specific_ds_in_op(op, "Oligo_Seq_nm_lines");

    if (dsnm == NULL)
    {
        if ((dsnm = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_ds_source(dsnm, "Oligo_Seq_nm_lines");
        set_ds_line_color(dsnm, Green);
        set_dash_line(dsnm);
    }

    dsnm->nx = dsnm->ny = 0;

    dsnmi = find_source_specific_ds_in_op(op, "Oligo_Seq_unmatched_nm_lines");

    if (dsnmi == NULL)
    {
        if ((dsnmi = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_ds_source(dsnmi, "Oligo_Seq_unmatched_nm_lines");
        set_ds_line_color(dsnmi, Red);
        set_dash_line(dsnmi);
    }

    dsnmi->nx = dsnmi->ny = 0;


    dshit = find_source_specific_ds_in_op(op, "Oligo_Seq_hits");

    if (dshit == NULL)
    {
        if ((dshit = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_plot_symb(dshit, "\\pt5\\di");
        set_ds_line_color(dshit, Yellow);
        set_ds_source(dshit, "Oligo_Seq_hits");
    }
    set_dot_line(dshit);
    dshit->nx = dshit->ny = 0;


    dshit2 = find_source_specific_ds_in_op(op, "Oligo_Seq_filter_hits");

    if (dshit2 == NULL)
    {
        if ((dshit2 = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_plot_symb(dshit2, "\\pt5\\di");
        set_ds_line_color(dshit2, Lightmagenta);
        set_ds_source(dshit2, "Oligo_Seq_filter_hits");
    }
    set_dot_line(dshit2);
    dshit2->nx = dshit2->ny = 0;

    d_s* dshit3 = find_source_specific_ds_in_op(op, "Oligo_Seq_filter_3_hits");

    if (dshit3 == NULL)
    {
        if ((dshit3 = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_plot_symb(dshit3, "\\pt5\\di");
        set_ds_line_color(dshit3, Lightred);
        set_ds_source(dshit3, "Oligo_Seq_filter_3_hits");
    }
    set_dot_line(dshit3);
    dshit3->nx = dshit3->ny = 0;


    dsor = find_source_specific_ds_in_op(op, "Oligo_Seq_origin");

    if (dsor == NULL)
    {
        if ((dsor = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsbp !");

        set_ds_source(dsor, "Oligo_Seq_origin");
        set_dash_line(dsor);
        set_ds_line_color(dsor, Lightblue);
        //set_plot_symb(dsor, "\\pt5\\oc");
    }

    dsor->nx = dsor->ny = 0;

    dsfit = find_source_specific_ds_in_op(op, "Oligo_Seq_fit");

    if (dsfit == NULL)
    {
        if ((dsfit = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
            return win_printf_OK("I can't create dsfit !");

        set_ds_source(dsfit, "Oligo_Seq_fit");
        set_dash_line(dsfit);
        //set_plot_symb(dsor, "\\pt5\\oc");
    }

    dsfit->nx = dsfit->ny = 0;

    remove_all_plot_label_from_ds(dsfit);

    if (label)
    {
        set_ds_plot_label(dsfit, op->x_hi, op->y_lo + (op->y_hi - op->y_lo) / 2, USR_COORD, "%s", label);
    }

    int kx, ky, match;
    op->x_lo = orx;
    op->x_hi = orx + onx;
    op->y_lo = ory;
    op->y_hi = ory + ony;

    if (idshistol >= 0)
       {   // we adjust the histogram to field of view
           dsh = op->dat[idshistol];
           for (i = 0; i < dsh->nx; i++)
               dsh->xd[i] += orx + 50 - dshisto_offest;
           dshisto_offest = orx + 50;
       }


    for (i = 0; i < dso->nx; i++)
      {  // we scan the oligo blocking position in nm
        ky = dso->yd[i];
        for (j = 0, match = 0; j < dss->nx; j++)
           {  // we scan the oligo expected blocking position in bp
              kx = dss->xd[j];
              if (kx < orx || kx > orx + onx)
                {
                   dssmatch[j] = -1;
                   continue;
                }
              x = dss->xd[j] - dss->xd[psl];
              if (dir < 0) x = -x;
              y = dso->yd[i] - dso->yd[pol];
              if (x == 0 && y == 0)
                add_new_point_to_ds(dshit, kx, ky);
              else if (y != 0)
                {
                   x /= y;
                   if (x >= stretch_minl && x <= stretch_maxl)
                     {
                       add_new_point_to_ds(dshit, kx, ky);
                       dssmatch[j]++;
                       match++;
                     }
                }
           }
        if (match) dsomatch[i]++;
      }

    kx = dss->xd[psl]; // we draw the two boundaries 
    ky = dso->yd[pol];
    if (dir < 0)
       {
           add_new_point_to_ds(dsor, orx, ory + ony);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, orx + onx, ory);
           add_new_point_to_ds(dsor, orxm, ory + ony);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, orxm + onxm, ory);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, orxm + onxm, ory);
       }
    else
       {
           add_new_point_to_ds(dsor, orx, ory);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, orx + onx, ory + ony);
           add_new_point_to_ds(dsor, orxm, ory);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, kx, ky);
           add_new_point_to_ds(dsor, orxm + onxm, ory + ony);
       }

    int mx, my, n_s0, n_s, sgn;
    for (i = 0, n_s = 0; i < dshit->nx; i++)
       {   // we look for non-ambiguous hits
          for (j = 0, mx = my = 0; j < dshit->nx; j++)
              {
                   if (j == i) continue;
                   mx += (dshit->xd[i] == dshit->xd[j]) ? 1 : 0;
                   my += (dshit->yd[i] == dshit->yd[j]) ? 1 : 0;
              }
          if (mx == 0 && my == 0) add_new_point_to_ds(dshit2, dshit->xd[i], dshit->yd[i]);
       }
    n_s = dshit2->nx; // Nb. of non-ambiguous hits
    for (i = n_s; i < dshit->nx; i++)
       {
          for (j = 0, mx = 0; j < n_s; j++)
              {
                   x = dshit->xd[i] - dshit2->xd[j];
                   if (dir < 0) x = -x;
                   y = dshit->yd[i] - dshit2->yd[j];
                   sgn = (x >= 0) ? 1 : -1;
                   x *= sgn;
                   y *= sgn;
                   mx += (x >= (stretch_minl*y) && x <= (stretch_maxl*y)) ? 1 : 0;
              }   // we keep hits compatible with all non-ambiguous hits 
          if (mx >= n_s) add_new_point_to_ds(dshit2, dshit->xd[i], dshit->yd[i]);
       }
    int iter;
    d_s *dsn, *dsn1;
    dsn = dshit2;
    dsn1 = dshit3;
    for (iter = 1, n_s0 = n_s; iter < dshit->nx; iter++)
      {
          dsn1->nx = dsn1->ny = 0;
          for (i = 0, n_s = 0; i < dsn->nx; i++)
             {   // we look for non-ambiguous hits
                for (j = 0, mx = my = 0; j < dsn->nx; j++)
                   {
                       if (j == i) continue;
                       mx += (dsn->xd[i] == dsn->xd[j]) ? 1 : 0;
                       my += (dsn->yd[i] == dsn->yd[j]) ? 1 : 0;
                   }
                if (mx == 0 && my == 0) add_new_point_to_ds(dsn1, dsn->xd[i], dsn->yd[i]);
             }
          n_s = dsn1->nx; // Nb. of non-ambiguous hits
          for (i = n_s; i < dsn->nx; i++)
            {
               for (j = 0, mx = 0; j < n_s; j++)
                  {
                     x = dsn->xd[i] - dsn1->xd[j];
                     if (dir < 0) x = -x;
                     y = dsn->yd[i] - dsn1->yd[j];
                     sgn = (x >= 0) ? 1 : -1;
                     x *= sgn;
                     y *= sgn;
                     mx += (x >= (stretch_minl*y) && x <= (stretch_maxl*y)) ? 1 : 0;
                  }   // we keep hits compatible with all non-ambiguous hits 
               if (mx >= n_s) add_new_point_to_ds(dsn1, dsn->xd[i], dsn->yd[i]);
            }
          //set_ds_source(dsn1, "Oligo_Seq_filter_%d_hits %d non-ambiguous hits over %d",iter,n_s,dsn1->nx);
          if (n_s > n_s0)
             {
                 d_s *dstmp = dsn;
                 dsn = dsn1;
                 dsn1 = dstmp;
                 n_s0 = n_s;
             }
          else iter = dshit->nx;
      }
    set_plain_line(dsn1);
    QuickSort_double(dsn->xd, dsn->yd, 0, dsn->nx - 1);
    QuickSort_double(dsn1->xd, dsn1->yd, 0, dsn1->nx - 1);

    for (i = 0; i < n_dsomatch; i++) dsomatch[i] = 0;
    for (i = 0; i < n_dssmatch; i++) dssmatch[i] = 0;
    for (j = 0, match = 0; j < dss->nx; j++)
       {  // we scan the oligo expected blocking position in bp
           kx = dss->xd[j];
           if (kx < orx || kx > orx + onx)
              {
                  dssmatch[j] = -1;
                  continue;
              }
           for (i = 0; i < dsn1->nx; i++)
               dssmatch[j] += (kx == dsn1->xd[i]) ? 1 : 0;
       }

    for (j = 0; j < dso->nx; j++)
      {  // we scan the oligo blocking position in nm
        ky = dso->yd[j];
           for (i = 0; i < dsn1->nx; i++)
               dsomatch[j] += (ky == dsn1->yd[i]) ? 1 : 0;
      }


# ifdef OLD

    for (i = 0; i < dso->nx; i++)
      {  // we scan the oligo blocking position in nm
        ky = dso->yd[i];

        for (j = 0, match = 0; j < dss->nx; j++)
        {  // we scan the oligo expected blocking position in bp
            kx = dss->xd[j];// - orx;
            if (kx < orx || kx > orx + onx)
               {
                  dssmatch[j] = -1;
                  continue;
               }
            x = dss->xd[j] - dss->xd[psl];

            if (dir < 0) x = -x;

            y = dso->yd[i] - dso->yd[pol];
            if (x == 0 && y == 0)
              {   // we are on the pivotal point
                add_new_point_to_ds(dshit, kx, ky);
                if (dir < 0)
                {
                    add_new_point_to_ds(dsor, orx, ory + ony);
                    add_new_point_to_ds(dsor, kx, ky);
                    add_new_point_to_ds(dsor, kx, ky);

                    add_new_point_to_ds(dsor, orx + onx, ory);
                    add_new_point_to_ds(dsor, orxm, ory + ony);
                    add_new_point_to_ds(dsor, kx, ky);

                    add_new_point_to_ds(dsor, kx, ky);
                    add_new_point_to_ds(dsor, orxm + onxm, ory);

                    add_new_point_to_ds(dsor, kx, ky);
                    add_new_point_to_ds(dsor, orxm + onxm, ory);

                    if (linear_fit_coef)
                    {
                        add_new_point_to_ds(dsfit, kx, ky);
                        add_new_point_to_ds(dsfit, ory, ony * *linear_fit_coef);
                    }
                }
                else
                {
                    add_new_point_to_ds(dsor, orx, ory);
                    add_new_point_to_ds(dsor, kx, ky);
                    add_new_point_to_ds(dsor, kx, ky);

                    add_new_point_to_ds(dsor, orx + onx, ory + ony);
                    add_new_point_to_ds(dsor, orxm, ory);
                    add_new_point_to_ds(dsor, kx, ky);

                    add_new_point_to_ds(dsor, kx, ky);
                    add_new_point_to_ds(dsor, orxm + onxm, ory + ony);

                    if (linear_fit_coef)
                    {
                        add_new_point_to_ds(dsfit, kx, ky);
                        add_new_point_to_ds(dsfit, ory, ony * *linear_fit_coef);
                    }
                }
            }
            else if (y != 0)
            {
                x /= y;

                if (x >= stretch_minl && x <= stretch_maxl)
                {
                    add_new_point_to_ds(dshit, kx, ky);
                    dssmatch[j]++;
                    match++;
                }
            }
        }
      if (match > 0)
        {
             add_new_point_to_ds(dsbp, orx, ky);
             add_new_point_to_ds(dsbp, orx + onx, ky);

        }
      else
        {
             add_new_point_to_ds(dsbpi, orx, ky);
             add_new_point_to_ds(dsbpi, orx + onx, ky);
        }
    }
# endif

    for (i = 0; i < dso->nx; i++)
        {  // we scan the oligo blocking position in bp
            ky = dso->yd[i];
            if (dsomatch[i] < 0) continue;
            else if (dsomatch[i] > 0)
               {
                   add_new_point_to_ds(dsbp, orx, ky);
                   add_new_point_to_ds(dsbp, orx + onx, ky);
               }
            else
               {
                   add_new_point_to_ds(dsbpi, orx, ky);
                   add_new_point_to_ds(dsbpi, orx + onx, ky);
               }
        }

    for (j = 0; j < dss->nx; j++)
        {  // we scan the oligo expected blocking position in bp
            kx = dss->xd[j];
            if (dssmatch[j] < 0) continue;
            else if (dssmatch[j] > 0)
               {
                   add_new_point_to_ds(dsnm, kx, ory);
                   add_new_point_to_ds(dsnm, kx, ory + ony);
               }
            else
               {
                   add_new_point_to_ds(dsnmi, kx, ory);
                   add_new_point_to_ds(dsnmi, kx, ory + ony);
               }
        }

    printf("%d, %d, %d, %d\n", orx, onx, ory, ony);
    op->need_to_refresh = 1;
    return 0;
}

int do_generate_Plot_Oligo_Seq(void)
{
    int i;
    O_p *op = NULL;
    pltreg *pr = NULL;

    if (updating_menu_state != 0)        return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number and place it in a new plot");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
        return win_printf_OK("cannot find data");

    i = win_scanf("Index ds nm %4d Iindex ds bp %4d [Index of histo ds (-1 none) %4d\n"
                  "First pivotal peak nm %4d I peak seq %6d\n"
                  "Direction >0 %R or <0%r\n"
                  "Strech min %6f stretch max %6f\n"
                  , &idso, &idss, &idshisto, &po, &ps, &dirr, &stretch_min, &stretch_max);

    if (i == WIN_CANCEL) return D_O_K;

    op->user_id = 223;
    generate_Plot_Oligo_Seq(op, idso, idss, idshisto, po, ps, (float)1 / stretch_max, (float)1 / stretch_min, (dirr) ? -1 : 1, NULL,
                            NULL);
    return refresh_plot(pr, UNCHANGED);
}


int do_move_fwd(void)
{
    O_p *op = NULL;
    pltreg *pr = NULL;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
        return win_printf_OK("cannot find data");

    if (updating_menu_state != 0)
    {
        if (op != NULL && op->user_id == 223)
        {
            remove_short_cut_from_dialog(0, KEY_PGUP);
            add_keyboard_short_cut(0, KEY_PGUP, 0, do_move_fwd);
        }

        return D_O_K;
    }

    dirr = 0;
    generate_Plot_Oligo_Seq(op, idso, idss, idshisto, po, ps, (float)1 / stretch_max, (float)1 / stretch_min, (dirr) ? -1 : 1, NULL,
                            NULL);
    return refresh_plot(pr, UNCHANGED);
}

int do_move_bwd(void)
{
    O_p *op = NULL;
    pltreg *pr = NULL;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
        return win_printf_OK("cannot find data");

    if (updating_menu_state != 0)
    {
        if (op != NULL && op->user_id == 223)
        {
            remove_short_cut_from_dialog(0, KEY_PGDN);
            add_keyboard_short_cut(0, KEY_PGDN, 0, do_move_bwd);
        }

        return D_O_K;
    }

    dirr = 1;
    generate_Plot_Oligo_Seq(op, idso, idss, idshisto, po, ps, (float)1 / stretch_max, (float)1 / stretch_min, (dirr) ? -1 : 1, NULL,
                            NULL);
    return refresh_plot(pr, UNCHANGED);
}

int do_move_psr(void)
{
    O_p *op = NULL;
    pltreg *pr = NULL;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
        return win_printf_OK("cannot find data");

    if (updating_menu_state != 0)
    {
        if (op != NULL && op->user_id == 223)
        {
            remove_short_cut_from_dialog(0, KEY_RIGHT);
            add_keyboard_short_cut(0, KEY_RIGHT, 0, do_move_psr);
            remove_short_cut_from_dialog(0, KEY_PGUP);
            add_keyboard_short_cut(0, KEY_PGUP, 0, do_move_fwd);
            remove_short_cut_from_dialog(0, KEY_PGDN);
            add_keyboard_short_cut(0, KEY_PGDN, 0, do_move_bwd);
        }

        return D_O_K;
    }


    /* we first find the data that we need to transform */
    po++;

    if (po >= op->dat[idso]->nx)
    {
        po = 0;
        ps++;
    }

    ps = (ps >= op->dat[idss]->nx) ? 0 : ps;
    generate_Plot_Oligo_Seq(op, idso, idss, idshisto, po, ps, (float)1 / stretch_max, (float)1 / stretch_min, (dirr) ? -1 : 1, NULL,
                            NULL);
    return refresh_plot(pr, UNCHANGED);
}

int do_move_psl(void)
{
    O_p *op = NULL;
    pltreg *pr = NULL;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
        return win_printf_OK("cannot find data");

    if (updating_menu_state != 0)
    {
        if (op != NULL && op->user_id == 223)
        {
            remove_short_cut_from_dialog(0, KEY_LEFT);
            add_keyboard_short_cut(0, KEY_LEFT, 0, do_move_psl);
            remove_short_cut_from_dialog(0, KEY_PGUP);
            add_keyboard_short_cut(0, KEY_PGUP, 0, do_move_fwd);
            remove_short_cut_from_dialog(0, KEY_PGDN);
            add_keyboard_short_cut(0, KEY_PGDN, 0, do_move_bwd);
        }

        return D_O_K;
    }


    /* we first find the data that we need to transform */
    po--;

    if (po < 0)
    {
        po = op->dat[idso]->nx - 1;
        ps--;
    }

    ps = (ps < 0) ? op->dat[idss]->nx - 1 : ps;
    generate_Plot_Oligo_Seq(op, idso, idss, idshisto, po, ps, (float)1 / stretch_max, (float)1 / stretch_min, (dirr) ? -1 : 1, NULL,
                            NULL);
    return refresh_plot(pr, UNCHANGED);
}

# ifdef OLD
int do_generate_Plot_with_matching_pts_in_2_ds(void)
{
    int i, j, k, l, nx, imin, jmin;
    O_p *op = NULL, *opn = NULL;
    pltreg *pr = NULL;
    d_s *ds1 = NULL, *ds2 = NULL, *dsn = NULL, *dsl = NULL, *dsb = NULL;
    static int ids1 = 0, ids2 = 1, in_y = 0, cross = 1, bound = 1;     static float stretch = 0.95, dstretch = 0.05, dz = 0.005;
    int n_m1, *match1 = NULL, *match2 = NULL;
    float min, tmp1, tmp2, tmp, **min2D = NULL, min1, max1, min2, max2, dzl;
    //extern int QuickSort_double(float * xd, float * yd, int l, int r);

    if (updating_menu_state != 0)        return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number and place it in a new plot");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
        return win_printf_OK("cannot find data");

    i = win_scanf("Find peaks compatible between 2 ds within an affine transform\n"
                  "First ds index %4d, second ds index %4d\n"
                  "Strech \\lambda %6f d\\lambda max %6f accuracy %6f\n"
                  "in X%R or in Y%r\n"
                  "Diplay Crossing lines %R->No, %r->partial, %r->full\n"
                  "%bDisplay boundary\n"
                  , &ids1, &ids2,  &stretch, &dstretch, &dz, &in_y,&cross, &bound);

    if (i == WIN_CANCEL) return D_O_K;

    if (ids1 < 0 || ids1 >= op->n_dat)
            return win_printf_OK("Index %d out of range [0,%d[",ids1,op->n_dat);
    ds1 = op->dat[ids1];
    nx = ds1->nx;
    if (ids2 < 0 || ids2 >= op->n_dat)
            return win_printf_OK("Index %d out of range [0,%d[",ids2,op->n_dat);
    ds2 = op->dat[ids2];
    nx = (ds2->nx > nx) ? ds2->nx : nx;

    min2D = (float**)calloc(ds2->nx,sizeof(float*));
    if (min2D == NULL)  return win_printf_OK("cannot allocate min2D");
    for(j = 0; j < ds2->nx; j++)
      {
         min2D[j] = (float*)calloc(ds1->nx,sizeof(float));
         if (min2D[j] == NULL)  return win_printf_OK("cannot allocate min2D[%d]",j);
      }

    match1 = (int*)calloc(ds1->nx,sizeof(int));
    if (match1 == NULL)  return win_printf_OK("cannot allocate match");
    for (i = 0; i < ds1->nx; i++)      {
       match1[i] = -1;
       tmp1 = (in_y) ? ds1->yd[i] : ds1->xd[i];
       min1 = (i == 0 || tmp1 < min1) ? tmp1 : min1;
       max1 = (i == 0 || tmp1 > max1) ? tmp1 : max1;
     }
    n_m1 = 0;


    match2 = (int*)calloc(ds2->nx,sizeof(int));
    if (match2 == NULL)  return win_printf_OK("cannot allocate match");
    for (i = 0; i < ds2->nx; i++)       {
         match2[i] = -1;
         tmp2 = (in_y) ? ds2->yd[i] : ds2->xd[i];
         min2 = (i == 0 || tmp2 < min2) ? tmp2 : min2;
         max2 = (i == 0 || tmp2 > max2) ? tmp2 : max2;
     }
    for(j = 0; j < ds2->nx; j++)
      {
         tmp2 = (in_y) ? ds2->yd[j] : ds2->xd[j];
         for (i = 0; i < ds1->nx; i++) min2D[j][i] = -1;
         for (i = 0; i < ds1->nx; i++)
            {
                tmp1 = (in_y) ? ds1->yd[i] : ds1->xd[i];
                tmp = fabs(tmp2 - (stretch * tmp1));
                dzl = dz + fabs(dstretch * tmp1);
                if (tmp < dzl)                  {
                    min2D[j][i] = tmp;
                    //win_printf("Possible [%d,%d] = %g",j,i,tmp);
                  }
                //else  win_printf("Not Possible [%d,%d] = %g, tmp1 %g tmp2 %g",j,i,tmp,tmp1,tmp2);
            }
      }

    opn = create_and_attach_one_plot(pr, nx, nx, 0);
    if (opn == NULL)  return win_printf_OK("cannot allocate match");
    if (in_y)
      {
         uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opn, IS_X_UNIT_SET);
         uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opn, IS_Y_UNIT_SET);
      }
    else
      {
         uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_X_UNIT_SET);
         uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_Y_UNIT_SET);
      }
    dsn = opn->dat[0];
    for (k = 0; k < nx; k++)
      {
         for(j = 0, min = FLT_MAX; j < ds2->nx; j++)
           {
              for (l = 0; match2[l] != j && l < n_m1; l++);
              if (match2[l] == j) continue;
              for (i = 0; i < ds1->nx; i++)
                {
                   for (l = 0; match1[l] != i && l < n_m1; l++);
                   if (match1[l] == i) continue;
                   if (min2D[j][i] >= 0 && min2D[j][i] < min)
                     {
                       min = min2D[j][i];
                       imin = i;
                       jmin = j;
                     }
                }
           }
         if (min != FLT_MAX)
           {
             match1[n_m1] = imin;
             match2[n_m1] = jmin;
             dsn->xd[n_m1] = (in_y) ? ds1->yd[imin] : ds1->xd[imin];
             dsn->yd[n_m1++] = (in_y) ? ds2->yd[jmin] : ds2->xd[jmin];
             dsn->nx = dsn->ny = n_m1;
           }
      }
    QuickSort_double(dsn->xd, dsn->yd, 0, dsn->nx - 1);
    set_plot_symb(dsn, "\\pt5\\oc");

    if (cross)
      {
         dsl = create_and_attach_one_ds(opn, 16, 16, 0);
         if (dsl == NULL)  return win_printf_OK("cannot allocate match");
         set_ds_dash_line(dsl);
         set_ds_line_color(dsl,Green);
         for(j = 0, dsl->nx = dsl->ny = 0; j < ds2->nx; j++)
           {
             for (l = 0; match2[l] != j && l < n_m1; l++);
             if (match2[l] != j) continue;

             tmp2  = (in_y) ? ds2->yd[j] : ds2->xd[j];
             //dsl->yd[dsl->nx] = tmp2;
             if (cross == 1)
               {
                 for(i = 0; i < ds1->nx; i++)
                   {
                      tmp1 = (in_y) ? ds1->yd[i] : ds1->xd[i];
                      if ((tmp2 - (stretch * tmp1)) < 0) break;
                   }
                 tmp1 = (in_y) ? ds1->yd[(i>1)?i-2:0] : ds1->xd[(i>1)?i-2:0];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->xd[dsl->nx++] = tmp1;
               }
             else {
                     add_new_point_to_ds(dsl, min1, tmp2);//dsl->xd[dsl->nx++] = min1;
             }
             //dsl->yd[dsl->nx] 
             //tmp2 = (in_y) ? ds2->yd[j] : ds2->xd[j];
             if (cross == 1)
               {
                 i += 2;
                 i = (i < ds1->nx) ? i : ds1->nx - 1;
                 tmp1 = (in_y) ? ds1->yd[i] : ds1->xd[i];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->xd[dsl->nx++] = tmp1;
               }
             else {
                     add_new_point_to_ds(dsl, max1, tmp2);//dsl->xd[dsl->nx++] = max1;
             }
           }
         for(j = 0; j < ds1->nx; j++)
           {
             for (l = 0; match1[l] != j && l < n_m1; l++);
             if (match1[l] != j) continue;

             tmp1 = (in_y) ? ds1->yd[j] : ds1->xd[j];
             //dsl->xd[dsl->nx] = tmp1;
             if (cross == 1)
               {
                 for(i = 0; i < ds2->nx; i++)
                   {
                      tmp2 = (in_y) ? ds2->yd[i] : ds2->xd[i];
                      if ((tmp2 - (stretch * tmp1)) > 0) break;
                   }
                 tmp2 = (in_y) ? ds2->yd[(i>1)?i-2:0] : ds2->xd[(i>1)?i-2:0];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->yd[dsl->nx++] = tmp2;
               }
             else  add_new_point_to_ds(dsl, tmp1, min2);//dsl->yd[dsl->nx++] = min2;
             //tmp1 = (in_y) ? ds1->yd[j] : ds1->xd[j];
             //dsl->xd[dsl->nx] = (in_y) ? ds1->yd[j] : ds1->xd[j];
             if (cross == 1)
               {
                 i += 2;
                 i = (i < ds2->nx) ? i : ds2->nx - 1;
                 tmp2 = (in_y) ? ds2->yd[i] : ds2->xd[i];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->yd[dsl->nx++] = tmp2;
               }
             else add_new_point_to_ds(dsl, tmp1, max2);//dsl->yd[dsl->nx++] = max2;
           }

         dsl = create_and_attach_one_ds(opn, 16, 16, 0);
         if (dsl == NULL)  return win_printf_OK("cannot allocate match");
         set_ds_dash_line(dsl);
         set_ds_line_color(dsl,Lightred);

         for(j = 0, dsl->nx = dsl->ny = 0; j < ds2->nx; j++)
           {
             for (l = 0; match2[l] != j && l < n_m1; l++);
             if (match2[l] == j) continue;

             tmp2  = (in_y) ? ds2->yd[j] : ds2->xd[j];
             //dsl->yd[dsl->nx] = tmp2;
             if (cross == 1)
               {
                 for(i = 0; i < ds1->nx; i++)
                   {
                      tmp1 = (in_y) ? ds1->yd[i] : ds1->xd[i];
                      if ((tmp2 - (stretch * tmp1)) < 0) break;
                   }
                 tmp1 = (in_y) ? ds1->yd[(i>1)?i-2:0] : ds1->xd[(i>1)?i-2:0];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->xd[dsl->nx++] = tmp1;
               }
             else add_new_point_to_ds(dsl, min1, tmp2);//dsl->xd[dsl->nx++] = min1;
             //dsl->yd[dsl->nx] 
             //tmp2 = (in_y) ? ds2->yd[j] : ds2->xd[j];
             if (cross == 1)
               {
                 i += 2;
                 i = (i < ds1->nx) ? i : ds1->nx - 1;
                 tmp1 = (in_y) ? ds1->yd[i] : ds1->xd[i];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->xd[dsl->nx++] = tmp1;
               }
             else add_new_point_to_ds(dsl, max1, tmp2);//dsl->xd[dsl->nx++] = max1;
           }

         dsl = create_and_attach_one_ds(opn, 16, 16, 0);
         if (dsl == NULL)  return win_printf_OK("cannot allocate match");
         set_ds_dash_line(dsl);
         set_ds_line_color(dsl,Lightmagenta);

         for(j = 0, dsl->nx = dsl->ny = 0; j < ds1->nx; j++)
           {
             for (l = 0; match1[l] != j && l < n_m1; l++);
             if (match1[l] == j) continue;

             tmp1 = (in_y) ? ds1->yd[j] : ds1->xd[j];
             //dsl->xd[dsl->nx] = tmp1;
             if (cross == 1)
               {
                 for(i = 0; i < ds2->nx; i++)
                   {
                      tmp2 = (in_y) ? ds2->yd[i] : ds2->xd[i];
                      if ((tmp2 - (stretch * tmp1)) > 0) break;
                   }
                 tmp2 = (in_y) ? ds2->yd[(i>1)?i-2:0] : ds2->xd[(i>1)?i-2:0];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->yd[dsl->nx++] = tmp2;
               }
             else  add_new_point_to_ds(dsl, tmp1, min2);//dsl->yd[dsl->nx++] = min2;
             //tmp1 = (in_y) ? ds1->yd[j] : ds1->xd[j];
             //dsl->xd[dsl->nx] = (in_y) ? ds1->yd[j] : ds1->xd[j];
             if (cross == 1)
               {
                 i += 2;
                 i = (i < ds2->nx) ? i : ds2->nx - 1;
                 tmp2 = (in_y) ? ds2->yd[i] : ds2->xd[i];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->yd[dsl->nx++] = tmp2;
               }
             else add_new_point_to_ds(dsl, tmp1, max2);//dsl->yd[dsl->nx++] = max2;
           }


      }
    if (bound)
      {
         dsb = create_and_attach_one_ds(opn, 8, 8, 0);
         if (dsb == NULL)  return win_printf_OK("cannot allocate match");
         set_ds_dash_line(dsb);
         dsb->xd[0] = min1;
         dsb->yd[0] = stretch * min1 - dstretch * fabs(min1) - dz;
         dsb->xd[1] = 0;
         dsb->yd[1] = -dz;
         dsb->xd[2] = 0;
         dsb->yd[2] = -dz;
         dsb->xd[3] = max1;
         dsb->yd[3] = stretch * max1 - dstretch * fabs(max1) - dz;

         dsb->xd[4] = min1;
         dsb->yd[4] = stretch * min1 + dstretch * fabs(min1) + dz;
         dsb->xd[5] = 0;
         dsb->yd[5] = dz;
         dsb->xd[6] = 0;
         dsb->yd[6] = dz;
         dsb->xd[7] = max1;
         dsb->yd[7] = stretch * max1 + dstretch * fabs(max1) + dz;
      }



    /*

    for (i = 0; i < ds1->nx; i++)
      {
        tmp1 = (in_y) ? ds1->yd[i] : ds1->xd[i];           for(j = 0, imin = -1, min = FLT_MAX; j < ds2->nx; j++)
         {
            for (k = 0; match[k] != j && k < nx; k++);
            if (match[k] == j) continue; // pt already matched
            tmp2 = (in_y) ? ds2->yd[j] : ds2->xd[j];               if ((tmp1 >= ((stretch - dstretch) * tmp2) -dz)                && (tmp1 <= ((stretch + dstretch) * tmp2) +dz))
              {
                 tmp = tmp1 - stretch * tmp2;
                 if (tmp < min)
                   {
                      min = tmp;
                      imin = j;
                   }
              }         }
        if (imin >= 0)
           match[i] = imin;
      }
    for (i = 0, k = 0; i < nx; i++)       {
         if (match[i] >= 0)
          {
             dsn->xd[k] = (in_y) ? ds1->yd[i] : ds1->xd[i];
             j = match[i];
             dsn->yd[k++] = (in_y) ? ds2->yd[j] : ds2->xd[j];             }          }
    dsn->nx = dsn->ny = k;
    */

    for(j = 0; j < ds2->nx; j++)
       free(min2D[j]);
    free(min2D);
    free(match2);
    free(match1);
    return refresh_plot(pr, UNCHANGED);
}

# endif
int do_generate_Plot_with_matching_pts_in_2_ds(void)
{
    int i, j, k, l, nx, imin = 0, jmin = 0;
    O_p *op = NULL, *opn = NULL;
    pltreg *pr = NULL;
    d_s *ds1 = NULL, *ds2 = NULL, *dsn = NULL, *dsl = NULL, *dsb = NULL;
    static int ids1 = 0, ids2 = 1, in_y1 = 0, in_y2 = 0, cross = 1, bound = 1;
    static float stretch = 0.95, dstretch = 0.05, dz = 0.005;
    int n_m1, *match1 = NULL, *match2 = NULL;
    float min, tmp1, tmp2, tmp, **min2D = NULL, min1 = 0, max1 = 0, min2 = 0, max2 = 0, dzl;
    //extern int QuickSort_double(float * xd, float * yd, int l, int r);

    if (updating_menu_state != 0)        return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiply the y coordinate"
                             "of a data set by a number and place it in a new plot");
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) // we get the plot region
        return win_printf_OK("cannot find data");

    i = win_scanf("Find peaks compatible between 2 ds within an affine transform\n"
                  "First ds index  %4d, in X%R or in Y%r\n"
                  "second ds index %4d, in X%R or in Y%r\n"
                  "Strech \\lambda %6f d\\lambda max %6f accuracy %6f\n"
                  "Diplay Crossing lines %R->No, %r->partial, %r->full\n"
                  "%bDisplay boundary\n"
                  , &ids1, &in_y1, &ids2, &in_y2,  &stretch, &dstretch, &dz,&cross, &bound);

    if (i == WIN_CANCEL) return D_O_K;

    if (ids1 < 0 || ids1 >= op->n_dat)
            return win_printf_OK("Index %d out of range [0,%d[",ids1,op->n_dat);
    ds1 = op->dat[ids1];
    nx = ds1->nx;
    if (ids2 < 0 || ids2 >= op->n_dat)
            return win_printf_OK("Index %d out of range [0,%d[",ids2,op->n_dat);
    ds2 = op->dat[ids2];
    nx = (ds2->nx > nx) ? ds2->nx : nx;

    min2D = (float**)calloc(ds2->nx,sizeof(float*));
    if (min2D == NULL)  return win_printf_OK("cannot allocate min2D");
    for(j = 0; j < ds2->nx; j++)
      {
         min2D[j] = (float*)calloc(ds1->nx,sizeof(float));
         if (min2D[j] == NULL)  return win_printf_OK("cannot allocate min2D[%d]",j);
      }

    match1 = (int*)calloc(ds1->nx,sizeof(int));
    if (match1 == NULL)  return win_printf_OK("cannot allocate match");
    for (i = 0; i < ds1->nx; i++)      {
       match1[i] = -1;
       tmp1 = (in_y1) ? ds1->yd[i] : ds1->xd[i];
       min1 = (i == 0 || tmp1 < min1) ? tmp1 : min1;
       max1 = (i == 0 || tmp1 > max1) ? tmp1 : max1;
     }
    n_m1 = 0;


    match2 = (int*)calloc(ds2->nx,sizeof(int));
    if (match2 == NULL)  return win_printf_OK("cannot allocate match");
    for (i = 0; i < ds2->nx; i++)       {
         match2[i] = -1;
         tmp2 = (in_y2) ? ds2->yd[i] : ds2->xd[i];
         min2 = (i == 0 || tmp2 < min2) ? tmp2 : min2;
         max2 = (i == 0 || tmp2 > max2) ? tmp2 : max2;
     }
    for(j = 0; j < ds2->nx; j++)
      {
         tmp2 = (in_y2) ? ds2->yd[j] : ds2->xd[j];
         for (i = 0; i < ds1->nx; i++) min2D[j][i] = -1;
         for (i = 0; i < ds1->nx; i++)
            {
                tmp1 = (in_y1) ? ds1->yd[i] : ds1->xd[i];
                tmp = fabs(tmp2 - (stretch * tmp1));
                dzl = dz + fabs(dstretch * tmp1);
                if (tmp < dzl)                  {
                    min2D[j][i] = tmp;
                    //win_printf("Possible [%d,%d] = %g",j,i,tmp);
                  }
                //else  win_printf("Not Possible [%d,%d] = %g, tmp1 %g tmp2 %g",j,i,tmp,tmp1,tmp2);
            }
      }

    opn = create_and_attach_one_plot(pr, nx, nx, 0);
    if (opn == NULL)  return win_printf_OK("cannot allocate match");
    if (in_y1)
      {
         uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opn, IS_X_UNIT_SET);
         uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opn, IS_Y_UNIT_SET);
      }
    else
      {
         uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_X_UNIT_SET);
         uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_Y_UNIT_SET);
      }
    dsn = opn->dat[0];
    for (k = 0; k < nx; k++)
      {
         for(j = 0, min = FLT_MAX; j < ds2->nx; j++)
           {
              for (l = 0; match2[l] != j && l < n_m1; l++);
              if (match2[l] == j) continue;
              for (i = 0; i < ds1->nx; i++)
                {
                   for (l = 0; match1[l] != i && l < n_m1; l++);
                   if (match1[l] == i) continue;
                   if (min2D[j][i] >= 0 && min2D[j][i] < min)
                     {
                       min = min2D[j][i];
                       imin = i;
                       jmin = j;
                     }
                }
           }
         if (min != FLT_MAX)
           {
             match1[n_m1] = imin;
             match2[n_m1] = jmin;
             dsn->xd[n_m1] = (in_y1) ? ds1->yd[imin] : ds1->xd[imin];
             dsn->yd[n_m1++] = (in_y2) ? ds2->yd[jmin] : ds2->xd[jmin];
             dsn->nx = dsn->ny = n_m1;
           }
      }
    QuickSort_double(dsn->xd, dsn->yd, 0, dsn->nx - 1);
    set_plot_symb(dsn, "\\pt5\\oc");

    if (cross)
      {
         dsl = create_and_attach_one_ds(opn, 16, 16, 0);
         if (dsl == NULL)  return win_printf_OK("cannot allocate match");
         set_ds_dash_line(dsl);
         set_ds_line_color(dsl,Green);
         for(j = 0, dsl->nx = dsl->ny = 0; j < ds2->nx; j++)
           {
             for (l = 0; match2[l] != j && l < n_m1; l++);
             if (match2[l] != j) continue;

             tmp2  = (in_y2) ? ds2->yd[j] : ds2->xd[j];
             //dsl->yd[dsl->nx] = tmp2;
             if (cross == 1)
               {
                 for(i = 0; i < ds1->nx; i++)
                   {
                      tmp1 = (in_y1) ? ds1->yd[i] : ds1->xd[i];
                      if ((tmp2 - (stretch * tmp1)) < 0) break;
                   }
                 tmp1 = (in_y1) ? ds1->yd[(i>1)?i-2:0] : ds1->xd[(i>1)?i-2:0];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->xd[dsl->nx++] = tmp1;
               }
             else {
                     add_new_point_to_ds(dsl, min1, tmp2);//dsl->xd[dsl->nx++] = min1;
             }
             //dsl->yd[dsl->nx] 
             //tmp2 = (in_y) ? ds2->yd[j] : ds2->xd[j];
             if (cross == 1)
               {
                 i += 2;
                 i = (i < ds1->nx) ? i : ds1->nx - 1;
                 tmp1 = (in_y2) ? ds1->yd[i] : ds1->xd[i];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->xd[dsl->nx++] = tmp1;
               }
             else {
                     add_new_point_to_ds(dsl, max1, tmp2);//dsl->xd[dsl->nx++] = max1;
             }
           }
         for(j = 0; j < ds1->nx; j++)
           {
             for (l = 0; match1[l] != j && l < n_m1; l++);
             if (match1[l] != j) continue;

             tmp1 = (in_y1) ? ds1->yd[j] : ds1->xd[j];
             //dsl->xd[dsl->nx] = tmp1;
             if (cross == 1)
               {
                 for(i = 0; i < ds2->nx; i++)
                   {
                      tmp2 = (in_y2) ? ds2->yd[i] : ds2->xd[i];
                      if ((tmp2 - (stretch * tmp1)) > 0) break;
                   }
                 tmp2 = (in_y2) ? ds2->yd[(i>1)?i-2:0] : ds2->xd[(i>1)?i-2:0];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->yd[dsl->nx++] = tmp2;
               }
             else  add_new_point_to_ds(dsl, tmp1, min2);//dsl->yd[dsl->nx++] = min2;
             //tmp1 = (in_y) ? ds1->yd[j] : ds1->xd[j];
             //dsl->xd[dsl->nx] = (in_y) ? ds1->yd[j] : ds1->xd[j];
             if (cross == 1)
               {
                 i += 2;
                 i = (i < ds2->nx) ? i : ds2->nx - 1;
                 tmp2 = (in_y2) ? ds2->yd[i] : ds2->xd[i];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->yd[dsl->nx++] = tmp2;
               }
             else add_new_point_to_ds(dsl, tmp1, max2);//dsl->yd[dsl->nx++] = max2;
           }

         dsl = create_and_attach_one_ds(opn, 16, 16, 0);
         if (dsl == NULL)  return win_printf_OK("cannot allocate match");
         set_ds_dash_line(dsl);
         set_ds_line_color(dsl,Lightred);

         for(j = 0, dsl->nx = dsl->ny = 0; j < ds2->nx; j++)
           {
             for (l = 0; match2[l] != j && l < n_m1; l++);
             if (match2[l] == j) continue;

             tmp2  = (in_y2) ? ds2->yd[j] : ds2->xd[j];
             //dsl->yd[dsl->nx] = tmp2;
             if (cross == 1)
               {
                 for(i = 0; i < ds1->nx; i++)
                   {
                      tmp1 = (in_y1) ? ds1->yd[i] : ds1->xd[i];
                      if ((tmp2 - (stretch * tmp1)) < 0) break;
                   }
                 tmp1 = (in_y1) ? ds1->yd[(i>1)?i-2:0] : ds1->xd[(i>1)?i-2:0];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->xd[dsl->nx++] = tmp1;
               }
             else add_new_point_to_ds(dsl, min1, tmp2);//dsl->xd[dsl->nx++] = min1;
             //dsl->yd[dsl->nx] 
             //tmp2 = (in_y) ? ds2->yd[j] : ds2->xd[j];
             if (cross == 1)
               {
                 i += 2;
                 i = (i < ds1->nx) ? i : ds1->nx - 1;
                 tmp1 = (in_y1) ? ds1->yd[i] : ds1->xd[i];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->xd[dsl->nx++] = tmp1;
               }
             else add_new_point_to_ds(dsl, max1, tmp2);//dsl->xd[dsl->nx++] = max1;
           }

         dsl = create_and_attach_one_ds(opn, 16, 16, 0);
         if (dsl == NULL)  return win_printf_OK("cannot allocate match");
         set_ds_dash_line(dsl);
         set_ds_line_color(dsl,Lightmagenta);

         for(j = 0, dsl->nx = dsl->ny = 0; j < ds1->nx; j++)
           {
             for (l = 0; match1[l] != j && l < n_m1; l++);
             if (match1[l] == j) continue;

             tmp1 = (in_y1) ? ds1->yd[j] : ds1->xd[j];
             //dsl->xd[dsl->nx] = tmp1;
             if (cross == 1)
               {
                 for(i = 0; i < ds2->nx; i++)
                   {
                      tmp2 = (in_y2) ? ds2->yd[i] : ds2->xd[i];
                      if ((tmp2 - (stretch * tmp1)) > 0) break;
                   }
                 tmp2 = (in_y2) ? ds2->yd[(i>1)?i-2:0] : ds2->xd[(i>1)?i-2:0];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->yd[dsl->nx++] = tmp2;
               }
             else  add_new_point_to_ds(dsl, tmp1, min2);//dsl->yd[dsl->nx++] = min2;
             //tmp1 = (in_y) ? ds1->yd[j] : ds1->xd[j];
             //dsl->xd[dsl->nx] = (in_y) ? ds1->yd[j] : ds1->xd[j];
             if (cross == 1)
               {
                 i += 2;
                 i = (i < ds2->nx) ? i : ds2->nx - 1;
                 tmp2 = (in_y2) ? ds2->yd[i] : ds2->xd[i];
                 add_new_point_to_ds(dsl, tmp1, tmp2);
                 //dsl->yd[dsl->nx++] = tmp2;
               }
             else add_new_point_to_ds(dsl, tmp1, max2);//dsl->yd[dsl->nx++] = max2;
           }


      }
    if (bound)
      {
         dsb = create_and_attach_one_ds(opn, 8, 8, 0);
         if (dsb == NULL)  return win_printf_OK("cannot allocate match");
         set_ds_dash_line(dsb);
         dsb->xd[0] = min1;
         dsb->yd[0] = stretch * min1 - dstretch * fabs(min1) - dz;
         dsb->xd[1] = 0;
         dsb->yd[1] = -dz;
         dsb->xd[2] = 0;
         dsb->yd[2] = -dz;
         dsb->xd[3] = max1;
         dsb->yd[3] = stretch * max1 - dstretch * fabs(max1) - dz;

         dsb->xd[4] = min1;
         dsb->yd[4] = stretch * min1 + dstretch * fabs(min1) + dz;
         dsb->xd[5] = 0;
         dsb->yd[5] = dz;
         dsb->xd[6] = 0;
         dsb->yd[6] = dz;
         dsb->xd[7] = max1;
         dsb->yd[7] = stretch * max1 + dstretch * fabs(max1) + dz;
      }


    for(j = 0; j < ds2->nx; j++)
       free(min2D[j]);
    free(min2D);
    free(match2);
    free(match1);
    return refresh_plot(pr, UNCHANGED);
}




MENU *ImOliSeq_image_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL) return mn;

    //  add_item_to_menu(mn,"average profile", do_ImOliSeq_average_along_y,NULL,0,NULL);
    //add_item_to_menu(mn,"image z rescaled", do_ImOliSeq_image_multiply_by_a_scalar,NULL,0,NULL);
    return mn;
}
MENU *ImOliSeq_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL) return mn;


    add_item_to_menu(mn, "Find matching points",do_generate_Plot_with_matching_pts_in_2_ds , NULL, 0, NULL);
    add_item_to_menu(mn, "view oligo on seq", do_generate_Plot_Oligo_Seq, NULL, 0, NULL);
    add_item_to_menu(mn, ">", do_move_psr , NULL, 0, NULL);
    add_item_to_menu(mn, "<", do_move_psl , NULL, 0, NULL);



    return mn;
}

int ImOliSeq_main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    add_image_treat_menu_item("ImOliSeq", NULL, ImOliSeq_image_menu(), 0, NULL);
    add_plot_treat_menu_item("ImOliSeq", NULL, ImOliSeq_plot_menu(), 0, NULL);
    return D_O_K;
}

int ImOliSeq_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;
    remove_item_to_menu(image_treat_menu, "ImOliSeq", NULL, NULL);
    remove_item_to_menu(plot_treat_menu, "ImOliSeq", NULL, NULL);
    return D_O_K;
}
#endif

