#ifndef _TESTOPENCL_H_
#define _TESTOPENCL_H_

PXV_FUNC(int, do_testOpenCl_rescale_plot, (void));
PXV_FUNC(MENU*, testOpenCl_plot_menu, (void));
PXV_FUNC(int, do_testOpenCl_rescale_data_set, (void));
PXV_FUNC(int, testOpenCl_main, (int argc, char **argv));
#endif

