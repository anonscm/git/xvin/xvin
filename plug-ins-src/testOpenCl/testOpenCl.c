/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _TESTOPENCL_C_
#define _TESTOPENCL_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 

#include "CL/cl.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>



/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin 
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "testOpenCl.h"



// For clarity,error checking has been omitted.


#define SUCCESS 0
#define FAILURE 1



int do_testOpenCl_hello(void)
{
  register int i;
  char ch[128];

  if(updating_menu_state != 0)	return D_O_K;



  /*Step1: Getting platforms and choose an available one.*/
  cl_uint numPlatforms;   //the NO. of platforms
  cl_platform_id platform = NULL; //the chosen platform
  cl_int  status = clGetPlatformIDs(0, NULL, &numPlatforms);
  if (status != CL_SUCCESS)
    {
      return win_printf_OK("Error: Getting platforms!");
    }

    /*For clarity, choose the first available platform. */
    if(numPlatforms > 0)
    {
        cl_platform_id* platforms = (cl_platform_id* )malloc(numPlatforms* sizeof(cl_platform_id));
        status = clGetPlatformIDs(numPlatforms, platforms, NULL);
        platform = platforms[0];
        free(platforms);
    }
    win_printf("%d Platform device available.\n",numPlatforms);
    /*Step 2:Query the platform and choose the first GPU device if has one.Otherwise use the CPU as device.*/
    cl_uint             numDevices = 0;
    cl_device_id        *devices;
    size_t sr;
    status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &numDevices);    
    if (numDevices == 0)    //no GPU available.
    {
      win_printf_OK("No GPU device available.\n"
		 "Choose CPU as default device.\n");
    }
    else
    {
      win_printf("%d GPU device available.\n",numDevices);
      devices = (cl_device_id*)malloc(numDevices * sizeof(cl_device_id));
      status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, numDevices, devices, NULL);
    }
    status = clGetDeviceInfo(devices[0], CL_DEVICE_MAX_COMPUTE_UNITS, 4, &numDevices, &sr);    
    win_printf("%d  device available.\n",numDevices);

    status = clGetDeviceInfo(devices[0], CL_DEVICE_MAX_CLOCK_FREQUENCY, 4, &numDevices, &sr);    
    win_printf("%d  clock freq.\n",numDevices);

    status = clGetDeviceInfo(devices[0], CL_DEVICE_NAME, 128, ch, &sr);    
    win_printf("device available.\n%s",ch);

    status = clGetDeviceInfo(devices[0], CL_DEVICE_OPENCL_C_VERSION, 128, ch, &sr);    
    win_printf("Cl version.\n%s",ch);


  i = win_printf("Hello from testOpenCl");
  if (i == CANCEL) win_printf_OK("you have press CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}


// Use a static data size for simplicity
//
#define DATA_SIZE (1024)

////////////////////////////////////////////////////////////////////////////////

// Simple compute kernel which computes the square of an input array 
//
const char *KernelSource = "\n" \
"__kernel void square(                                                       \n" \
"   __global float* input,                                              \n" \
"   __global float* output,                                             \n" \
"   const unsigned int count)                                           \n" \
"{                                                                      \n" \
"   int i = get_global_id(0);                                           \n" \
"   if(i < count)                                                       \n" \
"       output[i] = input[i] * input[i];                                \n" \
"}                                                                      \n" \
"\n";

////////////////////////////////////////////////////////////////////////////////

int do_testOpenCl_helloWorld(void) 
{
  int err;                            // error code returned from api calls
      
  float data[DATA_SIZE];              // original data set given to device
  float results[DATA_SIZE];           // results returned from device
  unsigned int correct;               // number of correct results returned

  size_t global;                      // global domain size for our calculation
  size_t local;                       // local domain size for our calculation

  cl_device_id device_id;             // compute device id 
  cl_context context;                 // compute context
  cl_command_queue commands;          // compute command queue
  cl_program program;                 // compute program
  cl_kernel kernel;                   // compute kernel
    
  cl_mem input;                       // device memory used for the input array
  cl_mem output;                      // device memory used for the output array
    
  // Fill our data set with random float values
  //
  int i = 0;
  unsigned int count = DATA_SIZE;


  if(updating_menu_state != 0)	return D_O_K;

  for(i = 0; i < count; i++)
    data[i] = rand() / (float)RAND_MAX;

  cl_uint numPlatforms;   //the NO. of platforms
  cl_platform_id platform = NULL; //the chosen platform
  cl_int  status = clGetPlatformIDs(0, NULL, &numPlatforms);
  if (status != CL_SUCCESS)
    {
      return win_printf_OK("Error: Getting platforms!");
    }

    /*For clarity, choose the first available platform. */
    if(numPlatforms > 0)
    {
        cl_platform_id* platforms = (cl_platform_id* )malloc(numPlatforms* sizeof(cl_platform_id));
        status = clGetPlatformIDs(numPlatforms, platforms, NULL);
        platform = platforms[0];
        free(platforms);
    }
    win_printf("%d Platform device available.\n",numPlatforms);


    /*Step 2:Query the platform and choose the first GPU device if has one.Otherwise use the CPU as device.*/
    cl_uint             numDevices = 0;
    status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &numDevices);    
    if (numDevices == 0)    //no GPU available.
    {
      return win_printf_OK("No GPU device available.\n"
		 "Choose CPU as default device.\n");
    }
    win_printf("%d GPU device available.\n",numDevices);
    
  // Connect to a compute device
  //
  int gpu = 1;
  err = clGetDeviceIDs(platform, (gpu ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU), 1, &device_id, NULL);
  if (err != CL_SUCCESS)
    {
      return win_printf_OK("Error: Failed to create a device group!\n");
    }
  
  // Create a compute context 
  //
  context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
  if (!context)
    {
      return win_printf_OK("Error: Failed to create a compute context!\n");
    }

  // Create a command commands
  //
  commands = clCreateCommandQueue(context, device_id, 0, &err);
  if (!commands)
    {
      return win_printf_OK("Error: Failed to create a command commands!\n");
    }

  // Create the compute program from the source buffer
  //
  program = clCreateProgramWithSource(context, 1, (const char **) & KernelSource, NULL, &err);
  if (!program)
    {
      return win_printf_OK("Error: Failed to create compute program!\n");
    }

  // Build the program executable
  //
  err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
  if (err != CL_SUCCESS)
    {
      size_t len;
      char buffer[2048];

      win_printf_OK("Error: Failed to build program executable!\n");
      clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
      return win_printf_OK("%s\n", buffer);
    }

  // Create the compute kernel in the program we wish to run
  //
  kernel = clCreateKernel(program, "square", &err);
  if (!kernel || err != CL_SUCCESS)
    {
      return win_printf_OK("Error: Failed to create compute kernel!\n");
    }

  // Create the input and output arrays in device memory for our calculation
  //
  input = clCreateBuffer(context,  CL_MEM_READ_ONLY,  sizeof(float) * count, NULL, NULL);
  output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(float) * count, NULL, NULL);
  if (!input || !output)
    {
      return win_printf_OK("Error: Failed to allocate device memory!\n");
    }    
    
  // Write our data set into the input array in device memory 
  //
  err = clEnqueueWriteBuffer(commands, input, CL_TRUE, 0, sizeof(float) * count, data, 0, NULL, NULL);
  if (err != CL_SUCCESS)
    {
      return win_printf_OK("Error: Failed to write to source array!\n");
    }

  // Set the arguments to our compute kernel
  //
  err = 0;
  err  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &input);
  err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &output);
  err |= clSetKernelArg(kernel, 2, sizeof(unsigned int), &count);
  if (err != CL_SUCCESS)
    {
      return win_printf_OK("Error: Failed to set kernel arguments! %d\n", err);
    }

  // Get the maximum work group size for executing the kernel on the device
  //
  err = clGetKernelWorkGroupInfo(kernel, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL);
  if (err != CL_SUCCESS)
    {
      return win_printf_OK("Error: Failed to retrieve kernel work group info! %d\n", err);
    }

  // Execute the kernel over the entire range of our 1d input data set
  // using the maximum number of work group items for this device
  //
  global = count;
  err = clEnqueueNDRangeKernel(commands, kernel, 1, NULL, &global, &local, 0, NULL, NULL);
  if (err)
    {
      return win_printf_OK("Error: Failed to execute kernel!\n");
    }
  win_printf("local %u",local);
  // Wait for the command commands to get serviced before reading back results
  //
  clFinish(commands);

  // Read back the results from the device to verify the output
  //
  err = clEnqueueReadBuffer( commands, output, CL_TRUE, 0, sizeof(float) * count, results, 0, NULL, NULL );  
  if (err != CL_SUCCESS)
    {
      return win_printf_OK("Error: Failed to read output array! %d\n", err);
    }
    
  // Validate our results
  //
  correct = 0;
  for(i = 0; i < count; i++)
    {
      if(results[i] == data[i] * data[i])
	correct++;
      if (i < 10)
	win_printf("result GPU %g result CPU %g dif %g",results[i],data[i] * data[i],results[i]-(data[i] * data[i]));
    }
    
  // Print a brief summary detailing the results
  //
  win_printf_OK("Computed '%d/%d' correct values!\n", correct, count);
    
  // Shutdown and cleanup
  //
  clReleaseMemObject(input);
  clReleaseMemObject(output);
  clReleaseProgram(program);
  clReleaseKernel(kernel);
  clReleaseCommandQueue(commands);
  clReleaseContext(context);

  return 0;
}




int do_testOpenCl_rescale_data_set(void)
{
  register int i, j;
  O_p *op = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == CANCEL)	return OFF;

  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_testOpenCl_rescale_plot(void)
{
  register int i, j;
  O_p *op = NULL, *opn = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  set_plot_title(opn, "Multiply by %f",factor);
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

MENU *testOpenCl_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Hello example", do_testOpenCl_hello,NULL,0,NULL);
  add_item_to_menu(mn,"Hello World", do_testOpenCl_helloWorld,NULL,0,NULL);
  add_item_to_menu(mn,"data set rescale in Y", do_testOpenCl_rescale_data_set,NULL,0,NULL);
  add_item_to_menu(mn,"plot rescale in Y", do_testOpenCl_rescale_plot,NULL,0,NULL);
  return mn;
}

int	testOpenCl_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "testOpenCl", NULL, testOpenCl_plot_menu(), 0, NULL);
  (void)argc;
  (void)argv;
  return D_O_K;
}

int	testOpenCl_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "testOpenCl", NULL, NULL);
  (void)argc;
  (void)argv;
  return D_O_K;
}
#endif

