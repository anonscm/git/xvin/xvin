/** Define and typedef declarations for the plug-in

This file contains the declaration of all defines and typedef in the plug-in, 
sorted by the .c file where they appear.


\author Adrien Meglio
*/

#ifndef NIDAQMXWRAP_H_
#define NIDAQMXWRAP_H_

// DEFINEs

#define NUMBER_OF_LINES 4
#define NUMBER_OF_AI_LINES 3


#define NIDAQ_MAX_POINT_FREQUENCY 833000 // The max write frequency (i.e. 1/VAL is the time between 2 points, in seconds) 

#define TRUE 1
#define FLASE 0

#define LINE_1 0
#define LINE_2 1
#define LINE_3 2
#define LINE_4 3

// TYPEDEFs

typedef struct LINE_STATUS
{
    float level; /** Percentage of max value (1 is maximum) */
    
    int num_points; /** Number of data points */
    float64* data; /** Data points */
    int status; /** ON of OFF */
} l_s ;

typedef struct LINE_IO
{
    float64 frequency; /** Number of periods by second */
    int num_cycles; /** Number of periods */
    
    l_s* lines;
} l_io ;


//Variables
#ifndef NIDAQMXWRAP_C_
PXV_VAR(TaskHandle, CURRENTtaskHandle);
PXV_VAR(TaskHandle, SYNCtaskHandle);
PXV_VAR(int, SYNC_flag);
PXV_VAR(int, SYNC_plot_flag);
PXV_VAR(int, num_points_per_period);
PXV_VAR(double*, AO_data);
PXV_VAR(double*, AI_data);
PXV_VAR(O_p, *sync_gop[]);
PXV_VAR(int, num_points);
#endif


//Functions
//PXV_FUNC(int, DAQmxErrChk,(int32 functionCall));
PXV_FUNC(int,	NIDAQ_main,(void));

PXV_FUNC(int, lines__merge_data_and_launch,(l_s *lines, int duration, float64 frequency));
PXV_FUNC(int, IO__NIDAQ_max_frequency,(float *max_frequency));
PXV_FUNC(int,	NIDAQ_user2nidaq_main,(void));

PXV_FUNC(int, copy_sync_data_to_ops,(void));
//PXV_FUNC(int, task__create_and_launch,(int task_duration, float64 frequency));
PXV_FUNC(int, task__sync_create_and_launch,(int task_duration, float64 frequency));
//PXV_FUNC(int32, CVICALLBACK task__end,(TaskHandle taskHandle, int32 status, void *callbackData));
PXV_FUNC(int32, CVICALLBACK task__sync_EveryNCallback,(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples,  void *callbackData));

PXV_FUNC(int, task__all_close,(void));
PXV_FUNC(int,	NIDAQ_task_handling_main,(void));


//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////


#endif
