/** \file nidaq.c

\brief Main program for AOTF control.
Rewritten in vincent's plug-in style by Francesco
    
\author Adrien Meglio

\version 18/07/2006
*/

#ifndef NIDAQMXWRAP_C_
#define NIDAQMXWRAP_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <NIDAQmx.h>
#include "../logfile/logfile.h"

#define BUILDING_PLUGINS_DLL
#include "nidaqmxwrap.h"

//Global Variables
TaskHandle CURRENTtaskHandle;
TaskHandle SYNCtaskHandle;
int SYNC_flag = 0;
int SYNC_plot_flag = 0;
int num_points_per_period;
double* AO_data;
double* AI_data;
O_p *sync_gop[NUMBER_OF_AI_LINES];
int num_points;


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// Base functions

/** Replacement function for error checking.

This function replaces the ill-formatted NI definition.

\param int32 functionCall The status to check.

\author Adrien Meglio
\version 13/09/2006
*/
#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else


/** Get Terminal name function.

This function copies the device name string in the correct form.

\param taskHandle the task handle
\param const char TerminalName[] the terminal name string
\param char triggerName[] the trigger name string

\return the error

\author Francesco Mosconi
\version 19/04/2007
*/
static int32 GetTerminalNameWithDevPrefix(TaskHandle taskHandle, const char terminalName[], char triggerName[])
{
  int32   error=0;
  char    errBuff[2048]={'\0'};
   char	device[256];
  int32	productCategory;
  uInt32	numDevices,i=1;
  
  DAQmxErrChk (DAQmxGetTaskNumDevices(taskHandle,&numDevices));
  while( i<=numDevices ) {
    DAQmxErrChk (DAQmxGetNthTaskDevice(taskHandle,i++,device,256));
    DAQmxErrChk (DAQmxGetDevProductCategory(device,&productCategory));
    if( productCategory!=DAQmx_Val_CSeriesModule && productCategory!=DAQmx_Val_SCXIModule )
      *triggerName++ = '/';
    strcat(strcat(strcpy(triggerName,device),"/"),terminalName);
    break;
  }

 Error:
	return error;
}

/* //////////////////////////////////////////////////////////////////////////////////////////////////////// */
/* //////////////////////////////////////////////////////////////////////////////////////////////////////// */
/* // Tash handling functions */

/* /\** Task setting and launching. */

/* NOTE : The configuration is such that the user may change the frequency independently of the resolution.  */
/* The user creates a 'lines' structure with a given number of points per period. It is the resolution of the signal.  */
/* Then the user can change the signal frequency via the #define FREQUENCY. */

/* \param int task_duration Specifies how many times the task will be played. May take the values PLAY_ONCE or PLAY_FOREVER. */
/* \param int frequency The frequency in sec^(-1) of the signal, i.e. 1/frequency  */
/* is the total duration in seconds of the task if it is played once.   */

/* \author Adrien Meglio */
/* \version 11/02/07 */
/* *\/  */
/* int task__create_and_launch(int task_duration, float64 frequency) */
/* {   */
/*   int32 ret = 0; */
    
/*   if (updating_menu_state != 0) return D_O_K; */
    
/*   if (CURRENTtaskHandle != 0) /\* If a task is currently running *\/ */
/*     { */
/*       draw_bubble(screen,0,550,100,"Task already running !"); */
        
/*       return D_O_K;  */
/*     } */
 
/*   DAQmxErrChk (DAQmxCreateTask("",&CURRENTtaskHandle)); */
    
/*   DAQmxErrChk (DAQmxCreateAOVoltageChan(CURRENTtaskHandle,"Dev1/ao0:3","",-10.0,10.0,DAQmx_Val_Volts,NULL)); */

/*   DAQmxErrChk (DAQmxRegisterDoneEvent(CURRENTtaskHandle,0,task__end,NULL)); */
    
/*   DAQmxErrChk (DAQmxCfgSampClkTiming(CURRENTtaskHandle,"",frequency*num_points_per_period,DAQmx_Val_Rising,task_duration,num_points_per_period)); */
	    
/*   DAQmxErrChk (DAQmxWriteAnalogF64(CURRENTtaskHandle,num_points_per_period,FALSE,10.0,DAQmx_Val_GroupByScanNumber,AO_data,&ret,NULL)); */
    	
/*   DAQmxErrChk (DAQmxStartTask(CURRENTtaskHandle)); */


/*   return D_O_K; */
/* } */

/** Task setting and launching. this version is for sinchronous analog output and input

NOTE : The configuration is such that the user may change the frequency independently of the resolution. 
The user creates a 'lines' structure with a given number of points per period. It is the resolution of the signal. 
Then the user can change the signal frequency via the #define FREQUENCY.

\param int task_duration Specifies how many times the task will be played. May take the values PLAY_ONCE or PLAY_FOREVER.
\param int frequency The frequency in sec^(-1) of the signal, i.e. 1/frequency 
is the total duration in seconds of the task if it is played once.  

\author Francesco Mosconi
\version 19/04/07
*/ 
int task__sync_create_and_launch(int task_duration, float64 frequency)
{  
 int32   error=0;
  char    errBuff[2048]={'\0'};
 
  int32 ret = 0;
  char        trigName[256];

  if (updating_menu_state != 0) return D_O_K;
    
  if (CURRENTtaskHandle != 0 || SYNCtaskHandle != 0) /* If a task is currently running */
    {
      draw_bubble(screen,0,550,100,"Task already running !");
        
      return D_O_K; 
    }
  /*********************************************/
  // DAQmx Configure Code
  /*********************************************/

  //first I configure an analog output task
  DAQmxErrChk (DAQmxCreateTask("",&CURRENTtaskHandle));
  DAQmxErrChk (DAQmxCreateAOVoltageChan(CURRENTtaskHandle,"Dev1/ao0:3","",-10.0,10.0,DAQmx_Val_Volts,NULL));
  DAQmxErrChk (DAQmxCfgSampClkTiming(CURRENTtaskHandle,"",frequency*num_points_per_period,DAQmx_Val_Rising,task_duration,num_points_per_period));

  //then I export the trigger
  DAQmxErrChk (GetTerminalNameWithDevPrefix(CURRENTtaskHandle,"ao/SampleClock",trigName));
  //then I configure the analog input task
  DAQmxErrChk (DAQmxCreateTask("",&SYNCtaskHandle));
  //I'm configuring the terminal as Referenced Single Ended (RSE), maybe Differential is better, but I haven't connected the pins yet.
  DAQmxErrChk (DAQmxCreateAIVoltageChan(SYNCtaskHandle,"Dev1/ai0,Dev1/ai1,Dev1/ai8","",DAQmx_Val_RSE,-10.0,10.0,DAQmx_Val_Volts,NULL));
  DAQmxErrChk (DAQmxCfgSampClkTiming(SYNCtaskHandle,trigName,frequency*num_points_per_period,DAQmx_Val_Rising,task_duration,num_points_per_period));
  
  //this sets the callback function that will retrieve the data
  DAQmxErrChk (DAQmxRegisterEveryNSamplesEvent(SYNCtaskHandle,DAQmx_Val_Acquired_Into_Buffer,num_points_per_period,0,task__sync_EveryNCallback,NULL));

  DAQmxErrChk (DAQmxDisableStartTrig (CURRENTtaskHandle));
  
  //Give data to the task
  DAQmxErrChk (DAQmxWriteAnalogF64(CURRENTtaskHandle,num_points_per_period,FALSE,10.0,DAQmx_Val_GroupByScanNumber,AO_data,&ret,NULL));
    	 
  /*********************************************/
  // DAQmx Start Code
  /*********************************************/
  
  DAQmxErrChk (DAQmxStartTask(SYNCtaskHandle));
  
  DAQmxErrChk (DAQmxStartTask(CURRENTtaskHandle));

 Error:
  if( DAQmxFailed(error) ) {
    DAQmxGetExtendedErrorInfo(errBuff,2048);
    dump_to_xv_log_file_with_date_and_time("DAQmx Error: %s\n",errBuff);
    task__all_close();
  }	   
  return D_O_K;
}


/** Stops the current task.

\author Adrien Meglio
\version 26/01/07
*/
int task__all_close(void)
{
  int32   error=0;
  char    errBuff[2048]={'\0'};
  
  if(updating_menu_state != 0)	return D_O_K;

  if (CURRENTtaskHandle != 0) 
    {
      //dump_to_xv_log_file_with_date_and_time("CURRENTTask not finished... I will stop it !");
        
      DAQmxErrChk (DAQmxStopTask(CURRENTtaskHandle));
      DAQmxErrChk (DAQmxClearTask(CURRENTtaskHandle));
    
      //dump_to_xv_log_file_with_date_and_time("CURRENTTask stopped");
    
      /* MANDATORY : Used to reinitialize the Current task */
      CURRENTtaskHandle = 0;
    }

  if (SYNCtaskHandle != 0) 
    {
      //dump_to_xv_log_file_with_date_and_time("SYNCTask not finished... I will stop it !");
      
      DAQmxErrChk (DAQmxStopTask(SYNCtaskHandle));
      DAQmxErrChk (DAQmxClearTask(SYNCtaskHandle));
      
      //dump_to_xv_log_file_with_date_and_time("SYNCTask stopped");
      
      /* MANDATORY : Used to reinitialize the Current task */
      SYNCtaskHandle = 0;
    } 

 Error:
  if( DAQmxFailed(error) ){
    DAQmxGetExtendedErrorInfo(errBuff,2048);
    dump_to_xv_log_file_with_date_and_time("DAQmx Error on Stop:\n %s\n",errBuff);
  }
  
  return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
// AI_data functions
/** Callback to read AI data into the AI_data array.

\param TaskHandle taskHandle
\param int32 everyNsamplesEventType
\param uInt32 nSamples
\param void *callbackData the parameters eventually passed to the callback function (I could think of passing a plot address here.

\author Francesco Mosconi
\version 19/04/07
*/
int32 CVICALLBACK task__sync_EveryNCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples,  void *callbackData)
{
  int32   error=0;
  char    errBuff[2048]={'\0'};
  int32       readAI = -1;
  unsigned long t, dts;
  static call = 0;
  static int toto = 1;
  call++;

  dts = get_my_uclocks_per_sec();

  if (updating_menu_state != 0) return D_O_K;

  /*********************************************/
  // DAQmx Read Code
  /*********************************************/

  DAQmxErrChk (DAQmxReadAnalogF64 (SYNCtaskHandle,nSamples,10.0,DAQmx_Val_GroupByScanNumber , AI_data, NUMBER_OF_AI_LINES*num_points_per_period, &readAI, NULL));
  if(SYNC_plot_flag)
    copy_sync_data_to_ops();

 Error:
  if( DAQmxFailed(error) ) {
    DAQmxGetExtendedErrorInfo(errBuff,2048);
    /*********************************************/
    // DAQmx Stop Code
    /*********************************************/
    if( SYNCtaskHandle!=0 ) {
      DAQmxStopTask(SYNCtaskHandle);
      DAQmxClearTask(SYNCtaskHandle);
      SYNCtaskHandle = 0;
    }
    if( CURRENTtaskHandle!=0 ) {
      DAQmxStopTask(CURRENTtaskHandle);
      DAQmxClearTask(CURRENTtaskHandle);
      CURRENTtaskHandle = 0;
    }
    dump_to_xv_log_file_with_date_and_time("DAQmx Error on everyN: %s\n",errBuff);
  }

  return 0;
}

int copy_sync_data_to_ops()
{
  register int i,j;

  //filling the datasets with the data from buffer
  for(i=0 ; i<NUMBER_OF_AI_LINES ; i++){
    if(sync_gop[i] == NULL) continue;

    for(j=0 ; j<num_points_per_period ; j++){
      sync_gop[i]->dat[0]->xd[j] = j;
      sync_gop[i]->dat[0]->yd[j] = AO_data[NUMBER_OF_LINES*j + i];
      sync_gop[i]->dat[1]->xd[j] = j;
      sync_gop[i]->dat[1]->yd[j] = AI_data[NUMBER_OF_AI_LINES*j + i];
    }

    sync_gop[i]->need_to_refresh = 1;
  }
  return D_O_K;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
// AO_data functions    
    
/** Merges lines data into AO_data.

The data individually stored in the structure lines (which may have different lengths, etc.)
are merged into a single, well-formatted pointer : AO_data, that will be sent to the card 
to be written on AO channels.

\author Adrien Meglio
\version 11/02/07
*/
int lines__merge_data_and_launch(l_s *lines, int duration, float64 frequency)
{
  int i,j;
  //    l_s *lines;
    
  if(updating_menu_state != 0)	return D_O_K;

  num_points_per_period = 0;
    
  /* Calculates the minimum number of points of AO_data */
  for (j = 0 ; j < NUMBER_OF_LINES ; j++)
    {
      if (lines[j].num_points > num_points_per_period) num_points_per_period = lines[j].num_points;
    }
    
  /* Checks that the writing frequency is compatible with the card */
  if (NUMBER_OF_LINES*num_points_per_period*frequency > NIDAQ_MAX_POINT_FREQUENCY)
    {
      dump_to_xv_log_file_with_date_and_time("Attention : you are attempting to write more points per second than the NIDAQ card can handle !\nReduce lines[i].num_points*FREQUENCY");
    }

  //dump_to_xv_log_file_with_date_and_time("Num points : %d",num_points_per_period);
    
  /* Generates AO_data */
  AO_data = (float64*) realloc(AO_data,NUMBER_OF_LINES*num_points_per_period*sizeof(float64));
  if (AO_data == NULL) return dump_to_xv_log_file_with_date_and_time("memory allocation error");

  //if(SYNC_flag){
    /* Generates AI_data */
    AI_data = (float64*) realloc(AI_data,NUMBER_OF_AI_LINES*num_points_per_period*sizeof(float64));
    if (AI_data == NULL) return dump_to_xv_log_file_with_date_and_time("memory allocation error");
    //}
    
  //dump_to_xv_log_file_with_date_and_time("AO data allocated");
    
  /* Merges the lines data into AO_data */
  for (i = 0 ; i < num_points_per_period ; i++)
    {
      for (j = 0 ; j < NUMBER_OF_LINES ; j++)
        {
	  /* Copies from lines.data to AO_data */
	  if (i < lines[j].num_points)
            {
	      AO_data[NUMBER_OF_LINES*i+j] = (float64) (lines[j].data[i])*lines[j].level*lines[j].status;
            }
	  /* If there is not enough points, just keep using the last point */
	  else AO_data[NUMBER_OF_LINES*i+j] = (float64) (lines[j].data[lines[j].num_points-1])*lines[j].level*lines[j].status;
        }        
    }
    
  //dump_to_xv_log_file_with_date_and_time("AO data updated");

  //  if(SYNC_flag)
  task__sync_create_and_launch(duration,frequency);
  //else
  //task__create_and_launch(duration,frequency);
       
  return D_O_K;
}    

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//

////////////////////////////////////////////////////////
//          USEFUL ONLY TO PASS TO NIDAQ_user         //
////////////////////////////////////////////////////////

/** Returns the max point frequency of the NIDAQ card.

\author Adrien Meglio
\version 11/02/07
*/
int IO__NIDAQ_max_frequency(float *max_frequency)
{
  if(updating_menu_state != 0)	return D_O_K;
    
  *max_frequency = NIDAQ_MAX_POINT_FREQUENCY;

  return D_O_K;
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// Housekeeping functions

int	nidaq_main(void)
{  
  if(updating_menu_state != 0) return D_O_K;

  //  menu_plug_in_main(); // Menu functions
    
  return D_O_K;
}

#endif




