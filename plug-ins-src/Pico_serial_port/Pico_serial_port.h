#ifndef _PICO_SERIAL_PORT_H_
#define _PICO_SERIAL_PORT_H_

#define READ_ONLY 0
#define WRITE_ONLY 1
#define WRITE_READ 2

# ifndef _PICO_SERIAL_PORT_C_
PXV_VAR(HANDLE, hCom);
PXV_VAR(COMMTIMEOUTS, lpTo);
PXV_VAR(COMMCONFIG, lpCC);
PXV_VAR(char, str_com[32]);

# endif

# ifdef _PICO_SERIAL_PORT_C_

HANDLE hCom;
COMMTIMEOUTS lpTo;
COMMCONFIG lpCC;
char str_com[32];

# endif
PXV_FUNC(int,Write_serial_port,(HANDLE hCom_port, char *ch, DWORD NumberOfBytesToWrite));
PXV_FUNC(int,Read_serial_port,(HANDLE hCom_port, char *ch, DWORD nNumberOfBytesToRead, unsigned long *lpNumberOfBytesWritten));
PXV_FUNC(int,talk_serial_port,(HANDLE hCom_port, char *command, int wait_answer, char * answer, DWORD NumberOfBytesToWrite, DWORD nNumberOfBytesToRead));
PXV_FUNC(int, CloseSerialPort,(HANDLE hCom));
PXV_FUNC(HANDLE,init_serial_port,(unsigned short port_number, int baudrate, int CTSRTS));
PXV_FUNC(int, write_command_on_serial_port,(void));
PXV_FUNC(int, read_on_serial_port,(void));
PXV_FUNC(int, close_serial_port,(void));
PXV_FUNC(MENU*, serialw_menu, (void));
PXV_FUNC(int, serialw32_main, (int argc, char **argv));
#endif

