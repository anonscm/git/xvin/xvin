/*
*    Plug-in program for simple serial comm 
*
*   
*      JF Allemand
*/
#ifndef _PICO_SERIAL_PORT_C_
#define _PICO_SERIAL_PORT_C_


# include "allegro.h"
# include "winalleg.h"
//# include <windowsx.h>
//# include "ctype.h"
# include "xvin.h"

#define BUILDING_PLUGINS_DLL

# include "Pico_serial_port.h"



// request definition


# define SET_ROT_REQUEST             1
# define SET_ZMAG_REQUEST            2
# define READ_ROT_REQUEST            3
# define READ_ZMAG_REQUEST           4
# define SET_ZMAG_REF_REQUEST        5
# define SET_ROT_REF_REQUEST         6
# define READ_TEMPERATURE_REQUEST    7
# define READ_FOCUS_REQUEST          8
# define SET_FOCUS_REQUEST           9


// third group is errors state
# define PICO_COM_ERROR      0x01000000 
# define ZMAG_TOP_LIMIT      0x02000000 
# define ZMAG_BOTTOM_LIMIT   0x02000000 
# define ZMAG_VCAP_LIMIT     0x04000000 
# define PIFOC_OVERFLOW      0x08000000 
# define PIFOC_OUT_OF_RANGE  0x10000000 

unsigned long t_rs = 0, dt_rs;


int request_pending = 0;
float request_parameter = 0;
int request_image_n = 0;


char *debug_file = "c:\\serial_debug.txt";
int debug = 0;
FILE *fp_debug = NULL;

float   pico_obj_position = 0, pico_Z_step = 0.1;
int pico_dac = 0;



unsigned long grab_start, grab_time;
int n_grab = 0;



float n_rota = 0, n_rot_offset = 0, n_rota_inst = 0;
float n_magnet_z = 0, n_magnet_z_inst = 0, n_magnet_offset = 0;
float v_rota = 1;  // in turns per sec
float v_mag = 2;   // in mm per sec
float absolute_maximum_zmag = 20;

UINT timeout = 200;  // ms

char last_answer_2[128];

int sent_cmd_and_read_answer(HANDLE hCom, char *Command, char *answer, DWORD n_answer, DWORD *n_written, DWORD *dwErrors, unsigned long *to);




#define SBAUD_300       1
#define SBAUD_600       2
#define SBAUD_1200      3
#define SBAUD_2400      4
#define SBAUD_4800      5
#define SBAUD_9600      6
#define SBAUD_19200     7
#define SBAUD_38400     8
#define SBAUD_57600     9
#define SBAUD_115200   10
#define SBAUD_230400   11
#define SBAUD_460800   12
#define SBAUD_921600   13




int generate_bauderate_value(int n)
{
  if (n ==  SBAUD_300)        return 300;
  else if (n == SBAUD_600)    return 600;
  else if (n == SBAUD_1200)   return 1200;
  else if (n == SBAUD_2400)   return 2400;
  else if (n == SBAUD_4800)   return 4800;
  else if (n == SBAUD_9600)   return 9600;
  else if (n == SBAUD_19200)  return 19200;
  else if (n == SBAUD_38400)  return 38400;
  else if (n == SBAUD_57600)  return 57600;
  else if (n == SBAUD_115200) return 115200;
  else if (n == SBAUD_230400) return 230400;
  else if (n == SBAUD_460800) return 460800;
  else if (n == SBAUD_921600) return 921600;
  else return 9600;
}

int retrieve_sbaud(int baudrate)
{
  if (baudrate == 300)        return  SBAUD_300;
  else if (baudrate == 600)    return  SBAUD_600;
  else if (baudrate == 1200)   return  SBAUD_1200;
  else if (baudrate == 2400)   return  SBAUD_2400;
  else if (baudrate == 4800)   return  SBAUD_4800;
  else if (baudrate == 9600)   return  SBAUD_9600;
  else if (baudrate == 19200)  return  SBAUD_19200;
  else if (baudrate == 38400)  return  SBAUD_38400;
  else if (baudrate == 57600)  return  SBAUD_57600;
  else if (baudrate == 115200) return  SBAUD_115200;
  else if (baudrate == 230400) return  SBAUD_230400;
  else if (baudrate == 460800) return  SBAUD_460800;
  else if (baudrate == 921600) return  SBAUD_921600;
  else return SBAUD_9600;
}


// http://msdn.microsoft.com/en-us/library/ms810467.aspx

HANDLE init_serial_port(unsigned short port_number, int baudrate, int RTSCTS)
{
    HANDLE hCom_port;
        
    sprintf( str_com, "\\\\.\\COM%d\0", port_number);
    hCom_port = CreateFile(str_com,GENERIC_READ|GENERIC_WRITE,0,NULL,
        OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,NULL);
    PurgeComm(hCom_port, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
    if (GetCommState( hCom_port, &lpCC.dcb) == 0) 
    {
        win_printf("GetCommState FAILED");
        return NULL;
    }    

    /* Initialisation des parametres par defaut */
    /*http://msdn.microsoft.com/library/default.asp?url=/library/en-us/devio/base/dcb_str.asp*/
    //win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.BaudRate = baudrate;//CBR_57600;//CBR_115200;
    //lpCC.dcb.BaudRate = CBR_115200;
    win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.ByteSize = 8;
    lpCC.dcb.StopBits = ONESTOPBIT;
    lpCC.dcb.Parity = NOPARITY;
    lpCC.dcb.fOutX = FALSE;
    lpCC.dcb.fInX = FALSE;


    if (RTSCTS == 0)
      {
	lpCC.dcb.fDtrControl = DTR_CONTROL_DISABLE;//DTR_CONTROL_HANDSHAKE or DTR_CONTROL_DISABLE;
	lpCC.dcb.fRtsControl = RTS_CONTROL_DISABLE;//RTS_CONTROL_HANDSHAKE
      }
    else
      {
	lpCC.dcb.fDtrControl = DTR_CONTROL_HANDSHAKE;// or DTR_CONTROL_DISABLE;
	lpCC.dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
	lpCC.dcb.fOutxCtsFlow = TRUE;
      }
 
 
    if (SetCommState( hCom_port, &lpCC.dcb )== 0) 
    {
        win_printf("SetCommState FAILED");
        return NULL;
    }    

    //Delay(60);

    if (GetCommTimeouts(hCom_port,&lpTo)== 0) 
    {
        win_printf("GetCommTimeouts FAILED");
        return NULL;
    }
        
    lpTo.ReadIntervalTimeout = 100;   // 100 ms max entre characters
    lpTo.ReadTotalTimeoutMultiplier = 1;
    lpTo.ReadTotalTimeoutConstant = 1;
    lpTo.WriteTotalTimeoutMultiplier = 1;
    lpTo.WriteTotalTimeoutConstant = 1;
    
    if (SetCommTimeouts(hCom_port,&lpTo)== 0) 
    {
        win_printf("SetCommTimeouts FAILED");
        return NULL;
    }    
    
    if (SetupComm(hCom_port,2048,2048) == 0) 
    {
        win_printf("Init Serial port %d FAILED",port_number+1);
        return NULL;
    }
    win_printf("Init Serial port %d OK",port_number);
    t_rs = my_uclock();
    dt_rs = get_my_uclocks_per_sec()/1000;
    return hCom_port;
}


int Write_serial_port(HANDLE handle, char *data, DWORD length)
{
  int success = 0;
  DWORD dwWritten = 0;
  OVERLAPPED o= {0};

  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  if (!WriteFile(handle, (LPCVOID)data, length, &dwWritten, &o))
    {
      if (GetLastError() == ERROR_IO_PENDING)
	if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	  if (GetOverlappedResult(handle, &o, &dwWritten, FALSE))
	    success = 1;
    }
  else    success = 1;
  if (dwWritten != length)     success = 0;
  CloseHandle(o.hEvent);
  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  if (dwWritten != length)    
	    fprintf (fp_debug,">pb writting %d char instead of %d\n",(int)dwWritten,(int)length);
	  fprintf (fp_debug,">\t %s (t = %g)\n",data,(double)(my_uclock()-t_rs)/dt_rs);
	  fclose(fp_debug);
	}
    }
  return (success == 0) ? -(dwWritten+1)  : dwWritten;
}

/*
int Write_serial_port(HANDLE handle, char *data, DWORD length)
{
  int success = 0, pos = 0;
  DWORD dwWritten = 0;
  OVERLAPPED o= {0};


  if (handle == NULL) return -2;

  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  for ( ;pos < (int)length; )
    {
      dwWritten = 0;
      if (!WriteFile(handle, (LPCVOID)(data + pos), length - pos, &dwWritten, &o))
	{
	  if (GetLastError() == ERROR_IO_PENDING)
	    if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	      if (GetOverlappedResult(handle, &o, &dwWritten, FALSE))
		success = 1;
	}
      else	success = 1;
      pos += dwWritten;
    }
  // if (dwWritten != length)     success = false;
  CloseHandle(o.hEvent);

  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  if (dwWritten != length)    
	    fprintf (fp_debug,">pb writting %d char instead of %d\n",(int)dwWritten,(int)length);
	  fprintf (fp_debug,">\t %s (t = %g)\n",data,(double)(my_uclock()-t_rs)/dt_rs);
	  fclose(fp_debug);
	}
    }


  return (success == 0) ? -1 : dwWritten;
}
*/


int ReadData(HANDLE handle, BYTE* data, DWORD length, DWORD* dwRead)
{
  int success = 0;
  OVERLAPPED o ={0};

  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  if (!ReadFile(handle, data, length, dwRead, &o))
    {
      if (GetLastError() == ERROR_IO_PENDING)
	if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	  success = 1;
      GetOverlappedResult(handle, &o, dwRead, FALSE);
    }
  else    success = 1;
  CloseHandle(o.hEvent);
  return success;
}


int check_for_rs232_error(HANDLE hComm)
{
  COMSTAT comStat;
  DWORD   dwErrors;
  int    fOOP, fOVERRUN, fPTO, fRXOVER, fRXPARITY, fTXFULL;
  int    fBREAK, fDNS, fFRAME, fIOE, fMODE;

    // Get and clear current errors on the port.
    if (!ClearCommError(hComm, &dwErrors, &comStat))
        // Report error in ClearCommError.
        return 0;

    // Get error flags.
    fDNS = dwErrors & CE_DNS;
    fIOE = dwErrors & CE_IOE;
    fOOP = dwErrors & CE_OOP;
    fPTO = dwErrors & CE_PTO;
    fMODE = dwErrors & CE_MODE;
    fBREAK = dwErrors & CE_BREAK;
    fFRAME = dwErrors & CE_FRAME;
    fRXOVER = dwErrors & CE_RXOVER;
    fTXFULL = dwErrors & CE_TXFULL;
    fOVERRUN = dwErrors & CE_OVERRUN;
    fRXPARITY = dwErrors & CE_RXPARITY;
    if (dwErrors)   win_printf("Com error %x",dwErrors);

    // COMSTAT structure contains information regarding
    // communications status.
    if (comStat.fCtsHold)
      win_printf(" Tx waiting for CTS signal");

    if (comStat.fDsrHold)
      win_printf(" Tx waiting for DSR signal");

    if (comStat.fRlsdHold)
      win_printf(" Tx waiting for RLSD signal");

    if (comStat.fXoffHold)
      win_printf(" Tx waiting, XOFF char rec'd");

    if (comStat.fXoffSent)
      win_printf(" Tx waiting, XOFF char sent");
    
    if (comStat.fEof)
      win_printf(" EOF character received");
    
    if (comStat.fTxim)
      win_printf(" Character waiting for Tx; char queued with TransmitCommChar");

    if (comStat.cbInQue)
      win_printf(" comStat.cbInQue bytes have been received, but not read");

    if (comStat.cbOutQue)
      win_printf(" comStat.cbOutQue bytes are awaiting transfer");
    return 0;
}

/*

int ReadData(HANDLE handle, char *data, DWORD length, DWORD* dwRead)
{
  int success = 0;
  OVERLAPPED o= {0};

  if (handle == NULL) return -2;
  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  if (!ReadFile(handle, data, length, dwRead, &o))
    {
      if (GetLastError() == ERROR_IO_PENDING)
	if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	  success = 1;
      GetOverlappedResult(handle, &o, dwRead, FALSE);
    }
  else
    success = 1;
  CloseHandle(o.hEvent);
  return success;
}
*/

int Read_serial_port(HANDLE handle, char *stuff, DWORD max_size, DWORD *dwRead)
{
  register int i, j;
  unsigned long t0;
  char lch[2];
  int timeout = 0;
  
  if (handle == NULL) return -2;


  t0 = get_my_uclocks_per_sec()/20; // we cannot wait more than 50 ms
  t0 += my_uclock();
  for (i = j = 0; j == 0 && i < max_size-1 && timeout == 0; )
    {
      *dwRead = 0;
      if (ReadData(handle, lch, 1, dwRead) == 0)
	{
	  stuff[i] = 0;
	  *dwRead = i;
	  if (debug)
	    {
	      fp_debug = fopen(debug_file,"a");
	      if (fp_debug != NULL)
		{
		  fprintf (fp_debug,"<-- ReadData error\n");
		  fprintf (fp_debug,"<\t %s (t = %g)\n",stuff,(double)(my_uclock()-t_rs)/dt_rs);
		  fclose(fp_debug);
		}
	    }
	  return -1;
	}
      /* 
      if (*dwRead < 1) 
	{
	  stuff[i] = 0;
	  *dwRead = i;
	  if (debug)
	    {
	      fp_debug = fopen(debug_file,"a");
	      if (fp_debug != NULL)
		{
		  fprintf (fp_debug,"<-- ReadData 0 char read\n");
		  fprintf (fp_debug,"<\t %s\n",stuff);
		  fclose(fp_debug);
		}
	    }
	  return -2;
	}
      */
      if (*dwRead == 1) 
	{
	  stuff[i] = lch[0];
	  j = (lch[0] == '\n') ? 1 : 0;
	  i++;
	}
      timeout = (t0 > my_uclock()) ? 0 : 1;
    }
  stuff[i] = 0;
  *dwRead = i;

  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  if (timeout) fprintf (fp_debug,"<\t timeout %s (t = %g)\n",stuff,(double)(my_uclock()-t_rs)/dt_rs);
	  else fprintf (fp_debug,"<\t %s (t = %g)\n",stuff,(double)(my_uclock()-t_rs)/dt_rs);
	  fclose(fp_debug);
	}
    }
  return (timeout) ? -2 : i;
}


int talk_serial_port(HANDLE hCom_port, char *command, int wait_answer, char *answer, DWORD NumberOfBytesToWrite, DWORD nNumberOfBytesToRead)
{
    int NumberOfBytesWrittenOnPort = -1;
    unsigned long NumberOfBytesWritten = -1;
    

    if (hCom == NULL) return win_printf_OK("No serial port init�");
    if (wait_answer != READ_ONLY)
    {
        NumberOfBytesWrittenOnPort = Write_serial_port(hCom_port, command, NumberOfBytesToWrite);
    }    
		
    if (wait_answer != WRITE_ONLY) 
    {
        Read_serial_port(hCom_port,answer,nNumberOfBytesToRead,&NumberOfBytesWritten);
    
    }	
    return 0;
}




int write_command_on_serial_port(void)
{
    static char Command[128]="test";
    int ret = 0;
    unsigned long t0;
    double dt;
    //char *test="\r";
    
    if(updating_menu_state != 0)	return D_O_K;
    
    win_scanf("Command to send? %s",&Command);
       
    strcat(Command,"\r");  
     //win_printf("deja cela %d\n Command %s",strlen(Command), Command);
    t0 = my_uclock();    
    ret = Write_serial_port(hCom,Command,strlen(Command));
    t0 = my_uclock() - t0;
    dt = 1000*(double)(t0)/get_my_uclocks_per_sec();
    win_printf("Command sent ret %d in %g mS",ret,dt);

    
   //sprintf(Command,"?nm\r");
   //strcat(Command,'\r');
    //win_printf("deja cela %d\n Command %s",strlen(Command), Command);
    
    //win_printf("%s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
    return D_O_K;
}


int read_on_serial_port(void)
{
    char Command[128];
    unsigned long lpNumberOfBytesWritten = 0;
    int ret = 0;
    DWORD nNumberOfBytesToRead = 127;
    unsigned long t0;
    
    if(updating_menu_state != 0)	return D_O_K;

    t0 = my_uclock();        
    //win_scanf("Command to send? %s",Command);
    ret = Read_serial_port(hCom,Command,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
    t0 = my_uclock() - t0;
    Command[lpNumberOfBytesWritten] = 0;

    win_printf("Read = %s \n ret %d lpNumberOfBytesRead = %d\n in %g mS",Command,ret,lpNumberOfBytesWritten,1000*(double)(t0)/get_my_uclocks_per_sec());

    //win_printf("command read = %s \n lpNumberOfBytesWritten = %d",Command,lpNumberOfBytesWritten);
    return D_O_K;
}




int write_read_serial_port(void)
{
    
    if(updating_menu_state != 0)	return D_O_K;
    if (hCom == NULL) return win_printf_OK("No serial port init�");
    
        write_command_on_serial_port();

        read_on_serial_port();


        return D_O_K;
}

int do_check_for_rs232_error(void)
{
    
    if(updating_menu_state != 0)	return D_O_K;
    if (hCom == NULL) return win_printf_OK("No serial port init�");

    check_for_rs232_error(hCom);
    return D_O_K;
}


int sent_cmd_and_read_answer(HANDLE hCom, char *Command, char *answer, DWORD n_answer, DWORD *n_written, DWORD *dwErrors, unsigned long *t0)
{
  int rets, ret, i, j;
  COMSTAT comStat;
  char l_command[128], ch[2], chtmp[128];
  DWORD len = 0;

  if (hCom == NULL || Command == NULL || answer == NULL || strlen(Command) < 1) return 1;

    for (i = 0, j = 1; i < 5 && j != 0; i++)
      {
	j = ClearCommError(hCom, dwErrors, &comStat);
	if (j && comStat.cbInQue)   Read_serial_port(hCom,chtmp,127,&len);
	if (j && comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	if (j && comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	if (j && comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	if (j && comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	if (j && comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	if (j && comStat.fEof)      my_set_window_title(" EOF character received");
	if (j && comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
	//if (j && comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	if (j && comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
      }


  for (i = 0; i < 126 && Command[i] != 0 && Command[i] != 13; i++)
    l_command[i] = Command[i]; // we copy until CR
  l_command[i++] = 13;         // we add it
  l_command[i] = 0;            // we end string
  if (t0) *t0 = my_uclock();
  /*
  for (i = 0; i < 126 && l_command[i] != 0; i++)
    {
      ch[0] = l_command[i];
      ch[1] = 0;
      rets = Write_serial_port(hCom,ch,1);
      if (rets <= 0)
	{
	  if (t0) *t0 = my_uclock() - *t0;
	  // Get and clear current errors on the port.
	  if (ClearCommError(hCom, dwErrors, &comStat)) 	
	    {
	      if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
	      return -2;
	    }
	  return -1;
	}
    }
  */

  rets = Write_serial_port(hCom,l_command,strlen(l_command));
  if (rets <= 0)
    {
      if (t0) *t0 = my_uclock() - *t0;
      // Get and clear current errors on the port.
      if (ClearCommError(hCom, dwErrors, &comStat)) 	
	{
	  if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
	  if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	  if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	  if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	  if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	  if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	  if (comStat.fEof)      my_set_window_title(" EOF character received");
	  if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
	  //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	  if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
	  return -2;
	}
      return -1;
    }

  *n_written = 0;
  ret = Read_serial_port(hCom,answer,n_answer,n_written);
  for  (; ret == 0; )
    ret = Read_serial_port(hCom,answer,n_answer,n_written);
  if (t0) *t0 = my_uclock() - *t0;
  answer[*n_written] = 0;
  strncpy(last_answer_2,answer,127);
  if (ret < 0)
    {
      // Get and clear current errors on the port.
      if (ClearCommError(hCom, dwErrors, &comStat)) 	
	{
	  if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
	  if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	  if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	  if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	  if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	  if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	  if (comStat.fEof)      my_set_window_title(" EOF character received");
	  if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
	  //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	  if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
	  return 2;
	}
      return 1;      
    }
  return 0;
}


int n_write_read_serial_port(void)
{
  pltreg *pr;
  O_p *op = NULL;
  d_s *ds = NULL, *ds2 = NULL;
  static char Command[128]="dac16?";
  static int ntimes = 1, i, j, n_ms = 1, d_plot = 1;
  unsigned long t0 = 0, tn = 0;
  double dt = 0, dtm = 0, dtmax = 0;
  char resu[128], *ch, lch[2], chtmp[128];
  DWORD len = 0;
  int ret = 0, n_er_r = 0, n_er_s = 0, rets = 0;
  unsigned long lpNumberOfBytesWritten = 0, nNumberOfBytesToRead = 128;
  COMSTAT comStat;
  DWORD   dwErrors;


    
  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot"));



  if (hCom == NULL) return win_printf_OK("No serial port init�");    
  for (ch = Command; *ch != 0; ch++) 
    if (*ch == '\r') *ch = 0;
  win_scanf("Command to send? %snumber of times %dNb of ms to sleep between exchange %dDraw timing plot %b\n"
	    ,&Command,&ntimes,&n_ms,&d_plot);
       
  strcat(Command,"\r");  
  
  if (d_plot)
    {
      op = create_and_attach_one_plot(pr, ntimes,ntimes, 0);
      if (op == NULL)    return win_printf("Cannot allocate plot");
      ds = op->dat[0];
      if (ds == NULL)    return win_printf("Cannot allocate plot");
      ds2 = create_and_attach_one_ds(op, ntimes, ntimes, 0);
      if (ds2 == NULL)    return win_printf("Cannot allocate plot");

      create_attach_select_y_un_to_op(op, IS_SECOND, 0 ,(float)1, -3, 0, "ms");
      set_plot_x_title(op, "N");
      set_plot_y_title(op, "Time");			

    }

    for(i = 0, dtm = dtmax = 0; i < ntimes; i++)
      {
	t0 = my_uclock();
	tn = t0 + (n_ms*get_my_uclocks_per_sec()/1000);
	for (j = 0, rets = 1; j < 126 && Command[j] != 0; j++)
	  {
	    lch[0] = Command[j];
	    lch[1] = 0;
	    if (ClearCommError(hCom, &dwErrors, &comStat)) // Report error in ClearCommError.
	      {
		if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
		if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
		if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
		if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
		if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
		if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
		if (comStat.fEof)      my_set_window_title(" EOF character received");
		if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
		//if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
		if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
		display_title_message("%d before command clearerror rets %d in %g mS error %d",i,rets,dt, (int)dwErrors);
	      }
	    rets = Write_serial_port(hCom,lch,1);
	    if (rets <= 0)
	      {
		// Get and clear current errors on the port.
		if (ClearCommError(hCom, &dwErrors, &comStat)) // Report error in ClearCommError.
		  {
		    if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
		    if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
		    if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
		    if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
		    if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
		    if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
		    if (comStat.fEof)      my_set_window_title(" EOF character received");
		    if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
		    //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
		    if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
		    display_title_message("%d command sent failed rets %d in %g mS error %d",i,rets,dt, (int)dwErrors);
		  }
	      }
	  }
	if (rets <= 0)
	  {
	    t0 = my_uclock() - t0;
	    dt = 1000*(double)(t0)/get_my_uclocks_per_sec();
	    dtm += dt;
	    if (dt > dtmax) dtmax = dt;
	    if (ds != NULL)
	      {
		ds->yd[i] = dt;
		ds->xd[i] = ds2->xd[i] = i;
		ds2->yd[i] = rets;
	      }
	    //purge_com();
	    // Get and clear current errors on the port.
	    if (ClearCommError(hCom, &dwErrors, &comStat)) // Report error in ClearCommError.
	      {
		if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
		if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
		if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
		if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
		if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
		if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
		if (comStat.fEof)      my_set_window_title(" EOF character received");
		if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
		//if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
		if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
		display_title_message("%d command sent failed rets %d in %g mS error %d",i,rets,dt, (int)dwErrors);
	      }
	    n_er_s++;
	    continue;
	  }
	
	ret = Read_serial_port(hCom,resu,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
	for  (; ret == 0; )
	  ret = Read_serial_port(hCom,resu,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
	t0 = my_uclock() - t0;
	dt = 1000*(double)(t0)/get_my_uclocks_per_sec();
	resu[lpNumberOfBytesWritten] = 0;
	if (ds != NULL)
	  {
	    ds->yd[i] = dt;
	    ds->xd[i] = i;
	    ds->xd[i] = ds2->xd[i] = i;
	    ds2->yd[i] = 0;
	  }
	dtm += dt;
	if (dt > dtmax) dtmax = dt;
	if (ret < 0)
	  {
	    // Get and clear current errors on the port.
	    if (ClearCommError(hCom, &dwErrors, &comStat))	      // Report error in ClearCommError.
	      {
		if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
		if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
		if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
		if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
		if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
		if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
		if (comStat.fEof)      my_set_window_title(" EOF character received");
		if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
		//if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
		if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
		display_title_message("%d command read failed = %s  ret %d Number Of Bytes = %d in %g mS Err %d",i,resu,ret,lpNumberOfBytesWritten,dt, (int)dwErrors);
	      }
	    ds2->yd[i] = 2;
	    purge_com();
	    n_er_r++;
	    continue;
	  }
	for ( ; my_uclock() < tn; );
	if ((ds != NULL) && ((i%1000) == 0))
	  {
	      op->need_to_refresh = 1;
	      refresh_plot(pr, pr->n_op-1);		
	  }	
      }
    dtm /= ntimes;
    win_printf("%d command read = %s \n ret %d Number Of Bytes  = %d\n"
	       "in %g mS in avg %g max\n %d read er %d sent er"
	       ,i,resu,ret,lpNumberOfBytesWritten,dtm, dtmax,n_er_r,n_er_s);
    refresh_plot(pr, pr->n_op-1);		
    return D_O_K;
}

int purge_com(void)
{
   if (hCom == NULL) return 1; 
  return PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
}
int init_port(void)
{
  int i;
  static int port_number = 1, sbaud = 0, hand = 0;
  char command[128], answer[128], *gr;
  int  ret, iret;
  unsigned long len = 0;
  






  if (hCom != NULL) return 0; 
  i = win_scanf("Port number ?%5d\nDump in and out in a debug file\n"
		"No %R yes %r\n"
		"Baudrate 300->%R 600->%r 1200->%r 2400->%r 4800->%r 9600->%r 19200->%r\n"
		"38400->%r 57600->%r 115200->%r 230400->%r 460800->%r 921600->%r\n"
		"HandShaking None %R Cts/Rts %r"
		,&port_number, &debug,&sbaud,&hand);
  if (i == CANCEL) return D_O_K;
  
  hCom = init_serial_port(port_number, generate_bauderate_value(sbaud+1), hand);
  if (hCom == NULL) return 1;
  if (debug)
    {
      fp_debug = fopen(debug_file,"w");
      if (fp_debug != NULL) fclose(fp_debug);
    }
  purge_com();

  sprintf(command,"echo=0\r");
  if (Write_serial_port(hCom,command,strlen(command)) < 0)
    win_printf("error timeout in write echo");
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  win_printf("After readind response to echo");
  gr = strstr(answer,"Echo off");
  return (gr != NULL) ? 0 : 1;
}

int my_sleep(long ms)
{
  unsigned long t0;

  for (t0 = my_uclock() + ms*(get_my_uclocks_per_sec()/1000); my_uclock() < t0; );
  return 0;
}

int grab_plot_from_serial_port(void)
{
  pltreg *pr;
  O_p *op;
  d_s *ds, *ds2 = NULL;
  static int axe = 0;
  int  i, nx = 400, a = 0, b = 0, c = 0, ret = 0;
  char Command[128], bug[256], answer[128];
  unsigned long lpNumberOfBytesWritten = 0;
  DWORD nNumberOfBytesToRead = 127;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot"));

  i = win_scanf("choose the axis to record transitory\n"
		"Rot %R Zmag %r focus %r\n",&axe);
  if (i == CANCEL) return D_O_K;


  bug[0] = 0; answer[0] = 0;
  op = create_and_attach_one_plot(pr, nx,nx, 0);
  if (op == NULL)    return win_printf("Cannot allocate plot");
  ds = op->dat[0];
  if (ds == NULL)    return win_printf("Cannot allocate plot");
  if (axe < 2)
    {
      ds2 = create_and_attach_one_ds(op, nx, nx, 0);
      if (ds2 == NULL)    return win_printf("Cannot allocate plot");
    }


  if (axe == 0) sprintf(Command,"rtran=0\r\n");
  else if (axe == 1) sprintf(Command,"ztran=0\r\n");
  else if (axe == 2) sprintf(Command,"ftran=0\r\n");
  i = 0;
  Write_serial_port(hCom,Command,9);
  ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
  for  (; ret <= 0; )
    ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);

  if (axe == 0) sprintf(Command,"rtran?\r\n");
  else if (axe == 1) sprintf(Command,"ztran?\r\n");
  else if (axe == 2) sprintf(Command,"ftran?\r\n");



  //win_printf("0 %s",answer);
  for (i = 0; i < nx; )
    {
      Write_serial_port(hCom,Command,8);
      ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
      for  (; ret <= 0; )
	ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
      if (axe < 2)
	{ 
	  if (sscanf(answer,"%d\t%d\t%d",&a,&b,&c) == 3)
	    {
	      ds->xd[i] = ds2->xd[i] = a;
	      ds->yd[i] = b;
	      ds2->yd[i] = c;
	    }
	  else sprintf(bug,"line %d >%s<len \n",i,answer); 
	  i++;
	  if (i%16 == 0) display_title_message("reading %d >%d %d %d",i,a,b,c);
	}
      else
	{ 
	  if (sscanf(answer,"%d\t%d",&a,&b) == 2)
	    {
	      ds->xd[i] = a;
	      ds->yd[i] = b;
	    }
	  else sprintf(bug,"line %d >%s<len \n",i,answer); 
	  i++;
	  if (i%16 == 0) display_title_message("reading %d >%d %d",i,a,b);
	}

    }

  refresh_plot(pr, pr->n_op-1);		
  return D_O_K;
}




int grab_objective_plot_from_serial_port(void)
{
  pltreg *pr;
  O_p *op;
  d_s *ds;
  //static int  delay = 2;
  static float zmin = 10, zmax = 240, zstep = 10;
  int  i, ret = 0, over = 0; //, nx = 512, a = 0, b = 0, c = 0, set = 0;
  float fer = 0, foc = 0, z = 0, delay = 0.5;
  char Command[128], bug[256], answer[128], *gr = NULL;
  unsigned long lpNumberOfBytesWritten = 0;
  DWORD nNumberOfBytesToRead = 127;
  unsigned long t0;
  DWORD dwErrors;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot"));

  i = win_scanf("Up and down objective modulation\n"
		"minimum Z value %6f\n"
		"maximum z value %6f\n"
		"step size %6f\n"
		"delay in second at each step %6f\n",&zmin,&zmax,&zstep,&delay);
  if (i == CANCEL) return D_O_K;


  bug[0] = 0; answer[0] = 0;
  op = create_and_attach_one_plot(pr, 256, 256, 0);
  if (op == NULL)    return win_printf("Cannot allocate plot");
  ds = op->dat[0];
  if (ds == NULL)    return win_printf("Cannot allocate plot");
  /*
  ds2 = create_and_attach_one_ds(op, 256, 256, 0);
  if (ds2 == NULL)    return win_printf("Cannot allocate plot");
  */

  snprintf(Command,128,"focus=%g\r\n",zmin);
  /*
  t0 = my_uclock();
  Write_serial_port(hCom,Command,strlen(Command));
  t0 = my_uclock() - t0;
  win_printf("%d char sent in %g ms\n",strlen(Command),1000*(double)(t0)/get_my_uclocks_per_sec());
  t0 = my_uclock();
  ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
  for  (; ret <= 0; )
    ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
  t0 = my_uclock() - t0;
  win_printf("%d char recieved in %g ms\n%s\n",(int)lpNumberOfBytesWritten
	     ,1000*(double)(t0)/get_my_uclocks_per_sec(),answer);

  */
  ret = sent_cmd_and_read_answer(hCom, Command, answer, 127, &lpNumberOfBytesWritten, &dwErrors, NULL);


  //win_printf("0 %s",answer);
  ds->nx = ds->ny = 0;
  //ds2->nx = ds2->ny = 0;
  for (z = zmin; z <= zmax; z += zstep)
    {
      snprintf(Command,128,"focus=%g\r\n",z);
      ret = sent_cmd_and_read_answer(hCom, Command, answer, 127, &lpNumberOfBytesWritten, &dwErrors, NULL);
      if (ret >= 0)
	{
	  foc = -1;
	  if (sscanf(answer,"Focus  overflow at %f",&foc) == 1)  over = 1;
	  else if (sscanf(answer,"Focus at %f",&foc) == 1) over = 0;
	  my_sleep((int)(delay*1000));
	  //for (t0 = my_uclock()+(get_my_uclocks_per_sec()); my_uclock() < t0; );
	  snprintf(Command,128,"A3?\r\n");
	  ret = sent_cmd_and_read_answer(hCom, Command, answer, 127, &lpNumberOfBytesWritten, &dwErrors, NULL);
	  if (ret >= 0)
	    {
	      fer = -1;
	      sscanf(answer,"Focus error = %f",&fer);
	      add_new_point_to_ds(ds,foc,fer);
	    }
	}
      op->need_to_refresh = 1;
      refresh_plot(pr, pr->n_op-1);		
    }


  for (z = zmax; z >= zmin; z -= zstep)
    {
      snprintf(Command,128,"focus=%g\r\n",z);
      ret = sent_cmd_and_read_answer(hCom, Command, answer, 127, &lpNumberOfBytesWritten, &dwErrors, NULL);
      if (ret >= 0)
	{
	  foc = -1;
	  if (sscanf(answer,"Focus  overflow at %f",&foc) == 1)  over = 1;
	  else if (sscanf(answer,"Focus at %f",&foc) == 1) over = 0;
	  my_sleep((int)(delay*1000));
	  //for (t0 = my_uclock()+(get_my_uclocks_per_sec()); my_uclock() < t0; );
	  snprintf(Command,128,"A3?\r\n");
	  ret = sent_cmd_and_read_answer(hCom, Command, answer, 127, &lpNumberOfBytesWritten, &dwErrors, NULL);
	  if (ret >= 0)
	    {
	      fer = -1;
	      sscanf(answer,"Focus error = %f",&fer);
	      add_new_point_to_ds(ds,foc,fer);
	    }
	}
      op->need_to_refresh = 1;
      refresh_plot(pr, pr->n_op-1);		
    }


  refresh_plot(pr, pr->n_op-1);		
  return D_O_K;
}


        
int CloseSerialPort(HANDLE hCom)
{
    if (CloseHandle(hCom) == 0) return win_printf("Close Handle FAILED!");
    return D_O_K;
}

int close_serial_port(void)
{
        if(updating_menu_state != 0)	return D_O_K;
        CloseSerialPort(hCom);
        return D_O_K;
}




int	set_rot_value(float rot)
{
  char command[128], answer[128];
  unsigned long len = 0;
  int roti, set = -1, ret;
  //unsigned long t0;
  DWORD dwErrors;

  roti = (int)(0.5 + 8000*rot);
  snprintf(command,128,"roti=%d&%d\r",roti,roti);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return (ret -5);
  if (sscanf(answer,"Roti aiming %d",&set) != 1)  return -1;
  if (roti == set)    n_rota = rot;
  return (roti == set) ? 0 : 1;
}


int	set_rot_ref_value(float rot)
{
  char command[128], answer[128];
  unsigned long len = 0;
  int ret;
  float set = -1;
  //unsigned long t0;
  DWORD dwErrors;


  snprintf(command,128,"rhome=%f\r",rot);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return (ret -5);
  if (sscanf(answer,"Rot home: %f",&set) != 1)  return -1;
  if (rot == set)    n_rota = rot;
  return (fabs(rot - set) > 0.01) ? 0 : 1;
}



float read_rot_value(void)
{
  char command[128], answer[128];
  unsigned long len = 0;
  int set = 0x7FFFFFFF, ret;
  float rot = 0;
  //unsigned long t0;
  DWORD dwErrors;

  snprintf(command,128,"roti?\r");
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return __FLT_MAX__;
  if (sscanf(answer,"Roti = %d",&set) != 1)  return __FLT_MAX__;
  if (set == 0x7FFFFFFF)  return __FLT_MAX__;
  rot = ((float)(set+4))/8000;
  return n_rota = rot;//n_rota - n_rot_offset;
}



int   set_magnet_z_value(float pos)
{
  char command[128], answer[128];
  unsigned long len = 0;
  int zmagi, set = -1, ret;
  //unsigned long t0;
  DWORD dwErrors;

  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos; 
  zmagi = (int)(0.5 + 8000*pos);
  snprintf(command,128,"zmagi=%d&%d\r",zmagi,zmagi);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Zmag aiming %d",&set) != 1)  return -3;
  if (zmagi == set) n_magnet_z = pos;
  return (zmagi == set) ? 0 : 3;
}

float read_magnet_z_value(void)
{
  char command[128], answer[128];
  unsigned long len = 0;
  int  set = 0x7FFFFFFF, ret;
  //unsigned long t0;
  DWORD dwErrors;

  snprintf(command,128,"zmagi?\r");
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return __FLT_MAX__;
  if (sscanf(answer,"Zmag = %d",&set) != 1)  return __FLT_MAX__;
  if (set == 0x7FFFFFFF)  return __FLT_MAX__;
  n_magnet_z = ((float)(set+4))/8000;
  return n_magnet_z;	
}

int read_magnet_z_value_and_status(float *z, float *vcap, int *limit, int *vcap_servo)
{
  char command[128], answer[128];
  unsigned long len = 0;
  int  set = -1, cap = -1, ret = 0, nr = 0;
  //unsigned long t0;
  DWORD dwErrors;

  snprintf(command,128,"zmagi?\r");
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (vcap_servo) *vcap_servo = 0;
  if (limit) *limit = 0;
  if (vcap) *vcap = 0;
  if (z) *z = 0;
  nr = sscanf(answer,"Zmag = %d",&set);
  if (nr == 1)
    {
      nr = sscanf(answer,"Zmag = %d Vcap = %d",&set,&cap);
      if (nr == 2)
	{
	  if (vcap_servo) *vcap_servo = 0;
	}
      else 
	{
	  nr = sscanf(answer,"Zmag = %d Vservo = %d",&set,&cap); 
	  if (nr == 2 && vcap_servo != NULL) *vcap_servo = 1;
	}
    }
  else ret = 3;
  if (strstr(answer,"NL") != NULL && limit != NULL) *limit = -1; 
  if (strstr(answer,"PL") != NULL && limit != NULL) *limit = 1; 
  if (nr > 0) 
    {
      n_magnet_z = ((float)(set+4))/8000;
      if (z) *z = n_magnet_z;
    }
  if ((nr > 1) && (vcap != NULL)) 
      *vcap = ((float)(cap))/1000;
  return ret;	
}



int set_motors_speed(float v)
{
  char command[128], answer[128];
  unsigned long len = 0;
  int vzi, set = -1, ret;
  //unsigned long t0;
  DWORD dwErrors;

  // 8 pulses per ms => 1 mm per sec
  v_rota = v;
  vzi = (int)(0.5 + 8*fabs(v));
  snprintf(command,128,"rpid7=%d\r",vzi);
  /*  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Rot vmax %d",&set) != 1) return 1;
  return (vzi == set) ? 0 : 1;
}
float  get_motors_speed()
{
  char command[128], answer[128];
  unsigned long len = 0;
  int  set = -1, ret;
  //unsigned long t0;
  DWORD dwErrors;

  // 8 pulses per ms => 1 mm per sec
  snprintf(command,128,"rpid7?\r");
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return __FLT_MAX__;
  if (sscanf(answer,"Rot vmax %d",&set) != 1) return __FLT_MAX__;
  v_rota = (float)set/8;
    return v_rota;
}



int set_zmag_motor_speed(float v)
{
  char command[128], answer[128];
  unsigned long len = 0;
  int vzi, set = -1, ret;
  //unsigned long t0;
  DWORD dwErrors;

  // 8 pulses per ms => 1 mm per sec
  v_mag = v;
  vzi = (int)(0.5 + 8*fabs(v));
  snprintf(command,128,"zpid7=%d\r",vzi);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Zmag vmax %d",&set) != 1) return 1;
  return (vzi == set) ? 0 : 1;
}
float  get_zmag_motor_speed()
{
  char command[128], answer[128];
  unsigned long len = 0;
  int  set = -1, ret;
  //unsigned long t0;
  DWORD dwErrors;

  // 8 pulses per ms => 1 mm per sec
  snprintf(command,128,"zpid7?\r");
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return __FLT_MAX__;
  if (sscanf(answer,"Zmag vmax %d",&set) != 1) return __FLT_MAX__;
  v_mag = (float)set/8;
  return v_mag;
}



int set_zmag_motor_pid(int num, int val)
{
  char command[128], answer[128], ison[32];
  unsigned long len = 0;
  int  set = -1, ret, iret;
  //unsigned long t0;
  DWORD dwErrors;

  if (num < 0 || num > 9) return 1;
  if (num < 3 && (val < 0 || val > 15))                   return 2;
  else if (num == 2 && (val < 0 || val > 255))            return 3;
  else if (num > 4 && num < 7 && (val < 0 || val > 128)) return 4;
  snprintf(command,128,"zpid%d=%d\r",num,val);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  iret = 0;
  if (num == 0)       iret = sscanf(answer,"Zmag P %d",&set);
  else if (num == 1)  iret = sscanf(answer,"Zmag I %d",&set);
  else if (num == 2)  iret = sscanf(answer,"Zmag D %d",&set);
  else if (num == 3)  iret = sscanf(answer,"zmax PWM %d",&set);
  else if (num == 4)  iret = sscanf(answer,"zmax Range %d",&set);
  else if (num == 5)  iret = sscanf(answer,"Zmag vP %d",&set);
  else if (num == 6)  iret = sscanf(answer,"Zmag vI %d",&set);
  else if (num == 7)  iret = sscanf(answer,"Zmag vmax %d",&set);
  else if (num == 8)  
    {
      iret = sscanf(answer,"Zmag PID %s",ison); 
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  else if (num == 9)  
    {
      iret = sscanf(answer,"brake %s",ison);
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  if (iret == 0) return 5;
  return (val == set) ? 0 : 1;
}
int  get_zmag_motor_pid(int num)
{
  char command[128], answer[128], ison[32];
  unsigned long len = 0;
  int  set = -1, ret, iret = 0;
  //unsigned long t0;
  DWORD dwErrors;

  if (num < 0 || num > 9) return -1;
  snprintf(command,128,"zpid%d?\r",num);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;

  if (num == 0)       iret = sscanf(answer,"Zmag P %d",&set);
  else if (num == 1)  iret = sscanf(answer,"Zmag I %d",&set);
  else if (num == 2)  iret = sscanf(answer,"Zmag D %d",&set);
  else if (num == 3)  iret = sscanf(answer,"zmax PWM %d",&set);
  else if (num == 4)  iret = sscanf(answer,"zmax Range %d",&set);
  else if (num == 5)  iret = sscanf(answer,"Zmag vP %d",&set);
  else if (num == 6)  iret = sscanf(answer,"Zmag vI %d",&set);
  else if (num == 7)  iret = sscanf(answer,"Zmag vmax %d",&set);
  else if (num == 8)  
    {
      iret = sscanf(answer,"Zmag PID %s",ison); 
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  else if (num == 9)  
    {
      iret = sscanf(answer,"brake %s",ison);
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  if (iret == 0) return -5;
  return set;
}



int set_rotation_motor_pid(int num, int val)
{
  char command[128], answer[128], ison[32];
  unsigned long len = 0;
  int  set = -1, ret, iret;
  //unsigned long t0;
  DWORD dwErrors;

  if (num < 0 || num > 9) return 1;
  if (num < 3 && (val < 0 || val > 15))                   return 2;
  else if (num == 2 && (val < 0 || val > 255))            return 3;
  else if (num > 4 && num < 7 && (val < 0 || val > 128)) return 4;
  snprintf(command,128,"rpid%d=%d\r",num,val);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;

  iret = 0;
  if (num == 0)       iret = sscanf(answer,"Rot P %d",&set);
  else if (num == 1)  iret = sscanf(answer,"Rot I %d",&set);
  else if (num == 2)  iret = sscanf(answer,"Rot D %d",&set);
  else if (num == 3)  iret = sscanf(answer,"max PWM %d",&set);
  else if (num == 4)  iret = sscanf(answer,"max Range %d",&set);
  else if (num == 5)  iret = sscanf(answer,"Rot vP %d",&set);
  else if (num == 6)  iret = sscanf(answer,"Rot vI %d",&set);
  else if (num == 7)  iret = sscanf(answer,"Rot vmax %d",&set);
  else if (num == 8)  
    {
      iret = sscanf(answer,"Rot PID %s",ison); 
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  else if (num == 9)  
    {
      iret = sscanf(answer,"brake %s",ison);
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  if (iret == 0) return 5;
  return (val == set) ? 0 : 1;
}
int  get_rotation_motor_pid(int num)
{
  char command[128], answer[128], ison[32];
  unsigned long len = 0;
  int  set = -1, ret, iret = 0;
  //unsigned long t0;
  DWORD dwErrors;

  if (num < 0 || num > 9) return -1;
  snprintf(command,128,"rpid%d?\r",num);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (num == 0)       iret = sscanf(answer,"Rot P %d",&set);
  else if (num == 1)  iret = sscanf(answer,"Rot I %d",&set);
  else if (num == 2)  iret = sscanf(answer,"Rot D %d",&set);
  else if (num == 3)  iret = sscanf(answer,"max PWM %d",&set);
  else if (num == 4)  iret = sscanf(answer,"max Range %d",&set);
  else if (num == 5)  iret = sscanf(answer,"Rot vP %d",&set);
  else if (num == 6)  iret = sscanf(answer,"Rot vI %d",&set);
  else if (num == 7)  iret = sscanf(answer,"Rot vmax %d",&set);
  else if (num == 8)  
    {
      iret = sscanf(answer,"Rot PID %s",ison); 
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  else if (num == 9)  
    {
      iret = sscanf(answer,"brake %s",ison);
      set = (strstr(ison,"On")) ? 1 : 0;
    }
  if (iret == 0) return -5;
  return set;
}



int set_magnet_z_ref_value(float pos)  /* in mm */
{
  char command[128], answer[128];
  unsigned long len = 0;
  int  ret;
  float set = -1;
  //unsigned long t0;
  DWORD dwErrors;

  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos; 
  snprintf(command,128,"zhome=%f\r",pos);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Zmag home: %f",&set) != 1) return 1;
  n_magnet_z = pos;
  return (fabs(pos - set) > 0.01) ? 0 : 1;
}

int   set_z_origin(float pos, int dir)
{
  char command[128], answer[128];
  unsigned long len = 0;
  int ret;
  float  set = -1;
  //unsigned long t0;
  DWORD dwErrors;

  snprintf(command,128,"zori%d=%g\r",(dir==0)?0:1,pos);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Zmag origin: %f",&set) != 1) return 1;
  return (pos == set) ? 0 : 1;
}


int   set_Vcap_min(float volts)
{
  char command[128], answer[128];
  unsigned long len = 0;
  int ret = 0;
  float  set = -1;
  //unsigned long t0;
  DWORD dwErrors;

  snprintf(command,128,"zvcap=%f\r",volts);
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0;*/
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Vcap_min %f",&set) != 1) ret = 1;
  else if (fabs(set - volts) > 0.010) ret = 1;
  else ret = 0;
  return ret;
}

float  get_Vcap_min()
{
  char command[128], answer[128];
  unsigned long len = 0;
  int ret;
  //unsigned long t0;
  float set = -1;
  DWORD dwErrors;
  
  snprintf(command,128,"zvcap?\r");
  /*Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0, t0 = my_uclock()+(get_my_uclocks_per_sec()/100); my_uclock() < t0 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);  // we wait 10ms at most
    answer[len] = 0; */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret != 0) return ret;
  if (sscanf(answer,"Vcap_min %f",&set) != 1) return -1;
  else return set;
}


# ifdef OLD

int	set_rot_value(float rot)
{
  char command[128], answer[128], *gr;
  unsigned long len = 0;
  int roti, set = -1, ret, iret;
  DWORD dwErrors;

  purge_com();
  roti = (int)(0.5 + 8000*rot);

  sprintf(command,"roti=%d&%d\r",roti,roti);
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (ret == 0)
    {
      gr = strstr(answer," aiming ");
      if (gr != NULL) sscanf(gr," aiming %d",&set);
      //win_printf("answer %s\ndac %d",answer,set);
      if (roti != set)
	{
	  set_window_title(answer);
	  Write_serial_port(hCom,command,strlen(command));
	  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
	    ret = Read_serial_port(hCom, answer, 127, &len);
	  answer[len] = 0;
	  gr = strstr(answer," aiming ");
	  if (gr != NULL) sscanf(gr," aiming %d",&set);
	}
      //if (len) set_window_title(answer);
      if (roti == set)    n_rota = rot;
    }
  return (roti == set) ? 0 : 1;
}


int	set_rot_ref_value(float rot)
{
  char command[128], answer[128], *gr;
  unsigned long len = 0;
  int ret, iret;
  float set = -1;
  DWORD dwErrors;

  //  purge_com();
  //n_rota = rot + n_rot_offset;
  //roti = (int)(0.5 + 8000*n_rota);

  sprintf(command,"rhome=%f\r",rot);
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  gr = strstr(answer," home: ");
  if (gr != NULL) sscanf(gr," home: %f",&set);
  //win_printf("answer %s\ndac %d",answer,set);
  if (fabs(rot - set) > 0.01)
    {
      Write_serial_port(hCom,command,strlen(command));
      for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
	ret = Read_serial_port(hCom, answer, 127, &len);
      answer[len] = 0;
      gr = strstr(answer," home: ");
      if (gr != NULL) sscanf(gr," home %f",&set);
    }
  //if (len) set_window_title(answer);
  return (fabs(rot - set) > 0.01) ? 0 : 1;
}



float read_rot_value(void)
{
  char command[128], answer[128], *gr;
  unsigned long len = 0;
  int set = -1, ret, iret;
  DWORD   dwErrors;

  //  purge_com();
  sprintf(command,"roti?\r");
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  gr = strstr(answer," = ");
  if (gr == NULL) 
    {
      Write_serial_port(hCom,command,strlen(command));
      for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
	ret = Read_serial_port(hCom, answer, 127, &len);
      answer[len] = 0;
      gr = strstr(answer," = ");
    }
  if (gr != NULL) 
    {
      sscanf(gr," = %d",&set);
      n_rota = ((float)(set+4))/8000;
    }
  //win_printf("answer %s\ndac %d",answer,set);
  //if (len) set_window_title(answer);
  return n_rota;// - n_rot_offset;
}



int   set_magnet_z_value(float pos)
{
  char command[128], answer[128], *gr;
  unsigned long len = 0;
  int zmagi, set = -1, ret, iret;
  DWORD   dwErrors;

  //  purge_com();
  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos; 
  zmagi = (int)(0.5 + 1000*pos);
  zmagi *= 8;
  sprintf(command,"zmagi=%d&%d\r",zmagi,zmagi);
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  gr = strstr(answer," aiming ");
  if (gr != NULL) sscanf(gr," aiming %d",&set);

  if (zmagi != set)
    {
      set_window_title(answer);
      Write_serial_port(hCom,command,strlen(command));
      for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
	ret = Read_serial_port(hCom, answer, 127, &len);
      answer[len] = 0;
      gr = strstr(answer," aiming ");
      if (gr != NULL) sscanf(gr," aiming %d",&set);
    }
  //win_printf("answer %s\ndac %d",answer,set);
  if (zmagi == set) n_magnet_z = pos;
  //if (len) set_window_title(answer);
  return (zmagi == set) ? 0 : 1;
}

float read_magnet_z_value(void)
{
  char command[128], answer[128], text[256], *gr;
  unsigned long len = 0;
  int  set = -1, ret, iret;
  DWORD   dwErrors;

  //  purge_com();
  sprintf(command,"zmagi?\r");
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  gr = strstr(answer," = ");
  if (gr == NULL) 
    {
      Write_serial_port(hCom,command,strlen(command));
      for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
	ret = Read_serial_port(hCom, answer, 127, &len);
      answer[len] = 0;
      gr = strstr(answer," = ");
    }
  if (gr != NULL) 
    {
      sscanf(gr," = %d",&set);
      n_magnet_z = ((float)(set+4))/8000;
    }
  //win_printf("answer %s\ndac %d",answer,set);
  
  if (len) //set_window_title(answer);
    {
      sprintf (text,"read Zmag %g",n_magnet_z);
      //set_window_title(text);
    }
  return n_magnet_z;	
}


int set_motors_speed(float v)
{
  char command[128], answer[128], *gr;
  unsigned long len = 0;
  int vzi, set = -1, ret, iret;
  DWORD   dwErrors;

  // 8 pulses per ms => 1 mm per sec
  //  purge_com();
  v_rota = v;
  vzi = (int)(0.5 + 8*fabs(v));
  sprintf(command,"rpid7=%d\r",vzi);
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  gr = strstr(answer," vmax ");
  if (gr != NULL) sscanf(gr," vmax %d",&set);
  //win_printf("answer %s\ndac %d",answer,set);
  return (vzi == set) ? 0 : 1;
}
float  get_motors_speed()
{
  char command[128], answer[128], *gr;
  unsigned long len = 0;
  int  set = -1, ret, iret;
  DWORD   dwErrors;

  // 8 pulses per ms => 1 mm per sec
  //purge_com();
  sprintf(command,"rpid7?\r");
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  gr = strstr(answer," vmax ");
  if (gr != NULL) 
    {
      sscanf(gr," vmax %d",&set);
      v_rota = (float)set/8;
    }
  return v_rota;
}

int set_magnet_z_ref_value(float pos)  /* in mm */
{
  char command[128], answer[128], *gr;
  unsigned long len = 0;
  int  ret, iret;
  float set = -1;
  DWORD   dwErrors;

  //  purge_com();
  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos; 
  sprintf(command,"zhome=%f\r",pos);
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  gr = strstr(answer," home: ");
  if (gr != NULL) sscanf(gr," home: %f",&set);

  if (fabs(pos - set) > 0.01)
    {
      Write_serial_port(hCom,command,strlen(command));
      for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
	ret = Read_serial_port(hCom, answer, 127, &len);
      answer[len] = 0;
      gr = strstr(answer," home: ");
      if (gr != NULL) sscanf(gr," home: %f",&set);
    }
  //win_printf("answer %s\ndac %d",answer,set);
  n_magnet_z = pos;
  //if (len) set_window_title(answer);
  return (fabs(pos - set) > 0.01) ? 0 : 1;
}






int   set_z_origin(float pos)
{
  char command[128], answer[128], *gr;
  unsigned long len = 0;
  int ret, iret;
  float  set = -1;
  DWORD   dwErrors;

  //  purge_com();
  sprintf(command,"zori1=%g\r",pos);
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 10000 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  gr = strstr(answer," origin: ");
  if (gr != NULL) sscanf(gr," origin: %f",&set);
  //win_printf("answer %s\ndac %d",answer,set);
  //if (len) set_window_title(answer);
  return (pos == set) ? 0 : 1;
}



int set_ZPID_param(int num, int val)
{
  char command[128], answer[128], state[32];
  unsigned long len = 0;
  int  set = -1, ret, iret;
  DWORD   dwErrors;

  //  purge_com();
  if (num < 0 || num > 9) return 1;
  sprintf(command,"zpid%d=%d\r",num,val);
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (num == 0)       ret = sscanf(answer,"Zmag P %d",&set);
  else if (num == 1)  ret = sscanf(answer,"Zmag I %d",&set);
  else if (num == 2)  ret = sscanf(answer,"Zmag D %d",&set);
  else if (num == 3)  ret = sscanf(answer,"zmax PWM %d",&set);
  else if (num == 4)  ret = sscanf(answer,"zmax Range %d",&set);
  else if (num == 5)  ret = sscanf(answer,"Zmag vP %d",&set);
  else if (num == 6)  ret = sscanf(answer,"Zmag vI %d",&set);
  else if (num == 7)  ret = sscanf(answer,"Zmag vmax %d",&set);
  else if (num == 8)  
    {
      ret = sscanf(answer,"Zmag PID %s",state); 
      set = (strncmp(state,"On",2) == 0) ? 1 : 0;
    }
  else if (num == 9)  
    {
      ret = sscanf(answer,"brake %s",state);
      set = (strncmp(state,"On",2) == 0) ? 1 : 0;
    }
  else return 1;
  return (val == set) ? 0 : 1;
}
int get_ZPID_param(int num)
{
  char command[128], answer[128], state[32];
  unsigned long len = 0;
  int set = -1, ret, iret;
  DWORD   dwErrors;

  //  purge_com();
  if (num < 0 || num > 9) return -1;
  sprintf(command,"zpid%d?\r",num);
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (num == 0)       ret = sscanf(answer,"Zmag P %d",&set);
  else if (num == 1)  ret = sscanf(answer,"Zmag I %d",&set);
  else if (num == 2)  ret = sscanf(answer,"Zmag D %d",&set);
  else if (num == 3)  ret = sscanf(answer,"zmax PWM %d",&set);
  else if (num == 4)  ret = sscanf(answer,"zmax Range %d",&set);
  else if (num == 5)  ret = sscanf(answer,"Zmag vP %d",&set);
  else if (num == 6)  ret = sscanf(answer,"Zmag vI %d",&set);
  else if (num == 7)  ret = sscanf(answer,"Zmag vmax %d",&set);
  else if (num == 8)  
    {
      ret = sscanf(answer,"Zmag PID %s",state); 
      set = (strncmp(state,"On",2) == 0) ? 1 : 0;
    }
  else if (num == 9)  
    {
      ret = sscanf(answer,"brake %s",state);
      set = (strncmp(state,"On",2) == 0) ? 1 : 0;
    }
  else return -1;
  return set;
}



int set_RPID_param(int num, int val)
{
  char command[128], answer[128], state[32];
  unsigned long len = 0;
  int  set = -1, ret, iret;
  DWORD   dwErrors;

  //  purge_com();
  if (num < 0 || num > 9) return 1;
  sprintf(command,"rpid%d=%d\r",num,val);
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (num == 0)       ret = sscanf(answer,"Rot P %d",&set);
  else if (num == 1)  ret = sscanf(answer,"Rot I %d",&set);
  else if (num == 2)  ret = sscanf(answer,"Rot D %d",&set);
  else if (num == 3)  ret = sscanf(answer,"max PWM %d",&set);
  else if (num == 4)  ret = sscanf(answer,"max Range %d",&set);
  else if (num == 5)  ret = sscanf(answer,"Rot vP %d",&set);
  else if (num == 6)  ret = sscanf(answer,"Rot vI %d",&set);
  else if (num == 7)  ret = sscanf(answer,"Rot vmax %d",&set);
  else if (num == 8)  
    {
      ret = sscanf(answer,"Rot PID %s",state); 
      set = (strncmp(state,"On",2) == 0) ? 1 : 0;
    }
  else if (num == 9)  
    {
      ret = sscanf(answer,"brake %s",state);
      set = (strncmp(state,"On",2) == 0) ? 1 : 0;
    }
  else return 1;
  return (val == set) ? 0 : 1;
}
int get_RPID_param(int num)
{
  char command[128], answer[128], state[32];
  unsigned long len = 0;
  int  set = -1, ret, iret;
  DWORD   dwErrors;

  //  purge_com();
  if (num < 0 || num > 9) return -1;
  sprintf(command,"rpid%d?\r",num);
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  if (num == 0)       ret = sscanf(answer,"Rot P %d",&set);
  else if (num == 1)  ret = sscanf(answer,"Rot I %d",&set);
  else if (num == 2)  ret = sscanf(answer,"Rot D %d",&set);
  else if (num == 3)  ret = sscanf(answer,"max PWM %d",&set);
  else if (num == 4)  ret = sscanf(answer,"max Range %d",&set);
  else if (num == 5)  ret = sscanf(answer,"Rot vP %d",&set);
  else if (num == 6)  ret = sscanf(answer,"Rot vI %d",&set);
  else if (num == 7)  ret = sscanf(answer,"Rot vmax %d",&set);
  else if (num == 8)  
    {
      ret = sscanf(answer,"Rot PID %s",state); 
      set = (strncmp(state,"On",2) == 0) ? 1 : 0;
    }
  else if (num == 9)  
    {
      ret = sscanf(answer,"brake %s",state);
      set = (strncmp(state,"On",2) == 0) ? 1 : 0;
    }
  else return -1;
  return set;
}


# endif


int    set_magnet_z_value_and_wait(float pos)
{
  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos; 
  //pos -=  n_magnet_offset;
  n_magnet_z = pos;// + n_magnet_offset;
  return 0;
}
int    set_rot_value_and_wait(float rot)
{
  //rot -= n_rot_offset;
  n_rota = rot;// + n_rot_offset;
  return 0;
}

int	go_and_dump_z_magnet(float z)
{
  n_magnet_z = z;	
  set_magnet_z_value(n_magnet_z);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);
}
int    go_wait_and_dump_z_magnet(float zmag)
{
  if (n_magnet_z == zmag)		return 0;
  set_magnet_z_value_and_wait(zmag);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);	
}

int    go_and_dump_rot(float r)
{
  //n_rota = r;
  set_rot_value(r);	
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);
}
int	go_wait_and_dump_rot(float r)
{
  if ((n_rota) == r)		return 0;
  set_rot_value_and_wait(r);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);	
}


int	go_wait_and_dump_log_specific_rot(float r, char *log)
{	
  if ((n_rota) == r)		return 0;
  set_rot_value_and_wait(r);
  //dump_to_specific_log_file_only(log,"Rotation number auto changed %g\n",n_rota);
  return 0;
}

int	go_wait_and_dump_log_specific_z_magnet(float zmag, char *log)
{
  if (n_magnet_z == zmag)		return 0;
  set_magnet_z_value_and_wait(zmag);
  //dump_to_specific_log_file_only(log,"Magnet Z changed auto to %g\n",n_magnet_z);	
  return 0;
}



float read_Z_value_accurate_in_micron(void)
{
  char command[128], answer[128], *gr;
  int  set = -1, ret;
  unsigned long len = 0;
  DWORD   dwErrors;
  
  //  purge_com();
  sprintf(command,"dac16?\r");
  /*
  Write_serial_port(hCom,command,strlen(command));
  //talk_serial_port(hCom, command, WRITE_READ, answer, strlen(command), 127);
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  gr = strstr(answer," = ");
  if (gr != NULL) sscanf(gr," = %d",&set);
  //win_printf("answer %s\ndac %d",answer,set);
  if (set >= 0)       pico_dac = set;
  pico_obj_position = ((float)(pico_dac*250))/65535;
  //if (len) set_window_title(answer);
  return pico_obj_position;
}

int read_Z_value(void)
{
	return (int)(10*read_Z_value_accurate_in_micron());	
}
int set_Z_step(float z)  /* in micron */
{
	pico_Z_step = z;
	return 0;
}

float read_Z_value_OK(int *error) /* in microns */
{
  return read_Z_value_accurate_in_micron();
}

int set_Z_value(float z)  /* in microns */
{
  char command[128], answer[128], *gr;
  int dac, set = -1, ret, iret;
  unsigned long len = 0;
  DWORD   dwErrors;
  
  //  purge_com();
  if (z < 0) dac = 0;
  else if (z >= 250) dac = 65535;
  else dac = (int)((65536 * z)/250);
  if (dac > 65535) dac = 65535;
  sprintf(command,"dac16=%d&%d\r\n",dac,dac);
  /*
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  */
  ret = sent_cmd_and_read_answer(hCom, command, answer, 127, &len, &dwErrors, NULL);
  gr = strstr(answer," = ");
  if (gr != NULL) sscanf(gr," = %d",&set);
  if (set != dac)
    {
      Write_serial_port(hCom,command,strlen(command));
      for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
	ret = Read_serial_port(hCom, answer, 127, &len);
      answer[len] = 0;
      gr = strstr(answer," = ");
      if (gr != NULL) sscanf(gr," = %d",&set);
    }
  //win_printf("answer %s\ndac %d",answer,set);
  if (set == dac) 
    {
      pico_dac = set;
      pico_obj_position = ((float)(pico_dac*250))/65535;
    }
  //if (len) set_window_title(answer);
  return (dac == set) ? 0 : 1;
}
float read_last_Z_value(void) /* in microns */
{
  return pico_obj_position;	
}

int	set_Z_obj_accurate(float zstart)
{
  return set_Z_value(zstart);
}
int set_Z_value_OK(float z) 
{
  return set_Z_value(z);
}

int inc_Z_value(int step)  /* in micron */
{
  set_Z_value(pico_obj_position + pico_Z_step*step);
  return 0;
}



int read_Z(void)
{
  float z;
  if(updating_menu_state != 0)	return D_O_K;
  
  z = read_Z_value_accurate_in_micron();
  win_printf("The objective position in z is %f \\mu m\n",z);
  return D_O_K;
    
}
int set_Z(void)
{
  register int i;
  static float z = 0;
	
  if(updating_menu_state != 0)	return D_O_K;
  
  z = read_last_Z_value();
  i = win_scanf("Where do you want to go in \\mu m %f",&z);
  if (i == CANCEL)	return D_O_K;
  set_Z_value(z);
  return D_O_K;
}

int move_obj_small_up(void)
{
  float z;
  if(updating_menu_state != 0)	
      return D_O_K;	

  z = read_Z_value_accurate_in_micron();
  set_Z_value(z+0.1);
  return D_O_K;
}

int move_obj_up(void)
{
  float z;
  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut(0, KEY_UP, 0, move_obj_up);
      add_keyboard_short_cut(0, KEY_F1, 0, move_obj_up);
      add_keyboard_short_cut(0, KEY_F2, 0, move_obj_small_up);

      return D_O_K;	
    }
  z = read_Z_value_accurate_in_micron();
  set_Z_value(z+1);
  return D_O_K;
}

int move_obj_small_dwn(void)
{
  float z;
  if(updating_menu_state != 0)	
      return D_O_K;	
  z = read_Z_value_accurate_in_micron();
  set_Z_value(z-0.1);
  return D_O_K;
}

int move_obj_dwn(void)
{
  float z;
  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut(0, KEY_DOWN, 0, move_obj_dwn);
      add_keyboard_short_cut(0, KEY_F4, 0, move_obj_dwn);
      add_keyboard_short_cut(0, KEY_F3, 0, move_obj_small_dwn);
      return D_O_K;	
    }	
  z = read_Z_value_accurate_in_micron();
  set_Z_value(z-1);
  return D_O_K;
}




/*
To monitor communications events :


       SetCommMask(hCom, dwEvtMask );

dwEvtMask is logical or with : 
EV_BREAK, EV_CTS, EV_DSR, EV_ERR, EV_RING, EV_RLSD , EV_RXCHAR, EV_RXFLAG, EV_TXEMPTY values

 and used WaitCommEvent function

  BOOL WaitCommEvent(
     HANDLE hFile,
     LPDWORD lpEvtMask,
     LPOVERLAPPED lpOverlapped
   );
*/




int read_magnets(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  win_printf("The magnets position \nin z is %f mm\n"
	     "rotation %f \n"
	     ,read_magnet_z_value(),read_rot_value());
  return D_O_K;
}
int set_magnets_rot(void)
{
  register int i;
  static float z = 0;
	
  if(updating_menu_state != 0)	return D_O_K;
  
  z = read_rot_value();
  i = win_scanf("New magnets angular position in turns %f",&z);
  if (i == CANCEL)	return D_O_K;
  set_rot_value(z);
  return D_O_K;;
}

int set_magnets_z(void)
{
  register int i;
  static float z = 0;
	
  if(updating_menu_state != 0)	return D_O_K;
  
  z = read_magnet_z_value();
  i = win_scanf("New magnets position in mm %f",&z);
  if (i == CANCEL)	return D_O_K;

  set_magnet_z_value(z);

  //go_and_dump_z_magnet(z);
  //do_update_menu();

  return D_O_K;;
}


int move_mag(void)
{
  register int i;
  static float z = 0;
	
  if(updating_menu_state != 0)	return D_O_K;
  
  z = read_magnet_z_value();
  i = win_scanf("New magnets position in mm %f",&z);
  if (i == CANCEL)	return D_O_K;

  set_magnet_z_value(z);
  return D_O_K;;
}



int   ref_magnets_zmag(void)
{
  register int i;
  float z = n_magnet_z;
	
  if(updating_menu_state != 0)	return D_O_K;
  i = win_scanf("Enter the new Z reference magnet position %f",&z);
  if (i == CANCEL)	return OFF;
  i = win_printf("are you sure that you want to \n"
		 "change reference of Z magnet to %g",z); 
  if (i == CANCEL)	return OFF;	
  n_magnet_z = z;	
  set_magnet_z_ref_value(n_magnet_z);
  return D_O_K;;
}


int set_magnets_rot_velocity(void)
{
  register int i;
  static float z = 0;
	
  if(updating_menu_state != 0)	return D_O_K;
  
  z = v_rota = get_motors_speed();
  i = win_scanf("New magnets angular velocity in turns/s %f",&z);
  if (i == CANCEL)	return D_O_K;
  set_motors_speed(z);
  return D_O_K;;
}

int do_set_z_origin(void)
{
  register int i;
  static float z = 0;
	
  if(updating_menu_state != 0)	return D_O_K;
  
  i = win_scanf("New magnets Z origin %f",&z);
  if (i == CANCEL)	return D_O_K;
  set_z_origin(z, 0);
  return D_O_K;;
}



int zmag_small_step_up(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	

  set_magnet_z_value(read_magnet_z_value() + 0.05);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z,n_rota);
  set_window_title(buf);
  return D_O_K;
}
int zmag_small_step_dwn(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  set_magnet_z_value(read_magnet_z_value() - 0.05);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z,n_rota);
  set_window_title(buf);
  return D_O_K;
}
int zmag_step_up(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  set_magnet_z_value(read_magnet_z_value() + 0.25);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z,n_rota);
  set_window_title(buf);
  return D_O_K;
}
int zmag_step_dwn(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  set_magnet_z_value(read_magnet_z_value() - 0.25);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z,n_rota);
  set_window_title(buf);
  return D_O_K;
}


int rot_small_step_up(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  set_rot_value( read_rot_value() + 0.25);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z,n_rota);
  set_window_title(buf);
  return D_O_K;
}
int rot_small_step_dwn(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  set_rot_value(read_rot_value() - 0.25);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z,n_rota);
  set_window_title(buf);
  return D_O_K;
}
int rot_step_up(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  set_rot_value(read_rot_value() + 1);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z,n_rota);
  set_window_title(buf);
  return D_O_K;
}
int rot_step_dwn(void)
{
  char buf[256];

  if(updating_menu_state != 0)	      return D_O_K;	
  set_rot_value( read_rot_value() - 1);
  sprintf(buf,"Obj : %g Zmag %g rotation %g",read_last_Z_value(),n_magnet_z,n_rota);
  set_window_title(buf);
  return D_O_K;
}


int mag_z(void)
{
  int index = RETRIEVE_MENU_INDEX;

  if(updating_menu_state != 0)	
    {
      if ((fabs(n_magnet_z-(((float)index) /100))) < 0.02)
	active_menu->flags |=  D_SELECTED;
      else active_menu->flags &= ~D_SELECTED;
      return D_O_K;	
    }
  set_magnet_z_value(((float)index) /100);
  return OFF;
}

MENU *preset_mag_z(float z0, float z1, float dz)
{
  register int i, j;
  float tmp;
  char c[32];
  static MENU pmp[32];
  
  
  if (z0 == z1)
    {
      for (i = CANCEL; i == CANCEL; )
	i = win_scanf("You can define preset values to move Z position"
		     "of the magnets by a menu. Please define the maximum value (mm) %f"
		     "the minimum value (mm) %f"
		     "the step size (mm) (no more than 20 steps are possible %f",&z1,&z0,&dz);
    }
  if (z0 > z1) 
    {
      tmp = z1;
      z1 = z0;
      z0 = tmp;
    }	
  if (dz < 0) dz = -dz;
  if (dz > (z1 - z0)/20) dz = (z1 - z0)/20;
  
  for (i = (int)(100*z0), j = 0; i <= (int)(100*z1) && j < 31 ; i += (int)(100*dz), j++)
    {
      sprintf(c,"%5.2f mm",((float)i)/100);
      add_item_to_menu(pmp,strdup(c), mag_z,NULL,MENU_INDEX(i),NULL);
    }
  pmp[j].text = NULL;
  return pmp;
}
int	set_max_abs_zmag(void)
{
  register int i;
  float m = absolute_maximum_zmag;
  
  if(updating_menu_state != 0)	return D_O_K;  

  i = win_scanf("Define the absolute maximum Z mag %f",&m);
  if (i == CANCEL)	return OFF;
  absolute_maximum_zmag = m;
  return OFF;
}

float what_is_present_rot_value(void)
{
	return n_rota;
}
float what_is_present_z_mag_value(void)
{
	return n_magnet_z;
}


int	ref_magnets_rot(void)
{
  register  int i;
  float r = n_rota;
	
  if(updating_menu_state != 0)	
    {
      add_keyboard_short_cut(0, KEY_F5, 0, zmag_step_up);
      add_keyboard_short_cut(0, KEY_F6, 0, zmag_small_step_up);
      add_keyboard_short_cut(0, KEY_F7, 0, zmag_small_step_dwn);
      add_keyboard_short_cut(0, KEY_F8, 0, zmag_step_dwn);
      add_keyboard_short_cut(0, KEY_F9, 0, rot_step_up);
      add_keyboard_short_cut(0, KEY_F10, 0, rot_small_step_up);
      add_keyboard_short_cut(0, KEY_F11, 0, rot_small_step_dwn);
      add_keyboard_short_cut(0, KEY_F12, 0, rot_step_dwn);
      return D_O_K;	
    }
  i = win_scanf("Enter the new reference \n of rotation number %f",&r);
  if (i == CANCEL)	return OFF;
  i = win_printf("are you sure that you want to \n"
		 "change reference of rotation number to %g",r); 
  if (i == CANCEL)	return OFF;
  n_rota = r;
  set_rot_ref_value(n_rota);
  return D_O_K;
}

int	inc_magnets_rot(void)
{
  register int i;
  float r = n_rota;
  static float dr = 10;
  char c[256];
	
  if(updating_menu_state != 0)	return D_O_K;
  r = read_rot_value();
  sprintf(c,"the rotation number is presently %g {\\sl turns} \n"
	  "increment this value by %%f",r);
	
  i = win_scanf(c,&dr);
  if (i == CANCEL)	return OFF;
  if (r > 1000 || r < -1000)
    i = win_printf("the new rot position %f\n" 
		   "may take some time to reach !\n"
		   " do you really want to go there ?",r);
  if (i == CANCEL)	return OFF;	
  n_rota = r + dr;	
  set_rot_value(n_rota);
  return D_O_K;
}



float get_temperature(int t)  
{
  char command[128], answer[128], *gr;
  int   ret, iret, Tn = -1;
  float set = 0;
  static float last[3] = {0,0,0};
  unsigned long len = 0;
  unsigned long t0;

  if (t < 0 || t > 2) return 0;
  //purge_com();
  sprintf(command,"T%d?\r",t);
  t0 = my_uclock();        
  Write_serial_port(hCom,command,strlen(command));
  for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
    ret = Read_serial_port(hCom, answer, 127, &len);
  answer[len] = 0;
  t0 = my_uclock() - t0;
  if (len == 0 && debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  fprintf (fp_debug,"!! zero length response after %g ms\n",1000*(double)(t0)/get_my_uclocks_per_sec());
	  fclose(fp_debug);
	}
    }
  Tn = -1;
  if (answer[0] == 'T')
    sscanf(answer+1,"%d",&Tn);
  gr = strstr(answer," is at ");
  if (gr == NULL || Tn < 0)
    {
      Write_serial_port(hCom,command,strlen(command));
      for (iret = ret = 0; iret < 4 && ret <= 0; iret++)
	ret = Read_serial_port(hCom, answer, 127, &len);
      answer[len] = 0;
      if (debug)
	{
	  fp_debug = fopen(debug_file,"a");
	  if (fp_debug != NULL)
	    {
	      fprintf (fp_debug,"!! order resent after %g ms\n",1000*(double)(t0)/get_my_uclocks_per_sec());
	      fclose(fp_debug);
	    }
	  
	}
    }
  Tn = -1;
  if (answer[0] == 'T')
    sscanf(answer+1,"%d",&Tn);
  gr = strstr(answer," is at ");
  if (gr != NULL && Tn >= 0)
    { 
      sscanf(gr," is at %g",&set);
      if (Tn < 3 && Tn >=0) 
	{
	  last[Tn] = set;
	  if (debug)
	    {
	      fp_debug = fopen(debug_file,"a");
	      if (fp_debug != NULL)
		{
		  fprintf (fp_debug,"Answer ok after %g ms\n",1000*(double)(t0)/get_my_uclocks_per_sec());
		  fclose(fp_debug);
		}
	      
	    }
	}

    }
  else 
    {
      set_window_title(answer);
      set = last[t];
    }
  return (Tn == t) ? set : last[t];
}


int thermal_z_op_idle_action(O_p *op, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  pltreg *pr;
  char buf[1024];

  if (d->dp == NULL)    return 1;
  pr = (pltreg*)d->dp;        /* the image region is here */
  if (pr->one_p == NULL || pr->n_op == 0)        return 1;


   if ((my_uclock() - grab_start) < (n_grab*grab_time)) 
     {
       sprintf(buf,"waiting %ld",my_uclock() - grab_start);
       set_window_title(buf);
       return D_O_K;
     }
   if (n_grab >= op->dat[0]->mx)
     {
       op->op_idle_action = NULL;
       set_window_title("Removing idle action");
       return D_O_K;
     }
   add_new_point_to_ds(op->dat[0],n_grab,get_temperature(0));
   add_new_point_to_ds(op->dat[1],n_grab,get_temperature(1));
   add_new_point_to_ds(op->dat[2],n_grab,get_temperature(2));
   op->need_to_refresh = 1;
   n_grab++;
   return   refresh_plot(pr, UNCHANGED);
}

int record_thermal_z(void)
{
   int i;
   static int nf = 1024, nm = 5;
   pltreg *pr;
   O_p *op;
   d_s *ds;

   if(updating_menu_state != 0)	return D_O_K;

   if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	return OFF;


   i = win_scanf("Grabing temperature vs time\n"
		 "total number of points for recording %6d\n"
		 "how many measurements per sec %4d\n",&nf,&nm);

   if (i == CANCEL) return D_O_K;

   grab_time = get_my_uclocks_per_sec()/nm;

   op = create_and_attach_one_plot(pr,  nf, nf, 0);
   ds = op->dat[0];
   set_ds_source(ds, "Sensor temperature");
   ds->nx = ds->ny = 0;

   add_new_point_to_ds(ds,0,get_temperature(0));


   if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
     return win_printf_OK("I can't create plot !");
   set_ds_source(ds, "Sample temperature");
   ds->nx = ds->ny = 0;
   add_new_point_to_ds(ds,0,get_temperature(1));

   if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
     return win_printf_OK("I can't create plot !");
   set_ds_source(ds, "back temperature");
   ds->nx = ds->ny = 0;
   add_new_point_to_ds(ds,0,get_temperature(2));

   grab_start = my_uclock();
   n_grab = 1;
   op->op_idle_action = thermal_z_op_idle_action;
   return D_O_K;
}




/*
Attemp to read the pico response from serial port
if a full answer is available then transfer it
otherwise grab characters and respond that full message is not yet there

*/

char *catch_pending_request_answer(int *size, int im_n, int *error, unsigned long *t0)
{
  static char answer[128], request_answer[128];
  unsigned long dwRead = 0;
  static int i = 0;
  int j, max_size = 128, lf_found = 0;
  char lch[2];

  if (t0 != NULL) *t0 = my_uclock();
  if (hCom == NULL) 
   {
     *error = -3;
     if (t0 != NULL) *t0 = my_uclock()- *t0;
     return NULL;   // no serial port !
   }

  for (j = 0; j == 0 && i < max_size-1; )
    {// we grab rs232 char already arrived
      dwRead = 0;
      if (ReadData(hCom, lch, 1, &dwRead) == 0)
	{  // something wrong
	  answer[i] = 0;
	  dwRead = i;
	  if (debug)
	    {
	      fp_debug = fopen(debug_file,"a");
	      if (fp_debug != NULL)
		{
		  fprintf (fp_debug,"<-- ReadData error at im %d\n", im_n);
		  fprintf (fp_debug,"<\t %s\n",answer);
		  fclose(fp_debug);
		}
	    }
	  *error = -2;
	  if (t0 != NULL) *t0 = my_uclock()- *t0;
	  return NULL;    // serial port read error !
	}
      if (dwRead == 1) 
	{
	  answer[i] = lch[0];
	  j = lf_found = (lch[0] == '\n') ? 1 : 0;
	  i++;
	}
      else if (dwRead == 0) j = 1; 
    }
  answer[i] = 0;
  *size = i;
  if (lf_found)
    {
      *error = 0;
      strncpy(request_answer, answer,i);
      if (debug)
	{
	  fp_debug = fopen(debug_file,"a");
	  if (fp_debug != NULL)
	    {
	      fprintf (fp_debug,"Im %d : <\t %s\n",im_n,request_answer);
	      fclose(fp_debug);
	    }
	}
      if (t0 != NULL) *t0 = my_uclock()- *t0;
      i = 0;
      return request_answer;
    }
  *error = 1;
  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  fprintf (fp_debug,"Im %d : <\t %s...\n",im_n,answer);
	  fclose(fp_debug);
	}
    }
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return NULL;
}




int rs232_register(int type, int image_n, float val)
{
  if (request_pending)  return 1;
  request_pending = type;
  request_image_n = image_n;
  request_parameter = val;
  return 0;
}

int what_is_pending_request(int *type, int *image_n, float *val)
{
  if (request_pending == 0)  return 1;
  *type = request_pending ;
  *image_n = request_image_n;
  *val = request_parameter;
  return 0;
}


int request_read_rot_value(int image_n, unsigned long *t0)
{
  char command[128];
  
  if (rs232_register(READ_ROT_REQUEST,image_n,0)) return 1; // rs232 buisy
  if (t0 != NULL) *t0 = my_uclock();
  snprintf(command,128,"roti?\r");
  Write_serial_port(hCom,command,strlen(command));
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return 0;
}
float read_rot_raw_from_request_answer(char *answer, int im_n, float val, int *error)
{
  int set = -1;
  float r = 0;

  *error = 0;
  if (answer != NULL)
    {
      if (sscanf(answer,"Roti = %d",&set) == 1)
	r =  n_rota = ((float)(set+4))/8000;
      else *error = PICO_COM_ERROR;
    } 
  else *error =  PICO_COM_ERROR;
  return r;	
}



int request_read_temperature_value(int image_n, int t, unsigned long *t0)
{
  char command[128];

  if (rs232_register(READ_TEMPERATURE_REQUEST,image_n,(float)t)) return 1; // rs232 buisy
  if (t0 != NULL) *t0 = my_uclock();
  if (t < 0 || t > 3) return 2;
  snprintf(command,128,"T%d?\r\n",t);
  Write_serial_port(hCom,command,strlen(command));
  if (t0 != NULL) *t0 = my_uclock()- *t0;
  return 0;
}

float read_temperature_from_request_answer(char *answer, int im_n, float val, int *error, int *channel)
{
  float t = 0;

  *error = 0;
  if (answer != NULL)
    {
      if (sscanf(answer,"T%d = %f",channel,&t) == 2)
	{
	  return t;
	}
      else *error |= PICO_COM_ERROR;
    } 
  else *error =  PICO_COM_ERROR;
  return t;	
}


int test_request(void)
{
  unsigned long t0[32], tt;
  int size[32], error = 0, channel = -1, i;
  char *ans;
  float T0;
  double dt;

 if(updating_menu_state != 0)	return D_O_K;

 tt = my_uclock();
 request_read_temperature_value(23, 0, t0);

 for (i = 1, ans = NULL; i < 32 &&  ans == NULL; i++)
   {
     ans = catch_pending_request_answer(size + i, 23, &error, t0 + i);
   }
 for ( ans = NULL; i < 32 &&  ans == NULL; i++)
   {
     ans = catch_pending_request_answer(size + i, 23, &error, t0 + i);
   }
 T0 = read_temperature_from_request_answer(ans, 23, 0, &error, &channel);
 tt = my_uclock()- tt;
 dt = 1000*(double)(t0[0])/get_my_uclocks_per_sec();
 win_printf("Write took %g ms",dt);
 dt = 1000*(double)(t0[1])/get_my_uclocks_per_sec();

 win_printf("%d read 1st took %g ms\n%s",i,dt,ans);
 dt = 1000*(double)(tt)/get_my_uclocks_per_sec();
 win_printf("T%d = %g\ntotal time %g ms",channel,T0,dt);

 return D_O_K;
}


int grab_zmag_plot_from_serial_port(void)
{
  pltreg *pr;
  O_p *op, *op2, *opz, *opv, *opc;
  d_s *ds, *dst, *ds2 = NULL, *dsc, *der, *dsver, *dsvt, *dszer;
  d_s *dszp, *dszi, *dszd;
  d_s *dsvp, *dsvi;
  static int axe = 0;
  int  i, j, nx = 400, a = 0, b = 0, c = 0, ret = 0, zmag_inte = 0, iver, last_i; // , v_servo
  char Command[128], bug[256], answer[128];
  unsigned long lpNumberOfBytesWritten = 0;
  //DWORD nNumberOfBytesToRead = 127;
  int zmag_P, zmag_I, zmag_D, zmag_max_PWM, zmag_PWM_offset, zmag_v_P, zmag_v_I, zmag_speed_target, tmpi;
  int ler, ier, terrorz;  // , terrorvz
  int zmag_P0, zmag_I0, zmag_D0, zmag_max_PWM0, zmag_PWM_offset0, zmag_v_P0, zmag_v_I0, zmag_speed_target0;
  int zmagi, zmag_speed;
  static float step = 1, sgn = 1, zmag;
  unsigned long t0, tt, tn;
  DWORD   dwErrors;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot"));


  for (i = 0, tmpi = -1; tmpi < 0 && i < 5; i++,   my_sleep(10), tmpi = get_zmag_motor_pid(0));
  if (tmpi < 0) return win_printf("Could not read Z pid0");
  else zmag_P0 = zmag_P = tmpi;

  for (i = 0, tmpi = -1; tmpi < 0 && i < 5; i++,   my_sleep(10), tmpi = get_zmag_motor_pid(1));
  if (tmpi < 0) return win_printf("Could not read Z pid1");
  else zmag_I0 = zmag_I = tmpi;

  for (i = 0, tmpi = -1; tmpi < 0 && i < 5; i++,   my_sleep(10), tmpi = get_zmag_motor_pid(2));
  if (tmpi < 0) return win_printf("Could not read Z pid2");
  else zmag_D0 = zmag_D = tmpi;

  for (i = 0, tmpi = -1; tmpi < 0 && i < 5; i++,   my_sleep(10), tmpi = get_zmag_motor_pid(3));
  if (tmpi < 0) return win_printf("Could not read Z pid3");
  else   zmag_max_PWM0 = zmag_max_PWM = tmpi;

  for (i = 0, tmpi = -1; tmpi < 0 && i < 5; i++,   my_sleep(10), tmpi = get_zmag_motor_pid(4));
  if (tmpi < 0) return win_printf("Could not read Z pid4");
  else zmag_PWM_offset0 = zmag_PWM_offset = tmpi;

  for (i = 0, tmpi = -1; tmpi < 0 && i < 5; i++,   my_sleep(10), tmpi = get_zmag_motor_pid(5));
  if (tmpi < 0) return win_printf("Could not read Z pid5");
  else zmag_v_P0 = zmag_v_P = tmpi;

  for (i = 0, tmpi = -1; tmpi < 0 && i < 5; i++,   my_sleep(10), tmpi = get_zmag_motor_pid(6));
  if (tmpi < 0) return win_printf("Could not read Z pid6");
  else  zmag_v_I0 = zmag_v_I = tmpi;

  for (i = 0, tmpi = -1; tmpi < 0 && i < 5; i++,   my_sleep(10), tmpi = get_zmag_motor_pid(7));
  if (tmpi < 0) return win_printf("Could not read Z pid7");
  else   zmag_speed_target0 = zmag_speed_target = tmpi;


  i = win_scanf("I am going to do a step of %6f mm\n"
		"After you have change feedback\n"
		"PID setting P %5d I %5d D %5d\n"
		"pwm %5d offset %5d\n"
		"vP %5d vI %5d Vm %5d\n",&step,&zmag_P,&zmag_I,&zmag_D,
		&zmag_max_PWM,&zmag_PWM_offset,&zmag_v_P,&zmag_v_I,&zmag_speed_target);

  if (i == CANCEL) return D_O_K;
  if (zmag_P != zmag_P0)   
    {
      for (i = 0, tmpi = -1; tmpi != 0 && i < 5; i++,   my_sleep(10), tmpi = set_zmag_motor_pid(0,zmag_P));
      if (tmpi != 0) return win_printf("Could not set Z pid0");
    }
  if (zmag_I != zmag_I0)   
    {
      for (i = 0, tmpi = -1; tmpi != 0 && i < 5; i++,   my_sleep(10), tmpi = set_zmag_motor_pid(1,zmag_I));
      if (tmpi != 0) return win_printf("Could not set Z pid1");
    }
  if (zmag_D != zmag_D0)   
    {
      for (i = 0, tmpi = -1; tmpi != 0 && i < 5; i++,   my_sleep(10), tmpi = set_zmag_motor_pid(2,zmag_D));
      if (tmpi != 0) return win_printf("Could not set Z pid2");
    }
  if (zmag_max_PWM != zmag_max_PWM0)   
    {
      for (i = 0, tmpi = -1; tmpi != 0 && i < 5; i++,   my_sleep(10), tmpi = set_zmag_motor_pid(3,zmag_max_PWM));
      if (tmpi != 0) return win_printf("Could not set Z pid3");
    }
  if (zmag_PWM_offset != zmag_PWM_offset0)   
    {
      for (i = 0, tmpi = -1; tmpi != 0 && i < 5; i++,   my_sleep(10), tmpi = set_zmag_motor_pid(4,zmag_PWM_offset));
      if (tmpi != 0) return win_printf("Could not set Z pid4");
    }
  if (zmag_v_P != zmag_v_P0)   
    {
      for (i = 0, tmpi = -1; tmpi != 0 && i < 5; i++,   my_sleep(10), tmpi = set_zmag_motor_pid(5,zmag_v_P));
      if (tmpi != 0) return win_printf("Could not set Z pid5");
    }
  if (zmag_v_I != zmag_v_I0)   
    {
      for (i = 0, tmpi = -1; tmpi != 0 && i < 5; i++,   my_sleep(10), tmpi = set_zmag_motor_pid(6,zmag_v_I));
      if (tmpi != 0) return win_printf("Could not set Z pid6");
    }
  if (zmag_speed_target != zmag_speed_target0)   
    {
      for (i = 0, tmpi = -1; tmpi != 0 && i < 5; i++,   my_sleep(10), tmpi = set_zmag_motor_pid(7,zmag_speed_target));
      if (tmpi != 0) return win_printf("Could not set Z pid7");
    }
  sgn = (step < 0) ? -1 : 1;
  zmag = read_magnet_z_value();
  if (zmag == __FLT_MAX__) zmag = read_magnet_z_value();
  if (zmag == __FLT_MAX__) return win_printf("Could not read zmag");
  my_sleep(10);
  zmag = ((float)((int)((sgn*0.5) + zmag*200)))/200;
  for (i = 0, tmpi = -1; i < 5 && tmpi != 0; tmpi = set_magnet_z_value(zmag + step), i++);
  if (tmpi) return win_printf("Could not set zmag");
  zmagi = (int)(((zmag+step)*8000)+0.5);
  i = win_printf("Did motor move");




  bug[0] = 0; answer[0] = 0;
  pr = create_pltreg_with_op(&op, nx, nx, 0);
  if (op == NULL)    return win_printf("Cannot allocate plot");
  ds = op->dat[0];
  if (ds == NULL)    return win_printf("Cannot allocate plot");
  dst = create_and_attach_one_ds(op, nx, nx, 0);
  if (dst == NULL)    return win_printf("Cannot allocate plot");


  op2 = create_and_attach_one_plot(pr, nx,nx, 0);
  if (op2 == NULL)    return win_printf("Cannot allocate plot");
  dsvt = op2->dat[0];
  if (dsvt == NULL)    return win_printf("Cannot allocate plot");
  dsc = create_and_attach_one_ds(op2, nx, nx, 0);
  if (dsc == NULL)    return win_printf("Cannot allocate plot");



  opc = create_and_attach_one_plot(pr, nx,nx, 0);
  if (opc == NULL)    return win_printf("Cannot allocate plot");
  ds2 = opc->dat[0];
  der = create_and_attach_one_ds(opc, nx, nx, 0);
  if (der == NULL)    return win_printf("Cannot allocate plot");


  opz = create_and_attach_one_plot(pr, nx,nx, 0);
  if (opz == NULL)    return win_printf("Cannot allocate plot");
  dszer = opz->dat[0];
  set_ds_source(dszer, "Zmag error");
  dszp = create_and_attach_one_ds(opz, nx, nx, 0);
  if (dszp == NULL)    return win_printf("Cannot allocate plot");
  set_ds_source(dszp, "P pid");
  dszi = create_and_attach_one_ds(opz, nx, nx, 0);
  if (dszi == NULL)    return win_printf("Cannot allocate plot");
  set_ds_source(dszi, "I pid");
  dszd = create_and_attach_one_ds(opz, nx, nx, 0);
  if (dszd == NULL)    return win_printf("Cannot allocate plot");
  set_ds_source(dszd, "D pid");


  opv = create_and_attach_one_plot(pr, nx,nx, 0);
  if (opv == NULL)    return win_printf("Cannot allocate plot");
  dsver = opv->dat[0];
  set_ds_source(dszer, "Zmag speep error");
  dsvp = create_and_attach_one_ds(opv, nx, nx, 0);
  if (dsvp == NULL)    return win_printf("Cannot allocate plot");
  set_ds_source(dsvp, "P speed");
  dsvi = create_and_attach_one_ds(opv, nx, nx, 0);
  if (dsvi == NULL)    return win_printf("Cannot allocate plot");
  set_ds_source(dsvi, "I speed");

  snprintf(Command,128,"ztran=0\r\n");

  i = 0;
  /*
  Write_serial_port(hCom,Command,9);
  ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
  for  (; ret == 0; )
    ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
  */
  ret = sent_cmd_and_read_answer(hCom, Command, answer, 127, &lpNumberOfBytesWritten, &dwErrors, NULL);
  if (ret) ret = sent_cmd_and_read_answer(hCom, Command, answer, 127, &lpNumberOfBytesWritten, &dwErrors, NULL);
  sprintf(Command,"ztran?\r\n");

  //win_printf("0 %s",answer);
  for (last_i = i = 0, tt = 0; i < nx; )
    {
      t0 = my_uclock();
      tn = t0 + (get_my_uclocks_per_sec()/100);
      ret = sent_cmd_and_read_answer(hCom, Command, answer, 127, &lpNumberOfBytesWritten, &dwErrors, NULL);
      if (ret)
	{
	    sprintf(Command,"ztran=%d\r\n",last_i+1);
	    for (j = 0, ret = 1; ret != 0 && j < 5; j++) 
	      ret = sent_cmd_and_read_answer(hCom, Command, answer, 127, &lpNumberOfBytesWritten, &dwErrors, NULL);
	    if (ret) win_printf("error correcting %s",Command);
	    sprintf(Command,"ztran?\r\n");
	    ret = sent_cmd_and_read_answer(hCom, Command, answer, 127, &lpNumberOfBytesWritten, &dwErrors, NULL);
	    if (ret) win_printf("second read error!");
	}
      tt += my_uclock() - t0; 
      if (axe < 2)
	{ 
	  if (sscanf(answer,"%d\t%d\t%d",&a,&b,&c) == 3)
	    {
	      ds->xd[i] = ds2->xd[i] = dst->xd[i] = dsc->xd[i] = der->xd[i] = a;
	      dszp->xd[i] = dszi->xd[i] = dszd->xd[i] = a;
	      last_i = a;
	      dsvp->xd[i] = dsvi->xd[i] = a;
	      dsvt->xd[i] = dsver->xd[i] = dszer->xd[i] = a;
	      ds->yd[i] = c;
	      if (i == 0) 
		{
		  dst->yd[i] = ds->yd[i] + sgn*zmag_speed_target;
		  dsc->yd[i] = 0;
		  zmag_inte = b << 2;
		}
	      else 
		{
		  dst->yd[i] = dst->yd[i-1] + sgn*zmag_speed_target;
		  dsc->yd[i] = ds->yd[i] - ds->yd[i-1];
		}
	      dsvt->yd[i] = sgn*zmag_speed_target;
	      if (sgn < 0 && dst->yd[i] < zmagi) dsvt->yd[i] = 0;
	      if (sgn >= 0 && dst->yd[i] > zmagi) dsvt->yd[i] = 0;
	      dsver->yd[i] = dsvt->yd[i] - dsc->yd[i];
	      if (sgn < 0) dst->yd[i] = (dst->yd[i] < zmagi) ? zmagi : dst->yd[i];
	      else  dst->yd[i] = (dst->yd[i] > zmagi) ? zmagi : dst->yd[i];
	      ds2->yd[i] = b;
	      dszer->yd[i] = dst->yd[i] - ds->yd[i];

	      // dst->yd[i] = zmag_target_inst

	      ler = dst->yd[i] -ds->yd[i];  //zmag_target_inst - zmag_pos; this is the error
	      ler = (ler > 4096) ? 4096 :ler;                  // we saturate the error
	      ler = (ler < -4096) ? -4096 :ler;                // 
	      ier = (int)ler;                                    // this is the error in int
	      iver = zmag_speed = dsvt->yd[i] -(int)dsc->yd[i];            
	      //(zmag_pos_1 - zmag_pos); we compute - the speed in ticks/ms
	      terrorz = ier << 2;
	      terrorz = terrorz >> zmag_P;                       // this is the correction for position
	      dszp->yd[i] = terrorz;
# ifdef OLD
	  // feedback with proportionnal only
	      if (ler > 0)                                       // processing positive velocity
		{
		  iver += zmag_speed_target;                     // we get the speed error signal 
		  terrorvz = iver << zmag_v_P;                   // correction for velocity feedback with proportionnal only
		  v_servo = (terrorvz < terrorz) ? 1 : 0;        // test which servo is active
		}
	      else
		{
		  iver -= zmag_speed_target;                     // we get the speed error signal 
		  terrorvz = iver << zmag_v_P;                   // correction for velocity feedback with proportionnal only
		  v_servo = (terrorvz > terrorz) ? 1 : 0;        // test which servo is active
		} 
	      if (v_servo)
		{
		  terrorz = terrorvz;                            // terrorz use velocity feedback
		  zmag_inte += iver << zmag_v_I;                 // we make the velocity integration
		  dsvp->yd[i] = terrorvz;
		}
	      else 
		{
# endif
		  zmag_inte += ier >> zmag_I;                    // we make the position integration 
		  if (zmag_D >= 0) 
		    {
		      terrorz += zmag_speed << zmag_D;             // we add derivative in position feedback
		      dszd->yd[i] = zmag_speed << zmag_D;
		    }
# ifdef OLD
		}
# endif
	      zmag_inte = (zmag_inte > 511) ? 511 : zmag_inte;   // we limit the integral factor
	      zmag_inte = (zmag_inte < -511) ? -511 : zmag_inte; // to 64 at the level of PWM
# ifdef OLD
	      if (v_servo)		  dsvi->yd[i] = zmag_inte;
	      else
# endif
	      dszi->yd[i] = zmag_inte;
	      terrorz += zmag_inte >> 2;                         // zmag_inte is reduced to keep accuracy
	      
	      if (terrorz < 0) 
		{
		  terrorz -= zmag_PWM_offset;       // we add an offset to compensate the motor one
		  if (terrorz < -zmag_max_PWM)   terrorz = -((int)zmag_max_PWM);
		}
	      else 
		{
		  terrorz += zmag_PWM_offset;      // we add an offset to compensate the motor one
		  if (terrorz > zmag_max_PWM) 	 terrorz = (int)zmag_max_PWM;
		}

	      der->yd[i] = terrorz;
	      
	    }
	  else sprintf(bug,"line %d >%s<len \n",i,answer); 
	  i++;
	  if (i%16 == 0) display_title_message("reading %d >%d %d %d",i,a,b,c);
	}
      else
	{ 
	  if (sscanf(answer,"%d\t%d",&a,&b) == 2)
	    {
	      ds->xd[i] = a;
	      ds->yd[i] = b;
	    }
	  else sprintf(bug,"line %d >%s<len \n",i,answer); 
	  i++;
	  if (i%16 == 0) display_title_message("reading %d >%d %d",i,a,b);
	}
	for ( ; my_uclock() < tn; );
    }
  win_printf("Mean read time %g ms",1000*(double)(tt)/(nx*get_my_uclocks_per_sec()));

  set_plot_title(op,"\\stack{{Zmag timing}{P %d I %d D %d}{pwm %d range %d}{zmag_v_P %d zmag_v_I %d vm %d}}",
		 zmag_P, zmag_I, zmag_D, zmag_max_PWM, zmag_PWM_offset, zmag_v_P, zmag_v_I, zmag_speed_target);
  create_attach_select_y_un_to_op(op, IS_METER, 0 ,(float)1/8000, -3, 0, "mm");
  create_attach_select_x_un_to_op(op, IS_SECOND, 0,(float)1, -3, 0, "ms");
  set_plot_x_title(op, "Time");
  set_plot_y_title(op, "Zmag");			


  set_plot_title(op2,"\\stack{{dZmag/dt timing}{P %d I %d D %d}{pwm %d range %d}{zmag_v_P %d zmag_v_I %d vm %d}}",
		 zmag_P, zmag_I, zmag_D, zmag_max_PWM, zmag_PWM_offset, zmag_v_P, zmag_v_I, zmag_speed_target);
  create_attach_select_y_un_to_op(op2, 0, 0 ,(float)1/8, 0, 0, "mm/s");
  create_attach_select_x_un_to_op(op2, IS_SECOND, 0,(float)1, -3, 0, "ms");
  set_plot_x_title(op2, "Time");
  set_plot_y_title(op2, "dZmag/dt");			

  set_plot_title(opc,"\\stack{{PWM command}{P %d I %d D %d}{pwm %d range %d}{zmag_v_P %d zmag_v_I %d vm %d}}",
		 zmag_P, zmag_I, zmag_D, zmag_max_PWM, zmag_PWM_offset, zmag_v_P, zmag_v_I, zmag_speed_target);
  create_attach_select_x_un_to_op(opc, IS_SECOND, 0,(float)1, -3, 0, "ms");
  set_plot_x_title(opc, "Time");
  set_plot_y_title(opc, "PWM drive");			

  set_plot_title(opz,"\\stack{{Zmag error}{P %d I %d D %d}{pwm %d range %d}{zmag_v_P %d zmag_v_I %d vm %d}}",
		 zmag_P, zmag_I, zmag_D, zmag_max_PWM, zmag_PWM_offset, zmag_v_P, zmag_v_I, zmag_speed_target);
  create_attach_select_x_un_to_op(opz, IS_SECOND, 0,(float)1, -3, 0, "ms");
  set_plot_x_title(opz, "Time");
  set_plot_y_title(opz, "PWM drive");			

  set_plot_title(opv,"\\stack{{Zmag speed error}{P %d I %d D %d}{pwm %d range %d}{zmag_v_P %d zmag_v_I %d vm %d}}",
		 zmag_P, zmag_I, zmag_D, zmag_max_PWM, zmag_PWM_offset, zmag_v_P, zmag_v_I, zmag_speed_target);
  create_attach_select_x_un_to_op(opz, IS_SECOND, 0,(float)1, -3, 0, "ms");
  set_plot_x_title(opv, "Time");
  set_plot_y_title(opv, "PWM drive");			

  set_magnet_z_value(zmag);
  refresh_plot(pr, pr->n_op-1);		
  return D_O_K;
}

int test_bug(void)
{
  int i, ni = 10000, ier = 0;
  float z;

  if(updating_menu_state != 0)	return D_O_K;
  

  i = win_scanf("nb of test %d",&ni);
  if (i == CANCEL)   return D_O_K;
  for (i = 0; i < ni; i++)
    {
      z = read_magnet_z_value();
      if (z == __FLT_MAX__) win_printf("    error reading zmag %d %s",ier++,last_answer_2);
      if ((i%500) == 0) my_set_window_title("                                      iter %d zmag %g",i,z);
    }
  return D_O_K;
}

int grab_rot_plot_from_serial_port(void)
{
  pltreg *pr;
  O_p *op, *op2, *opc; // , *opz, *opv
  d_s *ds, *dst, *ds2 = NULL, *dsc, *der, *dsvt;
  //d_s *dszp, *dszi, *dszd, *dszer;
  //d_s *dsvp, *dsvi, *dsver;
  static int axe = 0;
  int  i, nx = 400, a = 0, b = 0, c = 0, ret = 0, rot_inte = 0, iver; // , v_servo
  char Command[128], bug[256], answer[128];
  unsigned long lpNumberOfBytesWritten = 0, tn;
  //DWORD nNumberOfBytesToRead = 127;
  int rot_P, rot_I, rot_D, rot_max_PWM, rot_PWM_offset, rot_v_P, rot_v_I, rot_speed_target;
  int ler, ier, terrorz; // , terrorvz
  int rot_P0, rot_I0, rot_D0, rot_max_PWM0, rot_PWM_offset0, rot_v_P0, rot_v_I0, rot_speed_target0;
  int roti, rot_speed;
  static float step = 1, sgn = 1, rot;
  DWORD   dwErrors;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot"));


  rot_P0 = rot_P = get_rotation_motor_pid(0);
  my_sleep(10);
  rot_I0 = rot_I = get_rotation_motor_pid(1);
  my_sleep(10);
  rot_D0 = rot_D = get_rotation_motor_pid(2);
  my_sleep(10);
  rot_max_PWM0 = rot_max_PWM = get_rotation_motor_pid(3);
  my_sleep(10);
  rot_PWM_offset0 = rot_PWM_offset = get_rotation_motor_pid(4);
  my_sleep(10);
  rot_v_P0 = rot_v_P = get_rotation_motor_pid(5);
  my_sleep(10);
  rot_v_I0 = rot_v_I = get_rotation_motor_pid(6);
  my_sleep(10);
  rot_speed_target0 = rot_speed_target = get_rotation_motor_pid(7);


  i = win_scanf("I am going to do a step of %6f mm\n"
		"After you have change feedback\n"
		"PID setting P %5d I %5d D %5d\n"
		"pwm %5d offset %5d\n"
		"vP %5d vI %5d Vm %5d\n",&step,&rot_P,&rot_I,&rot_D,
		&rot_max_PWM,&rot_PWM_offset,&rot_v_P,&rot_v_I,&rot_speed_target);

  if (i == CANCEL) return D_O_K;
  if (rot_P != rot_P0)   {set_rotation_motor_pid(0,rot_P);   my_sleep(10);}
  if (rot_I != rot_I0)   {set_rotation_motor_pid(1,rot_I);  my_sleep(10);}
  if (rot_D != rot_D0)    {set_rotation_motor_pid(2,rot_D);   my_sleep(10);}
  if (rot_max_PWM != rot_max_PWM0)   {set_rotation_motor_pid(3,rot_max_PWM);   my_sleep(10);}
  if (rot_PWM_offset != rot_PWM_offset0)   {set_rotation_motor_pid(4,rot_PWM_offset);   my_sleep(10);}
  if (rot_v_P != rot_v_P0)   {set_rotation_motor_pid(5,rot_v_P);   my_sleep(10);}
  if (rot_v_I != rot_v_I0)   {set_rotation_motor_pid(6,rot_v_I);   my_sleep(10);}
  if (rot_speed_target != rot_speed_target0)   { set_rotation_motor_pid(7,rot_speed_target);   my_sleep(10);}
  sgn = (step < 0) ? -1 : 1;
  rot = read_rot_value();
  my_sleep(10);
  rot = ((float)((int)((sgn*0.5) + rot*200)))/200;
  set_rot_value(rot + step);
  roti = (int)(((rot+step)*8000)+0.5);
  i = win_printf("Did motor move");




  bug[0] = 0; answer[0] = 0;
  pr = create_pltreg_with_op(&op, nx, nx, 0);
  if (op == NULL)    return win_printf("Cannot allocate plot");
  ds = op->dat[0];
  if (ds == NULL)    return win_printf("Cannot allocate plot");
  dst = create_and_attach_one_ds(op, nx, nx, 0);
  if (dst == NULL)    return win_printf("Cannot allocate plot");


  op2 = create_and_attach_one_plot(pr, nx,nx, 0);
  if (op2 == NULL)    return win_printf("Cannot allocate plot");
  dsvt = op2->dat[0];
  if (dsvt == NULL)    return win_printf("Cannot allocate plot");
  dsc = create_and_attach_one_ds(op2, nx, nx, 0);
  if (dsc == NULL)    return win_printf("Cannot allocate plot");



  opc = create_and_attach_one_plot(pr, nx,nx, 0);
  if (opc == NULL)    return win_printf("Cannot allocate plot");
  ds2 = opc->dat[0];
  der = create_and_attach_one_ds(opc, nx, nx, 0);
  if (der == NULL)    return win_printf("Cannot allocate plot");

  /*
  opz = create_and_attach_one_plot(pr, nx,nx, 0);
  if (opz == NULL)    return win_printf("Cannot allocate plot");
  dszer = opz->dat[0];
  set_ds_source(dszer, "Rot error");
  dszp = create_and_attach_one_ds(opz, nx, nx, 0);
  if (dszp == NULL)    return win_printf("Cannot allocate plot");
  set_ds_source(dszp, "P pid");
  dszi = create_and_attach_one_ds(opz, nx, nx, 0);
  if (dszi == NULL)    return win_printf("Cannot allocate plot");
  set_ds_source(dszi, "I pid");
  dszd = create_and_attach_one_ds(opz, nx, nx, 0);
  if (dszd == NULL)    return win_printf("Cannot allocate plot");
  set_ds_source(dszd, "D pid");


  opv = create_and_attach_one_plot(pr, nx,nx, 0);
  if (opv == NULL)    return win_printf("Cannot allocate plot");
  dsver = opv->dat[0];
  set_ds_source(dszer, "Rot speep error");
  dsvp = create_and_attach_one_ds(opv, nx, nx, 0);
  if (dsvp == NULL)    return win_printf("Cannot allocate plot");
  set_ds_source(dsvp, "P speed");
  dsvi = create_and_attach_one_ds(opv, nx, nx, 0);
  if (dsvi == NULL)    return win_printf("Cannot allocate plot");
  set_ds_source(dsvi, "I speed");
  */
  snprintf(Command,128,"rtran=0\r\n");

  i = 0;
  /*
  Write_serial_port(hCom,Command,9);
  ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
  for  (; ret <= 0; )
    ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
  */
  ret = sent_cmd_and_read_answer(hCom, Command, answer, 127, &lpNumberOfBytesWritten, &dwErrors, NULL);

  sprintf(Command,"rtran?\r\n");

  //win_printf("0 %s",answer);
  for (i = 0; i < nx; )
    {
      tn = my_uclock() + (get_my_uclocks_per_sec()/100);
      /*
      Write_serial_port(hCom,Command,8);
      ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
      for  (; ret == 0; )
	ret = Read_serial_port(hCom,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
      */
      ret = sent_cmd_and_read_answer(hCom, Command, answer, 127, &lpNumberOfBytesWritten, &dwErrors, NULL);
      if (axe < 2)
	{ 
	  if (sscanf(answer,"%d\t%d\t%d",&a,&b,&c) == 3)
	    {
	      ds->xd[i] = ds2->xd[i] = dst->xd[i] = dsc->xd[i] = der->xd[i] = a;
	      //dszp->xd[i] = dszi->xd[i] = dszd->xd[i] = a;
	      //dsvp->xd[i] = dsvi->xd[i] = a;
	      dsvt->xd[i] = a;
	      //dsver->xd[i] = dszer->xd[i] = a;
	      ds->yd[i] = c;
	      if (i == 0) 
		{
		  dst->yd[i] = ds->yd[i] + sgn*rot_speed_target;
		  dsc->yd[i] = 0;
		  rot_inte = b << 2;
		}
	      else 
		{
		  dst->yd[i] = dst->yd[i-1] + sgn*rot_speed_target;
		  dsc->yd[i] = ds->yd[i] - ds->yd[i-1];
		}
	      dsvt->yd[i] = sgn*rot_speed_target;
	      if (sgn < 0 && dst->yd[i] < roti) dsvt->yd[i] = 0;
	      if (sgn >= 0 && dst->yd[i] > roti) dsvt->yd[i] = 0;
	      //dsver->yd[i] = dsvt->yd[i] - dsc->yd[i];
	      if (sgn < 0) dst->yd[i] = (dst->yd[i] < roti) ? roti : dst->yd[i];
	      else  dst->yd[i] = (dst->yd[i] > roti) ? roti : dst->yd[i];
	      ds2->yd[i] = b;
	      //dszer->yd[i] = dst->yd[i] - ds->yd[i];

	      // dst->yd[i] = rot_target_inst

	      ler = dst->yd[i] -ds->yd[i];  //rot_target_inst - rot_pos; this is the error
	      ler = (ler > 4096) ? 4096 :ler;                  // we saturate the error
	      ler = (ler < -4096) ? -4096 :ler;                // 
	      ier = (int)ler;                                    // this is the error in int
	      iver = rot_speed = dsvt->yd[i] -(int)dsc->yd[i];            
	      //(rot_pos_1 - rot_pos); we compute - the speed in ticks/ms
	      terrorz = ier << 2;
	      terrorz = terrorz >> rot_P;                       // this is the correction for position
	      //dszp->yd[i] = terrorz;
# ifdef OLD
	  // feedback with proportionnal only
	      if (ler > 0)                                       // processing positive velocity
		{
		  iver += rot_speed_target;                     // we get the speed error signal 
		  terrorvz = iver << rot_v_P;                   // correction for velocity feedback with proportionnal only
		  v_servo = (terrorvz < terrorz) ? 1 : 0;        // test which servo is active
		}
	      else
		{
		  iver -= rot_speed_target;                     // we get the speed error signal 
		  terrorvz = iver << rot_v_P;                   // correction for velocity feedback with proportionnal only
		  v_servo = (terrorvz > terrorz) ? 1 : 0;        // test which servo is active
		} 
	      if (v_servo)
		{
		  terrorz = terrorvz;                            // terrorz use velocity feedback
		  rot_inte += iver << rot_v_I;                 // we make the velocity integration
		  //dsvp->yd[i] = terrorvz;
		}
	      else 
		{
# endif
		  rot_inte += ier >> rot_I;                    // we make the position integration 
		  if (rot_D >= 0) 
		    {
		      terrorz += rot_speed << rot_D;             // we add derivative in position feedback
		      //dszd->yd[i] = rot_speed << rot_D;
		    }
# ifdef OLD
		}
# endif
	      rot_inte = (rot_inte > 511) ? 511 : rot_inte;   // we limit the integral factor
	      rot_inte = (rot_inte < -511) ? -511 : rot_inte; // to 64 at the level of PWM
# ifdef OLD
	      if (v_servo)		  dsvi->yd[i] = rot_inte;
	      else
# endif
		//dszi->yd[i] = rot_inte;
	      terrorz += rot_inte >> 2;                         // rot_inte is reduced to keep accuracy
	      
	      if (terrorz < 0) 
		{
		  terrorz -= rot_PWM_offset;       // we add an offset to compensate the motor one
		  if (terrorz < -rot_max_PWM)   terrorz = -((int)rot_max_PWM);
		}
	      else 
		{
		  terrorz += rot_PWM_offset;      // we add an offset to compensate the motor one
		  if (terrorz > rot_max_PWM) 	 terrorz = (int)rot_max_PWM;
		}

	      der->yd[i] = terrorz;
	      
	    }
	  else sprintf(bug,"line %d >%s<len \n",i,answer); 
	  i++;
	  if (i%16 == 0) display_title_message("reading %d >%d %d %d",i,a,b,c);
	}
      else
	{ 
	  if (sscanf(answer,"%d\t%d",&a,&b) == 2)
	    {
	      ds->xd[i] = a;
	      ds->yd[i] = b;
	    }
	  else sprintf(bug,"line %d >%s<len \n",i,answer); 
	  i++;
	  if (i%16 == 0) display_title_message("reading %d >%d %d",i,a,b);
	}
	for ( ; my_uclock() < tn; );
    }
  set_plot_title(op,"\\stack{{Rot timing}{P %d I %d D %d}{pwm %d range %d}{rot_v_P %d rot_v_I %d vm %d}}",
		 rot_P, rot_I, rot_D, rot_max_PWM, rot_PWM_offset, rot_v_P, rot_v_I, rot_speed_target);
  create_attach_select_y_un_to_op(op, 0, 0 ,(float)1/8000, 0, 0, "tr");
  create_attach_select_x_un_to_op(op, IS_SECOND, 0,(float)1, -3, 0, "ms");
  set_plot_x_title(op, "Time");
  set_plot_y_title(op, "Rot");			


  set_plot_title(op2,"\\stack{{dRot/dt timing}{P %d I %d D %d}{pwm %d range %d}{rot_v_P %d rot_v_I %d vm %d}}",
		 rot_P, rot_I, rot_D, rot_max_PWM, rot_PWM_offset, rot_v_P, rot_v_I, rot_speed_target);
  create_attach_select_y_un_to_op(op2, 0, 0 ,(float)1/8, 0, 0, "tr/s");
  create_attach_select_x_un_to_op(op2, IS_SECOND, 0,(float)1, -3, 0, "ms");
  set_plot_x_title(op2, "Time");
  set_plot_y_title(op2, "dRot/dt");			

  set_plot_title(opc,"\\stack{{PWM command}{P %d I %d D %d}{pwm %d range %d}{rot_v_P %d rot_v_I %d vm %d}}",
		 rot_P, rot_I, rot_D, rot_max_PWM, rot_PWM_offset, rot_v_P, rot_v_I, rot_speed_target);
  create_attach_select_x_un_to_op(opc, IS_SECOND, 0,(float)1, -3, 0, "ms");
  set_plot_x_title(opc, "Time");
  set_plot_y_title(opc, "PWM drive");			

  /*
  set_plot_title(opz,"\\stack{{Rot error}{P %d I %d D %d}{pwm %d range %d}{rot_v_P %d rot_v_I %d vm %d}}",
		 rot_P, rot_I, rot_D, rot_max_PWM, rot_PWM_offset, rot_v_P, rot_v_I, rot_speed_target);
  create_attach_select_x_un_to_op(opz, IS_SECOND, 0,(float)1, -3, 0, "ms");
  set_plot_x_title(opz, "Time");
  set_plot_y_title(opz, "PWM drive");			

  set_plot_title(opv,"\\stack{{Rot speed error}{P %d I %d D %d}{pwm %d range %d}{rot_v_P %d rot_v_I %d vm %d}}",
		 rot_P, rot_I, rot_D, rot_max_PWM, rot_PWM_offset, rot_v_P, rot_v_I, rot_speed_target);
  create_attach_select_x_un_to_op(opz, IS_SECOND, 0,(float)1, -3, 0, "ms");
  set_plot_x_title(opv, "Time");
  set_plot_y_title(opv, "PWM drive");			
  */

  set_rot_value(rot);
  refresh_plot(pr, pr->n_op-1);		
  return D_O_K;
}




MENU *serialw_menu(void) 
{
  static MENU mn[32];
 
    
  if (mn[0].text != NULL)	return mn;
  
    add_item_to_menu(mn,"Init serial port", init_port,NULL,0,NULL);
    add_item_to_menu(mn,"Write serial port", write_command_on_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Read serial port", read_on_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Write read serial port", write_read_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Write read n times", n_write_read_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Close serial port", close_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Grab data", grab_plot_from_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Adj. Zmag PID", grab_zmag_plot_from_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Adj. Rot PID", grab_rot_plot_from_serial_port,NULL,0,NULL); 
   add_item_to_menu(mn,"Grab focus err",grab_objective_plot_from_serial_port,NULL,0,NULL);
    add_item_to_menu(mn,"Record T", record_thermal_z,NULL,0,NULL);
    add_item_to_menu(mn,"Test request T", test_request,NULL,0,NULL);
    add_item_to_menu(mn,"Clear error", do_check_for_rs232_error,NULL,0,NULL);


    return mn;
}



MENU *magnetscontrol_plot_menu(void)
{
	static MENU mn[32], *pm = NULL, ref[4];


	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(ref,"Set Z magnet reference", ref_magnets_zmag,NULL,0,NULL);
	add_item_to_menu(ref,"Set Z origin", do_set_z_origin,NULL,0,NULL);
	add_item_to_menu(ref,"Set rotation reference", ref_magnets_rot,NULL,0,NULL);


	pm = preset_mag_z(10, 24, 0.5);
	add_item_to_menu(mn,"Magnets rotation", set_magnets_rot,NULL,0,NULL);
	add_item_to_menu(mn,"Increment rotation", inc_magnets_rot,NULL,0,NULL);
	add_item_to_menu(mn,"\0",	NULL,			NULL,	0, NULL);
	add_item_to_menu(mn,"Read position", read_magnets,NULL,0,NULL);
	add_item_to_menu(mn,"Magnets z position", set_magnets_z,NULL,0,NULL);
	add_item_to_menu(mn,"set Z_{abs}", set_max_abs_zmag,NULL,0,NULL);
	add_item_to_menu(mn,"\0",	NULL,			NULL,	0, NULL);
	add_item_to_menu(mn,"Preset Z",NULL, pm, 0,NULL);
	add_item_to_menu(mn,"\0",	NULL,			NULL,	0, NULL);
	add_item_to_menu(mn,"References",NULL, ref, 0,NULL);
	add_item_to_menu(mn,"\0",	NULL,			NULL,	0, NULL);
	add_item_to_menu(mn,"Rotation velocity",set_magnets_rot_velocity, NULL, 0,NULL);
	add_item_to_menu(mn,"Test com",test_bug, NULL, 0,NULL);

	//	add_item_to_menu(mn,"Search bead", do_search_coilable_bead,NULL,0,NULL);



	return mn;
}




MENU *focus_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Read Objective position", read_Z,NULL,0,NULL);
	add_item_to_menu(mn,"Set Objective position", set_Z,NULL,0,NULL);
	add_item_to_menu(mn,"Move up", move_obj_up,NULL,0,NULL);
	add_item_to_menu(mn,"Move down", move_obj_dwn,NULL,0,NULL);

	return mn;
}



int	Pico_serial_port_main(int argc, char **argv)
{
  add_plot_treat_menu_item("SERIAL PORT", NULL, serialw_menu(), 0, NULL);
  add_plot_treat_menu_item ( "Magnets", NULL, magnetscontrol_plot_menu(), 0, NULL);	
  add_plot_treat_menu_item ( "focus", NULL, focus_plot_menu(), 0, NULL);	
  
  return D_O_K;
}

int	Pico_serial_port_unload(int argc, char **argv)
{
    if (CloseHandle(hCom) == 0) return win_printf("Close Handle FAILED!");
	remove_item_to_menu(plot_treat_menu, "SERIAL PORT", NULL, NULL);
	return D_O_K;
}

int GetPortNumXP2000Vista(WORD vid, WORD pid, char* ser)
{
  //Variables used for Registry access
  HKEY tmpKey, tmpSubKey, tmpPortKey;
  char portKeyString[256];
  DWORD valtype;
  DWORD length = 100;
  char portString[101];
  //Set portnum to -1, so if there is an error we will
  //know by returning a negative port value
  int portNum = -1;
  //  Open keys to get to the key where the port number is located. This key is:
  //  HKLM\System\CurrentControlSet\Enum\USB\Vid_xxxx&Pid_yyyy&Mi_00\zzzz_00\Device  Parameters\PortName
    if (ERROR_SUCCESS == RegOpenKeyEx(HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\", 0,
				      KEY_READ, &tmpKey))
      {
	if (ERROR_SUCCESS == RegOpenKey(tmpKey, "Enum\\USB\\", &tmpSubKey))
	  {
	    //Loop through and replace spaces for WinXP2000Vista
	    int i = 0;
	    while (ser[i] != '\0')
	      {
		if (ser[i] == 0x20)
		  ser[i] = '_';
		i++;
	      }
	    //The portkey string should look like this
	    //"Vid_XXXX&Pid_XXXX&MI_00\\XXXX_00" where the XXXX's are Vid, Pid and serial string  - version less than 5.0
	   
	      //"Vid_XXXX&Pid_XXXX\\XXXX" where the XXXX's are Vid, Pid and serial string - version greater than or equal to 5.0
	      snprintf(portKeyString,256,"Vid_%04x&Pid_%04x&Mi_00\\%s_00\\Device Parameters\\", vid, pid, ser);
	    //If the portkey string is in the registry, then go ahead and open the portname
	    if (ERROR_SUCCESS == RegOpenKeyEx(tmpSubKey, portKeyString, 0, KEY_READ,
					      &tmpPortKey))
	      {
		if (ERROR_SUCCESS == RegQueryValueEx(tmpPortKey, "PortName", NULL, &valtype,
						     (unsigned char *)portString, &length))
		  {
		    // When we obtain this key, it will be in string format of
		    // "COMXX" where XX is the port. Simply make the first three
		    // elements of the string 0, and call the atoi function to obtain
		    // the number of the port.
		    portString[0] = '0';
		    portString[1] = '0';
		    portString[2] = '0';
		    portNum = atoi(portString);
		  }
		//Make sure to close all open keys for cleanup
		RegCloseKey(tmpPortKey);
	      }
	    else
	      {
		snprintf(portKeyString,256,"Vid_%04x&Pid_%04x\\%s\\Device Parameters\\", vid, pid,ser);
		//If the portkey string is in the registry, then go ahead and open the portname
		if (ERROR_SUCCESS == RegOpenKeyEx(tmpSubKey, portKeyString, 0, KEY_READ,
						  &tmpPortKey))
		  {
		    if (ERROR_SUCCESS == RegQueryValueEx(tmpPortKey, "PortName", NULL, &valtype,
							 (unsigned char *)portString, &length))
		      {
			// When we obtain this key, it will be in string format of
			// "COMXX" where XX is the port. Simply make the first three
			// elements of the string 0, and call the atoi function to obtain
			// the number of the port.
			portString[0] = '0';
			portString[1] = '0';
			portString[2] = '0';
			portNum = atoi(portString);
		      }
		    //Make sure to close all open keys for cleanup
		    RegCloseKey(tmpPortKey);
		  }
	      }
	    RegCloseKey(tmpSubKey);
	  }
	RegCloseKey(tmpKey);
      }
  RegCloseKey(HKEY_LOCAL_MACHINE);
    // Return the number of the port the device is connected too
  return portNum;
}


# ifdef C2103
int GetPortNumXP2000Vista(WORD vid, WORD pid, char* ser)
{
  //Variables used for Registry access
  HKEY tmpKey, tmpSubKey, tmpPortKey;
  CString portKeyString;
  DWORD valtype;
  char* portString;
  DWORD length = 100;
  portString = new char[101];
  //Set portnum to -1, so if there is an error we will
  //know by returning a negative port value
  int portNum = -1;
  //  Open keys to get to the key where the port number is located. This key is:
  //  HKLM\System\CurrentControlSet\Enum\USB\Vid_xxxx&Pid_yyyy&Mi_00\zzzz_00\Device
  Parameters\PortName
    if (ERROR_SUCCESS == RegOpenKeyEx(HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\", 0,
				      KEY_READ, &tmpKey))
      {
	if (ERROR_SUCCESS == RegOpenKey(tmpKey, "Enum\\USB\\", &tmpSubKey))
	  {
	    //Loop through and replace spaces for WinXP2000Vista
	    int i = 0;
	    while (ser[i] != '\0')
	      {
		if (ser[i] == 0x20)
		  ser[i] = '_';
		i++;
	      }
	    //The portkey string should look like this
	    //"Vid_XXXX&Pid_XXXX&MI_00\\XXXX_00" where the XXXX's are Vid, Pid and serial string
	    - version less than 5.0
	      //"Vid_XXXX&Pid_XXXX\\XXXX" where the XXXX's are Vid, Pid and serial string -
	      version greater than or equal to 5.0
	      portKeyString.Format("Vid_%04x&Pid_%04x&Mi_00\\%s_00\\Device Parameters\\", vid,
				   pid, ser);
	    //If the portkey string is in the registry, then go ahead and open the portname
	    if (ERROR_SUCCESS == RegOpenKeyEx(tmpSubKey, portKeyString, 0, KEY_READ,
					      &tmpPortKey))
	      {
		if (ERROR_SUCCESS == RegQueryValueEx(tmpPortKey, "PortName", NULL, &valtype,
						     (unsigned char *)portString, &length))
		  {
		    // When we obtain this key, it will be in string format of
		    // "COMXX" where XX is the port. Simply make the first three
		    // elements of the string 0, and call the atoi function to obtain
		    // the number of the port.
		    portString[0] = '0';AN197
					  8 Rev. 0.7
					  portString[1] = '0';
		    portString[2] = '0';
		    portNum = atoi(portString);
		  }
		//Make sure to close all open keys for cleanup
		RegCloseKey(tmpPortKey);
	      }
	    else
	      {
		portKeyString.Format("Vid_%04x&Pid_%04x\\%s\\Device Parameters\\", vid, pid,
				     ser);
		//If the portkey string is in the registry, then go ahead and open the portname
		if (ERROR_SUCCESS == RegOpenKeyEx(tmpSubKey, portKeyString, 0, KEY_READ,
						  &tmpPortKey))
		  {
		    if (ERROR_SUCCESS == RegQueryValueEx(tmpPortKey, "PortName", NULL, &valtype,
							 (unsigned char *)portString, &length))
		      {
			// When we obtain this key, it will be in string format of
			// "COMXX" where XX is the port. Simply make the first three
			// elements of the string 0, and call the atoi function to obtain
			// the number of the port.
			portString[0] = '0';
			portString[1] = '0';
			portString[2] = '0';
			portNum = atoi(portString);
		      }
		    //Make sure to close all open keys for cleanup
		    RegCloseKey(tmpPortKey);
		  }
	      }
	    RegCloseKey(tmpSubKey);
	  }
	RegCloseKey(tmpKey);
      }
  RegCloseKey(HKEY_LOCAL_MACHINE);
  delete portString;
  // Return the number of the port the device is connected too
  return portNum;
}
#endif
#endif
