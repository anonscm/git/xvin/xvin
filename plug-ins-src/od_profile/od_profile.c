/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _OD_PROFILE_C_
#define _OD_PROFILE_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 
# include <windowsx.h>
# include <windows.h>

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "od_profile.h"
//place here other headers of this plugin 


char *debug_file = "c:\\serial_debug.txt";
int debug = DEBUG;
FILE *fp_debug = NULL;

unsigned long t_rs = 0, dt_rs;
UINT timeout = 500;  // ms
char last_answer_2[128];

static int direction = 0, freq = 5, minu = 60, nbac = 96, iac = 0, first_id = 1;
clock_t start = 0;
char save_path[512];
char save_file[512];



///  SERIAL FUNCTIONS  ///

HANDLE init_serial_port(unsigned short port_number, int baudrate, int n_bits, int parity, int stops, int RTSCTS)
{
    HANDLE hCom_port;
    //win_printf("entering init ");

    sprintf( str_com, "\\\\.\\COM%d\0", port_number);
    hCom_port = CreateFile(str_com,GENERIC_READ|GENERIC_WRITE,0,NULL,
        OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,NULL);
    PurgeComm(hCom_port, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
    if (GetCommState( hCom_port, &lpCC.dcb) == 0) 
    {
        win_printf("GetCommState FAILED\n port %d baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
		   ,port_number, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }    

    /* Initialisation des parametres par defaut */
    /*http://msdn.microsoft.com/library/default.asp?url=/library/en-us/devio/base/dcb_str.asp*/
    //win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.BaudRate = baudrate;//CBR_57600;//CBR_115200;
    //lpCC.dcb.BaudRate = CBR_115200;
    //win_printf("lpCC.dcb.BaudRate = %d",lpCC.dcb.BaudRate);
    lpCC.dcb.ByteSize = n_bits; //8;
    lpCC.dcb.StopBits = (stops == 1) ? ONESTOPBIT : TWOSTOPBITS;//stops;//ONESTOPBIT;
    lpCC.dcb.Parity = parity;//NOPARITY;
    lpCC.dcb.fOutX = FALSE;
    lpCC.dcb.fInX = FALSE;


    if (RTSCTS == 0)
      {
	lpCC.dcb.fDtrControl = DTR_CONTROL_DISABLE;//DTR_CONTROL_HANDSHAKE or DTR_CONTROL_DISABLE;
	lpCC.dcb.fRtsControl = RTS_CONTROL_DISABLE;//RTS_CONTROL_HANDSHAKE
      }
    else
      {
	lpCC.dcb.fDtrControl = DTR_CONTROL_HANDSHAKE;// or DTR_CONTROL_DISABLE;
	lpCC.dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
	lpCC.dcb.fOutxCtsFlow = TRUE;
      }
 
 
    if (SetCommState( hCom_port, &lpCC.dcb )== 0) 
    {
        win_printf("SetCommState FAILED\n port %d baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
		   ,port_number, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }    

    //Delay(60);

    if (GetCommTimeouts(hCom_port,&lpTo)== 0) 
    {
        win_printf("GetCommTimeouts FAILED\n port %d baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
		   ,port_number, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }
        
    lpTo.ReadIntervalTimeout = timeout;   // 100 ms max entre characters
    lpTo.ReadTotalTimeoutMultiplier = 1;
    lpTo.ReadTotalTimeoutConstant = 1;
    lpTo.WriteTotalTimeoutMultiplier = 1;
    lpTo.WriteTotalTimeoutConstant = 1;
    
    if (SetCommTimeouts(hCom_port,&lpTo)== 0) 
    {
        win_printf("SetCommTimeouts FAILED\n port %d baudrate %d \nbits %d parity %d stops %d RTSCTS %d\n"
		   ,port_number, baudrate, n_bits, parity, stops, RTSCTS);
        return NULL;
    }    
    
    if (SetupComm(hCom_port,2048,2048) == 0) 
    {
        win_printf("Init Serial port %d FAILED",port_number);
        return NULL;
    }
    my_set_window_title("Init Serial port %d OK",port_number);
    return hCom_port;
}

int purge_com(HANDLE hCom)
{
  if (hCom == NULL) return 1; 
  return PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
}

int Write_serial_port(HANDLE handle, char *data, DWORD length)
{
  int success = 0;
  DWORD dwWritten = 0;
  OVERLAPPED o= {0};

  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  if (!WriteFile(handle, (LPCVOID)data, length, &dwWritten, &o))
    {
      if (GetLastError() == ERROR_IO_PENDING)
	if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	  if (GetOverlappedResult(handle, &o, &dwWritten, FALSE))
	    success = 1;
    }
  else    success = 1;
  if (dwWritten != length)     success = 0;
  CloseHandle(o.hEvent);
  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  if (dwWritten != length)    
	    fprintf (fp_debug,">pb writting %d char instead of %d\n",(int)dwWritten,(int)length);
	  fprintf (fp_debug,">\t %s\n",data);
	  fclose(fp_debug);
	}
    }
  return (success == 0) ? -1 : dwWritten;
}


int ReadData(HANDLE handle, BYTE* data, DWORD length, DWORD* dwRead)
{
  int success = 0;
  OVERLAPPED o ={0};

  o.hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  if (!ReadFile(handle, data, length, dwRead, &o))
    {
      if (GetLastError() == ERROR_IO_PENDING)
	if (WaitForSingleObject(o.hEvent, timeout) == WAIT_OBJECT_0)
	  success = 1;
      GetOverlappedResult(handle, &o, dwRead, FALSE);
    }
  else    success = 1;
  CloseHandle(o.hEvent);
  return success;
}


int init_port(void)
{
  int i;
  static int port_number = 1, sbaud = 0, hand = 0;

  if(updating_menu_state != 0)	return D_O_K;

  if (hCom != NULL) return  win_printf_OK("Init Serial port in use"); 
  i = win_scanf("Port number ?%5d\nDump in and out in a debug file\n"
		"No %R yes %r\n"
		"Baudrate 57.6k %R 115.2k %r 230.4k %r 460.8k %r 921.6k %r\n"
		"HandShaking None %R Cts/Rts %r"
		,&port_number,&debug,&sbaud,&hand);
  if (i == CANCEL) return D_O_K;
  hCom = init_serial_port(port_number, pow(2,sbaud)*57600, 8, 0, 1, 0);
  if (hCom == NULL) return 1;
  if (debug)
    {
      fp_debug = fopen(debug_file,"w");
      if (fp_debug != NULL) fclose(fp_debug);
    }
  purge_com(hCom);
  
  return D_O_K;
}


int Read_serial_port(HANDLE handle, char *stuff, DWORD max_size, DWORD *dwRead)
{
  register int i, j;
  unsigned long t0;
  char lch[2];
  int timeout = 0;
  
  if (handle == NULL) return -2;


  t0 = get_my_uclocks_per_sec()/20; // we cannot wait more than 50 ms
  t0 += my_uclock();
  for (i = j = 0; j == 0 && i < max_size-1 && timeout == 0; )
    {
      *dwRead = 0;
      if (ReadData(handle, lch, 1, dwRead) == 0)
	{
	  stuff[i] = 0;
	  *dwRead = i;
	  if (debug)
	    {
	      fp_debug = fopen(debug_file,"a");
	      if (fp_debug != NULL)
		{
		  fprintf (fp_debug,"<-- ReadData error\n");
		  fprintf (fp_debug,"<\t %s (t = %g)\n",stuff,(double)(my_uclock()-t_rs)/dt_rs);
		  fclose(fp_debug);
		}
	    }
	  return -1;
	}
      if (*dwRead == 1) 
	{
	  stuff[i] = lch[0];
	  j = (lch[0] == '\n') ? 1 : 0;
	  i++;
	}
      timeout = (t0 > my_uclock()) ? 0 : 1;
    }
  stuff[i] = 0;
  *dwRead = i;

  if (debug)
    {
      fp_debug = fopen(debug_file,"a");
      if (fp_debug != NULL)
	{
	  if (timeout) fprintf (fp_debug,"<\t timeout %s (t = %g)\n",stuff,(double)(my_uclock()-t_rs)/dt_rs);
	  else fprintf (fp_debug,"<\t %s (t = %g)\n",stuff,(double)(my_uclock()-t_rs)/dt_rs);
	  fclose(fp_debug);
	}
    }
  return (timeout) ? -2 : i;
}

int sent_cmd_and_read_answer(HANDLE hCom, char *Command, char *answer, DWORD n_answer, DWORD *n_written, DWORD *dwErrors, unsigned long *t0)
{
  int rets, ret, i, j;
  COMSTAT comStat;
  char l_command[128], chtmp[128]; // , ch[2]
  DWORD len = 0;

  if (hCom == NULL || Command == NULL || answer == NULL || strlen(Command) < 1) return 1;

    for (i = 0, j = 1; i < 5 && j != 0; i++)
      {
	j = ClearCommError(hCom, dwErrors, &comStat);
	if (j && comStat.cbInQue)   Read_serial_port(hCom,chtmp,127,&len);
	//if (j && comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	//if (j && comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	//if (j && comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	//if (j && comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	//if (j && comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	//if (j && comStat.fEof)      my_set_window_title(" EOF character received");
	if (j && comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
	//if (j && comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	if (j && comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
      }


  for (i = 0; i < 126 && Command[i] != 0 && Command[i] != 13; i++)
    l_command[i] = Command[i]; // we copy until CR
  l_command[i++] = 13;         // we add it
  l_command[i] = 0;            // we end string
  if (t0) *t0 = my_uclock();
  /*
  for (i = 0; i < 126 && l_command[i] != 0; i++)
    {
      ch[0] = l_command[i];
      ch[1] = 0;
      rets = Write_serial_port(hCom,ch,1);
      if (rets <= 0)
	{
	  if (t0) *t0 = my_uclock() - *t0;
	  // Get and clear current errors on the port.
	  if (ClearCommError(hCom, dwErrors, &comStat)) 	
	    {
	      if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
	      return -2;
	    }
	  return -1;
	}
    }
  */

  rets = Write_serial_port(hCom,l_command,strlen(l_command));
  if (rets <= 0)
    {
      if (t0) *t0 = my_uclock() - *t0;
      // Get and clear current errors on the port.
      if (ClearCommError(hCom, dwErrors, &comStat)) 	
	{
	  if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
	  //if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	  //if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	  //if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	  //if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	  //if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	  //if (comStat.fEof)      my_set_window_title(" EOF character received");
	  if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
	  //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	  if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
	  return -2;
	}
      return -1;
    }

  *n_written = 0;
  ret = Read_serial_port(hCom,answer,n_answer,n_written);
  for  (; ret == 0; )
    ret = Read_serial_port(hCom,answer,n_answer,n_written);
  if (t0) *t0 = my_uclock() - *t0;
  answer[*n_written] = 0;
  strncpy(last_answer_2,answer,127);
  if (ret < 0)
    {
      // Get and clear current errors on the port.
      if (ClearCommError(hCom, dwErrors, &comStat)) 	
	{
	  if (comStat.cbInQue)  Read_serial_port(hCom,chtmp,127,&len);
	  //if (comStat.fCtsHold)  my_set_window_title(" Tx waiting for CTS signal");
	  //if (comStat.fDsrHold)  my_set_window_title(" Tx waiting for DSR signal");
	  //if (comStat.fRlsdHold) my_set_window_title(" Tx waiting for RLSD signal");
	  //if (comStat.fXoffHold) my_set_window_title(" Tx waiting, XOFF char rec'd");
	  //if (comStat.fXoffSent) my_set_window_title(" Tx waiting, XOFF char sent");
	  //if (comStat.fEof)      my_set_window_title(" EOF character received");
	  if (comStat.fTxim)     my_set_window_title(" Character waiting for Tx; char queued with TransmitCommChar");
	  //if (comStat.cbInQue)   my_set_window_title(" comStat.cbInQue bytes have been received, but not read");
	  if (comStat.cbOutQue)  my_set_window_title(" comStat.cbOutQue bytes are awaiting transfer");
	  return 2;
	}
      return 1;      
    }
  return 0;
}

int CloseSerialPort(HANDLE hCom)
{
    if (CloseHandle(hCom) == 0) return win_printf("Close Handle FAILED!");
    return D_O_K;
}

int close_serial_port(void)
{
        if(updating_menu_state != 0)	return D_O_K;
        CloseSerialPort(hCom);
	hCom = NULL;
        return D_O_K;
}



int write_command_on_serial_port(void)
{
    static char Command[128]="test";
    
    if(updating_menu_state != 0)	return D_O_K;
    
    win_scanf("Command to send? %s",&Command);
    strcat(Command,"\r");  
    Write_serial_port(hCom,Command,strlen(Command));

    return D_O_K;
}


int read_on_serial_port(void)
{
    char Command[128];
    unsigned long lpNumberOfBytesWritten = 0;
    int ret = 0;
    DWORD nNumberOfBytesToRead = 127;
    unsigned long t0;
    
    if(updating_menu_state != 0)	return D_O_K;

    t0 = my_uclock();        
    ret = Read_serial_port(hCom,Command,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
    t0 = my_uclock() - t0;
    Command[lpNumberOfBytesWritten] = 0;

    win_printf("command read = %s \n ret %d lpNumberOfBytesWritten = %d\n in %g mS",Command,ret,lpNumberOfBytesWritten,1000*(double)(t0)/get_my_uclocks_per_sec());

    return D_O_K;
}


int write_read_serial_port(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    if (hCom == NULL) return win_printf_OK("No serial port init");
    
        write_command_on_serial_port();

        read_on_serial_port();

        return D_O_K;
}


int n_write_read_serial_port(void)
{
    static char Command[128]="dac16?";
    static int ntimes = 1, i;
    unsigned long t0;
    double dt, dtm, dtmax;
    char resu[128], *ch;
    int ret = 0;
    unsigned long lpNumberOfBytesWritten = 0;
    DWORD nNumberOfBytesToRead = 127;
    
    if(updating_menu_state != 0)	return D_O_K;

    if (hCom == NULL) return win_printf_OK("No serial port init�");    
    for (ch = Command; *ch != 0; ch++) 
      if (*ch == '\r') *ch = 0;
    win_scanf("Command to send? %snumber of times %d",&Command,&ntimes);
       
    strcat(Command,"\r");  

    for(i = 0, dtm = dtmax = 0; i < ntimes; i++)
      {
	t0 = my_uclock();    
	Write_serial_port(hCom,Command,strlen(Command));
	ret = Read_serial_port(hCom,resu,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
	for  (; ret <= 0; )
	  ret = Read_serial_port(hCom,resu,nNumberOfBytesToRead,&lpNumberOfBytesWritten);

	t0 = my_uclock() - t0;
	dt = 1000*(double)(t0)/get_my_uclocks_per_sec();
	resu[lpNumberOfBytesWritten] = 0;
	dtm += dt;
	if (dt > dtmax) dtmax = dt;
      }
    dtm /= ntimes;
    win_printf("command read = %s \n ret %d Number Of Bytes  = %d\nin %g mS in avg %g max",resu,ret,lpNumberOfBytesWritten,dtm, dtmax);
    return D_O_K;
}


///////////////////////////////////////////
///  Functions for profile acquisition  ///
///////////////////////////////////////////

// Idle action for OD profile plot
int odp_op_idle_action(O_p *op, DIALOG *d)
{
  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;
  if (op->need_to_refresh)      d->proc(MSG_DRAW,d,0);	
  return 0;
}

// Define a timer to acquire OD every second
VOID CALLBACK dummyAPCProc(LPVOID lpArgToCompletionRoutine, DWORD dwTimerLowValue, DWORD dwTimerHighValue)
{
  return;
}

DWORD WINAPI AcquisitionThreadProc( LPVOID lpParam ) 
{
  HANDLE hTimer = NULL;
  LARGE_INTEGER liDueTime;
  DIALOG *starting_one = NULL;
  opd *lopd;
  d_s *lds;
  float Tstart, dT, od=0;
  int i, freq = 1;
  static int nx;
  char fullname[1024];

#ifndef FAKE_RS232 
  static char command[128];
  char resu[128];
  int ret = 0;
  unsigned long len = 0;
/*   unsigned long lpNumberOfBytesWritten = 0; */
/*   DWORD nNumberOfBytesToRead = 127; */
  DWORD dwErrors;
# endif

  // Access to dataset
  lopd = (opd*)lpParam;
  if (lopd == NULL)
    return 0;
  if (lopd->op == NULL)
    return 0;
  if (lopd->op->dat == NULL)
    return 0;
  lds = lopd->op->dat[0];
  if (lds == NULL)
    return 0;

  if (lopd->Temp == NULL)
    return 0;
  Tstart = lopd->Temp[0];
  dT = lopd->Temp[1] - lopd->Temp[0];

  freq = lopd->freq;

  // Create a waitable timer.
  if (freq == 0)
    return 1;
#ifndef FAKE_RS232 
  liDueTime.QuadPart=-10000000/freq; // 100ns*10000000 =1000ms
#else
  liDueTime.QuadPart=-100000/freq;   // 100x faster
#endif
  hTimer = CreateWaitableTimer(NULL, TRUE, "WaitableTimer2");
  if (!hTimer)   win_printf_OK("CreateWaitableTimer failed (%d)\n", GetLastError());
  starting_one = active_dialog; // to check if windows change
  source_running = 1;
  nx = 0;
  while (nx < lds->mx && source_running == 1)
    {
      // Set a timer to wait for 1 seconds.
      if (!SetWaitableTimer(hTimer, &liDueTime, 0, dummyAPCProc, NULL, 0))
	win_printf_OK("SetWaitableTimer failed (%d)\n", GetLastError());

      // Wait for the timer.
      SleepEx (INFINITE, TRUE);

      // To be executed at each timer overflow
#ifndef FAKE_RS232 
      sprintf(command, "OD?\r");
/*       Write_serial_port(hComMC, command, strlen(command)); */
/*       ret = Read_serial_port(hComMC,resu,nNumberOfBytesToRead,&lpNumberOfBytesWritten); */
/*       for  (; ret <= 0; ) */
/* 	ret = Read_serial_port(hComMC,resu,nNumberOfBytesToRead,&lpNumberOfBytesWritten); */
/*       sscanf(resu, "OD is %f", &od); */
      ret = sent_cmd_and_read_answer(hComMC, command, resu, 127, &len, &dwErrors, NULL);
      if ((ret != 0) || (sscanf(resu, "OD is %f", &od) != 1))
	od = ERROR_VALUE;
#endif
#ifdef FAKE_RS232 
      od = 10*cos((M_PI * nx)/40); 
#endif
      i = direction ? lds->mx - nx - 1 : nx;
      lds->xd[i] = (float) i;
      lds->yd[i] = od;
      nx++;
      //set_plot_title(lopd->op, "dir %d i %d  mx %d nx %d", direction, i, lds->mx, nx);
      lopd->op->need_to_refresh = 1;

      if (nx%10 == 0)
	{
	  append_filename(fullname, lopd->op->dir, lopd->op->filename, 1024);
  	  save_one_plot(lopd->op, fullname);
	}
    }

  CloseHandle(hThread);

  return 0;
}

int create_acquisition_thread(opd *lopd)
{
  DWORD dwThreadId;
    
  hThread = CreateThread( 
			 NULL,              // default security attributes
			 0,                 // use default stack size  
			 AcquisitionThreadProc,// thread function 
			 (void*)lopd,       // argument to thread function 
			 0,                 // use default creation flags 
			 &dwThreadId);      // returns the thread identifier 

  if (hThread == NULL) 	win_printf_OK("No thread created");
  SetThreadPriority(hThread,  THREAD_PRIORITY_NORMAL);

  return 0;
}

// Param query, initialize plot and start acquisition
int start_one_od_profile(int nb, int direction, int freq)
{
  pltreg *pr;
  O_p *op;
  d_s *ds;
  int i, nx = NB_ACQ;
  float Temp[2];
  int do_save(void);
#ifndef FAKE_RS232 
  int ret = 0;
  long int motMax;
  char command[128], answer[128]="";
  unsigned long lpNumberOfBytesWritten = 0;
  DWORD nNumberOfBytesToRead = 127;
#endif

  // Create and allocate plot
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return 1;

  nx *= freq;    // NB_ACQ points for 1Hz
  op = create_and_attach_one_plot(pr, nx, nx, 0);
  if (op == NULL)    return 2;
  ds = op->dat[0];
  if (ds == NULL)    return 3;
  op->op_idle_action = odp_op_idle_action;

  //set_plot_symb(ds, "\\pt5\\oc");
  set_plot_title(op, "OD Profile on %s", ctime(&(ds->time)));

  //Temp[0] = 37;
  //Temp[1] = 65;
#ifdef TEMP_INFO
#ifndef FAKE_RS232 
  for (i=0; i<2; i++)
    {
      sprintf(command,"T%d?\r", i);
      Write_serial_port(hComMC,command,strlen(command));
      ret = Read_serial_port(hComMC,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
      for  (; ret <= 0; )
	ret = Read_serial_port(hComMC,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);

      sprintf(command, "T%d is at %%f", i);
      sscanf(answer, command, (Temp+i));
      // sscanf(answer, command, Temp + (direction?i:(1-i)));
    }
  my_set_window_title("T[0]: %f\nT[1]: %f\n", Temp[0], Temp[1]);
# endif
  set_ds_source(ds, "T0: %2.3f, T1: %2.3f", Temp[0], Temp[1]);

  create_attach_select_x_un_to_op(op, 0, Temp[0], (Temp[1]-Temp[0])/nx, 0, 0, "{}^oC");
  set_plot_x_prime_title(op, "Temperature");
  op->c_xu_p = op->c_xu;
#endif
  create_attach_select_x_un_to_op(op, IS_METER, 0, (float) L_PER_SEC/freq, -3, 0, "mm");
  set_plot_x_title(op,"Position");
  set_plot_y_title(op,"OD");

  // Set plot params
  ds->nx = ds->mx;
  ds->ny = ds->mx;
  dopd.pr = pr;
  dopd.op = op;
  dopd.Temp = Temp;
  dopd.freq = freq;
  set_op_filename(op, "OD_profile_%03d.gr",nb);
  if (nb == 0)
    {
      do_save();
      strcpy(save_path, op->dir);
      strcpy(save_file, op->filename);
      for (i = strlen(save_file) -1; i > 0 && save_file[i] != '_'; i--);
      if (sscanf(save_file+i+1, "%d", &first_id) != 1)
	  first_id = 0;
      save_file[i] = 0;
    }
  else 
    {
      set_op_path(op, "%s",save_path);
      set_op_filename(op, "%s_%03d.gr",save_file,first_id+nb);
    }

#ifndef FAKE_RS232 
  // Set motor speed and start motion
  sprintf(command,"cvel(450)\r\n");
  Write_serial_port(hComMot,command,strlen(command));
  ret = Read_serial_port(hComMot,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
  for  (; ret <= 0; )
    ret = Read_serial_port(hComMot,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);

  motMax = direction ? 0 : MOT_MAX;
  sprintf(command,"cmove(%ld,0)\r\n", motMax);
  Write_serial_port(hComMot,command,strlen(command));
  ret = Read_serial_port(hComMot,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
  for  (; ret <= 0; )
    ret = Read_serial_port(hComMot,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
# endif

  // Start acquisition
  create_acquisition_thread(&dopd);

  return 0;
}

int record_profiles_idle_action(DIALOG *d)
{
  if ((clock() - start) < (iac * minu * 60 * CLOCKS_PER_SEC)) return 0;

  direction = (direction ? 0 : 1);  
  if (start_one_od_profile(iac, direction, freq) != 0)
    {
      win_printf("Problem allocating plot \nAborting record");
      general_idle_action = NULL;
    }  
  iac++;
  if (iac >= nbac)
    {
      general_idle_action = NULL;
      //win_title_used = 0;
    }
  return 0;
}


// Param query, initialize plot and start acquisition
int start_one_acquisition(void)
{
  int  i;

  if(updating_menu_state != 0)	return D_O_K;

#ifndef FAKE_RS232 

  if (hComMC != NULL) return win_printf("Cannot open connexion on uC port");
  hComMC = init_serial_port(uC_PORT, uC_BAUD, 8, 0, 1, 0);
  if (hComMC == NULL) return 1;
  purge_com(hComMC);

  if (hComMot != NULL) return win_printf("Cannot open connexion on motor port");
  hComMot = init_serial_port(MOTOR_PORT, MOTOR_BAUD, 8, 0, 1, 0);
  if (hComMot == NULL) return 1;
  purge_com(hComMot);

  // User prompt
  i = win_printf("Please check that LED and laser are ON\n");
  if (i == CANCEL) return D_O_K;
# endif
  i = win_scanf("Please check that the motor is at bottom or top position\n\n"
		"%R Bottom to top\t%r Top to bottom\n"
		"Acquisition frequency %3d / sec\n"
		,&direction,&freq);
  if (i == CANCEL) return D_O_K;

  start_one_od_profile(0, direction, freq);
  return 0;   
}

int start_timelapse_acquisition(void)
{
  int  i;

  if(updating_menu_state != 0)	return D_O_K;

#ifndef FAKE_RS232 

  if (hComMC != NULL) return win_printf("Cannot open connexion on uC port");
  hComMC = init_serial_port(uC_PORT, uC_BAUD, 8, 0, 1, 0);
  if (hComMC == NULL) return 1;
  purge_com(hComMC);

  if (hComMot != NULL) return win_printf("Cannot open connexion on motor port");
  hComMot = init_serial_port(MOTOR_PORT, MOTOR_BAUD, 8, 0, 1, 0);
  if (hComMot == NULL) return 1;
  purge_com(hComMot);

  // User prompt
  i = win_printf("Please switch the laser ON\n");
  if (i == CANCEL) return D_O_K;
# endif
  i = win_scanf("Please check that the motor is at bottom or top position\n\n"
		/* "%R Bottom to top\t%r Top to bottom\n" */
		"%R Zero to Max (Top to bottom)\t%r Max to Zero (Bottom to top)\n"
		"Acquisition frequency %3d / sec\n"
		"Number of profiles to record %5d\n"
		"Number of minutes between records %5d\n"
		,&direction,&freq,&nbac,&minu);
  if (i == CANCEL) return D_O_K;

  start = clock();
  iac = 0;

  direction = (direction ? 0 : 1);  // must be inverted as idle action change it
  general_idle_action = record_profiles_idle_action;
  //win_title_used = 1;  // make window title writable even with refresh
  return 0;
}

int killThread(void)
{
  source_running = 0;

  return 0;
}

int stop_acquisition()
{
#ifndef FAKE_RS232 
  int  ret = 0;
  char command[128], answer[128]="";
  unsigned long lpNumberOfBytesWritten = 0;
  DWORD nNumberOfBytesToRead = 127;
#endif
  
  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_S, 0, stop_acquisition);
      return D_O_K;
    }

#ifndef FAKE_RS232 
  sprintf(command,"halt()\r\n");
  Write_serial_port(hComMot,command,strlen(command));
  ret = Read_serial_port(hComMot,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);
  for  (; ret <= 0; )
    ret = Read_serial_port(hComMot,answer,nNumberOfBytesToRead,&lpNumberOfBytesWritten);

/*   CloseSerialPort(hComMC); */
/*   CloseSerialPort(hComMot); */
/*   hComMC = NULL; */
/*   hComMot = NULL; */

#endif
  killThread();

  return D_O_K;
}

int stop_timelapse(void)
{
  if(updating_menu_state != 0)	return D_O_K;

  general_idle_action = NULL;
  
  return D_O_K;
}


///////////////////////////////////////////
///          Menu Functions             ///
///////////////////////////////////////////

MENU *serialw_menu(void) 
{
  static MENU mn[32];
 
    
  if (mn[0].text != NULL)	return mn;
  
  add_item_to_menu(mn,"Init serial port", init_port,NULL,0,NULL);
  add_item_to_menu(mn,"Write serial port", write_command_on_serial_port,NULL,0,NULL);
  add_item_to_menu(mn,"Read serial port", read_on_serial_port,NULL,0,NULL);
  add_item_to_menu(mn,"Write read serial port", write_read_serial_port,NULL,0,NULL);
  add_item_to_menu(mn,"Write read n times", n_write_read_serial_port,NULL,0,NULL);
  add_item_to_menu(mn,"Close serial port", close_serial_port,NULL,0,NULL);
  return mn;
}


MENU *od_profile_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Start OD profile acquisition", start_one_acquisition,NULL,0,NULL);
  add_item_to_menu(mn,"Start timelapse OD profile acquisition", start_timelapse_acquisition,NULL,0,NULL);
  add_item_to_menu(mn,"Stop current OD profile acquisition", stop_acquisition,NULL,0,NULL);
  add_item_to_menu(mn,"Stop timelapse acquisition", stop_timelapse,NULL,0,NULL);

  return mn;
}

int	od_profile_main(int argc, char **argv)
{
  add_plot_treat_menu_item("Serial Port", NULL, serialw_menu(), 0, NULL);
  add_plot_treat_menu_item("OD profile", NULL, od_profile_plot_menu(), 0, NULL);
  return D_O_K;
}

int	od_profile_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "Serial Port", NULL, NULL);
  remove_item_to_menu(plot_treat_menu, "OD profile", NULL, NULL);
  return D_O_K;
}
#endif

