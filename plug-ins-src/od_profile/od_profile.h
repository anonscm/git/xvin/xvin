#ifndef _OD_PROFILE_H_
#define _OD_PROFILE_H_

#define DEBUG       1
//#define TEMP_INFO       // uncomment to print temp axis and save T0 and t1 in source
#define ERROR_VALUE 1e-9  // value when rs232 error

#define READ_ONLY   0
#define WRITE_ONLY  1
#define WRITE_READ  2

#define uCONTROLLER 0
#define MOTOR       1

#define uC_PORT     6
#define uC_BAUD     57600
#define MOTOR_PORT  7
#define MOTOR_BAUD  9600

#define MOT_SPEED   450
/* #define MOT_MAX     115000 */
/* #define NB_ACQ      252 */
/* #define MOT_MAX     148000  // 375mm (large thin chamber) */
/* #define NB_ACQ      316 */
#define MOT_MAX     75000 // 200mm (small thin chamber)
#define NB_ACQ      161
#define CHEMO_LENGTH 200

#define L_PER_SEC   1.186708    // 375mm in 316sec
#define OD_OFFSET   6
// offset between od and laser is 17000

typedef struct od_profile_data
{
  pltreg *pr;
  O_p *op;
  float *Temp;
  int freq;
} opd;


# ifndef _OD_PROFILE_C_
PXV_VAR(HANDLE, hCom);
PXV_VAR(HANDLE, hComMC);
PXV_VAR(HANDLE, hComMot);
PXV_VAR(COMMTIMEOUTS, lpTo);
PXV_VAR(COMMCONFIG, lpCC);
PXV_VAR(int, source_running);
PXV_VAR(char, str_com[32]);

PXV_VAR(opd, dopd);
# endif

# ifdef _OD_PROFILE_C_

HANDLE hThread = NULL;
HANDLE hCom = NULL, hComMC = NULL, hComMot = NULL;
COMMTIMEOUTS lpTo;
COMMCONFIG lpCC;
int source_running = 0;
char str_com[32];

opd dopd;                 // the data passed to the thread proc

# endif

PXV_FUNC(MENU*, serialw_menu, (void));
PXV_FUNC(MENU*, od_profile_plot_menu, (void));
PXV_FUNC(int, od_profile_main, (int argc, char **argv));

#endif

