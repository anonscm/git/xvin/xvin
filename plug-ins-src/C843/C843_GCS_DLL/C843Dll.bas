Attribute VB_Name = "MC843Dll"
Option Explicit

''''''''''''''''''''''''''''''''
' low level C843-QMC commands. '
''''''''''''''''''''''''''''''''
Public Const ADJUST_ACTUAL_POSITION = &HF5
Public Const CLEAR_INTERRUPT = &HAC
Public Const CLEAR_POSITION_ERROR = &H47
Public Const GET_ACTIVITY_STATUS = &HA6
Public Const GET_ACTUAL_VELOCITY = &HAD
Public Const GET_CAPTURE_VALUE = &H36
Public Const GET_CHECKSUM = &HF8
Public Const GET_COMMANDED_ACCELERATION = &HA7
Public Const GET_COMMANDED_POSITION = &H1D
Public Const GET_COMMANDED_VELOCITY = &H1E
Public Const GET_CURRENT_MOTOR_COMMAND = &H3A
Public Const GET_DERIVATIVE = &H9B
Public Const GET_EVENT_STATUS = &H31
Public Const GET_HOST_IO_ERROR = &HA5
Public Const GET_INTEGRAL = &H9A
Public Const GET_INTERRUPT_AXIS = &HE1
Public Const GET_PHASE_COMMAND = &HEA
Public Const GET_POSITION_ERROR = &H99
Public Const GET_SIGNAL_STATUS = &HA4
Public Const GET_TIME = &H3E
Public Const GET_TRACE_COUNT = &HBB
Public Const GET_TRACE_STATUS = &HBA
Public Const GET_VERSION = &H8F
Public Const INITIALIZE_PHASE = &H7A
Public Const MULTI_UPDATE = &H5B
Public Const NO_OPERATION = &H0
Public Const READ_ANALOG = &HEF
Public Const READ_BUFFER = &HC9
Public Const READ_IO = &H83
Public Const RESET = &H39
Public Const RESET_EVENT_STATUS = &H34
Public Const SET_ACCELERATION = &H90
Public Const GET_ACCELERATION = &H4C
Public Const SET_ACTUAL_POSITION = &H4D
Public Const GET_ACTUAL_POSITION = &H37
Public Const SET_ACTUAL_POSITION_UNITS = &HBE
Public Const GET_ACTUAL_POSITION_UNITS = &HBF
Public Const SET_AUTO_STOP_MODE = &HD2
Public Const GET_AUTO_STOP_MODE = &HD3
Public Const SET_AXIS_MODE = &H87
Public Const GET_AXIS_MODE = &H88
Public Const SET_AXIS_OUT_SOURCE = &HED
Public Const GET_AXIS_OUT_SOURCE = &HEE
Public Const SET_BREAKPOINT = &HD4
Public Const GET_BREAKPOINT = &HD5
Public Const GET_BREAKPOINT_VALUE = &HD7
Public Const SET_BUFFER_FUNCTION = &HCA
Public Const GET_BUFFER_FUNCTION = &HCB
Public Const SET_BUFFER_LENGTH = &HC2
Public Const GET_BUFFER_LENGTH = &HC3
Public Const SET_BUFFER_READ_INDEX = &HC6
Public Const GET_BUFFER_READ_INDEX = &HC7
Public Const SET_BUFFER_START = &HC0
Public Const GET_BUFFER_START = &HC1
Public Const SET_BUFFER_WRITE_INDEX = &HC4
Public Const GET_BUFFER_WRITE_INDEX = &HC5
Public Const SET_CAPTURE_SOURCE = &HD8
Public Const GET_CAPTURE_SOURCE = &HD9
Public Const SET_COMMUTATION_MODE = &HE2
Public Const GET_COMMUTATION_MODE = &HE3
Public Const SET_DECELERATION = &H91
Public Const GET_DECELERATION = &H92
Public Const SET_DERIVATIVE_TIME = &H9C
Public Const GET_DERIVATIVE_TIME = &H9D
Public Const SET_DIAGNOSTIC_PORT_MODE = &H89
Public Const GET_DIAGNOSTIC_PORT_MODE = &H8A
Public Const SET_ENCODER_MODULUS = &H8E
Public Const GET_ENCODER_MODULUS = &H8D
Public Const SET_ENCODER_SOURCE = &HDA
Public Const GET_ENCODER_SOURCE = &HDB
Public Const SET_ENCODER_TO_STEP_RATIO = &HDE
Public Const GET_ENCODER_TO_STEP_RATIO = &HDF
Public Const SET_GEAR_MASTER = &HAE
Public Const GET_GEAR_MASTER = &HAF
Public Const SET_GEAR_RATIO = &H14
Public Const GET_GEAR_RATIO = &H59
Public Const SET_INTEGRATION_LIMIT = &H95
Public Const GET_INTEGRATION_LIMIT = &H96
Public Const SET_INTERRUPT_MASK = &H2F
Public Const GET_INTERRUPT_MASK = &H56
Public Const SET_JERK = &H13
Public Const GET_JERK = &H58
Public Const SET_KAFF = &H93
Public Const SET_KD = &H27
Public Const GET_KD = &H52
Public Const SET_KI = &H26
Public Const GET_KI = &H51
Public Const SET_KOUT = &H9E
Public Const GET_KOUT = &H9F
Public Const SET_KP = &H25
Public Const GET_KP = &H50
Public Const SET_KVFF = &H2B
Public Const GET_KVFF = &H54
Public Const SET_LIMIT_SWITCH_MODE = &H80
Public Const GET_LIMIT_SWITCH_MODE = &H81
Public Const SET_MOTION_COMPLETE_MODE = &HEB
Public Const GET_MOTION_COMPLETE_MODE = &HEC
Public Const SET_MOTOR_BIAS = &HF
Public Const GET_MOTOR_BIAS = &H2D
Public Const SET_MOTOR_COMMAND = &H77
Public Const GET_MOTOR_COMMAND = &H69
Public Const SET_MOTOR_LIMIT = &H6
Public Const GET_MOTOR_LIMIT = &H7
Public Const SET_MOTOR_MODE = &HDC
Public Const GET_MOTOR_MODE = &HDD
Public Const SET_NUMBER_PHASES = &H85
Public Const GET_NUMBER_PHASES = &H86
Public Const SET_OUTPUT_MODE = &HE0
Public Const GET_OUTPUT_MODE = &H6E
Public Const SET_PHASE_ANGLE = &H84
Public Const GET_PHASE_ANGLE = &H2C
Public Const SET_PHASE_CORRECTION_MODE = &HE8
Public Const GET_PHASE_CORRECTION_MODE = &HE9
Public Const SET_PHASE_COUNTS = &H75
Public Const GET_PHASE_COUNTS = &H7D
Public Const SET_PHASE_INITIALIZE_MODE = &HE4
Public Const GET_PHASE_INITIALIZE_MODE = &HE5
Public Const SET_PHASE_INITIALIZE_TIME = &H72
Public Const GET_PHASE_INITIALIZE_TIME = &H7C
Public Const SET_PHASE_OFFSET = &H76
Public Const GET_PHASE_OFFSET = &H7B
Public Const SET_PHASE_PRESCALE = &HE6
Public Const GET_PHASE_PRESCALE = &HE7
Public Const SET_POSITION = &H10
Public Const GET_POSITION = &H4A
Public Const SET_POSITION_ERROR_LIMIT = &H97
Public Const GET_POSITION_ERROR_LIMIT = &H98
Public Const SET_PROFILE_MODE = &HA0
Public Const GET_PROFILE_MODE = &HA1
Public Const SET_SAMPLE_TIME = &H38
Public Const GET_SAMPLE_TIME = &H61
Public Const SET_SERIAL_PORT_MODE = &H8B
Public Const GET_SERIAL_PORT_MODE = &H8C
Public Const SET_SETTLE_TIME = &HAA
Public Const GET_SETTLE_TIME = &HAB
Public Const SET_SETTLE_WINDOW = &HBC
Public Const GET_SETTLE_WINDOW = &HBD
Public Const SET_SIGNAL_SENSE = &HA2
Public Const GET_SIGNAL_SENSE = &HA3
Public Const SET_START_VELOCITY = &H6A
Public Const GET_START_VELOCITY = &H6B
Public Const SET_STEP_RANGE = &HCF
Public Const GET_STEP_RANGE = &HCE
Public Const SET_STOP_MODE = &HD0
Public Const GET_STOP_MODE = &HD1
Public Const SET_TRACE_MODE = &HB0
Public Const GET_TRACE_MODE = &HB1
Public Const SET_TRACE_PERIOD = &HB8
Public Const GET_TRACE_PERIOD = &HB9
Public Const SET_TRACE_START = &HB2
Public Const GET_TRACE_START = &HB3
Public Const SET_TRACE_STOP = &HB4
Public Const GET_TRACE_STOP = &HB5
Public Const SET_TRACE_VARIABLE = &HB6
Public Const GET_TRACE_VARIABLE = &HB7
Public Const SET_TRACKING_WINDOW = &HA8
Public Const GET_TRACKING_WINDOW = &HA9
Public Const SET_VELOCITY = &H11
Public Const GET_VELOCITY = &H4B
Public Const UPDATE = &H1A
Public Const WRITE_BUFFER = &HC8
Public Const WRITE_IO = &H82





'************************************************************
'*                                                          *
'*                  Only for DLL-Version.                   *
'*                                                          *
'************************************************************


'////////////////////////////////////////////
'// DLL initialization and comm functions. //
'////////////////////////////////////////////
Public Declare Function C843_Connect Lib "C843_GCS_DLL.dll" ( _
                                ByVal iBoardNumber As Long _
                                ) As Long
                                
Public Declare Function C843_IsConnected Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long _
                                ) As Long
                                
Public Declare Sub C843_CloseConnection Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long _
                                )
                                
Public Declare Function C843_GetError Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long _
                                ) As Long
                                
Public Declare Function C843_SetErrorCheck Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal bErrorCheck As Long _
                                ) As Long
                                
Public Declare Function C843_TranslateError Lib "C843_GCS_DLL.dll" ( _
                                ByVal errNr As Long, _
                                ByVal szBuffer As String, _
                                ByVal maxlen As Long _
                                ) As Long
                                
Public Declare Function C843_GetCurrentBoardId Lib "C843_GCS_DLL.dll" ( _
                                ByVal iBoardNumber As Long _
                                ) As Long
                                

'/////////////////////////////
'// special C843 functions. //
'/////////////////////////////
Public Declare Function C843_CLR Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String _
                                ) As Long
                                
Public Declare Function C843_DFF Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pdValarray As Double _
                                ) As Long
                                
Public Declare Function C843_qDFF Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pdValarray As Double _
                                ) As Long

Public Declare Function C843_SMO Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pnValarray As Long _
                                ) As Long
                                
Public Declare Function C843_qSMO Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pnValarray As Long _
                                ) As Long
                                
Public Declare Function C843_SVO Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbValarray As Long _
                                ) As Long
                                
Public Declare Function C843_qSVO Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbValarray As Long _
                                ) As Long

Public Declare Function C843_IsReferenceOK Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbValarray As Long _
                                ) As Long
                                
Public Declare Function C843_SPA Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef iCmdarray As Long, _
                                ByRef dValarray As Double, _
                                ByVal sStageNames As String _
                                ) As Long
                                
Public Declare Function C843_qSPA Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef iCmdarray As Long, _
                                ByRef dValarray As Double, _
                                ByVal sStageNames As String, _
                                ByVal iMaxNamesLength As Long _
                                ) As Long

Public Declare Function C843_STE Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal cAxis As Byte, _
                                ByVal dPos As Double _
                                ) As Long
                                
Public Declare Function C843_qSTE Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal cAxis As Byte, _
                                ByVal iOffset As Long, _
                                ByVal nrValues As Long, _
                                ByRef pdValarray As Double _
                                ) As Long
                                
' config stages
Public Declare Function C843_SAI Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szOldAxes As String, _
                                ByVal szNewAxes As String _
                                ) As Long

Public Declare Function C843_qSAI Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByVal maxlen As Long) _
                                As Long
                                
Public Declare Function C843_qTVI Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByVal maxlen As Long _
                                ) As Long
                                
Public Declare Function C843_qCST Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByVal names As String, _
                                ByVal maxlen As Long _
                                ) As Long
                                
Public Declare Function C843_CST Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByVal names As String _
                                ) As Long
                                
Public Declare Function C843_qVST Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szBuffer As String, _
                                ByVal maxlen As Long _
                                ) As Long


'//////////////
'// general. //
'//////////////
Public Declare Function C843_qIDN Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szBuffer As String, _
                                ByVal maxlen As Long _
                                ) As Long

Public Declare Function C843_qERR Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByRef pnError As Long _
                                ) As Long

Public Declare Function C843_INI Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String _
                                ) As Long

Public Declare Function C843_MOV Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pdValarray As Double _
                                ) As Long

Public Declare Function C843_qMOV Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pdValarray As Double _
                                ) As Long

Public Declare Function C843_MVR Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pdValarray As Double _
                                ) As Long
                                
Public Declare Function C843_qONT Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbValarray As Long _
                                ) As Long

Public Declare Function C843_POS Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pdValarray As Double _
                                ) As Long

Public Declare Function C843_qPOS Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pdValarray As Double _
                                ) As Long

Public Declare Function C843_HLT Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String _
                                ) As Long

Public Declare Function C843_STP Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long _
                                ) As Long

Public Declare Function C843_VEL Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pdValarray As Double _
                                ) As Long

Public Declare Function C843_qVEL Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pdValarray As Double _
                                ) As Long

Public Declare Function C843_IsMoving Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbValarray As Long _
                                ) As Long

Public Declare Function C843_WAI Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long _
                                ) As Long

Public Declare Function C843_GetInputChannelNames Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szBuffer As String, _
                                ByVal maxlen As Long _
                                ) As Long
                                
Public Declare Function C843_GetOutputChannelNames Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szBuffer As String, _
                                ByVal maxlen As Long _
                                ) As Long
                                
Public Declare Function C843_DIO Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szChannels As String, _
                                ByRef pbValarray As Long _
                                ) As Long
                                
Public Declare Function C843_qDIO Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szChannels As String, _
                                ByRef pbValarray As Long _
                                ) As Long
                                
Public Declare Function C843_qHLP Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal sbuffer As String, _
                                ByVal maxlen As Long) As Long


'/////////////
'// limits. //
'/////////////
Public Declare Function C843_MNL Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String _
                                ) As Long
                                
Public Declare Function C843_MPL Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String _
                                ) As Long
                                
Public Declare Function C843_REF Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String _
                                ) As Long
                                
Public Declare Function C843_qREF Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbValarray As Long _
                                ) As Long
                                
Public Declare Function C843_qLIM Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbValarray As Long _
                                ) As Long
                                
Public Declare Function C843_GetRefResult Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pnResult As Long _
                                ) As Long
                                
Public Declare Function C843_IsReferencing Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbIsReferencing As Long _
                                ) As Long
                                
                                
Public Declare Function C843_qTMN Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbValarray As Double _
                                ) As Long
                                
Public Declare Function C843_qTMX Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbValarray As Double _
                                ) As Long
                                
Public Declare Function C843_DFH Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String _
                                ) As Long
                                
Public Declare Function C843_qDFH Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbValarray As Double _
                                ) As Long
                                
Public Declare Function C843_GOH Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String _
                                ) As Long
                                
Public Declare Function C843_RON Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbValarray As Long _
                                ) As Long
                                
Public Declare Function C843_qRON Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbValarray As Long _
                                ) As Long

                                

'/////////////////////
'// "old" commands. //
'/////////////////////
Public Declare Function C843_BRA Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String, _
                                ByRef pbValarray As Long _
                                ) As Long
                                
Public Declare Function C843_qBRA Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szBuffer As String, _
                                ByVal maxlen As Long _
                                ) As Long
                                


'//////////////////////
'// String commands. //
'//////////////////////

Public Declare Function C843_C843Commandset Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szCommand As String, _
                                ByVal szAwnser As String, _
                                ByVal iMaxSize As Long _
                                ) As Long
                                
Public Declare Function C843_GcsCommandset Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szCommand As String _
                                ) As Long

Public Declare Function C843_GcsGetAnswer Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAnswer As String, _
                                ByVal iBufferSize As Long _
                                ) As Long
                                
Public Declare Function C843_GcsGetAnswerSize Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByRef iAnswerSize As Long _
                                ) As Long



'///////////////////
'// QMC commands. //
'///////////////////

Public Declare Function C843_SetQMC Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal bCmd As Byte, _
                                ByVal bAxis As Byte, _
                                ByVal Param As Long _
                                ) As Long
                                
Public Declare Function C843_GetQMC Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal bCmd As Byte, _
                                ByVal bAxis As Byte, _
                                ByRef pResult As Long _
                                ) As Long
                                
Public Declare Function C843_SetQMCA Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal bCmd As Byte, _
                                ByVal bAxis As Byte, _
                                ByVal Param1 As Integer, _
                                ByVal lParam2 As Long _
                                ) As Long
                                
Public Declare Function C843_GetQMCA Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal bCmd As Byte, _
                                ByVal bAxis As Byte, _
                                ByVal lParam As Integer, _
                                ByRef pResult As Long _
                                ) As Long



'///////////////
'// Specieal. //
'///////////////
Public Declare Function C843_AddStage Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szAxes As String _
                                ) As Long

Public Declare Function C843_RemoveStage Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long, _
                                ByVal szStageName As String _
                                ) As Long

Public Declare Function C843_OpenUserStagesEditDialog Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long _
                                ) As Long
                                
Public Declare Function C843_OpenPiStagesEditDialog Lib "C843_GCS_DLL.dll" ( _
                                ByVal ID As Long _
                                ) As Long

