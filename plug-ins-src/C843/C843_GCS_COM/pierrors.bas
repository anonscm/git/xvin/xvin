' PIErrors.bas
'
' This file defines symbols for each error code used in C/C++ programs.
'
' This file is automagically generated from the central error code list.
' DO NOT ADD ERROR CODES IN THIS FILE! Use the error list and the generation tool instead!

Attribute VB_Name = "MPiErrors"
Option Explicit

'
' Dll Errors - DLL Errors occured in GCS DLL
'
Public Const PI_UNKNOWN_AXIS_IDENTIFIER = -1001                'Unknown axis identifier
Public Const PI_NR_NAV_OUT_OF_RANGE = -1002                'Number for NAV out of range - must be in [1,10000]
Public Const PI_INVALID_SGA = -1003                'Invalid value for SGA - must be one of {1, 10, 100, 1000}
Public Const PI_UNEXPECTED_RESPONSE = -1004                'Controller has sent unexpected response
Public Const PI_NO_MANUAL_PAD = -1005                'No manual control pad installed, calls to SMA and related commands are not allowed
Public Const PI_INVALID_MANUAL_PAD_KNOB = -1006                'Invalid number for manual control pad knob
Public Const PI_INVALID_MANUAL_PAD_AXIS = -1007                'Axis not currently controlled by a manual control pad
Public Const PI_CONTROLLER_BUSY = -1008                'Controller is busy with some lengthy operation (e.g. reference movement, fast scan algorithm)
Public Const PI_THREAD_ERROR = -1009                'Internal error - could not start thread
Public Const PI_IN_MACRO_MODE = -1010                'Controller is (already) in macro mode - command not valid in macro mode
Public Const PI_NOT_IN_MACRO_MODE = -1011                'Controller not in macro mode - command not valid unless macro mode active
Public Const PI_MACRO_FILE_ERROR = -1012                'Could not open file to write macro or to read macro
Public Const PI_NO_MACRO_OR_EMPTY = -1013                'No macro with given name on controller or macro is empty
Public Const PI_MACRO_EDITOR_ERROR = -1014                'Internal error in macro editor
Public Const PI_INVALID_ARGUMENT = -1015                'One of the arguments given to the function is invalid (empty string, index out of range, ...)
Public Const PI_AXIS_ALREADY_EXISTS = -1016                'Axis identifier is already in use for a connected stage
Public Const PI_INVALID_AXIS_IDENTIFIER = -1017                'Invalid axis identifier
Public Const PI_COM_ARRAY_ERROR = -1018                'Could not access array data in COM server
Public Const PI_COM_ARRAY_RANGE_ERROR = -1019                'Range of array does not fit the number of parameters
Public Const PI_INVALID_SPA_CMD_ID = -1020                'Command ID given to SPA or SPA? is not valid
Public Const PI_NR_AVG_OUT_OF_RANGE = -1021                'Number for AVG out of range - must be >0
Public Const PI_WAV_SAMPLES_OUT_OF_RANGE = -1022                'Number of samples given to WAV out of range
Public Const PI_WAV_FAILED = -1023                'Generation of wave failed
Public Const PI_MOTION_ERROR = -1024                'motion error while axis was moving
Public Const PI_RUNNING_MACRO = -1025                'Controller is (already) running a macro
Public Const PI_PZT_CONFIG_FAILED = -1026                'Configuration of PZT stage or amplifier failed.
Public Const PI_PZT_CONFIG_INVALID_PARAMS = -1027                'Current settings are not valid for desired configuration.
Public Const PI_UNKNOWN_CHANNEL_IDENTIFIER = -1028                'Unknown channel identifier
Public Const PI_WAVE_PARAM_FILE_ERROR = -1029                'Error while reading/writing to wave generator parameter file.
Public Const PI_UNKNOWN_WAVE_MACRO = -1030                'Could not find description of wave form. Maybe WG.INI is missing?
Public Const PI_WAVE_MACRO_FUNC_NOT_LOADED = -1031                'The WGMacro DLL function was not found at startup
Public Const PI_USER_CANCELLED = -1032                'The user cancelled a dialog
Public Const PI_C844_ERROR = -1033                'Error from the C-844 Controller
Public Const PI_DLL_NOT_LOADED = -1034                'DLL neccessary to call function not loaded, or function not found in DLL
Public Const PI_PARAMETER_FILE_PROTECTED = -1035                'The opened parameter file is protected and cannot be edited
Public Const PI_NO_PARAMETER_FILE_OPENED = -1036                'There is no parameter file opened
Public Const PI_STAGE_DOES_NOT_EXIST = -1037                'The selected stages does not exist
Public Const PI_PARAMETER_FILE_ALREADY_OPENED = -1038                'There is already a parameter file opened. Please close this file before openig a new file
Public Const PI_PARAMETER_FILE_OPEN_ERROR = -1039                'DLL neccessary to call function not loaded, or function not found in DLL
Public Const PI_INVALID_CONTROLLER_VERSION = -1040                'The Version of the connected controller is invalid.
Public Const PI_PARAM_SET_ERROR = -1041                'parameter could not be set with SPA, parameter on controller undefined!
Public Const PI_NUMBER_OF_POSSIBLE_WAVES_EXCEEDED = -1042                'The Number of the possible waves has exceeded
Public Const PI_NUMBER_OF_POSSIBLE_GENERATORS_EXCEEDED = -1043                'The Number of the possible waves generators has exceeded
Public Const PI_NO_WAVE_FOR_AXIS_DEFINED = -1044                'There is no wave for the given axis defind
Public Const PI_CANT_STOP_OR_START_WAV = -1045                'You can't stop a wave of an axis if it's already stopped, or start it if it's already started
Public Const PI_REFERENCE_ERROR = -1046                'Not all axes could be referenced
Public Const PI_REQUIRED_WAVE_MACRO_NOT_FOUND = -1047                'Could not find parameter set, required by frequency relation.
Public Const PI_INVALID_SPP_CMD_ID = -1048                'Command ID given to SPP or SPP? is not valid
Public Const PI_STAGE_NAME_ISNT_UNIQUE = -1049                'A stagename given to CST isn't unique
Public Const PI_FILE_TRANSFER_BEGIN_MISSING = -1050                'A uuencoded file transfered did not start with \"begin\" and the proprer filename
Public Const PI_FILE_TRANSFER_ERROR_TEMP_FILE = -1051                'Could not create/read file on host PC
Public Const PI_FILE_TRANSFER_CRC_ERROR = -1052                'Checksum error when transfering a file to/from the controller
Public Const PI_COULDNT_FIND_PISTAGES_DAT = -1053                'The database PiStages.dat could not be found. This file is required to connect a Stage with the CST command
Public Const PI_NO_WAVE_RUNNING = -1054                'There is no wave running for the specified Axis
Public Const PI_INVALID_PASSWORD = -1055                'Invalid Password
Public Const PI_OPM_COM_ERROR = -1056                'Error during communication with OPM, maybe no OPM connected
'
'  End of Dll Errors
'

'
' Controller Errors - Errors set by the controller or the GCS DLL
'
Public Const PI_CNTR_NO_ERROR = 0                'No error
Public Const PI_CNTR_PARAM_SYNTAX = 1                'Parameter syntax error
Public Const PI_CNTR_UNKNOWN_COMMAND = 2                'Unknown command
Public Const PI_CNTR_COMMAND_TOO_LONG = 3                'Command length out of limit or command buffer overrun
Public Const PI_CNTR_SCAN_ERROR = 4                'Scan error - Needs to be specified!
Public Const PI_CNTR_MOVE_WITHOUT_REF_OR_NO_SERVO = 5                'Unallowable move attempted on unreferenced axis, or move attempted with servo off
Public Const PI_CNTR_INVALID_SGA_PARAM = 6                'Parameter for SGA not valid
Public Const PI_CNTR_POS_OUT_OF_LIMITS = 7                'Position out of limits
Public Const PI_CNTR_VEL_OUT_OF_LIMITS = 8                'Velocity out of limits
Public Const PI_CNTR_SET_PIVOT_NOT_POSSIBLE = 9                'Attempt to set pivot point while U,V or W is not equal 0
Public Const PI_CNTR_STOP = 10                'Controller was stopped
Public Const PI_CNTR_SST_OR_SCAN_RANGE = 11                'Parameter for SST or for one of the embedded scan algorithms out of range
Public Const PI_CNTR_INVALID_SCAN_AXES = 12                'Invalid axis combination for fast scan
Public Const PI_CNTR_INVALID_NAV_PARAM = 13                'Parameter for NAV out of range
Public Const PI_CNTR_INVALID_ANALOG_INPUT = 14                'Invalid analog channel
Public Const PI_CNTR_INVALID_AXIS_IDENTIFIER = 15                'Invalid axis identifier
Public Const PI_CNTR_INVALID_STAGE_NAME = 16                'Unknown stage name
Public Const PI_CNTR_PARAM_OUT_OF_RANGE = 17                'Parameter out of range
Public Const PI_CNTR_INVALID_MACRO_NAME = 18                'Invalid macro name
Public Const PI_CNTR_MACRO_RECORD = 19                'Error while recording macro
Public Const PI_CNTR_MACRO_NOT_FOUND = 20                'Macro not found
Public Const PI_CNTR_AXIS_HAS_NO_BRAKE = 21                'Axis has no brake
Public Const PI_CNTR_DOUBLE_AXIS = 22                'Axis identifier given more than once
Public Const PI_CNTR_ILLEGAL_AXIS = 23                'Illegal axis
Public Const PI_CNTR_PARAM_NR = 24                'Incorrect number of parameters
Public Const PI_CNTR_INVALID_REAL_NR = 25                'Invalid floating point number
Public Const PI_CNTR_MISSING_PARAM = 26                'Missing parameter
Public Const PI_CNTR_SOFT_LIMIT_OUT_OF_RANGE = 27                'Soft limit out of range
Public Const PI_CNTR_NO_MANUAL_PAD = 28                'No manual pad connected
Public Const PI_CNTR_NO_JUMP = 29                'No more step response values
Public Const PI_CNTR_INVALID_JUMP = 30                'No step response values recorded
Public Const PI_CNTR_AXIS_HAS_NO_REFERENCE = 31                'Axis has no reference sensor
Public Const PI_CNTR_STAGE_HAS_NO_LIM_SWITCH = 32                'Axis has no limit switch
Public Const PI_CNTR_NO_RELAY_CARD = 33                'No relay card installed
Public Const PI_CNTR_CMD_NOT_ALLOWED_FOR_STAGE = 34                'The last command was not allowed for selected stage(s)
Public Const PI_CNTR_NO_DIGITAL_INPUT = 35                'No digital input installed
Public Const PI_CNTR_NO_DIGITAL_OUTPUT = 36                'No digital output installed
Public Const PI_CNTR_INVALID_CNTR_NUMBER = 39                'Controller number invalid
Public Const PI_CNTR_NO_JOYSTICK_CONNECTED = 40                'No joystick connected
Public Const PI_CNTR_INVALID_EGE_AXIS = 41                'Invalid axis for electronic gearing, axis can not be slave
Public Const PI_CNTR_SLAVE_POSITION_OUT_OF_RANGE = 42                'Position of slave axis is out of range
Public Const PI_CNTR_COMMAND_EGE_SLAVE = 43                'Slave Axis cannot be commanded directly when electronic gearing is enabled
Public Const PI_CNTR_JOYSTICK_CALIBRATION_FAILED = 44                'Calibration of joystick failed
Public Const PI_CNTR_REFERENCING_FAILED = 45                'Referencing failed
Public Const PI_CNTR_OPM_MISSING = 46                'OPM (Optical Power Meter) Missing
Public Const PI_CNTR_OPM_NOT_INITIALIZED = 47                'OPM (Optical Power Meter) cannot be initialized/ is not initialized
Public Const PI_CNTR_OPM_COM_ERROR = 48                'OPM (Optical Power Meter) Communication Error
Public Const PI_CNTR_MOVE_TO_LIMIT_SWITCH_FAILED = 49                'Move to limit switch failed
Public Const PI_CNTR_REF_WITH_REF_DISABLED = 50                'Reference attempted on an axis with referencing disabled
Public Const PI_CNTR_AXIS_UNDER_JOYSTICK_CONTROL = 51                'Selected axis is controlled by joystick
Public Const PI_CNTR_COMMUNICATION_ERROR = 52                'Controller detected communication error
Public Const PI_CNTR_NO_AXIS = 200                'No stage connected
Public Const PI_CNTR_NO_AXIS_PARAM_FILE = 201                'File with axis parameter not found.
Public Const PI_CNTR_INVALID_AXIS_PARAM_FILE = 202                'Invalid axis parameter file
Public Const PI_CNTR_NO_AXIS_PARAM_BACKUP = 203                'Backup file with axis parameter not found.
Public Const PI_CNTR_RESERVED_204 = 204                'PI internal error code 204
Public Const PI_CNTR_SMO_WITH_SERVO_ON = 205                'SMO with servo on
Public Const PI_CNTR_UUDECODE_INCOMPLETE_HEADER = 206                'uudecode : incomplete header
Public Const PI_CNTR_UUDECODE_NOTHING_TO_DECODE = 207                'uudecode : nothing to decode
Public Const PI_CNTR_UUDECODE_ILLEGAL_FORMAT = 208                'uudecode : illegal UUE format
Public Const PI_CNTR_CRC32_ERROR = 209                'CRC32 error
Public Const PI_CNTR_ILLEGAL_FILENAME = 210                'Illegal file name
Public Const PI_CNTR_SENDING_BUFFER_OVERFLOW = 301                'Sending Buffer Overflow
Public Const PI_CNTR_VOLTAGE_OUT_OF_LIMITS = 302                'Voltage out of limits
Public Const PI_CNTR_VOLTAGE_SET_WHEN_SERVO_ON = 303                'Attempt to set voltage when servo on
Public Const PI_CNTR_RECEIVING_BUFFER_OVERFLOW = 304                'Received command is too long
Public Const PI_CNTR_EEPROM_ERROR = 305                'Error while reading/writing EEPROM
Public Const PI_CNTR_I2C_ERROR = 306                'Error on I2C bus
Public Const PI_CNTR_RECEIVING_TIMEOUT = 307                'Timeout while receiving command
Public Const PI_CNTR_UNKNOWN_ERROR = 555                'Unknown controller error, caused by BasMac
Public Const PI_CNTR_TOO_MANY_NESTED_MACROS = 1000                'Too many nested macros
Public Const PI_CNTR_MACRO_ALREADY_DEFINED = 1001                'Macro already defined
Public Const PI_CNTR_NO_MACRO_RECORDING = 1002                'No macro recording
Public Const PI_CNTR_INVALID_MAC_PARAM = 1003                'Invalid parameter for MAC
Public Const PI_CNTR_RESERVED_1004 = 1004                'PI internal error code 1004
Public Const PI_CNTR_ALREADY_HAS_SERIAL_NUMBER = 2000                'Controller already has a serial number
Public Const PI_CNTR_SECTOR_ERASE_FAILED = 4000                'Sektor Erase failed
Public Const PI_CNTR_FLASH_PROGRAM_FAILED = 4001                'Flash programm failed
Public Const PI_CNTR_FLASH_READ_FAILED = 4002                'Flash read failed
Public Const PI_CNTR_HW_MATCHCODE_ERROR = 4003                'HW Matchcode missing/invalid
Public Const PI_CNTR_FW_MATCHCODE_ERROR = 4004                'FW Matchcode missing/invalid
Public Const PI_CNTR_HW_VERSION_ERROR = 4005                'FW Version missing/invalid
Public Const PI_CNTR_FW_VERSION_ERROR = 4006                'FW Mark missing/invalid
'
'  End of Controller Errors
'

'
' Interface Errors - Interface errors occured while comunicating with the controller
'
Public Const COM_NO_ERROR = 0                'No error occurred during function call
Public Const COM_ERROR = -1                'Error during com operation (could not be specified)
Public Const SEND_ERROR = -2                'Error while sending data
Public Const REC_ERROR = -3                'Error while receiving data
Public Const NOT_CONNECTED_ERROR = -4                'Not connected (no port with given ID open)
Public Const COM_BUFFER_OVERFLOW = -5                'Buffer overflow
Public Const CONNECTION_FAILED = -6                'Error while opening port
Public Const COM_TIMEOUT = -7                'Timeout error
Public Const COM_MULTILINE_RESPONSE = -8                'There are more lines waiting in buffer
Public Const COM_INVALID_ID = -9                'There is no interface open with the given ID
Public Const COM_NOTIFY_EVENT_ERROR = -10                'The event for the notification could not be opened
Public Const COM_NOT_IMPLEMENTED = -11                'The function was not implemented (e.g. only RS-232 communication provides this feature and it was called for IEEE488)
Public Const COM_ECHO_ERROR = -12                'Error while sending \"echoed\" data
Public Const COM_GPIB_EDVR = -13                'IEEE488: System error
Public Const COM_GPIB_ECIC = -14                'IEEE488: Function requires GPIB board to be CIC
Public Const COM_GPIB_ENOL = -15                'IEEE488: Write function detected no Listeners
Public Const COM_GPIB_EADR = -16                'IEEE488: Interface board not addressed correctly
Public Const COM_GPIB_EARG = -17                'IEEE488: Invalid argument to function call
Public Const COM_GPIB_ESAC = -18                'IEEE488: Function requires GPIB board to be SAC
Public Const COM_GPIB_EABO = -19                'IEEE488: I/O operation aborted
Public Const COM_GPIB_ENEB = -20                'IEEE488: Non-existent interface board
Public Const COM_GPIB_EDMA = -21                'IEEE488: Error performing DMA
Public Const COM_GPIB_EOIP = -22                'IEEE488: I/O operation started before previous operation completed
Public Const COM_GPIB_ECAP = -23                'IEEE488: No capability for intended operation
Public Const COM_GPIB_EFSO = -24                'IEEE488: File system operation error
Public Const COM_GPIB_EBUS = -25                'IEEE488: Command error during device call
Public Const COM_GPIB_ESTB = -26                'IEEE488: Serial poll status byte lost
Public Const COM_GPIB_ESRQ = -27                'IEEE488: SRQ remains asserted
Public Const COM_GPIB_ETAB = -28                'IEEE488: The return buffer is full.
Public Const COM_GPIB_ELCK = -29                'IEEE488: Address or board is locked.
Public Const COM_RS_INVALID_DATA_BITS = -30                'RS-232: The use of 5 data bits with 2 stop bits is an invalid combination, as is 6, 7, or 8 data bits with 1.5 stop bits.
Public Const COM_ERROR_RS_SETTINGS = -31                'RS-232: Error when configuring the COM port
Public Const COM_INTERNAL_RESOURCES_ERROR = -32                'Error when dealing with internal system resources (events, threads, ...)
Public Const COM_DLL_FUNC_ERROR = -33                'A DLL or one of the required functions could not be loaded.
'
'  End of Interface Errors
'


