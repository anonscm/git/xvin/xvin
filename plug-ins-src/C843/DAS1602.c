/*
*    Plug-in program for PI linear motors in Xvin.
 *
 *    V. Croquette
 *      JF Allemand
  */
#ifndef _DAS1602_C_
#define _DAS1602_C_


# include "allegro.h"
# include "winalleg.h"
# include <windowsx.h>
# include "ctype.h"
# include "xvin.h"
# include "cbw.h"


# define BUILDING_PLUGINS_DLL
# include "cbw.h"


int BoardNum = 0;
int Channel_out = 1; 
int Channel_in = 1; 
int Range = UNI10VOLTS;







int Init_DAS1602(void)
{
    float RevNum;
    int ErrCode = 0;
    char ErrMsg[ERRSTRLEN];
    char BoardName[256];
    
    
    if(updating_menu_state != 0)	return D_O_K;
    //win_printf("motor id %d",id_C843);               
    i = cbDeclareRevision(&RevNum);
    
    if (i != 0) return win_printf("%s",cbGetErrMsg( ErrCode, ErrMsg));
    
    i = cbGetBoardName(BoardNum, BoardName);
    
    if (i != 0) return win_printf("%s",cbGetErrMsg( ErrCode, ErrMsg));
    else win_printf("%s detected", BoardName);
    
    return 0;
    
}

int set_voltage(float voltage)
{
    int ErrCode = 0;
    char ErrMsg[ERRSTRLEN];
    unsigned short DataValue;
    int i = 0;

    i = cbFromEngUnits(BoardNum,Range,voltage,&DataValue)
    if (i != 0) return win_printf("%s",cbGetErrMsg( ErrCode, ErrMsg));
    
    i = cbAOut(BoardNum,Channel_out,Range,DataValue );
    if (i != 0) return win_printf("%s",cbGetErrMsg( ErrCode, ErrMsg));
    return D_O_K;
}
int Analog_out(void)
{
    float voltage = 0; 
    if(updating_menu_state != 0)	return D_O_K;
    
    win_scanf("Voltage%f(0 < V < 10)",&voltage);
    
    if ( voltage < 0 || voltage > 10) return win_printf("Dummy");
    
    set_voltage(voltage);
    
    return D_O_K;
    
}



float get_voltage(int Chan)
{
    int ErrCode = 0;
    char ErrMsg[ERRSTRLEN];
    unsigned short DataValue;
    int i = 0;
    float voltage;

    i = cbAIn(BoardNum,Chan,Range,&DataValue );
    if (i != 0) return (float)win_printf("%s",cbGetErrMsg( ErrCode, ErrMsg));
    
    i = cbToEngUnits (BoardNum,Range,DataValue,&voltage);
    if (i != 0) return (float)win_printf("%s",cbGetErrMsg( ErrCode, ErrMsg));
    
    return voltage;
}


int Analog_in(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    
    win_printf("Voltage %f",get_voltage(Channel_in));
    
    return D_O_K;
    
}




MENU *DAS1602_menu(void)
{
  static MENU mn[32];
 
    
  if (mn[0].text != NULL)	return mn;
  
  
    add_item_to_menu(mn,"Init DAS1602", Init_DAS1602,NULL,0,NULL);
    add_item_to_menu(mn,"Analog OUT", Analog_out,NULL,0,NULL);
    add_item_to_menu(mn,"Analog IN", Analog_in,NULL,0,NULL);
    
  return mn;
}



int	DAS1602_main(int argc, char **argv)
{
  add_plot_treat_menu_item("DAS1602 ", NULL, DAS1602_menu(), 0, NULL);
	
  Init_DAS1602();
  return D_O_K;
}

int	DAS1602_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "DAS1602", NULL, NULL);
	return D_O_K;
}
#endif

