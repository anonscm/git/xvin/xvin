#ifndef _NR_UTILS_H_
#define _NR_UTILS_H_

# include <platform.h>

#define SQR(a)		((float)(a) == 0.0) ? 0.0 : (float)(a)*(float)(a)
#define DSQR(a)		((double)(a) == 0.0) ? 0.0 : (double)(a)*(double)(a)
#define DMAX(a,b)	((double)(a) > (double)(b)) ?  (double)(a) : (double)(b)
#define DMIN(a,b)	((double)(a) < (double)(b)) ?  (double)(a) : (double)(b)
#define FMAX(a,b)	((float)(a) > (float)(b)) ?  (float)(a) : (float)(b)
#define FMIN(a,b)	((float)(a) < (float)(b)) ?  (float)(a) : (float)(b)
//#define LMAX(a,b)	((long)(a) > (long)(b)) ?  (long)(a) : (long)(b)
//#define LMIN(a,b)	((long)(a) < (long)(b)) ?  (long)(a) : (long)(b)
#define IMAX(a,b)	((int)(a) > (int)(b)) ?  (int)(a) : (int)(b)
#define IMIN(a,b)	((int)(a) < (int)(b)) ?  (int)(a) : (int)(b)
#define SIGN(a,b)	((b) >= 0.0) ? fabs(a) : -fabs(a)


PXV_FUNC(void, nrerror, (char error_text[]));
PXV_FUNC(float, *vector, (int nl, int nh));
PXV_FUNC(int, *ivector, (int nl, int nh));
PXV_FUNC(unsigned, char *cvector, (int nl, int nh));
PXV_FUNC(unsigned, int *lvector, (int nl, int nh));
PXV_FUNC(double, *dvector, (int nl, int nh));
PXV_FUNC(float, **matrix, (int nrl, int nrh, int ncl, int nch));
PXV_FUNC(double, **dmatrix, (int nrl, int nrh, int ncl, int nch));
PXV_FUNC(int, **imatrix, (int nrl, int nrh, int ncl, int nch));
PXV_FUNC(float, **submatrix, (float **a, int oldrl, int oldrh, int oldcl, int oldch,
	int newrl, int newcl));
PXV_FUNC(float, **convert_matrix, (float *a, int nrl, int nrh, int ncl, int nch));
PXV_FUNC(float, ***f3tensor, (int nrl, int nrh, int ncl, int nch, int ndl, int ndh));
PXV_FUNC(void, free_vector, (float *v, int nl, int nh));
PXV_FUNC(void, free_ivector, (int *v, int nl, int nh));
PXV_FUNC(void, free_cvector, (unsigned char *v, int nl, int nh));
PXV_FUNC(void, free_lvector, (unsigned int *v, int nl, int nh));
PXV_FUNC(void, free_dvector, (double *v, int nl, int nh));
PXV_FUNC(void, free_matrix, (float **m, int nrl, int nrh, int ncl, int nch));
PXV_FUNC(void, free_dmatrix, (double **m, int nrl, int nrh, int ncl, int nch));
PXV_FUNC(void, free_imatrix, (int **m, int nrl, int nrh, int ncl, int nch));
PXV_FUNC(void, free_submatrix, (float **b, int nrl, int nrh, int ncl, int nch));
PXV_FUNC(void, free_convert_matrix, (float **b, int nrl, int nrh, int ncl, int nch));
PXV_FUNC(void, free_f3tensor, (float ***t, int nrl, int nrh, int ncl, int nch,
	int ndl, int ndh));
PXV_FUNC(float, gasdev, (int *idum));
PXV_FUNC(float, ran1, (int *idum));
PXV_FUNC(double, ran2, (long long *idum));
PXV_FUNC(void, mrqmin, (float x[], float y[], float sig[], int ndata, float a[], 
	int ia[], int ma, float **covar, float **alpha, float *chisq,
	void (*funcs)(float, float [], float *, float [], int), float *alamda));
PXV_FUNC(void, mrqmin_c, (float x[], float y[], float sig[], int ndata, float a[], 
	int ia[], int ma, float **covar, float **alpha, float *chisq,
	void (*funcs)(float, float [], float *, float [], int), float *alamda));


PXV_FUNC(void, svdfit, (float x[], float y[], float sig[], int ndata, float a[], int ma,
	float **u, float **v, float w[], float *chisq,
			void (*funcs)(float, float [], int)));

PXV_FUNC(void, lfit, (float x[], float y[], float sig[], int ndat, float a[], int ia[],
		      int ma, float **covar, float *chisq, void (*funcs)(float, float [], int)));
PXV_FUNC(float, gammq, (float a, float x));
PXV_FUNC(void, gser, (float *gamser, float a, float x, float *gln));
PXV_FUNC(void, gcf, (float *gammcf, float a, float x, float *gln));
PXV_FUNC(float, gammln, (float xx));
PXV_FUNC(void, mrqcof, (float x[], float y[], float sig[], int ndata, float a[], int ia[],
			int ma, float **alpha, float beta[], float *chisq,
			void (*funcs)(float, float [], float *, float [], int)));
PXV_FUNC(void, gaussj, (float **a, int n, float **b, int m));
PXV_FUNC(void, covsrt, (float **covar, int ma, int ia[], int mfit));
PXV_FUNC(void, svdcmp, (float **a, int m, int n, float w[], float **v));
PXV_FUNC(void, svbksb, (float **u, float w[], float **v, int m, int n, float b[], float x[]));
PXV_FUNC(float, pythag, (float a, float b));
#endif /* _NR_UTILS_H_ */
