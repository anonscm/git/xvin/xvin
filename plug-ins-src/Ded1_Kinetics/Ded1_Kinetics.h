#ifndef _DED1_KINETICS_H_
#define _DED1_KINETICS_H_

PXV_FUNC(int, do_Ded1_Kinetics_rescale_plot, (void));
PXV_FUNC(MENU*, Ded1_Kinetics_plot_menu, (void));
PXV_FUNC(int, do_Ded1_Kinetics_rescale_data_set, (void));
PXV_FUNC(int, Ded1_Kinetics_main, (int argc, char **argv));
#endif

