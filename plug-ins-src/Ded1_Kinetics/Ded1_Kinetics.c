/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _DED1_KINETICS_C_
#define _DED1_KINETICS_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin 
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "Ded1_Kinetics.h"




typedef struct _oligo_ded1
{
  double dt; // pas de temps
  double OD; // the amount of oligo-hybridized to the hairpin in complex with Ded1
  double C;  // the amount of close hairpin
  double O;  // the amount of open hairpin by an oligo
  double D;  // Ded1
  double T; // temps de simulation

  double kon; // Ded1 kon
  double koff; // Ded1 koff
  double To;  // the relaxation time of the oligo hybridized to the hairpin alone
  double Td;  // the relaxation time of the oligo hybridized to the hairpin with Ded1
  double To_1;  // 1/To
  double Td_1;  // 1/Td
  double D0;   // initial [Ded1] 
  double O0;   // initial [O] 
  int iter;


} oligo_ded1;


// equations 
// dOD/dt = (-1/Td).OD + kon.O.D
// dC/dt  = (1/To).O + (1/Td).OD
// dO/dt  = -kon.O.D - (1/To).O 
// dD/dt  = -kon.O.D + (1/Td).OD


// equations [Ded1] = cte
// dOD/dt = (-1/Td).OD + kon.O.D_0
// dC/dt  = (1/To).O + (1/Td).OD
// dO/dt  = -kon.O.D_0 - (1/To).O 


// equations [Ded1] = cte + Koff 
// dOD/dt = (-1/Td).OD + kon.O.D_0 - (1/Toff).OD 
// dC/dt  = (1/To).O + (1/Td).OD
// dO/dt  = -kon.O.D_0 - (1/To).O + (1/Toff).OD 


// equations [Ded1] = cte + Kd
//D * 0 = Kd * OD ;  O + OD = O0
//  (Oo - OD) * D = Kd *Od;   Oo.D = OD*(Kd +D)
//  OD = Oo.D/(Kd+D)
// dOD/dt = (-1/Td).OD + kon.O.D_0 - (1/Toff).OD 
// dC/dt  = (1/To).O + (1/Td).OD
// dO/dt  = -kon.O.D_0 - (1/To).O + (1/Toff).OD 



// equations competitor 
// dOD/dt = (-1/Td).OD + kon.O.D
// dC/dt  = (1/To).O + (1/Td).OD
// dO/dt  = -kon.O.D - (1/To).O 
// dD/dt  = -kon.O.D + (1/Td).OD
// dDX/dt =  
 

int advance_one_step(oligo_ded1 *od)
{
  double k1OD, k2OD, k3OD, k4OD; 
  double k1C, k2C, k3C, k4C;  
  double k1O, k2O, k3O, k4O;     
  double k1D, k2D, k3D, k4D;    
  double OD, C, O, D;

  if (od == NULL) return 1;

  OD = od->OD; C = od->C; O = od->O; D = od->D;

  k1OD = od->dt * (-od->Td_1 * OD + od->kon * O * D);
  k1C = od->dt * (od->To_1 * O + od->Td_1 * OD);
  k1O = od->dt * (-od->kon * O * D -od->To_1 * O);
  k1D = od->dt * (-od->kon * O * D -od->Td_1 * OD);

  k2OD = od->dt * (-od->Td_1 * (OD + k1OD/2) + od->kon * (O + k1O/2) * (D + k1D/2));
  k2C = od->dt * (od->To_1 * (O + k1O/2) + od->Td_1 * (OD + k1OD/2));
  k2O = od->dt * (-od->kon * (O + k1O/2) * (D + k1D/2) -od->To_1 * (O + k1O/2));
  k2D = od->dt * (-od->kon * (O + k1O/2) * (D + k1D/2) -od->Td_1 * (OD + k1OD/2));

  k3OD = od->dt * (-od->Td_1 * (OD + k2OD/2) + od->kon * (O + k2O/2) * (D + k2D/2));
  k3C = od->dt * (od->To_1 * (O + k2O/2) + od->Td_1 * (OD + k2OD/2));
  k3O = od->dt * (-od->kon * (O + k2O/2) * (D + k2D/2) -od->To_1 * (O + k2O/2));
  k3D = od->dt * (-od->kon * (O + k2O/2) * (D + k2D/2) -od->Td_1 * (OD + k2OD/2));

  k4OD = od->dt * (-od->Td_1 * (OD + k3OD) + od->kon * (O + k3O) * (D + k3D));
  k4C = od->dt * (od->To_1 * (O + k3O) + od->Td_1 * (OD + k3OD));
  k4O = od->dt * (-od->kon * (O + k3O) * (D + k3D) -od->To_1 * (O + k3O));
  k4D = od->dt * (-od->kon * (O + k3O) * (D + k3D) -od->Td_1 * (OD + k3OD));

  od->OD = OD + (k1OD + 2*k2OD + 2*k3OD + k4OD)/6;
  od->C = C + (k1C + 2*k2C + 2*k3C + k4C)/6;
  od->O = O + (k1O + 2*k2O + 2*k3O + k4O)/6;
  od->D = D + (k1D + 2*k2D + 2*k3D + k4D)/6;
  od->iter++;
  od->T += od->dt;
  return 0;
}

// equations [Ded1] = cte + Koff 
// dOD/dt = (-1/Td).OD + kon.O.D_0 - (1/Toff).OD 
// dC/dt  = (1/To).O + (1/Td).OD
// dO/dt  = -kon.O.D_0 - (1/To).O + (1/Toff).OD 


int advance_one_step_Ded1_cte(oligo_ded1 *od)
{
  double k1OD, k2OD, k3OD, k4OD; 
  double k1C, k2C, k3C, k4C;  
  double k1O, k2O, k3O, k4O;     
  double OD, C, O, D;

  if (od == NULL) return 1;

  OD = od->OD; C = od->C; O = od->O; D = od->D;

  k1OD = od->dt * (-od->Td_1 * OD + od->kon * O * D - od->koff * OD);
  k1C = od->dt * (od->To_1 * O + od->Td_1 * OD);
  k1O = od->dt * (-od->kon * O * D -od->To_1 * O + od->koff * OD);

  k2OD = od->dt * (-od->Td_1 * (OD + k1OD/2) + od->kon * (O + k1O/2) * D - od->koff * (OD + k1OD/2));
  k2C = od->dt * (od->To_1 * (O + k1O/2) + od->Td_1 * (OD + k1OD/2));
  k2O = od->dt * (-od->kon * (O + k1O/2) * D -od->To_1 * (O + k1O/2) + od->koff * (OD + k1OD/2));

  k3OD = od->dt * (-od->Td_1 * (OD + k2OD/2) + od->kon * (O + k2O/2) * D - od->koff * (OD + k2OD/2));
  k3C = od->dt * (od->To_1 * (O + k2O/2) + od->Td_1 * (OD + k2OD/2));
  k3O = od->dt * (-od->kon * (O + k2O/2) * D -od->To_1 * (O + k2O/2) + od->koff * (OD + k2OD/2));

  k4OD = od->dt * (-od->Td_1 * (OD + k3OD) + od->kon * (O + k3O) * D - od->koff * (OD + k3OD));
  k4C = od->dt * (od->To_1 * (O + k3O) + od->Td_1 * (OD + k3OD));
  k4O = od->dt * (-od->kon * (O + k3O) * D -od->To_1 * (O + k3O) + od->koff * (OD + k3OD));

  od->OD = OD + (k1OD + 2*k2OD + 2*k3OD + k4OD)/6;
  od->C = C + (k1C + 2*k2C + 2*k3C + k4C)/6;
  od->O = O + (k1O + 2*k2O + 2*k3O + k4O)/6;
  od->iter++;
  od->T += od->dt;
  return 0;
}





int do_simul_oligo_ded1(void)
{
  register int i, j;
  O_p *opn = NULL;
  d_s *dso, *dsod, *dsc, *dsd;
  pltreg *pr = NULL;
  static float kon = 1, To = 20, Td = 5, dt = 0.1 ,T = 100, D = 10, O = 0.001;
  static int ndis = 100;
  oligo_ded1 od;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");


  i = win_scanf("Oligo Ded1 experiment.\n"
		"Give Kon %8f (10^6 M^-1 s^-1)\n"
		"Binding time of oligo alone %8f s\n"
		"Binding time of oligo with Ded1 %8f s\n"
		"Time step %8f, Total duration %6f\n"
		"Number of iter between display %6d\n"
		"Initial conditions\n"
		"[Ded1] = %8f (nM)\n"
		"[oligo] = %8f (nM)\n"
		,&kon,&To,&Td,&dt,&T,&ndis,&D,&O);
  if (i == CANCEL)	return OFF;


  if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dso = opn->dat[0]; dso->nx = dso->ny = 0;
  set_ds_source(dso,"Bind oligo alone");

  if ((dsod = create_and_attach_one_ds(opn, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsod->nx = dsod->ny = 0;
  set_ds_source(dsod,"Bind oligo with Ded1");

  if ((dsd = create_and_attach_one_ds(opn, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsd->nx = dsd->ny = 0;
  set_ds_source(dsd,"Ded1");

  if ((dsc = create_and_attach_one_ds(opn, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsc->nx = dsc->ny = 0;
  set_ds_source(dsc,"Close hairpin");

  set_plot_title(opn, "Ded1 oligo experiment");
  set_plot_x_title(opn, "Time");
  set_plot_y_title(opn, "{\\color{yellow}[O]/[O]_0}, {\\color{lightgreen}[OD]/[O]_0}, [D]/[D]_0{\\color{lightblue}, [C]/[O]_0}");

  od.kon = kon * 1e6;
  od.To = To; od.To_1 = (double)1/od.To;
  od.Td = Td; od.Td_1 = (double)1/od.Td;
  od.dt = dt;
  od.T = 0;
  od.iter = 0;
  od.D = od.D0 = 1e-9*D;
  od.O0 = od.O = 1e-9*O;
  od.C = od.OD = 0;


  for (j = 0, od.T = 0; od.T < T; j++)
  {
    advance_one_step(&od);
    add_new_point_to_ds(dso, od.T, od.O/od.O0);
    add_new_point_to_ds(dsc, od.T, od.C/od.O0);
    add_new_point_to_ds(dsod, od.T, od.OD/od.O0);
    add_new_point_to_ds(dsd, od.T, od.D/od.D0);
    opn->need_to_refresh = 1;
    if (j%ndis == 0) refresh_plot(pr, pr->n_op-1);
  }


  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}


int do_simul_oligo_ded1_cte(void)
{
  register int i, j;
  O_p *opn = NULL;
  d_s *dso, *dsod, *dsc;
  pltreg *pr = NULL;
  static float kon = 1, koff = 0, To = 20, Td = 5, dt = 0.1 ,T = 100, D = 10, O = 0.001, n_t_max = 3;
  static int ndis = 100;
  int n_decay = 0, j_max;
  double OD_max = 0, decay_m = 0, C_1, dC, T_stop, T_start;
  oligo_ded1 od;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  i = win_scanf("Oligo Ded1 experiment.\n"
		"Give Kon %8f (10^6 M^-1 s^-1)\n"
		"Give Koff %8f (s^-1)\n"
		"Binding time of oligo alone %8f s\n"
		"Binding time of oligo with Ded1 %8f s\n"
		"Time step %8f, Total duration %6f\n"
		"Number of iter between display %6d\n"
		"Initial conditions\n"
		"[Ded1] = %8f (nM)\n"
		"[oligo] = %8f (nM)\n"
		"I propose to fit the end of the reaction\nto an exponential decay after the max of OD\n"
		"Specify when to start t = %6f x time of T_{max}\n"
		,&kon,&koff,&To,&Td,&dt,&T,&ndis,&D,&O,&n_t_max);
  if (i == CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dso = opn->dat[0]; dso->nx = dso->ny = 0;
  set_ds_source(dso,"Bind oligo alone");

  if ((dsod = create_and_attach_one_ds(opn, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsod->nx = dsod->ny = 0;
  set_ds_source(dsod,"Bind oligo with Ded1");

  if ((dsc = create_and_attach_one_ds(opn, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsc->nx = dsc->ny = 0;
  set_ds_source(dsc,"Close hairpin");


  od.kon = kon * 1e6;
  od.koff = koff;
  od.To = To; od.To_1 = (double)1/od.To;
  od.Td = Td; od.Td_1 = (double)1/od.Td;
  od.dt = dt;
  od.T = 0;
  od.iter = 0;
  od.D = od.D0 = 1e-9*D;
  od.O0 = od.O = 1e-9*O;
  od.C = od.OD = 0;


  set_plot_title(opn, "\\stack{{[Ded1] = cte. oligo experiment}"
		 "{\\pt7 k_{on} = %g x 10^6 M^{-1} s^{-1}, k_{off} = %g s^{-1}, Kd = %g nM T_0 = %g s, T_d = %g s}"
		 "{\\pt7 [Ded1]_0 = %g nM, [Open hairpin] = %g nM}}"
		 ,kon,koff,koff/(kon*1e-3),To,Td,D,O);
  set_plot_x_title(opn, "Time");
  set_plot_y_title(opn, "{\\color{yellow}[O]/[O]_0}, {\\color{lightgreen}[OD]/[O]_0}, {\\color{lightblue}, 1 - [C]/[O]_0}");

  for (j = 0, od.T = 0, C_1 = 0.0, T_stop = T_start = 0; od.T < T; j++)
  {
    advance_one_step_Ded1_cte(&od);
    add_new_point_to_ds(dso, od.T, od.O/od.O0);
    add_new_point_to_ds(dsod, od.T, od.OD/od.O0);
    //add_new_point_to_ds(dsc, od.T, (double)1.0 - od.C/od.O0);
    add_new_point_to_ds(dsc, od.T, (od.O + od.OD)/od.O0);
    if (od.OD > OD_max)
      {
	OD_max = od.OD;
	j_max = j;
      } 
    else if (j > j_max * n_t_max)
      {
	if (fabs((od.O + od.OD)/od.O0) > 1e-12) // (double)1.0 - od.C/od.O0) > 1e-12)
	  {
	    dC = ((od.O + od.OD)/od.O0) - C_1;
	    dC *= 2;
	    dC /= (((od.O + od.OD)/od.O0) + C_1);
	    dC /= dt;
	    decay_m += dC;
	    n_decay++;
	    T_stop = od.T;
	  }
      }
    else T_start = od.T;
    C_1 = (od.O + od.OD)/od.O0;
    opn->need_to_refresh = 1;
    if (j%ndis == 0) refresh_plot(pr, pr->n_op-1);
  }
  //win_printf("decay %g",((double)n_decay)/decay_m);
  set_ds_plot_label(dsc, opn->x_lo + (opn->x_hi - opn->x_lo)/4, 
		    opn->y_hi - (opn->y_hi - opn->y_lo)/4, USR_COORD
		    ,"\\stack{{decay %g S}{start %g S}{stop %g S}}"
		    ,-((double)n_decay)/decay_m,T_start,T_stop);	 


  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int simul_oligo_ded1_cte(float kon, float koff, float To, float Td, float D, float O, float dt, float T, float n_t_max, double *Tau)
{
  register int j;
  oligo_ded1 od;
  int n_decay = 0, j_max;
  double OD_max = 0, decay_m = 0, C_1, dC, T_start;

  od.kon = kon * 1e6;
  od.koff = koff;
  od.To = To; od.To_1 = (double)1/od.To;
  od.Td = Td; od.Td_1 = (double)1/od.Td;
  od.dt = dt;
  od.T = 0;
  od.iter = 0;
  od.D = od.D0 = 1e-9*D;
  od.O0 = od.O = 1e-9*O;
  od.C = od.OD = 0;

  for (j = 0, od.T = 0, C_1 = 0.0, T_start = 0; od.T < T; j++)
  {
    advance_one_step_Ded1_cte(&od);
    if (od.OD > OD_max)
      {
	OD_max = od.OD;
	j_max = j;
      } 
    else if (j > j_max * n_t_max)
      {
	if (fabs((od.O + od.OD)/od.O0) > 1e-12)
	  {
	    dC = ((od.O + od.OD)/od.O0) - C_1;
	    dC *= 2;
	    dC /= (((od.O + od.OD)/od.O0) + C_1);
	    dC /= dt;
	    decay_m += dC;
	    n_decay++;
	  }
      }
    else T_start = od.T;
    C_1 = (od.O + od.OD)/od.O0;
  }
  *Tau = -((double)n_decay)/decay_m;
  return 0;
}


int do_test_simul_oligo_ded1_cte(void)
{
  register int i;
  static float kon = 1, koff = 0, To = 20, Td = 5, dt = 0.1 ,T = 100, D = 10, O = 0.001, n_t_max = 3;
  double Tau = 0;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }

  i = win_scanf("Oligo Ded1 experiment.\n"
		"Give Kon %8f (10^6 M^-1 s^-1)\n"
		"Give Koff %8f (s^-1)\n"
		"Binding time of oligo alone %8f s\n"
		"Binding time of oligo with Ded1 %8f s\n"
		"Time step %8f, Total duration %6f\n"
		"Initial conditions\n"
		"[Ded1] = %8f (nM)\n"
		"[oligo] = %8f (nM)\n"
		"I propose to fit the end of the reaction\nto an exponential decay after the max of OD\n"
		"Specify when to start t = %6f x time of T_{max}\n"
		,&kon,&koff,&To,&Td,&dt,&T,&D,&O,&n_t_max);
  if (i == CANCEL)	return OFF;


  simul_oligo_ded1_cte(kon, koff, To, Td, D, O, dt, T, n_t_max, &Tau);
  win_printf("decay %g",Tau);
 return D_O_K;
}





int do_simul_oligo_ded1_cte_ds(void)
{
  register int i;
  static float kon = 1, koff = 0, To = 20, Td = 5, dt = 0.1 ,T = 100, O = 0.001, n_t_max = 3;
  double Tau = 0, chi2;
  O_p *op = NULL;
  d_s *dsi, *dsd;
  pltreg *pr = NULL;
  float dmin, dmax;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  for (i = 0, dmin = dmax = dsi->xd[i]; i < dsi->nx; i++)
    {
      dmin = (dsi->xd[i] < dmin) ? dsi->xd[i] : dmin;
      dmax = (dsi->xd[i] > dmax) ? dsi->xd[i] : dmax;
    }


  i = win_scanf("Oligo Ded1 experiment.\n"
		"Give Kon %8f (10^6 M^-1 s^-1)\n"
		"Give Koff %8f (s^-1)\n"
		"Binding time of oligo alone %8f s\n"
		"Binding time of oligo with Ded1 %8f s\n"
		"Time step %8f, Total duration %6f\n"
		"Initial conditions\n"
		"[Ded1] = min %6f max %6f (nM)\n"
		"[oligo] = %8f (nM)\n"
		"I propose to fit the end of the reaction\nto an exponential decay after the max of OD\n"
		"Specify when to start t = %6f x time of T_{max}\n"
		,&kon,&koff,&To,&Td,&dt,&T,&dmin,&dmax,&O,&n_t_max);
  if (i == CANCEL)	return OFF;


  if ((dsd = create_and_attach_one_ds(op, dsi->nx, dsi->nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  set_ds_source(dsd,"Ded1 fit k_{on} %g (10^6 M^-1 s^-1) k_{off} %g (s^-1), T_0 %g s T_d %g s",kon,koff,To,Td);

  for (i = 0, chi2 = 0; i < dsi->nx; i++)
    {
      simul_oligo_ded1_cte(kon, koff, To, Td, dsi->xd[i], O, dt, T, n_t_max, &Tau);
      dsd->xd[i] = dsi->xd[i];
      dsd->yd[i] = Tau;
      chi2 += (Tau - dsi->yd[i])*(Tau - dsi->yd[i])/(dsi->ye[i]*dsi->ye[i]);
    }
  set_ds_plot_label(dsd, op->x_lo + (op->x_hi - op->x_lo)/4, 
		    op->y_hi - (op->y_hi - op->y_lo)/4, USR_COORD
		    ,"\\stack{{\\chi^2 %g n %d}{\\pt7 k_{on} %g (10^6 M^{-1} s^{-1}) k_{off} = %g s^{-1}}"
		    "{\\pt7 T_0 %g s T_d %g s}}",chi2,dsi->nx,kon,koff,To,Td);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}





int do_simul_oligo_ded1_competitor(void)
{
  register int i, j;
  O_p *opn = NULL;
  d_s *dso, *dsod, *dsc;
  pltreg *pr = NULL;
  static float kon = 1, To = 20, Td = 5, dt = 0.1 ,T = 100, D = 10, O = 0.001, X = 100, Kd = 100;
  static int ndis = 100;
  float De;
  oligo_ded1 od;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");


  i = win_scanf("Oligo Ded1 experiment.\n"
		"Give Kon of ded1 %8f (10^6 M^-1 s^-1)\n"
		"Binding time of oligo alone %8f s\n"
		"Binding time of oligo with Ded1 %8f s\n"
		"Time step %8f, Total duration %6f\n"
		"Number of iter between display %6d\n"
		"Initial conditions\n"
		"[Ded1] = %8f (nM)\n"
		"[oligo] = %8f (nM)\n"
		"Competitor concentration %8f (nM)\n"
		"Kd competitor Ded1 %8f (nM)\n"
		,&kon,&To,&Td,&dt,&T,&ndis,&D,&O,&X,&Kd);
  if (i == CANCEL)	return OFF;


  if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dso = opn->dat[0]; dso->nx = dso->ny = 0;
  set_ds_source(dso,"Bind oligo alone");

  if ((dsod = create_and_attach_one_ds(opn, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsod->nx = dsod->ny = 0;
  set_ds_source(dsod,"Bind oligo with Ded1");

  if ((dsc = create_and_attach_one_ds(opn, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsc->nx = dsc->ny = 0;
  set_ds_source(dsc,"Close hairpin");


  od.kon = kon * 1e6;
  od.To = To; od.To_1 = (double)1/od.To;
  od.Td = Td; od.Td_1 = (double)1/od.Td;
  od.dt = dt;
  od.T = 0;
  od.iter = 0;
  De = (Kd * D)/(X + Kd); //competitor effect
  od.D = od.D0 = 1e-9*De;
  od.O0 = od.O = 1e-9*O;
  od.C = od.OD = 0;

  set_plot_title(opn, "\\stack{{Ded1 oligo experiment with competitor}"
		 "{\\pt7 k_{on} = %g x 10^6 M^{-1} s^{-1}, T_0 = %g s, T_d = %g s, K_d = %g nM}"
		 "{\\pt7 [Ded1]_0 = %g nM, [Open hairpin] = %g nM, [competitor]_0 = %g nM [Ded1]_{eff} = %g nM}}"
		 ,kon,To,Td,Kd,D,O,X,De);
  set_plot_x_title(opn, "Time");
  set_plot_y_title(opn, "{\\color{yellow}[O]/[O]_0}, {\\color{lightgreen}[OD]/[O]_0}, {\\color{lightblue}, 1 - [C]/[O]_0}");


  for (j = 0, od.T = 0; od.T < T; j++)
  {
    advance_one_step_Ded1_cte(&od);
    add_new_point_to_ds(dso, od.T, od.O/od.O0);
    add_new_point_to_ds(dsc, od.T, 1.0 - od.C/od.O0);
    add_new_point_to_ds(dsod, od.T, od.OD/od.O0);
    opn->need_to_refresh = 1;
    if (j%ndis == 0) refresh_plot(pr, pr->n_op-1);
  }

  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}




MENU *Ded1_Kinetics_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Simul Oligo Ded1", do_simul_oligo_ded1,NULL,0,NULL);
  add_item_to_menu(mn,"Simul Oligo Ded1 cte", do_simul_oligo_ded1_cte,NULL,0,NULL);
  add_item_to_menu(mn,"Test Simul Oligo Ded1 cte", do_test_simul_oligo_ded1_cte,NULL,0,NULL);
  add_item_to_menu(mn,"Simul Oligo Ded1 cte on ds", do_simul_oligo_ded1_cte_ds,NULL,0,NULL);
  add_item_to_menu(mn,"Simul Oligo Ded1 + competitor", do_simul_oligo_ded1_competitor,NULL,0,NULL);



  return mn;
}

int	Ded1_Kinetics_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "Ded1_Kinetics", NULL, Ded1_Kinetics_plot_menu(), 0, NULL);
  return D_O_K;
}

int	Ded1_Kinetics_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "Ded1_Kinetics", NULL, NULL);
  return D_O_K;
}
#endif

