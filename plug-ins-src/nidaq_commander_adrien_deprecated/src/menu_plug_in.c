/** \file menu_plug_in.c
    \brief Plug-in program for menu settings and display.
    
    This is a subprogram that creates, displays and interfaces the menus with the plug-in functions.
    
    \author Adrien Meglio
*/
#ifndef _MENU_PLUG_IN_C
#define _MENU_PLUG_IN_C

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../../nidaqmxwrap_thread/include/nidaqmxwrap_thread.h"
#include "nidaq_commander_adrien.h"

#define BUILDING_PLUGINS_DLL
#include "menu_plug_in.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Menu functions

/** A menu containing a clickable item and a static submenu */
MENU *commander_base_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
//	add_item_to_menu(mn,"Alternate lines",AOTF__sequence__alternate_all,NULL,0,NULL);
//    add_item_to_menu(mn,"Fret model",AOTF__sequence__FRET_three,NULL,0,NULL);
//    add_item_to_menu(mn,"PIFOC alternate",PIFOC__sequence__alternate,NULL,0,NULL);
//    add_item_to_menu(mn,"AOTF alternate",AOTF__sequence__alternate,NULL,0,NULL);
//    add_item_to_menu(mn,"Plot 'lines'",lines__sequence__plot,NULL,0,NULL);
//    add_item_to_menu(mn,"Stop tasks",task__all_close,NULL,0,NULL);
//    add_item_to_menu(mn,"Change task frequency",lines__get_frequency,NULL,0,NULL);
//
//	//add_item_to_menu(mn,"Stop",task__all_close,NULL,0,NULL);
//    add_item_to_menu(mn,"Set PIFOC",PIFOC__set_MICRONS,NULL,0,NULL);
    
    /* Useless : only here to be constantly evaluated */
/*     add_item_to_menu(mn,"Shortcut",short__KEY_PLUS_PAD,NULL,0,NULL);  */
/*     add_item_to_menu(mn,"Shortcut",short__KEY_MINUS_PAD,NULL,0,NULL);  */
/*     add_item_to_menu(mn,"Shortcut",short__KEY_P,NULL,0,NULL);  */
/*     add_item_to_menu(mn,"Shortcut",short__KEY_L,NULL,0,NULL);  */
/*     add_item_to_menu(mn,"Shortcut",short__KEY_1_PAD,NULL,0,NULL);  */
/*     add_item_to_menu(mn,"Shortcut",short__KEY_2_PAD,NULL,0,NULL);  */
/*     add_item_to_menu(mn,"Shortcut",short__KEY_3_PAD,NULL,0,NULL);  */
/*     add_item_to_menu(mn,"Shortcut",short__KEY_G,NULL,0,NULL);  */
/*     add_item_to_menu(mn,"Shortcut",short__KEY_O,NULL,0,NULL);  */
/*     add_item_to_menu(mn,"Shortcut",short__KEY_F,NULL,0,NULL);  */
/*     add_item_to_menu(mn,"Shortcut",short__KEY_C,NULL,0,NULL);  */
    
	
	return mn;
}

/** Main menu for plug_in */     
int menu_plug_in_main(void)
{   
    if(updating_menu_state != 0) return D_O_K;
    
    //win_report("Menu capabilities loaded");
    
    add_plot_treat_menu_item("AOTF control",NULL,commander_base_menu(),0,NULL);
    add_image_treat_menu_item("AOTF control",NULL,commander_base_menu(),0,NULL);
	
	return D_O_K;    
}


#endif

