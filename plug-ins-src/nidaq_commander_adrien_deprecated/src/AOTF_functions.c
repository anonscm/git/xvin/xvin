/** \file AOTF_functions.c
    \brief Sub-plug-in program for AOTF control with NIDAQ-M 6229 in Xvin.
    
    Controls function generator for different AOTF control situations.
    BEWARE : requests the NIDAQ plugin !
    
    \sa NIDAQ.c
    \author Adrien Meglio
    \version 01/11/2006
*/
#ifndef _AOTF_functions_C_
#define _AOTF_functions_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../../nidaqmxwrap_thread/include/nidaqmxwrap_thread.h"
#include "nidaq_commander_adrien.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Shortcut functions

/** Defines a shortcut to switch the current line.

\author Adrien Meglio
\version 26/01/07
*/
int AOTF__switch_LINE(int line)
{
    if(updating_menu_state != 0) return D_O_K;
    
//    if (lio == NULL) 
//    {
//        lines__initialize_lio();
//        return win_report("lines structures did not exist ! They were recreated but\n"
//        "the current function 'AOTF__sequence__switch_line' was interrupted");
//    }
    
    if (local_lio.lines[line].status == LINE_ON)
    {   
        local_lio.lines[line].status = LINE_OFF;
    } else
    {
        local_lio.lines[line].status = LINE_ON;
    }
    
    /* Recreates the complete sequence (all lines merged) to be sent to the card */  
    IO__refresh_lio(local_lio);

    /* Prints into the log file */
    switch (local_lio.lines[line].status)
    {
        case LINE_OFF :
        win_event("Switched line LINE_%d off",line);
        break;
        case LINE_ON :
        win_event("Switched line LINE_%d on",line);
        break;
    }
    
    return D_O_K;
} 

/** Sets the line intensity to a given value (between 0 and 1).

\author Adrien Meglio
\version 29/01/07
*/
int AOTF__set_LINE_intensity(int line, float level)
{
    if(updating_menu_state != 0) return D_O_K;
    
    //win_report("Pre-level %.2f",level);
    
    level = (level < 0) ? 0 : level;
    level = (level > 1) ? 1 : level;
    
    local_lio.lines[line].amplitude = (float) level*AOTF_MAX_VOLT;
    win_event("Changed line LINE %d level to %d/100",line,100*level);

    /* Recreates the complete sequence (all lines merged) to be sent to the card */  
    IO__refresh_lio(local_lio);
    
    return D_O_K;
} 

/** Defines a shortcut to get the current line intensity.

\author Adrien Meglio
\version 26/01/07
*/
int AOTF__get_LINE_intensity(int line)
{
    int level = 0;
    
    if(updating_menu_state != 0) return D_O_K;
    
    win_scanf("Set line LINE to what percentage of maximal intensity ? %d",&level);
    
    AOTF__set_LINE_intensity(line,(float) level/100.0);
    
    return D_O_K;
} 

int initialize_AOTF(void)
{
    int j;

    local_lio.frequency = 50;
    local_lio.num_cycles = -1;
    local_lio.num_points = 10;
    
//    win_report("%.2f %d %d",local_lio.frequency,local_lio.num_cycles,local_lio.num_points);
    
    for (j=0 ; j<NUMBER_OF_AO_LINES ; j++)
    {
        local_lio.lines[j].status = LINE_OFF;
        
        local_lio.lines[j].function = waveform__const_function;
        local_lio.lines[j].offset = 0;
        local_lio.lines[j].amplitude = AOTF_MAX_VOLT;
        
//        win_report("%d %.2f %.2f",local_lio.lines[j].status,local_lio.lines[j].offset,local_lio.lines[j].amplitude);
    }
    
    /* Recreates the complete sequence (all lines merged) to be sent to the card */  
    IO__refresh_lio(local_lio);
    
    return D_O_K;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Base functions

int	AOTF_functions_main(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    initialize_AOTF();
    
    return D_O_K;
}

#endif

