 /** \file IO.c
    \brief Plug-in program for exporting data to other plugins.
     
    \author Adrien Meglio
    \version 22/01/07
*/

#ifndef _IO_C_
#define _IO_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../../nidaqmxwrap_thread/include/nidaqmxwrap_thread.h"
#include "nidaq_commander_adrien.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

/* //////////////////////////////////////////////////////////////////////////////////////////////////////// */
/* //////////////////////////////////////////////////////////////////////////////////////////////////////// */
/* // Export functions for PIFOC */

/* /** Returns the PIFOC position in the range 0-1. */

/* \author Adrien Meglio */
/* \version 22/01/07 */
/* */ 
/* void IO__give_PIFOC_percent(float *position) */
/* { */
/*     if(updating_menu_state != 0) return; */
    
/*     *position = PIFOC_POSITION; */
    
/*     return;  */
/* } */

/* /** Returns the PIFOC position in microns (assuming PIFOC_COURSE_MICRONS is correct). */

/* \author Adrien Meglio */
/* \version 22/01/07 */
/* */ 
/* void IO__give_PIFOC_microns(float *position) */
/* { */
/*     if(updating_menu_state != 0) return; */
    
/*     *position = (1-PIFOC_POSITION)*PIFOC_COURSE_MICRONS; */
    
/*     return; */
/* } */

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
// Export functions for AOTF

/** Returns the level of a requested line.

\author Adrien Meglio
\version 22/01/07
*/
void IO__give_line_level(int line, float *level)
{
    if(updating_menu_state != 0) return;
    
    *level = local_lio.lines[line].amplitude/AOTF_MAX_VOLT;
    
    return; 
}

/** Returns the status of a requested line.

\author Adrien Meglio
\version 26/01/07
*/
void IO__give_line_status(int line, int *status)
{
    if(updating_menu_state != 0) return;
    
    *status = local_lio.lines[line].status;
    
    return; 
}



//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Base functions

int	IO_main(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    return D_O_K;
}

#endif
