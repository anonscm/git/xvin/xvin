/** \file AOTF_functions.c
    \brief Sub-plug-in program for AOTF control with NIDAQ-M 6229 in Xvin.
    
    Controls function generator for different AOTF control situations.
    BEWARE : requests the NIDAQ plugin !
    
    \sa NIDAQ.c
    \author Adrien Meglio
    \version 01/11/2006
*/
#ifndef _AOTF_functions_C_
#define _AOTF_functions_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../../nidaqmxwrap_thread/include/nidaqmxwrap_thread.h"
#include "nidaq_commander_adrien.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Base functions

int nidaq_commander_adrien_main(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    /* Load the thread capabilities (do it first !) */
    nidaqmxwrap_thread_main();
    IO__change_thread_priority(THREAD_PRIORITY_LOWEST); /* Set the NI card data refreshing thread priority to medium */
    
    /* Initialization */  
    lines__initialize_local_lio(&local_lio); 
//    win_event("local_lio initialized");

    menu_plug_in_main();    
//    win_event("menu loaded");
    
/*    PIFOC_functions_main();
    win_event("PIFOC_functions loaded"); */
    
    AOTF_functions_main();
//    win_event("AOTF_functions loaded");
    
/*    shortcuts_main();
    win_event("shortcuts loaded"); */
    
    IO_main();
//    win_event("IO loaded");
    
    /* Start the nidaq card operation */
    //task__continuous_buffer_regeneration();
    
    return D_O_K;
}

#endif

