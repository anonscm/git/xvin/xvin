/** \file PIFOC_functions.c
    \brief Sub-plug-in program for PIFOC control with NIDAQ-M 6229 in Xvin.
    
    Controls function generator for different PIFOC control situations.
    BEWARE : requests the NIDAQ plugin !
    
    \sa NIDAQ.c
    \author Adrien Meglio
    \version 15/11/06
*/
#ifndef _PIFOC_functions_C_
#define _PIFOC_functions_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>
#include <windows.h>
#include <stdio.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../../nidaqmxwrap_thread/include/nidaqmxwrap_thread.h"
#include "nidaq_commander_adrien.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

/** Directly sets the PIFOC to a given position.

\author Adrien Meglio
\version 16/01/07
*/
int PIFOC__set_MICRONS(void)
{
    float PIFOC_temp;    
        
    if(updating_menu_state != 0) return D_O_K;
    
    PIFOC_temp = 0;
    win_scanf("Position du PIFOC : %f �m",&PIFOC_temp);
       
    PIFOC__move(1-PIFOC_temp/PIFOC_COURSE_MICRONS - PIFOC_POSITION);
    /* The PIFOC is mounted upside down (0% is PIFOC_COURSE_MICRONS and 100% is 0) */
    
    return D_O_K;
}

/** Moves the PIFOC of a given quantity.

\param float pifoc_move The displacement in percent (between 0 and 1) of the max course.

\author Adrien Meglio
\version 15/11/06
*/
int PIFOC__move(float pifoc_move)
{
    int i; 
        
    if(updating_menu_state != 0) return D_O_K;
    
    /* Checks that the value is not off-limits */
    PIFOC_POSITION += pifoc_move;
    PIFOC_POSITION = (PIFOC_POSITION*PIFOC_TOTAL_COURSE > PIFOC_MAX_VOLT) ? PIFOC_MAX_VOLT/PIFOC_TOTAL_COURSE : PIFOC_POSITION;
    PIFOC_POSITION = (PIFOC_POSITION*PIFOC_TOTAL_COURSE < PIFOC_MIN_VOLT) ? PIFOC_MIN_VOLT/PIFOC_TOTAL_COURSE : PIFOC_POSITION;
    
    local_lio.lines[PIFOC_LINE].amplitude = (float64) PIFOC_POSITION*PIFOC_TOTAL_COURSE; 

    /* Recreates the complete sequence (all lines merged) to be sent to the card */  
    IO__refresh_lio(local_lio);
    
    /* Prints into the log file */
    //win_event("Changed PIFOC position to %f �m (really %f/100)\n",(1-PIFOC_POSITION)*PIFOC_COURSE_MICRONS,100*PIFOC_POSITION);
    
	return D_O_K;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Bead search functions

/** A PIFOC reference setting mode.

Allows the user to dynamically select 2 PIFOC references by ctrl+F1/ctrl+F2  
then go back to them just by F1/F2. Program exit is provided by the escape key.

\author Adrien Meglio
\version 2/02/07
*/
DWORD WINAPI thread__PIFOC_reference(LPVOID lpParam) 
{
    int pressed_key;
    int exit_status = 0;
    float pifoc_position_percent_1 = 0;
    float pifoc_position_percent_2 = 1;
    
    while (exit_status == 0)
    {
        if (key[KEY_A]) /* Q is KEY_A on qwerty keyboards */
        {
            exit_status = 1;
            //win_report("Exiting reference mode");
        }
        
        if (key[KEY_F1]) 
        {
             if (key_shifts & KB_CTRL_FLAG) 
             {
	       IO__whatis__motor_p_consigne_in_points(&pifoc_position_percent_1);
                //win_report("Set reference 1 to : %.0f",pifoc_position_percent_1);
             } 
             else 
             {
                PIFOC__move(pifoc_position_percent_1 - PIFOC_POSITION);
                //win_report("Moved to reference 1");
             }
        }
        
        if (key[KEY_F2]) 
        {
             if (key_shifts & KB_CTRL_FLAG) 
             {
	       IO__whatis__motor_p_consigne_in_points(&pifoc_position_percent_2);
                //win_report("Set reference 2 to : %.0f",pifoc_position_percent_2);
             } 
             else 
             {
                PIFOC__move(pifoc_position_percent_2 - PIFOC_POSITION);
                //win_report("Moved to reference 2");
             }
        }
   }
    
    return D_O_K;
}

/** PIFOC reference control thread launcher.

Initializes the PIFOC reference control thread.
\param int None.
\return int Error code.
\author Adrien Meglio
\version 2/02/07
*/ 
int launcher__PIFOC_reference(void)
{
    HANDLE hThread = NULL;
    DWORD dwThreadId;

    hThread = CreateThread( 
            NULL,              // default security attributes
            0,                 // use default stack size  
            thread__PIFOC_reference,// thread function 
            NULL,        // argument to thread function 
            0,                 // use default creation flags 
            &dwThreadId);      // returns the thread identifier 

    if (hThread == NULL) 	win_report("No thread created");
    SetThreadPriority(hThread,  THREAD_PRIORITY_LOWEST);

    return D_O_K;
}


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Shortcut functions

/** Defines a shortcut to move the focal plane upwards.

\author Adrien Meglio
\version 26/01/07
*/
int PIFOC__up(void)
{
    if(updating_menu_state != 0) return D_O_K;
     
    PIFOC__move(-PIFOC_FINESSE_MICRONS/PIFOC_COURSE_MICRONS);
    /* The PIFOC is mounted upside down (0% is PIFOC_COURSE_MICRONS and 100% is 0) */

//    win_report("Move up");
    
    return D_O_K;
}

/** Defines a shortcut to move the focal plane downwards.

\author Adrien Meglio
\version 26/01/07
*/
int PIFOC__down(void)
{
    if(updating_menu_state != 0) return D_O_K;
      
    PIFOC__move(PIFOC_FINESSE_MICRONS/PIFOC_COURSE_MICRONS);
    /* The PIFOC is mounted upside down (0% is PIFOC_COURSE_MICRONS and 100% is 0) */

//    win_report("Move up");
    
    return D_O_K;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// 
/* Implement :
 - test__check_overflow : reads the TTL Overflow signal from pin 26a and corrects the problem if it occurs
 - test__get_PZT : reads the piezo position from pin 8a (range : 0-1 V i.e. 1/10th of PIFOC_POSITION), checks that 
 it is consistent with PIFOC_POSITION, and displays it on the screen
*/

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Base functions

/** Main function of NIDAQ PIFOC capabilities

\author Adrien Meglio
\version 15/11/06
*/
int	PIFOC_functions_main(void)
{
    if(updating_menu_state != 0) return D_O_K;
    
    PIFOC_FINESSE_MICRONS = 1; /* Default finesse : 1�m */
    local_lio.lines[PIFOC_LINE].status = LINE_ON;
    
    launcher__PIFOC_reference(); /* Enters PIFOC reference mode */
    
    return D_O_K;
}

#endif



