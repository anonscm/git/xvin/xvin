#ifndef _LEQUALZ_H_
#define _LEQUALZ_H_

PXV_FUNC(int, do_lequalz_rescale_plot, (void));
PXV_FUNC(MENU*, lequalz_plot_menu, (void));
PXV_FUNC(int, do_lequalz_rescale_data_set, (void));
PXV_FUNC(int, lequalz_main, (int argc, char **argv));
#endif

