/*
*        Plug-in program for plot treatement in Xvin.
 *
 *        V. Croquette
    */
#ifndef _LEQUALZ_C_
#define _LEQUALZ_C_

# include "ctype.h"
# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 

 # include "xvplot.h"
/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "lequalz.h"


pltreg     *prepare_analysis_grabing_pltreg(char *name, char *sample, float bd_x0, float bd_y0, 
					    float bd_z0, float du_x0, float du_y0, float du_z0);


# ifndef EXE    
int    update_treatement_file(d_s *ds, char *file)
{
    int l = 0;
    char *cs;
    
    if (ds == NULL || file == NULL)    return 1;
    if (ds->treatement != NULL)     l = strlen(ds->treatement);
    l += strlen(file);
    cs = (char*)calloc(l+1, sizeof(char));
    if (cs == NULL)    return 1;
    if (ds->treatement != NULL) sprintf(cs,"%s%s",ds->treatement,file);
    else    sprintf(cs,"%s",file);
    if (ds->treatement != NULL) free(ds->treatement);
    ds->treatement = cs;
    return 0;
}
# endif

d_s *add_data_point_to_source_specific_ds_in_pr(pltreg *pr, char *src, float x, float y, char *label)
{
    d_s *ds = NULL;
    
    ds = find_source_specific_ds_in_pr(pr,src);    
    if (ds == NULL)        return NULL;
    ds->yd[(ds->ny < ds->my) ? ds->ny++ : ds->my - 1] = y;
    ds->xd[(ds->nx < ds->mx) ? ds->nx++ : ds->mx - 1] = x;
    if (ds->nx != ds->ny)   win_printf("different nb. of points in x %d and y %d",ds->nx,ds->ny);
    update_treatement_file(ds,label);            
    return ds;
}

int    lequalz_file(char *l_file)
{
    register int i, j, k;
    int    nx, file_index;
    char path[128], file[128], f_line[256], s1[128], s2[1024], s3[128], basename[16];
    float rot,zmag,xm,kxd,kxs,xdu,xfc,xfhz;
    float rx,ym,kyd,kys,ydu,yfc,yfhz,ry,zmin;
    float kzd,kzs,zp0,zfc,zfhz,rz,l,fox,foy,anisotropy,angle;
    FILE *fp, *fpi;
    //    acreg *ac, *act = cur_ac_reg;
    float bd_x0 = 0,bd_y0 = 0,bd_z0 = 0;
    float du_x0 = 0,du_y0 = 0,du_z0 = 0;
    float a = .878;
    pltreg *pri;
    float mds_y[64];
    multi_d_s *mds = NULL;
    //Bto *bt;
    



    fp = fopen(l_file,"r");
    if (fp == NULL)
    {
        printf("cannot open %s !\n",l_file);
        return 1;
    }
    if (extract_file_path(f_line, 256, l_file) == NULL)
    {
        printf("cannot extract path %s !\n",l_file);
        return 1;
    }
    sprintf(file,"%s\\force.gr",f_line);
    fpi = fopen(file,"r");
    if (fpi == NULL)
    {
        printf("cannot open %s !\n",file);
        return 1;
    }
    for (j = 0; fgets(f_line,256,fpi) != NULL && j == 0;    )
    {
        if ((sscanf(f_line,"%s%s%s",s1,s2,s3) == 3)    
            && (strcmp(s1,"-src") == 0) && (strcmp(s3,"force") == 0)) 
        {    
            i = fscanf(fpi," xbr = %f ybr = %f zbr = %f\n xpr = %f ypr = %f zpr = %f",
                     &bd_x0,&bd_y0,&bd_z0,&du_x0,&du_y0,&du_z0);
            if (i != 6)
            {
                win_scanf("bead x0 %f y0 %f z0 %f dust x0 %f y0 %f z0 %f",
                        &bd_x0,&bd_y0,&bd_z0,&du_x0,&du_y0,&du_z0);
	    }
        }
        if ((sscanf(f_line,"%s%s",s1,s2) == 2) && (strcmp(s1,"-treat") == 0)) 
        {
	  /*
	  k = sscanf(s2,"%[a-zA-Z]",basename);
	  win_printf("k %d base %s ext %s",k,s2,basename);
	  */
	  for (k = 0; k < 256 && s2[k] != '.' && s2[k] != 0 ; k++);
	  if (s2[k] != '.') win_printf("pb finding basename !");

	  k = (k > 0) ? k-1 : k;
	  for ( ; k >= 0 && isdigit(s2[k]); k--);
	  basename[k] = 0;			       
	  for ( ; k > 0; k--)
                basename[k-1] = s2[k];
	  
	  /*
	    win_printf("base %s ext %s",s2,basename);
            for (k = 0; k < 5; k++)
                basename[k] = s2[k+1];

            basename[5] = 0;
	  */
            //win_printf("basename %s s2 %s\n",basename,s2);
            j = 1; 
        }        
    }
    fclose(fpi);
    if (extract_file_path(f_line, 256, l_file) == NULL)
        return win_printf_OK("cannot extract path %s !\n",l_file);

    pri = prepare_analysis_grabing_pltreg(backslash_to_slash(f_line), "junk", 
                     bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0);
    if (pri == NULL)    return win_printf_OK("cannot create project");

    pri->path = strdup(f_line);	

    if (extract_file_name(f_line, 256, l_file) == NULL)
        return win_printf_OK("cannot extract path %s !\n",l_file);
    pri->filename = strdup(f_line);  



    do_one_plot(pri);
    if ((mds = create_multi_d_s(34)) == NULL) 
        return win_printf("pas de mem");
    mds->source = strdup(backslash_to_slash(l_file));
    sprintf(mds->name[0],"# index");
    sprintf(mds->name[1],"%s",basename);
    sprintf(mds->name[2],"Mag rot");
    sprintf(mds->name[3],"Mag Z");
    sprintf(mds->name[4],"nx");
    sprintf(mds->name[5],"Bead <x>");
    sprintf(mds->name[6],"k_{xd}");
    sprintf(mds->name[7],"k_{xs}");
    sprintf(mds->name[8],"Dust <x>");
    sprintf(mds->name[9],"fc_x (mode)");
    sprintf(mds->name[10],"fc_x (Hz)");
    sprintf(mds->name[11],"\\sigma^4/\\delta (x)");
    sprintf(mds->name[12],"Bead <y>");
    sprintf(mds->name[13],"k_{yd}");
    sprintf(mds->name[14],"k_{ys}");
    sprintf(mds->name[15],"Dust <y>");
    sprintf(mds->name[16],"fc_y (mode)");
    sprintf(mds->name[17],"fc_y (Hz)");
    sprintf(mds->name[18],"\\sigma^4/\\delta (y)");
    sprintf(mds->name[19],"Bead <z>");
    sprintf(mds->name[20],"k_{zd}");
    sprintf(mds->name[21],"k_{zs}");
    sprintf(mds->name[22],"Dust <z>");
    sprintf(mds->name[23],"fc_z (mode)");
    sprintf(mds->name[24],"fc_z (Hz)");
    sprintf(mds->name[25],"\\sigma^4/\\delta (z)");    
    sprintf(mds->name[26],"l=\\sqrt{x^2+y^2+z^2}");
    sprintf(mds->name[27],"f_{ox}");
    sprintf(mds->name[28],"f_{oy}");
    sprintf(mds->name[29],"anisotropy");
    sprintf(mds->name[30],"angle");
    sprintf(mds->name[31],"l = z");
    sprintf(mds->name[32],"f_{1x}");
    sprintf(mds->name[33],"f_{1y}");    
    
    for (j = 0; fgets(f_line,256,fp) != NULL;    )
    {
        if ((sscanf(f_line,"%s%s%s",s1,s2,s3) == 3)    
            && (strcmp(s1,"{\\bf") == 0) && (strcmp(s2,"Analyse") == 0) 
            && (strcmp(s3,"spectrale") == 0))
        {
	    i = fscanf(fp," \\noindent\\begin{tabular}{|l|c|c|c|c|c|c|c|c|}\n\\hline\n "
		       "file & axe & $<mean>$ & $k$ & $k_s$ & r\\'ef\\'erence & $f_c$  "
		       "& $f_c$ & nx/r  \\\\\n rot %g Zm %g& & $\\mu m$ & $\\times "
		       "10^{-8} N/m$ & $\\times 10^{-8} N/m$ & $\\mu m$ & \\# & (Hz) "
		       "& %d \\\\\n\\hline\n\\small %s & x(t) & %g & %g & %g & %g & %g "
		       "& %g & %g  \\\\\n\\hline\n %s  & y(t) & %g & %g & %g & %g & %g "
		       "& %g & %g  \\\\\n \\hline\n   & z(t) & %g & %g & %g & %g & %g & "
		       "%g & %g  \\\\\n \\hline\n   & L & %g & $f_0$ %g & $f_1$ %g & "
		       "\\multicolumn{2}{|c|}{ani. %g \\%%} & \\multicolumn{2}{|c|}{"
		       "angle %g ${}^\\circ$} \\\\\n  \\hline\n\\end{tabular} \n",
		       &rot,&zmag,&nx,path,&xm,&kxd,&kxs,&xdu,&xfc,&xfhz,&rx,file,&ym,
		       &kyd,&kys,&ydu,&yfc,&yfhz,&ry,&zmin,&kzd,&kzs,&zp0,&zfc,&zfhz,
		       &rz,&l,&fox,&foy,&anisotropy,&angle);
	    //win_printf("%d analysing %s %s\n",j,(path != NULL)?path :" ",(file!= NULL)?file:"unknown!");

	    if (i != 31)   win_printf("Pb nb of item read %d is not what was expected",i);
	    for (i = 0; file[i] && file[i] != '.'; i++);
	    file_index = j;
	    sscanf(file + (((i - 3) >= 0) ? i - 3 : 0),"%d",&file_index);

	    mds_y[0] = j;
	    mds_y[1] = file_index;


	    mds_y[2] = rot;
	    mds_y[3] = zmag;
	    mds_y[4] = nx;
	    mds_y[5] = xm;
	    mds_y[6] = kxd;
	    mds_y[7] = kxs;
	    mds_y[8] = xdu;
	    mds_y[9] = xfc;
	    mds_y[10] = xfhz;
	    mds_y[11] = rx;
	    mds_y[12] = ym;
	    mds_y[13] = kyd;
	    mds_y[14] = kys;
	    mds_y[15] = ydu;
	    mds_y[16] = yfc;
	    mds_y[17] = yfhz;
	    mds_y[18] = ry;
	    mds_y[19] = zmin;
	    mds_y[20] = kzd;
	    mds_y[21] = kzs;
	    mds_y[22] = zp0;
	    mds_y[23] = zfc;
	    mds_y[24] = zfhz;
	    mds_y[25] = rz;    
	    mds_y[26] = l;
	    mds_y[27] = fox;
	    mds_y[28] = foy;
	    mds_y[29] = anisotropy;
	    mds_y[30] = angle;

	    l = (zmin-zp0+du_z0)*a;
	    kxs *= 1e-8;
	    kys *= 1e-8;
	    fox = l * kxs * 1e6;
	    foy = l * kys * 1e6;        
	    
	    mds_y[31] = l;
	    mds_y[32] = fox;
	    mds_y[33] = foy;    

    
	    add_data_to_multi_d_s(mds, mds_y);


        
	    //win_printf("l = %g fox = %g foy = %g",l,fox,foy);
	    add_data_point_to_source_specific_ds_in_pr(pri, 
		"DNA force extension curve f_x", l, fox, file);
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "DNA force extension curve f_y", l, foy, file);
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "DNA force extension curve f_m", l, (foy + fox)/2, file);
	    add_data_point_to_source_specific_ds_in_pr(pri,         
            "DNA stiffness versus extension curve",l,kzs,file);
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "DNA extension versus Z magnet", l,zmag, file);
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "Force versus Z magnet curve f_x", zmag, fox, file);
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "Force versus Z magnet curve f_y", zmag, foy, file);                            
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "Force versus Z magnet curve f_m", zmag, (foy + fox)/2, file);            
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "DNA extension versus sigma curve", rot, l, file);            
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "DNA bead position", xm - xdu + du_x0, ym - ydu + du_y0, file);    
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "DNA reference position", xdu - du_x0, ydu - du_y0, file);        
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "DNA anisotropy curve", file_index, anisotropy, file);            
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "DNA angle anisotropy curve", file_index, angle, file);    
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "DNA viscosity radius 0", l, 1e9*kxs/(12*M_PI*M_PI*xfhz), file);
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "DNA viscosity radius 1", l, 1e9*kys/(12*M_PI*M_PI*yfhz), file);
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "DNA x position versus z", xm, (zmin-zp0+du_z0)*a, file);
	    add_data_point_to_source_specific_ds_in_pr(pri, 
            "DNA y position versus z", ym, (zmin-zp0+du_z0)*a, file);    
            j++;
        }
    }
    fclose(fp);
    pri->use.to = IS_MULTI_D_S;
    pri->use.stuff = (void *)mds;
    
    
    //    bt = (Bto *) find_stuff(ac, IS_KNOB1, KILL_B);
    //if (!bt) bt->db.action = on_yes_kill_multi_d_s_acreg;
    do_one_plot(pri);
    //act = xvplot_plot_menu(mds);
    //add_sub_menu(act,"claudefu",claudefu_menu());
    //add_sub_menu(act,"corel",cardinal_menu());
    //add_menu_item(act, "Rm point", kill_point, ONE_BOUNCE, 0);
    //add_menu_item(act, "new file", load_lequalz_file, ONE_BOUNCE, 0);
    //add_menu_item(act, "append file", load_lequalz_file, ONE_BOUNCE, 1);
        
    //for (i=0 ; i< ac->n_list ; i++)
    //{
    //    if(ac->list[i].to == IS_MENU)
    //        ac->list[i].stuff =(void *)act;
    //}    
    //    activate_acreg(ac);
    refresh_plot(pri, UNCHANGED);    
    return 0;
}

int    lequalz_append_file(pltreg *plt_mds, char *l_file)
{
    register int i, j;
    int    nx, file_index, n_added = 0;
    char path[128], file[128], f_line[256], s1[128], s2[128], s3[128];
    float rot,zmag,xm,kxd,kxs,xdu,xfc,xfhz;
    float rx,ym,kyd,kys,ydu,yfc,yfhz,ry,zmin;
    float kzd,kzs,zp0,zfc,zfhz,rz,l,fox,foy,anisotropy,angle;
    FILE *fp, *fpi;
    float bd_x0 = 0,bd_y0 = 0,bd_z0 = 0;
    float du_x0 = 0,du_y0 = 0,du_z0 = 0;
    float a = .878;
    float mds_y[64];
    multi_d_s *mds = NULL;
    

    mds = find_multi_d_s_in_pltreg(plt_mds); 
    if (mds == NULL || plt_mds == NULL)    return win_printf("cannot find data");


    fp = fopen(l_file,"r");
    if (fp == NULL)
    {
        printf("cannot open %s !\n",l_file);
        return 1;
    }
    if (extract_file_path(f_line, 256, l_file) == NULL)
    {
        printf("cannot extract path %s !\n",l_file);
        return 1;
    }
    sprintf(file,"%s\\force.gr",f_line);
    fpi = fopen(file,"r");
    if (fpi == NULL)
    {
        printf("cannot open %s !\n",file);
        return 1;
    }
    for (j = 0; fgets(f_line,256,fpi) != NULL && j == 0;    )
    {
        if ((sscanf(f_line,"%s%s%s",s1,s2,s3) == 3)    
            && (strcmp(s1,"-src") == 0) && (strcmp(s3,"force") == 0)) 
        {    
            i = fscanf(fpi," xbr = %f ybr = %f zbr = %f\n xpr = %f ypr = %f zpr = %f",
                     &bd_x0,&bd_y0,&bd_z0,&du_x0,&du_y0,&du_z0);
            if (i != 6)
            {
                win_scanf("bead x0 %f y0 %f z0 %f dust x0 %f y0 %f z0 %f",
                        &bd_x0,&bd_y0,&bd_z0,&du_x0,&du_y0,&du_z0);
            }
            j = 1;
        }
    }
    fclose(fpi);

    for (j = mds->n; fgets(f_line,256,fp) != NULL;    )
    {
        if ((sscanf(f_line,"%s%s%s",s1,s2,s3) == 3)    
            && (strcmp(s1,"{\\bf") == 0) && (strcmp(s2,"Analyse") == 0) 
            && (strcmp(s3,"spectrale") == 0))
        {
	    i = fscanf(fp," \\noindent\\begin{tabular}{|l|c|c|c|c|c|c|c|c|}\n\\hline\n "
		       "file & axe & $<mean>$ & $k$ & $k_s$ & r\\'ef\\'erence & $f_c$  "
		       "& $f_c$ & nx/r  \\\\\n rot %g Zm %g& & $\\mu m$ & $\\times "
		       "10^{-8} N/m$ & $\\times 10^{-8} N/m$ & $\\mu m$ & \\# & (Hz) "
		       "& %d \\\\\n\\hline\n\\small %s & x(t) & %g & %g & %g & %g & %g "
		       "& %g & %g  \\\\\n\\hline\n %s  & y(t) & %g & %g & %g & %g & %g "
		       "& %g & %g  \\\\\n \\hline\n   & z(t) & %g & %g & %g & %g & %g & "
		       "%g & %g  \\\\\n \\hline\n   & L & %g & $f_0$ %g & $f_1$ %g & "
		       "\\multicolumn{2}{|c|}{ani. %g \\%%} & \\multicolumn{2}{|c|}{"
		       "angle %g ${}^\\circ$} \\\\\n  \\hline\n\\end{tabular} \n",
		       &rot,&zmag,&nx,path,&xm,&kxd,&kxs,&xdu,&xfc,&xfhz,&rx,file,&ym,
		       &kyd,&kys,&ydu,&yfc,&yfhz,&ry,&zmin,&kzd,&kzs,&zp0,&zfc,&zfhz,
		       &rz,&l,&fox,&foy,&anisotropy,&angle);
	    /*
            i = fscanf(fp," \\noindent\\begin{tabular}{|l|c|c|c|c|c|c|c|c|}\n\\hline\n"
                     " file & axe & $<mean>$ & $k$ & $k_s$ & r\\'ef\\'erence & $f_c$    "
                     "& $f_c$ & nx/r    \\\\\n rot %g Zm %g& & $\\mu m$ & $\\times 10^{-8} N/m$ "
                     "& $\\times 10^{-8} N/m$ & $\\mu m$ & \\# & (Hz) & %d \\\\\n\\hline\n"
                     "\\small %s & x(t) & %g & %g & %g & %g & %g & %g & %g    \\\\\n\\hline\n"
                     " %s    & y(t) & %g & %g & %g & %g & %g & %g & %g    \\\\\n \\hline\n"
                     "     & z(t) & %g & %g & %g & %g & %g & %g & %g    \\\\\n \\hline\n"
                     "     & L & %g & $f_0$ %g & $f_1$ %g & \\multicolumn{2}{|c|}{ani. %g \\%%} &"
                     " \\multicolumn{2}{|c|}{angle %g ${}^\\circ$} \\\\\n    \\hline\n"
                     "\\end{tabular} \n",&rot,&zmag,&nx,path,&xm,&kxd,&kxs,&xdu,&xfc,&xfhz,
                     &rx,file,&ym,&kyd,&kys,&ydu,&yfc,&yfhz,&ry,&zmin,&kzd,&kzs,&zp0,&zfc,
                     &zfhz,&rz,&l,&fox,&foy,&anisotropy,&angle);
	    */
    /*
            printf("%d analysing %s %s\n",j,(path != NULL)?path :" ",(file!= NULL)?file:"unknown!");*/

            for (i = 0; file[i] && file[i] != '.'; i++);
            file_index = j;
            sscanf(file + (((i - 3) >= 0) ? i - 3 : 0),"%d",&file_index);

            mds_y[0] = j;
            mds_y[1] = file_index;


            mds_y[2] = rot;
            mds_y[3] = zmag;
            mds_y[4] = nx;
            mds_y[5] = xm;
            mds_y[6] = kxd;
            mds_y[7] = kxs;
            mds_y[8] = xdu;
            mds_y[9] = xfc;
            mds_y[10] = xfhz;
            mds_y[11] = rx;
            mds_y[12] = ym;
            mds_y[13] = kyd;
            mds_y[14] = kys;
            mds_y[15] = ydu;
            mds_y[16] = yfc;
            mds_y[17] = yfhz;
            mds_y[18] = ry;
            mds_y[19] = zmin;
            mds_y[20] = kzd;
            mds_y[21] = kzs;
            mds_y[22] = zp0;
            mds_y[23] = zfc;
            mds_y[24] = zfhz;
            mds_y[25] = rz;    
            mds_y[26] = l;
            mds_y[27] = fox;
            mds_y[28] = foy;
            mds_y[29] = anisotropy;
            mds_y[30] = angle;
            
            l = (zmin-zp0+du_z0)*a;
            kxs *= 1e-8;
            kys *= 1e-8;
            fox = l * kxs * 1e6;
            foy = l * kys * 1e6;        
            
            mds_y[31] = l;
            mds_y[32] = fox;
            mds_y[33] = foy;    


	    for (i = 0; i < mds->n; ++i)
	      if(mds->x[1][i] == file_index) break;

	    if (i != mds->n) break; // the point is already there

	    n_added++;
            add_data_to_multi_d_s(mds, mds_y);

        
/*        win_printf("l = %g fox = %g foy = %g",l,fox,foy);*/
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "DNA force extension curve f_x", l, fox, file);
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "DNA force extension curve f_y", l, foy, file);
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "DNA force extension curve f_m", l, (foy + fox)/2, file);
            add_data_point_to_source_specific_ds_in_pr(plt_mds,         
            "DNA stiffness versus extension curve",l,kzs,file);
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "DNA extension versus Z magnet", l,zmag, file);
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "Force versus Z magnet curve f_x", zmag, fox, file);
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "Force versus Z magnet curve f_y", zmag, foy, file);                            
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "Force versus Z magnet curve f_m", zmag, (foy + fox)/2, file);
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "DNA extension versus sigma curve", rot, l, file);            
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "DNA bead position", xm - xdu + du_x0, ym - ydu + du_y0, file);    
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "DNA reference position", xdu - du_x0, ydu - du_y0, file);        
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "DNA anisotropy curve", file_index, anisotropy, file);            
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "DNA angle anisotropy curve", file_index, angle, file);    
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "DNA viscosity radius 0", l, 1e9*kxs/(12*M_PI*M_PI*xfhz), file);
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "DNA viscosity radius 1", l, 1e9*kys/(12*M_PI*M_PI*yfhz), file);
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "DNA x position versus z", xm, (zmin-zp0+du_z0)*a, file);
            add_data_point_to_source_specific_ds_in_pr(plt_mds, 
            "DNA y position versus z", ym, (zmin-zp0+du_z0)*a, file);    
            j++;
        }
    }
    fclose(fp);
    //    activate_acreg(ac_mds);
    refresh_plot(plt_mds, UNCHANGED);
    win_printf("Nb of point added %d",n_added);
    return 0;
}

pltreg     *prepare_analysis_grabing_pltreg(char *name, char *sample, float bd_x0, float bd_y0, 
                     float bd_z0, float du_x0, float du_y0, float du_z0)
{
    register int i;
    static int nx = 1024;
    pltreg *pr;
    O_p *op;
    d_s *ds;
    char cur_path[DIR_LEN];
    char *my_getcwd(char *path, int size);

    


    my_getcwd(cur_path ,DIR_LEN);                
    slash_to_backslash(cur_path);
    for (i = 0; i < DIR_LEN && cur_path[i] != 0; i++);
    if (i < DIR_LEN)
    {
        i = (i>0) ? i-1 : 0;
        if (cur_path[i] == '/')        cur_path[i] = 0;
    }
    pr = create_and_register_new_plot_project(0,     32,    900,    668);
    if (pr == NULL)        
    {
        win_printf("cannot create acreg !");
        return NULL;
    }
    ds = create_and_attach_one_ds(op = pr->one_p, nx, nx, 0);
    if (ds == NULL)
    {    
         win_printf("cannot find stuff in acreg!");    
        return NULL;
    }
    
    ds->source = my_sprintf(ds->source,"DNA force extension curve f_x \n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g",
			    bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");
    
    if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create ds!");
        return NULL;
    }
    
    ds->source = my_sprintf(ds->source,"DNA force extension curve f_y \n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");
    
    
    if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create ds!");
        return NULL;
    }
    
    
    ds->source = my_sprintf(ds->source,"DNA force extension curve f_m \n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;    
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");
        
    set_plot_title(op, "Sample %s",sample);
    set_plot_x_title(op, "Allongement (\\mu m)");
    set_plot_y_title(op, "Force (pN)");
    set_plot_file(op,"force.gr");
    set_plot_path(op,"%s/%s/",cur_path,name);
    
    if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create plot !");
        return NULL;
    }
    
    
    ds = op->dat[0];
    ds->source = my_sprintf(ds->source,"DNA stiffness versus extension curve \n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");

    set_plot_title(op, "Sample %s",sample);
    set_plot_x_title(op, "Allongement (\\mu m)");
    set_plot_y_title(op, "Stiffness (10^{-8} N/m)");
    set_plot_file(op,"ldekz.gr");
    set_plot_path(op,"%s/%s/",cur_path,name);

    if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create plot !");
        return NULL;
    }
    
    
    ds = op->dat[0];
    ds->source = my_sprintf(ds->source,"DNA extension versus sigma curve \n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");

    set_plot_title(op, "Sample %s",sample);
    set_plot_y_title(op, "Allongement (\\mu m)");
    set_plot_x_title(op, "nombre de tours");
    set_plot_file(op,"chapeaux.gr");
    set_plot_path(op,"%s/%s/",cur_path,name);
    
    if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create plot !");
        return NULL;
    }
        

    ds = op->dat[0];
    ds->source = my_sprintf(ds->source,"DNA extension versus Z magnet curve \n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");

    set_plot_title(op, "Sample %s",sample);
    set_plot_x_title(op, "Allongement (\\mu m)");
    set_plot_y_title(op, "Magnets distance");
    set_plot_file(op,"lzmag.gr");
    set_plot_path(op,"%s/%s/",cur_path,name);
    
    if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create plot !");
        return NULL;
    }
    
    
    ds = op->dat[0];
    ds->source = my_sprintf(ds->source,"Force versus Z magnet curve f_x\n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");

    set_plot_title(op, "Sample %s",sample);
    set_plot_y_title(op, "Force (pN)");
    set_plot_x_title(op, "Magnets distance");
    set_plot_file(op,"forcez.gr");
    set_plot_path(op,"%s/%s/",cur_path,name);
    
    if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create ds!");
        return NULL;
    }
    ds->source = my_sprintf(ds->source,"Force versus Z magnet curve f_y\n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");    

    if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create ds!");
        return NULL;
    }
    ds->source = my_sprintf(ds->source,"Force versus Z magnet curve f_m\n"
			    "xbr = %g ybr = %g zbr = %g \n"
			    " xpr = %g ypr = %g zpr = %g ",
			    bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");            
            
    if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create plot !");
        return NULL;
    }
        

    ds = op->dat[0];
    ds->source = my_sprintf(ds->source,"DNA bead position \nxbr = %g ybr = %g zbr = %g \n"
                " xpr = %g ypr = %g zpr = %g ",bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");

    set_plot_title(op, "Bead hozizontal displacement");
    set_plot_y_title(op, "y (\\mu m)");
    set_plot_x_title(op, "x (\\mu m)");    
    set_plot_file(op,"beadxy.gr");
    set_plot_path(op,"%s/%s/",cur_path,name);

    if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create plot !");
        return NULL;
    }
        

    ds = op->dat[0];
    ds->source = my_sprintf(ds->source,"DNA reference position \nxbr = %g ybr = %g zbr = %g \n"
                " xpr = %g ypr = %g zpr = %g ",bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;    
    set_dot_line(ds);
    set_plot_symb(ds, "\\pl");
    
    set_plot_title(op, "r�f�rence drift");
    set_plot_y_title(op, "y (\\mu m)");
    set_plot_x_title(op, "x (\\mu m)");    
    set_plot_file(op,"refxy.gr");
    set_plot_path(op,"%s/%s/",cur_path,name);
    
    if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create plot !");
        return NULL;
    }
    
    
    ds = op->dat[0];
    ds->source = my_sprintf(ds->source,"DNA anisotropy curve \nxbr = %g ybr = %g zbr = %g \n"
                " xpr = %g ypr = %g zpr = %g ",bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");

    set_plot_title(op, "Sample %s",sample);
    set_plot_y_title(op, "Anisotropy (%%)");
    set_plot_x_title(op, "index du points");
    set_plot_file(op,"anisotro.gr");
    set_plot_path(op,"%s/%s/",cur_path,name);
    
    if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create plot !");
        return NULL;
    }
        

    ds = op->dat[0];
    ds->source = my_sprintf(ds->source,"DNA angle anisotropy curve \nxbr = %g ybr = %g zbr = %g \n"
                " xpr = %g ypr = %g zpr = %g ",bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");

    set_plot_title(op, "Sample %s",sample);
    set_plot_y_title(op, "Aisotropy angle({}^{\\pt5\\oc})");
    set_plot_x_title(op, "index du points");                        
    set_plot_file(op,"anisoang.gr");
    set_plot_path(op,"%s/%s/",cur_path,name);
                        
    if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create plot !");
        return NULL;
    }
        

    ds = op->dat[0];
    ds->source = my_sprintf(ds->source,"DNA viscosity radius 0 \nxbr = %g ybr = %g zbr = %g \n"
                " xpr = %g ypr = %g zpr = %g ",bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");

    if ((ds = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create ds!");
        return NULL;
    }
        

    ds->source = my_sprintf(ds->source,"DNA viscosity radius 1 \nxbr = %g ybr = %g zbr = %g \n"
                " xpr = %g ypr = %g zpr = %g ",bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;    
    set_dot_line(ds);
    set_plot_symb(ds, "\\pl");
    
    set_plot_title(op, "viscosity \\times r");
    set_plot_y_title(op, "\\eta r (\\mu m \\times millipoiseuille})");
    set_plot_x_title(op, "L (\\mu m)");    
    set_plot_file(op,"etar.gr");
    set_plot_path(op,"%s/%s/",cur_path,name);
    
    if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create plot !");
        return NULL;
    }
        

    ds = op->dat[0];
    ds->source = my_sprintf(ds->source,"DNA x position versus z \nxbr = %g ybr = %g zbr = %g \n"
                " xpr = %g ypr = %g zpr = %g ",bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;
    set_dot_line(ds);
    set_plot_symb(ds, "\\oc");
    set_plot_title(op, "Bead displacements");
    set_plot_y_title(op, "Height z (\\mu m)");
    set_plot_x_title(op, "X (\\mu m)");    
    set_plot_file(op,"hdex.gr");
    set_plot_path(op,"%s/%s/",cur_path,name);

    if ((op = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
    {
         win_printf("cannot create plot !");
        return NULL;
    }
        

    ds = op->dat[0];
    ds->source = my_sprintf(ds->source,"DNA y position versus z \nxbr = %g ybr = %g zbr = %g \n"
                " xpr = %g ypr = %g zpr = %g ",bd_x0,bd_y0,bd_z0,du_x0,du_y0,du_z0 );
    ds->nx = ds->ny = 0;    
    set_dot_line(ds);
    set_plot_symb(ds, "\\pl");
    
    set_plot_title(op, "Bead displacements");
    set_plot_y_title(op, "Height z (\\mu m)");
    set_plot_x_title(op, "Y (\\mu m)");    
    set_plot_file(op,"hdey.gr");
    set_plot_path(op,"%s/%s/",cur_path,name);

    //    setaction_dybox(&pr->db, IS_BOX_REGION, run_pr_db_2, (void*)pr);

    do_one_plot(pr);
    
    return pr;

}
	
int display_mds_point(pltreg *pr, multi_d_s *mds, int index)
{
	register int i;
	char file[256], path[256];
	void *b;
	
	i = win_printf("point index %g file index %g rot %g zmag %g nb of points %g\n"
		       "bead x %g k_xd %g kxs %g \ndust x %g fc_x %g fc_x %g (Hz) \\sigma %g\n"
		       "bead y %g k_yd %g kys %g \ndust y %g fc_y %g fc_y %g (Hz) \\sigma %g\n"
		       "bead z %g k_zd %g kzs %g \ndust z %g fc_z %g fc_z %g (Hz) \\sigma %g\n"
		       "l=\\sqrt{x^2+y^2+z^2} %g f_{ox} %g f_{oy} %g\nanisotropy %g angle %g\n"
		       "l_z %g f_{1x} %g f_{1y} %g\n load file %s%03d.gr ?",mds->x[0][index],
		       mds->x[1][index],mds->x[2][index],mds->x[3][index],mds->x[4][index],
		       mds->x[5][index],mds->x[6][index],mds->x[7][index],mds->x[8][index],
		       mds->x[9][index],mds->x[10][index],mds->x[11][index],mds->x[12][index],
		       mds->x[13][index],mds->x[14][index],mds->x[15][index],mds->x[16][index],
		       mds->x[17][index],mds->x[18][index],mds->x[19][index],mds->x[20][index],
		       mds->x[21][index],mds->x[22][index],mds->x[23][index],mds->x[24][index],
		       mds->x[25][index],mds->x[26][index],mds->x[27][index],mds->x[28][index],
		       mds->x[29][index],mds->x[30][index],mds->x[31][index],mds->x[32][index],
		       mds->x[33][index],mds->name[1],(int)(mds->x[1][index]));
	if (i == OK)
	{
		sprintf(file,"%s%03d.gr",mds->name[1],(int)(mds->x[1][index]));
		if (pr->path != NULL)	
		{
		        sprintf(path,"%s/",pr->path);
		  	load_plt_file_in_pltreg(pr, file, path);
			reorder_loaded_plot_file_list(file, path);
			reorder_visited_path_list(path);
			return D_REDRAW;
		}
	}
	return 0;
}



int		display_nearest_point_acquisition(pltreg *pr, int x_0, int y_0, DIALOG *d)
{
	register int i,j;
	float x, y, de, tmp;
	O_p *op;
	d_s *ds;
	multi_d_s *mds;

	mds = find_multi_d_s_in_pltreg(pr);		
	if (mds == NULL)  return D_O_K;

	if (pr->one_p == NULL)		return 1;
	op = pr->one_p;
	//sc = pr->screen_scale;	
	//b = pr->stack;
	y = y_pr_2_pltdata_raw(pr, y_0 - d->y);
	x = x_pr_2_pltdata_raw(pr, x_0 - d->x);	
	ds = op->dat[op->cur_dat];
	de = (x-ds->xd[0])*(x-ds->xd[0])+(y-ds->yd[0])*(y-ds->yd[0]);
	for (i = 0, j = 0; i <ds->nx; i++)
	  {
	    tmp = (x-ds->xd[i])*(x-ds->xd[i])+(y-ds->yd[i])*(y-ds->yd[i]);
	    j = (tmp < de) ? i : j;
	    de = (tmp < de) ? tmp : de;
	  } 
	return display_mds_point(pr, mds, j);
}
int	load_lequalz_file(void)
{
	register int i, index;
	char path[512], file[256];
	static char fullfile[512], *fu = NULL;
	multi_d_s *mds;
	pltreg* pr;
	

	pr = find_pr_in_current_dialog(NULL);
	mds = find_multi_d_s_in_pltreg(pr);		
	/*
	if(updating_menu_state != 0)	
	{
		xvplot_plot_menu(mds);	
		return D_O_K;	
	}
	*/
	if(updating_menu_state != 0) return D_O_K;	
	index = RETRIEVE_MENU_INDEX;
	switch_allegro_font(1);
	if (fu == NULL)
	{
		fu = (char*)get_config_string("TRACKING-LOG-FILE","last_loaded",NULL);
		if (fu != NULL)	
		{
			strcpy(fullfile,fu);
			fu = fullfile;
		}
		else
		{
			my_getcwd(fullfile, 512);
			strcat(fullfile,"\\");
		}
	}

        i = file_select_ex("Experiment log file", fullfile, "tex", 512, 0, 0);
	switch_allegro_font(0);
	if (i != 0) 	
	{
		fu = fullfile;
		set_config_string("TRACKING-LOG-FILE","last_loaded",fu);
		extract_file_name(file, 256, fullfile);
		extract_file_path(path, 512, fullfile);
		strcat(path,"\\");
		if (index == 0 || pr == NULL)	lequalz_file(fullfile);
		else	lequalz_append_file(pr, fullfile);
	}
	return 0;
}





MENU *lequalz_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)    return mn;
    //add_item_to_menu(mn,"data set rescale in Y", do_lequalz_rescale_data_set,NULL,0,NULL);
    //add_item_to_menu(mn,"plot rescale in Y", do_lequalz_rescale_plot,NULL,0,NULL);
    //    add_item_to_menu(mn, "Rm point", kill_point,NULL,0,NULL);
    add_item_to_menu(mn, "new file", load_lequalz_file,NULL,MENU_INDEX(0),NULL);
    add_item_to_menu(mn, "append file", load_lequalz_file,NULL,MENU_INDEX(1),NULL);


    return mn;
}

int    lequalz_main(int argc, char **argv)
{
  (void)argc;
    (void)argv;
    xvplot_main(0, NULL);
    add_plot_treat_menu_item ( "lequalz", NULL, lequalz_plot_menu(), 0, NULL);
    general_pr_action = display_nearest_point_acquisition;
    return D_O_K;
}

int    lequalz_unload(int argc, char **argv)
{
      (void)argc;
    (void)argv;
    remove_item_to_menu(plot_treat_menu, "lequalz", NULL, NULL);
    return D_O_K;
}
#endif

