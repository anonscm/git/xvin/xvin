#ifndef _GC_H_
#define _GC_H_


// definitions for GC treatments:
#define GC_CLASSIC          0x0100
#define GC_NORMALIZED	    0x0200
#define GC_NORMALIZED_TAU	0x0400

#define GC_ONE			    0x1000
#define GC_ALL			    0x2000 

// for fitting:
#define TAKE_VISIBLE	0x0100
#define TAKE_ALL		0x0200

// for convergence:
#define GC_INJECTED		0x0100
#define GC_DISSIPATED	0x0200


// definitions of functions:
PXV_FUNC(int, do_test_GC_symmetry, 	(void) );
PXV_FUNC(int, do_test_integrated_FT, 	(void) );

// GUI functions:
PXV_FUNC(MENU*, GC_plot_menu, (void));
PXV_FUNC(int, GC_main, (int argc, char **argv));
#endif

