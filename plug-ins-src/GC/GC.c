
#ifndef _GC_C_
#define _GC_C_


#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"

#include <string.h>
#include <gsl/gsl_histogram.h>

/* If you include other plug-ins header do it here*/ 
#include "../inout/inout.h"
#include "../hist/hist.h"
/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "GC.h"		// to add specific functions from GC.c in the 'hist' menu




int do_divide_by_tau(void)
{	int	index;
	register int i, k;
	pltreg	*pr;
	O_p	*op;
	d_s	*ds, *ds_test=NULL;
	int	tau; 
	int	*ind, n_ds;
	char s[128];

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	if (!( (index&GC_ONE) || (index&GC_ALL) )) return(win_printf_OK("? work on one or all dataset(s) ?"));

	if (key[KEY_LSHIFT]) 
	{ 	return(win_printf_OK("This function divides the GC symmetry function\n"
				"ln(p(+a)/p(-a))\nby \\tau\n\n"
				"value of \\tau is searched for in dataset information"));
	}
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)  return win_printf_OK("cannot find data!");

	if (index&GC_ONE)
	{ 	n_ds  = 1;
		ind   = (int*)calloc(n_ds,sizeof(int));
		ind[0]= op->cur_dat;
	}
	else
	{	n_ds  = op->n_dat;
		ind   = (int*)calloc(n_ds,sizeof(int));
		for (i=0; i<n_ds; i++) ind[i]=i;
	}

	for (k=0; k<n_ds; k++)
	{	ds =  op->dat[ ind[k] ];
		if (ds == NULL)   return (win_printf_OK("can't find data set"));

		tau = grep_int_in_string(ds->treatement, "tau=");
		if (tau==-1) 	tau = grep_int_in_string(ds->history, "tau=");
		
		if (tau>0)
		{	ds_test = create_and_attach_one_ds(op, ds->nx, ds->ny, 0);
			if (ds_test==NULL) return(win_printf_OK("cannot allocate dataset"));

			for (i=0; i<ds->nx; i++)
			{	ds_test->xd[i] = ds->xd[i];
				ds_test->yd[i] = ds->yd[i]/tau;
			}		

			inherit_from_ds_to_ds(ds_test, ds);
			ds_test->treatement = my_sprintf(ds_test->treatement,"divided by tau=%d", tau);

			if ( (k==0) || (k==n_ds-1)) 
			set_ds_plot_label(ds_test, ds_test->xd[ds->nx*2/3-1], ds_test->xd[ds->ny-1], USR_COORD, 
				"\\tau = %d", tau);
			ds_test->color = ds->color;
		}
	}
	free(ind);
	
	if (n_ds>1)	// if we worked on all datasets...
	if (win_scanf("Remove old datasets ?")!=CANCEL)
	{	for (k=0; k<n_ds; k++) remove_ds_n_from_op (op, 0);	// always remove the 0th because counting is shifted after any one is deleted
		sprintf(s, "\\frac{1}{\\tau} %s", op->y_title);
		set_plot_y_title(op, s);
	}
	set_plot_x_auto_range (op);
	set_plot_y_auto_range (op);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of "do_divide_by_tau"









int do_adimension_symmetry_functions(void)
{	int	index;
	register int i, k;
	pltreg	*pr;
	O_p	*op;
	d_s	*ds, *ds_test=NULL;
	int	tau;
 static float	tau0=2.1873;
 static float	f_acq=819.2, mean_W=45;
	char	bool_tau=1; 
	int	*ind, n_ds;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	if (!( (index&GC_ONE) || (index&GC_ALL) )) return(win_printf_OK("? work on one or all dataset(s) ?"));

	if (key[KEY_LSHIFT]) 
	{ 	return(win_printf_OK("This function makes the GC symmetry functions non-dimensional\n"
				"initial functions are 'ln(p(+a_\\tau)/p(-a_\\tau))' vs 'a_\\tau'\n\n"
				"after non-dimensionalization, one has the functions defined by Cohen and van Zon\n"
				"value of \\tau is searched for in dataset information"));
	}
        if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)  return win_printf_OK("cannot find data!");

	if (win_scanf("non-dimensionalization of symetry functions\n\n"
			"\\tau_0 %8f (such that \\tau/\\tau_0 is OK, with \\tau being an integer)\n"
			"acquisition frequency %8f Hz\n"
			"<W> (same as <Q>) in kT units %8f\n"
			"%b multiply by \\tau (if not already normalized in Y by \\tau)\n",
			&tau0, &f_acq, &mean_W, &bool_tau) ==CANCEL) return(D_O_K);

	

	if (index&GC_ONE)
	{ 	n_ds  = 1;
		ind   = (int*)calloc(n_ds,sizeof(int));
		ind[0]= op->cur_dat;
	}
	else
	{	n_ds  = op->n_dat;
		ind   = (int*)calloc(n_ds,sizeof(int));
		for (i=0; i<n_ds; i++) ind[i]=i;
	}

	for (k=0; k<n_ds; k++)
	{	ds =  op->dat[ ind[k] ];
		if (ds == NULL)   return (win_printf_OK("can't find data set"));

		tau = grep_int_in_string(ds->treatement, "tau=");
		if (tau==-1) 	tau = grep_int_in_string(ds->history, "tau=");
		
		if (tau>0)
		{	ds_test = create_and_attach_one_ds(op, ds->nx, ds->ny, 0);
			if (ds_test==NULL) return(win_printf_OK("cannot allocate dataset"));

			for (i=0; i<ds->nx; i++)
			{	ds_test->xd[i] = ds->xd[i];
				ds_test->yd[i] = ds->yd[i] / (mean_W/f_acq);

				if (bool_tau==1) ds_test->yd[i] /= tau;
			}		

			inherit_from_ds_to_ds(ds_test, ds);
			ds_test->treatement = my_sprintf(ds_test->treatement,"normalized like Cohen-vanZon");

			if ( (k==0) || (k==n_ds-1)) 
			set_ds_plot_label(ds_test, ds_test->xd[ds->nx*2/3-1], ds_test->xd[ds->ny-1], USR_COORD, 
				"\\tau = %d", tau);
			ds_test->color = ds->color;
		}
		else return(win_printf_OK("error, no tau values in dataset info!"));

	}

	free(ind);
	
	if (n_ds>1)	// if we worked on all datasets...
	if (win_scanf("Remove old datasets ?")!=CANCEL)
	for (k=0; k<n_ds; k++) remove_ds_n_from_op (op, 0);	// always remove the 0th because counting is shifted after any one is deleted
	set_plot_x_auto_range(op);
	set_plot_y_auto_range(op);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of "do_adimension_symmetry_functions"






/********************************************************************/
/* test the GC theorem                               				*/
/* computing symetry functions out of a pdf							*/
/********************************************************************/
int do_test_GC_symmetry(void)	// allocates the second dataset !
{	register int i,k;
	int		index;
	int 	*ind=NULL, n_ds=1;
	pltreg	*pr;
	O_p	*op, *op_new=NULL;
	d_s	*ds, *ds_test;
	int	indice_of_zero, indice_of_min, indice_of_max;
	int	excursion_max, tau;
	int  nh, 		// number of points in n histogram
		 nsf=0;		// number of symmetry functions correctly computed
	gsl_histogram *hist;
	int 	bool_filter=0, bool_new_plot=0, bool_normalize_by_a=0, bool_normalize_by_tau=0;
	double	p_p,p_n;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	if (!( (index&GC_ONE) || (index&GC_ALL) )) return(win_printf_OK("? work on one or all dataset(s) ?"));

	if (key[KEY_LSHIFT]) 
	{ 	if (index & GC_CLASSIC) return win_printf_OK("This routine tests the symmetry of a pdf around 0\n"
			"it computes:\n\n S(a) = ln( {p(x=+a) \\over p(x=-a)} )\n\n(Gallavotti-Cohen theorem)\n"
			"a new dataset is created");
		else if (index & GC_NORMALIZED) return win_printf_OK("This routine tests the symmetry of a pdf around 0\n"
			"and normalized the result by a\n."
			"it computes:\n\n S(a) = {1 \\over a} ln( {p(x=+a) \\over p(x=-a)} )\n\n(Gallavotti-Cohen theorem)"
			"a new dataset is created");
		else return(win_printf_OK("Mmm... there is a problem.\nThis function has no use..."));
	}
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)  return win_printf_OK("cannot find data!");
	
    if (index & GC_CLASSIC) 		bool_normalize_by_a = 0;
    if (index & GC_NORMALIZED)		bool_normalize_by_a = 1;
    if (index & GC_NORMALIZED_TAU)	bool_normalize_by_tau = 1;
    
	if (index&GC_ONE)
	{ 	n_ds  = 1;
		ind   = (int*)calloc(n_ds,sizeof(int));
		ind[0]= op->cur_dat;
	}
	else
	{	i=win_scanf("{\\pt14\\color{yellow}Compute symmetry functions}\n\n"
					"%b normalize by \\tau\n"
					"%b normalize by a\n\n"
					"%R remove old datasets\n"
					"%r create a new plot", 
					&bool_normalize_by_tau, &bool_normalize_by_a, &bool_new_plot);
		if (i==CANCEL) return(D_O_K);
		if (bool_new_plot==0) op_new=op;
			
		n_ds  = op->n_dat;
		ind   = (int*)calloc(n_ds,sizeof(int));
		for (i=0; i<n_ds; i++) ind[i]=i;
	}

	for (k=0; k<n_ds; k++)
	{	ds =  op->dat[ ind[k] ];
		if (ds == NULL)   return (win_printf_OK("can't find data set"));
		nh=ds->nx;
		ds_to_histogram(ds, &hist);
	
		if (gsl_histogram_find (hist, 0, &indice_of_zero) != 0/*GSL_SUCCESS*/) 
		{	gsl_histogram_free(hist);
			return(win_printf_OK("0 is not in the histogram (dataset %d)!", ind[k]));
		}

		indice_of_min = 0;
		for (i=0; i<indice_of_zero; i++)
		{	if ( gsl_histogram_get(hist, i)==0) indice_of_min=i;
		}
		indice_of_max = nh;
		for (i=nh-1; i>indice_of_zero; i--)
		{	if ( gsl_histogram_get(hist, i)==0) indice_of_max=i;
		}	
		
		if ( (indice_of_zero-indice_of_min) > (indice_of_max-indice_of_zero) )
			excursion_max = (indice_of_max-indice_of_zero);
		else
			excursion_max = (indice_of_zero-indice_of_min);

//	win_printf_OK("zero is at %d, min is at %d, max is at %d, excursion max= %d",
//			indice_of_zero, indice_of_min, indice_of_max, excursion_max);

		if (excursion_max>1)
		{	nsf++; // we count one more symmetry function
		
			if ( (nsf==1) && (bool_new_plot==1)	) 
			{		op_new = create_and_attach_one_plot(pr, excursion_max, excursion_max, 0);
					ds_test= op_new->dat[0];
			}
			else ds_test = create_and_attach_one_ds(op_new, excursion_max, excursion_max, 0);
			if (ds_test==NULL) return(win_printf_OK("cannot allocate dataset"));
			if (bool_filter==1)
			{ 	win_printf("not implemented yet!"); }
			else
			{  	for (i=0; i<excursion_max; i++)
				{	ds_test->xd[i] = ds->xd[indice_of_zero+i];
					p_p = gsl_histogram_get(hist, indice_of_zero+i);
					p_n = gsl_histogram_get(hist, indice_of_zero-i);
					ds_test->yd[i] = (float)(log(p_p/p_n));
				}
			}

			inherit_from_ds_to_ds(ds_test, ds);
			ds_test->color = ds->color;
			ds_test->treatement = my_sprintf(ds_test->treatement,"GC test %d points", excursion_max);

			if (bool_normalize_by_a)
			{	ds_test->yd[0]=0;
				for (i=1; i<excursion_max; i++)	ds_test->yd[i] /= ds_test->xd[i];
				ds_test->treatement = my_sprintf(ds_test->treatement,"normalized by a");
			}

			if (bool_normalize_by_tau)
			{	tau = grep_int_in_string(ds->treatement, "tau=");
				if (tau==-1) 	tau = grep_int_in_string(ds->history, "tau=");
		
				if (tau>0)
				{	for (i=0; i<excursion_max; i++)	ds_test->yd[i] /= (float)tau;
					ds_test->treatement = my_sprintf(ds_test->treatement,"normalized by \\tau");
				}
			}
			
			ds_test->treatement = my_sprintf(ds_test->treatement,"\n");
		}

		gsl_histogram_free(hist);


	}

	free(ind);
	
	if (n_ds>1)	// if we worked on all datasets...
	if (bool_new_plot==0)
	{	for (k=0; k<n_ds; k++)
			remove_ds_n_from_op (op, 0);	// always remove the 0th because counting is shifted after any one is deleted
    }
	set_plot_title  (op_new, "symmetry function");
	set_plot_y_title(op_new, "%s%sln({\\frac{p(+a)}{p(-a)}})", 
					(bool_normalize_by_a) ? "{\\frac{1}{a}}" : "",
					(bool_normalize_by_tau) ? "{\\frac{1}{\\tau}}" : "");
	set_plot_x_title(op_new, "value a");
	
   	op_new->iopt &= ~(XLOG|YLOG);
	set_plot_x_auto_range (op_new);
	set_plot_y_auto_range (op_new);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of 'do_test_GC_symmetry'










/* test the integrated fluctuation theorem */
int do_test_integrated_FT(void)	// allocates the second dataset !
{	register int i,k;
	int	index;
	int 	*ind=NULL, n_ds=1;
	pltreg	*pr;
	O_p	*op, *op_result=NULL;
	d_s	*ds, *ds_result;
	int 	indice_of_zero;
	int nh;
	gsl_histogram *hist;
	int		*tau, n_good;
static float	sigma_assymptotic=1., fec=2048.;
	double	p_p, p_n, *proba_p, *proba_n, *expo_n, *expo_p, e_p, e_n, sum, dbin, scale, lower, upper, mean, sigma_tmp;
static int bool_proba_direct=0, bool_proba_expo=0, bool_use_tau=1, bool_IFT_negs=1;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	
	if (key[KEY_LSHIFT]) 
	{ 	return win_printf_OK("This routine tests the integrated fluct. theor. (IFT)\n"
			"It computes P-/P+ vs \\tau, where\n\n"
			"P_{-}(\\tau) = \\int_0^\\infty P(X_\\tau=-a) da   and   "
			"P_{+}(\\tau) = \\int_0^\\infty P(X_\\tau=+a) da \n\n\n"
			"as well as < exp(-\\sigma_+ X_\\tau) >\n\n for all datasets representing pdfs of X_\\tau\n\n"
			"if X_\\tau is by definition divided by \\tau, then one should instead study the IFT with\n"
			"< exp(-\\sigma_+ \\tau X_\\tau) > , ie, multiply by \\tau  in the exponential\n\n"
			"a new plot is created and populated at will.");
	}
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)  return win_printf_OK("cannot find data!");

    if (win_scanf("{\\pt14\\color{yellow}Integrated Fluctuation Theorem}\n\n"
    			"What is the asymptotic \\sigma_+ ? %8f\n"
    			"What is the sampling frequency f_s ? %8f (such that 1 pt is 1/f_s second)\n"
    			"%b multiply A by \\tau in the exponential (necessary if power instead of work)\n\n"
    			"%b plot estimate from negative events (low accuracy!)\n"
    			"%b plot integrated probabilities, computed directly\n"
    			"%b plot integrated probabilities, deduced from IFT\n", 
    			&sigma_assymptotic, &fec,
    			&bool_IFT_negs, &bool_use_tau, &bool_proba_direct, &bool_proba_expo)==CANCEL) return(D_O_K);
    if (fec<=0) return(win_printf_OK("sampling frequency %g cannot be negative or zero", fec));
    
    n_ds  = op->n_dat;
		
	tau     = (int*)   calloc(n_ds, sizeof(int));
	proba_p = (double*)calloc(n_ds, sizeof(double));
	proba_n = (double*)calloc(n_ds, sizeof(double));
	expo_p  = (double*)calloc(n_ds, sizeof(double));
	expo_n  = (double*)calloc(n_ds, sizeof(double));
	n_good=0;
	    		
	for (k=0; k<n_ds; k++)
	{	ds =  op->dat[ k ];
		if (ds == NULL)   return (win_printf_OK("can't find data set"));
		nh=ds->nx;
		ds_to_histogram(ds, &hist);
	
		// now, normalize the histogram, to make it a nice pdf:		
		sum   = gsl_histogram_sum (hist); // sum of all the bins;
		gsl_histogram_get_range (hist, 1, &lower, &upper); // lower and upper bound of bin #1
		dbin  = upper-lower;
		scale = 1/(sum*dbin);
		gsl_histogram_scale(hist, (double)scale);	// multiplies histogram y-axis by scale

		// we need to know where is the mean : positive or negative ?		
		mean = gsl_histogram_mean (hist);

		if (gsl_histogram_find (hist, 0, &indice_of_zero) != 0/*GSL_SUCCESS*/) 
		{	gsl_histogram_free(hist);
			return(win_printf_OK("0 is not in the histogram (dataset %d)!", ind[k]));
		}
		
		tau[n_good] = grep_int_in_string(ds->treatement, "tau=");
		if (tau[n_good]==-1) 	tau[n_good] = grep_int_in_string(ds->history, "tau=");
		
		if (bool_use_tau==1)	sigma_tmp = sigma_assymptotic*(double)tau[n_good]/fec;
		else					sigma_tmp = sigma_assymptotic;
		
		p_n=0.; // (double)gsl_histogram_get(hist, indice_of_zero);
		p_p=0.; // (double)gsl_histogram_get(hist, indice_of_zero);
		e_n=0.;
		e_p=0.;				

		if (mean>0)
		{	for (i=0; i<indice_of_zero; i++)
			{	p_n += (double)gsl_histogram_get(hist, i)*dbin;		
				e_n += (double)gsl_histogram_get(hist, i)*exp(-(double)ds->xd[i]*sigma_tmp)*dbin;
			}
			e_n /= p_n;
			
			for (i=indice_of_zero; i<nh; i++)
			{	p_p += (double)gsl_histogram_get(hist, i)*dbin;		
				e_p += (double)gsl_histogram_get(hist, i)*exp(-(double)ds->xd[i]*sigma_tmp)*dbin;
			}
			e_p /= p_p;
		}
		else if (mean<0) 
		{	for (i=0; i<indice_of_zero; i++)
			{	p_p += (double)gsl_histogram_get(hist, i)*dbin;		
				e_p += (double)gsl_histogram_get(hist, i)*exp(+(double)ds->xd[i]*sigma_tmp)*dbin;
			}
			e_p /= p_p;

			for (i=indice_of_zero; i<nh; i++)
			{	p_n += (double)gsl_histogram_get(hist, i)*dbin;		
				e_n += (double)gsl_histogram_get(hist, i)*exp(+(double)ds->xd[i]*sigma_tmp)*dbin;
			}
			e_n /= p_n;
		}
	
		gsl_histogram_free(hist);
				
		if ( (tau[n_good]!=-1) && (p_p>0) && (p_n>0) && (mean!=0) )
		{	proba_p[n_good] = p_p;
			proba_n[n_good] = p_n;
			expo_p [n_good] = e_p;
			expo_n [n_good] = e_n;
			n_good++;
		}
	}
	
	if (n_good<=0) return(win_printf_OK("IFT p+/p- : I didn't find anything..."
				"Either there are no positive events, or there are no negative events..."));
				
	op_result = create_and_attach_one_plot(pr, n_good, n_good, 0);
	if (op_result == NULL)		return(win_printf_OK("Cannot allocate plot"));
	
	// first dataset : the ratio of integrated probabilities
	if ((ds_result=op_result->dat[0])== NULL)		return(win_printf_OK("Cannot allocate dataset"));
    for (k=0; k<n_good; k++)
    {	ds_result->xd[k] = tau[k];
    	ds_result->yd[k] = (float)(proba_n[k]/proba_p[k]);
	}	
	ds_result->treatement = my_sprintf(ds_result->treatement,"IFT test: P-/P+ from %d histograms", n_good);
	
	// second dataset : the exponential, averaged on positive values
	if ((ds_result=create_and_attach_one_ds(op_result, n_good, n_good, 0))==NULL) return(win_printf_OK("Cannot allocate dataset"));
	for (k=0; k<n_good; k++)
	{	ds_result->xd[k] = tau[k];
		ds_result->yd[k] = (float)expo_p[k];
  	}
	ds_result->treatement = my_sprintf(ds_result->treatement,"IFT test: <exp(-X)>|(X>0)");

	if (bool_IFT_negs==1) // the exponential, averaged on negative events, this cannot be a precise estimate!
	{ if ((ds_result=create_and_attach_one_ds(op_result, n_good, n_good, 0))==NULL) return(win_printf_OK("Cannot allocate dataset"));
	  for (k=0; k<n_good; k++)
	  {	ds_result->xd[k] = tau[k];
		ds_result->yd[k] = (float)1./(float)expo_n[k];
  	  }
  	  ds_result->treatement = my_sprintf(ds_result->treatement,"IFT test: 1/<exp(-X)>|(X<0)");
	}
	
  	if (bool_proba_direct==1)
	{ // dataset for integrated probability of a negative value
	  if ((ds_result=create_and_attach_one_ds(op_result, n_good, n_good, 0))==NULL) return(win_printf_OK("Cannot allocate dataset"));
	  for (k=0; k<n_good; k++)
	  {	ds_result->xd[k] = tau[k];
		ds_result->yd[k] = (float)proba_n[k];
  	  }
  	  ds_result->treatement = my_sprintf(ds_result->treatement,"IFT test: integrated P-, direct");

  	  // dataset for integrated probability of a positive value
	  if ((ds_result=create_and_attach_one_ds(op_result, n_good, n_good, 0))==NULL) return(win_printf_OK("Cannot allocate dataset"));
	  for (k=0; k<n_good; k++)
	  {	ds_result->xd[k] = tau[k];
		ds_result->yd[k] = (float)proba_p[k];
  	  }
  	  ds_result->treatement = my_sprintf(ds_result->treatement,"IFT test: integrated P+, direct");
	}
  
	if (bool_proba_expo==1)
	{ // dataset for integrated probability of a negative value, deduced from exponential averages
	  if ((ds_result=create_and_attach_one_ds(op_result, n_good, n_good, 0))==NULL) return(win_printf_OK("Cannot allocate dataset"));
	  for (k=0; k<n_good; k++)
	  {	ds_result->xd[k] = tau[k];
		ds_result->yd[k] = (float)expo_p[k]/((float)1.+(float)expo_p[k]);
  	  }
  	  ds_result->treatement = my_sprintf(ds_result->treatement,"IFT test: P- = <exp>/(1+<exp>) using IFT");

	  // dataset 4 : integrated probability of a positive value, deduced from exponential averages
	  if ((ds_result=create_and_attach_one_ds(op_result, n_good, n_good, 0))==NULL) return(win_printf_OK("Cannot allocate dataset"));
	  for (k=0; k<n_good; k++)
	  {	ds_result->xd[k] = tau[k];
		ds_result->yd[k] = (float)1./((float)1.+(float)expo_p[k]);
  	  }
  	  ds_result->treatement = my_sprintf(ds_result->treatement,"IFT test: P+ = 1/(1+<exp>) using IFT");
  	}

  	free(tau);
	free(proba_p); free(proba_n); free(expo_p); free(expo_n);
	  	
	set_plot_x_auto_range(op_result);
	set_plot_y_auto_range(op_result);
	set_plot_title  (op_result, "Integrated Fluctuation Theorem"); 
	set_plot_x_title(op_result, "\\tau (pts)"); 
	set_plot_y_title(op_result, "\\frac{p(a>0)}{P(a<0)},  <exp(-\\sigma_+%sa)>|_{a>0}", (bool_use_tau==1) ? "\\tau " : " "); 
	op_result->filename = Transfer_filename(op->filename);
    op_result->dir = Mystrdup(op->dir);
    	
	
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} /// end of 'do_test_integrated_FT'







int do_measure_slope(void)
{	register int i, k;
	int	index;	
	int	*ind, n_ds;
	pltreg	*pr;
	O_p	*op, *op_slopes;
	d_s	*ds=NULL, *ds2=NULL, *ds_slopes;
	int 	nx, n_keep;
	int	tau;
	double 	X2=0., XY=0., Y2=0., X=0., slope;
	int	bool_plot_fits=0;	// if ==1, then fits are plotted
	int bool_error_bars=1;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	if ( ! ( (index&TAKE_ALL) || (index&TAKE_VISIBLE)) )	return(win_printf_OK("bad call..."));	// visible points or all points
 	if ( ! ( (index&GC_ONE)   || (index&GC_ALL)) ) 		return(win_printf_OK("bad call..."));	// 1 or all datasets

	if (key[KEY_LSHIFT]) 
	{ 	if (index&TAKE_ALL) return win_printf_OK("This routine measures the slope,"
			"it assumes that the dataset is linear (not affine)\n"
			"all the points are used.");
		else if (index&TAKE_VISIBLE) return win_printf_OK("This routine measures the slope,"
			"it assumes that the dataset is linear (not affine)\n"
			"only visible points are used.");
		else return(win_printf_OK("Mmm... there is a problem.\nThis function has no use.?...?"));
	}

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)  return win_printf_OK("cannot find data!"); 
	if (index&GC_ONE)
	{ 	n_ds  = 1;
		ind   = (int*)calloc(n_ds,sizeof(int));
		ind[0]= op->cur_dat;
	}
	else
	{	n_ds  = op->n_dat;
		ind   = (int*)calloc(n_ds,sizeof(int));
		for (i=0; i<n_ds; i++) ind[i]=i;
	}

	op_slopes = create_and_attach_one_plot(pr, n_ds, n_ds, 0);
    	if (op_slopes == NULL)		return(win_printf_OK("Cannot allocate plot"));
    	ds_slopes = op_slopes->dat[0];
    	if (ds_slopes == NULL)		return(win_printf_OK("Cannot allocate dataset"));
		if (bool_error_bars==1)
		if ((alloc_data_set_y_error(ds_slopes)==NULL) || (ds_slopes->ye==NULL))
									return win_printf_OK("Cannot allocate error bars");


	for (k=0; k<n_ds; k++)
	{	ds =  op->dat[ ind[k] ];
		if (ds == NULL)   return (win_printf_OK("can't find data set"));
		nx=ds->nx;

		X2=(double)0.; XY=(double)0.; Y2=(double)0.; X=0.; n_keep=0;
		if (index & TAKE_VISIBLE)
	    	for (i=0; i<nx; i++)	
		  {	if ( (ds->xd[i] < op->x_hi) && (ds->xd[i] >= op->x_lo)
			  && (ds->yd[i] < op->y_hi) && (ds->yd[i] >= op->y_lo) )
			{	X  += (double)ds->xd[i];
				X2 += (double)ds->xd[i]*(double)ds->xd[i];
				XY += (double)ds->yd[i]*(double)ds->xd[i];
				Y2 += (double)ds->yd[i]*(double)ds->yd[i]; // required just for error bars
				n_keep++;
			}  
		  }
		else for (i=0; i<nx; i++) 
			{	X  += (double)ds->xd[i];
				X2 += (double)ds->xd[i]*(double)ds->xd[i];
				XY += (double)ds->yd[i]*(double)ds->xd[i];
				Y2 += (double)ds->yd[i]*(double)ds->yd[i]; // required just for error bars
				n_keep++;
			}  
		
		if (X2==0)	slope=0;
		else 		slope=(double)(XY/X2);

		if (bool_plot_fits==1)
		{	ds2 = create_and_attach_one_ds(op, 2, 2, 0);
			if (ds2==NULL) return(win_printf("cannot allocate dataset"));
			ds2->xd[0]=0;	ds2->yd[0]=0;
			ds2->xd[1]=ds->xd[n_keep-1]; 	ds2->yd[1]=(float)slope*ds2->xd[1];

			inherit_from_ds_to_ds(ds2, ds);
			ds2->treatement = my_sprintf(ds2->treatement,"purely linear fit (not affine)");
		}

		tau = grep_int_in_string(ds->treatement, "tau=");
		if (tau==-1) 	tau = grep_int_in_string(ds->history, "tau=");
	
		ds_slopes->xd[k] = (float)tau;
		ds_slopes->yd[k] = (float)slope;
	
		if (bool_error_bars==1)
		{	if (X2!=0) ds_slopes->ye[k] = (float)(sqrt(fabs(Y2/X2 - slope*slope)));
		}
	}
	
	free(ind);

	set_plot_title  (op_slopes, "slopes");
	set_plot_x_title(op_slopes, "\\tau (points, 1/f_{acq})"); 
	set_plot_y_title(op_slopes, "\\sigma(\\tau)");
    op_slopes->filename = Transfer_filename(op->filename);
    op_slopes->dir = Mystrdup(op->dir);

	inherit_from_ds_to_ds(ds_slopes, ds);
	ds_slopes->treatement = my_sprintf(ds_slopes->treatement,"slope of the GC sym. function vs \\tau\n%s",
		(index&TAKE_VISIBLE) ? "using visible points in x" : "using all points in x");

	return refresh_plot(pr, UNCHANGED);
}




double symetry_function_W(float a, float tau, float tau0)
{	double x, epsilon;

	x 	= (double)(tau/tau0);
	epsilon = (1-exp(-x))/x;
	return ( a/(1-epsilon) );
}



/*double symetry_function_Q(float a, float tau, float tau0, float p_diss)
{	double x;


double h(float p, float p_diss)
{	if (p>=1) return(0);
	return(8*p/(9-p*p) + 3*(log(3-p) + log(1+p) - log(3+p) - log(1-p))/(2*p_diss) );
}

double r(float p, float p_diss)
{	double r1 = -log(p_diss) -1.5*log(3-p) -1.5*log(1+p) -0.5*log(p-1) +0.5*log(576);
	double r2 = p_diss*(p+1)*(-2*p*p +8*p -10) / (4*(3-p));
	return(r1-1.5+r2); 
}

	x = (double)(tau/tau0);
	if (a<=1)	return (a);
			    // (a - h(a,p_diss)/x );
	if (a<=3)	return (a - (a-1)*(a-1)/4);
			    // (a - (a-1)*(a-1)/4 + (log(x)+r(a,p_diss))/(p_diss*x) );
	else		return (2);
			    // (2 + 2*sqrt(2*(a-3)/x) );
			    // (2 + 2*sqrt(2*(a-3)/x) - log(x)/(2*p_diss*x) );
}*/



/*
int do_plot_theor_sym_func(void)
{
	int i, i2, k;
	O_p 	*op = NULL;
	int	nx, n_ds, *ind;
	d_s 	*ds, *ds2=NULL;
	pltreg *pr = NULL;
	int	index;
static	float	mean_W=10;
	int	tau;
static  float	tau0=2.1873;
	float	f_acq=819.2;
	char	bool_tau=1;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	if ( ! ( (index&GC_INJECTED) || (index&GC_DISSIPATED)) ) 	return(win_printf_OK("bad call..."));	// for W or Q

	/* display routine action if SHIFT is pressed */
	/*if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine plots convergence functions for W or Q.\n"
					"all \tau values are treated\n");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return win_printf_OK("cannot plot region");
	nx = ds->ny;	// number of points in time series 
	
	if (win_scanf("I will compute theoretical f_\\tau^W or f_\\tau^Q\n\n"
			"\\tau_0 %8f (such that \\tau/\\tau_0 is OK, with \\tau being an integer)\n"
			"acquisition frequency %8f Hz\n"
			"<W> (same as <Q>) in kT/s units %8f\n"
			"%b multiply by \\tau (if not already normalized in Y by \\tau)\n",
			&tau0, &f_acq, &mean_W, &bool_tau) ==CANCEL) return(D_O_K);

	// indices of datasets that we will operated on:
	n_ds  = op->n_dat;
	ind   = (int*)calloc(n_ds,sizeof(int));
	for (i=0; i<n_ds; i++) ind[i]=i;

	for (k=0; k<n_ds; k++)
	{	ds =  op->dat[ ind[k] ];
		if (ds == NULL)   return (win_printf_OK("can't find data set"));
		nx=ds->nx;
	
		tau = grep_int_in_string(ds->treatement, "tau=");
		if (tau==-1) 	tau = grep_int_in_string(ds->history, "tau=");
	
		if ( (tau!=-1) && (tau!=0) )
		{	ds2 = create_and_attach_one_ds (op, nx, nx+1, 0);
			ds2->xd[0]=0;
			ds2->yd[0]=0;
			for (i=0; i<nx; i++)
			{	i2 = i; // +1;
				ds2->xd[i2] = ds->xd[i];
				ds2->yd[i2] = (index&GC_INJECTED) ? symetry_function_W(ds2->xd[i2], tau, tau0)
								 : symetry_function_Q(ds2->xd[i2], tau, tau0, mean_W/f_acq);
				ds2->yd[i2] *= mean_W/f_acq;
				if (bool_tau==1) ds2->yd[i2] *= (tau);
			}
//	inherit_from_ds_to_ds(ds2, ds1);
			ds2->treatement = my_sprintf(ds2->treatement,"tau=%d\n<W> = %f, convergence function for %s",
							(int)tau, mean_W, (index&GC_INJECTED) ? "W" : "Q");
			ds2->color = ds->color;
		}
		else return(win_printf_OK("error, no tau values in dataset info!"));
	}

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_plot_theor_sym_func*/







/*int do_look_at_convergence(void)
{	register int i, k;
	int	index;	
	int	*ind, n_ds;
	pltreg	*pr;
	O_p	*op, *op_out;
	d_s	*ds=NULL, *ds_out=NULL;
	int 	nx, n_keep, p_keep;
	int	tau, *taus;
static float	tau0=2.1873;
	double 	X=0, Y=0, XX=0;
//	int	bool_plot_fits=0;	// if ==1, then fits are plotted
	double  *tmp;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	if ( ! ( (index&TAKE_ALL) || (index&TAKE_VISIBLE)) )	return(win_printf_OK("bad call..."));	// visible points or all points
 	if ( ! ( (index&GC_ONE)   || (index&GC_ALL)) ) 		return(win_printf_OK("bad call..."));	// 1 or all datasets

	if (win_scanf("I will compute the distance between experimental f_\\tau^W or f_\\tau^Q\n"
			"and its theoretical value for \\tau -> \\infty.\n\n"
			"\\tau_0 %8f (such that \\tau/\\tau_0 is OK, with \\tau being an integer)\n"
			"(this will be used to on-dimensionalize the \\tau-axis)\n\n"
			"note that symetry functions must be non-dimensionalized first!!!",
			&tau0) ==CANCEL) return(D_O_K);


	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)  return win_printf_OK("cannot find data!"); 
	if (index&GC_ONE)
	{ 	n_ds  = 1;
		ind   = (int*)calloc(n_ds,sizeof(int));
		ind[0]= op->cur_dat;
	}
	else
	{	n_ds  = op->n_dat;
		ind   = (int*)calloc(n_ds,sizeof(int));
		for (i=0; i<n_ds; i++) ind[i]=i;
	}

	XX=0;
	p_keep=0;
	tmp =(double*)calloc(n_ds, sizeof(double));
	taus=(int*)   calloc(n_ds, sizeof(int));

	for (k=0; k<n_ds; k++)
	{	ds =  op->dat[ ind[k] ];
		if (ds == NULL)   return (win_printf_OK("can't find data set"));
		nx=ds->nx;

		tau = grep_int_in_string(ds->treatement, "tau=");
		if (tau==-1) 	tau = grep_int_in_string(ds->history, "tau=");
	

		X=(double)0; Y=(double)0; n_keep=0;
		if (index & TAKE_VISIBLE)
	    	for (i=0; i<nx; i++)	
		  {	if ( (ds->xd[i] < op->x_hi) && (ds->xd[i] >= op->x_lo) )
			{	X += (double)ds->xd[i];
				Y += (double)ds->yd[i];
				n_keep++;
			}  
		  }

		if ( (n_keep>0) && (tau>0) )
		{	XX   	    += (X/n_keep);
			tmp [p_keep] = Y/n_keep;
			taus[p_keep] = tau;
			p_keep ++;
		}
	}
	if (p_keep<=0) return(win_printf_OK("no values are treatable!\ntry to enlarge the zone"));
	XX /= p_keep;

	op_out = create_and_attach_one_plot(pr, p_keep, p_keep, 0);
    	if (op_out == NULL)		return(win_printf_OK("Cannot allocate plot"));
    	ds_out = op_out->dat[0];
    	if (ds_out == NULL)		return(win_printf_OK("Cannot allocate dataset"));

	for (k=0; k<p_keep; k++)
	{	ds_out->xd[k] = taus[k]/tau0;
		ds_out->yd[k] = tmp[k] - symetry_function_Q(XX, taus[k], tau0, 0);
	}


	free(tmp);

	set_plot_title  (op_out, "convergence for Q_\\tau/<Q_\\tau> = %4.3g", XX);
	set_plot_x_title(op_out, "\\tau/\\tau_0"); 
	set_plot_y_title(op_out, "f_Q(\\tau) - f_Q(\\infty)");
    	op_out->filename = Transfer_filename(op->filename);
    	op_out->dir = Mystrdup(op->dir);

	inherit_from_ds_to_ds(ds_out, ds);
	
	return refresh_plot(pr, UNCHANGED);
}// end do_look_at_convergence*/






MENU *GC_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
//	add_item_to_menu(mn,"GC symmetry function (on one dataset)",	do_test_GC_symmetry,	NULL, GC_CLASSIC|GC_ONE,	NULL);
	add_item_to_menu(mn,"GC symmetry function",						do_test_GC_symmetry,	NULL, GC_CLASSIC|GC_ALL,	NULL);
//	add_item_to_menu(mn,"\0", 										NULL,NULL,0,NULL);
//	add_item_to_menu(mn,"test GC symmetry, normalized by x", 		do_test_GC_symmetry,	NULL, GC_NORMALIZED|GC_ONE,	NULL);
	add_item_to_menu(mn,"GC symmetry function, normalized by x",	do_test_GC_symmetry,	NULL, GC_NORMALIZED|GC_ALL,	NULL);
	add_item_to_menu(mn,"GC symmetry function, normalized by \\tau",do_test_GC_symmetry,	NULL, GC_NORMALIZED_TAU|GC_ALL,	NULL);
	add_item_to_menu(mn,"\0", 				NULL,NULL,0,NULL);
	add_item_to_menu(mn,"Integrated FT: P+/P- vs <exp(-X)> (all)",  do_test_integrated_FT,  NULL, 0,					NULL);
	add_item_to_menu(mn,"\0", 				NULL,NULL,0,NULL);
	add_item_to_menu(mn,"normalize symmetry functions by tau",		do_divide_by_tau,		NULL, GC_ALL,				NULL);
	add_item_to_menu(mn,"normalize symmetry functions (full).", 	do_adimension_symmetry_functions,NULL, GC_ALL,		NULL);
	add_item_to_menu(mn,"\0",				NULL,NULL, 0, NULL);	
//	add_item_to_menu(mn,"Plot theor. Sym. func. for W",				do_plot_theor_sym_func, NULL, GC_INJECTED, 			NULL);	
//	add_item_to_menu(mn,"Plot theor. Sym. func. for Q",				do_plot_theor_sym_func, NULL, GC_DISSIPATED, 		NULL);	
	add_item_to_menu(mn,"\0", 				NULL,NULL,0,NULL);
	add_item_to_menu(mn,"fit slope on all points",					do_measure_slope,		NULL, TAKE_ALL|GC_ALL,		NULL);
	add_item_to_menu(mn,"fit slope on visible points",				do_measure_slope,		NULL, TAKE_VISIBLE|GC_ALL,	NULL);
//	add_item_to_menu(mn,"examine convergence for Q",				do_look_at_convergence,	NULL, TAKE_VISIBLE|GC_ALL,	NULL);

	return mn;
}

int	GC_main(int argc, char **argv)
{
	add_plot_treat_menu_item("Fluctuation Theorems", NULL, GC_plot_menu(), 0, NULL);
	return D_O_K;
}
#endif

