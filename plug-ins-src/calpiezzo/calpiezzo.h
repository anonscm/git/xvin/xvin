#ifndef _CALPIEZZO_H_
#define _CALPIEZZO_H_

PXV_FUNC(int, do_calpiezzo_rescale_plot, (void));
PXV_FUNC(MENU*, calpiezzo_plot_menu, (void));
PXV_FUNC(int, do_calpiezzo_rescale_data_set, (void));
PXV_FUNC(int, calpiezzo_main, (int argc, char **argv));
#endif

