/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _CALPIEZZO_C_
#define _CALPIEZZO_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "calpiezzo.h"


int do_calpiezzo_compute_dz(void)
{
	register int i, j, k;
	O_p *op = NULL;
	int nf;
	static int n_dso, per, n_per;
	d_s *ds, *dsi, *dso, *dstmp;
	pltreg *pr = NULL;
	float interpolate_point_by_poly_2_in_array(float *x, float *y, int nx, float xp);


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine extract step size of piezzo"
				     "using the periodicity of the saw tooth signal");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("cannot find data");
	nf = dsi->nx;	/* this is the number of points in the data set */

	i = win_scanf("Indicate the nb of the data set of objective move %4d\n"
		      "the period of the saw tooth %8d\n"
		      "the number of period to analyse %8d\n",&n_dso,&per,&n_per);
	if (i == CANCEL)	return OFF;

	nf = n_per*per;
	dso = op->dat[n_dso];
	if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	dstmp = create_and_attach_one_ds(op, n_per,n_per,0);
	if (dstmp == NULL) return win_printf_OK("cannot create tmp ds !");


	for (k = 0; k < per-1; k++)
	  {
	    for (j = 0; j < n_per; j++)
	      {
		dstmp->yd[j] = dsi->yd[k+j*per];
		dstmp->xd[j] = dsi->xd[k+j*per];
	      }
	    for (j = 0; j < nf; j++)
	      ds->xd[j] = interpolate_point_by_poly_2_in_array(dstmp->xd, dstmp->yd, n_per, dsi->xd[j]);
	    for (j = 0; j < n_per; j++)
	      ds->yd[k+(j*per)] = dsi->yd[1+k+(j*per)] - ds->xd[1+k+(j*per)];
	  }
	for (k = 0; k < nf-1; k++)
	  {
	    ds->xd[k] = dso->yd[k+1]-dso->yd[k];
	  }
	ds->xd[nf-1] = 0;
	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement,"y multiplied by");
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

int do_calpiezzo_rescale_plot(void)
{
	register int i, j;
	O_p *op = NULL, *opn = NULL;
	int nf;
	static float factor = 1;
	d_s *ds, *dsi;
	pltreg *pr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the y coordinate"
	"of a data set by a number and place it in a new plot");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("cannot find data");
	nf = dsi->nx;	/* this is the number of points in the data set */
	i = win_scanf("By what factor do you want to multiply y %f",&factor);
	if (i == CANCEL)	return OFF;

	if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds = opn->dat[0];

	for (j = 0; j < nf; j++)
	{
		ds->yd[j] = factor * dsi->yd[j];
		ds->xd[j] = dsi->xd[j];
	}

	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement,"y multiplied by");
	set_plot_title(opn, "Multiply by %f",factor);
	if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
	if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
	opn->filename = Transfer_filename(op->filename);
	uns_op_2_op(opn, op);
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

MENU *calpiezzo_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"data set rescale in Y", do_calpiezzo_compute_dz,NULL,0,NULL);
	//	add_item_to_menu(mn,"plot rescale in Y", do_calpiezzo_rescale_plot,NULL,0,NULL);
	return mn;
}

int	calpiezzo_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "calpiezzo", NULL, calpiezzo_plot_menu(), 0, NULL);
	return D_O_K;
}

int	calpiezzo_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "calpiezzo", NULL, NULL);
	return D_O_K;
}
#endif

