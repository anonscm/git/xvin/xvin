#ifndef _CORDE_PEAKS_H_
#define _CORDE_PEAKS_H_

// functions accessible from the menu:
PXV_FUNC(int, do_find_peak_and_width,		(void));
PXV_FUNC(int, do_corde_peaks_set_parameters, (void));

PXV_FUNC(int, do_corde_peaks_get_value,     (void));


// internal functions :
int ds_find_peak     (d_s *ds, int start, int end,                        float *x_peak, float *y_peak);
int ds_find_peak_info(d_s *ds, int start, int end, int domain_multiplier, float *x_peak, float *y_peak, float *width_min, float *width_max);



#endif

