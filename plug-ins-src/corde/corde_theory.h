#ifndef _CORDE_THEORY_H_
#define _CORDE_THEORY_H_

// functions using analytical derivations :
PXV_FUNC(int, do_graph_spectre_simple,		(void));
PXV_FUNC(int, do_graph_spectre_better,		(void));
PXV_FUNC(int, do_graph_dispersion_relation,	(void));
PXV_FUNC(int, do_graph_response_electric_ampli, (void));

PXV_FUNC(int, do_graph_spectre_reflexions,	(void));
PXV_FUNC(int, do_graph_response,	 	    (void));
PXV_FUNC(int, do_graph_response_2008,		(void));

#endif

