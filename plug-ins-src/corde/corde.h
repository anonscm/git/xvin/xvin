#ifndef _CORDE_H_
#define _CORDE_H_

#define CORDE_LOCAL               0x000000100
#define CORDE_GLOBAL              0x000001000
#define CORDE_POSITION            0x000010000
#define CORDE_POSITION_AND_WIDTH  0x000100000

extern float f, speed, L, A_0, a_d;
extern float T_0, raideur;
extern float C, R_a, b, a;
extern int   corde_what_is_x; // what is the x-unit of the current dataset ?

PXV_FUNC(MENU*, corde_plot_menu, 		(void));
PXV_FUNC(int, corde_main, 				(int argc, char **argv));
PXV_FUNC(int, corde_unload,				(int argc, char **argv));

// functions for corde in turbulence :
PXV_FUNC(int, do_graph_spectre_simple,		(void));
PXV_FUNC(int, do_graph_spectre_better,		(void));
PXV_FUNC(int, do_graph_spectre_reflexions,	(void));
PXV_FUNC(int, do_graph_response,		(void));
PXV_FUNC(int, do_corde_separate_modes,		(void));

void spit(char *message);

#endif

