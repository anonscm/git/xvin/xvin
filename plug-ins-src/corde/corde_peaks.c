#ifndef _CORDE_PEAKS_C_
#define _CORDE_PEAKS_C_

// #include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"
// #include <gsl/gsl_histogram.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_sf.h>
// #include <fftw3.h>

#include "corde.h"
#include "corde_peaks.h"

// global variables for this file:
double	PRECISION=1e-6;
int		spline_points=3;
int 		N_more=200;
int 		domain_multiplier=4;

// some default values for global variables:
#define domain_multiplier_default 4


/****************************************************************/
/* to find the first local maximum and minimum of a data set	*/
/* 	and the position of the half maxima							*/
/****************************************************************/
/* by default, domain_multiplier should be 4 (it worked)		*/
/* if peaks are very thin, this value can be increased			*/
/****************************************************************/
int ds_find_peak_info(d_s *ds, int start, int end, int domain_multiplier, float *x_peak, float *y_peak, float *width_min, float *width_max)
{
	register int i;
	float 	max_x, max_y, max_2;
	int		width_min_4, width_max_4;
	int 		nx, max_i;
	gsl_interp_accel 	*acc;
	gsl_spline			*spline;
	double				*x, *y, dx;
	double				epsilon, tmp_x, tmp_y /* l1, l2 */;
		
	nx = end - start;
	if (nx<5)	return(-1);

	max_i=ds_find_peak(ds, start, end, &max_x, &max_y); // last 2 parameters are pointers !
	if (max_i<0) return(win_printf_OK("couldn't find max... (when not even searching for width)\n->aborting"));
	
	*x_peak = max_x;
	*y_peak = max_y;
	
	max_2   = max_y/(double)(2.);  // demi-hauteur
	epsilon = max_y*PRECISION;

	//search for the width around the peak, in 2 steps:
	// first step, a larger estimate:
	i=max_i;
	while ( (i>=start) && ((double)ds->yd[i]>(max_y/(double)domain_multiplier)) )		i--;
	if (i>start) width_min_4=i;
	else		 return(-2); // we reach the limit...
	
	i=max_i;
	while ( (i<end) && ( (double)ds->yd[i] > (max_y/(double)domain_multiplier)) )		i++;
	if (i<end-1) width_max_4=i;
	else		 return(-3); // we reach the limit...
	
	// second step, a fit in a large region [width_min_4, width_max_4] defined by the domain multiplier \\lambda
	// such that for all x in [width_min_4, width_max_4] we have y(x)> (y_max/domain_multiplier)
	nx = width_max_4 - width_min_4;
	if (nx<4)	return(-10); // not enough resolution
	
	x      = (double*)calloc(nx, sizeof(double));
	y      = (double*)calloc(nx, sizeof(double));
	for (i=0; i<nx; i++)
	{	x[i] = (double)ds->xd[i+width_min_4];
		y[i] = (double)ds->yd[i+width_min_4];
	}
	acc    = gsl_interp_accel_alloc();
	spline = gsl_spline_alloc(gsl_interp_cspline, nx);
	gsl_spline_init(spline, x, y, nx);

/* on 2007/06/11, following method is commented out : it was assuming monotony of the splined function, which may not be correct!*/	
/*	
	// before the peak (x<x_max), we search for the point at which y = y_max/2:
	l1 = x[0];
	l2 = max_x; 
	if (l1>=l2) return(-7);
	tmp_x = (l1+l2)/(double)2.;
	tmp_y = gsl_spline_eval(spline, tmp_x, acc);
	while ( fabs(tmp_y-max_2)>epsilon ) 
	{	if (tmp_y > max_2)	l2 = tmp_x;
		else				l1 = tmp_x;
		tmp_x = (l1+l2)/(double)2.;
		tmp_y = gsl_spline_eval(spline, tmp_x, acc);
	}
	*width_min = (float)tmp_x;
	

	l1 = max_x;
	l2 = ds->xd[width_max_4];
	tmp_x = (l1+l2)/(double)2.;
	tmp_y = gsl_spline_eval(spline, tmp_x, acc);
	while ( fabs(tmp_y-max_2)>epsilon ) 
	{	if (tmp_y > max_2)	l1 = tmp_x;
		else				l2 = tmp_x;
		tmp_x = (l1+l2)/(double)2.;
		tmp_y = gsl_spline_eval(spline, tmp_x, acc);
	}
	*width_max = (float)tmp_x;
*/	

	dx = (double)ds->xd[max_i] - (double)ds->xd[max_i-1]; // local dx=df
	
	// before the peak (x<x_max), we search for the point at which y = y_max/2:
	tmp_x = max_x;
	tmp_y = gsl_spline_eval(spline, tmp_x, acc);
	i=0;
	while ( (tmp_y>max_2) && (i<N_more) ) 
	{	tmp_x -= dx/N_more;
		tmp_y  = gsl_spline_eval(spline, tmp_x, acc);
	}
	if (i<N_more)	*width_min = (float)tmp_x;
	else 	return(-17);

	// after the peak (x>x_max), we search for the point at which y = y_max/2:
	tmp_x = max_x;
	tmp_y = gsl_spline_eval(spline, tmp_x, acc);
	i=0;
	while ( (tmp_y>max_2) && (i<N_more) ) 
	{	tmp_x += dx/N_more;
		tmp_y  = gsl_spline_eval(spline, tmp_x, acc);
	}
	if (i<N_more)	*width_max = (float)tmp_x;
	else 	return(-19);
	
	
	gsl_spline_free(spline);
	gsl_interp_accel_free(acc);
	free(x); free(y);
	
	return(0);
} /* end of the "ds_find_peak_info" function */



/*****************************************************************/
/* to find the first local maximum and minimum of a data set	*/
/*                                                               */
/* returns the index closest to max (i_max) on success			*/
/* returns a negative value on error						*/
/*****************************************************************/
int ds_find_peak(d_s *ds, int start, int end, float *x_peak, float *y_peak)
{
	register int i;
	double 		max_x, max_y;
	int 			nx, max_i = start;
	gsl_interp_accel	*acc;
	gsl_spline		*spline;
	double			*x, *y, dx;
	double			tmp_x, tmp_y;
		
	nx = end - start; // nb of points in the window
	if (nx<3)	return(-1);

	// search for the max:
	max_x = (double)ds->xd[start];
	max_y = (double)ds->yd[start];
	max_i = start;
	for (i=start; i<end; i++)
	{	if ((double)ds->yd[i] >= max_y) 
		{ max_y = (double)ds->yd[i];
		  max_x = (double)ds->xd[i]; 
		  max_i = i;
		}
	}

	if ( ((end-max_i)<(spline_points/2+2)) || ( (max_i-start)< (spline_points/2+2)) )  // then max is too close from a boundary
	return(-1);
	
	nx = spline_points;
		
	x      = (double*)calloc(nx+1, sizeof(double));
	y      = (double*)calloc(nx+1, sizeof(double));
	for (i=0; i<nx; i++)
	{	x[i] = (double)ds->xd[max_i - spline_points/2 +i];
		y[i] = (double)ds->yd[max_i - spline_points/2 +i];
	}
	acc    = gsl_interp_accel_alloc();
	spline = gsl_spline_alloc(gsl_interp_cspline, nx);
	gsl_spline_init(spline, x, y, nx);
	
	dx = (double)ds->xd[max_i] - (double)ds->xd[max_i-1]; // local dx=df
	for (i=0; i<N_more; i++)
	{	tmp_x = (double)ds->xd[max_i-1] + (dx*(double)i/((double)N_more/(double)2.));
		tmp_y = gsl_spline_eval(spline, tmp_x, acc);
		if (tmp_y > max_y) 
		{	max_y = tmp_y;
			max_x = tmp_x;
		}
	}
	
	gsl_spline_free(spline);
	gsl_interp_accel_free(acc);
	free(x); free(y);
	
	*x_peak = max_x;
	*y_peak = max_y;
	return(max_i);
} /* end of the "ds_find_peak" function */







/****************************************************************/
/* to find peaks, and their width								*/
/****************************************************************/
int do_find_peak_and_width(void)
{
	int		i, j, ret=0, index;
	pltreg 	*pr;
	O_p 	*op, *opd=NULL;
	d_s 	*ds, *dsd=NULL; 
	int 	N, ny, i_min, i_max;
	float	x_min, x_max, dx;
	int		n_h=1, n_h_max, h, *h_index=NULL;
	float	x_peak, y_peak, width_min, width_max;
static float f_0=-1.;
static int	bool_fondamental_type=1, h_start=1, bool_look_width=1;
static char	h_string[128]="1:10";
	int		domain_multiplier=4, start, end;
	char 	message_to_spit[128]="searching...", *message=NULL;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
		
	if (key[KEY_LSHIFT]) return(win_printf_OK("This routine gives the maximum of \n"
				"the visible points of the active dataset.\n"
				"it also gives the width of half-width\n\n"
				"if you choose to work on all harmonics, a new plot is created"));
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return(win_printf_OK("I cannot find data"));
	
	/* ci-dessous, on cherche quels sont les points dans la zone du plot affichee : */
	x_min = op->x_lo; // region from the active x-axis limits
	x_max = op->x_hi;
	ny    = ds->ny;
	N=0; // number of points to work on
	for (i=0; i<ny; i++)  if ( (ds->xd[i]>=x_min) && (ds->xd[i]<=x_max) ) N++;
	i_min=0; 	while (ds->xd[i_min]<x_min) 	i_min++;
	i_max=ny-1; while (ds->xd[i_max]>x_max)		i_max = i_max-1; 
	dx    = (ds->xd[i_min+1] - ds->xd[i_min]);
	
	while ( (ret=ds_find_peak_info(ds, i_min, i_max, domain_multiplier, &x_peak, &y_peak, &width_min, &width_max))<0)
	{	switch (ret) 
		{	case -1 : return(win_printf_OK("error during peak examination.\n"
							"not enough points in the window\n"
							"try to enlarge it..."));
					break;
			case -10: sprintf(message_to_spit,"error\n"
							"not enough points in the sub-window\n"
							"give a number larger than 4 (domain multiplier): %%d\n"
							"or click CANCEL");
			          if (win_scanf(message_to_spit, &domain_multiplier)==CANCEL) return(D_O_K);
			          break;
			case -3:  return(win_printf_OK("error 3. I abort"));
					break;	
			default:  return(win_printf_OK("unknown error... I abort"));
					break;
        }
	}
	
	if (index&CORDE_LOCAL)
	return(win_printf_OK("For visible points of active dataset\n"
			"max is at {\\color{lightred}x=%g} and it is y=%g\n"
			"half with is beetween x=%g and x=%g\n"
			"so {\\color{lightred}\\Delta x_{1/2} = %g}\n"
			"and Q = \\frac{x_{max}}{\\Delta x_{1/2}} = %g",
			x_peak, y_peak, width_min, width_max, (width_max-width_min), x_peak/(width_max-width_min) ));
		
	if (f_0<0.) // first use of the function
		f_0 = x_peak;
	
	message=my_sprintf(message, "{\\color{yellow}\\pt14 Harmonics examination}\n\n"
		"%%R examine position and height only\n"
		"%%r examine position, height and width (a good spectral resolution is required!)\n"
		"I need to know where is the first harmonic (fondamental)\n"
		"%%R (1) use the fondamental frequency (impose it)\n"
		"%%r (2) use the harmonic number in the visible window (detected at {\\color{Lightgreen}%7.2f})\n\n"
		"fondamental is %%8f Hz (used only if choice 1 above)\n"
		"the visible window contains harmonic number %%4d (used only if choice 2 above)\n\n"
		"examine harmonics %%20s", 
		x_peak);
	i=win_scanf(message, &bool_look_width, &bool_fondamental_type, &f_0, &h_start, &h_string);
	if (i==CANCEL) return(D_O_K);
	h_index = str_to_index(h_string, &n_h);	// malloc is done by str_to_index
	if ( (h_index==NULL) || (n_h<1) )	return(win_printf_OK("bad values for harmonics !"));
		
	if (bool_fondamental_type==0)
	{	h_start = (int)(x_peak/f_0); }
	else
	{	f_0     = x_peak/(float)h_start; }
	
	n_h_max = (int)floor( (double)ds->xd[ny-1]/f_0 - (double)(x_max-x_min)/f_0);
	if (n_h>n_h_max) 					
	{	sprintf(h_string, "1:%d", n_h_max);
		i=win_printf("larger possible harmonic is %d\n"
					"I suggest {\\color{yellow}%s}\n\n"
					"click OK to accept or CANCEL to abort", n_h_max, h_string);
		if (i==CANCEL) return(D_O_K);
		h_index = str_to_index(h_string, &n_h);	// malloc is done by str_to_index
		if ( (h_index==NULL) || (n_h<1) )	return(win_printf_OK("bad values for harmonics !"));
	}
	
	if ((opd = create_and_attach_one_plot(pr, n_h, n_h, 0)) == NULL)
		return(win_printf_OK("cannot create plot !")); // first dataset is for peak position
	inherit_from_ds_to_ds(opd->dat[0], ds);
	opd->dat[0]->treatement = my_sprintf(opd->dat[0]->treatement, "peaks position (Hz)");
	set_ds_dot_line(opd->dat[0]);	set_ds_point_symbol(opd->dat[0], "\\pt6\\oc");
	
	if ((dsd = create_and_attach_one_ds (opd, n_h, n_h, 0)) ==NULL)
		return(win_printf_OK("cannot create dataset !")); // third dataset is for peak height
	inherit_from_ds_to_ds(dsd, ds);
	dsd->treatement = my_sprintf(dsd->treatement, "peaks height (V/\\sqrt{Hz})");
	set_ds_dot_line(dsd);	set_ds_point_symbol(dsd, "\\pt6\\oc");
	
	if (bool_look_width==1) // then 2 additional datasets
	{
	if ((dsd = create_and_attach_one_ds (opd, n_h, n_h, 0)) ==NULL)
		return win_printf_OK("cannot create dataset !"); // second dataset is for peak width
	inherit_from_ds_to_ds(dsd, ds);
	dsd->treatement = my_sprintf(dsd->treatement, "peaks width (Hz)");
	set_ds_dot_line(dsd);	set_ds_point_symbol(dsd, "\\pt6\\oc");

	if ((dsd = create_and_attach_one_ds (opd, n_h, n_h, 0)) ==NULL)
		return win_printf_OK("cannot create dataset !"); // fourth dataset is for "facteur de qualite"
	inherit_from_ds_to_ds(dsd, ds);
	dsd->treatement = my_sprintf(dsd->treatement, "facteur de qualite (V/\\sqrt{Hz})");
	set_ds_dot_line(dsd);	set_ds_point_symbol(dsd, "\\pt6\\oc");
	}
	
	for (i=0; i<n_h; i++)
	{	h=h_index[i];
		opd->dat[0]->xd[i] = (float)h;			opd->dat[0]->yd[i] = 0.;
		opd->dat[1]->xd[i] = (float)h;			opd->dat[1]->yd[i] = 0.;
		if (bool_look_width==1)
		{ opd->dat[2]->xd[i] = (float)h;		opd->dat[2]->yd[i] = 0.;
		  opd->dat[3]->xd[i] = (float)h;		opd->dat[3]->yd[i] = 0.;
		}
	
		sprintf(message_to_spit, "examining harmonic %d/%d at %d f_0", i, n_h, h); spit(message_to_spit);
		
		start = i_min + (int)((float)f_0/dx*(float)(h-h_start));
		end   = i_max + (int)((float)f_0/dx*(h-h_start));
			 
		if (bool_look_width==1)
				ret=ds_find_peak_info(ds, start, end, domain_multiplier, &x_peak, &y_peak, &width_min, &width_max);
		else 	ret=ds_find_peak     (ds, start, end,  				  &x_peak, &y_peak);
		if (ret<0)
		{	message=my_sprintf(message, "error during examination of harmonic number %d.\n"
							"compact interval [%d; %d] pts, [%6.2f; %6.2f] Hz\n",
							h, start, end, start*dx, end*dx);
			switch (ret) 
			{	case -1 : message=my_sprintf(message, "not enough points in the window\n\n");
						break;
				case -10: message=my_sprintf(message, "domain multiplier was %d.\n\n"
							"try to search for position and height, but not for width...\n\n",
							domain_multiplier);
			          	break;
				default:  message=my_sprintf(message, "unknown error\n\n");
						break;
			}	
			message=my_sprintf(message, "click OK to go on, or CANCEL to exit");
			j = win_printf(message); free(message); message=NULL;
			if (j==CANCEL) return(D_O_K);
		}
		else 
		{	if ( (ret==start) || (ret==end) || (ret==(end-1)) )
			{	win_printf("{\\color{green}\\pt14 Warning!}\n"
					"max was found close to point of index %d\n"
					"when searching in (index-)interval [%d %d]\n\n"
					"this is strange... enlarge the window please!", ret, start, end);
			}
			
			opd->dat[0]->yd[i] = x_peak;
			opd->dat[1]->yd[i] = y_peak;
			
			if (bool_look_width==1)
			{	opd->dat[2]->yd[i] = width_max - width_min;
				if (width_max - width_min==0) opd->dat[3]->yd[i] = 0;
				else opd->dat[3]->yd[i] = x_peak/(width_max - width_min) ;
			}
		}
	}
	
	set_plot_title(opd,"\\stack{{peaks position (Hz), height (a.u.)}%s{%s}}", (bool_look_width==1) ? ", {width (Hz) and Q}": "", op->title);
	set_plot_x_title(opd, "harmonic number (f_0=%4.2f Hz)", f_0	);
	set_plot_y_title(opd, "(Hz), (a.u.)");
	opd->filename = Transfer_filename(op->filename);
     opd->dir = Mystrdup(op->dir);

     free(h_index);
	return(refresh_plot(pr, UNCHANGED));
} /* end of the "do_find_peak_and_width" function */





/****************************************************************/
/* to set fine parameters that are used to find peaks,          */
/* and their width                                              */
/* 2007/07                                                      */
/****************************************************************/
int do_corde_peaks_set_parameters(void)
{	d_s  *ds;
    char *message=NULL;
	int   bool_spline_points=0;
	float df, fs;
	
	bool_spline_points=(spline_points-3)/2;
	
	if(updating_menu_state != 0)	return D_O_K;	
		
	if (key[KEY_LSHIFT]) return(win_printf_OK("This routine sets fine parameters used to find peaks"));
	
	message = my_sprintf(message, "{\\pt14\\color{yellow}How to find peaks... parameters}\n\n"
							"number of data points to use around the peak : %%R 3, %%r 5 or %%r 7 (was %d)\n", 
							"number of new points to create around the peak N_{more} = %%5d (was %d)\n\n"
							"precision (only to search for width) %%7f (was %7f)\n"
							"domain multiplier \\lambda (only to search for width) %%5d (was %5d, default is %5f)\n"
							"(\\lambda defines a search region of x such that y(x) > (max/\\lambda) )",
							spline_points, N_more, PRECISION, domain_multiplier, domain_multiplier_default);
	
	if (ac_grep(cur_ac_reg,"%ds",&ds) != 1)	return(win_printf_OK("I cannot find data"));
	fs = find_sampling_frequency_dialog(ds->xd, ds->nx);
	
	if (fs>0) // we detected a correct sampling frequency...
	{	df = (float)1./fs;
		message=my_sprintf(message, "\nfor the current dataset, \\Delta f = %7f Hz\n"
								"so peak will be detected with accuracy \\Delta f / N_{more} = %8f Hz\n", df, df/N_more); 
	}
	
	if (win_scanf(message, &bool_spline_points, &N_more, &PRECISION, &domain_multiplier)==CANCEL) return(D_O_K);
	spline_points = 3 + bool_spline_points*2;
	
	free(message); message=NULL;
	return(D_O_K);
}






/****************************************************************/
/* to find value of a dataset at peaks                          */
/****************************************************************/
int do_corde_peaks_get_value(void)
{
	int		i, i_peak, index;
	pltreg 	*pr;
	O_p 	*op;
	d_s 	*ds, *dsd=NULL; 
	int 	N, ny;
	float	dx;
	float	x_peak;
static int	bool_use_X_or_Y=1, nds=0;
	char 	*message=NULL;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
		
	if (key[KEY_LSHIFT]) return(win_printf_OK("This routine gives the values of the active dataset\n"
				"matching the abscissae of another dataset (e.g. obtained from ""find peaks"")\n\n"
                "a new plot is created"));
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return(win_printf_OK("I cannot find data"));
	
	ny  = ds->ny;
	dx  = ds->xd[1]-ds->xd[0]; if (dx==0) return(win_printf_OK("dx is 0 ??"));
	
	message=my_sprintf(message, "I will extract values of dataset %d \n"
                                "at abscissae given by %%R X (abscissae) %%r Y (ordonnae)\n"
								"of dataset %%d", op->cur_dat); 
	if (win_scanf(message, &bool_use_X_or_Y, &nds) == CANCEL) return(D_O_K);
	if (nds >= op->n_dat)   return(win_printf_OK("dataset number for index is too large!"));
	if (nds == op->cur_dat) return(win_printf_OK("dataset number for index is the same!"));
	
	N=op->dat[nds]->nx; // number of points to work on
	if ((dsd = create_and_attach_one_ds (op, N, N, 0)) ==NULL)
		return(win_printf_OK("cannot create dataset !")); // third dataset is for peak height
	inherit_from_ds_to_ds(dsd, ds);
	dsd->treatement = my_sprintf(dsd->treatement, "value at peaks");
	set_ds_dot_line(dsd);	set_ds_point_symbol(dsd, "\\pt6\\oc");

	for (i=0; i<N; i++)  
    {   x_peak = (bool_use_X_or_Y==0) ? op->dat[nds]->xd[i] : op->dat[nds]->yd[i];
        dsd->xd[i] = x_peak;
        
        i_peak = 0;
        while (ds->xd[i_peak]<x_peak) i_peak++;
        
        dsd->yd[i] = ds->yd[i_peak];
    }
	
	return(refresh_plot(pr, UNCHANGED));
} /* end of the "do_corde_peaks_get_value" function */





#endif
