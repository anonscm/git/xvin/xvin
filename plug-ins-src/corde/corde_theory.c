#ifndef _CORDE_THEORY_C_
#define _CORDE_THEORY_C_

#include <complex.h>

#include "allegro.h"
#include "xvin.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "corde.h"
#include "corde_theory.h"


int do_graph_spectre_simple(void)
{	int	 n;
	register int 	i;
	O_p 	*op = NULL;
	d_s 	*ds0, *ds1;
	pltreg	*pr = NULL;
	double	omega_0, omega, x;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plots the expected spectrum of a string\n"
					"This is the simplest model with some dissipation.\n\n"
					"A new dataset is created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow}String spectrum - Parameters}\n\n"
			"dissipation f = %8f Hz\n"
			"sound celerity c = %8f m/s\n"
			"string length L = %8f m\n"
			"normalisation A_0 = %8f (a.u.)\n\n"
            "abcissa of current ds is %R frequency, %r nondimensional",
			&f, &speed, &L, &A_0, &corde_what_is_x);
	if (i==CANCEL) return(D_O_K);
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds0) != 3)		return win_printf_OK("cannot plot region");
	n=ds0->nx;
	
	omega_0 = (double)M_PI*(double)speed/(double)L;
	
	if ((ds1 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	for (i=0; i<n; i++)
	{	ds1->xd[i] = ds0->xd[i];
	    if (corde_what_is_x==0) // frequencies
        	omega = (double)ds0->xd[i]*2.*M_PI; // pulsation
    	else // non dimensional 
            omega = (double)ds0->xd[i]*omega_0;
		x	   	  = omega*(double)L/(double)speed;
		
		ds1->yd[i] = (float)(A_0/(cos(x)*cos(x)*f*f*L*L/(speed*speed)+4*sin(x)*sin(x)));			
	}
	ds1->treatement = my_sprintf(ds1->treatement,"spectre avec f=%8f, c=%8f and L=%8f", 
						(float)(f), (float)(speed), (float)L);
	inherit_from_ds_to_ds(ds1, ds0);
	
	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_graph_spectre_simple' */





int do_graph_spectre_better(void)
{	int	 n;
	register int 	i;
	O_p 	*op = NULL;
	d_s 	*ds0, *ds1;
	pltreg	*pr = NULL;
	double	omega, omega_0, omega_a, q;
static int bool_output_impedance=1;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plots the expected spectrum of a string\n"
					"it takes into account possible reflexions on the boundaries\n\n"
					"This is the electrical response function (U -> U) !\n\n"
					"A new dataset is created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow}String spectrum - Parameters}\n\n"
			"dissipation f = %8f Hz\n"
			"sound celerity c = %8f m/s\n"
			"string length L = %8f m\n"
			"normalisation A_0 = %8f (a.u.)\n\n"
			"{\\pt14\\color{yellow}Reflexions parameters}\n\n"
			"Tension = %8f N\n"
			"raideur d'un piezo %8f N/m\n\n"
			"%b correct by effect of amplificator impedance (output piezo)\n"
			"piezo capacitance C = %8f F\n"
			"amplificator input impedance R_{a} = %8f \\Omega\n\n"
            "abcissa of current ds is %R frequency, %r nondimensional",
			&f, &speed, &L, &A_0, &T_0, &raideur, &bool_output_impedance, &C, &R_a, &corde_what_is_x);
	if (i==CANCEL) return(D_O_K);
	
	omega_0 = (double)M_PI*(double)speed/(double)L;
	omega_a = (double)1./(R_a*(double)(C));
	if (omega_a==0) return(win_printf_OK("\\omega_a = 1/R_{a}C is zero !"));
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds0) != 3)		return win_printf_OK("cannot plot region");
	n=ds0->nx;
	
	if ((ds1 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	for (i=0; i<n; i++)
	{	ds1->xd[i] = ds0->xd[i];
    	if (corde_what_is_x==0) // frequencies
        	omega = (double)ds0->xd[i]*2.*M_PI; // pulsation
    	else // non dimensional 
            omega = (double)ds0->xd[i]*omega_0;
		q          = sqrt(omega*omega + (double)f*f/(double)4.) / speed;
		
		ds1->yd[i] = A_0*(float)(q/fabs(sin(q*L)+q*T_0*cos(q*L)/raideur));
		
		if (bool_output_impedance==1)
		{ 	ds1->yd[i] *= (float)((omega*raideur)/sqrt(1.+omega*omega/(omega_a*omega_a)));
		}
	}
	ds1->treatement = my_sprintf(ds1->treatement,"spectre avec f=%8f, c=%8f and L=%8f et des reflexions...", 
						(float)(f), (float)(speed), (float)L);
	inherit_from_ds_to_ds(ds1, ds0);
	
	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_graph_spectre_better' */



// initally, it was omega vs q
// on 2007/12/15, i changed it into q vs omega, which allowed simple plotting of dispersion
int do_graph_dispersion_relation(void)
{	int	 n;
	register int 	i;
	O_p 	*op = NULL;
	d_s 	*ds0, *ds1;
	pltreg	*pr = NULL;
	double	omega_2, q=1.;
static int corde_what_is_x;
    double B=2.6e-5;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plots the dispersion relation of a string\n"
					"ie, f={\\frac{\\omega}{2\\pi}}) vs q \n\n"
					"A new dataset is created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow}String dispersion relation - Parameters}\n\n"
			"dissipation f = %8f Hz\n"
			"sound celerity c = %8f m/s\n"
			"string length L = %8f m\n"
            "dispersion ( ac^2\\partial^{4} u /\\partial x^4 ), a=%8f\n\n"
            "x-unit are %R frequency, %r nondimensional, %r wavenumber",
			&f, &speed, &L, &a_d, &corde_what_is_x);
	if (i==CANCEL) return(D_O_K);
	if (corde_what_is_x==0) return(win_printf_OK("I plot \\omega (q) and not the opposite..."));
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds0) != 3)		return win_printf_OK("cannot plot region");
	n=ds0->nx;
	
	if ((ds1 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	for (i=0; i<n; i++)
	{	ds1->xd[i] = ds0->xd[i];
	
	    if (corde_what_is_x==2) // on current plot, x axis are wavenumbers
	    { q     = (double)ds1->xd[i];
        }
        else if (corde_what_is_x==0)       // on current plot, x axis are frequencies
		{ }
        else /* if (corde_what_is_x==1) */    // on current plot, x axis are non-dimensionalized (f/f_0 or k/k_0)
        { q     = (double)ds1->xd[i]*M_PI/L;
        } 
		
		omega_2    = (double)speed*speed*q*q*(1. + (double)a_d*q*q) - (double)f*f/(double)4.;
		
		ds1->yd[i] = (float)sqrt(omega_2) / (2.*M_PI);
		
	}
	ds1->treatement = my_sprintf(ds1->treatement,"dispersion relation with f=%8f, c=%8f and L=%8f, dispersion a=%8f", 
						(float)(f), (float)(speed), (float)L, (float)a_d);
	inherit_from_ds_to_ds(ds1, ds0);
	
	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_graph_dispersion_relation' */








/* d'apres les calculs du 19/07/2006 */
int do_graph_response_electric_ampli(void)
{	int	 n;
	register int 	i;
	O_p 	*op = NULL;
	d_s 	*ds0, *ds1, *ds2=NULL;
	pltreg	*pr = NULL;
static int bool_consider_damping=1, bool_plot_inverse=0, bool_plot_qT=0;
	double	ab, omega, omega_a, q, epsilon, Ha_re, Ha_im, Ha_mod2, sigma;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plots the modulus of the response function H_a\n"
					"that describes the electrical coupling between the output piezo\n"
					"and the amplificator (SR640) input impedance\n\n"
					"A new dataset is created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow}Output Amplificator Response H_a}\n\n"
            "{\\pt14\\color{lightgreen}               "
            "H_a = abR_aT_0q\\frac{i\\omega+\\sigma}{1+i\\frac{\\omega}{\\omega_a}}}\n\n\n"
            "{\\pt14\\color{yellow}Mecanical parameters}\n\n"
			"Tension = %8f N\n"
			"raideur d'un piezo %8f N/m\n\n"
			"{\\pt14\\color{yellow}Electric boundary conditions}\n\n"
			"coefficients of piezo a = %8f u.a.\n"
			"                       b = %8f m/V\n"
			"piezo capacitance C = %8f F\n"
			"amplificator input impedance R_{a} = %8f \\Omega\n\n"
            "%b consider damping (\\sigma)\n"
            "%b plot inverse response function 1/H_a instead of H_a\n"
            "%b plot also (qT_0) together with H_a (a second dataset will be created)\n\n"
            "x-unit of current dataset is %R frequency, %r nondimensional, %r wavenumber",
			&T_0, &raideur, &a, &b, &C, &R_a, &bool_consider_damping, &bool_plot_inverse, &bool_plot_qT, &corde_what_is_x);
	if (i==CANCEL) return(D_O_K);
	ab=a*b;
	if (a*b==0) return(win_printf_OK("a*b is zero. This is a problem to me..."));
	
	omega_a = (double)1./(R_a*(double)(C)); // pulsation = 2\\pi*frequency !!!
	if (omega_a==0) return(win_printf_OK("\\omega_a = 1/R_{a}C is zero !"));
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds0) != 3)		return win_printf_OK("cannot plot region");
	n=ds0->nx;
	
	if (bool_consider_damping==1)	sigma = (double)-f/2.;
	else							sigma = (double)0.;
	
	if ((ds1 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	if (bool_plot_qT==1) 	
    if ((ds2 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
    
	for (i=0; i<n; i++)
	{	ds1->xd[i] = ds0->xd[i]; // frequency
		
        if (corde_what_is_x==2) // on current plot, x axis are wavenumbers
	    { q     = (double)ds1->xd[i];
	      omega = sqrt(q*q*speed*speed - (double)f*f/(double)4.);
        }
        else if (corde_what_is_x==0)       // on current plot, x axis are frequencies
		{ omega = (double)ds1->xd[i]*2.*M_PI; // pulsation 
		  q     = sqrt(omega*omega + (double)f*f/(double)4.) / speed;
        }
        else /* if (corde_what_is_x==1) */    // on current plot, x axis are non-dimensionalized (f/f_0 or k/k_0)
        { q     = (double)ds1->xd[i]*M_PI/L;
          omega = sqrt(q*q*speed*speed - (double)f*f/(double)4.);
	    } 
		
		epsilon    = (double)q*(double)T_0/(double)raideur;		
				
		// amplificator response function :
		Ha_re      = ab*R_a*T_0*q*(sigma + omega*omega/omega_a) / ((double)1.+omega*omega/(omega_a*omega_a));  // cahier VIbis, p99
		Ha_im      = ab*R_a*T_0*q*omega*(1. - sigma/omega_a)    / ((double)1.+omega*omega/(omega_a*omega_a));
		Ha_mod2    = Ha_re*Ha_re + Ha_im*Ha_im;
			
		if (bool_plot_inverse==1) ds1->yd[i] = (float)1./(float)sqrt(Ha_mod2);
		else                      ds1->yd[i] = (float)sqrt(Ha_mod2);
		
		if (bool_plot_qT==1)
		{  ds2->xd[i] = ds0->xd[i];
  		   ds2->yd[i] = (float)q*T_0;
        }
	}
	inherit_from_ds_to_ds(ds1, ds0);
    if (bool_plot_inverse==1) ds1->treatement = my_sprintf(ds1->treatement,"1/|H_a|");
	else                      ds1->treatement = my_sprintf(ds1->treatement,"|H_a|");

	if (bool_plot_qT==1)
	{  inherit_from_ds_to_ds(ds2, ds0);
       ds2->treatement = my_sprintf(ds2->treatement,"qT_0");
    }

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_graph_response_electric_ampli' */




/****************************************************/
/* d'apres les calculs du 25/07/2006                */
/* corriges le 19/02/2007                           */
/* convention (sigma x) ajoutee le 27/06/2007       */
/****************************************************/
int do_graph_response(void)
{	int	    n, nm;
	register int 	i, j;
	O_p 	*op = NULL;
	d_s 	*ds0, *ds1, *ds_TFD=NULL, *ds_real=NULL, *ds_imag=NULL;
	pltreg	*pr = NULL;
static int 	bool_elec_meca=0, /* 0 for elec, and 1 for meca */
            bool_increase_resolution=0,
			bool_plot_TFD=0, bool_plot_real=0, bool_plot_imag=0,
			bool_convention=0,
            n_more=8; // add more point to increase resolution (peaks height)
	double	ab, omega, omega_a, q, epsilon, epsilon_p, 
            Ha_re, Ha_im, Ha_mod2, H_re_inv=0., H_im_inv=0., sigma;
    double  dx, tmp;
    double complex  mu, eta, Ha_x, D, q_p, tmp_c;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plots the expected response function of a string\n"
					"it takes into account possible reflexions on the boundaries\n"
					"new datasets are created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow}String Response Function - Parameters}\n\n"
			"dissipation f = %8f Hz\n"
			"sound celerity c = %8f m/s\n"
			"string length L = %8f m\n"
			"{\\pt14\\color{yellow}Reflexions parameters}\n\n"
			"Tension T_0 = %8f N\n"
			"spring constant of a piezo k %8f N/m\n\n"
			"{\\pt14\\color{yellow}Electric boundary conditions}\n\n"
			"coefficients of piezo a = %8f u.a. (=b!)\n"
			"                           b = %8f m/V\n"
			"piezo capacitance C = %8f F\n"
			"amplificator input impedance R_{a} = %8f \\Omega\n\n"
			"use convention %R (\\sigma t) or %r (\\sigma x)\n"
			"%R plot electrical response\n"
			"%r plot mechanical response\n"
			"%r plot electrical response corrected by modulus of SR640 amplificator, and by qT_0\n"
			"current X-axis are %R frequencies (Hz) or %r adimensionalized or %r wavenumbers (m^{-1}) ?\n"
			"%b increase resolution by a factor %5d\n"
			"modulus will be plotted anyway.\n"
			"%b plot also Im(H)/\\omega\n"
			"%b plot also Re(1/H)\n"
			"%b plot also Im(1/H)",
			&f, &speed, &L, /* &A_0, */ &T_0, &raideur, &a, &b, &C, &R_a, 
			&bool_convention, &bool_elec_meca, &corde_what_is_x, &bool_increase_resolution, &n_more, 
            &bool_plot_TFD, &bool_plot_real, &bool_plot_imag);
	if (i==CANCEL) return(D_O_K);
	ab = (double)a*b;
	if ( (a*b==0) && ( (bool_plot_imag) || (bool_plot_TFD) ) )
	{	i=win_printf("a*b is zero.\nI cannot plot imaginary parts, there are vanishing !\n\n"
					"click OK to go on without ploting imaginary parts, or CANCEL to abort");
		if (i==CANCEL) return(D_O_K);
		bool_plot_TFD=0;
		bool_plot_imag=0;
	}
	if (bool_increase_resolution==1) 
	{ if (n_more<1) return(win_printf_OK("You choose to increase the computation resolution\n"
                                            "in order to better describe the peaks.\n\n"
                                            "but please choose n\\_more larger than 1!"));
    }

	omega_a = (double)1./((double)R_a*(double)C); // pulsation, not frequency !
	if (omega_a==0) return(win_printf_OK("\\omega_a = 1/R_{a}C is zero !"));
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds0) != 3)		return win_printf_OK("cannot plot region");
	n=ds0->nx;
	
	if (bool_increase_resolution==1)   nm=n_more;
    else                               nm=1;
    if ((ds1 = create_and_attach_one_ds(op, n*nm, n*nm, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	dx = (double)(ds0->xd[1] - ds0->xd[0]);
    
    
	if (bool_plot_TFD==1)
	{	if ((ds_TFD = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	}	
	if (bool_plot_real==1)
	{	if ((ds_real = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	}	
	if (bool_plot_imag==1)
	{	if ((ds_imag = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	}	

if (bool_convention==0) // convention with dissipation in time (2006)
{
	sigma = (double)-f/(double)2.;
	
	for (i=0; i<n*nm; i++)
	{	j = (i-(i%nm))/nm; // (j is integer part of (i/nm), but cleaner)
        ds1->xd[i] = ds0->xd[j] + (float)(i%nm)*dx/nm;
	    ds1->yd[i] = (float)0.;
	   
	    if (corde_what_is_x==2) // on current plot, x axis are wavenumbers
	    { q     = (double)ds1->xd[i];
	      omega = sqrt(q*q*speed*speed - (double)f*f/(double)4.);
        }
        else if (corde_what_is_x==0)       // on current plot, x axis are frequencies
		{ omega = (double)ds1->xd[i]*2.*M_PI; // pulsation 
		  q     = sqrt(omega*omega + (double)f*f/(double)4.) / speed;
        }
        else /* if (corde_what_is_x==1) */    // on current plot, x axis are non-dimensionalized (f/f_0 or k/k_0)
        { q     = (double)ds1->xd[i]*M_PI/L;
          omega = sqrt(q*q*speed*speed - (double)f*f/(double)4.);
	    } 
		epsilon    = (double)q*T_0/raideur;
		
		Ha_re      = ab*R_a*T_0*q*(sigma + omega*omega/omega_a) / ((double)1.+omega*omega/(omega_a*omega_a));  // cahier VIbis, p99
		Ha_im      = ab*R_a*T_0*q*omega*(1. - sigma/omega_a)    / ((double)1.+omega*omega/(omega_a*omega_a));
		Ha_mod2    = Ha_re*Ha_re + Ha_im*Ha_im;


      	if ((q*T_0)==0.) // then epsilon and Ha are also vanishing...
		{   H_re_inv = (double)1.; // artificial value to prefent infinity from division by zero !
		    H_im_inv = (double)1.;
        }
        else
        { if (bool_elec_meca==1) // mecanical response
		  {	tmp        = (Ha_re-epsilon)*(Ha_re-epsilon) + Ha_im*Ha_im;
            H_re_inv   = (double)(-sin(q*L) - cos(q*L)*(Ha_re - epsilon)/tmp ); // cahier VI, p210			
			H_im_inv   = (double)(          + cos(q*L)* Ha_im / tmp          );
			
		    H_re_inv  *= (double)(q*T_0);
			H_im_inv  *= (double)(q*T_0);
		  }
		  else if (bool_elec_meca==0) // electrical response
		  {	H_re_inv   = -(double)(1.+epsilon*epsilon)*sin(q*L) * ( Ha_re/Ha_mod2) // cahier VIbis, p99
						+ (cos(q*L)+epsilon*sin(q*L));
			H_im_inv   = -(double)(1.+epsilon*epsilon)*sin(q*L) * (-Ha_im/Ha_mod2);
		  }
		  else if (bool_elec_meca==2) // electrical response, corrected by modulus of ampli response
		  { H_re_inv   = -(double)(1.+epsilon*epsilon)*sin(q*L) * ( Ha_re/Ha_mod2) // cahier VIbis, p99
						+ (cos(q*L)+epsilon*sin(q*L));
			H_im_inv   = -(double)(1.+epsilon*epsilon)*sin(q*L) * (-Ha_im/Ha_mod2);
			
			H_re_inv  *= (sqrt(Ha_mod2)/(q*T_0));
			H_im_inv  *= (sqrt(Ha_mod2)/(q*T_0));
		  }
        }
      
         
		ds1->yd[i] = (float)fabs((double)1./sqrt(H_re_inv*H_re_inv + H_im_inv*H_im_inv));
		
		if ((i%nm)==0) // points of the original dataset, i/nm can be replaced by j
		{
		if (bool_plot_TFD==1) 
		{	ds_TFD->xd[i/nm] = ds0->xd[i];
			ds_TFD->yd[i/nm] = (float)fabs(-H_im_inv/(H_re_inv*H_re_inv + H_im_inv*H_im_inv) );
			if (omega!=0) 	ds_TFD->yd[i/nm]/= omega;
		}
		if (bool_plot_real==1) 
		{	ds_real->xd[i/nm]= ds0->xd[i];
			ds_real->yd[i/nm]= (float)H_re_inv;
		}
		if (bool_plot_imag==1) 
		{	ds_imag->xd[i/nm]= ds0->xd[i];
			ds_imag->yd[i/nm]= (float)H_im_inv;
		}
        } // end of n%nm case
	}
}
else // convention with dissipation in space
{   for (i=0; i<n*nm; i++)
	{	j = (i-(i%nm))/nm; // (j is integer part of (i/nm), but cleaner)
        ds1->xd[i] = ds0->xd[j] + (float)(i%nm)*dx/nm;
	    ds1->yd[i] = (float)0.;
	   
	    if (corde_what_is_x==2) // on current plot, x axis are wavenumbers
	    { q     = (double)ds1->xd[i];
	      omega = sqrt(q*q*speed*speed / ( (double)1. + (double)f*f/((double)4.*q*q*speed*speed)));
        }
        else if (corde_what_is_x==0)       // on current plot, x axis are frequencies
		{ omega = (double)ds1->xd[i]*2.*M_PI; // pulsation 
		  if (f<=omega) 
		  q     = sqrt(omega*omega/(double)2.*( (double)1. + sqrt( (double)1. - (double)f*f/((double)omega*omega) ) ));
		  else
		  q     =0.;
		  
		  q    /= speed;
        }
        else /* if (corde_what_is_x==1) */    // on current plot, x axis are non-dimensionalized (f/f_0 or k/k_0)
        { q     = (double)ds1->xd[i]*M_PI/L;
          omega = sqrt(q*q*speed*speed / ( (double)1. + (double)f*f/((double)4.*q*q*speed*speed)));
	    } 
		
        sigma      = (double)f*q/sqrt(4*q*q*speed*speed+f*f);
        epsilon    = (double)q*T_0/raideur;
	    epsilon_p  = (double)sigma*T_0/raideur;  // cahier VI,p191
	    
	    eta        = epsilon_p + I * epsilon;
	    Ha_x       = (ab*raideur/C)*(epsilon-I*epsilon_p)*(omega/omega_a)/((double)1. + I*omega/omega_a); // VIp203
	    mu         = eta + Ha_x;
	    q_p        = q - I*sigma;
	    D          =   ((double)1.-mu)*((double)1.+eta)*cexp(-I*q_p*L)	// VIp193
	                 - ((double)1.+mu)*((double)1.-eta)*cexp(+I*q_p*L);
	    
	    { if (bool_elec_meca==1) // mechanical response
		  {	tmp_c      = eta*raideur/mu/(double)2. * ( (1-mu)*cexp(-I*q_p*L) + (1+mu)*cexp(+I*q_p*L) );
			H_re_inv   = creal(tmp_c);
               H_im_inv   = cimag(tmp_c);
		  }
		  else if (bool_elec_meca==0) // electrical response
		  {	H_re_inv   = creal(-D/(Ha_x*(double)2.));
               H_im_inv   = cimag(-D/(Ha_x*(double)2.));
		  }
		  else if (bool_elec_meca==2) // electrical response, corrected by modulus of ampli response
		  {  H_re_inv   = creal(-D/(Ha_x*(double)2.));
               H_im_inv   = cimag(-D/(Ha_x*(double)2.));
               
               H_re_inv  *= (sqrt(cabs(Ha_x))/(q*T_0));
			H_im_inv  *= (sqrt(cabs(Ha_x))/(q*T_0));
		  }
        }
        
            
		ds1->yd[i] = (float)fabs((double)1./sqrt(H_re_inv*H_re_inv + H_im_inv*H_im_inv));
		
		if ((i%nm)==0) // points of the original dataset, i/nm can be replaced by j
		{
		if (bool_plot_TFD==1) 
		{	ds_TFD->xd[i/nm] = ds0->xd[i];
			ds_TFD->yd[i/nm] = (float)fabs(-H_im_inv/(H_re_inv*H_re_inv + H_im_inv*H_im_inv) );
			if (omega!=0) 	ds_TFD->yd[i/nm] /= (float)omega;
		}
		if (bool_plot_real==1) 
		{	ds_real->xd[i/nm]= ds0->xd[i];
			ds_real->yd[i/nm]= (float)H_re_inv;
		}
		if (bool_plot_imag==1) 
		{	ds_imag->xd[i/nm]= ds0->xd[i];
			ds_imag->yd[i/nm]= (float)H_im_inv;
		}
        } // end of n%nm case
        
     }
}	
//	inherit_from_ds_to_ds(ds1, ds0);
	ds1->treatement = my_sprintf(ds1->treatement,"string response function (%s) modulus\nf=%4f, c=%4f and L=%4f et des reflexions...", 
						(bool_elec_meca==1) ? "meca" : (bool_elec_meca==0) ? "elec" : "elec, corrected", 
                        (float)(f), (float)(speed), (float)L);

	if (bool_plot_TFD==1)
	{	// inherit_from_ds_to_ds(ds_TFD, ds0);
		ds_TFD->treatement = my_sprintf(ds_TFD->treatement,"string response function (%s) Im(H)/\\omega\nf=%4f, c=%4f and L=%4f et des reflexions...", 
						(bool_elec_meca==1) ? "meca" : (bool_elec_meca==0) ? "elec" : "elec, corrected", 
                        (float)(f), (float)(speed), (float)L);
	}
	if (bool_plot_real==1)
	{	// inherit_from_ds_to_ds(ds_real, ds0);
		ds_real->treatement = my_sprintf(ds_real->treatement,"string response function (%s) Re(1/H)\nf=%4f, c=%4f and L=%4f et des reflexions...", 
						(bool_elec_meca==1) ? "meca" : (bool_elec_meca==0) ? "elec" : "elec, corrected",
                        (float)(f), (float)(speed), (float)L);
	}
	if (bool_plot_imag==1)
	{	// inherit_from_ds_to_ds(ds_imag, ds0);
		ds_imag->treatement = my_sprintf(ds_imag->treatement,"string response function (%s) Im(1/H)\nf=%4f, c=%4f and L=%4f et des reflexions...", 
						(bool_elec_meca==1) ? "meca" : (bool_elec_meca==0) ? "elec" : "elec, corrected", 
                        (float)(f), (float)(speed), (float)L);
	}
	
	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_graph_response' */












/****************************************************/
/* d'apres les calculs de mai 2008                  */
/* cahier VII p61 et suivantes						*/
/****************************************************/
int do_graph_response_2008(void)
{	int	    n, nm;
	register int 	i, j;
	O_p 	*op = NULL;
	d_s 	*ds0, *ds1, *ds_TFD=NULL, *ds_real=NULL, *ds_imag=NULL;
	pltreg	*pr = NULL;
static int 	bool_elec_meca=0, /* 0 for elec, and 1 for meca */
            bool_increase_resolution=0,
			bool_plot_TFD=0, bool_plot_real=0, bool_plot_imag=0,
			bool_convention=0,
            n_more=8; // add more point to increase resolution (peaks height)
	double	ab, omega, omega_a, q, epsilon, epsilon_p, d,
            Ha_mod2, H_re_inv=0., H_im_inv=0., sigma;
    double  dx;
    double complex  mu, eta, Ha_x, Ha, Hc, H=0.+I*0., D, q_p, tmp_c;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plots the expected response function of a string\n"
					"it takes into account possible reflexions on the boundaries\n"
					"\nFormulas using new piezos eqs (2008) are used !\n"
					"new datasets are created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow}String Response Function - Parameters}\n\n"
			"dissipation f = %8f Hz\n"
			"sound celerity c = %8f m/s\n"
			"string length L = %8f m\n"
			"{\\pt14\\color{yellow}Reflexions parameters}\n\n"
			"Tension T_0 = %8f N\n"
			"spring constant of a piezo k %8f N/m\n\n"
			"{\\pt14\\color{yellow}Electric boundary conditions}\n\n"
			"coefficients of piezo a = %8f u.a. (=b!)\n"
			"                           b = %8f m/V\n"
			"piezo capacitance C = %8f F\n"
			"amplificator input impedance R_{a} = %8f \\Omega\n\n"
			"use convention %R (\\sigma t) or %r (\\sigma x)\n"
			"%R plot electrical response\n"
			"%r plot mechanical response\n"
			"%r plot electrical response corrected by modulus of SR640 amplificator, and by qT_0\n"
			"current X-axis are %R frequencies (Hz) or %r adimensionalized or %r wavenumbers (m^{-1}) ?\n"
			"%b increase resolution by a factor %5d\n"
			"modulus will be plotted anyway.\n"
			"%b plot also Im(H)/\\omega\n"
			"%b plot also Re(1/H)\n"
			"%b plot also Im(1/H)",
			&f, &speed, &L, /* &A_0, */ &T_0, &raideur, &a, &b, &C, &R_a, 
			&bool_convention, &bool_elec_meca, &corde_what_is_x, &bool_increase_resolution, &n_more, 
            &bool_plot_TFD, &bool_plot_real, &bool_plot_imag);
	if (i==CANCEL) return(D_O_K);
	ab = (double)a*b;
	if ( (a*b==0) && ( (bool_plot_imag) || (bool_plot_TFD) ) )
	{	i=win_printf("a*b is zero.\nI cannot plot imaginary parts, there are vanishing !\n\n"
					"click OK to go on without ploting imaginary parts, or CANCEL to abort");
		if (i==CANCEL) return(D_O_K);
		bool_plot_TFD=0;
		bool_plot_imag=0;
	}
	if (bool_increase_resolution==1) 
	{ if (n_more<1) return(win_printf_OK("You choose to increase the computation resolution\n"
                                            "in order to better describe the peaks.\n\n"
                                            "but please choose n\\_more larger than 1!"));
    }

    if (bool_convention==1) return(win_printf_OK("You choose convention with a damping in space\n"
                                            "but the theory isn't done yet !"));

	omega_a = (double)1./((double)R_a*(double)C); // pulsation, not frequency !
	if (omega_a==0) return(win_printf_OK("\\omega_a = 1/R_{a}C is zero !"));
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds0) != 3)		return win_printf_OK("cannot plot region");
	n=ds0->nx;
	
	if (bool_increase_resolution==1)   nm=n_more;
    else                               nm=1;
    if ((ds1 = create_and_attach_one_ds(op, n*nm, n*nm, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	dx = (double)(ds0->xd[1] - ds0->xd[0]);
    
    
	if (bool_plot_TFD==1)
	{	if ((ds_TFD = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	}	
	if (bool_plot_real==1)
	{	if ((ds_real = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	}	
	if (bool_plot_imag==1)
	{	if ((ds_imag = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	}	

if (bool_convention==0) // convention with dissipation in time (2006)
{
	sigma = (double)-f/(double)2.;
	d     = ab*(double)raideur/(double)C;
	
	for (i=0; i<n*nm; i++)
	{	j = (i-(i%nm))/nm; // (j is integer part of (i/nm), but cleaner)
        ds1->xd[i] = ds0->xd[j] + (float)(i%nm)*dx/nm;
	    ds1->yd[i] = (float)0.;
	   
	    if (corde_what_is_x==2) // on current plot, x axis are wavenumbers
	    { q     = (double)ds1->xd[i];
	      omega = sqrt(q*q*speed*speed - (double)f*f/(double)4.);
        }
        else if (corde_what_is_x==0)       // on current plot, x axis are frequencies
		{ omega = (double)ds1->xd[i]*2.*M_PI; // pulsation 
		  q     = sqrt(omega*omega + (double)f*f/(double)4.) / speed;
        }
        else /* if (corde_what_is_x==1) */    // on current plot, x axis are non-dimensionalized (f/f_0 or k/k_0)
        { q     = (double)ds1->xd[i]*M_PI/L;
          omega = sqrt(q*q*speed*speed - (double)f*f/(double)4.);
	    } 
		
		epsilon    = (double)q*T_0/raideur;
		
		Hc = (1.0 + I*omega/omega_a) / (sigma/omega_a + I*omega/omega);  // cahier VII p68
		if (d!=0) Hc = -Hc/d;
		Ha = -epsilon/Hc;
		Ha_mod2 = creal(H)*creal(H) + cimag(H)*cimag(H);
		
        if (bool_elec_meca==1) // mecanical response
		{		H = raideur*( (1.0 - 1.0/Hc)*cos(q*L) - epsilon*sin(q*L) ); // cahier VII p65
		}
		else if (bool_elec_meca==0) // electrical response
		{		H = cos(q*L);
		
				if (epsilon!=0) 
				H += (((1.0 + epsilon*epsilon)*Hc - 1)*sin(q*L))/epsilon;
		}
		else if (bool_elec_meca==2) // electrical response, corrected by modulus of ampli response
		{		H = cos(q*L);
		
				if (epsilon!=0) 
				H += (((1.0 + epsilon*epsilon)*Hc - 1)*sin(q*L))/epsilon;
				
				if (epsilon!=0) 
				H *= (sqrt(Ha_mod2)/(q*T_0));
		}
        H_re_inv  = creal(H);
		H_im_inv  = cimag(H);

         
		ds1->yd[i] = (float)cabs((double)1./H);
		
		if ((i%nm)==0) // points of the original dataset, i/nm can be replaced by j
		{
		if (bool_plot_TFD==1) 
		{	ds_TFD->xd[i/nm] = ds0->xd[i];
			ds_TFD->yd[i/nm] = (float)fabs( cimag((double)1./H) );
			if (omega!=0) 	ds_TFD->yd[i/nm]/= omega;
		}
		if (bool_plot_real==1) 
		{	ds_real->xd[i/nm]= ds0->xd[i];
			ds_real->yd[i/nm]= (float)H_re_inv;
		}
		if (bool_plot_imag==1) 
		{	ds_imag->xd[i/nm]= ds0->xd[i];
			ds_imag->yd[i/nm]= (float)H_im_inv;
		}
        } // end of n%nm case
	}
}
else // convention with dissipation in space
{   for (i=0; i<n*nm; i++)
	{	j = (i-(i%nm))/nm; // (j is integer part of (i/nm), but cleaner)
        ds1->xd[i] = ds0->xd[j] + (float)(i%nm)*dx/nm;
	    ds1->yd[i] = (float)0.;
	   
	    if (corde_what_is_x==2) // on current plot, x axis are wavenumbers
	    { q     = (double)ds1->xd[i];
	      omega = sqrt(q*q*speed*speed / ( (double)1. + (double)f*f/((double)4.*q*q*speed*speed)));
        }
        else if (corde_what_is_x==0)       // on current plot, x axis are frequencies
		{ omega = (double)ds1->xd[i]*2.*M_PI; // pulsation 
		  if (f<=omega) 
		  q     = sqrt(omega*omega/(double)2.*( (double)1. + sqrt( (double)1. - (double)f*f/((double)omega*omega) ) ));
		  else
		  q     =0.;
		  
		  q    /= speed;
        }
        else /* if (corde_what_is_x==1) */    // on current plot, x axis are non-dimensionalized (f/f_0 or k/k_0)
        { q     = (double)ds1->xd[i]*M_PI/L;
          omega = sqrt(q*q*speed*speed / ( (double)1. + (double)f*f/((double)4.*q*q*speed*speed)));
	    } 
		
        sigma      = (double)f*q/sqrt(4*q*q*speed*speed+f*f);
        epsilon    = (double)q*T_0/raideur;
	    epsilon_p  = (double)sigma*T_0/raideur;  // cahier VI,p191
	    
	    eta        = epsilon_p + I * epsilon;
	    Ha_x       = (ab*raideur/C)*(epsilon-I*epsilon_p)*(omega/omega_a)/((double)1. + I*omega/omega_a); // VIp203
	    mu         = eta + Ha_x;
	    q_p        = q - I*sigma;
	    D          =   ((double)1.-mu)*((double)1.+eta)*cexp(-I*q_p*L)	// VIp193
	                 - ((double)1.+mu)*((double)1.-eta)*cexp(+I*q_p*L);
	    
	    { if (bool_elec_meca==1) // mechanical response
		  {	tmp_c      = eta*raideur/mu/(double)2. * ( (1-mu)*cexp(-I*q_p*L) + (1+mu)*cexp(+I*q_p*L) );
			H_re_inv   = creal(tmp_c);
               H_im_inv   = cimag(tmp_c);
		  }
		  else if (bool_elec_meca==0) // electrical response
		  {	H_re_inv   = creal(-D/(Ha_x*(double)2.));
               H_im_inv   = cimag(-D/(Ha_x*(double)2.));
		  }
		  else if (bool_elec_meca==2) // electrical response, corrected by modulus of ampli response
		  {  H_re_inv   = creal(-D/(Ha_x*(double)2.));
               H_im_inv   = cimag(-D/(Ha_x*(double)2.));
               
               H_re_inv  *= (sqrt(cabs(Ha_x))/(q*T_0));
			H_im_inv  *= (sqrt(cabs(Ha_x))/(q*T_0));
		  }
        }
        
            
		ds1->yd[i] = (float)fabs((double)1./sqrt(H_re_inv*H_re_inv + H_im_inv*H_im_inv));
		
		if ((i%nm)==0) // points of the original dataset, i/nm can be replaced by j
		{
		if (bool_plot_TFD==1) 
		{	ds_TFD->xd[i/nm] = ds0->xd[i];
			ds_TFD->yd[i/nm] = (float)fabs(-H_im_inv/(H_re_inv*H_re_inv + H_im_inv*H_im_inv) );
			if (omega!=0) 	ds_TFD->yd[i/nm] /= (float)omega;
		}
		if (bool_plot_real==1) 
		{	ds_real->xd[i/nm]= ds0->xd[i];
			ds_real->yd[i/nm]= (float)H_re_inv;
		}
		if (bool_plot_imag==1) 
		{	ds_imag->xd[i/nm]= ds0->xd[i];
			ds_imag->yd[i/nm]= (float)H_im_inv;
		}
        } // end of n%nm case
        
     }
}	
//	inherit_from_ds_to_ds(ds1, ds0);
	ds1->treatement = my_sprintf(ds1->treatement,"string response function (%s) modulus\nf=%4f, c=%4f and L=%4f et des reflexions...", 
						(bool_elec_meca==1) ? "meca" : (bool_elec_meca==0) ? "elec" : "elec, corrected", 
                        (float)(f), (float)(speed), (float)L);

	if (bool_plot_TFD==1)
	{	// inherit_from_ds_to_ds(ds_TFD, ds0);
		ds_TFD->treatement = my_sprintf(ds_TFD->treatement,"string response function (%s) Im(H)/\\omega\nf=%4f, c=%4f and L=%4f et des reflexions...", 
						(bool_elec_meca==1) ? "meca" : (bool_elec_meca==0) ? "elec" : "elec, corrected", 
                        (float)(f), (float)(speed), (float)L);
	}
	if (bool_plot_real==1)
	{	// inherit_from_ds_to_ds(ds_real, ds0);
		ds_real->treatement = my_sprintf(ds_real->treatement,"string response function (%s) Re(1/H)\nf=%4f, c=%4f and L=%4f et des reflexions...", 
						(bool_elec_meca==1) ? "meca" : (bool_elec_meca==0) ? "elec" : "elec, corrected",
                        (float)(f), (float)(speed), (float)L);
	}
	if (bool_plot_imag==1)
	{	// inherit_from_ds_to_ds(ds_imag, ds0);
		ds_imag->treatement = my_sprintf(ds_imag->treatement,"string response function (%s) Im(1/H)\nf=%4f, c=%4f and L=%4f et des reflexions...", 
						(bool_elec_meca==1) ? "meca" : (bool_elec_meca==0) ? "elec" : "elec, corrected", 
                        (float)(f), (float)(speed), (float)L);
	}
	
	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_graph_response_2008' */




int do_graph_spectre_reflexions(void)
{	int	 n;
	register int 	i;
	O_p 	*op = NULL;
	d_s 	*ds0, *ds1;
	pltreg	*pr = NULL;
static float T_0=9.81*4.0, masse=(2.45e-4)*(5e-3), raideur = (1e-7)*(2.61e5)*(2.61e5);
	double	omega, x, q, beta_r, beta_i, beta_mod2;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This plots the expected spectrum of a string\n"
					"it takes into account possible reflexions on the boundaries\n"
					"A new dataset is created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow}String spectrum - Parameters}\n\n"
			"dissipation f = %8f Hz\n"
			"sound celerity c = %8f m/s\n"
			"string length L = %8f m\n"
			"normalisation A_0 = %8f (a.u.)\n\n"
			"{\\pt14\\color{yellow}Reflexions parameters}\n\n"
			"Tension = %8f N\n"
			"masse de corde sur le piezo : %8f kg\n"
			"raideur d'un piezo %8f N/m",
			&f, &speed, &L, &A_0, &T_0, &masse, &raideur);
	if (i==CANCEL) return(D_O_K);
	
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds0) != 3)		return win_printf_OK("cannot plot region");
	n=ds0->nx;
	
	if ((ds1 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	for (i=0; i<n; i++)
	{	ds1->xd[i] = ds0->xd[i];
		omega      = (double)ds0->xd[i]*2.*M_PI;
		q          = sqrt(omega*omega + (double)f*f/(double)4.) / speed;
		beta_mod2  = (-(double)raideur + (double)masse*(omega*omega-f*f/4.)) * (-(double)raideur + (double)masse*(omega*omega-f*f/4))
					+ (double)masse*masse*f*f*omega*omega;
		beta_r	   = T_0 * (-(double)raideur + masse*(omega*omega-f*f/4.)) / beta_mod2;
		beta_i     = T_0 * masse*(-f)*omega / beta_mod2;
		x	   	   = omega*(double)L/(double)speed;
		
		ds1->yd[i] = (float)((double)1./(  ( (double)1. + q*q*(beta_r*beta_r-beta_i*beta_i)) * ( (double)1. + q*q*(beta_r*beta_r-beta_i*beta_i)) 
								 +  ( (double)2.*q*q*beta_r*beta_i)    * ( (double)2.*q*q*beta_r*beta_i) 
								 ));
		ds1->yd[i] *= (float)( ((double)1. + q*beta_r/tan(q*L)) * ((double)1. + q*beta_r/tan(q*L)) 
						     + (q*beta_i/tan(q*L))*(q*beta_i/tan(q*L)) );	
		ds1->yd[i] = A_0*(float)sqrt((double)(ds1->yd[i]));
	}
	ds1->treatement = my_sprintf(ds1->treatement,"spectre avec f=%8f, c=%8f and L=%8f et des reflexions...", 
						(float)(f), (float)(speed), (float)L);
	inherit_from_ds_to_ds(ds1, ds0);
	
	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_graph_spectre_reflexions' */



#endif

