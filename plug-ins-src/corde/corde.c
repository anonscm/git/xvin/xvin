#ifndef _CORDE_C_
#define _CORDE_C_

#include <gsl/gsl_histogram.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_sf.h>
#include <complex.h>
#include <fftw3.h>

#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"
#include "fftw3_lib.h"

#include "../response/response.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "corde.h"
#include "corde_peaks.h"  // to find peaks
#include "corde_theory.h" // to plot analytical results


float f=0.1, speed=413, L=0.603, A_0=1.;
float a_d=1e-6; /* dispersion */
float T_0=9.81*4.0, raideur = 1.386e8;
/* 2007-06-27, changed. before : raideur = (1e-7)*(2.61e5)*(2.61e5); */
float C=90e-12, R_a=1e6, b=3e-8, a=3e-8; /* before : a=1e-7 */
int   corde_what_is_x=0; // what is the x-unit of the current dataset ?







int do_corde_frequency_into_wavenumber(void)
{	int	 n;
	register int 	i;
	O_p 	*op = NULL;
	d_s 	*ds0, *ds1;
	pltreg	*pr = NULL;
	double	omega, q;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This converts the abciss from frequency into wavenumber\n"
					"ie, x -> x * q_0/f_0 (neglecting dispersion) \n\n"
					"A new dataset is created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow}Frequency to wavenumber - Parameters}\n\n"
			"dissipation f = %8f Hz\n"
			"sound celerity c = %8f m/s\n"
			"string length L = %8f m\n\n"
            "note: dispersion (due to dissipation) is taken into acount via f\n"
            "choose f=0 to cancel dispersion.",
			&f, &speed, &L);
	if (i==CANCEL) return(D_O_K);
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds0) != 3)		return win_printf_OK("cannot plot region");
	n=ds0->nx;
	
	if ((ds1 = create_and_attach_one_ds(op, n, n, 0)) == NULL)	return(win_printf_OK("cannot create dataset"));
	for (i=0; i<n; i++)
	{	ds1->yd[i] = ds0->yd[i];
		omega      = (double)ds0->xd[i]*2.*M_PI;
		q          = sqrt(omega*omega + (double)f*f/(double)4.) / speed;
		
		ds1->xd[i] = (float)q;
		
	}
	ds1->treatement = my_sprintf(ds1->treatement,"freq -> wavenumber in X");
	inherit_from_ds_to_ds(ds1, ds0);
	
	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_corde_frequency_into_wavenumber' */






void spit(char *message)
{	size_t	l_max=55;
	char 	white_string[64]="                                              ";
	
	if (strlen(message)<l_max) 	strncat (message, white_string, l_max-strlen(message));
	textout_ex(screen, font, message, 40, 40, makecol(100,155,155), makecol(2,1,1));
	return;
}



int do_corde_separate_modes(void)
{
	int	i, j, index;
	pltreg 	*pr;
	O_p 	*op, *opd=NULL;
	d_s 	*ds, *dsd=NULL; 
	int 	ny, width;
	int	n_h=1, n_h_max, h, *h_index=NULL;
static char	h_string[128]="1:10";
static float	f_0=1., df=13;
    float   f_acq =1.;
static int      bool_output=0;
	char 	message_to_spit[128]="searching...", *message=NULL;
	float   f, f_low, f_high;
	int     mode_0=1., mode, go_on=1;
	fftw_complex *y_complex;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
		
	if (key[KEY_LSHIFT]) return(win_printf_OK("This routine performs Hilbert transform of the active dataset.\n"
				"itis written to isolate a single mode.\n\n"
				"if you choose to work on all harmonics, a new plot is created\n"
				"with as many datasets as modes, each one corresponding to an Hilbert transform."));
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return(win_printf_OK("I cannot find data"));
	ny = ds->ny;	
    f_acq = find_sampling_frequency(ds->xd, ny);
    
	if (index&CORDE_GLOBAL)
	while (go_on==1)
	{	f    = f_0;
	    go_on=0;
		mode = (int)( (float)(f/f_acq)*(float)ny );
		message=my_sprintf(message, "{\\color{yellow}\\pt14 Hilbert transform(s)}\n\n"
			"I need to know the fondamental frequency, and the sampling frequency\n"
			"   f_p = %%8f Hz (fundamental freq.)\n"
			"   f_s = %%8f Hz (sampling freq.)\n"
			"   \\delta f = %%8f Hz (filter width)\n"
			"f_p = %5.3f corresponds to mode %d in (f_{s}/N) units\n"
			"examine harmonics %%20s\n"
			"%%R plot amplitude or %%r real part or %%r phase.\n\n"
 			"%%b plot this window again, with updated values",
			f, mode);
		i=win_scanf(message, &f_0, &f_acq, &df, &h_string, &bool_output, &go_on);
		if (i==CANCEL) return(D_O_K);
		free(message); message=NULL;
	}
	else if (index&CORDE_LOCAL)
	while (go_on==1)
	{	f    = f_0;
		go_on=0;
		mode = (int)( (float)(f/f_acq)*(float)ny );
		message=my_sprintf(message, "{\\color{yellow}\\pt14 Hilbert transform}\n\n"
			"I need to know the frequency of the peak you want, and the sampling frequency\n"
			"   f_p = %%8f Hz (mode to study)\n"
			"   f_s = %%8f Hz (sampling)\n"
			"   \\delta f = %%8f Hz (filter width)\n"
			"f_p = %5.3f corresponds to mode %d in (f_{s}/N) units\n"
			"%%R plot amplitude or %%r real part or %%r phase.\n\n"
 			"%%b plot this window again, with updated values",
			f, mode);
		i=win_scanf(message, &f_0, &f_acq, &df, &bool_output, &go_on);
    	if (i==CANCEL) return(D_O_K);
		free(message); message=NULL;
			
		sprintf(h_string, "1");
	}
	else return(D_O_K);
	
	mode_0 = (int)( (float)(f_0/f_acq)*(float)ny );	
	width  = (int)( (float)( df/f_acq)*(float)ny );	
	if ( (mode_0-2*width)<0 ) return(win_printf_OK("f_0=%f Hz (%d a.u.)\n"
                            "and filter width %f Hz (%d a.u.) are incompatible!", f_0, mode_0, df, width));

	h_index = str_to_index(h_string, &n_h);	// malloc is done by str_to_index
	if ( (h_index==NULL) || (n_h<1) )	return(win_printf_OK("bad values for harmonics !"));

	n_h_max = (int)floor( (f_acq/2.)/f_0 - (double)(3.*df)/f_0);
	if (n_h>n_h_max) 					
	{	sprintf(h_string, "1:%d", n_h_max);
		i=win_printf("larger possible harmonic is %d\n"
					"I suggest {\\color{yellow}%s}\n\n"
					"click OK to accept or CANCEL to abort", n_h_max, h_string);
		if (i==CANCEL) return(D_O_K);
		h_index = str_to_index(h_string, &n_h);	// malloc is done by str_to_index
		if ( (h_index==NULL) || (n_h<1) )	return(win_printf_OK("bad values for harmonics !"));
	}
		
	if (index&CORDE_LOCAL) opd = op;
		
	for (i=0; i<n_h; i++)
	{	h=h_index[i];

		if ( (index&CORDE_GLOBAL) && (i==0) )
		{	if ((opd = create_and_attach_one_plot(pr, ny, ny, 0)) == NULL)
			   return(win_printf_OK("cannot create plot !"));
			dsd = opd->dat[0];
			set_plot_title(opd,"\\stack{{%s}{demodulation, [%s]f_0}}", op->title, h_string);
			set_plot_x_title(opd, "%s", op->x_title);
			set_plot_y_title(opd, "%s", op->y_title);
			opd->filename = Transfer_filename(op->filename);
     		opd->dir = Mystrdup(op->dir);
		}
		else 	
		{	if ((dsd = create_and_attach_one_ds (opd, ny, ny, 0)) ==NULL)
			return(win_printf_OK("cannot create dataset !"));
		}
		
		if (bool_output==0)      message=my_sprintf(message, "modulus");
		else if (bool_output==1) message=my_sprintf(message, "real part");
		else if (bool_output==2) message=my_sprintf(message, "angle");
		inherit_from_ds_to_ds(dsd, ds);
		dsd->treatement = my_sprintf(dsd->treatement, "Demodulation (%s)\naround %5.1f Hz, harmonic=%d", message, h*f_0, h);
		free(message); message=NULL;
	
		sprintf(message_to_spit, "Hilbert transform %d/%d at %d f_0", i+1, n_h, h); spit(message_to_spit);
		
        f_low  = (float)h*f_0 - 2.*df;
		f_high = (float)h*f_0 + 2.*df;
		y_complex = Hilbert_transform(ds->yd, ny, f_acq, f_low, df, f_high, df, 1);
	
		for (j=0; j<ny; j++) 
		{	dsd->xd[j] = ds->xd[j];
			if (bool_output==0)      dsd->yd[j] = (float)cabs(y_complex[j]);
			else if (bool_output==1) dsd->yd[j] = (float)creal(y_complex[j]);
			else if (bool_output==2) dsd->yd[j] = (float)carg(y_complex[j]);
		}
		
		free(y_complex);
	}
	
     	free(h_index);
	return(refresh_plot(pr, UNCHANGED));
} /* end of the "do_corde_separate_mode" function */







int do_corde_fit_slopes(void)
{
	int	    i, j, index;
	pltreg 	*pr;
	O_p 	*op, *opd=NULL;
	d_s 	*ds, *ds_slopes=NULL; 
	int 	ny, n_keep;
	double  X2, XY, Y2, X, Y, slope, b;
	int	    h;
static char	h_string[128]="harmonic=";
static int bool_signe=1;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
		
	if (key[KEY_LSHIFT]) return(win_printf_OK("This routine performs linear fits on all datasets.\n"
				"Only visible points are used.\n\n"
				"A new plot is created, with a new dataset\n"
				"X-axis of this dataset will be indexed by some string searched for."));
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return(win_printf_OK("I cannot find data"));
 
	i=win_scanf("{\\color{yellow}\\pt14 Linear Fit}\n\n"
			"I need to know how to present results\n"
			"I will plot slopes versus a parameter, tell me how this\n"
            "parameter is described in datasets to fit:%s\n"
            "%%b multiply slope by (-1)",
			&h_string, &bool_signe);
	if (i==CANCEL) return(D_O_K);

    opd = create_and_attach_one_plot(pr, op->n_dat, op->n_dat, 0);
    if (opd==NULL) return(win_printf_OK("cannot create plot !"));
    ds_slopes = opd->dat[0];
    if (ds_slopes==NULL) return(win_printf_OK("cannot create dataset !"));
   			   			
	for (j=0; j<op->n_dat; j++)
	{	ds = op->dat[j];
        ny = ds->nx;
        if (ds == NULL)   return (win_printf_OK("can't find data set"));
		
        h = grep_int_in_string(ds->treatement, h_string);
	    if (h==-1) h = grep_int_in_string(ds->history, h_string);
	
		X2=(double)0.; XY=(double)0.; Y2=(double)0.; X=0.; Y=0.; n_keep=0;
		for (i=0; i<ny; i++)	
		{	if ( (ds->xd[i] < op->x_hi) && (ds->xd[i] >= op->x_lo)
			  && (ds->yd[i] < op->y_hi) && (ds->yd[i] >= op->y_lo) )
			{	X  += (double)ds->xd[i];
			    Y  += (double)ds->yd[i];
				X2 += (double)ds->xd[i]*(double)ds->xd[i];
				XY += (double)ds->yd[i]*(double)ds->xd[i];
				Y2 += (double)ds->yd[i]*(double)ds->yd[i]; // required just for error bars
				n_keep++;
			}  
		}
		
/*		// imposing b=0 in fit Y=aX+B: 
		if (X2==0)	slope=0.;
		else 		slope=(double)(XY/X2);
*/
        // general fit Y=aX+B: 
        if (n_keep<2)     { slope = 0.; b=0.; }
        else 
        {  b = (X2-X*X/(double)n_keep); // tmp!
           if (b==0) slope = 0.;
           else      slope = (XY - X*Y/(double)n_keep)/b;
           b = (Y - slope*X)/(double)n_keep;
        }

	    ds_slopes->xd[j] = (float)h;
		ds_slopes->yd[j] = (float)slope;
		if (bool_signe==1) ds_slopes->yd[j] *=-1;
	}

    set_plot_title(opd,"\\stack{{%s}{slopes}}", op->title);
	set_plot_x_title(opd, "%s", h_string);
	set_plot_y_title(opd, "slope");
	opd->filename = Transfer_filename(op->filename);
	opd->dir = Mystrdup(op->dir);
	
	set_ds_dot_line(ds_slopes);	set_ds_point_symbol(ds_slopes, "\\pt10\\oc");

	return(refresh_plot(pr, UNCHANGED));
} /* end of the "do_corde_fit_slope" function */






// 2007-11-14
int do_corde_fit_response(void)
{
	int	    i, j, index;
	pltreg 	*pr;
	O_p 	*op, *op_modulus=NULL, *op_ri=NULL;
	d_s 	*ds, *dsd=NULL, *ds_r=NULL, *ds_i=NULL,
            *ds_position=NULL, *ds_max=NULL, *ds_imag=NULL, *ds_width=NULL, *ds_tan=NULL; 
	int 	ny, n_h, n_keep;
	double  X2, XY_r, XY_i, Y2_r, Y2_i, X, Y_r, Y_i, slope_r, b_r, slope_i, b_i;
	double  x1, max1;
	int	    h, i_start, i_end;
	float   f_low, f_high;
static float delta_f=13.;
static int bool_response_inverted=0, bool_plot_coeffs=1, bool_plot_modulus=1, bool_plot_ReIm;
static int bool_plot_complete=1;
	char 	message_to_spit[128]="searching...";
	fftw_complex H;

	if(updating_menu_state != 0)	
	{	if (current_response->mode==0) active_menu->flags |=  D_DISABLED;
		else	 			           active_menu->flags &= ~D_DISABLED; 
		return D_O_K;	
	}
	index = active_menu->flags & 0xFFFFFF00;
		
	if (key[KEY_LSHIFT]) return(win_printf_OK("This routine performs a linear fit of the real and\n"
				"imaginary part of the response function in the vicinity of one or several harmonics.\n\n"
				"Active dataset must contain in Y frequencies to search harmonics,\n"
				"i.e., it has to be produced by ""find peaks""\n\n"
				"A new plot is created, with as many datasets as harmonics treated.\n"));
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return(win_printf_OK("I cannot find data"));
	n_h = ds->ny;
    
	i=win_scanf("{\\color{yellow}\\pt14 Linear fit of the string response function}\n\n"
			"Active response is %R normal or %r inverted\n."
			"Give me \\delta f such that fit will be operated on"
			"[f_n - \\delta f; f_n + \\delta f]\n"
            		"\\delta f = %8f Hz\n"
			"%b plot modulus of H in ranges of fit\n"
			"%b plot real and imaginary parts of 1/H in ranges of fit\n"
            "%b plot position, height, width, Im and tan \\delta  of fitted peaks : \n"
            "     %R assume peak is located at f such that Re(1/H)=0\n"
            "     %r find peak as the max of modulus |H|\n\n"
            "{\\color{lightred}note}: a good spectral resolution is of course expected!",
			&bool_response_inverted, &delta_f, 
            &bool_plot_modulus, &bool_plot_ReIm, &bool_plot_coeffs, &bool_plot_complete);
	if (i==CANCEL) return(D_O_K);
//    if (bool_plot_coeffs==0) 

	if (bool_response_inverted==0) inverse_response(current_response);
 	ny = current_response->n; // nb of pts in response function
	
	if (bool_plot_coeffs==1)
	{	ds_position = create_and_attach_one_ds (op, n_h, n_h, 0); inherit_from_ds_to_ds(ds_position, ds);
		ds_max      = create_and_attach_one_ds (op, n_h, n_h, 0); inherit_from_ds_to_ds(ds_max, ds);
		ds_imag     = create_and_attach_one_ds (op, n_h, n_h, 0); inherit_from_ds_to_ds(ds_imag, ds);
		ds_width    = create_and_attach_one_ds (op, n_h, n_h, 0); inherit_from_ds_to_ds(ds_width, ds);
		ds_tan      = create_and_attach_one_ds (op, n_h, n_h, 0); inherit_from_ds_to_ds(ds_tan, ds);
			      
        if ( (ds_position == NULL) || (ds_max == NULL) || (ds_imag == NULL) || (ds_width == NULL) || (ds_tan == NULL) )
		   return (win_printf_OK("can't create data sets"));
	}
	
	for (j=0; j<n_h; j++)
	{	h=ds->xd[j];
        	f=ds->yd[j];
	
		sprintf(message_to_spit, "fit %d/%d at %d f_0", j+1, n_h, h); spit(message_to_spit);
		
        f_low  = f - delta_f;
		f_high = f + delta_f;
		
		// preparing fit :
		X2=(double)0.; X=0.; Y_r=0.; Y_i=0.; n_keep=0;
		XY_r=(double)0.; Y2_r=(double)0.; XY_i=(double)0.; Y2_i=(double)0.; 
		i_start=ny-1; i_end=0;
		for (i=0; i<ny; i++)	
		{	if ( (current_response->f[i] < f_high) && (current_response->f[i] >= f_low) )
			{  if (i_start>i) i_start=i;
			   if (i_end  <i) i_end=i;
                	   X    += current_response->f[i];
			   Y_r  += creal(current_response->r[i]);
   			   Y_i  += cimag(current_response->r[i]);
			   X2   += current_response->f[i]       *current_response->f[i];
			   XY_r += creal(current_response->r[i])*current_response->f[i];
			   Y2_r += creal(current_response->r[i])*creal(current_response->r[i]); // required just for error bars
			   XY_i += cimag(current_response->r[i])*current_response->f[i];
			   Y2_i += cimag(current_response->r[i])*cimag(current_response->r[i]); // required just for error bars
			   n_keep++;
			}  
		}	
		if ( (i_end-i_start) >= n_keep) return(win_printf_OK("pb with nb of points...")); 
        	if (n_keep<2)     return(win_printf_OK("%d points to perform fit... not enough!\n\n"
					"probably the time-windows of fftw is too small\n"
					"try to increase \\delta f...", n_keep));
        
		// fit of real part:
        	{  b_r = (X2-X*X/(double)n_keep); // tmp!
	           if (b_r==0) slope_r = 0.;
        	   else        slope_r = (XY_r - X*Y_r/(double)n_keep)/b_r;
	           b_r = (Y_r - slope_r*X)/(double)n_keep;
        	}

	        // fit of imaginary part:
        	{  b_i = (X2-X*X/(double)n_keep); // tmp!
        	   if (b_i==0) slope_i = 0.;
	           else        slope_i = (XY_i - X*Y_i/(double)n_keep)/b_i;
        	   b_i = (Y_i - slope_i*X)/(double)n_keep;
        	}
	
	        // creating plot and datasets, and saving fit:	
            if (bool_plot_modulus==1)  // plot modulus of fit
		    {  if (j==0)
		       {	if ((op_modulus = create_and_attach_one_plot(pr, n_keep, n_keep, 0)) == NULL)
			           return(win_printf_OK("cannot create plot !"));
		           	set_plot_title  (op_modulus,"\\stack{{%s}{fits of response function}{%s}}", op->title, current_response->info);
	                set_plot_x_title(op_modulus, "f (Hz)");
                    set_plot_y_title(op_modulus, "|H|");
	                op_modulus->filename = Transfer_filename(op->filename);
	                op_modulus->dir = Mystrdup(op->dir);

                    dsd = op_modulus->dat[0];	
               }
		       else dsd = create_and_attach_one_ds (op_modulus, n_keep, n_keep, 0);
  		       if (dsd==NULL)	return(win_printf_OK("cannot create dataset !"));
  		 
               // saving data:      
  		       for (i=i_start; i<=i_end; i++)
		       {   dsd->xd[i-i_start] = (float)current_response->f[i];
		           H          =   (slope_r*current_response->f[i] + b_r) 
        	       	          + I*(slope_i*current_response->f[i] + b_i) ;
		           dsd->yd[i-i_start] = (float)( (double)1.0/cabs(H) );
	           }
	           
	           // saving infos:
               inherit_from_ds_to_ds(dsd, ds);
               dsd->treatement = my_sprintf(dsd->treatement, "fit of H, modulus, harmonic=%d", h);
		    }
		    if (bool_plot_ReIm==1) // plot Re and Im
		    {  if (j==0)
		       {	if ((op_ri = create_and_attach_one_plot(pr, n_keep, n_keep, 0)) == NULL)
			           return(win_printf_OK("cannot create plot !"));
			        set_plot_title  (op_ri,"\\stack{{%s}{fits of response function}}", op->title);
	                set_plot_x_title(op_ri, "f (Hz)");
                    set_plot_y_title(op_ri, "Re and Im of 1/H");
	                op_ri->filename = Transfer_filename(op->filename);
	                op_ri->dir = Mystrdup(op->dir);

                    ds_r = op_ri->dat[0];	
                    ds_i = create_and_attach_one_ds (op_ri, n_keep, n_keep, 0);
               }
		       else 
               {    ds_r = create_and_attach_one_ds (op_ri, n_keep, n_keep, 0);
                    ds_i = create_and_attach_one_ds (op_ri, n_keep, n_keep, 0);               
               }
  		       if ( (ds_r==NULL) || (ds_i==NULL) )	return(win_printf_OK("cannot create dataset !"));
  		    		 
               // saving:      
  		       for (i=i_start; i<=i_end; i++)
		       {   ds_r->xd[i-i_start] = (float)current_response->f[i];
		           ds_i->xd[i-i_start] = (float)current_response->f[i];
		           H          =   (slope_r*current_response->f[i] + b_r) 
        	       	          + I*(slope_i*current_response->f[i] + b_i) ;
        	//       H          = (double)1.0/H;	          
		           ds_r->yd[i-i_start] = (float)( creal(H) );
		           ds_i->yd[i-i_start] = (float)( cimag(H) );
	           }
	           
	           // saving infos:
               inherit_from_ds_to_ds(ds_r, ds);
               inherit_from_ds_to_ds(ds_i, ds);
               ds_r->treatement = my_sprintf(ds_r->treatement, "fit of real part of 1/H, harmonic=%d", h);
               ds_i->treatement = my_sprintf(ds_i->treatement, "fit of imag part of 1/H, harmonic=%d", h);
		    }
		    
		if (bool_plot_coeffs==1) 
        {   ds_position->xd[j] = (float)ds->xd[j];
            ds_imag->xd[j]     = (float)ds->xd[j];
            ds_max->xd[j]      = (float)ds->xd[j];
	        ds_width->xd[j]    = (float)ds->xd[j];
            ds_tan->xd[j]      = (float)ds->xd[j];
                   
            if (bool_plot_complete==0)
            { if (slope_r*slope_i!=0) 
		      {  ds_position->yd[j] = (float) (-b_r/slope_r);
                 ds_imag->yd[j]     = (float)((-b_r/slope_r)*slope_i + b_i);
		//	     ds_imag->yd[j]     = (float)1.0/ds_imag->yd[j]; // if not, it is Im(1/H)
			     ds_max->yd[j]      = (float)(1.0/fabs((-b_r/slope_r)*slope_i + b_i));
			     ds_width->yd[j]    = (float)fabs( ((-b_r/slope_r)*slope_i + b_i)* 2.0 
                                 * sqrt(3.*slope_r*slope_r + 4.*slope_i*slope_i) / (slope_r*slope_r + slope_i*slope_i) );
		         ds_tan->xd[j]      = (float)0.; // not defined if Re  0 !!!
              }	
		    }
	        else 
	        { if (slope_r*slope_r + slope_i*slope_i!=0) 
		      {  x1   = -(slope_r*b_r + slope_i*b_i) / (slope_r*slope_r + slope_i*slope_i);
		         max1 =  (slope_r*b_r + slope_i*b_i)*x1 + b_r*b_r + b_i*b_i; // corrige le 2007/12/04
		                 // cf cahier VI p 281
                 ds_position->yd[j] = (float)x1;
                 ds_max->yd[j]      = (float)(1.0/sqrt(max1));
			     ds_imag->yd[j]     = (float)(x1*slope_i + b_i);
			     ds_width->yd[j]    = (float)(2.0*sqrt( 3*max1 / (slope_r*slope_r + slope_i*slope_i) ));
// il y avait une erreur dans le cahier de manip de Julien Frison, novembre 2007.
		         ds_tan->yd[j]      = (float)( (x1*slope_i + b_i)/(x1*slope_r + b_r));;  	// Im(H)/Re(H)
              }
            }
        }
	
	}

	if (bool_response_inverted==0) inverse_response(current_response);
	

	if (bool_plot_coeffs==1)
	{	ds_position->treatement = my_sprintf(ds_position->treatement, "peak position (Hz) from linear fit (%s)",
                                  (bool_plot_complete==0) ? "Re(1/H)=0" : "max");
		ds_imag->treatement     = my_sprintf(ds_imag->treatement, "imaginary part of H at resonnance from linear fit (%s)",
                                  (bool_plot_complete==0) ? "Re(1/H)=0" : "max");
		ds_max->treatement      = my_sprintf(ds_max->treatement, "peak height (max of H) from linear fit (%s)",
                                  (bool_plot_complete==0) ? "Re(1/H)=0" : "max");
		ds_width->treatement    = my_sprintf(ds_width->treatement, "peak width from linear fit (%s)",
                                  (bool_plot_complete==0) ? "Re(1/H)=0" : "max");
		ds_tan->treatement      = my_sprintf(ds_tan->treatement, "tan\\delta  from linear fit (%s)",
                                  (bool_plot_complete==0) ? "Re(1/H)=0 : NOT DEFINED!!!" : "max");
		
		set_ds_dot_line(ds_position); set_ds_point_symbol(ds_position, "\\pt10\\sq");
		set_ds_dot_line(ds_imag);	  set_ds_point_symbol(ds_imag, "\\pt10\\sq");
		set_ds_dot_line(ds_max);	  set_ds_point_symbol(ds_max, "\\pt10\\sq");
		set_ds_dot_line(ds_width);	  set_ds_point_symbol(ds_width, "\\pt10\\sq");
		set_ds_dot_line(ds_tan);	  set_ds_point_symbol(ds_tan, "\\pt10\\sq");
	}	
	return(refresh_plot(pr, UNCHANGED));
} /* end of the "do_corde_fit_response" function */






MENU *corde_plot_theory1_submenu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
	add_item_to_menu(mn,"spectrum of string/cavity (better? 07/12)",	do_graph_spectre_better,NULL, 0, NULL);
	add_item_to_menu(mn,"spectrum with reflexions",			do_graph_spectre_reflexions,	NULL, 0, NULL);
	add_item_to_menu(mn,"\0",					0,				NULL, 0, NULL);
	add_item_to_menu(mn,"plot piezo/SR640 response function",	do_graph_response_electric_ampli,	NULL, 0, NULL);
	add_item_to_menu(mn,"plot full string response function",	do_graph_response,			NULL, 0, NULL);
	
	return mn;
}
	
MENU *corde_plot_theory2_submenu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
	add_item_to_menu(mn,"plot full string response function",	do_graph_response_2008,			NULL, 0, NULL);
	
	return mn;
}




MENU *corde_plot_menu(void)
{
	static MENU mn[32];
	static MENU *submn = NULL;

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"theor. dispersion relation",	        do_graph_dispersion_relation,	NULL, 0, NULL);
	add_item_to_menu(mn,"freq -> wavenumber in X",	    		do_corde_frequency_into_wavenumber,	NULL, 0, NULL);
	add_item_to_menu(mn,"\0",					0,				NULL, 0, NULL);
	add_item_to_menu(mn,"spectrum of string/cavity (simple)",	do_graph_spectre_simple,	NULL, 0, NULL);
	submn = corde_plot_theory1_submenu();
	add_item_to_menu(mn,"theory with piezos (v1)",		        0,							submn,0, NULL);
	submn = corde_plot_theory2_submenu();
	add_item_to_menu(mn,"theory with piezos (v2, 2008)",		0,							submn,0, NULL);
	add_item_to_menu(mn,"\0",					0,				NULL, 0, NULL);	
	add_item_to_menu(mn,"peak position and width (visible)",	do_find_peak_and_width,		NULL, CORDE_LOCAL, NULL);
	add_item_to_menu(mn,"peak position and width (all)",		do_find_peak_and_width,		NULL, CORDE_GLOBAL, NULL);
	add_item_to_menu(mn,"set parameters to find peaks",		    do_corde_peaks_set_parameters,	NULL, 0, NULL);
	add_item_to_menu(mn,"fit exp. response function at peaks",  do_corde_fit_response,	NULL, 0, NULL);
	add_item_to_menu(mn,"get value of ds at peaks",             do_corde_peaks_get_value,	NULL, 0, NULL);
	add_item_to_menu(mn,"\0",					0,				NULL, 0, NULL);	
	add_item_to_menu(mn,"isolate a mode (Hilbert transf.)",		do_corde_separate_modes,	NULL, CORDE_LOCAL, NULL);
	add_item_to_menu(mn,"isolate several modes",			    do_corde_separate_modes,	NULL, CORDE_GLOBAL, NULL);
	add_item_to_menu(mn,"fit all slopes",			            do_corde_fit_slopes,	     NULL, CORDE_GLOBAL, NULL);
	
	return(mn);
}

int	corde_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("corde", NULL, corde_plot_menu(), 0, NULL);
	return D_O_K;
}


int	corde_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu, "corde",	NULL, NULL);
	return D_O_K;
}
#endif

