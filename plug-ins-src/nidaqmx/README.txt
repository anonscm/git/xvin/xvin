adapted from:
http://forums.ni.com/ni/board/message?board.id=250&message.id=17557

When compiling in Windows,
Link with flag: -lnidaqmx

Put these files in your library path before compiling and linking.

The library file (NIDAQmx.lib) is from Windows NIDAQ version 8.3


#!/bin/sh

PATH="$PATH"

dumpbin /exports "C:\NIDAQmx.lib" > nidaq.exports

echo LIBRARY nicaiu.dll > nidaq.defs

echo EXPORTS >> nidaq.defs

sed "1,/ordinal *name/d;/^$/d;/Summary$/,+100d;s/^ \+\([^ ]\+\)\( .*\|\)$/\\1/;s/^_\(.*\)$/\1/" nidaq.exports |sort >> nidaq.defs

dlltool  -k -d nidaq.defs -l libnidaqmx.a c:/WINDOWS/system32/nicaiu.dll 