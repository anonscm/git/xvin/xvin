#include "cl_utils.h"
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include "bstrlib.h"

cl_device_id device = 0;
cl_context context = NULL;
cl_command_queue command_queue[256];
cl_int command_queue_count = 0;
size_t max_workgroup_size = 0;


cl_device_id get_device()
{
    return device;
}
cl_context get_context()
{
    return context;
}
cl_command_queue get_command_queue(int idx)
{
    if (idx < command_queue_count)
    {
        return command_queue[idx];
    }

    return NULL;
}

cl_command_queue get_htd_queue()
{
    return command_queue[0];
}
cl_command_queue get_dth_queue()
{
    return command_queue[1];
}
cl_command_queue get_dtd_queue()
{
    return command_queue[2];
}


size_t get_max_workgroup_size()
{
    return max_workgroup_size;
}
/**
 * @brief Reset OpenCl
 * All context will be erased.
 */
void reset_opencl()
{
    device = 0;
    for (int i = 0; i < command_queue_count; ++i)
    {
        clReleaseCommandQueue(command_queue[i]);
    }
    clReleaseContext(context);
    command_queue_count = 0;
}

int do_choose_plateform(void)
{
    cl_int  err;
    cl_uint numPlatforms = 0;   //the NO. of platforms
    cl_uint numDevices = 0;   //the NO. of platforms
    cl_platform_id platform = NULL; //the chosen platform
    cl_uint choosen_platform = 0;
    cl_uint choosen_device = 0;
    char buffer[10000] = {0};

    CLCHECK(clGetPlatformIDs(0, NULL, &numPlatforms));

    /*For clarity, choose the first available platform. */
    if (numPlatforms > 0)
    {
        cl_platform_id *platforms = (cl_platform_id *)malloc(numPlatforms * sizeof(cl_platform_id));
        CLCHECK(clGetPlatformIDs(numPlatforms, platforms, NULL));
        char name[100] = {0};
        CLCHECK(clGetPlatformInfo(platforms[0], CL_PLATFORM_NAME, sizeof(name), name, NULL));
        strcat(buffer, "Be sure no gpu task is running before changing platform\n\nChoose OpenCL platform :\n %R ");
        strcat(buffer, name);

        for (cl_uint i = 1; i < numPlatforms; ++i)
        {
            char name[100] = {0};
            CLCHECK(clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, sizeof(name), name, NULL));
            strcat(buffer, "\n%r ");
            strcat(buffer, name);
        }

        if (win_scanf(buffer, &choosen_platform) == CANCEL)
        {
            free(platforms);
            return CANCEL;
        }

        platform = platforms[choosen_platform];
        free(platforms);

    }
    else
    {
        win_printf("No OpenCl platform available");
        return CANCEL;
    }

    /*Step 2:Query the platform and choose the first GPU device if has one.Otherwise use the CPU as device.*/
    memset(buffer, 0, sizeof(buffer));
    CLCHECK(clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices));

    if (numDevices > 0)
    {
        cl_device_id *devices = (cl_device_id *) malloc(numDevices * sizeof(cl_device_id));

        CLCHECK(clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, numDevices, devices, NULL));
        char name[1024] = {0};
        CLCHECK(clGetDeviceInfo(devices[0], CL_DEVICE_NAME, sizeof(name), name, NULL));

        strcat(buffer, "Choose OpenCL device :\n %R ");
        strcat(buffer, name);

        for (cl_uint i = 1; i < numDevices; ++i)
        {
            char name[100] = {0};
            CLCHECK(clGetDeviceInfo(devices[i], CL_DEVICE_NAME, sizeof(name), name, NULL));
            strcat(buffer, "\n%r ");
            strcat(buffer, name);
        }

        free(devices);

        if (win_scanf(buffer, &choosen_device) == CANCEL)
        {
            return CANCEL;
        }
    }
    else
    {
        win_printf("No OpenCl device available");
        return CANCEL;
    }

    init_opencl(choosen_platform, choosen_device);
    return D_O_K;
}


/**
 * @brief Set the current initialize platform, device and the 3 command queue
 *
 * @param choosen_platform a valid platform ID
 * @param choosen_device a valid device ID
 */
void init_opencl(cl_uint choosen_platform, cl_uint choosen_device)
{
    cl_int  err;
    cl_uint numPlatforms = 0;   //the NO. of platforms
    cl_uint numDevices = 0;   //the NO. of platforms
    cl_platform_id platform = NULL; //the chosen platform
    char buffer[10000] = {0};
    cl_platform_id platforms[32] = {0};
    cl_device_id devices[32] = {0};

    reset_opencl();

    CLCHECK(clGetPlatformIDs(32, platforms, &numPlatforms));

    if (choosen_platform < numPlatforms && choosen_platform < 32)
    {
        platform = platforms[choosen_platform];
    }

    /*Step 2:Query the platform and choose the first GPU device if has one.Otherwise use the CPU as device.*/
    memset(buffer, 0, sizeof(buffer));
    CLCHECK(clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 32, devices, &numDevices));

    if (choosen_device < my_min(numDevices,32u))
    {
        device = devices[choosen_device];
    }

    context = clCreateContext(0, 1, &device, NULL, NULL, &err);
    CLCHECK(err);

    create_command_queue();
    create_command_queue();
    create_command_queue();

    clGetDeviceInfo(device,
                    CL_DEVICE_MAX_WORK_GROUP_SIZE,
                    sizeof(max_workgroup_size),
                    &max_workgroup_size,
                    NULL);

    set_config_int("OPENCL", "LAST-PLATFORM", choosen_platform);
    set_config_int("OPENCL", "LAST-DEVICE", choosen_device);
}

void init_opencl_last_config()
{
    cl_uint default_platform = get_config_int("OPENCL", "LAST-PLATFORM", 0);
    cl_uint default_device = get_config_int("OPENCL", "LAST-DEVICE", 0);

    init_opencl(default_platform, default_device);
}

char *load_opencl_file(const char *filename)
{
    char unix_filename[512] = {0};
    char *str;
    printf("%s\n", filename);
    FILE *input_file = fopen(filename, "r");

    strncpy(unix_filename, filename, sizeof(unix_filename));
    backslash_to_slash(unix_filename);

    if (input_file == NULL)
        return NULL;

    bstring file_contents = bread((bNread) fread, input_file);
    fclose(input_file);

    struct bstrList  *lines = bsplit(file_contents, '\n');
    btrunc(file_contents, 0);

    for (int lineNo = 0; lineNo < lines->qty; ++lineNo)
    {
        bstring line = lines->entry[lineNo];

        bstring prefline = bformat("#line %d \"%s\"\n %s \n", lineNo + 1, unix_filename, line->data);

        bconcat(file_contents, prefline);
        bdestroy(prefline);
    }

    bstrListDestroy(lines);

    str = bstr2cstr(file_contents, '.');
    bdestroy(file_contents);
    return str;
}

cl_command_queue create_command_queue()
{
    cl_int err;
    cl_command_queue cq;
    cq = clCreateCommandQueue(context, device, 0, &err);
    CLCHECK(err);
    command_queue[command_queue_count] = cq;
    ++command_queue_count;
    return cq;
}

// Helper function to get error string
// *********************************************************************

#ifdef OLD
const char *oclErrorString(cl_int error)
{
    static const char *errorString[] =
    {
        "CL_SUCCESS",
        "CL_DEVICE_NOT_FOUND",
        "CL_DEVICE_NOT_AVAILABLE",
        "CL_COMPILER_NOT_AVAILABLE",
        "CL_MEM_OBJECT_ALLOCATION_FAILURE",
        "CL_OUT_OF_RESOURCES",
        "CL_OUT_OF_HOST_MEMORY",
        "CL_PROFILING_INFO_NOT_AVAILABLE",
        "CL_MEM_COPY_OVERLAP",
        "CL_IMAGE_FORMAT_MISMATCH",
        "CL_IMAGE_FORMAT_NOT_SUPPORTED",
        "CL_BUILD_PROGRAM_FAILURE",
        "CL_MAP_FAILURE",
        "CL_MISALIGNED_SUB_BUFFER_OFFSET",
        "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL UNKNOWN ERROR",
        "CL_INVALID_VALUE",
        "CL_INVALID_DEVICE_TYPE",
        "CL_INVALID_PLATFORM",
        "CL_INVALID_DEVICE",
        "CL_INVALID_CONTEXT",
        "CL_INVALID_QUEUE_PROPERTIES",
        "CL_INVALID_COMMAND_QUEUE",
        "CL_INVALID_HOST_PTR",
        "CL_INVALID_MEM_OBJECT",
        "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR",
        "CL_INVALID_IMAGE_SIZE",
        "CL_INVALID_SAMPLER",
        "CL_INVALID_BINARY",
        "CL_INVALID_BUILD_OPTIONS",
        "CL_INVALID_PROGRAM",
        "CL_INVALID_PROGRAM_EXECUTABLE",
        "CL_INVALID_KERNEL_NAME",
        "CL_INVALID_KERNEL_DEFINITION",
        "CL_INVALID_KERNEL",
        "CL_INVALID_ARG_INDEX",
        "CL_INVALID_ARG_VALUE",
        "CL_INVALID_ARG_SIZE",
        "CL_INVALID_KERNEL_ARGS",
        "CL_INVALID_WORK_DIMENSION",
        "CL_INVALID_WORK_GROUP_SIZE",
        "CL_INVALID_WORK_ITEM_SIZE",
        "CL_INVALID_GLOBAL_OFFSET",
        "CL_INVALID_EVENT_WAIT_LIST",
        "CL_INVALID_EVENT",
        "CL_INVALID_OPERATION",
        "CL_INVALID_GL_OBJECT",
        "CL_INVALID_BUFFER_SIZE",
        "CL_INVALID_MIP_LEVEL",
        "CL_INVALID_GLOBAL_WORK_SIZE",
        "CL_INVALID_PROPERTY",
    };

    const int errorCount = sizeof(errorString) / sizeof(errorString[0]);

    const int index = -error;

    return (index >= 0 && index < errorCount) ? errorString[index] : "";

}
#endif

// Some platform does not have the flags...
#ifndef CL_INVALID_PROPERTY
#define CL_INVALID_PROPERTY 1 // 1 is never use, since values are negative
#endif

const char *oclErrorString(cl_int err)
{

    switch (err)
    {
    case CL_SUCCESS:
        return "CL_SUCCESS";

    case CL_DEVICE_NOT_FOUND:
        return "CL_DEVICE_NOT_FOUND";

    case CL_DEVICE_NOT_AVAILABLE:
        return "CL_DEVICE_NOT_AVAILABLE";

    case CL_COMPILER_NOT_AVAILABLE:
        return "CL_COMPILER_NOT_AVAILABLE";

    case CL_MEM_OBJECT_ALLOCATION_FAILURE:
        return "CL_MEM_OBJECT_ALLOCATION_FAILURE";

    case CL_OUT_OF_RESOURCES:
        return "CL_OUT_OF_RESOURCES";

    case CL_OUT_OF_HOST_MEMORY:
        return "CL_OUT_OF_HOST_MEMORY";

    case CL_PROFILING_INFO_NOT_AVAILABLE:
        return "CL_PROFILING_INFO_NOT_AVAILABLE";

    case CL_MEM_COPY_OVERLAP:
        return "CL_MEM_COPY_OVERLAP";

    case CL_IMAGE_FORMAT_MISMATCH:
        return "CL_IMAGE_FORMAT_MISMATCH";

    case CL_IMAGE_FORMAT_NOT_SUPPORTED:
        return "CL_IMAGE_FORMAT_NOT_SUPPORTED";

    case CL_BUILD_PROGRAM_FAILURE:
        return "CL_BUILD_PROGRAM_FAILURE";

    case CL_MAP_FAILURE:
        return "CL_MAP_FAILURE";

    case CL_MISALIGNED_SUB_BUFFER_OFFSET:
        return "CL_MISALIGNED_SUB_BUFFER_OFFSET";

    case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST:
        return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";

    case CL_INVALID_VALUE:
        return "CL_INVALID_VALUE";

    case CL_INVALID_DEVICE_TYPE:
        return "CL_INVALID_DEVICE_TYPE";

    case CL_INVALID_PLATFORM:
        return "CL_INVALID_PLATFORM";

    case CL_INVALID_DEVICE:
        return "CL_INVALID_DEVICE";

    case CL_INVALID_CONTEXT:
        return "CL_INVALID_CONTEXT";

    case CL_INVALID_QUEUE_PROPERTIES:
        return "CL_INVALID_QUEUE_PROPERTIES";

    case CL_INVALID_COMMAND_QUEUE:
        return "CL_INVALID_COMMAND_QUEUE";

    case CL_INVALID_HOST_PTR:
        return "CL_INVALID_HOST_PTR";

    case CL_INVALID_MEM_OBJECT:
        return "CL_INVALID_MEM_OBJECT";

    case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:
        return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";

    case CL_INVALID_IMAGE_SIZE:
        return "CL_INVALID_IMAGE_SIZE";

    case CL_INVALID_SAMPLER:
        return "CL_INVALID_SAMPLER";

    case CL_INVALID_BINARY:
        return "CL_INVALID_BINARY";

    case CL_INVALID_BUILD_OPTIONS:
        return "CL_INVALID_BUILD_OPTIONS";

    case CL_INVALID_PROGRAM:
        return "CL_INVALID_PROGRAM";

    case CL_INVALID_PROGRAM_EXECUTABLE:
        return "CL_INVALID_PROGRAM_EXECUTABLE";

    case CL_INVALID_KERNEL_NAME:
        return "CL_INVALID_KERNEL_NAME";

    case CL_INVALID_KERNEL_DEFINITION:
        return "CL_INVALID_KERNEL_DEFINITION";

    case CL_INVALID_KERNEL:
        return "CL_INVALID_KERNEL";

    case CL_INVALID_ARG_INDEX:
        return "CL_INVALID_ARG_INDEX";

    case CL_INVALID_ARG_VALUE:
        return "CL_INVALID_ARG_VALUE";

    case CL_INVALID_ARG_SIZE:
        return "CL_INVALID_ARG_SIZE";

    case CL_INVALID_KERNEL_ARGS:
        return "CL_INVALID_KERNEL_ARGS";

    case CL_INVALID_WORK_DIMENSION:
        return "CL_INVALID_WORK_DIMENSION";

    case CL_INVALID_WORK_GROUP_SIZE:
        return "CL_INVALID_WORK_GROUP_SIZE";

    case CL_INVALID_WORK_ITEM_SIZE:
        return "CL_INVALID_WORK_ITEM_SIZE";

    case CL_INVALID_GLOBAL_OFFSET:
        return "CL_INVALID_GLOBAL_OFFSET";

    case CL_INVALID_EVENT_WAIT_LIST:
        return "CL_INVALID_EVENT_WAIT_LIST";

    case CL_INVALID_EVENT:
        return "CL_INVALID_EVENT";

    case CL_INVALID_OPERATION:
        return "CL_INVALID_OPERATION";

    case CL_INVALID_GL_OBJECT:
        return "CL_INVALID_GL_OBJECT";

    case CL_INVALID_BUFFER_SIZE:
        return "CL_INVALID_BUFFER_SIZE";

    case CL_INVALID_MIP_LEVEL:
        return "CL_INVALID_MIP_LEVEL";

    case CL_INVALID_GLOBAL_WORK_SIZE:
        return "CL_INVALID_GLOBAL_WORK_SIZE";

    case CL_INVALID_PROPERTY:
        return "CL_INVALID_PROPERTY";

    default:
        return "UNKNOWN CL ERROR CODE";
    }
}


