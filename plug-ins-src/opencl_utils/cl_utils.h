#pragma once
#include "xvin.h"
#include "glsw.h"
#include <CL/opencl.h>

#ifndef NOCOLOR
		#define RED   "\033[31m"
		#define GREEN "\033[32m"
		#define BLUE  "\033[34m"
		#define BOLD  "\033[1m"
		#define UND   "\033[4m"
		#define ENDS  "\033[0m"
#else
		#define RED   ""
		#define GREEN ""
		#define BLUE  ""
		#define BOLD  ""
		#define UND   ""
		#define ENDS  ""
#endif

#define CLCHECK(e) do { cl_int err__ = (e); if(err__ != CL_SUCCESS) { printf(RED "[OPENCL][error]" ENDS "\n\tCalling:\t"#e "\n\tAt:\t\t" __FILE__ ":%d \n\tError Code:\tn°%d - %s\n", __LINE__, err__,oclErrorString(err__));}} while(0)

PXV_FUNC(cl_device_id, get_device, (void));
PXV_FUNC(cl_context, get_context, (void));
PXV_FUNC(cl_command_queue, get_command_queue, (cl_int idx));
PXV_FUNC(size_t, get_max_workgroup_size,(void));
PXV_FUNC(void, init_opencl, (cl_uint choosen_platform, cl_uint choosen_device));
PXV_FUNC(cl_kernel, prepare_kernel, (const char *kernel_name, const char *kernel_source, const char * flags));
PXV_FUNC(void, init_opencl_last_config, (void));
PXV_FUNC(cl_command_queue, create_command_queue, (void));
PXV_FUNC(const char*, oclErrorString,(cl_int error));
PXV_FUNC(cl_command_queue, get_htd_queue, ());
PXV_FUNC(cl_command_queue, get_dth_queue, ());
PXV_FUNC(cl_command_queue, get_dtd_queue, ());
PXV_FUNC(char *,load_opencl_file,(const char * filename));
PXV_FUNC(int, do_choose_plateform, (void));
PXV_FUNC(void, reset_opencl, (void));
