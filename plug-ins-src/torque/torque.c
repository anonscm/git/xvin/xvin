/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _TORQUE_C_
#define _TORQUE_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 
#include "cardinal.h"
# include "xvplot.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "torque.h"

extern int last_x_index;
extern int last_y_index;

int do_coil_filter(void)
{
	register int i, j;
	O_p *op = NULL, *opn = NULL;
	int nf;
	static float factor = 1;
	float tmp;
	d_s *ds, *dsi;
	pltreg *pr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the y coordinate"
	"of a data set by a number and place it in a new plot");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("cannot find data");
	nf = dsi->nx;	/* this is the number of points in the data set */
	i = win_scanf("What is the memory factor %f",&factor);
	if (i == CANCEL)	return OFF;

	if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds = opn->dat[0];

	for (j = 0, tmp = dsi->yd[j]; j < nf; j++)
	{
		tmp = ds->yd[j] = factor * tmp + (1-factor) * dsi->yd[j];
		ds->xd[j] = dsi->xd[j];
	}

	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement,"y multiplied by %f",factor);
	set_plot_title(opn, "Multiply by %f",factor);
	if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
	if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
	opn->filename = Transfer_filename(op->filename);
	uns_op_2_op(opn, op);
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}


int do_generate_signal(void)
{
	register int i, j, k;
	O_p  *opn = NULL;
	int nf = 4096; // total number of points
	int np = 128;  // nb of points in one period
	int ninv = 256; //number of point in half period
	int n0 = 32; //starting point 
	double phi;
	d_s *ds;
	pltreg *pr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine generates a sinus signal with inverting"
	"phase as in the torque experiment");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
		return win_printf_OK("cannot find data");

	i = win_scanf("Signal generator over %6d points\n"
		      "number of points per period %4d\n"
		      "number of points before frequency reversal %4d\n"
		      "number of points for starting %4d\n"
		      ,&nf,&np,&ninv,&n0);
	if (i == CANCEL)	return OFF;

	if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds = opn->dat[0];

	for (j = 0, phi = 0; j < nf+n0; j++)
	{
	  k = (ninv+j-n0) / ninv;
	  phi += (k%2 == 0) ? (M_PI*2)/np : -(M_PI*2)/np;
	  if (j >= n0)
	    {
	      ds->yd[j-n0] = cos(phi);
	      ds->xd[j-n0] = j-n0;
	    }
	}

	/* now we must do some house keeping */
	ds->source = my_sprintf(ds->source,"Frequency generator");
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

int find_R_of_coil(void)
{
	register int i, j;
	O_p  *opn = NULL, *op;
	int niter = 1024, ndas; //starting point 
	float rmin = 1., rmax = 100., ymin = 0., rbest = 0., moy;
	d_s *ds, *dsi, *dsn;
	pltreg *pr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine try to find the resistance of a LR circuit knowing the volatage ds0 and the current ds1");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
	ndas = op->cur_dat +1;
	i = win_scanf("Data set number for Ri data %4d\n"
		      "number of iteration %6d points\n"
		      "between Rmin  %6f\n"
		      "and Rmax %6f\n"
		      ,&ndas,&niter,&rmin,&rmax);
	if (i == CANCEL)	return OFF;
	dsi = op->dat[ndas];

	if ((opn = create_and_attach_one_plot(pr, niter, niter, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	dsn = opn->dat[0];

	for (j = 0; j < niter; j++)
	{
	  dsn->xd[j] = rmin + ((rmax - rmin)*j)/niter;
	  for (i = 0; i < ds->nx; i++)
	    {
	      dsn->yd[j] += (ds->yd[i] - (dsn->xd[j] * dsi->yd[i])) * (ds->yd[i] - (dsn->xd[j] * dsi->yd[i]));
	    }
	  if (j == 0 || dsn->yd[j] < ymin)
	    {
	      ymin = dsn->yd[j];
	      rbest = dsn->xd[j];
	    }
	}
	/* now we must do some house keeping */
	dsn->source = my_sprintf(ds->source,"Best R");
	set_plot_title(opn,"Minimum at %g",rbest);
   if ((dsn = create_and_attach_one_ds(op, ds->nx, ds->nx, 0)) == NULL)
     return win_printf_OK("I can't create plot !");
   set_ds_source(dsn, "Coil voltage");
   for (i = 0, moy = 0; i < ds->nx; i++)
     {
       dsn->yd[i] = ds->yd[i] - (rbest * dsi->yd[i]);
       moy += dsn->yd[i];
       dsn->xd[i] = ds->xd[i];
     }
   if (ds->nx > 0) moy /= ds->nx;
   for (i = 0; i < ds->nx; i++)
     dsn->yd[i] -= moy;
   refresh_plot(pr, UNCHANGED);
   return D_O_K;
}
int draw_R_vs_L(void)
{
  register int i;
  O_p  *opn = NULL, *op;
  static int  ndas; //starting point 
  static float rbest, moy;
  d_s *ds, *dsi, *dsn;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine try to find the resistance of a LR circuit knowing the volatage ds0 and the current ds1");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("cannot find data");
  ndas = op->cur_dat +1;
  i = win_scanf("Data set number for Ri data %4d\n"
		"best R %6f\n"
		,&ndas,&rbest);
  if (i == CANCEL)	return OFF;
  dsi = op->dat[ndas];

  if ((opn = create_and_attach_one_plot(pr, ds->nx, ds->nx, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsn = opn->dat[0];
  set_ds_source(dsn, "Coil voltage");
  for (i = 0, moy = 0; i < ds->nx; i++)
    {
      dsn->yd[i] = ds->yd[i] - (rbest * dsi->yd[i]);
      moy += dsn->yd[i];
      dsn->xd[i] = ds->xd[i];
    }
  if (ds->nx > 0) moy /= ds->nx;
  for (i = 0; i < ds->nx; i++)
    dsn->yd[i] -= moy;
  for (i = 0, moy = 0; i < ds->nx; i++)
    {
      moy += dsn->yd[i];
      dsn->yd[i] = moy;
      dsn->xd[i] = rbest * dsi->yd[i];
    }


  refresh_plot(pr, UNCHANGED);
   return D_O_K;
}

int simulate_torque(void)
{
  register int i;
  O_p  *opn = NULL;
  static int nf = 4096; // total number of points
  static int ninv = 256; //number of point in half period
  d_s *ds;
  int dir = 1;
  pltreg *pr = NULL;
  double k1x,k2x,k3x,k4x;
  double k1v, k2v, k3v, k4v;
  double P = 5;
  static float x0 = 0, P0 = 5;
  double x = 0; // position boussole 
  double v = 0; // phase of the rotating field
  static int np = 64;
  double h = 1/np;
  double hpi =  2 * M_PI/np;	




  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine generates a sinus signal with inverting"
			   "phase as in the torque experiment");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");
  
  i = win_scanf("Torque simulation over %6d points\n"
		"strength of the rotating field %6f\n"
		"starting angular position %6f\n"
		"number of points per period %4d\n"
		"number of points before frequency reversal %4d\n"
		//"number of points for starting %4d\n"
		,&nf,&P0,&x0,&np,&ninv);
  if (i == CANCEL)	return OFF;

  h = ((double)1)/np;
  hpi =  2 * M_PI/np;	
  P = P0;
  x = x0;
  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  for (i = 0; i < ds->nx; i++)
    {
      dir = ((i/ninv)%2 == 0) ? 1 : -1;
      //ds->xd[i] = -P*sin(x-v)/(2*M_PI);
      ds->yd[i] = x/M_PI;
      //ds->xd[i] = v/(M_PI*2);
      ds->xd[i] = (float)i/np;
      k1v = dir*hpi;
      k1x = -h * (P * sin (2*(x-v)));
      
      k2v = dir*hpi;
      k2x = -h * (P * sin (2*((x+k1x/2) - (v+k1v/2))));
      
      k3v = dir*hpi;
      k3x = -h * (P * sin (2*((x+k2x/2) - (v+k2v/2))));
      
      k4v = dir*hpi;			
      k4x = -h * (P * sin (2*((x+k3x) - (v+k3v))));
      
      x = x + ((k1x+2*k2x+2*k3x+k4x)/6);
      v = v + ((k1v+2*k2v+2*k3v+k4v)/6);
    }
  ds->source = my_sprintf(ds->source,"Simul torque");
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int simulate_torque_happo(void)
{
  register int i, j, k;
  O_p  *opn = NULL;
  static int nf = 4096; // total number of points
  static int ninv = 256; //number of point in half period
  int nx; 
  d_s *ds, *ds2;
  int dir = 1;
  pltreg *pr = NULL;
  double k1x,k2x,k3x,k4x;
  double k1v, k2v, k3v, k4v;
  double P = 5, amp;
  static float x0 = 0, P0 = 5;
  double x = 0; // position boussole 
  double v = 0; // phase of the rotating field
  static int np = 64, fp = 0;
  double h = 1/np;
  double hpi =  2 * M_PI/np;	




  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine generates a sinus signal with inverting"
			   "phase as in the torque experiment");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");
  
  i = win_scanf("Torque simulation over %6d points\n"
		"strength of the rotating field %6f\n"
		"starting angular position %6f\n"
		"number of points per period %4d\n"
		"number of points before frequency reversal %4d\n"
		"one point per full period %b\n"
		//"number of points for starting %4d\n"
		,&nf,&P0,&x0,&np,&ninv,&fp);
  if (i == CANCEL)	return OFF;

  h = ((double)1)/np;
  hpi =  2 * M_PI/np;	
  P = P0;
  x = x0;
  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  if ((ds2 = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
     return win_printf_OK("I can't create plot !");
  set_ds_source(ds2, "Coil voltage");

  nx = (fp == 0) ? nf : nf * ninv * 2;
  for (i = 0; i < nx; i++)
    {
      dir = ((i/ninv)%2 == 0) ? 1 : -1;
      amp = (float)((i%ninv)*2*M_PI)/ninv;
      amp = 1 - cos(amp);
      //amp = 1;
      j = (fp == 0) ? i : i / (ninv * 2);
      k = (fp == 0) ? 1 :  (ninv * 2);

      ds2->yd[j] = amp;
      ds->yd[j] += x/(M_PI*k);
      ds->xd[j] = ds2->xd[j] = (float)i/np;

      k1v = dir*hpi;
      k1x = -h * (P * amp * sin (2*(x-v)));
      //k1x = -h * (P * amp * sin (x));
      
      k2v = dir*hpi;
      k2x = -h * (P * amp * sin (2*((x+k1x/2) - (v+k1v/2))));
      //k2x = -h * (P * amp * sin (x+k1x/2));
      k3v = dir*hpi;
      k3x = -h * (P * amp * sin (2*((x+k2x/2) - (v+k2v/2))));
      //k3x = -h * (P * amp * sin (x+k2x/2));

      k4v = dir*hpi;			
      k4x = -h * (P * amp * sin (2*((x+k3x) - (v+k3v))));
      //k4x = -h * (P * amp * sin (x+k3x));

      x = x + ((k1x+2*k2x+2*k3x+k4x)/6);
      v = v + ((k1v+2*k2v+2*k3v+k4v)/6);
    }
  ds->source = my_sprintf(ds->source,"Simul torque");
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int simulate_torque_fra_happo(void)
{
  register int i, j, k;
  O_p  *opn = NULL;
  static int nf = 4096; // total number of points
  static int ninv = 256; //number of point in half period
  int nx;
  d_s *ds, *ds2;
  int dir = 1;
  pltreg *pr = NULL;
  double k1x,k2x,k3x,k4x;
  double k1v, k2v, k3v, k4v;
  double P = 5, amp;
  static float x0 = 0, P0 = 5;
  double x = 0; // position boussole 
  double v = 0; // phase of the rotating field
  static int np = 64, fp = 0;
  double h = 1/np;
  double hpi =  2 * M_PI/np;	
  static int power = 6;



  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine generates a sinus signal with inverting"
			   "phase as in the torque experiment");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");
  
  i = win_scanf("Torque simulation over %6d points\n"
		"strength of the rotating field %6f\n"
		"starting angular position %6f\n"
		"number of points per period %4d\n"
		"number of points before frequency reversal %4d\n"
		"one point per full period %b\n"
		"power for apolisation function (use only 2,4,6,8)%d\n"
		//"number of points for starting %4d\n"
		,&nf,&P0,&x0,&np,&ninv,&fp,&power);
  if (i == CANCEL)	return OFF;

  h = ((double)1)/np;
  hpi =  2 * M_PI/np;	
  P = P0;
  x = x0;
  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  if ((ds2 = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
     return win_printf_OK("I can't create plot !");
  set_ds_source(ds2, "Coil voltage");

  nx = (fp == 0) ? nf : nf * ninv * 2;
  for (i = 0; i < nx; i++)
    {
      dir = ((i/ninv)%2 == 0) ? 1 : -1;
      amp = (float)((i%ninv)*M_PI)/ninv;
      amp = 2*(1 - pow(cos(amp),power))/(1 + pow(cos(amp),power));
      //amp = 1;
      j = (fp == 0) ? i : i / (ninv * 2);
      k = (fp == 0) ? 1 :  (ninv * 2);

      ds2->yd[j] = amp;
      ds->yd[j] += x/(M_PI*k);
      ds->xd[j] = ds2->xd[j] = (float)i/np;

      k1v = dir*hpi;
      k1x = -h * (P * amp * sin (2*(x-v)));
      //k1x = -h * (P * amp * sin (x));
      
      k2v = dir*hpi;
      k2x = -h * (P * amp * sin (2*((x+k1x/2) - (v+k1v/2))));
      //k2x = -h * (P * amp * sin (x+k1x/2));
      k3v = dir*hpi;
      k3x = -h * (P * amp * sin (2*((x+k2x/2) - (v+k2v/2))));
      //k3x = -h * (P * amp * sin (x+k2x/2));

      k4v = dir*hpi;			
      k4x = -h * (P * amp * sin (2*((x+k3x) - (v+k3v))));
      //k4x = -h * (P * amp * sin (x+k3x));

      x = x + ((k1x+2*k2x+2*k3x+k4x)/6);
      v = v + ((k1v+2*k2v+2*k3v+k4v)/6);
    }
  ds->source = my_sprintf(ds->source,"Simul torque");
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int extract_angle_and_cm_from_traj(void)
{
  int i;
  O_p  *op;
  d_s *dsbt1, *dsbt2, *dsa, *dsan, *dscmx,*dscmy ;
  pltreg *pr = NULL;
  float a = 0, ao = 0;
  int nl = 0;
  int np;

 if(updating_menu_state != 0)	return D_O_K;
 if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsbt1) != 3)
   return win_printf_OK("cannot find data");
 if((dsbt2 = op->dat[1]) == NULL)  return win_printf_OK("cannot find data");
  
 np = (dsbt1->nx > dsbt2->nx) ? dsbt1->nx : dsbt2->nx;

 if((op = create_and_attach_one_plot(pr,np,np,0)) == NULL)
   return win_printf_OK("I can't create plot !");
 dscmx = op->dat[0];
 set_ds_source(dscmx, "CM motion X");

 if((dscmy = create_and_attach_one_ds(op,np,np,0)) == NULL)
   return win_printf_OK("I can't create dscmy !");
 set_ds_source(dscmy, "CM motion Y");

 if((op = create_and_attach_one_plot(pr,np,np,0)) == NULL)
   return win_printf_OK("I can't create plot !");

 dsa = op->dat[0];
 set_ds_source(dsa, "angle motion");
 
 if((op = create_and_attach_one_plot(pr,np,np,0)) == NULL)
   return win_printf_OK("I can't create plot !");
 
 dsan = op->dat[0];
 set_ds_source(dscmy, "angle motion no loop");

 
 for(i = 0; i< np;i++){
   dscmx->xd[i] = dscmy->xd[i] = dsa->xd[i] = dsan->xd[i] = i;
   dscmx->yd[i] = (dsbt1->xd[i] + dsbt2->xd[i])/2;
   dscmy->yd[i] = (dsbt1->yd[i] + dsbt2->yd[i])/2;
   a = dsa->yd[i] = dsan->yd[i] = atan2(dsbt1->yd[i] - dsbt2->yd[i],dsbt1->xd[i] - dsbt2->xd[i]);
   if(ao - a > 3.*PI/2){
     nl++;
   }
   if(ao - a < -3.*PI/2){
     nl--;
   }
   dsa->yd[i] += nl*2*PI;
   ao = a;
 }

 return D_O_K;
}


int treat_stepped_angle(void)
{
  int i;
  O_p  *op, *opn;
  d_s *ds, *dsn;
  pltreg *pr = NULL;
  int np;
  int freq = 25;
  int ns = 5;
  int nf = ns*freq;
  int nsteps = 12;
  int angden = 6;
  float angstp =PI/angden;
  int step = 0;
  int sign = -1;

 if(updating_menu_state != 0)	return D_O_K;
 if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
   return win_printf_OK("cannot find data");

  i = win_scanf("freq = %5d\n"
		"ns = %5d\n"
		"nsteps %5d\n"
		"angstp = PI/%5d\n"
		"sign = %5d\n",&freq,&ns,&nsteps,&angden,&sign);
  if (i == CANCEL)	return OFF;   

 np = ds->nx;
 nf = ns*freq;
 angstp = PI/(float)angden;
 if((opn = create_and_attach_one_plot(pr,np,np,0)) == NULL)
   return win_printf_OK("I can't create plot !");

 dsn = opn->dat[0];
 set_ds_source(dsn, "treated angle");

 for(i = 0; i< np ;i++){
   if(i%nf == 0)
     step = step + sign;
   if(i%(nsteps*nf) == 0)
     sign = (sign == 1) ? -1 : 1;
   dsn->xd[i] = ds->xd[i];
   dsn->yd[i] = ds->yd[i] + step*angstp;
 }

 refresh_plot(pr, UNCHANGED);

 return D_O_K;
}


int average_continuous_angle(void)
{
  int i;
  O_p  *op;
  d_s *ds, *dsn;
  pltreg *pr = NULL;
  int np;
  int factor = 126250;
  int den;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("cannot find data");

  i = win_scanf("nb points = %d\n",&factor);
  if (i == CANCEL)	return OFF;   

  np = ds->nx;
  den = np/factor;
  np = den*factor;
  if((dsn = create_and_attach_one_ds(op,factor,factor,0)) == NULL)
    return win_printf_OK("I can't create ds !");

  set_ds_source(dsn, "averaged angle");

  for(i = 0; i< np ;i++){
    dsn->xd[i%factor] = ds->xd[i%factor];
    dsn->yd[i%factor] += ds->yd[i]/(den);
  }

  refresh_plot(pr, UNCHANGED);

  return D_O_K;
}


int throw_away_points(void)
{
  int i;
  O_p  *op;
  d_s *ds, *dsn;
  pltreg *pr = NULL;
  int np, nnp;
  int keep = 1;
  int total = 4;
  int start = 2;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("cannot find data");

  i = win_scanf("keep %4d every %4d, starting from point %d\n"
      ,&keep,&total,&start);
  if (i == CANCEL)	return OFF;   

  np = ds->nx;
  nnp = np/total;
  if((dsn = create_and_attach_one_ds(op,nnp,nnp,0)) == NULL)
    return win_printf_OK("I can't create ds !");

  set_ds_source(dsn, "averaged angle");

  for(i = start; i< np ;i++){
    if((i-start)%total == 0){
      dsn->xd[(i-start)/total] = ds->xd[i];
      dsn->yd[(i-start)/total] = ds->yd[i];
    }
  }

  refresh_plot(pr, UNCHANGED);

  return D_O_K;
}


int crop_dataset(void)
{
  int i, k=0, t=0;
  int keep = 125000, throw = 1250;
  O_p  *op;
  d_s *ds, *dsn;
  pltreg *pr = NULL;
  int np;
  
  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("cannot find data");

  i = win_scanf("keep %4d points \n"
		"then trow away %4d points\n"
      ,&keep,&throw);
  if (i == CANCEL)	return OFF;   

  np = ds->nx;
  if((dsn = create_and_attach_one_ds(op,np,np,0)) == NULL)
    return win_printf_OK("I can't create ds !");

  set_ds_source(dsn, "cropped ds");
  k = 0, t = 0;
  while(i < np){
    for(k = 0; k < keep ;k++){
      if(i == np) break;
      dsn->xd[i-t*throw] = ds->xd[i];
      dsn->yd[i-t*throw] = ds->yd[i];
      i++;
    }
    for(k = 0; k < throw ;k++)
      i++;
    t++;
  }
  np = np-t*throw;
  //dsn = build_adjust_data_set(dsn,np,np);
  refresh_plot(pr, UNCHANGED);

  return D_O_K;
}


int analyse_ellinc_dataset(void)
{
  int i, k=0, n = 0;
  int keep = 125000, throw = 1250, xoffs = 500;
  O_p  *op, *opn;
  d_s *ds,*dsi, *dsn;
  float ell = -0.01;
  float ellinc = -0.001;
  int periods = 5;
  pltreg *pr = NULL;
  int np, nnp;
  
  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2 
      || ((dsi = op->dat[0]) == NULL)
      || ((ds = op->dat[1]) == NULL))
    return win_printf_OK("cannot find data");

  i = win_scanf("keep %8d points \n"
      "then trow away %8d points\n"
      "number of periods to average %5d\n"
      "starting ell %5f\n"
      "ellinc %5f\n"
      "xoffset %5d\n"
      ,&keep,&throw,&periods,&ell,&ellinc,&xoffs);
  if (i == CANCEL)	return OFF;

  np = ds->nx;
  nnp = keep/periods;

  if((opn = create_and_attach_one_plot(pr,nnp,nnp,0)) == NULL)
    return win_printf_OK("I can't create ds !");
  n = 0;
  while(i < np){
    if((dsn = create_and_attach_one_ds(opn,nnp,nnp,0)) == NULL)
      return win_printf_OK("I can't create ds !");
    set_ds_source(dsn, "O-I ell = %f",ell + n*ellinc);

    for(k = 0 ; k < keep ; k++){
      if(i == np) break;
      dsn->xd[k%nnp] = ds->xd[k%nnp];
      dsn->yd[k%nnp] += ds->yd[i]/periods;
      i++;
    }
    for(k = 0 ; k < nnp ; k++)
      dsn->yd[k] -= dsi->yd[k];

    for(k = 0; k < throw ;k++)
      i++;
    n++;
  }
  refresh_plot(pr, UNCHANGED);

  return D_O_K;
}

int catenate_2ds(void)
{
  int k=0;
  O_p  *op;
  d_s *ds1,*ds2, *dsn;
  pltreg *pr = NULL;
  int np1, np2, np;
  
  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2 
      || ((ds1 = op->dat[1]) == NULL)
      || ((ds2 = op->dat[2]) == NULL))
    return win_printf_OK("cannot find data");

  np1 = ds1->nx;
  np2 = ds2->nx;
  np = np1 + np2;

  if((dsn = create_and_attach_one_ds(op,np,np,0)) == NULL)
      return win_printf_OK("I can't create ds !");
  set_ds_source(dsn, "%s + %s",ds1->source,ds2->source);
  for(k = 0 ; k < np1 ; k++){
    dsn->xd[k] = ds1->xd[k];
    dsn->yd[k] = ds1->yd[k];
  }
  for(k = 0 ; k < np2 ; k++){
    dsn->xd[k+np1] = ds2->xd[k];
    dsn->yd[k+np1] = ds2->yd[k];
  }
  
  refresh_plot(pr, UNCHANGED);

  return D_O_K;
}

/** This function Uses the plots generated by the mod_rot routine in trackBead 
 * to extract the angle of rotation.

\author Francesco Mosconi
 */
int torque_inspect_mod_rot(void)
{
  register int i;
  char f_line[512];
  FILE *fpi = NULL;
  pltreg  *pr;
  O_p *opi, *opbd0, *opbd1;
  int bd0, bd1, irepeat, mod_rot;
  int nrepeat;

  if(updating_menu_state != 0)    return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&opi) != 2)
  {
    win_printf("cannot find data");
    return OFF;
  }
  if (sscanf(opi->filename,"X(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",&bd0,&mod_rot,&irepeat) != 3)
    return win_printf_OK("Francesco you did not load a mod_rot file!\n%s",opi->filename);
  bd1 = bd0 + 1;
  sprintf(f_line,"%sX(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",opi->dir,bd1,mod_rot,irepeat);
  fpi = fopen(f_line,"r");
  if (fpi == NULL)
  {
    bd1 = bd0 - 1;
    sprintf(f_line,"%sX(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",opi->dir,bd1,mod_rot,irepeat);
    fpi = fopen(f_line,"r");
  }
  if (fpi == NULL)
    return win_printf_OK("Francesco I cannot find your second bead file!\n");
  fclose(fpi);
  for (nrepeat = irepeat+1; ;nrepeat++)
  {
    sprintf(f_line,"%sX(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",opi->dir,bd0,mod_rot,nrepeat);
    fpi = fopen(f_line,"r");
    if (fpi == NULL)  break;
    fclose(fpi);
    sprintf(f_line,"%sX(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",opi->dir,bd1,mod_rot,nrepeat);
    fpi = fopen(f_line,"r");
    if (fpi == NULL)  break;
    fclose(fpi);
  }

  for (i = irepeat; i < nrepeat; i++)
  {
    sprintf(f_line,"X(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",bd0,mod_rot,i);
    opbd0 = create_plot_from_gr_file(f_line, opi->dir);
    sprintf(f_line,"X(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",bd1,mod_rot,i);
    opbd1 = create_plot_from_gr_file(f_line, opi->dir);
    if (opbd0 == NULL || opbd1 == NULL)
      return win_printf_OK("could not read data!\n");
    add_data_to_pltreg (pr, IS_ONE_PLOT,opbd0);
    add_data_to_pltreg (pr, IS_ONE_PLOT,opbd1);
  }

  refresh_plot(pr, UNCHANGED);

  return D_O_K;
}


/** This function Uses the plots generated by the mod_rot routine in trackBead 
  * to extract the angle of rotation.

\author Francesco Mosconi
*/
int torque_do_full_treat_mod_rot(void)
{
  register int i,j,k;
  char f_line[512];
  FILE *fpi = NULL;
  pltreg  *pr;
  O_p *opi = NULL, *opbd0 = NULL, *opbd1 = NULL, *opa = NULL, *opan = NULL, *opas = NULL;
  d_s *dsa = NULL, *dsan = NULL;
  int bd0, bd1, irepeat, mod_rot;
  float a = 0, ao = 0;
  int nl = 0, el = 0, p = 0;
  int nrepeat = 0, broken = 0;
  static int np, nprot = 1250, npa = 11250, nsteps = 6, before = 100, debug = 1;
  static float start = -75., rotstep = 30.,threshold = 0.;
  char s[256];
	
	
  if(updating_menu_state != 0)    return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&opi) != 2)
    {
      win_printf("cannot find data");
      return OFF;
    }
  if (sscanf(opi->filename,"X(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",&bd0,&mod_rot,&irepeat) != 3)
    return win_printf_OK("Francesco you did not load a mod_rot file!\n%s",opi->filename);
  bd1 = bd0 + 1;
  sprintf(f_line,"%sX(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",opi->dir,bd1,mod_rot,irepeat);
  fpi = fopen(f_line,"r");
  if (fpi == NULL)
    {
      bd1 = bd0 - 1;
      sprintf(f_line,"%sX(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",opi->dir,bd1,mod_rot,irepeat);
      fpi = fopen(f_line,"r");
    }
  if (fpi == NULL)
    return win_printf_OK("Francesco I cannot find your second bead file!\n");
  fclose(fpi);
  for (nrepeat = irepeat+1; ;nrepeat++)
    {
      sprintf(f_line,"%sX(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",opi->dir,bd0,mod_rot,nrepeat);
      fpi = fopen(f_line,"r");
      if (fpi == NULL)  break;
      fclose(fpi);
      sprintf(f_line,"%sX(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",opi->dir,bd1,mod_rot,nrepeat);
      fpi = fopen(f_line,"r");
      if (fpi == NULL)  break;
      fclose(fpi);
    }
	
	
  i = win_scanf("rotation points %8d (I will not use these)\n"
		"angle points %8d (I will use these)\n"
		"starting position %5f\n"
		"rotation step %5f\n"
		"n steps %5d\n"
		"threshold %5f\n"
		"before %6d\n"
		"debug %R no %r yes\n"
		,&nprot,&npa,&start,&rotstep,&nsteps,&threshold,&before,&debug);
  if (i == CANCEL)	return OFF;
  for (i = irepeat; i < nrepeat; i++)
    {
      sprintf(f_line,"X(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",bd0,mod_rot,i);
      opbd0 = create_plot_from_gr_file(f_line, opi->dir);
      sprintf(f_line,"X(t)Y(t)Z(t)bd%dmod_rot%d-%d.gr",bd1,mod_rot,i);
      opbd1 = create_plot_from_gr_file(f_line, opi->dir);
      if (opbd0 == NULL || opbd1 == NULL)
	return win_printf_OK("could not read op!\n");
      if (opbd0->dat == NULL || opbd1->dat == NULL || opbd0->dat+1 == NULL || opbd1->dat+1 == NULL)
	return win_printf_OK("could not read dat!\n");
		
      for(j = 1 ; j <= nsteps ; j++){
	p = j*(nprot+npa)-before;
	if((p >= opbd0->dat[0]->nx) || (p >= opbd1->dat[0]->nx) || (p >= opbd0->dat[1]->nx) || (p >= opbd1->dat[1]->nx))
	  return win_printf("aiutooooooooo!");
	if(debug) win_printf("p = %d",p);
	if((fabs((opbd0->dat[0]->yd[p])-(opbd1->dat[0]->yd[p])) < threshold )||
	   (fabs(opbd0->dat[1]->yd[p]-opbd1->dat[1]->yd[p]) < threshold )){
	  if(debug){					   sprintf(s,"hey!\n"
								   "j = %d\n"
								   "p  = %d\n"
								   "%f\n"
								   "%f\n"
								   "continue debugging? %%R no %%r yes"
								   ,j,p
								   ,fabs(opbd0->dat[0]->yd[nprot+npa-before]-opbd1->dat[0]->yd[nprot+npa-before])
								   ,fabs(opbd0->dat[1]->yd[nprot+npa-before]-opbd1->dat[1]->yd[nprot+npa-before]));
	    win_scanf(s,&debug);
	  }
	  broken = 1;
	  j=nsteps;
	}
      }
      if(broken){
	free_one_plot(opbd0);
	free_one_plot(opbd1);
	++el;
	broken = 0;
	continue;
      }		
		
		
      if(i == irepeat){
	opa = create_and_attach_one_plot(pr,opbd1->dat[0]->nx,opbd1->dat[0]->nx,0);
	opan = create_and_attach_one_plot(pr,opbd1->dat[0]->nx,opbd1->dat[0]->nx,0);
	set_plot_title(opa,"full angle");
	set_plot_title(opan,"angle");
	set_plot_x_title(opa,"time");
	set_plot_x_title(opan,"time");
	uns_op_2_op_by_type(opbd0, IS_X_UNIT_SET, opa, IS_X_UNIT_SET);
	uns_op_2_op_by_type(opbd0, IS_X_UNIT_SET, opan, IS_X_UNIT_SET);
	create_attach_select_y_un_to_op (opa, IS_METER, 0., 1., -6, 0,"\\mu m");
	create_attach_select_y_un_to_op (opan, IS_METER, 0., 1., -6, 0,"\\mu m");
	dsa = opa->dat[0];
	dsan = opan->dat[0];
      }
      else{
	dsa = create_and_attach_one_ds(opa,opbd1->dat[0]->nx,opbd1->dat[0]->nx,0);
	dsan = create_and_attach_one_ds(opan,opbd1->dat[0]->nx,opbd1->dat[0]->nx,0);
      }
      for(k = 0; k< opbd1->dat[0]->nx ;k++){
	dsa->xd[k] = dsan->xd[k] = opbd0->dat[0]->xd[k];
	a = dsa->yd[k] = dsan->yd[k] = atan2(opbd0->dat[1]->yd[k] - opbd1->dat[1]->yd[k],opbd0->dat[0]->yd[k] - opbd1->dat[0]->yd[k]);
	if(ao - a > 3.*PI/2){
	  nl++;
	}
	if(ao - a < -3.*PI/2){
	  nl--;
	}
	dsa->yd[k] += nl*2*PI;
	ao = a;
      }
      free_one_plot(opbd0);
      free_one_plot(opbd1);
    }
	
  win_printf("eliminated %d/%d plots, i=%d",el,nrepeat,i);
	
  opa->need_to_refresh |= 1 ;
  opan->need_to_refresh |= 1 ;
	
  if(opan == NULL)
    return win_printf("something wrong, null pointer encountered at opan");
  if(opan->dat == NULL)
    return win_printf("something wrong, null pointer encountered at opan->dat");
  if(opan->dat+1 == NULL)
    return win_printf("something wrong, null pointer encountered at opan->dat+1");
  if(opan->dat[1]->nx == 0)
    return win_printf("something wrong, null pointer encountered at opan->dat[1]->nx");
  np = opan->dat[1]->nx;
  if (np != (nsteps * (nprot+npa)))
    return win_printf("wrong number of points:\nnp = %d\nnsteps * (nprot+npa) = %d\n",np,nsteps * (nprot+npa));
	
  for(i = 0 ; i < nsteps; i++){
    opas = create_and_attach_one_plot(pr,npa,npa,0);
    set_plot_title(opas,"angle at rot = %f",start+i*rotstep);
    set_plot_x_title(opas,"time");
    uns_op_2_op (opas, opan);
    for (j = 0; j < opan->n_dat; j++)
      {
	if(j == 0)
	  dsan = opas->dat[0];
	else
	  dsan = create_and_attach_one_ds(opas,npa,npa,0);
	for(k = 0; k < npa ;k++){
	  dsan->xd[k] = opan->dat[j]->xd[i*(npa+nprot)+nprot+k];
	  dsan->yd[k] = opan->dat[j]->yd[i*(npa+nprot)+nprot+k];
	}
      }
    sprintf(f_line,"%smod_rot_angle_%3.1f.gr",opi->dir,start+i*rotstep);
    save_one_plot_bin(opas, f_line);
  }
  opas->need_to_refresh |= 1 ;
  refresh_plot(pr, UNCHANGED);
	
  return D_O_K;
}

int treat_mod_rot_angle(void)
{
  register int i,j,k;
  pltreg  *pr;
  O_p *op = NULL, *opan = NULL;
  d_s *dsan;
  static int np, nprot = 5250, npa = 17250, nsteps = 3;
  static float start = -50, rotstep = 50;

  if(updating_menu_state != 0)	return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
  {
    win_printf("cannot find data");
    return OFF;
  }

  i = win_scanf("rotation points %8d (I will not use these)\n"
		"angle points %8d (I will use these)\n"
		"starting position %5f\n"
		"rotation step %5f\n"
		"n steps %5d\n"
		,&nprot,&npa,&start,&rotstep,&nsteps);
  if (i == CANCEL)	return OFF;

  np = op->dat[0]->nx;
  if (np != (nsteps * (nprot+npa)))
    return win_printf("wrong number of points");
  
  for(i = 0 ; i < nsteps; i++){
    opan = create_and_attach_one_plot(pr,1,1,0);
    set_plot_title(opan,"angle at rot = %f",start+i*rotstep);
    set_plot_x_title(opan,"time");
    uns_op_2_op (opan, op);
    remove_ds_n_from_op(opan, 0);
    for (j = 0; j < op->n_dat; j++)
    {
      dsan = create_and_attach_one_ds(opan,npa,npa,0);
      for(k = 0; k < npa ;k++){
	dsan->xd[k] = op->dat[j]->xd[i*(npa+nprot)+nprot+k];
	dsan->yd[k] = op->dat[j]->yd[i*(npa+nprot)+nprot+k];
      }
    }
  }
  opan->need_to_refresh |= 1 ;
  refresh_plot(pr, UNCHANGED);

  return D_O_K;
}

int do_plotcrop(void)
{
  register int i,j;
  pltreg  *pr;
  O_p *op;
  d_s *ds;
  static int nprot = 1024, rotstep = 1, nrs = 1;

  if(updating_menu_state != 0)
	{
        add_keyboard_short_cut(0, KEY_C, 0, do_plotcrop);
        return D_O_K; 
    }	

  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
  {
    win_printf("cannot find data");
    return OFF;
  }

  i = win_scanf("number of points in step%8d\n"
				"first step number (1 to n) %8d\n"
				"how many steps to include %8d\n"
				,&nprot,&rotstep,&nrs);
  if (i == CANCEL)	return OFF;

  for(j = 0 ; j < 3 ; j++){
	ds = create_and_attach_one_ds(op,nprot*nrs,nprot*nrs,0);
	set_ds_source(ds,"%s",op->dat[j]->source);
	set_ds_treatement(ds,"cropped original dataset. kept only %d points of step n %d",nprot,rotstep);
	for(i = 0 ; i < nprot*nrs ; i++){
		ds->xd[i] = op->dat[j]->xd[(rotstep-1)*nprot + i];
		ds->yd[i] = op->dat[j]->yd[(rotstep-1)*nprot + i];
    }
  }
  
  op->need_to_refresh |= 1 ;
  refresh_plot(pr, UNCHANGED);

  return D_O_K;
}


/********************************************************************************/
/* exports all plots in corresponding tabular files			 				*/
/* assuming X-axis of the first dataset is the same for all dataset,	 	*/
/********************************************************************************/
int do_torque_export_tabular_file(void)
{
  register int i, j;
  O_p 	*op;
  int	n_col, n_lines;
  pltreg	*pr = NULL;
  char 	fullfilename[512];
  FILE	*file;
  
  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_R, 0, do_torque_export_tabular_file);
      return D_O_K; 
    }	
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT]) return win_printf_OK("This routine exports all the datasets of the current plot in a tabular ascii file .dat\n"
					    "\nFor now (11/2003), it assumes the X-axis of all the datasets is the same\n"
					    "and it saves it as the first colomn of the tabular file.");
  
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find plot!");
  n_col 	= op->n_dat + 1;
  n_lines = op->dat[0]->nx;
  
  j=0;
  for (i=1; i<op->n_dat; i++) 
    {	if (op->dat[i]->nx != n_lines) 
	j++;
    }
  if (j!=0) return win_printf_OK("There are %d datasets which number of points differ !!!\n"
				 "Eliminate them from the current plot, and try again.");
  
  //i = file_select_ex("Export ascii tabular .dat file", fullfilename, "dat", 512, 0, 0);
  
  sprintf(fullfilename,"%s%s_ASC.csv",op->dir,op->filename);
  
  if (i == 0) return D_O_K;
  file=fopen(fullfilename,"wt");
  if (file==NULL) return(win_printf("cannot create file !"));
  
  for (j=0; j<n_lines; j++)
    {
      fprintf(file, "%f %f", op->dat[0]->xd[j], op->dat[0]->yd[j]);
      for (i=1; i<op->n_dat; i++)
	{	fprintf(file, " %f", op->dat[i]->yd[j]);
	}
      fprintf(file, "\n");
    }
  
  fclose(file);	
  win_printf("exported file %s%s_ASC.csv",op->dir,op->filename);
  
  return D_O_K;
}

/********************************************************************************/
/* exports all plots in corresponding tabular files, separating each dataset    */
/*   in a separate file			 		       	                */
/********************************************************************************/
int do_torque_export_tabular_file_separate_ds(void)
{
  register int i, j, k;
  O_p 	*op;
  pltreg	*pr = NULL;
  char 	fullfilename[512];
  FILE	*file;
  
  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_E, 0, do_torque_export_tabular_file_separate_ds);
      return D_O_K; 
    }	
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT]) return win_printf_OK("This routine exports all the datasets of the current plot"
					    "in a tabular ascii file .csv\n");
  
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find plot!");
  

  for (k=0; k<op->n_dat; k++) 
    {
      j=strlen(op->filename);
      for(;op->filename[j] != '.';j--);
      op->filename[j] = 0;
      
      sprintf(fullfilename,"%s%s_ds%d_ASC.csv",op->dir,op->filename,k);
      
      file=fopen(fullfilename,"wt");
      if (file==NULL) return(win_printf("cannot create file !"));
      
      for (i=0; i<op->dat[k]->nx; i++)
	{
	  if(op->dat[k]->ye != NULL){
	    fprintf(file, "%f %f %f\n", op->dat[k]->xd[i], op->dat[k]->yd[i], op->dat[k]->ye[i]);
	  }
	  else{
	    fprintf(file, "%f %f\n", op->dat[k]->xd[i], op->dat[k]->yd[i]);
	  }
	}
      fprintf(file, "\n");
      fclose(file);	
    }
  
  win_printf("exported files %s%s_ds%d_ASC.csv",op->dir,op->filename,k);
  
  return D_O_K;
}

/********************************************************************************/
/* separates two ways hats into two separate files							 	*/
/********************************************************************************/
int do_torque_separate_two_ways_hats(void)
{
  register int i,j;
  int np;
  O_p 	*op,*opa,*opb;
  pltreg	*pr = NULL;
  char 	fullfilename[512];

  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_T, 0, do_torque_separate_two_ways_hats);
      return D_O_K; 
    }	
	
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT]) return win_printf_OK("separates two ways hats into two separate files");

  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find plot!");
  np = op->dat[0]->nx;

  opa = duplicate_plot_data (op, NULL);
  opb = duplicate_plot_data (op, NULL);

  for(i = np-1 ; i >= np/2 ; i--){
    remove_point_from_data_set(opb->dat[0], i-np/2);
    remove_point_from_data_set(opa->dat[0], i);
  }
  j=strlen(op->filename);
  for(;op->filename[j] != '.';j--);
  op->filename[j] = 0;
  sprintf(fullfilename,"%s%sa.gr",op->dir,op->filename);
  save_one_plot_bin(opa, fullfilename);
  sprintf(fullfilename,"%s%sb.gr",op->dir,op->filename);
  save_one_plot_bin(opb, fullfilename);
  win_printf("created files:\n%s%sa.gr\n%s%sb.gr",op->dir,op->filename,op->dir,op->filename);
  free_one_plot(opa);
  free_one_plot(opb);

  return D_O_K;
}

int do_weighted_average(void)
{
  register int i, j;
  O_p *op = NULL, *opn = NULL;
  int np = 0;
  pltreg *pr = NULL;
  d_s *ds = NULL, *dsn = NULL;
  float w = 0., wj = 0., zmag = 0.;
  int ndat = 0;
  char *st;
  if(updating_menu_state != 0)
    {
      //   add_keyboard_short_cut(0, KEY_E, 0, do_torque_export_tabular_file);
      return D_O_K; 
    }
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)		return win_printf_OK("cannot find plot!");
  ndat = op->n_dat;
  opn = create_and_attach_one_plot(pr,ndat,ndat,0);
  dsn = opn->dat[0];
  dsn =  alloc_data_set_y_error(dsn);
  for(i = 0; i < ndat; i++){
    ds = op->dat[i];
    if (ds->source == NULL)  return win_printf_OK("this is not a hat curve");
    if (strstr(ds->source,"Zmag") != NULL)
    {
      st = strstr(ds->source,"Zmag");
      if (st == NULL) 
	return win_printf_OK("this is not a good hat curve");
      if (sscanf(st,"Zmag %f",&zmag) != 1)
	return win_printf_OK("problem recovering parameters from hat curve");	
    }
    np=ds->nx;
    w=0.;
    dsn->yd[i] = 0.;
    for(j = 0;j < np;j++){
      wj = 1/((ds->ye[j])*(ds->ye[j]));
      w += wj;
      dsn->xd[i] = zmag;
      dsn->yd[i] += ds->yd[j]*wj;
    }
    dsn->yd[i]/=w;
    dsn->ye[i] = 1/sqrt(w);
  }
  set_plot_title (opn, "Weighted Mean Force");
  set_plot_y_title (opn, "Force (pN)");
  set_ds_dot_line(dsn);
  set_ds_point_symbol(dsn, "\\pt6\\oc");
  opn->need_to_refresh |= 1 ;
  refresh_plot(pr, UNCHANGED);
  
  return D_O_K;
}

int w_av_2n(float *yin,float *ein,int np,float *y,float *e)
{
  register int j = 0;
  float w = 0.,wj = 0.;
  
  w=0.;
  for(j = 0;j < np;j++){
    wj = 1/(ein[j]*ein[j]);
    w += wj;
    *y += yin[j]*wj;
  }
  *y /=w;
  *e = 1/sqrt(w);
  return D_O_K;
}

int do_scan_xvplot(void)
{
  multi_d_s *mds;
  O_p *op;
  d_s *ds;
  int i;
  pltreg* pr;

  if(updating_menu_state != 0)      return D_O_K;          
  if (ac_grep(cur_ac_reg,"%pr", &pr) != 1)
    return win_printf_OK("ou est la pltreg ?");
  
  if ((mds = find_multi_d_s_in_pltreg(pr))==NULL) 
    return win_printf_OK("not a xv_plot plot region");
  
  if (mds != NULL){
    last_x_index = mds->x_index = 12;
    last_y_index = mds->y_index = 3;
    //here I create the plot for the k_x including the error:
    if ((op = create_and_attach_one_plot(pr, mds->n, mds->n, 0))==NULL)
             return OFF;
    
    if ((ds = op->dat[0])==NULL) return OFF;
    if ((ds = alloc_data_set_y_error(ds)) == NULL)
      return win_printf("I can't allocate y error!");
    for ( i = 0 ; i < mds->n ; i++)
    {
        ds->xd[i] = mds->x[mds->x_index][i];
        ds->yd[i] = 1000000*mds->x[mds->y_index][i];
	ds->ye[i] = 1000000*mds->x[mds->y_index+1][i];
    }
    ds->source = my_sprintf(NULL,"file %s x-> col %d y -> col %d",
        (mds->source != NULL) ? mds->source :"unknown",mds->x_index,mds->y_index);
    ds->nx = ds->ny = mds->n;
    set_plot_x_title(op, "%s", mds->name[mds->x_index]);
    set_plot_y_title(op, "10^6 %s", mds->name[mds->y_index]);
    set_plot_y_log(op);
    //here I create the two plots for the Zbead and Zfixed
    last_x_index = mds->x_index = 12;
    last_y_index = mds->y_index = 14;
    do_xvplot();
    last_x_index = mds->x_index = 12;
    last_y_index = mds->y_index = 15;
    do_xvplot();

  }
  
  return D_O_K;
}

int do_average_ar(void)
{
  O_p *op;
  d_s *ds,*dsn;
  int i;
  pltreg* pr;

  if(updating_menu_state != 0)      return D_O_K;          
  if (ac_grep(cur_ac_reg,"%pr%op%ds", &pr,&op,&ds) != 3)
    return win_printf_OK("data not found?");

  if ((dsn = create_and_attach_one_ds(op, ds->nx/2, ds->nx/2, 0))==NULL)
    return OFF;
  if(dsn->nx*2 != ds->nx)
    add_new_point_to_ds(dsn,0,0);
    
  if (ds->ye !=NULL)
    if ((dsn = alloc_data_set_y_error(dsn)) == NULL)
      return win_printf("I can't allocate y error!");
  for ( i = 0 ; i < ds->nx/2 ; i++){
    dsn->xd[i] = ds->xd[i];
    dsn->yd[i] = (ds->yd[i] + ds->yd[ds->nx - i -1])/2;
    if(dsn->ye != NULL)
      dsn->ye[i] = sqrt(ds->ye[i]*ds->ye[i] + ds->ye[ds->nx - i - 1]*ds->ye[ds->nx - i - 1])/sqrt(2);
  }
  if(dsn->nx*2 != ds->nx){
    dsn->xd[dsn->nx - 1] = ds->xd[ds->nx/2];
    dsn->yd[dsn->nx - 1] = ds->yd[ds->nx/2];
    if(dsn->ye != NULL)
      dsn->ye[dsn->nx - 1] = ds->ye[ds->nx/2];
  }

  op->need_to_refresh |= 1 ;
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}


int do_average_2ds(void)
{
  O_p *op;
  d_s *ds,*dsi,*dsn;
  int i;
  pltreg* pr;
  float yin[2],ein[2];
  
  if(updating_menu_state != 0)      return D_O_K;          
  if (ac_grep(cur_ac_reg,"%pr%op", &pr,&op) != 2)
    return win_printf_OK("data not found");
  if((ds = op->dat[0]) == NULL)
    return win_printf_OK("data not found");
  if((dsi = op->dat[1]) == NULL)
    return win_printf_OK("data not found");
  
  if ((dsn = create_and_attach_one_ds(op, ds->nx, ds->nx, 0)) == NULL)
    return OFF;
  
  if (ds->ye !=NULL)
    if ((dsn = alloc_data_set_y_error(dsn)) == NULL)
      return win_printf("I can't allocate y error!");
  for ( i = 0 ; i < ds->nx ; i++){
    dsn->xd[i] = ds->xd[i];
    if(dsn->ye != NULL){
      yin[0] = ds->yd[i];
      yin[1] = dsi->yd[i];
      ein[0] = ds->ye[i];
      ein[1] = dsi->ye[i];
      w_av_2n(yin,ein,2,dsn->yd+i,dsn->ye+i);
    }
    else
      dsn->yd[i] = (ds->yd[i] + dsi->yd[i])/2;
  }
  
  op->need_to_refresh |= 1 ;
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

MENU *torque_simulation_submenu(void)
{
  static MENU mn[32];
  
  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"generator", do_generate_signal,NULL,0,NULL);
  add_item_to_menu(mn,"filter", do_coil_filter ,NULL,0,NULL);
  add_item_to_menu(mn,"Best R", find_R_of_coil ,NULL,0,NULL);
  add_item_to_menu(mn,"adj Best R", draw_R_vs_L ,NULL,0,NULL);
  add_item_to_menu(mn,"Simulate", simulate_torque ,NULL,0,NULL);
  add_item_to_menu(mn,"Simulate happo", simulate_torque_happo ,NULL,0,NULL);
  add_item_to_menu(mn,"Simulate fra happo", simulate_torque_fra_happo ,NULL,0,NULL);
  return mn;
}

MENU *torque_old_submenu(void)
{
  static MENU mn[32];
  
  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Treat Stepped Angle", treat_stepped_angle ,NULL,0,NULL);
  add_item_to_menu(mn,"Average Continuous Angle", average_continuous_angle,NULL,0,NULL);
  add_item_to_menu(mn,"Throw away points", throw_away_points,NULL,0,NULL);
  add_item_to_menu(mn,"Crop Dataset", crop_dataset,NULL,0,NULL);
  add_item_to_menu(mn,"Analyse Ellinc Dataset", analyse_ellinc_dataset,NULL,0,NULL);
  add_item_to_menu(mn,"Catenate 2 ds", catenate_2ds,NULL,0,NULL);
  return mn;
}

MENU *torque_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"simulation",NULL,torque_simulation_submenu(),0,NULL);
	add_item_to_menu(mn,"old",NULL,torque_old_submenu(),0,NULL);
	add_item_to_menu(mn,"Extract Angle and CM from Traj", extract_angle_and_cm_from_traj,NULL,0,NULL);
	add_item_to_menu(mn,"Inspect mod rot files", torque_inspect_mod_rot,NULL,0,NULL);
	add_item_to_menu(mn,"Treat mod rot files", torque_do_full_treat_mod_rot,NULL,0,NULL);
	add_item_to_menu(mn,"Treat mod rot angle", treat_mod_rot_angle,NULL,0,NULL);
	add_item_to_menu(mn,"Crop plot", do_plotcrop,NULL,0,NULL);
	add_item_to_menu(mn,"Export plot to csv", do_torque_export_tabular_file,NULL,0,NULL);
	add_item_to_menu(mn,"Export all ds to separate csv", do_torque_export_tabular_file_separate_ds,NULL,0,NULL);
	add_item_to_menu(mn,"Separate two ways hats", do_torque_separate_two_ways_hats,NULL,0,NULL);
	add_item_to_menu(mn,"weighted average", do_weighted_average,NULL,0,NULL);
	add_item_to_menu(mn,"Do scan plot", do_scan_xvplot, NULL,0,NULL);
	add_item_to_menu(mn,"Do average ar", do_average_ar, NULL,0,NULL);
	add_item_to_menu(mn,"Do average 2ds", do_average_2ds, NULL,0,NULL);
	return mn;
}

int	torque_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "torque", NULL, torque_plot_menu(), 0, NULL);
	return D_O_K;
}

int	torque_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "torque", NULL, NULL);
	return D_O_K;
}
#endif

