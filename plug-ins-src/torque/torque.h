#ifndef _TORQUE_H_
#define _TORQUE_H_

PXV_FUNC(int, do_generate_signal, (void));
PXV_FUNC(MENU*, torque_plot_menu, (void));
PXV_FUNC(int, torque_main, (int argc, char **argv));
#endif

