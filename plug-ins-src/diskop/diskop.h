#ifndef _DISKOP_H_
#define _DISKOP_H_

#define DISKOP_ONE_FILE		    0x000100
#define DISKOP_SEVERAL_FILES	0x000800


PXV_FUNC(MENU*, diskop_plot_menu, 		(void));
PXV_FUNC(int, diskop_main, 			(int argc, char **argv));
PXV_FUNC(int, diskop_unload,			(int argc, char **argv));

// functions for corde in turbulence :
PXV_FUNC(int, do_diskop_PSD,			    (void));
PXV_FUNC(int, do_diskop_compute_response,	(void));

// functions of general interest :
PXV_FUNC(int, do_diskop_decimate_NI_files,      (void));
PXV_FUNC(int, do_diskop_extract_NI_files,       (void));

PXV_FUNC(void, spit_unallocated,                (char *message));

#endif

