#ifndef _DISKOP_C_
#define _DISKOP_C_

#include <gsl/gsl_histogram.h>
#include <gsl/gsl_sf.h>
#include <fftw3.h>

#include "allegro.h"
#include "xvin.h"
#include "../../include/xvin/fftw3_lib.h"
#include "xv_tools_lib.h"

#include "../inout/inout.h"
#include "../response/response.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "diskop.h"





int do_diskop_decimate_NI_files(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memory 
	O_p 		*op1=NULL;
	pltreg	*pr = NULL;
	int		index, i_file, n_files;	// current file number, number of taus, number of files
	int		*files_index=NULL;		// all the files
static	char 		files_ind_string[128]	="0:1:2"; 
static	char 		file_extension[50]	=".bin";
static	char		root_filename[128]	= "run02";
static int 		bool_file_type=1;
	int		file_type=0;
static int		N_decimate=10, N_channels=2;
	char		*s=NULL;
	char		filename_in[256], filename_out[256];
	char		message_to_spit[128]="operating on file";
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine loads a set of files\n"
				"and saves them after decimation (average)\n\n"
				"(in order to save some disk space)");
	}
	
	s=my_sprintf(s, "{\\color{yellow}\\pt14 Decimate a set of NI files}\n"
			"root name of files %%s"
			"range of files %%s"
			"file extension %%s\n"
			"file type : %%R NI4472 or %%r NI-DAQ/mx\n"
			"Number of channels in a file : %%2d\n"
			"decimate data by a factor of %%3d\n");
	i=win_scanf(s, &root_filename, &files_ind_string, &file_extension, 
				&bool_file_type, &N_channels, &N_decimate);
	free(s); s=NULL;
	if (i==CANCEL) return(OFF);
	if (N_decimate<=1) return(OFF);
	if (bool_file_type==0) 		file_type = IS_NI_4472_FILE;
	else if (bool_file_type==1) file_type = IS_NI_DAQmx_FILE;
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];

		sprintf(filename_in, "%s%d%s", root_filename, i_file, file_extension);
		sprintf(message_to_spit, "%d/%d file %s: converting", k+1, n_files, filename_in); spit_unallocated(message_to_spit);
		sprintf(filename_out, "%s%d%s%s", root_filename, i_file, "_dec", file_extension);
	
		if (decimate_NI_file(filename_in, file_type, N_channels, N_decimate, filename_out)<0) // if error
		if (win_printf("error with file %d/%d\n click OK to continue, or CANCEL to exit", k+1, n_files)==CANCEL) return(D_O_K);
		
	} //end of the loop over k, ie, loop over the files;
	free(files_index);

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_diskop_decimate_NI_files' */






int do_diskop_extract_NI_files(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memory 
	O_p 		*op1=NULL;
	pltreg	*pr = NULL;
static float offset_1=0., factor_m=1., factor_d=1., offset_2=0.;
	int		index, i_file, n_files;	// current file number, number of taus, number of files
	int		*files_index=NULL;		// all the files
static char 	files_ind_string[128]	="0:1:2"; 
static char 	file_extension[50]	= ".bin";
static char	root_filename[128]	= "run02";
static char 	file_extension_o[50]	= ".dat";
static char	root_filename_o[128]= "run02_x_";
static int 		bool_file_type=1;
	int		file_type=0;
static int		N_channels=2, i_channel=1, N_decimate=1;
	char		*s=NULL;
	char		filename_in[256], filename_out[256];
	char		message_to_spit[128]="operating on file";
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine loads a set of NI files (.bin)\n"
				"and saves them after in raw binary format (.dat)\n\n"
				"(in order to use the 'cycles' plug-in.)");
	}
	
	s=my_sprintf(s, "{\\color{yellow}\\pt14 Extract data from a set of NI files}\n"
			"root name of input NI files %%s"
			"range of files %%s"
			"input NI files extension %%s\n"
			"file type : %%R NI4472 or %%r NI-DAQ/mx\n"
			"Number of channels in a file : %%2d\n"
			"channel you want : %%2d\n"
			"decimate by keeping 1 point every %%6d points\n"
			"add first an offset %%7f and then\n"
			"multiply by %%7f and divide by %%7f\n"
			"and then add another offset %%7f"
            "root name of binary output files %%s\n"
            "output binary files extension %%s");
	i=win_scanf(s, &root_filename, &files_ind_string, &file_extension, 
				&bool_file_type, &N_channels, &i_channel, &N_decimate, 
                &offset_1, &factor_m, &factor_d, &offset_2, &root_filename_o, &file_extension_o);
	free(s); s=NULL;
	if (i==CANCEL) return(OFF);
	if (N_decimate<1) return(OFF);
	if (bool_file_type==0) 		file_type = IS_NI_4472_FILE;
	else if (bool_file_type==1) file_type = IS_NI_DAQmx_FILE;
	
	if (factor_d==0) return(win_printf_OK("I don't like to divide by zero..."));
	
	i_channel --; // convention humaine -> convention C 
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];

		sprintf(filename_in, "%s%d%s", root_filename, i_file, file_extension);
		sprintf(message_to_spit, "%d/%d file %s: extracting", k+1, n_files, filename_in); spit_unallocated(message_to_spit);
		sprintf(filename_out, "%s%d%s", root_filename_o, i_file, file_extension_o);

		if (extract_NI_file(filename_in, file_type, N_channels, i_channel, N_decimate, 
                            factor_m/factor_d, (factor_m/factor_d)*offset_1 + offset_2, filename_out)<0) // if error
		if (win_printf("error with file %d/%d\n click OK to continue, or CANCEL to exit", k+1, n_files)==CANCEL) return(D_O_K);
		
	} //end of the loop over k, ie, loop over the files;
	free(files_index);

    i_channel ++; // convention C -> convention humaine pour la prochaine fois

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_diskop_extract_NI_files' */






int do_diskop_PSD(void)
{ 	register long i, j;
    int      k;
   	pltreg *pr;         // plot region
   	O_p 	*opd=NULL;  // destination plots
   	d_s 	*dsd=NULL;  // destination datasets
	int     nx;
   	char 	*s=NULL, message_to_spit[128]="toto";
static	float	f_acq=-1.0, percent_overlap=0.8;
static  int 	chunk_size = 1000000;
static	int	bool_hanning=1, n_fft, n_overlap;
static 	int 	bool_file_type=1;
static int 	do_scan_chunks=0;
static size_t 	j_start, j_end;
static	char	selected_channel_string[32]="1,2";
static char 	fullfilename[512]="toto.bin";
static	char 	files_ind_string[128]	="0:1:2"; 
static	char 	file_extension[8]	=".bin";
static	char	root_filename[128]	= "run02";
static int 	N_decimate=1;
	int	index;
	FILE 	*data_file=NULL;	
	char	filename[256],pathname[512];
	long 	N=1;
	int		N_channels=1, i_channel=1, n_chan, n_files, i_file;
	int 	*channel_index=NULL, *files_index=NULL;
	float	F_s, F_r;
	double	moyenne;
	float	*tampon_float, *tmp, *tmp_PSD=NULL, *tmp_freq=NULL;
	char	*channel_string=NULL, *channel_cfg=NULL, *user_header=NULL;
	char 	*date_stamp=NULL;
	long	current_position=-1;

	if (updating_menu_state != 0)	return D_O_K;	
	index = (active_menu->flags & 0xFFFFFF00);
	if ( !(index & PSD_ENERGY) && !(index & PSD_AMP) ) return D_O_K;
	    
        if (key[KEY_LSHIFT])   return(win_printf_OK("This routine computes the Power Spectrum Density (PSD)\n"
        		"of the selected channels of one or several files acquired with Labview\n"
				"A new plot is created (log-log display)"));    

	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)  			return(win_printf_OK("Could not find plot region"));

	if (index & DISKOP_ONE_FILE)	
	{	if (win_scanf("{\\pt14\\color{yellow}PSD of data from a file}\n"
				"%R NI4472 card (DAQ : streamfloat.vi / streamfloat5.vi)\n"
				"%r NI5555 card (DAQ/mx : streamfloat7mx/streamfloat8.vi) ",
				&bool_file_type)==CANCEL) 		return(D_O_K);
		if ( file_select_ex("select a NI file for PSD", fullfilename, "", 512, 0, 0) == 0) 
									return(D_O_K);
		n_files=1;
	}
	else if (index & DISKOP_SEVERAL_FILES)	
	{	if (win_scanf("{\\pt14\\color{yellow}PSD of data from several files}\n"
				"%R NI4472 card (DAQ : streamfloat.vi / streamfloat5.vi)\n"
				"%r NI5555 card (DAQ/mx : streamfloat7mx/streamfloat8mx.vi)\n\n"
				"name of the file (no suffix) %s\n"
				"suffix : %8s\n"
				"file range %12s",
				&bool_file_type, &root_filename, &file_extension, &files_ind_string)==CANCEL) 		
									return(D_O_K);
		files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
		if ( (files_index==NULL) || (n_files<1) )		return(win_printf_OK("bad values for files indices !"));
	}
	else return(win_printf_OK("Error in function call"));

	for (k=0; k<n_files; k++)
	{	if (index & DISKOP_SEVERAL_FILES)
		{	i_file=files_index[k]; 
// win_printf_OK("We are about to open file %d/%d", k+1, n_files);		
		
			sprintf(fullfilename, "%s%d%s", root_filename, i_file, file_extension);
			sprintf(message_to_spit, "%d/%d file %s: loading", k+1, n_files, fullfilename); spit_unallocated(message_to_spit);
		}

		if (bool_file_type==0) 
			current_position=get_header_informations_NI4472(fullfilename, &channel_string, &channel_cfg, 
							&N_channels, &F_s, &F_r, &date_stamp, &user_header, &N);
		else if (bool_file_type==1) current_position=get_header_informations_DAQMX(fullfilename, &channel_string, 
							&N_channels, &F_s, &date_stamp, &user_header, &N);
		if (current_position<0)		return(win_printf_OK("Error reading file header, aborting."));
	
		if (N_channels==1) sprintf(selected_channel_string,"1"); // fatalement, n'est-ce pas ?
		else	sprintf(selected_channel_string,"1:%d", N_channels);
	
		extract_file_name( filename,    256, fullfilename);
		extract_file_path( pathname,	512, fullfilename);
		
		if ( k==0 ) // if first (or single) file:
		{	s=my_sprintf(s,"File %s, created %s with %s\n%s\nchannel names: %s\n"
				"{\\color{lightgreen}f_{acq} is %g Hz}\n\n"
				"There are {\\color{yellow} %d channels}\nWhich channel(s) do you want ?  %%s\n\n"
				"There are %d points per channels\n"
				"There are %d points in total (over all channels)\n\n"
				"keep 1 point every %%4d points (decimate)\n"
				"%%R scan all the file at once\n"
				"%%r scan only a given part of the file\n"
				"%%r scan chunks of %%10d points",
				filename, date_stamp, (bool_file_type==0) ? "NI-DAQ" : "DAQ/mx", 
				user_header, channel_string, F_s, N_channels, (int)(N/N_channels), N);
		    	i=win_scanf(s, &selected_channel_string, &N_decimate, &do_scan_chunks, &chunk_size);
		    	free(s); s=NULL;
		    	free(channel_string);
		    	if (i==CANCEL) return(D_O_K);
			if (do_scan_chunks!=0) return(win_printf_OK("not available yet..."));
	
			channel_index = str_to_index(selected_channel_string, &n_chan);	// malloc is done by str_to_index
			if ( (channel_index==NULL) || (n_chan<1) )	return(win_printf_OK("bad values for channels numbers !"));
	
			for (i_channel=0; i_channel<n_chan; i_channel++)
			{	if ( (channel_index[i_channel]<1) || (channel_index[i_channel]>N_channels) ) 
					{ return(win_printf_OK("incorrect channel number !")); }
			}
			N=N/N_channels;
			if (N_decimate<1) 		return(win_printf_OK("Very bad decimation !")); 
					
			if (do_scan_chunks==2)
			{	s=my_sprintf(s,"There are %d points in time, for %d different channels\n"
					"(%d points in total)\n"
					"decimation by a factor %d will keep {\\color{red}%d points in time}\n\n"
					"load points in the interval [A B[ with\n"
					"A = %%8d (0<=A<%d)\n"
					"B = %%8d (0<B<=%d)", N, N_channels, N*N_channels, N_decimate, N/N_decimate, N/N_decimate, N/N_decimate);
				i=win_scanf(s, &j_start, &j_end);
				free(s); s=NULL;
				if (i==CANCEL) return(D_O_K);
				if ( (j_start<0) || (j_start>=N/N_decimate) )
					return(win_printf("bad interval!"));
			}
			else
			{	j_start = 0;
				j_end   = N/N_decimate;
			}
		}	
		f_acq = (float)F_s/(float)N_decimate;
		nx	  = (int)j_end-(int)j_start;

	
		if (k==0)
		{	// below, we prepare the PSD computation:	
	 	    n_fft     = (int)f_acq;
		    n_overlap = (int)((float)percent_overlap*(float)100.);
            
            s=my_sprintf(s, "{\\pt14\\color{yellow}Power Spectral Density (PSD)}\n\n"
				"there are %d channels to look at.\n"
				"one channel has %d points. \n"
				"- number of points of fft window : %%8d\n"
				"- overlap between windows       : %%8d percents\n"
				"- acquisition frequency (in Hz) : %%8f\n"
				"     (for normalization)\n\n"
				"%%b use Hanning window ?\n", n_chan, N);
			i=win_scanf(s, &n_fft, &n_overlap, &f_acq, &bool_hanning);
			free(s); s=NULL;
			if (i==CANCEL)	return(D_O_K);
			percent_overlap =(float)n_overlap/((float)(100.));
			n_overlap       =(int)(percent_overlap*(float)n_fft);
	
			if ( (n_overlap>=n_fft) || (percent_overlap>=1.) )	
			return(win_printf_OK("overlap is too large compared to n-fft\n"
				"in points    : %d>=%d\n"
				"in percents : %d>=100", n_overlap, n_fft, (int)((float)100.*percent_overlap)));
		}
	
 		tampon_float = (float*)calloc(N_channels, sizeof(float)); // size = number of channels in file
		tmp          = (float*)calloc(nx, sizeof(float)); if (tmp==NULL) return(win_printf_OK("out of memory"));

// win_printf(" f acq = %g\n N= %d\n n channels = %d\n nx = %d\n start at %d and end at %d\n"
//				" N channels = %d\n n fft = %d\n N decimate %d", 
//				f_acq, N, n_chan, nx, j_start, j_end, N_channels, n_fft, N_decimate);
	
		data_file=fopen(fullfilename, "rb");
		for (i_channel=0; i_channel<n_chan; i_channel++) 
		{	fseek(data_file, current_position, SEEK_SET); // we move from the beginning of the file, to the end of the header
			fseek(data_file, j_start*N_decimate*N_channels*sizeof(float), SEEK_CUR);
	
	        sprintf(message_to_spit, "(%d/%d) %s loading channel %d", k+1, n_files, filename, channel_index[i_channel]); 
			spit_unallocated(message_to_spit);
		
			for (j=0; j<nx; j++) // we load nx points from the data file
			{	
				moyenne=(double)0.; tmp[j]=0.;

				for (i=0; i<N_decimate; i++) 
				{	fread(tampon_float, sizeof(float), N_channels, data_file);
#ifndef XV_MAC	
			        swap_bytes(tampon_float, N_channels, sizeof(float));
#else 
#ifndef MAC_POWERPC				
			        swap_bytes(tampon_float, N_channels, sizeof(float));
#endif
#endif

					moyenne += (double)tampon_float[channel_index[i_channel]-1];
				}
				tmp[j] = (float)(moyenne/(double)N_decimate);
			}

// win_printf_OK("loaded data from channel %d (indice %d)\n\n"
//				"tmp: 0 = %f / %d = %f", channel_index[i_channel], i_channel, (float)tmp[0], nx, (float)tmp[nx-1]);

			if (k==0) // first file, we create the datasets (one per channel)
			{	if (i_channel==0)
				{	opd = create_and_attach_one_plot(pr, (n_fft/2)+1, (n_fft/2)+1, 0);
    					if (opd == NULL)        return(win_printf_OK("Cannot allocate plot"));
    					dsd = opd->dat[0];
				}
				else 	dsd = create_and_attach_one_ds(opd, (n_fft/2)+1, (n_fft/2)+1, 0);
				if (dsd == NULL)    	return(win_printf_OK("Cannot allocate dataset"));
				tmp_PSD =dsd->yd;
				tmp_freq=dsd->xd;
    			}
			else
			{	if (i_channel==0)
				{  tmp_PSD  = calloc(n_fft/2+1, sizeof(float));
				   tmp_freq = calloc(n_fft/2+1, sizeof(float));
                }
			}

			sprintf(message_to_spit, "(%d/%d) %s computing PSD of channel %d", k+1, n_files, filename, channel_index[i_channel]); 
			spit_unallocated(message_to_spit);
		
			i=compute_psd_of_float_data(tmp, tmp_PSD, tmp_freq, f_acq, nx, n_fft, n_overlap, bool_hanning);
			if (i==0) return win_printf_OK("nothing to do with your values !");

			if (k!=0)
			{	for (i=0; i<((n_fft/2)+1); i++)
					opd->dat[i_channel]->yd[i] += tmp_PSD[i];
			}
		}
		fclose(data_file);

		free(tampon_float);
		free(tmp);
  } //end of the loop over k, ie, loop over the files;

  if (index & DISKOP_SEVERAL_FILES) free(files_index);
  if (n_files>1) { free(tmp_PSD); free(tmp_freq);}

	for (i_channel=0; i_channel<n_chan; i_channel++)
	{	if (n_files>1)			// we averaged spectra over n_files files
		{	for (i=0 ; i<= n_fft/2 ; i++)
			{      	opd->dat[i_channel]->yd[i] /= (float)n_files;
  		}
		}

		if (index & PSD_AMP)		// here, user asked for a psd in amplitude units instead of energy units
		{	for (i=0 ; i<= n_fft/2 ; i++)
			{      	opd->dat[i_channel]->yd[i] = (float)sqrt(opd->dat[i_channel]->yd[i]);
  		}
		}
		
		set_ds_source(opd->dat[i_channel],
				"%s acquisition, %s\n%s\nchannel %d/%d, f_{acq}=%gHz\n%s\ndecimation 1/%d points",
				(bool_file_type==0) ? "NI-DAQ" : "DAQ/mx", 
				(index & DISKOP_ONE_FILE) ? filename : root_filename, 
				date_stamp, channel_index[i_channel], N_channels, F_s, user_header, N_decimate);
			
		opd->dat[i_channel]->treatement=my_sprintf(NULL,"P.S.D. (%s) over %d points, f_{acq}=%gHz, %s",
				(index & PSD_AMP) ? "amplitude" : "energy", n_fft, f_acq, 
				(bool_hanning==1) ? "Hanning window" : "no windowing");
  }

	// following lines creates a label not attach to a dataste, but to the plot itself:
	s=my_sprintf(s,"\\fbox{\\stack{{%s %s}{%s}}}", 
		(index & DISKOP_ONE_FILE) ? filename : root_filename, date_stamp, user_header); 
	push_plot_label(opd, opd->dat[0]->xd[n_fft/4], opd->dat[0]->yd[n_fft/4], s, USR_COORD);
	free(s); s=NULL;
	
    if (index & DISKOP_ONE_FILE) opd->filename = strdup(filename);
    else                         opd->filename = strdup(root_filename);
	opd->dir 	  = Mystrdup(pathname);	
    set_plot_title(opd, "PSD %s %s", (index & DISKOP_ONE_FILE) ? filename : root_filename, date_stamp);
	set_plot_x_title(opd, "frequency (Hz)");
	if (index & PSD_AMP) set_plot_y_title(opd, "channel %s/%d V/\\sqrt{Hz}", selected_channel_string, N_channels);
	else		         set_plot_y_title(opd, "channel %s/%d V^2/Hz", 	selected_channel_string, N_channels);
		   
	opd->iopt |= XLOG;
	opd->iopt |= YLOG;

	free(user_header);
	free(date_stamp);
	free(channel_index);
	
 	return(refresh_plot(pr, UNCHANGED));     
} /* end of function 'do_diskop_PSD' */






int do_diskop_compute_response(void)
{ 	register int i, j, k;
   	pltreg *pr;         // plot region
   	O_p *op, *opd=NULL;      // initial and destination plots
   	d_s 	*dsd=NULL;     // initial and destination datasets
   	int     nx;
   	char 	*s=NULL, message_to_spit[128]="operating on file";
static	float	f_acq=-1.0, percent_overlap=0.8;
static  int 	chunk_size = 1000000;
static	int		bool_overlap_type=1, bool_hanning=1, n_fft, n_overlap, bool_crossresponse=1;
static 	int 	bool_file_type=1;
static int 		do_scan_chunks=0;
static size_t 	j_start, j_end;
static	int		selected_channel_input=1, selected_channel_output=2;
static char 	fullfilename[512]="toto.bin";
static	char 	files_ind_string[128]	="0:1:2";
static	char 	file_extension[8]	=".bin";
static	char	root_filename[128]	= "run02";
static int 	N_decimate=1;
       int	index;
	FILE 	*data_file=NULL;	
	char	filename[256],pathname[512];
	long 	N=1;
	int		N_channels=1, n_files, i_file;
	int 	*files_index=NULL;
	float	F_s, F_r;
	float	moyenne_in, moyenne_out;
	float	*tampon_float, *tmp_in, *tmp_out;
	char	*channel_string, *channel_cfg, *user_header;
	char 	*date_stamp;
	long	current_position=-1;
  r_func *response=NULL;


	if (updating_menu_state != 0)	return D_O_K;	
	index = (active_menu->flags & 0xFFFFFF00);
	    
    if (key[KEY_LSHIFT])   return(win_printf_OK("This routine computes a response function\n"
        		"using selected channels of a file acquired with Labview.\n"
        		"This function can operate on several files, provided their\n"
            "names only differ by a integer number.\n\n"
				"A new plot is created (log-log display of the response modulus)\n"
				"The current response is updated"));

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)  			return(win_printf_OK("Could not find plot data"));


	if (index & DISKOP_ONE_FILE)
	{	if (win_scanf("{\\pt14\\color{yellow}Response function, using data from a file}\n"
				"%R NI4472 card (DAQ : streamfloat.vi / streamfloat5.vi)\n"
				"%r NI5555 card (DAQ/mx : streamfloat7mx/streamfloat8.vi) ",
				&bool_file_type)==CANCEL) 		return(D_O_K);
    if ( file_select_ex("select a NI file for response computation", fullfilename, "", 512, 0, 0) == 0)
										return(D_O_K);
		n_files=1;
	}
	else if (index & DISKOP_SEVERAL_FILES)
	{	if (win_scanf("{\\pt14\\color{yellow}Response function using data from several files}\n"
				"%R NI4472 card (DAQ : streamfloat.vi / streamfloat5.vi)\n"
				"%r NI5555 card (DAQ/mx : streamfloat7mx/streamfloat8mx.vi)\n\n"
				"name of the file (no suffix) %s\n"
				"suffix : %8s      file range %12s\n",
				&bool_file_type, &root_filename, &file_extension, &files_ind_string)==CANCEL)
									return(D_O_K);
		files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
		if ( (files_index==NULL) || (n_files<1) )		return(win_printf_OK("bad values for files indices !"));
	}
	else return(win_printf_OK("Error in function call"));


	for (k=0; k<n_files; k++)
	{	if (index & DISKOP_SEVERAL_FILES)
		{	i_file=files_index[k];
			sprintf(fullfilename, "%s%d%s", root_filename, i_file, file_extension);
		}

		if (bool_file_type==0)
			current_position=get_header_informations_NI4472(fullfilename, &channel_string, &channel_cfg,
							&N_channels, &F_s, &F_r, &date_stamp, &user_header, &N);
		else if (bool_file_type==1) current_position=get_header_informations_DAQMX(fullfilename, &channel_string,
							&N_channels, &F_s, &date_stamp, &user_header, &N);
		if (current_position<0)		return(win_printf_OK("Error reading file header, aborting."));

	  if (N_channels<2) 			return(win_printf_OK("not enough channels in file %s", fullfilename));

		extract_file_name( filename,    256, fullfilename);
		extract_file_path( pathname,	512, fullfilename);
		
		if (k==0)
		{	s=my_sprintf(s,"File %s, created %s with %s\n%s\nchannel names: %s\n"
				"{\\color{lightred}f_{acq} is %g Hz}\n\n"
				"There are {\\color{yellow} %d channels}\n"
        "channel with excitation : %%3d\n"
		    "channel with response   : %%3d\n\n"
		    "There are %d points per channels\n"
				"There are %d points in total (over all channels)\n\n"
				"keep 1 point every %%4d points (decimate)\n"
				"use %%R all points / %%r part of the file /\n %%r chunks of %%8d points",
		    filename, date_stamp, (bool_file_type==0) ? "NI-DAQ" : "DAQ/mx",
				user_header, channel_string, F_s, N_channels, (int)(N/N_channels), N);
	    i=win_scanf(s, &selected_channel_input, &selected_channel_output, &N_decimate, &do_scan_chunks, &chunk_size);
  	  free(s); s=NULL;
		  free(channel_string);
		  if (i==CANCEL) return(D_O_K);
		  
			if (do_scan_chunks!=0) return(win_printf_OK("not available yet..."));

      if ( (selected_channel_input<1) || (selected_channel_input>N_channels) )
				return(win_printf_OK("incorrect channel number (%d) for input !", selected_channel_input));
	    if ( (selected_channel_output<1) || (selected_channel_output>N_channels) )
				return(win_printf_OK("incorrect channel number (%d) for output !", selected_channel_output));

			N=N/N_channels;
			if (N_decimate<1) 		return(win_printf_OK("Very bad decimation !"));

			if (do_scan_chunks==1)
			{	s=my_sprintf(s,"There are %d points in time, for %d different channels\n"
					"(%d points in total)\n"
					"decimation by a factor %d will keep {\\color{red}%d points in time}\n\n"
					"load points in the interval [A B[ with\n"
					"A = %%8d (0<=A<%d)\n"
					"B = %%8d (0<B<=%d)", N, N_channels, N*N_channels, N_decimate, N/N_decimate, N/N_decimate, N/N_decimate);
				i=win_scanf(s, &j_start, &j_end);
				free(s); s=NULL;
				if (i==CANCEL) return(D_O_K);
				if ( (j_start<0) || (j_start>=N/N_decimate) )
					return(win_printf("bad interval!"));
			}
	    else if (do_scan_chunks==2)
	    {
	    }
	    else // all points, complete file
	    {	j_start = 0;
		    j_end   = N/N_decimate;
	    }
		}
		f_acq = (float)F_s/(float)N_decimate;
		nx	  = (int)j_end-(int)j_start;

   
		if (k==0)
		{   // below, we prepare the FFT
		   n_fft     = (int)f_acq;
		  n_overlap = (int)((float)percent_overlap*(float)100.);

          s=my_sprintf(s, "{\\pt14\\color{yellow}Response function between channel %d and %d}\n\n"
			   "dataset has %d points. \n"
			   "- number of points of fft window : %%8d\n"
			   "- overlap between windows       : %%8d\n"
			   "     overlap in %%R points or %%r in percents\n"
			   "- acquisition frequency (in Hz) : %%8f\n"
			   "     (for normalization)\n\n"
			   "%%b use Hanning window ?\n"
			   "%%R Normal response H=%d/%d or %%r Cross-response H=<%d %d*> / (%d %d*)\n",
			   selected_channel_input, selected_channel_output, nx,
			   selected_channel_output, selected_channel_input, selected_channel_output, selected_channel_input, selected_channel_input, selected_channel_input);
	    i=win_scanf(s, &n_fft, &n_overlap, &bool_overlap_type, &f_acq, &bool_hanning, &bool_crossresponse);
	    free(s); s=NULL;
      if (i==CANCEL)	return(D_O_K);
			percent_overlap = (float)n_overlap/((float)(100.));
			n_overlap       =(int)(percent_overlap*(float)n_fft);

			if ( (n_overlap>=n_fft) || (percent_overlap>=1.) )
			return(win_printf_OK("overlap is too large compared to n-fft\n"
				"in points    : %d>=%d\n"
				"in percents : %d>=100", n_overlap, n_fft, (int)((float)100.*percent_overlap)));
				
			reset_response(current_response, n_fft/2+1, n_fft, f_acq);
			response = init_response(n_fft/2+1, n_fft, f_acq);
		}

 		tampon_float = (float*)calloc(N_channels, sizeof(float)); // size = number of channels in file
    tmp_in       = (float*)calloc((j_end-j_start), sizeof(float)); if (tmp_in==NULL) return(win_printf_OK("out of memory"));
	  tmp_out	     = (float*)calloc((j_end-j_start), sizeof(float)); if (tmp_out==NULL) return(win_printf_OK("out of memory"));

// win_printf(" f acq = %g\n N= %d\n n channels = %d\n nx = %d\n start at %d and end at %d\n"
//				" N channels = %d\n n fft = %d\n N decimate %d",
//				f_acq, N, n_chan, nx, j_start, j_end, N_channels, n_fft, N_decimate);

		data_file=fopen(fullfilename, "rb");
	  fseek(data_file, current_position, SEEK_SET); // we move from the beginning of the file, to the end of the header
	  fseek(data_file, j_start*N_decimate*N_channels*sizeof(float), SEEK_CUR);

	  for (i=0; i<(j_end-j_start); i++) { tmp_in[i]=(float)0.; tmp_out[i]=(float)0.; }
				
	  sprintf(message_to_spit, "(%d/%d) %s loading data", k+1, n_files, filename); spit_unallocated(message_to_spit);
	
	  for (j=j_start; j<j_end; j++)
	  {	 moyenne_in  = 0.;
		   moyenne_out = 0.;
		   for (i=0; i<N_decimate; i++)
		   {	fread(tampon_float, sizeof(float), N_channels, data_file);
#ifndef XV_MAC	
			    swap_bytes(tampon_float, N_channels, sizeof(float));
#else 
#ifndef MAC_POWERPC				
			    swap_bytes(tampon_float, N_channels, sizeof(float));
#endif
#endif
			    moyenne_in  += (double)tampon_float[selected_channel_input-1];
			    moyenne_out += (double)tampon_float[selected_channel_output-1];
		   }
		   tmp_in [j-j_start] = (float)moyenne_in /(float)N_decimate;
		   tmp_out[j-j_start] = (float)moyenne_out/(float)N_decimate;
	  }
	  fclose(data_file);
	
	  sprintf(message_to_spit, "(%d/%d) %s computing response", k+1, n_files, filename); spit_unallocated(message_to_spit);
	
	  if (bool_crossresponse==0)
		  i=compute_response_from_float_data(response, tmp_out, tmp_in, nx, f_acq, n_fft, n_overlap, bool_hanning);
	  if (bool_crossresponse==1)
		  i=compute_response_from_float_data_using_correlation(response, tmp_out, tmp_in, nx, f_acq, n_fft, n_overlap, bool_hanning);
	  if (i==0) return(win_printf_OK("nothing done with your values !"));

	  free(tmp_in);
	  free(tmp_out);
	  free(tampon_float);

	 	for (i=0; i<(n_fft/2+1); i++)
		{	  if (k==0) current_response->f[i]    = response->f[i];
			  current_response->r[i][0] += response->r[i][0];
			  current_response->r[i][1] += response->r[i][1];
		}
  }//end of the loop over k, the file number
  
  if (n_files>1)
  {  for (i=0; i<(n_fft/2+1); i++)
		{	  current_response->r[i][0] /= (double)n_files;
			  current_response->r[i][1] /= (double)n_files;
		}
  }
  
  current_response->mode |= RESPONSE_CONTINUOUS;
  current_response->info=my_sprintf(current_response->info,"%s\n%s\nf_{acq}=%gHz\n%s\ndecimation 1/%d points\n"
                                    "response %d/%d, over windows of %d points, f_{acq}=%gHz, %s%s",
			                       filename, date_stamp, F_s, user_header, N_decimate,
                             selected_channel_output, selected_channel_input, n_fft, f_acq,
                             (bool_hanning==1) ? "Hanning window" : "no windowing");
  if (index & DISKOP_SEVERAL_FILES) current_response->info=my_sprintf(current_response->info, "%d files used", n_files);

  // now we create a new plot and a new dataset to show the modulus of the response function:	
	opd = create_and_attach_one_plot(pr, (n_fft/2)+1, (n_fft/2)+1, 0);
  if (opd == NULL)    return win_printf_OK("Cannot allocate plot");
  dsd = opd->dat[0];
	if (dsd == NULL)    return win_printf_OK("Cannot allocate dataset");
    
	for (i=0 ; i<=(n_fft/2); i++)
  {  	dsd->xd[i] = current_response->f[i];
     	dsd->yd[i] = (float)sqrt( current_response->r[i][0]*current_response->r[i][0] + current_response->r[i][1]*current_response->r[i][1] );
  }

  set_ds_source(dsd, "%s acquisition, %s\n%s\nf_{acq}=%gHz\n%s\ndecimation 1/%d points",
			   (bool_file_type==0) ? "NI-DAQ" : "DAQ/mx",
          filename, date_stamp, F_s, user_header, N_decimate);
  if (index & DISKOP_SEVERAL_FILES)  current_response->info=my_sprintf(current_response->info, "%d files used", n_files);

  dsd->treatement=my_sprintf(NULL,"response %d/%d, over %d points, f_{acq}=%gHz, %s",
			selected_channel_output, selected_channel_input, n_fft, f_acq, (bool_hanning==1) ? "Hanning window" : "no windowing");
  if (index & DISKOP_SEVERAL_FILES)  current_response->info=my_sprintf(current_response->info, "%d files used", n_files);
        
  if (n_files==1)
  {   opd->filename   = strdup(filename);      
      set_plot_title  (opd, "Response %s %s",   filename, date_stamp);
  }
  else
  {	  opd->filename   = strdup(root_filename);
      set_plot_title  (opd, "Response %s %s (%d files)", root_filename, date_stamp, n_files);
  }

	opd->dir 		    = Mystrdup(pathname);
    set_plot_x_title(opd, "frequency (Hz)");
	set_plot_y_title(opd, "response H=%d/%d", selected_channel_output, selected_channel_output);
   
	opd->iopt |= XLOG;
    opd->iopt |= YLOG;

    free(user_header);
	free(date_stamp);
	
 	return(refresh_plot(pr, UNCHANGED));     
} /* end of function 'do_diskop_PSD' */





void spit_unallocated(char *message)
{	size_t	l_max=55;
	char 	white_string[64]="                                              ";
	
	if (strlen(message)<l_max) 	strncat (message, white_string, l_max-strlen(message));
	textout_ex(screen, font, message, 40, 40, makecol(100,155,155), makecol(2,1,1));
	return;
}



MENU *diskop_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"decimate a series of NI files",	do_diskop_decimate_NI_files,	NULL, 0, NULL);	
	add_item_to_menu(mn,"extract NI files to .dat bin files",	do_diskop_extract_NI_files,	NULL, 0, NULL);	
	add_item_to_menu(mn,"\0",					            0,			                 NULL, 0, NULL);	
	add_item_to_menu(mn,"PSD (amplitude)  (1 file)",		do_diskop_PSD,			NULL, PSD_AMP | DISKOP_ONE_FILE, NULL);
	add_item_to_menu(mn,"PSD (energy)     (1 file)",		do_diskop_PSD,			NULL, PSD_ENERGY | DISKOP_ONE_FILE, NULL);
	add_item_to_menu(mn,"compute response (1 file)",		do_diskop_compute_response,	NULL, DISKOP_ONE_FILE, NULL);
	add_item_to_menu(mn,"\0",					            0,				        NULL, 0, NULL);	
	add_item_to_menu(mn,"PSD (amplitude)  (N files)",		do_diskop_PSD,			NULL, PSD_AMP | DISKOP_SEVERAL_FILES, NULL);
	add_item_to_menu(mn,"PSD (energy)     (N files)",		do_diskop_PSD,			NULL, PSD_ENERGY | DISKOP_SEVERAL_FILES, NULL);
	add_item_to_menu(mn,"compute response (N files)",		do_diskop_compute_response,	NULL, DISKOP_SEVERAL_FILES, NULL);

	return(mn);
}

int	diskop_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("disk operations", NULL, diskop_plot_menu(), 0, NULL);
	return D_O_K;
}


int	diskop_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu, "disk operations",	NULL, NULL);
	return D_O_K;
}
#endif

