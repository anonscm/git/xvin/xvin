/****************************************************************/
/*    Plug-in program for random number generation in XVin	*/
/*								*/
/* 	V. Croquette, N. Garnier				*/
/*	18 novembre 2003					*/
/****************************************************************/
#ifndef _RANDOM_C_
#define _RANDOM_C_

#include "allegro.h"
#include "xvin.h"
#define GSL_DLL
# define DLL_EXPORT
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#define BUILDING_PLUGINS_DLL
#include "random.h"

const gsl_rng_type	*random_generator_type;		/* type of the random generator 	*/
gsl_rng 			*r;  						/* random generator 				*/
	
unsigned long int seed = 123;
int seed_type=SEED_CLOCK;
	

int init_random_generator(gsl_rng **rl)
{	
	random_generator_type = gsl_rng_default;
	gsl_rng_env_setup();	// 3 lines to init the random number generator
	*rl = gsl_rng_alloc(random_generator_type);
	
	if (seed_type==SEED_CLOCK) gsl_rng_set (*rl, (unsigned long int)clock());
	if (seed_type==SEED_FIXED) gsl_rng_set (*rl, (unsigned long int)seed);

	return(0); 	
}
	
	
	
int do_change_seed(void)
{	int user_seed_type;
	int i;

	if (updating_menu_state != 0)	return D_O_K;	
	
	if (seed_type==SEED_CLOCK) user_seed_type=0;
	if (seed_type==SEED_FIXED) user_seed_type=1;
	
	i=win_scanf("What seed do you prefer ?\n"
			"%R use clock at runtime (prefered)\n"
			"%r fixed, with value %8d",
			&user_seed_type, &seed);

//	gsl_rng_default_seed

	if (i==WIN_CANCEL) return(D_O_K);
	
	if (user_seed_type==0) seed_type = SEED_CLOCK;
	if (user_seed_type==1) seed_type = SEED_FIXED;

	return(D_O_K);
}
	

int do_change_random_generator(void)
{	static char first_time=1;

	if (first_time==1) 
	{	random_generator_type = gsl_rng_default;
		first_time=0;
	}

	if (updating_menu_state != 0)	return D_O_K;	

// 	eventually write something here...
	win_printf_OK("not implemented yet");

	return(D_O_K);
}


int do_generate_random_number(void)
{	register int i;
	char	*info=NULL;
	pltreg	*pr;
	O_p	*op;
	d_s	*ds, *ds2=NULL;
	int 	index, nh;
	static float x1=0, x2=1, x3=0;


	if (updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	
	if (key[KEY_LSHIFT]) 	
	{ 	info = my_sprintf(info,"This routine generates randomly a series of values\n");
		if (index == RANDOM_GAUSSIAN)
		{ info = my_sprintf(info,"from Gaussian distribution\n\n"
				"p(x) dx = {1 \\over \\sqrt{2 \\pi \\sigma^2}} exp (-x^2 / 2\\sigma^2) dx\n"); }
        	else if (index == RANDOM_GAUSSIAN_TAIL)
		{ info = my_sprintf(info,"from the upper tail of a Gaussian distribution with standard deviation \\sigma.\n" 
				"The values returned are larger than the lower limit a,\n"
				"which must be positive."); }
		else if (index == RANDOM_EXPONENTIAL)
		info = my_sprintf(info,"from the exponential distribution with mean mu:\n"
        			"p(x) dx = {1 \\over \\mu} exp(-x/\\mu) dx");
		else if (index == RANDOM_LAPLACE)
		info = my_sprintf(info,"from the Laplace distribution with width a");
		else if (index == RANDOM_EXPPOWER)
		info = my_sprintf(info,"from the exponential power distribution with scale parameter a and exponent b:\n\n"
				"p(x) dx = {1 \\over 2 a \\Gamma(1+1/b)} exp(-|x/a|^b) dx\n\n"
				"For b = 1 this reduces to the Laplace distribution.\n" 
				"For b = 2 it has the same form as a gaussian distribution,\n"
				"but with a = \\sqrt{2} \\sigma.");
		else if (index == RANDOM_CAUCHY)
		info = my_sprintf(info,"from the Cauchy distribution with scale parameter a:\n\n"
				"p(x) dx = {1 \\over a\\pi (1 + (x/a)^2) } dx");
		else if (index == RANDOM_RAYLEIGH)
		info = my_sprintf(info,"from the Rayleigh distribution with scale parameter \\sigma :\n\n"
				"p(x) dx = {x \\over \\sigma^2} exp(- x^2/(2 \\sigma^2)) dx\n"
				"for x > 0."); 
		else if (index == RANDOM_RAYLEIGH_TAIL)
		info = my_sprintf(info,"from the tail of the Rayleigh distribution\n"
				"with scale parameter \\sigma  and a lower limit of a:\n"
				"p(x) dx = {x \\over \\sigma^2} exp ((a^2 - x^2) / (2 \\sigma^2)) dx\n"
				"for x > a.");
		else if (index == RANDOM_LANDAU)
		info = my_sprintf(info,"from the Landau distribution, defined analytically by the complex integral:\n\n"
				"p(x) = (1/(2 \\pi i)) \\int_{c-i\\infty}^{c+i\\infty} ds exp(s log(s) + x s)\n\n" 
				"For numerical purposes it is more convenient to use the following equivalent form of the integral:\n\n"
				"p(x) = (1/\\pi) \\int_0^\\infty dt exp(-t log(t) - x t) sin(\\pi t).");
		else if (index == RANDOM_LEVY)
		info = my_sprintf(info,"from the Levy symmetric stable distribution with scale c and exponent \\alpha.\n"
				"The symmetric stable probability distribution is defined by a fourier transform:\n\n"
				"p(x) = {1 \\over 2 \\pi} \\int_{-\\infty}^{+\\infty} dt exp(-it x - |c t|^\\alpha)\n\n"
				"There is no explicit solution for the form of p(x)\n"
				"For \\alpha = 1 the distribution reduces to the Cauchy distribution.\n"
				"For \\alpha = 2 it is a Gaussian distribution with \\sigma = \\sqrt{2} c.\n"
				"For \\alpha < 1 the tails of the distribution become extremely wide.\n"
				"The algorithm only works for 0 < \\alpha <= 2.\n");
		else if (index == RANDOM_LEVY_SKEW)
		info = my_sprintf(info,"from the Levy skew stable distribution with scale c, exponent \\alpha and skewness parameter \\beta.\n" 
				"The skewness parameter must lie in the range [-1,1].\n" 
				"The Levy skew stable probability distribution is defined by a fourier transform:\n\n"
				"p(x) = {1 \\over 2 \\pi} \\int_{-\\infty}^{+\\infty} dt "
				"exp(-it x - |c t|^\\alpha (1-i \\beta sign(t) tan(\\pi \\alpha/2)))\n\n"
				"For \\alpha = 1 the term tan(\\pi \\alpha/2) is replaced by -(2/\\pi)log|t|.\n" 
                		"For \\alpha = 2 the distribution reduces to a Gaussian distribution with \\sigma = \\sqrt{2} c\n"
				"and the skewness parameter has no effect.\n"
				"For \\alpha < 1 the tails of the distribution become extremely wide.\n"
				"The symmetric distribution corresponds to \\beta = 0.\n"
				"The algorithm only works for 0 < \\alpha <= 2.");
		else if (index == RANDOM_GAMMA)
		info = my_sprintf(info,"from the gamma distribution:\n\n"
				"p(x) dx = {1 \\over \\Gamma(a) b^a} x^{a-1} e^{-x/b} dx\n\n"
				"for x > 0.");
		else if (index == RANDOM_FLAT)
		info = my_sprintf(info,"from the flat (uniform) distribution in [a b]:\n\n"
				"p(x) dx = {1 \\over (b-a)} dx  if a <= x < b\n"
				"and 0 otherwise."); 
		else if (index == RANDOM_LOGNORMAL)
		info = my_sprintf(info,"from the lognormal distribution:\n\n"
				"p(x) dx = {1 \\over x \\sqrt{2 \\pi \\sigma^2} } exp(-(ln(x) - \\zeta)^2/2 \\sigma^2) dx\n\n"
				"for x > 0."); 
		else if (index == RANDOM_CHISQ)
		info = my_sprintf(info,"from the chi-squared distribution with \\nu degrees of freedom:\n"
				"p(x) dx = {1 \\over 2 \\Gamma(\\nu/2) } (x/2)^{\\nu/2 - 1} exp(-x/2) dx\n\n"
				"for x >= 0.");
		else if (index == RANDOM_PARETO)
		info = my_sprintf(info,"from the Pareto distribution of order a:\n\n" 
				"p(x) dx = (a/b) / (x/b)^{a+1} dx\n\n"
				"for x >= b."); 
		else if (index == RANDOM_GUMBEL1)
		info = my_sprintf(info,"from the Type-1 Gumbel distribution:\n\n"
				"p(x) dx = a b exp(-(b exp(-ax) + ax)) dx\n\n"
				"for -\\infty < x < \\infty.");
		else if (index == RANDOM_GUMBEL2)
		info = my_sprintf(info,"from the Type-2 Gumbel distribution:\n\n"
				"p(x) dx = a b x^{-a-1} exp(-b x^{-a}) dx\n\n"
				"for 0 < x < \\infty."); 
		else if (index == RANDOM_UNIFORM)
		info = my_sprintf(info,"from the flat (uniform) distribution in [0 1[");
		else 
		{ info = my_sprintf(info,"Hum... this function do not seem to be implemented yet..."); }
	
		return win_printf_OK(info);
	}

    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)  return win_printf_OK("cannot find data!");
	if (ds == NULL)   return (win_printf_OK("can't find data set"));
    	nh = ds->nx;

	info = my_sprintf(info,"random, "); // all info will start with those words
	
	init_random_generator(&r);

	if (index == RANDOM_GAUSSIAN)
	{	if (win_scanf("Gaussian distribution\n"
			"p(x) dx = {1 \\over \\sqrt{2 \\pi \\sigma^2}} exp (-x^2 / 2\\sigma^2) dx\n"
			"mean ? %f variance \\sigma ?%f How many points ?%d",&x1,&x2,&nh) ==WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_gaussian(r, (double)x2) + x1; }
		info = my_sprintf(info,"Gaussian distribution <y>=%f \\sigma=%f", x1, x2);
	}
	else if (index == RANDOM_GAUSSIAN_TAIL)
	{	// This function provides random variates from the upper tail of a Gaussian distribution 
		// with standard deviation sigma. The values returned are larger than the lower limit a, 
		// which must be positive.
		if (win_scanf("Gaussian tail distribution\n lower limit ? %f variance ?%f How many points ?%d",&x1,&x2,&nh)
			 ==WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_gaussian_tail(r, (double)x1, (double)x2);}
		info = my_sprintf(info,"Gaussian tail distribution, above a=%f, \\sigma=%f",x1,x2);
	}
	else if (index == RANDOM_EXPONENTIAL)
	{	// This function returns a random variate from the exponential distribution with mean mu.
		if (win_scanf("Exponential distribution\n"
			"p(x) dx = {1 \\over \\mu} exp(-x/\\mu) dx\n mean \\mu?%f How many points ?%d",&x1,&nh) ==WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_exponential(r, (double)x1);}
		info = my_sprintf(info,"Exponential distribution mean=%f",x1);
	}
	else if (index == RANDOM_LAPLACE)
	{	// This function returns a random variate from the Laplace distribution with width a.
		if (win_scanf("Laplace distribution\n width ?%f How many points ?%d",&x1,&nh) ==WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_laplace(r, (double)x1);}
		info = my_sprintf(info,"Laplace distribution width=%f",x1);
	}
	else if (index == RANDOM_EXPPOWER)
	{	// This function returns a random variate from the exponential power distribution
		// with scale parameter a and exponent b. The distribution is
		// p(x) dx = {1 \over 2 a \Gamma(1+1/b)} \exp(-|x/a|^b) dx
		// For b = 1 this reduces to the Laplace distribution. 
		// For b = 2 it has the same form as a gaussian distribution, but with a = \sqrt{2} \sigma.
		if (win_scanf("Exponential power distribution\n scale parameter ?%f exponent ?%f"
			" How many points ?%d",&x1,&x2,&nh) ==WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_exppow(r, (double)x1, (double)x2);}
		info = my_sprintf(info,"Exponential power distribution scale=%f, power=%f",x1,x2);
	}
	else if (index == RANDOM_CAUCHY)
	{	// This function returns a random variate from the Cauchy distribution with scale parameter a.
		// The probability distribution for Cauchy random variates is,
		// p(x) dx = {1 \over a\pi (1 + (x/a)^2) } dx
		if (win_scanf("Cauchy distribution\n scale parameter ?%f How many points ?%d",&x1,&nh) ==WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_cauchy(r, (double)x1);}
		info = my_sprintf(info,"Cauchy distribution scale=%f",x1);
	}
	else if (index == RANDOM_RAYLEIGH)
	{	// This function returns a random variate from the Rayleigh distribution 
		// with scale parameter sigma. The distribution is,
		// p(x) dx = {x \over \sigma^2} \exp(- x^2/(2 \sigma^2)) dx
		// for x > 0. 
		if (win_scanf("Rayleigh distribution\n scale parameter ?%f How many points ?%d",&x1,&nh) ==WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_rayleigh(r, (double)x1);}
		info = my_sprintf(info,"Rayleigh distribution scale=%f",x1);
	}
	else if (index == RANDOM_RAYLEIGH_TAIL)
	{	// This function returns a random variate from the tail of the Rayleigh distribution 
		// with scale parameter sigma and a lower limit of a. The distribution is,
		// p(x) dx = {x \over \sigma^2} \exp ((a^2 - x^2) /(2 \sigma^2)) dx
		// for x > a. 
		if (win_scanf("Rayleigh tail distribution\n"
				"p(x) dx = {x \\over \\sigma^2} exp ((a^2 - x^2) / (2 \\sigma^2)) dx\n"
				"scale parameter \\sigma ?%f lower limit a ?%f How many points ?%d",&x1,&x2,&nh) 
				==WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_rayleigh_tail(r, (double)x1, (double)x2);}
		info = my_sprintf(info,"Rayleigh tail distribution scale=%f, above %f",x1,x2);
	}
	else if (index == RANDOM_LANDAU)
	{	// This function returns a random variate from the Landau distribution. 
		// The probability distribution for Landau random variates is defined analytically by the complex integral,
		// p(x) = (1/(2 \pi i)) \int_{c-i\infty}^{c+i\infty} ds exp(s log(s) + x s) 
		// For numerical purposes it is more convenient to use the following equivalent form of the integral,
		// p(x) = (1/\pi) \int_0^\infty dt \exp(-t \log(t) - x t) \sin(\pi t).
		if (win_scanf("How many points ?%d",&nh) == WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_landau(r);}
		info = my_sprintf(info,"Landau distribution",x1,x2);
	}
	else if (index == RANDOM_LEVY)
	{	// This function returns a random variate from the Levy symmetric stable distribution with scale c 
		// and exponent alpha. The symmetric stable probability distribution is defined by a fourier transform,
		// p(x) = {1 \over 2 \pi} \int_{-\infty}^{+\infty} dt \exp(-it x - |c t|^alpha)
		// There is no explicit solution for the form of p(x)
		// For \alpha = 1 the distribution reduces to the Cauchy distribution. 
		// For \alpha = 2 it is a Gaussian distribution with \sigma = \sqrt{2} c. 
		// For \alpha < 1 the tails of the distribution become extremely wide.
		// The algorithm only works for 0 < alpha <= 2. 
		if (win_scanf("Levy symmetric stable distribution\n scale ?%f exponent ?%f"
			" How many points ?%d",&x1,&x2,&nh) == WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_levy(r, (double)x1, (double)x2);}
		info = my_sprintf(info,"Levy symmetric stable distribution scale=%f, exponent=%f",x1,x2);
	}
	else if (index == RANDOM_LEVY_SKEW)
	{	// This function returns a random variate from the Levy skew stable distribution with scale c, 
		// exponent alpha and skewness parameter beta. The skewness parameter must lie in the range [-1,1]. 
		// The Levy skew stable probability distribution is defined by a fourier transform,
		// p(x) = {1 \over 2 \pi} \int_{-\infty}^{+\infty} dt \exp(-it x - |c t|^alpha (1-i beta sign(t) tan(pi alpha/2)))
		// When \alpha = 1 the term \tan(\pi \alpha/2) is replaced by -(2/\pi)\log|t|. 
		// There is no explicit solution for the form of p(x) and the library does not define a 
		// corresponding pdf function. 
		// For \alpha = 2 the distribution reduces to a Gaussian distribution with \sigma = \sqrt{2} c 
		// and the skewness parameter has no effect. 
		// For \alpha < 1 the tails of the distribution become extremely wide. 
		// The symmetric distribution corresponds to \beta = 0.
		// The algorithm only works for 0 < alpha <= 2. 
		if (win_scanf("Levy skew stable distribution\n scale parameter ?%f exponent ?%f"
				" skewness parameter? %f How many points ?%d",&x1,&x2,&x3,&nh) == WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_levy_skew(r, (double)x1, (double)x2, (double)x3);}
		info = my_sprintf(info,"Levy skew stable distribution scale=%f, power=%f, skewness=%f",x1,x2,x3);
	}
	else if (index == RANDOM_GAMMA)
	{	// This function returns a random variate from the gamma distribution. The distribution function is,
		// p(x) dx = {1 \over \Gamma(a) b^a} x^{a-1} e^{-x/b} dx
		// for x > 0. 
		if (win_scanf("Gamma distribution\n p(x) dx = {1 \\over \\Gamma(a) b^a} x^{a-1} e^{-x/b} dx\n"
				"a ?%f b ?%f How many points ?%d",&x1,&x2,&nh) == WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_gamma(r, (double)x1, (double)x2);}
		info = my_sprintf(info,"Gamma distribution a=%f, b=%f",x1,x2);
	}
	else if (index == RANDOM_FLAT)
	{	// This function returns a random variate from the flat (uniform) distribution from a to b. 
		// The distribution is,
		// p(x) dx = {1 \over (b-a)} dx
		// if a <= x < b and 0 otherwise. 
		if (win_scanf("Flat (uniform) distribution in [a b]\n a=?%f b=?%f How many points ?%d",
					&x1,&x2,&nh) == WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_flat(r, (double)x1, (double)x2);}
		info = my_sprintf(info,"Flat distribution in [%f, %f]",x1,x2);
	}
	else if (index == RANDOM_LOGNORMAL)
	{	// p(x) dx = {1 \over x \sqrt{2 \pi \sigma^2} } \exp(-(\ln(x) - \zeta)^2/2 \sigma^2) dx
		// for x > 0. 
		if (win_scanf("Lognormal distribution\n"
				"p(x) dx = {1 \\over x \\sqrt{2 \\pi \\sigma^2} } exp(-(ln(x) - \\zeta)^2/2 \\sigma^2) dx\n"
				"zeta \\zeta ?%f sigma \\sigma ?%f How many points ?%d",&x1,&x2,&nh) == WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_lognormal(r, (double)x1, (double)x2);}
		my_sprintf(info,"lognormal distribution zeta=%f, sigma=%f",x1,x2);
	}
	else if (index == RANDOM_CHISQ)
	{	// This function returns a random variate from the chi-squared distribution 
		// with nu degrees of freedom. The distribution function is,
		// p(x) dx = {1 \over 2 \Gamma(\nu/2) } (x/2)^{\nu/2 - 1} \exp(-x/2) dx
		// for x >= 0. 
		if (win_scanf("Chi-squared \\Chi^2 distribution\n number of freedom degrees ?%f"
				" How many points ?%d",&x1,&nh) == WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_chisq(r, (double)x1);}
		info = my_sprintf(info,"Chi-squared distribution n=%f",x1);
	}
	else if (index == RANDOM_PARETO)
	{	// This function returns a random variate from the Pareto distribution of order a. 
		// The distribution function is p(x) dx = (a/b) / (x/b)^{a+1} dx
		// for x >= b. 
		if (win_scanf("Pareto distribution\n p(x) dx = (a/b) / (x/b)^{a+1} dx\n"
				"a ?%f b ?%f How many points ?%d",&x1,&x2,&nh) == WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_pareto(r, (double)x1, (double)x2);}
		info = my_sprintf(info,"Pareto distribution a=%f, b=%f",x1,x2);
	}
	else if (index == RANDOM_GUMBEL1)
	{	// This function returns a random variate from the Type-1 Gumbel distribution. 
		// The Type-1 Gumbel distribution function is,
		// p(x) dx = a b \exp(-(b \exp(-ax) + ax)) dx
		// for -\infty < x < \infty. 
		if (win_scanf("type-1 Gumbel distribution\n p(x) dx = a b exp(-(b exp(-ax) + ax)) dx\n"
				"a ?%f b ?%f How many points ?%d",&x1,&x2,&nh) == WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_gumbel1(r, (double)x1, (double)x2);}
		info = my_sprintf(info,"type-1 Gumbel distribution a=%f, b=%f",x1,x2);
	}
	else if (index == RANDOM_GUMBEL2)
	{	// This function returns a random variate from the Type-2 Gumbel distribution.
		// The Type-2 Gumbel distribution function is,
		// p(x) dx = a b x^{-a-1} \exp(-b x^{-a}) dx
		// for 0 < x < \infty. 
		if (win_scanf("type-2 Gumbel distribution\n p(x) dx = a b x^{-a-1} exp(-b x^{-a}) dx\n"
				"a ?%f b ?%f How many points ?%d",&x1,&x2,&nh) == WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_ran_gumbel2(r, (double)x1, (double)x2);}
		info = my_sprintf(info,"type-2 Gumbel distribution a=%f, b=%f",x1,x2);
	}
	else if (index == RANDOM_UNIFORM)
	{	// This function returns a random variate from the flat (uniform) distribution.
		// there is no upper or lower limit for x.
		if (win_scanf("Uniform distribution in [0 1[\n How many points ?%d",
					&nh) == WIN_CANCEL) return OFF;
		if ((ds2 = create_and_attach_one_ds(op, nh, nh, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");
		for (i=0; i<nh; i++) 
		{	ds2->yd[i] = (float)gsl_rng_uniform(r);}
		info = my_sprintf(info,"Uniform distribution in [0, 1[");
	}
	else 
	{	return(win_printf_OK("This distribution is not implemented yet!"));
	}

	gsl_rng_free(r);

	for (i=0; i<nh; i++) { ds2->xd[i]=i; }

	set_ds_source(ds2, "%s", info);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

MENU *random_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"change random generator", 	do_change_random_generator, NULL, 0,		NULL);
	add_item_to_menu(mn,"change seed", 				do_change_seed,				NULL, 0,		NULL);
	add_item_to_menu(mn,"\0", 			NULL,NULL,0,NULL);
	add_item_to_menu(mn,"flat (uniform) in [a b]", 	do_generate_random_number, NULL, RANDOM_FLAT,		NULL);
	add_item_to_menu(mn,"flat (uniform) in [0 1[", 	do_generate_random_number, NULL, RANDOM_UNIFORM,	NULL);
	add_item_to_menu(mn,"\0", 			NULL,NULL,0,NULL);
	add_item_to_menu(mn,"Cauchy",		 	do_generate_random_number, NULL, RANDOM_CAUCHY,		NULL);
	add_item_to_menu(mn,"chi-squared",	 	do_generate_random_number, NULL, RANDOM_CHISQ,		NULL);
	add_item_to_menu(mn,"exponential",	 	do_generate_random_number, NULL, RANDOM_EXPONENTIAL,	NULL);
	add_item_to_menu(mn,"exponential power", 	do_generate_random_number, NULL, RANDOM_EXPPOWER,	NULL);
	add_item_to_menu(mn,"gamma",		 	do_generate_random_number, NULL, RANDOM_GAMMA,		NULL);
	add_item_to_menu(mn,"Gaussian",		 	do_generate_random_number, NULL, RANDOM_GAUSSIAN,	NULL);
	add_item_to_menu(mn,"Gaussian tail",	 	do_generate_random_number, NULL, RANDOM_GAUSSIAN_TAIL,	NULL);
	add_item_to_menu(mn,"type-1 Gumble",	 	do_generate_random_number, NULL, RANDOM_GUMBEL1,	NULL);
	add_item_to_menu(mn,"type-2 Gumble",	 	do_generate_random_number, NULL, RANDOM_GUMBEL2,	NULL);
	add_item_to_menu(mn,"Laplace",		 	do_generate_random_number, NULL, RANDOM_LAPLACE,	NULL);
	add_item_to_menu(mn,"Landau",		 	do_generate_random_number, NULL, RANDOM_LANDAU,		NULL);
	add_item_to_menu(mn,"Levy alpha-stable", 	do_generate_random_number, NULL, RANDOM_LEVY,		NULL);
	add_item_to_menu(mn,"Levy skew alpha-stable", 	do_generate_random_number, NULL, RANDOM_LEVY_SKEW,	NULL);
	add_item_to_menu(mn,"log-normal",	 	do_generate_random_number, NULL, RANDOM_LOGNORMAL,	NULL);
	add_item_to_menu(mn,"Pareto",		 	do_generate_random_number, NULL, RANDOM_PARETO,		NULL);
	add_item_to_menu(mn,"Rayleigh",		 	do_generate_random_number, NULL, RANDOM_RAYLEIGH,	NULL);
	add_item_to_menu(mn,"Rayleigh tail",	 	do_generate_random_number, NULL, RANDOM_RAYLEIGH_TAIL,	NULL);
		
	return mn;
}


int random_main(int argc, char **argv)
{
    (void)argc;
    (void)argv;
	add_plot_treat_menu_item ( "random numbers", NULL, random_plot_menu(), 0, NULL);
	return D_O_K;
}


int random_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;  
	remove_item_to_menu(plot_treat_menu,"random numbers",	NULL, NULL);
	return D_O_K;
}
#endif

