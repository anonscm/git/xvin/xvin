#ifndef _RANDOM_H_
#define _RANDOM_H_

// definitions for measures on histograms/pdfs:
#define RANDOM_GAUSSIAN			0x0100
#define RANDOM_GAUSSIAN_TAIL        	0x0200 
#define RANDOM_BIVARIATE_GAUSSIAN      	0x0300 
#define RANDOM_EXPONENTIAL        	0x0400 
#define RANDOM_LAPLACE        		0x0500 
#define RANDOM_EXPPOWER     	   	0x0600 
#define RANDOM_CAUCHY       	 	0x0700 
#define RANDOM_RAYLEIGH      	  	0x0800 
#define RANDOM_RAYLEIGH_TAIL 	  	0x0900 
#define RANDOM_LANDAU        		0x0A00 
#define RANDOM_LEVY        		0x0B00 
#define RANDOM_LEVY_SKEW     	  	0x0C00 
#define RANDOM_GAMMA        		0x1000 
#define RANDOM_FLAT        		0x1100 
#define RANDOM_LOGNORMAL		0x1200
#define RANDOM_CHISQ        		0x1300 
#define RANDOM_PARETO			0x1400
#define RANDOM_GUMBEL1       		0x1500 
#define RANDOM_GUMBEL2        	 	0x1600 
#define RANDOM_UNIFORM			0x1700

#define SEED_FIXED				0x0100
#define SEED_CLOCK				0x0200

int init_random_generator(gsl_rng **r);

XV_FUNC(MENU*, random_plot_menu, (void));
XV_FUNC(int, random_main, (int argc, char **argv));
XV_FUNC(int, random_unload, (int argc, char **argv));

XV_FUNC(int, do_change_random_generator, (void) );
XV_FUNC(int, do_change_seed,			 (void) );
XV_FUNC(int, do_generate_random_number,  (void) );
#endif

