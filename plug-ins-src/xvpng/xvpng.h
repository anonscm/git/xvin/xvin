#ifndef _XVPNG_H_
#define _XVPNG_H_

PXV_FUNC(int, do_xvpng_average_along_y, (void));
PXV_FUNC(MENU*, xvpng_image_menu, (void));
PXV_FUNC(int, xvpng_main, (int argc, char **argv));
#endif

