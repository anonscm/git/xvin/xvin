#ifndef _DS_MERGE_H_
#define _DS_MERGE_H_

PXV_FUNC(int, do_ODspacetime_average_along_y, (void));
PXV_FUNC(MENU*, ds_merge_image_menu, (void));
PXV_FUNC(int, ds_merge_main, (int argc, char **argv));
#endif

