/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _DS_MERGE_C_
#define _DS_MERGE_C_

# include "allegro.h"
# include "xvin.h"
# include "plot_ds.h"
# include "string.h"
# include "ctype.h"
# include "stdlib.h"

/* If you include other regular header do it here*/ 
# include "xvplot.h"
# include "cardinal.h"
# include "../../src/menus/plot/treat/p_treat_p2im.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "ds_merge.h"
//place here other headers of this plugin

int add_ds_from_files(void)
{
  register int i;
  int index, index_end;
  pltreg *pr;
  O_p *op, *opn;
  d_s *ds;
  char c[512], basename[256];
  char f_line[256], dir[512], file[512];            
  FILE *fpi;
    
  if(updating_menu_state != 0)    return D_O_K;        

  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");

  strcpy(dir, op->dir);
  strcpy(file, op->filename);
  put_backslash(dir);
  //win_printf("%s",backslash_to_slash(dir)); 

  sprintf(c,"%s\%s", dir, file);
  //sprintf(c,"%s\\20080702_000.gr",dir);
  //win_printf("%s\n%s", &dir, &c);
  fpi = fopen(c,"r");
  if (fpi == NULL)
    {
      win_printf("cannot open %s !\n",c); /*bizarre, on dit que l'on arrive pas a ouvrir le directory alors que c'est le fichier qui nous int�resse. pas important*/
      return 1;
    }

  
  fclose(fpi);
  index = retrieve_basename_and_index(op->filename, strlen(op->filename), basename, 256);

  /*determine index_end (la boucle s'arr�te quand fpi es NULL, donc indx-1 pour avoir le dernier fichier). donc travailler avec fichier num�rot� 1, 2...3 et graber l'heure dans les headers*/
  for (index_end = index; ;index_end++)
    {
      sprintf(f_line,"%s%s%03d.gr",dir,basename,index_end);
      fpi = fopen(f_line,"r");
      if (fpi == NULL) break; 
      fclose(fpi);
    }
  index_end--;
      
  sprintf(c,"I am going to retreat acquisition files starting at %s%03d\n"
	  " until final index %%d",basename,index);
  win_scanf(c,&index_end);

  for (i = index + 1; i <= index_end; i++)
    {
      sprintf(f_line,"%s%03d.gr",basename,i);
      opn = create_plot_from_gr_file(f_line, dir);
      if (opn == NULL)  return win_printf_OK("could not load %s",f_line);
      else display_title_message("loaded %s",f_line);
      ds =  duplicate_data_set(opn->dat[0],NULL);
      add_data_to_one_plot(op, IS_DATA_SET, (void*)ds);
      free_one_plot(opn);
      refresh_plot(pr, UNCHANGED);
      auto_x_y_limit();
    }
  //mds->source = my_sprintf(NULL,"%s%s%03d-%03d.xv",op->dir,basename,index,index_end);


  //prn = build_plots_for_acq(mds, &bd_du);
  //prn->filename = my_sprintf(NULL,"%s%03d-%03d.xv",basename,index,index_end);
  //prn->path = strdup(dir);

  return D_O_K;    
} 

int order_all_ds_x(void)
{
  pltreg *pr;
  O_p *op;
  d_s *dsi, *ds;
  int n;
    
  if(updating_menu_state != 0)    return D_O_K;        

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)	return D_O_K;
  for (n = 0; n < op->n_dat; n++)
    {
      ds = op->dat[n];
      sort_ds_along_x(ds);
    }
  win_printf_OK("Ordering done");
  return D_O_K;   
}

int substract_ds_to_all_ds(void)
{
  pltreg *pr;
  O_p *op;
  d_s *ds, *bg;
  int i, n;
    
  if(updating_menu_state != 0)    return D_O_K;        

  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return D_O_K;
  bg = op->dat[0];
  for (n = 0; n < op->n_dat; n++)
    {
      ds = op->dat[n];
      for(i = 0; i < ds->mx; i++)
	ds->yd[i] -= bg->yd[i];
    }
  //refresh_plot(pr, UNCHANGED);
  //op->need_to_refresh = 1;
  win_printf_OK("Substraction done");

  return refresh_plot(pr, UNCHANGED);
}

int diff_growth_rate(void)
{	
  int i, n;
  float tlapse;
  pltreg *pr;
  O_p *opi, *opn; 
  d_s *dsi, *dsi2, *dsd = NULL;
  int  nx;

  if(updating_menu_state != 0)	return D_O_K;	

  if (key[KEY_LSHIFT])
    {	
      return win_printf("This routine computes growth rate for a time\n"
			"lapse of datasets");
    }	

  i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opi,&dsi);
  if ( i != 3)		return win_printf("cannot find data");

  for (n = 0; n < opi->n_dat-1; n++)
    {
      dsi = opi->dat[n];
      dsi2 = opi->dat[n+1];
      nx = dsi->nx;        
      tlapse = (float) ((int long long)dsi2->time - (int long long)dsi->time);
      tlapse /= 3600;
      if (n == 0)
        {
	  opn = create_and_attach_one_plot(pr, nx, nx, 0);
	  if (opn == NULL)		return win_printf("Cannot allocate plot");

	  if (opi->title   != NULL) set_plot_title(opn, opi->title);
	  if (opi->x_title != NULL) set_plot_x_title(opn, opi->x_title);
	  if (opi->x_prime_title != NULL) set_plot_x_prime_title(opn, opi->x_prime_title);
	  if (opi->y_title != NULL) set_plot_y_title(opn, "Growth rate");
	  opn->filename = Transfer_filename(opi->filename);
	  uns_op_2_op(opn, opi);
	  //	  uns_prime_op_2_op(opn, opi);

	  dsd = opn->dat[0];
        }
      else
        {	
	  dsd = create_and_attach_one_ds(opn, nx, nx, 0);
	  if (dsd == NULL)		return win_printf("Cannot allocate plot");
        }
      dsd->time = dsi->time;

      for (i = 0; i < nx; i++)
	{
	  dsd->xd[i] = dsi->xd[i];
	  dsd->yd[i] = (dsi2->yd[i] - dsi->yd[i]) / tlapse;
	}

      inherit_from_ds_to_ds(dsd,dsi); 
      refresh_plot(pr, UNCHANGED);
    }
  return refresh_plot(pr, UNCHANGED);
}	
 
int fir_all_ds(void)
{
  register int i, j, n;
  pltreg *pr;
  O_p *opi, *opn; 
  d_s *dsi, *dsd = NULL;
  int  nx;
  static int nfir = 32;

  if(updating_menu_state != 0)	return D_O_K;	

  if (key[KEY_LSHIFT])
    {	
      return win_printf("This routine creates a new plot containing\n"
			"the simple Finite Inpulse Response filtering\n"
			"of all data sets. This filtering corresponds\n"
			"to sliding averaging over a finite number of points");
    }	
    
  i = win_scanf("FIR filtering over %d points",&nfir);
  if (i == CANCEL)	return 0;

  i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opi,&dsi);
  if ( i != 3)		return win_printf("cannot find data");

  for (n = 0; n < opi->n_dat; n++)
    {
      dsi = opi->dat[n];
      nx = dsi->nx;         
      if (n == 0)
        {
	  opn = create_and_attach_one_plot(pr, nx-nfir,nx-nfir, 0);
	  if (opn == NULL)		return win_printf("Cannot allocate plot");

	  if (opi->title   != NULL) set_plot_title(opn, opi->title);
	  if (opi->x_title != NULL) set_plot_x_title(opn, opi->x_title);
	  if (opi->x_prime_title != NULL) set_plot_x_prime_title(opn, opi->x_prime_title);
	  if (opi->y_title != NULL) set_plot_y_title(opn, opi->y_title);
	  opn->filename = Transfer_filename(opi->filename);
	  uns_op_2_op(opn, opi);
	  //	  uns_prime_op_2_op(opn, opi);

	  dsd = opn->dat[0];
        }
      else
        {	
	  dsd = create_and_attach_one_ds(opn, nx-nfir,nx-nfir, 0);
	  if (dsd == NULL)		return win_printf("Cannot allocate plot");
        }
      dsd->time = dsi->time;
      for (i = 0; i < nx-nfir; i++)
	{
	  for (j = 0, dsd->xd[i] = dsd->yd[i] = 0; j < nfir; j++)
	    {
	      dsd->xd[i] += dsi->xd[i+j];
	      dsd->yd[i] += dsi->yd[i+j];
	    }
	  dsd->xd[i] /= nfir;
	  dsd->yd[i] /= nfir;
	}

      inherit_from_ds_to_ds(dsd,dsi); 
      set_formated_string(&dsd->treatement,"FIR filter over %d points",nfir);	

      refresh_plot(pr, UNCHANGED);
    }
  return refresh_plot(pr, UNCHANGED);
}

int od_treatment(void)
{
  int order=0, bg_rm=0;

  if(updating_menu_state != 0)	return D_O_K;	

  win_scanf("This routine analyses od profile\n"
	   "timelapse data\n\n"
	   "%b  Need to reorder datasets\n"
	   "%b  Need to remove background\n", &order, &bg_rm);

  add_ds_from_files();
  if (order) order_all_ds_x();
  if (bg_rm) substract_ds_to_all_ds();
  fir_all_ds();
  do_plot_to_im();

  return D_O_K;
}

MENU *ds_merge_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"add all datasets", add_ds_from_files,NULL,0,NULL);
	add_item_to_menu(mn,"order datasets along x", order_all_ds_x,NULL,0,NULL);
	add_item_to_menu(mn,"substract selected ds to all ds", substract_ds_to_all_ds,NULL,0,NULL);
	add_item_to_menu(mn,"Compute growth rate", diff_growth_rate,NULL,0,NULL);
	add_item_to_menu(mn,"FIR on all datasets", fir_all_ds,NULL,0,NULL);
	add_item_to_menu(mn,"Plot -> Image", do_plot_to_im,NULL,0,NULL);
	add_item_to_menu(mn,"OD treatment", od_treatment,NULL,0,NULL);
	return mn;
}

int	ds_merge_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "Merge datasets", NULL, ds_merge_image_menu(), 0, NULL);
	return D_O_K;
}

int	ds_merge_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "Merge Datasets", NULL, NULL);
	return D_O_K;
}
#endif

 
