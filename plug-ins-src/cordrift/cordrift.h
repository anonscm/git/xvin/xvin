#pragma once
PXV_FUNC(int, do_cordrift_rescale_plot, (void));
PXV_FUNC(MENU*, cordrift_plot_menu, (void));
PXV_FUNC(int, do_cordrift_rescale_data_set, (void));
PXV_FUNC(int, cordrift_main, (int argc, char **argv));
PXV_FUNC(O_p*, cordrift_remove_simple_high_derivative_part_of_each_data_set, (O_p *op, float deltaz, int dn, int nei));

PXV_FUNC(int, correct_blocking_for_dsDNA_oligo_length, (d_s *dsi, int mode, float oligo_dz));
PXV_FUNC(O_p*, cordrift_remove_high_derivative_part_of_data_set, (O_p *op, int nei, int nmin, float deltaz , int repeat, int max, int ex_size, float p, float sigm));
PXV_FUNC(d_s*, cordrift_colapse_all_data_set_in_one, (O_p *op));

PXV_FUNC(d_s*, cordrift_simple_remove_high_derivative_part_of_ds, (d_s *dsi, float deltaz, int dn, int nei)); // , d_s *dsdc

PXV_FUNC(O_p*, cordrift_remove_and_cut_simple_high_derivative_part_of_data_set, (O_p *op, float deltaz, int dn, int nei, int nmin));
PXV_FUNC(d_s *, cordrift_colapse_all_data_set_in_one_by_diff_histo, (O_p *op, float sigma));
PXV_FUNC(d_s *, cordrift_simple_remove_high_derivative_part_of_ds_at_ends, (d_s *dsi, float deltaz, int dn, int nei, int end_size));

PXV_FUNC(O_p *, cordrift_remove_simple_high_derivative_part_of_a_single_data_set, (d_s *dsi, float deltaz, int dn, int nei, int nmin));
PXV_FUNC(int, transform_peaks_to_points,(d_s *dsi, d_s *dsjump, float lim, int two_states));


PXV_FUNC(int, do_cordrift_reproduce_decreasing_steps_duration, (void));
