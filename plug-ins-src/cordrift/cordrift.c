/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _CORDRIFT_C_
#define _CORDRIFT_C_

#include <allegro.h>
# include "xvin.h"
# include "float.h"

/* If you include other regular header do it here*/
 # include "../trackBead/record.h"
 # include "../trackBead/force.h"
 #include "../cardinal/cardinal.h"
#ifdef PIAS
  # include "../trackBead/force_rec.h"

  #endif

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "cordrift.h"

 char oligoseq[64] = {0};


int do_cordrift_hello(void)
{
  int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from cordrift");
  if (i == WIN_CANCEL) win_printf_OK("you have press CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}


d_s *keep_flat_part_data_set(d_s *dsi, float deltaz, int dn, int nei, int nmin)
{
  d_s *dsd = NULL, *ds = NULL;
  int i, j, nf, k, ki;
  float tmp, tmp2;
   if (dsi == NULL) return NULL;

  nf = dsi->nx;	/* this is the number of points in the data set */

  dsd = build_data_set(nf,nf);
  if (dsd == NULL) win_printf_OK("cannot create dsd !");

  for (i = 0; i < dsd->nx; i++)
    {  // we differanciate
      dsd->xd[i] = dsi->xd[i];
      if (i == 0)
	{
	  tmp = dn*dsi->yd[i];
	  for(k = 0, tmp2 = 0; k < dn; k++)
	    {
	      j = i + k + 1;
	      j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
	      tmp2 += dsi->yd[j];
	    }
	  dsd->yd[i] = 2*(tmp-tmp2)/dn;
	}
      else if (i == dsi->nx - 1)
	{
	  for(k = 0, tmp = 0; k < dn; k++)
	    {
	      j = i - k - 1;
	      j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
	      tmp += dsi->yd[j];
	    }
	  tmp2 = dn*dsi->yd[i];
	  dsd->yd[i] = 2*(tmp-tmp2)/dn;
	}
      else
	{
	  for(k = 0, tmp = 0; k < dn; k++)
	    {
	      j = i - k - 1;
	      j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
	      tmp += dsi->yd[j];
	    }
	  for(k = 0, tmp2 = 0; k < dn; k++)
	    {
	      j = i + k + 1;
	      j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
	      tmp2 += dsi->yd[j];
	    }
	  dsd->yd[i] = (tmp-tmp2)/dn;
	}
    }

  /*
  dsd->nx = dsd->ny = 0;
  for (i = dn; i < dsi->nx - dn; i++)
    {
      for(k = i - dn, tmp = 0;(k < i) && (k < dsi->nx); k++) tmp += dsi->yd[k];
      for(k = i, tmp2 = 0; (k < i+dn) && (k < dsi->nx); k++) tmp2 += dsi->yd[k];
      add_new_point_to_ds(dsd, i, (tmp-tmp2)/dn);
    }
  */
  for (i = 0; i < dsd->nx; i++)
    if (fabs(dsd->yd[i]) > deltaz) dsd->yd[i] = -FLT_MAX;
  for (i = 1; i < dsd->nx; i++)
    {
      if ((dsd->yd[i-1] != -FLT_MAX) && (dsd->yd[i] == -FLT_MAX))
	{
	  for (j = i - nei; j < i; j++)
	    dsd->yd[(j<0)?0:j] = -FLT_MAX;
	}
    }
  for (i = dsd->nx - 1; i > 0; i--)
    {
      if ((dsd->yd[i-1] == -FLT_MAX) && (dsd->yd[i] != -FLT_MAX))
	{
	  for (j = i; j < i+nei; j++)
	    dsd->yd[(j < dsd->nx) ? j :dsd->nx-1] = -FLT_MAX;
	}
    }
  for (i = dsd->nx - 1, j = 0; i >= 0; i--)
    {
      if ((dsd->yd[i] != -FLT_MAX))      dsd->yd[i] = j++;
      else j = 0;
    }
  for (i = 0; i < dsd->nx; i++)
    {
      if ((dsd->yd[i] != -FLT_MAX))
	{
	  j = dsd->yd[i];
	  if (j > nmin && j < dsi->nx)
	    {
	      if ((ds = build_data_set( j, j)) == NULL)
		return win_printf_ptr("cannot create plot !");
	      inherit_from_ds_to_ds(ds, dsi);
	      for (k = 0; (k < j) && (k < dsi->nx); k++)
		{
		  ki = (int)(dsd->xd[i]) + k;
		  if (ki < 0 || ki >= dsi->nx) continue;
		  ds->yd[k] = dsi->yd[ki];
		  ds->xd[k] = dsi->xd[ki];
		}
	    }
	  if (j>0) i += j - 1;
	}
    }
  /* now we must do some house keeping */
  free_data_set(dsd);
  return ds;
}







int do_cordrift_keep_flat_part_data_set(void)
{
  int i, j, k;
  O_p *op = NULL, *opn = NULL;
  int id, ids, nf, n_dat, ki;
  static int dn = 3, nei = 1, all_ds = 0, new_plt = 1, nmin = 5;
  static float deltaz = 0.01;
  float tmp, tmp2;
  d_s *ds = NULL, *dsi = NULL, *dsd = NULL;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("Cut data set in flat pieces excluding region\n"
		"where |\\Sigma_n^{n+\\delta} z_i - \\Sigma_{n-\\delta}^{n} z_i / \\delta| is stronger than %6f\n"
		"with \\delta = %4d, Exclude also neighbor points j with j = %4d\n"
		"Do not keep stretch of consecutive points having less than %4d points\n"
		"Proceed all data sets %b. Place result in a new plot%b\n"
		,&deltaz,&dn,&nei,&nmin,&all_ds,&new_plt);
  if (i == WIN_CANCEL)	return OFF;
  ids = op->cur_dat;
  if (new_plt)
    {
      i = op->n_dat;
      op->n_dat = 0; // we want to copy only the plot stuuff
      opn = duplicate_plot_data(op, NULL);
      if (opn == NULL) win_printf_OK("cannot create plot !");
      add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
      op->n_dat = i;
    }
  else opn = op;
  nf = (nf <= 0) ? 16 : nf;
  dsd = build_data_set(nf,nf);
  if (dsd == NULL) win_printf_OK("cannot create dsd !");
  dsd->nx = dsd->ny = 0;

  for (id = 0, n_dat = op->n_dat; id < n_dat; id++)
    {
      if (all_ds == 0 && id != ids) continue;
      dsi = op->dat[id];
      dsd->nx = dsd->ny = 0;
      for (i = dn; i < dsi->nx - dn; i++)
	{
	  for(k = i - dn, tmp = 0;(k < i) && (k < dsi->nx); k++) tmp += dsi->yd[k];
	  for(k = i, tmp2 = 0; (k < i+dn) && (k < dsi->nx); k++) tmp2 += dsi->yd[k];
	  add_new_point_to_ds(dsd, i, (tmp-tmp2)/dn);
	}
      for (i = 0; i < dsd->nx; i++)
	if (fabs(dsd->yd[i]) > deltaz) dsd->yd[i] = -FLT_MAX;
      for (i = 1; i < dsd->nx; i++)
	{
	  if ((dsd->yd[i-1] != -FLT_MAX) && (dsd->yd[i] == -FLT_MAX))
	    {
	      for (j = i - nei; j < i; j++)
		dsd->yd[(j<0)?0:j] = -FLT_MAX;
 	    }
 	}
      for (i = dsd->nx - 1; i > 0; i--)
 	{
           if ((dsd->yd[i-1] == -FLT_MAX) && (dsd->yd[i] != -FLT_MAX))
 	     {
 	       for (j = i; j < i+nei; j++)
		 dsd->yd[(j < dsd->nx) ? j :dsd->nx-1] = -FLT_MAX;
             }
        }
      for (i = dsd->nx - 1, j = 0; i >= 0; i--)
	{
	  if ((dsd->yd[i] != -FLT_MAX))      dsd->yd[i] = j++;
	  else j = 0;
	}
      for (i = 0; i < dsd->nx; i++)
	{
        if ((dsd->yd[i] != -FLT_MAX))
	  {
            j = dsd->yd[i];
	      if (j > nmin && j < dsi->nx)
		{
		  if ((ds = create_and_attach_one_ds(opn, j, j, 0)) == NULL)
		    return win_printf_OK("cannot create plot !");
		  inherit_from_ds_to_ds(ds, dsi);
		  for (k = 0; (k < j) && (k < dsi->nx); k++)
		    {
		      ki = (int)(dsd->xd[i]) + k;
		      if (ki < 0 || ki >= dsi->nx) continue;
		      ds->yd[k] = dsi->yd[ki];
		      ds->xd[k] = dsi->xd[ki];
		    }
		}
	      if (j>0) i += j - 1;
	    }
	}
    }
  /* now we must do some house keeping */
  free_data_set(dsd);
  /* refisplay the entire plot */
  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}




O_p *cordrift_remove_simple_high_derivative_part_of_a_single_data_set(d_s *dsi, float deltaz, int dn, int nei, int nmin)
{
  int i, j, k;
  O_p *opn = NULL;
  float tmp, tmp2;
  int  ki;
  d_s *ds = NULL, *dsd = NULL;

  if (dsi == NULL || dsi->nx == 0 || dsi->ny == 0) return NULL;

  opn = create_one_empty_plot();
  if (opn == NULL) win_printf_OK("cannot create plot !");


  dsd = build_data_set(dsi->nx, dsi->nx);
  if (dsd == NULL) return win_printf_ptr("cannot do non-linear filter !");
  for (i = 0; i < dsd->nx; i++)
    {  // we differanciate
       dsd->xd[i] = dsi->xd[i];
       if (i == 0)
         {
            tmp = dn*dsi->yd[i];
            for(k = 0, tmp2 = 0; k < dn; k++)
               {
                  j = i + k + 1;
                  j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
                  tmp2 += dsi->yd[j];
               }
            dsd->yd[i] = (tmp-tmp2)/dn;
         }
       else if (i == dsi->nx - 1)
         {
           for(k = 0, tmp = 0; k < dn; k++)
             {
                j = i - k - 1;
                j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
                tmp += dsi->yd[j];
             }
           tmp2 = dn*dsi->yd[i];
           dsd->yd[i] = (tmp-tmp2)/dn;
         }
       else
         {
           for(k = 0, tmp = 0; k < dn; k++)
            {
               j = i - k - 1;
               j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
               tmp += dsi->yd[j];
            }
           for(k = 0, tmp2 = 0; k < dn; k++)
            {
               j = i + k + 1;
               j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
               tmp2 += dsi->yd[j];
            }
           dsd->yd[i] = (tmp-tmp2)/dn;
         }
    }
  for (i = 1; i < dsd->nx - 1; i++)
    {
       if (fabs(dsd->yd[i]) > deltaz)
         {
            if ((dsd->yd[i]- dsd->yd[i-1]) * (dsd->yd[i+1]- dsd->yd[i]) <= 0)
              {  // we are on the maximum
                 dsd->yd[i] = -FLT_MAX;
              }
            else if (fabs(dsd->yd[i+1]- dsd->yd[i-1]) > 2*deltaz)
              {   // we have a big jump
                 dsd->yd[i] = -FLT_MAX;
              }
         }
    }
  if (fabs(dsd->yd[0]) > deltaz) dsd->yd[0] = -FLT_MAX;
  if (fabs(dsd->yd[dsd->nx-1]) > deltaz)  dsd->yd[dsd->nx-1] = -FLT_MAX;
  //for (i = 0; i < dsd->nx; i++)
  //if (fabs(dsd->yd[i]) > deltaz) dsd->yd[i] = -FLT_MAX;
  for (i = 1; i < dsd->nx; i++)
    {
       if ((dsd->yd[i-1] != -FLT_MAX) && (dsd->yd[i] == -FLT_MAX))
          {
             for (j = i - nei; j < i; j++)
               dsd->yd[(j<0)?0:j] = -FLT_MAX;
          }
    }
  for (i = dsd->nx - 1; i > 0; i--)
    {
       if ((dsd->yd[i-1] == -FLT_MAX) && (dsd->yd[i] != -FLT_MAX))
          {
             for (j = i; j < i+nei; j++)
              dsd->yd[(j < dsd->nx) ? j :dsd->nx-1] = -FLT_MAX;
          }
    }
  for (i = dsd->nx - 1, j = 0; i >= 0; i--)
    {
       if ((dsd->yd[i] != -FLT_MAX))      dsd->yd[i] = j++;
       else j = 0;
    }
  for (i = 0; i < dsd->nx; i++)
    {
       if ((dsd->yd[i] != -FLT_MAX))
         {
            j = dsd->yd[i];
            if (j > nmin && j < dsi->nx)
               {
                 if ((ds = create_and_attach_one_ds(opn, j, j, 0)) == NULL)
                         return win_printf_ptr("cannot create ds in plot !");
                 inherit_from_ds_to_ds(ds, dsi);
                 for (k = 0; (k < j) && (k < dsi->nx); k++)
                   {
                      ki = i + k;//(int)(dsd->xd[i]) + k;
                      if (ki < 0 || ki >= dsi->nx) continue;
                      ds->yd[k] = dsi->yd[ki];
                      ds->xd[k] = dsi->xd[ki];
                   }
               }
            if (j>0) i += j - 1;
         }
    }
  for (i = dsd->nx - 1; i >= 0; i--)
    {
       if ((dsd->yd[i] == -FLT_MAX))      dsd->yd[i] = -5;
    }
  free_data_set(dsd);
  dsd = NULL;
  return opn;
}




O_p *cordrift_remove_and_cut_simple_high_derivative_part_of_data_set(O_p *op, float deltaz, int dn, int nei, int nmin)
{
  int i, j, k;
  O_p *opn = NULL;
  float tmp, tmp2;
  int id, n_dat, ki;
  d_s *ds = NULL, *dsi = NULL, *dsd = NULL;

  if (op == NULL) return NULL;

  i = op->n_dat;
  op->n_dat = 0; // we want to copy only the plot stuuff
  opn = duplicate_plot_data(op, NULL);
  if (opn == NULL) win_printf_OK("cannot create plot !");
  op->n_dat = i;

  for (id = 0, n_dat = op->n_dat; id < n_dat; id++)
    {
      dsi = op->dat[id];
      if (dsi->nx ==0 || dsi->ny == 0) continue;


      dsd = build_data_set(dsi->nx, dsi->nx);
      if (dsd == NULL) return win_printf_ptr("cannot do non-linear filter !");
      for (i = 0; i < dsd->nx; i++)
	{  // we differanciate
	  dsd->xd[i] = dsi->xd[i];
	  if (i == 0)
	    {
	      tmp = dn*dsi->yd[i];
	      for(k = 0, tmp2 = 0; k < dn; k++)
		{
		  j = i + k + 1;
		  j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
		  tmp2 += dsi->yd[j];
		}
	      dsd->yd[i] = (tmp-tmp2)/dn;
	    }
	  else if (i == dsi->nx - 1)
	    {
	      for(k = 0, tmp = 0; k < dn; k++)
		{
		  j = i - k - 1;
		  j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
		  tmp += dsi->yd[j];
		}
	      tmp2 = dn*dsi->yd[i];
	      dsd->yd[i] = (tmp-tmp2)/dn;
	    }
	  else
	    {
	      for(k = 0, tmp = 0; k < dn; k++)
		{
		  j = i - k - 1;
		  j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
		  tmp += dsi->yd[j];
		}
	      for(k = 0, tmp2 = 0; k < dn; k++)
		{
		  j = i + k + 1;
		  j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
		  tmp2 += dsi->yd[j];
		}
	      dsd->yd[i] = (tmp-tmp2)/dn;
	    }
	}


      for (i = 1; i < dsd->nx - 1; i++)
	{
	  if (fabs(dsd->yd[i]) > deltaz)
	    {
	      if ((dsd->yd[i]- dsd->yd[i-1]) * (dsd->yd[i+1]- dsd->yd[i]) <= 0)
		{  // we are on the maximum
		  dsd->yd[i] = -FLT_MAX;
		}
	      else if (fabs(dsd->yd[i+1]- dsd->yd[i-1]) > 2*deltaz)
		{   // we have a big jump
		  dsd->yd[i] = -FLT_MAX;
		}
	    }
	}
      if (fabs(dsd->yd[0]) > deltaz)
	{
	  dsd->yd[0] = -FLT_MAX;
	}
      if (fabs(dsd->yd[dsd->nx-1]) > deltaz)
	{
	  dsd->yd[dsd->nx-1] = -FLT_MAX;
	}

      //for (i = 0; i < dsd->nx; i++)
      //if (fabs(dsd->yd[i]) > deltaz) dsd->yd[i] = -FLT_MAX;
      for (i = 1; i < dsd->nx; i++)
	{
	  if ((dsd->yd[i-1] != -FLT_MAX) && (dsd->yd[i] == -FLT_MAX))
	    {
	      for (j = i - nei; j < i; j++)
		dsd->yd[(j<0)?0:j] = -FLT_MAX;
 	    }
 	}
      for (i = dsd->nx - 1; i > 0; i--)
 	{
           if ((dsd->yd[i-1] == -FLT_MAX) && (dsd->yd[i] != -FLT_MAX))
 	     {
 	       for (j = i; j < i+nei; j++)
		 dsd->yd[(j < dsd->nx) ? j :dsd->nx-1] = -FLT_MAX;
             }
        }
      for (i = dsd->nx - 1, j = 0; i >= 0; i--)
	{
	  if ((dsd->yd[i] != -FLT_MAX))      dsd->yd[i] = j++;
	  else j = 0;
	}
      for (i = 0; i < dsd->nx; i++)
	{
        if ((dsd->yd[i] != -FLT_MAX))
	  {
            j = dsd->yd[i];
	      if (j > nmin && j < dsi->nx)
		{
		  if ((ds = create_and_attach_one_ds(opn, j, j, 0)) == NULL)
		    return win_printf_ptr("cannot create plot !");
		  inherit_from_ds_to_ds(ds, dsi);
		  for (k = 0; (k < j) && (k < dsi->nx); k++)
		    {
		      ki = i + k;//(int)(dsd->xd[i]) + k;
		      if (ki < 0 || ki >= dsi->nx) continue;
		      ds->yd[k] = dsi->yd[ki];
		      ds->xd[k] = dsi->xd[ki];
		    }
		}
	      if (j>0) i += j - 1;
	    }
	}
      for (i = dsd->nx - 1; i >= 0; i--)
	{
	  if ((dsd->yd[i] == -FLT_MAX))      dsd->yd[i] = -5;
	}

      free_data_set(dsd);
      dsd = NULL;
    }
  return opn;
}


d_s *cordrift_simple_remove_high_derivative_part_of_ds_at_ends(d_s *dsi, float deltaz, int dn, int nei, int end_size)
//, d_s *dsdc)
{
        int i, j, k, remove;
  float tmp, tmp2;
  d_s *ds = NULL, *dsd = NULL;


  if (dsi == NULL || dsi->nx ==0 || dsi->ny == 0) return NULL;

  dsd = build_data_set(dsi->nx, dsi->nx);
  if (dsd == NULL) return win_printf_ptr("cannot do non-linear filter !");
  for (i = 0; i < dsd->nx; i++)
    {  // we differanciate
      dsd->xd[i] = dsi->xd[i];
      if (i == 0)
	{
	  tmp = dn*dsi->yd[i];
	  for(k = 0, tmp2 = 0; k < dn; k++)
	    {
	      j = i + k + 1;
	      j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
	      tmp2 += dsi->yd[j];
	    }
	  dsd->yd[i] = (tmp-tmp2)/dn;
	}
      else if (i == dsi->nx - 1)
	{
	  for(k = 0, tmp = 0; k < dn; k++)
	    {
	      j = i - k - 1;
	      j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
	      tmp += dsi->yd[j];
	    }
	  tmp2 = dn*dsi->yd[i];
	  dsd->yd[i] = (tmp-tmp2)/dn;
	}
      else
	{
	  for(k = 0, tmp = 0; k < dn; k++)
	    {
	      j = i - k - 1;
	      j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
	      tmp += dsi->yd[j];
	    }
	  for(k = 0, tmp2 = 0; k < dn; k++)
	    {
	      j = i + k + 1;
	      j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
	      tmp2 += dsi->yd[j];
	    }
	  dsd->yd[i] = (tmp-tmp2)/dn;
	}
    }
  for (i = 1; i < dsd->nx - 1; i++)
    {
      if (i > end_size && i < dsd->nx - end_size) continue;
      if (fabs(dsd->yd[i]) > deltaz)
	{
	  if ((dsd->yd[i]- dsd->yd[i-1]) * (dsd->yd[i+1]- dsd->yd[i]) <= 0)
	    {  // we are on the maximum
	      dsd->yd[i] = -FLT_MAX;
	      //if (dsdc != NULL)  dsdc->yd[i] = -2;
	    }
	  else if (fabs(dsd->yd[i+1]- dsd->yd[i-1]) > 2*deltaz)
	    {   // we have a big jump
	      dsd->yd[i] = -FLT_MAX;
	      //if (dsdc != NULL)  dsdc->yd[i] = -1;
	    }
	}
    }
  if (fabs(dsd->yd[0]) > deltaz)
    {
      dsd->yd[0] = -FLT_MAX;
      //if (dsdc != NULL)  dsdc->yd[0] = -1;
    }
  if (fabs(dsd->yd[dsd->nx-1]) > deltaz)
    {
      dsd->yd[dsd->nx-1] = -FLT_MAX;
      //if (dsdc != NULL)  dsdc->yd[dsd->nx-1] = -1;
    }
  for (i = 1; i < dsd->nx; i++)
    {
      if ((dsd->yd[i-1] != -FLT_MAX) && (dsd->yd[i] == -FLT_MAX))
	{
	  for (j = i - nei; j < i; j++)
	    dsd->yd[(j<0)?0:j] = -FLT_MAX;
	}
    }
  for (i = dsd->nx - 1; i > 0; i--)
    {
      if ((dsd->yd[i-1] == -FLT_MAX) && (dsd->yd[i] != -FLT_MAX))
	{
	  for (j = i; j < i+nei; j++)
	    dsd->yd[(j < dsd->nx) ? j :dsd->nx-1] = -FLT_MAX;
	}
    }
  for (i = end_size, remove = 0; i >= 0; i--)
    {
       remove = (dsd->yd[i] == -FLT_MAX) ? 1 : remove;
       if (remove) dsd->yd[i] = -FLT_MAX;
    }
  for (i = dsd->nx - end_size, remove = 0; i < dsd->nx; i++)
    {
       remove = (dsd->yd[i] == -FLT_MAX) ? 1 : remove;
       if (remove) dsd->yd[i] = -FLT_MAX;
    }

  for (i = 0, j = 0; i < dsd->nx; i++)
    {
      j += ((dsd->yd[i] != -FLT_MAX)) ? 1 : 0;
    }
  if (j == 0)
    {
      free_data_set(dsd);
      return NULL;
    }
  if ((ds = build_data_set(j, j)) != NULL)
    {
      for (i = 0, j = 0; i < dsd->nx; i++)
	{
	  if ((dsd->yd[i] != -FLT_MAX))
	  {
	    ds->xd[j] = dsd->xd[i];
	    ds->yd[j] = dsi->yd[i];
	    j++;
	  }
	}
      inherit_from_ds_to_ds(ds, dsi);
    }
  free_data_set(dsd);
  dsd = NULL;
  return ds;
}



d_s *cordrift_simple_remove_high_derivative_part_of_ds(d_s *dsi, float deltaz, int dn, int nei)
//, d_s *dsdc)
{
  int i, j, k;
  float tmp, tmp2;
  d_s *ds = NULL, *dsd = NULL;


  if (dsi == NULL || dsi->nx ==0 || dsi->ny == 0) return NULL;

  dsd = build_data_set(dsi->nx, dsi->nx);
  if (dsd == NULL) return win_printf_ptr("cannot do non-linear filter !");
  for (i = 0; i < dsd->nx; i++)
    {  // we differanciate
      dsd->xd[i] = dsi->xd[i];
      if (i == 0)
	{
	  tmp = dn*dsi->yd[i];
	  for(k = 0, tmp2 = 0; k < dn; k++)
	    {
	      j = i + k + 1;
	      j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
	      tmp2 += dsi->yd[j];
	    }
	  dsd->yd[i] = (tmp-tmp2)/dn;
	}
      else if (i == dsi->nx - 1)
	{
	  for(k = 0, tmp = 0; k < dn; k++)
	    {
	      j = i - k - 1;
	      j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
	      tmp += dsi->yd[j];
	    }
	  tmp2 = dn*dsi->yd[i];
	  dsd->yd[i] = (tmp-tmp2)/dn;
	}
      else
	{
	  for(k = 0, tmp = 0; k < dn; k++)
	    {
	      j = i - k - 1;
	      j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
	      tmp += dsi->yd[j];
	    }
	  for(k = 0, tmp2 = 0; k < dn; k++)
	    {
	      j = i + k + 1;
	      j = (j < 0) ? 0 : ((j < dsi->nx) ? j : dsi->nx - 1);
	      tmp2 += dsi->yd[j];
	    }
	  dsd->yd[i] = (tmp-tmp2)/dn;
	}
    }
  /*
  if (dsdc != NULL)
    {
      for (i = 0; i < dsd->nx && i < dsdc->nx; i++)
	{
	  dsdc->xd[i] = dsd->xd[i];
	  dsdc->yd[i] = dsd->yd[i];
	}
    }
  */
  for (i = 1; i < dsd->nx - 1; i++)
    {
      if (fabs(dsd->yd[i]) > deltaz)
	{
	  if ((dsd->yd[i]- dsd->yd[i-1]) * (dsd->yd[i+1]- dsd->yd[i]) <= 0)
	    {  // we are on the maximum
	      dsd->yd[i] = -FLT_MAX;
	      //if (dsdc != NULL)  dsdc->yd[i] = -2;
	    }
	  else if (fabs(dsd->yd[i+1]- dsd->yd[i-1]) > 2*deltaz)
	    {   // we have a big jump
	      dsd->yd[i] = -FLT_MAX;
	      //if (dsdc != NULL)  dsdc->yd[i] = -1;
	    }
	}
    }
  if (fabs(dsd->yd[0]) > deltaz)
    {
      dsd->yd[0] = -FLT_MAX;
      //if (dsdc != NULL)  dsdc->yd[0] = -1;
    }
  if (fabs(dsd->yd[dsd->nx-1]) > deltaz)
    {
      dsd->yd[dsd->nx-1] = -FLT_MAX;
      //if (dsdc != NULL)  dsdc->yd[dsd->nx-1] = -1;
    }
  for (i = 1; i < dsd->nx; i++)
    {
      if ((dsd->yd[i-1] != -FLT_MAX) && (dsd->yd[i] == -FLT_MAX))
	{
	  for (j = i - nei; j < i; j++)
	    dsd->yd[(j<0)?0:j] = -FLT_MAX;
	}
    }
  for (i = dsd->nx - 1; i > 0; i--)
    {
      if ((dsd->yd[i-1] == -FLT_MAX) && (dsd->yd[i] != -FLT_MAX))
	{
	  for (j = i; j < i+nei; j++)
	    dsd->yd[(j < dsd->nx) ? j :dsd->nx-1] = -FLT_MAX;
	}
    }
  for (i = 0, j = 0; i < dsd->nx; i++)
    {
      j += ((dsd->yd[i] != -FLT_MAX)) ? 1 : 0;
    }
  if (j == 0)
    {
      free_data_set(dsd);
      return NULL;
    }
  if ((ds = build_data_set(j, j)) != NULL)
    {
      for (i = 0, j = 0; i < dsd->nx; i++)
	{
	  if ((dsd->yd[i] != -FLT_MAX))
	  {
	    ds->xd[j] = dsd->xd[i];
	    ds->yd[j] = dsi->yd[i];
	    j++;
	  }
	}
      inherit_from_ds_to_ds(ds, dsi);
    }
  free_data_set(dsd);
  dsd = NULL;
  return ds;
}

O_p *cordrift_remove_simple_high_derivative_part_of_each_data_set(O_p *op, float deltaz, int dn, int nei)
{
  int i;
  O_p  *opn = NULL;
  d_s *dsi = NULL, *ds = NULL;

  if (op == NULL || op->n_dat == 0) return NULL;
  i = op->n_dat;
  op->n_dat = 0; // we want to copy only the plot stuuff
  opn = duplicate_plot_data(op, NULL);
  if (opn == NULL) win_printf_OK("cannot create plot !");
  op->n_dat = i;

  for(i = 0; i < op->n_dat; i++)
    {
      dsi = op->dat[i];
      ds = cordrift_simple_remove_high_derivative_part_of_ds(dsi, deltaz, dn, nei);
      if (ds != NULL)
	  add_one_plot_data(opn, IS_DATA_SET, (void*)ds);
    }

  if (opn == NULL) win_printf_OK("cannot create plot !");
  return opn;
}


int do_cordrift_remove_simple_high_derivative_part_of_each_data_set(void)
{
  int i;
  O_p *op = NULL, *opn = NULL;
  static int nei = 1, dn = 3;
  static float deltaz = 0.01;
  d_s *dsi = NULL, *ds = NULL;//, *dsdc = NULL;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Remove points in data set where the derivative, done after a simple differenciation:\n"
		"|\\Sigma_n^{n+1+\\delta} z_i - \\Sigma_{n-1-\\delta}^{n} z_i / \\delta| is stronger than %6f\n"
		"using \\delta = %4d, Exclude neighbor points j with j = %4d\n"
		"All data sets are proeeded and result is placed in a new plot\n"
		,&deltaz,&dn,&nei);
  if (i == WIN_CANCEL)	return OFF;


  opn = cordrift_remove_simple_high_derivative_part_of_each_data_set(op, deltaz, dn, nei);
  if (opn == NULL) win_printf_OK("cannot create plot !");
  add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
  return refresh_plot(pr, pr->n_op-1);

  i = op->n_dat;
  op->n_dat = 0; // we want to copy only the plot stuuff
  opn = duplicate_plot_data(op, NULL);
  if (opn == NULL) win_printf_OK("cannot create plot !");
  add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
  op->n_dat = i;


  for(i = 0; i < op->n_dat; i++)
    {
      dsi = op->dat[i];
      //dsdc = build_data_set(dsi->nx, dsi->nx);
      ds = cordrift_simple_remove_high_derivative_part_of_ds(dsi, deltaz, dn, nei);
      if (ds != NULL)
	{
	  add_one_plot_data(opn, IS_DATA_SET, (void*)ds);
	  //add_one_plot_data(opn, IS_DATA_SET, (void*)dsdc);
	}
    }

  if (opn == NULL) win_printf_OK("cannot create plot !");
  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}


int do_cordrift_remove_simple_high_derivative_part_of_each_data_set_ends(void)
{
  int i;
  O_p *op = NULL, *opn = NULL;
  static int nei = 1, dn = 3, size = 7;
  static float deltaz = 0.01;
  d_s *dsi = NULL, *ds = NULL;//, *dsdc = NULL;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Remove points at data set ends where the derivative, done after a simple differenciation:\n"
		"|\\Sigma_n^{n+1+\\delta} z_i - \\Sigma_{n-1-\\delta}^{n} z_i / \\delta| is stronger than %6f\n"
		"using \\delta = %4d, Exclude neighbor points j with j = %4d\n"
                "Define end size %5d\n"
		"All data sets are proeeded and result is placed in a new plot\n"
		,&deltaz,&dn,&nei,&size);
  if (i == WIN_CANCEL)	return OFF;


  i = op->n_dat;
  op->n_dat = 0; // we want to copy only the plot stuuff
  opn = duplicate_plot_data(op, NULL);
  if (opn == NULL) win_printf_OK("cannot create plot !");
  add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
  op->n_dat = i;


  for(i = 0; i < op->n_dat; i++)
    {
      dsi = op->dat[i];
      //dsdc = build_data_set(dsi->nx, dsi->nx);
      ds = cordrift_simple_remove_high_derivative_part_of_ds_at_ends(dsi, deltaz, dn, nei, size);
      if (ds != NULL)
	{
	  add_one_plot_data(opn, IS_DATA_SET, (void*)ds);
	  //add_one_plot_data(opn, IS_DATA_SET, (void*)dsdc);
	}
    }

  if (opn == NULL) win_printf_OK("cannot create plot !");
  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}




int do_cordrift_remove_and_cut_simple_high_derivative_part_of_data_set(void)
{
  int i;
  O_p *op = NULL, *opn = NULL;
  static int nei = 1, nmin = 5, dn = 3;
  static float deltaz = 0.01;
  d_s *dsi = NULL;
  pltreg *pr = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Cut data set in continuous pieces excluding regions where the derivative, done after\n"
		" a differenciation, |\\Sigma_n^{n+1+\\delta} z_i - \\Sigma_{n-1-\\delta}^{n} z_i / \\delta| is stronger than %6f\n"
		"With \\delta = %4d Exclude neighbor points j with j = %4d\n"
		"Do not keep stretch of consecutive points having less than %4d points\n"
		"All data sets are proeeded and result is placed in a new plot\n"
		,&deltaz,&dn,&nei,&nmin);
  if (i == WIN_CANCEL)	return OFF;


  opn = cordrift_remove_and_cut_simple_high_derivative_part_of_data_set(op, deltaz, dn, nei, nmin);


  if (opn == NULL) win_printf_OK("cannot create plot !");
  add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);

  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}


d_s *find_histo_peak_boundary(d_s *dsh, d_s *dsp, float mthres)
{
  int i, j, k, ki, j_1, j1;
  d_s *dspb0 = NULL;
  float tmp_1, tmp, ax;

  if (dsh == NULL || dsh->nx < 2 || dsp == NULL || dsp->nx < 1) return NULL;
  dspb0 = build_data_set(2*dsp->nx,2*dsp->nx); // peak boundary
  if (dspb0 == NULL) return win_printf_ptr("null dspb");

  ax = (float)(dsh->nx-1)/(dsh->xd[dsh->nx-1] - dsh->xd[0]);

  for(i = 0; i < dsp->nx; i++)
    {
       // index of corresponding point in histo
       k = (int)(0.5 + ((dsp->xd[i] - dsh->xd[0])*ax));
       if (i == 0)
          {
              tmp_1 = (dsh->xd[0] + dsp->xd[i])/2;
              j_1 = (int)(0.5 + ((tmp_1 - dsh->xd[0])*ax));
          }
       else
          {   // we look for minimum between peak i-1 and i
              j_1 = (int)(0.5 + ((dsp->xd[i-1] - dsh->xd[0])*ax));
              for (j = ki = k, tmp = dsp->yd[i]; j >= j_1 && j < dsh->nx; j--)
                   tmp = (dsh->yd[j] < tmp) ? dsh->yd[ki = j] : tmp;
              j_1 = ki;
          }
       j_1 = (j_1 <= 0) ? 0 : j_1;
       for (j = k; j > j_1 && j <
	      dsh->nx; j--)
           if (dsh->yd[j] < mthres) break;
       dspb0->yd[2*i] = dsh->xd[j];
       dspb0->xd[2*i] = dsh->yd[j];
       if (i == dsp->nx - 1)
          {
              tmp_1 = (dsh->xd[dsh->nx - 1] + dsp->xd[i])/2;
              j1 = (int)(0.5 + ((tmp_1 - dsh->xd[0])*ax));
          }
       else
          {   // we look for minimum between peak i and i + 1
              j1 = (int)(0.5 + ((dsp->xd[i+1] - dsh->xd[0])*ax));
              for (j = ki = k, tmp = dsp->yd[i]; j <= j1 && j < dsh->nx; j++)
                   tmp = (dsh->yd[j] < tmp) ? dsh->yd[ki = j] : tmp;
              j1 = ki;
          }
       j1 = (j1 <= 0) ? 0 : j1;
       for (j = k; j < j1 && j < dsh->nx; j++)
           if (dsh->yd[j] < mthres) break;
       dspb0->yd[2*i+1] = dsh->xd[j];
       dspb0->xd[2*i+1] = dsh->yd[j];
    }
  return dspb0;
}


int test_cordrift_reproduce_decreasing_steps(void)
{
  pltreg *pr = NULL;
  O_p *op = NULL;
  int i, j, k;
  d_s *dsi = NULL, *dsh = NULL, *dsp = NULL, *dspb = NULL, *dspb0 = NULL, *dsd = NULL, *dspn = NULL;
  float tmp_1, ax, tmp, ext, histo_i, histo_pi = 0;
  int j_1, j1, ki;
  static float mthres = 3;
  static int nmin = 3, neat = 0;
  static float biny = 0.005, contr = 3;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("transform  data set in steps using histo \n"
		"gaussian binsize of histo %6f\n"
		"Do not keep stretch of consecutive points having less than %4d points\n"
		"Exclude points with averaging smaller than %6f\n"
                "%b keep only unambiguous peaks with contrast greater tha %5f\n"
		"All data sets are proeeded and result is placed in a new plot\n"
		,&biny,&nmin,&mthres,&neat,&contr);
  if (i == WIN_CANCEL)	return OFF;



  if (dsi == NULL || dsi->nx ==0 || dsi->ny == 0) return win_printf_OK("null ds");

  dsh = build_histo_ds_with_gaussian_convolution(dsi, biny);
  if (dsh == NULL || dsh->nx < 2) return win_printf_OK("null dsh");
  dsp = find_max_peaks(dsh, mthres);
  if (dsp == NULL || dsp->nx <= 0) return warning_message("null dsp");
  dspb = build_data_set(dsp->nx,dsp->nx); // peak boundary
  if (dspb == NULL) return win_printf_OK("null dspb");
  dspn = build_data_set(dsp->nx,dsp->nx); // peak unambiguous
  if (dspn == NULL) return win_printf_OK("null dspb");
  ax = (float)(dsh->nx-1)/(dsh->xd[dsh->nx-1] - dsh->xd[0]);
  // we define the peak boundaries
  dspb0 = build_data_set(2*dsp->nx,2*dsp->nx); // peak boundary
  if (dspb0 == NULL) return win_printf_OK("null dspb");

  for(i = 0, histo_i = 0; i < dsh->nx; i++)
      histo_i += dsh->yd[i];

  for(i = 0; i < dsp->nx; i++)
    {
       // index of corresponding point in histo
       k = (int)(0.5 + ((dsp->xd[i] - dsh->xd[0])*ax));
       if (i == 0)
          {
              tmp_1 = (dsh->xd[0] + dsp->xd[i])/2;
              j_1 = (int)(0.5 + ((tmp_1 - dsh->xd[0])*ax));
          }
       else
          {   // we look for minimum between peak i-1 and i
              j_1 = (int)(0.5 + ((dsp->xd[i-1] - dsh->xd[0])*ax));
              for (j = ki = k, tmp = dsp->yd[i]; j >= j_1 && j < dsh->nx; j--)
                   tmp = (dsh->yd[j] < tmp) ? dsh->yd[ki = j] : tmp;
              j_1 = ki;
          }
       j_1 = (j_1 <= 0) ? 0 : j_1;
       for (j = k; j > j_1 && j < dsh->nx; j--)
           if (dsh->yd[j] < mthres) break;
       //win_printf("left %d boundary: j_1 = %d -> y = %g\n j = %d -> y = %g; k = %d -> y = %g\n" ,i,j_1,dsh->yd[j_1],j,dsh->yd[j],k,dsh->yd[k]);
       dspb->xd[i] = dsh->xd[j];
       dspn->yd[i] += (j == j_1 && dsp->yd[i] < 3*dsh->yd[j]) ? 1 : 0;
       dspn->xd[i] = dsp->xd[i];
       dspb0->yd[2*i] = dsh->xd[j];
       dspb0->xd[2*i] = dsh->yd[j];
       if (i == dsp->nx - 1)
          {
              tmp_1 = (dsh->xd[dsh->nx - 1] + dsp->xd[i])/2;
              j1 = (int)(0.5 + ((tmp_1 - dsh->xd[0])*ax));
          }
       else
          {   // we look for minimum between peak i and i + 1
              j1 = (int)(0.5 + ((dsp->xd[i+1] - dsh->xd[0])*ax));
              for (j = ki = k, tmp = dsp->yd[i]; j <= j1 && j < dsh->nx; j++)
                   tmp = (dsh->yd[j] < tmp) ? dsh->yd[ki = j] : tmp;
              j1 = ki;
          }
       j1 = (j1 <= 0) ? 0 : j1;
       for (j = k; j < j1 && j < dsh->nx; j++)
           if (dsh->yd[j] < mthres) break;
       //win_printf("right %d boundary: k = %d -> y = %g\n j = %d -> y = %g; j1 = %d -> y = %g\n" ,i,k,dsh->yd[k],j,dsh->yd[j],j1,dsh->yd[j1]);
       dspn->yd[i] += (j == j1 && dsp->yd[i] < 3*dsh->yd[j]) ? 1 : 0;
       dspn->xd[i] = dsp->xd[i];
       dspb->yd[i] = dsh->xd[j];
       dspb0->yd[2*i+1] = dsh->xd[j];
       dspb0->xd[2*i+1] = dsh->yd[j];
       j1 = j;
       for (j = j_1; j < j1 && j < dsh->nx; j++)
	 histo_pi += dsh->yd[j];

    }
  d_s *dspc = build_data_set(16,16);
  if (dspc == NULL) return win_printf_OK("null dspc");
  dspc->nx = dspc->ny = 0;
  d_s *dspbc = build_data_set(16,16);
  if (dspbc == NULL) return win_printf_OK("null dspbc");
  dspbc->nx = dspbc->ny = 0;
  for(i = 0; i < dsp->nx; i++)
    {
       if (neat > 0 && dspn->yd[i] > 0) continue;
       add_new_point_to_ds(dspc, dsp->xd[i], dsp->yd[i]);
       add_new_point_to_ds(dspbc, dspb->xd[i], dspb->yd[i]);
    }

  dsd = build_data_set(2*dspc->nx,2*dspc->nx); // steps
  if (dsd == NULL) return win_printf_OK("null dsd");
  for(i = 0; i < dspc->nx; i++)
    {
        dsd->yd[2*i] = dsd->yd[2*i + 1] = dspc->xd[i];
        dsd->xd[2*i] = dsi->xd[dsi->nx-1];  // max
        dsd->xd[2*i + 1] = dsi->xd[0];   // min
    }
  for(k = 0; k < dsi->nx; k++)
    {
        for(i = 0; i < dspc->nx; i++)
          {
                  //if (neat > 0 && dspn->yd[i] > 0) continue;
             if(dsi->yd[k] >= dspbc->xd[i] && dsi->yd[k] <= dspbc->yd[i])
                {
                   dsd->xd[2*i] = (dsd->xd[2*i] > dsi->xd[k]) ? dsi->xd[k] : dsd->xd[2*i];
                   dsd->xd[2*i+1] = (dsd->xd[2*i+1] < dsi->xd[k]) ? dsi->xd[k] : dsd->xd[2*i+1];
                   break;
                }
          }
    }
  set_ds_dash_line(dsd);
  for(k = 0; k < dsh->nx; k++)
    {
        tmp = dsh->yd[k];
        dsh->yd[k] = dsh->xd[k];
        dsh->xd[k] = 0;
        for(i = 0; i < dsp->nx; i++)
          {
             if(dsh->yd[k] >= dspb->xd[i] && dsh->yd[k] <= dspb->yd[i])
                {
                   dsh->xd[k] = tmp;
                   break;
                }
          }

    }


  for(i = 0, ext = 0; i < dsp->nx; i++)
          ext += dsp->yd[i];

  tmp = 1;
  if (histo_i * ext > 0)
    tmp = dsi->nx * histo_pi / (histo_i * ext);

  for(i = 0; i < dsp->nx; i++) dsp->yd[i] *= tmp;

  for(i = 0; i < dsp->nx; i++)
    {
       tmp = dsp->xd[i];
       dsp->xd[i] = dsp->yd[i];
       dsp->yd[i] = tmp;
    }
  add_one_plot_data(op, IS_DATA_SET, (void*)dsh);
  add_one_plot_data(op, IS_DATA_SET, (void*)dsp);
  add_one_plot_data(op, IS_DATA_SET, (void*)dsd);
  add_one_plot_data(op, IS_DATA_SET, (void*)dspb0);
  free_data_set(dspb);
  free_data_set(dspbc);
  free_data_set(dspc);
  op->need_to_refresh = 1;
  return  refresh_plot(pr, UNCHANGED);
}

d_s *cordrift_reproduce_decreasing_steps(d_s *dsi, float mthres, int nmin, float biny)
{
  int i, j, k, ki;
  d_s *dsh = NULL, *dsp = NULL, *dspb = NULL, *dsd = NULL;
  float tmp, tmp_1, ax;
  int j_1, j1;

  (void)nmin;
  if (dsi == NULL || dsi->nx ==0 || dsi->ny == 0) return NULL;

  dsh = build_histo_ds_with_gaussian_convolution(dsi, biny);
  if (dsh == NULL || dsh->nx < 2) return win_printf_ptr("null dsh");
  dsp = find_max_peaks(dsh, mthres);
  if (dsp == NULL || dsp->nx <= 0) return win_printf_ptr("null dsh");
  dspb = build_data_set(dsp->nx,dsp->nx); // peak boundary
  if (dspb == NULL) return win_printf_ptr("null dspb");
  ax = (float)(dsh->nx-1)/(dsh->xd[dsh->nx-1] - dsh->xd[0]);

  // we define the peak boundaries
  for(i = 0; i < dsp->nx; i++)
    {
       // index of corresponding point in histo
       k = (int)(0.5 + ((dsp->xd[i] - dsh->xd[0])*ax));
       if (i == 0)
          {
              tmp_1 = (dsh->xd[0] + dsp->xd[i])/2;
              j_1 = (int)(0.5 + ((tmp_1 - dsh->xd[0])*ax));
          }
       else
          {   // we look for minimum between peak i-1 and i
              j_1 = (int)(0.5 + ((dsp->xd[i-1] - dsh->xd[0])*ax));
              for (j = ki = k, tmp = dsp->yd[i]; j >= j_1 && j < dsh->nx; j--)
                   tmp = (dsh->yd[j] < tmp) ? dsh->yd[ki = j] : tmp;
              j_1 = ki;
          }
       j_1 = (j_1 <= 0) ? 0 : j_1;
       for (j = k; j > j_1 && j < dsh->nx; j--)
           if (dsh->yd[j] < mthres) break;
       dspb->xd[i] = dsh->xd[j];
       if (i == dsp->nx - 1)
          {
              tmp_1 = (dsh->xd[dsh->nx - 1] + dsp->xd[i])/2;
              j1 = (int)(0.5 + ((tmp_1 - dsh->xd[0])*ax));
          }
       else
          {   // we look for minimum between peak i and i + 1
              j1 = (int)(0.5 + ((dsp->xd[i+1] - dsh->xd[0])*ax));
              for (j = ki = k, tmp = dsp->yd[i]; j <= j1 && j < dsh->nx; j++)
                   tmp = (dsh->yd[j] < tmp) ? dsh->yd[ki = j] : tmp;
              j1 = ki;
          }
       j1 = (j1 <= 0) ? 0 : j1;
       for (j = k; j < j1 && j < dsh->nx; j++)
           if (dsh->yd[j] < mthres) break;
       dspb->yd[i] = dsh->xd[j];
    }



  dsd = build_data_set(2*dsp->nx,2*dsp->nx); // steps
  if (dsd == NULL) return NULL;
  for(i = 0; i < dsp->nx; i++)
    {
        dsd->yd[2*i] = dsd->yd[2*i + 1] = dsp->xd[i];
        dsd->xd[2*i] = dsi->xd[dsi->nx-1];  // max
        dsd->xd[2*i + 1] = dsi->xd[0];   // min
    }
  for(k = 0; k < dsi->nx; k++)
    {
        for(i = 0; i < dsp->nx; i++)
          {
             if(dsi->yd[k] >= dspb->xd[i] && dsi->yd[k] <= dspb->yd[i])
                {
                   dsd->xd[2*i] = (dsd->xd[2*i] > dsi->xd[k]) ? dsi->xd[k] : dsd->xd[2*i];
                   dsd->xd[2*i+1] = (dsd->xd[2*i+1] < dsi->xd[k]) ? dsi->xd[k] : dsd->xd[2*i+1];
                   break;
                }
          }
    }

  set_ds_dash_line(dsd);
  free_data_set(dsh);
  free_data_set(dsp);
  free_data_set(dspb);
  return dsd;
}



d_s *add_peak_limits_as_err_in_histo(d_s *dsh, // the gaussian histogram
				d_s *dsp, // the peak found for thar histogram
				float mthres) // the lowest value of histogram
{
  int i, j, k, j1, j_1, ki;
  float ax, tmp_1, tmp;

  if (dsh == NULL || dsh->nx < 2) return win_printf_ptr("null dsh");
  if (dsp == NULL || dsp->nx <= 0) return win_printf_ptr("null dsp");
  ax = (float)(dsh->nx-1)/(dsh->xd[dsh->nx-1] - dsh->xd[0]);
  alloc_data_set_x_error(dsp);
  alloc_data_set_x_down_error(dsp);
  // we define the peak boundaries
  for(i = 0; i < dsp->nx; i++)
    {
       // index of corresponding point in histo
       k = (int)(0.5 + ((dsp->xd[i] - dsh->xd[0])*ax));
       if (i == 0)
          {
              tmp_1 = (dsh->xd[0] + dsp->xd[i])/2;
              j_1 = (int)(0.5 + ((tmp_1 - dsh->xd[0])*ax));
          }
       else
          {   // we look for minimum between peak i-1 and i
              j_1 = (int)(0.5 + ((dsp->xd[i-1] - dsh->xd[0])*ax));
              for (j = ki = k, tmp = dsp->yd[i]; j >= j_1 && j < dsh->nx; j--)
                   tmp = (dsh->yd[j] < tmp) ? dsh->yd[ki = j] : tmp;
              j_1 = ki;
          }
       j_1 = (j_1 <= 0) ? 0 : j_1;
       for (j = k; j > j_1 && j < dsh->nx; j--)
           if (dsh->yd[j] < mthres) break;
       j_1 = j;
       if (i == dsp->nx - 1)
          {
              tmp_1 = (dsh->xd[dsh->nx - 1] + dsp->xd[i])/2;
              j1 = (int)(0.5 + ((tmp_1 - dsh->xd[0])*ax));
          }
       else
          {   // we look for minimum between peak i and i + 1
              j1 = (int)(0.5 + ((dsp->xd[i+1] - dsh->xd[0])*ax));
              for (j = ki = k, tmp = dsp->yd[i]; j <= j1 && j < dsh->nx; j++)
                   tmp = (dsh->yd[j] < tmp) ? dsh->yd[ki = j] : tmp;
              j1 = ki;
          }
       j1 = (j1 <= 0) ? 0 : j1;
       for (j = k; j < j1 && j < dsh->nx; j++)
           if (dsh->yd[j] < mthres) break;
       j1 = j;
       if (dsp->xed != NULL) dsp->xed[i] = fabs(dsh->xd[j_1] - dsp->xd[i]);
       if (dsp->xe != NULL) dsp->xe[i] = fabs(dsh->xd[j1] - dsp->xd[i]);
    }
  return dsp;
}


# define NONE  0
# define END_PH3_PH4_PH5 1
# define PH5_ONLY 2


d_s *cordrift_reproduce_decreasing_steps_duration(d_s *dsi,     // input data
						  float mthres, // the lowest value of histogram
						  int navg,     // the Nb. of averaging to find step boundary
						  float biny,   // the histogram bin size
						  int applyonlyto) // specify a phase restriction may be NONE, END_PH3_PH4_PH5, PH5_ONLY

{
    int i, j, k, ki, min, max;
    d_s *dsh = NULL, *dsp = NULL;
    float tmp, tmp_1, ax, histo_i, histo_pi, ext;
    int j_1, j1;
    int ph_poss[32] = {0};
    int ph_pose[32] = {0};
    int phni = 0, stph = 0, enph = 0;
    float *xdptr = NULL, *ydptr = NULL;
    int dsinx = 0;
    char *st = NULL;

    if (dsi == NULL || dsi->nx ==0 || dsi->ny == 0) return NULL;

    if (dsi->source != NULL)
      {
	for (st = strstr(dsi->source,"Phase["); st != NULL && st[0] != 0; st = strstr(st+1,"Phase["))
	  {
	    if (sscanf(st,"Phase[%d] = [%d,%d]",&phni,&stph,&enph) == 3)
	      {
		if (phni < 32)
		  {
		    ph_poss[phni] = stph;
		    ph_pose[phni] = enph;
		  }
	      }
	  }
      }
    if (applyonlyto == PH5_ONLY && ph_pose[5] != 0 && ph_pose[5] - ph_poss[5] < dsi->nx)
      {
	xdptr = dsi->xd;
	ydptr = dsi->yd;
	dsinx = dsi->nx;
	dsi->xd = xdptr + ph_poss[5];
	dsi->yd = ydptr + ph_poss[5];
	dsi->nx = dsi->ny = ph_pose[5] - ph_poss[5];
      }
    else if (applyonlyto == END_PH3_PH4_PH5 && ph_pose[3] != 0 && ph_pose[5] != 0
	     && ph_pose[5] - ph_poss[3] < dsi->nx)
      {
	xdptr = dsi->xd;
	ydptr = dsi->yd;
	dsinx = dsi->nx;
	i = ph_poss[3] + (2*(ph_pose[3] - ph_poss[3]))/3;
	dsi->xd = xdptr + i;
	dsi->yd = ydptr + i;
	dsi->nx = dsi->ny = ph_pose[5] - i;
      }



  dsh = build_histo_ds_with_gaussian_convolution(dsi, biny);
  if (dsh == NULL || dsh->nx < 2) return win_printf_ptr("null dsh");
  dsp = find_max_peaks(dsh, mthres);
  if (dsp == NULL || dsp->nx <= 0) return win_printf_ptr("null dsp");
  ax = (float)(dsh->nx-1)/(dsh->xd[dsh->nx-1] - dsh->xd[0]);
  set_ds_treatement(dsp,"Averaging = %d",navg);
  alloc_data_set_y_error(dsp);
  alloc_data_set_y_down_error(dsp);
  alloc_data_set_y_box(dsp);
  alloc_data_set_x_error(dsp);
  // we define the peak boundaries
  for(i = 0, histo_pi = 0; i < dsp->nx; i++)
    {
       // index of corresponding point in histo
       k = (int)(0.5 + ((dsp->xd[i] - dsh->xd[0])*ax));
       if (i == 0)
          {
              tmp_1 = (dsh->xd[0] + dsp->xd[i])/2;
              j_1 = (int)(0.5 + ((tmp_1 - dsh->xd[0])*ax));
          }
       else
          {   // we look for minimum between peak i-1 and i
              j_1 = (int)(0.5 + ((dsp->xd[i-1] - dsh->xd[0])*ax));
              for (j = ki = k, tmp = dsp->yd[i]; j >= j_1 && j < dsh->nx; j--)
                   tmp = (dsh->yd[j] < tmp) ? dsh->yd[ki = j] : tmp;
              j_1 = ki;
          }
       j_1 = (j_1 <= 0) ? 0 : j_1;
       for (j = k; j > j_1 && j < dsh->nx; j--)
           if (dsh->yd[j] < mthres) break;
       j_1 = j;
       if (i == dsp->nx - 1)
          {
              tmp_1 = (dsh->xd[dsh->nx - 1] + dsp->xd[i])/2;
              j1 = (int)(0.5 + ((tmp_1 - dsh->xd[0])*ax));
          }
       else
          {   // we look for minimum between peak i and i + 1
              j1 = (int)(0.5 + ((dsp->xd[i+1] - dsh->xd[0])*ax));
              for (j = ki = k, tmp = dsp->yd[i]; j <= j1 && j < dsh->nx; j++)
                   tmp = (dsh->yd[j] < tmp) ? dsh->yd[ki = j] : tmp;
              j1 = ki;
          }
       j1 = (j1 <= 0) ? 0 : j1;
       for (j = k; j < j1 && j < dsh->nx; j++)
           if (dsh->yd[j] < mthres) break;
       j1 = j;
       if (dsp->yed != NULL) dsp->yed[i] = fabs(dsh->xd[j_1] - dsp->xd[i]);
       if (dsp->ye != NULL) dsp->ye[i] = fabs(dsh->xd[j1] - dsp->xd[i]);
       for (j = j_1; j < j1 && j < dsh->nx; j++)
            histo_pi += dsh->yd[j];
    }

  for(i = 0, histo_i = 0; i < dsh->nx; i++)
      histo_i += dsh->yd[i];

  for(i = 0, ext = 0; i < dsp->nx; i++)
          ext += dsp->yd[i];
  //tmp = (histo_pi > 0) ? histo_i/histo_pi : 1;

  tmp = 1;
  if (histo_i * ext > 0)
    tmp = dsi->nx * histo_pi / (histo_i * ext);

  for(i = 0; i < dsp->nx; i++)
          dsp->yd[i] *= tmp;


  float tmpmin, tmpmax, mean, sig2; // , tmpmino, tmpmaxo
  // new method to find size of plateau
  for(i = dsp->nx-1; i >= 0; i--)
    {
      tmpmin = (i < dsp->nx - 1) ? (dsp->xd[i+1] - dsp->xd[i])/2 : 8 * biny;
      tmpmin = (tmpmin > 8 * biny) ? 8 * biny : tmpmin;
      tmpmax = (i > 0) ? (dsp->xd[i-1] - dsp->xd[i])/2 : -8 * biny;
      tmpmax = (tmpmax < -8* biny) ? -8 * biny : tmpmax;
      //tmpmino = tmpmin;
      //tmpmaxo = tmpmax;
      if (dsp->yed != NULL) tmpmax = -dsp->yed[i];
      if (dsp->ye != NULL) tmpmin = dsp->ye[i];
      for(j = min = max = 0; j < dsi->nx - navg; j++)
	{
	  for (k = 0, mean = 0; k < navg; k++)
	    mean += dsi->yd[j+k];
	  mean /= (navg > 0) ? navg : 1;
	  min = ((mean - dsp->xd[i]) > tmpmin) ? j+navg/2 : min;
	  max = ((mean - dsp->xd[i]) > tmpmax) ? j+navg/2 : max;
	}
      for(j = min, mean = 0, k = 0; j < max; j++)
	{
	  mean += dsi->yd[j];
	  k++;
	}
      mean = (k > 0) ? mean/k : mean;
      for(j = min, sig2 = 0; j < max; j++)
	sig2 += (dsi->yd[j] - mean)*(dsi->yd[j] - mean);
      sig2 = (k > 1) ? sig2/(k-1) : sig2;
      if (dsp->ybu != NULL) dsp->ybu[i] = sqrt(sig2);
      if (dsp->ybd != NULL) dsp->ybd[i] = sqrt(sig2);
      dsp->yd[i] = (dsi->xd[max] + dsi->xd[min])/2;
      dsp->xe[i] = (dsi->xd[max] - dsi->xd[min])/2;
      /*
      win_printf("Peak %d at %g limo + %g limo - %g\n"
		 "lim + %g lim - %g\n"
		 "j min %d -> %g max %d ->%g dt %g\n"
		 ,i,dsp->xd[i],tmpmino,tmpmaxo,tmpmin,tmpmax,min,dsi->xd[min]
		 ,max,dsi->xd[max],dsi->xd[max] - dsi->xd[min]);
      */
    }


  for(i = 0; i < dsp->nx; i++)
    {  //we swap dsp in x and y
       tmp = dsp->xd[i];
       dsp->xd[i] = dsp->yd[i];
       dsp->yd[i] = tmp;
    }
  if (xdptr != NULL && ydptr != NULL)
    {
      dsi->xd = xdptr;
      dsi->yd = ydptr;
      dsi->nx = dsi->ny = dsinx;
    }


  free_data_set(dsh);
  return dsp;
}


d_s **image_cordrift_cut_decreasing_steps_by_histo(d_s *dsi, float mthres, int nmin, float biny)
{
        int i, j, k, ki;
  d_s *dsh = NULL, *dsp = NULL, *dspb = NULL, **dsd = NULL;
  float tmp, tmp_1, ax;
  int j_1, j1;

  (void)nmin;
  if (dsi == NULL || dsi->nx ==0 || dsi->ny == 0) return NULL;

  dsh = build_histo_ds_with_gaussian_convolution(dsi, biny);
  if (dsh == NULL || dsh->nx < 2) return win_printf_ptr("null dsh");
  dsp = find_max_peaks(dsh, mthres);
  if (dsp == NULL || dsp->nx <= 0) return win_printf_ptr("null dsp");
  dspb = build_data_set(dsp->nx,dsp->nx); // peak boundary
  if (dspb == NULL) return win_printf_ptr("null dspb");
  ax = (float)(dsh->nx-1)/(dsh->xd[dsh->nx-1] - dsh->xd[0]);

  // we define the peak boundaries
  for(i = 0; i < dsp->nx; i++)
    {
       // index of corresponding point in histo
       k = (int)(0.5 + ((dsp->xd[i] - dsh->xd[0])*ax));
       if (i == 0)
          {
              tmp_1 = (dsh->xd[0] + dsp->xd[i])/2;
              j_1 = (int)(0.5 + ((tmp_1 - dsh->xd[0])*ax));
          }
       else
          {   // we look for minimum between peak i-1 and i
              j_1 = (int)(0.5 + ((dsp->xd[i-1] - dsh->xd[0])*ax));
              for (j = ki = k, tmp = dsp->yd[i]; j >= j_1 && j < dsh->nx; j--)
                   tmp = (dsh->yd[j] < tmp) ? dsh->yd[ki = j] : tmp;
              j_1 = ki;
          }
       j_1 = (j_1 <= 0) ? 0 : j_1;
       for (j = k; j > j_1 && j < dsh->nx; j--)
           if (dsh->yd[j] < mthres) break;
       dspb->xd[i] = dsh->xd[j];
       if (i == dsp->nx - 1)
          {
              tmp_1 = (dsh->xd[dsh->nx - 1] + dsp->xd[i])/2;
              j1 = (int)(0.5 + ((tmp_1 - dsh->xd[0])*ax));
          }
       else
          {   // we look for minimum between peak i and i + 1
              j1 = (int)(0.5 + ((dsp->xd[i+1] - dsh->xd[0])*ax));
              for (j = ki = k, tmp = dsp->yd[i]; j <= j1 && j < dsh->nx; j++)
                   tmp = (dsh->yd[j] < tmp) ? dsh->yd[ki = j] : tmp;
              j1 = ki;
          }
       j1 = (j1 <= 0) ? 0 : j1;
       for (j = k; j < j1 && j < dsh->nx; j++)
           if (dsh->yd[j] < mthres) break;
       dspb->yd[i] = dsh->xd[j];
    }
  dsd = (d_s **)calloc(dsp->nx,sizeof(d_s*));
  if (dsd == NULL) return NULL;
  for(i = 0; i < dsp->nx; i++)
     {
         dsd[i] = build_data_set(16,16); // steps
         if (dsd[i] == NULL) return NULL;
         dsd[i]->nx = dsd[i]->ny = 0;
     }
  for(k = 0; k < dsi->nx; k++)
    {
        for(i = 0; i < dsp->nx; i++)
          {
             if(dsi->yd[k] >= dspb->xd[i] && dsi->yd[k] <= dspb->yd[i])
                {
                   add_new_point_to_ds(dsd[i], dsi->xd[k], dsi->yd[k]);
                   break;
                }
          }
    }
  return dsd;
}

int do_cordrift_find_peaks_rate_and_duration(void)
{
  int i;
  O_p *op = NULL, *opn = NULL;
  d_s *dsi = NULL, *ds = NULL, *dsb = NULL;
  pltreg *pr = NULL;
  static int nmin = 3;
  static float biny = 0.005;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("find peaks rate and mean blockage duration using histo \n"
		"gaussian binsize of histo %6f\n"
		"Do not keep peaks having less than %4d points\n"
		,&biny,&nmin);
  if (i == WIN_CANCEL)	return OFF;


  opn = build_histo_with_exponential_convolution(pr, op, NULL, biny);
  if (opn == NULL) win_printf_OK("cannot create plot !");
  ds = find_max_peaks(opn->dat[0], ((float)nmin)/2);
  if (ds == NULL || ds->nx <= 0) return warning_message("null dsp");
  add_one_plot_data(opn, IS_DATA_SET, (void*)ds);
  dsb = find_histo_peak_boundary(opn->dat[0], ds, ((float)nmin)/2);
  if (dsb == NULL || dsb->nx <= 0) return win_printf_OK("null dspb");
  add_one_plot_data(opn, IS_DATA_SET, (void*)dsb);
  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}

O_i* draw_noise_vs_angles(pltreg *pr,O_p *op, imreg *imr, d_s *dsz, d_s *dsx, d_s *dsy, float angle_max,  float angle_step, int fft_window, int fft_shift, float freq_low, float freq_high)
{
  un_s *un = NULL;
  float dy, dt;
  int nstep = (int) (angle_max/angle_step);
  float v1[3];
  float v2[3];
  d_s *dszrot = NULL;
  d_s *dsspec;
  float mean;
  float min = FLT_MAX;
  int imin = 0;
  int jmin = 0;
  int addp;
  float angle_x;
  float angle_y;
  float **rot_mat;
  d_s *dsrotx, *dsroty;
  int i =0, j=0, k=0;
  O_p *opn = NULL;

  //d_s *dsangle;


  if (dsz->nx != dsx->nx || dsz->nx != dsy->nx) return(win_printf_OK("Ds do not have same size"));
  dszrot = build_data_set(dsz->nx,dsz->nx);
  un = op->yu[op->c_yu];
	if (un->type != IS_METER || un->decade != IS_MICRO)
		win_printf("warning the y unit is not \\mu m !\nnumerical value will be wrong");
	dy = un->dx;
	un = op->xu[op->c_xu];
	if (un->type != IS_SECOND || un->decade != 0)
		win_printf("warning the x unit is not seconds !\nnumerical value will be wrong");
/*	dt = un->dx;*/
	dt = un->dx * (dsz->xd[dsz->nx-1] - dsz->xd[0]);
	dt = (dsz->nx > 1) ? dt/(dsz->nx-1) : dt;
  //dsangle = build_data_set(16,16);
  //alloc_data_set_y_error(dsangle);
  //dsangle->nx = dsangle ->ny = 0;

  O_i *oid = create_and_attach_oi_to_imr(imr, 2*nstep, 2*nstep, IS_FLOAT_IMAGE);

  for ( i = -nstep;i <nstep;i++)
  {
    for ( j = -nstep;j<nstep;j++)
    {
      printf("i%d j %d\n",i,j);
      angle_x = i*angle_step;
      angle_y = j*angle_step;
      if (angle_x ==0 && angle_y == 0)
      {
        dsspec = sliding_spectrum(dsz, NULL, fft_window, fft_shift, 1, 0,1, 1, 1,  dy,  dt);
      }
      else{


      v1[0]=(float)tan(-angle_x);
      v1[1]=(float)tan(-angle_y);
      v1[2]=1;
      v2[0]=0;
      v2[1]=0;
      v2[2]=1;
      rot_mat = compute_rotation_matrix_bt_2_vectors(v1,v2);



      for ( k =0;k<dsz->nx;k++)
      {
        if (dsz->xd[k] != dsx->xd[k]) return(win_printf_OK("X are not synchronized"));
        if (dsz->xd[k] != dsy->xd[k]) return(win_printf_OK("X are not synchronized"));
        dszrot->yd[k]=rot_mat[2][0]*dsx->yd[k]+rot_mat[2][1]*dsy->yd[k]+rot_mat[2][2]*dsz->yd[k];
      }
        dsspec = sliding_spectrum(dszrot, NULL, fft_window, fft_shift, 1, 0,1, 1, 1,  dy,  dt);
      }
        mean = 0;
        addp = 0;
        for (int jj = 0;jj<dsspec->nx;jj++)
        {
          if ((dsspec->xd[jj] > freq_low) && (dsspec->xd[jj] <freq_high))
          {
            mean += dsspec->yd[jj];
            addp+=1;
          }
        }
        mean/=addp;
        if (mean < min)
        {
          imin = i;
          jmin = j;
          min = mean;
        }

        free(dsspec);

        oid->im.pixel[nstep+i].fl[nstep+j] = mean;
      }
    }
create_attach_select_x_un_to_oi(oid, IS_RAW_U, -angle_max, angle_step, 0, 0, "rad");
create_attach_select_y_un_to_oi(oid, IS_RAW_U, -angle_max, angle_step, 0, 0, "rad");
opn = create_and_attach_one_plot(pr,2*nstep,2*nstep,0);
create_attach_select_x_un_to_op(opn, IS_RAW_U, -angle_max, angle_step, 0, 0, "rad");
create_attach_select_y_un_to_op(opn, IS_RAW_U, -angle_max, angle_step, 0, 0, "rad");
dsrotx = opn->dat[0];
dsroty = create_and_attach_one_ds(opn,2*nstep,2*nstep,0);

set_ds_source(dsrotx,"Integrated noise as a function of x angle");
set_ds_source(dsroty,"Integrated noise as a function of y angle");

dsrotx ->nx = dsrotx->ny = 0;
dsroty->nx = dsroty->ny = 0;
i = imin;
  for ( j = -nstep;j<nstep;j++)
  {
    angle_x = i*angle_step;
    angle_y = j*angle_step;
    if (angle_x ==0 && angle_y == 0)
    {
      dsspec = sliding_spectrum(dsz, NULL, fft_window, fft_shift, 1, 0,1, 1, 1,  dy,  dt);
    }
    else{


    v1[0]=(float)tan(-angle_x);
    v1[1]=(float)tan(-angle_y);
    v1[2]=1;
    v2[0]=0;
    v2[1]=0;
    v2[2]=1;
    rot_mat = compute_rotation_matrix_bt_2_vectors(v1,v2);



    for ( k =0;k<dsz->nx;k++)
    {
      if (dsz->xd[k] != dsx->xd[k]) return(win_printf_OK("X are not synchronized"));
      if (dsz->xd[k] != dsy->xd[k]) return(win_printf_OK("X are not synchronized"));
      dszrot->yd[k]=rot_mat[2][0]*dsx->yd[k]+rot_mat[2][1]*dsy->yd[k]+rot_mat[2][2]*dsz->yd[k];
    }
      dsspec = sliding_spectrum(dszrot, NULL, fft_window, fft_shift, 1, 0,1, 1, 1,  dy,  dt);
    }
      mean = 0;
      addp = 0;
      for (int jj = 0;jj<dsspec->nx;jj++)
      {
        if ((dsspec->xd[jj] > freq_low) && (dsspec->xd[jj] <freq_high))
        {
          mean += dsspec->yd[jj];
          addp+=1;
        }
      }
      mean/=addp;
      free(dsspec);
      add_new_point_to_ds(dsroty,angle_y,mean);

    }
    j = jmin;
      for ( i = -nstep;i<nstep;i++)
      {
        angle_x = i*angle_step;
        angle_y = j*angle_step;
        if (angle_x ==0 && angle_y == 0)
        {
          dsspec = sliding_spectrum(dsz, NULL, fft_window, fft_shift, 1, 0,1, 1, 1,  dy,  dt);
        }
        else{


        v1[0]=(float)tan(-angle_x);
        v1[1]=(float)tan(-angle_y);
        v1[2]=1;
        v2[0]=0;
        v2[1]=0;
        v2[2]=1;
        rot_mat = compute_rotation_matrix_bt_2_vectors(v1,v2);



        for ( k =0;k<dsz->nx;k++)
        {
          if (dsz->xd[k] != dsx->xd[k]) return(win_printf_OK("X are not synchronized"));
          if (dsz->xd[k] != dsy->xd[k]) return(win_printf_OK("X are not synchronized"));
          dszrot->yd[k]=rot_mat[2][0]*dsx->yd[k]+rot_mat[2][1]*dsy->yd[k]+rot_mat[2][2]*dsz->yd[k];
        }
          dsspec = sliding_spectrum(dszrot, NULL, fft_window, fft_shift, 1, 0,1, 1, 1,  dy,  dt);
        }
          mean = 0;
          addp = 0;
          for (int jj = 0;jj<dsspec->nx;jj++)
          {
            if ((dsspec->xd[jj] > freq_low) && (dsspec->xd[jj] <freq_high))
            {
              mean += dsspec->yd[jj];
              addp+=1;
            }
          }
          mean/=addp;
          free(dsspec);
          add_new_point_to_ds(dsrotx,angle_x,mean);

        }





for ( i = 0;i<3;i++) free(rot_mat[i]);
free(rot_mat);
free_data_set(dszrot);
return oid;
}

int do_draw_noise_vs_angles(void)
{
  int i = 0;
  int nsp;
  int nshift;
  static float angle_max = 0.3;
  static float angle_step = 0.01;
  if(updating_menu_state != 0)	return D_O_K;
  O_i *oid = NULL;
  pltreg *pr;
  O_p *op;
  d_s *dsi;
  O_p *opres;
  imreg *imr;
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
	if ( i != 3)		return win_printf("cannot find data");
  static float freq_low = 0;
  static float freq_high = 0;
  d_s *dsz, *dsx, *dsy;
  d_s *dsres ;

  if (op->n_dat < 3) return 0;

  dsz = op->dat[2];
  dsx = op->dat[0];
  dsy = op->dat[1];

	 nsp = get_config_int("spectrum", "slidding_size", 256);
	 nshift = get_config_int("spectrum", "slidding_offset", 128);

	i = win_scanf("slidding spectrum along signal\n"
 "select fft number of points %d"
		"and shift between sample %d"
		"Max angles : %f\n"
    "Angle steps : %f\n"
  "Averaging between freq lox %f and freq high %f \n",&nsp,&nshift,&angle_max,&angle_step,&freq_low,&freq_high);
	if (i == WIN_CANCEL)	return 1;
  imr = create_and_register_new_image_project(0,   32,  900,  668);

	set_config_int("spectrum", "slidding_size", nsp);
	set_config_int("spectrum", "slidding_offset", nshift);
  //opres = create_and_attach_one_plot(pr,16,16,0);
  //free_data_set(opres->dat[0]);
  oid = draw_noise_vs_angles(pr,op,imr,dsz,dsx,dsy,  angle_max,   angle_step,  nsp, nshift,freq_low,freq_high);
//  opres->dat[0] = dsres;

  remove_image (imr, 0, imr->o_i[0]);


  return 0;

}




int do_cordrift_reproduce_decreasing_steps_duration(void)
{
  int i, j, curop, k;
  O_p *op = NULL, *opn = NULL;
  d_s *dsi = NULL, *ds = NULL;
  pltreg *pr = NULL;
  static int nmin = 6, oligo_cor = 1, use_deriv = 0, insert = 1, keep_err = 0, ini_t = 0, ph5 = 1;
  static float biny = 0.005, mthres = 3, oligo_dz = 0.001, bin_max = 0.015;
  double sigHF = 0, uperlimit;
  char question[2048] = {0};

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  uperlimit = fabs(op->y_hi - op->y_lo);
  get_sigma_of_derivative_of_partial_ds(dsi, 0, dsi->nx, uperlimit, &sigHF);




  snprintf(question,sizeof(question),"transform data set in blockage duration using histogram to find them\n"
	   "The selected dataset has a noise of %g\n"
	   "%%R1) Use a fix bin size for the histogram\n"
	   "%%r2) compute bin size from signal derivative\n"
	   "In case 1) specify gaussian binsize of histo %%6f\n"
	   "in case 2) specify the maximum bin size accepetd %%6f\n"
	   "Nb. of points averaged to find boudary %%4d points\n"
	   "Exclude points with averaging smaller than %%6f\n"
	   "Apply treatment to (%%R-all) (%%r->End of PH3 to end of PH5) (%%r->PH5 only)\n"
	   "All data sets are proceeded and result is placed in a new plot\n"
	   "%%R->No oligo length correction\n"
	   "%%r->Oligo correction with the actual numbers of steps\n"
	   "%%r->Oligo correction with the estimated numbers of steps\n"
	   "Define the Z correction of 1 oligo %%5f\n"
	   "Insert plot just after the treated plot %%b\n"
	   "Keep error bars %%b; Add initial time of ds in X %%b\n"
	   ,sigHF/sqrt(2));

  i = win_scanf(question,&use_deriv,&biny,&bin_max,&nmin,&mthres,&ph5,&oligo_cor,&oligo_dz,&insert,&keep_err,&ini_t);
  if (i == WIN_CANCEL)	return OFF;


  i = op->n_dat;
  op->n_dat = 0; // we want to copy only the plot stuuff
  curop = pr->cur_op;
  opn = duplicate_plot_data(op, NULL);
  if (opn == NULL) win_printf_OK("cannot create plot !");
  add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
  op->n_dat = i;
  for (j = pr->n_op - 1 ;insert > 0 && j > curop+1; j--)
    pr->o_p[j] = pr->o_p[j-1];
  if (insert) pr->o_p[curop+1] = opn;


  for(i = 0; i < op->n_dat; i++)
    {
      dsi = op->dat[i];
      if (use_deriv)
	{
	  get_sigma_of_derivative_of_partial_ds(dsi, 0, dsi->nx, uperlimit, &sigHF);
	  //sigHF /= sqrt(2);
	  if (sigHF > (double)bin_max) continue;
	  ds = cordrift_reproduce_decreasing_steps_duration(dsi, mthres, nmin, (float)sigHF,ph5);
	}
      else ds = cordrift_reproduce_decreasing_steps_duration(dsi, mthres, nmin, biny, ph5);
      if (keep_err == 0)
	{
	  free_data_set_y_down_error(ds);
	  free_data_set_y_error(ds);
	}
      if (ds != NULL)
	{
          correct_blocking_for_dsDNA_oligo_length(ds, oligo_cor, oligo_dz);
	  add_one_plot_data(opn, IS_DATA_SET, (void*)ds);
	  if (ini_t) for (k = 0; k < ds->nx; k++)
		       ds->xd[k] += dsi->xd[0];
	}
      else warning_message("null ds");
    }

  if (opn == NULL) win_printf_OK("cannot create plot !");
  refresh_plot(pr, (insert) ? curop+1:pr->n_op-1);
  return D_O_K;
}

int do_cordrift_reproduce_decreasing_steps(void)
{
  int i;
  O_p *op = NULL, *opn = NULL;
  d_s *dsi = NULL, *ds = NULL;
  pltreg *pr = NULL;
  static int nmin = 3;
  static float biny = 0.005, mthres = 3;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("transform  data set in steps using histo \n"
		"gaussian binsize of histo %6f\n"
		"Do not keep stretch of consecutive points having less than %4d points\n"
		"Exclude points with averaging smaller than %6f\n"
		"All data sets are proeeded and result is placed in a new plot\n"
		,&biny,&nmin,&mthres);
  if (i == WIN_CANCEL)	return OFF;


  i = op->n_dat;
  op->n_dat = 0; // we want to copy only the plot stuuff
  opn = duplicate_plot_data(op, NULL);
  if (opn == NULL) win_printf_OK("cannot create plot !");
  add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
  op->n_dat = i;

  for(i = 0; i < op->n_dat; i++)
    {
      dsi = op->dat[i];
      ds = cordrift_reproduce_decreasing_steps(dsi, mthres, nmin, biny);
      if (ds != NULL)
	{
	  add_one_plot_data(opn, IS_DATA_SET, (void*)ds);
	}
      else warning_message("null ds");
    }

  if (opn == NULL) win_printf_OK("cannot create plot !");
  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}



d_s *cordrift_remove_high_derivative_part_of_ds(d_s *dsi, int nei, int nmin, float deltaz , int repeat,
						      int max, int ex_size, float p
						      , float sigm)
{
  int i, j, itmp = 0, itmp2 = 0;
  d_s *ds = NULL, *dsd = NULL;
  double chi2;


  (void)nmin;
  if (dsi == NULL || dsi->nx ==0 || dsi->ny == 0) return NULL;

  dsd = jumpfit_vc_2_exclude_edge(dsi, NULL, repeat, max, ex_size, sigm, p, &chi2);
  if (dsd == NULL) return win_printf_ptr("cannot do non-linear filter !");
  dsd->yd[dsd->nx-1] = dsi->yd[dsd->nx-1]; // small last point bug
  dsd->xd[dsd->nx-1] = dsi->xd[dsd->nx-1]; // small last point bug
  //dsd->ny = dsd->nx = (dsd->nx > 0) ? dsd->nx-1 : 0;
  for (i = 0; i < dsd->nx-1; i++)
    {  // we differanciate
      dsd->yd[i] = dsd->yd[i+1] - dsd->yd[i];
      for (j = 0; j < dsd->nx && j < dsi->nx; j++)
	{
	  if (dsi->xd[j] <= dsd->xd[i]) itmp = j;
	  if (dsi->xd[j] <= dsd->xd[i+1]) itmp2 = j;
	}
      dsd->xd[i] = (float)((int)(itmp + itmp2/2));
    }
  dsd->ny = dsd->nx = (dsd->nx > 0) ? dsd->nx-1 : 0;
  for (i = 0; i < dsd->nx; i++)
    if (fabs(dsd->yd[i]) > deltaz) dsd->yd[i] = -FLT_MAX;
  for (i = 1; i < dsd->nx; i++)
    {
      if ((dsd->yd[i-1] != -FLT_MAX) && (dsd->yd[i] == -FLT_MAX))
	{
	  for (j = i - nei; j < i; j++)
	    dsd->yd[(j<0)?0:j] = -FLT_MAX;
	}
    }
  for (i = dsd->nx - 1; i > 0; i--)
    {
      if ((dsd->yd[i-1] == -FLT_MAX) && (dsd->yd[i] != -FLT_MAX))
	{
	  for (j = i; j < i+nei; j++)
	    dsd->yd[(j < dsd->nx) ? j :dsd->nx-1] = -FLT_MAX;
	}
    }
  for (i = 0, j = 0; i < dsd->nx; i++)
    {
      j += ((dsd->yd[i] != -FLT_MAX)) ? 1 : 0;
    }
  j += ((dsd->yd[dsd->nx-1] != -FLT_MAX)) ? 1 : 0;
  if (j == 0)
    {
      free_data_set(dsd);
      return NULL;
    }
  if ((ds = build_data_set(j, j)) != NULL)
    {
      for (i = 0, j = 0; i < dsd->nx; i++)
	{
	  if ((dsd->yd[i] != -FLT_MAX))
	  {
	    ds->xd[j] = dsi->xd[i];//(int)dsd->xd[i]];
	    ds->yd[j] = dsi->yd[i];//(int)dsd->xd[i]];
	    j++;
	  }
	}
      if (j < ds->nx)
	{
	    ds->xd[j] = dsi->xd[dsi->nx-1];
	    ds->yd[j] = dsi->yd[dsi->nx-1];
	    j++;
	}
    }
  free_data_set(dsd);
  dsd = NULL;
  return ds;
}



O_p *cordrift_remove_high_derivative_part_of_data_set(O_p *op, int nei, int nmin,
						      float deltaz , int repeat,
						      int max, int ex_size, float p
						      , float sigm)
{
  int i, j, k, itmp = 0, itmp2 = 0;
  O_p *opn = NULL;
  int id, n_dat, ki;
  d_s *ds = NULL, *dsi = NULL, *dsd = NULL;
  double chi2;

  if (op == NULL) return NULL;

  i = op->n_dat;
  op->n_dat = 0; // we want to copy only the plot stuuff
  opn = duplicate_plot_data(op, NULL);
  if (opn == NULL) win_printf_OK("cannot create plot !");
  op->n_dat = i;

  for (id = 0, n_dat = op->n_dat; id < n_dat; id++)
    {
      dsi = op->dat[id];
      if (dsi->nx ==0 || dsi->ny == 0) continue;
      dsd = jumpfit_vc_2_exclude_edge(dsi, NULL, repeat, max, ex_size, sigm, p, &chi2);
      if (dsd == NULL) return win_printf_ptr("cannot do non-linear filter !");
      dsd->yd[dsd->nx-1] = dsi->yd[dsd->nx-1]; // small last point bug
      //dsd->ny = dsd->nx = (dsd->nx > 0) ? dsd->nx-1 : 0;
      for (i = 0; i < dsd->nx-1; i++)
	{  // we differanciate
	  dsd->yd[i] = dsd->yd[i+1] - dsd->yd[i];
	  for (j = 0; j < dsd->nx && j < dsi->nx; j++)
	    {
	      if (dsi->xd[j] <= dsd->xd[i]) itmp = j;
	      if (dsi->xd[j] <= dsd->xd[i+1]) itmp2 = j;
	    }
	  dsd->xd[i] = (float)((int)(itmp + itmp2/2));
	}
      dsd->ny = dsd->nx = (dsd->nx > 0) ? dsd->nx-1 : 0;
      for (i = 0; i < dsd->nx; i++)
	if (fabs(dsd->yd[i]) > deltaz) dsd->yd[i] = -FLT_MAX;
      for (i = 1; i < dsd->nx; i++)
	{
	  if ((dsd->yd[i-1] != -FLT_MAX) && (dsd->yd[i] == -FLT_MAX))
	    {
	      for (j = i - nei; j < i; j++)
		dsd->yd[(j<0)?0:j] = -FLT_MAX;
 	    }
 	}
      for (i = dsd->nx - 1; i > 0; i--)
 	{
           if ((dsd->yd[i-1] == -FLT_MAX) && (dsd->yd[i] != -FLT_MAX))
 	     {
 	       for (j = i; j < i+nei; j++)
		 dsd->yd[(j < dsd->nx) ? j :dsd->nx-1] = -FLT_MAX;
             }
        }
      for (i = dsd->nx - 1, j = 1; i >= 0; i--)
	{
	  if ((dsd->yd[i] != -FLT_MAX))      dsd->yd[i] = j++;
	  else j = 0;
	}
      for (i = 0; i < dsd->nx; i++)
	{
        if ((dsd->yd[i] != -FLT_MAX))
	  {
            j = dsd->yd[i];
	      if (j > nmin && j < dsi->nx)
		{
		  if ((ds = create_and_attach_one_ds(opn, j, j, 0)) == NULL)
		    return win_printf_ptr("cannot create plot !");
		  inherit_from_ds_to_ds(ds, dsi);
		  for (k = 0; (k < j) && (k < dsi->nx); k++)
		    {
		      ki = i + k;//(int)(dsd->xd[i]) + k;
		      if (ki < 0 || ki >= dsi->nx) continue;
		      ds->yd[k] = dsi->yd[ki];
		      ds->xd[k] = dsi->xd[ki];
		    }
		}
	      if (j>0) i += j - 1;
	    }
	}
      for (i = dsd->nx - 1; i >= 0; i--)
	{
	  if ((dsd->yd[i] == -FLT_MAX))      dsd->yd[i] = -5;
	}

      free_data_set(dsd);
      dsd = NULL;
    }
  return opn;
}

// d_s *dsic is the selected data set specifying the color of the data sets that will be treated
// if NULL all ds are treated
O_p *cordrift_remove_high_derivative_part_of_data_set_by_color(O_p *op, d_s *dsic, int nei, int nmin,
                                                               float deltaz , int repeat,
                                                               int max, int ex_size, float p
                                                               , float sigm, float oligo_dz, int oligo_cor)
{
  int i, j, k, itmp = 0, itmp2 = 0;
  O_p *opn = NULL;
  int id, n_dat, ki, step = 0, max_step = 0, i_tmp = 0, n_cycle, n_bead, max_dz ;
  d_s *ds = NULL, *dsi = NULL, *dsd = NULL;
  float tmp, z_last_step = 0, z_step0 = 0;
  double chi2;
  char *st = NULL;

  if (op == NULL) return NULL;

  i = op->n_dat;
  op->n_dat = 0; // we want to copy only the plot stuuff
  opn = duplicate_plot_data(op, NULL);
  if (opn == NULL) win_printf_OK("cannot create plot !");
  op->n_dat = i;

  for (id = 0, n_dat = op->n_dat; id < n_dat; id++)
    {
      dsi = op->dat[id];
      if (dsi->nx ==0 || dsi->ny == 0) continue;
      if (dsic != NULL && dsi->color != dsic->color) continue;
      n_cycle =  n_bead = -1;
      if (dsi->source != NULL)
         {
            st = strstr(dsi->source, "bead");
            if (st != NULL)     sscanf(st, "bead %d",&n_bead);
            st = strstr(dsi->source, "cycle");
            if (st != NULL)     sscanf(st, "cycle %d",&n_cycle);
         }
      dsd = jumpfit_vc_2_exclude_edge(dsi, NULL, repeat, max, ex_size, sigm, p, &chi2);
      if (dsd == NULL) return win_printf_ptr("cannot do non-linear filter !");
      dsd->yd[dsd->nx-1] = dsi->yd[dsd->nx-1]; // small last point bug
      //dsd->ny = dsd->nx = (dsd->nx > 0) ? dsd->nx-1 : 0;
      for (i = 0; i < dsd->nx-1; i++)
	{  // we differanciate
	  dsd->yd[i] = dsd->yd[i+1] - dsd->yd[i];
	  for (j = 0; j < dsd->nx && j < dsi->nx; j++)
	    {
	      if (dsi->xd[j] <= dsd->xd[i]) itmp = j;
	      if (dsi->xd[j] <= dsd->xd[i+1]) itmp2 = j;
	    }
	  dsd->xd[i] = (float)((int)(itmp + itmp2/2));
	}
      dsd->ny = dsd->nx = (dsd->nx > 0) ? dsd->nx-1 : 0;
      for (i = 0; i < dsd->nx; i++)
	if (fabs(dsd->yd[i]) > deltaz) dsd->yd[i] = -FLT_MAX;
      for (i = 1; i < dsd->nx; i++)
	{
	  if ((dsd->yd[i-1] != -FLT_MAX) && (dsd->yd[i] == -FLT_MAX))
	    {
	      for (j = i - nei; j < i; j++)
		dsd->yd[(j<0)?0:j] = -FLT_MAX;
 	    }
 	}
      for (i = dsd->nx - 1; i > 0; i--)
 	{
           if ((dsd->yd[i-1] == -FLT_MAX) && (dsd->yd[i] != -FLT_MAX))
 	     {
 	       for (j = i; j < i+nei; j++)
		 dsd->yd[(j < dsd->nx) ? j :dsd->nx-1] = -FLT_MAX;
             }
        }
      for (i = dsd->nx - 1, j = 1; i >= 0; i--)
	{
	  if ((dsd->yd[i] != -FLT_MAX))      dsd->yd[i] = j++;
	  else j = 0;
	}
      for (i = 0, step = 0, tmp = 0; i < dsd->nx; i++)
	{
           if ((dsd->yd[i] != -FLT_MAX))
	   {
              j = dsd->yd[i];
	      if (j > nmin && j < dsi->nx) {
                  for (k = 0, tmp = 0, i_tmp = 0; (k < j) && (k < dsi->nx); k++) {
		      ki = i + k;
                      tmp += dsi->yd[ki];
                      i_tmp++;
                  }
                  tmp = (i_tmp > 0) ? tmp/i_tmp : tmp;
                  if (step == 0) z_step0 = tmp;
                  step++;
              }
              if (j>0) i += j - 1;
	    }
	}
      max_step = step;
      z_last_step = tmp;
      my_set_window_title("treating bead %d cycle %d with %d steps 1st at %g last at %g"
                          ,n_bead,n_cycle,max_step,z_step0,z_last_step);
      tmp = (z_step0 > z_last_step) ? (z_step0 / (z_step0 - z_last_step)) : 2;
      tmp = (tmp > 2) ? 2 : tmp;
      tmp = (tmp < 0) ? 0 : tmp;
      if (oligo_cor == 0) max_dz = 0;
      else if (oligo_cor == 1) max_dz = max_step - 1;
      else max_dz = (int)(0.5 + (tmp * max_step)) - 1;
      max_dz = (max_dz < 0) ? 0 : max_dz;
      for (i = 0, step = 0; i < dsd->nx; i++)
	{
        if ((dsd->yd[i] != -FLT_MAX))
	  {
            j = dsd->yd[i];
	      if (j > nmin && j < dsi->nx)
		{
		  if ((ds = create_and_attach_one_ds(opn, j, j, 0)) == NULL)
		    return win_printf_ptr("cannot create plot !");
		  inherit_from_ds_to_ds(ds, dsi);
                  set_ds_treatement(ds, "Bead %d cycle %d step %d in %d oligo correction %g step 0 -> %g last step -> %g ; tmp %g max_dz %d "
                                    , n_bead,n_cycle,step,max_step
                                    , (oligo_cor) ? (float)(oligo_dz * (max_dz - step)) : 0
                                    ,z_step0, z_last_step, tmp, max_dz);
		  for (k = 0; (k < j) && (k < dsi->nx); k++)
		    {
		      ki = i + k;//(int)(dsd->xd[i]) + k;
		      if (ki < 0 || ki >= dsi->nx) continue;
		      ds->yd[k] = dsi->yd[ki];
                      ds->yd[k] += (oligo_cor) ? (float)(oligo_dz * (max_dz - step)) : 0;
		      ds->xd[k] = dsi->xd[ki];
		    }
                  step++;
		}
	      if (j>0) i += j - 1;
	    }
	}
      for (i = dsd->nx - 1; i >= 0; i--)
	{
	  if ((dsd->yd[i] == -FLT_MAX))      dsd->yd[i] = -5;
	}

      free_data_set(dsd);
      dsd = NULL;
    }
  return opn;
}


int correct_blocking_for_dsDNA_oligo_length(d_s *dsi, int mode, float oligo_dz)
{
    int i, max_step;
    float tmp, z_step0, z_last_step, max_dz;

    if (dsi == NULL || dsi->nx < 2) return 1;
    z_step0 = dsi->yd[0];
    z_last_step = dsi->yd[dsi->nx-1];
    max_step = dsi->nx;

    tmp = (z_step0 > z_last_step) ? (z_step0 / (z_step0 - z_last_step)) : 2;
    tmp = (tmp > 2) ? 2 : tmp;
    tmp = (tmp < 0) ? 0 : tmp;
    if (mode == 0) max_dz = 0;
    else if (mode == 1) max_dz = max_step - 1;
    else max_dz = (int)(0.5 + (tmp * max_step)) - 1;
    max_dz = (max_dz < 0) ? 0 : max_dz;
    for (i = 0; i < dsi->nx; i++)
         dsi->yd[i] += (mode) ? (float)(oligo_dz * (max_dz - i)) : 0;
    return 0;
}



int do_cordrift_remove_high_derivative_part_of_each_data_set(void)
{
  int i;
  O_p *op = NULL, *opn = NULL;
  static int nei = 1, nmin = 5;
  static float deltaz = 0.01;
  d_s *dsi = NULL, *ds = NULL;
  pltreg *pr = NULL;
  static int repeat = 1, max = 32, ex_size = 0;
  static float p = 13, sigm = 0.003;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Remove points in data set where the derivative, done after a non-linear \n"
		"filter, is stronger than %6f\n"
		"Exclude neighbor points j with j = %4d\n"
		"Do not keep stretch of consecutive points having less than %4d points\n"
		"Non-linear fit parameters:\n"
		"Number of times the filter is applied? %8d. \n"
		"Define the bounds on the # points used per fit w= (2m + 1):\n"
		"maxixum size for averaging m = %8d  \n"
		"Define the the sigma value of noise= %8f; \n"
		"and the weighting power p= %8f\n"
		"Exclude points with averaging smaller than %8d\n"
		"All data sets are proeeded and result is placed in a new plot\n"
		,&deltaz,&nei,&nmin,&repeat, &max, &sigm, &p, &ex_size);
  if (i == WIN_CANCEL)	return OFF;


  i = op->n_dat;
  op->n_dat = 0; // we want to copy only the plot stuuff
  opn = duplicate_plot_data(op, NULL);
  if (opn == NULL) win_printf_OK("cannot create plot !");
  add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
  op->n_dat = i;

  for(i = 0; i < op->n_dat; i++)
    {
      dsi = op->dat[i];
      ds = cordrift_remove_high_derivative_part_of_ds(dsi, nei, nmin, deltaz , repeat,
						      max, ex_size, p, sigm);
      if (ds != NULL)
	{
	  add_one_plot_data(opn, IS_DATA_SET, (void*)ds);
	}
    }

  if (opn == NULL) win_printf_OK("cannot create plot !");
  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}


int do_cordrift_remove_high_derivative_part_of_data_set_2(void)
{
  int i;
  O_p *op = NULL, *opn = NULL;
  static int nei = 1, nmin = 5;
  static float deltaz = 0.01;
  d_s *dsi = NULL;
  pltreg *pr = NULL;
  static int repeat = 1, max = 32, ex_size = 0;
  static float p = 13, sigm = 0.003;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Cut data set in continuous pieces excluding regions where\n"
		"the derivative, done after a non-linear filter, is stronger than %6f\n"
		"Exclude neighbor points j with j = %4d\n"
		"Do not keep stretch of consecutive points having less than %4d points\n"
		"Non-linear fit parameters:\n"
		"Number of times the filter is applied? %8d. \n"
		"Define the bounds on the # points used per fit w= (2m + 1):\n"
		"maxixum size for averaging m = %8d  \n"
		"Define the the sigma value of noise= %8f; \n"
		"and the weighting power p= %8f\n"
		"Exclude points with averaging smaller than %8d\n"
		"All data sets are proeeded and result is placed in a new plot\n"
		,&deltaz,&nei,&nmin,&repeat, &max, &sigm, &p, &ex_size);
  if (i == WIN_CANCEL)	return OFF;


  opn = cordrift_remove_high_derivative_part_of_data_set(op, nei, nmin, deltaz, repeat, max, ex_size, p, sigm);


  if (opn == NULL) win_printf_OK("cannot create plot !");
  add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);

  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}


int do_cordrift_remove_high_derivative_part_of_data_set_2_color(void)
{
  int i;
  O_p *op = NULL, *opn = NULL;
  static int nei = 1, nmin = 5;
  static float deltaz = 0.01;
  d_s *dsi = NULL;
  pltreg *pr = NULL;
  static int repeat = 1, max = 32, ex_size = 0, match_color = 1, oligo_cor = 1;
  static float p = 13, sigm = 0.003, oligo_dz = 0.001;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Cut data set in continuous pieces excluding regions where\n"
		"the derivative, done after a non-linear filter, is stronger than %6f\n"
		"Exclude neighbor points j with j = %4d\n"
		"Do not keep stretch of consecutive points having less than %4d points\n"
		"Non-linear fit parameters:\n"
		"Number of times the filter is applied? %8d. \n"
		"Define the bounds on the # points used per fit w= (2m + 1):\n"
		"maxixum size for averaging m = %8d  \n"
		"Define the the sigma value of noise= %8f; \n"
		"and the weighting power p= %8f\n"
		"Exclude points with averaging smaller than %8d\n"
		"Treat %RAll data sets or only %r datasets having the same color than the selected one\n"
                "result is placed in a new plot\n"
                "%R->No oligo length correction\n"
                "%r->Oligo correction with the actual numbers of steps\n"
                "%r->Oligo correction with the estimated numbers of steps\n"
                "Define the Z correction of 1 oligo %5f\n"
		,&deltaz,&nei,&nmin,&repeat, &max, &sigm, &p, &ex_size, &match_color, &oligo_cor,&oligo_dz);
  if (i == WIN_CANCEL)	return OFF;

  if (match_color == 0) dsi = NULL;

  opn = cordrift_remove_high_derivative_part_of_data_set_by_color(op, dsi, nei, nmin, deltaz, repeat
                                                                  , max, ex_size, p, sigm, oligo_dz, oligo_cor);


  if (opn == NULL) win_printf_OK("cannot create plot !");
  add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);

  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}





int do_cordrift_remove_high_derivative_part_of_data_set(void)
{
  int i, j, k, itmp = 0, itmp2 = 0;
  O_p *op = NULL, *opn = NULL;
  int id, ids, n_dat, ki;
  static int nei = 1, all_ds = 0, new_plt = 1, nmin = 5;
  static float deltaz = 0.01;
  d_s *ds = NULL, *dsi = NULL, *dsd = NULL;
  pltreg *pr = NULL;
  static int repeat = 1, max = 32, ex_size = 0;
  static float p = 13, sigm = 0.003;
  double chi2;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = win_scanf("Cut data set in continuous pieces excluding regions where\n"
		"the derivative, done after a non-linear filter, is stronger than %6f\n"
		"Exclude neighbor points j with j = %4d\n"
		"Do not keep stretch of consecutive points having less than %4d points\n"
		"Non-linear fit parameters:\n"
		"Number of times the filter is applied? %8d. \n"
		"Define the bounds on the # points used per fit w= (2m + 1):\n"
		"maxixum size for averaging m = %8d  \n"
		"Define the the sigma value of noise= %8f; \n"
		"and the weighting power p= %8f\n"
		"Exclude points with averaging smaller than %8d\n"
		"Proceed all data sets %b. Place result in a new plot%b\n"
		,&deltaz,&nei,&nmin,&repeat, &max, &sigm, &p, &ex_size, &all_ds,&new_plt);
  if (i == WIN_CANCEL)	return OFF;
  ids = op->cur_dat;
  if (new_plt)
    {
      i = op->n_dat;
      op->n_dat = 0; // we want to copy only the plot stuuff
      opn = duplicate_plot_data(op, NULL);
      if (opn == NULL) win_printf_OK("cannot create plot !");
      add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
      op->n_dat = i;
    }
  else opn = op;

  for (id = 0, n_dat = op->n_dat; id < n_dat; id++)
    {
      if (all_ds == 0 && id != ids) continue;
      dsi = op->dat[id];
      dsd = jumpfit_vc_2_exclude_edge(dsi, NULL, repeat, max, ex_size, sigm, p, &chi2);
      if (dsd == NULL) return win_printf_OK("cannot do non-linear filter !");
      dsd->ny = dsd->nx = (dsd->nx > 0) ? dsd->nx-1 : 0;
      //add_one_plot_data(op, IS_DATA_SET, (void*)dsd);
      for (i = 0; i < dsd->nx-1; i++)
	{  // we differanciate
	  dsd->yd[i] = dsd->yd[i+1] - dsd->yd[i];
	  for (j = 0; j < dsd->nx && j < dsi->nx; j++)
	    {
	      if (dsi->xd[j] <= dsd->xd[i]) itmp = j;
	      if (dsi->xd[j] <= dsd->xd[i+1]) itmp2 = j;
	    }
	  dsd->xd[i] = (float)((int)(itmp + itmp2/2));
	}
      dsd->ny = dsd->nx = (dsd->nx > 0) ? dsd->nx-1 : 0;
      for (i = 0; i < dsd->nx; i++)
	if (fabs(dsd->yd[i]) > deltaz) dsd->yd[i] = -FLT_MAX;
      for (i = 1; i < dsd->nx; i++)
	{
	  if ((dsd->yd[i-1] != -FLT_MAX) && (dsd->yd[i] == -FLT_MAX))
	    {
	      for (j = i - nei; j < i; j++)
		dsd->yd[(j<0)?0:j] = -FLT_MAX;
 	    }
 	}
      for (i = dsd->nx - 1; i > 0; i--)
 	{
           if ((dsd->yd[i-1] == -FLT_MAX) && (dsd->yd[i] != -FLT_MAX))
 	     {
 	       for (j = i; j < i+nei; j++)
		 dsd->yd[(j < dsd->nx) ? j :dsd->nx-1] = -FLT_MAX;
             }
        }
      for (i = dsd->nx - 1, j = 0; i >= 0; i--)
	{
	  if ((dsd->yd[i] != -FLT_MAX))      dsd->yd[i] = j++;
	  else j = 0;
	}
      for (i = 0; i < dsd->nx; i++)
	{
        if ((dsd->yd[i] != -FLT_MAX))
	  {
            j = dsd->yd[i];
	      if (j > nmin && j < dsi->nx)
		{
		  if ((ds = create_and_attach_one_ds(opn, j, j, 0)) == NULL)
		    return win_printf_OK("cannot create plot !");
		  inherit_from_ds_to_ds(ds, dsi);
		  for (k = 0; (k < j) && (k < dsi->nx); k++)
		    {
		      ki = i + k;//(int)(dsd->xd[i]) + k;
		      if (ki < 0 || ki >= dsi->nx) continue;
		      ds->yd[k] = dsi->yd[ki];
		      ds->xd[k] = dsi->xd[ki];
		    }
		}
	      if (j>0) i += j - 1;
	    }
	}
      for (i = dsd->nx - 1; i >= 0; i--)
	{
	  if ((dsd->yd[i] == -FLT_MAX))      dsd->yd[i] = -5;
	}

      free_data_set(dsd);
      dsd = NULL;
    }
  /* now we must do some house keeping */
  /* refisplay the entire plot */
  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}




d_s *cordrift_colapse_all_data_set_in_one(O_p *op)
{
  int i, j;
  int id, nf, n_dat, ki, itmp, ids;
  float tmp, minx = 0, maxx = 0;
  d_s *ds = NULL, *dsi = NULL, *dsd = NULL, *dsn = NULL;
  //int QuickSort_double(float *xd, float *yd, int l, int r);

  if (op == NULL) return NULL;

  dsd = build_data_set(op->n_dat,op->n_dat);
  if (dsd == NULL)
    {
      win_printf_ptr("cannot create dsd !");
      return NULL;
    }

  for (id = 0, n_dat = op->n_dat; id < n_dat; id++)
    {
      ds = op->dat[id];
      dsd->xd[id] = ds->nx;
      dsd->yd[id] = id;
      if (id == 0)
	{
	  minx = ds->xd[0];
	  maxx = ds->xd[ds->nx-1];
	}
      minx = (ds->xd[0] < minx) ? ds->xd[0] : minx;
      maxx = (ds->xd[ds->nx-1] > maxx) ? ds->xd[ds->nx-1] : maxx;
    }
  QuickSort_double(dsd->xd, dsd->yd, 0, dsd->nx - 1);
  nf = (int)(maxx - minx + 1.5);
  if ((ds = build_data_set(nf, nf)) == NULL)
    return win_printf_ptr("cannot create ds");
  if ((dsn = build_data_set(nf, nf)) == NULL)
    return win_printf_ptr("cannot create ds");
  for (i = 0; i < ds->nx; i++)
    {   // we prepare a ds to recieve the average
      ds->xd[i] = dsn->xd[i] = (int)minx + i;
      ds->yd[i] = dsn->yd[i] = 0;
    }

  id = (int)dsd->yd[dsd->nx - 1];
  if (id >= 0 || id < n_dat)  dsi = op->dat[id];
  //win_printf("longest ds %d len %d",id,dsi->nx);
  for (i = 0, ki = 0; i < dsi->nx; i++)
    {  // we copy first biggest dataset as reference
      for ( ; ki < ds->nx && ds->xd[ki] < dsi->xd[i]; ki++);
      if (ki >= 0 && ki < ds->nx)
	{
	  ds->yd[ki] = dsi->yd[i];
	  dsn->yd[ki] += 1;
	}
    }
  for (ids = dsd->nx-2; ids >= 0; ids--)
    {
      id = (int)dsd->yd[ids];
      if (id < 0 || id >= n_dat) continue;
      dsi = op->dat[id];

      for (j = ki = 0, tmp = 0, itmp = 0; j < dsi->nx; j++)
	{
	  for ( ; ki < ds->nx && ds->xd[ki] < dsi->xd[j]; ki++);
	  if (ki >= 0 && ki < ds->nx && dsn->yd[ki] > 0)
	    {
	      tmp += dsi->yd[j] - (ds->yd[ki]/dsn->yd[ki]);
	      itmp++;
	    }
	}
      tmp = (itmp > 0) ? tmp/itmp : tmp;
      for (j = 0; j < dsi->nx; j++)  dsi->yd[j] -= tmp;
      //win_printf("ds %d len %d shifted by %g",id,dsi->nx,tmp);
      set_ds_treatement(dsi, "Data shifted in Y by %g", tmp);
      for (i = 0, ki = 0; i < dsi->nx; i++)
	{  // we copy first biggest dataset as reference
	  for ( ; ki < ds->nx && ds->xd[ki] < dsi->xd[i]; ki++);
	  if (ki >= 0 && ki < ds->nx)
	    {
	      ds->yd[ki] += dsi->yd[i];
	      dsn->yd[ki] += 1;
	    }
	}
    }
  for (ki = 0; ki < ds->nx; ki++)
    ds->yd[ki] = (dsn->yd[ki] > 0) ? (ds->yd[ki]/dsn->yd[ki]) : ds->yd[ki];

  free_data_set(dsd);
  free_data_set(dsn);
  return ds;
}

d_s *cordrift_colapse_all_data_set_in_one_by_diff_histo(O_p *op, float sigma)
{
  int i, j, k;
  int id, nf, ki, ids;
  float minx = 0, maxx = 0, yavg, tmp, val;
  d_s *ds = NULL; // Final averaged ds
  d_s *dsi = NULL; // the input data set
  float **extend = NULL, **dist = NULL;


  if (op == NULL || op->n_dat <= 0) return NULL;

  extend = (float**)calloc(op->n_dat,sizeof(float*));
  if (extend == NULL) return NULL;
  dist = (float**)calloc(op->n_dat,sizeof(float*));
  if (dist == NULL) return NULL;
  for(i = 0; i < op->n_dat; i++)
    {
       extend[i] = (float*)calloc(op->n_dat,sizeof(float));
       if (extend[i] == NULL) return NULL;
       dist[i] = (float*)calloc(op->n_dat,sizeof(float));
       if (dist[i] == NULL) return NULL;
    }


  for (id = 0; id < op->n_dat; id++)
    {
      ds = op->dat[id];
      if (id == 0)
	{
	  minx = ds->xd[0];
	  maxx = ds->xd[ds->nx-1];
	}
      minx = (ds->xd[0] < minx) ? ds->xd[0] : minx;
      maxx = (ds->xd[ds->nx-1] > maxx) ? ds->xd[ds->nx-1] : maxx;
    }
  nf = (int)(maxx - minx + 1.5);
  if ((ds = build_data_set(nf, nf)) == NULL)
    return win_printf_ptr("cannot create ds");

  for(i = 0; i < op->n_dat; i++)
    {
       d_s *ds1, *ds2;
       ds1 = op->dat[i];
       for(j = 0; j < op->n_dat; j++)
         {
            if (i == j) continue;
            ds2 = op->dat[j];
            ds->nx = ds->ny = 0;
            for (k = 0, ki = 0; k < ds1->nx; k++)
               {
                 for ( ; ki < ds2->nx && ds2->xd[ki] < ds1->xd[k]; ki++);
                 if (ki >= 0 && ki < ds2->nx && (fabs(ds2->xd[ki] - ds1->xd[k]) < 0.5))
                    {
                        add_new_point_to_ds(ds, ds->nx, ds1->yd[k] - ds2->yd[ki]);
                    }
               }
            find_max_of_histo_of_ds_with_gaussian_convolution(ds, sigma, &yavg, &val);
            //if (j == i + 1)
            //win_printf("ds1 %d with %d pts; ds2 %d with %d pts-> diff %d pts\nDist %g extend %g",i,ds1->nx,j,ds2->nx,ds->nx,yavg,val);
            dist[i][j] = yavg;
            extend[i][j] = val;
         }
    }
  free_data_set(ds);
  for(i = 0, ki = 0, maxx = 0; i < op->n_dat; i++)
    {
       for(j = 0, tmp = 0; j < op->n_dat; j++)
         {
            if (i == j) continue;
            tmp += extend[i][j];
         }
       if (i == 0 || tmp > maxx)
         {
            maxx = tmp;
            ki = i;
         }
    }
  //win_printf("Over %d ds, ds %d has the maximum correlation %g",op->n_dat,ki,maxx/(op->n_dat -1));
  ids = ki;
  for(i = 0; i < op->n_dat; i++)
    {
       if (i == ids) continue;
       dsi = op->dat[i];
       for(j = 0; j < dsi->nx; j++)
         {
            dsi->yd[j] -= dist[i][ids];
         }
    }
  ds = average_all_ds_of_op_by_max_of_histo(op, sigma, 0, 0);
  for(i = 0; i < op->n_dat; i++)
    {
       free(extend[i]);
       free(dist[i]);
    }
  free(extend);
  free(dist);
  return ds;
}

d_s *cordrift_colapse_all_data_set_in_one_by_diff_histo_old(O_p *op, float sigma)
{
  int i, j, k;
  int id, nf, n_dat, ki, ids;
  float minx = 0, maxx = 0, yavg, tmp;
  d_s *ds = NULL; // Final averaged ds
  d_s *dsi = NULL; // the input data set
  d_s *dsd = NULL; // a dataset used to order all the ds of the op
  d_s *dsn = NULL; // a dataset used to hold the number of points
  d_s *dsdif = NULL; // a dataset corresponding to the difference between dsi and the ref ds
  d_s *dsd2 = NULL; // a dataset used to hold pts of all the ds of the op having same x
  //int QuickSort_double(float *xd, float *yd, int l, int r);

  if (op == NULL) return NULL;

  dsd = build_data_set(op->n_dat,op->n_dat);
  dsd2 = build_data_set(op->n_dat,op->n_dat);
  if (dsd == NULL || dsd2 == NULL)
    {  // we build a data set to sort the best reference one
      win_printf_ptr("cannot create dsd !");
      return NULL;
    }

  for (id = 0, n_dat = op->n_dat; id < n_dat; id++)
    {
      ds = op->dat[id];
      for(i = 0, tmp = 0; i < ds->nx - 1; i++)
         tmp += (ds->yd[i+1] - ds->yd[i]) * (ds->yd[i+1] - ds->yd[i]);
      tmp /= (ds->nx > 1) ? ds->nx - 1 : 1;
      tmp = (tmp > 0) ? (4 * sigma * sigma)/tmp : ds->nx;
      // we select a ds with a maximum of points but small fluctuations
      dsd->xd[id] = ds->nx + tmp;
      dsd->yd[id] = id;
      if (id == 0)
	{
	  minx = ds->xd[0];
	  maxx = ds->xd[ds->nx-1];
	}
      minx = (ds->xd[0] < minx) ? ds->xd[0] : minx;
      maxx = (ds->xd[ds->nx-1] > maxx) ? ds->xd[ds->nx-1] : maxx;
    }
  QuickSort_double(dsd->xd, dsd->yd, 0, dsd->nx - 1);
  nf = (int)(maxx - minx + 1.5);
  if ((ds = build_data_set(nf, nf)) == NULL)
    return win_printf_ptr("cannot create ds");
  if ((dsn = build_data_set(nf, nf)) == NULL)
    return win_printf_ptr("cannot create ds");
  if ((dsdif = build_data_set(nf, nf)) == NULL)
    return win_printf_ptr("cannot create ds");
  for (i = 0; i < ds->nx; i++)
    {   // we prepare a ds to recieve the average
      ds->xd[i] = dsn->xd[i] = dsdif->xd[i] = (int)minx + i;
      ds->yd[i] = dsn->yd[i] = dsdif->yd[i] = 0;
    }

  id = (int)dsd->yd[dsd->nx - 1]; // index of biggest ds
  if (id >= 0 || id < n_dat)  dsi = op->dat[id];
  //win_printf("longest ds %d len %d",id,dsi->nx);
  for (i = 0, ki = 0; i < dsi->nx; i++)
    {  // we copy first biggest dataset as reference
      for ( ; ki < ds->nx && ds->xd[ki] < dsi->xd[i]; ki++);
      if (ki >= 0 && ki < ds->nx)
	{
	  ds->yd[ki] = dsi->yd[i];
	  dsn->yd[ki] += 1;
	}
    }
  for (ids = dsd->nx-2; ids >= 0; ids--)
    {
      id = (int)dsd->yd[ids];
      if (id < 0 || id >= n_dat) continue;
      dsi = op->dat[id];

      for (j = ki = 0; j < dsi->nx; j++)
	{
	  for ( ; ki < ds->nx && ds->xd[ki] < dsi->xd[j]; ki++);
	  if (ki >= 0 && ki < ds->nx && dsn->yd[ki] > 0)
	    {
              dsdif->yd[j] = dsi->yd[j] - (ds->yd[ki]/dsn->yd[ki]);
	    }
	}
      dsdif->nx = dsdif->ny = dsi->nx;
      find_max_of_histo_of_ds_with_gaussian_convolution(dsdif, sigma, &yavg, NULL);
      for (j = 0; j < dsi->nx; j++)  dsi->yd[j] -= yavg;
      //win_printf("ds %d len %d shifted by %g",id,dsi->nx,tmp);
      set_ds_treatement(dsi, "Data shifted in Y by %g over %d datasets histo over %g", yavg,op->n_dat,sigma);
      for (i = 0, ki = 0; i < dsi->nx; i++)
	{  // we copy first biggest dataset as reference
	  for ( ; ki < ds->nx && ds->xd[ki] < dsi->xd[i]; ki++);
          tmp = (dsn->yd[ki] > 0) ? dsi->yd[i] - (ds->yd[ki]/dsn->yd[ki]) : 0;
          if (ki >= 0 && ki < ds->nx && (fabs(tmp) < 3*sigma))
	    {
	      ds->yd[ki] += dsi->yd[i];
	      dsn->yd[ki] += 1;
	    }
          else if (ids == dsd->nx-2)
	    {
              ds->yd[ki] = 0;
	      dsn->yd[ki] = 0;
	    }
	}
    }
  for (ki = 0; ki < ds->nx; ki++)
    ds->yd[ki] = (dsn->yd[ki] > 0) ? (ds->yd[ki]/dsn->yd[ki]) : ds->yd[ki];

  for (i = 0; i < ds->nx; i++)
    {
       for (ids = 0, k = 0; ids < op->n_dat; ids++)
         {
            dsi = op->dat[ids];
            for (ki = 0 ; ki < dsi->nx && dsi->xd[ki] < ds->xd[i]; ki++);
            if (dsi->xd[ki] == ds->xd[i])
              {
                 dsd2->yd[k] = dsi->yd[ki];
                 dsd2->xd[k++] = ds->xd[i];
              }
	}
      dsd2->nx = dsd2->ny = k;
      if (find_max_of_histo_of_ds_with_gaussian_convolution(dsd2, sigma, &yavg, NULL) == 0)
            ds->yd[i] = yavg;
    }

  free_data_set(dsd);
  free_data_set(dsd2);
  free_data_set(dsn);
  free_data_set(dsdif);
  return ds;
}




O_p *cordrift_colapse_data_set(O_p *op, int add_avgn, int multi_ds, int col_in_new_plt)
{
  int i, j;
  O_p  *opc = NULL;
  int id, nf, n_dat, ki, itmp, ids, start, end;
  float tmp, minx = 0, maxx = 0;
  d_s *ds = NULL, *dsi = NULL, *dsd = NULL, *dsn = NULL, *dsf = NULL;
  //int QuickSort_double(float *xd, float *yd, int l, int r);

  if (op == NULL) return NULL;

  dsd = build_data_set(op->n_dat,op->n_dat);
  if (dsd == NULL)
    {
      win_printf_OK("cannot create dsd !");
      return NULL;
    }

  for (id = 0, n_dat = op->n_dat; id < n_dat; id++)
    {
      ds = op->dat[id];
      dsd->xd[id] = ds->nx;
      dsd->yd[id] = id;
      if (id == 0)
	{
	  minx = ds->xd[0];
	  maxx = ds->xd[ds->nx-1];
	}
      minx = (ds->xd[0] < minx) ? ds->xd[0] : minx;
      maxx = (ds->xd[ds->nx-1] > maxx) ? ds->xd[ds->nx-1] : maxx;
    }
  QuickSort_double(dsd->xd, dsd->yd, 0, dsd->nx - 1);
  nf = (int)(maxx - minx + 1.5);
  if ((ds = build_data_set(nf, nf)) == NULL)
    return win_printf_ptr("cannot create ds");
  if (add_avgn)
    {
      if ((dsn = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	return win_printf_ptr("cannot create ds");
    }
  else
    {
      if ((dsn = build_data_set(nf, nf)) == NULL)
	return win_printf_ptr("cannot create ds");
    }
  for (i = 0; i < ds->nx; i++)
    {   // we prepare a ds to recieve the average
      ds->xd[i] = dsn->xd[i] = (int)minx + i;
      ds->yd[i] = dsn->yd[i] = 0;
    }

  id = (int)dsd->yd[dsd->nx - 1];
  if (id >= 0 || id < n_dat)  dsi = op->dat[id];
  //win_printf("longest ds %d len %d",id,dsi->nx);
  for (i = 0, ki = 0; i < dsi->nx; i++)
    {  // we copy first biggest dataset as reference
      for ( ; ki < ds->nx && ds->xd[ki] < dsi->xd[i]; ki++);
      if (ki >= 0 && ki < ds->nx)
	{
	  ds->yd[ki] = dsi->yd[i];
	  dsn->yd[ki] += 1;
	}
    }
  for (ids = dsd->nx-2; ids >= 0; ids--)
    {
      id = (int)dsd->yd[ids];
      if (id < 0 || id >= n_dat) continue;
      dsi = op->dat[id];

      for (j = ki = 0, tmp = 0, itmp = 0; j < dsi->nx; j++)
	{
	  for ( ; ki < ds->nx && ds->xd[ki] < dsi->xd[j]; ki++);
	  if (ki >= 0 && ki < ds->nx && dsn->yd[ki] > 0)
	    {
	      tmp += dsi->yd[j] - (ds->yd[ki]/dsn->yd[ki]);
	      itmp++;
	    }
	}
      tmp = (itmp > 0) ? tmp/itmp : tmp;
      for (j = 0; j < dsi->nx; j++)  dsi->yd[j] -= tmp;
      //win_printf("ds %d len %d shifted by %g",id,dsi->nx,tmp);
      set_ds_treatement(dsi, "Data shifted in Y by %g", tmp);
      for (i = 0, ki = 0; i < dsi->nx; i++)
	{  // we copy first biggest dataset as reference
	  for ( ; ki < ds->nx && ds->xd[ki] < dsi->xd[i]; ki++);
	  if (ki >= 0 && ki < ds->nx)
	    {
	      ds->yd[ki] += dsi->yd[i];
	      dsn->yd[ki] += 1;
	    }
	}
    }
  for (ki = 0; ki < ds->nx; ki++)
    ds->yd[ki] = (dsn->yd[ki] > 0) ? (ds->yd[ki]/dsn->yd[ki]) : ds->yd[ki];

  if (multi_ds == 1)
    {
      for(i = 0, start = end = -1; i < dsn->nx; i++)
	{
	  if (start < 0 && dsn->yd[i] > 0) end = start = i;
	  else if (dsn->yd[i] > 0) end = i;
	  if ((end > start) && ((dsn->yd[i] == 0) || (i == dsn->nx - 1)))
	    {
	      end = i;
	      if (end-start > 0)
		{
		  if (col_in_new_plt == 0)
		    {
		      if ((dsf = create_and_attach_one_ds(op, end-start, end-start, 0)) == NULL)
			return win_printf_ptr("cannot create ds");
		    }
		  else
		    {
		      if (opc == NULL)
			{
			  if ((opc = create_one_plot(end-start, end-start, 0)) == NULL)
			    return win_printf_ptr("cannot create ds");
			  dsf = opc->dat[0];
			  uns_op_2_op(opc, op);
			  if (op->y_title != NULL) set_plot_y_title(opc, "%s", op->y_title);
			  if (op->x_title != NULL) set_plot_x_title(opc, "%s", op->x_title);
			}
		      else
			{
			  if ((dsf = create_and_attach_one_ds(opc, end-start, end-start, 0)) == NULL)
			    return win_printf_ptr("cannot create ds");
			}

		    }
		  set_ds_source(dsn, "Colapse signal of points [%d,%d[", start, end);
		  for (ki = 0; ki < dsf->nx && start+ki < ds->nx; ki++)
		    {
		      dsf->xd[ki] = ds->xd[start+ki];
		      dsf->yd[ki] = ds->yd[start+ki];
		    }
		}
	      end = start = -1;
	    }
	}
      free_data_set(ds);
    }
  else 	add_one_plot_data(op, IS_DATA_SET, (void*)ds);
  free_data_set(dsd);
  if (add_avgn == 0)   free_data_set(dsn);
  return opc;
}


int do_cordrift_colapse_data_set_single_ds(void)
{
  int i;
  O_p *opi = NULL;
  d_s *ds = NULL, *dsi = NULL;
  pltreg *pr = NULL;
  //int QuickSort_double(float *xd, float *yd, int l, int r);

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  //we assume data in xd on integer frame numbers
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opi,&dsi) != 3)
    return win_printf_OK("cannot find data");

  i = win_printf("This routine colapse along y datasets to gerenetate an average\n"
		 "in a single dataset \n");
  if (i == WIN_CANCEL)    return D_O_K;

  ds = cordrift_colapse_all_data_set_in_one(opi);
  if (ds == NULL) win_printf_OK("cannot create ds !");

  add_one_plot_data(opi, IS_DATA_SET, (void*)ds);
  refresh_plot(pr, pr->n_op-1);
  return D_O_K;
}
int do_cordrift_colapse_data_set_single_ds_by_histo(void)
{
  int i;
  O_p *opi = NULL;
  d_s *ds = NULL, *dsi = NULL;
  pltreg *pr = NULL;
  static float sigma = 0.005;
  //int QuickSort_double(float *xd, float *yd, int l, int r);

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  //we assume data in xd on integer frame numbers
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opi,&dsi) != 3)
    return win_printf_OK("cannot find data");

  i = win_scanf("This routine colapse along y datasets to gerenetate an average\n"
                "in a single dataset using an histo of %4f \n",&sigma);
  if (i == WIN_CANCEL)    return D_O_K;

  ds = cordrift_colapse_all_data_set_in_one_by_diff_histo(opi,sigma);
  if (ds == NULL) win_printf_OK("cannot create ds !");

  add_one_plot_data(opi, IS_DATA_SET, (void*)ds);
  refresh_plot(pr, UNCHANGED);//pr->n_op-1);
  return D_O_K;
}


int do_cordrift_colapse_data_set(void)
{
  int i, j;
  O_p *op = NULL, *opi = NULL, *opc = NULL;
  int id, nf, n_dat, ki, itmp, ids, start, end;
  float tmp, minx = 0, maxx = 0;
  d_s *ds = NULL, *dsi = NULL, *dsd = NULL, *dsn = NULL, *dsf = NULL;
  pltreg *pr = NULL;
  static int new_plt = 0, add_avgn = 0, multi_ds = 0, col_in_new_plt = 0;
  //int QuickSort_double(float *xd, float *yd, int l, int r);

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine cuts datasets keeping only flat part");

  //we assume data in xd on integer frame numbers
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opi,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */

  i = win_scanf("This routine colapse along y datasets to gerenetate an average\n"
		"Do you want to operate on this plot%R or on a copy%r\n"
		"Do you want the average data \n%R->in a sinle dataset \n%r->in one datatets per bunch of points\n"
		"Place the colapsed result in a new plot %b\n"
		"Click here %b to add a datasets representing the number of average per point\n"
		,&new_plt,&multi_ds,&col_in_new_plt,&add_avgn);
  if (i == WIN_CANCEL)    return D_O_K;

  if (new_plt)
    {
       op = duplicate_plot_data(opi, NULL);
       if (op == NULL) win_printf_OK("cannot create plot !");
       add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)op);
    }
  else op = opi;

  dsd = build_data_set(op->n_dat,op->n_dat);
  if (dsd == NULL) win_printf_OK("cannot create dsd !");

  for (id = 0, n_dat = op->n_dat; id < n_dat; id++)
    {
      ds = op->dat[id];
      dsd->xd[id] = ds->nx;
      dsd->yd[id] = id;
      if (id == 0)
	{
	  minx = ds->xd[0];
	  maxx = ds->xd[ds->nx-1];
	}
      minx = (ds->xd[0] < minx) ? ds->xd[0] : minx;
      maxx = (ds->xd[ds->nx-1] > maxx) ? ds->xd[ds->nx-1] : maxx;
    }
  QuickSort_double(dsd->xd, dsd->yd, 0, dsd->nx - 1);
  nf = (int)(maxx - minx + 0.5);
  if ((ds = build_data_set(nf, nf)) == NULL)
    return win_printf_OK("cannot create ds");
  if (add_avgn)
    {
      if ((dsn = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
	return win_printf_OK("cannot create ds");
    }
  else
    {
      if ((dsn = build_data_set(nf, nf)) == NULL)
	return win_printf_OK("cannot create ds");
    }
  for (i = 0; i < ds->nx; i++)
    {   // we prepare a ds to recieve the average
      ds->xd[i] = dsn->xd[i] = (int)minx + i;
      ds->yd[i] = dsn->yd[i] = 0;
    }

  id = (int)dsd->yd[dsd->nx - 1];
  if (id >= 0 || id < n_dat)  dsi = op->dat[id];
  //win_printf("longest ds %d len %d",id,dsi->nx);
  for (i = 0, ki = 0; i < dsi->nx; i++)
    {  // we copy first biggest dataset as reference
      for ( ; ki < ds->nx && ds->xd[ki] < dsi->xd[i]; ki++);
      if (ki >= 0 && ki < ds->nx)
	{
	  ds->yd[ki] = dsi->yd[i];
	  dsn->yd[ki] += 1;
	}
    }
  for (ids = dsd->nx-2; ids >= 0; ids--)
    {
      id = (int)dsd->yd[ids];
      if (id < 0 || id >= n_dat) continue;
      dsi = op->dat[id];

      for (j = ki = 0, tmp = 0, itmp = 0; j < dsi->nx; j++)
	{
	  for ( ; ki < ds->nx && ds->xd[ki] < dsi->xd[j]; ki++);
	  if (ki >= 0 && ki < ds->nx && dsn->yd[ki] > 0)
	    {
	      tmp += dsi->yd[j] - (ds->yd[ki]/dsn->yd[ki]);
	      itmp++;
	    }
	}
      tmp = (itmp > 0) ? tmp/itmp : tmp;
      for (j = 0; j < dsi->nx; j++)  dsi->yd[j] -= tmp;
      //win_printf("ds %d len %d shifted by %g",id,dsi->nx,tmp);
      set_ds_treatement(dsi, "Data shifted in Y by %g", tmp);
      for (i = 0, ki = 0; i < dsi->nx; i++)
	{  // we copy first biggest dataset as reference
	  for ( ; ki < ds->nx && ds->xd[ki] < dsi->xd[i]; ki++);
	  if (ki >= 0 && ki < ds->nx)
	    {
	      ds->yd[ki] += dsi->yd[i];
	      dsn->yd[ki] += 1;
	    }
	}
    }
  for (ki = 0; ki < ds->nx; ki++)
    ds->yd[ki] = (dsn->yd[ki] > 0) ? (ds->yd[ki]/dsn->yd[ki]) : ds->yd[ki];

  if (multi_ds == 1)
    {
      for(i = 0, start = end = -1; i < dsn->nx; i++)
	{
	  if (start < 0 && dsn->yd[i] > 0) end = start = i;
	  else if (dsn->yd[i] > 0) end = i;
	  if ((end > start) && ((dsn->yd[i] == 0) || (i == dsn->nx - 1)))
	    {
	      end = i;
	      if (end-start > 0)
		{
		  if (col_in_new_plt == 0)
		    {
		      if ((dsf = create_and_attach_one_ds(op, end-start, end-start, 0)) == NULL)
			return win_printf_OK("cannot create ds");
		    }
		  else
		    {
		      if (opc == NULL)
			{
			  if ((opc = create_and_attach_one_plot(pr, end-start, end-start, 0)) == NULL)
			    return win_printf_OK("cannot create ds");
			  dsf = opc->dat[0];
			  uns_op_2_op(opc, op);
			  if (op->y_title != NULL) set_plot_y_title(opc, "%s", op->y_title);
			  if (op->x_title != NULL) set_plot_x_title(opc, "%s", op->x_title);
			}
		      else
			{
			  if ((dsf = create_and_attach_one_ds(opc, end-start, end-start, 0)) == NULL)
			    return win_printf_OK("cannot create ds");
			}

		    }
		  set_ds_source(dsn, "Colapse signal of points [%d,%d[", start, end);
		  for (ki = 0; ki < dsf->nx && start+ki < ds->nx; ki++)
		    {
		      dsf->xd[ki] = ds->xd[start+ki];
		      dsf->yd[ki] = ds->yd[start+ki];
		    }
		}
	      end = start = -1;
	    }
	}
      free_data_set(ds);
    }
  else 	add_one_plot_data(op, IS_DATA_SET, (void*)ds);
  free_data_set(dsd);
  if (add_avgn == 0)   free_data_set(dsn);
  /* refisplay the entire plot */
  refresh_plot(pr, pr->n_op-1);
  return D_O_K;

}
int do_cordrift_rescale_plot(void)
{
  int i, j;
  O_p *op = NULL, *opn = NULL;
  int nf;
  static float factor = 1;
  d_s *ds = NULL, *dsi = NULL;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == WIN_CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  set_plot_title(opn, "Multiply by %f",factor);
  if (op->x_title != NULL) set_plot_x_title(opn, "%s", op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, "%s", op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int keep_significant_rate(void)
{
  O_p *op = NULL, *opn = NULL, *opsig = NULL, *opnsig = NULL;
  d_s *dsi = NULL, *dst = NULL, *dso = NULL, *dsisig = NULL, *dsosig = NULL;
  pltreg *pr = NULL;
  int i;
  static float thres = 40, rmax = 200;
  static int size = 256, disp = 0, opsign = -1;
  static float ratio = 0.5;
  static int type = 0;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])    return win_printf_OK("This routine filter data ");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");


  i = win_scanf("Keep helicase rate only if greater than some level\n"
		"Replacing points not matching by zero (copy to save first)\n"
		"Rate range [%4f,%4f[\n"
		"Size of the slidding window %5d"
		"Minimum ratio of rates exceeding the threshold in the window %4f [0,1]\n"
		"%R rate above %r below the threshold\n"
		"%b Display selection criterium only (no data)\n"
		"Treat also helicase signal place in plot %4d (<0 skip)\n"
		,&thres,&rmax,&size,&ratio,&type,&disp,&opsign);
  if (i == WIN_CANCEL) return D_O_K;
  opsig = (opsign < 0 || opsign >= pr->n_op) ? NULL : pr->o_p[opsign];

  int ids, k, j, start, end;
  float tmp;
  if (opsig != NULL && opsig->n_dat != op->n_dat)
    win_printf("Signal %d and rate %d have different nb. of datasets !"
	       ,opsig->n_dat, op->n_dat);
  for (i = 0, k = 0; opsig != NULL && i < op->n_dat && i < opsig->n_dat; i++)
    {
      k += (op->dat[i]->nx !=  opsig->dat[i]->nx) ? 1 : 0;
      if (k == 1)
	win_printf("Dataset %d Signal has %d pts and rate has %d pts!"
		   ,i,op->dat[i]->nx, opsig->dat[i]->nx);
    }
  if (k > 1)
    win_printf("%d Datasets have not the same nb of points !",k);

  opn = create_one_empty_plot();
  if (opn == NULL) return win_printf_OK("cannot create plot");
  uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_X_UNIT_SET);
  set_plot_title(opn,"Rate data selected %s [%g, %g[ over %d with r = %g",(type)?"out":"in",thres,rmax,size,ratio);

  if (disp == 0)
    {
      uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opn, IS_Y_UNIT_SET);
      if (opsig != NULL)
	{
	  opnsig = create_one_empty_plot();
	  if (opnsig == NULL) return win_printf_OK("cannot create plot");
	  uns_op_2_op_by_type(opsig, IS_X_UNIT_SET, opnsig, IS_X_UNIT_SET);
	  uns_op_2_op_by_type(opsig, IS_Y_UNIT_SET, opnsig, IS_Y_UNIT_SET);
	  set_plot_title(opnsig,"Signal data selected %s [%g, %g[ over %d with r = %g",(type)?"out":"in",thres,rmax,size,ratio);
	}
    }

  for (ids = 0; ids < op->n_dat; ids++)
    {
      dsi = op->dat[ids];
      dst = build_data_set(dsi->nx,dsi->nx);
      dsisig = (opsig != NULL && ids < opsig->n_dat) ? opsig->dat[ids] : NULL;
      for (j = 0; j < dsi->nx; j++)
	{
	  tmp = op->ay + op->dy * dsi->yd[j];
	  if (type == 0)
	    dst->xd[j] = (tmp >= thres && tmp < rmax) ? 1 : 0;
	  else if (type == 1)
	    dst->xd[j] = (tmp < thres || tmp >= rmax) ? 1 : 0;
	}
      for (j = 0; j < dsi->nx - size; j++)
	{
	  for(k = 0; k < size && j+k < dsi->nx; k++)
	    dst->yd[j+size/2] += dst->xd[j+k];
	}
      if (disp)
	{
	  for(k = 0; k < dsi->nx; k++)
	    dst->xd[k] = dsi->xd[k];
	  if (add_one_plot_data(opn, IS_DATA_SET, (void*)dst))
	    return win_printf_OK("cannot add data");
	}
      else
	{
	  for (j = 0, start = end = -1; j < dsi->nx; j++)
	    {
	      if (start < 0)
		start = (dst->yd[j] > (int)(ratio*size)) ? j : start;
	      else if (end < 0)
		end = (dst->yd[j] <= (int)(ratio*size)) ? j : end;
	      else if (start >= 0 && end >= 0)
		{
		  if (end - start > 1)
		    {
		      dso = create_and_attach_one_ds(opn, end - start, end - start, 0);
		      if (dso == NULL) return win_printf_OK("cannot create dsl");
		      inherit_from_ds_to_ds(dso, dsi);
		      set_ds_treatement(dso,"data keep rate %s [%g,%g[ at %d and %d"
				    ,(type)?"out":"in",thres,rmax,start,end);
		      for (k = 0; k < dso->nx && start+k < dsi->nx; k++)
			{
			  dso->xd[k] = dsi->xd[start+k];
			  dso->yd[k] = dsi->yd[start+k];
			}
		      if (opsig != NULL && dsisig != NULL && end < dsisig->nx)
			{
			  dsosig = create_and_attach_one_ds(opnsig, end - start, end - start, 0);
			  if (dsosig == NULL) return win_printf_OK("cannot create dsl");
			  inherit_from_ds_to_ds(dsosig, dsisig);
			  set_ds_treatement(dsosig,"data keep rate %s [%g,%g[ at %d and %d"
					,(type)?"out":"in",thres,rmax,start,end);
			  for (k = 0; k < dsosig->nx && start+k < dsisig->nx; k++)
			    {
			      dsosig->xd[k] = dsisig->xd[start+k];
			      dsosig->yd[k] = dsisig->yd[start+k];
			    }
			}
		    }
		  end = start = -1;
		}
	    }
	}
    }
  if (opnsig != NULL && opnsig->n_dat > 0)
    {
      add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opnsig);
    }

  if (opn->n_dat > 0)
    {
      add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
      return refresh_plot(pr, pr->n_op-1);
    }
  else
    {
      free_one_plot(opn);
      win_printf("No data added");
      return refresh_plot(pr, UNCHANGED);
    }
}

int keep_significant_strand_switch(void)
{
  O_p *op = NULL, *opn = NULL, *opsig = NULL, *opnsig = NULL;
  d_s *dsi = NULL, *dst = NULL, *dso = NULL, *dsisig = NULL, *dsosig = NULL;
  pltreg *pr = NULL;
  int i;
  static float thres = 40, rmax = 200;
  static int size = 256, disp = 0, opsign = -1;
  static float ratio = 0.5;
  static int type = 0;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])    return win_printf_OK("This routine filter data ");
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");


  i = win_scanf("Keep helicase strand switch if |rate| greater than some level\n"
		"And a rise edge is followed by a falling edge\n"
		"Replacing points not matching by zero (copy to save first)\n"
		"Rate range [%4f,%4f[\n"
		"Half Size of the slidding window %5d"
		"Minimum ratio of rates exceeding the threshold in the window %4f [0,1]\n"
		"%b Display selection criterium only (no data)\n"
		"Treat also helicase signal place in plot %4d (<0 skip)\n"
		,&thres,&rmax,&size,&ratio,&disp,&opsign);
  if (i == WIN_CANCEL) return D_O_K;
  opsig = (opsign < 0 || opsign >= pr->n_op) ? NULL : pr->o_p[opsign];

  int ids, k, j, start, end;
  float tmp;
  if (opsig != NULL && opsig->n_dat != op->n_dat)
    win_printf("Signal %d and rate %d have different nb. of datasets !"
	       ,opsig->n_dat, op->n_dat);
  for (i = 0, k = 0; opsig != NULL && i < op->n_dat && i < opsig->n_dat; i++)
    {
      k += (op->dat[i]->nx !=  opsig->dat[i]->nx) ? 1 : 0;
      if (k == 1)
	win_printf("Dataset %d Signal has %d pts and rate has %d pts!"
		   ,i,op->dat[i]->nx, opsig->dat[i]->nx);
    }
  if (k > 1)
    win_printf("%d Datasets have not the same nb of points !",k);

  opn = create_one_empty_plot();
  if (opn == NULL) return win_printf_OK("cannot create plot");
  uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_X_UNIT_SET);
  set_plot_title(opn,"Rate data selected %s [%g, %g[ over %d with r = %g",(type)?"out":"in",thres,rmax,size,ratio);

  if (disp == 0)
    {
      uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opn, IS_Y_UNIT_SET);
      if (opsig != NULL)
	{
	  opnsig = create_one_empty_plot();
	  if (opnsig == NULL) return win_printf_OK("cannot create plot");
	  uns_op_2_op_by_type(opsig, IS_X_UNIT_SET, opnsig, IS_X_UNIT_SET);
	  uns_op_2_op_by_type(opsig, IS_Y_UNIT_SET, opnsig, IS_Y_UNIT_SET);
	  set_plot_title(opsig,"Signal data selected %s [%g, %g[ over %d with r = %g",(type)?"out":"in",thres,rmax,size,ratio);
	}
    }

  for (ids = 0; ids < op->n_dat; ids++)
    {
      dsi = op->dat[ids];
      dst = build_data_set(dsi->nx,dsi->nx);
      dsisig = (opsig != NULL && ids < opsig->n_dat) ? opsig->dat[ids] : NULL;
      for (j = 0; j < dsi->nx; j++)
	{
	  tmp = op->ay + op->dy * dsi->yd[j];
	  if (tmp >= thres && tmp < rmax)     dst->xd[j] = 1;
	  else if (-tmp >= thres && -tmp < rmax)     dst->xd[j] = -1;
	  else dst->xd[j] = 0;
	}
      for (j = size; j < dsi->nx - size; j++)
	{
	  for(k = -size; k < size && j+k < dsi->nx; k++)
	    dst->yd[j] += (k < 0) ? dst->xd[j+k] : -dst->xd[j+k];
	}
      if (disp)
	{
	  for(k = 0; k < dsi->nx; k++)
	    dst->xd[k] = dsi->xd[k];
	  if (add_one_plot_data(opn, IS_DATA_SET, (void*)dst))
	    return win_printf_OK("cannot add data");
	}
      else
	{
	  for (j = 0, start = end = -1; j < dsi->nx; j++)
	    {
	      if (start < 0)
		start = (dst->yd[j] > (int)(ratio*size)) ? j : start;
	      else if (end < 0)
		end = (dst->yd[j] <= (int)(ratio*size)) ? j : end;
	      else if (start >= 0 && end >= 0)
		{
		  if (end - start > 1)
		    {
		      dso = create_and_attach_one_ds(opn, end - start, end - start, 0);
		      if (dso == NULL) return win_printf_OK("cannot create dsl");
		      inherit_from_ds_to_ds(dso, dsi);
		      set_ds_treatement(dso,"data keep  strand switch rate %s [%g,%g[ at %d and %d"
				    ,(type)?"out":"in",thres,rmax,start,end);
		      for (k = 0; k < dso->nx && start+k < dsi->nx; k++)
			{
			  dso->xd[k] = dsi->xd[start+k];
			  dso->yd[k] = dsi->yd[start+k];
			}
		      if (opsig != NULL && dsisig != NULL && end < dsisig->nx)
			{
			  dsosig = create_and_attach_one_ds(opnsig, end - start, end - start, 0);
			  if (dsosig == NULL) return win_printf_OK("cannot create dsl");
			  inherit_from_ds_to_ds(dsosig, dsisig);
			  set_ds_treatement(dsosig,"data keep strand switch rate %s [%g,%g[ at %d and %d"
					,(type)?"out":"in",thres,rmax,start,end);
			  for (k = 0; k < dsosig->nx && start+k < dsisig->nx; k++)
			    {
			      dsosig->xd[k] = dsisig->xd[start+k];
			      dsosig->yd[k] = dsisig->yd[start+k];
			    }
			}
		    }
		  end = start = -1;
		}
	    }
	}
    }
  if (opnsig != NULL && opnsig->n_dat > 0)
    {
      add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opnsig);
    }

  if (opn->n_dat > 0)
    {
      add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
      return refresh_plot(pr, pr->n_op-1);
    }
  else
    {
      free_one_plot(opn);
      win_printf("No data added");
      return refresh_plot(pr, UNCHANGED);
    }
}



d_s *average_over_smooth_window(d_s *dsi, int n_out)
{
    register int i, j, k;
    float w, w_2, x, tmp, co;
    d_s *dso = NULL;

    if (dsi == NULL) return NULL;
    if (n_out < 2 || dsi->nx < 2 || n_out >= dsi->nx) return NULL;
    dso = build_data_set(n_out,n_out);
    if (dso == NULL) return NULL;
    w = ((float)(dsi->nx-1))/n_out;
    w_2 = w/2;
    for(i = 0; i < dsi->nx; i++)
      {
	j = (int)((float)i)/w;
	x = w_2 + j * w;
	if (i < w_2)
	  {
	    dso->yd[0] += dsi->yd[i];
	    dso->xd[0] += 1;
	  }
	else if (i >= dsi->nx - w_2)
	  {
	    dso->yd[dso->nx-1] += dsi->yd[i];
	    dso->xd[dso->nx-1] += 1;
	  }
	else
	  {
	    k = ((float)i >= x) ? j+1 : j - 1;
	    k = (k < 0) ? 0 : k;
	    k = (k < dso->nx) ? k : dso->nx-1;
	    co = (cos(M_PI*(x - i)/w))/2;
	    tmp = 0.5 + co;
	    dso->yd[j] += tmp*dsi->yd[i];
	    dso->xd[j] += tmp;
	    tmp = 1 - tmp;
	    dso->yd[k] += tmp*dsi->yd[i];
	    dso->xd[k] += tmp;
	  }
      }
    for(i = 0; i < dso->nx; i++)
      {
	dso->yd[i] /= (dso->xd[i] > 0) ? dso->xd[i] : 1;
	dso->xd[i] = w_2 + (i * w);
      }
    return dso;
}

int do_average_over_smooth_window(void)
{
    O_p *op = NULL;
    d_s *dsi = NULL, *dso = NULL;
    pltreg *pr = NULL;
    int i;
    static int n_out = 256;

    if(updating_menu_state != 0)	return D_O_K;

    if (key[KEY_LSHIFT])    return win_printf_OK("This routine filter data ");
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
      return win_printf_OK("cannot find data");

    n_out = dsi->nx /4;
    i = win_scanf("How many final points %5d\n",&n_out);
    if (i == WIN_CANCEL)	return OFF;
    dso = average_over_smooth_window(dsi, n_out);
    if (dso == NULL) return win_printf_OK("cannot create data set smooth");
    if (add_one_plot_data(op, IS_DATA_SET, (void*)dso))
      return win_printf_OK("cannot add data");
    return refresh_plot(pr, UNCHANGED);
}

int do_prepare_weighted_histo(void)
{
    O_p *op = NULL, *opn = NULL;
    d_s *dsi = NULL, *dso = NULL;
    pltreg *pr = NULL;
    int i, j;
    float rate;
    int n_out = 0;
    static float rmin = -500, rmax = 500;

    if(updating_menu_state != 0)	return D_O_K;

    if (key[KEY_LSHIFT])    return win_printf_OK("This routine filter data ");
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
      return win_printf_OK("cannot find data");
    for (i = n_out = 0; i < op->n_dat; i++)
      n_out += op->dat[i]->nx;

    i = win_scanf("Preparation of a weighted histogram of helicase rate\n"
		  "Range of rate acceptable [%5f, %5f]\n"
		  ,&rmin,&rmax);
    if (i == WIN_CANCEL)	return OFF;
    if ((opn = create_and_attach_one_plot(pr, n_out, n_out, 0)) == NULL)
      return win_printf_OK("cannot create plot !");
    dso = opn->dat[0];

    for (i = n_out = 0; i < op->n_dat; i++)
      {
	dsi = op->dat[i];
	for (j = 0; j < dsi->nx; j++)
	  {
	    rate = op->ay + op->dy * dsi->yd[j];
	    if (rate >= rmin && rate <= rmax && n_out < dso->nx)
	      {
		dso->xd[n_out] = rate * op->dx;
		dso->yd[n_out++] = dsi->yd[j];
	      }
	  }
      }
    dso->nx = dso->ny = n_out;
    uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opn, IS_Y_UNIT_SET);
    return refresh_plot(pr, UNCHANGED);
}

/*
  fit parameter a in y = a*exp(-x/tau)
  by minimisation of chi^2

 */

int	find_best_sigma_of_gaussian_fit(d_s *dsi,    // input data set
					int verbose, // issue explicit error message
					double x0,   // the center position
					double sig,  // the imposed sig value
					double *a,   // the best a value
					double *E)   // the chi^2
{
  register int i, j;
  double fy, f, x, y, fx, er, la = 0;


  if (dsi == NULL)
    {
      if (verbose) win_printf("No valid data \n");
      return 1;
    }
  if (dsi->ye == NULL)
    {
      if (verbose) win_printf("No valid data error in Y \n");
      return 2;
    }
  for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
      er = dsi->ye[i];
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = exp(-(x - x0)*(x - x0)/(2*sig*sig));
      if (er > 0)
	{
	  fy += y*fx/(er*er);
	  f += (fx*fx)/(er*er);
	}
    }
  if (f != 0)    la = fy/f;
  if (a) *a = la;//*sqrt(2*M_PI*sig*sig);
  else return 2;
  for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
      x = dsi->xd[i];
      y = dsi->yd[i];
      fx = la*exp(-(x - x0)*(x - x0)/(2*sig*sig));
      er = dsi->ye[i];
      f = y - fx;
      if (er > 0)
	{
	  *E += (f*f)/(er*er);
	  j++;
	}
    }
  return 0;
}



int do_compute_activity(void)
{
    O_p *op = NULL, *oph = NULL;
    d_s *dsi = NULL, *dsh = NULL, *dsh2 = NULL, *dshf = NULL;
    pltreg *pr = NULL;
    int i, j;
    float rate;
    int n_out = 0, active = 0, inactive, ih, nh;
    double mean, sigma;
    static float thres = 10, mxuwnd = 500, mxrezip = -500;
    double a, dt, E = 0, E1 = 0, sig;

    if(updating_menu_state != 0)	return D_O_K;

    if (key[KEY_LSHIFT])    return win_printf_OK("This routine filter data ");
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
      return win_printf_OK("cannot find data");
    for (i = n_out = 0; i < op->n_dat; i++)
      n_out += op->dat[i]->nx;

    i = win_scanf("compute activity of helicase by summing points with\n"
		  "absolute rate > a threshold value %5f (bp/s)\n"
		  "and normalizing to all points\n"
		  "Max unwinding rate %4f Max rezip rate %4f\n"
		  ,&thres,&mxuwnd,&mxrezip);
    if (i == WIN_CANCEL)	return OFF;

    nh = 0.5 + 2*thres;
    if ((oph = create_and_attach_one_plot(pr, nh, nh, 0)) == NULL)
      return win_printf_OK("cannot create plot !");
    dsh = oph->dat[0];
    alloc_data_set_y_error(dsh);
    if ((dshf = create_and_attach_one_ds(oph, 20*nh, 20*nh, 0)) == NULL)
      return win_printf_OK("cannot create plot !");

    for (i = active = inactive = 0, mean = 0, sigma = 0; i < op->n_dat; i++)
      {
	dsi = op->dat[i];
	for (j = 0; j < dsi->nx; j++)
	  {
	    rate = op->ay + op->dy * dsi->yd[j];
	    if (fabs(rate) <= thres)
	      {
		ih = (int)(rate + nh/2);
		if (ih >= 0 && ih < nh)
		  dsh->yd[ih] += 1;
	      }
	  }
      }
    for (i = 0; i < nh; i++)
      {
	dsh->xd[i] = 0.5 + i - nh/2;
	dsh->ye[i] = sqrt(dsh->yd[i]);
      }


    sig = thres/2;

    i = find_best_sigma_of_gaussian_fit(dsh, 0, 0.0, sig, &a,&E);
    if (i)
      {
	win_printf("Error %d",i);
	return D_O_K;
      }
    dt = sig/10;
    for (i = 0; fabs(dt) > 0.01 && i < 1024; i++)
      {
	sig += dt;
	find_best_sigma_of_gaussian_fit(dsh, 0, 0.0, sig, &a,&E1);
	if (E1 > E)   dt = -dt/2;
	E = E1;
      }
    if (i > 1023)
      win_printf("%d iter \\tau = %g, a = %g \\chi^2 = %g",i,sig,a,E);
    if ((dsh2 = create_and_attach_one_ds(oph, nh, nh, 0)) == NULL)
      return win_printf_OK("cannot create ds !");
    alloc_data_set_y_error(dsh2);
    dsh2->nx = dsh2->ny = 0;
    float tmp, thres_neg = 0, thres_pos = 0;

    for (i = 0; i < nh; i++)
      {
	tmp  = a* exp(-(dsh->xd[i]*dsh->xd[i])/(2*sig*sig));
	dsh2->xd[dsh2->nx] = dsh->xd[i];
	dsh2->yd[dsh2->nx] = dsh->yd[i];
	dsh2->ye[dsh2->nx] = dsh->ye[i];
	if (dsh2->nx == 0)  thres_neg = dsh->xd[i];
	if (dsh->yd[i] < 2 * tmp)
	  {
	    dsh2->ny++;
	    dsh2->nx = dsh2->ny;
	    thres_pos = dsh->xd[i];
	  }
      }

    i = find_best_sigma_of_gaussian_fit(dsh2, 0, 0.0, sig, &a,&E);
    if (i)
      {
	win_printf("Error %d",i);
	return D_O_K;
      }
    dt = sig/10;
    for (i = 0; fabs(dt) > 0.001 && i < 1024; i++)
      {
	sig += dt;
	find_best_sigma_of_gaussian_fit(dsh2, 0, 0.0, sig, &a,&E1);
	if (E1 > E)   dt = -dt/2;
	//win_printf("Fit 2 iter %d; sig = %g, a = %g \\chi^2_1=%g \\chi^2 = %g dt %g",i,sig,a,E1,E,dt);
	E = E1;
      }
    if (i > 1023)
      win_printf("Fit 2 %d iter \\tau = %g, a = %g \\chi^2 = %g",i,sig,a,E);
    //free_data_set(dsh2);


    for (i = 0; i < dshf->nx; i++)
      {
	dshf->xd[i] = 0.5 + (float)i/20 - nh/2;
	dshf->yd[i] = a* exp(-(dshf->xd[i]*dshf->xd[i])/(2*sig*sig));
      }
    int inac_h;
    int act_pos = 0, act_neg = 0;
    float pos_unwound = 0, neg_rezip = 0;
    for (i = 0, inac_h = 0; i < nh; i++)
      {
	tmp  = a* exp(-(dsh->xd[i]*dsh->xd[i])/(2*sig*sig));
	// we replace gaussian feet by the fit value
	inac_h  += (dsh->yd[i] > (1.5*tmp)) ? (int)tmp : dsh->yd[i];
	if (i < nh/2 && dsh->yd[i] > (1.5*tmp)) act_neg += dsh->yd[i] - tmp;
	else if (i >= nh/2 && dsh->yd[i] > (1.5*tmp)) act_pos += dsh->yd[i] - tmp;
      }
    act_pos = 0, act_neg = 0;
    for (i = active = inactive = 0, mean = 0, sigma = 0; i < op->n_dat; i++)
      {
	dsi = op->dat[i];
	for (j = 0; j < dsi->nx; j++)
	  {
	    rate = op->ay + op->dy * dsi->yd[j];
	    if (rate > thres_pos)
	      {
		active++;
		act_pos++;
		rate = (rate > mxuwnd) ? mxuwnd : rate;
		pos_unwound += rate * op->dx;
	      }
	    else if (rate < thres_neg)
	      {
		active++;
		act_neg++;
		rate = (rate < mxrezip) ? mxrezip : rate;
		neg_rezip += rate * op->dx;
	      }
	    else
	      {
		mean += rate;
		sigma += rate*rate;
		inactive++;
	      }
	  }
      }
    sigma = (inactive > 0) ? sqrt(sigma /inactive) : sqrt(sigma);
    mean = (inactive > 0) ? mean /inactive : mean;
    int g_inact = (int)(a*sqrt(2*M_PI*sig*sig));
    win_printf("%d data points total %g(s)\n"
	       "%d data points active %g(s)\n"
	       "activity ratio %g\n"
	       "mean of inactive points %g; sigma %g\n"
	       "Fitting a Gaussian to inactive \\sigma  = %g\n"
	       "Nb. of active point = %d, activity %g\n"
	       "Nb. of active point 2 = %d, activity 2 %g\n"
	       "Nb. of active point > 0 = %d, activity > 0 %g\n"
	       ,n_out,(op->dx*n_out),active,(op->dx*active),(float)n_out/active
	       ,mean,sigma,sig,n_out-g_inact,(float)n_out/(n_out-g_inact)
	       ,n_out-inac_h,(float)n_out/(n_out-inac_h)
	       ,act_pos,(float)n_out/act_pos);
    set_plot_title(oph,"\\pt7\\stack{{%d data points total %g(s); %d data points active %g(s)}"
		   "{activity ratio %g; mean of inactive points %g; sigma %g}"
		   "{Fitting a Gaussian to inactive \\sigma  = %g; Nb. of active point = %d, activity %g}"
		   "{ Nb. of active point = %d ->%gs, activity %g}"
		   "{ Nb. of active point < %g = %d ->%gs, activity  < 0 %g;}"
		   "{ Nb. of active point > %g = %d ->%gs, activity  > 0 %g;}"
		   "{%g (kb) unwound (<rate>=%g)}"
		   "{%g (kb) rezip(<rate> =%g)}}"
		   ,n_out,(op->dx*n_out),active,(op->dx*active),(float)n_out/active
		   ,mean,sigma,sig,n_out-g_inact,(float)n_out/(n_out-g_inact)
		   ,n_out-inac_h,op->dx*(n_out-inac_h),(float)n_out/(n_out-inac_h)
		   ,thres_neg,act_neg,op->dx*act_neg,(float)n_out/act_neg
		   ,thres_pos,act_pos,op->dx*act_pos,(float)n_out/act_pos
		   ,pos_unwound/1000,pos_unwound/(act_pos*op->dx)
		   ,neg_rezip/1000,neg_rezip/(act_neg*op->dx));
    return refresh_plot(pr, UNCHANGED);
}


int do_find_noise_S_and_N_phase(void)
{
    O_p *op = NULL;
    d_s *dsi = NULL, *dso = NULL;
    pltreg *pr = NULL;
    int i, j;
    float rate;
    int nf = 0;
    static int size = 256, over = 54;
    static float sigma = 5, V0 = 35, sigmaN = 3;

    if(updating_menu_state != 0)	return D_O_K;

    if (key[KEY_LSHIFT])    return win_printf_OK("This routine filter data ");
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
      return win_printf_OK("cannot find data!");

    nf = dsi->nx;
    i = win_scanf("Analyze a signal derivative and deduce phases using mean and variance\n"
		  "Size of slidding window %4d size of overla %4d\n"
		  "Phase No helicase = mean value 0 and |variance| < \\sigma = %5f\n"
		  "Normal mode mean > V_0 = %5f and |variance| > \\sigma\n"
		  "Slow mode |variance| > \\sigma_N = %5f but |mean| < V_0\n"
		  ,&size,&over,&sigma,&V0,&sigmaN);
    if (i == WIN_CANCEL)	return OFF;
    dso = create_and_attach_one_ds(op, nf, nf, 0);
    if (dso == NULL)
      return win_printf_OK("cannot create ds of size %d!",nf);

    int k, s_2 = size/2, o_2 = over/2;
    float mean, sig = 0, state;
    for (j = 0; j < dsi->nx - size; j+= over)
      {
	for (i = 0, mean = 0; i < size; i++)
	  {
	    rate = op->ay + op->dy * dsi->yd[j+i];
	    mean += rate;
	  }
	mean /= size;
	for (i = 0, sig = 0; i < size; i++)
	  {
	    rate = (op->ay + op->dy * dsi->yd[j+i]) - mean;
	    sig += rate * rate;
	  }
	sig /= (size-1);
	sig = sqrt(sig);
	if (fabs(mean) < sigma && fabs(sig) < sigma)
	  state = 0;
	else if (fabs(mean) > V0)// && fabs(sig) > sigma)
	  state = 1;
	else if (fabs(sig) > sigmaN)
	  state = 0.5;
	else state = 2 + sig;
	for (i = 0; i < over; i++)
	  {
	    k = j+i-o_2+s_2;
	    dso->yd[k] = state;
	    dso->xd[k] = dsi->xd[k];
	  }
      }
    return refresh_plot(pr, UNCHANGED);
}

int do_find_noise(void)
{
    O_p *op = NULL;
    d_s *dsi = NULL, *dso = NULL, *dso2 = NULL;
    pltreg *pr = NULL;
    int i, j, k;
    int nf = 0;
    static float biny = 0.0005, ratio = 1.4;
    static int size = 256, over = 54, debug = 0;

    if(updating_menu_state != 0)	return D_O_K;

    if (key[KEY_LSHIFT])    return win_printf_OK("This routine filter data ");
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
      return win_printf_OK("cannot find data!");

    nf = dsi->nx;
    i = win_scanf("Analyze a signal derivative and deduce phases using mean and variance\n"
		  "Size of slidding window %4d size of overla %4d\n"
		  "Bin for gaussian %4f ratio %4f\n"
		  "%bDebug"
		  ,&size,&over,&biny,&ratio,&debug);
    if (i == WIN_CANCEL)	return OFF;
    nf = 1 + (dsi->nx-size/over);
    if (debug)       dso = create_and_attach_one_ds(op, nf, nf, 0);
    else dso = build_data_set(nf, nf);
    if (dso == NULL)
      return win_printf_OK("cannot create ds of size %d!",nf);
    if (debug)        dso2 = create_and_attach_one_ds(op, nf, nf, 0);
    else dso2 = build_data_set(nf, nf);
    if (dso2 == NULL)
      return win_printf_OK("cannot create ds of size %d!",nf);



    float mean, my2, my4, ypos = 0, height = 0;
    double sigHF;
    for (j = 0; j < dso->nx && (size+j*over)< dsi->nx; j++)
      {
	mean_y2_on_array(dsi->yd+j*over, size, 0, &mean, &my2, &my4);
	my2 = sqrt(my2);
	get_sigma_of_derivative_of_partial_ds(dsi, j*over, j*over+size, (double)(4 * my2), &sigHF);
	dso->yd[j] = my2;
	dso2->yd[j] = sigHF;
	dso->xd[j] = dso2->xd[j] = dsi->xd[j*over+size/2];
      }
    dso->nx = dso2->nx = dso->ny = dso2->ny = j;
    find_max_of_histo_of_ds_with_gaussian_convolution(dso2, biny, &ypos, &height);

    if (debug) win_printf("Noise hF peak at %g h %g",ypos,height);
    int start, end;
    d_s *dst = NULL;
    for (j = 0, start = end = -1; j < dsi->nx; j++)
      {
	if (start < 0)
	  start = (dso->yd[j/over] < ratio*ypos) ? j : start;
	else if (end < 0)
	  end = (dso->yd[j/over] > ratio*ypos) ? j : end;
	else if (start >= 0 && end >= 0)
	  {
	    if (end - start > 10)
	      {
		dst = create_and_attach_one_ds(op, end - start, end - start, 0);
		if (dst == NULL) return win_printf_OK("cannot create dsl");
		inherit_from_ds_to_ds(dst, dsi);
		set_ds_treatement(dst,"data noise kept at %d and %d",start,end);
		for (k = 0; k < dst->nx && start+k < dsi->nx; k++)
		  {
		    dst->xd[k] = dsi->xd[start+k];
		    dst->yd[k] = dsi->yd[start+k];
		  }
	      }
	    end = start = -1;
	  }
      }
    if (debug == 0)
      {
	free_data_set(dso);
	free_data_set(dso2);
      }
    return refresh_plot(pr, UNCHANGED);

}


d_s *find_peaks(d_s *dsi, float thres)
{
    d_s *dsp = NULL;
    int i, k, i1, ret;
    float  Max_pos, Max_val, tmpx, p;

    if (dsi == NULL || dsi->nx < 3) return NULL;

    dsp = build_data_set(16, 16);
    if (dsp == NULL) return NULL;
    dsp->nx = dsp->ny = 0;
    set_ds_dot_line(dsp);
    set_plot_symb(dsp, "\\pt5\\oc ");
    inherit_from_ds_to_ds(dsp, dsi);
    set_ds_source(dsp, "Peaks found with threshold %g",thres);


    for (k = 1; k < dsi->nx - 1; k++)
      {
	if ((dsi->yd[k] > dsi->yd[k - 1]) && (dsi->yd[k] >= dsi->yd[k + 1])
	    && (dsi->yd[k] > thres))
	  {
	    //win_printf("k = %d y %g",k,dsi->yd[k]);
	    ret = find_max_around(dsi->yd, dsi->nx, k, &Max_pos, &Max_val, NULL);

	    //win_printf("k = %d y %g\n ret %d pos %g val %g",k,dsi->yd[k],ret,Max_pos,Max_val);
	    if (ret == 0)
	      {
		i = (int)Max_pos;
		i1 = i + 1;
		p = (float)i1 - Max_pos;
		i = (i < 0) ? 0 : i;
		i = (i < dsi->nx) ? i : dsi->nx - 1;
		i1 = (i1 < 0) ? 0 : i1;
		i1 = (i1 < dsi->nx) ? i1 : dsi->nx - 1;
		tmpx = p * dsi->xd[i] + (1 - p) * dsi->xd[i1];
		add_new_point_to_ds(dsp, tmpx, Max_val);
	      }
	  }
      }
    return dsp;
}

int take_care_of_pause_exceeding_window = 1;

int convert_ds_pauses_in_mean_z_and_duration_function(pltreg *pr, O_p *op, int wsize, float dw, float ratio, float fthres, int hasmax, int zeroadj, int dumpcsv, int keepindex, int fullgr, int innm, int insert, char *oligoseq1)
{
    int i, j, k;
    float zmin = 0, zmax = 0, nz;
    O_p *opn = NULL, *opt = NULL;
    d_s *dsi = NULL, *dsn = NULL, *dst = NULL, *dsti = NULL, *dsp = NULL, *dsh = NULL, *dsph = NULL, *dstmp = NULL;
    d_s *dszmv = NULL; // to store zmag et vcap
    int nf;
    float ax, dx, tmp;
    static char cfilename[1024] = {0};
    char *cs = NULL;
    int acq = -1, i_bead = -1, acq0 = -1, i_bead0 = -1, cys = -1, cye = -1, cy;
    float vcap_open = 0, vcap_test = 0, vcap_low = 0, vcap_open0, vcap_test0, vcap_low0;
    float F_open = 0, F_test = 0, F_low = 0, F_open0, F_test0, F_low0;
    float zmag_open = 0, zmag_test = 0, zmag_low = 0, zmag_open0 = 0, zmag_test0 = 0, zmag_low0 = 0;
    int nvcap_open = 0, nvcap_test = 0, nvcap_low = 0;
    int nF_open = 0, nF_test = 0, nF_low = 0;
    int nzmag_open = 0, nzmag_test = 0, nzmag_low = 0;
    int ph_poss[32] = {0};
    int ph_pose[32] = {0};
    int phni = 0, stph = 0, enph = 0, nmin = 6;
    char *st = NULL;
    int *dsfirstpt = NULL;
    int *dsoffpt = NULL;
    float *dsendph5 = NULL;
    float *dsstartph5 = NULL;

    if (pr == NULL || op == NULL) return win_printf_OK("bad data");

    nf = op->n_dat;
    opt = create_and_attach_one_plot(pr, nf, nf, (insert) ? INSERT_HERE : 0);
    if (opt == NULL)      return win_printf_OK("could not alllocate plot");
    dsti = opt->dat[0];
    dsfirstpt = (int*)calloc(nf,sizeof(int));
    dsoffpt = (int*)calloc(nf,sizeof(int));
    dsendph5 = (float*)calloc(nf,sizeof(float));
    dsstartph5 = (float*)calloc(nf,sizeof(float));
    if (dsfirstpt == NULL || dsoffpt == NULL || dsendph5 == NULL || dsstartph5 == NULL)
      return win_printf_OK("could not alllocate tmp arrays");
    set_ds_source(dsti, "Mean binding position and duration");
    uns_op_2_op(opt, op);
    set_ds_dot_line(dsti);
    set_plot_symb(dsti, "\\pt5\\di");
    set_plot_y_title(opt, "Blocking position");
    set_plot_x_title(opt, "Blocking duration");

    for (i = 0, dsti->nx = 0, dsti->ny = 0; i < op->n_dat; i++)
      {
	dsi = op->dat[i];
	if (dsi->xe != NULL && dsti->xe == NULL)
	    alloc_data_set_x_error(dsti);
	size_t it;
	for (it = 0; it < 32; it++) ph_poss[it] = 0;
	for (it = 0; it < 32; it++) ph_pose[it] = 0;
	if (dsi->source != NULL)
	  {
	    st = strstr(dsi->source,"starting =");
	    stph = 0;
	    if (st != NULL)  sscanf(st,"starting = %d",&stph);
	    enph = 0;
	    st = strstr(dsi->source,"with reference =");
	    if (st != NULL)  sscanf(st,"with reference = %d",&enph);
	    dsoffpt[i] = stph - enph;
	    for (st = strstr(dsi->source,"Phase["); st != NULL && st[0] != 0; st = strstr(st+1,"Phase["))
	      {
		if (sscanf(st,"Phase[%d] = [%d,%d]",&phni,&stph,&enph) == 3)
		  {
		    if (phni < 32)
		      {
			ph_poss[phni] = stph;
			ph_pose[phni] = enph;
		      }
		  }
	      }
	  }
	dsendph5[i] = ph_pose[5]; // we keep the end of ph5 for each ds
	dsstartph5[i] = ph_poss[5];
	if (dsi->treatement != NULL)
	  sscanf(dsi->treatement,"Averaging = %d",&nmin);
	if (dsi->src_parameter_type[2] != NULL)
	  {
	    F_open0 = 0;
	    if (sscanf(dsi->src_parameter_type[2],"Zmag open => Force = %f",&F_open0) == 1)
	      {
		zmag_open0 = dsi->src_parameter[2];
		zmag_open += zmag_open0;
		nzmag_open++;
		F_open += F_open0;
		nF_open++;
	      }
	  }
	if (dsi->src_parameter_type[3] != NULL)
	  {
	    F_test0 = 0;
	    if (sscanf(dsi->src_parameter_type[3],"Zmag test => Force = %f",&F_test0) == 1)
	      {
		zmag_test0 = dsi->src_parameter[3];
		zmag_test += zmag_test0;
		nzmag_test++;
		F_test += F_test0;
		nF_test++;
	      }
	  }
	if (dsi->src_parameter_type[4] != NULL)
	  {
	    F_low0 = 0;
	    if (sscanf(dsi->src_parameter_type[4],"Zmag low => Force = %f",&F_low0) == 1)
	      {
		zmag_low0 = dsi->src_parameter[4];
		zmag_low += zmag_low0;
		nzmag_low++;
		F_low += F_low0;
		nF_low++;
	      }
	  }
	if (dsi->src_parameter_type[5] != NULL)
	  {
	    if (strstr(dsi->src_parameter_type[5],"Vcap open") != NULL)
	      {
		vcap_open0 = dsi->src_parameter[5];
		vcap_open += vcap_open0;
		nvcap_open++;
	      }
	  }
	if (dsi->src_parameter_type[6] != NULL)
	  {
	    if (strstr(dsi->src_parameter_type[6],"Vcap test") != NULL)
	      {
		vcap_test0 = dsi->src_parameter[6];
		vcap_test += vcap_test0;
		nvcap_test++;
	      }
	  }
	if (dsi->src_parameter_type[7] != NULL)
	  {
	    if (strstr(dsi->src_parameter_type[7],"Vcap low") != NULL)
	      {
		vcap_low0 = dsi->src_parameter[7];
		vcap_low += vcap_low0;
		nvcap_low++;
	      }
	  }
	if (dsi->source != NULL)
	  {
	    j = -1;
	    cs = strstr(dsi->source,"Bead Cycle");
	    if (cs != NULL)
	      j = sscanf(cs,"Bead Cycle %d",&cy);
	    else if (cs == NULL)
	      {
		cs = strstr(dsi->source,"cycle");
		if (cs != NULL)
		  j = sscanf(cs,"cycle %d",&cy);
	      }
	    if (j == 1)
	      {
		if (cys < 0) cys = cy;
		if (cye < 0) cye = cy;
		cys = (cy < cys) ? cy : cys;
		cye = (cye < cy) ? cy : cye;
	      }
	    cs = strstr(dsi->source,"Acquistion");
	    if (cs != NULL)  // for compability with spelling issue
	      {
		if (sscanf(cs,"Acquistion %d for bead %d",&acq,&i_bead) == 2)
		  {
		    if (acq0 < 0) acq0 = acq;
		    if (i_bead0 < 0) i_bead0 = i_bead;
		    if ((acq0 >= 0 && acq0 != acq) || (i_bead0 >= 0 && i_bead0 != i_bead))
		      win_printf("Inconstancy in Acquisition %d -> %d\n"
				 "on in bead Nb. %d -> %d"
				 ,acq0, acq, i_bead0, i_bead);
		  }
		else if (sscanf(cs,"Acquistion %d bead %d",&acq,&i_bead) == 2)
		  {
		    if (acq0 < 0) acq0 = acq;
		    if (i_bead0 < 0) i_bead0 = i_bead;
		    if ((acq0 >= 0 && acq0 != acq) || (i_bead0 >= 0 && i_bead0 != i_bead))
		      win_printf("Inconstancy in Acquisition %d -> %d\n"
				 "on in bead Nb. %d -> %d"
				 ,acq0, acq, i_bead0, i_bead);
		  }

	      }
	    else cs = strstr(dsi->source,"Acquisition");
	    if (cs != NULL)
	      {
		if (sscanf(cs,"Acquisition %d for bead %d",&acq,&i_bead) == 2)
		  {
		    if (acq0 < 0) acq0 = acq;
		    if (i_bead0 < 0) i_bead0 = i_bead;
		    if ((acq0 >= 0 && acq0 != acq) || (i_bead0 >= 0 && i_bead0 != i_bead))
		      win_printf("Inconstancy in Acquisition %d -> %d\n"
				 "on in bead Nb. %d -> %d"
				 ,acq0, acq, i_bead0, i_bead);
		  }
		else if (sscanf(cs,"Acquisition %d bead %d",&acq,&i_bead) == 2)
		  {
		    if (acq0 < 0) acq0 = acq;
		    if (i_bead0 < 0) i_bead0 = i_bead;
		    if ((acq0 >= 0 && acq0 != acq) || (i_bead0 >= 0 && i_bead0 != i_bead))
		      win_printf("Inconstancy in Acquisition %d -> %d\n"
				 "on in bead Nb. %d -> %d"
				 ,acq0, acq, i_bead0, i_bead);
		  }

	      }

	  }
	for (j = 0; j < dsi->nx && j < dsi->ny; j++)
	  {
	    if (dsi->xe != NULL && dsti->xe != NULL)
	      add_new_point_with_x_error_to_ds(dsti,dsi->xd[j],dsi->xe[j],dsi->yd[j]);
	    else add_new_point_to_ds(dsti,dsi->xd[j],dsi->yd[j]);
	    zmin = ((i == 0) || (dsi->yd[j] < zmin)) ? dsi->yd[j] : zmin;
	    zmax = ((i == 0) || (dsi->yd[j] > zmax)) ? dsi->yd[j] : zmax;
	    // we keep the rank of the first point to recover ds index
	    dsfirstpt[i] = dsti->nx;
	  }
      }
    zmag_open = (nzmag_open > 0) ? zmag_open/nzmag_open : zmag_open;
    zmag_test = (nzmag_test > 0) ? zmag_test/nzmag_test : zmag_test;
    zmag_low = (nzmag_low > 0) ? zmag_low/nzmag_low : zmag_low;

    F_open = (nF_open > 0) ? F_open/nF_open : F_open;
    F_test = (nF_test > 0) ? F_test/nF_test : F_test;
    F_low = (nF_low > 0) ? F_low/nF_low : F_low;

    vcap_open = (nvcap_open > 0) ? vcap_open/nvcap_open : vcap_open;
    vcap_test = (nvcap_test > 0) ? vcap_test/nvcap_test : vcap_test;
    vcap_low = (nvcap_low > 0) ? vcap_low/nvcap_low : vcap_low;

    // check if unit is micron !
    get_afine_param_from_op(opt, IS_Y_UNIT_SET, &ax, &dx);
    dx *= 1e9;
    nz = (int) (0.5+ (zmax - zmin)*dx*dw);
    nf = nz + 4 * wsize;
    nf = (nf > 16384) ? 16384 : nf;

    opn = create_and_attach_one_plot(pr, nf, nf,  (insert) ? INSERT_HERE : 0);
    if (opn == NULL)	  return win_printf_OK("cannot create plot opn with %d points!",nf);
    dsn = opn->dat[0];
    opn->need_to_refresh = 1;

    uns_op_2_op_by_type(opt, IS_Y_UNIT_SET, opn, IS_X_UNIT_SET);
    //set_plot_symb(dsn, "\\pt5\\di");
    set_ds_source(dsn, "window_histo w %d dx %g", wsize, dw);
    set_plot_title(opn, "window histo of %s", (op->title != NULL) ? op->title : "");
    set_plot_x_title(opn, "value");
    set_op_filename(opn, "%s-histo.gr",op->filename);

    dsp = create_and_attach_one_ds(opn, 16, 16, 0);
    if (dsp == NULL)
      return win_printf_OK("cannot create ds for peak!");
    alloc_data_set_x_error(dsp);
    set_ds_dot_line(dsp);
    set_plot_symb(dsp, "\\pt5\\oc");
    dsp->nx = dsp->ny = 0;
    set_ds_source(dsp, "Number of binding window_histo w %d dx %g", wsize, dw);

    dst = create_and_attach_one_ds(opt, 16, 16, 0);
    if (dst == NULL)	return win_printf_OK("cannot create ds dst for timin!");
    alloc_data_set_x_error(dst);
    set_ds_dot_line(dst);
    dst->nx = dst->ny = 0;
    set_plot_symb(dst, "\\di");
    set_ds_source(dst, "Binding duration window_histo w %d dx %g", wsize, dw);

    dsh = build_histo_ds_with_gaussian_convolution(dsti, ratio*((float)wsize)/1000);
    if (dsh == NULL) 	return win_printf_OK("cannot create histogramm ds!");
    add_one_plot_data(opn, IS_DATA_SET, (void*)dsh);


    dsph = find_peaks(dsh, fthres);
    if (dsph == NULL) 	return win_printf_OK("cannot create histogramm peak ds!");
    add_one_plot_data(opn, IS_DATA_SET, (void*)dsph);
    add_peak_limits_as_err_in_histo(dsh, dsph, (float)fthres/2);


    dszmv = build_data_set(dsph->nx, dsph->nx);
    if (dszmv) alloc_data_set_y_error(dszmv);
    float sig, sigy;
    int np, npt;
    for  (i = 0; i < nf; i++)
      {
	tmp = zmin + ((zmax - zmin)*(i - 2*wsize))/nz;
	for (k = 0; k < dsti->nx; k++)
	  {
	    if (fabs(dx*(tmp - dsti->yd[k])) < ((float)wsize/2))
	      {
		dsn->yd[i] += 1;
	      }
	  }
	dsn->xd[i] = tmp;
      }

    float minsigy = 1;
    int dsi_i = 0, ptind = 0, ph5_st = 0;
    for (i = 0; i < dsph->nx; i++)
      {
	for (k = 0, tmp = 0, np = 0, npt = 0; k < dsti->nx; k++)
	  {
	    for(dsi_i = 0; k > dsfirstpt[dsi_i] && dsi_i < op->n_dat; dsi_i++);
	    if (fabs(dx*(dsph->xd[i] - dsti->yd[k])) < ((float)wsize/2))
	      {
		npt++;
		ptind = dsoffpt[dsi_i] + dsendph5[dsi_i] - nmin;
		ph5_st = dsoffpt[dsi_i] + dsstartph5[dsi_i];
		/*
		if (k > dsti->nx - 20)
		  {
		    win_printf("Peak %d Point %d in cycle %d\n"
			       "ptind %d Ph5 start %d\n"
			       "x + err = %g max expected %g"
			       "np %d npt %d"
			       "y = %g wsize %d"
			       ,i,k,dsi_i,ptind,ph5_st
			       ,(dsti->xd[k] + fabs(dsti->xe[k]))
			       ,(float)ptind,np,npt,dsti->yd[k],wsize);
		  }
		*/
		if (ptind < 0) np++;// win_printf("ptind < 0");}
		else if ((dsti->xd[k] - (float)ph5_st) < 0) np++; //win_printf("not in ph5");}
		else if ((1000*dsti->yd[k] - (wsize/2)) < 0) np++; //win_printf("zero peak");}
		else if ((dsti->xd[k] + fabs(dsti->xe[k]) - (float)ptind) < 0) np++;// win_printf("Not exceeding");}
		//else win_printf("points %d exceeds window in cycle %d\nx + er %g ptind %d\nnp %d npt %d"
		//		,k,dsi_i,(dsti->xd[k] + fabs(dsti->xe[k])),ptind,np,npt);
		//if ((k != 0) || (fabs(dsti->yd[k]) < wsize/2)) np++;
		// this is to treat correctly blocage exhausting the test window
		// we count the number of real end of blocage if we are not dealing with the
		// last blocage with non zero extension
		tmp += 2*fabs(dsti->xe[k]);
		// in all case we add the blocking time
		if (dsti->xd[k] < (float)ph5_st && dszmv != NULL)
		  {
		    dszmv->yd[i] = zmag_open;
		    dszmv->xd[i] = vcap_open;
		    dszmv->ye[i] = F_open;
		  }
		else
		  {
		    dszmv->yd[i] = zmag_test;
		    dszmv->xd[i] = vcap_test;
		    dszmv->ye[i] = F_test;
		  }
	      }
	  }
	//win_printf("point %d npt %d np %d\n"
	//	   "total time %g",dst->nx,npt,np,tmp*opt->dx);

	if (take_care_of_pause_exceeding_window == 0) np = npt;
	tmp /= (np > 0) ? np : 1;
	for (k = 0, sig = sigy = 0; k < dsti->nx; k++)
	  {
	    if (fabs(dx*(dsph->xd[i] - dsti->yd[k])) < ((float)wsize/2))
	      {
		//sig += (dsti->xd[k] - tmp) * (dsti->xd[k] - tmp);
		sigy += (dsti->yd[k] - dsph->xd[i]) * (dsti->yd[k] - dsph->xd[i]);
	      }
	  }
	//sig /= (np > 1) ? np-1 : 1;
	//sig = sqrt(sig);
	sig = (np > 0) ? tmp * sqrt(np)/np : tmp;
	sigy /= (npt > 1) ? npt-1 : 1;
	sigy = sqrt(sigy);
	// sigy might be 0 we shall replace it by the min
	minsigy = (sigy > 0 && sigy < minsigy) ? sigy : minsigy;
	add_new_point_with_x_error_to_ds(dsp, dsph->xd[i], sigy, npt);
	add_new_point_with_x_error_to_ds(dst, tmp, sig, dsph->xd[i]);
      }
    for (i = 0; i < dsp->nx; i++)
      {
	if (dsp->xe[i] == 0) dsp->xe[i] = minsigy;
      }
    float x, a, a0, a1;
    for (i = 0; i < dsh->nx; i++)
      {
	x = dsh->xd[i];
	for(j = 0; j < dsph->nx; j++)
	  if(dsph->xd[j] >= x) break;
	j = (j > 0) ? j - 1 : j;
	k = j + 1;
	k = (k < dsph->nx) ? k : dsph->nx - 1;
	if (k == dsph->nx - 1) j = k-1;
	a0 = (dsph->yd[j] > 0) ? (dsp->yd[j]/dsph->yd[j]) : 1;
	a1 = (dsph->yd[k] > 0) ? (dsp->yd[k]/dsph->yd[k]) : 1;
	a = a0 * (dsph->xd[k] - x) + a1 * (x - dsph->xd[j]);
	a = ((dsph->xd[k] - dsph->xd[j]) > 0) ? a/(dsph->xd[k] - dsph->xd[j]) : 1;
	dsh->yd[i] *= a;
      }

    alloc_data_set_x_box(dsph);
    //alloc_data_set_x_error(dsph);
    alloc_data_set_y_error(dsph);
    for(j = 0; j < dsph->nx; j++)
      {
	dsph->yd[j] = dsp->yd[j];
	dsph->xbu[j] = dsph->xbd[j] = dsp->xe[j];
	dsph->ye[j] = opt->ax + opt->dx * dst->xd[j];
      }

    for (i = 0; i < nf; i++)
      dsn->xd[i] = zmin + ((zmax - zmin)* (i - 2*wsize))/nz;;
    dstmp = build_data_set(dsti->nx, dsti->nx);
    if (dstmp == NULL)      return win_printf_OK("could not alllocate tmp ds");
    for (j = 0, zmin = zmax = 0, sig = 0, np = 0; j < dsti->nx; j++)
      {
	for (i = 0, k = 0, tmp = dsti->yd[j] - dsp->xd[0]; i < dsp->nx; i++)
	  {
	    if (fabs(dsti->yd[j] - dsp->xd[i]) < fabs(tmp))
	      {
		k = i;
		tmp = dsti->yd[j] - dsp->xd[i];
	      }
	  }
	dstmp->yd[j] = tmp;
	dstmp->xd[j] = k;
	if (j == 0) zmin = zmax = 0;
	zmin = (tmp < zmin) ? tmp : zmin;
	zmax = (tmp > zmax) ? tmp : zmax;
	if ((hasmax > 0) && (k == dsp->nx - 1)) continue;
	sig += tmp * tmp;
	np++;
      }
    sig /= (np > 1) ? (np - 1) : 1;
    sig = sqrt(sig);
    for (i = 0, sigy = 0, j = 0; i < dstmp->nx; i++)
      {
	tmp = dstmp->yd[i];
	k = (int)dstmp->xd[i];
	if (hasmax && k == dsp->nx - 1) continue;
	if (fabs(tmp) < 3*sig)
	  {
	    sigy += tmp * tmp;
	    j++;
	  }
      }
    sigy /= (j > 1) ? (j -1) : 1;
    sigy = sqrt(sigy);
    free_data_set(dstmp);
    win_printf("Found %d blocking peaks with sig = %g nm\n"
	       "with %d peaks within 3\\sigma  sig_c = %g nm",np,sig*1000,j,sigy*1000);

    if (dumpcsv)
      {
	FILE *fp;
	int header_needed = 0;
	if (op->dir && op->filename)
	  {
	    append_filename(cfilename, op->dir, op->filename, 1024);
	    replace_extension(cfilename, cfilename, "csv", sizeof(cfilename));
	  }

	if (do_select_file(cfilename, sizeof(cfilename), "CSV-FILE", "*.csv", "File to save peaks", 0))
	  {
	    return win_printf_OK("Cannot select input file");
	  }

	fp = fopen(cfilename, "r");
	header_needed = (fp == NULL) ? 1 : 0;

	if (dumpcsv == 2) fp = fopen(cfilename, "a");
	else fp = fopen(cfilename, "w");

	if (fp == NULL)
	  {
	    return win_printf_OK("Cannot open file:\n%s", backslash_to_slash(cfilename));
	  }
	if (header_needed)
	  {
	    if (keepindex)
	      {
		fprintf(fp, "Peak_Nb\t");
		fprintf(fp, "Bead_Nb\t");
		if (innm)  fprintf(fp, "X(nm)\t");
		else fprintf(fp, "X(%s)\t", (opn->x_unit != NULL) ? opn->x_unit : "");
		fprintf(fp, "BindingNb\t");
		if (innm)  fprintf(fp, "dX(nm)\t");
		else fprintf(fp, "dX(%s)\t", (opn->x_unit != NULL) ? opn->x_unit : "");
		fprintf(fp, "Duration(%s)\t",(opt->x_unit != NULL) ? opt->x_unit : "");
		fprintf(fp, "ErrorInDur\n");
	      }
	    else
	      {
		fprintf(fp, "Bead_Nb\t");
		fprintf(fp, "Peak_Nb\t");
		if (innm)  fprintf(fp, "X(nm)\t");
		else fprintf(fp, "X(%s)\t", (opn->x_unit != NULL) ? opn->x_unit : "");
		if (innm)  fprintf(fp, "dX(nm)\t");
		else fprintf(fp, "dX(%s)\t", (opn->x_unit != NULL) ? opn->x_unit : "");
		fprintf(fp, "BindingNb\t");
		fprintf(fp, "Duration(%s)\t",(opt->x_unit != NULL) ? opt->x_unit : "");
		fprintf(fp, "ErrorInDur\t");
		fprintf(fp, "Trk_Nb\t");
		fprintf(fp, "Zmag\t");
		fprintf(fp, "Fe\t");
		fprintf(fp, "Vcap\t");
		fprintf(fp, "Cycle_s\t");
		fprintf(fp, "Cycle_e\t");
		fprintf(fp, "OligoSeq\n");
	      }
	  }
	else fprintf(fp, "\n");
	float pos;
	for (i = 0; i < dsp->nx && i < dst->nx; i++)
	  {
	    if (keepindex)
	      {
		fprintf(fp, "%d\t", i);
		fprintf(fp, "%d\t", i_bead0);
		pos = dsp->xd[i];
		if (zeroadj) pos -= dsp->xd[0];
		if (innm)  fprintf(fp, "%g\t", 1000*(opn->ax + opn->dx * pos));
		else fprintf(fp, "%g\t", opn->ax + opn->dx * pos);
		fprintf(fp, "%g\t", dsp->yd[i]);
		if (innm)   fprintf(fp, "%g\t", 1000*(opn->ax + opn->dx * dsp->xe[i]));
		else fprintf(fp, "%g\t", opn->ax + opn->dx * dsp->xe[i]);
		fprintf(fp, "%g\t", opt->ax + opt->dx * dst->xd[i]);
		pos = sqrt(dsp->yd[i]);
		pos = (pos < 1) ? 1 : pos;
		fprintf(fp, "%g\n", opt->ax + opt->dx * (dst->xe[i]/pos));
	      }
	    else
	      {
		fprintf(fp, "%d\t", i_bead);
		fprintf(fp, "%d\t", i);
		pos = dsp->xd[i];
		if (zeroadj) pos -= dsp->xd[0];
		if (innm)  fprintf(fp, "%g\t", 1000*(opn->ax + opn->dx * pos));
		else fprintf(fp, "%g\t", opn->ax + opn->dx * pos);
		if (innm)   fprintf(fp, "%g\t", 1000*(opn->ax + opn->dx * dsp->xe[i]));
		else fprintf(fp, "%g\t", opn->ax + opn->dx * dsp->xe[i]);
		fprintf(fp, "%g\t", dsp->yd[i]);
		fprintf(fp, "%g\t", opt->ax + opt->dx * dst->xd[i]);
		pos = sqrt(dsp->yd[i]);
		pos = (pos < 1) ? 1 : pos;
		fprintf(fp, "%g\t", opt->ax + opt->dx * (dst->xe[i]/pos));
		fprintf(fp, "%d\t",acq0);
		if (dszmv != NULL)
		  {
		    fprintf(fp, "%g\t",dszmv->yd[i]);
		    fprintf(fp, "%g\t",dszmv->ye[i]);
		    fprintf(fp, "%g\t",dszmv->xd[i]);
		  }
		else
		  {
		    fprintf(fp, "%g\t",zmag_test);
		    fprintf(fp, "%g\t",F_test);
		    fprintf(fp, "%g\t",vcap_test);
		  }
		fprintf(fp, "%d\t",cys);
		fprintf(fp, "%d\t",cye);
		fprintf(fp, "%s\n",oligoseq1);
	      }
	  }
	fclose(fp);
      }

    if (zeroadj)
      {
	for (i = 0; i < dsh->nx; i++)
	  dsh->xd[i] -= dsph->xd[0];
	for (i = dsph->nx - 1; i >= 0; i--)
	  dsph->xd[i] -= dsph->xd[0];
      }
    if (fullgr == 0)
      {
	remove_data (pr, IS_ONE_PLOT, (void*)opt);
	remove_ds_from_op(opn, dsp);
	remove_ds_from_op(opn, dsn);
      }
    if (dszmv) free_data_set(dszmv);
    if (dsfirstpt != NULL) free(dsfirstpt);
    if (dsoffpt != NULL) free(dsoffpt);
    if (dsendph5 != NULL) free(dsendph5);

    return 0;
}

int convert_ds_pauses_in_mean_z_and_duration_2(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    int curop;
    static int   inrange = 0, ndss = 0, ndse = 0;
    static int wsize = 15, hasmax = 1, zeroadj = 0, dumpcsv = 0, keepindex = 0, fullgr = 0, innm = 1, insert = 1, thres = 3;
    static float dw = 1, ratio = 0.5;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
      return 0;


    if (updating_menu_state != 0)  return 0;
    ndse = (ndse == 0 || ndse >= op->n_dat) ? op->n_dat : ndse;
    ndss = (ndss >= op->n_dat) ? 0 : ndss;
    i = win_scanf("Treat data where each datasets points is the blocking duration in x and \n"
		  "poition in y. This will compute Average blocking time and position\n"
		  "Size of the averaging window in nm W=%4d and step %4f in (nm)\n"
		  "Peaks will be found with a gaussian histogram with bin = %4f*W\n"
		  "Threshold number of binding below witch peaks are disregard %4f\n"
		  "Data contains the open HP state at max z: No->%R, Yes->%r\n"
		  "Compute duration taking care of pauses exceeding test phase %b\n"
		  "Data contains the close HP state at min z:\n"
		  "Readjust peaks positions to make its position 0: No->%R, Yes->%r\n"
		  "Dump data in a CSV file:  No->%R, Yes->%r, Append to file->%r\n"
		  "Keep csv compatible with find peaks %b\n"
		  "Add the Oligosequence: (space = no oligo)\n %32s\n"
		  "keep all data %b Extension in %R->\\mu m or %r->nm\n"
		  "Insert plots %b\n"
		  "Analysis all ds->%R or in the range->%r [%4d,%4d[\n"
		  ,&wsize,&dw,&ratio,&thres,&hasmax,&take_care_of_pause_exceeding_window,&zeroadj
		  ,&dumpcsv,&keepindex,oligoseq,&fullgr,&innm,&insert,&inrange,&ndss,&ndse);
    if (i == WIN_CANCEL) return 0;



    curop = pr->cur_op;
    int n_dat = 0;
    d_s **orgds = NULL;
    if (inrange)
      {
	orgds = op->dat;
	n_dat = op->n_dat;
	if (ndss < 0 || ndss >= op->n_dat)
	  return win_printf_OK("1st ds %d out of range [0,%d[",ndss,op->n_dat);
	if (ndse < 0 || ndse > op->n_dat)
	  return win_printf_OK("last ds %d out of range [0,%d[",ndse,op->n_dat);
	op->dat = orgds + ndss;
	op->n_dat = ndse - ndss;
      }
    convert_ds_pauses_in_mean_z_and_duration_function(pr, op, wsize, dw, ratio, thres, hasmax, zeroadj, dumpcsv, keepindex, fullgr, innm, insert, oligoseq);
    if (inrange)
      {
	if (orgds != NULL) op->dat = orgds;
	if (n_dat > 0) op->n_dat = n_dat;
      }

    return refresh_plot(pr, (insert) ? curop+1 : pr->n_op - 1);
}

int do_normalize_oligo_histo(void)
{
    int i;
    O_p *op = NULL;
    d_s *dsi = NULL, *ds = NULL;
    pltreg *pr = NULL;

   if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
      return win_printf_OK("This routine cuts datasets keeping only flat part");

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
      return win_printf_OK("cannot find data");

    if (dsi->xe == NULL || dsi->xed == NULL || dsi->xbu == NULL || dsi->xbd == NULL)
      return win_printf_OK("cannot find box error bars");
    ds = create_and_attach_one_ds(op,2*dsi->nx,2*dsi->nx,0);
    if (ds == NULL) return win_printf_OK("cannot create data set");

    for (i = 0; i < dsi->nx; i++)
      {
	if ((i > 0) && fabs((dsi->xd[i] - dsi->xed[i]) - (dsi->xd[i-1] + dsi->xe[i-1])) < ((dsi->xbu[i] + dsi->xbd[i])/2))
	  ds->xd[2*i] = dsi->xd[i] - (dsi->xed[i] + dsi->xbd[i])/2;
	else ds->xd[2*i] = dsi->xd[i] - dsi->xed[i];
	ds->yd[2*i] = dsi->yd[i];
	if ((i < dsi->nx -1) && fabs((dsi->xd[i] + dsi->xe[i]) - (dsi->xd[i+1] - dsi->xed[i+1])) < ((dsi->xbu[i] + dsi->xbd[i])/2))
	  ds->xd[2*i+1] = dsi->xd[i] + (dsi->xe[i] + dsi->xbu[i])/2;
	else 	ds->xd[2*i+1] = dsi->xd[i] + dsi->xe[i];
	ds->yd[2*i+1] = dsi->yd[i];
      }
    inherit_from_ds_to_ds(ds, dsi);
    set_ds_source(ds, "Normalize histo");

    return refresh_plot(pr, UNCHANGED);
}

int do_cordrift_reproduce_decreasing_steps_duration_and_analyze(void)
{
    int i, j, curop;
    O_p *op = NULL, *opn = NULL;
    d_s *dsi = NULL, *ds = NULL;
    pltreg *pr = NULL;
    static int nmin = 6, oligo_cor = 0, use_deriv = 0, insert = 1, keepdur = 0;
    static float biny = 0.005, mthres = 3, oligo_dz = 0.001, bin_max = 0.015, fthres = 1.5;
    static int   keep_err = 0, ph5 = 1;
    static int wsize = 15, hasmax = 1, zeroadj = 0, dumpcsv = 0, keepindex = 0, fullgr = 0, innm = 1;
    static float dw = 1, ratio = 0.35;
    double sigHF = 0, uperlimit;
    char question[2048] = {0};

    if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
      return win_printf_OK("This routine cuts datasets keeping only flat part");

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
      return win_printf_OK("cannot find data");
    uperlimit = fabs(op->y_hi - op->y_lo);
    get_sigma_of_derivative_of_partial_ds(dsi, 0, dsi->nx, uperlimit, &sigHF);


    snprintf(question,sizeof(question)
	     ,"Transform data set in blockage duration using histogram to find them\n"
	     "And then analyze them. The selected dataset has a noise of %g\n"
	     "%%R1) Use a fix bin size for the histogram\n"
	     "%%r2) compute bin size from signal derivative\n"
	     "In case 1) specify gaussian binsize of histo %%6f\n"
	     "in case 2) specify the maximum bin size accepetd %%6f\n"
	     "Nb. of points averaged to find boudary %%4d points\n"
	     "Exclude points with averaging smaller than %%6f\n"
	     "Apply treatment to (%%R-all) (%%r->end of PH3 to end of PH5) (%%r->PH5 only)\n"
	     "%%b keep all data sets proceeded and in a new plot\n"
	     "Keep error bars %%b\n"
	     "Oligo correction:\n"
	     "%%R->None or using the %%r->actual numbers or %%r->estimated numbers of steps\n"
	     "Define the Z correction of 1 oligo %%5f\n"
	     "Then compute Average blocking time and position\n"
	     "Size of the averaging window in nm W=%%4d and step %%4f in (nm)\n"
	     "Peaks will be found with a gaussian histogram with bin = %%4f*W\n"
	     "Threshold number of binding below witch peaks are disregard %%4f\n"
	     "Data contains the open HP state at max z: No->%%R, Yes->%%r\n"
	     "Compute duration taking care of pauses exceeding test phase %%b\n"
	     "Data contains the close HP state at min z:\n"
	     "Readjust peaks positions to make its position 0: No->%%R, Yes->%%r\n"
	     "Dump data in a CSV file:  No->%%R, Yes->%%r, Append to file->%%r\n"
	     "Keep csv compatible with find peaks %%b\n"
	     "Add the Oligosequence: (space = no oligo)\n %%32s\n"
	     "keep all data %%b Extension in %%R->\\mu m or %%r->nm\n"
	     "Insert plots %%b\n"
	     ,sigHF/sqrt(2));

    i = win_scanf(question,&use_deriv,&biny,&bin_max,&nmin,&mthres,&ph5,&keepdur,&keep_err
		  ,&oligo_cor,&oligo_dz,&wsize,&dw,&ratio,&fthres,&hasmax
		  ,&take_care_of_pause_exceeding_window,&zeroadj,&dumpcsv
		  ,&keepindex,oligoseq,&fullgr,&innm,&insert);
    if (i == WIN_CANCEL)	return OFF;

    curop = pr->cur_op;
    i = op->n_dat;
    op->n_dat = 0; // we want to copy only the plot stuuff
    curop = pr->cur_op;
    opn = duplicate_plot_data(op, NULL);
    if (opn == NULL) win_printf_OK("cannot create plot !");
    op->n_dat = i;
    if (keepdur)
      {
	add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opn);
	for (j = pr->n_op - 1 ;insert > 0 && j > curop+1; j--)
	  pr->o_p[j] = pr->o_p[j-1];
	if (insert) pr->o_p[curop+1] = opn;
      }


    for(i = 0; i < op->n_dat; i++)
      {
	dsi = op->dat[i];
	if (use_deriv)
	  {
	    get_sigma_of_derivative_of_partial_ds(dsi, 0, dsi->nx, uperlimit, &sigHF);
	    //sigHF /= sqrt(2);
	    if (sigHF > (double)bin_max) continue;
	    ds = cordrift_reproduce_decreasing_steps_duration(dsi, mthres, nmin, (float)sigHF, ph5);
	  }
	else ds = cordrift_reproduce_decreasing_steps_duration(dsi, mthres, nmin, biny, ph5);
      if (keep_err == 0)
	{
	  free_data_set_y_down_error(ds);
	  free_data_set_y_error(ds);
	}
	if (ds != NULL)
	  {
	    correct_blocking_for_dsDNA_oligo_length(ds, oligo_cor, oligo_dz);
	    add_one_plot_data(opn, IS_DATA_SET, (void*)ds);
	  }
	else warning_message("null ds");
      }

    if (opn == NULL) win_printf_OK("cannot create plot !");

    convert_ds_pauses_in_mean_z_and_duration_function(pr, opn, wsize, dw, ratio, fthres, hasmax, zeroadj, dumpcsv, keepindex, fullgr, innm, insert, oligoseq);

    return refresh_plot(pr, (insert) ? curop+1 : pr->n_op - 1);
}









# ifdef KEEP

int convert_ds_pauses_in_mean_z_and_duration_2(void)
{
    int i, j, k;
    float zmin = 0, zmax = 0, nz;
    pltreg *pr = NULL;
    O_p *op = NULL, *opn = NULL, *opt = NULL;
    d_s *dsi = NULL, *dsn = NULL, *dst = NULL, *dsti = NULL, *dsp = NULL, *dsh = NULL, *dsph = NULL, *dstmp = NULL;
    int nf, curop;
    float ax, dx, tmp;
    static int  thres = 3;
    static int wsize = 15, hasmax = 1, zeroadj = 0, dumpcsv = 0, keepindex = 0, fullgr = 0, innm = 1, insert = 1;
    static float dw = 1, ratio = 0.5;
    static char cfilename[1024] = {0};
    char *cs = NULL;
    int acq = -1, i_bead = -1, acq0 = -1, i_bead0 = -1, cys = -1, cye = -1, cy;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
      return 0;
    //g_r = find_g_record_in_pltreg(pr);


  if (updating_menu_state != 0)  return 0;
  nf = op->n_dat;
  i = win_scanf("Treat data where each datasets points is the blocking duration in x and \n"
		"poition in y. This will compute Average blocking time and position\n"
		"Size of the averaging window in nm W=%4d and step %4f in (nm)\n"
		"Peaks will be found with a gaussian histogram with bin = %4f*W\n"
		"Threshold number of binding below witch peaks are disregard %4d\n"
		"Data contains the open HP state at max z: No->%R, Yes->%r\n"
		"Data contains the close HP state at min z:\n"
		"Readjust peaks positions to make its position 0: No->%R, Yes->%r\n"
		"Dump data in a CSV file:  No->%R, Yes->%r, Append to file->%r\n"
		"Keep csv compatible with find peaks %b\n"
		"keep all data %b Extension in %R->\\mu m or %r->nm\n"
		"Insert plots %b\n"
		,&wsize,&dw,&ratio,&thres,&hasmax,&zeroadj,&dumpcsv,&keepindex,&fullgr,&innm,&insert);
  if (i == WIN_CANCEL) return 0;
  curop = pr->cur_op;
  opt = create_and_attach_one_plot(pr, nf, nf, (insert) ? INSERT_HERE : 0);
  if (opt == NULL)      return 0;
  dsti = opt->dat[0];
  set_ds_source(dsti, "Mean binding position and duration");
  uns_op_2_op(opt, op);
  set_ds_dot_line(dsti);
  set_plot_symb(dsti, "\\pt5\\di");
  set_plot_y_title(opt, "Blocking position");
  set_plot_x_title(opt, "Blocking duration");
  for (i = 0, dsti->nx = 0, dsti->ny = 0; i < op->n_dat; i++)
    {
      dsi = op->dat[i];
      if (dsi->source != NULL)
	{
	  cs = strstr(dsi->source,"Bead Cycle");
	  if (cs != NULL)
	    {
	      if (sscanf(cs,"Bead Cycle %d",&cy) == 1)
		{
		  if (cys < 0) cys = cy;
		  if (cye < 0) cye = cy;
		  cys = (cy < cys) ? cy : cys;
		  cye = (cye < cy) ? cy : cye;
		}
	    }
	  cs = strstr(dsi->source,"Acquistion");
	  if (cs != NULL)  // for compability with spelling issue
	    {
	      if (sscanf(cs,"Acquistion %d for bead %d",&acq,&i_bead) == 2)
		{
		  if (acq0 < 0) acq0 = acq;
		  if (i_bead0 < 0) i_bead0 = i_bead;
		  if ((acq0 >= 0 && acq0 != acq) || (i_bead0 >= 0 && i_bead0 != i_bead))
		    win_printf("Inconstancy in Acquisition %d -> %d\n"
			       "on in bead Nb. %d -> %d"
			       ,acq0, acq, i_bead0, i_bead);
		}
	    }
	  else cs = strstr(dsi->source,"Acquisition");
	  if (cs != NULL)
	    {
	      if (sscanf(cs,"Acquisition %d for bead %d",&acq,&i_bead) == 2)
		{
		  if (acq0 < 0) acq0 = acq;
		  if (i_bead0 < 0) i_bead0 = i_bead;
		  if ((acq0 >= 0 && acq0 != acq) || (i_bead0 >= 0 && i_bead0 != i_bead))
		    win_printf("Inconstancy in Acquisition %d -> %d\n"
			       "on in bead Nb. %d -> %d"
			       ,acq0, acq, i_bead0, i_bead);
		}
	    }

	}
      for (j = 0; j < dsi->nx && j < dsi->ny; j++)
	{
	  add_new_point_to_ds(dsti,dsi->xd[j],dsi->yd[j]);
	  zmin = ((i == 0) || (dsi->yd[j] < zmin)) ? dsi->yd[j] : zmin;
	  zmax = ((i == 0) || (dsi->yd[j] > zmax)) ? dsi->yd[j] : zmax;
	}
    }

  get_afine_param_from_op(opt, IS_Y_UNIT_SET, &ax, &dx);
  dx *= 1e9;
  nz = (int) (0.5+ (zmax - zmin)*dx*dw);
  nf = nz + 4 * wsize;

  opn = create_and_attach_one_plot(pr, nf, nf,  (insert) ? INSERT_HERE : 0);
  if (opn == NULL)	  return win_printf_OK("cannot create plot!");
  dsn = opn->dat[0];
  opn->need_to_refresh = 1;

  uns_op_2_op_by_type(opt, IS_Y_UNIT_SET, opn, IS_X_UNIT_SET);
  //set_plot_symb(dsn, "\\pt5\\di");
  set_ds_source(dsn, "window_histo w %d dx %g", wsize, dw);
  set_plot_title(opn, "window histo of %s", (op->title != NULL) ? op->title : "");
  set_plot_x_title(opn, "value");

  dsp = create_and_attach_one_ds(opn, 16, 16, 0);
  if (dsp == NULL)
    return win_printf_OK("cannot create plot!");
  alloc_data_set_x_error(dsp);
  set_ds_dot_line(dsp);
  set_plot_symb(dsp, "\\pt5\\oc");
  dsp->nx = dsp->ny = 0;
  set_ds_source(dsp, "Number of binding window_histo w %d dx %g", wsize, dw);

  dst = create_and_attach_one_ds(opt, 16, 16, 0);
  if (dst == NULL)	return win_printf_OK("cannot create plot!");
  alloc_data_set_x_error(dst);
  set_ds_dot_line(dst);
  dst->nx = dst->ny = 0;
  set_plot_symb(dst, "\\di");
  set_ds_source(dst, "Binding duration window_histo w %d dx %g", wsize, dw);

  dsh = build_histo_ds_with_gaussian_convolution(dsti, ratio*((float)wsize)/1000);
  if (dsh == NULL) 	return win_printf_OK("cannot create histogramm ds!");
  add_one_plot_data(opn, IS_DATA_SET, (void*)dsh);


  dsph = find_peaks(dsh, thres);
  if (dsph == NULL) 	return win_printf_OK("cannot create histogramm peak ds!");
  add_one_plot_data(opn, IS_DATA_SET, (void*)dsph);

  float sig, sigy;
  int np;
  for  (i = 0; i < nf; i++)
    {
      tmp = zmin + ((zmax - zmin)*(i - 2*wsize))/nz;
      for (k = 0; k < dsti->nx; k++)
	{
	  if (fabs(dx*(tmp - dsti->yd[k])) < ((float)wsize/2))
	    {
	      dsn->yd[i] += 1;
	    }
	}
      dsn->xd[i] = tmp;
    }

  for (i = 0; i < dsph->nx; i++)
    {
      for (k = 0, tmp = 0, np = 0; k < dsti->nx; k++)
	{
	  if (fabs(dx*(dsph->xd[i] - dsti->yd[k])) < ((float)wsize/2))
	    {
	      np++;
	      tmp += dsti->xd[k];
	    }
	}

      tmp /= (np > 0) ? np : 1;
      for (k = 0, sig = sigy = 0; k < dsti->nx; k++)
	{
	  if (fabs(dx*(dsph->xd[i] - dsti->yd[k])) < ((float)wsize/2))
	    {
	      sig += (dsti->xd[k] - tmp) * (dsti->xd[k] - tmp);
	      sigy += (dsti->yd[k] - dsph->xd[i]) * (dsti->yd[k] - dsph->xd[i]);
	    }
	}
      sig /= (np > 1) ? np-1 : 1;
      sig = sqrt(sig);
      sigy /= (np > 1) ? np-1 : 1;
      sigy = sqrt(sigy);
      add_new_point_with_x_error_to_ds(dsp, dsph->xd[i], sigy, np);
      add_new_point_with_x_error_to_ds(dst, tmp, sig, dsph->xd[i]);
    }
  float x, a, a0, a1;
  for (i = 0; i < dsh->nx; i++)
    {
      x = dsh->xd[i];
      for(j = 0; j < dsph->nx; j++)
	if(dsph->xd[j] >= x) break;
      j = (j > 0) ? j - 1 : j;
      k = j + 1;
      k = (k < dsph->nx) ? k : dsph->nx - 1;
      if (k == dsph->nx - 1) j = k-1;
      a0 = (dsph->yd[j] > 0) ? (dsp->yd[j]/dsph->yd[j]) : 1;
      a1 = (dsph->yd[k] > 0) ? (dsp->yd[k]/dsph->yd[k]) : 1;
      a = a0 * (dsph->xd[k] - x) + a1 * (x - dsph->xd[j]);
      a = ((dsph->xd[k] - dsph->xd[j]) > 0) ? a/(dsph->xd[k] - dsph->xd[j]) : 1;
      dsh->yd[i] *= a;
    }

  alloc_data_set_x_error(dsph);
  alloc_data_set_y_error(dsph);
  for(j = 0; j < dsph->nx; j++)
    {
      dsph->yd[j] = dsp->yd[j];
      dsph->xe[j] = dsp->xe[j];
      dsph->ye[j] = opt->ax + opt->dx * dst->xd[j];
    }

  for (i = 0; i < nf; i++)
    dsn->xd[i] = zmin + ((zmax - zmin)* (i - 2*wsize))/nz;;
  dstmp = build_data_set(dsti->nx, dsti->nx);
  if (dstmp == NULL)      return 0;
  for (j = 0, zmin = zmax = 0, sig = 0, np = 0; j < dsti->nx; j++)
    {
      for (i = 0, k = 0, tmp = dsti->yd[j] - dsp->xd[0]; i < dsp->nx; i++)
	{
	  if (fabs(dsti->yd[j] - dsp->xd[i]) < fabs(tmp))
	    {
	      k = i;
	      tmp = dsti->yd[j] - dsp->xd[i];
	    }
	}
      dstmp->yd[j] = tmp;
      dstmp->xd[j] = k;
      if (j == 0) zmin = zmax = 0;
      zmin = (tmp < zmin) ? tmp : zmin;
      zmax = (tmp > zmax) ? tmp : zmax;
      if ((hasmax > 0) && (k == dsp->nx - 1)) continue;
      sig += tmp * tmp;
      np++;
    }
  sig /= (np > 1) ? (np - 1) : 1;
  sig = sqrt(sig);
  for (i = 0, sigy = 0, j = 0; i < dstmp->nx; i++)
    {
      tmp = dstmp->yd[i];
      k = (int)dstmp->xd[i];
      if (hasmax && k == dsp->nx - 1) continue;
      if (fabs(tmp) < 3*sig)
	{
	  sigy += tmp * tmp;
	  j++;
	}
    }
  sigy /= (j > 1) ? (j -1) : 1;
  sigy = sqrt(sigy);
  free_data_set(dstmp);
  win_printf("Found %d blocking peaks with sig = %g nm\n"
	     "with %d peaks within 3\\sigma  sig_c = %g nm",np,sig*1000,j,sigy*1000);

  if (dumpcsv)
    {
      FILE *fp;
      int header_needed = 0;
      if (op->dir && op->filename)
        {
	  append_filename(cfilename, op->dir, op->filename, 1024);
	  replace_extension(cfilename, cfilename, "csv", sizeof(cfilename));
        }

      if (do_select_file(cfilename, sizeof(cfilename), "CSV-FILE", "*.csv", "File to save peaks", 0))
        {
	  return win_printf_OK("Cannot select input file");
        }

      fp = fopen(cfilename, "r");
      header_needed = (fp == NULL) ? 1 : 0;

      if (dumpcsv == 2) fp = fopen(cfilename, "a");
      else fp = fopen(cfilename, "w");

      if (fp == NULL)
        {
	  return win_printf_OK("Cannot open file:\n%s", backslash_to_slash(cfilename));
        }
      if (header_needed)
	{
	  if (keepindex)
	    {
	      fprintf(fp, "Peak_Nb\t");
	      fprintf(fp, "Bead_Nb\t");
	      if (innm)  fprintf(fp, "X(nm)\t");
	      else fprintf(fp, "X(%s)\t", (opn->x_unit != NULL) ? opn->x_unit : "");
	      fprintf(fp, "BindingNb\t");
	      if (innm)  fprintf(fp, "dX(nm)\t");
	      else fprintf(fp, "dX(%s)\t", (opn->x_unit != NULL) ? opn->x_unit : "");
	      fprintf(fp, "Duration(%s)\t",(opt->x_unit != NULL) ? opt->x_unit : "");
	      fprintf(fp, "ErrorInDur\n");
	    }
	  else
	    {
	      fprintf(fp, "Bead_Nb\t");
	      fprintf(fp, "Peak_Nb\t");
	      if (innm)  fprintf(fp, "X(nm)\t");
	      else fprintf(fp, "X(%s)\t", (opn->x_unit != NULL) ? opn->x_unit : "");
	      if (innm)  fprintf(fp, "dX(nm)\t");
	      else fprintf(fp, "dX(%s)\t", (opn->x_unit != NULL) ? opn->x_unit : "");
	      fprintf(fp, "BindingNb\t");
	      fprintf(fp, "Duration(%s)\t",(opt->x_unit != NULL) ? opt->x_unit : "");
	      fprintf(fp, "ErrorInDur\t");
	      fprintf(fp, "Trk_Nb\t");
	      fprintf(fp, "Cycle_s\t");
	      fprintf(fp, "Cycle_e\n");
	    }
	}
      else fprintf(fp, "\n");
      float pos;
      for (i = 0; i < dsp->nx && i < dst->nx; i++)
	{
	  if (keepindex)
	    {
	      fprintf(fp, "%d\t", i);
	      fprintf(fp, "%d\t", i_bead0);
	      pos = dsp->xd[i];
	      if (zeroadj) pos -= dsp->xd[0];
	      if (innm)  fprintf(fp, "%g\t", 1000*(opn->ax + opn->dx * pos));
	      else fprintf(fp, "%g\t", opn->ax + opn->dx * pos);
	      fprintf(fp, "%g\t", dsp->yd[i]);
	      if (innm)   fprintf(fp, "%g\t", 1000*(opn->ax + opn->dx * dsp->xe[i]));
	      else fprintf(fp, "%g\t", opn->ax + opn->dx * dsp->xe[i]);
	      fprintf(fp, "%g\t", opt->ax + opt->dx * dst->xd[i]);
	      pos = sqrt(dsp->yd[i]);
	      pos = (pos < 1) ? 1 : pos;
	      fprintf(fp, "%g\n", opt->ax + opt->dx * (dst->xe[i]/pos));
	    }
	  else
	    {
	      fprintf(fp, "%d\t", i_bead);
	      fprintf(fp, "%d\t", i);
	      pos = dsp->xd[i];
	      if (zeroadj) pos -= dsp->xd[0];
	      if (innm)  fprintf(fp, "%g\t", 1000*(opn->ax + opn->dx * pos));
	      else fprintf(fp, "%g\t", opn->ax + opn->dx * pos);
	      if (innm)   fprintf(fp, "%g\t", 1000*(opn->ax + opn->dx * dsp->xe[i]));
	      else fprintf(fp, "%g\t", opn->ax + opn->dx * dsp->xe[i]);
	      fprintf(fp, "%g\t", dsp->yd[i]);
	      fprintf(fp, "%g\t", opt->ax + opt->dx * dst->xd[i]);
	      pos = sqrt(dsp->yd[i]);
	      pos = (pos < 1) ? 1 : pos;
	      fprintf(fp, "%g\t", opt->ax + opt->dx * (dst->xe[i]/pos));
	      fprintf(fp, "%d\t",acq0);
	      fprintf(fp, "%d\t",cys);
	      fprintf(fp, "%d\n",cye);
	    }
	}
      fclose(fp);
    }

  if (zeroadj)
    {
      for (i = 0; i < dsh->nx; i++)
	dsh->xd[i] -= dsph->xd[0];
      for (i = dsph->nx - 1; i >= 0; i--)
	dsph->xd[i] -= dsph->xd[0];
    }
  if (fullgr == 0)
    {
      remove_data (pr, IS_ONE_PLOT, (void*)opt);
      remove_ds_from_op(opn, dsp);
      remove_ds_from_op(opn, dsn);
    }

  return refresh_plot(pr, (insert) ? curop+1 : pr->n_op - 1);
}

# endif


int do_find_bounded_histo_avg(void)
{
    int i, j;
    pltreg  *pr = NULL;
    O_p     *ops = NULL;
    d_s     *dsd = NULL, *dsi = NULL;
    static float biny = 0.003, thres = 0.5;
    static int multi_peak = 0, ph5 = 1, subph5 = 0, setval = 1;
    static float ymin = -0.02, ymax = 0.02;
    float lymin = -0.02, lymax = 0.02;
    int ph_poss[32] = {0};
    int ph_pose[32] = {0};
    int phni = 0, stph = 0, enph = 0;
    char *st = NULL;
    float **opxd = NULL;
    float **opyd = NULL;
    int *opnx = NULL;
	size_t it;
    char question[1024] = {0};

    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine performs averaging oy x and y of all data sets\n"
                             "having the same point index");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &ops, &dsi) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

	for (it = 0; it < 32; it++) ph_poss[it] = 0;
	for (it = 0; it < 32; it++) ph_pose[it] = 0;
	if (dsi->source != NULL)
        {
            st = strstr(dsi->source,"starting =");
            stph = 0;
            if (st != NULL)  sscanf(st,"starting = %d",&stph);
            enph = 0;
            st = strstr(dsi->source,"with reference =");
            if (st != NULL)  sscanf(st,"with reference = %d",&enph);
            for (st = strstr(dsi->source,"Phase["); st != NULL && st[0] != 0; st = strstr(st+1,"Phase["))
                {
                    if (sscanf(st,"Phase[%d] = [%d,%d]",&phni,&stph,&enph) == 3)
                        {
                            if (phni < 32)
                                {
                                    ph_poss[phni] = stph;
                                    ph_pose[phni] = enph;
                                }
                        }
                }
        }



    snprintf(question,sizeof(question),
             "Average all data sets of a plot at the same integer point value\n"
             "using an histogram along Y to average and peak maximum value.\n"
             "With y values bounded either by:%%R the present Y plot boundary at [%g,%g]\n"
             "or %%r set by you in [%%5f, %%5f[\n"
             "This routine assumes X values to integers in rising order\n"
             "Bin size in Y %%8f for the histogramm. Select:\n"
             "\\oc  %%R a single point corresponding to the maximum of the histogram\n"
             "\\oc  %%r all maxima greater than a threshold value\n"
             "\\oc  %%r the combination of both\n"
             "Define the treshold value %%8f\n"
             "%%b apply only on phase 5\n"
             "%%b substract average to signal of phase 5\n",ops->y_lo, ops->y_hi);
    i = win_scanf(question,&setval,&ymin,&ymax, &biny, &multi_peak, &thres,&ph5,&subph5);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }
    lymin = (setval == 0) ? ops->y_lo : ymin;
    lymax = (setval == 0) ? ops->y_hi : ymax;
    if (ph5)
        {
            opxd = (float**)calloc(ops->n_dat,sizeof(float*));
            opyd = (float**)calloc(ops->n_dat,sizeof(float*));
            opnx = (int*)calloc(ops->n_dat,sizeof(int));
            if (opxd == NULL || opyd == NULL || opnx == NULL) ph5 = 0;
            for (i = 0; ph5 > 0 && i < ops->n_dat; i++)
                {
                    dsi = ops->dat[i];
                    opxd[i] = dsi->xd;
                    opyd[i] = dsi->yd;
                    opnx[i] = dsi->nx;
                    for (it = 0; it < 32; it++) ph_poss[it] = 0;
                    for (it = 0; it < 32; it++) ph_pose[it] = 0;
                    if (dsi->source != NULL)
                        {
                            st = strstr(dsi->source,"starting =");
                            stph = 0;
                            if (st != NULL)  sscanf(st,"starting = %d",&stph);
                            enph = 0;
                            st = strstr(dsi->source,"with reference =");
                            if (st != NULL)  sscanf(st,"with reference = %d",&enph);
                            for (st = strstr(dsi->source,"Phase["); st != NULL && st[0] != 0; st = strstr(st+1,"Phase["))
                                {
                                    if (sscanf(st,"Phase[%d] = [%d,%d]",&phni,&stph,&enph) == 3)
                                        {
                                            if (phni < 32)
                                                {
                                                    ph_poss[phni] = stph;
                                                    ph_pose[phni] = enph;
                                                }
                                        }
                                }
                        }
                    if (phni >= 5)
                        {
                            dsi->xd = dsi->xd + ph_poss[5];
                            dsi->yd = dsi->yd + ph_poss[5];
                            dsi->nx = dsi->ny = 1 + ph_pose[5] - ph_poss[5];
                        }
                }
        }
    dsd = average_all_ds_of_op_by_max_of_bounded_histo(ops, biny, multi_peak, thres,lymin,lymax);
    if (ph5)
        {
            for (i = 0;i < ops->n_dat; i++)
                {
                    dsi = ops->dat[i];
                    if (subph5)
                        {
                            for (j = 0;j < dsi->nx && j < dsd->nx; j++)
                                dsi->yd[j] -= dsd->yd[j];
                        }
                    dsi->xd = opxd[i];
                    dsi->yd = opyd[i];
                    dsi->nx = dsi->ny = opnx[i];
                }
        }
    if (opxd) free(opxd);
    if (opyd) free(opyd);
    if (opnx) free(opnx);
    add_data_to_one_plot(ops, IS_DATA_SET, dsd);
    ops->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
}

#ifdef PIAS
int do_jump_fit(void)
{
    int i;
    pltreg  *pr = NULL;
    O_p     *ops = NULL;
    d_s     *dsi = NULL, *dszf = NULL;
    static float sig = 0.003, thres = 3.0;
    static int nl_filter_min = 2, nl_filter_max=64;
    static float nl_filter_p = 13.0, nl_filter_sigma = 0.0008;


    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine performs a non linear step filter\n");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &ops, &dsi) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    win_scanf("Please indicate the parameter of the filter\n"
                "Min length %6d, Max length %6d\n"
              "Weighting power %6f, sigma of data %6f\n",&nl_filter_min,&nl_filter_max,&nl_filter_p,&nl_filter_sigma);

    dszf = create_and_attach_one_ds(ops,dsi->nx,dsi->ny,0);


    jumpfit_vc_2(dsi, dszf, 1, nl_filter_min, nl_filter_max, nl_filter_sigma,nl_filter_p, NULL);
    ops->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);


}



int transform_peaks_to_points(d_s *dsi, d_s *dsjump, float lim, int two_states)
{
    float imax = 0, imin = 0, min = 0, max = 0;
    float tempimin = 0, tempimax = 0;
    float tempmax = 0, tempmin = 0;
    float tempi = 0;
    int ind_last_max_event = 0, ind_last_min_event = 0, last_event = 0, ind_last_min = 0, ind_last_max = 0;
    if (dsi == NULL || dsjump == NULL ) return(win_printf_OK("no dataset\n"));
    if (dsjump->xe == NULL) return(win_printf_OK("no allocated x error for dsjump\n"));
    dsjump->nx = dsjump -> ny =   0;
    for (int i = 0; i < dsi->nx; i++)
    {
      if (dsi->yd[i] < 0 && max != 0)   //si on a stocké un maximum et qu'on retraverse dans les négatifs -> maximum pris en compte
      {
        add_new_point_to_ds(dsjump,imax,max);
        dsjump->xe[dsjump->nx-1] = i;


        if (last_event == 1 && two_states == 1)   // si le maximum intervient après un autre maximum on calcul le minimum absolu entre les deux maw
        {
          min = 0;
          imin = 0;
          for (int j = ind_last_max_event;j<ind_last_max;j++)
          {
            if (dsi->yd[j] < min)
            {
              imin = dsi->xd[j];
              min = dsi->yd[j];
              ind_last_min_event = j;
            }
          }
          tempimin = dsjump->xd[dsjump->nx-1]; // we save the old point
          tempmin = dsjump->yd[dsjump->nx-1];
          tempi = dsjump->xe[dsjump->nx-1];

          dsjump->xe[dsjump->nx-1] = i; // we replace by the intermediary point
          dsjump->xd[dsjump->nx-1] = imin;
          dsjump->yd[dsjump->nx-1] = min;
          add_new_point_to_ds(dsjump,tempimin,tempmin); // we put the oldpoint after (to keep events in the right order)
          dsjump->xe[dsjump->nx-1] = tempi;
        }
        ind_last_max_event = ind_last_max;
        last_event = 1;
        imax = 0;
        max = 0;
        min = 0;
        imin = 0;
      }
      if (dsi->yd[i] > 0 && min != 0)
      {
        add_new_point_to_ds(dsjump,imin,min);
        dsjump->xe[dsjump->nx-1] = i;

        if (last_event == -1 && two_states == 1)
        {
          max = 0;
          imax = 0;
          for (int j = ind_last_min_event;j<ind_last_min;j++)
          {
            if (dsi->yd[j] > max)
            {
              imax = dsi->xd[j];
              max = dsi->yd[j];
              ind_last_max_event = j;
            }
          }
          tempimax = dsjump->xd[dsjump->nx-1]; // we save the old point
          tempmax = dsjump->yd[dsjump->nx-1];
          tempi = dsjump->xe[dsjump->nx-1];

          dsjump->xe[dsjump->nx-1] = i; // we replace by the intermediary point
          dsjump->xd[dsjump->nx-1] = imax;
          dsjump->yd[dsjump->nx-1] = max;
          add_new_point_to_ds(dsjump,tempimax,tempmax); // we put the oldpoint after (to keep events in the right order)
          dsjump->xe[dsjump->nx-1] = tempi;
        }
        ind_last_min_event = ind_last_min;
        last_event = -1;
        max = 0;
        imax = 0;
        imin = 0;
        min = 0;
      }
      if (dsi->yd[i] > max && dsi->yd[i] > lim)
      {
        imax = dsi->xd[i];
        max = dsi->yd[i];
        ind_last_max = i;
      }
      if (dsi->yd[i] < min && dsi->yd[i] < -lim)
      {
        imin = dsi->xd[i];
        min = dsi->yd[i];
        ind_last_min = i;
      }
    }

    return 0;

}
//
// int transform_jumps_to_lengths(d_s *dsup,d_s *dsdown, d_s *dsp, d_s *dsm)
// {
//     if (dsp == NULL || dsm == NULL || dsup == NULL || dsdown == NULL ) return(win_printf_OK("no dataset\n"));
//     int np = (dsp->nx > dsm -> nx)  ? dsp->nx : dsm->nx;
//     int begin_up =0;
//     if (dsm->xd[0] > dsp->xd[0]) begin_up = 0;
//     else begin_up = 1;
//     int i =0;
//
//     for ( i = 0; i<np-1;i++)
//     {
//       dsup->xd[i] = 0.5*dsp->xd[i] + 0.5*dsm->xd[i + begin_up];
//       dsdown->xd[i] = 0.5*dsp->xd[i+ 1 - begin_up] + 0.5*dsm->xd[i];
//       dsup->yd[i] = dsm->xd[i + begin_up] - dsp->xd[i];
//       dsdown->yd[i] = dsp->xd[i + 1 - begin_up] - dsm->xd[i];
//     }
//
//     dsup->nx = dsup->ny = dsdown->nx = dsdown->ny = i;
//
//
//     return 0;
//
// }
//
//
// int naive_two_states_finder(d_s *dsi, d_s *dsp, d_s *dsm, float lim)
// {
//   int nf = dsi->nx;
//   int last_event = 0;
//   int last_event_index = 0;
//   int min_index = 0, max_index = 0;
//   float min_jump = 0, max_jump = 0;
//   for (int i = 0; i<nf-1;i++)
//   {
//     if (dsi->yd[i+1] - dsi->yd[i] > lim)
//     {
//       if (last_event == 1)
//       {
//         for (int j =last_event_index;j<i;j++)
//         {
//           if (dsi->yd[j+1] - dsi->yd[j] < min_jump) min_jump = dsi->yd[j+1] - dsi->yd[j];
//           min_index = j;
//         }
//         add_new_point_to_ds(dsm,dsi->xd[min_index],dsi->yd[min_index+1] - dsi->yd[min_index]);
//       }
//       add_new_point_to_ds(dsp,dsi->xd[i],dsi->yd[i+1] - dsi->yd[i]);
//       last_event = 1;
//       last_event_index = i;
//     }
//     if (dsi->yd[i+1] - dsi->yd[i] < -lim)
//     {
//       if (last_event == -1)
//       {
//         for (int j =last_event_index;j<i;j++)
//         {
//           if (dsi->yd[j+1] - dsi->yd[j] > max_jump) max_jump = dsi->yd[j+1] - dsi->yd[j];
//           max_index = j;
//         }
//         add_new_point_to_ds(dsp,dsi->xd[max_index],dsi->yd[max_index+1] - dsi->yd[max_index]);
//       }
//       add_new_point_to_ds(dsm,dsi->xd[i],dsi->yd[i+1] - dsi->yd[i]);
//       last_event = -1;
//       last_event_index = i;
//     }
//   }
//
//   return 0;
// }
//
//
//
//
//
//
// int do_jump_height(void)
// {
//     int i;
//     pltreg  *pr = NULL;
//     O_p     *ops = NULL;
//     d_s     *dsi = NULL, *dszf = NULL, *dsp = NULL, *dsm = NULL, *dsup = NULL, *dsdown = NULL;
//
//
//     static int nl_filter_min = 2, nl_filter_max=64;
//     static float nl_filter_p = 13.0, nl_filter_sigma = 0.0008, lim = 0.0006;
//
//
//     if (updating_menu_state != 0)
//     {
//         return (D_O_K);
//     }
//
//     if (key[KEY_LSHIFT])
//     {
//         return win_printf_OK("This routine compute the height of jumps\n");
//     }
//
//     if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &ops, &dsi) != 3)
//     {
//         return win_printf_OK("cannot find data!");
//     }
//
//     win_scanf("Please indicate the parameter of the filter\n"
//                 "Min length %6d, Max length %6d\n"
//               "Weighting power %6f, sigma of data %6f\n"
//               "minimum value of peaks %6f\n",&nl_filter_min,&nl_filter_max,&nl_filter_p,&nl_filter_sigma,&lim);
//
//     dszf = create_and_attach_one_ds(ops,dsi->nx,dsi->ny,0);
//     dsm = create_and_attach_one_ds(ops,64,64,0);
//     dsp = create_and_attach_one_ds(ops,64,64,0);
//
//
//
//     jumpfit_height(dsi, dszf, NULL,1, nl_filter_min, nl_filter_max, nl_filter_sigma,nl_filter_p, NULL);
//     transform_peaks_to_points(dszf,dsp,dsm,lim);
//     int nf = (dsp->nx > dsm -> nx)  ? dsp->nx : dsm->nx;
//     dsup = create_and_attach_one_ds(ops,nf,nf,0);
//     dsdown = create_and_attach_one_ds(ops,nf,nf,0);
//
//     transform_jumps_to_lengths(dsup,dsdown, dsp, dsm);
//
//     ops->need_to_refresh = 1;
//     return refresh_plot(pr, UNCHANGED);
// }
//
//
// int retrieve_hybridation_time_from_scan(void)
// {
//     force_param *sph = NULL;
//     int page_n = 0, i_page = 0, ima = 0, n_avg = 0, nstep = 0, ii =0;
//     scan_info *sc_in = NULL;
//
//     float mean=0;
//     g_record *g_r;
//     pltreg  *pr = NULL;
//     O_p     *op = NULL, *optimes = NULL, *op2 = NULL;
//     d_s     *dsi = NULL, *dszf = NULL, *dsp = NULL,*dszmag = NULL, *dsm = NULL, *dsup = NULL, *dsdown = NULL, *ds=NULL, *dstimes = NULL;
//     b_record *br = NULL;
//     int jij=0;
//     static int naive = 0;
//
//
//     static int nl_filter_min = 2, nl_filter_max=64;
//     static float nl_filter_p = 13.0, nl_filter_sigma = 0.0008, lim = 0.0006;
//
//
//     if (updating_menu_state != 0)
//     {
//         return (D_O_K);
//     }
//
//
//     if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
//     {
//         return win_printf_OK("cannot find data!");
//     }
//
//     g_r = find_g_record_in_pltreg(pr);
//     sc_in = retrieve_scan_info(g_r, &nstep, WIN_CANCEL);
//     if (sc_in == NULL) return(win_printf_OK("no track info \n"));
//     sph = load_Force_param_from_trk(g_r);
//     if (sc_in == NULL) return(win_printf_OK("no force info \n"));
//     br = g_r->b_r[0];
//     dstimes = build_data_set(nstep,nstep);
//     dstimes->nx=dstimes->ny = 0;
//
//     jij = win_scanf("Please indicate the parameter of the filter\n"
//                 "Min length %6d, Max length %6d\n"
//               "Weighting power %6f, sigma of data %6f\n"
//               "minimum value of peaks %6f\n"
//               "%b Naive method (much faster - for jumps at least 8x higher than the noise)\n",&nl_filter_min,&nl_filter_max,&nl_filter_p,&nl_filter_sigma,&lim,&naive);
//
//     if (jij == WIN_CANCEL) return 0;
//
//
//
//     for (int j = 0 ; j < nstep; j++)
//     {
//
//         ima = sc_in[j].ims;
//         n_avg = sc_in[j].im_n;
//
//         op = create_and_attach_one_plot(pr, n_avg, n_avg, 0);
//         create_attach_select_x_un_to_op(op, IS_SECOND, 0
//                                         , (float)1 / g_r->Pico_param_record.camera_param.camera_frequency_in_Hz, 0, 0, "s");
//         create_attach_select_y_un_to_op(op, IS_METER, 0 , (float)1, -6, 0, "\\mu m");
//         set_plot_title(op, "Small Jumps Zmag %6.3f",sc_in[j].zmag);
//         set_plot_x_title(op,"Time");
//         set_plot_y_title(op,"Filtered data");
//         ds=op->dat[0];
//
//
//         op2 = create_and_attach_one_plot(pr, n_avg, n_avg, 0);
//         create_attach_select_x_un_to_op(op2, IS_SECOND, 0
//                                         , (float)1 / g_r->Pico_param_record.camera_param.camera_frequency_in_Hz, 0, 0, "s");
//         create_attach_select_y_un_to_op(op2, IS_SECOND, 0,(float)1 / g_r->Pico_param_record.camera_param.camera_frequency_in_Hz, 0, 0, "s");
//         set_plot_title(op2, "Times down and times up Zmag %6.3f",sc_in[j].zmag);
//         set_plot_x_title(op2,"Time");
//         set_plot_y_title(op2,"State duration");
//       //  dszmag = create_and_attach_one_ds(op,n_avg,n_avg,0);
//
//
//
//         for ( ii = ima + 1024 ; ii<ima+n_avg - 1024; ii++)
//         {
//           page_n = ii / g_r->page_size;
//           i_page = ii % g_r->page_size;
//           ds->xd[ii-ima-1024] = (g_r->timing_mode == 1) ? (g_r->imi[page_n][i_page] - g_r->imi_start) : g_r->imi[page_n][i_page];
//           ds->yd[ii-ima-1024] = g_r->z_cor * br->z[page_n][i_page];
//           //dszmag->yd[ii-ima-1024] = g_r->zmag[page_n][i_page];
//           mean +=  g_r->z_cor * br->z[page_n][i_page];
//         }
//
//         ds->nx = ds->ny = n_avg - 2048;
//         mean = mean/ds->nx;
//
//         for ( ii = ima + 1024 ; ii<ima+n_avg - 1024; ii++)
//         {
//           ds->yd[ii-ima-1024] -= mean;
//         }
//
//
//         mean =0;
//
//
//         dszf = create_and_attach_one_ds(op,ds->nx,ds->ny,0);
//         dsm = create_and_attach_one_ds(op,64,64,0);
//         dsp = create_and_attach_one_ds(op,64,64,0);
//
//         set_dot_line(dsm);
//         set_dot_line(ds);
//         set_plot_symb(dsm, "\\pt5\\oc");
//         set_dot_line(dsp);
//         set_plot_symb(dsp, "\\pt5\\oc");
//
//         if (naive == 0)
//         {
//         (jumpfit_height(ds, dszf, NULL,1, nl_filter_min, nl_filter_max, nl_filter_sigma,nl_filter_p, NULL)) ;
//         (transform_peaks_to_points(dszf,dsp,dsm,lim));
//       }
//       else   naive_two_states_finder(ds, dsp, dsm, lim);
//
//         int nf = (dsp->nx > dsm -> nx)  ? dsp->nx : dsm->nx;
//         if (nf ==0 ) continue;
//         dsup = create_and_attach_one_ds(op2,nf,nf,0);
//         dsdown = create_and_attach_one_ds(op2,nf,nf,0);
//         remove_one_plot_data(op2, IS_DATA_SET, (void *)op2->dat[0]);
//         transform_jumps_to_lengths(dsup,dsdown, dsp, dsm);
//         for (int iii = 0;iii<dsdown->nx;iii++)
//         {
//           mean+=dsdown->yd[iii]/dsdown->nx;
//         }
//         //win_printf_OK("mean of time for step %d is %f\n",j,mean/g_r->Pico_param_record.camera_param.camera_frequency_in_Hz);
//
//         add_new_point_to_ds(dstimes,sc_in[j].zmag,mean);
//
//         mean = 0;
//         op->need_to_refresh = 1;
//         refresh_plot(pr,pr->n_op - 1);
//   }
//   if (dstimes->nx != 0)
//   {
//   optimes = create_and_attach_one_plot(pr,dstimes->nx,dstimes->ny,0);
//   free_data_set(optimes->dat[0]);
//   optimes->dat[0] = dstimes;
//   set_ds_line_color(dstimes, Lightgreen);
// }
//
//   return 0;
// }
//


#endif



int do_find_signal_good_diff(void)
{
    int i;
    pltreg  *pr = NULL;
    O_p     *ops = NULL;
    d_s     *dsi = NULL;
    static float sig = 0.003, thres = 3.0;


    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine performs averaging oy x and y of all data sets\n"
                             "having the same point index");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &ops, &dsi) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    i = win_scanf("Define the \\sigma of the derivative expected %5f\n"
                  "And relative threshold %5f\n"
                  ,&sig,&thres);
    if (i == WIN_CANCEL) return 0;
    int nt =0, ng = 0;
    for (i = 1, nt = ng = 0; i < dsi->nx; i++)
        {
            if (dsi->xd[i] - dsi->xd[i-1] <= 1)
                {
                    nt++;
                    ng += (ops->dy*fabs(dsi->yd[i] - dsi->yd[i-1]) > (thres * sig)) ? 1 : 0;
                }
        }
    win_printf("%d not good /%d useful points",ng,nt);
    return 0;
}

MENU *cordrift_plot_menu(void)
{
  static MENU mn[32] = {0};

  if (mn[0].text != NULL)	return mn;
  //add_item_to_menu(mn,"Hello example", do_cordrift_hello,NULL,0,NULL);
  add_item_to_menu(mn,"Keep flat part of ds", do_cordrift_keep_flat_part_data_set,NULL,0,NULL);
  add_item_to_menu(mn, "\0",   NULL,   NULL,   0, NULL);

  add_item_to_menu(mn,"Cut and Remove high derivative part of ds NL",
		   do_cordrift_remove_high_derivative_part_of_data_set ,NULL,0,NULL);
  add_item_to_menu(mn,"Cut and Remove high derivative part of ds 2 NL",
		   do_cordrift_remove_high_derivative_part_of_data_set_2 ,NULL,0,NULL);
  add_item_to_menu(mn,"Cut and Remove high derivative part of one color ds 2 NL",
		   do_cordrift_remove_high_derivative_part_of_data_set_2_color ,NULL,0,NULL);
  add_item_to_menu(mn,"Remove high derivative part of ds NL",
		   do_cordrift_remove_high_derivative_part_of_each_data_set ,NULL,0,NULL);
       #ifdef PIAS
       add_item_to_menu(mn,"jumpfit on selected data set",do_jump_fit ,NULL,0,NULL);
       // add_item_to_menu(mn,"jump height on selected data set",do_jump_height ,NULL,0,NULL);
       // add_item_to_menu(mn,"hyb time from scan", retrieve_hybridation_time_from_scan ,NULL,0,NULL);
       #endif


  add_item_to_menu(mn, "\0",   NULL,   NULL,   0, NULL);
  add_item_to_menu(mn,"Remove simple high derivative part of ds", do_cordrift_remove_simple_high_derivative_part_of_each_data_set,NULL,0,NULL);

  add_item_to_menu(mn,"Cut and Remove simple high derivative part of ds",do_cordrift_remove_and_cut_simple_high_derivative_part_of_data_set ,NULL,0,NULL);

  add_item_to_menu(mn,"Remove high derivative part at ends of ds", do_cordrift_remove_simple_high_derivative_part_of_each_data_set_ends,NULL,0,NULL);
  add_item_to_menu(mn," Angles vs noise",do_draw_noise_vs_angles,NULL,0,NULL);

  add_item_to_menu(mn, "\0",   NULL,   NULL,   0, NULL);

  add_item_to_menu(mn,"colapse ds",do_cordrift_colapse_data_set ,NULL,0,NULL);
  add_item_to_menu(mn,"colapse in single ds",do_cordrift_colapse_data_set_single_ds,NULL,0,NULL);
  add_item_to_menu(mn,"colapse by histo of ds diff in single ds",do_cordrift_colapse_data_set_single_ds_by_histo,NULL,0,NULL);

  add_item_to_menu(mn,"make steps",do_cordrift_reproduce_decreasing_steps,NULL,0,NULL);
  add_item_to_menu(mn,"make duration",do_cordrift_reproduce_decreasing_steps_duration,NULL,0,NULL);
  add_item_to_menu(mn, "Blocking position and duration", convert_ds_pauses_in_mean_z_and_duration_2, NULL, 0, NULL);


  add_item_to_menu(mn, "Make duration and compute Blocking position",do_cordrift_reproduce_decreasing_steps_duration_and_analyze, NULL, 0, NULL);

  add_item_to_menu(mn,"test make steps",test_cordrift_reproduce_decreasing_steps,NULL,0,NULL);

  add_item_to_menu(mn,"Erase unsignificat rate",keep_significant_rate,NULL,0,NULL);
  add_item_to_menu(mn,"Keep strand switch",keep_significant_strand_switch,NULL,0,NULL);



  add_item_to_menu(mn,"Average over smooth window", do_average_over_smooth_window,NULL,0,NULL);
  add_item_to_menu(mn,"generate plot for weighted helicase rate",do_prepare_weighted_histo ,NULL,0,NULL);
  add_item_to_menu(mn,"Find noise, S and N phases",do_find_noise_S_and_N_phase   ,NULL,0,NULL);

  add_item_to_menu(mn,"Find noise",do_find_noise   ,NULL,0,NULL);
  add_item_to_menu(mn,"helicase activity",do_compute_activity,NULL,0,NULL);
  add_item_to_menu(mn,"do normalize oligo histo",do_normalize_oligo_histo,NULL,0,NULL);
  add_item_to_menu(mn,"Find bounded histo average",do_find_bounded_histo_avg,NULL,0,NULL);
  add_item_to_menu(mn,"Find good signal",do_find_signal_good_diff,NULL,0,NULL);


  return mn;
}

int	cordrift_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "cordrift", NULL, cordrift_plot_menu(), 0, NULL);
  return D_O_K;
}

int	cordrift_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "cordrift", NULL, NULL);
  return D_O_K;
}
#endif
