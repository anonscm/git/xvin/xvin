#include "core.h"
#include "gui.h"
#include "../trackBead/record.h"
#include "gitinfo.hh"
#include <cmath>
#include <boost/python.hpp>
#include <unistd.h>

extern "C"
{
    extern int     pias_argc;
    extern char ** pias_argv;
    extern int (*pias_call)(char const *);
    int call_pias_python_script(char const *cx);
}

namespace pywrapper
{
    namespace bp = boost::python;
    std::string exception()
    {
        PyObject *exc,*val,*tb;
        PyErr_Fetch(&exc,&val,&tb);
        PyErr_NormalizeException(&exc,&val,&tb);
        bp::handle<> hexc(exc), hval(bp::allow_null(val)), htb(bp::allow_null(tb));
        if(!hval)
            return bp::extract<std::string>(bp::str(hexc));
        else
        {
            bp::object traceback(bp::import("traceback"));
            bp::object format_exception(traceback.attr("format_exception"));
            bp::object formatted_list(format_exception(hexc,hval,htb));
            bp::object formatted(bp::str("").join(formatted_list));
            return bp::extract<std::string>(formatted);
        }
    }

    Results _run_cmd( std::string attr
                    , std::string cmd
                    , bp::object  obj = bp::object())
    {
        auto glob = global();

        static bool bthis = false;
        if(!bthis)
            bp::exec("import sys\nsys.path.append('./')\nsys.path.append('../')\n", glob);

        for(auto & x: cmd)
            if(x == '\\') x = '/';

        auto good = true;
        std::string out, err;
        auto fname = "run"+attr;
        try
        {
            auto mod = bp::import("pywrapper");
            auto fcn = mod.attr(fname.c_str());
            auto lst = fcn(cmd.c_str(), obj);
            out  = bp::extract<std::string> (lst[0]);
            err  = bp::extract<std::string> (lst[1]);
            good = true;
        } catch (bp::error_already_set &)
        {
            err  = exception();
            good = false;
        }

        return std::make_tuple(out, err, good);
    }

    void _do_msg(Results && res)
    {
        std::string msg;
        if(!std::get<2>(res))
            msg += "{\\color{yellow}CRASH OCCURRED!}\n";
        if(std::get<0>(res).size())
            msg += "\n{\\color{yellow}Output:}\n" + std::get<0>(res);
        if(std::get<1>(res).size())
            msg += "\n{\\color{yellow}Error:}\n" + std::get<1>(res);

        if(msg.size())
            win_printf_OK(msg.c_str());
    }

    boost::python::object global()
    { return bp::import("__main__").attr("__dict__"); }

    Results run(std::string file, bp::object obj)
    { return _run_cmd("file",   file, obj); }

    Results runmodule(std::string file, bp::object obj)
    { return _run_cmd("module", file, obj); }

    void guirunmodule(std::string file, bp::object obj)
    { _do_msg(runmodule(file, obj)); }

    void guirun(std::string file, bp::object obj)
    { _do_msg(run(file, obj)); }

    bp::object gitinfo()
    {
        bp::dict g;
        g["BRANCH"]   = git::branch ();
        g["HASHTAG"]  = git::hashtag();
        g["VERSION"]  = git::version();
        g["AUTHOR"]   = git::author ();
        g["DATE"]     = git::date   ();
        g["CLEAN"]    = git::clean  ();
        return g;
    }

    boost::python::object import(std::string name)
    {
        auto r = _run_cmd("code", "import "+name);
        bp::object mod;
        if(std::get<2>(r))
        {
            std::istringstream stream(name);
            std::string one;
            std::getline(stream, one, '.');

            mod = bp::import("sys").attr("modules")[one];
            while(std::getline(stream, one, '.'))
                mod = mod.attr(one.c_str());
        }
        return mod;
    }

    namespace
    {
        int         _dllcnt  = 0;
        wchar_t *   _program = nullptr;
        wchar_t *   _home    = nullptr;
        wchar_t *   _path    = nullptr;
    }

    void _import_numpy();
    void load   ()
    {
        ++_dllcnt;
        if(_dllcnt > 1)
            return; // not thread-safe

        pias_call = &call_pias_python_script;
#       ifdef XV_WIN32
        std::string prog = pias_argv[0];
        size_t      cnt  = 0;
        for(auto & x: prog)
            if(x == '\\')
            {
                ++cnt;
                x = '/';
            }

        _program = Py_DecodeLocale(prog.c_str(), nullptr);

        std::string home(prog);
        if(cnt >= 1)
            home.resize(home.rfind('/')+1);
        _home = Py_DecodeLocale(home.c_str(), nullptr);

        chdir(home.c_str());
        std::string path = home + "python/;" + home +"python/lib-dynload;./";
        _path = Py_DecodeLocale(path.c_str(), nullptr);
        printf("Program name: '%s'\n", prog.c_str());
        printf("PYTHONHOME: '%s'\n", home.c_str());
        printf("PYTHONPATH: '%s'\n", path.c_str());
        Py_SetProgramName(_program);
        Py_SetPythonHome (_home);
        Py_SetPath       (_path);
#       endif
        Py_Initialize();
        _import_numpy();
    }

    void unload ()
    {
        ++_dllcnt;
        if(_dllcnt > 0)
            return; // not thread-safe

        Py_Finalize();
        if(_program != nullptr) PyMem_RawFree(_program);
        if(_path != nullptr)    PyMem_RawFree(_path);
        if(_home != nullptr)    PyMem_RawFree(_home);
    }
}

int do_pywrapper_runcmd()
{
    if (updating_menu_state != 0) return D_O_K;

    char const * cx = open_one_file_config("python script", nullptr,
                        "python scripts (*.py))\0*.py\0All Files (*.*)\0*.*\0\0",
                        "PYWRAPPER", "FILE");
    return call_pias_python_script(cx);
};

extern "C"
{
    int call_pias_python_script(char const *cx)
    {
        if(cx == nullptr)
            return OFF;
        std::string x = cx;
        if(x.size() == 0)
            return OFF;
        pywrapper::guirun(x, boost::python::object());
        return D_O_K;
    }
}

PYMENU_IMPL_ALL(pywrapper,PYWRAPPER_MENUS)
