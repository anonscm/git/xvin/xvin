#pragma once
#include "xvin.h"
#include <allegro.h>
#ifdef XV_WIN32
#   include <winalleg.h>
#endif
#include "memorized_win_scan.hh"

#define PYWRAPPER_MENUS                               \
    ((false,  "run command",      do_pywrapper_runcmd))

MENU_DECLARE_ALL(pywrapper,PYWRAPPER_MENUS)
