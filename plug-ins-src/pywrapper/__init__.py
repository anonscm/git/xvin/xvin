u""" compiles and runs modules or scripts """
#!/usr/bin/env python3
from io import StringIO
import sys

def run(fcn):
    u"""runs a function and returns stdout and stderr"""
    old                    = sys.stdout, sys.stderr
    sys.stdout, sys.stderr = StringIO(), StringIO()
    try:
        fcn()
    finally:
        tmp                    = sys.stdout, sys.stderr
        sys.stdout, sys.stderr = old
    return tuple(x.getvalue() for x in tmp)

def runmodule(fname, obj):
    u""" imports and runs a module script"""
    def _fcn():
        mod = __import__(fname)
        for name in fname.split('.')[1:]:
            mod = getattr(mod, name)
        return mod.run(obj)
    return run(_fcn)

def runcode(code, obj):
    u""" compiles and runs a file script"""
    glob = globals()
    if obj is not None:
        glob.update(obj)

    # pylint: disable=locally-disabled,exec-used
    return run(lambda: exec(compile(code, "code", 'single')))

def runfile(fname, obj):
    u""" compiles and runs a file script"""
    glob = globals()
    if obj is not None:
        glob.update(obj)

    code = []
    with open(fname) as stream:
        code.append(compile(stream.read(), fname, 'exec'))

    # pylint: disable=locally-disabled,exec-used
    return run(lambda: exec(code[0], glob))
