#ifndef PYWRAPPER_CORE_HPP
#   define PYWRAPPER_CORE_HPP
#   include <type_traits>
#   include <valarray>
#   include <vector>
#   include <string>
#   include <cmath> // keep it here: compilation issue on windows with hypot
#   include <boost/optional.hpp>
#   include <boost/python.hpp>
#   include <boost/python/dict.hpp>
#   include <boost/preprocessor/seq/for_each.hpp>
#   include <boost/preprocessor/tuple/elem.hpp>
#   include <boost/preprocessor/stringize.hpp>
#   include "memorized_win_scan.hh"

#   define _PYWRAPPER_SAVE_(CF,V) , pywrapper::_item(BOOST_PP_STRINGIZE(V), CF.V)
#   define _PYWRAPPER_SAVE(_,CF,X) _PYWRAPPER_SAVE_(CF, BOOST_PP_TUPLE_ELEM(4,2,X))
#   define CONFIG_PYWRAPPER(CF,ITEMS,ARGS,FORM)                                \
    pywrapper::_topython(FORM, ITEMS BOOST_PP_SEQ_FOR_EACH(_PYWRAPPER_SAVE,CF,ARGS));

#   define _PYWRAPPER_ARGS(_,__,X)                        \
         , BOOST_PP_TUPLE_ELEM(4,1,X) BOOST_PP_TUPLE_ELEM(4,2,X) = BOOST_PP_TUPLE_ELEM(4,3,X)
#   define PYWRAPPER_ARGS(ARGS) BOOST_PP_SEQ_FOR_EACH(_PYWRAPPER_ARGS,_,ARGS)
#   define _PYWRAPPER_COPY_ARGS(_,CF,X)                   \
         cf.BOOST_PP_TUPLE_ELEM(4,2,X) = BOOST_PP_TUPLE_ELEM(4,2,X);
#   define PYWRAPPER_COPY_ARGS(CF,ARGS) BOOST_PP_SEQ_FOR_EACH(_PYWRAPPER_COPY_ARGS,CF,ARGS)

#   define _PYWRAPPER_SIG_(A,B) , bp::args(BOOST_PP_STRINGIZE(A)) = pywrapper::signature(B)
#   define _PYWRAPPER_SIG(_,__,X) \
        _PYWRAPPER_SIG_(BOOST_PP_TUPLE_ELEM(4,2,X), BOOST_PP_TUPLE_ELEM(4,3,X))
#   define PYWRAPPER_SIGNATURE(ARGS) BOOST_PP_SEQ_FOR_EACH(_PYWRAPPER_SIG,_,ARGS)
extern "C"
{
    struct data_set;
    int win_printf_OK(char const *, ...);
}

namespace pywrapper
{
    template <typename T> constexpr T signature(T x) { return x; };
    template <typename ... T>
    boost::python::object format(char const *, T && ...);

    namespace bp = boost::python;
    namespace
    {
        template <typename T>
        struct _PyItem
        {
            char const * title;
            T          & addr;
            T    const & operator*() const { return addr; }
        };

        template <typename T>
        inline _PyItem<T> _item(char const * a, T & b)
        { return _PyItem<T>({a, b}); }

        template <typename T>
        inline _PyItem<T const > _item(char const * a, T const & b)
        { return _PyItem<T const>({a, b}); }

        inline void _format(std::ostringstream & stream, std::pair<int, int> &&, char const * fmt)
        { stream << fmt; }

        template <typename T>
        inline T    _format_value(config::Item<T> const & item)  { return *item; }
        template <typename T>
        inline T    _format_value(_PyItem<T> const & item)       { return item.addr; }
        inline bool _format_value(bool const * item)             { return *item; }

        template <typename T>
        constexpr inline bool _bool()
        { return std::is_integral<
            typename std::remove_reference<decltype(*std::declval<T>())>::type
                                 >::value;
        };

        template <typename T>
        constexpr inline bool _int()
        { return std::is_integral<
            typename std::remove_reference<decltype(*std::declval<T>())>::type
                                 >::value;
        };

        template <typename T>
        inline typename std::enable_if< _bool<T>(), bool>::type _is_true(T const & x)
        { return bool(*x) == true; }

        template <typename T>
        inline typename std::enable_if<!_bool<T>(), bool>::type _is_true(T const &)
        { return false; }

        template <typename T>
        inline typename std::enable_if< _int<T>(), int>::type _get_int(T const & x)
        { return int(*x); }

        template <typename T>
        inline typename std::enable_if<!_int<T>(), int>::type _get_int(T const &)
        { return -1; }

        template <typename T0, typename ... T>
        void _format(std::ostringstream & stream, std::pair<int, int>  && radio,
                     char const * fmt, T0 && t0, T && ... t)
        {
            size_t i = 0;
            for(size_t e = strlen(fmt); i < e; ++i)
            {
                if(i == e-1 || fmt[i] != '%')
                    stream << fmt[i];
                else
                {
                    ++i;
                    if(fmt[i] == '%')
                    {
                        stream << '%';
                        continue;
                    }

                    bool found = false;
                    for(; !found && i < e; ++i)
                    {
                        switch(fmt[i])
                        {
                            case 'd':
                            case 'f':
                            case 's':
                                found = true;
                                stream << _format_value(t0);
                                break;
                            case 'b':
                                if(_is_true(t0))
                                    stream << "[X]";
                                else
                                    stream << "[ ]";
                                found = true;
                                break;
                            case 'R':
                                radio.first  = 0;
                                radio.second = _get_int(t0);
                            case 'r':
                                if(radio.second == radio.first)
                                    stream << "(*)";
                                else
                                    stream << "( )";
                            case 'q':
                            case 'Q':
                                found = true;
                            default:
                                break;
                        }
                    }
                    if(found)
                    {
                        ++radio.first;
                        return _format(stream, std::move(radio), fmt+i, std::forward<T>(t)...);
                    }
                }
            }
        }

        template <typename ...Args>
        inline void _topython( char const *           form
                             , bp::object           & pyc
                             , _PyItem<Args> && ...   args)
        {
            bp::dict dic;
            auto set = [&](auto const & a) { dic[a.title] = a.addr; return 0; };
            [](...){}(set(args)...);

            dic["help"] = format(form, args...);
            pyc.attr("update")(dic);
        }
    }

    template <typename T>
    inline
    typename std::enable_if<std::is_arithmetic<T>::value, bp::object>::type
    topython(std::pair<T,T> const & out)
    { return bp::make_tuple(out.first, out.second); }

    template <typename T0, typename T1>
    auto topython(T0 const & v, T1 const & fcn)
    -> decltype(fcn(*std::begin(v)), bp::object())
    {
        bp::object r((bp::detail::new_reference)::PyTuple_New(v.size()));
        decltype(v.size()) i = 0;
        for(auto & x: v)
        {
            bp::object obj = fcn(x);
            bp::incref(obj.ptr());
            PyTuple_SET_ITEM(r.ptr(), i++, obj.ptr());
        }
        return r;
    };

    template <typename T0, typename T1, typename T2>
    auto topython(T0 const & v, T1 const & fcn, T2 & args)
    -> decltype(fcn(*std::begin(v), args), bp::object())
    {
        bp::object r((bp::detail::new_reference)::PyTuple_New(v.size()));
        decltype(v.size()) i = 0;
        for(auto & x: v)
        {
            bp::object obj = fcn(x, args);
            bp::incref(obj.ptr());
            PyTuple_SET_ITEM(r.ptr(), i++, obj.ptr());
        }
        return r;
    };

    template <typename T>
    auto topython(std::vector<T> const & out)
    -> decltype( topython(out[0])
               , std::enable_if<!std::is_arithmetic<T>{}, void>{}
               , bp::object())
    { return topython(out, [](T const & x) { return topython(x); }); }

    template <typename T>
    auto topython(T const & v)
    -> decltype(bp::object(*std::begin(v)))
    {
        bp::object r((bp::detail::new_reference)::PyTuple_New(v.size()));
        decltype(v.size()) i = 0;
        for(auto it = std::begin(v), e = std::end(v); it != e; ++it)
        {
            bp::object obj(*it);
            bp::incref(obj.ptr());
            PyTuple_SET_ITEM(r.ptr(), i++, obj.ptr());
        }
        return r;
    };

    template<typename T>
    inline auto topython(boost::optional<T> const &o)
    -> decltype(topython(*o), bp::object())
    {
        if(o)
            return bp::object(topython(*o));
        return {};
    }

    template<typename T>
    inline
    typename std::enable_if<std::is_arithmetic<T>::value, bp::object>::type
    topython(T const &o)
    { return bp::object(o); }

    template <typename ... T>
    inline boost::python::object format(char const * fmt, T && ... args)
    {
        std::ostringstream stream;
        _format(stream, std::make_pair(0, -1), fmt, args...);
        return boost::python::str(stream.str());
    }

    // create numpy vector (memory copy): see module_ds.cc for code
    bp::handle<>        topython(unsigned char *, size_t, size_t);
    bp::handle<>        topython(std::vector<unsigned char>&, size_t, size_t);
    bp::handle<>        topython(std::vector<float>  const &o);
    bp::handle<>        topython(std::vector<double> const &o);
    bp::handle<>        topython(std::vector<size_t> const &o);
    bp::handle<>        topython(std::vector<int>    const &o);
    bp::handle<>        topython(std::vector<bool>   const &o);
    bp::handle<>        topython(std::valarray<float>  const &o);
    bp::handle<>        topython(std::valarray<double> const &o);
    bp::handle<>        topython(std::valarray<size_t> const &o);
    bp::handle<>        topython(std::valarray<int>    const &o);

    void extract_vector(bp::object o, std::vector<float>  &);
    void extract_vector(bp::object o, std::vector<double> &);
    void extract_vector(bp::object o, std::vector<size_t> &);
    void extract_vector(bp::object o, std::vector<int>    &);
    void extract_vector(bp::object o, std::vector<bool>   &);
    void extract_vector(bp::object o, std::vector<std::string> &);
    void extract_vector(bp::object o, std::valarray<float>  &);
    void extract_vector(bp::object o, std::valarray<double> &);
    void extract_vector(bp::object o, std::valarray<size_t> &);
    void extract_vector(bp::object o, std::valarray<int>    &);
    void extract_vector(bp::object o, std::vector<data_set const *>  &);
    void extract_vector(bp::object o, std::vector<data_set *>  &);


    namespace setters
    {
    }

    template <typename T0, typename T1>
    class FieldToPy : public bp::def_visitor<FieldToPy<T0,T1>>
    {
        public:
        FieldToPy(char const *n, T1 T0::*addr)
            : _name(n), _getter(addr), _setter(addr) {}
        FieldToPy(FieldToPy<T0,T1> const &) = default;

        friend class def_visitor_access;

        struct Getter
        {
            Getter(T1 T0::* x) : _addr(x) {}
            bp::object operator()(T0 const * self) const
            { return _call(self->*_addr); }

            private:
                template <typename T>
                static bp::object _call(boost::optional<T> const & x)
                {
                    if(x)
                    {
                        auto const & z = *x;
                        return _call(z);
                    }
                    return {};
                }

                template <typename T>
                static bp::object _call(std::vector<T> const & vect)
                { return bp::object(topython(vect)); }

                template <typename T>
                static bp::object _call(std::valarray<T> const & vect)
                { return bp::object(topython(vect)); }

                template <typename T>
                static bp::object _call(std::pair<T, T> const & vect)
                { return bp::object(topython(vect)); }

                template <typename T>
                static typename std::enable_if<std::is_arithmetic<T>::value, bp::object>::type
                _call(T vect)  { return bp::object(vect); }

                T1 T0::* _addr;
        };

        struct Setter
        {
            Setter(T1  T0::* x) : _addr(x) {}
            void operator()(T0 * self, bp::object obj)
            { _call(self->*_addr, obj); }

            private:
                T1 T0::* _addr;

                template <typename T>
                static void _call(std::vector<T> & vect, bp::object obj)
                { extract_vector(obj, vect); }

                template <typename T>
                static void _call(std::valarray<T> & vect, bp::object obj)
                { extract_vector(obj, vect); }

                template <typename T>
                static void _call(boost::optional<T> & vect, bp::object obj)
                {
                    if(obj.is_none())
                        vect = boost::optional<T>();
                    else
                    {
                        T tmp;
                        _call(tmp, obj);
                        vect = std::move(tmp);
                    }
                }

                template <typename T>
                static void _call(std::pair<T,T> & vect, bp::object obj)
                {
                    _call(vect.first, bp::object(obj[0]));
                    _call(vect.second, bp::object(obj[1]));
                }

                template <typename T>
                static typename std::enable_if<std::is_arithmetic<T>::value, void>::type
                _call(T & vect, bp::object obj)
                { vect = bp::extract<T>(obj); }

                static void
                _call(std::string & vect, bp::object obj)
                { vect = bp::extract<std::string>(obj); }
        };

        template <class classT>
        void visit(classT& c) const
        {
            bp::default_call_policies cp;
            boost::mpl::vector<bp::object, const T0*>             gsig;
            boost::mpl::vector<void,             T0*, bp::object> ssig;
            c.add_property(_name, bp::make_function(_getter, cp, gsig)
                                , bp::make_function(_setter, cp, ssig));
        }

        char const *    _name;
        Getter          _getter;
        Setter          _setter;
    };

    template <typename T0, typename T1>
    auto fieldtopy(char const *n, T1 T0::*a) { return FieldToPy<T0,T1>(n, a); }

    template <typename T0, typename T1>
    class EnumFieldToPy : public bp::def_visitor<EnumFieldToPy<T0,T1>>
    {
        public:
        EnumFieldToPy(char const *n, char const **o,  T1 T0::*addr)
            : _name(n), _getter(o, addr), _setter(o, addr) {}
        EnumFieldToPy(EnumFieldToPy<T0,T1> const &) = default;

        friend class def_visitor_access;

        struct Getter
        {
            Getter(char const ** o, T1 T0::* x) : _addr(x), _names(o) {}
            bp::object operator()(T0 const * self) const
            { return _call(self->*_addr, _names); }

            private:
                template <typename T>
                static bp::object _call(boost::optional<T> const & x, char const **n)
                {
                    if(!x) return {};
                    return bp::object(n[*x]);
                }

                template <typename T>
                static bp::object _call(T & x, char const ** n)
                { return bp::object(n[x]); }

                T1 T0::*        _addr;
                char const **   _names;
        };

        struct Setter
        {
            Setter(char const ** o, T1 T0::* x) : _addr(x), _names(o) {}
            void operator()(T0 * self, bp::object obj)
            {
                for(char const ** n = _names; n[0] != nullptr; ++n)
                    if(obj == n[0])
                    {
                        _call(self->*_addr, n - _names);
                        return;
                    }
                _call(self->*_addr);
            }

            private:
                template<typename T>
                static void _call(boost::optional<T> & x) { x = boost::optional<T>(); }

                template<typename T, typename K>
                static void _call(boost::optional<T> & x, K y)
                {
                    K tmp;
                    _call(tmp, y);
                    x = T(tmp);
                }

                template<typename T, typename K>
                static void _call(T & x, K y) { x = T(y); }

                template<typename T>
                static void _call(T & x) {}

                T1 T0::*        _addr;
                char const **   _names;
        };

        template <class classT>
        void visit(classT& c) const
        {
            bp::default_call_policies cp;
            boost::mpl::vector<bp::object, const T0*>             gsig;
            boost::mpl::vector<void,             T0*, bp::object> ssig;
            c.add_property(_name, bp::make_function(_getter, cp, gsig)
                                , bp::make_function(_setter, cp, ssig));
        }

        char const *    _name;
        Getter          _getter;
        Setter          _setter;
    };

    template <typename T0, typename T1>
    inline auto fieldtopy(char const * name, char const **n, T1 T0::* a)
    { return EnumFieldToPy<T0,T1>(name, n, a); }

    void get_opt(float  &, char const *, bp::dict);
    void get_opt(double &, char const *, bp::dict);
    void get_opt(size_t &, char const *, bp::dict);
    void get_opt(int    &, char const *, bp::dict);
    void get_opt(std::valarray<size_t> &, char const *, bp::dict);

    std::string exception();
    template <typename T>
    inline std::string runandcatch(T && fcn)
    {
        std::string msg;
        try                             { fcn(); }
        catch (bp::error_already_set &) { msg = exception(); }
        return msg;
    }

    template <typename T>
    inline bool runandprinterr(T && fcn)
    {
        try { fcn(); }
        catch (bp::error_already_set &)
        {
            std::string msg = "{\\color{yellow}CRASH OCCURRED!}\n"+exception();
            win_printf_OK(msg.c_str());
            return true;
        }
        return false;
    }
}
#endif
