#ifdef WORK_IN_PROGRESS_DONT_COMPILE_YET
#include "module.hpp"

namespace pywrapper
{
    struct ConstArgs
    {
        char const * fmt;
        bp::object   lst;
    };

    struct CurArgs
    {
        char const * fmt;
        size_t       ind;

        std::vector<bool> qitems;
        std::vector<bool> ritems;
    };

    struct Q_t {}; struct q_t {}; struct R_t{}; struct r_t {};

    template <typename T>
    struct _tuple
    {
        std::string t1, t2;
        T           val;
    };

    template <>
    struct _tuple<bool>
    {
        std::string t1, t2;
        int         val;
    };

    struct _qtuple
    {
        _qtuple(std::string a, std::string b, std::vector<int> & v, int i)
            : t1(a), t2(b), val(v), ind(i)
        {}
        _qtuple(_qtuple && a)
            : t1(a.t1), t2(a.t2), val(a.val), ind(a.ind)

        std::string          t1, t2;
        std::vector<int>  & val;
        int                  ind;
    };

    template <typename T>
    _tuple<T> _extract(bp::object obj)
    {
        bp::extract<T> get(obj);
        if(get.check())
            return {"", "", get()};

        std::string t1  = bp::extract<std::string>(obj[0]);
        std::string t2  = bp::extract<std::string>(obj[1]);
        T           val = bp::extract<T>          (obj[2]);
        config::get(t1, t2, val);
        return {t1, t2, val};
    }

    template <typename T>
    auto _extract(ConstArgs const & cf, CurArgs & cur)
    -> decltype(std::is_arithmetic<T>::value, _tuple<T>{"", "", T(0)})
    { return _extract(cf.lst[cur.ind++]); }

    template <typename T>
    auto  _pointer (_tuple<T> & x) { return &x.val; }
    int * _pointer (_qtuple   & x) { return &x.vect[x.ind]; }

    template <typename T>
    T _topython(_tuple<T> & x, bool s)
    {
        if(s)
            config::set(x.t1, x.t2, x.val);
        return x.val;
    }

    template <typename T>
    bool _can_topython(T  & x)  { return true; }
    template <>
    bool _can_topython<q_t>(T  & x)  { return false; }
    template <>
    bool _can_topython<r_t>(T  & x)  { return false; }

    bp::object _topython(_qtuple & x, bool s)
    {
        if(s)
            config::set(x.t1, x.t2, x.vect);
        std::vector<bool> tt(x.vect);
        return bp::make_tuple(tt);
    }

    template <>
    _qtuple _extract<Q_t>(ConstArgs const & cf, CurArgs & cur,
                          std::vector<int> & vect, int & ind)
    {
        auto        obj = cf.lst[cur.ind];
        std::string t1  = "";
        std::string t2  = "";

        bp::extract<std::string> get(obj[0]);
        if(get.check())
        {
            t1 = bp::extract<std::string>(obj[0]);
            t2 = bp::extract<std::string>(obj[1]);
            for(bp::stl_input_iterator<int> it(obj[2]), e; it != e; ++it)
                vect.push_back(*it);
            config::get(t1, t2, vect);
        }
        else
            for(bp::stl_input_iterator<int> it(obj), e; it != e; ++it)
                vect.push_back(*it);
        return _qtuple(t1, t2, vect, ind++);
    }

    template <>
    _qtuple _extract<Q_t>(ConstArgs const & cf, CurArgs & cur)
    { return _extract(cf, cur, cur.qitems, cur.qind); }

    template <>
    _qtuple _extract<q_t>(ConstArgs const & cf, CurArgs & cur)
    { return _qtuple("", "", cur.qitems, cur.qind++); }

    template <>
    _qtuple _extract<R_t>(ConstArgs const & cf, CurArgs & cur)
    { return _extract(cf, cur, cur.ritems, cur.rind); }

    template <>
    _qtuple _extract<r_t>(ConstArgs const & cf, CurArgs & cur)
    { return _qtuple("", "", cur.ritems, cur.rind++); }

    template <typename T, typename ... Args>
    bp::object scan(  ConstArgs const & cf
                    , CurArgs         & cur
                    , typename _policy<Args>::pointer_t ... args)
    {
        auto mem = _extract<T>(cf, cur);

        for (char const *& p = cur.cur; *p; p++)
        {
            if (*p == '\n')
                continue;
            else if (*p != '%')
                continue;

            char const * bp = nullptr;
            strtol(p + 1, &bp, 10);
            if (n_format != 0 && bp != NULL)
                p =  bp - 1;

            switch (*++p)
            {
#               define ONE_CASE(C,T)                                        \
                    case C:                                                 \
                        return scan<T, decltype((mem)), Args ...>           \
                                (cf, cur, mem, args ...);                   \
                        break;
                ONE_CASE('d', int)
                ONE_CASE('s', std::string)
                ONE_CASE('f', float)
                ONE_CASE('b', bool)
                ONE_CASE('Q', Q_t)
                ONE_CASE('q', q_t)
                ONE_CASE('R', R_t)
                ONE_CASE('r', r_t)
                case 'l':
                    ++p;
                    switch(p[0])
                    {
                        ONE_CASE('s', std::string)
                        ONE_CASE('f', double)
                    }
                    break;
                case 'm': break; // TODO NOT IMPLEMENTED
                case 't': break; // nothing to do
                default:  break; /* hoops    */
            }
        }

        if(win_scanf(fmt, _pointer(args) ...) != 0)
            return _make_tuple(_topython(args, true)...);
        else
            return _make_tuple(_topython(args, false)...);
    }
}
#endif
