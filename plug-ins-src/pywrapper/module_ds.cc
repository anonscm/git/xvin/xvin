#include "module_ds.h"
#include "module.hpp"
#include "../trackBead/indexing.h"
#define  NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/ndarraytypes.h>
#include <numpy/ndarrayobject.h>
namespace pywrapper
{
    namespace
    {
        template <typename T>
        decltype(NPY_FLOAT) _get_np();

        template <>
        constexpr inline decltype(NPY_FLOAT) _get_np<unsigned char>() { return  NPY_UBYTE; }
        template <>
        constexpr inline decltype(NPY_FLOAT) _get_np<size_t>()    { return  NPY_ULONG; }
        template <>
        constexpr inline decltype(NPY_FLOAT) _get_np<float>()     { return  NPY_FLOAT; }
        template <>
        constexpr inline decltype(NPY_FLOAT) _get_np<double>()    { return  NPY_DOUBLE; }
        template <>
        constexpr inline decltype(NPY_FLOAT) _get_np<int>()       { return  NPY_INT; }
        template <>
        constexpr inline decltype(NPY_FLOAT) _get_np<long>()      { return NPY_LONG; }
        template <>
        constexpr inline decltype(NPY_FLOAT) _get_np<long long>() { return NPY_LONGLONG; }
        template <>
        constexpr inline decltype(NPY_FLOAT) _get_np<bool>() { return NPY_BOOL; }

        template <typename T>
        inline bool _extract_from_np(bp::object obj, int & sz, float * dt)
        {
            if(!PyArray_CheckExact(obj.ptr()))
                return false;

            PyArrayObject * arr  = (PyArrayObject*) obj.ptr();
            constexpr const auto npy = _get_np<T>();
            if(PyArray_TYPE(arr) != npy)
                return false;

            npy_intp        size = PyArray_SIZE(arr);
            T             * dptr = (T*) PyArray_DATA(arr);
            sz                   = int(size);
            for(npy_intp k = 0;  k < size; ++k)
                dt[k] = dptr[k];
            return true;
        }

        template <typename T>
        inline bool _extract_from_scalar(bp::object obj, int sz, float* dt)
        {
            bp::extract<T> flt(obj);
            if(flt.check())
            {
                float x = (float) flt();
                for(int i = 0; i < sz; ++i)
                    dt[i] = x;
                return true;
            }
            return false;
        }

        template <typename T>
        inline bool _extract_from_stl(bp::object obj, int & sz, float *dt)
        {
            sz = 0;
            bp::extract<T> tpe(obj);
            if(tpe.check())
                dt[sz++] = tpe();
            else
                for(bp::stl_input_iterator<T> cit(obj), e; cit != e; ++cit)
                    dt[sz++] = *cit;
            return true;
        }

        template <typename T>
        inline void _extract_from_stl(bp::object obj, std::vector<T> & res)
        {
            res.resize(0);

            bp::extract<T> tpe(obj);
            if(tpe.check())
                res.push_back(tpe());
            else
                for(bp::stl_input_iterator<T> cit(obj), e; cit != e; ++cit)
                    res.push_back(*cit);
        }

        template <typename T>
        inline void _extract_from_stl(bp::object obj, std::valarray<T> & res)
        {
            std::vector<T> tmp;
            _extract_from_stl(obj, tmp);

            res.resize(tmp.size());
            for(size_t i = 0; i < tmp.size(); ++i)
                res[i] = tmp[i];
        }

        template <typename K, typename R, typename T = typename R::value_type>
        inline bool _extract_from_np(bp::object obj, R & res)
        {
            constexpr auto npy = _get_np<T>();
            PyArrayObject *  arr = (PyArrayObject*) obj.ptr();
            if(PyArray_TYPE(arr) != npy)
                return false;

            res.resize(PyArray_SIZE(arr));

            T  * dptr = (T*) PyArray_DATA(arr);
            for(npy_intp k = 0, e = npy_intp(res.size());  k < e; ++k)
                res[k] = K(dptr[k]);
            return true;
        }


        template <typename R, typename T = typename R::value_type>
        inline bool _extract_any_from_np(bp::object iter, R & res)
        {
            if(PyArray_CheckExact(iter.ptr()))
                return _extract_from_np<float>(iter, res)
                    || _extract_from_np<double>(iter, res)
                    || _extract_from_np<int>(iter, res)
                    || _extract_from_np<long>(iter, res)
                    || _extract_from_np<long long>(iter, res)
                    || _extract_from_np<size_t>(iter, res)
                    ;
            return false;
        }

        template <typename R>
        inline void _extract(bp::object iter, R & res)
        {
            if(!_extract_any_from_np(iter, res))
                _extract_from_stl(iter, res);
        }

        template <typename T>
        inline void _get_opt(T & a, char const * b, bp::dict c)
        {
            bp::str s(b);
            if(c.has_key(s))
                a = bp::extract<T>(c[s]);
        }

        template <typename T>
        inline void _get_opt(std::valarray<T> & a, char const * b, bp::dict c)
        {
            bp::str s(b);
            if(c.has_key(s))
            {
                bp::object arg = c[s];
                _extract(arg, a);
            }
        }

        template <typename T>
        bp::handle<> _vector_topy(T const & o)
        {
            using V = typename T::value_type;
            constexpr auto npy = _get_np<V>();
            npy_intp     dim[1] = {npy_intp(o.size())};
            PyObject *   obj    = PyArray_SimpleNew(1, dim, npy);
            bp::handle<> hd(obj);

            V * dt = (V*) PyArray_DATA((PyArrayObject*) obj);
            for(size_t i = 0; i < o.size(); ++i)
                dt[i] = o[i];

            return hd;
        }

        template <typename T, typename ... D>
        bp::handle<> _vector_topy(T const & o, D ... d)
        {
            using V = typename T::value_type;
            constexpr auto npy  = _get_np<V>();
            constexpr auto ndim = sizeof...(D);
            npy_intp     dim[ndim] = { npy_intp(d) ... };
            PyObject *   obj       = PyArray_SimpleNew(ndim, dim, npy);
            bp::handle<> hd(obj);

            V * dt = (V*) PyArray_DATA((PyArrayObject*) obj);
            for(size_t i = 0; i < o.size(); ++i)
                dt[i] = o[i];

            return hd;
        }

        template <typename T, typename ... D>
        bp::handle<> _vector_topy(T * o, D ... d)
        {
            constexpr auto npy  = _get_np<T>();
            constexpr auto ndim = sizeof...(D);
            npy_intp     dim[ndim] = { npy_intp(d) ... };
            PyObject *   obj       = PyArray_SimpleNewFromData(ndim, dim, npy, o);
            return bp::handle<>(obj);
        }
    }

    void extract_vector(bp::object o, std::vector<float>  & v) { _extract(o, v); }
    void extract_vector(bp::object o, std::vector<double> & v) { _extract(o, v); }
    void extract_vector(bp::object o, std::vector<size_t> & v) { _extract(o, v); }
    void extract_vector(bp::object o, std::vector<int>    & v) { _extract(o, v); }
    void extract_vector(bp::object o, std::vector<bool>   & v) { _extract_from_stl(o, v); }
    void extract_vector(bp::object o, std::vector<std::string>    & v) { _extract_from_stl(o, v); }
    void extract_vector(bp::object o, std::valarray<float>  & v) { _extract(o, v); }
    void extract_vector(bp::object o, std::valarray<double> & v) { _extract(o, v); }
    void extract_vector(bp::object o, std::valarray<size_t> & v) { _extract(o, v); }
    void extract_vector(bp::object o, std::valarray<int>    & v) { _extract(o, v); }
    void extract_vector(bp::object o, std::vector<data_set const *>  &v)
    {
        v.resize(0);
        for(bp::stl_input_iterator<data_set*> it(o), end; it != end; ++it)
            v.push_back(*it);
    }
    void extract_vector(bp::object o, std::vector<data_set *>  &v)
    {
        v.resize(0);
        for(bp::stl_input_iterator<data_set*> it(o), end; it != end; ++it)
            v.push_back(*it);
    }

    bp::handle<>        topython(unsigned char * v, size_t d1, size_t d2)
    { return _vector_topy(v, d1, d2); }
    bp::handle<>        topython(std::vector<unsigned char>  & v, size_t d1, size_t d2)
    { return _vector_topy(v, d1, d2); }
    bp::handle<>        topython(std::vector<float>  const &o) { return _vector_topy(o); }
    bp::handle<>        topython(std::vector<double> const &o) { return _vector_topy(o); }
    bp::handle<>        topython(std::vector<size_t> const &o) { return _vector_topy(o); }
    bp::handle<>        topython(std::vector<int>    const &o) { return _vector_topy(o); }
    bp::handle<>        topython(std::vector<bool>   const &o) { return _vector_topy(o); }
    bp::handle<>        topython(std::valarray<float>  const &o) { return _vector_topy(o); }
    bp::handle<>        topython(std::valarray<double> const &o) { return _vector_topy(o); }
    bp::handle<>        topython(std::valarray<size_t> const &o) { return _vector_topy(o); }
    bp::handle<>        topython(std::valarray<int>    const &o) { return _vector_topy(o); }

    void get_opt(float  & a, char const * b, bp::dict c) { _get_opt(a, b, c); }
    void get_opt(double & a, char const * b, bp::dict c) { _get_opt(a, b, c); }
    void get_opt(size_t & a, char const * b, bp::dict c) { _get_opt(a, b, c); }
    void get_opt(int    & a, char const * b, bp::dict c) { _get_opt(a, b, c); }
    void get_opt(std::valarray<size_t> & a, char const * b, bp::dict c)  { _get_opt(a, b, c); }

    _DSHandle::~_DSHandle()
    {
        if(_ptr != nullptr && _isowner)
            free_data_set(_ptr);
    }

    bp::handle<> _DSHandle::getdata(int self::*i, float * self::*x)
    {
        typedef bp::handle<> bh;
        npy_intp dim[1] = {0};
        float * info   = nullptr;
        if(good())
        {
            dim [0] = _ptr->*i;
            info    = _ptr->*x;
        }

        if(dim[0] == 0 || info == nullptr)
            return  bh(PyArray_SimpleNew(1, dim, NPY_FLOAT));
        return bh(PyArray_SimpleNewFromData(1, dim, NPY_FLOAT, info));
    }

    bp::handle<> _DSHandle::setdata(int self::*i, float * self::*x,
                                    bp::object const & iter)
    {
        if(good())
        {
            float * dt   = _ptr->*x;
            int &   sz   = _ptr->*i;
                 _extract_from_np    <float>     (iter, sz, dt)
            ||   _extract_from_np    <double>    (iter, sz, dt)
            ||   _extract_from_np    <int>       (iter, sz, dt)
            ||   _extract_from_np    <long>      (iter, sz, dt)
            ||   _extract_from_np    <long long> (iter, sz, dt)
            ||   _extract_from_np    <bool>      (iter, sz, dt)
            ||   _extract_from_scalar<float>     (iter, sz, dt)
            ||   _extract_from_scalar<double>    (iter, sz, dt)
            ||   _extract_from_scalar<int>       (iter, sz, dt)
            ||   _extract_from_scalar<long>      (iter, sz, dt)
            ||   _extract_from_scalar<long long> (iter, sz, dt)
            ||   _extract_from_scalar<bool>      (iter, sz, dt)
            ||   _extract_from_stl   <float>     (iter, sz, dt)
            ;
        }
        return getdata(i, x);
    }

    std::string _DSHandle::setls(bp::object tt)
    {
        if(good())
        {
            std::string t = bp::extract<std::string>
                                (bp::str(tt).upper().strip());
            if(t == "PLAIN")
                set_ds_plain_line(_ptr);
            else if(t == "DASH")
                set_ds_dash_line(_ptr);
            else if(t == "DOT")
                set_ds_dot_line(_ptr);
        }
        return getls();
    }

    std::string _DSHandle::getls()
    {
        if(bad())   return "";
        else if(_ptr->m == 0) return "DASH";
        else if(_ptr->m == 1) return "PLAIN";
        else if(_ptr->m == 2) return "DOT";
        return "";
    }

    std::string _DSHandle::getcolor()
    {
        if(good())
        {
            if     (_ptr->color == Black)        return "BLACK";
            else if(_ptr->color == Blue)         return "BLUE";
            else if(_ptr->color == Green)        return "GREEN";
            else if(_ptr->color == Cyan)         return "CYAN";
            else if(_ptr->color == Red)          return "RED";
            else if(_ptr->color == Magenta)      return "MAGENTA";
            else if(_ptr->color == Brown)        return "BROWN";
            else if(_ptr->color == Lightgray)    return "LIGHTGRAY";
            else if(_ptr->color == Darkgray)     return "DARKGRAY";
            else if(_ptr->color == Lightblue)    return "LIGHTBLUE";
            else if(_ptr->color == Lightgreen)   return "LIGHTGREEN";
            else if(_ptr->color == Lightcyan)    return "LIGHTCYAN";
            else if(_ptr->color == Lightred)     return "LIGHTRED";
            else if(_ptr->color == Lightmagenta) return "LIGHTMAGENTA";
            else if(_ptr->color == Yellow)       return "YELLOW";
            else if(_ptr->color == White)        return "WHITE";
        }
        return "";
    }

    std::string _DSHandle::setcolor(bp::object t)
    {
        if(good())
        {
            std::string x = bp::extract<std::string>
                                (bp::str(t).upper().strip());

            if     (x == "BLACK")        _ptr->color = Black;
            else if(x == "BLUE")         _ptr->color = Blue;
            else if(x == "GREEN")        _ptr->color = Green;
            else if(x == "CYAN")         _ptr->color = Cyan;
            else if(x == "RED")          _ptr->color = Red;
            else if(x == "MAGENTA")      _ptr->color = Magenta;
            else if(x == "BROWN")        _ptr->color = Brown;
            else if(x == "LIGHTGRAY")    _ptr->color = Lightgray;
            else if(x == "DARKGRAY")     _ptr->color = Darkgray;
            else if(x == "LIGHTBLUE")    _ptr->color = Lightblue;
            else if(x == "LIGHTGREEN")   _ptr->color = Lightgreen;
            else if(x == "LIGHTCYAN")    _ptr->color = Lightcyan;
            else if(x == "LIGHTRED")     _ptr->color = Lightred;
            else if(x == "LIGHTMAGENTA") _ptr->color = Lightmagenta;
            else if(x == "YELLOW")       _ptr->color = Yellow;
            else if(x == "WHITE")        _ptr->color = White;
        }
        return getcolor();
    }

    bp::object _DSHandle::str() const
    {
        char tmp[1024];
        snprintf(tmp, sizeof(tmp),
                 "DataSet <%p> data length is (%d,%d)",
                 this, this->_ptr->nx, this->_ptr->ny);
        return bp::str(tmp);
    };

    bp::object  _cycle_id(d_s const *cycle_ds)
    {
        auto res = indexing::cycleid(cycle_ds);
        if(res)
            return bp::object(res.value());
        else
            return {};
    }

    template <> inline constexpr char const * _str<data_set>  () { return "%ds"; }

    bp::object  _string_rep(bp::object const self)
    {
        bp::object pylen = self.attr("__len__")();
        bp::object pycls = self.attr("__class__").attr("__name__");
        int         len = bp::extract<int>(pylen);
        std::string cls = bp::extract<std::string>(pycls);

        char tmp[1024];
        snprintf(tmp, sizeof(tmp),
                 "%s <%p> contains %d elements",
                 cls.c_str(), self.ptr(), len);
        return bp::str(tmp);
    }

    struct Colors
    {
#       define _COLOR(X) static auto X() { return bp::str(#X); }
        _COLOR(BLACK)
        _COLOR(BLUE)
        _COLOR(GREEN)
        _COLOR(CYAN)
        _COLOR(RED)
        _COLOR(MAGENTA)
        _COLOR(BROWN)
        _COLOR(LIGHTGRAY)
        _COLOR(DARKGRAY)
        _COLOR(LIGHTBLUE)
        _COLOR(LIGHTGREEN)
        _COLOR(LIGHTCYAN)
        _COLOR(LIGHTRED)
        _COLOR(LIGHTMAGENTA)
        _COLOR(YELLOW)
        _COLOR(WHITE)
#       undef _COLOR
    };

    void _set_source(data_set * ds, bp::object t)
    {
        bp::extract<data_set *> o(t);
        if(o)
            set_ds_source(ds, (*o).source);
        else
        {
            std::string x = bp::extract<std::string>(bp::str(t));
            char tmp[2048];
            snprintf(tmp, sizeof(tmp), "%s", x.c_str());
            set_ds_source(ds, tmp);
        }
    }

    void _import_numpy()
    {
        import_array1();
    }

    void _import_ds()
    {
        bp::class_<Colors>("Colors", bp::no_init)
#           define _COLOR(X) .add_static_property(#X, &Colors::X)
            _COLOR(BLACK)
            _COLOR(BLUE)
            _COLOR(GREEN)
            _COLOR(CYAN)
            _COLOR(RED)
            _COLOR(MAGENTA)
            _COLOR(BROWN)
            _COLOR(LIGHTGRAY)
            _COLOR(DARKGRAY)
            _COLOR(LIGHTBLUE)
            _COLOR(LIGHTGREEN)
            _COLOR(LIGHTCYAN)
            _COLOR(LIGHTRED)
            _COLOR(LIGHTMAGENTA)
            _COLOR(YELLOW)
            _COLOR(WHITE)
#           undef _COLOR
            ;

        bp::class_<data_set, _DSHandle>("DataSet", bp::no_init)
            .def("current", _current<_DSHandle>)
            .staticmethod("current")
            .add_property("xd",     &_DSHandle::getxd, &_DSHandle::setxd)
            .add_property("yd",     &_DSHandle::getyd, &_DSHandle::setyd)
            .def("inheritsfrom",    &_DSHandle::inheritsfrom)
            .add_property("line",   &_DSHandle::getls,    &_DSHandle::setls)
            .def(_strprop("symbol", &data_set::symb,      set_ds_point_symbol))
            .def(_strprop("source", &data_set::source,    set_ds_source))
            .def("setsource", _set_source)
            .add_property("color",  &_DSHandle::getcolor, &_DSHandle::setcolor)
            .def("__str__",         &_DSHandle::str)
            .add_property("cycleid",  _cycle_id)
            ;

    }
}
