#ifndef __PYWRAPPER_MODULE_HPP
#   define  __PYWRAPPER_MODULE_HPP
#   include "module.h"
#   include "module_ds.h"
#   include "module_vector.hpp"

namespace pywrapper
{
    template <typename T, typename K>
    inline _StrProp<T,K>::_StrProp(char const *n, char * T::*get, K set)
        : _name  (n)
        , _getter(get)
        , _setter(get, set)
    {}

    template <typename T, typename K>
    inline bp::str _StrProp<T,K>::Getter::operator()(T const * obj) const
    {
        if(obj == nullptr)   return bp::str("");
        else if((obj->*_get) == nullptr) return bp::str("");

        std::string tmp = obj->*_get;
        return bp::str(tmp);
    }

    template <typename T, typename K>
    inline bp::str _StrProp<T,K>::Setter::operator()(T * obj, bp::object t)
    {
        if(obj == nullptr)
            ;
        else
        {
            bp::extract<T*> o(t);
            if(o)
                (*_set)(obj, Getter::get(&*o));
            else
            {
                std::string x = bp::extract<std::string>(bp::str(t));
                char tmp [2048];
                snprintf(tmp, sizeof(tmp), "%s", x.c_str());
                (*_set)(obj, tmp);
            }
        }
        return Getter::operator()(obj);
    }

    template <typename T, typename K>
    template <class classT>
    inline void _StrProp<T,K>::visit(classT& c) const
    {
        bp::default_call_policies cp;
        boost::mpl::vector<bp::str, const T*>             gsig;
        boost::mpl::vector<bp::str,       T*, bp::object> ssig;
        c.add_property(_name, bp::make_function(_getter, cp, gsig)
                            , bp::make_function(_setter, cp, ssig));
    }

    template <typename T>
    inline T _current()
    {
        typename T::self * p = nullptr;
        char tmp[512]; snprintf(tmp, sizeof(tmp), "%s", _str<typename T::self>());
        int nb = ac_grep(cur_ac_reg, tmp, &p);
        if(nb == 1)
            return T(nullptr, p);
        return T(nullptr, nullptr);
    }
}
#endif
