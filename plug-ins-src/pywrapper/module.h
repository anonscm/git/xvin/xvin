#pragma once
namespace pywrapper
{
    template <typename T>
    struct _Wrapper;
}

namespace boost
{
    template<class T> inline T * get_pointer( pywrapper::_Wrapper<T> const& p )
    { return p.ptr(); }
}

#include <valarray>
#include <vector>
#include <cmath> // keep it here: compilation issue on windows with hypot
#include <boost/python.hpp>
#include <boost/python/return_value_policy.hpp>
#include <boost/python/return_internal_reference.hpp>
#include <boost/python/manage_new_object.hpp>
#include <boost/python/iterator.hpp>
#include <boost/python/stl_iterator.hpp>
#include <boost/iterator/transform_iterator.hpp>
#include "xvin.h"
#include <allegro.h>
#ifdef XV_WIN32
#   include <winalleg.h>
#endif
# ifndef NO_TRACK
#include "../trackBead/record.h"
# endif
namespace boost
{
    namespace python
    {
        template<class T> struct pointee<pywrapper::_Wrapper<T>>
        {
            typedef T type;
        };
    }
}


namespace pywrapper
{
    namespace bp = boost::python;

    template <typename T, typename K>
    class _StrProp : public bp::def_visitor<_StrProp<T,K>>
    {
        public:
        _StrProp(char const *n, char * T::*get, K set);
        _StrProp(_StrProp<T,K> const &) = default;

        friend class def_visitor_access;

        struct Getter
        {
            Getter(char * T::* x) : _get(x) {}
            bp::str operator()(T const * obj) const;
            char *  get(T* obj) const { return obj->*_get; }

            private:
                char * T::*_get;
        };

        struct Setter: public Getter
        {
            Setter(char * T::* x, K y) : Getter(x), _set(y) {}
            bp::str operator()(T* obj, bp::object t);

            private:
                K _set;
        };

        template <class classT>
        void visit(classT& c) const;

        char const *    _name;
        Getter          _getter;
        Setter          _setter;
    };

    template <typename T, typename K>
    inline auto _strprop(const char * n, char * T::*g, K s)
    { return _StrProp<T,K>(n, g, s); }

    template <typename T>
    struct _Wrapper
    {
        typedef T self;
        typedef T element_type;

        _Wrapper() : _ptr(nullptr) {}
        _Wrapper(_Wrapper<T> const &) = default;
        explicit _Wrapper(self * b) : _ptr(b) {}

        explicit operator bool() const { return _ptr != nullptr; }
        bool              good() const { return _ptr != nullptr; }
        bool              bad () const { return _ptr == nullptr; }

        auto  ptr    () const { return _ptr; }
        auto  release()       { auto x = _ptr; _ptr = nullptr; return x; }
        auto  isowner() const { return _isowner; }
        auto  setisowner()    { _isowner = true; }

        protected:
            self   * _ptr;
            bool     _isowner = false;
    };
# ifndef NO_TRACK
    struct _BeadHandle : public _Wrapper<bead_record>
    {
        typedef gen_record  parent;
        typedef void        child;

        _BeadHandle()                     : _Wrapper<bead_record>(),  _parent(nullptr) {}
        _BeadHandle(parent * r, self * b) : _Wrapper<bead_record>(b), _parent(r) {}
        _BeadHandle(_BeadHandle const &) = default;

        protected:
            parent * _parent;
    };
# endif
    bp::object  _string_rep(bp::object const self);

    template <typename T> constexpr char const * _str();

    template <typename T>
    inline T _current();
    void     _module();
}
