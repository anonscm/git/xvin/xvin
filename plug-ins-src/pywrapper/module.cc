#include "module.hpp"
#include "core.hpp"
#include "gitinfo.hh"
#include "../trackBead/indexing.h"
#include <boost/version.hpp>

XV_VAR(int, SCRIPT_GO_ON);
extern "C"
{
        //int         reload_trk_in_new_project(char * fullfile, int verbose);
    int         load_plt_file_in_pltreg(plot_region *pr, char *file, char *path);
}

namespace filter { void _import_math(); }
namespace pywrapper
{
    auto _getstr(std::string path)
    {
        std::vector<char> tmp(path.c_str(), path.c_str()+path.size());
        tmp.push_back('\0');
        tmp.push_back('\0');
        return tmp;
    }

    void _batchmode(bool x) { IS_BATCH_SCRIPT = x ? 1 : 0; }

 # ifndef NO_TRACK
    void _loadtrack(std::string path)
    {
        auto tmp = _getstr(path);
        reload_trk_in_new_project(&tmp[0], -1);
    }
 # endif

    void _loadplot(bp::object pypath)
    {
        auto pr = indexing::currentpltreg();
        if(pr == nullptr)
            return;

        std::vector<std::string> paths;
        extract_vector(pypath, paths);
        for(auto const & x: paths)
        {
            auto file = _getstr(x.substr(x.rfind("/")+1));
            auto path = _getstr(x.substr(0,x.rfind("/")+1));
            load_plt_file_in_pltreg(pr, &file[0], &path[0]);
            refresh_plot(pr, UNCHANGED);
        }
    }

    void _close() { SCRIPT_GO_ON = 0; }

    bp::object _gitinfo()
    {
        bp::dict g;
        g["BRANCH"]   = git::branch ();
        g["HASHTAG"]  = git::hashtag();
        g["VERSION"]  = git::version();
        g["AUTHOR"]   = git::author ();
        g["DATE"]     = git::date   ();
        g["CLEAN"]    = git::clean  ();
        return g;
    }

    void _draw_module();
    void _module()
    {
        bp::def("gitinfo", _gitinfo);
        bp::def("winprintf", +[](std::string x) { win_printf_OK(x.c_str()); });

#       ifndef NO_TRACK
        bp::class_<_BeadHandle>("BeadRecord", bp::no_init);
        bp::def("loadtrack", _loadtrack);
# endif
        bp::def("loadplots", _loadplot);
        bp::def("batchmode", _batchmode);
        bp::def("close",     _close);

        _draw_module();
        _import_ds();
        _import_vectors();
        filter::_import_math();
    }
}

BOOST_PYTHON_MODULE(pyxvin) { pywrapper::_module(); }
