#include <boost/optional.hpp>
#include "../filters/filter_core.h"
#include "../trackBead/indexing.h"
#include "../trackBead/draw_track_event_phase_average.hh"
#include "core.hpp"

namespace bp = boost::python;
extern "C" { struct plot_region; struct one_plot; }
namespace trackrenorm
{
    struct PyScript
    {
        enum XAxis
        {
            xTime   = 0,
            xZmag   = 1,
            xEForce = 2,
            xPhase  = 3,
            xCycle  = 4
        };

        enum YAxis
        {
            yZ      = 0,
            yZmag   = 1
        };

        enum Cut
        {
            cPerPhase = 0,
            cPerCycle = 1,
            cPerBead  = 2,
            cNone     = 3
        };

        enum ZThresholdAction
        {
            zLower  = 0,
            zHigher = 1,
            zNone   = 2
        };

        enum Restriction
        {
            rNb   = 0,
            rZmag = 1,
            rNone = 2
        };

        boost::optional<std::vector<int>>        beads;
        boost::optional<bool>                    sameplot;
        boost::optional<std::pair<int,int>>      cyclerange;
        boost::optional<std::vector<int>>        fixed;
        boost::optional<bool>                    bfilterfixed;
        boost::optional<XAxis>                   xaxis;
        boost::optional<YAxis>                   yaxis;
        boost::optional<std::vector<bool>>       phases;
        boost::optional<Cut>                     cut;
        boost::optional<bool>                    zprofile;
        boost::optional<std::pair<int,int>>      skip;
        boost::optional<float>                   minextension;
        boost::optional<std::pair<int,int>>      minextphases;
        boost::optional<float>                   gaussian;
        boost::optional<ZThresholdAction>        zthresholdaction;
        boost::optional<float>                   zthreshold;
        boost::optional<int>                     alignphase;
        boost::optional<Restriction>             restrict;
        boost::optional<std::pair<int,int>>      nbskip;
        boost::optional<std::pair<float,float>>  zrange;
    };

    template<int I, typename T0, typename T1>
    inline void set(T0 & tu, boost::optional<T1> const &b)
    {
        if(b)
            *std::get<I>(tu) = *b;
    }

    template<int I, typename T0>
    inline void set(T0 & tu, boost::optional<bool> const &b)
    {
        if(b)
            *std::get<I>(tu) = *b ? 1 : 0;
    }

    template<int I, typename T0, typename T1>
    inline void set(T0 & tu, boost::optional<std::pair<T1,T1>> const &b)
    {
        if(b)
        {
            *std::get<I>(tu)   = b->first;
            *std::get<I+1>(tu) = b->second;
        }
    }

    template <typename ... Args>
    int do_win_scanf(PyScript &script, char const *, Args ... args)
    {
        auto tu  = std::make_tuple(args...);
        if(script.beads)
        {
            *std::get<0>(tu) = script.beads->size() != 1;
            *std::get<1>(tu) = (*script.beads)[0];
        } else
            *std::get<0>(tu) = 1;

        set<2>(tu, script.sameplot);
        set<3>(tu, script.cyclerange);

        if(script.fixed)
        {
            std::string fixed= "";
            for(auto i: *script.fixed)
                fixed += ";"+std::to_string(i);
            strcpy(std::get<5>(tu), fixed.c_str());
        }

        set<6>(tu,  script.bfilterfixed);
        set<7>(tu,  script.yaxis);
        set<8>(tu,  script.xaxis);

        if(script.phases)
            for(size_t i = 0; i < script.phases->size(); ++i)
                std::get<9>(tu)[i] = (*script.phases)[i] ? 1 : 0;

        set<10>(tu, script.cut);
        set<11>(tu, script.zprofile);
        set<12>(tu, script.skip);
        set<14>(tu, script.minextension);
        set<15>(tu, script.minextphases);
        set<17>(tu, script.gaussian);
        set<18>(tu, script.zthresholdaction);
        set<19>(tu, script.zthreshold);
        set<20>(tu, script.alignphase);
        set<21>(tu, script.restrict);
        set<22>(tu, script.nbskip);
        set<24>(tu, script.zrange);
        return D_O_K;
    };

    int on_do_one_bead(PyScript & script, int beadid)
    {
        if(!script.beads)
            return D_O_K;

        for(auto i: *script.beads)
            if(i == beadid)
                return D_O_K;
        return WIN_CANCEL;
    }

    int runpyscript(PyScript const & _script)
    {
        PyScript script = _script;
        char const * key = "draw_track_event_phase_average";
#       include "../trackBead/record.hh"
    }

}

namespace pywrapper
{
    using trackrenorm::PyScript;
    char const * xnames[] = { "xTime", "xZmag", "xEForce", "xPhase", "xCycle", nullptr};
    char const * ynames[] = { "yZ", "yZmag" };
    char const * cnames[] = { "cPerPhase", "cPerCycle", "cPerBead", "cNone" };
    char const * znames[] = { "zLower", "zHigher", "zNone" };
    char const * rnames[] = { "rNb", "rZmag", "rNone" };

    void _draw_module()
    {
        bp::class_<PyScript>("DumpArgs")
            .def(fieldtopy("beads", &PyScript::beads))
            .def(fieldtopy("sameplot",      &PyScript::sameplot))
            .def(fieldtopy("cyclerange",    &PyScript::cyclerange))
            .def(fieldtopy("fixed",         &PyScript::fixed))
            .def(fieldtopy("bfilterfixed",  &PyScript::bfilterfixed))
            .def(fieldtopy("xaxis",         xnames, &PyScript::xaxis))
            .def(fieldtopy("yaxis",         ynames, &PyScript::yaxis))
            .def(fieldtopy("phases",        &PyScript::phases))
            .def(fieldtopy("cut",           cnames, &PyScript::cut))
            .def(fieldtopy("zprofile",      &PyScript::zprofile))
            .def(fieldtopy("skip",          &PyScript::skip))
            .def(fieldtopy("minextension",  &PyScript::minextension))
            .def(fieldtopy("minextphases",  &PyScript::minextphases))
            .def(fieldtopy("gaussian",      &PyScript::gaussian))
            .def(fieldtopy("zthresholdaction", znames, &PyScript::zthresholdaction))
            .def(fieldtopy("zthreshold",    &PyScript::zthreshold))
            .def(fieldtopy("alignphase",    &PyScript::alignphase))
            .def(fieldtopy("restrict",      rnames, &PyScript::restrict))
            .def(fieldtopy("nbskip",        &PyScript::nbskip))
            .def(fieldtopy("zrange",        &PyScript::zrange))
            .def("run",                     trackrenorm::runpyscript)
            ;
    }
};
