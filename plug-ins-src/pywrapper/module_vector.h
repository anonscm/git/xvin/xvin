#pragma once
#include "module.h"
#include "module_ds.h"
namespace pywrapper
{
    template <typename T>
    struct _VectorHandle : public _Wrapper<typename T::self>
    {
        using policy = T;
        using parent = typename T::parent;
        using self = typename T::self;
        using child = typename T::child;

        _VectorHandle()                   : _Wrapper<self>(nullptr) {}
        _VectorHandle(parent *, self * p) : _Wrapper<self>(p) {}
        explicit _VectorHandle(self * b)  : _Wrapper<self>(b) {}
        _VectorHandle(_VectorHandle<T> const &) = default;
        ~_VectorHandle();

        size_t  count   () const;
        auto    item    (int i) { return child(this->ptr(), *_item(i)); }
        bool    contains(typename child::self const *) const;
        auto    iter    (typename child::self ** addr, size_t i)
        {
            if(addr != nullptr) addr += i;
            auto p  = this->ptr();
            auto tr = [p](typename child::self * x) { return child(p, x); };
            return boost::make_transform_iterator(addr, tr);
        }

        auto begin() { return iter(_item(0), 0lu); }
        auto end  () { return iter(_item(0), count()); }

        protected:
            typename child::self ** _item(int i);
    };

    template <typename T>
    void remove(T & hdl, typename T::child & ds);

    struct _OPPolicy
    {
        using parent = pltreg;
        using self   = one_plot;
        using child  = _DSHandle;
        static auto         count(self const * p) { return p->n_dat;  }
        static auto         root (self const * p) { return p->dat;    }
        static auto         create ()             { return create_one_plot(1,1,0); }
        static auto         destroy(self * p)     { free_one_plot(p); }
        static auto         remove(self * p, child::self * c)
        { remove_ds_from_op(p, c); }
    };


    struct _PltRegPolicy
    {
        using parent = void;
        using self   = pltreg;
        using child  = _VectorHandle<_OPPolicy>;
        static auto          count(self const * p) { return p->n_op; }
        static auto          root (self const * p) { return p->o_p;  }
        static auto          create ()             { return (self*) nullptr; }
        static auto          destroy(self *)       {}
        static auto          remove(self * p, child::self * c)
        { remove_data_from_pltreg(p, IS_ONE_PLOT, (void*) c); }
    };
 # ifndef NO_TRACK
    struct _GRecPolicy
    {
        using parent = void;
        using self   = gen_record;
        using child  = _BeadHandle;
        static auto         count(self const * p) { return p->n_bead; }
        static auto         root (self const * p) { return p->b_r;  }
        static auto         create ()             { return (self*) nullptr; }
        static auto         destroy(self *)       {}
        static auto         remove(self *, child::self *)  {}
    };
# endif

    using OPHandle     = _VectorHandle<_OPPolicy>;
    using PltRegHandle = _VectorHandle<_PltRegPolicy>;
 # ifndef NO_TRACK
    using GRecHandle   = _VectorHandle<_GRecPolicy>;
# endif
    template <typename T>
    bp::class_<typename T::self, T> _topy(char const * name);

    void _import_vectors();
}
