#pragma once
#include <cmath> // keep it here: compilation issue on windows with hypot
#include <boost/python.hpp>
#include <string>
#define PYMENU_IMPL_ALL(A,B) MENU_IMPL_CUSTOM(A,B,pywrapper::load(),pywrapper::unload())
#define PY_IMG_MENU_IMPL_ALL(A,B) IMG_MENU_IMPL_CUSTOM(A,B,pywrapper::load(),pywrapper::unload())
namespace pywrapper
{
    typedef std::tuple<std::string, std::string, bool> Results;
    Results run     (std::string, boost::python::object);
    void    guirun  (std::string, boost::python::object);
    Results runmodule   (std::string, boost::python::object);
    void    guirunmodule(std::string, boost::python::object);
    boost::python::object gitinfo();
    boost::python::object global();
    boost::python::object import(std::string);

    void        load     ();
    void        unload   ();

    std::string exception();

    template <typename T>
    std::string runandcatch(T && fcn);
    template <typename T>
    bool        runandprinterr(T && fcn);
}
