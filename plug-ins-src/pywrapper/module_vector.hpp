#ifndef __PYWRAPPER_MODULE_VECTOR_HPP
#   define __PYWRAPPER_MODULE_VECTOR_HPP
#   include "module_vector.h"
#   include "module.hpp"
namespace pywrapper
{
    template <typename T>
    _VectorHandle<T>::~_VectorHandle()
    {
        if(this->ptr() != nullptr && this->isowner())
            policy::destroy(this->ptr());
    }

    template <typename T>
    size_t  _VectorHandle<T>::count() const
    {
        if(this->bad()) return 0lu;
        auto x = policy::count(this->ptr());
        if(x < 0)
            return 0lu;
        return size_t(x);
    }

    template <typename T>
    bool  _VectorHandle<T>::contains(typename child::self const * p) const
    {
        auto x = policy::root(this->ptr());
        return std::any_of(x, x+policy::count(this->ptr()),
                           [p](auto o) { return o == p; });
    }

    template <typename T>
    typename _VectorHandle<T>::child::self ** _VectorHandle<T>::_item(int i)
    {
        if(this->bad())
            return nullptr;
        size_t cnt = count();
        if(i < 0)
            i += cnt;
        if(i >= 0 && size_t(i) < cnt)
            return policy::root(this->ptr())+i;
        return nullptr;
    }

    template <> inline constexpr char const * _str<one_plot>  () { return "%op"; }
    template <> inline constexpr char const * _str<pltreg>    () { return "%pr"; }
 # ifndef NO_TRACK
    template <>
    inline GRecHandle _current<GRecHandle>()
    {
        auto p = _current<PltRegHandle>();
        if(p)
            return GRecHandle(nullptr, find_g_record_in_pltreg(p.ptr()));
        return GRecHandle();
    }
# endif
    template <typename T>
    inline bp::class_<typename T::self, T> _topy(char const * name)
    {
        return bp::class_<typename T::self, T>(name, bp::no_init)
                .def("__len__",         &T::count)
                .def("__getitem__",     &T::item)
                .def("__iter__",        bp::range( &T::begin, &T::end))
                .def("__contains__",    &T::contains)
                .def("__str__",         _string_rep)
                .def("current",         _current<T>)
                .staticmethod("current")
                ;
    }

    template <typename T>
    void remove(T & hdl, typename T::child & ds)
    {
        for(size_t i = 0; i < hdl.count(); ++i)
            if(T::policy::root(hdl.ptr())[i] == ds.ptr())
            {
                auto ptr = T::child::policy::create();
                T::policy::root(hdl.ptr())[i] = ptr;
                T::policy::remove(hdl.ptr(), ptr);
                ds.setisowner();
                break;
            }
    }

}
#endif
