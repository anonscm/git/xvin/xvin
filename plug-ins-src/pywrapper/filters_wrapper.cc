#include <boost/python/args.hpp>
#include "../filters/filter_core.h"
#include "core.hpp"
namespace filter
{
    using namespace pywrapper;
    namespace bp = boost::python;
    namespace forwardbackward
    {
        bp::handle<> _run( bp::object iter
                         , decltype(Args().derivate)  der    = Args().derivate
                         , decltype(Args().normalize) norm   = Args().normalize
                         , decltype(Args().precision) prec   = Args().precision
                         , decltype(Args().window)    w      = Args().window
                         , decltype(Args().power)     power  = Args().power
                         , bp::object                 est    = bp::object())
        {
            Args cf;
            cf.derivate  = der;
            cf.normalize = norm;
            cf.precision = prec;
            cf.window    = w;
            cf.power     = power;
            if(!est.is_none())
                extract_vector(est, cf.estimators);

            std::vector<float> dt;
            extract_vector(iter, dt);
            run(cf, dt.size(), dt.data());
            return topython(dt);
        }
    }

    namespace xvnonlin
    {
        bp::handle<> _run( bp::object iter
                         , decltype(Args().derivate)  der    = Args().derivate
                         , decltype(Args().precision) prec   = Args().precision
                         , decltype(Args().power)     power  = Args().power
                         , bp::object                 est    = bp::object())
        {
            Args cf;
            cf.derivate  = der;
            cf.precision = prec;
            cf.power     = power;
            if(!est.is_none())
                extract_vector(est, cf.estimators);

            std::vector<float> dt;
            extract_vector(iter, dt);
            run(cf, dt.size(), dt.data());
            return topython(dt);
        }
    }

    void _import_math()
    {
        forwardbackward::Args fw;
        auto est = topython(fw.estimators);
        bp::def( "forwardbackward"
               , forwardbackward::_run
               , (   bp::args("data")
                   , bp::args("derivate")   = fw.derivate
                   , bp::args("normalize")  = fw.normalize
                   , bp::args("precision")  = fw.precision
                   , bp::args("window")     = fw.window
                   , bp::args("power")      = fw.power
                   , bp::args("estimators") = est
                 )
               );

        xvnonlin::Args nl;
        est = topython(nl.estimators);
        bp::def( "nonlinear"
               , xvnonlin::_run
               , (   bp::args("data")
                   , bp::args("derivate")   = fw.derivate
                   , bp::args("precision")  = nl.precision
                   , bp::args("power")      = nl.power
                   , bp::args("estimators") = est
                 )
               );
    };
};
