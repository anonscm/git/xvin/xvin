#pragma once
#include "module.h"
namespace pywrapper
{
    struct _DSHandle : public _Wrapper<data_set>
    {
        using policy = _DSHandle;
        using parent = one_plot;
        using self   = data_set;
        using child  = void;

        static auto create () { return build_data_set(1,1); }

        using _Wrapper<self>::_Wrapper;
        _DSHandle()                   : _Wrapper<self>()  {}
        _DSHandle(parent *, self * d) : _Wrapper<self>(d) {}
        ~_DSHandle();

        bp::handle<> getdata(int self::*, float * self::*);
        bp::handle<> setdata(int self::*, float * self::*, bp::object const &);

        bp::handle<> getxd()
        { return getdata(&self::nx, &self::xd); }

        bp::handle<> setxd(bp::object iter)
        { return setdata(&self::nx, &self::xd, iter); }

        bp::handle<> getyd()
        { return getdata(&self::ny, &self::yd); }

        bp::handle<> setyd(bp::object iter)
        { return setdata(&self::ny, &self::yd, iter); }

        void inheritsfrom(_DSHandle const & o)
        { inherit_from_ds_to_ds(_ptr, o._ptr); }

        std::string setls(bp::object tt);

        std::string getls();

        std::string getcolor();

        std::string setcolor(bp::object t);
        bp::object str() const;
    };

    void _import_ds();
}
