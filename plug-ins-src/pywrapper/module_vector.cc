#include <set>
#include "module_vector.hpp"
#include "module_ds.h"
#include "../hybridstat/hybridstat.hh"
#include "../trackBead/indexing.h"
#include "core.hpp"
#include <boost/python/init.hpp>
namespace pywrapper
{
    auto _create_op()
    { return new OPHandle(nullptr, nullptr); }

    auto _op_create(OPHandle & hdl, ssize_t n)
    {
        if(hdl.ptr() == nullptr)
        {
            auto x  = _current<PltRegHandle>();
            OPHandle tmp(x.ptr(), create_and_attach_one_plot(x.ptr(), n, n, 0));
            hdl = tmp;
            tmp.release();
        } else
            create_and_attach_one_ds(hdl.ptr(), n, n, 0);
        return hdl.item(-1);
    }

    void _op_refresh(OPHandle & hdl)
    { if(hdl.ptr() != nullptr) hdl.ptr()->need_to_refresh = 1; }

    auto _pltreg_create(PltRegHandle & hdl, size_t n)
    {
        create_and_attach_one_plot(hdl.ptr(), n, n, 0);
        return hdl.item(-1);
    }

    void _pltreg_refresh1(PltRegHandle & hdl)
    {
        if(hdl.ptr() == nullptr)
            return;
        refresh_plot(hdl.ptr(), UNCHANGED);
    }

    void _pltreg_refresh2(PltRegHandle & hdl, int op)
    {
        if(hdl.ptr() == nullptr)
            return;
        refresh_plot(hdl.ptr(), op);
    }

    void _pltreg_refresh3(PltRegHandle & hdl, OPHandle & op)
    {
        if(hdl.ptr() == nullptr)
            return;
        if(op.ptr() == nullptr)
            return;
        op.ptr()->need_to_refresh = 1;
        for(int i = 0; i < hdl.ptr()->n_op; ++i)
            if(hdl.ptr()->o_p[i] == op.ptr())
            {
                refresh_plot(hdl.ptr(), i);
                break;
            }
    }

    void _refresh()
    {
        pltreg * pr = nullptr;
        char tmp[] = "%pr";
        if(ac_grep(cur_ac_reg, tmp, &pr) != 1)
            return;
        refresh_plot(pr, UNCHANGED);
    }

    template <typename T, indexing::opt_t<int> (*FCN)(T const *)>
    bp::object _opt(T const * x)
    {
        auto r = (*FCN)(x);
        if(r)
            return bp::object(r.value());
        return bp::object();
    }

    bp::object _phaserange(pltreg const * plt, int cid, int phase)
    {
        auto r = indexing::phaserange(plt, cid, phase);
        if(r)
            return bp::make_tuple(r.value().first, r.value().second);
        return bp::object();
    }

    struct _CycleRanger: public std::vector<_DSHandle>
    {
        _CycleRanger()                      = default;
        _CycleRanger(_CycleRanger const &)  = default;
        _CycleRanger(int cycle)
        {
            char tmp[] = "%pr";
            pltreg * curr = nullptr;
            if(1 != ac_grep(cur_ac_reg, tmp, &curr))
                return;
            _init(curr, cycle);
        }

        _CycleRanger(pltreg * plt, int cycle)
        { _init(plt, cycle); }

        void _init(pltreg * curr, int cycle)
        {
            for(auto x: indexing::selectcycle(curr, cycle))
                push_back(_DSHandle(x.first, x.second));
        }
    };

    _CycleRanger _getcycles(pltreg * plt, int cycle)
    { return _CycleRanger(plt, cycle); }

    bp::object _getcyclelist(pltreg const * curr)
    {
        auto r = indexing::cycles(curr);
        return topython(r);
    }

    bp::object _phasestart(gen_record * gr, int cid, int phase)
    {
        using namespace indexing;
        auto r  = phasestart(gr, cid, phase);
        if(!r)
            return {};
        return bp::object(*r);
    }

    void _saveplot(one_plot * op, std::string const & apath)
    {
        char path[1024], file[1024], fullfile[2048];
        strcpy(fullfile, apath.c_str());
        extract_file_name(file, 256, fullfile);
        extract_file_path(path, 512, fullfile);
        set_op_filename(op, file);
        set_op_path    (op, path);
        save_one_plot_bin(op, fullfile);
    }

    bp::object _phases(gen_record * gr)
    {
        if(gr == nullptr || gr->abs_pos < 0)
            return {};
        int lmin = 0, lmax = 0, lphase = 0;
        retrieve_min_max_event_and_phases(gr, &lmin, &lmax, &lphase);
        int inds[2] = { 0, 0};

        std::vector<int> mem;
        for(int i = lmin; i <= lmax; ++i)
            for(int k = 0; k < lphase; ++k)
            {
                int  _   = 0;
                retrieve_image_index_of_next_point_phase(gr, i, k, inds, inds+1, &_);
                mem.push_back(inds[0]);
                inds[0] = inds[1];
            }

        return bp::object(topython(mem));
    }

    void _import_vectors()
    {
        _topy<OPHandle>("OnePlot")
            .def( "__init__", boost::python::make_constructor(_create_op))
            .def("newitem", _op_create)
            .def("remove",  remove<OPHandle>)
            .def("refresh", _op_refresh)
            .def(_strprop("title",   &one_plot::title,   set_plot_title))
            .def(_strprop("xlabel",  &one_plot::x_title, set_plot_x_title))
            .def(_strprop("ylabel",  &one_plot::y_title, set_plot_y_title))
            .def(_strprop("filename",&one_plot::filename,set_op_filename))
            .def(_strprop("path",    &one_plot::dir,     set_op_path))
            .add_property("trackid", &_opt<one_plot, indexing::trackid>)
            .add_property("beadid",  &_opt<one_plot, indexing::beadid>)
            .def("save",    _saveplot)
            ;

        bp::class_<_CycleRanger>("CycleIterator", bp::no_init)
            .def("__iter__", bp::iterator<_CycleRanger>());

        _topy<PltRegHandle>("PltReg")
            .def("phaserange",  _phaserange)
            .def("cycle",       _getcycles)
            .add_property("cycles",      _getcyclelist)
            .def("newitem",     _pltreg_create)
            .def("remove",      remove<PltRegHandle>)
            .def("refresh",     _pltreg_refresh1)
            .def("refresh",     _pltreg_refresh2)
            .def("refresh",     _pltreg_refresh3)
            ;

        bp::def("refresh_plot", _refresh);

        _topy<GRecHandle>("GenRecord")
            .def("phasestart", _phasestart)
            .def("allphases",  _phases)
            .def("nphases",    indexing::nphases)
            .def("cyclemin",   indexing::cyclemin)
            .def("cyclemax",   indexing::cyclemax)
            .def("ncycles",    indexing::ncycles)
            ;
    }
}
