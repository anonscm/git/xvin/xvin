# ifndef PARSERFIT_H
# define PARSERFIT_H

#define FIT(i) gsl_vector_get(s->x, i)
#define ERR(i) sqrt(gsl_matrix_get(covar,i,i))
#define GSL_FN_EVAL(F,x)    (*((F)->function))(x,(F)->params)

typedef struct FitAndParserParameters {
  size_t n_fit;/*Nombre de points to fit*/
  double * y;/*valeurs*/
  double * sigma;/*ecart type*/
  double * xdata;/*valeur des abscisses*/
  //size_t n_points_parser;/*Nombre de points to calculate function with parser*/
  int NumberOfParameters;/*Nombe de paramètres*/
  char **VariablesNames;/*Nom des variables*/
  char *FunctionExpression;/*expression de la fonction pour le parser*/
  double *ParamValues;
  int ParameterToVarry;
} Parameters;

PXV_FUNC(void, OnError, ());
PXV_FUNC(int, func_f, (const gsl_vector * x, void *params, gsl_vector * f));
PXV_FUNC(int, func_df, (const gsl_vector * x, void *params, gsl_matrix * J));
PXV_FUNC(int, func_fdf, (const gsl_vector * x, void *params, gsl_vector * f, gsl_matrix * J));
PXV_FUNC(int, set_error_type, (int *ErrorType , O_p *op , float *error_ampl , int *dsnumb));
PXV_FUNC(int, set_gsl_fit, (Parameters *parameters));
PXV_FUNC(int, set_multiple_variable_parser, (Parameters *parameters));
PXV_FUNC(double, evaluate_function_in_param_value, (double x, void *param));
PXV_FUNC(double, evaluate_function_in_x, (double x, void *param));
PXV_FUNC(int, set_parser_fit, (Parameters *parameter) );
PXV_FUNC(int, fit, (void));
PXV_FUNC(int, set_FCS_fit, (Parameters *parameter));
PXV_FUNC(int, fit_FCS, (void) );
PXV_FUNC(int, set_parameters, (Parameters *parameters));
PXV_FUNC(int, plot_function, (Parameters *parameters));
PXV_FUNC(int, plot_function_with_parameters, (void));

gsl_function F;

parser_handle hParser;
double *var = NULL;

 
# endif /*ifndef PARSERFIT_H*/

