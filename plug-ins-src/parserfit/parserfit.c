/** **************************************************************************
    \file parserfit.c
    \author JF
    \brief Plug in for plotting and fitting with gsl in XVin

// help to make a running plugin//
gsl webpage: http://www.gnu.org/software/gsl/
download gsl Complete package, except sources	 at: http://gnuwin32.sourceforge.net/packages/gsl.htm
install it in C:\Program Files\GnuWin32 directory
copy libgsl.dll and libgslcblas.dll from 
C:\Program Files\GnuWin32\bin to 
C:\Program Files\XVin\bin
do make clean
do make
//end help//

muParser as well as gsl must be installed...
reading both documentation might be usefull

        JF 2005
***************************************************************************/

/*
    The parser is provided by Ingo Berg and can be downloaded on sourceforge: 
        
  Copyright (C) 2004, 2005 Ingo Berg

  Permission is hereby granted, free of charge, to any person obtaining a copy of this 
  software and associated documentation files (the "Software"), to deal in the Software
  without restriction, including without limitation the rights to use, copy, modify, 
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
  permit persons to whom the Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or 
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/
# ifndef PARSERFIT_C
# define PARSERFIT_C

#include <allegro.h>
#include <winalleg.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "gsl/gsl_vector.h"
#include "gsl/gsl_blas.h"
#include "gsl/gsl_multifit_nlin.h"
#include "gsl/gsl_deriv.h"


#include "xvin.h"
#include "muParserDLL.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL



#include "parserfit.h"

/*Function declarations*/
void OnError();
int func_f (const gsl_vector * x, void *params, gsl_vector * f);
int func_df (const gsl_vector * x, void *params, gsl_matrix * J);
int set_error_type(int *ErrorType , O_p *op , float *error_ampl , int *dsnumb);
int set_gsl_fit(Parameters *parameters);
int set_multiple_variable_parser(Parameters *parameters);
double evaluate_function_in_param_value(double x, void *param);
double evaluate_function_in_x(double x, void *param);
int set_parser_fit (Parameters *parameter);
int fit(void);
int set_FCS_fit (Parameters *parameter);
int fit_FCS(void);
int set_parameters(Parameters *parameters);
int plot_function (Parameters *parameters);
int plot_function_with_parameters(void);
MENU *fitfunction_menu(void);
int parserfit_main(int argc, char* argv[]);


/******************************************************************

    Parser error handling
*******************************************************************/


void OnError()
{
  register int i;

    i = win_printf("%s",mupGetErrorMsg());
    if (i == CANCEL)
      return;
    i = win_printf("%s",mupGetErrorToken());
    if (i == CANCEL)
      return;
    i = win_printf("%s",mupGetErrorPos());
    if (i == CANCEL)
      return;
    i = win_printf("%s",mupGetErrorCode());
    if (i == CANCEL)
      return;
}

/*****************************************************************

    Gsl fit part
    
******************************************************************/
/*****************************************************************
    Calcul de la fonction
*****************************************************************/
int func_f (const gsl_vector * x, void *params, gsl_vector * f)
{
      size_t n = ((Parameters *)params)->n_fit;
      double *y = ((Parameters *)params)->y;
      double *xdata = ((Parameters *)params)->xdata;
      double *sigma = ((Parameters *) params)->sigma;
      double *ParametersValue = ((Parameters *)params)->ParamValues;
      int NumParams = ((Parameters *) params)->NumberOfParameters;
      size_t i;
      double *Y;
      double x_value = 0;

      //On actualise les parametres de la fonction
      Y = (double *) calloc(n, sizeof(double));
      
      for (i = 0 ; i < NumParams -1  ; i++)
                        ParametersValue[i+1] = gsl_vector_get (x, i);//a definir dans les params
      
      for (i = 0; i < n; i++)
          {
      
                  x_value = (double) xdata[i];
                  ((Parameters *) params)->ParamValues[0] = x_value;
                  
                  ((Parameters *) params)->ParameterToVarry = 0;
                  //On calcule la fonction en x donc on fixe le premier parametre
                  Y[i] = evaluate_function_in_param_value(x_value, params);
      
                  gsl_vector_set (f, i, (Y[i] - y[i])/sigma[i]);
           }
      
      return GSL_SUCCESS;
}

/****************************************************************
    Calcul du jacobien
*****************************************************************/
int func_df (const gsl_vector * x, void *params, gsl_matrix * J)
{
  size_t n = ((Parameters *)params)->n_fit;
  double *xdata = ((Parameters *)params)->xdata;
  double *ParametersValue = ((Parameters *) params)->ParamValues;
  int NumParams = ((Parameters *) params)->NumberOfParameters;
  size_t i,j;
  register int k;
  double h = .00000001;//Ameliorer lire doc gsl
  double result_partial_derivation;
  double abserr;
  double x_value;
  //Met en place F pour calculer la derivee avec gsl
  
  F.function = &evaluate_function_in_param_value;
  F.params = params;
  
  
  //On recupere les parametres de gsl et on les mets dans les variables du parser
  for (i = 0 ; i < NumParams - 1 ; i++)
      ParametersValue[i+1] = gsl_vector_get (x, i);
  
  //On calcule les derives partielles pour chaque valeur de x prise dans xdata  
  for (i = 0; i < n; i++)
    {
        ((Parameters *) params)->ParamValues[0] = (double) xdata[i];
        for (j = 1 ; j < NumParams ; j++)
        {
              x_value = (double) ParametersValue[j];
              if ( (float)((Parameters *) params)->sigma[i] == 0)
		{
		  k = win_printf("sigma(i) = %g",(float)((Parameters *) params)->sigma[i]);
		  if (k == CANCEL)
		    return OFF;
		}
              ((Parameters *) params)->ParameterToVarry = j;
              
              gsl_deriv_central (&F, x_value , h , &result_partial_derivation, &abserr);
              gsl_matrix_set (J, i, j-1, result_partial_derivation / ((Parameters *) params)->sigma[i]); 
        }
    }
    
  //On remet x comme variable    
  ((Parameters *) params)->ParameterToVarry = 0;
  
  return GSL_SUCCESS;
}



int func_fdf (const gsl_vector * x, void *params, gsl_vector * f, gsl_matrix * J)
{
  func_f (x, params, f);
  func_df (x, params, J);

  return GSL_SUCCESS;
}


/**********************************************************************

Set the error values for fitting

**********************************************************************/
int set_error_type(int *ErrorType , O_p *op , float *error_ampl , int *dsnumb)
{
    int error_flag = 3;
    int i = 0 ;
     
    win_scanf("You want to work with : \n %R constant error type  \n %r relative error type  \n %r error type in another data set  \n %r error type in error bars \n",&error_flag); 
    //win_printf("error flag %d",error_flag);
    //if ((error_flag > 3)  || (error_flag <0)) return 0;
    if (error_flag == 2)
    {
    	i = win_scanf("Errors are in data set?\n%d ",dsnumb);
        if (i == CANCEL)	return OFF;
        if (op->dat[*dsnumb]->nx != op->dat[op->cur_dat]->nx)	
		return win_printf_OK("your data sets differs in size!");
	
    }
    
    	
    if (error_flag < 2)
    {
    	i = win_scanf("Amplitude of the error ?\n%f",error_ampl);
    	if (i == CANCEL)	return OFF;
   	}
    if (error_flag == 3)
    {
        if (op->dat[op->cur_dat]->ye == NULL) return win_printf_OK("No error bars!");
    	
    }

   	*ErrorType = error_flag;
   	
   	
   	return D_O_K;
}


/********************************************************************
    Set the parameters for the gsl fit
*********************************************************************/
int set_gsl_fit(Parameters *parameters)
{
  const gsl_multifit_fdfsolver_type *T = NULL;
  gsl_multifit_fdfsolver *s = NULL;
  int status = 0;
  size_t i, iter = 0;
  int num_points_to_be_fitted = 1;
  int NumParams = ((Parameters *) parameters)->NumberOfParameters-1;
  gsl_matrix *covar = gsl_matrix_alloc (NumParams, NumParams);
  double *y = NULL/*,*sigma = NULL*/,*xdata = NULL;
  gsl_multifit_function_fdf f;
  static double *x_init; 
  static float *x_test_init;
  gsl_vector_view x;
  double chi;
  static O_p *op = NULL;
  static pltreg *pr = NULL;
  char label[1024] = "",tmp[1024] = "FIT";
  d_s *ds_plot_fit = NULL;
  char schar [256];
  size_t n,p;
  int ErrorType = 0;
  float error_ampl = 0.1;
  int dsnumb = 0;
  d_s *ds = NULL;
  
  

     /*we must find the data to be fitted*/
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("cannot find data");
    
	num_points_to_be_fitted = get_ds_nx(ds);
	
	(*parameters).n_fit = num_points_to_be_fitted;
    /*allocate memory for the data*/
	y = (double *)calloc((*parameters).n_fit,sizeof(double));
	xdata = (double *)calloc((*parameters).n_fit,sizeof(double));
	parameters->sigma = (double *)calloc((*parameters).n_fit,sizeof(double));
	x_init = (double *) calloc((*parameters).NumberOfParameters-1,sizeof(double));
    x_test_init = (float *) calloc((*parameters).NumberOfParameters-1,sizeof(float));  
    	
   	//We choose the errors
	set_error_type( &ErrorType , op , &error_ampl , &dsnumb);

	for (i=0; i < (*parameters).n_fit ; i++)
	{
	  /*deffinition of data to deal with user coord
	  //from pixel to user coordinates
	  xdata[i] = (double)( (ds->xd[i] - op->ax)/op->dx );
	  y[i] = (double)( (ds->yd[i] - op->ay)/op->dy );
	  */
	  xdata[i] = (double)ds->xd[i];
	  y[i] = (double)ds->yd[i];
		   
	  switch  (ErrorType)
	    {	
	    case 0 : parameters->sigma[i] = error_ampl;
	      break;
	    case 1 : parameters->sigma[i] = ds->yd[i]*error_ampl; //ds->yd[i]*op->dy*error_ampl for dealing with user coord
	      break;
	    case 2 : parameters->sigma[i] = op->dat[dsnumb]->yd[i]; //(op->dat[dsnumb]->yd[i]- op->ay)/op->dy
	      break;
	    case 3 : parameters->sigma[i] = ds->ye[i]; //ds->ye[i]/op->dy
	      break;
	    }
	}


   	(*parameters).xdata = xdata;
   	(*parameters).y = y;
   	
   	/* first set init values*/

    for (i = 0 /* en 0 c'estla variable */; i < (*parameters).NumberOfParameters - 1 ;i++)
    {	
    	sprintf(schar,"%s init = %%f",(*parameters).VariablesNames[i+1]);
        win_scanf(schar,&x_test_init[i]);
        x_init[i] =(double)x_test_init[i];
    }
    
    
	/*set the solver parameters*/	
  
    
    n = (*parameters).n_fit;
    p = (*parameters).NumberOfParameters-1;
    x = gsl_vector_view_array (x_init, p);
    
    
    f.f = func_f;
    f.df = func_df;
    f.fdf = func_fdf;
    f.n = n;
    f.p = p;//a cause du x en parametre 0
    f.params = (void *) parameters;//&d;
    
    T = gsl_multifit_fdfsolver_lmsder;
    if (T == NULL) return win_printf("T NULL");
    s = gsl_multifit_fdfsolver_alloc (T, n, p);
    if (s == NULL) return win_printf("BAD");
     
    gsl_multifit_fdfsolver_set (s, &f, &x.vector);
     

     
  /* fit procedure starts*/
    do
    {
          iter++;
          status = gsl_multifit_fdfsolver_iterate (s);
          if (status)
          break;

          status = gsl_multifit_test_delta (s->dx, s->x,1e-4, 1e-4);/*end of fit criterium*/
    }          
    while ((status == GSL_CONTINUE) && (iter < 500));
  
  gsl_multifit_covar (s->J, 0.0, covar);

      /*plot the new data*/
  
  
  if ((ds_plot_fit = create_and_attach_one_ds(op, (*parameters).n_fit, (*parameters).n_fit , 0)) == NULL)

					    	return 0;
					    	
   //x est le parametre 0					 					    	
  (*parameters).ParameterToVarry = 0;

  for (i = 0; i < (*parameters).n_fit; i++)
    {
      
      ds_plot_fit->xd[i] = xdata[i];
      //il fut s'assurer que le parametre 0 est celui qui est fixe dans la ligne qui suit
      ds_plot_fit->yd[i] = (float)evaluate_function_in_param_value((double)ds_plot_fit->xd[i], parameters);

      /*Change this accolades contain by this comment  for dealing with user coord*/
      /*
	    //first we calculate the function in user coord
	    ds_plot_fit->xd[i] = xdata[i]; //from user coord to pixel coord
	    //il fut s'assurer que le parametre 0 est celui qui est fixe dans la ligne qui suit
	    ds_plot_fit->yd[i] = (float)evaluate_function_in_param_value((double)ds_plot_fit->xd[i], parameters);
	    //then we transform in pixel coord
	    ds_plot_fit->xd[i] = (ds_plot_fit->xd[i]*op->dx) + op->ax; 
	    ds_plot_fit->yd[i] = (ds_plot_fit->yd[i]*op->dy) + op->ay;
       */
    }

 
  
  chi = gsl_blas_dnrm2(s->f);
  strcat(label,"\\fbox{\\pt8\\stack{");
  for (i = 1 ; i < (*parameters).NumberOfParameters; i++)
  {
        sprintf(tmp,"{%s = %g +/- %g }",(*parameters).VariablesNames[i], (float)FIT(i-1),(float)ERR(i-1));
        strcat(label,tmp);
  }    
  sprintf (tmp, "{chisq/dof = %g}}}",pow(chi, 2.0)/ (num_points_to_be_fitted - p));
  strcat(label,tmp);
  push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, op->y_hi - (op->y_hi - op->y_lo)/4, label ,USR_COORD);
  //win_printf("text %s",label);
  gsl_multifit_fdfsolver_free (s);
  
  
  op->need_to_refresh = 1;
  refresh_plot(pr, UNCHANGED);
  broadcast_dialog_message(MSG_DRAW,0);
  return 0;

      
}
/************************************************************************

            PARSER PART
            
************************************************************************/
/****************************************************************************
    Give to the parser the number different variable names and the expression
    
**************************************************************************/
int set_multiple_variable_parser(Parameters *parameters)
{

    int i = 0;
    hParser = mupInit(); // Create a new handle
    //if (mupError()!) win_printf("mupInit ");
    // Set a callback for error handling
    mupSetErrorHandler(OnError);
    
    var = (double *) calloc((*parameters).NumberOfParameters,sizeof(double)); 
    if (var == NULL) return win_printf("Too bad");
    
    for (i = 0 ; i < (*parameters).NumberOfParameters ; i++)
    {
        mupDefineVar(hParser, (*parameters).VariablesNames[i] , var+i);
    }    
    //if (mupError()!) win_printf("mupDefineVar ");
    mupSetExpr(hParser, (*parameters).FunctionExpression);
    //if (mupError()!) win_printf("mupSetExpr ");
    return D_O_K;
}
    
    
/*************************************************************

This function allows evaluation of a multiparameter value

*************************************************************/      
double evaluate_function_in_param_value(double x, void *param)
{
    int i = 0;
    Parameters *parameters = (Parameters *)param;
    double Result;
 
       for (i = 0 ; i < parameters->NumberOfParameters ; i++)
    {
        if (i == (*parameters).ParameterToVarry)   *(var + i) = x;
        else *(var + i) = parameters->ParamValues[i];
    }    
    Result = mupEval(hParser);
      
    return Result;
}

/*************************************************************

This function allows evaluation of f(x) 

*************************************************************/

double evaluate_function_in_x(double x, void *param)
{
    parser_handle hParser;
    //double fVal = 0.;
    
    int i = 0;
    Parameters *parameters = (Parameters *)param;
    double Result;
    
    
    hParser = mupInit(); // Create a new handle
    //if (mupError()!) win_printf("mupInit ");
    // Set a callback for error handling
    mupSetErrorHandler(OnError);
    //mupDefineVar(hParser, "x", &x);
    
    var = (double *) calloc(parameters->NumberOfParameters,sizeof(double)); 
    for (i = 0 ; i < parameters->NumberOfParameters ; i++)
        mupDefineVar(hParser, parameters->VariablesNames[i] , var+i);
    //if (mupError()!) win_printf("mupDefineVar ");
    for (i = 0 ; i < parameters->NumberOfParameters ; i++)
        *(var + i) = *(parameters->ParamValues + i);
    //mupSetVarFactory(hParser, AddVariable);
    
    mupSetExpr(hParser, parameters->FunctionExpression);
    //if (mupError()!) win_printf("mupSetExpr ");
    
    Result = mupEval(hParser);
    mupRelease(hParser); // Release an existing parser handle
    free(var);
    
    return Result;
    
} 


/*****************************************************************************

Set the parameter strcuture for a general fit

******************************************************************************/
int set_parser_fit (Parameters *parameter)
{   
    
    char names[512],tmp[512];
    //int n_pos = 0, strpos = 0;
    int i = 0;
    int j = 0, k = 0 , l = 0;
    
    (*parameter).NumberOfParameters = 1;
    
    (*parameter).FunctionExpression = (char *) calloc(512,sizeof(char));   
    l = win_scanf("Function to fit with x as a variable \n f(x) = %s \n Names of the free parameters with ; delimiter (place one at the end) \n %s",(*parameter).FunctionExpression,names);     
    if (l == CANCEL) return D_O_K;
    
    //we count the number of parameters from the delimiters in the string
    for (i = 0; strcspn ( names + j, ";" ) !=  strlen (names+j) && i < 20; i++) 
    {
        //win_printf("%s \n cspn %d \n len %d\n i %d j %d",names+j,strcspn ( names + j, ";" ),strlen (names+j),i,j);
        j += strcspn ( names + j, ";" )+1;
    }    
    (*parameter).NumberOfParameters += i;
    if (strcspn ( names , ";" ) == 0) (*parameter).NumberOfParameters = 1;
    //win_printf("NumberOfParameters %d \n %s",(*parameter).NumberOfParameters,(*parameter).FunctionExpression);
    
    (*parameter).VariablesNames = (char **) calloc((*parameter).NumberOfParameters,sizeof(char *));
    (*parameter).ParamValues = (double *) calloc((*parameter).NumberOfParameters,sizeof(double));
    
    for (i = 0; i < (*parameter).NumberOfParameters ; i++)
    {
        (*parameter).VariablesNames[i] = (char *) calloc(32,sizeof(char));
    } 
    
      (*parameter).VariablesNames[0] = "x";
    //We look for the variables names now that we counted them
    strcpy ( tmp, names );
    j = 0;
    
    for (i = 0; strcspn ( names + j, ";" ) !=  (strlen (names+j))  && i < 20 ; i++) 
    {
        k = strcspn ( tmp  , ";" );
        
        if ( k == 0)           
        {
                    strcpy ( tmp , names + j + 1 );
                    j ++ ;
                    
        }    
        else
        {           strncpy ( (*parameter).VariablesNames[i+1] , names + j, k ); 
                    j += k +1;
                    if (j <= strlen (names))
                    {                    strcpy ( tmp, names + j  );
                    }    
        }    
    }  
    return D_O_K;  
}



/************************************************************************

                GENERAL FUNCTION
                
*************************************************************************/


int fit(void)
{
    static Parameters MainParameter;
    
    if(updating_menu_state != 0)	return D_O_K;
    set_parser_fit (&MainParameter);
    set_multiple_variable_parser(&MainParameter);
    set_gsl_fit (&MainParameter);
    
    return D_O_K;
    
}


int set_FCS_fit (Parameters *parameter)
{   
    int i;
    
    (*parameter).NumberOfParameters = 1;
    
    (*parameter).FunctionExpression = (char *) calloc(512,sizeof(char));   
    (*parameter).FunctionExpression = "g0/(1+x/T)";
    (*parameter).NumberOfParameters += 2;//PUT THE NUMBER OF FREE PARAMETERS INSTEAD OF 2
    (*parameter).VariablesNames = (char **) calloc((*parameter).NumberOfParameters,sizeof(char *));
    (*parameter).ParamValues = (double *) calloc((*parameter).NumberOfParameters,sizeof(double));
    
    for (i = 0; i < (*parameter).NumberOfParameters ; i++)
    {
        (*parameter).VariablesNames[i] = (char *) calloc(32,sizeof(char));
    } 
    
      (*parameter).VariablesNames[0] = "x";
      (*parameter).VariablesNames[1] = "g0";
      (*parameter).VariablesNames[2] = "T";   
    return D_O_K;  
}



int fit_FCS(void)
{
    static Parameters MainParameter;
    
    if(updating_menu_state != 0)	return D_O_K;
    set_FCS_fit (&MainParameter);
    set_multiple_variable_parser(&MainParameter);
    set_gsl_fit (&MainParameter);
    
    return D_O_K;
    
}

/*This down part will be soon removed by Andre*/
int set_gsl_fit_several_ds(Parameters *parameters)
{
  const gsl_multifit_fdfsolver_type *T = NULL;
  gsl_multifit_fdfsolver *s = NULL;
  int status = 0;
  size_t i, iter = 0;
  int num_points_to_be_fitted = 1;
  int NumParams = ((Parameters *) parameters)->NumberOfParameters-1;
  gsl_matrix *covar = gsl_matrix_alloc (NumParams, NumParams);
  double *y = NULL/*,*sigma = NULL*/,*xdata = NULL;
  gsl_multifit_function_fdf f;
  static double *x_init; 
  static float *x_test_init;
  gsl_vector_view x;
  double chi;
  static O_p *op = NULL;
  static pltreg *pr = NULL;
  char label[2048] = "",tmp[2048] = "FIT";
  d_s *ds_plot_fit = NULL;
  char schar [256];
  size_t n,p;
  int ErrorType = 0;
  float error_ampl = 0.1;
  int dsnumb = 0;
  d_s *ds = NULL;
  /*vriables added by Andre*/
  int first_ds = 0, //number of first ds to fit
      last_ds = 1, //number of last ds to fit
      nbr_ds, k, //total number of  ds to fit
      max_num_points_to_be_fitted = 0, //to allocate memory to biggest ds
      nx,
      icolor = 0; //ds color
  float *results;

     /*we must find the data to op cointaining ds*/
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
       return win_printf_OK("cannot find data");
       
    last_ds = op->n_dat -1; //defining last_ds
    //win_printf("directory: %s", backslash_to_slash(op->dir));
    //taking parameters from user
    i = win_scanf("Fit data sets\n"
		  "from %5d to %5d", &first_ds, &last_ds);
    if (i == CANCEL)
      return OFF;
    //housekeeping
    if( (first_ds < 0) || (first_ds >= last_ds) || (last_ds > (op->n_dat -1)) )
      {
        win_printf("Wrong data set number");
        return OFF;
      }
    
    nbr_ds = (last_ds - first_ds) + 1; //defining nbr_ds
    results = (float *)calloc( nbr_ds, sizeof(float)); //memory for store fit results from several ds
    //ds = op->dat[first_ds]; //takes the first ds wanted
    
    /*determine max_num_to_be_fitted*/
    for(i = first_ds; i < (last_ds + 1); i++)
    {
          nx = op->dat[i]->nx;
          max_num_points_to_be_fitted =
          ( ( max_num_points_to_be_fitted > nx ) ? 
          (max_num_points_to_be_fitted) : (nx) );
          //max_num_points_to_be_fitted = max(max_num_points_to_be_fitted, get_ds_nx(ds[i]));
    }
    //win_printf("max num to fit%d", max_num_points_to_be_fitted);
    /**/
	//

    
    /*allocate enough memory for the largest ds*/
    y = (double *)calloc(max_num_points_to_be_fitted,sizeof(double)); //(double *)calloc((*parameters).n_fit,sizeof(double));
    xdata = (double *)calloc(max_num_points_to_be_fitted,sizeof(double)); //(double *)calloc((*parameters).n_fit,sizeof(double));
    parameters->sigma = (double *)calloc(max_num_points_to_be_fitted,sizeof(double)); //(double *)calloc((*parameters).n_fit,sizeof(double));
    x_init = (double *) calloc((*parameters).NumberOfParameters-1,sizeof(double)); //(double *) calloc((*parameters).NumberOfParameters-1,sizeof(double));
    x_test_init = (float *) calloc((*parameters).NumberOfParameters-1,sizeof(float));  //(float *) calloc((*parameters).NumberOfParameters-1,sizeof(float));
    	
   	//We choose the errors
    set_error_type( &ErrorType , op , &error_ampl , &dsnumb);

	  	
   	/* first set init values for the first ds*/

    for (i = 0 /* en 0 c'estla variable */; i < (*parameters).NumberOfParameters - 1 ;i++)
    {	
    	sprintf(schar,"%s init = %%f",(*parameters).VariablesNames[i+1]);
        win_scanf(schar,&x_test_init[i]);
        x_init[i] =(double)x_test_init[i];
    }
    
    //loop over the different ds
    for(k = first_ds; k < (last_ds + 1); k++)
      {

	ds = op->dat[k];
	//defining number of points in current ds
	num_points_to_be_fitted = get_ds_nx(ds);	
	(*parameters).n_fit = num_points_to_be_fitted;
	win_printf("fitting ds %d", k);
	/*choose the error for the current ds*/
	for (i=0; i < (*parameters).n_fit ; i++)
	  {
	    //from pixel to user coordinates
	    xdata[i] = (double)( (ds->xd[i] - op->ax)/op->dx );
	    y[i] = (double)( (ds->yd[i] - op->ay)/op->dy );
	    switch  (ErrorType)
	      {	
	      case 0 : parameters->sigma[i] = error_ampl;
		break;
	      case 1 : parameters->sigma[i] = ds->yd[i]*op->dy*error_ampl; //for dealing with user coord
		break;
	      case 2 : parameters->sigma[i] = (op->dat[dsnumb]->yd[i]- op->ay)/op->dy; //for dealing with user coord
		break;
	      case 3 : parameters->sigma[i] = ds->ye[i]/op->dy; //for dealing with user coord
		break;
	    }
	    
	  }
	/*and the data for the current ds*/
   	(*parameters).xdata = xdata;
   	(*parameters).y = y;
   		
	/*set the solver parameters*/
        
	n = (*parameters).n_fit;
	p = (*parameters).NumberOfParameters-1;
	x = gsl_vector_view_array (x_init, p); /*XXX il faut changer l'initialisation des autres ds*/
	
	
	f.f = func_f;
	f.df = func_df;
	f.fdf = func_fdf;
	f.n = n;
	f.p = p;//a cause du x en parametre 0
	f.params = (void *) parameters;//&d;
	
	T = gsl_multifit_fdfsolver_lmsder;
	if (T == NULL) return win_printf("T NULL");
	s = gsl_multifit_fdfsolver_alloc (T, n, p);
	if (s == NULL) return win_printf("BAD");
	
	gsl_multifit_fdfsolver_set (s, &f, &x.vector);
	
	
	
	/* fit procedure starts*/
	do
	  {
	    iter++;
	    status = gsl_multifit_fdfsolver_iterate (s);
	    if (status)
	      break;
	    
	    status = gsl_multifit_test_delta (s->dx, s->x,1e-4, 1e-4);/*end of fit criterium*/
	  }          
	while ((status == GSL_CONTINUE) && (iter < 500));
	
	gsl_multifit_covar (s->J, 0.0, covar);
	
	/*plot the new data*/
	
	
	if ((ds_plot_fit = create_and_attach_one_ds(op, (*parameters).n_fit, (*parameters).n_fit , 0)) == NULL)
	  return 0;
	
	//x est le parametre 0					 					    	
	(*parameters).ParameterToVarry = 0;
	
	for (i = 0; i < (*parameters).n_fit; i++)
	  {
	    
	    //first we calculate the function in user coord
	    ds_plot_fit->xd[i] = xdata[i]; //from user coord to pixel coord
	    //il fut s'assurer que le parametre 0 est celui qui est fixe dans la ligne qui suit
	    ds_plot_fit->yd[i] = (float)evaluate_function_in_param_value((double)ds_plot_fit->xd[i], parameters);
	    //then we transform in pixel coord
	    ds_plot_fit->xd[i] = (ds_plot_fit->xd[i]*op->dx) + op->ax; 
	    ds_plot_fit->yd[i] = (ds_plot_fit->yd[i]*op->dy) + op->ay;
	  }
	
	icolor = get_ds_line_color(ds);
	set_ds_line_color(ds_plot_fit, icolor);
	
	chi = gsl_blas_dnrm2(s->f);
	sprintf(label,"\\color{%d}\\fbox{\\pt6\\stack{", icolor);
	for (i = 1 ; i < (*parameters).NumberOfParameters; i++)
	  {
	    sprintf(tmp,"{%s = %g +/- %g }",(*parameters).VariablesNames[i], (float)FIT(i-1),(float)ERR(i-1));
	    strcat(label,tmp);
	  }    
	sprintf (tmp, "{chisq/dof = %g}}}",pow(chi, 2.0)/ (num_points_to_be_fitted - p));
	strcat(label,tmp);
	push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo)/4, op->y_hi - (op->y_hi - op->y_lo)/4, label ,USR_COORD);
	//win_printf("text %s",label);
	for (i = 0 /* en 0 c'estla variable */; i < (*parameters).NumberOfParameters - 1 ;i++)
	  {	
	    //sprintf(schar,"%s init = %%f",(*parameters).VariablesNames[i+1]);
	    //win_scanf(schar,&x_test_init[i]);
	    x_init[i] =(double) FIT(i);
	  }

      }
    gsl_multifit_fdfsolver_free (s);
        
    op->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    broadcast_dialog_message(MSG_DRAW,0);
    return 0;
    
    
}

int set_biexponential_fit (Parameters *parameter)
{   
    int i;
    
    (*parameter).NumberOfParameters = 1;
    
    (*parameter).FunctionExpression = (char *) calloc(512,sizeof(char));   
    (*parameter).FunctionExpression = "a0+a1*exp(-l1*x)+a2*exp(-l2*x)";
    (*parameter).NumberOfParameters += 5;//PUT THE NUMBER OF FREE PARAMETERS INSTEAD OF 5
    (*parameter).VariablesNames = (char **) calloc((*parameter).NumberOfParameters,sizeof(char *));
    (*parameter).ParamValues = (double *) calloc((*parameter).NumberOfParameters,sizeof(double));
    
    for (i = 0; i < (*parameter).NumberOfParameters ; i++)
    {
        (*parameter).VariablesNames[i] = (char *) calloc(32,sizeof(char));
    } 
    
      (*parameter).VariablesNames[0] = "x";
      (*parameter).VariablesNames[1] = "a0";
      (*parameter).VariablesNames[2] = "a1";
      (*parameter).VariablesNames[3] = "a2";
      (*parameter).VariablesNames[4] = "l1";
      (*parameter).VariablesNames[5] = "l2";
      
         
    return D_O_K;  
}

int fit_biexponential(void)
{
    static Parameters MainParameter;
    
    if(updating_menu_state != 0)	return D_O_K;
    set_biexponential_fit  (&MainParameter);
    set_multiple_variable_parser(&MainParameter);
    set_gsl_fit (&MainParameter);
    
    return D_O_K;
    
}

int fit_several_biexponential(void)
{
    static Parameters MainParameter;
    
    if(updating_menu_state != 0)	return D_O_K;
    set_biexponential_fit  (&MainParameter);
    set_multiple_variable_parser(&MainParameter);
    set_gsl_fit_several_ds(&MainParameter);
      //    set_gsl_fit (&MainParameter);
    
    return D_O_K;
    
}
/*This upper part will be soon removed by Andre*/

/**************************************************************************

Set the value for each parameter except x, the function variable

*****************************************************************************/

int set_parameters(Parameters *parameters)
{
    int i= 0;
    char schar[512];
    float tmp;
    
    for (i=1 /*on prendra le premier comme variable de la fonction*/; i < (*parameters).NumberOfParameters ;i++)
    {	
    	sprintf(schar,"%s value = %%f",(*parameters).VariablesNames[i]);
    	//sprintf(scharb,"%s = %%f",schar);
        win_scanf(schar,&tmp);
        (*parameters).ParamValues[i] =(double)tmp;
    }
   return D_O_K;
    
}

/******************************************************************************

Plot the function when all the parameters have been set

******************************************************************************/

int plot_function (Parameters *parameters)
{
    d_s *ds = NULL;
    O_p *op = NULL, *opnew = NULL;
    pltreg *pr = NULL;
    int npoints = 1024;
    float xmin = 0, xmax = 10;
    int j = 0;
    float f;
    int i = 0;
    
    
    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2) 		return win_printf_OK("cannot find data");
    
    j = win_scanf("You will plot a function f(x)= /n Between xmin = %f \n and xmax = %f \n with npoints=%d",&xmin,&xmax,&npoints);
    if (j == CANCEL) return D_O_K;
    
    opnew = create_and_attach_one_plot(pr,npoints,npoints, 0);//Affiner la taille du dataset
    if (opnew == NULL) return 1;
    
    ds = create_and_attach_one_ds (opnew,npoints, npoints, IS_FLOAT);
    
    remove_ds_from_op(opnew, opnew->dat[0]);
    
    (*parameters).ParameterToVarry = 0;
    /*il faudrait que je m'en serve*/
    F.function = &evaluate_function_in_param_value;
    F.params = parameters;
    
    for (i = 0; i < npoints ; i++)
    {
        f = xmin+i*(xmax-xmin)/npoints;
        ds->xd[i] =f;
        ds->yd[i] = (float)evaluate_function_in_param_value((double)ds->xd[i], parameters);
        
              
    }   
    
    set_plot_y_title(opnew, (*parameters).FunctionExpression);	
    set_plot_x_title(opnew, "x");
    set_ds_plain_line(ds);
    	    
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
} 





int plot_function_with_parameters(void)
{
    static Parameters MainParameter;
    
    if(updating_menu_state != 0)	return D_O_K;
        
    set_parser_fit (&MainParameter);
    set_parameters (&MainParameter);
    set_multiple_variable_parser(&MainParameter);
    plot_function (&MainParameter);
    
    return D_O_K;
    
    
    
}
MENU *fitfunction_menu(void)
{
	static MENU mn[32];
	
	if (mn[0].text != NULL)	return mn;

	
   add_item_to_menu(mn, "Fit FCS", fit_FCS,   NULL,       0, NULL  );
   add_item_to_menu(mn, "Fit biexponential", fit_biexponential,   NULL,       0, NULL  );
   add_item_to_menu(mn, "Fit several biexponential", fit_several_biexponential,   NULL,       0, NULL  );

   add_item_to_menu(mn, "General Fit ", fit,   NULL,       0, NULL  );
   add_item_to_menu(mn, "Plot ", plot_function_with_parameters,   NULL,       0, NULL  );
     
   return mn;
}


// main program
int parserfit_main(int argc, char* argv[])
{
  
 
    add_plot_treat_menu_item("Fit function", NULL, fitfunction_menu() , 0, NULL);
    
    broadcast_dialog_message(MSG_DRAW,0);
	
    
  return 0;
}
int	parserfit_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "Fit function", NULL, NULL);
  
  return D_O_K;
}
# endif    


