/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _SEQHELI_H_
#define _SEQHELI_H_

#include <allegro.h>
#include "xvin.h"
#include "../../../plug-ins-src/trackBead/select_rec.h"
//#include "xv_plugins.c"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 #include "SeqHeli.h"


PXV_FUNC(int,menu_do_op_heli_seq_config,(void));
PXV_FUNC(int,do_op_heli_seq_config,(pltreg *pr));
PXV_FUNC(int,do_select_SeqHeli,(void));
PXV_FUNC(int,do_select_SeqHeli,(void));
PXV_FUNC(int,add_gaussian_histo_from_ds_zone,(int jpr,O_p *op,d_s *ds,int bead_nb,float xstart,float xend));
PXV_FUNC(int, draw_gauss_histo_from_click,(pltreg *pr, int x_0, int y_0, DIALOG *d));
PXV_FUNC(MENU *,SeqHeli_plot_menu,(void));
#endif
