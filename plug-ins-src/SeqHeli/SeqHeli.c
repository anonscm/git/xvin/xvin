/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _SEQHELI_C_
#define _SEQHELI_C_

#include <allegro.h>
#include "xvin.h"
//#include "xv_plugins.c"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 #include "SeqHeli.h"


static O_p **op_heli_seq;
static O_p ***plot_list_heli_seq;
static pltreg **prlist_heli_seq;
static float *sigma_heli_seq;
static int prnb_heli_seq;


int menu_do_op_heli_seq_config(void)
{
  pltreg *pr = NULL;
  if (updating_menu_state != 0) return D_O_K;
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");
  do_op_heli_seq_config(pr);
  return 0;
}

int do_op_heli_seq_config(pltreg *pr)
{

  static float sigma = 0.005;
  g_record *g_r = NULL;
  int j = 0;
  O_p *oph = NULL;
  int i = win_scanf(" This will configure the click HeliSeq \n"
  "Gaussian histogram sigma : %6f\n",&sigma);
  if (i == WIN_CANCEL) return 1;
  int foundpr = 0;
  for (j = 0;j<prnb_heli_seq;j++)
  {
    if (pr == prlist_heli_seq[j])
    {
      foundpr = 1;
      sigma_heli_seq[j] = sigma;
      break;
    }
  }

  if (foundpr == 0)
{
  g_r = find_g_record_in_pltreg(pr);
  if (g_r == NULL)   win_printf_OK("No gen record found");
  op_heli_seq = realloc(op_heli_seq,sizeof(O_p*)*(prnb_heli_seq+1));
  plot_list_heli_seq = realloc(plot_list_heli_seq,sizeof(O_p**)*(prnb_heli_seq+1));
  plot_list_heli_seq[prnb_heli_seq] = calloc(g_r->n_bead,sizeof(O_p*));
  prlist_heli_seq = realloc(prlist_heli_seq,sizeof(pltreg*)*(prnb_heli_seq+1));
  prlist_heli_seq[prnb_heli_seq] = pr;
  oph = create_and_attach_one_plot(pr,16,16,0);
  set_plot_title(oph,"Histogram HeliSeq of all beads");
  set_plot_x_title(oph,"Extension");
  set_plot_y_title(oph,"Probability");
  op_heli_seq[prnb_heli_seq] = oph;
  sigma_heli_seq = realloc(sigma_heli_seq,sizeof(float)*(prnb_heli_seq+1));
  sigma_heli_seq[prnb_heli_seq] = sigma;
  prnb_heli_seq += 1;
}

  return 0;
}

int 	do_select_SeqHeli(void)
{

  if(updating_menu_state != 0)
    {
      if ((general_pr_action == draw_gauss_histo_from_click)
   && strstr(active_menu->text, "Unselect Seq Heli") == NULL)
   {
   snprintf(active_menu->text,128,"Unselect Seq Heli");
  }
      else  if ((general_pr_action != draw_gauss_histo_from_click)
   && strstr(active_menu->text, "Select Seq Heli") == NULL)
   {
   snprintf(active_menu->text,128,"Select Seq Heli");
   }
      return D_O_K;
    }

  if (general_pr_action == draw_gauss_histo_from_click)
    {
      general_pr_action = prev_general_pr_action;
      prev_general_pr_action = draw_gauss_histo_from_click;
    }
  else
    {
      prev_general_pr_action = general_pr_action;
      general_pr_action = draw_gauss_histo_from_click;
    }
  return 0;
}

int add_gaussian_histo_from_ds_zone(int jpr,O_p *op,d_s *ds,int bead_nb,float xstart,float xend)
{

  O_p *op1 = NULL;
  O_p *op2 = NULL;
  d_s *ds1 = NULL;
  d_s *ds2 = NULL;
  O_p *opr = NULL;
  d_s *dsd = NULL;
  int inds = 0;
  int inde = 0;
  for (int i = 0;i<ds->nx;i++)
  {
    if (fabs(ds->xd[i] - xstart) < 1) inds = i;
    if (fabs(ds->xd[i] - xend) < 1)
    {
      inde = i;
      break;
    }

  }
  dsd =duplicate_partial_data_set(ds,NULL,inds,inde-inds+1);
  op1 = op_heli_seq[jpr];
  op2 = plot_list_heli_seq[jpr][bead_nb];
  if (op2 == NULL)
  {
    op2 = create_and_attach_one_plot(prlist_heli_seq[jpr],16,16,0);
    set_plot_title(op2,"Histogram HeliSeq for bead %d",bead_nb);
    set_plot_x_title(op2,"Extension");
    set_plot_y_title(op2,"Probability");
    plot_list_heli_seq[jpr][bead_nb] = op2;
    ds2 = op2->dat[0];
  }
  else ds2 = create_and_attach_one_ds(op2,16,16,0);
  ds1 = create_and_attach_one_ds(op1,16,16,0);

  opr = build_histo_with_exponential_convolution_2(NULL, op, dsd, sigma_heli_seq[jpr]);
  duplicate_data_set(dsd,ds2);
  duplicate_data_set(dsd,ds1);
  //free_one_plot(opr);
  free_data_set(dsd);

  return 0;

}


 int draw_gauss_histo_from_click(pltreg *pr, int x_0, int y_0, DIALOG *d)
 {

     int   dx = 0, dy = 0, ldx, ldy, color;
     //float  xa, ya; // x, y,
     int bead_nb;
     //struct box *b;
     O_p *op = NULL;
     d_s *ds = NULL;
     g_record *g_r = NULL;
     float xstart, xend = 0;
     static int lock = 0;

     if (lock == 1) return D_O_K;
     lock = 1;

     if (pr->one_p == NULL)		return D_O_K;
     g_r = find_g_record_in_pltreg(pr);
     if (g_r == NULL)   win_printf_OK("No gen record found");
     int foundpr = 0;
     int jpr = 0;
     for (jpr = 0;jpr<prnb_heli_seq;jpr++)
     {
       if (pr == prlist_heli_seq[jpr])
       {
         foundpr = 1;
         break;
       }
     }

   if (foundpr == 0)
      {
            if (do_op_heli_seq_config(pr))
            {
              lock = 0;
              return D_O_K;
            }
          }





     op = pr->one_p;
     bead_nb = op->user_ispare[ISPARE_BEAD_NB];
     if (bead_nb < 0 || bead_nb >= g_r->n_bead)  win_printf_OK("bead nb out of range");
     op = pr->one_p;
     ds = op->dat[op->cur_dat];
     //sc = pr->screen_scale;
     //b = pr->stack;
     //ya = y_pr_2_pltdata_raw(pr, y_0);// y =
     //xa = x_pr_2_pltdata_raw(pr, x_0);//x =

     xstart = x_pr_2_pltdata_raw(pr, x_0);
     ldy = 0;
     ldx = 0;
     color = makecol(255, 64, 64);
     while ((mouse_b & 0x02) == 0)
       {     // as long as no right click
         if (key[KEY_ESC])
         {
           lock = 0;
           return D_REDRAWME;
         }
         dx = mouse_x - x_0;
         dy = mouse_y - y_0;
         if  (ldy != dy || dx != ldx)
   	{   // if mouse moving we update line
   	  blit_plot_reg(d);
      for(;screen_acquired;); // we wait for screen_acquired back to 0;
      screen_acquired = 1;
   	  acquire_bitmap(screen);
   	  circle(screen, x_0,y_0, 2, color);
   	  line(screen,x_0,y_0,x_0 + dx,y_0 + dy,color);
   	  release_bitmap(screen);
      screen_acquired = 0;

   	  // we display point values
   	  draw_bubble(screen, B_LEFT, 20, 64, "x0 = %g y0 %g",
   		      x_pr_2_pltdata(pr,x_0),y_pr_2_pltdata(pr,y_0));
   	  draw_bubble(screen, B_RIGHT, SCREEN_W-64, 64, "x1 = %g y2 %g",
   		      x_pr_2_pltdata(pr,x_0 + dx),y_pr_2_pltdata(pr,y_0 + dy));
   	  //y = y_pr_2_pltdata_raw(pr, y_0 + dy);
   	  //x = x_pr_2_pltdata_raw(pr, x_0 + dx);
   	}
    ldx = dx;
    ldy = dy;
  }


   	xend = x_pr_2_pltdata_raw(pr, mouse_x);
    if (xend-xstart > 5)  add_gaussian_histo_from_ds_zone(jpr,op,ds,bead_nb,xstart,xend);
    lock = 0;
    return D_REDRAWME;

}




MENU *SeqHeli_plot_menu(void)
{
  static MENU mn[32];
  char *mnstr = NULL;

  if (mn[0].text != NULL)	return mn;
  mnstr = (char *) calloc(128,sizeof(char));
  if (mnstr == NULL) return NULL;
  snprintf(mnstr,128,"Select Seq Heli");
  add_item_to_menu(mn,mnstr,do_select_SeqHeli,NULL,0,NULL);
  add_item_to_menu(mn,"change configuration",menu_do_op_heli_seq_config,NULL,0,NULL);

  return mn;
}

int	SeqHeli_main(int argc, char **argv)
{
  (void)argc;  (void)argv;  add_plot_treat_menu_item ( "SeqHeli", NULL, SeqHeli_plot_menu(), 0, NULL);
  op_heli_seq = NULL;
  plot_list_heli_seq = NULL;
  prlist_heli_seq = NULL;
  sigma_heli_seq = NULL;
  prnb_heli_seq = 0;
  return D_O_K;
}

int	SeqHeli_unload(int argc, char **argv)
{
  (void)argc;  (void)argv;
  if (general_pr_action == draw_gauss_histo_from_click)
    {
      general_pr_action = prev_general_pr_action;
      prev_general_pr_action = draw_gauss_histo_from_click;
    }
  remove_item_to_menu(plot_treat_menu, "SeqHeli", NULL, NULL);
  free(sigma_heli_seq);
  for (int i = 0;i<prnb_heli_seq;i++)
  {
    free(plot_list_heli_seq[i]);
  }
  free(op_heli_seq);
  free(plot_list_heli_seq);
  free(prlist_heli_seq);

  return D_O_K;
}
#endif
