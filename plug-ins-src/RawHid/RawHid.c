/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _RAWHID_C_
#define _RAWHID_C_

#include <allegro.h>
# include "xvin.h"
#ifdef XV_UNIX
#include "hid_LINUX.h"
#else
#include "rawhid-o.h"
#endif
#include <pthread.h>

#include <unistd.h>

/* If you include other regular header do it here*/


/* But not below this define */
# include "../xvplot/xvplot.h"
//# define BUILDING_PLUGINS_DLL
# include "RawHid.h"

# define PACK_SIZE 64

int adc_avg = -1;
int adc_res = -1;
int adc_gain = -1;
int adc_speed = -1;
int adc_samp = -1;
int adc_ref = -1;


typedef struct hiddata
{
  unsigned char data[PACK_SIZE];
  unsigned char n_read;
  unsigned char new;
  unsigned char treated;
  int n_pack;
  int n_rec;
  long timestamp;
} hid_d;

# define N_PACK 1024
hid_d packet[N_PACK];
int i_pack_r = 0;  // recieved
int tot_pack_t = 0;  // treated
int tot_pack = 0;  // recieved
int tot_bytes = 0;

char hid_message[256] = {0};
int n_hid_mes = 0;
int n_disp_hid_mes = 0;

pthread_t thread_hid = 0;


typedef struct ramp_iv
{
  unsigned short vdata[16384];
  unsigned short idata[16384];
  unsigned char treated;
  int n_sp;
  int i_sp;
  int n_rec;
} r_iv;

r_iv ra;
int n_ra = 0;

typedef struct _sigvt
{
  unsigned short vdata[16384];
  unsigned char treated;
  int n_sp;
  int i_sp;
  int n_rec;
  unsigned int duration; // micros
} sigvt;

sigvt vt;
int n_vt = 0;

multi_d_s *mds = NULL;

static void *grab_hid_data(void* data)
{
  int hid = -1, hidt, i, left;
  int num;
  unsigned int *ui = NULL;
  unsigned short int *usi = NULL;
  char dsname[8][16];
  int nds;
  float values[8];

  (void)data;
  while (1) {
    hidt = rawhid_open(1, 0x16C0, 0x0480, 0xFFAB, 0x0200);
    if (hidt <= 0)
      {
	if (hid >= 0)
	  {
	    rawhid_close(hid);
	    hid = -1;
	    hidt = rawhid_open(1, 0x16C0, 0x0480, 0xFFAB, 0x0200);
	  }
	// Arduino-based example is 16C0:0486:FFAB:0200
	if (hidt <= 0)
	  {
	    hidt = rawhid_open(1, 0x16C0, 0x0486, 0xFFAB, 0x0200);
	    if (hidt <= 0)
	      {

		snprintf(hid_message,sizeof(hid_message),"Waiting for device:");
		n_hid_mes++;
		sleep(1000);
		continue;
	      }
	  }
      }
    if (hidt <= 0) continue;
    if (hidt >= 0) hid = hidt;
    snprintf(hid_message,sizeof(hid_message),"Listening:");
    n_hid_mes++;
    while (1)
      {
	num = rawhid_recv(0, packet[i_pack_r].data, PACK_SIZE, 200);
	if (num < 0) break;
	if (num == 0) continue;
	packet[i_pack_r].n_read = num;
	packet[i_pack_r].new = 1;
	usi = (unsigned short int *)(packet[i_pack_r].data + 2);
	ui = (unsigned int *)(packet[i_pack_r].data + 4);
	if (packet[i_pack_r].data[0] == 'R')
	  {// a ramp
	    left = usi[0];
	    if (packet[i_pack_r].data[1] == '\r')
	      { // a new ramp
		ra.n_sp = left;
		ra.i_sp = 0;
	      }
	    for (i = 0; i < 15 && i < left; i++)
	      {
		ra.vdata[ra.i_sp] = usi[2*i + 1];
		ra.idata[ra.i_sp++] = usi[2*i + 2];
	      }
	    if (i == left)
	      {
		ra.n_rec = tot_pack;
		n_ra++;
	      }
	  }
	else if (packet[i_pack_r].data[0] == 'V')
	  {// a time series
	    left = usi[0];
	    i = 0;
	    if (packet[i_pack_r].data[1] == '\r')
	      { // a new time series
		vt.n_sp = left;
		vt.i_sp = 0;
		vt.duration = ui[0];
		adc_avg = packet[i_pack_r].data[8];
		adc_res = packet[i_pack_r].data[9];
		adc_gain = packet[i_pack_r].data[10];
		adc_speed = packet[i_pack_r].data[11];
		adc_samp = packet[i_pack_r].data[12];
		adc_ref = packet[i_pack_r].data[13];
		i = 5;
	      }
	    for ( ; i < 30 && i < left; i++)
	      {
		vt.vdata[vt.i_sp++] = usi[i + 1];
	      }
	    if (i == left)
	      {
		vt.n_rec = tot_pack;
		n_vt++;
	      }
	  }
	else if (packet[i_pack_r].data[0] == 'M')
	  {// a time series
	    left = usi[0];
	    i = 0;
	    int k = 0, ki = 0;
	    unsigned char ch;
	    if (packet[i_pack_r].data[1] == '\r')
	      { // a new xvplot stuff
		for(i = ki = nds = 0; i < 62 && nds < 8; i++)
		  {
		    ch = packet[i_pack_r].data[i+2];
		    if (ch == 0)
		      {
			if (ki > 0)
			  {
			    dsname[nds][ki++] = 0;
			    ki = 0;
			    nds++;
			  }
			break;
		      }
		    if(ch != ' ' && ch != '\t')
		      dsname[nds][ki++] = (char)ch;
		    else if (ki > 0)
		      {
			dsname[nds][ki++] = 0;
			ki = 0;
			nds++;
		      }
		  }
		if (nds > 0)
		  {
		    mds = create_multi_d_s(nds);
		    mds->source = strdup("RawHid");
		    for (k = 0; k < mds->n_d_s && k < nds; k++)
		      {
			for (ki = 0; dsname[k][ki] != 0 && ki < 15; ki++)
			  mds->name[k][ki] = dsname[k][ki];
			if (ki == 15) mds->name[k][ki] = 0;
		      }
		  }
	      }
	    else if (mds != NULL)
	      {
		for(i = ki = nds = 0; i < 62 && nds < 8; i++)
		  {
		    ch = packet[i_pack_r].data[i+2];
		    if (ch == 0)
		      {
			if (ki > 0)
			  {
			    dsname[nds][ki++] = 0;
			    values[nds] = atof(dsname[nds]);
			    ki = 0;
			    nds++;
			  }
			break;
		      }
		    if(ch != ' ' && ch != '\t')
		      {
			ch = (ch == ',') ? '.' : ch;
			dsname[nds][ki++] = ch;
		      }
		    else if (ki > 0)
		      {
			dsname[nds][ki++] = 0;
			values[nds] = atof(dsname[nds]);
			ki = 0;
			nds++;
		      }
		  }
		add_data_to_multi_d_s(mds, values);
		snprintf(hid_message,sizeof(hid_message),"Multivalues %d -> %g %g %g",mds->n,values[0],values[1],values[2]);
		n_hid_mes++;
	      }
	  }

	i_pack_r++;
	i_pack_r = (i_pack_r < N_PACK) ? i_pack_r : 0;
	tot_pack++;
	tot_bytes += num;
	snprintf(hid_message,sizeof(hid_message),"Connected %d packet read with %d bytes",tot_pack,tot_bytes);
	n_hid_mes++;
      }
    rawhid_close(0);
    snprintf(hid_message,sizeof(hid_message),"Device disconnected waiting for new device:");
    n_hid_mes++;
  }
  return NULL;
}






int do_RawHid_hello(void)
{
  int i, j, k;
  char mes[1024] = {0};
  unsigned short int *usi = NULL;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from RawHid\n%s",hid_message);
  if (i == WIN_CANCEL) return 0;

  for (k = 0; k != WIN_CANCEL; )
    {
      j = tot_pack_t%N_PACK;
      for (i = 0, mes[0] = 0; i<packet[j].n_read; i++)
	{
	  sprintf(mes+strlen(mes),"%02X ", packet[j].data[i] & 255);
	  if (i % 16 == 15 && i < packet[j].n_read-1)
	    sprintf(mes+strlen(mes),"\n");
	}
      usi = (unsigned short int *)(packet[j].data + 2);
      k = win_printf("Packet %d out of %d size %d content:\n%s\n"
		     "%c %d : %d-> %d %d -> %d ... p %d\n"
		     ,tot_pack_t,tot_pack,packet[j].n_read,mes,packet[j].data[0],
		     (int)usi[0],(int)usi[1],(int)usi[2],(int)usi[3],(int)usi[4],(int)usi[30]);
      tot_pack_t = (tot_pack_t < tot_pack-1) ? tot_pack_t + 1 : tot_pack_t;
    }

  return 0;
}


int do_RawHid_send_stuff(void)
{
  int i;
  char mes[1024] = {0};

  if(updating_menu_state != 0)	return D_O_K;

  i = win_scanf("Enter your command %s",mes);
  if (i == WIN_CANCEL) return 0;
  for (i = strlen(mes); i < 64; i++) mes[i] = 0;
  i = rawhid_send(0, mes, 64, 220);
  if (i != 64)
    win_printf("Error sending :%s\n %d send instead of %d",mes,i,strlen(mes));
  return 0;
}


int do_RawHid_chg_ADC_avg(void)
{
  int i, i_avg = 0, avg = 0;
  char mes[1024] = {0};

  if(updating_menu_state != 0)	return D_O_K;

  avg = (adc_avg > 0) ? adc_avg : 0;
  for (i_avg = 0; avg > 0; i_avg++, avg /= 2);
  i = win_scanf("To specify ADC speed set Nb. of averaging\n"
		"0->%R 1->%r 2->%r 4->%r 8->%r 16->%r 32->%r\n",&i_avg);
  if (i == WIN_CANCEL) return 0;
  for(avg = 0; i_avg > 0; avg = (avg) ? 2*avg : 1, i_avg--);
  snprintf(mes,sizeof(mes),"A%d",avg);
  for (i = strlen(mes); i < 64; i++) mes[i] = 0;
  i = rawhid_send(0, mes, 64, 220);
  if (i != 64)
    win_printf("Error sending :%s\n %d send instead of %d",mes,i,strlen(mes));
  return 0;
}


int do_RawHid_rescale_data_set(void)
{
  int i, j;
  O_p *op = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == WIN_CANCEL)	return OFF;

  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int RawHid_grab_ramp_idle_action(O_p *op, DIALOG *d)
{
  int i, nf;
  pltreg *pr;
  d_s *ds;
  char mes[64] = {0};
  static int pk = 0;

  (void)d;
  if(updating_menu_state != 0)	return D_O_K;
  if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
    return win_printf_OK("cannot find pr");
  if (ra.n_sp <= 0) return 0;
  if (ra.i_sp < vt.n_sp) return 0; // we wait
  if (ra.n_rec <= pk) return 0;
  pk = ra.n_rec;
  nf = ra.n_sp;
  ds = op->dat[0];
  ds->nx = ds->ny = 0;
  for (i = 0; i < nf; i++)
  {
    add_new_point_to_ds(ds,(float)ra.vdata[i],(float)ra.idata[i]);
  }
  set_ds_source(ds,"Ramp %d",ra.n_rec);
  set_plot_title(op, "Ramp %d",ra.n_rec);
  op->need_to_refresh = 1;
  mes[0] = 'R';
  rawhid_send(0, mes, 64, 220);
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_RawHid_rescale_plot(void)
{
  int j;
  O_p  *opn = NULL;
  int nf;
  d_s *ds;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  nf = ra.n_sp;	/* this is the number of points in the data set */

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  opn->op_idle_action = RawHid_grab_ramp_idle_action;
  ds = opn->dat[0];

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = (float)ra.idata[j];
    ds->xd[j] = (float)ra.vdata[j];
  }

  /* now we must do some house keeping */
  //inherit_from_ds_to_ds(ds, dsi);
  set_ds_source(ds,"ramp");
  set_plot_title(opn, "Ramp %d (%d)",n_ra,ra.n_rec);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}


int RawHid_grab_signal_idle_action(O_p *op, DIALOG *d)
{
  int i, j, nf;
  pltreg *pr;
  d_s *ds;
  double dt;
  char mes[64] = {0};
  static int pk = 0, first = 1;
  float tmp, decade, vref;

  (void)d;
  if(updating_menu_state != 0)	return D_O_K;
  if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
    return win_printf_OK("cannot find pr");
  if (vt.n_sp <= 0)
    {
      if (first)
	{
	  sprintf (mes,"V4096");
	  rawhid_send(0, mes, 64, 220);
	  first = 0;
	}
      return 0;
    }
  if (vt.i_sp < vt.n_sp) return 0; // we wait
  if (vt.n_rec <= pk) return 0;
  pk = vt.n_rec;
  nf = vt.n_sp;
  ds = op->dat[0];
  ds->nx = ds->ny = 0;
  dt = (double)(vt.duration)/nf;
  for (i = 0; i < nf; i++)
  {
    add_new_point_to_ds(ds,(float)i,(float)vt.vdata[i]);
  }
  for (i = 0; i < op->n_xu; i++)
    {
      if (op->xu[i]->type == IS_SECOND) break;
    }
  if (i == op->n_xu)
    create_attach_select_x_un_to_op(op, IS_SECOND, 0, (float)dt, -6, 0, "\\mu s");
  else
    {
      decade = op->xu[i]->decade + 6;
      tmp = (float)pow(10,-decade);
      op->xu[i]->dx = tmp*dt;
      //op->xu[i]->decade = -6;
    }
  set_op_x_unit_set(op, i);

  vref = (adc_ref) ? 3.3 : 1.2;
  for (i = 0, j = 1; i < adc_res; i++) j *= 2;
  vref /= j;
  for (i = 0; i < op->n_yu; i++)
    {
      if (op->yu[i]->type == IS_VOLT) break;
    }
  if (i == op->n_yu)
    create_attach_select_y_un_to_op(op, IS_VOLT, 0, vref, 0, 0, "V");
  else
    {
      decade = op->yu[i]->decade;
      tmp = (float)pow(10,-decade);
      op->yu[i]->dx = tmp*vref;
    }
  set_op_y_unit_set(op, i);



  set_ds_source(ds,"signal %d (%d)  Avg %d, Res %d , Gain %d, Speed %d, Samp %d, Ref %d"
		,n_vt,vt.n_rec, adc_avg, adc_res, adc_gain, adc_speed, adc_samp, adc_ref);
  set_plot_title(op, "\\stack{{signal %d (%d)}"
		 "{\\pt7   Avg %d, Res %d , Gain %d, Speed %d, Samp %d, Ref %d}}"
		 ,n_vt,vt.n_rec, adc_avg, adc_res, adc_gain, adc_speed, adc_samp, adc_ref);

  op->need_to_refresh = 1;
  sprintf (mes,"V4096");
  rawhid_send(0, mes, 64, 220);
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_RawHid_grab_signal(void)
{
  int j;
  O_p  *opn = NULL;
  int nf;
  d_s *ds;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");


  //if (vt.n_sp == 0) return win_printf_OK("Signal has 0 points !");

  nf = vt.n_sp;	/* this is the number of points in the data set */

  if ((opn = create_and_attach_one_plot(pr, (nf) ? nf : 4096, (nf) ? nf : 4096, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];
  opn->op_idle_action = RawHid_grab_signal_idle_action;
  if (nf > 0)
    {
      for (j = 0; j < nf; j++)
	{
	  ds->yd[j] = (float)vt.vdata[j];
	  ds->xd[j] = ((float)(vt.duration)*j)/nf;
	}
    }
  else ds->nx = ds->ny = 0;

  /* now we must do some house keeping */
  //inherit_from_ds_to_ds(ds, dsi);
  set_ds_source(ds,"signal");
  set_plot_title(opn, "signal %d",vt.n_rec);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int RawHid_grab_multivalues_idle_action(O_p *op, DIALOG *d)
{
  int i, j, ix, iy, redraw;
  pltreg *pr;
  d_s *ds;

  (void)d;
  if(updating_menu_state != 0)	return D_O_K;
  if ((pr = find_pr_in_current_dialog(NULL)) == NULL)
    return win_printf_OK("cannot find pr");
  if (mds == NULL) return win_printf_OK("cannot find mds");
  for (i = 0, redraw = 0; i < op->n_dat; i++)
    {
      ds = op->dat[i];
      ix = ds->user_ispare[0];
      iy = ds->user_ispare[1];
      if (ix < 0 || ix > mds->n_d_s) continue;
      if (iy < 0 || iy > mds->n_d_s) continue;
      if (ds->nx >= mds->n) continue;
      else
	{
	  for(j = ds->nx; j < mds->n; j++)
	    add_new_point_to_ds(ds,mds->x[ix][j],mds->x[iy][j]);
	  redraw++;
	}
    }
  if (redraw == 0) return 0;
  set_plot_title(op, "Multivalue signal %d",mds->n);;
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;

}




int do_RawHid_grab_multivalues(void)
{
  int i;
  O_p  *opn = NULL;
  pltreg *pr = NULL;
  char mes[64];


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");
  if (mds != NULL)
    {
      i = win_scanf("A multivalue recording is already in place\n"
		    "to start a new one press Ok\nto continue with this one press CANCEL\n");
      if (i == WIN_OK)
	{
	  free_multi_d_s(mds);
	  mds = NULL;
	}
    }
  sprintf(mes,"M ");
  i = rawhid_send(0, mes, 64, 220);
  if (i != 64)
    return win_printf_OK("Error sending :%s\n %d send instead of %d",mes,i,strlen(mes));
  win_printf("test");
  if (mds == NULL) return win_printf_OK("No multi values recording active !");
  opn = create_Op_from_multi_d_s(pr, mds);
  if (opn == NULL)     return win_printf_OK("cannot create plot !");
  opn->op_idle_action = RawHid_grab_multivalues_idle_action;
  set_plot_title(opn, "Multivalue signal");
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



MENU *RawHid_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Read Raw Hid", do_RawHid_hello,NULL,0,NULL);
  add_item_to_menu(mn,"Send command",do_RawHid_send_stuff ,NULL,0,NULL);


  add_item_to_menu(mn,"data set rescale in Y", do_RawHid_rescale_data_set,NULL,0,NULL);
  add_item_to_menu(mn,"Grab ramp", do_RawHid_rescale_plot,NULL,0,NULL);
  add_item_to_menu(mn,"Grab signal", do_RawHid_grab_signal ,NULL,0,NULL);
  add_item_to_menu(mn,"Grab Multi values", do_RawHid_grab_multivalues,NULL,0,NULL);
  add_item_to_menu(mn,"Set Averaging", do_RawHid_chg_ADC_avg,NULL,0,NULL);


  
  return mn;
}

int	RawHid_main(int argc, char **argv)
{
  int ret = 0;
  (void)argc;
  (void)argv;
  ret = pthread_create (&thread_hid, NULL, grab_hid_data, NULL);
  if (ret) win_printf_OK("Could not start HID thread!");
  add_plot_treat_menu_item ( "RawHid", NULL, RawHid_plot_menu(), 0, NULL);
  return D_O_K;
}

int	RawHid_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "RawHid", NULL, NULL);
  return D_O_K;
}
#endif
