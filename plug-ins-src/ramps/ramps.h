#ifndef _RAMPS_H_
#define _RAMPS_H_

#define	RAMPS_LOWER		0x0100
#define	RAMPS_UPPER		0x0200
#define	RAMPS_BEGIN		0x1000
#define	RAMPS_MIDDLE	0x2000
#define	RAMPS_END		0x4000

#define	FILE_BINARY		0x0100
#define	FILE_NI4472		0x0200


PXV_FUNC(MENU*, ramps_plot_menu, 		(void));
PXV_FUNC(int, ramps_main, 			(int argc, char **argv));
PXV_FUNC(int, ramps_unload,			(int argc, char **argv));

PXV_FUNC(int, do_find_boundaries,					(void));
PXV_FUNC(int, do_hist_periodic_build_from_power_ds,	(void));
PXV_FUNC(int, do_hist_periodic_build_from_power_files,	(void));
PXV_FUNC(int, do_hist_periodic_build_from_NI4472,		(void));
PXV_FUNC(int, do_hist_periodic_build_from_M_dep_files, (void));
PXV_FUNC(int, do_stats_periodic_build_from_M_dep_files,(void));
PXV_FUNC(int, do_average_over_cycles,				(void));

PXV_FUNC(int*, impose_indices,		(float dt, float cycle_period, float start_time, int N, int *n_ind));
PXV_FUNC(int, ask_for_parameters_to_search_for_indices, 
									(int *bool_search_indices, int *point_position, int *plateau_position, 
									int *bool_remove_bounds, int *n_last, int *bool_shift_indices, int *n_shift, 
									int *bool_add_more_indices, int *n_more, int *N_shift_auto) );
	
PXV_FUNC(int, find_search_domain,	(float *x, int nx, int plateau_position, float *y_min, float *y_max));
						
PXV_FUNC(int*, find_set_of_indices, (float *x, int nx, int *n_ind,
						int bool_search_indices, int point_position, int plateau_position, 
						int bool_remove_bounds, int n_last, 
						int bool_shift_indices, int n_shift, 
						int bool_add_more_indices, int n_more,
						int N_shift_auto,
						float y_min, float y_max) );

PXV_FUNC(void, spit,						(char *message));

#endif

