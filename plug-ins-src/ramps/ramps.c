#ifndef _RAMPS_C_
#define _RAMPS_C_

#include "allegro.h"
#include "xvin.h"
#include "../include/xvin/xv_tools_lib.h"

#include <gsl/gsl_histogram.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_statistics.h>
#include <fftw3.h> // because ../resistance/resistance.h uses type fftw_complex

/* If you include other plug-ins header do it here*/ 
#include "../hist/hist.h"
#include "../inout/inout.h"
#include "../resistance/resistance.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "ramps.h"

#ifndef k_b
#define k_b 1.3806503e-23	// m2 kg s-2 K-1
#endif

#ifndef Temperature
#define Temperature 300.		// K
#endif

#define kT_Fred 4.005443325e-021 // le kT utilisé par Frédéric




int do_find_boundaries(void)
{	O_p 	*op1 = NULL;
	d_s 	*ds1, *ds2=NULL;
	pltreg *pr = NULL;
	float 	y_min=0, y_max=0;
static int 	n_last=2, n_more, n_shift, N_shift_auto;
static int	point_position=2, plateau_position=1;
static int	bool_search_indices=1, bool_remove_bounds=1, bool_shift_indices, bool_add_more_indices;
	int		ny;
	int		index;
	int		*ind=NULL;
	int		n_cycles;
	register int i;
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])	
		return win_printf_OK("This routine finds or imposes indices in a dataset.\n"
        					"Indices correspond to begining or ending of a cycle, or something else.\n\n"
							"a new dataset is created.");
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->ny;	// number of points in time series 

	if (index !=0)
	{ 	if (index & RAMPS_BEGIN)	point_position = 0;
		if (index & RAMPS_MIDDLE)	point_position = 1;
		if (index & RAMPS_END)		point_position = 2;
		if (index & RAMPS_LOWER)	plateau_position = 0;
		if (index & RAMPS_UPPER)	plateau_position = 1;
	}
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices, &point_position, &plateau_position, 
						&bool_remove_bounds, &n_last, &bool_shift_indices, &n_shift, 
						&bool_add_more_indices, &n_more, &N_shift_auto);
	if (i==CANCEL) return(OFF);
	
	if (bool_search_indices==1)
	{ 	i=find_search_domain(ds1->yd, ds1->ny, plateau_position, &y_min, &y_max);
	}
	
	ind = find_set_of_indices(ds1->yd, ny, 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
	if (ind==NULL) return(win_printf_OK("No points left. Found indices were probably only at the boundaries..."));
	
	ds2 = create_and_attach_one_ds (op1, n_cycles, n_cycles, 0);
	for (i=0; i<n_cycles; i++)
	{	ds2->xd[i] = ds1->xd[ind[i]];
		ds2->yd[i] = ds1->yd[ind[i]];
	}
	inherit_from_ds_to_ds(ds2, ds1);
	set_ds_dot_line(ds2);
	set_ds_point_symbol(ds2, "\\pt6\\sq");
	ds2->treatement = my_sprintf(ds2->treatement,"%d indices found.", n_cycles);

	free(ind);
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_find_boundaries









int do_hist_periodic_build_from_power_ds(void)
{
register int 	i, i2, j, j2;
	O_p 	*op2, *op1 = NULL;
static	int	nf=1024+1, ny, n_cycles;
	gsl_histogram *hist;
	double	a_tau;	
	float	h_min=-1, h_max=3;
	d_s 	*ds2, *ds1, *ds_ind;
	pltreg	*pr = NULL;
	int	tau, n_tau;
	int	*tau_index=NULL;
	int	index;
static	char	tau_string[128]="1,2,5,10:5:50";
	double  dt;
	int	*ind; // indices

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine builds an histogram\n"
				"out of the current dataset, and place it in a new plot\n\n"
				"data a(t) is first averaged using a sliding average:\n\n"
				"a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt'\n\n"
				"different a_\\tau(t_i) are used with t_i from a dataset of indices\n"
				"(indices are assumed to be in the next ds, after the current one)");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	ny=ds1->nx;
	if (op1->n_dat<=1)	return(win_printf_OK("I need 2 datasets!\n"
				"active one for the data,\nnext one for indices"));
	ds_ind = (op1->cur_dat == op1->n_dat-1 ) ? op1->dat[0] : op1->dat[op1->cur_dat + 1];
	n_cycles = ds_ind->nx;
	if (n_cycles>=ny) 	return(win_printf_OK("You probably exchanged the 2 datasets ;-)"));

	dt = (double)(ds1->xd[ny-1]-ds1->xd[0])/(ny-1);
//	win_printf("dt is \n%g\n%g\n%g", dt, ds1->xd[2]-ds1->xd[1], ds1->xd[1]);
	if ( (ds1->xd[2]-ds1->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)				return(win_printf_OK("dt is zero !"));
	ind = (int*)calloc(n_cycles, sizeof(int));
	for (i=0; i<n_cycles; i++)	ind[i] = (int)rintf(ds_ind->xd[i]/dt);
	
	h_max = op1->y_hi;
    h_min = op1->y_lo;

	i=win_scanf("number of bins in histogram? %d min %f max %f\n"
			"using averaged data a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt' \n\n"
			"\\tau (integer, nb of pts) ? %s"
			"Integration in time starts at specified times (indices)\n",
			&nf,&h_min,&h_max,&tau_string);
	if (i==CANCEL) return OFF;

	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	return(win_printf("bad values for \\tau !"));
	if ((ind[n_cycles-1]+tau_index[n_tau-1])>=ny)	return(win_printf_OK("larger tau (%d) is too large!", tau_index[n_tau-1]));
			
	if ((op2 = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	
	for (i=0; i<n_tau; i++)
	{	tau = tau_index[i];

		if (i!=0)
		if ((ds2 = create_and_attach_one_ds(op2, nf, nf, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");

		(gsl_histogram *)hist = gsl_histogram_alloc(nf);
		gsl_histogram_set_ranges_uniform (hist, (double)h_min, (double)h_max);

		for (i2=0; i2<n_cycles; i2++)
		{	j=ind[i2];
			a_tau=0;
			for (j2=0; j2<tau; j2++)
			{	a_tau += (double)ds1->yd[(j+j2)];		// average over tau points
			}
			a_tau = (double)(a_tau/tau);
			gsl_histogram_increment(hist, (double)a_tau); // histogram of averaged data
		}

		histogram_to_ds(hist, ds2, nf);
		gsl_histogram_free(hist);

		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"histogram, averaged data over \\tau=%d points", tau);
	
	}

	set_plot_title(op2,"histogram");
	set_plot_x_title(op2, "x (%d values)",n_cycles);
	set_plot_y_title(op2, "pdf");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

    	op2->iopt |= YLOG;

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_hist_periodic_build_from_ds' */







int do_hist_periodic_build_from_power_files(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memory 
register int i2, j, j2;
	O_p 	*op2, *op1=NULL;
static int	n_bin=100;
	int		n_power, n_excitation=0, n_cycles, n_cycles_total=0;
	gsl_histogram *hist;
	double	a_tau;
static float	h_min=-5, h_max=15;
	d_s 	*ds2;
	pltreg	*pr = NULL;
	int		tau, i_file, n_tau, n_files;	// current tau, current file number, number of taus, number of files
	int		*tau_index=NULL;		// all the taus
	int		*files_index=NULL;		// all the files
	int		index;
static	char	tau_string[128]		="1,2,5,10:5:50";
static	char files_ind_string[128]	="1:1:20"; 
static	char file_extension[8]		=".dat";
static	char	power_root_filename[128]	     = "Pcl_";
static	char excitation_root_filename[128] = "M";
	char	s[512];
	char	filename[256];
	int		*ind=NULL; // indices
	float	*excitation=NULL, *power;
	float	y_min=0, y_max=1;
static float	epsilon=0.001;
	float	data_min, data_max;	// for detection of ramps
static int	bool_remove_bounds=1;
static int  n_last=1; // how many points should be removed at the end of the dataset
static int	bool_report=0;
static int	plateau_position=0; // upper - lower plateau
static int	point_position=1;	// start - middle - end
static int  bool_add_more_indices=0, bool_shift_indices=0;
static int	n_more=1, n_shift;
static int  bool_search_indices=1;
static int	N_shift_auto=1;
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine builds a set of histograms\n"
				"out of a set of files A, and places it in a new plot\n\n"
				"data a(t) from file 1 is first averaged using a sliding average:\n\n"
				"a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt'\n\n"
				"different a_\\tau(t_i) are used with t_i from indices found in files B\n");
	}
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of file with power data %%s"
			"root name of file with excitation data (to find indices)\n (type 'none' to discard) %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n"
			"%%b print to screen a report for each file");
	i=win_scanf(s, &power_root_filename, &excitation_root_filename, &files_ind_string, &file_extension, &bool_report);
	if (i==CANCEL) return(OFF);
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));

	if (strcmp(excitation_root_filename, "none")==0) 	bool_search_indices=0;
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices, &point_position, &plateau_position, 
						&bool_remove_bounds, &n_last, &bool_shift_indices, &n_shift, 
						&bool_add_more_indices, &n_more, &N_shift_auto);
	if (i==CANCEL) return(OFF);

	i=win_scanf("number of bins in histogram? %d min %f max %f\n"
			"using averaged data {\\pt16 a_\\tau(t) = {1 \\over \\tau} \\int_t_i^{t_i+\\tau} a(t') dt'}\n\n"
			"\\tau (integer, nb of pts) ? %s"
			"Integration in time starts at times t_i found with previous rule.",
			&n_bin, &h_min, &h_max, &tau_string);
	if (i==CANCEL) return OFF;
	
	if (bool_add_more_indices==1)
	{	n_more -= tau_index[n_tau-1]; // not to escape from the ramp
	}				
	
	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	return(win_printf("bad values for \\tau !"));
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	if ((op2 = create_and_attach_one_plot(pr, n_bin, n_bin, 0))==NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%d%s", power_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_power=load_data_bin(filename, &power);

		if ( (bool_search_indices==1) && (k==0) )	// first file: we autodetect some parameters		
		{	win_scanf("Precision \\epsilon ? %8f\n(depends on the noise level on the plateaus)", &epsilon);
			data_min=data_max=excitation[0];
			for (i=1; i<n_excitation-1; i++)
			{	if (excitation[i]<data_min) data_min=excitation[i];
				if (excitation[i]>data_max) data_max=excitation[i];
			}
			if (plateau_position==0)
			{ y_min = data_min-epsilon;
		  	  y_max = data_min+epsilon;
			}
			if (plateau_position==1)
			{ y_min = data_max-epsilon;
			  y_max = data_max+epsilon;
			}
		}
		
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);	
		
		if (bool_search_indices==1)
		{	sprintf(filename, "%s%d%s", excitation_root_filename, i_file, file_extension);
			sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
			n_excitation=load_data_bin(filename, &excitation);
		}
		ind = find_set_of_indices(excitation, 
						( (bool_search_indices==1) ? n_excitation : n_excitation - tau_index[n_tau-1]), 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
		if (ind==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));

		n_cycles_total += n_cycles;
		if (n_cycles<1) 	return(win_printf_OK("Mmm... not enough ramps in file %s", filename));
		
		if (bool_report==1)	
		{	if (bool_search_indices==1)
					win_printf_OK("file number %d/%d\n(%s and associated excitation)\n\n"
							"%d points in excitation\n%d points in power\n\n%d usable cycles found", 
							k+1, n_files, filename, n_excitation, n_power, n_cycles);
		
			else	win_printf_OK("file number %d/%d\n(%s)\n\n"
							"%d points in power file\n%d usable windows, separated by %d points\n"
							"(cf larger \\tau is %d)", 
							k+1, n_files, filename, n_power, n_cycles, N_shift_auto, tau_index[n_tau-1]);
		}

		sprintf(s, "%d/%d cumulating histograms", k+1, n_files); spit(s);
							
		for (i=0; i<n_tau; i++)
		{	tau=tau_index[i];

			if (k==0) // first file : we create the datasets
			{	if (i!=0) // not the first tau: we add a dataset
				{	if ((ds2 = create_and_attach_one_ds(op2, n_bin, n_bin, 0)) == NULL)	
						return win_printf_OK("cannot create dataset !");
				}
				ds2->treatement = my_sprintf(ds2->treatement,"histogram, averaged data over \\tau=%d points", tau);
						
				(gsl_histogram *)hist = gsl_histogram_alloc(n_bin);
				gsl_histogram_set_ranges_uniform (hist, (double)h_min, (double)h_max);
			}
			else 
			{	ds2=op2->dat[i];
				ds_to_histogram(ds2, &hist); // hist will be allocated perfectly
			}

			for (i2=0; i2<n_cycles; i2++)
			{	j=ind[i2];
				a_tau=0.;
				for (j2=0; j2<tau; j2++)
				{	a_tau+=power[(j+j2)];		// average over tau points
				}
				gsl_histogram_increment(hist, (double)(a_tau/tau)); // histogram of averaged data
			}

			histogram_to_ds(hist, ds2, n_bin);
			gsl_histogram_free(hist);
	
		} //end of loop over i, ie, loop over tau

		free(excitation);
		free(power);
		free(ind);
		
	} //end of the loop over k, ie, loop over the files;
	free(tau_index);
	free(files_index);
	
	set_plot_title(op2,"histograms");
	set_plot_x_title(op2, "A_\\tau = \\frac{1}{\\tau} \\int_t^{t+\\tau} A(t') dt'  (%d %s from %d files)", 
			n_cycles_total, (bool_search_indices==1) ? "cycles" : "windows", n_files);
	set_plot_y_title(op2, "pdf");
	op2->filename = Transfer_filename(op1->filename);
   	op2->dir = Mystrdup(op1->dir);
   	op2->iopt |= YLOG;

	return(refresh_plot(pr, UNCHANGED));
} /* end of function 'do_hist_periodic_build_from_power_files' */





int do_hist_periodic_build_from_NI4472(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memory 
register int	i2, j, j2;
	O_p 		*op2, *op1=NULL;
static int	n_bin=81;
	int		n_power, n_excitation, n_cycles, n_cycles_total=0;
	gsl_histogram *hist;
	float	a_tau;
static float	h_min=-1, h_max=1;
	d_s 		*ds2;
	pltreg	*pr = NULL;
	int		tau, i_file, n_tau, n_files;	// current tau, current file number, number of taus, number of files
	int		*tau_index=NULL;		// all the taus
	int		*files_index=NULL;		// all the files
	int		index;
static	char	tau_string[128]		="100:100:200";
static	char files_ind_string[128]	="0:1:2"; 
static	char file_extension[8]		=".bin";
static	char	root_filename[128]	     = "run02";
    long	N_total;
static int 	N_decimate=10;
	long		header_length;
static int	N_channels=3, excitation_channel=1, response_channel=3;
static float	excitation_factor=1., response_factor=14000;
	char		s[512];
	char		filename[256];
	int		*ind=NULL, *ind2=NULL; // indices
	int		n_ind1, n_ind2;
	float	*excitation, *power;
	float	y_min=0, y_max=1;
static float	epsilon=0.001;		// precision for the dectection of ramps
static float	data_min, data_max;	// for detection of ramps
	float	f_acq, dt;
static float	start_time=0.1, cycle_period=0.5;
static int	bool_remove_bounds=1;
static int	bool_impose_indices=0;
static int	bool_report=0;
	int		bool_normalize_u=1;
static int	plateau_position=0;
static int	point_position=1;
	int		ret=0;
	double	*u1, *U, *I, *i_R;
static float	U1_I_factor=1./(5e10), U1_offset=0., i_p=(1e-14);
	double	mean_U, var_U, mean_I, var_I, mean_i_R, var_i_R, mean_W, var_W, mean_Q, var_Q;
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine builds a set of histograms\n"
				"out of a set of files, and places it in a new plot\n\n"
				"data a(t) from file 1 is first averaged using a sliding average:\n\n"
				"a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt'\n\n"
				"different a_\\tau(t_i) are used with t_i from indices found in file A\n\n"
				"This is a batch, written for the study of fluctuations in a resistor\n"
				"and it should be ran with caution!");
	}
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of files %%s"
			"range of files %%s"
			"file extension (note: expected files are from NI4472) %%s\n"
			"Number of channels in a file : %%2d\n"
			"channel with excitation : %%2d    divide by factor %%8f\n"
			"channel with response  : %%2d    divide by factor %%8f\n"
			"decimate data by a factor of %%3d\n\n"
			"%%b print to screen a report for each file");
	i=win_scanf(s, &root_filename, &files_ind_string, &file_extension, 
				&N_channels, &excitation_channel, &excitation_factor, &response_channel, &response_factor,
				&N_decimate, &bool_report);
	if (i==CANCEL) return(OFF);
	if (excitation_factor*response_factor<1e-50) return(win_printf_OK("factors are too small (vanishing ?)"));
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));

	i=win_scanf("How to find the points t_i where to start from ?\n"
			"%R first points\n"
			"%r middle points\n"
			"%r last points\n"
			"of\n"
			"%R lower plateau\n"
			"%r upper plateau\n"
			"using precision %8f\n\n"
			"%b forget this rule, and impose the indices\n"
			"%b remove first and last found indices", 
			&point_position, &plateau_position, &epsilon, &bool_impose_indices, &bool_remove_bounds);
	if (i==CANCEL) return(OFF);
	
	if (bool_impose_indices==1)
	{ sprintf(s, "Imposing indices\n"
			"cycle period (in seconds) : %%10f\n"
			"starting time (in seconds) : %%10f\n\n");
	i=win_scanf(s, &cycle_period, &start_time);
	if (i==CANCEL) return(OFF);
	}
			
	sprintf(s,"How should I process the data ?\n\n"
			"compute I=i_c+i_p from excitation U_1 according to\n"
			"I(t) = G*(U_1(t) - U_0)  + i_p\n"
			"U_0 = %%10f\n G = %%10f\ni_p = %%10f");
	i=win_scanf(s, &U1_offset, &U1_I_factor, &i_p); 
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("number of bins in histogram? %d min %f max %f\n"
			"using averaged data {\\pt16 a_\\tau(t) = {1 \\over \\tau} \\int_t_i^{t_i+\\tau} a(t') dt'}\n\n"
			"\\tau (integer, nb of pts) ? %s"
			"Integration in time starts at times t_i found with previous rule.",
			&n_bin, &h_min, &h_max, &tau_string);
	if (i==CANCEL) return(OFF);
	
	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	return(win_printf("bad values for \\tau !"));
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	if ((op2 = create_and_attach_one_plot(pr, n_bin, n_bin, 0))==NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];

		sprintf(filename, "%s%d%s", root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);

		ret=read_header_NI4472(filename, &header_length, &N_total, &f_acq);
		if (ret==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
		n_excitation=load_NI4472_data(filename, header_length, N_total, N_channels, excitation_channel, N_decimate, &excitation);				
		n_power     =load_NI4472_data(filename, header_length, N_total, N_channels, response_channel,   N_decimate, &power);
		dt=(float)N_decimate/f_acq;
		
		sprintf(s, "%d/%d file %s: converting data", k+1, n_files, filename); spit(s);
				
		if ((n_excitation!=n_power) || ((N_total/(N_channels*N_decimate))!=n_excitation)) 
			return(win_printf_OK("error loading data\nincorrect number of points"));
		u1 = (double*)calloc(n_excitation, sizeof(double));
		U  = (double*)calloc(n_excitation, sizeof(double));
		for (i=0; i<n_excitation; i++)
		{	u1[i]	= (double)(excitation[i]/excitation_factor);
			U[i]  	= (double)(power[i]/response_factor);
		}
			
		sprintf(s, "%d/%d file %s: searching cycles", k+1, n_files, filename); spit(s);

		if (bool_impose_indices==1)
		{	ind=impose_indices(dt, cycle_period, start_time, n_excitation, &n_ind2);
		}
		else
		{
		if (k==0)	// first file: we autodetect some parameters		
		{	win_scanf("Precision \\epsilon ? %8f\n(depends on the noise level on the plateaus)", &epsilon);
			data_min=data_max=excitation[0];
			for (i=1; i<n_excitation-1; i++)
			{	if (excitation[i]<data_min) data_min=excitation[i];
				if (excitation[i]>data_max) data_max=excitation[i];
			}
			if (plateau_position==0)
			{ y_min = data_min-epsilon;
		  	  y_max = data_min+epsilon;
		  	}
		  	if (plateau_position==1)
			{ y_min = data_max-epsilon;
		  	  y_max = data_max+epsilon;
		  	}
		}
		
		// refine the search for indices:
		ind   = find_interval_float (excitation, n_excitation-1, y_min, y_max, &n_ind1);
		if (point_position==0)	ind2  = find_and_keep_firsts_int(ind, n_ind1, &n_ind2);
		if (point_position==1)	ind2  = find_and_keep_middles_int(ind, n_ind1, &n_ind2);
		if (point_position==2)	ind2  = find_and_keep_lasts_int(ind, n_ind1, &n_ind2);
		free(ind);
		ind=ind2;
		}
	
		// eventually remove the first and last indices:
		if (bool_remove_bounds==1)
		{	n_ind2 -= 2;  // there will be 2 points less : the first and the last are removed.
			for (i=0; i<n_ind2; i++)		ind[i]=ind[i+1];
		}
		
		if (n_ind2<=0) return(win_printf_OK("file %s\nNo points left. Found indices were only at the boundaries...",filename));
	
		
		// count the number of period (ie, number of cycles), and perform some tests:
		n_cycles = n_ind2;
		n_cycles_total += n_cycles;
		if (n_cycles<1) 	return(win_printf_OK("Mmm... not enough ramps in file %s", filename));
		
		if ((ind[n_cycles-1]+tau_index[n_tau-1])>=n_power)	
					return(win_printf_OK("filen %s\nlarger tau %d is too large!\nlast index is %d\n%d points in dataset", 
							filename, tau_index[n_tau-1], ind[n_cycles-1], n_power));
					
		// eventually print some information on the file:					
		if (bool_report==1)	win_printf_OK("file %s (file %d/%d)\n\n"
							"%d channels, %d points, %d points per channel\n"
							"decimation by %d, %d points kept\n\n"
							"acquisition frequency %g\n\n"
							"%d usable cycles found", 
							filename, k+1, n_files, N_channels, N_total, n_excitation*N_decimate, 
							N_decimate, n_excitation, f_acq, n_cycles);
		
		sprintf(s,"%d/%d file %s: computing quantities", k+1, n_files, filename); spit(s);

		// compute the interesting quantity out of the data read from the file:
		I  =(double*)calloc((size_t)n_excitation, sizeof(double));
		i_R=(double*)calloc((size_t)n_excitation, sizeof(double));
			mean_U = gsl_stats_mean(U, 1, n_excitation);
			var_U  = gsl_stats_sd_m(U, 1, n_excitation, mean_U);
		compute_I   (u1, n_excitation, (double)U1_I_factor, (double)U1_offset, (double)i_p, I);
			mean_I = gsl_stats_mean(I, 1, n_excitation);
			var_I  = gsl_stats_sd_m(I, 1, n_excitation, mean_I);
		compute_i_R (U, n_excitation, get_C(), I, dt, i_R);
			mean_i_R = gsl_stats_mean(i_R+1, 1, n_excitation-1);
			var_i_R  = gsl_stats_sd_m(i_R+1, 1, n_excitation-1, mean_i_R);
		compute_power(U,   n_excitation,   get_R(), I,   dt, bool_normalize_u, u1);	// computes the work (power), puts it in u1
			mean_W = gsl_stats_mean(u1+1, 1, n_excitation-1);
			var_W  = gsl_stats_sd_m(u1+1, 1, n_excitation-1, mean_W);
		compute_power(U+1, n_excitation-2, get_R(), i_R, dt, bool_normalize_u, U+1);	// computes the heat (power), puts it in U
			mean_Q = gsl_stats_mean(U+1, 1, n_excitation-2);
			var_Q  = gsl_stats_sd_m(U+1, 1, n_excitation-2, mean_Q);
		for (i=0; i<n_excitation; i++)
		{	power[i] = (float)(u1[i]);
		}

		if (bool_report==1)	win_printf("Assuming R=%6g \\Omega, C=%6g F and i_p=%6g A\n\n"
				"mean U is %6g V, with variance %6g V\n"
				"mean I is %6g A, with variance %6g A\n"
				"mean i_R is %6g A, with variance %6g A\n\n"
				"mean W is %6g k_BT, variance is %6g k_BT\n"
				"mean Q is %6g k_BT, variance is %6g k_BT\n"
				"for time lags dt = %g s (facq = %g Hz)",
				get_R(), get_C(), i_p,  
				mean_U, var_U, mean_I, var_I, mean_i_R, var_i_R, mean_W, var_W, mean_Q, var_Q, dt, f_acq/N_decimate);

		sprintf(s, "%d/%d file %s: cumulating histograms", k+1, n_files, filename); spit(s);
									
		// here, we cumulate the data and build the histograms:
		for (i=0; i<n_tau; i++)
		{	tau=tau_index[i];

			if (k==0) // first file : we create the datasets
			{	if (i!=0) // not the first tau: we add a dataset
				{	if ((ds2 = create_and_attach_one_ds(op2, n_bin, n_bin, 0)) == NULL)	
						return win_printf_OK("cannot create dataset !");
				}
				ds2->treatement = my_sprintf(ds2->treatement,"histogram, data averaged over \\tau=%d points", tau);
						
				(gsl_histogram *)hist = gsl_histogram_alloc(n_bin);
				gsl_histogram_set_ranges_uniform (hist, (double)h_min, (double)h_max);
			}
			else 
			{	ds2=op2->dat[i];
				ds_to_histogram(op2->dat[i], &hist); // hist will be allocated perfectly
			}

			for (i2=0; i2<n_cycles; i2++)
			{	j=ind[i2];
				a_tau=0;
				for (j2=0; j2<tau; j2++)
				{	a_tau+=power[(j+j2)];		// average over tau points
				}
				gsl_histogram_increment(hist, (double)(a_tau/tau)); // histogram of averaged data
			}

			histogram_to_ds(hist, op2->dat[i], n_bin);
			gsl_histogram_free(hist);
	
		} //end of loop over i, ie, loop over tau

		free(excitation);
		free(power);
		free(ind);
		free(I); free(U);
		free(i_R); free(u1);
		
	} //end of the loop over k, ie, loop over the files;
	free(tau_index);
	free(files_index);
	
	set_plot_title(op2,"histograms");
	set_plot_x_title(op2, "x (%d cycles from %d files)", n_cycles_total, n_files);
	set_plot_y_title(op2, "pdf");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

    	op2->iopt |= YLOG;

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_hist_periodic_build_from_NI4472' */









int do_hist_periodic_build_from_M_dep_files(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memory 
register int	i2, j, j2;
	O_p 		*op2, *op1=NULL;
static int	n_bin=100;
	int		n_theta, n_excitation, n_cycles, n_cycles_total=0;
	gsl_histogram *hist;
	double	a_tau;
static float	h_min=-5, h_max=15;
	d_s 		*ds2;
	pltreg	*pr = NULL;
	int		tau, i_file, n_tau, n_files;	// current tau, current file number, number of taus, number of files
	int		*tau_index=NULL;		// all the taus
	int		*files_index=NULL;		// all the files
	int		index;
static	char	tau_string[128]		="1,2,5,10:5:50";
static	char files_ind_string[128]	="1:1:20"; 
static	char file_extension[8]		=".dat";
static	char theta_root_filename[128]   = "dep";
static	char excitation_root_filename[128] = "M";
static	char indices_root_filename[128] = "M";
	char		s[512];
	char		filename[256];
	int		*ind=NULL; // indices
	float	*excitation, *theta; // *power;
	float	y_min=0, y_max=1;
static int	bool_remove_bounds=1 ;
static int  	n_last=1; // how many points should be removed at the end of the dataset
static int	bool_report=0;
static int	plateau_position=0;
static int	point_position=1;
static int	bool_add_more_indices=0, bool_shift_indices=0, bool_substract_vanZon=1, indice_reference=0;
static int	n_more=1, n_shift=0;
	int  	bool_search_indices=1;
	int		N_shift_auto=1;
static float	fec=2048.;
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine builds a set of histograms\n"
				"out of a set of files of \\theta(t) and M(t).\n\n"
				"data W_\\tau(t) is computed as :\n\n"
				"W_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} -(M(t')-\\epsilonM(t)).{\\frac{d\\theta}{dt}}(t') dt'\n\n"
				"with \\epsilon = 0 or 1\n"
				"A new plot is created");
	}
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of file with angle \\theta %%s"
			"root name of file with excitation M %%s"
			"root name of file to find indices %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n"
			"%%b print to screen a report for each file");
	i=win_scanf(s, &theta_root_filename, &excitation_root_filename, &indices_root_filename, &files_ind_string, &file_extension, &bool_report);
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("sampling frequency ? %8f", &fec);
	if (i==CANCEL) return(OFF);
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices, &point_position, &plateau_position, 
							&bool_remove_bounds, &n_last, &bool_shift_indices, &n_shift, 
							&bool_add_more_indices, &n_more, &N_shift_auto);
	if (i==CANCEL) return(OFF);
		
	i=win_scanf("%b Do you want the van Zon power/work ?\n\n"
				"W = \\frac{1}{\\tau}\\int_t^{t+\\tau} - [ M(t') - M(t)] \\frac{{d\\theta}{dt}} (t') dt'\n\n"
				"if you want this, what should be substracted ?\n"
				"%R M(t) or %r M(t-1)\n"
				"{\\pt6 (best is M(t)}", &bool_substract_vanZon, &indice_reference);
	if (i==CANCEL) return(D_O_K);
				
	i=win_scanf("number of bins in histogram? %d min %f max %f\n"
			"using averaged data {\\pt16 a_\\tau(t) = {1 \\over \\tau} \\int_t_i^{t_i+\\tau} a(t') dt'}\n\n"
			"\\tau (integer, nb of pts) ? %s"
			"Integration in time starts at times t_i found with previous rule.",
			&n_bin, &h_min, &h_max, &tau_string);
	if (i==CANCEL) return OFF;			
	
	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	return(win_printf("bad values for \\tau !"));
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	if ((op2 = create_and_attach_one_plot(pr, n_bin, n_bin, 0))==NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	
	if (bool_add_more_indices==1)
	{	n_more -= n_shift;
		n_more -= tau_index[n_tau-1]; // not to escape from the ramp
	}	
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%d%s", theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);

		sprintf(filename, "%s%d%s", indices_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_excitation=load_data_bin(filename, &excitation);
		
		if (n_excitation!=n_theta) return(win_printf_OK("M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		if ( (bool_search_indices==1) && (k==0))	// first file: we autodetect some parameters		
		{	i=find_search_domain(excitation, n_excitation, plateau_position, &y_min, &y_max);
			if (i==CANCEL) return(D_O_K);
		}
	
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);
		
		ind = find_set_of_indices(excitation, 
						( (bool_search_indices==1) ? n_excitation : n_excitation - tau_index[n_tau-1]), 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
		if (ind==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));
					
		if ((ind[n_cycles-1]+tau_index[n_tau-1])>=n_excitation)
		{	i=win_printf_OK("filen %s\nlarger tau %d is too large!\nlast index is %d\n%d points in dataset\n\n"
						"click CANCEL to abort or OK to remove one more period",
						filename, tau_index[n_tau-1], ind[n_cycles-1], n_excitation);
			if (i==CANCEL) return(D_O_K);
			n_cycles --;
		}

		if (strcmp(indices_root_filename, excitation_root_filename)!=0)
		{	sprintf(filename, "%s%d%s", excitation_root_filename, i_file, file_extension);
			sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
			n_excitation=load_data_bin(filename, &excitation);
		
			if (n_excitation!=n_theta) return(win_printf_OK("forcing M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		}
		
		n_cycles_total += n_cycles;
		if (n_cycles<1) 	return(win_printf_OK("Mmm... not enough ramps in file %s", filename));
			
		if (bool_report==1)	
		{	if (bool_search_indices==1)
				win_printf_OK("file number %d/%d\n(\\theta in %s)\n\n"
							"%d points in \\theta\n%d points in excitation M\n\n%d usable cycles found", 
							k+1, n_files, filename, n_theta, n_excitation, n_cycles);
			else
				win_printf_OK("file number %d/%d\n(%s)\n\n"
							"%d points in \\theta file\n%d usable windows, separated by %d points\n"
							"(cf larger \\tau is %d)", 
							k+1, n_files, filename, n_theta, n_cycles, N_shift_auto, tau_index[n_tau-1]);
		}

		sprintf(s, "%d/%d cumulating histograms", k+1, n_files); spit(s);
							
		for (i=0; i<n_tau; i++)
		{	tau=tau_index[i];

			if (k==0) // first file : we create the datasets
			{	if (i!=0) // not the first tau: we add a dataset
				{	if ((ds2 = create_and_attach_one_ds(op2, n_bin, n_bin, 0)) == NULL)	
						return win_printf_OK("cannot create dataset !");
				}
				ds2->treatement = my_sprintf(ds2->treatement,"histogram, averaged data over \\tau=%d points", tau);
						
				(gsl_histogram *)hist = gsl_histogram_alloc(n_bin);
				gsl_histogram_set_ranges_uniform (hist, (double)h_min, (double)h_max);
			}
			else 
			{	ds2=op2->dat[i];
				ds_to_histogram(ds2, &hist); // hist will be allocated perfectly
			}

			for (i2=0; i2<n_cycles; i2++)
			{	j=ind[i2];	// point de départ de l'intégration
				a_tau=0.;
				for (j2=0; j2<tau; j2++)
				{	a_tau += - ( (double)excitation[j+j2] - (double)bool_substract_vanZon*excitation[j - indice_reference] )
								*((double)theta[j+j2]-theta[j+j2-1]);	// average over tau points
				}
				gsl_histogram_increment(hist, (double)(a_tau*fec/(kT_Fred*(double)tau))); // histogram of averaged data
			}

			histogram_to_ds(hist, ds2, n_bin);
			gsl_histogram_free(hist);
	
		} //end of loop over i, ie, loop over tau

		free(excitation);
		free(theta);
	//	free(power);
		free(ind);
		
	} //end of the loop over k, ie, loop over the files;
	free(tau_index);
	free(files_index);
	
	set_plot_title(op2,"histograms");
	if (bool_search_indices==1) 
		set_plot_x_title(op2, "\\frac{1}{\\tau} W_\\tau (%d cycles from %d files)", n_cycles_total, n_files);
	else
		set_plot_x_title(op2, "\\frac{1}{\\tau} W_\\tau (%d windows from %d files)", n_cycles_total, n_files);
	set_plot_y_title(op2, "pdf");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

    	op2->iopt |= YLOG;

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_hist_periodic_build_from_M_dep_files' */







int do_stats_periodic_build_from_M_dep_files(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memory 
register int	i2, j, j2, m;
	pltreg	*pr = NULL;
	O_p 	*op2=NULL;
	d_s 	*ds_mean=NULL, *ds_sigma=NULL, *ds_skewness=NULL, *ds_kurtosis=NULL, *ds_GC=NULL, *ds_GC4=NULL;
	double	a_tau, mean, sigma, skewness=0., kurtosis=0., GC_estimate=0., GC_tmp, GC4_estimate, good_factor;
	double	*y_tau, *total_sigma, *dispersion;
static float	fec=2048.;
	float	*excitation, *theta; // *power;
	float	y_min=0, y_max=1;
	int		tau=1, i_file, n_tau, n_files;	// current tau, current file number, number of taus, number of files
	int		*tau_index=NULL;		// all the taus
	int		*files_index=NULL;		// all the files
	int		index;
static	char	tau_string[128]		="1:50";
static	char files_ind_string[128]	="1:1:20"; 
static	char file_extension[8]		=".dat";
static	char theta_root_filename[128]   = "dep";
static	char excitation_root_filename[128] = "M";
static	char indices_root_filename[128] = "M";
	int		*ind=NULL; // indices
static int	bool_remove_bounds=1 ;
static int  n_last=1; // how many points should be removed at the end of the dataset
static int	bool_report=0;
static int	plateau_position=0;
static int	point_position=1;
static int	bool_add_more_indices=0, bool_shift_indices=0, bool_substract_vanZon=1, indice_reference=0;
static int 	bool_mean=10, bool_sigma=1, bool_skew=1, bool_kurtosis=1, bool_substract_3_to_kurtosis=0, 
			bool_GC=1, bool_GC4=1, tau_units=0, bool_dispersion=0;
static int	n_more=1, n_shift=0;
	int		n_theta, n_excitation, n_cycles, n_cycles_total=0, n_intervals;
	int  	bool_search_indices=1;
	int		N_shift_auto=1;
	char	s[512];
	char	filename[256];

	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine computes moments\n"
				"out of a set of files of \\theta(t) and M(t).\n\n"
				"data W_\\tau(t) is computed as :\n\n"
				"W_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} -(M(t')-\\epsilonM(t)).{\\frac{d\\theta}{dt}}(t') dt'\n\n"
				"with \\epsilon = 0 or 1\n"
				"A new plot is created");
	}
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of file with angle \\theta %%s"
			"root name of file with forcing M %%s"
			"root name of file to detect indices %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n"
			"%%b print to screen a report for each file");
	i=win_scanf(s, &theta_root_filename, &excitation_root_filename, &indices_root_filename, 
			&files_ind_string, &file_extension, &bool_report);
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("sampling frequency ? %8f", &fec);
	if (i==CANCEL) return(OFF);
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices, &point_position, &plateau_position, 
							&bool_remove_bounds, &n_last, &bool_shift_indices, &n_shift, 
							&bool_add_more_indices, &n_more, &N_shift_auto);
	if (i==CANCEL) return(OFF);
		
	i=win_scanf("%b Do you want the van Zon power/work ?\n\n"
				"W = \\frac{1}{\\tau}\\int_t^{t+\\tau} - [ M(t') - M(t)] \\frac{{d\\theta}{dt}} (t') dt'\n\n"
				"if you want this, what should be substracted ?\n"
				"%R M(t) or %r M(t-1)\n"
				"{\\pt6 (best is M(t)}", &bool_substract_vanZon, &indice_reference);
	if (i==CANCEL) return(D_O_K);
				
	i=win_scanf("{\\pt14\\color{yellow}Compute moments}\n"
			"%b mean\n"
			"%b \\sigma\n"
			"%b skewness\n"
			"%b kurtosis (%b substract 3)\n"
			"%b GC estimator 2<x>/\\sigma^2\n"
			"%b GC estimator at order 4\n"
			"%b add error bars for dispersion from file to file"
			"value(s) of \\tau : %s\n"
			"\\tau units are %R points or %r seconds (from f_{ec})\n",
			&bool_mean, &bool_sigma, &bool_skew, &bool_kurtosis, &bool_substract_3_to_kurtosis, 
			&bool_GC, &bool_GC4, &bool_dispersion, &tau_string, &tau_units);
	if (i==CANCEL) return(OFF);
	if ( (bool_mean + bool_sigma + bool_skew + bool_kurtosis + bool_GC + bool_GC4) == 0) return(OFF);
	if (bool_GC4==1)  
	{	if ( (bool_skew * bool_kurtosis * bool_GC)==0)
		{	i=win_printf("I will also compute the skewness, the kurtosis,\nand GC estimate at order 2\n"
						"associated datasets will be created.");
			if (i==CANCEL) return(D_O_K);
		}
		bool_skew     = 1;
		bool_kurtosis = 1;
		bool_GC       = 1;
		
	}
	
	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	return(win_printf_OK("bad values for \\tau !"));
//	if (tau_index[0]==1) return(win_printf_OK("\\tau = 1 is not allowed..."));

	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)				return win_printf_OK("cannot find plot region");
	
	if (bool_add_more_indices==1)
	{	n_more -= n_shift;
		n_more -= tau_index[n_tau-1]; // not to escape from the ramp
	}	
	
	if ((op2 = create_and_attach_one_plot(pr, n_tau, n_tau, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
		
	if (bool_mean==1)
	{	ds_mean=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		ds_mean->treatement = my_sprintf(ds_mean->treatement,"mean vs \\tau");
		set_ds_source(ds_mean, "data from %d files %s and %s", n_files, theta_root_filename, excitation_root_filename);
	}
	if (bool_sigma==1)
	{	ds_sigma=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		ds_sigma->treatement = my_sprintf(ds_sigma->treatement,"sigma vs \\tau");
		set_ds_source(ds_sigma, "data from %d files %s and %s", n_files, theta_root_filename, excitation_root_filename);
	}
	if (bool_skew==1) 	
	{	ds_skewness=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		ds_skewness->treatement = my_sprintf(ds_skewness->treatement,"skewness vs \\tau");
		set_ds_source(ds_skewness, "data from %d files %s and %s", n_files, theta_root_filename, excitation_root_filename);
	}
	if (bool_kurtosis==1)
	{ 	ds_kurtosis=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		ds_kurtosis->treatement = my_sprintf(ds_kurtosis->treatement,"kurtosis%s vs \\tau", (bool_substract_3_to_kurtosis==1) ? "-3" : " ");
		set_ds_source(ds_kurtosis, "data from %d files %s and %s", n_files, theta_root_filename, excitation_root_filename);
	}
	if (bool_GC==1)
	{ 	ds_GC=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		ds_GC->treatement = my_sprintf(ds_GC->treatement,"GC estimator 2<x>/\\sigma^2 vs \\tau");
		set_ds_source(ds_GC, "data from %d files %s and %s", n_files, theta_root_filename, excitation_root_filename);
	}
	if (bool_GC4==1)
	{ 	ds_GC4=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		ds_GC4->treatement = my_sprintf(ds_GC4->treatement,"GC estimator at order 4");
		set_ds_source(ds_GC4, "data from %d files %s and %s", n_files, theta_root_filename, excitation_root_filename);
	}
	remove_ds_n_from_op(op2, 0);
	
	total_sigma = (double*)calloc(n_tau, sizeof(double));
	dispersion  = (double*)calloc(n_tau, sizeof(double));
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%d%s", theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);

		sprintf(filename, "%s%d%s", indices_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_excitation=load_data_bin(filename, &excitation);
		
		if (n_excitation!=n_theta) return(win_printf_OK("forcing M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		if ( (bool_search_indices==1) && (k==0))	// first file: we autodetect some parameters		
		{	i=find_search_domain(excitation, n_excitation, plateau_position, &y_min, &y_max);
			if (i==CANCEL) return(D_O_K);
		}
	
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);
		
		ind = find_set_of_indices(excitation, 
						( (bool_search_indices==1) ? n_excitation : n_excitation - tau_index[n_tau-1]), 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
		if (ind==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));
					
		if ((ind[n_cycles-1]+tau_index[n_tau-1])>=n_excitation)
		{	i=win_printf_OK("filen %s\nlarger tau %d is too large!\nlast index is %d\n%d points in dataset\n\n"
						"click CANCEL to abort or OK to remove one more period",
						filename, tau_index[n_tau-1], ind[n_cycles-1], n_excitation);
			if (i==CANCEL) return(D_O_K);
			n_cycles --;
		}

		n_cycles_total += n_cycles;
		if (n_cycles<1) 	return(win_printf_OK("Mmm... not enough ramps in file %s", filename));
			
		if (strcmp(indices_root_filename, excitation_root_filename)!=0)
		{	sprintf(filename, "%s%d%s", excitation_root_filename, i_file, file_extension);
			sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
			n_excitation=load_data_bin(filename, &excitation);
		
			if (n_excitation!=n_theta) return(win_printf_OK("forcing M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		}
	
		if (bool_report==1)	
		{	if (bool_search_indices==1)
				win_printf_OK("file number %d/%d\n(\\theta in %s)\n\n"
							"%d points in \\theta\n%d points in excitation M\n\n%d usable cycles found", 
							k+1, n_files, filename, n_theta, n_excitation, n_cycles);
			else
				win_printf_OK("file number %d/%d\n(%s)\n\n"
							"%d points in \\theta file\n%d usable windows, separated by %d points\n"
							"(cf larger \\tau is %d)", 
							k+1, n_files, filename, n_theta, n_cycles, N_shift_auto, tau_index[n_tau-1]);
		}

		sprintf(s, "%d/%d computing moments", k+1, n_files); spit(s);
		
		for (i=0; i<n_tau; i++)
		{	tau=tau_index[i];
			good_factor = (double)fec/(kT_Fred*(double)tau);
			if (tau_units==1) good_factor *= (double)fec;

			n_intervals = n_cycles;
			y_tau = (double*)calloc(n_intervals, sizeof(double));
			for (i2=0, m=0; i2<n_cycles; i2++)
			{	j=ind[i2];	// point de départ de l'intégration
				a_tau=0.;
			
				for (j2=0; j2<tau; j2++)
				{	a_tau += - ( (double)excitation[j+j2] - (double)bool_substract_vanZon*excitation[j - indice_reference] )
								*((double)theta[j+j2]-theta[j+j2-1]);	// average over tau points
				}
				y_tau[m]=(double)(a_tau*good_factor);
				m++;
			}
			mean	= gsl_stats_mean(y_tau, 1, n_intervals);
			sigma	= gsl_stats_sd_m(y_tau, 1, n_intervals, mean);
			total_sigma[i] += sigma*n_cycles;
			dispersion[i]  += sigma; // we will measure the diff between this estimator of sigma, and the other one

			if (bool_mean==1)
			{	ds_mean->yd[i] += (float)mean*(float)n_cycles;
			}
			if (bool_sigma==1)
			{	if (k==0) ds_sigma->yd[i]=0.;
				ds_sigma->yd[i] += (float)sigma*(float)n_cycles;  
			}
			if (bool_skew==1)
			{	skewness = gsl_stats_skew_m_sd (y_tau, 1, n_intervals, mean, sigma);
				ds_skewness->yd[i] += (float)skewness*(float)n_cycles;
			}
			if (bool_kurtosis==1)
			{	kurtosis = gsl_stats_kurtosis_m_sd (y_tau, 1, n_intervals, mean, sigma);
				ds_kurtosis->yd[i] += (float)kurtosis*(float)n_cycles;
			}
			if (bool_GC==1)
			{	if (sigma!=0) 	GC_estimate = (double)2.*mean/(sigma*sigma*tau);
				else 			GC_estimate = 0.;
				GC_estimate	*= (double)n_cycles;
				if (tau_units==0)	ds_GC->yd[i] += (float)GC_estimate;
				else 				ds_GC->yd[i] += (float)GC_estimate*fec;
			}
			if (bool_GC4==1)
			{	if (sigma!=0) 	GC_tmp = (double)2.*mean/(sigma*sigma*tau);
				else 			GC_tmp = 0.;
				GC4_estimate    = GC_estimate 
								+ ( -4.*skewness + (kurtosis*mean)/sigma
									+ skewness*mean*mean/(sigma*sigma) - kurtosis*mean*mean*mean/(3.*sigma*sigma*sigma)
								  )/sigma;
				GC4_estimate	*= (double)n_cycles;
				if (tau_units==0)	ds_GC4->yd[i] += (float)GC4_estimate;
				else 				ds_GC4->yd[i] += (float)GC4_estimate*fec;
			}
			free(y_tau);
		
		} //end of loop over i, ie, loop over tau

		free(excitation);
		free(theta);
		free(ind);
		
	} //end of the loop over k, ie, loop over the files;
	free(files_index);
	
	for (i=0; i<n_tau; i++) 
	{	total_sigma[i] /= (double)(n_cycles_total);
		dispersion[i]  /= (double)(n_files);
	}
		
	if (bool_mean==1)
	{	for (i=0; i<n_tau; i++) 
		{	ds_mean->xd[i] = tau_index[i];
			ds_mean->yd[i] /= (float)(n_cycles_total);
		}
		if (bool_dispersion==1)
		{	if ((alloc_data_set_y_error(ds_mean)==NULL) || (ds_mean->ye==NULL))
					return win_printf_OK("I can't create errors !");
			for (i=0; i<n_tau; i++) 
			{	if (total_sigma[i]!=0) ds_mean->ye[i] = ds_mean->yd[i]*(float)((dispersion[i]-total_sigma[i])/total_sigma[i]);
			}
		}
	}
	if (bool_sigma==1)
	{	for (i=0; i<n_tau; i++)
		{	ds_sigma->xd[i] = tau_index[i];
			ds_sigma->yd[i] /= (float)(n_cycles_total);
		}
		if (bool_dispersion==1)
		{	if ((alloc_data_set_y_error(ds_sigma)==NULL) || (ds_sigma->ye==NULL))
					return win_printf_OK("I can't create errors !");
			for (i=0; i<n_tau; i++) 
			{	ds_sigma->ye[i] = (float)((dispersion[i]-total_sigma[i]));
			}
		}
	}
	
	if (bool_skew==1)
	{	for (i=0; i<n_tau; i++)
		{	ds_skewness->xd[i] = tau_index[i];
			ds_skewness->yd[i] /= (float)n_cycles_total;
		}
		if (bool_dispersion==1)
		{	if ((alloc_data_set_y_error(ds_skewness)==NULL) || (ds_skewness->ye==NULL))
					return win_printf_OK("I can't create errors !");
			for (i=0; i<n_tau; i++) 
			{	if (total_sigma[i]!=0)
					ds_skewness->ye[i] = ds_skewness->yd[i]*(float)(3.*(dispersion[i]-total_sigma[i])/total_sigma[i]);
			}
		}
	}
	if (bool_kurtosis==1)
	{	for (i=0; i<n_tau; i++)
		{	ds_kurtosis->xd[i] = tau_index[i];
			ds_kurtosis->yd[i] /= (float)n_cycles_total;
		
			if (bool_substract_3_to_kurtosis==0) 
			{	ds_kurtosis->yd[i] += (float)3.; 
			}
		}
		if (bool_dispersion==1)
		{	if ((alloc_data_set_y_error(ds_kurtosis)==NULL) || (ds_kurtosis->ye==NULL))
					return win_printf_OK("I can't create errors !");
			for (i=0; i<n_tau; i++) 
			{	if (abs(total_sigma[i])>1e-33) 
					ds_kurtosis->ye[i] = ds_kurtosis->yd[i]*(float)(4.*(dispersion[i]-total_sigma[i])/total_sigma[i]);
			}
		}
	}
	if (bool_GC==1)
	{	for (i=0; i<n_tau; i++) 
		{	ds_GC->xd[i] = tau_index[i];
			ds_GC->yd[i] /= (float)(n_cycles_total);
		}
	}
	if (bool_GC4==1)
	{	for (i=0; i<n_tau; i++) 
		{	ds_GC4->xd[i] = tau_index[i];
			ds_GC4->yd[i] /= (float)(n_cycles_total);
		}
	}
	
	free(tau_index);
	free(total_sigma);
	free(dispersion);
	
	set_plot_title(op2,"moments");
	set_plot_x_title(op2, "\\tau (nb of points, 1/f_{ec})");
	set_plot_y_title(op2, "moments (%d cycles from %d files)", n_cycles_total, n_files);
	op2->filename = Transfer_filename(filename);
    op2->dir = Mystrdup(pr->o_p[pr->cur_op-1]->dir);

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_stats_periodic_build_from_M_dep_files' */







int do_hist_full_cycles_from_M_dep_files(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memory 
register int	i2, j, j2;
	O_p 		*op2, *op1=NULL;
static int	n_bin=100;
	int		n_theta, n_excitation, n_cycles, n_cycles_total=0;
	gsl_histogram *hist;
	double	a_tau;
static float	h_min=-5, h_max=15;
	d_s 		*ds2;
	pltreg	*pr = NULL;
	int		tau, i_file, n_tau, n_files;	// current tau, current file number, number of taus, number of files
	int		*tau_index=NULL;		// all the taus
	int		*files_index=NULL;		// all the files
	int		index;
static	char	tau_string[128]		="1,2,5,10:5:50";
static	char files_ind_string[128]	="1:1:20"; 
static	char file_extension[8]		=".dat";
static	char	theta_root_filename[128]   = "dep";
static	char excitation_root_filename[128] = "M";
	char		s[512];
	char		filename[256];
	int		*ind=NULL; // indices
	float	*excitation, *theta; // *power;
	float	y_min=0, y_max=1;
static int	bool_remove_bounds=1 ;
static int  	n_last=1; // how many points should be removed at the end of the dataset
static int	bool_report=0;
static int	plateau_position=0;
static int	point_position=1;
static int	bool_add_more_indices=0, bool_shift_indices=0, bool_substract_vanZon=1, indice_reference=0;
static int	n_more=1, n_shift=0;
	int  	bool_search_indices=1;
	int		N_shift_auto=1;
static float	fec=2048.;
float		periode_typique=0.;
	int		n_pts_integration;
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine builds a set of histograms\n"
				"out of a set of files of \\theta(t) and M(t).\n\n"
				"data W_\\tau(t) is computed as :\n\n"
				"W_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} -(M(t')-\\epsilonM(t)).{\\frac{d\\theta}{dt}}(t') dt'\n\n"
				"with \\epsilon = 0 or 1\n"
				"\\tau is a perfect multiple of the excitation period.\n\n"
				"A new plot is created");
	}
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of file with angle \\theta %%s"
			"root name of file with excitation M (used also to find indices) %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n"
			"%%b print to screen a report for each file");
	i=win_scanf(s, &theta_root_filename, &excitation_root_filename, &files_ind_string, &file_extension, &bool_report);
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("sampling frequency ? %8f", &fec);
	if (i==CANCEL) return(OFF);
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices, &point_position, &plateau_position, 
							&bool_remove_bounds, &n_last, &bool_shift_indices, &n_shift, 
							&bool_add_more_indices, &n_more, &N_shift_auto);
	if (i==CANCEL) return(OFF);

	if (bool_add_more_indices==0) n_more=1;
			
	i=win_scanf("%b Do you want the van Zon power/work ?\n\n"
				"W = \\frac{1}{\\tau}\\int_t^{t+\\tau} - [ M(t') - M(t)] \\frac{{d\\theta}{dt}} (t') dt'\n\n"
				"if you want this, what should be substracted ?\n"
				"%R M(t) or %r M(t-1)\n"
				"{\\pt6 (best is M(t)}", &bool_substract_vanZon, &indice_reference);
	if (i==CANCEL) return(D_O_K);
				
	i=win_scanf("number of bins in histogram? %d min %f max %f\n"
			"using averaged data {\\pt16 a_\\tau(t) = {1 \\over \\tau} \\int_t_i^{t_i+\\tau} a(t') dt'}\n\n"
			"\\tau (integer, {\\color{yellow}multiple of excitation period}) ? %s"
			"Integration in time starts at times t_i found with previous rule.",
			&n_bin, &h_min, &h_max, &tau_string);
	if (i==CANCEL) return OFF;			
	
	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	return(win_printf("bad values for \\tau !"));
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	if ((op2 = create_and_attach_one_plot(pr, n_bin, n_bin, 0))==NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	
	if (bool_add_more_indices==1)
	{	n_more -= n_shift;
//		n_more -= tau_index[n_tau-1]; // not to escape from the ramp
	}	
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%d%s", theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);

		sprintf(filename, "%s%d%s", excitation_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_excitation=load_data_bin(filename, &excitation);
		
		if (n_excitation!=n_theta) return(win_printf_OK("M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		if ( (bool_search_indices==1) && (k==0))	// first file: we autodetect some parameters		
		{	i=find_search_domain(excitation, n_excitation, plateau_position, &y_min, &y_max);
			if (i==CANCEL) return(D_O_K);
		}
	
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);
		
		ind = find_set_of_indices(excitation, 
						( (bool_search_indices==1) ? n_excitation : n_excitation - tau_index[n_tau-1]), 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
		if (ind==NULL)  return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));
		if (n_cycles<2) return(win_printf_OK("file %s\nNot enough cycles... I need more than 1!",filename));
			
		if (k==0)		
		{	periode_typique = (float)(ind[n_cycles-n_more] - ind[0])/(float)((float)n_cycles/(float)n_more-1.);
			i=win_printf("Typical period of excitation signal,\n"
						"from first file : %6.2f", periode_typique);
			if (i==CANCEL) return(D_O_K);
		}
		
		if ((ind[n_cycles-1]+tau_index[n_tau-1]*periode_typique)>=n_excitation)
		{	i=win_printf("filen %s\nlarger tau %d is too large!\nlast index is %d\n%d points in dataset\n"
						"typical forcing period is %6.2f\n"
						"click CANCEL to abort or OK to remove one more period",
						filename, tau_index[n_tau-1], ind[n_cycles-1], n_excitation, periode_typique);
			if (i==CANCEL) return(D_O_K);
			n_cycles --;
		}

		n_cycles_total += n_cycles;
		if (n_cycles<1) 	return(win_printf_OK("Mmm... not enough ramps in file %s", filename));
			
		if (bool_report==1)	
		{	if (bool_search_indices==1)
				win_printf_OK("file number %d/%d\n(\\theta in %s)\n\n"
							"%d points in \\theta\n%d points in excitation M\n\n%d usable cycles found", 
							k+1, n_files, filename, n_theta, n_excitation, n_cycles);
			else
				win_printf_OK("file number %d/%d\n(%s)\n\n"
							"%d points in \\theta file\n%d usable windows, separated by %d points\n"
							"(cf larger \\tau is %d)", 
							k+1, n_files, filename, n_theta, n_cycles, N_shift_auto, tau_index[n_tau-1]);
		}

		sprintf(s, "%d/%d cumulating histograms", k+1, n_files); spit(s);
							
		for (i=0; i<n_tau; i++)
		{	tau=tau_index[i];

			if (k==0) // first file : we create the datasets
			{	if (i!=0) // not the first tau: we add a dataset
				{	if ((ds2 = create_and_attach_one_ds(op2, n_bin, n_bin, 0)) == NULL)	
						return win_printf_OK("cannot create dataset !");
				}
				ds2->treatement = my_sprintf(ds2->treatement,"histogram, averaged data over \\tau=%d periods", tau);
						
				(gsl_histogram *)hist = gsl_histogram_alloc(n_bin);
				gsl_histogram_set_ranges_uniform (hist, (double)h_min, (double)h_max);
			}
			else 
			{	ds2=op2->dat[i];
				ds_to_histogram(ds2, &hist); // hist will be allocated perfectly
			}

			for (i2=0; i2<=n_cycles-n_more*tau; i2++)
			{	j=ind[i2];	// point de départ de l'intégration
				a_tau=0.;
				n_pts_integration = ind[i2+tau*n_more]-j;
				for (j2=0; j2<n_pts_integration; j2++)
				{	a_tau += - ( (double)excitation[j+j2] - (double)bool_substract_vanZon*excitation[j - indice_reference] )
								*((double)theta[j+j2]-theta[j+j2-1]);	// average over tau points
				}
				gsl_histogram_increment(hist, (double)(a_tau*fec/(kT_Fred*(double)n_pts_integration))); // histogram of averaged data
			}

			histogram_to_ds(hist, ds2, n_bin);
			gsl_histogram_free(hist);
	
		} //end of loop over i, ie, loop over tau

		free(excitation);
		free(theta);
	//	free(power);
		free(ind);
		
	} //end of the loop over k, ie, loop over the files;
	free(tau_index);
	free(files_index);
	
	set_plot_title(op2,"histograms");
	if (bool_search_indices==1) 
		set_plot_x_title(op2, "\\frac{1}{\\tau} W_\\tau (%d cycles from %d files)", n_cycles_total, n_files);
	else
		set_plot_x_title(op2, "\\frac{1}{\\tau} W_\\tau (%d windows from %d files)", n_cycles_total, n_files);
	set_plot_y_title(op2, "pdf");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

    	op2->iopt |= YLOG;

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_hist_full_cycles_from_M_dep_files' */




/************************************************************************************************************/
/* this function is the one used by all functions in this plug'in											*/
/************************************************************************************************************/
int *find_set_of_indices(float *x, int nx, int *n_ind,
						int bool_search_indices, int point_position, int plateau_position, 
						int bool_remove_bounds, int n_last, 
						int bool_shift_indices, int n_shift, 
						int bool_add_more_indices, int n_more,
						int N_shift_auto,
						float y_min, float y_max)
{	int		i, k2; // slow loops, no need of registers, let's save some cache memory 
	int		*ind=NULL, *ind2=NULL; // indices
	int		n_ind1, n_ind2;
	
	if (bool_search_indices==1)
	{	
		// search for indices:
		ind   = find_interval_float (x, nx-1, y_min, y_max, &n_ind1);
		if (point_position==0)	ind2  = find_and_keep_firsts_int(ind, n_ind1, &n_ind2);
		if (point_position==1)	ind2  = find_and_keep_middles_int(ind, n_ind1, &n_ind2);
		if (point_position==2)	ind2  = find_and_keep_lasts_int(ind, n_ind1, &n_ind2);
		free(ind);
		ind=ind2;	
			
		k2=0;	// a shift on index if the first one is removed
		if (bool_remove_bounds==1)
		{	n_ind2 -= (1+n_last);  // there will be n_last points less : the first and the last are removed.
			for (i=0; i<n_ind2; i++)		ind[i]=ind[i+1];
		}
		if (n_ind2<=0) return(NULL); 
				
		if (bool_shift_indices==1)
		{	for (k2=0; k2<n_ind2; k2++)
			{	ind[k2] += n_shift;
			}
		}
			
		if (bool_add_more_indices==1)
		{	ind2 = calloc(n_ind2*n_more, sizeof(float));
			for (i=0; i<n_more; i++)
			{	for (k2=0; k2<n_ind2; k2++)
				{	ind2[k2*n_more+i] = ind[k2] + i;
				}
			}
			free(ind);
			ind=ind2;
			n_ind2 *= n_more;
		}
		
		n_ind[0] = n_ind2;
	}		
		
	else	// we impose the indices
	{	// n_ind = (int)((nx - tau_index[n_tau-1]) / N_shift);
		n_ind[0] = (int)(nx/N_shift_auto);
		ind=(int*)calloc(n_ind[0], sizeof(int));
		for (i=0; i<n_ind[0]; i++)
		{	ind[i] = (int)(i*N_shift_auto);
		}
	}

	return ind;
} /* end of function 'find_set_of_indices' */




int find_search_domain(float *x, int nx, int plateau_position, float *y_min, float *y_max)
{   int i;
static int	 relative_or_absolute=0;
static float epsilon=5e-14;
	float 	amp;
	
	i=win_scanf("There is a criterium to find indices.\n\n"
				"%R with absolute precision\n"
				"%r with relative precision\n"
				" \\epsilon = %8f", 
				&relative_or_absolute, &epsilon);
	if (i==CANCEL) return(CANCEL);
	
	y_min[0] = y_max[0] = x[0];
					
	for (i=1; i<nx; i++)
	{	if (x[i]<y_min[0]) y_min[0]=x[i];
		if (x[i]>y_max[0]) y_max[0]=x[i];
	}
	amp = y_max[0]-y_min[0];
	
	if (plateau_position==0) // minimum
	{	y_max[0] = y_min[0];
	}
	if (plateau_position==1) // maximum
	{ 	y_min[0] = y_max[0];
	}
	
	if (relative_or_absolute==0) // absolute
	{	y_min[0] -= epsilon;
	  	y_max[0] += epsilon;
    }
    if (relative_or_absolute==1) // relative
	{	y_min[0] -= (amp*epsilon);
	  	y_max[0] += (amp*epsilon);
    }
    
	return(D_O_K);	
}






int ask_for_parameters_to_search_for_indices(int *bool_search_indices, int *point_position, int *plateau_position, 
							int *bool_remove_bounds, int *n_last, 
							int *bool_shift_indices, int *n_shift, 
							int *bool_add_more_indices, int *n_more,
							int *N_shift_auto)
{	int		i;

	i=win_scanf("{\\pt14\\color{yellow} Indices and cycles...}\n"
			"%b search for indices (if not, you will be ask how to impose them)\n"
			"How to find the points t_i where to start from ?\n"
			"%R first points\n"
			"%r middle points\n"
			"%r last points\n"
			"of\n"
			"%R lower plateau\n"
			"%r upper plateau\n\n"
			"%b remove first and last %4d indices\n"
			"%b shift indices by %5d\n (to account for relaxation time)\n"
			"%b add manually %5d more indices\n (total duration of ramp)", 
			bool_search_indices,
			point_position, plateau_position, bool_remove_bounds, n_last, 
			bool_shift_indices, n_shift, bool_add_more_indices, n_more);
	if (i==CANCEL) return(OFF);
			
	if (bool_search_indices[0]==0)
	{
		i=win_scanf("{\\pt14\\color{yellow} Imposing indices...}\n"
			"Instead of searching for indices, I will impose them.\n\n"
			"Skip %d between two consecutive averaging windows\n"
			"(note: if files are big, and integration times \\tau are large, I recommend\n"
			"to use set the number above to a significant fraction of \\tau to avoid \n"
			"diverging computation times)",
			N_shift_auto);
	}
	else
	{	if (n_more[0]<1) return(win_printf_OK("Adding manually more indices\n %d should >=1", n_more[0]));
	}
		
	return(0);
} /* end of function 'ask_for_parameters_to_search_for_indices' */





int do_average_over_cycles(void)
{
	double  dt, moy;
	float t_init=0.;
	int	 ny, n_cycles;
	int   period, period_min, period_max;
	int	 index;
	int	 do_shift=0;
	int	 *ind; // indices
	register int 	i, i2;
	O_p 	*op2, *op1 = NULL;
	d_s 	*ds2, *ds1, *ds_ind;
	pltreg	*pr = NULL;
	char	s[512];
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine averages a dataset\n"
				"and places the result in a new plot\n\n"
				"data a(t) is averaged avec cycles starting at times t_i\n"
				"with t_i from a dataset of indices\n"
				"(indices are assumed to be in the next ds, after the current one)");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	ny=ds1->nx;
	if (op1->n_dat<=1)	return(win_printf_OK("I need 2 datasets!\n"
				"active one for the data,\nnext one for indices"));
	ds_ind = (op1->cur_dat == op1->n_dat-1 ) ? op1->dat[0] : op1->dat[op1->cur_dat + 1];
	n_cycles = ds_ind->nx;
	if (n_cycles>=ny) 	return(win_printf_OK("You probably exchanged the 2 datasets ;-)"));

	dt     = (double)(ds1->xd[ny-1]-ds1->xd[0])/(ny-1);
//	win_printf("dt is \n%g\n%g\n%g", dt, ds1->xd[2]-ds1->xd[1], ds1->xd[1]);
	if ( (ds1->xd[2]-ds1->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)				return(win_printf_OK("dt is zero !"));
	ind = (int*)calloc(n_cycles, sizeof(int));
	for (i=0; i<n_cycles; i++)	ind[i] = (int)rintf((ds_ind->xd[i]-ds1->xd[0])/dt);
	
	period_min=ny;
	period_max=0;
	for (i=1; i<n_cycles; i++)
	{	period = (ind[i]-ind[i-1]);
		if (period<period_min) period_min=period;
		if (period>period_max) period_max=period;
	}
	
	sprintf(s,"From given indices (dataset %d), the period $T$ of the signal is\n"
			"%d <= $T$ <= %d (in points)\n"
			"%6.0g <= $T$ <= %6.0g (in seconds)\n"
			"\n"
			"There are %d periods\n\n"
			"%%R start x-axis at t=0"
			"%%r start x-axis at first index",
			(op1->cur_dat == op1->n_dat-1 ) ? 0 : op1->cur_dat + 1,
			period_min, period_max, dt*period_min, dt*period_max, n_cycles);
	i=win_scanf(s, &do_shift);
	if (i==CANCEL) return OFF;

	if ((ind[n_cycles-1]+period_min)>=ny)	return(win_printf_OK("please remove the last point!"));
		
	
	if ((op2 = create_and_attach_one_plot(pr, period_min, period_min, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	 
	if (do_shift==1)	t_init=ds1->xd[ind[0]];
	else				t_init=0;
	
	for (i=0; i<period_min; i++)
	{	
		moy=0;
		for (i2=0; i2<n_cycles; i2++)		moy += ds1->yd[ind[i2]+i];
			
		ds2->xd[i] = t_init + (float)(dt*i);
		ds2->yd[i] = (float)(moy/n_cycles);
	}

	ds2->treatement = my_sprintf(ds2->treatement,"averaged over %d cycles", n_cycles);
	inherit_from_ds_to_ds(ds2, ds1);
	set_plot_title(op2,"averaged signal");
	set_plot_x_title(op2, "time (s)",n_cycles);
	set_plot_y_title(op2, "signal");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_average_over_cycles' */










int do_decimate_NI4472(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memory 
	O_p 		*op1=NULL;
	pltreg	*pr = NULL;
	int		index, ret, i_file, n_files;	// current tau, current file number, number of taus, number of files
	int		*files_index=NULL;		// all the files
static	char files_ind_string[128]	="0:1:2"; 
static	char file_extension[8]		=".bin";
static	char	root_filename[128]	     = "run02";
	long		N_total;
static int 	N_decimate=10;
	long		header_length;
static int	N_channels=3;
	char		s[512];
	float	f_acq;
	char		filename_in[256], filename_out[256];
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine loads a set of files\n"
				"and saves them after decimation (average)\n\n"
				"(in order to save some disk space)");
	}
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of files %%s"
			"range of files %%s"
			"file extension (note: expected files are from NI4472) %%s\n"
			"Number of channels in a file : %%2d\n"
			"decimate data by a factor of %%3d\n");
	i=win_scanf(s, &root_filename, &files_ind_string, &file_extension, 
				&N_channels, &N_decimate);
	if (i==CANCEL) return(OFF);
	if (N_decimate<=1) return(OFF);
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];

		sprintf(filename_in, "%s%d%s", root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: converting", k+1, n_files, filename_in); spit(s);
		sprintf(filename_out, "%s%d%s%s", root_filename, i_file, "_dec", file_extension);
		
		ret=read_header_NI4472(filename_in, &header_length, &N_total, &f_acq);
		if (ret==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename_in));
		// decimate_NI4472_file(filename_in, N_total, N_channels, N_decimate, filename_out);
		// replaced 2007/06/15
		decimate_NI_file(filename_in, IS_NI_4472_FILE, N_channels, N_decimate, filename_out);
		
	} //end of the loop over k, ie, loop over the files;
	free(files_index);

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_decimate_NI4472' */





int *impose_indices(float dt, float cycle_period, float start_time, int N, int *n_ind)
{	int	*ind, first_ind, i;

	if ((start_time>dt*(N-1)) || (start_time<0) )
	{	win_printf_OK("imposing indices: starting time is too large...");
		return(NULL);
	}
	if (cycle_period<=dt) 
	{	win_printf_OK("imposing indices: cycle period is too small!");
		return(NULL);
	}
	first_ind = (int)(start_time/dt);
	*n_ind=(int)(((float)dt*(N-1)-start_time)/cycle_period); 
	ind=(int*)calloc(*n_ind, sizeof(int));
	for (i=0; i<*n_ind; i++) 
	{	ind[i]=first_ind + (int)(cycle_period*i/dt);
	}
	return(ind);
}



void spit(char *message)
{	size_t	l_max=55;
	char 	white_string[64]="                                              ";
	
	if (strlen(message)<l_max) 	strncat (message, white_string, l_max-strlen(message));
	textout_ex(screen, font, message, 40, 40, makecol(100,155,155), makecol(2,1,1));
	return;
}


MENU *ramps_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Find begining of lower plateau", 	do_find_boundaries,	NULL, RAMPS_LOWER|RAMPS_BEGIN, NULL);	
	add_item_to_menu(mn,"Find ending of lower plateau",		do_find_boundaries,	NULL, RAMPS_LOWER|RAMPS_END, NULL);	
	add_item_to_menu(mn,"Find begining of higher plateau",	do_find_boundaries,	NULL, RAMPS_UPPER|RAMPS_BEGIN, NULL);	
	add_item_to_menu(mn,"Find ending of higher plateau",	do_find_boundaries,	NULL, RAMPS_UPPER|RAMPS_END, NULL);
	add_item_to_menu(mn,"Find something else",				do_find_boundaries, NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"Average over cycles",				do_average_over_cycles, 				NULL, 0, NULL);
	add_item_to_menu(mn,"Histograms for averaged data",		do_hist_periodic_build_from_power_ds,	NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"histograms from binary power files",		do_hist_periodic_build_from_power_files,NULL, 0, NULL);
	add_item_to_menu(mn,"Batch on NI4472 files (U)",		do_hist_periodic_build_from_NI4472,	NULL, 0, NULL);
	add_item_to_menu(mn,"histograms from binary M&dep files",		do_hist_periodic_build_from_M_dep_files,NULL, 0, NULL);
	add_item_to_menu(mn,"histograms from binary M&dep cycles",		do_hist_full_cycles_from_M_dep_files,NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"moments from binary M&dep files",	do_stats_periodic_build_from_M_dep_files,NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"decimate a series of files",		do_decimate_NI4472,					NULL, 0, NULL);	
	
	return(mn);
}

int	ramps_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("ramps", NULL, ramps_plot_menu(), 0, NULL);
	return D_O_K;
}


int	ramps_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"ramps",	NULL, NULL);
	return D_O_K;
}
#endif

