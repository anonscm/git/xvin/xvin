/*	
**	OHY_S.C
**	 
**	Arnaud Chiffaudel 25.02.1999
**	Nicolas Garnier	  11.02.2004
**	
**	Fonctions Generales orientees vers les specials,... :
**		Manipulation des parametres des espace-temps
**			- Struture ohy
**			- Parsage et ecriture des specials
**			- Xvin user functions
*/

# include "xvin.h"
#define BUILDING_PLUGINS_DLL
# include "ohy.h"

/* Future Public Declarations */
char 	*my_sprintf(char *dest, char *format, ...);


/* Button Functions */
int	do_use_2_specials(void);
int	do_specials_2_use(void);
int	do_remove_ohy_specials(void);
int	do_remove_all_specials(void);
int	do_free_ohy_use(void);


/* Button Functions for Specials and Use (Dangerous) manipulation */

int do_specials_2_use(void)
{
	imreg	*imr;
	pltreg	*pr;
	O_p	*op;
	d_s	*ds;

	if (updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%im",&imr) == 1)  
	{
		if(imr->one_i == NULL)	return 1;
		ohy_image_specials_2_use(imr->one_i);
	}
	else if (ac_grep(cur_ac_reg,"%pr",&pr) == 1)
	{
		if ((op = pr->one_p) == NULL || op->n_dat == 0)	return 1;
		if ((ds = op->dat[op->cur_dat]) == NULL)	return 1;
		ohy_ds_specials_2_use(ds);
	}
	else return 1;
	return 0;
}

int do_use_2_specials(void)
{
	imreg	*imr;
	pltreg	*pr;
	O_p	*op;
	d_s	*ds;
	
	if (updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%im",&imr) == 1)
	{
		if(imr->one_i == NULL)	return 1;
		ohy_image_use_2_specials(imr->one_i);
	}
	else if (ac_grep(cur_ac_reg,"%pr",&pr) == 1)
	{
		if ((op = pr->one_p) == NULL || op->n_dat == 0)	return 1;
		if ((ds = op->dat[op->cur_dat]) == NULL)	return 1;
		ohy_ds_use_2_specials(ds);
	}
	else return 1;
	return 0;
}

int free_ohy_use(void)
{
	imreg	*imr;
	pltreg	*pr;
	O_p	*op;
	d_s	*ds;
	
	if (updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%im",&imr) == 1)
	{
		if(imr->one_i == NULL)				return 1;
		return ohy_image_free_use(imr->one_i);
	}
	else if (ac_grep(cur_ac_reg,"%pr",&pr) == 1)
	{
		if ((op = pr->one_p) == NULL || op->n_dat == 0)	return 1;
		if ((ds = op->dat[op->cur_dat]) == NULL)	return 1;
		return ohy_ds_free_use(ds);
	}
	else return 1;
	return 0;
}

int remove_ohy_specials(void)
{
	imreg	*imr;
	pltreg	*pr;
	O_p	*op;
	d_s	*ds;
	
	if (updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%im",&imr) == 1)
	{
		if(imr->one_i == NULL)				return 1;
		ohy_image_remove_specials(imr->one_i);
	}
	else if (ac_grep(cur_ac_reg,"%pr",&pr) == 1)
	{
		if ((op = pr->one_p) == NULL || op->n_dat == 0)	return 1;
		if ((ds = op->dat[op->cur_dat]) == NULL)	return 1;
		ohy_ds_remove_specials(ds);
	}
	else return 1;
	return 0;
}

int remove_all_specials(void)
{
	imreg	*imr;
	pltreg	*pr;
	O_i	*oi;
	O_p	*op;
	d_s	*ds;
	
	if (updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%im",&imr) == 1)
	{
		if((oi = imr->one_i) == NULL)			return 1;
		if (oi->im.special != NULL)
			remove_from_one_image(oi, ALL_SPECIAL,oi->im.special);
	}
	else if (ac_grep(cur_ac_reg,"%pr",&pr) == 1)
	{
		if ((op = pr->one_p) == NULL || op->n_dat == 0)	return 1;
		if ((ds = op->dat[op->cur_dat]) == NULL)	return 1;
		if (ds->special != NULL)
			remove_from_data_set(ds, ALL_SPECIAL, ds->special);
	}
	else return 1;
	return 0;
}

int do_count_specials(void)
{
	imreg	*imr;
	pltreg	*pr;
	O_i	*oi;
	O_p	*op;
	d_s	*ds;
	
	if (updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%im",&imr) == 1)
	{
		if ((oi = imr->one_i) == NULL)		return 1;
		if (oi->im.special == NULL)
			win_ti_printf("Specials Info", "oi->im.special = NULL\noi->im.n special = %d\noi->im.m special = %d", 
			oi->im.n_special, oi->im.m_special);
		else
			win_ti_printf("Specials Info", "oi->im.special = not NULL\noi->im.n special = %d\noi->im.m special = %d",
			oi->im.n_special, oi->im.m_special);
	}
	else if (ac_grep(cur_ac_reg,"%pr",&pr) == 1)
	{
		if ((op = pr->one_p) == NULL)			return 1;
		if (op->n_dat == 0)				return 1;
		if ((ds = op->dat[op->cur_dat]) == NULL)	return 1;
		if (ds->special == NULL)
			win_ti_printf("Specials Info", "ds->special = NULL\nds->n special = %d\nds->m special = %d", ds->n_special, ds->m_special);
		else
			win_ti_printf("Specials Info", "ds->special = pas NULL\nds->n special = %d\nds->m special = %d", ds->n_special, ds->m_special);
	}
	else return 1;
	return 0;
}

int do_show_ohy_specials(void)
{
	int	i;
	imreg	*imr;
	pltreg	*pr;
	O_i	*oi;
	O_p	*op;
	d_s	*ds;
	int	n_src = 0;
	char	*data, *ss, **src, title[128];
	
	if (updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) == 2)
	{
		if (oi->im.pixel == NULL)			return win_printf_OK("image is NULL!");
		sprintf(title, "Info for image %d", imr->cur_oi);
//		ohy_image_remove_specials(oi);		// commented 2004/02/16 NG
//		ohy_image_use_2_specials(oi);		// commented 2004/02/16 NG
		src = oi->im.special;
		n_src = oi->im.n_special;
	}
	else if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) == 1)
	{
		if (op->n_dat == 0)				return win_printf_OK("no dataset in plot!");
		if ((ds = op->dat[op->cur_dat]) == NULL)	return win_printf_OK("dataset is NULL!");
		sprintf(title, "Info for data set %d", op->cur_dat);
//		ohy_ds_remove_specials(ds);		// commented 2004/02/16 NG
//		ohy_ds_use_2_specials(ds);		// commented 2004/02/16 NG
		src = ds->special;
		n_src = ds->n_special;
	}
	else return win_printf_OK("no image and no plot found !");

	if (src == NULL || n_src == 0)	return win_printf_OK("no specials ! (%d found)\n%s", n_src, src[0]);;

	data = NULL;
	for (i = 0; i < n_src; i++)
	{
		ss = src[i];
		if (strncmp(ss, "OHY", 3) == 0)
			data = my_sprintf(data, "\\pt8{%s}\n", ss + 3);
/*		{
			data = stradd(data, "\\pt8");
			data = stradd(data, ss + 3);
			data = stradd(data, "\n");
		}
*/	}
	win_ti_printf(title, "%s", data);
	free(data);
	
	return 0;
}


/* Xvin Users functions */

int ohy_ds_remove_specials(d_s *ds)
{
	int	i;
	
	if(ds->special != NULL)
		for (i = (ds->n_special - 1); i >= 0; i--)
			if (strncmp(ds->special[i], "OHY", 3) == 0)
				remove_from_data_set(ds, IS_SPECIAL, (void*)ds->special[i]);
	return 0;
}

int ohy_ds_specials_2_use(d_s *ds)
{
	struct hook hk;
	
	if (ds->special != NULL)
	{		
		hk.to = IS_DATA_SET;
		hk.stuff = (void*)ds;
		return ohy_specials_2_use(hk);
	}
	return 0;
}

int ohy_ds_use_2_specials(d_s *ds)
{
	struct hook hk;
	
	if (ds->use.stuff != NULL && ds->use.to == IS_OHY)
	{		
		hk.to = IS_DATA_SET;
		hk.stuff = (void*)ds;
		return ohy_use_2_specials(hk);
	}
	return 0;
}

int ohy_ds_free_use(d_s *ds)
{
	ohy_s *as;
	
	if (ds->use.stuff != NULL && ds->use.to == IS_OHY)
	{
		as = (ohy_s*)ds->use.stuff;
		ds->use.to = 0;
		ds->use.stuff = NULL;
		free(as);
	}
	return 0;
}

int ohy_ds_duplicate_use(d_s *dest, d_s *src)
{
	ohy_s	*as;
	
	if (src->use.stuff != NULL && src->use.to == IS_OHY)
	{
		if ((as = (ohy_s*) calloc(1, sizeof(ohy_s))) == NULL)
		{
			win_printf_OK("Calloc error. Use not duplicated");
			return 1;
		}
		*as = *((ohy_s*)src->use.stuff);
		dest->use.stuff = (void*)as;
		dest->use.to = IS_OHY;
	}
	return 0;
}

int ohy_image_use_2_ds_use(d_s *ds, O_i *oi)
{
	ohy_s	*as;
	
	if (oi->im.use.stuff != NULL && oi->im.use.to == IS_OHY)
	{
		if ((as = (ohy_s*) calloc(1, sizeof(ohy_s))) == NULL)
		{
			win_printf_OK("Calloc error. Use not duplicated");
			return 1;
		}
		*as = *((ohy_s*)oi->im.use.stuff);
		ds->use.stuff = (void*)as;
		ds->use.to = IS_OHY;
	}
	return 0;
}

int ohy_image_remove_specials(O_i *oi)
{
	int	i;
	
	if(oi->im.special != NULL)
		for (i = (oi->im.n_special - 1); i >= 0; i--)
			if (strncmp(oi->im.special[i], "OHY", 3) == 0)
				remove_from_one_image(oi, IS_SPECIAL,
						(void*)oi->im.special[i]);
	return 0;
}

int ohy_image_specials_2_use(O_i *oi)
{
	struct hook hk;
	
	if (oi->im.special != NULL)
	{		
		hk.to = IS_ONE_IMAGE;
		hk.stuff = (void*)oi;
		return ohy_specials_2_use(hk);
	}
	else return 0;
}


int ohy_image_use_2_specials(O_i *oi)
{
	struct hook hk;
	
	if (oi->im.use.stuff != NULL && oi->im.use.to == IS_OHY)
	{		
		hk.to = IS_ONE_IMAGE;
		hk.stuff = (void*)oi;
		return ohy_use_2_specials(hk);
	}
	else return 0;
}

int ohy_image_free_use(O_i *oi)
{
	ohy_s *as;
	
	if (oi->im.use.stuff != NULL && oi->im.use.to == IS_OHY)
	{
		as = (ohy_s*)oi->im.use.stuff;
		oi->im.use.to = 0;
		oi->im.use.stuff = NULL;
		free(as);
	}
	return 0;
}

int ohy_image_duplicate_use(O_i *dest, O_i *src)
{
	ohy_s	*as;
	
	if (src->im.use.stuff != NULL && src->im.use.to == IS_OHY)
	{
		if ((as = (ohy_s*) calloc(1, sizeof(ohy_s))) == NULL)
		{
			win_printf_OK("Calloc error. Use not duplicated");
			return 1;
		}
		*as = *((ohy_s*)src->im.use.stuff);
		dest->im.use.stuff = (void*)as;
		dest->im.use.to = IS_OHY;
	}
	return 0;
}

int ohy_specials_2_use(struct hook hk)		/* Low level parser */
{
	int	i, j = 0, k = 0;
	O_i	*oi = NULL;
	d_s	*ds = NULL;
	ohy_s	*as;

	int	ret, err = 0;
	int	n_src = 0;
	char	*ss, **src, s[128], stype[128];

	
	if (hk.to == IS_ONE_IMAGE)
	{
		oi = (O_i*)hk.stuff;
		if (oi->im.use.stuff == NULL || oi->im.use.to != IS_OHY)
		{
			as = (ohy_s*) calloc(1, sizeof(ohy_s));
			oi->im.use.to = IS_OHY;
			oi->im.use.stuff = (void*)as;
		}
		else	as = (ohy_s*)oi->im.use.stuff;
		
		if ((n_src = oi->im.n_special))	src = oi->im.special;
		else	{ err = 16; goto error; }
	}
	else if (hk.to == IS_DATA_SET)
	{
		ds = (d_s*)hk.stuff;
		if (ds->use.stuff == NULL || ds->use.to != IS_OHY)
		{
			as = (ohy_s*) calloc(1, sizeof(ohy_s));
			ds->use.to = IS_OHY;
			ds->use.stuff = (void*)as;
		}
		else	as = (ohy_s*)ds->use.stuff;
		
		if ((n_src = ds->n_special))	src = ds->special;
		else	{ err = 16; goto error; }
	}
	else return 1;
	
	if (as == NULL)	{ err = 8; goto error; }

	for (i = 0; i < n_src; i++)
	{
		ss = src[i];
		if (strncasecmp(ss, "OHY", 3) != 0)		j += 1;
		else
		{
			ss += 3;
			ret = sscanf(ss, "%s", s);
			if (ret != 1)				break;
			if (strncasecmp(s, "x_offset", 8) == 0)
			{
				ret = sscanf(ss, "%*s %d", &(as->x_offset));
				if (ret != 1)	err--;
			}
			else if (strncasecmp(s, "x_rotate", 8) == 0)
			{		
				ret = sscanf(ss, "%*s %f", &(as->x_rotate));
				if (ret != 1)	err--;
			}
			else if (strncasecmp(s, "freqacq", 7) == 0)
			{		
				ret = sscanf(ss, "%*s %d", &(as->freqacq));
				if (ret != 1)	err--;
			}
			else if (strncasecmp(s, "lnumber", 7) == 0)
			{		
				ret = sscanf(ss, "%*s %d", &(as->lnumber));
				if (ret != 1)	err--;
			}
			else if (strncasecmp(s, "cnumber", 7) == 0)
			{		
				ret = sscanf(ss, "%*s %d", &(as->cnumber));
				if (ret != 1)	err--;
			}
			else if (strncasecmp(s, "Xcenter", 7) == 0)
			{		
				ret = sscanf(ss, "%*s %d", &(as->Xcenter));
				if (ret != 1)	err--;
			}
			else if (strncasecmp(s, "Ycenter", 7) == 0)
			{		
				ret = sscanf(ss, "%*s %d", &(as->Ycenter));
				if (ret != 1)	err--;
			}
			else if (strncasecmp(s, "radius", 6) == 0)
			{		
				ret = sscanf(ss, "%*s %d", &(as->radius));
				if (ret != 1)	err--;
			}
			else if (strncasecmp(s, "Rangle", 6) == 0)
			{		
				ret = sscanf(ss, "%*s %f", &(as->Rangle));
				if (ret != 1)	err--;
			}
			else if (strncasecmp(s, "type", 4) == 0)
			{		
				ret = sscanf(ss, "%*s %s", stype);
				if (ret != 1)	err--;
				if (strncasecmp(stype, "IS_PHOTO", 8)       ==0) { as->type=IS_PHOTO; }
				else if (strncasecmp(stype, "IS_CIRCLE", 9) ==0) { as->type=IS_CIRCLE; }
				else if (strncasecmp(stype, "IS_COLUMN", 9) ==0) { as->type=IS_COLUMN; }
				else if (strncasecmp(stype, "IS_LINE", 7)   ==0) { as->type=IS_LINE; }
				else if (strncasecmp(stype, "IS_RADIUS", 9) ==0) { as->type=IS_RADIUS; }
				else			       { as->type=0; }
			}
			else	k++;
		}
	}
	
	if (j == n_src)
	{
		win_printf_OK("These data contains only non-OHY specials\n"
				"No ohy-setup structure will be attached");
		if	(hk.to == IS_ONE_IMAGE)	ohy_image_free_use(oi);
		else if (hk.to == IS_DATA_SET)	ohy_ds_free_use(ds);
	}

	if (err < 0 || k != 0)
		win_printf_OK("Parse error :\n"
				"%d specials detected\n"
				"%d OHY specials parsed with\n"
				"\t\t\t%d unknown\n"
				"\t\t\t%d errors.", n_src, n_src-j, k, -err);
	
error:	switch (err)
	{
		case 8:
			win_printf_OK("Calloc error !");
			return 1;
		case 16:
			win_printf_OK("This file as no specials attached to");
			return 1;
	};
	
	return 0;
}



int ohy_use_2_specials(struct hook hk)		/* Low level writer */
{
	register int i = 0;
	d_s	*ds;
	O_i	*oi;
	ohy_s	*as;
	char	s[128],stype[128];
	
	if (hk.to == IS_ONE_IMAGE)
	{
		oi = (O_i*)hk.stuff;
		as = (ohy_s*)(oi->im.use.stuff);
		ohy_image_remove_specials(oi);
	}
	else if (hk.to == IS_DATA_SET)
	{
		ds = (d_s*)hk.stuff;
		as = (ohy_s*)(ds->use.stuff);
		ohy_ds_remove_specials(ds);
	}
	else return 1;
	
	if (as == NULL)	return 1;

	if (as->type == IS_PHOTO) { sprintf(stype,"IS_PHOTO"); }
	else if (as->type == IS_CIRCLE) { sprintf(stype,"IS_CIRCLE"); }
	else if (as->type == IS_COLUMN) { sprintf(stype,"IS_COLUMN"); }
	else if (as->type == IS_LINE)   { sprintf(stype,"IS_LINE"); }
	else if (as->type == IS_RADIUS) { sprintf(stype,"IS_RADIUS"); }
	else			        { sprintf(stype," "); }

	sprintf(s,"type %s",stype);	   	   i+=add_ohy_special(s,hk);
	sprintf(s,"freqacq %d",as->freqacq);	   i+=add_ohy_special(s,hk);	
	sprintf(s,"lnumber %d",as->lnumber);	   i+=add_ohy_special(s,hk);
	sprintf(s,"cnumber %d",as->cnumber);	   i+=add_ohy_special(s,hk);
	sprintf(s,"x_offset %d",as->x_offset);	   i+=add_ohy_special(s,hk);
	sprintf(s,"x_rotate %f",as->x_rotate);	   i+=add_ohy_special(s,hk);
	sprintf(s,"Xcenter %d",as->Xcenter);	   i+=add_ohy_special(s,hk);
	sprintf(s,"Ycenter %d",as->Ycenter);	   i+=add_ohy_special(s,hk);
	sprintf(s,"radius %d",as->radius);	   i+=add_ohy_special(s,hk);
	sprintf(s,"Rangle %f",as->Rangle);	   i+=add_ohy_special(s,hk);

	
	if (i)
	{
		win_printf_OK("Error Writing %d OHY Specials", i);
		return 1;
	}
	return 0;
}

int add_ohy_special(const char *s, struct hook hk)
{
	char *spe;

	if ((spe = stradd("OHY  ", s)) == NULL)		return 1;
	if (hk.to == IS_ONE_IMAGE)
		add_to_one_image((O_i*)hk.stuff, IS_SPECIAL, (void*)spe);
	else if (hk.to == IS_DATA_SET)
		add_to_data_set((d_s*)hk.stuff, IS_SPECIAL, (void*)spe);
	else return 1;
	
	free(spe);
	return 0;
}













