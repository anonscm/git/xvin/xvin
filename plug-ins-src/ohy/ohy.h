/*	OHY.H
**
**	
**
**	AC
**	25.02.99
*/


# define IS_OHY	1000000
# define IS_PHOTO	2
# define IS_CIRCLE	8
# define IS_RADIUS	16
# define IS_COLUMN	32
# define IS_LINE		64

/* OHY Declarations */
PXV_FUNC(int,	ohy_main, (int argc, char **argv) );
PXV_FUNC(MENU*, ohy_menu, (void) );

/* menu functions, executed when clicked on : (implemented in ohy_s.c) */
PXV_FUNC(int,	do_count_specials,	(void) );
PXV_FUNC(int,	do_show_ohy_specials,	(void) );


/* C-level functions */						/* OHY_U.C */

char	*strupd(char *s1, const char *s2);
char	*stradd(const char *s1, const char *s2);

/*int	strcasecmp(const char*, const char*);		*/	/* includes */
/*int	strncasecmp(const char*, const char*, int n);*/


/* OHY use and specials management stuff for images and data_sets (ds) */

int	ohy_ds_remove_specials(d_s *ds);			/* OHY_S.C */
PXV_FUNC(int,	ohy_ds_specials_2_use,	(d_s *ds) );
PXV_FUNC(int,	ohy_ds_use_2_specials,	(d_s *ds) );
PXV_FUNC(int,	ohy_ds_free_use,	(d_s *ds) );
PXV_FUNC(int,	ohy_ds_duplicate_use,	(d_s *dest, d_s *src) );

PXV_FUNC(int,	ohy_image_use_2_ds_use, (d_s *ds, O_i *oi) );

int	ohy_image_remove_specials(O_i *oi);
PXV_FUNC(int,	ohy_image_specials_2_use, (O_i *oi) );
PXV_FUNC(int,	ohy_image_use_2_specials, (O_i *oi) );
PXV_FUNC(int,	ohy_image_free_use,	 (O_i *oi) );
PXV_FUNC(int,	ohy_image_duplicate_use, (O_i *dest, O_i *src) );

int	ohy_specials_2_use(struct hook hk);		/* Low level parser */
int	ohy_use_2_specials(struct hook hk);		/* Low level writer */
int	add_ohy_special(const char *s, struct hook hk);
				/* Very low level writer (one special) */

/* Structure de donnees accrochees aux espace-temps */

typedef struct ohy
{
	int 	type;		/* type d(image : photo ou e-t 				*/
	int 	freqacq;	/* frequence d'acquisition 				*/
	int 	lnumber;	/* numero de la ligne (et) 				*/
	int 	cnumber;	/* numero de la colonne (et) 				*/
	int 	x_offset;	/* Decalage de l'image par rapport a son origine	*/
	float	x_rotate;	/* Coordonees du repere tournant dans lequel l'image est affichee */
	int	Xcenter;	/* centre du cercle et/ou de l'image		 	*/
	int 	Ycenter;	/* centre du cercle et/ou de l'image 			*/
	int 	radius;		/* rayon d'un cercel (et) 				*/
	float 	Rangle;		/* angle d'un rayon (et) 				*/
}ohy_s;
