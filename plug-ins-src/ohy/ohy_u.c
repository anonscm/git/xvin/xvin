/*	
**	OHY_U.C
**	 
**	VC/AC
**	12.10.92 - 17.11.92 - 16.12.92 - 16.11.93 - 12.04.94 - 26.02.99
**
**		C-Level Functions
**		Xvin-Level Functions
**
*/

# include "xvin.h"
# include "ohy.h"




/* C-level functions */

char *strupd(char *s1, const char *s2)
{
	if	(s1 == NULL)		return (s1 = strdup(s2));
	else if (strcmp(s1, s2))
	{
		free(s1);		return (s1 = strdup(s2));
	}
	else				return (s1);
}

char *stradd(const char *s1, const char *s2)
{
	char *cnt, *dest;
	
	if ((s1 == NULL) && (s2 == NULL))	return NULL;
	if (s1 == NULL)				return strdup(s2);
	if (s2 == NULL)				return strdup(s1);

	dest = (char*) malloc( (strlen(s1) + strlen(s2) + 1) * sizeof(char) );
	if (dest == NULL)			return NULL;
	
	cnt = dest;
	while ((*cnt++ = *s1++));
	cnt--;
	while ((*cnt++ = *s2++));
	
	return dest;
}

