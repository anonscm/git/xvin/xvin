/*	
**	OHY.C
**	 
**	AC/NG
**	26.02.99
**			
**	Construction des menus des specials et use pour espace-temps
*/

# include "xvin.h"
#define BUILDING_PLUGINS_DLL
# include "ohy.h"


/************************************************************************/
/* Implementation							*/
/************************************************************************/
int ohy_main(int argc, char **argv)
{

	/* User functions loading  : */
	
	ds_special_2_use	= ohy_ds_specials_2_use;
	ds_use_2_special	= ohy_ds_use_2_specials;
	ds_free_use		= ohy_ds_free_use;
	ds_duplicate_use	= ohy_ds_duplicate_use;
	image_special_2_use	= ohy_image_specials_2_use;
	image_use_2_special	= ohy_image_use_2_specials;
	image_free_use		= ohy_image_free_use;
	image_duplicate_use	= ohy_image_duplicate_use;	

	add_image_treat_menu_item ("OHY stuff", NULL, ohy_menu(), 0, NULL);
	return D_O_K;

}/* end of function "ohy_main" */





MENU *ohy_menu(void)
{	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Count Specials", 		do_count_specials,	NULL, 0, NULL);
	add_item_to_menu(mn,"Show OHY Specials", 	do_show_ohy_specials,	NULL, 0, NULL);	
	return mn;
}



int ohy_unload(int argc, char **argv)
{ 
	remove_item_to_menu(image_treat_menu,"OHY stuff", NULL, NULL);
	return D_O_K;
}
