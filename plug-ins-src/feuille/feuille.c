/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _FEUILLE_C_
#define _FEUILLE_C_

#include <allegro.h>
# include "xvin.h"

/* If you include other regular header do it here*/ 
# include "../xvuEye/xvuEye.h"
# include "../Due_serial/Due_serial.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "feuille.h"

PXV_FUNC(int, read_Due_param,(char *cmd, int *val));
PXV_FUNC(char *, Get_Due_cycle, (void));


int do_feuille_hello(void)
{
  int i;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_printf("Hello from feuille");
  if (i == WIN_CANCEL) win_printf_OK("you have press WIN_CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}

int do_plt_Extract_periodic_signal(void)
{
  int i, j;
  O_p *op = NULL;
  int nf, was_imr = 0;
  d_s *ds, *dsi, *dssi, *dsgpio = NULL;
  pltreg *pr = NULL;
  imreg *imr = NULL;
  O_i *oi;
  int lper = 0, ln_per = 0, lstart = 0, ln_per_0 = 0;
  static int per = 20, n_per = 10, start = 10, skip = 0, mod_start = 0;
  char *st = NULL;
  double re, im, phi;
  static float dphi = 0;
  char message[2048] = {0};


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }

  /*

"Led Cycle: %5d camera frames forone period of modulation\n"
	     "DAC constant signal LED0 %d ; LED1 %d\n"
	     "number of modulation periods %d\n"
	     "DAC modulation amplitude LED0 %d ; LED1 %d\n"
	     "number of periods %d of signal\n"
	     "DAC stand by signal LED0 %d ; LED1 %d\n"
  */

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
   {
      if (ac_grep(cur_ac_reg,"%imr%oi",&imr,&oi) != 2)
         return win_printf_OK("cannot image data");
      op = find_oi_cur_op(oi);
      dsi = find_source_specific_ds_in_op(op, "Data taken at xc =");
      if (dsi == NULL) return win_printf_OK("cannot find data set ");
      was_imr = 1;
   }

  nf = dsi->nx;	/* this is the number of points in the data set */
 
  if (dsi->source != NULL)
    {
      if (strncmp(dsi->source,"Data taken at xc =",18) == 0
	  || strncmp(dsi->source,"Data take at xc =",17) == 0)
	{
	  st = strstr(dsi->source, "number of modulation periods");
	  if (st != NULL)
	    {
	      if (sscanf(st, "number of modulation periods %d", &ln_per_0) != 1)
		{
		  win_printf("Cannot find number of modulation periods");
		}
	      else ln_per_0 = ln_per_0;
	    } 
	  else return win_printf_OK("Cannot find :number of modulation periods");
	  
	  st = strstr(dsi->source, "Led Cycle:");
	  if (st != NULL)
	    {
	      if (sscanf(st, "Led Cycle: %d", &lper) != 1)
		{
		  win_printf("Cannot find number of camera frames per  periods");
		}
	      else per = lper;
	    }
	  else return win_printf_OK("Cannot find :Led Cycle:");
	  st = strstr(dsi->source, "number of periods ");
	  if (st != NULL)
	    {
	      if (sscanf(st, "number of periods  %d", &ln_per) != 1)
		{
		  win_printf("Cannot find number of periods ");
		}
	      else n_per = ln_per;
	    }
	  else return win_printf_OK("Cannot find :number of periods ");
	}
      //else return win_printf_OK("incompatible data source ;\nData taken at xc =\nor\n"
      //			"Data take at xc =\n%s",dsi->source);
    }
  start = ln_per_0 * per;
  dsgpio = find_source_specific_ds_in_op(op, "GPIO 1 synchro for Data taken at xc =");
  if (dsgpio != NULL)
    {  // we recover modulation parameter from GPIO stuff 
      for (i = 0; i < dsgpio->nx; i++)
	if (dsgpio->yd[i] == 1) break;
      if (i < dsgpio->nx) 
	{
          for (; i < dsgpio->nx && dsgpio->yd[i] >0; i++);
          for (; i < dsgpio->nx && dsgpio->yd[i] <1; i++);
	  lstart = i;
	  for (i = lstart; i < dsgpio->nx; i++)
	    if (dsgpio->yd[i] == 0) break;
	  for ( ; i < dsgpio->nx; i++)
	    if (dsgpio->yd[i] == 1) break;
	  if (i < dsgpio->nx) lper = i - lstart;
	  if (start != lstart)
             {
                 win_printf("Found sart of period 2 at %d from GPIO istead of %d (I use GPIO)",lstart,start);
                 start = lstart;
             }
	  if (lper != per)
             {
                 win_printf("Found per at %d from GPIO istead of %d ( I use GPIO)",lper,per);	  
                 per = lper;
             }
	}
    }

  n_per -= 1; // skip first period

  snprintf(message,sizeof(message),"Extracting periodic modulation\n"
           "Define period %%4d\n"
           "Nb. of period %%4d\n"
           "Found index of first period at %d\n"
           "Nb of period to skip %%4d\n"
           "%%b click here to impose the starting index\n"
           "\\delta \\phi %%5f\n",start);
  i = win_scanf(message,&per, &n_per, &skip,&mod_start, &dphi);
  if (i == WIN_CANCEL)	return OFF;

  if (mod_start)
     {
         i = win_scanf("Define the starting index %5d",&start);
         if (i == WIN_CANCEL)	return OFF;
     }
  else start += skip * per;

  if ((ds = create_and_attach_one_ds(op, per, per, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  if ((dssi = create_and_attach_one_ds(op, per, per, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = start, i = 0; j < nf && j < start + n_per * per; j++)
  {
    ds->yd[i] += dsi->yd[j];
    ds->xd[i] += 1;
    i++;
    i = (i < per) ? i : 0;
  }

  for (j = start, i = 0; j < nf && i < per; j++, i++)
  {
    ds->yd[i] /= (ds->xd[i] > 0) ? ds->xd[i] : 1;
    dssi->yd[i] = ((float)per/(M_PI*2)) * (cos((M_PI * 2 * i+1)/per) - cos((M_PI * 2 * i)/per));
    dssi->xd[i] = ds->xd[i] = dsi->xd[j];
  }
  for (re = im = 0, i = 0; i < per; i++)
    {
      re += ds->yd[i] * cos((M_PI * (2 * i - 1))/per);
      im += ds->yd[i] * sin((M_PI * (2 * i - 1))/per);
    }
  phi = atan2(im,re);
  win_printf("en Cos = %g, en Sin = %g\n\\phi = %g, \\phi_c = %g"
	     ,re,im,(phi*180/M_PI-90),(phi*180/M_PI)-dphi-90);
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Periodic signal %d period %d n_per offset %d %f",per, n_per, start);
  /* refisplay the entire plot */
  if (was_imr) refresh_image(imr, UNCHANGED);
  else refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int do_plt_Extract_periodic_signal_cor_decay(void)
{
  int i, j, k;
  O_p *op = NULL, *opn = NULL;
  int nf, was_imr = 0;
  d_s *ds, *dsi, *dssi, *dsgpio = NULL;
  pltreg *pr = NULL;
  imreg *imr = NULL;
  O_i *oi;
  int lper = 0, ln_per = 0, lstart = 0, ln_per_0 = 0;
  static int per = 20, n_per = 10, start = 10, skip = 0, mod_start = 0;
  char *st = NULL;
  static float dphi = 0;
  char message[2048] = {0};


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }

  /*

"Led Cycle: %5d camera frames forone period of modulation\n"
	     "DAC constant signal LED0 %d ; LED1 %d\n"
	     "number of modulation periods %d\n"
	     "DAC modulation amplitude LED0 %d ; LED1 %d\n"
	     "number of periods %d of signal\n"
	     "DAC stand by signal LED0 %d ; LED1 %d\n"
  */

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
   {
      if (ac_grep(cur_ac_reg,"%imr%oi",&imr,&oi) != 2)
         return win_printf_OK("cannot image data");
      op = find_oi_cur_op(oi);
      dsi = find_source_specific_ds_in_op(op, "Data taken at xc =");
      if (dsi == NULL) return win_printf_OK("cannot find data set ");
      was_imr = 1;
   }

  nf = dsi->nx;	/* this is the number of points in the data set */
 
  if (dsi->source != NULL)
    {
      if (strncmp(dsi->source,"Data taken at xc =",18) == 0
	  || strncmp(dsi->source,"Data take at xc =",17) == 0)
	{
	  st = strstr(dsi->source, "number of modulation periods");
	  if (st != NULL)
	    {
	      if (sscanf(st, "number of modulation periods %d", &ln_per_0) != 1)
		{
		  win_printf("Cannot find number of modulation periods");
		}
	      else ln_per_0 = ln_per_0;
	    } 
	  else return win_printf_OK("Cannot find :number of modulation periods");
	  
	  st = strstr(dsi->source, "Led Cycle:");
	  if (st != NULL)
	    {
	      if (sscanf(st, "Led Cycle: %d", &lper) != 1)
		{
		  win_printf("Cannot find number of camera frames per  periods");
		}
	      else per = lper;
	    }
	  else return win_printf_OK("Cannot find :Led Cycle:");
	  st = strstr(dsi->source, "number of periods ");
	  if (st != NULL)
	    {
	      if (sscanf(st, "number of periods  %d", &ln_per) != 1)
		{
		  win_printf("Cannot find number of periods ");
		}
	      else n_per = ln_per;
	    }
	  else return win_printf_OK("Cannot find :number of periods ");
	}
      //else return win_printf_OK("incompatible data source ;\nData taken at xc =\nor\n"
      //			"Data take at xc =\n%s",dsi->source);
    }
  start = ln_per_0 * per;
  dsgpio = find_source_specific_ds_in_op(op, "GPIO 1 synchro for Data taken at xc =");
  if (dsgpio != NULL)
    {  // we recover modulation parameter from GPIO stuff 
      for (i = 0; i < dsgpio->nx; i++)
	if (dsgpio->yd[i] == 1) break;
      if (i < dsgpio->nx) 
	{
          for (; i < dsgpio->nx && dsgpio->yd[i] >0; i++);
          for (; i < dsgpio->nx && dsgpio->yd[i] <1; i++);
	  lstart = i;
	  for (i = lstart; i < dsgpio->nx; i++)
	    if (dsgpio->yd[i] == 0) break;
	  for ( ; i < dsgpio->nx; i++)
	    if (dsgpio->yd[i] == 1) break;
	  if (i < dsgpio->nx) lper = i - lstart;
	  if (start != lstart)
             {
                 win_printf("Found sart of period 2 at %d from GPIO istead of %d (I use GPIO)",lstart,start);
                 start = lstart;
             }
	  if (lper != per)
             {
                 win_printf("Found per at %d from GPIO istead of %d ( I use GPIO)",lper,per);	  
                 per = lper;
             }
	}
    }

  n_per -= 1; // skip first period

  snprintf(message,sizeof(message),"Extracting periodic modulation\n"
           "Define period %%4d\n"
           "Nb. of period %%4d\n"
           "Found index of first period at %d\n"
           "Nb of period to skip %%4d\n"
           "%%b click here to impose the starting index\n"
           "\\delta \\phi %%5f\n",start);
  i = win_scanf(message,&per, &n_per, &skip,&mod_start, &dphi);
  if (i == WIN_CANCEL)	return OFF;

  if (mod_start)
     {
         i = win_scanf("Define the starting index %5d",&start);
         if (i == WIN_CANCEL)	return OFF;
     }
  else start += skip * per;

  if (opn == NULL)
    {
      if ((opn = create_and_attach_one_plot(pr, n_per, n_per, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      dssi = opn->dat[0];
    }
  if ((ds = create_and_attach_one_ds(opn, per, per, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  //if ((dssi = create_and_attach_one_ds(op, per, per, 0)) == NULL)
  //return win_printf_OK("cannot create plot !");

  for (j = start, i = k = 0; j < nf && j < start + n_per * per; j++)
  {
    if (ds == NULL)
      {
	if ((ds = create_and_attach_one_ds(opn, per, per, 0)) == NULL)
	  return win_printf_OK("cannot create plot !");
      }
    ds->yd[i] = dsi->yd[j];
    ds->xd[i] = i;
    dssi->yd[k] += (dsi->yd[j] > 0) ? log(dsi->yd[j]) : -50;
    dssi->xd[k] += 1;    
    i++;
    if (i >= per)
      {
	i = 0;
	k++;
	ds = NULL;
      }
  }
  for (i = 0; i < n_per; i++)
  {
    dssi->yd[i] /= (dssi->xd[i] > 0) ? dssi->xd[i] : 1;
    dssi->xd[i] = i;
  }
  double *ai = NULL;
  int ret;
  ret = fit_all_ds_to_xn_polynome(dssi, 3, &ai);
  win_printf("fit ret %d a0 = %g a1 = %g a2 = %g",ret,ai[0] ,ai[1] ,ai[2]);
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Periodic signal %d period %d n_per offset %d %f",per, n_per, start);
  /* refisplay the entire plot */
  if (was_imr) refresh_image(imr, UNCHANGED);
  else refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int do_feuille_extract_re_im(void)
{
  int i, j;
  imreg *imr = NULL;
  O_i *oi = NULL;
  O_p *opi = NULL;
  int  nfi, onx, ony;
  d_s *ds, *dssi, *dsgpio = NULL;
  int lper = 0, ln_per = 0, lstart = 0, ln_per_0 = 0;
  static int per = 20, n_per = 6, start = 10, reim = 1, skip = 0, mod_start = 0, window = 1, harmo = 1;
  char *st = NULL;
  double re, im;
  static float dphi = 0;
  char message[2048] = {0};

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%imr%oi",&imr,&oi) != 2)
    return win_printf_OK("cannot find data");

  ony = oi->im.ny;	onx = oi->im.nx;
  nfi = abs(oi->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
 
  if (nfi < 2) return win_printf_OK("this is not a movie");
  if (oi->im.source != NULL)
    {
      if (strncmp(oi->im.source,"Data taken at xc =",18) == 0
	  || strncmp(oi->im.source,"Data take at xc =",17) == 0)
	{
	  st = strstr(oi->im.source, "number of modulation periods");
	  if (st != NULL)
	    {
	      if (sscanf(st, "number of modulation periods %d", &ln_per_0) != 1)
		{
		  win_printf("Cannot find number of modulation periods");
		}
	      else ln_per_0 = ln_per_0;
	    } 
	  else return win_printf_OK("Cannot find :number of modulation periods");
	  
	  st = strstr(oi->im.source, "Led Cycle:");
	  if (st != NULL)
	    {
	      if (sscanf(st, "Led Cycle: %d", &lper) != 1)
		{
		  win_printf("Cannot find number of camera frames per  periods");
		}
	      else per = lper;
	    }
	  else return win_printf_OK("Cannot find :Led Cycle:");
	  st = strstr(oi->im.source, "number of periods ");
	  if (st != NULL)
	    {
	      if (sscanf(st, "number of periods  %d", &ln_per) != 1)
		{
		  win_printf("Cannot find number of periods ");
		}
	      else n_per = ln_per;
	    }
	  else return win_printf_OK("Cannot find :number of periods ");
	  st = strstr(oi->im.source, "First signal modulation at ");
	  if (st != NULL)
	    {
	      if (sscanf(st, "First signal modulation at %d", &lstart) != 1)
		{
		  win_printf("Cannot find number of periods ");
		}
	      else start = lstart;
	    }
	  else return win_printf_OK("Cannot find :number of periods ");

	}
    }
  opi = find_oi_cur_op(oi);
  dsgpio = find_source_specific_ds_in_op(opi, "GPIO 1 synchro");
  if (dsgpio != NULL)
    {   //  we llok for the start of the first period
        for (i = 0; i < dsgpio->nx && dsgpio->yd[i] < 1; i++);
        int i0 = i;
        if (i < dsgpio->nx)
           {
               for (; i < dsgpio->nx && dsgpio->yd[i] >0; i++);
               for (; i < dsgpio->nx && dsgpio->yd[i] <1; i++);
               start = i;
               win_printf("First cycle found at %d\nsecond cycle found by GPIO starts at %d",i0,start);
           }
    }
  n_per -= 1; // skip first period
  snprintf(message,sizeof(message),"Extracting periodic modulation\n"
           "Define period %%4d\n"
           "Nb. of period %%4d\n"
           "Found index of first period at %d\n"
           "Nb of period to skip %%4d\n"
           "%%b click here to impose the starting index\n"
           "\\delta \\phi %%5f\n"
           "Outupt %%Ra complex image or %%rRe|Im side by side\n"
	   "Apply a windowing %%b, look for hramonic %%4d\n",start);
  i = win_scanf(message,&per, &n_per, &skip,&mod_start, &dphi,&reim,&window,&harmo);
  if (i == WIN_CANCEL)	return OFF;

  if (mod_start)
     {
         i = win_scanf("Define the starting index %5d",&start);
         if (i == WIN_CANCEL)	return OFF;
     }
  else start += skip * per;


  if ((ds = build_data_set(nfi, nfi)) == NULL)
    return win_printf_OK("cannot create plot !");

  if ((dssi = build_data_set(per, per)) == NULL)
    return win_printf_OK("cannot create plot !");

  O_i *oid = NULL;
  if (reim == 0) oid = create_and_attach_oi_to_imr(imr, onx, ony, IS_COMPLEX_IMAGE);
  else oid = create_and_attach_oi_to_imr(imr, 2*onx, ony, IS_FLOAT_IMAGE);
  if (oid == NULL) return win_printf_OK("cannot create plot !");
  int ix, iy, ipw, n_per_eff;
  n_per_eff = n_per - skip;
  for (iy = 0; iy < ony; iy++)
    {
      for (ix = 0; ix < onx; ix++)
	{
	  extract_z_profile_from_movie(oi, ix, iy, ds->yd);
	  for (i = 0; i < per; i++) dssi->yd[i] = dssi->xd[i] = 0;
	  for (j = start, i = 0; j < nfi && j < start + n_per * per; j++)
	    {
	      if (window)
		{
		  ipw = (j - start)/per;
		  dssi->yd[i] += (1 - cos((M_PI * 2 * ipw)/n_per_eff))*ds->yd[j];
		}
	      else 	dssi->yd[i] += ds->yd[j];
	      dssi->xd[i] += 1;
	      i++;
	      i = (i < per) ? i : 0;
	    }
	  for (i = 0; i < per; i++)
	    {
	      dssi->yd[i] /= (dssi->xd[i] > 0) ? dssi->xd[i] : 1;
	    }
	  for (re = im = 0, i = 0; i < per; i++)
	    {
	      re += dssi->yd[i] * cos(dphi+(M_PI * harmo * (2 * i - 1))/per);
	      im += dssi->yd[i] * sin(dphi+(M_PI * harmo * (2 * i - 1))/per);
	    }
	  if (reim == 0)
	    {
	      oid->im.pixel[iy].fl[2*ix] = re;
	      oid->im.pixel[iy].fl[2*ix+1] = im;
	    }
	  else
	    {
	      oid->im.pixel[iy].fl[ix] = re;
	      oid->im.pixel[iy].fl[onx+ix] = im;
	    }
	} 
    } 
  free_data_set(ds);
  free_data_set(dssi);
  /* now we must do some house keeping */
  inherit_from_im_to_im(oid,oi);
  uns_oi_2_oi(oid,oi);
  set_oi_treatement(oid, "Projection on a Fourier component starting at %d with a "
		    "period of %d averaging %d period phase %g radians, windowing %s harmonic %d"
		    ,start,per,n_per_eff,dphi,((window)?"On":"Off"),harmo);
  oid->need_to_refresh = ALL_NEED_REFRESH;
  set_oi_horizontal_extend(oid, 1+reim);
  /* refisplay the entire plot */
  find_zmin_zmax(oid);
  return (refresh_image(imr, imr->n_oi - 1));
}

int rotate_image_phase(void)
{
    O_i *oi = NULL;
    imreg *imr = NULL;
    int i, j;
    double ph, amp, phm, ampm, phmv;
    char question[1024] = {0};
    static int pi_2 = 1;
    static float phuser = 0;
    
    if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
      {
	return win_printf_OK("This routine multiply the y coordinate"
			     "of a data set by a number");
      }
    
    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%imr%oi",&imr,&oi) != 2)
      return win_printf_OK("cannot find data");
    
    if (oi->im.data_type != IS_COMPLEX_IMAGE)
      return win_printf_OK("You need a complex image to turn phase!");

    
    
    float re, im;
    for (i = oi->im.nys, phm = ampm =0; i < oi->im.nye; i++)
      {
	for (j = oi->im.nxs; j < oi->im.nxe; j++)
	  {
	    re = oi->im.pixel[i].cp[j].re;
	    im = oi->im.pixel[i].cp[j].im;
	    amp = sqrt(re*re + im*im);
	    if (amp > 0)
	      {
		ph = atan2(im,re);
		phm += ph * amp;
		ampm += amp;
	      }
	  }
      }
    phm = (ampm > 0) ? phm/ampm : phm;

    snprintf(question,sizeof(question),"The average phase of the visible area is %g radians\n"
	     "Specify if you want to change this average to:\n"
	     "%%R->0 or %%r->\\pi/2 or %%r->To your specific value\n"
	     "In this las case specify the desired phase %%5f\n",phm);
	     
    i = win_scanf(question,&pi_2,&phuser);
    if (i == WIN_CANCEL) return 0;

    if (pi_2 == 2)
      phmv = phuser;
    else if (pi_2 == 1)
      phmv = -phm;
    else phmv = -phm + M_PI_2;
    phmv = (phmv > M_PI) ? phmv - M_PI : phmv;
    phmv = (phmv < -M_PI) ? phmv + M_PI : phmv;    
    float red, imd;
    red = cos(phmv);
    imd = sin(phmv);    
    for (i = 0; i < oi->im.ny; i++)
      {
	for (j = 0; j < oi->im.nx; j++)
	  {
	    re = oi->im.pixel[i].cp[j].re;
	    im = oi->im.pixel[i].cp[j].im;
	    oi->im.pixel[i].cp[j].re = re * red - im * imd;
	    oi->im.pixel[i].cp[j].im = re * imd + red * im;
	  }
      }
    update_oi_treatment(oi,"Turn phase by %g radians", phmv);
    find_zmin_zmax(oi);
    return refresh_image(imr, UNCHANGED);
}


int do_feuille_rescale_plot(void)
{
  int i, j;
  O_p *op = NULL, *opn = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == WIN_CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  set_plot_title(opn, "Multiply by %f",factor);
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}


int average_partial_image_in_ds(O_i *oi,int id, unsigned long long t, unsigned long long dt, void*ptr)
{
  register int i, j;
  int x0, x1, y0, y1, itmp, val;
  float tmp;
  union pix *ps;
  d_s *ds = NULL;

  (void)t;
  (void)dt;
  (void)id;
  if (oi == NULL || ptr == NULL) return 1;
  ds = (d_s*)ptr;
  my_set_window_title("averaging %d of %d",ds->nx, ds->mx);
  if (ds->nx >= ds->mx || ds->ny >= ds->my) return 0; // no averaging
  if (ds->nx == ds->user_ispare[4])
    {
      read_Due_param("S=", &val);
      set_ds_treatement(ds, "Modulation started at %d",val);
    }
  x0 = ds->user_ispare[0] - ds->user_ispare[2]/2;
  x0 = (x0 < 0) ? 0 : x0;
  x1 = x0 + ds->user_ispare[2];
  x1 = (x1 > oi->im.nx) ? oi->im.nx : x1;
  y0 = ds->user_ispare[1] - ds->user_ispare[3]/2;
  y0 = (y0 < 0) ? 0 : y0;
  y1 = y0 + ds->user_ispare[3];
  y1 = (y1 > oi->im.ny) ? oi->im.ny : y1;
  ps = oi->im.pixel;
  if (oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      itmp += ps[i].ch[j];
	    }
	  tmp += (float)itmp;
	}
    }
  if (oi->im.data_type == IS_INT_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      itmp += ps[i].in[j];
	    }
	  tmp += (float)itmp;
	}
    }
  if (oi->im.data_type == IS_UINT_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      itmp += ps[i].ui[j];
	    }
	  tmp += (float)itmp;
	}
    }
  itmp = (y1 - y0)*(x1-x0);
  if (itmp > 0) tmp /= itmp;
  ds->yd[ds->nx] = tmp;
  ds->xd[ds->nx] = ds->nx;//id;
  ds->nx = ds->ny = ds->nx + 1;
  return 0;
}



int average_partial_image_in_oi_and_ds(O_i *oi,int id, unsigned long long t, unsigned long long dt, void*ptr)
{
  register int i, j;
  int x0, x1, y0, y1, itmp, val, error;
  float tmp;
  union pix *ps, *pd;
  d_s *ds = NULL, *ds2 = NULL;
  O_p *op = NULL;
  O_i *oim = NULL;
  static int start = 0;


  (void)t;
  (void)dt;
  (void)id;
  if (oi == NULL || ptr == NULL) return 1;
  oim = (O_i*)ptr;
  op = find_oi_cur_op(oim);
  ds = op->dat[0];
  ds2 = (op->n_dat > 1) ? op->dat[1] : NULL;
  my_set_window_title("averaging %d of %d",ds->nx, ds->mx);
  if (ds->nx >= ds->mx || ds->ny >= ds->my) return 0; // no averaging
  if (ds->nx == ds->user_ispare[4])
    {
      read_Due_param("S=", &val);
      set_ds_treatement(ds, "Modulation started at %d",val);
      start = 0;
    }
  x0 = ds->user_ispare[0] - ds->user_ispare[2]/2;
  x0 = (x0 < 0) ? 0 : x0;
  x1 = x0 + ds->user_ispare[2];
  x1 = (x1 > oi->im.nx) ? oi->im.nx : x1;
  y0 = ds->user_ispare[1] - ds->user_ispare[3]/2;
  y0 = (y0 < 0) ? 0 : y0;
  y1 = y0 + ds->user_ispare[3];
  y1 = (y1 > oi->im.ny) ? oi->im.ny : y1;
  ps = oi->im.pixel;
  switch_frame(oim,ds->nx);
  pd = oim->im.pixel;
  if (oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      pd[i-y0].ch[j-x0] = ps[i].ch[j];
	      itmp += ps[i].ch[j];
	    }
	  tmp += (float)itmp;
	}
    }
  if (oi->im.data_type == IS_INT_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      pd[i-y0].in[j-x0] = ps[i].in[j];
	      itmp += ps[i].in[j];
	    }
	  tmp += (float)itmp;
	}
    }
  if (oi->im.data_type == IS_UINT_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      pd[i-y0].ui[j-x0] = ps[i].ui[j];
	      itmp += ps[i].ui[j];
	    }
	  tmp += (float)itmp;
	}
    }
  itmp = (y1 - y0)*(x1-x0);
  if (itmp > 0) tmp /= itmp;
  ds->yd[ds->nx] = tmp;
  ds->xd[ds->nx] = ds->nx;//id;
  i = read_GPIO_1_input(&error);
  ds2->yd[ds2->nx] = i;
  if (error) ds2->yd[ds2->nx] = -1;
  else if (start == 0 && i == 1)
    {
      start = 1;
      set_oi_source(oim, "%s\nFirst signal modulation at %d",ds->source,ds->nx);
    }
  ds->nx = ds->ny = ds->nx + 1;
  ds2->xd[ds2->nx] = ds2->nx;
  ds2->nx = ds2->ny = ds2->nx + 1;
  return 0;
}



int average_partial_image_in_oi_in_time_and_ds(O_i *oi,int id, unsigned long long t, unsigned long long dt, void*ptr)
{
  register int i, j;
  int x0, x1, y0, y1, itmp, val, error, nfi, navg;
  float tmp;
  union pix *ps, *pd;
  d_s *ds = NULL, *ds2 = NULL;
  O_p *op = NULL;
  O_i *oim = NULL;
  static int start = 0;


  (void)t;
  (void)dt;
  (void)id;
  if (oi == NULL || ptr == NULL) return 1;
  oim = (O_i*)ptr;
  op = find_oi_cur_op(oim);
  ds = op->dat[0];
  ds2 = (op->n_dat > 1) ? op->dat[1] : NULL;
  my_set_window_title("averaging %d of %d",ds->nx, ds->mx);

  nfi = abs(oim->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  navg = (int)(ds->mx/nfi);
  if (ds->nx >= ds->mx || ds->ny >= ds->my) return 0; // no averaging
  if (ds->nx == ds->user_ispare[4])
    {
      read_Due_param("S=", &val);
      set_ds_treatement(ds, "Modulation started at %d",val);
      start = 0;
    }
  x0 = ds->user_ispare[0] - ds->user_ispare[2]/2;
  x0 = (x0 < 0) ? 0 : x0;
  x1 = x0 + ds->user_ispare[2];
  x1 = (x1 > oi->im.nx) ? oi->im.nx : x1;
  y0 = ds->user_ispare[1] - ds->user_ispare[3]/2;
  y0 = (y0 < 0) ? 0 : y0;
  y1 = y0 + ds->user_ispare[3];
  y1 = (y1 > oi->im.ny) ? oi->im.ny : y1;
  ps = oi->im.pixel;
  switch_frame(oim,(int)(ds->nx/navg));
  pd = oim->im.pixel;
  if (oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      pd[i-y0].ui[j-x0] += ps[i].ch[j];
	      itmp += ps[i].ch[j];
	    }
	  tmp += (float)itmp;
	}
    }
  if (oi->im.data_type == IS_INT_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      pd[i-y0].in[j-x0] += ps[i].in[j];
	      itmp += ps[i].in[j];
	    }
	  tmp += (float)itmp;
	}
    }
  if (oi->im.data_type == IS_UINT_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      pd[i-y0].ui[j-x0] += ps[i].ui[j];
	      itmp += ps[i].ui[j];
	    }
	  tmp += (float)itmp;
	}
    }
  itmp = (y1 - y0)*(x1-x0);
  if (itmp > 0) tmp /= itmp;
  ds->yd[ds->nx] = tmp;
  ds->xd[ds->nx] = ds->nx;//id;
  i = read_GPIO_1_input(&error);
  ds2->yd[ds2->nx] = i;
  if (error) ds2->yd[ds2->nx] = -1;
  else if (start == 0 && i == 1)
    {
      start = 1;
      set_oi_source(oim, "%s\nFirst signal modulation at %d",ds->source,ds->nx);
    }
  ds->nx = ds->ny = ds->nx + 1;
  ds2->xd[ds2->nx] = ds2->nx;
  ds2->nx = ds2->ny = ds2->nx + 1;
  return 0;
}

int grab_im_avg(void)
{
  int i;
  imreg *imr;
  O_i *ois, *oim = NULL;
  O_p *op;
  d_s *ds, *ds2 = NULL;
  char *s = NULL;
  static int xc = 512, yc = 256, w = 128, h = 128, nf = 200, start = 10;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
      return win_printf_OK("This routine start the averaging of an image area");


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return win_printf_OK("cannot find image region");


  i = win_scanf("Realtime averaging of image area;\nDefine area center xc %6d yc %6d\n"
		"Width %6d Height %6d\nDuration in frame %6d\n"
		"Start modulation after %4d frames"
                ,&xc, &yc, &w, &h, &nf, &start);

  if (i == WIN_CANCEL) return 0;


  oim = create_and_attach_movie_to_imr(imr, w, h, ois->im.data_type, nf);
  if (oim == NULL) return win_printf_OK("cannot create movie of modulation");

  op = create_and_attach_op_to_oi(oim, nf, nf, 0, 0);
  //op = create_and_attach_op_to_oi(ois, nf, nf, 0, 0);
  if (op == NULL) return win_printf_OK("cannot create plot");
  ds = op->dat[0];
  ds->nx = ds->ny = 0;
  if ((ds2 = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create second ds");
  ds2->nx = ds2->ny = 0;
  s = Get_Due_cycle();
  set_ds_source(ds, "Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s", xc,yc,h,w,s);
  set_ds_source(ds2, "GPIO 1 synchro for Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s", xc,yc,h,w,s);
  if (s != NULL) free(s);
  ds->user_ispare[0] = xc;
  ds->user_ispare[1] = yc;
  ds->user_ispare[2] = w;
  ds->user_ispare[3] = h;
  ds->user_ispare[4] = start;
  data_ptr = (void*)oim; // ds;
  track_image = average_partial_image_in_oi_and_ds;//average_partial_image_in_ds;

//for  (; (volatile)(ds->nx) < ds->mx; ) sleep(100);
//track_image = NULL;
  find_zmin_zmax(oim);
  //  we llok for the start of the first period
  for (i = 0; i < ds2->nx && ds2->yd[i] < 1; i++);
  int i0 = i;
  if (i < ds2->nx)
    {
       for (; i < ds2->nx && ds2->yd[i] >0; i++);
       for (; i < ds2->nx && ds2->yd[i] <1; i++);
       start = i;


       set_ds_source(ds, "Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s;"
                     " First point of per 0 = %d, first point of per 1 = %d", xc,yc,h,w,s, i0, i);
       set_ds_source(ds2, "GPIO 1 synchro for Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s"
                     " First point of per 0 = %d, first point of per 1 = %d", xc,yc,h,w,s, i0, i);

       set_oi_source(oim, "%s\nFirst point of per 0 = %d, first point of per 1 = %d",oim->im.source, i0, i);

    }


  ois->need_to_refresh |= ALL_NEED_REFRESH;
  return refresh_image(imr, imr->n_oi - 1);
 }


int grab_im_avg_in_time(void)
{
  int i, type;
  imreg *imr;
  O_i *ois, *oim = NULL;
  O_p *op;
  d_s *ds, *ds2 = NULL;
  char *s = NULL;
  static int xc = 512, yc = 256, w = 128, h = 128, nf = 200, nv = 16, start = 10;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
      return win_printf_OK("This routine start the averaging of an image area");


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return win_printf_OK("cannot find image region");


  i = win_scanf("Realtime averaging of image area;\n"
                "Define area center xc %6d yc %6d\n"
		"Width %6d Height %6d\n"
                "Nb of video frames averaged in 1 frame of movie Nv=%5d\n"
                "Nb of frame in the movie Nf=%6d\n"
                "Duration will be Nf*Nv\n"
		"Start modulation after %4d frames"
                ,&xc, &yc, &w, &h, &nv, &nf, &start);

  if (i == WIN_CANCEL) return 0;

  type = (ois->im.data_type == IS_CHAR_IMAGE) ? IS_UINT_IMAGE : ois->im.data_type;
  oim = create_and_attach_movie_to_imr(imr, w, h, type, nf);
  if (oim == NULL) return win_printf_OK("cannot create movie of modulation");

  op = create_and_attach_op_to_oi(oim, nv*nf, nv*nf, 0, 0);
  //op = create_and_attach_op_to_oi(ois, nf, nf, 0, 0);
  if (op == NULL) return win_printf_OK("cannot create plot");
  ds = op->dat[0];
  ds->nx = ds->ny = 0;
  if ((ds2 = create_and_attach_one_ds(op, nv*nf, nv*nf, 0)) == NULL)
    return win_printf_OK("cannot create second ds");
  ds2->nx = ds2->ny = 0;
  s = Get_Due_cycle();
  set_ds_source(ds, "Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s", xc,yc,h,w,s);
  set_ds_source(ds2, "GPIO 1 synchro for Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s", xc,yc,h,w,s);
  if (s != NULL) free(s);
  ds->user_ispare[0] = xc;
  ds->user_ispare[1] = yc;
  ds->user_ispare[2] = w;
  ds->user_ispare[3] = h;
  ds->user_ispare[4] = start;
  data_ptr = (void*)oim; // ds;
  set_zmin_zmax_values(oim, 0, (ois->im.data_type == IS_CHAR_IMAGE) ? 255*nv : 4095*nv);
  set_z_black_z_white_values(oim, 0, (ois->im.data_type == IS_CHAR_IMAGE) ? 255*nv : 4095*nv);
  track_image = average_partial_image_in_oi_in_time_and_ds;//average_partial_image_in_ds;

//for  (; (volatile)(ds->nx) < ds->mx; ) sleep(100);
//track_image = NULL;
  find_zmin_zmax(oim);
  //  we llok for the start of the first period
  for (i = 0; i < ds2->nx && ds2->yd[i] < 1; i++);
  int i0 = i;
  if (i < ds2->nx)
    {
       for (; i < ds2->nx && ds2->yd[i] >0; i++);
       for (; i < ds2->nx && ds2->yd[i] <1; i++);
       start = i;


       set_ds_source(ds, "Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s;"
                     " First point of per 0 = %d, first point of per 1 = %d", xc,yc,h,w,s, i0, i);
       set_ds_source(ds2, "GPIO 1 synchro for Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s"
                     " First point of per 0 = %d, first point of per 1 = %d", xc,yc,h,w,s, i0, i);

       set_oi_source(oim, "%s\nFirst point of per 0 = %d, first point of per 1 = %d",oim->im.source, i0, i);

    }


  ois->need_to_refresh |= ALL_NEED_REFRESH;
  return refresh_image(imr, imr->n_oi - 1);
 }


int mean_and_sigma_of_small_image(void)
{
  int i, j, ii, jj, onx , ony, onx2, ony2;
  imreg *imr;
  O_i *ois, *oim = NULL;
  static int xm = 8, ym = 8;
  double val, mean ,sigma;
  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
      return win_printf_OK("This routine compute mean and sigma of small area of an image");


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return win_printf_OK("cannot find image region");


  i = win_scanf("This routine compute mean and sigma of small area of an image\n"
                "Define size of ROI in x %4d in y %4d\n"
                ,&xm, &ym);

  if (i == WIN_CANCEL) return 0;
  onx = ois->im.nx;
  ony = ois->im.ny;  

  onx2 = onx/xm;
  ony2 = ony/ym;  
  oim = create_and_attach_movie_to_imr(imr, onx2, ony2, IS_FLOAT_IMAGE, 2);
  if (oim == NULL) return win_printf_OK("cannot create movie of modulation");

  for (i = 0; i < ony2; i++)
    {
      for (j = 0; j < onx2; j++)
	{
	  for (ii = 0,mean = 0; ii < ym; ii++)
	    {
	      for (jj = 0; jj < xm; jj++)
		{
		  val = 0;
		  get_raw_pixel_value(ois, j*xm+jj, i*ym+ii, &val);
		  mean += val;
		}
	    }
	  mean /= xm*ym;
	  for (ii = 0,sigma = 0; ii < ym; ii++)
	    {
	      for (jj = 0; jj < xm; jj++)
		{
		  val = 0;
		  get_raw_pixel_value(ois, j*xm+jj, i*ym+ii, &val);
		  sigma += (val - mean)*(val - mean);
		}
	    }
	  sigma /= (xm*ym)-1;
	  sigma = sqrt(sigma);
	  oim->im.pxl[0][i].fl[j] = mean;
	  oim->im.pxl[1][i].fl[j] = sigma;	
	}
    }
  find_zmin_zmax(oim); 
  oim->need_to_refresh |= ALL_NEED_REFRESH;
  return refresh_image(imr, imr->n_oi - 1);

}


MENU *feuille_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  //add_item_to_menu(mn,"Hello example", do_feuille_hello,NULL,0,NULL);
  add_item_to_menu(mn,"Extract periodic signal", do_plt_Extract_periodic_signal,NULL,0,NULL);
  //add_item_to_menu(mn,"plot rescale in Y", do_feuille_rescale_plot,NULL,0,NULL);
  add_item_to_menu(mn,"Extract plot periodic signal cor decay", do_plt_Extract_periodic_signal_cor_decay,NULL,0,NULL);    
  return mn;
}

MENU *feuille_image_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;

  add_item_to_menu(mn,"start camera", do_xvuEye_init_rolling_buffer_movie_xvin,NULL,0,NULL);
  add_item_to_menu(mn,"chg Freq", do_set_camera_freq,NULL,0,NULL);
  add_item_to_menu(mn,"chg exposure", do_set_camera_exposure,NULL,0,NULL);
  
  add_item_to_menu(mn,"chg gain", do_set_camera_gain,NULL,0,NULL);
  add_item_to_menu(mn,"chg binning", do_set_camera_binning,NULL,0,NULL);
  add_item_to_menu(mn,"chg black level", adjust_black_level,NULL,0,NULL);
  
  add_item_to_menu(mn,"stop uEye movie", do_stop_camera,NULL,0,NULL);
  add_item_to_menu(mn,"freeze uEye movie", do_freeze_camera,NULL,0,NULL);
  add_item_to_menu(mn,"live uEye movie", do_live_camera,NULL,0,NULL);
  add_item_to_menu(mn,"Adjust AOI origin", adj_AOI_position,NULL,0,NULL);
  add_item_to_menu(mn,"Read GPIO 1",do_read_gpio ,NULL,0,NULL);
  add_item_to_menu(mn,"stop uEye movie", do_stop_camera,NULL,0,NULL);
  add_item_to_menu(mn, "\0",   NULL,   NULL,   0, NULL);
  add_item_to_menu(mn, "Define modulation", Set_Due_cycle, NULL, 0, NULL);
  //add_item_to_menu(mn, "Start modulation", start_modulation, NULL, 0, NULL);
  add_item_to_menu(mn,"Average partial image", grab_im_avg,NULL,0,NULL);
  add_item_to_menu(mn,"Average partial image in space and time", grab_im_avg_in_time,NULL,0,NULL);
  add_item_to_menu(mn, "\0",   NULL,   NULL,   0, NULL);
  add_item_to_menu(mn,"Extract image periodic signal", do_feuille_extract_re_im,NULL,0,NULL);
  add_item_to_menu(mn,"Extract plot periodic signal", do_plt_Extract_periodic_signal,NULL,0,NULL);

  add_item_to_menu(mn,"Extract plot periodic signal cor decay", do_plt_Extract_periodic_signal_cor_decay,NULL,0,NULL);
  add_item_to_menu(mn,"Rotate complex image phase",rotate_image_phase ,NULL,0,NULL);  

  add_item_to_menu(mn,"mean and sigma small image",mean_and_sigma_of_small_image ,NULL,0,NULL);  


  return mn;
}


int	feuille_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "feuille", NULL, feuille_plot_menu(), 0, NULL);
  add_image_treat_menu_item ( "feuille", NULL, feuille_image_menu(), 0, NULL);
  Due_serial_main(argc, argv);
  return D_O_K;
}

int	feuille_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "feuille", NULL, NULL);
  remove_item_to_menu(image_treat_menu, "feuille", NULL, NULL);
  return D_O_K;
}
#endif

