#pragma once
PXV_FUNC(int, do_feuille_rescale_plot, (void));
PXV_FUNC(MENU*, feuille_plot_menu, (void));
PXV_FUNC(int, do_feuille_rescale_data_set, (void));
PXV_FUNC(int, feuille_main, (int argc, char **argv));
