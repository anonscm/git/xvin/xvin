#ifndef _HIST2D_C_
#define _HIST2D_C_

#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"

#include <gsl/gsl_histogram.h>
#include <gsl/gsl_histogram2d.h>
#include <gsl/gsl_statistics_float.h> // to compute mean and variance

/* If you include other plug-ins header do it here*/ 
// #include "../inout/inout.h"
#include "../hist/hist.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "hist2d.h"



// hist must be allocated!
int fill_histogram2d_with_averaged_float_data(gsl_histogram2d *hist, float *x, float *y, int nx, int tau, int shift)
{   register int j, j2;
    double x_tau, y_tau;

    for (j=0; j<nx-tau+1; j+=shift)
	{	x_tau=(double)0.0;
	    y_tau=(double)0.0;
		for (j2=0; j2<tau; j2++)
		{	x_tau += (double)x[(j+j2)];		// average over tau points
  		    y_tau += (double)y[(j+j2)];		// average over tau points
		}
		gsl_histogram2d_increment(hist, (double)(x_tau/tau), (double)(y_tau/tau)); // histogram of averaged data
	}
    return(0);
}


		
int do_hist2d_build_from_ds(void)
{
register int 	i;
	pltreg	*pr = NULL;
	O_p 	*op1 = NULL;
    d_s 	*ds2, *ds1;
	imreg 	*imr;			/* image region 			*/
	O_i 	*oi;		    	/* one image source and destination 	*/
static float range=5.;
static int	shift=1, bool_take_log=1, bool_update_unitsets=1;
static int	nhx=400+1, nhy=400+1;
static	char	tau_string[128]="1,2,5,10:5:50";
	double  x1, x2, y1, y2;
	float	hx_min=-1, hx_max=3, hy_min=-1, hy_max=3;
    float mean_x, var_x, mean_y, var_y;
	int nx;
	int	tau, n_tau;
	int	*tau_index=NULL;
	int	index;
	gsl_histogram2d *hist;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	if (index==HIST_AVERAGE)
			return win_printf_OK("This routine builds a 2d-histogram\n"
				"out of the current and previous datasets, and place it in a new plot\n\n"
				"data a(t) is first averaged using a sliding average:\n\n"
				"a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt'\n\n"
				"(this is for example used in the study of FTs)");
		if (index==HIST_NO_AVERAGE)
			return win_printf_OK("This routine builds a 2d-histogram\n"
				"out of the current and previous datasets, and places it in a new plot");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot find dataset");
    if (op1->n_dat<2)                                       return win_printf_OK("I need at least 2 datasets"); 
    ds2 = (op1->cur_dat==0) ? op1->dat[op1->n_dat-1] : op1->dat[op1->cur_dat-1]; // previous dataset in the stack
    if (ds1->nx != ds2->nx)                                 
       return win_printf_OK("active ds %d has %d points but ds %d has %d points", op1->cur_dat, ds1->nx, op1->cur_dat-1, ds2->nx);                     
	nx=ds1->nx;

	mean_x = gsl_stats_float_mean(ds1->yd, 1, nx);
	var_x  = gsl_stats_float_sd_m(ds1->yd, 1, nx, mean_x);
	mean_y = gsl_stats_float_mean(ds2->yd, 1, nx);
	var_y  = gsl_stats_float_sd_m(ds2->yd, 1, nx, mean_y);

	hx_max = mean_x + range*var_x;
    hx_min = mean_x - range*var_x;
	hy_max = mean_y + range*var_y;
    hy_min = mean_y - range*var_y;

    i=win_scanf("{\\color{yellow}\\pt14 building histograms}\n\n"
                "number of bins in histogram in X : %5d\n"
                    "min of range in X : %8f\n"
                    "max of range in X : %8f\n"
                    "number of bins in histogram in Y : %5d\n"
                    "min of range in Y : %8f\n"
                    "max of range in Y : %8f\n\n"
                    "%b work with log of pdf",
                    &nhx, &hx_min, &hx_max, &nhy, &hy_min, &hy_max, &bool_take_log);
    if (i==CANCEL) return OFF;
    
	if (index==HIST_NO_AVERAGE)
	{	
	}
	else if (index==HIST_AVERAGE)
	{	i=win_scanf("using averaged data a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt' \n\n"
			"sampled every N points\n\n \\tau (integer, nb of pts) ? %s shift by (integer, N>=1) %5d",
			&tau_string, &shift);
	}
	else return OFF;
	if (i==CANCEL) return OFF;

	if (index==HIST_NO_AVERAGE)
		tau_index = str_to_index("1", &n_tau);
	else	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	return(win_printf_OK("bad values for \\tau !"));

    if (n_tau!=1) return(win_printf_OK("At the moment, I can work with only 1 value\nof \\tau at the same time!"));

    // create new image:
    imr = create_and_register_new_image_project( 0,   32,  900,  668);
	if (imr == NULL) return(win_printf_OK("could not create imreg"));	
    oi = create_and_attach_oi_to_imr(imr, nhx, nhy, IS_FLOAT_IMAGE);
    remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
	                        select_image_of_imreg(imr, imr->n_oi -1);

	for (i=0; i<n_tau; i++)
	{	tau=tau_index[i];

		hist = (gsl_histogram2d *)gsl_histogram2d_alloc(nhx, nhy);
		gsl_histogram2d_set_ranges_uniform (hist, (double)hx_min, (double)hx_max, (double)hy_min, (double)hy_max);

        fill_histogram2d_with_averaged_float_data(hist, ds1->yd, ds2->yd, nx, tau, shift);

		histogram2d_to_oi(hist, oi, bool_take_log);

		if (bool_update_unitsets==1)
        {  gsl_histogram2d_get_xrange (hist, 0, &x1, &x2);
           set_unit_offset(oi->xu[0], (float)(x1+x2)/2.);
           set_unit_increment(oi->xu[0], (float)(x2-x1) );
           set_oi_x_unit_set (oi, 0);
       
           gsl_histogram2d_get_yrange (hist, 0, &y1, &y2);
           set_unit_offset(oi->yu[0], (float)(y1+y2)/2.);
           set_unit_increment(oi->yu[0], (float)(y2-y1) );
           set_oi_y_unit_set (oi, 0);
        }
		gsl_histogram2d_free(hist);

	//	inherit_from_ds_to_ds(ds2, ds1);
	//	ds2->treatement = my_sprintf(ds2->treatement,"histogram, averaged data over \\tau=%d points", tau);
	
	}
	free(tau_index);

	oi->im.source = Mystrdup(ds1->source);
    set_im_title(oi, "2d-pdf");
    set_im_x_title(oi, "%s", op1->x_title);
    set_im_y_title(oi, "%s", op1->y_title);
	find_zmin_zmax(oi);			/* recherche du meilleur contraste pour l'affichage 	*/
	do_one_image(imr);

	return(D_REDRAW);		/* retour et rafraichissement video */

} // end of "do_hist2d_build_from_ds(void)"



int histogram2d_to_oi(gsl_histogram2d *hist, O_i *oi, int bool_take_log) // oi must already be allocated!!
{	register int jx, jy;
    double  z;
	union 	pix *pd;   		/* data des images (matrice) 	       	*/
	int     nhx, nhy;
	
	pd  = oi->im.pixel;

    nhx = gsl_histogram2d_nx(hist);
    nhy = gsl_histogram2d_ny(hist);
    
	for (jx=0; jx<nhx; jx++)
	{	for (jy=0; jy<nhx; jy++)
        {	z = gsl_histogram2d_get(hist, jx, jy);

            if (bool_take_log==1) 
            {   if (z>0)  pd[jy].fl[jx] = (float)log(fabs(z));
                else      pd[jy].fl[jx] = (float)0.;
            }
            else
            {   pd[jy].fl[jx] = (float)(z);
            }
		}
	}

	return(nhx*nhy);
}




int oi_to_histogram2d(O_i *oi, gsl_histogram2d **hist) // hist will be allocated perfectly
{	register int i,j;
	int   nhx, nhy;
	double hx_min, hx_max, hy_min, hy_max, dx, dy;
    
	nhx=oi->im.nx;      nhy=oi->im.ny;
	
	if (oi->xu!=NULL)
	{ dx     = (double)oi->xu[0]->dx;
      hx_min = (double)oi->xu[0]->ax     - dx/2.;
	  hx_max = (double)oi->xu[nhx-1]->ax + dx/2.;
    }
    else return(win_printf_OK("no X-units! I abort"));
    if (oi->yu!=NULL)
	{ dy     = (double)oi->yu[0]->dx;
      hy_min = (double)oi->yu[0]->ax     - dy/2.;
	  hy_max = (double)oi->yu[nhy-1]->ax + dy/2.;
    }
    else return(win_printf_OK("no Y-units! I abort"));
    *hist = (gsl_histogram2d *)gsl_histogram2d_alloc(nhx, nhy);
	gsl_histogram2d_set_ranges_uniform (*hist, hx_min, hx_max, hy_min, hy_max);

	for (i=0; i<nhx; i++)
	for (j=0; j<nhy; j++)
	{	// gsl_histogram2d_accumulate (*hist, hx_min+i*dx, hy_min+j*dy, oi->im.pixel[j].fl[i]);
	    (*hist)->bin[i*nhy + j] += oi->im.pixel[j].fl[i];
	}

	return(nhx*nhy);
}



int do_save_pdf2d(void)
{	imreg  *imr;
	O_i	   *ois;
	gsl_histogram2d *hist;
static int 	bool_normalize=1;
static char 	pdf_filename[512];
	FILE	*file;
	
	if(updating_menu_state != 0)	return D_O_K;		
       	
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine saves the active image in a file.\n"
		"a special (5 columns - ascii) format is used, as specified in the GSL.\n"
		"saved 2d-pdf can be further used in XVin statistical functions...");
	
	if (win_scanf("{\\color{yellow}\\pt14 Save 2d-pdf}\n\n"
		"%b normalize pdf in Z (pdf) before saving\n"
		"filename %s",
		&bool_normalize, &pdf_filename) == CANCEL) return(D_O_K);

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return win_printf_OK("cannot find image!");

	oi_to_histogram2d(ois, &hist); //this allocates hist autimatically
	if (bool_normalize==1)	normalize_histogram2d_Z(hist);

	file=fopen(pdf_filename,"wt");
	gsl_histogram2d_fprintf (file, hist, "%g", "%g");
	// see docs: http://www.gnu.org/software/gsl/manual/html_node/Reading-and-writing-histograms.html
	fclose(file);
	gsl_histogram2d_free(hist);

	return(D_O_K);
} // do_save_pdf



int normalize_histogram2d_Z(gsl_histogram2d *hist)
{//	register int i;
	int nhx, nhy;
	double scale;
	double mean_x, sigma_x, mean_y, sigma_y;
	double N, lower_x, upper_x, lower_y, upper_y, width_x, width_y;

    mean_x  = gsl_histogram2d_xmean(hist);	mean_y  = gsl_histogram2d_ymean(hist);
	sigma_x = gsl_histogram2d_xsigma(hist);	sigma_y = gsl_histogram2d_ysigma(hist);
	
	nhx = gsl_histogram2d_nx(hist); // number of bins;
	nhy = gsl_histogram2d_ny(hist); // number of bins;
	N   = gsl_histogram2d_sum (hist); // sum of all the bins;
	gsl_histogram2d_get_xrange (hist, 1, &lower_x, &upper_x); // lower and upper bound of bin #1
	gsl_histogram2d_get_yrange (hist, 1, &lower_y, &upper_y); // lower and upper bound of bin #1
	width_x = upper_x - lower_x;
	width_y = upper_y - lower_y;	
	scale   = (double)1./(N*width_x*width_y);

	gsl_histogram2d_scale(hist, scale);	// multiplies histogram z-axis by scale

	return(nhx*nhy);
}




MENU *hist2d_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"build histogram from dataset", 	do_hist2d_build_from_ds,			NULL, HIST_NO_AVERAGE,	NULL);
	add_item_to_menu(mn,"average and build histograms from dataset",  do_hist2d_build_from_ds, NULL, HIST_AVERAGE,	NULL);	
	
	return mn;
}


MENU *hist2d_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"save active ds as pdf",		    do_save_pdf2d,		            NULL, 0, NULL);	
   
	return mn;
}


int	hist2d_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("2d-Histograms", NULL, hist2d_plot_menu(), 0, NULL);
	add_image_treat_menu_item("2d-Histograms", NULL, hist2d_image_menu(), 0, NULL);
	return D_O_K;
	return D_O_K;
}


int	hist2d_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,  "2d-Histograms", NULL, NULL);
	remove_item_to_menu(image_treat_menu, "2d-Histograms", NULL, NULL);
     	return D_O_K;
}


#endif
