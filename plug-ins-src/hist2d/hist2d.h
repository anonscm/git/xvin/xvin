#ifndef _HIST2D_H_
#define _HIST2D_H_


PXV_FUNC(MENU, *hist2d_plot_menu,   (void));
PXV_FUNC(MENU, *hist2d_image_menu,  (void));
PXV_FUNC(int,	hist2d_main,        (int argc, char **argv));
PXV_FUNC(int,	hist2d_unload,      (int argc, char **argv));
    
PXV_FUNC(int, fill_histogram2d_with_averaged_float_data, (gsl_histogram2d *hist, float *x, float *y, int nx, int tau, int shift) );
PXV_FUNC(int, do_hist2d_build_from_ds,                   (void) );

PXV_FUNC(int, histogram2d_to_oi,                      (gsl_histogram2d *hist, O_i *oi, int bool_take_log) );
PXV_FUNC(int, oi_to_histogram2d,                      (O_i *oi, gsl_histogram2d **hist) );

PXV_FUNC(int, normalize_histogram2d_Z,                (gsl_histogram2d *hist) );
PXV_FUNC(int, do_save_pdf2d,	                      (void));

#endif

