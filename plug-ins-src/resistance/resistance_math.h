#ifndef _RESISTANCE_MATH_H_
#define _RESISTANCE_MATH_H_

PXV_FUNC(int, resistance_normalize_u,       (double *u, int ny, double R, double I) );
PXV_FUNC(int, resistance_compute_I,			(double *excitation, int ny, double factor, double offset, double i_p, double *I));
PXV_FUNC(int, resistance_compute_du_dt,		(double *u, int ny, double *t, double dt, double *du_dt, int derivate_mode));
PXV_FUNC(int, resistance_compute_i_R,		(double *u, int ny, double *t, double C_local, double *I, double dt, double *i_R, int derivate_mode));
PXV_FUNC(int, resistance_compute_power,	    (double *u, int ny, double R_local, double *It, double I, double dt, int bool_normalize_u, double *w));
PXV_FUNC(int, resistance_compute_Gibbs_entropy_inc, (double *u, int ny, gsl_histogram *hist, double *dS) );
PXV_FUNC(int, resistance_compute_Gibbs_heat_power,  (double *u, int ny, gsl_histogram *hist, double dt, double *Q_Gibbs) );
PXV_FUNC(int, resistance_compute_irrev_heat_power,  (double *Q, double *Q_Gibbs, int ny, double *Q_irrev) );

PXV_FUNC(void,   complex_multiply, (fftw_complex a, fftw_complex b, fftw_complex c));
PXV_FUNC(void,   complex_divide,   (fftw_complex a, fftw_complex b, fftw_complex c));

PXV_FUNC(double, get_R,	(void));	
PXV_FUNC(double, get_C,	(void));
PXV_FUNC(double, get_I,	(void));

#endif

