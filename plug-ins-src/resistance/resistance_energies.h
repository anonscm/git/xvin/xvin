#ifndef _RESISTANCE_ENERGIES_H_
#define _RESISTANCE_ENERGIES_H_


#define RESISTANCE_ENERGY_VARIATION					0x0200
#define RESISTANCE_ENERGY						0x0100


PXV_FUNC(int, do_resistance_compute_heat,		(void));
PXV_FUNC(int, do_resistance_compute_work,		(void));
PXV_FUNC(int, do_resistance_compute_heat_Gibbs,		(void));
PXV_FUNC(int, do_resistance_compute_energy_variation,	(void));

#endif

