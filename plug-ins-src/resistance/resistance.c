#ifndef _RESISTANCE_C_
#define _RESISTANCE_C_

#include "allegro.h"
#include "xvin.h"
#include "fft_filter_lib.h"
#include "xv_tools_lib.h"

#include <gsl/gsl_errno.h> // for error code returning
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_statistics.h>
#include <fftw3.h>

/* If you include other plug-ins header do it here*/ 
#include "../response/response.h"
#include "../inout/inout.h"
#include "../find/find.h"
#include "../hist/hist.h"
#include "../diff/diff.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "resistance.h"
#include "resistance_energies.h"
#include "resistance_math.h"

double  R=9.52e6;
double  C=2.8e-10;
double  I=2.85e-13; // 14e-14;

double  kT = k_b*Temperature;


// transform float en double;
int f_to_d(float *in, double *out, int N)
{	register int i;

	for (i=0; i<N; i++)
	{	out[i] = (double)in[i];
	}
	
	return(0);
}//end of f_to_d



// parametres resistance
int do_set_resistance_parameters(void)
{	float lR=(float)R, lC=(float)C, lI=(float)I, lkT=(float)kT;
	
	if(updating_menu_state != 0)	return D_O_K;	

	if (win_scanf("%10f as k_B.T (enter 0 for room value, 300K)\n\n%10f as C\n\n%10f as R",
			&lkT, &lC, &lR)==CANCEL) return(D_O_K);

	if (lkT==0) kT=(double)k_b*Temperature; 
	R=(double)lR;
	C=(double)lC;
	I=(double)lI;

	return(0);
} // end of do_set_resistance parameters



//////////////////////////////////////////////////////////////////////
//						FIT SPECTRE RESISTANCE						//
//////////////////////////////////////////////////////////////////////
// Fits R et C or just C
int do_fit_LP_RC(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny;
	d_s 	*ds1, *ds2;
	pltreg *pr = NULL;
	int	index;
	int	i_min, i_max, N;
	double	f2, Sp2, sx, sx2, sy, sxy, delta;
	double	a=0, b=0, omega_0=0, f_0=0;
//	double    R,C;
	float	f_min, f_max, lR=(float)R;
	char		s[256];
	int		bool_plot_on_all_points=1.;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	if ( (index!=RESISTANCE_FIT_RC) && (index!=RESISTANCE_FIT_C) ) 
		return(win_printf_OK("bad call"));

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
    {	if (index==RESISTANCE_FIT_RC)
    		return win_printf_OK("This routine fits an amplitude (V/sqrt(Hz)) PSD\n"
					"and gives the RC value\n\n"
					"a new dataset is created.");
		else if (index==RESISTANCE_FIT_RC)
			return win_printf_OK("This routine fits an amplitude (V/sqrt(Hz)) PSD\n"
					"and gives the C value, knowing R\n\n"
					"a new dataset is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	if (op1->y_title==NULL)					return(win_printf_OK("bad plot (current)\n I need an amplitude PSD"));
	if (strcmp(op1->y_title,"V/\\sqrt{Hz}")!=0)		return(win_printf_OK("bad plot (current)\n I need an amplitude PSD"));
	
	ny = ds1->ny;	// number of points in time series

	f_min = op1->x_lo;	// visible plot limits in the X-direction
	f_max = op1->x_hi;
	N=0;				// N will ne the number of points to perform the fit over
	for (i=0; i<ny; i++)
	{	if ( (ds1->xd[i]>=f_min) && (ds1->xd[i]<=f_max) )
		{	N++;
		}
	}
	i_min=0;
	while (ds1->xd[i_min]<f_min) 	
	{	i_min++;
	}
	i_max=ny-1;
	while ( ds1->xd[i_max]>f_max )
	{	i_max = i_max-1; 
	}

	if (index==RESISTANCE_FIT_C)
	{	if (win_scanf("You want to fix the resistance\nWhat is it (Ohm)? %f", &lR)==CANCEL) return(D_O_K);
		R=(double)lR;
	}
	
	sprintf(s,"I will fit in compact interval [%g, %g], over %d points\n"
			"which indices are [%d, %d]\n\n"
			"%%R plot fit on visible points where it is performed\n"
			"%%r plot fit on all points of dataset", 
			f_min, f_max, N, i_min, i_max);
	i=win_scanf(s, &bool_plot_on_all_points);
	if (N!=(i_max-i_min+1)) return(win_printf_OK("pb with N"));
	
	sx=0; sx2=0; sy=0; sxy=0;
	for (i=i_min; i<=i_max; i++)
	{	f2    = (double)(4.*M_PI*M_PI)*(ds1->xd[i]*ds1->xd[i]); // pulsation au carr�
		Sp2   = (double)(ds1->yd[i]*ds1->yd[i]);
		sx  += (f2);
		sx2 += (f2*f2);
		sy  += (1. / Sp2);
		sxy += (f2 / Sp2);
	}
			
	if (index==RESISTANCE_FIT_RC)
	{
		delta = sx*sx - (double)N*sx2;	if (delta==0) return(win_printf("slope is infinite !"));
		a = (sy*sx  - (double)N*sxy)/delta;	// fit y=a*x+b
		b = (sx*sxy - sx2*sy)/delta;

		if (a<0)	return(win_printf("R is negative !!!"));
		if (b<0)	return(win_printf("C is negative !!!"));
		if (b<1e-30)	return(win_printf("RC is infinite !!!"));	
		if (a<1e-30)	return(win_printf("RC = 0 !!!"));	
		omega_0 = sqrt(b/a);
		f_0     = omega_0/(2.*M_PI);
		
		R = 1./(4.*k_b*Temperature*b);
		C = 1./(omega_0*R);

	} // end of fit of R and C
	else
	{	if (sx2==0) 	return(win_printf("C is infinite !!!"));	
		
		b = 1./(4*k_b*Temperature*R);
		a = (sxy - b*sx)/sx2;
		
		if (a<1e-30) 	return(win_printf("C is zero !!!"));	
		if (b<1e-30) 	return(win_printf("C is infinite !!!"));		
		
		omega_0	= sqrt(b/a);
		f_0     = omega_0/(2.*M_PI);

		C = 1./(omega_0*R);
	} // end of fit of C only

		
	
	if (bool_plot_on_all_points==1)
	{	i_min=0; 
		i_max=ny-1;	
		N = ny;
	}
	ds2 = create_and_attach_one_ds (op1, N, N, 0);

	for (i=i_min; i<=i_max; i++)
	{	ds2->xd[i-i_min] = ds1->xd[i];
		ds2->yd[i-i_min] = (float)sqrt((4.*k_b*Temperature*R)/(1. + ds1->xd[i]*ds1->xd[i]*1/f_0*1/f_0));
	}

	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"LP RC filter fit using [%f, %f]\n%s R=%g and C=%g", 
				ds1->xd[i_min], ds1->xd[i_max], (index==RESISTANCE_FIT_C) ? "imposing" : "", R, C);
	
	set_ds_plot_label(ds2, ds2->xd[(int)(i_min+i_max)/2], ds2->yd[(int)(i_min+i_max)/2], USR_COORD,
				"\\fbox{\\stack{{\\omega_0 = %2.3g/s  f_0 = %2.3g Hz}{\\tau = RC = %2.3gs}"
				"{\\sqrt{4kTR}=%2.3g V}{R = %2.3g\\Omega   C = %2.3gF}}}", 
				omega_0, f_0, 1/omega_0, 1/sqrt(b), R, C);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_fit_LP_RC





// Spectre passe-bas R et C connu
int do_plot_lorentzian_spectrum(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny;
	d_s 	*ds1, *ds2;
	pltreg *pr = NULL;
	int	index;
	double	omega_0;
	float	lC=(float)C, lR=(float)R;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;


	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine plots a lorentzien spectrum\n"
					"using frequencies from the x-axis of the current dataset\n\n"
					"a new dataset is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");

	ny = ds1->ny;	// number of points in time series

	i=win_scanf("What is the resistance (Ohm)? %8f\nWhat is the capacity (Farad)? %8f", &lR, &lC);
	if (i==CANCEL) return(D_O_K);
	R=(double)lR; C=(double)lC;
	omega_0 = 1/(R*C);

	ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
	for (i=0; i<ny; i++)
	{	ds2->xd[i] = ds1->xd[i];
		ds2->yd[i] = (float)sqrt((4*k_b*Temperature*R)/(1 + 4.*M_PI*M_PI*ds1->xd[i]*ds1->xd[i]/(omega_0*omega_0)));
	}

	set_ds_source(ds2,"Low-pass RC Lorentzian spectrum\nwith R=%g\\Omega and C=%gF", R, C);

	set_ds_plot_label(ds2, ds2->xd[ny/2], ds2->yd[ny/2], USR_COORD, "\\fbox{\\stack{{\\omega_0 = %2.3g/s  f_0 = %2.3gHz \\tau = RC = %2.3gs}"
				"{\\sqrt{4kTR}=%2.3gV}{R = %2.3g\\Omega   C = %2.3gF}}}", 
				omega_0, omega_0/(2*M_PI), 1/omega_0, sqrt(4*k_b*Temperature*R), R, C);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_plot_lorentzian_spectrum







#ifdef _RESPONSE_H_
int do_build_Lorentzian_LP_RC(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny;
	d_s 	*ds1;
	pltreg *pr = NULL;
	int	index;
	double	f_0=1/(float)(R*C);
	float	lC=(float)C, lR=(float)R;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the response of a low-pass RC circuit\n"
					"using frequencies from the x-axis of the current dataset\n\n"
					"no dataset is created, but the current response function is updated");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");

	ny = ds1->ny;	// number of points in time series
	
	i=0;
	if (win_scanf("%R input the cut-off-frequency\n%r input the values of R and C",&index)==CANCEL) 
		return(D_O_K);
	if (index==1)
	{	if (win_scanf("What is the resistance (Ohm)? %10f\nWhat is the capacity (Farad)? %10f", &lR, &lC)==CANCEL)
			return(D_O_K);
		R=(double)lR; C=(double)lC;
		f_0 = 1/(R*C);
	}
	else if (index==0)
	{	if (win_scanf("What is the cut-off frequency %10f", &f_0)==CANCEL)
			return(D_O_K);
	}
	else return(D_O_K);

	reset_response(current_response, ny, (ny-1)*2, ds1->xd[ny-1]*2);
	for (i=0; i<ny; i++)
	{	current_response->f[i] = ds1->xd[i];
		current_response->r[i][0] = 1/(1+ds1->xd[i]*ds1->xd[i]/(f_0*f_0));
		current_response->r[i][1] = (-ds1->xd[i]/f_0)/(1+ds1->xd[i]*ds1->xd[i]/(f_0*f_0));
	}
	current_response->mode = RESPONSE_PLOT_MODULUS;
	current_response->info = my_sprintf(current_response->info, 
			"Low-pass RC Response\nwith RC=%gs\nf_0=%gHz, \\omega_0=%g/s",
			R*C, f_0, 2*M_PI*f_0);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_plot_lorentzian_spectrum
#endif








//routine ecrite le 09/11/2005:do_compute_imposed_current_in_resistance

int do_resistance_compute_imposed_current(void)
{
	double  a=1.1e-4;
	double  b=1.805e-4;
	double  offset=1.797e-4;
	int i,k;
	int index;
	int		*files_index=NULL;		// all the files
	int i_file, n_files;
	O_p 	*op2=NULL, *op1=NULL;
	pltreg	*pr = NULL;
	static	char files_ind_string[128]	="0:1:9"; 
	static	char file_extension[8]		=".bin";
	static	char	root_filename[128]	     = "run03";
	long		header_length;
	static int	N_channels=3;
	long		N_total;
	static int N_decimate=10;
	static int	excitation_channel=1;
	int n_excitation;
	char		s[512];
	char		filename[256];
	float  lR=(float)R,la=(float)a,lb=(float)b,loffset=(float)offset;
	float float_mexcitation;
	float	*excitation;
	float f_acq;
	double  tmp;
	int		ret=0;
	d_s  *ds2=NULL;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes from some files the imposed current injected in a R//C circuit\n"
					"when the generator's tension is known."
					"a new plot is created.");
	}
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of files %%s"
			"range of files %%s"
			"file extension (note: expected files are from NI4472) %%s\n"
			"Number of channels in a file : %%2d\n"
			"Which channel do you want (generator's tension) : %%2d"
			"decimate data by a factor of %%3d\n\n");
	i=win_scanf(s, &root_filename, &files_ind_string, &file_extension, 
				&N_channels, &excitation_channel,&N_decimate );
				
	i=win_scanf("{\\pt14\\color{yellow} Param�tres de la calibration}\n"
				"What is the resistance (Ohm)? %12f\n"
				"<U_{400}/400> = a <U1> + b\n"
				"with a = %10f and b = %10f\n"
				"offset lors de la calibration : %12f\n",
				&lR, &la, &lb, &loffset);
	if (i==CANCEL) return(OFF);
	
	
	R=(double)lR;a = (double)la;b=(double)lb;offset=(double)offset;
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	
	if ((op2 = create_and_attach_one_plot(pr, n_files, n_files, 0))==NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	    
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];

		sprintf(filename, "%s%d%s", root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: looking U1", k+1, n_files, filename); spit(s);
		
		ret=read_header_NI4472(filename, &header_length, &N_total, &f_acq);
		if (ret==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
	    n_excitation=load_NI4472_data(filename, header_length, N_total, N_channels, excitation_channel, N_decimate, &excitation);
	
	    float_mexcitation = gsl_stats_float_mean(excitation, 1, n_excitation); 
	    free(excitation);
		
	    tmp = a * (double)float_mexcitation + b - offset;
	    tmp /= (double)R;
	    tmp *= (double)1e14;
	    ds2->yd[k] = (float)tmp;
		ds2->xd[k] = (float)k;
	}
	
	
	
	set_ds_dot_line(ds2);
	set_ds_point_symbol(ds2, "\\pt12\\di");
	set_plot_title(op2, "valeur moyenne de I");
	set_plot_x_title(op2, "File number");
	set_plot_y_title(op2, "<I>(1^{-14}A)");
	ds2->treatement = my_sprintf(ds2->treatement,"mean I calculated from U1 and calbration's parameters");


	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the compute_imposed_current_in_resistance



int do_resistance_compute_intensity(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny, n_out;
	d_s 	*ds1, *ds2=NULL;
	pltreg *pr = NULL;
	int	index;
	double	f_0;
	float	lC=(float)C, lR=(float)R;	
	double	dt;
	double	*u, *t, *It, *i_R;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	if ( (index!=DERIVATION_METHOD_FINITE_DIFFERENCES_ORDRE2) && (index!=DERIVATION_METHOD_SPLINE) && (index!=DERIVATION_METHOD_FOURIER) )
		return(win_printf_OK("bad call!"));

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the intensity of the current flowing in a resistor\n"
					"when the tension is known. It is assumed the resistor is in parallel\n"
					"with a capacitor.\n\n"
					"a new dataset is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->ny;	// number of points in time series 
	if (ny<4) 						return(win_printf_OK("not enough points !"));

	i=win_scanf("What is the resistance (Ohm)? %8f\nWhat is the capacity (Farad)? %8f", &lR, &lC);
	if (i==CANCEL) return(D_O_K);
	R=(double)lR; C=(double)lC;
	f_0 = 1/(R*C);

	dt = (double)(ds1->xd[ny-1]-ds1->xd[0])/(ny-1);
	if ( (ds1->xd[2]-ds1->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)				return(win_printf_OK("dt is zero !"));
	
	u  = (double*)calloc(ny, sizeof(double));
	t  = (double*)calloc(ny, sizeof(double));
	It = (double*)calloc(ny, sizeof(double));
	i_R= (double*)calloc(ny, sizeof(double));
	for (i=0; i<ny; i++)
	{	u[i] = (double)ds1->yd[i];
		t[i] = (double)ds1->xd[i];
		It[i]= (double)I;
	}	
	n_out = resistance_compute_i_R(u, ny, t, C, It, dt, i_R, index);
	
	ds2 = create_and_attach_one_ds (op1, ny-2, ny-2, 0);
	memcpy(ds2->xd, ds1->xd+1, (ny-2)*sizeof(float)); 
	for (i=0; i<ny-2; i++)
	{	ds2->yd[i] = (float)-R*C*i_R[i+1];
	}

	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"R.i_R from U (%s) ", 
			(index==DERIVATION_METHOD_FINITE_DIFFERENCES_ORDRE2) ? "finite differences (o2)" :
			(index==DERIVATION_METHOD_SPLINE) 	      ? "splines" :
									"Fourier");
	free(u);	free(t);
	free(It);	free(i_R);
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_compute_intensity_in_resistance





int do_resistance_integrate_equation(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny;
	d_s 	*ds1, *ds2=NULL;
	pltreg *pr = NULL;
	int	index;
	double	f_0, omega;
	float	lC=(float)C, lR=(float)R;
static float lRg=4.4e10, lCg=1e-12, lip=14e-14, lR8=10000, lC8=0.;	
	double	dt, factor;
	double	*U1=NULL, *is=NULL;
	float	f_acq;
	fftw_complex 	*ft_U1=NULL, *ft_is=NULL;
	fftw_plan 	plan;
	int	n_fft;
static	int 	bool_hanning=0, bool_is=0, bool_multiply_is_by_R=1, bool_multiply_U=1;
static	float ampli_factor = 400;
	fftw_complex Z, Zg, Z8, tmp_1, tmp_2, tmp_3, tmp_4, tmp_5;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine integrates the electrokinetic equations in Fourier space\n"
					"and computes U for a given profile of U_1.\n\n"
					"U_1 is read from a dataset.\n"
					"a new dataset is created for U.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->ny;	// number of points in time series 
	if (ny<4) 						return(win_printf_OK("not enough points !"));

	i=win_scanf("{\\pt14\\color{yellow}Input values}\n\n"
	 			"resistance R                 %8f \\Omega\ncapacity C                    %8f F\n"
				"resistance R_g                %8f \\Omega\nassociated capacity C_g %8f F\n"
				"resistance R_8                %8f \\Omega\nassociated capacity C_8 %8f F\n"
				"polarisation current       %8f A.\n\n"
				"%b Hanning window\n%b output i_s  %b multiply it by R\n%b multiply by factor %7f", 
				&lR, &lC, &lRg, &lCg, &lR8, &lC8, &lip, 
				&bool_hanning, &bool_is, &bool_multiply_is_by_R, &bool_multiply_U, &ampli_factor);
	if (i==CANCEL) return(D_O_K);
	R=(double)lR; C=(double)lC;
	f_0 = 1/(R*C);

	dt = (double)(ds1->xd[ny-1]-ds1->xd[0])/(ny-1);
	if ( (ds1->xd[2]-ds1->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)						return(win_printf_OK("dt is zero !"));

	n_fft=ny;
	f_acq=1/(ds1->xd[1]-ds1->xd[0]);

	U1    = fftw_malloc((ny)*sizeof(double));		// to store initial data
    	ft_U1 = fftw_malloc((ny/2+1)   *sizeof(fftw_complex));
    	is    = fftw_malloc((ny)*sizeof(double));
    	ft_is = fftw_malloc((ny/2+1)   *sizeof(fftw_complex));
	
	for (i=0; i<ny; i++) U1[i]=(double)ds1->yd[i];
	for (i=0; i<ny; i++) is[i]=(double)lip;			// now is contains ip
	
	if (bool_hanning==1) fft_window_real(ny, U1, 0.001);

	plan = fftw_plan_dft_r2c_1d(ny, U1, ft_U1, FFTW_ESTIMATE);
	fftw_execute(plan); 
	fftw_destroy_plan(plan);

	plan = fftw_plan_dft_r2c_1d(ny, is, ft_is, FFTW_ESTIMATE); // now ft_is contains the FT of ip
	fftw_execute(plan); 
	fftw_destroy_plan(plan);
	
	for (i=0; i<(ny+1)/2; i++)	// the fft has ny/2+1 complex points // loop was initially with (ny+1)/2
     {	omega = (i*f_acq/n_fft*2*M_PI); // pulsation 
     	
     	// test impedance:
     	Z[0]  =      lR            /(1. + lR*lR*lC*lC*omega*omega);
     	Z[1]  =   -lR*lR*lC*omega  /(1. + lR*lR*lC*lC*omega*omega);
     	// current generator impedance:
     	Zg[0] =      lRg           /(1. + lRg*lRg*lCg*lCg*omega*omega);
     	Zg[1] = -lRg*lRg*lCg*omega /(1. + lRg*lRg*lCg*lCg*omega*omega);
     	// another impedance (tension divisor)
     	Z8[0] =      lR8           /(1. + lR8*lR8*lC8*lC8*omega*omega);
     	Z8[1] = -lR8*lR8*lC8*omega /(1. + lR8*lR8*lC8*lC8*omega*omega);
     	// another impedance : 2Zg + Z8 :
     	tmp_1[0] = 2*Zg[0] + Z8[0];
     	tmp_1[1] = 2*Zg[1] + Z8[1];
     	// another impedance : 2Zg + Z8 + 2Z:
     	tmp_2[0] = tmp_1[0] + 2*Z[0];
     	tmp_2[1] = tmp_1[1] + 2*Z[1];
     	
     	tmp_3[0] = ft_U1[i][0];
     	tmp_3[1] = ft_U1[i][1];
     	complex_multiply(tmp_1, ft_is[i], tmp_5);
     	complex_multiply(Z,     tmp_3, tmp_4);
     	tmp_4[0] = tmp_4[0] + tmp_5[0];
     	tmp_4[1] = tmp_4[1] + tmp_5[1];
     	complex_divide  (tmp_4, tmp_2, tmp_3);  // now, tmp_3 contains the FT of U     	
     	
		if (bool_is==1)
		{	complex_divide(tmp_3, Z, tmp_4); // there, tmp_4 contains I = ip + is
     		tmp_4[0] -= ft_is[i][0];
     		tmp_4[1] -= ft_is[i][1];
     		
     		ft_is[i][0] = tmp_4[0]; // here is is
     		ft_is[i][1] = tmp_4[1];
		}
		
		ft_U1[i][0] = tmp_3[0];
		ft_U1[i][1] = tmp_3[1]; 
		  
	}
	if (ny%2==0) // the Nyquist frequency is represented by a mode which is purely real (like mode 0)
	{ 	ft_U1[ny/2][0]=0;  
		ft_U1[ny/2][1]=0;
		
		if (bool_is==1)
		{	ft_is[ny/2][0]=0;  
			ft_is[ny/2][1]=0;
		}
	}

		// we Fourier-transform back:
	{	plan = fftw_plan_dft_c2r_1d(ny, ft_U1, U1, FFTW_ESTIMATE);   
		fftw_execute(plan); 
		fftw_destroy_plan(plan);

		ds2 = create_and_attach_one_ds (op1, ny, ny, 0);

		factor = (double)(1./ny);
		if (bool_multiply_U==1) factor *= (double)ampli_factor;
		
		if (bool_hanning==1) fft_unwindow_real(ny, U1, 0.001);
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = ds1->xd[i];
			ds2->yd[i] = (float)(U1[i]*factor);
		}
		
		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"U integrated from U1");
		if (bool_multiply_U==1) ds2->treatement = my_sprintf(ds2->treatement,"multiplied by %f", ampli_factor);
	}

	// if we also want i_s, we Fourier transfrom it back too:
	if (bool_is==1)
	{	plan = fftw_plan_dft_c2r_1d(ny, ft_is, is, FFTW_ESTIMATE);   
		fftw_execute(plan); 
		fftw_destroy_plan(plan);

		ds2 = create_and_attach_one_ds (op1, ny, ny, 0);

		factor = 1./ny;
		if (bool_multiply_U==1) factor *= (double)ampli_factor;
		if (bool_multiply_is_by_R==1) factor *= (double)lR;

		if (bool_hanning==1) fft_unwindow_real(ny, is, 0.001);		
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = ds1->xd[i];
			ds2->yd[i] = (float)(is[i]*factor);
		}
		
		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"i_s integrated from U1");
	}

	fftw_free(U1); fftw_free(ft_U1);
	fftw_free(is); fftw_free(ft_is);
		
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_integrate_equation function




int do_hist_periodic_build_from_NI4472(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memory 
register int	i2, j, j2;
	O_p 		*op2, *op1=NULL;
static int	n_bin=81;
	int		n_power, n_excitation, n_cycles, n_cycles_total=0;
	gsl_histogram *hist;
	float	a_tau;
static float	h_min=-1, h_max=1;
	d_s 		*ds2;
	pltreg	*pr = NULL;
	int		tau, i_file, n_tau, n_files;	// current tau, current file number, number of taus, number of files
	int		*tau_index=NULL;		// all the taus
	int		*files_index=NULL;		// all the files
	int		index;
static	char	tau_string[128]		="100:100:200";
static	char files_ind_string[128]	="0:1:2"; 
static	char file_extension[8]		=".bin";
static	char	root_filename[128]	     = "run02";
	long		N_total;
static int 	N_decimate=10;
	long		header_length;
static int	N_channels=3, excitation_channel=1, response_channel=3;
static float	excitation_factor=1., response_factor=14000;
	char		s[512];
	char		filename[256];
	int		*ind=NULL, *ind2=NULL; // indices
	int		n_ind1, n_ind2;
	float	*excitation, *power;
	float	y_min=0, y_max=1;
static float	epsilon=0.001;		// precision for the dectection of ramps
static float	data_min, data_max;	// for detection of ramps
	float	f_acq, dt;
static float	start_time=0.1, cycle_period=0.5;
static int	bool_remove_bounds=1;
static int	bool_impose_indices=0;
static int	bool_report=0;
	int		bool_normalize_u=1;
static int	plateau_position=0;
static int	point_position=1;
	int		ret=0;
	double	*u1, *U, *I, *i_R, *t;
static float	U1_I_factor=1./(5e10), U1_offset=0., i_p=(1e-14);
	double	mean_U, var_U, mean_I, var_I, mean_i_R, var_i_R, mean_W, var_W, mean_Q, var_Q;
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine builds a set of histograms\n"
				"out of a set of files, and places it in a new plot\n\n"
				"data a(t) from file 1 is first averaged using a sliding average:\n\n"
				"a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt'\n\n"
				"different a_\\tau(t_i) are used with t_i from indices found in file A\n\n"
				"This is a batch, written for the study of fluctuations in a resistor\n"
				"and it should be ran with caution!");
	}
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of files %%s"
			"range of files %%s"
			"file extension (note: expected files are from NI4472) %%s\n"
			"Number of channels in a file : %%2d\n"
			"channel with excitation : %%2d    divide by factor %%8f\n"
			"channel with response  : %%2d    divide by factor %%8f\n"
			"decimate data by a factor of %%3d\n\n"
			"%%b print to screen a report for each file");
	i=win_scanf(s, &root_filename, &files_ind_string, &file_extension, 
				&N_channels, &excitation_channel, &excitation_factor, &response_channel, &response_factor,
				&N_decimate, &bool_report);
	if (i==CANCEL) return(OFF);
	if (excitation_factor*response_factor<1e-50) return(win_printf_OK("factors are too small (vanishing ?)"));
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));

	i=win_scanf("How to find the points t_i where to start from ?\n"
			"%R first points\n"
			"%r middle points\n"
			"%r last points\n"
			"of\n"
			"%R lower plateau\n"
			"%r upper plateau\n"
			"using precision %8f\n\n"
			"%b forget this rule, and impose the indices\n"
			"%b remove first and last found indices", 
			&point_position, &plateau_position, &epsilon, &bool_impose_indices, &bool_remove_bounds);
	if (i==CANCEL) return(OFF);
	
	if (bool_impose_indices==1)
	{ sprintf(s, "Imposing indices\n"
			"cycle period (in seconds) : %%10f\n"
			"starting time (in seconds) : %%10f\n\n");
	i=win_scanf(s, &cycle_period, &start_time);
	if (i==CANCEL) return(OFF);
	}
			
	sprintf(s,"How should I process the data ?\n\n"
			"compute I=i_c+i_p from excitation U_1 according to\n"
			"I(t) = G*(U_1(t) - U_0)  + i_p\n"
			"U_0 = %%10f\n G = %%10f\ni_p = %%10f");
	i=win_scanf(s, &U1_offset, &U1_I_factor, &i_p); 
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("number of bins in histogram? %d min %f max %f\n"
			"using averaged data {\\pt16 a_\\tau(t) = {1 \\over \\tau} \\int_t_i^{t_i+\\tau} a(t') dt'}\n\n"
			"\\tau (integer, nb of pts) ? %s"
			"Integration in time starts at times t_i found with previous rule.",
			&n_bin, &h_min, &h_max, &tau_string);
	if (i==CANCEL) return(OFF);
	
	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	return(win_printf("bad values for \\tau !"));
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	if ((op2 = create_and_attach_one_plot(pr, n_bin, n_bin, 0))==NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];

		sprintf(filename, "%s%d%s", root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);

		ret=read_header_NI4472(filename, &header_length, &N_total, &f_acq);
		if (ret==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
		n_excitation=load_NI4472_data(filename, header_length, N_total, N_channels, excitation_channel, N_decimate, &excitation);				
		n_power     =load_NI4472_data(filename, header_length, N_total, N_channels, response_channel,   N_decimate, &power);
		dt=(float)N_decimate/f_acq;
		
		sprintf(s, "%d/%d file %s: converting data", k+1, n_files, filename); spit(s);
				
		if ((n_excitation!=n_power) || ((N_total/(N_channels*N_decimate))!=n_excitation)) 
			return(win_printf_OK("error loading data\nincorrect number of points"));
		u1 = (double*)calloc(n_excitation, sizeof(double));
		U  = (double*)calloc(n_excitation, sizeof(double));
		for (i=0; i<n_excitation; i++)
		{	u1[i]	= (double)(excitation[i]/excitation_factor);
			U[i]  	= (double)(power[i]/response_factor);
		}
			
		sprintf(s, "%d/%d file %s: searching cycles", k+1, n_files, filename); spit(s);

		if (bool_impose_indices==1)
		{	ind=impose_indices(dt, cycle_period, start_time, n_excitation, &n_ind2);
		}
		else
		{
		if (k==0)	// first file: we autodetect some parameters		
		{	win_scanf("Precision \\epsilon ? %8f\n(depends on the noise level on the plateaus)", &epsilon);
			data_min=data_max=excitation[0];
			for (i=1; i<n_excitation-1; i++)
			{	if (excitation[i]<data_min) data_min=excitation[i];
				if (excitation[i]>data_max) data_max=excitation[i];
			}
			if (plateau_position==0)
			{ y_min = data_min-epsilon;
		  	  y_max = data_min+epsilon;
		  	}
		  	if (plateau_position==1)
			{ y_min = data_max-epsilon;
		  	  y_max = data_max+epsilon;
		  	}
		}
		
		// refine the search for indices:
		ind   = find_interval_float (excitation, n_excitation-1, y_min, y_max, &n_ind1);
		if (point_position==0)	ind2  = find_and_keep_firsts_int(ind, n_ind1, &n_ind2);
		if (point_position==1)	ind2  = find_and_keep_middles_int(ind, n_ind1, &n_ind2);
		if (point_position==2)	ind2  = find_and_keep_lasts_int(ind, n_ind1, &n_ind2);
		free(ind);
		ind=ind2;
		}
	
		// eventually remove the first and last indices:
		if (bool_remove_bounds==1)
		{	n_ind2 -= 2;  // there will be 2 points less : the first and the last are removed.
			for (i=0; i<n_ind2; i++)		ind[i]=ind[i+1];
		}
		
		if (n_ind2<=0) return(win_printf_OK("file %s\nNo points left. Found indices were only at the boundaries...",filename));
	
		
		// count the number of period (ie, number of cycles), and perform some tests:
		n_cycles = n_ind2;
		n_cycles_total += n_cycles;
		if (n_cycles<1) 	return(win_printf_OK("Mmm... not enough ramps in file %s", filename));
		
		if ((ind[n_cycles-1]+tau_index[n_tau-1])>=n_power)	
					return(win_printf_OK("filen %s\nlarger tau %d is too large!\nlast index is %d\n%d points in dataset", 
							filename, tau_index[n_tau-1], ind[n_cycles-1], n_power));
					
		// eventually print some information on the file:					
		if (bool_report==1)	win_printf_OK("file %s (file %d/%d)\n\n"
							"%d channels, %d points, %d points per channel\n"
							"decimation by %d, %d points kept\n\n"
							"acquisition frequency %g\n\n"
							"%d usable cycles found", 
							filename, k+1, n_files, N_channels, N_total, n_excitation*N_decimate, 
							N_decimate, n_excitation, f_acq, n_cycles);
		
		sprintf(s,"%d/%d file %s: computing quantities", k+1, n_files, filename); spit(s);

		// compute the interesting quantity out of the data read from the file:
		t  =(double*)calloc((size_t)n_excitation, sizeof(double));
		I  =(double*)calloc((size_t)n_excitation, sizeof(double));
		i_R=(double*)calloc((size_t)n_excitation, sizeof(double));
			mean_U = gsl_stats_mean(U, 1, n_excitation);
			var_U  = gsl_stats_sd_m(U, 1, n_excitation, mean_U);
		for (i=0; i<n_excitation; i++) t[i] = (double)i*dt;	
		resistance_compute_I   (u1, n_excitation, (double)U1_I_factor, (double)U1_offset, (double)i_p, I);
			mean_I = gsl_stats_mean(I, 1, n_excitation);
			var_I  = gsl_stats_sd_m(I, 1, n_excitation, mean_I);
		resistance_compute_i_R (U, n_excitation, t, get_C(), I, dt, i_R, DERIVATION_METHOD_SPLINE);
			mean_i_R = gsl_stats_mean(i_R+1, 1, n_excitation-1);
			var_i_R  = gsl_stats_sd_m(i_R+1, 1, n_excitation-1, mean_i_R);
		resistance_compute_power(U, n_excitation, get_R(), I, get_I(),  dt, bool_normalize_u, u1);	// computes the work (power), puts it in u1
			mean_W = gsl_stats_mean(u1+1, 1, n_excitation-1);
			var_W  = gsl_stats_sd_m(u1+1, 1, n_excitation-1, mean_W);
		resistance_compute_power(U+1, n_excitation-2, get_R(), i_R, get_I(), dt, bool_normalize_u, U+1);	// computes the heat (power), puts it in U
			mean_Q = gsl_stats_mean(U+1, 1, n_excitation-2);
			var_Q  = gsl_stats_sd_m(U+1, 1, n_excitation-2, mean_Q);
		for (i=0; i<n_excitation; i++)
		{	power[i] = (float)(u1[i]);
		}

		if (bool_report==1)	win_printf("Assuming R=%6g \\Omega, C=%6g F and i_p=%6g A\n\n"
				"mean U is %6g V, with variance %6g V\n"
				"mean I is %6g A, with variance %6g A\n"
				"mean i_R is %6g A, with variance %6g A\n\n"
				"mean W is %6g k_BT, variance is %6g k_BT\n"
				"mean Q is %6g k_BT, variance is %6g k_BT\n"
				"for time lags dt = %g s (facq = %g Hz)",
				get_R(), get_C(), i_p,  
				mean_U, var_U, mean_I, var_I, mean_i_R, var_i_R, mean_W, var_W, mean_Q, var_Q, dt, f_acq/N_decimate);

		sprintf(s, "%d/%d file %s: cumulating histograms", k+1, n_files, filename); spit(s);
									
		// here, we cumulate the data and build the histograms:
		for (i=0; i<n_tau; i++)
		{	tau=tau_index[i];

			if (k==0) // first file : we create the datasets
			{	if (i!=0) // not the first tau: we add a dataset
				{	if ((ds2 = create_and_attach_one_ds(op2, n_bin, n_bin, 0)) == NULL)	
						return win_printf_OK("cannot create dataset !");
				}
				ds2->treatement = my_sprintf(ds2->treatement,"histogram, data averaged over \\tau=%d points", tau);
						
				hist = gsl_histogram_alloc(n_bin);
				gsl_histogram_set_ranges_uniform (hist, (double)h_min, (double)h_max);
			}
			else 
			{	ds2=op2->dat[i];
				ds_to_histogram(op2->dat[i], &hist); // hist will be allocated perfectly
			}

			for (i2=0; i2<n_cycles; i2++)
			{	j=ind[i2];
				a_tau=0;
				for (j2=0; j2<tau; j2++)
				{	a_tau+=power[(j+j2)];		// average over tau points
				}
				gsl_histogram_increment(hist, (double)(a_tau/tau)); // histogram of averaged data
			}

			histogram_to_ds(hist, op2->dat[i], n_bin);
			gsl_histogram_free(hist);
	
		} //end of loop over i, ie, loop over tau

		free(excitation);
		free(power);
		free(ind);
		free(I); free(U); free(t);
		free(i_R); free(u1);
		
	} //end of the loop over k, ie, loop over the files;
	free(tau_index);
	free(files_index);
	
	set_plot_title(op2,"histograms");
	set_plot_x_title(op2, "x (%d cycles from %d files)", n_cycles_total, n_files);
	set_plot_y_title(op2, "pdf");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

    	op2->iopt |= YLOG;

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_hist_periodic_build_from_NI4472' */




//fonction cr�ee le 15/11/2005
int do_adjust_offset_NI4472(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memory 
	float  lR=(float)R;
	float  lI=(float)I;
	int err;
	double mean_tmp;
	float	*excitation;
	int n_excitation;
	float G;
	double  *tmp;
	O_p 		*op1=NULL;
	pltreg	*pr = NULL;
	int		index, ret, i_file, n_files;	// current tau, current file number, number of taus, number of files
	int		*files_index=NULL;		// all the files
static	char files_ind_string[128]	="0:1:2"; 
static	char file_extension[8]		=".bin";
static	char	root_filename[128]	     = "run02";
	long	N_total;
static int 	N_decimate=10;
	long		header_length;
static int	N_channels=3;
static int  excitation_channel = 3;
	char		s[512];
	float	f_acq;
	char		filename_in[256], filename_out[256];
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine loads a set of files\n"
				"and saves them after suppress offset and adjust I\n\n");
	}
	
	G=14000;
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of files %%s"
			"range of files %%s"
			"file extension (note: expected files are from NI4472) %%s\n"
			"Number of channels in a file : %%2d\n"
			"Which channel do you want (generator's tension) : %%2d"
			"decimate data by a factor of %%3d\n\n");
	i=win_scanf(s, &root_filename, &files_ind_string, &file_extension, 
				&N_channels, &excitation_channel,&N_decimate );
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("{\\color{yellow}\\pt14 Intensity}\n\n"
			"What is the resistance (Ohm)? %12f\n"
			"What is the imposed current (A)? %12f\n"
			"What is the gain ? %12f\n\n",
			&lR, &lI, &G);
	if (i==CANCEL) return(D_O_K);
	R=(double)lR; I=(double)lI;
	
		
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];

		sprintf(filename_in, "%s%d%s", root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: converting", k+1, n_files, filename_in); spit(s);
		sprintf(filename_out, "%s%d%s%s", root_filename, i_file, "_U", file_extension);
		
		ret=read_header_NI4472(filename_in, &header_length, &N_total, &f_acq);
		if (ret==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename_in));
		n_excitation=load_NI4472_data(filename_in, header_length, N_total, N_channels, excitation_channel, N_decimate, &excitation);
		tmp  = (double*)calloc(n_excitation, sizeof(double));
		for (i=0;i<n_excitation; i++)
		{ 		tmp[i] = (double) excitation[i];
		}
		
		mean_tmp = gsl_stats_mean(tmp, 1, n_excitation);
		
		for (i=0;i<n_excitation; i++)
		{ 		tmp[i] -= mean_tmp ;
				tmp[i] /= (double) G;
				tmp[i] += R*I;
		}
		for (i=0;i<n_excitation; i++)
		{ 		excitation[i] = (float) tmp[i];
		}
		err=save_data_bin(filename_out, excitation, n_excitation);
		free(tmp);
		free(excitation);
	} //end of the loop over k, ie, loop over the files;
	free(files_index);

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_adjust_offset_NI4472' */



void spit(char *message)
{	size_t	l_max=55;
	char 	white_string[64]="                                              ";
	
	if (strlen(message)<l_max) 	strncat (message, white_string, l_max-strlen(message));
	textout_ex(screen, font, message, 40, 40, makecol(100,155,155), makecol(2,1,1));
	return;
}








MENU *resistance_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"fit RC from PSD", 			do_fit_LP_RC,			NULL, RESISTANCE_FIT_RC,	NULL);	
	add_item_to_menu(mn,"fit C only from PSD",	 	do_fit_LP_RC,			NULL, RESISTANCE_FIT_C, 	NULL);	
	add_item_to_menu(mn,"plot RC spectrum",			do_plot_lorentzian_spectrum,		NULL, 0, NULL);	
	add_item_to_menu(mn,"view / change parameters",	do_set_resistance_parameters,		NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,						NULL, 0, NULL);	
	add_item_to_menu(mn,"compute mean I (from U1)",	do_resistance_compute_imposed_current,	NULL, 0, NULL);	
	add_item_to_menu(mn,"compute i_R (finite differences)",	do_resistance_compute_intensity,	NULL, DERIVATION_METHOD_FINITE_DIFFERENCES_ORDRE2, NULL);	
	add_item_to_menu(mn,"compute i_R (spline functions)",	do_resistance_compute_intensity,	NULL, DERIVATION_METHOD_SPLINE, NULL);	
	add_item_to_menu(mn,"compute i_R (Fourier)",	do_resistance_compute_intensity,	NULL, DERIVATION_METHOD_FOURIER, NULL);	
	add_item_to_menu(mn,"\0",						0,						NULL, 0, NULL);	
	add_item_to_menu(mn,"compute work W",			do_resistance_compute_work,		NULL, 0, NULL);	
	add_item_to_menu(mn,"compute heat Q",			do_resistance_compute_heat,		NULL, DERIVATION_METHOD_FOURIER, NULL);	
	add_item_to_menu(mn,"compute heat E",			do_resistance_compute_energy_variation,	NULL, RESISTANCE_ENERGY, NULL);	
	add_item_to_menu(mn,"compute heat Delta E",		do_resistance_compute_energy_variation,	NULL, RESISTANCE_ENERGY_VARIATION, NULL);	
	add_item_to_menu(mn,"compute heat Q Gibbs",		do_resistance_compute_heat_Gibbs, 	NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,						NULL, 0, NULL);	
	add_item_to_menu(mn,"compute U from U1",		do_resistance_integrate_equation,		NULL, 0, NULL);	
	add_item_to_menu(mn,"batch on NI4472 files (U)",do_hist_periodic_build_from_NI4472,	NULL, 0, NULL);
	add_item_to_menu(mn,"suppress offset and adjust I on NI4472 files (U)",do_adjust_offset_NI4472,	NULL, 0, NULL);
#ifdef _RESPONSE_H_
	add_item_to_menu(mn,"\0",						0,						NULL, 0, NULL);	
	add_item_to_menu(mn,"build Response Function",		do_build_Lorentzian_LP_RC,		NULL, 0, NULL);	
#endif		

	return mn;
}

int	resistance_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("resistance", NULL, resistance_plot_menu(), 0, NULL);
	return D_O_K;
}


int	resistance_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"resistance",	NULL, NULL);
	return D_O_K;
}
#endif

