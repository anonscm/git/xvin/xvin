#ifndef _RESISTANCE_MATH_C_
#define _RESISTANCE_MATH_C_

#include "allegro.h"
#include "xvin.h"
// #include "xv_tools_lib.h"

#include <gsl/gsl_errno.h> // for error code returning
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_statistics.h>
#include <fftw3.h>

/* If you include other plug-ins header do it here*/ 
// #include "../hist/hist.h"
#include "../diff/diff.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "resistance.h"
#include "resistance_math.h"


void complex_multiply(fftw_complex a, fftw_complex b, fftw_complex c)
{	
	c[0] = a[0]*b[0] - a[1]*b[1];
	c[1] = a[0]*b[1] + a[1]*b[0];
	
	return;
}

void complex_divide(fftw_complex a, fftw_complex b, fftw_complex c)
{	double modulus_squared;

	modulus_squared = b[0]*b[0] + b[1]*b[1];
	
	c[0] = ( a[0]*b[0] + a[1]*b[1])/modulus_squared;
	c[1] = (-a[0]*b[1] + a[1]*b[0])/modulus_squared;
	
	return;
}



int resistance_normalize_u(double *u, int ny, double R, double I)
{   double mean_u;
    register int i;
    
    mean_u    = gsl_stats_mean(u, 1, ny);
	for (i=0; i<ny; i++)
	{	u[i] = u[i] - mean_u + R*I;
	}
	
    return(0);	
}




int resistance_compute_I(double *excitation, int ny, double factor, double offset, double i_p, double *I)
{	register int i;
	
	for (i=0; i<ny; i++)
	{	I[i] = (excitation[i]-offset)*factor + i_p;
	}

	return(0);	
}


// power, in kT units
int resistance_compute_power(double *u, int ny, double R_local, double *It, double I, double dt, int bool_normalize_u, double *w)
{	register int i;

	if (bool_normalize_u==1) resistance_normalize_u(u, ny, R_local, I);

	for (i=0; i<ny; i++)
	{	w[i] = u[i]*It[i]*dt/kT;
	}

	return(0);	
}


int resistance_compute_du_dt(double *u, int ny, double *t, double dt, double *du_dt, int derivate_mode)
{   int bool_hanning=0;

    if (derivate_mode==DERIVATION_METHOD_FINITE_DIFFERENCES_ORDRE2) // ny-2 points left
    {	return(diff_2o(u, ny, dt, du_dt)); // double version
    }
    if (derivate_mode==DERIVATION_METHOD_FOURIER) // ny points
	{	return(diff_fourier(u, ny, dt, du_dt, bool_hanning)); // double version
	}
	if (derivate_mode==DERIVATION_METHOD_SPLINE)
	{   return(diff_splines(t, u, ny, du_dt)); // double version
	}
	
	return(0);	
}// end of compute_du_dt()


int resistance_compute_i_R(double *u, int ny, double *t, double C_local, double *I, double dt, double *i_R, int derivate_mode)
{	register int i;
	int n_out;

	n_out = resistance_compute_du_dt(u, ny, t, dt, i_R, derivate_mode);
	for (i=1; i<ny-1; i++)	i_R[i] = I[i] - i_R[i]*C_local;
	
	return(n_out);	
}// end of compute_i_R()



/* function resistance_compute_Gibbs_entropy_inc */
/* computes increment of trajectory-dependent entropy between time i and time i+1 */
/* 2007/10/15 */
/* the quantity computed here is an entropy (an increment of entropy) */
/* it is not an entropy production (you have to divide by dt for that */
int resistance_compute_Gibbs_entropy_inc(double *u, int ny, gsl_histogram *hist, double *dS)
{	double  p, p2; // proba      
	register int i, n_errors=0, n_zeros=0;
    	unsigned int j;
	
	// hist must be normalized.
	// following function in plug'in "hist" does the job:
    	// int normalize_histogram_Y(gsl_histogram *hist);
	// invoke it before calling the present function
    
	// first element:
    	if (gsl_histogram_find(hist, u[0], &j)==GSL_SUCCESS) // then u[i] was found at position j
	         p = gsl_histogram_get( hist, j);
    	else { p =(double)0.0;
		n_errors++; }
        
	// then increments:
	for (i=1; i<ny; i++)
    	{	p2 = p; // probability of preceeding event
    
        	if (gsl_histogram_find(hist, u[i], &j)==GSL_SUCCESS) // then u[i] was found at position j
	        	p = gsl_histogram_get( hist, j);
        	else { 	p =(double)0.0;
			n_errors++;  
		     }
        
        	if ( (p*p2)!= 0)
        	{	dS[i-1] = (double)k_b*(log(p2)-log(p));
		}
        	else 	
		{ 	dS[i-1] = (double)0.0; 
			n_zeros++;   
		}
	}
	if (n_errors>0) win_printf("%d data points were out of the pdf.\n"
			"Try to enlarge the range.", n_errors);
	if (n_zeros>0)  win_printf("%d points with zero probability.\n"
			"Try to reduce the number of bins.", n_zeros);

	return(0);	
}// resistance_compute_Gibbs_entropy_inc




int resistance_compute_Gibbs_heat_power(double *u, int ny, gsl_histogram *hist, double dt, double *Q_Gibbs)
{   register int i;
                                          
    resistance_compute_Gibbs_entropy_inc(u, ny, hist, Q_Gibbs);
    for (i=0; i<ny; i++) Q_Gibbs[i] *= (double)Temperature/dt;    
	
	/* division by dt is to have an entropy production  */
	/* multiplication by temperature then gives a power */
    
    return(0);
}




int resistance_compute_irrev_heat_power(double *Q, double *Q_Gibbs, int ny, double *Q_irrev)
{   register int i;
                                          
    for (i=0; i<ny; i++) Q_irrev[i] = Q[i] - Q_Gibbs[i];    
    
    return(0);
}



double get_R(void)
{ return(R);
}

double get_C(void)
{ return(C);
}

double get_I(void)
{ return(I);
}


#endif
