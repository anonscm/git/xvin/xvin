#ifndef _RESISTANCE_ENERGIES_C_
#define _RESISTANCE_ENERGIES_C_

#include "allegro.h"
#include "xvin.h"
// #include "xv_tools_lib.h"

#include <gsl/gsl_errno.h> // for error code returning
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_statistics.h>
#include <fftw3.h>

/* If you include other plug-ins header do it here */#include "../hist/hist.h"
#include "../diff/diff.h"
#include "../inout/inout.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "resistance.h"
#include "resistance_energies.h"
#include "resistance_math.h"



int do_resistance_compute_heat(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny;
	d_s 	*ds1, *ds2=NULL;
	pltreg *pr = NULL;
	int	index;
	double	f_0;
	float	lC=(float)C, lR=(float)R, lI=(float)I;	
	double	dt=1;
	double	*Q, *u, *ir;
	float	f_acq;
	double	*t=NULL, *It;
static int 	bool_normalize_u=1, bool_save_u=0, bool_save_i=0, bool_kT =1, bool_deriv=1;
	double	mean_u, mean_i, mean_Q, var_i, var_u, var_Q;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the heat dissipated in a resistor\n"
					"when the tension is known. It is assumed the resistor is in parallel\n"
					"with a capacitor.\n\n"
					"a new dataset is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->ny;	// number of points in time series 
	if (ny<4) 						return(win_printf_OK("not enough points !"));

	i=win_scanf("{\\color{yellow}\\pt14 Heat dissipated in R//C}\n\n"
            "Q_\tau(t) = \\int_t^{t+\\tau} U.i_R.dt'\n\n"
			"What is the resistance (Ohm)? %12f\nWhat is the capacity (Farad)? %12f\n"	
			"What is the imposed current (A)? %12f\n\n"
			"derivatives are computed with %R f.d.o2 %r splines %r Fourier\n"
		    "%b normalize U ( U-<U>+RI ) and then %b update U (old dataset)\n"
			"%b plot intensity (new dataset)\n"
			"%b heat divided by kT",
			&lR, &lC, &lI, &bool_deriv, &bool_normalize_u, &bool_save_u, &bool_save_i, &bool_kT);
	if (i==CANCEL) return(D_O_K);
	R=(double)lR; C=(double)lC; I=(double)lI;
	f_0 = 1/(R*C);

	dt = (double)(ds1->xd[ny-1]-ds1->xd[0])/(ny-1);
	if ( (ds1->xd[2]-ds1->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)				return(win_printf_OK("dt is zero !"));

	f_acq=(float)(1./dt);

	u  = fftw_malloc((ny)*sizeof(double));		// to store u
    t  = fftw_malloc((ny)*sizeof(double));		// to store t
    It = fftw_malloc((ny)*sizeof(double));		// to store I (we generalize the code for a time dependent I)
	for (i=0; i<ny; i++)	
    {   u [i] =(double)ds1->yd[i];
	    t [i] =(double)ds1->xd[i];
	    It[i] =(double)I;
    }

	if (bool_normalize_u==1)
	{	resistance_normalize_u(u, ny, R, I);
		if (bool_save_u==1)	for (i=0; i<ny; i++) ds1->yd[i]=(float)u[i];
	}		
	mean_u = gsl_stats_mean(u, 1, ny);
	var_u  = gsl_stats_sd_m(u, 1, ny, mean_u);
	
    ir = fftw_malloc((ny)*sizeof(double));		// to store i_R
    if (bool_deriv==0) 
         resistance_compute_i_R(u, ny, t, C, It, dt, ir, DERIVATION_METHOD_FINITE_DIFFERENCES_ORDRE2);
    else if (bool_deriv==1) 
         resistance_compute_i_R(u, ny, t, C, It, dt, ir, DERIVATION_METHOD_SPLINE);
    else if (bool_deriv==2) 
         resistance_compute_i_R(u, ny, t, C, It, dt, ir, DERIVATION_METHOD_FOURIER);
        
	mean_i = gsl_stats_mean(ir+1, 1, ny-2);
	var_i  = gsl_stats_sd_m(ir+1, 1, ny-2, mean_i);

	if (bool_save_i==1)
	{	
		ds2 = create_and_attach_one_ds (op1, ny-2, ny-2, 0);
		for (i=1; i<ny-1; i++)
		{	ds2->xd[i-1] = ds1->xd[i];
			ds2->yd[i-1] = (float)(ir[i]);
		}
		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"i_R = I - C dU/dt\nI=%4.2g A", I);
	}

	Q = fftw_malloc((ny-2)*sizeof(double));	
	for (i=1; i<ny-1; i++)
	{	Q[i-1] = u[i]*ir[i]*dt;
	}
	if (bool_kT==1)  for (i=0; i<ny-2; i++)	Q[i] /= kT;
	mean_Q = gsl_stats_mean(Q, 1, ny-2);
	var_Q  = gsl_stats_sd_m(Q, 1, ny-2, mean_Q);

    if (1==1)
	{	
		ds2 = create_and_attach_one_ds (op1, ny-2, ny-2, 0);
		for (i=1; i<ny-1; i++)
		{	ds2->xd[i-1] = ds1->xd[i];
			ds2->yd[i-1] = (float)(Q[i-1]);
		}
		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"heat Q_R = U.i_R.dt  for I=%4.2g A", I);

		set_ds_plot_label(ds2, ds2->xd[1], 0, USR_COORD, "\\pt10\\stack{{R=%4.2g \\Omega / C=%4.2g F}"
				"{I=%4.4g A / dt=%4.4g s / P=%4.4g %s/s}"
				"{<i_R>=%4.2g A / <U>=%4.2g V / <Q>=%5.5g %s}"
				"{\\sigma_i=%4.2g A / \\sigma_U=%4.2g V / \\sigma_Q=%4.4g %s}}",  
				R, C, I, dt, mean_Q/dt, (bool_kT==1) ? "kT" : "J", 
				mean_i, mean_u, mean_Q, (bool_kT==1) ? "kT" : "J", var_i, var_u, var_Q, (bool_kT==1) ? "kT" : "J");
	}

	fftw_free(u);	fftw_free(t);
    fftw_free(It);	fftw_free(ir);
	fftw_free(Q);
	
	refresh_plot(pr, UNCHANGED);
	return(D_O_K);
} // end of the do_compute_heat_in_resistance







int do_resistance_compute_work(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny;
	d_s 	*ds1, *ds2=NULL;
	pltreg *pr = NULL;
	int	index;
	float	lR=(float)R, lI=(float)I;	
	double	dt=1;
	double	*W, *u;
	float	f_acq;
	int 	bool_normalize_u=1, bool_save_u=0, bool_kT =1;
	double	mean_u, mean_W, var_u, var_W;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the work injected in a RC circuit (//)\n"
					"when the tension is known. It is assumed the resistor is in parallel\n"
					"with a capacitor.\n\n"
					"a new dataset is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->ny;	// number of points in time series 
	if (ny<4) 						return(win_printf_OK("not enough points !"));

	i=win_scanf("{\\color{yellow}\\pt14 Injected Work in R//C}\n\n"
			"What is the resistance (Ohm)? %12f\n"
			"What is the imposed current (A)? %12f\n\n"
			"%b normalize U ( U-<U>+RI ) and then %b update U (old dataset)\n"
			"%b work divided by kT\n",
			&lR, &lI, &bool_normalize_u, &bool_save_u, &bool_kT);
	if (i==CANCEL) return(D_O_K);
	R=(double)lR; I=(double)lI;

	dt = (double)(ds1->xd[ny-1]-ds1->xd[0])/(ny-1);
	if ( (ds1->xd[2]-ds1->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)				return(win_printf_OK("dt is zero !"));

	f_acq=(float)(1/dt);

	u  = fftw_malloc((ny)*sizeof(double));		// to store u
	for (i=0; i<ny; i++)	u[i]=(double)ds1->yd[i];

	if (bool_normalize_u==1)
	{	mean_u    = gsl_stats_mean(u, 1, ny);
		for (i=0; i<ny; i++)
		{	u[i] = u[i] - mean_u + R*I;
		}
		if (bool_save_u==1)
		{ 	for (i=0; i<ny; i++)
			{	ds1->yd[i]=(float)u[i];
			}
		}
	}

	mean_u = gsl_stats_mean(u, 1, ny);
	var_u  = gsl_stats_sd_m(u, 1, ny, mean_u);

	W = fftw_malloc(ny*sizeof(double));	
	for (i=0; i<ny; i++)
	{	W[i] = u[i]*I*dt;
	}
	if (bool_kT==1)
	{  for (i=0; i<ny; i++)
		{	W[i] /= kT;
		}
	}
	mean_W = gsl_stats_mean(W, 1, ny);
	var_W  = gsl_stats_sd_m(W, 1, ny, mean_W);

	if (1==1)
	{	
		ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = ds1->xd[i];
			ds2->yd[i] = (float)(W[i]);
		}
		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"work W_R = U.I.dt  for I=%4.2g A", I);

		set_ds_plot_label(ds2, ds2->xd[1], 0, USR_COORD, "\\pt10\\stack{{imposed I=%4.4g A / dt=%4.4g s / P=%4.4g %s/s}"
				"{<U> = %4.4g V  <W>= %5.5g %s}"
				"{\\sigma_U = %4.4g V  /  \\sigma_W = %4.4g %s}}",  
				I,  dt, mean_W/dt, (bool_kT==1) ? "kT" : "J", mean_u, mean_W, (bool_kT==1) ? "kT" : "J", var_u, var_W, (bool_kT==1) ? "kT" : "J");
	}

	fftw_free(u);
	fftw_free(W);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_resistance_compute_work function






// 2007/10/15
int do_resistance_compute_heat_Gibbs(void)
{
	register int i;
	O_p 	*op1 = NULL, *op2=NULL;
	int	    ny, nh=80;
	d_s 	*ds1, *ds2=NULL, *ds_hist=NULL;
	pltreg *pr = NULL;
	int	index;
	float	lR=(float)R, lI=(float)I;	
	double	dt=1, c;
	double	*Q_Gibbs, *u;
	float	f_acq, extension=5.;
static int 	bool_normalize_u=1, bool_proba=0, bool_kT=1, bool_plot_pdf=0, bool_auto_detect_bins_in_file=0;
	double	mean_u, mean_Q_Gibbs, var_u, var_Q_Gibbs;
	gsl_histogram *hist=NULL;
static char pdf_filename[512]="pdf.dat";
	FILE *file=NULL;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the Gibbs entropy/heat in a RC circuit (//)\n"
					"when the tension is known. It is assumed the resistor is in parallel\n"
					"with a capacitor.\n\n"
					"a new dataset is created.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->ny;	// number of points in time series 
	if (ny<4) 						return(win_printf_OK("not enough points !"));

	i=win_scanf("{\\color{yellow}\\pt14 Gibbs trajectory-dependent heat in R//C}\n\n"
			"What is the resistance (Ohm)? %12f\n"
			"What is the imposed current (A)? %12f\n\n"
			"%b normalize U ( U-<U>+RI )\n"
			"%b work divided by kT\n"
            "%R compute probabilities assuming Gaussian distribution\n"
            "%r compute probabilities (no assumption)\n"
            "%r use an existing pdf\n\n"
		"%b plot the pdf used.",
			&lR, &lI, &bool_normalize_u, &bool_kT, &bool_proba, &bool_plot_pdf);
	if (i==CANCEL) return(D_O_K);
	R=(double)lR; I=(double)lI;


	dt = (double)(ds1->xd[ny-1]-ds1->xd[0])/(ny-1);
	if ( (ds1->xd[2]-ds1->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)				return(win_printf_OK("dt is zero !"));
	f_acq=(float)(1./dt);

	u  = fftw_malloc((ny)*sizeof(double));		// to store u
	for (i=0; i<ny; i++) u[i]=(double)ds1->yd[i];

	if (bool_normalize_u==1) resistance_normalize_u(u, ny, R, I);
	mean_u = gsl_stats_mean(u, 1, ny);
	var_u  = gsl_stats_sd_m(u, 1, ny, mean_u);

// win_printf("<u> = %g\n \\sigma = %g", mean_u, var_u);

	// get the pdf of U:
	if (bool_proba==0) // assuming Gaussian distribution
    	{  if (win_scanf("nb of bins ? %5d\n"
                     "extension (in \\sigma) ? +/- %5f", &nh, &extension)==CANCEL) return(D_O_K);
       	   ds_hist = build_data_set(nh, nh);
    	   for (i=0; i<nh; i++)
       	   { 	c              = mean_u - extension*var_u + (double)i/(double)nh*(double)2.*extension*var_u;
     		ds_hist->xd[i] = (float)(c);
           	ds_hist->yd[i] = (float)( exp(-(c-mean_u)*(c-mean_u)/(2.*var_u*var_u)) );
           	ds_hist->yd[i] /=(float) ( sqrt(2.*M_PI)*var_u) ;
           }
    
           if (ds_to_histogram(ds_hist, &hist)!=nh) return(win_printf_OK("error in pdf allocation"));
        free_data_set(ds_hist);
	}
    	else if (bool_proba==1) // computing distribution from dataset
    	{  if (win_scanf("nb of bins ? %5d\n"
                     "extension (in \\sigma) ? +/- %5f", &nh, &extension)==CANCEL) return(D_O_K);
           hist = (gsl_histogram *)gsl_histogram_alloc(nh);
	   gsl_histogram_set_ranges_uniform (hist, (double)(mean_u-extension*var_u), (double)(mean_u+extension*var_u));

	   for (i=0; i<ny; i++)
	   {	gsl_histogram_increment(hist, (double)(u[i])); // histogram of averaged data
	   }
  	   normalize_histogram_Y(hist); // to be secure...
        }
	else if (bool_proba==2) // loading distribution from file on disk
	{	win_scanf("{\\pt14\\color{yellow}load pdf}\n"
			"filename %s\nnumber of bins %5d\n or %b autodetect", &pdf_filename, &nh, &bool_auto_detect_bins_in_file);
		if (bool_auto_detect_bins_in_file==1) 
		{	nh = count_number_of_lines_in_ascii_file(pdf_filename, 3); // 3 columns
			win_printf("I autodetected %d lines\n => %d bins", nh, nh);
		}
		if (nh<2) return(win_printf_OK("error with nb of bins..."));
		file=fopen(pdf_filename, "rt");
		if (file==NULL) return(win_printf_OK("cannot open file %s", pdf_filename));
		hist = (gsl_histogram *)gsl_histogram_alloc(nh);
		if (gsl_histogram_fscanf(file, hist)!=0) return(win_printf_OK("(GSL) error loading pdf")); 	
		fclose(file);
// win_printf("psd loaded from file");	
	}
 

	// transform histogram into usable pdf:
	normalize_histogram_Y(hist);        

	if (bool_plot_pdf==1)
	{	if ((op2 = create_and_attach_one_plot (pr, nh, nh, 0))==NULL) return win_printf_OK("cannot create plot for pdf !");
		ds2 = op2->dat[0];
    		histogram_to_ds(hist, ds2, nh);
	}
win_printf("?");

	Q_Gibbs = fftw_malloc(ny*sizeof(double));	
	resistance_compute_Gibbs_heat_power(u, ny, hist, dt, Q_Gibbs);
	for (i=1; i<ny-1; i++)
	{ Q_Gibbs[i] *= dt; // par analogie avec W et Q, je prefere multiplier aussi ici par dt.
	}

win_printf("?");

	if (bool_kT==1)  for (i=0; i<ny; i++)	Q_Gibbs[i] /= kT;

	mean_Q_Gibbs = gsl_stats_mean(Q_Gibbs, 1, ny);
	var_Q_Gibbs  = gsl_stats_sd_m(Q_Gibbs, 1, ny, mean_Q_Gibbs);

	if (1==1)
	{	
		ds2 = create_and_attach_one_ds (op1, ny-2, ny-2, 0);
		for (i=1; i<ny-1; i++)
		{	ds2->xd[i-1] = ds1->xd[i];
			ds2->yd[i-1] = (float)(Q_Gibbs[i]);
		}
		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"Gibbs eq. entropy fluct. for I=%4.2g A", I);

		set_ds_plot_label(ds2, ds2->xd[1], 0, USR_COORD, "\\pt10\\stack{{imposed I=%4.4g A / dt=%4.4g s / P=%4.4g %s/s}"
				"{<U>=%4.4g V / <Q_{Gibbs}>=%5.5g %s}"
				"{\\sigma_U=%4.4g V / \\sigma_Q_{Gibbs}=%4.4g %s}}",  
				I, dt, mean_Q_Gibbs/dt, (bool_kT==1) ? "kT" : "J", 
				mean_u, mean_Q_Gibbs, (bool_kT==1) ? "kT" : "J", 
				var_u, var_Q_Gibbs, (bool_kT==1) ? "kT" : "J");
	}
	fftw_free(u);
	fftw_free(Q_Gibbs);
	gsl_histogram_free(hist);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_resistance_compute_heat_Gibbs function








int do_resistance_compute_energy_variation(void)
{
	register int i;
	O_p 	*op1 = NULL;
	int	ny;
	d_s 	*ds1, *ds2=NULL;
	pltreg *pr = NULL;
	int	index;
	float	lR=(float)R, lC=(float)C, lI=(float)I;	
	double	dt=1;
	double	*E, *u, *dE, *t;
	float	f_acq;
	int 	bool_normalize_u=1, bool_save_u=0, bool_kT =1;
	double	mean_u, mean_E, var_u, var_E;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes the potential energy %s in a RC circuit (//)\n"
					"when the tension is known. It is assumed the resistor is in parallel\n"
					"with a capacitor.\n\n"
					"a new dataset is created.", (index && RESISTANCE_ENERGY) ? "" : "variation");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->ny;	// number of points in time series 
	if (ny<4) 						return(win_printf_OK("not enough points !"));

	i=win_scanf("{\\color{yellow}\\pt14 Poentential Energy variation in R//C}\n\n"
			"What is the resistance (Ohm)? %12f\n"
			"What is the capacitor (Farad)? %12f\n"
			"What is the imposed current (A)? %12f\n\n"
			"%b normalize U ( U-<U>+RI ) and then %b update U (old dataset)\n"
			"%b work divided by kT\n",
			&lR, &lC, &lI, &bool_normalize_u, &bool_save_u, &bool_kT);
	if (i==CANCEL) return(D_O_K);
	R=(double)lR; C=(double)lC; I=(double)lI;

	dt = (double)(ds1->xd[ny-1]-ds1->xd[0])/(ny-1);
	if ( (ds1->xd[2]-ds1->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)				return(win_printf_OK("dt is zero !"));
	f_acq=(float)(1/dt);

	u  = fftw_malloc((ny)*sizeof(double));		// to store u
	for (i=0; i<ny; i++)	u[i]=(double)ds1->yd[i];

	if (bool_normalize_u==1)
	{	resistance_normalize_u(u, ny, R, I);
		if (bool_save_u==1)
		{ 	for (i=0; i<ny; i++)  ds1->yd[i]=(float)u[i];
		}
	}

	mean_u = gsl_stats_mean(u, 1, ny);
	var_u  = gsl_stats_sd_m(u, 1, ny, mean_u);

	E = fftw_malloc(ny*sizeof(double));	
	for (i=0; i<ny; i++)
	{	E[i] = (double)0.5*C*u[i]*u[i];
	}
	if (bool_kT==1)
	{  for (i=0; i<ny; i++)
		{	E[i] /= kT;
		}
	}
	if (index==RESISTANCE_ENERGY_VARIATION)
	{ 
		t  = fftw_malloc(ny*sizeof(double));
		dE = fftw_malloc(ny*sizeof(double));
		for (i=0; i<ny; i++) t[i] = (double)dt*i;
		diff_splines(t, E, ny, dE);
		fftw_free(t);
		memcpy(E, dE, ny*sizeof(double));
		fftw_free(dE);
	}

	mean_E = gsl_stats_mean(E, 1, ny);
	var_E  = gsl_stats_sd_m(E, 1, ny, mean_E);

	if (1==1)
	{	
		ds2 = create_and_attach_one_ds (op1, ny, ny, 0);
		for (i=0; i<ny; i++)
		{	ds2->xd[i] = ds1->xd[i];
			ds2->yd[i] = (float)(E[i]);
		}
		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"%s W_R = U.I.dt  for I=%4.2g A", 
				(index==RESISTANCE_ENERGY) ? "energy" : "energy variation", I);

		set_ds_plot_label(ds2, ds2->xd[1], 0, USR_COORD, "\\pt10\\stack{{I=%4.4g A / dt=%4.4g s / P=%4.4g %s/s}"
				"{<U> = %4.4g V / <%s>=%5.5g %s}"
				"{\\sigma_U = %4.4g V  / \\sigma_{%s} = %4.4g %s}}",  
				I,  dt, mean_E/dt, (bool_kT==1) ? "kT" : "J", 
				mean_u, (index==RESISTANCE_ENERGY) ? "E" : "\\Delta E", mean_E, (bool_kT==1) ? "kT" : "J", 
				var_u,  (index==RESISTANCE_ENERGY) ? "E" : "\\Delta E", var_E, (bool_kT==1) ? "kT" : "J");
	}

	fftw_free(u);
	fftw_free(E);

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_resistance_compute_energy_variation function

#endif
