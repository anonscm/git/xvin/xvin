#ifndef _RESISTANCE_H_
#define _RESISTANCE_H_

// NG, 2007-03-08 : following lines commented out because already defined in plugin diff.
// #define	DERIVATION_METHOD_FINITE_DIFFERENCES	0x0100
// #define	DERIVATION_METHOD_FOURIER				0x0200
// #define	DERIVATION_METHOD_SPLINE				0x0400

#define RESISTANCE_FIT_RC						0x0200
#define RESISTANCE_FIT_C						0x0100


#define k_b 1.3806503e-23	// m2 kg s-2 K-1
#define Temperature (25.+273.)		// K

extern double  R;
extern double  C;
extern double  I;
extern double  kT;

PXV_FUNC(MENU*, resistance_plot_menu, 			(void));
PXV_FUNC(int, resistance_main, 			(int argc, char **argv));
PXV_FUNC(int, resistance_unload,		(int argc, char **argv));

PXV_FUNC(int, do_set_resistance_parameters,		(void));
PXV_FUNC(int, do_fit_LP_RC,				(void));
PXV_FUNC(int, do_plot_lorentzian_spectrum,		(void));
PXV_FUNC(int, do_resistance_compute_imposed_current,	(void));
PXV_FUNC(int, do_resistance_compute_intensity,	(void));
PXV_FUNC(int, do_resistance_integrate_equation,              (void));
PXV_FUNC(int, do_hist_periodic_build_from_NI4472,		(void));
PXV_FUNC(int, do_adjust_offset_NI4472,		(void));


void spit(char *message);

#endif

