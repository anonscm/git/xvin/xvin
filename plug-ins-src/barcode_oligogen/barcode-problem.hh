#pragma once
#include <string>
#include "../fishing_rod/sequence.hh"

class BarcodeProblem
{
    public:
        BarcodeProblem(SequenceSet &sequences);
        BarcodeProblem(const BarcodeProblem& o); // /!\ Shadow copy
        virtual  ~BarcodeProblem();

        BarcodeProblem& operator= (const BarcodeProblem& o); // /!\ Shadow copy

        const std::string& name() const;



    private:
        SequenceSet& sequences_;
        int minDistBetweenOligosPos_;
        float minTm;
        float maxTm;

};
