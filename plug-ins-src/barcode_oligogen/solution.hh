#pragma once
#include  <vector>
#include "barcode-problem.hh"
#include "../fingerprint/bidirectional-oligo.hh"

#define MIN_OLIGO_SIZE 6
class Solution;

class Solution
{
    public:
        Solution(BarcodeProblem& problem);
        Solution(const Solution& o);
        virtual ~Solution();

        Solution& operator= (const Solution& o);
        void generate(void);
        Solution mutate(void);
        Solution crossover(Solution& s);

        bool isValid(void);
        float fitness(void);
        float fitness(void) const;

    private:
        BarcodeProblem &barcodeProblem_;
        std::mt19937 randomGen_;
        std::uniform_int_distribution<> randomDistrib_;
        std::vector<BiDirOligo> oligos;
        bool isGenerated_;
        static const char nucleoIdxToChar[4];

        Oligo generateOligo();
        char selectNucleotide(void);

};
