/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _MOVIECUT_C_
#define _MOVIECUT_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "moviecut.h"

O_i	*moviecut_image_cut_movie(O_i *ois, int  dlx, int dly, int urx, int ury)
{
  register int i, j, k=0;
  O_i *oid;
  int onx, ony, data_type;
  int nf;
  union pix *ps, *pd;

  onx = urx - dlx;
  ony = ury - dly;
  data_type = ois->im.data_type;
  nf =ois->im.n_f;
  oid =  create_one_movie(onx, ony, data_type, nf);


  if (oid == NULL)
    {
      win_printf("Can't create dest image");
      return NULL;
    }
  for(k=0;k<nf;k++){
    switch_frame(ois,k);
    switch_frame(oid,k);
    if (data_type == IS_CHAR_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j=0; j< onx; j++)
	      {
		pd[i].ch[j] = ps[i+dly].ch[j+dlx];
	      }
	  }
      }
    else if (data_type == IS_INT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j=0; j< onx; j++)
	      {
		pd[i].in[j] = ps[i+dly].in[j+dlx];
	      }
	  }
      }
    else if (data_type == IS_UINT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j=0; j< onx; j++)
	      {
		pd[i].ui[j] = ps[i+dly].ui[j+dlx];
	      }
	  }
      }
    else if (data_type == IS_LINT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j=0; j< onx; j++)
	      {
		pd[i].li[j] = ps[i+dly].li[j+dlx];
	      }
	  }
      }
    else if (data_type == IS_FLOAT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j=0; j< onx; j++)
	      {
		pd[i].fl[j] = ps[i+dly].fl[j+dlx];
	      }
	  }
      }
    else if (data_type == IS_DOUBLE_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j=0; j< onx; j++)
	      {
		pd[i].db[j] = ps[i+dly].db[j+dlx];
	      }
	  }
      }
/*     else if (data_type == IS_COMPLEX_IMAGE) */
/*       { */
/* 	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++) */
/* 	  { */
/* 	    for (j=0; j< onx; j++) */
/* 	      { */
/* 		pd[i].fl[2*j] = ps[i+dly].fl[2*(j+dlx)]; */
/* 		pd[i].fl[2*j+1] = ps[i+dly].fl[2*(j+dlx)+1]; */
/* 	      } */
/* 	  } */
/*       } */
/*     else if (data_type == IS_COMPLEX_DOUBLE_IMAGE) */
/*       { */
/* 	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++) */
/* 	  { */
/* 	    for (j=0; j< onx; j++) */
/* 	      { */
/* 		pd[i].db[2*j] = ps[i+dly].db[2*(j+dlx)]; */
/* 		pd[i].db[2*j+1] = ps[i+dly].db[2*(j+dlx)+1]; */
/* 	      } */
/* 	  } */
/*       } */
/*     else if (data_type == IS_RGB_PICTURE) */
/*       { */
/* 	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++) */
/* 	  { */
/* 	    for (j=0; j< 3*onx; j++) */
/* 	      { */
/* 		pd[i].ch[j] = ps[i+dly].ch[j+dlx]; */
/* 	      } */
/* 	  } */
/*       } */
/*     else if (data_type == IS_RGBA_PICTURE) */
/*       { */
/* 	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++) */
/* 	  { */
/* 	    for (j=0; j< 4*onx; j++) */
/* 	      { */
/* 		pd[i].ch[j] = ps[i+dly].ch[j+dlx]; */
/* 	      } */
/* 	  } */
/*       }*/
    else win_printf("not implemented");
  }
  inherit_from_im_to_im(oid,ois);
  oid->width = ois->width*onx/ois->im.nx;
  oid->width = ois->height*ony/ois->im.ny;
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s",
  					     ois->title);
  //set_formated_string(&oid->im.treatement,
  //	"Image %s", ois->filename);
  return oid;
}

int do_moviecut(void)
{
	O_i *ois=NULL, *oid=NULL;
	imreg *imr;
	int i;
	static int dlx =200, dly=200, urx=400, ury=400;

	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine cuts a movie");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	map_pixel_ratio_of_image_and_screen(ois, 1, 1);
	i = win_scanf("down left corner x %4d,y %4d\n"
		      "up right corner x %4d,y %4d\n",&dlx,&dly,&urx,&ury);
	if (i == WIN_CANCEL)	return D_O_K;
	oid = moviecut_image_cut_movie(ois,dlx,dly,urx,ury);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	map_pixel_ratio_of_image_and_screen(oid, 1, 1);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}



O_i	*movie_square_avg(O_i *ois, int x_avg, int y_avg)
{
  register int i, j, k=0;
  int l, m;
  O_i *oid;
   int onx, ony, data_type;
  int nf;
  union pix *ps, *pd;


  ony = ois->im.ny;	
  onx = ois->im.nx;	
  nf = ois->im.n_f;
  oid =  create_one_movie(onx - x_avg,ony - y_avg , IS_FLOAT_IMAGE, nf);
  data_type = ois->im.data_type;

  if (oid == NULL)
    {
      win_printf("Can't create dest image");
      return NULL;
    }
  for(k=0;k<nf;k++)
    {
    my_set_window_title("Processing image %d",k);
    switch_frame(ois,k);
    switch_frame(oid,k);
    if (data_type == IS_CHAR_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_avg; i++)
	  {
	    for (j=0; j< onx - x_avg; j++)
	      {
		for (l = 0; l < y_avg; l++)
		  {
		    for (m = 0; m < x_avg; m++)
		      pd[i].fl[j] += ps[i+l].ch[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_INT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_avg; i++)
	  {
	    for (j=0; j< onx - x_avg; j++)
	      {
		for (l = 0; l < y_avg; l++)
		  {
		    for (m = 0; m < x_avg; m++)
		      pd[i].fl[j] += ps[i+l].in[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_UINT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_avg; i++)
	  {
	    for (j=0; j< onx - x_avg; j++)
	      {
		for (l = 0; l < y_avg; l++)
		  {
		    for (m = 0; m < x_avg; m++)
		      pd[i].fl[j] += ps[i+l].ui[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_LINT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_avg; i++)
	  {
	    for (j=0; j< onx - x_avg; j++)
	      {
		for (l = 0; l < y_avg; l++)
		  {
		    for (m = 0; m < x_avg; m++)
		      pd[i].fl[j] += ps[i+l].li[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_FLOAT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_avg; i++)
	  {
	    for (j=0; j< onx - x_avg; j++)
	      {
		for (l = 0; l < y_avg; l++)
		  {
		    for (m = 0; m < x_avg; m++)
		      pd[i].fl[j] += ps[i+l].fl[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_DOUBLE_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_avg; i++)
	  {
	    for (j=0; j< onx - x_avg; j++)
	      {
		for (l = 0; l < y_avg; l++)
		  {
		    for (m = 0; m < x_avg; m++)
		      pd[i].fl[j] += ps[i+l].db[j+m];
		  }
	      }
	  }
      }
    else win_printf("not implemented");
  }
  inherit_from_im_to_im(oid,ois);
  oid->width = ois->width*onx/ois->im.nx;
  oid->width = ois->height*ony/ois->im.ny;
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s",
  					     ois->title);
  //set_formated_string(&oid->im.treatement,
  //	"Image %s", ois->filename);
  return oid;
}

int do_movie_square_avg(void)
{
	O_i *ois=NULL, *oid=NULL;
	imreg *imr;
	int i;
	static int wx =20, wy=20;

	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average a movie on a square");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	map_pixel_ratio_of_image_and_screen(ois, 1, 1);
	i = win_scanf("X average size  %8d\n"
		      "X average size  %8d\n",&wx,&wy);
	if (i == WIN_CANCEL)	return D_O_K;
	oid = movie_square_avg(ois,wx,wy);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	map_pixel_ratio_of_image_and_screen(oid, 1, 1);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}



O_i *create_appodisation_float_image(int onx, int ony, float smp, int desapo)
{
  register int i, j;
  int nx_2, ny_2, nx_4, ny_4, x, y;//, nxy
  float smp_1;//, rxy;
  O_i *oid;
  union pix *pd;
 
  oid = create_one_image(onx, ony, IS_FLOAT_IMAGE);
  if (oid == NULL)	return win_printf_ptr("cannot create image !");	
  nx_2 = onx/2; ny_2 = ony/2;
  nx_4 = onx/4; ny_4 = ony/4;
  //nxy = nx_2 + ny_2;
  //rxy = sqrt(nx_4 * nx_4 +ny_4 * ny_4);
  smp_1 = 1 - (2 * smp);
  for (i = 0, pd = oid->im.pixel; i < ony ; i++)
    {
      y = (i < ny_2) ? i : ony - i;
      for (j = 0; j< onx; j++)
	{
	  x = (j < nx_2) ? j : onx - j;
	  if (x < nx_4)
	    {
	      if (y < x)  pd[i].fl[j] = 0.5 * (1 - smp_1 * cos((M_PI * y)/ny_4));  
	      else  pd[i].fl[j] = 0.5 * (1 - (smp_1 * cos((M_PI * x)/nx_4)));  
	    }
	  else
	    {
	      if (y < nx_4)  pd[i].fl[j] = 0.5 * (1 - (smp_1 * cos((M_PI * y)/ny_4)));  
	      else           pd[i].fl[j] = 1 - smp;

	    }
	  /*
	  if ((j -i) >= 0)
	    {
	      if ((j + i - nxy) >= 0)  pd[i].fl[j] = 0.5 * (1 - smp_1 * cos((M_PI * j)/nx_2));  
	      else                     pd[i].fl[j] = 0.5 * (1 - smp_1 * cos((M_PI * i)/ny_2));  
	    }
	  else
	    {
	      if ((j + i - nxy) >= 0)  pd[i].fl[j] = 0.5 * (1 - smp_1 * cos((M_PI * i)/ny_2));  
	      else                     pd[i].fl[j] = 0.5 * (1 - smp_1 * cos((M_PI * j)/nx_2));  
	    }
	  */
	  
	}
    }
  if (desapo)
    {
      for (i = 0, pd = oid->im.pixel; i < ony ; i++)
	{
	  for (j = 0; j< onx; j++)
	    pd[i].fl[j] = (pd[i].fl[j] != 0) ? (float)1/pd[i].fl[j] : pd[i].fl[j];
	}
    }
  find_zmin_zmax(oid);
  oid->need_to_refresh = ALL_NEED_REFRESH;		
  return oid;
}



O_i	*movie_square_avg_apo(O_i *ois, int x_avg, int y_avg)
{
  register int i, j, k=0;
  int l, m;
  O_i *oid, *oit;
   int onx, ony, data_type;
  int nf;
  union pix *ps, *pd, *pt;


  ony = ois->im.ny;	
  onx = ois->im.nx;	
  nf = ois->im.n_f;
  oid =  create_one_movie(onx - x_avg,ony - y_avg , IS_FLOAT_IMAGE, nf);
  oit =  create_one_image(x_avg,y_avg , IS_FLOAT_IMAGE);
  data_type = ois->im.data_type;

  if (oid == NULL || oit == NULL)
    {
      win_printf("Can't create dest image");
      return NULL;
    }

  oit = create_appodisation_float_image(x_avg,y_avg, 0, 0);
  if (oit == NULL)	return win_printf_ptr("Could not create apodisation image!");


  pt = oit->im.pixel;
  for(k=0;k<nf;k++)
    {
    switch_frame(ois,k);
    switch_frame(oid,k);
    my_set_window_title("Processing image %d",k);
    if (data_type == IS_CHAR_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_avg; i++)
	  {
	    for (j=0; j< onx - x_avg; j++)
	      {
		for (l = 0; l < y_avg; l++)
		  {
		    for (m = 0; m < x_avg; m++)
		      pd[i].fl[j] += pt[l].fl[m] * ps[i+l].ch[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_INT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_avg; i++)
	  {
	    for (j=0; j< onx - x_avg; j++)
	      {
		for (l = 0; l < y_avg; l++)
		  {
		    for (m = 0; m < x_avg; m++)
		      pd[i].fl[j] += pt[l].fl[m] * ps[i+l].in[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_UINT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_avg; i++)
	  {
	    for (j=0; j< onx - x_avg; j++)
	      {
		for (l = 0; l < y_avg; l++)
		  {
		    for (m = 0; m < x_avg; m++)
		      pd[i].fl[j] += pt[l].fl[m] * ps[i+l].ui[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_LINT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_avg; i++)
	  {
	    for (j=0; j< onx - x_avg; j++)
	      {
		for (l = 0; l < y_avg; l++)
		  {
		    for (m = 0; m < x_avg; m++)
		      pd[i].fl[j] += pt[l].fl[m] * ps[i+l].li[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_FLOAT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_avg; i++)
	  {
	    for (j=0; j< onx - x_avg; j++)
	      {
		for (l = 0; l < y_avg; l++)
		  {
		    for (m = 0; m < x_avg; m++)
		      pd[i].fl[j] += pt[l].fl[m] * ps[i+l].fl[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_DOUBLE_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_avg; i++)
	  {
	    for (j=0; j< onx - x_avg; j++)
	      {
		for (l = 0; l < y_avg; l++)
		  {
		    for (m = 0; m < x_avg; m++)
		      pd[i].fl[j] += pt[l].fl[m] * ps[i+l].db[j+m];
		  }
	      }
	  }
      }
    else win_printf("not implemented");
  }
  inherit_from_im_to_im(oid,ois);
  oid->width = ois->width*onx/ois->im.nx;
  oid->width = ois->height*ony/ois->im.ny;
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s",
  					     ois->title);
  //set_formated_string(&oid->im.treatement,
  //	"Image %s", ois->filename);
  free_one_image(oit);
  return oid;
}

int do_movie_square_avg_apo(void)
{
	O_i *ois=NULL, *oid=NULL;
	imreg *imr;
	int i;
	static int wx =20, wy=20;

	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average a movie on a square");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	map_pixel_ratio_of_image_and_screen(ois, 1, 1);
	i = win_scanf("X average size  %8d\n"
		      "X average size  %8d\n",&wx,&wy);
	if (i == WIN_CANCEL)	return D_O_K;
	oid = movie_square_avg_apo(ois,wx,wy);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	map_pixel_ratio_of_image_and_screen(oid, 1, 1);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}


O_i	*image_square_local_convolution_in_x(O_i *ois, int xc, int yc, int x_size, int y_size, int width)
{
  register int i, j, k=0;
  int l, m;
  O_i *oid = NULL, *oit = NULL;
   int onx, ony, data_type;
  int nf;
  union pix *ps = NULL, *pd = NULL, *pt = NULL;

  (void)xc;
  (void)yc;
  ony = ois->im.ny;	
  onx = ois->im.nx;	
  nf = ois->im.n_f;
  oid = create_one_image(2 * width + 1, nf, IS_FLOAT_IMAGE);
  data_type = ois->im.data_type;

  oit = create_one_image(x_size, y_size, IS_FLOAT_IMAGE);
  if (oid == NULL || oit == NULL)
    {
      win_printf("Can't create dest image");
      return NULL;
    }

  
  pt = oit->im.pixel;
  for(k=0;k<nf;k++)
    {
    switch_frame(ois,k);
    switch_frame(oid,k);
    my_set_window_title("Processing image %d",k);
    if (data_type == IS_CHAR_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_size; i++)
	  {
	    for (j=0; j< onx - x_size; j++)
	      {
		for (l = 0; l < y_size; l++)
		  {
		    for (m = 0; m < x_size; m++)
		      pd[i].fl[j] += pt[l].fl[m] * ps[i+l].ch[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_INT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_size; i++)
	  {
	    for (j=0; j< onx - x_size; j++)
	      {
		for (l = 0; l < y_size; l++)
		  {
		    for (m = 0; m < x_size; m++)
		      pd[i].fl[j] += pt[l].fl[m] * ps[i+l].in[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_UINT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_size; i++)
	  {
	    for (j=0; j< onx - x_size; j++)
	      {
		for (l = 0; l < y_size; l++)
		  {
		    for (m = 0; m < x_size; m++)
		      pd[i].fl[j] += pt[l].fl[m] * ps[i+l].ui[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_LINT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_size; i++)
	  {
	    for (j=0; j< onx - x_size; j++)
	      {
		for (l = 0; l < y_size; l++)
		  {
		    for (m = 0; m < x_size; m++)
		      pd[i].fl[j] += pt[l].fl[m] * ps[i+l].li[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_FLOAT_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_size; i++)
	  {
	    for (j=0; j< onx - x_size; j++)
	      {
		for (l = 0; l < y_size; l++)
		  {
		    for (m = 0; m < x_size; m++)
		      pd[i].fl[j] += pt[l].fl[m] * ps[i+l].fl[j+m];
		  }
	      }
	  }
      }
    else if (data_type == IS_DOUBLE_IMAGE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony - y_size; i++)
	  {
	    for (j=0; j< onx - x_size; j++)
	      {
		for (l = 0; l < y_size; l++)
		  {
		    for (m = 0; m < x_size; m++)
		      pd[i].fl[j] += pt[l].fl[m] * ps[i+l].db[j+m];
		  }
	      }
	  }
      }
    else win_printf("not implemented");
  }
  inherit_from_im_to_im(oid,ois);
  oid->width = ois->width*onx/ois->im.nx;
  oid->width = ois->height*ony/ois->im.ny;
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s",
  					     ois->title);
  //set_formated_string(&oid->im.treatement,
  //	"Image %s", ois->filename);
  free_one_image(oit);
  return oid;
}


MENU *moviecut_image_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)	return mn;
    add_item_to_menu(mn,"cut movie", do_moviecut,NULL,0,NULL);
    add_item_to_menu(mn,"Avg movie", do_movie_square_avg,NULL,0,NULL);
    add_item_to_menu(mn,"Avg movie apo", do_movie_square_avg_apo,NULL,0,NULL);

    return mn;
}

int	moviecut_main(int argc, char **argv)
{
    (void)argc;
    (void)argv;      
    add_image_treat_menu_item ( "moviecut", NULL, moviecut_image_menu(), 0, NULL);
    return D_O_K;
}

int	moviecut_unload(int argc, char **argv)
{
    (void)argc;
    (void)argv;    
    remove_item_to_menu(image_treat_menu, "moviecut", NULL, NULL);
    return D_O_K;
}
#endif

