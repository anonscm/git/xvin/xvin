#ifndef _MOVIECUT_H_
#define _MOVIECUT_H_

PXV_FUNC(int, do_moviecut, (void));
PXV_FUNC(MENU*, moviecut_image_menu, (void));
PXV_FUNC(int, moviecut_main, (int argc, char **argv));
#endif

