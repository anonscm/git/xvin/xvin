#ifndef _FITS_CHI_C_
#define _FITS_CHI_C_

// #include "allegro.h"
#include "xvin.h"

// below are gsl includes:
// #include <stdlib.h>
// #include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
// #include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_sf.h>

/* If you include other plug-ins header do it here*/ 
// #include "../../include/fftw3.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "fits.h"
#include "fits_chi.h"


// required by the example from gsl:
int chi_f (const gsl_vector *x, void *params, gsl_vector *f)
{
  size_t n = ((struct data *)params)->n;
  double *y = ((struct data *)params)->y;
//  double *sigma = ((struct data *) params)->sigma;
	double Yi;
  double a = gsl_vector_get (x, 0);
  
  size_t i;

  for (i = 0; i < n; i++)
    {
      /* Model Yi = Model Yi =  */
      double t = (double) i*fits_dt + fits_t_min;
     if (t+a <= 0) 	Yi=0.;
     else      		Yi = -a/2.*log(2) - gsl_sf_lngamma(a/2.) - 1/2.*(t+a)+(a/2.-1)*log(t+a);
	 
     
      gsl_vector_set (f, i, (Yi - y[i]) /* /sigma[i] */ );
    }

  return GSL_SUCCESS;
}


// required by the example from gsl:
int chi_df (const gsl_vector * x, void *params, gsl_matrix *J)
{
  size_t n = ((struct data *)params)->n;
//  double *sigma = ((struct data *) params)->sigma;

  double a = gsl_vector_get (x, 0);

  size_t i;
  for (i = 0; i < n; i++)
    {
      /* Jacobian matrix J(i,j) = dfi / dxj, */
      /* where fi = (Yi - yi)/sigma[i],      */
      /*       Yi = log(chi(a) */
      /* and the xj are the parameters (a) */
      double t = (double)i*fits_dt + fits_t_min;
//      double s = sigma[i];
     if (t+a <= 0) 	gsl_matrix_set (J, i, 0, 0 /* /s */ );
     else gsl_matrix_set (J, i, 0, -1/2.*log(2)-1/2.*gsl_sf_psi(a/2.)+1/2.+1/2.*log(t+a)+(a/2.-1)/(t+a) /* /s */ ); 
     }
  return GSL_SUCCESS;
}


// required by the example from gsl:
int chi_fdf (const gsl_vector *x, void *params, gsl_vector *f, gsl_matrix *J)
{
  chi_f (x, params, f);
  chi_df (x, params, J);

  return GSL_SUCCESS;
}

#endif
