#ifndef _FITS_CHI_H_
#define _FITS_CHI_H_

int chi_f   (const gsl_vector *x, void *params, gsl_vector *f);
int chi_df  (const gsl_vector *x, void *params, gsl_matrix *J);
int chi_fdf (const gsl_vector *x, void *params, gsl_vector *f, gsl_matrix *J);

#endif
