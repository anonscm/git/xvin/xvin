#ifndef _FITS_BHP_H_
#define _FITS_BHP_H_

int BHP_f   (const gsl_vector *x, void *params, gsl_vector *f);
int BHP_df  (const gsl_vector *x, void *params, gsl_matrix *J);
int BHP_fdf (const gsl_vector *x, void *params, gsl_vector *f, gsl_matrix *J);

extern int		bool_fit_BHP_also_m;
extern double 	fits_BHP_m;

#endif
