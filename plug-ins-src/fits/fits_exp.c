#ifndef _FITS_EXP_C_
#define _FITS_EXP_C_

// #include "allegro.h"
#include "xvin.h"

// below are gsl includes:
// #include <stdlib.h>
// #include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
// #include <gsl/gsl_multifit_nlin.h>

/* If you include other plug-ins header do it here*/ 
// #include "../../include/fftw3.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "fits.h"
#include "fits_exp.h"

	
/*	This comes from an example program that fits a weighted exponential model with background 
	to experimental data, Y = A \exp(-\lambda t) + b. 
	The first part of the program sets up the functions expb_f and expb_df to calculate 
	the model and its Jacobian. The appropriate fitting function is given by,

	f_i = ((A \exp(-\lambda t_i) + b) - y_i)/\sigma_i

	where we have chosen t_i = i. The Jacobian matrix J is the derivative of these 
	functions with respect to the three parameters (A, \lambda, b). It is given by,

	J_{ij} = d f_i / d x_j

	where x_0 = A, x_1 = \lambda and x_2 = b.

*/




// required by the example from gsl:
int expb_f (const gsl_vector *x, void *params, gsl_vector *f)
{
  size_t n  = ((struct data *)params)->n;
  double *y = ((struct data *)params)->y;
//  double *sigma = ((struct data *) params)->sigma;

  double A      = gsl_vector_get (x, 0);
  double lambda = gsl_vector_get (x, 1);
  double b      = gsl_vector_get (x, 2);

  size_t i;

  for (i = 0; i < n; i++)
    {
      /* Model Yi = A * exp(-lambda * i) + b */
      double t = (double)i*fits_dt + fits_t_min;
      double Yi = A * exp (-lambda * t) + b;
      gsl_vector_set (f, i, (Yi - y[i]) /* /sigma[i] */ );
    }

  return GSL_SUCCESS;
}


// required by the example from gsl:
int expb_df (const gsl_vector *x, void *params, gsl_matrix *J)
{
  size_t n = ((struct data *)params)->n;
//  double *sigma = ((struct data *) params)->sigma;

  double A = gsl_vector_get (x, 0);
  double lambda = gsl_vector_get (x, 1);

  size_t i;

  for (i = 0; i < n; i++)
    {
      /* Jacobian matrix J(i,j) = dfi / dxj, */
      /* where fi = (Yi - yi)/sigma[i],      */
      /*       Yi = A * exp(-lambda * i) + b  */
      /* and the xj are the parameters (A,lambda,b) */
      double t = (double)i*fits_dt + fits_t_min;
//      double s = sigma[i];
      double e = exp(-lambda * t);
      gsl_matrix_set (J, i, 0, e /* /s */ ); 
      gsl_matrix_set (J, i, 1, -t * A * e /* /s */);
      gsl_matrix_set (J, i, 2, 1. /* /s */);

    }
  return GSL_SUCCESS;
}


// required by the example from gsl:
int expb_fdf (const gsl_vector *x, void *params, gsl_vector *f, gsl_matrix *J)
{
  expb_f (x, params, f);
  expb_df (x, params, J);

  return GSL_SUCCESS;
}

#endif
