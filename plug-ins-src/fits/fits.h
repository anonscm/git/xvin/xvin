#ifndef _FITS_H_
#define _FITS_H_

struct data {
  size_t n;
  double *y;
//  double * sigma;
};

extern double fits_dt, fits_t_min;

PXV_FUNC(MENU*, fits_plot_menu, 	(void));
PXV_FUNC(int,   fits_main,          (int argc, char **argv));
PXV_FUNC(int,   fits_unload,		(int argc, char **argv));

PXV_FUNC(int,   do_fit_exponential,	(void));
PXV_FUNC(int,   do_fit_Gumbel,	    (void));

#endif

