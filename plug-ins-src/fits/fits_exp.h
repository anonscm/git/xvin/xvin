#ifndef _FITS_EXP_H_
#define _FITS_EXP_H_

int expb_f   (const gsl_vector *x, void *params, gsl_vector *f);
int expb_df  (const gsl_vector *x, void *params, gsl_matrix *J);
int expb_fdf (const gsl_vector *x, void *params, gsl_vector *f, gsl_matrix *J);

#endif
