#ifndef _FITS_GUMBEL_C_
#define _FITS_GUMBEL_C_

// #include "allegro.h"
#include "xvin.h"

// below are gsl includes:
// #include <stdlib.h>
// #include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
// #include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_sf.h>

/* If you include other plug-ins header do it here*/ 
// #include "../../include/fftw3.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "fits.h"
#include "fits_gumbel.h"

int bool_fit_gumbel_also_m=1;
double fits_gumbel_m=1;

// required by the example from gsl:
int Gumbel_f (const gsl_vector *x, void *params, gsl_vector *f)
{
  size_t n = ((struct data *)params)->n;
  double *y = ((struct data *)params)->y;
  double t, Yi;

  double a = gsl_vector_get (x, 0);
  double b = gsl_vector_get (x, 1);
  double m = gsl_vector_get (x, 2);
  if (bool_fit_gumbel_also_m==0) m=fits_gumbel_m;
  size_t i;

  for (i = 0; i < n; i++)
    {
      /* Model Yi = Model Yi = ln(a)+ln(b)-a*(i-m)-b*exp(a*(i-m)) */
      t = (double)i*fits_dt + fits_t_min;
      Yi = log(fabs(a)) + log(b) - a*(t-m)-b*exp(-a*(t-m));
	
      gsl_vector_set (f, i, (Yi - y[i]) /* /sigma[i] */ );
    }
  return GSL_SUCCESS;
}


// required by the example from gsl:
int Gumbel_df (const gsl_vector * x, void *params, gsl_matrix *J)
{
  size_t n = ((struct data *)params)->n;
//  double *sigma = ((struct data *) params)->sigma;

  	double a = gsl_vector_get (x, 0);
  	double b = gsl_vector_get (x, 1);
	double m = gsl_vector_get (x, 2);
	if (bool_fit_gumbel_also_m==0) m=fits_gumbel_m;
    size_t i;

     for (i = 0; i < n; i++)
    {
      /* Jacobian matrix J(i,j) = dfi / dxj, */
      /* where fi = (Yi - yi)/sigma[i],      */
      /*       Yi = ln(a)+ln(b)-a*(i-m)-b*exp(a*(i-m))  */
      /* and the xj are the parameters (a,b,m) */
      double t = (double)i*fits_dt + fits_t_min;
//      double s = sigma[i];
      double e = exp(-a*(t-m));
      gsl_matrix_set (J, i, 0, 1./fabs(a)+b*(t-m)*e-(t-m) /* /s */ ); 
      gsl_matrix_set (J, i, 1, 1./b-e /* /s */);
      if (bool_fit_gumbel_also_m==0) 
      			gsl_matrix_set (J, i, 2, 0. /* /s */);
	  else		
	  		 	gsl_matrix_set (J, i, 2, a-a*b*e /* /s */);

    }
  return GSL_SUCCESS;
}


// required by the example from gsl:
int Gumbel_fdf (const gsl_vector *x, void *params, gsl_vector *f, gsl_matrix *J)
{
  Gumbel_f (x, params, f);
  Gumbel_df (x, params, J);

  return GSL_SUCCESS;
}

#endif
