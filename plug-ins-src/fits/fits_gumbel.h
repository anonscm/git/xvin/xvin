#ifndef _FITS_GUMBEL_H_
#define _FITS_GUMBEL_H_

int Gumbel_f   (const gsl_vector *x, void *params, gsl_vector *f);
int Gumbel_df  (const gsl_vector *x, void *params, gsl_matrix *J);
int Gumbel_fdf (const gsl_vector *x, void *params, gsl_vector *f, gsl_matrix *J);

extern int bool_fit_gumbel_also_m;
extern double fits_gumbel_m;

#endif
