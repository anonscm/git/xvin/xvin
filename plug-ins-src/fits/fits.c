#ifndef _FITS_C_
#define _FITS_C_

#include "allegro.h"
#include "xvin.h"

// below are gsl includes:
#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_sf.h>
#include <gsl/gsl_histogram.h>
        
/* If you include other plug-ins header do it here*/ 
// #include "../../include/fftw3.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "../hist/hist.h"
#include "fits.h"
#include "fits_exp.h"
#include "fits_power.h"
#include "fits_gumbel.h"
#include "fits_BHP.h"
#include "fits_chi.h"

#define FIT(i) gsl_vector_get(s->x, i)
#define ERR(i) sqrt(gsl_matrix_get(covar,i,i))


double  fits_dt=1., fits_t_min=0.; // don't forget to set this properly in every fit function below!

// required by the example from gsl:
void print_state (size_t iter, gsl_multifit_fdfsolver * s)
{
  printf ("iter: %3u x = % 15.8f % 15.8f % 15.8f "
          "|f(x)| = %g\n",
          iter,
          gsl_vector_get (s->x, 0), 
          gsl_vector_get (s->x, 1),
          gsl_vector_get (s->x, 2), 
          gsl_blas_dnrm2 (s->f));
}

 


// The main part of the program sets up a Levenberg-Marquardt solver 
// and some simulated random data. The data uses the known parameters 
// (1.0,5.0,0.1) combined with gaussian noise (standard deviation = 0.1) 
// over a range of 40 timesteps. The initial guess for the parameters is chosen as (0.0, 1.0, 0.0).
int do_fit_exponential(void)
{
	O_p 	*op1 = NULL;
	d_s 	*ds1, *ds2;
	pltreg *pr = NULL;
	int	index;
	double	a, b, lambda, da, db, dlambda;
	size_t	n_iteration = 0, N_iteration_max = 500;
	
	const gsl_multifit_fdfsolver_type *T;
	gsl_multifit_fdfsolver *s;
	int status;
	size_t i;
	size_t n;				// number of points in the dataset to fit
	const size_t p = 3;		// number of parameters
	gsl_matrix *covar = gsl_matrix_alloc (p, p);
	double *y;
	double dt;
//	double *sigma;
	double precision=1.0e-5;	
	struct data d;
  	gsl_multifit_function_fdf f;
	double params_init[3] = { 1.0, 0.0, 0.0 };
	gsl_vector_view       params;
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine fits a dataset according to\n\n"
					"{\\pt14 y(t) = a.exp(\\lambda t) + b}\n\n"
					"A Levenberg-Marquardt solver (from GSL) is used.\n"
					"a new dataset is created with the fit.");
	}
	
  	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	n = ds1->ny;	// number of points in time series
	dt = fabs((double)(ds1->xd[n-1]-ds1->xd[0])/((double)(n-1)));
	if ( fabs(dt-fabs((double)(ds1->xd[2]-ds1->xd[1]))) > (dt*precision) ) 
       if (win_printf("dt is not constant!\n %g vs %g\n\n click CANCEL to abort", 
                              fabs(dt-(ds1->xd[2]-ds1->xd[1])), dt*precision*n)==CANCEL)
          return(D_O_K);
	if ( fabs(dt-fabs((double)ds1->xd[1]-ds1->xd[0])) > (dt*precision) ) 
       if (win_printf("dt is not constant!\n %g vs %g\n\n click CANCEL to abort", 
                                fabs(dt-(ds1->xd[1]-ds1->xd[0])), dt*precision*n)==CANCEL)
          return(D_O_K);
          
    fits_dt    = dt;
    fits_t_min = ds1->xd[0]; 
	
	/* This is the data to be fitted: */
	y     = (double*)calloc(n, sizeof(double));
//	sigma = (double*)calloc(n, sizeof(double));
	for (i=0; i<n; i++)
    	{	y[i] = ds1->yd[i];
   // sigma[i] = 0.1; // unknow indeed...
    	}
    	
    	d.n = n;
    	d.y = y;

    	//initial guesses:
	b      = (double)ds1->yd[n-1];       // last y-value
	a      = (double)ds1->yd[0] - b;      // first y-value
	lambda = 1./(double)(ds1->xd[n-1]-ds1->xd[0]); // time lag of the fit interval
	params_init[0] = a;
	params_init[1] = lambda;
	params_init[2] = b;

	    	
      // gsl initializations:
	params = gsl_vector_view_array (params_init, p);


	f.f   = &expb_f;
  	f.df  = &expb_df;
  	f.fdf = &expb_fdf;
  	f.n = n;			// number of data points
  	f.p = p;			// number of parameters (to fit)
  	f.params = &d; 	// the data
  	// end of gsl initializations
		
	T = gsl_multifit_fdfsolver_lmsder;
  	s = gsl_multifit_fdfsolver_alloc (T, n, p);
  	gsl_multifit_fdfsolver_set (s, &f, &params.vector);

	do
	{	n_iteration++;
      	status = gsl_multifit_fdfsolver_iterate (s);
		if (status) break;
      	status = gsl_multifit_test_delta (s->dx, s->x, precision, precision);
	}
	while (status == GSL_CONTINUE && n_iteration < N_iteration_max);
		
	gsl_multifit_covar (s->J, 0.0, covar);
	

	a      = FIT(0);	da      = ERR(0);
	lambda = FIT(1); 	dlambda = ERR(1);
    b      = FIT(2);	db      = ERR(2);

     double chi = gsl_blas_dnrm2(s->f);
     chi = pow(chi, 2.0);
     win_printf_OK("status = %s\n\n %d iterations\n\n \\chi^2 = %g\n\\chi^2 per dof = %g\n", 
     			gsl_strerror (status), n_iteration, (float)(chi), (float)(chi/(n - p)));

	gsl_multifit_fdfsolver_free (s);
	
	ds2 = create_and_attach_one_ds (op1, n, n, 0);
	for (i=0; i<n; i++)
	{	ds2->xd[i] = ds1->xd[i];
		ds2->yd[i] = (float)( a*exp(-lambda*ds2->xd[i]) + b);
	}
 
	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"exponential fit");
		
	set_ds_plot_label(ds2, ds2->xd[(int)(n/2)], ds2->yd[(int)(n/2)], USR_COORD,
				"\\fbox{\\pt10\\stack{{y(t) = a.exp(-\\lambda t) + b}"
				"{a = %g +/- %g}"
				"{b = %g +/- %g}"
				"{\\lambda=%g +/- %g}"
				"{\\chi^2 = %g, per dof = %g\n}}}", 
				a, da, b, db, lambda, dlambda, (float)chi, (float)(chi/(n - p)) );

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_fit_exponential






// The main part of the program sets up a Levenberg-Marquardt solver 
// and some simulated random data. The data uses the known parameters 
// (1.0,5.0,0.1) combined with gaussian noise (standard deviation = 0.1) 
// over a range of 40 timesteps. The initial guess for the parameters is chosen as (0.0, 1.0, 0.0).
int do_fit_power(void)
{
	O_p 	*op1 = NULL;
	d_s 	*ds1, *ds2;
	pltreg *pr = NULL;
	int	index, k,l;
	double	A, q, B, dA, dq, dB;
	float   fA, fB, fq, fprecis;
	size_t	n_iteration = 0, N_iteration_max = 500;
	
	const gsl_multifit_fdfsolver_type *T;
	gsl_multifit_fdfsolver *s;
	int status;
	size_t i;
	size_t n;				// number of points in the dataset to fit
	const size_t p = 3;		// number of parameters
	gsl_matrix *covar = gsl_matrix_alloc (p, p);
	double *y;
	double dt;
//	double *sigma;
	double precision=1.0e-5;	
	struct data d;
  	gsl_multifit_function_fdf f;
	double params_init[3] = { 1.0, 0.0, 0.0 };
	gsl_vector_view       params;
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine fits a dataset according to\n\n"
					"{\\pt14 y(t) = A + \\frac{B}{t^q} = A + B.t^{-q}\n\n"
					"exponant q can be imposed, or fitted.\n"
					"A Levenberg-Marquardt solver (from GSL) is used.\n"
					"a new dataset is created with the fit.");
	}
  	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	n = ds1->ny;	// number of points in time series
	
	l=0; // number of negative or vanishing points
	for (k=0; k<n; k++) { if (ds1->xd[k]<=0) l++; }
	if (l>0) return(win_printf_OK("there are %d points with"
				"negative or vanishing abscissa.\n Aborting...", l));
	
	
	dt = fabs((double)(ds1->xd[n-1]-ds1->xd[0])/((double)(n-1)));
	if ( fabs(dt-fabs((double)(ds1->xd[2]-ds1->xd[1]))) > (dt*precision) ) 
       if (win_printf("dt is not constant!\n %g vs %g\n\n click CANCEL to abort", 
                              fabs(dt-(ds1->xd[2]-ds1->xd[1])), dt*precision*n)==CANCEL)
          return(D_O_K);
	if ( fabs(dt-fabs((double)ds1->xd[1]-ds1->xd[0])) > (dt*precision) ) 
       if (win_printf("dt is not constant!\n %g vs %g\n\n click CANCEL to abort", 
                                fabs(dt-(ds1->xd[1]-ds1->xd[0])), dt*precision*n)==CANCEL)
          return(D_O_K);
          
     fits_dt    = dt;
     fits_t_min = ds1->xd[0]; 
	
	/* This is the data to be fitted: */
	y     = (double*)calloc(n, sizeof(double));
	for (i=0; i<n; i++)	y[i] = (double)ds1->yd[i];
    	
    	d.n = n;
    	d.y = y;

    	//initial guesses:
	fA = ds1->yd[n-1];       // last y-value
	fq = (float)power_fit_exponant;				// we guess exponent 1
	if ((ds1->xd[1]*ds1->xd[n-1])!=0)
		 fB = (ds1->yd[n-1]-ds1->yd[1]) / ( 1./ds1->xd[n-1] - 1./ds1->xd[1]); // an educated guess...
	else fB = ds1->yd[1];
	
	fprecis = (float)precision;
	bool_power_fit_exponant=1-bool_power_fit_exponant;
	if (win_scanf("{\\pt14\\color{yellow}fit y(t) = A + B t^{-q}}\n\n"
                  "I will fit A and B and\n"
                  "%R fit exponant q\n"
                  "%r impose exponant q=%4f\n\n"
                  "initial guesses:\n"
                  "A = %6f B=%6f q=%4f\n\n"
                  "max number of iterations : %6d\n"
                  "precision               : %7f", 
       &bool_power_fit_exponant, &power_fit_exponant, &fA, &fB, &fq, &N_iteration_max, &fprecis)==CANCEL) return(D_O_K);
	
	bool_power_fit_exponant=1-bool_power_fit_exponant;
	precision=(double)fprecis;
	
	params_init[0] = (double)fA;
	params_init[1] = (double)fq;
	params_init[2] = (double)fB;
	    	
      // gsl initializations:
	params = gsl_vector_view_array (params_init, p);

	f.f   = &power_f;
  	f.df  = &power_df;
  	f.fdf = &power_fdf;
  	f.n = n;			// number of data points
  	f.p = p;			// number of parameters (to fit)
  	f.params = &d; 	// the data
  	// end of gsl initializations
		
	T = gsl_multifit_fdfsolver_lmsder;
  	s = gsl_multifit_fdfsolver_alloc (T, n, p);
  	gsl_multifit_fdfsolver_set (s, &f, &params.vector);

	do
	{	n_iteration++;
      	status = gsl_multifit_fdfsolver_iterate (s);
		if (status) break;
      	status = gsl_multifit_test_delta (s->dx, s->x, precision, precision);
	}
	while (status == GSL_CONTINUE && n_iteration < N_iteration_max);
		
	gsl_multifit_covar (s->J, 0.0, covar);
	
	A = FIT(0);	dA = ERR(0);
	q = FIT(1); dq = ERR(1);
    B = FIT(2);	dB = ERR(2);

     double chi = gsl_blas_dnrm2(s->f);
     chi = pow(chi, 2.0);
     win_printf_OK("status = %s\n\n %d iterations\n\n \\chi^2 = %g\n\\chi^2 per dof = %g\n", 
     			gsl_strerror (status), n_iteration, (float)(chi), (float)(chi/(n - p)));

	gsl_multifit_fdfsolver_free (s);
	
	ds2 = create_and_attach_one_ds (op1, n, n, 0);
	for (i=0; i<n; i++)
	{	ds2->xd[i] = ds1->xd[i];
		ds2->yd[i] = (float)( A + B * pow(ds2->xd[i], -q));
	}
 
	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"power law fit : y = A + B.x^{-q}");
		
	set_ds_plot_label(ds2, ds2->xd[(int)(n/2)], ds2->yd[(int)(n/2)], USR_COORD,
				"\\fbox{\\pt10\\stack{{y(t) = a + \\frac{b}{t^q}}"
				"{a = %g +/- %g}"
				"{b = %g +/- %g}"
				"{q = %g +/- %g %s}"
				"{\\chi^2 = %g, per dof = %g\n}}}", 
				A, dA, B, dB, q, dq, (bool_power_fit_exponant==0) ? "(imposed)" : "(fit)",
                (float)chi, (float)(chi/(n - p)) );

	refresh_plot(pr, UNCHANGED);
	return(D_O_K);
} // end of the do_fit_power





// The main part of the program sets up a Levenberg-Marquardt solver 
// and some simulated random data. The data uses the known parameters 
// (1.0,5.0,0.1) combined with gaussian noise (standard deviation = 0.1) 
// over a range of 40 timesteps. The initial guess for the parameters is chosen as (0.0, 1.0, 0.0).
int do_fit_gumbel(void)
{
	O_p 	*op1 = NULL;
	d_s 	*ds1, *ds2;
	pltreg *pr = NULL;
	int	index;
	double	a, b, m, da, db, dm;
	size_t	n_iteration = 0, N_iteration_max = 5000;
	
	const gsl_multifit_fdfsolver_type *T;
	gsl_multifit_fdfsolver *s;
	int status;
	size_t i;
	size_t n;				// number of points in the dataset to fit
	const size_t p = 3;		// number of parameters
	gsl_matrix *covar = gsl_matrix_alloc (p, p);
	double *y;
	double dt;
//	double *sigma;
	double precision=1.0e-5;
	float 	max_x, max_y, l_a, l_b, l_m;
	gsl_multifit_function_fdf f;
	
	struct data d;
	gsl_vector_view params;
	double params_init[3] = { 1.0, 0.5, 0.0 };
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine fits a dataset according to\n\n"
					"{\\pt14 y(t) = ab exp(-(b*exp(-a(x-m))+a*(x-m))))\n\n"
					"it takes the log itself, so don''t do it\n"
					"A Levenberg-Marquardt solver (from GSL) is used.\n"
					"a new dataset is created with the fit.");
	}
	
  	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	n = ds1->ny;	// number of points in time series
	
	dt = fabs((double)(ds1->xd[n-1]-ds1->xd[0])/((double)(n-1)));
	if ( fabs(dt-fabs((double)(ds1->xd[2]-ds1->xd[1]))) > (dt*precision) ) 
       if (win_printf("dt is not constant!\n %g vs %g\n\n click CANCEL to abort", 
                              fabs(dt-(ds1->xd[2]-ds1->xd[1])), dt*precision*n)==CANCEL)
          return(D_O_K);
	if ( fabs(dt-fabs((double)ds1->xd[1]-ds1->xd[0])) > (dt*precision) ) 
       if (win_printf("dt is not constant!\n %g vs %g\n\n click CANCEL to abort", 
                                fabs(dt-(ds1->xd[1]-ds1->xd[0])), dt*precision*n)==CANCEL)
          return(D_O_K);
          
    fits_dt    = dt;
    fits_t_min = ds1->xd[0]; 

	/* This is the data to be fitted: */
	y     = (double*)calloc(n, sizeof(double));
    status=0;
	for (i=0; i<n; i++)
   	{	y[i] = (double)ds1->yd[i];
   	    if (y[i]>0) y[i] = log(y[i]);
   	    else       
        {           y[i] = log(precision);
                    status++;
        }
   	}
   
   	if (status!=0) win_printf("%d points in the histogram were 0-valued, log(0) was replaced by %g\n"
                                  "I keep going, but this may result in a poor fit...", 
                                  status, log(precision));
   	d.n = n;
    d.y = y;

   //initial guesses:
	max_x=ds1->xd[0];
	max_y=ds1->yd[0];
	for (i=1; i<ds1->nx; i++)
	{	if (ds1->yd[i] > max_y)
		{		max_y=ds1->yd[i];
				max_x=ds1->xd[i];
		}
	}
   	params_init[0] = 1.;
	params_init[1] = 1.;
	params_init[2] = max_x;
	l_a=(float)params_init[0];
	l_b=(float)params_init[1];
	l_m=(float)params_init[2];
	if (bool_fit_BHP_also_m==1) fits_BHP_m=l_m;
	
	i=win_scanf("{\\pt14\\color{yellow} Initial parameters for (a,b,m) \n"
				"p(x)=abs(a.b)  exp (-(b*exp(-a*(x-m))+a*(x-m)))}\n"
				"a = %8f\n"
				"a>0 if exponential tail is for x>m\n"
				"a<0 if exponential tail is for x<m\n"
				"b = %8f (b >0)\n "
				"m = %8f\n\n"
				"%b fit also m (unclicking will force m=0)",
				&l_a, &l_b, &l_m, &bool_fit_gumbel_also_m);
	if (i==CANCEL) return(OFF);
	if (l_b<=0) win_printf_OK("b must be positive");
	if (l_a==0) win_printf_OK("a must be non zero");
	params_init[0] = (double)l_a;
	params_init[1] = (double)l_b;
	params_init[2] = (double)l_m;
	
   	
	//gsl initializations:
	params = gsl_vector_view_array (params_init, p);

	f.f   = &Gumbel_f;
  	f.df  = &Gumbel_df;
  	f.fdf = &Gumbel_fdf;
  	f.n = n;			// number of data points
  	f.p = p;			// number of parameters (to fit)
	f.params = &d; 	// the data
  	// end of gsl initializations
		
	T = gsl_multifit_fdfsolver_lmsder;
  	s = gsl_multifit_fdfsolver_alloc (T, n, p);
  	gsl_multifit_fdfsolver_set (s, &f, &params.vector);

	do
	{	n_iteration++;
      	status = gsl_multifit_fdfsolver_iterate (s);
		if (status) break;
      	status = gsl_multifit_test_delta (s->dx, s->x, precision, precision);
	}
	while (status == GSL_CONTINUE && n_iteration < N_iteration_max);
		
	gsl_multifit_covar (s->J, 0.0, covar);

	a  = FIT(0);	da = ERR(0);
	b  = FIT(1); 	db = ERR(1);
    m  = FIT(2);	dm = ERR(2);

     double chi = gsl_blas_dnrm2(s->f);
     chi = pow(chi, 2.0);
     win_printf_OK("status = %s\n\n %d iterations\n\n \\chi^2 = %g\n\\chi^2 per dof = %g\n", 
     			gsl_strerror (status), n_iteration, (float)(chi), (float)(chi/(n - p)));

	gsl_multifit_fdfsolver_free (s);
	
	ds2 = create_and_attach_one_ds (op1, n, n, 0);
	for (i=0; i<n; i++)
	{	ds2->xd[i] = ds1->xd[i];
		ds2->yd[i] = (float)(fabs(a*b)*exp(-(b*exp(-a*(ds2->xd[i]-m))+a*(ds2->xd[i]-m))));
	}
 
	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"Gumbel fit");
		
	set_ds_plot_label(ds2, ds2->xd[(int)(n/2)], ds2->yd[(int)(n/2)], USR_COORD,
				"\\fbox{\\pt10\\stack{{y(t) = abs(a.b).exp(-(b.exp(-a(x-m))+a*(x-m)))}"
				"{a = %g +/- %g}"
				"{b = %g +/- %g}"
				"{m = %g +/- %g}"
				"{\\chi^2 = %g, per dof = %g\n}}}", 
				a, da, b, db, m, dm, (float)chi, (float)(chi/(n - p)) );

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_fit_gumbel





// The main part of the program sets up a Levenberg-Marquardt solver 
// and some simulated random data. The data uses the known parameters 
// (1.0,5.0,0.1) combined with gaussian noise (standard deviation = 0.1) 
// over a range of 40 timesteps. The initial guess for the parameters is chosen as (0.0, 1.0, 0.0).
int do_fit_BHP(void)
{
	O_p 	*op1 = NULL;
	d_s 	*ds1, *ds2;
	pltreg *pr = NULL;
	int	index;
	double	a, b, m, da, db, dm;
	size_t	n_iteration = 0, N_iteration_max = 5000;
	
	const gsl_multifit_fdfsolver_type *T;
	gsl_multifit_fdfsolver *s;
	int status;
	size_t i;
	size_t n;				// number of points in the dataset to fit
	const size_t p = 3;		// number of parameters
	gsl_matrix *covar = gsl_matrix_alloc (p, p);
	double *y;
	double dt;
//	double *sigma;
	double precision=1.0e-5;
	float 	max_x, max_y, l_a, l_b, l_m;
	gsl_multifit_function_fdf f;
	
	struct data d;
	gsl_vector_view params;
	double params_init[3] = { 1.0, 0.5, 0.0 };
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine fits a dataset according to\n\n"
					"{\\pt14 y(t) = a^a*b/\\Gamma(a) exp(a*b*(x-m)-b*exp(-a(x-m)))}\n\n"
					"it takes the log itself, so don''t do it\n"
					"A Levenberg-Marquardt solver (from GSL) is used.\n"
					"a new dataset is created with the fit.");
	}
	
  	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	n = ds1->ny;	// number of points in time series
	
	dt = fabs((double)(ds1->xd[n-1]-ds1->xd[0])/((double)(n-1)));
	if ( fabs(dt-fabs((double)(ds1->xd[2]-ds1->xd[1]))) > (dt*precision) ) 
       if (win_printf("dt is not constant!\n %g vs %g\n\n click CANCEL to abort", 
                              fabs(dt-(ds1->xd[2]-ds1->xd[1])), dt*precision*n)==CANCEL)
          return(D_O_K);
	if ( fabs(dt-fabs((double)ds1->xd[1]-ds1->xd[0])) > (dt*precision) ) 
       if (win_printf("dt is not constant!\n %g vs %g\n\n click CANCEL to abort", 
                                fabs(dt-(ds1->xd[1]-ds1->xd[0])), dt*precision*n)==CANCEL)
          return(D_O_K);
          
    fits_dt    = dt;
    fits_t_min = ds1->xd[0]; 

	/* This is the data to be fitted: */
	y     = (double*)calloc(n, sizeof(double));
    status=0;
	for (i=0; i<n; i++)
   	{	y[i] = (double)ds1->yd[i];
   	    if (y[i]>0) y[i] = log(y[i]);
   	    else       
        {           y[i] = log(precision);
                    status++;
        }
   	}
   
   	if (status!=0) win_printf("%d points in the histogram were 0-valued, log(0) was replaced by %g\n"
                                  "I keep going, but this may result in a poor fit...", 
                                  status, log(precision));
   	d.n = n;
    d.y = y;

   //initial guesses:
	max_x=ds1->xd[0];
	max_y=ds1->yd[0];
	for (i=1; i<ds1->nx; i++)
	{	if (ds1->yd[i] > max_y)
		{		max_y=ds1->yd[i];
				max_x=ds1->xd[i];
		}
	}
   	params_init[0] = 1.;
	params_init[1] = 1.;
	params_init[2] = max_x;
	l_a=(float)params_init[0];
	l_b=(float)params_init[1];
	l_m=(float)params_init[2];
	
	i=win_scanf("{\\pt14\\color{yellow} Initial parameters for (a,b,m) \n\n"
				"p(x)=\\frac{a^a.b}{\\Gamma(a)} exp(a.b.(x-m)-a.exp(b(x-m)))}\n\n"
				"a = %8f (a>0)\n"
				"b = %8f\n"
				"b>0 if exponential tails is for x<m\n"
				"b<0 if exponential tails is for x>m\n"
				"m = %8f\n\n"
				"%b fit also m (unclicking will force m above)",
				&l_a, &l_b, &l_m, &bool_fit_BHP_also_m);
	if (i==CANCEL) return(OFF);
	if (l_a<=0) win_printf_OK("a must be positive");
	if (l_b==0) win_printf_OK("b must be non zero");
	params_init[0] = (double)l_a;
	params_init[1] = (double)l_b;
	params_init[2] = (double)l_m;
   	if (bool_fit_BHP_also_m==1) fits_BHP_m=l_m;
   	
	//gsl initializations:
	params = gsl_vector_view_array (params_init, p);

	f.f   = &BHP_f;
  	f.df  = &BHP_df;
  	f.fdf = &BHP_fdf;
  	f.n = n;			// number of data points
  	f.p = p;			// number of parameters (to fit)
	f.params = &d; 	// the data
  	// end of gsl initializations
		
	T = gsl_multifit_fdfsolver_lmsder;
  	s = gsl_multifit_fdfsolver_alloc (T, n, p);
  	gsl_multifit_fdfsolver_set (s, &f, &params.vector);

	do
	{	n_iteration++;
      	status = gsl_multifit_fdfsolver_iterate (s);
		if (status) break;
      	status = gsl_multifit_test_delta (s->dx, s->x, precision, precision);
	}
	while (status == GSL_CONTINUE && n_iteration < N_iteration_max);
		
	gsl_multifit_covar (s->J, 0.0, covar);

	a  = FIT(0);	da = ERR(0);
	b  = FIT(1); 	db = ERR(1);
    m  = FIT(2);	dm = ERR(2);

     double chi = gsl_blas_dnrm2(s->f);
     chi = pow(chi, 2.0);
     win_printf_OK("status = %s\n\n %d iterations\n\n \\chi^2 = %g\n\\chi^2 per dof = %g\n", 
     			gsl_strerror (status), n_iteration, (float)(chi), (float)(chi/(n - p)));

	gsl_multifit_fdfsolver_free (s);
	
	ds2 = create_and_attach_one_ds (op1, n, n, 0);
	for (i=0; i<n; i++)
	{	ds2->xd[i] = ds1->xd[i];
		ds2->yd[i] = (float)(pow(a,a)*fabs(b)/gsl_sf_gamma(a))*exp(a*b*(ds1->xd[i]-m)-a*exp(b*(ds1->xd[i]-m)));
	}
 
	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"BHP fit");
		
	set_ds_plot_label(ds2, ds2->xd[(int)(n/2)], ds2->yd[(int)(n/2)], USR_COORD,
				"\\fbox{\\pt10\\stack{{y(t) = \\frac{a^{a}.b}{\\Gamma(a)}exp(a.b.(x-m)-a.exp(b.(x-m)))}"
				"{a = %g +/- %g}"
				"{b = %g +/- %g}"
				"{m = %g +/- %g}"
				"{\\chi^2 = %g, per dof = %g\n}}}", 
				a, da, b, db, m, dm, (float)chi, (float)(chi/(n - p)) );

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_fit_BHP

// The main part of the program sets up a Levenberg-Marquardt solver 
// and some simulated random data. The data uses the known parameters 
// (1.0,5.0,0.1) combined with gaussian noise (standard deviation = 0.1) 
// over a range of 40 timesteps. The initial guess for the parameters is chosen as (0.0, 1.0, 0.0).
int do_fit_chi(void)
{
	O_p 	*op1 = NULL;
	d_s 	*ds1, *ds2;
	pltreg *pr = NULL;
	int	index;
	double	a, da;
	size_t	n_iteration = 0, N_iteration_max = 5000;
	
	const gsl_multifit_fdfsolver_type *T;
	gsl_multifit_fdfsolver *s;
	int status;
	size_t i;
	size_t n;				// number of points in the dataset to fit
	const size_t p = 1;		// number of parameters
	gsl_matrix *covar = gsl_matrix_alloc (p, p);
	double *y;
	double dt;
//	double *sigma;
	double precision=1.0e-5;
	float 	l_a;
	gsl_multifit_function_fdf f;
	gsl_histogram	*hist;
	struct data d;
	gsl_vector_view params;
	double params_init[1] = { 1.0 };
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine fits a dataset according to\n\n"
					"{\\pt14 y(t) = \frac{1}{2^{a/2}\\Gamma(a/2)}exp(-(x-a)/2).(x-a)^{a/2-1}}\n\n"
					"Mean is supposed to be null\n"
					"it takes the log itself, so don''t do it\n"
					"A Levenberg-Marquardt solver (from GSL) is used.\n"
					"a new dataset is created with the fit.");
	}
	
  	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	n = ds1->ny;	// number of points in time series
	
	dt = fabs((double)(ds1->xd[n-1]-ds1->xd[0])/((double)(n-1)));
	if ( fabs(dt-fabs((double)(ds1->xd[2]-ds1->xd[1]))) > (dt*precision) ) 
       if (win_printf("dt is not constant!\n %g vs %g\n\n click CANCEL to abort", 
                              fabs(dt-(ds1->xd[2]-ds1->xd[1])), dt*precision*n)==CANCEL)
          return(D_O_K);
	if ( fabs(dt-fabs((double)ds1->xd[1]-ds1->xd[0])) > (dt*precision) ) 
       if (win_printf("dt is not constant!\n %g vs %g\n\n click CANCEL to abort", 
                                fabs(dt-(ds1->xd[1]-ds1->xd[0])), dt*precision*n)==CANCEL)
          return(D_O_K);
          
    fits_dt    = dt;
    fits_t_min = ds1->xd[0]; 

	/* This is the data to be fitted: */
	y     = (double*)calloc(n, sizeof(double));
    status=0;
	for (i=0; i<n; i++)
   	{	y[i] = (double)ds1->yd[i];
   	    if (y[i]>0) y[i] = log(y[i]);
   	    else       
        {           y[i] = log(precision);
                    status++;
        }
   	}
   
   	if (status!=0) win_printf("%d points in the histogram were 0-valued, log(0) was replaced by %g\n"
                                  "I keep going, but this may result in a poor fit...", 
                                  status, log(precision));
   	d.n = n;
    d.y = y;

   //initial guesses:
   //loi du chi2 la variance vaut 2n...
   	ds_to_histogram(ds1, &hist);
    params_init[0] = gsl_histogram_sigma ( hist)*gsl_histogram_sigma ( hist)/2.;
	l_a=(float)params_init[0];
	
	i=win_scanf("{\\pt14\\color{yellow} Initial parameters for (a) \n\n"
				"mean of PDF is supposed to be null\n\n"
				"p(x)=\\frac{1}{2^{a/2}\\Gamma(a/2)}exp(-(x-a)/2).(x-a)^{a/2-1}}\n\n"
				"a = %8f (=variance/2, by default) \n\n",
				&l_a);
	if (i==CANCEL) return(OFF);
	if (l_a<0) win_printf_OK("a must be positive");
	params_init[0] = (double)l_a;
	
	//gsl initializations:
	params = gsl_vector_view_array (params_init, p);

	f.f   = &chi_f;
  	f.df  = &chi_df;
  	f.fdf = &chi_fdf;
  	f.n = n;			// number of data points
  	f.p = p;			// number of parameters (to fit)
	f.params = &d; 	// the data
  	// end of gsl initializations
		
	T = gsl_multifit_fdfsolver_lmsder;
  	s = gsl_multifit_fdfsolver_alloc (T, n, p);
  	gsl_multifit_fdfsolver_set (s, &f, &params.vector);

	do
	{	n_iteration++;
      	status = gsl_multifit_fdfsolver_iterate (s);
		if (status) break;
      	status = gsl_multifit_test_delta (s->dx, s->x, precision, precision);
	}
	while (status == GSL_CONTINUE && n_iteration < N_iteration_max);
		
	gsl_multifit_covar (s->J, 0.0, covar);

	a  = FIT(0);	da = ERR(0);
	
     double chi = gsl_blas_dnrm2(s->f);
     chi = pow(chi, 2.0);
     win_printf_OK("status = %s\n\n %d iterations\n\n \\chi^2 = %g\n\\chi^2 per dof = %g\n", 
     			gsl_strerror (status), n_iteration, (float)(chi), (float)(chi/(n - p)));

	gsl_multifit_fdfsolver_free (s);
	
	ds2 = create_and_attach_one_ds (op1, n, n, 0);
	for (i=0; i<n; i++)
	{	ds2->xd[i] = ds1->xd[i];
		ds2->yd[i] = (float)(1./pow(2.,a/2.)*1./gsl_sf_gamma(a/2.)*exp(-(ds2->xd[i]+a)/2.)*pow((ds2->xd[i]+a),a/2.-1));
	}
 
	inherit_from_ds_to_ds(ds2, ds1);
	ds2->treatement = my_sprintf(ds2->treatement,"BHP fit");
		
	set_ds_plot_label(ds2, ds2->xd[(int)(n/2)], ds2->yd[(int)(n/2)], USR_COORD,
				"\\fbox{\\pt10\\stack{{y(t) = \\frac{1}{2^{a/2}\\Gamma(a/2)}exp(-(x+a)/2).(x+a)^{a/2-1}}"
				"{a = %g +/- %g}"
				"{\\chi^2 = %g, per dof = %g\n}}}", 
				a, da, (float)chi, (float)(chi/(n - p)) );

	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_fit_chi_squared



MENU *fits_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Fit exponential", 	do_fit_exponential,	      NULL, 0, NULL);	
	add_item_to_menu(mn,"Fit power law", 	do_fit_power,		      NULL, 0, NULL);	
	add_item_to_menu(mn,"\0",				0,			      NULL, 0, NULL);	
	add_item_to_menu(mn,"Fit PDF Gumbel", 		do_fit_gumbel,       NULL, 0, NULL);	
	add_item_to_menu(mn,"Fit PDF BHP", 		do_fit_BHP,          NULL, 0, NULL);	
	add_item_to_menu(mn,"Fit PDF chi(n)", 		do_fit_chi,          NULL, 0, NULL);		
	
	return mn;
}

int	fits_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("fits", NULL, fits_plot_menu(), 0, NULL);
	return D_O_K;
}


int	fits_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"fits",	NULL, NULL);
	return D_O_K;
}
#endif

