#ifndef _FITS_BHP_C_
#define _FITS_BHP_C_

// #include "allegro.h"
#include "xvin.h"

// below are gsl includes:
// #include <stdlib.h>
// #include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
// #include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_sf.h>

/* If you include other plug-ins header do it here*/ 
// #include "../../include/fftw3.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "fits.h"
#include "fits_BHP.h"
#include "fits_gumbel.h"

int		bool_fit_BHP_also_m=0;
double 	fits_BHP_m=0.;

// required by the example from gsl:
int BHP_f (const gsl_vector *x, void *params, gsl_vector *f)
{
  size_t n = ((struct data *)params)->n;
  double *y = ((struct data *)params)->y;
//  double *sigma = ((struct data *) params)->sigma;

  double a = gsl_vector_get (x, 0);
  double b = gsl_vector_get (x, 1);
  double m = gsl_vector_get (x, 2);
  if (bool_fit_BHP_also_m==0) m=fits_BHP_m;
  size_t i;

  for (i = 0; i < n; i++)
    {
      /* Model Yi = Model Yi = a*ln(a)+ln(b)-ln(Gamma(a))+a*b*(i-m)-a*exp(b*(i-m)) */
      double t = (double)i*fits_dt + fits_t_min;
      double Yi = a*log(a) + log(fabs(b)) - gsl_sf_lngamma(a)+a*b*(t-m)-a*exp(b*(t-m));
      gsl_vector_set (f, i, (Yi - y[i]) /* /sigma[i] */ );
    }

  return GSL_SUCCESS;
}


// required by the example from gsl:
int BHP_df (const gsl_vector * x, void *params, gsl_matrix *J)
{
  size_t n = ((struct data *)params)->n;
//  double *sigma = ((struct data *) params)->sigma;

  double a = gsl_vector_get (x, 0);
  double b = gsl_vector_get (x, 1);
  double m = gsl_vector_get (x, 2);
  if (bool_fit_BHP_also_m==0) m=fits_BHP_m;
  size_t i;
  for (i = 0; i < n; i++)
    {
      /* Jacobian matrix J(i,j) = dfi / dxj, */
      /* where fi = (Yi - yi)/sigma[i],      */
      /*       Yi = A * exp(-B * i) + b  */
      /* and the xj are the parameters (A,B,b) */
      double t = (double)i*fits_dt + fits_t_min;
//      double s = sigma[i];
      double e = exp(b*(t-m));
      gsl_matrix_set (J, i, 0, log(a)+1.- gsl_sf_psi(a)+b*(t-m)-e /* /s */ ); 
      gsl_matrix_set (J, i, 1, 1./fabs(b)+a*(t-m) - a*(t-m)*e /* /s */);
      if (bool_fit_BHP_also_m==0) 
      			gsl_matrix_set (J, i, 2, 0. /* /s */);
	  else		
	  		 	gsl_matrix_set (J, i, 2, -a*b+a*b*e /* /s */);

    }
  return GSL_SUCCESS;
}


// required by the example from gsl:
int BHP_fdf (const gsl_vector *x, void *params, gsl_vector *f, gsl_matrix *J)
{
  BHP_f (x, params, f);
  BHP_df (x, params, J);

  return GSL_SUCCESS;
}

#endif
