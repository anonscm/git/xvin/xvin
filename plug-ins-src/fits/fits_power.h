#ifndef _FITS_POWER_H_
#define _FITS_POWER_H_

int power_f   (const gsl_vector *x, void *params, gsl_vector *f);
int power_df  (const gsl_vector *x, void *params, gsl_matrix *J);
int power_fdf (const gsl_vector *x, void *params, gsl_vector *f, gsl_matrix *J);


extern int   bool_power_fit_exponant;
extern float power_fit_exponant;

#endif
