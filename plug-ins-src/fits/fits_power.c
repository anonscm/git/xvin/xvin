#ifndef _FITS_POWER_C_
#define _FITS_POWER_C_

// #include "allegro.h"
#include "xvin.h"

// below are gsl includes:
// #include <stdlib.h>
// #include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
// #include <gsl/gsl_multifit_nlin.h>

/* If you include other plug-ins header do it here*/ 
// #include "../../include/fftw3.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "fits.h"
#include "fits_power.h"

int   bool_power_fit_exponant=1;
float power_fit_exponant=(float)1.;
	

// required by the example from gsl:
int power_f (const gsl_vector *x, void *params, gsl_vector *f)
{
  size_t n  = ((struct data *)params)->n;
  double *y = ((struct data *)params)->y;
//  double *sigma = ((struct data *) params)->sigma;

  double A = gsl_vector_get (x, 0);
  double q = gsl_vector_get (x, 1);
  double B = gsl_vector_get (x, 2);
  if (bool_power_fit_exponant==0) // we don't fit the exponant, so we impose it
  {  q=power_fit_exponant; }
  
  size_t i;

  for (i = 0; i < n; i++)
    {
      /* Model Yi = A * exp(-lambda * i) + b */
      double t = (double)i*fits_dt + fits_t_min;
      double Yi = A + B * pow(t, -q);
      gsl_vector_set (f, i, (Yi - y[i]) /* /sigma[i] */ );
    }

  return GSL_SUCCESS;
}


// required by the example from gsl:
int power_df (const gsl_vector *x, void *params, gsl_matrix *J)
{
  size_t n = ((struct data *)params)->n;
//  double *sigma = ((struct data *) params)->sigma;

//  double A = gsl_vector_get (x, 0); // unused in derivatives
  double q = gsl_vector_get (x, 1);
  double B = gsl_vector_get (x, 2);
  if (bool_power_fit_exponant==0) // we don't fit the exponant, so we impose it
  {  q=power_fit_exponant; }
  
  size_t i;

  for (i = 0; i < n; i++)
    {
      /* Jacobian matrix J(i,j) = dfi / dxj, */
      /* where fi = (Yi - yi)/sigma[i],      */
      /*       Yi = A + B /(i^p)  */
      /* and the xj are the parameters (A,p,B) */
      double t = (double)i*fits_dt + (double)fits_t_min;
//      double s = sigma[i];
      double e = pow(t, -q);
      gsl_matrix_set (J, i, 0, 1.0 /* /s */ ); 
      gsl_matrix_set (J, i, 2, e   /* /s */);
      if (bool_power_fit_exponant==0) // we don't fit the exponant, so we don't vary it
      			gsl_matrix_set (J, i, 1, 0. /* /s */);
	  else		
	  		 	gsl_matrix_set (J, i, 1, -log(t)*B*e /* /s */);
    }
  return GSL_SUCCESS;
}


// required by the example from gsl:
int power_fdf (const gsl_vector *x, void *params, gsl_vector *f, gsl_matrix *J)
{
  power_f (x, params, f);
  power_df (x, params, J);

  return GSL_SUCCESS;
}

#endif
