#include "../pywrapper/core.h"
#include "../pywrapper/core.hpp"
#include "../trackBead/trackBead.h"
#include "../trackBead/action.h"
#include "../trackBead/track_util.h"
#include "../trackBead/magnetscontrol.h"
#include "color.h"
#include "plot_ds.h"
#include "gui.hh"
#include <numeric>
#include <thread>
#include <chrono>

namespace
{
    namespace bp = boost::python;

    struct Args
    {
        float f0         = 5.;
        float f1         = 15.;
        float athreshold = 0.1;
        float rthreshold = 0.1;
        int   cthreshold = 50;
        int   gaussian   = 3;
        int   contrast   = 25;
        int   nbeads     = 200;
        int   beaddist   = 15;
        int   width      = 25;
        int   center     = 1;
    };

    template <typename  ... T>
    inline auto _query(char const * others, T &&... elems)
    {
        static Args args;
        char tmp[2048];
        snprintf(tmp, sizeof(tmp),
                 "{\\color{yellow}Key parameters}\n"
                 "Force values: closed beads %%f, open beads %%f\n"
                 "Maximum number of beads: %%d\n"
                 "Minimum distance between beads: %%d\n"
                 "Fixed-beads threshold: %%d\n"
                 "%s\n"
                 "{\\color{yellow}Details}\n"
                 "Filter widths: gaussian %%d, contrast %%d\n"
                 "Peak thresholds: absolute %%f, relative %%f\n"
                 "Thumbnail size: %%d\n"
                 "Center peaks: %%b\n", others);

        if(config::query("BEAD-DETECTION", tmp,
                         config::item("OPEN-F",     args.f0),
                         config::item("CLOSED-F",   args.f1),
                         config::item("NUM-BEADS",  args.nbeads),
                         config::item("BEAD-DIST",  args.beaddist),
                         config::item("CANNY",      args.cthreshold),
                         elems...,
                         config::item("GAUSSIAN",   args.gaussian),
                         config::item("CONTRAST",   args.contrast),
                         config::item("PEAKS-ABS",  args.athreshold),
                         config::item("PEAKS-REL",  args.rthreshold),
                         config::item("WIDTH",      args.width),
                         config::item("CENTER",     args.center)))
            return std::make_tuple(args, false);
        return std::make_tuple(args, true);
    }

    auto _getimage(float z0)
    {
        using namespace std::chrono_literals;

        fill_next_available_action(track_info->imi[track_info->c_i]+10,
                                   MV_ZMAG_ABS, z0);
        while (std::abs(track_info->zmag[track_info->c_i]-z0) > 0.005)
            std::this_thread::sleep_for(1ms);

        auto const & img = oi_TRACK->im;
        auto const * ptr = (unsigned char const *) (img.mem[img.c_f]);
        std::vector<unsigned char> tmp(ptr, ptr+img.nx*img.ny);
        return pywrapper::topython(tmp, img.ny, img.nx);
    }

    using bead_t = std::vector<std::tuple<size_t, size_t, float>>;
    auto _peaks(Args const & args)
    {
        bp::dict info;
        info["nbeads"]     = args.nbeads;
        info["beaddist"]   = args.beaddist;
        info["gaussian"]   = args.gaussian;
        info["contrast"]   = args.contrast;
        info["athreshold"] = args.athreshold;
        info["rthreshold"] = args.rthreshold;
        info["width"]      = args.width;
        info["center"]     = args.center == 1;

        auto img0 = _getimage(convert_force_to_zmag(args.f0, 0));
        auto img1 = _getimage(convert_force_to_zmag(args.f1, 0));

        bead_t res;
        try
        {
            auto mod = pywrapper::import("beaddetector");
            auto out = mod.attr("findBeads")(img0, img1, info);
            for(bp::stl_input_iterator<bp::object> cit(out), e; cit != e; ++cit)
            {
                size_t a = bp::extract<size_t>((*cit)[1]);
                size_t b = bp::extract<size_t>((*cit)[0]);
                float  c = bp::extract<int>   ((*cit)[2]);
                res.emplace_back(std::make_tuple(a, b, c));
            }
        } catch (bp::error_already_set &)
        {
            std::string msg = "{\\color{yellow}CRASH OCCURRED!}\n";
            msg += pywrapper::exception();
            win_printf_OK(msg.c_str());
        }
        return res;
    }

    auto _nfixed(Args const & args, bead_t const & beads)
    {
        auto fcn = [&](auto i, auto const & b)
                   { return i+(std::get<2>(b) < args.cthreshold ? 1 : 0); };
        return std::accumulate(beads.begin(), beads.end(), 0u, fcn);
    }

    bool _exist(float x, float y, float nearness_radius)
    {

        for (int i = 0; i < track_info->n_b; ++i)
        {
            b_track *bt = track_info->bd[i];

            float x1 = x - bt->xc;
            float y1 = y - bt->yc;
            float r = sqrt(x1 * x1 + y1 * y1);

            if (r < nearness_radius)
            {
                return true;
            }
        }

        return false;
    }

}

int do_find_beads(void)
{
    if (updating_menu_state != 0)
        return D_O_K;

    auto query = _query("");
    if(!std::get<1>(query))
        return D_O_K;

    auto peaks = _peaks(std::get<0>(query));
    auto fixed = _nfixed(std::get<0>(query), peaks);
    auto good  = peaks.size() - fixed;
    return win_printf_OK("%d beads were found:\n- %d fixed\n- %d good\n",
                         int(peaks.size()), fixed, good);
}

int do_display_beads(void)
{
    if (updating_menu_state != 0)
        return D_O_K;

    auto query = _query("");
    if(!std::get<1>(query))
        return D_O_K;

    auto peaks = _peaks(std::get<0>(query));
    if(peaks.size() == 0)
        return win_printf_OK("No beads were found!");

    auto fixed = _nfixed(std::get<0>(query), peaks);
    auto good  = peaks.size() - fixed;

    imreg *imr = NULL;
    char tmp[] = "%im";
    if (ac_grep(NULL, tmp, &imr) < 1 || imr == NULL)
        return -1;

    auto &img = *oi_TRACK;
    auto  cio = create_and_attach_oi_to_imr(imr, img.im.nx, img.im.ny, IS_CHAR_IMAGE);
    duplicate_image_or_movie(&img, cio, 0);
    auto  op  = create_and_attach_op_to_oi(cio, good, good, 0, IM_SAME_X_AXIS + IM_SAME_Y_AXIS);

    char tmp1[] =  "good beads", tmp2[] = "\\fd", tmp3[] = "fixed beads";
    auto  dsG = op->dat[0];
    set_ds_source(dsG, tmp1);
    set_ds_point_symbol(dsG, tmp2);
    set_ds_dot_line(dsG);
    set_ds_line_color(dsG, Green);

    auto  dsB = create_and_attach_one_ds(op, fixed, fixed, 0);
    set_ds_source(dsG, tmp3);
    set_ds_point_symbol(dsG, tmp2);
    set_ds_dot_line(dsG);
    set_ds_line_color(dsG, Red);

    size_t iG = 0, iB = 0;
    auto cthr = std::get<0>(query).cthreshold;
    for(auto const & peak: peaks)
        if(std::get<2>(peak) > cthr)
        {
            dsG->xd[iG]   = std::get<0>(peak);
            dsG->yd[iG++] = std::get<1>(peak);
        }
        else
        {
            dsB->xd[iB]   = std::get<0>(peak);
            dsB->yd[iB++] = std::get<1>(peak);
        }

    cio->need_to_refresh = ALL_NEED_REFRESH;
    op ->need_to_refresh = ALL_NEED_REFRESH;
    return D_O_K;
}

int do_target_beads(void)
{
    if (updating_menu_state != 0)
        return D_O_K;

    static int len = 128, width = 16;
    auto query = _query("Crosses: length %6d, width %6d",
                        config::item("count_beads_crosses_length", len),
                        config::item("count_beads_crosses_width",  width));
    if(!std::get<1>(query))
        return D_O_K;

    auto peaks = _peaks(std::get<0>(query));
    for (auto const & peak: peaks)
        if(!_exist(std::get<0>(peak), std::get<1>(peak), std::get<0>(query).beaddist))
        {
            do_add_bead_in_x_y(len, width, std::get<0>(peak), std::get<1>(peak));
            auto & bd = *track_info->bd[track_info->n_b - 1];
            bd.saved_x  = bd.fence_x  = std::get<0>(peak) * oi_TRACK->dx;
            bd.saved_y  = bd.fence_y  = std::get<1>(peak) * oi_TRACK->dy;
            bd.xc       = bd.fence_xi = std::get<0>(peak);
            bd.yc       = bd.fence_yi = std::get<1>(peak);
        }
    return D_O_K;
}

PY_IMG_MENU_IMPL_ALL(beaddetector,BEADDETECTOR_MENUS)
