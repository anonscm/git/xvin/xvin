#include "../pywrapper/core.h"
#include "../pywrapper/core.hpp"
#include "../trackBead/trackBead.h"
#include "../trackBead/action.h"
#include "../trackBead/track_util.h"
#include "../trackBead/magnetscontrol.h"
#include "color.h"
#include "plot_ds.h"
#include "gui.hh"
#include <numeric>
#include <thread>
#include <chrono>

namespace
{
    namespace bp = boost::python;
    using     cc = std::chrono::steady_clock;

    struct Args
    {
        float f0         = 5.;
        float f1         = 15.;
        float wait       = 1.f;
        float athreshold = 95.;
        float rthreshold = 10.;
        int   cthreshold = 10;
        int   gaussian   = 3;
        int   contrast   = 25;
        int   nbeads     = 200;
        int   beaddist   = 15;
        int   width      = 25;
    };

    using bead_t = std::vector<std::tuple<size_t, size_t, float>>;
    struct Query
    {
        Args         cf;
        decltype(cc::now()) start;
        int          index  = 0;
        int          status = 0;
        bp::handle<> img[2];
        bead_t       beads;
        size_t       good  = 0;
        size_t       fixed = 0;
        O_i        * oi;
    };

    template <typename  ... T>
    inline auto _query(char const * others, T &&... elems)
    {
        static Args args;
        char tmp[2048];
        snprintf(tmp, sizeof(tmp),
                 "{\\color{yellow}Key parameters}\n"
                 "Force values: closed beads %%f, open beads %%f\n"
                 "Wait for %%6f seconds before a taking a picture\n"
                 "Maximum number of beads: %%d\n"
                 "Minimum distance between beads: %%d\n"
                 "Fixed-beads threshold: %%d\n"
                 "(Should be ~10 for 1µm beads, 15 for 3µm beads)\n"
                 "%s\n"
                 "{\\color{yellow}Details}\n"
                 "Filter widths: gaussian %%d, contrast %%d\n"
                 "Peak thresholds: absolute %%f, relative %%f\n"
                 "Thumbnail size: %%d\n", others);

        if(config::query("BEAD-DETECTION", tmp,
                         config::item("OPEN-F",     args.f0),
                         config::item("CLOSED-F",   args.f1),
                         config::item("WAIT",       args.wait),
                         config::item("NUM-BEADS",  args.nbeads),
                         config::item("BEAD-DIST",  args.beaddist),
                         config::item("DISTANCE",   args.cthreshold),
                         elems...,
                         config::item("GAUSSIAN",   args.gaussian),
                         config::item("CONTRAST",   args.contrast),
                         config::item("PEAKS-ABS",  args.athreshold),
                         config::item("PEAKS-REL",  args.rthreshold),
                         config::item("WIDTH",      args.width)))
            return std::make_tuple(args, false);
        return std::make_tuple(args, true);
    }

    auto _peaks(Args const & args, bp::handle<> img0, bp::handle<> img1)
    {
        bp::dict info;
        info["nbeads"]     = args.nbeads;
        info["beaddist"]   = args.beaddist;
        info["gaussian"]   = args.gaussian;
        info["contrast"]   = args.contrast;
        info["athreshold"] = args.athreshold;
        info["rthreshold"] = args.rthreshold;
        info["width"]      = args.width;

        bead_t res;
        try
        {
            auto mod = pywrapper::import("beaddetector");
            auto out = mod.attr("findBeads")(img0, img1, info);
            for(bp::stl_input_iterator<bp::object> cit(out), e; cit != e; ++cit)
            {
                size_t a = bp::extract<size_t>((*cit)[1]);
                size_t b = bp::extract<size_t>((*cit)[0]);
                float  c = bp::extract<float> ((*cit)[2]);
                res.emplace_back(std::make_tuple(a, b, c));
            }
        } catch (bp::error_already_set &)
        {
            std::string msg = "{\\color{yellow}CRASH OCCURRED!}\n";
            msg += pywrapper::exception();
            win_printf_OK(msg.c_str());
        }
        return res;
    }

    auto _nfixed(Args const & args, bead_t const & beads)
    {
        auto fcn = [&](auto i, auto const & b)
                   { return i+(std::get<2>(b) < args.cthreshold ? 1 : 0); };
        return std::accumulate(beads.begin(), beads.end(), 0u, fcn);
    }

    template <typename T>
    int  _base_run(int im, future_job * job, T fcn)
    {
        auto dt   = (Query*) job->more_data;
        auto zcur = track_info->zmag[track_info->c_i];
        if(dt->status == 0)
        {
            //auto fcur = convert_only_zmag_to_force(zcur, 0);
            dt->index = 1;//std::abs(fcur - dt->cf.f0) <= std::abs(fcur - dt->cf.f1) ? 0 : 1;
        }

        auto ztarget = convert_force_to_zmag(dt->index ? dt->cf.f0 : dt->cf.f1, 0);
        if(dt->status == 0 || dt->status == 3)
        {
            fill_next_available_action(im+1, MV_ZMAG_ABS, ztarget);
            dt->status += 1;
            job->imi    = im+16;
        }
        else if(dt->status == 1 || dt->status == 4)
        {
            if(std::abs(zcur-ztarget) < 0.005)
            {
                dt->start = cc::now();
                dt->status += 1;
            }
            else
                job->imi = im+2;
        }
        else if(dt->status == 2 || dt->status == 5)
        {
            auto delta = cc::now() - dt->start;
            if(std::chrono::duration_cast<std::chrono::seconds>(delta).count()
                    > dt->cf.wait)
            {
                auto const & img = dt->oi->im;
                auto const * ptr = (unsigned char const *) (img.mem[img.c_f]);
                std::vector<unsigned char> tmp(ptr, ptr+img.nx*img.ny);
                dt->img[dt->index] = pywrapper::topython(tmp, img.ny, img.nx);
                dt->index   = dt->index == 1 ? 0 : 1;
                dt->status += 1;
                job->imi = im+1;
            }
            else
                job->imi = im+2;
        }
        else if(dt->status == 6)
        {
            dt->beads   = _peaks(dt->cf, dt->img[0], dt->img[1]);
            dt->fixed   = _nfixed(dt->cf, dt->beads);
            dt->good    = dt->beads.size() - dt->fixed;
            fcn(*dt);
            delete dt;
            job->in_progress = 0;
            job->more_data   = nullptr;
        }
        else
            assert(false);
        return D_O_K;
    }

    template <typename  ... T>
    inline auto _run(int (*fcn)(int, future_job *), char const * others = "", T &&... elems)
    {
        if (updating_menu_state != 0)
            return D_O_K;

        try { bp::import("beaddetector"); }
        catch (bp::error_already_set &)
        {
                std::string msg = "{\\color{yellow}CRASH OCCURRED!}\n";
                msg += pywrapper::exception();
                win_printf_OK(msg.c_str());
            return D_O_K;
        }

        auto query = _query(others, elems...);
        if(!std::get<1>(query))
            return D_O_K;

        Query * dt = new Query();
        dt->cf = std::get<0>(query);
        dt->oi = oi_TRACK;

        fill_next_available_job_spot(1, track_info->c_i+16, 0, 0,
                                     nullptr, nullptr, nullptr, nullptr,
                                     dt, fcn, 0);
        return D_O_K;
    }

    int _report(int im, future_job * job)
    {
        static auto fcn = [](Query & query)
        {
            win_printf_OK("%d beads were found:\n- %d fixed\n- %d good\n",
                          int(query.beads.size()), query.fixed, query.good);
        };
        return _base_run(im, job, fcn);
    }

    int _display(int im, future_job * job)
    {
        auto fcn = +[](Query & query)
        {
            imreg *imr = NULL;
            char tmp[] = "%im";
            if (ac_grep(NULL, tmp, &imr) < 1 || imr == NULL)
                return;

            auto n  = query.good > 0 ? query.good : query.fixed;
            if(n == 0)
            {
                win_printf_OK("No beads were found");
                return;
            }

            auto &img = *query.oi;
            auto  cio = create_and_attach_oi_to_imr(imr, img.im.nx, img.im.ny, IS_CHAR_IMAGE);
            auto  op  = create_and_attach_op_to_oi(cio, n, n, 0, IM_SAME_X_AXIS + IM_SAME_Y_AXIS);

            char  tmp1[] =  "good beads", tmp2[] = "\\fd",
                  tmp3[] = "fixed beads", tmp4[] = "\\oc";
            auto  dsG    = op->dat[0];
            duplicate_image_or_movie(&img, cio, 0);
            if(query.good > 0)
            {
                alloc_data_set_y_error(dsG);
                set_ds_source(dsG, tmp1);
                set_ds_point_symbol(dsG, tmp2);
                set_ds_dot_line(dsG);
                set_ds_line_color(dsG, Green);
            }

            d_s * dsB = nullptr;
            if(query.fixed > 0)
            {
                dsB = create_and_attach_one_ds(op, query.fixed, query.fixed, 0);
                alloc_data_set_y_error(dsB);
                set_ds_source(dsB, tmp3);
                set_ds_point_symbol(dsB, tmp4);
                set_ds_dot_line(dsB);
                set_ds_line_color(dsB, Red);
            }

            size_t iG = 0, iB = 0;
            auto cthr = query.cf.cthreshold;
            for(auto const & peak: query.beads)
                if(std::get<2>(peak) > cthr && dsG != nullptr)
                {
                    dsG->xd[iG] = std::get<0>(peak);
                    dsG->yd[iG] = std::get<1>(peak);
                    dsG->ye[iG] = std::get<2>(peak);
                    ++iG;
                }
                else if(dsB != nullptr)
                {
                    dsB->xd[iB] = std::get<0>(peak);
                    dsB->yd[iB] = std::get<1>(peak);
                    dsB->ye[iB] = std::get<2>(peak);
                    ++iB;
                }

            char tmp5[512];
            snprintf(tmp5, sizeof(tmp5), "FOV with %d/%d beads",
                     int(query.good), int(query.beads.size()));
            set_oi_filename(cio, tmp5);
            set_op_filename(op,  tmp5);
            cio->need_to_refresh = ALL_NEED_REFRESH;
            op ->need_to_refresh = ALL_NEED_REFRESH;
        };
        return _base_run(im, job, fcn);
    }

    int _target(int im, future_job * job)
    {
        static int  len = 128, width = 16;

        static auto fcn = +[](Query & query)
        {
            auto cthr = query.cf.cthreshold;
            int  i    = 0;
            for (auto const & peak: query.beads)
            {
                if(std::get<2>(peak) <= cthr)
                    continue;

                if(i < track_info->n_b)
                    do_add_bead_in_x_y(len, width, std::get<0>(peak), std::get<1>(peak));
                auto & bd = *track_info->bd[i++];

                bd.saved_x = bd.fence_x  = std::get<0>(peak) * query.oi->dx;
                bd.saved_y = bd.fence_y  = std::get<1>(peak) * query.oi->dy;
                bd.x0 = bd.xc = bd.fence_xi = std::get<0>(peak);
                bd.y0 = bd.yc = bd.fence_yi = std::get<1>(peak);
            }
        };

        return _base_run(im, job, fcn);
    }
}

int do_find_beads   (void) { return _run(_report);  }
int do_display_beads(void) { return _run(_display); }
int do_target_beads (void) { return _run(_target);  }

PY_IMG_MENU_IMPL_ALL(beaddetector,BEADDETECTOR_MENUS)
