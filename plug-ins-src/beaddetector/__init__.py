#!/usr/bin/env python3
# -*- coding: utf-8 -*-
u""" Detects beads """
import numpy
import scipy.ndimage.filters as _filt
import skimage.feature       as _feat
#from   ._bead                import IMG

IMG = numpy.fromfile(__file__[:__file__.replace('\\', '/').rfind('/')+1]+"bead",
                     dtype = 'f4').reshape((78,51))

class BeadDetector: # pylint: disable=too-many-instance-attributes
    u"""
    Finds beads in an image and uses another one to figure out whether
    they are likely to be fixed beads.
    """
    def __init__(self, **kw):
        self.gaussian      = kw.get('gaussian',    3.)
        self.contrast      = kw.get('contrast',    25.)
        self.nbeads        = kw.get('nbeads',      200)
        self.beaddist      = kw.get('beaddist',    15)
        self.athreshold    = kw.get('athreshold',  95.)
        self.rthreshold    = kw.get('rthreshold',  10.)
        self.width         = kw.get('width',       25)
        self.thumbgaussian = 2
        self.stripsize     = 3
        self.maskmax       = 95.

    def __get(self, name, args):
        return args.get(name, getattr(self, name))

    @staticmethod
    def _normed(img):
        img -= img.min()
        img /= img.max()
        return img

    def filterImage(self, data, **kw):
        u"""
        Returns an image where low spacial frequencies have been squashed.

        The *gaussian* width can be provided as a argument.
        """
        ret = 1.-data/_filt.gaussian_filter(data, self.__get('gaussian', kw))
        return self._normed(ret)

    def contrastImage(self, data, **kw):
        u"""
        computes a locally contrasted image
        """
        contrast = self.__get('contrast', kw)
        if contrast > 0:
            maxi = _filt.maximum_filter(data, contrast)
            mini = _filt.minimum_filter(data, contrast)
            mask = (maxi - mini) / (maxi+mini)
            mask = numpy.clip(mask, 0., numpy.percentile(mask, self.maskmax))
            data = mask*(1.-data)
            return self._normed(data)
        return data

    def findBeads(self, data, **kw):
        u"""
        Returns a list of peaks in the image.

        Keyword arguments are:

        * *nbeads* is the maximum number of such peaks
        * *beaddist* is the minimum distance between 2 peaks
        * *athreshold* is the absolute threshold for a peak to be selected
        * *rthreshold* is the relative threshold for a peak to be selected
        * *contrast* is provided, the image is multiplied by is local contrast.
        """
        mdist = self.__get('beaddist',   kw)
        aval  = numpy.percentile(data, self.__get('athreshold', kw))
        rval  = self.__get('rthreshold', kw) * 1e-2
        ini   = self.__get('nbeads',kw)#*2
        peaks = _feat.peak_local_max(data,
                                     num_peaks     = ini,
                                     min_distance  = mdist,
                                     threshold_abs = aval,
                                     threshold_rel = rval)
        return peaks

    def thumbnail(self, bead, img, contr, **kw):
        u"""
        Extracts a thumbnail image centered around the *bead* position.

        Keyword arguments are:

        * *width* is the half-size of the width image
        """
        width = self.__get("width", kw)

        xmin  = numpy.array([max(0, bead[0]-width), max(0, bead[1]-width)])

        if contr is not None:
            tmp   = contr[xmin[0]:bead[0]+width, xmin[1]:bead[1]+width]
            xmin +=  _feat.peak_local_max(tmp, num_peaks = 1)[0]
            xmin -= tmp.shape[0]//2, tmp.shape[1]//2
            xmin  = max(xmin[0], 0), max(xmin[1], 0)

        thumb = img[xmin[0]:xmin[0]+2*width, xmin[1]:xmin[1]+2*width]
        return self.filterImage(thumb, gaussian = self.__get('thumbgaussian', kw))

    def convolutions(self, bead, imgs, axis):
        u"Returns a grade indicating how un-likely a bead is fixed"
        thb = self.thumbnail(bead, *imgs)
        thb = numpy.clip(thb,
                         numpy.percentile(thb, 100.-self.maskmax),
                         numpy.percentile(thb, self.maskmax))
        thb = self._normed(thb)

        ix1, ix2 = thb.shape[axis]//2-self.stripsize, self.stripsize*2+1
        strip    = thb[:,ix1:][:,:ix2] if axis == 1 else thb[ix1:,:][:ix2,:]
        strip    = strip.mean(axis = axis)
        return numpy.array([numpy.max(numpy.convolve(strip, i, 'same'))
                            for i in IMG])

    def grade(self, bead, imgs1, imgs2):
        u"Returns a grade indicating how un-likely a bead is fixed"
        xstrips = (self.convolutions(bead, imgs1, 1),
                   self.convolutions(bead, imgs2, 1))
        ystrips = (self.convolutions(bead, imgs1, 0),
                   self.convolutions(bead, imgs2, 0))

        def _measure(img1, img2):
            img1  -= numpy.median(img1)
            img2  -= numpy.median(img2)
            coeffs = numpy.polyfit(img1, img2, 1)
            vals   = numpy.abs((img2 - coeffs[0]*img1-coeffs[1]))
            return numpy.median(vals/numpy.sqrt(1.+coeffs[0]**2))

        return min(_measure(*xstrips), _measure(*ystrips))

def findBeads(img1, img2, kwa):
    u"""
    Returns a sorted list of beads. Ones unlikely to be fixed are higher in
    the list.
    """
    alg    = BeadDetector(**kwa)
    clean  = alg.filterImage(img1), alg.filterImage(img2)
    contr  = tuple(alg.contrastImage(cle) for cle in clean)
    beads  = tuple(alg.findBeads(contr[0]))
    grades = numpy.zeros((len(beads,)), dtype = numpy.float32)
    for i, bead in enumerate(beads):
        grades[i] = min(alg.grade(bead, (img1, None), (img2, contr[1])),
                        alg.grade(bead, (img1, None), (img2, None)))

    inds  = numpy.argsort(grades)[::-1][:alg.nbeads]
    return tuple((int(beads[i][0]), int(beads[i][1]), float(grades[i])) for i in inds)
