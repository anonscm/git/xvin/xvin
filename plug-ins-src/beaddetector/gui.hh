#pragma once
#include "memorized_win_scan.hh"
#include "xvin.h"
#include <allegro.h>
#ifdef XV_WIN32
#   include <winalleg.h>
#endif

#define BEADDETECTOR_MENUS                          \
((false, "display beads",      do_display_beads))   \
((false, "report bead counts", do_target_beads))    \
((false, "target beads",       do_target_beads))
MENU_DECLARE_ALL(beaddetector,BEADDETECTOR_MENUS)
