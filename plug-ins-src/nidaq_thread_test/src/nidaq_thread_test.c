/** \file nidaq_thread_test.c

    Tries to update directly the card.
    
    \author Adrien Meglio
    
    \version 13/02/07
*/
#ifndef NIDAQ_THREAD_TEST_C_
#define NIDAQ_THREAD_TEST_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../../logfile/logfile.h"
#include "../../nidaqmxwrap_thread/include/nidaqmxwrap_thread.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "nidaq_thread_test.h"



////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Square function.

\param float x The independent variable
\param void *p This is the duty cycle. It is cast on a float and it indicates the ratio of 'up' to 'up+down'.
\return float64 the function value

\author Francesco Mosconi
\version 21/03/07
*/    
float64 nidaq_test_square_function(float x,void* p)
{
  
  float * param = (float *) p;
  float duty_cycle = *param;
  
  if(x <= duty_cycle)return 1;

  return -1;
}

int nidaq_test_square_function_menu(void)
{
    int j;
    
    if(updating_menu_state != 0) return D_O_K;
  
    for (j=0 ; j<NUMBER_OF_LINES ; j++)
    {
        lio.lines[j].amplitude = 2.5;
        lio.lines[j].offset = 2.5;
        lio.lines[j].phase = j/NUMBER_OF_LINES;  
        
        lio.lines[j].function = nidaq_test_square_function;
        lio.lines[j].params = (void*) NULL;
    }

    return D_O_K;

}



////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Triangle function.

\param float x The independent variable
\param void *p This is the duty cycle. It is cast on a float and it indicates the ratio of 'upward' to the period length.
\return float64 the function value

\author Francesco Mosconi
\version 21/03/07
*/    
float64 nidaq_test_triangle_function(float x,void* p)
{
  float * param = (float *) p;
  float duty_cycle = *param;
  
  if(x <= duty_cycle) 
    return (float64) -1 + 2./duty_cycle*x ;

  return (float64) 1 - 2/(1-duty_cycle)*x;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Sine function.

\param float x The independent variable
\param void *p This is NULL;
\return float64 sin(x)

\author Francesco Mosconi
\version 21/03/07
*/    
float64 nidaq_test_sin_function(float x,void* p)
{
  return (float64) sin(x);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Cosine function
    
\param float x The independent variable
\param void *p This is NULL;
\return float64 cos(x)

\author Francesco Mosconi
\version 21/03/07
*/    
float64 nidaq_test_cos_function(float x,void* p)
{
  return (float64) cos(x);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Constant function
    
\param float x The independent variable
\param void *p This is NULL;
\return float64 1

\author Francesco Mosconi
\version 21/03/07
*/    
float64 nidaq_test_const_function(float x,void* p)
{
  return 1.;
}



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Menu functions

/** A menu containing a clickable item and a static submenu */
MENU *nidaq_thread_test_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
    add_item_to_menu(mn,"Test : Start",task__continuous_buffer_regeneration,NULL,0,NULL);
    add_item_to_menu(mn,"Test : Stop",task__stop,NULL,0,NULL);
    add_item_to_menu(mn,"Test : Square",nidaq_test_square_function_menu,NULL,0,NULL);
    //add_item_to_menu(mn,"Test : Alternate Two",menu__alternate_two,NULL,0,NULL);
    //add_item_to_menu(mn,"Test : Alternate Three",menu__alternate_three,NULL,0,NULL);
   
    return mn;
}

/** Main menu for plug_in */     
int menu_plug_in_main(void)
{   
    if(updating_menu_state != 0) return D_O_K;
    
    //win_report("Menu capabilities loaded");
    
    add_plot_treat_menu_item("Nidaq thread test",NULL,nidaq_thread_test_menu(),0,NULL);
    add_image_treat_menu_item("Nidaq thread test",NULL,nidaq_thread_test_menu(),0,NULL);
	
	return D_O_K;    
}

int nidaq_thread_test_main(void)
{
    if (updating_menu_state != 0) return D_O_K;
    
    menu_plug_in_main();
    
    return D_O_K;
}


#endif
