/** Function declarations for the plug-in

\author Francesco Mosconi
*/

#ifndef NIDAQ_THREAD_TEST_H_
#define NIDAQ_THREAD_TEST_H_

PXV_FUNC(float64, nidaq_test_square_function,(float x,void* p));
PXV_FUNC(float64, nidaq_test_triangle_function,(float x,void* p));
PXV_FUNC(float64, nidaq_test_sin_function,(float x,void* p));
PXV_FUNC(float64, nidaq_test_cos_function,(float x,void* p));
PXV_FUNC(float64, nidaq_test_cos_function,(float x,void* p));
PXV_FUNC(int, nidaq_thread_test_main,(void));

PXV_FUNC(MENU, *NIDAQ_base_menu, (void));
PXV_FUNC(int, menu_plug_in_main, (void));

#endif
