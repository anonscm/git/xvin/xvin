#ifndef _AGING_H_
#define _AGING_H_



PXV_FUNC(int, do_fluctuations_files,								(void));
PXV_FUNC(int, do_response_files,								(void));
PXV_FUNC(int, do_extract_at_one_frequency,								(void));
PXV_FUNC(int, do_plot_resonance_frequency,								(void));

void spit(char *message);

#endif

