#ifndef _AGING_C_
#define _AGING_C_

#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"

#include <gsl/gsl_histogram.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_statistics_float.h>
#include <fftw3.h>


/* If you include other plug-ins header do it here*/ 
#include "../inout/inout.h"
#include "../diff/diff.h"
#include "../pendule/pendule.h"
#include "../response/response.h"
#include "../response/response_math.h"
#include "../../src/menus/plot/treat/p_treat_fftw.h"
#include "fft_filter_lib.h"
/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "aging.h"

#define k_b 1.3806503e-23	// m2 kg s-2 K-1
#define Temperature (25.+273.)		// K

// offsets de l'ampli photodiode
float pendule_x_offset= 0.;
float pendule_y_offset= 0.;
float lambda=632.8, indice=1, conv_radian=1.25e-7;


//////////////////////////////////////////////////////////////////////////////////////////////////
//			Traitements de fichiers de fluctuations					//
//////////////////////////////////////////////////////////////////////////////////////////////////

int do_fluctuations_files(void)
{  //Declaration des variables
register int 	i, j, k;
	O_p 	*op2=NULL, *op1 = NULL;
	float rayon=0., mean_noise=0., mean_noise_ref=0.;
static	int	n_X=1, n_Y=2, n_excitation=3, N_decimate=1, N_channels=3;
	int N_total, nx, ny, nx_out, n_M;
	int *files_index=NULL;		// all the files
	int i_file, n_files;
	float	F_r;
	d_s 	*ds2=NULL, *ds_R=NULL, *ds_duration=NULL, *ds_noise=NULL;
	pltreg	*pr = NULL;
	float	*x, *y, *excitation, *phase, *tmp;
static	char files_ind_string[128]	="0:1:9"; 
static	char file_extension[8]		=".bin";
static	char root_filename[128]	     = "run03";
	char filename[256];
	char *message=NULL, *date_stamp=NULL;
	long current_position;
	char *channel_string=NULL, *channel_cfg=NULL, *user_header=NULL;
	char s[512];
static 	int bool_plot_mean_of_noise=0, bool_plot_mean_radius=0, bool_hanning=1, bool_plot_duration=0;
static 	int bool_radian=1, bool_normalize_psd=0, bool_criteria=0;
static 	int n_fft=16384, n_overlap=14000;
static 	float epsilon=1.e-2, f_acq=2048, fmin=850, fmax=950;
	int i_start, i_end, j_min, j_max;
	

	
	if(updating_menu_state != 0)	return D_O_K;	
//	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK(	"This routine calculates the displacement in nm from X and Y channel\n"
	  				" and plots the psd of the deplacement.\n"
					" The signal can be filtered.\n"
					"We can also plot the contrast function with the files"
					"We can also plot the mean of the noise of the interferometer (end of psd)");
	}
	
	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)		return win_printf_OK("cannot plot region");
	

	// following is useless. I comment it out. NG, 2006-05-23
/*	current_position=get_header_informations_NI4472(fullfilename, &channel_string, &channel_cfg, &N_channels, &f_acq, &F_r,
								&date_stamp, &user_header, &N);
	if (current_position<0)		return(win_printf_OK("Error reading file header for %s, aborting.", NI_filename));
*/
	
	//Instruction demandee a l'utilisateur...		
	message=my_sprintf(message,"{\\color{yellow}\\pt14 Operate on several files}\n"
				"root name of fluctuation files %%s"
				"range of fluctuation files %%s"
				"file extension (note: expected files are from NI4472) %%s\n"
				"Number of channels in a file : %%2d\n"
				"channel with X data %%5d offset to substract %%8f\n"
     			"channel with Y data %%5d offset to substract %%8f\n"
     			"channel with excitation %%5d"
     			"decimate every %%6d points\n\n");
	i=win_scanf(message, &root_filename, &files_ind_string, &file_extension, &N_channels,
    		        &n_X, &pendule_x_offset, &n_Y, &pendule_y_offset, &n_excitation, &N_decimate);
	if (i==CANCEL) return(D_O_K);
	free(message); message=NULL;
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf_OK("bad values for files indices !"));
	
	i=win_scanf("{\\pt14\\color{yellow} Parameters for the loading of phase}\n"
				"laser wavelength : %10f (nm)\n"
     		  	"optical indice : %10f\n\n"
     		  	"%R data in nm, or %r in rad /Conversion coefficient: %9f rad/nm\n"
				"%b Criterium to find indices between which there is no excitation  \n"
				"in this case, epsilon = %8f \n\n"
				"%b plot the duration of the fluctuations in a file\n"
				"%b plot mean of radius (<R>)\n"
				"%b plot mean of noise of interferometer (last points of psd) \n"
				"in this cas select the limit of frequencies between which mean is calculated\n"
				"f_{min} = %8f Hz  and   f_{max} = %8f Hz\n\n"
				"normalize all psd by mean of noise (reference is first file)? %b",
			&lambda, &indice, &bool_radian, &conv_radian, &bool_criteria, &epsilon, &bool_plot_duration, &bool_plot_mean_radius, &bool_plot_mean_of_noise,
			&fmin, &fmax, &bool_normalize_psd);
	if (i==CANCEL) return(D_O_K);
	
		
	message = my_sprintf(message, "number of points of fft window ?    %%6d\n"
			"overlap (in points) between windows ? %%6d\n"
			"acquisition frequency (in Hz, for normalization) ?%%9f\n"
			"%%b use Hanning window ?");
	if (win_scanf(message, &n_fft, &n_overlap, &f_acq, &bool_hanning) == CANCEL)
			return D_O_K;
	free(message); message=NULL;

	if (n_overlap>=n_fft)
			return win_printf("overlap is too large compared to n\\_fft (%d>=%d)",n_overlap, n_fft);
	if ((op2=create_and_attach_one_plot(pr, (n_fft/2)+1, (n_fft/2)+1, 0)) == NULL)
			return win_printf_OK("cannot create PSD plot !");
	if ((ds2=op2->dat[0]) == NULL)
			return win_printf_OK("Cannot allocate PSD dataset");
					

	if (bool_plot_mean_radius==1)
	{ 	if ((op1 = create_and_attach_one_plot(pr, n_files, n_files, 0)) == NULL)
				return win_printf_OK("cannot create plot!");
		ds_R = op1->dat[0];
		ds_R->treatement = my_sprintf(ds_R->treatement,"mean of radius");
		set_plot_title(op1,"Mean radius in a file", op1->filename);
		set_plot_y_title(op1, "contrast mean radius (V)");
	}
	
	if (bool_plot_duration==1)
	{ 	if ((op1 = create_and_attach_one_plot(pr, n_files, n_files, 0)) == NULL)
				return win_printf_OK("cannot create plot");
		ds_duration = op1->dat[0];
		ds_duration->treatement = my_sprintf(ds_duration->treatement,"duration of fluctuation");
		set_plot_title(op1,"Duration of fluctuation in a file", op1->filename);
		set_plot_y_title(op1, "t_f (s)");
	}
	
	if (bool_plot_mean_of_noise==1)
	{ 	if ((op1 = create_and_attach_one_plot(pr, n_files, n_files, 0)) == NULL)
				return win_printf_OK("cannot create plot");
		ds_noise = op1->dat[0];
		ds_noise->treatement = my_sprintf(ds_noise->treatement,"mean of noise");
		set_plot_title(op1,"Mean noise of interferometer", op1->filename);
		set_plot_y_title(op1, "mean noise (%s/\\sqrt{Hz})", (bool_radian==1) ? "rad" : "nm");
	}
	
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		sprintf(filename, "%s%d%s", root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s", k+1, n_files, filename); spit(s);
		
		current_position=get_header_informations_NI4472(filename, &channel_string, &channel_cfg, &N_channels, &f_acq, &F_r,
								&date_stamp, &user_header, &N_total);
		if (current_position<0)		return(win_printf_OK("Error reading file header for %s, aborting.", filename));
  		f_acq=(float) f_acq/N_decimate;

		nx  = load_NI4472_data(filename, current_position, N_total, N_channels, n_X, 	N_decimate, &x);				
		ny  = load_NI4472_data(filename, current_position, N_total, N_channels, n_Y, 	N_decimate, &y);
		n_M = load_NI4472_data(filename, current_position, N_total, N_channels, n_excitation, N_decimate, &excitation);
		if (nx ==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
		if (ny ==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
		if (n_M==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
	   	if (nx!=ny)  return(win_printf_OK("error! not the same number of points in X and Y!"));	
		if (nx!=n_M) return(win_printf_OK("error! not the same number of points in X and M!"));	
		nx_out=nx;
// Detection de la zone de l'excitation!!!
		i_start=0; i_end=nx;
		if (bool_criteria==1)
		{	for (i=0; i<nx/2; i++)
				{	if (-epsilon>excitation[i]) i_start=i;
					if (epsilon<excitation[i]) i_start=i;
				}
			for (i=nx; i>nx/2; i--)
				{	if (-epsilon>excitation[i]) i_end=i+1;
					if (epsilon<excitation[i]) i_end=i+1;
				}
		}
		
//Trac� du rayon moyen par fichier!!!
		if (bool_plot_mean_radius==1)
		{	rayon=0.;
			for (i=i_start; i<i_end; i++) 
			{	x[i]	-=	pendule_x_offset;
				y[i]	-=	pendule_y_offset;		
				rayon 	+=	sqrt(x[i]*x[i]+y[i]*y[i]);
			}
			rayon	/=	(float)(i_end-i_start);
			ds_R->xd[k]=(float)k;
			ds_R->yd[k]=rayon;
		} 
		
		phase=(float*)calloc(nx, sizeof(float));
		compute_phase(phase, x, y, nx, pendule_x_offset, pendule_y_offset, lambda, indice, bool_radian, conv_radian);
		free(x);
		free(y);
		
		nx_out=i_end-i_start;
		if (bool_plot_mean_radius==1)
		{  	ds_duration->xd[k]=(float)k;
			ds_duration->yd[k]=(float)nx_out/f_acq;			
		}
		
		
		tmp=(float*)calloc(nx_out, sizeof(float));
		for (j=0; j<nx_out; j++) tmp[j]=phase[i_start+j];
		free(phase);
		
		
		if (k!=0) // first file : we add a dataset
		{	if ((ds2 = create_and_attach_one_ds(op2, (n_fft/2)+1, (n_fft/2)+1, 0)) == NULL)	return win_printf_OK("cannot create dataset !");
		}
		i=compute_psd_of_float_data( tmp, ds2->yd, ds2->xd, f_acq, nx_out, n_fft, n_overlap, bool_hanning);
		for (j=0; j<(n_fft/2)+1; j++) ds2->yd[j]=(float)sqrt(ds2->yd[j]);
		if ((bool_plot_mean_radius+bool_normalize_psd)>=1)
		{  	mean_noise=0.;
			j_min=(int) fmin*n_fft/f_acq;
			j_max=(int) fmax*n_fft/f_acq;
			for (j=j_min; j<j_max;j++) mean_noise+=ds2->yd[j];
			if (bool_plot_mean_radius==1)
			{	ds_noise->xd[k]=(float)k;
				ds_noise->yd[k]=(float)mean_noise/(float)(j_max-j_min);
			}
			if (bool_normalize_psd==1) 
			{	if (k==0) mean_noise_ref=mean_noise;
				else
				{ 	for (j=0; j<(n_fft/2)+1; j++) ds2->yd[j]*=mean_noise_ref/mean_noise;
				}
			}	
		
		}
		
		ds2->treatement = my_sprintf(ds2->treatement,"psd from file number %d done at time %s", k, date_stamp);
		free(tmp);
	}
	set_plot_x_title(op2, "f (Hz)");
	set_plot_y_title(op2, "%s/\\sqrt{Hz}", (bool_radian==1) ? "rad" : "nm");
	op2->iopt |= XLOG;
   	op2->iopt |= YLOG;
	return refresh_plot(pr, UNCHANGED);
}
//end of function do_fluctuations_files

//////////////////////////////////////////////////////////////////////////////////////////////////
//			Traitements de fichiers de reponse					//
//////////////////////////////////////////////////////////////////////////////////////////////////


int do_response_files(void)
{  //Declaration des variables
register int 	i, j, k;
	O_p 	*op1, *op_modulus=NULL, *op_phase = NULL, *op_real=NULL, *op_imaginary = NULL;
	O_p		*op_real_inverse = NULL, *op_imaginary_inverse = NULL, *op_FDT=NULL;
static	int	n_X=1, n_Y=2, n_excitation=3, N_decimate=1, N_channels=3;
	int N_total, nx, ny, nx_out, n_M;
	int		*files_index=NULL;		// all the files
	int i_file, n_files;
	float	F_r;
	d_s 	*ds_modulus=NULL, *ds_phase=NULL, *ds_real=NULL, *ds_imaginary=NULL;
	d_s		*ds_real_inverse=NULL,*ds_imaginary_inverse=NULL, *ds_FDT=NULL;
	pltreg	*pr = NULL;
	float	*x, *y, *excitation, *phase, *tmp_phase, *tmp_excitation;
static	char files_ind_string[128]	="0:1:9"; 
static	char file_extension[8]		=".bin";
static	char	root_filename[128]	     = "run03";
	char		filename[256];
	char *message=NULL, *date_stamp=NULL;
	long current_position;
	char *channel_string=NULL, *channel_cfg=NULL, *user_header=NULL;
	char		s[512];
static int bool_radian=1, bool_excitation_squared=1;
static int bool_modulus=1, bool_phase=1, bool_real=1, bool_imaginary=1, bool_FDT=1;
static int bool_real_inverse=1, bool_imaginary_inverse=1, bool_sum_sinus=1, bool_criteria=1;
static int bool_remember_values=1, bool_overlap_type=0, bool_crossresponse=1, bool_hanning=1;
static int n_fft=16384, n_overlap=14000;
static float epsilon=1.e-2, f_acq=2048, percent_overlap=0.8;;
	int i_start, i_end,imin = 0, imax = 0;
static char	f_string[128]="2,3,7,11,13,17,19,23,29,31";
static float fmin=0.1, fmax=10;
	int		n_f;
	float 	*f_index=NULL;
	

	
	if(updating_menu_state != 0)	return D_O_K;	
//	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK(	"This routine calculates the displacement in nm from X and Y channel\n"
	  				"and computes the (complex) response function H\n\n"
					"defined in Fourier space by S = H.E\n"
					"with S being the displacement in nm\n"
					"and E being the excitation.\n\n"
					"Response function is plotted, and kept in memory." );
	}
	
	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)		return win_printf_OK("cannot plot region");
	
	message=my_sprintf(message,"{\\color{yellow}\\pt14 Operate on several files}\n"
				"root name of fluctuation files %%s"
				"range of fluctuation files %%s"
				"file extension (note: expected files are from NI4472) %%s\n"
				"Number of channels in a file : %%2d\n"
				"channel with X data %%5d offset to substract %%8f\n"
     			"channel with Y data %%5d offset to substract %%8f\n"
     			"channel with excitation %%5d\n\n"
     			"decimate every %%6d points\n\n");
	i=win_scanf(message, &root_filename, &files_ind_string, &file_extension, &N_channels,
    		        &n_X, &pendule_x_offset, &n_Y, &pendule_y_offset, &n_excitation, &N_decimate);
	if (i==CANCEL) return(D_O_K);
	free(message); message=NULL;
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf_OK("bad values for files indices !"));
	
	i=win_scanf("{\\pt14\\color{yellow} Parameters for the loading of phase}\n"
				"laser wavelength : %10f (nm)\n"
     		  	"optical indice : %10f\n\n"
     		  	"%R data in nm, or %r in rad /Conversion coefficient: %9f rad/nm\n\n"
				"%b Criterium to find indices between which there is no excitation  \n"
				"in this case, epsilon = %8f \n\n"
				"excitation is linear %R or quadratic %r",
				&lambda, &indice, &bool_radian, &conv_radian, &bool_criteria, &epsilon, &bool_excitation_squared);
	if (i==CANCEL) return(D_O_K);
	
	message = my_sprintf(message, "{\\pt14\\color{yellow}Response function H}\n\n"
			"- number of points of fft window : %%8d\n"
			"- overlap between windows       : %%8d\n"
			"     overlap in %%R points or %%r in percents\n"
			"- acquisition frequency (in Hz) : %%8f\n"
			"     (for normalization) %s\n\n"
			"%%b use Hanning window ?\n"
			"%%R Normal response H=ds2/ds1 or %%r Cross-response H=<ds2 ds1*> / (ds2 ds1*)\n"
			"%%b remember values");
	k=win_scanf(message,&n_fft, &n_overlap, &bool_overlap_type, &f_acq, &bool_hanning, &bool_crossresponse, &bool_remember_values);
	free(message);
	if (k==CANCEL) return(D_O_K);
		
	percent_overlap = (float)n_overlap/((float)(100.));
	if (bool_overlap_type==1)	n_overlap       =(int)(percent_overlap*(float)n_fft);
	else						percent_overlap =(float)n_overlap/(float)n_fft;
	
	if ( (n_overlap>=n_fft) || (percent_overlap>=1.) )	
	return(win_printf_OK("overlap is too large compared to n-fft\n"
					"in points    : %d>=%d\n"
					"in percents : %d>=100", n_overlap, n_fft, (int)((float)100.*percent_overlap)));
	
	i=win_scanf("{\\pt14\\color{yellow} Plot}\n"
				"%R excitation is composed of a sum of sinusoides\n"
				"%r excitation is a colored noise\n"
				"Sum of sinusoides, frequencies ? %s\n"
				"Colored noise, min frequency ? %10f max frequency ? %10f\n"
     		  	"do you to plot?\n"
     		  	"%b modulus of response function\n"
     		  	"%b phase of response function\n"
     		  	"%b real part of response function\n"
     		  	"%b imaginary part of response function\n"
     		  	"%b real part of inverse response function\n"
     		  	"%b imaginary part of inverse response function\n"
     		  	"%b Im/omega",
     		  	&bool_sum_sinus, &f_string,  &fmin, &fmax, &bool_modulus, &bool_phase, &bool_real,
     		  	&bool_imaginary, &bool_real_inverse, &bool_imaginary_inverse, &bool_FDT);
	if (i==CANCEL) return(D_O_K);
 	
	if (bool_sum_sinus==1)
	{	 imin=(int)fmin *n_fft/f_acq;
		 imax=(int)fmax *n_fft/f_acq;
	}
	else f_index = str_to_float(f_string, &n_f);	// malloc is done by str_to_index
    
    
	if (bool_modulus==1)
	{ 	if (bool_sum_sinus==1)
		{	if ((op_modulus=create_and_attach_one_plot(pr, (imax-imin), (imax-imin), 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}
		else 
		{	if ((op_modulus=create_and_attach_one_plot(pr, n_f, n_f, 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}		
		if ((ds_modulus=op_modulus->dat[0]) == NULL)
			return win_printf_OK("Cannot allocate dataset");
		ds_modulus->treatement = my_sprintf(ds_modulus->treatement,"modulus of response function");
		set_plot_title(op_modulus,"Modulus of response function", op_modulus->filename);
	}
	if (bool_real==1)
	{ 	if (bool_sum_sinus==1)
		{	if ((op_real=create_and_attach_one_plot(pr, (imax-imin), (imax-imin), 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}
		else 
		{	if ((op_real=create_and_attach_one_plot(pr, n_f, n_f, 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}	
		if ((ds_real=op_real->dat[0]) == NULL)
			return win_printf_OK("Cannot allocate dataset");
		ds_real->treatement = my_sprintf(ds_real->treatement,"modulus of response function");
		set_plot_title(op_real,"Real part of response function", op_modulus->filename);
	}
	if (bool_imaginary==1)
	{ 	if (bool_sum_sinus==1)
		{	if ((op_imaginary=create_and_attach_one_plot(pr, (imax-imin), (imax-imin), 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}
		else 
		{	if ((op_imaginary=create_and_attach_one_plot(pr, n_f, n_f, 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}	
		if ((ds_imaginary=op_imaginary->dat[0]) == NULL)
			return win_printf_OK("Cannot allocate dataset");
		ds_imaginary->treatement = my_sprintf(ds_imaginary->treatement,"imaginary of response function");
		set_plot_title(op_imaginary,"imaginary of response function", op_imaginary->filename);
	}
	if (bool_phase==1)
	{ 	if (bool_sum_sinus==1)
		{	if ((op_phase=create_and_attach_one_plot(pr, (imax-imin), (imax-imin), 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}
		else 
		{	if ((op_phase=create_and_attach_one_plot(pr, n_f, n_f, 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}
		if ((ds_phase=op_phase->dat[0]) == NULL)
			return win_printf_OK("Cannot allocate dataset");
		ds_phase->treatement = my_sprintf(ds_phase->treatement,"phase of response function");
		set_plot_title(op_phase,"phase of response function", op_phase->filename);
	}
	if (bool_real_inverse==1)
	{ 	if (bool_sum_sinus==1)
		{	if ((op_real_inverse=create_and_attach_one_plot(pr, (imax-imin), (imax-imin), 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}
		else 
		{	if ((op_real_inverse=create_and_attach_one_plot(pr, n_f, n_f, 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}
		if ((ds_real_inverse=op_real_inverse->dat[0]) == NULL)
			return win_printf_OK("Cannot allocate dataset");
		ds_real_inverse->treatement = my_sprintf(ds_real_inverse->treatement,"real part of inverse of response function");
		set_plot_title(op_real_inverse,"real part of inverse of response function", op_real_inverse->filename);
	}
	if (bool_imaginary_inverse==1)
	{ 	if (bool_sum_sinus==1)
		{	if ((op_imaginary_inverse=create_and_attach_one_plot(pr, (imax-imin), (imax-imin), 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}
		else 
		{	if ((op_imaginary_inverse=create_and_attach_one_plot(pr, n_f, n_f, 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}
		if ((ds_imaginary_inverse=op_imaginary_inverse->dat[0]) == NULL)
			return win_printf_OK("Cannot allocate dataset");
		ds_imaginary_inverse->treatement = my_sprintf(ds_imaginary_inverse->treatement,"imaginary part of inverse of response function");
		set_plot_title(op_imaginary_inverse,"imaginary part of inverse of response function", op_imaginary_inverse->filename);
	}
	
	if (bool_FDT==1)
	{ 	if (bool_sum_sinus==1)
		{	if ((op_FDT=create_and_attach_one_plot(pr, (imax-imin), (imax-imin), 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}
		else 
		{	if ((op_FDT=create_and_attach_one_plot(pr, n_f, n_f, 0)) == NULL)
				return win_printf_OK("cannot create plot !");
		}
		if ((ds_FDT=op_FDT->dat[0]) == NULL)
			return win_printf_OK("Cannot allocate dataset");
		ds_FDT->treatement = my_sprintf(ds_FDT->treatement,"imaginary part of inverse of response function");
		set_plot_title(op_FDT,"Im(\\chi)/omega", op_FDT->filename);
	}
	

	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		sprintf(filename, "%s%d%s", root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s", k+1, n_files, filename); spit(s);
		
		current_position=get_header_informations_NI4472(filename, &channel_string, &channel_cfg, &N_channels, &f_acq, &F_r,
								&date_stamp, &user_header, &N_total);
		if (current_position<0)		return(win_printf_OK("Error reading file header for %s, aborting.", filename));
  		f_acq=(float) f_acq/N_decimate;

		nx  = load_NI4472_data(filename, current_position, N_total, N_channels, n_X, 	N_decimate, &x);				
		ny  = load_NI4472_data(filename, current_position, N_total, N_channels, n_Y, 	N_decimate, &y);
		n_M = load_NI4472_data(filename, current_position, N_total, N_channels, n_excitation, N_decimate, &excitation);
		if (nx ==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
		if (ny ==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
		if (n_M==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
	   	if (nx!=ny)  return(win_printf_OK("error! not the same number of points in X and Y!"));	
		if (nx!=n_M) return(win_printf_OK("error! not the same number of points in X and M!"));	
		
		
// Detection de la zone de fluctuation: zone ou l'excitation est nulle!!!
		if (bool_criteria==1)
		{	i_start=(int)nx/2; i_end= (int)nx/2;
			for (i=(int)nx/2; i>0; i--)
				{	if (-epsilon>excitation[i]) i_start=i;
					if (epsilon<excitation[i]) i_start=i;
				}
			for (i=(int)nx; i<nx; i++)
				{	if (-epsilon>excitation[i]) i_end=i+1;
					if (epsilon<excitation[i]) i_end=i+1;
				}
		}
		else
		{	i_start=0;
			i_end=nx;
		}
		nx_out=i_end-i_start;
	//Trac� du rayon moyen par fichier!!!
		nx_out=i_end-i_start;
		phase=(float*)calloc(nx, sizeof(float));
		compute_phase(phase, x, y, nx, pendule_x_offset, pendule_y_offset, lambda, indice, bool_radian, conv_radian);
		free(x);
		free(y);
		tmp_phase=(float*)calloc(nx_out, sizeof(float));
		for (j=0; j<nx_out; j++) tmp_phase[j]=phase[i_start+j];
		free(phase);
		
		tmp_excitation=(float*)calloc(nx_out, sizeof(float));
		for (j=0; j<nx_out; j++) tmp_excitation[j]=excitation[i_start+j];
		free(excitation);
		
		if (bool_excitation_squared==1) for (j=0; j<nx_out; j++) tmp_excitation[j]=tmp_excitation[j]*tmp_excitation[j];
		
		if (bool_crossresponse==0)
			i=compute_response_from_float_data(current_response, tmp_phase, tmp_excitation, nx_out, f_acq, n_fft, n_overlap, bool_hanning);
		if (bool_crossresponse==1)
			i=compute_response_from_float_data_using_correlation(current_response, tmp_phase, tmp_excitation, nx_out, f_acq, n_fft, n_overlap, bool_hanning);
		if (i==0) return win_printf_OK("nothing done with your values !");
		if (bool_modulus==1)
		{	if (bool_sum_sinus==1)
			{	if (k!=0) // first file : we add a dataset
				{	if ((ds_modulus = create_and_attach_one_ds(op_modulus, (imax-imin), (imax-imin), 0)) == NULL)
					return win_printf_OK("cannot create dataset !");
				}
				for (i=imin ; i<imax ; i++)
   				{  	ds_modulus->xd[i] = current_response->f[i];
	       				ds_modulus->yd[i] = (float)sqrt( current_response->r[i][0]*current_response->r[i][0]
       										 + current_response->r[i][1]*current_response->r[i][1] );
    				}
			}
			else
			{	if (k!=0) // first file : we add a dataset
				{	if ((ds_modulus = create_and_attach_one_ds(op_modulus, n_f, n_f, 0)) == NULL)
					return win_printf_OK("cannot create dataset !");
				}
				for (j=0; j<n_f; j++)
				{	i = (int)(f_index[j]*n_fft/f_acq);
					ds_modulus->xd[j] = (float)f_index[j];
					ds_modulus->yd[j] = (float)sqrt( current_response->r[i][0]*current_response->r[i][0]
       										 + current_response->r[i][1]*current_response->r[i][1] );
   				}
   			}
	   		set_plot_x_title(op_modulus, "f (Hz)");
    			ds_modulus->treatement = my_sprintf(ds_modulus->treatement,"response function from file number %d done at time %s",
									k, date_stamp);
   	 	}
		if (bool_real==1)
		{	if (bool_sum_sinus==1)
			{	if (k!=0) // first file : we add a dataset
				{	if ((ds_real = create_and_attach_one_ds(op_real, (imax-imin), (imax-imin), 0)) == NULL)
					return win_printf_OK("cannot create dataset !");
				}
				for (i=imin ; i<imax ; i++)
   				{  	ds_real->xd[i] = current_response->f[i];
       				ds_real->yd[i] = (float)current_response->r[i][0];
    			}
			}
			else
			{	if (k!=0) // first file : we add a dataset
				{	if ((ds_real = create_and_attach_one_ds(op_real, n_f, n_f, 0)) == NULL)
					return win_printf_OK("cannot create dataset !");
				}
				for (j=0; j<n_f; j++)
				{	i = (int)(f_index[j]*n_fft/f_acq);
					ds_real->xd[j] = (float)f_index[j];
					ds_real->yd[j] = (float)current_response->r[i][0];
				}
			}
    		set_plot_x_title(op_real, "f (Hz)");
    		ds_real->treatement = my_sprintf(ds_real->treatement,"response function from file number %d done at time %s",
									k, date_stamp);
		}
		if (bool_imaginary==1)
		{	if (bool_sum_sinus==1)
			{	if (k!=0) // first file : we add a dataset
				{	if ((ds_imaginary = create_and_attach_one_ds(op_imaginary, (imax-imin), (imax-imin), 0)) == NULL)
					return win_printf_OK("cannot create dataset !");
				}
				for (i=imin ; i<imax ; i++)
				{  	ds_imaginary->xd[i] = current_response->f[i];
       				ds_imaginary->yd[i] = (float)current_response->r[i][1];
    			}
			}
			else
			{	if (k!=0) // first file : we add a dataset
				{	if ((ds_imaginary = create_and_attach_one_ds(op_imaginary, n_f, n_f, 0)) == NULL)
					return win_printf_OK("cannot create dataset !");
				}
				for (j=0; j<n_f; j++)
				{	i = (int)(f_index[j]*n_fft/f_acq);
					ds_imaginary->xd[j] = (float)f_index[j];
					ds_imaginary->yd[j] = (float)current_response->r[i][1];
				}
			}
    		set_plot_x_title(op_imaginary, "f (Hz)");
    		ds_imaginary->treatement = my_sprintf(ds_imaginary->treatement,"response function from file number %d done at time %s",
									k, date_stamp);
		}
		if (bool_phase==1)
		{	if (bool_sum_sinus==1)
			{	if (k!=0) // first file : we add a dataset
				{	if ((ds_phase = create_and_attach_one_ds(op_phase, (imax-imin), (imax-imin), 0)) == NULL)
						return win_printf_OK("cannot create dataset !");
				}
				for (i=imin ; i<imax ; i++)
				{  	ds_phase->xd[i] = current_response->f[i];
       				ds_phase->yd[i] = (float)atan2(current_response->r[i][1],current_response->r[i][0]);
    			}
			}
			else
			{	if (k!=0) // first file : we add a dataset
				{	if ((ds_phase = create_and_attach_one_ds(op_phase, n_f, n_f, 0)) == NULL)
						return win_printf_OK("cannot create dataset !");
				}
				for (j=0; j<n_f; j++)
				{	i = (int)(f_index[j]*n_fft/f_acq);
					ds_phase->xd[j] = (float)f_index[j];
					ds_phase->yd[j] = (float)atan2(current_response->r[i][1],current_response->r[i][0]);
				}
			}
    		set_plot_x_title(op_phase, "f (Hz)");
    		ds_phase->treatement = my_sprintf(ds_phase->treatement,"response function from file number %d done at time %s",
									k, date_stamp);
		}
		if (bool_real_inverse==1)
		{	if (bool_sum_sinus==1)
			{	if (k!=0) // first file : we add a dataset
				{	if ((ds_real_inverse = create_and_attach_one_ds(op_real_inverse, (imax-imin), (imax-imin), 0)) == NULL)
						return win_printf_OK("cannot create dataset !");
				}
				for (i=imin ; i<imax ; i++)
				{  	ds_real_inverse->xd[i] = current_response->f[i];
       				ds_real_inverse->yd[i] = (float)current_response->r[i][0];
   					ds_real_inverse->yd[i] /=(float)( current_response->r[i][0]*current_response->r[i][0]
   												 + current_response->r[i][1]*current_response->r[i][1] ); 
				}
    		}
    		else
    		{	if (k!=0) // first file : we add a dataset
					if ((ds_real_inverse = create_and_attach_one_ds(op_real_inverse, n_f, n_f, 0)) == NULL)
						return win_printf_OK("cannot create dataset !");
				for (j=0; j<n_f; j++)
				{	i = (int)(f_index[j]*n_fft/f_acq);
					ds_real_inverse->xd[j] = (float)f_index[j];
					ds_real_inverse->yd[j] = (float)current_response->r[i][0];
   					ds_real_inverse->yd[j] /=(float)( current_response->r[i][0]*current_response->r[i][0]
   												 + current_response->r[i][1]*current_response->r[i][1] ); 
				}
    		}
    		set_plot_x_title(op_real_inverse, "f (Hz)");
    		ds_real_inverse->treatement = my_sprintf(ds_real_inverse->treatement,
    				"response function from file number %d done at time %s",k, date_stamp);
		}
		if (bool_imaginary_inverse==1)
		{	if (bool_sum_sinus==1)
			{	if (k!=0) // first file : we add a dataset
				{	if ((ds_imaginary_inverse = create_and_attach_one_ds(op_imaginary_inverse, (imax-imin), (imax-imin), 0)) == NULL)
						return win_printf_OK("cannot create dataset !");
				}
				for (i=imin ; i<imax ; i++)
				{  	ds_imaginary_inverse->xd[i] = current_response->f[i];
   					ds_imaginary_inverse->yd[i] = -(float)current_response->r[i][1];
       					ds_imaginary_inverse->yd[i] /= (float)(current_response->r[i][0]*current_response->r[i][0]
       											 + current_response->r[i][1]*current_response->r[i][1] );
   				}
    		}
    		else
    		{	if (k!=0) // first file : we add a dataset
				{	if ((ds_imaginary_inverse = create_and_attach_one_ds(op_imaginary_inverse, n_f, n_f, 0)) == NULL)
						return win_printf_OK("cannot create dataset !");
				}
				for (j=0; j<n_f; j++)
				{	i = (int)(f_index[j]*n_fft/f_acq);
					ds_imaginary_inverse->xd[j] = (float)f_index[j];
					ds_imaginary_inverse->yd[j] = -(float)current_response->r[i][1];
      					ds_imaginary_inverse->yd[j] /= (float)( current_response->r[i][0]*current_response->r[i][0]
       											 + current_response->r[i][1]*current_response->r[i][1] );
  				}
    		}
   			set_plot_x_title(op_imaginary_inverse, "f (Hz)");
   			ds_imaginary_inverse->treatement = my_sprintf(ds_imaginary_inverse->treatement,
    			"response function from file number %d done at time %s",k, date_stamp);
		}
		if (bool_FDT==1)
		{	if (bool_sum_sinus==1)
			{	if (k!=0) // first file : we add a dataset
				{	if ((ds_FDT = create_and_attach_one_ds(op_FDT, (imax-imin), (imax-imin), 0)) == NULL)
						return win_printf_OK("cannot create dataset !");
				}
				for (i=imin ; i<imax ; i++)
				{  	ds_FDT->xd[i] = current_response->f[i];
   					ds_FDT->yd[i] = (float)current_response->r[i][1];
       				if (ds_FDT->xd[i]==0) ds_FDT->yd[i] = ds_FDT->yd[i];
					else ds_FDT->yd[i] /= (float)ds_FDT->xd[i];
   					
   				}
    		}
    		else
    		{	if (k!=0) // first file : we add a dataset
				{	if ((ds_FDT = create_and_attach_one_ds(op_FDT, n_f, n_f, 0)) == NULL)
						return win_printf_OK("cannot create dataset !");
				}
				for (j=0; j<n_f; j++)
				{	i = (int)(f_index[j]*n_fft/f_acq);
					
					ds_FDT->xd[j] = (float)f_index[j];
					ds_FDT->yd[j] = (float)current_response->r[i][1];
      				if (ds_FDT->xd[j]==0) ds_FDT->yd[j] = ds_FDT->yd[j];
					else ds_FDT->yd[j] /= (float)ds_FDT->xd[j];
       			}
    		}
   			set_plot_x_title(op_FDT, "f (Hz)");
   			ds_FDT->treatement = my_sprintf(ds_FDT->treatement,
    			"response function from file number %d done at time %s",k, date_stamp);
		}
		free(tmp_phase);free(tmp_excitation);
		
	}
	free(f_index);
	return refresh_plot(pr, UNCHANGED);
}
/* end of do response function */



//Fonction qui extrait d'une serie de dataset les points � une frequence donn�e
int do_extract_at_one_frequency(void)
{  //Declaration des variables
register int i,j;
	int	index;
	int 	n_ds=1;
	pltreg	*pr;
	O_p	*op, *op_new=NULL;
	d_s	*ds, *ds2;
	int  nh; 		
	float d_h, i_max_f, i_min_f;	
static float	f_min=0.5, f_max=1.5;
	int 	i_min, i_max;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	

	if (key[KEY_LSHIFT])
	{	 return win_printf_OK("This routine average the psd over a window around a frequency of some datasets\n"
				"a new dataset is created");
	}
	
	
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)  return win_printf_OK("cannot find data!");
	
    
	i=win_scanf("{\\pt14\\color{yellow} Extraction at one frequency}\n\n"
					"Window of averaging :\n"
					"f_{min} = %8f Hz \n"
					"f_{max} = %8f Hz \n", &f_min, &f_max);
	if (i==CANCEL) return(D_O_K);
	if (f_max<f_min) 
	return win_printf_OK("Attention a l'ordre des frequences... tu es fatigue...");
	
	n_ds  = op->n_dat;
	if ((op_new = create_and_attach_one_plot(pr, n_ds, n_ds, 0))==NULL)
	return win_printf_OK("cannot create plot !");
	ds2= op_new->dat[0];
	
	if (ds == NULL)   return (win_printf_OK("can't find data set"));
	nh=ds->nx;
	d_h=(float)ds->xd[nh-1];
	d_h/=(float)(nh-1);
	i_min_f=(float)f_min/(float)d_h;
	i_min=(int) i_min_f;
	
	i_max_f=(float)f_max/(float)d_h;
	i_max=(int) i_max_f;
	win_printf("The extract frequency is %f Hz",(float)(i_max+i_min)*d_h/2.);
	for (i=0; i<n_ds; i++)
	{	ds =  op->dat[i];
		ds2->xd[i] = i;
		for (j=i_min; j<i_max; j++)
		{	ds2->yd[i] += (float)ds->yd[j];
		}
		ds2->yd[i] /= (i_max-i_min);		
	}
	inherit_from_ds_to_ds(ds2, ds);
	ds2->treatement = my_sprintf(ds2->treatement,"extract frequency is %f Hz\n"
												" averaged between %f Hz and %f Hz", (float)((i_max+i_min)*d_h/2), f_min, f_max);

	set_plot_y_title(op_new, "%s", op->y_title);
	set_plot_x_title(op_new, "time (file number)");
	
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}	
/* end of do_extract_at_one_frequency */

//do_compute_resonance_frequency
int do_plot_resonance_frequency(void)
{  //Declaration des variables
register int i,j;
	int	index;
	int 	n_ds=1;
	pltreg	*pr;
	O_p	*op, *op_new=NULL;
	d_s	*ds, *ds2;
	int  nh; 		
	float d_h, i_max_f, i_min_f, f_0, y_max;	
static float	f_min=0.5, f_max=1.5;
	int 	i_min, i_max;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;
	

	if (key[KEY_LSHIFT])
	{	 return win_printf_OK("This routine find the resonance frequencys\n"
				"a new dataset is created");
	}
	
	
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)  return win_printf_OK("cannot find data!");
	
    
	i=win_scanf("{\\pt14\\color{yellow} Resonance}\n\n"
					"Window :\n"
					"f_{min} = %8f Hz \n"
					"f_{max} = %8f Hz \n", &f_min, &f_max);
	if (i==CANCEL) return(D_O_K);
	if (f_max<f_min) 
	return win_printf_OK("Attention a l'ordre des frequences... tu es fatigue...");
	
	n_ds  = op->n_dat;
	if ((op_new = create_and_attach_one_plot(pr, n_ds, n_ds, 0))==NULL)
	return win_printf_OK("cannot create plot !");
	ds2= op_new->dat[0];
	
	if (ds == NULL)   return (win_printf_OK("can't find data set"));
	nh=ds->nx;
	d_h=(float)ds->xd[nh-1];
	d_h/=(float)(nh-1);
	i_min_f=(float)f_min/(float)d_h;
	i_min=(int) i_min_f;
	
	i_max_f=(float)f_max/(float)d_h;
	i_max=(int) i_max_f;
	
	for (i=0; i<n_ds; i++)
	{	ds =  op->dat[i];
		ds2->xd[i] = i;
		f_0=0;
		y_max=0;
		for (j=i_min; j<i_max; j++)
		{	if (ds->yd[j]>y_max)
			{	y_max=ds->yd[j];
				f_0=ds->xd[j];
			}
		}
		ds2->yd[i] = f_0;		
	}
	inherit_from_ds_to_ds(ds2, ds);
	
	set_plot_y_title(op_new, "resonance frequency (Hz)");
	set_plot_x_title(op_new, "time (file number)");
	
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

/*
float date_stamp_to_time(char date_stamp)
{	int	n=0, m=0;
	register int i;
	int	err;
	float x, x2, x3, k;
	float	*index, *index_tmp;
	char	tmp[128];
	char	c, c2;
	int	THE_SIZE_MAX=512;

	tmp=(float*)calloc(THE_SIZE_MAX, sizeof(float));

	m=strlen(str);
	for (i=0; i<m; i++)
	{
		if (sscanf(str+i,"%f",&x)!=0)		// on a trouve un entier
		{	tmp[n]=x;
			n++;

			sprintf(tmp,"%g",x);//	win_printf_OK("i found %d and the size it occupied was %d", j, strlen(tmp));
			i+=strlen(tmp);

		if (sscanf(str+i,"%c%f",&c,&x2)==2)
		{	if ( (c=='/') || (c==':') )
			{//	win_printf("/ ou : detecte");
				i++;

				sprintf(tmp,"%g",x2);
				i+=strlen(tmp);

				err=sscanf(str+i,"%c%f",&c2,&x3);
				if ( (err==2) && (c2==':') )
		  		{//	win_printf("alors j'ai une boucle de %d a %d avec un pas de %d", j, j3, j2);
					sprintf(tmp,"%g",x3);
					i+=strlen(tmp);

					if ( (x<x3) && (x2>0) && (((x3-x)/x2)+n<THE_SIZE_MAX) )
					for (k=x+x2; k<=x3; k+=x2, n++) index_tmp[n]=k;
					else {       if (index_tmp) free(index_tmp);
                                 return(NULL);
                         }
				}
				else 
				{//	win_printf("alors j'ai une boucle de %d a %d", j, j2);
					if ( (x<x2) && (x2-x+n<THE_SIZE_MAX) )
					for (k=x+1; k<=x2; k++, n++) index_tmp[n]=k;
					else {      if (tmp) free(tmp);
                                return(NULL);
                         }
				}
				
			}
		}

		}	
	}

	index=(float*)calloc(n, sizeof(float));
	for (i=0; i<n; i++)	index[i]=tmp[i]; 
	if (tmp) free(tmp);

	*N=n;
	return index;
}	
*/

void spit(char *message)
{	size_t	l_max=55;
	char 	white_string[64]="                                              ";
	
	if (strlen(message)<l_max) 	strncat (message, white_string, l_max-strlen(message));
	textout_ex(screen, font, message, 40, 40, makecol(100,155,155), makecol(2,1,1));
	return;
}


MENU *aging_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
	add_item_to_menu(mn,"plot_psd_fluctuations_from_NI4472_files",	do_fluctuations_files,		NULL, 0, NULL);
	add_item_to_menu(mn,"plot_response_function_from_NI4472_files",	do_response_files,		NULL, 0, NULL);
	add_item_to_menu(mn,"extract_at one frequency",	do_extract_at_one_frequency,		NULL, 0, NULL);
	add_item_to_menu(mn,"find_resonance_frequency",	do_plot_resonance_frequency,		NULL, 0, NULL);
	
	return mn;
}


int	aging_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("aging", NULL, aging_plot_menu(), 0, NULL);
	return D_O_K;
}


int	aging_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"aging",	NULL, NULL);
	return D_O_K;
}
#endif

