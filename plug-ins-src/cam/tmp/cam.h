#ifndef CAM_H_
#define CAM_H_

#define   X_MAX_LIM    512
#define   Y_MAX_LIM    512

#ifdef CAM_C_
PXV_FUNC(MENU*, cam_image_menu, (void));
PXV_FUNC(int, cam_main, (int argc, char **argv));

int x_max = 100 , y_max = 100;
char tree[X_MAX_LIM][Y_MAX_LIM], fungus[X_MAX_LIM][Y_MAX_LIM], orchids[X_MAX_LIM][Y_MAX_LIM];

long seed = (long) 1;

#else
extern int x_max, y_max;
extern char tree[X_MAX_LIM][Y_MAX_LIM], fungus[X_MAX_LIM][Y_MAX_LIM], orchids[X_MAX_LIM][Y_MAX_LIM];

extern long seed;

#endif


#endif

