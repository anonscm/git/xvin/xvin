/** \file cam.c
    \cellular automaton model
    \author Thomas Julou
*/
#ifndef CAM_C_
#define CAM_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 
//# include "ran1.h"
# include "../nrutil/nrutil.h"
# include "simul.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "cam.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


void refresh_display(imreg* imr)
{
  O_i* image;
  union pix *data;
  int i,j;

  image = imr->o_i[imr->cur_oi];
  data = image->im.pixel;  

  for (i=0 ; i<y_max ; i++)
  for (j=0 ; j<x_max ; j++)
    data[i].rgb[j].r = data[i].rgb[j].g = data[i].rgb[j].b = (unsigned char) tree[i][j];
  
  refresh_image(imr, imr->cur_oi);
  find_zmin_zmax(image);
}

int call(void)
{
  imreg* imr;
  O_i* image;
  int i, seed_int;
  float prop = 0.5;

  if(updating_menu_state != 0)	return D_O_K;

  seed_int = (int) seed;
  i = win_scanf("Please enter\na seed (6 digits max)   %6d\nthe lattice size (less than 1024)   %6d\nthe initial prop of trees (between 0 and 1)   %6f",
  		&seed_int, &x_max, &prop);
  if (i == CANCEL)
    return OFF;
  else if (x_max > 512)
    {
      win_printf("Simulation will exit: lattice size excess 512!");
      return OFF;
    }
  else
    {
      y_max = x_max;
      seed = (long int) -labs(seed_int);
    }
 
  imr = create_and_register_new_image_project(0,0,100,100);
  image = imr->o_i[imr->cur_oi];
  alloc_one_image(image,x_max,y_max,IS_RGB_PICTURE);

  for (i=0; i<100; i++)
    {
      rand_lattice();
      refresh_display(imr);
    }

  win_printf("prop was %6f", prop);

  return D_O_K;
}

/** Menu function for \a cam.c */
MENU *cam_image_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)
    return mn;
  add_item_to_menu(mn,"New Simulation", call,NULL,0,NULL);
  return mn;
}

/** Main function of \&a cam.c */
int	cam_main(int argc, char **argv)
{
  imreg* imr;
  O_i* image;
  int i,j;
  union pix *data;
  int nx, ny;
	
  nx = 128;
  ny = 128;
	
  if(updating_menu_state != 0)	return D_O_K;
  
  imr = create_and_register_new_image_project(0,0,100,100);
    
  image = imr->o_i[imr->cur_oi];
  alloc_one_image(image,nx,ny,IS_RGB_PICTURE);
    
  data = image->im.pixel;  
  for (i=0 ; i<ny ; i++)
    {
      for (j=0 ; j<nx ; j++)
	{
	  //data[i].fl[j] = i+j;
	  data[i].rgb[j].r = data[i].rgb[j].g = data[i].rgb[j].b = (unsigned char) (i+j);
	}
    }
  
  smooth_interpol = 0;
  refresh_image(imr, imr->cur_oi);
  find_zmin_zmax(image);

  add_image_treat_menu_item("CAM", NULL, cam_image_menu(), 0, NULL);
  return D_O_K;
}

/** Menu unload function for \a cam.c */
int	cam_unload(int argc, char **argv)
{
  remove_item_to_menu(image_treat_menu, "CAM", NULL, NULL);
  return D_O_K;
}
#endif



  // 	imr->one_i = imr->o_i[imr->cur_oi];
  //	image->need_to_refresh |= ALL_NEED_REFRESH;
  //	imr->screen_scale=PIXEL_1;

/*   set_oi_horizontal_extend(image, 1.5); */
/*   set_oi_vertical_extend(image, 0.15); */
/*   smooth_interpol = 0; */
/*   refresh_image(imr, imr->cur_oi); */
/*   find_zmin_zmax(image); */
