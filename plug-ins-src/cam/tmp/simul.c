#ifndef SIMUL_C_
#define SIMUL_C_

# include "cam.h"

void rand_lattice(void)
{
  int i, j;
  for (i = 0; i < x_max; i++)
    {
      for (j = 0; j < y_max; j++)
	{
	  tree[i][j] = (char) (ran1(&seed)*256);
	}
    }
}

#endif
