#ifndef SIMUL_C_
#define SIMUL_C_
 
# include "allegro.h"
# include "xvin.h"

# include "cam.h"
# include "simul.h"
# include "SFMT.h"


/*******************************************************************************
* Procedure for initializing the lattice properties (neighbourhood)            *
* The first ngb (id 0) is on the top left corner                               *
*******************************************************************************/
int borderCheck(wood *w, int i, char c)
{
  switch(c)
    {
      case X:
	if (i < 0)       i = w->grid_width + i;
	if (i >= w->grid_width)  i = i - w->grid_width;
	break;
      case Y:
	if (i < 0)       i = w->grid_width + i;
	if (i >= w->grid_width)  i = i - w->grid_width;
	break;
    }
  return i;
}

int initLattice(wood *w)
{
  int x, y, i;
  switch(NBR_MAX)
    {
      case 4:
	break;
      case 8:
	for (x=0; x<w->grid_width; x++)
	for (y=0; y<w->grid_width; y++)
	  {
	    for (i=0; i<NBRL_MAX; i++)
	    switch(i)
	      {
	        case 0:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x-1, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y-1, Y);
		  break;
	        case 1:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x,   X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y-1, Y);
		  break;
	        case 2:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x+1, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y-1, Y);
		  break;
	        case 3:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x+1, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y,   Y);
		  break;
	        case 4:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x+1, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y+1, Y);
		  break;
	        case 5:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x,   X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y+1, Y);
		  break;
	        case 6:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x-1, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y+1, Y);
		  break;
	        case 7:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x-1, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y,   Y);
		  break;
	        case 8:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x-2, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y-2, Y);
		  break;
	        case 9:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x-1, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y-2, Y);
		  break;
	        case 10:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x,   X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y-2, Y);
		  break;
	        case 11:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x+1, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y-2, Y);
		  break;
	        case 12:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x+2, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y-2, Y);
		  break;
	        case 13:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x+2, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y-1, Y);
		  break;
	        case 14:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x+2, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y,   Y);
		  break;
	        case 15:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x+2, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y+1, Y);
		  break;
	        case 16:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x+2, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y+2, Y);
		  break;
	        case 17:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x+1, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y+2, Y);
		  break;
	        case 18:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x,   X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y+2, Y);
		  break;
	        case 19:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x-1, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y+2, Y);
		  break;
	        case 20:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x-2, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y+2, Y);
		  break;
	        case 21:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x-2, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y+1, Y);
		  break;
	        case 22:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x-2, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y,   Y);
		  break;
	        case 23:
		  w->grid[x][y].nbr[i][X] = borderCheck(w, x-2, X);
		  w->grid[x][y].nbr[i][Y] = borderCheck(w, y-1, Y);
		  break;
	      }
	  }
	break;
    }
  return D_O_K;
}

/*******************************************************************************
 * Procedure for initiating a new replicate: manage allocation, initialization *
 ******************************************************************************/
int new_replicate(simul *sim, imreg *imr, pltreg *pr)
{
  wood *w;
  O_i *oi;
  O_p *op;
  char oi_filename[16];
  int i, j;  

  // memory allocation for wood    
  w = (wood*) calloc(1, sizeof(wood));
  if (w == NULL)	return win_printf_OK("cannot create wood structure");

  w->grid = (cell **) calloc(sim->grid_width, sizeof(cell*));
  if (w->grid == NULL)	return win_printf_OK("cannot allocate grid structure");
  for (i = 0; i < sim->grid_width; i++)
    {
      w->grid[i] = (cell *) calloc(sim->grid_width, sizeof(cell));
      if (w->grid[i] == NULL)	return win_printf_OK("cannot allocate cells");
    }
  for (i = 0; i < sim->grid_width; i++)
    for (j = 0; j < sim->grid_width; j++)
      {
	w->grid[i][j].orchid = (orchidType *) calloc(sim->max_orch, sizeof(orchidType));
	if (w->grid[i][j].orchid == NULL)	return win_printf_OK("cannot allocate orchids");
      }

  initWood(w);
  if (sim->cur_rep > 0)
    w->idle_state = 1;

  // create and allocate corresponding plots ds
  for (i = 0; i < NSPP; i++)
    {
      op = pr->o_p[i];
      create_and_attach_one_ds(op, sim->t_max+1, sim->t_max+1, 1);
      set_ds_source(op->dat[op->cur_dat], "replicate #%03d", sim->cur_rep);
    }

  // create and allocate corresponding oi
  oi = create_and_attach_movie_to_imr (imr, sim->grid_width, sim->grid_width, IS_RGB_PICTURE, (int) sim->t_max + 1);
  if (oi == NULL) return win_printf_OK("cannot create new movie for simul");
  imr->cur_oi = imr->n_oi - 1;

  // define as movie on disk
  sprintf(oi_filename, "rep_%03d.gr", sim->cur_rep);
  set_oi_filename(oi, oi_filename);
  set_oi_path(oi, sim->path);
  //   oi->filename = Mystrdupre(oi->filename,"rep_001.gr");
  //   oi->dir = Mystrdupre(oi->dir,sim->path);
  //oi->im.movie_on_disk = 1;
  
  // set display options
  set_oi_mode(oi, TRUE_RGB);
  set_zmin_zmax_values(oi, 0, w->max_orch-1);
  select_image_of_imreg_and_display(imr, imr->cur_oi);

  refresh_wood_display(imr, pr, oi, w);

  // attach wood to oi
  oi->im.use.stuff = (void*)w;
  oi->im.use.to = IS_WOOD;

  // set idle action to manage iteration
  oi->oi_idle_action = simulate_n_step;
   
  return D_O_K;
}



/*******************************************************************************
 * Procedure for initializing the spatial layout and the events rates           *
 *******************************************************************************/
int initCell(simul *sim, wood *w, int x, int y)
{
  int ind;

  if (genrand_res53() < sim->param[TREE].Freq)
    w->grid[x][y].nbtree = 1;
  else
    w->grid[x][y].nbtree = 0;

  w->grid[x][y].nborchid = 0;
  for (ind = 0; ind < w->max_orch; ind++)
    if (genrand_res53() < sim->param[ORCHID].Freq/w->max_orch)
      w->grid[x][y].nborchid += 1;

  if (genrand_res53() < sim->param[FUNGUS].Freq)
    w->grid[x][y].nbfungus = 1;
  else
    w->grid[x][y].nbfungus = 0;

  return D_O_K;
}

int initWood(wood *w)
{
  imreg* imr;
  O_i* oi;
  simul* sim;
  int x, y, ind;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    return win_printf_OK("cannot find data");

  if ((sim = find_simul_in_oi(imr->o_i[0])) == NULL)
    return win_printf_OK("cannot find simul (init wood)");

  // Inherit params from simul structure
  w->t = 0;
  w->t_max = sim->t_max;
  w->extinction = 0;
  w->c_f = 0;
  w->grid_width = sim->grid_width;
  w->max_orch = sim->max_orch;
  w->idle_state = 0;
  //w->freq_disp = sim->freq_disp; //???

  // Init lattice
  initLattice(w);

  // Init spatial layout and set vars to 0
  for (x = 0; x < sim->grid_width; x++)
    for (y = 0; y < sim->grid_width; y++)
      {
	initCell(sim, w, x, y);

	// Set all rates and params to 0
	w->grid[x][y].ngb[TREE]   = 0;
	w->grid[x][y].ngb[ORCHID] = 0;
	w->grid[x][y].ngb[FUNGUS] = 0;

	w->grid[x][y].tree.birthR = 0;
	w->grid[x][y].tree.deathR = 0;
	w->grid[x][y].tree.con_fungus = 0;
      
	for (ind = 0; ind < w->max_orch; ind++)
	  {
	    w->grid[x][y].orchid[ind].dispR = 0;
	    w->grid[x][y].orchid[ind].birthR = 0;
	    w->grid[x][y].orchid[ind].deathR = 0;
	    w->grid[x][y].orchid[ind].theta  = 0;
	    w->grid[x][y].orchid[ind].carbon = 0;
	  }

	w->grid[x][y].fungus.birthR = 0;
	w->grid[x][y].fungus.deathR = 0;
	w->grid[x][y].fungus.carbon = 0;
      }

  sim->Isum = sim->param[FUNGUS].I0 + sim->param[ORCHID].I0;
  sim->Isum *= sim->grid_width * sim->grid_width;

  // Calculate initial vars
  for (x = 0; x < sim->grid_width; x++)
    for (y = 0; y < sim->grid_width; y++)
      calculateNeighbours(w, x, y);

  if (sim->is_cmn)
    {
      for (x = 0; x < sim->grid_width; x++)
	for (y = 0; y < sim->grid_width; y++)
	  updateConFungus(w, x, y);

      for (x = 0; x < sim->grid_width; x++)
	for (y = 0; y < sim->grid_width; y++)
	  carbon(sim, w, x, y);
    }

  for (x = 0; x < sim->grid_width; x++)
    for (y = 0; y < sim->grid_width; y++)
      updateRates(sim, w, x, y);

  return D_O_K;
}

/*******************************************************************************
 * Procedure for counting and updating the number of neighbours                 *
 *******************************************************************************/
int calculateNeighbours(wood *w, int x, int y)
{
  cell *f_cell;
  int ind;

  f_cell = &w->grid[x][y];

  f_cell->ngb[TREE]   = f_cell->nbtree;
  f_cell->ngb[ORCHID] = f_cell->nborchid;
  f_cell->ngb[FUNGUS] = f_cell->nbfungus;

  for (ind = 0; ind < NBR_MAX; ind++) 
    {
      f_cell->ngb[TREE]   += w->grid[f_cell->nbr[ind][X]][f_cell->nbr[ind][Y]].nbtree;
      f_cell->ngb[ORCHID] += w->grid[f_cell->nbr[ind][X]][f_cell->nbr[ind][Y]].nborchid;
      f_cell->ngb[FUNGUS] += w->grid[f_cell->nbr[ind][X]][f_cell->nbr[ind][Y]].nbfungus;
    }

  return D_O_K;
}

int addOneNgbInNgbhood(wood *w, int x, int y, char sp)
{
  cell *f_cell;
  int ind, spp = (int) sp;

  f_cell = &w->grid[x][y];

  f_cell->ngb[spp] += 1;
  for (ind = 0; ind < NBR_MAX; ind++) 
    w->grid[f_cell->nbr[ind][X]][f_cell->nbr[ind][Y]].ngb[spp] += 1;

  return D_O_K;
}

int removeOneNgbInNgbhood(wood *w, int x, int y, char sp)
{
  cell *f_cell;
  int ind, spp = (int) sp;

  f_cell = &w->grid[x][y];

  f_cell->ngb[spp] -= 1;
  for (ind = 0; ind < NBR_MAX; ind++) 
    w->grid[f_cell->nbr[ind][X]][f_cell->nbr[ind][Y]].ngb[spp] -= 1;

  return D_O_K;
}

/*******************************************************************************
 * Procedure for implementing the carbon fluxes                                 *
 *******************************************************************************/
int updateConFungus(wood *w, int x, int y)
{
  cell *f_cell;

  f_cell = &w->grid[x][y];

  if (f_cell->nbtree > 0)
    {
      if (f_cell->nbfungus > 0)
	f_cell->tree.con_fungus = f_cell->ngb[FUNGUS];
    }
  else
    f_cell->tree.con_fungus = 0;

  return D_O_K;
}

int carbon(simul *sim, wood* w, int x, int y)
{
  cell *f_cell, *n_cell;
  int ind, r_ind;
  char remaining_orch, already_visited[sim->max_orch];

  f_cell = &w->grid[x][y];

  // Fungus' carbon
  f_cell->fungus.carbon = -ERROR;
  if (f_cell->nbfungus > 0)
    {
      //win_printf_OK("dbg t: %d  f: %d", f_cell->nbtree, f_cell->tree.con_fungus);
      if ((f_cell->nbtree > 0) && (f_cell->tree.con_fungus > 0))
	f_cell->fungus.carbon += f_cell->nbtree / f_cell->tree.con_fungus;
      for (ind = 0; ind < NBR_MAX; ind++) 
	{
	  n_cell = &w->grid[f_cell->nbr[ind][X]][f_cell->nbr[ind][Y]];
	  if (n_cell->nbfungus > 0)
	    {
	      //win_printf_OK("dbg t: %d  f: %d", n_cell->nbtree, n_cell->tree.con_fungus);
	      if ((n_cell->nbtree > 0) && (n_cell->tree.con_fungus > 0))
		f_cell->fungus.carbon += n_cell->nbtree / n_cell->tree.con_fungus;
	    }
	}
    }
	
  // Orchids' carbon
  for (ind = 0; ind < sim->max_orch; ind++)
    {
      f_cell->orchid[ind].carbon = -ERROR;
      already_visited[ind] = 0;
    }
	
  remaining_orch = f_cell->nborchid;
  while (remaining_orch > 0)
    {	
      //win_printf_OK("dbg while %d", remaining_orch);
      // Random choice of the focal orchid individual
      r_ind = (int) (genrand_res53() * (float) remaining_orch); // r_ind = position in remaining orch array
      for (ind = 0; ind <= r_ind; ind++)
	if (already_visited[ind])
	  r_ind += 1;
      if (r_ind >= f_cell->nborchid) win_printf_OK("Error at time %4.2f: r_ind > nborch", w->t);
      already_visited[r_ind] = 1;
      remaining_orch -= 1;

      if (f_cell->fungus.carbon > f_cell->orchid[r_ind].theta)
	{
	  f_cell->orchid[r_ind].carbon = f_cell->orchid[r_ind].theta;
	  f_cell->fungus.carbon -= f_cell->orchid[r_ind].theta;
	}
      else
	{
	  f_cell->orchid[r_ind].carbon = f_cell->fungus.carbon;
	  f_cell->fungus.carbon = -ERROR;
	}
    }

  return D_O_K;
}

/*******************************************************************************
 * Procedure for updating the events rates                                      *
 *******************************************************************************/
int updateRates(simul *sim, wood *w, int x, int y)
{
  cell *f_cell;
  int ind;

  f_cell = &w->grid[x][y];

  // Calculate the event rates
  if (f_cell->nbtree > 0)
    {
      f_cell->tree.birthR = sim->param[TREE].B0;
      f_cell->tree.deathR = sim->param[TREE].D0;
 	  
      f_cell->tree.birthR -= sim->param[TREE].B1[TREE] * (f_cell->ngb[TREE] - 1);
      f_cell->tree.birthR -= sim->param[TREE].B1[ORCHID] * f_cell->ngb[ORCHID];
      f_cell->tree.birthR -= sim->param[TREE].B1[FUNGUS] * f_cell->ngb[FUNGUS];
      f_cell->tree.deathR += sim->param[TREE].D1[TREE] * (f_cell->ngb[TREE] - 1);	  
      f_cell->tree.deathR += sim->param[TREE].D1[ORCHID] * f_cell->ngb[ORCHID];	  
      f_cell->tree.deathR += sim->param[TREE].D1[FUNGUS] * f_cell->ngb[FUNGUS];	  

      if (f_cell->tree.birthR < ERROR)   f_cell->tree.birthR = 0.0;
      if (f_cell->tree.deathR < ERROR)   f_cell->tree.deathR = 0.0;
      // Log output
    }
  else
    {
      f_cell->tree.birthR = 0.0;
      f_cell->tree.deathR = 0.0;
    }

  for (ind = 0; ind < f_cell->nborchid; ind++)
    {
      f_cell->orchid[ind].birthR = sim->param[ORCHID].B0;
      f_cell->orchid[ind].deathR = sim->param[ORCHID].D0;

      f_cell->orchid[ind].birthR -= sim->param[ORCHID].B1[TREE] * f_cell->ngb[TREE];
      f_cell->orchid[ind].birthR -= sim->param[ORCHID].B1[ORCHID] * (f_cell->ngb[ORCHID] - 1);
      f_cell->orchid[ind].birthR -= sim->param[ORCHID].B1[FUNGUS] * f_cell->ngb[FUNGUS];
      f_cell->orchid[ind].deathR += sim->param[ORCHID].D1[TREE] * f_cell->ngb[TREE];	  
      f_cell->orchid[ind].deathR += sim->param[ORCHID].D1[ORCHID] * (f_cell->ngb[ORCHID] - 1);	  
      f_cell->orchid[ind].deathR += sim->param[ORCHID].D1[FUNGUS] * f_cell->ngb[FUNGUS];

      if (f_cell->orchid[ind].birthR < ERROR)   f_cell->orchid[ind].birthR = 0.0;
      if (f_cell->orchid[ind].deathR < ERROR)   f_cell->orchid[ind].deathR = 0.0;

      f_cell->orchid[ind].dispR = sim->disp_init * f_cell->orchid[ind].birthR;
      // Log output
    }
  for (ind = f_cell->nborchid; ind < w->max_orch; ind++)
    {
      f_cell->orchid[ind].birthR = 0.0;
      f_cell->orchid[ind].deathR = 0.0;
      f_cell->orchid[ind].dispR = 0.0;
    }

  if (f_cell->nbfungus > 0)
    {
      f_cell->fungus.birthR = sim->param[FUNGUS].B0;
      f_cell->fungus.deathR = sim->param[FUNGUS].D0;

      f_cell->fungus.birthR -= sim->param[FUNGUS].B1[TREE] * f_cell->ngb[TREE];
      f_cell->fungus.birthR -= sim->param[FUNGUS].B1[ORCHID] * f_cell->ngb[ORCHID];
      f_cell->fungus.birthR -= sim->param[FUNGUS].B1[FUNGUS] * (f_cell->ngb[FUNGUS] - 1);
      f_cell->fungus.deathR += sim->param[FUNGUS].D1[TREE] * f_cell->ngb[TREE];	  
      f_cell->fungus.deathR += sim->param[FUNGUS].D1[ORCHID] * f_cell->ngb[ORCHID];	  
      f_cell->fungus.deathR += sim->param[FUNGUS].D1[FUNGUS] * (f_cell->ngb[FUNGUS] - 1);

      if (f_cell->fungus.birthR < ERROR)   f_cell->fungus.birthR = 0.0;
      if (f_cell->fungus.deathR < ERROR)   f_cell->fungus.deathR = 0.0;
      // Log output
    }
  else
    {
      f_cell->fungus.birthR = 0.0;
      f_cell->fungus.deathR = 0.0;
    }

  return D_O_K;
}

/*******************************************************************************
 * Procedure for implementing the events                                        *
 *******************************************************************************/
int choose_event(simul *sim, wood *w, event *ev)
{
  int    ind, x = 0, y = 0;
  float	 Bsum, Dsum, Ssum, sum, add = 0;
  float rn;
  
  // Sum of birth rates
  Bsum = 0.0;
  for (x = 0; x < sim->grid_width; x++)
    for (y = 0; y < sim->grid_width; y++)
      {
	Bsum += w->grid[x][y].tree.birthR;
	for (ind = 0; ind < w->grid[x][y].nborchid; ind++)
	  Bsum += w->grid[x][y].orchid[ind].birthR;
	Bsum += w->grid[x][y].fungus.birthR;
      }

  // Sum of death rates
  Dsum = 0.0;
  for (x = 0; x < sim->grid_width; x++)
    for (y = 0; y < sim->grid_width; y++)
      {
        Dsum += w->grid[x][y].tree.deathR;
        for (ind = 0; ind < w->grid[x][y].nborchid; ind++)
          Dsum += w->grid[x][y].orchid[ind].deathR;
        Dsum += w->grid[x][y].fungus.deathR;
      }

  // Sum of dispersal rates
  Ssum = 0.0;
  for (x = 0; x < sim->grid_width; x++)
    for (y = 0; y < sim->grid_width; y++)
      for (ind = 0; ind < w->grid[x][y].nborchid; ind++)
          Ssum += w->grid[x][y].orchid[ind].dispR;
  
  // Sum of all rates
  sum = Bsum + Dsum + sim->Isum + Ssum;  // S stay for dispersal

  // Random time to next event (exponential distribution of waiting times)
  if (sum > ERROR)
    ev->dt = -log(1.0 - genrand_res53()) / sum;
  else
    {
      // END OF SIMULATION
      //win_printf("Sum of rates is zero: exiting from program");
      w->idle_state = 0;
      w->extinction = 1;
      w->t = w->t_max;
      return D_O_K;
      //exit(1);
    }

  // Random choice of type of event
  rn = genrand_res53() * sum;
  if (rn <  Bsum)	                   ev->type = 'b';
  else if (rn < Bsum + Dsum)	           ev->type = 'd';
  else if (rn < Bsum + Dsum + sim->Isum)   ev->type = 'i';
  else 	                                   ev->type = 's';

  // Random choice of species and cell to which event happens
  ev->sp = 0;
  rn = genrand_res53();
  switch(ev->type)
    {
    case 'b':
      rn = rn * Bsum;
      add = 0.0;  // vary between 0 and Bsum
      for (x = 0; x < sim->grid_width; x++)
      for (y = 0; y < sim->grid_width; y++)
        {
          if (rn < add + w->grid[x][y].tree.birthR)
            {
              ev->sp = TREE;
              goto label_b;
            }
          else
            add += w->grid[x][y].tree.birthR;
    
          for (ind = 0; ind < w->grid[x][y].nborchid; ind++)
            if (rn < add + w->grid[x][y].orchid[ind].birthR)    
            {
              ev->sp = ORCHID;
	      ev->orch_ind = ind;
              goto label_b;
            }
            else
              add += w->grid[x][y].orchid[ind].birthR;
    
          if (rn < add + w->grid[x][y].fungus.birthR)
          {
            ev->sp = FUNGUS;
            goto label_b;
          }
          else
            add += w->grid[x][y].fungus.birthR;
        }
    label_b:break;	

    case 'd':
      rn = rn * Dsum;
      add = 0.0;  // vary between 0 and Dsum
      for (x = 0; x < sim->grid_width; x++)
      for (y = 0; y < sim->grid_width; y++)
      {
        if (rn < add + w->grid[x][y].tree.deathR)
        {
          ev->sp = TREE;
          goto label_d;
        }
        else
          add += w->grid[x][y].tree.deathR;
  
        for (ind = 0; ind < w->grid[x][y].nborchid; ind++)
          if (rn < add + w->grid[x][y].orchid[ind].deathR)
          {
            ev->sp = ORCHID;
	    ev->orch_ind = ind;
            goto label_d;
          }
          else
            add += w->grid[x][y].orchid[ind].deathR;
  
        if (rn < add + w->grid[x][y].fungus.deathR)
        {
          ev->sp = FUNGUS;
          goto label_d;
        }
        else
          add += w->grid[x][y].fungus.deathR;
      }
    label_d:break;	

    case 'i':
      rn = rn * sim->Isum;
      add = 0.0;  // vary between 0 and Isum
      for (x = 0; x < sim->grid_width; x++)
      for (y = 0; y < sim->grid_width; y++)
      {
        if (rn < add + sim->param[FUNGUS].I0)
        {
          ev->sp = FUNGUS;
          goto label_i;
        }
        else
          add += sim->param[FUNGUS].I0;
  
	if (rn < add + sim->param[ORCHID].I0)
          {
            ev->sp = ORCHID;
            goto label_i;
          }
          else
            add += sim->param[ORCHID].I0;
      }
    label_i:break;	

    case 's':
      rn = rn * Ssum;
      add = 0.0;  // vary between 0 and Dsum
      for (x = 0; x < sim->grid_width; x++)
      for (y = 0; y < sim->grid_width; y++)
        for (ind = 0; ind < w->grid[x][y].nborchid; ind++)
          if (rn < add + w->grid[x][y].orchid[ind].dispR)
          {
            ev->sp = ORCHID;
	    ev->orch_ind = ind;
            goto label_s;
          }
          else
            add += w->grid[x][y].orchid[ind].dispR;
      
    label_s:break;	
    }

  // Record cell to which event happens
  ev->x = x;
  ev->y = y;
  if (x >= sim->grid_width || y >= sim->grid_width)
    win_printf_OK("end:x or y event is grid size (or more)");

  return D_O_K;
}

int birth(simul* sim, wood* w, event* evt, modif* mdf)
{
  int ind, x_birth = 0, y_birth = 0;
  
  // Birth occurs at a random location in the neighbourhood (if not already present)
  switch(evt->sp)
    {
    case TREE:
      ind = genrand_res53() * NBR_MAX;
      x_birth = w->grid[evt->x][evt->y].nbr[ind][X];
      y_birth = w->grid[evt->x][evt->y].nbr[ind][Y];

      if (w->grid[x_birth][y_birth].nbtree == 0)
      {
        mdf->is_modif = 1;
        w->grid[x_birth][y_birth].nbtree = 1;
      }
    break;

    case ORCHID:
      ind = genrand_res53() * (NBR_MAX + 1);
      if (ind < NBR_MAX)
      {
        x_birth = w->grid[evt->x][evt->y].nbr[ind][X];
        y_birth = w->grid[evt->x][evt->y].nbr[ind][Y];
      }
      else
      {
        x_birth = evt->x;
        y_birth = evt->y;
      }

      if (w->grid[x_birth][y_birth].nborchid < sim->max_orch)
      {
        mdf->is_modif = 1;
        w->grid[x_birth][y_birth].nborchid += 1;
	if (evt->type == 'b')
	  w->grid[x_birth][y_birth].orchid[w->grid[x_birth][y_birth].nborchid].theta =  w->grid[evt->x][evt->y].orchid[evt->orch_ind].theta; //sim->theta_init;
	else // evt->type == 'i'
	  w->grid[x_birth][y_birth].orchid[w->grid[x_birth][y_birth].nborchid].theta = sim->theta_init;
      }
    break;

    case FUNGUS:
      ind = genrand_res53() * NBR_MAX;
      x_birth = w->grid[evt->x][evt->y].nbr[ind][X];
      y_birth = w->grid[evt->x][evt->y].nbr[ind][Y];

      if (w->grid[x_birth][y_birth].nbfungus == 0)
      {
        mdf->is_modif = 1;
        w->grid[x_birth][y_birth].nbfungus = 1;
      }
    break;
    }

  if (mdf->is_modif)
    {
      mdf->x = x_birth;
      mdf->y = y_birth;
     
      addOneNgbInNgbhood(w, mdf->x, mdf->y, evt->sp);
    }
  
  return D_O_K;
}  

int death(simul* sim, wood* w, event* evt, modif* mdf)
{
  char i, dead_ind;
  switch(evt->sp)
    {
    case TREE:
      if (w->grid[evt->x][evt->y].nbtree > 0)
	{
	  mdf->is_modif = 1;
	  w->grid[evt->x][evt->y].nbtree = 0;
	}
      break;

    case ORCHID:
      if (w->grid[evt->x][evt->y].nborchid > 0)
	{
	  if (w->grid[evt->x][evt->y].nborchid == 1)
	    w->grid[evt->x][evt->y].orchid[0].theta = 0;
	  else 
	  {
	    // retrieve which orchid ind dies and reorganize orchids array
	    dead_ind = evt->orch_ind;
	    for (i = dead_ind; i < w->grid[evt->x][evt->y].nborchid-1; i++)
	      w->grid[evt->x][evt->y].orchid[i].theta = w->grid[evt->x][evt->y].orchid[i+1].theta;
	  } 

	  mdf->is_modif = 1;
	  w->grid[evt->x][evt->y].nborchid -= 1;
	}
      break;

    case FUNGUS:
      if (w->grid[evt->x][evt->y].nbfungus > 0)
	{
	  mdf->is_modif = 1;
	  w->grid[evt->x][evt->y].nbfungus = 0;
	}
      break;
    }
  
  if (mdf->is_modif)
    {
      mdf->x = evt->x;
      mdf->y = evt->y;
     
      removeOneNgbInNgbhood(w, mdf->x, mdf->y, evt->sp);
    }

  return D_O_K;
}

int immig(simul* sim, wood* w, event* evt, modif* mdf)
{
  int x_birth = 0, y_birth = 0;
  
  // Birth occurs at a random location in the neighbourhood (if not already present)
  x_birth = evt->x;
  y_birth = evt->y;

  switch(evt->sp)
    {
    case TREE:
      win_printf_OK("Error: immig of species t");      
    break;

    case ORCHID:
      if (w->grid[x_birth][y_birth].nborchid < sim->max_orch)
	{
	  mdf->is_modif = 1;
	  w->grid[x_birth][y_birth].nborchid += 1;
	  w->grid[x_birth][y_birth].orchid[w->grid[x_birth][y_birth].nborchid].theta = sim->theta_init;
	}
    break;

    case FUNGUS:
      if (w->grid[x_birth][y_birth].nbfungus == 0)
	{
	  mdf->is_modif = 1;
	  w->grid[x_birth][y_birth].nbfungus = 1;
	}
    break;
    }

  if (mdf->is_modif)
    {
      mdf->x = x_birth;
      mdf->y = y_birth;
     
      addOneNgbInNgbhood(w, mdf->x, mdf->y, evt->sp);
    }
  
  return D_O_K;
}  

int dispersal(simul* sim, wood* w, event* evt, modif* mdf)
{
  int x_birth = 0, y_birth = 0;

  if (evt->sp != ORCHID)
    win_printf_OK("Error: dispersal of species %c", evt->sp);
  else
    {
      x_birth = genrand_res53() * (sim->grid_width);
      y_birth = genrand_res53() * (sim->grid_width);

      if (w->grid[x_birth][y_birth].nborchid < sim->max_orch)
	{
	  mdf->is_modif = 1;
	  w->grid[x_birth][y_birth].nborchid += 1;
	  w->grid[x_birth][y_birth].orchid[w->grid[x_birth][y_birth].nborchid].theta =  w->grid[evt->x][evt->y].orchid[evt->orch_ind].theta;
	}
    }

  if (mdf->is_modif)
    {
      mdf->x = x_birth;
      mdf->y = y_birth;
     
      addOneNgbInNgbhood(w, mdf->x, mdf->y, evt->sp);
    }

  return D_O_K;
}
#endif

