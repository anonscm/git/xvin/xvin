#ifndef SIMUL_H_
#define SIMUL_H_

// FUNCTIONS DECLARATIONS
PXV_FUNC(int, new_replicate, (simul *sim, imreg *imr, pltreg *pr));
PXV_FUNC(int, initWood, (wood *w));
PXV_FUNC(int, calculateNeighbours, (wood *w, int x, int y));
PXV_FUNC(int, updateConFungus, (wood *w, int x, int y));
PXV_FUNC(int, carbon, (simul *sim, wood* w, int x, int y));
PXV_FUNC(int, updateRates, (simul *sim, wood *w, int x, int y));
PXV_FUNC(int, choose_event, (simul *sim, wood *w, event *ev));
PXV_FUNC(int, birth, (simul* sim, wood* w, event* evt, modif* mdf));
PXV_FUNC(int, death, (simul* sim, wood* w, event* evt, modif* mdf));
PXV_FUNC(int, immig, (simul* sim, wood* w, event* evt, modif* mdf));
PXV_FUNC(int, dispersal, (simul* sim, wood* w, event* evt, modif* mdf));

//PXV_FUNC(int, updateCell, (simul *sim, wood *w, int x, int y));


#ifdef SIMUL_C_

#endif

#endif
