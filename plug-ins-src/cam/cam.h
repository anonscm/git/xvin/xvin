#ifndef CAM_H_
#define CAM_H_

#define    IS_PLTREG  5019
#define    IS_WOOD    5020
#define    IS_SIMUL   5021
#define    ERROR      1e-9

#define    NSPP          3    // Number of 'species'
#define	   TREE          0
#define    ORCHID        1
#define	   FUNGUS        2

#define    NBR_MAX       8    // 8-cell neighbourhood
#define    NBRL_MAX     24    // Second order 8-cell neighbourhood
#define    X             0    // index in the nbr array
#define    Y             1    // index in the nbr array

//   8  9 10 11 12
//  23  0  1  2 13
//  22  7 fc  3 14
//  21  6  5  4 15
//  20 19 18 17 16

typedef struct
{
  float	Freq;
  float	I0;           // the constant immig rate per cell (from outside the lattice)
  float	B0;
  float	B1[NSPP];
  float	D0;	 
  float	D1[NSPP];
} speciesProperties;

typedef struct
{
  float birthR;
  float deathR;
  char con_fungus;
} treeType;

typedef struct
{
  float dispR;
  float birthR;
  float deathR;
  float theta;
  // float disp;
  float carbon;
} orchidType;

typedef struct
{
  float birthR;
  float deathR;
  float carbon;
} fungusType;

typedef struct
{
  char nbtree;
  char nborchid;
  char nbfungus;
  short int  nbr[NBRL_MAX][2];
  char ngb[NSPP];        // number of inds of this species in the neighbourhood (including the focal cell)
  treeType   tree;
  orchidType *orchid;
  fungusType fungus;
} cell;

typedef struct _event
{
  char  type;
  float dt;
  char	sp;
  char  orch_ind;
  int x;
  int y;
} event;

typedef struct _modif
{
  char is_modif;
  int x;
  int y;
} modif;

// Variables initialized by user
typedef struct _simul
{
  struct hook use;    // used to attach pltreg
  int cur_rep;
  int n_rep;
  int iter_per_idle;
  float t_max;
  int grid_width;
  char seed[512];
  unsigned long int ran_count;
  int freq_display;   // deprecated (refreshed every time point)
  char path[512];
  int is_cmn;
  speciesProperties param[NSPP];
  int max_orch;
  float theta_init;
  float disp_init; // proportion of disp/local reprod
  float Isum;
 } simul;

typedef struct _wood
{
  float t;
  float t_max;
  char extinction;
  int c_f;
  int grid_width;
  int max_orch;
  cell **grid;
  int idle_state;
} wood;


// FUNCTIONS DECLARATIONS
PXV_FUNC(simul*, find_simul_in_oi, (O_i *oi));
PXV_FUNC(int, refresh_wood_display, (imreg* imr, pltreg* pr, O_i* oi, wood* w));
PXV_FUNC(int, simulate_n_step, (O_i *oi, DIALOG *d));


#ifdef CAM_C_

#else


#endif

#endif
