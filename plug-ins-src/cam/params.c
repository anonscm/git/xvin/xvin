#ifndef PARAMS_C_
#define PARAMS_C_

# include "allegro.h"
# include "xvin.h"

# include "cam.h"
# include "params.h"


/*******************************************************************************
 * Procedures for importing parameters and initializing simul structure        *
 ******************************************************************************/
int report(char *format)
{
  win_printf("Error while reading parameter file: %s not found", format);
  return OFF;
}

int importParam(simul* sim)
{
  FILE *inCam = NULL;           // Input file for community parameters
  int  sp, i;
  char file[512];
  char name[40], format[40];

  switch_allegro_font(1);
  strcpy(file, "/Users/thomasjulou/Documents/Maitrise/Stage evol/Code/mhp_xvin/input.dat");
  i = file_select_ex("Load params (*.dat)", file, "dat", 512, 0, 0);
  if (i == CANCEL) return i;
  extract_file_path(sim->path, 512, file);
  switch_allegro_font(0);

  inCam = fopen(file, "r");
  if (inCam == NULL)
    report("File open");

  // Retrieve general parameters
  sim->cur_rep = 0;

  sprintf(name, "seed");
  sprintf(format, "%s %%s\n", name);
  if (fscanf(inCam, format, sim->seed) != 1)   report(name);
  
  sprintf(name, "n_rep");
  sprintf(format, "%s %%d\n", name);
  if (fscanf(inCam, format, &sim->n_rep) != 1)   report(name);
  
  sprintf(name, "iter_per_idle");
  sprintf(format, "%s %%d\n", name);
  if (fscanf(inCam, format, &sim->iter_per_idle) != 1)   report(name);

  sprintf(name, "t_max");
  sprintf(format, "%s %%f\n", name);
  if (fscanf(inCam, format, &sim->t_max) != 1)   report(name);
  
  sprintf(name, "grid_width");
  sprintf(format, "%s %%d\n", name);
  if (fscanf(inCam, format, &sim->grid_width) != 1)   report(name);
  
  sprintf(name, "freq_display");
  sprintf(format, "%s %%d\n", name);
  if (fscanf(inCam, format, &sim->freq_display) != 1)   report(name);
  
  sprintf(name, "is_cmn");
  sprintf(format, "%s %%d\n", name);
  if (fscanf(inCam, format, &sim->is_cmn) != 1)   report(name);
  
  sprintf(name, "max_orch");
  sprintf(format, "%s %%d\n", name);
  if (fscanf(inCam, format, &sim->max_orch) != 1)   report(name);
  
  sprintf(name, "theta_init");
  sprintf(format, "%s %%f\n", name);
  if (fscanf(inCam, format, &sim->theta_init) != 1)   report(name);
  
  sprintf(name, "disp_init");
  sprintf(format, "%s %%f\n", name);
  if (fscanf(inCam, format, &sim->disp_init) != 1)   report(name);
  
  /***Loop for inputing parameters one species at a time***/
  for (sp=0; sp<NSPP; ++sp)
    {
      /***Read Freq's***/
      sprintf(name, "Freq[%d]", sp);
      sprintf(format, "%s %%g\n", name);
      if (fscanf(inCam, format, &sim->param[sp].Freq) != 1)   report(name);

      /***Read I0's***/
      sprintf(name, "I0[%d]", sp);
      sprintf(format, "%s %%g\n", name);
      if (fscanf(inCam, format, &sim->param[sp].I0) != 1)   report(name);

      /***Read B0's***/
      sprintf(name, "B0[%d]", sp);
      sprintf(format, "%s %%g\n", name);
      if (fscanf(inCam, format, &sim->param[sp].B0) != 1)   report(name);

      /***Read D0's***/
      sprintf(name, "D0[%d]", sp);
      sprintf(format, "%s %%g\n", name);
      if (fscanf(inCam, format, &sim->param[sp].D0) != 1)   report(name);    

      /***Read B1's***/
      for (i=0; i<NSPP; ++i)
	{
	  sprintf(name, "B1[%d][%d]", sp, i);
	  sprintf(format, "%s %%g\n", name);      
	  if (fscanf(inCam, format, &sim->param[sp].B1[i]) != 1)   report(name); 
	}

      /***Read D1's***/
      for (i=0; i<NSPP; ++i)
	{
	  sprintf(name, "D1[%d][%d]", sp, i);
	  sprintf(format, "%s %%g\n", name);          
	  if(fscanf(inCam, format, &sim->param[sp].D1[i]) != 1)   report(name);
	}
    }
  fclose(inCam);

  return D_O_K;
}

/*******************************************************************************
* Procedure for displaying parameters and allowing to modify them              *
*******************************************************************************/
int askForParam(simul* sim)
{
  int i;

  i = win_scanf("Please enter:\n\n"
		"the lattice width                               %6d\n"
		"the ending time of simulation        %6f\n"
		"the number of replicates                 %6d\n\n"
		"%b     implement carbon transfer (i.e. cmn)\n\n"
		"the display refreshing rate              %6d\n"
		"the number of iteration per idle      %6d\n"
		"a random gen seed %s",
	        &sim->grid_width, &sim->t_max, &sim->n_rep,
		&sim->is_cmn,
		&sim->freq_display, &sim->iter_per_idle, sim->seed);
  if (i == CANCEL)
    return i;

  i = win_scanf("Please enter (between 0 and 1) :\n\n"
		"Freq[TREE]       %6f\nI0[TREE]           %6f\n"
		"B0[TREE]          %6f\nD0[TREE]          %6f\n\n"
		"Freq[ORCHID]   %6f\nI0[ORCHID]       %6f\n"
		"B0[ORCHID]      %6f\nD0[ORCHID]      %6f\n"
		"theta                  %6f\ndisp                    %6f\n\n"
		"Freq[FUNGUS]  %6f\nI0[FUNGUS]      %6f\n"
		"B0[FUNGUS]     %6f\nD0[FUNGUS]     %6f", 
		&sim->param[TREE].Freq, &sim->param[TREE].I0,
		&sim->param[TREE].B0, &sim->param[TREE].D0,
		&sim->param[ORCHID].Freq, &sim->param[ORCHID].I0,
		&sim->param[ORCHID].B0, &sim->param[ORCHID].D0,
		&sim->theta_init, &sim->disp_init,
		&sim->param[FUNGUS].Freq, &sim->param[FUNGUS].I0,
		&sim->param[FUNGUS].B0, &sim->param[FUNGUS].D0);
  if (i == CANCEL)
    return i;

 return D_O_K;
}


#endif

