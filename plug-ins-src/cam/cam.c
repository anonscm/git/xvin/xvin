/** \file cam.c
    \cellular automaton model
    \author Thomas Julou
*/
#ifndef CAM_C_
#define CAM_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 
# include "gsl/gsl_statistics_double.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "cam.h"
# include "simul.h"
# include "params.h"
# include "SFMT.h"

simul* find_simul_in_oi(O_i *oi) 
{
  if (oi == NULL) return NULL;
  if (oi->im.use.to == IS_SIMUL)
    return (simul *)oi->im.use.stuff;
  return NULL;
}

wood* find_wood_in_oi(O_i *oi) 
{
  if (oi == NULL) return NULL;
  if (oi->im.use.to == IS_WOOD)
    return (wood *)oi->im.use.stuff;
  return NULL;
}

pltreg* find_pltreg_in_simul(simul *sim) 
{
  if (sim == NULL) return NULL;
  if (sim->use.to == IS_PLTREG)
    return (pltreg *)sim->use.stuff;
  return NULL;
}

/*******************************************************************************
* Procedure for plot initialization                                            *
*******************************************************************************/
int init_pltreg(pltreg* pr, simul* sim)
// the first ds created for density plot is the mean density (#0)
// the density of each replicated has index "rep_index + 1"
{
  O_p* op;
  int i, ds_l;

  ds_l = floor(sim->t_max) + 1;
  op = pr->o_p[pr->cur_op];
  create_and_attach_one_ds(op,ds_l,ds_l,1);
  //create_attach_select_x_un_to_op (op, IS_RAW_U, 0, 1, 0, 0, "(au)");

  for (i = pr->n_op; i < 4; i++)
    create_and_attach_one_plot(pr,ds_l,ds_l,0);
  
  // Set names and labels
  for (i = 0; i < NSPP; i++)
    {
      op = pr->o_p[i];
      set_plot_title(op, "Species %d density", i);
      set_plot_x_title(op, "time");
      set_plot_y_title(op, "density");
      set_op_filename(op, "sp%d_density.gr", i);
      set_ds_source(op->dat[0], "mean density");
      if (alloc_data_set_y_error(op->dat[0]) == NULL)
        return win_printf_OK("cannot allocate error bars");;
      set_ds_point_symbol(op->dat[0], "\\pt8 \\di");
    }
  set_ds_line_color(pr->o_p[TREE]->dat[0], Lightgreen);
  set_ds_line_color(pr->o_p[ORCHID]->dat[0], blue);
  set_ds_line_color(pr->o_p[FUNGUS]->dat[0], Lightred);
	
  // Special features for MEAN PLOT (#3)
  // create three datasets
  op = pr->o_p[3];
  set_plot_title(op, "Mean densities");
  set_plot_x_title(op, "time");
  set_plot_y_title(op, "density");
  set_op_filename(op, "mean_density.gr");
  create_and_attach_one_ds(op,ds_l,ds_l,1);
  create_and_attach_one_ds(op,ds_l,ds_l,1);  
  for (i = 0; i < NSPP; i++)
    if (alloc_data_set_y_error(op->dat[i]) == NULL)
      return win_printf_OK("cannot allocate error bars");;
  
  set_ds_source(op->dat[TREE], "Mean density of tree");
  set_ds_line_color(op->dat[TREE], Lightgreen);
  set_ds_source(op->dat[ORCHID], "Mean density of orchids");
  set_ds_line_color(op->dat[ORCHID], blue);
  set_ds_source(op->dat[FUNGUS], "Mean density of fungus");
  set_ds_line_color(op->dat[FUNGUS], Lightred);
  
  return D_O_K;
}

/*******************************************************************************
* Procedure for updating display from wood                                          *
*******************************************************************************/
int refresh_wood_display(imreg* imr, pltreg* pr, O_i* oi, wood* w)
//void refresh_wood_display(imreg* imr, O_i* oi, wood* w)
{
  union pix *data;
  simul *sim;
  d_s *t_dens, *f_dens, *o_dens;
  d_s *t_mdens, *f_mdens, *o_mdens;
  int i,j;
  int t_max, o_max, f_max;
  double *data_t = NULL, *data_o = NULL, *data_f = NULL;


  if ((sim = find_simul_in_oi(imr->o_i[0])) == NULL)
    return win_printf_OK("cannot find simul");

  // variable for IMAGE DISPLAY
  if (oi->im.pxl[w->c_f] == NULL)
    return win_printf_OK("cannot find image data (refresh display)");
  data = oi->im.pxl[w->c_f];
  
  // variables for PLOT DISPLAY
  t_dens = pr->o_p[TREE]->dat[sim->cur_rep+1];
  o_dens = pr->o_p[ORCHID]->dat[sim->cur_rep+1];
  f_dens = pr->o_p[FUNGUS]->dat[sim->cur_rep+1];
  t_dens->xd[w->c_f] = o_dens->xd[w->c_f] = f_dens->xd[w->c_f] = w->c_f; //w->t;
  t_dens->yd[w->c_f] = o_dens->yd[w->c_f] = f_dens->yd[w->c_f] = 0;

  t_mdens = pr->o_p[TREE]->dat[0];
  o_mdens = pr->o_p[ORCHID]->dat[0];
  f_mdens = pr->o_p[FUNGUS]->dat[0];
  t_mdens->xd[w->c_f] = o_mdens->xd[w->c_f] = f_mdens->xd[w->c_f] = w->c_f; //w->t;

  // GET CURRENT VARIABLES
  for (i = 0 ; i < w->grid_width ; i++)
    for (j = 0 ; j < w->grid_width ; j++)
      {
	data[i].rgb[j].r = (unsigned char) w->grid[i][j].nbfungus;
	data[i].rgb[j].g = (unsigned char) w->grid[i][j].nbtree;
	data[i].rgb[j].b = (unsigned char) w->grid[i][j].nborchid;
      
	t_dens->yd[w->c_f] += w->grid[i][j].nbtree;
	f_dens->yd[w->c_f] += w->grid[i][j].nbfungus;
	o_dens->yd[w->c_f] += w->grid[i][j].nborchid;
      }

  data_t = (double *) calloc(sim->cur_rep, sizeof(double));
  data_o = (double *) calloc(sim->cur_rep, sizeof(double));
  data_f = (double *) calloc(sim->cur_rep, sizeof(double));
  for (i = 1; i < sim->cur_rep+1; i++)
    {
      data_t[i-1] = (double) pr->o_p[TREE]->dat[i]->yd[w->c_f];
      data_o[i-1] = (double) pr->o_p[ORCHID]->dat[i]->yd[w->c_f];
      data_f[i-1] = (double) pr->o_p[FUNGUS]->dat[i]->yd[w->c_f];
    }
  t_mdens->yd[w->c_f] = (float) gsl_stats_mean(data_t, 1, sim->cur_rep);
  t_mdens->ye[w->c_f] = (float) gsl_stats_sd(data_t, 1, sim->cur_rep);
  o_mdens->yd[w->c_f] = (float) gsl_stats_mean(data_o, 1, sim->cur_rep);
  o_mdens->ye[w->c_f] = (float) gsl_stats_sd(data_o, 1, sim->cur_rep);
  f_mdens->yd[w->c_f] = (float) gsl_stats_mean(data_f, 1, sim->cur_rep);
  f_mdens->ye[w->c_f] = (float) gsl_stats_sd(data_f, 1, sim->cur_rep);
  free(data_t); free(data_o); free(data_f); 

  // update mean plot
  t_max = f_max = sim->grid_width * sim->grid_width;
  o_max = t_max * sim->max_orch;  
  pr->o_p[3]->dat[TREE]->xd[w->c_f] = pr->o_p[3]->dat[ORCHID]->xd[w->c_f] = pr->o_p[3]->dat[FUNGUS]->xd[w->c_f] = w->c_f;
  pr->o_p[3]->dat[TREE]->yd[w->c_f] = t_mdens->yd[w->c_f] / t_max;
  pr->o_p[3]->dat[TREE]->ye[w->c_f] = t_mdens->ye[w->c_f] / t_max;
  pr->o_p[3]->dat[ORCHID]->yd[w->c_f] = o_mdens->yd[w->c_f] / o_max;
  pr->o_p[3]->dat[ORCHID]->ye[w->c_f] = o_mdens->ye[w->c_f] / o_max;
  pr->o_p[3]->dat[FUNGUS]->yd[w->c_f] = f_mdens->yd[w->c_f] / f_max;
  pr->o_p[3]->dat[FUNGUS]->ye[w->c_f] = f_mdens->ye[w->c_f] / f_max;

  // REFRESH DISPLAY (must refresh pltreg before imreg)
  switch_project_to_this_pltreg(pr);
  refresh_plot(pr, UNCHANGED);

  switch_project_to_this_imreg(imr);
  //switch_frame(oi, oi->im.c_f);
  switch_frame(oi, w->c_f);
  refresh_image(imr, imr->cur_oi);

  w->c_f++;
  return D_O_K;
}

/*******************************************************************************
* Procedure for initiating a new simulation: manage allocation, initialization*
******************************************************************************/
int new_simul(void)
{
  imreg* imr;
  O_i* oi;
  pltreg* pr;
  simul* sim;
  int i, j;

  int seed_cnt;
  uint32_t seeds[100];

//   int tmp;
//   long ltmp;
//   unsigned long ultmp;

  if (updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_N, 0, new_simul);
      return D_O_K;
    }	
  
  // Each simulation has its own imreg and pltreg
  // simul struct is attached to the first image (and has pltreg attached to it)
  // each replicate is an oi with an attached wood struct
  imr = create_and_register_new_image_project(0, 32, 900, 668);
  if (imr == NULL) return win_printf_OK("cannot create image region");
  pr = create_and_register_new_plot_project(0, 32, 900, 668);
  if (pr == NULL) return win_printf_OK("cannot create plot region");

  // Create simul
  sim = (simul*)calloc(1,sizeof(simul));
  if (sim == NULL)	return win_printf_OK("cannot create simul structure");

  i = importParam(sim);
  if (i != D_O_K)  return win_printf_OK("cannot import params");
  i = askForParam(sim);
  if (i != D_O_K)  return win_printf_OK("simulation aborted");

  // attach pltreg to simul
  sim->use.stuff = (void*)pr;
  sim->use.to = IS_PLTREG;

  // Init plots
  init_pltreg(pr, sim);
  switch_project_to_this_imreg(imr);
  
  // Initialize rand
  seed_cnt = 0;
  for (i = 0; (i < 100) && (i < strlen(sim->seed)); i++)
    {
      seeds[i] = sim->seed[i];
      seed_cnt++;
    }
  init_by_array(seeds, seed_cnt);
  sim->ran_count = 0;

  /*   tmp = 2431; */
  /*   ltmp = 546372819; */
  /*   ultmp = 546372819; */
  /*   win_printf_OK("int %d\nlong int %ld\nulong int %u", tmp, ltmp, ultmp); */

  // Display first image (color key)
  oi = imr->o_i[imr->cur_oi];
  alloc_one_image(oi, 4, sim->max_orch, IS_RGB_PICTURE);
  set_oi_mode(oi, TRUE_RGB);
  for (i=0 ; i<sim->max_orch ; i++)
    for (j=0 ; j<4 ; j++)
      {
	oi->im.pixel[i].rgb[j].r = (unsigned char) (j/2)*(15);   // fungus
	oi->im.pixel[i].rgb[j].g = (unsigned char) (j%2)*(15);   // tree
	oi->im.pixel[i].rgb[j].b = (unsigned char) (i+ (i?6:0)); // orchid
      }
  set_oi_filename(oi, "Color key");
  set_oi_horizontal_extend(oi, 0.4);
  smooth_interpol = 0;
  refresh_image(imr, imr->cur_oi);
  find_zmin_zmax(oi);

  // attach simul to first oi
  oi->im.use.stuff = (void*)sim;
  oi->im.use.to = IS_SIMUL;


  // START ONE REPLICATE
  // create and allocate wood (must be done for first oi of imreg)
  new_replicate(sim, imr, pr);
  
  return D_O_K;
}

/*******************************************************************************
 * Procedure for toggling the idle state of the simul                          *
 ******************************************************************************/
int start_pause_simul(void)
{
  imreg *imr = NULL;
  O_i *oi = NULL;
  wood *w = NULL;

  if (updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_SPACE, 0, start_pause_simul);
      return D_O_K;
    }

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    return win_printf_OK("cannot find data");

  if ((w = find_wood_in_oi(oi)) == NULL)
    return win_printf_OK("cannot find wood");

  w->idle_state = w->idle_state ? 0 : 1;
  return D_O_K;	
}

int display_simul_state(void)
{
  imreg *imr = NULL;
  O_i *oi = NULL;
  simul *sim = NULL;
  wood *w = NULL;

  if (updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_D, 0, display_simul_state);
      return D_O_K;
    }

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    return win_printf_OK("cannot find data");

  if ((sim = find_simul_in_oi(imr->o_i[0])) == NULL)
    return win_printf_OK("cannot find simul");

  if ((w = find_wood_in_oi(oi)) == NULL)
    return win_printf_OK("cannot find wood");

  win_printf_OK("current time: %f\nmax time:     %f\n\nSimulation %s\nRn gen seed: %s\n",//# calls to rn gen: %lu", 
                w->t, w->t_max,
                (w->idle_state ? "is running" : "paused"),
                sim->seed);
  return D_O_K;	
}

int display_ran_count(void)
{
  imreg *imr = NULL;
  O_i *oi = NULL;
  simul *sim = NULL;

  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_R, 0, display_ran_count);
      return D_O_K;
    }

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    return win_printf_OK("cannot find data");

  if ((sim = find_simul_in_oi(imr->o_i[0])) == NULL)
    return win_printf_OK("cannot find simul");

  win_printf_OK("ran count is %ul", sim->ran_count);

  return D_O_K;	
}

/*******************************************************************************
* Procedure for calling stochastic simulation (as an oi idle action)          *
******************************************************************************/
int simulate_n_step(O_i *oi, DIALOG *d)
{
  imreg *imr;
  pltreg *pr;
  simul *sim;
  wood* w;
  float t;
  event evt;
  modif mdf;
  int i, ind;

  if ((w = find_wood_in_oi(oi)) == NULL)
    return win_printf_OK("cannot find wood");
  
  if (w->idle_state == 0)
    return 0;

  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;
  //   pr = (pltreg*)d->dp;        /* the plot region is here */
  //   if (pr->one_p == NULL || pr->n_op == 0)        return 1;

  if ((sim = find_simul_in_oi(imr->o_i[0])) == NULL)
    return win_printf_OK("cannot find simul attached to image");
  if ((pr = find_pltreg_in_simul(sim)) == NULL)
    return win_printf_OK("cannot find pltreg attached to simul");

  for (i = 0; i < sim->iter_per_idle; i++)
    {    
      t = w->t;
      // case end of current iteration
      if (t >= w->t_max)
      {
        oi->oi_idle_action = NULL;
        if (w->extinction)
          refresh_wood_display(imr, pr, oi, w);
        sim->cur_rep += 1;
        if (sim->cur_rep < sim->n_rep)
          {
            new_replicate(sim, imr, pr);
            return D_O_K;
          }
        else
          return win_printf_OK("all iteration completed");
      }

      // else run current iteration  
      choose_event(sim, w, &evt);
 
      if (floor(t) != floor(t+evt.dt))
      	//refresh_wood_display(imr, oi, w);
      	refresh_wood_display(imr, pr, oi, w);

      // Implement the chosen event
      w->t += evt.dt;

      mdf.is_modif = 0;
      mdf.x = mdf.y = 0;

      switch(evt.type)
      {
      case 'b':
        birth(sim, w, &evt, &mdf);
        break;
      case 'd':
        death(sim, w, &evt, &mdf);
        break;
      case 'i':
        immig(sim, w, &evt, &mdf);
        break;
      case 's':
        dispersal(sim, w, &evt, &mdf);
        break;
      }

      // Update the lattice around the modification
      if (mdf.is_modif)
      {
        if (sim->is_cmn)
          {
            // Update connected fungus (on 8 cells)
            if (evt.sp != ORCHID)
              for (ind = 0; ind < NBR_MAX; ind++)
                updateConFungus(w, w->grid[mdf.x][mdf.y].nbr[ind][X], w->grid[mdf.x][mdf.y].nbr[ind][Y]);
          
            // Update carbon (on 24 cells)
            for (ind = 0; ind < NBRL_MAX; ind++)
              carbon(sim, w, w->grid[mdf.x][mdf.y].nbr[ind][X], w->grid[mdf.x][mdf.y].nbr[ind][Y]);
          }
    
        // Update rates (on 24 cells)
        for (ind = 0; ind < NBRL_MAX; ind++)
          updateRates(sim, w, w->grid[mdf.x][mdf.y].nbr[ind][X], w->grid[mdf.x][mdf.y].nbr[ind][Y]);
      }
    }

  return D_O_K;
}

/*******************************************************************************
* XVin's stuff (plug-in and menu managment)                                   *
******************************************************************************/

/** Menu function for \a cam.c */
MENU *cam_image_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)
    return mn;
  add_item_to_menu(mn,"New Simulation", new_simul,NULL,0,NULL);
  add_item_to_menu(mn,"Start/Pause simulation", start_pause_simul,NULL,0,NULL);
  add_item_to_menu(mn,"Display simul state", display_simul_state,NULL,0,NULL);
  add_item_to_menu(mn,"Display ran count", display_ran_count,NULL,0,NULL);
  return mn;
}

/** Main function of cam.c */
int	cam_main(int argc, char **argv)
{
  add_image_treat_menu_item("CAM", NULL, cam_image_menu(), 0, NULL);
  add_plot_treat_menu_item("CAM", NULL, cam_image_menu(), 0, NULL);
  return D_O_K;
}

/** Menu unload function for \a cam.c */
int	cam_unload(int argc, char **argv)
{
  remove_item_to_menu(image_treat_menu, "CAM", NULL, NULL);
  remove_item_to_menu(plot_treat_menu, "CAM", NULL, NULL);
  return D_O_K;
}
#endif



// 	imr->one_i = imr->o_i[imr->cur_oi];
//	image->need_to_refresh |= ALL_NEED_REFRESH;
//	imr->screen_scale=PIXEL_1;

/*   set_oi_vertical_extend(image, 0.15); */
/*   smooth_interpol = 0; */
/*   refresh_image(imr, imr->cur_oi); */
/*   find_zmin_zmax(image); */
