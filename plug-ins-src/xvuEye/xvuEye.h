#ifndef _XVUEYE_H_
#define _XVUEYE_H_

PXV_FUNC(int, do_xvuEye_average_along_y, (void));
PXV_FUNC(MENU*, xvuEye_image_menu, (void));
PXV_FUNC(int, xvuEye_main, (int argc, char **argv));

PXV_FUNC(int, oiuEye_idle_action, (struct  one_image *oi, DIALOG *d));
PXV_FUNC(int, do_xvuEye_init_rolling_buffer_movie_xvin,(void));
PXV_FUNC(int, do_set_camera_freq,(void));
PXV_FUNC(int, do_set_camera_exposure,(void));
PXV_FUNC(int, do_set_camera_gain,(void));
PXV_FUNC(int, do_set_camera_binning,(void));
PXV_FUNC(int, adjust_black_level,(void));
PXV_FUNC(int, do_stop_camera,(void));

PXV_FUNC(int, do_freeze_camera,(void));
PXV_FUNC(int, do_live_camera,(void));
PXV_FUNC(int, adj_AOI_position,(void));
PXV_FUNC(int, do_read_gpio,(void));

PXV_FUNC(int, grab_im_avg, (void));
PXV_FUNC(int, grab_im_avg_in_time, (void));

PXV_FUNC(int, read_GPIO_1_input, (int *error));

PXV_FUNC(int, copy_partial_image_in_movie, (O_i *oi,int id, unsigned long long t, unsigned long long dt, void*ptr));

PXV_FUNC(float ,(*grab_PI_z_value),(void));

PXV_FUNC(float ,(*grab_track_image_z_value),(void));

#ifndef _XVUEYE_C_
PXV_VAR(O_i *, oi_xvuEye);
PXV_VAR(char ,xvueye_ti_message[]);
PXV_FUNC(int, (*track_image), (O_i *oid,int id, unsigned long long, unsigned long long, void*));
PXV_VAR(void, *data_ptr);
#else
O_i *oi_xvuEye = NULL;
#endif


#endif

