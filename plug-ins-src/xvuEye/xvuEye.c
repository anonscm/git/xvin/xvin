/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _XVUEYE_C_
#define _XVUEYE_C_
# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif

# include "xvin.h"
#include <pthread.h>
/* If you include other regular header do it here*/


# ifdef SDK_3_5_0
# undef IS_CHAR
# define SDK_3
# ifdef XV_WIN32
# include "../trackBead/uEye/SDK_3_5_0/uEye.h"
# include "../trackBead/uEye/SDK_3_5_0/uEye_tools.h"
# endif
# endif
# ifdef SDK_3_8_0
# undef IS_CHAR
# define SDK_3
# ifdef XV_WIN32
# include "../trackBead/uEye/SDK_3_8_0/uEye.h"
# include "../trackBead/uEye/SDK_3_8_0/uEye_tools.h"
# endif
# endif
# ifdef SDK_4_0_0
# undef IS_CHAR
# define SDK_4
# ifdef XV_WIN32
# include "../trackBead/uEye/SDK_4_0_0/uEye.h"
# include "../trackBead/uEye/SDK_4_0_0/uEye_tools.h"
# endif
# endif
# ifdef SDK_4_2_1
# undef IS_CHAR
# define SDK_4
# ifdef XV_WIN32
# include "../trackBead/uEye/SDK_4_2_1/uEye.h"
# include "../trackBead/uEye/SDK_4_2_1/uEye_tools.h"
# endif
# endif
# ifdef SDK_4_2_2
# undef IS_CHAR
# define SDK_4
# ifdef XV_WIN32
# include "../trackBead/uEye/SDK_4_2_2/uEye.h"
# include "../trackBead/uEye/SDK_4_2_2/uEye_tools.h"
# endif
# endif
# ifdef SDK_4_3_0
# undef IS_CHAR
# define SDK_4
# ifdef XV_WIN32
# include "../trackBead/uEye/SDK_4_3_0/uEye.h"
# include "../trackBead/uEye/SDK_4_3_0/uEye_tools.h"
//# include "../trackBead/uEye/SDK_4_3_0/Dynamic_uEye_api.h"
//# include "../trackBead/uEye/SDK_4_3_0/DynamicuEyeTools.h"
# endif
# endif
# ifdef SDK_4_7_0
# undef IS_CHAR
# define SDK_4
# ifdef XV_WIN32
# include "../trackBead/uEye/SDK_4_7_0/uEye.h"
# include "../trackBead/uEye/SDK_4_7_0/uEye_tools.h"
# endif
# endif
# ifdef SDK_4_7_2
# undef IS_CHAR
# define SDK_4
# ifdef XV_WIN32
# include "../trackBead/uEye/SDK_4_7_2/uEye.h"
# include "../trackBead/uEye/SDK_4_7_2/uEye_tools.h"
# endif
# endif
# ifdef SDK_4_8_0
# undef IS_CHAR
# define SDK_4
# ifdef XV_WIN32
# include "../trackBead/uEye/SDK_4_8_0/uEye.h"
# include "../trackBead/uEye/SDK_4_8_0/uEye_tools.h"
# endif
# endif
# ifdef SDK_4_9_0
# undef IS_CHAR
# define SDK_4
# ifdef XV_WIN32
# include "../trackBead/uEye/SDK_4_9_0/uEye.h"
# include "../trackBead/uEye/SDK_4_9_0/uEye_tools.h"
# endif
# endif

# ifdef XV_UNIX
#include <ueye.h>
# endif

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled

# include "../Due_serial/Due_serial.h"
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# undef IS_CHAR


# include "xvuEye.h"

//PXV_FUNC(int, read_Due_param,(char *cmd, int *val));
//PXV_FUNC(char *, Get_Due_cycle, (void));

//place here other headers of this plugin

// pointer to function called at each frame
int (*track_image)(O_i *oid,int id, unsigned long long, unsigned long long, void*);

float (*grab_track_image_z_value)(void) = NULL;
int gpio_1_in_ok = 0;
int support_GPIO_1_as_input(void);
int config_GPIO_1_as_input(void);
int read_GPIO_1_input(int *error);

char xvueye_ti_message[64] = {0};


//# define AVI_OK

HIDS	m_hCam = (HIDS) 0;			// handle to camera
SENSORINFO sInfo;
//O_i *oid;

void *data_ptr = NULL;

# ifdef AVI_OK
int nAviID = -1;
int rec_AVI = 0;
# endif

// Initialize all handles
#ifdef XV_WIN32
HANDLE hStreamThread = NULL;
# elif defined XV_UNIX
pthread_t stream_thread;
# endif

//HANDLE hStreamEvent = NULL;
# ifdef XV_WIN32
HANDLE EventList[4];
HANDLE hEventFreeze = NULL;
HANDLE hEventNewImage = NULL;
HANDLE hEventRemoved = NULL;
HANDLE hEventReplug = NULL;

# endif
HANDLE hEventKill = NULL;

int iStreamChannel = 0;
BOOL bEnableThread;

float (*grab_PI_z_value)(void) = NULL;



# ifdef XV_WIN32
DWORD WINAPI StreamProcess(LPVOID lpParam);
# elif defined XV_UNIX
void *StreamProcess (void *lpParam);
# endif
//void StreamProcess(void);
void WaitForThreadToTerminate(void);
void CloseThreadHandle(void);



int oi_data_length(O_i* oi)
{
 int data_len = 1;

 if (oi == NULL) return 1;

  switch (oi->im.data_type)
    {
    case IS_CHAR_IMAGE:			data_len = 1;		break;
    case IS_RGB_PICTURE:		data_len = 3;		break;
    case IS_RGBA_PICTURE:		data_len = 4;		break;
    case IS_RGB16_PICTURE:		data_len = 6;		break;
    case IS_RGBA16_PICTURE:		data_len = 8;		break;
    case IS_INT_IMAGE:			data_len = 2;		break;
    case IS_UINT_IMAGE:			data_len = 2;		break;
    case IS_LINT_IMAGE:			data_len = 4;		break;
    case IS_FLOAT_IMAGE:		data_len = 4;		break;
    case IS_COMPLEX_IMAGE:		data_len = 8;		break;
    case IS_DOUBLE_IMAGE:		data_len = 8;		break;
    case IS_COMPLEX_DOUBLE_IMAGE:	data_len = 16;		break;
    default:	                        data_len = 1;		break;
    };
  return data_len;
}


int set_oi_to_uEye_pointers(O_i* oi, void* data_pointer, int nx, int ny)
{

  register int i = 0;
  int data_len = 1, nf=0;
  void *buf = NULL;
  int type = -1;

  if (oi == NULL) return win_printf_OK("Oi NULL");
  type = oi->im.data_type ;

  data_len = oi_data_length(oi);

  nf = (oi->im.n_f > 0) ? oi->im.n_f : 1;
  oi->im.ny = ny;
  oi->im.nx = nx;
  oi->im.pixel = (union pix *)realloc(oi->im.pixel, ny*nf * sizeof(union pix));
  if (oi->im.pixel == NULL) return xvin_error(Out_Of_Memory);
  if (nf > 1)
    {
      oi->im.pxl = (union pix **)realloc(oi->im.pxl, nf*sizeof(union pix*));
      oi->im.mem = (void **)realloc(oi->im.mem, nf*sizeof(void*));
      if (oi->im.pxl == NULL  || oi->im.mem == NULL) return xvin_error(Out_Of_Memory);
    }
  else oi->im.mem = (void **)realloc(oi->im.mem, sizeof(void*));
  oi->im.m_f = nf;
  buf = data_pointer; // already allocated by uEye
  if (oi->im.pixel == NULL || buf == NULL)	return xvin_error(Out_Of_Memory);
  oi->im.mem[0] = buf;
  for (i=0 ; i< ny*nf ; i++)
    {
      switch (type)
	{
	case IS_CHAR_IMAGE:
	  oi->im.pixel[i].ch = (unsigned char*)buf;
	  break;
	case IS_RGB_PICTURE:
	  oi->im.pixel[i].ch = (unsigned char*)buf;
	  break;
	case IS_RGBA_PICTURE:
	  oi->im.pixel[i].ch = (unsigned char*)buf;
	  break;
	case IS_RGB16_PICTURE:
	  oi->im.pixel[i].in = (short int*)buf;
	  break;
	case IS_RGBA16_PICTURE:
	  oi->im.pixel[i].in = (short int*)buf;
	  break;
	case IS_INT_IMAGE:
	  oi->im.pixel[i].in = (short int *)buf;
	  break;
	case IS_UINT_IMAGE:
	  oi->im.pixel[i].ui = (unsigned short int *)buf;
	  break;
	case IS_LINT_IMAGE:
	  oi->im.pixel[i].li = (int *)buf;
	  break;
	case IS_FLOAT_IMAGE:
	  oi->im.pixel[i].fl = (float *)buf;
	  break;
	case IS_COMPLEX_IMAGE:
	  oi->im.pixel[i].fl = (float *)buf;
	  break;
	case IS_DOUBLE_IMAGE:
	  oi->im.pixel[i].db = (double *)buf;
	  break;
	case IS_COMPLEX_DOUBLE_IMAGE:
	  oi->im.pixel[i].db = (double *)buf;
	  break;
	};
      if ( (i % ny == 0) && (oi->im.n_f > 0))
	{
	  oi->im.pxl[i/ny] = oi->im.pixel + i;
	  oi->im.mem[i/ny] = (void*)buf;
	}
      buf += nx * data_len;
    }
  oi->im.nxs = oi->im.nys = 0;
  oi->im.nxe = oi->im.nx = nx;
  oi->im.nye = oi->im.ny = ny;

  if (type == IS_COMPLEX_IMAGE  || type == IS_COMPLEX_DOUBLE_IMAGE)
    oi->im.mode = RE;
  oi->need_to_refresh |= ALL_NEED_REFRESH;
  return D_O_K;
}


int attach_uEye_pointer_to_movie(O_i* oi, void* data_pointer, int nx, int ny, int fr)
{

  register int i = 0;
  int data_len = 1, nf=0;
  void *buf = NULL;
  int type = -1;

  if (oi == NULL) return win_printf_OK("Oi NULL");
  type = oi->im.data_type ;

  data_len = oi_data_length(oi);

  nf = (oi->im.n_f > 0) ? oi->im.n_f : 1;
  oi->im.ny = ny;
  oi->im.nx = nx;
  if (oi->im.pixel == NULL)
    {
      oi->im.pixel = (union pix *)realloc(oi->im.pixel, ny*nf * sizeof(union pix));
      if (oi->im.pixel == NULL) return xvin_error(Out_Of_Memory);
      if (nf > 1)
	{
	  oi->im.pxl = (union pix **)realloc(oi->im.pxl, nf*sizeof(union pix*));
	  oi->im.mem = (void **)realloc(oi->im.mem, nf*sizeof(void*));
	  if (oi->im.pxl == NULL  || oi->im.mem == NULL) return xvin_error(Out_Of_Memory);
	}
      else oi->im.mem = (void **)realloc(oi->im.mem, sizeof(void*));
      oi->im.m_f = nf;
    }
  buf = data_pointer; // already allocated by uEye
  if (oi->im.pixel == NULL || buf == NULL)	return xvin_error(Out_Of_Memory);

  if (fr >= nf) return win_printf_OK("frame out of range");


  oi->im.mem[fr] = buf;
  for (i=fr*ny ; i< ny*(fr+1) ; i++)
    {
      switch (type)
	{
	case IS_CHAR_IMAGE:
	  oi->im.pixel[i].ch = (unsigned char*)buf;
	  break;
	case IS_RGB_PICTURE:
	  oi->im.pixel[i].ch = (unsigned char*)buf;
	  break;
	case IS_RGBA_PICTURE:
	  oi->im.pixel[i].ch = (unsigned char*)buf;
	  break;
	case IS_RGB16_PICTURE:
	  oi->im.pixel[i].in = (short int*)buf;
	  break;
	case IS_RGBA16_PICTURE:
	  oi->im.pixel[i].in = (short int*)buf;
	  break;
	case IS_INT_IMAGE:
	  oi->im.pixel[i].in = (short int *)buf;
	  break;
	case IS_UINT_IMAGE:
	  oi->im.pixel[i].ui = (unsigned short int *)buf;
	  break;
	case IS_LINT_IMAGE:
	  oi->im.pixel[i].li = (int *)buf;
	  break;
	case IS_FLOAT_IMAGE:
	  oi->im.pixel[i].fl = (float *)buf;
	  break;
	case IS_COMPLEX_IMAGE:
	  oi->im.pixel[i].fl = (float *)buf;
	  break;
	case IS_DOUBLE_IMAGE:
	  oi->im.pixel[i].db = (double *)buf;
	  break;
	case IS_COMPLEX_DOUBLE_IMAGE:
	  oi->im.pixel[i].db = (double *)buf;
	  break;
	};
      if ( (i % ny == 0) && (oi->im.n_f > 0))
	{
	  oi->im.pxl[i/ny] = oi->im.pixel + i;
	  //oi->im.mem[i/ny] = (void*)buf;
	}
      buf += nx * data_len;
    }
  oi->im.nxs = oi->im.nys = 0;
  oi->im.nxe = oi->im.nx = nx;
  oi->im.nye = oi->im.ny = ny;

  if (type == IS_COMPLEX_IMAGE  || type == IS_COMPLEX_DOUBLE_IMAGE)
    oi->im.mode = RE;
  oi->need_to_refresh |= ALL_NEED_REFRESH;
  return D_O_K;
}



O_i* create_one_uEye_image(int nx, int ny, int data_type, void* ptr)
{
  O_i *oi;

  if((oi = (O_i *)calloc(1,sizeof(O_i))) == NULL)
    return xvin_ptr_error(Out_Of_Memory);
  nx = 4*(nx/4);
   if (init_one_image(oi, data_type)) 		return NULL;
  oi->im.data_type = data_type;
  set_oi_to_uEye_pointers(oi, ptr, nx, ny);
  return oi;
}



int do_xvuEye_init(void)
{
  O_i *oidl;
  imreg *imr;
  int  m_Ret, m_nSizeX, m_nSizeY;
  int x0,y0,x1,y1;
  int m_lMemoryId = 0;
  char *buf; // unsigned
  IS_RECT rectAOI;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf_OK("cannot find image region");

  m_hCam = (HIDS) 0; 	// open next camera
  // init camera - no window handle required
  m_Ret = is_InitCamera( &m_hCam, NULL );


  if( m_Ret == IS_SUCCESS )
    {
      // retrieve original image size
      is_GetSensorInfo( m_hCam, &sInfo );
      m_nSizeX = sInfo.nMaxWidth;
      m_nSizeY = sInfo.nMaxHeight;

      // setup the color depth to the current windows setting
      //is_GetColorDepth(m_hCam, &m_nBitsPerPixel, &m_nColorMode);
      is_SetColorMode(m_hCam, IS_CM_MONO8);
      rectAOI.s32X     =  rectAOI.s32Y     = x0 = y0 = 0;
      rectAOI.s32Width = x1 = m_nSizeX;
      rectAOI.s32Height = y1 = m_nSizeY;


      m_Ret = is_AOI( m_hCam, IS_AOI_IMAGE_SET_AOI, (void*)&rectAOI, sizeof(rectAOI));
      //is_SetAOI(m_hCam,IS_SET_IMAGE_AOI,&x0,&y0,&x1,&y1);




      //oid = create_and_attach_oi_to_imr(imr, m_nSizeX, m_nSizeY, IS_CHAR_IMAGE);
      //if (oid == NULL)	  return win_printf_OK("Can't create dest image");

      is_AllocImageMem( m_hCam, m_nSizeX, m_nSizeY, 8, &buf, &m_lMemoryId);


      oidl = create_one_uEye_image(m_nSizeX, m_nSizeY, IS_CHAR_IMAGE, (void*)buf);
      if (oidl == NULL)	  return win_printf_OK("Can't create dest image");
      add_image(imr, IS_CHAR_IMAGE, (void*)oidl);

      is_SetImageMem( m_hCam, buf, m_lMemoryId );	// set memory active
      //is_SetImageMem( m_hCam, oid->im.pixel[0].ch, 0 );	// set memory active

      is_FreezeVideo (m_hCam, 100); // IS_WAIT
      // display initialization
      //is_SetImageSize( m_hCam, m_nSizeX, m_nSizeY );
      //is_SetDisplayMode( m_hCam, IS_SET_DM_DIB);

      // enable the dialog based error report
      m_Ret = is_SetErrorReport(m_hCam, IS_ENABLE_ERR_REP); //IS_DISABLE_ERR_REP);
      if( m_Ret != IS_SUCCESS )
	{
	  win_printf("ERROR: Can not enable the automatic uEye error report!");
	  return 0;
	}

      map_pixel_ratio_of_image_and_screen(oidl, 1, 1);
      set_zmin_zmax_values(oidl, 0, 255);
      set_z_black_z_white_values(oidl, 0, 255);
      refresh_image(imr, imr->n_oi - 1);
    }
  else
    {
      win_printf( "ERROR: Can not open uEye camera!");
      return 0;
    }
  return (refresh_image(imr, imr->n_oi - 1));
}




int do_xvuEye_init_xvin(void)
{
  O_i *oidl;
  imreg *imr;
  int  m_Ret, m_nSizeX, m_nSizeY;
  int x0,y0,x1,y1;
  int m_lMemoryId = 0;
  unsigned char *buf;
# if defined(SDK_4_0_0) || defined(SDK_4_2_1) || defined(SDK_4_2_2) || defined(SDK_4_3_0) || defined(SDK_4_7_0) || defined(SDK_4_7_2) || defined(SDK_4_8_0) || defined(SDK_4_9_0)
  IS_RECT rectAOI;
# endif

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf_OK("cannot find image region");

  m_hCam = (HIDS) 0; 	// open next camera
  // init camera - no window handle required
  m_Ret = is_InitCamera( &m_hCam, NULL );


  if( m_Ret == IS_SUCCESS )
    {
      // retrieve original image size
      is_GetSensorInfo( m_hCam, &sInfo );
      m_nSizeX = sInfo.nMaxWidth;
      m_nSizeY = sInfo.nMaxHeight;

      // setup the color depth to the current windows setting
      //is_GetColorDepth(m_hCam, &m_nBitsPerPixel, &m_nColorMode);
      is_SetColorMode(m_hCam, IS_CM_MONO8);
      x0 = y0 = 0;
      x1 = m_nSizeX;
      y1 = m_nSizeY;

# ifdef SDK_3
      is_SetAOI(m_hCam,IS_SET_IMAGE_AOI,&x0,&y0,&x1,&y1);
# elif defined(SDK_4)
      rectAOI.s32X     = x0;
      rectAOI.s32Y     = y0;
      rectAOI.s32Width = x1;
      rectAOI.s32Height = y1;
      is_AOI( m_hCam, IS_AOI_IMAGE_SET_AOI, (void*)&rectAOI, sizeof(rectAOI));
# endif


      oidl = create_and_attach_oi_to_imr(imr, m_nSizeX, m_nSizeY, IS_CHAR_IMAGE);
      if (oidl == NULL)	  return win_printf_OK("Can't create dest image");

      buf = oidl->im.pixel[0].ch;
      is_SetAllocatedImageMem( m_hCam, m_nSizeX, m_nSizeY, 8, (char*)buf, &m_lMemoryId);


      is_SetImageMem( m_hCam, (char*)buf, m_lMemoryId );	// set memory active
      //is_SetImageMem( m_hCam, oidl->im.pixel[0].ch, 0 );	// set memory active

      is_FreezeVideo (m_hCam, 100); // IS_WAIT
      // display initialization
      //is_SetImageSize( m_hCam, m_nSizeX, m_nSizeY );
      //is_SetDisplayMode( m_hCam, IS_SET_DM_DIB);

      // enable the dialog based error report
      m_Ret = is_SetErrorReport(m_hCam, IS_ENABLE_ERR_REP); //IS_DISABLE_ERR_REP);
      if( m_Ret != IS_SUCCESS )
	{
	  win_printf("ERROR: Can not enable the automatic uEye error report!");
	  return 0;
	}

      map_pixel_ratio_of_image_and_screen(oidl, 1, 1);
      set_zmin_zmax_values(oidl, 0, 255);
      set_z_black_z_white_values(oidl, 0, 255);
      refresh_image(imr, imr->n_oi - 1);
      is_ExitCamera (m_hCam);
    }
  else
    {
      win_printf( "ERROR: Can not open uEye camera!");
      return 0;
    }
  return (refresh_image(imr, imr->n_oi - 1));
}




int do_stop_camera(void)
{
    if (updating_menu_state != 0)
      {
	if (m_hCam == 0)  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }  

#ifdef XV_WIN32
    SetEvent(hEventFreeze);
#else
    is_FreezeVideo(m_hCam, IS_DONT_WAIT);
#endif
  bEnableThread = FALSE;
  return 0;
}



int do_freeze_camera(void)
{
    if (updating_menu_state != 0)
      {
	if (m_hCam == 0)  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }  
  is_StopLiveVideo( m_hCam, IS_WAIT );
  //SetEvent(hEventFreeze);
  return 0;
}

int do_live_camera(void)
{
    if (updating_menu_state != 0)
      {
	if (m_hCam == 0)  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }  
  is_CaptureVideo( m_hCam, IS_WAIT );
  //bEnableThread = TRUE;
  return 0;
}


float *radial_non_pixel_square_image_sym_profile_in_array_RGB(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay, int channel)
{
  int i, j;
  union pix *ps;
  int onx, ony, ri, type;
  float z = 0, x, y, r, p, p0 = 0, pr0 = 0;
  int imin, imax, jmin,jmax; //  xci, yci

  if (ois == NULL)	return NULL;
  if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
  if (rz == NULL)		return NULL;
  ps = ois->im.pixel;
  ony = ois->im.ny;	onx = ois->im.nx;
  type = ois->im.data_type;
  for (i = 0; i < 2*r_max; i++)	rz[i] = 0;

  imin = (int)(yc - (r_max/ay));
  imax = (int)(0.5 + yc + (r_max/ay));
  jmin = (int)(xc -  r_max/ax);
  jmax = (int)(0.5 + xc + r_max/ax);
  imin = (imin < 0) ? 0 : imin;
  imax = (imax > ony) ? ony : imax;
  jmin = (jmin < 0) ? 0 : jmin;
  jmax = (jmax > onx) ? onx : jmax;
  if (type == IS_RGB_PICTURE)
    {
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].ch[3*j+channel];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		}
	      else
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		}
	    }
	}
    }
  for (i = 0; i < r_max; i++)
    {
      rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
    }
  p0 = (pr0 == 0) ? 0 : p0/pr0;
  for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i];
  for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i];
  rz[0] = p0;
  return rz;
}
// grab the RED in a Bayer Raw image
float *radial_non_pixel_square_image_sym_profile_in_array_R_on_RAW8(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay, int channel)
{
  int i, j;
  union pix *ps;
  int onx, ony, ri, type;
  float z = 0, x, y, r, p, p0 = 0, pr0 = 0;
  int imin, imax, jmin,jmax; //  xci, yci

  if (ois == NULL)	return NULL;
  if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
  if (rz == NULL)		return NULL;
  ps = ois->im.pixel;
  ony = ois->im.ny;	onx = ois->im.nx;
  type = ois->im.data_type;
  for (i = 0; i < 2*r_max; i++)	rz[i] = 0;

  imin = (int)(yc - (r_max/ay));
  imax = (int)(0.5 + yc + (r_max/ay));
  jmin = (int)(xc -  r_max/ax);
  jmax = (int)(0.5 + xc + r_max/ax);
  imin = (imin < 0) ? 0 : imin;
  imax = (imax > ony) ? ony : imax;
  jmin = (jmin < 0) ? 0 : jmin;
  jmax = (jmax > onx) ? onx : jmax;
  imin += (imin%2);
  jmin += (jmin%2);
  if (type == IS_RGB_PICTURE)
    {
      for (i = imin; i < imax; i+=2)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j+=2)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].ch[3*j+channel];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		}
	      else
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		}
	    }
	}
    }
  for (i = 0; i < r_max; i++)
    {
      rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
    }
  p0 = (pr0 == 0) ? 0 : p0/pr0;
  for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i];
  for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i];
  rz[0] = p0;
  return rz;
}

// grab the BLUE in a Bayer Raw image
float *radial_non_pixel_square_image_sym_profile_in_array_B_on_RAW8(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay, int channel)
{
  int i, j;
  union pix *ps;
  int onx, ony, ri, type;
  float z = 0, x, y, r, p, p0 = 0, pr0 = 0;
  int imin, imax, jmin,jmax; //  xci, yci

  if (ois == NULL)	return NULL;
  if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
  if (rz == NULL)		return NULL;
  ps = ois->im.pixel;
  ony = ois->im.ny;	onx = ois->im.nx;
  type = ois->im.data_type;
  for (i = 0; i < 2*r_max; i++)	rz[i] = 0;

  imin = (int)(yc - (r_max/ay));
  imax = (int)(0.5 + yc + (r_max/ay));
  jmin = (int)(xc -  r_max/ax);
  jmax = (int)(0.5 + xc + r_max/ax);
  imin = (imin < 0) ? 0 : imin;
  imax = (imax > ony) ? ony : imax;
  jmin = (jmin < 0) ? 0 : jmin;
  jmax = (jmax > onx) ? onx : jmax;
  imin = (imin%2) ? imin : imin + 1;
  jmin = (jmin%2) ? jmin : jmin + 1;
  if (type == IS_RGB_PICTURE)
    {
      for (i = imin; i < imax; i+=2)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j+=2)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].ch[3*j+channel];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		}
	      else
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		}
	    }
	}
    }
  for (i = 0; i < r_max; i++)
    {
      rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
    }
  p0 = (pr0 == 0) ? 0 : p0/pr0;
  for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i];
  for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i];
  rz[0] = p0;
  return rz;
}


// grab the GREEN in a Bayer Raw image
float *radial_non_pixel_square_image_sym_profile_in_array_G_on_RAW8(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay, int channel)
{
  int i, j;
  union pix *ps;
  int onx, ony, ri, type;
  float z = 0, x, y, r, p, p0 = 0, pr0 = 0;
  int imin, imax, jmin,jmax, jmin2; //  xci, yci

  if (ois == NULL)	return NULL;
  if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
  if (rz == NULL)		return NULL;
  ps = ois->im.pixel;
  ony = ois->im.ny;	onx = ois->im.nx;
  type = ois->im.data_type;
  for (i = 0; i < 2*r_max; i++)	rz[i] = 0;

  imin = (int)(yc - (r_max/ay));
  imax = (int)(0.5 + yc + (r_max/ay));
  jmin = (int)(xc -  r_max/ax);
  jmax = (int)(0.5 + xc + r_max/ax);
  imin = (imin < 0) ? 0 : imin;
  imax = (imax > ony) ? ony : imax;
  jmin = (jmin < 0) ? 0 : jmin;
  jmax = (jmax > onx) ? onx : jmax;
  if (type == IS_RGB_PICTURE)
    {
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  if (i%2) jmin2 = (jmin%2) ? jmin + 1 : jmin;
	  else jmin2 = (jmin%2) ? jmin : jmin + 1;
	  for (j = jmin2; j < jmax; j+=2)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].ch[3*j+channel];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		}
	      else
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		}
	    }
	}
    }
  for (i = 0; i < r_max; i++)
    {
      rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
    }
  p0 = (pr0 == 0) ? 0 : p0/pr0;
  for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i];
  for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i];
  rz[0] = p0;
  return rz;
}






int _get_camera_shutter_in_us(int *set, int *smin, int *smax)
{
# ifdef SDK_3
    double dmin, dmax, dintervall;
# endif
    double dexp;
# ifdef SDK_4
    int nRet;
    double dblRange[3];
    UINT nCaps = 0;
# endif

# ifdef SDK_3
    is_GetExposureRange(m_hCam, &dmin, &dmax, &dintervall);
    is_SetExposureTime(m_hCam, IS_GET_EXPOSURE_TIME, &dexp);

    if (set) *set = (int)(0.5 + (1000 * dexp));

    if (smin) *smin = (int)(0.5 + (1000 * dmin));

    if (smax) *smax = (int)(0.5 + (1000 * dmax));

# elif defined(SDK_4)
    nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_CAPS, (void *)&nCaps, sizeof(nCaps));

    if (nRet == IS_SUCCESS)
    {
        // Fine increment supported
        if (nCaps & IS_EXPOSURE_CAP_FINE_INCREMENT)
        {
            nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_FINE_INCREMENT_RANGE
                               , (void *)dblRange, sizeof(dblRange));

            if (nRet == IS_SUCCESS)
            {
                if (smin) *smin = (int)(0.5 + (1000 * dblRange[0]));

                if (smax) *smax = (int)(0.5 + (1000 * dblRange[1]));
            }
        }
        else if (nCaps & IS_EXPOSURE_CAP_EXPOSURE)
        {
            nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE
                               , (void *)dblRange, sizeof(dblRange));

            if (nRet == IS_SUCCESS)
            {
                if (smin) *smin = (int)(0.5 + (1000 * dblRange[0]));

                if (smax) *smax = (int)(0.5 + (1000 * dblRange[1]));
            }
        }
    }

    nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE, (void *)&dexp, sizeof(dexp));

    if (nRet == IS_SUCCESS)   if (set) *set = (int)(0.5 + (1000 * dexp));

# endif
    return 0;
}



int _set_camera_shutter_in_us(int exp, int *set, int *smin, int *smax)
{
# ifdef SDK_3
    double dmin, dmax, dintervall;
# endif
    double dexp, nexp;
    int min = 0, max = 0;
# ifdef SDK_4
    int nRet;
    double dblRange[3];
    UINT nCaps = 0;
# endif

# ifdef SDK_3
    is_GetExposureRange(m_hCam, &dmin, &dmax, &dintervall);
    is_SetExposureTime(m_hCam, IS_GET_EXPOSURE_TIME, &dexp);
    min = (int)(0.5 + (1000 * dmin));
    max = (int)(0.5 + (1000 * dmax));
# endif

# ifdef SDK_4
    nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_CAPS, (void *)&nCaps, sizeof(nCaps));

    if (nRet == IS_SUCCESS)
    {
        // Fine increment supported
        if (nCaps & IS_EXPOSURE_CAP_FINE_INCREMENT)
        {
            nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_FINE_INCREMENT_RANGE
                               , (void *)dblRange, sizeof(dblRange));

            if (nRet == IS_SUCCESS)
            {
                min = (int)(0.5 + (1000 * dblRange[0]));
                max = (int)(0.5 + (1000 * dblRange[1]));
            }
        }
        else if (nCaps & IS_EXPOSURE_CAP_EXPOSURE)
        {
            nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE
                               , (void *)dblRange, sizeof(dblRange));

            if (nRet == IS_SUCCESS)
            {
                min = (int)(0.5 + (1000 * dblRange[0]));
                max = (int)(0.5 + (1000 * dblRange[1]));
            }
        }
    }

    is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE, (void *)&dexp, sizeof(dexp));
# endif

    if (exp == 0) exp = max;  // no shutter means maximum exposure

    if (exp < min) exp = min;

    if (exp > max) exp = max;

    dexp = (double)exp / 1000;

# ifdef SDK_3
    is_SetExposureTime(m_hCam, dexp, &nexp);
# elif defined(SDK_4)
    is_Exposure(m_hCam, IS_EXPOSURE_CMD_SET_EXPOSURE, (void *)&dexp, sizeof(dexp));
    is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE, (void *)&nexp, sizeof(nexp));
# endif

    if (set) *set = (int)(0.5 + (1000 * nexp));

    if (smin) *smin = min;

    if (smax) *smax = max;

    return 0;
}

int _get_camera_gain(float *gain, float *fmin, float *fmax, float *finc)
{
    int gMaxGain;//, ninpos;
    int gGain;
    float fmaxg, fming;

    gMaxGain = is_SetHWGainFactor(m_hCam, IS_INQUIRE_MASTER_GAIN_FACTOR, 100);
    fmaxg = (float)gMaxGain / 100;
    fming = 1.0;
    gGain = is_SetHWGainFactor(m_hCam, IS_GET_MASTER_GAIN_FACTOR, 100);

    if (gain) *gain = ((float)gGain) / 100;

    if (fmin) *fmin = fming;

    if (fmax) *fmax = fmaxg;

    if (finc) *finc = ((float)1) / 100;

    return 0;
}

int _set_camera_gain(float gain, float *set, float *fmin, float *fmax, float *finc)
{
    int gMaxGain;//, ninpos;
    int gGain;
    float fmaxg, fming;

    gMaxGain = is_SetHWGainFactor(m_hCam, IS_INQUIRE_MASTER_GAIN_FACTOR, 100);
    fmaxg = (float)gMaxGain / 100;
    fming = 1.0;

    gGain = (int)(0.5 + (100 * gain));
    gGain = (gGain > gMaxGain) ? gMaxGain : gGain;
    gGain = (gGain < 100) ? 100 : gGain;
    is_SetHWGainFactor(m_hCam, IS_SET_MASTER_GAIN_FACTOR, gGain);

    gGain = is_SetHWGainFactor(m_hCam, IS_GET_MASTER_GAIN_FACTOR, 100);

    if (gain) *set = ((float)gGain) / 100;

    if (fmin) *fmin = fming;

    if (fmax) *fmax = fmaxg;

    if (finc) *finc = ((float)1) / 100;

    return 0;
}



int do_set_camera_freq(void)
{
    double fr, nfr;
    float ffr;
    int pmin, pmax, pm, ret;
    double min, max, intervall;
    char question[1024];
    int i, optsn = 1;
    float gain, fmin, fmax, finc, g;
    int set = 0, smin = 0, smax = 0, exp;

# ifdef SDK_4
    int nRet;
    UINT nPixelClock = 0, nRange[3] = {0, 0, 0};
# endif


    if (updating_menu_state != 0)
      {
	if (m_hCam == 0)  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }

# ifdef SDK_3
    ret = is_GetPixelClockRange(m_hCam, &pmin, &pmax);

    if (ret != IS_SUCCESS)       win_printf("Could not get camera rate");

    pm = is_SetPixelClock(m_hCam, IS_GET_PIXEL_CLOCK);
# elif defined(SDK_4)
     nRet = is_PixelClock(m_hCam, IS_PIXELCLOCK_CMD_GET_RANGE, (void *)nRange, sizeof(nRange));

    if (nRet == IS_SUCCESS)
    {
        pmin = nRange[0];
        pmax = nRange[1];
    }
    else return win_printf("Cannot get camera pixel clock range");

    ret = is_PixelClock(m_hCam, IS_PIXELCLOCK_CMD_GET, (void *)&nPixelClock, sizeof(nPixelClock));

    if (ret != IS_SUCCESS) return win_printf("Cannot get camera pixel clock");

    pm = nPixelClock;
# endif

    is_GetFrameTimeRange(m_hCam, &min, &max, &intervall);
    is_SetFrameRate(m_hCam, IS_GET_FRAMERATE, &fr);

    ffr = fr;
    //  fpsmin= 1/max ; fpsmax=1/min ; fpsn= 1/(min + n * intervall)
    snprintf(question, sizeof(question), "Camera frequency settings\n"
             "Pixel clock (%d min) < (%d present) < (%d max)\n"
             "New pixel clock %%8d\n"
             "Frame rate (%g min) < (%g present) < (%g max)\n"
             "New Frame rate %%8f\n"
	     "%%bOptimize S/N"
             , pmin, pm, pmax, (float)1 / max, (float)1 / fr, (float)1 / min);

    i = win_scanf(question, &pm, &ffr,&optsn);

    if (i == WIN_CANCEL)    return 0;

# ifdef SDK_3
    is_SetPixelClock(m_hCam, pm);
# elif defined(SDK_4)
    nPixelClock = pm;
    ret = is_PixelClock(m_hCam, IS_PIXELCLOCK_CMD_SET, (void *)&nPixelClock, sizeof(nPixelClock));
    if (ret != IS_SUCCESS)       win_printf("Could not set camera pixel clock");
# endif

    fr = ffr;
    is_SetFrameRate(m_hCam, fr, &nfr);

    ret = is_SetFrameRate(m_hCam, IS_GET_FRAMERATE, &fr);

    if (ret != IS_SUCCESS)       win_printf("Could not get camera rate");


    if (optsn)
      {
	_get_camera_gain(&gain, &fmin, &fmax, &finc);
	_get_camera_shutter_in_us(&set, &smin, &smax);
	g = fmin;
	_set_camera_gain(g, &gain, &fmin, &fmax, &finc);
	exp = smax;
	_set_camera_shutter_in_us(exp, &set, &smin, &smax);
      }


    return 0;
}





int do_set_camera_exposure(void)
{
    int i;
    double min, max, intervall, exp, nexp;
    float fexp;
    char question[1024];
# ifdef SDK_4
    int nRet;
    double dblRange[3];
    UINT nCaps = 0;
# endif
    
    if (updating_menu_state != 0)
      {
	if (m_hCam == 0)  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }


# ifdef SDK_3
  is_GetExposureRange (m_hCam, &min, &max, &intervall);
  is_SetExposureTime (m_hCam, IS_GET_EXPOSURE_TIME, &nexp);
# elif defined(SDK_4)
    nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_CAPS, (void*)&nCaps, sizeof(nCaps));
    if (nRet == IS_SUCCESS)
    {      // Fine increment supported
        if (nCaps & IS_EXPOSURE_CAP_FINE_INCREMENT)
        {
            nRet = is_Exposure(m_hCam,IS_EXPOSURE_CMD_GET_FINE_INCREMENT_RANGE
                               ,(void*)dblRange,sizeof(dblRange));
            if (nRet == IS_SUCCESS)
            {
                min = (int)(dblRange[0]);
                max = (int)(dblRange[1]);
                intervall = (int)(dblRange[2]);
            }
        }
        else if (nCaps & IS_EXPOSURE_CAP_EXPOSURE)
        {
            nRet = is_Exposure(m_hCam,IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE
                               ,(void*)dblRange,sizeof(dblRange));
            if (nRet == IS_SUCCESS)
            {
                min = (int)(dblRange[0]);
                max = (int)(dblRange[1]);
                intervall = (int)(dblRange[2]);
            }
        }
    }
    nRet = is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE, (void*)&nexp, sizeof(nexp));
# endif

  fexp = nexp;

  snprintf(question,sizeof(question),"Camera exposure setting\n"
	   "Range min %g max %g dt %g"
	   "new value %%f",min,max,intervall);
  i = win_scanf(question,&fexp);
  if (i == WIN_CANCEL)    return 0;
  exp = fexp;
# ifdef SDK_3
  is_SetExposureTime (m_hCam, exp, &nexp);
# elif defined(SDK_4)
  is_Exposure(m_hCam, IS_EXPOSURE_CMD_SET_EXPOSURE, (void*)&exp, sizeof(exp));
  is_Exposure(m_hCam, IS_EXPOSURE_CMD_GET_EXPOSURE, (void*)&nexp, sizeof(nexp));
# endif


  return 0;
}

int get_camera_y_binning(void)
{
  return is_SetBinning (m_hCam, IS_GET_BINNING_FACTOR_VERTICAL);
}

int get_camera_x_binning(void)
{
  return is_SetBinning (m_hCam, IS_GET_BINNING_FACTOR_HORIZONTAL);
}

int set_camera_y_binning(int bin_y)
{
  int binpos;
  int binning = 0;

  if  (bin_y == 2)      binning = IS_BINNING_2X_VERTICAL;
  else if (bin_y == 3)  binning = IS_BINNING_3X_VERTICAL;
  else if (bin_y == 4)  binning = IS_BINNING_4X_VERTICAL;
  else if (bin_y == 5)  binning = IS_BINNING_5X_VERTICAL;
  else if (bin_y == 6)  binning = IS_BINNING_6X_VERTICAL;
  else if (bin_y == 8)  binning = IS_BINNING_8X_VERTICAL;
  else if (bin_y == 16)  binning = IS_BINNING_16X_VERTICAL;
  else return 1;

  binpos = is_SetBinning (m_hCam, IS_GET_SUPPORTED_BINNING);
  //if (binpos & binning)
  //{
  binpos = is_SetBinning (m_hCam, binning);
  if (binpos == IS_SUCCESS) return 0;
  //}
  else return 2;
}


int set_camera_x_binning(int bin_x)
{
  int binpos;
  int binning = 0;

  if  (bin_x == 2)      binning = IS_BINNING_2X_HORIZONTAL;
  else if (bin_x == 3)  binning = IS_BINNING_3X_HORIZONTAL;
  else if (bin_x == 4)  binning = IS_BINNING_4X_HORIZONTAL;
  else if (bin_x == 5)  binning = IS_BINNING_5X_HORIZONTAL;
  else if (bin_x == 6)  binning = IS_BINNING_6X_HORIZONTAL;
  else if (bin_x == 8)  binning = IS_BINNING_8X_HORIZONTAL;
  else if (bin_x == 16)  binning = IS_BINNING_16X_HORIZONTAL;
  else return 1;

  binpos = is_SetBinning (m_hCam, IS_GET_SUPPORTED_BINNING);
  win_printf("Binning sup %d selected %d",binpos,binpos & binning);
  //  if (binpos & binning)
  //{
  binpos = is_SetBinning (m_hCam, binning);
  if (binpos == IS_SUCCESS) return 0;
  //}
  else return 2;
}



int do_set_camera_binning(void)
{
  int binpos, i;
  int binx2, binx3, binx4, binx6, biny2, biny3, biny4, biny6, binx = 0, biny = 0, binning = 0;
  char question[1024];

    if (updating_menu_state != 0)
      {
	if (m_hCam == 0)  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }



  binpos = is_SetBinning (m_hCam, IS_GET_SUPPORTED_BINNING);
  biny2 = (binpos & IS_BINNING_2X_VERTICAL) ? 1 : 0;
  biny3 = (binpos & IS_BINNING_3X_VERTICAL) ? 1 : 0;
  biny4 = (binpos & IS_BINNING_4X_VERTICAL) ? 1 : 0;
  biny6 = (binpos & IS_BINNING_6X_VERTICAL) ? 1 : 0;

  binx2 = (binpos & IS_BINNING_2X_HORIZONTAL) ? 1 : 0;
  binx3 = (binpos & IS_BINNING_3X_HORIZONTAL) ? 1 : 0;
  binx4 = (binpos & IS_BINNING_4X_HORIZONTAL) ? 1 : 0;
  binx6 = (binpos & IS_BINNING_6X_HORIZONTAL) ? 1 : 0;

  binpos = is_SetBinning (m_hCam, IS_GET_BINNING);
  if      (binpos & IS_BINNING_2X_VERTICAL) binx = 1;
  else if (binpos & IS_BINNING_3X_VERTICAL) binx = 2;
  else if (binpos & IS_BINNING_4X_VERTICAL) binx = 3;
  else if (binpos & IS_BINNING_6X_VERTICAL) binx = 4;
  else binx = 0;

  if      (binpos & IS_BINNING_2X_HORIZONTAL) biny = 1;
  else if (binpos & IS_BINNING_3X_HORIZONTAL) biny = 2;
  else if (binpos & IS_BINNING_4X_HORIZONTAL) biny = 3;
  else if (binpos & IS_BINNING_6X_HORIZONTAL) biny = 4;
  else biny = 0;


  /*
  win_printf("Supported binning fot this camera\n"
	     "Along X x2 %s x3 %s x4 %s x6 %s\n"
	     "Along Y x2 %s y3 %s x4 %s x6 %s\n"
	     ,(binx2)?"yes":"no ",(binx3)?"yes":"no ",(binx4)?"yes":"no ",(binx6)?"yes":"no "
	     ,(biny2)?"yes":"no ",(biny3)?"yes":"no ",(biny4)?"yes":"no ",(biny6)?"yes":"no ");
  */


  snprintf(question,sizeof(question),"Supported binning fot this camera\n"
	     "Along X None %%R x2 %s %%r x3 %s %%r x4 %s %%r x6 %s %%r\n"
	     "Along Y None %%R x2 %s %%r y3 %s %%r x4 %s %%r x6 %s %%r\n"
	   ,(binx2)?"yes":"no ",(binx3)?"yes":"no ",(binx4)?"yes":"no ",(binx6)?"yes":"no "
	   ,(biny2)?"yes":"no ",(biny3)?"yes":"no ",(biny4)?"yes":"no ",(biny6)?"yes":"no ");


  i = win_scanf(question,&binx,&biny);
  if (i == WIN_CANCEL) return OFF;



  if  (binx == 1 && binx2)      binning = IS_BINNING_2X_VERTICAL;
  else if (binx == 2 && binx3)  binning = IS_BINNING_3X_VERTICAL;
  else if (binx == 3 && binx4)  binning = IS_BINNING_4X_VERTICAL;
  else if (binx == 4 && binx6)  binning = IS_BINNING_6X_VERTICAL;
  else binning = 0;


  if  (biny == 1 && biny2)      binning |= IS_BINNING_2X_HORIZONTAL;
  else if (biny == 2 && biny2)  binning |= IS_BINNING_3X_HORIZONTAL;
  else if (biny == 3 && biny2)  binning |= IS_BINNING_4X_HORIZONTAL;
  else if (biny == 4 && biny2)  binning |= IS_BINNING_6X_HORIZONTAL;
  if (binning == 0) binning = IS_BINNING_DISABLE;

  is_SetBinning (m_hCam, binning);

  return 0;

}

int do_set_camera_gain(void)
{
  int i;
  int gMaxRed, gMaxGreen, gMaxBlue, gMaxGain; //, ninpos;
  int gRed, gGreen, gBlue, gGain, gboost, gboostavail;
  char question[1024];


    if (updating_menu_state != 0)
      {
	if (m_hCam == 0)  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }  


//Query the maximum gain factor for the red channel:
  gMaxRed = is_SetHWGainFactor (m_hCam, IS_INQUIRE_RED_GAIN_FACTOR, 100);
  gMaxGreen = is_SetHWGainFactor (m_hCam, IS_INQUIRE_GREEN_GAIN_FACTOR, 100);
  gMaxBlue = is_SetHWGainFactor (m_hCam, IS_INQUIRE_BLUE_GAIN_FACTOR, 100);
  gMaxGain = is_SetHWGainFactor (m_hCam, IS_INQUIRE_MASTER_GAIN_FACTOR, 100);


  gRed = is_SetHWGainFactor (m_hCam, IS_GET_RED_GAIN_FACTOR, 100);
  gGreen = is_SetHWGainFactor (m_hCam, IS_GET_GREEN_GAIN_FACTOR, 100);
  gBlue = is_SetHWGainFactor (m_hCam, IS_GET_BLUE_GAIN_FACTOR, 100);
  gGain = is_SetHWGainFactor (m_hCam, IS_GET_MASTER_GAIN_FACTOR, 100);

  gboostavail = is_SetGainBoost (m_hCam, IS_GET_SUPPORTED_GAINBOOST);
  gboost = is_SetGainBoost (m_hCam, IS_GET_GAINBOOST);

  win_printf("Camera has master gain %s current %d max %d\n"
	     "Camera has Red gain %s current %d max %d\n"
	     "Camera has Green gain %s current %d max %d\n"
	     "Camera has Blue gain %s current %d max %d\n"
	     "Camare support gain boost %s current val %d\n"
	     ,((sInfo.bMasterGain)?"Yes":"No"),gGain,gMaxGain,
	     ((sInfo.bRGain) ?"Yes":"No"),gRed,gMaxRed,
	     ((sInfo.bGGain) ?"Yes":"No"),gGreen,gMaxGreen,
	     ((sInfo.bBGain) ?"Yes":"No"),gBlue,gMaxBlue,
	     ((gboostavail) ?"Yes":"No"),gboost
	     );



  snprintf(question,1024,"Camera %s %s master gain current value %%8d max %d\n"
	   "Camera %s gain boost (No boost->%%R, boost %%r)\n"
	   ,sInfo.strSensorName,((sInfo.bMasterGain)?"has":"does not have"),gMaxGain,
	     ((gboostavail) ?"supports":"does not support"));

  i = win_scanf(question,&gGain,&gboost);
  if (i == WIN_CANCEL) return OFF;

  is_SetHWGainFactor (m_hCam, IS_SET_MASTER_GAIN_FACTOR, gGain);

  is_SetGainBoost (m_hCam, (gboost) ? IS_SET_GAINBOOST_ON : IS_SET_GAINBOOST_OFF);

  /*


  gGain = is_SetHardwareGain (m_hCam, IS_GET_MASTER_GAIN,IS_IGNORE_PARAMETER,
			      IS_IGNORE_PARAMETER,IS_IGNORE_PARAMETER);
  gRed = is_SetHardwareGain (m_hCam, IS_GET_RED_GAIN,IS_IGNORE_PARAMETER,
			      IS_IGNORE_PARAMETER,IS_IGNORE_PARAMETER);
  gGreen = is_SetHardwareGain (m_hCam, IS_GET_GREEN_GAIN,IS_IGNORE_PARAMETER,
			      IS_IGNORE_PARAMETER,IS_IGNORE_PARAMETER);
  gBlue = is_SetHardwareGain (m_hCam, IS_GET_BLUE_GAIN,IS_IGNORE_PARAMETER,
			      IS_IGNORE_PARAMETER,IS_IGNORE_PARAMETER);

  win_printf("Camera has master gain %s current %d max %d\n"
	     "Camera has Red gain %s current %d max %d\n"
	     "Camera has Green gain %s current %d max %d\n"
	     "Camera has Blue gain %s current %d max %d\n"
	     "Camare support gain boost %s current val %d\n"
	     ,((sInfo.bMasterGain)?"Yes":"No"),gGain,gMaxGain,
	     ((sInfo.bRGain) ?"Yes":"No"),gRed,gMaxRed,
	     ((sInfo.bGGain) ?"Yes":"No"),gGreen,gMaxGreen,
	     ((sInfo.bBGain) ?"Yes":"No"),gBlue,gMaxBlue,
	     ((gboostavail) ?"Yes":"No"),gboost
	     );

	     did not work ?

  */

  /*
INT is_SetHardwareGain (HIDS hCam, INT nMaster,
INT nRed, INT nGreen, INT nBlue)
INT is_SetHWGainFactor (HIDS hCam, INT nMode, INT nFactor)

IS_SET_GAINBOOST_ON
  */
  return 0;
}


/*
//==============================================================////
// Terminate image acquisition thread
//==============================================================////
void Terminate_UEYE_Thread(void)
{
    SetEvent(hStreamEvent);
    bEnableThread = FALSE;
}
*/
//==============================================================////
// Wait for thread to terminate
//==============================================================////

void WaitForThreadToTerminate(void)
{
  //WaitForSingleObject(hStreamEvent, INFINITE);
    // Close the thread handle and stream event handle
    CloseThreadHandle();
}
////////////////////////////////////////////////////////////////////


//==============================================================////
// Close handles and stream
//==============================================================////
void CloseThreadHandle(void)
{
#ifdef XV_WIN32
  
    if(hStreamThread)
    {
        CloseHandle(hStreamThread);
        hStreamThread = NULL;
    }
#endif
    /*
    if(hStreamEvent)
    {
        CloseHandle(hStreamEvent);
        hStreamEvent = NULL;
    }
    */
}
////////////////////////////////////////////////////////////////////

# ifdef XV_WIN32
DWORD WINAPI StreamProcess(LPVOID lpParam)
# elif defined XV_UNIX
void *StreamProcess (void *lpParam)
# endif
{
    char *buf;
    int id, ret, mok = 0, id_1 = 0;
    INT ids_err = 0;
    IS_CHAR *ids_err_msg = NULL;
#ifdef XV_WIN32
    DWORD  iWaitResult, retf = 1;
#elif defined XV_UNIX
    int lastEventReturn = IS_SUCCESS;
    void *retf = NULL;
#endif
    int	iResult = 0, state = 0;
    unsigned long  dt0, dt1;
    long long dt;
    char message[1024];
    UEYEIMAGEINFO ImageInfo;
    unsigned long long u64TimestampDevice, abs_fr_nb = 0, u64t_1 = 0;
    O_p *op;
    static double Zobj = -1;
    static int frame_mod = 0;
    
    
    (void)lpParam;
#ifdef XV_WIN32
    if (hEventFreeze != NULL)    CloseHandle(hEventFreeze);
    // Create a new one
    hEventFreeze = CreateEvent(NULL, TRUE, FALSE, NULL);
    hEventNewImage = CreateEvent(NULL, TRUE, FALSE, NULL);
    hEventRemoved = CreateEvent(NULL, TRUE, FALSE, NULL);
    hEventReplug = CreateEvent(NULL, TRUE, FALSE, NULL);
  // Make a list of events to be used in WaitForMultipleObjects

    EventList[0] = hEventNewImage;
    EventList[1] = hEventFreeze;
    EventList[2] = hEventRemoved;
    EventList[3] = hEventReplug;

    ret = is_InitEvent(m_hCam, hEventNewImage,IS_SET_EVENT_FRAME);
    if( ret != IS_SUCCESS )
      { win_printf_OK("could not init event!"); return retf;}
    ret = is_InitEvent(m_hCam, hEventRemoved,IS_SET_EVENT_REMOVE);
    if( ret != IS_SUCCESS )
      { win_printf_OK("could not init event!"); return retf;}
    ret = is_InitEvent(m_hCam, hEventReplug,IS_SET_EVENT_DEVICE_RECONNECTED);
    if( ret != IS_SUCCESS )
      { win_printf_OK("could not init event!"); return retf;}

#endif

    ret = is_EnableEvent(m_hCam, IS_SET_EVENT_FRAME);
    if( ret != IS_SUCCESS )
      { win_printf_OK("could not init event!"); return retf;}
    ret = is_EnableEvent(m_hCam, IS_SET_EVENT_REMOVE);
    if( ret != IS_SUCCESS )
      { win_printf_OK("could not init event!"); return retf;}
    ret = is_EnableEvent(m_hCam, IS_SET_EVENT_DEVICE_RECONNECTED);
    if( ret != IS_SUCCESS )
      { win_printf_OK("could not init event!"); return retf;}  
    
    dt0 = my_uclock();
    is_CaptureVideo (m_hCam, 100); // IS_WAIT

    while(bEnableThread)
      {
#ifdef XV_WIN32      
	// Wait for New Buffer event (or Freeze event)
	iWaitResult = WaitForMultipleObjects(4, EventList,FALSE, 10000/*00*/);
	// Did we get a new buffer event?
	if(iWaitResult == WAIT_OBJECT_0)
#else
	  lastEventReturn = is_WaitEvent(m_hCam, IS_SET_EVENT_FRAME, 10000);

        if (lastEventReturn == IS_SUCCESS)
#endif
	
	  {
	    is_GetActiveImageMem(m_hCam,&buf,&id);
	    //mok = (buf == oid->im.mem[id]) ? 1 : 0;
	    //for (mok = 0; buf != oid->im.mem[mok]; mok++);
	    my_set_window_title("image id %d %s",id,xvueye_ti_message);
	    ret = is_GetImageInfo( m_hCam, id, &ImageInfo, sizeof(ImageInfo));
	    if (ret == IS_SUCCESS)
	      {
		u64TimestampDevice = ImageInfo.u64TimestampDevice;
		if (u64t_1 == 0) u64t_1 = u64TimestampDevice; 
		abs_fr_nb = ImageInfo.u64FrameNumber;
	      }
	    id = id - 2;
	    id = (id < 0) ? oi_xvuEye->im.n_f + id : id;
	    op = (oi_xvuEye->n_op > 0) ? oi_xvuEye->o_p[0] : NULL;
	    mok++;
	    if (id != id_1)
	      {
		dt1 = my_uclock();
		
		//if ((oid->need_to_refresh & (INTERNAL_BITMAP_NEED_REFRESH | BITMAP_NEED_REFRESH)) == 0)
		switch_frame(oi_xvuEye,id);
		if (track_image != NULL)
		  {
		    track_image(oi_xvuEye,id,abs_fr_nb,u64TimestampDevice,data_ptr);
		  }
		dt = (long long)dt1;
		dt -= (long long)dt0;
		
		Zobj = -1;
		if ((op != NULL) && (grab_PI_z_value == NULL) && (op->n_dat > 0)
			 && (op->dat != NULL) && (op->dat[0]->nx > id))
		  {
		    op->dat[0]->yd[id] = u64TimestampDevice - u64t_1; 
		    op->dat[0]->xd[id] = abs_fr_nb;		    
		  }		
		else if ((op != NULL) && (grab_PI_z_value != NULL) && (op->n_dat > 0)
		    && (op->dat != NULL) && (op->dat[0]->nx > id) && frame_mod >= 200)
		  {
		    Zobj = op->dat[0]->yd[id] = grab_PI_z_value();
		    op->dat[0]->xd[id] = id;
		    frame_mod = 0;
		  }
		else if (Zobj != -1)
		  {
		    op->dat[0]->yd[id] = grab_PI_z_value();
		    op->dat[0]->xd[id] = id;
		  }
		
		frame_mod += dt;
		
		//if (state == 0)
		//my_set_window_title("im id %d, position %g, mok %d camera fr %llu d-Tstamp %llu freq %g", id, Zobj, mok, abs_fr_nb, u64TimestampDevice - u64t_1, (double)get_my_uclocks_per_sec()/(double)(dt));
		//else
		//my_set_window_title("camera removed im id %d mok %d freq %g", id, mok, (double)get_my_uclocks_per_sec()/(double)(dt));
		
		dt0 = dt1;
		id_1 = id;
		mok = 0;
		//u64t_1 = u64TimestampDevice;
#ifdef XV_WIN32	      
# ifdef AVI_OK
		if (rec_AVI)
		  {
		    isavi_AddFrame (nAviID, oi_xvuEye->im.mem[id]);
		  }
# endif
# endif	      
	      }
#ifdef XV_WIN32	  
	    ResetEvent(hEventNewImage);  // needed to get one event per frame
#endif	  
	  }
#ifdef XV_WIN32
        else if (iWaitResult == WAIT_OBJECT_0 + 2) // camera removed
#else
        else if (lastEventReturn == IS_TIMED_OUT)
#endif
	{
	  state = 1;
	}
#ifdef XV_WIN32	
        else   if(iWaitResult == WAIT_OBJECT_0 + 3)  // camera reconnected
#else
        else if (lastEventReturn == IS_TIMED_OUT)
#endif	
	{
	  state = 0;
	  win_printf("10s Timeout");
	}
      else
        {
            is_GetError(m_hCam, &ids_err, &ids_err_msg);
#ifdef XV_WIN32
            warning_message("ids: err: %d, msg: %s, last event return: %ld\n", ids_err, ids_err_msg, iWaitResult);
#else
            warning_message("ids: err: %d, msg: %s, last event return: %d\n", ids_err, ids_err_msg, lastEventReturn);
#endif
	  
#ifdef XV_WIN32      
	  switch(iWaitResult)
            {
	      // Freeze event
	    case	WAIT_OBJECT_0 + 1:
	      iResult = 1;
	      bEnableThread = FALSE;
	      strcat(message,"freeze process");
	      my_set_window_title(message);
	      break;
	      // Timeout
	    case	WAIT_TIMEOUT:
	      iResult = 2;
	      strcat(message,"- 10s timeout");
	      my_set_window_title(message);
	      bEnableThread = FALSE;
	      break;
	      // Unknown?
	    default:
	      iResult = 3;
	      break;
            }
#else
	  if (is_WaitEvent(m_hCam, IS_SET_EVENT_REMOVE, 100) == IS_SUCCESS)
            {
                state = 1;
             } 
	  else if (is_WaitEvent(m_hCam, IS_SET_EVENT_DEVICE_RECONNECTED, 100) == IS_SUCCESS)
            {
                state = 0;
            }
            else
            {
                ret = 3;
            }

#endif
	  
        }
    }
  strcat(message,"- out loop");
  my_set_window_title("%s",message);


  ret = is_ExitEvent(m_hCam, IS_SET_EVENT_FRAME);
  if( ret != IS_SUCCESS )
    {
      win_printf_OK("could not init event!");
#ifdef XV_WIN32      
      return;
#elif defined XV_UNIX
      return NULL;
#endif	
    }


  strcat(message,"- closing camera");
  my_set_window_title("%s",message);
  (void)iResult;
  is_StopLiveVideo (m_hCam, 100); // IS_WAIT
  is_ClearSequence (m_hCam);
  is_ExitCamera (m_hCam);

  //Terminate_UEYE_Thread();     // Terminate the thread.

#ifdef XV_WIN32  
  // Free the kill event
    if (hEventFreeze != NULL)
      {
        CloseHandle(hEventFreeze);
        hEventFreeze = NULL;
      }

    // Free the new image event object
    if (hEventNewImage != NULL)
      {
        CloseHandle(hEventNewImage);
        hEventNewImage = NULL;
      }
#endif
    WaitForThreadToTerminate();
  strcat(message,"- Out thread");
  my_set_window_title("%s",message);
# ifdef XV_WIN32
  return 0;
# elif defined XV_UNIX
  return NULL;
# endif
  
}

BOOL CreateStreamThread(void)
{
#ifdef XV_WIN32
    DWORD dwThreadId;
    if(hStreamThread)        return FALSE;
    // Set the thread execution flag
    bEnableThread = TRUE;

    hStreamThread = CreateThread(NULL,          // default security attributes
				 0,             // use default stack size
				 (LPTHREAD_START_ROUTINE)StreamProcess,
				                // thread function
				 NULL,          // argument to thread function
				 0,             // use default creation flags
				 &dwThreadId);  // returns the thread identifier
    if(hStreamThread == NULL)
      {	//      J_DataStream_Close(m_hDS);
	win_printf_OK("No thread created");
	return FALSE;
      }
    SetThreadPriority(hStreamThread,  THREAD_PRIORITY_TIME_CRITICAL);
#elif defined XV_UNIX
    bEnableThread = TRUE;
    if (pthread_create(&stream_thread,         // threadId
                       NULL,                   // default security attributes
                       StreamProcess,          // thread function
                       NULL)!= 0)              // argument to thread function

    {        win_printf_OK("No thread created");    }
#endif    
    return TRUE;
}



/*

int shut_down_trk(void)
{
  //int shut_down_magnets(void);
  DWORD lpExitCode;

  GetExitCodeThread(hStreamThread, &lpExitCode);
  TerminateThread(hStreamThread, lpExitCode);
  CloseHandle(hStreamThread);

  while (source_running);

  return 0;
}

*/
int oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  BITMAP *imb;
  unsigned long t0 = 0;

  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

  t0 = my_uclock();
  if ((IS_DATA_IN_USE(oi) == 0) && (oi->need_to_refresh & BITMAP_NEED_REFRESH))
    {
      display_image_stuff_16M(imr,d);
      oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
    }



  imb = (BITMAP*)oi->bmp.stuff;


  if (oi->need_to_refresh & INTERNAL_BITMAP_NEED_REFRESH)
    {
      SET_BITMAP_IN_USE(oi);
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off //+ d->x
	   , imr->y_off - imb->h + d->y, imb->w, imb->h);
      release_bitmap(screen);
      oi->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
      t0 = my_uclock() - t0;
      SET_BITMAP_NO_MORE_IN_USE(oi);
    }

  return 0;
}


int adjust_black_level(void)
{
  int  i, m_Ret, nBlacklevelCaps, nOffset = 0;
  char question[1024];
 IS_RANGE_S32 nRange;


   if (updating_menu_state != 0)
      {
	if (m_hCam == 0)  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }  


  m_Ret = is_Blacklevel(m_hCam, IS_BLACKLEVEL_CMD_GET_CAPS,
			(void*)&nBlacklevelCaps, sizeof(nBlacklevelCaps));

  if (m_Ret == IS_SUCCESS)
    {
      win_printf("The user can changed the state of the auto blacklevel %d\n"
		 "The user can change the offset %d"
		 ,(nBlacklevelCaps & IS_BLACKLEVEL_CAP_SET_AUTO_BLACKLEVEL)
		 ,(nBlacklevelCaps & IS_BLACKLEVEL_CAP_SET_OFFSET));

      m_Ret = is_Blacklevel(m_hCam, IS_BLACKLEVEL_CMD_GET_OFFSET, (void*)&nOffset, sizeof(nOffset));


      m_Ret = is_Blacklevel(m_hCam, IS_BLACKLEVEL_CMD_GET_OFFSET_RANGE, (void*)&nRange, sizeof(nRange));

      snprintf(question,sizeof(question),"Black level min %d max %d inc %d\n"
	       "Present offset %d new one %%6d\n"
	       ,nRange.s32Min, nRange.s32Max, nRange.s32Inc,nOffset);
      i = win_scanf(question,&nOffset);
      if (i == WIN_CANCEL) return OFF;
      m_Ret = is_Blacklevel(m_hCam, IS_BLACKLEVEL_CMD_SET_OFFSET, (void*)&nOffset, sizeof(nOffset));
    }
  return 0;
}


int support_GPIO_1_as_input(void)
{
  INT nRet = IS_SUCCESS;
  IO_GPIO_CONFIGURATION gpioConfiguration;

  //if (m_hCam == 0) return 0;
// Read information about GPIO1
  gpioConfiguration.u32Gpio = IO_GPIO_1;
  //gpioConfiguration.u32Configuration = IS_GPIO_INPUT; IS_IO_CMD_GPIOS_GET_SUPPORTED_INPUTS
  nRet = is_IO(m_hCam, IS_IO_CMD_GPIOS_GET_CONFIGURATION, (void*)&gpioConfiguration,
            sizeof(gpioConfiguration) );
  if (nRet == IS_SUCCESS)
    {
      return (gpioConfiguration.u32Caps & IS_GPIO_INPUT) ? 1 : 0;
    }
  else return 0;
}


int config_GPIO_1_as_input(void)
{
  INT nRet = IS_SUCCESS;
  IO_GPIO_CONFIGURATION gpioConfiguration;

  //if (m_hCam == 0) return 1;
// Read information about GPIO1
  gpioConfiguration.u32Gpio = IO_GPIO_1;
  gpioConfiguration.u32Configuration = IS_GPIO_INPUT;
  nRet = is_IO(m_hCam, IS_IO_CMD_GPIOS_SET_CONFIGURATION, (void*)&gpioConfiguration,
            sizeof(gpioConfiguration));
  gpio_1_in_ok = (nRet == IS_SUCCESS) ? 1 : 0;
  return (nRet == IS_SUCCESS) ? 0: 1;
}

int read_GPIO_1_input(int *error)
{
  INT nRet = IS_SUCCESS;
  IO_GPIO_CONFIGURATION gpioConfiguration;

  //if (m_hCam == 0 || gpio_1_in_ok != 1)
  if (gpio_1_in_ok != 1)
    {
      *error = 1;
      return 0;
    }
// Read information about GPIO1
  gpioConfiguration.u32Gpio = IO_GPIO_1;
  gpioConfiguration.u32Configuration = IS_GPIO_INPUT;
  nRet = is_IO(m_hCam, IS_IO_CMD_GPIOS_GET_CONFIGURATION, (void*)&gpioConfiguration,
            sizeof(gpioConfiguration));
  if (nRet != IS_SUCCESS)
    {
      *error = 1;
      return 0;
    }
  *error = 0;
  return  gpioConfiguration.u32State;
}


int do_xvuEye_init_rolling_buffer_movie_xvin(void)
{
  imreg *imr;
  int  m_Ret, m_nSizeX, m_nSizeY, nOffset = 0;// , nBlacklevelCaps
  //IS_RANGE_S32 nRange;
  int i, x0,y0,x1,y1, col, use_AOI = 0;
  int m_lMemoryId = 0, n_frames = 128, colorMode = 0, pixsize = 8, type = IS_CHAR_IMAGE, bin_x = 0, bin_y = 0;
  char question[1024];
  unsigned char *buf;
# ifdef SDK_4
  IS_RECT rectAOI;
# endif
  IO_FLASH_PARAMS flashParams;
  int   nFlashDelayInc,  nFlashDelayMin, nFlashDelayMax;
  unsigned int nFlashDurationMin, nFlashDurationMax, nFlashDurationInc, nMode;
  //unsigned int nMode;


  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine multiply the image intensity"
			   "by a user defined scalar factor");
    }
  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf_OK("cannot find image region");

  m_hCam = (HIDS) 0; 	// open next camera
  // init camera - no window handle required
  m_Ret = is_InitCamera( &m_hCam, NULL );
  win_printf_OK("found camera? %d", m_Ret);//3 cant open, -1 not successful, 0 ok

  if( m_Ret == IS_SUCCESS )
    {
      // retrieve original image size
      is_GetSensorInfo( m_hCam, &sInfo );
      m_nSizeX = sInfo.nMaxWidth;
      m_nSizeY = sInfo.nMaxHeight;
      colorMode = sInfo.nColorMode;
      is_Blacklevel(m_hCam, IS_BLACKLEVEL_CMD_GET_OFFSET, (void*)&nOffset, sizeof(nOffset));

      // setup the color depth to the current windows setting
      //is_GetColorDepth(m_hCam, &m_nBitsPerPixel, &m_nColorMode);

      x0 = y0 = 0;
      x1 = m_nSizeX;
      y1 = m_nSizeY;



      x0 = get_config_int("IDS-camera", "AOI-x0", 0);
      y0 = get_config_int("IDS-camera", "AOI-y0", 0);
      x1 = get_config_int("IDS-camera", "AOI-w", 800);
      y1 = get_config_int("IDS-camera", "AOI-h", 600);
      x0 += x1/2;
      y0 += y1/2;
      n_frames = get_config_int("IDS-camera", "R-buf-size", 128);
      if (colorMode == IS_COLORMODE_BAYER)
	{
	  col = 3;
	  snprintf(question,sizeof(question),"For Camera %s (%dx%d)\n"
		   "with pixel size %g \\mu m\n"
		   "Black level %d\n"
		   "%%R->No AOI\n,"
                   "%%r->AOI centered (just define width and height)\n"
                   "%%r->Defines AOI fully\n"
		   "Xc %%8d (only in last case); width %%8d (for last 2 cases)\n"
		   "Yy %%8d (only in last case); height %%8d (for last 2 cases)\n"
		   "Nb. of frames in buffer %%8d\n"
		   "Binning in X %%R->1, %%r->2, %%r->3, %%r->4\n"
		   "Binning in Y %%R->1, %%r->2, %%r->3, %%r->4\n"
		   "Black and white:\n %%R->BW8bits %%r->BW10bits %%r->BW12bits \n"
		   "Color:\n %%r->RGB8bits %%r->raw8bits\n"
		   "%%r->RGB10bits %%r->raw10bits\n"
		   "%%r->RGB12bits %%r->raw12bits\n"
		   ,sInfo.strSensorName,m_nSizeX,m_nSizeY,((float)sInfo.wPixelSize)/100,nOffset);
	  i = win_scanf(question,&use_AOI,&x0,&x1,&y0,&y1,&n_frames,&bin_x,&bin_y,&col);
	}
      else
	{
	  col = 0;
	  snprintf(question,sizeof(question),"For Camera %s (%dx%d)\n"
		   "with pixel size %g \\mu m\n"
		   "Black level %d\n"
		   "%%R->No AOI\n,"
                   "%%r->AOI centered (just define width and height)\n"
                   "%%r->Defines AOI fully\n"
		   "Xc %%8d (only in last case); width %%8d (for last 2 cases)\n"
		   "Yy %%8d (only in last case); height %%8d (for last 2 cases)\n"
		   "Nb. of frames in buffer %%8d\n"
		   "Binning in X %%R->1, %%r->2, %%r->3, %%r->4\n"
		   "Binning in Y %%R->1, %%r->2, %%r->3, %%r->4\n"
		   "Black and white:\n %%R->BW8bits %%r->BW10bits %%r->BW12bits \n"
		   ,sInfo.strSensorName,m_nSizeX,m_nSizeY,((float)sInfo.wPixelSize)/100,nOffset);
	  i = win_scanf(question,&use_AOI,&x0,&x1,&y0,&y1,&n_frames,&bin_x,&bin_y,&col);
	}
      if (use_AOI == 0)
        {
            x0 = y0 = 0;
            x1 = m_nSizeX;
            y1 = m_nSizeY;
        }
      else if (use_AOI == 0)
        {
            x0 = m_nSizeX/2 - x1/2;
            y0 = m_nSizeY/2 - y1/2;
        }
      else
        {
           x0 -= x1/2;
           y0 -= y1/2;
        }

      set_config_int("IDS-camera", "AOI-x0", x0);
      set_config_int("IDS-camera", "AOI-y0", y0);
      set_config_int("IDS-camera", "AOI-w", x1);
      set_config_int("IDS-camera", "AOI-h", y1);
      set_config_int("IDS-camera", "R-buf-size", n_frames);



      if (i == WIN_CANCEL) return OFF;
      //win_printf("Bin x %d bin y %d",bin_x, bin_y);


      if (col == 1)
	{
	  m_Ret = is_SetColorMode(m_hCam, IS_CM_MONO10);
	  if( m_Ret != IS_SUCCESS)
	    return win_printf_OK("Cannot set camera to mono10 mode!");
	  pixsize = 16;
	  type = IS_UINT_IMAGE;
	}
      else if (col == 2)
	{
	  m_Ret = is_SetColorMode(m_hCam, IS_CM_MONO12);
	  if( m_Ret != IS_SUCCESS)
	    return win_printf_OK("Cannot set camera to mono12 mode!");
	  pixsize = 16;
	  type = IS_UINT_IMAGE;
	}
      else if (col == 3)
	{
	  m_Ret = is_SetColorMode(m_hCam, IS_CM_RGB8_PACKED);
	  if( m_Ret != IS_SUCCESS)
	    return win_printf_OK("Cannot set camera to RGB8 mode!");
	  pixsize = 24;
	  type = IS_RGB_PICTURE;
	}
      else if (col == 4)
     	{
	  m_Ret = is_SetColorMode(m_hCam, IS_CM_SENSOR_RAW8);
	  if( m_Ret != IS_SUCCESS)
	    return win_printf_OK("Cannot set camera to RAW8 mode!");
	  pixsize = 8;
	  type = IS_CHAR_IMAGE;
	}
      else if (col == 5)
	{
	  m_Ret = is_SetColorMode(m_hCam, IS_CM_RGB10_UNPACKED);
	  if( m_Ret != IS_SUCCESS)
	    return win_printf_OK("Cannot set camera to RGB10 mode!");
	  pixsize = 48;
	  type = IS_RGB16_PICTURE;
	}
      else if (col == 6)
     	{
	  m_Ret = is_SetColorMode(m_hCam, IS_CM_SENSOR_RAW10);
	  if( m_Ret != IS_SUCCESS)
	    return win_printf_OK("Cannot set camera to RAW10 mode!");
	  pixsize = 16;
	  type = IS_UINT_IMAGE;
	}
      else if (col == 7)
	{
	  m_Ret = is_SetColorMode(m_hCam, IS_CM_RGB12_UNPACKED);
	  if( m_Ret != IS_SUCCESS)
	    return win_printf_OK("Cannot set camera to RGB12 mode!");
	  pixsize = 48;
	  type = IS_RGB16_PICTURE;
	}
      else if (col == 8)
     	{
	  m_Ret = is_SetColorMode(m_hCam, IS_CM_SENSOR_RAW12);
	  if( m_Ret != IS_SUCCESS)
	    return win_printf_OK("Cannot set camera to RAW12 mode!");
	  pixsize = 16;
	  type = IS_UINT_IMAGE;
	}
      else
	{
	  m_Ret = is_SetColorMode(m_hCam, IS_CM_MONO8);
	  if( m_Ret != IS_SUCCESS)
	    return win_printf_OK("Cannot set camera to MONO8 mode!");
	  pixsize = 8;
	  type = IS_CHAR_IMAGE;
	}

      //x1 = x0+x1;
      //y1 = y0+y1;

# ifdef SDK_3
      is_SetAOI(m_hCam,IS_SET_IMAGE_AOI,&x0,&y0,&x1,&y1);
# elif defined(SDK_4)
      rectAOI.s32X     = x0;
      rectAOI.s32Y     = y0;
      rectAOI.s32Width = x1;
      rectAOI.s32Height = y1;
      is_AOI( m_hCam, IS_AOI_IMAGE_SET_AOI, (void*)&rectAOI, sizeof(rectAOI));
# endif

      if (bin_x)
	{
	  if (set_camera_x_binning(bin_x+1))
	    win_printf("Pb setting x bining");
	  x1 /= get_camera_x_binning();
	  //win_printf("Bin x %d cam %d",bin_x,get_camera_x_binning());
	}

      if (bin_y)
	{
	  if (set_camera_y_binning(bin_y+1))
	    win_printf("Pb setting y bining");
	  y1 /= get_camera_y_binning();
	  //win_printf("Bin y %d cam %d",bin_y,get_camera_y_binning());
	}



      oi_xvuEye = create_and_attach_movie_to_imr(imr, x1, y1, type, n_frames);
      if (oi_xvuEye == NULL)	  return win_printf_OK("Can't create dest image");

      for (i = 0; i < n_frames; i++)
	{
	  buf = oi_xvuEye->im.mem[i];
	  is_SetAllocatedImageMem( m_hCam, x1, y1, pixsize, (char*)buf, &m_lMemoryId);
	  is_AddToSequence(m_hCam,(char*)buf,m_lMemoryId);
	}


      // Set the flash to a high active pulse for each image in the trigger mode
      nMode = IO_FLASH_MODE_FREERUN_HI_ACTIVE;
      m_Ret = is_IO(m_hCam, IS_IO_CMD_FLASH_SET_MODE, (void*)&nMode, sizeof(nMode));

      // Get the minimum values for flash delay and flash duration
      m_Ret = is_IO(m_hCam, IS_IO_CMD_FLASH_GET_PARAMS_MIN, (void*)&flashParams, sizeof(flashParams));
      if (m_Ret == IS_SUCCESS)
	{
	  nFlashDelayMin   = flashParams.s32Delay;
	  nFlashDurationMin = flashParams.u32Duration;
	}
      // Get the maximum values for flash delay and flash duration
      m_Ret = is_IO(m_hCam, IS_IO_CMD_FLASH_GET_PARAMS_MAX, (void*)&flashParams, sizeof(flashParams));
      if (m_Ret == IS_SUCCESS)
	{
	  nFlashDelayMax   = flashParams.s32Delay;
	  nFlashDurationMax = flashParams.u32Duration;
	}

      // Get the increment for flash delay and flash duration
      m_Ret = is_IO(m_hCam, IS_IO_CMD_FLASH_GET_PARAMS_INC, (void*)&flashParams, sizeof(flashParams));
      if (m_Ret == IS_SUCCESS)
	{
	  nFlashDelayInc   = flashParams.s32Delay;
	  nFlashDurationInc = flashParams.u32Duration;
	}
      flashParams.s32Delay = 10;
      flashParams.u32Duration = 2000;
      // Set the current values for flash delay and flash duration
      m_Ret = is_IO(m_hCam, IS_IO_CMD_FLASH_SET_PARAMS, (void*)&flashParams, sizeof(flashParams));
      m_Ret = is_IO(m_hCam, IS_IO_CMD_FLASH_GET_PARAMS, (void*)&flashParams, sizeof(flashParams));




      /*
      win_printf("Flash parameters\nDelay [%d,%d], inc %d\nDuration [%u,%u] inc %u\n"
		 "Flash delay set to %d duration %u\n"
		 ,nFlashDelayMin,nFlashDelayMax,nFlashDelayInc
		 ,nFlashDurationMin,nFlashDurationMax,nFlashDurationInc
		 ,flashParams.s32Delay,flashParams.u32Duration);
      */


      i = support_GPIO_1_as_input();
      if (i == 0) win_printf("GPIO input 1 supported %s",(i)?"yes":"no");
      if (i)
	{
	  i = config_GPIO_1_as_input();
	  if (i) win_printf("GPIO input 1 config ok: %s",(i==0)?"yes":"no");
	}

      // Enable flash auto freerun
      //nValue = IS_FLASH_AUTO_FREERUN_ON;
      //m_Ret = is_IO(m_hCam, IS_IO_CMD_FLASH_SET_AUTO_FREERUN, (void*)&nValue, sizeof(nValue));


      //is_SetImageMem( m_hCam, oid->im.pixel[0].ch, 0 );	// set memory active

      //is_CaptureVideo (m_hCam, 100); // IS_WAIT
      oi_xvuEye->oi_idle_action = oi_idle_action;
      create_and_attach_op_to_oi(oi_xvuEye, n_frames, n_frames, 0, 0);
      CreateStreamThread();



      // display initialization
      //is_SetImageSize( m_hCam, m_nSizeX, m_nSizeY );
      //is_SetDisplayMode( m_hCam, IS_SET_DM_DIB);

      map_pixel_ratio_of_image_and_screen(oi_xvuEye, 1, 1);
      set_zmin_zmax_values(oi_xvuEye, 0, 255);
      set_z_black_z_white_values(oi_xvuEye, 0, 255);
      refresh_image(imr, imr->n_oi - 1);
    }
  else
    {
      win_printf( "ERROR: Can not open uEye camera!");
      return 0;
    }
  return (refresh_image(imr, imr->n_oi - 1));
}

int adj_AOI_position(void)
{
# ifdef SDK_3
    int ret;
# endif
    int i;
    int x0, y0, width, height;
# ifdef SDK_4
    IS_RECT rectAOI;
# endif

    if (updating_menu_state != 0)
      {
	if (m_hCam == 0)  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }  

    x0 = 0; y0 = 0; width = sInfo.nMaxWidth;  height = sInfo.nMaxHeight;

# ifdef SDK_3
    ret = is_SetAOI(m_hCam,IS_GET_IMAGE_AOI,&x0,&y0,&width,&height);
    if( ret != IS_SUCCESS )       win_printf("Could not get AOI");
# elif defined(SDK_4)
    is_AOI( m_hCam, IS_AOI_IMAGE_GET_AOI, (void*)&rectAOI, sizeof(rectAOI));
    x0 = rectAOI.s32X;
    y0 = rectAOI.s32Y;
    width = rectAOI.s32Width;
    height = rectAOI.s32Height;
# endif


    i = win_scanf("Defines AOI origin position\n"
                  "X0 %8d y0 %8d\n",&x0,&y0);
    if (i == WIN_CANCEL) return OFF;

# ifdef SDK_3
    ret = is_SetAOI(m_hCam,IS_SET_IMAGE_AOI,&x0,&y0,&width,&height);
    if( ret != IS_SUCCESS )       win_printf("Could not set AOI");
# elif defined(SDK_4)
    rectAOI.s32X     = x0;
    rectAOI.s32Y     = y0;
    rectAOI.s32Width = width;
    rectAOI.s32Height = height;
    is_AOI( m_hCam, IS_AOI_IMAGE_SET_AOI, (void*)&rectAOI, sizeof(rectAOI));
# endif


    return 0;
}




# ifdef AVI_OK
int start_ueye_avi(void)
{
  int  ret, i;
  static int quality = 80;
  double fr;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
      return win_printf_OK("This routine record an AVI movie");

  if (m_hCam == IS_INVALID_HIDS) 	return D_O_K;

  ret = isavi_InitAVI(&nAviID, m_hCam);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  ret = isavi_SetImageSize (nAviID, IS_AVI_CM_Y8, sInfo.nMaxWidth, sInfo.nMaxHeight, 0, 0, 0); // sInfo.nMaxWidth
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  i = win_scanf("Jpeg quality(1->poor 100->excellent) %d",&quality);
  if (i == WIN_CANCEL) return D_O_K;

  ret = isavi_SetImageQuality (nAviID, quality);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  ret = isavi_OpenAVI(nAviID,"c:\\test.avi");
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);


  is_SetFrameRate(m_hCam,IS_GET_FRAMERATE,&fr);


  ret = isavi_SetFrameRate (nAviID, fr);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  ret = isavi_StartAVI (nAviID);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  rec_AVI = 1;

  return D_O_K;
}


int stop_ueye_avi(void)
{
  int ret;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
      return win_printf_OK("This routine stop the recording of an AVI movie");

  rec_AVI = 0;

  ret = isavi_StopAVI (nAviID);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  ret = isavi_CloseAVI (nAviID);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);

  ret = isavi_ExitAVI (nAviID);
  if (ret != IS_AVI_NO_ERR)    return win_printf_OK("Error initiating AVI %d",ret);
  nAviID = -1;
  return D_O_K;
}

# endif  // avi



int average_partial_image_in_ds(O_i *oi,int id, unsigned long long t, unsigned long long dt, void*ptr)
{
  register int i, j;
  int x0, x1, y0, y1, itmp, val;
  float tmp;
  union pix *ps;
  d_s *ds = NULL;

  (void)t;
  (void)dt;
  (void)id;
  if (oi == NULL || ptr == NULL) return 1;
  ds = (d_s*)ptr;
  my_set_window_title("averaging %d of %d",ds->nx, ds->mx);
  if (ds->nx >= ds->mx || ds->ny >= ds->my) return 0; // no averaging
  if (ds->nx == ds->user_ispare[4])
    {
      //read_Due_param("S=", &val);
      val=0;
      set_ds_treatement(ds, "Modulation started at %d",val);
    }
  x0 = ds->user_ispare[0] - ds->user_ispare[2]/2;
  x0 = (x0 < 0) ? 0 : x0;
  x1 = x0 + ds->user_ispare[2];
  x1 = (x1 > oi->im.nx) ? oi->im.nx : x1;
  y0 = ds->user_ispare[1] - ds->user_ispare[3]/2;
  y0 = (y0 < 0) ? 0 : y0;
  y1 = y0 + ds->user_ispare[3];
  y1 = (y1 > oi->im.ny) ? oi->im.ny : y1;
  ps = oi->im.pixel;
  if (oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      itmp += ps[i].ch[j];
	    }
	  tmp += (float)itmp;
	}
    }
  if (oi->im.data_type == IS_INT_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      itmp += ps[i].in[j];
	    }
	  tmp += (float)itmp;
	}
    }
  if (oi->im.data_type == IS_UINT_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      itmp += ps[i].ui[j];
	    }
	  tmp += (float)itmp;
	}
    }
  itmp = (y1 - y0)*(x1-x0);
  if (itmp > 0) tmp /= itmp;
  ds->yd[ds->nx] = tmp;
  ds->xd[ds->nx] = ds->nx;//id;
  ds->nx = ds->ny = ds->nx + 1;
  return 0;
}

int copy_partial_image_in_movie(O_i *oi,int id, unsigned long long t, unsigned long long dt, void*ptr)
{
  register int i, j, im;
  int x0, x1, y0, y1;//, itmp, , val, error
  static float zm = 0;
  unsigned long dt0;
  union pix *ps, *pd;
  O_i *oim = NULL;
  d_s *ds = NULL;
  O_p *op = NULL;
  static float dttmp = 0;
  static int idt = 0;


  (void)t;
  (void)dt;
  (void)id;
  if (oi == NULL || ptr == NULL) return 1;
  oim = (O_i*)ptr;
  op = find_oi_cur_op(oim);
  if (op != NULL) ds = op->dat[0];


  
  if (oim->im.n_f >= oim->im.m_f) return 0; // no averaging
  snprintf(xvueye_ti_message,64,"%d of %d zm = %g dt(copy) %g ms"
		      ,oim->im.c_f, oim->im.m_f,zm, (idt) ?dttmp/idt: dttmp);  
  if (oim->im.c_f == 0)
    {
      idt = 0;
      dttmp = 0;
    }
  dt0 = my_uclock();
  x0 = (int)(0.5 + oim->im.src_parameter[0]) - (int)(oim->im.src_parameter[2]/2);
  x0 = (x0 < 0) ? 0 : x0;
  x1 = x0 + (int)(0.5 + oim->im.src_parameter[2]);
  x1 = (x1 > oi->im.nx) ? oi->im.nx : x1;
  y0 = (int)(0.5 + oim->im.src_parameter[1]) - (int)(oim->im.src_parameter[3]/2);
  y0 = (y0 < 0) ? 0 : y0;
  y1 = y0 + (int)(0.5 + oim->im.src_parameter[3]);
  y1 = (y1 > oi->im.ny) ? oi->im.ny : y1;
  ps = oi->im.pixel;

  im = i = oim->im.n_f;
  oim->im.n_f = (oim->im.n_f < oim->im.m_f) ? oim->im.n_f + 1 : oim->im.m_f;
  switch_frame(oim,i);
  pd = oim->im.pixel;

  if (oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = y0; i < y1; i++)
	{
	  for (j = x0; j < x1; j++)
	      pd[i-y0].ch[j-x0] = ps[i].ch[j];
	}
    }
  if (oi->im.data_type == IS_RGB_PICTURE)
    {
      for (i = y0; i < y1; i++)
	{
	  for (j = x0; j < x1; j++)
	      pd[i-y0].rgb[j-x0] = ps[i].rgb[j];
	}
    }
  if (oi->im.data_type == IS_RGBA_PICTURE)
    {
      for (i = y0; i < y1; i++)
	{
	  for (j = x0; j < x1; j++)
	      pd[i-y0].rgba[j-x0] = ps[i].rgba[j];
	}
    }
  if (oi->im.data_type == IS_RGB16_PICTURE)
    {
      for (i = y0; i < y1; i++)
	{
	  for (j = x0; j < x1; j++)
	      pd[i-y0].rgb16[j-x0] = ps[i].rgb16[j];
	}
    }
  if (oi->im.data_type == IS_RGBA16_PICTURE)
    {
      for (i = y0; i < y1; i++)
	{
	  for (j = x0; j < x1; j++)
	      pd[i-y0].rgba16[j-x0] = ps[i].rgba16[j];
	}
    }
  if (oi->im.data_type == IS_INT_IMAGE)
    {
      for (i = y0; i < y1; i++)
	{
	  for (j = x0; j < x1; j++)
	      pd[i-y0].in[j-x0] = ps[i].in[j];
	}
    }
  if (oi->im.data_type == IS_UINT_IMAGE)
    {
      for (i = y0; i < y1; i++)
	{
	  for (j = x0; j < x1; j++)
	      pd[i-y0].ui[j-x0] = ps[i].ui[j];
	}
    }
  dt0 = my_uclock() - dt0;
  dttmp += (float)(1000*dt0)/get_my_uclocks_per_sec();
  idt++;
  if ((ds != NULL)  && (grab_track_image_z_value != NULL))
    {
      
      if (im%5 == 0)
	{
	  zm = grab_track_image_z_value();
	}
      if (im < ds->nx)
	{
	  ds->xd[im] = im;
	  ds->yd[im] = zm;
	}
	
    }
  return 0;
}


int average_partial_image_in_oi_and_ds(O_i *oi,int id, unsigned long long t, unsigned long long dt, void*ptr)
{
  register int i, j;
  int x0, x1, y0, y1, itmp, val, error;
  float tmp;
  union pix *ps, *pd;
  d_s *ds = NULL, *ds2 = NULL;
  O_p *op = NULL;
  O_i *oim = NULL;
  static int start = 0;


  (void)t;
  (void)dt;
  (void)id;
  if (oi == NULL || ptr == NULL) return 1;
  oim = (O_i*)ptr;
  op = find_oi_cur_op(oim);
  ds = op->dat[0];
  ds2 = (op->n_dat > 1) ? op->dat[1] : NULL;
  my_set_window_title("averaging %d of %d",ds->nx, ds->mx);
  if (ds->nx >= ds->mx || ds->ny >= ds->my) return 0; // no averaging
  if (ds->nx == ds->user_ispare[4])
    {
      //read_Due_param("S=", &val);
      val=0;
      set_ds_treatement(ds, "Modulation started at %d",val);
      start = 0;
    }
  x0 = ds->user_ispare[0] - ds->user_ispare[2]/2;
  x0 = (x0 < 0) ? 0 : x0;
  x1 = x0 + ds->user_ispare[2];
  x1 = (x1 > oi->im.nx) ? oi->im.nx : x1;
  y0 = ds->user_ispare[1] - ds->user_ispare[3]/2;
  y0 = (y0 < 0) ? 0 : y0;
  y1 = y0 + ds->user_ispare[3];
  y1 = (y1 > oi->im.ny) ? oi->im.ny : y1;
  ps = oi->im.pixel;
  switch_frame(oim,ds->nx);
  pd = oim->im.pixel;
  if (oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      pd[i-y0].ch[j-x0] = ps[i].ch[j];
	      itmp += ps[i].ch[j];
	    }
	  tmp += (float)itmp;
	}
    }
  if (oi->im.data_type == IS_INT_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      pd[i-y0].in[j-x0] = ps[i].in[j];
	      itmp += ps[i].in[j];
	    }
	  tmp += (float)itmp;
	}
    }
  if (oi->im.data_type == IS_UINT_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      pd[i-y0].ui[j-x0] = ps[i].ui[j];
	      itmp += ps[i].ui[j];
	    }
	  tmp += (float)itmp;
	}
    }
  itmp = (y1 - y0)*(x1-x0);
  if (itmp > 0) tmp /= itmp;
  ds->yd[ds->nx] = tmp;
  ds->xd[ds->nx] = ds->nx;//id;
  i = read_GPIO_1_input(&error);
  ds2->yd[ds2->nx] = i;
  if (error) ds2->yd[ds2->nx] = -1;
  else if (start == 0 && i == 1)
    {
      start = 1;
      set_oi_source(oim, "%s\nFirst signal modulation at %d",ds->source,ds->nx);
    }
  ds->nx = ds->ny = ds->nx + 1;
  ds2->xd[ds2->nx] = ds2->nx;
  ds2->nx = ds2->ny = ds2->nx + 1;
  return 0;
}



int average_partial_image_in_oi_in_time_and_ds(O_i *oi,int id, unsigned long long t, unsigned long long dt, void*ptr)
{
  register int i, j;
  int x0, x1, y0, y1, itmp, val, error, nfi, navg;
  float tmp;
  union pix *ps, *pd;
  d_s *ds = NULL, *ds2 = NULL;
  O_p *op = NULL;
  O_i *oim = NULL;
  static int start = 0;


  (void)t;
  (void)dt;
  (void)id;
  if (oi == NULL || ptr == NULL) return 1;
  oim = (O_i*)ptr;
  op = find_oi_cur_op(oim);
  ds = op->dat[0];
  ds2 = (op->n_dat > 1) ? op->dat[1] : NULL;
  my_set_window_title("averaging %d of %d",ds->nx, ds->mx);

  nfi = abs(oim->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  navg = (int)(ds->nx/nfi);
  if (ds->nx >= ds->mx || ds->ny >= ds->my) return 0; // no averaging
  if (ds->nx == ds->user_ispare[4])
    {
      //read_Due_param("S=", &val);
      val=0;
      set_ds_treatement(ds, "Modulation started at %d",val);
      start = 0;
    }
  x0 = ds->user_ispare[0] - ds->user_ispare[2]/2;
  x0 = (x0 < 0) ? 0 : x0;
  x1 = x0 + ds->user_ispare[2];
  x1 = (x1 > oi->im.nx) ? oi->im.nx : x1;
  y0 = ds->user_ispare[1] - ds->user_ispare[3]/2;
  y0 = (y0 < 0) ? 0 : y0;
  y1 = y0 + ds->user_ispare[3];
  y1 = (y1 > oi->im.ny) ? oi->im.ny : y1;
  ps = oi->im.pixel;
  switch_frame(oim,(int)(ds->nx/navg));
  pd = oim->im.pixel;
  if (oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      pd[i-y0].ui[j-x0] += ps[i].ch[j];
	      itmp += ps[i].ch[j];
	    }
	  tmp += (float)itmp;
	}
    }
  if (oi->im.data_type == IS_INT_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      pd[i-y0].in[j-x0] += ps[i].in[j];
	      itmp += ps[i].in[j];
	    }
	  tmp += (float)itmp;
	}
    }
  if (oi->im.data_type == IS_UINT_IMAGE)
    {
      for (i = y0, tmp = 0; i < y1; i++)
	{
	  for (j = x0, itmp = 0; j < x1; j++)
	    {
	      pd[i-y0].ui[j-x0] += ps[i].ui[j];
	      itmp += ps[i].ui[j];
	    }
	  tmp += (float)itmp;
	}
    }
  itmp = (y1 - y0)*(x1-x0);
  if (itmp > 0) tmp /= itmp;
  ds->yd[ds->nx] = tmp;
  ds->xd[ds->nx] = ds->nx;//id;
  i = read_GPIO_1_input(&error);
  ds2->yd[ds2->nx] = i;
  if (error) ds2->yd[ds2->nx] = -1;
  else if (start == 0 && i == 1)
    {
      start = 1;
      set_oi_source(oim, "%s\nFirst signal modulation at %d",ds->source,ds->nx);
    }
  ds->nx = ds->ny = ds->nx + 1;
  ds2->xd[ds2->nx] = ds2->nx;
  ds2->nx = ds2->ny = ds2->nx + 1;
  return 0;
}

int do_read_gpio(void)
{
  int i, error = 0;

    if (updating_menu_state != 0)
      {
	if (m_hCam == 0)  active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }  


  i = read_GPIO_1_input(&error);
  if (error) win_printf_OK("error reading GPIO 1");
  return win_printf_OK("GPIO 1 state at %d",i);
}


int grab_im_avg(void)
{
  int i;
  imreg *imr;
  O_i *ois, *oim = NULL;
  O_p *op;
  d_s *ds, *ds2 = NULL;
  char *s = NULL;
  static int xc = 512, yc = 256, w = 128, h = 128, nf = 200, start = 10;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
      return win_printf_OK("This routine start the averaging of an image area");


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return win_printf_OK("cannot find image region");


  i = win_scanf("Realtime averaging of image area;\nDefine area center xc %6d yc %6d\n"
		"Width %6d Height %6d\nDuration in frame %6d\n"
		"Start modulation after %4d frames"
                ,&xc, &yc, &w, &h, &nf, &start);

  if (i == WIN_CANCEL) return 0;


  oim = create_and_attach_movie_to_imr(imr, w, h, ois->im.data_type, nf);
  if (oim == NULL) return win_printf_OK("cannot create movie of modulation");

  op = create_and_attach_op_to_oi(oim, nf, nf, 0, 0);
  //op = create_and_attach_op_to_oi(ois, nf, nf, 0, 0);
  if (op == NULL) return win_printf_OK("cannot create plot");
  ds = op->dat[0];
  ds->nx = ds->ny = 0;
  if ((ds2 = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create second ds");
  ds2->nx = ds2->ny = 0;
  //s = Get_Due_cycle();

  set_ds_source(ds, "Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s", xc,yc,h,w,s);
  set_ds_source(ds2, "GPIO 1 synchro for Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s", xc,yc,h,w,s);
  if (s != NULL) free(s);
  ds->user_ispare[0] = xc;
  ds->user_ispare[1] = yc;
  ds->user_ispare[2] = w;
  ds->user_ispare[3] = h;
  ds->user_ispare[4] = start;
  data_ptr = (void*)oim; // ds;
  track_image = average_partial_image_in_oi_and_ds;//average_partial_image_in_ds;

//for  (; (volatile)(ds->nx) < ds->mx; ) sleep(100);
//track_image = NULL;
  find_zmin_zmax(oim);
  //  we llok for the start of the first period
  for (i = 0; i < ds2->nx && ds2->yd[i] < 1; i++);
  int i0 = i;
  if (i < ds2->nx)
    {
       for (; i < ds2->nx && ds2->yd[i] >0; i++);
       for (; i < ds2->nx && ds2->yd[i] <1; i++);
       start = i;


       set_ds_source(ds, "Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s;"
                     " First point of per 0 = %d, first point of per 1 = %d", xc,yc,h,w,s, i0, i);
       set_ds_source(ds2, "GPIO 1 synchro for Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s"
                     " First point of per 0 = %d, first point of per 1 = %d", xc,yc,h,w,s, i0, i);

       set_oi_source(oim, "%s\nFirst point of per 0 = %d, first point of per 1 = %d",oim->im.source, i0, i);

    }


  ois->need_to_refresh |= ALL_NEED_REFRESH;
  return refresh_image(imr, imr->n_oi - 1);
 }


int grab_im_avg_in_time(void)
{
  int i, type;
  imreg *imr;
  O_i *ois, *oim = NULL;
  O_p *op;
  d_s *ds, *ds2 = NULL;
  char *s = NULL;
  static int xc = 512, yc = 256, w = 128, h = 128, nf = 200, nv = 16, start = 10;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
      return win_printf_OK("This routine start the averaging of an image area");


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return win_printf_OK("cannot find image region");


  i = win_scanf("Realtime averaging of image area;\n"
                "Define area center xc %6d yc %6d\n"
		"Width %6d Height %6d\n"
                "Nb of video frames averaged in 1 frame of movie Nv=%5d\n"
                "Nb of frame in the movie Nf=%6d\n"
                "Duration will be Nf*Nv\n"
		"Start modulation after %4d frames"
                ,&xc, &yc, &w, &h, &nv, &nf, &start);

  if (i == WIN_CANCEL) return 0;

  type = (ois->im.data_type == IS_CHAR_IMAGE) ? IS_UINT_IMAGE : ois->im.data_type;
  oim = create_and_attach_movie_to_imr(imr, w, h, type, nf);
  if (oim == NULL) return win_printf_OK("cannot create movie of modulation");

  op = create_and_attach_op_to_oi(oim, nv*nf, nv*nf, 0, 0);
  //op = create_and_attach_op_to_oi(ois, nf, nf, 0, 0);
  if (op == NULL) return win_printf_OK("cannot create plot");
  ds = op->dat[0];
  ds->nx = ds->ny = 0;
  if ((ds2 = create_and_attach_one_ds(op, nv*nf, nv*nf, 0)) == NULL)
    return win_printf_OK("cannot create second ds");
  ds2->nx = ds2->ny = 0;
  //s = Get_Due_cycle();
  set_ds_source(ds, "Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s", xc,yc,h,w,s);
  set_ds_source(ds2, "GPIO 1 synchro for Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s", xc,yc,h,w,s);
  if (s != NULL) free(s);
  ds->user_ispare[0] = xc;
  ds->user_ispare[1] = yc;
  ds->user_ispare[2] = w;
  ds->user_ispare[3] = h;
  ds->user_ispare[4] = start;
  data_ptr = (void*)oim; // ds;
  track_image = average_partial_image_in_oi_in_time_and_ds;//average_partial_image_in_ds;

//for  (; (volatile)(ds->nx) < ds->mx; ) sleep(100);
//track_image = NULL;
  find_zmin_zmax(oim);
  //  we llok for the start of the first period
  for (i = 0; i < ds2->nx && ds2->yd[i] < 1; i++);
  int i0 = i;
  if (i < ds2->nx)
    {
       for (; i < ds2->nx && ds2->yd[i] >0; i++);
       for (; i < ds2->nx && ds2->yd[i] <1; i++);
       start = i;


       set_ds_source(ds, "Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s;"
                     " First point of per 0 = %d, first point of per 1 = %d", xc,yc,h,w,s, i0, i);
       set_ds_source(ds2, "GPIO 1 synchro for Data taken at xc = %d, yc = %d, h = %d w = %d cyle:\n %s"
                     " First point of per 0 = %d, first point of per 1 = %d", xc,yc,h,w,s, i0, i);

       set_oi_source(oim, "%s\nFirst point of per 0 = %d, first point of per 1 = %d",oim->im.source, i0, i);

    }


  ois->need_to_refresh |= ALL_NEED_REFRESH;
  return refresh_image(imr, imr->n_oi - 1);
 }


int average_image_profile_in_oi(O_i *oi,int id, unsigned long long t, unsigned long long dt, void*ptr)
{
  register int i, j;
  int x0, x1, y0, y1, li;
  O_i *oin;
  O_p *op;
  d_s *ds;
  union pix *ps, *pd;

  (void)t;
  (void)dt;
  if (oi == NULL || ptr == NULL) return 1;
  oin = (O_i*)ptr;
  op = (oin->o_p != NULL) ? oin->o_p[0] : NULL;
  ds = (op != NULL) ? op->dat[0] : NULL;
  //my_set_window_title("averaging %d of %d",oin->im.user_ispare[0][2], oin->im.ny);
  if (oin->im.user_ispare[0][2] >= oin->im.ny)
    {
      my_set_window_title("averaging %d of %d",oin->im.user_ispare[0][2], oin->im.ny);
      return 0; // no averaging
    }
  li = oin->im.user_ispare[0][2];
  if (li == 0) oin->im.user_ispare[0][3] = (int)my_uclock();
  if (ds != NULL && li < ds->nx)
    {
      ds->xd[li] = id;
      ds->yd[li] = (float)(my_uclock() - oin->im.user_ispare[0][3])/(float)get_my_uclocks_per_sec();
    }
  x0 = 0;
  x1 = oi->im.nx;
  y0 = oin->im.user_ispare[0][0] - oin->im.user_ispare[0][1]/2;
  y0 = (y0 < 0) ? 0 : y0;
  y1 = y0 + oin->im.user_ispare[0][1];
  y1 = (y1 > oi->im.ny) ? oi->im.ny : y1;
  ps = oi->im.pixel;
  pd = oin->im.pixel;

  if (oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = y0; i < y1; i++)
	{
	  for (j = x0; j < x1; j++)
	    {
	      pd[li].ui[j] += ps[i].ch[j];
	    }
	}
    }
  if (oi->im.data_type == IS_INT_IMAGE)
    {
      for (i = y0; i < y1; i++)
	{
	  for (j = x0; j < x1; j++)
	    {
	      pd[li].ui[j] += ps[i].in[j];
	    }
	}
    }
  if (oi->im.data_type == IS_UINT_IMAGE)
    {
      for (i = y0; i < y1; i++)
	{
	  for (j = x0; j < x1; j++)
	    {
	      pd[li].ui[j] += ps[i].ui[j];
	    }
	}
    }
  oin->im.user_ispare[0][2]++;
  return 0;
}

int oi_idle_profile_action(struct  one_image *oi, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  //BITMAP *imb;
  //unsigned long t0 = 0;
  static int last_disp = 0;
  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

  if (oi->im.user_ispare[0][2] <= last_disp) return 0;
  find_zmin_zmax(oi);

  oi->need_to_refresh |= ALL_NEED_REFRESH;
  return refresh_image(imr, UNCHANGED);
}



int grab_prof_avg(void)
{
  int i;
  imreg *imr;
  O_i *ois, *oin;

  static int yc = 64, h = 32, nf = 1024;

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
      return win_printf_OK("This routine start the averaging of an image area");


  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return win_printf_OK("cannot find image region");


  i = win_scanf("Realtime averaging of image profile;\nDefine center of averaging yc %6d\n"
		"Height %6d\nDuration in frame %6d\n"
		,&yc, &h, &nf);

  if (i == WIN_CANCEL) return 0;

  oin = create_and_attach_oi_to_imr(imr, ois->im.nx,nf, IS_UINT_IMAGE);
  if (oin == NULL)	  return win_printf_OK("Can't create dest image");

  create_and_attach_op_to_oi(oin, nf, nf, 0, 0);

  oin->im.user_ispare[0][0] = yc;
  oin->im.user_ispare[0][1] = h;
  oin->im.user_ispare[0][2] = 0;
  data_ptr = (void*)oin;
  track_image = average_image_profile_in_oi;

//for  (; (volatile)(ds->nx) < ds->mx; ) sleep(100);
//track_image = NULL;
  oin->need_to_refresh |= ALL_NEED_REFRESH;
  oin->oi_idle_action = oi_idle_profile_action;
  return refresh_image(imr, imr->n_oi - 1);
 }

int oiuEye_idle_action(struct  one_image *oi, DIALOG *d)
{  // we put in this routine all the screen display stuff
  // we put in this routine all the screen display stuff
  imreg *imr;
  BITMAP *imb;
  unsigned long t0 = 0;

  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

  t0 = my_uclock();
  if ((IS_DATA_IN_USE(oi) == 0) && (oi->need_to_refresh & BITMAP_NEED_REFRESH))
    {
      display_image_stuff_16M(imr,d);
      oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
    }



  imb = (BITMAP*)oi->bmp.stuff;


  if (oi->need_to_refresh & INTERNAL_BITMAP_NEED_REFRESH)
    {
      SET_BITMAP_IN_USE(oi);
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off //+ d->x
	   , imr->y_off - imb->h + d->y, imb->w, imb->h);
      release_bitmap(screen);
      oi->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
      t0 = my_uclock() - t0;
      SET_BITMAP_NO_MORE_IN_USE(oi);
    }

  return 0;
}




MENU *xvuEye_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"uEye init", do_xvuEye_init,NULL,0,NULL);
	add_item_to_menu(mn,"uEye init XVIN", do_xvuEye_init_xvin,NULL,0,NULL);
	add_item_to_menu(mn,"uEye movie init", do_xvuEye_init_rolling_buffer_movie_xvin,NULL,0,NULL);
	add_item_to_menu(mn,"chg Freq", do_set_camera_freq,NULL,0,NULL);
	add_item_to_menu(mn,"chg exposure", do_set_camera_exposure,NULL,0,NULL);

	add_item_to_menu(mn,"chg gain", do_set_camera_gain,NULL,0,NULL);
	add_item_to_menu(mn,"chg binning", do_set_camera_binning,NULL,0,NULL);
	add_item_to_menu(mn,"chg black level", adjust_black_level,NULL,0,NULL);




	add_item_to_menu(mn,"stop uEye movie", do_stop_camera,NULL,0,NULL);
	add_item_to_menu(mn,"freeze uEye movie", do_freeze_camera,NULL,0,NULL);
	add_item_to_menu(mn,"live uEye movie", do_live_camera,NULL,0,NULL);
	add_item_to_menu(mn,"Adjust AOI origin", adj_AOI_position,NULL,0,NULL);
# ifdef AVI_OK
	add_item_to_menu(mn,"start AVI movie", start_ueye_avi,NULL,0,NULL);
	add_item_to_menu(mn,"stop AVI movie", stop_ueye_avi,NULL,0,NULL);
# endif

	add_item_to_menu(mn,"Average partial image", grab_im_avg,NULL,0,NULL);
	add_item_to_menu(mn,"Average partial image in space and time", grab_im_avg_in_time,NULL,0,NULL);

	add_item_to_menu(mn,"Average profile vs time", grab_prof_avg,NULL,0,NULL);
	add_item_to_menu(mn,"Read GPIO 1",do_read_gpio ,NULL,0,NULL);



	return mn;
}

int	xvuEye_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_image_treat_menu_item ( "xvuEye", NULL, xvuEye_image_menu(), 0, NULL);
  //Due_serial_main(argc, argv);
  return D_O_K;
}

int	xvuEye_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(image_treat_menu, "xvuEye", NULL, NULL);
  return D_O_K;
}
#endif
