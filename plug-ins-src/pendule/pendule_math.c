#ifndef _PENDULE_MATH_C_
#define _PENDULE_MATH_C_
// functions to perform mathematical operations on data
#include "allegro.h"
#include "xvin.h"
// include "xv_tools_lib.h"

#include <gsl/gsl_errno.h> // for error code returning
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_statistics.h>
// #include <gsl/gsl_statistics_float.h>
#include <fftw3.h>


/* If you include other plug-ins header do it here*/ 
#include "../inout/inout.h"
#include "fft_filter_lib.h"
/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "pendule.h"
#include "pendule_math.h"


/* function pendule_compute_Gibbs_entropy_inc */
/* computes increment of trajectory-dependent entropy between time i and time i+1 */
/* 2007/10/18 */
/* the quantity computed here is an entropy (an increment of entropy) */
/* it is not an entropy production (you have to divide by dt for that */
/*                                                                      */
/* second order version. (x,v) is the trajectory in phase space */
int pendule_compute_Gibbs_entropy_inc(double *x, double *v, int ny, gsl_histogram *hist_x, gsl_histogram *hist_v, double *dS)
{	double  p, p2; // proba      
	register int i, n_errors=0, n_zeros=0;
    	unsigned int jx, jv;
	
	// histograms must be normalized.
	// following function in plug'in "hist" does the job:
    	// int normalize_histogram_Y(gsl_histogram *hist);
	// invoke it before calling the present function
    
	// first element:
    	if ( (gsl_histogram_find(hist_x, x[0], &jx)==GSL_SUCCESS) 
          && (gsl_histogram_find(hist_v, v[0], &jv)==GSL_SUCCESS) ) // then (x,v)[i] was found at position (jx,jv)
	    {     p  = gsl_histogram_get( hist_x, jx);
	          p *= gsl_histogram_get( hist_v, jv);
        }
    	else 
        {     p =(double)0.0;
		      n_errors++; 
        }
        
	// then increments:
        for (i=1; i<ny; i++)
    	{	p2 = p; // probability of preceeding event
    
        	if ( (gsl_histogram_find(hist_x, x[i], &jx)==GSL_SUCCESS) // then x[i] was found at position jx
	          && (gsl_histogram_find(hist_v, v[i], &jv)==GSL_SUCCESS) )
            {	p  = gsl_histogram_get( hist_x, jx);
                p *= gsl_histogram_get( hist_v, jv);
            }
        	else 
            { 	p =(double)0.0;
			    n_errors++;  
		    }
        
        	if ( (p*p2)!= 0)
        	{	dS[i-1] = (double)k_b*(log(p2)-log(p));
		    }
        	else 	
		    { 	dS[i-1] = (double)0.0; 
			    n_zeros++;   
		    }
	    }
	if (n_errors>0) win_printf("%d data points were out of the pdf.\n"
			"Try to enlarge the range.", n_errors);
	if (n_zeros>0)  win_printf("%d points with zero probability.\n"
			"Try to reduce the number of bins.", n_zeros);

	return(0);	
}// pendule_compute_Gibbs_entropy_inc



// x and v have ny points
// Q_Gibbs will have ny-1 usable pts, but ny points allocated
int pendule_compute_Gibbs_heat_power(double *x, double *v, int ny, 
        gsl_histogram *hist_x, gsl_histogram *hist_v, double dt, double *Q_Gibbs)
{   register int i;
                                          
    pendule_compute_Gibbs_entropy_inc(x, v, ny, hist_x, hist_v, Q_Gibbs);
    for (i=0; i<ny; i++) Q_Gibbs[i] *= (double)Temperature;    
	
	/* division by dt is to have an entropy production  */
	/* multiplication by temperature then gives a power */
	
	/* if we don't divide by dt here, we have a heat    */
    
    return(0);
}



#endif	
