#ifndef _PENDULE_ENERGIES_H_
#define _PENDULE_ENERGIES_H_

PXV_FUNC(int, do_pendule_compute_kinetic_and_potential_energy,			(void));
PXV_FUNC(int, do_pendule_compute_heat_power,							(void));
PXV_FUNC(int, do_pendule_compute_work_power,							(void));
PXV_FUNC(int, do_pendule_compute_heat_Gibbs_power,						(void));
PXV_FUNC(int, do_pendule_compute_Q_exc_power,						(void));

#endif
