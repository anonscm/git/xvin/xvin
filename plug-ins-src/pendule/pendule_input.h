#ifndef _PENDULE_INPUT_H_
#define _PENDULE_INPUT_H_

// following functions are defined in 'pendule_math.c':
PXV_FUNC(int , 	compute_phase,					(float *phase,float *x, float *y, int nx, float x_offset,
												float y_offset,float lambda, float indice, int bool_radian, float conv_rad));
PXV_FUNC(int,	ask_for_parameters_of_pendule,	(float *lambda, float *indice, int *bool_radian,float *conv_radian,
												int *do_filter_HP, float *cutoff_HP_f,float *width_HP_f,
												int *bool_remove_bounds_HP, int *do_filter_LP, float *cutoff_LP_f,float *width_LP_f,
												int *bool_remove_bounds_LP, int *bool_updat_current_filter	));
PXV_FUNC(int, 	filter_data_LP,					(float *phase, int nx,	float cutoff_LP_f,
												float width_LP_f, float f_acq, int bool_remove_bounds_LP,
												int bool_update_current_filter));
PXV_FUNC(int, 	filter_data_HP,					(float *phase, int nx,	float cutoff_HP_f,
												float width_HP_f, float f_acq, int bool_remove_bounds_HP,
												int bool_update_current_filter));
PXV_FUNC(int, 	compute_mean_radius,			(float *norm, float *x, float *y, int nx, float x_offset, float y_offset));

#endif
