#ifndef _PENDULE_C_
#define _PENDULE_C_

#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"

#include <gsl/gsl_histogram.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_statistics.h>
// #include <gsl/gsl_statistics_float.h>
#include <fftw3.h>


/* If you include other plug-ins header do it here*/ 
#include "../inout/inout.h"
#include "../diff/diff.h"
#include "fftw3_lib.h"
#include "fft_filter_lib.h"
/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "pendule.h"
#include "pendule_input.h" // functions to load and pre-treat phase from interferometer
#include "pendule_math.h"  // functions to perform mathematical operations on data
#include "pendule_energies.h" // functions to computes anergies and powers

// offsets de l'ampli photodiode
float pendule_x_offset= 0.;
float pendule_y_offset= 0.;
float lambda=632.8, indice=1., conv_radian=1.25e-7;


/**********************************************************************/
/* build a dataset with a signal that is the sum of sinusoides		*/
/* NG, 21/06/2002											*/
/**********************************************************************/
int do_load_phase_NI4472(void)
{  //Declaration des variables
register int 	i, j;
	O_p 	*op2=NULL, *op1 = NULL;
	float rayon=0.;
static	int	n_X=1, n_Y=2, N_decimate=1;
	int N_channels=2;
	int	    nx, ny, nx_out;
	long	header_length, N_total;
	float	f_acq;
	d_s 	*ds2=NULL, *ds1=NULL;
	pltreg	*pr = NULL;
	float	*x, *y, dt;
	float	*phase;
static char NI_filename[512]="toto";
	char *filename=NULL, *message=NULL;
static int do_filter_HP=0, bool_plot_unfiltered=1, bool_remove_bounds_HP=1;
static int do_filter_LP=0, bool_remove_bounds_LP=1, bool_update_current_filter=1;
static int bool_plot_psd=1, bool_hanning=1;
static int bool_plot_circle=0;
static int bool_radian=0;
static int n_fft=16384, n_overlap=14000;
static float 		cutoff_HP_f=0.1, width_HP_f=0.05;
static float 		cutoff_LP_f=400, width_LP_f=50;
	char 			*channel_string=NULL;
	char	*date_stamp=NULL, *user_header, *channel_cfg;
float F_r;
long current_position;
	

	
	if(updating_menu_state != 0)	return D_O_K;	
//	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine calculates the displacement in nm from X and Y channel\n"
							" and plots it.\n"
							" The phase is unwrapped\n"
							" The signal can be filtered.\n"
							"We can plot the psd of the deplacement. We can also plot the contrast function.");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)		return win_printf_OK("cannot plot region");
	
	
	//Instruction demandee a l'utilisateur...
	i = file_select_ex("Load X and Y data from a NI4472 .bin file", NI_filename, "bin", 512, 0, 0);
	if (i==0) return OFF;
	filename = extract_file_name( filename,      1, NI_filename);
	
	i=read_header_NI4472(NI_filename, &header_length, &N_total, &f_acq);
	if (i!=D_O_K) return(win_printf_OK("error looking at file %s\nthis file may not exist...", NI_filename));

	// following is useless. I comment it out. NG, 2006-05-23
	current_position=get_header_informations_NI4472(NI_filename, &channel_string, &channel_cfg, &N_channels, &f_acq, &F_r,
								&date_stamp, &user_header, &N_total);
	if (current_position<0)		return(win_printf_OK("Error reading file header for %s, aborting.", NI_filename));

	
	message=my_sprintf(message, "{\\color{yellow}\\pt14 File %s, created %s}\n"
    		  "number of channels in file ? %%5d (%s)\n"
     		  "channel with X data %%5d offset to substract %%6f\n"
     		  "channel with Y data %%5d offset to substract %%6f\n"
     		  "decimate every %%6d points\n\n"
     		  "%%b plot unfiltered data\n"
     		  "%%b filter data\n"
     		  "%%b plot phase traj. on circle\n"
     		  "%%b compute PSD (in nm/\\sqrt{Hz})\n", filename, date_stamp, channel_string);
    i=win_scanf(message,
			&N_channels, &n_X, &pendule_x_offset, &n_Y, &pendule_y_offset, &N_decimate,&bool_plot_unfiltered,
			&do_filter_HP, &bool_plot_circle, &bool_plot_psd);
		if (i==CANCEL) return OFF;
	free(message); message=NULL;
	
	
	i=ask_for_parameters_of_pendule(	&lambda, &indice, &bool_radian, &conv_radian, &do_filter_HP, &cutoff_HP_f,
										&width_HP_f, &bool_remove_bounds_HP, &do_filter_LP, &cutoff_LP_f,
										&width_LP_f, &bool_remove_bounds_LP, &bool_update_current_filter);
	if (i==CANCEL) return(OFF);
	
	
	nx = load_NI4472_data(NI_filename, header_length, N_total, N_channels, n_X, N_decimate, &x);				
	ny = load_NI4472_data(NI_filename, header_length, N_total, N_channels, n_Y, N_decimate, &y);
	dt=(float)N_decimate/f_acq;
	f_acq /= N_decimate;
	if (nx!=ny) return(win_printf_OK("error! not the same number of points in X and Y!"));	
	nx_out=nx;
		
	 //Creation des datasets temporels.
	phase=(float*)calloc(nx, sizeof(float));
	compute_phase(phase, x, y, nx, pendule_x_offset, pendule_y_offset, lambda, indice, bool_radian, conv_radian);
	//plot unfiltered data
	
	if (bool_plot_unfiltered==1)
	{	
		if ((op1 = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
			return win_printf_OK("cannot create plot !");
		ds1 = op1->dat[0];
		op1->filename = extract_file_name( op1->filename, 1, NI_filename);
		op1->dir 	  = extract_file_path( op1->dir, 	  1, NI_filename);	
		for (i=0; i<nx ; i++)
		{		ds1->xd[i]	=	(float)i/f_acq;
				ds1->yd[i]	= 	(float)phase[i];
		}
		set_plot_title(op1,"\\stack{{%s}{unwrapped phase}}", op1->filename);
		set_plot_x_title(op1, "time (s)");
		set_ds_source    (ds1, "NI4472 file %s", filename);
		set_ds_treatement(ds1, "unwrapped phase (%s)", (bool_radian==1) ? "en rad" : "en nm");
		set_plot_y_title(op1, "position %s", (bool_radian==1) ? "(rad)" : "(nm)");
	}
		
	if ((do_filter_LP+do_filter_HP)>=1)
	{	if (do_filter_LP==1)	
		{	nx_out=filter_data_LP(	phase, nx,	cutoff_LP_f, width_LP_f, f_acq, bool_remove_bounds_LP, bool_update_current_filter);
			win_printf("there will be %d pts (from %d) in the output", nx_out, nx);
		}
		if (do_filter_HP==1)	
		{	nx_out=filter_data_HP(	phase, nx,	cutoff_HP_f, width_HP_f, f_acq, bool_remove_bounds_HP, bool_update_current_filter);
			win_printf("there will be %d pts (from %d) in the output", nx_out, nx);
		}
		
		if (bool_plot_unfiltered==1)                                                            
		{	if ((ds1 = create_and_attach_one_ds(op1, nx_out, nx_out, 0)) == NULL)                         
													return(win_printf_OK("cannot create dataset"));                              
		}                                                                                       
		else                                                                                    
		{	if ((op1 = create_and_attach_one_plot(pr, nx_out, nx_out, 0)) == NULL)                        
													return win_printf_OK("cannot create plot !");                                
			ds1 = op1->dat[0];                                                                     
			op1->filename = extract_file_name( op1->filename, 1, NI_filename);                     
			op1->dir 	  = extract_file_path( op1->dir, 	  1, NI_filename);	                        
			set_plot_title(op1,"\\stack{{%s}{unwrapped phase}}", op1->filename);                 
			set_plot_x_title(op1, "time (s)");                                                     
			set_ds_source    (ds1, "NI4472 file %s", filename);                                    
			set_ds_treatement(ds1, "unwrapped phase (%s) ", (bool_radian==1) ? "en rad" : "en nm");
			set_plot_y_title(op1, "position %s", (bool_radian==1) ? "(rad)" : "(nm)");           
		}                                                                                       
		//saving the data:
		for (i=0; i<nx_out; i++)
		{	ds1->xd[i] 	=	(float)((i+(nx-nx_out)/2.)/f_acq);
			ds1->yd[i] 	=	(float)phase[i];
		}
		if (do_filter_LP==1) set_ds_treatement(ds1, "Low Pass filtred with cutoff %4.2f Hz and width %4.2f Hz", cutoff_LP_f, width_LP_f);
		if (do_filter_HP==1) set_ds_treatement(ds1, "High Pass filtred with cutoff %4.2f Hz and width %4.2f Hz", cutoff_HP_f, width_HP_f);
		nx=nx_out;
	}
	
	 //Creation du plot contraste
    if (bool_plot_circle==1)
    {	compute_mean_radius(&rayon, x, y, nx, pendule_x_offset, pendule_y_offset);
		if ((op2 = create_and_attach_one_plot(pr, 360, 360, 0)) == NULL)
				return win_printf_OK("cannot create plot for circle!");
		
		ds2 = op2->dat[0];
		
		for (j=0; j<360; j++)
		{	ds2->xd[j] = (float)(rayon*cos((double)j*2.*M_PI/360.)); 
			ds2->yd[j] = (float)(rayon*sin((double)j*2.*M_PI/360.));
		}
		
		set_ds_plot_label(ds2, 1., 1., 1, "\\color{lightgray} mean radius %g V", rayon);
		inherit_from_ds_to_ds(ds2, pr->o_p[pr->cur_op-1]->dat[0]);
		set_ds_line_color(ds2, Darkgray);
		ds2->treatement = my_sprintf(ds2->treatement,"pure circle, radius %g", rayon);
		set_ds_source(ds2, "NI4472 file %s", filename);
		
		ds2 = create_and_attach_one_ds (op2, nx, ny, 0);
		for (i=0; i<nx; i++)
		{	ds2->xd[i] = x[i];
			ds2->yd[i] = y[i];			
		}
		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"angle trajectory");
		set_ds_source(ds2, "NI4472 file %s", filename);
		set_ds_dot_line(ds2);
		
		set_plot_title(op2,   "phase");
		set_plot_x_title(op2, "X displacement (Volts)");
		set_plot_y_title(op2, "Y displacement (Volts)");
	}

	
	//Creation du spectre
    if (bool_plot_psd==1)
    {	message = my_sprintf(message, "dataset has %d points.\n"
			"number of points of fft window ?    %%6d\n"
			"overlap (in points) between windows ? %%6d\n"
			"acquisition frequency (in Hz, for normalization) ?%%9f\n"
			"%%b use Hanning window ?",nx);
		if (win_scanf(message, &n_fft, &n_overlap, &f_acq, &bool_hanning) == CANCEL) 
			return D_O_K;
		if (n_overlap>=n_fft) 
			return win_printf("overlap is too large compared to n\\_fft (%d>=%d)",
				n_overlap, n_fft);

		if ((op2=create_and_attach_one_plot(pr, (n_fft/2)+1, (n_fft/2)+1, 0)) == NULL)
			return win_printf_OK("cannot create PSD plot !");
    	if ((ds2=op2->dat[0]) == NULL)
    		return win_printf_OK("Cannot allocate PSD dataset");
    
    		
   		i=compute_psd_of_float_data( ds1->yd, ds2->yd, ds2->xd, f_acq, nx, n_fft, n_overlap, bool_hanning);
   		for (j=0; j<(n_fft/2)+1; j++) ds2->yd[j]=(float)sqrt(ds2->yd[j]);
   		
   		inherit_from_ds_to_ds(ds2, ds1);
   		ds2->treatement = my_sprintf(ds2->treatement,"PSD of displacement (%s/\\sqrt{Hz})", (bool_radian==1) ? "rad" : "nm");
   		set_ds_source(ds2, "NI4472 file %s", filename);
				
   		set_plot_title(op2,   "%s PSD (%d windows)", filename, i); 
		set_plot_x_title(op2, "f (Hz)");
		set_plot_y_title(op2, "%s/\\sqrt{Hz}", (bool_radian==1) ? "rad" : "nm");
		op2->iopt |= XLOG;
    	op2->iopt |= YLOG;
	}
	free(phase);
	free(x); free(y); // were forgotten!!! NG, 2006-05-23
    return refresh_plot(pr, UNCHANGED);
}





///////////////////////////////////////////////////////////////////////////////
//Theorie fluctuation
//Spectre theorique de fluctuations
int do_plot_theorical_fluctuation_psd(void)
{	int	 	n, i;
	O_p 	*op = NULL;
	d_s 	*ds0, *ds1;
	pltreg	*pr = NULL;
static float lI=2.375e-10, lk1=4.8e-4, lk2 = 2.8e-7, lnu=1.34e-8,lnu2=1.64e-11, lkT=k_b*Temperature;
static char bool_amplitude=1;
	int	bool_f_is_zero=0;
	double	I, k1,k2,nu,nu2, kT;
	double	omega, psd;
	if(updating_menu_state != 0)	return D_O_K;	
	
	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routines plot the theorical function of the fluctuation psd of theta (amplitude, rad/\\sqrt{Hz})"
								"A new dataset is created");
	}
	
	i=win_scanf("{\\pt14\\color{yellow} Parameters I d^2\\theta/dt^2 + \\nu d\\theta/dt + k\\theta = \\eta}\n\n"
			"Torque constant k = k' + i k''\n"
			"k' = %9f\n"
			"k'' = %9f \n"
			"viscosity (\\nu+\\nu 2 \\omega)\\nu = %9f  \\nu 2 = %9f\n"
			"Inertia I = %9f \n"
			"kT = %9f (<\\eta(t)\\eta(t+\\tau)> = 2kT \\delta(\\tau))\n\n"
			"PSD in %R energy (nm^2/Hz) or %r amplitude nm/\\sqrt{Hz}",
			&lk1,&lk2,&lnu,&lnu2,&lI,&lkT, &bool_amplitude);
	if (i==CANCEL) return(D_O_K);
	I=(double)lI; k1=(double)lk1; nu=(double)lnu; k2=(double)lk2; kT = (double)lkT;nu2=(double)lnu2;
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds0) != 3)		return win_printf_OK("cannot plot region");
	n=ds0->nx;
	
	if (ds0->xd[0]< (ds0->xd[2]-ds0->xd[1])/2.) bool_f_is_zero=1;
	
	if ((ds1 = create_and_attach_one_ds(op, n-bool_f_is_zero, n-bool_f_is_zero, 0)) == NULL)	
																return(win_printf_OK("cannot create dataset"));
	for (i=bool_f_is_zero; i<n; i++)
	{	ds1->xd[i-bool_f_is_zero] = ds0->xd[i];
		omega      				  = (double)(2.*M_PI*ds0->xd[i]);
		psd 					  = (double)(4.*kT*(nu+k2/omega+nu2*omega)/((k1-I*omega*omega)*(k1-I*omega*omega)+
											(k2+nu*omega+nu2*omega*omega)*(k2+nu*omega+nu2*omega*omega)));
		if (bool_amplitude==1)	ds1->yd[i-bool_f_is_zero] = (float)sqrt(psd);	
		else				    ds1->yd[i-bool_f_is_zero] = (float)psd;	
	}
	ds1->treatement = my_sprintf(ds1->treatement,"theorical fluctuation psd (amplitude) with I = %g \n k' = %g \n k'' = %g \n nu = %g \n kT = %g", 
						lI,lk1,lk2,lnu,lkT);
	

	return refresh_plot(pr, UNCHANGED);
}
 /* end of function 'do_plot_theorical_fluctuation_psd' */


///////////////////////////////////////////////////////////////////////////////


// Mesure absolue de la valeur de la constante de raideur//
int do_absolute_measurement_of_C_from_NI4472(void)
{   //Declaration des variables...	
	int i,k;
	int index;
	int		*files_index=NULL;		// all the files
	int i_file, n_files;
	int	nx, nx0=0, ny;
	O_p 	*op2=NULL, *op1=NULL;
	pltreg	*pr = NULL;
	d_s  *ds2=NULL,*ds1=NULL, *ds_R=NULL, *ds_theta=NULL,*ds_X=NULL, *ds_Y=NULL, *ds_varX=NULL, *ds_varY=NULL ;
static	char files_ind_string[128]	="0:1:9"; 
static	char file_extension[8]		=".bin";
static	char	root_filename[128]	     = "run03";
	char		s[512];
	long		header_length, N_total;
	char		filename[256];
static	int	N_channels=4, n_X=1, n_Y=2, N_decimate=1;
	float f_acq;
static float kT=k_b*Temperature;
	float mean_theta,var_theta, mean_X, mean_Y, var_X, var_Y;
	float C;
	float	*x, *y, *phase=NULL;
static float 		cutoff_HP_f=0.1, width_HP_f=0.05;
static float 		cutoff_LP_f=400, width_LP_f=50;
static int do_filter_HP=1, bool_remove_bounds_HP=1, bool_update_current_filter=1;
static int do_filter_LP=1, bool_remove_bounds_LP=1;
static int bool_radian=1, bool_stiffness=1;                                                        
static int bool_mean_R=1,bool_mean_theta=0, bool_mean_XY=0,  bool_var_XY=0, bool_contrast=1;                                                                    
	float rayon ;

	
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes from some files the spring constant of each NI4472 files\n"
					"from the mean square fluctuation of the angle theta \\sigma^2=kT/C"
					"a new plot is created.");
		}
	
			
	//Instruction demandee a l'utilisateur...		
	sprintf(s,"{\\color{yellow}\\pt14 Operate on several files}\n"
				"root name of files %%s"
				"range of files %%s"
				"file extension (note: expected files are from NI4472) %%s\n"
				"Number of channels in a file : %%2d\n"
				"channel with X data %%5d offset to substract %%8f\n"
     			"channel with Y data %%5d offset to substract %%8f\n"
     			"decimate every %%6d points\n\n");
    i=win_scanf(s, &root_filename, &files_ind_string, &file_extension, &N_channels,
    			 &n_X, &pendule_x_offset, &n_Y, &pendule_y_offset, &N_decimate);
	if (i==CANCEL) return(OFF);
	
	sprintf(s,"{\\color{yellow}\\pt14 What do you want to plot}\n"
				"Torsion stiffness (C=kT/\\sigma_\\theta^{2}) %%b\n"
				"with kT = %%9f (<\\eta(t)\\eta(t+\\tau)> = 2kT \\delta(\\tau))\n\n"
				"Mean of theta (<\\theta>) %%b\n"
				"Mean of radius (<R>)  %%b\n"
				"Mean of V_X and V_Y (<V_X>, <V_Y>) %%b\n"
				"Variance of V_X and V_Y (\\sqrt{<\\delta V_X^2>}, \\sqrt{<\\delta V_Y^2>}) %%b\n"
				"Plot contrast (<V_Y> vs <V_X>) %%b\n");
    i=win_scanf(s, &bool_stiffness,&kT,  &bool_mean_theta, &bool_mean_R, &bool_mean_XY,
    			 &bool_var_XY, &bool_contrast);
	if (i==CANCEL) return(OFF);
				
	
	i=ask_for_parameters_of_pendule(	&lambda, &indice, &bool_radian, &conv_radian, &do_filter_HP, &cutoff_HP_f,
										&width_HP_f, &bool_remove_bounds_HP, &do_filter_LP, &cutoff_LP_f, &width_LP_f,
										&bool_remove_bounds_LP, &bool_update_current_filter);
	if (i==CANCEL) return(OFF);
	
		
	//Creation des datasets...	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	
	if ((op2 = create_and_attach_one_plot(pr, n_files, n_files, 0))==NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	if (bool_mean_R==1)
	{ 	ds_R 	 = create_and_attach_one_ds(op2, n_files, n_files, 0);
		ds_R->treatement = my_sprintf(ds_R->treatement,"mean of radius");
	}
	if (bool_mean_theta==1) 
	{	ds_theta = create_and_attach_one_ds(op2, n_files, n_files, 0);
		ds_theta->treatement = my_sprintf(ds_theta->treatement,"mean of \\theta");
	}
	if (bool_mean_XY==1) 	
	{	ds_X 	 = create_and_attach_one_ds(op2, n_files, n_files, 0);
		ds_Y 	 = create_and_attach_one_ds(op2, n_files, n_files, 0);
		ds_X->treatement = my_sprintf(ds_X->treatement,"mean of V_X");
		ds_Y->treatement = my_sprintf(ds_Y->treatement,"mean of V_Y");
	}
	if (bool_var_XY==1)		
	{	ds_varX = create_and_attach_one_ds(op2, n_files, n_files, 0);
		ds_varY = create_and_attach_one_ds(op2, n_files, n_files, 0);
		ds_varX->treatement = my_sprintf(ds_varX->treatement,"variance of V_X");
		ds_varY->treatement = my_sprintf(ds_varY->treatement,"variance of V_Y");
	}
	if (bool_contrast==1)	
	{	if ((op1 = create_and_attach_one_plot(pr, 360, 360, 0)) == NULL)
				return win_printf_OK("cannot create plot for circle!");
		ds1 = op1->dat[0];
		ds1->treatement = my_sprintf(ds1->treatement,"Vx vs Vy");
	}

	  	
	//Boucle sur les fichiers
	for (k=0; k<n_files; k++)
	{	i_file=files_index[k];
		sprintf(filename, "%s%d%s", root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s", k+1, n_files, filename); spit(s);
		i=read_header_NI4472(filename, &header_length, &N_total, &f_acq);
		if (i==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", root_filename));
		nx = load_NI4472_data(filename, header_length, N_total, N_channels, n_X, N_decimate, &x);				
		ny = load_NI4472_data(filename, header_length, N_total, N_channels, n_Y, N_decimate, &y);
		if (nx==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
		if (ny==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
	   	if (nx!=ny) return(win_printf_OK("error! not the same number of points in X and Y!"));	
	   	
	   	if (k==0) 
	   	{	 	nx0=nx; 
	   			phase=(float*)calloc(nx0, sizeof(float));
   		}
		if (nx>nx0) return(win_printf_OK("error: file %d has more points than expected", k));
	   	
		compute_phase(phase, x, y, nx, pendule_x_offset, pendule_y_offset,lambda, indice, bool_radian, conv_radian);		
					   			
		if (bool_mean_R==1)
		{	compute_mean_radius(&rayon, x, y, nx, pendule_x_offset, pendule_y_offset);
			ds_R->xd[k]=(float)k;
			ds_R->yd[k]=rayon;
		}  
	   	if (bool_mean_theta==1)
	   	{	mean_theta=gsl_stats_float_mean(phase, 1, nx);
	   		ds_theta->xd[k]=(float)k;	
	   	    ds_theta->yd[k]=mean_theta;
   		}
	   	if (bool_mean_XY+bool_contrast>=1)
	   	{	mean_X=gsl_stats_float_mean(x, 1, nx);
		   	mean_Y=gsl_stats_float_mean(y, 1, nx);
		   	if (bool_mean_XY==1)
		   	{  	ds_X->xd[k]=(float)k;
		   		ds_X->yd[k]=mean_X;
		   		ds_Y->xd[k]=(float)k;
		   		ds_Y->yd[k]=mean_Y;
	   		}
	   		if (bool_contrast==1)
	   		{ds1->xd[k]=mean_X;
	   		ds1->yd[k]=mean_Y;
   			}
		}
		   		  
	   	if (bool_var_XY==1)
	   	{	nx=filter_data_HP(	x, nx,	cutoff_HP_f, width_HP_f, f_acq, bool_remove_bounds_HP, bool_update_current_filter);
	   		nx=filter_data_HP(	y, nx,	cutoff_HP_f, width_HP_f, f_acq, bool_remove_bounds_HP, bool_update_current_filter);
	   		var_X  = gsl_stats_float_sd(x, 1, nx);  
	   		var_Y  = gsl_stats_float_sd(y, 1, nx);
			ds_varX->xd[k]=(float)k;
			ds_varX->yd[k]=var_X;
			ds_varY->xd[k]=(float)k;
			ds_varY->yd[k]=var_Y;
		}
		
		if (bool_stiffness==1)
		{	nx=filter_data_HP(	phase, nx,	cutoff_HP_f, width_HP_f, f_acq, bool_remove_bounds_HP, bool_update_current_filter);		
			var_theta  = gsl_stats_float_sd(phase, 1, nx);
			C=(float)kT/(var_theta*var_theta);
			ds2->yd[k] = (float)C;
			ds2->xd[k] = (float)k;
		}
		
		free(x); free(y);
	}
	
	free(phase);
	
	set_ds_dot_line(ds2);
	set_ds_point_symbol(ds2, "\\pt12\\di");
	set_plot_title(op2, "dispersion raideur C");
	set_plot_x_title(op2, "File number");
	set_plot_y_title(op2, "C (N.m/rad)");
	ds2->treatement = my_sprintf(ds2->treatement,"spring constant calculated by equipartition of energy");
	 
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the mesure absolue de la valeur de la constante de raideur






int do_absolute_measurement_of_C_from_bin_files(void)
{	int i,k;
	int	*files_index=NULL;
	int	n_theta, n_files, index, i_file;
	char		s[512];
static	char files_ind_string[128]	="1:1:20";    
static	char file_extension[8]		=".bin";         
static	char theta_root_filename[128] = "theta";
	char		filename[512];
	float	*theta;
	pltreg	*pr = NULL;
static float 	cutoff_HP_f=0.1, width_HP_f=0.05, kT=k_b*Temperature, f_acq=8192;
	d_s  *ds2=NULL,*ds_theta=NULL;
int bool_mean_theta=1, bool_stiffness=1, bool_filter=1, bool_remove_bounds_HP=1;
	O_p 	*op2=NULL,*op1;
float mean_theta, C, var_theta;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine computes from some files the spring constant of each NI4472 files\n"
					"from the mean square fluctuation of the angle theta \\sigma^2=kT/C"
					"a new plot is created.");
		}
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of file with angle \\theta %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n");
	i=win_scanf(s, &theta_root_filename, &files_ind_string, &file_extension);
	if (i==CANCEL) return(OFF);
	sprintf(s,"{\\color{yellow}\\pt14 What do you want to plot}\n"
				"Mean of theta (<\\theta>) %%b\n"
				"Torsion stiffness (C=kT/\\sigma_\\theta^{2}) %%b\n"
				"with kT = %%9f (<\\eta(t)\\eta(t+\\tau)> = 2kT \\delta(\\tau))\n\n"
				"%%b filter data (High-Pass filter): cutoff: %%8f Hz, width : %%8f Hz (e.g.: cutoff/2)\n"
				"%%b remove first and last points where filter is giving weird results\n"
				"acquisition frequency (Hz) %%9f" );
    i=win_scanf(s, &bool_mean_theta, &bool_stiffness, &kT,&bool_filter,&cutoff_HP_f, &width_HP_f,&bool_remove_bounds_HP,&f_acq);
    if (i==CANCEL) return(OFF);		
					
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	if ((op2 = create_and_attach_one_plot(pr, n_files, n_files, 0))==NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	if (bool_mean_theta==1) 
	{	ds_theta = create_and_attach_one_ds(op2, n_files, n_files, 0);
		ds_theta->treatement = my_sprintf(ds_theta->treatement,"mean of \\theta");
	}
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%d%s", theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);
		
		if (bool_mean_theta==1)
	   	{	mean_theta=gsl_stats_float_mean(theta, 1, n_theta);
	   		ds_theta->xd[k]=(float)k;	
	   	    ds_theta->yd[k]=mean_theta;
   		}
			
		if (bool_stiffness==1)
		{	n_theta=filter_data_HP(theta, n_theta,	cutoff_HP_f, width_HP_f, f_acq, bool_remove_bounds_HP, 0);		
			var_theta  = gsl_stats_float_sd(theta, 1, n_theta);
			C=(float)kT/(var_theta*var_theta);
			ds2->yd[k] = (float)C;
			ds2->xd[k] = (float)k;
		}
		free(theta);
	}  
	free(files_index);                                                                   
	return refresh_plot(pr, UNCHANGED);
}  //do_absolute_measurement_of_from_bin_files

///////////////////////////////////////////////////////////////////////////////






//chargement des fichiers et sauvegarde en theta et M
int do_convert_angle_and_excitation_from_NI4472(void)
{	int	i,k;
	int	index;
	int	*files_index=NULL;		// all the files
	int	i_file, n_files, i_ref=43;
	int	nx, ny, nx_out, n_M, nx_out_tmp;
	pltreg	*pr = NULL;
static	char 	files_ind_string[128]	="0:1:49"; 
static	char 	file_extension[8]	=".bin";
static	char	root_filename[128]	= "run01";
	char	s[2048];
	long	header_length, N_total;
	char	filename[256], filename_excitation_out[256], filename_phase_out[256], filename_dtheta_out[256];
static	int	N_channels=4, n_X=1, n_Y=2, n_excitation=3, N_decimate=1;
	float f_acq;
static	float calibration=-1.148495e-12, offset_M=0, offset_theta=0;
	float	*x, *y, *excitation, *phase, *dtheta, *t, *phase_tmp;
static float 		cutoff_HP_f=0.1, width_HP_f=0.05;
static float 		cutoff_LP_f=400., width_LP_f=100.;
static float 		cutoff_ref=10., width_ref=5.;
static int bool_radian=1, bool_remove_bounds_HP=1,bool_remove_bounds_LP=1, bool_update_current_filter=0, bool_normalize=1 ;
static	int do_filter_HP=1, do_filter_LP=1, compute_dtheta=1;
	float var_ref=1, var;
	int err;
	
	
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
        {	return win_printf_OK("This routine extract from NI4472 files the excitation in Nm/rad,\n"
					"the angle theta in rad. This angle was filtered with a low pass and a high pass. \n"
					"We can save a .dat files with the derivate of theta computed with splines.");
	}
	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)	return win_printf_OK("cannot find region");
	
	//Demande des parametres a l'utilisateurs	
	sprintf(s,"{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of files %%s"
			"range of files %%s"
			"file extension (note: expected files are from NI4472) %%s\n"
			"Number of channels in a file : %%2d\n"
			"channel with X data %%5d with offset to substract %%8f\n "
     			"channel with Y data %%5d with offset to substract %%8f\n"
     			"channel with excitation data %%5d\n"
     			"decimate every %%6d points\n\n"
     			"offset de l'excitation(Nm/rad): %%10f\n"
     			"calibration de l'excitation %%10f\n"

     			"offset de la phase(rad): %%10f\n"
			"do you want de save \\frac{d\\theta}{dt} computed with splines? %%b\n"
			"do you want to normalize \\theta (all files with the same standard deviation) ? %%b");
	i=win_scanf(s, &root_filename, &files_ind_string, &file_extension, &N_channels, &n_X, &pendule_x_offset, &n_Y, &pendule_y_offset, &n_excitation,
    			 &N_decimate, &offset_M, &calibration, &offset_theta, &compute_dtheta, &bool_normalize);
	if (i==CANCEL) return(OFF);
	
	i=ask_for_parameters_of_pendule(&lambda, &indice, &bool_radian, &conv_radian, &do_filter_HP, &cutoff_HP_f,
					&width_HP_f, &bool_remove_bounds_HP, &do_filter_LP, &cutoff_LP_f, &width_LP_f,
					&bool_remove_bounds_LP, &bool_update_current_filter);
	if (i==CANCEL) return(OFF);
	
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf_OK("bad values for files indices !"));
		
	
	if (bool_normalize==1)
	{	i=win_scanf("Reference file number %6d\n"
				"High Pass filter :\n"
				"frequency %9f width %9f", &i_ref, &cutoff_ref, &width_ref);
		if (i==CANCEL) return(D_O_K);
		sprintf(filename, "%s%d%s", root_filename, i_ref, file_extension);
		i=read_header_NI4472(filename, &header_length, &N_total, &f_acq);
		if (i!=D_O_K) return(win_printf_OK("error looking at file %s\nthis file may not exist...", root_filename));

		nx = load_NI4472_data(filename, header_length, N_total, N_channels, n_X, N_decimate, &x);				
		ny = load_NI4472_data(filename, header_length, N_total, N_channels, n_Y, N_decimate, &y);
		phase=(float*)calloc(nx, sizeof(float));
		compute_phase(phase, x, y, nx, pendule_x_offset, pendule_y_offset, lambda, indice, bool_radian, conv_radian);
		nx_out=filter_data_HP(	phase, nx, cutoff_ref, width_ref, f_acq, 1, 0);
		var_ref  = gsl_stats_float_sd(phase, 1, nx_out);
		free(phase); free(x); free(y);
	}
	//boucle sur les fichiers...
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];

		sprintf(filename, "%s%d%s", root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s", k+1, n_files, filename); spit(s);
		i=read_header_NI4472(filename, &header_length, &N_total, &f_acq);
		if (i==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", root_filename));
		f_acq=(float) f_acq/N_decimate;

		nx  = load_NI4472_data(filename, header_length, N_total, N_channels, n_X, 	N_decimate, &x);				
		ny  = load_NI4472_data(filename, header_length, N_total, N_channels, n_Y, 	N_decimate, &y);
		n_M = load_NI4472_data(filename, header_length, N_total, N_channels, n_excitation, N_decimate, &excitation);
		if (nx ==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
		if (ny ==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
		if (n_M==-1) return(win_printf_OK("error looking at file %s\nthis file may not exist...", filename));
	   	if (nx!=ny)  return(win_printf_OK("error! not the same number of points in X and Y!"));	
		if (nx!=n_M) return(win_printf_OK("error! not the same number of points in X and M!"));	

		phase=(float*)calloc(nx, sizeof(float));
		compute_phase(phase, x, y, nx, pendule_x_offset, pendule_y_offset, lambda, indice, bool_radian, conv_radian);
		free(x);
		free(y);
		
		if (bool_normalize==1)
		{ 	phase_tmp=(float*)calloc(nx, sizeof(float));
			memcpy(phase_tmp, phase, nx*sizeof(float));
			
			nx_out_tmp=filter_data_HP(	phase_tmp, nx,	cutoff_ref, width_ref, f_acq, 1, 0);
			var=gsl_stats_float_sd(phase_tmp, 1, nx_out_tmp);
			free(phase_tmp);
			
			for (i=0; i<nx; i++)
			{	phase[i] *= var_ref/var;
			}
		}
		if (do_filter_LP==1)	
		{
		nx_out=filter_data_LP(	phase, nx,	cutoff_LP_f, width_LP_f, f_acq, bool_remove_bounds_LP, bool_update_current_filter);
		for (i=0; i<nx_out ; i++)
		{	excitation[i]	 = (float)(excitation[i+(nx-nx_out)/2]);
		}
		nx=nx_out;
		}
		if (do_filter_HP==1)	
		{
		nx_out=filter_data_HP(	phase, nx,	cutoff_HP_f, width_HP_f, f_acq, bool_remove_bounds_HP, bool_update_current_filter);
		for (i=0; i<nx_out ; i++)
		{	excitation[i]	 = (float)(excitation[i+(nx-nx_out)/2]);
		}
		nx=nx_out;
		}
		
		
		
		for (i=0; i<nx ; i++)
		{	excitation[i]	*= calibration; 
			excitation[i]	+= offset_M;
		}
		for (i=0; i<nx ; i++)
		{	phase[i]	+= offset_theta;
		}
		
		
		sprintf(filename_excitation_out, "%s%s%d%s", root_filename,"M",     i_file, file_extension);
		sprintf(filename_phase_out,      "%s%s%d%s", root_filename,"theta", i_file, file_extension);
		err=save_data_bin(filename_excitation_out, excitation, nx);
		err=save_data_bin(filename_phase_out,      phase,      nx);
		
		free(excitation);
		
		if (compute_dtheta==1)
		{	
			t=(float*)calloc(nx, sizeof(float));
			for (i=0;i<nx;i++) 		t[i]=(float)i;

			dtheta=(float*)calloc(nx, sizeof(float));
			diff_splines_float(t, phase, nx, dtheta);
			free(t);
	
			sprintf(filename_dtheta_out, "%s%s%d%s", root_filename,"dtheta", i_file, file_extension);	
			err=save_data_bin(filename_dtheta_out, dtheta, nx);
			free(dtheta);
		}
		free(phase);
	}
	win_printf("extraction is done");
	
	return(refresh_plot(pr, UNCHANGED));
}




void spit(char *message)
{	size_t	l_max=55;
	char 	white_string[64]="                                              ";
	
	if (strlen(message)<l_max) 	strncat (message, white_string, l_max-strlen(message));
	textout_ex(screen, font, message, 40, 40, makecol(100,155,155), makecol(2,1,1));
	return;
}


MENU *pendule_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
	add_item_to_menu(mn,"load phase from NI4472",						do_load_phase_NI4472,						NULL, 0, NULL);
	add_item_to_menu(mn,"extract theta and excitation from NI4472",		do_convert_angle_and_excitation_from_NI4472,NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"plot theor. fluctuation psd",					do_plot_theorical_fluctuation_psd,			NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"calibrate the spring constant from NI4472",   	do_absolute_measurement_of_C_from_NI4472,	NULL, 0, NULL);
	add_item_to_menu(mn,"calibrate the spring constant from bin files",	do_absolute_measurement_of_C_from_bin_files,NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"save kinetic and potential energy", 			do_pendule_compute_kinetic_and_potential_energy,	NULL,0,NULL);
	add_item_to_menu(mn,"save work power", 								do_pendule_compute_work_power,						NULL,0,NULL);
	add_item_to_menu(mn,"save heat power", 								do_pendule_compute_heat_power, 						NULL,0,NULL);
	add_item_to_menu(mn,"save heat power (Gibbs)", 						do_pendule_compute_heat_Gibbs_power, 				NULL,0,NULL);
	add_item_to_menu(mn,"save heat power (excess)", 						do_pendule_compute_Q_exc_power, 				NULL,0,NULL);
	
	return mn;
}


int	pendule_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("pendule", NULL, pendule_plot_menu(), 0, NULL);
	return D_O_K;
}


int	pendule_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"pendule",	NULL, NULL);
	return D_O_K;
}
#endif

