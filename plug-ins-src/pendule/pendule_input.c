#ifndef _PENDULE_INPUT_C_
#define _PENDULE_INPUT_C_
 // functions to load and pre-treat phase from interferometer 
#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"

#include <gsl/gsl_histogram.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_statistics.h>
// #include <gsl/gsl_statistics_float.h>
#include <fftw3.h>


/* If you include other plug-ins header do it here*/ 
#include "../inout/inout.h"
#include "../../src/menus/plot/treat/p_treat_fftw.h"
#include "fft_filter_lib.h"
#include "fftw3_lib.h"
/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "pendule.h"


//Calcul de l'angle theta (en radian) ou la difference de marche (en nm) a partir de x et y
int compute_phase(	float *phase, float *x, float *y, int nx, float x_offset, float y_offset,
					float lambda, float indice, int bool_radian, float conv_radian)
{	int j2,i2;
	
	for (j2=0; j2<nx; j2++)
	{	x[j2]	  -=  x_offset;
		y[j2]	  -=  y_offset;
		(phase)[j2] = (float)atan2((double)y[j2], (double)x[j2]); 
	}
	
	for (i2=1; i2<nx; i2++)
	{	if ( fabs((phase)[i2]-(phase)[i2-1]) >= M_PI)
		{
			if (phase[i2]<phase[i2-1]) // we are -2\pi to low, so correct this	
				for (j2=i2; j2<nx; j2++)	(phase)[j2] += 2.*M_PI;
			else
				for (j2=i2; j2<nx; j2++)	(phase)[j2] -= 2.*M_PI;
		}
	}
	// conversion en nanometres :
	for (j2=0; j2<nx; j2++) 			(phase)[j2] *= lambda/(2.*indice*M_PI);
	//conversion en radian
	if (bool_radian==1)	
		for (j2=0; j2<nx; j2++) 		(phase)[j2] *= conv_radian;
	return(0);
}
//end of function


//Fonction qui demande les parametres de la calibration
int ask_for_parameters_of_pendule(	float *lambda, float *indice, int *bool_radian, float *conv_radian,
									int *do_filter_HP, float *cutoff_HP_f, float *width_HP_f,
									int *bool_remove_bounds_HP, int *do_filter_LP, float *cutoff_LP_f, float *width_LP_f,
									int *bool_remove_bounds_LP, int *bool_update_current_filter	)
{	int i;
	float l_lambda=*lambda,l_indice=*indice,l_conv_radian=*conv_radian, l_cutoff_HP_f=*cutoff_HP_f, l_width_HP_f=*width_HP_f;
	float l_cutoff_LP_f=*cutoff_LP_f, l_width_LP_f=*width_LP_f;
	int l_bool_remove_bounds_LP=*bool_remove_bounds_LP, l_do_filter_LP=*do_filter_HP;
	int l_bool_radian=*bool_radian, l_do_filter_HP=*do_filter_HP, l_bool_remove_bounds_HP=*bool_remove_bounds_HP;
	int l_bool_update_current_filter=*bool_update_current_filter;
		
	i=win_scanf("{\\pt14\\color{yellow} Parameters for the loading of phase}\n"
				"laser wavelength : %10f (nm)\n"
     		  	"optical indice : %10f\n\n"
     		  	"%R data in nm, or %r in rad /Conversion coefficient: %9f rad/nm\n"
     	 	  	"%b filter data (High-Pass filter): cutoff: %8f Hz, width : %8f Hz (e.g.: cutoff/2)\n"z
				"%b remove first and last points where filter is giving weird results\n"
				"%b filter data (Low-Pass filter): cutoff: %8f Hz, width : %8f Hz (e.g.: cutoff/2)\n"
				"%b remove first and last points where filter is giving weird results\n"
				"%b update current filter",
				&l_lambda, &l_indice, &l_bool_radian,&l_conv_radian,&l_do_filter_HP,&l_cutoff_HP_f, &l_width_HP_f,
				&l_bool_remove_bounds_HP, &l_do_filter_LP, &l_cutoff_LP_f, &l_width_LP_f, &l_bool_remove_bounds_LP, &l_bool_update_current_filter);
	if (i==CANCEL) return(OFF);
	
	*lambda=l_lambda;*indice=l_indice;*bool_radian=l_bool_radian;*conv_radian=l_conv_radian;
	*do_filter_LP=l_do_filter_LP;*cutoff_LP_f=l_cutoff_LP_f;*width_LP_f=l_width_LP_f;
	*do_filter_HP=l_do_filter_HP;*cutoff_HP_f=l_cutoff_HP_f;*width_HP_f=l_width_HP_f;
	*bool_remove_bounds_LP=l_bool_remove_bounds_LP;
	*bool_remove_bounds_HP=l_bool_remove_bounds_HP;*bool_update_current_filter=l_bool_update_current_filter;
	return(0);     	 	  
}



//Filtrage des data High Pass
int filter_data_HP		(float *phase, int nx,	float cutoff_HP_f, float width_HP_f, float f_acq,
					int bool_remove_bounds_HP,int bool_update_current_filter)
{	int				i;
//	int 			cutoff, width;
	int				nx_out;
//	double 			*in;
	float 		*in;
//	fftw_complex 	*out;
//	fftw_plan 		plan;
//	fftf 			*filtre_HP;
	int	bool_Hanning=0; // par defaut, il n'y avait pas de fenetrage applique!
                                // du coup, il peut-etre pertinent d'appliquer un fenetrage, et du coup de ne pas enlever de pts...
	
/*	
// filter construction:
		cutoff = (int)((cutoff_HP_f*(float)nx)/(f_acq));
		width  = (int)((width_HP_f *(float)nx)/(f_acq));
		filtre_HP = build_highpass_filter(nx, cutoff, width );
		
// filter application: 
		in  = fftw_malloc((nx+(nx%2))*sizeof(double));		// to store initial data
    	out = fftw_malloc((nx/2+1)   *sizeof(fftw_complex));	// to store the complex Fourier transform
 		if (in==NULL || out==NULL)  return win_printf_OK("malloc error");
 		
		for (i=0; i<nx ; i++)      in[i] = (double)((phase)[i]);

		plan = fftw_plan_dft_r2c_1d(nx, in, out, FFTW_ESTIMATE);	fftw_execute(plan);		fftw_destroy_plan(plan);
		apply_filter_complex(filtre_HP, out, nx/2+1);	// the fft has nx/2+1 complex points
		plan = fftw_plan_dft_c2r_1d(nx, out, in, FFTW_ESTIMATE); 	fftw_execute(plan); 	fftw_destroy_plan(plan);
*/

		// following function does a calloc itself:
		in = Fourier_filter_float(phase, nx, f_acq, -1, -1, cutoff_HP_f, width_HP_f, bool_Hanning);
		// -1 as a lowpass frequency implies a HP filter.

		if (bool_remove_bounds_HP==1)		nx_out = nx - (int)(3.*(f_acq/cutoff_HP_f));
		else								nx_out = nx;
		
/*
		if (bool_update_current_filter==1)	current_filter=filtre_HP;
		else free_filter(filtre_HP);
*/		
		for (i=0; i<nx_out ; i++)	(phase)[i]=(float)(in[i+(nx-nx_out)/2]/nx);
		fftw_free(in); 
/*		fftw_free(out);
*/
		return(nx_out);
}



//Filtrage des data Low Pass
int filter_data_LP	(float *phase, int nx,	float cutoff_LP_f, float width_LP_f, float f_acq,
					int bool_remove_bounds_LP,int bool_update_current_filter)
{	int		i;
	int		nx_out;
	float		*in;
	int 		bool_Hanning=0;

	// following function does a calloc itself:
	in = Fourier_filter_float(phase, nx, f_acq, cutoff_LP_f, width_LP_f, -1, -1, bool_Hanning);
	// -1 as the highpass frequency implies a LP filter.

	if (bool_remove_bounds_LP==1)		nx_out = nx - (int)(3.*(f_acq/cutoff_LP_f));
	else					nx_out = nx;
		
//		if (bool_update_current_filter==1)	current_filter=filtre_LP;
//		else free_filter(filtre_LP);
		
	for (i=0; i<nx_out ; i++)	(phase)[i]=(float)(in[i+(nx-nx_out)/2]/nx);
	fftw_free(in); 
	return(nx_out);
}



//fonction donnant le rayon local du cercle en fonction
int compute_mean_radius(float *norm, float *x, float *y, int nx, float x_offset, float y_offset)
{	register int i;
	
	*norm=0;
	for (i=0; i<nx; i++) 
	{	x[i]	-=	x_offset;
		y[i]	-=	y_offset;		
		*norm 	+=	sqrt(x[i]*x[i]+y[i]*y[i]);
	}
	*norm	/=	(float)nx;
	return(0);
}

#endif	
