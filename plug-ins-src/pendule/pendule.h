#ifndef _PENDULE_H_
#define _PENDULE_H_

#define k_b 1.3806503e-23	// m2 kg s-2 K-1
#define Temperature (25.+273.)		// K

PXV_FUNC(int, do_load_phase_NI4472,								(void));
PXV_FUNC(int, do_plot_theorical_fluctuation_psd,				(void));
PXV_FUNC(int, do_absolute_measurement_of_C_from_NI4472,			(void));
PXV_FUNC(int, do_absolute_measurement_of_C_from_bin_files,		(void));
PXV_FUNC(int, do_convert_angle_and_excitation_from_NI4472,		(void));
void spit(char *message);

																					
#endif

