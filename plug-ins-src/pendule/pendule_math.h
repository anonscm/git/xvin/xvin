#ifndef _PENDULE_MATH_H_
#define _PENDULE_MATH_H_

// following functions are defined in 'pendule_math.c':
PXV_FUNC(int, pendule_compute_Gibbs_entropy_inc, (double *u, double *v, int ny,
                                                 gsl_histogram *hist_x, gsl_histogram *hist_v, double *dS));

PXV_FUNC(int, pendule_compute_Gibbs_heat_power,  (double *x, double *v, int ny, 
                                                 gsl_histogram *hist_x, gsl_histogram *hist_v, double dt, double *Q_Gibbs));
                                                 
#endif
