#ifndef _PENDULE_ENERGIES_C_
#define _PENDULE_ENERGIES_C_
// functions to computes energies and powers
#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"

#include <gsl/gsl_histogram.h>

/* If you include other plug-ins header do it here*/ 
#include "../inout/inout.h"
#include "../hist/hist.h"
// #include "../diff/diff.h"
// #include "../../src/menus/plot/treat/p_treat_fftw.h"
// #include "fft_filter_lib.h"
/* But not below this define */
#define BUILDING_PLUGINS_DLL

#include "pendule.h"
// #include "pendule_input.h" // functions to load and pre-treat phase from interferometer
#include "pendule_math.h"  // functions to perform mathematical operations on data
#include "pendule_energies.h"



///////////////////////////////////////////////////////////////////////////////
//This function calculate the kinetic and potential energy and save them in files
int do_pendule_compute_kinetic_and_potential_energy(void)
{	int i,k;
	int	*files_index=NULL;
	int	n_theta, n_dtheta, n_files, index, i_file;
	char		s[1024];
static	char files_ind_string[128]	="1:1:20";    
static	char file_extension[8]		=".bin";         
static	char theta_root_filename[128]   = "theta";
static char  Ec_root_filename[128] 	="Ec";
static char  Ep_root_filename[128] 	="Ep"; 
static char  dEc_root_filename[128] 	="dEc";
static char  dEp_root_filename[128] 	="dEp"; 
static char  dEcEp_root_filename[128] 	="dEcEp";
static	char dtheta_root_filename[128] = "dtheta";
	char		filename[512],filename_Ep_out[256], filename_Ec_out[256],filename_dEp_out[256], filename_dEc_out[256],filename_dEcEp_out[256];
	float	*theta, *dtheta, *Ep, *Ec,*tmp;
static float C=5e-4, f0=221, f_acq=8192;
	pltreg	*pr = NULL;
	int err;
int bool_dEp=0, bool_dEc=0, bool_dEcEp=0;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine computes the kinetic energy and the potential energy,\n"
												"and save them in bin files.");
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of file with angle \\theta %%s"
			"root name of file with the derivate of angle d\\theta %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n"
			"root name of output file with Ep? %%s\n"
			"root name of output file with Ec? %%s\n"
			"calculate dEp ? %%b\n"
			"calculate dEc ? %%b\n"
			"calculate dEc+dEp ? %%b\n");
	i=win_scanf(s, &theta_root_filename, &dtheta_root_filename,  
			&files_ind_string, &file_extension, &Ep_root_filename, &Ec_root_filename, &bool_dEp, &bool_dEc, &bool_dEcEp);
	if (i==CANCEL) return(OFF);
		
	i=win_scanf("sampling frequency ? %8f\n"
				"resonance frequency ? %8f\n"
				"torsion stiffness C ? %8f",
				&f_acq, &f0, &C);
	if (i==CANCEL) return(OFF);				
					
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%d%s", theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);
		
		sprintf(filename, "%s%d%s", dtheta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_dtheta=load_data_bin(filename, &dtheta);

		if (n_dtheta!=n_theta) return(win_printf_OK("d\\theta has %d points but \\theta has %d points", n_dtheta, n_theta));		
		
		Ec=(float*)calloc(n_theta,sizeof(float));
		Ep=(float*)calloc(n_theta,sizeof(float));
		
		for (i=0; i<n_theta; i++)
		{   Ep[i]=C*(theta[i]*theta[i])/2.;
			Ec[i]=C/(4.*M_PI*M_PI*f0*f0)*f_acq*f_acq*(dtheta[i]*dtheta[i])/2.;
		}
		
		sprintf(filename_Ep_out, "%s%d%s", Ep_root_filename, i_file, file_extension);
		err=save_data_bin(filename_Ep_out, Ep, n_theta);                                  

		
		sprintf(filename_Ec_out, "%s%d%s", Ec_root_filename, i_file, file_extension);  
		err=save_data_bin(filename_Ec_out, Ec, n_theta);  
		if (bool_dEp==1)
		{tmp=(float*)calloc(n_theta-1,sizeof(float));
			for (i=0;i<n_theta-1;i++) tmp[i] = Ep[i+1]-Ep[i];
		sprintf(filename_dEp_out, "%s%d%s", dEp_root_filename, i_file, file_extension);  
		err=save_data_bin(filename_dEp_out, tmp, n_theta-1);  
		free(tmp);
		}
		if (bool_dEc==1)
		{tmp=(float*)calloc(n_theta-1,sizeof(float));
			for (i=0;i<n_theta-1;i++) tmp[i] = Ec[i+1]-Ec[i];
		sprintf(filename_dEc_out, "%s%d%s", dEc_root_filename, i_file, file_extension);  
		err=save_data_bin(filename_dEc_out, tmp, n_theta-1);  
		free(tmp);
		}
		
		if (bool_dEcEp==1)
		{tmp=(float*)calloc(n_theta-1,sizeof(float));
			for (i=0;i<n_theta-1;i++) tmp[i] = Ec[i+1]-Ec[i]+Ep[i+1]-Ep[i];
		sprintf(filename_dEcEp_out, "%s%d%s", dEcEp_root_filename, i_file, file_extension);  
		err=save_data_bin(filename_dEcEp_out, tmp, n_theta-1);  
		free(tmp);
		}
		
		free(Ec);                                 
		free(Ep);
		free(theta);
		free(dtheta);
	}       
	free(files_index);                                                              
	return refresh_plot(pr, UNCHANGED);
} //end of function do_compute_kinetic_and_potential_energy






//This function calculate the heat power and save them in files
int do_pendule_compute_heat_power(void)
{	int i,k;
	int	*files_index=NULL;
	int	n_theta, n_dtheta,n_M, n_files, index, i_file;
	char		s[512];
static	char files_ind_string[128]	="1:1:20";    
static	char file_extension[8]		=".bin";         
static	char theta_root_filename[128]   = "theta";
static char  P_Q_root_filename[128] 	="P_Q";
static char  M_root_filename[128] 	="M"; 
static	char dtheta_root_filename[128] = "dtheta";
	char		filename[512],filename_P_Q_out[256];
	float	*theta, *dtheta, *M, *P_Q;
	float Ep, Ep_old, Ec, Ec_old;
static float C=5e-4, f0=221, f_acq=8192;
	pltreg	*pr = NULL;
	int err;


	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine computes the heat power,\n"
												"and save it in bin files.");
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of file with angle \\theta %%s"
			"root name of file with the derivate of angle d\\theta %%s"
			"root name of file with excitation M %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n"
			"root name of output file with P_Q? %%s\n");
	i=win_scanf(s, &theta_root_filename, &dtheta_root_filename,  &M_root_filename,
			&files_ind_string, &file_extension, &P_Q_root_filename);
	if (i==CANCEL) return(OFF);
		
	i=win_scanf("sampling frequency ? %8f\n"
				"resonance frequency ? %8f\n"
				"torsion stiffness C ? %8f",
				&f_acq, &f0, &C);
	if (i==CANCEL) return(OFF);				
					
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%d%s", theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);
		
		sprintf(filename, "%s%d%s", dtheta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_dtheta=load_data_bin(filename, &dtheta);

		sprintf(filename, "%s%d%s", M_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_M=load_data_bin(filename, &M);
		
		if (n_dtheta!=n_theta) return(win_printf_OK("d\\theta has %d points but \\theta has %d points", n_dtheta, n_theta));		
		if (n_M!=n_theta) return(win_printf_OK("M has %d points but \\theta has %d points", n_M, n_theta));		
		
		P_Q=(float*)calloc(n_theta,sizeof(float));
		
		Ep_old=C*(theta[0]*theta[0])/2.;
		Ec_old=C/(4.*M_PI*M_PI*f0*f0)*f_acq*f_acq*(dtheta[0]*dtheta[0])/2.;
			
		for (i=1; i<n_theta; i++)
		{   Ep=C*(theta[i]*theta[i])/2.;
			Ec=C/(4.*M_PI*M_PI*f0*f0)*f_acq*f_acq*(dtheta[i]*dtheta[i])/2.;
			P_Q[i-1] = M[i-1]*dtheta[i-1]-(Ep-Ep_old+Ec-Ec_old);
			Ep_old=Ep;
			Ec_old=Ec;
		}
		P_Q[n_theta-1]=0.;
		
		sprintf(filename_P_Q_out, "%s%d%s", P_Q_root_filename, i_file, file_extension);
		err=save_data_bin(filename_P_Q_out, P_Q, n_theta);                                  
 
		
		free(P_Q);                                 
		free(theta);
		free(dtheta);
		free(M);
	}       
	free(files_index);                                                              
	return refresh_plot(pr, UNCHANGED);
}  //end of function do_compute_heat_power
 


//This function calculate the work power and save them in files
int do_pendule_compute_work_power(void)
{	int i,k;
	int	*files_index=NULL;
	int	n_dtheta,n_M, n_files, index, i_file;
	char		s[512];
static	char files_ind_string[128]	="1:1:20";    
static	char file_extension[8]		=".bin";         
static char  P_W_root_filename[128] 	="P_W";
static char  M_root_filename[128] 	="M"; 
static	char dtheta_root_filename[128] = "dtheta";
	char		filename[512],filename_P_W_out[256];
	float	*dtheta, *M, *P_W;
static float f_acq=8192, correc=1./1.0477;
	pltreg	*pr = NULL;
	int err;


	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine computes the work power in kT/Npts,\n"
												"and save it in bin files.");
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of file with the derivate of angle d\\theta %%s"
			"root name of file with excitation M %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n"
			"root name of output file with P_W (kT/s)? %%s\n");
	i=win_scanf(s, &dtheta_root_filename,  &M_root_filename,
			&files_ind_string, &file_extension, &P_W_root_filename);
	if (i==CANCEL) return(OFF);
		
	i=win_scanf("sampling frequency ? %8f\n"
				"facteur correctif \\sqrt{C_calib/C_ref} ? %8f\n"
				"(the power will be multiply by this facteur)",
				&f_acq, &correc);
	if (i==CANCEL) return(OFF);				
					
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%d%s", dtheta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_dtheta=load_data_bin(filename, &dtheta);

		sprintf(filename, "%s%d%s", M_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_M=load_data_bin(filename, &M);
		
		
		if (n_M!=n_dtheta) return(win_printf_OK("M has %d points but \\theta has %d points", n_M, n_dtheta));		
		
		P_W=(float*)calloc(n_dtheta,sizeof(float));
		
		
			
		for (i=0; i<n_dtheta; i++)
		{   P_W[i] = correc*M[i]*dtheta[i];
		}
			
		sprintf(filename_P_W_out, "%s%d%s", P_W_root_filename, i_file, file_extension);
		err=save_data_bin(filename_P_W_out, P_W, n_dtheta);                                  
 
		
		free(P_W);                                 
		free(dtheta);
		free(M);
	}  
	free(files_index);                                                                   
	return refresh_plot(pr, UNCHANGED);
}  //end of function do_pendule_compute_work_power
   	
        	
        	
        	




//This function calculate the Gibbs heat power and save them in files
// NOTE : We assume p(x,v) = p(x)*p(v) (independance) 
//        so we use 2 1d-pdfs to handle the 2d-distribution p(x,v)
// this can be generalized if necessary...
int do_pendule_compute_heat_Gibbs_power(void)
{	int i,k;
	int	*files_index=NULL;
	int	n_theta, n_dtheta, n_files, index, i_file;
	char		s[512];
static	char files_ind_string[128]	="1:1:20";    
static	char file_extension[8]		=".bin";         
static	char theta_root_filename[128]   = "theta";
static	char dtheta_root_filename[128] = "dtheta";
static char  P_Q_root_filename[128] 	="P_Q_Gibbs";
static int 	bool_auto_detect_bins_in_file_x=0, bool_auto_detect_bins_in_file_v=0;
	gsl_histogram *hist_x=NULL, *hist_v=NULL;
static char pdf_filename_x[512]="pdf_theta.dat";
static char pdf_filename_v[512]="pdf_dtheta.dat";
	char		filename[512],filename_P_Q_out[256];
	double	*theta, *dtheta, *P_Q_Gibbs;
static float C=5e-4, f0=221, f_acq=8192;
static int nh_x=401, nh_v=401;
	pltreg	*pr = NULL;
	int err;
	FILE *file=NULL;


	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine computes the heat power,\n"
												"and save it in bin files.");
	
	sprintf(s, "{\\color{yellow}\\pt14 Gibbs trajectory-dependent heat, on several files}\n"
			"root name of file with angle \\theta %%s"
			"root name of file with the derivate of angle d\\theta %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n"
			"root name of output file with Q_{Gibbs}? %%s\n");
	i=win_scanf(s, &theta_root_filename, &dtheta_root_filename, 
			&files_ind_string, &file_extension, &P_Q_root_filename);
	if (i==CANCEL) return(OFF);
		
	i=win_scanf("sampling frequency ? %8f\n"
				"resonance frequency ? %8f\n"
				"torsion stiffness C ? %8f",
				&f_acq, &f0, &C);
	if (i==CANCEL) return(OFF);				
					
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));

  // load the histograms/pdfs:	
	{ win_scanf("{\\pt14\\color{yellow}load pdf of \\theta and d\\theta/dt}\n"
		"filename for pdf of \\theta %s\nnumber of bins : %5d or %b autodetect\n"
		"filename for pdf of d\\theta/dt %s\nnumber of bins : %5d or %b autodetect\n",
        &pdf_filename_x, &nh_x, &bool_auto_detect_bins_in_file_x, &pdf_filename_v, &nh_v, &bool_auto_detect_bins_in_file_v);
		if (bool_auto_detect_bins_in_file_x==1) 
		{	nh_x = count_number_of_lines_in_ascii_file(pdf_filename_x, 3); // 3 columns
			if (win_printf("I autodetected %d lines\n => %d bins in histogram of \\theta", nh_x, nh_x)==CANCEL)
               return(D_O_K);
		}
		if (bool_auto_detect_bins_in_file_v==1) 
		{	nh_v = count_number_of_lines_in_ascii_file(pdf_filename_v, 3); // 3 columns
			if (win_printf("I autodetected %d lines\n => %d bins in histogram of d\\theta /dt", nh_v, nh_v)==CANCEL)
			   return(D_O_K);
		}
		if ( (nh_x<2) || (nh_v<2) ) return(win_printf_OK("error with nb of bins..."));
		
		file=fopen(pdf_filename_x, "rt");
		if (file==NULL) return(win_printf_OK("cannot open file %s", pdf_filename_x));
		hist_x = (gsl_histogram *)gsl_histogram_alloc(nh_x);
		if (gsl_histogram_fscanf(file, hist_x)!=0) return(win_printf_OK("(GSL) error loading pdf of \\theta !")); 	
		fclose(file);

		file=fopen(pdf_filename_v, "rt");
		if (file==NULL) return(win_printf_OK("cannot open file %s", pdf_filename_v));
		hist_v = (gsl_histogram *)gsl_histogram_alloc(nh_v);
		if (gsl_histogram_fscanf(file, hist_v)!=0) return(win_printf_OK("(GSL) error loading pdf of d\\theta/dt !")); 	
		fclose(file);
    }
        
	// transform histograms into usable pdf:
	normalize_histogram_Y(hist_x);        
	normalize_histogram_Y(hist_v);        
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%d%s", theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin_to_double(filename, &theta);
		
		sprintf(filename, "%s%d%s", dtheta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_dtheta=load_data_bin_to_double(filename, &dtheta);
	
		if (n_dtheta!=n_theta) return(win_printf_OK("d\\theta has %d points but \\theta has %d points", n_dtheta, n_theta));		
		
		P_Q_Gibbs=(double*)calloc(n_theta,sizeof(double));
        // following function is in "pendule_math.c" :
        pendule_compute_Gibbs_heat_power(theta, dtheta, n_theta, hist_x, hist_v, 1.0, P_Q_Gibbs);		
		// P_Q_Gibbs has n_theta points allocated, but n_theta-1 points wil physical values
		// we set dt=1., because we compute heat increments, dt-independent
		// idem work,heat,Ep,Ec when working on files
		
		sprintf(filename_P_Q_out, "%s%d%s", P_Q_root_filename, i_file, file_extension);
		err=save_data_double_bin(filename_P_Q_out, P_Q_Gibbs, n_theta);                                  
                                
		free(theta); free(dtheta); free(P_Q_Gibbs);  
	}       
	gsl_histogram_free(hist_x);
	gsl_histogram_free(hist_v);
	free(files_index);                                                              
	return refresh_plot(pr, UNCHANGED);
}  //end of function do_pendule_compute_heat_Gibbs_power
 
 //This function calculate the work power and save them in files
int do_pendule_compute_Q_exc_power(void)
{	int i,k;
	int	*files_index=NULL;
	int	n_dtheta,n_M, n_files, index, i_file;
	char		s[512];
static	char files_ind_string[128]	="1:1:20";    
static	char file_extension[8]		=".bin";         
static char  P_W_root_filename[128] 	="Q_ex";
static char  M_root_filename[128] 	="Q_HK"; 
static	char dtheta_root_filename[128] = "Q_tot";
	char		filename[512],filename_P_W_out[256];
	float	*dtheta, *M, *Q_exc;
	pltreg	*pr = NULL;
	int err;


	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])	return win_printf_OK("This routine computes the work power in kT/Npts,\n"
												"and save it in bin files.");
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of file with exchange heat %%s"
			"root name of file with housekeeping heat %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n"
			"root name of output file with Q_exc (kT/s)? %%s\n");
	i=win_scanf(s, &dtheta_root_filename,  &M_root_filename,
			&files_ind_string, &file_extension, &P_W_root_filename);
	if (i==CANCEL) return(OFF);
		
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%d%s", dtheta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_dtheta=load_data_bin(filename, &dtheta);

		sprintf(filename, "%s%d%s", M_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_M=load_data_bin(filename, &M);
		
		
		if (n_M!=n_dtheta) return(win_printf_OK("M has %d points but \\theta has %d points", n_M, n_dtheta));		
		
		Q_exc=(float*)calloc(n_dtheta,sizeof(float));
		
		
			
		for (i=0; i<n_dtheta; i++)
		{   Q_exc[i] = M[i]+dtheta[i];
		}
			
		sprintf(filename_P_W_out, "%s%d%s", P_W_root_filename, i_file, file_extension);
		err=save_data_bin(filename_P_W_out, Q_exc, n_dtheta);                                  
 
		
		free(Q_exc);                                 
		free(dtheta);
		free(M);
	}  
	free(files_index);                                                                   
	return refresh_plot(pr, UNCHANGED);
}  //end of function do_pendule_excess_heat 	


#endif
