#ifndef _TRACKRG_H_
#define _TRACKRG_H_

PXV_FUNC(int, do_trackRG_average_along_y, (void));
PXV_FUNC(MENU*, trackRG_image_menu, (void));
PXV_FUNC(int, trackRG_main, (int argc, char **argv));
#endif

