/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _TRACKRG_C_
#define _TRACKRG_C_

# include "allegro.h"
# include "xvin.h"
# include "immintrin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "trackRG.h"
//place here other headers of this plugin 

int oi_mouse_GifAvg_action(struct one_image *oi, int x0, int y0, int mode, DIALOG *d);

# define IS_IM_EXREMUM 357951

static double thres_s = 0, tolerance_s = 0, sigma_s = 1.4;
static float err_a_s = 0.15, err_b_s = 200;



# define	INDEX_OUT_OF_RANGE	3
# define	PB_WITH_EXTREMUM	2
# define	TOO_MUCH_NOISE		4
# define	GETTING_REF_PROFILE	1


/*	Find maximum position in a 1d array of 5 points with x[2] greater than its neighbours
 *	return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int	find_local_max(double *x, double *Max_pos, double *Max_val, double *Max_deriv)
{
  int   ret = 0, delta;
  double a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0;

  f_2 = x[0];
  f_1 = x[1];
  f0 = x[2];
  f1 = x[3];
  f2 = x[4];
  if (f_1 < f1)
    {
      a = -f_1/6 + f0/2 - f1/2 + f2/6;
      b = f_1/2 - f0 + f1/2;
      c = -f_1/3 - f0/2 + f1 - f2/6;
      d = f0;
      delta = 0;
    }	
  else
    {
      a = b = c = 0;	
      a = -f_2/6 + f_1/2 - f0/2 + f1/6;
      b = f_2/2 - f_1 + f0/2;
      c = -f_2/3 - f_1/2 + f0 - f1/6;
      d = f_1;
      delta = -1;
    }
  if (fabs(a) < 1e-8)
    {
      if (b != 0)	xm =  - c/(2*b) - (3 * a * c * c)/(4 * b * b * b);
      else
	{
	  xm = 0;
	  ret |= PB_WITH_EXTREMUM;
	}
    }
  else if ((b*b - 3*a*c) < 0)
    {
      ret |= PB_WITH_EXTREMUM;
      xm = 0;
    }
  else
    xm = (-b - sqrt(b*b - 3*a*c))/(3*a);
  *Max_pos = (float)(xm + delta); 
  *Max_val = (float)((a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d);
  if (Max_deriv)    *Max_deriv = (float)((6 * a * xm) + (2 * b));
  return ret;
}


int detect_maxima_from_avg_profiles(double xp[5], double yp[5], double threshold_z, double threshold_dz, double max[6])
{
  double Maxx_pos, Maxx_val, Maxx_deriv, Maxy_pos, Maxy_val, Maxy_deriv;
  int ret;
  
  if ((yp[2] > yp[1]) && (yp[2] >= yp[3]) && (xp[2] > xp[1]) 
      && (xp[2] >= xp[3]) && (yp[2] > threshold_z) && (xp[2] > threshold_z))
    { 
      ret = find_local_max(yp, &Maxy_pos, &Maxy_val, &Maxy_deriv);
      if (ret == 0)
	{
	  ret = find_local_max(xp, &Maxx_pos, &Maxx_val, &Maxx_deriv);
	  if ((ret == 0) && (Maxx_val > threshold_z) && (Maxy_val > threshold_z) 
	      && (fabs(Maxx_deriv) > threshold_dz) && (fabs(Maxy_deriv) > threshold_dz))	
	    {
	      max[0] = Maxx_pos;
	      max[1] = Maxy_pos;
	      max[2] = Maxx_val;
	      max[3] = Maxy_val;
	      max[4] = Maxx_deriv;
	      max[5] = Maxy_deriv;
	      return 1;
	    }
	}
    }
  return 0;
}

int project_on_gaussian(double **zd,    // input data set
			int verbose, // issue explicit error message
			float error_a, // the error proportional to the signal
			float error_b, // the error offset
			double x0,     // center position 
			double y0,     // should be close to 0 < 2
			double sigma,  // size of the gaussian
			double *a,   // the best a value
			double *b,   // the best b value
			double *E)   // the chi^2
{
  register int i, j;
  int size_x = 9, off_x = 4, size_y = 9, off_y = 4;
  double  f, fx, er, la, lb, gau[9][9], r2, tmpx, tmpy, lE, sigma2;
  double sy = 0, sy2 = 0, sfx = 0, sfx2 = 0, sfxy = 0, sig_2, ny = 0;

  sigma2 = sigma*sigma;
  for (i = 0; i < size_y; i++)
    {
      tmpy = -y0 + i - off_y;
      tmpy *= tmpy;
      for (j = 0; j < size_x; j++)
	{
	  tmpx = -x0 + j - off_x;
	  tmpx *= tmpx;
	  r2 = tmpx + tmpy;
	  r2 /= (2*sigma2);
	  gau[i][j] = exp(-r2);
	  er = error_b + error_a * zd[i][j];
	  if (er > 0)
	    {
	      sig_2 = (double)1/(er*er);
	      sy += zd[i][j]*sig_2;
	      sy2 += zd[i][j]*zd[i][j]*sig_2;
	      sfx += gau[i][j]*sig_2;
	      sfx2 += gau[i][j]*gau[i][j]*sig_2;
	      sfxy += zd[i][j]*gau[i][j]*sig_2;
	      ny += sig_2;
	    }
	  else if (verbose) win_printf("at point %d %d error negative %g",i,j,er);
	}
    }
  lb = (sfx2 * ny) - sfx * sfx;
  if (lb == 0) 
    {
      if (verbose) win_printf_OK("\\Delta  = 0 \n can't fit that!");
      return 3;
    }
  la = sfxy * ny - sfx * sy;
  la /= lb;
  lb = (sfx2 * sy - sfx * sfxy)/lb;
  for (i = 0, lE = 0; i < size_y; i++)
    {
      for (j = 0; j < size_x; j++)
	{
	  fx = la*gau[i][j] + lb;
	  f = zd[i][j] - fx;
	  er = error_b + error_a * zd[i][j];
	  if (er > 0)
	    {
	      lE += (f*f)/(er*er);
	      j++;
	    }
	}
    }
  if (a) *a = la;
  if (b) *b = lb;
  if (E) *E = lE;
  return 0;
}


int extract_x_y_avg_profiles_in_oi(O_i *oi, int ix, int iy, double xp[], int size_x, int wx, double yp[], int size_y, int wy)
{
  int mode, *li, ixp, iyp;
  unsigned char *ch;
  rgb *rgb;
  rgb16 *rgb16;
  rgba *rgba;
  rgba16 *rgba16;
  short int *in;
  unsigned short int *ui;
  float *fl, zr, zi;
  double *db, t, zdr, zdi;

  
  if (oi == NULL)	return 2;

  if (ix < 0 || iy < 0) return 1;
  if ((ix + size_x >  oi->im.nx) || (iy + size_y >  oi->im.ny)) return 1;

  mode = oi->im.mode;

  for (iyp = 0; iyp < size_y; iyp++) yp[iyp] = 0;
  for (ixp = 0; ixp < size_x; ixp++) xp[ixp] = 0;

  if ( oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  ch = oi->im.pixel[iy+iyp].ch;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += (double) (ch[ix+ixp]);
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += (double) (ch[ix+ixp]);
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGB_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgb = oi->im.pixel[iy+iyp].rgb;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    xp[ixp] += ((double)(rgb[ix+ixp].r + rgb[ix+ixp].g + rgb[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	xp[ixp] += (double)rgb[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	xp[ixp] += (double)rgb[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	xp[ixp] += (double)rgb[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  xp[ixp] += (double)255;
		  else return 1;
		}
	      if (abs(2*ixp - size_x) <= wx)  
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    yp[iyp] += ((double)(rgb[ix+ixp].r + rgb[ix+ixp].g + rgb[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	yp[iyp] += (double)rgb[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	yp[iyp] += (double)rgb[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	yp[iyp] += (double)rgb[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  yp[iyp] += (double)255;
		  else return 1;
		}
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGBA_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgba = oi->im.pixel[iy+iyp].rgba;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    xp[ixp] += ((double)(rgba[ix+ixp].r + rgba[ix+ixp].g + rgba[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	xp[ixp] += (double)rgba[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	xp[ixp] += (double)rgba[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	xp[ixp] += (double)rgba[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  xp[ixp] += (double)rgba[ix+ixp].a;
		  else return 1;
		}
	      if (abs(2*ixp - size_x) <= wx)  
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    yp[iyp] += ((double)(rgba[ix+ixp].r + rgba[ix+ixp].g + rgba[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	yp[iyp] += (double)rgba[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	yp[iyp] += (double)rgba[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	yp[iyp] += (double)rgba[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  yp[iyp] += (double)rgba[ix+ixp].a;
		  else return 1;
		}
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGB16_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgb16 = oi->im.pixel[iy+iyp].rgb16;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    xp[ixp] += ((double)(rgb16[ix+ixp].r + rgb16[ix+ixp].g + rgb16[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	xp[ixp] += (double)rgb16[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	xp[ixp] += (double)rgb16[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	xp[ixp] += (double)rgb16[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  xp[ixp] += (double)255;
		  else return 1;
		}
	      if (abs(2*ixp - size_x) <= wx)  
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    yp[iyp] += ((double)(rgb16[ix+ixp].r + rgb16[ix+ixp].g + rgb16[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	yp[iyp] += (double)rgb16[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	yp[iyp] += (double)rgb16[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	yp[iyp] += (double)rgb16[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  yp[iyp] += (double)255;
		  else return 1;
		}
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGBA16_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgba16 = oi->im.pixel[iy+iyp].rgba16;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    xp[ixp] += ((double)(rgba16[ix+ixp].r + rgba16[ix+ixp].g + rgba16[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	xp[ixp] += (double)rgba16[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	xp[ixp] += (double)rgba16[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	xp[ixp] += (double)rgba16[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  xp[ixp] += (double)rgba16[ix+ixp].a;
		  else return 1;
		}
	      if (abs(2*ixp - size_x) <= wx)  
		{
		  if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		    yp[iyp] += ((double)(rgba16[ix+ixp].r + rgba16[ix+ixp].g + rgba16[ix+ixp].b))/3;
		  else if (oi->im.mode & R_LEVEL)	yp[iyp] += (double)rgba16[ix+ixp].r;
		  else if (oi->im.mode & G_LEVEL)	yp[iyp] += (double)rgba16[ix+ixp].g;
		  else if (oi->im.mode & B_LEVEL)	yp[iyp] += (double)rgba16[ix+ixp].b;
		  else if (oi->im.mode == ALPHA_LEVEL)  yp[iyp] += (double)rgba16[ix+ixp].a;
		  else return 1;
		}
	    }
	}
    }
  else if ( oi->im.data_type == IS_INT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  in = oi->im.pixel[iy+iyp].in;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += (double) (in[ix+ixp]);
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += (double) (in[ix+ixp]);
	    }
	}
    }
  else if ( oi->im.data_type == IS_UINT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  ui = oi->im.pixel[iy+iyp].ui;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += (double) (ui[ix+ixp]);
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += (double) (ui[ix+ixp]);
	    }
	}
    }
  else if ( oi->im.data_type == IS_LINT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  li = oi->im.pixel[iy+iyp].li;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += (double) (li[ix+ixp]);
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += (double) (li[ix+ixp]);
	    }
	}
    }
  else if ( oi->im.data_type == IS_FLOAT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  fl = oi->im.pixel[iy+iyp].fl;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += (double) (fl[ix+ixp]);
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += (double) (fl[ix+ixp]);
	    }
	}
    }
  else if ( oi->im.data_type == IS_DOUBLE_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  db = oi->im.pixel[iy+iyp].db;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += (double) (db[ix+ixp]);
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += (double) (db[ix+ixp]);
	    }
	}
    }
  else if ( oi->im.data_type == IS_COMPLEX_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  fl = oi->im.pixel[iy+iyp].fl;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      zr = fl[2*(ix+ixp)];
	      zi = fl[2*(ix+ixp)+1];
	      if (mode == RE)		t = (double)zr;
	      else if (mode == IM)	t = (double)zi;
	      else if (mode == AMP)	t = sqrt(zr*zr+zi*zi);
	      else if (mode == AMP_2)	t = (double)(zr*zr+zi*zi);
	      else if (mode == LOG_AMP)	t = (zr*zr+zi*zi > 0) ? log10(zr*zr+zi*zi) : -40.0;	    
	      else return 3;
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += t;
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += t;
	    }
	}
    }
  else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  db = oi->im.pixel[iy+iyp].db;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      zdr = db[2*(ix+ixp)];
	      zdi = db[2*(ix+ixp)+1];
	      if (mode == RE)		t = zdr;
	      else if (mode == IM)	t = zdi;
	      else if (mode == AMP)	t = sqrt(zdr*zdr+zdi*zdi);
	      else if (mode == AMP_2)	t = (zdr*zdr+zdi*zdi);
	      else if (mode == LOG_AMP)	t = (zdr*zdr+zdi*zdi > 0) ? log10(zdr*zdr+zdi*zdi) : -40.0;	    
	      else return 3;
	      if (abs(2*iyp - size_y) <= wy)  xp[ixp] += t;
	      if (abs(2*ixp - size_x) <= wx)  yp[iyp] += t;
	    }
	}
    }
  else return 1;
  return 0;
}


int extract_2D_double_array_in_oi(O_i *oi, int ix, int iy, double **zd, int size_x, int size_y)
{
  int mode, *li, ixp, iyp;
  unsigned char *ch;
  rgb *rgb;
  rgb16 *rgb16;
  rgba *rgba;
  rgba16 *rgba16;
  short int *in;
  unsigned short int *ui;
  float *fl, zr, zi;
  double *db, t, zdr, zdi;

  
  if (oi == NULL)	return 2;

  if (ix < 0 || iy < 0) return 1;
  if ((ix + size_x >  oi->im.nx) || (iy + size_y >  oi->im.ny)) return 1;

  mode = oi->im.mode;

  if ( oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  ch = oi->im.pixel[iy+iyp].ch;
	  for (ixp = 0; ixp < size_x; ixp++)
	    zd[iyp][ixp] = (double) (ch[ix+ixp]);
	}
    }
  else 	if ( oi->im.data_type == IS_RGB_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgb = oi->im.pixel[iy+iyp].rgb;
	  for (ixp = 0; ixp < size_x; ixp++)
	    {
	      if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		zd[iyp][ixp] = ((double)(rgb[ix+ixp].r + rgb[ix+ixp].g + rgb[ix+ixp].b))/3;
	      else if (oi->im.mode & R_LEVEL)	    zd[iyp][ixp] = (double)rgb[ix+ixp].r;
	      else if (oi->im.mode & G_LEVEL)	    zd[iyp][ixp] = (double)rgb[ix+ixp].g;
	      else if (oi->im.mode & B_LEVEL)	    zd[iyp][ixp] = (double)rgb[ix+ixp].b;
	      else if (oi->im.mode == ALPHA_LEVEL)  zd[iyp][ixp] = (double)255;
	      else return 1;
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGBA_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgba = oi->im.pixel[iy+iyp].rgba;
	  for (ixp = 0; ixp < size_x; ixp++)
	    {
	      if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		zd[iyp][ixp] = ((double)(rgba[ix+ixp].r + rgba[ix+ixp].g + rgba[ix+ixp].b))/3;
	      else if (oi->im.mode & R_LEVEL)	     zd[iyp][ixp] = (double)rgba[ix+ixp].r;
	      else if (oi->im.mode & G_LEVEL)	     zd[iyp][ixp] = (double)rgba[ix+ixp].g;
	      else if (oi->im.mode & B_LEVEL)	     zd[iyp][ixp] = (double)rgba[ix+ixp].b;
	      else if (oi->im.mode == ALPHA_LEVEL)   zd[iyp][ixp] = (double)rgba[ix+ixp].a;
	      else return 1;
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGB16_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgb16 = oi->im.pixel[iy+iyp].rgb16;
	  for (ixp = 0; ixp < size_x; ixp++)
	    {
	      if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		zd[iyp][ixp] = ((double)(rgb16[ix+ixp].r + rgb16[ix+ixp].g + rgb16[ix+ixp].b))/3;
	      else if (oi->im.mode & R_LEVEL)	    zd[iyp][ixp] = (double)rgb16[ix+ixp].r;
	      else if (oi->im.mode & G_LEVEL)	    zd[iyp][ixp] = (double)rgb16[ix+ixp].g;
	      else if (oi->im.mode & B_LEVEL)	    zd[iyp][ixp] = (double)rgb16[ix+ixp].b;
	      else if (oi->im.mode == ALPHA_LEVEL)  zd[iyp][ixp] = (double)255;
	      else return 1;
	    }
	}
    }
  else 	if ( oi->im.data_type == IS_RGBA16_PICTURE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  rgba16 = oi->im.pixel[iy+iyp].rgba16;
	  for (ixp = 0; ixp < size_x; ixp++)
	    {
	      if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
		zd[iyp][ixp] = ((double)(rgba16[ix+ixp].r + rgba16[ix+ixp].g + rgba16[ix+ixp].b))/3;
	      else if (oi->im.mode & R_LEVEL)	     zd[iyp][ixp] = (double)rgba16[ix+ixp].r;
	      else if (oi->im.mode & G_LEVEL)	     zd[iyp][ixp] = (double)rgba16[ix+ixp].g;
	      else if (oi->im.mode & B_LEVEL)	     zd[iyp][ixp] = (double)rgba16[ix+ixp].b;
	      else if (oi->im.mode == ALPHA_LEVEL)   zd[iyp][ixp] = (double)rgba16[ix+ixp].a;
	      else return 1;
	    }
	}
    }
  else if ( oi->im.data_type == IS_INT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  in = oi->im.pixel[iy+iyp].in;
	  for (ixp = 0; ixp < size_x; ixp++)
	    zd[iyp][ixp] = (double) (in[ix+ixp]);
	}
    }
  else if ( oi->im.data_type == IS_UINT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  ui = oi->im.pixel[iy+iyp].ui;
	  for (ixp = 0; ixp < size_x; ixp++)
	    zd[iyp][ixp] = (double) (ui[ix+ixp]);
	}
    }
  else if ( oi->im.data_type == IS_LINT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  li = oi->im.pixel[iy+iyp].li;
	  for (ixp = 0; ixp < size_x; ixp++)
	    zd[iyp][ixp] = (double) (li[ix+ixp]);
	}
    }
  else if ( oi->im.data_type == IS_FLOAT_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  fl = oi->im.pixel[iy+iyp].fl;
	  for (ixp = 0; ixp < size_x; ixp++)
	    zd[iyp][ixp] = (double) (fl[ix+ixp]);
	}
    }
  else if ( oi->im.data_type == IS_DOUBLE_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  db = oi->im.pixel[iy+iyp].db;
	  for (ixp = 0; ixp < size_x; ixp++)
	    zd[iyp][ixp] = (double) (db[ix+ixp]);
	}
    }
  else if ( oi->im.data_type == IS_COMPLEX_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  fl = oi->im.pixel[iy+iyp].fl;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      zr = fl[2*(ix+ixp)];
	      zi = fl[2*(ix+ixp)+1];
	      if (mode == RE)		t = (double)zr;
	      else if (mode == IM)	t = (double)zi;
	      else if (mode == AMP)	t = sqrt(zr*zr+zi*zi);
	      else if (mode == AMP_2)	t = (double)(zr*zr+zi*zi);
	      else if (mode == LOG_AMP)	t = (zr*zr+zi*zi > 0) ? log10(zr*zr+zi*zi) : -40.0;	    
	      else return 3;
	      zd[iyp][ixp] = t;
	    }
	}
    }
  else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
      for (iyp = 0; iyp < size_y; iyp++)
	{ 
	  db = oi->im.pixel[iy+iyp].db;
	  for (ixp = 0; ixp < size_x; ixp++)
	    { 
	      zdr = db[2*(ix+ixp)];
	      zdi = db[2*(ix+ixp)+1];
	      if (mode == RE)		t = zdr;
	      else if (mode == IM)	t = zdi;
	      else if (mode == AMP)	t = sqrt(zdr*zdr+zdi*zdi);
	      else if (mode == AMP_2)	t = (zdr*zdr+zdi*zdi);
	      else if (mode == LOG_AMP)	t = (zdr*zdr+zdi*zdi > 0) ? log10(zdr*zdr+zdi*zdi) : -40.0;	    
	      else return 3;
	      zd[iyp][ixp] = t;
	    }
	}
    }
  else return 1;
  return 0;
}



int add_im_extremum(im_ext_ar *iea, double x, double y, double data, double derivate, double chi2, double sigma)
{
  im_ext *ie;
  //  static int verbose = D_O_K;

  if (iea == NULL || iea->ie == NULL) return -1;
  //if (verbose != CANCEL)
  //  verbose = win_printf("adding pixel to maximun i_ie %d n_ie %d\n x %d y %d, val %g\n"
  //			 "Pressing CANCEL will supress further message\n",i_ie,iea->n_ie,x,y,data);
  if (iea->n_ie >= iea->m_ie)
    {
      iea->m_ie = alloc_im_ext_array(iea, 2*iea->m_ie);
      if (iea->m_ie <= 0) return -2;
    }
  ie = iea->ie + iea->n_ie;
  if (data > ie->zmax)
    {
      ie->zmax = data;
      ie->dnb = derivate;
      ie->chi2 = chi2;
      ie->sigma = sigma;
      ie->xm = x;
      ie->ym = y;
    } 
  ie->xpos = x;
  ie->ypos = y;
  ie->np++;
  ie->x0 = (int)(x+0.5) - 2;
  ie->x1 = (int)(x+0.5) + 2;
  ie->y0 = (int)(y+0.5) - 2;
  ie->y1 = (int)(y+0.5) + 2;
  iea->n_ie++;
  return (iea->n_ie - 1);
}


int	find_best_a_an_b_and_sigma_of_gaussian_fit_on_9x9(double **zd,    // input data set
							  int verbose, // issue explicit error message
							  float error_a, // the error proportional to the signal
							  float error_b, // the error offset
							  double x0,     // center position 
							  double y0,     // should be close to 0 < 2
							  double *sigma,  // an array of imposed size of the gaussian
							  int n_sig, // the number of sigma
							  double *a,   // the best a value
							  double *b,   // the best b value
							  double *sig,
							  double *E)   // the chi^2
{	
  register int i, i1, k;
  double  *la, *lb, *lE, lEmax;
  int  imax, ret;
  d_s *ds;
  float Max_pos, Max_val, tmpx, p;  

  if (zd == NULL || n_sig < 1) 
    {
      if (verbose) win_printf("No valid data \n"); 
      return 1;
    }
  lE = (double*)calloc(n_sig,sizeof(double));
  la = (double*)calloc(n_sig,sizeof(double));
  lb = (double*)calloc(n_sig,sizeof(double));
  ds = build_data_set(n_sig,n_sig);
  if (lE == NULL || lb == NULL || la == NULL || ds == NULL)
    {
      if (verbose) win_printf("nomore memory\n"); 
      return 1;
    }
  for (k = 0, lEmax = 0; k < n_sig; k++)
    {
      ds->xd[k] = (float)sigma[k];
      project_on_gaussian(zd, verbose, error_a, error_b, x0, y0, sigma[k], la+k, lb+k, lE+k);
      ds->yd[k] = -(float)lE[k];
      if (k == 0 || lE[k] < lEmax)
	{
	  lEmax = lE[k];
	  imax = k;
	}
    }

  ret = find_max_around(ds->yd, ds->nx, imax, &Max_pos, &Max_val, NULL);
  if (ret == 0)
    {
      i = (int)Max_pos;
      i1 = i + 1;
      p = (float)i1 - Max_pos;
      i = (i < 0) ? 0 : i;
      i = (i < ds->nx) ? i : ds->nx-1;
      i1 = (i1 < 0) ? 0 : i1;
      i1 = (i1 < ds->nx) ? i1 : ds->nx-1;
      tmpx = p * ds->xd[i] + (1-p) * ds->xd[i1];
      project_on_gaussian(zd, verbose, error_a, error_b, x0, y0, tmpx, a, b, E);
      if (sig) *sig = tmpx;
    }
  else
    {
      if (a) *a = la[imax];
      if (b) *b = lb[imax];
      if (E) *E = lEmax;
      if (sig) *sig = sigma[imax];
    }
  //find_local_max(double *x, double *Max_pos, double *Max_val, double *Max_deriv)
  free(lE);
  free(la);
  free(lb);
  free_data_set(ds);
  return 0;
}


/*

  ROI = NULL -> complete image
  x0 = ROI[0],   x1 = ROI[1],   
  y0 = ROI[2],   y1 = ROI[3],   

 */


im_ext_ar *find_channel_maxima_positions(O_i *oi, im_ext_ar *iea, double threshold_z,  double threshold_dz, int max_number, int *ROI, int multi, double sigma, float err_a, float err_b)
{
  register int i, j, k;
  int  nmax, nys, nye, nxs, nxe, np=0, keep_mode = 0;
  double  **zdp = NULL, *zdpl[9], zdpl1[81], max[6], xp[5], yp[5], amp, background, E, x0, y0, sigmaf = 0;// zd[9][9],
  double sig[15] = {0.7,0.75,0.8,0.85,0.9,0.95,1.0,1.05,1.1,1.15,1.2,1.25,1.3,1.35,1.4};

  (void)sigma;
  for (zdp = zdpl, i = 0; i < 9; i++)
    zdpl[i] = zdpl1 + 9*i;
  // zd[i] = calloc(9, sizeof(double));
  
  if (oi == NULL)	{xvin_error(Wrong_Argument); return NULL;}
  nmax = (max_number > 0) ? max_number : 16;
  if (iea == NULL) 
    {
      iea = (im_ext_ar *)calloc(1,sizeof(im_ext_ar));
      if (iea == NULL)  return NULL;
      if (alloc_im_ext_array(iea, nmax) < 0) return NULL;
    }
  if (iea == NULL)  return NULL;

  //mode = oi->im.mode;
  nxs = (ROI) ? ROI[0] : 0;
  nys = (ROI) ? ROI[2] : 0;
  nxe = (ROI) ? ROI[1] : oi->im.nx;
  nye = (ROI) ? ROI[3] : oi->im.ny;
  nxs = (nxs > 3) ? nxs : 4;
  nxe = (nxe < oi->im.nx-4) ? nxe : oi->im.nx-5;
  nys = (nys > 3) ? nys : 4;
  nye = (nye < oi->im.ny-4) ? nye : oi->im.ny-5;


  for (i = nys ; i< nye ; i++)
    {
      for (j = nxs ; j< nxe ; j++)
	{
	  extract_x_y_avg_profiles_in_oi(oi, j-2, i-2, xp, 5, 3, yp, 5, 3);
	  if (detect_maxima_from_avg_profiles(xp, yp, 2*threshold_z, 2*threshold_dz, max) == 1) 
	    {  // multiplication by 2 of thresholds compensates ~integration on 3 value arround a max
	      extract_2D_double_array_in_oi(oi, j-4, i-4, zdp, 9, 9);
	      //find_best_a_an_b_of_gaussian_fit_on_9x9(zdp, 1, 1, max[0], max[1], sigma, &amp, &background, &E, 0);
	      find_best_a_an_b_and_sigma_of_gaussian_fit_on_9x9(zdp,1,err_a,err_b,max[0],max[1],sig,15, &amp, &background, &sigmaf,&E);

	      //win_printf("max found in %d %g %d %g zdp %g, amp %g bkg %g",j,max[1],i,max[0],zdp[4][4],amp,background);
	      k = find_in_im_extremum(iea, j, i, 2);
	      if ((amp > threshold_z) && (k == (int)iea->n_ie))
		{
		  k = (iea->n_ie > 0) ? iea->n_ie - 1 : 0;
		  if ((multi == 0) && (np > 0) && (amp > iea->ie[k].zmax)) 
		    {
		      iea->n_ie = (iea->n_ie > 0) ? iea->n_ie - 1 : 0; // we erase the last peak 
		      add_im_extremum(iea, max[0] + j, max[1] + i, amp, background, E, sigmaf);
		      np++;
		    }
		  else if (multi || (np == 0))
		    {
		      add_im_extremum(iea, max[0] + j, max[1] + i, amp, background, E, sigmaf);
		      np++;
		    }
		}
	    }
	}
    }
  if (multi == 0) 
    {
      k = (iea->n_ie > 0) ? iea->n_ie - 1 : 0;
      extract_2D_double_array_in_oi(oi, iea->ie[k].x0-2, iea->ie[k].y0-2, zdp, 9, 9);
      x0 = iea->ie[k].xpos - (int)(iea->ie[k].xpos + 0.5);
      y0 = iea->ie[k].ypos - (int)(iea->ie[k].ypos + 0.5);
      //find_best_a_an_b_of_gaussian_fit_on_9x9(zdp, 1, 1, x0, y0, sigma, &amp, &background, &E, 0);
      find_best_a_an_b_and_sigma_of_gaussian_fit_on_9x9(zdp,1,err_a,err_b,x0,y0,sig,15, &amp, &background, &sigmaf,&E);
    }

  if ((oi->im.data_type == IS_RGBA16_PICTURE) || (oi->im.data_type == IS_RGB16_PICTURE)
      || (oi->im.data_type == IS_RGBA_PICTURE) || (oi->im.data_type == IS_RGB_PICTURE))
      oi->im.mode = keep_mode;

  my_set_window_title("%d pixels in %d maxima",np,iea->n_ie);
  oi->need_to_refresh |= BITMAP_NEED_REFRESH;
  oi->im.use.to = IS_IM_EXREMUM;
  oi->im.use.stuff = (void*)iea;
  oi->oi_mouse_action = oi_mouse_GifAvg_action;
  return iea;
}

int convert_maxima_search_info_in_plot(im_ext_ar *iea, O_i *ois, double thres, double tolerance) 
{
  register int i, ip;
  d_s *ds = NULL;
  O_p *op = NULL;

  if (iea == NULL || iea->n_ie < 1) return 1;
  ip = find_op_nb_of_source_specific_ds_in_oi(ois, "multi max found with threshold");
  if (ip < 0 || ip >= ois->n_op)
    {
      op = create_and_attach_op_to_oi(ois, iea->n_ie, iea->n_ie, 0, IM_SAME_Y_AXIS|IM_SAME_X_AXIS);
      if (op == NULL) return D_O_K;
      ds = op->dat[0];
      set_ds_dot_line(ds);
      set_ds_point_symbol(ds, "\\pt4\\oc");
      ds->nx = ds->ny = 0;
    }
  else
    {
      op = ois->o_p[ip];
      ds = op->dat[0];
      ds->nx = ds->ny = 0;
    }
  set_ds_source(ds, "multi max found with threshold %g tolerence %d",thres,tolerance);
  for (i = 0; i < (int)iea->n_ie; i++)
    add_new_point_to_ds(ds, iea->ie[i].xpos, iea->ie[i].ypos);

  ois->need_to_refresh = PLOT_NEED_REFRESH;	
  return 0;
}

int oi_mouse_GifAvg_action(struct one_image *oi, int x0, int y0, int mode, DIALOG *d)
{
  int x, y, ip, i, ROI[4], n0, keep_mode = 0;
  static int old_x = 0, old_y = 0, remove = 0;
  float px1, py1;
  static double reduce = 0.5, sig[15] = {0.7,0.75,0.8,0.85,0.9,0.95,1.0,1.05,1.1,1.15,1.2,1.25,1.3,1.35,1.4};
  static float err_a = 0.15, err_b = 200;
  static int w = 8;
  imreg *imr;
  im_ext_ar *iea = NULL;
  char question[1024];
  double  **zdp = NULL, *zdpl[9], zdpl1[81], amp = 0, background = 0, E = 0, sigma = 0;

  for (zdp = zdpl, i = 0; i < 9; i++)
    zdpl[i] = zdpl1 + 9*i;

  (void)x0;
  (void)y0;
  (void)mode;
  
  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  
  if (imr->one_i == NULL)        return win_printf_OK("Could not find image data");
  oi = imr->one_i;
  if (oi->im.use.to != IS_IM_EXREMUM || oi->im.use.stuff == NULL) 
    return win_printf_OK("Could not find extremum info");
  iea = (im_ext_ar *)oi->im.use.stuff;
  
  x = mouse_x; y = mouse_y;
  px1 = x_imr_2_imdata(imr, mouse_x);    
  py1 = y_imr_2_imdata(imr, mouse_y);    
  
  if(x != old_x || x != old_y)
    {
      ip = find_in_im_extremum(iea, px1, py1, 2);
      if (ip < (int)iea->n_ie)
	{
	  snprintf(question,sizeof(question),"Extremum fund at:\nx = %g y = %g\n"
		   "Amplitude %g Background %g \n\\chi^2 = %g \\sigma = %g\n"
		   "To do nothinng click here->%%R\n"
		   "To remove extremum->%%r\n"
		   "to fit sigma in Gaussian->%%r \n"
		   "For this last case give the error expression\n"
		   "Noise offset %%8f noise factor %%8f\n",
		   iea->ie[ip].xpos, iea->ie[ip].ypos, iea->ie[ip].zmax, iea->ie[ip].dnb, iea->ie[ip].chi2, iea->ie[ip].sigma);
	  i = win_scanf(question,&remove,&err_b, &err_a);
	  if (i == CANCEL) return D_O_K;
	  if (remove == 1)
	    {
	      for (i = ip; i < (int)(iea->n_ie - 1); i++)
		iea->ie[i] = iea->ie[i+1]; 
	      iea->n_ie = (iea->n_ie > 1) ? iea->n_ie - 1 : 1;
	      /*
	      if (oi->im.data_type == IS_RGB16_PICTURE) 
		convert_maxima_search_info_in_GREEN(iea, oi, thres_s, tolerance_s);
	      else 
		{
		  convert_maxima_search_info_in_plot(iea, oi, thres_s, tolerance_s);
		  oi->need_to_refresh |= PLOT_NEED_REFRESH;	  
		}
	      */
	    }
	  else if (remove == 2)
	    {
	      if ((oi->im.data_type == IS_RGBA16_PICTURE) || (oi->im.data_type == IS_RGB16_PICTURE)
		  || (oi->im.data_type == IS_RGBA_PICTURE) || (oi->im.data_type == IS_RGB_PICTURE))
		{
		  keep_mode = oi->im.mode;
		  oi->im.mode = G_LEVEL;
		}
	      extract_2D_double_array_in_oi(oi, iea->ie[ip].x0-2, iea->ie[ip].y0-2, zdp, 9, 9);
	      //win_printf("center %g pos %g %g ",zdp[4][4],iea->ie[ip].xpos - (int)(iea->ie[ip].xpos+0.5), 
	      //	 iea->ie[ip].ypos - (int)(iea->ie[ip].ypos + 0.5));
	      find_best_a_an_b_and_sigma_of_gaussian_fit_on_9x9(zdp,1,err_a,err_b,iea->ie[ip].xpos - (int)(iea->ie[ip].xpos+0.5), 
					      iea->ie[ip].ypos - (int)(iea->ie[ip].ypos + 0.5),sig,15, &amp, &background, &sigma,&E);

	      if ((oi->im.data_type == IS_RGBA16_PICTURE) || (oi->im.data_type == IS_RGB16_PICTURE)
		  || (oi->im.data_type == IS_RGBA_PICTURE) || (oi->im.data_type == IS_RGB_PICTURE))
		  oi->im.mode = keep_mode;

	      win_printf("Initial Extremum fund at:\nx = %g y = %g\n"
			 "Amplitude %g Background %g \n\\chi^2 %g sigma %g\n"
			 "New fit sigma = %g amp %g Background %g\n"
			 "\\chi^2 = %g",iea->ie[ip].xpos, iea->ie[ip].ypos, 
			 iea->ie[ip].zmax, iea->ie[ip].dnb, iea->ie[ip].chi2, iea->ie[ip].sigma, sigma,amp,background,E);
	    }
	}
      else
	{
	  ROI[0] = (int)(0.5+px1) - w;	  ROI[1] = (int)(0.5+px1) + w;
	  ROI[2] = (int)(0.5+py1) - w;	  ROI[3] = (int)(0.5+py1) + w;
	  n0 = iea->n_ie;
	  //	  iea = find_maxima_positions(oi, iea, thres_s*reduce, tolerance_s*reduce, 256, ROI, 0, sigma_s, err_a_s, err_b_s);
	  if ((int)(iea->n_ie) > n0)
	    {
	      i = win_printf("New peak found at x = %g y = %g\n amp %g background %g\n\\chi^2 = %g \\sigma  = %g\n"
			     "Press Ok to keep CANCEL to refuse this maximum\n"
			     ,iea->ie[n0].xpos,iea->ie[n0].ypos,iea->ie[n0].zmax,iea->ie[n0].dnb, iea->ie[n0].chi2, iea->ie[n0].sigma);
	      if (i == CANCEL) iea->n_ie = n0;
	      else 
		{
		  /*
		  if (oi->im.data_type == IS_RGB16_PICTURE) 
		    convert_maxima_search_info_in_GREEN(iea, oi, thres_s, tolerance_s);
		  else 
		    {
		      convert_maxima_search_info_in_plot(iea, oi, thres_s, tolerance_s);
		      oi->need_to_refresh |= PLOT_NEED_REFRESH;	  
		    }
		  */
		}
	    }
	  else
	    {
	      i = win_scanf("No peak found! you can reduce the theshold\n"
			    "Reduction factor %10lf\n"
			    "Extend the search arround the mouse position\n"
			    "New extend - and + arround position %8d\n",&reduce,&w);
	      if (i == CANCEL) return D_O_K;
	    }
	}
    }
  old_x = x;
  old_y = y;
  oi->need_to_refresh |= PLOT_NEED_REFRESH;	
  return (refresh_image(imr, UNCHANGED));
}


int  do_find_fluo_maxima(void)
{
  O_i   *ois;
  imreg *imr;
  static int i, nmax = 256, with_amp = 1;//, dumpcsv = 1;
  im_ext_ar *iea = NULL;
  static double thres = 2000,  tolerance = 1000, sigma = 0.9;
  static float err_a = 0.15, err_b = 200;
	
  if(updating_menu_state != 0)	return D_O_K;		

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  
  i = win_scanf("Find bright spots in image\n"
		"Sigma of gaussian %12lf\n"
		"Specify the threshold in Z %12lf\n"
		"Specify the threshold in dZ %12lf\n"
		"Define the Signal error as er(Y) = a*y +b\n"
		"Enter a %8f and b %8f\n"
		"Maximum number of spots %8d \n"
		"generate amp ds %b\n"
		,&sigma,&thres, &tolerance,&err_a,&err_b,&nmax,&with_amp);
  if (i == CANCEL) return D_O_K;

  //  iea = find_maxima_positions(ois, NULL, thres, tolerance, nmax, NULL, 1,sigma, err_a, err_b);
  thres_s = thres;
  tolerance_s = tolerance;
  sigma_s = sigma;
  err_a_s = err_a;
  err_b_s = err_b;
  /*
  if (ois->im.data_type == IS_RGB16_PICTURE) 
    convert_maxima_search_info_in_GREEN(iea, ois, thres, tolerance);
  else 
    {
      convert_maxima_search_info_in_plot(iea, ois, thres, tolerance);
      ois->need_to_refresh = PLOT_NEED_REFRESH;	  
    }
  */
  return (refresh_image(imr, UNCHANGED));
}



int do_trackRG_barycentre(void)
{
  register int i, j;
  int onx, ony, data_type;
  O_i *ois = NULL;
  O_p *op;
  d_s *ds;
  double tmp, tmpx, tmpy;
  imreg *imr = NULL;
  static int channel = 0,ix = 0, iy = 0, iwx = 8, iwy = 8, dop=0;
  union pix *ps;  
  int jmin2;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine average the image intensity"
			   "of several lines of an image");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    return D_O_K;
  
  //  if (ois->im.data_type != IS_RGB_PICTURE )
  // return win_printf_OK("I need an RGB picture!");
  i = win_scanf("I am going to compute the barycenter of a light dot\n"
		"Specify the color channel %R->Red %r->Green %r->Blue\n"
		"Define the dot position in pixel x = %8d y = %8d\n"
		"Define averaging size in x %8d in y %8d\n"
		"Do not draw profiles->%R in X->%r in Y->%r\n"
		,&channel,&ix,&iy,&iwx,&iwy,&dop);
  if (i == CANCEL)	return D_O_K;

  onx = ois->im.nx;	ony = ois->im.ny;	data_type = ois->im.data_type;

  if(dop == 1)
    {
      op = create_and_attach_op_to_oi(ois, onx, onx, 0, IM_SAME_X_AXIS);
      if (op == NULL) return D_O_K;
      ds = op->dat[0];
    }
  if(dop == 2)
    {
      op = create_and_attach_op_to_oi(ois, ony, ony, 0, IM_SAME_Y_AXIS);
      if (op == NULL) return D_O_K;
      ds = op->dat[0];
    }
  if (data_type == IS_RGB_PICTURE)
    {
      tmp = tmpx = tmpy = 0;
      for (i = iy-iwy, ps = ois->im.pixel; i < iy+iwy; i++)
	{
	  if (i < 0 || i >= ony) continue;
	  for (j=ix-iwx; j< ix+iwx; j++)
	    {
	      if (j < 0 || j >= onx) continue;
	      if (channel == 0)	              
		{
		  tmp += ps[i].rgb[j].r;
		  tmpx += j*ps[i].rgb[j].r;
		  tmpy += i*ps[i].rgb[j].r;
		  if(dop == 1) {ds->yd[j] += ps[i].rgb[j].r; ds->xd[j] +=1;} 
		  if(dop == 2) {ds->yd[i] += ps[i].rgb[j].r; ds->xd[i] +=1;} 
		}
	      else if (channel == 1)	      
		{
		  tmp += ps[i].rgb[j].g;
		  tmpx += j*ps[i].rgb[j].g;
		  tmpy += i*ps[i].rgb[j].g;
		  if(dop == 1) {ds->yd[j] += ps[i].rgb[j].g; ds->xd[j] +=1;} 
		  if(dop == 2) {ds->yd[i] += ps[i].rgb[j].g; ds->xd[i] +=1;} 
		}
	      else if (channel == 2)	      
		{
		  tmp += ps[i].rgb[j].b;
		  tmpx += j*ps[i].rgb[j].b;
		  tmpy += i*ps[i].rgb[j].b;
		  if(dop == 1) {ds->yd[j] += ps[i].rgb[j].b; ds->xd[j] +=1;} 
		  if(dop == 2) {ds->yd[i] += ps[i].rgb[j].b; ds->xd[i] +=1;} 
		}
	      //tmp += ps[i].ch[3*j+channel];
	    }
	}
    }
  if (data_type == IS_RGB16_PICTURE)
    {
      tmp = tmpx = tmpy = 0;
      for (i = iy-iwy, ps = ois->im.pixel; i < iy+iwy; i++)
	{
	  if (i < 0 || i >= ony) continue;
	  for (j=ix-iwx; j< ix+iwx; j++)
	    {
	      if (j < 0 || j >= onx) continue;
	      if (channel == 0)	              
		{
		  tmp += ps[i].rgb16[j].r;
		  tmpx += j*ps[i].rgb16[j].r;
		  tmpy += i*ps[i].rgb16[j].r;
		  if(dop == 1) {ds->yd[j] += ps[i].rgb16[j].r; ds->xd[j] +=1;} 
		  if(dop == 2) {ds->yd[i] += ps[i].rgb16[j].r; ds->xd[i] +=1;} 
		}
	      else if (channel == 1)	      
		{
		  tmp += ps[i].rgb16[j].g;
		  tmpx += j*ps[i].rgb16[j].g;
		  tmpy += i*ps[i].rgb16[j].g;
		  if(dop == 1) {ds->yd[j] += ps[i].rgb16[j].g; ds->xd[j] +=1;} 
		  if(dop == 2) {ds->yd[i] += ps[i].rgb16[j].g; ds->xd[i] +=1;} 
		}
	      else if (channel == 2)	      
		{
		  tmp += ps[i].rgb16[j].b;
		  tmpx += j*ps[i].rgb16[j].b;
		  tmpy += i*ps[i].rgb16[j].b;
		  if(dop == 1) {ds->yd[j] += ps[i].rgb16[j].b; ds->xd[j] +=1;} 
		  if(dop == 2) {ds->yd[i] += ps[i].rgb16[j].b; ds->xd[i] +=1;} 
		}
	      //tmp += ps[i].ch[3*j+channel];
	    }
	}
    }
  else if (data_type == IS_CHAR_IMAGE)
    {
      tmp = tmpx = tmpy = 0;
      if (channel == 0) // RED        
	{
	  for (i = (iy-iwy) + (iy-iwy)%2, ps = ois->im.pixel; i < iy+iwy; i+= 2)
	    {
	      if (i < 0 || i >= ony) continue;
	      for (j=ix-iwx+(ix-iwx)%2; j< ix+iwx; j+=2)
		{
		  if (j < 0 || j >= onx) continue;
		  tmp += ps[i].ch[j];
		  tmpx += j*ps[i].ch[j] ;
		  tmpy += i*ps[i].ch[j];
		  if(dop == 1) {ds->yd[j] += ps[i].ch[j]; ds->xd[j] +=1;} 
		  if(dop == 2) {ds->yd[i] += ps[i].ch[j]; ds->xd[i] +=1;} 
		}
	    }
	}
      else if (channel == 1) // GREEN 
	{
	  for (i = iy-iwy, ps = ois->im.pixel; i < iy+iwy; i++)
	    {
	      if (i < 0 || i >= ony) continue;
	      if (i%2) jmin2 = ((ix-iwx)%2) ? ix-iwx + 1 : ix-iwx;
	      else jmin2 = ((ix-iwx)%2) ? ix-iwx : ix-iwx + 1;
	      for (j=jmin2; j< ix+iwx; j+=2)
		{
		  if (j < 0 || j >= onx) continue;
		  tmp += ps[i].ch[j];
		  tmpx += j*ps[i].ch[j] ;
		  tmpy += i*ps[i].ch[j];
		  if(dop == 1) {ds->yd[j] += ps[i].ch[j]; ds->xd[j] +=1;} 
		  if(dop == 2) {ds->yd[i] += ps[i].ch[j]; ds->xd[i] +=1;} 
		}
	    }
	}
      else if (channel == 2)  // BLUE 
	{
	  for (i = (iy-iwy)+ 1 - (iy-iwy)%2, ps = ois->im.pixel; i < iy+iwy; i+= 2)
	    {
	      if (i < 0 || i >= ony) continue;
	      for (j=ix-iwx+1-(ix-iwx)%2; j< ix+iwx; j+=2)
		{
		  if (j < 0 || j >= onx) continue;
		  tmp += ps[i].ch[j];
		  tmpx += j*ps[i].ch[j] ;
		  tmpy += i*ps[i].ch[j];
		  if(dop == 1) {ds->yd[j] += ps[i].ch[j]; ds->xd[j] +=1;} 
		  if(dop == 2) {ds->yd[i] += ps[i].ch[j]; ds->xd[i] +=1;} 
		}
	    }
	}
      //tmp += ps[i].ch[3*j+channel];
    }
  else if (data_type == IS_UINT_IMAGE)
    {
      tmp = tmpx = tmpy = 0;
      if (channel == 0) // RED        
	{
	  for (i = (iy-iwy) + (iy-iwy)%2, ps = ois->im.pixel; i < iy+iwy; i+= 2)
	    {
	      if (i < 0 || i >= ony) continue;
	      for (j=ix-iwx+(ix-iwx)%2; j< ix+iwx; j+=2)
		{
		  if (j < 0 || j >= onx) continue;
		  tmp += ps[i].ui[j];
		  tmpx += j*ps[i].ui[j] ;
		  tmpy += i*ps[i].ui[j];
		  if(dop == 1) {ds->yd[j] += ps[i].ui[j]; ds->xd[j] +=1;} 
		  if(dop == 2) {ds->yd[i] += ps[i].ui[j]; ds->xd[i] +=1;} 
		}
	    }
	}
      else if (channel == 1) // GREEN 
	{
	  for (i = iy-iwy, ps = ois->im.pixel; i < iy+iwy; i++)
	    {
	      if (i < 0 || i >= ony) continue;
	      if (i%2) jmin2 = ((ix-iwx)%2) ? ix-iwx + 1 : ix-iwx;
	      else jmin2 = ((ix-iwx)%2) ? ix-iwx : ix-iwx + 1;
	      for (j=jmin2; j< ix+iwx; j+=2)
		{
		  if (j < 0 || j >= onx) continue;
		  tmp += ps[i].ui[j];
		  tmpx += j*ps[i].ui[j] ;
		  tmpy += i*ps[i].ui[j];
		  if(dop == 1) {ds->yd[j] += ps[i].ui[j]; ds->xd[j] +=1;} 
		  if(dop == 2) {ds->yd[i] += ps[i].ui[j]; ds->xd[i] +=1;} 
		}
	    }
	}
      else if (channel == 2)  // BLUE 
	{
	  for (i = (iy-iwy)+ 1 - (iy-iwy)%2, ps = ois->im.pixel; i < iy+iwy; i+= 2)
	    {
	      if (i < 0 || i >= ony) continue;
	      for (j=ix-iwx+1-(ix-iwx)%2; j< ix+iwx; j+=2)
		{
		  if (j < 0 || j >= onx) continue;
		  tmp += ps[i].ui[j];
		  tmpx += j*ps[i].ui[j] ;
		  tmpy += i*ps[i].ui[j];
		  if(dop == 1) {ds->yd[j] += ps[i].ui[j]; ds->xd[j] +=1;} 
		  if(dop == 2) {ds->yd[i] += ps[i].ui[j]; ds->xd[i] +=1;} 
		}
	    }
	}
      //tmp += ps[i].ch[3*j+channel];
    }
  if (dop)
    {
      for (i = 0; i < ds->nx; i++) 
	{
	  ds->yd[i] /= (ds->xd[i]) ? ds->xd[i] : 1; 
	  ds->xd[i] = i;
	}
    }
  else return win_printf_OK("Wrong image format!");
  if (tmp > 0)
    {
      tmpx /= tmp;
      tmpy /= tmp;
    }

  ois->need_to_refresh = PLOT_NEED_REFRESH;	
  refresh_image(imr, UNCHANGED);
  return win_printf_OK("Spot at x %d->%g y %d->%g => amp = %g"
		       ,ix,tmpx,iy,tmpy,tmp);
}





int do_trackRG_average_along_y(void)
{
	register int i, j;
	int l_s, l_e;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average the image intensity"
			"of several lines of an image");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	return D_O_K;
	/* we grap the data that we need, the image and the screen region*/
	l_s = 0; l_e = ois->im.ny;
	/* we ask for the limit of averaging*/
	i = win_scanf("Average Between lines%d and %d",&l_s,&l_e);
	if (i == CANCEL)	return D_O_K;
	/* we create a plot to hold the profile*/
	op = create_one_plot(ois->im.nx,ois->im.nx,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0]; /* we grab the data set */
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	op->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;
	inherit_from_im_to_ds(ds, ois);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"trackRG averaged profile from line %d to %d",l_s, l_e);
	set_formated_string(&ds->treatement,"%s averaged profile from line %d to %d",l_s, l_e);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	for (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, ds->xd);
		for (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];
	}
	for (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)
	{
		ds->xd[i] = i;
		ds->yd[i] /= j;
	}
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}

O_i	*trackRG_image_multiply_by_a_scalar(O_i *ois, float factor)
{
	register int i, j;
	O_i *oid;
	float tmp;
	int onx, ony, data_type;
	union pix *ps, *pd;

	onx = ois->im.nx;	ony = ois->im.ny;	data_type = ois->im.data_type;
	oid =  create_one_image(onx, ony, data_type);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return NULL;
	}
	if (data_type == IS_CHAR_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_INT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].in[j];
				pd[i].in[j] = (tmp < -32768) ? -32768 :
					((tmp > 32767) ? 32767 : (short int)tmp);
			}
		}
	}
	else if (data_type == IS_UINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].ui[j];
				pd[i].ui[j] = (tmp < 0) ? 0 :
					((tmp > 65535) ? 65535 : (unsigned short int)tmp);
			}
		}
	}
	else if (data_type == IS_LINT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				tmp = factor * ps[i].li[j];
				pd[i].li[j] = (tmp < 0x10000000) ? 0x10000000 :
					((tmp > 0x7FFFFFFF) ? 0x7FFFFFFF : (int)tmp);
			}
		}
	}
	else if (data_type == IS_FLOAT_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[j] = factor * ps[i].fl[j];
			}
		}
	}
	else if (data_type == IS_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[j] = factor * ps[i].db[j];
			}
		}
	}
	else if (data_type == IS_COMPLEX_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].fl[2*j] = factor * ps[i].fl[2*j];
				pd[i].fl[2*j+1] = factor * ps[i].fl[2*j+1];
			}
		}
	}
	else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< onx; j++)
			{
				pd[i].db[2*j] = factor * ps[i].db[2*j];
				pd[i].db[2*j+1] = factor * ps[i].db[2*j+1];
			}
		}
	}
	else if (data_type == IS_RGB_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 3*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	else if (data_type == IS_RGBA_PICTURE)
	{
		for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
		{
			for (j=0; j< 4*onx; j++)
			{
				tmp = factor * ps[i].ch[j];
				pd[i].ch[j] = (tmp < 0) ? 0 :
					((tmp > 255) ? 255 : (unsigned char)tmp);
			}
		}
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	oid->im.win_flag = ois->im.win_flag;
	if (ois->title != NULL)	set_im_title(oid, "%s rescaled by %g",
		 ois->title,factor);
	set_formated_string(&oid->im.treatement,
		"Image %s rescaled by %g", ois->filename,factor);
	return oid;
}

int do_trackRG_image_multiply_by_a_scalar(void)
{
	O_i *ois, *oid;
	imreg *imr;
	int i;
	static float factor = 2.0;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine multiply the image intensity"
		"by a user defined scalar factor");
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		win_printf("Cannot find image");
		return D_O_K;
	}
	i = win_scanf("define the factor of mutiplication %f",&factor);
	if (i == CANCEL)	return D_O_K;
	oid = trackRG_image_multiply_by_a_scalar(ois,factor);
	if (oid == NULL)	return win_printf_OK("unable to create image!");
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}

//typedef float v8sf __attribute__ ((vector_size (32))); // vector of four single floats
union f8vector 
{
  __v8sf v;
  float f[8];
};
/*
_mm256_load_ps

Moves packed single-precision floating point values from aligned memory location to a destination vector. The corresponding Intel� AVX instruction is VMOVAPS.
Syntax

extern __m256 _mm256_load_ps(float const *a);
Arguments

*a
	

pointer to a memory location that can hold constant float32 values; the address must be 32-byte aligned
Description

Loads packed single-precision floating point values (float32 values) from the 256-bit aligned memory location pointed to by a, into a destination float32 vector, which is retured by the intrinsic.
Returns

A 256-bit vector with float32 values.

_mm256_broadcast_ss, _mm_broadcast_ss

Loads and broadcasts 256/128-bit scalar single-precision floating point values to a 256/128-bit destination operand. The corresponding Intel� AVX instruction is VBROADCASTSS.
Syntax

extern __m256 _mm256_broadcast_ss(float const *a);

extern __m128 _mm_broadcast_ss(float const *a);
Arguments

*a
	

pointer to a memory location that can hold constant 256-bit or 128-bit float32 values
Description

Loads scalar single-precision floating-point values from the specified address pointed to by a, and broadcasts it to elements in the destination vector.

The _m256_broadcast_ss intrinsic broadcasts the loaded values to all eight elements in the 256-bit destination vector.

The _mm_broadcast_ss intrinsic broadcasts the loaded values to all four elements in the 128-bit destination vector.
Returns

Result of the load and broadcast operation.



*/
union f8vector f8, fax, fay, f0, fx, fy, fxc, fyc, fy2, fr[64], fx2[64];


float *radial_non_pixel_square_image_sym_profile_in_array_avx(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay)
{
  register int i, j, k;
  union pix *ps;
  int onx, ony, ri, type, onx_8;
  float z = 0, p, p0 = 0, pr0 = 0, rmax2, tmp; // , z1, x, y
  int imin, imax, jmin, jmax;//, jmin_8, jmax_8;
  
  
  
  //for (i = 0; i < 2*r_max && i < 256; i++)	radial_dx[i] = 0;
  if (ois == NULL)	return NULL;
  if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
  if (rz == NULL)		return NULL;
  ps = ois->im.pixel;
  ony = ois->im.ny;	onx = ois->im.nx;
  type = ois->im.data_type;
  for (i = 0; i < 2*r_max; i++)	rz[i] = 0;
  rmax2 = r_max * r_max;
  //we define the ROI of image to analyze
  imin = (int)(yc - (r_max/ay));
  imax = (int)(0.5 + yc + (r_max/ay));	
  jmin = (int)(xc -  r_max/ax);
  jmax = (int)(0.5 + xc + r_max/ax);
  // we keep ROI in image
  imin = (imin < 0) ? 0 : imin;
  imax = (imax > ony) ? ony : imax;
  jmin = (jmin < 0) ? 0 : jmin;
  jmax = (jmax > onx) ? onx : jmax;

  onx_8 = (jmax - jmin) >> 3;
  onx_8 += ((8 * (jmax - jmin)) < (jmax - jmin)) ? 1 : 0; 


  //jmin_8 = jmin >> 3;
  //jmax_8 = 1+(jmax >> 3);
  //jmax_8 = (jmax_8 > onx_8) ? onx_8 : jmax_8;
  
  
  // we convert xc, yc, ax, ay in 256 v8sf
  fxc.v = _mm256_broadcast_ss(&xc);
  fyc.v = _mm256_broadcast_ss(&yc);
  fax.v = _mm256_broadcast_ss(&ax);
  fay.v = _mm256_broadcast_ss(&ay);
  tmp = 8;  f8.v = _mm256_broadcast_ss(&tmp);
  //r = fr[0].f;  //????

  f0.f[0] = 0; f0.f[1] = 1; f0.f[2] = 2; f0.f[3] = 3;  // pixel index 
  f0.f[4] = 4; f0.f[5] = 5; f0.f[6] = 6; f0.f[7] = 7;  // pixel index 
  tmp = jmin;   fx.v = _mm256_broadcast_ss(&tmp);
  fx.v = __builtin_ia32_addps256(fx.v, f0.v);
  fx.v = __builtin_ia32_subps256(fx.v, fxc.v);            // distance in pixel from center
  fx.v = __builtin_ia32_mulps256(fax.v, fx.v);           // rescaling in x
  f8.v = __builtin_ia32_mulps256(fax.v, f8.v);           // we prepare rescaled substraction
  for (j = 0; j < onx_8; j++)
    {
      fx2[j].v = __builtin_ia32_mulps256 (fx.v, fx.v);    // we compute X2
      fx.v = __builtin_ia32_addps256(fx.v, f8.v);         // we move index
    }
  // now the fx2 array contains (ax*(image_x_index - xc))^2 

  if (type == IS_INT_IMAGE)			
    {
      tmp = (float)imin;     f0.v = _mm256_broadcast_ss(&tmp); // starting y position
      fy.v = __builtin_ia32_subps256(f0.v, fyc.v);             // distance in pixel to yc
      fy.v = __builtin_ia32_mulps256(fay.v, fy.v);             // rescaled in y
      for (i = imin; i < imax; i++)
	{
	  fy2.v = __builtin_ia32_mulps256(fy.v, fy.v);       // we compute y2 
	  fy.v = __builtin_ia32_addps256(fy.v, fay.v);        // move by one rescaled pixel in y
	  tmp = rmax2 - fy2.f[0];
	  if (tmp < 0) continue;  // we skip r too big
	  for (k = 0; k < onx_8; k++)
	    {  // we compute all r values along the line
	      fr[k].v = __builtin_ia32_addps256(fy2.v, fx2[k].v);  // we compute x2 + y2
	      fr[k].v = __builtin_ia32_sqrtps256(fr[k].v);         // we get the distance
	      /*
	      fri[k].v = __mm256_ceil_ps(fr[k].v);
	      frp[k].v = __builtin_ia32_subps256(fr[k].v, fri[k].v);
	      tmp = 1; fr_p[k].v = _mm256_broadcast_ss(&tmp);
	      fr_p[k].v = __builtin_ia32_subps256(fr_p[k].v, frp[k].v);
	      */

	    }
	  for (j = jmin; j < jmax; j++)
	    {
	      tmp = fr[(j-jmin)>>3].f[(j-jmin)&0x07];
	      ri = (int)tmp;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].in[j];
	      p = tmp - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		}
	    }
	}
    }
  else if (type == IS_CHAR_IMAGE)			
    {
      tmp = (float)imin;     f0.v = _mm256_broadcast_ss(&tmp); // starting y position
      fy.v = __builtin_ia32_subps256(f0.v, fyc.v);             // distance in pixel to yc
      fy.v = __builtin_ia32_mulps256(fay.v, fy.v);             // rescaled in y
      for (i = imin; i < imax; i++)
	{
	  fy2.v = __builtin_ia32_mulps256(fy.v, fy.v);       // we compute y2 
	  fy.v = __builtin_ia32_addps256(fy.v, fay.v);        // move by one rescaled pixel in y
	  tmp = rmax2 - fy2.f[0];
	  if (tmp < 0) continue;  // we skip r too big
	  for (k = 0; k < onx_8; k++)
	    {  // we compute all r values along the line
	      fr[k].v = __builtin_ia32_addps256(fy2.v, fx2[k].v);  // we compute x2 + y2
	      fr[k].v = __builtin_ia32_sqrtps256(fr[k].v);         // we get the distance
	    }
	  for (j = jmin; j < jmax; j++)
	    {
	      tmp = fr[(j-jmin)>>3].f[(j-jmin)&0x07];
	      ri = (int)tmp;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].ch[j];
	      p = tmp - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		}
	    }
	}
    }
  else if (type == IS_UINT_IMAGE)			
    {
      tmp = (float)imin;     f0.v = _mm256_broadcast_ss(&tmp); // starting y position
      fy.v = __builtin_ia32_subps256(f0.v, fyc.v);             // distance in pixel to yc
      fy.v = __builtin_ia32_mulps256(fay.v, fy.v);             // rescaled in y
      for (i = imin; i < imax; i++)
	{
	  fy2.v = __builtin_ia32_mulps256(fy.v, fy.v);       // we compute y2 
	  fy.v = __builtin_ia32_addps256(fy.v, fay.v);        // move by one rescaled pixel in y
	  tmp = rmax2 - fy2.f[0];
	  if (tmp < 0) continue;  // we skip r too big
	  for (k = 0; k < onx_8; k++)
	    {  // we compute all r values along the line
	      fr[k].v = __builtin_ia32_addps256(fy2.v, fx2[k].v);  // we compute x2 + y2
	      fr[k].v = __builtin_ia32_sqrtps256(fr[k].v);         // we get the distance
	    }
	  for (j = jmin; j < jmax; j++)
	    {
	      tmp = fr[(j-jmin)>>3].f[(j-jmin)&0x07];
	      ri = (int)tmp;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].ui[j];
	      p = tmp - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		}
	    }
	}
    }
  for (i = 0; i < r_max; i++)
    {
      rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
      //radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
    }
  p0 = (pr0 == 0) ? 0 : p0/pr0;  
  for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i]; 
  for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i]; 
  if (rz[r_max] == 0) rz[r_max] = rz[r_max+1];
  rz[0] = p0;
  return rz;
}


/*	take a radial image profile of an image with a different scaling in 
 *	x and y, real x = i_image * ax,  y = i_image * ay
 *
 *
 *
 */


float *radial_non_pixel_square_image_sym_profile_in_array(float *rz, O_i *ois, float xc, float yc, int r_max, float ax, float ay)
{
  int i, j;
  union pix *ps;
  int onx, ony, ri, type;
  float z = 0, z1, x, y, r, p, p0 = 0, pr0 = 0;
  int imin, imax, jmin,jmax; //  xci, yci
  
  //for (i = 0; i < 2*r_max && i < 256; i++)	radial_dx[i] = 0;
  //for (i = 0; i < 4096; i++)      evanescent_black_level_histo[i] = 0;
  if (ois == NULL)	return NULL;
  if (rz == NULL)		rz = (float*)calloc(2*r_max,sizeof(float));
  if (rz == NULL)		return NULL;
  ps = ois->im.pixel;
  ony = ois->im.ny;	onx = ois->im.nx;
  type = ois->im.data_type;
  for (i = 0; i < 2*r_max; i++)	rz[i] = 0;
  //xci = (int)xc;
  //yci = (int)yc;
  
  imin = (int)(yc - (r_max/ay));
  imax = (int)(0.5 + yc + (r_max/ay));	
  jmin = (int)(xc -  r_max/ax);
  jmax = (int)(0.5 + xc + r_max/ax);
  imin = (imin < 0) ? 0 : imin;
  imax = (imax > ony) ? ony : imax;
  jmin = (jmin < 0) ? 0 : jmin;
  jmax = (jmax > onx) ? onx : jmax;	
  if (type == IS_COMPLEX_IMAGE)
    {  
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      if ( ois->im.mode == RE)
		z = ps[i].fl[2*j];
	      else if ( ois->im.mode == IM)
		z = ps[i].fl[2*j+1];
	      else if ( ois->im.mode == AMP)
		{
		  z = ps[i].fl[2*j];
		  z1 = ps[i].fl[2*j+1];
		  z = sqrt(z*z + z1*z1);
		}
	      else if ( ois->im.mode == AMP_2)
		{
		  z = ps[i].fl[2*j];
		  z1 = ps[i].fl[2*j+1];
		  z = z*z + z1*z1;
		}				
	      else if ( ois->im.mode == LOG_AMP)
		{
		  z = ps[i].fl[2*j];
		  z1 = ps[i].fl[2*j+1];
		  z = z*z + z1*z1;
		  z = (z > 0) ? log10(z) : -40.0;
		}				
	      else
		{
		  win_printf("Unknown mode for complex image");
		  return NULL;
		}
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  //radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  //radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  //radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  else if (type == IS_FLOAT_IMAGE) 		
    {
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = ps[i].fl[j];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  //radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  //radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  //radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  else if (type == IS_INT_IMAGE)			
    {
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].in[j];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  //radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  //radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  //radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  else if (type == IS_UINT_IMAGE)			
    {
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].ui[j];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  //radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  //radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  //radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  else if (type == IS_CHAR_IMAGE)			
    {
      for (i = imin; i < imax; i++)
	{
	  y = ay * ((float)i - yc);
	  y *= y;
	  for (j = jmin; j < jmax; j++)
	    {
	      x = ax * ((float)j - xc);
	      x *= x;
	      r = (float)sqrt(x + y);
	      ri = (int)r;
	      if (ri > r_max)	continue;	/* bug break;*/
	      z = (float)ps[i].ch[j];
	      p = r - ri;
	      if (ri == r_max)
		{
		  p0 += (1-p)*z;
		  pr0 += (1-p);
		  //radial_dx[r_max] -= 1-p;
		}
	      else 
		{
		  rz[ri] += (1-p)*z;
		  rz[ri+r_max] += (1-p);
		  //radial_dx[ri] += p;
		}
	      ri++;
	      if (ri < r_max)
		{
		  rz[ri] += p*z;
		  rz[ri+r_max] += p;
		  //radial_dx[ri] -= 1-p;
		}
	    }
	}
    }
  for (i = 0; i < r_max; i++)
    {
      rz[i] = (rz[i+r_max] == 0) ? 0 : rz[i]/rz[i+r_max];
      //      radial_dx[i] = (rz[i+r_max] == 0) ? 0 : radial_dx[i]/rz[i+r_max];
    }
  p0 = (pr0 == 0) ? 0 : p0/pr0;  
  for (i = 0; i < r_max; i++)		rz[i+r_max] = rz[i]; 
  for (i = 1; i < r_max; i++)		rz[r_max-i] = rz[r_max+i]; 
  rz[0] = p0;
  return rz;
}




int do_radial(void)
{
  register int i;
  imreg *imr;
  O_i *ois;
  O_p *op;
  d_s *ds;
  static float xc = 256 , yc = 256, ax = 1, ay = 1;
  static int r_max = 64;
  unsigned long u_micro;
  int t;
  
  if(updating_menu_state != 0)	return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  xc = ois->im.nx/2;
  yc = ois->im.ny/2;
  i = win_scanf("center xc %f yc %f r_{max} %dax %f ay %f",&xc,&yc,&r_max,&ax,&ay);
  if (i == CANCEL)	return OFF;
  
  
  op = create_and_attach_op_to_oi(ois, 2*r_max, 2*r_max, 0, 0);
  ds = op->dat[0];
  u_micro = my_uclock();
  for (i = 0; i < 100; i++)
    radial_non_pixel_square_image_sym_profile_in_array(ds->yd, ois, xc, yc, r_max, ax, ay);
  t = (int)(my_uclock() - u_micro);
  win_printf("dt = %g \\mu ms", 10000*((double)t/((int)MY_UCLOCKS_PER_SEC)));
  
  u_micro = my_uclock();
  for (i = 0; i < 100; i++)
    radial_non_pixel_square_image_sym_profile_in_array_avx(ds->yd, ois, xc, yc, r_max, ax, ay);
  t = (int)(my_uclock() - u_micro);
  win_printf("dt avx = %g \\mu ms", 10000*((double)t/((int)MY_UCLOCKS_PER_SEC)));
  
  for (i = 0; i < ds->nx; i++) ds->xd[i] = i;
  set_plot_title(op,"do radial fixed dt = %g \\mu ms", 
		 10000*((double)t/((int)MY_UCLOCKS_PER_SEC)));
  ds->source = Mystrdupre(ds->source,op->title);	
  broadcast_dialog_message(MSG_DRAW,0);
  (refresh_im_plot(imr, ois->n_op - 1));	
  
  return 0;
}


O_i *demosaicing_bayer_color(O_i *ois, O_i *oid)
{
  int type, Ok, nfi, nfd, onx, ony;
  int i, j, ii, jj, z, norm = 0, coef, bayer_c, c ,im;
  union pix *pd, *ps;

  if (ois == NULL) return NULL;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  onx = ois->im.nx; 
  ony = ois->im.ny;

  if (oid == NULL) 
    {
      if (ois->im.data_type == IS_CHAR_IMAGE) type = IS_RGB_PICTURE;
      else if (ois->im.data_type == IS_UINT_IMAGE) type = IS_RGB16_PICTURE;
      else return win_printf_ptr("Image type not supported");
      if (nfi < 2) oid = create_one_image(ois->im.nx, ois->im.ny, type);
      else         oid = create_one_movie(ois->im.nx, ois->im.ny, type, nfi);
    }
  else
    {
      Ok = (ois->im.nx == oid->im.nx) ? 1 : 0;
      if (Ok == 0) return win_printf_ptr("Destination image x size %d differs from the source one %d!"
					 ,oid->im.nx,ois->im.nx);
      Ok = (ois->im.ny == oid->im.ny) ? Ok : 0;
      if (Ok == 0) return win_printf_ptr("Destination image y size %d differs from the source one %d!"
					 ,oid->im.ny,ois->im.ny);
      nfd = abs(oid->im.n_f);
      nfd = (nfd == 0) ? 1 : nfd;
      Ok = (nfi == nfd) ? Ok : 0;
      if (Ok == 0) return win_printf_ptr("Destination image frame number %d differs from the source one %d!"
					 ,nfi,nfd);
      if (ois->im.data_type == IS_CHAR_IMAGE)
	{ 
	  Ok = (oid->im.data_type == IS_RGB_PICTURE || oid->im.data_type == IS_RGBA_PICTURE) ? Ok : 0;
	  if (Ok == 0) return win_printf_ptr("Destination image type is incompatible wth the source one");
	}
      else if (ois->im.data_type == IS_INT_IMAGE)
	{ 
	  Ok = (oid->im.data_type == IS_RGB16_PICTURE || oid->im.data_type == IS_RGBA16_PICTURE) ? Ok : 0;
	  if (Ok == 0) return win_printf_ptr("Destination image type is incompatible wth the source one");
	}
    }
  if (oid == NULL) return NULL;
  
  type = oid->im.data_type;
  if (type == IS_RGB_PICTURE)
    {
     for (im = 0; im < nfi; im++)
	{
	  switch_frame(ois,im);
	  switch_frame(oid,im);
	  pd = oid->im.pixel;
	  ps = ois->im.pixel;
	  for (i = 0; i < ony; i++)
	    {
	      for (j = 0; j < onx; j++)
		{
		  if (i%2 == 0 && j%2 == 0) bayer_c = 0; // red
		  else if (i%2 == 0 && j%2) bayer_c = 1; // green
		  else if (i%2 && j%2 == 0) bayer_c = 1; // green
		  else if (i%2 && j%2)      bayer_c = 2; // blue
		  for (c = 0; c < 3; c++)
		    {
		      if (c == bayer_c) 
			{   // we keep color
			  norm = 1;
			  z = ps[i].ch[j]; 
			}
		      else if (c == 1 && bayer_c != 1) // GREEN on RED and GREEN on BLUE  
			{
			  norm = z = 0;
			  for (jj = -2; jj <=  2; jj++)
			    {
			      if ((j+jj) >= 0 && (j+jj) < onx)
				{
				  coef = (abs(jj) == 2) ? 2 : -1;
				  norm += coef;
				  z +=  coef + ps[i].ch[j+jj]; 
				}
			    }
			  for (ii = -2; ii <=  2; ii++)
			    {
			      if ((i+ii) >= 0 && (i+ii) < ony)
				{
				  coef = (abs(ii) == 2) ? 2 : -1;
				  norm += coef;
				  z +=  coef + ps[i+ii].ch[j]; 
				}
			    }
			}
		      else if ((c == 0 && bayer_c == 2) || (c == 2 && bayer_c == 0)) 
			{  // RED on Blue and Blue on RED
			  z = 12 * ps[i].ch[j]; 
			  norm = 12;
			  for (jj = -2; jj <=  2; jj += 4)
			    {
			      if ((j+jj) >= 0 && (j+jj) < onx)
				{
				  coef = -3;
				  norm += coef;
				  z +=  coef + ps[i].ch[j+jj]; 
				}
			    }		
			  for (ii = -2; ii <=  2; ii += 4)
			    {
			      if ((i+ii) >= 0 && (i+ii) < ony)
				{
				  coef = -3;
				  norm += coef;
				  z +=  coef + ps[i+ii].ch[j]; 
				}
			    }		
			  for (ii = -1; ii <=  1; ii += 2)
			    {
			      for (jj = -1; jj <=  1; jj += 2)
				{
				  if ((j+jj) >= 0 && (j+jj) < onx && (i+ii) >= 0 && (i+ii) < ony)
				    {
				      coef = 4;
				      norm += coef;
				      z +=  coef + ps[i+ii].ch[j+jj]; 
				    }
				}	
			    }	
			}
		      else if ((c == 0 && bayer_c == 1 && i%2 == 0) || (c == 2 && bayer_c == 1 && i%2 == 1))
			{
			  z = 10 * ps[i].ch[j]; 
			  norm = 10;
			  for (ii = -2; ii <=  2; ii += 4)
			    {
			      if ((i+ii) >= 0 && (i+ii) < ony)
				{
				  coef = 1;
				  norm += coef;
				  z +=  coef + ps[i+ii].ch[j]; 
				}
			    }	
			  for (ii = -1; ii <=  1; ii += 2)
			    {
			      for (jj = -1; jj <=  1; jj += 2)
				{
				  if ((j+jj) >= 0 && (j+jj) < onx && (i+ii) >= 0 && (i+ii) < ony)
				    {
				      coef = -2;
				      norm += coef;
				      z +=  coef + ps[i+ii].ch[j+jj]; 
				    }
				}	
			    }	
			  for (jj = -2; jj <=  2; jj += 4)
			    {
			      if ((j+jj) >= 0 && (j+jj) < onx)
				{
				  coef = -2;
				  norm += coef;
				  z +=  coef + ps[i].ch[j+jj]; 
				}
			    }		
			  for (jj = -1; jj <=  1; jj += 2)
			    {
			      if ((j+jj) >= 0 && (j+jj) < onx)
				{
				  coef = 8;
				  norm += coef;
				  z +=  coef + ps[i].ch[j+jj]; 
				}
			    }		
			}
		      else if ((c == 0 && bayer_c == 1 && i%2 == 1) || (c == 2 && bayer_c == 1 && i%2 == 0))
			{
			  z = 10 * ps[i].ch[j]; 
			  norm = 10;
			  for (ii = -2; ii <=  2; ii += 4)
			    {
			      if ((i+ii) >= 0 && (i+ii) < ony)
				{
				  coef = -2;
				  norm += coef;
				  z +=  coef + ps[i+ii].ch[j]; 
				}
			    }	
			  for (ii = -1; ii <=  1; ii += 2)
			    {
			      if ((i+ii) >= 0 && (i+ii) < ony)
				{
				  coef = 8;
				  norm += coef;
				  z +=  coef + ps[i+ii].ch[j]; 
				}
			    }	

			  for (ii = -1; ii <=  1; ii += 2)
			    {
			      for (jj = -1; jj <=  1; jj += 2)
				{
				  if ((j+jj) >= 0 && (j+jj) < onx && (i+ii) >= 0 && (i+ii) < ony)
				    {
				      coef = -2;
				      norm += coef;
				      z +=  coef + ps[i+ii].ch[j+jj]; 
				    }
				}	
			    }
			  for (jj = -2; jj <=  2; jj += 4)
			    {
			      if ((j+jj) >= 0 && (j+jj) < onx)
				{
				  coef = 1;
				  norm += coef;
				  z +=  coef + ps[i].ch[j+jj]; 
				}
			    }		
			}
		      else 
			{
			  if (i < 2 && j < 2) win_printf("Untreated data i = %d j = %d c = %d"
							 "Bayer c %d",i,j,c,bayer_c);
			  z = 0;
			}
		      if (norm > 0) pd[i].ch[3*j+c] = z/norm;	
		    }
		}
	    }
	}
    }
  if (type == IS_RGB16_PICTURE)
    {
      for (im = 0; im < nfi; im++)
	{
	  switch_frame(ois,im);
	  switch_frame(oid,im);
	  pd = oid->im.pixel;
	  ps = ois->im.pixel;
	  for (i = 0; i < ony; i++)
	    {
	      for (j = 0; j < onx; j++)
		{
		  if (i%2 == 0 && j%2 == 0) bayer_c = 0; // red
		  else if (i%2 == 0 && j%2) bayer_c = 1; // green
		  else if (i%2 && j%2 == 0) bayer_c = 1; // green
		  else if (i%2 && j%2)      bayer_c = 2; // blue
		  for (c = 0; c < 3; c++)
		    {
		      if (c == bayer_c) 
			{   // we keep color
			  norm = 1;
			  z = ps[i].ui[j]; 
			}
		      else if (c == 1 && bayer_c != 1) // GREEN on RED and GREEN on BLUE  
			{
			  norm = z = 0;
			  for (jj = -2; jj <=  2; jj++)
			    {
			      if ((j+jj) >= 0 && (j+jj) < onx)
				{
				  coef = (abs(jj) == 2) ? 2 : -1;
				  norm += coef;
				  z +=  coef + ps[i].ui[j+jj]; 
				}
			    }
			  for (ii = -2; ii <=  2; ii++)
			    {
			      if ((i+ii) >= 0 && (i+ii) < ony)
				{
				  coef = (abs(ii) == 2) ? 2 : -1;
				  norm += coef;
				  z +=  coef + ps[i+ii].ui[j]; 
				}
			    }
			}
		      else if ((c == 0 && bayer_c == 2) || (c == 2 && bayer_c == 0)) 
			{  // RED on Blue and Blue on RED
			  z = 12 * ps[i].ui[j]; 
			  norm = 12;
			  for (jj = -2; jj <=  2; jj += 4)
			    {
			      if ((j+jj) >= 0 && (j+jj) < onx)
				{
				  coef = -3;
				  norm += coef;
				  z +=  coef + ps[i].ui[j+jj]; 
				}
			    }		
			  for (ii = -2; ii <=  2; ii += 4)
			    {
			      if ((i+ii) >= 0 && (i+ii) < ony)
				{
				  coef = -3;
				  norm += coef;
				  z +=  coef + ps[i+ii].ui[j]; 
				}
			    }		
			  for (ii = -1; ii <=  1; ii += 2)
			    {
			      for (jj = -1; jj <=  1; jj += 2)
				{
				  if ((j+jj) >= 0 && (j+jj) < onx && (i+ii) >= 0 && (i+ii) < ony)
				    {
				      coef = 4;
				      norm += coef;
				      z +=  coef + ps[i+ii].ui[j+jj]; 
				    }
				}	
			    }	
			}
		      else if ((c == 0 && bayer_c == 1 && i%2 == 0) || (c == 2 && bayer_c == 1 && i%2 == 1))
			{
			  z = 10 * ps[i].ui[j]; 
			  norm = 10;
			  for (ii = -2; ii <=  2; ii += 4)
			    {
			      if ((i+ii) >= 0 && (i+ii) < ony)
				{
				  coef = 1;
				  norm += coef;
				  z +=  coef + ps[i+ii].ui[j]; 
				}
			    }	
			  for (ii = -1; ii <=  1; ii += 2)
			    {
			      for (jj = -1; jj <=  1; jj += 2)
				{
				  if ((j+jj) >= 0 && (j+jj) < onx && (i+ii) >= 0 && (i+ii) < ony)
				    {
				      coef = -2;
				      norm += coef;
				      z +=  coef + ps[i+ii].ui[j+jj]; 
				    }
				}	
			    }	
			  for (jj = -2; jj <=  2; jj += 4)
			    {
			      if ((j+jj) >= 0 && (j+jj) < onx)
				{
				  coef = -2;
				  norm += coef;
				  z +=  coef + ps[i].ui[j+jj]; 
				}
			    }		
			  for (jj = -1; jj <=  1; jj += 2)
			    {
			      if ((j+jj) >= 0 && (j+jj) < onx)
				{
				  coef = 8;
				  norm += coef;
				  z +=  coef + ps[i].ui[j+jj]; 
				}
			    }		
			}
		      else if ((c == 0 && bayer_c == 1 && i%2 == 1) || (c == 2 && bayer_c == 1 && i%2 == 0))
			{
			  z = 10 * ps[i].ui[j]; 
			  norm = 10;
			  for (ii = -2; ii <=  2; ii += 4)
			    {
			      if ((i+ii) >= 0 && (i+ii) < ony)
				{
				  coef = -2;
				  norm += coef;
				  z +=  coef + ps[i+ii].ui[j]; 
				}
			    }	
			  for (ii = -1; ii <=  1; ii += 2)
			    {
			      if ((i+ii) >= 0 && (i+ii) < ony)
				{
				  coef = 8;
				  norm += coef;
				  z +=  coef + ps[i+ii].ui[j]; 
				}
			    }	

			  for (ii = -1; ii <=  1; ii += 2)
			    {
			      for (jj = -1; jj <=  1; jj += 2)
				{
				  if ((j+jj) >= 0 && (j+jj) < onx && (i+ii) >= 0 && (i+ii) < ony)
				    {
				      coef = -2;
				      norm += coef;
				      z +=  coef + ps[i+ii].ui[j+jj]; 
				    }
				}	
			    }
			  for (jj = -2; jj <=  2; jj += 4)
			    {
			      if ((j+jj) >= 0 && (j+jj) < onx)
				{
				  coef = 1;
				  norm += coef;
				  z +=  coef + ps[i].ui[j+jj]; 
				}
			    }		
			}
		      else 
			{
			  if (i < 2 && j < 2) win_printf("Untreated data i = %d j = %d c = %d"
							 "Bayer c %d",i,j,c,bayer_c);
			  z = 0;
			}
		      if (norm > 0) pd[i].ui[3*j+c] = z/norm;	
		    }
		}
	    }
	}
    }
  return oid;
}



O_i *demosaicing_bayer_color_div_2(O_i *ois, O_i *oid)
{
  int type, Ok, nfi, nfd, onx, ony;
  int i, j ,im;
  union pix *pd, *ps;

  if (ois == NULL) return NULL;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  onx = ois->im.nx; 
  ony = ois->im.ny;

  if (oid == NULL) 
    {
      if (ois->im.data_type == IS_CHAR_IMAGE) type = IS_RGB_PICTURE;
      else if (ois->im.data_type == IS_UINT_IMAGE) type = IS_RGB16_PICTURE;
      else return win_printf_ptr("Image type not supported");
      if (nfi < 2) oid = create_one_image(ois->im.nx/2, ois->im.ny/2, type);
      else         oid = create_one_movie(ois->im.nx/2, ois->im.ny/2, type, nfi);
    }
  else
    {
      Ok = (ois->im.nx == 2*oid->im.nx) ? 1 : 0;
      if (Ok == 0) return win_printf_ptr("Destination image x size %d differs from the source one %d!"
					 ,oid->im.nx,ois->im.nx);
      Ok = (ois->im.ny == 2*oid->im.ny) ? Ok : 0;
      if (Ok == 0) return win_printf_ptr("Destination image y size %d differs from the source one %d!"
					 ,oid->im.ny,ois->im.ny);
      nfd = abs(oid->im.n_f);
      nfd = (nfd == 0) ? 1 : nfd;
      Ok = (nfi == nfd) ? Ok : 0;
      if (Ok == 0) return win_printf_ptr("Destination image frame number %d differs from the source one %d!"
					 ,nfi,nfd);
      if (ois->im.data_type == IS_CHAR_IMAGE)
	{ 
	  Ok = (oid->im.data_type == IS_RGB_PICTURE || oid->im.data_type == IS_RGBA_PICTURE) ? Ok : 0;
	  if (Ok == 0) return win_printf_ptr("Destination image type is incompatible wth the source one");
	}
      else if (ois->im.data_type == IS_INT_IMAGE)
	{ 
	  Ok = (oid->im.data_type == IS_RGB16_PICTURE || oid->im.data_type == IS_RGBA16_PICTURE) ? Ok : 0;
	  if (Ok == 0) return win_printf_ptr("Destination image type is incompatible wth the source one");
	}
    }
  if (oid == NULL) return NULL;
  
  type = oid->im.data_type;
  if (type == IS_RGB_PICTURE)
    {
     for (im = 0; im < nfi; im++)
	{
	  switch_frame(ois,im);
	  switch_frame(oid,im);
	  pd = oid->im.pixel;
	  ps = ois->im.pixel;
	  for (i = 0; i < ony; i += 2)
	    {
	      for (j = 0; j < onx; j += 2)
		{
		  pd[i/2].rgb[j/2].r = ps[i].ch[j];
		  pd[i/2].rgb[j/2].b = ps[i+1].ch[j+1];
 		  pd[i/2].rgb[j/2].g = (ps[i].ch[j+1] + ps[i+1].ch[j])/2;
		}
	    }
	}
    }
  if (type == IS_RGB16_PICTURE)
    {
      for (im = 0; im < nfi; im++)
	{
	  switch_frame(ois,im);
	  switch_frame(oid,im);
	  pd = oid->im.pixel;
	  ps = ois->im.pixel;
	  for (i = 0; i < ony; i += 2)
	    {
	      for (j = 0; j < onx; j += 2)
		{
		  pd[i/2].rgb16[j/2].r = ps[i].ui[j];
		  pd[i/2].rgb16[j/2].b = ps[i+1].ui[j+1];
 		  pd[i/2].rgb16[j/2].g = (ps[i].ui[j+1] + ps[i+1].ui[j])/2;
		}
	    }
	}
    }
  return oid;
}



int do_demosaicing_bayer_color(void)
{
  O_i *ois = NULL, *oid = NULL;
  imreg *imr;

  if(updating_menu_state != 0)	return D_O_K;		

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  oid = demosaicing_bayer_color_div_2(ois, NULL);
  if (oid == NULL)	return D_O_K;
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  oid->need_to_refresh = ALL_NEED_REFRESH;			
  return (refresh_image(imr, imr->n_oi - 1));
}

int do_avx_test(void)
{
  if(updating_menu_state != 0)	return D_O_K;

  if (__builtin_cpu_supports ("sse2"))    win_printf("sse2 supported");
  if (__builtin_cpu_supports ("sse3"))    win_printf("sse3 supported");
  if (__builtin_cpu_supports ("sse4.1"))    win_printf("sse4.1 supported");
  if (__builtin_cpu_supports ("sse4.2"))    win_printf("sse4.2 supported");
  if (__builtin_cpu_supports ("avx"))    win_printf("avx supported");
  if (__builtin_cpu_supports ("avx2"))    win_printf("avx2 supported");


  return D_O_K;
}

MENU *trackRG_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Barycentre", do_trackRG_barycentre,NULL,0,NULL);
	add_item_to_menu(mn,"Find channel peaks", do_find_fluo_maxima,NULL,0,NULL);
	add_item_to_menu(mn,"average profile", do_trackRG_average_along_y,NULL,0,NULL);
	add_item_to_menu(mn,"image z rescaled", do_trackRG_image_multiply_by_a_scalar,NULL,0,NULL);
	add_item_to_menu(mn,"Check SSE", do_avx_test,NULL,0,NULL);
	add_item_to_menu(mn,"Radial profile", do_radial, NULL, 0, NULL);
	add_item_to_menu(mn,"raw => RGB", do_demosaicing_bayer_color, NULL, 0, NULL);


	return mn;
}

int	trackRG_main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  add_image_treat_menu_item ( "trackRG", NULL, trackRG_image_menu(), 0, NULL);
  return D_O_K;
}

int	trackRG_unload(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  remove_item_to_menu(image_treat_menu, "trackRG", NULL, NULL);
  return D_O_K;
}
#endif

