#ifndef _FOCUS_C_
#define _FOCUS_C_


# include "ctype.h"
# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"

# include "xv_plugins.h"	// for inclusions of libraries that control dynamic loading of libraries.
# include "Pico_cfg.h"

# include "focus.h"



int    set_focus_device(Focus_param *f_p)
{
  int ret = 0, i;
  char *argvp[2];
  void*	hinstLib;
  char pl_name[256];

  //if (strncmp("Dummy",f_p->Focus_driver,6) == 0)
  //  {
  sprintf(pl_name,"%s_focus",f_p->Focus_driver);
  for (i=0; pl_name[i]; i++)
       pl_name[i] = tolower(pl_name[i]);

  //argvp[0] = "dummy_focus";
  argvp[0] = pl_name;
      argvp[1] = NULL;
      load_plugin(1, argvp);
      hinstLib = grep_plug_ins_hinstLib(argvp[0]);
      if (hinstLib == NULL)
	return win_printf_OK("Could not find plugins %s",argvp[0]);
      read_Z_value = (int(*)(void))GetProcAddress(hinstLib, "_read_Z_value");
      read_Z_value_accurate_in_micron = (float (*)(void))GetProcAddress(hinstLib,"_read_Z_value_accurate_in_micron");
      read_Z_value_OK = (float (*)(int *error))GetProcAddress(hinstLib,"_read_Z_value_OK"); 
      read_last_Z_value = (float(*)(void))GetProcAddress(hinstLib, "_read_last_Z_value");
      set_Z_value = (int (*)(float z))GetProcAddress(hinstLib,"_set_Z_value");
      set_Z_obj_accurate = (int	(*)(float zstart))GetProcAddress(hinstLib,"_set_Z_obj_accurate");
      set_Z_value_OK = (int (*)(float z))GetProcAddress(hinstLib,"_set_Z_value_OK");
      set_Z_step = (int (*)(float z))GetProcAddress(hinstLib,"_set_Z_step");
      inc_Z_value = (int (*)(int step))GetProcAddress(hinstLib,"_inc_Z_value");
      small_inc_Z_value = (int (*)(int up))GetProcAddress(hinstLib,"_small_inc_Z_value");
      big_inc_Z_value = (int (*)(int up))GetProcAddress(hinstLib,"_big_inc_Z_value");
      //win_printf_OK("Focus plugins loaded");
      //  }
      //else
      // { 
      //win_printf("Unknowng focusing device");
      //return 1;
      //}
  return ret;
}


# endif /* _ZOBJ_C_ */




