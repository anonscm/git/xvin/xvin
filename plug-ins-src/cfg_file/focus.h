#ifndef _FOCUS_H_
#define _FOCUS_H_

# include "Pico_cfg.h"

#ifdef _FOCUS_C_

//float   *focus_position = NULL, *focus_Z_step = NULL;

int	(*what_is_objective_device)(void);
char	*(*describe_objective_device)(void);

int 	(*read_Z_value)(void);
/* this function reads the Z position of the objective it return an integer in tenth of 
microns example 235 corresponds to 23.5 microns */

float 	(*read_Z_value_accurate_in_micron)(void);
/* this function reads the Z position of the objective it return a float in
microns. This function is replacing read_Z_value() */

float 	(*read_Z_value_OK)(int *error); 
/* this function reads the Z position of the objective it return a float in
microns an error flag is provided. This function is replacing read_Z_value() */


float 	(*read_last_Z_value)(void); 
/* this function reads the last Z position of the objective it return a float in
microns it just grab the last position and return very fast, it does not ask physically for the position */



/* setting the objective position */

int 	(*set_Z_value)(float z);
/* 	move the objective to the position z in micron, the return value corresponds to an error
flag : 0 -> no error , RS232_ERROR	in case of rs232 error */

int	(*set_Z_obj_accurate)(float zstart);
/* 	move the objective to the position z in micron with improved accuracy, the return value 
corresponds to an error flag : 0 -> no error , RS232_ERROR	in case of rs232 error */

int 	(*set_Z_value_OK)(float z);
/* 	move the objective to the position z in micron, the return value corresponds to an error
flag : 0 -> no error , RS232_ERROR	in case of rs232 error */


/* setting the objective position by quatized step (for stepper motors) */


int 	(*set_Z_step)(float z);
/* define the stepping size to move the objective */

int 	(*inc_Z_value)(int step);
/* move the objective by a certain number of steps. The stepping size is defined 
by set_Z_step(float z) */

/* functions used to move the objective with arrows on the keyboard */

int 	(*small_inc_Z_value)(int up);

int 	(*big_inc_Z_value)(int up);

/* function available on the LEICA only */

//acreg   *(*z_objective_menu)(void);
# else
//PXV_VAR(float*, focus_position); 
//PXV_VAR(float*, focus_Z_step); 

PXV_FUNCPTR(int, what_is_objective_device,(void));
PXV_FUNCPTR(char*, describe_objective_device,(void));
PXV_FUNCPTR(int, read_Z_value,(void));
PXV_FUNCPTR(float, read_Z_value_accurate_in_micron,(void));
PXV_FUNCPTR(float, read_Z_value_OK, (int *error));
PXV_FUNCPTR(int, set_Z_value, (float z));
PXV_FUNCPTR(int, set_Z_obj_accurate, (float zstart));
PXV_FUNCPTR(int, set_Z_value_OK, (float z));
PXV_FUNCPTR(int, set_Z_step, (float z));
PXV_FUNCPTR(int, inc_Z_value, (int step));
PXV_FUNCPTR(int, small_inc_Z_value, (int up));
PXV_FUNCPTR(int, big_inc_Z_value, (int up));


# endif

PXV_FUNC(int, set_focus_device, (Focus_param *f_p));


# endif
