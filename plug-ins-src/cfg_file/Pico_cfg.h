#ifndef _PICO_CFG_H_
#define _PICO_CFG_H_

#define OIL_IMMERSION 2
#define WATER_IMMERSION 1
#define AIR_IMMERSION 0

#define COMMERCIAL 0
#define HOME_MADE 1
// Plutot faire une structure de structures
typedef struct _micro_param {
    char* config_microscope_time;
    char* microscope_user_name;         /* the name of the microscope */
    char* microscope_manufacturer_name; /* the name of the microscope */
    int microscope_type;                /* COMMERCIAL or HOME_MADE      */
    char* PicoTwist_model;
    float zoom_factor;
    float zoom_min, zoom_max;
    float field_factor;
    float microscope_factor;
    float imaging_lens_focal_distance_in_mm;
    int LED_wavelength;
    // time_t config_microscope_time_t;
    // float max_LED_current;
    // float LED_current;
    // char *junk;
} Micro_param;

typedef struct _camera_param {
    char* config_camera_time;
    char* camera_manufacturer;
    char* camera_model;
    float camera_frequency_in_Hz;
    float pixel_clock;
    char* camera_config_file;
    char* camera_software_program;
    float x_pixel_2_microns;
    float y_pixel_2_microns;
    float pixel_h_in_microns;
    float pixel_w_in_microns;
    // time_t config_camera_time_t;
    int nb_pxl_x;
    int nb_pxl_y;
    int nb_bits;
    int interlace;
    float time_opening_factor;
    float shutter_time_us;
} Camera_param;

typedef struct _obj_param {
    char* config_objective_time;
    char* objective_manufacturer;
    float objective_magnification;
    int immersion_type;
    float objective_numerical_aperture;
    float immersion_index;
    float buffer_index;
    // time_t config_obj_time_t;
} Obj_param;

typedef struct _focus_param {
    char* config_focus_time;
    int Focus_is_piezzo;
    float Focus_max_expansion;
    char* Focus_driver;
    int Focus_direction;
    char* Focus_model;
    //  char *plugin_name;
    // int nb_bits;
    // float slew_rate;
    // float rising_time;
} Focus_param;

typedef struct _rotation_motor_param {
    char* config_rotation_motor_time;
    char* rotation_motor_manufacturer;
    char* rotation_motor_model;
    char* rotation_motor_controller;
    float rotation_motor_position;
    float P_rotation_motor;
    float I_rotation_motor;
    float D_rotation_motor;
    float rotation_max_velocity;
    float rotation_velocity;
    float gear_ratio;
    int nb_pulses_per_turn;
    // int direction;

} Rotation_Motor_param;

typedef struct _translation_motor_param {
    char* config_translation_motor_time;

    char* translation_motor_manufacturer;
    char* translation_motor_model;
    char* translation_motor_controller;
    float translation_motor_position;
    float P_translation_motor;
    float I_translation_motor;
    float D_translation_motor;
    float translation_max_velocity;
    float translation_velocity;
    float translation_max_pos;
    int translation_motor_inverted;
    float Inductive_sensor_servo_voltage;
    float zmag_ref_for_inductive_test;
    float Vcap_at_zmag_ref_test;
    float Zmag_vs_Vcap_poly_a0;
    float Zmag_vs_Vcap_poly_a1;
    float Zmag_vs_Vcap_poly_a2;
    float last_saved_z_position;
    int nb_pulses_per_mm;
    float zmag_offset;
    float motor_range;
    float motor_backslash;
    int motor_serial_nb;
    int ref_limit_switch;
    // int direction;
#ifdef ZMAG_FACTOR
    float zmag_factor; //tv 09/03/2017 : when ratio between motor translation and magnet translation
#endif
} Translation_Motor_param;

typedef struct _serial_param {
    char* config_serial_time;
    char* serial_port_str;
    int serial_port;
    int serial_BaudRate;
    int serial_ByteSize;
    int serial_StopBits;
    int serial_Parity;
    int serial_fDtrControl;
    int serial_fRtsControl;
    char* firmware_version;
    int rs232_started_fine;
} Serial_param;

typedef struct _experiment_parameter {
    char* project;
    char* user_firstname;
    char* user_lastname;
    char* experiment_set;
    char* experiment_name;
    char* experiment_filename;
    char* experiment_type; // Ramp, testing, opening assey, closing assay, opening+closing, rotation
                           // assey
    int reagents_name_size;
    char** reagents_name;
    float* reagents_concentration;
    char* buffer_name;
    char* molecule_name; // hairpin name, mix name, molecule name
    char* linker_name;
    char* config_expt_time;
    //    char *protein_in_use;
    float bead_size;
    float molecule_extension;
    char *name_template;
    int scan_number;
    int bead_number;
} Expt_param;

// the force is expected to be F = fo exp -(a1 * zmag + a2 * zmag * zmag + ...+ ai * zmag ^ i)
// the development holds for i < nd
// the six indexes are for different beads
// 0 -> MyOne
// 1 -> M280
// 2 -> M450

typedef struct _Magnet_param {
    char* config_magnet_time;
    char* magnet_name;
    char* manufacturer;
    int series_nb;
    float gap;
    float zmag_contact_sample;
    int nb; // number of beads type with known parameters
    int nd[6];
    float f0[6];
    float a1[6];
    float a2[6];
    float a3[6];
    float a4[6];
} Magnet_param;

typedef struct _Bead_param {
    char* config_bead_time;
    char* bead_name;
    char* manufacturer;
    float radius;
    float low_field_suceptibility;
    float saturating_magnetization;
    float saturating_field;
} Bead_param;

typedef struct _DNA_molecule {
    char* config_molecule_time;
    char* molecule_name;
    float ssDNA_molecule_extension;
    float dsDNA_molecule_extension;
    float dsxi;
    float nick_proba;
} DNA_molecule;

typedef struct _Sdi_2_param {
  float SDI_2_calibration;
  float photons_per_level;
} Sdi_2_param;

typedef struct _sdi_param {

  //calibration parameters, the same for all beads when sdi properly aligned (only in this case)
  float v_1;  //for z&x : group velocity in pix.µm-1
  float v_0;  //for z : phase velocity of z in rad.µm-1
  float v_x;  //for x : phase velocity of z in rad.µm-1
  float nu0;  //frequency of the fringes (in pix ^-1)
  //design parameters (slits & blades)
  float x1;//position of low angle (mm, in FP)
  float x2;//position of high angle
  float wx;//width of slits
  float wy;//height of slits
  float n_index;//refractive index

  //algo parameters
  int x_roi_size_zx;//size of profiles along x axis of camera for x/z tracking
  int y_roi_size_zx;//how many lines are averaged to get the profiles
  int x_window_order;
  int x_window_fwhm;
  int dist_half_beads;//distance (along y ) between the fringes of one bead
  int dc_width;//width of BF to remove
  float nu_threshold;
  int x_roi_size_y;
  int y_roi_size_y;
  int y_window_fwhm;
  int y_window_order;

  //bead diagnostic parameters for quality and functions
  //quality : some threshold about contrast and intensity max + obvious crazy points
  float contrast_min_limit;
  float I_min_limit;
  float I_max_limit;
  float z_min_limit;
  float z_max_limit;
  float y_min_limit;
  float y_max_limit;
  float x_min_limit;
  float x_max_limit;
  int weird_count_limit;
  //parameters for diagnostic of the function of the bead : ref bead/opening hairpin etc
  float std_threshold_x;
  float std_threshold_y;
  float std_threshold_z;
  float z_expected_range;
  float z_range_tol;

  //parameters for detection of beads
  //first detection of fringes
  int guess_dx; //rough expected half minimal distance between fringes (pixel)
  int guess_dy; //rough expected half minimal distance between y profiles (pixel unit)
  int intensity_threshold_detect; //threshold for intensity (int, acting on pixel)
  float intensity_min_filter; //to filter fringes with intensity threshold
  float intensity_max_filter;
  float contrast_threshold_filter;

  //second detection of beads with correlation of browian motion along y
  float correlation_min;//threshold to say that 2 y profiles comes from the same bead
  int dist_y_min;
  int dist_y_max;
  int dist_x_max;
  int verbose;
  float zmag_y_corr;//going to a quite low force
  int y_corr_n_points;

  //parameters for xyz jobs on beads : zeros of mol, counting opening hp...
  int xyz_n_points;
  float z_mag_close;
  float z_mag_open;
  float z_mag_lf_l0;
  int zmag_cycle_period; //in frame

} Sdi_param;

typedef struct _pico_parameter {
    Micro_param micro_param;
    Camera_param camera_param;
    Obj_param obj_param;
    Focus_param focus_param;
    Translation_Motor_param translation_motor_param;
    Rotation_Motor_param rotation_motor_param;
    Serial_param serial_param;
    Expt_param expt_param;
    Magnet_param magnet_param;
    DNA_molecule dna_molecule;
    Bead_param bead_param;
#ifdef SDI_VERSION
	Sdi_param sdi_param;
#endif
#ifdef SDI_VERSION_2
Sdi_2_param SDI_2_param;
#endif
} Pico_parameter;

#define YES 1

#ifdef _XVCFG_C_
char pico_config_filename[512] = "Unknown.cfg";
#endif
#ifndef _XVCFG_C_
PXV_VAR(Pico_parameter, Pico_param);
PXV_VAR(char, pico_config_filename[512]);
#endif

PXV_FUNC(int, XVcfg_main, (void));
PXV_FUNC(int, preload_config_file, (void));
PXV_FUNC(int, load_Pico_config, (void));

PXV_FUNC(int, write_Pico_config_file, (void));
PXV_FUNC(int, Open_Pico_config_file, (void));
PXV_FUNC(int, Close_Pico_config_file, (void));
PXV_FUNC(MENU*, config_file_menu, (void));

//tv 07 2017
PXV_FUNC(Sdi_param *, duplicate_pico_Sdi_param, (Sdi_param *pico_in, Sdi_param *pico_out));
PXV_FUNC(Micro_param*, duplicate_pico_Micro_param, (Micro_param * pico_in, Micro_param* pico_out));
PXV_FUNC(
    Camera_param*, duplicate_pico_Camera_param, (Camera_param * pico_in, Camera_param* pico_out));
PXV_FUNC(Obj_param*, duplicate_pico_Obj_param, (Obj_param * pico_in, Obj_param* pico_out));
PXV_FUNC(Focus_param*, duplicate_pico_Focus_param, (Focus_param * pico_in, Focus_param* pico_out));
PXV_FUNC(Rotation_Motor_param*, duplicate_pico_Rotation_Motor_param,
    (Rotation_Motor_param * pico_in, Rotation_Motor_param* pico_out));
PXV_FUNC(Translation_Motor_param*, duplicate_pico_Translation_Motor_param,
    (Translation_Motor_param * pico_in, Translation_Motor_param* pico_out));
PXV_FUNC(
    Serial_param*, duplicate_pico_Serial_param, (Serial_param * pico_in, Serial_param* pico_out));
PXV_FUNC(Expt_param*, duplicate_pico_Expt_param, (Expt_param * pico_in, Expt_param* pico_out));
PXV_FUNC(
    Magnet_param*, duplicate_pico_Magnet_param, (Magnet_param * pico_in, Magnet_param* pico_out));
PXV_FUNC(
    DNA_molecule*, duplicate_pico_DNA_molecule, (DNA_molecule * pico_in, DNA_molecule* pico_out));
PXV_FUNC(Bead_param*, duplicate_pico_Bead_param, (Bead_param * pico_in, Bead_param* pico_out));

PXV_FUNC(Pico_parameter*, duplicate_Pico_parameter,
    (Pico_parameter * pico_in, Pico_parameter* pico_out));

//tv 03 2017
PXV_FUNC(int, set_pico_config_file_Sdi_parameters, (void));
PXV_FUNC(int, set_pico_config_file_SDI_2_parameters, (void));
PXV_FUNC(int, set_pico_config_file_Micro_parameters,(void));
PXV_FUNC(int, set_pico_config_file_Camera_parameters,(void));
PXV_FUNC(int, set_pico_config_file_Obj_parameters,(void));
PXV_FUNC(int, set_pico_config_file_Focus_parameters,(void));
PXV_FUNC(int, set_pico_config_file_Rotation_Motor_parameters,(void));
PXV_FUNC(int, set_pico_config_file_Translation_Motor_parameters,(void));
PXV_FUNC(int, set_pico_config_file_Serial_parameters,(void));
PXV_FUNC(int, set_pico_config_file_Expt_parameters,(void));
PXV_FUNC(int, set_pico_config_file_Magnet_parameters,(void));
PXV_FUNC(int, set_pico_config_file_DNA_Molecule,(void));
PXV_FUNC(int, set_pico_config_file_Bead_param,(void));
PXV_FUNC(int, set_pico_config_file_all_parameters,(void));

PXV_FUNC(int, state_is_pico, (void));
PXV_FUNC(int, switch_from_pico_serial_to_dummy, (int is_pico));
PXV_FUNC(int, change_serial_config_new, (void));
PXV_FUNC(int, change_expt_config,(void));
PXV_FUNC(char *, generate_filename, (Pico_parameter *params, const char *format, int size));
PXV_FUNC(char *, print_html_pico_expt_params, (Pico_parameter *params));
#endif
