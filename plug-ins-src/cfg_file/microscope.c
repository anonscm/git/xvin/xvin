# ifndef _MICROSCOPE_C_
# define _MICROSCOPE_C_


# include "allegro.h"
#ifdef XV_WIN32
# include "winalleg.h"
#endif
# include "xvin.h"


# define BUILDING_PLUGINS_DLL
#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)

# include "Pico_cfg.h"
# include "microscope.h"



float	compute_y_microscope_scaling_new(Obj_param *obj, Micro_param *mic, Camera_param *cam)
{
	float fy;
	
	fy = mic->microscope_factor * cam->pixel_h_in_microns; 
	fy /= (mic->field_factor != 0) ?  mic->field_factor : 1;
	fy /= ((float)obj->objective_magnification * mic->zoom_factor);
	fy *= ((float)175)/mic->imaging_lens_focal_distance_in_mm;
	return fy;
}


float	compute_x_microscope_scaling_new(Obj_param *obj, Micro_param *mic, Camera_param *cam)
{
	float fy;
	
	fy = mic->microscope_factor * cam->pixel_w_in_microns; 
	fy /= (mic->field_factor != 0) ?  mic->field_factor : 1;
	fy /= ((float)obj->objective_magnification * mic->zoom_factor);
	fy *= ((float)175)/mic->imaging_lens_focal_distance_in_mm;
	return fy;
}

int def_oi_scaling(O_i *oi)
{
  float fy, fx;
  if (oi == NULL) return -1;
  while (oi->n_xu > 1)
    remove_from_one_image(oi, IS_X_UNIT_SET, (void*)oi->xu[oi->n_xu-1]);		
  while (oi->n_yu > 1)
    remove_from_one_image(oi, IS_Y_UNIT_SET, (void*)oi->yu[oi->n_yu-1]);		
  fy = compute_y_microscope_scaling_new(&(Pico_param.obj_param), 
					&(Pico_param.micro_param), &(Pico_param.camera_param));
  fx = compute_x_microscope_scaling_new(&(Pico_param.obj_param), 
					&(Pico_param.micro_param), &(Pico_param.camera_param));
  create_attach_select_x_un_to_oi(oi, IS_METER, 0, fx, -6, 0,"\\mu m");
  create_attach_select_y_un_to_oi(oi, IS_METER, 0, fy, -6, 0, "\\mu m");
  return 0;
}


# endif /* _MICROSCOPE_C_ */
