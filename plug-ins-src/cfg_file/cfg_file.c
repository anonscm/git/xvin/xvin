/*
 *    Plug-in program for picotwist config file
 *
 *    V. Croquette &JF Allemand
 */

#include "xvin.h"
#ifndef _PICO_CFG_C_
#define _PICO_CFG_C_

#ifndef _XVCFG_C_
#define _XVCFG_C_

#include "allegro.h"
#ifdef XV_WIN32
#include "winalleg.h"
#endif
#include <locale.h>
/* If you include other plug-ins header do it here*/

/* But not below this define */

#define BUILDING_PLUGINS_DLL
#include "Pico_cfg.h"
#include <glib.h>
#undef _PXV_DLL
#define _PXV_DLL __declspec(dllexport)

Pico_parameter Pico_param = { 0 };

char cfg_warning[8192] = {0};

const char *retrieve_pico_config_filename(void)
{
#ifdef GAME
    snprintf(pico_config_filename, 256, "Pico.cfg");
#elif defined(MOVIE)
    snprintf(pico_config_filename, 256, "Movie.cfg");
#elif defined(PIAS)
    snprintf(pico_config_filename, 256, "PlayItAgainSam.cfg");
#elif defined(PLAYITSAM)
    snprintf(pico_config_filename, 256, "PlayItSam.cfg");
#elif defined(PIFOC_DAS1602)
    snprintf(pico_config_filename, 256, "PicoTS.cfg");
#else
    snprintf(pico_config_filename, 256, "Picotwist.cfg");
#endif
    return pico_config_filename;
}
#ifdef XV_GTK_DIALOG
#include "form_builder.h"
#endif

int preload_config_file(void)
{
    char file[1024] = { 0 };
    FILE *fp;
    setlocale(LC_ALL, "C");
    retrieve_pico_config_filename();
    snprintf(file, sizeof(file), "%s%s", config_dir, pico_config_filename);
    fp = fopen(file, "r");

    if (fp)
    {
        set_config_file(file);
    }

    return D_O_K;
}

int Open_Pico_config_file(void)
{
    char file[1024], filebk[1024];
    unsigned char uch;
    int i;
    FILE *fp, *fpk;
    setlocale(LC_ALL, "C");
    retrieve_pico_config_filename();
    snprintf(file, sizeof(file), "%s%s", config_dir, pico_config_filename);
    replace_extension(filebk, file, "bkc", sizeof(filebk));
    fp = fopen(file, "r");

    if (fp == NULL)
    {
        fpk = fopen(filebk, "rb");

        if (fpk == NULL)
        {
            i = win_printf(
                    "Program configuration file:\n%s\n"
                    "is missing ! all your instrument parameters depend upon this file.\n"
                    "This occurs when you acquisition program crashes while this file was opened.\n"
                    "We apologize for the inconvenience.\n"
                    "To prevent this problem you should copy a running version of your configuration\n"
                    "file to a back up configuration file named:\n%s\n"
                    "Contrary to the configuration file which has to be written, this file will only\n"
                    "be read from the acquisition program to regenerate the working configuration "
                    "file\n"
                    "thus this backup file should never be lost but is not updated only manually.\n"
                    "This backup configuration file does not exist in your system.\n"
                    "We suggest that you generate this backup configuration file by extracting it\n"
                    "from your last TRK file using PlayItAgainSam program \n"
                    " + menu treatment => record => extrac last configuration file and save it as\n"
                    "%s\n"
                    "We recommend to stop the acquisition program by pressing OK to avoid using\n"
                    "eronous parameters which may crashes your sample.",
                    backslash_to_slash(file), backslash_to_slash(filebk), backslash_to_slash(filebk));

            if (i != WIN_CANCEL)
            {
                exit(0);
            }
        }
        else
        {
            i = win_printf(
                    "Program configuration file:\n%s\n"
                    "is missing ! all your instrument parameters depend upon this file.\n"
                    "This occurs when you acquisition program crashes while this file was opened.\n"
                    "We apologize for the inconvenience.\n"
                    "You have a backup configuration file available\n%s\n"
                    "This file contains a valid configuration but automatic file index and\n"
                    "non-essential user data may be outdated\n"
                    "By pressing Ok you will generate a new configuration file copied from this\n"
                    "backup file. The program will stop, start it again to recover its normal\n"
                    "operation mode. If you fill that it is better to update your backup "
                    "configuration\n"
                    "file prior restarting the acquisition press WIN_CANCEL\n"
                    "To update your backup configuration file follow this procedure:\n"
                    "You can extract it from your last TRK file using PlayItAgainSam program \n"
                    " + menu treatment => record => extrac last configuration file and save it as\n"
                    "%s\n",
                    backslash_to_slash(file), backslash_to_slash(filebk), backslash_to_slash(filebk));

            if (i == WIN_CANCEL)
            {
                exit(0);
            }

            fp = fopen(file, "wb");

            if (fp == NULL)
            {
                win_printf(
                    "I cannot create the configuration file:\n%s!\n", backslash_to_slash(file));
                exit(0);
            }

            for (i = fread(&uch, sizeof(unsigned char), 1, fpk); i == 1;
                    i = fread(&uch, sizeof(unsigned char), 1, fpk))
            {
                fwrite(&uch, sizeof(unsigned char), 1, fp);
            }

            fclose(fp);
            win_printf("Configuration File:\n%s\nregenerated from:\n%s\n"
                       "After the program exits restart it to recover normal operation\n",
                       backslash_to_slash(file), backslash_to_slash(filebk));
            exit(0);
        }
    }

    // win_printf("config from %s",backslash_to_slash(pico_config_filename));
    set_config_file(file);
    return D_O_K;
}

int Close_Pico_config_file(void)
{
#ifdef BUILDING_DLL
    setlocale(LC_ALL, "C");
    override_config_file(NULL);
#endif
    return D_O_K;
}

int write_Pico_config_file(void)
{
    setlocale(LC_ALL, "C");
    set_pico_config_file_all_parameters();
    flush_config_file();
    do_update_menu();
    return D_O_K;
}

Micro_param *duplicate_pico_Micro_param(Micro_param *pico_in, Micro_param *pico_out)
{
    if (pico_in == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    if (pico_out == NULL)
    {
        win_printf_OK("OUT NULL POINTER");
        return NULL;
    }

    // pico_out->junk = strdup(pico_in->junk);
    pico_out->config_microscope_time = strdup(pico_in->config_microscope_time);
    pico_out->microscope_user_name = strdup(pico_in->microscope_user_name);
    pico_out->microscope_manufacturer_name = strdup(pico_in->microscope_manufacturer_name);
    pico_out->microscope_type = pico_in->microscope_type;
    pico_out->PicoTwist_model = strdup(pico_in->PicoTwist_model);
    pico_out->zoom_factor = pico_in->zoom_factor;
    pico_out->zoom_min = pico_in->zoom_min, pico_out->zoom_max = pico_in->zoom_max;
    pico_out->field_factor = pico_in->field_factor;
    pico_out->microscope_factor = pico_in->microscope_factor;
    pico_out->imaging_lens_focal_distance_in_mm = pico_in->imaging_lens_focal_distance_in_mm;
    pico_out->LED_wavelength = pico_in->LED_wavelength;
    return pico_out;
}

Camera_param *duplicate_pico_Camera_param(Camera_param *pico_in, Camera_param *pico_out)
{
    if (pico_in == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    if (pico_out == NULL)
    {
        win_printf_OK("OUT NULL POINTER");
        return NULL;
    }

    *pico_out = *pico_in;
    pico_out->config_camera_time = strdup(pico_in->config_camera_time);
    pico_out->camera_manufacturer = strdup(pico_in->camera_manufacturer);
    pico_out->camera_model = strdup(pico_in->camera_model);
    pico_out->camera_software_program = strdup(pico_in->camera_software_program);
    pico_out->camera_config_file = strdup(pico_in->camera_config_file);
    pico_out->camera_frequency_in_Hz = pico_in->camera_frequency_in_Hz;
    pico_out->pixel_clock = pico_in->pixel_clock;
    /*
       pico_out->x_pixel_2_microns = pico_in->x_pixel_2_microns;
       pico_out->y_pixel_2_microns = pico_in->y_pixel_2_microns;
       */
    pico_out->pixel_h_in_microns = pico_in->pixel_h_in_microns;
    pico_out->pixel_w_in_microns = pico_in->pixel_w_in_microns;
    // win_printf("in nx %d  ny %d\nout nx %d ny %d
    // ",pico_in->nb_pxl_x,pico_in->nb_pxl_y,pico_out->nb_pxl_x,pico_out->nb_pxl_y);
    return pico_out;
}

Obj_param *duplicate_pico_Obj_param(Obj_param *pico_in, Obj_param *pico_out)
{
    if (pico_in == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    if (pico_out == NULL)
    {
        win_printf_OK("OUT NULL POINTER");
        return NULL;
    }

    pico_out->config_objective_time = strdup(pico_in->config_objective_time);
    pico_out->objective_manufacturer = strdup(pico_in->objective_manufacturer);
    pico_out->objective_magnification = pico_in->objective_magnification;
    pico_out->immersion_type = pico_in->immersion_type;
    pico_out->objective_numerical_aperture = pico_in->objective_numerical_aperture;
    pico_out->immersion_index = pico_in->immersion_index;
    pico_out->buffer_index = pico_in->buffer_index;
    return pico_out;
}

Focus_param *duplicate_pico_Focus_param(Focus_param *pico_in, Focus_param *pico_out)
{
    if (pico_in == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    if (pico_out == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    pico_out->config_focus_time = strdup(pico_in->config_focus_time);
    pico_out->Focus_is_piezzo = pico_in->Focus_is_piezzo;
    pico_out->Focus_max_expansion = pico_in->Focus_max_expansion;
    pico_out->Focus_driver = strdup(pico_in->Focus_driver);
    pico_out->Focus_direction = pico_in->Focus_direction;
    pico_out->Focus_model = strdup(pico_in->Focus_model);
    return pico_out;
}
Rotation_Motor_param *duplicate_pico_Rotation_Motor_param(
    Rotation_Motor_param *pico_in, Rotation_Motor_param *pico_out)
{
    if (pico_in == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    if (pico_out == NULL)
    {
        win_printf_OK("OUT NULL POINTER");
        return NULL;
    }

    pico_out->config_rotation_motor_time = strdup(pico_in->config_rotation_motor_time);
    pico_out->rotation_motor_controller = strdup(pico_in->rotation_motor_controller);
    pico_out->rotation_motor_manufacturer = strdup(pico_in->rotation_motor_manufacturer);
    pico_out->rotation_motor_model = strdup(pico_in->rotation_motor_model);
    pico_out->rotation_motor_position = pico_in->rotation_motor_position;
    pico_out->gear_ratio = pico_in->gear_ratio;
    pico_out->P_rotation_motor = pico_in->P_rotation_motor;
    pico_out->I_rotation_motor = pico_in->I_rotation_motor;
    pico_out->D_rotation_motor = pico_in->D_rotation_motor;
    pico_out->rotation_max_velocity = pico_in->rotation_max_velocity;
    pico_out->rotation_velocity = pico_in->rotation_velocity;
    pico_out->nb_pulses_per_turn = pico_in->nb_pulses_per_turn;
    return pico_out;
}

Translation_Motor_param *duplicate_pico_Translation_Motor_param(
    Translation_Motor_param *pico_in, Translation_Motor_param *pico_out)
{
    if (pico_in == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    if (pico_out == NULL)
    {
        win_printf_OK("OUT NULL POINTER");
        return NULL;
    }

    pico_out->config_translation_motor_time = strdup(pico_in->config_translation_motor_time);
    pico_out->translation_motor_manufacturer = strdup(pico_in->translation_motor_manufacturer);
    pico_out->translation_motor_model = strdup(pico_in->translation_motor_model);
    pico_out->translation_motor_controller = strdup(pico_in->translation_motor_controller);
    pico_out->translation_motor_position = pico_in->translation_motor_position;
    pico_out->P_translation_motor = pico_in->P_translation_motor;
    pico_out->I_translation_motor = pico_in->I_translation_motor;
    pico_out->D_translation_motor = pico_in->D_translation_motor;
    pico_out->translation_max_velocity = pico_in->translation_max_velocity;
    pico_out->translation_velocity = pico_in->translation_velocity;
    pico_out->translation_max_pos = pico_in->translation_max_pos;
    pico_out->translation_motor_inverted = pico_in->translation_motor_inverted;
    pico_out->Inductive_sensor_servo_voltage = pico_in->Inductive_sensor_servo_voltage;
    pico_out->zmag_ref_for_inductive_test = pico_in->zmag_ref_for_inductive_test;
    pico_out->Vcap_at_zmag_ref_test = pico_in->Vcap_at_zmag_ref_test;
    pico_out->Zmag_vs_Vcap_poly_a0 = pico_in->Zmag_vs_Vcap_poly_a0;
    pico_out->Zmag_vs_Vcap_poly_a1 = pico_in->Zmag_vs_Vcap_poly_a1;
    pico_out->Zmag_vs_Vcap_poly_a2 = pico_in->Zmag_vs_Vcap_poly_a2;
    pico_out->nb_pulses_per_mm = pico_in->nb_pulses_per_mm;
    pico_out->last_saved_z_position=pico_in->last_saved_z_position;
    pico_out->zmag_offset = pico_in->zmag_offset;
    pico_out->motor_range = pico_in->motor_range;
    pico_out->motor_backslash = pico_in->motor_backslash;
    pico_out->motor_serial_nb = pico_in->motor_serial_nb;
    pico_out->ref_limit_switch = pico_in->ref_limit_switch;
#ifdef ZMAG_FACTOR
    pico_out->zmag_factor = pico_in->zmag_factor; //tv 090317
#endif
    return pico_out;
}

Serial_param *duplicate_pico_Serial_param(Serial_param *pico_in, Serial_param *pico_out)
{
    if (pico_in == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    if (pico_out == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    pico_out->config_serial_time = strdup(pico_in->config_serial_time);
    pico_out->serial_port = pico_in->serial_port;
    pico_out->serial_port_str = pico_in->serial_port_str;
    pico_out->serial_BaudRate = pico_in->serial_BaudRate;
    pico_out->serial_ByteSize = pico_in->serial_ByteSize;
    pico_out->serial_StopBits = pico_in->serial_StopBits;
    pico_out->serial_Parity = pico_in->serial_Parity;
    pico_out->serial_fDtrControl = pico_in->serial_fDtrControl;
    pico_out->serial_fRtsControl = pico_in->serial_fRtsControl;
    pico_out->firmware_version = strdup(pico_in->firmware_version);
    pico_out->rs232_started_fine = pico_in->rs232_started_fine;
    return pico_out;
}

Expt_param *duplicate_pico_Expt_param(Expt_param *pico_in, Expt_param *pico_out)
{
    if (pico_in == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    if (pico_out == NULL)
    {
        win_printf_OK("OUT NULL POINTER");
        return NULL;
    }

    pico_out->config_expt_time = strdup(pico_in->config_expt_time);
    pico_out->molecule_name = strdup(pico_in->molecule_name);
    // pico_out->protein_in_use = strdup(pico_in->protein_in_use);
    pico_out->bead_size = pico_in->bead_size;
    pico_out->molecule_extension = pico_in->molecule_extension;
    pico_out->scan_number = pico_in->scan_number;
    pico_out->bead_number = pico_in->bead_number;
    pico_out->project = strdup(pico_in->project);
    pico_out->experiment_set = strdup(pico_in->experiment_set);
    pico_out->experiment_type = strdup(pico_in->experiment_type);
    pico_out->experiment_name = strdup(pico_in->experiment_name);
    pico_out->user_firstname = strdup(pico_in->user_firstname);
    pico_out->user_lastname = strdup(pico_in->user_lastname);
    pico_out->experiment_filename = strdup(pico_in->experiment_filename);
    pico_out->reagents_name_size = pico_in->reagents_name_size;
    pico_out->reagents_name = NULL;

    if (pico_in->reagents_name_size > 0)
    {
        pico_out->reagents_name
            = (char **)realloc(pico_out->reagents_name, pico_in->reagents_name_size * sizeof(char *));

        for (int i = 0; i < pico_in->reagents_name_size; ++i)
        {
            pico_out->reagents_name[i] = pico_in->reagents_name[i];
        }
    }
    else
    {
        pico_out->reagents_name = NULL;
    }

    pico_out->buffer_name = pico_in->buffer_name;
    pico_out->linker_name = pico_in->linker_name;
    pico_out->name_template = pico_in->name_template;
    return pico_out;
}

Magnet_param *duplicate_pico_Magnet_param(Magnet_param *pico_in, Magnet_param *pico_out)
{
    int i;

    if (pico_in == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    if (pico_out == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    pico_out->config_magnet_time = strdup(pico_in->config_magnet_time);
    pico_out->magnet_name = strdup(pico_in->magnet_name);
    pico_out->manufacturer = strdup(pico_in->manufacturer);
    pico_out->series_nb = pico_in->series_nb;
    pico_out->gap = pico_in->gap;
    pico_out->zmag_contact_sample = pico_in->zmag_contact_sample;
    pico_out->nb = pico_in->nb;

    for (i = 0; i < 6; i++)
    {
        pico_out->nd[i] = pico_in->nd[i];
        pico_out->f0[i] = pico_in->f0[i];
        pico_out->a1[i] = pico_in->a1[i];
        pico_out->a2[i] = pico_in->a2[i];
        pico_out->a3[i] = pico_in->a3[i];
        pico_out->a4[i] = pico_in->a4[i];
    }

    return pico_out;
}

DNA_molecule *duplicate_pico_DNA_molecule(DNA_molecule *pico_in, DNA_molecule *pico_out)
{
    if (pico_in == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    if (pico_out == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    pico_out->config_molecule_time = strdup(pico_in->config_molecule_time);
    pico_out->molecule_name = strdup(pico_in->molecule_name);
    pico_out->ssDNA_molecule_extension = pico_in->ssDNA_molecule_extension;
    pico_out->dsDNA_molecule_extension = pico_in->dsDNA_molecule_extension;
    pico_out->dsxi = pico_in->dsxi;
    pico_out->nick_proba = pico_in->nick_proba;
    return pico_out;
}

Bead_param *duplicate_pico_Bead_param(Bead_param *pico_in, Bead_param *pico_out)
{
    if (pico_in == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    if (pico_out == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    pico_out->config_bead_time = strdup(pico_in->config_bead_time);
    pico_out->bead_name = strdup(pico_in->bead_name);
    pico_out->manufacturer = strdup(pico_in->manufacturer);
    pico_out->radius = pico_in->radius;
    pico_out->low_field_suceptibility = pico_in->low_field_suceptibility;
    pico_out->saturating_magnetization = pico_in->saturating_magnetization;
    pico_out->saturating_field = pico_in->saturating_field;
    return pico_out;
}

Pico_parameter *duplicate_Pico_parameter(Pico_parameter *pico_in, Pico_parameter *pico_out)
{
    if (pico_in == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    if (pico_out == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }

    duplicate_pico_Micro_param(&pico_in->micro_param, &pico_out->micro_param);
    duplicate_pico_Camera_param(&pico_in->camera_param, &pico_out->camera_param);
    duplicate_pico_Obj_param(&pico_in->obj_param, &pico_out->obj_param);
    duplicate_pico_Focus_param(&pico_in->focus_param, &pico_out->focus_param);
    duplicate_pico_Translation_Motor_param(
        &pico_in->translation_motor_param, &pico_out->translation_motor_param);
    duplicate_pico_Rotation_Motor_param(
        &pico_in->rotation_motor_param, &pico_out->rotation_motor_param);
    duplicate_pico_Serial_param(&pico_in->serial_param, &pico_out->serial_param);
    duplicate_pico_Expt_param(&pico_in->expt_param, &pico_out->expt_param);
    duplicate_pico_Magnet_param(&pico_in->magnet_param, &pico_out->magnet_param);
    duplicate_pico_DNA_molecule(&pico_in->dna_molecule, &pico_out->dna_molecule);
    duplicate_pico_Bead_param(&pico_in->bead_param, &pico_out->bead_param);
#ifdef SDI_VERSION
	duplicate_pico_Sdi_param(&pico_in->sdi_param, &pico_out->sdi_param);
#endif
    return pico_out;
}
#ifdef SDI_VERSION_2
int set_pico_config_file_SDI_2_parameters(void)
{
  //parameters of sdi physics
	set_config_float("SDI", "SDI_2_calibration", Pico_param.SDI_2_param.SDI_2_calibration);
  set_config_float("SDI", "SDI_2_photons_per_level", Pico_param.SDI_2_param.photons_per_level);
  return D_O_K;
}
#endif

#ifdef SDI_VERSION
Sdi_param *duplicate_pico_Sdi_param(Sdi_param *pico_in, Sdi_param *pico_out)
{
    if (pico_in == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }
    if (pico_out == NULL)
    {
        win_printf_OK("IN NULL POINTER");
        return NULL;
    }
	//calibration parameters
  pico_out->v_0 = pico_in->v_0;
	pico_out->v_1 = pico_in->v_1;
	pico_out->v_x = pico_in->v_x;
	pico_out->nu0 = pico_in->nu0;

	//filter design
	pico_out->x1 = pico_in->x1;
	pico_out->x2 = pico_in->x2;
	pico_out->wx = pico_in->wx;
	pico_out->wy = pico_in->wy;
	pico_out->n_index = pico_in->n_index;
    return pico_out;
}




int set_pico_config_file_Sdi_parameters(void)
{
  //parameters of sdi physics
	set_config_float("SDI", "internal_slit_position", Pico_param.sdi_param.x1);
	set_config_float("SDI", "external_slit_position", Pico_param.sdi_param.x2);
	set_config_float("SDI", "slit_width", Pico_param.sdi_param.wx);
	set_config_float("SDI", "slit_height", Pico_param.sdi_param.wy);
	set_config_float("SDI", "refraction_index", Pico_param.sdi_param.n_index);
  set_config_float("SDI", "z_phase_velocity", Pico_param.sdi_param.v_0);
	set_config_float("SDI", "x_phase_velocity", Pico_param.sdi_param.v_x);
	set_config_float("SDI", "group_velocity", Pico_param.sdi_param.v_1);
	set_config_float("SDI", "frequency", Pico_param.sdi_param.nu0);

	//parameters of tracking algo
	set_config_int("SDI", "dist_half_beads", Pico_param.sdi_param.dist_half_beads);
	set_config_int("SDI", "x_roi_size_zx", Pico_param.sdi_param.x_roi_size_zx);
	set_config_int("SDI", "y_roi_size_zx", Pico_param.sdi_param.y_roi_size_zx);
	set_config_int("SDI", "x_window_order", Pico_param.sdi_param.x_window_order);
	set_config_int("SDI", "x_window_fwhm", Pico_param.sdi_param.x_window_fwhm);
	set_config_float("SDI", "nu_threshold", Pico_param.sdi_param.nu_threshold);
	set_config_int("SDI", "dc_width", Pico_param.sdi_param.dc_width);
	set_config_int("SDI", "x_roi_size_y", Pico_param.sdi_param.x_roi_size_y);
	set_config_int("SDI", "y_roi_size_y", Pico_param.sdi_param.y_roi_size_y);
	set_config_int("SDI", "y_window_fwhm", Pico_param.sdi_param.y_window_fwhm );
	set_config_int("SDI", "y_window_order", Pico_param.sdi_param.y_window_order);

  //parameters for quality of beads
  set_config_int("SDI", "weird_count_limit", Pico_param.sdi_param.weird_count_limit);
  set_config_float("SDI", "contrast_min_limit", Pico_param.sdi_param.contrast_min_limit);
  set_config_float("SDI", "I_min_limit", Pico_param.sdi_param.I_min_limit);
  set_config_float("SDI", "I_max_limit", Pico_param.sdi_param.I_max_limit);
  set_config_float("SDI", "z_min_limit", Pico_param.sdi_param.z_min_limit);
  set_config_float("SDI", "z_max_limit", Pico_param.sdi_param.z_max_limit);
  set_config_float("SDI", "y_min_limit", Pico_param.sdi_param.y_min_limit);
  set_config_float("SDI", "y_max_limit", Pico_param.sdi_param.y_max_limit);
  set_config_float("SDI", "x_min_limit", Pico_param.sdi_param.x_min_limit);
  set_config_float("SDI", "x_max_limit", Pico_param.sdi_param.x_max_limit);
  //parameters for analyzing the function of a bead
  set_config_float("SDI", "std_threshold_z", Pico_param.sdi_param.std_threshold_z);
  set_config_float("SDI", "std_threshold_x", Pico_param.sdi_param.std_threshold_x);
  set_config_float("SDI", "std_threshold_y", Pico_param.sdi_param.std_threshold_y);
  set_config_float("SDI", "z_expected_range", Pico_param.sdi_param.z_expected_range);
  set_config_float("SDI", "z_range_tol", Pico_param.sdi_param.z_range_tol);

  //parameter analyzing the y profiles : detection of beads
  set_config_float("SDI", "y_correlation_min", Pico_param.sdi_param.correlation_min);
  set_config_int("SDI", "dist_y_min", Pico_param.sdi_param.dist_y_min);
  set_config_int("SDI", "dist_y_max", Pico_param.sdi_param.dist_y_max);
  set_config_int("SDI", "dist_x_max", Pico_param.sdi_param.dist_x_max);
  set_config_int("SDI", "verbose", Pico_param.sdi_param.verbose);
  set_config_float("SDI", "zmag_y_corr", Pico_param.sdi_param.zmag_y_corr);
  set_config_int("SDI", "y_corr_n_points", Pico_param.sdi_param.y_corr_n_points);

  //parameter analyze of beads : taking 0, counting hp
  set_config_int("SDI", "xyz_n_points", Pico_param.sdi_param.xyz_n_points);
  set_config_int("SDI", "zmag_cycle_period", Pico_param.sdi_param.zmag_cycle_period);
  set_config_float("SDI", "z_mag_lf_l0", Pico_param.sdi_param.z_mag_lf_l0);
  set_config_float("SDI", "z_mag_open", Pico_param.sdi_param.z_mag_open);
  set_config_float("SDI", "z_mag_close", Pico_param.sdi_param.z_mag_close);

  return D_O_K;
}
#endif

int set_pico_config_file_Micro_parameters(void)
{
    set_config_string(
        "MICROSCOPE", "config_microscope_time", Pico_param.micro_param.config_microscope_time);
    set_config_string(
        "MICROSCOPE", "microscope_user_name", Pico_param.micro_param.microscope_user_name);
    set_config_string("MICROSCOPE", "microscope_manufacturer_name",
                      Pico_param.micro_param.microscope_manufacturer_name);
    set_config_int("MICROSCOPE", "microscope_type", Pico_param.micro_param.microscope_type);
    set_config_string("MICROSCOPE", "PicoTwist_model", Pico_param.micro_param.PicoTwist_model);
    set_config_float("MICROSCOPE", "zoom_factor", Pico_param.micro_param.zoom_factor);
    set_config_float("MICROSCOPE", "zoom_min", Pico_param.micro_param.zoom_min);
    set_config_float("MICROSCOPE", "zoom_max", Pico_param.micro_param.zoom_max);
    set_config_float("MICROSCOPE", "field_factor", Pico_param.micro_param.field_factor);
    set_config_float("MICROSCOPE", "microscope_factor", Pico_param.micro_param.microscope_factor);
    set_config_float("MICROSCOPE", "imaging_lens_focal_distance_in_mm",
                     Pico_param.micro_param.imaging_lens_focal_distance_in_mm);
    set_config_int("MICROSCOPE", "LED_wavelength", Pico_param.micro_param.LED_wavelength);
    //    write_Pico_config_file();
    // Close_Pico_config_file();
    return D_O_K;
}

int set_pico_config_file_Camera_parameters(void)
{
    // Open_Pico_config_file();
    //  win_printf("camera model %s \n nx %d \n ny %d",Pico_param.camera_param.camera_model
    //     ,Pico_param.camera_param.nb_pxl_x,Pico_param.camera_param.nb_pxl_y);
    set_config_string("CAMERA", "config_camera_time", Pico_param.camera_param.config_camera_time);
    set_config_string("CAMERA", "camera_manufacturer", Pico_param.camera_param.camera_manufacturer);
    set_config_string("CAMERA", "camera_model", Pico_param.camera_param.camera_model);
    set_config_float(
        "CAMERA", "camera_frequency_in_Hz", Pico_param.camera_param.camera_frequency_in_Hz);
    set_config_float("CAMERA", "pixel_clock", Pico_param.camera_param.pixel_clock);
    set_config_string("CAMERA", "camera_config_file", Pico_param.camera_param.camera_config_file);
    set_config_string(
        "CAMERA", "camera_software_program", Pico_param.camera_param.camera_software_program);
    set_config_float("CAMERA", "x_pixel_2_microns", Pico_param.camera_param.x_pixel_2_microns);
    set_config_float("CAMERA", "y_pixel_2_microns", Pico_param.camera_param.y_pixel_2_microns);
    set_config_float("CAMERA", "pixel_h_in_microns", Pico_param.camera_param.pixel_h_in_microns);
    set_config_float("CAMERA", "pixel_w_in_microns", Pico_param.camera_param.pixel_w_in_microns);
    set_config_int("CAMERA", "nb_pxl_x", Pico_param.camera_param.nb_pxl_x);
    set_config_int("CAMERA", "nb_pxl_y", Pico_param.camera_param.nb_pxl_y);
    set_config_int("CAMERA", "nb_bits", Pico_param.camera_param.nb_bits);
    set_config_int("CAMERA", "interlace", Pico_param.camera_param.interlace);
    set_config_float("CAMERA", "time_opening_factor", Pico_param.camera_param.time_opening_factor);
    set_config_int("CAMERA", "shutter_time_us", Pico_param.camera_param.shutter_time_us);
    // write_Pico_config_file();
    // Close_Pico_config_file();
    return D_O_K;
}

int set_pico_config_file_Obj_parameters(void)
{
    // Open_Pico_config_file();
    set_config_string(
        "OBJECTIVE", "config_objective_time", Pico_param.obj_param.config_objective_time);
    set_config_string(
        "OBJECTIVE", "objective_manufacturer", Pico_param.obj_param.objective_manufacturer);
    set_config_float(
        "OBJECTIVE", "objective_magnification", Pico_param.obj_param.objective_magnification);
    set_config_int("OBJECTIVE", "immersion_type", Pico_param.obj_param.immersion_type);
    set_config_float("OBJECTIVE", "objective_numerical_aperture",
                     Pico_param.obj_param.objective_numerical_aperture);
    set_config_float("OBJECTIVE", "immersion_index", Pico_param.obj_param.immersion_index);
    set_config_float("OBJECTIVE", "buffer_index", Pico_param.obj_param.buffer_index);
    // write_Pico_config_file();
    // Close_Pico_config_file();
    return D_O_K;
}

int set_pico_config_file_Focus_parameters(void)
{
    // Open_Pico_config_file();
    set_config_string("FOCUS", "config_focus_time", Pico_param.focus_param.config_focus_time);
    set_config_int("FOCUS", "Focus_is_piezzo", Pico_param.focus_param.Focus_is_piezzo);
    set_config_string("FOCUS", "Focus_model", Pico_param.focus_param.Focus_model);
    set_config_float("FOCUS", "Focus_max_expansion", Pico_param.focus_param.Focus_max_expansion);
    set_config_string("FOCUS", "Focus_driver", Pico_param.focus_param.Focus_driver);
    set_config_int("FOCUS", "Focus_direction", Pico_param.focus_param.Focus_direction);
    // write_Pico_config_file();
    // Close_Pico_config_file();
    return D_O_K;
}

int set_pico_config_file_Rotation_Motor_parameters(void)
{
    // Open_Pico_config_file();
    set_config_string("ROTATION_MOTOR", "config_rotation_motor_time",
                      Pico_param.rotation_motor_param.config_rotation_motor_time);
    set_config_string("ROTATION_MOTOR", "rotation_motor_manufacturer",
                      Pico_param.rotation_motor_param.rotation_motor_manufacturer);
    set_config_string("ROTATION_MOTOR", "rotation_motor_controller",
                      Pico_param.rotation_motor_param.rotation_motor_controller);
    set_config_string("ROTATION_MOTOR", "rotation_motor_model",
                      Pico_param.rotation_motor_param.rotation_motor_model);
    set_config_float("ROTATION_MOTOR", "gear_ratio", Pico_param.rotation_motor_param.gear_ratio);
    set_config_float(
        "ROTATION_MOTOR", "P_rotation_motor", Pico_param.rotation_motor_param.P_rotation_motor);
    set_config_float(
        "ROTATION_MOTOR", "I_rotation_motor", Pico_param.rotation_motor_param.I_rotation_motor);
    set_config_float(
        "ROTATION_MOTOR", "D_rotation_motor", Pico_param.rotation_motor_param.D_rotation_motor);
    set_config_float("ROTATION_MOTOR", "rotation_max_velocity",
                     Pico_param.rotation_motor_param.rotation_max_velocity);
    set_config_float(
        "ROTATION_MOTOR", "rotation_velocity", Pico_param.rotation_motor_param.rotation_velocity);
    set_config_float("ROTATION_MOTOR", "rotation_motor_position",
                     Pico_param.rotation_motor_param.rotation_motor_position);
    set_config_int(
        "ROTATION_MOTOR", "nb_pulses_per_turn", Pico_param.rotation_motor_param.nb_pulses_per_turn);
    return D_O_K;
}

int set_pico_config_file_Translation_Motor_parameters(void)
{
    set_config_string("TRANSLATION_MOTOR", "config_translation_motor_time",
                      Pico_param.translation_motor_param.config_translation_motor_time);
    set_config_string("TRANSLATION_MOTOR", "translation_motor_manufacturer",
                      Pico_param.translation_motor_param.translation_motor_manufacturer);
    set_config_string("TRANSLATION_MOTOR", "translation_motor_model",
                      Pico_param.translation_motor_param.translation_motor_model);
    set_config_float("TRANSLATION_MOTOR", "P_translation_motor",
                     Pico_param.translation_motor_param.P_translation_motor);
    set_config_float("TRANSLATION_MOTOR", "I_translation_motor",
                     Pico_param.translation_motor_param.I_translation_motor);
    set_config_float("TRANSLATION_MOTOR", "D_translation_motor",
                     Pico_param.translation_motor_param.D_translation_motor);
    set_config_float("TRANSLATION_MOTOR", "translation_max_velocity",
                     Pico_param.translation_motor_param.translation_max_velocity);
    set_config_float("TRANSLATION_MOTOR", "translation_velocity",
                     Pico_param.translation_motor_param.translation_velocity);
    set_config_float("TRANSLATION_MOTOR", "translation_motor_position",
                     Pico_param.translation_motor_param.translation_motor_position);
    set_config_float("TRANSLATION_MOTOR", "translation_max_pos",
                     Pico_param.translation_motor_param.translation_max_pos);
    set_config_int("TRANSLATION_MOTOR", "translation_motor_inverted",
                   Pico_param.translation_motor_param.translation_motor_inverted);
    set_config_string("TRANSLATION_MOTOR", "translation_motor_controller",
                      Pico_param.translation_motor_param.translation_motor_controller);
    set_config_float("TRANSLATION_MOTOR", "Inductive_sensor_servo_voltage",
                     Pico_param.translation_motor_param.Inductive_sensor_servo_voltage);
    set_config_float("TRANSLATION_MOTOR", "zmag_ref_for_inductive_test",
                     Pico_param.translation_motor_param.zmag_ref_for_inductive_test);
    set_config_float("TRANSLATION_MOTOR", "Vcap_at_zmag_ref_test",
                     Pico_param.translation_motor_param.Vcap_at_zmag_ref_test);
    set_config_float("TRANSLATION_MOTOR", "Zmag_vs_Vcap_poly_a0",
                     Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a0);
    set_config_float("TRANSLATION_MOTOR", "Zmag_vs_Vcap_poly_a1",
                     Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a1);
    set_config_float("TRANSLATION_MOTOR", "Zmag_vs_Vcap_poly_a2",
                     Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a2);
    set_config_float("TRANSLATION_MOTOR", "last_saved_z_position",
                                      Pico_param.translation_motor_param.last_saved_z_position);
    set_config_int("TRANSLATION_MOTOR", "nb_pulses_per_mm",
                   Pico_param.translation_motor_param.nb_pulses_per_mm);
    set_config_float(
        "TRANSLATION_MOTOR", "zmag_offset", Pico_param.translation_motor_param.zmag_offset);
    set_config_float(
        "TRANSLATION_MOTOR", "motor_range", Pico_param.translation_motor_param.motor_range);
    set_config_float(
        "TRANSLATION_MOTOR", "motor_backslash", Pico_param.translation_motor_param.motor_backslash);
    set_config_int(
        "TRANSLATION_MOTOR", "motor_serial_nb", Pico_param.translation_motor_param.motor_serial_nb);
    set_config_int("TRANSLATION_MOTOR", "ref_limit_switch",
                   Pico_param.translation_motor_param.ref_limit_switch);
#ifdef ZMAG_FACTOR
    set_config_float("TRANSLATION_MOTOR", "zmag_factor", Pico_param.translation_motor_param.zmag_factor); //tv 090317 : zmag_factor
#endif
	// write_Pico_config_file();
    // Close_Pico_config_file();
    return D_O_K;
}

int set_pico_config_file_Serial_parameters(void)
{
    //  Open_Pico_config_file();
    set_config_string(
        "SERIAL_COM", "config_serial_time", Pico_param.serial_param.config_serial_time);
    set_config_int("SERIAL_COM", "serial_port", Pico_param.serial_param.serial_port);
    set_config_string("SERIAL_COM", "serial_port_str", Pico_param.serial_param.serial_port_str);
    set_config_int("SERIAL_COM", "serial_BaudRate", Pico_param.serial_param.serial_BaudRate);
    set_config_int("SERIAL_COM", "serial_ByteSize", Pico_param.serial_param.serial_ByteSize);
    set_config_int("SERIAL_COM", "serial_StopBits", Pico_param.serial_param.serial_StopBits);
    set_config_int("SERIAL_COM", "serial_Parity", Pico_param.serial_param.serial_Parity);
    set_config_int("SERIAL_COM", "serial_fDtrControl", Pico_param.serial_param.serial_fDtrControl);
    set_config_int("SERIAL_COM", "serial_fRtsControl", Pico_param.serial_param.serial_fRtsControl);
    set_config_string("SERIAL_COM", "firmware_version", Pico_param.serial_param.firmware_version);
    set_config_int("SERIAL_COM", "rs232_started_fine", Pico_param.serial_param.rs232_started_fine);
    // write_Pico_config_file();
    // Close_Pico_config_file();
    return D_O_K;
}

int set_pico_config_file_Expt_parameters(void)
{
    //  Open_Pico_config_file();
    const char reagent_prefix[] = "reagent_name_";
    char buf[2048] = { 0 };
    set_config_string("EXPERIMENT", "project", Pico_param.expt_param.project);
    set_config_string("EXPERIMENT", "experiment_set", Pico_param.expt_param.experiment_set);
    set_config_string("EXPERIMENT", "experiment_type", Pico_param.expt_param.experiment_type);
    set_config_string("EXPERIMENT", "experiment_name", Pico_param.expt_param.experiment_name);
    set_config_string("EXPERIMENT", "user_firstname", Pico_param.expt_param.user_firstname);
    set_config_string("EXPERIMENT", "user_lastname", Pico_param.expt_param.user_lastname);
    set_config_string("EXPERIMENT", "experiment_filename", Pico_param.expt_param.experiment_filename);
    set_config_int("EXPERIMENT", "reagents_name_size", Pico_param.expt_param.reagents_name_size);

    for (int i = 0; i < Pico_param.expt_param.reagents_name_size; ++i)
    {
        snprintf(buf, sizeof(buf), "%s%d", reagent_prefix, i);
        set_config_string("EXPERIMENT", buf, Pico_param.expt_param.reagents_name[i]);
    }

    set_config_string("EXPERIMENT", "buffer_name", Pico_param.expt_param.buffer_name);
    set_config_string("EXPERIMENT", "linker_name", Pico_param.expt_param.linker_name);
    set_config_string("EXPERIMENT", "name_template", Pico_param.expt_param.name_template);
    set_config_string("EXPERIMENT", "config_expt_time", Pico_param.expt_param.config_expt_time);
    set_config_string("EXPERIMENT", "molecule_name", Pico_param.expt_param.molecule_name);
    // set_config_string("EXPERIMENT", "protein_in_use", Pico_param.expt_param.protein_in_use);
    set_config_float("EXPERIMENT", "bead_size", Pico_param.expt_param.bead_size);
    set_config_float("EXPERIMENT", "molecule_extension", Pico_param.expt_param.molecule_extension);
    set_config_int("EXPERIMENT", "scan_number", Pico_param.expt_param.scan_number);
    set_config_int("EXPERIMENT", "bead_number", Pico_param.expt_param.bead_number);
    return D_O_K;
}

int set_pico_config_file_Magnet_parameters(void)
{
    //  Open_Pico_config_file();
    int i, j;
    char ch[128];
    set_config_string("MAGNET", "config_magnet_time", Pico_param.magnet_param.config_magnet_time);
    set_config_string("MAGNET", "magnet_name", Pico_param.magnet_param.magnet_name);
    set_config_string("MAGNET", "manufacturer", Pico_param.magnet_param.manufacturer);
    set_config_int("MAGNET", "series_nb", Pico_param.magnet_param.series_nb);
    set_config_float("MAGNET", "gap", Pico_param.magnet_param.gap);
    set_config_float("MAGNET", "zmag_contact_sample", Pico_param.magnet_param.zmag_contact_sample);
    set_config_int("MAGNET", "nb", Pico_param.magnet_param.nb);

    for (i = 0; i < Pico_param.magnet_param.nb && i < 6; i++)
    {
        snprintf(ch, 128, "nd_%d", i);
        set_config_int("MAGNET", ch, Pico_param.magnet_param.nd[i]);
        snprintf(ch, 128, "f0_%d", i);
        set_config_float("MAGNET", ch, Pico_param.magnet_param.f0[i]);

        for (j = 1; j < Pico_param.magnet_param.nd[i] && j < 5; j++)
        {
            if (j == 1)
            {
                snprintf(ch, 128, "a1_%d", i);
                set_config_float("MAGNET", ch, Pico_param.magnet_param.a1[i]);
            }

            if (j == 2)
            {
                snprintf(ch, 128, "a2_%d", i);
                set_config_float("MAGNET", ch, Pico_param.magnet_param.a2[i]);
            }

            if (j == 3)
            {
                snprintf(ch, 128, "a3_%d", i);
                set_config_float("MAGNET", ch, Pico_param.magnet_param.a3[i]);
            }

            if (j == 4)
            {
                snprintf(ch, 128, "a4_%d", i);
                set_config_float("MAGNET", ch, Pico_param.magnet_param.a4[i]);
            }
        }
    }

    return D_O_K;
}

int set_pico_config_file_DNA_Molecule(void)
{
    //  Open_Pico_config_file();
    set_config_string(
        "MOLECULE", "config_molecule_time", Pico_param.dna_molecule.config_molecule_time);
    set_config_string("MOLECULE", "molecule_name", Pico_param.dna_molecule.molecule_name);
    set_config_float(
        "MOLECULE", "ssDNA_molecule_extension", Pico_param.dna_molecule.ssDNA_molecule_extension);
    set_config_float(
        "MOLECULE", "dsDNA_molecule_extension", Pico_param.dna_molecule.dsDNA_molecule_extension);
    set_config_float("MOLECULE", "dsxi", Pico_param.dna_molecule.dsxi);
    set_config_float("MOLECULE", "nick_proba", Pico_param.dna_molecule.nick_proba);
    return D_O_K;
}

int set_pico_config_file_Bead_param(void)
{
    //  Open_Pico_config_file();
    set_config_string("BEAD", "config_bead_time", Pico_param.bead_param.config_bead_time);
    set_config_string("BEAD", "bead_name", Pico_param.bead_param.bead_name);
    // win_printf("save Bead name %s",Pico_param.bead_param.bead_name);
    set_config_string("BEAD", "manufacturer", Pico_param.bead_param.manufacturer);
    set_config_float("BEAD", "radius", Pico_param.bead_param.radius);
    set_config_float(
        "BEAD", "low_field_suceptibility", Pico_param.bead_param.low_field_suceptibility);
    set_config_float(
        "BEAD", "saturating_magnetization", Pico_param.bead_param.saturating_magnetization);
    set_config_float("BEAD", "saturating_field", Pico_param.bead_param.saturating_field);
    return D_O_K;
}

int set_pico_config_file_all_parameters(void)
{
    Open_Pico_config_file();
    set_pico_config_file_Micro_parameters();
    set_pico_config_file_Camera_parameters();
    set_pico_config_file_Obj_parameters();
    set_pico_config_file_Focus_parameters();
    set_pico_config_file_Translation_Motor_parameters();
    set_pico_config_file_Rotation_Motor_parameters();
    set_pico_config_file_Serial_parameters();
    set_pico_config_file_Expt_parameters();
    set_pico_config_file_Magnet_parameters();
    set_pico_config_file_DNA_Molecule();
    set_pico_config_file_Bead_param();
#ifdef SDI_VERSION
	set_pico_config_file_Sdi_parameters();
#endif
#ifdef SDI_VERSION_2
	set_pico_config_file_SDI_2_parameters();
#endif
    //  write_Pico_config_file();
    Close_Pico_config_file();
    return D_O_K;
}

////////////////////////////////////LOADING PARAMETERS/////////////////////////////////////
//tv 23/03/2017
#ifdef SDI_VERSION
int load_Pico_Sdi_config(void)
{
  Open_Pico_config_file();
	//config of the slits
	Pico_param.sdi_param.x1 = get_config_float("SDI", "internal_slit_position", 1.);
	Pico_param.sdi_param.x2 = get_config_float("SDI", "external_slit_position", 1.);
	Pico_param.sdi_param.wx = get_config_float("SDI", "slit_width", 1.);
	Pico_param.sdi_param.wy = get_config_float("SDI", "slit_height", 1.);
	Pico_param.sdi_param.n_index = get_config_float("SDI", "refraction_index", 1.);
	Pico_param.sdi_param.v_0 = get_config_float("SDI", "z_phase_velocity", 1.);
  Pico_param.sdi_param.v_x = get_config_float("SDI", "x_phase_velocity", 1.);
	Pico_param.sdi_param.v_1 = get_config_float("SDI", "group_velocity", 1.);
	Pico_param.sdi_param.nu0 = get_config_float("SDI", "frequency", 1.);

	//parameters of the tracking algo
  Pico_param.sdi_param.nu_threshold = get_config_float("SDI", "nu_threshold", 1.);
	Pico_param.sdi_param.dist_half_beads = get_config_int("SDI", "dist_half_beads", 1);
	Pico_param.sdi_param.x_roi_size_zx = get_config_int("SDI", "x_roi_size_zx", 1);
	Pico_param.sdi_param.y_roi_size_zx = get_config_int("SDI", "y_roi_size_zx", 1);
	Pico_param.sdi_param.x_window_order = get_config_int("SDI", "x_window_order", 1);
	Pico_param.sdi_param.x_window_fwhm = get_config_int("SDI", "x_window_fwhm", 1);
	Pico_param.sdi_param.dc_width = get_config_int("SDI", "dc_width", 1);
	Pico_param.sdi_param.x_roi_size_y = get_config_int("SDI", "x_roi_size_y", 1);
	Pico_param.sdi_param.y_roi_size_y = get_config_int("SDI", "y_roi_size_y", 1);
	Pico_param.sdi_param.y_window_fwhm = get_config_int("SDI", "y_window_fwhm", 1);
	Pico_param.sdi_param.y_window_order = get_config_int("SDI", "y_window_order", 1);

  //parameters for quality of beads
  Pico_param.sdi_param.weird_count_limit = get_config_int("SDI", "weird_count_limit", 1);
  Pico_param.sdi_param.contrast_min_limit = get_config_float("SDI", "contrast_min_limit", 1.);
  Pico_param.sdi_param.I_min_limit = get_config_float("SDI", "I_min_limit", 1.);
  Pico_param.sdi_param.I_max_limit = get_config_float("SDI", "I_max_limit", 1.);
  Pico_param.sdi_param.z_min_limit = get_config_float("SDI", "z_min_limit", 1.);
  Pico_param.sdi_param.z_max_limit = get_config_float("SDI", "z_max_limit", 1.);
  Pico_param.sdi_param.y_min_limit = get_config_float("SDI", "y_min_limit", 1.);
  Pico_param.sdi_param.y_max_limit = get_config_float("SDI", "y_max_limit", 1.);
  Pico_param.sdi_param.x_max_limit = get_config_float("SDI", "x_max_limit", 1.);
  Pico_param.sdi_param.x_min_limit = get_config_float("SDI", "x_min_limit", 1.);

  //parameters for analyzing the function of a bead
  Pico_param.sdi_param.std_threshold_z = get_config_float("SDI", "std_threshold_z", 1.);
  Pico_param.sdi_param.std_threshold_x = get_config_float("SDI", "std_threshold_x", 1.);
  Pico_param.sdi_param.std_threshold_y = get_config_float("SDI", "std_threshold_y", 1.);
  Pico_param.sdi_param.z_expected_range = get_config_float("SDI", "z_expected_range", 1.);
  Pico_param.sdi_param.z_range_tol = get_config_float("SDI", "z_range_tol", 1.);

  //parameter for detection of beads
  Pico_param.sdi_param.correlation_min = get_config_float("SDI", "y_correlation_min", 1.);
  Pico_param.sdi_param.dist_y_min = get_config_int("SDI", "dist_y_min", 1);
  Pico_param.sdi_param.dist_y_max = get_config_int("SDI", "dist_y_max", 1);
  Pico_param.sdi_param.dist_x_max = get_config_int("SDI", "dist_x_max", 1);
  Pico_param.sdi_param.verbose = get_config_int("SDI", "verbose", 1);
  Pico_param.sdi_param.zmag_y_corr = get_config_float("SDI", "zmag_y_corr", 1.);
  Pico_param.sdi_param.y_corr_n_points = get_config_int("SDI", "y_corr_n_points", 1);

  //parameters fro analyze beads
  Pico_param.sdi_param.xyz_n_points = get_config_int("SDI", "xyz_n_points", 1);
  Pico_param.sdi_param.zmag_cycle_period = get_config_int("SDI", "zmag_cycle_period", 1);
  Pico_param.sdi_param.z_mag_lf_l0 = get_config_float("SDI", "z_mag_lf_l0", 1.);
  Pico_param.sdi_param.z_mag_open = get_config_float("SDI", "z_mag_open", 1.);
  Pico_param.sdi_param.z_mag_close =get_config_float("SDI", "z_mag_close", 1.);

  Close_Pico_config_file();
  return D_O_K;
}
#endif

#ifdef SDI_VERSION_2
int load_Pico_SDI_2_config(void)
{
  Open_Pico_config_file();
	//config of the slits
	Pico_param.SDI_2_param.SDI_2_calibration = get_config_float("SDI", "SDI_2_calibration", -1.);
  Pico_param.SDI_2_param.photons_per_level = get_config_float("SDI", "SDI_2_photons_per_level", -1.);


  Close_Pico_config_file();
  return D_O_K;
}
#endif

int load_Pico_Micro_config(void)
{
    Open_Pico_config_file();
    //    Pico_param.micro_param.junk = strdup(get_config_string("MICROSCOPE", "junk", "\0"));
    Pico_param.micro_param.config_microscope_time
        = strdup(get_config_string("MICROSCOPE", "config_microscope_time", "\0"));
    // Default_Pico_param.micro_param.config_microscope_time);
    Pico_param.micro_param.microscope_user_name
        = strdup(get_config_string("MICROSCOPE", "microscope_user_name", "\0"));
    // Default_Pico_param.micro_param.microscope_user_name);
    Pico_param.micro_param.microscope_manufacturer_name
        = strdup(get_config_string("MICROSCOPE", "microscope_manufacturer_name", "\0"));
    // Default_Pico_param.micro_param.microscope_manufacturer_name);
    Pico_param.micro_param.microscope_type
        = get_config_int("MICROSCOPE", "microscope_type", HOME_MADE);
    // Default_Pico_param.micro_param.microscope_type);
    Pico_param.micro_param.PicoTwist_model
        = strdup(get_config_string("MICROSCOPE", "PicoTwist_model",
                                   "yap")); // Default_Pico_param.micro_param.PicoTwist_model);
    Pico_param.micro_param.zoom_factor = get_config_float("MICROSCOPE", "zoom_factor",
                                         1.); // Default_Pico_param.micro_param.zoom_factor);
    Pico_param.micro_param.zoom_min = get_config_float("MICROSCOPE", "zoom_min",
                                      1.); // Default_Pico_param.micro_param.zoom_min);
    Pico_param.micro_param.zoom_max = get_config_float("MICROSCOPE", "zoom_max",
                                      1.); // Default_Pico_param.micro_param.zoom_max);
    Pico_param.micro_param.field_factor = get_config_float("MICROSCOPE", "field_factor",
                                          1.); // Default_Pico_param.micro_param.field_factor);
    Pico_param.micro_param.microscope_factor = get_config_float("MICROSCOPE", "microscope_factor",
            1.); // Default_Pico_param.micro_param.microscope_factor);
    Pico_param.micro_param.imaging_lens_focal_distance_in_mm
        = get_config_float("MICROSCOPE", "imaging_lens_focal_distance_in_mm", 175.);
    // Default_Pico_param.micro_param.imaging_lens_focal_distance_in_mm);
    Pico_param.micro_param.LED_wavelength = get_config_int("MICROSCOPE", "LED_wavelength", 665);
    // Default_Pico_param.micro_param.LED_wavelength);
    if (Pico_param.micro_param.microscope_factor < 0.5 || Pico_param.micro_param.microscope_factor > 1.5)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value for micro_param.microscope_factor %g replaced by 1.0\n"
		 ,Pico_param.micro_param.microscope_factor);
	Pico_param.micro_param.microscope_factor = 1;
      }
    if (Pico_param.micro_param.imaging_lens_focal_distance_in_mm < 20)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value for micro_param.imaging_lens_focal_distance_in_mm %g replaced by 125.0\n"
		 ,Pico_param.micro_param.imaging_lens_focal_distance_in_mm);
	Pico_param.micro_param.imaging_lens_focal_distance_in_mm = 125;
      }
    if (Pico_param.micro_param.zoom_factor < 0.5 || Pico_param.micro_param.zoom_factor >= 2.5)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value for micro_param.zoom_factor %g replaced by 1.0\n"
		 ,Pico_param.micro_param.zoom_factor);
	Pico_param.micro_param.zoom_factor = 1;
      }

    Close_Pico_config_file();
    return D_O_K;
}

int load_Pico_Camera_config(void)
{
    Open_Pico_config_file();
    Pico_param.camera_param.config_camera_time
        = strdup(get_config_string("CAMERA", "config_camera_time", "\0"));
    Pico_param.camera_param.camera_manufacturer
        = strdup(get_config_string("CAMERA", "camera_manufacturer", "\0"));
    Pico_param.camera_param.camera_model
        = strdup(get_config_string("CAMERA", "camera_model", "\0"));
    Pico_param.camera_param.camera_frequency_in_Hz
        = get_config_float("CAMERA", "camera_frequency_in_Hz", 30.);
    Pico_param.camera_param.pixel_clock = get_config_float("CAMERA", "pixel_clock", 160.);
    Pico_param.camera_param.camera_config_file
        = strdup(get_config_string("CAMERA", "camera_config_file", "\0"));
    Pico_param.camera_param.camera_software_program
        = strdup(get_config_string("CAMERA", "camera_software_program", "\0"));
    Pico_param.camera_param.x_pixel_2_microns
        = get_config_float("CAMERA", "x_pixel_2_microns", 10.);
    Pico_param.camera_param.y_pixel_2_microns
        = get_config_float("CAMERA", "y_pixel_2_microns", 10.);
    Pico_param.camera_param.pixel_h_in_microns
        = get_config_float("CAMERA", "pixel_h_in_microns", 8.3);
    Pico_param.camera_param.pixel_w_in_microns
        = get_config_float("CAMERA", "pixel_w_in_microns", 8.3);
    Pico_param.camera_param.nb_pxl_x = get_config_int("CAMERA", "nb_pxl_x", 768);
    Pico_param.camera_param.nb_pxl_y = get_config_int("CAMERA", "nb_pxl_y", 512);
    Pico_param.camera_param.nb_bits = get_config_int("CAMERA", "nb_bits", 8);
    Pico_param.camera_param.interlace = get_config_int("CAMERA", "interlace", 0);
    Pico_param.camera_param.time_opening_factor
        = get_config_float("CAMERA", "time_opening_factor", 0.97);
    Pico_param.camera_param.shutter_time_us = get_config_int("CAMERA", "shutter_time_us", 0);

    if (Pico_param.camera_param.camera_frequency_in_Hz < 2)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value for camera_param.camera_frequency_in_Hz %g replaced by 20 Hz\n"
		 ,Pico_param.camera_param.camera_frequency_in_Hz);
	Pico_param.camera_param.camera_frequency_in_Hz = 20;
      }
    if (Pico_param.camera_param.pixel_clock < 2)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value for camera_param.pixel_clock %g replaced by 274 MHz\n"
		 ,Pico_param.camera_param.pixel_clock);
	Pico_param.camera_param.pixel_clock = 274;
      }

    Close_Pico_config_file();
    return D_O_K;
}

int load_Pico_Obj_config(void)
{
    Open_Pico_config_file();
    Pico_param.obj_param.config_objective_time
        = strdup(get_config_string("OBJECTIVE", "config_objective_time", "\0"));
    Pico_param.obj_param.objective_manufacturer
        = strdup(get_config_string("OBJECTIVE", "objective_manufacturer", "\0"));
    Pico_param.obj_param.objective_magnification
        = get_config_float("OBJECTIVE", "objective_magnification", 100.);
    Pico_param.obj_param.immersion_type = get_config_int("OBJECTIVE", "immersion_type", 2);
    Pico_param.obj_param.objective_numerical_aperture
        = get_config_float("OBJECTIVE", "objective_numerical_aperture", 1.2);
    Pico_param.obj_param.immersion_index = get_config_float("OBJECTIVE", "immersion_index", 1.518);
    Pico_param.obj_param.buffer_index = get_config_float("OBJECTIVE", "buffer_index", 1.33);
    if (Pico_param.obj_param.objective_magnification < 5)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value objective_magnification %g replaced by 100x NA = 1.2 oil immersion\n"
		 ,Pico_param.obj_param.objective_magnification);
	Pico_param.obj_param.objective_magnification = 100;
	Pico_param.obj_param.objective_numerical_aperture = 1.2;
	Pico_param.obj_param.immersion_type = 2;
	Pico_param.obj_param.immersion_index = 1.518;
	Pico_param.obj_param.buffer_index = 1.33;
      }
    Close_Pico_config_file();
    return D_O_K;
}

int load_Pico_Focus_config(void)
{
    Open_Pico_config_file();
    Pico_param.focus_param.config_focus_time
        = strdup(get_config_string("FOCUS", "config_focus_time", "\0"));
    Pico_param.focus_param.Focus_is_piezzo = get_config_int("FOCUS", "Focus_is_piezzo", 1);
    Pico_param.focus_param.Focus_model = strdup(get_config_string("FOCUS", "Focus_model", "pico"));//dummy
    Pico_param.focus_param.Focus_max_expansion
        = get_config_float("FOCUS", "Focus_max_expansion", 250);
    if (Pico_param.focus_param.Focus_max_expansion < 10)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value of Focus_max_expansion %g replaced by 250 microns\n"
		 ,Pico_param.focus_param.Focus_max_expansion);
	Pico_param.focus_param.Focus_max_expansion = 250;
      }
    Pico_param.focus_param.Focus_driver
      = strdup(get_config_string("FOCUS", "Focus_driver", "pico")); //dummy
    Pico_param.focus_param.Focus_direction = get_config_int("FOCUS", "Focus_direction", 1);
    // set_focus_device(&(Pico_param.focus_param));
    Close_Pico_config_file();
    // win_printf("Focus
    // driver\n%s\nmodel\n%s\n",Pico_param.focus_param.Focus_driver,Pico_param.focus_param.Focus_model);
    return D_O_K;
}

int load_Pico_Rotation_Motor_config(void)
{
    Open_Pico_config_file();
    Pico_param.rotation_motor_param.config_rotation_motor_time
        = strdup(get_config_string("ROTATION_MOTOR", "config_rotation_motor_time", "\0"));
    Pico_param.rotation_motor_param.rotation_motor_controller
      = strdup(get_config_string("ROTATION_MOTOR", "rotation_motor_controller", "pico"));//dummy
    Pico_param.rotation_motor_param.rotation_motor_manufacturer
      = strdup(get_config_string("ROTATION_MOTOR", "rotation_motor_manufacturer", "pico"));//dummy
    Pico_param.rotation_motor_param.rotation_motor_model
      = strdup(get_config_string("ROTATION_MOTOR", "rotation_motor_model", "pico"));// dummy
    Pico_param.rotation_motor_param.gear_ratio
        = get_config_float("ROTATION_MOTOR", "gear_ratio", 1.0);
    Pico_param.rotation_motor_param.P_rotation_motor
        = get_config_float("ROTATION_MOTOR", "P_rotation_motor", 200.);
    Pico_param.rotation_motor_param.I_rotation_motor
        = get_config_float("ROTATION_MOTOR", "I_rotation_motor", 100.);
    Pico_param.rotation_motor_param.D_rotation_motor
        = get_config_float("ROTATION_MOTOR", "D_rotation_motor", 50.);
    Pico_param.rotation_motor_param.rotation_max_velocity
        = get_config_float("ROTATION_MOTOR", "rotation_max_velocity", 1.);
    Pico_param.rotation_motor_param.rotation_velocity
        = get_config_float("ROTATION_MOTOR", "rotation_velocity", 1.);
    Pico_param.rotation_motor_param.rotation_motor_position
        = get_config_float("ROTATION_MOTOR", "rotation_motor_position", 0);
    Pico_param.rotation_motor_param.nb_pulses_per_turn
        = get_config_int("ROTATION_MOTOR", "nb_pulses_per_turn", 8000);
    if (Pico_param.rotation_motor_param.nb_pulses_per_turn < 1)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value for rotation_motor.nb_pulses_per_turn %d replaced by 8000\n"
		 ,Pico_param.rotation_motor_param.nb_pulses_per_turn);
	Pico_param.rotation_motor_param.nb_pulses_per_turn = 8000;
      }
    if (Pico_param.rotation_motor_param.rotation_max_velocity <= 0)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value for rotation_motor_param.rotation_max_velocity %g replaced by 5\n"
		 ,Pico_param.rotation_motor_param.rotation_max_velocity);
	Pico_param.rotation_motor_param.rotation_max_velocity = 5;
      }
    if (Pico_param.rotation_motor_param.gear_ratio <= 0)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value for rotation_motor.gear_ratio %g replaced by 1\n"
		 ,Pico_param.rotation_motor_param.gear_ratio);
	Pico_param.rotation_motor_param.gear_ratio = 1;
      }

    return D_O_K;
}

int load_Pico_Translation_Motor_config(void)
{
    Open_Pico_config_file();
    Pico_param.translation_motor_param.config_translation_motor_time
        = strdup(get_config_string("TRANSLATION_MOTOR", "config_translation_motor_time", "\0"));
    Pico_param.translation_motor_param.translation_motor_inverted
        = get_config_int("TRANSLATION_MOTOR", "translation_motor_inverted", 1);
    Pico_param.translation_motor_param.translation_max_velocity
        = get_config_float("TRANSLATION_MOTOR", "translation_max_velocity", 15.);
    Pico_param.translation_motor_param.translation_velocity
        = get_config_float("TRANSLATION_MOTOR", "translation_velocity", 1);
    Pico_param.translation_motor_param.translation_motor_position
        = get_config_float("TRANSLATION_MOTOR", "translation_motor_position", 0);
    Pico_param.translation_motor_param.translation_max_pos
        = get_config_float("TRANSLATION_MOTOR", "translation_max_pos", 25);
    Pico_param.translation_motor_param.translation_motor_manufacturer
      = strdup(get_config_string("TRANSLATION_MOTOR", "translation_motor_manufacturer", "pico"));// dummy
    Pico_param.translation_motor_param.translation_motor_model
      = strdup(get_config_string("TRANSLATION_MOTOR", "translation_motor_model", "pico"));//dummy
    Pico_param.translation_motor_param.P_translation_motor
        = get_config_float("TRANSLATION_MOTOR", "P_translation_motor", 200);
    Pico_param.translation_motor_param.I_translation_motor
        = get_config_float("TRANSLATION_MOTOR", "I_translation_motor", 100);
    Pico_param.translation_motor_param.D_translation_motor
        = get_config_float("TRANSLATION_MOTOR", "D_translation_motor", 50);
    Pico_param.translation_motor_param.translation_motor_position
        = get_config_float("TRANSLATION_MOTOR", "translation_motor_position", 0);
    Pico_param.translation_motor_param.translation_motor_controller
      = strdup(get_config_string("TRANSLATION_MOTOR", "translation_motor_controller", "pico"));// dummy
    Pico_param.translation_motor_param.Inductive_sensor_servo_voltage
        = get_config_float("TRANSLATION_MOTOR", "Inductive_sensor_servo_voltage", 0);
    Pico_param.translation_motor_param.zmag_ref_for_inductive_test
        = get_config_float("TRANSLATION_MOTOR", "zmag_ref_for_inductive_test", 0.5);
    Pico_param.translation_motor_param.Vcap_at_zmag_ref_test
        = get_config_float("TRANSLATION_MOTOR", "Vcap_at_zmag_ref_test", 0);
    Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a0
        = get_config_float("TRANSLATION_MOTOR", "Zmag_vs_Vcap_poly_a0", 0);
    Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a1
        = get_config_float("TRANSLATION_MOTOR", "Zmag_vs_Vcap_poly_a1", 0);
    Pico_param.translation_motor_param.Zmag_vs_Vcap_poly_a2
        = get_config_float("TRANSLATION_MOTOR", "Zmag_vs_Vcap_poly_a2", 0);
    Pico_param.translation_motor_param.nb_pulses_per_mm
        = get_config_int("TRANSLATION_MOTOR", "nb_pulses_per_mm", 8000);
    Pico_param.translation_motor_param.zmag_offset
        = get_config_float("TRANSLATION_MOTOR", "zmag_offset", 0.0);
    Pico_param.translation_motor_param.motor_range
        = get_config_float("TRANSLATION_MOTOR", "motor_range", 6.0);
  Pico_param.translation_motor_param.last_saved_z_position
        = get_config_float("TRANSLATION_MOTOR", "last_saved_z_position", 2.0);
    Pico_param.translation_motor_param.motor_backslash
        = get_config_float("TRANSLATION_MOTOR", "motor_backslash", 0.05);
    Pico_param.translation_motor_param.motor_serial_nb
        = get_config_int("TRANSLATION_MOTOR", "motor_serial_nb", 800000);
    Pico_param.translation_motor_param.ref_limit_switch
        = get_config_int("TRANSLATION_MOTOR", "ref_limit_switch", 1);
#ifdef ZMAG_FACTOR
    Pico_param.translation_motor_param.zmag_factor = get_config_float("TRANSLATION_MOTOR", "zmag_factor", 19.0);   //tv 090317 : debug in pico
#endif

    if (Pico_param.translation_motor_param.nb_pulses_per_mm < 1)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value for camera_param.nb_pulses_per_mm %d replaced by 8000\n"
		 ,Pico_param.translation_motor_param.nb_pulses_per_mm);
	Pico_param.translation_motor_param.nb_pulses_per_mm = 8000;
      }
    if (Pico_param.translation_motor_param.translation_max_velocity <= 0)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value for translation_max_velocity %g replaced by 10 mm/s\n"
		 ,Pico_param.translation_motor_param.translation_max_velocity);
	Pico_param.translation_motor_param.translation_max_velocity = 10;
      }

	Close_Pico_config_file();
    return D_O_K;
}

int load_Pico_Serial_config(void)
{
    Open_Pico_config_file();
    Pico_param.serial_param.config_serial_time
        = strdup(get_config_string("SERIAL_COM", "config_serial_time", "\0"));
    Pico_param.serial_param.serial_port = get_config_int("SERIAL_COM", "serial_port", 1);
    Pico_param.serial_param.serial_port_str
        = strdup(get_config_string("SERIAL_COM", "serial_port_str", "/dev/ttyS0"));
    Pico_param.serial_param.serial_BaudRate
        = get_config_int("SERIAL_COM", "serial_BaudRate", 57600);
    Pico_param.serial_param.serial_ByteSize = get_config_int("SERIAL_COM", "serial_ByteSize", 8);
    Pico_param.serial_param.serial_StopBits = get_config_int("SERIAL_COM", "serial_StopBits", 1);
    Pico_param.serial_param.serial_Parity = get_config_int("SERIAL_COM", "serial_Parity", 0);
    Pico_param.serial_param.serial_fDtrControl
        = get_config_int("SERIAL_COM", "serial_fDtrControl", 1);
    Pico_param.serial_param.serial_fRtsControl
        = get_config_int("SERIAL_COM", "serial_fRtsControl", 1);
    Pico_param.serial_param.firmware_version
        = strdup(get_config_string("SERIAL_COM", "firmware_version", "\0"));
    Pico_param.serial_param.rs232_started_fine
        = get_config_int("SERIAL_COM", "rs232_started_fine", 0);

    if (Pico_param.serial_param.serial_port <= 0)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value for serial_param.serial_port %d replaced by 1\n"
		 ,Pico_param.serial_param.serial_port);
	Pico_param.serial_param.serial_port = 1;
      }
    if (Pico_param.serial_param.serial_BaudRate < 57600)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value for serial_param.BaudRate %d replaced by 57600\n"
		 ,Pico_param.serial_param.serial_BaudRate);
	Pico_param.serial_param.serial_BaudRate = 57600;
      }
    if (Pico_param.serial_param.serial_ByteSize != 8)
      {
	snprintf(cfg_warning + strlen(cfg_warning),sizeof(cfg_warning)-strlen(cfg_warning),
		 "Incompatible value for serial_param.ByteSize %d replaced by 8, baudrate = 57600, port 1\n"
		 ,Pico_param.serial_param.serial_ByteSize);
	Pico_param.serial_param.serial_ByteSize = 8;
	Pico_param.serial_param.serial_port = 1;
	Pico_param.serial_param.serial_port_str = "/dev/ttyS0";
	Pico_param.serial_param.serial_BaudRate = 57600;
	Pico_param.serial_param.serial_StopBits = 1;
	Pico_param.serial_param.serial_Parity = 0;
	Pico_param.serial_param.serial_fDtrControl = 1;
	Pico_param.serial_param.serial_fRtsControl = 1;
      }


    Close_Pico_config_file();
    return D_O_K;
}

int load_Pico_Expt_config(void)
{
    const char reagent_prefix[] = "reagent_name_";
    char buf[2048] = { 0 };
    Open_Pico_config_file();

    Pico_param.expt_param.config_expt_time
        = strdup(get_config_string("EXPERIMENT", "config_expt_time", ""));
    Pico_param.expt_param.molecule_name
        = strdup(get_config_string("EXPERIMENT", "molecule_name", ""));
    Pico_param.expt_param.bead_size = get_config_float("EXPERIMENT", "bead_size", 1.0);
    Pico_param.expt_param.molecule_extension
        = get_config_float("EXPERIMENT", "molecule_extension", 4.5);
    Pico_param.expt_param.scan_number = get_config_int("EXPERIMENT", "scan_number", 0);
    Pico_param.expt_param.bead_number = get_config_int("EXPERIMENT", "bead_number", 0);
    Pico_param.expt_param.project = strdup(get_config_string("EXPERIMENT", "project", ""));
    Pico_param.expt_param.experiment_set
        = strdup(get_config_string("EXPERIMENT", "experiment_set", ""));
    Pico_param.expt_param.experiment_name
        = strdup(get_config_string("EXPERIMENT", "experiment_name", ""));
    Pico_param.expt_param.user_firstname
        = strdup(get_config_string("EXPERIMENT", "user_firstname", ""));
    Pico_param.expt_param.user_lastname
        = strdup(get_config_string("EXPERIMENT", "user_lastname", ""));
    Pico_param.expt_param.experiment_filename
        = strdup(get_config_string("EXPERIMENT", "experiment_filename", ""));
    Pico_param.expt_param.experiment_type
        = strdup(get_config_string("EXPERIMENT", "experiment_type", " "));
    Pico_param.expt_param.buffer_name = strdup(get_config_string("EXPERIMENT", "buffer_name", ""));
    Pico_param.expt_param.linker_name = strdup(get_config_string("EXPERIMENT", "linker_name", ""));
    Pico_param.expt_param.name_template = strdup(get_config_string("EXPERIMENT", "name_template",
                                          "{{project}}_{{set}}_{{name}}_{{type}}_{{reagents_name}}"));
    Pico_param.expt_param.reagents_name_size
        = get_config_int("EXPERIMENT", "reagents_name_size", 0);
    Pico_param.expt_param.reagents_name = (char **)realloc(Pico_param.expt_param.reagents_name,
                                          Pico_param.expt_param.reagents_name_size * sizeof(char *));

    for (int i = 0; i < Pico_param.expt_param.reagents_name_size; ++i)
    {
        snprintf(buf, sizeof(buf), "%s%d", reagent_prefix, i);
        Pico_param.expt_param.reagents_name[i] = strdup(get_config_string("EXPERIMENT", buf, ""));
    }

    Close_Pico_config_file();
    return D_O_K;
}

int load_pico_config_file_Magnet_parameters(void)
{
    //  Open_Pico_config_file();
    int i, j;
    char ch[128];
    Open_Pico_config_file();
    Pico_param.magnet_param.config_magnet_time
        = strdup(get_config_string("MAGNET", "config_magnet_time", "\0"));
    Pico_param.magnet_param.magnet_name = strdup(get_config_string("MAGNET", "magnet_name", "\0"));
    Pico_param.magnet_param.manufacturer
        = strdup(get_config_string("MAGNET", "manufacturer", "\0"));
    Pico_param.magnet_param.series_nb = get_config_int("MAGNET", "series_nb", 1);
    Pico_param.magnet_param.gap = get_config_float("MAGNET", "gap", 1);
    Pico_param.magnet_param.zmag_contact_sample
        = get_config_float("MAGNET", "zmag_contact_sample", 0);
    Pico_param.magnet_param.nb = get_config_int("MAGNET", "nb", 1);

    for (i = 0; i < Pico_param.magnet_param.nb && i < 6; i++)
    {
        snprintf(ch, 128, "nd_%d", i);
        Pico_param.magnet_param.nd[i] = get_config_int("MAGNET", ch, 3);
        snprintf(ch, 128, "f0_%d", i);
        Pico_param.magnet_param.f0[i] = get_config_float("MAGNET", ch, 12);

        for (j = 1; j < Pico_param.magnet_param.nd[i] && j < 5; j++)
        {
            if (j == 1)
            {
                snprintf(ch, 128, "a1_%d", i);
                Pico_param.magnet_param.a1[i] = get_config_float("MAGNET", ch, 1.9);
            }

            if (j == 2)
            {
                snprintf(ch, 128, "a2_%d", i);
                Pico_param.magnet_param.a2[i] = get_config_float("MAGNET", ch, 0.03);
            }

            if (j == 3)
            {
                snprintf(ch, 128, "a3_%d", i);
                Pico_param.magnet_param.a3[i] = get_config_float("MAGNET", ch, 0);
            }

            if (j == 4)
            {
                snprintf(ch, 128, "a4_%d", i);
                Pico_param.magnet_param.a4[i] = get_config_float("MAGNET", ch, 0);
            }
        }
    }

    Close_Pico_config_file();
    return D_O_K;
}

int load_Pico_DNA_molecule_config(void)
{
    Open_Pico_config_file();
    Pico_param.dna_molecule.config_molecule_time
        = strdup(get_config_string("MOLECULE", "config_molecule_time", "\0"));
    Pico_param.dna_molecule.molecule_name
        = strdup(get_config_string("MOLECULE", "molecule_name", "\0"));
    Pico_param.dna_molecule.ssDNA_molecule_extension
        = get_config_float("MOLECULE", "ssDNA_molecule_extension", 0.0);
    Pico_param.dna_molecule.dsDNA_molecule_extension
        = get_config_float("MOLECULE", "dsDNA_molecule_extension", 4.5);
    Pico_param.dna_molecule.dsxi = get_config_float("MOLECULE", "dsxi", 53);
    Pico_param.dna_molecule.nick_proba = get_config_float("MOLECULE", "nick_proba", .5);
    Close_Pico_config_file();
    return D_O_K;
}

int load_Pico_Bead_param(void)
{
    //  Open_Pico_config_file();
    Pico_param.bead_param.config_bead_time
        = strdup(get_config_string("BEAD", "config_bead_time", "\0"));
    Pico_param.bead_param.bead_name = strdup(get_config_string("BEAD", "bead_name", "\0"));
    // win_printf("Load Bead name %s",Pico_param.bead_param.bead_name);
    Pico_param.bead_param.manufacturer = strdup(get_config_string("BEAD", "manufacturer", "\0"));
    Pico_param.bead_param.radius = get_config_float("BEAD", "radius", 0.5);
    Pico_param.bead_param.low_field_suceptibility
        = get_config_float("BEAD", "low_field_suceptibility", 0);
    Pico_param.bead_param.saturating_magnetization
        = get_config_float("BEAD", "saturating_magnetization", 0);
    Pico_param.bead_param.saturating_field = get_config_float("BEAD", "saturating_field", 0);
    return D_O_K;
}

int load_Pico_config(void)
{
    if (updating_menu_state != 0)
    {
        return D_O_K;
    }
    cfg_warning[0] = 0;  // we erase warning

    load_Pico_Micro_config();
    load_Pico_Camera_config();
    load_Pico_Obj_config();
    load_Pico_Focus_config();
    load_Pico_Translation_Motor_config();
    load_Pico_Rotation_Motor_config();
    load_Pico_Serial_config();
    load_Pico_Expt_config();
    load_pico_config_file_Magnet_parameters();
    load_Pico_DNA_molecule_config();
    load_Pico_Bead_param();
#ifdef SDI_VERSION
	load_Pico_Sdi_config();
#endif
#ifdef SDI_VERSION_2
	load_Pico_SDI_2_config();
#endif
    // TO REMOVE AFTER TEST
    write_Pico_config_file();
    if (strlen(cfg_warning) > 0)
      {
	win_printf("Your configuration file contains incompatible parameters that have been replaced by\n"
		   "more realistic values (see below). This indicates that your configuration file:\n"
		   "%s has been probably partially erased during a crash.\n"
		   "Please consider reinitializing your configuration file, you can extract your last valid\n"
		   "configuration file from le last valid trk file recorded. Find below warnings\n%s"
		   ,pico_config_filename,cfg_warning);
      }

    return D_O_K;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// WE GO TO THE MODIFICATION
// PART/////////////////////////////////////////////////////////////////////
#ifdef SDI_VERSION
int change_sdi_config(void)
{
    int change_button = 1;
    Sdi_param tmp_sdi_param;
    //long lTime, lTimeb;
    //char question[1024], name[128], manufacturer[128], model[128];
    if (updating_menu_state != 0)
    {
        snprintf(active_menu->text, 128, "Modify sdi parameters");
        return D_O_K;
    }
    duplicate_pico_Sdi_param(&Pico_param.sdi_param, &tmp_sdi_param);
    if(win_scanf("Design of slits : internal slit at %4fmm, external at %4f, width of %3f, height of %3f, n=%3f\n"
				"Z phase velocity = %5f, X phase velocity = %5f (rad per mic), Group velocity %5f (pix per mic)\n"
				"Frequency of the fringes %3f (pix -1)\n"
                 "{\\color{lightred}Do you want to change these settings?} %R YES %r NO\n",
				&tmp_sdi_param.x1, &tmp_sdi_param.x2, &tmp_sdi_param.wx, &tmp_sdi_param.wy, &tmp_sdi_param.n_index,
				&tmp_sdi_param.v_0, &tmp_sdi_param.v_x, &tmp_sdi_param.v_1, &tmp_sdi_param.nu0,
				&change_button)==WIN_CANCEL) return D_O_K;

    if (change_button == 0)
    {
        change_button = 1;
        if(win_scanf("Are you really sure you want to change the settings \n"
                      "This may have unwanted effects \n %R YES %r NO\n",
                      &change_button) == WIN_CANCEL) return D_O_K;
        if (change_button == 0)
        {
            duplicate_pico_Sdi_param(&tmp_sdi_param, &Pico_param.sdi_param);
            //lTimeb = time(&lTime);
            write_Pico_config_file();
        }
    }
    return D_O_K;
}
#endif

int change_microscope_config(void)
{
    int change_button = 1, j;
    Micro_param tmp_pico_param;
    int i;
    long lTime, lTimeb;
    //  load_Pico_config();
    char question[1024], name[128], manufacturer[128], model[128], *s;

    if (updating_menu_state != 0)
    {
        // micro
        if (Pico_param.micro_param.microscope_user_name != NULL
                && strlen(Pico_param.micro_param.microscope_user_name) > 3)
            snprintf(active_menu->text, 128, "Modify %s microscope (factor %g)",
                     Pico_param.micro_param.microscope_user_name,
                     Pico_param.micro_param.microscope_factor);
        else
            snprintf(active_menu->text, 128, "Modify Dummy microscope (factor %g)",
                     Pico_param.micro_param.microscope_factor);

        return D_O_K;
    }

    duplicate_pico_Micro_param(&Pico_param.micro_param, &tmp_pico_param);

    if (tmp_pico_param.microscope_type == 1)
    {
        snprintf(question, 1024,
                 "Last modification %s\n"
                 "\n User name %%20s \n"
                 "Type %%R Commercial %%r PicoTwist %%r Homemade \n"
                 "Model %%20s \n"
                 "Microscope Factor %%4f \n"
                 "Lens focal distance %%4f mm\n"
                 "LED Wavelength %%5d nn\n "
                 "{\\color{lightred}Do you want to change these settings?} %%R YES %%r NO\n",
                 Pico_param.micro_param.config_microscope_time);
        strncpy(name, tmp_pico_param.microscope_user_name, 128);
        strncpy(model, tmp_pico_param.PicoTwist_model, 128);
        i = win_scanf(question,
                      name,                                              //
                      &tmp_pico_param.microscope_type,                   //
                      model,                                             //
                      &tmp_pico_param.microscope_factor,                 //
                      &tmp_pico_param.imaging_lens_focal_distance_in_mm, //
                      &tmp_pico_param.LED_wavelength,                    //
                      &change_button);                                   //

        if (tmp_pico_param.microscope_user_name)
        {
            free(tmp_pico_param.microscope_user_name);
        }

        tmp_pico_param.microscope_user_name = strdup(name);

        if (tmp_pico_param.PicoTwist_model)
        {
            free(tmp_pico_param.PicoTwist_model);
        }

        tmp_pico_param.PicoTwist_model = strdup(model);

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }
    }
    else
    {
        snprintf(question, 1024,
                 "Last modification %s\n"
                 "\n User name %%20s \n Manufacturer %%20s \n"
                 "Type %%R Commercial %%r PicoTwist %%r Homemade \n"
                 "Model %%20s \n Zoom Factor %%8f "
                 "Min %%4f  Max %%4f \n"
                 "Field Factor %%4f  Microscope Factor %%4f \n"
                 "Lens focal distance %%4f mm\n"
                 "LED Wavelength %%5d nn\n "
                 "{\\color{lightred}Do you want to change these settings?} %%R YES %%r NO\n",
                 Pico_param.micro_param.config_microscope_time);
        strncpy(name, tmp_pico_param.microscope_user_name, 128);
        strncpy(manufacturer, tmp_pico_param.microscope_manufacturer_name, 128);
        strncpy(model, tmp_pico_param.PicoTwist_model, 128);
        i = win_scanf(question,
                      name,                                              //
                      manufacturer,                                      //
                      &tmp_pico_param.microscope_type,                   //
                      model,                                             //
                      &tmp_pico_param.zoom_factor,                       //
                      &tmp_pico_param.zoom_min,                          //
                      &tmp_pico_param.zoom_max,                          //
                      &tmp_pico_param.field_factor,                      //
                      &tmp_pico_param.microscope_factor,                 //
                      &tmp_pico_param.imaging_lens_focal_distance_in_mm, //
                      &tmp_pico_param.LED_wavelength,                    //
                      &change_button);                                   //

        if (tmp_pico_param.microscope_user_name)
        {
            free(tmp_pico_param.microscope_user_name);
        }

        tmp_pico_param.microscope_user_name = strdup(name);

        if (tmp_pico_param.microscope_manufacturer_name)
        {
            free(tmp_pico_param.microscope_manufacturer_name);
        }

        tmp_pico_param.microscope_manufacturer_name = strdup(manufacturer);

        if (tmp_pico_param.PicoTwist_model)
        {
            free(tmp_pico_param.PicoTwist_model);
        }

        tmp_pico_param.PicoTwist_model = strdup(model);

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }
    }

    if (change_button == 0 && i != WIN_CANCEL)
    {
        change_button = 1;
        i = win_scanf("Are you really sure you want to change the settings \n"
                      "This may have unwanted effects \n %R YES %r NO\n",
                      &change_button);

        if (change_button == 0 && i != WIN_CANCEL)
        {
            duplicate_pico_Micro_param(&tmp_pico_param, &Pico_param.micro_param);
            lTimeb = time(&lTime);

            if (Pico_param.micro_param.config_microscope_time)
            {
                free(Pico_param.micro_param.config_microscope_time);
            }

            s = Pico_param.micro_param.config_microscope_time = strdup(ctime(&lTimeb));

            for (j = 0; s[j] != 0; j++)
            {
                s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
            }

            write_Pico_config_file();
        }
    }

    if (tmp_pico_param.microscope_user_name)
    {
        free(tmp_pico_param.microscope_user_name);
    }

    if (tmp_pico_param.microscope_manufacturer_name)
    {
        free(tmp_pico_param.microscope_manufacturer_name);
    }

    if (tmp_pico_param.PicoTwist_model)
    {
        free(tmp_pico_param.PicoTwist_model);
    }

    if (tmp_pico_param.config_microscope_time)
    {
        free(tmp_pico_param.config_microscope_time);
    }

    return D_O_K;
}

int change_objective_config(void)
{
    int change_button = 1, j;
    Obj_param tmp_pico_param;
    int i;
    char question[1024], omn1[64], omn2[64], manufacturer[128], *s;
    long lTime, lTimeb;

    if (updating_menu_state != 0)
    {
        // objective
        if (Pico_param.obj_param.immersion_type == 0)
        {
            snprintf(omn2, 64, "Air");
        }
        else if (Pico_param.obj_param.immersion_type == 1)
        {
            snprintf(omn2, 64, "Water");
        }
        else
        {
            snprintf(omn2, 64, "Oil");
        }

        snprintf(omn1, 64, "%gx%g/%s", Pico_param.obj_param.objective_magnification,
                 Pico_param.obj_param.objective_numerical_aperture, omn2);

        if (Pico_param.obj_param.objective_manufacturer != NULL
                && strlen(Pico_param.obj_param.objective_manufacturer) > 2)
            snprintf(active_menu->text, 128, "Modify %s objective %s",
                     Pico_param.obj_param.objective_manufacturer, omn1);
        else
        {
            snprintf(active_menu->text, 128, "Modify Dummy objective %s", omn1);
        }

        return D_O_K;
    }

    duplicate_pico_Obj_param(&Pico_param.obj_param, &tmp_pico_param);
    strncpy(manufacturer, tmp_pico_param.objective_manufacturer, 128);
    snprintf(question, 1024, "Last modification %s\n"
             "Manufacturer %%20s \n"
             "Objective magnification %%4f numerical aperture %%4f\n"
             "immersion_type %%R Air %%r Water %%r Oil \n"
             "For immersion objectives: glass index  %%12f\n"
	     "Buffer index %%f\n"
             "Do you want to change these settings? %%R YES %%r NO\n",
             Pico_param.obj_param.config_objective_time);
    i = win_scanf(question,
                  manufacturer,                            //
                  &tmp_pico_param.objective_magnification, //
                  &tmp_pico_param.objective_numerical_aperture,
                  &tmp_pico_param.immersion_type, //
                  &tmp_pico_param.immersion_index, &tmp_pico_param.buffer_index,
                  &change_button); //

    if (tmp_pico_param.objective_manufacturer)
    {
        free(tmp_pico_param.objective_manufacturer);
    }

    tmp_pico_param.objective_manufacturer = strdup(manufacturer);

    if (change_button == 0 && i != WIN_CANCEL)
    {
        change_button = 1;
        i = win_scanf("Are you really sure you want to change the settings \n"
                      "This may have unwanted effects \n %R YES %r NO\n",
                      &change_button);

        if (change_button == 0 && i != WIN_CANCEL)
        {
            duplicate_pico_Obj_param(&tmp_pico_param, &Pico_param.obj_param);
            lTimeb = time(&lTime);

            if (Pico_param.obj_param.config_objective_time)
            {
                free(Pico_param.obj_param.config_objective_time);
            }

            s = Pico_param.obj_param.config_objective_time = strdup(ctime(&lTimeb));

            for (j = 0; s[j] != 0; j++)
            {
                s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
            }

            write_Pico_config_file();
            do_update_menu();
        }
    }

    if (tmp_pico_param.objective_manufacturer)
    {
        free(tmp_pico_param.objective_manufacturer);
    }

    if (tmp_pico_param.config_objective_time)
    {
        free(tmp_pico_param.config_objective_time);
    }

    return D_O_K;
}

int change_camera_config(void)
{
    int change_button = 1, j;
    Camera_param tmp_pico_param;
    int i;
    char question[1024], manufacturer[128], model[128], config[128], *s;
    long lTime, lTimeb;

    if (updating_menu_state != 0)
    {
        // camera
        if (Pico_param.camera_param.camera_model != NULL
                && strlen(Pico_param.camera_param.camera_model) > 3)
            snprintf(active_menu->text, 128, "Modify %s camera %dx%d (%g Hz)",
                     Pico_param.camera_param.camera_model, Pico_param.camera_param.nb_pxl_x,
                     Pico_param.camera_param.nb_pxl_y, Pico_param.camera_param.camera_frequency_in_Hz);
        else
            snprintf(active_menu->text, 128, "Modify Dummy camera %dx%d (%g Hz)",
                     Pico_param.camera_param.nb_pxl_x, Pico_param.camera_param.nb_pxl_y,
                     Pico_param.camera_param.camera_frequency_in_Hz);

        return D_O_K;
    }

    duplicate_pico_Camera_param(&Pico_param.camera_param, &tmp_pico_param);
    snprintf(question, 1024,
             "Last modification %s\n"
             "Manufacturer %%20s \n  Model %%20s \n"
             "Frequency %%6f \n Config file %%20s \n"
             "Pixel size (in microns) in X %%8f Y %%8f \n"
             "Number of pixel in X %%6d in Y %%6d. Nb. of bits %%4d\n"
             "Is camera interlaced %%R NO %%r Yes\n"
             "Exposition time ratio to period %%8f\n"
             "Shutter time in(\\mu s) %%8d\n"
             "{\\color{lightred}Do you want to change these settings?} %%R YES %%r NO\n",
             Pico_param.camera_param.config_camera_time);
    strncpy(manufacturer, tmp_pico_param.camera_manufacturer, 128);
    strncpy(model, tmp_pico_param.camera_model, 128);
    strncpy(config, tmp_pico_param.camera_config_file, 128);
    i = win_scanf(question,
                  manufacturer,                           //
                  model,                                  //
                  &tmp_pico_param.camera_frequency_in_Hz, //
                  config,
                  &tmp_pico_param.pixel_w_in_microns, //
                  &tmp_pico_param.pixel_h_in_microns, //
                  &tmp_pico_param.nb_pxl_x, &tmp_pico_param.nb_pxl_y, &tmp_pico_param.nb_bits,
                  &tmp_pico_param.interlace, &tmp_pico_param.time_opening_factor,
                  &tmp_pico_param.shutter_time_us,
                  &change_button); //

    if (tmp_pico_param.camera_manufacturer)
    {
        free(tmp_pico_param.camera_manufacturer);
    }

    tmp_pico_param.camera_manufacturer = strdup(manufacturer);

    if (tmp_pico_param.camera_model)
    {
        free(tmp_pico_param.camera_model);
    }

    tmp_pico_param.camera_model = strdup(model);

    if (tmp_pico_param.camera_config_file)
    {
        free(tmp_pico_param.camera_config_file);
    }

    tmp_pico_param.camera_config_file = strdup(config);

    if (change_button == 0 && i != WIN_CANCEL)
    {
        change_button = 1;
        i = win_scanf("Are you really sure you want to change the settings \n"
                      "This may have unwanted effects \n %R YES %r NO\n",
                      &change_button);

        if (change_button == 0 && i != WIN_CANCEL)
        {
            lTimeb = time(&lTime);
            duplicate_pico_Camera_param(&tmp_pico_param, &Pico_param.camera_param);

            if (Pico_param.camera_param.config_camera_time)
            {
                free(Pico_param.camera_param.config_camera_time);
            }

            s = Pico_param.camera_param.config_camera_time = strdup(ctime(&lTimeb));

            for (j = 0; s[j] != 0; j++)
            {
                s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
            }

            write_Pico_config_file();
            do_update_menu();
            // win_printf("camera freq tmp %g, pico %g",tmp_pico_param.camera_frequency_in_Hz,
            //     Pico_param.camera_param.camera_frequency_in_Hz);
        }
    }

    if (tmp_pico_param.config_camera_time)
    {
        free(tmp_pico_param.config_camera_time);
    }

    if (tmp_pico_param.camera_manufacturer)
    {
        free(tmp_pico_param.camera_manufacturer);
    }

    if (tmp_pico_param.camera_model)
    {
        free(tmp_pico_param.camera_model);
    }

    if (tmp_pico_param.camera_config_file)
    {
        free(tmp_pico_param.camera_config_file);
    }

    return D_O_K;
}

int change_focus_config(void)
{
    int change_button = 1, j;
    Focus_param tmp_pico_param;
    int i, is_pico = 0;
    char question[1024], model[128], *s;
    long lTime, lTimeb;

    if (updating_menu_state != 0)
    {
        // focus
        if (Pico_param.focus_param.Focus_driver != NULL
                && strlen(Pico_param.focus_param.Focus_driver) > 3)
            snprintf(active_menu->text, 128, "Modify %s focus device (%g microns)",
                     Pico_param.focus_param.Focus_driver, Pico_param.focus_param.Focus_max_expansion);
        else
            snprintf(active_menu->text, 128, "Modify Dummy focus device (%g microns)",
                     Pico_param.focus_param.Focus_max_expansion);

        return D_O_K;
    }

    duplicate_pico_Focus_param(&Pico_param.focus_param, &tmp_pico_param);

    if (strncmp(tmp_pico_param.Focus_driver, "pico", 4) == 0)
    {
        is_pico = 1;
    }

    snprintf(question, 1024,
             "Last modification %s\n"
             "Simulating focus device %%R or using Picotwist serial interface %%r\n"
             "Are you using a piezzo %%R Yes %%r No \n  Model %%20s \n"
             "What is its maximum expansion in \\mu m %%6f \n"
             "What is its direction ? %%R UP %%r DOWN \n"
             "{\\color{lightred}Do you want to change these settings?} %%R YES %%r NO\n",
             Pico_param.focus_param.config_focus_time);
    strncpy(model, tmp_pico_param.Focus_model, 128);
    i = win_scanf(question, &is_pico,
                  &tmp_pico_param.Focus_is_piezzo,     //
                  model,                               //
                  &tmp_pico_param.Focus_max_expansion, //
                  &tmp_pico_param.Focus_direction,
                  &change_button); //

    if (tmp_pico_param.Focus_model)
    {
        free(tmp_pico_param.Focus_model);
    }

    tmp_pico_param.Focus_model = strdup(model);

    if (tmp_pico_param.Focus_driver)
    {
        free(tmp_pico_param.Focus_driver);
    }

    if (is_pico)
    {
        tmp_pico_param.Focus_driver = strdup("pico");
    }
    else
    {
        tmp_pico_param.Focus_driver = strdup("dummy");
    }

    if (change_button == 0 && i != WIN_CANCEL)
    {
        change_button = 1;
        i = win_scanf("Are you really sure you want to change the settings \n"
                      "This may have unwanted effects \n %R YES %r NO\n",
                      &change_button);

        if (change_button == 0 && i != WIN_CANCEL)
        {
            lTimeb = time(&lTime);
            duplicate_pico_Focus_param(&tmp_pico_param, &Pico_param.focus_param);

            if (Pico_param.focus_param.config_focus_time)
            {
                free(Pico_param.focus_param.config_focus_time);
            }

            s = Pico_param.focus_param.config_focus_time = strdup(ctime(&lTimeb));

            for (j = 0; s[j] != 0; j++)
            {
                s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
            }

            write_Pico_config_file();
            do_update_menu();
        }
    }

    if (tmp_pico_param.Focus_model)
    {
        free(tmp_pico_param.Focus_model);
    }

    if (tmp_pico_param.Focus_driver)
    {
        free(tmp_pico_param.Focus_driver);
    }

    if (tmp_pico_param.config_focus_time)
    {
        free(tmp_pico_param.config_focus_time);
    }

    load_Pico_Focus_config();
    return D_O_K;
}

int change_translation_motor_config(void)
{
    int change_button = 1, j;
    Translation_Motor_param tmp_pico_param;
    int i, is_pico = 0;
    char question[1024], manufacturer[128], model[128], *s;
    long lTime, lTimeb;

    if (updating_menu_state != 0)
    {
        // translation
        if (Pico_param.translation_motor_param.translation_motor_controller != NULL
                && strlen(Pico_param.translation_motor_param.translation_motor_controller) > 3)
            snprintf(active_menu->text, 128, "Modify %s magnets translation device",
                     Pico_param.translation_motor_param.translation_motor_controller);
        else
        {
            snprintf(active_menu->text, 128, "Modify Dummy magnets translation device");
        }

        return D_O_K;
    }

    duplicate_pico_Translation_Motor_param(&Pico_param.translation_motor_param, &tmp_pico_param);

    if (strncmp(tmp_pico_param.translation_motor_controller, "pico", 4) == 0)
    {
        is_pico = 1;
    }

#ifdef ZMAG_FACTOR
	    snprintf(question, 1024,
             "Last modification %s\n"
             "Simulating focus device %%R or using Picotwist serial interface %%r\n"
             "Motor manufacturer %%20s \n  Controller Model %%20s \nSerial Nb %%20d\n"
             "Feedback Parameters P %%4f  I%%4f  D%%4f\n"
             "Translation maximum velocity %%8f \n Translation velocity %%8f\n"
             "Translation Maximum Position %%8f\n"
             "Is the motor inverted %%R YES %%r NO\n"
             "Ref limit switch position %%4d (-1 bottom, 1 top)\n"
             "Offset to the limit switch position %%12f\n"
             "Motor range %%12f backslash %%12f\n"
             "Nb. of sensor ticks per mm of translation %%8d\n"
             "Inductive sensor minimum value  %%8f (Volts) 0-> do not use sensor\n"
             "If you have an Inductive sensor, its indication may be used to check Zmag\n"
             "assuming that Zmag = a_0 + a_1 Vcap + a_2 Vcap^2\n"
             "Define a_0 %%8f (mm)\n"
             "Define a_1 %%8f (mm) 0-> do not use sensor\n"
             "Define a_2 %%8f (mm)\n"
			 "Define zmag factor of translation %%8f \n"
             "{\\color{lightred}Do you want to change these settings?} %%R YES %%r NO\n",
             Pico_param.translation_motor_param.config_translation_motor_time);
    strncpy(manufacturer, tmp_pico_param.translation_motor_manufacturer, 128);
    strncpy(model, tmp_pico_param.translation_motor_model, 128);
    i = win_scanf(question, &is_pico,
                  manufacturer, //
                  model, &tmp_pico_param.motor_serial_nb, &tmp_pico_param.P_translation_motor,
                  &tmp_pico_param.I_translation_motor, &tmp_pico_param.D_translation_motor,
                  &tmp_pico_param.translation_max_velocity, &tmp_pico_param.translation_velocity,
                  &tmp_pico_param.translation_max_pos, &tmp_pico_param.translation_motor_inverted,
                  &tmp_pico_param.ref_limit_switch, &tmp_pico_param.zmag_offset, &tmp_pico_param.motor_range,
                  &tmp_pico_param.motor_backslash, &tmp_pico_param.nb_pulses_per_mm,
                  &tmp_pico_param.Inductive_sensor_servo_voltage, &tmp_pico_param.Zmag_vs_Vcap_poly_a0,
                  &tmp_pico_param.Zmag_vs_Vcap_poly_a1, &tmp_pico_param.Zmag_vs_Vcap_poly_a2, &tmp_pico_param.zmag_factor,
                  &change_button); //
#else
    snprintf(question, 1024,
             "Last modification %s\n"
             "Simulating focus device %%R or using Picotwist serial interface %%r\n"
             "Motor manufacturer %%20s \n  Controller Model %%20s \nSerial Nb %%20d\n"
             "Feedback Parameters P %%4f  I%%4f  D%%4f\n"
             "Translation maximum velocity %%8f \n Translation velocity %%8f\n"
             "Translation Maximum Position %%8f\n"
             "Is the motor inverted %%R YES %%r NO\n"
             "Ref limit switch position %%4d (-1 bottom, 1 top)\n"
             "Offset to the limit switch position %%12f\n"
             "Motor range %%12f backslash %%12f\n"
             "Nb. of sensor ticks per mm of translation %%8d\n"
             "Inductive sensor minimum value  %%8f (Volts) 0-> do not use sensor\n"
             "If you have an Inductive sensor, its indication may be used to check Zmag\n"
             "assuming that Zmag = a_0 + a_1 Vcap + a_2 Vcap^2\n"
             "Define a_0 %%8f (mm)\n"
             "Define a_1 %%8f (mm) 0-> do not use sensor\n"
             "Define a_2 %%8f (mm)\n"
             "{\\color{lightred}Do you want to change these settings?} %%R YES %%r NO\n",
             Pico_param.translation_motor_param.config_translation_motor_time);
    strncpy(manufacturer, tmp_pico_param.translation_motor_manufacturer, 128);
    strncpy(model, tmp_pico_param.translation_motor_model, 128);
    i = win_scanf(question, &is_pico,
                  manufacturer, //
                  model, &tmp_pico_param.motor_serial_nb, &tmp_pico_param.P_translation_motor,
                  &tmp_pico_param.I_translation_motor, &tmp_pico_param.D_translation_motor,
                  &tmp_pico_param.translation_max_velocity, &tmp_pico_param.translation_velocity,
                  &tmp_pico_param.translation_max_pos, &tmp_pico_param.translation_motor_inverted,
                  &tmp_pico_param.ref_limit_switch, &tmp_pico_param.zmag_offset, &tmp_pico_param.motor_range,
                  &tmp_pico_param.motor_backslash, &tmp_pico_param.nb_pulses_per_mm,
                  &tmp_pico_param.Inductive_sensor_servo_voltage, &tmp_pico_param.Zmag_vs_Vcap_poly_a0,
                  &tmp_pico_param.Zmag_vs_Vcap_poly_a1, &tmp_pico_param.Zmag_vs_Vcap_poly_a2,
                  &change_button); //
#endif
    if (tmp_pico_param.translation_motor_manufacturer)
    {
        free(tmp_pico_param.translation_motor_manufacturer);
    }

    tmp_pico_param.translation_motor_manufacturer = strdup(manufacturer);

    if (tmp_pico_param.translation_motor_controller)
    {
        free(tmp_pico_param.translation_motor_controller);
    }

    if (is_pico)
    {
        tmp_pico_param.translation_motor_controller = strdup("pico");
    }
    else
    {
        tmp_pico_param.translation_motor_controller = strdup("dummy");
    }

    if (tmp_pico_param.translation_motor_model)
    {
        free(tmp_pico_param.translation_motor_model);
    }

    tmp_pico_param.translation_motor_model = strdup(model);

    if (change_button == 0 && i != WIN_CANCEL)
    {
        change_button = 1;
        win_scanf("Are you really sure you want to change the settings \n"
                  "This may have unwanted effects \n %R YES %r NO\n",
                  &change_button);

        if (change_button == 0 && i != WIN_CANCEL)
        {
            lTimeb = time(&lTime);
            duplicate_pico_Translation_Motor_param(
                &tmp_pico_param, &Pico_param.translation_motor_param);

            if (Pico_param.translation_motor_param.config_translation_motor_time)
            {
                free(Pico_param.translation_motor_param.config_translation_motor_time);
            }

            s = Pico_param.translation_motor_param.config_translation_motor_time
                = strdup(ctime(&lTimeb));

            for (j = 0; s[j] != 0; j++)
            {
                s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
            }

            write_Pico_config_file();
            do_update_menu();
        }
    }

    if (tmp_pico_param.translation_motor_manufacturer)
    {
        free(tmp_pico_param.translation_motor_manufacturer);
    }

    if (tmp_pico_param.translation_motor_model)
    {
        free(tmp_pico_param.translation_motor_model);
    }

    if (tmp_pico_param.translation_motor_controller)
    {
        free(tmp_pico_param.translation_motor_controller);
    }

    if (tmp_pico_param.config_translation_motor_time)
    {
        free(tmp_pico_param.config_translation_motor_time);
    }

    return D_O_K;
}

int change_rotation_motor_config(void)
{
    int change_button = 1, j;
    Rotation_Motor_param tmp_pico_param;
    int i, is_pico = 0;
    char question[1024], manufacturer[128], model[128], control[128], *s;
    long lTime, lTimeb;

    if (updating_menu_state != 0)
    {
        // rotation
        if (Pico_param.rotation_motor_param.rotation_motor_controller != NULL
                && strlen(Pico_param.rotation_motor_param.rotation_motor_controller) > 3)
            snprintf(active_menu->text, 128, "Modify %s magnets rotation device",
                     Pico_param.rotation_motor_param.rotation_motor_controller);
        else
        {
            snprintf(active_menu->text, 128, "Modify Dummy magnets rotation device");
        }

        return D_O_K;
    }

    duplicate_pico_Rotation_Motor_param(&Pico_param.rotation_motor_param, &tmp_pico_param);

    // win_printf("duplication OK");
    if (strncmp(tmp_pico_param.rotation_motor_controller, "pico", 4) == 0)
    {
        is_pico = 1;
    }

    snprintf(question, 1024,
             "Last modification %s\n"
             "Simulating focus device %%R or using Picotwist serial interface %%r\n"
             "Rotation Motor manufacturer %%20s\n"
             "Motor Model %%20s\n"
             "Gear ratio %%8f\n"
             "Feedback Parameters P %%4f  I%%4f D%%4f\n"
             "Nb. of sensor ticks per turn %%8d\n"
             "Rotation maximum velocity %%6f\nRotation velocity %%6f\n"
             "{\\color{lightred}Do you want to change these settings?} %%R YES %%r NO\n",
             Pico_param.rotation_motor_param.config_rotation_motor_time);
    strncpy(manufacturer, tmp_pico_param.rotation_motor_manufacturer, 128);
    strncpy(model, tmp_pico_param.rotation_motor_model, 128);
    strncpy(control, tmp_pico_param.rotation_motor_controller, 128);
    i = win_scanf(question, &is_pico,
                  manufacturer, //
                  model,        //
                  &tmp_pico_param.gear_ratio, &tmp_pico_param.P_rotation_motor,
                  &tmp_pico_param.I_rotation_motor, &tmp_pico_param.D_rotation_motor,
                  &tmp_pico_param.nb_pulses_per_turn, &tmp_pico_param.rotation_max_velocity,
                  &tmp_pico_param.rotation_velocity,
                  &change_button); //

    if (tmp_pico_param.rotation_motor_manufacturer)
    {
        free(tmp_pico_param.rotation_motor_manufacturer);
    }

    tmp_pico_param.rotation_motor_manufacturer = strdup(manufacturer);

    if (tmp_pico_param.rotation_motor_model)
    {
        free(tmp_pico_param.rotation_motor_model);
    }

    tmp_pico_param.rotation_motor_model = strdup(model);

    if (tmp_pico_param.rotation_motor_controller)
    {
        free(tmp_pico_param.rotation_motor_controller);
    }

    if (is_pico)
    {
        tmp_pico_param.rotation_motor_controller = strdup("pico");
    }
    else
    {
        tmp_pico_param.rotation_motor_controller = strdup("dummy");
    }

    if (change_button == 0 && i != WIN_CANCEL)
    {
        change_button = 1;
        win_scanf("Are you really sure you want to change the settings \n"
                  "This may have unwanted effects \n %R YES %r NO\n",
                  &change_button);

        if (change_button == 0 && i != WIN_CANCEL)
        {
            lTimeb = time(&lTime);
            duplicate_pico_Rotation_Motor_param(&tmp_pico_param, &Pico_param.rotation_motor_param);

            if (Pico_param.rotation_motor_param.config_rotation_motor_time)
            {
                free(Pico_param.rotation_motor_param.config_rotation_motor_time);
            }

            s = Pico_param.rotation_motor_param.config_rotation_motor_time = strdup(ctime(&lTimeb));

            for (j = 0; s[j] != 0; j++)
            {
                s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
            }

            write_Pico_config_file();
            do_update_menu();
        }
    }

    if (tmp_pico_param.rotation_motor_manufacturer)
    {
        free(tmp_pico_param.rotation_motor_manufacturer);
    }

    if (tmp_pico_param.rotation_motor_model)
    {
        free(tmp_pico_param.rotation_motor_model);
    }

    if (tmp_pico_param.rotation_motor_controller)
    {
        free(tmp_pico_param.rotation_motor_controller);
    }

    if (tmp_pico_param.config_rotation_motor_time)
    {
        free(tmp_pico_param.config_rotation_motor_time);
    }

    return D_O_K;
}

#ifdef SDI_VERSION_2
int change_SDI_2_calib_config(void)
{
    int change_button = 1, j;
    int i, is_pico = 0;
    float tmp_SDI_2,tmp_SDI_2_photons;


    if (updating_menu_state != 0)
    {
            return D_O_K;
    }

    tmp_SDI_2 = Pico_param.SDI_2_param.SDI_2_calibration;
    tmp_SDI_2_photons = Pico_param.SDI_2_param.photons_per_level;

    i = win_scanf("SDI 2 calibration parameters \n"
              "Coefficient relating shear and delta z : %f\n"
              "Photons per numerical level %f\n",&tmp_SDI_2,&tmp_SDI_2_photons);
    if (i==WIN_CANCEL) return 0;

    i = win_scanf("Are you really sure you want to change the settings \n"
                  "This may have unwanted effects \n %R YES %r NO\n",
                  &change_button);
    if (i==WIN_CANCEL) return 0;

        if (change_button == 0)
        {
            Pico_param.SDI_2_param.SDI_2_calibration = tmp_SDI_2;
            Pico_param.SDI_2_param.photons_per_level = tmp_SDI_2_photons;
            write_Pico_config_file();
            do_update_menu();
        }


    return D_O_K;
}
#endif

int change_expt_config(void)
{
    int change_button = 0, j;
    int win_answer = D_O_K;
    Expt_param tmp_pico_param = { 0 };
    char firstname[256] = { 0 };
    char lastname[256] = { 0 };
    char mol[128] = { 0 };
    char proj[128] = { 0 };
    char expt_set[128] = { 0 };
    char expt_type[128] = { 0 };
#if defined(XV_GTK_DIALOG) && !defined(XV_NO_GTK_WIN_SCANF)
    int expt_type_idx = 0;
# else
    char question[1024] = { 0 };
#endif
    char expt_name[128] = { 0 };
    char buffer[128] = { 0 };
    char linker[128] = { 0 };
    char reagents_string[2048] = { 0 };
    char name_template[2048] = {0};
    char *s = NULL;
    long lTime, lTimeb;
    char *saveptr = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    duplicate_pico_Expt_param(&Pico_param.expt_param, &tmp_pico_param);
    strncpy(mol, tmp_pico_param.molecule_name, sizeof(mol));
    strncpy(proj, tmp_pico_param.project, sizeof(proj));
    strncpy(expt_set, tmp_pico_param.experiment_set, sizeof(expt_set));
    strncpy(expt_type, tmp_pico_param.experiment_type, sizeof(expt_type));
    strncpy(expt_name, tmp_pico_param.experiment_name, sizeof(expt_name));
    strncpy(firstname, tmp_pico_param.user_firstname, sizeof(firstname));
    strncpy(lastname, tmp_pico_param.user_lastname, sizeof(firstname));
    strncpy(buffer, tmp_pico_param.buffer_name, sizeof(buffer));
    strncpy(linker, tmp_pico_param.linker_name, sizeof(linker));
    strncpy(name_template, tmp_pico_param.name_template, sizeof(name_template));
    reagents_string[0] = '\0';

    for (int i = 0; i < Pico_param.expt_param.reagents_name_size; ++i)
    {
        my_strncat(
            reagents_string, Pico_param.expt_param.reagents_name[i], sizeof(reagents_string));
        my_strncat(reagents_string, ";", sizeof(reagents_string));
    }

#if defined(XV_GTK_DIALOG) && !defined(XV_NO_GTK_WIN_SCANF)
    form_window_t *form = form_create("Experimental configuration");
    form_push_label(form, "Last Modification %s", Pico_param.expt_param.config_expt_time);
    form_newline(form);
    form_push_label(form, "Experimentalist :");
    form_newline(form);
    form_push_label(form, "\tFirstname");
    form_push_text(form, firstname);
    form_newline(form);
    form_push_label(form, "\tLastname");
    form_push_text(form, lastname);
    form_newline(form);
    form_push_label(form, "Project");
    form_push_text(form, proj);
    form_newline(form);
    form_push_label(form, "Experiment set");
    form_push_text(form, expt_set);
    form_newline(form);
    form_push_label(form, "Experiment name");
    form_push_text(form, expt_name);
    form_newline(form);
    form_push_label(form, "Experiment type");
    form_push_combo_option_with_value(form, "Closing", &expt_type_idx, expt_type);
    form_push_combo_option_with_value(form, "Opening", &expt_type_idx, expt_type);
    form_push_combo_option_with_value(form, "Both", &expt_type_idx, expt_type);
    form_push_combo_option_with_value(form, "Rotation", &expt_type_idx, expt_type);
    form_push_combo_option_with_value(form, "Ramp", &expt_type_idx, expt_type);
    form_push_combo_option_with_value(form, "Test", &expt_type_idx, expt_type);
    form_newline(form);
    form_push_label(form, "Molecule name");
    form_push_text(form, mol);
    form_newline(form);
    form_push_label(form, "Reagents (separated by ;)");
    form_push_text(form, reagents_string);
    form_newline(form);
    form_push_label(form, "Buffer");
    form_push_text(form, buffer);
    form_newline(form);
    form_push_label(form, "Linker");
    form_push_text(form, linker);
    form_newline(form);
    form_push_label(form, "Bead size");
    form_push_float(form, 0.05, &tmp_pico_param.bead_size);
    form_newline(form);
    form_push_label(form, "Molecule Extension");
    form_push_float(form, 0.05, &tmp_pico_param.molecule_extension);
    form_newline(form);
    form_push_label(form, "Scan number");
    form_push_int(form, &tmp_pico_param.scan_number);
    form_newline(form);
    form_push_label(form, "Bead number");
    form_push_int(form, &tmp_pico_param.scan_number);
    form_newline(form);
    form_push_label(form, "Name Template");
    form_push_text(form, name_template);
    form_newline(form);
    win_answer = form_run(form);
    form_free(form);
#else
    snprintf(question, 1024,
             "Last modification %s\n"
             "Experimentalist : \n"
             "\tFirst Name %%7s\n"
             "\tLast Name %%7s\n\n"
             "Project %%7s \n"
             "Experiment set %%7s \n"
             "Experiment name %%7s \n"
             "Experiment type  %%7s \n(test, ramp, opening, closing, opening+closing, rotation)\n"
             "Molecule name %%7s \n"
             "Reagents (separated by ;) %%7s \n"
             "Buffer %%7s \n"
             "Linker %%7s \n"
             "Bead size %%5f\n"
             "Molecule extension %%5f\n"
             "Scan number  %%5d\n"
             "Bead number %%5d\n"
             "Name template %%7s\n"
             //"Do you want to change these settings? %%R YES %%r NO\n"
             ,
             Pico_param.expt_param.config_expt_time);
    win_answer = win_scanf(question, firstname, lastname, proj, expt_set, expt_name, expt_type,
                           mol,             //
                           reagents_string, //
                           buffer, linker,  &tmp_pico_param.bead_size, &tmp_pico_param.molecule_extension,
                           &tmp_pico_param.scan_number, &tmp_pico_param.bead_number,
                           name_template/*,
                           &change_button*/); //
#endif
    free(tmp_pico_param.molecule_name);
    tmp_pico_param.molecule_name = strdup(mol);
    free(tmp_pico_param.project);
    tmp_pico_param.project = strdup(proj);
    free(tmp_pico_param.experiment_set);
    tmp_pico_param.experiment_set = strdup(expt_set);
    free(tmp_pico_param.experiment_name);
    tmp_pico_param.experiment_name = strdup(expt_name);
    free(tmp_pico_param.user_firstname);
    tmp_pico_param.user_firstname = strdup(firstname);
    free(tmp_pico_param.user_lastname);
    tmp_pico_param.user_lastname = strdup(lastname);
    free(tmp_pico_param.experiment_type);
    tmp_pico_param.experiment_type = strdup(expt_type);

    for (int i = 0; i < tmp_pico_param.reagents_name_size; ++i)
    {
        free(tmp_pico_param.reagents_name[i]);
    }

    free(tmp_pico_param.reagents_name);
    tmp_pico_param.reagents_name = NULL;
    tmp_pico_param.reagents_name_size = 0;
    s = strtok_r(reagents_string, ";", &saveptr);

    while (s)
    {
        tmp_pico_param.reagents_name_size++;
        tmp_pico_param.reagents_name = (char **)realloc(
                                           tmp_pico_param.reagents_name, tmp_pico_param.reagents_name_size * sizeof(char *));
        tmp_pico_param.reagents_name[tmp_pico_param.reagents_name_size - 1] = strdup(s);
        s = strtok_r(NULL, ";", &saveptr);
    }

    free(tmp_pico_param.buffer_name);
    tmp_pico_param.buffer_name = strdup(buffer);
    free(tmp_pico_param.linker_name);
    tmp_pico_param.linker_name = strdup(linker);
    free(tmp_pico_param.name_template);
    tmp_pico_param.name_template = strdup(name_template);
    free(tmp_pico_param.experiment_filename);
    tmp_pico_param.experiment_filename = NULL;
    free(tmp_pico_param.molecule_name);
    tmp_pico_param.molecule_name = strdup(mol);

    if (change_button == 0 && win_answer != WIN_CANCEL)
    {
        //change_button = 1;
        //win_answer = win_scanf("Are you really sure you want to change the settings \n"
        //                       "This may have unwanted effects \n %R YES %r NO\n",
        //                       &change_button);
        //if (change_button == 0 && win_answer != WIN_CANCEL)
        //{
        lTimeb = time(&lTime);
        duplicate_pico_Expt_param(&tmp_pico_param, &Pico_param.expt_param);

        if (Pico_param.expt_param.config_expt_time)
        {
            free(Pico_param.expt_param.config_expt_time);
        }

        s = Pico_param.expt_param.config_expt_time = strdup(ctime(&lTimeb));

        for (j = 0; s[j] != 0; j++)
        {
            s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
        }

        Pico_param.expt_param.experiment_filename = generate_filename(&Pico_param, Pico_param.expt_param.name_template,
                strlen(Pico_param.expt_param.name_template));
        write_Pico_config_file();
        //}
    }

    return D_O_K;
}

int state_is_pico(void)
{
    int is_pico = 0;

    if (Pico_param.rotation_motor_param.rotation_motor_controller != NULL
            && strncmp(Pico_param.rotation_motor_param.rotation_motor_controller, "pico", 4) == 0)
    {
        is_pico = 1;
    }

    if (Pico_param.translation_motor_param.translation_motor_controller != NULL
            && strncmp(Pico_param.translation_motor_param.translation_motor_controller, "pico", 4) == 0)
    {
        is_pico = 1;
    }

    if (Pico_param.focus_param.Focus_driver != NULL
            && strncmp(Pico_param.focus_param.Focus_driver, "pico", 4) == 0)
    {
        is_pico = 1;
    }

    return is_pico;
}

int switch_from_pico_serial_to_dummy(int is_pico)
{
    int j;
    char *s = NULL;
    long lTime, lTimeb;
    lTimeb = time(&lTime);

    if (Pico_param.serial_param.config_serial_time)
    {
        free(Pico_param.serial_param.config_serial_time);
    }

    s = Pico_param.serial_param.config_serial_time = strdup(ctime(&lTimeb));

    for (j = 0; s[j] != 0; j++)
    {
        s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
    }

    if (Pico_param.rotation_motor_param.rotation_motor_controller)
    {
        free(Pico_param.rotation_motor_param.rotation_motor_controller);
    }

    if (is_pico)
    {
        Pico_param.rotation_motor_param.rotation_motor_controller = strdup("pico");
    }
    else
    {
        Pico_param.rotation_motor_param.rotation_motor_controller = strdup("dummy");
    }

    if (Pico_param.translation_motor_param.translation_motor_controller)
    {
        free(Pico_param.translation_motor_param.translation_motor_controller);
    }

    if (is_pico)
    {
        Pico_param.translation_motor_param.translation_motor_controller = strdup("pico");
    }
    else
    {
        Pico_param.translation_motor_param.translation_motor_controller = strdup("dummy");
    }

    if (Pico_param.focus_param.Focus_driver)
    {
        free(Pico_param.focus_param.Focus_driver);
    }

    if (is_pico)
    {
        Pico_param.focus_param.Focus_driver = strdup("pico");
    }
    else
    {
        Pico_param.focus_param.Focus_driver = strdup("dummy");
    }

    write_Pico_config_file();
    return 0;
}

int change_serial_config(void)
{
    int change_button = 1, j;
    Serial_param tmp_pico_param;
    int i, is_pico = 0;
    char question[1024], *s;
    long lTime, lTimeb;

    if (Pico_param.rotation_motor_param.rotation_motor_controller != NULL
            && strncmp(Pico_param.rotation_motor_param.rotation_motor_controller, "pico", 4) == 0)
    {
        is_pico = 1;
    }

    if (Pico_param.translation_motor_param.translation_motor_controller != NULL
            && strncmp(Pico_param.translation_motor_param.translation_motor_controller, "pico", 4) == 0)
    {
        is_pico = 1;
    }

    if (Pico_param.focus_param.Focus_driver != NULL
            && strncmp(Pico_param.focus_param.Focus_driver, "pico", 4) == 0)
    {
        is_pico = 1;
    }

    if (updating_menu_state != 0)
    {
        // serial
        if (is_pico)
        {
            if (Pico_param.serial_param.firmware_version)
            {
                s = Pico_param.serial_param.firmware_version;

                for (j = 0; s[j] != 0; j++)
                {
                    s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
                }

#ifdef XV_WIN32
                snprintf(active_menu->text, 128, "Modify serial interface port %d (firmware %s)",
                         Pico_param.serial_param.serial_port, Pico_param.serial_param.firmware_version);
#else
                snprintf(active_menu->text, 128, "Modify serial interface port %s (firmware %s)",
                         Pico_param.serial_param.serial_port_str,
                         Pico_param.serial_param.firmware_version);
#endif
            }
            else
#ifdef XV_WIN32
                snprintf(active_menu->text, 128, "Modify PicoTwist serial port %d",
                         Pico_param.serial_param.serial_port);

#else
                snprintf(active_menu->text, 128, "Modify PicoTwist serial port %s",
                         Pico_param.serial_param.serial_port_str);
#endif
        }
        else
        {
            snprintf(active_menu->text, 128, "Modify Dummy interface device");
        }

        return D_O_K;
    }

    duplicate_pico_Serial_param(&Pico_param.serial_param, &tmp_pico_param);
    snprintf(question, 1024, "Last modification %s\n"
             "Simulating devices %%R or using Picotwist serial interface %%r\n",
             Pico_param.serial_param.config_serial_time);
    i = win_scanf(question, &is_pico);

    if (i != WIN_CANCEL && is_pico)
    {
        snprintf(question, 1024, "Last modification %s\n"
                 "Picotwist serial interface\n"
                #ifndef XV_WIN32
                 "Serial Port %%4s",
#else
                 "Serial Port %%4d"
                 "Baud rate %%4d Byte size %%4d\n"
                 "StopBits %%4d Parity  %%4d  fDtrControl %%4d fRtsControl%%4d\n",
#endif
                 Pico_param.serial_param.config_serial_time);
        i = win_scanf(question,
#ifndef XV_WIN32
                      tmp_pico_param.serial_port_str //
#else
                      & tmp_pico_param.serial_port, //
                      & tmp_pico_param.serial_BaudRate, //
                      &tmp_pico_param.serial_ByteSize, &tmp_pico_param.serial_StopBits,
                      &tmp_pico_param.serial_Parity, &tmp_pico_param.serial_fDtrControl,
                      &tmp_pico_param.serial_fRtsControl
#endif
                     );

    }

    if (i != WIN_CANCEL)
    {
        change_button = 1;
        win_scanf("Are you really sure you want to change the settings \n"
                  "This may have unwanted effects \n %R YES %r NO\n",
                  &change_button);

        if (change_button == 0 && i != WIN_CANCEL)
        {
            lTimeb = time(&lTime);
            duplicate_pico_Serial_param(&tmp_pico_param, &Pico_param.serial_param);

            if (Pico_param.serial_param.config_serial_time)
            {
                free(Pico_param.serial_param.config_serial_time);
            }

            s = Pico_param.serial_param.config_serial_time = strdup(ctime(&lTimeb));

            for (j = 0; s[j] != 0; j++)
            {
                s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
            }

            if (Pico_param.rotation_motor_param.rotation_motor_controller)
            {
                free(Pico_param.rotation_motor_param.rotation_motor_controller);
            }

            if (is_pico)
            {
                Pico_param.rotation_motor_param.rotation_motor_controller = strdup("pico");
            }
            else
            {
                Pico_param.rotation_motor_param.rotation_motor_controller = strdup("dummy");
            }

            if (Pico_param.translation_motor_param.translation_motor_controller)
            {
                free(Pico_param.translation_motor_param.translation_motor_controller);
            }

            if (is_pico)
            {
                Pico_param.translation_motor_param.translation_motor_controller = strdup("pico");
            }
            else
            {
                Pico_param.translation_motor_param.translation_motor_controller = strdup("dummy");
            }

            if (Pico_param.focus_param.Focus_driver)
            {
                free(Pico_param.focus_param.Focus_driver);
            }

            if (is_pico)
            {
                Pico_param.focus_param.Focus_driver = strdup("pico");
            }
            else
            {
                Pico_param.focus_param.Focus_driver = strdup("dummy");
            }

            write_Pico_config_file();
        }
    }

    if (tmp_pico_param.config_serial_time)
    {
        free(tmp_pico_param.config_serial_time);
    }

    return D_O_K;
}

int change_serial_config_new(void)
{
    int change_button = 1, j;
    Serial_param tmp_pico_param;
    int i, is_pico = 0;
    char question[1024], *s;
    long lTime, lTimeb;

    if (Pico_param.rotation_motor_param.rotation_motor_controller != NULL
            && strncmp(Pico_param.rotation_motor_param.rotation_motor_controller, "pico", 4) == 0)
    {
        is_pico = 1;
    }

    if (Pico_param.translation_motor_param.translation_motor_controller != NULL
            && strncmp(Pico_param.translation_motor_param.translation_motor_controller, "pico", 4) == 0)
    {
        is_pico = 1;
    }

    if (Pico_param.focus_param.Focus_driver != NULL
            && strncmp(Pico_param.focus_param.Focus_driver, "pico", 4) == 0)
    {
        is_pico = 1;
    }

    if (updating_menu_state != 0)
    {
        // serial
        if (is_pico)
#ifdef XV_WIN32
            snprintf(active_menu->text, 128, "Modify PicoTwist serial port %d",
                     Pico_param.serial_param.serial_port);

#else
            snprintf(active_menu->text, 128, "Modify PicoTwist serial port %s",
                     Pico_param.serial_param.serial_port_str);
#endif
        else
        {
            snprintf(active_menu->text, 128, "Modify Dummy interface device");
        }

        return D_O_K;
    }

    duplicate_pico_Serial_param(&Pico_param.serial_param, &tmp_pico_param);
    is_pico = 1;
#ifndef XV_WIN32
    snprintf(question, 1024, "Last modification %s\n"
             "Picotwist serial interface\n"
             "Serial Port %%s",
             Pico_param.serial_param.config_serial_time);
#else
    snprintf(question, 1024, "Last modification %s\n"
             "Picotwist serial interface\n"
             "Serial Port %%4d"
             "Baud rate %%4d Byte size %%4d\n"
             "StopBits %%4d Parity  %%4d  fDtrControl %%4d fRtsControl%%4d\n",
             Pico_param.serial_param.config_serial_time);
#endif
#ifndef XV_WIN32
    i = win_scanf(question,
                  &tmp_pico_param.serial_port_str //
                 );
#else
    i = win_scanf(question,
                  &tmp_pico_param.serial_port,     //
                  &tmp_pico_param.serial_BaudRate, //
                  &tmp_pico_param.serial_ByteSize, &tmp_pico_param.serial_StopBits,
                  &tmp_pico_param.serial_Parity, &tmp_pico_param.serial_fDtrControl,
                  &tmp_pico_param.serial_fRtsControl); //
#endif

    if (i != WIN_CANCEL)
    {
        change_button = 1;
        win_scanf("Are you really sure you want to change the settings \n"
                  "This may have unwanted effects \n %R YES %r NO\n",
                  &change_button);

        if (change_button == 0 && i != WIN_CANCEL)
        {
            lTimeb = time(&lTime);
            duplicate_pico_Serial_param(&tmp_pico_param, &Pico_param.serial_param);

            if (Pico_param.serial_param.config_serial_time)
            {
                free(Pico_param.serial_param.config_serial_time);
            }

            s = Pico_param.serial_param.config_serial_time = strdup(ctime(&lTimeb));

            for (j = 0; s[j] != 0; j++)
            {
                s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
            }

            if (Pico_param.rotation_motor_param.rotation_motor_controller)
            {
                free(Pico_param.rotation_motor_param.rotation_motor_controller);
            }

            if (is_pico)
            {
                Pico_param.rotation_motor_param.rotation_motor_controller = strdup("pico");
            }
            else
            {
                Pico_param.rotation_motor_param.rotation_motor_controller = strdup("dummy");
            }

            if (Pico_param.translation_motor_param.translation_motor_controller)
            {
                free(Pico_param.translation_motor_param.translation_motor_controller);
            }

            if (is_pico)
            {
                Pico_param.translation_motor_param.translation_motor_controller = strdup("pico");
            }
            else
            {
                Pico_param.translation_motor_param.translation_motor_controller = strdup("dummy");
            }

            if (Pico_param.focus_param.Focus_driver)
            {
                free(Pico_param.focus_param.Focus_driver);
            }

            if (is_pico)
            {
                Pico_param.focus_param.Focus_driver = strdup("pico");
            }
            else
            {
                Pico_param.focus_param.Focus_driver = strdup("dummy");
            }

            write_Pico_config_file();
        }
    }

    if (tmp_pico_param.config_serial_time)
    {
        free(tmp_pico_param.config_serial_time);
    }

    return D_O_K;
}

int change_magnet_config(void)
{
    int j;
    int change_button = 1;
    Magnet_param tmp_pico_param;
    int i;
    char question[1024];
    char mag_name[128], manufacturer[128], *s;
    long lTime, lTimeb;

    if (updating_menu_state != 0)
    {
        if (Pico_param.magnet_param.magnet_name != NULL
                && strlen(Pico_param.magnet_param.magnet_name) > 3)
            snprintf(active_menu->text, 128, "Modify %s magnets (Z contact = %6.3f mm)",
                     Pico_param.magnet_param.magnet_name, Pico_param.magnet_param.zmag_contact_sample);
        else
            snprintf(active_menu->text, 128, "Modify Dummy magnets (Z contact = %6.3f mm)",
                     Pico_param.magnet_param.zmag_contact_sample);

        return D_O_K;
    }

    duplicate_pico_Magnet_param(&Pico_param.magnet_param, &tmp_pico_param);
    snprintf(question, 1024, "Last configuration modification %s\n"
             "Name of device %%20s\nManufacturer %%20s\n"
             "Series Nb %%10dGap size (mm) %%8f\n"
             "Zmag corresonding to the contact of the magnet with the bead %%8f\n"
             "For each magnet device, an expected force may be computed\n"
             "according to its distance Z from the sample. "
             "The force takes \nthe form F = F_0 exp(\\Sigma_1^n a_i.Z^i) where"
             " F_0, a_i, n depend on the bead type, (Z is < 0 ex: -1mm)\n"
             "Specify the number of type of beads for which you know \n"
             "these parameters (you will enter them next) %%10d\n"
             "Do you want to change these settings? %%R YES %%r NO\n",
             Pico_param.magnet_param.config_magnet_time);
    strncpy(mag_name, tmp_pico_param.magnet_name, 128);
    strncpy(manufacturer, tmp_pico_param.manufacturer, 128);
    i = win_scanf(question, mag_name, manufacturer, &tmp_pico_param.series_nb, &tmp_pico_param.gap,
                  &tmp_pico_param.zmag_contact_sample, &tmp_pico_param.nb, &change_button);

    if (tmp_pico_param.magnet_name != NULL)
    {
        free(tmp_pico_param.magnet_name);
    }

    tmp_pico_param.magnet_name = strdup(mag_name);

    if (tmp_pico_param.manufacturer != NULL)
    {
        free(tmp_pico_param.manufacturer);
    }

    tmp_pico_param.manufacturer = strdup(manufacturer);

    if (change_button == 0 && i != WIN_CANCEL)
    {
        change_button = 1;
        i = win_scanf("Are you really sure you want to change the settings \n"
                      "This may have unwanted effects \n %R YES %r NO\n",
                      &change_button);

        if (change_button == 0 && i != WIN_CANCEL)
        {
            lTimeb = time(&lTime);

            for (i = 0; i < tmp_pico_param.nb; i++)
            {
                if (i == 0)
                {
                    win_scanf("For MyOne type of bead\nF = F0*exp-(a.z + b.z^2 + c.z^3 + d.z^4)\n"
                              "F0 %8f a %8f\nb %8f c %8f d %8f\n",
                              &tmp_pico_param.f0, &(tmp_pico_param.a1[i]), &(tmp_pico_param.a2[i]),
                              &(tmp_pico_param.a3[i]), &(tmp_pico_param.a4[i]));
                }
                else if (i == 1)
                {
                    win_scanf(
                        "For 2.8 \\mu m type of bead\nF = F0*exp-(a.z + b.z^2 + c.z^3 + d.z^4)\n"
                        "F0 %8f a %8f\nb %8f c %8f d %8f\n",
                        &tmp_pico_param.f0, &(tmp_pico_param.a1[i]), &(tmp_pico_param.a2[i]),
                        &(tmp_pico_param.a3[i]), &(tmp_pico_param.a4[i]));
                }
                else if (i == 2)
                {
                    win_scanf(
                        "For 4.5 \\mu m type of bead\nF = F0*exp-(a.z + b.z^2 + c.z^3 + d.z^4)\n"
                        "F0 %8f a %8f\nb %8f c %8f d %8f\n",
                        &tmp_pico_param.f0, &(tmp_pico_param.a1[i]), &(tmp_pico_param.a2[i]),
                        &(tmp_pico_param.a3[i]), &(tmp_pico_param.a4[i]));
                }
                else
                {
                    snprintf(question, 1024,
                             "For %d type of bead\nF = F0*exp-(a.z + b.z^2 + c.z^3 + d.z^4)\n"
                             "F0 %%8f a %%8f\nb %%8f c %%8f d %%8f\n",
                             i);
                    win_scanf(question, &tmp_pico_param.f0, &(tmp_pico_param.a1[i]),
                              &(tmp_pico_param.a2[i]), &(tmp_pico_param.a3[i]), &(tmp_pico_param.a4[i]));
                }

                j = 5;

                if (tmp_pico_param.a4[i] == 0)
                {
                    j = 4;
                }

                if (j == 4 && tmp_pico_param.a3[i] == 0)
                {
                    j = 3;
                }

                if (j == 3 && tmp_pico_param.a2[i] == 0)
                {
                    j = 2;
                }

                if (j == 2 && tmp_pico_param.a1[i] == 0)
                {
                    j = 1;
                }

                tmp_pico_param.nd[i] = j;
            }

            duplicate_pico_Magnet_param(&tmp_pico_param, &Pico_param.magnet_param);

            if (Pico_param.magnet_param.config_magnet_time)
            {
                free(Pico_param.magnet_param.config_magnet_time);
            }

            s = Pico_param.magnet_param.config_magnet_time = strdup(ctime(&lTimeb));

            for (j = 0; s[j] != 0; j++)
            {
                s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
            }

            write_Pico_config_file();
            do_update_menu();
        }
    }

    if (tmp_pico_param.magnet_name != NULL)
    {
        free(tmp_pico_param.magnet_name);
    }

    if (tmp_pico_param.manufacturer != NULL)
    {
        free(tmp_pico_param.manufacturer);
    }

    if (tmp_pico_param.config_magnet_time != NULL)
    {
        free(tmp_pico_param.config_magnet_time);
    }

    load_pico_config_file_Magnet_parameters();
    // load_Pico_Focus_config();
    return D_O_K;
}

int change_molecule_config(void)
{
    int change_button = 1, j;
    DNA_molecule tmp_pico_param;
    int i;
    char question[1024], name[128], *s;
    long lTime, lTimeb;

    if (updating_menu_state != 0)
    {
        // molecule
        if (Pico_param.dna_molecule.molecule_name != NULL
                && strlen(Pico_param.dna_molecule.molecule_name) > 3)
            snprintf(active_menu->text, 128, "Modify %s Molecule (%g microns)",
                     Pico_param.dna_molecule.molecule_name,
                     Pico_param.dna_molecule.dsDNA_molecule_extension);
        else
            snprintf(active_menu->text, 128, "Modify Dummy Molecule (%g microns)",
                     Pico_param.dna_molecule.dsDNA_molecule_extension);

        return D_O_K;
    }

    duplicate_pico_DNA_molecule(&Pico_param.dna_molecule, &tmp_pico_param);
    snprintf(question, 1024,
             "Last modification %s\n"
             "name %%20s \ndsDNA length (microns) %%8f\n"
             "dsDNA persistence length (nm) %%8f\n"
             "ssDNA length (microns) %%8f \n"
             "Nicking probability (between 0 and 1) %%8f\n"
             "{\\color{lightred}Do you want to change these settings?} %%R YES %%r NO\n",
             Pico_param.dna_molecule.config_molecule_time);
    strncpy(name, tmp_pico_param.molecule_name, 128);
    i = win_scanf(question,
                  name,                                     //
                  &tmp_pico_param.dsDNA_molecule_extension, //
                  &tmp_pico_param.dsxi,                     //
                  &tmp_pico_param.ssDNA_molecule_extension, &tmp_pico_param.nick_proba,
                  &change_button); //

    if (tmp_pico_param.molecule_name)
    {
        free(tmp_pico_param.molecule_name);
    }

    tmp_pico_param.molecule_name = strdup(name);

    if (change_button == 0 && i != WIN_CANCEL)
    {
        change_button = 1;
        i = win_scanf("Are you really sure you want to change the settings \n"
                      "This may have unwanted effects \n %R YES %r NO\n",
                      &change_button);

        if (change_button == 0 && i != WIN_CANCEL)
        {
            duplicate_pico_DNA_molecule(&tmp_pico_param, &Pico_param.dna_molecule);
            lTimeb = time(&lTime);

            if (Pico_param.dna_molecule.config_molecule_time)
            {
                free(Pico_param.dna_molecule.config_molecule_time);
            }

            s = Pico_param.dna_molecule.config_molecule_time = strdup(ctime(&lTimeb));

            for (j = 0; s[j] != 0; j++)
            {
                s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
            }

            write_Pico_config_file();
            do_update_menu();
        }
    }

    if (tmp_pico_param.molecule_name)
    {
        free(tmp_pico_param.molecule_name);
    }

    if (tmp_pico_param.config_molecule_time)
    {
        free(tmp_pico_param.config_molecule_time);
    }

    return D_O_K;
}

int change_bead_config(void)
{
    int change_button = 1, j;
    Bead_param tmp_pico_param;
    int i;
    char question[1024], manufacturer[128], name[128], *s;
    long lTime, lTimeb;

    if (updating_menu_state != 0)
    {
        // bead
        if (Pico_param.bead_param.bead_name != NULL && strlen(Pico_param.bead_param.bead_name) > 3)
            snprintf(active_menu->text, 128, "Modify %s bead (D = %g microns)",
                     Pico_param.bead_param.bead_name, 2 * Pico_param.bead_param.radius);
        else
            snprintf(active_menu->text, 128, "Modify Dummy bead (D = %g microns)",
                     2 * Pico_param.bead_param.radius);

        return D_O_K;
    }

    duplicate_pico_Bead_param(&Pico_param.bead_param, &tmp_pico_param);
    snprintf(question, 1024,
             "Last modification %s\n"
             "Bead name %%20s\nManufacturer  %%20s\nRadius (\\mu m) %%8f\n"
             "Low field suceptibility %%8f \n"
             "magnetization at saturation %%8f"
             "\nsaturation field %%8f\n"
             "{\\color{lightred}Do you want to change these settings?} %%R YES %%r NO\n",
             Pico_param.bead_param.config_bead_time);
    strncpy(manufacturer, tmp_pico_param.manufacturer, 128);
    strncpy(name, tmp_pico_param.bead_name, 128);
    i = win_scanf(question,
                  name,                   //
                  manufacturer,           //
                  &tmp_pico_param.radius, //
                  &tmp_pico_param.low_field_suceptibility, &tmp_pico_param.saturating_magnetization,
                  &tmp_pico_param.saturating_field,
                  &change_button); //

    if (tmp_pico_param.manufacturer)
    {
        free(tmp_pico_param.manufacturer);
    }

    tmp_pico_param.manufacturer = strdup(manufacturer);

    if (tmp_pico_param.bead_name)
    {
        free(tmp_pico_param.bead_name);
    }

    tmp_pico_param.bead_name = strdup(name);

    if (change_button == 0 && i != WIN_CANCEL)
    {
        change_button = 1;
        i = win_scanf("Are you really sure you want to change the settings \n"
                      "This may have unwanted effects \n %R YES %r NO\n",
                      &change_button);

        if (change_button == 0 && i != WIN_CANCEL)
        {
            duplicate_pico_Bead_param(&tmp_pico_param, &Pico_param.bead_param);
            lTimeb = time(&lTime);

            if (Pico_param.bead_param.config_bead_time)
            {
                free(Pico_param.bead_param.config_bead_time);
            }

            s = Pico_param.bead_param.config_bead_time = strdup(ctime(&lTimeb));

            for (j = 0; s[j] != 0; j++)
            {
                s[j] = (s[j] == '\n' || s[j] == '\r') ? 0 : s[j];
            }

            write_Pico_config_file();
            do_update_menu();
        }
    }

    if (tmp_pico_param.manufacturer)
    {
        free(tmp_pico_param.manufacturer);
    }

    if (tmp_pico_param.bead_name)
    {
        free(tmp_pico_param.bead_name);
    }

    if (tmp_pico_param.config_bead_time)
    {
        free(tmp_pico_param.config_bead_time);
    }

    return D_O_K;
}

MENU *config_file_menu(void)
{
    char *mntx = NULL;
    // char tmn[256], rmn[256], omn[256], fmn[256], cmn[256], mmn[256], bmn[256], mamn[256],
    // mimn[256], fimn[256];
    char omn1[256], omn2[64];
    static MENU config_mn[32] = { 0 };
    static int is_init = 0;

    if (is_init)
    {
        for (int i = 0; i < 32 && config_mn[i].text != NULL; ++i)
        {
            free(config_mn[i].text);
            config_mn[i].text = NULL;
        }
    }
    config_mn[0].text = NULL;
    mntx = (char *)calloc(128, sizeof(char));
    if (mntx == NULL) return NULL;

    snprintf(mntx, 128, "Reload %s", pico_config_filename);
    add_item_to_menu(config_mn, mntx, load_Pico_config, NULL, 0, NULL);
    add_item_to_menu(config_mn, strdup("\0"), NULL, NULL, 0, NULL);

    mntx = (char *)calloc(128, sizeof(char));
    if (mntx == NULL) return NULL;

	//menu dummy
    snprintf(mntx, 128, "Modify Dummy interface device");
    add_item_to_menu(config_mn, mntx, change_serial_config, NULL, 0, NULL);
    mntx = (char *)calloc(128, sizeof(char));
    if (mntx == NULL) return NULL;

	//menu focus
    if (Pico_param.focus_param.Focus_model != NULL && strlen(Pico_param.focus_param.Focus_model) > 3)
        snprintf(mntx, 128, "Modify %s focus device (%g microns)",
                 Pico_param.focus_param.Focus_model, Pico_param.focus_param.Focus_max_expansion);
    else
        snprintf(mntx, 128, "Modify Dummy focus device (%g microns)",
                 Pico_param.focus_param.Focus_max_expansion);
    add_item_to_menu(config_mn, mntx, change_focus_config, NULL, 0, NULL);
    mntx = (char *)calloc(128, sizeof(char));
    if (mntx == NULL) return NULL;

	//menu translation
    if (Pico_param.translation_motor_param.translation_motor_model != NULL
            && strlen(Pico_param.translation_motor_param.translation_motor_model) > 3)
        snprintf(mntx, 128, "Modify %s magnets translation device",
                 Pico_param.translation_motor_param.translation_motor_model);
    else snprintf(mntx, 128, "Modify Dummy magnets translation device");
    add_item_to_menu(config_mn, mntx, change_translation_motor_config, NULL, 0, NULL);

    #ifdef SDI_VERSION_2
    mntx = (char *)calloc(128, sizeof(char));
    snprintf(mntx, 128, "SDI 2 config menu");
    add_item_to_menu(config_mn, mntx, change_SDI_2_calib_config, NULL, 0, NULL);
    #endif

	//menu rotation
    mntx = (char *)calloc(128, sizeof(char));
    if (mntx == NULL) return NULL;
    if (Pico_param.rotation_motor_param.rotation_motor_model != NULL
            && strlen(Pico_param.rotation_motor_param.rotation_motor_model) > 3)
        snprintf(mntx, 128, "Modify %s magnets rotation device",
                 Pico_param.rotation_motor_param.rotation_motor_model);
    else snprintf(mntx, 128, "Modify Dummy magnets rotation device");
    add_item_to_menu(config_mn, mntx, change_rotation_motor_config, NULL, 0, NULL);
    add_item_to_menu(config_mn, strdup("\0"), NULL, NULL, 0, NULL);

    // camera
    mntx = (char *)calloc(128, sizeof(char));
    if (mntx == NULL) return NULL;
    if (Pico_param.camera_param.camera_model != NULL
            && strlen(Pico_param.camera_param.camera_model) > 3)
        snprintf(mntx, 128, "Modify %s camera (%g Hz)", Pico_param.camera_param.camera_model,
                 Pico_param.camera_param.camera_frequency_in_Hz);
    else snprintf(mntx, 128, "Modify Dummy camera (%g Hz)",
                 Pico_param.camera_param.camera_frequency_in_Hz);
    add_item_to_menu(config_mn, mntx, change_camera_config, NULL, 0, NULL);
    add_item_to_menu(config_mn, strdup("\0"), NULL, NULL, 0, NULL);

	// micro
    mntx = (char *)calloc(128, sizeof(char));
    if (mntx == NULL) return NULL;
    if (Pico_param.micro_param.microscope_user_name != NULL
            && strlen(Pico_param.micro_param.microscope_user_name) > 3)
        snprintf(mntx, 128, "Modify %s microscope (factor %g)",
                 Pico_param.micro_param.microscope_user_name, Pico_param.micro_param.microscope_factor);
    else snprintf(mntx, 128, "Modify Dummy microscope (factor %g)",
                 Pico_param.micro_param.microscope_factor);
    add_item_to_menu(config_mn, mntx, change_microscope_config, NULL, 0, NULL);

	// objective
    mntx = (char *)calloc(128, sizeof(char));
    if (mntx == NULL) return NULL;
    if (Pico_param.obj_param.immersion_type == 0) snprintf(omn2, 64, "Air");
    else if (Pico_param.obj_param.immersion_type == 1)
    {
        snprintf(omn2, 64, "Water");
    }
    else
    {
        snprintf(omn2, 64, "Oil");
    }
    snprintf(omn1, 256, "%gx%g/%s", Pico_param.obj_param.objective_magnification,
             Pico_param.obj_param.objective_numerical_aperture, omn2);
    if (Pico_param.obj_param.objective_manufacturer != NULL
            && strlen(Pico_param.obj_param.objective_manufacturer) > 2)
        snprintf(
            mntx, 128, "Modify %s objective %s", Pico_param.obj_param.objective_manufacturer, omn1);
    else snprintf(mntx, 128, "Modify Dummy objective %s", omn1);
    add_item_to_menu(config_mn, mntx, change_objective_config, NULL, 0, NULL);

	// magnet
    mntx = (char *)calloc(128, sizeof(char));
    if (mntx == NULL) return NULL;
    if (Pico_param.magnet_param.magnet_name != NULL && strlen(Pico_param.magnet_param.magnet_name) > 3)
        snprintf(mntx, 128, "Modify %s magnets (Contact = %6.3f mm)",
                 Pico_param.magnet_param.magnet_name, Pico_param.magnet_param.zmag_contact_sample);
    else snprintf(mntx, 128, "Modify Dummy magnets (Contact = %6.3f mm)",
                 Pico_param.magnet_param.zmag_contact_sample);
    add_item_to_menu(config_mn, mntx, change_magnet_config, NULL, 0, NULL);
    add_item_to_menu(config_mn, strdup("\0"), NULL, NULL, 0, NULL);

	// molecule
    mntx = (char *)calloc(128, sizeof(char));
    if (mntx == NULL) return NULL;
    if (Pico_param.dna_molecule.molecule_name != NULL && strlen(Pico_param.dna_molecule.molecule_name) > 3)
        snprintf(mntx, 128, "Modify %s Molecule (%g microns)",
                 Pico_param.dna_molecule.molecule_name,
                 Pico_param.dna_molecule.dsDNA_molecule_extension);
    else snprintf(mntx, 128, "Modify Dummy Molecule (%g microns)",
                 Pico_param.dna_molecule.dsDNA_molecule_extension);
    add_item_to_menu(config_mn, mntx, change_molecule_config, NULL, 0, NULL);

    // bead
    mntx = (char *)calloc(128, sizeof(char));
    if (mntx == NULL) return NULL;
    if (Pico_param.bead_param.bead_name != NULL && strlen(Pico_param.bead_param.bead_name) > 3)
        snprintf(mntx, 128, "Modify %s bead (D = %g microns)", Pico_param.bead_param.bead_name,
                 2 * Pico_param.bead_param.radius);
    else snprintf(mntx, 128, "Modify Dummy bead (D = %g microns)", 2 * Pico_param.bead_param.radius);
    add_item_to_menu(config_mn, mntx, change_bead_config, NULL, 0, NULL);

    add_item_to_menu(config_mn, strdup("Change Expt parameters"), change_expt_config, NULL, 0, NULL);

//tv 24/03/2017
#ifdef SDI_VERSION
	 add_item_to_menu(config_mn, strdup("Change SDI parameters"), change_sdi_config, NULL, 0, NULL);
#endif
	is_init = 1;
    return config_mn;
}

static gboolean
eval_cb(const GMatchInfo *info,
        GString          *res,
        gpointer          params_void)
{
    gchar *match = NULL;
    char buf[1024] = {0};
    Pico_parameter *params = (Pico_parameter *) params_void;
    match = g_match_info_fetch(info, 0);

    if (g_strcmp0(match, "{{date}}") == 0)
    {
        // TODO
    }
    if (g_strcmp0(match, "{{project}}") == 0)
    {
        g_string_append(res, params->expt_param.project);
    }
    else if (g_strcmp0(match, "{{set}}") == 0)
    {
        g_string_append(res, params->expt_param.experiment_set);
    }
    else if (g_strcmp0(match, "{{name}}") == 0)
    {
        g_string_append(res, params->expt_param.experiment_name);
    }
    else if (g_strcmp0(match, "{{type}}") == 0)
    {
        g_string_append(res, params->expt_param.experiment_type);
    }
    else if (g_strcmp0(match, "{{molecule}}") == 0)
    {
        g_string_append(res, params->expt_param.molecule_name);
    }
    else if (g_strcmp0(match, "{{reagents_name}}") == 0)
    {
        if (params->expt_param.reagents_name_size > 0)
        {
            g_string_append(res, params->expt_param.reagents_name[0]);
        }

        for (int i = 1; i < params->expt_param.reagents_name_size; ++i)
        {
            g_string_append(res, "-");
            g_string_append(res, params->expt_param.reagents_name[i]);
        }
    }
    else if (g_strcmp0(match, "{{reagents}}") == 0)
    {
        if (params->expt_param.reagents_name_size > 0)
        {
            g_string_append(res, params->expt_param.reagents_name[0]);
            g_string_append(res, "~");
            snprintf(buf, 1024, "%f", params->expt_param.reagents_concentration[0]);
            g_string_append(res, buf);
        }

        for (int i = 1; i < params->expt_param.reagents_name_size; ++i)
        {
            g_string_append(res, "-");
            g_string_append(res, params->expt_param.reagents_name[i]);
            g_string_append(res, "~");
            snprintf(buf, 1024, "%f", params->expt_param.reagents_concentration[i]);
            g_string_append(res, buf);
        }
    }
    else if (g_strcmp0(match, "{{buffer}}") == 0)
    {
        g_string_append(res, params->expt_param.buffer_name);
    }
    else if (g_strcmp0(match, "{{linker}}") == 0)
    {
        g_string_append(res, params->expt_param.linker_name);
    }
    else if (g_strcmp0(match, "{{comments}}") == 0)
    {
        //NOT IMPLEMENTED
    }
    else if (g_strcmp0(match, "{{framerate}}") == 0)
    {
        snprintf(buf, 1024, "%f", params->camera_param.camera_frequency_in_Hz);
        g_string_append(res, buf);
    }
    else if (g_strcmp0(match, "{{bead_size}}") == 0)
    {
        snprintf(buf, 1024, "%f", params->expt_param.bead_size);
        g_string_append(res, buf);
    }
    else if (g_strcmp0(match, "{{min_zmag}}") == 0)
    {
        //NOT IMPLEMENTED
    }
    else if (g_strcmp0(match, "{{min_zmag}}") == 0)
    {
        //NOT IMPLEMENTED
    }

    g_free(match);
    return FALSE;
}

/**
 * @brief
 *
 * format :
 * {{temp}} : Temperature of the sample [NOT IMPLEMENTED]
 * {{project}} : Project in which belong this experiment
 * {{set}} : Experiment set
 * {{reagents_name}} : name of reagents
 * {{reagents}} : name + concentration of reagents
 * {{type}} : opening, closing, both, ramp
 * {{molecule}} : name of the molecule observed
 * {{buffer}}
 * {{linker}}
 * {{comments}}
 * {{framerate}} : Camera framerate
 * {{bead_size}}
 * {{min_zmag}}
 * {{max_zmag}}
 *
 * @return
 */

char *generate_filename(Pico_parameter *params, const char *format, int size)
{
    gchar *res = NULL;
    GError *err = NULL;
    GRegex *reg = g_regex_new("{{(.*)}}", G_REGEX_UNGREEDY, G_REGEX_MATCH_NOTEMPTY, &err);
    res = g_regex_replace_eval(reg, format, size, 0, G_REGEX_MATCH_NOTEMPTY, eval_cb, params, &err);
    printf("%s\n", res);
    return res;
}

char *print_html_pico_expt_params(Pico_parameter *params)
{
    char buf[16000] = {0};
    char reagents[16000] = {0};
    char num[32] = {0};
    Expt_param *expt = &params->expt_param;

    for (int i = 0; i < expt->reagents_name_size; ++i)
    {
        if (expt->reagents_concentration)
        {
            snprintf(num, sizeof(num), "%f", expt->reagents_concentration[i]);
        }

        my_strncat(reagents, "<li>", sizeof(reagents));
        my_strncat(reagents, expt->reagents_name[i], sizeof(reagents));

        if (expt->reagents_concentration)
        {
            my_strncat(reagents, " at ", sizeof(reagents));
            my_strncat(reagents, num, sizeof(reagents));
        }

        my_strncat(reagents, "</li>\n", sizeof(reagents));
    }

    snprintf(buf, sizeof(buf),
             "<ul>"
             "<li>Experimentalist :"
             "<ul><li>First Name <strong>%s</strong></li>"
             "<li>Last Name <strong>%s</strong></li></ul></li>"
             "<li><strong>Project:</strong> %s </li> \n"
             "<li><strong>Experiment set:</strong> %s</li> \n"
             "<li><strong>Experiment name:</strong> %s</li> \n"
             "<li><strong>Experiment type:</strong> %s</li> \n"
             "<li><strong>Molecule name:</strong> %s</li> \n"
             "<li><strong>Reagents:</strong> \n <ul>\n%s\n</ul>\n</li> \n"
             "<li><strong>Buffer:</strong> %s</li> \n"
             "<li><strong>Linker:</strong> %s</li> \n"
             "<li><strong>Bead size:</strong> %f</li>\n"
             "<li><strong>Molecule extension:</strong> %f</li>\n"
             "<li><strong>Scan number:</strong> %d</li>\n"
             "<li><strong>Bead number:</strong> %d</li>\n"
             "</ul>",
             expt->user_firstname,
             expt->user_lastname,
             expt->project,
             expt->experiment_set,
             expt->experiment_name,
             expt->experiment_type,
             expt->molecule_name,
             reagents,
             expt->buffer_name,
             expt->linker_name,
             expt->bead_size,
             expt->molecule_extension,
             expt->scan_number,
             expt->bead_number
            );
    return strdup(buf);
}
int XVcfg_main(void)
{
    // PXV_VAR(MENU*, plot_experiment_menu);
    load_Pico_config();
#ifdef DYNAMIC
    add_image_treat_menu_item("Config", NULL, config_file_menu(), 0, NULL);
#endif
    //  if (plot_experiment_menu != NULL)
    //  add_item_to_menu(plot_experiment_menu,"Config", NULL, config_file_menu(), 0, NULL);
    // else add_plot_treat_menu_item ( "Config", NULL, config_file_menu(), 0, NULL);
    return D_O_K;
}

int config_file_menu_unload(void)
{
    remove_item_to_menu(plot_treat_menu, "Config", NULL, NULL);
    return D_O_K;
}

#endif
#endif
