# ifndef _MICROSCOPE_H_
# define _MICROSCOPE_H_

PXV_FUNC(float,	compute_y_microscope_scaling_new, (Obj_param *obj, Micro_param *mic, Camera_param *cam));
PXV_FUNC(float,	compute_x_microscope_scaling_new, (Obj_param *obj, Micro_param *mic, Camera_param *cam));

PXV_FUNC(int, def_oi_scaling, (O_i *oi));



# endif /* _MICROSCOPE_C_ */
