#ifndef _XVPOINTGREY_H_
#define _XVPOINTGREY_H_

PXV_FUNC(int, do_xvPointGrey_average_along_y, (void));
PXV_FUNC(MENU*, xvPointGrey_image_menu, (void));
PXV_FUNC(int, xvPointGrey_main, (int argc, char **argv));
#endif

