/*
 *    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _XVPOINTGREY_C_
#define _XVPOINTGREY_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 
#include "FlyCapture2_C.h"
#include <stdio.h>
# include "float.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "xvPointGrey.h"
//place here other headers of this plugin 


//=============================================================================
// Copyright © 2008 Point Grey Research, Inc. All Rights Reserved.
//
// This software is the confidential and proprietary information of Point
// Grey Research, Inc. ("Confidential Information").  You shall not
// disclose such Confidential Information and shall use it only in
// accordance with the terms of the license agreement you entered into
// with PGR.
//
// PGR MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
// SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE, OR NON-INFRINGEMENT. PGR SHALL NOT BE LIABLE FOR ANY DAMAGES
// SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
// THIS SOFTWARE OR ITS DERIVATIVES.
//=============================================================================
//=============================================================================
// $Id: FlyCapture2Test_C.c,v 1.29 2010/04/13 21:35:02 hirokim Exp $
//=============================================================================

#if defined(WIN32) || defined(WIN64)
#define _CRT_SECURE_NO_WARNINGS		
#endif

int print_error(fc2Error error, char *mes)
{
    if (error == FC2_ERROR_UNDEFINED) win_printf("FC2_ERROR_UNDEFINED\n%s\n",mes);
    else if (error == FC2_ERROR_OK) win_printf("FC2_ERROR_OK\n%s\n",mes);
    else if (error == FC2_ERROR_FAILED) win_printf("FC2_ERROR_FAILED\n%s\n",mes);
    else if (error == FC2_ERROR_NOT_IMPLEMENTED) win_printf("FC2_ERROR_NOT_IMPLEMENTED\n%s\n",mes);
    else if (error == FC2_ERROR_FAILED_BUS_MASTER_CONNECTION) win_printf("FC2_ERROR_FAILED_BUS_master_connection\n%s\n",mes);
    else if (error == FC2_ERROR_NOT_CONNECTED) win_printf("FC2_ERROR_NOT_CONNECTED\n%s\n",mes);
    else if (error == FC2_ERROR_INIT_FAILED) win_printf("FC2_ERROR_INIT_FAILED\n%s\n",mes);
    else if (error == FC2_ERROR_NOT_INTITIALIZED) win_printf("FC2_ERROR_NOT_INTITIALIZED\n%s\n",mes);
    else if (error == FC2_ERROR_INVALID_PARAMETER) win_printf("FC2_ERROR_INVALID_PARAMETER\n%s\n",mes);
    else if (error == FC2_ERROR_INVALID_SETTINGS) win_printf("FC2_ERROR_INVALID_SETTINGS\n%s\n",mes);
    else if (error == FC2_ERROR_INVALID_BUS_MANAGER) win_printf("FC2_ERROR_INVALID_BUS_manager\n%s\n",mes);
    else if (error == FC2_ERROR_MEMORY_ALLOCATION_FAILED) win_printf("FC2_ERROR_MEMORY_ALLOCATION_failed\n%s\n",mes);
    else if (error == FC2_ERROR_LOW_LEVEL_FAILURE) win_printf("FC2_ERROR_LOW_LEVEL_failure\n%s\n",mes);
    else if (error == FC2_ERROR_NOT_FOUND) win_printf("FC2_ERROR_NOT_FOUND\n%s\n",mes);
    else if (error == FC2_ERROR_FAILED_GUID) win_printf("FC2_ERROR_FAILED_GUID\n%s\n",mes);
    else if (error == FC2_ERROR_INVALID_PACKET_SIZE) win_printf("FC2_ERROR_INVALID_PACKET_size\n%s\n",mes);
    else if (error == FC2_ERROR_INVALID_MODE) win_printf("FC2_ERROR_INVALID_MODE\n%s\n",mes);
    else if (error == FC2_ERROR_NOT_IN_FORMAT7) win_printf("FC2_ERROR_NOT_IN_format7\n%s\n",mes);
    else if (error == FC2_ERROR_NOT_SUPPORTED) win_printf("FC2_ERROR_NOT_SUPPORTED\n%s\n",mes);
    else if (error == FC2_ERROR_TIMEOUT) win_printf("FC2_ERROR_TIMEOUT\n%s\n",mes);
    else if (error == FC2_ERROR_BUS_MASTER_FAILED) win_printf("FC2_ERROR_BUS_MASTER_failed\n%s\n",mes);
    else if (error == FC2_ERROR_INVALID_GENERATION) win_printf("FC2_ERROR_INVALID_GENERATION\n%s\n",mes);
    else if (error == FC2_ERROR_LUT_FAILED) win_printf("FC2_ERROR_LUT_FAILED\n%s\n",mes);
    else if (error == FC2_ERROR_IIDC_FAILED) win_printf("FC2_ERROR_IIDC_FAILED\n%s\n",mes);
    else if (error == FC2_ERROR_STROBE_FAILED) win_printf("FC2_ERROR_STROBE_FAILED\n%s\n",mes);
    else if (error == FC2_ERROR_TRIGGER_FAILED) win_printf("FC2_ERROR_TRIGGER_FAILED\n%s\n",mes);
    else if (error == FC2_ERROR_PROPERTY_FAILED) win_printf("FC2_ERROR_PROPERTY_FAILED\n%s\n",mes);
    else if (error == FC2_ERROR_PROPERTY_NOT_PRESENT) win_printf("FC2_ERROR_PROPERTY_NOT_present\n%s\n",mes);
    else if (error == FC2_ERROR_REGISTER_FAILED) win_printf("FC2_ERROR_REGISTER_FAILED\n%s\n",mes);
    else if (error == FC2_ERROR_READ_REGISTER_FAILED) win_printf("FC2_ERROR_READ_REGISTER_failed\n%s\n",mes);
    else if (error == FC2_ERROR_WRITE_REGISTER_FAILED) win_printf("FC2_ERROR_WRITE_REGISTER_failed\n%s\n",mes);
    else if (error == FC2_ERROR_ISOCH_FAILED) win_printf("FC2_ERROR_ISOCH_FAILED\n%s\n",mes);
    else if (error == FC2_ERROR_ISOCH_ALREADY_STARTED) win_printf("FC2_ERROR_ISOCH_ALREADY_started\n%s\n",mes);
    else if (error == FC2_ERROR_ISOCH_NOT_STARTED) win_printf("FC2_ERROR_ISOCH_NOT_started\n%s\n",mes);
    else if (error == FC2_ERROR_ISOCH_START_FAILED) win_printf("FC2_ERROR_ISOCH_START_failed\n%s\n",mes);
    else if (error == FC2_ERROR_ISOCH_RETRIEVE_BUFFER_FAILED) win_printf("FC2_ERROR_ISOCH_RETRIEVE_buffer_failed\n%s\n",mes);
    else if (error == FC2_ERROR_ISOCH_STOP_FAILED) win_printf("FC2_ERROR_ISOCH_STOP_failed\n%s\n",mes);
    else if (error == FC2_ERROR_ISOCH_SYNC_FAILED) win_printf("FC2_ERROR_ISOCH_SYNC_failed\n%s\n",mes);
    else if (error == FC2_ERROR_ISOCH_BANDWIDTH_EXCEEDED) win_printf("FC2_ERROR_ISOCH_BANDWIDTH_exceeded\n%s\n",mes);
    else if (error == FC2_ERROR_IMAGE_CONVERSION_FAILED) win_printf("FC2_ERROR_IMAGE_CONVERSION_failed\n%s\n",mes);
    else if (error == FC2_ERROR_IMAGE_LIBRARY_FAILURE) win_printf("FC2_ERROR_IMAGE_LIBRARY_failure\n%s\n",mes);
    else if (error == FC2_ERROR_BUFFER_TOO_SMALL) win_printf("FC2_ERROR_BUFFER_TOO_small\n%s\n",mes);
    else if (error == FC2_ERROR_IMAGE_CONSISTENCY_ERROR) win_printf("FC2_ERROR_IMAGE_CONSISTENCY_error\n%s\n",mes);
    else if (error == FC2_ERROR_FORCE_32BITS) win_printf("FC2_ERROR_FORCE_32BITS\n%s\n",mes);
    else win_printf("FC2_ERROR_UNknown %d\n%s\n",error,mes);
    return 0;
}


typedef struct _PG_data
{
  imreg *imr;
  O_i *oid;
  pltreg *pr;
  O_p *opt;
  O_p *opi;
} PG_data;

PG_data PG_d;


int alloc_auxillaury_memory_to_one_movie(O_i *oi, int nx, int ny, int type)
{
  int i = 0;
  int  nf=0;

  if (oi == NULL)		return 1;
  nf = oi->im.n_f;
  oi->im.nx = nx;
  oi->im.ny = ny;
  switch (type)
    {
    case IS_CHAR_IMAGE:
      oi->z_min = 0;
      oi->z_max = 255;
      break;
    case IS_RGB_PICTURE:
      oi->z_min = 0;
      oi->z_max = 255;
      oi->im.mode = TRUE_RGB;
      break;		
    case IS_RGBA_PICTURE:
      oi->z_min = 0;
      oi->z_max = 255;
      oi->im.mode = TRUE_RGB;
      break;		
    case IS_RGB16_PICTURE:
      oi->z_min = -32768;
      oi->z_max = 32767;
      oi->im.mode = TRUE_RGB;
      break;		
    case IS_RGBA16_PICTURE:
      oi->z_min = -32768;
      oi->z_max = 32767;
      oi->im.mode = TRUE_RGB;
      break;		
    case IS_INT_IMAGE:
      oi->z_min = -32768;
      oi->z_max = 32767;
      break;
    case IS_UINT_IMAGE:
      oi->z_min = 0;
      oi->z_max = 65536;
      break;
    case IS_LINT_IMAGE:
      oi->z_min = -2e30;
      oi->z_max = 2e30;
      break;
    case IS_FLOAT_IMAGE:
      oi->z_min = -1;
      oi->z_max = 1;
      break;
    case IS_COMPLEX_IMAGE:
      oi->z_min = -1;
      oi->z_max = 1;
      break;
    case IS_DOUBLE_IMAGE:
      oi->z_min = -1;
      oi->z_max = 1;
      break;
    case IS_COMPLEX_DOUBLE_IMAGE:
      oi->z_min = -1;
      oi->z_max = 1;
      break;
    default:
      return 1;
      break;
    };
  if (oi->im.pixel == NULL)
    oi->im.pixel = (union pix *)realloc(oi->im.pixel, ny*nf * sizeof(union pix));
  if (oi->im.pixel == NULL) return 2;

  if (oi->im.pxl == NULL)
    oi->im.pxl = (union pix **)realloc(oi->im.pxl, nf*sizeof(union pix*));
  if (oi->im.pxl == NULL) return 3;

  if (oi->im.mem == NULL)
    oi->im.mem = (void **)realloc(oi->im.mem, nf*sizeof(void*));
  if (oi->im.mem == NULL) return 4;

  oi->im.m_f = nf;

  oi->im.nxs = oi->im.nys = 0;
  oi->im.nxe = oi->im.nx = nx;
  oi->im.nye = oi->im.ny = ny;
  oi->im.data_type = type;	
  if (type == IS_COMPLEX_IMAGE  || type == IS_COMPLEX_DOUBLE_IMAGE)	
    oi->im.mode = RE;
  oi->im.s_l = (S_l ***)calloc(nf,sizeof(S_l**));	
  oi->im.m_sl = (int*)calloc(nf,sizeof(int));	
  oi->im.n_sl = (int*)calloc(nf,sizeof(int));	
  if (oi->im.s_l == NULL || oi->im.n_sl == NULL || oi->im.m_sl == NULL)	    return 4;
  for (i = 0;i < nf; i++)
    {
      oi->im.s_l[i] = NULL;
      oi->im.n_sl[i] = oi->im.m_sl[i] = 0;
    }
  oi->im.user_ispare = (int**)calloc(nf,sizeof(int*));	
  oi->im.user_fspare = (float**)calloc(nf,sizeof(float*));	
  if (oi->im.user_ispare == NULL || oi->im.user_fspare == NULL)    return 5;
  oi->im.user_nfpar = oi->im.user_nipar = 4;
  for (i = 0;i < nf; i++)
    {
      oi->im.user_ispare[i] = (int*)calloc(oi->im.user_nipar,sizeof(int));	
      oi->im.user_fspare[i] = (float*)calloc(oi->im.user_nfpar,sizeof(float));	
      if (oi->im.user_ispare[i] == NULL || oi->im.user_fspare[i] == NULL)	return 6;
    }
  oi->need_to_refresh |= ALL_NEED_REFRESH;	
  return 0;
}


int add_memory_to_one_movie(O_i *oi, int type, int frame, unsigned char *pData)
{
  int i = 0;
  int nf=0;
  int ny, nx, off;

  if (oi == NULL)		return 1;
  nf = oi->im.n_f;
  ny = oi->im.ny;
  nx = oi->im.nx;
  if (oi->im.pixel == NULL) return 2;
  if (oi->im.pxl == NULL) return 3;
  if (oi->im.mem == NULL) return 4;
  oi->im.m_f = nf;
  if (frame >= 0 && frame < nf)
    {
      off = frame*ny; 
      oi->im.mem[frame] = (void*)pData;
      oi->im.pxl[frame] = oi->im.pixel + off;
      for (i=0; i< ny ; i++)
	{
	  switch (type)
	    {
	    case IS_CHAR_IMAGE:          oi->im.pixel[off+i].ch = (unsigned char*)pData + (ny-1-i)*nx;        break;
	    case IS_RGB_PICTURE:          oi->im.pixel[off+i].ch = (unsigned char*)pData + (ny-1-i)*nx*3;       break; 
	    case IS_RGBA_PICTURE:         oi->im.pixel[off+i].ch = (unsigned char*)pData + (ny-1-i)*nx*4;       break;
	    case IS_RGB16_PICTURE:        oi->im.pixel[off+i].in = (short int*)pData + (ny-1-i)*nx*3;           break;
	    case IS_RGBA16_PICTURE:       oi->im.pixel[off+i].in = (short int*)pData + (ny-1-i)*nx*4;           break; 
	    case IS_INT_IMAGE:            oi->im.pixel[off+i].in = (short int *)pData + (ny-1-i)*nx;	   break;
	    case IS_UINT_IMAGE:           oi->im.pixel[off+i].ui = (unsigned short int *)pData + (ny-1-i)*nx; break;
	    case IS_LINT_IMAGE:           oi->im.pixel[off+i].li = (int *)pData + (ny-1-i)*nx;	           break;
	    case IS_FLOAT_IMAGE:          oi->im.pixel[off+i].fl = (float *)pData + (ny-1-i)*nx;	           break;
	    case IS_COMPLEX_IMAGE:        oi->im.pixel[off+i].fl = (float *)pData + (ny-1-i)*nx*2;              break; 
	    case IS_DOUBLE_IMAGE:         oi->im.pixel[off+i].db = (double *)pData + (ny-1-i)*nx;             break;
	    case IS_COMPLEX_DOUBLE_IMAGE: oi->im.pixel[off+i].db = (double *)pData + (ny-1-i)*nx*2;              break;
	    };
	}
    }
  if (oi->im.n_f == 0)	    oi->im.pxl[0] = oi->im.pixel;
  oi->im.data_type = type;	
  if (type == IS_COMPLEX_IMAGE  || type == IS_COMPLEX_DOUBLE_IMAGE)	oi->im.mode = RE;
  oi->need_to_refresh |= ALL_NEED_REFRESH;	
  return 0;
}


O_i* create_one_movie_not_allocated(int nx, int ny, int data_type, int n_frames)
{
  O_i *oi;
  
  if((oi = (O_i *)calloc(1,sizeof(O_i))) == NULL)  return NULL;
  if (init_one_image(oi, data_type)) 	           return NULL;
  oi->im.n_f = n_frames;
  if (alloc_auxillaury_memory_to_one_movie(oi, nx, ny, data_type)) return NULL;
  return oi;
}

int spot_light_idle_action(O_p *op, DIALOG *d)
{
  (void)d;
  if ((PG_d.opi != NULL) && (op == PG_d.opi))	return refresh_plot(PG_d.pr, UNCHANGED);
  if ((PG_d.opt != NULL) && (op == PG_d.opt))	return refresh_plot(PG_d.pr, UNCHANGED);
  return 0;
}


int create_timing_plot(void)
{
  pltreg *pr;
  O_p *op;

  if ((pr = create_and_register_hidden_new_plot_project(0, 0, 900,  668, "Timing")) == NULL)     
    return 1;
  PG_d.pr = pr;
  if ((op = create_and_attach_one_plot(pr, 4096, 4096,0)) == NULL)     return 2;
  op->op_idle_action = spot_light_idle_action;
  set_plot_title(op,"Camera timing");
  set_plot_x_title(op,"Frame number");
  set_plot_y_title(op,"Inter frame dt (ms)");
  PG_d.opt = op;

  if ((op = create_and_attach_one_plot(pr, 4096, 4096,0)) == NULL)    return 2;
  PG_d.opi = op;
  op->op_idle_action = spot_light_idle_action;
  set_plot_title(op,"Light variation");
  
  remove_data (pr, IS_ONE_PLOT, (void *)pr->o_p[0]);
  return 0;  
}


fc2Context context;



void PrintBuildInfo()
{
  fc2Version version;
  char versionStr[512];
  char timeStamp[512];

  fc2GetLibraryVersion( &version );

  sprintf( 
	  versionStr, 
	  "FlyCapture2 library version: %d.%d.%d.%d\n", 
	  version.major, version.minor, version.type, version.build );

  win_printf( "%s", versionStr );

  sprintf( timeStamp, "Application build date: %s %s\n\n", __DATE__, __TIME__ );

  win_printf( "%s", timeStamp );
}

void PrintCameraInfo( fc2Context context1 )
{
  fc2Error error;
  fc2CameraInfo camInfo;
  error = fc2GetCameraInfo( context1, &camInfo );
  if ( error != FC2_ERROR_OK )
    {
      // Error
    }

  win_printf(
	     "\n*** CAMERA INFORMATION ***\n"
	     "Serial number - %u\n"
	     "Camera model - %s\n"
	     "Camera vendor - %s\n"
	     "Sensor - %s\n"
	     "Resolution - %s\n"
	     "Firmware version - %s\n"
	     "Firmware build time - %s\n\n",
	     camInfo.serialNumber,
	     camInfo.modelName,
	     camInfo.vendorName,
	     camInfo.sensorInfo,
	     camInfo.sensorResolution,
	     camInfo.firmwareVersion,
	     camInfo.firmwareBuildTime );
}

void SetTimeStamping( fc2Context context1, BOOL enableTimeStamp )
{
  fc2Error error;
  fc2EmbeddedImageInfo embeddedInfo;

  error = fc2GetEmbeddedImageInfo( context1, &embeddedInfo );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2GetEmbeddedImageInfo: %d\n", error );
    }

  if ( embeddedInfo.timestamp.available != 0 )
    {       
      embeddedInfo.timestamp.onOff = enableTimeStamp;
    }    

  error = fc2SetEmbeddedImageInfo( context1, &embeddedInfo );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2SetEmbeddedImageInfo: %d\n", error );
    }
}

void GrabImages( fc2Context context1, int numImagesToGrab )
{
  fc2Error error;
  fc2Image rawImage;
  fc2Image convertedImage;
  //fc2TimeStamp prevTimestamp = {0};
  int i;
  pltreg *pr = NULL;
  d_s *ds = NULL;
  O_p *op = NULL;

  error = fc2CreateImage( &rawImage );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2CreateImage: %d\n", error );
    }

  error = fc2CreateImage( &convertedImage );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2CreateImage: %d\n", error );
    }

  // If externally allocated memory is to be used for the converted image,
  // simply assigning the pData member of the fc2Image structure is
  // insufficient. fc2SetImageData() should be called in order to populate
  // the fc2Image structure correctly. This can be done at this point,
  // assuming that the memory has already been allocated.

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    {win_printf_OK("cannot find data"); return;}

  if ((op = create_and_attach_one_plot(pr, numImagesToGrab, numImagesToGrab, 0)) == NULL)
    {win_printf_OK("cannot create plot !"); return;}
  ds = op->dat[0];



  for ( i=0; i < numImagesToGrab; i++ )
    {
      // Retrieve the image
      error = fc2RetrieveBuffer( context1, &rawImage );
      if ( error != FC2_ERROR_OK )
        {
	  win_printf( "Error in retrieveBuffer: %d\n", error);
        }
      else
        {
	  // Get and print out the time stamp
	  fc2TimeStamp ts = fc2GetImageTimeStamp( &rawImage);
	  //int diff = (ts.cycleSeconds - prevTimestamp.cycleSeconds) * 8000
	  //  + (ts.cycleCount - prevTimestamp.cycleCount);
	  //prevTimestamp = ts;
	  //win_printf("timestamp [%d %d] - %d\n",ts.cycleSeconds, ts.cycleCount, diff );
	  ds->xd[i] = i;
	  ds->yd[i] = ts.cycleSeconds + 8000 + ts.cycleCount;
        }       
    }

  if ( error == FC2_ERROR_OK )
    {
      // Convert the final image to RGB
      error = fc2ConvertImageTo(FC2_PIXEL_FORMAT_BGR, &rawImage, &convertedImage);
      if ( error != FC2_ERROR_OK )
        {
	  win_printf( "Error in fc2ConvertImageTo: %d\n", error );
        }

      // Save it to PNG
      win_printf("Saving the last image to fc2TestImage.png \n");
      error = fc2SaveImage( &convertedImage, "fc2TestImage.png", FC2_PNG );
      if ( error != FC2_ERROR_OK )
	{
	  win_printf( "Error in fc2SaveImage: %d\n", error );
	  win_printf( "Please check write permissions.\n");
	}
    }

  error = fc2DestroyImage( &rawImage );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2DestroyImage: %d\n", error );
    }

  error = fc2DestroyImage( &convertedImage );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2DestroyImage: %d\n", error );
    }
    refresh_plot(pr, pr->n_op-1);
}

int test_camera(void)
{        
  fc2Error error;
  fc2PGRGuid guid;
  unsigned int numCameras = 0;    
  const int k_numImages = 100;

  if(updating_menu_state != 0)	return D_O_K;

  PrintBuildInfo();

  error = fc2CreateContext( &context );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2CreateContext: %d\n", error );
      return 0;
    }        

  error = fc2GetNumOfCameras( context, &numCameras );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2GetNumOfCameras: %d\n", error );
      return 0;
    }

  if ( numCameras == 0 )
    {
      // No cameras detected
      win_printf( "No cameras detected.\n");
      return 0;
    }

  // Get the 0th camera
  error = fc2GetCameraFromIndex( context, 0, &guid );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2GetCameraFromIndex: %d\n", error );
      return 0;
    }    

  error = fc2Connect( context, &guid );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2Connect: %d\n", error );
      return 0;
    }

  PrintCameraInfo( context );    

  SetTimeStamping( context, TRUE );

  error = fc2StartCapture( context );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2StartCapture: %d\n", error );
      return 0;
    }

  GrabImages( context, k_numImages );   

  error = fc2StopCapture( context );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2StopCapture: %d\n", error );
      return 0;
    }

  error = fc2DestroyContext( context );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2DestroyContext: %d\n", error );
      return 0;
    }

  win_printf( "Done!\n" );

  return 0;
}

int oi_idle_action(struct  one_image *oi, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  BITMAP *imb;
  unsigned long t0 = 0;

  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

  t0 = my_uclock();
  if ((IS_DATA_IN_USE(oi) == 0) && (oi->need_to_refresh & BITMAP_NEED_REFRESH))
    {
      display_image_stuff_16M(imr,d);
      oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
    }
  imb = (BITMAP*)oi->bmp.stuff;
  if (oi->need_to_refresh & INTERNAL_BITMAP_NEED_REFRESH)
    {
      SET_BITMAP_IN_USE(oi);
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off //+ d->x
	   , imr->y_off - imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);
      oi->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
      t0 = my_uclock() - t0;
      SET_BITMAP_NO_MORE_IN_USE(oi);
    }
  return 0;
}


void fc2ImageEventCallback_1(fc2Image *image, void *pCallbackData)
{
  register int i, j;
  O_i *oi = (O_i*)pCallbackData;
  int nf, cf;
  unsigned char *ps, *pd;
  static unsigned char *pD[32];
  static unsigned int im = 0;
  static int ipd = 0, npd = 0;

  if (im == 0) for(i = 0; i < 32; i++) pD[i] = 0;
  if (image == NULL) 
    {
      my_set_window_title("image is NULL");
      return;
    }
  //fc2RetrieveBuffer( context, &rawImage );
  for(i = 0; i < 32 && i < npd && pD[i] != image->pData; i++);
  if (i == npd && npd < 32)
    {
      pD[npd++] = image->pData;
    } 
  else ipd = i;
  nf = (oi->im.n_f > 0) ? oi->im.n_f : 1;
  cf = oi->im.c_f;
  cf++;
  cf = (cf < nf) ? cf : 0;
  for (i = 0; i < oi->im.ny && i < (int)image->rows; i++)
    {
      pd = oi->im.pxl[cf][i].ch;
      ps = image->pData + i * image->cols;
      for (j = 0; j < oi->im.nx && j < (int)image->cols; j++)
	pd[j] = ps[j];
    }
  switch_frame(oi,cf);
  my_set_window_title("im %u id %u x %u, recieved %u oi %d x %d pointer %u ipd %u n %u",im++,image->cols,image->rows,image->receivedDataSize,oi->im.nx,oi->im.ny,image->pData,ipd,npd);
  return;
}

float getCameraFrameRate(fc2Context context1)
{
  fc2Error error;
  fc2Property prop;                          //Declare a Property struct.
  prop.type = FC2_FRAME_RATE;                //Define the property to adjust.
  error = fc2GetProperty(context1, &prop );   //Set the property.
  return (error != FC2_ERROR_OK ) ? FLT_MIN : prop.absValue;
}


float getCameraShutter(fc2Context context1)
{
  fc2Error error;
  fc2Property prop;                          //Declare a Property struct.
  prop.type = FC2_SHUTTER;                   //Define the property to adjust.
  error = fc2GetProperty(context1, &prop );   //Set the property.
  return (error != FC2_ERROR_OK ) ? FLT_MIN : prop.absValue;
}


float getCameraGain(fc2Context context1)
{
  fc2Error error;
  fc2Property prop;                          //Declare a Property struct.
  prop.type = FC2_GAIN;                      //Define the property to adjust.
  error = fc2GetProperty(context1, &prop );   //Set the property.
  return (error != FC2_ERROR_OK ) ? FLT_MIN : prop.absValue;
}

float getCameraGamma(fc2Context context1)
{
  fc2Error error;
  fc2Property prop;                          //Declare a Property struct.
  prop.type = FC2_GAMMA;                     //Define the property to adjust.
  error = fc2GetProperty(context1, &prop );   //Set the property.
  return (error != FC2_ERROR_OK ) ? FLT_MIN : prop.absValue;
}




fc2Error setCameraFrameRate(fc2Context context1, float val)
{
  fc2Property prop;             //Declare a Property struct.
  prop.type = FC2_FRAME_RATE;   //Define the property to adjust.
  prop.onOff = TRUE;            //Ensure the property is on.
  prop.autoManualMode = FALSE;  //Ensure auto-adjust mode is off.
  prop.absControl = TRUE;       //Ensure the property is set up to use absolute value control.
  prop.absValue = val;           //Set the absolute value of shutter to 20 ms.
  return fc2SetProperty(context1, &prop );   //Set the property.
}


fc2Error setCameraShutter(fc2Context context1, float val)
{
  fc2Property prop;               //Declare a Property struct.
  prop.type = FC2_SHUTTER;        //Define the property to adjust.
  prop.onOff = TRUE;              //Ensure the property is on.
  prop.autoManualMode = FALSE;    //Ensure auto-adjust mode is off.
  prop.absControl = TRUE;         //Ensure the property is set up to use absolute value control.
  prop.absValue = val;            //Set the absolute value of shutter to 20 ms.
  return fc2SetProperty(context1, &prop );   //Set the property.
}


fc2Error setCameraShutter_i(fc2Context context1, unsigned int val)
{
  fc2Property prop;               //Declare a Property struct.
  prop.type = FC2_SHUTTER;        //Define the property to adjust.
  prop.onOff = TRUE;              //Ensure the property is on.
  prop.autoManualMode = FALSE;    //Ensure auto-adjust mode is off.
  prop.absControl = FALSE;         //Ensure the property is set up to use absolute value control.
  prop.valueA = val;            //Set the absolute value of shutter to 20 ms.
  return fc2SetProperty(context1, &prop );   //Set the property.
}


fc2Error setCameraGain(fc2Context context1, float val)
{
  fc2Property prop;               //Declare a Property struct.
  prop.type = FC2_GAIN;           //Define the property to adjust.
  prop.onOff = TRUE;              //Ensure the property is on.
  prop.autoManualMode = FALSE;    //Ensure auto-adjust mode is off.
  prop.absControl = TRUE;         //Ensure the property is set up to use absolute value control.
  prop.absValue = val;            //Set the absolute value of shutter to 20 ms.
  return fc2SetProperty(context1, &prop );   //Set the property.
}

fc2Error setCameraGamma(fc2Context context1, float val)
{
  fc2Property prop;               //Declare a Property struct.
  prop.type = FC2_GAMMA;          //Define the property to adjust.
  prop.onOff = TRUE;              //Ensure the property is on.
  prop.autoManualMode = FALSE;    //Ensure auto-adjust mode is off.
  prop.absControl = TRUE;         //Ensure the property is set up to use absolute value control.
  prop.absValue = val;            //Set the absolute value of shutter to 20 ms.
  return fc2SetProperty(context1, &prop );   //Set the property.
}


fc2Error setCameraExposure(fc2Context context1, float val)
{
  fc2Property prop;               //Declare a Property struct.
  prop.type = FC2_AUTO_EXPOSURE;  //Define the property to adjust.
  prop.onOff = TRUE;              //Ensure the property is on.
  prop.autoManualMode = FALSE;    //Ensure auto-adjust mode is off.
  prop.absControl = TRUE;         //Ensure the property is set up to use absolute value control.
  prop.absValue = val;            //Set the absolute value of shutter to 20 ms.
  return fc2SetProperty(context1, &prop );   //Set the property.
}

float getCameraExposure(fc2Context context1)
{
  fc2Error error;
  fc2Property prop;                          //Declare a Property struct.
  prop.type = FC2_AUTO_EXPOSURE;                     //Define the property to adjust.
  error = fc2GetProperty(context1, &prop );   //Set the property.
  return (error != FC2_ERROR_OK ) ? FLT_MIN : prop.absValue;
}


int change_camera_exposure(void)
{
  static float exp, newexp = 0;
  int i;
  char question[256];

  if(updating_menu_state != 0)	return D_O_K;
  exp = getCameraExposure(context);
  if (newexp == 0) newexp = exp;
  snprintf(question,sizeof(question),"Present camra exposure is %g EV\n"
	   "specify the new one %%8f",exp);
  i = win_scanf(question,&newexp);
  if (i == CANCEL) return D_O_K;
  setCameraExposure(context,newexp);
  return 0;
}



int change_camera_rate(void)
{
  static float rate, newrate = 0;
  int i;
  char question[256];

  if(updating_menu_state != 0)	return D_O_K;
  rate = getCameraFrameRate(context);
  if (newrate <= 0) newrate = rate;
  snprintf(question,sizeof(question),"Present camra rate is %g Hz\n"
	   "specify the new one %%8f",rate);
  i = win_scanf(question,&newrate);
  if (i == CANCEL) return D_O_K;
  setCameraFrameRate(context,newrate);
  return 0;
}
int change_camera_shutter(void)
{
  static float exp, newexp = 0;
  int i;
  char question[256];
  fc2Error error;
  fc2PropertyInfo fpi;

  if(updating_menu_state != 0)	return D_O_K;

  fpi.type = FC2_SHUTTER;                   //Define the property to adjust.
  if ((error = fc2GetPropertyInfo(context, &fpi)) != FC2_ERROR_OK ) 
    print_error(error, "in fc2GetPropertyInfo");

  win_printf("Shutter property : present %d\n"
	     "autoSupported %d manualSupported %d onOffSupported %d onePushSupported %d\n"
	     "absValSupported %d readOutSupported %d\n"
	     "Int min %d max %d\n"
	     "Abs min %g max %g\n"
	     "unit %s\n"
	     "unit A %s\n"
	     ,fpi.present, fpi.autoSupported, fpi.manualSupported, fpi.onOffSupported,
	     fpi.onePushSupported, fpi.absValSupported, fpi.readOutSupported, 
	     fpi.min, fpi.max, fpi.absMin, fpi.absMax, fpi.pUnits, fpi.pUnitAbbr);
    
  exp = getCameraShutter(context);
  if (newexp <= 0) newexp = exp;
  snprintf(question,sizeof(question),"Present shutter is %g \n"
	   "specify the new one %%8f",exp);
  i = win_scanf(question,&newexp);
  if (i == CANCEL) return D_O_K;
  setCameraShutter(context,newexp);
  return 0;
}

int change_camera_shutter_i(void)
{
  static float exp = 0;
  static int i,  newexp = 0;
  char question[256];
  fc2Error error;
  fc2PropertyInfo fpi;

  if(updating_menu_state != 0)	return D_O_K;

  fpi.type = FC2_SHUTTER;                   //Define the property to adjust.
  if ((error = fc2GetPropertyInfo(context, &fpi)) != FC2_ERROR_OK ) 
    print_error(error, "in fc2GetPropertyInfo");

  win_printf("Shutter property : present %d\n"
	     "autoSupported %d manualSupported %d onOffSupported %d onePushSupported %d\n"
	     "absValSupported %d readOutSupported %d\n"
	     "Int min %d max %d\n"
	     "Abs min %g max %g\n"
	     "unit %s\n"
	     "unit A %s\n"
	     ,fpi.present, fpi.autoSupported, fpi.manualSupported, fpi.onOffSupported,
	     fpi.onePushSupported, fpi.absValSupported, fpi.readOutSupported, 
	     fpi.min, fpi.max, fpi.absMin, fpi.absMax, fpi.pUnits, fpi.pUnitAbbr);
    
  exp = getCameraShutter(context);
  snprintf(question,sizeof(question),"Present shutter is %g \n"
	   "specify the new one int %%8d",exp);
  i = win_scanf(question,&newexp);
  if (i == CANCEL) return D_O_K;
  setCameraShutter_i(context,newexp);
  return 0;
}


int change_camera_gain(void)
{
  static float gain, newgain = 0;
  int i;
  char question[256];

  if(updating_menu_state != 0)	return D_O_K;
  gain = getCameraGain(context);
  if (newgain <= 0) newgain = gain;
  snprintf(question,sizeof(question),"Present gain is %g \n"
	   "specify the new one %%8f",gain);
  i = win_scanf(question,&newgain);
  if (i == CANCEL) return D_O_K;
  setCameraGain(context,newgain);
  return 0;
}


int change_camera_gamma(void)
{
  static float gamma, newgamma = -10;
  int i;
  char question[256];

  if(updating_menu_state != 0)	return D_O_K;
  gamma = getCameraGamma(context);
  if (newgamma <= 10) newgamma = gamma;
  snprintf(question,sizeof(question),"Present gamma is %g \n"
	   "specify the new one %%8f",gamma);
  i = win_scanf(question,&newgamma);
  if (i == CANCEL) return D_O_K;
  setCameraGamma(context,newgamma);
  return 0;
}


int test_camera2(void)
{        
  fc2Error error;
  fc2PGRGuid guid;
  unsigned int numCameras = 0;    
  fc2Config config;
  //const int k_numImages = 100;
  fc2Image rawImage;
  //fc2TimeStamp prevTimestamp = {0};
  imreg *imr;
  O_i *oid;

  if(updating_menu_state != 0)	return D_O_K;

  //PrintBuildInfo();

  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf_OK("cannot find image region");


  error = fc2CreateContext( &context );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2CreateContext: %d\n", error );
      return 0;
    }        

  error = fc2GetNumOfCameras( context, &numCameras );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2GetNumOfCameras: %d\n", error );
      return 0;
    }

  if ( numCameras == 0 )
    {
      // No cameras detected
      win_printf( "No cameras detected.\n");
      return 0;
    }

  // Get the 0th camera
  error = fc2GetCameraFromIndex( context, 0, &guid );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2GetCameraFromIndex: %d\n", error );
      return 0;
    }    

  error = fc2Connect( context, &guid );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2Connect: %d\n", error );
      return 0;
    }

  PrintCameraInfo( context );    

  SetTimeStamping( context, TRUE );

  win_printf("Camera rate %g Hz\nCamera shutter %g\n"
	     "Camera gain %g \nCamera Gamma %g\n"
	     ,getCameraFrameRate(context),getCameraShutter(context)
	     ,getCameraGain(context),getCameraGamma(context));


  fc2GetConfiguration( context,  &config);
  win_printf("Camera config %u buffers, mode %d",config.numBuffers,config.grabMode);

  win_scanf("change buff # %d",&(config.numBuffers));
  fc2SetConfiguration( context,  &config);

  fc2GetConfiguration( context,  &config);
  win_printf("Camera config %u buffers, mode %d",config.numBuffers,config.grabMode);

  error = fc2StartCapture( context );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2StartCapture: %d\n", error );
      return 0;
    }


  error = fc2CreateImage( &rawImage );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2CreateImage: %d\n", error );
    }

  error = fc2RetrieveBuffer( context, &rawImage );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in retrieveBuffer: %d\n", error);
    }

  win_printf("image prop %u x %u, stride %u size %u"
		  ,rawImage.rows,rawImage.cols,rawImage.stride,rawImage.dataSize);

  error = fc2StopCapture( context );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2StopCapture: %d\n", error );
      return 0;
    }

  oid = create_and_attach_movie_to_imr(imr, rawImage.cols, rawImage.rows, IS_CHAR_IMAGE, 2);
  if (oid == NULL)	  return win_printf_OK("Can't create dest image");

  fc2StartCaptureCallback ( context, fc2ImageEventCallback_1, (void*)oid); 

  //fc2SetCallback(context, fc2ImageEventCallback_1, (void*)oid); 


  map_pixel_ratio_of_image_and_screen(oid, 1, 1);
  set_zmin_zmax_values(oid, 0, 255);
  set_z_black_z_white_values(oid, 0, 255);

  refresh_image(imr, imr->n_oi - 1);
  oid->oi_idle_action = oi_idle_action;

  /*



  GrabImages( context, k_numImages );   

  error = fc2StopCapture( context );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2StopCapture: %d\n", error );
      return 0;
    }

  error = fc2DestroyContext( context );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2DestroyContext: %d\n", error );
      return 0;
    }

  win_printf( "Done!\n" );
  */
  return 0;
}

void fc2ImageEventCallback_2(fc2Image *image, void *pCallbackData)
{
  O_i *oi = (O_i*)pCallbackData;
  int nf, cf;

  static unsigned int im = 0;
  if (image == NULL) 
    {
      my_set_window_title("image is NULL");
      return;
    }
  //fc2RetrieveBuffer( context, &rawImage );
  nf = (oi->im.n_f > 0) ? oi->im.n_f : 1;
  cf = oi->im.c_f;
  cf++;
  cf = (cf < nf) ? cf : 0;
  /*
  for (i = 0; i < oi->im.ny && i < image->rows; i++)
    {
      pd = oi->im.pxl[cf][i].ch;
      ps = image->pData + i * image->cols;
      for (j = 0; j < oi->im.nx && j < image->cols; j++)
	pd[j] = ps[j];
    }
  */
  switch_frame(oi,cf);
  my_set_window_title("im %u id %u x %u, recieved %u oi %d x %d ",im++,image->cols,image->rows,image->receivedDataSize,oi->im.nx,oi->im.ny);
  return;
}



void fc2ImageEventCallback_7(fc2Image *image, void *pCallbackData)
{
  register int i, j;
  PG_data *PG_d1 = (PG_data*)pCallbackData;
  d_s *ds;
  static unsigned int im = 0;
  static int ipd = 0;
  static unsigned long  t, t_1 = 0, t_16 = 0;// t1
  double dt;

  if (image == NULL) 
    {
      my_set_window_title("image is NULL");
      return;
    }

  for(i = 0; i < PG_d1->oid->im.n_f; i++)
    if (image->pData == (unsigned char *)PG_d1->oid->im.mem[i]) break;
  ipd = i;

  my_set_window_title("im %u id %u x %u, recieved %u oi frame %d",im,image->cols,
  		      image->rows,image->receivedDataSize,ipd);  



  if (ipd < PG_d1->oid->im.n_f) 
    {
      switch_frame(PG_d1->oid,ipd); 
      my_set_window_title("Frame %d",ipd);
      //printf("Frame %d",ipd);
      
      if (PG_d1->opi != NULL && PG_d1->opi->n_dat > 0)
	{
	  ds = PG_d1->opi->dat[0];
	  j = im % ds->mx;
	  ds->xd[j] = im;
	  ds->nx = ((int)(im+1) < ds->mx) ? (int)(im + 1) : ds->mx;
	  if (PG_d1->oid->im.data_type == IS_CHAR_IMAGE)
	    ds->yd[j] = PG_d1->oid->im.pixel[PG_d1->oid->im.ny/2].ch[PG_d1->oid->im.nx/2];
	  else if (PG_d1->oid->im.data_type == IS_UINT_IMAGE)
	    ds->yd[j] = PG_d1->oid->im.pixel[PG_d1->oid->im.ny/2].ui[PG_d1->oid->im.nx/2];
	  PG_d1->opi->need_to_refresh = 1; 
	}
      if (PG_d1->opt != NULL && PG_d1->opt->n_dat > 0)
	{
	  ds = PG_d1->opt->dat[0];
	  j = im % ds->mx;
	  ds->nx = (im+1 < (unsigned int)ds->mx) ? im + 1 : (unsigned int)ds->mx;
	  t = my_uclock();
	  ds->yd[j] = (float)((double)t - (double)t_1); //(float)(((double)(t - t_1))/t1);//im;
	  ds->yd[j] /=  get_my_uclocks_per_sec();
	  ds->yd[j] *= 1000;
	  ds->xd[j] = im;
	  t_1 = t;
	  if (im && !(im & 0x0f))
	    {
	      dt = ((double)t - (double)t_16); //(float)(((double)(t - t_1))/t1);//im;
	      dt /=  16*get_my_uclocks_per_sec();
	      dt = (dt >0) ? (double)1/dt : 0;
	      set_plot_title(PG_d1->opt,"\\pt8 Timing rate %g Hz found %g",getCameraFrameRate(context),dt);
	      t_16 = t;
	    }
	  PG_d1->opt->need_to_refresh = 1; 
	}
      im++;
    }
  /*

  if (ipd < PG_d1->oid->im.n_f) 
    {
      switch_frame(PG_d1->oid,ipd); 
      if (PG_d1->opi != NULL && PG_d1->opi->n_dat > 0)
	{
	  ds = PG_d1->opi->dat[0];
	  j = im % ds->nx;
	  ds->xd[j] = im;
	  if (PG_d1->oid->im.data_type == IS_CHAR_IMAGE)
	    ds->yd[j] = PG_d1->oid->im.pixel[PG_d1->oid->im.ny/2].ch[PG_d1->oid->im.nx/2];
	  else if (PG_d1->oid->im.data_type == IS_UINT_IMAGE)
	    ds->yd[j] = PG_d1->oid->im.pixel[PG_d1->oid->im.ny/2].ui[PG_d1->oid->im.nx/2];
	  PG_d1->opi->need_to_refresh = 1; 
	}
      if (PG_d1->opt != NULL && PG_d1->opt->n_dat > 0)
	{
	  ds = PG_d1->opt->dat[0];
	  j = im % ds->nx;
	  //ts = fc2GetImageTimeStamp(image);
	  t = my_uclock();
	  ds->yd[j] = (float)((double)t - (double)t_1); //(float)(((double)(t - t_1))/t1);//im;
	  ds->yd[j] /= t1;
	  ds->yd[j] *= 1000;
	  ds->xd[j] = im;//ts.cycleSeconds + 8000 + ts.cycleCount;
	  t_1 = t;
	  PG_d1->opt->need_to_refresh = 1; 
	}
    }
  im++;
  */
  return;
}



int test_camera3(void)
{        
  fc2Error error;
  fc2PGRGuid guid;
  unsigned int numCameras = 0;    
  //const int k_numImages = 100;
  fc2Image rawImage;
  //fc2TimeStamp prevTimestamp = {0};
  imreg *imr;
  O_i *oid;
  fc2VideoMode videoMode; 
  fc2FrameRate frameRate;

  if(updating_menu_state != 0)	return D_O_K;

  //PrintBuildInfo();

  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf_OK("cannot find image region");


  error = fc2CreateContext( &context );
  if ( error != FC2_ERROR_OK )
    {      win_printf( "Error in fc2CreateContext: %d\n", error );      return 0;    }        

  error = fc2GetNumOfCameras( context, &numCameras );
  if ( error != FC2_ERROR_OK )
    {      win_printf( "Error in fc2GetNumOfCameras: %d\n", error );      return 0;    }

  if ( numCameras == 0 )
    {      win_printf( "No cameras detected.\n");      return 0;    }

  // Get the 0th camera
  error = fc2GetCameraFromIndex( context, 0, &guid );
  if ( error != FC2_ERROR_OK )
    {      win_printf( "Error in fc2GetCameraFromIndex: %d\n", error );      return 0;    }    

  error = fc2Connect( context, &guid ); 
  if ( error != FC2_ERROR_OK ) 
    {       win_printf( "Error in fc2Connect: %d\n", error );      return 0;    }

  PrintCameraInfo( context );    

  SetTimeStamping( context, TRUE );


  error = fc2GetVideoModeAndFrameRate(context, &videoMode, &frameRate); 
  if ( error != FC2_ERROR_OK )
    {      win_printf( "Error in fc2StartCapture: %d\n", error );      return 0;    }




  error = fc2StartCapture( context );
  if ( error != FC2_ERROR_OK )
    {      win_printf( "Error in fc2StartCapture: %d\n", error );      return 0;    }

  error = fc2CreateImage( &rawImage );
  if ( error != FC2_ERROR_OK )    win_printf( "Error in fc2CreateImage: %d\n", error );   

  error = fc2RetrieveBuffer( context, &rawImage );
  if ( error != FC2_ERROR_OK )    win_printf( "Error in retrieveBuffer: %d\n", error);


  error = fc2StopCapture( context );
  if ( error != FC2_ERROR_OK )
    {      win_printf( "Error in fc2StopCapture: %d\n", error );      return 0;    }


  /*

  fc2GetConfiguration( context,  &config);
  win_scanf("change buff # %d",&(config.numBuffers));
  config.grabMode.BUFFER_FRAMES;
  fc2SetConfiguration( context,  &config);

  fc2GetConfiguration( context,  &config);
  win_printf("Camera config %u buffers, mode %d",config.numBuffers,config.grabMode);

  */



  oid = create_and_attach_movie_to_imr(imr, rawImage.cols, rawImage.rows, IS_CHAR_IMAGE, 10);
  if (oid == NULL)	  return win_printf_OK("Can't create dest image");




  error = fc2SetUserBuffers( context, oid->im.pxl[0][0].ch,  rawImage.cols * rawImage.rows, 10);
  if ( error != FC2_ERROR_OK )
    {  win_printf( "Error in fc2StopCapture setting buffers: %d\n", error ); return 0;    }


  fc2StartCaptureCallback ( context, fc2ImageEventCallback_2, (void*)oid); 

  //fc2SetCallback(context, fc2ImageEventCallback_1, (void*)oid); 


  map_pixel_ratio_of_image_and_screen(oid, 1, 1);
  set_zmin_zmax_values(oid, 0, 255);
  set_z_black_z_white_values(oid, 0, 255);

  refresh_image(imr, imr->n_oi - 1);
  oid->oi_idle_action = oi_idle_action;
  return 0;
}

int oi_idle_PG_action(struct  one_image *oi, DIALOG *d)
{
  // we put in this routine all the screen display stuff
  imreg *imr;
  BITMAP *imb;
  unsigned long t0 = 0;
  static int init = 1;


  if (d->dp == NULL)    return 1;
  imr = (imreg*)d->dp;        /* the image region is here */
  if (imr->one_i == NULL || imr->n_oi == 0)        return 1;

  if (init == 1)
    {
      oi->need_to_refresh |= ALL_NEED_REFRESH;	
      refresh_image(imr, UNCHANGED);
      init = 0;
      return 0;
    }
  t0 = my_uclock();
  if ((IS_DATA_IN_USE(oi) == 0) && (oi->need_to_refresh & BITMAP_NEED_REFRESH))
    {
      display_image_stuff_16M(imr,d);
      oi->need_to_refresh |= INTERNAL_BITMAP_NEED_REFRESH;
    }
  imb = (BITMAP*)oi->bmp.stuff;
  if (oi->need_to_refresh & INTERNAL_BITMAP_NEED_REFRESH)
    {
      SET_BITMAP_IN_USE(oi);
      acquire_bitmap(screen);
      blit(imb,screen,0,0,imr->x_off //+ d->x
	   , imr->y_off - imb->h + d->y, imb->w, imb->h); 
      release_bitmap(screen);
      oi->need_to_refresh &= ~INTERNAL_BITMAP_NEED_REFRESH;
      t0 = my_uclock() - t0;
      SET_BITMAP_NO_MORE_IN_USE(oi);
    }
  return 0;
}



 void fc2ImageEventCallback_4(fc2Image *image, void *pCallbackData)
{
  register int i, j;
  PG_data *PG_d1 = (PG_data*)pCallbackData;
  d_s *ds;
  static unsigned char* pD[128] = {0};
  static unsigned int init_oi = 0, im = -1;
  static int ipd = 0, npd = 0;
  //fc2TimeStamp ts;
  static unsigned long  t, t_1 = 0, t_16 = 0;// t1
  double dt;

  if (image == NULL) 
    {
      my_set_window_title("image is NULL");
      return;
    }
  // WE find image buffer index 
  for(i = 0; i < 128 && i < npd && pD[i] != image->pData; i++);
  if (i == npd && npd < 128)
    {  // a new buffer not yet store
      pD[npd] = image->pData;
      if (add_memory_to_one_movie(PG_d1->oid, PG_d1->oid->im.data_type, npd, image->pData))
	win_printf("Error in building movie");
      my_set_window_title("adding frame %d",npd);
      //printf("adding frame %d",npd);
      npd++;
    }
  else ipd = i;

  if ((PG_d1->oid->oi_idle_action == NULL) && (npd == PG_d1->oid->im.n_f))
    {
      add_image(PG_d1->imr, PG_d1->oid->im.data_type, (void*)PG_d1->oid);
      PG_d1->imr->cur_oi = PG_d1->imr->n_oi - 1;
      PG_d1->imr->one_i = PG_d1->imr->o_i[PG_d1->imr->cur_oi];
      PG_d1->oid->oi_idle_action = oi_idle_PG_action;
      PG_d1->imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;    
      init_oi = 1;
      //t1 = get_my_uclocks_per_sec();
      my_set_window_title("preparing idle");
      //printf("preparing idle");
    }

  //my_set_window_title("im %u id %u x %u, recieved %u oi %d x %d ",im,image->cols,
  //		      image->rows,image->receivedDataSize,PG_d1->oid->im.nx,PG_d1->oid->im.ny);  

  if (npd >= PG_d1->oid->im.n_f) 
    {
      switch_frame(PG_d1->oid,ipd); 
      my_set_window_title("Frame %d",ipd);
      //printf("Frame %d",ipd);
      
      if (PG_d1->opi != NULL && PG_d1->opi->n_dat > 0)
	{
	  ds = PG_d1->opi->dat[0];
	  j = im % ds->mx;
	  ds->xd[j] = im;
	  ds->nx = ((int)(im+1) < ds->mx) ? (int)(im + 1) : ds->mx;
	  if (PG_d1->oid->im.data_type == IS_CHAR_IMAGE)
	    ds->yd[j] = PG_d1->oid->im.pixel[PG_d1->oid->im.ny/2].ch[PG_d1->oid->im.nx/2];
	  else if (PG_d1->oid->im.data_type == IS_UINT_IMAGE)
	    ds->yd[j] = PG_d1->oid->im.pixel[PG_d1->oid->im.ny/2].ui[PG_d1->oid->im.nx/2];
	  PG_d1->opi->need_to_refresh = 1; 
	}
      if (PG_d1->opt != NULL && PG_d1->opt->n_dat > 0)
	{
	  ds = PG_d1->opt->dat[0];
	  j = im % ds->mx;
	  ds->nx = ((int)(im+1) < ds->mx) ? (int)(im + 1) : ds->mx;
	  t = my_uclock();
	  ds->yd[j] = (float)((double)t - (double)t_1); //(float)(((double)(t - t_1))/t1);//im;
	  ds->yd[j] /=  get_my_uclocks_per_sec();
	  ds->yd[j] *= 1000;
	  ds->xd[j] = im;
	  t_1 = t;
	  if (im && !(im & 0x0f))
	    {
	      dt = ((double)t - (double)t_16); //(float)(((double)(t - t_1))/t1);//im;
	      dt /=  16*get_my_uclocks_per_sec();
	      dt = (dt >0) ? (double)1/dt : 0;
	      set_plot_title(PG_d1->opt,"\\pt8 Timing rate %g Hz found %g",getCameraFrameRate(context),dt);
	      t_16 = t;
	    }
	  PG_d1->opt->need_to_refresh = 1; 
	}
      im++;
    }
  return;
}


int test_camera4(void)
{        
  fc2Error error;
  fc2PGRGuid guid;
  unsigned int numCameras = 0;    
  fc2Config config;
  //const int k_numImages = 100;
  fc2Image rawImage;
  fc2VideoMode videoMode; 
  fc2FrameRate frameRate;
  //fc2TimeStamp prevTimestamp = {0};
  imreg *imr;
  O_i *oid;
  int Byte_nb = 1, type;

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf_OK("cannot find image region");


  error = fc2CreateContext( &context );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2CreateContext: %d\n", error );
      return 0;
    }        

  error = fc2GetNumOfCameras( context, &numCameras );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2GetNumOfCameras: %d\n", error );
      return 0;
    }

  if ( numCameras == 0 )
    {
      // No cameras detected
      win_printf( "No cameras detected.\n");
      return 0;
    }

  // Get the 0th camera
  error = fc2GetCameraFromIndex( context, 0, &guid );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2GetCameraFromIndex: %d\n", error );
      return 0;
    }    

  error = fc2Connect( context, &guid );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2Connect: %d\n", error );
      return 0;
    }

  PrintCameraInfo( context );    

  SetTimeStamping( context, TRUE );

  win_printf("Camera rate %g Hz\nCamera shutter %g\n"
	     "Camera gain %g \nCamera Gamma %g\n"
	     ,getCameraFrameRate(context),getCameraShutter(context)
	     ,getCameraGain(context),getCameraGamma(context));


  fc2GetConfiguration( context,  &config);
  win_scanf("How many buffer do you want %8d mode %8d",&(config.numBuffers),&(config.grabMode));
  fc2SetConfiguration( context,  &config);
  fc2GetConfiguration( context,  &config);
  win_printf("Camera config %u buffers, mode %d",config.numBuffers,config.grabMode);



  error = fc2GetVideoModeAndFrameRate(context, &videoMode, &frameRate); 
  if ( error != FC2_ERROR_OK )
    {      win_printf( "Error in fc2StartCapture: %d\n", error );      return 0;    }
  win_printf("Videomode %d and frame rate %d",videoMode,frameRate); 


  error = fc2StartCapture( context );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2StartCapture: %8d (<128)\n", error );
      return 0;
    }


  error = fc2CreateImage( &rawImage );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2CreateImage: %d\n", error );
    }

  error = fc2RetrieveBuffer( context, &rawImage );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in retrieveBuffer: %d\n", error);
    }


  if (rawImage.format == FC2_PIXEL_FORMAT_MONO8 || rawImage.format == FC2_PIXEL_FORMAT_RAW8)
    {Byte_nb = 1; type = IS_CHAR_IMAGE;}
  else if (rawImage.format == FC2_PIXEL_FORMAT_MONO16 || rawImage.format == FC2_PIXEL_FORMAT_RAW16)
    {Byte_nb = 2; type = IS_UINT_IMAGE;}
  else Byte_nb = 0;

  win_printf("image prop %u x %u, stride %u size %u #of byte %d"
	     ,rawImage.rows,rawImage.cols,rawImage.stride,rawImage.dataSize,Byte_nb);


  error = fc2StopCapture( context );
  if ( error != FC2_ERROR_OK )
    {
      win_printf( "Error in fc2StopCapture: %d\n", error );
      return 0;
    }

  oid = create_one_movie_not_allocated(rawImage.cols, rawImage.rows, type, config.numBuffers);
  if (oid == NULL)	  return win_printf_OK("Can't create dest image");


  PG_d.imr = imr;
  PG_d.oid = oid;

  create_timing_plot();

  fc2StartCaptureCallback(context, fc2ImageEventCallback_4, (void*)&PG_d); 

  //fc2SetCallback(context, fc2ImageEventCallback_1, (void*)oid); 


  map_pixel_ratio_of_image_and_screen(oid, 1, 1);
  set_zmin_zmax_values(oid, 0, 255);
  set_z_black_z_white_values(oid, 0, 255);
  return 0;
}


# define USE_BUFFERS
int test_camera_7(void)
{        
# ifdef USE_BUFFERS
  unsigned int bufferSize, totalSize, imsize = 0;    
  unsigned char *pBufferArea = NULL;
# endif
  int i;
  char question[512] = {0};
  static int notfullsize = 0, notcentered = 0, mono8 = 0;
  fc2Error error;
  fc2PGRGuid guid;
  int width = 1920, height = 1200, offX = 0, offY = 0;
  unsigned int numCameras = 0;
  //fc2Image rawImage;
  imreg *imr;
  O_i *oid;
  //fc2VideoMode videoMode; 
  //fc2FrameRate frameRate;
  fc2Format7Info f7Info;
  BOOL supported = FALSE;
  fc2Format7ImageSettings f7Settings;
  BOOL settingsAreValid = FALSE;
  fc2Format7PacketInfo packetInfo;
  fc2Config config;
  int Byte_nb = 1, type;

  if(updating_menu_state != 0)	return D_O_K;

  //PrintBuildInfo();

  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf_OK("cannot find image region");

  if ((error = fc2CreateContext( &context )) != FC2_ERROR_OK )
    print_error(error, "in fc2CreateContext");

  if ((error = fc2GetNumOfCameras( context, &numCameras )) != FC2_ERROR_OK )
    print_error(error, "in fc2GetNumOfCameras");

  if ( numCameras == 0 )     {      win_printf( "No cameras detected.\n");      return 0;    }

  // Get the 0th camera
  if ((error = fc2GetCameraFromIndex( context, 0, &guid )) != FC2_ERROR_OK )
    print_error(error, "in fc2GetCameraFromIndex");

  if ((error = fc2Connect( context, &guid )) != FC2_ERROR_OK ) 
    print_error(error, "in fc2Connect");

  f7Info.mode = FC2_MODE_0;
  if ((error = fc2GetFormat7Info(context, &f7Info, &supported)) != FC2_ERROR_OK ) 
    print_error(error, "in fc2GetFormat7info");
  if (!supported) return win_printf("Format7 not supported\n");

  /*
  win_printf("Format 7 : maxWidth %u maxHeight %u\n"
	     "offsetHStepSize %u, offsetVStepSize %u,\n"
	     "imageHStepSize %u, imageVStepSize %u,\n"
	     "pixelFormatBitField¨%u, vendorPixelFormatBitField %u\n"
	     "packetSize %u, minPacketSize %u, maxPacketSize %u\n"
	     "percentage %g"
	     ,f7Info.maxWidth, f7Info.maxHeight, f7Info.offsetHStepSize,f7Info.offsetVStepSize, f7Info.imageHStepSize,
	     f7Info.imageVStepSize, f7Info.pixelFormatBitField, f7Info.vendorPixelFormatBitField,
	     f7Info.packetSize, f7Info.minPacketSize, f7Info.maxPacketSize,f7Info.percentage);

  
  */

  if ((error = fc2GetConfiguration( context,  &config)) != FC2_ERROR_OK ) 
    print_error(error, "in fc2GetConfiguration");

  snprintf(question,sizeof(question),"What data type do you want ?\n"
	   "%%R->raw8 %%r->mono8 %%r->raw16 %%r->mono16\n"
	   "Define ROI size and postion\n"
	   "%%R to Sensor size %dx%d or %%r ROI define below\n"
	   "Width = %%4dx%d Heigh = %%4dx%d\n"
	   "%%RCenter ROI or %%r specify position\n"
	   "Offset in X = %%4dx%d Offset in y = %%4dx%d\n"
	   "Define number of Frames %%4d in the circular buffer\n"
	   ,f7Info.maxWidth, f7Info.maxHeight
	   ,f7Info.imageHStepSize, f7Info.imageVStepSize
	   ,f7Info.offsetHStepSize,f7Info.offsetVStepSize);

  i = win_scanf(question,&mono8,&notfullsize,&width,&height,&notcentered,&offX,&offY,&(config.numBuffers));
  if (i == CANCEL) return 0;

  if (notfullsize == (unsigned int)0)
    {
      width = f7Info.maxWidth;
      height = f7Info.maxHeight;
      offX = 0;
      offY = 0;
    }
  else
    {
      width *= f7Info.imageHStepSize;
      height *= f7Info.imageVStepSize;
      width = (width > (int)f7Info.maxWidth) ? (int)f7Info.maxWidth : width;
      height = (height > (int)f7Info.maxHeight) ? (int)f7Info.maxHeight : height;
      if (notcentered == (unsigned int)0)
	{
	  offX = (f7Info.maxWidth - width)/2;
	  offX = f7Info.offsetHStepSize * (offX/f7Info.offsetHStepSize);
	  offY = (f7Info.maxHeight - height)/2;
	  offY = f7Info.offsetVStepSize * (offY/f7Info.offsetVStepSize);
	}
      else
	{
	  offX *= f7Info.offsetHStepSize;
	  if (offX + width > (int)f7Info.maxWidth)
	    {
	      offX =  f7Info.maxWidth - width;
	      offX = f7Info.offsetHStepSize * (offX/f7Info.offsetHStepSize);
	    }
	  offY *= f7Info.offsetVStepSize;
	  if (offY + height > (int)f7Info.maxHeight)
	    {
	      offY = f7Info.maxHeight - height;
	      offY = f7Info.offsetVStepSize * (offY/f7Info.offsetVStepSize);
	    }
	}
    }
  if (mono8 == 0)      f7Settings.pixelFormat = FC2_PIXEL_FORMAT_RAW8;
  else if (mono8 == 1) f7Settings.pixelFormat = FC2_PIXEL_FORMAT_MONO8;
  else if (mono8 == 2) f7Settings.pixelFormat = FC2_PIXEL_FORMAT_RAW16;
  else if (mono8 == 3) f7Settings.pixelFormat = FC2_PIXEL_FORMAT_MONO16;
  else return win_printf( "Wrong data type!\n");

  type = (mono8 < 2) ? IS_CHAR_IMAGE : IS_UINT_IMAGE;
  Byte_nb = 1 + mono8/2;

# ifdef USE_BUFFERS
  config.grabMode = FC2_BUFFER_FRAMES;
# else
  config.grabMode = FC2_DROP_FRAMES;
# endif
  if ((error = fc2SetConfiguration( context,  &config)) != FC2_ERROR_OK ) 
    print_error(error, "in fc2SetConfiguration");


  f7Settings.width = width;
  f7Settings.height = height;
  f7Settings.offsetX = offX;
  f7Settings.offsetY = offY;
  f7Settings.mode = FC2_MODE_0;

  if ((error = fc2ValidateFormat7Settings(context, &f7Settings, &settingsAreValid, &packetInfo)) != FC2_ERROR_OK) 
    print_error(error, "in fc2ValidateFormat7Settings");

  if (settingsAreValid == FALSE) win_printf("Invalid Format7 settings\n");
  // Set f7 settings

  if ((error = fc2SetConfiguration( context,  &config)) != FC2_ERROR_OK ) 
    print_error(error, "in fc2SetConfiguration");


if ((error = fc2GetConfiguration( context,  &config)) != FC2_ERROR_OK ) 
    print_error(error, "in fc2GetConfiguration");
  //win_printf("Camera config %u buffers, mode %d",config.numBuffers,config.grabMode);



  if((error = fc2SetFormat7Configuration(context, &f7Settings, f7Info.percentage)) != FC2_ERROR_OK) 
    return print_error(error,"fc2SetFormat7Configuration");


/*
  if((error = fc2SetFormat7ConfigurationPacket(context, &f7Settings, f7Info.maxPacketSize)) != FC2_ERROR_OK) 
    return print_error(error,"fc2SetFormat7ConfigurationPacket");
*/


  
# ifdef USE_BUFFERS
  imsize = width * height * Byte_nb;
  bufferSize = ((unsigned int)(imsize + 1024 - 1) / 1024) * 1024;
  totalSize = config.numBuffers * bufferSize;

  pBufferArea = (unsigned char*)malloc(totalSize);
  if (pBufferArea == NULL) return win_printf("Failed to allocate memory for buffers\n");
# endif  

  oid = create_one_movie_not_allocated(width, height, type, config.numBuffers); //type
  if (oid == NULL) return win_printf("cannot create oid\n");

# ifdef USE_BUFFERS  
  for(i = 0; i < (int)config.numBuffers; i++)
    add_memory_to_one_movie(oid, type, i, pBufferArea + i * bufferSize);


  if((error = fc2SetUserBuffers( context, pBufferArea, bufferSize, config.numBuffers)) != FC2_ERROR_OK )
    print_error(error, "in fc2StopCapture setting buffers");




# endif  

  PG_d.imr = imr;
  PG_d.oid = oid;

  create_timing_plot();

# ifdef USE_BUFFERS
  fc2StartCaptureCallback(context, fc2ImageEventCallback_7, (void*)&PG_d); 
# else
  fc2StartCaptureCallback(context, fc2ImageEventCallback_4, (void*)&PG_d); 
# endif

  map_pixel_ratio_of_image_and_screen(oid, 1, 1);
  if(Byte_nb == 1)
    {
      set_zmin_zmax_values(oid, 0, 255);
      set_z_black_z_white_values(oid, 0, 255);
    }
  else
    {
      set_zmin_zmax_values(oid, 0, 65535);
      set_z_black_z_white_values(oid, 0, 65535);
    } 

# ifdef USE_BUFFERS
  add_image(imr, oid->im.data_type, (void*)oid);
  imr->cur_oi = imr->n_oi - 1;
  imr->one_i = imr->o_i[imr->cur_oi];
  oid->oi_idle_action = oi_idle_action;
  imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;    
# endif
  refresh_image(imr, imr->n_oi - 1);

  return 0;

}






MENU *xvPointGrey_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Test", test_camera,NULL,0,NULL);
  return mn;
}

MENU *xvPointGrey_image_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Test live", test_camera2,NULL,0,NULL);
  add_item_to_menu(mn,"Test live 3", test_camera3,NULL,0,NULL);
  add_item_to_menu(mn,"Frame rate", change_camera_rate,NULL,0,NULL);
  add_item_to_menu(mn,"Shutter", change_camera_shutter,NULL,0,NULL);
  add_item_to_menu(mn,"Shutter i", change_camera_shutter_i,NULL,0,NULL);
  add_item_to_menu(mn,"Gain", change_camera_gain,NULL,0,NULL);
  add_item_to_menu(mn,"Gamma", change_camera_gamma,NULL,0,NULL);
  add_item_to_menu(mn,"Exposure", change_camera_exposure,NULL,0,NULL);
  add_item_to_menu(mn,"Test live 4", test_camera4,NULL,0,NULL);
  add_item_to_menu(mn,"Test Format 7", test_camera_7,NULL,0,NULL);

  return mn;
}



int	xvPointGrey_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "xvPointGrey", NULL, xvPointGrey_plot_menu(), 0, NULL);
  add_image_treat_menu_item ( "xvPointGrey", NULL, xvPointGrey_image_menu(), 0, NULL);
  return D_O_K;
}

int	xvPointGrey_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(image_treat_menu, "xvPointGrey", NULL, NULL);
  return D_O_K;
}




#endif

# ifdef KEEP

int test_camera_7(void)
{        
  //int i;
  fc2Error error;
  fc2PGRGuid guid;
  int width = 1920, height = 1200, offX = 0, offY = 0;
  unsigned int numCameras = 0, imsize = 0;
  unsigned int bufferSize, totalSize;    
  //unsigned char *pBufferArea = NULL;
  //fc2Image rawImage;
  imreg *imr;
  O_i *oid;
  //fc2VideoMode videoMode; 
  //fc2FrameRate frameRate;
  fc2Format7Info f7Info;
  //BOOL supported = FALSE;
  fc2Format7ImageSettings f7Settings;
  BOOL settingsAreValid = FALSE;
  fc2Format7PacketInfo packetInfo;
  fc2Config config;


  if(updating_menu_state != 0)	return D_O_K;

  //PrintBuildInfo();

  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf_OK("cannot find image region");

  if ((error = fc2CreateContext( &context )) != FC2_ERROR_OK )
    print_error(error, "in fc2CreateContext");

  if ((error = fc2GetNumOfCameras( context, &numCameras )) != FC2_ERROR_OK )
    print_error(error, "in fc2GetNumOfCameras");

  if ( numCameras == 0 )     {      win_printf( "No cameras detected.\n");      return 0;    }

  // Get the 0th camera
  if ((error = fc2GetCameraFromIndex( context, 0, &guid )) != FC2_ERROR_OK )
    print_error(error, "in fc2GetCameraFromIndex");

  if ((error = fc2Connect( context, &guid )) != FC2_ERROR_OK ) 
    print_error(error, "in fc2Connect");

  /*
  if ((error = fc2GetFormat7Info(context, &f7Info, &supported)) != FC2_ERROR_OK ) 
    print_error(error, "in fc2GetFormat7info");
  if (!supported) return win_printf("Format7 not supported\n");


  win_printf("Format 7 : maxWidth %u maxHeight %u\n"
	     "offsetHStepSize %u, offsetVStepSize %u,\n"
	     "imageHStepSize %u, imageVStepSize %u,\n"
	     "pixelFormatBitField¨%u, vendorPixelFormatBitField %u\n"
	     "packetSize %u, minPacketSize %u, maxPacketSize %u\n"
	     "percentage %g"
	     ,f7Info.maxWidth, f7Info.maxHeight, f7Info.offsetHStepSize,f7Info.offsetVStepSize, f7Info.imageHStepSize,
	     f7Info.imageVStepSize, f7Info.pixelFormatBitField, f7Info.vendorPixelFormatBitField,
	     f7Info.packetSize, f7Info.minPacketSize, f7Info.maxPacketSize,f7Info.percentage);

  */


  if ((error = fc2GetConfiguration( context,  &config)) != FC2_ERROR_OK ) 
    print_error(error, "in fc2GetConfiguration");

  win_scanf("Change buff # %8d\n"
	    "ROI size width %8d height %8d\n"
	    "offset in x %8d in y %8d\n"
	    ,&(config.numBuffers),&width,&height,&offX,&offY);
  //config.grabMode = FC2_BUFFER_FRAMES;
  if ((error = fc2SetConfiguration( context,  &config)) != FC2_ERROR_OK ) 
    print_error(error, "in fc2SetConfiguration");


  f7Settings.width = width;
  f7Settings.height = height;
  f7Settings.offsetX = offX;
  f7Settings.offsetY = offY;
  f7Settings.pixelFormat = FC2_PIXEL_FORMAT_RAW8;
  f7Settings.mode = FC2_MODE_0;

  error = fc2ValidateFormat7Settings(context, &f7Settings, &settingsAreValid, &packetInfo);
  if (error != FC2_ERROR_OK) 
    {       win_printf( "Error in fc2ValidateFormat7Settings: %d\n", error );      return 0;    }



  if (settingsAreValid == FALSE) win_printf("Invalid Format7 settings\n");
  // Set f7 settings

  /*
  */
  if ((error = fc2GetConfiguration( context,  &config)) != FC2_ERROR_OK ) 
    print_error(error, "in fc2GetConfiguration");
  win_printf("Camera config %u buffers, mode %d",config.numBuffers,config.grabMode);



  imsize = width * height;
  bufferSize = ((unsigned int)(imsize + 1024 - 1) / 1024) * 1024;
  totalSize = config.numBuffers * bufferSize;

  pBufferArea = (unsigned char*)malloc(totalSize);
  if (pBufferArea == NULL) return win_printf("Failed to allocate memory for buffers\n");


  oid = create_one_movie_not_allocated(width, height, IS_CHAR_IMAGE, config.numBuffers);
  if (oid == NULL) return win_printf("cannot create oid\n");


  for(i = 0; i < (int)config.numBuffers; i++)
    add_memory_to_one_movie(oid, IS_CHAR_IMAGE, i, pBufferArea + i * bufferSize);


  error = fc2SetUserBuffers( context, pBufferArea, bufferSize, config.numBuffers);
  if ( error != FC2_ERROR_OK )
    {  win_printf( "Error in fc2StopCapture setting buffers: %d\n", error ); return 0;    }

  PG_d.imr = imr;
  PG_d.oid = oid;

  create_timing_plot();

  fc2StartCaptureCallback(context, fc2ImageEventCallback_7, (void*)&PG_d); 


  //fc2StartCaptureCallback(context, fc2ImageEventCallback_4, (void*)&PG_d); 


  map_pixel_ratio_of_image_and_screen(oid, 1, 1);
  set_zmin_zmax_values(oid, 0, 255);
  set_z_black_z_white_values(oid, 0, 255);

  refresh_image(imr, imr->n_oi - 1);
  oid->oi_idle_action = oi_idle_action;

  return 0;

}
# endif


/*
fc2Error fc2SetVideoModeAndFrameRate  ( fc2Context  context,  
  fc2VideoMode  videoMode,  
  fc2FrameRate  frameRate  
 ) 



//Declare a Property struct.
Property prop;
//Define the property to adjust.
prop.type = AUTO_EXPOSURE;
//Ensure the property is on.
prop.onOff = true;
//Ensure auto-adjust mode is off.
prop.autoManualMode = false;
//Ensure the property is set up to use absolute value control.
prop.absControl = true;
//Set the absolute value of auto exposure to -3.5 EV.
prop.absValue = -3.5;
//Set the property.
error = cam.SetProperty( &prop );



fc2GetProperty 	( 	fc2Context  	context,
		fc2Property *  	prop	 
	) 


FLYCAPTURE2_C_API fc2Error fc2SetProperty 	( 	fc2Context  	context,
		fc2Property *  	prop	 
	) 			



FC2_FRAME_RATE



Property prop;   //Declare a Property struct.
prop.type = FC2_FRAME_RATE;   //Define the property to adjust.
prop.onOff = true;            //Ensure the property is on.
prop.autoManualMode = false;  //Ensure auto-adjust mode is off.
prop.absControl = true;       //Ensure the property is set up to use absolute value control.
prop.absValue = 20;           //Set the absolute value of shutter to 20 ms.
error = fc2SetProperty(context, &prop );   //Set the property.





//Declare a Property struct.
Property prop;
//Define the property to adjust.
prop.type = SHUTTER;
//Ensure the property is on.
prop.onOff = true;
//Ensure auto-adjust mode is off.
prop.autoManualMode = false;
//Ensure the property is set up to use absolute value control.
prop.absControl = true;
//Set the absolute value of shutter to 20 ms.
prop.absValue = 20;
//Set the property.
error = cam.SetProperty( &prop );


 fc2Error fc2GetEmbeddedImageInfo  ( fc2Context  context,  
  fc2EmbeddedImageInfo *  pInfo  
 )  



*/
