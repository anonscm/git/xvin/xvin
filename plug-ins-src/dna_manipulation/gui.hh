#pragma once
#include "xvin.h"


PXV_FUNC(MENU, *dna_manipulation_plot_menu, (void));
PXV_FUNC(int, dna_manipulation_main, (int argc, char **argv));
PXV_FUNC(int, dna_manipulation_unload, (int argc, char **argv));
PXV_FUNC(int, do_acridine_affinity_check_sequence, (void));
PXV_FUNC(int, do_get_selfhyb_by_size, (void));
