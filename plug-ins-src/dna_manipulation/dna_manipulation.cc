#include "dna_manipulation.hh"
#include "../dna_utils/utils.h"
#include <regex>
#include <string>
#include <algorithm> // std::shuffle
#include <vector>    // std::array
#include <random>    // std::default_random_engine
#include <chrono>    // std::chrono::system_clock
#include <set>
// GAAGAC (2/6)
// GGTCTC (1/5)

// typedef std::set<Sequence> SequenceSet;

int generate_acridine_affinity_check_sequence(int nseqsize, std::ostream& ostr)
{
    int maxIdx = std::pow(4, nseqsize);
    std::vector<int> vec1;
    std::vector<int> vec2;
    std::vector<int> vec3;
    std::vector<int> vec4;
    char oligo1[50] = "";
    char oligo2[50] = "";
    char oligo3[50] = "";
    char oligo4[50] = "";
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::srand(seed);

    if (nseqsize > 50)
    {
        warning_message("Sequence size too big %d > %d ", nseqsize, 50);
    }

    for (int i = 0; i < maxIdx; ++i)
    {
        vec1.push_back(i);
    }

    vec2 = std::vector<int>(vec1.cbegin(), vec1.cend());
    vec3 = std::vector<int>(vec1.cbegin(), vec1.cend());
    vec4 = std::vector<int>(vec1.cbegin(), vec1.cend());
    std::default_random_engine randgen = std::default_random_engine(seed);
    std::shuffle(vec1.begin(), vec1.end(), randgen);
    std::shuffle(vec2.begin(), vec2.end(), randgen);
    std::shuffle(vec3.begin(), vec3.end(), randgen);
    std::shuffle(vec4.begin(), vec4.end(), randgen);
    ostr << ">> acridine affinity check sequence with " << nseqsize << " tested nucleotides" << std::endl;

    while (!vec1.empty())
    {
        get_oligo_by_index(oligo1, nseqsize, vec1.back());
        get_oligo_by_index(oligo2, nseqsize, vec2.back());
        get_oligo_by_index(oligo3, nseqsize, vec3.back());
        get_oligo_by_index(oligo4, nseqsize, vec4.back());
        vec1.pop_back();
        vec2.pop_back();
        vec3.pop_back();
        vec4.pop_back();
        const char *seq1 = std::rand() % 2 ? "CTTG" : "CAAG" ;
        const char *seq2 = std::rand() % 2 ? "CACG" : "CGTG";
        const char *seq3 = std::rand() % 2 ? "ACAG" : "CTGT";
        const char *seq4 = std::rand() % 2 ? "ATCT" : "AGAT";
        ostr << seq1 << oligo1 << seq2 << oligo2 << seq3 << oligo3 << seq4 << oligo4 << std::endl;
    }

    return 0;
}

int get_selfhyb_by_size(int oligo_size, std::ostream& ostr)
{
    char oligo[50] = "";
    std::set<std::string> palindrom;
    int maxIdx = std::pow(4, oligo_size);

    for (int i = 0; i < maxIdx; ++i)
    {
        bool is_palindrom = true;
        get_oligo_by_index(oligo, 4, i);

        for (int j = 0; is_palindrom && j < oligo_size; ++j)
        {
            is_palindrom = (oligo[j] == nucleo_complementary(oligo[oligo_size - j - 1]));
        }
        if (is_palindrom)
        {
            palindrom.insert(std::string(oligo));
        }
    }
    ostr << "Number of self hybridating oligo:" << palindrom.size() << std::endl;
    ostr << "Self hybridating sequences: " << std::endl;
    for (const std::string& ol : palindrom)
    {
        ostr << ol << std::endl;
    }
    ostr << "------------" << std::endl;
    return 0;

}
