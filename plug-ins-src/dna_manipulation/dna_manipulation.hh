#pragma once
#include "../fasta_utils/fasta_utils.h"
#include <vector>
#include <iostream>

int generate_acridine_affinity_check_sequence(int nseqsize, std::ostream& ostr);
int get_selfhyb_by_size(int oligo_size, std::ostream& ostr);
