#include "gui.hh"
#include <iostream>
#include <fstream> 
#include "dna_manipulation.hh"

int do_acridine_affinity_check_sequence(void)
{
    int nseqsize = 0;
    int form_ret = 0;
    char *fullfile = NULL;
    std::fstream ofile;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    nseqsize = get_config_int("DNA_MANIPULATION", "nseqsize", 3);

    form_ret = win_scanf("size of the N oligonucleotide %d",
                         &nseqsize);
    fullfile = save_one_file_config("choose were to save the sequence", NULL, "acridine_seq.fasta", "fasta\0*.fasta*\0All file\0*.*\0\0", "DNA_MANIPULATION", "last_saved");

    if (form_ret != D_O_K || fullfile == NULL)
        return form_ret;

    set_config_int("DNA_MANIPULATION", "nseqsize", nseqsize);


    ofile.open(fullfile, std::ofstream::out | std::ofstream::app);
    
    if(ofile.is_open())
    {
        generate_acridine_affinity_check_sequence(nseqsize, ofile);
    }
    ofile.close();
    return D_O_K;
    
}
int do_get_selfhyb_by_size(void)
{
    int oligo_size = 4;
    int form_ret = 0;
    char *fullfile = NULL;
    std::fstream ofile;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }
    oligo_size = get_config_int("DNA_MANIPULATION", "palind_oligo_size", oligo_size);

    form_ret = win_scanf("size of the oligonucleotides %d",
                         &oligo_size);

    fullfile = save_one_file_config("choose were to save the sequence", NULL, "palindrom.txt", "txt\0*.txt*\0All file\0*.*\0\0", "DNA_MANIPULATION", "last_saved");

    if (form_ret != D_O_K || fullfile == NULL)
        return form_ret;

    set_config_int("DNA_MANIPULATION", "palind_oligo_size", oligo_size);
    ofile.open(fullfile, std::ofstream::out | std::ofstream::app);
    
    if(ofile.is_open())
    {
        get_selfhyb_by_size(oligo_size, ofile);
    }
    else 
    {
        warning_message("can't open file %s", fullfile);
    }
    ofile.close();
    return D_O_K;
    
}

MENU *dna_manipulation_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)
        return mn;
    add_item_to_menu(mn, "generate acridine affinity check sequence",do_acridine_affinity_check_sequence, NULL, 0, NULL);
    add_item_to_menu(mn, "get oligo palindrom", do_get_selfhyb_by_size, NULL, 0, NULL);
    return mn;
}

int dna_manipulation_main(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    //testRestEnzParser();
    fasta_utils_main(0, NULL);

    add_plot_treat_menu_item ( "dna manipulation", NULL, dna_manipulation_plot_menu(), 0, NULL);
    return D_O_K;
}

int dna_manipulation_unload(int argc, char **argv)
{
    (void) argc;
    (void) argv;

    remove_item_to_menu(plot_treat_menu, "dna manipulation", NULL, NULL);
    return D_O_K;
}
