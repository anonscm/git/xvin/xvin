#ifndef _EVOLIVE_H_
#define _EVOLIVE_H_

PXV_FUNC(int, do_evolive_new, (void));
PXV_FUNC(int, do_evolive_slice, (void));
PXV_FUNC(MENU*, evolive_image_menu, (void));
PXV_FUNC(int, evolive_main, (int argc, char **argv));
#endif

