/*
*    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _EVOLIVE_C_
#define _EVOLIVE_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 

# include "../nrutil/nrutil.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "evolive.h"


# define EMPTY 0
# define STATEA 64
# define STATEB 128
# define STATEC 255

unsigned char imv[] = {EMPTY, STATEA, STATEB, STATEC};

int   do_evolive_new(void)
{
	register int i, j;
	static int onx = 512, ony = 512, live_n = 0;
	O_i *oi = NULL;
	union pix  *ps;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine perform a life evolution");
	}
	i = win_scanf("Live evolution game\n"
		      "enter the size of the game\n"
		      "in X %5d and Y %5d\n",&onx,&ony);
	if (i == CANCEL) 	return D_O_K;

	// we create the image
	oi =  create_one_image(onx, ony, IS_CHAR_IMAGE);
	set_im_title(oi, "Life game");
	if (oi == NULL)	win_printf_OK("Can't create dest image");

	// we find the image region or we create it
	if ((i = ac_grep(cur_ac_reg,"%im",&imr)) != 1)
	    imr = create_and_register_new_image_project( 0,   32,  900,  668);
	if (imr == NULL)	
	    win_printf_OK("could not create image region !");		
	srand(15);

	add_image (imr, oi->im.data_type, (void *)oi);
	if (i == 0)  remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
	select_image_of_imreg(imr, imr->n_oi -1);


	// write initial condition
	for (i = 0, ps = oi->im.pixel; i < ony ; i++)
	  {
	    for (j=0; j< onx; j++)
	      {
		if ((i < onx/2) && (j < ony/2))	          ps[i].ch[j] = EMPTY;
		else if ((i >= onx/2) && (j < ony/2))	  ps[i].ch[j] = STATEA;
		else if ((i < onx/2) && (j >= ony/2))	  ps[i].ch[j] = STATEB;
		else if ((i >= onx/2) && (j >= ony/2))	  ps[i].ch[j] = STATEC;
	      }
	  }
	set_oi_source(oi, "Live game %d", live_n++);
	set_zmin_zmax_values(oi, 0, 255);
	set_z_black_z_white_values(oi, 0, 255);
	//find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}



int   do_evolive_slice(void)
{
	register int i, j;
	int is, id, onx, ony;
	static int nd = 512, ns = 512;
	O_i *oi = NULL;
	union pix  *ps;
	long idum = 45;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine perform a life evolution");
	}
	// we retrieve the image
	if ((i = ac_grep(cur_ac_reg,"%im%oi",&imr,&oi)) != 2)
	  win_printf_OK("Can't find image");
	i = win_scanf("Live evolution game\n"
		      "Enter the nb of step between to display refresh\n"
		      "%6d and the number of time to go %6d\n"
		      "random seed",&nd,&ns,&idum);
	if (i == CANCEL) 	return D_O_K;
	// we retrieve the image size
	onx = oi->im.nx;
	ony = oi->im.ny;

	for (is = 0, ps = oi->im.pixel; is < ns ; is++)
	  {
	    for (id = 0; id < nd; id++)
	      {
		i = (int)(ony*ran1(&idum));
		//i = rand()%ony;
		i = (i < ony) ? i : ony - 1;
		j = (int)(onx*ran1(&idum));
		//j = rand()%onx;
		j = (j < onx) ? j : onx - 1;
		//switch a random pixel to one state at random
		ps[i].ch[j] = imv[rand()%4];
	      }
	    oi->need_to_refresh |= BITMAP_NEED_REFRESH;			
	    refresh_image(imr, imr->n_oi - 1);
	    display_title_message("step %d",is);
	  }
	return (refresh_image(imr, imr->n_oi - 1));
}

MENU *evolive_image_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"new live", do_evolive_new,NULL,0,NULL);
	add_item_to_menu(mn,"live slice", do_evolive_slice,NULL,0,NULL);
	return mn;
}

int	evolive_main(int argc, char **argv)
{
	add_image_treat_menu_item ( "evolive", NULL, evolive_image_menu(), 0, NULL);
	//do_evolive_new();
	return D_O_K;
}

int	evolive_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "evolive", NULL, NULL);
	return D_O_K;
}
#endif









