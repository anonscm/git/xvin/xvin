#include "menu.hh"
#include "../fingerprint/sequence/pos-fingerprint.hh"
#include "../fingerprint/fingerprint-io.hh"
#include "oligoseq.hh"
#include <fstream>


OligoSeq *oligoSeq;

int do_load_references(void)
{
    file_list_t *files = 0;

    if (updating_menu_state != 0)
    {
        active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Load a fingerprint reference file");
    }


    if (!(files = open_files_config("Open fingerprint reference File", NULL,
                             "Simple Fingerprint\0*.sfgp\0\0", "SFGP-FILE", "last_loaded")))
    {
        return OFF;
    }

    while (files != NULL)
    {
        const char *file = files->data;
        loadReference(file);
        files = files->next;
    }

    free_file_list(files);
    do_update_menu();

    return D_O_K;
}

int loadReference(const char *filepath)
{
    PosFingerprint *fgp = NULL;
    std::ifstream ifs;

    ifs.open(filepath, std::ifstream::in);
    if (ifs.good())
    {
        fgp = loadSimplePosFingerprint(ifs);
        oligoSeq->addReference(fgp);

    }
    ifs.close();

    return 0;
}

MENU *oligoseq_plot_menu(void)

{

}
int oligoseq_main(int argc, char **argv)
{

    oligoSeq = new OligoSeq();
    //fasta_utils_main(argc, argv);
    add_plot_treat_menu_item("Sequencing Oligos", NULL, oligoseq_plot_menu(), 0, NULL);

    return D_O_K;
}
