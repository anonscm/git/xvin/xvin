#pragma once
#include <vector>
#include "../fingerprint/sequence/pos-fingerprint.hh"

template<typename T> using RatioMatrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;


class OligoSeq
{
  public:
    OligoSeq();
    virtual ~OligoSeq();

    void addReference(PosFingerprint *fgp);


  private:
    std::vector<PosFingerprint *> references_;
    std::vector<RatioMatrix<float> *> referenceRatios_;


    bool generateRefRatios(void);
    RatioMatrix<float> *generateRatio(const PosFingerprint& fgp);
};
