#include "oligoseq.hh"


OligoSeq::OligoSeq()
{
}

OligoSeq::~OligoSeq()
{
}

void OligoSeq::addReference(PosFingerprint *fgp)
{
    if (fgp)
    {
        references_.push_back(fgp);
    }
}

bool OligoSeq::generateRefRatios()
{
    referenceRatios_.clear();

    for (uint i = 0; i < references_.size(); ++i)
    {
        RatioMatrix<float> *curRatioMatrix = generateRatio(*(references_[i]));
        referenceRatios_.push_back(curRatioMatrix);
    }

    return 0;
}

RatioMatrix<float> *OligoSeq::generateRatio(const PosFingerprint& fgp)
{
    const PositionVector& refPos = fgp.positions();
    RatioMatrix<float> *curRatioMatrix = new RatioMatrix<float>(refPos.size(), refPos.size());

    for (uint i = 0; i < refPos.size(); ++i)
    {
        int curHybRef = refPos[i];

        for (uint j = 0; j < refPos.size(); ++j)
        {
            int curHyb = refPos[j];
            (*curRatioMatrix)(i, j) = curHyb / curHybRef;
        }
    }

    return curRatioMatrix;
}
