#pragma once
#include "xvin.h"

PXV_FUNC(int, do_load_references, (void));
PXV_FUNC(int, oligoseq_main, (int argc, char **argv));
PXV_FUNC(MENU *,oligoseq_plot_menu,(void));


// NOT exported
int loadReference(const char *filepath);
