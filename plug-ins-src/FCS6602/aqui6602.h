# ifndef AQUI6602_H
# define AQUI6602_H



#define IS_CONTING_DATA		563412

#define COUNTING_PAGE_SIZE	4096


/* en fait, ce serait mieux de l'accrocher a une plotregion. */
/* mais celles-ci n'ont pas de hook !*/

typedef struct _conting_data
{
  int	n_data;		          /* number of data points	*/
  int	conting_freq;	          /* the freq of the counting clock	*/
  int	page_size;	          /* the number of points in one page	*/
  int	n_page, m_page, i_page;	  /* the number of page  */
  char	*file;	                  /* name of the file	*/
  char	*path;	                  /* name of the path	*/
  unsigned long	**page;		  /* names of data sets */
  float	**page_t0;		  /* the starting time of pages in seconds */
  unsigned long time;	          /* date of creation */
} conting_data;


PXV_FUNC(conting_data*,	find_conting_data_in_ds, (d_s* ds));
PXV_FUNC(conting_data*,	create_conting_data, (int page_size));
PXV_FUNC(void,		free_conting_data, (conting_data *c_d));




PXV_FUNC(int, detect_NI_boards,(void));
PXV_FUNC(int, init_boards_6602,(void));
PXV_FUNC(int, aquisition_one_APD,(void));
PXV_FUNC(int, plot_histogram_from_data,(void));
PXV_FUNC(int, aquisition_two_APD,(void));
PXV_FUNC(int, calibration_two_APD,(void));
PXV_FUNC(int, translate_ds_6602_in_real_time,(d_s *ds,d_s *ds_real_time));
PXV_FUNC(int, do_hist_from_ds_ct_bin_size,(d_s *ds,gsl_histogram **hist,float bin_size));
PXV_FUNC(int, histogram_to_ds,(gsl_histogram *hist, d_s *ds, int nf));
PXV_FUNC(int, trigger_generation,(int counter_number, clock_t counting_time));
XV_FUNC(int, draw_big_bubble, (BITMAP* b, int mode, int x, int y, char *format, ...));

XV_ARRAY(MENU,  project_menu);

#endif
