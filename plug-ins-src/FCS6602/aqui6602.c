#ifndef APD6602_C
#define APD6602_C

# include "allegro.h"
# include "winalleg.h"

# include "xvin.h"
#include "../Nidaq/nidaqcns.h"
#include "../Nidaq/nidaq.h"
#include "../stat/stat.h"
#include "../New_nidaq/daq6602.h"

/*this is for draw big bubble, pas propre!!*/

//# include "plot_reg_gui.h"
# include "box2alfn.h"
//# include "op2box.h"
//# include "imr_reg_gui.h"
//# include "plot2box.h"
# include <alfont.h>
//# include "im_mn.h"


#define IS_TIME_BASE	80
#define UP			 	1
#define DOWN			0

#ifndef AQUI6602_H
#include "aqui6602.h"
#endif

/* a modifier quand les exportations de variables marcheront mieux*/
 int NI_6602 = 0;
 int NI_6503 = 0;
 int status = 0;
 long unsigned int *  buffer;
#define PCI_6503   256
#define PCI_6602   232 




conting_data    *find_conting_data(pltreg *ds) 
{
    if (!ds) return NULL;
    if ( ds->use.to == IS_CONTING_DATA )
            return (conting_data *)ds->use.stuff;
    return NULL;
}


 
int    draw_big_bubble(BITMAP* b, int mode, int x, int y, char *format, ...)

{
    int prev;
    static char *c;

    va_list ap;

    

    if (c == NULL)    c = (char *)calloc(2048,sizeof(char));

    if (c == NULL)                return 1;

    va_start(ap, format);

    vsprintf(c,format,ap);

    va_end(ap);        

    if (user_font == NULL)        ttf_init();
    prev = alfont_text_mode(makecol(0, 0, 0));
    alfont_set_font_size(user_font, 80);

    y -= alfont_text_height(user_font)/2;
/*    b = uconvert(mes, U_ASCII, buf, U_UTF8, 256);
*/
  /* output in the middle of the screen, upper part, without antialiasing */

    acquire_bitmap(screen);
    if (mode == B_RIGHT)
          alfont_textout_right(b, user_font, c, x, y, makecol(255, 255, 0));

    else if (mode == B_LEFT)
          alfont_textout(b, user_font, c, x, y, makecol(255, 255, 0));
    
    else       alfont_textout_centre(b, user_font, c, x, y, makecol(255, 255, 0));

    release_bitmap(screen);    
    alfont_text_mode(prev);
    return 0;
}

int detect_NI_boards(void)
{	
   	short int infovalue;
	int i;
	
 	if(updating_menu_state != 0)	return D_O_K;	
	
	for (i=1;i<3;i++)
	{
		status = Init_DA_Brds (i,&infovalue);
		if (infovalue == PCI_6503)
  		{
    	 	NI_6503=i;
//    	 	win_printf(" Board %d is DAQ6503",i);
 	 	}
 	 	else if (infovalue == PCI_6602)
  		{
    	 	NI_6602=i;
//    	 	win_printf(" Board %d is Counter 6602",i);
 	 	}
 	 	else win_printf("No board detected");
 
 	}
 	return 0;

}




int init_boards_6602(void)
{	
	short int board;
 	
 	status = Init_DA_Brds (NI_6602,&board);
// 	if (board == PCI_6602)
//	win_printf("Initialised Board 6602");
	
	return 0;	
}

# define VCND_BUFFER_SIZE 4131072
 
int aquisition_one_APD(void)
{		
		
  int i,j,  temp=1, previous_data_counted=0;
  int numPoints = 65636;
  unsigned long numPts_read = 0;
  int numPts_to_read = 32768;
  static clock_t counting_time = 1000;
  clock_t start=0;
  int data_counted=0;
  //t data_to_count = 131072;
  unsigned long read_buffer[65636],buffer[65636];
		
  O_p *op=NULL;
  d_s *ds_counts=NULL;
  pltreg *pr = NULL;
  int counter_number = 1;
  static int source_of_counter = 2;
  int time_base;
		
  if(updating_menu_state != 0)	return D_O_K;	
		
		
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)  return win_printf_OK("cannot find data!");
		
	    
	    
  i = win_scanf("How long do you want to count (in ms)%d"
		"Your APD is connected to COUNTER GATE(!) number?\n%d",&counting_time,&counter_number);
  if (i == CANCEL) return D_O_K;


  op = create_and_attach_one_plot(pr,VCND_BUFFER_SIZE,VCND_BUFFER_SIZE, 0);
  if (op == NULL) return win_printf_OK("cannot create plot!");
  ds_counts = op->dat[0];

  
  switch (counter_number)
    {
    case 0 : counter_number = ND_COUNTER_0;
      break; 
    case 1 : counter_number = ND_COUNTER_1;
      break; 
    case 2 : counter_number = ND_COUNTER_2;
      break; 
    case 3 : counter_number = ND_COUNTER_3;
      break; 
    case 4 : counter_number = ND_COUNTER_4;
      break; 
    case 5 : counter_number = ND_COUNTER_5;
      break; 
    case 6 : counter_number = ND_COUNTER_6;
      break; 
    case 7 : counter_number = ND_COUNTER_7;
      break; 
    default : return win_printf("Unknown counter");
    }
				
  win_scanf("Your time base (source here) is : \n"
	    "Laser pulse (76MHz) %R Internal 80MHz %r\n"
	    "Be sure your choice is compatible with your connections",&source_of_counter);
		
  switch (source_of_counter) 
    {
    case 0 : time_base =  76000000;
      break;
			
    case 1: time_base =  80000000;
      break; 
    default : win_printf("We lack a base time to know the clock");
    }
		
  ds_counts->use.to = IS_TIME_BASE;
  ds_counts->use.stuff = (void *) &time_base;
    
  /* Reset the counter*/
  //win_printf (" counter number %d \n NI_6602 = %d",counter_number,NI_6602);
  
  status = GPCTR_Control (NI_6602, counter_number, ND_RESET);
  if (status != 0) win_printf("status = %d",status);
  /* Set application->period measurement*/
  status = GPCTR_Set_Application (NI_6602, counter_number, ND_BUFFERED_PERIOD_MSR );
  if (status != 0) win_printf("status = %d",status);
  if (source_of_counter == 0)
      status = GPCTR_Change_Parameter (NI_6602, counter_number, ND_SOURCE, ND_DEFAULT_PFI_LINE);
  else if (source_of_counter ==1)
      status = GPCTR_Change_Parameter (NI_6602, counter_number, ND_SOURCE, ND_INTERNAL_MAX_TIMEBASE);
		
  /* The gate is the APD signal*/
  status = GPCTR_Change_Parameter (NI_6602, counter_number, ND_GATE, ND_DEFAULT_PFI_LINE/*ND_PFI_34 for counter 1*/);
		
  /*We set a continuous acquisition*/
  status = GPCTR_Change_Parameter (NI_6602, counter_number, ND_BUFFER_MODE, ND_CONTINUOUS);
		
  /* So we need to configure the buffer*/
		
  status = GPCTR_Config_Buffer (NI_6602, counter_number, 0, numPoints, buffer);
		
  //config_buff(counter_number,numPoints);
  		
  /* We prepare the counter*/
  		
  status = GPCTR_Control (NI_6602, counter_number, ND_PREPARE);
		
		
		
  /* With ARM counting starts*/
  status = GPCTR_Control (NI_6602, counter_number, ND_ARM);
  		
  draw_bubble(screen,0,800,350,"We are counting");
  /*start=clock();
    while ( (clock()-start) < 100);
    status = GPCTR_Watch (NI_6602, ND_COUNTER_0, ND_AVAILABLE_POINTS  , &param);
    win_printf("Available points = %d",param);*/
  start=clock(); 
  ds_counts->nx = 0;
  while ( (clock()-start) < counting_time) /*& (keypressed != TRUE)*/
    {
      status = GPCTR_Read_Buffer (NI_6602, counter_number, ND_READ_MARK, 0/*offset*/,
				  numPts_to_read,  0, &numPts_read, read_buffer);
			
      //memcpy(ds_counts->xd,data_counted,
			
      if (status != 0) draw_bubble(screen,0,800,150,"time %u, status = %d",clock()-start,status);
      //if (status == 10800) draw_bubble(screen,0,800,150,"time out error. does this matter?");
      draw_bubble(screen,0,800,250,"data read %d",numPts_read);
      data_counted+=numPts_read;
      /* We have to check that ds has enough memory*/ 	
      if (ds_counts->mx <= (ds_counts->nx+numPts_to_read))// && ds_counts->nx < 4194304) 
	{
	  ds_counts = build_adjust_data_set(ds_counts, 2*ds_counts->mx, 2*ds_counts->mx);
	  if (ds_counts == NULL) win_printf("Null realloc"); 
	  draw_big_bubble(screen,0,400,350,"data to count %d",ds_counts->mx);
	}
            				
			
      for (j = 0 ; j < numPts_read; j++)
	{ 		
	  ds_counts->xd[ds_counts->nx] = ds_counts->nx;
	  ds_counts->yd[ds_counts->nx++] = read_buffer[j];
	}
            
      if ((clock()-start)%200 < 10 && temp)
	{
	  temp = 0;
	  draw_bubble(screen,0,800,200,"data_counted %d",ds_counts->nx);
	  draw_big_bubble(screen,0,400,150,"APD1 %d Hz", (ds_counts->nx-previous_data_counted)*5);
	  previous_data_counted=ds_counts->nx;
	}
 
      if ((clock()-start)%200 > 50) temp=1;
            
    }
  /* We stop the counter since aquisition time is over*/
  status = GPCTR_Control (NI_6602, counter_number, ND_DISARM);
  /*
    op->ay = 0.;
    op->dy = (source_of_counter == 1) ? 1/76000000 : 1/80000000;
  */
            
  draw_bubble(screen,0,800,350,"data read %d",data_counted);
         	 
  set_plot_x_title(op, "Photon");	
  set_plot_y_title(op, "Count interval");
  set_ds_plain_line(ds_counts);
    	    
  broadcast_dialog_message(MSG_DRAW,0);
    	    

  return 0;


} 

# define BIGSIZE 65636
# define BIGSIZE_2 (BIGSIZE>>1)
 
int aquisition_one_APD_vc(void)
{		
  int i,j,k, previous_data_counted=0;
  int numPoints = BIGSIZE;
  unsigned long numPts_read = 0;
  int numPts_to_read = BIGSIZE_2;
  static clock_t counting_time = 1000;
  clock_t start=0;
  int data_counted=0, n_ulbuf = 0;
  static int display = 1;
  //t data_to_count = 131072;
  unsigned long buffer[BIGSIZE], read_buffer[65636], *ulbuf[4096];
 
  O_p *op=NULL;
  d_s *ds_counts=NULL;
  pltreg *pr = NULL;
  static int counter_numb = 1;
  static int source_of_counter = 1;
  int time_base, counter_number;
  double tmpd;
		
  if(updating_menu_state != 0)	return D_O_K;	
		
		
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)  return win_printf_OK("cannot find data!");
		
	    
	    
  i = win_scanf("How long do you want to count (in ms)%d"
		"Your APD is connected to COUNTER GATE(!) number?\n%d",&counting_time,&counter_numb);
  if (i == CANCEL) return D_O_K;

  
  switch (counter_numb)
    {
    case 0 : counter_number = ND_COUNTER_0;
      break; 
    case 1 : counter_number = ND_COUNTER_1;
      break; 
    case 2 : counter_number = ND_COUNTER_2;
      break; 
    case 3 : counter_number = ND_COUNTER_3;
      break; 
    case 4 : counter_number = ND_COUNTER_4;
      break; 
    case 5 : counter_number = ND_COUNTER_5;
      break; 
    case 6 : counter_number = ND_COUNTER_6;
      break; 
    case 7 : counter_number = ND_COUNTER_7;
      break; 
    default : return win_printf("Unknown counter");
    }
				
  win_scanf("Your time base (source here) is : \n"
	    "Laser pulse (76MHz) %R Internal 80MHz %r\n"
	    "Be sure your choice is compatible with your connections\n"
	    "Real time display no %R yes %r",&source_of_counter, &display);
		
  switch (source_of_counter) 
    {
    case 0 : time_base =  76000000;
      break;
			
    case 1: time_base =  80000000;
      break; 
    default : win_printf("We lack a base time to know the clock");
    }
		
    
  /* Reset the counter*/
  //win_printf (" counter number %d \n NI_6602 = %d",counter_number,NI_6602);
  
  status = GPCTR_Control (NI_6602, counter_number, ND_RESET);
  if (status != 0) win_printf("status = %d",status);
  /* Set application->period measurement*/
  status = GPCTR_Set_Application (NI_6602, counter_number, ND_BUFFERED_PERIOD_MSR );
  if (status != 0) win_printf("status = %d",status);
  if (source_of_counter == 0)
      status = GPCTR_Change_Parameter (NI_6602, counter_number, ND_SOURCE, ND_DEFAULT_PFI_LINE);
  else if (source_of_counter ==1)
      status = GPCTR_Change_Parameter (NI_6602, counter_number, ND_SOURCE, ND_INTERNAL_MAX_TIMEBASE);
		
  /* The gate is the APD signal*/
  status = GPCTR_Change_Parameter (NI_6602, counter_number, ND_GATE, ND_DEFAULT_PFI_LINE/*ND_PFI_34 for counter 1*/);
		
  /*We set a continuous acquisition*/
  status = GPCTR_Change_Parameter (NI_6602, counter_number, ND_BUFFER_MODE, ND_CONTINUOUS);
		
  /* So we need to configure the buffer*/
		
  status = GPCTR_Config_Buffer (NI_6602, counter_number, 0, numPoints, buffer);
		
  //config_buff(counter_number,numPoints);
  		
  
  ulbuf[n_ulbuf] = (unsigned long*)calloc(BIGSIZE_2,sizeof(unsigned long));
  if (ulbuf[n_ulbuf] != NULL) n_ulbuf++;
  
  /* We prepare the counter*/		
  status = GPCTR_Control (NI_6602, counter_number, ND_PREPARE);
		
		
		
  /* With ARM counting starts*/
  status = GPCTR_Control (NI_6602, counter_number, ND_ARM);
  		
  if (display) draw_bubble(screen,0,800,350,"We are counting");
  /*start=clock();
    while ( (clock()-start) < 100);
    status = GPCTR_Watch (NI_6602, ND_COUNTER_0, ND_AVAILABLE_POINTS  , &param);
    win_printf("Available points = %d",param);*/
  start=clock(); 
  data_counted = i = j = 0;
  while ( (clock()-start) < counting_time) /*& (keypressed != TRUE)*/
    {
      status = GPCTR_Read_Buffer (NI_6602, counter_number, ND_READ_MARK, 0/*offset*/,
				  numPts_to_read,  0, &numPts_read, read_buffer);
      for(k = 0 ; k <numPts_read ; k++, data_counted++)
	{
	  i = data_counted/BIGSIZE_2;
	  if (i >= n_ulbuf) 
	    {
	      ulbuf[n_ulbuf] = (unsigned long*)calloc(BIGSIZE_2,sizeof(unsigned long));
	      if (ulbuf[n_ulbuf] != NULL && n_ulbuf < 4095) n_ulbuf++;		
	      if (display) draw_bubble(screen,0,800,500,"page flipped %d",n_ulbuf);
	    }
	  ulbuf[i][data_counted%BIGSIZE_2] = read_buffer[k];
	}
      if (status != 0) draw_bubble(screen,0,800,150,"time %u, status = %d",clock()-start,status);
      //if (status == 10800) draw_bubble(screen,0,800,150,"time out error. does this matter?");
      if (display) draw_bubble(screen,0,800,250,"data read %d",numPts_read);
      /* We have to check that ds has enough memory*/ 	
            
      if ((clock()-start) > j*200)
	{
	  j++;
	  if (display) draw_bubble(screen,0,800,200,"data_counted %d",data_counted);
	  if (display) draw_big_bubble(screen,0,400,150,"APD1 %d Hz", (data_counted-previous_data_counted)*5);
	  previous_data_counted=data_counted;
	}
 
    }
  /* We stop the counter since aquisition time is over*/
  status = GPCTR_Control (NI_6602, counter_number, ND_DISARM);


  //win_printf("Hello");

  op = create_and_attach_one_plot(pr,data_counted-1,data_counted-1, 0);
  if (op == NULL) return win_printf_OK("cannot create plot!");
  ds_counts = op->dat[0];
  
  for (j = 1, tmpd = 0; j < data_counted; j++)
    {// we skip the first point 
      ds_counts->yd[j-1] = ulbuf[j/BIGSIZE_2][j%BIGSIZE_2];
      if (j < 10) win_printf("pt %d %g page %d index %d",j,ds_counts->yd[j-1],j/BIGSIZE_2,j%BIGSIZE_2);
      tmpd += ds_counts->yd[j-1];
      ds_counts->xd[j-1] = tmpd/time_base;
    }

  for (j = 0; j < n_ulbuf; j++) free(ulbuf[j]);


  create_attach_select_x_un_to_op(op, IS_SECOND, 0, 1, 0, 0, "s");	
  create_attach_select_y_un_to_op(op, IS_SECOND, 0, ((float)1000000)/time_base, -6, 0, "\\mu s");
  ds_counts->use.to = IS_TIME_BASE;
  ds_counts->use.stuff = (void *) &time_base;
            
  draw_bubble(screen,0,800,350,"data read %d",data_counted);
         	 
  set_plot_x_title(op, "Photon");	
  set_plot_y_title(op, "Count interval");
  set_ds_plain_line(ds_counts);
    	    
  broadcast_dialog_message(MSG_DRAW,0);
    	    

  return 0;


} 

int plot_histogram_from_data(void)
{	O_p *op = NULL, *op_new = NULL;
	d_s *ds = NULL,*ds_real_time = NULL, *ds_histo = NULL;
	pltreg *pr = NULL;
	static float bin_size = 10000;
	gsl_histogram *hist = NULL;
	int n_histo = 1;

		if(updating_menu_state != 0)	return D_O_K;	
		
		
		if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)  return win_printf_OK("cannot find data!");
		
  		if (ds == NULL)   return (win_printf_OK("can't find data set"));
		
		if ((op_new = create_and_attach_one_plot(pr, ds->nx, ds->nx, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
		
  		ds_real_time = op_new->dat[0];
  		
  		
  		win_scanf("What is your bin size in microsecond : \n %f",&bin_size);
/*we obtain the times of arrival of each photon*/
  		translate_ds_6602_in_real_time(ds,ds_real_time);
/*we build a histogram for the intensity curve*/
        n_histo = do_hist_from_ds_ct_bin_size(ds_real_time,&hist,bin_size/1000000);
/*we attach the dataset*/  		
  		ds_histo = create_and_attach_one_ds(op_new,n_histo, n_histo,0);
/*??*/  		
    	histogram_to_ds(hist, ds_histo, n_histo); 

    	set_plot_title(op_new,"Photon rate");
    	set_plot_x_title(op_new, "Time (s)");
    	set_plot_y_title(op_new, "Photons");
	   	ds_histo->treatement = my_sprintf(ds_histo->treatement,"translation and histogram");
    
	    refresh_plot(pr, UNCHANGED);
	    return D_O_K;

} 


int translate_ds_6602_in_real_time(d_s *ds,d_s *ds_real_time)
{
	int i;
	
	if (ds->use.to == IS_TIME_BASE)
	{
    win_printf("I divided for the time base");
    	ds_real_time->xd[0] = ds->yd[0]/(int) &ds->use.stuff;
		ds_real_time->yd[0] = 1;

		for ( i = 1; i < ds->nx ; i++)
			{
					ds_real_time->xd[i] = (ds->yd[i]/(int) &ds->use.stuff + ds_real_time->xd[i-1]);
					ds_real_time->yd[i] = 1;
			}
	
			
	}
	else
	{
        win_printf("little trick");
        
        ds_real_time->xd[0] = ds->yd[0]/80000000;
	    ds_real_time->yd[0] = 1;
				
		for ( i = 1; i < ds->nx ; i++)
			{
					ds_real_time->xd[i] = (ds->yd[i]/80000000 + ds_real_time->xd[i-1]);/*little trick*/
					ds_real_time->yd[i] = 1;
//					if (i== (ds->nx)-1|| i<3) win_printf("i= %d \n x real time %f",i,ds_real_time->xd[i]);
			}
	
				
	}
//	win_printf("ds nx %d",ds->nx);
	return 0;
}

int do_hist_from_ds_ct_bin_size(d_s *ds,gsl_histogram **hist,float bin_size)
{
	register int i;
	long int number_of_bin=1;
	
	//d_s 	*ds2, *ds1;
	//int	index;
	double *ranges;
	
	
	float max = ds->xd[0];
	float min = ds->xd[0];
	
//	win_printf("bin_size = %f ds nx %d",bin_size,ds->nx);
	
 	for (i = 0; i < ds->nx ; i++)
 	{
 		max = (ds->xd[i] > max) ? ds->xd[i] : max;
  		min = (ds->xd[i] < min) ? ds->xd[i] : min;
	}
	
//	win_printf("number of bin = %d",number_of_bin);
	
//    win_printf("max= %f",max);
//	win_printf("min = %f",min);
	
    win_printf("div = %f",(max - min)/bin_size);
    
    //understand if either lrint or lround are better than simply casting on an int
    number_of_bin = (int) /*lroundf*/ ((max - min)/bin_size); 
    
	
    win_printf("number of bins = %d",number_of_bin);
    *hist = (gsl_histogram *)gsl_histogram_alloc(number_of_bin);
 
 	ranges = (double *)calloc(number_of_bin + 1/*bin_size*/,sizeof(double));
    
//    win_printf("no new number of bin = %d",number_of_bin);
    /*il problema e' che il'array dei ranges deve contenere un 
    elemento in piu' peche il num of bins!!! devo decidere se assegnare il max
    o se aggiungere un intervallo regolare*/
    
    for (i = 0; i <= number_of_bin ; i++)
 	{
 		ranges[i] = bin_size * i;
// 		if(i== number_of_bin) win_printf("%d ranges again= %lf",i,ranges[i]);

	}
	
//    win_printf("ranges= %lf",ranges);
	
	gsl_histogram_set_ranges(*hist, ranges, number_of_bin+1);

//	win_printf("here?");
	
	for (i=0; i < ds->nx; i++)
	{	
		gsl_histogram_increment(*hist, (double)ds->xd[i]);
		
	}
//	win_printf("maybe here number of bin = %d",number_of_bin);
	return number_of_bin;
}


int histogram_to_ds(gsl_histogram *hist, d_s *ds, int nf) // ds must already be allocated!!
{	register int j;
	double x1, x2, y;
	
	for (j = 0; j < nf; j++)
	{
		gsl_histogram_get_range( hist, j, &x1, &x2);
		y = gsl_histogram_get( hist, j);
		ds->yd[j] = (float)(y);
		ds->xd[j] = (float)( (x1+x2)/2 );
	}

	return(nf);
}







int trigger_generation(int counter_number, clock_t counting_time)
{
	int Status = 0;
  
  
    unsigned long int ulGpctrOutput = ND_GPCTR0_OUTPUT;/* on crache sur la sortie de 0 par defaut*/
    unsigned long int ulLOWcount = 50;/*we set 50*10�s at start level after the beginning*/
    unsigned long int ulHIGHcount = 100;/* 100*10�s working level pixel aquisition time*/
  

    ulHIGHcount = (unsigned long) 100*counting_time;
    
    
    
    switch (counter_number)
      	{
       		case 0 : ulGpctrOutput = ND_GPCTR0_OUTPUT;
         			 break; 
	        case 1 : ulGpctrOutput = ND_GPCTR1_OUTPUT;
         			 break; 
	        case 2 : ulGpctrOutput = ND_GPCTR2_OUTPUT;
         			 break; 
		    case 3 : ulGpctrOutput = ND_GPCTR3_OUTPUT;
         			 break; 
		    case 4 : ulGpctrOutput = ND_GPCTR4_OUTPUT;
         			 break; 
		    case 5 : ulGpctrOutput = ND_GPCTR5_OUTPUT;
         			 break; 
		    case 6 : ulGpctrOutput = ND_GPCTR6_OUTPUT;
         			 break; 
		    case 7 : ulGpctrOutput = ND_GPCTR7_OUTPUT;
         			 break; 
	        default : return win_printf("Unknown counter");
        }
    
    
	Status = GPCTR_Control(NI_6602, counter_number, ND_RESET);
    
    Status = GPCTR_Set_Application(NI_6602, counter_number,ND_SINGLE_PULSE_GNR);
    
    Status = GPCTR_Change_Parameter(NI_6602, counter_number,ND_SOURCE, ND_INTERNAL_100_KHZ);
    
    Status = GPCTR_Change_Parameter(NI_6602, counter_number,ND_COUNT_1, ulLOWcount);
    
    Status = GPCTR_Change_Parameter(NI_6602, counter_number,ND_COUNT_2, ulHIGHcount);
    
    Status = Select_Signal(NI_6602, ulGpctrOutput, ulGpctrOutput,ND_HIGH_TO_LOW);
       
    Status = GPCTR_Control(NI_6602, counter_number , ND_PROGRAM);
    /* we launched the pulse generation*/
    return 0;
}



int aquisition_two_APD(void)
{		
		int i_1,i_2,j_1,j_2;
		int numPoints = 32768;
		unsigned long numPts_read_1 = 0, numPts_read_2 = 0;
		int numPts_to_read = 32768;
		clock_t counting_time = 1000;
		clock_t start = 0;
		int data_counted_1 = 0,data_counted_2 = 0;
		int temp = 1, previous_data_counted_1=0,previous_data_counted_2=0;
		int data_to_count = 1048576;//131072;
		long unsigned int read_buffer_1[32768],read_buffer_2[32768];
		unsigned long buffer_1[32768],buffer_2[32768];
		
		O_p *op=NULL;
		d_s *ds_APD_1 = NULL, *ds_APD_2 = NULL, *ds = NULL;
    	pltreg *pr = NULL;
		int counter_number_1 = 1;
		int counter_number_2 = 2;
		int counter_number_trigger = 7;
		int source_of_counter = 2;
		
		
		if(updating_menu_state != 0)	return D_O_K;	
		
		
		if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)  return win_printf_OK("cannot find data!");
		
  		if (ds == NULL)   return (win_printf_OK("can't find data set"));
		
  		ds_APD_1 = create_and_attach_one_ds(op,data_to_count,data_to_count,IS_DATA_SET);
	    
	    ds_APD_2 = create_and_attach_one_ds(op,data_to_count,data_to_count,IS_DATA_SET);
	    
	    win_scanf("How long do you want to count (in ms)%d ",&counting_time);
  		
  		win_scanf("Your APD number 1 is connected to COUNTER GATE(!) number (0 to 6)?\n%d\n Your APD number 2 is connected to COUNTER GATE(!) number (0 to 6)?\n%d",&counter_number_1,&counter_number_2);
    
     	switch (counter_number_1)
      	{
       		case 0 : counter_number_1 = ND_COUNTER_0;
         			 break; 
	        case 1 : counter_number_1 = ND_COUNTER_1;
         			 break; 
	        case 2 : counter_number_1 = ND_COUNTER_2;
         			 break; 
		    case 3 : counter_number_1 = ND_COUNTER_3;
         			 break; 
		    case 4 : counter_number_1 = ND_COUNTER_4;
         			 break; 
		    case 5 : counter_number_1 = ND_COUNTER_5;
         			 break; 
		    case 6 : counter_number_1 = ND_COUNTER_6;
         			 break; 
		     
	        default : return win_printf("Unknown counter");
        }
				
		switch (counter_number_2)
      	{
       		case 0 : counter_number_2 = ND_COUNTER_0;
         			 break; 
	        case 1 : counter_number_2 = ND_COUNTER_1;
         			 break; 
	        case 2 : counter_number_2 = ND_COUNTER_2;
         			 break; 
		    case 3 : counter_number_2 = ND_COUNTER_3;
         			 break; 
		    case 4 : counter_number_2 = ND_COUNTER_4;
         			 break; 
		    case 5 : counter_number_2 = ND_COUNTER_5;
         			 break; 
		    case 6 : counter_number_2 = ND_COUNTER_6;
         			 break; 
	        default : return win_printf("Unknown counter");
        }
        
        if (counter_number_1 == counter_number_2) return win_printf("Error : 2 APDs on the same GATE!");
		
		win_scanf("Your time base (source here) is : \n (1) Laser pulse (76MHz) \n (2) Internal 80MHz \n %d \n Be sure your choice is compatible with your connections",&source_of_counter);
		
  		//ds_counts->use.to = IS_TIME_BASE;
  		//ds_counts->use.stuff = (void *) source_of_counter;
    
    
    	/* Reset the counter*/
	
  
		status = GPCTR_Control (NI_6602, counter_number_1, ND_RESET);
		
		status = GPCTR_Control (NI_6602, counter_number_2, ND_RESET);
		
		/* Set application->period measurement*/
		status = GPCTR_Set_Application (NI_6602, counter_number_1, ND_BUFFERED_PERIOD_MSR );
		status = GPCTR_Set_Application (NI_6602, counter_number_2, ND_BUFFERED_PERIOD_MSR );
		
		
  		if (source_of_counter == 1)
  		{
		  		status = GPCTR_Change_Parameter (NI_6602, counter_number_1, ND_SOURCE, ND_DEFAULT_PFI_LINE);
		  		status = GPCTR_Change_Parameter (NI_6602, counter_number_2, ND_SOURCE, ND_DEFAULT_PFI_LINE);
		}
		else if (source_of_counter == 2)
  		{
    		status = GPCTR_Change_Parameter (NI_6602, counter_number_1, ND_SOURCE, ND_INTERNAL_MAX_TIMEBASE);
    		status = GPCTR_Change_Parameter (NI_6602, counter_number_2, ND_SOURCE, ND_INTERNAL_MAX_TIMEBASE);
		}	
		
		
  		/* The gate is the APD signal*/
  		status = GPCTR_Change_Parameter (NI_6602, counter_number_1, ND_GATE, ND_DEFAULT_PFI_LINE);
  		draw_bubble(screen,0,400,100,"change par st 1= %d",status);
  		status = GPCTR_Change_Parameter (NI_6602, counter_number_2, ND_GATE, ND_DEFAULT_PFI_LINE);
  		draw_bubble(screen,0,600,100,"change par st 2= %d",status);
          		
		/*We set a continuous acquisition*/
		status = GPCTR_Change_Parameter (NI_6602, counter_number_1, ND_BUFFER_MODE, ND_CONTINUOUS);
  		draw_bubble(screen,0,400,120,"change par st 1= %d",status);
		status = GPCTR_Change_Parameter (NI_6602, counter_number_2, ND_BUFFER_MODE, ND_CONTINUOUS);
  		draw_bubble(screen,0,600,120,"change par st 2= %d",status);
          		
		/* So we need to configure the buffer*/
	
  		status = GPCTR_Config_Buffer (NI_6602, counter_number_1, 0, numPoints, buffer_1);
  		draw_bubble(screen,0,400,140,"config buffer 1= %d",status);
  		status = GPCTR_Config_Buffer (NI_6602, counter_number_2, 0, numPoints, buffer_2);
  		draw_bubble(screen,0,600,140,"config buffer 2= %d",status);
           		
  		/* We set the starting mode on TRIGGER*/
//  		status = GPCTR_Change_Parameter (NI_6602, counter_number_1, ND_START_TRIGGER, ND_ENABLED);
//  		status = GPCTR_Change_Parameter (NI_6602, counter_number_2, ND_START_TRIGGER, ND_ENABLED);
  		
  		/* We prepare the counter*/
  		
  		status = GPCTR_Control (NI_6602, counter_number_1, ND_PREPARE);
  		draw_bubble(screen,0,400,160,"prepare 1= %d",status);
		status = GPCTR_Control (NI_6602, counter_number_2, ND_PREPARE);
  		draw_bubble(screen,0,600,160,"prepare 2= %d",status);
		/* With ARM counting starts*/
		
		trigger_generation(counter_number_trigger,counting_time); //a envoyer sur le gate de l'APD pour bloquer le comptage
  		
  		status = GPCTR_Control (NI_6602, counter_number_1, ND_ARM);
  		draw_bubble(screen,0,400,180,"arm 1= %d",status);
  		status = GPCTR_Control (NI_6602, counter_number_2, ND_ARM);
  		draw_bubble(screen,0,600,180,"arm 2= %d",status);
  		
  		
    	draw_bubble(screen,0,800,350,"We are counting");
  		
   	    start=clock(); 
		
        while ( (clock()-start) < counting_time && (keypressed() != TRUE))
		
		{
			i_1 = data_counted_1;
   			i_2 = data_counted_2;
      			
			status = GPCTR_Read_Buffer (NI_6602, counter_number_1, ND_READ_MARK, 0,numPts_to_read,  0, &numPts_read_1, read_buffer_1);
			if (status != 0) draw_bubble(screen,0,800,150,"time %u, status c 1 = %d",clock()-start,status);
			status = GPCTR_Read_Buffer (NI_6602, counter_number_2, ND_READ_MARK, 0,numPts_to_read,  0, &numPts_read_2, read_buffer_2);
			if (status != 0) draw_bubble(screen,0,800,250,"time %u, status c 2 = %d",clock()-start,status);
			
   			data_counted_1+=numPts_read_1;
   			data_counted_2+=numPts_read_2;
   		   	
           		/* We have to check that ds has enough memory*/ 	
   			if ((data_to_count < (data_counted_1+numPts_to_read)) || (data_to_count < (data_counted_2+numPts_to_read)) )
      			{ 
      				ds_APD_1 = build_adjust_data_set(ds_APD_1, data_to_count*2, data_to_count*2);
      				ds_APD_2 = build_adjust_data_set(ds_APD_2, data_to_count*2, data_to_count*2);
      				data_to_count *= 2;
      				draw_big_bubble(screen,0,400,450,"data to count %d",data_to_count);
  				}
   			
            				
			for (j_1=0; (data_counted_1-i_1)>0;i_1++,j_1++)
   			{ 		
                  ds_APD_1->xd[i_1] = i_1;
                  ds_APD_1->yd[i_1] = read_buffer_1[j_1];
            }
            for (j_2=0; (data_counted_2-i_2)>0;i_2++,j_2++)
   			{ 		
                  ds_APD_2->xd[i_2] = i_2;
                  ds_APD_2->yd[i_2] = read_buffer_2[j_2];
            }
            if ((clock()-start)%200 < 30 && temp)
            {
            temp = 0;
		    draw_big_bubble(screen,0,400,250,"APD 1 %d Hz", (data_counted_1-previous_data_counted_1)*5);
            draw_big_bubble(screen,0,400,350,"APD 2 %d Hz", (data_counted_2-previous_data_counted_2)*5);
            previous_data_counted_1=data_counted_1;
            previous_data_counted_2=data_counted_2;
            }
  
          if ((clock()-start)%200 > 50 && !temp) temp=1;

  		}
  		/* We stop the counter since aquisition time is over*/
  		
  		status = GPCTR_Control (NI_6602, counter_number_1, ND_DISARM);
  		draw_bubble(screen,0,800,150,"status c 1 = %d",status);
  		
  		status = GPCTR_Control (NI_6602, counter_number_2, ND_DISARM);
  		draw_bubble(screen,0,800,250,"status c 2 = %d",status);

  			
  			op->ay = 0;
  			op->dy = (source_of_counter == 1) ? 1/76000000 : 1/80000000;
    
  	        draw_bubble(screen,0,800,350,"data read %d",data_counted_1);
         	 
    	    set_plot_x_title(op, "Time");	
    	    set_plot_y_title(op, "Count ");
    	    set_ds_plain_line(ds_APD_1);
    	    set_ds_plain_line(ds_APD_2);
         
         	broadcast_dialog_message(MSG_DRAW,0); /*this line should be re-enabled once the 1 sec gap is solved*/
    	    

		return 0;


}  



int calibration_two_APD(void)
{		
		int i_1,i_2;
		int numPoints = 32768;
		unsigned long numPts_read_1 = 0, numPts_read_2 = 0;
		int numPts_to_read = 32768;
		clock_t start = 0;
		int data_counted_1 = 0,data_counted_2 = 0;
		int temp = 1, previous_data_counted_1=0,previous_data_counted_2=0;
		long unsigned int read_buffer_1[32768],read_buffer_2[32768];
		unsigned long buffer_1[32768],buffer_2[32768];
	
    	int counter_number_1 = 1;
		int counter_number_2 = 2;
		int counter_number_trigger = 7;
		int source_of_counter = 2;
		
		
		if(updating_menu_state != 0)	return D_O_K;	
		
		win_scanf("Your APD number 1 is connected to COUNTER GATE(!) number (0 to 6)?\n%d\n Your APD number 2 is connected to COUNTER GATE(!) number (0 to 6)?\n%d",&counter_number_1,&counter_number_2);
    
     	switch (counter_number_1)
      	{
       		case 0 : counter_number_1 = ND_COUNTER_0;
         			 break; 
	        case 1 : counter_number_1 = ND_COUNTER_1;
         			 break; 
	        case 2 : counter_number_1 = ND_COUNTER_2;
         			 break; 
		    case 3 : counter_number_1 = ND_COUNTER_3;
         			 break; 
		    case 4 : counter_number_1 = ND_COUNTER_4;
         			 break; 
		    case 5 : counter_number_1 = ND_COUNTER_5;
         			 break; 
		    case 6 : counter_number_1 = ND_COUNTER_6;
         			 break; 
		     
	        default : return win_printf("Unknown counter");
        }
				
		switch (counter_number_2)
      	{
       		case 0 : counter_number_2 = ND_COUNTER_0;
         			 break; 
	        case 1 : counter_number_2 = ND_COUNTER_1;
         			 break; 
	        case 2 : counter_number_2 = ND_COUNTER_2;
         			 break; 
		    case 3 : counter_number_2 = ND_COUNTER_3;
         			 break; 
		    case 4 : counter_number_2 = ND_COUNTER_4;
         			 break; 
		    case 5 : counter_number_2 = ND_COUNTER_5;
         			 break; 
		    case 6 : counter_number_2 = ND_COUNTER_6;
         			 break; 
	        default : return win_printf("Unknown counter");
        }
        
        if (counter_number_1 == counter_number_2) return win_printf("Error : 2 APDs on the same GATE!");
		
		win_scanf("Your time base (source here) is : \n (1) Laser pulse (76MHz) \n (2) Internal 80MHz \n %d \n Be sure your choice is compatible with your connections",&source_of_counter);
		
  		//ds_counts->use.to = IS_TIME_BASE;
  		//ds_counts->use.stuff = (void *) source_of_counter;
    
    
    	/* Reset the counter*/
	
  
		status = GPCTR_Control (NI_6602, counter_number_1, ND_RESET);
		
		status = GPCTR_Control (NI_6602, counter_number_2, ND_RESET);
		
		/* Set application->period measurement*/
		status = GPCTR_Set_Application (NI_6602, counter_number_1, ND_BUFFERED_PERIOD_MSR );
		status = GPCTR_Set_Application (NI_6602, counter_number_2, ND_BUFFERED_PERIOD_MSR );
		
		
  		if (source_of_counter == 1)
  		{
		  		status = GPCTR_Change_Parameter (NI_6602, counter_number_1, ND_SOURCE, ND_DEFAULT_PFI_LINE);
		  		status = GPCTR_Change_Parameter (NI_6602, counter_number_2, ND_SOURCE, ND_DEFAULT_PFI_LINE);
		}
		else if (source_of_counter == 2)
  		{
    		status = GPCTR_Change_Parameter (NI_6602, counter_number_1, ND_SOURCE, ND_INTERNAL_MAX_TIMEBASE);
    		status = GPCTR_Change_Parameter (NI_6602, counter_number_2, ND_SOURCE, ND_INTERNAL_MAX_TIMEBASE);
		}	
		
		
  		/* The gate is the APD signal*/
  		status = GPCTR_Change_Parameter (NI_6602, counter_number_1, ND_GATE, ND_DEFAULT_PFI_LINE);
  		draw_bubble(screen,0,400,100,"change par st 1= %d",status);
  		status = GPCTR_Change_Parameter (NI_6602, counter_number_2, ND_GATE, ND_DEFAULT_PFI_LINE);
  		draw_bubble(screen,0,600,100,"change par st 2= %d",status);
          		
		/*We set a continuous acquisition*/
		status = GPCTR_Change_Parameter (NI_6602, counter_number_1, ND_BUFFER_MODE, ND_CONTINUOUS);
  		draw_bubble(screen,0,400,120,"change par st 1= %d",status);
		status = GPCTR_Change_Parameter (NI_6602, counter_number_2, ND_BUFFER_MODE, ND_CONTINUOUS);
  		draw_bubble(screen,0,600,120,"change par st 2= %d",status);
          		
		/* So we need to configure the buffer*/
	
  		status = GPCTR_Config_Buffer (NI_6602, counter_number_1, 0, numPoints, buffer_1);
  		draw_bubble(screen,0,400,140,"config buffer 1= %d",status);
  		status = GPCTR_Config_Buffer (NI_6602, counter_number_2, 0, numPoints, buffer_2);
  		draw_bubble(screen,0,600,140,"config buffer 2= %d",status);
           		
  		/* We set the starting mode on TRIGGER*/
//  		status = GPCTR_Change_Parameter (NI_6602, counter_number_1, ND_START_TRIGGER, ND_ENABLED);
//  		status = GPCTR_Change_Parameter (NI_6602, counter_number_2, ND_START_TRIGGER, ND_ENABLED);
  		
  		/* We prepare the counter*/
  		
  		status = GPCTR_Control (NI_6602, counter_number_1, ND_PREPARE);
  		draw_bubble(screen,0,400,160,"prepare 1= %d",status);
		status = GPCTR_Control (NI_6602, counter_number_2, ND_PREPARE);
  		draw_bubble(screen,0,600,160,"prepare 2= %d",status);
		/* With ARM counting starts*/
		
  		status = GPCTR_Control (NI_6602, counter_number_1, ND_ARM);
  		draw_bubble(screen,0,400,180,"arm 1= %d",status);
  		status = GPCTR_Control (NI_6602, counter_number_2, ND_ARM);
  		draw_bubble(screen,0,600,180,"arm 2= %d",status);
  		
  		
    	draw_bubble(screen,0,800,350,"We are counting");
  		
   	    start=clock(); 
		
        while (keypressed() != TRUE)
		
		{
			i_1 = data_counted_1;
   			i_2 = data_counted_2;
      			
			status = GPCTR_Read_Buffer (NI_6602, counter_number_1, ND_READ_MARK, 0,numPts_to_read,  0, &numPts_read_1, read_buffer_1);
			if (status != 0) draw_bubble(screen,0,800,150,"time %u, status c 1 = %d",clock()-start,status);
			status = GPCTR_Read_Buffer (NI_6602, counter_number_2, ND_READ_MARK, 0,numPts_to_read,  0, &numPts_read_2, read_buffer_2);
			if (status != 0) draw_bubble(screen,0,800,250,"time %u, status c 2 = %d",clock()-start,status);
			
   			data_counted_1+=numPts_read_1;
   			data_counted_2+=numPts_read_2;
   		   
            if ((clock()-start)%200 < 30 && temp)
            {
            temp = 0;
		    draw_big_bubble(screen,0,400,250,"APD 1 %d Hz", (data_counted_1-previous_data_counted_1)*5);
            draw_big_bubble(screen,0,400,350,"APD 2 %d Hz", (data_counted_2-previous_data_counted_2)*5);
            previous_data_counted_1=data_counted_1;
            previous_data_counted_2=data_counted_2;
            }
  
          if ((clock()-start)%200 > 50 && !temp) temp=1;

  		}
  		/* We stop the counter since aquisition time is over*/
  		
  		status = GPCTR_Control (NI_6602, counter_number_1, ND_DISARM);
  		draw_bubble(screen,0,800,150,"status c 1 = %d",status);
  		
  		status = GPCTR_Control (NI_6602, counter_number_2, ND_DISARM);
  		draw_bubble(screen,0,800,250,"status c 2 = %d",status);

  			
  	        draw_bubble(screen,0,800,350,"data read %d",data_counted_1);
         	 
         	broadcast_dialog_message(MSG_DRAW,0); /*this line should be re-enabled once the 1 sec gap is solved*/
    	    

		return 0;


}  


MENU AQUI6602_menu[32] =
{
   
  /* { "Init boards",             init_boards_6602, NULL,       0, NULL  },*/
  { "calibration two APDs",      calibration_two_APD, NULL,       0, NULL  },
  { "aquisition two APDs",      aquisition_two_APD, NULL,       0, NULL  },
   { "aquisition one APD",        aquisition_one_APD, NULL,       0, NULL  },
   { "aquisition one APD vc",        aquisition_one_APD_vc, NULL,       0, NULL  },
  //{ "continuous_buff_event",   continuous_buff_event, NULL,       0, NULL  },
  {"plot histogram from data",   plot_histogram_from_data,   NULL,     0, NULL },
     { "Detect NI boards",       	  detect_NI_boards,	NULL,       0, NULL  },
   
   { NULL,                                  NULL,             NULL,       0, NULL  }
};

int aqui6602_main(int argc, char **argv)
{
    detect_NI_boards();
    init_boards_6602();
    add_plot_edit_menu_item("Aquisition FCS", NULL, AQUI6602_menu, 0, NULL);
    
	broadcast_dialog_message(MSG_DRAW,0);
	


	return D_O_K;    
}

#endif

