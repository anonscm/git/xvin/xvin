/**
	\file diffusion.c
	\author V. Croquette slightly modified by A. Est�vez Torres
*   \brief Image FFT for diffusion analysis
 *	
 	Allows to FFT, derive and recenter a diffusion pattern in 
 	an image for analyze diffusion modes and extract them from FFT
 	image into a plot
 *    
  */
#ifndef _DIFFUSION_C_
#define _DIFFUSION_C_

# include "allegro.h"
# include "xvin.h"
# include "fftl32n.h"
# include "fillib.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "diffusion.h"


# define 	WRONG_ARGUMENT		-1
# define 	OUT_MEMORY			-2
# define 	FFT_NOT_SUPPORTED	-15
# define	PB_WITH_EXTREMUM	2
# define	TOO_MUCH_NOISE		4



int deplace(float *x, int nx)
{
	register int i, j;
	float tmp;
	
	for (i = 0, j = nx/2; j < nx ; i++, j++)
	{
		tmp = x[j];
		x[j] = x[i];
		x[i] = tmp;
	}
	return 0;
}
int fourier(float *x, int nx, int df)
{
	if ( df>0 )
	{
		realtr1(nx, x);
		fft(nx, x, 1);
		realtr2(nx, x, 1);
	}
	else if ( df<0 )
	{
		realtr2(nx, x, -1);
		fft(nx, x, -1);
		realtr1(nx, x);
	}
	return 0;
}

int translate_fft(float *x, int nx, float dx, float *xt)
{
	register int i,j;
	double phase, co, si, re, im;
	
	phase = -2*M_PI*dx/nx;
	for ( i=1 ; i<nx/2 ; i++)
	{
		j = 2*i;
		co = cos(phase*i);
		si = sin(phase*i);
		re = (double) x[j];
		im = (double) x[j+1];
		xt[j]   = (float)( co*re - si*im);
		xt[j+1] = (float)( co*im + si*re);
	}
	xt[0] = x[0];
	xt[1] = x[1];
	return 0;
}
int fftwindow_flat_top1(int npts, float *x, float smp)
{
	register int i, j;
	int n_2, n_4;
	float tmp;

	if ((j = fft_init(npts)) != 0)
		return j;

	n_2 = npts/2;
	n_4 = n_2/2;
	x[0] *= smp;
	smp += 1.0;
	for (i=1, j=n_4-2; j >=0; i++, j-=2)
	{
		tmp = (smp - fftsin[j]);
		x[i] *= tmp;
		x[npts-i] *= tmp;
	}
	for (i=n_4/2, j=0; i < n_4 ; i++, j+=2)
	{
		tmp = (smp + fftsin[j]);
		x[i] *= tmp;
		x[npts-i] *= tmp;
	}
	return 0;
}

int imaginary_fft(float *x, int nx, float *xt)
{
	register int i,j;
	float re, im;
	
	for ( i=1 ; i<nx/2 ; i++)
	{
		j = 2*i;
		re = x[j];
		im = x[j+1];
		xt[j]   = im;
		xt[j+1] = -re;
	}
	xt[0] = 0;  /* x[0] */
	xt[1] = 0;
	return 0;
}
int 	complex_and_fourier_backward(int nx, float *fx1)
{
	imaginary_fft(fx1,nx,fx1);
	/*	get back to real world */
	realtr2(nx, fx1, -1);
	fft(nx, fx1, -1);
	realtr1(nx, fx1);
	deplace(fx1, nx);
	return 0;
}	

int flatten(float *x, int nx)
{
	register int i, j;
	float ad, af, xd, xf;
	
	for (i = 0, j = (3*nx)/4, ad = af = 0; i < nx/4 ; i++, j++)
	{
		ad += x[i];
		af += x[j];
	}
	ad *= 4;
	af *= 4;
	ad /= nx;
	af /= nx;
	xd = (float)(nx)/8;
	xf = nx - xd;
	ad /= (xf - xd);
	af /= (xf - xd);	
	for (i = 0; i < nx ; i++)
		x[i] -= ad * (xf - i) + af * (i - xd) ;
	return 0;
}
int 	shift_and_fourier_backward(int nx, float *fx1, float offset)
{
	if (offset != 0)	translate_fft(fx1,nx,offset,fx1);
	/*	get back to real world */
	realtr2(nx, fx1, -1);
	fft(nx, fx1, -1);
	realtr1(nx, fx1);
	deplace(fx1, nx);
	return 0;
}	
int	find_max_by_phase(float *re, float *im, int nx, float *dx, float *corr, float *ft)
{
	register int i, nmax, j;
	float fmax = 0, f_2, f_1, f0, f1, f2,  amp = 0, tmp = 0.5, Re,Im;
	int ret = 0;
	
	for (i = 0, nmax = 0; i < nx; i++) 
	{
		amp = re[i] * re[i] + im[i] * im[i];
		if (amp > fmax) 
		{
			nmax = i;
			fmax = amp;
		}
	}
	
	
	f_2 = atan2(im[(nx + nmax - 2) % nx],re[(nx + nmax - 2) % nx]);	
	f_1 = atan2(im[(nx + nmax - 1) % nx],re[(nx + nmax - 1) % nx]);
	f0 = atan2(im[nmax],re[nmax]);
	f1 = atan2(im[(nmax + 1) % nx],re[(nmax + 1) % nx]);	
	f2 = atan2(im[(nmax + 2) % nx],re[(nmax + 2) % nx]);	
	if (f_2*f_1 <= 0)
	{
		if ((f_2-f_1) != 0) 		tmp =  f_2/(f_2-f_1);
		else	 ret = TOO_MUCH_NOISE;
		tmp -= 2;
	}	
	else if (f_1*f0 <= 0)
	{
		if ((f_1-f0) != 0) 	tmp = f_1/(f_1-f0);
		else	ret = TOO_MUCH_NOISE;
		tmp -= 1;
	}	
	else if (f1*f0 <= 0)
	{
		if ((f0-f1) != 0) 	tmp =  f0/(f0-f1);
		else	ret = TOO_MUCH_NOISE;
	}
	else if (f1*f2 <= 0)
	{
		if ((f1-f2) != 0) 	tmp = f2/(f2-f1);
		else			ret = TOO_MUCH_NOISE;
		tmp += 1;
	}		
	else ret = TOO_MUCH_NOISE;
	j = (nx + nmax - 1) % nx;
	Re = re[j];
	Im = im[j];
	amp = (Re*Re + Im*Im) * (tmp - 1) * tmp/2;
	Re = re[nmax];
	Im = im[nmax];	
	amp -= (Re*Re + Im*Im) * (tmp - 1) * (tmp + 1);
	j = (nx + nmax + 1) % nx;
	Re = re[j];
	Im = im[j];	
	amp += (Re*Re + Im*Im) * (tmp + 1) * tmp/2;
	amp = sqrt(amp);		
	tmp += nmax;	
	*dx = tmp;
	*corr = fmax;
	return ret;
}
/*	Find maximum position in a 1d array, the array is supposed periodic
 *	return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int	find_max1(float *x, int nx, float *Max_pos, float *Max_val)
{
	register int i;
	int  nmax, p, ret = 0, delta;
	double a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0;

	for (i = 0, nmax = 0; i < nx; i++) nmax = (x[i] > x[nmax]) ? i : nmax;
	f_2 = x[(nx + nmax - 2) % nx];
	f_1 = x[(nx + nmax - 1) % nx];
	f0 = x[nmax];
	f1 = x[(nmax + 1) % nx];
	f2 = x[(nmax + 2) % nx];
	if (f_1 < f1)
	{
		a = -f_1/6 + f0/2 - f1/2 + f2/6;
		b = f_1/2 - f0 + f1/2;
		c = -f_1/3 - f0/2 + f1 - f2/6;
		d = f0;
		delta = 0;
	}	
	else
	{
		a = b = c = 0;	
		a = -f_2/6 + f_1/2 - f0/2 + f1/6;
		b = f_2/2 - f_1 + f0/2;
		c = -f_2/3 - f_1/2 + f0 - f1/6;
		d = f_1;
		delta = -1;
	}
	if (a == 0)
	{
		xm = (b != 0) ? - c/(2*b) : (double)nmax;
	}
	else if ((b*b - 3*a*c) < 0)
	{
		ret |= PB_WITH_EXTREMUM;
		xm = (double)nmax;
	}
	else
		xm = (-b - sqrt(b*b - 3*a*c))/(3*a);
	for (p = -2; p < 1; p++)
	{
		if (6*a*p+b > 0) ret |= TOO_MUCH_NOISE;
	}	
	*Max_pos = (float)(xm + nmax + delta); 
	*Max_val = (a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d;
	return ret;
}
/*		correlate "x1" with its invert over "nx" points, returns the correlation
 *		in "fx1". "fx1" will be used to compute fft and result 
 *		setting them to NULL will allocate space. "window" will enable
 *		a windowing process if != 0 a double windowing if = to 2.
 */
int	correlate_1d_sig_and_invert(float *x1, int nx, int window, float *fx1, int filter, int remove_dc)
{
	register int i, j;
	float moy, smp = 0.05;
	float m1, re1, re2, im1, im2;
	
	if (x1 == NULL)							return WRONG_ARGUMENT;
	if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;
	if (fx1 == NULL)	fx1 = (float*)calloc(nx,sizeof(float));
	if (fx1 == NULL)						return OUT_MEMORY;

	/*	compute fft of x1 */
	for(i = 0; i < nx; i++)		fx1[i] = x1[i];
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += fx1[i] + fx1[j];	
	for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)	
		fx1[i] = x1[i] - moy;
	if (window)	fftwindow1(nx, fx1, smp);
	realtr1(nx, fx1);
	fft(nx, fx1, 1);
	realtr2(nx, fx1, 1);
	
	/*	compute normalization */
	for (i=0, m1 = 0; i< nx; i+=2)
		m1 += fx1[i] * fx1[i] + fx1[i+1] * fx1[i+1];
		
	/*	compute correlation in Fourier space */
	for (i=2; i< nx; i+=2)
	{
		re1 = fx1[i];		re2 = fx1[i];
		im1 = fx1[i+1];		im2 = -fx1[i+1];
		fx1[i] 		= (re1 * re2 + im1 * im2)/m1;
		fx1[i+1] 	= (im1 * re2 - re1 * im2)/m1;
	}
	fx1[0]	= 2*(fx1[0] * fx1[0])/m1;	/* these too mode are special */
	fx1[1] 	= 2*(fx1[1] * fx1[1])/m1;
	if (filter> 0)				lowpass_smooth_half (nx, fx1, filter);
	
	/*	get back to real world */
	realtr2(nx, fx1, -1);
	fft(nx, fx1, -1);
	realtr1(nx, fx1);
	deplace(fx1, nx);
	return 0;
}	

int	correlate_1d_sig_and_invert_in_fourier(float *x1, int nx, int window, float *fx1, int filter, int remove_dc)
{
	register int i;
	float  smp = 0.05;
	float m1, re1, re2, im1, im2;
	
	if (x1 == NULL)							return WRONG_ARGUMENT;
	if (fft_init(nx) || filter_init(nx))	return FFT_NOT_SUPPORTED;
	if (fx1 == NULL)	fx1 = (float*)calloc(nx,sizeof(float));
	if (fx1 == NULL)						return OUT_MEMORY;

	/*	compute fft of x1 */
	for(i = 0; i < nx; i++)		fx1[i] = x1[i];
/*	
	for(i = 0, j = (3*nx)/4, moy = 0; i < nx/4; i++, j++)		
		moy += fx1[i] + fx1[j];	
	for(i = 0, moy = (2*moy)/nx; remove_dc && i < nx; i++)	
		fx1[i] = x1[i] - moy;
*/
	flatten(fx1, nx);
	if (window == 1)		fftwindow1(nx, fx1, smp);
	else if (window == 2)	fftwindow_flat_top1(nx, fx1, smp);
	realtr1(nx, fx1);
	fft(nx, fx1, 1);
	realtr2(nx, fx1, 1);
	
	/*	compute normalization */
	for (i=0, m1 = 0; i< nx; i+=2)
		m1 += fx1[i] * fx1[i] + fx1[i+1] * fx1[i+1];
	m1 /= 2;	
		
	/*	compute correlation in Fourier space */
	for (i=2; i< nx; i+=2)
	{
		re1 = fx1[i];		re2 = fx1[i];
		im1 = fx1[i+1];		im2 = -fx1[i+1];
		fx1[i] 		= (re1 * re2 + im1 * im2)/m1;
		fx1[i+1] 	= (im1 * re2 - re1 * im2)/m1;
	}
	fx1[0]	= 2*(fx1[0] * fx1[0])/m1;	/* these too mode are special */
	fx1[1] 	= 2*(fx1[1] * fx1[1])/m1;
	if (filter> 0)				lowpass_smooth_half (nx, fx1, filter);
	return 0;
}	

float	find_distance_from_center_by_phase(float *x1, int cl, int wflag, int filter, int black_circle, float *corr)
{
	static float fx1[256], fr[256], fi[256];
	int i = 0;
	float  dx; 
	correlate_1d_sig_and_invert_in_fourier(x1, cl, wflag, fx1, filter, black_circle);
	fx1[0] = fx1[1] = 0;
	for (i = 0; i < cl; i++)	fi[i] = fr[i] = fx1[i];
	shift_and_fourier_backward(cl, fr, 0);
	complex_and_fourier_backward(cl, fi);
	find_max_by_phase(fr,fi,cl,&dx,corr,fx1);
	
	dx -= cl/2;
	dx /= 2;
	return dx;	
}
float	find_distance_from_center(float *x1, int cl, int flag, int filter, int black_circle, float *corr)
{
	static float fx1[256];
	float dx;
	
	correlate_1d_sig_and_invert(x1, cl, flag, fx1, filter, !black_circle);
	find_max1(fx1, cl, &dx,corr);
	dx -= cl/2;
	dx /=2;
	return dx;
}

	/*	compute the forward fft of a real image
	*	-dest is the result, if dest == NULL then it is created
	*	-ois is the source image
	*	-title is the ptr. to the title button to print message
	*		if NULL, no message print during process
	*	-smp is a small number tipically .05 in the Hanning window
	*		if smp = 0 the reverse operation is impossible
	*/
O_i 	*forward_fft_1d_im(O_i *dest, O_i *ois, float smp)
{
	register  int j;
	int nout = 0, onx, ony, no;
	char c[128];
	union pix *pd;
	O_i  *oid;

	if (ois == NULL || ois->im.pixel == NULL)
	{
		win_printf("No image found");	
		return (NULL);	
	}
	ony = ois->im.ny;	onx = ois->im.nx;	
	if (ois->im.data_type == IS_COMPLEX_IMAGE)
	{
		win_printf ("Cannot treat a complex image yet!");
		return(NULL);
	}
	if (dest != NULL)		/*	if dest exists check that destination is OK */
	{
		if (dest->im.data_type != IS_COMPLEX_IMAGE || 
		dest->im.nx != onx/2 || dest->im.ny != ony)
		{
			free_one_image(dest);
			dest = NULL;
		}
	}
 	oid = (dest != NULL) ? dest : create_one_image(onx/2, ony, IS_COMPLEX_IMAGE);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return(NULL);
	}
	
	pd = oid->im.pixel;
	if (fft_init(onx))
	{
		free_one_image(oid);
		win_printf("Can't initialize fft");
		return (NULL);
	}
	for (j=0; j< ony; j++)
	{
		no = onx * j;
		if  (no >= nout + 16384)	
		{	/* say how much done ~ every second*/
			sprintf (c, "fft row %d",j);
			display_title_message(c);
			nout = no;
		}
		extract_raw_line (ois, j, pd[j].fl);
		if (!(ois->im.win_flag & X_PER))	fftwindow1(onx, pd[j].fl, smp);
		realtr1(onx, pd[j].fl);
		fft(onx, pd[j].fl, 1);
		realtr2(onx, pd[j].fl, 1);
	}

	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
/*
	un = build_unit_set(0, xs, 1, 0, 0, NULL);		
	if (un == NULL)		return 1;
	add_to_one_image (oid, IS_X_UNIT_SET, (void *)un);
	un = build_unit_set(0, ys, 1, 0, 0, NULL);
	if (un == NULL)		return 1;	
	add_to_one_image (oid, IS_Y_UNIT_SET, (void *)un);
	oid->c_xu = oid->n_xu - 1;
	oid->c_yu = oid->n_yu - 1;
*/
	oid->im.win_flag = ois->im.win_flag;
	oid->im.mode = AMP;
	set_im_x_title(oid,"wavenumber k_x");
	set_im_y_title(oid,"time");
	set_im_title(oid, "\\stack{{fft 1D of image}{%s}}",(ois->title != NULL) 
	? ois->title : ((ois->im.source != NULL) ? ois->im.source : "untitled"));
	set_formated_string(&oid->im.treatement,"sloppy fft1d image of %s",
	(ois->title != NULL) ? ois->title : ((ois->im.source != NULL) 
	? ois->im.source : "untitled"));	
	return oid;
}


	/*	compute the forward fft of a real symmetrized image 
	*	-dest is the result, if dest == NULL then it is created
	*	-ois is the source image
	*	-title is the ptr. to the title button to print message
	*		if NULL, no message print during process
	*	-smp is a small number tipically .05 in the Hanning window
	*		if smp = 0 the reverse operation is impossible
	*/
O_i 	*forward_fft_1dsym_im(O_i *dest, O_i *ois, float smp)
{
	register int j, k;
	int nout = 0, onx, ony, no;
	char c[128];
	union pix *pd;
	O_i  *oid;

	if (ois == NULL || ois->im.pixel == NULL)
	{
		win_printf("No image found");	
		return (NULL);	
	}
	ony = ois->im.ny;	onx = 2*ois->im.nx;	
	if (ois->im.data_type == IS_COMPLEX_IMAGE)
	{
		win_printf ("Cannot treat a complex image yet!");
		return(NULL);
	}
	if (dest != NULL)		/*	if dest exists check that destination is OK */
	{
		if (dest->im.data_type != IS_COMPLEX_IMAGE || 
		dest->im.nx != onx/2 || dest->im.ny != ony)
		{
			free_one_image(dest);
			dest = NULL;
		}
	}
 	oid = (dest != NULL) ? dest : create_one_image(onx/2, ony, IS_COMPLEX_IMAGE);
	if (oid == NULL)
	{
		win_printf("Can't create dest image");
		return(NULL);
	}
	
	pd = oid->im.pixel;
	if (fft_init(onx))
	{
		free_one_image(oid);
		win_printf("Can't initialize fft");
		return (NULL);
	}
	for (j=0; j< ony; j++)
	{
		no = onx * j;
		if (no >= nout + 16384)	
		{	/* say how much done ~ every second*/
			sprintf (c, "fft row %d",j);
			display_title_message(c);
			nout = no;
		}
		extract_raw_line (ois, j, pd[j].fl+onx/2);
		/* we place symetric profile */
		for (k = 0; k < onx/2; k++) pd[j].fl[(onx/2)-k-1] = pd[j].fl[(onx/2)+k];
		if (!(ois->im.win_flag & X_PER))	fftwindow1(onx, pd[j].fl, smp);
		realtr1(onx, pd[j].fl);
		fft(onx, pd[j].fl, 1);
		realtr2(onx, pd[j].fl, 1);
	}

	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
/*
	un = build_unit_set(0, xs, 1, 0, 0, NULL);		
	if (un == NULL)		return 1;
	add_to_one_image (oid, IS_X_UNIT_SET, (void *)un);
	un = build_unit_set(0, ys, 1, 0, 0, NULL);
	if (un == NULL)		return 1;	
	add_to_one_image (oid, IS_Y_UNIT_SET, (void *)un);
	oid->c_xu = oid->n_xu - 1;
	oid->c_yu = oid->n_yu - 1;
*/
	oid->im.win_flag = ois->im.win_flag;
	oid->im.mode = AMP;
	set_im_x_title(oid,"wavenumber k_x");
	set_im_y_title(oid,"time");
	set_im_title(oid, "\\stack{{fft sym 1D of image}{%s}}",(ois->title != NULL) 
	? ois->title : ((ois->im.source != NULL) ? ois->im.source : "untitled"));
	set_formated_string(&oid->im.treatement,"sloppy fft1d sym image of %s",
	(ois->title != NULL) ? ois->title : ((ois->im.source != NULL) 
	? ois->im.source : "untitled"));	
	return oid;
}

O_i	*do_1d_deriv(O_i *dest, O_i *ois)
{
	register  int j, k;
	int  onx, ony;
	union pix *pd;
	O_i  *oid;
	float xt[4096];

	if (ois == NULL || ois->im.pixel == NULL)
	{
		win_printf("No image found");	
		return (NULL);	
	}
	ony = ois->im.ny;	onx = ois->im.nx;	
	if (ois->im.data_type == IS_COMPLEX_IMAGE)
	{
		win_printf ("Cannot treat a complex image yet!");
		return(NULL);
	}
	if (dest != NULL)		/*	if dest exists check that destination is OK */
	{
		if (dest->im.data_type != IS_FLOAT_IMAGE || 
		dest->im.nx != onx || dest->im.ny != ony)
		{
			free_one_image(dest);
			dest = NULL;
		}
	}
 	oid = (dest != NULL) ? dest : create_one_image(onx, ony, IS_FLOAT_IMAGE);
	if (oid == NULL)
	{
		win_printf("Can't create dest image %d %d",onx,ony);
		return(NULL);
	}
	
	pd = oid->im.pixel;
	for (j=0; j< ony; j++)
	{

		extract_raw_line (ois, j, xt);
		for (k=0; k< onx-1; k++)	pd[j].fl[k] = xt[k+1] - xt[k];

		
						
	}

	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);

	oid->im.win_flag = ois->im.win_flag;
	oid->im.mode = AMP;
	set_im_x_title(oid,"x");
	set_im_y_title(oid,"time");
	set_im_title(oid, "\\stack{{deriv 1D of image}{%s}}",(ois->title != NULL) 
	? ois->title : ((ois->im.source != NULL) ? ois->im.source : "untitled"));
	set_formated_string(&oid->im.treatement,"deriv 1d image of %s",
	(ois->title != NULL) ? ois->title : ((ois->im.source != NULL) 
	? ois->im.source : "untitled"));	
	return oid;
}
O_i	*do_1d_deriv_avg(O_i *dest, O_i *ois, int nexte)
{
	register  int j, k, ii;
	int  onx, ony, nexte_2, nexte_4, i1, i2;
	union pix *pd;
	O_i  *oid;
	float xt[4096];

	if (ois == NULL || ois->im.pixel == NULL)
	{
		win_printf("No image found");	
		return (NULL);	
	}
	ony = ois->im.ny;	onx = ois->im.nx;	
	if (ois->im.data_type == IS_COMPLEX_IMAGE)
	{
		win_printf ("Cannot treat a complex image yet!");
		return(NULL);
	}
	if (dest != NULL)		/*	if dest exists check that destination is OK */
	{
		if (dest->im.data_type != IS_FLOAT_IMAGE || 
		dest->im.nx != onx || dest->im.ny != ony)
		{
			free_one_image(dest);
			dest = NULL;
		}
	}
 	oid = (dest != NULL) ? dest : create_one_image(onx, ony, IS_FLOAT_IMAGE);
	if (oid == NULL)
	{
		win_printf("Can't create dest image %d %d",onx,ony);
		return(NULL);
	}
	
	pd = oid->im.pixel;

	nexte_4 = nexte/4;
	nexte_2 = nexte/2;
	for (j=0; j< ony; j++)
	{
		extract_raw_line (ois, j, xt);
		for (k=0; k< onx; k++)
		{
			for (pd[j].fl[k] = 0, ii = 0; ii < nexte_4; ii++)
			{
				i1 = k-ii+(nexte_2);
				i1 = (i1 < onx) ? i1: onx-1;
				i2 = k+ii-(nexte_2);
				i2 = (i2 < 0) ? 0: i2;				
				pd[j].fl[k] += xt[i1] - xt[i2];
			}
			 
		}
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);

	oid->im.win_flag = ois->im.win_flag;
	oid->im.mode = AMP;
	set_im_x_title(oid,"x");
	set_im_y_title(oid,"time");
	set_im_title(oid, "\\stack{{deriv 1D avg over %d of image}{%s}}",nexte,(ois->title != NULL) 
	? ois->title : ((ois->im.source != NULL) ? ois->im.source : "untitled"));
	set_formated_string(&oid->im.treatement,"deriv 1d avg over %d image of %s",nexte,
	(ois->title != NULL) ? ois->title : ((ois->im.source != NULL) 
	? ois->im.source : "untitled"));	
	return oid;
}

O_i	*do_FIR_avg_along_y(O_i *dest, O_i *ois, int size, int start)
{
	register  int j, k, ii;
	int  onx, ony, ony2;
	union pix *pd;
	O_i  *oid;
	float xt[4096];

	if (ois == NULL || ois->im.pixel == NULL)
	{
		win_printf("No image found");	
		return (NULL);	
	}
	ony = ois->im.ny;	onx = ois->im.nx;	
	if (ois->im.data_type == IS_COMPLEX_IMAGE)
	{
		win_printf ("Cannot treat a complex image yet!");
		return(NULL);
	}
	ony2 = (ony - start)/size;
	
	if (dest != NULL)		/*	if dest exists check that destination is OK */
	{
		if (dest->im.data_type != IS_FLOAT_IMAGE || 
		dest->im.nx != onx || dest->im.ny != ony2)
		{
			free_one_image(dest);
			dest = NULL;
		}
	}
 	oid = (dest != NULL) ? dest : create_one_image(onx, ony2, IS_FLOAT_IMAGE);
	if (oid == NULL)
	{
		win_printf("Can't create dest image %d %d",onx,ony);
		return(NULL);
	}
	
	pd = oid->im.pixel;


	for (j = 0; j < ony2; j++)
	{
		for (k=0; k< onx; k++) pd[j].fl[k] = 0;
		for (ii = 0; ii < size; ii++)
		{
			extract_raw_line (ois, (j*size)+ii, xt);
			for (k=0; k< onx; k++)
			{
				pd[j].fl[k] += xt[k];
			}
		}
		for (k=0; k< onx; k++) pd[j].fl[k] /= size;
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);
	for (ii=0 ; ii< oid->n_yu ; ii++)
	{
		oid->yu[ii]->ax += start * oid->yu[ii]->dx;
		oid->yu[ii]->dx *= size;
	}
	oid->ay += start * oid->dy;
	oid->dy *= size;
		
	oid->im.win_flag = ois->im.win_flag;
	oid->im.mode = AMP;
	set_im_x_title(oid,"x");
	set_im_y_title(oid,"time");
	set_im_title(oid, "\\stack{{FIR 1D avg over %d starting at %d of image}{%s}}",size,start,(ois->title != NULL) 
	? ois->title : ((ois->im.source != NULL) ? ois->im.source : "untitled"));
	set_formated_string(&oid->im.treatement,"FIR 1d avg over %d starting at %d image of %s",size, start,
	(ois->title != NULL) ? ois->title : ((ois->im.source != NULL) 
	? ois->im.source : "untitled"));	
	return oid;
}

O_i	*do_1d_avg_and_translate(O_i *dest, O_i *ois, int nexte, float *ac_pos, float center)
{
	register  int j;
	int  onx, ony, nexte_2, nexte_4;
	union pix *pd;
	O_i  *oid;
	float xt[4096];

	if (ois == NULL || ois->im.pixel == NULL)
	{
		win_printf("No image found");	
		return (NULL);	
	}
	ony = ois->im.ny;	onx = ois->im.nx;	
	if (ois->im.data_type == IS_COMPLEX_IMAGE)
	{
		win_printf ("Cannot treat a complex image yet!");
		return(NULL);
	}
	if (dest != NULL)		/*	if dest exists check that destination is OK */
	{
		if (dest->im.data_type != IS_FLOAT_IMAGE || 
		dest->im.nx != onx || dest->im.ny != ony)
		{
			free_one_image(dest);
			dest = NULL;
		}
	}
 	oid = (dest != NULL) ? dest : create_one_image(onx, ony, IS_FLOAT_IMAGE);
	if (oid == NULL)
	{
		win_printf("Can't create dest image %d %d",onx,ony);
		return(NULL);
	}
	
	pd = oid->im.pixel;

	nexte_4 = nexte/4;
	nexte_2 = nexte/2;
	for (j=0; j< ony; j++)
	{
		extract_raw_line (ois, j, xt);
		/*for (k=0; k< onx; k++)
		{
			for (pd[j].fl[k] = 0, ii = 0; ii < nexte_4; ii++)
			{
				i1 = k-ii+(nexte_2);
				i1 = (i1 < onx) ? i1: onx-1;
				i2 = k+ii-(nexte_2);
				i2 = (i2 < 0) ? 0: i2;				
				pd[j].fl[k] += xt[i1] - xt[i2];
			}
		}*/
		//for (k=0; k< onx; k++) xt[k] = pd[j].fl[k];
		fourier(xt, onx, 1);
		translate_fft(xt,onx,center - ac_pos[j], pd[j].fl);
		fourier(pd[j].fl, onx, -1);		
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);

	oid->im.win_flag = ois->im.win_flag;
	oid->im.mode = AMP;
	set_im_x_title(oid,"x");
	set_im_y_title(oid,"time");
	set_im_title(oid, "\\stack{{recenter 1D avg over %d of image}{%s}}",nexte,(ois->title != NULL) 
	? ois->title : ((ois->im.source != NULL) ? ois->im.source : "untitled"));
	set_formated_string(&oid->im.treatement,"recenter avg over %d image of %s",nexte,
	(ois->title != NULL) ? ois->title : ((ois->im.source != NULL) 
	? ois->im.source : "untitled"));	
	return oid;
}

O_i	*do_1d_deriv_avg_and_translate(O_i *dest, O_i *ois, int nexte, float *ac_pos, float center)
{
	register  int j, k, ii;
	int  onx, ony, nexte_2, nexte_4, i1, i2;
	union pix *pd;
	O_i  *oid;
	float xt[4096];

	if (ois == NULL || ois->im.pixel == NULL)
	{
		win_printf("No image found");	
		return (NULL);	
	}
	ony = ois->im.ny;	onx = ois->im.nx;	
	if (ois->im.data_type == IS_COMPLEX_IMAGE)
	{
		win_printf ("Cannot treat a complex image yet!");
		return(NULL);
	}
	if (dest != NULL)		/*	if dest exists check that destination is OK */
	{
		if (dest->im.data_type != IS_FLOAT_IMAGE || 
		dest->im.nx != onx || dest->im.ny != ony)
		{
			free_one_image(dest);
			dest = NULL;
		}
	}
 	oid = (dest != NULL) ? dest : create_one_image(onx, ony, IS_FLOAT_IMAGE);
	if (oid == NULL)
	{
		win_printf("Can't create dest image %d %d",onx,ony);
		return(NULL);
	}
	
	pd = oid->im.pixel;

	nexte_4 = nexte/4;
	nexte_2 = nexte/2;
	for (j=0; j< ony; j++)
	{
		extract_raw_line (ois, j, xt);
		for (k=0; k< onx; k++)
		{
			for (pd[j].fl[k] = 0, ii = 0; ii < nexte_4; ii++)
			{
				i1 = k-ii+(nexte_2);
				i1 = (i1 < onx) ? i1: onx-1;
				i2 = k+ii-(nexte_2);
				i2 = (i2 < 0) ? 0: i2;				
				pd[j].fl[k] += xt[i1] - xt[i2];
			}
		}
		for (k=0; k< onx; k++) xt[k] = pd[j].fl[k];
		fourier(xt, onx, 1);
		translate_fft(xt,onx,center - ac_pos[j], pd[j].fl);
		fourier(pd[j].fl, onx, -1);		
	}
	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);

	oid->im.win_flag = ois->im.win_flag;
	oid->im.mode = AMP;
	set_im_x_title(oid,"x");
	set_im_y_title(oid,"time");
	set_im_title(oid, "\\stack{{deriv and recenter 1D avg over %d of image}{%s}}",nexte,(ois->title != NULL) 
	? ois->title : ((ois->im.source != NULL) ? ois->im.source : "untitled"));
	set_formated_string(&oid->im.treatement,"deriv 1d and recenter avg over %d image of %s",nexte,
	(ois->title != NULL) ? ois->title : ((ois->im.source != NULL) 
	? ois->im.source : "untitled"));	
	return oid;
}






d_s	*do_1d_find_edge(O_p *dest, d_s *ds, O_i *ois, int w, int center, int nexte, int starty)
{
	register  int j, k, i;
	int  onx, ony, nx_2, start, ii;
	union pix *pd;
	O_i  *oid;
	O_p *op;
	float *x1 = NULL, *x2 = NULL, dx, corr; /* , dx[nexte]*/
	//acreg *ac, *aci = cur_ac_reg;

	if (ois == NULL || ois->im.pixel == NULL || dest == NULL)
	{
		win_printf("No image found");	
		return (NULL);	
	}
	ony = ois->im.ny;	onx = ois->im.nx;	
	if (ois->im.data_type == IS_COMPLEX_IMAGE)
	{
		win_printf ("Cannot treat a complex image yet!");
		return(NULL);
	}
	if (ds == NULL)		/*	if dest exists check that destination is OK */
	{
		ds = create_and_attach_one_ds(dest,ony,ony,0);
	}
	x1 = (float*) calloc(onx,sizeof(float));
	x2 = (float*) calloc(onx,sizeof(float));
	if (x1 == NULL || x2 == NULL) return NULL;
	
	nx_2 = w/2;
	start = center - nx_2;
	if (start < nexte/2) start = nexte/2;
	if (onx - start - w < nexte/2) start = onx - w - nexte/2;
	pd = oid->im.pixel;
	
# ifdef OLD	
	extract_raw_line (ois, starty, x1);
	for (j=starty+1; j< ony; j++)
	{
		extract_raw_line (ois, j, x2);
		for (i=0; i <nexte; i++)
		{
			for (k=0, dx[i] = 0; k< w; k++)
			{
				dx[i] += x2[start+k+i-(nexte/2)] - x1[start+k];
			}
		}
		for (i=k=0; i <nexte-1; i++)
		{
			if (dx[i]*dx[i+1] <= 0) k = i;
		}
		ds->xd[j] = k - (nexte/2) + dx[k]/(dx[k]-dx[k+1]);
		display_title_message("line %d dx %g dX %d",j,dx[k]/(dx[k]+dx[k+1]),k - (nexte/2) );
		if (j == ony/2) ac = create_plot_from_1d_array("correl", nexte, dx);
		ds->yd[j] = j;
		start += k - (nexte/2);
		ds->yd[j] = start + w/2;		
		if (start < nexte/2) start = nexte/2;
		if (onx - start - w < nexte/2) start = onx - w - nexte/2;
		xt = x1;
		x1 = x2;
		x2 = xt;
		 
	}
#endif
	for (j=starty, i = OK; j< ony; j++)
	{
		extract_raw_line (ois, j, x2);
		for (k=0; k< w; k++)
		{
			x1[k] = x2[start+k+(nexte/2)] - x2[start+k-(nexte/2)];
			for (ii = 1; ii <nexte/4; ii++)
				x1[k] += x2[start+k-ii+(nexte/2)] - x2[start+k+ii-(nexte/2)];
			 
		}
/*	    activate_acreg(create_plot_from_1d_array("front deriv", w, x1));*/
		dx = find_distance_from_center(x1, w, 0, w>>4, 1,&corr);
		
		ds->xd[j] = start + nx_2 + dx;
		if (i == OK) i = win_printf("line %d dx %g dX %d",j,dx,start);
		else display_title_message("line %d dx %g dX %d",j,dx,start);
 		if (j == ony/2) 
		  {
		    // ac = create_plot_from_1d_array("correl", w, x1);
		    op = create_and_attach_op_to_oi(ois, w, w, 0, 0);
		    for (k=0;op != NULL &&  k< w; k++)
		      {
			op->dat[0]->xd[k] = k;
			op->dat[0]->yd[k] = x1[k];
		      }
		  }
		ds->yd[j] = j;
		start += (int)(dx+.5);
		if (start < nexte/2) start = nexte/2;
		if (onx - start - w < nexte/2) start = onx - w - nexte/2;
		 
	}

	inherit_from_im_to_ds(ds, ois);
	uns_oi_2_op(ois, IS_X_UNIT_SET, dest, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, dest, IS_Y_UNIT_SET);
	free(x1);
	free(x2);
	//activate_acreg(ac);	
	//activate_acreg(aci);
	return ds;
}


int	do_sloppy_tracking(void)
{
	int i;
	O_i  *ois;
	O_p *op;
	d_s *ds;
	imreg *imr;
	static int width = 256, center = 256, nexte = 32, starty = 0;
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
		return OFF;
	i = win_scanf("track profile over %d starting at y line %d center in x %d extent of diff %d"
		,&width,&starty,&center,&nexte);
	if (i == CANCEL) return OFF;
	op = create_and_attach_op_to_oi(ois, ois->im.ny, ois->im.ny, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
	ds = op->dat[0];
	do_1d_find_edge(op, ds, ois, width, center, nexte, starty);
	return refresh_im_plot(imr, ois->n_op - 1);
}

int	do_sloppy_fft1d(void)
{
	O_i  *oid;
	imreg *imr;
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return D_O_K;


	oid = forward_fft_1d_im(NULL, imr->one_i, .05);
	if (oid == NULL)	return -1;
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}
int	do_sloppy_fft1d_sym(void)
{
	O_i  *oid;
	imreg *imr;
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return D_O_K;

	oid = forward_fft_1dsym_im(NULL, imr->one_i, .05);
	if (oid == NULL)	return -1;
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}


int	do_sloppy_deriv_avg(void)
{
	O_i  *oid;
	imreg *imr;
	static int nexte = 32;
	int i;
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return D_O_K;
	i = win_scanf("average size %d",&nexte);
	if (i == CANCEL)	return OFF;
	oid = do_1d_deriv_avg(NULL, imr->one_i, nexte);
	if (oid == NULL)	return -1;
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}

int	do_sloppy_FIR_avg(void)
{
	O_i  *oid;
	imreg *imr;
	static int size = 32, start = 0;
	int i;
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return D_O_K;
	i = win_scanf("average size %dstarting at %Md",&size,&start);
	if (i == CANCEL)	return OFF;
	oid = do_FIR_avg_along_y(NULL, imr->one_i, size, start);
	if (oid == NULL)	return -1;
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}



int	do_avg_recenter(void)
{
	O_i  *oid, *ois;
	O_p *op;
	imreg *imr;
	static int nexte = 32;
	int i;
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im%oi%oiop",&imr,&ois,&op) != 3)	return OFF;
		
	i = win_scanf("averaging size %d",&nexte);
	if (i == CANCEL)	return OFF;
	oid = do_1d_avg_and_translate(NULL, ois, nexte, op->dat[0]->xd, (float)ois->im.nx/2);
	if (oid == NULL)	return -1;
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}


int	do_sloppy_deriv_avg_recenter(void)
{
	O_i  *oid, *ois;
	O_p *op;
	imreg *imr;
	static int nexte = 32;
	int i;
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im%oi%oiop",&imr,&ois,&op) != 3)	return OFF;
		
	i = win_scanf("averaging size %d",&nexte);
	if (i == CANCEL)	return OFF;
	oid = do_1d_deriv_avg_and_translate(NULL, ois, nexte, op->dat[0]->xd, (float)ois->im.nx/2);
	if (oid == NULL)	return -1;
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}



int	do_sloppy_deriv(void)
{
	O_i  *oid;
	imreg *imr;
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return D_O_K;

	oid = do_1d_deriv(NULL, imr->one_i);
	if (oid == NULL)	return -1;
	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	return (refresh_image(imr, imr->n_oi - 1));
}

/*Extracts nbr_of_row rows from an image*/
int extract_n_lines(O_i *oi, O_p *op, int nbr_of_lines)
{
    register int i, j;
    d_s *ds; 
    
    ds = op->dat[0];
	for (i = 0; i < nbr_of_lines; i++)
	  {
	    if (i) ds = create_and_attach_one_ds(op,oi->im.nx,oi->im.nx,0);
	    extract_raw_line (oi, i, ds->yd);
	    for (j = 0; j < ds->nx; j++)
            ds->xd[j] = j;
	    //for (j = 0; j < ds->ny; j++) ds->yd[j] -= background;
	    /*	    if (i == 0) set_ds_line_color(ds, Yellow);
	    if (i == 1) set_ds_line_color(ds, Lightblue);
	    if (i == 2) set_ds_line_color(ds, Lightred);
	    if (i == 3) set_ds_line_color(ds, Lightgreen);
	    if (i == 4) set_ds_line_color(ds, Lightmagenta);
	    if (i == 5) set_ds_line_color(ds, Lightcyan);
	    if (i == 6) set_ds_line_color(ds, Darkgray);
	    if (i == 7) set_ds_line_color(ds, Cyan);
	    if (i == 8) set_ds_line_color(ds, Magenta);*/
	  }
	uns_oi_2_op(oi, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(oi, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
    return 0;
}

/*Extracts nbr_of_row rows from an image*/
int extract_n_rows(O_i *oi, O_p *op, int nbr_of_rows)
{
    register int i, j;
    d_s *ds; 
    
    ds = op->dat[0];
	for (i = 0; i < nbr_of_rows; i++)
	  {
	    if (i) ds = create_and_attach_one_ds(op,oi->im.ny,oi->im.ny,0);
	    extract_raw_row (oi, i, ds->yd);
	    for (j = 0; j < ds->nx; j++) 
            ds->xd[j] = j;
        if (i == 0) set_ds_line_color(ds, Yellow);
	    if (i == 1) set_ds_line_color(ds, Lightblue);
	    if (i == 2) set_ds_line_color(ds, Lightred);
	    if (i == 3) set_ds_line_color(ds, Lightgreen);
	    if (i == 4) set_ds_line_color(ds, Lightmagenta);
	    if (i == 5) set_ds_line_color(ds, Lightcyan);
	    if (i == 6) set_ds_line_color(ds, Darkgray);
	    if (i == 7) set_ds_line_color(ds, Cyan);
	    if (i == 8) set_ds_line_color(ds, Magenta);
        
	  }
	uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(oi, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
    return 0;
}

/*Extracts n-lines from an image and copy into plot*/
int	do_extract_n_lines(void)
{
	O_i  *oi;
	imreg *imr;
	static int index = 0, nbr_of_lines = 8;
	pltreg *pr = NULL;
	O_p *op;
	static int background = 1;
	//char *oi_filename;
	register int i;
	
	if(updating_menu_state != 0)
        return D_O_K;
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return D_O_K;

//taking thata from user
	i = win_scanf("How many lines do you want to extract%5d\n"
		      "Background %5d is substracted to amplitude\n"
		      "In a new project %R or just copied in scratchpad %r\n",&nbr_of_lines, &background, &index);
	if(i == CANCEL)
	  return OFF;
	if(nbr_of_lines > oi->im.ny)
	  {
	    win_printf("number of lines bigger than image");
	    return CANCEL;
	  }
	if (index == 0)
	  {
	    pr = create_and_register_new_plot_project(0,   32,  900,  668);
	    if (pr == NULL)    	win_printf_OK("cannot create plot region");
	    op = create_one_plot(oi->im.nx, oi->im.nx, 0);
	    
	  }
	else
	  {
	    if (op_copied != NULL)
	      {
		free_one_plot(op_copied);
		op_copied = NULL;
	      }
	    op = create_one_plot(oi->im.nx, oi->im.nx, 0);
	  }
	if (op == NULL)    	win_printf_OK("cannot create plot");
	//oi_filename = (char *)malloc( sizeof(char *) );

//executing the functin	
    extract_n_lines(oi, op, nbr_of_lines);

    if(get_oi_filename(oi) != NULL)
    {
	   //oi_filename = get_oi_filename(oi);
	   /*set_plot_title (op, "%s modes 0-%d", oi_filename, n_line-1);
	   set_plot_x_title (op, "Position along ");
	   set_plot_y_title (op, "Mode amplitude");*/
	   //op->filename = strdup(oi_filename);
	   set_op_filename(op, oi->filename);
    }
	if (index == 0)
	  {
        add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)op);
        remove_data_from_pltreg (pr, IS_ONE_PLOT, (void *)pr->o_p[0]);
	    if (pr) do_one_plot(pr);
	    broadcast_dialog_message(MSG_DRAW,0);	
	    return D_REDRAW;	
	  }
	else return OK;
}

/*Menu called function executing n_row*/
int	do_extract_n_rows(void)
{
	O_i  *oi;
	imreg *imr;
	static int index = 0, nbr_of_rows = 12;
	pltreg *pr = NULL;
	O_p *op;
	//char op_filename[256];
	register int i;
	
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return D_O_K;

//taking data from user
	i = win_scanf("How many rows do you want %5d\n"
		      "In a new project %R or just copied in scrachpad %r\n",&nbr_of_rows,&index);
	if(i == CANCEL)
	  return OFF;
	if (index == 0)
	  {
	    pr = create_and_register_new_plot_project(0,   32,  900,  668);
	    if (pr == NULL)    	win_printf_OK("cannot create plot region");
	    op = create_one_plot(oi->im.ny, oi->im.ny, 0);
	  }
	else
	  {
	    if (op_copied != NULL)
	      {
		free_one_plot(op_copied);
		op_copied = NULL;
	      }
	    op = create_one_plot(oi->im.ny, oi->im.ny, 0);
	  }
	if (op == NULL)    	win_printf_OK("cannot create plot");

//executing the function	
	extract_n_rows(oi, op, nbr_of_rows);
	set_plot_title (op, "%s modes 0-%d", oi->filename, nbr_of_rows-1);
	set_plot_x_title (op, "Position along y");
	set_plot_y_title (op, "Mode amplitude");
	set_op_filename(op, oi->filename);

	if (index == 0)
	  {
        add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)op);
	    remove_data_from_pltreg (pr, IS_ONE_PLOT, (void *)pr->o_p[0]);
	    if (pr) do_one_plot(pr);
	    broadcast_dialog_message(MSG_DRAW,0);	
	    return D_REDRAW;	
	  }
	else return OK;
}

int	do_sloppy_fft1d_and_extract_n_rows(void)
{
	O_i  *oi;
	imreg *imr;
	static int  index = 0, 
                nbr_of_rows = 12,
                im_mode =0;
	pltreg *pr = NULL;
	O_p *op;
	register int i;
	char op_filename[256];
	
	if(updating_menu_state != 0)	return D_O_K;
	if (ac_grep(cur_ac_reg,"%im",&imr) != 1)	return D_O_K;

//making FFT image
	oi = forward_fft_1d_im(NULL, imr->one_i, .05);
	if (oi == NULL)	return -1;
	
//taking data from user
	i = win_scanf("1D FFT of selected image will be performed along x.\n"
                    "Then, %5d Fourier modes will be ploted along y\n"
		            "In a new project %R or just copied in scratchpad %r\n"
                    "Select image mode as:\n"
                    "%R Amplitude,   %r Real part,   %r Imaginary part",&nbr_of_rows,&index, &im_mode);
	if(i == CANCEL)
	  return OFF;
	
    if(im_mode == 0)  
	   oi->im.mode = AMP;
	else if(im_mode == 1)
        oi->im.mode = RE;
	else if (im_mode == 2)
        oi->im.mode = IM;
	if (index == 0)
	  {
	    pr = create_and_register_new_plot_project(0,   32,  900,  668);
	    if (pr == NULL)    	win_printf_OK("cannot create plot region");
	    op = create_one_plot(oi->im.ny, oi->im.ny, 0);
	  }
	else
	  {
	    if (op_copied != NULL)
	      {
		free_one_plot(op_copied);
		op_copied = NULL;
	      }
	    op = create_one_plot(oi->im.ny, oi->im.ny, 0);
	  }
	if (op == NULL)    	win_printf_OK("cannot create plot");

//executing the function	
	extract_n_rows(oi, op, nbr_of_rows);
    set_plot_title (op, "%s modes 0-%d", oi->filename, nbr_of_rows-1);
	set_plot_x_title (op, "Position along y");
	set_plot_y_title (op, "Mode amplitude");
	strcpy(op_filename, oi->filename);
	strcat(op_filename, "-mod");
	set_op_filename(op, op_filename);
	if (index == 0)
	  {
        add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)op);
	    remove_data_from_pltreg (pr, IS_ONE_PLOT, (void *)pr->o_p[0]);
	    if (pr) do_one_plot(pr);
	    broadcast_dialog_message(MSG_DRAW,0);	
	    return D_REDRAW;	
	  }
	else return OK;
}


MENU *diffusion_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"FFT 1d",do_sloppy_fft1d,NULL,0,NULL);
	add_item_to_menu(mn,"FFT 1d sym",do_sloppy_fft1d_sym,NULL,0,NULL);
	add_item_to_menu(mn,"FFT 1d to plot",do_sloppy_fft1d_and_extract_n_rows,NULL,0,NULL);
	add_item_to_menu(mn,"Extract n rows",do_extract_n_rows,NULL,0,NULL);
	add_item_to_menu(mn,"Extract n lines",do_extract_n_lines,NULL,0,NULL);
	add_item_to_menu(mn,"avg recenter",do_avg_recenter,NULL,0,NULL);
	add_item_to_menu(mn,"deriv 1d",do_sloppy_deriv,NULL,0,NULL);
	add_item_to_menu(mn,"deriv 1d avg",do_sloppy_deriv_avg,NULL,0,NULL);
	add_item_to_menu(mn,"deriv 1d avg recenter",do_sloppy_deriv_avg_recenter,NULL,0,NULL);
	add_item_to_menu(mn,"tracking",do_sloppy_tracking,NULL,0,NULL);
	add_item_to_menu(mn,"FIR avg",do_sloppy_FIR_avg,NULL,0,NULL);
	
	return mn;
}

int	diffusion_main(int argc, char **argv)
{
	add_image_treat_menu_item ( "diffusion", NULL, diffusion_plot_menu(), 0, NULL);
	return D_O_K;
}

int	diffusion_unload(int argc, char **argv)
{
	remove_item_to_menu(image_treat_menu, "diffusion", NULL, NULL);
	return D_O_K;
}
#endif

