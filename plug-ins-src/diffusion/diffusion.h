#ifndef _DIFFUSION_H_
#define _DIFFUSION_H_

PXV_FUNC(int, deplace,(float *x, int nx));
PXV_FUNC(int, fourier,(float *x, int nx, int df));
PXV_FUNC(int, translate_fft,(float *x, int nx, float dx, float *xt));
PXV_FUNC(int, fftwindow_flat_top1,(int npts, float *x, float smp));
PXV_FUNC(int, imaginary_fft,(float *x, int nx, float *xt));
PXV_FUNC(int, 	complex_and_fourier_backward,(int nx, float *fx1));
PXV_FUNC(int, flatten,(float *x, int nx));
PXV_FUNC(int, 	shift_and_fourier_backward,(int nx, float *fx1, float offset));
PXV_FUNC(int,	find_max_by_phase,(float *re, float *im, int nx, float *dx, float *corr, float *ft));
PXV_FUNC(int,	find_max1,(float *x, int nx, float *Max_pos, float *Max_val));
PXV_FUNC(int,	correlate_1d_sig_and_invert,(float *x1, int nx, int window, float *fx1, int filter, int remove_dc));
PXV_FUNC(int,	correlate_1d_sig_and_invert_in_fourier,(float *x1, int nx, int window, float *fx1, int filter, int remove_dc));
PXV_FUNC(float,	find_distance_from_center_by_phase,(float *x1, int cl, int wflag, int filter, int black_circle, float *corr));
PXV_FUNC(float,	find_distance_from_center,(float *x1, int cl, int flag, int filter, int black_circle, float *corr));
PXV_FUNC(O_i, 	*forward_fft_1d_im,(O_i *dest, O_i *ois, float smp));
PXV_FUNC(O_i, 	*forward_fft_1dsym_im,(O_i *dest, O_i *ois, float smp));
PXV_FUNC(O_i,	*do_1d_deriv,(O_i *dest, O_i *ois));
PXV_FUNC(O_i,	*do_1d_deriv_avg,(O_i *dest, O_i *ois, int nexte));
PXV_FUNC(O_i,	*do_FIR_avg_along_y,(O_i *dest, O_i *ois, int size, int start));
PXV_FUNC(O_i,	*do_1d_avg_and_translate,(O_i *dest, O_i *ois, int nexte, float *ac_pos, float center));
PXV_FUNC(O_i,	*do_1d_deriv_avg_and_translate,(O_i *dest, O_i *ois, int nexte, float *ac_pos, float center));
PXV_FUNC(d_s,	*do_1d_find_edge,(O_p *dest, d_s *ds, O_i *ois, int w, int center, int nexte, int starty));
PXV_FUNC(int,	do_sloppy_tracking,(void));
PXV_FUNC(int,	do_sloppy_fft1d,(void));
PXV_FUNC(int,	do_sloppy_fft1d_sym,(void));
PXV_FUNC(int,	do_sloppy_deriv_avg,(void));
PXV_FUNC(int,	do_sloppy_FIR_avg,(void));
PXV_FUNC(int,	do_avg_recenter,(void));
PXV_FUNC(int,	do_sloppy_deriv_avg_recenter,(void));
PXV_FUNC(int,	do_sloppy_deriv,(void));
PXV_FUNC(int, extract_n_lines,(O_i *oi, O_p *op, int nbr_of_lines));
PXV_FUNC(int, extract_n_rows,(O_i *oi, O_p *op, int nbr_of_rows));
PXV_FUNC(int,	do_extract_n_lines,(void));
PXV_FUNC(int,	do_extract_n_rows,(void));
PXV_FUNC(int,	do_sloppy_fft1d_and_extract_n_rows,(void));
PXV_FUNC(MENU, *diffusion_plot_menu,(void));
PXV_FUNC(int,	diffusion_main,(int argc, char **argv));
PXV_FUNC(int,	diffusion_unload,(int argc, char **argv));


#endif

