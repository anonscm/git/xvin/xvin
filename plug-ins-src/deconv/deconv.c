/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _DECONV_C_
#define _DECONV_C_

# include "allegro.h"
# include "xvin.h"
#include <fftw3.h>		// include the fftw v3.0.1 library for computing spectra:

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "deconv.h"


int do_deconv_rescale_data_set(void)
{
	register int j;
	O_p *op = NULL;
	int nf;
	d_s *ds, *dsi;
	pltreg *pr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine differentiate the y coordinate"
				     "of a data set");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("cannot find data");
	nf = dsi->nx;	/* this is the number of points in the data set */

	if ((ds = create_and_attach_one_ds(op, nf-1, nf-1, 0)) == NULL)
		return win_printf_OK("cannot create plot !");

	for (j = 0; j < nf-1; j++)
	{
		ds->yd[j] = dsi->yd[j+1] - dsi->yd[j];
		ds->xd[j] = (dsi->xd[j+1] + dsi->xd[j])/2;
	}

	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement,"y differentiate");
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

int do_deconv_rescale_plot(void)
{
	register int i, j, k;
	O_p *op = NULL, *opn = NULL;
	int nf;
	static float factor = 100000;
	d_s *ds, *dsi1, *dsi2;
	int n_dsi1 = 0, n_dsi2 = 0;
	pltreg *pr = NULL;
	double  *in=NULL, *out=NULL , *out2=NULL; 
	double  re1, re2, im1, im2, fac_1, norm, max = 0, sqnorm, norm2; 
	fftw_plan plan;     // fft plan for fftw3
	char message[1024];

	if(updating_menu_state != 0)	return D_O_K;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine deconvolve the second data set by the first one"
				     "and place the result in a new plot");
	}
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi1) != 3)
		return win_printf_OK("cannot find data");
	nf = dsi1->nx;	/* this is the number of points in the data set */
	n_dsi1 = op->cur_dat;
	n_dsi2 = (n_dsi1 + 1)%op->n_dat;

	sprintf(message,"This function will deconvolve the selected dataset\n"
		"having index %5d by a second data set having index %%5d\n"
		"Define the maximum multiplicative factor %%12f\n",n_dsi1);
	i = win_scanf(message,&n_dsi2,&factor);
	if (i == CANCEL)	return OFF;
	if ( n_dsi2 < 0 || n_dsi2 >= op->n_dat)	return win_printf_OK("Index out of range");
	dsi2 = op->dat[n_dsi2];
	

	if (dsi2->nx != dsi1->nx) return win_printf_OK("Data sets have not the same number of points");

	if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds = opn->dat[0];

	in = fftw_malloc(nf*sizeof(double));
	out = fftw_malloc(nf*sizeof(double));
	out2 = fftw_malloc(nf*sizeof(double));
	// plan           = fftw_create_plan(nx, FFTW_REAL_TO_COMPLEX, FFTW_ESTIMATE); // fftw2 version!
	plan = fftw_plan_r2r_1d(nf, in, out, FFTW_R2HC, FFTW_ESTIMATE);   // the new fftw3 version!
	//plan = fftw_plan_dft_r2c_1d(nf, in, out, FFTW_ESTIMATE);	
	for (i=0 ; i< nf ; i++)     in[i] = dsi2->yd[i];
	fftw_execute(plan); // fftw3 version!
	for (i=0 ; i<= nf ; i++)     out2[i] = out[i];
	for (i=0 ; i< nf ; i++)     
	  {
	    in[i] = dsi1->yd[i];
	    //if (i%128 == 0) win_printf("pt %d val %g",i,in[i]);
	  }
	fftw_execute(plan); // fftw3 version!

	// r0, r1, r2, ..., rn/2, i(n+1)/2-1, ..., i2, i1 
	// n = 5 => 0->r0,1->r1,2->r2,3->i2,4->i1
	// n = 6 => 0->r0,1->r1,2->r2,3->r3,4->i2,5->i1

	fac_1 = (double)1/(factor*factor);


	for (k = 0, i=1; i<(nf+1)/2; ++i)          // k < nx/2 rounded up
	  {
	    re1 = out[i];	    re2 = out2[i];
	    im1 = out[nf-i];	    im2 = out2[nf-i];
	    norm = re1*re1 + im1*im1;
	    norm2 = re2*re2 + im2*im2;
	    sqnorm = sqrt(norm);
	    k += (norm < norm2*fac_1) ? 1 : 0;
	    norm = (norm < norm2*fac_1) ? factor*sqnorm : (double)1/norm;
	    //norm = (norm != 0) ? (double)1/norm : 1;
	    in[i] = (re2 * re1 + im2 * im1)*norm;
	    in[nf-i] = (re2 * im1 - re1 * im2)*norm;
	  }
	if (nf % 2 ==0) // nx is even
	  {
	    re1 = out[nf/2];	    re2 = out2[nf/2];
	    sqnorm = re1;
	    norm2 = re2;
	    k += (sqnorm*factor < norm2) ? 1 : 0;
	    sqnorm = (sqnorm*factor < norm2) ? factor : (double)1/sqnorm;
	    //norm = (norm != 0) ? (double)1/norm : 1;
	    in[nf/2] = re2*sqnorm;
	  }
	re1 = out[0];	    re2 = out2[0];
	sqnorm = re1;
	norm2 = re2;
	k += (sqnorm*factor < norm2) ? 1 : 0;
	sqnorm = (sqnorm*factor < norm2) ? factor : (double)1/sqnorm;
	in[0] = re2*sqnorm;
	fftw_destroy_plan(plan);
	win_printf("Nb of gain reduction %d",k);
	//for (i=0 ; i<= nf ; i++)     in[i] = out[i];
	//plan = fftw_plan_dft_c2r_1d(nf, in, out, FFTW_ESTIMATE);   
	plan = fftw_plan_r2r_1d(nf, in, out, FFTW_HC2R, FFTW_ESTIMATE);   // the new fftw3 version!
	fftw_execute(plan); // fftw3 version!


	for (j = 0; j < nf; j++)
	{
		ds->yd[j] = out[j];
		ds->xd[j] = dsi1->xd[j];
	}

	fftw_destroy_plan(plan);
	fftw_free(in); fftw_free(out); fftw_free(out2);

	/* now we must do some house keeping */
	inherit_from_ds_to_ds(ds, dsi1);
	ds->treatement = my_sprintf(ds->treatement,"y multiplied by %f",factor);
	set_plot_title(opn, "Multiply by %f",factor);
	if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
	if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
	opn->filename = Transfer_filename(op->filename);
	uns_op_2_op(opn, op);
	/* refisplay the entire plot */
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}

MENU *deconv_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"simple derivate", do_deconv_rescale_data_set,NULL,0,NULL);
	add_item_to_menu(mn,"Deconvolution", do_deconv_rescale_plot,NULL,0,NULL);
	return mn;
}

int	deconv_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "deconv", NULL, deconv_plot_menu(), 0, NULL);
	return D_O_K;
}

int	deconv_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "deconv", NULL, NULL);
	return D_O_K;
}
#endif

