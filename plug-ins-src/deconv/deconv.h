#ifndef _DECONV_H_
#define _DECONV_H_

PXV_FUNC(int, do_deconv_rescale_plot, (void));
PXV_FUNC(MENU*, deconv_plot_menu, (void));
PXV_FUNC(int, do_deconv_rescale_data_set, (void));
PXV_FUNC(int, deconv_main, (int argc, char **argv));
#endif

