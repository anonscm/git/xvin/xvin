/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _TEST_PORTAUDIO_C_
#define _TEST_PORTAUDIO_C_

# include "allegro.h"
# include "xvin.h"
# include "portaudio.h"
/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin 
# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "Test_portaudio.h"



#ifdef WIN32
#include <windows.h>
#if PA_USE_ASIO
#include "pa_asio.h"
#endif
#endif


static void PrintSupportedStandardSampleRates(
					      const PaStreamParameters *inputParameters,
					      const PaStreamParameters *outputParameters, char *Message, int size)
{
  static double standardSampleRates[] = {
    8000.0, 9600.0, 11025.0, 12000.0, 16000.0, 22050.0, 24000.0, 32000.0,
    44100.0, 48000.0, 88200.0, 96000.0, 192000.0, -1 /* negative terminated list */
  };
  int i, printCount;
  PaError err;
 
  printCount = 0;
  for( i=0; standardSampleRates[i] > 0; i++ )
    {
      err = Pa_IsFormatSupported( inputParameters, outputParameters, standardSampleRates[i] );
      if( err == paFormatIsSupported )
	{
	  if( printCount == 0 )
	    {
	      snprintf(Message+strlen(Message),size-strlen(Message),"\t%8.2f", standardSampleRates[i] );
	      printCount = 1;
	    }
	  else if( printCount == 4 )
	    {
	      snprintf(Message+strlen(Message),size-strlen(Message),",\n\t%8.2f", standardSampleRates[i] );
	      printCount = 1;
	    }
	  else
	    {
	      snprintf(Message+strlen(Message),size-strlen(Message),", %8.2f", standardSampleRates[i] );
	      ++printCount;
	    }
	}
    }
  if( !printCount )
    snprintf(Message+strlen(Message),size-strlen(Message),"None\n" );
  else
    snprintf(Message+strlen(Message),size-strlen(Message),"\n" );
}
 



/*******************************************************************/
int Test(void)
{
  int i, numDevices, defaultDisplayed;
  const PaDeviceInfo *deviceInfo;
  PaStreamParameters inputParameters, outputParameters;
  PaError err;
  char Message[4096], *m; 
 
  err = Pa_Initialize();
  if( err != paNoError )
      return win_printf_OK( "ERROR: Pa_Initialize returned 0x%x\n", err );

 
  win_printf( "PortAudio version number = %d\nPortAudio version text = '%s'\n",
	  Pa_GetVersion(), Pa_GetVersionText() );
 
 
  numDevices = Pa_GetDeviceCount();
  if( numDevices < 0 )
      return win_printf_OK( "ERROR: Pa_GetDeviceCount returned 0x%x\n", numDevices );

  m = Message;
  snprintf(Message,sizeof(Message), "Number of devices = %d\n", numDevices );
  for( i=0; i<numDevices; i++ )
    {
      deviceInfo = Pa_GetDeviceInfo( i );
      printf( "--------------------------------------- device #%d\n", i );
 
      /* Mark global and API specific default devices */
      defaultDisplayed = 0;
      if( i == Pa_GetDefaultInputDevice() )
	{
	  snprintf(m+strlen(m),sizeof(Message)-strlen(m), "[ Default Input" );
	  defaultDisplayed = 1;
	}
      else if( i == Pa_GetHostApiInfo( deviceInfo->hostApi )->defaultInputDevice )
	{
	  const PaHostApiInfo *hostInfo = Pa_GetHostApiInfo( deviceInfo->hostApi );
	  snprintf(m+strlen(m),sizeof(Message)-strlen(m),"[ Default %s Input", hostInfo->name );
	  defaultDisplayed = 1;
	}
 
      if( i == Pa_GetDefaultOutputDevice() )
	{
	  snprintf(m+strlen(m),sizeof(Message)-strlen(m),(defaultDisplayed ? "," : "[") );
	  snprintf(m+strlen(m),sizeof(Message)-strlen(m)," Default Output" );
	  defaultDisplayed = 1;
	}
      else if( i == Pa_GetHostApiInfo( deviceInfo->hostApi )->defaultOutputDevice )
	{
	  const PaHostApiInfo *hostInfo = Pa_GetHostApiInfo( deviceInfo->hostApi );
	  snprintf(m+strlen(m),sizeof(Message)-strlen(m),(defaultDisplayed ? "," : "[") );
	  snprintf(m+strlen(m),sizeof(Message)-strlen(m)," Default %s Output", hostInfo->name );
	  defaultDisplayed = 1;
	}
 
      if( defaultDisplayed )
	snprintf(m+strlen(m),sizeof(Message)-strlen(m)," ]\n" );
 
      /* print device info fields */
#ifdef WIN32
      { /* Use wide char on windows, so we can show UTF-8 encoded device names */
	wchar_t wideName[MAX_PATH];
	MultiByteToWideChar(CP_UTF8, 0, deviceInfo->name, -1, wideName, MAX_PATH-1);
	snprintf(m+strlen(m),sizeof(Message)-strlen(m), "Name = %s\n", deviceInfo->name ); // wideName
      }
#else
      snprintf(m+strlen(m),sizeof(Message)-strlen(m),"Name = %s\n", deviceInfo->name );
#endif
      snprintf(m+strlen(m),sizeof(Message)-strlen(m),"Host API = %s\n", Pa_GetHostApiInfo( deviceInfo->hostApi )->name );
      snprintf(m+strlen(m),sizeof(Message)-strlen(m),"Max inputs = %d", deviceInfo->maxInputChannels );
      snprintf(m+strlen(m),sizeof(Message)-strlen(m),", Max outputs = %d\n", deviceInfo->maxOutputChannels );
 
      snprintf(m+strlen(m),sizeof(Message)-strlen(m),"Default low input latency = %8.4f\n", deviceInfo->defaultLowInputLatency );
      snprintf(m+strlen(m),sizeof(Message)-strlen(m),"Default low output latency = %8.4f\n", deviceInfo->defaultLowOutputLatency );
      snprintf(m+strlen(m),sizeof(Message)-strlen(m),"Default high input latency = %8.4f\n", deviceInfo->defaultHighInputLatency );
      snprintf(m+strlen(m),sizeof(Message)-strlen(m),"Default high output latency = %8.4f\n", deviceInfo->defaultHighOutputLatency );
 
#ifdef WIN32
#if PA_USE_ASIO
      /* ASIO specific latency information */
      if( Pa_GetHostApiInfo( deviceInfo->hostApi )->type == paASIO ){
	long minLatency, maxLatency, preferredLatency, granularity;
 
	err = PaAsio_GetAvailableLatencyValues( i,
						&minLatency, &maxLatency, &preferredLatency, &granularity );
 
	snprintf(m+strlen(m),sizeof(Message)-strlen(m),"ASIO minimum buffer size = %ld\n", minLatency );
	snprintf(m+strlen(m),sizeof(Message)-strlen(m),"ASIO maximum buffer size = %ld\n", maxLatency );
	snprintf(m+strlen(m),sizeof(Message)-strlen(m),"ASIO preferred buffer size = %ld\n", preferredLatency );
 
	if( granularity == -1 )
	  snprintf(m+strlen(m),sizeof(Message)-strlen(m),"ASIO buffer granularity = power of 2\n" );
	else
	  snprintf(m+strlen(m),sizeof(Message)-strlen(m),"ASIO buffer granularity = %ld\n", granularity );
      }
#endif /* PA_USE_ASIO */
#endif /* WIN32 */
 
      snprintf(m+strlen(m),sizeof(Message)-strlen(m),"Default sample rate = %8.2f\n", deviceInfo->defaultSampleRate );
 
      /* poll for standard sample rates */
      inputParameters.device = i;
      inputParameters.channelCount = deviceInfo->maxInputChannels;
      inputParameters.sampleFormat = paInt16;
      inputParameters.suggestedLatency = 0; /* ignored by Pa_IsFormatSupported() */
      inputParameters.hostApiSpecificStreamInfo = NULL;
 
      outputParameters.device = i;
      outputParameters.channelCount = deviceInfo->maxOutputChannels;
      outputParameters.sampleFormat = paInt16;
      outputParameters.suggestedLatency = 0; /* ignored by Pa_IsFormatSupported() */
      outputParameters.hostApiSpecificStreamInfo = NULL;
 
      if( inputParameters.channelCount > 0 )
	{
	  snprintf(m+strlen(m),sizeof(Message)-strlen(m),"Supported standard sample rates\n for half-duplex 16 bit %d channel input = \n",
		 inputParameters.channelCount );
	  PrintSupportedStandardSampleRates( &inputParameters, NULL, Message, sizeof(Message));
	}
 
      if( outputParameters.channelCount > 0 )
	{
	  snprintf(m+strlen(m),sizeof(Message)-strlen(m),"Supported standard sample rates\n for half-duplex 16 bit %d channel output = \n",
		 outputParameters.channelCount );
	  PrintSupportedStandardSampleRates( NULL, &outputParameters, Message, sizeof(Message));
	}
 
      if( inputParameters.channelCount > 0 && outputParameters.channelCount > 0 )
	{
	  snprintf(m+strlen(m),sizeof(Message)-strlen(m),"Supported standard sample rates\n for full-duplex 16 bit %d channel input, %d channel output = \n",
		 inputParameters.channelCount, outputParameters.channelCount );
	  PrintSupportedStandardSampleRates( &inputParameters, &outputParameters, Message, sizeof(Message));
	}
      win_printf_OK(m);
      m[0] = 0;
    }
 
  Pa_Terminate();
 
  snprintf(m+strlen(m),sizeof(Message)-strlen(m),"----------------------------------------------\n");
  return win_printf_OK(m);
}



int do_Test_portaudio_hello(void)
{
  register int i;
  //int err;

  if(updating_menu_state != 0)	return D_O_K;

  Test();
  //err = Pa_Initialize();
  //if( err != paNoError ) win_printf("cannot init portaudio");

  i = win_printf("Hello from Test_portaudio");
  if (i == CANCEL) win_printf_OK("you have press CANCEL");
  else win_printf_OK("you have press OK");
  return 0;
}








int do_Test_portaudio_rescale_data_set(void)
{
  register int i, j;
  O_p *op = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == CANCEL)	return OFF;

  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_Test_portaudio_rescale_plot(void)
{
  register int i, j;
  O_p *op = NULL, *opn = NULL;
  int nf;
  static float factor = 1;
  d_s *ds, *dsi;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number and place it in a new plot");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("By what factor do you want to multiply y %f",&factor);
  if (i == CANCEL)	return OFF;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  for (j = 0; j < nf; j++)
  {
    ds->yd[j] = factor * dsi->yd[j];
    ds->xd[j] = dsi->xd[j];
  }

  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"y multiplied by %f",factor);
  set_plot_title(opn, "Multiply by %f",factor);
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

MENU *Test_portaudio_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Hello example", do_Test_portaudio_hello,NULL,0,NULL);
  add_item_to_menu(mn,"data set rescale in Y", do_Test_portaudio_rescale_data_set,NULL,0,NULL);
  add_item_to_menu(mn,"plot rescale in Y", do_Test_portaudio_rescale_plot,NULL,0,NULL);
  return mn;
}

int	Test_portaudio_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "Test_portaudio", NULL, Test_portaudio_plot_menu(), 0, NULL);
  return D_O_K;
}

int	Test_portaudio_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "Test_portaudio", NULL, NULL);
  return D_O_K;
}
#endif

