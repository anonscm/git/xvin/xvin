#ifndef _TEST_PORTAUDIO_H_
#define _TEST_PORTAUDIO_H_

PXV_FUNC(int, do_Test_portaudio_rescale_plot, (void));
PXV_FUNC(MENU*, Test_portaudio_plot_menu, (void));
PXV_FUNC(int, do_Test_portaudio_rescale_data_set, (void));
PXV_FUNC(int, Test_portaudio_main, (int argc, char **argv));
#endif

