/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _SDIPAPER_C_
#define _SDIPAPER_C_

#include <allegro.h>
# include "xvin.h"

/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "sdipaper.h"

 //this function takes all the movies in an imreg and simulate a pattern
 int construct_pattern_of_movies(imreg *imr)
 {
   O_i *oi_union=NULL;
   int oi_nx, oi_ny, oi_nf;
   int nb_im_i, nb_im_f;//number of the initial movies, ond total number of movies
   static int n_pix_y, n_pix_y_2, n_pix_x, n_pix_x_2;//about the siez of images
   static int nb_x, nb_y, yc_pix_i;//about the number of lines and rows of the final movie
   static int shift_y,squeeze_x, n_repeat;
   static float bgd;
   int j,k,i,l,m;//index for loops

   win_printf_OK("This plug-in allow you to construct a movie by assemblying and periodically repeating the list\n"
               "of movies loaded in the current image region (must have the same size). There are %d movies in your image region",imr->n_oi);

   if (win_scanf("Specify how many times you want to repeat the current list of movies : %d\n"
               "How many small movies per lines ? %d and how many pixels (from the center) to keep along y %d and x %d \n"
               "specify a shift along y of %d and a squeeze offset along x of %d to optimize the density of beads\n"
               "and the background threshold %f"
               ,&n_repeat, &nb_x, &n_pix_y, &n_pix_x, &shift_y, &squeeze_x, &bgd)==CANCEL) return D_O_K;

   nb_im_i=imr->n_oi;
   nb_im_f= nb_im_i * n_repeat;
   //n_pix_y=imr->o_i[0]->im.ny;
   yc_pix_i=(int) (imr->o_i[0]->im.ny / 2);
   n_pix_y_2 = (int) (n_pix_y / 2);
   nb_y = (int) (nb_im_f / nb_x);//number of lines in the final movie
   if (nb_im_f % nb_x !=0) nb_y++;//add one line in the final movie if the total number of beads is not a multiply of nb_x
   oi_nx= imr->o_i[0]->im.nx + (nb_x) * (imr->o_i[0]->im.nx - squeeze_x);//total number of pixels along x of the final movie
   oi_ny= nb_y * n_pix_y;
   oi_nf=imr->o_i[0]->im.n_f;

   oi_union=create_and_attach_movie_to_imr(imr, oi_nx, oi_ny+shift_y, IS_CHAR_IMAGE, oi_nf);

   for (m=0 ; m< nb_y ; m++)
   {
     for(l=0;l<nb_x;l++)
     {
       if(((l+m*nb_x)%nb_im_i)<imr->n_oi-1)
       {
         for (i=0;i<oi_nf;i++)
         {
           switch_frame(imr->o_i[(l+m*nb_x)%nb_im_i],i);
           switch_frame(oi_union,i);
           for(j=0;j<n_pix_y;j++)
           {
             for(k=0;k<imr->o_i[0]->im.nx;k++)
             {
               oi_union->im.pixel[j + m * n_pix_y + shift_y * (int)(l % 2)].ch[k + l * imr->o_i[0]->im.nx - squeeze_x * (l+1)] +=
                   ((imr->o_i[(l+m*nb_x)%nb_im_i]->im.pixel[j-n_pix_y_2+yc_pix_i].ch[k] > (unsigned char) bgd ) ? imr->o_i[(l+m*nb_x)%nb_im_i]->im.pixel[j-n_pix_y_2+yc_pix_i].ch[k] : 0);

             }
           }
         }
       }
     }
   }

   for (m=0;m<oi_nf;m++)
   {
     switch_frame(oi_union,m);
     for (j=0;j<oi_ny+shift_y;j++)
     {
       for(i=0;i<oi_nx;i++) if (oi_union->im.pixel[j].ch[i]==0) oi_union->im.pixel[j].ch[i]=(unsigned char) bgd;
     }
   }

   find_zmin_zmax(oi_union);
   return 0;
 }
 int do_concatenate_movies(void)
 {
   if (updating_menu_state !=0) return D_O_K;
   O_i *conc_oi = NULL;
   imreg *imr = NULL;
   if(ac_grep(cur_ac_reg, "%im", &imr) != 1) return D_O_K;
   sdi_v2_concatenate_char_movies(imr, conc_oi);
   return 0;
 }

 //this goes to a imreg and concanate movies
 int sdi_v2_concatenate_char_movies(imreg *imr, O_i *conc_oi)
 { // conc_oi hangs in the air !
   int i, j, k, l, nx, ny, frame_offset, nf_conc, n_oi;
   static int first_oi = 0;
   static int last_oi = 0;

   if (imr == NULL) return D_O_K;
   n_oi = imr->n_oi;

   if(win_scanf("this routine concatenes movies of the current image region %d movies.\n"
               "specify from %d to %d (must be same size along x/y dimensions, m1 debug)", &n_oi, &first_oi, &last_oi) == WIN_CANCEL) return D_O_K;

   nx=imr->o_i[first_oi]->im.nx;
   ny=imr->o_i[last_oi]->im.ny;
   for (i=first_oi + 1; i<last_oi ;i++)
   {
     win_printf_OK("i %d", i);
     if (imr->o_i[i]->im.nx != nx || imr->o_i[i]->im.ny != ny)
     {
       win_printf_OK("game over : one of your movies do not have the same x or y dim !");
       return D_O_K;
     }
   }
   nf_conc = 0;
   for (i=first_oi ; i<last_oi ; i++) nf_conc += imr->o_i[i]->im.n_f;
   win_printf_OK("n %d", nf_conc);
   conc_oi = create_and_attach_movie_to_imr(imr, nx, ny, IS_CHAR_IMAGE, nf_conc);
   win_printf_OK("movie alloc ok");
   frame_offset = 0;
   for (i=first_oi ; i<last_oi ; i++)
   {
     for (j=0 ; j<imr->o_i[i]->im.n_f ; j++)
     {
       switch_frame(imr->o_i[i], j);
       switch_frame(conc_oi, j + frame_offset);
       for(k=0 ; k<imr->o_i[i]->im.ny ; k++)
       {
         for (l=0 ; l<imr->o_i[i]->im.nx ; l++) conc_oi->im.pixel[k].ch[l]=imr->o_i[i]->im.pixel[k].ch[l];
       }
     }
     frame_offset += imr->o_i[i]->im.n_f;
   }
   refresh_image(imr, UNCHANGED);
   return 0;
 }


 int estimate_mean(float *tab, int n, float *mean)
 {
   int i;
   double mean_tmp;
   if (tab == NULL || mean == NULL) return 1;
   mean_tmp=0;
   for (i=0;i<n;i++) mean_tmp+=*(tab+i);
   mean_tmp = mean_tmp / n;
   *mean = (float) mean_tmp;
   return 0;
 }
 int estimate_std(float *tab, int n, float *std)
 {
   int i;
   float mean;
   double var_tmp;
   if (tab == NULL || std == NULL) return 1;
   var_tmp=0;
   mean=0;
   estimate_mean(tab,n,&mean);
   for (i=0;i<n;i++) var_tmp += (*(tab+i) - mean) * (*(tab+i) - mean);
   var_tmp = var_tmp / (n-1) ;
   *std = (float) sqrt(var_tmp);
   return 0;
 }
 d_s *extract_pixel_vs_time(O_i *oi, int iy, int ix) //todo: diff data_type of image
 {
   int i;
   d_s *ds=NULL;
   if (ix>=oi->im.nx || iy>=oi->im.ny)
   {
     win_printf_OK("Pixel coordinate exceeds image dimension");
     return NULL;
   }
   ds = build_adjust_data_set(ds, oi->im.n_f, oi->im.n_f);
   for (i=0;i<oi->im.n_f;i++)
   {
     switch_frame(oi,i);
     ds->xd[i] = i;
     ds->yd[i] = oi->im.pixel[iy].ch[ix];
   }
   return ds;
 }

int do_plot_gain(void)
{
  if (updating_menu_state != 0)  return D_O_K;
  int nx, ny;
  imreg *imr = NULL;
  O_i *ois = NULL;
  if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return OFF;
  nx = ois->im.nx;
  ny = ois->im.ny;
  d_s *ds_tmp=NULL;
  d_s *ds_res=NULL;
  ds_res = build_adjust_data_set(ds_res,nx*ny,nx*ny);
  float tmp=0;
  int count = 0;
  for (int i=0;i<ny;i++)
  {
    for (int j=0;j<nx;j++)
    {
      ds_tmp = extract_pixel_vs_time(ois, i, j);
      estimate_mean(ds_tmp->yd, ds_tmp->nx, &ds_res->xd[count]);
      estimate_std(ds_tmp->yd, ds_tmp->nx, &tmp);
      ds_res->yd[count] = tmp * tmp / ds_res->xd[count];
      count ++;
    }
  }
  O_p *op_res=NULL;
  pltreg *plr=NULL;
  create_hidden_pltreg_with_op(&op_res, 1, 1, 0, "res");
  add_data_to_one_plot(op_res, IS_DATA_SET, ds_res);
  set_plot_title(op_res,"camera gain (var over mean) vs mean");
  op_res->need_to_refresh =  1;
  refresh_plot(plr, UNCHANGED);
  return 0;
}

O_i *compute_pix_mean_of_movie(O_i *ois, int x0,int y0, int nx, int ny)
{
  if (ois==NULL) return D_O_K;
  if ( x0>=ois->im.nx || y0>=ois->im.ny || (x0+nx)>ois->im.nx || (y0+ny)>ois->im.ny )
  {
    win_printf_OK("Your ROI (%d,%d,%d,%d) exceeds image dimensions (%d,%d)",x0,y0,nx,ny,ois->im.nx,ois->im.ny);
    return NULL;
  }
  O_i *oid=NULL;
  nx = ois->im.nx;
  ny = ois->im.ny;
  oid = create_one_image(nx, ny, IS_FLOAT_IMAGE);
  d_s *ds_tmp=NULL;
  float avg_tmp=0;
  for (int i=0;i<ny;i++)
  {
    for (int j=0;j<nx;j++)
    {
      ds_tmp = extract_pixel_vs_time(ois, i, j);
      estimate_mean(ds_tmp->yd, ds_tmp->nx, &oid->im.pixel[i].fl[j]);
    }
  }
  return oid;
}

 int do_compute_pix_mean_of_movie(void)
 {
   if (updating_menu_state != 0)  return D_O_K;
   int nx, ny;
   int x0=0;
   int y0=0;
   imreg *imr = NULL;
   union pix *ps = NULL, *pd = NULL;
   O_i *ois = NULL, *oid = NULL;
   if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return OFF;
   nx = ois->im.nx;
   ny = ois->im.ny;
   if (win_scanf("This routine computes the mean value of each pixel over time of the current movie \n"
          "Please specify your ROI: x0=%d y0=%d, nx=%d, ny=%d", &x0,&y0,&nx,&ny) == WIN_CANCEL) return D_O_K;
   oid = compute_pix_mean_of_movie(ois, x0, y0, nx, ny);
   if (oid == NULL) return D_O_K;
   //inherit_from_im_to_im(oid, ois);
   set_im_title(oid, "mean of pix");
   set_oi_treatement(oid, "var of pix");
   add_image(imr, oid->type, (void *)oid);
   find_zmin_zmax(oid);
   return (refresh_image(imr, imr->n_oi - 1));
 }
 int do_compute_pix_variance_of_movie(void)
 {
   if (updating_menu_state != 0)  return D_O_K;
   int nx, ny;
   imreg *imr = NULL;
   union pix *ps = NULL, *pd = NULL;
   O_i *ois = NULL, *oid = NULL;
   if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return OFF;
   nx = ois->im.nx;
   ny = ois->im.ny;
   oid = create_one_image(nx, ny, IS_FLOAT_IMAGE);
   d_s *ds_tmp=NULL;
   float avg_tmp=0;
   for (int i=0;i<ny;i++)
   {
     for (int j=0;j<nx;j++)
     {
       ds_tmp = extract_pixel_vs_time(ois, i, j);
       estimate_std(ds_tmp->yd, ds_tmp->nx, &oid->im.pixel[i].fl[j]);
     }
   }
   //inherit_from_im_to_im(oid, ois);
   set_im_title(oid, "variance of pix");
   set_oi_treatement(oid, "var of pix");
   add_image(imr, oid->type, (void *)oid);
   find_zmin_zmax(oid);
   return (refresh_image(imr, imr->n_oi - 1));
}
int do_extract_pixel_vs_time(void)
 {
   if (updating_menu_state !=0) return D_O_K;
   imreg *imr=NULL;
   O_i *oi=NULL;
   O_p *op=NULL;
   pltreg *plr=NULL;
   if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) < 1)	return D_O_K;
   static int xpix=0;
   static int ypix=0;
   int i;
   d_s *ds=NULL;
   if (win_scanf("choose the coordinates of pixel ligne %d col %d", &ypix, &xpix) == CANCEL) return D_O_K;
   ds = extract_pixel_vs_time(oi, ypix, xpix);
   plr = create_hidden_pltreg_with_op(&op, 1,1,0,"pix vs time");
   add_data_to_one_plot(op, IS_DATA_SET, ds);
   return 0;
 }

do_count_photon_from_movie(O_i *oi, int x0, int y0, int nx, int ny)
{

}

MENU *sdipaper_image_menu(void)
{
  static MENU mn[32];
  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn, "Calibrate gain from movie", do_plot_gain, NULL, 0, NULL);
  add_item_to_menu(mn, "Compute variance of pix of movie", do_compute_pix_variance_of_movie, NULL, 0, NULL);
  add_item_to_menu(mn, "Compute mean of pix of movie", do_compute_pix_mean_of_movie, NULL, 0, NULL);
  add_item_to_menu(mn, "Extract pixel vs time", do_extract_pixel_vs_time, NULL, 0, NULL);
  return mn;
}
MENU *sdipaper_plot_menu(void)
{
  static MENU mn[32];
  if (mn[0].text != NULL)	return mn;
  return mn;
}

int	sdipaper_main(int argc, char **argv)
{
  (void)argc;  (void)argv;
  add_plot_treat_menu_item ( "sdipaper", NULL, sdipaper_plot_menu(), 0, NULL);
  add_image_treat_menu_item( "sdipaper", NULL, sdipaper_image_menu(), 0, NULL);
  return D_O_K;
}

int	sdipaper_unload(int argc, char **argv)
{
  (void)argc;  (void)argv;  remove_item_to_menu(plot_treat_menu, "sdipaper", NULL, NULL);
  return D_O_K;
}
#endif
