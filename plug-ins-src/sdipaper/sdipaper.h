#pragma once


PXV_FUNC(int, sdi_v2_concatenate_char_movies, (imreg *imr, O_i *conc_oi));

PXV_FUNC(int, do_sdipaper_rescale_plot, (void));
PXV_FUNC(MENU*, sdipaper_plot_menu, (void));
PXV_FUNC(int, do_sdipaper_rescale_data_set, (void));
PXV_FUNC(int, sdipaper_main, (int argc, char **argv));
