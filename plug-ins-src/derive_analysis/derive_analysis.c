/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _DERIVE_ANALYSIS_C_
#define _DERIVE_ANALYSIS_C_


#include "xvin.h"
#include "../trackBead/record.h"
#include <allegro.h>
#include <math.h>
/* If you include other regular header do it here*/


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "derive_analysis.h"


int difference_between_one_of_data(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    int i,j,jj;
    int r;
    float mean_x,mean_y,mean_x2,mean_y2,mean_z,mean_z2;
    int page_n,i_page;
    int bd1,bdf=0;
    d_s *ds1=NULL;
    d_s *ds2=NULL;
    d_s *ds=NULL;
    O_p *op = NULL;
    O_p *opn=NULL;
  //  g_record *g_r = NULL;
    b_record *b_r = NULL;
    pltreg *pr=NULL;
    g_record *g_r = NULL;
    int im0,ending,param_cst,profile_invalid;
    float xavg,yavg,zavg,x1,x2,y1,y2;
    int nstart,nend,nf,nfp;
    int av_length=256;
    int nb_pairs=0;
    int to_c=0;
    int fixed_ds;


    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;
    g_r = find_g_record_in_pltreg(pr);
    if (g_r==NULL) return D_O_K;

    nf = g_r->abs_pos;
    nstart=0;
    nend=nf;
    nfp=nend-nstart;

    if (pr->one_p == NULL)		return D_O_K;
    op = pr->one_p;

    i = win_scanf("Please indicate the data tha you want to substract %d\n",&fixed_ds);
    if (i == WIN_CANCEL) return D_O_K;
    if (fixed_ds>=op->n_dat || fixed_ds<0)
    {
      win_printf("There is no such dataset. \n");
      return D_O_K;
    }

    if(read_bead_number_from_ds_source(op->dat[fixed_ds],&bdf)==1)
    {
      win_printf("unable to read bead number from fixed ds\n");
      return D_O_K;
    };



    opn = create_and_attach_one_plot(pr, op->n_dat,op->n_dat, 0);
    if (opn == NULL) return win_printf_OK("Cannot allocate plot");
    ds = opn->dat[0];
    opn->user_id = 0;
    nb_pairs=0;

    for (jj=0;jj<op->n_dat;jj++)
    {
      if (jj==fixed_ds) continue;
      if (jj!=0)
      {
        ds=build_adjust_data_set(NULL,op->dat[fixed_ds]->nx,op->dat[fixed_ds]->nx);
      }

      else
      {
        ds=build_adjust_data_set(ds,op->dat[fixed_ds]->nx,op->dat[fixed_ds]->nx);
      }
      if ((op->dat[jj]->nx<op->dat[fixed_ds]->nx) || (op->dat[jj]->ny<op->dat[fixed_ds]->ny)) continue;

      if(read_bead_number_from_ds_source(op->dat[jj],&bd1)==1)
      {
      win_printf("unable to read bead number from source %d\n",jj);
      continue;
      }

      for (j=0;j<op->dat[fixed_ds]->nx;j++)
      {
        printf("%d %d \n",jj,j);
        ds->yd[j]=op->dat[jj]->yd[j]-op->dat[fixed_ds]->yd[j];
        ds->xd[j]=op->dat[fixed_ds]->xd[j];
      }
      set_ds_source(ds, "Signal of bead %d substracted from signal of bead %d\n", bdf,bd1);
      set_ds_line_color(ds,230);
      set_plot_symb(ds, "\\pt5\\oc ");
      set_ds_dot_line(ds);
      add_ds_to_op(opn,ds);



    }




}


int do_derive_analysis_hello(void)
{

  if(updating_menu_state != 0)	return D_O_K;


  int i,j,jj;
  int r;
  float mean_x,mean_y,mean_x2,mean_y2,mean_z,mean_z2;
  int page_n,i_page;
  int bd1,bd2=0;

  r=win_printf("In order to use this functions, you should first adjust the Y of your datasets\n");

  if (r==WIN_CANCEL) return D_O_K;
  d_s *ds1=NULL;
  d_s *ds2=NULL;
  d_s *ds=NULL;
  O_p *op = NULL;
  O_p *opn=NULL;
//  g_record *g_r = NULL;
  b_record *b_r = NULL;
  pltreg *pr=NULL;
  g_record *g_r = NULL;
  int im0,ending,param_cst,profile_invalid;
  float xavg,yavg,zavg,x1,x2,y1,y2;
  int nstart,nend,nf,nfp;
  int av_length=256;
  int nb_pairs=0;
  int to_c=0;



  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;
  g_r = find_g_record_in_pltreg(pr);

  nf = g_r->abs_pos;
  nstart=0;
  nend=nf;
  nfp=nend-nstart;
//  g_r = find_g_record_in_pltreg(pr);
//  if (g_r == NULL)   {win_printf_OK("No gen record found"); return D_O_K;}
  if (pr->one_p == NULL)		return D_O_K;
  op = pr->one_p;

//create plot with the derive as a function of the distance between the beads

opn = create_and_attach_one_plot(pr, (op->n_dat)*(op->n_dat-1)/2,(op->n_dat)*(op->n_dat-1)/2, 0);
if (opn == NULL) return win_printf_OK("Cannot allocate plot");
ds = opn->dat[0];
opn->user_id = 0;
nb_pairs=0;

  for (i=0;i<op->n_dat;i++)
  {
    for (j=i+1;j<op->n_dat;j++)
{
    to_c=0;
    mean_x=0;
    mean_y=0;
    mean_x2=0;
    mean_y2=0;
    printf("%d %d\n",i,j);
    ds1=op->dat[i];
    ds2=op->dat[j];
    if(read_bead_number_from_ds_source(ds1,&bd1)==1) continue;
    if(read_bead_number_from_ds_source(ds2,&bd2)==1) continue;
    mean_z=0;
    mean_z2=0;
    for (jj=ds1->ny-av_length;jj<ds1->ny;jj++)
    {
      mean_z+=ds1->yd[jj];
      if (jj>ds2->ny)
      {

        to_c=1;
        continue;
      }
      mean_z2+=ds2->yd[jj];
    }
    if (to_c==1) continue;
    mean_z=mean_z/av_length;
    mean_z2=mean_z2/av_length;


    b_r=g_r->b_r[bd1];
    for (jj=nstart;jj<nstart+av_length;jj++) //average x position over the first 256
    {
      page_n=jj / g_r->page_size;
      i_page=jj % g_r->page_size;
      mean_x+=g_r->ax+g_r->dx * b_r->x[page_n][i_page];
      mean_y+=g_r->ay+g_r->dy * b_r->y[page_n][i_page];
    }
    mean_x=mean_x/av_length;
    mean_y=mean_y/av_length;
    b_r=g_r->b_r[bd2];
    for (jj=nstart;jj<nstart+av_length;jj++) //average y position over the first 256
    {
      page_n=jj / g_r->page_size;
      i_page=jj % g_r->page_size;
      mean_x2+=g_r->ax+g_r->dx * b_r->x[page_n][i_page];
      mean_y2+=g_r->ay+g_r->dy * b_r->y[page_n][i_page];
    }
    mean_x2=mean_x2/av_length;
    mean_y2=mean_y2/av_length;
    ds->xd[nb_pairs]=sqrt(pow(mean_y2-mean_y,2)+pow(mean_x2-mean_x,2));
    ds->yd[nb_pairs]=fabs(mean_z-mean_z2);
    nb_pairs+=1;

  }
  }

ds=build_adjust_data_set(ds, nb_pairs, nb_pairs);

set_plot_symb(ds, "\\pt5\\oc ");
alloc_data_set_x_error(ds);
set_ds_dot_line(ds);

set_plot_x_title(opn, "Distance between beads");
set_plot_y_title(opn, "Difference in derive");
create_attach_select_x_un_to_op(opn, IS_METER, 0 , (float)1, -6, 0, "\\mu m");
create_attach_select_y_un_to_op(opn, IS_METER, 0 , (float)1, -6, 0, "\\mu m");


// create plot with the derive as a function of x a
nf = g_r->abs_pos;
nstart=0;
nend=nf;
nfp=nend-nstart;
opn = create_and_attach_one_plot(pr, op->n_dat, op->n_dat, 0);
if (opn == NULL) return win_printf_OK("Cannot allocate plot");
ds = opn->dat[0];
opn->user_id = 0;
//ds->nx=(op->n_dat);
//ds->ny=ds->nx;
nb_pairs=0;

  for (i=0;i<op->n_dat;i++)
  {

    mean_x=0;
    mean_y=0;

    ds1=op->dat[i];

    if(read_bead_number_from_ds_source(ds1,&bd1)==1) continue;
    mean_z=0;
    for (jj=ds1->ny-av_length;jj<ds1->ny;jj++)
    {
      mean_z+=ds1->yd[jj];

    }
    mean_z=mean_z/av_length;


    b_r=g_r->b_r[bd1];
    for (jj=nstart;jj<nstart+av_length;jj++) //average x position over the first 256
    {
      page_n=jj / g_r->page_size;
      i_page=jj % g_r->page_size;
      mean_x+=g_r->ax+g_r->dx * b_r->x[page_n][i_page];

    }
    mean_x=mean_x/av_length;

    ds->xd[nb_pairs]=mean_x;
    ds->yd[nb_pairs]=mean_z;
    nb_pairs+=1;

  }

ds=build_adjust_data_set(ds, nb_pairs, nb_pairs);

set_plot_symb(ds, "\\pt5\\oc ");
alloc_data_set_x_error(ds);
set_ds_dot_line(ds);

set_plot_x_title(opn, "x");
set_plot_y_title(opn, "Derive");
create_attach_select_x_un_to_op(opn, IS_METER, 0 , (float)1, -6, 0, "\\mu m");
create_attach_select_y_un_to_op(opn, IS_METER, 0 , (float)1, -6, 0, "\\mu m");


// create dataset with derive as a difference of y
nf = g_r->abs_pos;
nstart=0;
nend=nf;
nfp=nend-nstart;
opn = create_and_attach_one_plot(pr, opn->dat[0], opn->dat[0], 0);
if (opn == NULL) return win_printf_OK("Cannot allocate plot");
ds = opn->dat[0];
opn->user_id = 0;
nb_pairs=0;

  for (i=0;i<op->n_dat;i++)
  {

    mean_x=0;
    mean_y=0;

    ds1=op->dat[i];

    if(read_bead_number_from_ds_source(ds1,&bd1)==1) continue;
    mean_z=0;
    for (jj=ds1->ny-av_length;jj<ds1->ny;jj++)
    {
      mean_z+=ds1->yd[jj];

    }
    mean_z=mean_z/av_length;


    b_r=g_r->b_r[bd1];
    for (jj=nstart;jj<nstart+av_length;jj++) //average x position over the first 256
    {
      page_n=jj / g_r->page_size;
      i_page=jj % g_r->page_size;
      mean_y+=g_r->ay+g_r->dy * b_r->y[page_n][i_page];

    }
    mean_y=mean_y/av_length;

    ds->xd[nb_pairs]=mean_y;
    ds->yd[nb_pairs]=mean_z;
    nb_pairs+=1;

  }


ds=build_adjust_data_set(ds, nb_pairs, nb_pairs);

set_plot_symb(ds, "\\pt5\\oc ");
alloc_data_set_x_error(ds);
set_ds_dot_line(ds);

set_plot_x_title(opn, " y");
set_plot_y_title(opn, "Derive");
create_attach_select_x_un_to_op(opn, IS_METER, 0 , (float)1, -6, 0, "\\mu m");
create_attach_select_y_un_to_op(opn, IS_METER, 0 , (float)1, -6, 0, "\\mu m");

  return D_O_K;
}


MENU *derive_analysis_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Draw derive as a function of x and y", do_derive_analysis_hello,NULL,0,NULL);
  add_item_to_menu(mn," Differences",   difference_between_one_of_data,NULL,0,NULL);
  return mn;
}

int	derive_analysis_main(int argc, char **argv)
{
  (void)argc;  (void)argv;  add_plot_treat_menu_item ( "Derive Analysis", NULL, derive_analysis_plot_menu(), 0, NULL);
  return D_O_K;
}

int	derive_analysis_unload(int argc, char **argv)
{
  (void)argc;  (void)argv;  remove_item_to_menu(plot_treat_menu, "Derive Analysis", NULL, NULL);
  return D_O_K;
}
#endif
