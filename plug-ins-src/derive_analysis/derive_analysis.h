#pragma once
PXV_FUNC(int, do_derive_analysis_rescale_plot, (void));
PXV_FUNC(MENU*, derive_analysis_plot_menu, (void));
PXV_FUNC(int, do_derive_analysis_rescale_data_set, (void));
PXV_FUNC(int, derive_analysis_main, (int argc, char **argv));
