/** \file nidaqmxwrap_AIAO_waveform.
    
    \author Francesco Mosconi
    
    \version 26/04/07
*/
#ifndef NIDAQMXWRAP_AIAO_WAVEFORM_C_
#define NIDAQMXWRAP_AIAO_WAVEFORM_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "nidaqmxwrap_AIAO.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL


////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Square function.

\param float x The independent variable
\param void *p This is the duty cycle. It is cast on a float and it indicates the ratio of 'up' to 'up+down'.
\return float64 the function value

\author Francesco Mosconi
\version 21/03/07
*/    
float64 waveform__square_function(float x,w_p *par)
{
  float p = 0.;
  float d = 0.5;
  float o = 0.;
  float a = 1.;
  int   s = 1;

  if(par != NULL){
    p = par->p;
    d = par->d;
    o = par->o;
    a = par->a;
    s = par->s;
  }
  if(((x*s - p) - floor(x*s - p)) <= d) return o + a;
  
  return o - a;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Triangle function.

\param float x The independent variable
\param void *p This is the duty cycle. It is cast on a float and it indicates the ratio of 'upward' to the period length.
\return float64 the function value

\author Francesco Mosconi
\version 21/03/07
*/    
float64 waveform__triangle_function(float y,w_p *par)
{
  float x = 0.;
  float p = 0.;
  float d = 0.5;
  float o = 0.;
  float a = 1.;
  int   s  = 1;

  if(par != NULL){
    p = par->p;
    d = par->d;
    o = par->o;
    a = par->a; 
    s = par->s;
  }
  x = ((y*s - p) - floor(y*s - p));
  
  if(x <= d) 
    return (float64) o + a*(-1 + 2./d*x);
  
  return (float64) o + a*(3 - 2./(1 - d)*x);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Sine function.

\param float x The independent variable
\param void *p This is NULL;
\return float64 sin(x)

\author Francesco Mosconi
\version 21/03/07
*/    
float64 waveform__sin_function(float x,w_p *par)
{
  float p = 0.;
  float o = 0.;
  float a = 1.;
  int   s  = 1;
 
  if(par != NULL){
    p = par->p;
    o = par->o;
    a = par->a; 
    s = par->s;
  }

  return (float64) o + a*sin(2*PI*x*s - p);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Cosine function
    
\param float x The independent variable
\param void *p This is NULL;
\return float64 cos(x)

\author Francesco Mosconi
\version 21/03/07
*/    
float64 waveform__cos_function(float x,w_p *par)
{
  float p = 0.;
  float o = 0.;
  float a = 1.;
  int   s  = 1;

  if(par != NULL){
    p = par->p;
    o = par->o;
    a = par->a; 
    s = par->s;
  }

  return (float64) o + a*cos(2*PI*x*s - p);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Constant function
    
\param float x The independent variable
\param void *p This is NULL;
\return float64 1

\author Francesco Mosconi
\version 21/03/07
*/    
float64 waveform__const_function(float x,w_p *par)
{
  float o = 0.;

  if(par != NULL)
    o = par->o;
    
  return o;
}

#endif





