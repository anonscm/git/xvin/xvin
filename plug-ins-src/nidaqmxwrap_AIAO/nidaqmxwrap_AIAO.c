/*    Wrapper Plug-in for Continuous Synchronous Analog and Digital input in Xvin winth NIDAQMX.
 *
 *    F. Mosconi
  */
#ifndef _NIDAQMXWRAP_AIAO_C_
#define _NIDAQMXWRAP_AIAO_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
# include "../logfile/logfile.h"
#include <NIDAQmx.h>

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "nidaqmxwrap_AIAO.h"


d_s *create_xy_from_theta(d_s *dsth, d_s *dsxy)
{
  register int i = 0;
  int np = dsth->nx;
  if(dsth == NULL)
    return win_printf_ptr("dsth = NULL");

  dsxy = build_adjust_data_set(dsxy,2*np,2*np);

  for(i = 0 ; i < np ; i++){
    dsxy->xd[2*i] = dsxy->xd[2*i+1] = dsth->xd[i];
    dsxy->yd[2*i] = cos(dsth->yd[i]);
    dsxy->yd[2*i+1] = sin(dsth->yd[i]);
    dsxy->nx += 2;
    dsxy->ny += 2;
  } 
  return dsxy;
}

d_s *create_xy_from_theta_notime(d_s *dsth, d_s *dsxy)
{
  register int i = 0;
  int np = dsth->nx;
  if(dsth == NULL)
    return win_printf_ptr("dsth = NULL");

  dsxy = build_adjust_data_set(dsxy,np,np);

  for(i = 0 ; i < np ; i++){
    dsxy->xd[i] = cos(dsth->yd[i]);
    dsxy->yd[i] = sin(dsth->yd[i]);
    dsxy->nx += 1;
    dsxy->ny += 1;
  } 
  return dsxy;

}
d_s *add_first_np_to_xy_from_theta_notime(int np, O_p *op, d_s *ds, float phase, int direction)
{
  register int i = 0;
  int n = ds->nx;
  d_s *ds1 = NULL;
  
  ds1 = create_and_attach_one_ds(op,np,np,0);

  for(i = 0 ; i < np ; i++){
    ds1->xd[i] = cos(phase + direction*ds->yd[i%n]);
    ds1->yd[i] = sin(phase + direction*ds->yd[i%n]);
  }

  return ds1;
}


d_s *create_abc_from_xy(d_s *dsxy, d_s *dsabc)
{
  register int i = 0;
  int np = dsxy->nx/2;
  if(dsxy == NULL)
    return win_printf_ptr("dsxy = NULL");

  dsabc = build_adjust_data_set(dsabc,3*np,3*np);

  for(i = 0 ; i < np ; i++){
    dsabc->xd[3*i] = dsabc->xd[3*i+1] = dsabc->xd[3*i+2] = dsxy->xd[2*i];
    dsabc->yd[3*i] = dsxy->yd[2*i] * AmatrTrans[0] + dsxy->yd[2*i + 1] * AmatrTrans[3];
    dsabc->yd[3*i+1] = dsxy->yd[2*i] * AmatrTrans[1] + dsxy->yd[2*i + 1] * AmatrTrans[4];
    dsabc->yd[3*i+2] = dsxy->yd[2*i] * AmatrTrans[2] + dsxy->yd[2*i + 1] * AmatrTrans[5];
;
    dsabc->nx += 3;
    dsabc->ny += 3;
  } 
  return dsabc;

}

///test function for different arrays
int test(void)
{
  int i=0,j=0, n=0, k = 0,N,P;
  pltreg *pr;
  O_p *opmem = NULL, *oprot = NULL, *opvec = NULL;
  d_s *ds = NULL,*dssig = NULL, *ds1 = NULL , *ds2 = NULL, *ds3 = NULL , *dsbf = NULL , *dsccw = NULL, *dsxyccw = NULL, *dsabcccw = NULL,*dsrot = NULL, *dst = NULL, *dsxy = NULL;
  static int direction =1, np = 300000, bf = 100, sync = 1;
  static float level = 1., n_rota = 0, fs = 100000., f1 = 1000., f2 = 100., frot = 1., angle = 0.,dc = 0.5;
  float r = 1., f2a, f2b, rot = 1., r0 = 0.,m = 0.;
  int np1 = 128, np2, np3 =128, nprot = 128, ofi=0, move = 0, jo=0;

  if(updating_menu_state != 0) return D_O_K;


  if(win_scanf("np = %5d, fs = %5fHz\n"
	    "f1 = %5f Hz, f2 = %5f Hz\n"
	    "dc = %5f\n"
	    "frot = %5f Hz\n"
	    "bf = %5d\n"
	    ,&np,&fs,&f1,&f2,&dc,&frot,&bf) == CANCEL)
    return D_O_K;

  //first create a new project for the nidaqmx plug-in. this will be called at init
  //first define a plot with the angles and phases for the fast rotation.
  pr = create_hidden_pltreg_with_op(&opmem, np, np, 0,"test rotating field allocated structures");
  if (pr == NULL)  win_printf_OK("Could not find or allocate plot region!");

  set_plot_title(opmem,"Fast rotation");
  set_plot_x_title(opmem, "Time (s)");

  if((dssig = opmem->dat[0])==NULL)
    return(win_printf("ds == NULL"));
  set_ds_source(dssig, "Complete signal");
  if((dsrot = create_and_attach_one_ds(opmem,np,np,0))==NULL)
    return(win_printf("dsrot == NULL"));
  set_ds_source(dsrot, "Phase of rotation");

  //this is the dataset for the fast rotating field angle
  np1 = (int) fs/f1;
  if((ds1 = create_and_attach_one_ds(opmem,np1,np1,0))==NULL)
    return(win_printf("ds1 == NULL"));
  set_ds_source(ds1, "fast rotating angle");
  for(i = 0 ; i < np1 ; i++){
    ds1->xd[i] = (float)i/fs;
    ds1->yd[i] = (float) 2 * PI * f1/fs * i;
  }

  //this is the dataset for the apolisation modulation
  f2a = f2/dc;
  f2b = f2/(1-dc);
  np2 = (int) fs/f2;
  if((ds2 = create_and_attach_one_ds(opmem,np2,np2,0))==NULL)
    return(win_printf("ds2a == NULL"));
  set_ds_source(ds2, "apolisation modulation");
  for(i = 0 ; i < np2 ; i++){
    if(((float)i/np2)< dc){
      ds2->xd[i] = (float)i/fs;
      ds2->yd[i] = 1-cos(2*PI *f2a/fs * i);
    }
    else{
      ds2->xd[i] = (float)i/fs;
      ds2->yd[i] = 1-cos(2*PI *f2b/fs * (i-(dc*np2)));
    }
  }

  //then I define a plot for the continuous rotation

  nprot = (int) fs/frot;
  if((oprot = create_and_attach_one_plot(pr,nprot,nprot,0))==NULL)
    return(win_printf("can't create and attach one plot"));
  set_plot_title(oprot,"Slow rotation");
  set_plot_x_title(oprot, "time (s)");			
  set_plot_y_title(oprot, "Angle (rad)");

  dsccw = oprot->dat[0];
  set_ds_source(dsccw, "ccw rot angle");
  for(i = 0 ; i < nprot ; i++){
    dsccw->xd[i] = (float)i/fs;
    dsccw->yd[i] = 2*PI * frot/fs * i;
  }

  //now I define a plot with the vector field in XY space for one full apolisation cycle

  if((opvec = create_and_attach_one_plot(pr,np2,np2,0))==NULL)
    return(win_printf("can't create and attach one plot"));
  set_plot_title(opvec,"XY vector components");
  dsxy = opvec->dat[0];
  set_ds_source(dsxy, "full fast rotation cycle");
  for(i = 0, k = 0 ; i < np2 ; i++){
    if(((float)i/np2)< dc){
      dsxy->xd[i] = ds2->yd[i%np2] * cos(ds1->yd[k%np1]);
      dsxy->yd[i] = ds2->yd[i%np2] * sin(ds1->yd[k%np1]);
      k++;
    }
    else if(((float)i/np2) >= dc){
      dsxy->xd[i] = ds2->yd[i%np2] * cos(ds1->yd[k%np1]);
      dsxy->yd[i] = ds2->yd[i%np2] * sin(ds1->yd[k%np1]);
      k--;
    }
  }

  // and I add three datasets to plot the axes of the coils
  if((ds = create_and_attach_one_ds(opvec,100,100,0))==NULL)
    return(win_printf("ds axe a == NULL"));
  set_ds_source(ds, "axis a");
  for(i = 0 ; i < 100 ; i++){
    ds->xd[i] = i*axis_a[0]/100;
    ds->yd[i] = i*axis_a[1]/100;
  }
  if((ds = create_and_attach_one_ds(opvec,100,100,0))==NULL)
    return(win_printf("ds axe b == NULL"));
  set_ds_source(ds, "axis b");
  for(i = 0 ; i < 100 ; i++){
    ds->xd[i] = i*axis_b[0]/100;
    ds->yd[i] = i*axis_b[1]/100;
  }
  if((ds = create_and_attach_one_ds(opvec,100,100,0))==NULL)
    return(win_printf("ds axe c == NULL"));
  set_ds_source(ds, "axis c");
  for(i = 0 ; i < 100 ; i++){
    ds->xd[i] = i*axis_c[0]/100;
    ds->yd[i] = i*axis_c[1]/100;
  }

  dsxyccw = create_xy_from_theta_notime(dsccw,NULL);
  add_ds_to_op(opvec,dsxyccw);


  //I want the various options, sync, async,
  //and I want to be able to control the general amplitude.

  if(win_scanf("%R async, %r sync\n"
	       "old r_0 = %5f\n"
	       "new r_0 = %5f\n"
	       ,&sync,&r0,&r) == CANCEL)
    return D_O_K;
  
  if((dsbf = create_and_attach_one_ds(opmem,2*bf,2*bf,0))==NULL)
    return(win_printf("dsbf == NULL"));
  set_ds_source(dsbf, "vectorbuffer dataset");
  
  m = r - r0;
  jo = 0.5 + r0 * nprot;
  jo = jo%nprot;
  jo  = (jo < 0) ? jo + nprot : jo;
  jo  = (jo >= nprot) ? jo - nprot : jo;
  k = jo;
  if(sync){
    for(i = 0 ; i < np ; i++){
      dsbf->xd[i%bf] = (float)i/fs;
 
      if(i%2 == 0)
	dsbf->yd[i%bf] = dsxy->yd[(i/2)%np2];
      else
	dsbf->yd[i%bf] = dsxy->xd[(i/2)%np2];

      dsrot->xd[i%np] = (float)i/f2;
      dsrot->yd[i%np] = dsccw->yd[k%nprot];
      
      dssig->xd[i%np] = (float)i/fs;
      dssig->yd[i%np] = dsbf->yd[i%bf];

      if(m > 0){
	P = 0.5 + m * nprot;
	if(i < P)
	  k = (k + 1) >= nprot ? 0 : k+1;
	}
      else if(m < 0){
	P = 0.5 - m * nprot;
	if(i < P)
	  k = (k - 1) < 0 ? nprot-1 : k - 1;
      }

    }
  }
  //else{
      
  //}


  opmem->need_to_refresh |= 1;
  opvec->need_to_refresh |= 1;
  refresh_plot(pr, UNCHANGED);
  
  return D_O_K;
}

// Control sequences

/** Rotating magnetic field.

All lines are sinus-modulated each with a phase shift of 2PI/3.

\author Francesco Mosconi
\version 12/02/2070
*/
int rotation__rotating_B_all_interface(void)
{
  int ret = 0;
  if(updating_menu_state != 0){
    add_keyboard_short_cut(0, KEY_ENTER, 0, rotation__rotating_B_all_interface); 
    return D_O_K;
  }
  
  if((ret = rotation__set_new_rfp(grf)) == CANCEL) return win_printf("ret = %d", ret);
  if(grf->stop_and_restart){
    grf->stop_and_restart = 0;
    task__stop_and_restart();
  }
  else if((grf->stop_and_restart == 0 ) && grf->update){
    grf->update = 0;
    newGrfReady = 1;
  }
  else
    win_printf("something wrong because stop and restar = 0 and grf->update = 0!");
  return D_O_K;
}

int magnets__finite_rotation(int nt)
{
  frnt = nt;
  task__finite_rotation_field_task(frfreq, frlevel, frnt);
  return 0;
}

int magnets__finite_rotation_no_stop_interface(void)
{
  float nt = 1;
  if(updating_menu_state != 0) return D_O_K;
  if(win_scanf("number of turns %5f\n",&nt)==CANCEL) return D_O_K;
  magnets__finite_rotation_no_stop(nt);
  return 0;
}

int magnets__finite_rotation_no_stop(float rot_move)
{
  rotp += task_grf->ed.rotnp * rot_move;
  return 0;
}

int set_sync(void)
{
  if(updating_menu_state != 0) return D_O_K;
  if(win_scanf("%R async %r sync\n",&sync)==CANCEL) return D_O_K;
  return 0;
}

int magnets__set_fixed_level(float level)
{
  fixed_level = level;
  return 0;
}

int set_fixed_level_interface(void)
{
  float level = fixed_level;
  if(updating_menu_state != 0) return D_O_K;
  if(win_scanf("level %5f\n",&level)==CANCEL) return D_O_K;
  magnets__set_fixed_level(level);
  return 0;
}

int magnets__set_global_level(float level)
{
  global_level = level;
  return 0;
}

int set_global_level_interface(void)
{
  float level = global_level;
  if(updating_menu_state != 0) return D_O_K;
  if(win_scanf("level %5f\n",&level)==CANCEL) return D_O_K;
  magnets__set_global_level(level);
  return 0;
}

int finite_rotation_interface(void)
{
  if(updating_menu_state != 0){
    add_keyboard_short_cut(0, KEY_SPACE, 0, finite_rotation_interface);    return D_O_K;
  }

  if(win_scanf("level %5f\n"
	    "number of turns %5d\n"
	    "freq %5f Hz\n"
	    ,&frlevel
	    ,&frnt
	    ,&frfreq
	       )==CANCEL) return D_O_K;
  task__stop();
  task__finite_rotation_field_task(frfreq, frlevel, frnt);
  task__create_and_launch();
  return D_O_K;
}

int set_myBufferRegen(void)
{
   if(updating_menu_state != 0) return D_O_K;

   if(win_scanf("Buffer Regeneration: %R automatic %r myBufferRegen\n",&myBufferRegen)==CANCEL) return D_O_K;
   task__stop_and_restart();
   return D_O_K;
}

MENU *nidaqmxwrap_AIAO_shortcut_menu(void)
{
  static MENU mn[32];
  /* Useless : only here to be constantly evaluated */
  add_item_to_menu(mn,"Shortcut",short__KEY_PLUS_PAD,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_MINUS_PAD,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_P,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_L,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_1_PAD,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_2_PAD,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_3_PAD,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_4_PAD,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_5_PAD,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_6_PAD,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_7_PAD,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_8_PAD,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_9_PAD,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_G,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_O,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_F,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_C,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_R,NULL,0,NULL);
  add_item_to_menu(mn,"Shortcut",short__KEY_0_PAD,NULL,0,NULL);
  return mn;
}

MENU *nidaqmxwrap_AIAO_useless_menu(void)
{
  static MENU mn[32];
  add_item_to_menu(mn,"set myBufferRegen",set_myBufferRegen,NULL,0,NULL);
  add_item_to_menu(mn,"\0",     NULL, NULL, 0, NULL);
  add_item_to_menu(mn,"plot cir",rotation__plot_cir,NULL,0,NULL);
  add_item_to_menu(mn,"plot earr",rotation__plot_earr,NULL,0,NULL);
  add_item_to_menu(mn,"display rotating field",display__rotating_field,NULL,0,NULL);
  return mn;
}

MENU *nidaqmxwrap_AIAO_menu(void)
{
  static MENU mn[32];
  
  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"test",test,NULL,0,NULL);
  add_item_to_menu(mn,"set global level",set_global_level_interface,NULL,0,NULL);
  add_item_to_menu(mn,"set fixed level",set_fixed_level_interface,NULL,0,NULL);
  add_item_to_menu(mn,"set sync",set_sync,NULL,0,NULL);
  add_item_to_menu(mn,"shortcut", NULL, nidaqmxwrap_AIAO_shortcut_menu(),0,NULL);
  add_item_to_menu(mn,"useless", NULL, nidaqmxwrap_AIAO_useless_menu(),0,NULL);
  add_item_to_menu(mn,"\0",     NULL, NULL, 0, NULL);
  add_item_to_menu(mn,"B",rotation__rotating_B_all_interface,NULL,0,NULL);
  add_item_to_menu(mn,"finite rotation",finite_rotation_interface,NULL,0,NULL);
  add_item_to_menu(mn,"finite rotation no stop",magnets__finite_rotation_no_stop_interface,NULL,0,NULL);
  add_item_to_menu(mn,"start",task__stop_and_restart,NULL,0,NULL);
  add_item_to_menu(mn,"stop",task__stop,NULL,0,NULL);
  add_item_to_menu(mn,"stop and reset to zero",task__stop_and_reset_to_zero,NULL,0,NULL);
  return mn;
}

int	nidaqmxwrap_AIAO_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "nidaqmxwrap AIAO", NULL, nidaqmxwrap_AIAO_menu(), 0, NULL);
  add_image_treat_menu_item ( "nidaqmxwrap AIAO", NULL, nidaqmxwrap_AIAO_menu(), 0, NULL);
  rotation__rfp_functions_main();
  task__new_rfp_to_AOdata();
  return D_O_K;
}

int	nidaqmxwrap_AIAO_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "nidaqmxwrap_AIAO", NULL, NULL);
  remove_item_to_menu(image_treat_menu, "nidaqmxwrap_AIAO", NULL, NULL);
  task__stop();
  return D_O_K;
}
#endif

