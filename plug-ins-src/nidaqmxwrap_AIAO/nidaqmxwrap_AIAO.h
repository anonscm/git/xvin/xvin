#ifndef _NIDAQMXWRAP_AIAO_H_
#define _NIDAQMXWRAP_AIAO_H_

#include "nidaqmxwrap_AIAO_defs.h"
#include "nidaqmxwrap_AIAO_gvars.h"

//exported functions
PXV_FUNC(int, rotation__rotating_B_all_interface,(void));
PXV_FUNC(int, magnets__finite_rotation,(int nt));
PXV_FUNC(int, magnets__finite_rotation_no_stop_interface,(void));
PXV_FUNC(int, magnets__finite_rotation_no_stop,(float rot_move));
PXV_FUNC(int, magnets__set_global_level,(float level));
PXV_FUNC(int, set_global_level_interface,(void));
PXV_FUNC(int, finite_rotation_interface,(void));
PXV_FUNC(int, set_myBufferRegen,(void));
PXV_FUNC(MENU, *nidaqmxwrap_AIAO_shortcut_menu,(void));
PXV_FUNC(MENU, *nidaqmxwrap_AIAO_useless_menu,(void));
PXV_FUNC(MENU, *nidaqmxwrap_AIAO_menu,(void));
PXV_FUNC(int,	nidaqmxwrap_AIAO_main,(int argc, char **argv));
PXV_FUNC(int,	nidaqmxwrap_AIAO_unload,(int argc, char **argv));

//from matrixutil
PXV_FUNC(int, my_2d_rotation,(float *input_v,float *output_v,float angle));
PXV_FUNC(int, my_2d_ellipse,(float *input_v,float *output_v,float ell));
PXV_FUNC(int, my_2d_rotellipse,(float *input_v,float *output_v,float ell,float angle));
PXV_FUNC(float, my_scalar_product,(float *v_1,float *v_2));
PXV_FUNC(int, my_matrix_product,(float64 prefactor, float64 *m1, int m1sz1, int m1sz2, float64 *m2, int m2sz1, int m2sz2, float64 *mout));

// From waveform
PXV_FUNC(float64, waveform__square_function,(float x,w_p *par));
PXV_FUNC(float64, waveform__triangle_function,(float y,w_p *par));
PXV_FUNC(float64, waveform__sin_function,(float x,w_p *par));
PXV_FUNC(float64, waveform__cos_function,(float x,w_p *par));
PXV_FUNC(float64, waveform__const_function,(float x,w_p *par));

//from rotation
PXV_FUNC(int, rotation__copy_rfpe,(rfpe *source, rfpe *dest));
PXV_FUNC(int, rotation__copy_rfpea,(rfpea *source, rfpea *dest));
PXV_FUNC(int, rotation__copy_rfpc,(rfpc *source, rfpc *dest));
PXV_FUNC(int, rotation__copy_new_rfp,(new_rfp *source, new_rfp *dest));
PXV_FUNC(int, rotation__calculate_ellipse_matrix,(rfpe *e));
PXV_FUNC(int, rotation__calculate_earr_matrix,(rfpea *ea));
PXV_FUNC(int, rotation__calculate_rfpc_matrix,(rfpc *c));
PXV_FUNC(int, rotation__check_rfpc,(rfpc *c));
PXV_FUNC(int, rotation__check_new_rfp,(new_rfp *rf));
PXV_FUNC(int, rotation__init_rfpea,(rfpea *e));
PXV_FUNC(int, rotation__resize_rfpea,(rfpea *ea,int  np));
PXV_FUNC(int, rotation__free_rfpea,(rfpea *e));
PXV_FUNC(int, rotation__init_rfpc,(rfpc *c));
PXV_FUNC(int, rotation__init_new_rfp,(new_rfp *rf));
PXV_FUNC(int, rotation__plot_cir,(void));
PXV_FUNC(int, rotation__set_new_rfp,(new_rfp *rf));
PXV_FUNC(int, rotation__plot_earr,(void));
PXV_FUNC(int, plot_data,(float64 *data,int np));
PXV_FUNC(int, rotation__add_next_nSamples_earr_contribution,(new_rfp *rf, float64 *data, int nSamples));
PXV_FUNC(int, rotation__rfp_functions_main,(void));

//From Task
PXV_FUNC(int, task__new_rfp_to_AOdata,(void));
PXV_FUNC(int32, CVICALLBACK task__EveryNRefreshBufferCallback,(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData));
PXV_FUNC(int, task__allocate_memory_space,(int ns));
PXV_FUNC(int, task__reallocate_memory_space,(int ns));
PXV_FUNC(int, task__free_memory_space,(void));
PXV_FUNC(int, task__static_field_task,(float level, float angle));
PXV_FUNC(int, task__generate_finite_rotation_data,(void));
PXV_FUNC(int, task__finite_rotation_field_task,(float freq, float level, int num_turns));
PXV_FUNC(int, task__create_and_launch,());
PXV_FUNC(int, task__stop,(void));
PXV_FUNC(int, task__stop_and_reset_to_zero,(void));
PXV_FUNC(int, task__stop_and_restart,(void));

//From display
PXV_FUNC(int, display__dataAO_plot_idle_action,(O_p *op, DIALOG *d));
PXV_FUNC(int, display__rotating_field,(void));
PXV_FUNC(int, display__rotating_field_stop,(void));

//From Shortcuts
PXV_FUNC(int, display_values_to_screen,(void));
PXV_FUNC(int, short__KEY_PLUS_PAD,(void));
PXV_FUNC(int, short__KEY_MINUS_PAD,(void));
PXV_FUNC(int, short__KEY_P,(void));
PXV_FUNC(int, short__KEY_L,(void));
PXV_FUNC(int, short__KEY_R,(void));
PXV_FUNC(int, short__KEY_1_PAD,(void));
PXV_FUNC(int, short__KEY_2_PAD,(void));
PXV_FUNC(int, short__KEY_3_PAD,(void));
PXV_FUNC(int, short__KEY_4_PAD,(void));
PXV_FUNC(int, short__KEY_7_PAD,(void));
PXV_FUNC(int, short__KEY_5_PAD,(void));
PXV_FUNC(int, short__KEY_8_PAD,(void));
PXV_FUNC(int, short__KEY_6_PAD,(void));
PXV_FUNC(int, short__KEY_9_PAD,(void));
PXV_FUNC(int, short__KEY_G,(void));
PXV_FUNC(int, short__KEY_O,(void));
PXV_FUNC(int, short__KEY_F,(void));
PXV_FUNC(int, short__KEY_C,(void));
PXV_FUNC(int, short__KEY_0_PAD,(void));
PXV_FUNC(int,	shortcuts_main,(void));

//from config
PXV_FUNC(int, config__open,(void));
PXV_FUNC(int, config__close,(void));
PXV_FUNC(int, config__write,(new_rfp *rf));
PXV_FUNC(int, config__rf_set,(new_rfp *rf));
PXV_FUNC(int, config__es_set,(new_rfp *rf));
PXV_FUNC(int, config__set,(new_rfp *rf));
PXV_FUNC(int, config__rf_load,(new_rfp *rf));
PXV_FUNC(int, config__es_load,(new_rfp *rf));
PXV_FUNC(int, config__load,(new_rfp *rf));


#endif

