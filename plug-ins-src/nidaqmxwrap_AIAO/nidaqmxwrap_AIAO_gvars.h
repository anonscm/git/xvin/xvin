/** global variables declarations for the plug-in

\author Francesco Mosconi
*/
 
#ifndef _NIDAQMXWRAP_AIAO_GVARS_H_
#define _NIDAQMXWRAP_AIAO_GVARS_H_

/*********************************************************/
/**                 from matrixutils                    **/
/*********************************************************/
#ifdef _NIDAQMXWRAP_AIAO_MATRIXUTIL_C_

/** Definition of the vectors a,b,c 
   used as coordinates corresponding to the three
   currents. the vectors are defined as
   a = [cos(PI/6), sin(PI/6]
   b = [cos(PI/2), sin(PI/2]
   a = [cos(5*PI/6), sin(5*PI/6]
   The coordinates of a unit vector forming an angle theta 
   with the x axis are defined in this system as:
   v(theta) = 2/3 [cos(theta - PI/6),cos(theta - PI/2),cos(theta - 5*PI/6)]
   thus, for example, the a axis has coordinates a = [2/3, 1/3, -1/3]
*/

const float axis_a[2] = {0.866025/*40378443864676372317075294*/ , 0.5};
const float axis_b[2] = {0. , 1.};
const float axis_c[2] = {-0.866025/*40378443864676372317075294*/ , 0.5};

/**Definition of some matrices and vectors useful
   to go back and forth from the cartesian coords x,y 
   to the non cartesian a,b,c coords
   all matrices are arranged as A_{ij} = A[i*dim+j]={a_11,a_12,a_13,...}
*/
//the following is a 3x2 matrix to go from x,y to a,b,c
//it acts on cartesian coordinates
const float64 Amatr[6] = {0.5773502691896257645, 0.333333333,
			  0., 0.666666666,
			  -0.5773502691896257645, 0.333333333};
///this is the transpose of the A matrix, and it is 2x3
const float64 AmatrTrans[6] = {0.5773502691896257645, 0., -0.5773502691896257645,
			       0.333333333, 0.666666666, 0.333333333};
///the following is a 2x3 matrix to go from a,b,c to x,y
///it acts on a,b,c coordinates
const float64 Rmatr[6] = {-0.866025, 0., 0.866025,
			  0.5, 1., 0.5} ;
///this is the transpose of the R matrix, and it is 3x2
///the following is equal to the product AR
const float64 RmatrTrans[6] = {-0.866025, 0.5,
			       0., 1.,
			       0.866025, 0.5} ;
const float64 Nmatr[9] = {0.666666666, 0.333333333, -0.333333333,
			  0.333333333, 0.666666666, 0.333333333,
			  -0.333333333, 0.333333333, -0.666666666} ;
#else
extern const float axis_a[2];
extern const float axis_b[2];
extern const float axis_c[2];
extern const float64 Amatr[6];
extern const float64 AmatrTrans[6];
extern const float64 Rmatr[6];
extern const float64 RmatrTrans[6];
extern const float64 Nmatr[9];
#endif

/*********************************************************/
/**                 from rotation                       **/
/*********************************************************/
#ifdef _NIDAQMXWRAP_AIAO_ROTATION_C_
/** grf is used 

*/
new_rfp *grf;
/** task_grf is used 

*/
new_rfp *task_grf;
/** pending_task_grf is used 

*/
new_rfp *pending_task_grf;
#else
extern new_rfp *grf, *task_grf, *pending_task_grf;
#endif

/*********************************************************/
/**                 from task                           **/
/*********************************************************/
#ifdef _NIDAQMXWRAP_AIAO_TASK_C_
int callbackcounter = 0;
int sync = 0;
int doRotation = 0;
int rotp = 0;
int nprot = 0;
float64 frot = 1.;
int frnt = 1;
float frlevel = 0.03, frfreq = 1. /*Hz*/;
static TaskHandle  AOtaskHandle=0;
float64     *AOdata = NULL, *dst = NULL, *cw = NULL, *ccw = NULL;
int taskRunning = 0;
int taskStillRunning = 0;
int copyDataToOp = 0;
int copyAverageToOp = 0;
int copyDataToHystOp = 0;
int AOdataFlag = 0;
int32  totalAOdataWritten = 0;
int32 totalAIdataRead = 0;
int newDataReady = 0;
int newGrfReady = 0;
int myBufferRegen = 1;
float64 task__smplFreq = 1;
float64 task__signalFreq = 1;
int task__nSamples = 2;
int task__nSamplesEveryN = 1;
O_p *task__gop = NULL;
O_p *task__avgop = NULL;
float rbest[3] = {-21.3652,-22.2363,-21.8359};
int number_of_periods_to_average = 1;
int count[NUMBER_OF_AI_LINES] = {0,0,0} ;
float global_level = 1.;
float fixed_level = 0.01;

#else
extern int callbackcounter;
extern int sync;
extern int doRotation;
extern int rotp;
extern int nprot;
extern float64 frot;
extern int frnt;
extern float frlevel, frfreq;
extern float64 *AOdata, *dst, *ccw, *cw;
extern int taskRunning;
extern int taskStillRunning;
extern int copyDataToOp;
extern int copyAverageToOp;
extern int copyDataToHystOp;
extern int AOdataFlag;
extern int32  totalAOdataWritten;
extern int newDataReady;
extern int newGrfReady;
extern int myBufferRegen;
extern float64 task__smplFreq;
extern float64 task__signalFreq;
extern int task__nSamples;
extern int task__nSamplesEveryN;
extern O_p *task__gop;
extern O_p *task__avgop;
extern float rbest[3];
extern int number_of_periods_to_average;
extern int count[NUMBER_OF_AI_LINES];
extern float global_level;
extern float fixed_level;
#endif

#endif



