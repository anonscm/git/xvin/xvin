/** \file nidaqmxwrap_AIAO_rotation.c
    \brief rotation control with NIDAQ-M 6229 in Xvin.

    \author Francesco Mosconi
    \version 04/06/2007
*/
#ifndef _NIDAQMXWRAP_AIAO_ROTATION_C_
#define _NIDAQMXWRAP_AIAO_ROTATION_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../logfile/logfile.h"
#include "nidaqmxwrap_AIAO.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//new_rfp structure
int rotation__copy_rfpe(rfpe *source, rfpe *dest)
{
  register int i = 0;
  dest->ell = source->ell;
  dest->ellinc = source->ellinc;  
  dest->phasenum = source->phasenum;
  dest->phaseden =source->phaseden;
  dest->eta = source->eta;
  for (i = 0 ; i < 6 ; i++){
    dest->F[i] = source->F[i];
  }
  return 0;
}

int rotation__copy_rfpea(rfpea *source, rfpea *dest)
{
  register int i = 0;

  /** copy the sync rotation parameters and values*/
  dest->rotnp= source->rotnp;
  dest->rotell = source->rotell;
  
  if(dest->rotnp != source->rotnp){
    //win_printf("dest->rotnp = %d != source->rotnp = %d",dest->rotnp,source->rotnp);
    dest->rotnp = source->rotnp;
    if((dest->etaccw = (float64 *) realloc(dest->etaccw,dest->rotnp*sizeof(float64))) == NULL)
      return dump_to_xv_log_file_with_date_and_time("Could not allocate dest->etaccw!");
    if((dest->Sccw = (float64*) realloc(dest->Sccw,dest->rotnp*sizeof(float64))) == NULL)
      return dump_to_xv_log_file_with_date_and_time("Could not allocate e->Sccw!");
    if((dest->Fccw = (float64*) realloc(dest->Fccw,6*dest->rotnp*sizeof(float64))) == NULL)
      return dump_to_xv_log_file_with_date_and_time("Could not allocate e->Fccw!");
  }
  
  dest->curetaccw = source->curetaccw;

  for (i = 0 ; i < dest->rotnp ; i++){
    dest->etaccw[i] = source->etaccw[i];
    dest->Sccw[i] = source->Sccw[i];
  }
  for (i = 0 ; i < 6*dest->rotnp ; i++){
    dest->Fccw[i] = source->Fccw[i];
  }
  
  /**copy the modulation parameters and values*/
  for (i = 0 ; i < NP ; i++){
    dest->etanpa[i] = source->etanpa[i];
    dest->etanreps[i] = source->etanreps[i];
    dest->functionflag[i] = source->functionflag[i];
    dest->ell[i] = source->ell[i];
  }
 
 if(dest->etatotnp != source->etatotnp){
   //win_printf("dest->etatotnp = %d != source->etatotnp = %d",dest->etatotnp,source->etatotnp);
    dest->etatotnp = source->etatotnp;
    if((dest->etarr = (float64 *) realloc(dest->etarr,dest->etatotnp*sizeof(float64))) == NULL)
      return dump_to_xv_log_file_with_date_and_time("Could not allocate dest->etarr!");
    if((dest->F = (float64*) realloc(dest->F,6*dest->etatotnp*sizeof(float64))) == NULL)
      return dump_to_xv_log_file_with_date_and_time("Could not allocate e->etarr!");
  }
  dest->etatotnp = source->etatotnp;

  dest->etafunction = source->etafunction;
  dest->etapar = source->etapar;
  dest->cureta = source->cureta;
  
  for (i = 0 ; i < dest->etatotnp ; i++)
    dest->etarr[i] = source->etarr[i];
  for (i = 0 ; i < 6*dest->etatotnp ; i++)
    dest->F[i] = source->F[i];

  return 0;
}

int rotation__copy_rfpc(rfpc *source, rfpc *dest)
{
  register int i = 0;
  dest->ebizarre = source->ebizarre ;
  dest->num_turns = source->num_turns ;
  dest->num_points = source->num_points;
  dest->apol = source->apol;
  dest->epsilon = source->epsilon;
  dest->num_pos_turns = source->num_pos_turns;
  if(dest->total_num_points != source->total_num_points){
    dest->total_num_points = source->total_num_points;
    if((dest->cir = (float64 *) realloc(dest->cir,2*source->total_num_points*sizeof(float64))) == NULL) 
      return dump_to_xv_log_file_with_date_and_time("Could not allocate dest->cir!");
  }
  for (i = 0 ; i < dest->total_num_points ; i++){
    dest->cir[i*2] = source->cir[i*2];
    dest->cir[i*2+1] = source->cir[i*2+1];
  }
  return 0;
}

int rotation__copy_new_rfp(new_rfp *source, new_rfp *dest)
{
  if(dest == NULL)
    dump_to_xv_log_file_with_date_and_time("dest = NULL!");
  dest->frequency = source->frequency;
  dest->level = source->level;
  dest->cmodflag = source->cmodflag = 0;
  rotation__copy_rfpc(&(source->c), &(dest->c));
  dest->esmodflag = source->esmodflag = 0;
  rotation__copy_rfpe(&(source->es), &(dest->es));
  dest->esmodflag = source->esmodflag = 0;
  rotation__copy_rfpea(&(source->ed), &(dest->ed));
  return 0;
}

int rotation__update_new_rfp(new_rfp *source, new_rfp *dest)
{
  if(dest == NULL)
    dump_to_xv_log_file_with_date_and_time("dest = NULL!");
  dest->frequency = source->frequency;
  dest->level = source->level;
  if(source->cmodflag){
    dest->cmodflag = source->cmodflag = 0;
    rotation__copy_rfpc(&(source->c), &(dest->c));
  }
  if(source->esmodflag){
    dest->esmodflag = source->esmodflag = 0;
    rotation__copy_rfpe(&(source->es), &(dest->es));
  }
  if(source->edmodflag){
    dest->esmodflag = source->esmodflag = 0;
    rotation__copy_rfpea(&(source->ed), &(dest->ed));
  }
  return 0;
}

/**This calculates the ellipse matrix F
  F is a 2x3 Matrix defined as F = ell* 
  |c/SQ3 + s/3 , 2*s/3 , -c/SQ3 + s/3|
  |s/SQ3 - c/3 ,-2*c/3 , -s/SQ3 - c/3|
   c = cos(2*ell)
   s = sin(2*ell)
 */
int rotation__calculate_ellipse_matrix(rfpe *e)
{
  float64 c,s;
  /*re-calculate the value of eta based on the
    actual phasenum and phaseden values*/  
  e->eta = (float) e->phasenum/e->phaseden*2*PI;
  /*re-calculate the values of the matrix entries
   */
  c = cos(2*e->eta);
  s = sin(2*e->eta);
  e->F[0] = c/SQ3 + s/3;
  e->F[1] = 2*s/3;
  e->F[2] = -c/SQ3 + s/3;
  e->F[3] = s/SQ3 - c/3;
  e->F[4] = -2*c/3;
  e->F[5] = -s/SQ3 - c/3;
  return 0;
}

int task__calculate_cccw_arrays(float frot)
{
  register int i = 0;
  float64 fs = task__smplFreq;
  int nprot = (int) fs/frot;

  if((cw = (float64 *) realloc(cw,3*nprot*sizeof(float64))) == NULL)
    return dump_to_xv_log_file_with_date_and_time("Could not reallocate cw!");
  if((ccw = (float64 *) realloc(ccw,3*nprot*sizeof(float64))) == NULL)
    return dump_to_xv_log_file_with_date_and_time("Could not reallocate cw!");

  for(i = 0 ; i < nprot; i++){
    cw [3*i  ] = - sin(2*PI * frot/fs * i) * AmatrTrans[0] + cos(2*PI * frot/fs * i)*AmatrTrans[3];
    cw [3*i+1] = - sin(2*PI * frot/fs * i) * AmatrTrans[1] + cos(2*PI * frot/fs * i)*AmatrTrans[4];
    cw [3*i+2] = - sin(2*PI * frot/fs * i) * AmatrTrans[2] + cos(2*PI * frot/fs * i)*AmatrTrans[5];
    ccw[3*i  ] = sin(2*PI * frot/fs * i) * AmatrTrans[0] + cos(2*PI * frot/fs * i)*AmatrTrans[3];
    ccw[3*i+1] = sin(2*PI * frot/fs * i) * AmatrTrans[1] + cos(2*PI * frot/fs * i)*AmatrTrans[4];
    ccw[3*i+2] = sin(2*PI * frot/fs * i) * AmatrTrans[2] + cos(2*PI * frot/fs * i)*AmatrTrans[5];
  }
  return 0;
}

int rotation__calculate_Fcccw_matrix(rfpea *ea)
{
  register int j = 0;
  float64 c,s, eta;

  if((ea->etaccw = (float64 *) realloc(ea->etaccw,ea->rotnp*sizeof(float64))) == NULL)
    return dump_to_xv_log_file_with_date_and_time("Could not allocate ea->etaccw!");  
    if((ea->Sccw = (float64*) realloc(ea->Sccw,ea->rotnp*sizeof(float64))) == NULL)
      return dump_to_xv_log_file_with_date_and_time("Could not allocate e->Sccw!");
  if((ea->Fccw = (float64 *) realloc(ea->Fccw,6*ea->rotnp*sizeof(float64))) == NULL)
    return dump_to_xv_log_file_with_date_and_time("Could not reallocate ea->Fcw!");
  for(j = 0 ; j < ea->rotnp; j++){
    eta = ea->etaccw[j] = (float64) j*2*PI/ea->rotnp;

    ea->Sccw[j] = sin(eta);

    c = (float64) cos(2*eta);
    s = (float64) sin(2*eta);

    ea->Fccw[6*j] = c/SQ3 + s/3;
    ea->Fccw[6*j+1] = 2*s/3;
    ea->Fccw[6*j+2] = - c/SQ3 + s/3;
    ea->Fccw[6*j+3] = s/SQ3 - c/3;
    ea->Fccw[6*j+4] = -2*c/3;
    ea->Fccw[6*j+5] = - s/SQ3 - c/3;
  }
  return 0;
}

int rotation__calculate_earr_matrix(rfpea *ea)
{
  register int i = 0,j = 0, k = 0;
  float64 c,s;

  ea->etatotnp = 0;
  for(j = 0 ; j < NP ; j++){
    ea->etatotnp += ea->etanreps[j]*ea->etanpa[j];
  }
  //  win_printf("etatotnp = %d,%d,%d",ea->etatotnp,ea->etanreps[0],ea->etanpa[0]);
  
  if((ea->etarr = (float64 *) realloc(ea->etarr,ea->etatotnp*sizeof(float64))) == NULL)
    return dump_to_xv_log_file_with_date_and_time("Could not allocate ea->etarr!");
  if((ea->F = (float64*) realloc(ea->F,6*ea->etatotnp*sizeof(float64))) == NULL)
    return dump_to_xv_log_file_with_date_and_time("Could not allocate ea->F!");
  //  win_event("calculate earr has reallocated ea->F to 6 * %d points", ea->etatotnp);
 
  j = 0;
  for(i = 0 ; i < NP ; i++){
    switch(ea->functionflag[i]){
    case 0:
      ea->etafunction = waveform__const_function;
      break;
    case 1: 
      ea->etafunction = waveform__sin_function;
      break;
    case 2:
      ea->etafunction = waveform__triangle_function;
      break;
    case 3: 
      ea->etafunction = waveform__square_function;
      break;
    }
    for(k = 0 ; k < ea->etanreps[i]*ea->etanpa[i] ; k++){
      ea->etarr[j] = (float64) ea->etafunction((double)(k%ea->etanpa[i])/ea->etanpa[i],&(ea->etapar));
      c = (float64) cos(2*ea->etarr[j]);
      s = (float64) sin(2*ea->etarr[j]);
      ea->F[6*j] =   ea->ell[i]* (c/SQ3 + s/3);
      ea->F[6*j+1] = ea->ell[i]* (2*s/3);
      ea->F[6*j+2] = ea->ell[i]* (-c/SQ3 + s/3);
      ea->F[6*j+3] = ea->ell[i]* (s/SQ3 - c/3);
      ea->F[6*j+4] = ea->ell[i]* (-2*c/3);
      ea->F[6*j+5] = ea->ell[i]* (-s/SQ3 - c/3);
      j++;
    }
  }
  return 0;
}

int rotation__calculate_rfpc_matrix(rfpc *c)
{
  int i = 0;
  double x, apo, theta;

  if((c->cir = (float64*) realloc(c->cir,2*c->total_num_points*sizeof(float64))) == NULL) { dump_to_xv_log_file_with_date_and_time("memory allocation error"); return -1;}
  if(c->apol){
    for (i = 0 ; i < c->total_num_points ; i++)
    {
      x = (double) i/c->total_num_points;
      if(x < c->dc){
	theta = (double) c->spF*x;
	apo = (1 + c->ebizarre)*(1 - cos(c->spFapo1*x));
      }
      else{
	theta = (double) c->spF*(2*c->dc - x);
	apo = (1 - c->ebizarre)*(1 - cos(c->spFapo2*(x - c->dc)));
      }
      c->cir[i*2] = (float64) apo*cos (theta);
      c->cir[i*2+1] = (float64) apo*sin (theta);
    }
  }
  else{
    for (i = 0 ; i < c->total_num_points ; i++)
    {
      x = (double) i/c->total_num_points;
      if(x < c->dc){
	theta = (double) (1 + c->ebizarre)*c->spF*x;
      }
      else{
	theta = (double) (1 - c->ebizarre)*c->spF*(2*c->dc - x);
      }
      c->cir[i*2] = (float64) cos (theta);
      c->cir[i*2+1] = (float64) sin (theta);
    }
  }
  return 0;
}

int rotation__check_rfpea(rfpea *e)
{
  int ret = 0;
  rotation__calculate_Fcccw_matrix(e);
  rotation__calculate_earr_matrix(e);
  return ret;
}

int rotation__check_rfpc(rfpc *c)
{
  int ret = 0;
  if(c->num_turns < 0) return ret = -1;
  if(c->num_points < 0) return ret = -4;
  if(c->apol != 0 && c->apol != 1) return ret = -5;
  if(c->num_pos_turns < 0) return ret = -8;
  if((c->spF = (2*PI*c->num_turns+2*c->epsilon)) <=0) return -9;
  c->dc =  (float) (2*PI*c->num_pos_turns + c->epsilon)/c->spF;
  if(c->dc != 0)
    c->spFapo1 = 2*PI/c->dc;
  if(c->dc != 1)
    c->spFapo2 = 2*PI/(1-c->dc);
  c->total_num_points = (int) (c->num_turns+2*c->epsilon/PI)*c->num_points;
  rotation__calculate_rfpc_matrix(c);
  return ret;
}

int rotation__check_new_rfp(new_rfp *rf)
{
  int ret = 0;
  if((ret = rotation__check_rfpc(&(rf->c))) != 0 ) return ret;
  if(rf->frequency < 0) return ret = -3;
  if((rf->c.apol == 0) && (rf->level > 10 || rf->level < -10)) return ret = -6;
  if((rf->c.apol == 1) && (rf->level > 5 || rf->level < -5)) return ret = -7;
  if(rf->es.phaseden != 0)
    rf->es.eta = (float) rf->es.phasenum/rf->es.phaseden*2*PI;
  else rf->es.eta = 0;
  rotation__calculate_ellipse_matrix(&(rf->es));
  rotation__check_rfpea(&(rf->ed));
  rotation__check_rfpc(&(rf->c));
  return ret;
}

int rotation__init_rfpea(rfpea *e)
{
  register int i = 0 ;

  e->etanreps[0] = 1;
  e->etanreps[1] = 0;
  e->etanreps[2] = 0;
  e->etanpa[0] = 1;
  e->ell[0] = 0.;
  for(i = 1 ; i < NP ; i++){
    e->etanpa[i] = 0;
    e->ell[i] = 0.;
    e->etanreps[i] = 1;
  }
  if((e->etarr = (float64 *) calloc(e->etatotnp,sizeof(float64))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not allocate e->etarr!");
  if((e->etaccw = (float64 *) calloc(e->rotnp,sizeof(float64))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not allocate e->etaccw!");
  e->etafunction = waveform__const_function;
  e->etapar.p = 0.;
  e->etapar.d = 0.5;
  e->etapar.o = 0.;
  e->etapar.a = 0;
  e->etapar.s = 1;
  e->cureta = 0;
  e->curetaccw = 0;
  if((e->F = (float64*) calloc(6*e->etatotnp,sizeof(float64))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not allocate e->etarr!");
  if((e->Sccw = (float64*) calloc(e->rotnp,sizeof(float64))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not allocate e->Sccw!");
  if((e->Fccw = (float64*) calloc(6*e->rotnp,sizeof(float64))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not allocate e->Fccw!");
  rotation__check_rfpea(e);
  return 0;
}

int rotation__resize_rfpea(rfpea *ea,int  np)
{
  ea->etatotnp = np;
  if((ea->etarr = (float64 *) realloc(ea->etarr,ea->etatotnp*sizeof(float64))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not reallocate ea->etarr!");
  ea->cureta = 0;
  if((ea->F = (float64 *) realloc(ea->F,6*ea->etatotnp*sizeof(float64))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not reallocate ea->F!");
  return 0;
}

int rotation__free_rfpea(rfpea *e)
{
  free(e->etarr);
  free(e->etaccw);
  free(e->F);
  free(e->Fccw);
  free(e);
  return 0;
}

int rotation__init_rfpc(rfpc *c)
{
  c->ebizarre = 0.;
  c->num_turns = 10;
  c->num_points = 100;
  c->apol = 1;
  c->epsilon = 0.;
  c->num_pos_turns = 5;
  c->total_num_points = 1000;
  if((c->cir = (float64 *) calloc(2*c->total_num_points,sizeof(float64))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not allocate c->cir!");
  rotation__check_rfpc(c);
  return 0;
}

int rotation__init_new_rfp(new_rfp *rf)
{
  config__load(rf);
  // rf->frequency = 1000;
  //  rf->level = 0.5;
  //  rf->es.ell = 0.;//-0.0212082;
  //  rf->es.ellinc = 0.;
  //  rf->es.phasenum = 0;//140;
  //  rf->es.phaseden = 360;
  //  rf->es.eta = 0.;
  rf->cmodflag = 0;
  rotation__init_rfpc(&(rf->c));
  rf->esmodflag = 0;
  rotation__init_rfpea(&(rf->ed));
  rf->edmodflag = 0;
  rf->stop_and_restart = 0;
  rf->update = 0;
  rotation__check_new_rfp(rf);
  return 0;
}

int rotation__plot_cir(void)
{
  int i=0;
  pltreg *pr;
  O_p *op = NULL;
  d_s *ds = NULL;
  rfpc *c = NULL;
  int np = 0;

  if(updating_menu_state != 0) return D_O_K;

  if((c = &(grf->c)) == NULL)
    return dump_to_xv_log_file_with_date_and_time("error, cir = NULL");

  np = c->total_num_points;
  //win_printf("np = %d",np);

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot region"));
  if((op = create_and_attach_one_plot(pr,np,np,0))==NULL)
    return(win_printf("can't create and attach one plot"));
  if((ds = op->dat[0])==NULL)
    return(win_printf("ds == NULL"));


  for (i = 0 ; i < np ; i++){
    ds->xd[i] = c->cir[i*2];
    ds->yd[i] = c->cir[i*2+1];
  }

  op->need_to_refresh |= 1;
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int rotation__set_new_rfp(new_rfp *rf)
{
   int i = 0, j=0;
  if(updating_menu_state != 0) return D_O_K;

  if(win_scanf("Rotation Parameters Setting:\n"
	       "e bizarre %6f\n"
	       "total number of turns rf->num turns: %5d\n"
	       "rf->frequency (turns per s): %6f\n"
	       "rf->num points: %6d\n"
	       "field amplitude rf->level (V): %6f\n"
	       "apolisation? %R NO %r YES\n"
	       "more than one turn? rf->epsilon %6f\n"
	       "number of positive turns rf->num pos turns %6d\n"
	       "First elliptical correction\n"
	       "ellipticity (0 for circle) rf->es.ell: %5f\n"
	       "ellipticity increment rf->es.ellinc: %5f\n"
	       "Phase angle = %6d/%6d PI \n"
	       "Non stop rotation parameters:\n"
	       "rotell: %5f\n"
	       "rotnp: %5d\n"
	       "Modulation parameters:\n"
	       "0:nb points %5d, nb repeats %5d, rf->ed.ell[0]: %5f\n"
	       "function: %R Const, %r Sine, %r Triangle, %r Square\n"
	       "1:nb points %5d, nb repeats %5d, rf->ed.ell[1]: %5f\n"
	       "function: %R Const, %r Sine, %r Triangle, %r Square\n"
	       "2:nb points %5d, nb repeats %5d, rf->ed.ell[2]: %5f\n"
	       "function: %R Const, %r Sine, %r Triangle, %r Square\n"
	       "oscillation phase : %5f\n"
	       "oscillation duty cycle : %5f\n"
	       "oscillation offset : %5f\n"
	       "oscillation amplitude (rad) : %5f\n"
	       "oscillation sign (+1 or -1) %3d\n"
	       ,&(rf->c.ebizarre)
	       ,&(rf->c.num_turns)
	       ,&(rf->frequency)
	       ,&(rf->c.num_points)
	       ,&(rf->level)
	       ,&(rf->c.apol)
	       ,&(rf->c.epsilon)
	       ,&(rf->c.num_pos_turns)
	       ,&(rf->es.ell)
	       ,&(rf->es.ellinc)
	       ,&(rf->es.phasenum)
	       ,&(rf->es.phaseden)
	       ,&(rf->ed.rotell)
	       ,&(rf->ed.rotnp)
	       ,rf->ed.etanpa  ,rf->ed.etanreps,  &(rf->ed.ell[0]),&(rf->ed.functionflag[0])
	       ,rf->ed.etanpa+1,rf->ed.etanreps+1,&(rf->ed.ell[1]),&(rf->ed.functionflag[1])
	       ,rf->ed.etanpa+2,rf->ed.etanreps+2,&(rf->ed.ell[2]),&(rf->ed.functionflag[2])
	       ,&(rf->ed.etapar.p)
	       ,&(rf->ed.etapar.d)
	       ,&(rf->ed.etapar.o)
	       ,&(rf->ed.etapar.a)
	       ,&(rf->ed.etapar.s)
	       ) == CANCEL) return CANCEL;
  if((i = rotation__check_new_rfp(rf))!=0) {
    win_printf("something is wrong!\ncheck rf returned %d",i);
    return CANCEL;
  }
  if((task_grf->c.ebizarre != rf->c.ebizarre) ||
     (task_grf->c.total_num_points != rf->c.total_num_points) ||
     (task_grf->frequency != rf->frequency) ||
     (task_grf->c.num_points != rf->c.num_points) ||
     (task_grf->c.num_turns != rf->c.num_turns) ||
     (task_grf->c.epsilon != rf->c.epsilon) || 
     (task_grf->c.num_pos_turns != rf->c.num_pos_turns) || 
     (task_grf->c.apol != rf->c.apol)){
    rf->cmodflag = 1;
    rf->stop_and_restart = 1;
  }
  if((task_grf->level != rf->level)){
    pending_task_grf->level = rf->level;
    if(rf->stop_and_restart == 0)
      rf->update = 1;
  }
  if((task_grf->es.ell != rf->es.ell) ||
     (task_grf->es.phasenum != rf->es.phasenum) ||
     (task_grf->es.phaseden != rf->es.phaseden)){
    rf->esmodflag = 1;
    rotation__calculate_ellipse_matrix(&(rf->es));
    if(rf->stop_and_restart == 0)
      rf->update = 1;
  }

  if((task_grf->ed.rotell != rf->ed.rotell)||
     (task_grf->ed.rotnp != rf->ed.rotnp)){
    rf->edmodflag = 1;
    rotation__calculate_Fcccw_matrix(&(rf->ed));
    if(rf->stop_and_restart == 0)
      rf->update = 1;
  }

  if((task_grf->ed.etanpa[0] != rf->ed.etanpa[0]) ||
     (task_grf->ed.etanpa[1] != rf->ed.etanpa[1]) ||
     (task_grf->ed.etanpa[2] != rf->ed.etanpa[2]) ||
     (task_grf->ed.ell[0] != rf->ed.ell[0]) ||
     (task_grf->ed.ell[1] != rf->ed.ell[1]) ||
     (task_grf->ed.ell[2] != rf->ed.ell[2]) ||
     (task_grf->ed.etanreps[0] != rf->ed.etanreps[0]) ||
     (task_grf->ed.etanreps[1] != rf->ed.etanreps[1]) ||
     (task_grf->ed.etanreps[2] != rf->ed.etanreps[2]) ||
     (task_grf->ed.etapar.p != rf->ed.etapar.p) ||
     (task_grf->ed.etapar.d != rf->ed.etapar.d) ||
     (task_grf->ed.etapar.o != rf->ed.etapar.o) ||
     (task_grf->ed.etapar.a != rf->ed.etapar.a) ||
     (task_grf->ed.etapar.s != rf->ed.etapar.s) ||
     (task_grf->ed.functionflag[0] != rf->ed.functionflag[0]) ||
     (task_grf->ed.functionflag[1] != rf->ed.functionflag[1]) ||
     (task_grf->ed.functionflag[2] != rf->ed.functionflag[2])){
    //    win_event("hey! you just changed something in the ed parameters");
    rf->edmodflag = 1;
    rotation__calculate_earr_matrix(&(rf->ed));
    if(rf->stop_and_restart == 0)
      rf->update = 1;
  }

  config__write(rf);
  // rotation__update_new_rfp(rf,pending_task_grf);
  rotation__copy_new_rfp(rf,pending_task_grf);
  //  rotation__copy_new_rfp(rf,task_grf);
  return 0;
}

int rotation__plot_earr(void)
{
  int i=0;
  pltreg *pr;
  new_rfp *rf;
  O_p *op;
  d_s *ds;
  int np = 0;
  if(updating_menu_state != 0) return D_O_K;
  
  rf = grf;
  if(rf->ed.etarr == NULL) return dump_to_xv_log_file_with_date_and_time("error, earr = NULL");
  np = rf->ed.etatotnp;

  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot region"));

    op = create_and_attach_one_plot(pr,np,np,0);

  ds = op->dat[0];
  set_ds_source(ds, "grf->ed.etarr");

  for (i = 0 ; i < np ; i++){
    ds->xd[i] = i;
    ds->yd[i] = (float) rf->ed.etarr[i];
  }

  rf = pending_task_grf;
  if(rf->ed.etarr == NULL) return dump_to_xv_log_file_with_date_and_time("error, earr = NULL");
  np = rf->ed.etatotnp;

  ds = create_and_attach_one_ds(op,np,np,0);
  set_ds_source(ds, "pending_task_grf->ed.etarr");

  for (i = 0 ; i < np ; i++){
    ds->xd[i] = i;
    ds->yd[i] = (float) rf->ed.etarr[i];
  }

  rf = task_grf;
  if(rf->ed.etarr == NULL) return dump_to_xv_log_file_with_date_and_time("error, earr = NULL");
  np = rf->ed.etatotnp;

  ds = create_and_attach_one_ds(op,np,np,0);
  set_ds_source(ds, "task_rf->ed.etarr");

  for (i = 0 ; i < np ; i++){
    ds->xd[i] = i;
    ds->yd[i] = (float) rf->ed.etarr[i];
  }

  set_plot_title(op, "np = %d, ell = %f, ampl = %f",np,rf->ed.ell[0],rf->ed.etapar.a);

  op->need_to_refresh |= 1;
  refresh_plot(pr, UNCHANGED);
  return 0;
}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
int plot_data(float64 *data,int np)
{
  int i;
  O_p *op;
  pltreg *pr;
  d_s *ds;
  if(updating_menu_state != 0) return D_O_K;
  
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return(win_printf("can't find plot region"));
  
  op = create_and_attach_one_plot(pr,np,np,0);
  ds = op->dat[0];
  
  for(i = 0 ; i < np ; i++){
    ds->xd[i] = i;
    ds->yd[i] = data[i];
  }
  op->need_to_refresh = 1;
  return refresh_plot(pr, UNCHANGED);
}

/**This is a fast implementation of the ellipse function .
   
\author Francesco Mosconi
\version 28/05/2007
*/
int rotation__add_next_nSamples_earr_contribution(new_rfp *rf, float64 *data, int nSamples)
{
  register int i,n;
  int m = 0;
  int np = 1;
  float64 M[2*NUMBER_OF_AI_LINES];
  
  if(rf == NULL)
    return dump_to_xv_log_file_with_date_and_time("NULL rf");
  m = rf->c.total_num_points;
  
  if(nSamples%m == 0)
    np = nSamples/m;
  else
    dump_to_xv_log_file_with_date_and_time("mismatch between nSamples and rf->total_num_points");
  //    win_event("np = %d, nSamples = %d, m = %d", np, nSamples, m);
  
  if(data + nSamples*NUMBER_OF_AI_LINES - 1 == NULL)
    return dump_to_xv_log_file_with_date_and_time("NULL AOdata encountered in rotation__add_next_nSamples_earr_contribution");

  for(n = 0 ; n < np ; n++){
    rf->ed.cureta = ( rf->ed.cureta + 1) >= rf->ed.etatotnp ? 0 : rf->ed.cureta + 1;
    //    win_event("rf->ed.etanp = %d, ed.cureta = %d",rf->ed.etanp,rf->ed.cureta);
    for(i = 0; i < 2*NUMBER_OF_AI_LINES; i++){
      //this is a linear approximation for ell << 1, check if it is sufficiently good
      M[i] = 
	AmatrTrans[i] + 
	rf->es.ell*rf->es.F[i] +
	rf->ed.F[6*(rf->ed.cureta)+i];
      //win_event("ed.F[%d] = %f",6*(rf->ed.cureta)+i,(float) rf->ed.F[6*(rf->ed.cureta)+i]);
    }
    my_matrix_product((float64) rf->level, rf->c.cir,m,2,M,2,NUMBER_OF_AI_LINES,data+n*m*NUMBER_OF_AI_LINES);
  }
  //plot_data(data, nSamples*NUMBER_OF_AI_LINES);
  return 0;
}

int rotation__rfp_functions_main(void)
{
  if(updating_menu_state != 0) return D_O_K;
  
  if(pending_task_grf == NULL)
    if((pending_task_grf = (new_rfp *) calloc(1,sizeof(new_rfp))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not allocate grf structure !");
  if(task_grf == NULL)
    if((task_grf = (new_rfp *) calloc(1,sizeof(new_rfp))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not allocate grf structure !");
  if(grf == NULL)
    if((grf = (new_rfp *) calloc(1,sizeof(new_rfp))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not allocate newGrf structure !");
  
  rotation__init_new_rfp(grf);
  rotation__init_new_rfp(task_grf);
  rotation__init_new_rfp(pending_task_grf);
  
  return D_O_K;
  
}
#endif
