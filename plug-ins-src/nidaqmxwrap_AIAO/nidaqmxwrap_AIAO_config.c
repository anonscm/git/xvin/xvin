/*
*    Plug-in program for nidaqmxwrap_AIAO config file
*
*    F. Mosconi
*/

#ifndef _NIDAQMXWRAP_AIAO_CONFIG_C_
#define _NIDAQMXWRAP_AIAO_CONFIG_C_

# include "allegro.h"
# include "winalleg.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 
# include "../logfile/logfile.h"
#include <NIDAQmx.h>

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "nidaqmxwrap_AIAO.h"

int config__open(void)
{

  char file[256];
  extern char prog_dir[256];
  
  sprintf(file,"%snidaqmxwrap_AIAO.cfg",prog_dir);
  override_config_file(file);

  return D_O_K;
}  


int config__close(void)
{
  override_config_file(NULL);
  return D_O_K;
}


int config__write(new_rfp *rf)
{
  config__set(rf);
  flush_config_file();
  return D_O_K;
}


/////////////////////////////////////////SETTING PARAMETERS//////////////////////////////////////////
 
int config__rf_set(new_rfp *rf)
{   
    set_config_float("NEWRFP","frequency",rf->frequency);
    set_config_float("NEWRFP","level",rf->level);
	
    return D_O_K;
}

int config__es_set(new_rfp *rf)
{
  set_config_float("ES","ell",rf->es.ell);
  set_config_float("ES","ellinc",rf->es.ellinc);
  set_config_int("ES","phasenum",rf->es.phasenum);
  set_config_int("ES","phaseden",rf->es.phaseden);
  set_config_float("ES","eta",rf->es.eta);
  return D_O_K;
}

int config__ed_set(new_rfp *rf)
{
  set_config_float("ED","rotell",rf->ed.rotell);
  set_config_int("ED","rotnp",rf->ed.rotnp);
  return D_O_K;
}

int config__set(new_rfp *rf)
{
  config__open();
  config__rf_set(rf);
  config__es_set(rf);
  config__ed_set(rf);
  config__close();
  return D_O_K;
}    

////////////////////////////////////LOADING PARAMETERS/////////////////////////////////////


int config__rf_load(new_rfp *rf)
{   
    rf->frequency = get_config_float("NEWRFP","frequency",1000);
    rf->level = get_config_float("NEWRFP","level",0.5);
	
    return D_O_K;
}   

int config__es_load(new_rfp *rf)
{   
    rf->es.ell = get_config_float("ES","ell",0.);
    rf->es.ellinc = get_config_float("ES","ellinc",0.);
    rf->es.phasenum = get_config_int("ES","phasenum",0);
    rf->es.phaseden = get_config_int("ES","phaseden",360);	
    rf->es.eta = get_config_float("ES","eta",0.);
    return D_O_K;
}   

int config__ed_load(new_rfp *rf)
{   
    rf->ed.rotell = get_config_float("ED","rotell",0.1);
    rf->ed.rotnp = get_config_int("ED","rotnp",200);
    return D_O_K;
}   


int config__load(new_rfp *rf)
{
  config__open();
  config__rf_load(rf);
  config__es_load(rf);
  config__ed_load(rf);
  config__close();

  return D_O_K;
}

# endif

