clear
%%%%%%%%%%%%%%%%initial parameters definition%%%%%%%%%%%%%%%%%
P = 3.1415926535897932385; %%PI

%%these are the axes
a = [sqrt(3)/2;1/2];
b = [0;1];
c = [-sqrt(3)/2;1/2];
%%this is the matrix that maps xy coords to abc coords

A = [1/sqrt(3) 1/3 ; 0 2/3 ; -1/sqrt(3) 1/3];

%%this is the matrix that maps abc coords to xy coords

R = [a b c];

%%check that the product RA is the 2d Unity

I = R*A

%%whereas the product AR is not the 3d Unity
%%but it reflects the definition of my coordinates
%%and it has the nice property that N = NN = NNN = ...
%%N =
%%
%%   0.66667   0.33333  -0.33333
%%   0.33333   0.66667   0.33333
%%  -0.33333   0.33333   0.66667
N = A*R;

%%here I define the ellipse parameters

N1 = 5.;
N2 = 5.;
ell = 0.25;

epsilon = 0.*P;

dc = (2*P*N1 + epsilon)/(2*P*(N1+N2)+2*epsilon);
F = (2*P*(N1+N2)+ 2*epsilon);
Fapo1 = (2*P/dc);
Fapo2 = (2*P/(1-dc));

t1 = 0:0.001:dc;
t2 = dc:0.001:1;
t = [t1 t2]

theta1 = F*t1;
theta2 = F*(2*dc-t2);
theta = [theta1 theta2]'
%%%%%%%%%%%%%%%%initial parameters definition%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%apolisation%%%%%%%%%%%%%%%%%
apo = [(1-cos(Fapo1*t1)) (1-cos(Fapo2*(t1-dc)))]';
plot(t,apo)
%%%%%%%%%%%%%%%%apolisation%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%circle%%%%%%%%%%%%%%%%%
cir = [cos(theta) sin(theta)];
plot (t,cir(:,1),t,cir(:,2))
%%%%%%%%%%%%%%%%circle%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%circle%%%%%%%%%%%%%%%%%
cirapo = [apo apo] .* cir;
plot (t,cirapo(:,1),t,cirapo(:,2))
%%%%%%%%%%%%%%%%circle%%%%%%%%%%%%%%%%%

%%first of all check that if I represent 
%%cir in the a,b,c coords I get the same figure:

fapo = cirapo*A';

ciraporec = fapo*R';
plot (t,cirapo(:,1),t,cirapo(:,2),t,ciraporec(:,1),t,ciraporec(:,2));

%%now add ellipse contribution

%%first define the function

function [T] = my_abc_ell(A,ell,eta);
  T = zeros(2,3);
  c = cos(2*eta);
  s = sin(2*eta);
  sq3 = sqrt(3);

  T = A' + ell*[c/sq3+s/3 2*s/3 -c/sq3+s/3 ; s/sq3-c/3 -2*c/3 -s/sq3-c/3 ]
endfunction

%%now define eta
eta = 3/4*P;
T = my_abc_ell(A,ell,eta);
feapo = cirapo*T;

%% I plot it in cartesian coordinates:
feapoc = feapo*R';
%%plot to check:
plot(cir(:,1),cir(:,2),cirapo(:,1),cirapo(:,2),feapoc(:,1),feapoc(:,2))

