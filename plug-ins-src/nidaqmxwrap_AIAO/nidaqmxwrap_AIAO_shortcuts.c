/** \file shortcuts.c
    
    These functions allow shortcut control through keyboard.
    
    \author Adrien Meglio
    
    \version 26/01/07
*/
#ifndef _NIDAMXWRAP_AIAO_SHORTCUTS_C_
#define _NIDAMXWRAP_AIAO_SHORTCUTS_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include "../logfile/logfile.h"
#include <NIDAQmx.h>
#include "nidaqmxwrap_AIAO.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

int currentMode = LINE_MODE;
float epsilonFinesse = 0.1;
float ellFinesse = 0.01;


int display_values_to_screen(void)
{
  int v = 100;
  int o = 60;
  if(updating_menu_state != 0)  return D_O_K;
  draw_bubble(screen,0,o+800, v+60,"Rotation Parameters Setting:");
  draw_bubble(screen,0,o+800, v+80,"grf->c.num turns: %d" , grf->c.num_turns);
  draw_bubble(screen,0,o+800, v+120,"grf->frequency %6f",	grf->frequency);
  draw_bubble(screen,0,o+800, v+140,"grf->c.num points: %6d",grf->c.num_points);
  draw_bubble(screen,0,o+800, v+160,"grf->level : %6f" ,grf->level);
  draw_bubble(screen,0,o+800, v+180,"grf->c.apol : %d",	grf->c.apol);
  draw_bubble(screen,0,o+800, v+200,"grf->c.epsilon %6f",	grf->c.epsilon);
  draw_bubble(screen,0,o+800, v+220,"grf->c.num pos turns : %6d", grf->c.num_pos_turns);

  draw_bubble(screen,0,o+800, v+260,"First ellipse");
  draw_bubble(screen,0,o+800, v+280,"grf->es.ell: %f" , grf->es.ell);
  draw_bubble(screen,0,o+800, v+300,"grf->es.ellinc: %f" , grf->es.ellinc);
  draw_bubble(screen,0,o+800, v+320,"grf->es.eta: %f" , grf->es.eta);
  draw_bubble(screen,0,o+800, v+340,"grf->es.phasenum: %d" , grf->es.phasenum);
  draw_bubble(screen,0,o+800, v+360,"grf->es.phaseden: %d" , grf->es.phaseden);
  
  draw_bubble(screen,0,o+800, v+400,"Second ellipse");
  draw_bubble(screen,0,o+800, v+420,"grf->ed.ell: %f" , grf->ed.ell);
  draw_bubble(screen,0,o+800, v+440,"grf->ed.cureta: %d" , grf->ed.cureta);
  draw_bubble(screen,0,o+800, v+460,"grf->ed.cureta value: %f" , grf->ed.etarr[grf->ed.cureta]);
  draw_bubble(screen,0,o+800, v+480,"rotp: %d" , rotp);
  draw_bubble(screen,0,o+800, v+500,"sync: %d" , sync);
  draw_bubble(screen,0,o+800, v+520,"grf->ed.curetaccw: %d" , grf->ed.curetaccw);
  draw_bubble(screen,0,o+800, v+540,"grf->ed.curetaccw value: %f" , grf->ed.etaccw[grf->ed.curetaccw]);
  draw_bubble(screen,0,o+800, v+560,"current mode = %d",currentMode);
  draw_bubble(screen,0,o+800, v+580,"ell finesse = %f",ellFinesse);
  return D_O_K;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Keyboard control functions

/** Defines a shortcut for key KEY_PLUS_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_PLUS_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_PLUS_PAD, 0, short__KEY_PLUS_PAD);
        return D_O_K; 
    }
     
    if (currentMode == ETA_MODE)
    {   
      grf->es.phasenum += 1;
      rotation__copy_rfpe(&(grf->es),&(pending_task_grf->es));
      newGrfReady = 1;
    }
    if (currentMode == ELL_MODE)
    {   
      grf->es.ell += ellFinesse;
      rotation__copy_rfpe(&(grf->es),&(pending_task_grf->es));
      newGrfReady = 1;
    }

    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_MINUS_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_MINUS_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_MINUS_PAD, 0, short__KEY_MINUS_PAD);
        return D_O_K;
    }
     
    if (currentMode == ETA_MODE)
    {   
      grf->es.phasenum -= 1;
      rotation__copy_rfpe(&(grf->es),&(pending_task_grf->es));
      newGrfReady = 1;
    }
    if (currentMode == ELL_MODE)
    {   
      grf->es.ell -= ellFinesse;
      rotation__copy_rfpe(&(grf->es),&(pending_task_grf->es));
      newGrfReady = 1;
    }
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_P.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_P(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_P, 0, short__KEY_P);
        return D_O_K;
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_L.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_L(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_L, 0, short__KEY_L);
        return D_O_K;
    }
    
    currentMode = LINE_MODE;
        
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_R.

\author Francesco Mosconi
\version 26/01/07
*/
int short__KEY_R(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_R, 0, short__KEY_R);
        return D_O_K;
    }
    
    currentMode = ROTATION_MODE;
        
    return display_values_to_screen();
}


/** Defines a shortcut for key KEY_1_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_1_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_1_PAD, 0, short__KEY_1_PAD);
        return D_O_K;
    }

    //    ellipseNumber = 0;
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_2_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_2_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_2_PAD, 0, short__KEY_2_PAD);
        return D_O_K;
    }

    //    ellipseNumber = 1;
    return display_values_to_screen();
}
/** Defines a shortcut for key KEY_3_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_3_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_3_PAD, 0, short__KEY_3_PAD);
        return D_O_K;
    }

    return display_values_to_screen();
}


/** Defines a shortcut for key KEY_4_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_4_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_4_PAD, 0, short__KEY_4_PAD);
        return D_O_K;
    }
    
    if (currentMode == ROTATION_MODE)
    {
      grf->c.epsilon = grf->c.epsilon + epsilonFinesse;
      rotation__copy_new_rfp(grf,pending_task_grf);
      task__stop_and_restart();
    }
     
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_7_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_7_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_7_PAD, 0, short__KEY_7_PAD);
        return D_O_K;
    }
     if (currentMode == ROTATION_MODE)
    {
      grf->c.epsilon = grf->c.epsilon - epsilonFinesse;
      rotation__copy_new_rfp(grf,pending_task_grf);
      task__stop_and_restart();
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_5_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_5_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_5_PAD, 0, short__KEY_5_PAD);
        return D_O_K;
    }

    currentMode = ETA_MODE;
   
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_8_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_8_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_8_PAD, 0, short__KEY_8_PAD);
        return D_O_K;
    }
    
    currentMode = ELL_MODE;
   
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_5_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_6_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_6_PAD, 0, short__KEY_6_PAD);
        return D_O_K;
    }
    
    else if (currentMode == ROTATION_MODE)
    {   
      grf->c.num_pos_turns = grf->c.num_pos_turns + 1;
      rotation__copy_new_rfp(grf,pending_task_grf);
      task__stop_and_restart();
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_6_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_9_PAD(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_9_PAD, 0, short__KEY_9_PAD);
        return D_O_K;
    }
    
    else if (currentMode == ROTATION_MODE)
    {   
      grf->c.num_pos_turns = (grf->c.num_pos_turns - 1 > 0) ? grf->c.num_pos_turns - 1 : grf->c.num_pos_turns;
      rotation__copy_new_rfp(grf,pending_task_grf);
      task__stop_and_restart();
    }
    
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_G.

\author Francesco Mosconi
\version 05/10/07
*/
int short__KEY_G(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_G, 0, short__KEY_G);
        return D_O_K;
    }
    task__stop();
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_O.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_O(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_O, 0, short__KEY_O);
        return D_O_K;
    }
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_F.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_F(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_F, 0, short__KEY_F);
        return D_O_K;
    }

    else if (currentMode == ROTATION_MODE)
    {   
        epsilonFinesse /= 10;
    }
    else if (currentMode == ETA_MODE)
    {   
      grf->es.phaseden += 1;
    }
    else if (currentMode == ELL_MODE)
    {   
        ellFinesse /= 10;
    }
    return display_values_to_screen();
}

/** Defines a shortcut for key KEY_C.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_C(void)
{
    if(updating_menu_state != 0)      
    {
        add_keyboard_short_cut(0, KEY_C, 0, short__KEY_C);
        return D_O_K;
    }
     
    else if (currentMode == ROTATION_MODE)
    {   
        epsilonFinesse *= 10;
    }
    else if (currentMode == ETA_MODE)
    {   
      grf->es.phaseden -= 1;
    }
    else if (currentMode == ELL_MODE)
    {   
        ellFinesse *= 10;
    }
    return display_values_to_screen();
}
/** Defines a shortcut for key KEY_0_PAD.

\author Adrien Meglio
\version 26/01/07
*/
int short__KEY_0_PAD(void)
{
  if(updating_menu_state != 0)      
    {
      add_keyboard_short_cut(0, KEY_0_PAD, 0, short__KEY_0_PAD);
      return D_O_K;
    }
  return display_values_to_screen();
}

//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
// Base functions

/** Main function of shortcuts capabilities

\author Adrien Meglio
\version 15/11/06
*/
int	shortcuts_main(void)
{
    if(updating_menu_state != 0) return D_O_K;

    return D_O_K;
}

#endif



