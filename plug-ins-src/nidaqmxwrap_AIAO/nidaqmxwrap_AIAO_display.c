
#ifndef _NIDAQMXWRAP_AIAO_DISPLAY_C_
#define _NIDAQMXWRAP_AIAO_DISPLAY_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
# include "../logfile/logfile.h"
#include <NIDAQmx.h>

/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "nidaqmxwrap_AIAO.h"


int display__dataAO_plot_idle_action(O_p *op, DIALOG *d)
{
  d_s *ds;
  int i = 0;
  int v = 100;
  int o = 70;

  if (d == NULL || d->dp == NULL || d->proc == NULL)    return 1;

 /*  ds = op->dat[3]; */
/*   for(i = 0 ; i < ds->mx ; i++){ */

/*     ds->xd[i] =  */
/*       //AOdata[3*i] * RmatrTrans[0] + AOdata[3*i + 1] * RmatrTrans[2] +  AOdata[3*i + 2] * RmatrTrans[4]; */
/*       (AOdata[3*i] -  AOdata[3*i + 2]) * RmatrTrans[0]; */
/*     ds->yd[i] =  */
/*       //AOdata[3*i] * RmatrTrans[1] + AOdata[3*i + 1] * RmatrTrans[3] +  AOdata[3*i + 2] * RmatrTrans[5]; */
/*       (AOdata[3*i] + AOdata[3*i + 2]) * RmatrTrans[1] */
/*       + AOdata[3*i + 1]; */
/*   } */

  ds = op->dat[4];

  ds->xd[1] =  global_level*task_grf->level*cos(task_grf->ed.etaccw[task_grf->ed.curetaccw]);
  ds->yd[1] =  global_level*task_grf->level*sin(task_grf->ed.etaccw[task_grf->ed.curetaccw]);


  ds = op->dat[5];

  ds->xd[1] =  global_level*task_grf->level*cos(task_grf->es.eta);
  ds->yd[1] =  global_level*task_grf->level*sin(task_grf->es.eta);

  ds = op->dat[6];

  ds->xd[1] =  global_level*task_grf->level*cos(task_grf->ed.etarr[task_grf->ed.cureta]);
  ds->yd[1] =  global_level*task_grf->level*sin(task_grf->ed.etarr[task_grf->ed.cureta]);

  draw_bubble(screen,0,o+800, v+60,"Rotation Parameters Setting:");
  draw_bubble(screen,0,o+800, v+80,"grf->c.num turns: %d" , task_grf->c.num_turns);
  draw_bubble(screen,0,o+800, v+120,"task_grf->frequency %6f",	task_grf->frequency);
  draw_bubble(screen,0,o+800, v+140,"task_grf->c.num points: %6d",task_grf->c.num_points);
  draw_bubble(screen,0,o+800, v+160,"task_grf->level : %6f" ,task_grf->level);
  draw_bubble(screen,0,o+800, v+180,"task_grf->c.apol : %d",	task_grf->c.apol);
  draw_bubble(screen,0,o+800, v+200,"task_grf->c.epsilon %6f",	task_grf->c.epsilon);
  draw_bubble(screen,0,o+800, v+220,"task_grf->c.num pos turns : %6d", task_grf->c.num_pos_turns);

  draw_bubble(screen,0,o+800, v+260,"First ellipse");
  draw_bubble(screen,0,o+800, v+280,"task_grf->es.ell: %f" , task_grf->es.ell);
  draw_bubble(screen,0,o+800, v+300,"task_grf->es.ellinc: %f" , task_grf->es.ellinc);
  draw_bubble(screen,0,o+800, v+320,"task_grf->es.eta: %f" , task_grf->es.eta);
  draw_bubble(screen,0,o+800, v+340,"task_grf->es.phasenum: %d" , task_grf->es.phasenum);
  draw_bubble(screen,0,o+800, v+360,"task_grf->es.phaseden: %d" , task_grf->es.phaseden);
  
  draw_bubble(screen,0,o+800, v+400,"Second ellipse");
  draw_bubble(screen,0,o+800, v+420,"task_grf->ed.ell: %f" , task_grf->ed.ell);
  draw_bubble(screen,0,o+800, v+440,"task_grf->ed.cureta: %d" , task_grf->ed.cureta);
  draw_bubble(screen,0,o+800, v+460,"task_grf->ed.cureta value: %f" , task_grf->ed.etarr[task_grf->ed.cureta]);
  draw_bubble(screen,0,o+800, v+480,"rotp: %d" , rotp);
  draw_bubble(screen,0,o+800, v+500,"sync: %d" , sync);
  draw_bubble(screen,0,o+800, v+540,"cbcntr: %d" , callbackcounter);

  op->need_to_refresh = 1;
  d->proc(MSG_DRAW,d,0); 

  return 0;
}

int display__rotating_field(void)
{
   int i, j;
   pltreg *pr;
   O_p *op;
   d_s *ds;
   static int nf = 2048;


   if(updating_menu_state != 0)	       return D_O_K;

   nf = task__nSamples/70;

   // We create a plot region to display the current field status
   pr = create_hidden_pltreg_with_op(&op, 2, 2, 0,"nidaqmxwrap_AIAO rotating field allocated structures");
   if (pr == NULL)  win_printf_OK("Could not find or allocate plot region!");
   
   op->x_lo = (-10);
   op->x_hi = (10);
   set_plot_x_fixed_range(op);
   op->y_lo = (-10);
   op->y_hi = (10);
   set_plot_y_fixed_range(op);


   // and I add three datasets to plot the axes of the coils
   ds = op->dat[0];
   set_ds_source(ds, "axis a");
   ds->xd[0] = 0;
   ds->yd[0] = 0;
   ds->xd[1] = 5*axis_a[0];
   ds->yd[1] = 5*axis_a[1];
   set_ds_point_symbol(ds, "a");

   if((ds = create_and_attach_one_ds(op,2,2,0))==NULL)
     return(win_printf("ds axe b == NULL"));
   set_ds_source(ds, "axis b");
   ds->xd[0] = 0;
   ds->yd[0] = 0;
   ds->xd[1] = 5*axis_b[0];
   ds->yd[1] = 5*axis_b[1];
   set_ds_point_symbol(ds, "b");

   if((ds = create_and_attach_one_ds(op,2,2,0))==NULL)
     return(win_printf("ds axe c == NULL"));
   set_ds_source(ds, "axis c");
   ds->xd[0] = 0;
   ds->yd[0] = 0;
   ds->xd[1] = 5*axis_c[0];
   ds->yd[1] = 5*axis_c[1];
   set_ds_point_symbol(ds, "c");


   if((ds = create_and_attach_one_ds(op,nf,nf,0))==NULL)
     return(win_printf("ds axe a == NULL"));
   set_ds_source(ds,"dataAO reconstucted ellipse");

   ds = create_and_attach_one_ds(op, 2, 2, 0);
   if (ds == NULL)  win_printf_OK("Could not create ds!");
   set_ds_source(ds, "ed.etaccw[task_grf->ed.curetaccw]");

   ds = create_and_attach_one_ds(op, 2, 2, 0);
   if (ds == NULL)  win_printf_OK("Could not create ds!");
   set_ds_source(ds, "es.eta");

   ds = create_and_attach_one_ds(op, 2, 2, 0);
   if (ds == NULL)  win_printf_OK("Could not create ds!");
   set_ds_source(ds, "ed.etarr[task_grf->ed.cureta]");

   create_attach_select_y_un_to_op(op, IS_VOLT, 0, 1., 0, 0, "V");
   op->op_idle_action = display__dataAO_plot_idle_action;
   
   set_op_filename(op, "dataAO.gr");
   set_plot_title(op, "dataAO reconstructed ellipse");

   return D_O_K;
}

int display__rotating_field_stop(void)
{
  int i = 0;
  pltreg *pr;
  O_p *op;

  if(updating_menu_state != 0) return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;

  op->op_idle_action = NULL;

  return 0;
}

# endif
