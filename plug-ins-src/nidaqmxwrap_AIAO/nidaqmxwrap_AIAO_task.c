/** \file nidaqmxwrap_AIAO_task.c
    \brief rotation control with NIDAQ-M 6229 in Xvin.

    \author Francesco Mosconi
    \version 04/06/2007
 */
#ifndef _NIDAQMXWRAP_AIAO_TASK_C_
#define _NIDAQMXWRAP_AIAO_TASK_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
# include "../logfile/logfile.h"
#include <NIDAQmx.h>
#include "nidaqmxwrap_AIAO.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL


#define DAQmxErrChk(functionCall) if( DAQmxFailed(error=(functionCall)) ) goto Error; else


int task__new_rfp_to_AOdata(void)
{
  int m = 0;
  int np = 0;
  float64 frequency = 0;
  int nSamples = 0;
  
  rotation__copy_new_rfp(pending_task_grf,task_grf);
  
  m = task_grf->c.total_num_points;
  np = (task_grf->frequency >= task_grf->c.num_turns) ? (int)  task_grf->frequency/grf->c.num_turns : 1;
  frequency = task_grf->c.num_points*task_grf->frequency;
  nSamples = np*m;
  
  if(frequency != task__smplFreq)
    task__smplFreq = frequency;
  
  if(nSamples != task__nSamples){
    task__reallocate_memory_space(nSamples);
    task__nSamples = np*m;
  }
  rotation__add_next_nSamples_earr_contribution(task_grf,AOdata,task__nSamples);
  config__write(task_grf);
  return task__create_and_launch();
} 

int32 CVICALLBACK task__EveryNRefreshBufferCallback(TaskHandle taskHandle, int32 everyNsamplesEventType, uInt32 nSamples, void *callbackData)
{
  register int i,j,k,n, nn =0;
  int m = 1;
  int np = 1;
  float64 c,s, ell;
  new_rfp *tmpGrf = NULL;
  float64 M[2*NUMBER_OF_AI_LINES];
  int32         error=0;
  char          errBuff[2048]={'\0'};
  float64       *data;
  int32         dataWritten = 0;
  int test=0;
 
  ++callbackcounter;
  if(task_grf == NULL)
    return dump_to_xv_log_file_with_date_and_time("NULL task_grf");

  //swapping data structures is done only when I didn't change 
  //the number of points or anything else that touches memory
  if(newGrfReady){
    tmpGrf = task_grf;
    task_grf = pending_task_grf;
    pending_task_grf = tmpGrf;
    newGrfReady = 0;
    test = 1;
  }

  m = task_grf->c.total_num_points;

  if(nSamples%m == 0)
    np = nSamples/m;
  else
    dump_to_xv_log_file_with_date_and_time("mismatch between nSamples and task_grf->total_num_points");
    
  //set data pointer at last write location
  data = AOdata+NUMBER_OF_AI_LINES*totalAOdataWritten;

  //check if there is enough space for writing new data
  if(data + nSamples*NUMBER_OF_AI_LINES - 1 == NULL)
    return dump_to_xv_log_file_with_date_and_time("NULL AOdata encountered in callback");

  switch(sync){
  case 1:
/*     if((cw == NULL)||(ccw == NULL)){ */
/*       sync = 0; */
/*       break; */
/*     } */
    for(n = 0 ; n < np ; n++){
      if(rotp > 0){
	task_grf->ed.curetaccw = ( task_grf->ed.curetaccw - 1) < 0 ? task_grf->ed.rotnp - 1 : task_grf->ed.curetaccw - 1;
	rotp--;
      }
      else if(rotp < 0){
	task_grf->ed.curetaccw = ( task_grf->ed.curetaccw + 1) >= task_grf->ed.rotnp ? 0 : task_grf->ed.curetaccw + 1;
	rotp++;
      }
      
      for(i = 0; i < m; i++){
	data[NUMBER_OF_AI_LINES*(n*m+i)] = 
	  global_level
	  * fixed_level
	  * task_grf->ed.Sccw[task_grf->ed.curetaccw];
	data[NUMBER_OF_AI_LINES*(n*m+i) + 1] = 
	  global_level
	  * fixed_level
	  * task_grf->ed.Sccw[(task_grf->ed.curetaccw + task_grf->ed.rotnp / 4 )% task_grf->ed.rotnp];
	//the line above is to add a shift of PI/2 to the signal
	data[NUMBER_OF_AI_LINES*(n*m+i) + 2] = 
	  global_level
	  * fixed_level
	  * task_grf->ed.Sccw[(task_grf->ed.curetaccw + task_grf->ed.rotnp / 12 * 5) % task_grf->ed.rotnp];
	//the line above is to add a shift of 5PI/6 to the signal
      }
    }
    break;

  case 0:
    //	1) generate data at position AOdata+NUMBER_OF_AI_LINES*totalAOdataWritten
    //its the same as in function rotation__add_next_nSamples_earr_contribution
    //but I avoid calling many times the my_matrix_product routine, so that the
    //calculation is even faster.
    for(n = 0 ; n < np ; n++){
      if(rotp > 0){
	task_grf->ed.curetaccw = ( task_grf->ed.curetaccw - 1) < 0 ? task_grf->ed.rotnp - 1 : task_grf->ed.curetaccw - 1;
	ell = task_grf->ed.rotell;
	rotp--;
      }
      else if(rotp < 0){
	task_grf->ed.curetaccw = ( task_grf->ed.curetaccw + 1) >= task_grf->ed.rotnp ? 0 : task_grf->ed.curetaccw + 1;
	ell = task_grf->ed.rotell;
	rotp++;
      }
      else{
	ell = 0.;
	task_grf->ed.cureta = ( task_grf->ed.cureta + 1) >= task_grf->ed.etatotnp ? 0 : task_grf->ed.cureta + 1;
      }
/*     if(stci == 0){ */
/* 	//every time I do a complete series I recalculate the static ellipse, */
/* 	//incrementing the ellipticity of a fixed amount */
/* 	task_grf->es.ell = task_grf->es.ell + task_grf->es.ellinc; */
/* 	c = (float64) cos(2*task_grf->es.eta); */
/* 	s = (float64) sin(2*task_grf->es.eta); */
/* 	task_grf->es.F[0] = c/SQ3 + s/3; */
/* 	task_grf->es.F[1] = 2*s/3; */
/* 	task_grf->es.F[2] = -c/SQ3 + s/3; */
/* 	task_grf->es.F[3] = s/SQ3 - c/3; */
/* 	task_grf->es.F[4] = -2*c/3; */
/* 	task_grf->es.F[5] = -s/SQ3 - c/3; */
/* 	stci = task_grf->es.np; */
/*       } */
      
      for(i = 0; i < 2*NUMBER_OF_AI_LINES; i++){//generating the actual M matrix for each of them
	//this is a linear approximation for ell << 1, check if it is sufficiently good
	M[i] = 
	  AmatrTrans[i] + 
	  task_grf->es.ell*task_grf->es.F[i] + 
	  /*task_grf->ed.ell[0]*/task_grf->ed.F[6*(task_grf->ed.cureta)+i]+
	  ell*task_grf->ed.Fccw[6*(task_grf->ed.curetaccw)+i];
	//if(test)
	//  win_event("task ed.F[%d] = %f",6*(task_grf->ed.cureta)+i,(float) task_grf->ed.F[6*(task_grf->ed.cureta)+i]);
      }
      //now multiply cir times the newly generated M matrix
      for(i = 0; i < m; i++){
	for(j = 0; j < NUMBER_OF_AI_LINES; j++){
	  data[NUMBER_OF_AI_LINES*(n*m+i) + j] = 0;
	  for(k = 0; k < 2; k++){
	    data[NUMBER_OF_AI_LINES*(n*m+i) +j] 
	      += (float64) global_level*task_grf->level * 
	      task_grf->c.cir[2*i + k] * 
	      M[NUMBER_OF_AI_LINES*k + j];
	  }
	}
      }
    }
    break;
    //here I finished generating the new data
  }
  test = 0;
  
  //	2) send to card data just generated
  
  /*********************************************/
  // DAQmx Write Code
  /*********************************************/
  DAQmxErrChk (DAQmxWriteAnalogF64(AOtaskHandle,nSamples,0,10.0,DAQmx_Val_GroupByScanNumber,data,&dataWritten,NULL));
  AOdataFlag++;
  totalAOdataWritten += dataWritten;
  
  if((AOdataFlag >= 2) || (totalAOdataWritten + nSamples > nSamples*2)){
    AOdataFlag = 0;
    totalAOdataWritten = 0;
  }

 Error:
  if( DAQmxFailed(error) ) {
    DAQmxGetExtendedErrorInfo(errBuff,2048);
    /*********************************************/
    // DAQmx Stop Code
    /*********************************************/
    if( AOtaskHandle ) {
      DAQmxStopTask(AOtaskHandle);
      DAQmxClearTask(AOtaskHandle);
      AOtaskHandle = 0;
    }
    dump_to_xv_log_file_with_date_and_time("DAQmx Error: %s\n",errBuff);
  }
  //win_event("write callback ended");
  return 0;
  
}

/**
This function correctly allocates the memory space for the data
 */
int task__allocate_memory_space(int ns)
{
  /* Generates AOData */
  if((AOdata = (float64*) realloc(AOdata,ns*NUMBER_OF_AI_LINES*sizeof(float64))) == NULL) { dump_to_xv_log_file_with_date_and_time("memory allocation error\ncould not reallocate AOdata in task  allocate memory space"); return -1;}
  return 0;
}

/**
This function correctly reallocates the memory space for the data
 */
int task__reallocate_memory_space(int ns)
{
  /* Generaes AOData */
  if((AOdata = (float64*) realloc(AOdata,ns*NUMBER_OF_AI_LINES*sizeof(float64))) == NULL) { dump_to_xv_log_file_with_date_and_time("memory allocation error\ncould not reallocate AOdata in task  reallocate memory space"); return -1;}
  return 0;
}

/**
This function correctly frees the memory space for the data
 */
int task__free_memory_space(void)
{
  free(AOdata);
  return 0;
}

/** This is the task to reset the value of the field to level and angle 
It controls directly the three lines and it sends a single value on each of them.

\param float level the value of driving voltage.
\param float angle the angle of orientation for the field (phase in degrees)


\author Francesco Mosconi
\version 04/06/07
 */
int task__static_field_task(float level, float angle)
{
  TaskHandle taskHandle;
  int i = 0;
  int32   error=0;
  char    errBuff[2048]={'\0'};
  float64     data[NUMBER_OF_AI_LINES] ;

  for(i = 0 ; i < NUMBER_OF_AI_LINES ; i++){
    data[i]= (float64) level*(sin((double) PI*i/NUMBER_OF_AI_LINES +
				  (double) PI/(2*NUMBER_OF_AI_LINES) + 
				  (double) angle*PI/180.0 ));
  }
  /*********************************************/
  // DAQmx Configure Code
  /*********************************************/

  DAQmxErrChk (DAQmxCreateTask("",&taskHandle));
  DAQmxErrChk (DAQmxCreateAOVoltageChan(taskHandle,"Dev2/ao0:2","",-10.0,10.0,DAQmx_Val_Volts,""));

  /*********************************************/
  // DAQmx Start Code
  /*********************************************/
  DAQmxErrChk (DAQmxStartTask(taskHandle));

  /*********************************************/
  // DAQmx Write Code
  /*********************************************/
  DAQmxErrChk (DAQmxWriteAnalogF64(taskHandle,1,0,10.0,DAQmx_Val_GroupByChannel,data,NULL,NULL));
   
 Error:
     if( DAQmxFailed(error) )
     DAQmxGetExtendedErrorInfo(errBuff,2048);
 if( taskHandle ) {
   /*********************************************/
    // DAQmx Stop Code
   /*********************************************/
   DAQmxStopTask(taskHandle);
   DAQmxClearTask(taskHandle);
   taskHandle = 0;
 }
 if( DAQmxFailed(error) )
   dump_to_xv_log_file_with_date_and_time("DAQmx Error: %s\n",errBuff);

 return 0;
}

/** This is the task do a finite number of turns. 
It controls directly the three lines and it sends sine wave of 1000 points
on each of them.

\param float level the value of driving voltage.
\param float angle the angle of orientation for the field (phase in degrees)


\author Francesco Mosconi
\version 04/06/07
 */
int task__generate_finite_rotation_data(void)
{
  int i,j;
  float64 fs = task__smplFreq;
  
  free(ccw);  
  free(cw);

  if((  ccw = (float64*) calloc(NUMBER_OF_AI_LINES*nprot,sizeof(float64))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not allocate ccw!");
  if((  cw = (float64*) calloc(NUMBER_OF_AI_LINES*nprot,sizeof(float64))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not allocate cw!");

  for (i = 0 ; i < nprot ; i++){
    for (j = 0 ; j < NUMBER_OF_AI_LINES ; j++)
    {
      ccw[i*NUMBER_OF_AI_LINES + j] = (float64) 
	(sin(
	     (double) 2*PI*i/nprot + //
	     (double) PI*j/NUMBER_OF_AI_LINES + //phase shift  between lines is PI/3
	     (double) PI/(2*NUMBER_OF_AI_LINES))); //add also a phase of PI/6 so that central line is line number 2
      cw[i*NUMBER_OF_AI_LINES + j] = (float64) 
	(sin(
	     (double) -2*PI*i/nprot + //
	     (double) PI*j/NUMBER_OF_AI_LINES + //phase shift  between lines is PI/3
	     (double) PI/(2*NUMBER_OF_AI_LINES))); //add also a phase of PI/6 so that central line is line number 2
    }
  }

  return 0;
}

int task__finite_rotation_field_task(float freq, float level, int num_turns)
{
  TaskHandle taskHandle;
  int i = 0;
  int32   error=0;
  char    errBuff[2048]={'\0'};
  frot = freq;
  float64 *data = NULL;

  nprot = (int) task__smplFreq/frot;
  
  if(ccw == NULL || cw == NULL)
    task__generate_finite_rotation_data();
  

  if((  data = (float64*) calloc(NUMBER_OF_AI_LINES*nprot , sizeof(float64))) == NULL) return dump_to_xv_log_file_with_date_and_time("Could not allocate data!");

  if(num_turns > 0)
    for (i = 0 ; i < NUMBER_OF_AI_LINES*nprot ; i++)
      data[i] = ccw[i]*level;
  else if(num_turns < 0){
    for (i = 0 ; i < NUMBER_OF_AI_LINES*nprot ; i++)
      data[i] = cw[i]*level;
    num_turns = -num_turns;
  }
  else
    return 0;

  /*********************************************/
  // DAQmx Configure Code
  /*********************************************/

  DAQmxErrChk (DAQmxCreateTask("",&taskHandle));
  DAQmxErrChk (DAQmxCreateAOVoltageChan(taskHandle,"Dev2/ao0:2","",-10.0,10.0,DAQmx_Val_Volts,""));
  DAQmxErrChk (DAQmxCfgSampClkTiming(taskHandle,"", task__smplFreq,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,nprot*num_turns));

  /*********************************************/
  // DAQmx Write Code
  /*********************************************/
  DAQmxErrChk (DAQmxWriteAnalogF64(taskHandle,nprot,0,10.0,DAQmx_Val_GroupByScanNumber,data,NULL,NULL));

  /*********************************************/
  // DAQmx Start Code
  /*********************************************/
  DAQmxErrChk (DAQmxStartTask(taskHandle));
  /*********************************************/
  // DAQmx Wait Code
  /*********************************************/
  DAQmxErrChk (DAQmxWaitUntilTaskDone(taskHandle,10.0));

 Error:
     if( DAQmxFailed(error) )
     DAQmxGetExtendedErrorInfo(errBuff,2048);
 if( taskHandle ) {
   /*********************************************/
    // DAQmx Stop Code
   /*********************************************/
   DAQmxStopTask(taskHandle);
   DAQmxClearTask(taskHandle);
   taskHandle = 0;
 }
 if( DAQmxFailed(error) )
   dump_to_xv_log_file_with_date_and_time("DAQmx Error: %s\n",errBuff);

 free(data);
 return 0;
}

int task__create_and_launch()
{
  int32   error=0, AObuffersize = 0;
  char    errBuff[2048]={'\0'};
  int i;

  for(i = 0 ; i< NUMBER_OF_AI_LINES*task__nSamples ; i++)
    if(AOdata+i == NULL) return dump_to_xv_log_file_with_date_and_time("encountered null data at position %d",i);

  if(AOdata == NULL)
    return -1;

  AOdataFlag = 0;
  totalAOdataWritten = 0;
  /*********************************************/
  // DAQmx Configure Code
  /*********************************************/

  DAQmxErrChk (DAQmxCreateTask("",&AOtaskHandle));
  DAQmxErrChk (DAQmxCreateAOVoltageChan(AOtaskHandle,"Dev2/ao0:2","",-10.0,10.0,DAQmx_Val_Volts,NULL));
  //win_event("task sampling frequency is: %f, task nSamples before write configure is : %d",task__smplFreq,task__nSamples);
  DAQmxErrChk (DAQmxCfgSampClkTiming(AOtaskHandle,"",task__smplFreq,DAQmx_Val_Rising,DAQmx_Val_ContSamps,task__nSamples));

  if(myBufferRegen){
    DAQmxErrChk (DAQmxRegisterEveryNSamplesEvent(AOtaskHandle,DAQmx_Val_Transferred_From_Buffer,task__nSamples/2,0,task__EveryNRefreshBufferCallback,NULL));
  /*********************************************/
  // DAQmx shutting off Buffer Regeneration
  /*********************************************/
    DAQmxErrChk (DAQmxSetWriteRegenMode(AOtaskHandle,DAQmx_Val_DoNotAllowRegen));
  }
  /*********************************************/
  // DAQmx Write Code
  /*********************************************/
  DAQmxErrChk (DAQmxWriteAnalogF64(AOtaskHandle,task__nSamples,0,10.0,DAQmx_Val_GroupByScanNumber,AOdata,NULL,NULL));

  DAQmxErrChk (DAQmxGetBufOutputBufSize(AOtaskHandle, &AObuffersize));
  /*********************************************/
  // DAQmx Start Code
  /*********************************************/

  DAQmxErrChk (DAQmxStartTask(AOtaskHandle));

 Error:
  if( DAQmxFailed(error) ){
    DAQmxGetExtendedErrorInfo(errBuff,2048);
    if( AOtaskHandle ) {
      /*********************************************/
      // DAQmx Stop Code
      /*********************************************/
      DAQmxStopTask(AOtaskHandle);
      DAQmxClearTask(AOtaskHandle);
      AOtaskHandle = 0;
      //win_event("error in AOtaskHandle, I stopped it!");
    }
    if( DAQmxFailed(error) )
      dump_to_xv_log_file_with_date_and_time("DAQmx Error: %s\n",errBuff);
  }
  return taskStillRunning = taskRunning = 1;
}

int task__stop(void)
{
  int32   error=0;
  char    errBuff[2048]={'\0'};
  if(updating_menu_state != 0)    return D_O_K;
  DAQmxGetExtendedErrorInfo(errBuff,2048);
    if( AOtaskHandle ) {
      /*********************************************/
      // DAQmx Stop Code
      /*********************************************/
      DAQmxStopTask(AOtaskHandle);
      DAQmxClearTask(AOtaskHandle);
      AOtaskHandle = 0;
      //win_event("Stopped AOtask");
    }
    if( DAQmxFailed(error) )
      dump_to_xv_log_file_with_date_and_time("DAQmx Error: %s\n",errBuff);
    //    win_printf("tasks succesfully stopped");
    return 0;
}

int task__stop_and_reset_to_zero(void)
{
  if(updating_menu_state != 0)    return D_O_K;
  global_level = 0.;
  Sleep(100);
  task__stop();
  global_level = 1.;
  return D_O_K;
}

int task__stop_and_restart(void)
{
  if(updating_menu_state != 0)    return D_O_K;
  task__stop();
  task__new_rfp_to_AOdata();
  return D_O_K;
}

#endif
