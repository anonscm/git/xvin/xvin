/** defines and typedefs declarations for the plug-in

\author Francesco Mosconi
*/

//defines
#ifndef _NIDAQMXWRAP_AIAO_DEFS_H_
#define _NIDAQMXWRAP_AIAO_DEFS_H_



#define LINE_MODE 2
#define ROTATION_MODE 3
#define ELL_MODE 4
#define ETA_MODE 5
#define STOP_AND_RESTART 0x2
#define UPDATE 0x1
#define NUMBER_OF_AI_LINES 3
#define NIDAQ_MAX_WRITE_FREQUENCY 1000000 // The max write frequency for NI6733 (i.e. 1/VAL is the time between 2 points, in seconds) 
#define SQ3 1.7320508075688772935274463415059 //the square root of 3. 
#define NP 3

//typedefs

typedef struct WAVEFORM_PARAMS
{
  float a;//the amplitude
  float o;//the offset
  float p;//the phase
  float f;//the frequency
  float d;//the duty cycle
  int   s;//the sign befor the argument (+ or -)
} w_p;

typedef struct rotating_field_parameter_circle
{
  float ebizarre;
  int num_turns; //number of periods the whole sequence must be repeated
  int num_points; //spatial frequency (points per turn)
  int apol; //flag for apolisation
  float epsilon; //epsilon parameter for doing a bit more than a turn
  int num_pos_turns; //number of turns in each direction
  int total_num_points; //the total number of points to be transmitted to the l_io.
  float dc;
  float spF;
  float spFapo1;
  float spFapo2;
  float64 *cir;//this contains the data for the non elliptical field, generated using the above parameters.
} rfpc;

/**this defines the static ellipse correction*/
typedef struct rotating_field_parameter_second_order_correction
{
  float ell; //ellipticity (first order definition: axis are defined as 1-e and 1+e.
  float ellinc;//ellipticity increment when doing more than one point
  float eta; //starting rotation phase
  int phasenum;
  int phaseden;
  float64 F[6];//this is the F matrix that I will use to calculate the ellipse correction 
} rfpe;


/**this defines the dynamic ellipse correction*/
typedef struct rotating_field_parameter_second_order_correction_array
{
  /*** definitions of the parameters
       used to generate an asyncronous rotation signal
   */
  ///the number of points per turn
  int rotnp;
  ///the ellipticity
  float rotell;
  ///the array of eta values on which to loop
  float64 *etaccw;
  ///the index of the current eta value
  int curetaccw;
  ///the array of sin values used for sync rotation
  float64 *Sccw;
  ///the array of matrix elements
  float64 *Fccw;

  /*** definitions of the parameters
       used to generate a syncronous angle modulation.
       NP indicates the number of assignable functions.
       Currently this is set to 3.
  */
  ///number of points per period
  int etanpa[NP];
  ///number of repetitions of each period before changing signal
  int etanreps[NP];
  ///ellipticity (first order definition: axis are defined as 1-e and 1+e)
  float ell[NP];
  ///function flags (chose constant, sine, triangle, square signal)
  int functionflag[NP];
  ///the number of entries in the etaarr array = sum_i etanpa[i]*etanpp
  int etatotnp;
  ///the array of eta values on which to loop
  float64 *etarr;
  ///pointer to the actual function
  float64 (*etafunction)(float x, w_p *par);
  ///global parameters of the generated signals (amplidute etc are the same for all)
  w_p etapar;
  ///the index of the current eta value
  int cureta;
  ///array of matrix elements used to calculate the ellipse correction, it is 6*etanp 
  float64 *F;
} rfpea;

typedef struct new_rotating_field_parameters
{
  /// frequency (in turns per second) of the high frequency signal
  float frequency;
  /// field amplitude
  float level;
  /// flag that indicates if the circular field has been modified
  int cmodflag;
  ///the circular field (fast rotation plus modulation)
  rfpc c;
  /// flag that indicates if the static ellipse correction has been modified
  int esmodflag;
  /// static elliptical correction;
  rfpe es;
  /// flag that indicates if the static ellipse correction has been modified
  int edmodflag;
  ///dynamic elliptical correction;
  rfpea ed;
  ///flag to stop and restart the task
  int stop_and_restart;
  ///flag to simply update the dynamic part
  int update;
} new_rfp;

#endif



