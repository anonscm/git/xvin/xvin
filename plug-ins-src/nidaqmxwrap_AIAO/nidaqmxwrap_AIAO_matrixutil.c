/** \file AOTF_functions.c
    \brief Sub-plug-in program for AOTF control with NIDAQ-M 6229 in Xvin.

    Controls function generator for different AOTF control situations.
    BEWARE : requests the NIDAQ plugin !

    \sa NIDAQ.c
    \author Adrien Meglio
    \version 01/11/2006
*/
#ifndef _NIDAQMXWRAP_AIAO_MATRIXUTIL_C_
#define _NIDAQMXWRAP_AIAO_MATRIXUTIL_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../logfile/logfile.h"
#include "nidaqmxwrap_AIAO.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

/**defines the matrix R(angle) that rotates a vector in the
   2d plane of an angle "angle" ccw
*/
int my_2d_rotation(float *input_v,float *output_v,float angle)
{
  if(input_v == NULL ||input_v + 1 == NULL)
    return -1;
  if(output_v == NULL ||output_v + 1 == NULL)
    output_v = (float *) calloc(2,sizeof(float));
  output_v[0] = cos(angle)*input_v[0] - sin(angle)*input_v[1];
  output_v[1] = sin(angle)*input_v[0] + cos(angle)*input_v[1];
  return 0;
}

/**defines the matrix A(e) that deforms the axis of the plane in order to transform circles into ellipses
 */
int my_2d_ellipse(float *input_v,float *output_v,float ell)
{
  if(input_v == NULL ||input_v + 1 == NULL)
    return -1;
  if(output_v == NULL ||output_v + 1 == NULL)
    output_v = (float *) calloc(2,sizeof(float));
  output_v[0] = (1+ell)*input_v[0];
  output_v[1] = (1-ell)*input_v[1];
  return 0;
}

/**This is equivalent to doing R(a).A(e).R(-a) and it results in to turn
   a circle into an ellipse with the major axis turned of an angle a ccw
   suppose a>0 and e>0
*/
int my_2d_rotellipse(float *input_v,float *output_v,float ell,float angle)
{
  if(input_v == NULL ||input_v + 1 == NULL)
    return -1;
  if(output_v == NULL ||output_v + 1 == NULL)
    output_v = (float *) calloc(2,sizeof(float));
  output_v[0] = (1 + ell*cos(2*angle))*input_v[0] + ell*sin(2*angle)*input_v[1];
  output_v[1] = ell*sin(2*angle)*input_v[0] + (1 - ell*cos(2*angle))*input_v[1];
  return 0;
}

float my_scalar_product(float *v_1,float *v_2)
{
  return v_1[0] * v_2[0] + v_1[1] * v_2[1];
}

/**This calculates the product of two matrices m1 and m2 of sizes [m1sz1 x m1sz2] and [m2sz1 x m2sz2] respectively
   It assumes that the memory size allocated for mout is correct!!!
*/
int my_matrix_product(float64 prefactor, float64 *m1, int m1sz1, int m1sz2, float64 *m2, int m2sz1, int m2sz2, float64 *mout)
{
  int i = 0, j = 0, k = 0;
  
  if(m1sz2 != m2sz1) return dump_to_xv_log_file_with_date_and_time("size mismatch :\n"
				       "function called with args:\n"
				       "prefactor = %lf\n"
				       "m1sz1 = %d\n"
				       "m1sz2 = %d\n"
				       "m2sz1 = %d\n"
				       "m2sz2 = %d\n"
				       "matrices cannot be multiplied"
				       ,prefactor,m1sz1,m1sz2,m2sz1,m2sz2);
  if(mout+m1sz1*m2sz2 == NULL) return dump_to_xv_log_file_with_date_and_time("memory has not been correctly allocated");
  
  for(i = 0; i < m1sz1; i++){
    for(j = 0; j < m2sz2; j++){
      mout[i*m2sz2 + j] = 0;
      for(k = 0; k < m1sz2; k++){
	mout[i*m2sz2 +j] += prefactor*m1[i*m1sz2 + k]*m2[k*m2sz2 + j];
      }
    }
  }
  return 0;
}

#endif
