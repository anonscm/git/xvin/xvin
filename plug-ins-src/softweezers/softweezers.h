#ifndef _SOFTWEEZERS_H_
#define _SOFTWEEZERS_H_

PXV_FUNC(int, do_softweezers_rescale_plot, (void));
PXV_FUNC(MENU*, softweezers_plot_menu, (void));
PXV_FUNC(int, do_softweezers_rescale_data_set, (void));
PXV_FUNC(int, softweezers_main, (int argc, char **argv));
#endif

