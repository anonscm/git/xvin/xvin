/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _SOFTWEEZERS_C_
#define _SOFTWEEZERS_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled



#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "softweezers.h"
//place here other headers of this plugin 
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 
# include "xvplot.h"

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "softweezers.h"


int	log_softweezers = -1;


int avg_periodic_signal(pltreg *pr, O_p *op, d_s *dsi, int npts_out, int n_start, int n_end, int with_er)
{
  register int i, j, k;
  double	sy, sy2, sx;
  d_s *ds = NULL;

  if (op == NULL)
    {
      op = create_and_attach_one_plot(pr, npts_out, npts_out, 0);
      if (op == NULL)    return win_printf_OK("I can't create plot !");
      ds = op->dat[0];
    }
  else ds = create_and_attach_one_ds(op, npts_out, npts_out, 0);
  if (ds == NULL)    return win_printf_OK("I can't create plot !");
  ds->ny = npts_out;	
  if (with_er)
    {
      if ((alloc_data_set_y_error(ds) == NULL) || (ds->ye == NULL))
	return win_printf_OK("I can't create errors !");
    }
  for (i = 0; i < npts_out; i++)
    {
      for (k = 0, j = i + n_start, sx = sy = 0; j < n_end; j+= npts_out, k++)
	  sy += dsi->yd[j];
      if (k != 0)	  sy /= k;
      ds->xd[i] = i;
      ds->yd[i] = sy;
      if (with_er)
	{
	  for (k = 0, j = i + n_start, sy2 = 0; j < n_end; j+= npts_out, k++)
	    sy2 += (dsi->yd[j] - sy) * (dsi->yd[j] - sy);
	  k = (k > 1) ? k - 1 : k;		
	  sy2 = (k != 0) ? sy2/(k) : sy2;			
	  sy2 = sqrt(sy2);	
	  ds->ye[i] = sy2;	
	}		
    }
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Periodic averaged starting at index %d "
		    "period size %d number of averaging %d",
		    n_start,npts_out,(n_end - n_start) / npts_out);
  return 0;
}  

float square_wave(float t, float A0, float A1, float per, float ldec)
{
  float fper2 = per/2, tmp;

  t += per;
  t -= ((int)(t/per)) * per; 


  if (t < fper2)	  
    {
      tmp = (A1 - A0)*exp(-t/ldec) + A0;
      //if (max - min > 0) dsb->yd[i] = (dsb->yd[i] < min) ? min : dsb->yd[i];
      //dsb->yd[i] = (dsb->yd[i] > min) ? min : dsb->yd[i];
    }
  else
    {
      t -= fper2;
      tmp = (A0-A1) * exp(-t/ldec) + A1;
      //if (max - min > 0)   dsb->yd[i] = (dsb->yd[i] > max) ? max : dsb->yd[i];
      //dsb->yd[i] = (dsb->yd[i] < max) ? max : dsb->yd[i];
    }
  return tmp;
}


int find_square_phase(O_p *op, d_s *dsi, int start, int per, int n_per, int skip, int ldecn, 
  float ldec,  float ldecinc, float *offset, float *ldecb)
{
  register int i, j, k;
  int nf, nmin, nmax, nxi, ns, ne, ixi, iminxi = 0, ilec, iminminxi = 0;
  float min, max, off, kf, tmp, fper2, minxi, minminxi, ldectmp = 0, mindec = -1;
  d_s *ds, *dsb, *dsxi, *dsxildec;


  if (dsi == NULL) return 1;
  nf = dsi->nx;
  if ((ds = create_and_attach_one_ds(op, n_per*per, n_per*per, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  if ((dsb = create_and_attach_one_ds(op, n_per*per, n_per*per, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  nxi = 200*skip;
  if ((dsxi = build_data_set(nxi, nxi)) == NULL)
    return win_printf_OK("cannot create plot !");


  if ((dsxildec = build_data_set(2*ldecn+1, 2*ldecn+1)) == NULL)
    return win_printf_OK("cannot create plot !");

    for (j = start, i = 0, nmin = nmax = 0, min = max = 0; j < nf && i < ds->nx; j++, i++)
    {
      ds->yd[i] = dsi->yd[j];
      k = i % per;
      if ((k > skip) && (k < (per/2 - skip)))
	{
	  max += ds->yd[i];
	  nmax++;
	}
      else if ((k > (skip + per/2)) && (k < (per - skip)))
	{
	  min += ds->yd[i];
	  nmin++;
	}
      ds->xd[i] = dsi->xd[j];
    }
  if (nmin > 0) min /= nmin;
  if (nmax > 0) max /= nmax;

  //win_printf("max %g min %g",max,min);
  ns = per/4;
  ne = (n_per*per) - ns;
  fper2 = ((float)per)/2;

  for (ilec = 0, minminxi = -1; ilec < (2*ldecn + 1); ilec++)
  //for (ilec = 2*ldecn, minminxi = -1; ilec >= 0; ilec--)
   {
      ldectmp = ldec + (ilec - ldecn) * ldecinc;
      for (ixi = nxi-1, minxi = -1; ixi >= 0; ixi--)
	{
	  //ixi = 0;
	  //win_scanf("ixi %d",&ixi);
	  off = (float)ixi/100;
	  off -= skip;
	  for (j = start, i = 0; j < nf && i < ds->nx; j++, i++)
	    {
	      k = i;
	      kf = off + i + per;
	      dsb->yd[i] = square_wave(kf, max, min, per, ldectmp);
	      dsb->xd[i] = dsi->xd[j];
	    }
	  dsxi->xd[ixi] = off;
	  for (i = ns, dsxi->yd[ixi] = 0;  i < ne; i++)
	    {
	      tmp = dsb->yd[i] - ds->yd[i];
	      tmp *= tmp;
	      dsxi->yd[ixi] += tmp;
	    }
	  if (minxi < 0 || dsxi->yd[ixi] < minxi)
	    {
	      iminxi = ixi;
	      minxi = dsxi->yd[ixi];
	    }
	}
      dsxildec->xd[ilec] = iminxi;
      dsxildec->yd[ilec] = minxi;
      if (minminxi < 0 || minxi < minminxi)
	{
	  iminminxi = iminxi;
	  mindec = ldectmp; 
	  minminxi = minxi;
	}
    }
      
  off = (float)iminminxi/100;
  off -= skip;
  win_printf("off %g xi2 %g ldec %g",off,minminxi,mindec);
  *ldecb = mindec;
  for (j = start, i = 0; j < nf && i < ds->nx; j++, i++)
    {
      k = i;
      kf = off + i + per;
      dsb->yd[i] = square_wave(kf, max, min, per, mindec);
      dsb->xd[i] = dsi->xd[j];
    }



  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Burst start %d period %d nper %d",start,per,n_per);
  set_ds_treatement(dsb,"Burst start %d period %d nper %d real start %g xi2 %g ldec %g",start,per,n_per,(-off)+start,minminxi,mindec);
  /* refisplay the entire plot */

  if (offset != NULL) *offset = off;
  free_data_set(dsxi);
  free_data_set(dsxildec);
  return 0;
}



/************************************************************************/
/* project the y component on one Fourier mode (cosine and sine) */
/************************************************************************/
int one_mode_and_derivate(pltreg *pr, O_p *op, d_s *dsi, int kx, float dx, float dt, float *As, 
			  float *Sig, float *phis, float *Ortho, float *Deriv)
{ 	
  register int i;
  d_s  *dsdr = NULL, *dsdi = NULL, *dsdp = NULL, *dsdv = NULL;     // initial and destination datasets
  int     nx;
  double co, si, phi, tmp, A;

  nx = dsi->nx; 
  if (nx<2) return win_printf_OK("not enough points in dataset!");
  
  dsdr = create_and_attach_one_ds(op, nx, nx, 0);
  if (dsdr == NULL)    	return win_printf_OK("Cannot allocate dataset");
  inherit_from_ds_to_ds(dsdr, dsi);
  if (kx > 0 && kx < nx/2) 
    {
      dsdi = create_and_attach_one_ds(op, nx, nx, 0);
      if (dsdi == NULL)    	return win_printf_OK("Cannot allocate dataset");
      inherit_from_ds_to_ds(dsdi, dsi);
    }
  for (i = 0, co = si = 0; i < nx; i++)
    {
      phi = M_PI * 2 * (dx + i);
      phi *= kx;
      phi /= nx;
      co += dsi->yd[i] * cos(phi);
      si += dsi->yd[i] * sin(phi);
    }
  co /= nx; si /= nx;
  if (kx > 0 && kx < nx/2)
    {
      co *= 2;
      si *= 2;
    }
  for (i = 0; i < nx; i++)
    {
      phi = M_PI * 2 * (dx + i);
      phi *= kx;
      phi /= nx;
      dsdr->yd[i] =  co * cos(phi);
      dsdr->xd[i] = dsi->xd[i];
    }
  for (i = 0; dsdi != NULL && i < nx; i++)
    {
      phi = M_PI * 2 * (dx + i);
      phi *= kx;
      phi /= nx;
      dsdi->yd[i] =  si * sin(phi);
      dsdi->xd[i] = dsi->xd[i];
    }
  set_ds_treatement(dsdr,"Cosine projection of mode %d = %g, dx = %g phi %g radians (%g degrees)",
		    kx,co,dx,atan2(si,co),(atan2(si,co)*180)/M_PI);
  if (dsdi != NULL)
    set_ds_treatement(dsdi,"Sine projection of mode %d = %g, dx %g phi %g radians (%g degrees)",
		      kx,si,dx,atan2(si,co),(atan2(si,co)*180)/M_PI);
  
  *Sig = sqrt(co*co + si*si);
  
  dsdp = create_and_attach_one_ds(op, nx, nx, 0);
  if (dsdp == NULL)    	return win_printf_OK("Cannot allocate dataset");
  inherit_from_ds_to_ds(dsdp, dsi);
  if (si != 0)
    {
      tmp = co*co/si;
      tmp *= tmp;
      A = sqrt(tmp);
      tmp += co*co;
      tmp = sqrt(tmp);
      A += si;
    }
  else A = tmp = 0;
  for (i = 0; i < nx; i++)
    {
      phi = M_PI * 2 * (dx + i);
      phi *= kx;
      phi /= nx;
      dsdp->yd[i] =  co * co * sin(phi);
      dsdp->yd[i] -=  co * si * cos(phi);
      if (si != 0) dsdp->yd[i] /=  si;
      dsdp->xd[i] = dsi->xd[i];
    }
  *As = A;
  set_ds_treatement(dsdp,"orthogonal component of mode %d = %g, dx = %g phi %g radians (%g degrees)"
		    " total amp = %g",kx,tmp,dx,atan2(si,co),(atan2(si,co)*180)/M_PI,A);
  *Ortho = tmp;
  *phis = (atan2(si,co)*180)/M_PI;
  dsdv = create_and_attach_one_ds(op, nx, nx, 0);
  if (dsdv == NULL)    	return win_printf_OK("Cannot allocate dataset");
  inherit_from_ds_to_ds(dsdv, dsi);	
  tmp = (M_PI * 2 * kx)/(dt*nx);
  for (i = 0; i < nx; i++)
    {
      phi = M_PI * 2 * (dx + i);
      phi *= kx;
      phi /= nx;
      dsdv->yd[i] =  tmp * si * cos(phi);
      dsdv->yd[i] -=  tmp * co * sin(phi);
      dsdv->xd[i] = dsi->xd[i];
    }
  A = tmp * sqrt(co * co + si * si);
  *Deriv = A;
  set_ds_treatement(dsdv,"Velocity component of mode %d = %g, dx %g phi %g radians (%g degrees) dt %g s",
		    kx,A,dx,atan2(si,co),(atan2(si,co)*180)/M_PI,dt);


  return 0;    
}



int do_softweezers_find_square_phase(void)
{
  register int i, j, k;
  O_p *op = NULL;
  int nf, nmin, nmax, nxi, ns, ne, ixi, iminxi = 0, ilec, iminminxi = 0;
  static int start = 0, per = 150, n_per = 3, skip = 5, ldecn = 20;
  static float ldec = 1, ldecinc = 0.05;
  float min, max, off, kf, tmp, fper2, minxi, minminxi, ldectmp, mindec = -1;
  d_s *ds, *dsi, *dsb, *dsxi, *dsxildec;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine finds the square signal used to specify the signal phase in Francesco calibration");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("To find phase of the square signal burst\n"
		"indicate the burst first point starting index %8d\n"
		"what is the number of points in one period %8d\n\n"
		"what is the number of periods in the burst %8d\n\n"
		"number of points to skip at transition to find max %8d\n\n"
		"what is the estimated decay length at transition %8f\n\n"
		"what is the estimated decay length increment for optimisation %8f\n\n"		
		"what is the number of steps optimisation arround estimated position %8d\n\n"		
		,&start,&per,&n_per,&skip, &ldec,&ldecinc,&ldecn);
  if (i == CANCEL)	return OFF;

  if ((ds = create_and_attach_one_ds(op, n_per*per, n_per*per, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  if ((dsb = create_and_attach_one_ds(op, n_per*per, n_per*per, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  nxi = 200*skip;
  if ((dsxi = create_and_attach_one_ds(op, nxi, nxi, 0)) == NULL)
    return win_printf_OK("cannot create plot !");


  if ((dsxildec = create_and_attach_one_ds(op, 2*ldecn+1, 2*ldecn+1, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  for (j = start, i = 0, nmin = nmax = 0, min = max = 0; j < nf && i < ds->nx; j++, i++)
    {
      ds->yd[i] = dsi->yd[j];
      k = i % per;
      if ((k > skip) && (k < (per/2 - skip)))
	{
	  max += ds->yd[i];
	  nmax++;
	}
      else if ((k > (skip + per/2)) && (k < (per - skip)))
	{
	  min += ds->yd[i];
	  nmin++;
	}
      ds->xd[i] = dsi->xd[j];
    }
  if (nmin > 0) min /= nmin;
  if (nmax > 0) max /= nmax;

  ns = per/4;
  ne = (n_per*per) - ns;
  fper2 = ((float)per)/2;

  for (ilec = 0, minminxi = -1; ilec < (2*ldecn + 1); ilec++)
  //for (ilec = 2*ldecn, minminxi = -1; ilec >= 0; ilec--)
    {
      ldectmp = ldec + (ilec - ldecn) * ldecinc;
      for (ixi = nxi-1, minxi = -1; ixi >= 0; ixi--)
	{
	  //ixi = 0;
	  //win_scanf("ixi %d",&ixi);
	  off = (float)ixi/100;
	  off -= skip;
	  for (j = start, i = 0; j < nf && i < ds->nx; j++, i++)
	    {
	      //k = i % per;
	      k = i;
	      kf = off + i + per;
	      kf -= ((int)(kf/per)) * per; 
	      if (kf < fper2)	  
		{
		  dsb->yd[i] = (min - max)*exp(-kf/ldectmp) + max;
		  dsb->yd[i] = (dsb->yd[i] < min) ? min : dsb->yd[i];
		}
	      else
		{
		  kf -= fper2;
		  dsb->yd[i] = (max-min) * exp(-kf/ldectmp) + min;
		  dsb->yd[i] = (dsb->yd[i] > max) ? max : dsb->yd[i];
		}
	      dsb->xd[i] = dsi->xd[j];
	    }
	  dsxi->xd[ixi] = off;
	  for (i = ns, dsxi->yd[ixi] = 0;  i < ne; i++)
	    {
	      tmp = dsb->yd[i] - ds->yd[i];
	      tmp *= tmp;
	      dsxi->yd[ixi] += tmp;
	    }
	  if (minxi < 0 || dsxi->yd[ixi] < minxi)
	    {
	      iminxi = ixi;
	      minxi = dsxi->yd[ixi];
	    }
	}
      dsxildec->xd[ilec] = iminxi;
      dsxildec->yd[ilec] = minxi;
      if (minminxi < 0 || minxi < minminxi)
	{
	  iminminxi = iminxi;
	  mindec = ldectmp; 
	  minminxi = minxi;
	}
    }
      
  off = (float)iminminxi/100;
  off -= skip;
  win_printf("off %g xi2 %g ldec %g",off,minminxi,mindec);
  for (j = start, i = 0; j < nf && i < ds->nx; j++, i++)
    {
      //k = i % per;
      k = i;
      kf = off + i + per;
      kf -= ((int)(kf/per)) * per; 
      if (kf < fper2)	  
	{
	  dsb->yd[i] = (min - max)*exp(-kf/mindec) + max;
	  dsb->yd[i] = (dsb->yd[i] < min) ? min : dsb->yd[i];
	}
      else
	{
	  kf -= fper2;
	  dsb->yd[i] = (max-min) * exp(-kf/mindec) + min;
	  dsb->yd[i] = (dsb->yd[i] > max) ? max : dsb->yd[i];
	}
      dsb->xd[i] = dsi->xd[j];
    }



  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  set_ds_treatement(ds,"Burst start %d period %d nper %d",start,per,n_per);
  set_ds_treatement(dsb,"Burst start %d period %d nper %d real start %g xi2 %g ldec %g",start,per,n_per,off+start,minminxi,mindec);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int remove_phase_jump(O_p *op, d_s *dsi)
{ 	
  register int i;
  d_s  *dsd = NULL;
  int     nx;
  double tmp;
  
  
  nx = dsi->nx; 
  dsd = create_and_attach_one_ds(op, nx, nx, 0);
  if (dsd == NULL)    	return win_printf_OK("Cannot allocate dataset");
  inherit_from_ds_to_ds(dsd, dsi);
  dsd->yd[0] =    dsi->yd[0];
  dsd->xd[0] =    dsi->xd[0];
  for (i = 1, tmp = 0; i < nx; i++)
    {
      if ((dsi->yd[i] - dsi->yd[i-1]) > M_PI)
	  tmp -= 2*M_PI;
      else if ((dsi->yd[i] - dsi->yd[i-1]) < -M_PI)
	  tmp += 2*M_PI;
      dsd->yd[i] = dsi->yd[i] + tmp;
      dsd->xd[i] = dsi->xd[i];      
    }
  set_ds_treatement(dsd,"Phase jump removed");
  return 0;
}

int do_remove_phase_jump(void)
{ 	
  register int i;
  pltreg *pr;         // plot region
  O_p *op;      // initial plot
  d_s *dsi;
  int     nx;
  
  if (updating_menu_state != 0)	return D_O_K;	
  if (key[KEY_LSHIFT])   return win_printf_OK("This routine remove phase jump in angle traces\n"
					      "A new dataset is created");    
  
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)  
    return(win_printf_OK("Could not find plot data"));
  
  nx = dsi->nx; if (nx<2) return(win_printf_OK("not enough points in dataset!"));
  remove_phase_jump(op, dsi);
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int do_softweezers_find_square_phase_avg_sinus(void)
{
  O_p *op = NULL;
  int nf, i, istartcor, index;
  static int start = 0, per = 150, n_per = 3, skip = 5, ldecn = 10, kx = 1;
  static float ldec = .8, ldecinc = 0.05;
  float offset, startcor, dx, dt = 0.04, A, Sig, Ortho, Deriv, phi, ldecm; 
  static int 	npts_in, npts_out = 125, n_persig = 8, n_end, stop_after_N = 0, with_er = 1, xv_sav = 1;
  d_s *dsi;
  pltreg *pr = NULL;
  char	s[4096];
  static int auto_file_index = 0;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine finds the square signal used to specify the signal phase in Francesco calibration");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  index = RETRIEVE_MENU_INDEX;
  nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("To find phase of the square signal burst\n"
		"indicate the burst first point starting index %8d\n"
		"what is the number of points in one period %8d\n\n"
		"what is the number of periods in the burst %8d\n\n"
		"number of points to skip at transition to find max %8d\n\n"
		"what is the estimated decay length at transition %8f\n\n"
		"what is the estimated decay length increment for optimisation %8f\n\n"		
		"what is the number of steps optimisation arround estimated position %8d\n\n" 
		,&start,&per,&n_per,&skip, &ldec,&ldecinc,&ldecn);
  if (i == CANCEL)	return OFF;


  find_square_phase(op, dsi, start, per, n_per, skip, ldecn, ldec, ldecinc, &offset, &ldecm);

  startcor = (float)start - offset;
  //startcor = (float)start;    // to check
  istartcor = (int)(startcor + 0.5);
  dx = startcor - istartcor;
  //dx = offset;     // to check
  dx = -dx;

  win_printf("start cor %d dx %g",istartcor,dx);

  npts_in = dsi->nx;			/* this is the number of points in the data set */
  
  sprintf(s,"There are %d points in the source dataset.\n"
	  "the routine will average a periodic signal\n"
	  "starting at point of index %d\n"
	  "one period extending over %%8d points\n"
	  "process signal until the end of dataset %%R or stops after N averaging periods %%r\n"
	  "If you select N periods, indicate the N value %%6d (number of full periods)\n "
	  "Do you want statistical error bars? No %%R Yes %%r\n",npts_in,istartcor);
  i = win_scanf(s,&npts_out,&stop_after_N,&n_persig,&with_er); 
  if (i==CANCEL) return refresh_plot(pr, UNCHANGED);
  if (npts_out <= 0) return win_printf_OK("N=%d is prohibited !!!",npts_out);
  
  istartcor +=  per * n_per;  // we jump over square burst
  istartcor +=  npts_out;  // we jump over one sinusoidal period to remove transient


  n_end = (stop_after_N) ? istartcor + n_persig * npts_out : npts_in;
  n_end = (n_end < npts_in) ? n_end : npts_in;


  avg_periodic_signal(pr, NULL, dsi, npts_out, istartcor, n_end, with_er);
  op = pr->o_p[pr->cur_op];
  dsi = op->dat[0];

  i = win_scanf("Computing projections and viscous drag\n"
		"mode number kx %8d\ndt between points (seconds) %8f\n"
		"Save in a XV plot No %R yes %r\n",&kx,&dt,&xv_sav);
  if (i == CANCEL) return refresh_plot(pr, UNCHANGED);
  if (dt <= 0) return win_printf_OK("Impossible value for dt %g",dt);
  
  one_mode_and_derivate(pr, op, dsi, kx, dx, dt, &A, &Sig, &phi, &Ortho, &Deriv);

  refresh_plot(pr, UNCHANGED);



  if (xv_sav)	
    {
      if (log_softweezers == -1 || index == 1) 	
	{
	  if (do_create_xvplot_file(NULL) == NULL)	
	      return 0;
	  auto_file_index = 0;
	  log_softweezers++;
	  dump_to_current_xvplot_file("%%index\tSigPer\tkx\tNavg\tstart\tstartcor\tdx"
				      "\tSyncPer\tNSync\tskip\tldec\tA\tSig\tOrtho\tphi\tDeriv\tdt");
	}
      dump_to_current_xvplot_file( "%d\t%d\t%d\t%d\t%d\t%d\t%g\t%d\t%d\t%d"
				   "\t%g\t%g\t%g\t%g\t%g\t%g\t%g",auto_file_index,npts_out,kx,(n_end - istartcor)/npts_out,start,istartcor,
				   dx,per,n_per,skip,ldecm,A,Sig,Ortho,phi,Deriv,dt);
      auto_file_index++;
    }

  return D_O_K;
}

MENU *softweezers_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"Find square signal phase", do_softweezers_find_square_phase,NULL,0,NULL);
	add_item_to_menu(mn,"Find square signal phase + sinus", do_softweezers_find_square_phase_avg_sinus,NULL,0,NULL);
	add_item_to_menu(mn,"Find square signal phase + sinus (new file)", do_softweezers_find_square_phase_avg_sinus,NULL,MENU_INDEX(1),NULL);
	add_item_to_menu(mn,"Remove phase jump", do_remove_phase_jump,NULL,0,NULL);

	return mn;
}

int	softweezers_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "softweezers", NULL, softweezers_plot_menu(), 0, NULL);
	return D_O_K;
}

int	softweezers_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "softweezers", NULL, NULL);
	return D_O_K;
}
#endif

