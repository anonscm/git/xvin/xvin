#ifndef _CYCLES_H_
#define _CYCLES_H_

#define	FILE_BINARY		0x0100
#define	FILE_NI4472		0x0200


PXV_FUNC(MENU*, cycles_plot_menu, 						(void));
PXV_FUNC(int, cycles_main, 								(int argc, char **argv));
PXV_FUNC(int, cycles_unload,							(int argc, char **argv));
PXV_FUNC(int, do_find_boundaries,						(void));
PXV_FUNC(int, do_hist_periodic_build_from_ds,			(void));
PXV_FUNC(int, do_correct_offset_in_files,		        (void));
PXV_FUNC(int, do_correct_contrast_in_files,		        (void));
PXV_FUNC(int, do_compute_fluctuations_in_files,		    (void));
PXV_FUNC(int, do_filter_bin_files,						(void));
PXV_FUNC(int, do_deriv_bin_files,						(void));
PXV_FUNC(int, do_compute_integrated_response,		    (void));
PXV_FUNC(int, do_hist_periodic_build_from_files, 		(void));
PXV_FUNC(int, do_stats_periodic_build_from_M_dep_files, (void));
PXV_FUNC(int, do_average_over_cycles_from_files,		(void));
PXV_FUNC(int, do_correl_over_cycles_from_files,			(void));
PXV_FUNC(int, do_correl_from_files,						(void));
PXV_FUNC(int, do_average_over_cycles,					(void));
PXV_FUNC(int, do_cycles_correl_function,				(void));
PXV_FUNC(int, do_plot_psd_from_ds,						(void));
PXV_FUNC(void, spit,									(char *message));

#endif

