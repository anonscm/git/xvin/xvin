/**-name: cycles
  *-author: N. Garnier
  *-function: Functions to extract info/measurements on periodic signals
  */

#ifndef _CYCLES_C_
#define _CYCLES_C_

#include "allegro.h"
#include "xvin.h"
#include "xv_tools_lib.h"

#include <gsl/gsl_histogram.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_statistics.h>
#include <fftw3.h> // because ../resistance/resistance.h uses type fftw_complex

//for plot_psd_cycles
#include "fftw3_lib.h"
#include "fft_filter_lib.h"

/* If you include other plug-ins header do it here*/ 
#include "../hist/hist.h"
#include "../inout/inout.h"
#include "../find/find.h"
#include "../diff/diff.h"


/* But not below this define */
#define BUILDING_PLUGINS_DLL
#include "cycles.h"

#ifndef k_b
#define k_b 1.3806503e-23	// m2 kg s-2 K-1
#endif

#ifndef Temperature
#define Temperature (25.+273.)		// K
#endif

//#define kT_Fred 4.005443325e-021 // le kT utilisé par Frédéric
#define C 5.15e-4 // raideur du ressort


//List of functions which compute some works...
double compute_work_classic_from_power(float *M, float *power, int i_start, int tau, int indice_reference)
{	double	a_tau=0.;
	register int j2;
				
	for (j2=0; j2<tau; j2++)
	{	a_tau += (double)power[i_start + j2];		// average over tau points
	}
	return(a_tau);
}			
				
   
double compute_work_classic_from_M_dep(float *excitation, float*theta, int i_start, int tau, int indice_reference)
{	double a_tau=0.;
	register int j2;

	a_tau=0.;
	for (j2=0; j2<tau; j2++)
	{	a_tau += - ( (double)excitation[i_start+j2] )
					*((double)theta[i_start + j2] - theta[i_start + j2-1]);	// average over tau points
	}
	return(a_tau);
}

double compute_work_classic_from_M_ddep(float *excitation, float*theta, int i_start, int tau, int indice_reference)
{	double a_tau=0.;
	register int j2;

	a_tau=0.;
	for (j2=0; j2<tau; j2++)
	{	a_tau += -(double)(excitation[i_start+j2]*theta[i_start+j2]);	// average over tau points
	}
	return(a_tau);
}

// vinzou = vanZon
double compute_work_vinzou_from_M_dep(float *excitation, float*theta, int i_start, int tau, int indice_reference)
{	double a_tau=0.;
	register int j2;

	a_tau=0.;
	for (j2=0; j2<tau; j2++)
	{	a_tau += - ( (double)excitation[i_start+j2] - (double)excitation[i_start - indice_reference] )
					*((double)theta[i_start + j2] - (double)theta[i_start + j2-1]);	// average over tau points
	}
	return(a_tau);
}

// vinzou = vanZon
double compute_work_vinzou_from_M_ddep(float *excitation, float*theta, int i_start, int tau, int indice_reference)
{	double a_tau=0.;
	register int j2;

	a_tau=0.;
	for (j2=0; j2<tau; j2++)
	{	a_tau += - ( (double)excitation[i_start+j2] - (double)excitation[i_start - indice_reference] )
					*((double)theta[i_start + j2] );	// average over tau points
	}
	return(a_tau);
}

double compute_work_Jarzynski_newref_from_M_dep(float *excitation, float*theta, int i_start, int tau, int indice_reference)
{	double a_tau=0.;
	register int j2;

	a_tau=0.;
	for (j2=0; j2<tau; j2++)
	{	a_tau +=  ( (double)theta[i_start+j2] - (double)excitation[i_start+j2]/C )
					*((double)excitation[i_start + j2] - (double)excitation[i_start + j2-1]);	// average over tau points
	}
	return(a_tau);
}
//List of functions which compute some works...



//Function which find the first, middle or last point of a cycle
int do_find_boundaries(void)
{	O_p 	*op1 = NULL;
	d_s 	*ds1, *ds2=NULL;
	pltreg *pr = NULL;
	float 	y_min=0, y_max=0;
static int 	n_last=2, n_more, n_shift, N_shift_auto;
static int	point_position=2, plateau_position=1;
static int	bool_search_indices=1, bool_remove_bounds=1, bool_shift_indices, bool_add_more_indices;
	int		ny;
	int		index;
	int		*ind=NULL;
	int		n_cycles;
	register int i;
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])	
		return win_printf_OK("This routine finds or imposes indices in a dataset.\n"
        					"Indices correspond to begining or ending of a cycle, or something else.\n\n"
							"a new dataset is created.");
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)	return win_printf_OK("cannot plot region");
	ny = ds1->ny;	// number of points in time series 

	if (index !=0)
	{ 	if (index & FIND_BEGIN)	point_position = 0;
		if (index & FIND_MIDDLE)	point_position = 1;
		if (index & FIND_END)		point_position = 2;
		if (index & FIND_LOWER)	plateau_position = 0;
		if (index & FIND_UPPER)	plateau_position = 1;
	}
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices, &point_position, &plateau_position, 
						&bool_remove_bounds, &n_last, &bool_shift_indices, &n_shift, 
						&bool_add_more_indices, &n_more, &N_shift_auto);
	if (i==CANCEL) return(OFF);
	
	if (bool_search_indices==1)
	{ 	i=find_search_domain(ds1->yd, ds1->ny, plateau_position, &y_min, &y_max);
	}
	
	ind = find_set_of_indices(ds1->yd, ny, 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
	if (ind==NULL) return(win_printf_OK("No points left. Found indices were probably only at the boundaries..."));
	
	ds2 = create_and_attach_one_ds (op1, n_cycles, n_cycles, 0);
	for (i=0; i<n_cycles; i++)
	{	ds2->xd[i] = ds1->xd[ind[i]];
		ds2->yd[i] = ds1->yd[ind[i]];
	}
	inherit_from_ds_to_ds(ds2, ds1);
	set_ds_dot_line(ds2);
	set_ds_point_symbol(ds2, "\\pt6\\sq");
	ds2->treatement = my_sprintf(ds2->treatement,"%d indices found.", n_cycles);

	free(ind);
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
} // end of the do_find_boundaries







// List of functions which compute histogram using indices as starting point
int do_hist_periodic_build_from_ds(void)
{
register int 	i, i2, j, j2;
	O_p 	*op2, *op1 = NULL;
static	int	nf=1024+1, ny, n_cycles;
	gsl_histogram *hist;
	double	a_tau;	
	float	h_min=-1, h_max=3;
	d_s 	*ds2, *ds1, *ds_ind;
	pltreg	*pr = NULL;
	int	tau, n_tau;
	int	*tau_index=NULL;
	int	index;
static	char	tau_string[128]="1,2,5,10:5:50";
	double  dt;
	int	*ind; // indices

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine builds an histogram\n"
				"out of the current dataset, and place it in a new plot\n\n"
				"data a(t) is first averaged using a sliding average:\n\n"
				"a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt'\n\n"
				"different a_\\tau(t_i) are used with t_i from a dataset of indices\n"
				"(indices are assumed to be in the next ds, after the current one)");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	ny=ds1->nx;
	if (op1->n_dat<=1)	return(win_printf_OK("I need 2 datasets!\n"
				"active one for the data,\nnext one for indices"));
	ds_ind = (op1->cur_dat == op1->n_dat-1 ) ? op1->dat[0] : op1->dat[op1->cur_dat + 1];
	n_cycles = ds_ind->nx;
	if (n_cycles>=ny) 	return(win_printf_OK("You probably exchanged the 2 datasets ;-)"));

	dt = (double)(ds1->xd[ny-1]-ds1->xd[0])/(ny-1);
//	win_printf("dt is \n%g\n%g\n%g", dt, ds1->xd[2]-ds1->xd[1], ds1->xd[1]);
	if ( (ds1->xd[2]-ds1->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)				return(win_printf_OK("dt is zero !"));
	ind = (int*)calloc(n_cycles, sizeof(int));
	for (i=0; i<n_cycles; i++)	ind[i] = (int)rintf((ds_ind->xd[i] - ds1->xd[0])/dt);
	
	h_max = op1->y_hi;
    h_min = op1->y_lo;

	i=win_scanf("number of bins in histogram? %d min %f max %f\n"
			"using averaged data a_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} a(t') dt' \n\n"
			"\\tau (integer, nb of pts) ? %s"
			"Integration in time starts at specified times (indices)\n",
			&nf,&h_min,&h_max,&tau_string);
	if (i==CANCEL) 	return OFF;

	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	
					return(win_printf("bad values for \\tau !"));
	if ((ind[n_cycles-1]+tau_index[n_tau-1])>=ny)	
					return(win_printf_OK("larger tau (%d) is too large!\n"
										"%d cycles, last one starting at %d\n"
										"ny = %d", 
										tau_index[n_tau-1], n_cycles, ind[n_cycles-1], ny ));
			
	if ((op2 = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	
	for (i=0; i<n_tau; i++)
	{	tau = tau_index[i];

		if (i!=0)
		if ((ds2 = create_and_attach_one_ds(op2, nf, nf, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");

		hist = gsl_histogram_alloc(nf);
		gsl_histogram_set_ranges_uniform (hist, (double)h_min, (double)h_max);

		for (i2=0; i2<n_cycles; i2++)
		{	j=ind[i2];
			a_tau=0;
			for (j2=0; j2<tau; j2++)
			{	a_tau += (double)ds1->yd[(j+j2)];		// average over tau points
			}
			a_tau = (double)(a_tau/tau);
			gsl_histogram_increment(hist, (double)a_tau); // histogram of averaged data
		}

		histogram_to_ds(hist, ds2, nf);
		gsl_histogram_free(hist);

		inherit_from_ds_to_ds(ds2, ds1);
		ds2->treatement = my_sprintf(ds2->treatement,"histogram, averaged data over \\tau=%d points", tau);
	
	}

	set_plot_title(op2,"histogram");
	set_plot_x_title(op2, "x (%d values)",n_cycles);
	set_plot_y_title(op2, "pdf");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

    	op2->iopt |= YLOG;

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_hist_periodic_build_from_ds' */




float compute_cycle_average(float *excitation, int *ind, int n_cycles, int tau_max)
{   register int i, j;
    float m=0.;
    
    for (i=0; i<n_cycles; i++)
    {   for (j=0; j<tau_max; j++)
            m += excitation[ ind[i]+j ];
    }
    
    return( m/(float)(n_cycles*tau_max));
}



int do_correct_offset_in_files(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memory 
	O_p 		*op2, *op1=NULL;
static int	n_bin=100;
	int		n_theta, n_excitation, n_cycles;
	d_s 		*ds2;
	pltreg	*pr = NULL;
	int		i_file, n_files;	// current tau, current file number, number of taus, number of files
//	int		*tau_index=NULL;		// all the taus
	int		*files_index=NULL;		// all the files
	int		index;
static	char files_ind_string[128]	="1:1:20"; 
static	char file_extension[8]		=".dat";
static	char theta_root_filename[128]   = "run01_x";
static	char indices_root_filename[128] = "run01_M";
static	char out_root_filename[128]     = "run01_xo";
static  char path_name[512] = "1";
	char	s[512];
	char	filename[512];
	int		*ind=NULL; // indices
	float	*excitation, *theta; // *power;
	float	y_min=0, y_max=1;
static int	bool_remove_bounds=1 ;
static int  n_last=1; // how many points should be removed at the end of the dataset
static int	bool_report=0;
static int	plateau_position=0;
static int	point_position=1;
static int	bool_add_more_indices=0, bool_shift_indices=0;
static int	n_more=1, n_shift=0;
	int  	bool_search_indices=1;
	int		N_shift_auto=1;
static int  tau_max=10;
static float offset_reference=0., offset_final=0.;
char last_path[512];  
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine corrects data from a set of files\n"
				"by computing the offset, and substracting it\n\n"
				"A new file is created");
	}
	sprintf(last_path,"last_visited_%d",0);
	strcpy(path_name,(char*)get_config_string("GR-FILE-PATH",last_path,NULL));

	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"path name %%s"
			"root name of file to operate %%s"
			"root name of file to find indices {\\pt8(usually, excitation files)} %%s"
			"root name of file to write %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n"
			"%%b print to screen a report for each file");
	i=win_scanf(s, &path_name, &theta_root_filename, &indices_root_filename, &out_root_filename,
                   &files_ind_string, &file_extension, &bool_report);
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("I will search for indices, and then compute the average\n"
                   " of the data starting at indices, and spanning over %6d points\n"
                   "\n"
                   "I will then substract the average\n"
                   "and finally add a constant %8f",
                   &tau_max, &offset_reference);
	if (i==CANCEL) return(OFF);
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices, &point_position, &plateau_position, 
							&bool_remove_bounds, &n_last, &bool_shift_indices, &n_shift, 
							&bool_add_more_indices, &n_more, &N_shift_auto);
	if (i==CANCEL) return(OFF);
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	if ((op2 = create_and_attach_one_plot(pr, n_bin, n_bin, 0))==NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	
/*	if (bool_add_more_indices==1)
	{//	n_more -= n_shift;
	//	n_more -= tau_index[n_tau-1]; // not to escape from the ramp
	}
*/	
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%s%d%s", path_name, theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);
		if (n_theta==-1) // then error while opening the file
		{  return(win_printf_OK("error with file\n%s\ncreated with this information:\n%s\n%s\n%d\n%s", 
                  filename, path_name, theta_root_filename, i_file, file_extension));
        }
		sprintf(filename, "%s%s%d%s", path_name, indices_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_excitation=load_data_bin(filename, &excitation);
		
		if (n_excitation<n_theta-2) return(win_printf_OK("M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		if ( (bool_search_indices==1) && (k==0))	// first file: we autodetect some parameters		
		{	i=find_search_domain(excitation, n_excitation, plateau_position, &y_min, &y_max);
			if (i==CANCEL) return(D_O_K);
		}
	
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);
		
		ind = find_set_of_indices(excitation, 
						n_excitation, 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
		if (ind==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));
					
		if ((ind[n_cycles-1]+tau_max)>=n_excitation)
		{	i=win_printf_OK("filen %s\ntau= %d is too large!\n%d points in dataset\n\n"
						"click CANCEL to abort or OK to remove one more period",
						filename, tau_max, n_excitation);
			if (i==CANCEL) return(D_O_K);
			n_cycles --;
		}

		if (bool_report==1)	
		{	if (bool_search_indices==1)
				win_printf_OK("file number %d/%d\n(\\theta in %s)\n\n"
							"%d points in \\theta\n%d points in excitation M\n\n%d usable cycles found", 
							k+1, n_files, filename, n_theta, n_excitation, n_cycles);
			else
				win_printf_OK("file number %d/%d\n(%s)\n\n"
							"%d points in \\theta file\n%d usable windows, separated by %d points\n"
							"(cf larger \\tau is %d)", 
							k+1, n_files, filename, n_theta, n_cycles, N_shift_auto, tau_max);
		}

        sprintf(s, "%d/%d computing reference state", k+1, n_files); spit(s);
               offset_final = offset_reference - compute_cycle_average(theta, ind, n_cycles, tau_max);
        for (i=0; i<n_theta; i++) theta[i] += offset_final;
        
        sprintf(filename, "%s%s%d%s", path_name, out_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: saving data", k+1, n_files, filename); spit(s);
        save_data_bin(filename, theta, n_theta);
        
		free(excitation);
		free(theta);
		free(ind);
		
	} //end of the loop over k, ie, loop over the files;
	free(files_index);
	
	return(refresh_plot(pr, UNCHANGED));
}

int do_correct_contrast_in_files(void)
{	int		i, i1, i2, k; // slow loops, no need of registers, let's save some cache memory 
	O_p 		*op2, *op1=NULL;
static int	n_bin=100;
static int i_ref=13;
	int		n_theta, n_excitation, n_cycles_HP,n_cycles_LP;
	d_s 		*ds2;
	pltreg	*pr = NULL;
	int		i_file, n_files;	// current tau, current file number, number of taus, number of files
	int		*files_index=NULL;		// all the files
	int		index;
static	char files_ind_string[128]	="1:1:20"; 
static	char file_extension[8]		=".dat";
static	char theta_root_filename[128]   = "run01_x";
static	char indices_root_filename[128] = "run01_M";
static	char out_root_filename[128]     = "run01_xo";
static  char path_name[512] = "1";
	char	s[512];
	char	filename[512];
	int		*ind_HP=NULL,*ind_LP=NULL; // indices
	float	*excitation, *theta; // *power;
	float	y_min_HP=0, y_max_HP=1;
static int	bool_remove_bounds_HP=1 ;
static int  n_last_HP=1; // how many points should be removed at the end of the dataset
static int	plateau_position_HP=1;
static int	point_position_HP=1;
static int	bool_add_more_indices_HP=0, bool_shift_indices_HP=0;
static int	n_more_HP=1, n_shift_HP=0;
	int  	bool_search_indices_HP=1;
	int		N_shift_auto_HP=1;
	float	y_min_LP=0, y_max_LP=1;
static int	bool_remove_bounds_LP=1 ;
static int  n_last_LP=1; // how many points should be removed at the end of the dataset
static int	plateau_position_LP=0;
static int	point_position_LP=1;
static int	bool_add_more_indices_LP=0, bool_shift_indices_LP=0;
static int	n_more_LP=1, n_shift_LP=0;
	int  	bool_search_indices_LP=1;
	int		N_shift_auto_LP=1;
static int  tau_max=100;
	char last_path[512];  
	float moy_HP=0., moy_LP=0., moy_HP_ref=0., moy_LP_ref=0.;
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine corrects data from a set of files\n"
				"by computing the offset, and substracting it\n\n"
				"A new file is created");
	}
	sprintf(last_path,"last_visited_%d",0);
	strcpy(path_name,(char*)get_config_string("GR-FILE-PATH",last_path,NULL));
    
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"path name %%s"
			"root name of file to operate %%s"
			"root name of file to find indices {\\pt8(usually, excitation files)} %%s"
			"root name of file to write %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s");
	i=win_scanf(s, &path_name, &theta_root_filename, &indices_root_filename, &out_root_filename,
                   &files_ind_string, &file_extension);
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("I will search for indices, and then compute the average\n"
                   " of the data starting at indices, and spanning over %6d points\n"
                   "\n"
                   "I will then substract the average",
                   &tau_max);
	if (i==CANCEL) return(OFF);
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices_HP, &point_position_HP, &plateau_position_HP, 
							&bool_remove_bounds_HP, &n_last_HP, &bool_shift_indices_HP, &n_shift_HP, 
							&bool_add_more_indices_HP, &n_more_HP, &N_shift_auto_HP);
	if (i==CANCEL) return(OFF);
		
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices_LP, &point_position_LP, &plateau_position_LP, 
							&bool_remove_bounds_LP, &n_last_LP, &bool_shift_indices_LP, &n_shift_LP, 
							&bool_add_more_indices_LP, &n_more_LP, &N_shift_auto_LP);
	if (i==CANCEL) return(OFF);
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	if ((op2 = create_and_attach_one_plot(pr, n_bin, n_bin, 0))==NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	
	i=win_scanf("Reference file number %6d\n",&i_ref);
	if (i==CANCEL) return(D_O_K);
	
	sprintf(filename, "%s%s%d%s", path_name, theta_root_filename, i_ref, file_extension);
	n_theta=load_data_bin(filename, &theta);
	if (n_theta==-1) // then error while opening the file
			{  return(win_printf_OK("error with file\n%s\ncreated with this information:\n%s\n%s\n%d\n%s", 
                 		 filename, path_name, theta_root_filename, i_ref, file_extension));
   			}
	sprintf(filename, "%s%s%d%s", path_name, indices_root_filename, i_ref, file_extension);
	n_excitation=load_data_bin(filename, &excitation);
		
	if (n_excitation!=n_theta) return(win_printf_OK("M has %d points but \\theta has %d points", n_excitation, n_theta));
		
	i=find_search_domain(excitation, n_excitation, plateau_position_HP, &y_min_HP, &y_max_HP);
	if (i==CANCEL) return(D_O_K);
	
	i=find_search_domain(excitation, n_excitation, plateau_position_LP, &y_min_LP, &y_max_LP);
			if (i==CANCEL) return(D_O_K);
	sprintf(s, "reference_file"); spit(s);
		
	ind_HP = find_set_of_indices(excitation, n_excitation, &n_cycles_HP,
						bool_search_indices_HP, point_position_HP, plateau_position_HP, 
						bool_remove_bounds_HP, n_last_HP, 
						bool_shift_indices_HP, n_shift_HP, 
						bool_add_more_indices_HP, n_more_HP,
						N_shift_auto_HP,
						y_min_HP, y_max_HP);
	if (ind_HP==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));
					
		
	ind_LP = find_set_of_indices(excitation, n_excitation, 
						&n_cycles_LP,
						bool_search_indices_LP, point_position_LP, plateau_position_LP, 
						bool_remove_bounds_LP, n_last_LP, 
						bool_shift_indices_LP, n_shift_LP, 
						bool_add_more_indices_LP, n_more_LP,
						N_shift_auto_LP,
						y_min_LP, y_max_LP);
	if (ind_LP==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));					

	if ((ind_HP[n_cycles_HP-1]+tau_max)>=n_excitation)
		{	i=win_printf_OK("filen %s\ntau= %d is too large!\n%d points in dataset\n\n"
						"click CANCEL to abort or OK to remove one more period",
						filename, tau_max, n_excitation);
			if (i==CANCEL) return(D_O_K);
			n_cycles_HP --;
		}
		
	if ((ind_LP[n_cycles_LP-1]+tau_max)>=n_excitation)
		{	i=win_printf_OK("filen %s\ntau= %d is too large!\n%d points in dataset\n\n"
						"click CANCEL to abort or OK to remove one more period",
						filename, tau_max, n_excitation);
			if (i==CANCEL) return(D_O_K);
			n_cycles_LP --;
		}
		
	for (i1=0; i1<tau_max; i1++) 
			{	for (i2=0; i2<n_cycles_HP; i2++)
				{	moy_HP_ref +=(float)theta[ind_HP[i2]+i1];
				}
			}
      	moy_HP_ref/=tau_max;
	moy_HP_ref/=n_cycles_HP;
      		
	for (i1=0; i1<tau_max; i1++) 
			{	for (i2=0; i2<n_cycles_LP; i2++)
				{	moy_LP_ref +=(float)theta[ind_LP[i2]+i1];
				}
			}
      	moy_LP_ref/=tau_max;
	moy_LP_ref/=n_cycles_LP;
	
	free(excitation);
	free(theta);
	free(ind_HP);
	free(ind_LP);
	moy_HP=0.;
	moy_LP=0.;
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%s%d%s", path_name, theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);
		if (n_theta==-1) // then error while opening the file
			{  return(win_printf_OK("error with file\n%s\ncreated with this information:\n%s\n%s\n%d\n%s", 
                 		 filename, path_name, theta_root_filename, i_file, file_extension));
   			}
		sprintf(filename, "%s%s%d%s", path_name, indices_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_excitation=load_data_bin(filename, &excitation);
		
		if (n_excitation!=n_theta) return(win_printf_OK("M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);
		
		ind_HP = find_set_of_indices(excitation, 
						n_excitation, 	&n_cycles_HP,
						bool_search_indices_HP, point_position_HP, plateau_position_HP, 
						bool_remove_bounds_HP, n_last_HP, 
						bool_shift_indices_HP, n_shift_HP, 
						bool_add_more_indices_HP, n_more_HP,
						N_shift_auto_HP,
						y_min_HP, y_max_HP);
		if (ind_HP==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));
					
		
		ind_LP = find_set_of_indices(excitation, 
						n_excitation, 
						&n_cycles_LP,
						bool_search_indices_LP, point_position_LP, plateau_position_LP, 
						bool_remove_bounds_LP, n_last_LP, 
						bool_shift_indices_LP, n_shift_LP, 
						bool_add_more_indices_LP, n_more_LP,
						N_shift_auto_LP,
						y_min_LP, y_max_LP);
		if (ind_LP==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));					

		if ((ind_HP[n_cycles_HP-1]+tau_max)>=n_excitation)
		{	i=win_printf_OK("filen %s\ntau= %d is too large!\n%d points in dataset\n\n"
						"click CANCEL to abort or OK to remove one more period",
						filename, tau_max, n_excitation);
			if (i==CANCEL) return(D_O_K);
			n_cycles_HP --;
		}
		
		if ((ind_LP[n_cycles_LP-1]+tau_max)>=n_excitation)
		{	i=win_printf_OK("filen %s\ntau= %d is too large!\n%d points in dataset\n\n"
						"click CANCEL to abort or OK to remove one more period",
						filename, tau_max, n_excitation);
			if (i==CANCEL) return(D_O_K);
			n_cycles_LP --;
		}
		
		for (i1=0; i1<tau_max; i1++) 
			{	for (i2=0; i2<n_cycles_HP; i2++)
				{	moy_HP +=(float)theta[ind_HP[i2]+i1];
				}
			}
      		moy_HP/=tau_max;
		moy_HP/=n_cycles_HP;
      		
		for (i1=0; i1<tau_max; i1++) 
			{	for (i2=0; i2<n_cycles_LP; i2++)
				{	moy_LP +=(float)theta[ind_LP[i2]+i1];
				}
			}
      		moy_LP/=tau_max;
		moy_LP/=n_cycles_LP;
	        for (i=0; i<n_theta; i++)
			{	theta[i] *= (float)(moy_HP_ref-moy_LP_ref)/(moy_HP-moy_LP);
				theta[i] += (float)moy_HP_ref-moy_HP*(moy_HP_ref-moy_LP_ref)/(moy_HP-moy_LP);
				theta[i] -= (float)moy_LP_ref;
			}
        	
		sprintf(filename, "%s%s%d%s", path_name, out_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: saving data", k+1, n_files, filename); spit(s);
		save_data_bin(filename, theta, n_theta);
        
		free(excitation);
		free(theta);
		free(ind_HP);
		free(ind_LP);
		moy_HP=0.;
		moy_LP=0.;
		
	} //end of the loop over k, ie, loop over the files;
	free(files_index);
	
	return(refresh_plot(pr, UNCHANGED));
}



int do_compute_fluctuations_in_files(void)
{
register int 	i, i2, k;
	int  	 period, period_min=0., period_max;
	int	 index, i_file, n_files;
	int	 *ind; // indices
	pltreg	*pr = NULL;
	char	s[512];
	int	*files_index=NULL;		// all the files
static	char 	files_ind_string[128]	="1:1:20"; 
static	char 	file_extension[8]		=".bin";
static	char 	theta_root_filename[128]   = "run01theta";
static	char 	out_root_filename[128]   = "ftheta";
static	char 	indices_root_filename[128] = "run01M";
static int	plateau_position=0;
static int	point_position=1;
static int	bool_add_more_indices=0, bool_shift_indices=0;
static int	n_more=1, n_shift=0;
	int  	bool_search_indices=1;
	int	N_shift_auto=1;
static  char 	path_name[512] = "1";
	char	filename[512];
static float 	fec=32768., times=20.;
static int	bool_remove_bounds=1 ;
static int  	n_last=1 ,i4 =0; // how many points should be removed at the end of the dataset
	float	*excitation, *theta, *ftheta=NULL, *average=NULL, mean_ftheta=0;
	int	n_theta=0., n_excitation, n_cycles, n_cycles_total=0, n_times;
	float	y_min=0, y_max=1;
	char 	last_path[512]; 
	

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine averages a dataset\n"
				"out of a set of files of \\theta(t) and M(t).\n\n"
				"and computes next fluctuations of each cycles\n\n"
				"data are saved in files\n");
	}
	
	
	sprintf(last_path,"last_visited_%d",0);
	strcpy(path_name,(char*)get_config_string("GR-FILE-PATH",last_path,NULL));
    
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"path name %%s"
			"root name of file with angle \\theta, or power P %%s"
			"root name of file to find indices {\\pt8(usually, excitation files)} %%s"
			"root name of file to write %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n");
	i=win_scanf(s, &path_name, &theta_root_filename, &indices_root_filename, &out_root_filename, &files_ind_string, &file_extension);
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("sampling frequency ? %8f\n\n"
				"times duration of the cycle %8f\n\n"
				"starting name at %d", &fec, &times, &i4);
	if (i==CANCEL) return(OFF);
	
	n_times=(float)fec*times;
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices, &point_position, &plateau_position, 
				&bool_remove_bounds, &n_last, &bool_shift_indices, &n_shift, 
							&bool_add_more_indices, &n_more, &N_shift_auto);
	if (i==CANCEL) return(OFF);

				
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%s%d%s", path_name, theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);
		if (n_theta==-1) // then error while opening the file
			{	return(win_printf_OK("error with file\n%s\ncreated with this information:\n%s\n%s\n%d\n%s", 
           		       			filename, path_name, theta_root_filename, i_file, file_extension));
			}
			       
		sprintf(filename, "%s%s%d%s", path_name, indices_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_excitation=load_data_bin(filename, &excitation);
		if (n_excitation!=n_theta) return(win_printf_OK("Commutation has %d points but \\theta has %d points", n_excitation, n_theta));
		
		if ( (bool_search_indices==1) && (k==0))	// first file: we autodetect some parameters		
		{	i=find_search_domain(excitation, n_excitation, plateau_position, &y_min, &y_max);
			if (i==CANCEL) return(D_O_K);
		}
		
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);
		
		ind = find_set_of_indices(excitation, 
						( (bool_search_indices==1) ? n_excitation : n_excitation), 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
		if (ind==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));
			
		
		if (n_cycles<1) 	return(win_printf_OK("Mmm... not enough ramps in file %s", filename));
		if (k==0) 
		{	period_min=n_theta;
			period_max=0;
			for (i=1; i<n_cycles; i++)
			{	period = (ind[i]-ind[i-1]);
				if (period<period_min) period_min=period;
				if (period>period_max) period_max=period;
			}
		
		 }
		while ((ind[n_cycles-1]+period_min)>n_theta) n_cycles-=1;
		average=(float*)calloc(n_times, sizeof(float));
		for (i=0; i<n_times; i++) average[i] = 0;
		
		for (i=0; i<n_times; i++)
		{	for (i2=0; i2<n_cycles; i2++)
			{	average[i]+=(float)theta[ind[i2]+i];
			}
		}
		for (i=0;i<n_times;i++)
		{	average[i] /= (float)n_cycles;
		}
		ftheta=(float*)calloc(n_times, sizeof(float));
		for (i2=0; i2<n_cycles; i2++)
		{	for (i=0; i<n_times; i++)
			{ftheta[i] = theta[ind[i2]+i]-average[i];
			mean_ftheta += ftheta[i];
			}
			mean_ftheta /=n_times;
			for (i=0; i<n_times; i++)
			{ftheta[i] -= mean_ftheta;
			}
			
			sprintf(filename, "%s%s%d%s", path_name, out_root_filename, i4+i2+n_cycles_total, file_extension);
			sprintf(s, "%d/%d file %s: saving data",i4+i2+n_cycles_total, n_files, filename); spit(s);
			save_data_bin(filename, ftheta, n_times);
		}
			
		n_cycles_total += n_cycles;
		free(ftheta);
		free(average);
		free(excitation);
		free(theta);
		free(ind);
		
		

		
	} //end of the loop over k, ie, loop over the files;
	
	free(files_index);
	
	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_compute_fluctuations_in files' */


int do_filter_bin_files(void)
{
register int 	i, k;
	int	 index, i_file, n_files;
	pltreg	*pr = NULL;
	char	s[512];
	int	*files_index=NULL;		// all the files
static	char 	files_ind_string[128]	="1:1:20"; 
static	char 	file_extension[8]		=".bin";
static	char 	theta_root_filename[128]   = "run01theta";
static	char 	out_root_filename[128]   = "ftheta";
static  char 	path_name[512] = "1";
	char	filename[512];
	float	*theta, *in;
	int	n_theta=0.;
	char 	last_path[512]; 
static float cutoff_HP_f=10., width_HP_f = 5., f_acq = 32768.; 
	

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine filters a dataset\n"
				"out of a set of files of \\theta(t).\n\n"
				"and save the result in files\n");
	}
	
	
	sprintf(last_path,"last_visited_%d",0);
	strcpy(path_name,(char*)get_config_string("GR-FILE-PATH",last_path,NULL));
    
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"path name %%s"
			"root name of file with angle \\theta, or power P %%s"
			"root name of file to write %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n");
	i=win_scanf(s, &path_name, &theta_root_filename, &out_root_filename, &files_ind_string, &file_extension);
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("(High-Pass filter): cutoff: %8f Hz, width : %8f Hz (e.g.: cutoff/2)\n\n"
	"acquisition frequence %8f", &cutoff_HP_f, &width_HP_f, &f_acq);
	if (i==CANCEL) return(OFF);
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	
				
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%s%d%s", path_name, theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);
		if (n_theta==-1) // then error while opening the file
			{	return(win_printf_OK("error with file\n%s\ncreated with this information:\n%s\n%s\n%d\n%s", 
           		       			filename, path_name, theta_root_filename, i_file, file_extension));
			}
			       
		
	
		in = Fourier_filter_float(theta, n_theta, f_acq, cutoff_HP_f, width_HP_f, -1, -1,  1);
		
		for (i=0;i<n_theta;i++) in[i]/=n_theta;
		sprintf(filename, "%s%s%d%s", path_name, out_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: saving data", k+1, n_files, filename); spit(s);
		save_data_bin(filename, in, n_theta);
		free(theta);
		fftw_free(in); 
	}
	free(files_index);
	
	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_filter_bin_files' */

int do_deriv_bin_files(void)
{
register int 	i, k;
	int	 index, i_file, n_files;
	pltreg	*pr = NULL;
	char	s[512];
	int	*files_index=NULL;		// all the files
static	char 	files_ind_string[128]	="1:1:20"; 
static	char 	file_extension[8]		=".bin";
static	char 	theta_root_filename[128]   = "run01theta";
static	char 	out_root_filename[128]   = "ftheta";
static  char 	path_name[512] = "1";
	char	filename[512];
	float	*theta, *dtheta=NULL, *t=NULL;
	int	n_theta=0.;
static float f_acq = 16384.;
	char 	last_path[512]; 


	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine deriv by spline a dataset\n"
				"out of a set of files of \\theta(t).\n\n"
				"and save the result in files\n");
	}
	
	
	sprintf(last_path,"last_visited_%d",0);
	strcpy(path_name,(char*)get_config_string("GR-FILE-PATH",last_path,NULL));
    
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"path name %%s"
			"root name of file with angle \\theta, or power P %%s"
			"root name of file to write %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n");
	i=win_scanf(s, &path_name, &theta_root_filename, &out_root_filename, &files_ind_string, &file_extension);
	if (i==CANCEL) return(OFF);
	
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	i=win_scanf("acquisition frequence %8f", &f_acq);
	if (i==CANCEL) return(OFF);
	
				
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%s%d%s", path_name, theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);
		if (n_theta==-1) // then error while opening the file
			{	return(win_printf_OK("error with file\n%s\ncreated with this information:\n%s\n%s\n%d\n%s", 
           		       			filename, path_name, theta_root_filename, i_file, file_extension));
			}
			       
		
	
		
		t=(float*)calloc(n_theta, sizeof(float));
		for (i=0;i<n_theta;i++) 		t[i]=(float)i;

		dtheta=(float*)calloc(n_theta, sizeof(float));
		for (i=0; i<n_theta; i++) dtheta[i] = 0;
		diff_splines_float(t, theta, n_theta, dtheta);
		free(t);
		for (i=0; i<n_theta; i++) dtheta[i] *= f_acq;
	
		sprintf(filename, "%s%s%d%s", path_name, out_root_filename, i_file, file_extension);
		save_data_bin(filename, dtheta, n_theta);
		free(dtheta);
		free(theta);

	}
	free(files_index);
	
	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_deriv_bin_files' */

//Trace la reponse integree a partir de la reponse impulsionnelle
int do_compute_integrated_response(void)
{	register int i,k;
	static float t_i = 0.4, dt=0.1, f_acq=8192.;
	int i_0,dt_n;
	int		index;
	int 	*ind=NULL, n_ds=1;
	pltreg	*pr;
	O_p	*op, *op_new=NULL;
	d_s	*ds, *ds_test=NULL;
		
		if(updating_menu_state != 0)	return D_O_K;	
		index = active_menu->flags & 0xFFFFFF00;
		
		if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)  return win_printf_OK("cannot find data!");
	
		i=win_scanf("{\\pt14\\color{yellow}Compute integrate response}\n\n"
					"initial time %8f\n"
					"interval of time %8f\n\n"
					"acquisition frequency %8f\n",
					&t_i, &dt, &f_acq);
		if (i==CANCEL) return(D_O_K);
		
		i_0 = t_i*f_acq;
		dt_n = dt*f_acq;
		
		n_ds  = op->n_dat;
		ind   = (int*)calloc(n_ds,sizeof(int));
		for (i=0; i<n_ds; i++) ind[i]=i;

for (k=0; k<n_ds; k++)
	{	ds =  op->dat[ ind[k] ];
		if (ds == NULL)   return (win_printf_OK("can't find data set"));
		if (k==0)
		{op_new = create_and_attach_one_plot(pr, n_ds, n_ds, 0);
		ds_test= op_new->dat[0];
		}
		for (i=0; i<n_ds; i++)
			{	ds_test->xd[i]=i_0+dt_n*i;
				ds_test->yd[i] +=ds->yd[i_0+dt_n*i];
			}
	}
for (i=0; i<n_ds; i++) 
{	ds_test->xd[i] /=f_acq;
	ds_test->yd[i] *=dt;
}
	refresh_plot(pr, UNCHANGED);
	return D_O_K;
}//end of function do_compute_integrated_response

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//histogramme sur des fichiers//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int do_hist_periodic_build_from_files(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memory 
register int	i2, j;
	O_p 		*op2, *op1=NULL;
static int	n_bin=100;
	int		n_theta, n_excitation, n_cycles, n_cycles_total=0;
	gsl_histogram *hist;
	double	a_tau=0, factor=1., my_kT=1.0;
static float	h_min=-5, h_max=15;
	d_s 		*ds2;
	pltreg	*pr = NULL;
	int		tau, i_file, n_tau, n_files;	// current tau, current file number, number of taus, number of files
	int		*tau_index=NULL;		// all the taus
	int		*files_index=NULL;		// all the files
	int		index;
static	char tau_string[128]		="1,2,5,10:5:50";
static	char files_ind_string[128]	="1:1:20"; 
static	char file_extension[8]		=".dat";
static	char theta_root_filename[128]   = "theta";
static	char excitation_root_filename[128] = "M";
static	char indices_root_filename[128] = "M";
static  char path_name[512] = "1";
	char	s[512];
	char	filename[512];
	int		*ind=NULL; // indices
	float	*excitation, *theta; // *power;
	float	y_min=0, y_max=1;
static int	bool_remove_bounds=1 ;
static int  n_last=1; // how many points should be removed at the end of the dataset
static int	bool_report=0;
static int	plateau_position=0;
static int	point_position=1;
static int	bool_add_more_indices=0, bool_shift_indices=0, bool_substract_vanZon=1, indice_reference=0;
static int  quantity_to_compute=0;
static int	n_more=1, n_shift=0;
static int  bool_normalize_by_dt=0, bool_normalize_by_kT=0;
	int  	bool_search_indices=1;
	int		N_shift_auto=1;
static float fec=2048.;
double (*compute_power_function)(float*, float*, int, int, int) = NULL;    
char last_path[512];  
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine builds a set of histograms\n"
				"out of a set of files of either the power P(t) or \\theta(t) and M(t).\n\n"
				"data W_\\tau(t) is computed as :\n\n"
				"W_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} P(t') dt'\n\n"
				"or\n\n"
				"W_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} -(M(t')-\\epsilonM(t)).{\\frac{d\\theta}{dt}}(t') dt'\n\n"
				"with \\epsilon = 0 or 1\n"
				"A new plot is created");
	}
	sprintf(last_path,"last_visited_%d",0);
	strcpy(path_name,(char*)get_config_string("GR-FILE-PATH",last_path,NULL));
    
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"path name %%s"
			"root name of file with angle \\theta, or power P %%s"
			"root name of file with excitation M %%s"
			"root name of file to find indices {\\pt8(usually, excitation files)} %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n"
			"%%b print to screen a report for each file");
	i=win_scanf(s, &path_name, &theta_root_filename, &excitation_root_filename, &indices_root_filename, &files_ind_string, &file_extension, &bool_report);
	if (i==CANCEL) return(OFF);
	
	
	if (bool_normalize_by_kT==1) my_kT = k_b*Temperature; 
	else                         my_kT = 1.0;
	i=win_scanf("sampling frequency f_s = %8f\n\n"
                "%b normalize by dt = 1/f_s\n"
                "%b normalize by kT = %8f\n", 
                &fec, &bool_normalize_by_dt, &bool_normalize_by_kT, &my_kT);
	if (i==CANCEL) return(OFF);
	if (bool_normalize_by_kT==0) my_kT = 1.0;
	
	if (bool_normalize_by_dt==1) factor = (double)fec;
	else                         factor = (double)1.0;
    factor /= (double)my_kT;
 
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices, &point_position, &plateau_position, 
							&bool_remove_bounds, &n_last, &bool_shift_indices, &n_shift, 
							&bool_add_more_indices, &n_more, &N_shift_auto);
	if (i==CANCEL) return(OFF);
		
	// here, we choose the math. function to operate on the data:
	i=win_scanf("{\\pt14\\color{yellow}Quantity to compute}\n\n"
				"%R time-average an already computed power (in J/Npts), results in (kT/s):\n"
				"   W = \\frac{1}{\\tau}\\int_t^{t+\\tau} P(t') dt'\n\n"
				"%r time-average a power, from M et d\\theta defined :\n"
				"   W = \\frac{1}{\\tau} \\int_t^{t+\\tau} -M(t')*\\frac{d\\theta}{dt}(t') dt'\n\n"
				"%r time-average a power, defined as:\n"
				"   W = \\frac{1}{\\tau}\\int_t^{t+\\tau} - M(t') \\frac{d\\theta}{dt} (t') dt'\n\n"
				"%r time-average a Jarzynski with referential change power/work defined as:\n"
				"   W = \\frac{1}{\\tau}\\int_t^{t+\\tau}  [\\theta(t') - M(t')/C] \\frac{{d\\M}{dt}} (t') dt'\n\n" 
				"%r time-average a van Zon power/work from M and theta defined as:\n\n"
				"   W = \\frac{1}{\\tau}\\int_t^{t+\\tau} - [ M(t') - M(t)] \\frac{{d\\theta}{dt}} (t') dt'\n\n"
				"%r time-average a van Zon power/work from M and d\\theta defined as:\n\n"
				"   W = \\frac{1}{\\tau}\\int_t^{t+\\tau} - [ M(t') - M(t)] \\frac{{d\\theta}{dt}} (t') dt'\n\n"
				"in these last case (vanZon like), what should be substracted ?\n"
				"%R M(t) or %r M(t-1) {\\pt6 (best is M(t)} \n\n",
				&quantity_to_compute, &indice_reference);
	if (i==CANCEL) return(D_O_K);
	
	switch (quantity_to_compute)
   	{
      case 0 : 	compute_power_function = &compute_work_classic_from_power; 
      			break;
      case 1 :  compute_power_function = &compute_work_classic_from_M_ddep;
      			bool_substract_vanZon=0;
      			break;
      case 2 :  compute_power_function = &compute_work_classic_from_M_dep;
      			bool_substract_vanZon=0;
      			break;
      case 4 :  compute_power_function = &compute_work_vinzou_from_M_dep; 
      			bool_substract_vanZon=1;
      			break;
      case 5 :  compute_power_function = &compute_work_vinzou_from_M_ddep; 
      			bool_substract_vanZon=1;
      			break;
      case 3 : 	compute_power_function = &compute_work_Jarzynski_newref_from_M_dep; 
      			break;
	default : return(win_printf_OK("no action defined, aborting."));
   	}
   	
   //	if (quantity_to_compute==3) if (win_scanf("spring constant C ? %9f ", &C)==CANCEL) return(D_O_K);
   	
	i=win_scanf("number of bins in histogram? %d min %f max %f\n"
			"using averaged data {\\pt16 a_\\tau(t) = {1 \\over \\tau} \\int_t_i^{t_i+\\tau} a(t') dt'}\n\n"
			"\\tau (integer, nb of pts) ? %s"
			"Integration in time starts at times t_i found with previous rule.",
			&n_bin, &h_min, &h_max, &tau_string);
	if (i==CANCEL) return OFF;			
	
	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	return(win_printf("bad values for \\tau !"));
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	if ((op2 = create_and_attach_one_plot(pr, n_bin, n_bin, 0))==NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	
	if (bool_add_more_indices==1)
	{	n_more -= n_shift;
		n_more -= tau_index[n_tau-1]; // not to escape from the ramp
	}
	
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%s%d%s", path_name, theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);
		if (n_theta==-1) // then error while opening the file
			{  return(win_printf_OK("error with file\n%s\ncreated with this information:\n%s\n%s\n%d\n%s", 
           		       filename, path_name, theta_root_filename, i_file, file_extension));
   			     }
		sprintf(filename, "%s%s%d%s", path_name, indices_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_excitation=load_data_bin(filename, &excitation);
		
		if (n_excitation<n_theta-2) return(win_printf_OK("M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		if ( (bool_search_indices==1) && (k==0))	// first file: we autodetect some parameters		
		{	i=find_search_domain(excitation, n_excitation, plateau_position, &y_min, &y_max);
			if (i==CANCEL) return(D_O_K);
		}
	
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);
		
		ind = find_set_of_indices(excitation, 
						( (bool_search_indices==1) ? n_excitation : n_excitation - tau_index[n_tau-1]), 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
		if (ind==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));
					
		if ((ind[n_cycles-1]+tau_index[n_tau-1])>=n_excitation)
		{	i=win_printf_OK("filen %s\nlarger tau %d is too large!\nlast index is %d\n%d points in dataset\n\n"
						"click CANCEL to abort or OK to remove one more period",
						filename, tau_index[n_tau-1], ind[n_cycles-1], n_excitation);
			if (i==CANCEL) return(D_O_K);
			n_cycles --;
		}

		if (strcmp(indices_root_filename, excitation_root_filename)!=0)
		{	sprintf(filename, "%s%d%s", excitation_root_filename, i_file, file_extension);
			sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
			n_excitation=load_data_bin(filename, &excitation);
		
			if (n_excitation!=n_theta) return(win_printf_OK("forcing M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		}
		
		n_cycles_total += n_cycles;
		if (n_cycles<1) 	return(win_printf_OK("Mmm... not enough ramps in file %s", filename));
			
		if (bool_report==1)	
		{	if (bool_search_indices==1)
				win_printf_OK("file number %d/%d\n(\\theta in %s)\n\n"
							"%d points in \\theta\n%d points in excitation M\n\n%d usable cycles found", 
							k+1, n_files, filename, n_theta, n_excitation, n_cycles);
			else
				win_printf_OK("file number %d/%d\n(%s)\n\n"
							"%d points in \\theta file\n%d usable windows, separated by %d points\n"
							"(cf larger \\tau is %d)", 
							k+1, n_files, filename, n_theta, n_cycles, N_shift_auto, tau_index[n_tau-1]);
		}

		sprintf(s, "%d/%d cumulating histograms", k+1, n_files); spit(s);
							
		for (i=0; i<n_tau; i++)
		{	tau=tau_index[i];

			if (k==0) // first file : we create the datasets
			{	if (i!=0) // not the first tau: we add a dataset
				{	if ((ds2 = create_and_attach_one_ds(op2, n_bin, n_bin, 0)) == NULL)	
						return win_printf_OK("cannot create dataset !");
				}
				if (bool_normalize_by_kT==1) ds2->treatement = my_sprintf(ds2->treatement,"data divided by kT");
				if (bool_normalize_by_dt==1) ds2->treatement = my_sprintf(ds2->treatement,"data divided by dt=1/f_{acq}");
				ds2->treatement = my_sprintf(ds2->treatement,"histogram, averaged data over \\tau=%d points", tau);
						
				hist = gsl_histogram_alloc(n_bin);
				gsl_histogram_set_ranges_uniform (hist, (double)h_min, (double)h_max);
			}
			else 
			{	ds2=op2->dat[i];
				ds_to_histogram(ds2, &hist); // hist will be allocated perfectly
			}

			for (i2=0; i2<n_cycles; i2++)
			{	j=ind[i2];	// point de départ de l'intégration

				a_tau = compute_power_function(excitation, theta, j, tau, indice_reference);
				gsl_histogram_increment(hist, (double)(a_tau*factor/(double)tau)); // histogram of averaged data
				// note (2007/06/20) : factor takes into account the sampling frequency
				// together with the possible normalization by a given kT.
				// we also have to divide by tau, because this is not done by the functions
				// which calculate the work (dividing by tau gives a power, which 
				// is the average work).
			}

			histogram_to_ds(hist, ds2, n_bin);
			gsl_histogram_free(hist);
	
		} //end of loop over i, ie, loop over tau

		free(excitation);
		free(theta);
		free(ind);
		
	} //end of the loop over k, ie, loop over the files;
	free(tau_index);
	free(files_index);
	
	set_plot_title(op2,"histograms");
	set_plot_x_title(op2, "\\frac{1}{\\tau} %sW_\\tau (%d %s from %d files)", 
        (bool_normalize_by_kT==1) ? "\\frac{1}{kT}" : "", n_cycles_total, 
        (bool_search_indices==1) ? "cycles" : "windows",  n_files);
	set_plot_y_title(op2, "pdf");
	op2->filename = Transfer_filename(op1->filename);
	op2->dir = Mystrdup(op1->dir);

    op2->iopt |= YLOG;

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_hist_periodic_build_from_files' */






int do_stats_periodic_build_from_M_dep_files(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memory 
register int	i2, j, m;
	pltreg	*pr = NULL;
	O_p 	*op2=NULL;
	d_s 	*ds_mean=NULL, *ds_sigma=NULL, *ds_skewness=NULL, *ds_kurtosis=NULL, *ds_GC=NULL, *ds_GC4=NULL;
	double	a_tau, mean, sigma, skewness=0., kurtosis=0., GC_estimate=0., GC_tmp, GC4_estimate;
	double	*y_tau, *total_sigma, *dispersion;
	double  my_kT=1.0;
static float	fec=2048.;
	float	*excitation, *theta; // *power;
	float	y_min=0, y_max=1;
	int		tau=1, i_file, n_tau, n_files;	// current tau, current file number, number of taus, number of files
	int		*tau_index=NULL;		// all the taus
	int		*files_index=NULL;		// all the files
	int		index;
static	char	tau_string[128]		="1:50";
static	char files_ind_string[128]	="1:1:20"; 
static	char file_extension[8]		=".dat";
static	char theta_root_filename[128]   = "dep";
static	char excitation_root_filename[128] = "M";
static	char indices_root_filename[128] = "M";
	int		*ind=NULL; // indices
double (*compute_power_function)(float*, float*, int, int, int) = NULL;    

static int	bool_remove_bounds=1 ;
static int  n_last=1; // how many points should be removed at the end of the dataset
static int	bool_report=0;
static int	plateau_position=0;
static int	point_position=1;
static int	bool_add_more_indices=0, bool_shift_indices=0, bool_substract_vanZon=1, indice_reference=0;
static int 	bool_mean=10, bool_sigma=1, bool_skew=1, bool_kurtosis=1, bool_substract_3_to_kurtosis=0, 
			bool_GC=1, bool_GC4=1, tau_units=0, bool_dispersion=0;
static int	n_more=1, n_shift=0;
static int  quantity_to_compute=0;
static int  bool_normalize_by_kT=0;

	int		n_theta, n_excitation, n_cycles, n_cycles_total=0, n_intervals;
	int  	bool_search_indices=1;
	int		N_shift_auto=1;
	char	s[512];
	char	filename[256];

	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine computes moments\n"
				"out of a set of files of \\theta(t) and M(t).\n\n"
				"data W_\\tau(t) is computed as :\n\n"
				"W_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} -(M(t')-\\epsilonM(t)).{\\frac{d\\theta}{dt}}(t') dt'\n\n"
				"with \\epsilon = 0 or 1\n"
				"other formulas are available\n"
				"A new plot is created");
	}
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of file with angle \\theta or x %%s"
			"root name of file with forcing M or F %%s"
			"root name of file to detect indices %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n"
			"%%b print to screen a report for each file");
	i=win_scanf(s, &theta_root_filename, &excitation_root_filename, &indices_root_filename, 
			&files_ind_string, &file_extension, &bool_report);
	if (i==CANCEL) return(OFF);
	
	if (bool_normalize_by_kT==1) my_kT = k_b*Temperature; 
	else                         my_kT = 1.0;
	i=win_scanf("sampling frequency ? %8f\n\n"
                "%b normalize by kT = %8f", &fec, &bool_normalize_by_kT, &my_kT);
	if (i==CANCEL) return(OFF);
	if (bool_normalize_by_kT==0) my_kT = 1.0;
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices, &point_position, &plateau_position, 
				&bool_remove_bounds, &n_last, &bool_shift_indices, &n_shift, 
							&bool_add_more_indices, &n_more, &N_shift_auto);
	if (i==CANCEL) return(OFF);
		
	i=win_scanf("{\\pt14\\color{yellow}Quantity to compute}\n\n"
				"%R time-average an already computed power (in J/Npts), results in (kT/s):\n"
				"   W = \\frac{1}{\\tau}\\int_t^{t+\\tau} P(t') dt'\n\n"
				"%r time-average a power, from M et d\\theta defined :\n"
				"   W = \\frac{1}{\\tau} \\int_t^{t+\\tau} -M(t')*\\frac{d\\theta}{dt}(t') dt'\n\n"
				"%r time-average a power, defined as:\n"
				"   W = \\frac{1}{\\tau}\\int_t^{t+\\tau} - M(t') \\frac{d\\theta}{dt} (t') dt'\n\n"
				"%r time-average a Jarzynski with referential change power/work defined as:\n"
				"   W = \\frac{1}{\\tau}\\int_t^{t+\\tau}  [\\theta(t') - M(t')/C] \\frac{{d\\M}{dt}} (t') dt'\n\n" 
				"%r time-average a van Zon power/work from M and theta defined as:\n\n"
				"   W = \\frac{1}{\\tau}\\int_t^{t+\\tau} - [ M(t') - M(t)] \\frac{{d\\theta}{dt}} (t') dt'\n\n"
				"%r time-average a van Zon power/work from M and d\\theta defined as:\n\n"
				"   W = \\frac{1}{\\tau}\\int_t^{t+\\tau} - [ M(t') - M(t)] \\frac{{d\\theta}{dt}} (t') dt'\n\n"
				"in these last case (vanZon like), what should be substracted ?\n"
				"%R M(t) or %r M(t-1) {\\pt6 (best is M(t)} \n\n",
				&quantity_to_compute, &indice_reference);
	if (i==CANCEL) return(D_O_K);
	
	switch (quantity_to_compute)
   	{
      case 0 : 	compute_power_function = &compute_work_classic_from_power; 
      			break;
      case 1 :  compute_power_function = &compute_work_classic_from_M_ddep;
      			bool_substract_vanZon=0;
      			break;
      case 2 :  compute_power_function = &compute_work_classic_from_M_dep;
      			bool_substract_vanZon=0;
      			break;
      case 4 :  compute_power_function = &compute_work_vinzou_from_M_dep; 
      			bool_substract_vanZon=1;
      			break;
      case 5 :  compute_power_function = &compute_work_vinzou_from_M_ddep; 
      			bool_substract_vanZon=1;
      			break;
      case 3 : 	compute_power_function = &compute_work_Jarzynski_newref_from_M_dep; 
      			break;
	default : return(win_printf_OK("no action defined, aborting."));
   	}
   	
				
	i=win_scanf("{\\pt14\\color{yellow}Compute moments}\n"
			"%b mean\n"
			"%b \\sigma\n"
			"%b skewness\n"
			"%b kurtosis (%b substract 3)\n"
			"%b GC estimator 2<x>/\\sigma^2\n"
			"%b GC estimator at order 4\n"
			"%b add error bars for dispersion from file to file"
			"value(s) of \\tau : %s\n"
			"for GC: \\tau units are %R points or %r seconds (from f_{ec})\n",
			&bool_mean, &bool_sigma, &bool_skew, &bool_kurtosis, &bool_substract_3_to_kurtosis, 
			&bool_GC, &bool_GC4, &bool_dispersion, &tau_string, &tau_units);
	if (i==CANCEL) return(OFF);
	if ( (bool_mean + bool_sigma + bool_skew + bool_kurtosis + bool_GC + bool_GC4) == 0) return(OFF);
	if (bool_GC4==1)  
	{	if ( (bool_skew * bool_kurtosis * bool_GC)==0)
		{	i=win_printf("I will also compute the skewness, the kurtosis,\nand GC estimate at order 2\n"
						"associated datasets will be created.");
			if (i==CANCEL) return(D_O_K);
		}
		bool_skew     = 1;
		bool_kurtosis = 1;
		bool_GC       = 1;
		
	}
	
	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	return(win_printf_OK("bad values for \\tau !"));
//	if (tau_index[0]==1) return(win_printf_OK("\\tau = 1 is not allowed..."));

	if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)				return win_printf_OK("cannot find plot region");
	
	if (bool_add_more_indices==1)
	{	n_more -= n_shift;
		n_more -= tau_index[n_tau-1]; // not to escape from the ramp
	}	
	
	if ((op2 = create_and_attach_one_plot(pr, n_tau, n_tau, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
		
	if (bool_mean==1)
	{	ds_mean=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
	    if (bool_normalize_by_kT==1) ds_mean->treatement = my_sprintf(ds_mean->treatement,"data divided by kT = %g", my_kT);
		ds_mean->treatement = my_sprintf(ds_mean->treatement,"mean vs \\tau");
		set_ds_source(ds_mean, "data from %d files %s and %s", n_files, theta_root_filename, excitation_root_filename);
	}
	if (bool_sigma==1)
	{	ds_sigma=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
	    if (bool_normalize_by_kT==1) ds_sigma->treatement = my_sprintf(ds_sigma->treatement,"data divided by kT = %g", my_kT);
		ds_sigma->treatement = my_sprintf(ds_sigma->treatement,"sigma vs \\tau");
		set_ds_source(ds_sigma, "data from %d files %s and %s", n_files, theta_root_filename, excitation_root_filename);
	}
	if (bool_skew==1) 	
	{	ds_skewness=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		if (bool_normalize_by_kT==1) ds_skewness->treatement = my_sprintf(ds_skewness->treatement,"data divided by kT = %g", my_kT);
        ds_skewness->treatement = my_sprintf(ds_skewness->treatement,"skewness vs \\tau");
		set_ds_source(ds_skewness, "data from %d files %s and %s", n_files, theta_root_filename, excitation_root_filename);
	}
	if (bool_kurtosis==1)
	{ 	ds_kurtosis=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		if (bool_normalize_by_kT==1) ds_kurtosis->treatement = my_sprintf(ds_kurtosis->treatement,"data divided by kT = %g", my_kT);
        ds_kurtosis->treatement = my_sprintf(ds_kurtosis->treatement,"kurtosis%s vs \\tau", (bool_substract_3_to_kurtosis==1) ? "-3" : " ");
		set_ds_source(ds_kurtosis, "data from %d files %s and %s", n_files, theta_root_filename, excitation_root_filename);
	}
	if (bool_GC==1)
	{ 	ds_GC=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		if (bool_normalize_by_kT==1) ds_GC->treatement = my_sprintf(ds_GC->treatement,"data divided by kT = %g", my_kT);
        ds_GC->treatement = my_sprintf(ds_GC->treatement,"GC estimator 2<x>/\\sigma^2 vs \\tau");
		set_ds_source(ds_GC, "data from %d files %s and %s", n_files, theta_root_filename, excitation_root_filename);
	}
	if (bool_GC4==1)
	{ 	ds_GC4=create_and_attach_one_ds (op2, n_tau, n_tau, 0);
		if (bool_normalize_by_kT==1) ds_GC4->treatement = my_sprintf(ds_GC4->treatement,"data divided by kT = %g", my_kT);
        ds_GC4->treatement = my_sprintf(ds_GC4->treatement,"GC estimator at order 4");
		set_ds_source(ds_GC4, "data from %d files %s and %s", n_files, theta_root_filename, excitation_root_filename);
	}
	remove_ds_n_from_op(op2, 0);
	
	total_sigma = (double*)calloc(n_tau, sizeof(double));
	dispersion  = (double*)calloc(n_tau, sizeof(double));
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%d%s", theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);

		sprintf(filename, "%s%d%s", indices_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_excitation=load_data_bin(filename, &excitation);
		
		if (n_excitation<n_theta-2) return(win_printf_OK("M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		if ( (bool_search_indices==1) && (k==0))	// first file: we autodetect some parameters		
		{	i=find_search_domain(excitation, n_excitation, plateau_position, &y_min, &y_max);
			if (i==CANCEL) return(D_O_K);
		}
	
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);
		
		ind = find_set_of_indices(excitation, 
						( (bool_search_indices==1) ? n_excitation : n_excitation - tau_index[n_tau-1]), 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
		if (ind==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));
		
					
		if ((ind[n_cycles-1]+tau_index[n_tau-1])>=n_excitation)
		{	i=win_printf_OK("filen %s\nlarger tau %d is too large!\nlast index is %d\n%d points in dataset\n\n"
						"click CANCEL to abort or OK to remove one more period",
						filename, tau_index[n_tau-1], ind[n_cycles-1], n_excitation);
			if (i==CANCEL) return(D_O_K);
			n_cycles --;
		}

			
		if (strcmp(indices_root_filename, excitation_root_filename)!=0)
		{	sprintf(filename, "%s%d%s", excitation_root_filename, i_file, file_extension);
			sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
			n_excitation=load_data_bin(filename, &excitation);
		
			if (n_excitation!=n_theta) return(win_printf_OK("forcing M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		}
		
		n_cycles_total += n_cycles;
		if (n_cycles<1) 	return(win_printf_OK("Mmm... not enough ramps in file %s", filename));

	
		if (bool_report==1)	
		{	if (bool_search_indices==1)
				win_printf_OK("file number %d/%d\n(\\theta in %s)\n\n"
							"%d points in \\theta\n%d points in excitation M\n\n%d usable cycles found", 
							k+1, n_files, filename, n_theta, n_excitation, n_cycles);
			else
				win_printf_OK("file number %d/%d\n(%s)\n\n"
							"%d points in \\theta file\n%d usable windows, separated by %d points\n"
							"(cf larger \\tau is %d)", 
							k+1, n_files, filename, n_theta, n_cycles, N_shift_auto, tau_index[n_tau-1]);
		}

		sprintf(s, "%d/%d computing moments", k+1, n_files); spit(s);
		
		for (i=0; i<n_tau; i++)
		{	tau=tau_index[i];
			n_intervals=n_cycles;
			y_tau=(double*)calloc(n_cycles, sizeof(double));
			for (i2=0, m=0; i2<n_cycles; i2++)
			{	j=ind[i2];	// point de départ de l'intégration
				a_tau = compute_power_function(excitation, theta, j, tau, indice_reference);
				y_tau[m]=(double)a_tau/my_kT;
				m++;
			}
			mean	= gsl_stats_mean(y_tau, 1, n_intervals);
			sigma	= gsl_stats_sd_m(y_tau, 1, n_intervals, mean);
			total_sigma[i] += sigma*n_cycles;
			dispersion[i]  += sigma; // we will measure the diff between this estimator of sigma, and the other one

			if (bool_mean==1)
			{	ds_mean->yd[i] += (float)mean*(float)n_cycles;
			}
			if (bool_sigma==1)
			{	if (k==0) ds_sigma->yd[i]=0.;
				ds_sigma->yd[i] += (float)sigma*(float)n_cycles;  
			}
			if (bool_skew==1)
			{	skewness = gsl_stats_skew_m_sd (y_tau, 1, n_intervals, mean, sigma);
				ds_skewness->yd[i] += (float)skewness*(float)n_cycles;
			}
			if (bool_kurtosis==1)
			{	kurtosis = gsl_stats_kurtosis_m_sd (y_tau, 1, n_intervals, mean, sigma);
				ds_kurtosis->yd[i] += (float)kurtosis*(float)n_cycles;
			}
			if (bool_GC==1)
			{	if (sigma!=0) 	GC_estimate = (double)2.*mean/(sigma*sigma*tau);
				else 			GC_estimate = 0.;
				GC_estimate	*= (double)n_cycles;
				if (tau_units==0)	ds_GC->yd[i] += (float)GC_estimate;
				else 				ds_GC->yd[i] += (float)GC_estimate*fec;
			}
			if (bool_GC4==1)
			{	if (sigma!=0) 	GC_tmp = (double)2.*mean/(sigma*sigma*tau);
				else 			GC_tmp = 0.;
				GC4_estimate    = GC_estimate 
								+ ( -4.*skewness + (kurtosis*mean)/sigma
									+ skewness*mean*mean/(sigma*sigma) - kurtosis*mean*mean*mean/(3.*sigma*sigma*sigma)
								  )/sigma;
				GC4_estimate	*= (double)n_cycles;
				if (tau_units==0)	ds_GC4->yd[i] += (float)GC4_estimate;
				else 				ds_GC4->yd[i] += (float)GC4_estimate*fec;
			}
			free(y_tau);
		
		} //end of loop over i, ie, loop over tau

		free(excitation);
		free(theta);
		free(ind);
		
	} //end of the loop over k, ie, loop over the files;
	free(files_index);
	
	for (i=0; i<n_tau; i++) 
	{	total_sigma[i] /= (double)(n_cycles_total);
		dispersion[i]  /= (double)(n_files);
	}
		
	if (bool_mean==1)
	{	for (i=0; i<n_tau; i++) 
		{	ds_mean->xd[i] = tau_index[i];
			ds_mean->yd[i] /= (float)(n_cycles_total);
		}
		if (bool_dispersion==1)
		{	if ((alloc_data_set_y_error(ds_mean)==NULL) || (ds_mean->ye==NULL))
					return win_printf_OK("I can't create errors !");
			for (i=0; i<n_tau; i++) 
			{	if (total_sigma[i]!=0) ds_mean->ye[i] = ds_mean->yd[i]*(float)((dispersion[i]-total_sigma[i])/total_sigma[i]);
			}
		}
	}
	if (bool_sigma==1)
	{	for (i=0; i<n_tau; i++)
		{	ds_sigma->xd[i] = tau_index[i];
			ds_sigma->yd[i] /= (float)(n_cycles_total);
		}
		if (bool_dispersion==1)
		{	if ((alloc_data_set_y_error(ds_sigma)==NULL) || (ds_sigma->ye==NULL))
					return win_printf_OK("I can't create errors !");
			for (i=0; i<n_tau; i++) 
			{	ds_sigma->ye[i] = (float)((dispersion[i]-total_sigma[i]));
			}
		}
	}
	
	if (bool_skew==1)
	{	for (i=0; i<n_tau; i++)
		{	ds_skewness->xd[i] = tau_index[i];
			ds_skewness->yd[i] /= (float)n_cycles_total;
		}
		if (bool_dispersion==1)
		{	if ((alloc_data_set_y_error(ds_skewness)==NULL) || (ds_skewness->ye==NULL))
					return win_printf_OK("I can't create errors !");
			for (i=0; i<n_tau; i++) 
			{	if (total_sigma[i]!=0)
					ds_skewness->ye[i] = ds_skewness->yd[i]*(float)(3.*(dispersion[i]-total_sigma[i])/total_sigma[i]);
			}
		}
	}
	if (bool_kurtosis==1)
	{	for (i=0; i<n_tau; i++)
		{	ds_kurtosis->xd[i] = tau_index[i];
			ds_kurtosis->yd[i] /= (float)n_cycles_total;
		
			if (bool_substract_3_to_kurtosis==0) 
			{	ds_kurtosis->yd[i] += (float)3.; 
			}
		}
		if (bool_dispersion==1)
		{	if ((alloc_data_set_y_error(ds_kurtosis)==NULL) || (ds_kurtosis->ye==NULL))
					return win_printf_OK("I can't create errors !");
			for (i=0; i<n_tau; i++) 
			{	if (abs(total_sigma[i])>1e-33) 
					ds_kurtosis->ye[i] = ds_kurtosis->yd[i]*(float)(4.*(dispersion[i]-total_sigma[i])/total_sigma[i]);
			}
		}
	}
	if (bool_GC==1)
	{	for (i=0; i<n_tau; i++) 
		{	ds_GC->xd[i] = tau_index[i];
			ds_GC->yd[i] /= (float)(n_cycles_total);
		}
	}
	if (bool_GC4==1)
	{	for (i=0; i<n_tau; i++) 
		{	ds_GC4->xd[i] = tau_index[i];
			ds_GC4->yd[i] /= (float)(n_cycles_total);
		}
	}
	
	free(tau_index);
	free(total_sigma);
	free(dispersion);
	set_plot_title(op2,"moments");
	set_plot_x_title(op2, "\\tau (nb of points, 1/f_{ec})");
	set_plot_y_title(op2, "moments (%d cycles from %d files)", n_cycles_total, n_files);
	op2->filename = Transfer_filename(filename);
    op2->dir = Mystrdup(pr->o_p[pr->cur_op-1]->dir);

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_stats_periodic_build_from_M_dep_files' */





int do_hist_full_cycles_from_M_dep_files(void)
{	int		i, k; // slow loops, no need of registers, let's save some cache memo/(k_b*Temperaturery 
register int	i2, j, j2;
	O_p 		*op2, *op1=NULL;
static int	n_bin=100;
	int		n_theta, n_excitation, n_cycles, n_cycles_total=0;
	gsl_histogram *hist;
	double	a_tau;
static float	h_min=-5, h_max=15;
	d_s 		*ds2;
	pltreg	*pr = NULL;
	int		tau, i_file, n_tau, n_files;	// current tau, current file number, number of taus, number of files
	int		*tau_index=NULL;		// all the taus
	int		*files_index=NULL;		// all the files
	int		index;
static	char	tau_string[128]		="1,2,5,10:5:50";
static	char files_ind_string[128]	="1:1:20"; 
static	char file_extension[8]		=".dat";
static	char	theta_root_filename[128]   = "dep";
static	char excitation_root_filename[128] = "M";
	char		s[512];
	char		filename[256];
	int		*ind=NULL; // indices
	float	*excitation, *theta; // *power;
	float	y_min=0, y_max=1;
static int	bool_remove_bounds=1 ;
static int  	n_last=1; // how many points should be removed at the end of the dataset
static int	bool_report=0;
static int	plateau_position=0;
static int	point_position=1;
static int	bool_add_more_indices=0, bool_shift_indices=0, bool_substract_vanZon=1, indice_reference=0;
static int	n_more=1, n_shift=0;
	int  	bool_search_indices=1;
	int		N_shift_auto=1;
static float	fec=2048.;
float		periode_typique=0.;
	int		n_pts_integration;
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine builds a set of histograms\n"
				"out of a set of files of \\theta(t) and M(t).\n\n"
				"data W_\\tau(t) is computed as :\n\n"
				"W_\\tau(t) = {1 \\over \\tau} \\int_t^{t+\\tau} -(M(t')-\\epsilonM(t)).{\\frac{d\\theta}{dt}}(t') dt'\n\n"
				"with \\epsilon = 0 or 1\n"
				"\\tau is a perfect multiple of the excitation period.\n\n"
				"A new plot is created");
	}
	
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"root name of file with angle \\theta %%s"
			"root name of file with excitation M (used also to find indices) %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n"
			"%%b print to screen a report for each file");
	i=win_scanf(s, &theta_root_filename, &excitation_root_filename, &files_ind_string, &file_extension, &bool_report);
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("sampling frequency ? %8f", &fec);
	if (i==CANCEL) return(OFF);
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices, &point_position, &plateau_position, 
							&bool_remove_bounds, &n_last, &bool_shift_indices, &n_shift, 
							&bool_add_more_indices, &n_more, &N_shift_auto);
	if (i==CANCEL) return(OFF);

	if (bool_add_more_indices==0) n_more=1;
			
	i=win_scanf("%b Do you want the van Zon power/work ?\n\n"
				"W = \\frac{1}{\\tau}\\int_t^{t+\\tau} - [ M(t') - M(t)] \\frac{{d\\theta}{dt}} (t') dt'\n\n"
				"if you want this, what should be substracted ?\n"
				"%R M(t) or %r M(t-1)\n"
				"{\\pt6 (best is M(t)}", &bool_substract_vanZon, &indice_reference);
	if (i==CANCEL) return(D_O_K);
				
	i=win_scanf("number of bins in histogram? %d min %f max %f\n"
			"using averaged data {\\pt16 a_\\tau(t) = {1 \\over \\tau} \\int_t_i^{t_i+\\tau} a(t') dt'}\n\n"
			"\\tau (integer, {\\color{yellow}multiple of excitation period}) ? %s"
			"Integration in time starts at times t_i found with previous rule.",
			&n_bin, &h_min, &h_max, &tau_string);
	if (i==CANCEL) return OFF;			
	
	tau_index = str_to_index(tau_string, &n_tau);	// malloc is done by str_to_index
	if ( (tau_index==NULL) || (n_tau<1) )	return(win_printf("bad values for \\tau !"));
		
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
	if ((op2 = create_and_attach_one_plot(pr, n_bin, n_bin, 0))==NULL)	return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	
	if (bool_add_more_indices==1)
	{	n_more -= n_shift;
//		n_more -= tau_index[n_tau-1]; // not to escape from the ramp
	}	
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%d%s", theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);

		sprintf(filename, "%s%d%s", excitation_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_excitation=load_data_bin(filename, &excitation);
		
		if (n_excitation!=n_theta) return(win_printf_OK("M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		if ( (bool_search_indices==1) && (k==0))	// first file: we autodetect some parameters		
		{	i=find_search_domain(excitation, n_excitation, plateau_position, &y_min, &y_max);
			if (i==CANCEL) return(D_O_K);
		}
	
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);
		
		ind = find_set_of_indices(excitation, 
						( (bool_search_indices==1) ? n_excitation : n_excitation - tau_index[n_tau-1]), 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
		if (ind==NULL)  return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));
		if (n_cycles<2) return(win_printf_OK("file %s\nNot enough cycles... I need more than 1!",filename));
			
		if (k==0)		
		{	periode_typique = (float)(ind[n_cycles-n_more] - ind[0])/(float)((float)n_cycles/(float)n_more-1.);
			i=win_printf("Typical period of excitation signal,\n"
						"from first file : %6.2f", periode_typique);
			if (i==CANCEL) return(D_O_K);
		}
		
		if ((ind[n_cycles-1]+tau_index[n_tau-1]*periode_typique)>=n_excitation)
		{	i=win_printf("filen %s\nlarger tau %d is too large!\nlast index is %d\n%d points in dataset\n"
						"typical forcing period is %6.2f\n"
						"click CANCEL to abort or OK to remove one more period",
						filename, tau_index[n_tau-1], ind[n_cycles-1], n_excitation, periode_typique);
			if (i==CANCEL) return(D_O_K);
			n_cycles --;
		}

		n_cycles_total += n_cycles;
		if (n_cycles<1) 	return(win_printf_OK("Mmm... not enough ramps in file %s", filename));
			
		if (bool_report==1)	
		{	if (bool_search_indices==1)
				win_printf_OK("file number %d/%d\n(\\theta in %s)\n\n"
							"%d points in \\theta\n%d points in excitation M\n\n%d usable cycles found", 
							k+1, n_files, filename, n_theta, n_excitation, n_cycles);
			else
				win_printf_OK("file number %d/%d\n(%s)\n\n"
							"%d points in \\theta file\n%d usable windows, separated by %d points\n"
							"(cf larger \\tau is %d)", 
							k+1, n_files, filename, n_theta, n_cycles, N_shift_auto, tau_index[n_tau-1]);
		}

		sprintf(s, "%d/%d cumulating histograms", k+1, n_files); spit(s);
							
		for (i=0; i<n_tau; i++)
		{	tau=tau_index[i];

			if (k==0) // first file : we create the datasets
			{	if (i!=0) // not the first tau: we add a dataset
				{	if ((ds2 = create_and_attach_one_ds(op2, n_bin, n_bin, 0)) == NULL)	
						return win_printf_OK("cannot create dataset !");
				}
				ds2->treatement = my_sprintf(ds2->treatement,"histogram, averaged data over \\tau=%d periods", tau);
						
				hist = gsl_histogram_alloc(n_bin);
				gsl_histogram_set_ranges_uniform (hist, (double)h_min, (double)h_max);
			}
			else 
			{	ds2=op2->dat[i];
				ds_to_histogram(ds2, &hist); // hist will be allocated perfectly
			}

			for (i2=0; i2<=n_cycles-n_more*tau; i2++)
			{	j=ind[i2];	// point de départ de l'intégration
				a_tau=0.;
				n_pts_integration = ind[i2+tau*n_more]-j;
				for (j2=0; j2<n_pts_integration; j2++)
				{	a_tau += - ( (double)excitation[j+j2] - (double)bool_substract_vanZon*excitation[j - indice_reference] )
								*((double)theta[j+j2]-theta[j+j2-1]);	// average over tau points
				}
				gsl_histogram_increment(hist, (double)(a_tau*fec/(k_b*Temperature*(double)n_pts_integration))); // histogram of averaged data
			}

			histogram_to_ds(hist, ds2, n_bin);
			gsl_histogram_free(hist);
	
		} //end of loop over i, ie, loop over tau

		free(excitation);
		free(theta);
	//	free(power);
		free(ind);
		
	} //end of the loop over k, ie, loop over the files;
	free(tau_index);
	free(files_index);
	
	set_plot_title(op2,"histograms");
	if (bool_search_indices==1) 
		set_plot_x_title(op2, "\\frac{1}{\\tau} W_\\tau (%d cycles from %d files)", n_cycles_total, n_files);
	else
		set_plot_x_title(op2, "\\frac{1}{\\tau} W_\\tau (%d windows from %d files)", n_cycles_total, n_files);
	set_plot_y_title(op2, "pdf");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

    	op2->iopt |= YLOG;

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_hist_full_cycles_from_M_dep_files' */




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//moyenne sur des cycles//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




int do_average_over_cycles(void)
{
	double  dt, moy;
	float t_init=0.;
	int	 ny, n_cycles;
	int   period, period_min, period_max;
	int	 index;
	int	 do_shift=0;
	int	 *ind; // indices
	register int 	i, i2;
	O_p 	*op2, *op1 = NULL;
	d_s 	*ds2, *ds1, *ds_ind;
	pltreg	*pr = NULL;
	char	s[512];
	
	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine averages a dataset\n"
				"and places the result in a new plot\n\n"
				"data a(t) is averaged avec cycles starting at times t_i\n"
				"with t_i from a dataset of indices\n"
				"(indices are assumed to be in the next ds, after the current one)");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	ny=ds1->nx;
	if (op1->n_dat<=1)	return(win_printf_OK("I need 2 datasets!\n"
				"active one for the data,\nnext one for indices"));
	ds_ind = (op1->cur_dat == op1->n_dat-1 ) ? op1->dat[0] : op1->dat[op1->cur_dat + 1];
	n_cycles = ds_ind->nx;
	if (n_cycles>=ny) 	return(win_printf_OK("You probably exchanged the 2 datasets ;-)"));

	dt     = (double)(ds1->xd[ny-1]-ds1->xd[0])/(ny-1);
//	win_printf("dt is \n%g\n%g\n%g", dt, ds1->xd[2]-ds1->xd[1], ds1->xd[1]);
	if ( (ds1->xd[2]-ds1->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)				return(win_printf_OK("dt is zero !"));
	ind = (int*)calloc(n_cycles, sizeof(int));
	for (i=0; i<n_cycles; i++)	ind[i] = (int)rintf((ds_ind->xd[i]-ds1->xd[0])/dt);
	period_min=ny;
	period_max=0;
	for (i=1; i<n_cycles; i++)
	{	period = (ind[i]-ind[i-1]);
		if (period<period_min) period_min=period;
		if (period>period_max) period_max=period;
	}
	
	sprintf(s,"From given indices (dataset %d), the period $T$ of the signal is\n"
			"%d <= $T$ <= %d (in points)\n"
			"%6.0g <= $T$ <= %6.0g (in seconds)\n"
			"\n"
			"There are %d periods\n\n"
			"%%R start x-axis at t=0"
			"%%r start x-axis at first index",
			(op1->cur_dat == op1->n_dat-1 ) ? 0 : op1->cur_dat + 1,
			period_min, period_max, dt*period_min, dt*period_max, n_cycles);
	i=win_scanf(s, &do_shift);
	if (i==CANCEL) return OFF;

	if ((ind[n_cycles-1]+period_min)>=ny)	return(win_printf_OK("please remove the last point!"));
		
	
	if ((op2 = create_and_attach_one_plot(pr, period_min, period_min, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	 
	if (do_shift==1)	t_init=ds1->xd[ind[0]];
	else				t_init=0;
	
	for (i=0; i<period_min; i++)
	{	
		moy=0;
		for (i2=0; i2<n_cycles; i2++)		moy += ds1->yd[ind[i2]+i];
			
		ds2->xd[i] = t_init + (float)(dt*i);
		ds2->yd[i] = (float)(moy/n_cycles);
	}

	ds2->treatement = my_sprintf(ds2->treatement,"averaged over %d cycles", n_cycles);
	inherit_from_ds_to_ds(ds2, ds1);
	set_plot_title(op2,"averaged signal");
	set_plot_x_title(op2, "time (s)",n_cycles);
	set_plot_y_title(op2, "signal");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_average_over_cycles' */

int do_cycles_correl_function(void)
{
	double  dt, moy;
	float t_max=20., d_t=0.1,norm, inter;
	int n_2=1,tot;
	int n_max;
	int	 ny;
	int	 index;
	int bool_normalize=1;
	register int 	i, i2;
	O_p 	*op2, *op1 = NULL;
	d_s 	*ds2, *ds1;
	pltreg	*pr = NULL;
	char	s[512];

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine do the correlation function of a dataset\n"
				"and places the result in a new plot\n\n");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	ny=ds1->nx;

	dt     = (double)(ds1->xd[ny-1]-ds1->xd[0])/(ny-1);
	if ( (ds1->xd[2]-ds1->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)				return(win_printf_OK("dt is zero !"));
	
	i=win_scanf("time interval [0;t] t = %8f s? \n\n"
				"pas de temps  dt = %8f s\n\n"
				"skip %8d between two consecutive points\n\n"
				"%b normalize correlation function", &t_max ,&d_t ,&n_2, &bool_normalize);
	if (i==CANCEL) return(OFF);	
	n_max = (int)t_max/dt;
	if ((op2 = create_and_attach_one_plot(pr, n_max, n_max, 0)) == NULL)
		return win_printf_OK("cannot create plot !");
	ds2 = op2->dat[0];
	
	moy=0;
	for (i=0; i<ny; i++)
	{	
		moy += ds1->yd[i];
	}
	moy/=(float)ny;
	win_printf("%f",moy);
	
	inter=(float) d_t/dt;
	for (i=0; i<n_max; i=i+(int)inter)
	{	sprintf(s, "%d pourcent done", (int)100*i/n_max); spit(s);
		tot=0;
		for (i2=0; i2<ny-i; i2=i2+n_2) 
		{
			ds2->yd[i]+= (float)(ds1->yd[i2]-moy)*(ds1->yd[i2+i]-moy);
			tot+=1;
		}
		ds2->yd[i]/=(float)(tot);
		ds2->xd[i] = (float)(dt*i);
	}
	norm=ds2->yd[0];
	if (bool_normalize==1) 
	for (i=0; i<n_max; i=i+(int)inter)	ds2->yd[i]/=(float)norm;
	inherit_from_ds_to_ds(ds2, ds1);
	set_plot_title(op2,"correlation function");
	set_plot_x_title(op2, "time (s)");
	set_plot_y_title(op2, "signal");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_cycle_correl_function' */


int do_average_over_cycles_from_files(void)
{
register int 	i, i2, k;
	int  	 period, period_min=0., period_max;
	int	 index, i_file, n_files;
	int	 *ind; // indices
	O_p 	*op2 = NULL, *op1 = NULL;
	d_s 	*ds2 = NULL;
	pltreg	*pr = NULL;
	char	s[512];
	int	*files_index=NULL;		// all the files
static	char 	files_ind_string[128]	="1:1:20"; 
static	char 	file_extension[8]		=".bin";
static	char 	theta_root_filename[128]   = "run01theta";
static	char 	indices_root_filename[128] = "run01M";
static int	plateau_position=0;
static int	point_position=1;
static int	bool_add_more_indices=0, bool_shift_indices=0;
static int	n_more=1, n_shift=0;
	int  	bool_search_indices=1;
	int	N_shift_auto=1;
static  char 	path_name[512] = "1";
	char	filename[512];
static float 	fec=32768.;
static int	bool_remove_bounds=1 ;
static int  	n_last=1; // how many points should be removed at the end of the dataset
	float	*excitation, *theta;
	int	n_theta=0., n_excitation, n_cycles, n_cycles_total=0;
	float	y_min=0, y_max=1;
	char 	last_path[512];  
	

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine averages a dataset\n"
				"out of a set of files of \\theta(t) and M(t).\n\n"
				"and places the result in a new plot\n\n"
				"data a(t) is averaged avec cycles starting at times t_i\n");
	}
	
	
	sprintf(last_path,"last_visited_%d",0);
	strcpy(path_name,(char*)get_config_string("GR-FILE-PATH",last_path,NULL));
    
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"path name %%s"
			"root name of file with angle \\theta, or power P %%s"
			"root name of file to find indices {\\pt8(usually, excitation files)} %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n");
	i=win_scanf(s, &path_name, &theta_root_filename, &indices_root_filename, &files_ind_string, &file_extension);
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("sampling frequency ? %8f\n\n", &fec);
	if (i==CANCEL) return(OFF);
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf("bad values for files indices !"));
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices, &point_position, &plateau_position, 
				&bool_remove_bounds, &n_last, &bool_shift_indices, &n_shift, 
							&bool_add_more_indices, &n_more, &N_shift_auto);
	if (i==CANCEL) return(OFF);

				
	
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%s%d%s", path_name, theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);
		if (n_theta==-1) // then error while opening the file
			{	return(win_printf_OK("error with file\n%s\ncreated with this information:\n%s\n%s\n%d\n%s", 
           		       			filename, path_name, theta_root_filename, i_file, file_extension));
			}
			       
		sprintf(filename, "%s%s%d%s", path_name, indices_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_excitation=load_data_bin(filename, &excitation);
		
		if (n_excitation!=n_theta) return(win_printf_OK("Commutation has %d points but \\theta has %d points", n_excitation, n_theta));
		
		if ( (bool_search_indices==1) && (k==0))	// first file: we autodetect some parameters		
		{	i=find_search_domain(excitation, n_excitation, plateau_position, &y_min, &y_max);
			if (i==CANCEL) return(D_O_K);
		}
	
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);
		
		ind = find_set_of_indices(excitation, 
						( (bool_search_indices==1) ? n_excitation : n_excitation), 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
		if (ind==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));
			
		
		if (n_cycles<1) 	return(win_printf_OK("Mmm... not enough ramps in file %s", filename));
		if (k==0) 
		{	period_min=n_theta;
			period_max=0;
			for (i=1; i<n_cycles; i++)
			{	period = (ind[i]-ind[i-1]);
				if (period<period_min) period_min=period;
				if (period>period_max) period_max=period;
			}
		 	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
			if ((op2 = create_and_attach_one_plot(pr, period_min, period_min, 0)) == NULL)	return win_printf_OK("cannot create plot !");
			ds2 = op2->dat[0];
		}
		while ((ind[n_cycles-1]+period_min)>n_theta) n_cycles-=1;
		n_cycles_total += n_cycles;
		
		for (i=0; i<period_min; i++)
		{	for (i2=0; i2<n_cycles; i2++)
			{	ds2->yd[i]+=(float)theta[ind[i2]+i];
			}
		}
		free(excitation);
		free(theta);
		free(ind);
		
	} //end of the loop over k, ie, loop over the files;
	free(files_index);
	
	for (i=0;i<period_min;i++)
	{	ds2->xd[i] = (float)(i/fec);
		ds2->yd[i] /= (float)n_cycles_total;
	}
	win_printf("Nombre total de cycles %d", n_cycles_total);
	set_plot_title(op2,"averaged signal");
	set_plot_x_title(op2, "time (s)",n_cycles);
	set_plot_y_title(op2, "signal");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_average_over_cycles from files' */



int do_correl_over_cycles_from_files(void)
{
register int 	i, i1,i2, i3, k;
	int   	period, period_min=0., period_max;
	int	index, i_file, n_files;
	int	*ind; // indices
	O_p 	*op2 = NULL, *op1 = NULL;
	d_s 	*ds2 = NULL;
	pltreg	*pr = NULL;
	char	s[512];
	int	*files_index=NULL;		// all the files
	int	ti=0, n_ti;	// current tau, current file number, number of taus, number of files
	int	*ti_index=NULL;		// all the taus
static	char 	ti_string[128]		="1,2,5,10:5:50";
static	char 	files_ind_string[128]	="1:1:20"; 
static	char 	file_extension[8]		=".bin";
static	char 	theta_root_filename[128]   = "run01theta";
static	char 	indices_root_filename[128] = "run01M";
static int	plateau_position=0, point_position=1;
static int	bool_add_more_indices=0, bool_shift_indices=0;
static int	n_more=1, n_shift=0;
	int  	bool_search_indices=1;
	int	N_shift_auto=1;
static  char 	path_name[512] = "1";
	char	filename[512];
static float 	fec=32768.;
static int	bool_remove_bounds=1 ;
static int 	n_last=1; // how many points should be removed at the end of the dataset
	float	*excitation, *theta, *average=NULL;
	int	n_theta=0., n_excitation, n_cycles, n_cycles_total=0;
	float	y_min=0, y_max=1;
	char last_path[512]; 
	float calib=1.;
static int n_average=16;


	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine do a correlation of a dataset\n"
				"out of a set of files of \\theta(t) and M(t).\n\n"
				"and places the result in a new plot\n\n"
				"data theta(t)theta(t+\\tau) is averaged over cycles starting at times t_i\n\n"
				"We chose some value of t and \\tau goes from -t to the semi-period of the period");
	}
	
	
	sprintf(last_path,"last_visited_%d",0);
	strcpy(path_name,(char*)get_config_string("GR-FILE-PATH",last_path,NULL));
    
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"path name %%s"
			"root name of file with angle \\theta, or power P %%s"
			"root name of file to find indices {\\pt8(usually, excitation files)} %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s\n");
	i=win_scanf(s, &path_name, &theta_root_filename, &indices_root_filename, &files_ind_string, &file_extension);
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("sampling frequency ? %8f\n\n", &fec);
	if (i==CANCEL) return(OFF);
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf_OK("bad values for files indices !"));
	
	i=ask_for_parameters_to_search_for_indices(&bool_search_indices, &point_position, &plateau_position, 
				&bool_remove_bounds, &n_last, &bool_shift_indices, &n_shift, 
							&bool_add_more_indices, &n_more, &N_shift_auto);
	if (i==CANCEL) return(OFF);

	i=win_scanf("correlation function is <n(t_i)n(t_i+\\tau)>\n\n"
			"t_i (integer, nb of pts) ? %s\n\n"
			"average over %d points around t_i",
			&ti_string, &n_average);
	if (i==CANCEL) return OFF;
	
	ti_index = str_to_index(ti_string, &n_ti);	// malloc is done by str_to_index
	if ( (ti_index==NULL) || (n_ti<1) )	return(win_printf_OK("bad values for \\ti !"));

//Average
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%s%d%s", path_name, theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);
		if (n_theta==-1) // then error while opening the file
			{  return(win_printf_OK("error with file\n%s\ncreated with this information:\n%s\n%s\n%d\n%s", 
           		       filename, path_name, theta_root_filename, i_file, file_extension));
   			     }
		sprintf(filename, "%s%s%d%s", path_name, indices_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_excitation=load_data_bin(filename, &excitation);
		
		if (n_excitation!=n_theta) return(win_printf_OK("M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		if ( (bool_search_indices==1) && (k==0))	// first file: we autodetect some parameters		
		{	i=find_search_domain(excitation, n_excitation, plateau_position, &y_min, &y_max);
			if (i==CANCEL) return(D_O_K);
		}
	
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);
		
		ind = find_set_of_indices(excitation, 
						( (bool_search_indices==1) ? n_excitation : n_excitation), 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
		if (ind==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));
		free(excitation);	
		if (k==0) 
		{	
			period_min=n_theta;
			period_max=0;
			for (i=1; i<n_cycles; i++)
			{	period = (ind[i]-ind[i-1]);
				if (period<period_min) period_min=period;
				if (period>period_max) period_max=period;
			}
			//if (ti>period_min-1)	return(win_printf_OK("ti is too large (maximum value for ti is %d !",period_min));
		 	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
			if ((op2 = create_and_attach_one_plot(pr, period_min, period_min, 0)) == NULL)	return win_printf_OK("cannot create plot !");
			ds2 = op2->dat[0];
			average=(float*)calloc(period_min, sizeof(float));
			for (i=0; i<period_min; i++) average[i] = 0;
			
		}
		while (ind[n_cycles-1]+ti_index[n_ti-1]+period_min>n_theta) n_cycles--;
		n_cycles_total += n_cycles;
		if (n_cycles<1) 	return(win_printf_OK("Mmm... not enough ramps in file %s", filename));
		

		for (i1=0; i1<period_min; i1++)
		{	for (i2=0; i2<n_cycles; i2++)
				{	average[i1]+=(float)theta[ind[i2]+i1];
					
				}
		}
		
		free(theta);
		free(ind);
		
	} //end of the loop over k, ie, loop over the files;

	for (i1=0; i1<period_min; i1++)
	{
	average[i1]=average[i1]/n_cycles_total;
	}
	
//Correlation function
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%s%d%s", path_name, theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);
		if (n_theta==-1) // then error while opening the file
			{  return(win_printf_OK("error with file\n%s\ncreated with this information:\n%s\n%s\n%d\n%s", 
           		       filename, path_name, theta_root_filename, i_file, file_extension));
   			     }
		sprintf(filename, "%s%s%d%s", path_name, indices_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_excitation=load_data_bin(filename, &excitation);
		
		if (n_excitation!=n_theta) return(win_printf_OK("M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);
		
		ind = find_set_of_indices(excitation, 
						( (bool_search_indices==1) ? n_excitation : n_excitation), 
						&n_cycles,
						bool_search_indices, point_position, plateau_position, 
						bool_remove_bounds, n_last, 
						bool_shift_indices, n_shift, 
						bool_add_more_indices, n_more,
						N_shift_auto,
						y_min, y_max);
		if (ind==NULL) return(win_printf_OK("file %s\nNo points left. Found indices were probably only at the boundaries...",filename));
		free(excitation);	

		
		while (ind[n_cycles-1]+ti_index[n_ti-1]+period_min>n_theta) n_cycles--;
		n_cycles_total += n_cycles;
		if (n_cycles<1) 	return(win_printf_OK("Mmm... not enough ramps in file %s", filename));
		for (i=0; i<n_ti; i++)
		{	ti=ti_index[i];
			if (k==0) // first file : we create the datasets
			{	if (i!=0) // not the first ti: we add a dataset
				{	if ((ds2 = create_and_attach_one_ds(op2, period_min, period_min, 0)) == NULL)	
					return win_printf_OK("cannot create dataset !");
				}
			}
			else 
			{	ds2=op2->dat[i];
			}
			
			 
			for (i1=n_average; i1<(period_min-n_average); i1++) // i1 = ti + tau
			{	for (i2=0; i2<n_cycles; i2++)
				{	for (i3 = 0; i3<n_average ;i3++)
					{ds2->yd[i1] +=(float)(theta[ind[i2]+ti+i3-n_average]-average[ti]+i3-n_average)*(theta[ind[i2]+i1+i3-n_average]-average[i1+i3-n_average]);
					}
				}
			}
		}
	
		
		free(theta);
		free(ind);
		
	} //end of the loop over k, ie, loop over the files;
	free(files_index);
	for (i=0; i<n_ti; i++)
	{
	ti=ti_index[i];
	ds2=op2->dat[i];
	for (i1=0; i1<period_min; i1++)
	{
	ds2->xd[i1] = (float)(i1-ti)/fec;
	ds2->yd[i1] = (float)ds2->yd[i1]/n_cycles_total/n_average;
	//ds2->yd[i1] = (float)average[i1];
	}
	calib=(float)ds2->yd[ti];
 	for (i1=0; i1<period_min; i1++)
 	{
	ds2->yd[i1] = (float)ds2->yd[i1]/calib;
 	}
	ds2->treatement = my_sprintf(ds2->treatement,"correlation, starting at time ti=%d", ti);
 	}
	free(average);
	free(ti_index);
	win_printf("Nombre total de cycles %d", n_cycles_total);

	set_plot_title(op2,"averaged signal");
	set_plot_x_title(op2, "time (s)",n_cycles);
	set_plot_y_title(op2, "signal");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_correl_over_cycles from files' */


int do_correl_from_files(void)
{
register int 	i, i1, k;
	int	index, i_file, n_files;
	O_p 	*op2 = NULL, *op1 = NULL;
	d_s 	*ds2 = NULL;
	pltreg	*pr = NULL;
	char	s[512];
	int	*files_index=NULL;		// all the files
	int	ti=0, n_ti;	// current tau, current file number, number of taus, number of files
	int	*ti_index=NULL;		// all the taus
static	char 	ti_string[128]		="1,2,5,10:5:50";
static	char 	files_ind_string[128]	="1:1:20"; 
static	char 	file_extension[8]		=".bin";
static	char 	theta_root_filename[128]   = "run01theta";
static	char 	indices_root_filename[128] = "run01M";
static  char 	path_name[512] = "1";
	char	filename[512];
static float 	fec=32768.;
	float	*excitation, *theta, *average=NULL, calib;
	int	n_theta=0., n_excitation;
	char last_path[512]; 
static int n_average=16;
static int bool_normalize=1;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine do a correlation of a dataset\n"
				"out of a set of files of \\theta(t) and M(t).\n\n"
				"and places the result in a new plot\n\n"
				"data theta(t)theta(t+\\tau) is averaged starting at times t_i\n\n"
				"We chose some value of t and \\tau goes from -t to the semi-period of the period");
	}
	
	
	sprintf(last_path,"last_visited_%d",0);
	strcpy(path_name,(char*)get_config_string("GR-FILE-PATH",last_path,NULL));
    
	sprintf(s, "{\\color{yellow}\\pt14 Operate on several files}\n"
			"path name %%s"
			"root name of file with angle \\theta, or power P %%s"
			"root name of file to find indices {\\pt8(usually, excitation files)} %%s"
			"range of files %%s"
			"file extension (note: files should contain floats) %%s"
			"normalize correlation function  %%b");
	i=win_scanf(s, &path_name, &theta_root_filename, &indices_root_filename, &files_ind_string, &file_extension, &bool_normalize);
	if (i==CANCEL) return(OFF);
	
	i=win_scanf("sampling frequency ? %8f\n\n", &fec);
	if (i==CANCEL) return(OFF);
	
	files_index = str_to_index(files_ind_string, &n_files);	// malloc is done by str_to_index
	if ( (files_index==NULL) || (n_files<1) )	return(win_printf_OK("bad values for files indices !"));
	

	i=win_scanf("correlation function is <n(t_i)n(t_i+\\tau)>\n\n"
			"t_i (integer, nb of pts) ? %s\n\n"
			"average over %d points around t_i",
			&ti_string, &n_average);
	if (i==CANCEL) return OFF;
	
	ti_index = str_to_index(ti_string, &n_ti);	// malloc is done by str_to_index
	if ( (ti_index==NULL) || (n_ti<1) )	return(win_printf_OK("bad values for \\ti !"));

		
//Correlation function
	for (k=0; k<n_files; k++)
	{
		i_file=files_index[k];
		
		sprintf(filename, "%s%s%d%s", path_name, theta_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_theta=load_data_bin(filename, &theta);
		if (n_theta==-1) // then error while opening the file
			{  return(win_printf_OK("error with file\n%s\ncreated with this information:\n%s\n%s\n%d\n%s", 
           		       filename, path_name, theta_root_filename, i_file, file_extension));
   			     }
		sprintf(filename, "%s%s%d%s", path_name, indices_root_filename, i_file, file_extension);
		sprintf(s, "%d/%d file %s: loading data", k+1, n_files, filename); spit(s);
		n_excitation=load_data_bin(filename, &excitation);
		
		if (n_excitation!=n_theta) return(win_printf_OK("M has %d points but \\theta has %d points", n_excitation, n_theta));
		
		
		sprintf(s, "%d/%d searching indices", k+1, n_files); spit(s);
		
	

		if (k==0) 
		{	
		 	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op1) != 2)				return win_printf_OK("cannot plot region");
			if ((op2 = create_and_attach_one_plot(pr, n_theta, n_theta, 0)) == NULL)	return win_printf_OK("cannot create plot !");
			ds2 = op2->dat[0];
		}
		for (i=0; i<n_ti; i++)
		{	ti=ti_index[i];
			if (k==0) // first file : we create the datasets
			{	if (i!=0) // not the first ti: we add a dataset
				{	if ((ds2 = create_and_attach_one_ds(op2, ti, ti, 0)) == NULL)	
					return win_printf_OK("cannot create dataset !");
				}
			}
			else 
			{	ds2=op2->dat[i];
			}
			
			 
			for (i1=0; i1<ti; i1++) // i1 = ti + tau
			{	//for (i3 = 0; i3<n_average ;i3++)
					//{ds2->yd[i1] +=(float)(theta[ti+i3-n_average])*(excitation[i1+i3-n_average]);
					//}
					ds2->yd[i1] +=(float)(theta[ti])*(excitation[i1]);
			}
		}
	
				free(excitation);
		free(theta);
		
	} //end of the loop over k, ie, loop over the files;
	free(files_index);
	for (i=0; i<n_ti; i++)
	{
	ti=ti_index[i];
	ds2=op2->dat[i];
	for (i1=0; i1<ti; i1++)
	{
	ds2->xd[i1] = (float)i1/fec;
	ds2->yd[i1] = (float)ds2->yd[i1]/n_files/n_average;
	}
	if (bool_normalize==1)
	{
	calib=(float)ds2->yd[0];
 	for (i1=0; i1<ti; i1++)
 	{
	ds2->yd[i1] = (float)ds2->yd[i1]/calib;
 	}
	}
	ds2->treatement = my_sprintf(ds2->treatement,"correlation, starting at time ti=%d", ti);
 	}
	free(average);
	free(ti_index);
	
	set_plot_title(op2,"averaged signal");
	set_plot_x_title(op2, "time (s)");
	set_plot_y_title(op2, "signal");
	op2->filename = Transfer_filename(op1->filename);
    	op2->dir = Mystrdup(op1->dir);

	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_correl_from files' */
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//fin de moyenne sur des cycles//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Function which plots psd
int do_plot_psd_from_ds(void)
{
register int 	i, i2, j, l;
	O_p 	*op2, *op1 = NULL;
static	int	nf=1024+1, ny, n_cycles;
	d_s 	*ds2, *ds1, *ds_ind;
	pltreg	*pr = NULL;
	int	index;
	char s[512];
	double  dt;
	int	*ind; // indices
static int n_fft=1600, n_overlap=0, bool_hanning=0;
static float f_acq=8192;
	float *psd, *psd_tmp;

	if(updating_menu_state != 0)	return D_O_K;	
	index = active_menu->flags & 0xFFFFFF00;

	/* display routine action if SHIFT is pressed */
	if (key[KEY_LSHIFT])
	{	return win_printf_OK("This routine builds a psd \n"
				"out of the current dataset starting at some points of dataset, and place it in a new plot\n\n"
				"(indices are assumed to be in the next ds, after the current one)");
	}
	
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op1,&ds1) != 3)		return win_printf_OK("cannot plot region");
	ny=ds1->nx;
	if (op1->n_dat<=1)	return(win_printf_OK("I need 2 datasets!\n"
						"active one for the data,\nnext one for indices"));
	ds_ind = (op1->cur_dat == op1->n_dat-1 ) ? op1->dat[0] : op1->dat[op1->cur_dat + 1];
	n_cycles = ds_ind->nx;
	if (n_cycles>=ny) 	return(win_printf_OK("You probably exchanged the 2 datasets ;-)"));

	dt = (double)(ds1->xd[ny-1]-ds1->xd[0])/(ny-1);
//	win_printf("dt is \n%g\n%g\n%g", dt, ds1->xd[2]-ds1->xd[1], ds1->xd[1]);
	if ( (ds1->xd[2]-ds1->xd[1])!=(dt) ) 	return(win_printf_OK("dt is not constant ?!"));
	if (dt==0)				return(win_printf_OK("dt is zero !"));
	ind = (int*)calloc(n_cycles, sizeof(int));
	for (i=0; i<n_cycles; i++)	ind[i] = (int)rintf((ds_ind->xd[i] - ds1->xd[0])/dt);
	

	sprintf(s, "dataset has %d points.\n"
			"number of points of fft window ?    %%6d\n"
			"overlap (in points) between windows ? %%6d\n"
			"acquisition frequency (in Hz, for normalization) ?%%9f\n"
			"%%b use Hanning window ?",ny);
	if (win_scanf(s, &n_fft, &n_overlap, &f_acq, &bool_hanning) == CANCEL)  return D_O_K;
	
	if (n_overlap>=n_fft) 
			return win_printf("overlap is too large compared to n\\_fft (%d>=%d)",
				n_overlap, n_fft);

	if ((op2=create_and_attach_one_plot(pr, (n_fft/2)+1, (n_fft/2)+1, 0)) == NULL)
			return win_printf_OK("cannot create PSD plot !");
    if ((ds2=op2->dat[0]) == NULL)
    		return win_printf_OK("Cannot allocate PSD dataset");
	
	if ((ds2 = create_and_attach_one_ds(op2, nf, nf, 0)) == NULL)	
			return win_printf_OK("cannot create dataset !");

			
	psd=(float*)calloc(n_fft,sizeof(float));
	psd_tmp=(float*)calloc((n_fft/2)+1,sizeof(float));
	for (i2=0; i2<n_cycles; i2++)
		{	j=ind[i2];
			for (l=0;l<n_fft; l++) psd[l]=ds1->yd[l+j];
			i=compute_psd_of_float_data( psd, ds2->yd, ds2->xd, f_acq, n_fft, n_fft, n_overlap, bool_hanning);
			for (l=0;l<(n_fft/2)+1; l++) psd_tmp[l]+=ds2->yd[l];
		}	
			
        		
   	for (j=0; j<(n_fft/2)+1; j++) ds2->yd[j]=(float)sqrt(psd_tmp[j]/n_cycles);
   		inherit_from_ds_to_ds(ds2, ds1);
   		ds2->treatement = my_sprintf(ds2->treatement,"PSD of displacement (rad/\\sqrt{Hz})");

	
		op2->iopt |= XLOG;
    	op2->iopt |= YLOG;
	
	free(psd);
	free(psd_tmp);
    
	return refresh_plot(pr, UNCHANGED);
} /* end of function 'do_plot_psd_from_ds' */










void spit(char *message)
{	size_t	l_max=55;
	char 	white_string[64]="                                              ";
	
	if (strlen(message)<l_max) 	strncat (message, white_string, l_max-strlen(message));
	textout_ex(screen, font, message, 40, 40, makecol(100,155,155), makecol(2,1,1));
	return;
}


MENU *cycles_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"Find begining of lower plateau", 	do_find_boundaries,			NULL, FIND_LOWER|FIND_BEGIN, NULL);	
	add_item_to_menu(mn,"Find ending of lower plateau",		do_find_boundaries,			NULL, FIND_LOWER|FIND_END, NULL);	
	add_item_to_menu(mn,"Find begining of higher plateau",	do_find_boundaries,			NULL, FIND_UPPER|FIND_BEGIN, NULL);	
	add_item_to_menu(mn,"Find ending of higher plateau",	do_find_boundaries,			NULL, FIND_UPPER|FIND_END, NULL);
	add_item_to_menu(mn,"Find something else",				do_find_boundaries, 		NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"Select minim. interval length",	do_find_set_interval_length, 		NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"Average over cycles",				do_average_over_cycles, 				NULL, 0, NULL);
	add_item_to_menu(mn,"Correlation function",				do_cycles_correl_function, 				NULL, 0, NULL);
	add_item_to_menu(mn,"Histograms for averaged data",		do_hist_periodic_build_from_ds,			NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"correct offset in files",          do_correct_offset_in_files,       		NULL, 0, NULL);
	add_item_to_menu(mn,"correct contrast in files",        do_correct_contrast_in_files,       		NULL, 0, NULL);
	add_item_to_menu(mn,"compute fluctuation in files",     do_compute_fluctuations_in_files,       		NULL, 0, NULL);
	add_item_to_menu(mn,"filt bin files",				do_filter_bin_files,       		NULL, 0, NULL);
	add_item_to_menu(mn,"deriv bin files by spline",		do_deriv_bin_files,       		NULL, 0, NULL);
	add_item_to_menu(mn,"compute integrated response",		do_compute_integrated_response,       		NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"Average over cycles from binary files",		do_average_over_cycles_from_files,		NULL, 0, NULL);
	add_item_to_menu(mn,"Correlation over cycles from binary files",		do_correl_over_cycles_from_files,		NULL, 0, NULL);
	add_item_to_menu(mn,"Correlation from binary files",		do_correl_from_files,		NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"histograms from binary files",		do_hist_periodic_build_from_files,		NULL, 0, NULL);
	add_item_to_menu(mn,"histograms from binary M&dep cycles",	do_hist_full_cycles_from_M_dep_files,NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"moments from binary M&dep files",	do_stats_periodic_build_from_M_dep_files,NULL, 0, NULL);
	add_item_to_menu(mn,"\0",						0,		NULL, 0, NULL);	
	add_item_to_menu(mn,"PSD of cycles",					do_plot_psd_from_ds,						NULL, 0, NULL);	
	return(mn);
}

int	cycles_main(int argc, char **argv)
{
	add_plot_treat_menu_item ("cycles", NULL, cycles_plot_menu(), 0, NULL);
	return D_O_K;
}


int	cycles_unload(int argc, char **argv)
{ 
	remove_item_to_menu(plot_treat_menu,"cycles",	NULL, NULL);
	return D_O_K;
}
#endif
