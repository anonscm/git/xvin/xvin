/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _MODT_C_
#define _MODT_C_

# include "allegro.h"
# include "xvin.h"
# include "fftl32n.h"

/* If you include other plug-ins header do it here*/ 
float interpolate_point_by_poly_4_in_array(float *x, float *y, int nx, float xp);

/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "modT.h"



int IO_write(unsigned char canal, int min);
int IO_read(unsigned char canal_read);


int do_prepare_signal(void)
{
  register int i, j;
  O_p *op = NULL, *opn = NULL;
  static int n_dss;
  static int nf = 32768;
  float x, y, minx, maxx;
  float rei, res, imi, ims, ampi, amps, tmpi, tmps;
  d_s *dsi, *dsr, *dss, *ds, *dsp;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine compute prepare  response"
			   "of a system by comparing two data sets, one being the input signal \n"
			   "the other the response");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  n_dss = op->cur_dat;


  i = win_scanf("This routine assume a transient responte to an heaviside input\n"
		"and transform it in a square signal response with equally spaced points\n"
		"Select the number of interpolated points in half a period (power of 2) %5d\n"
		,&nf);
  if (i == CANCEL)	return OFF;

  if (fft_init(nf))	return win_printf_OK("cannot init fft!not a power of 2!");	

  if ((dss = create_and_attach_one_ds(op, 2*nf, 2*nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  if ((dsr = create_and_attach_one_ds(op, 2*nf, 2*nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  minx = 0;
  maxx = dsi->xd[dsi->nx-1];
  for (i = 0; i < nf; i++) 
    {
      x = minx + ((maxx-minx)*i)/(nf);	
      y = interpolate_point_by_poly_4_in_array(dsi->xd, dsi->yd, dsi->nx, x);
      dss->xd[i] = dsr->xd[i] = x;
      dss->yd[i] = y;
      dsr->yd[i] = 1;
      dss->xd[nf+i] = dsr->xd[nf+i] = maxx + x;
      dss->yd[nf+i] = dsi->yd[dsi->nx-1] - y;
      dsr->yd[nf+i] = 0;
    }

  inherit_from_ds_to_ds(dss, dsi);
  inherit_from_ds_to_ds(dsr, dsi);
  refresh_plot(pr, UNCHANGED);

  nf *= 2;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  if ((dsp = create_and_attach_one_ds(opn, nf/2, nf/2, 0)) == NULL)
    return win_printf_OK("cannot create plot !");


  for (j = 0; j < nf; j++)
    {
      ds->yd[j] = dsr->yd[j];
      ds->xd[j] = dss->yd[j];
    }

  realtr1(nf, ds->yd);
  fft(nf, ds->yd, 1);
  realtr2(nf, ds->yd, 1);
  realtr1(nf, ds->xd);
  fft(nf, ds->xd, 1);
  realtr2(nf, ds->xd, 1);


  for (j = 1, tmpi = ds->yd[1], tmps = ds->xd[1]; j < nf/2; j+=2)
    {
      rei = ds->yd[2*j];
      res = ds->xd[2*j];
      imi = ds->yd[(2*j)+1];
      ims = ds->xd[(2*j)+1];
      ampi = sqrt(rei * rei + imi * imi);
      amps = sqrt(res * res + ims * ims);
      ds->xd[j/2] = dsp->xd[j/2] = j;
      ds->yd[j/2] = (ampi != 0) ? amps/ampi : amps;
      dsp->yd[j/2] = atan2((ims * rei) - (imi * res), (rei * res) + (imi * ims));
    }
  //ds->yd[nf/4] = (tmpi != 0) ? tmps/tmpi : tmps;
  //ds->xd[nf/4] = nf/2;

  ds->nx = ds->ny = nf/4;
  dsp->nx = dsp->ny = nf/4;

  create_attach_select_x_un_to_op(opn, 0, 0, (float)1/(2*maxx), 0, 0, "Hz");


  inherit_from_ds_to_ds(ds, dsi);
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
  opn->filename = Transfer_filename(op->filename);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}





int do_real_time_analysis(void)
{
  register int i, j;
  O_p *op = NULL, *opn = NULL;
  static int n_dss, nfi, low, high;
  static int nf = 32768, canal_read = 1, canal_write = 1;
  static float dt = 1, fre = 4000;
  float x, y, minx, maxx;
  float rei, res, imi, ims, ampi, amps, tmpi, tmps;
  d_s *dsi, *dsr, *dss, *ds, *dsp;
  pltreg *pr = NULL;
  static unsigned long t0, t1, tt, per_sec = 0;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine generate a square wave signal and analyze the response"
			   "of a system by comparing two data sets, one being the input signal \n"
			   "the other the response");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)     return win_printf_OK("cannot find data");

  if (per_sec == 0)     per_sec = MY_UCLOCKS_PER_SEC;

  win_printf("per sec = %d",(int)per_sec);

  i = win_scanf("This routine generates an heaviside input signal having :\n"
		"low value (dac unit) %6d high value (dac unit) %6d\n"
		" and a period (in sec) %6f\n sampling freq %6f\n"
		"canal read %3d canal write %3d\n"
		" and transform it in a square \nsignal response with equally spaced points\n"
		"Select the number of interpolated points in half a period (power of 2) %5d\n"
		,&low,&high,&dt,&fre,&nf);
  if (i == CANCEL)	return OFF;


  n_dss = op->cur_dat;

  if (fft_init(nf))	return win_printf_OK("cannot init fft!not a power of 2!");	

  nfi = (int)(0.5+dt*fre);

  if ((op = create_and_attach_one_plot(pr, 2*nfi, 2*nfi, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  dsi = opn->dat[0];


  if ((dss = create_and_attach_one_ds(op, 2*nf, 2*nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");

  if ((dsr = create_and_attach_one_ds(op, 2*nf, 2*nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");




  for (i = 0, tt = t1 = t0 = my_uclock(); i < nfi; i++)
    {
      dsi->xd[i] = (float)(((double)(tt-t0))/per_sec);
      t1 = t0 + (i*per_sec)/nfi;
      //if (2*i < nfi) IO_write((unsigned char)canal_write, low);
      //else IO_write((unsigned char)canal_write, high);
      //dsi->yd[i] = 10./65536.*(double)IO_read((unsigned char) canal_read);
      for ( ; (tt = my_uclock()) < t1; ) ;	   
    }




  minx = dt/2;
  maxx = dt;
  for (i = 0; i < nf; i++) 
    {
      x = minx + ((maxx-minx)*i)/(nf);	
      y = interpolate_point_by_poly_4_in_array(dsi->xd+nfi/2, dsi->yd+nfi/2, nfi/2, x);
      dss->xd[i] = dsr->xd[i] = x;
      dss->yd[i] = y;
      dsr->yd[i] = 1;
      dss->xd[nf+i] = dsr->xd[nf+i] = maxx + x;
      dss->yd[nf+i] = dsi->yd[dsi->nx-1] - y;
      dsr->yd[nf+i] = 0;
    }

  inherit_from_ds_to_ds(dss, dsi);
  inherit_from_ds_to_ds(dsr, dsi);
  refresh_plot(pr, UNCHANGED);

  nf *= 2;

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  if ((dsp = create_and_attach_one_ds(opn, nf/2, nf/2, 0)) == NULL)
    return win_printf_OK("cannot create plot !");


  for (j = 0; j < nf; j++)
    {
      ds->yd[j] = dsr->yd[j];
      ds->xd[j] = dss->yd[j];
    }

  realtr1(nf, ds->yd);
  fft(nf, ds->yd, 1);
  realtr2(nf, ds->yd, 1);
  realtr1(nf, ds->xd);
  fft(nf, ds->xd, 1);
  realtr2(nf, ds->xd, 1);


  for (j = 1, tmpi = ds->yd[1], tmps = ds->xd[1]; j < nf/2; j+=2)
    {
      rei = ds->yd[2*j];
      res = ds->xd[2*j];
      imi = ds->yd[(2*j)+1];
      ims = ds->xd[(2*j)+1];
      ampi = sqrt(rei * rei + imi * imi);
      amps = sqrt(res * res + ims * ims);
      ds->xd[j/2] = dsp->xd[j/2] = j;
      ds->yd[j/2] = (ampi != 0) ? amps/ampi : amps;
      dsp->yd[j/2] = atan2((ims * rei) - (imi * res), (rei * res) + (imi * ims));
    }
  //ds->yd[nf/4] = (tmpi != 0) ? tmps/tmpi : tmps;
  //ds->xd[nf/4] = nf/2;

  ds->nx = ds->ny = nf/4;
  dsp->nx = dsp->ny = nf/4;

  create_attach_select_x_un_to_op(opn, 0, 0, (float)1/(2*maxx), 0, 0, "Hz");


  inherit_from_ds_to_ds(ds, dsi);
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
  opn->filename = Transfer_filename(op->filename);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}


int do_modT_response(void)
{
  register int i, j;
  O_p *op = NULL, *opn = NULL;
  static int n_dsi, n_dss;
  int nf;
  float rei, res, imi, ims, ampi, amps, tmpi, tmps;
  static float factor = 1;
  d_s *ds, *dsi, *dss, *dsp;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine compute the spectral response"
			   "of a system by comparing two data sets, one being the input signal \n"
			   "the other the response");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dss) != 3)
    return win_printf_OK("cannot find data");

  n_dsi = n_dss = op->cur_dat;
  n_dsi++;
  n_dsi %= op->n_dat;


  i = win_scanf("This routine compute the response spectrum of a filter\n"
		"using an input signal and its response one\n"
		"Select the data set number of the imput signal %5d\n"
		"of the response signal %5d\n",&n_dsi, &n_dss);
  if (i == CANCEL)	return OFF;

  if (n_dsi < 0 || n_dsi >= op->n_dat)
    return win_printf_OK("data set index out of range for input!");

  if (n_dss < 0 || n_dss >= op->n_dat)
    return win_printf_OK("data set index out of range for signal!");


  dsi = op->dat[n_dsi];
  dss = op->dat[n_dss];

  if (dsi->nx != dss->nx || dsi->ny != dss->ny)
    return win_printf_OK("Signal must have equal size!");

  nf = dsi->nx;	/* this is the number of points in the data set */

  if (fft_init(nf))	return win_printf_OK("cannot init fft!not a power of 2!");	

  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  ds = opn->dat[0];

  if ((dsp = create_and_attach_one_ds(opn, nf/2, nf/2, 0)) == NULL)
    return win_printf_OK("cannot create plot !");


  for (j = 0; j < nf; j++)
    {
      ds->yd[j] = dsi->yd[j];
      ds->xd[j] = dss->yd[j];
    }

  realtr1(nf, ds->yd);
  fft(nf, ds->yd, 1);
  realtr2(nf, ds->yd, 1);
  realtr1(nf, ds->xd);
  fft(nf, ds->xd, 1);
  realtr2(nf, ds->xd, 1);


  for (j = 1, tmpi = ds->yd[1], tmps = ds->xd[1], ds->yd[0] = ds->xd[0] = 0; j < nf/2; j++)
    {
      rei = ds->yd[2*j];
      res = ds->xd[2*j];
      imi = ds->yd[(2*j)+1];
      ims = ds->xd[(2*j)+1];
      ampi = sqrt(rei * rei + imi * imi);
      amps = sqrt(res * res + ims * ims);
      ds->xd[j] = dsp->xd[j] = j;
      ds->yd[j] = (ampi != 0) ? amps/ampi : amps;
      dsp->yd[j] = atan2((ims * rei) - (imi * res), (rei * res) + (imi * ims));
    }
  ds->yd[nf/2] = (tmpi != 0) ? tmps/tmpi : tmps;
  ds->xd[nf/2] = nf/2;
  ds->nx = ds->ny = nf/2 + 1;
  /* now we must do some house keeping */
  inherit_from_ds_to_ds(ds, dsi);
  ds->treatement = my_sprintf(ds->treatement,"y multiplied by %f",factor);
  set_plot_title(opn, "Multiply by %f",factor);
  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);
  opn->filename = Transfer_filename(op->filename);
  uns_op_2_op(opn, op);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

MENU *modT_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	add_item_to_menu(mn,"francesco",do_real_time_analysis,NULL,0,NULL);
	add_item_to_menu(mn,"prepare signal",do_prepare_signal,NULL,0,NULL);
	add_item_to_menu(mn,"response_fct", do_modT_response,NULL,0,NULL);

	return mn;
}

int	modT_main(int argc, char **argv)
{
	add_plot_treat_menu_item ( "modT", NULL, modT_plot_menu(), 0, NULL);
	return D_O_K;
}

int	modT_unload(int argc, char **argv)
{
	remove_item_to_menu(plot_treat_menu, "modT", NULL, NULL);
	return D_O_K;
}
#endif

