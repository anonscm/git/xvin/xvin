#ifndef _MODT_H_
#define _MODT_H_

PXV_FUNC(int, do_modT_rescale_plot, (void));
PXV_FUNC(MENU*, modT_plot_menu, (void));
PXV_FUNC(int, do_modT_rescale_data_set, (void));
PXV_FUNC(int, modT_main, (int argc, char **argv));
#endif

