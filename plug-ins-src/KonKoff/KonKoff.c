/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _KONKOFF_C_
#define _KONKOFF_C_

# include "allegro.h"
# include "xvin.h"
# include "float.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled


#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
//place here other headers of this plugin 
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "KonKoff.h"


int compute_best_aF_and_xi2_kon_koff(int a_is_1, d_s *dst, float Oligo_T, d_s *dsc, float Topen_C, float kon, float koff, float *aF, double *E)
{
  double xi2 = 0;
  double fy, f, y, fx, er, la = 0;
  int i, j;


  if (a_is_1 == 0)
    {
      for (i = 0, f = fy = 0; i < dst->nx; i++)
	{
	  y = dst->yd[i];
	  er = dst->ye[i];
	  //"P_{block} = a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]\n"
	  fx = (double)(kon* Oligo_T)/((kon* Oligo_T) + koff);
	  fx *= (1.0 - exp(-((kon* Oligo_T) + koff)*dst->xd[i]));
	  if (er > 0)
	    {
	      fy += y*fx/(er*er);
	      f += (fx*fx)/(er*er);
	    }
	}
      for (i = 0; i < dsc->nx; i++)
	{
	  y = dsc->yd[i];
	  er = dsc->ye[i];
	  //"P_{block} = a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]\n"
	  fx = (double)(kon* dsc->xd[i])/((kon* dsc->xd[i]) + koff);
	  fx *= (1.0 - exp(-((kon* dsc->xd[i] + koff)*Topen_C)));
	  if (er > 0)
	    {
	      fy += y*fx/(er*er);
	      f += (fx*fx)/(er*er);
	    }
	}
      if (f != 0)    la = fy/f;
      else return -3;
      if (la < 0.000001) win_printf("Low la = %g f = %g fy = %g",la,f,fy); 
    }
  else la = 1;
  la = (la > 1.0) ? 1.0 : la; 
  if (aF) *aF = la;
  for (i = 0, j = 0, xi2 = 0; i < dst->nx; i++)
    {
      y = dst->yd[i];
      er = dst->ye[i];
      fx = (double)la*(kon* Oligo_T)/((kon* Oligo_T) + koff);
      fx *= (1.0 - exp(-((kon* Oligo_T) + koff)*dst->xd[i]));
      f = y - fx;
      if (er > 0)
	{
	  xi2 += (f*f)/(er*er);
	  j++;
	}
    }	
  for (i = 0; i < dsc->nx; i++)
    {
      y = dsc->yd[i];
      er = dsc->ye[i];
      //"P_{block} = a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]\n"
      fx = (double)la*(kon* dsc->xd[i])/((kon* dsc->xd[i]) + koff);
      fx *= (1.0 - exp(-((kon* dsc->xd[i] + koff)*Topen_C)));
      f = y - fx;
      if (er > 0)
	{
	  xi2 += (f*f)/(er*er);
	  j++;
	}
    }
  if (E) *E = xi2;
  return j;
} 


int do_KonKoff_rescale_data_set(void)
{
  register int i;
  O_p *opt = NULL, *opc = NULL;
  int nf = 1024, np;
  d_s *dst, *dstf = NULL, *dsc, *dscf = NULL;
  pltreg *pr = NULL;
  static int a_is_1 = 0, is_P_vs_C = 0, n_plot_2 = 0, n_ds_1 = -1, n_ds_2 = 0, opim_off = 0, n_kon = 256, n_koff = 256;
  static float Oligo_T = 20, Topen_C = 10;
  static float kon = 0.003, konmin = 0.001, konmax = 0.009;
  static float koff = 0.01, koffmin = 0.001, koffmax = 0.1;
  float aF = 1.0, best_aF, best_kon, best_koff, maxx, fx;
  double E, E1;
  char st[2048];

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opt,&dst) != 3)
    return win_printf_OK("cannot find data");
  if (n_ds_1 < 0) n_ds_1 = opt->cur_dat;
  i = win_scanf("The dataset %8d of the present plot displays:\n"
		"%R P_{block} vs T_{open} or %r P_{block} vs [Oligo]\n"
		"Specify the plot number of the second ploa %8d and its ds# %8d\n"
		"I am going to fit :\n\n"
		"P_{block} = a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]\n\n"
		"Impose a(F) = 1 %b\n"
		"Optimize versus %R k_{on} or %r k_{off}\n"
		"For P_{block} vs T_{open} specify [oligo] = %8f\n"
		"For P_{block} vs [Oligo] specify T_{open} = %8f\n"
		"Range k_{on} = %8f min %8f max %8f #increments %8d\n"
		"Range k_{off} = %8f min %8f max %8f #increments %8d\n"
		,&n_ds_1,&is_P_vs_C,&n_plot_2,&n_ds_2,&a_is_1,&opim_off,&Oligo_T,&Topen_C,&kon, &konmin, &konmax,&n_kon
		,&koff, &koffmin, &koffmax,&n_koff);
  if (i == CANCEL)	return OFF;
  if (n_plot_2 < 0 || n_plot_2 >= pr->n_op)  
        return win_printf_OK("Plot 2 out of range");
  if (n_ds_1 >= 0 && n_ds_1 < opt->n_dat) dst = opt->dat[n_ds_1];
  if (is_P_vs_C) 
    {
      opc = opt;
      dsc = dst;
      opt = pr->o_p[n_plot_2];
      if (n_ds_2 < 0 || n_ds_2 >= opt->n_dat)  
	return win_printf_OK("ds 2 out of range"); 
      dst = opt->dat[n_ds_2];
    }
  else
    {
      opc = pr->o_p[n_plot_2];
      if (n_ds_2 < 0 || n_ds_2 >= opc->n_dat)  
	return win_printf_OK("ds 2 out of range"); 
      dsc = opc->dat[n_ds_2];
    }
  if (dsc->ye == NULL)
    return win_printf_OK("No error bar found for P_{block} vs [Oligo]"); 
  if (dst->ye == NULL)
    return win_printf_OK("No error bar found for P_{block} vs T_{open}"); 

  dstf = find_source_specific_ds_in_op(opt,"Fit P_{block} vs T_{open}");
  if (dstf == NULL)
    {
      if ((dstf = create_and_attach_one_ds(opt, nf, nf, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      set_ds_source(dstf, "Fit P_{block} vs T_{open}, a(F) = %g; k_{on} = %g; k_{off} = %g",aF,kon,koff);

    }
  dscf = find_source_specific_ds_in_op(opc,"Fit P_{block} vs [Oligo]");
  if (dscf == NULL)
    {
      if ((dscf = create_and_attach_one_ds(opc, nf, nf, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      set_ds_source(dscf, "Fit P_{block} vs [Oligo], a(F) = %g; k_{on} = %g; k_{off} = %g",aF,kon,koff);
    }

  i = compute_best_aF_and_xi2_kon_koff(a_is_1, dst, Oligo_T, dsc, Topen_C, kon, koff, &aF, &E);

  best_koff = koff;
  best_aF = aF;
  best_kon = kon;

  if (opim_off)
    {
      for (koff = koffmin; koff <= koffmax; koff += (koffmax - koffmin)/n_koff)
	{
	  np = compute_best_aF_and_xi2_kon_koff(a_is_1, dst, Oligo_T, dsc, Topen_C, kon, koff, &aF, &E1);
	  if (np < 0) win_printf("Pb in fit");
	  if (E1 < E && np > 0)
	    {
	      best_koff = koff;
	      best_aF = aF;
	      best_kon = kon;
	      E = E1;
	    }
	}   
    }
  else
    {
      for (kon = konmin; kon <= konmax; kon += (konmax - konmin)/n_kon)
	{
	  np = compute_best_aF_and_xi2_kon_koff(a_is_1, dst, Oligo_T, dsc, Topen_C, kon, koff, &aF, &E1);
	  if (np < 0) win_printf("Pb in fit");
	  if (E1 < E && np > 0)
	    {
	      best_koff = koff;
	      best_aF = aF;
	      best_kon = kon;
	      E = E1;
	    }
	}   
    }
  koff = best_koff; 
  aF = best_aF; 
  kon = best_kon; 

  for (i = 0, maxx = dst->xd[0]; i < dst->nx; i++) 
    maxx = (maxx < dst->xd[i]) ? dst->xd[i] : maxx;
  maxx *= 1.1;
  
  for (i = 0; i < dstf->nx; i++)
    {
      dstf->xd[i] = (maxx *i)/dstf->nx;
      fx = (double)aF*(kon* Oligo_T)/((kon* Oligo_T) + koff);
      fx *= (1.0 - exp(-((kon* Oligo_T) + koff)*dstf->xd[i]));
      dstf->yd[i] = fx;
      opt->need_to_refresh = 1;
    }	
  set_ds_source(dstf, "Fit P_{block} vs T_{open}, a(F) = %g; k_{on} = %g; k_{off} = %g; \\xi^2 = %g",aF,kon,koff,E);
  remove_all_plot_label_from_ds(dstf);
  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl P_{block} =}{a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]}"
	  "{a(F) =  %g}{k_{on} = %g nM^{-1}s^{-1}}{k_{off}  = %gs^{-1}}{Kd = %g nM;T_{off} = %g s}"
	  "{\\chi^2 = %g, n = %d}}}}"
	  ,aF,kon,koff,((kon>0)?koff/kon:0),((koff > 0)?(1/koff):0),E,np-3);
  set_ds_plot_label(dstf, opt->x_lo + (opt->x_hi - opt->x_lo)/4, 
		    opt->y_hi - (opt->y_hi - opt->y_lo)/4, USR_COORD,st);	 

  for (i = 0, maxx = dsc->xd[0]; i < dsc->nx; i++) 
    maxx = (maxx < dsc->xd[i]) ? dsc->xd[i] : maxx;
  maxx *= 1.1;

  for (i = 0; i < dscf->nx; i++)
    {
      dscf->xd[i] = (maxx *i)/dscf->nx;
      //"P_{block} = a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]\n"
      fx = (double)aF*(kon* dscf->xd[i])/((kon* dscf->xd[i]) + koff);
      fx *= (1.0 - exp(-((kon* dscf->xd[i] + koff)*Topen_C)));
      dscf->yd[i] = fx;
      opc->need_to_refresh = 1;
    }
  set_ds_source(dscf, "Fit P_{block} vs [Oligo], a(F) = %g; k_{on} = %g; k_{off} = %g; \\xi^2 = %g",aF,kon,koff,E);
  remove_all_plot_label_from_ds(dscf);
  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl P_{block} =}{a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]}"
	  "{a(F) =  %g}{k_{on} = %g nM^{-1}s^{-1}}{k_{off}  = %gs^{-1}}{Kd = %g nM;T_{off} = %g s}"
	  "{\\chi^2 = %g, n = %d}}}}"
	  ,aF,kon,koff,((kon>0)?koff/kon:0),((koff > 0)?(1/koff):0),E,np-3);
  set_ds_plot_label(dscf, opt->x_lo + (opt->x_hi - opt->x_lo)/4, 
		    opt->y_hi - (opt->y_hi - opt->y_lo)/4, USR_COORD,st);	 

  refresh_plot(pr, UNCHANGED);
  return D_O_K;

}


int compute_best_aF_and_xi2_kon_kd(int a_is_1, d_s *dst, float Oligo_T, d_s *dsc, float Topen_C, float kon, float kd, float *aF, double *E)
{
  double xi2 = 0;
  double fy, f, y, fx, er, la = 0;
  int i, j;


  if (a_is_1 == 0)
    {
      for (i = 0, f = fy = 0; i < dst->nx; i++)
	{
	  y = dst->yd[i];
	  er = dst->ye[i];
	  fx = (double)Oligo_T/(Oligo_T + kd);
	  fx *= (1.0 - exp(-(kon* (Oligo_T+ kd)*dst->xd[i])));
	  if (er > 0)
	    {
	      fy += y*fx/(er*er);
	      f += (fx*fx)/(er*er);
	    }
	}
      for (i = 0; i < dsc->nx; i++)
	{
	  y = dsc->yd[i];
	  er = dsc->ye[i];
	  fx = (double)dsc->xd[i]/(dsc->xd[i] + kd);
	  fx *= (1.0 - exp(-(kon* (dsc->xd[i]+ kd)*Topen_C)));
	  if (er > 0)
	    {
	      fy += y*fx/(er*er);
	      f += (fx*fx)/(er*er);
	    }
	}
      if (f != 0)    la = fy/f;
      else return -3;
      if (la < 0.000001) win_printf("Low la = %g f = %g fy = %g",la,f,fy); 
    }
  else la = 1;
  la = (la > 1.0) ? 1.0 : la; 
  if (aF) *aF = la;
  for (i = 0, j = 0, xi2 = 0; i < dst->nx; i++)
    {
      y = dst->yd[i];
      er = dst->ye[i];
      fx = (double)la*Oligo_T/(Oligo_T + kd);
      fx *= (1.0 - exp(-(kon* (Oligo_T+ kd)*dst->xd[i])));
      f = y - fx;
      if (er > 0)
	{
	  xi2 += (f*f)/(er*er);
	  j++;
	}
    }	
  for (i = 0; i < dsc->nx; i++)
    {
      y = dsc->yd[i];
      er = dsc->ye[i];
      fx = (double)la*dsc->xd[i]/(dsc->xd[i] + kd);
      fx *= (1.0 - exp(-(kon* (dsc->xd[i]+ kd)*Topen_C)));
      f = y - fx;
      if (er > 0)
	{
	  xi2 += (f*f)/(er*er);
	  j++;
	}
    }
  if (E) *E = xi2;
  return j;
} 



int do_KonKoff_Kd_kon(void)
{
  register int i;
  O_p *opt = NULL, *opc = NULL;
  int nf = 1024, np;
  d_s *dst, *dstf = NULL, *dsc, *dscf = NULL;
  pltreg *pr = NULL;
  static int a_is_1 = 0, is_P_vs_C = 0, n_plot_2 = 0, n_ds_1 = -1, n_ds_2 = 0, opim_off = 0, n_kon = 256, n_kd = 256; // , n_koff = 256
  static float Oligo_T = 20; // in nM
  static float Topen_C = 10; // in s
  static float kon = 0.003, konmin = 0.001, konmax = 0.009;
  static float koff = 0.01; //, koffmin = 0.001, koffmax = 0.1;
  static float kd = 0.01, kdmin = 0.001, kdmax = 0.1;
  float aF = 1.0, best_aF, best_kon, best_kd, maxx, fx;
  double E, E1;
  char st[2048];

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opt,&dst) != 3)
    return win_printf_OK("cannot find data");
  if (n_ds_1 < 0) n_ds_1 = opt->cur_dat;
  i = win_scanf("The dataset %8d of the present plot displays:\n"
		"%R P_{block} vs T_{open} or %r P_{block} vs [Oligo]\n"
		"Specify the plot number of the second ploa %8d and its ds# %8d\n"
		"I am going to fit :\n\n"
		"P_{block} = a(F)\\frac{[O]}{[O]+K_d}[1 -e^{-(k_{on}.([O]+Kd)).T_{open}}]\n\n"
		"Impose a(F) = 1 %b\n"
		"Optimize versus %R k_{on} or %r Kd\n"
		"For P_{block} vs T_{open} specify [oligo] = %8f\n"
		"For P_{block} vs [Oligo] specify T_{open} = %8f\n"
		"Range k_{on} = %8f min %8f max %8f #increments %8d\n"
		"Range K_{d} = %8f min %8f max %8f #increments %8d\n"
		,&n_ds_1,&is_P_vs_C,&n_plot_2,&n_ds_2,&a_is_1,&opim_off,&Oligo_T,&Topen_C,&kon, &konmin, &konmax,&n_kon
		,&kd, &kdmin, &kdmax,&n_kd);
  if (i == CANCEL)	return OFF;
  if (n_plot_2 < 0 || n_plot_2 >= pr->n_op)  
        return win_printf_OK("Plot 2 out of range");
  if (n_ds_1 >= 0 && n_ds_1 < opt->n_dat) dst = opt->dat[n_ds_1];
  if (is_P_vs_C) 
    {
      opc = opt;
      dsc = dst;
      opt = pr->o_p[n_plot_2];
      if (n_ds_2 < 0 || n_ds_2 >= opt->n_dat)  
	return win_printf_OK("ds 2 out of range"); 
      dst = opt->dat[n_ds_2];
    }
  else
    {
      opc = pr->o_p[n_plot_2];
      if (n_ds_2 < 0 || n_ds_2 >= opc->n_dat)  
	return win_printf_OK("ds 2 out of range"); 
      dsc = opc->dat[n_ds_2];
    }
  if (dsc->ye == NULL)    return win_printf_OK("No error bar found for P_{block} vs [Oligo]"); 
  if (dst->ye == NULL)    return win_printf_OK("No error bar found for P_{block} vs T_{open}"); 

  dstf = find_source_specific_ds_in_op(opt,"Fit P_{block} vs T_{open}");
  if (dstf == NULL)
    {
      if ((dstf = create_and_attach_one_ds(opt, nf, nf, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      set_ds_source(dstf, "Fit P_{block} vs T_{open}, a(F) = %g; k_{on} = %g; k_{off} = %g",aF,kon,koff);

    }
  dscf = find_source_specific_ds_in_op(opc,"Fit P_{block} vs [Oligo]");
  if (dscf == NULL)
    {
      if ((dscf = create_and_attach_one_ds(opc, nf, nf, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      set_ds_source(dscf, "Fit P_{block} vs [Oligo], a(F) = %g; k_{on} = %g; k_{off} = %g",aF,kon,koff);
    }


  i = compute_best_aF_and_xi2_kon_kd(a_is_1, dst, Oligo_T, dsc, Topen_C, kon, kd, &aF, &E);

  best_kd = kd;
  best_aF = aF;
  best_kon = kon;

  if (opim_off)
    {
      for (kd = kdmin; kd <= kdmax; kd += (kdmax - kdmin)/n_kd)
	{
	  np = compute_best_aF_and_xi2_kon_kd(a_is_1, dst, Oligo_T, dsc, Topen_C, kon, kd, &aF, &E1);
	  if (np < 0) win_printf("Pb in fit");
	  if (E1 < E && np > 0)
	    {
	      best_kd = kd;
	      best_aF = aF;
	      best_kon = kon;
	      E = E1;
	    }
	}   
    }
  else
    {
      for (kon = konmin; kon <= konmax; kon += (konmax - konmin)/n_kon)
	{
	  np = compute_best_aF_and_xi2_kon_kd(a_is_1, dst, Oligo_T, dsc, Topen_C, kon, kd, &aF, &E1);
	  if (np < 0) win_printf("Pb in fit");
	  if (E1 < E && np > 0)
	    {
	      best_kd = kd;
	      best_aF = aF;
	      best_kon = kon;
	      E = E1;
	    }
	}   
    }
  kd = best_kd; 
  aF = best_aF; 
  kon = best_kon; 
  koff = kd * kon;

  for (i = 0, maxx = dst->xd[0]; i < dst->nx; i++) 
    maxx = (maxx < dst->xd[i]) ? dst->xd[i] : maxx;
  maxx *= 1.1;
  
  for (i = 0; i < dstf->nx; i++)
    {
      dstf->xd[i] = (maxx *i)/dstf->nx;
      fx = (double)aF*Oligo_T/(Oligo_T + kd);
      fx *= (1.0 - exp(-(kon* (Oligo_T+ kd)*dstf->xd[i])));
      dstf->yd[i] = fx;
      opt->need_to_refresh = 1;
    }	
	set_ds_source(dstf, "Fit P_{block} vs T_{open}, a(F) = %g; K_d = %g; k_{on} = %g; k_{off} = %g; \\xi^2 = %g",aF,kd,kon,koff,E);
  remove_all_plot_label_from_ds(dstf);
  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl P_{block} =}{a(F)\\frac{[O]}{[O]+K_d}[1 -e^{-(k_{on}.([O]+K_d)).T_{open}}]}"
	  "{a(F) =  %g; K_d = %g}{k_{on} = %g nM^{-1}s^{-1}}{k_{off}  = %gs^{-1}}{Kd = %g nM;T_{off} = %g s}"
	  "{\\chi^2 = %g, n = %d}}}}"
	  ,aF,kd,kon,koff,((kon>0)?koff/kon:0),((koff > 0)?(1/koff):0),E,np-3);
  set_ds_plot_label(dstf, opt->x_lo + (opt->x_hi - opt->x_lo)/4, 
		    opt->y_hi - (opt->y_hi - opt->y_lo)/4, USR_COORD,st);	 

  for (i = 0, maxx = dsc->xd[0]; i < dsc->nx; i++) 
    maxx = (maxx < dsc->xd[i]) ? dsc->xd[i] : maxx;
  maxx *= 1.1;

  for (i = 0; i < dscf->nx; i++)
    {
      dscf->xd[i] = (maxx *i)/dscf->nx;
      fx = (double)aF*dscf->xd[i]/(dscf->xd[i] + kd);
      fx *= (1.0 - exp(-(kon* (dscf->xd[i]+ kd)*Topen_C)));
      dscf->yd[i] = fx;
      opc->need_to_refresh = 1;
    }
	set_ds_source(dscf, "Fit P_{block} vs [Oligo], a(F) = %g; K_d = %g; k_{on} = %g; k_{off} = %g; \\xi^2 = %g",aF,kd,kon,koff,E);
  remove_all_plot_label_from_ds(dscf);
  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl P_{block} =}{a(F)\\frac{[O]}{[O]+K_d}[1 -e^{-(k_{on}.([O]+K_d)).T_{open}}]}"
	  "{a(F) =  %g; K_d = %g}{k_{on} = %g nM^{-1}s^{-1}}{k_{off}  = %gs^{-1}}{Kd = %g nM;T_{off} = %g s}"
	  "{\\chi^2 = %g, n = %d}}}}"
	  ,aF,kd,kon,koff,((kon>0)?koff/kon:0),((koff > 0)?(1/koff):0),E,np-3);
  set_ds_plot_label(dscf, opt->x_lo + (opt->x_hi - opt->x_lo)/4, 
		    opt->y_hi - (opt->y_hi - opt->y_lo)/4, USR_COORD,st);	 

  refresh_plot(pr, UNCHANGED);
  return D_O_K;

}


int compute_best_aF_and_xi2_kon_kd_for_op(int a_is_1, O_p *ops, float kon, float kd, float *aF, double *E)
{
  double xi2 = 0;
  double fy, f, y, fx, er, la = 0;
  int i, j, ids;
  d_s *dst = NULL;
  d_s *dsc = NULL;
  float Topen_C, Oligo_T;

  if (ops == NULL) return -1;
  if (a_is_1 == 0)
    {
      for (ids = 0, f = fy = 0; ids < ops->n_dat; ids++)
	{
	  dsc = dst = ops->dat[ids];
	  if (dsc->treatement == NULL) continue;
	  if (strncmp(dsc->treatement,"P_{block}([Oligo]) with T_{open} = ",34) == 0)
	    {
	      if (dsc->ye == NULL) return win_printf("no error bars in dsc");
	      Topen_C = dsc->src_parameter[0];
	      for (i = 0; i < dsc->nx; i++)
		{
		  y = dsc->yd[i];
		  er = dsc->ye[i];
		  fx = (double)dsc->xd[i]/(dsc->xd[i] + kd);
		  fx *= (1.0 - exp(-(kon* (dsc->xd[i]+ kd)*Topen_C)));
		  if (er > 0)
		    {
		      fy += y*fx/(er*er);
		      f += (fx*fx)/(er*er);
		    }
		  else win_printf("Negative error ds %d pt %d",ids,i); 
		}
	      //win_printf("ds %d [oligo] %d pts compute f %g fy %g Topen_C %g",ids,dsc->nx,f,fy,Topen_C);
	    }
	  if (strncmp(dst->treatement,"P_{block}(T_{open}) with [Oligo] = ",34) == 0)
	    {
	      if (dst->ye == NULL) return win_printf("no error bars in dsc");
	      Oligo_T = dst->src_parameter[0];
	      for (i = 0; i < dst->nx; i++)
		{
		  y = dst->yd[i];
		  er = dst->ye[i];
		  fx = (double)Oligo_T/(Oligo_T + kd);
		  fx *= (1.0 - exp(-(kon* (Oligo_T+ kd)*dst->xd[i])));
		  if (er > 0)
		    {
		      fy += y*fx/(er*er);
		      f += (fx*fx)/(er*er);
		    }
		  else win_printf("Negative error ds %d pt %d",ids,i); 
		}
	      //win_printf("ds %d [oligo] %d pts compute f %g fy %g Oligo_T %g",ids,dsc->nx,f,fy,Oligo_T);
	    }
	}
      if (f != 0)    la = fy/f;
      else 
	{
	  win_printf("f = %g! fy = %g imposin a = 1",la,f,fy);
	  la = 1;
	}
      if (la < 0.000001) win_printf("Low la = %g f = %g fy = %g",la,f,fy); 
    }
  else 
    {
      la = 1;
      //win_printf("imposing a = 1");
    }
  la = (la > 1.0) ? 1.0 : la; 
  if (aF) *aF = la;
  //win_printf("start computing \\chi^2");
  for (ids = 0, j = 0, xi2 = 0; ids < ops->n_dat; ids++)
    {
      dsc = ops->dat[ids];
      if (dsc->treatement == NULL) continue;
      if (strncmp(dsc->treatement,"P_{block}([Oligo]) with T_{open} = ",34) == 0)
	{
	  if (dsc->ye == NULL) return win_printf("no error bars in dsc");
	  Topen_C = dsc->src_parameter[0];
	  for (i = 0; i < dsc->nx; i++)
	    {
	      y = dsc->yd[i];
	      er = dsc->ye[i];
	      fx = (double)la*dsc->xd[i]/(dsc->xd[i] + kd);
	      fx *= (1.0 - exp(-(kon* (dsc->xd[i]+ kd)*Topen_C)));
	      f = y - fx;
	      if (er > 0)
		{
		  xi2 += (f*f)/(er*er);
		  j++;
		}
	    }
	}
      else if (strncmp(dsc->treatement,"P_{block}(T_{open}) with [Oligo] = ",34) == 0)
	{
	  if (dsc->ye == NULL) return win_printf("no error bars in dsc");
	  Oligo_T = dsc->src_parameter[0];
	  for (i = 0; i < dsc->nx; i++)
	    {
	      y = dsc->yd[i];
	      er = dsc->ye[i];
	      fx = (double)la*Oligo_T/(Oligo_T + kd);
	      fx *= (1.0 - exp(-(kon* (Oligo_T+ kd)*dsc->xd[i])));
	      f = y - fx;
	      if (er > 0)
		{
		  xi2 += (f*f)/(er*er);
		  j++;
		}
	    }	
	}
      //else win_printf("Did not find treatment \n%s\n",dsc->treatement);
    }
  if (E) *E = xi2;
  return j;
} 


int draw_fit_on_data(pltreg *pr, O_p *op, float a_is_1, float kon, float kd, float aF, double E, int nd)
{
  int i, j;
  float koff = kd * kon;
  float xl, yl, fx, Topen_C, Oligo_T;
  d_s *ds;
  char st[2048];

  for (i = 0; i < op->n_dat; i++)
    {
      ds = op->dat[i];
      if (ds->treatement == NULL) continue;
      if (strncmp(ds->treatement,"Fit P_{block} vs [Oligo] with T_{open} = ",24) == 0)
	{
	  Topen_C = ds->src_parameter[0];
	  for (j = 0; j < ds->nx; j++)
	    {
	      fx = (double)aF*ds->xd[j]/(ds->xd[j] + kd);
	      fx *= (1.0 - exp(-(kon* (ds->xd[j]+ kd)*Topen_C)));
	      ds->yd[j] = fx;
	    }
	  op->need_to_refresh = 1;
	  xl = op->x_lo + (op->x_hi - op->x_lo)/4;
	  yl = op->y_hi - (op->y_hi - op->y_lo)/4;
	  if (ds->n_lab > 0)
	    {
	      xl = ds->lab[0]->xla;
	      yl = ds->lab[0]->yla;
	    }
	  set_src_parameter(ds, 1, aF, "aF = ");
	  set_src_parameter(ds, 2, kd, "kd = ");
	  set_src_parameter(ds, 3, kon, "k_{on} = ");
	  set_src_parameter(ds, 4, a_is_1, "a_is_1  = ");  
	  set_src_parameter(ds, 5, E, "\\chi^2  = ");  
	  set_ds_treatement(ds, "Fit P_{block} vs [Oligo] with T_{open} = %g s; a(F) = %g; "
			    "K_d = %g; k_{on} = %g; k_{off} = %g; \\chi^2 = %g; aIs1 = %d"
			    ,Topen_C,aF,kd,kon,koff,E,a_is_1);
	  remove_all_plot_label_from_ds(ds);
	  snprintf(st,sizeof(st),"\\fbox{\\pt8\\stack{{\\sl P_{block} =}{a(F)\\frac{[O]}"
		   "{[O]+K_d}[1 -e^{-(k_{on}.([O]+K_d)).T_{open}}]}"
		   "{a(F) =  %g; K_d = %g nM}{k_{on} = %g nM^{-1}s^{-1}}{k_{off}  = %gs^{-1}}{T_{off} = %g s}"
		   "{\\chi^2 = %g, n = %d}}}}"
		   ,aF,kd,kon,koff,((koff > 0)?((float)1/koff):0),E,nd-3);
	  set_ds_plot_label(ds, xl, yl, USR_COORD, st);	 
	}

      if (strncmp(ds->treatement,"Fit P_{block} vs T_{open} with [Oligo] =",38) == 0)
	{
	  Oligo_T = ds->src_parameter[0];
	  for (j = 0; j < ds->nx; j++)
	    {
	      fx = (double)aF*Oligo_T/(Oligo_T + kd);
	      fx *= (1.0 - exp(-(kon* (Oligo_T+ kd)*ds->xd[j])));
	      ds->yd[j] = fx;
	    }	
	  op->need_to_refresh = 1;
	  xl = op->x_lo + (op->x_hi - op->x_lo)/4;
	  yl = op->y_hi - (op->y_hi - op->y_lo)/4;
	  if (ds->n_lab > 0)
	    {
	      xl = ds->lab[0]->xla;
	      yl = ds->lab[0]->yla;
	    }
	  set_src_parameter(ds, 1, aF, "aF = ");
	  set_src_parameter(ds, 2, kd, "kd = ");
	  set_src_parameter(ds, 3, kon, "k_{on} = ");
	  set_src_parameter(ds, 4, a_is_1, "a_is_1  = ");  
	  set_src_parameter(ds, 5, E, "\\chi^2  = ");  
	  set_ds_treatement(ds, "Fit P_{block} vs T_{open} with [Oligo] = %g nM; a(F) = %g; "
			    "K_d = %g; k_{on} = %g; k_{off} = %g; \\chi^2 = %g; aIs1 = %d"
			    ,Oligo_T,aF,kd,kon,koff,E,a_is_1);
	  remove_all_plot_label_from_ds(ds);
	  snprintf(st,sizeof(st),"\\fbox{\\pt8\\stack{{\\sl P_{block} =}{a(F)\\frac{[O]}"
		   "{[O]+K_d}[1 -e^{-(k_{on}.([O]+K_d)).T_{open}}]}"
		   "{a(F) =  %g; K_d = %g nM}{k_{on} = %g nM^{-1}s^{-1}}{k_{off}  = %gs^{-1}}{T_{off} = %g s}"
		   "{\\chi^2 = %g, n = %d}}}}"
		   ,aF,kd,kon,koff,((koff > 0)?((float)1/koff):0),E,nd-3);
	  set_ds_plot_label(ds, xl, yl, USR_COORD, st);	 
	}
    }
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}

int inc_kd(void)
{
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsc = NULL;
  int a_is_1 = 1, nd = 0;
  float  kon, kd, aF = 1;
  double  E = 0;

  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2) return D_O_K;

  dsc = find_treatement_specific_ds_in_op(op,"Fit P_{block} vs [Oligo] with T_{open} = ");
  if (dsc == NULL) 
    dsc = find_treatement_specific_ds_in_op(op,"Fit P_{block} vs T_{open} with [Oligo] = ");
  if (dsc != NULL)
    {
      if (dsc->src_parameter_type[2] != NULL)
	{
	  kd = dsc->src_parameter[2];
	  kon = dsc->src_parameter[3];
	  kd *= 1.01;
	  a_is_1 = dsc->src_parameter[4];
	  nd = compute_best_aF_and_xi2_kon_kd_for_op(a_is_1, op, kon, kd, &aF, &E);
	  draw_fit_on_data(pr, op, a_is_1, kon, kd, aF, E, nd);
	  refresh_plot(pr, UNCHANGED);
	  return D_O_K;
	}
    }
  return D_O_K;
}


int dec_kd(void)
{
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsc = NULL;
  int a_is_1 = 1, nd = 0;
  float  kon, kd, aF = 1;
  double  E = 0;

  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2) return D_O_K;

  dsc = find_treatement_specific_ds_in_op(op,"Fit P_{block} vs [Oligo] with T_{open} = ");
  if (dsc == NULL) 
    dsc = find_treatement_specific_ds_in_op(op,"Fit P_{block} vs T_{open} with [Oligo] = ");
  if (dsc != NULL)
    {
      if (dsc->src_parameter_type[2] != NULL)
	{
	  kd = dsc->src_parameter[2];
	  kon = dsc->src_parameter[3];
	  kd *= 0.99;
	  a_is_1 = dsc->src_parameter[4];
	  nd = compute_best_aF_and_xi2_kon_kd_for_op(a_is_1, op, kon, kd, &aF, &E);
	  draw_fit_on_data(pr, op, a_is_1, kon, kd, aF, E, nd);
	  refresh_plot(pr, UNCHANGED);
	  return D_O_K;
	}
    }
  return D_O_K;
}

int inc_kon(void)
{
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsc = NULL;
  int a_is_1 = 1, nd = 0;
  float  kon, kd, aF = 1;
  double  E = 0;

  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2) return D_O_K;

  dsc = find_treatement_specific_ds_in_op(op,"Fit P_{block} vs [Oligo] with T_{open} = ");
  if (dsc == NULL) 
    dsc = find_treatement_specific_ds_in_op(op,"Fit P_{block} vs T_{open} with [Oligo] = ");
  if (dsc != NULL)
    {
      if (dsc->src_parameter_type[2] != NULL)
	{
	  kd = dsc->src_parameter[2];
	  kon = dsc->src_parameter[3];
	  kon *= 1.01;
	  a_is_1 = dsc->src_parameter[4];
	  nd = compute_best_aF_and_xi2_kon_kd_for_op(a_is_1, op, kon, kd, &aF, &E);
	  draw_fit_on_data(pr, op, a_is_1, kon, kd, aF, E, nd);
	  refresh_plot(pr, UNCHANGED);
	  return D_O_K;
	}
    }
  return D_O_K;
}


int dec_kon(void)
{
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsc = NULL;
  int a_is_1 = 1, nd = 0;
  float  kon, kd, aF = 1;
  double  E = 0;

  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2) return D_O_K;

  dsc = find_treatement_specific_ds_in_op(op,"Fit P_{block} vs [Oligo] with T_{open} = ");
  if (dsc == NULL) 
    dsc = find_treatement_specific_ds_in_op(op,"Fit P_{block} vs T_{open} with [Oligo] = ");
  if (dsc != NULL)
    {
      if (dsc->src_parameter_type[2] != NULL)
	{
	  kd = dsc->src_parameter[2];
	  kon = dsc->src_parameter[3];
	  kon *= 0.99;
	  a_is_1 = dsc->src_parameter[4];
	  nd = compute_best_aF_and_xi2_kon_kd_for_op(a_is_1, op, kon, kd, &aF, &E);
	  draw_fit_on_data(pr, op, a_is_1, kon, kd, aF, E, nd);
	  refresh_plot(pr, UNCHANGED);
	  return D_O_K;
	}
    }
  return D_O_K;
}

int do_KonKoff_Kd_kon_multi_ds(void)
{
  register int i, j, k;
  O_p *op = NULL;
  int nf = 1024;
  d_s *dstf = NULL, *ds = NULL;
  pltreg *pr = NULL;
  static int a_is_1 = 0, look = 1;
  static int pblock = 0, vs_conc_oligo = 0;
  static float Oligo_T = 20; // in nM
  static float Topen_C = 10; // in s
  static float kon = 0.003;
  static float kd = 10;
  static float ratio = 1.001;
  float aF = 1.0,  tmp, koff = 0;
  double E = 0, Et = 0, Em;
  char st[2048] = {0}, question[1024] = {0};
  int nd, minmin, km;
  float ko[3][3] = {{0}}, Kd[3][3] = {{0}},kon_nxt,kd_nxt;

  if(updating_menu_state != 0)	
    {
      remove_short_cut_from_dialog(0, KEY_UP);
      remove_short_cut_from_dialog(0, KEY_DOWN);
      remove_short_cut_from_dialog(0, KEY_RIGHT);
      remove_short_cut_from_dialog(0, KEY_LEFT);
      add_keyboard_short_cut(0, KEY_UP, 0, inc_kd);
      add_keyboard_short_cut(0, KEY_DOWN, 0, dec_kd);
      add_keyboard_short_cut(0, KEY_RIGHT, 0, inc_kon);
      add_keyboard_short_cut(0, KEY_LEFT, 0, dec_kon);
      return D_O_K;		
    }



  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("cannot find data");

  for (i = 0; i < op->n_dat; i++)
    {
      ds = op->dat[i];
      if (ds->treatement != NULL)
	{
	  if (sscanf(ds->treatement,"P_{block}([Oligo]) with T_{open} = %f",&Topen_C) == 1)
	    {
	      set_src_parameter(ds, 0, Topen_C, "T_{open} = ");
	      continue;
	    }
	  else if (sscanf(ds->treatement,"P_{block}(T_{open}) with [Oligo] = %f",&Oligo_T) == 1)
	    {
	      set_src_parameter(ds, 0, Oligo_T, "[Oligo] = ");
	      continue;
	    }
	  else if (strncmp(ds->treatement,"Fit P_{block} vs [Oligo] with T_{open} =",40) == 0)
	    continue;
	  else if (strncmp(ds->treatement,"Fit P_{block} vs T_{open} with [Oligo] =",40) == 0)
	    continue;
	  //else win_printf("patterns:\nP_{block}([Oligo]) with T_{open} = ;or\n"
	  //		  "P_{block}(T_{open}) with [Oligo] =;\nnot found in ds %d treatment\n%s"
	  //		  ,i,ds->treatement);
	}
      if (ds->ye == NULL)    continue;
      snprintf(question,sizeof(question),"For Dataset %d: with source\n%s\n"
	       "Specify if it is an oligo Blocking probabity %%b"
	       "If yes is it %%RP_{block}(T_{open}) or %%rP_{block}([Oligo])\n"   
	       ,i,((ds->source)?ds->source:"no source"));
      j = win_scanf(question,&pblock,&vs_conc_oligo);
      if (j == CANCEL) continue;
      if (vs_conc_oligo)
	{
	  tmp = 10;
	  win_scanf("For P_{block}([Oligo]) define T_{open} = %6f in s\n",&tmp);
	  set_src_parameter(ds, 0, tmp, "T_{open} = ");
	  set_ds_treatement(ds,"P_{block}([Oligo]) with T_{open} = %g s;",tmp);
	}
      else
	{
	  tmp = 20;
	  win_scanf("For P_{block}(T_{open}) define  [Oligo] = %6f in nM\n",&tmp);
	  set_src_parameter(ds, 0, tmp, "[Oligo] = ");
	  set_ds_treatement(ds,"P_{block}(T_{open}) with [Oligo] = %g nM;",tmp);
	}
    }
  Topen_C = 10; Oligo_T = 20; aF = 1; kd = 10; kon = 0.004; koff = 0.01; E = 0;
  for (i = 0, nd = op->n_dat; i < nd; i++)
    {
      ds = op->dat[i];
      if (ds->treatement == NULL) continue;
      if (strncmp(ds->treatement,"P_{block}([Oligo]) with T_{open}",22) == 0)
	{
	  if (sscanf(ds->treatement,"P_{block}([Oligo]) with T_{open} = %f",&Topen_C) != 1)
	    win_printf("Cannot recover T_{open} in \n%s",ds->treatement);
	  //Topen_C = ds->src_parameter[0];
	  snprintf(st,sizeof(st),"Fit P_{block} vs [Oligo] with T_{open} = %g s;",Topen_C);
	  dstf = find_treatement_specific_ds_in_op(op,st);
	  if (dstf == NULL)
	    {
	      snprintf(question,sizeof(question),"For dataset %d \n%s\n"
		       "Over what maximum range of [Oligo] \ndo you want to draw fit %%6f in nM\n",i,st);
	      for (j = 0, tmp = -FLT_MAX; j < ds->nx; j++) 
		tmp = (ds->xd[j] > tmp) ? ds->xd[j] : tmp;
	      tmp *= 1.25;
	      j = win_scanf(question,&tmp);
	      if ((dstf = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create data set");
	      set_ds_treatement(dstf, "%s Max-range = %g",st,tmp);
	      inherit_from_ds_to_ds(dstf, ds);
	      for (j = 0; j < dstf->nx; j++) dstf->xd[j] = (tmp*j)/dstf->nx; 
	    }
	  else
	    {
	      sscanf(dstf->treatement,"Fit P_{block} vs [Oligo] with T_{open} = %f s; a(F) = %f; "
		     "K_d = %f; k_{on} = %f; k_{off} = %f; \\chi^2 = %lf; aIs1 = %d",
		     &Topen_C,&aF,&kd,&kon,&koff,&E,&a_is_1);
	    }
	  set_src_parameter(dstf, 0, Topen_C, "T_{open} = "); 
	}
      if (strncmp(ds->treatement,"P_{block}(T_{open}) with [Oligo]",22) == 0)
	{
	  if (sscanf(ds->treatement,"P_{block}(T_{open}) with [Oligo] = %f",&Oligo_T) != 1)
	    win_printf("Cannot recover [Oligo] in \n%s",ds->treatement);
	  //Oligo_T = ds->src_parameter[0];
	  snprintf(st,sizeof(st),"Fit P_{block} vs T_{open} with [Oligo] = %g nM;",Oligo_T);
	  dstf = find_treatement_specific_ds_in_op(op,st);
	  if (dstf == NULL)
	    {
	      snprintf(question,sizeof(question),"For dataset %d \n%s\n"
		       "Over what maximum range of time \ndo you want to draw fit %%6f in s\n",i,st);
	      for (j = 0, tmp = -FLT_MAX; j < ds->nx; j++) 
		tmp = (ds->xd[j] > tmp) ? ds->xd[j] : tmp;
	      tmp *= 1.25;
	      j = win_scanf(question,&tmp);
	      if ((dstf = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)
		return win_printf_OK("cannot create data set");
	      set_ds_treatement(dstf, "%s Max-range = %g",st,tmp);
	      inherit_from_ds_to_ds(dstf, ds);
	      for (j = 0; j < dstf->nx; j++) dstf->xd[j] = (tmp*j)/dstf->nx; 
	    }
	  else
	    {
	      sscanf(dstf->treatement, "Fit P_{block} vs T_{open} with [Oligo] = %f nM; a(F) = %f; "
		     "K_d = %f; k_{on} = %f; k_{off} = %f; \\chi^2 = %lf; aIs1 = %d"
		     ,&Oligo_T,&aF,&kd,&kon,&koff,&E,&a_is_1);
	    }
	  set_src_parameter(dstf, 0, Oligo_T, "[Oligo] = ");
	}
    }




  //if (n_ds_1 <= 0) n_ds_1 = opt->cur_dat;
  i = win_scanf("Fit P_{block} = a(F)\\frac{[O]}{[O]+K_d}[1 -e^{-(k_{on}.([O]+Kd)).T_{open}}]\n\n"
		"To multiple datasets\n"
		"Impose a(F) = 1 %b\n"
		"Starting k_{on} = %8f\n"
		"Starting K_{d} = %8f\n"
		"%bLook for minimum of \\chi^2\n"
		"searching ratio %6f\n"
		,&a_is_1,&kon,&kd,&look, &ratio);
  if (i == CANCEL)	return OFF;


  nd = compute_best_aF_and_xi2_kon_kd_for_op(a_is_1, op, kon, kd, &aF, &E);
  draw_fit_on_data(pr, op, a_is_1, kon, kd, aF, E, nd);

  if (look)
    {
	for (i = 0, minmin = 0; minmin == 0 && i < 10000000; i++)
	  {
	    ko[0][0] = ko[0][1] = ko[0][2] = ratio * kon;  
	    ko[1][0] = ko[1][1] = ko[1][2] = kon;  
	    ko[2][0] = ko[2][1] = ko[2][2] = kon/ratio;

	    Kd[0][0] = Kd[1][0] = Kd[2][0] = ratio * kd;  
	    Kd[0][1] = Kd[1][1] = Kd[2][1] = kd;  
	    Kd[0][2] = Kd[1][2] = Kd[2][2] = kd/ratio;
  
	    for (k = 0, Em = FLT_MAX; k < 9; k++)
	      {
		compute_best_aF_and_xi2_kon_kd_for_op(a_is_1, op, ko[k/3][k%3], Kd[k/3][k%3], &aF, &Et);
		if (Et <= Em)
		  {
		    Em = Et;
		    km = k;
		    kon_nxt = ko[k/3][k%3];
		    kd_nxt = Kd[k/3][k%3];
		  }
	      }
	    if (km == 4) minmin = 1;
	    kon = kon_nxt;
	    kd = kd_nxt;
	    nd = compute_best_aF_and_xi2_kon_kd_for_op(a_is_1, op, kon, kd, &aF, &E);
	    draw_fit_on_data(pr, op, a_is_1, kon, kd, aF, E, nd);
	    display_title_message("iter %d minmin %d kon %g kd %g E %g", i,minmin, kon,kd,E);
	  }
    }


  refresh_plot(pr, UNCHANGED);
  return D_O_K;

}




int compute_best_aF_and_xi2_kon_koff_2(int a_is_1, d_s *dst1, float Oligo_T1, d_s *dst2, float Oligo_T2, float kon, float koff, float *aF, double *E)
{
  double xi2 = 0;
  double fy, f, y, fx, er, la = 0;
  int i, j;

  if (a_is_1 == 0)
    {
      for (i = 0, f = fy = 0; i < dst1->nx; i++)
	{
	  y = dst1->yd[i];
	  er = dst1->ye[i];
	  fx = (double)(kon* Oligo_T1)/((kon* Oligo_T1) + koff);
	  fx *= (1.0 - exp(-((kon* Oligo_T1) + koff)*dst1->xd[i]));
	  if (er > 0)
	    {
	      fy += y*fx/(er*er);
	      f += (fx*fx)/(er*er);
	    }
	}
      for (i = 0; i < dst2->nx; i++)
	{
	  y = dst2->yd[i];
	  er = dst2->ye[i];
	  fx = (double)(kon* Oligo_T2)/((kon* Oligo_T2) + koff);
	  fx *= (1.0 - exp(-((kon* Oligo_T2) + koff)*dst2->xd[i]));
	  if (er > 0)
	    {
	      fy += y*fx/(er*er);
	      f += (fx*fx)/(er*er);
	    }
	}
      if (f != 0)    la = fy/f;
      else return -3;
      if (la < 0.000001) win_printf("Low la = %g f = %g fy = %g",la,f,fy);
    }
  else la = 1;
  la = (la > 1.0) ? 1.0 : la; 
  if (aF) *aF = la;
  for (i = 0, j = 0, xi2 = 0; i < dst1->nx; i++)
    {
      y = dst1->yd[i];
      er = dst1->ye[i];
      fx = (double)la*(kon* Oligo_T1)/((kon* Oligo_T1) + koff);
      fx *= (1.0 - exp(-((kon* Oligo_T1) + koff)*dst1->xd[i]));
      f = y - fx;
      if (er > 0)
	{
	  xi2 += (f*f)/(er*er);
	  j++;
	}
    }	
  for (i = 0; i < dst2->nx; i++)
    {
      y = dst2->yd[i];
      er = dst2->ye[i];
      fx = (double)la*(kon* Oligo_T2)/((kon* Oligo_T2) + koff);
      fx *= (1.0 - exp(-((kon* Oligo_T2) + koff)*dst2->xd[i]));
      f = y - fx;
      if (er > 0)
	{
	  xi2 += (f*f)/(er*er);
	  j++;
	}
    }
  if (E) *E = xi2;
  return j;
} 



int do_KonKoff_on_2_data_sets(void)
{
  register int i;
  O_p *opt = NULL;
  int nf = 1024, np;
  d_s *dst1, *dst1f = NULL, *dst2, *dst2f = NULL;
  pltreg *pr = NULL;
  static int  a_is_1 = 0, n_ds_1 = -1, n_ds_2 = 0, opim_off = 0, n_kon = 256, n_koff = 256;
  static float Oligo_T1 = 20, Oligo_T2 = 50;
  static float kon = 0.003, konmin = 0.001, konmax = 0.009;
  static float koff = 0.01, koffmin = 0.001, koffmax = 0.1;
  float aF = 1.0, best_aF, best_kon, best_koff, maxx, fx;
  double E, E1;
  char st[2048];

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&opt,&dst1) != 3)
    return win_printf_OK("cannot find data");
  if (n_ds_1 < 0) n_ds_1 = opt->cur_dat;
  i = win_scanf("The dataset %8d of the present plot displays:\n"
		"P_{block} vs T_{open}\n"
		"Specify the second ds# %8d\n"
		"I am going to fit :\n\n"
		"P_{block} = a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]\n\n"
		"Impose a(F) to be 1 %b\n"
		"Optimize versus %R k_{on} or %r k_{off}\n"
		"For data set 1 specify [oligo1] = %8f\n"
		"For data set 2 specify [oligo2] = %8f\n"
		"Range k_{on} = %8f min %8f max %8f #increments %8d\n"
		"Range k_{off} = %8f min %8f max %8f #increments %8d\n"
		,&n_ds_1,&n_ds_2,&a_is_1,&opim_off,&Oligo_T1,&Oligo_T2,&kon, &konmin, &konmax,&n_kon
		,&koff, &koffmin, &koffmax,&n_koff);
  if (i == CANCEL)	return OFF;
  if (n_ds_1 >= 0 && n_ds_1 < opt->n_dat) dst1 = opt->dat[n_ds_1];
  dst2 = opt->dat[n_ds_2];

  if (dst1->ye == NULL)
    return win_printf_OK("No error bar found for P_{block} vs T_{open1}" ); 
  if (dst2->ye == NULL)
    return win_printf_OK("No error bar found for P_{block} vs T_{open2}"); 

  dst1f = find_source_specific_ds_in_op(opt,"Fit 1 P_{block} vs T_{open}");
  if (dst1f == NULL)
    {
      if ((dst1f = create_and_attach_one_ds(opt, nf, nf, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      set_ds_source(dst1f, "Fit 1 P_{block} vs T_{open}, a(F) = %g; k_{on} = %g; k_{off} = %g",aF,kon,koff);

    }


  dst2f = find_source_specific_ds_in_op(opt,"Fit 2 P_{block} vs T_{open}");
  if (dst2f == NULL)
    {
      if ((dst2f = create_and_attach_one_ds(opt, nf, nf, 0)) == NULL)
	return win_printf_OK("cannot create data set");
      set_ds_source(dst2f, "Fit 2 P_{block} vs T_{open}, a(F) = %g; k_{on} = %g; k_{off} = %g",aF,kon,koff);

    }

  i = compute_best_aF_and_xi2_kon_koff_2(a_is_1, dst1, Oligo_T1, dst2, Oligo_T2, kon, koff, &aF, &E);

  best_koff = koff;
  best_aF = aF;
  best_kon = kon;



  if (opim_off)
    {
      for (koff = koffmin; koff <= koffmax; koff += (koffmax - koffmin)/n_koff)
	{
	  np = compute_best_aF_and_xi2_kon_koff_2(a_is_1, dst1, Oligo_T1, dst2, Oligo_T2, kon, koff, &aF, &E1);	  
	  if (np < 0) win_printf("Pb in fit");
	  if (E1 < E && np > 0)
	    {
	      best_koff = koff;
	      best_aF = aF;
	      best_kon = kon;
	      E = E1;
	    }
	}   
    }
  else
    {
      for (kon = konmin; kon <= konmax; kon += (konmax - konmin)/n_kon)
	{
	  np = compute_best_aF_and_xi2_kon_koff_2(a_is_1, dst1, Oligo_T1, dst2, Oligo_T2, kon, koff, &aF, &E1);	  
	  if (np < 0) win_printf("Pb in fit");
	  if (E1 < E && np > 0)
	    {
	      best_koff = koff;
	      best_aF = aF;
	      best_kon = kon;
	      E = E1;
	    }
	}   
    }



  koff = best_koff; 
  aF = best_aF; 
  kon = best_kon; 

  for (i = 0, maxx = dst1->xd[0]; i < dst1->nx; i++) 
    maxx = (maxx < dst1->xd[i]) ? dst1->xd[i] : maxx;
  maxx *= 1.1;
  
  for (i = 0; i < dst1f->nx; i++)
    {
      dst1f->xd[i] = (maxx *i)/dst1f->nx;
      fx = (double)aF*(kon* Oligo_T1)/((kon* Oligo_T1) + koff);
      fx *= (1.0 - exp(-((kon* Oligo_T1) + koff)*dst1f->xd[i]));
      dst1f->yd[i] = fx;
      opt->need_to_refresh = 1;
    }	
  set_ds_source(dst1f, "Fit 1 P_{block} vs T_{open}, a(F) = %g; k_{on} = %g; k_{off} = %g; \\xi^2 = %g",aF,kon,koff,E);
  remove_all_plot_label_from_ds(dst1f);
  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl P_{block} =}{a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]}"
	  "{a(F) =  %g}{k_{on} = %g nM^{-1}s^{-1}}{k_{off}  = %gs^{-1}}{Kd = %g nM;T_{off} = %g s}"
	  "{\\chi^2 = %g, n = %d}}}}"
	  ,aF,kon,koff,((kon>0)?koff/kon:0),((koff > 0)?(1/koff):0),E,np-3);
  set_ds_plot_label(dst1f, opt->x_lo + (opt->x_hi - opt->x_lo)/4, 
		    opt->y_hi - (opt->y_hi - opt->y_lo)/4, USR_COORD,st);	 

  for (i = 0, maxx = dst2->xd[0]; i < dst2->nx; i++) 
    maxx = (maxx < dst2->xd[i]) ? dst2->xd[i] : maxx;
  maxx *= 1.1;

  for (i = 0; i < dst2f->nx; i++)
    {
      dst2f->xd[i] = (maxx *i)/dst2f->nx;
      //"P_{block} = a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]\n"
      fx = (double)aF*(kon* Oligo_T2)/((kon* Oligo_T2) + koff);
      fx *= (1.0 - exp(-((kon* Oligo_T2) + koff)*dst2f->xd[i]));
      dst2f->yd[i] = fx;
      opt->need_to_refresh = 1;
    }
  set_ds_source(dst2f, "Fit 2 P_{block} vs T_{open}, a(F) = %g; k_{on} = %g; k_{off} = %g; \\xi^2 = %g",aF,kon,koff,E);
  remove_all_plot_label_from_ds(dst2f);
  sprintf(st,"\\fbox{\\pt8\\stack{{\\sl P_{block} =}{a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]}"
	  "{a(F) =  %g}{k_{on} = %g nM^{-1}s^{-1}}{k_{off}  = %gs^{-1}}{Kd = %g nM;T_{off} = %g s}"
	  "{\\chi^2 = %g, n = %d}}}}"
	  ,aF,kon,koff,((kon>0)?koff/kon:0),((koff > 0)?(1/koff):0),E,np-3);
  set_ds_plot_label(dst2f, opt->x_lo + (opt->x_hi - opt->x_lo)/4, 
		    opt->y_hi - (opt->y_hi - opt->y_lo)/4, USR_COORD,st);	 

  refresh_plot(pr, UNCHANGED);
  return D_O_K;

}




int do_draw_KonKoff(void)
{
  register int i, j;
  O_p *op = NULL;
  int nf = 1024;
  d_s *ds = NULL;
  pltreg *pr = NULL;
  static int is_P_vs_C = 0, nrepeat = 1;
  static float Oligo_T = 20; // in nM
  static float Topen_C = 10; // in s
  static float kon = 0.003;
  static float koff = 0.01, mopen = 60, moligo = 500;
  float aF = 1.0, fx, tmp;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");
  i = win_scanf("I am going to plot :\n\n"
		"P_{block} = a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]\n\n"
		"Draw %R P_{block} vs T_{open} or %r P_{block} vs [Oligo]\n"
		"Specify a(F) = %8f\n"
		"For P_{block} vs T_{open} specify [oligo] = %8f\n"
		"For P_{block} vs [Oligo] specify T_{open} = %8f\n"
		"k_{on} = %8f k_{off} = %8f\n"
		"Max T_{open} %8f Max [Oligo] %8f\n"
		"%8d repeats multiple data sets\n"
		,&is_P_vs_C,&aF,&Oligo_T,&Topen_C,&kon,&koff,&mopen,&moligo,&nrepeat);
  if (i == CANCEL)	return OFF;

  op = create_and_attach_one_plot(pr, nf, nf, 0);
  if (op == NULL) return win_printf_OK("cannot create plot");
  ds = op->dat[0];




  set_plot_y_title(op,"P_{block}"); 
  if (is_P_vs_C)       
    {
      set_plot_title(op,"\\fbox{\\pt8\\stack{{\\sl P_{block} = a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]}\\pt7{T_{open} = %g s; k_{on} = %g nM^{-1}s^{-1}; k_{off} = %g s^{-1}; Kd = %g nM; a(F) = %g}}}",Topen_C,kon,koff,((kon>0)?koff/kon:0),aF);
      set_plot_x_title(op,"[Oligo] (nM)");
      for (j = nrepeat; j > 0; j--)
	{
	  if (ds == NULL)
	    {
	      ds = create_and_attach_one_ds(op, nf, nf, 0);
	      if (ds == NULL) return win_printf_OK("cannot create ds");
	    }
	  tmp = (j*Topen_C)/nrepeat;
	  set_ds_source(ds, "P_{block} vs T_{open}, a(F) = %g; k_{on} = %g; k_{off} = %g; T_{open} = %gs",aF,kon,koff,tmp);
	  for (i = 0; i < ds->nx; i++)
	    {
	      ds->xd[i] = (moligo *i)/ds->nx;
	      fx = (double)aF*(kon* ds->xd[i])/((kon* ds->xd[i]) + koff);
	      fx *= (1.0 - exp(-((kon* ds->xd[i] + koff)*tmp)));
	      ds->yd[i] = fx;
	      op->need_to_refresh = 1;
	    }
	  ds = NULL;
	}
    }
  else                 
    {
      set_plot_title(op,"\\fbox{\\pt8\\stack{{\\sl P_{block} = a(F)\\frac{k_{on}.[O]}{k_{on}.[O]+k_{off}}[1 -e^{-(k_{on}.[O]+k_{off}).T_{open}}]}\\pt7{[Oligo] = %g nM; k_{on} = %g nM^{-1}s^{-1}; k_{off} = %g s^{-1}; Kd = %g nM; a(F) = %g}}}",Oligo_T,kon,koff,((kon>0)?koff/kon:0),aF);
      set_plot_x_title(op,"T_{open} (s)"); 
      for (j = nrepeat; j > 0; j--)
	{
	  if (ds == NULL)
	    {
	      ds = create_and_attach_one_ds(op, nf, nf, 0);
	      if (ds == NULL) return win_printf_OK("cannot create ds");
	    }
	  tmp = (j*Oligo_T)/nrepeat;
	  set_ds_source(ds, "P_{block} vs T_{open}, a(F) = %g; k_{on} = %g; k_{off} = %g; [Oligo] = %g nM",aF,kon,koff,tmp);
	  for (i = 0; i < ds->nx; i++)
	    {
	      ds->xd[i] = (mopen *i)/ds->nx;
	      fx = (double)aF*(kon* tmp)/((kon* tmp) + koff);
	      fx *= (1.0 - exp(-((kon* tmp) + koff)*ds->xd[i]));
	      ds->yd[i] = fx;
	      op->need_to_refresh = 1;
	    }	
	  ds = NULL;
	}

    }
  refresh_plot(pr, UNCHANGED);
  return D_O_K;

}



MENU *KonKoff_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Draw kon koff",do_draw_KonKoff ,NULL,0,NULL);
  add_item_to_menu(mn,"Fit kon koff from p(t) and P([O])", do_KonKoff_rescale_data_set,NULL,0,NULL);
  add_item_to_menu(mn,"Fit kon koff from 2ds P(t)", do_KonKoff_on_2_data_sets,NULL,0,NULL);
  add_item_to_menu(mn,"Fit Kd kon from p(t) and P([O])", do_KonKoff_Kd_kon,NULL,0,NULL);
  add_item_to_menu(mn,"Fit Kd kon for multiple ds", do_KonKoff_Kd_kon_multi_ds,NULL,0,NULL);




  return mn;
}

int	KonKoff_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_plot_treat_menu_item ( "KonKoff", NULL, KonKoff_plot_menu(), 0, NULL);
  return D_O_K;
}

int	KonKoff_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(plot_treat_menu, "KonKoff", NULL, NULL);
  return D_O_K;
}
#endif

