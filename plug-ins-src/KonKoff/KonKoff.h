#ifndef _KONKOFF_H_
#define _KONKOFF_H_

PXV_FUNC(int, do_KonKoff_rescale_plot, (void));
PXV_FUNC(MENU*, KonKoff_plot_menu, (void));
PXV_FUNC(int, do_KonKoff_rescale_data_set, (void));
PXV_FUNC(int, KonKoff_main, (int argc, char **argv));
#endif

