%{

#include <stdio.h>
#include <string.h>
typedef struct option
{      char name[64];
       char english_name[64];
       char validation[32];	
       char note[8];
       char ECTS[32];
}Option;

char *premier="\n \\documentclass[a4,12pt]{article} \n \\usepackage[latin1]{inputenc} \n \\usepackage[french]{babel} \n \\usepackage[dvips]{graphics} \n \\usepackage{fancybox}\n \\pagestyle{empty} \n \\begin{document}";

char *new_student = "\n \\begin{minipage}{3cm} \n \\includegraphics{chou.eps} \n \\end{minipage} \n \\qquad \n \\begin{minipage}{5cm} \n \\begin{tabular}{c}$\\cal FORMATION~INTERUNIVERSITAIRE $\\\\$\\cal DE~PHYSIQUE $\\\\Ecole Normale Sup\\'erieure\\\\ Universit\\'e Pierre et Marie Curie\\\\ Universit\\'e Denis Diderot \\\\Universit\\'e Paris Sud \\end{tabular} \\end{minipage} \n \\vskip 1cm \\centerline{\\doublebox{\\begin{minipage}{10cm}\\begin{center}\\Large RELEV\\'E DE NOTES\\\\ PREMIER SEMESTRE M1 DE PHYSIQUE\\end{center}\\end{minipage}}}\n \\vskip 1 cm";


#define getnote(colonne,number)	if (n_col== colonne) sprintf(opt[number].note,"%s",yytext)
#define getname(colonne,number)	if (n_col== colonne) sprintf(opt[number].name,"%s",yytext)
#define getvalidation(colonne,number)	if (n_col== colonne) {sprintf(opt[number].validation,"%s",yytext);}
#define getECTS(colonne,number) if (n_col==colonne) sprintf(opt[number].ECTS,"(%s ECTS)",yytext)

Option opt[32];
char moyenne[8];
char texte[1500];
char eng_text[1500];
char student_name[32];
char student_first_name[32];
int n_lines = 0;
int n_col = 0;
int i=0,j=0, k=0, m;
int link = 0;
%}
%x recover_classes_name
%x is_promotion
%x is_good_part
%%

"Promotion" BEGIN(is_promotion);
<is_promotion>"\n" {
			
			strcat(texte,premier);	
			BEGIN(INITIAL);
			BEGIN(is_good_part);
			
			sprintf(opt[0].name ,"Structure fondamentale de la mati\\`ere");
			sprintf(opt[0].english_name ,"Fundamental structure of matter");
			sprintf(opt[0].ECTS,"(6 ECTS)");

			sprintf(opt[1].name,"Th\\'eorie quantique des champs");
			sprintf(opt[1].english_name ,"Quantum field theory");
			sprintf(opt[1].ECTS,"(6 ECTS)");

			sprintf(opt[2].name,"Ph\\'enom\\`enes Non lin\\'eaires et syst\\`emes dynamiques");
			sprintf(opt[2].english_name ,"Non linear physics");
			sprintf(opt[2].ECTS , "(3 ECTS)");	

	    		sprintf(opt[3].name,"Hydrodynamics");
	    		sprintf(opt[3].english_name,"Hydrodynamics");
			sprintf(opt[3].ECTS , "(6 ECTS)");

			sprintf(opt[4].name,"Physique Exp\\'erimentale");
	    		sprintf(opt[4].english_name,"Experimental physics");
			sprintf(opt[4].ECTS , "(6 ECTS)");

			sprintf(opt[5].name,"Physique pour la biologie");
	    		sprintf(opt[5].english_name,"Physics for biology");
			sprintf(opt[5].ECTS , "(6 ECTS)");

			sprintf(opt[6].name,"Projet bibliographique");	
	    		sprintf(opt[6].english_name,"Bibliography project");
			sprintf(opt[6].ECTS , "(6 ECTS)");

			sprintf(opt[7].name,"Coh\\'erence quantique et dissipation");
	    		sprintf(opt[7].english_name,"Quantum coherence and dissipation");
			sprintf(opt[7].ECTS, "(3 ECTS)");

			sprintf(opt[8].name,"Information quantique");
	    		sprintf(opt[8].english_name,"Quantum theory");
			sprintf(opt[8].ECTS ,"(3 ECTS)");

			sprintf(opt[9].name,"Introduction \\`a la relativit\\'e g\\'en\\'erale");
	    		sprintf(opt[9].english_name,"Introduction to general relativity");
			sprintf(opt[9].ECTS , "(3 ECTS)");

			sprintf(opt[10].name,"Dynamics of Astrophysical Gases");
	    		sprintf(opt[10].english_name,"Dynamics of Astrophysical Gases");
			sprintf(opt[10].ECTS , "(3 ECTS)");

			sprintf(opt[11].name,"Physique num\\'erique :\\\\algorithmes et calculs en m\\'ecanique statistique");
			//sprintf(opt[11].english_name,"Numerical physics and statistical mechanics");
			sprintf(opt[11].ECTS , "(3 ECTS)");

			sprintf(opt[12].name,"Physique num\\'erique : \\\\approche num\\'erique en physique macroscopique");
	    		//sprintf(opt[12].english_name,"Numerical physics and macroscopic physics");
			sprintf(opt[12].ECTS , "(3 ECTS)");

			sprintf(opt[62].name,"Stage long en laboratoire");
			sprintf(opt[62].english_name,"Long term lab internship");
			sprintf(opt[62].ECTS , "(30 ECTS)");
		}

<is_good_part>[^;\n]*		{ 
		
//		printf("colo %d yytext %s",n_col,yytext);
		if (n_col==2) {sprintf(student_name," %s ",yytext);}
		if (n_col==3) {sprintf (student_first_name," %s ",yytext);}
		/*Notes de langues*/
		getnote(4,30);
		getname(5,30);
		getnote(6,31);
		getname(7,31);

		getnote(55,62);/*Note stage long*/
		if (n_col==56) sprintf(moyenne,"%s",yytext);

		getnote(34,25);
		getECTS(35,25);
		getvalidation(36,25);
		getname(37,25);
		
		getnote(38,26);
		getECTS(39,26);
		getvalidation(40,26);
		getname(41,26);

		getnote(42,27);
		getECTS(43,27);
		getvalidation(44,27);
		getname(45,27);
		
		getnote(46,28);
		getECTS(47,28);
		getvalidation(48,28);
		getname(49,28);
		
		for (i=8,m=0; m<24 && i < 33;m++,i++)
		{
			if (yytext != "\0")
			{
				getnote(i,m);
				i++;
				getvalidation(i,m);
			}
			else
			{
				if ((n_col - 8) %2 == 0)  sprintf(opt[n_col-8].validation, "0");
			}
		}

}
<is_good_part>; {
			n_col++;
	//		printf("ncol %d",n_col);
			//if ((n_col >=10) && (n_col%2 == 1)) j++;
		}

<is_good_part>"\n" {
		   n_col=0; 
		   n_lines++; 
		   
		   
		   strcat(texte,"\n");
 		   strcat(texte,new_student);
   		   
		   strcat(texte,student_first_name);
   		   strcat(texte,student_name); 

		   strcat(texte,"a obtenu durant le premier semestre de son (M1) de physique les notes suivantes :\n \\bigskip \\noindent \\begin{enumerate}\n"); 


		   strcat(texte,"\\item[]");
   		   strcat(texte,opt[30].name);
   		   strcat(texte,"\\hfill ");
   		   strcat(texte,opt[30].note);
		   strcat(texte,"(3 ECTS)");
		   strcat(texte,"\n");
   		   strcat(texte,"\\item[]");
   		   strcat(texte,opt[31].name);
		   strcat(texte,"\\hfill ");
 		   strcat(texte,opt[31].note);
		   strcat(texte,"(3 ECTS)");
		   strcat(texte,"\n");

		   for (k=0; k < 30; k++)
   		       { 
		       	  if (strcmp(opt[k].validation,"1")==0 || strcmp(opt[k].validation,"1,0")==0 )
			     			{
			//				printf("k=%d valid =%s",k,opt[k].validation);
							strcat(texte,"\\item[]");
							strcat(texte,opt[k].name);
							strcat(texte,"\\hfill ");
							strcat(texte,opt[k].note);
							strcat(texte,opt[k].ECTS);
							strcat(texte,"\n");
						} 
			}
		//strcat(texte,"\\item[]");
		//strcat(texte,opt[62].name);
		//strcat(texte,"\\hfill ");
		//strcat(texte,opt[62].note);
		//strcat(texte,opt[62].ECTS);
		strcat(texte,"\n");
        	strcat(texte,"\\end{enumerate}\n");

	        if (atof(moyenne) >= 10)
		{
			strcat(texte,student_first_name);
   	        	strcat(texte,student_name); 
	        	strcat(texte," a obtenu son M1 avec la note moyenne de ");
			strcat(texte,moyenne);

			strcat(texte," avec la mention ");
			if (atof(moyenne) <12) strcat(texte,"PASSABLE");
			if (atof(moyenne) <14 && atof(moyenne) >= 12) strcat(texte,"ASSEZ BIEN");
			if (atof(moyenne) <16 && atof(moyenne) >=14) strcat(texte,"BIEN");
			if (atof(moyenne) >= 16 ) strcat(texte,"TRES BIEN");
			
 		}
		strcat(texte,"\\bigskip \n \\vskip 1cm \n \\hfill \\begin{minipage}{8cm}\\begin{center}{\\bf Jean-François Allemand}\\newline Professeur responsable du parcours FIP \\end{center}\\end{minipage} \n \\newpage");

		   printf (texte);

		   sprintf(texte,"\0");

		   for (j=0; j< 29; j++)sprintf(opt[j].validation,"0");
		   j=0;
		   		  	
		  }		  
<<EOF>> 	  {
			strcat(texte,"\n \\end{document}");
			printf(texte);
			yyterminate();
		  }
			
%%


int main( argc, argv )
int argc;
char **argv;
{

    ++argv, --argc;  /* skip over program name */
    if ( argc > 0 )
            yyin = fopen( argv[0], "r" );
    else
              printf("");
            
    
    yylex();
    return 0;
}