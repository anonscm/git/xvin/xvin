#pragma once
PXV_FUNC(int, do_mlfi_rescale_plot, (void));
PXV_FUNC(MENU*, mlfi_plot_menu, (void));
PXV_FUNC(int, do_mlfi_rescale_data_set, (void));
PXV_FUNC(int, mlfi_main, (int argc, char **argv));
