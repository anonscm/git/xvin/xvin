/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _MLFI_C_
#define _MLFI_C_

#include <allegro.h>
#include "winalleg.h"
# include "xvin.h"
#include <pthread.h>

/* If you include other regular header do it here*/ 
#include "ziAPI.h"
#ifdef _WIN32
#include <windows.h>
#define PRId64 "d"
#define PRIu64 "u"
#define PRsize_t "I"
#define PRptrdiff_t "I"
#else
#include <inttypes.h>
#define PRsize_t "z"
#define PRptrdiff_t "t"
#include <string.h>
#include <unistd.h>
#endif


#define DEMOD_COUNT 4
#define PID_COUNT 4


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "mlfi.h"
//# include "ziUtils.h"


struct MyZIgenData {
  ZIConnection conn;
  char serverAddress[128] = {0};
  char node_name[16][128] = {0};

  uint64_t lastTimestamp[16];
  uint64_t totalSampleCount[16];
  double dataTransferRate[16];
  uint64_t expectedTimestampDiff[16];
  uint64_t eventsWithDataloss[16];
  int ds_nb[16];
  ZIIntegerData clockbase;
  const char *deviceId;
};
typedef struct MyZIgenData MyZIgenData;

MyZIgenData GenD;


/// The structure used to hold data for a single demodulator sample
struct MyZIDemodSample {
  /// inode => eventPath
  int inode;
  /// The timestamp at which the sample has been measured.
  ZITimeStamp timeStamp;  // MFFI one
  /// X part of the sample.
  float x;
  /// Y part of the sample.
  float y;

  /// oscillator frequency at that sample.
  float frequency;
  /// oscillator phase at that sample.
  float phase;

  /// the current bits of the DIO.
  uint32_t dioBits;

  /// trigger bits
  uint32_t trigger;

  /// value of Aux input 0.
  float auxIn0;
  /// value of Aux input 1.
  float auxIn1;
  unsigned char n_read;
  unsigned char new;
  unsigned char not_treated;
  int n_pack;
  int n_rec;
  long stimestamp;        // PC one  
};
typedef struct MyZIDemodSample MyZIDemodSample;


# define N_PACK 16384
MyZIDemodSample packet[N_PACK];
int i_pack_r = 0;  // packet recieved in rolling buffer [0, N_PACK[, i_packet_r is the next packet to fill-in
int ai_pack_r = 0; // packet recieved in absolute number
int i_pack_t = 0;  // packet treated in rolling buffer [0, N_PACK[
int ai_pack_t = 0; // packet treated in absolute number
int pack_overrun = 0;  // packet written before treated
int grabbing = 1;
char warning_in_thread[256] = {0};

ZITimeStamp starting_timeStamp = 0;

pthread_t thread_mlfi = 0;
O_p *opm = NULL;


static void *grab_mlfi_data(void* data)
{
    ZIEvent* Event;
    /* Allocate ZIEvent in heap memory instead of getting it from stack will secure against stack overflows 
       especially in windows.
    */
    if ((Event = ziAPIAllocateEventEx()) == NULL){
	win_printf("[ERROR] Can't allocate memory for Event.\n");
    }

    int n = 0, inode = 0;
    float tx, ty;
    time_t t_start = time(NULL);
    double poll_duration = 200.0, tS, tS0 = 0;  // seconds
    while (grabbing) {
      n++;
      // Poll for data with timeout.
      if (ziAPIPollDataEx(GenD.conn, Event, 1000) == ZI_INFO_SUCCESS)
	{
	  switch (Event->valueType) {
	  case ZI_VALUE_TYPE_DEMOD_SAMPLE:
	    {
	      for (inode=0; inode < DEMOD_COUNT + PID_COUNT; inode++)
		if (stricmp(node_name[inode],(const char*)Event->path) == 0) break;
	      if (inode < DEMOD_COUNT + PID_COUNT)
		{
		  // Check for sampleloss between polled events.
		  if (GenD.lastTimestamp[inode] != 0) {
		    uint64_t timestampDiff = Event->value.demodSample[0].timeStamp - GenD.lastTimestamp[inode];
		    GenD.lastTimestamp[inode] = Event->value.demodSample[Event->count - 1].timeStamp;
		    if (timestampDiff != GenD.expectedTimestampDiff[inode]) {
		      snprintf(warning_in_thread,sizeof(warning_in_thread),"[WARN] event %d sampleloss detected between events from %s.\n",n,Event->path);
		      GenD.eventsWithDataloss[inode]++;
		      GenD.lastTimestamp[inode] = Event->value.demodSample[Event->count - 1].timeStamp;
		      break;
		    }
		  }
		  // Check for sampleloss between all samples within the received event
		  for (i = 1; i < (int)Event->count; ++i) {
		    uint64_t timestampDiff = Event->value.demodSample[i].timeStamp - Event->value.demodSample[i-1].timeStamp;
		    if (timestampDiff != GenD.expectedTimestampDiff[inode]) {
		      snprintf(warning_in_thread,sizeof(warning_in_thread),"[WARN] sampleloss detected within %s.\n",Event->path);
		      GenD.eventsWithDataloss[inode]++;
		      break;
		    }
		  }
		  for (i = 0; i < Event->count; i++)
		    {
		      packet[i_pack_r].stimestamp = my_uclock();
		      pack_overrun += (packet[i_pack_r].not_treated) ? 1 : 0;
		      packet[i_pack_r].iode = inode;
		      packet[i_pack_r].x = Event->value.demodSample[i].x;
		      packet[i_pack_r].y = Event->value.demodSample[i].y;
		      packet[i_pack_r].timeStamp = Event->value.demodSample[i].timeStamp;
		      if (ai_pack_r == 0) starting_timeStamp = packet[i_pack_r].timeStamp;

		      packet[i_pack_r].frequency = Event->value.demodSample[i].frequency;
		      packet[i_pack_r].phase = Event->value.demodSample[i].phase;
		      packet[i_pack_r].dioBits = Event->value.demodSample[i].dioBits;
		      packet[i_pack_r].trigger = Event->value.demodSample[i].trigger;
		      packet[i_pack_r].auxIn0 = Event->value.demodSample[i].auxIn0;
		      packet[i_pack_r].auxIn1 = Event->value.demodSample[i].auxIn1;
		      packet[i_pack_r].new = 1;
		      packet[i_pack_r].not_treated = 1;
		      ai_pack_r++;
		      i_pack_r++;
		      i_pack_r = (i_pack_r < N_PACK) ? i_pack_r : 0;
		    }
		  GenD.totalSampleCount[inode] += Event->count;
		}
	      else snprintf(warning_in_thread,sizeof(warning_in_thread),"Node %s not recognized!",Event->path);
	      // The fields of the demodulator sample can be accessed as following.
	      // for (size_t i = 0; i < 1; ++i) {
	      //   printf("[INFO]    - sample %" PRsize_t "d TS = %" PRsize_t "d, X = %e, Y = %e, auxin0 = %e, auxin1 = %e, bits = %d\n",
	      //          i, Event->value.demodSample[i].timeStamp, Event->value.demodSample[i].x, Event->value.demodSample[i].y,
	      //          Event->value.demodSample[i].auxIn0, Event->value.demodSample[i].auxIn1, Event->value.demodSample[i].dioBits);
	      // }
	    }
	    break;
	  case ZI_VALUE_TYPE_DOUBLE_DATA:
	    snprintf(warning_in_thread,sizeof(warning_in_thread),"[INFO] Poll returned double data from %s.\n",Event->path);
	    break;
	  case ZI_VALUE_TYPE_INTEGER_DATA:
	    snprintf(warning_in_thread,sizeof(warning_in_thread),"[INFO] Poll returned integer data from %s.\n",Event->path);
	    break;
	  case ZI_VALUE_TYPE_DOUBLE_DATA_TS:
	    {
	      // API Level >= 4 only: Double data with a timestamp, e.g., /devX/pids/0/stream/value
	      // std::cout << "[INFO] Poll returned double data with timestamps from " << Event->path << ".\n";
	      for (inode=0; inode < DEMOD_COUNT + PID_COUNT; inode++)
		if (stricmp(node_name[inode],(const char*)Event->path) == 0) break;
	      if (inode < DEMOD_COUNT + PID_COUNT)
		{
		  GenD.totalSampleCount[inode] += Event->count;
	  
		  if ((GenD.expectedTimestampDiff[inode] == 0) && (Event->count > 1)) {
		    // Due to #9093 (see above), we use the first measured timestamp delta as the reference for all subsequent
		    // samples.
		    GenD.expectedTimestampDiff[inode] = Event->value.doubleDataTS[1].timeStamp - Event->value.doubleDataTS[0].timeStamp;
		    GenD.dataTransferRate[inode] = (double)clockbase/expectedTimestampDiff[inode];
		    snprintf(warning_in_thread,sizeof(warning_in_thread),"[INFO] %s  measured rate %I64d ,measured timestamp delta: %I64d",Event->path
			     ,GenD.dataTransferRate[inode],GenD.expectedTimestampDiff[inode]);
		  }
		  // Check for sampleloss between polled events
		  if (GenD.lastTimestamp[inode] != 0) {
		    uint64_t timestampDiff = Event->value.doubleDataTS[0].timeStamp - lastTimestamp[inode];
		    GenD.lastTimestamp[inode] = Event->value.doubleDataTS[Event->count - 1].timeStamp;
		    if (timestampDiff != GenD.expectedTimestampDiff[inode]) {
		      snprintf(warning_in_thread,sizeof(warning_in_thread),"[WARN] event %d sampleloss detected between events from %s.\n", n, Event->path);
		      GenD.eventsWithDataloss[inode]++;
		      GenD.lastTimestamp[inode] = Event->value.doubleDataTS[Event->count - 1].timeStamp;
		      break;
		    }
		  }
		  // Check for sampleloss between polled events
		  for (i = 1; i < (int)Event->count; ++i) {
		    uint64_t timestampDiff = Event->value.doubleDataTS[i].timeStamp - Event->value.doubleDataTS[i-1].timeStamp;
		    if (timestampDiff != GenD.expectedTimestampDiff[inode]) {
		      snprintf(warning_in_thread,sizeof(warning_in_thread)"[WARN] sampleloss detected within %s.\n",Event->path);
		      GenD.eventsWithDataloss[inode]++;
		      break;
		    }
		  }
		}
	      else snprintf(warning_in_thread,sizeof(warning_in_thread),"Node %s not recognized!",Event->path);
	      break;
	    }
	  case ZI_VALUE_TYPE_INTEGER_DATA_TS:
	    // API Level >= 4 only: Integer data with a timestamp, e.g., a device setting such as /devX/
	    snprintf(warning_in_thread,sizeof(warning_in_thread),"[INFO] Poll returned integer data with timestamps from %s.\n",Event->path);
	    break;
	  case ZI_VALUE_TYPE_NONE:
	    snprintf(warning_in_thread,sizeof(warning_in_thread),"[INFO] No event was polled (sample count is %s.\n",Event->count);
	    break;
	  default:
	    snprintf(warning_in_thread,sizeof(warning_in_thread)"[WARN] The returned event has an unexpected ValueType..\n");
	    break;
	  }
	}
    }

}


int RawHid_grab_signal_idle_action(O_p *op, DIALOG *d)
{
  int i, j, nf;
  pltreg *pr;
  d_s *ds;
  double dt;
  char mes[64] = {0};
  static int pk = 0, first = 1;
  float tmp, decade, vref;
    
  (void)d;
  if(updating_menu_state != 0)	return D_O_K;		
  if ((pr = find_pr_in_current_dialog(NULL)) == NULL)		
    return win_printf_OK("cannot find pr");
  if (vt.n_sp <= 0) 
    {
      if (first)
	{
	  sprintf (mes,"V1024");
	  rawhid_send(0, mes, 64, 220);
	  first = 0;
	}
      return 0;
    }
  if (vt.i_sp < vt.n_sp) return 0; // we wait
  if (vt.n_rec <= pk) return 0;
  pk = vt.n_rec;
  nf = vt.n_sp;
  ds = op->dat[0];
  ds->nx = ds->ny = 0;
  dt = (double)(vt.duration)/nf;
  for (i = 0; i < nf; i++)
  {
    add_new_point_to_ds(ds,(float)i,(float)vt.vdata[i]);
  }
  for (i = 0; i < op->n_xu; i++)
    {
      if (op->xu[i]->type == IS_SECOND) break;
    }
  if (i == op->n_xu)
    create_attach_select_x_un_to_op(op, IS_SECOND, 0, (float)dt, -6, 0, "\\mu s");
  else
    {
      decade = op->xu[i]->decade + 6;
      tmp = (float)pow(10,-decade);
      op->xu[i]->dx = tmp*dt;
      //op->xu[i]->decade = -6;
    }
  set_op_x_unit_set(op, i);

  vref = (adc_ref) ? 3.3 : 1.2;
  for (i = 0, j = 1; i < adc_res; i++) j *= 2;
  vref /= j;
  for (i = 0; i < op->n_yu; i++)
    {
      if (op->yu[i]->type == IS_VOLT) break;
    }
  if (i == op->n_yu)
    create_attach_select_y_un_to_op(op, IS_VOLT, 0, vref, 0, 0, "V");
  else
    {
      decade = op->yu[i]->decade;
      tmp = (float)pow(10,-decade);
      op->yu[i]->dx = tmp*vref;
    }
  set_op_y_unit_set(op, i);



  set_ds_source(ds,"signal %d (%d)  Avg %d, Res %d , Gain %d, Speed %d, Samp %d, Ref %d"
		,n_vt,vt.n_rec, adc_avg, adc_res, adc_gain, adc_speed, adc_samp, adc_ref);
  set_plot_title(op, "\\stack{{signal %d (%d)}"
		 "{\\pt7   Avg %d, Res %d , Gain %d, Speed %d, Samp %d, Ref %d}}"
		 ,n_vt,vt.n_rec, adc_avg, adc_res, adc_gain, adc_speed, adc_samp, adc_ref);

  op->need_to_refresh = 1;
  sprintf (mes,"V1024");
  rawhid_send(0, mes, 64, 220);
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}	



	      ds = op->dat[inode];
	      if (inode == 0)
		{
		  if (ds->nx + Event->count >= ROLLING_BUFFER_SIZE)
		    {
		      for (i = 0; i < ROLLING_BUFFER_SIZE - Event->count; i++)
			{
			  ds->xd[i] = ds->xd[i + Event->count];
			  ds->yd[i] = ds->yd[i + Event->count]; 		      
			}
		      for (i = 0; i < Event->count; i++)
			{
			  tS = (double)Event->value.demodSample[i].timeStamp/clockbase;
			  ds->xd[ROLLING_BUFFER_SIZE - Event->count + i] = tS - tS0;
			  tx = Event->value.demodSample[i].x;
			  ty = Event->value.demodSample[i].y;
			  ds->yd[ROLLING_BUFFER_SIZE - Event->count + i] =  1000* sqrt(tx*tx+ty*ty);
			}
		      ds->nx = ds->ny = ROLLING_BUFFER_SIZE;		      
		    }
		  else
		    {
		      for (i = 0; i < Event->count; i++)
			{
			  tS = (double)Event->value.demodSample[i].timeStamp/clockbase;
			  if (tS0 == 0) tS0 = tS;
			  ds->xd[ds->nx] = tS - tS0;
			  tx = Event->value.demodSample[i].x;
			  ty = Event->value.demodSample[i].y;
			  ds->yd[ds->nx] =  1000* sqrt(tx*tx+ty*ty);
			  ds->nx = (ds->nx < ROLLING_BUFFER_SIZE) ? ds->nx+1 : ROLLING_BUFFER_SIZE;
			}
		    }
		  op->need_to_refresh = 1;    
		  refresh_plot(pr, pr->n_op-1);
		}
	      else
		{
		  for (i = 0; i < Event->count; i++)
		    {
		      add_new_point_to_ds(ds, Event->value.demodSample[i].x, Event->value.demodSample[i].y);
		    }
		}




int do_mlfi_hello(void)
{
  int i;
  ZIResult_enum retVal;
  ZIConnection conn;
  char* errBuffer;
  char serverAddress[128] = {0};

  if(updating_menu_state != 0)	return D_O_K;


  // Initialize ZIConnection.
  retVal = ziAPIInit(&conn);
  if (retVal != ZI_INFO_SUCCESS) {
    ziAPIGetError(retVal, &errBuffer, NULL);
    win_printf("Can't init Connection: %s\n", errBuffer);
    return 0;
  }


  // Connect to the Data Server: Use port 8005 for the HF2 Data Server, use
  // 8004 for the UHF and MF Data Servers. HF2 only support ZI_API_VERSION_1,
  // see the LabOne Programming Manual for an explanation of API Levels.
  //const char serverAddress[] = "localhost";
  i = win_scanf("Serveur name (may be localhost mf-dev3200 or 192.168.50.2):\n"
		"%32s",serverAddress);
  if (i == WIN_CANCEL) return 0;
  retVal = ziAPIConnectEx(conn, serverAddress, 8004, ZI_API_VERSION_5, NULL);
  if (retVal != ZI_INFO_SUCCESS) {
    ziAPIGetError(retVal, &errBuffer, NULL);
    win_printf("Error, can't connect to the Data Server: `%s`.\n", errBuffer);
  } else {
    /*
      Do something using ZIConnection here.
    */
    win_printf("Do something using ZIConnection here.");
    // Since ZIAPIDisconnect always returns ZI_INFO_SUCCESS
    // no error handling is required.
    ziAPIDisconnect(conn);
  }

  // Since ZIAPIDestroy always returns ZI_INFO_SUCCESS
  // no error handling is required.
  ziAPIDestroy(conn);
  return 0;
}


bool isError(ZIResult_enum resultCode) {
  if (resultCode != ZI_INFO_SUCCESS) {
    char* message;
    ziAPIGetError(resultCode, &message, NULL);
    win_printf("Error: %s\n", message);
    return true;
  }
  return false;
}

void checkError(ZIResult_enum resultCode) {
  if (resultCode != ZI_INFO_SUCCESS) {
    char* message;
    ziAPIGetError(resultCode, &message, NULL);
    win_printf(message);
  }
}

char *checkError_str(ZIResult_enum resultCode)
{
  if (resultCode != ZI_INFO_SUCCESS)
    {
      char* message;
      ziAPIGetError(resultCode, &message, NULL);
      return strdup(message);
    }
  else return NULL;
}



/// Create a Data Server session for the device and connect it on a physical interface (if not previously connected).
/** This function is a helper function to create an API session for the specified device on an appropriate Data
    Server. It uses Zurich Instruments Device Discovery to find the specified device on the local area network and
    determine which Data Server may be used to connect to it. The API Level used for the connection is the minimum Level
    supported by the device and by the input argument maxSupportedApilevel.

    @param[in]  conn                  The initialised ::ZIConnection which will be associated with the created API 
                                      session.
    @param[in]  deviceAddress         The device address for which to create the API session, e.g., dev2006 or
                                      UHF-DEV2006 (as displayed on the back panel of the instrument).
    @param[in]  maxSupportedApilevel  A valid API Level (ZIAPIVersion_enum) that specifies the maximum API Level
                                      supported by the client code that will work with the API session.
    @param[out] deviceId              The device's ID as reported by ::ziAPIDiscoveryFind.
 */
int ziCreateAPISession(ZIConnection conn, char* deviceAddress, ZIAPIVersion_enum maxSupportedApilevel,
                       const char** deviceId) {
  if (!isError(ziAPIDiscoveryFind(conn, deviceAddress, deviceId))) {
    char message[1024];
    {
      const char *serveraddress;
      ZIIntegerData discoverable = 0;
      ZIIntegerData serverport = 0;
      ZIIntegerData apilevel;
      const char* connected;
      // First check that the device is discoverable on the network or another interface.
      checkError(ziAPIDiscoveryGetValueI(conn, *deviceId, "discoverable", &discoverable));
      if (discoverable != 1) {
        snprintf(message, sizeof(message), "`%s` is not discoverable.", *deviceId);
        win_printf(message);
        return 1;
      } else {
        snprintf(message, sizeof(message), "Discovered device `%s`.", *deviceId);
        win_printf(message);
      }
      // The device is discoverable - get the discovery properties required to
      // create a connnection via a Data Server.
      checkError(ziAPIDiscoveryGetValueS(conn, *deviceId, "serveraddress", &serveraddress));
      checkError(ziAPIDiscoveryGetValueI(conn, *deviceId, "serverport", &serverport));
      checkError(ziAPIDiscoveryGetValueI(conn, *deviceId, "apilevel", &apilevel));
      checkError(ziAPIDiscoveryGetValueS(conn, *deviceId, "connected", &connected));

      ZIIntegerData apilevelConnection = apilevel;
      if (maxSupportedApilevel < apilevel) {
        apilevelConnection = maxSupportedApilevel;
      }
      // Create an API Session to the Data Server reported by discovery.
      snprintf(message, sizeof(message), "Creating an API Session with the Data Server running on %s on port %I64d with API Level %I64d", serveraddress, serverport, apilevel);
      win_printf(message);
      //checkError(ziAPIConnectEx(conn, serveraddress, serverport,
      //                        static_cast<ZIAPIVersion_enum>(apilevelConnection), NULL));

      // Try to connect the device to the Data Server if not already.
      if (strcmp(connected, "\0")) {
        snprintf(message, sizeof(message), "Device is already connected on interface `%s`.", connected);
        ziAPIWriteDebugLog(0, message);
      } else {
        const char* interfaces;
        checkError(ziAPIDiscoveryGetValueS(conn, *deviceId, "interfaces", &interfaces));
        snprintf(message, sizeof(message), "Device is not connected, available interfaces: `%s`.", interfaces);
        win_printf(message);
        // Get the first available interface.
        char* saveptr;
        char* interfaceConnection = strtok_r((char*)interfaces, "\n", &saveptr);
        if (strcmp(interfaceConnection, "\0") == 0) {
          // This should not happen if the device is discoverable.
          snprintf(message, sizeof(message),
                   "Error: The device `%s` is not connected but could not read an available interface.\n", *deviceId);
          win_printf(message);
          return 1;
        }
        snprintf(message, sizeof(message), "Will try to connect on: `%s`.\n", interfaceConnection);
        win_printf(message);
        checkError(ziAPIConnectDevice(conn, *deviceId, interfaceConnection, NULL));
      }
      return 0;
    } 
  } else {
    return 1;
  }

}


# define ROLLING_BUFFER_SIZE 4096

int do_mlfi_read_AuxIn(void)
{
  int i;
  ZIResult_enum retVal;
  ZIConnection conn;
  char* errBuffer;
  char serverAddress[128] = {0};
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds;

  
  if(updating_menu_state != 0)	return D_O_K;


  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  if ((op = create_and_attach_one_plot(pr, ROLLING_BUFFER_SIZE, ROLLING_BUFFER_SIZE, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  op->dat[0]->nx =   op->dat[0]->ny = 0;
  for (i = 1; i < DEMOD_COUNT + PID_COUNT; i++)
    {
      if ((ds = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      ds->nx = ds->ny = 0;
    }
  refresh_plot(pr, pr->n_op-1);  
  // Initialize ZIConnection.
  retVal = ziAPIInit(&conn);
  if (retVal != ZI_INFO_SUCCESS) {
    ziAPIGetError(retVal, &errBuffer, NULL);
    win_printf("Can't init Connection: %s\n", errBuffer);
    return 0;
  }

  


  // Connect to the Data Server: Use port 8005 for the HF2 Data Server, use
  // 8004 for the UHF and MF Data Servers. HF2 only support ZI_API_VERSION_1,
  // see the LabOne Programming Manual for an explanation of API Levels.
  //const char serverAddress[] = "localhost";
  snprintf(serverAddress,sizeof(serverAddress),"192.168.53.70");
  i = win_scanf("Serveur name (may be localhost mf-dev3409 or 192.168.53.70):\n"
		"%32s",serverAddress);
  if (i == WIN_CANCEL) return 0;
  retVal = ziAPIConnectEx(conn, serverAddress, 8004, ZI_API_VERSION_5, NULL);
  if (retVal != ZI_INFO_SUCCESS) {
    ziAPIGetError(retVal, &errBuffer, NULL);
    win_printf("Error, can't connect to the Data Server: `%s`.\n", errBuffer);
  } else {
    char deviceAddress[] = "dev3409";
    const char *deviceId;
    if (ziCreateAPISession(conn, deviceAddress, ZI_API_VERSION_5, &deviceId) != 0)
      return  win_printf("Error, can't create APISession\n");
    win_printf("DeviceId = %s\n",deviceId);

    ZIAuxInSample AuxInSample[3];
    char nodePath[1024];
    ZIResult_enum RetVal;
    char* ErrBuffer;
    
    // Get the instrument's clockbase to convert timestamps (in ticks) to seconds
    snprintf(nodePath, sizeof(nodePath), "/%s/auxins/0/sample", deviceId);
    for (i = 0; i < 3; i++)
      RetVal = ziAPIGetAuxInSample(conn, nodePath, AuxInSample + i);

    if (RetVal!= ZI_INFO_SUCCESS) {
    ziAPIGetError(RetVal, &ErrBuffer, NULL);
    win_printf("Error, can't get Parameter: %s\n", ErrBuffer);
    } else {
      win_printf("0-> TimeStamp = %I64d, ch0=%f, ch1=%f\n"
		 "1-> TimeStamp = %I64d, ch0=%f, ch1=%f\n"
		 "2-> TimeStamp = %I64d, ch0=%f, ch1=%f\n",
		 AuxInSample[0].timeStamp, AuxInSample[0].ch0, AuxInSample[0].ch1,
		 AuxInSample[1].timeStamp, AuxInSample[1].ch0, AuxInSample[1].ch1,
		 AuxInSample[2].timeStamp, AuxInSample[2].ch0, AuxInSample[2].ch1);
    }
  }
  return 0;
}



int do_mlfi_test(void)
{
  int i;
  ZIResult_enum retVal;
  ZIConnection conn;
  char* errBuffer;
  char serverAddress[128] = {0};
  char node_name[16][128] = {0};

  uint64_t lastTimestamp[16];
  uint64_t totalSampleCount[16];
  double dataTransferRate[16];
  uint64_t expectedTimestampDiff[16];
  uint64_t eventsWithDataloss[16];
  
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds;

  
  if(updating_menu_state != 0)	return D_O_K;


  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");

  if ((op = create_and_attach_one_plot(pr, ROLLING_BUFFER_SIZE, ROLLING_BUFFER_SIZE, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  op->dat[0]->nx =   op->dat[0]->ny = 0;
  for (i = 1; i < DEMOD_COUNT + PID_COUNT; i++)
    {
      if ((ds = create_and_attach_one_ds(op, 16, 16, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      ds->nx = ds->ny = 0;
    }
  refresh_plot(pr, pr->n_op-1);  
  // Initialize ZIConnection.
  retVal = ziAPIInit(&conn);
  if (retVal != ZI_INFO_SUCCESS) {
    ziAPIGetError(retVal, &errBuffer, NULL);
    win_printf("Can't init Connection: %s\n", errBuffer);
    return 0;
  }


  // Connect to the Data Server: Use port 8005 for the HF2 Data Server, use
  // 8004 for the UHF and MF Data Servers. HF2 only support ZI_API_VERSION_1,
  // see the LabOne Programming Manual for an explanation of API Levels.
  //const char serverAddress[] = "localhost";
  snprintf(serverAddress,sizeof(serverAddress),"192.168.53.70");
  i = win_scanf("Serveur name (may be localhost mf-dev3409 or 192.168.53.70):\n"
		"%32s",serverAddress);
  if (i == WIN_CANCEL) return 0;
  retVal = ziAPIConnectEx(conn, serverAddress, 8004, ZI_API_VERSION_5, NULL);
  if (retVal != ZI_INFO_SUCCESS) {
    ziAPIGetError(retVal, &errBuffer, NULL);
    win_printf("Error, can't connect to the Data Server: `%s`.\n", errBuffer);
  } else {
    char deviceAddress[] = "dev3409";
    const char *deviceId;
    if (ziCreateAPISession(conn, deviceAddress, ZI_API_VERSION_5, &deviceId) != 0)
      return  win_printf("Error, can't create APISession\n");
    win_printf("DeviceId = %s\n",deviceId);


    char nodePath[1024];

    // Get the instrument's clockbase to convert timestamps (in ticks) to seconds
    snprintf(nodePath, sizeof(nodePath), "/%s/clockbase", deviceId);
    ZIIntegerData clockbase;
    checkError(ziAPIGetValueI(conn, nodePath, &clockbase));

    win_printf("Clock is set to %ld \n",clockbase);

    for (i=0; i < DEMOD_COUNT; i++)
      {
	snprintf(node_name[i], sizeof(node_name[i]), "/%s/demods/%u/sample", deviceId,i);
	checkError(ziAPISubscribe(conn, node_name[i]));
	win_printf("nodePath=\n%s",node_name[i]);
        lastTimestamp[i] = 0;
        totalSampleCount[i] = 0;
        eventsWithDataloss[i] = 0;
	
	ZIDoubleData rate;
	snprintf(nodePath, sizeof(nodePath), "/%s/demods/%d/rate", deviceId, i);
	checkError(ziAPIGetValueD(conn, nodePath, &rate));
	dataTransferRate[i] = rate;
        
	expectedTimestampDiff[i] = (rate > 0) ? ((double)clockbase/rate + 0.5) : 0;
	win_printf("Rate = %g, expectedTimestampDiff %ld",rate,expectedTimestampDiff[i]);
      }
    /*
    win_printf("Device 0: %s\nRate = %g, expectedTimestampDiff %ld\n"
	       "Device 1: %s\nRate = %g, expectedTimestampDiff %ld\n"
	       "Device 2: %s\nRate = %g, expectedTimestampDiff %ld\n"
	       "Device 3: %s\nRate = %g, expectedTimestampDiff %ld\n"
	       ,node_name[0],dataTransferRate[0],expectedTimestampDiff[0]
	       ,node_name[1],dataTransferRate[1],expectedTimestampDiff[1]
	       ,node_name[2],dataTransferRate[2],expectedTimestampDiff[2]
	       ,node_name[3],dataTransferRate[3],expectedTimestampDiff[3]	       
	       );
    */
    for (i=0; i < PID_COUNT; i++)
      {
	snprintf(node_name[DEMOD_COUNT+i], sizeof(node_name[DEMOD_COUNT+i]), "/%s/pids/%u/stream/value", deviceId, i);
	checkError(ziAPISubscribe(conn, node_name[DEMOD_COUNT+i]));
        lastTimestamp[DEMOD_COUNT+i] = 0;
        totalSampleCount[DEMOD_COUNT+i] = 0;
        eventsWithDataloss[DEMOD_COUNT+i] = 0;    

	ZIDoubleData pidStreamRate;
	snprintf(nodePath, sizeof(nodePath), "/%s/pids/%d/stream/effectiverate", deviceId, i);
	checkError(ziAPIGetValueD(conn, nodePath, &pidStreamRate));
        dataTransferRate[DEMOD_COUNT+i] = pidStreamRate;	
	//win_printf("pidStreamRate = %g",pidStreamRate);
	expectedTimestampDiff[DEMOD_COUNT+i] = (pidStreamRate > 0) ? ((double)clockbase/pidStreamRate + 0.5) : 0;
	win_printf("[INFO] %s is using rate %ld expected timestamp delta: %ld",node_name[DEMOD_COUNT+i]
		   ,dataTransferRate[DEMOD_COUNT+i],expectedTimestampDiff[DEMOD_COUNT+i]);

      }

	

    ZIEvent* Event;
    /* Allocate ZIEvent in heap memory instead of getting it from stack will secure against stack overflows 
       especially in windows.
    */
    if ((Event = ziAPIAllocateEventEx()) == NULL){
	win_printf("[ERROR] Can't allocate memory for Event.\n");
    }



    int n = 0, inode = 0;
    float tx, ty;
    time_t t_start = time(NULL);
    double poll_duration = 200.0, tS, tS0 = 0;  // seconds
    while (difftime(time(NULL), t_start) < poll_duration) {
      n++;
      // Poll for data with timeout.
      checkError(ziAPIPollDataEx(conn, Event, 1000));
      // if (Event->count > 0) {
      //   std::cout << "[INFO] Event contains " <<  Event->count << " samples from " << Event->path << ".\n";
      // }
      switch (Event->valueType) {
      case ZI_VALUE_TYPE_DEMOD_SAMPLE:
	{
	  for (inode=0; inode < DEMOD_COUNT + PID_COUNT; inode++)
	    if (stricmp(node_name[inode],(const char*)Event->path) == 0) break;
	  if (inode < DEMOD_COUNT + PID_COUNT)
	    {
	      // Check for sampleloss between polled events.
	      if (lastTimestamp[inode] != 0) {
		uint64_t timestampDiff = Event->value.demodSample[0].timeStamp - lastTimestamp[inode];
		lastTimestamp[inode] = Event->value.demodSample[Event->count - 1].timeStamp;
		if (timestampDiff != expectedTimestampDiff[inode]) {
		  win_printf("[WARN] event %d sampleloss detected between events from %s.\n",n,Event->path);
		  eventsWithDataloss[inode]++;
		  lastTimestamp[inode] = Event->value.demodSample[Event->count - 1].timeStamp;
		  break;
		}
	      }
	      // Check for sampleloss between all samples within the received event
	      for (i = 1; i < (int)Event->count; ++i) {
		uint64_t timestampDiff = Event->value.demodSample[i].timeStamp - Event->value.demodSample[i-1].timeStamp;
		if (timestampDiff != expectedTimestampDiff[inode]) {
		  win_printf("[WARN] sampleloss detected within %s.\n",Event->path);
		  eventsWithDataloss[inode]++;
		  break;
		}
	      }
	      ds = op->dat[inode];
	      if (inode == 0)
		{
		  if (ds->nx + Event->count >= ROLLING_BUFFER_SIZE)
		    {
		      for (i = 0; i < ROLLING_BUFFER_SIZE - Event->count; i++)
			{
			  ds->xd[i] = ds->xd[i + Event->count];
			  ds->yd[i] = ds->yd[i + Event->count]; 		      
			}
		      for (i = 0; i < Event->count; i++)
			{
			  tS = (double)Event->value.demodSample[i].timeStamp/clockbase;
			  ds->xd[ROLLING_BUFFER_SIZE - Event->count + i] = tS - tS0;
			  tx = Event->value.demodSample[i].x;
			  ty = Event->value.demodSample[i].y;
			  ds->yd[ROLLING_BUFFER_SIZE - Event->count + i] =  1000* sqrt(tx*tx+ty*ty);
			}
		      ds->nx = ds->ny = ROLLING_BUFFER_SIZE;		      
		    }
		  else
		    {
		      for (i = 0; i < Event->count; i++)
			{
			  tS = (double)Event->value.demodSample[i].timeStamp/clockbase;
			  if (tS0 == 0) tS0 = tS;
			  ds->xd[ds->nx] = tS - tS0;
			  tx = Event->value.demodSample[i].x;
			  ty = Event->value.demodSample[i].y;
			  ds->yd[ds->nx] =  1000* sqrt(tx*tx+ty*ty);
			  ds->nx = (ds->nx < ROLLING_BUFFER_SIZE) ? ds->nx+1 : ROLLING_BUFFER_SIZE;
			}
		    }
		  op->need_to_refresh = 1;    
		  refresh_plot(pr, pr->n_op-1);
		}
	      else
		{
		  for (i = 0; i < Event->count; i++)
		    {
		      add_new_point_to_ds(ds, Event->value.demodSample[i].x, Event->value.demodSample[i].y);
		    }
		}
	      totalSampleCount[inode] += Event->count;

	    }
	  else win_printf("Node %s not recognized!",Event->path);
	  // The fields of the demodulator sample can be accessed as following.
	  // for (size_t i = 0; i < 1; ++i) {
	  //   printf("[INFO]    - sample %" PRsize_t "d TS = %" PRsize_t "d, X = %e, Y = %e, auxin0 = %e, auxin1 = %e, bits = %d\n",
	  //          i, Event->value.demodSample[i].timeStamp, Event->value.demodSample[i].x, Event->value.demodSample[i].y,
	  //          Event->value.demodSample[i].auxIn0, Event->value.demodSample[i].auxIn1, Event->value.demodSample[i].dioBits);
	  // }
	}
	break;
      case ZI_VALUE_TYPE_DOUBLE_DATA:
	win_printf("[INFO] Poll returned double data from %s.\n",Event->path);
	break;
      case ZI_VALUE_TYPE_INTEGER_DATA:
	win_printf("[INFO] Poll returned integer data from %s.\n",Event->path);
	break;
      case ZI_VALUE_TYPE_DOUBLE_DATA_TS:
	{
	  // API Level >= 4 only: Double data with a timestamp, e.g., /devX/pids/0/stream/value
	  // std::cout << "[INFO] Poll returned double data with timestamps from " << Event->path << ".\n";
	  for (inode=0; inode < DEMOD_COUNT + PID_COUNT; inode++)
	    if (stricmp(node_name[inode],(const char*)Event->path) == 0) break;
	  if (inode < DEMOD_COUNT + PID_COUNT)
	    {
	      totalSampleCount[inode] += Event->count;
	  
	      if ((expectedTimestampDiff[inode] == 0) && (Event->count > 1)) {
		// Due to #9093 (see above), we use the first measured timestamp delta as the reference for all subsequent
		// samples.
		expectedTimestampDiff[inode] = Event->value.doubleDataTS[1].timeStamp - Event->value.doubleDataTS[0].timeStamp;
		dataTransferRate[inode] = (double)clockbase/expectedTimestampDiff[inode];
		win_printf("[INFO] %s  measured rate %I64d ,measured timestamp delta: %I64d",Event->path
			   ,dataTransferRate[inode],expectedTimestampDiff[inode]);
	      }
	      // Check for sampleloss between polled events
	      if (lastTimestamp[inode] != 0) {
		uint64_t timestampDiff = Event->value.doubleDataTS[0].timeStamp - lastTimestamp[inode];
		lastTimestamp[inode] = Event->value.doubleDataTS[Event->count - 1].timeStamp;
		if (timestampDiff != expectedTimestampDiff[inode]) {
		  win_printf("[WARN] event %d sampleloss detected between events from %s.\n", n, Event->path);
		  eventsWithDataloss[inode]++;
		  lastTimestamp[inode] = Event->value.doubleDataTS[Event->count - 1].timeStamp;
		  break;
		}
	      }
	      // Check for sampleloss between polled events
	      for (i = 1; i < (int)Event->count; ++i) {
		uint64_t timestampDiff = Event->value.doubleDataTS[i].timeStamp - Event->value.doubleDataTS[i-1].timeStamp;
		if (timestampDiff != expectedTimestampDiff[inode]) {
		  win_printf("[WARN] sampleloss detected within %s.\n",Event->path);
		  eventsWithDataloss[inode]++;
		  break;
		}
	      }
	    }
	  else win_printf("Node %s not recognized!",Event->path);
	  break;
	}
      case ZI_VALUE_TYPE_INTEGER_DATA_TS:
	// API Level >= 4 only: Integer data with a timestamp, e.g., a device setting such as /devX/
	win_printf("[INFO] Poll returned integer data with timestamps from %s.\n",Event->path);
	break;
      case ZI_VALUE_TYPE_NONE:
	win_printf("[INFO] No event was polled (sample count is %s.\n",Event->count);
	break;
      default:
	win_printf("[WARN] The returned event has an unexpected ValueType..\n");
	break;
      }
    }


    for (inode=0; inode < DEMOD_COUNT + PID_COUNT; inode++)
      {
	//if (totalSampleCount[inode] > 0)
	win_printf("%d events\nfor Node %s, total sample count %d",n,node_name[inode],totalSampleCount[inode]);
      }
    
    
    /*
      Do something using ZIConnection here.
    */
    win_printf("Do something using ZIConnection here.");
    // Since ZIAPIDisconnect always returns ZI_INFO_SUCCESS
    // no error handling is required.
    ziAPIDisconnect(conn);
    
  }

  // Since ZIAPIDestroy always returns ZI_INFO_SUCCESS
  // no error handling is required.
  ziAPIDestroy(conn);
  refresh_plot(pr, pr->n_op-1);
  return 0;
}
 

int do_prepare_mlfi_thread(void)
{
  int i;
  ZIResult_enum retVal;
  char* errBuffer;
  pltreg *pr = NULL;
  d_s *ds;

  
  if(updating_menu_state != 0)	return D_O_K;

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");
  // Initialize ZIConnection.
  retVal = ziAPIInit(&GenD.conn);
  if (retVal != ZI_INFO_SUCCESS) {
    ziAPIGetError(retVal, &errBuffer, NULL);
    win_printf("Can't init Connection: %s\n", errBuffer);
    return 0;
  }


  // Connect to the Data Server: Use port 8005 for the HF2 Data Server, use
  // 8004 for the UHF and MF Data Servers. HF2 only support ZI_API_VERSION_1,
  // see the LabOne Programming Manual for an explanation of API Levels.
  //const char serverAddress[] = "localhost";
  snprintf(serverAddress,sizeof(serverAddress),"192.168.53.70");
  i = win_scanf("Serveur name (may be localhost mf-dev3409 or 192.168.53.70):\n"
		"%32s",serverAddress);
  if (i == WIN_CANCEL) return 0;
  retVal = ziAPIConnectEx(GenD.conn, GenD.serverAddress, 8004, ZI_API_VERSION_5, NULL);
  if (retVal != ZI_INFO_SUCCESS) {
    ziAPIGetError(retVal, &errBuffer, NULL);
    win_printf("Error, can't connect to the Data Server: `%s`.\n", errBuffer);
  } else {
    char deviceAddress[] = "dev3409";
    if (ziCreateAPISession(GenD.conn, GenD.deviceAddress, ZI_API_VERSION_5, &GenD.deviceId) != 0)
      return  win_printf("Error, can't create APISession\n");
    //win_printf("DeviceId = %s\n",deviceId);


    char nodePath[1024];

    // Get the instrument's clockbase to convert timestamps (in ticks) to seconds
    snprintf(nodePath, sizeof(nodePath), "/%s/clockbase", GenD.deviceId);
    
    checkError(ziAPIGetValueI(GenD.conn, nodePath, &GenD.clockbase));

    win_printf("Clock is set to %ld \n",GenD.clockbase);



  if ((opm = create_and_attach_one_plot(pr, ROLLING_BUFFER_SIZE, ROLLING_BUFFER_SIZE, 0)) == NULL)
    return win_printf_OK("cannot create plot !");
  opm->dat[0]->nx =   opm->dat[0]->ny = 0;
  ds = opm->dat[0];
  refresh_plot(pr, pr->n_op-1);  


    
    for (i=0; i < DEMOD_COUNT; i++)
      {
	ZIDoubleData rate;	
	snprintf(GenD.node_name[i], sizeof(GenD.node_name[i]), "/%s/demods/%u/sample", GenD.deviceId,i);
	if (ziAPISubscribe(GenD.conn, GenD.node_name[i]) == ZI_INFO_SUCCESS)
	  {
	    GenD.lastTimestamp[i] = 0;
	    GenD.totalSampleCount[i] = 0;
	    GenD.eventsWithDataloss[i] = 0;
	    snprintf(GenD.nodePath, sizeof(GenD.nodePath), "/%s/demods/%d/rate", GenD.deviceId, i);
	    if (ziAPIGetValueD(GenD.conn, GenD.nodePath, &rate)  == ZI_INFO_SUCCESS)
	      GenD.dataTransferRate[i] = rate;
	    else rate = 0;
	    GenD.expectedTimestampDiff[i] = (rate > 0) ? ((double)GenD.clockbase/rate + 0.5) : 0;
	    if (ds == NULL)
	      {
		if ((ds = create_and_attach_one_ds(opm, ROLLING_BUFFER_SIZE, ROLLING_BUFFER_SIZE, 0)) == NULL)
		  return win_printf_OK("cannot create plot !");
	      }
	    ds->nx = ds->ny = 0;
	    GenD.ds_nb[i] = (opm->n_dat > 0) ? opm->n_dat-1 : 0;
	    set_ds_source(ds,"%s",GenD.node_name[i]);
	    ds = NULL;
	  }
      }
    for (i=0; i < PID_COUNT; i++)
      {
	ZIDoubleData pidStreamRate;	
	snprintf(GenD.node_name[DEMOD_COUNT+i], sizeof(GenD.node_name[DEMOD_COUNT+i]), "/%s/pids/%u/stream/value", deviceId, i);
	if (ziAPISubscribe(conn, GenD.node_name[DEMOD_COUNT+i]) == ZI_INFO_SUCCESS)
	  {
	    GenD.lastTimestamp[DEMOD_COUNT+i] = 0;
	    GenD.totalSampleCount[DEMOD_COUNT+i] = 0;
	    GenD.eventsWithDataloss[DEMOD_COUNT+i] = 0;    
	    snprintf(GenD.nodePath, sizeof(GenD.nodePath), "/%s/pids/%d/stream/effectiverate", GenD.deviceId, i);
	    if (ziAPIGetValueD(GenD.conn, GenD.nodePath, &pidStreamRate) == ZI_INFO_SUCCESS)
	      GenD.dataTransferRate[DEMOD_COUNT+i] = pidStreamRate;
	    else GenD.pidStreamRate = 0;	
	    expectedTimestampDiff[DEMOD_COUNT+i] = (pidStreamRate > 0) ? ((double)GenD.clockbase/pidStreamRate + 0.5) : 0;
	    if (ds == NULL)
	      {
		if ((ds = create_and_attach_one_ds(opm, ROLLING_BUFFER_SIZE, ROLLING_BUFFER_SIZE, 0)) == NULL)
		  return win_printf_OK("cannot create plot !");
	      }
	    ds->nx = ds->ny = 0;
	    GenD.ds_nb[DEMOD_COUNT+i] = (opm->n_dat > 0) ? opm->n_dat-1 : 0;
	    set_ds_source("%s",GenD.node_name[DEMOD_COUNT+i]);
	    ds = NULL;
	    
	  }
      }

	
    if (pthread_create (&thread_mlfi, NULL, grab_mlfi_data, NULL))
      win_printf_OK("Could not start MLFI grabbing thread!");

  }

  // Since ZIAPIDestroy always returns ZI_INFO_SUCCESS
  // no error handling is required.
  //  ziAPIDestroy(conn);
  refresh_plot(pr, pr->n_op-1);
  return 0;
}



MENU *mlfi_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Hello example", do_mlfi_hello,NULL,0,NULL);
  add_item_to_menu(mn,"test example", do_mlfi_test,NULL,0,NULL);
  add_item_to_menu(mn,"Read Aux Ins", do_mlfi_read_AuxIn,NULL,0,NULL);
  add_item_to_menu(mn,"Start data grabbing", do_prepare_mlfi_thread,NULL,0,NULL);  



  return mn;
}

int	mlfi_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;  
  add_plot_treat_menu_item ( "mlfi", NULL, mlfi_plot_menu(), 0, NULL);
  return D_O_K;
}

int	mlfi_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;    
  remove_item_to_menu(plot_treat_menu, "mlfi", NULL, NULL);
  return D_O_K;
}
#endif

