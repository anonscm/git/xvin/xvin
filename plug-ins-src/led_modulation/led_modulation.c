/*
 *    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
 */
#ifndef _LED_MODULATION_C_
#define _LED_MODULATION_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
//place here all headers from plugins already compiled
# include "../wlc/wlc.h"
# include "../nrutil/nrutil.h"

#undef _PXV_DLL
#define _PXV_DLL   __declspec(dllexport)
# include "led_modulation.h"
//place here other headers of this plugin 
# include "allegro.h"
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "led_modulation.h"

# define POISSON_MAK_K 20
float  LED_fp[POISSON_MAK_K], LED_sfp[POISSON_MAK_K];


double T = 298, xi = 50;

float mean_extension = 0, kx = 0, ky = 0, kz = 0;
float  dx2_0 = 0, dy2_0 = 0, dz2_0 = 0;
float camera_freq = 60;
int over_sampling = 128, sim_over = 8;
int shutter_period = 10, shutter_on_period = 2;
float  LED_poisson_freq = 2.0;
int led_freq = 2;
float led_amp = 1;
float betax, betay, betaz;
float taux, tauy, tauz;
float xnoise, ynoise, znoise;
//float x_bn, y_bn, z_bn;
//float x_bnt, y_bnt, z_bnt;
static long idum = 4587;

int convert_force_to_k(float applied_force, float bead_r, float DNA_L)
{
  float x;
  double alpha;
  double dt, tmpd;//, rot, zmag;

  alpha = applied_force;
  alpha *= xi/(1.38e-2 * T);
  x = extension_versus_alpha(alpha);
  mean_extension = DNA_L * x; 
  if (x > 0.1) 
    {
      kx = applied_force/mean_extension;  // pN/microns
      ky = applied_force/(mean_extension + bead_r);  // pN/microns
    }
  else 
    {
      kx = marko_siggia_improved_derivate(x);
      kx *= (1.38e-2 * T)/xi;
      ky = kx;
      kx /= DNA_L;
      ky /= (DNA_L + bead_r);
    }
  mean_extension = (mean_extension < 0) ? 0 : mean_extension;


  kz = marko_siggia_improved_derivate(x);
  kz *= (1.38e-2 * T)/xi;
  kz /= DNA_L;


  tauz = tauy = taux = 6 * M_PI * 1e-3 * bead_r;

  if (kx > 0) 
    {
      taux /= kx;  
      dx2_0 = (1.38e-5 * T)/kx;
    }
  else dx2_0 = taux = 0;
  if (ky > 0) 
    {
      tauy /= ky;  
      dy2_0 = (1.38e-5 * T)/ky;
    }
  else dy2_0 = tauy = 0;
  if (kz > 0) 
    {
      tauz /= kz;  
      dz2_0 = (1.38e-5 * T)/kz;
    }
  else dz2_0 = tauz = 0;

  // oversampling at OVER_SAMPLING x the camera rate
  dt = ((double)1)/(sim_over*over_sampling*camera_freq);
  
  betax = exp(-dt/taux);
  betay = exp(-dt/tauy);
  betaz = exp(-dt/tauz);

  tmpd = 1;
  tmpd -= betax * betax;
  xnoise = sqrt((tmpd * 1.38e-5 * T)/kx);  // en microns

  tmpd = 1;
  tmpd -= betay * betay;
  ynoise = sqrt((tmpd * 1.38e-5 * T)/ky);  // en microns

  tmpd = 1;
  tmpd -= betaz * betaz;
  znoise = sqrt((tmpd * 1.38e-5 * T)/kz);  // en microns


  return 0;
}


int do_led_modulation_simulation(void)
{
  register int i, j, l;
  int k;
  O_p *op = NULL, *opn = NULL;
  static int nf = 1024;
  d_s *dsn, *dsc, *dsled,  *dsl, *dss, *dsi, *dsp, *dspm;
  pltreg *pr = NULL;
  static float applied_force = 1, bead_r = 0.5, DNA_L = 5;
  float x_bnm = 0, y_bnm = 0, z_bnm = 0, tmp;    
  float x_bn = 0, y_bn, z_bn, norm, normp;

  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine simulates the brownian motion\n"
			   "of a bead thethered by a DNA molecule and \n"
			   "allows to calculate the resulting camera signal.\n"
			   "futhermore it allow to simulate a modulated \n"
			   "illumination \n");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");
  //nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("This routine simulates the brownian motion\n"
		"of a bead thethered by a DNA molecule and \n"
		"allows to calculate the resulting camera signal.\n"
		"The simulation is done at the camera frequency multiplied\n"
		"by an oversampling factor to better demonstrate aliasing\n"
		"Futhermore it allow to simulate a modulated \n"
		"illumination \n"
		"Applied force %8f bead radius %8f DNA length %8f\n"
		"Camera frequency f_c = %8f, over sampling ratio %6d\n"
		"Number of camera frame to simulate %8d\n"
		"LED modulation frequency %8d (x f_c) amplitude %8f\n"
		"Poisson burst frequency %8f\n"
		"shutter period (<over sampling ratio) %8d on period %8d\n"
		"random seed %8d\n"
		,&applied_force, &bead_r, &DNA_L
		,&camera_freq, &over_sampling, &nf, &led_freq, 
		&led_amp, &LED_poisson_freq, &shutter_on_period, 
		&shutter_period, &idum);
  if (i == CANCEL)	return OFF;

    

  for (i=0, j = 1; i< POISSON_MAK_K ; i++)
    {
      if (i == 0)	
	{
	  LED_fp[i] = exp(-LED_poisson_freq); 
	  LED_sfp[i] = LED_fp[i];
	}
      else 	
	{
	  j *= i;
	  LED_fp[i] = pow(LED_poisson_freq,i) * exp(-LED_poisson_freq)/j; 
	  LED_sfp[i] = LED_sfp[i-1] + LED_fp[i];
	}
    }
  
  if (LED_sfp[POISSON_MAK_K-1] < 0.999)
    win_printf("sfp[%d] = %g",POISSON_MAK_K-1,LED_sfp[POISSON_MAK_K-1]);



  if ((op = create_and_attach_one_plot(pr, nf*over_sampling, nf*over_sampling, 0)) == NULL)
    return win_printf_OK("cannot create plot 1!");
  dsled = op->dat[0];

  set_ds_source(dsled,"Instantaneous LED modulation freq %d amp %g",led_freq,led_amp);

  set_op_filename(op, "led-simul.gr");

  if ((opn = create_and_attach_one_plot(pr, nf*over_sampling, nf*over_sampling, 0)) == NULL)
    return win_printf_OK("cannot create plot 2!");
  dsn = opn->dat[0];
  set_ds_source(dsn,"Instantaneous X brownian motion camera freq %g oversampling %d",camera_freq,over_sampling);

  set_op_filename(opn, "led-mod_simul.gr");

  if ((dsc = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot 3!");

  set_ds_source(dsc,"X brownian motion average over one camera frame");



  if ((dsl = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot 4!");

  set_ds_source(dsl,"X brownian motion multiplied by LED");

  if ((dss = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot 5!");

  set_ds_source(dss,"window signal averaged per %d w %d",shutter_period,shutter_on_period);

  if ((dspm = create_and_attach_one_ds(op, nf*over_sampling, nf*over_sampling, 0)) == NULL)
    return win_printf_OK("cannot create plot 6!");

  set_ds_source(dspm,"Poisson modulation signal at %g",LED_poisson_freq);


  if ((dsi = create_and_attach_one_ds(op, nf*over_sampling, nf*over_sampling, 0)) == NULL)
    return win_printf_OK("cannot create plot 7!");

  set_ds_source(dsi,"periodic window modulation signal per %d w %d",shutter_period,shutter_on_period);


  if ((dsp = create_and_attach_one_ds(opn, nf, nf, 0)) == NULL)
    return win_printf_OK("cannot create plot 8!");

  set_ds_source(dsp,"Poisson signal averaged");

  convert_force_to_k(applied_force, bead_r, DNA_L);

  for (j = 0; j < nf; j++)
    {
      dsc->xd[j] = j;
      dsl->xd[j] = j;
      dss->xd[j] = j;
      dsp->xd[j] = j;
      for (i = 0, norm = normp = 0; i < over_sampling; i++)
	{
	  for (k = 0, x_bnm = y_bnm = z_bnm = 0; k < sim_over; k++)
	    {
	      x_bn = x_bn * betax + (float)(xnoise *gasdev(&idum));
	      x_bnm += x_bn;
	      y_bn = y_bn * betay + (float)(ynoise *gasdev(&idum));
	      y_bnm += y_bn;
	      z_bn = z_bn * betaz + (float)(znoise *gasdev(&idum));
	      z_bnm += z_bn;
	    }
	  if (sim_over > 0)
	    {
	      x_bnm /= sim_over;
	      y_bnm /= sim_over;
	      z_bnm /= sim_over;
	    }

	  dsn->yd[j * over_sampling + i] = x_bnm;
	  dsn->xd[j * over_sampling + i] = j + (float)i/over_sampling;
	  dspm->xd[j * over_sampling + i] = j + (float)i/over_sampling;
	  dsi->xd[j * over_sampling + i] = j + (float)i/over_sampling;
	  dsc->yd[j] += x_bnm/over_sampling;
	  dsled->yd[j * over_sampling + i] = 1 + led_amp * cos(M_PI*2*i*led_freq/over_sampling);
	  dsled->xd[j * over_sampling + i] = j + (float)i/over_sampling;
	  dsl->yd[j] += dsled->yd[j * over_sampling + i] * x_bnm/over_sampling;
	  if ((i%shutter_period) < shutter_on_period) 	    
	    {
	      dss->yd[j] += x_bnm;
	      norm += 1;
	      dsi->yd[j * over_sampling + i] = 1;
	    }
	  for (l = 0, tmp = ran1(&idum); l < POISSON_MAK_K && tmp > LED_sfp[l]; l++);
	  dspm->yd[j * over_sampling + i] = l;
	  dsp->yd[j] += (x_bnm*l);
	  normp += l;
	}
      if (normp)  dsp->yd[j] /= normp;
      if (norm)  dss->yd[j] /= norm;
    }
  create_attach_select_x_un_to_op(opn, IS_SECOND, 0, (float)1/camera_freq, 0,0, "s");
  create_attach_select_y_un_to_op(opn, IS_METER, 0, 1, -6,0, "\\mu m");
  /* now we must do some house keeping */

  create_attach_select_x_un_to_op(op, IS_SECOND, 0, (float)1/camera_freq, 0,0, "s");

  set_plot_title(opn, "F = %g bead r %g L %g/%g", applied_force, bead_r, mean_extension, DNA_L);
  set_plot_x_title(opn, "Time");
  set_plot_y_title(opn, "Signals");

  set_plot_title(op, "F = %g bead r %g L %g/%g", applied_force, bead_r, mean_extension, DNA_L);
  set_plot_x_title(op, "Time");
  set_plot_y_title(op, "Modulations");
  //opn->filename = Transfer_filename(op->filename);

  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int do_led_pw_modulation_on_simulation(void)
{
  register int i, j, l;
  int w, k;
  O_p *op = NULL, *opn = NULL;
  static int nf = 8192;
  d_s  *dsi, *dst, *dspw;
  pltreg *pr = NULL;
  float camera_freq1 = 0;
  int over_sampling1 = 0;
  float tmp, tmp2;    

  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine analyze the brownian motion\n"
			   "of a bead thethered by a DNA molecule and \n"
			   "allows to calculate the resulting camera signal.\n"
			   "assuming a PW  modulation illumination \n");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  i = sscanf(dsi->source,"Instantaneous X brownian motion camera freq %f oversampling %d",&camera_freq1,&over_sampling1);
  if (i != 2)      return win_printf_OK("cannot find camera freq and over_sampling");

  nf = dsi->nx;	/* this is the number of points in the data set */

  if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot 1!");
  dspw = opn->dat[0];
  dspw->nx = dspw->ny = 0;

  set_ds_source(dspw,"noise variance sersus size");
 
  dst = build_data_set(nf/over_sampling1,nf/over_sampling1);
  if (dst == NULL) return win_printf_OK("cannot create tmp ds!");

  for (i = 4, w = 1; i < over_sampling1; i*= 2, w *= 2)
    {
      for (j = 0; j < dst->nx; j++) 
	{
	  dst->yd[j] = 0;
	  dst->xd[j] = 0;
	}
      for (j = 0; j < nf; j++)
	{
	  k = j/ over_sampling1;
	  l = j - k % over_sampling1;
	  if (l%i < w) 
	    {
	      dst->yd[k] += dsi->yd[j];
	      dst->xd[k] += 1;
	    }
	}
      for (j = 0, tmp = 0; j < dst->nx; j++) 
	{
	  dst->yd[j] = (dst->xd[j] > 0) ? dst->yd[j]/dst->xd[j] : dst->yd[j];
	  tmp += dst->yd[j];
	}
      for (j = 0, tmp /= dst->nx, tmp2 = 0; j < dst->nx; j++) 
	  tmp2 += (dst->yd[j] - tmp) * (dst->yd[j] - tmp);

      tmp2 /= (dst->nx > 1) ? dst->nx - 1 : 1;
      add_new_point_to_ds(dspw, i,tmp2);
    }

  if (op->title != NULL)
    set_plot_title(opn, op->title);
  set_plot_x_title(opn, "W size");
  set_plot_y_title(opn, "PW variance");

  free_data_set(dst);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}


int do_led_single_pw_modulation_on_simulation(void)
{
  register int i, j, l;
  int w, k;
  O_p *op = NULL, *opn = NULL;
  static int nf = 8192;
  d_s  *dsi, *dst, *dspw;
  pltreg *pr = NULL;
  float camera_freq1 = 0;
  int over_sampling1 = 0;
  float tmp, tmp2;    

  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine analyze the brownian motion\n"
			   "of a bead thethered by a DNA molecule and \n"
			   "allows to calculate the resulting camera signal.\n"
			   "assuming a PW  modulation illumination \n");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  i = sscanf(dsi->source,"Instantaneous X brownian motion camera freq %f oversampling %d",&camera_freq1,&over_sampling1);
  if (i != 2)      return win_printf_OK("cannot find camera freq and over_sampling");

  nf = dsi->nx;	/* this is the number of points in the data set */

  if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot 1!");
  dspw = opn->dat[0];
  dspw->nx = dspw->ny = 0;

  set_ds_source(dspw,"noise variance sersus size");
 
  dst = build_data_set(nf/over_sampling1,nf/over_sampling1);
  if (dst == NULL) return win_printf_OK("cannot create tmp ds!");

  for (w = 1; w <= over_sampling1;  w *= 2)
    {
      for (j = 0; j < dst->nx; j++) 
	{
	  dst->yd[j] = 0;
	  dst->xd[j] = 0;
	}
      for (j = 0; j < nf; j++)
	{
	  k = j/ over_sampling1;
	  l = j - k % over_sampling1;
	  if (l%over_sampling1 < w) 
	    {
	      dst->yd[k] += dsi->yd[j];
	      dst->xd[k] += 1;
	    }
	}
      for (j = 0, tmp = 0; j < dst->nx; j++) 
	{
	  dst->yd[j] = (dst->xd[j] > 0) ? dst->yd[j]/dst->xd[j] : dst->yd[j];
	  tmp += dst->yd[j];
	}
      for (j = 0, tmp /= dst->nx, tmp2 = 0; j < dst->nx; j++) 
	  tmp2 += (dst->yd[j] - tmp) * (dst->yd[j] - tmp);

      tmp2 /= (dst->nx > 1) ? dst->nx - 1 : 1;
      add_new_point_to_ds(dspw, w,tmp2);
    }

  if (op->title != NULL)
    set_plot_title(opn, op->title);
  set_plot_x_title(opn, "W size");
  set_plot_y_title(opn, "Simple PW variance");

  free_data_set(dst);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int do_led_sine_modulation_on_simulation(void)
{
  register int i, j, l;
  int  k;
  O_p *op = NULL, *opn = NULL;
  int nf = 8192;
  static int bias = 1;
  d_s  *dsi, *dst, *dspw;
  pltreg *pr = NULL;
  float camera_freq1 = 0;
  int over_sampling1 = 0;
  float tmp, tmp2;    

  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine analyze the brownian motion\n"
			   "of a bead thethered by a DNA molecule and \n"
			   "allows to calculate the resulting camera signal.\n"
			   "assuming a cosine  modulation illumination \n");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  i = sscanf(dsi->source,"Instantaneous X brownian motion camera freq %f oversampling %d",&camera_freq1,&over_sampling1);
  if (i != 2)      return win_printf_OK("cannot find camera freq and over_sampling");

  nf = dsi->nx;	/* this is the number of points in the data set */

  i = win_scanf("Sinuus Led modulation \n"
		"Just sine %R sine and bias %r bias only %r\n",&bias);
  if (i == CANCEL) return OFF;

  if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot 1!");
  dspw = opn->dat[0];
  dspw->nx = dspw->ny = 0;

  set_ds_source(dspw,"noise variance sersus size bias = %d",bias);
 
  dst = build_data_set(nf/over_sampling1,nf/over_sampling1);
  if (dst == NULL) return win_printf_OK("cannot create tmp ds!");

  for (i = 1; i < over_sampling1/2; i++)
    {
      for (j = 0; j < dst->nx; j++) 
	{
	  dst->yd[j] = 0;
	  dst->xd[j] = 0;
	}
      for (j = 0; j < nf; j++)
	{
	  k = j/ over_sampling1;
	  l = j - k % over_sampling1;
	  if (bias < 2)
	    dst->yd[k] += dsi->yd[j] * cos(M_PI*2*j*i/over_sampling1);
	  if (bias) dst->yd[k] += dsi->yd[j];
	  dst->xd[k] += 1;
	}
      for (j = 0, tmp = 0; j < dst->nx; j++) 
	{
	  dst->yd[j] = (dst->xd[j] > 0) ? dst->yd[j]/dst->xd[j] : dst->yd[j];
	  tmp += dst->yd[j];
	}
      for (j = 0, tmp /= dst->nx, tmp2 = 0; j < dst->nx; j++) 
	  tmp2 += (dst->yd[j] - tmp) * (dst->yd[j] - tmp);

      tmp2 /= (dst->nx > 1) ? dst->nx - 1 : 1;
      add_new_point_to_ds(dspw, i,tmp2);
    }

  if (op->title != NULL)
    set_plot_title(opn, op->title);
  set_plot_x_title(opn, "led sine modulation freq");
  set_plot_y_title(opn, "sine mod variance");
  free_data_set(dst);

  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}



int do_led_poisson_modulation_on_simulation(void)
{
  register int i, j, l;
  int  k;
  O_p *op = NULL, *opn = NULL;
  int nf = 8192;
  d_s  *dsi, *dst, *dspw;
  pltreg *pr = NULL;
  float camera_freq1 = 0;
  int over_sampling1 = 0;
  float tmp, tmp2;    

  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine analyze the brownian motion\n"
			   "of a bead thethered by a DNA molecule and \n"
			   "allows to calculate the resulting camera signal.\n"
			   "assuming a Poisson noise modulation illumination \n");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");

  i = sscanf(dsi->source,"Instantaneous X brownian motion camera freq %f oversampling %d",&camera_freq1,&over_sampling1);
  if (i != 2)      return win_printf_OK("cannot find camera freq and over_sampling");

  nf = dsi->nx;	/* this is the number of points in the data set */


  if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot 1!");
  dspw = opn->dat[0];
  dspw->nx = dspw->ny = 0;

  set_ds_source(dspw,"noise variance");
 
  dst = build_data_set(nf/over_sampling1,nf/over_sampling1);
  if (dst == NULL) return win_printf_OK("cannot create tmp ds!");

  for (i = 0; i < 2; i++)
    {
      for (j = 0; j < dst->nx; j++) 
	{
	  dst->yd[j] = 0;
	  dst->xd[j] = 0;
	}
      for (j = 0; j < nf; j++)
	{
	  k = j/ over_sampling1;
	  l = j - k % over_sampling1;
	  if (i == 0)
	    {
	      dst->yd[k] += dsi->yd[j];
	      dst->xd[k] += 1;
	    }
	  else
	    {
	      for (l = 0, tmp = ran1(&idum); l < POISSON_MAK_K && tmp > LED_sfp[l]; l++);
	      dst->yd[k] += dsi->yd[j] * l;
	      dst->xd[k] += l;
	    }
	}
      for (j = 0, tmp = 0; j < dst->nx; j++) 
	{
	  dst->yd[j] = (dst->xd[j] > 0) ? dst->yd[j]/dst->xd[j] : dst->yd[j];
	  tmp += dst->yd[j];
	}
      for (j = 0, tmp /= dst->nx, tmp2 = 0; j < dst->nx; j++) 
	  tmp2 += (dst->yd[j] - tmp) * (dst->yd[j] - tmp);

      tmp2 /= (dst->nx > 1) ? dst->nx - 1 : 1;
      add_new_point_to_ds(dspw, i,tmp2);
    }

  if (op->title != NULL)
    set_plot_title(opn, op->title);
  set_plot_x_title(opn, "led sine modulation freq");
  set_plot_y_title(opn, "Poisson noise mod variance");
  free_data_set(dst);

  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}


int do_led_pw_modulation_on_multi_simulation(void)
{
  register int i, j, l;
  int w, k, i_force;
  O_p *op = NULL, *opn = NULL;
  static int nf = 8192, n_force = 10;
  d_s  *dsi, *dst, *dspw;
  pltreg *pr = NULL;
  float tmp, tmp2;    
  static float max_force = 20, force_div = 2, bead_r = 0.5, DNA_L = 5;
  float applied_force = 1;
  float x_bnm = 0, y_bnm = 0, z_bnm = 0;    
  float x_bn = 0, y_bn, z_bn, norm, normp;

  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine simulates the brownian motion\n"
			   "of a bead thethered by a DNA molecule and \n"
			   "allows to calculate the resulting camera signal.\n"
			   "futhermore it allow to simulate a modulated \n"
			   "illumination \n");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");
  //nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("This routine simulates the brownian motion\n"
		"of a bead thethered by a DNA molecule and \n"
		"allows to calculate the resulting camera signal.\n"
		"The simulation is done at the camera frequency multiplied\n"
		"by an oversampling factor to better demonstrate aliasing\n"
		"Futhermore it allow to simulate a modulated illumination \n"
		"Exponential force ramp: Max applied force %8f\n"
		"number of force step %8d step dividor %6f\n"
		"bead radius %8f DNA length %8f\n"
		"Camera frequency f_c = %8f, over sampling ratio %6d\n"
		"Number of camera frame to simulate %8d\n"
		"shutter total period (<over sampling ratio) %8d with on period %8d\n"
		"random seed %8d\n"
		,&max_force, &n_force, &force_div, &bead_r, &DNA_L
		,&camera_freq, &over_sampling, &nf,
		&shutter_period, 
		&shutter_on_period, &idum);
  if (i == CANCEL)	return OFF;




  if ((op = create_and_attach_one_plot(pr, nf*over_sampling, nf*over_sampling, 0)) == NULL)
    return win_printf_OK("cannot create plot 2!");
  dsi = op->dat[0];
  set_ds_source(dsi,"Instantaneous X brownian motion camera freq %g oversampling %d",camera_freq,over_sampling);

  set_op_filename(op, "led-mod_simul.gr");
  create_attach_select_x_un_to_op(op, IS_SECOND, 0, (float)1/camera_freq, 0,0, "s");
  create_attach_select_y_un_to_op(op, IS_METER, 0, 1, -6,0, "\\mu m");


  if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot 1!");
  dspw = opn->dat[0];
  dspw->nx = dspw->ny = 0;



  dst = build_data_set(nf,nf);
  if (dst == NULL) return win_printf_OK("cannot create tmp ds!");



  for(applied_force = max_force, i_force = 0; i_force < n_force; i_force++,applied_force /= force_div)
    { 
      convert_force_to_k(applied_force, bead_r, DNA_L);
      my_set_window_title("Simulating force %g",applied_force);
      for (j = 0; j < nf; j++)
	{
	  for (i = 0, norm = normp = 0; i < over_sampling; i++)
	    {
	      for (k = 0, x_bnm = y_bnm = z_bnm = 0; k < sim_over; k++)
		{
		  x_bn = x_bn * betax + (float)(xnoise *gasdev(&idum));
		  x_bnm += x_bn;
		  y_bn = y_bn * betay + (float)(ynoise *gasdev(&idum));
		  y_bnm += y_bn;
		  z_bn = z_bn * betaz + (float)(znoise *gasdev(&idum));
		  z_bnm += z_bn;
		}
	      if (sim_over > 0)
		{
		  x_bnm /= sim_over;
		  y_bnm /= sim_over;
		  z_bnm /= sim_over;
		}
	      dsi->yd[j * over_sampling + i] = x_bnm;
	      dsi->xd[j * over_sampling + i] = j + (float)i/over_sampling;
	    }
	}
      if (i_force != 0)
	{
	  if ((dspw = create_and_attach_one_ds(opn, 16, 16, 0)) == NULL)
	    return win_printf_OK("cannot create plot 1!");
	}
      dspw->nx = dspw->ny = 0;
      set_ds_source(dspw,"noise variance sersus size force = %g, Bead_r = %g, L%g/%g, shutter on %d/%d"
		    ,applied_force, bead_r, mean_extension, DNA_L, shutter_on_period, shutter_period);


      for (i = shutter_period, w = shutter_on_period; i <= over_sampling; i*= 2, w *= 2)
	{
	  for (j = 0; j < dst->nx; j++) 
	    {
	      dst->yd[j] = 0;
	      dst->xd[j] = 0;
	    }
	  for (j = 0; j < dsi->nx; j++)
	    {
	      k = j/ over_sampling;
	      l = j - k % over_sampling;
	      if (l%i < w) 
		{
		  dst->yd[k] += dsi->yd[j];
		  dst->xd[k] += 1;
		}
	    }
	  for (j = 0, tmp = 0; j < dst->nx; j++) 
	    {
	      dst->yd[j] = (dst->xd[j] > 0) ? dst->yd[j]/dst->xd[j] : dst->yd[j];
	      tmp += dst->yd[j];
	    }
	  for (j = 0, tmp /= dst->nx, tmp2 = 0; j < dst->nx; j++) 
	    tmp2 += (dst->yd[j] - tmp) * (dst->yd[j] - tmp);
	  
	  tmp2 /= (dst->nx > 1) ? dst->nx - 1 : 1;
	  add_new_point_to_ds(dspw, w,tmp2);
	}
      for (j = 0; j < dst->nx; j++) 
	{
	  dst->yd[j] = 0;
	  dst->xd[j] = 0;
	}
      for (j = 0; j < dsi->nx; j++)
	{
	  k = j/ over_sampling;
	  l = j - k % over_sampling;
	  dst->yd[k] += dsi->yd[j];
	  dst->xd[k] += 1;
	}
      for (j = 0, tmp = 0; j < dst->nx; j++) 
	{
	  dst->yd[j] = (dst->xd[j] > 0) ? dst->yd[j]/dst->xd[j] : dst->yd[j];
	  tmp += dst->yd[j];
	}
      for (j = 0, tmp /= dst->nx, tmp2 = 0; j < dst->nx; j++) 
	tmp2 += (dst->yd[j] - tmp) * (dst->yd[j] - tmp);
      
      tmp2 /= (dst->nx > 1) ? dst->nx - 1 : 1;
      add_new_point_to_ds(dspw, over_sampling,tmp2);
    }
  if (op->title != NULL)
    set_plot_title(opn, op->title);
  set_plot_x_title(opn, "W size");
  set_plot_y_title(opn, "PW variance");
  
  free_data_set(dst);
      /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}




int do_led_single_pw_modulation_on_multi_simulation(void)
{
  register int i, j, l;
  int w, k, i_force;
  O_p *op = NULL, *opn = NULL;
  static int nf = 8192, n_force = 10;
  d_s  *dsi, *dst, *dspw;
  pltreg *pr = NULL;
  float tmp, tmp2;    
  static float max_force = 20, force_div = 2, bead_r = 0.5, DNA_L = 5;
  float applied_force = 1;
  float x_bnm = 0, y_bnm = 0, z_bnm = 0;    
  float x_bn = 0, y_bn, z_bn, norm, normp;

  if(updating_menu_state != 0)	return D_O_K;
  
  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf_OK("This routine simulates the brownian motion\n"
			   "of a bead thethered by a DNA molecule and \n"
			   "allows to calculate the resulting camera signal.\n"
			   "futhermore it allow to simulate a modulated \n"
			   "illumination \n");
    }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr",&pr) != 1)
    return win_printf_OK("cannot find data");
  //nf = dsi->nx;	/* this is the number of points in the data set */
  i = win_scanf("This routine simulates the brownian motion\n"
		"of a bead thethered by a DNA molecule and \n"
		"allows to calculate the resulting camera signal.\n"
		"The simulation is done at the camera frequency multiplied\n"
		"by an oversampling factor to better demonstrate aliasing\n"
		"Futhermore it allow to simulate a modulated illumination \n"
		"Exponential force ramp: Max applied force %8f\n"
		"number of force step %8d step dividor %6f\n"
		"bead radius %8f DNA length %8f\n"
		"Camera frequency f_c = %8f, over sampling ratio %6d\n"
		"Number of camera frame to simulate %8d\n"
		"random seed %8d\n"
		,&max_force, &n_force, &force_div, &bead_r, &DNA_L
		,&camera_freq, &over_sampling, &nf, &idum);
  if (i == CANCEL)	return OFF;


  if ((op = create_and_attach_one_plot(pr, nf*over_sampling, nf*over_sampling, 0)) == NULL)
    return win_printf_OK("cannot create plot 2!");
  dsi = op->dat[0];
  set_ds_source(dsi,"Instantaneous X brownian motion camera freq %g oversampling %d",camera_freq,over_sampling);

  set_op_filename(op, "led-mod_simul.gr");

  create_attach_select_x_un_to_op(op, IS_SECOND, 0, (float)1/camera_freq, 0,0, "s");
  create_attach_select_y_un_to_op(op, IS_METER, 0, 1, -6,0, "\\mu m");


  if ((opn = create_and_attach_one_plot(pr, 16, 16, 0)) == NULL)
    return win_printf_OK("cannot create plot 1!");
  dspw = opn->dat[0];
  dspw->nx = dspw->ny = 0;

  //dst = build_data_set(nf/over_sampling,nf/over_sampling);
  dst = build_data_set(nf,nf);
  if (dst == NULL) return win_printf_OK("cannot create tmp ds!");

  for(applied_force = max_force, i_force = 0; i_force < n_force; i_force++,applied_force /= force_div)
    { 
      convert_force_to_k(applied_force, bead_r, DNA_L);
      my_set_window_title("Simulating force %g",applied_force);
      for (j = 0; j < nf; j++)
	{
	  for (i = 0, norm = normp = 0; i < over_sampling; i++)
	    {
	      for (k = 0, x_bnm = y_bnm = z_bnm = 0; k < sim_over; k++)
		{
		  x_bn = x_bn * betax + (float)(xnoise *gasdev(&idum));
		  x_bnm += x_bn;
		  y_bn = y_bn * betay + (float)(ynoise *gasdev(&idum));
		  y_bnm += y_bn;
		  z_bn = z_bn * betaz + (float)(znoise *gasdev(&idum));
		  z_bnm += z_bn;
		}
	      if (sim_over > 0)
		{
		  x_bnm /= sim_over;
		  y_bnm /= sim_over;
		  z_bnm /= sim_over;
		}
	      dsi->yd[j * over_sampling + i] = x_bnm;
	      dsi->xd[j * over_sampling + i] = j + (float)i/over_sampling;
	    }
	}
      if (i_force != 0)
	{
	  if ((dspw = create_and_attach_one_ds(opn, 16, 16, 0)) == NULL)
	    return win_printf_OK("cannot create plot 1!");
	}
      dspw->nx = dspw->ny = 0;
      set_ds_source(dspw,"noise variance sersus size force = %g, Bead_r = %g, L%g/%g,\n shutter on %d/%d kx = %e tau_x = %g dx^2 %e"
		    ,applied_force, bead_r, mean_extension, DNA_L, shutter_on_period, shutter_period, kx, taux, dx2_0);


      for (w = 1; w <= over_sampling;  w *= 2)
	{
	  for (j = 0; j < dst->nx; j++) 
	    {
	      dst->yd[j] = 0;
	      dst->xd[j] = 0;
	    }
	  for (j = 0; j < dsi->nx; j++) // was nf
	    {
	      k = j/ over_sampling;
	      l = j - k % over_sampling;
	      if (l%over_sampling < w) 
		{
		  dst->yd[k] += dsi->yd[j];
		  dst->xd[k] += 1;
		}
	    }
	  for (j = 0, tmp = 0; j < dst->nx; j++) 
	    {
	      dst->yd[j] = (dst->xd[j] > 0) ? dst->yd[j]/dst->xd[j] : dst->yd[j];
	      tmp += dst->yd[j];
	    }
	  for (j = 0, tmp /= dst->nx, tmp2 = 0; j < dst->nx; j++) 
	    tmp2 += (dst->yd[j] - tmp) * (dst->yd[j] - tmp);
	  
	  tmp2 /= (dst->nx > 1) ? dst->nx - 1 : 1;
	  //add_new_point_to_ds(dspw, (float)(w*1000)/(camera_freq*over_sampling),tmp2);  
	  add_new_point_with_y_error_to_ds(dspw, (float)(w*1000)/(camera_freq*over_sampling),tmp2, tmp2/sqrt(dst->nx));  
	}

      for (w = 1; w <= nf/32;  w *= 2)
	{
	  for (j = 0; j < dst->nx; j++) 
	    {
	      dst->yd[j] = 0;
	      dst->xd[j] = 0;
	    }
	  for (j = 0; j < dsi->nx; j++)
	    {
	      k = j/ (w*over_sampling);
	      l = j - k % (w*over_sampling);
	      dst->yd[k] += dsi->yd[j];
	      dst->xd[k] += 1;
	    }
	  for (j = 0, tmp = 0; j < dst->nx/w; j++) 
	    {
	      dst->yd[j] = (dst->xd[j] > 0) ? dst->yd[j]/dst->xd[j] : dst->yd[j];
	      tmp += dst->yd[j];
	    }
	  for (j = 0, tmp *= (float)w/dst->nx, tmp2 = 0; j < dst->nx/w; j++) 
	    tmp2 += (dst->yd[j] - tmp) * (dst->yd[j] - tmp);
	  
	  tmp2 /= ((dst->nx/w) > 1) ? (dst->nx/w) - 1 : 1;
	  add_new_point_with_y_error_to_ds(dspw, (float)(w*1000)/camera_freq,tmp2, tmp2/sqrt(dst->nx/w));  
	  //add_new_point_to_ds(dspw, (float)(w*1000)/(camera_freq),tmp2);
	}
    }
  if (op->title != NULL)
    set_plot_title(opn, op->title);
  set_plot_x_title(opn, "W size (ms)");
  set_plot_y_title(opn, "simple PW variance");
  
  free_data_set(dst);
      /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}


// fit derease of noise variance <\delta x^2> vs exposute time w

double compute_chi2_and_fit_wong( double tau_0, // characteristime in micro s
				 double *a,    // amplitude fitted
				 d_s *ds)      // data with error
{
  register int i;
  double n, d, tmp, sig2, fita = 0, alpha;
	
  if (ds == NULL || ds->nx < 3 || ds->ye == NULL || (tau_0 == 0)) return -1.0;
  for(i = 0, n = d = 0; i < ds->nx; i++)
    {
      alpha = (double)ds->xd[i]/ tau_0;
      tmp = 1 - exp(-alpha);
      tmp /= alpha * alpha;
      tmp = ((double)1.0/alpha) - tmp;
      tmp *= 2;
      sig2 = ds->ye[i]*ds->ye[i];
      if (sig2 == 0 || tmp == 0) 	
	win_printf("division by zero at pt %d",i);
      else
	{
	  n += (tmp * ds->yd[i])/sig2;
	  d += tmp*tmp/sig2;
	}
    }
  if (d == 0) 	win_printf("division by zero in fit");
  else	fita = n/d;
  for(i = 0, n = 0; i < ds->nx; i++)
    {
      alpha = (double)ds->xd[i]/ tau_0;
      tmp = 1 - exp(-alpha);
      tmp /= alpha * alpha;
      tmp = ((double)1.0/alpha) - tmp;
      tmp *= 2;
      sig2 = ds->ye[i]*ds->ye[i];
      if (sig2 == 0 || tmp == 0) 	
	win_printf("division by zero at pt %d",i);
      else
	{
	  tmp = ds->yd[i] - (fita*tmp);
	  n += (tmp*tmp)/sig2;
	}
    }
  if (a != NULL)	*a = fita;
  return n;	
}





int do_fit_wong(void)
{
  register int i;
  pltreg *pr;
  O_p *op;
  d_s *ds, *dsfit;
# ifdef DIS_CHI2
  d_s *dschi2a;
  O_p *opchi2;
# endif
  double mint, maxt, chi2 = 0, a, t, chi20, a0, t0, 
    chi21, a1, t1, tmp, alpha, tm, k, r_bead, f0, f0_th, F;
  static float Texp = 298, DNA_L = 1; 
  char st[1024];

  if(updating_menu_state != 0)	return D_O_K;		
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
    return win_printf_OK("cannot find data");


  if (ds->ye == NULL) 
    return win_printf_OK("data set must have error bars !");

  if ((dsfit = create_and_attach_one_ds(op, 256, 256, 0)) == NULL)
    return win_printf_OK("cannot create plot");	

  i = win_scanf("This routine fits the variance variation of a bead thethered by a DNA molecule\n"
		" when we change the camera exposure time\n"
		"enter Texp %8f \n"
                "precize DNA length (in micron)%8f\n"
		, &Texp, &DNA_L);
  if (i == CANCEL)	return OFF;

# ifdef DIS_CHI2
  opchi2 = create_and_attach_one_plot(pr, 32,32, 0);
  if (opchi2 == NULL)      return win_printf_OK("Cannot allocate plot");
  dschi2a = opchi2->dat[0];
  dschi2a->nx = dschi2a->ny = 0;
# endif

  
  for (i = 0, t0 = t1 = ds->xd[0]; i < ds->nx; i++)
    {
      if (t1 < ds->xd[i]) t1 = ds->xd[i];
      if (t0 > ds->xd[i]) t0 = ds->xd[i];
    }
  mint = t0; maxt = t1;
  //win_printf("min %g max %g",mint, maxt);
  t0 /= 4;
  if (t0 <= 0) t0 = 1;
  chi2 = chi20 = compute_chi2_and_fit_wong(t0, &a0, ds);
  t1 *= 4;
  for (t = t0, chi20 = compute_chi2_and_fit_wong(tm = t, &a, ds); t <= t1; t *=2)
    {
      chi2 = compute_chi2_and_fit_wong(t, &a, ds);
      //win_printf("\\chi^2 = %g n0 = %g a %g",chi2,t,a);
      if (chi2 < chi20) 
	{
	  chi20 = chi2;
	  tm = t;
	}
    }
  t = tm;
  //win_printf("first found %g chi2",t,chi20);
  chi21 = compute_chi2_and_fit_wong(2*t, &a1, ds);
  chi2 = compute_chi2_and_fit_wong(t/2, &a1, ds);
  if (chi21 < chi2)
    {
      chi2 = chi20;
      chi20 = compute_chi2_and_fit_wong((t0 = t), &a0, ds);
      chi21 = compute_chi2_and_fit_wong((t1 = 2*t), &a1, ds);
    }
  else
    {
      chi2 = chi20;
      chi20 = compute_chi2_and_fit_wong((t0 = t/2), &a0, ds);
      chi21 = compute_chi2_and_fit_wong((t1 = t), &a1, ds);
    }

  for (t = exp((log(t0)+log(t1))/2); fabs(t1/t0) > 1.00005 ; )
    {
      t = exp((log(t0)+log(t1))/2);
      chi2 = compute_chi2_and_fit_wong(t, &a, ds);
      display_title_message("\\chi^2 = %g n0 = %g a %g",chi2,t,a);
      //win_printf("\\chi^2 = %g n0 = %g a %g",chi2,t,a);
# ifdef DIS_CHI2
      add_new_point_to_ds(dschi2a, t, chi2);
# endif
      if (chi20 < chi21)
	{
	  chi21 = chi2;
	  t1 = t;
	  a1 = a;
	}
      else
	{
	  chi20 = chi2;
	  t0 = t;
	  a0 = a;
	}		
    }

  tmp = log(maxt/mint);
  for (i = 0; i < dsfit->nx; i++)
    {
      dsfit->xd[i] = exp((i*tmp)/dsfit->nx);
      /*
      if (i%16 == 0)
	{
	  win_printf("%d/%d mn %g max %g\n exp %g x %g\nlog %g"
		     ,i,dsfit->nx,mint,maxt,dsfit->xd[i],mint*dsfit->xd[i],tmp);
	}
      */
      dsfit->xd[i] *= mint;
      alpha = (double)dsfit->xd[i]/ t;
      dsfit->yd[i] = 1 - exp(-alpha);
      dsfit->yd[i] /= alpha * alpha;
      dsfit->yd[i] = ((double)1.0/alpha) - dsfit->yd[i];
      dsfit->yd[i] *= 2 * a;
    }
  
  k = 1.38e-11*Texp/a*op->dy;
  r_bead = t*op->dx*k/(6*M_PI*1e-3);
  f0 = 1e9*k/(12*M_PI*M_PI*r_bead);
  f0_th = 1e9*k/(12*M_PI*M_PI*0.5) ;
  F = 1e6*k*DNA_L;

  snprintf(st,sizeof(st),"\\fbox{\\pt8\\stack{{\\sl \\delta x^2(w) = "
	  "x_0^2 \\frac{2}{\\alpha} [1 - \\frac{1-exp^{-\\alpha}}{\\alpha}]}"
	  " {with \\alpha = w/\\tau_0}"
	  "{x_0^2 = %e%s}"
	  "{Stiffnes %g N/m at T = %g}"
	  "{\\tau_0 = %g %s}"
	  "{estimated bead radius = %g}"
          "{cut-off frequency = %g Hz}"
          "{ r=0.5 %g Hz}"
          "{DNA length %g {\\mu}m}"
          "{estimated force = %g pN}"
	  "{\\chi^2 = %g n = %d}}}",
	  a*op->dy,(op->y_unit != NULL)?op->y_unit:" ",
	   k,Texp,
	  t*op->dx,(op->x_unit != NULL)?op->x_unit:" ",
	   r_bead, f0,f0_th,DNA_L, F,
	  chi2,ds->nx);

  push_plot_label_in_ds(dsfit, dsfit->xd[dsfit->nx/2], a, st, USR_COORD);
# ifdef DIS_CHI2
  set_plot_x_title(opchi2, "w");
  set_plot_y_title(opchi2, "\\chi^2");
# endif
  
  return refresh_plot(pr,pr->n_op-1);
}





MENU *led_modulation_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"LED modulation", do_led_modulation_simulation,NULL,0,NULL);
  add_item_to_menu(mn,"LED PW modulation", do_led_pw_modulation_on_simulation,NULL,0,NULL);
  add_item_to_menu(mn,"LED single PW modulation", do_led_single_pw_modulation_on_simulation,NULL,0,NULL);
  add_item_to_menu(mn,"LED sine modulation", do_led_sine_modulation_on_simulation,NULL,0,NULL);
  add_item_to_menu(mn,"LED noise modulation", do_led_poisson_modulation_on_simulation,NULL,0,NULL);
  add_item_to_menu(mn,"Scan F LED PW modulation", do_led_pw_modulation_on_multi_simulation,NULL,0,NULL);

  add_item_to_menu(mn,"Scan F simple LED PW modulation", do_led_single_pw_modulation_on_multi_simulation,NULL,0,NULL);

  add_item_to_menu(mn,"Fit wong", do_fit_wong,NULL,0,NULL);


  return mn;
}

int	led_modulation_main(int argc, char **argv)
{
  add_plot_treat_menu_item ( "led_modulation", NULL, led_modulation_plot_menu(), 0, NULL);
  return D_O_K;
}

int	led_modulation_unload(int argc, char **argv)
{
  remove_item_to_menu(plot_treat_menu, "led_modulation", NULL, NULL);
  return D_O_K;
}
#endif

