#ifndef _LED_MODULATION_H_
#define _LED_MODULATION_H_

PXV_FUNC(int, do_led_modulation_rescale_plot, (void));
PXV_FUNC(MENU*, led_modulation_plot_menu, (void));
PXV_FUNC(int, do_led_modulation_rescale_data_set, (void));
PXV_FUNC(int, led_modulation_main, (int argc, char **argv));
#endif

