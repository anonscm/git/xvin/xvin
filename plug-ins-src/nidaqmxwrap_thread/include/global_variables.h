/** Global variables declarations for the plug-in

This file contains the declaration of all global variables in the plug-in, 
sorted by the .c file where they appear.
This allows all these variables to be used in any .c file in the plug-in 
as long as <<#define global_variables.h>> is declared in the .c file.

\author Adrien Meglio
*/

#ifndef GLOBAL_VARIABLES_H_
#define GLOBAL_VARIABLES_H_

#ifdef NIDAQMXWRAP_THREAD_C_
#define NIDAQMXWRAP_THREAD_C_

int flag__AO__card_data_do_refresh; /** "GO" will trigger AO_data refresh */
int flag__AO__card_active; /** Is a writing task currently taking place ? */
int32 global__AO__card_buffer_refreshed; /** Number of samples written */
int32 global__written; /** Number of samples written during the current task ; currently useless */

float global__last_known_frequency; /* The value of frequency before the last update of AO_data by local_lio */

int (*global__AO__pause_function)(void); /** The GLOBAL FUNCTION POINTER to the function, 
defined by the user, that will be called when the task stops */

TaskHandle CURRENTtaskHandle;

double* AO_data;

l_io local_lio;

int THREAD_PRIORITY; /* Priority of the refresh thread */

#else

extern int flag__AO__card_data_do_refresh; /** "GO" will trigger AO_data refresh */
extern int flag__AO__card_active; /** Is a writing task currently taking place ? */
extern int32 global__AO__card_buffer_refreshed; /** Number of samples written */
extern int32 global__written; /** Number of samples written during the current task ; currently useless */

extern float global__last_known_frequency; /* The value of frequency before the last update of AO_data by local_lio */

extern int (*global__AO__pause_function)(void); /** The GLOBAL FUNCTION POINTER to the function, 
extern defined by the user, that will be called when the task stops */

extern TaskHandle CURRENTtaskHandle;

extern double* AO_data;

extern l_io local_lio;

extern int THREAD_PRIORITY; /* Priority of the refresh thread */

#endif
#endif




