/** Function declarations for the plug-in

This file contains the declaration of all functions in the plug-in, 
sorted by the .c file where they appear and then by the category 
within the file to which they belong.
This allows all the functions to be used in any .c file in the plug-in 
as long as <<#define global_functions.h>> is declared in the .c file.

\author Adrien Meglio
*/

#ifndef NIDAQMXWRAP_THREAD_H_
#define NIDAQMXWRAP_THREAD_H_
#include "global_define.h"
#include "global_variables.h"

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From NIDAQ.C

PXV_FUNC(int, DAQmxErrChk, (int32 functionCall));

PXV_FUNC(int, NIDAQ_main, (void));

// From NIDAQ_task_handling.c

PXV_FUNC(int, task__continuous_buffer_regeneration, (void));

PXV_FUNC(int32, CVICALLBACK task__end, (void));
PXV_FUNC(int, task__all_close, (void));
PXV_FUNC(int, task__all_restart, (void));
PXV_FUNC(int, NIDAQ_task_handling_main, (void));

// From NIDAQ_user2nidaq.c

PXV_FUNC(int, IO__refresh_lio, (l_io lio));
PXV_FUNC(int, IO__change_thread_priority, (int thread_priority));

//PXV_FUNC(l_io, lines__initialize_local_lio, (void));
PXV_FUNC(int, lines__initialize_local_lio, (l_io* lio));

PXV_FUNC(int, lines__merge_data_and_launch, (void));

PXV_FUNC(int, IO__NIDAQ_max_frequency, (float *frequency));

PXV_FUNC(int, NIDAQ_user2nidaq_main, (void));

// From NIDAQ_test.c

PXV_FUNC(int, AOTF__sequence__alternate_one, (void));
PXV_FUNC(int, menu__alternate_one, (void));
PXV_FUNC(int, AOTF__sequence__alternate_two, (void));
PXV_FUNC(int, menu__alternate_two, (void));
PXV_FUNC(int, AOTF__sequence__alternate_three, (void));
PXV_FUNC(int, menu__alternate_three, (void));
PXV_FUNC(int, task__stop, (void));

// From NIDAQ_threads.c

PXV_FUNC(DWORD WINAPI, thread__AO_data_refresh, (LPVOID lpParam));
PXV_FUNC(int, launcher__AO__data_refresh_thread, (void));
PXV_FUNC(int, AO__sequence__empty, (void));
PXV_FUNC(int, AO__sequence__hold, (void));

// From nidaqmxwrap_thread_waveform.c
PXV_FUNC(float64, waveform__square_function,(float x,void* p));
PXV_FUNC(int, waveform__square_function_menu,(void));
PXV_FUNC(float64, waveform__triangle_function,(float x,void* p));
PXV_FUNC(float64, waveform__sin_function,(float x,void* p));
PXV_FUNC(float64, waveform__cos_function,(float x,void* p));
PXV_FUNC(float64, waveform__const_function,(float x,void* p));
PXV_FUNC(float64, waveform__testarbitrary_function,(float x,void* p));
PXV_FUNC(int, nidaqmx_wrap_thread_waveform_main,(void));

PXV_FUNC(MENU, *nidaqmxwrap_thread_waveform_menu, (void));



//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// From menu_plug_in.c

// Menu functions
PXV_FUNC(int, menu_plug_in_main, (void));


#endif


