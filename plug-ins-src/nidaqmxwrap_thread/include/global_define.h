/** Define and typedef declarations for the plug-in

This file contains the declaration of all define and typedef in the plug-in, 
sorted by the .c file where they appear.
This allows all these define to be used in any .c file in the plug-in 
as long as <<#define global_define.h>> is declared in the .c file.

\author Adrien Meglio
*/

#ifndef _GLOBAL_DEFINE_H_
#define _GLOBAL_DEFINE_H_

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// DEFINEs

// From AOTF_NIDAQ_base.c

#define NUMBER_OF_LINES 4
#define NUMBER_OF_AO_LINES 3

#define NIDAQ_MAX_POINT_FREQUENCY 833000 // The max write frequency (i.e. 1/VAL is the time between 2 points, in seconds) 

#define GO 1
#define WAIT 0

#define CARD_ACTIVE 1
#define CARD_PAUSE 2
#define CARD_STOP 3
#define CARD_RESTART 4

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
// TYPEDEFs

typedef struct LINE_STATUS
{   
    int status; /** The status (ON or OFF) */
    
    float64 (*function)(float x, void* params); /** Pointer to the refresh function.
    For each value i in [[0,num_points[[, AO_data[i] will be the float64 value of 'function(2*Pi*i/num_points)' (x is in [0,2Pi[) ;
    the function furthermore accepts params (void* params) to be recast in the function definition */
    
    void* params; /* Parameters to the function */
    
    float phase; /* i=0 will lead to f(phase,...) NOT f(2*Pi*phase,...) !! */
    float offset; /* will lead to f(x,...)+offset (i.e. f(x) is in [-1+offset,+1+offset]) */
    float amplitude; /* will lead to amplitude*f(x) (i.e. f(x) is in [-amplitude,+amplitude]) */
} l_s ;

typedef struct LINE_IO
{
    float frequency; /** Number of periods by second */
    unsigned int num_cycles; /** Number of cycles to perform. -1 will lead to infinity */
    int num_points; /** Number of data points per line, per period */
    
    l_s* lines;
} l_io ;

#endif



