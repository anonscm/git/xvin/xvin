/** \file menu_plug_in.c
    \brief Plug-in program for menu settings and display.
    
    This is a subprogram that creates, displays and interfaces the menus with the plug-in functions.
    
    \author Adrien Meglio
*/
#ifndef _MENU_PLUG_IN_C
#define _MENU_PLUG_IN_C

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

#include <NIDAQmx.h>
#include "../../logfile/logfile.h"
#include "nidaqmxwrap_thread.h"

#define BUILDING_PLUGINS_DLL

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Menu functions


/** Main menu for plug_in */     
int menu_nidaqmxwrap_thread_main(void)
{   
    if(updating_menu_state != 0) return D_O_K;
    
    return D_O_K;
}


#endif



