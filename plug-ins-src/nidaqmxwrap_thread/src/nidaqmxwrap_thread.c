/** \file NIDAQ.c

    \brief Main program for AOTF control.
    
    \author Adrien Meglio

    \version 18/07/2006
*/

#ifndef NIDAQMXWRAP_THREAD_C_
#define NIDAQMXWRAP_THREAD_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>


#include <NIDAQmx.h>
#include "../../logfile/logfile.h"
#include "nidaqmxwrap_thread.h"

#define BUILDING_PLUGINS_DLL


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// Base functions

/** Replacement function for error checking.

This function replaces the ill-formatted NI definition.

\param int32 functionCall The status to check.

\author Adrien Meglio
\version 13/09/2006
*/
int DAQmxErrChk(int32 functionCall)
{
    char errBuff[2048] = {'\0'};
    
    if(updating_menu_state != 0) return D_O_K;
    
    /* First tests whether an error has occured */
    if( DAQmxFailed(functionCall) )
    {
        DAQmxGetExtendedErrorInfo(errBuff,2048);
		
		/* Then stops current task */
	    if( CURRENTtaskHandle != 0 ) 
	    {
    	   	/*********************************************/
    		// DAQmx Stop Code
    		/*********************************************/
    		DAQmxStopTask(CURRENTtaskHandle);
    		DAQmxClearTask(CURRENTtaskHandle);
	    }
	    
	    /* Ultimately, prints error information and exits */
	    win_report("DAQmx Error: %s\n",errBuff);
	    //win_report("End of program, press Enter key to quit\n");
	    	    
	    /* Prints into the log file */
	    win_event("DAQmx Error: %s\n",errBuff);
	    
	    return D_O_K;
    }
    
    return D_O_K;
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// Housekeeping functions

int	nidaqmxwrap_thread_main(void)
{  
    if(updating_menu_state != 0) return D_O_K;
    
    THREAD_PRIORITY = THREAD_PRIORITY_TIME_CRITICAL; /* By default the refresh thread is time-critical */

    menu_nidaqmxwrap_thread_main(); // Menu functions
    
    nidaqmx_wrap_thread_waveform_main(); // Functions menu functions
    
    /* Initialization */  
    lines__initialize_local_lio(&local_lio);              
    task__initialize();    
        
    return D_O_K;
}

#endif








