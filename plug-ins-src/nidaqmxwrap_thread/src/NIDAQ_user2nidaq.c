/** \file NIDAQ_user2nidaq.c

    Contains all "user to NIDAQ" conversion functions via the "lines" structure.
    
    \author Adrien Meglio
    \author Francesco Mosconi
    \version 21/03/07
*/
#ifndef _NIDAQ_USER2NIDAQ_C_
#define _NIDAQ_USER2NIDAQ_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../../logfile/logfile.h"
#include "nidaqmxwrap_thread.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL
    
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
// IO functions

int IO__refresh_lio(l_io lio)
{
    local_lio = lio;
    
    lines__merge_data_and_launch();
    
    return D_O_K;
}    
    
int IO__change_thread_priority(int thread_priority)
{
    THREAD_PRIORITY = thread_priority;

    return D_O_K;
}
    
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
// AO_data functions     
    
//l_io lines__initialize_local_lio(void)
//{
//    int j;
//    l_io temp_lio;
//    
//    if(updating_menu_state != 0)	return temp_lio;
//    
//    //win_event("Initializing local_lio");
//    
//    temp_lio.frequency = 1;
//    temp_lio.num_cycles = -1;
//    temp_lio.num_points = 128;
//    
//    //win_event("Non-pointer data initialized in local_lio");
//    
//    temp_lio.lines = (l_s*) realloc(temp_lio.lines,NUMBER_OF_LINES*sizeof(l_s));
//    
//    //win_event("lines data allocated in local_lio");
//    
//    for (j=0 ; j<NUMBER_OF_LINES ; j++)
//    {
//        temp_lio.lines[j].status = 1;
//        temp_lio.lines[j].phase = 0;
//        temp_lio.lines[j].offset = 0;
//        temp_lio.lines[j].amplitude = 0;
//        
//        temp_lio.lines[j].params = (void*) NULL;
//        
//        temp_lio.lines[j].function = waveform__const_function;
//    }
//    
//    //win_event("local_lio initialized");
//    
//    return temp_lio;
//}

int lines__initialize_local_lio(l_io* lio)
{
    int j;
    l_io temp_lio;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    //win_event("Initializing local_lio");
    
    temp_lio.frequency = 1;
    temp_lio.num_cycles = -1;
    temp_lio.num_points = 128;
    
    //win_event("Non-pointer data initialized in local_lio");
    
    temp_lio.lines = (l_s*) calloc(NUMBER_OF_LINES,sizeof(l_s)); /* Do NOT use realloc here (for no reason,
    it prevents this function from working correctly in any other plugin calling the current one */
    if (temp_lio.lines == NULL) return win_report("Could not allocate lines data for lio structure !");
    
    //win_event("lines data allocated in local_lio");
    
    for (j=0 ; j<NUMBER_OF_LINES ; j++)
    {
        temp_lio.lines[j].status = 1;
        temp_lio.lines[j].phase = 0;
        temp_lio.lines[j].offset = 0;
        temp_lio.lines[j].amplitude = 0;
        
        temp_lio.lines[j].params = (void*) NULL;
        
        temp_lio.lines[j].function = waveform__const_function;
    }
    
    //win_event("lines data initialized in local_lio");
    
    *lio = temp_lio;
    
    //win_event("local_lio initialized");
    
    return D_O_K;
}
        
/** Merges lines data into AO_data.

The data individually stored in the structure lines are merged into a single, 
well-formatted pointer : AO_data, that will be sent to the card to be written 
on AO channels.

\author Adrien Meglio
\author Francesco Mosconi
\version 21/03/07
*/
int lines__merge_data_and_launch(void)
{
    int i,j;
    
    if(updating_menu_state != 0)	return D_O_K;

//    if (local_lio == NULL) 
//    {
//        lines__initialize_local_lio();
//        win_report("lines structures were re-created");
//    }

    /* Checks that the writing frequency is compatible with the card */
    if (NUMBER_OF_LINES*local_lio.num_points*local_lio.frequency > NIDAQ_MAX_POINT_FREQUENCY)
    {
      win_report("Attention : you are attempting to write more\n"
		 "points per second than the NIDAQ card can handle!\n"
		 "Reduce local_lio.lines[i].local_lio.num_points*FREQUENCY");
    }
          
    /* Generates AO_data */
    AO_data = (float64*) realloc(AO_data,NUMBER_OF_LINES*local_lio.num_points*sizeof(float64));
    if (AO_data == NULL) return win_report("memory allocation error");
    
    //win_report("AO data allocated");
    
    /* Merges the lines data into AO_data */
    for (i = 0 ; i < local_lio.num_points ; i++)
    {
        for (j = 0 ; j < NUMBER_OF_LINES ; j++)
        {
            /* Copies from lines.data to AO_data */
            AO_data[NUMBER_OF_LINES*i+j] = (float64) 
            local_lio.lines[j].status*
            (
                local_lio.lines[j].offset+local_lio.lines[j].amplitude*
	            (local_lio.lines[j].function((float)i/local_lio.num_points - local_lio.lines[j].phase,local_lio.lines[j].params))
            );
        }
    }
    
    //win_report("AO data updated");
    
    return D_O_K;
}    

///** Plots the data in the lines structure.
//
//\author Adrien Meglio
//\version 09/02/07
//*/
//int lines__sequence__plot(void)
//{
//    int i,j;
//    pltreg *current_pr;
//    O_p* current_plot;
//    d_s** lines_dataset;
//    int* nb_points;
//        
//    if(updating_menu_state != 0) return D_O_K;
//    
//    if (lines == NULL) 
//    {
//        lines__create_lines_structure();
//        win_report("lines structures was re-created");
//    }
//    
//    /* Finds the current plot region, or recreates it */
//    current_pr = get_pr();
//    if (current_pr == NULL)
//    {
//        current_pr = create_and_register_new_plot_project(0,0,100,100);
//    }
//    
//    /* Datasets creation */
//    nb_points = (int*) calloc(NUMBER_OF_AO_LINES,sizeof(int));
//    for (i=0 ; i<NUMBER_OF_AO_LINES ; i++)
//    {
//        nb_points[i] = lines[i].num_points;
//    }
//    lines_dataset = give_me_a__set_of_datasets_in_pr(NUMBER_OF_AO_LINES,nb_points,"FRET sequences");
//
//    /* Feeds the datasets with the data from lines */
//    for (i=0 ; i<NUMBER_OF_AO_LINES ; i++)
//    {
//        for (j=0 ; j<lines[i].num_points ; j++)
//        {
//            lines_dataset[i]->xd[j] = (float) j/(lines[i].num_points*g_frequency); /* Converts points to time */
//            lines_dataset[i]->yd[j] = (float) (lines[i].data[j])*(lines[i].level)*(lines[i].status); 
//        }
//    }
//    
//    /* Labels */
//    current_plot = current_pr->o_p[current_pr->cur_op];
//    current_plot->x_title = "Time (s)";
//    current_plot->y_title = "Input tension (V)";
//    
//    return D_O_K;
//}

#endif




