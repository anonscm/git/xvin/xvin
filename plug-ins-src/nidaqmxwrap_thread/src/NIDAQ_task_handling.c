/** \file NIDAQ_task_handling.c

    Defines the task and thread operations that handle the whole NIDAQ card processing.
    
    \author Adrien Meglio
    \author Francesco Mosconi
    \version 26/03/07
*/
#ifndef _NIDAQ_task_handling_C_
#define _NIDAQ_task_handling_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../../logfile/logfile.h"
#include "nidaqmxwrap_thread.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL


////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
// Task & thread configuration and operation

/** Initializes the task.

\author Adrien Meglio
\version 1/03/2007
*/
int task__initialize(void)
{
    if (updating_menu_state != 0) return D_O_K;
    
    /* After writing one period, the card will not wait for a signal before refreshing data */
    flag__AO__card_data_do_refresh = GO;   
        
    /* The 'pause' action will freeze the AO_data to its last point value */     
    global__AO__pause_function = AO__sequence__hold;
    
	return D_O_K;
}

/** A dynamic buffer operation.

The idea is that the card buffer has the structure [0000] (4 pts). When p out of 4 points have been read
(value given by DAQmxGetWriteSpaceAvail), upon call of DAQmxWriteAnalogF64(p points) the card buffer will now
be like 00[0011] (for p = 2) : the first p points of the card buffer are forgotten, a NEW index value for 
DAQmxGetWriteSpaceAvail is created. Then after reading p out of 4 points and calling DAQmxWriteAnalogF64(p points) 
the card buffer will now be like 0000[1122] and so on...
So : the idea is to create an initial card buffer TWICE as large as the "normal" data buffer (ie 2*p points), 
and refresh it every p points.

\author Adrien Meglio
\version 20/02/07
*/
int task__continuous_buffer_regeneration(void)
{
	float64     *temp_data;
	int         k;
	int32 		written;

    //int half_points = 500; /* Just for this test program */

    if (updating_menu_state != 0) return D_O_K;
    
    /* Stop all running tasks */
    task__all_close();
    
    /* (Re)sets the card in active mode */
    flag__AO__card_active = CARD_ACTIVE;

    /* For this new task, the card buffer will have never been refreshed */
    global__AO__card_buffer_refreshed = 0;
        
    /* If a task is currently running */
    if (CURRENTtaskHandle != 0) 
    {
        draw_bubble(screen,0,550,100,"Task already running !");
        
        return D_O_K; 
    }
    
    /* Refresh AO_data with the very last definition of local_lio */
    lines__merge_data_and_launch();
    
    /* Stores the new value of frequency in case it changes before the next period */
    global__last_known_frequency = local_lio.frequency;
    
	/* Initial card buffer : two AO_data concatenated ie "2*p" points */
	temp_data = (float64*) calloc(2*NUMBER_OF_LINES*local_lio.num_points,sizeof(float64)); 
	for (k=0 ; k<NUMBER_OF_LINES*local_lio.num_points ; k++)
	{
	    temp_data[k] = AO_data[k];
	    temp_data[k+NUMBER_OF_LINES*local_lio.num_points] = AO_data[k];
	}
	//temp_data = AO_data;
	//temp_data+NUMBER_OF_LINES*local_lio.num_points=AO_data;
    
    /* Task creation */
    DAQmxErrChk (DAQmxCreateTask("",&CURRENTtaskHandle));
    
    /* Write on all (0:3) AO lines between -10 and +10 volts */
    DAQmxErrChk (DAQmxCreateAOVoltageChan(CURRENTtaskHandle,"Dev1/ao0:3","",-10.0,10.0,DAQmx_Val_Volts,NULL));
    
    /* What to do when task finishes */   
    DAQmxErrChk (DAQmxRegisterDoneEvent(CURRENTtaskHandle,0,task__end,NULL));
    
    /* Write data continuously (DAQmx_Val_ContSamps). Since the card buffer is TWICE as large as the actual data buffer, 
       the point frequency is frequency*p points/sec */
	DAQmxErrChk (DAQmxCfgSampClkTiming(CURRENTtaskHandle,"",(float64) local_lio.frequency*local_lio.num_points,DAQmx_Val_Rising,DAQmx_Val_ContSamps,2*local_lio.num_points));
	
	/* Tells that even though output will be generated continuously, the user HAS to transfer fresh data buffer 
	to the card buffer at least each "2*p" points. The card will wait for fresh input to continue to write. */
	DAQmxErrChk (DAQmxSetWriteRegenMode(CURRENTtaskHandle,DAQmx_Val_DoNotAllowRegen));
    
	/* Transfer into the card buffer (in float64 mode) the initial buffer of "2*p" points */
    DAQmxErrChk (DAQmxWriteAnalogF64(CURRENTtaskHandle,2*local_lio.num_points,FALSE,-1,DAQmx_Val_GroupByScanNumber,temp_data,&written,NULL));
    	
	/* Start the task */
	DAQmxErrChk (DAQmxStartTask(CURRENTtaskHandle));
	   
	/* Continuously refresh AO_data when needed */   
	launcher__AO__data_refresh_thread();

	return D_O_K;
}

/** The thread controlling the casting of the data buffer "AO_data" into the NIDAQ card buffer

The thread :
- asks the card how many points are free to write in the card buffer
- if there are at least "p" points (data buffer size) available, begins refreshing process
- checks whether the process should wait for another process (like a movie acquisition) through 
the flag__refresh_card_data flag
- if given a "go", the process asks for the need to refresh "AO_data" itself
- if negative, the same "AO_data" is rewritten into the card buffer
- if positive, a new "AO_data" is generated and then written into the card buffer

\author Adrien Meglio
\author Francesco Mosconi
\version 22/03/07
*/
DWORD WINAPI thread__AO_data_refresh(LPVOID lpParam) 
{
    uInt32		writeSpaceAvail;
    
    while (flag__AO__card_active != CARD_STOP)
    {
        if (CURRENTtaskHandle != 0) 
        {   
            /* Tells how much space is now free to write in the card buffer */
            DAQmxErrChk (DAQmxGetWriteSpaceAvail(CURRENTtaskHandle, &writeSpaceAvail));
    	    
        	/* Refresh card buffer every "p" points written */
        	if (writeSpaceAvail > local_lio.num_points)
        	{
        	    //win_event("Enough space available in card buffer");
        	    
        	    /* Wait till card refresh signal is given */
        	    while (flag__AO__card_data_do_refresh != GO)
        	    {
        	        Sleep(50);
        	    }   		    
                
                if (flag__AO__card_active == CARD_ACTIVE)
                {   
                    //win_event("Card active and current task handle %d",CURRENTtaskHandle);
                    
                    /* Refresh data buffer for next use (the data buffer is of size "p") 
        		    NOTE : global__AOTF__refresh_function is a GLOBAL FUNCTION POINTER to the refresh
        		    function, as defined by the user. */
                    lines__merge_data_and_launch();
                    //win_event("lines refreshed");
                    
                    /* Counts the number of times the card buffer has been refreshed */
            	    global__AO__card_buffer_refreshed++;
            	    
            	    /* Checks whether enough periods have been written to the card */
            	    if (global__AO__card_buffer_refreshed >= local_lio.num_cycles)
            	    {
            	        flag__AO__card_active = CARD_PAUSE;
            	    }
                }
                else if (flag__AO__card_active == CARD_PAUSE)
    	        {
                    //win_event("Card in pause");
                    
                    global__AO__pause_function();                        
                    /* BEWARE : it executes this code indefinitly till tasks stops/restarts or flag__AO__card_active gets CARD_ACTIVE */
                }
                
                /* APPEND fresh data to the card buffer : note only "p" points are being written */
        	    DAQmxErrChk (DAQmxWriteAnalogF64(CURRENTtaskHandle,local_lio.num_points,FALSE,-1,DAQmx_Val_GroupByScanNumber,AO_data,&global__written,NULL));
        	    //win_event("sa %d gw %d",writeSpaceAvail,global__written);   
        	    
        	    /* If the frequency has changed since the last period, the task has to be entirely restarted.
                However, it is a good idea to let the current period finish quietly. To do that, we write a last
                set of AO_data (lines__merge_data_and_launch is called within the refresh loop ; if the card 
                buffer is not refreshed, an error will occur ; then we have to refresh the buffer once before 
                exiting the refesh loop) and set the card flag to STOP (by task__all_restart). The refresh thread 
                then finishes, and we stop the task (also in task__all_restart) and restart both the task
                and the thread (also in task__all_restart) */
                if (global__last_known_frequency != local_lio.frequency)
                {
		  task__all_restart(); /* Mettre un return devant pour sortir du thread ?? */
                }
                
                /* Stores the new value of frequency in case it changes before the next period */
                global__last_known_frequency = local_lio.frequency;
        	}
        }
        else 
        {
            Sleep(50); /* Wait till task is started */
        }
    }
    
    if (flag__AO__card_active == CARD_STOP)
    {
      global__AO__card_buffer_refreshed = 0;
      return D_O_K;
    }

    return D_O_K;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
// Launchers and handling

/** Creates the time-critical thread that will update the AO_data.

\param None.
\return int Error code.
\author Adrien Meglio
\version 21/02/07
*/ 
int launcher__AO__data_refresh_thread(void)
{
    HANDLE hThread = NULL;
    DWORD dwThreadId;

    hThread = CreateThread( 
            NULL,              // default security attributes
            0,                 // use default stack size  
            thread__AO_data_refresh,// thread function 
            NULL,        // argument to thread function 
            0,                 // use default creation flags 
            &dwThreadId);      // returns the thread identifier 

    if (hThread == NULL) 	win_report("No thread created");
    
    SetThreadPriority(hThread, THREAD_PRIORITY);

    return D_O_K;
}

/** What is done at the end of the task.

\author Adrien Meglio
\version 26/01/07
*/ 
int32 CVICALLBACK task__end(void)
{
  task__all_close();
  
  return D_O_K;
}

/** Stops the current task.

\author Adrien Meglio
\version 22/03/07
*/
int task__all_close(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    
    if (CURRENTtaskHandle != 0) 
    {
      //win_report("Task not finished... I will stop it !");
      
      /* Stops the refresh thread (otherwise WriteF64 would be called even though the task has been stopped, which causes an error) */
      flag__AO__card_active = CARD_STOP;
      
      /* Stops the task */
      DAQmxErrChk(DAQmxStopTask(CURRENTtaskHandle));
      DAQmxErrChk(DAQmxClearTask(CURRENTtaskHandle));
      
      //win_report("Task stopped");
      
      /* MANDATORY : Used to reinitialize the Current task */
      CURRENTtaskHandle = 0;
    }
    
    return D_O_K;
}   


/** Stops and restarts the current task.

\author Adrien Meglio
\version 22/03/07
*/
int task__all_restart(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    
    /* Stops the task */
    DAQmxErrChk(DAQmxStopTask(CURRENTtaskHandle));
    DAQmxErrChk(DAQmxClearTask(CURRENTtaskHandle));
      
    //win_report("Task stopped");
      
    /* MANDATORY : Used to reinitialize the Current task */
    CURRENTtaskHandle = 0;
    
    /* Restarts the task */
    task__continuous_buffer_regeneration();
    
    return D_O_K;
}   

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
// Some useful functions

/** An "empty" data sequence that sets all lines except the PIFOC line to 0

\author Adrien Meglio
\version 24/02/07
*/    
int AO__sequence__empty(void)
{
    int i,j;
    
    if (updating_menu_state != 0) return D_O_K;
    
    AO_data = (float64*) realloc(AO_data,NUMBER_OF_LINES*local_lio.num_points*sizeof(float64));
    for (i=0 ; i<local_lio.num_points ; i++)
	{
	    for (j=0 ; j<NUMBER_OF_LINES ; j++)
	    {
	        AO_data[NUMBER_OF_LINES*i+j] = 0;
	    }
	}

	return D_O_K;
}


/** A data sequence that frezzes all the lines to their last point value

\author Adrien Meglio
\version 22/03/07
*/    
int AO__sequence__hold(void)
{
    int i,j;
    float* line_last_value;
    
    if (updating_menu_state != 0) return D_O_K;
    
    /* Copies the last 4 points of AO_data into line_last_value*/
    line_last_value = (float*) realloc(line_last_value,NUMBER_OF_LINES*sizeof(float));
    for (j=0 ; j<NUMBER_OF_LINES ; j++)
	{
	    line_last_value[j] = AO_data[NUMBER_OF_LINES*(local_lio.num_points-1)+j];
	}
    
    /* Copies back the last 4 points of AO_data in EACH point of AO_data */
    AO_data = (float64*) realloc(AO_data,NUMBER_OF_LINES*local_lio.num_points*sizeof(float64));
    for (i=0 ; i<local_lio.num_points ; i++)
	{
	    for (j=0 ; j<NUMBER_OF_LINES ; j++)
	    {
	        AO_data[NUMBER_OF_LINES*i+j] = line_last_value[j];
	    }
	}

	return D_O_K;
}

#endif








