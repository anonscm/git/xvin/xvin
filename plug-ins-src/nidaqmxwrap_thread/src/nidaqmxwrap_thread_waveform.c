/** \file nidaqmxwrap_thread_waveform.c

    Tries to update directly the card.
    
    \author Adrien Meglio
    
    \version 13/02/07
*/
#ifndef NIDAQMXWRAP_THREAD_WAVEFORM_C_
#define NIDAQMXWRAP_THREAD_WAVEFORM_C_

#include <allegro.h>
#include <winalleg.h>
#include <xvin.h>

/* If you include other plug-ins header do it here*/ 
#include <NIDAQmx.h>
#include "../../logfile/logfile.h"
#include "nidaqmxwrap_thread.h"

/* But not below this define */
#define BUILDING_PLUGINS_DLL

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Arbitrary function.

\param float x The independent variable
\param void *p This is the duty cycle. It is cast on a float and it indicates the ratio of 'up' to 'up+down'.
\return float64 the function value

\author Francesco Mosconi
\version 21/03/07
*/    
float64 waveform__testarbitrary_function(float y,void* p)
{
  
  float * param = (float *) p;
  float duty_cycle = *param;
   float x = (y - floor(y));

  if( x <= duty_cycle)return sin(100/99*4*PI*x);

  return - sin(100/99*4*PI*x);
}


int waveform__testarbitrary_function_menu(void)
{
    int j;
    float *dc =  NULL;

    dc = (float*)  calloc(1,sizeof(float));
    *dc =0.5;
    
    if(updating_menu_state != 0) return D_O_K;

    for (j=0 ; j<NUMBER_OF_AO_LINES ; j++)
    {
        local_lio.lines[j].amplitude = 2.5;
        local_lio.lines[j].offset = 2.5;
        local_lio.lines[j].phase = (float) j/NUMBER_OF_AO_LINES;  

        local_lio.lines[j].function = waveform__testarbitrary_function;
        local_lio.lines[j].params = dc;
	
    }

    return D_O_K;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Square function.

\param float x The independent variable
\param void *p This is the duty cycle. It is cast on a float and it indicates the ratio of 'up' to 'up+down'.
\return float64 the function value

\author Francesco Mosconi
\version 21/03/07
*/    
float64 waveform__square_function(float x,void* p)
{
  
  float * param = (float *) p;
  float duty_cycle = *param;
  
  if((x - floor(x))<= duty_cycle)return 1;

  return -1;
}


int waveform__square_function_menu(void)
{
    int j;
    float *dc =  NULL;

    dc = (float*)  calloc(1,sizeof(float));
    *dc =0.5;
    
    if(updating_menu_state != 0) return D_O_K;

    for (j=0 ; j<NUMBER_OF_AO_LINES ; j++)
    {
        local_lio.lines[j].amplitude = 2.5;
        local_lio.lines[j].offset = 2.5;
        local_lio.lines[j].phase = (float) j/NUMBER_OF_AO_LINES;  

        local_lio.lines[j].function = waveform__square_function;
        local_lio.lines[j].params = dc;
	
    }

    return D_O_K;

}


////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Triangle function.

\param float x The independent variable
\param void *p This is the duty cycle. It is cast on a float and it indicates the ratio of 'upward' to the period length.
\return float64 the function value

\author Francesco Mosconi
\version 21/03/07
*/    
float64 waveform__triangle_function(float y,void* p)
{
  float * param = (float *) p;
  float duty_cycle = *param;
  float x = (y - floor(y));

  if(x <= duty_cycle) 
    return (float64) -1 + 2./duty_cycle*x ;

  return (float64) 3 - 2./(1-duty_cycle)*x;
}

int waveform__triangle_function_menu(void)
{
    int j;
    float *dc =  NULL;

    dc = (float*)  calloc(1,sizeof(float));
    *dc =0.5;

    if(updating_menu_state != 0) return D_O_K;
  
    for (j=0 ; j<NUMBER_OF_AO_LINES ; j++)
    {
        local_lio.lines[j].amplitude = 2.5;
        local_lio.lines[j].offset = 2.5;
        local_lio.lines[j].phase = (float) j/NUMBER_OF_AO_LINES;  
        
        local_lio.lines[j].function = waveform__triangle_function;
        local_lio.lines[j].params = dc;
    }

    return D_O_K;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Sine function.

\param float x The independent variable
\param void *p This is NULL;
\return float64 sin(x)

\author Francesco Mosconi
\version 21/03/07
*/    
float64 waveform__sin_function(float x,void* p)
{
  return (float64) sin(2*PI*x);
}

int waveform__sin_function_menu(void)
{
    int j;
    
    if(updating_menu_state != 0) return D_O_K;
  
    for (j=0 ; j<NUMBER_OF_AO_LINES ; j++)
    {
        local_lio.lines[j].amplitude = 2.5;
        local_lio.lines[j].offset = 2.5;
        local_lio.lines[j].phase = (float) j/NUMBER_OF_AO_LINES;  
        
        local_lio.lines[j].function = waveform__sin_function;
        local_lio.lines[j].params = (void*) NULL;
    }

    return D_O_K;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Cosine function
    
\param float x The independent variable
\param void *p This is NULL;
\return float64 cos(x)

\author Francesco Mosconi
\version 21/03/07
*/    
float64 waveform__cos_function(float x,void* p)
{
  return (float64) cos(2*PI*x);
}


int waveform__cos_function_menu(void)
{
    int j;
    
    if(updating_menu_state != 0) return D_O_K;
  
    for (j=0 ; j<NUMBER_OF_AO_LINES ; j++)
    {
        local_lio.lines[j].amplitude = 2.5;
        local_lio.lines[j].offset = 2.5;
        local_lio.lines[j].phase = (float) j/NUMBER_OF_AO_LINES;  
        
        local_lio.lines[j].function = waveform__cos_function;
        local_lio.lines[j].params = (void*) NULL;
    }

    return D_O_K;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/** An example of AO_data function
    Constant function
    
\param float x The independent variable
\param void *p This is NULL;
\return float64 1

\author Francesco Mosconi
\version 21/03/07
*/    
float64 waveform__const_function(float x,void* p)
{
  return 1.;
}


int waveform__const_function_menu(void)
{
    int j;
    
    if(updating_menu_state != 0) return D_O_K;
  
    for (j=0 ; j<NUMBER_OF_AO_LINES ; j++)
    {
        local_lio.lines[j].amplitude = 2.5;
        local_lio.lines[j].offset = 2.5;
        local_lio.lines[j].phase = (float) j/NUMBER_OF_AO_LINES;  
        
        local_lio.lines[j].function = waveform__const_function;
        local_lio.lines[j].params = (void*) NULL;
    }

    return D_O_K;

}

int lio__set_menu(void)
{
  if(updating_menu_state != 0) return D_O_K;
  win_scanf("frequency %6f\n"
	    "line0:\n"
	    "amplitude = %6f\n"
	    "offset = %6f\n"
	    "phase = %6f\n"
	    "line1:\n"
	    "amplitude = %6f\n"
	    "offset = %6f\n"
	    "phase = %6f\n"
	    "line2:\n"
	    "amplitude = %6f\n"
	    "offset = %6f\n"
	    "phase = %6f\n"
	    "line3:\n"
	    "amplitude = %6f\n"
	    "offset = %6f\n"
	    "phase = %6f\n",
	    &(local_lio.frequency),
	    &(local_lio.lines[0].amplitude),
	    &(local_lio.lines[0].offset),
	    &(local_lio.lines[0].phase),
	    &(local_lio.lines[1].amplitude),
	    &(local_lio.lines[1].offset),
	    &(local_lio.lines[1].phase),
	    &(local_lio.lines[2].amplitude),
	    &(local_lio.lines[2].offset),
	    &(local_lio.lines[2].phase),
	    &(local_lio.lines[3].amplitude),
	    &(local_lio.lines[3].offset),
	    &(local_lio.lines[3].phase));       
 
  return D_O_K;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Menu functions

/** A menu containing a clickable item and a static submenu */
MENU *nidaqmxwrap_thread_waveform_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	
    add_item_to_menu(mn,"Test : Start",task__continuous_buffer_regeneration,NULL,0,NULL);
    add_item_to_menu(mn,"Test : Stop",task__all_close,NULL,0,NULL);
    add_item_to_menu(mn,"Test : Restart",task__all_restart,NULL,0,NULL);
    add_item_to_menu(mn,"Test : Square",waveform__square_function_menu,NULL,0,NULL);
    add_item_to_menu(mn,"Test : Triangle",waveform__triangle_function_menu,NULL,0,NULL);
    add_item_to_menu(mn,"Test : Sine",waveform__sin_function_menu,NULL,0,NULL);
    add_item_to_menu(mn,"Test : Cosine",waveform__cos_function_menu,NULL,0,NULL);
    add_item_to_menu(mn,"Test : Constant",waveform__const_function_menu,NULL,0,NULL);
    add_item_to_menu(mn,"Test : Arbitrary",waveform__testarbitrary_function_menu,NULL,0,NULL);
    add_item_to_menu(mn,"Test : Set params",lio__set_menu,NULL,0,NULL);
    //add_item_to_menu(mn,"Test : Alternate Two",menu__alternate_two,NULL,0,NULL);
    //add_item_to_menu(mn,"Test : Alternate Three",menu__alternate_three,NULL,0,NULL);
   
    return mn;
}
    
int nidaqmx_wrap_thread_waveform_main(void)
{   
  if(updating_menu_state != 0) return D_O_K;
  
  add_plot_treat_menu_item("Nidaq Waveform Menu",NULL,nidaqmxwrap_thread_waveform_menu(),0,NULL);
  add_image_treat_menu_item("Nidaq Waveform Menu",NULL,nidaqmxwrap_thread_waveform_menu(),0,NULL);
  
  return D_O_K;    
}

#endif





