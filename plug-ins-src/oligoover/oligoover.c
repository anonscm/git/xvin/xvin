/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
#ifndef _OLIGOOVER_C_
#define _OLIGOOVER_C_

#include <allegro.h>
# include "xvin.h"

/* If you include other regular header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "oligoover.h"



// should work for woobbles and oligos of different size
int  does_oligos_seq_overlap(char *oli1, char *oli2)
{
    int i, j, k;
    int over = 0, size1, size2;
    char ch1, ch2;

    if (oli1 == NULL || oli2 == NULL) return -1000;

    size1 = strlen(oli1);
    size2 = strlen(oli2);


    for (i = k = 0, over = 0; over == 0 && i < size1; i++)
    {
        for (j = 0, over = 1; over > 0 && j < size2 && j < (size1 - i); j++)
        {
            ch1 = oli1[i + j] & 0x5f;
            ch2 = oli2[j] & 0x5f;

            if (ch1 == 'W')
            {
                over = ((ch2 == 'W') || (ch2 == 'A') || (ch2 == 'T')) ? (over + 1) : 0;
            }
            else if (ch1 == 'S')
            {
                over = ((ch2 == 'S') || (ch2 == 'C') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'M')
            {
                over = ((ch2 == 'K') || (ch2 == 'C') || (ch2 == 'A')) ? (over + 1) : 0;
            }
            else if (ch1 == 'K')
            {
                over = ((ch2 == 'M') || (ch2 == 'T') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'A')
            {
                over = ((ch2 == 'W') || (ch2 == 'A')) ? (over + 1) : 0;
            }
            else if (ch1 == 'T')
            {
                over = ((ch2 == 'W') || (ch2 == 'T')) ? (over + 1) : 0;
            }
            else if (ch1 == 'C')
            {
                over = ((ch2 == 'S') || (ch2 == 'C')) ? (over + 1) : 0;
            }
            else if (ch1 == 'G')
            {
                over = ((ch2 == 'S') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'N' || ch2 == 'N')
            {
                over++;
            }
            else over = 0;
        }
    }

    k = (over) ? over - 1 : 0;

    for (i = 0, over = 0; over == 0 && i < size2; i++)
    {
        for (j = 0, over = 1; over > 0 && j < size1 && j < (size2 - i); j++)
        {
            ch1 = oli2[i + j] & 0x5f;
            ch2 = oli1[j] & 0x5f;

            if (ch1 == 'W')
            {
                over = ((ch2 == 'W') || (ch2 == 'A') || (ch2 == 'T')) ? (over + 1) : 0;
            }
            else if (ch1 == 'S')
            {
                over = ((ch2 == 'S') || (ch2 == 'C') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'M')
            {
                over = ((ch2 == 'K') || (ch2 == 'C') || (ch2 == 'A')) ? (over + 1) : 0;
            }
            else if (ch1 == 'K')
            {
                over = ((ch2 == 'M') || (ch2 == 'T') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'A')
            {
                over = ((ch2 == 'W') || (ch2 == 'A')) ? (over + 1) : 0;
            }
            else if (ch1 == 'T')
            {
                over = ((ch2 == 'W') || (ch2 == 'T')) ? (over + 1) : 0;
            }
            else if (ch1 == 'C')
            {
                over = ((ch2 == 'S') || (ch2 == 'C')) ? (over + 1) : 0;
            }
            else if (ch1 == 'G')
            {
                over = ((ch2 == 'S') || (ch2 == 'G')) ? (over + 1) : 0;
            }
            else if (ch1 == 'N' || ch2 == 'N')
            {
                over++;
            }
            else over = 0;
        }
    }

    over = (over > 0) ? over - 1 : 0;
    k = (k > over) ? k : -over;
    return k;
}



int oligo_matrx(void)
{
  int i, over;
  char *oli[] = {"CACACTTACA","CTTACACCC","CCCACCCTT","CCTTACATCT","ACATCTACCT",
		  "TACCTCCAC","CTCACACCC","ACCCCCCCT","TCCACACCC","ACTCCCACA",
		  "CCCACCCAC","CCACACTCA"};
  int oli1 = 0, oli2 = 1;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_scanf("Select 2 oligo numbers\nOli1 %6d Oli2 %6d\n",&oli1,&oli2);
  if (i == CANCEL) return 0;
  over = does_oligos_seq_overlap(oli[oli1], oli[oli2]);
  if (over == 0)
    {
      win_printf("First Oligo %d-> %s and second oligo %d->%s\n"
		 "Do not overlap!",oli1,oli[oli1],oli2,oli[oli2]);
    }
  else
    {
      win_printf("First Oligo %d-> %s and second oligo %d->%s\n"
		 "Overlap over %d bases",oli1,oli[oli1],oli2,oli[oli2],over);
    } 
  return 0;
}

int oligo_matrix_col(void)
{
  int i, over;
  char *oli[] = {"CACACTTACA","CTTACACCC","CCCACCCTT","CCTTACATCT","ACATCTACCT",
		  "TACCTCCAC","CTCACACCC","ACCCCCCCT","TCCACACCC","ACTCCCACA",
		  "CCCACCCAC","CCACACTCA"};
  char resu[2048] = {0};
  int oli1 = 0, oli2 = 1;

  if(updating_menu_state != 0)	return D_O_K;

  i = win_scanf("Select first oligo numbers\nOli1 %6d\n",&oli1);
  if (i == CANCEL) return 0;
  for (oli2 = 0; oli2 < 12; oli2++)
    {
      over = does_oligos_seq_overlap(oli[oli1], oli[oli2]);
      snprintf(resu+strlen(resu),sizeof(resu)-strlen(resu),"Oligos %d with %d overlap %d\n"
	       ,oli1,oli2,over);
    }
  win_printf(resu);
  return 0;
}


int do_oligoover_corel_2_data_set(void)
{
  int i, j, ov;
  O_p *op = NULL;
  int nf, nf2, nfo;
  static int over = 10;
  d_s *ds, *dsi = NULL, *dsi2 = NULL;
  pltreg *pr = NULL;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  i = op->cur_dat + 1;
  i = (i >= op->n_dat) ? 0 : i;
  dsi2 = op->dat[i];

  nf = dsi->nx;	/* this is the number of points in the data set */
  nf2 = dsi2->nx; 
  if (nf != nf2) return win_printf_OK("datasets have different size !");
  i = win_scanf("What is the maximum shift %7d",&over);
  if (i == CANCEL)	return OFF;

  nfo = nf - 2 * over;
  for (ov = -over; ov <= over; ov++)
    {
      if ((ds = create_and_attach_one_ds(op, nfo, nfo, 0)) == NULL)
	return win_printf_OK("cannot create plot !");
      inherit_from_ds_to_ds(ds, dsi);
      set_ds_treatement(ds,"correl shift %d",ov);
      for (j = 0; j < nfo; j++)
	{
	  ds->yd[j] = dsi->yd[j+over+ov] * dsi2->yd[j+over];
	  ds->xd[j] = dsi->xd[j+over];
	}
    }

  /* now we must do some house keeping */
      /* refisplay the entire plot */      

  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}


MENU *oligoover_plot_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  add_item_to_menu(mn,"Correlate 2 ds over a range", do_oligoover_corel_2_data_set,NULL,0,NULL);
  add_item_to_menu(mn,"Overlap in 2 oligos", oligo_matrx,NULL,0,NULL);
  add_item_to_menu(mn,"Overlap ofoligos", oligo_matrix_col,NULL,0,NULL);


  return mn;
}

int	oligoover_main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  add_plot_treat_menu_item ( "oligoover", NULL, oligoover_plot_menu(), 0, NULL);
  return D_O_K;
}

int	oligoover_unload(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  remove_item_to_menu(plot_treat_menu, "oligoover", NULL, NULL);
  return D_O_K;
}
#endif

