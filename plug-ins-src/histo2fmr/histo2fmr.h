#pragma once

#include "platform.h"

PXV_FUNC(int, histo2fmr_main, (int argc, char **argv));

PXV_FUNC(int, do_add_peaks, (void));
PXV_FUNC(int, do_change_peak_val, (void));
PXV_FUNC(int, do_change_peak_val_new, (void));
PXV_FUNC(int, do_histo2fmr_convert, (void));
PXV_FUNC(int, load_config_peak,(void));
PXV_FUNC(int, save_config_peak,(void));
