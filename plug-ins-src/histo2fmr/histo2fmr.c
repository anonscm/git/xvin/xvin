# include "histo2fmr.h"
# include "allegro.h"
# include "xvin.h"
# include "stdbool.h"
# include "form_builder.h"

float *fmr_peak = NULL;
int fmr_peak_count = 0;

int do_add_peaks_new(void)
{
    int pcount = 1;
    int all_ret = 0;
    form_window_t *form = form_create("Add peak");

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    form_push_label(form, "Number of peaks to add"); 
    form_push_int(form, &pcount);
    all_ret = form_run(form);

    if (all_ret == CANCEL)
    {
        return OFF;
    }

    fmr_peak = (float *) realloc(fmr_peak, (fmr_peak_count + pcount) * sizeof(float));

    for (int i = fmr_peak_count; i < fmr_peak_count + pcount; ++i)
    {
        fmr_peak[i] = 0;
    }

    fmr_peak_count += pcount;
    return do_change_peak_val_new();
}
int do_add_peaks(void)
{
    int pcount = 1;
    int all_ret = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    all_ret = win_scanf("Number of peaks to add %d", &pcount);

    if (all_ret == CANCEL)
    {
        return OFF;
    }

    fmr_peak = (float *) realloc(fmr_peak, (fmr_peak_count + pcount) * sizeof(float));

    for (int i = fmr_peak_count; i < fmr_peak_count + pcount; ++i)
    {
        fmr_peak[i] = 0;
    }

    fmr_peak_count += pcount;
    return do_change_peak_val();
}

int do_change_peak_val_new(void)
{
    int all_ret = 0;
    bool fmr_peak_bool[200] = {0};

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    form_window_t *form = form_create("Modify Peaks");

    for (int i = 0; i < fmr_peak_count; ++i)
    {
        fmr_peak_bool[i] = true;
        form_push_bool(form, &(fmr_peak_bool[i]));
        form_push_label(form, "Peak %d", i);
        form_push_float(form, 1, &(fmr_peak[i]));
        form_newline(form);
    }

    all_ret = form_run(form);

    if (all_ret == D_O_K)
    {
        int i = 0;

        while (i < fmr_peak_count)
        {
            if (fmr_peak_bool[i] == 0)
            {
                for (int j = i + 1; j < fmr_peak_count; ++j)
                {
                    fmr_peak[j - 1] = fmr_peak[j];
                    fmr_peak_bool[j - 1] = fmr_peak_bool[j];
                }
                fmr_peak_count--;
            }
            else
            {
                ++i;
            }

        }

        save_config_peak();
    }

    form_free(form);
    return D_O_K;
}

int do_change_peak_val(void)
{
    int all_ret = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    char buf[10000] = {'\0'};
    char buf2[1024] = {'\0'};
    int index_modify = 0;
    int mode = 1;

    do
    {
        buf[0] = 0;
        strncat(buf, "Choose peak to modify\n", 10000);
        snprintf(buf2, 10000, "%%R Peak 0 = %f\n", fmr_peak[0]);
        strncat(buf, buf2, 10000);

        for (int i = 1; i < fmr_peak_count; ++i)
        {
            snprintf(buf2, 10000, "%%r Peak %d = %f\n", i, fmr_peak[i]);
            strncat(buf, buf2, 10000);
        }

        all_ret = win_scanf(buf, &index_modify);

        if (all_ret == CANCEL)
        {
            break;
        }

        mode = 1;
        snprintf(buf2, 10000, "%%R Remove peak \n%%r Peak %d = %%f\n", index_modify);
        all_ret = win_scanf(buf2, &mode, &fmr_peak[index_modify]);

        if (mode == 0)
        {
            for (int i = index_modify + 1; i < fmr_peak_count; ++i)
            {
                fmr_peak[i - 1] = fmr_peak[i];
            }

            fmr_peak_count--;
        }
    }
    while (true);

    save_config_peak();
    return D_O_K;
}

int do_histo2fmr_convert(void)
{
    int all_ret;
    pltreg *pr = NULL;
    O_p *histo_ops[1024] = {0};
    O_p *fmr_op = NULL;
    d_s *fmr_ds = NULL;
    int found_peaks_count = 0;
    int histo_ops_count = 0;
    float max_x_val = 0;
    float max_y_val = 0;
    float max_pos = 0;
    float prev_max_x_val = 0;
    float prev_max_y_val = 0;
    int find_max_ret = 0;
    float mthres = get_config_float("HISTO2FMR", "THRESHOLD", 0.01);
    float min_space = get_config_float("HISTO2FMR", "MINSPACE", 30);

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
    {
        return win_printf_OK("cannot find data");
    }

    for (int i = 0; i < pr->n_op; ++i)
    {
        O_p *cur_op = pr->o_p[i];

        if (cur_op->title != NULL && strstr(cur_op->title, "Histogram"))
        {
            histo_ops[histo_ops_count] = cur_op;
            histo_ops_count++;
        }
    }

    all_ret = win_scanf("Get first ds of each histogram and convert do FMR repeat data : \n Minimum Threshold %f \n Minimum space between peak %f nanometer",
                        &mthres, &min_space);

    if (all_ret == CANCEL)
    {
        return OFF;
    }

    set_config_float("HISTO2FMR", "THRESHOLD", mthres);
    set_config_float("HISTO2FMR", "MINSPACE", min_space);
    min_space = min_space / 1000; // histogram are in micro meter

    if ((fmr_op = create_and_attach_one_plot(pr, fmr_peak_count, fmr_peak_count, 0)) == NULL)
    {
        return win_printf_OK("cannot create plot!");
    }

    // create all plot
    for (int i = 0; i < histo_ops_count - 1; ++i)
    {
        create_and_attach_one_ds(fmr_op, fmr_peak_count, fmr_peak_count, 0);
    }

    //return 0;

    for (int i = 0; i < histo_ops_count; ++i)
    {
        O_p *histo_op = histo_ops[i];
        d_s *histo_ds = histo_op->dat[0];
        fmr_ds = fmr_op->dat[i];
        found_peaks_count = 0;

        for (int k = 1; k < histo_ds->nx - 1; k++)
        {
            if ((histo_ds->yd[k] > histo_ds->yd[k - 1]) && (histo_ds->yd[k] >= histo_ds->yd[k + 1])
                    && (histo_ds->yd[k] > mthres))
            {
                //win_printf("k = %d y %g",k,dsi->yd[k]);
                find_max_ret = find_max_around(histo_ds->yd, histo_ds->nx, k, &max_pos, &max_y_val, NULL);

                if (find_max_ret == 0)
                {
                    int pk = (int)max_pos;
                    int pk1 = pk + 1;
                    float p = (float)pk1 - max_pos;
                    pk = (pk < 0) ? 0 : pk;
                    pk = (pk < histo_ds->nx) ? pk : histo_ds->nx - 1;
                    pk1 = (pk1 < 0) ? 0 : pk1;
                    pk1 = (pk1 < histo_ds->nx) ? pk1 : histo_ds->nx - 1;
                    max_x_val = p * histo_ds->xd[pk] + (1 - p) * histo_ds->xd[pk1];

                    if (found_peaks_count > 0 && (max_x_val - prev_max_x_val) < min_space)
                    {
                        if (max_y_val > prev_max_y_val)
                        {
                            fmr_ds->xd[found_peaks_count - 1] = fmr_peak[found_peaks_count - 1];
                            fmr_ds->yd[found_peaks_count - 1] = max_x_val;
                        }
                    }
                    else if (found_peaks_count >= fmr_peak_count)
                    {
                        break;
                    }
                    else
                    {
                        fmr_ds->xd[found_peaks_count] = fmr_peak[found_peaks_count];
                        fmr_ds->yd[found_peaks_count] = max_x_val;
                        found_peaks_count++;
                    }

                    prev_max_x_val = max_x_val;
                    prev_max_y_val = max_y_val;
                }
            }
        }

        float offset = fmr_ds->yd[0];

        for (int i = 0; i < fmr_peak_count; ++i)
        {
            fmr_ds->yd[i] -= offset;
        }

        fmr_ds->source = (histo_op->title != NULL) ? strdup(histo_op->title) : NULL;
    }

    //remove first offset
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


MENU *histo2fmr_plot_menu(void)
{
    static MENU mn[32];

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "Convert histograms to fmr data", do_histo2fmr_convert, NULL, 0, NULL);
    add_item_to_menu(mn, "Add peaks", do_add_peaks, NULL, 0, NULL);
    add_item_to_menu(mn, "Change peak value", do_change_peak_val, NULL, 0, NULL);
    add_item_to_menu(mn, "Add peaks (NEW !)", do_add_peaks_new, NULL, 0, NULL);
    add_item_to_menu(mn, "Change peak value (NEW !)", do_change_peak_val_new, NULL, 0, NULL);
    return mn;
}

int load_config_peak(void)
{
    fmr_peak_count = get_config_int("HISTO2FMR", "PEAK_COUNT", 0);
    char buf[128] = {0};

    if (fmr_peak_count == 0)
    {
        fmr_peak = (float *) realloc(fmr_peak, 5 * sizeof(float));
        fmr_peak[0] = 0;
        fmr_peak[1] = 47;
        fmr_peak[2] = 88;
        fmr_peak[3] = 132;
        fmr_peak[4] = 196.5;
        fmr_peak_count = 5;
        save_config_peak();
    }
    else
    {
        fmr_peak = (float *) realloc(fmr_peak, fmr_peak_count * sizeof(float));

        for (int i = 0; i < fmr_peak_count; ++i)
        {
            snprintf(buf, 10000, "PEAK_%d", i);
            fmr_peak[i] = get_config_float("HISTO2FMR", buf, 0);
        }
    }

    return D_O_K;
}

int save_config_peak(void)
{
    char buf[128] = {0};
    set_config_int("HISTO2FMR", "PEAK_COUNT", fmr_peak_count);

    for (int i = 0; i < fmr_peak_count; ++i)
    {
        snprintf(buf, 10000, "PEAK_%d", i);
        set_config_float("HISTO2FMR", buf, fmr_peak[i]);
    }

    return D_O_K;
}

int histo2fmr_main(int argc, char **argv)
{
    (void) argc;
    (void) argv;
    load_config_peak();
    add_plot_treat_menu_item("histo2fmr", NULL, histo2fmr_plot_menu(), 0, NULL);
    return D_O_K;
}

int histo2fmr_unload(int argc, char **argv)
{
    (void) argc;
    (void) argv;
    remove_item_to_menu(plot_treat_menu, "histo2fmr", NULL, NULL);
    return D_O_K;
}
