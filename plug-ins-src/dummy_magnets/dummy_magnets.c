/*
*    Plug-in program for plot treatement in Xvin.
 *
 *    V. Croquette
  */
# ifndef _DUMMY_MAGNETS_C_
# define _DUMMY_MAGNETS_C_

# include "allegro.h"
# include "xvin.h"


/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
# include "../trackBead/magnetscontrol.h"

# undef _PXV_DLL
# define _PXV_DLL   __declspec(dllexport)
# include "dummy_magnets.h"


char	*describe_magnets_device_dummy(void)
{
  return "The translation and rotation of the magnets\n"
    "is faked by dummy functions just for test...\n"; 
}



int	_set_rot_value(float rot)
{
  //rot -= n_rot_offset;
  n_rota = rot + n_rot_offset;
  return 0;
}

int _set_rot_ref_value(float rot)  /* in tour */
{
  n_rot_offset = rot;
  //  n_rota = rot;
  return 0;
}

float _read_rot_value(void)
{
  return n_rota - n_rot_offset;
}


int   _set_magnet_z_value(float pos)
{
  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos; 
  pos -=  n_magnet_offset;
  n_magnet_z = pos + n_magnet_offset;
  return 0;
}

float _read_magnet_z_value(void)
{
  return n_magnet_z;	
}


int _set_motors_speed(float v)
{
  v_rota = v_mag = v;
  return 0;	
}
float  _get_motors_speed()
{
  return v_rota;
}

int _set_magnet_z_ref_value(float z)  /* in mm */
{
  n_magnet_offset = z;
  n_magnet_z = z;
  return 0;
}



int    _set_magnet_z_value_and_wait(float pos)
{
  pos = (pos > absolute_maximum_zmag) ? absolute_maximum_zmag : pos; 
  pos -=  n_magnet_offset;
  n_magnet_z = pos + n_magnet_offset;
  return 0;
}
int    _set_rot_value_and_wait(float rot)
{
  //rot -= n_rot_offset;
  n_rota = rot + n_rot_offset;
  return 0;
}

int	_go_and_dump_z_magnet(float z)
{
  n_magnet_z = z;	
  _set_magnet_z_value(n_magnet_z);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);
}
int    _go_wait_and_dump_z_magnet(float zmag)
{
  if (n_magnet_z == zmag)		return 0;
  _set_magnet_z_value_and_wait(zmag);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Magnet Z changed auto to %g\n",n_magnet_z);	
}

int    _go_and_dump_rot(float r)
{
  //n_rota = r;
  _set_rot_value(r);	
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);
}
int	_go_wait_and_dump_rot(float r)
{
  if ((n_rota - n_rot_offset) == r)		return 0;
  _set_rot_value_and_wait(r);
  return 0;
  //return dump_to_specific_log_file_with_time(CURRENT_LOG_FILE,"Rotation number auto changed %g\n",n_rota);	
}


int	_go_wait_and_dump_log_specific_rot(float r, char *log)
{	
  if ((n_rota - n_rot_offset) == r)		return 0;
  _set_rot_value_and_wait(r);
  //dump_to_specific_log_file_only(log,"Rotation number auto changed %g\n",n_rota);
  return 0;
}

int	_go_wait_and_dump_log_specific_z_magnet(float zmag, char *log)
{
  if (n_magnet_z == zmag)		return 0;
  _set_magnet_z_value_and_wait(zmag);
  //dump_to_specific_log_file_only(log,"Magnet Z changed auto to %g\n",n_magnet_z);	
  return 0;
}


# ifdef KEEP


int  do_motor1_in_turn_dummy(void)
{
  register int  i;
  float rot;
  
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in turn");
    }
  rot = n_rota;
  i = win_scanf("enter motor 2 new position %f",&rot);
  if (i == CANCEL)	return OFF;
  set_rot_value_dummy(rot);
  return 0;
}
int  do_motor1_in_turn_and_wait_dummy(char ch)
{
  register int  i;
  float rot;
  char pos[64];
  
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in turn");
    }

  rot = read_rot_value_dummy();
  sprintf(pos,"position read %g \n enter new position %%f",rot);
  rot = n_rota;
  i = win_scanf(pos,&rot);
  if (i == CANCEL)	return OFF;
  set_rot_value_and_wait_dummy(rot);
  return 0;
}



int	do_motor2_in_mm_dummy(void)
{
  register  int i;
  float pos;
  
  
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in mm");
    }
  /* we first find the data that we need to transform */
  pos =  n_magnet_z;
  i = win_scanf("enter motor 1 new position %f in mm",&pos);
  if (i == CANCEL)	return OFF;
  set_magnet_z_value_dummy(pos);
  return 0;
}
int	do_motor2_in_mm_and_wait_dummy(char ch)
{
  register  int i;
  float pos;
  char question[64];
  
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 2 \n"
			"to specified position in mm");
    }

  pos = read_magnet_z_value_dummy();
  sprintf(question,"read pos %g \n new pos in mm %%f",pos);
  pos =  n_magnet_z;
  i = win_scanf(question,&pos);
  if (i == CANCEL)	return OFF;
  set_magnet_z_value_and_wait_dummy(pos);
  return 0;
}
int	do_motor2_in_mm_n_time_dummy(void)
{
  register  int i, j;
  static float pos0 = 17,  pos_step = .1;
  static int nstep = 5, ntimes = 100;

  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in mm");
    }

  pos0 =  n_magnet_z;
  i = win_scanf("Zmag multi test \nenter motor max position in mm "
		"%fpos step %f nstep %d ntimes %d",
		&pos0,&pos_step,&nstep,&ntimes);
  if (i == CANCEL)	return OFF;
  for ( i = 0; i < ntimes; i++)
    {	
      for ( j = 0; j < nstep; j++)
	{	
	  set_magnet_z_value_dummy(pos0+j*pos_step);
	}
    }
  return 0;
}
int    do_rotate_n_time_dummy(void)
{
  register  int i, j;
  static float pos0 = 17,  pos_step = .1;
  static int nstep = 5, ntimes = 100;
  
  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
    {
      return win_printf("This routine moves motor 1 \n"
			"to specified position in turns");
    }


  pos0 =  n_rota;
  i = win_scanf("Rotation multi test \nenter motor max position in mm "
		"%fpos step %f nstep %d ntimes %d",
		&pos0,&pos_step,&nstep,&ntimes);
  if (i == CANCEL)	return OFF;
  for ( i = 0; i < ntimes; i++)
    {	
      for ( j = 0; j < nstep; j++)
	{	
	  set_rot_value_dummy(pos0+j*pos_step);
	}
    }
  return 0;
}

# endif


MENU *dummy_magnets_plot_menu(void)
{
	static MENU mn[32];

	if (mn[0].text != NULL)	return mn;
	//add_item_to_menu(mn,"data set rescale in Y", do_dummy_magnets_rescale_data_set,NULL,0,NULL);
	//add_item_to_menu(mn,"plot rescale in Y", do_dummy_magnets_rescale_plot,NULL,0,NULL);
	return mn;
}

int	dummy_magnets_main(int argc, char **argv)
{
  //	add_plot_treat_menu_item ( "dummy_magnets", NULL, dummy_magnets_plot_menu(), 0, NULL);
	return D_O_K;
}

int	dummy_magnets_unload(int argc, char **argv)
{
  //remove_item_to_menu(plot_treat_menu, "dummy_magnets", NULL, NULL);
	return D_O_K;
}
#endif

