#ifndef _TALBOT_H_
#define _TALBOT_H_

PXV_FUNC(int, do_talbot_average_along_y, (void));
PXV_FUNC(MENU*, talbot_image_menu, (void));
PXV_FUNC(int, talbot_main, (int argc, char **argv));
#endif

