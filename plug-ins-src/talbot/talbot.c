/*
 *    Plug-in program for image treatement in Xvin.
 *
 *    V. Croquette
 *
 *
 *    Modified by Hugo Trentesaux (hugo.trentesaux@u-psud.fr) on 2017-03-27
  */
#ifndef _TALBOT_C_
#define _TALBOT_C_

# include "allegro.h"
# include "xvin.h"

/* If you include other plug-ins header do it here*/ 


/* But not below this define */
# define BUILDING_PLUGINS_DLL
 # include "talbot.h"




	/* talbot image hugo version
		input
		onx,ony		image dimension
		dx,dz		image step
		lambda		
		l_d		decoherence lenght
		Id		relative diffused intensity at 1 mu m (direct intensity normalized to 1)
		theta_max
	*/
O_i	*talbot_image_new3(int onx, int ony, float dx, float dz, float lambda, float l_d, float Id, float theta_max, int power)
{
  register int i, j;
  O_i *oid;
  float r; 	// radius in x, y
  float z; 	// position of the focus plane ref to bead center
  float l, lb; 	// distance travelled by light obliquely
  float dl;	// optical path delay
  float I;	// 
  float iMin = cos(M_PI*theta_max/180); 	// minimum incidence
  union pix  *pd;

  
  oid =  create_one_image(onx, ony, IS_FLOAT_IMAGE);
  if (oid == NULL)
    {
      win_printf("Can't create dest image");
      return NULL;
    }

  for (i = 0, pd = oid->im.pixel; i < ony ; i++)
    {
      z = dz * i;		//* (i - ony/2); for both sides
      for (j=0; j< onx; j++)
	{
	  r = dx * (j - onx/2);		// radial position
	  l = sqrt(z*z + r*r);		// spherical position
	  lb = (l < 1) ? 1 : l;		// to avoid near points saturation
	  dl = (l - fabs(z))*1.33;		// optical path delay in water

	  I = (l == 0) ? 1: fabs(z/l);  	// cos theta, the intensity decreases as cos theta power power
	  I = (I < iMin) ? 0 : pow(I,power);		// I = cos theta < iMin = cos theta_max    <==>    theta > theta_max
          I *= Id;				// diffused intensity
          I /= lb*lb;				// intensity diminishes along propagation
						// relative intensity of diffused light

	  pd[i].fl[j] = 1+I + 2*sqrt(I)*cos(2*M_PI*dl/lambda)*exp(-dl/l_d); // two waves interferences
	}
    }
  create_attach_select_x_un_to_oi(oid, IS_METER, -dx*onx/2, dx, -6, 0, "\\mu m");
  create_attach_select_y_un_to_oi(oid, IS_METER, 0, dz, -6, 0, "\\mu m");		// -dz*ony/2 for both sides

  set_im_title(oid, "Calibration image");
  set_formated_string(&oid->im.source,"equally spaced reference profile Calibration image ");
  return oid;
}



// do talbot hugo version
int do_talbot_image_new3(void)
{
  O_i *oid;
  imreg *imr;
  static int i, onx = 128, ony = 50;
  static float dx = 0.08, dz = 0.3, lambda = 0.625, l_d = 2.0, Id = 0.8, theta_max = 70;
  static int power = 10;
  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine simulate a calibration image");

  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf_OK("Cannot find image region");

  i = win_scanf("Creating a calibration image\n"
		"size in x %6d size in y %6d\n"
		"dr (in microns) %5f dz (in microns) %5f\n"
		"wavelength %5f coherence length %5f\n"
		"Diffusive intensity %10f Power of diffusion (cos(t)^{power}) %d\n"
		"Max angle of light allow by the objective (in degrees)%2f\n"
		,&onx,&ony,&dx, &dz, &lambda, &l_d, &Id,&power,&theta_max);
  if (i == WIN_CANCEL)	return D_O_K;
  oid = talbot_image_new3(onx, ony, dx, dz, lambda, l_d, Id, theta_max,power);
  if (oid == NULL)	return win_printf_OK("unable to create image!");
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  oid->z_min = 0;	// set z limit in parameters
  //oid->z_max = 2;
  oid->z_black = 0;	// set black and white level in image
  //oid->z_white = 2;
  return (refresh_image(imr, imr->n_oi - 1));
}



MENU *talbot_image_menu(void)
{
  static MENU mn[32];

  if (mn[0].text != NULL)	return mn;
  /*add_item_to_menu(mn,"Calibration image", do_talbot_image_new,NULL,0,NULL);
  add_item_to_menu(mn,"Calibration image (gaussian decay in diffusion)", do_talbot_image_new2,NULL,0,NULL);  */
  add_item_to_menu(mn,"Calibration image (Hugo's version)", do_talbot_image_new3,NULL,0,NULL);  
  /* add_item_to_menu(mn,"Tilred Calibration image", do_talbot_image_tilted,NULL,0,NULL);
  add_item_to_menu(mn,"Back Calibration image", do_talbot_image_back_new,NULL,0,NULL);
  add_item_to_menu(mn,"pop", do_pop,NULL,0,NULL); */
  return mn; 
}


int	talbot_main(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  add_image_treat_menu_item ( "talbot", NULL, talbot_image_menu(), 0, NULL);
  return D_O_K;
}

int	talbot_unload(int argc, char **argv)
{
  (void)argc;
  (void)argv;
  remove_item_to_menu(image_treat_menu, "talbot", NULL, NULL);
  return D_O_K;
}
#endif


















// ------------------------------------------------------------ OUTDATED CODE --------------------------------------------------------------------------------

/*

O_i	*talbot_image_new(int onx, int ony, float dx, float dz, float lambda, float l_d, float Id, float theta_max, int rc)
{
  register int i, j, k;
  O_i *oid;
  float r; // radius in x, y
  float z; // position of the focus plane ref to bead center
  float l; // distance travelled by light obliquely
  float dl;// the difference of ray travel
  float It;
  float Imin; // minimum incidence
  float Ic;  // to account of rc
  union pix  *pd;

  
  oid =  create_one_image(onx, ony, IS_FLOAT_IMAGE);
  if (oid == NULL)
    {
      win_printf("Can't create dest image");
      return NULL;
    }
  Imin = cos(M_PI*theta_max/180);
  for (i = 0, pd = oid->im.pixel; i < ony ; i++)
    {
      z = dz * (i - ony/2);
      for (j=0; j< onx; j++)
	{
	  k = abs(j - onx/2);
	  // effect of bead radius absorption
	  Ic = (k < rc) ? 0 : 1;
	  Ic = (rc != 0) ? Ic : 1;
	  if ((rc > 0) &&  (k > rc) && (k <= 2*rc)) Ic = (1 - cos((M_PI*(k-rc))/rc))/2;
	  // length deviated
	  r = dx * (j - onx/2);
	  l = sqrt(z*z + r*r);
	  It = (l == 0) ? 1: fabs(z/l);  /* Id*cos \theta *
	  It = (It < Imin) ? 0 : It;
	  dl = fabs(l - fabs(z));  /* the difference of ray travel *
	  It *= Id * exp(-dl/l_d);
	  It = (It < Ic) ? It : Ic;
	  pd[i].fl[j] = (Ic-It);
	  It *= 2 * cos(M_PI*dl/lambda);
	  pd[i].fl[j] += It;
	}
    }
  create_attach_select_x_un_to_oi(oid, IS_METER, -dx*onx/2, dx, -6, 0, "\\mu m");
  create_attach_select_y_un_to_oi(oid, IS_METER, -dz*ony/2, dz, -6, 0, "\\mu m");

  set_im_title(oid, "Calibration image");
  set_formated_string(&oid->im.source,"equally spaced reference profile Calibration image ");
  return oid;
}



O_i	*talbot_image_new2(int onx, int ony, float dx, float dz, float lambda, float l_d, float Id, float theta_max
			   , float sigma) // we assume diffuse light decay with a gaussian law 
{
  register int i, j;
  O_i *oid;
  float r; // radius in x, y
  float z; // position of the focus plane ref to bead center
  float l, lb; // distance travelled by light obliquely
  float dl;// the difference of ray travel
  float It;
  float Imin; // minimum incidence
  float Itmp, Ib = 0, Ii = 0;
  union pix  *pd;

  
  oid =  create_one_image(onx, ony, IS_FLOAT_IMAGE);
  if (oid == NULL)
    {
      win_printf("Can't create dest image");
      return NULL;
    }
  Imin = cos(M_PI*theta_max/180);
  for (i = 0, pd = oid->im.pixel; i < ony ; i++)
    {
      z = dz * (i - ony/2);
      for (j=0; j< onx; j++)
	{
	  // length deviated
	  r = dx * (j - onx/2);
	  l = sqrt(z*z + r*r);
	  It = (l == 0) ? 1: fabs(z/l);  /* Id*cos \theta *
	  It = (It < Imin) ? 0 : It;
	  lb = (l < 1) ? 1 : l;
	  Itmp = (z != 0) ? Id*exp(-(r*r)/(2*z*z*sigma*sigma))/lb: Id;
	  It = (It < Imin) ? 0 : Itmp;
	  dl = fabs(l - fabs(z));  /* the difference of ray travel *
	  //It *= Id * exp(-dl/l_d);
	  if (It > 1)
	    {
	      Ib = It - 1;
	      Ii = 1;
	    }
	  else
	    {
	      Ib = 1 - It;
	      Ii = It;	      
	    }
	  pd[i].fl[j] = Ib + Ii * (1+cos(M_PI*dl/lambda));
	}
    }
  create_attach_select_x_un_to_oi(oid, IS_METER, -dx*onx/2, dx, -6, 0, "\\mu m");
  create_attach_select_y_un_to_oi(oid, IS_METER, -dz*ony/2, dz, -6, 0, "\\mu m");

  set_im_title(oid, "Calibration image");
  set_formated_string(&oid->im.source,"equally spaced reference profile Calibration image ");
  return oid;
}

O_i	*talbot_image_back_new(int onx, int ony, float dx, float dz, float lambda, float l_d, float Id, float theta_max, int rc)
{
  register int i, j, k;
  O_i *oid;
  float r, z, l, dl, It, Imin, Ic;
  union pix  *pd;

  
  oid =  create_one_image(onx, ony, IS_FLOAT_IMAGE);
  if (oid == NULL)
    {
      win_printf("Can't create dest image");
      return NULL;
    }
  Imin = cos(M_PI*theta_max/180);
  for (i = 0, pd = oid->im.pixel; i < ony ; i++)
    {
      z = dz * (i - ony/2);             // distance in z from bead center to glass
      for (j=0; j< onx; j++)
	{
	  k = abs(j - onx/2);           // distance in x
	  Ic = (k < rc) ? 0 : 1;        // intensity of the parallel ligght beam
	  Ic = (rc != 0) ? Ic : 1;
	  if ((k > rc) && (k <= 2*rc)) Ic = (1 - cos((M_PI*(k-rc))/rc))/2;
	  r = dx * (j - onx/2);
	  l = sqrt(z*z + r*r);
	  It = (l == 0) ? 1: fabs(z/l);  /* Id*cos \theta *
	  It = (It < Imin) ? 0 : It;
	  //dl = fabs(l - fabs(z) -fabs(z));  /* the difference of ray travel *
	  dl = fabs(l + fabs(z));  /* the difference of ray travel *
	  It *= Id * exp(-dl/l_d);
	  It = (It < Ic) ? It : Ic;
	  pd[i].fl[j] = (Ic-It);
	  It *= 2 * cos(M_PI*dl/lambda);
	  pd[i].fl[j] += It;
	}
    }
  create_attach_select_x_un_to_oi(oid, IS_METER, -dx*onx/2, dx, -6, 0, "\\mu m");
  create_attach_select_y_un_to_oi(oid, IS_METER, -dz*ony/2, dz, -6, 0, "\\mu m");

  set_im_title(oid, "Back Calibration image");
  set_formated_string(&oid->im.source,"equally spaced reference profile back Calibration image \n"
		      "nx %d ny %d dx %g dz %g \\lambda %g l_d %g Id  %g \\theta_{max} %g rc %d", 
		      onx, ony, dx, dz, lambda, l_d, Id, theta_max, rc);
  return oid;
}


int do_talbot_image_new(void)
{
  O_i *oid;
  imreg *imr;
  static int i, onx = 256, ony = 64, rc = 10;
  static float dx = 0.1, dz = 0.3, lambda = 0.6, l_d = 3, Id = 0.1, theta_max = 70;
  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine simulate a calibration image");

  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf_OK("Cannot find image region");

  i = win_scanf("Creating a calibration image\n"
		"size in x%dsize in y %d\n"
		"dr (in microns) %f dz (in microns) %f\n"
		"wavelength %f coherence length %f\n"
		" Difusive intensity %f\n"
		"Max angle of light allow by the objective (in degrees)%f\n"
		"critical radius %d\n",
		&onx,&ony,&dx, &dz, &lambda, &l_d, &Id,&theta_max,&rc);
  if (i == WIN_CANCEL)	return D_O_K;
  oid = talbot_image_new(onx, ony, dx, dz, lambda, l_d, Id, theta_max, rc);
  if (oid == NULL)	return win_printf_OK("unable to create image!");
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  return (refresh_image(imr, imr->n_oi - 1));
}

int do_talbot_image_new2(void)
{
  O_i *oid;
  imreg *imr;
  static int i, onx = 256, ony = 64;
  static float dx = 0.1, dz = 0.3, lambda = 0.6, l_d = 3, Id = 0.1, theta_max = 70, sigma = .5;
  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine simulate a calibration image");

  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf_OK("Cannot find image region");

  i = win_scanf("Creating a calibration image\n"
		"size in x %6d size in y %6d\n"
		"dr (in microns) %5f dz (in microns) %5f\n"
		"wavelength %5f coherence length %5f\n"
		" Difusive intensity %5f\n"
		"Max angle of light allow by the objective (in degrees)%5f\n"
		"Sigma diffusion %5f\n"
		,&onx,&ony,&dx, &dz, &lambda, &l_d, &Id,&theta_max,&sigma);
  if (i == WIN_CANCEL)	return D_O_K;
  oid = talbot_image_new2(onx, ony, dx, dz, lambda, l_d, Id, theta_max, sigma);
  if (oid == NULL)	return win_printf_OK("unable to create image!");
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  return (refresh_image(imr, imr->n_oi - 1));
}

int do_talbot_image_back_new(void)
{
  O_i *oid;
  imreg *imr;
  static int i, onx = 256, ony = 64, rc = 10;
  static float dx = 0.1, dz = 0.3, lambda = 0.6, l_d = 3, Id = 0.1, theta_max = 70;
  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine simulate a back calibration image");

  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf_OK("Cannot find image region");

  i = win_scanf("Creating a calibration image\n"
		"size in x%dsize in y %d\n"
		"dr (in microns) %f dz (in microns) %f\n"
		"wavelength %f coherence length %f\n"
		" Difusive intensity %f\n"
		"Max angle of light allow by the objective (in degrees)%f\n"
		"critical radius %d\n",
		&onx,&ony,&dx, &dz, &lambda, &l_d, &Id,&theta_max,&rc);
  if (i == WIN_CANCEL)	return D_O_K;
  oid = talbot_image_back_new(onx, ony, dx, dz, lambda, l_d, Id, theta_max, rc);
  if (oid == NULL)	return win_printf_OK("unable to create image!");
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  return (refresh_image(imr, imr->n_oi - 1));
}

// draw the image of a bead with a tilted illuminatex 

O_i	*talbot_image_tilted(int onx, int ony,       // image size 
			     float dx, float dy,     // concersion pixel microns
			     float lambda,           // wavelength 
			     float l_d,              // coherence length
			     float Id,               // strength of diffusive light
			     float theta_max,        // the maximum diffusion angle
			     int rc,                 // critical radius
			     float hc,               // the height of the bead to the focus plane
			     float xc)               // the shift of the bead in x
{
  register int i, j;
  O_i *oid;
  O_p *op;
  d_s *ds, *ds2;
  float x, y, dl, It, Imin, OD, OA, OE, si, co, x0;
  union pix  *pd;

  (void)rc;
  oid =  create_one_image(onx, ony, IS_FLOAT_IMAGE);
  if (oid == NULL)
    {
      win_printf("Can't create dest image");
      return NULL;
    }
  op = create_and_attach_op_to_oi(oid, onx, onx,0, IM_SAME_Y_AXIS|IM_SAME_X_AXIS);  
  ds = op->dat[0];
  if ((ds2 = create_and_attach_one_ds(op, ony, ony, 0)) == NULL)
    return win_printf_ptr("I can't create plot !");

  Imin = cos(M_PI*theta_max/180);
  OD = sqrt(hc*hc + xc*xc);
  si = xc/OD;
  co = hc/OD;
  for (i = 0, pd = oid->im.pixel; i < ony ; i++)
    {
      y = dy * (i - ony/2);
      for (j=0; j< onx; j++)
	{
	  x = dx * (j - onx/2);
	  x = xc - x;
	  if (j == 0) x0 = fabs(x); 
	  OE = OD - x * si;
	  OA = sqrt(OE*OE + x * x * co * co + y * y);
	  //cor = OE/OA;
	  dl = fabs(OE - OA);
	  It = (OA == 0) ? 1: fabs(OE/OA);  /* Id*cos \theta *
	  It = (It < Imin) ? 0 : It;
	  It *= Id * exp(-dl/l_d);
	  if (fabs(x) < x0)
	    {
	      ds2->xd[i] = (i - ony/2);
	      ds2->yd[i] = dl;
	      x0 = fabs(x); 
	    }
	  if (i == ony/2)
	    {
	      ds->xd[j] = (j - onx/2);
	      ds->yd[j] = dl;
	    }

	  It *= 2 * cos(M_PI*dl/lambda);
	  pd[i].fl[j] += It;
	}
    }
  for (i = 0, pd = oid->im.pixel; i < ony ; i++)
    {
      y = dy * (i - ony/2);
      for (j=0; j< onx; j++)
	{
	  x = dx * ((onx/2) - j);
	  x = xc - x;
	  OE = OD - x * si;
	  OA = sqrt(OE*OE + x * x * co * co + y * y);
	  //cor = OE/OA;
	  dl = fabs(OE - OA);
	  It = (OA == 0) ? 1: fabs(OE/OA);  /* Id*cos \theta *
	  It = (It < Imin) ? 0 : It;
	  It *= Id * exp(-dl/l_d);
	  It *= 2 * cos(M_PI*dl/lambda);
	  pd[i].fl[j] += It;
	}
    }
  create_attach_select_x_un_to_oi(oid, IS_METER, -dx*onx/2, dx, -6, 0, "\\mu m");
  create_attach_select_y_un_to_oi(oid, IS_METER, -dy*ony/2, dy, -6, 0, "\\mu m");

  set_im_title(oid, "Calibration image");
  set_formated_string(&oid->im.source,"equally spaced reference profile Calibration image ");
  return oid;
}


int do_talbot_image_tilted(void)
{
  O_i *oid;
  imreg *imr;
  static int i, onx = 256, ony = 64, rc = 10;
  static float dx = 0.1, dy = 0.1, lambda = 0.6, l_d = 3, Id = 0.1, theta_max = 70, hc = 2, xc = 2;
  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine simulate a calibration image");

  if (ac_grep(cur_ac_reg,"%im",&imr) != 1)
    return win_printf_OK("Cannot find image region");

  i = win_scanf("Creating a calibration image\n"
		"size in x%8d size in y %8d\n"
		"dx (in microns) %8f dy (in microns) %8f\n"
		"wavelength %8f coherence length %8f\n"
		" Difusive intensity %8f\n"
		"Max angle of light allow by the objective (in degrees)%8f\n"
		"critical radius %8d\n"
		"Height of bead %8f x shift %8f\n"
		,&onx,&ony,&dx, &dy, &lambda, &l_d, &Id,&theta_max,&rc,&hc,&xc);
  if (i == WIN_CANCEL)	return D_O_K;
  oid = talbot_image_tilted(onx, ony, dx, dy, lambda, l_d, Id, theta_max, rc,hc, xc);
  if (oid == NULL)	return win_printf_OK("unable to create image!");
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  return (refresh_image(imr, imr->n_oi - 1));
}



/* here it comes - the big bad ugly DIALOG array for our main dialog *
DIALOG pop_dialog[] =
{
  /* (dialog proc)     (x)   (y)   (w)   (h) (fg)(bg) (key) (flags)     (d1) (d2)    (dp)                   (dp2) (dp3) *
   
  /* this element just clears the screen, therefore it should come before the others *
  { d_myclear_proc,        0,   0,    400,    200,  0,  0,    0,      0,       0,   0,    NULL,                   NULL, NULL  },
  /* some more GUI elements, all of which require you to specify their dimensions *
  { d_button_proc,     60,  40,  160,   20,   0,  0,  't',      0,       0,   0,    "&Toggle Me!",          NULL, NULL  },
  { d_check_proc,      60,  70,  160,   20,   0,  0,  'c',      0,       0,   0,    "&Check Me!",           NULL, NULL  },
  { d_radio_proc,      60, 100,  160,   19,   0,  0,  's',      0,       0,   0,    "&Select Me!",          NULL, NULL  },
  { d_radio_proc,      220, 100,  160,   19,   0,  0,  'o',      0,       0,   0,    "&Or Me!",              NULL, NULL  },
  //   { d_yield_proc,        0,   0,    0,    0,   0,  0,    0,      0,       0,   0,    NULL,                   NULL, NULL  },
  { NULL,                0,   0,    0,    0,   0,  0,    0,      0,       0,   0,    NULL,                   NULL, NULL  }
};


int do_pop(void)
{
  int i;
  if(updating_menu_state != 0)	return D_O_K;

  position_dialog (pop_dialog, 20, 20);

  for (i = -1; i == -1; )
    {
   
      /* do the dialog *
      i = popup_dialog(pop_dialog, -1);
    }
  return D_O_K;
}


*/

