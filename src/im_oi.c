#ifndef _IM_OI_C_
#define _IM_OI_C_

# include "xvin/platform.h"
# include "im_oi.h"
# include "im_gr.h"
XV_FUNC(int, find_max_around,(float *x, int nx, int nmax, float *Max_pos, float *Max_val, float *Max_deriv));


# include <stdlib.h>
# include <string.h>
XV_FUNC(int, win_printf, (char *fmt, ...));


int (*image_special_2_use)(O_i *oi);
int (*image_use_2_special)(O_i *oi);
int (*image_free_use)(O_i *oi);
int (*image_duplicate_use)(O_i *oid, O_i *ois);
int (*switch_frame_of_disk_movie_c) (O_i *oi, int n);



int set_oi_mode(O_i *oi, int mode)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    oi->im.mode = mode;
    return 0;
}
int get_oi_mode(O_i *oi)
{
    if (oi == NULL)     xvin_error(Wrong_Argument);

    return (oi != NULL) ?   oi->im.mode : 0;
}
int set_oi_type(O_i *oi, int type)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    oi->im.data_type = type;
    return 0;
}
int get_oi_type(O_i *oi)
{
    if (oi == NULL)     xvin_error(Wrong_Argument);

    return (oi != NULL) ?   oi->im.data_type : 0;
}

int set_oi_region_of_interest_in_pixel(O_i *oi, int x0, int y0, int x1, int y1)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    oi->im.nxs = (x0 < oi->im.nx) && x0 >= 0 ? x0 : oi->im.nxs;
    oi->im.nxe = (x1 <= oi->im.nx) && x1 >= 0 ? x1 : oi->im.nxe;
    oi->im.nys = (y0 < oi->im.ny) && y0 >= 0 ? y0 : oi->im.nys;
    oi->im.nye = (y1 <= oi->im.ny) && y1 >= 0 ? y1 : oi->im.nye;
    oi->need_to_refresh |= ALL_NEED_REFRESH;
    return 0;
}
int get_oi_region_of_interest_in_pixel(O_i *oi, int *x0, int *y0, int *x1, int *y1)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    if (x0 != NULL)     *x0 = oi->im.nxs;

    if (x1 != NULL)     *x1 = oi->im.nxe;

    if (y0 != NULL)     *y0 = oi->im.nys;

    if (y1 != NULL)     *y1 = oi->im.nye;

    return 0;
}
int     set_oi_source(O_i *oi, char *format, ...)
{
    va_list ap;

    if (oi == NULL)     xvin_error(Wrong_Argument);

    if (format == NULL)
    {
        if (oi->im.source != NULL)  free(oi->im.source);

        oi->im.source = NULL;
    }
    else
    {
        va_start(ap, format);
        vset_formated_string(&(oi->im.source), format, ap);
        va_end(ap);
    }

    return 0;
}

int is_oi_a_movie(O_i *ois)
{
    int nfi;

    if (ois == NULL) return 0;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;
    return (nfi == 1) ? 1 : 0;
}

int nb_of_frames_in_movie(O_i *ois)
{
    int nfi;

    if (ois == NULL) return 0;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;
    return nfi;
}

int is_oi_a_color_image(O_i *ois)
{
    if (ois == NULL) return 0;

    if (ois->im.data_type == IS_RGB_PICTURE ||  ois->im.data_type == IS_RGBA_PICTURE
        || ois->im.data_type == IS_RGB16_PICTURE || ois->im.data_type == IS_RGBA16_PICTURE)
        return ois->im.data_type;
 else return 0;
}

int is_oi_a_complex_image(O_i *ois)
{
    if (ois == NULL) return 0;

    if (ois->im.data_type == IS_COMPLEX_IMAGE ||  ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
        return ois->im.data_type;
    else return 0;
}


char   *get_oi_source(O_i *oi)
{
    return (oi != NULL) ? oi->im.source : (char *) xvin_ptr_error(Wrong_Argument);
}

int     set_oi_history(O_i *oi, char *format, ...)
{
    va_list ap;

    if (oi == NULL)     xvin_error(Wrong_Argument);

    if (format == NULL)
    {
        if (oi->im.history != NULL) free(oi->im.history);

        oi->im.history = NULL;
    }
    else
    {
        va_start(ap, format);
        vset_formated_string(&(oi->im.history), format, ap);
        va_end(ap);
    }

    return 0;
}
char   *get_oi_history(O_i *oi)
{
    return (oi != NULL) ? oi->im.history : (char *)xvin_ptr_error(Wrong_Argument);
}
int     set_oi_treatement(O_i *oi, char *format, ...)
{
    va_list ap;

    if (oi == NULL)     xvin_error(Wrong_Argument);

    if (format == NULL)
    {
        if (oi->im.treatement != NULL)  free(oi->im.treatement);

        oi->im.treatement = NULL;
    }
    else
    {
        va_start(ap, format);
        vset_formated_string(&(oi->im.treatement), format, ap);
        va_end(ap);
    }

    return 0;
}
char   *get_oi_treatement(O_i *oi)
{
    return (oi != NULL) ? oi->im.treatement : (char *) xvin_ptr_error(Wrong_Argument);
}


int     set_oi_periodic_in_x(O_i *oi)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    oi->im.win_flag |= X_PER;
    return 0;
}
int     set_oi_periodic_in_y(O_i *oi)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    oi->im.win_flag |= Y_PER;
    return 0;
}
int     set_oi_periodic_in_x_and_y(O_i *oi)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    oi->im.win_flag |= X_PER + Y_PER;
    return 0;
}
int     set_oi_not_periodic_in_x(O_i *oi)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    oi->im.win_flag &= X_NOT_PER;
    return 0;
}
int     set_oi_not_periodic_in_y(O_i *oi)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    oi->im.win_flag &= Y_NOT_PER;
    return 0;
}
int     set_oi_not_periodic(O_i *oi)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    oi->im.win_flag &= X_NOT_PER & Y_NOT_PER;
    return 0;
}


int     get_oi_periodicity_in_x(O_i *oi)
{
    return (oi == NULL) ? xvin_error(Wrong_Argument) : \
           oi->im.win_flag & X_PER;
}
int     get_oi_periodicity_in_y(O_i *oi)
{
    return (oi == NULL) ? xvin_error(Wrong_Argument) : \
           oi->im.win_flag & Y_PER;
}

int set_oi_filename(O_i *oi, char *file)
{
    if (oi == NULL || file == NULL) return xvin_error(Wrong_Argument);

    if (oi->filename != NULL) free(oi->filename);

    oi->filename = strdup(file);
    return (oi->filename == NULL) ? xvin_error(Out_Of_Memory) : 0;
}
int set_oi_path(O_i *oi, char *dir)
{
    if (oi == NULL || dir == NULL) return xvin_error(Wrong_Argument);

    if (oi->dir != NULL) free(oi->dir);

    oi->dir = strdup(dir);
    return (oi->dir == NULL) ? xvin_error(Out_Of_Memory) : 0;
}
char *get_oi_filename(O_i *oi)
{
    return (oi != NULL) ? oi->filename : (char *) xvin_ptr_error(Wrong_Argument);
}
char *get_oi_path(O_i *oi)
{
    return (oi != NULL) ? oi->dir : (char *) xvin_ptr_error(Wrong_Argument);
}
char *get_oi_title(O_i *oi)
{
    return (oi != NULL) ? oi->title : (char *) xvin_ptr_error(Wrong_Argument);
}
char *get_oi_x_title(O_i *oi)
{
    return (oi != NULL) ? oi->x_title : (char *) xvin_ptr_error(Wrong_Argument);
}
char *get_oi_y_title(O_i *oi)
{
    return (oi != NULL) ? oi->y_title : (char *) xvin_ptr_error(Wrong_Argument);
}
char *get_oi_x_top_title(O_i *oi)
{
    return (oi != NULL) ? oi->x_prime_title : (char *) xvin_ptr_error(Wrong_Argument);
}
char *get_oi_y_right_title(O_i *oi)
{
    return (oi != NULL) ? oi->y_prime_title : (char *) xvin_ptr_error(Wrong_Argument);
}



int set_im_title(O_i *oi, char *format, ...)
{
    va_list ap;

    if (oi == NULL)     xvin_error(Wrong_Argument);

    if (format == NULL)
    {
        if (oi->title != NULL)  free(oi->title);

        oi->title = NULL;
    }
    else
    {
        va_start(ap, format);
        vset_formated_string(&(oi->title), format, ap);
        va_end(ap);
    }

    oi->need_to_refresh |= PLOT_NEED_REFRESH;
    return 0;
}

int set_im_x_title(O_i *oi, char *format, ...)
{
    va_list ap;

    if (oi == NULL)     xvin_error(Wrong_Argument);

    if (format == NULL)
    {
        if (oi->x_title != NULL)    free(oi->x_title);

        oi->x_title = NULL;
    }
    else
    {
        va_start(ap, format);
        vset_formated_string(&(oi->x_title), format, ap);
        va_end(ap);
    }

    oi->need_to_refresh |= PLOT_NEED_REFRESH;
    return 0;
}
int set_im_y_title(O_i *oi, char *format, ...)
{
    va_list ap;

    if (oi == NULL)     xvin_error(Wrong_Argument);

    if (format == NULL)
    {
        if (oi->y_title != NULL)    free(oi->y_title);

        oi->y_title = NULL;
    }
    else
    {
        va_start(ap, format);
        vset_formated_string(&(oi->y_title), format, ap);
        va_end(ap);
    }

    oi->need_to_refresh |= PLOT_NEED_REFRESH;
    return 0;
}
int set_im_x_top_title(O_i *oi, char *format, ...)
{
    va_list ap;

    if (oi == NULL)     xvin_error(Wrong_Argument);

    if (format == NULL)
    {
        if (oi->x_prime_title != NULL)  free(oi->x_prime_title);

        oi->x_prime_title = NULL;
    }
    else
    {
        va_start(ap, format);
        vset_formated_string(&(oi->x_prime_title), format, ap);
        va_end(ap);
    }

    oi->need_to_refresh |= PLOT_NEED_REFRESH;
    return 0;
}
int set_im_y_right_title(O_i *oi, char *format, ...)
{
    va_list ap;

    if (oi == NULL)     xvin_error(Wrong_Argument);

    if (format == NULL)
    {
        if (oi->y_prime_title != NULL)  free(oi->y_prime_title);

        oi->y_prime_title = NULL;
    }
    else
    {
        va_start(ap, format);
        vset_formated_string(&(oi->y_prime_title), format, ap);
        va_end(ap);
    }

    oi->need_to_refresh |= PLOT_NEED_REFRESH;
    return 0;
}


unsigned char *uchar_line_ptr(O_i *oi, int line)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx)
    {
        xvin_ptr_error(Wrong_Argument);
        return NULL;
    }

    return (oi)->im.pixel[(line)].ch;
}
short int *sint_line_ptr(O_i *oi, int line)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx)
    {
        xvin_ptr_error(Wrong_Argument);
        return NULL;
    }

    return (oi)->im.pixel[(line)].in;
}
unsigned short int *uint_line_ptr(O_i *oi, int line)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx)
    {
        xvin_ptr_error(Wrong_Argument);
        return NULL;
    }

    return (oi)->im.pixel[(line)].ui;
}
int *lint_line_ptr(O_i *oi, int line)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx)
    {
        xvin_ptr_error(Wrong_Argument);
        return NULL;
    }

    return (oi)->im.pixel[(line)].li;
}
float *flt_line_ptr(O_i *oi, int line)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx)
    {
        xvin_ptr_error(Wrong_Argument);
        return NULL;
    }

    return (oi)->im.pixel[(line)].fl;
}
double *dbl_line_ptr(O_i *oi, int line)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx)
    {
        xvin_ptr_error(Wrong_Argument);
        return NULL;
    }

    return (oi)->im.pixel[(line)].db;
}
rgb_t *rgb_line_ptr(O_i *oi, int line)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx)
    {
        xvin_ptr_error(Wrong_Argument);
        return NULL;
    }

    return (oi)->im.pixel[(line)].rgb;
}
rgba_t *rgba_line_ptr(O_i *oi, int line)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx)
    {
        xvin_ptr_error(Wrong_Argument);
        return NULL;
    }

    return (oi)->im.pixel[(line)].rgba;
}
rgb16_t *rgb16_line_ptr(O_i *oi, int line)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx)
    {
        xvin_ptr_error(Wrong_Argument);
        return NULL;
    }

    return (oi)->im.pixel[(line)].rgb16;
}
rgba16_t *rgba16_line_ptr(O_i *oi, int line)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx)
    {
        xvin_ptr_error(Wrong_Argument);
        return NULL;
    }

    return (oi)->im.pixel[(line)].rgba16;
}
mcomplex *cflt_line_ptr(O_i *oi, int line)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx)
    {
        xvin_ptr_error(Wrong_Argument);
        return NULL;
    }

    return (oi)->im.pixel[(line)].cp;
}
mdcomplex *cdbl_line_ptr(O_i *oi, int line)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx)
    {
        xvin_ptr_error(Wrong_Argument);
        return NULL;
    }

    return (oi)->im.pixel[(line)].dcp;
}


unsigned char get_uchar_pixel(O_i *oi, int line, int col)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx || col < 0 || col >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return 1;
    }

    return (oi)->im.pixel[(line)].ch[col];
}
short int get_sint_pixel(O_i *oi, int line, int col)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx || col < 0 || col >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return 1;
    }

    return (oi)->im.pixel[(line)].in[col];
}
unsigned short int get_uint_pixel(O_i *oi, int line, int col)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx || col < 0 || col >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return 1;
    }

    return (oi)->im.pixel[(line)].ui[col];
}
int get_lint_pixel(O_i *oi, int line, int col)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx || col < 0 || col >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return 1;
    }

    return (oi)->im.pixel[(line)].li[col];
}
float get_flt_pixel(O_i *oi, int line, int col)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx || col < 0 || col >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return 1;
    }

    return (oi)->im.pixel[(line)].fl[col];
}
double get_dbl_pixel(O_i *oi, int line, int col)
{
    if (oi == NULL || line < 0 || line >= oi->im.nx || col < 0 || col >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return 1;
    }

    return (oi)->im.pixel[(line)].db[col];
}
rgb_t get_rgb_pixel(O_i *oi, int line, int col)
{
    rgb_t trgb = {0, 0, 0};

    if (oi == NULL || line < 0 || line >= oi->im.nx || col < 0 || col >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return trgb;
    }

    return (oi)->im.pixel[(line)].rgb[col];
}
rgba_t get_rgba_pixel(O_i *oi, int line, int col)
{
    rgba_t trgba = {0, 0, 0, 0};

    if (oi == NULL || line < 0 || line >= oi->im.nx || col < 0 || col >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return  trgba;
    }

    return (oi)->im.pixel[(line)].rgba[col];
}
rgb16_t get_rgb16_pixel(O_i *oi, int line, int col)
{
    rgb16_t trgb = {0, 0, 0};

    if (oi == NULL || line < 0 || line >= oi->im.nx || col < 0 || col >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return  trgb;
    }

    return (oi)->im.pixel[(line)].rgb16[col];
}
rgba16_t get_rgba16_pixel(O_i *oi, int line, int col)
{
    rgba16_t trgba = {0, 0, 0, 0};

    if (oi == NULL || line < 0 || line >= oi->im.nx || col < 0 || col >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return  trgba;
    }

    return (oi)->im.pixel[(line)].rgba16[col];
}
mcomplex get_cflt_pixel(O_i *oi, int line, int col)
{
    mcomplex mc = {0, 0};

    if (oi == NULL || line < 0 || line >= oi->im.nx || col < 0 || col >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return mc;
    }

    return (oi)->im.pixel[(line)].cp[col];
}
mdcomplex get_cdbl_pixel(O_i *oi, int line, int col)
{
    mdcomplex mc = {0, 0};

    if (oi == NULL || line < 0 || line >= oi->im.nx || col < 0 || col >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return mc;
    }

    return (oi)->im.pixel[(line)].dcp[col];
}


int get_raw_pixel_value(O_i *oi, int ix, int iy, double *val)
{
    double zr, zi;

    if (oi == NULL || ix < 0 || iy < 0 || ix >= oi->im.nx || iy >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return 1;
    }

    switch (oi->im.data_type)
    {
    case  IS_CHAR_IMAGE:
        *val = (double)oi->im.pixel[iy].ch[ix];
        break;

    case  IS_RGB_PICTURE:
        if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
            *val = ((double)(oi->im.pixel[iy].rgb[ix].r + oi->im.pixel[iy].rgb[ix].g
                             + oi->im.pixel[iy].rgb[ix].b)) / 3;
        else if (oi->im.mode & R_LEVEL)
            *val = (double)oi->im.pixel[iy].rgb[ix].r;
        else if (oi->im.mode & G_LEVEL)
            *val = (double)oi->im.pixel[iy].rgb[ix].g;
        else if (oi->im.mode & B_LEVEL)
            *val = (double)oi->im.pixel[iy].rgb[ix].b;
        else if (oi->im.mode == ALPHA_LEVEL)
            *val = (double)255;
        else return 1;

        break;

    case  IS_RGBA_PICTURE:
        if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
            *val = ((double)(oi->im.pixel[iy].rgba[ix].r + oi->im.pixel[iy].rgba[ix].g
                             + oi->im.pixel[iy].rgba[ix].b)) / 3;
        else if (oi->im.mode & R_LEVEL)
            *val = (double)oi->im.pixel[iy].rgba[ix].r;
        else if (oi->im.mode & G_LEVEL)
            *val = (double)oi->im.pixel[iy].rgba[ix].g;
        else if (oi->im.mode & B_LEVEL)
            *val = (double)oi->im.pixel[iy].rgba[ix].b;
        else if (oi->im.mode == ALPHA_LEVEL)
            *val = (double)oi->im.pixel[iy].rgba[ix].a;
        else return 1;

        break;

    case  IS_RGB16_PICTURE:
        if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
            *val = ((double)(oi->im.pixel[iy].rgb16[ix].r + oi->im.pixel[iy].rgb16[ix].g
                             + oi->im.pixel[iy].rgb16[ix].b)) / 3;
        else if (oi->im.mode & R_LEVEL)
            *val = (double)oi->im.pixel[iy].rgb16[ix].r;
        else if (oi->im.mode & G_LEVEL)
            *val = (double)oi->im.pixel[iy].rgb16[ix].g;
        else if (oi->im.mode & B_LEVEL)
            *val = (double)oi->im.pixel[iy].rgb16[ix].b;
        else if (oi->im.mode == ALPHA_LEVEL)
            *val = (double)255;
        else return 1;

        break;

    case  IS_RGBA16_PICTURE:
        if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
            *val = ((double)(oi->im.pixel[iy].rgba16[ix].r + oi->im.pixel[iy].rgba16[ix].g
                             + oi->im.pixel[iy].rgba16[ix].b)) / 3;
        else if (oi->im.mode & R_LEVEL)
            *val = (double)oi->im.pixel[iy].rgba16[ix].r;
        else if (oi->im.mode & G_LEVEL)
            *val = (double)oi->im.pixel[iy].rgba16[ix].g;
        else if (oi->im.mode & B_LEVEL)
            *val = (double)oi->im.pixel[iy].rgba16[ix].b;
        else if (oi->im.mode == ALPHA_LEVEL)
            *val = (double)oi->im.pixel[iy].rgba16[ix].a;
        else return 1;

        break;

    case  IS_INT_IMAGE:
        *val = (double)oi->im.pixel[iy].in[ix];
        break;

    case  IS_UINT_IMAGE:
        *val = (double)oi->im.pixel[iy].ui[ix];
        break;

    case  IS_LINT_IMAGE:
        *val = (double)oi->im.pixel[iy].li[ix];
        break;

    case  IS_FLOAT_IMAGE:
        *val = (double)oi->im.pixel[iy].fl[ix];
        break;

    case  IS_DOUBLE_IMAGE:
        *val = oi->im.pixel[iy].db[ix];
        break;

    case  IS_COMPLEX_IMAGE:
        zr = oi->im.pixel[iy].cp[ix].re;
        zi = oi->im.pixel[iy].cp[ix].im;

        switch (oi->im.mode)
        {
        case AMP:
            *val = sqrt(zr * zr + zi * zi);
            break;

        case LOG_AMP:
            *val = zr * zr + zi * zi;
            *val = (*val > 0) ? log10(*val) : -40;
            break;

        case AMP_2:
            *val = zr * zr + zi * zi;
            break;

        case RE:
            *val = zr;
            break;

        case IM:
            *val = zi;
            break;
        };

        break;

    case  IS_COMPLEX_DOUBLE_IMAGE:
        zr = oi->im.pixel[iy].dcp[ix].dre;
        zi = oi->im.pixel[iy].dcp[ix].dim;

        switch (oi->im.mode)
        {
        case AMP:
            *val = sqrt(zr * zr + zi * zi);
            break;

        case LOG_AMP:
            *val = zr * zr + zi * zi;
            *val = (*val > 0) ? log10(*val) : -80;
            break;

        case AMP_2:
            *val = zr * zr + zi * zi;
            break;

        case RE:
            *val = zr;
            break;

        case IM:
            *val = zi;
            break;
        };

        break;

    default:
        return 1;
    }

    return 0;
}


int set_raw_pixel_value(O_i *oi, int ix, int iy, double *val)
{

    if (oi == NULL || ix < 0 || iy < 0 || ix >= oi->im.nx || iy >= oi->im.ny)
    {
        xvin_error(Wrong_Argument);
        return 1;
    }

    switch (oi->im.data_type)
    {
    case  IS_CHAR_IMAGE:
        oi->im.pixel[iy].ch[ix] = (unsigned char)val[0];
        break;

    case  IS_RGB_PICTURE:
    {
        oi->im.pixel[iy].rgb[ix].r = (unsigned char)val[0];
        oi->im.pixel[iy].rgb[ix].g = (unsigned char)val[1];
        oi->im.pixel[iy].rgb[ix].b = (unsigned char)val[2];
    }
    break;

    case  IS_RGBA_PICTURE:
    {
        oi->im.pixel[iy].rgba[ix].r = (unsigned char)val[0];
        oi->im.pixel[iy].rgba[ix].g = (unsigned char)val[1];
        oi->im.pixel[iy].rgba[ix].b = (unsigned char)val[2];
        oi->im.pixel[iy].rgba[ix].a = (unsigned char)val[3];
    }
    break;

    case  IS_RGB16_PICTURE:
    {
        oi->im.pixel[iy].rgb16[ix].r = (short int)val[0];
        oi->im.pixel[iy].rgb16[ix].g = (short int)val[1];
        oi->im.pixel[iy].rgb16[ix].b = (short int)val[2];
    }
    break;

    case  IS_RGBA16_PICTURE:
    {
        oi->im.pixel[iy].rgba16[ix].r = (short int)val[0];
        oi->im.pixel[iy].rgba16[ix].g = (short int)val[1];
        oi->im.pixel[iy].rgba16[ix].b = (short int)val[2];
        oi->im.pixel[iy].rgba16[ix].a = (short int)val[3];
    }
    break;

    case  IS_INT_IMAGE:
        oi->im.pixel[iy].in[ix] = (short int)val[0];
        break;

    case  IS_UINT_IMAGE:
        oi->im.pixel[iy].ui[ix] = (unsigned short int)val[0];
        break;

    case  IS_LINT_IMAGE:
        oi->im.pixel[iy].li[ix] = (int)val[0];
        break;

    case  IS_FLOAT_IMAGE:
        oi->im.pixel[iy].fl[ix] = (float)val[0];
        break;

    case  IS_DOUBLE_IMAGE:
        oi->im.pixel[iy].db[ix] = val[0];
        break;

    case  IS_COMPLEX_IMAGE:
        oi->im.pixel[iy].cp[ix].re = (float)val[0];
        oi->im.pixel[iy].cp[ix].im = (float)val[1];
        break;

    case  IS_COMPLEX_DOUBLE_IMAGE:
        oi->im.pixel[iy].dcp[ix].dre = val[0];
        oi->im.pixel[iy].dcp[ix].dim = val[1];
        break;

    default:
        return 1;
    }

    return 0;
}


int     copy_one_subset_of_image_in_a_bigger_one(O_i *dest, int posx, int posy, O_i *ois, int x_start, int y_start,         int nx, int ny)
{
  return copy_or_add_one_subset_of_image_in_a_bigger_one(dest, posx, posy, ois, x_start, y_start,  nx, ny, 0);

}

int     copy_or_add_one_subset_of_image_in_a_bigger_one(O_i *dest, int posx, int posy, O_i *ois, int x_start, int y_start,  int nx, int ny, int add)
{
    union pix *pd = NULL, *ps = NULL;
    int i, j, onx, ony, donx, dony;

    if (ois == NULL || dest == NULL)                       return 1;

    if (ois->im.data_type != dest->im.data_type)           return 2;

    pd = dest->im.pixel;
    ps = ois->im.pixel;
    onx = ois->im.nx;
    ony = ois->im.ny;
    donx = dest->im.nx;
    dony = dest->im.ny;

    if (posx < 0 || posy < 0 || x_start < 0 || y_start < 0)                        return 3;

    if ((x_start + nx > onx) || (y_start + ny > ony))  return 4;

    //win_printf("Desti -> %d %d\nori %d %d %d %d",posx, posy, x_start,  y_start, nx, ny);

    if (ois->im.data_type == IS_CHAR_IMAGE)
    {
        for (i = 0; i < ny && i + y_start < ony && i + posy < dony; i++)
        {
	  if (add)
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].ch[posx + j] += ps[i + y_start].ch[j + x_start];
	    }
	  else
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].ch[posx + j] = ps[i + y_start].ch[j + x_start];
	    }
        }
    }
    else if (ois->im.data_type == IS_RGB_PICTURE)
    {
        for (i = 0; i < ny && i + y_start < ony && i + posy < dony; i++)
        {
	  if (add)
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
		{
		  pd[posy + i].rgb[posx + j].r += ps[i + y_start].rgb[j + x_start].r;
		  pd[posy + i].rgb[posx + j].g += ps[i + y_start].rgb[j + x_start].g;
		  pd[posy + i].rgb[posx + j].b += ps[i + y_start].rgb[j + x_start].b;
		}
	    }
	  else
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].rgb[posx + j] = ps[i + y_start].rgb[j + x_start];
	    }
        }
    }
    else if (ois->im.data_type == IS_RGBA_PICTURE)
    {
        for (i = 0; i < ny && i + y_start < ony && i + posy < dony; i++)
        {
	  if (add)
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
		{
		  pd[posy + i].rgba[posx + j].r += ps[i + y_start].rgba[j + x_start].r;
		  pd[posy + i].rgba[posx + j].g += ps[i + y_start].rgba[j + x_start].g;
		  pd[posy + i].rgba[posx + j].b += ps[i + y_start].rgba[j + x_start].b;
		  pd[posy + i].rgba[posx + j].a += ps[i + y_start].rgba[j + x_start].a;
		}
	    }
	  else
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].rgba[posx + j] = ps[i + y_start].rgba[j + x_start];
	    }
        }
    }
    else if (ois->im.data_type == IS_RGB16_PICTURE)
    {
        for (i = 0; i < ny && i + y_start < ony && i + posy < dony; i++)
        {
	  if (add)
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
		{
		  pd[posy + i].rgb16[posx + j].r += ps[i + y_start].rgb16[j + x_start].r;
		  pd[posy + i].rgb16[posx + j].g += ps[i + y_start].rgb16[j + x_start].g;
		  pd[posy + i].rgb16[posx + j].b += ps[i + y_start].rgb16[j + x_start].b;
		}
	    }
	  else
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].rgb16[posx + j] = ps[i + y_start].rgb16[j + x_start];
	    }
        }
    }
    else if (ois->im.data_type == IS_RGBA16_PICTURE)
    {
        for (i = 0; i < ny && i + y_start < ony && i + posy < dony; i++)
        {
	  if (add)
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
		{
		  pd[posy + i].rgba16[posx + j].r += ps[i + y_start].rgba16[j + x_start].r;
		  pd[posy + i].rgba16[posx + j].g += ps[i + y_start].rgba16[j + x_start].g;
		  pd[posy + i].rgba16[posx + j].b += ps[i + y_start].rgba16[j + x_start].b;
		  pd[posy + i].rgba16[posx + j].a += ps[i + y_start].rgba16[j + x_start].a;
		}
	    }
	  else
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].rgba16[posx + j] = ps[i + y_start].rgba16[j + x_start];
	    }
        }
    }
    else if (ois->im.data_type == IS_INT_IMAGE)
    {
        for (i = 0; i < ny && i + y_start < ony && i + posy < dony; i++)
        {
	  if (add)
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].in[posx + j] += ps[i + y_start].in[j + x_start];
	    }
	  else
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].in[posx + j] = ps[i + y_start].in[j + x_start];
	    }
        }
    }
    else if (ois->im.data_type == IS_UINT_IMAGE)
    {
        for (i = 0; i < ny && i + y_start < ony && i + posy < dony; i++)
        {
	  if (add)
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].ui[posx + j] += ps[i + y_start].ui[j + x_start];
	    }
	  else
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].ui[posx + j] = ps[i + y_start].ui[j + x_start];
	    }
        }
    }
    else if (ois->im.data_type == IS_LINT_IMAGE)
    {
        for (i = 0; i < ny && i + y_start < ony && i + posy < dony; i++)
        {
	  if (add)
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].li[posx + j] += ps[i + y_start].li[j + x_start];
	    }
	  else
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].li[posx + j] = ps[i + y_start].li[j + x_start];
	    }

        }
    }
    else if (ois->im.data_type == IS_FLOAT_IMAGE)
    {
        for (i = 0; i < ny && i + y_start < ony && i + posy < dony; i++)
        {
	  if (add)
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].fl[posx + j] += ps[i + y_start].fl[j + x_start];
	    }
	  else
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].fl[posx + j] = ps[i + y_start].fl[j + x_start];
	    }
        }
    }
    else if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        for (i = 0; i < ny && i + y_start < ony && i + posy < dony; i++)
        {
	  if (add)
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
		{
		  pd[posy + i].cp[posx + j].re += ps[i + y_start].cp[j + x_start].re;
		  pd[posy + i].cp[posx + j].im += ps[i + y_start].cp[j + x_start].im;
		}
	    }
	  else
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].cp[posx + j] = ps[i + y_start].cp[j + x_start];
	    }
        }
    }
    else if (ois->im.data_type == IS_DOUBLE_IMAGE)
    {
        for (i = 0; i < ny && i + y_start < ony && i + posy < dony; i++)
        {
	  if (add)
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].db[posx + j] += ps[i + y_start].db[j + x_start];
	    }
	  else
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].db[posx + j] = ps[i + y_start].db[j + x_start];
	    }
        }
    }
    else if (ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        for (i = 0; i < ny && i + y_start < ony && i + posy < dony; i++)
        {
	  if (add)
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
		{
		  pd[posy + i].dcp[posx + j].dre += ps[i + y_start].dcp[j + x_start].dre;
		  pd[posy + i].dcp[posx + j].dim += ps[i + y_start].dcp[j + x_start].dim;
		}
	    }
	  else
	    {
	      for (j = 0; j < nx && j + x_start < onx && j + posx < donx ; j++)
                pd[posy + i].dcp[posx + j] = ps[i + y_start].dcp[j + x_start];
	    }

        }
    }

    return 0;
}


int     copy_and_swap_one_subset_of_image_in_a_bigger_one(O_i *dest, int posx, int posy, O_i *ois, int x_start,
        int y_start, int nx, int ny)
{
    union pix *pd = NULL, *ps = NULL;
    int i, j, onx, ony, donx, dony;

    if (ois == NULL || dest == NULL)                       return 1;

    if (ois->im.data_type != dest->im.data_type)           return 2;

    pd = dest->im.pixel;
    ps = ois->im.pixel;
    onx = ois->im.nx;
    ony = ois->im.ny;
    donx = dest->im.nx;
    dony = dest->im.ny;

    if (posx < 0 || posy < 0 || x_start < 0 || y_start < 0)                        return 3;

    if ((x_start + nx > onx) || (y_start + ny > ony))  return 4;

    //win_printf("Desti -> %d %d\nori %d %d %d %d",posx, posy, x_start,  y_start, nx, ny);

    if (ois->im.data_type == IS_CHAR_IMAGE)
    {
        for (i = 0; i < nx && i + x_start < onx && i + posy < dony; i++)
        {
            for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
                pd[posy + i].ch[posx + j] = ps[j + y_start].ch[i + x_start];
        }
    }
    else if (ois->im.data_type == IS_RGB_PICTURE)
    {
        for (i = 0; i < nx && i + x_start < onx && i + posy < dony; i++)
        {
            for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
                pd[posy + i].rgb[posx + j] = ps[j + y_start].rgb[i + x_start];
        }
    }
    else if (ois->im.data_type == IS_RGBA_PICTURE)
    {
        for (i = 0; i < nx && i + x_start < onx && i + posy < dony; i++)
        {
            for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
                pd[posy + i].rgba[posx + j] = ps[j + y_start].rgba[i + x_start];
        }
    }
    else if (ois->im.data_type == IS_RGB16_PICTURE)
    {
        for (i = 0; i < nx && i + x_start < onx && i + posy < dony; i++)
        {
            for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
                pd[posy + i].rgb16[posx + j] = ps[j + y_start].rgb16[i + x_start];
        }
    }
    else if (ois->im.data_type == IS_RGBA16_PICTURE)
    {
        for (i = 0; i < nx && i + x_start < onx && i + posy < dony; i++)
        {
            for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
                pd[posy + i].rgba16[posx + j] = ps[j + y_start].rgba16[i + x_start];
        }
    }
    else if (ois->im.data_type == IS_INT_IMAGE)
    {
        for (i = 0; i < nx && i + x_start < onx && i + posy < dony; i++)
        {
            for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
                pd[posy + i].in[posx + j] = ps[j + y_start].in[i + x_start];
        }
    }
    else if (ois->im.data_type == IS_UINT_IMAGE)
    {
        for (i = 0; i < nx && i + x_start < onx && i + posy < dony; i++)
        {
            for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
                pd[posy + i].ui[posx + j] = ps[j + y_start].ui[i + x_start];
        }
    }
    else if (ois->im.data_type == IS_LINT_IMAGE)
    {
        for (i = 0; i < nx && i + x_start < onx && i + posy < dony; i++)
        {
            for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
                pd[posy + i].li[posx + j] = ps[j + y_start].li[i + x_start];
        }
    }
    else if (ois->im.data_type == IS_FLOAT_IMAGE)
    {
        for (i = 0; i < nx && i + x_start < onx && i + posy < dony; i++)
        {
            for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
                pd[posy + i].fl[posx + j] = ps[j + y_start].fl[i + x_start];
        }
    }
    else if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        for (i = 0; i < nx && i + x_start < onx && i + posy < dony; i++)
        {
            for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
                pd[posy + i].cp[posx + j] = ps[j + y_start].cp[i + x_start];
        }
    }
    else if (ois->im.data_type == IS_DOUBLE_IMAGE)
    {
        for (i = 0; i < nx && i + x_start < onx && i + posy < dony; i++)
        {
            for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
                pd[posy + i].db[posx + j] = ps[j + y_start].db[i + x_start];
        }
    }
    else if (ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        for (i = 0; i < nx && i + x_start < onx && i + posy < dony; i++)
        {
            for (j = 0; j < ny && j + y_start < ony && j + posx < donx ; j++)
                pd[posy + i].dcp[posx + j] = ps[j + y_start].dcp[i + x_start];
        }
    }

    return 0;
}


int     set_all_pixel_to_color(O_i *dest, int bw, double flt_dbl_val)
{
    union pix *pd = NULL;
    int i, j, donx, dony, color;
    float fcolor;
    double dcolor;

    pd = dest->im.pixel;
    donx = dest->im.nx;
    dony = dest->im.ny;

    if (dest->im.data_type == IS_CHAR_IMAGE)
    {
        color = bw;//(bw) ? 255 : 0;

        for (i = 0; i < dony; i++)
        {
            for (j = 0; j < donx ; j++)
                pd[i].ch[j] = color;
        }
    }
    else if (dest->im.data_type == IS_RGB_PICTURE)
    {
        color = bw;//(bw) ? 255 : 0;

        for (i = 0; i < dony; i++)
        {
            for (j = 0; j < 3 * donx ; j++)
                pd[i].ch[j] = color;
        }
    }
    else if (dest->im.data_type == IS_RGBA_PICTURE)
    {
        color = bw;//(bw) ? 255 : 0;

        for (i = 0; i < dony; i++)
        {
            for (j = 0; j < 4 * donx ; j++)
                pd[i].ch[j] = color;
        }
    }
    else if (dest->im.data_type == IS_RGB16_PICTURE)
    {
        color = bw;//(bw) ? 65535 : 0;

        for (i = 0; i < dony; i++)
        {
            for (j = 0; j < 3 * donx ; j++)
                pd[i].ui[j] = color;
        }
    }
    else if (dest->im.data_type == IS_RGBA16_PICTURE)
    {
        color = bw;//(bw) ? 65535 : 0;

        for (i = 0; i < dony; i++)
        {
            for (j = 0; j < 4 * donx ; j++)
                pd[i].ui[j] = color;
        }
    }
    else if (dest->im.data_type == IS_INT_IMAGE)
    {
        color = bw;//(bw) ? 32767 : -32768;

        for (i = 0; i < dony; i++)
        {
            for (j = 0; j < donx ; j++)
                pd[i].in[j] = color;
        }
    }
    else if (dest->im.data_type == IS_UINT_IMAGE)
    {
        color = bw;//(bw) ? 65535 : 0;

        for (i = 0; i < dony; i++)
        {
            for (j = 0; j < donx ; j++)
                pd[i].ui[j] = color;
        }
    }
    else if (dest->im.data_type == IS_LINT_IMAGE)
    {
        color = bw;//(bw) ? 0x7FFFFFFF : 0x10000000;

        for (i = 0; i < dony; i++)
        {
            for (j = 0; j < donx ; j++)
                pd[i].li[j] = color;
        }
    }
    else if (dest->im.data_type == IS_FLOAT_IMAGE)
    {
        fcolor = (float)flt_dbl_val;

        for (i = 0; i < dony; i++)
        {
            for (j = 0; j < donx ; j++)
                pd[i].fl[j] = fcolor;
        }
    }
    else if (dest->im.data_type == IS_COMPLEX_IMAGE)
    {
        fcolor = flt_dbl_val;

        for (i = 0; i < dony; i++)
        {
            for (j = 0; j < 2 * donx ; j++)
                pd[i].fl[j] = fcolor;
        }
    }
    else if (dest->im.data_type == IS_DOUBLE_IMAGE)
    {
        dcolor = flt_dbl_val;

        for (i = 0; i < dony; i++)
        {
            for (j = 0; j < donx ; j++)
                pd[i].db[j] = dcolor;
        }
    }
    else if (dest->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        dcolor = flt_dbl_val;

        for (i = 0; i < dony; i++)
        {
            for (j = 0; j < 2 * donx ; j++)
                pd[i].db[j] = dcolor;
        }
    }

    return 0;
}





O_i *create_one_image(int nx, int ny, int data_type)
{
    O_i *oi = NULL;

    if ((oi = (O_i *)calloc(1, sizeof(O_i))) == NULL)
        return (O_i *) xvin_ptr_error(Out_Of_Memory);

    if (init_one_image(oi, data_type))      return NULL;

    if (alloc_one_image(oi, nx, ny, data_type))
        return (O_i *) xvin_ptr_error(Out_Of_Memory);

    return oi;
}
O_i *create_one_movie(int nx, int ny, int data_type, int n_frames)
{
    O_i *oi = NULL;

    if ((oi = (O_i *)calloc(1, sizeof(O_i))) == NULL)
        return (O_i *) xvin_ptr_error(Out_Of_Memory);

    if (init_one_image(oi, data_type))      return NULL;

    oi->im.n_f = n_frames;
    oi->im.multi_page = 1;
    if (alloc_one_image(oi, nx, ny, data_type))
        return (O_i *) xvin_ptr_error(Out_Of_Memory);

    return oi;
}

O_i *create_one_continuous_movie(int nx, int ny, int data_type, int n_frames)
{
    O_i *oi = NULL;

    if ((oi = (O_i *)calloc(1, sizeof(O_i))) == NULL)
        return (O_i *) xvin_ptr_error(Out_Of_Memory);

    if (init_one_image(oi, data_type))      return NULL;

    oi->im.n_f = n_frames;
    if (alloc_one_image(oi, nx, ny, data_type))
        return (O_i *) xvin_ptr_error(Out_Of_Memory);

    return oi;
}


int free_one_image(O_i *oi)
{
    int i, j;
    int data, nf;
    union pix *p = NULL;

    if (oi == NULL)	return xvin_error(Wrong_Argument);

    if (oi->im.n_f > 0 && oi->im.pxl != NULL)   p = oi->im.pxl[0];
    else                        p = oi->im.pixel;

    data = oi->im.data_type;

    if (p != NULL)
    {
        if (data == IS_CHAR_IMAGE && p[0].ch != NULL)       free(p[0].ch);
        else if (data == IS_INT_IMAGE && p[0].in != NULL)   free(p[0].in);
        else if (data == IS_RGB_PICTURE && p[0].ch != NULL) free(p[0].ch);
        else if (data == IS_RGB16_PICTURE && p[0].ch != NULL)   free(p[0].ch);
        else if (data == IS_FLOAT_IMAGE && p[0].fl != NULL) free(p[0].fl);
        else if (data == IS_COMPLEX_IMAGE && p[0].fl != NULL)   free(p[0].fl);
        else if (data == IS_UINT_IMAGE && p[0].ui != NULL)  free(p[0].ui);
        else if (data == IS_LINT_IMAGE && p[0].li != NULL)  free(p[0].li);
        else if (data == IS_RGBA_PICTURE && p[0].ch != NULL)    free(p[0].ch);
        else if (data == IS_RGBA16_PICTURE && p[0].ch != NULL)  free(p[0].ch);
        else if (data == IS_DOUBLE_IMAGE && p[0].db != NULL)    free(p[0].db);
        else if (data == IS_COMPLEX_DOUBLE_IMAGE && p[0].db != NULL)    free(p[0].db);
    }

    /*
    if (oi->im.iparam != NULL) free(oi->im.iparam);
    oi->im.iparam = NULL;
    if (oi->im.i1param != NULL) free(oi->im.i1param);
    oi->im.i1param = NULL;
    if (oi->im.i2param != NULL) free(oi->im.i2param);
    oi->im.i2param = NULL;
    if (oi->im.i3param != NULL) free(oi->im.i3param);
    oi->im.i3param = NULL;
    if (oi->im.fparam != NULL) free(oi->im.fparam);
    oi->im.fparam = NULL;
    */
    if (p != NULL)  free(p);

    if (oi->im.pxl != NULL)             free(oi->im.pxl);

    if (oi->im.mem != NULL)             free(oi->im.mem);

    if (oi->im.over != NULL)
    {
        if (oi->im.over[0] != NULL)     free(oi->im.over[0]);

        free(oi->im.over);
    }

    if (oi->im.special != NULL)
    {
        for (i = 0 ; i < oi->im.n_special ; i++)
        {
            if (oi->im.special[i] != NULL)
                free(oi->im.special[i]);
        }

        if (oi->im.special)  free(oi->im.special);
    }

    if (oi->im.source != NULL)          free(oi->im.source);

    if (oi->im.history != NULL)         free(oi->im.history);

    if (oi->im.treatement != NULL)      free(oi->im.treatement);

    if (oi->filename != NULL)           free(oi->filename);

    if (oi->dir != NULL)                free(oi->dir);

    if (oi->title != NULL)          free(oi->title);

    if (oi->x_title != NULL)            free(oi->x_title);

    if (oi->y_title != NULL)            free(oi->y_title);

    if (oi->x_prime_title != NULL)  free(oi->x_prime_title);

    if (oi->y_prime_title != NULL)  free(oi->y_prime_title);

    if (oi->x_prefix != NULL)       free(oi->x_prefix);

    if (oi->y_prefix != NULL)       free(oi->y_prefix);

    if (oi->x_unit != NULL)         free(oi->x_unit);

    if (oi->y_unit != NULL)         free(oi->y_unit);

    if (oi->z_prefix != NULL)       free(oi->z_prefix);

    if (oi->t_prefix != NULL)       free(oi->t_prefix);

    if (oi->z_unit != NULL)         free(oi->z_unit);

    if (oi->t_unit != NULL)         free(oi->t_unit);

    for (i = 0 ; i < oi->n_lab ; i++)
        if (oi->lab[i]->text != NULL)   free(oi->lab[i]->text);

    free(oi->lab);

    for (i = 0 ; i < oi->n_op ; i++)
        if (oi->o_p[i] != NULL)         free_one_plot(oi->o_p[i]);

    if (oi->o_p != NULL)    free(oi->o_p);

    if (image_free_use != NULL)         image_free_use(oi);

    nf = (oi->im.n_f > 0) ? oi->im.n_f : 1;

    //if (nf != 1) win_printf("free nf = %d",nf);
    for (i = 0 ; i < nf ; i++)
    {
        if (oi->im.user_ispare != NULL && oi->im.user_ispare[i] != NULL)
        {
            free(oi->im.user_ispare[i]);
            oi->im.user_ispare[i] = NULL;
        }

        if (oi->im.user_fspare != NULL && oi->im.user_fspare[i] != NULL)
        {
            free(oi->im.user_fspare[i]);
            oi->im.user_fspare[i] = NULL;
        }
    }

    if (oi->im.user_ispare != NULL) free(oi->im.user_ispare);

    oi->im.user_ispare = NULL;

    if (oi->im.user_fspare != NULL) free(oi->im.user_fspare);

    oi->im.user_fspare = NULL;




    if (oi->im.s_l != NULL && oi->im.m_sl != NULL && oi->im.n_sl != NULL)
    {
        for (i = 0 ; i < nf ; i++)
        {
            for (j = 0 ; j < oi->im.n_sl[i] ; j++)
            {
                if (i == 0) win_printf("nf %d i %d j %d", nf, i, j);

                if (oi->im.s_l[i][j]->text != NULL)  free(oi->im.s_l[i][j]->text);

                free(oi->im.s_l[i][j]);
            }

            if (oi->im.s_l[i])  free(oi->im.s_l[i]);
        }

        free(oi->im.s_l);
        free(oi->im.m_sl);
        free(oi->im.n_sl);
    }

    if (oi->xu != NULL)
    {
        for (i = 0 ; i < oi->n_xu ; i++)   free_unit_set(oi->xu[i]);

        free(oi->xu);
    }

    if (oi->yu != NULL)
    {
        for (i = 0 ; i < oi->n_yu ; i++)   free_unit_set(oi->yu[i]);

        free(oi->yu);
    }

    if (oi->zu != NULL)
    {
        for (i = 0 ; i < oi->n_zu ; i++)   free_unit_set(oi->zu[i]);

        free(oi->zu);
    }

    for (i = 0 ; i < 8 ; i++)
    {
        if (oi->im.src_parameter_type[i] != NULL)
        {
            free(oi->im.src_parameter_type[i]);
            oi->im.src_parameter_type[i] = NULL;
        }
    }

    free(oi);
    oi = NULL;
    return 0;
}
/*  O_i *duplicate_image(O_i *src, O_i *dest);
 *  DESCRIPTION Copy the data of src one_image to dest,
 *          if dest == NULL allocate space for the new image
 *
 *
 *  RETURNS     the dest one_image on success, NULL otherwise
 *
 */
O_i *duplicate_image_or_movie(O_i *src, O_i *dest, int ask)
{
    int i, j;
    int ii, jj, n_frames = 1;
    union pix *pd = NULL, *ps = NULL;
    p_l *sp = NULL, *dp = NULL;
    un_s *us = NULL;

    if (src == NULL)        return (O_i *) xvin_ptr_error(Wrong_Argument);

    if (dest == NULL)
    {
        if ((dest = (O_i *)calloc(1, sizeof(O_i))) == NULL)
            return (O_i *) xvin_ptr_error(Out_Of_Memory);

        init_one_image(dest, src->im.data_type);
    }

    dest->width = src->width;
    dest->height = src->height;
    dest->right = src->right;
    dest->up = src->up;
    dest->iopt = src->iopt;
    dest->iopt2 = src->iopt2;
    dest->tick_len = src->tick_len;
    dest->x_lo = src->x_lo;
    dest->x_hi = src->x_hi;
    dest->y_lo = src->y_lo;
    dest->y_hi = src->y_hi;
    dest->z_black = src->z_black;
    dest->z_white = src->z_white;
    dest->z_min = src->z_min;
    dest->z_max = src->z_max;
    dest->z_Rmin = src->z_Rmin;
    dest->z_Rmax = src->z_Rmax;
    dest->z_Gmin = src->z_Gmin;
    dest->z_Gmax = src->z_Gmax;
    dest->z_Bmin = src->z_Bmin;
    dest->z_Bmax = src->z_Bmax;
    dest->ax = src->ax;
    dest->dx = src->dx;
    dest->ay = src->ay;
    dest->dy = src->dy;
    dest->az = src->az;
    dest->dz = src->dz;
    dest->at = src->at;
    dest->dt = src->dt;
    dest->im.time = src->im.time;
    dest->im.win_flag = src->im.win_flag;
    dest->im.has_sqaure_pixel = src->im.has_sqaure_pixel;
    dest->title = Mystrdup(src->title);
    dest->x_title = Mystrdup(src->x_title);
    dest->y_title = Mystrdup(src->y_title);
    dest->x_prime_title = Mystrdup(src->x_prime_title);
    dest->y_prime_title = Mystrdup(src->y_prime_title);
    dest->x_prefix = Mystrdup(src->x_prefix);
    dest->y_prefix = Mystrdup(src->y_prefix);
    dest->x_unit = Mystrdup(src->x_unit);
    dest->y_unit = Mystrdup(src->y_unit);
    dest->z_prefix = Mystrdup(src->z_prefix);
    dest->t_prefix = Mystrdup(src->t_prefix);
    dest->z_unit = Mystrdup(src->z_unit);
    dest->t_unit = Mystrdup(src->t_unit);
    dest->im.source = Mystrdup(src->im.source);
    dest->im.history = Mystrdup(src->im.history);
    dest->im.treatement = Mystrdup(src->im.treatement);
    dest->im.record_duration = src->im.record_duration;

    /*
    dest->im.user_nipar = src->im.user_nipar;
    dest->im.user_nfpar = src->im.user_nfpar;
    */
    if (dest->im.special != NULL)
        remove_from_one_image(dest, ALL_SPECIAL, dest->im.special);

    if (src->im.special != NULL)
        for (i = 0 ; i < src->im.n_special ; i++)
            add_to_one_image(dest, IS_SPECIAL, src->im.special[i]);

    if (src->im.n_f > 0)
    {
        if (ask == 1)
        {
            i = win_printf("You are duplicating a movie with %d images\n"
                           "if you want to duplicate the entire movie press OK\n"
                           "if you want to copy a single frame press WIN_CANCEL", src->im.n_f);

            if (i ==WIN_OK)  n_frames = dest->im.n_f = src->im.n_f;
        }
        else if (ask == 0) n_frames = dest->im.n_f = src->im.n_f;
    }

    alloc_one_image(dest, src->im.nx, src->im.ny, src->im.data_type);
    pd = dest->im.pixel;
    ps = (n_frames > 1) ? src->im.pxl[0] : src->im.pixel;

    for (i = 0, ii = 0 ; i < n_frames * src->im.ny ; i++, ii++)
    {
        for (j = 0, jj = 0 ; j < src->im.nx ; j++, jj++)
        {
            switch (src->im.data_type)
            {
            case  IS_CHAR_IMAGE:
                pd[ii].ch[jj] = ps[i].ch[j];
                break;

            case  IS_RGB_PICTURE:
                pd[ii].ch[3 * jj] = ps[i].ch[3 * j];
                pd[ii].ch[(3 * jj) + 1] = ps[i].ch[(3 * j) + 1];
                pd[ii].ch[(3 * jj) + 2] = ps[i].ch[(3 * j) + 2];
                break;

            case  IS_RGBA_PICTURE:
                pd[ii].ch[3 * jj] = ps[i].ch[3 * j];
                pd[ii].ch[(3 * jj) + 1] = ps[i].ch[(3 * j) + 1];
                pd[ii].ch[(3 * jj) + 2] = ps[i].ch[(3 * j) + 2];
                pd[ii].ch[(3 * jj) + 3] = ps[i].ch[(3 * j) + 3];
                break;

            case  IS_RGB16_PICTURE:
                pd[ii].in[3 * jj] = ps[i].in[3 * j];
                pd[ii].in[(3 * jj) + 1] = ps[i].in[(3 * j) + 1];
                pd[ii].in[(3 * jj) + 2] = ps[i].in[(3 * j) + 2];
                break;

            case  IS_RGBA16_PICTURE:
                pd[ii].in[3 * jj] = ps[i].in[3 * j];
                pd[ii].in[(3 * jj) + 1] = ps[i].in[(3 * j) + 1];
                pd[ii].in[(3 * jj) + 2] = ps[i].in[(3 * j) + 2];
                pd[ii].in[(3 * jj) + 3] = ps[i].in[(3 * j) + 3];
                break;

            case  IS_INT_IMAGE:
                pd[ii].in[jj] = ps[i].in[j];
                break;

            case  IS_UINT_IMAGE:
                pd[ii].ui[jj] = ps[i].ui[j];
                break;

            case  IS_LINT_IMAGE:
                pd[ii].li[jj] = ps[i].li[j];
                break;

            case  IS_FLOAT_IMAGE:
                pd[ii].fl[jj] = ps[i].fl[j];
                break;

            case  IS_COMPLEX_IMAGE:
                pd[ii].fl[jj] = ps[i].fl[j];
                pd[ii].fl[jj + src->im.nx] = ps[i].fl[j + src->im.nx];
                break;

            case  IS_DOUBLE_IMAGE:
                pd[ii].db[jj] = ps[i].db[j];
                break;

            case  IS_COMPLEX_DOUBLE_IMAGE:
                pd[ii].db[jj] = ps[i].db[j];
                pd[ii].db[jj + src->im.nx] = ps[i].db[j + src->im.nx];
                break;

            }
        }
    }

    for (i = 0; i < n_frames; i++)
    {
        for (j = 0; j < src->im.user_nipar; j++)
            dest->im.user_ispare[i][j] = src->im.user_ispare[i][j];

        for (j = 0; j < src->im.user_nfpar; j++)
            dest->im.user_fspare[i][j] = src->im.user_fspare[i][j];
    }

    /*
        dest->im.iparam[i] = src->im.iparam[i];
        dest->im.i1param[i] = src->im.i1param[i];
        dest->im.i2param[i] = src->im.i2param[i];
        dest->im.i3param[i] = src->im.i3param[i];
        dest->im.fparam[i] = src->im.fparam[i];
    */
    dest->im.nxs = src->im.nxs;
    dest->im.nxe = src->im.nxe;
    dest->im.nys = src->im.nys;
    dest->im.nye = src->im.nye;
    dest->im.mode = src->im.mode;

    for (i = 0 ; i < src->n_lab ; i++)
    {
        sp = src->lab[i];
        dp = (p_l *)calloc(1, sizeof(p_l));

        if (dp == NULL)        return (O_i *) xvin_ptr_error(Out_Of_Memory);

        *dp = *sp;

        if (sp->text != NULL)       dp->text = strdup(sp->text);

        dest->lab[i] = dp;
    }

    while (dest->n_xu > 0)
    {
        remove_from_one_image(dest, IS_X_UNIT_SET, (void *)dest->xu[dest->n_xu - 1]);
    };

    if (src->xu != NULL && src->n_xu > 0)
    {
        for (i = 0 ; i < src->n_xu ; i++)
        {
            us = duplicate_unit_set(src->xu[i], NULL);
            add_to_one_image(dest, IS_X_UNIT_SET, (void *)us);
        }
    }

    dest->c_xu = src->c_xu;

    while (dest->n_yu > 0)
    {
        remove_from_one_image(dest, IS_Y_UNIT_SET, (void *)dest->yu[dest->n_yu - 1]);
    };

    if (src->yu != NULL && src->n_yu > 0)
    {
        for (i = 0 ; i < src->n_yu ; i++)
        {
            us = duplicate_unit_set(src->yu[i], NULL);
            add_to_one_image(dest, IS_Y_UNIT_SET, (void *)us);
        }
    }

    dest->c_yu = src->c_yu;

    while (dest->n_zu > 0)
    {
        remove_from_one_image(dest, IS_Z_UNIT_SET, (void *)dest->zu[dest->n_zu - 1]);
    };

    if (src->zu != NULL && src->n_zu > 0)
    {
        for (i = 0 ; i < src->n_zu ; i++)
        {
            us = duplicate_unit_set(src->zu[i], NULL);
            add_to_one_image(dest, IS_Z_UNIT_SET, (void *)us);
        }
    }

    dest->c_zu = src->c_zu;

    while (dest->n_tu > 0)
    {
        remove_from_one_image(dest, IS_T_UNIT_SET, (void *)dest->tu[dest->n_tu - 1]);
    };

    if (src->tu != NULL && src->n_tu > 0)
    {
        for (i = 0 ; i < src->n_tu ; i++)
        {
            us = duplicate_unit_set(src->tu[i], NULL);
            add_to_one_image(dest, IS_T_UNIT_SET, (void *)us);
        }
    }

    dest->c_tu = src->c_tu;

    dest->filename = Transfer_filename(src->filename);
    dest->dir = Mystrdup(src->dir);

    if (image_duplicate_use != NULL)    image_duplicate_use(dest, src);

    for (i = 0 ; i < n_frames && src->im.s_l != NULL; i++)
    {
        dest->im.s_l[i] = (S_l **)calloc(src->im.m_sl[i], sizeof(S_l *));
        dest->im.m_sl[i] = src->im.m_sl[i];
        dest->im.n_sl[i] = src->im.n_sl[i];

        for (j = 0 ; j < src->im.n_sl[i] ; j++)
        {
            dest->im.s_l[i][j] = (S_l *)calloc(1, sizeof(S_l));

            if (src->im.s_l[i][j]->text)
                dest->im.s_l[i][j]->text = strdup(src->im.s_l[i][j]->text);

            dest->im.s_l[i][j]->xla = src->im.s_l[i][j]->xla;
            dest->im.s_l[i][j]->yla = src->im.s_l[i][j]->yla;
            dest->im.s_l[i][j]->type = src->im.s_l[i][j]->type;
            dest->im.s_l[i][j]->draw_shape = src->im.s_l[i][j]->draw_shape;
            dest->im.s_l[i][j]->shape_action = src->im.s_l[i][j]->shape_action;
        }

    }
    for (i = 0 ; i < 8 ; i++)
    {
        dest->im.src_parameter[i] = src->im.src_parameter[i];

        if (src->im.src_parameter_type[i] != NULL && strlen(src->im.src_parameter_type[i]) > 0)
        {
            dest->im.src_parameter_type[i] = strdup(src->im.src_parameter_type[i]);
        }
    }

    dest->need_to_refresh |= ALL_NEED_REFRESH;
    return dest;
}

O_i *duplicate_image(O_i *src, O_i *dest)
{
    return duplicate_image_or_movie(src, dest, 1);
}


int init_one_image(O_i *oi, int type)
{
    un_s    *un = NULL;
    int i;

    if (oi == NULL)     return xvin_error(Wrong_Argument);

    oi->type = type;
    oi->width = oi->height = 1;
    oi->right = oi->up = 0;
    oi->iopt = 0;
    oi->iopt2 = X_NUM | Y_NUM | IM_USR_COOR_RT;
    oi->tick_len = -1;
    oi->x_lo = oi->x_hi = oi->y_lo = oi->y_hi = 0;
    oi->z_black = oi->z_white = 0;
    oi->dx = oi->dy = oi->dz = oi->dt = 1;
    oi->m_lab = MAX_DATA;
    oi->n_lab = oi->im.nx = oi->im.ny = oi->im.nxs = 0;
    oi->im.nys = oi->im.nxe = oi->im.nye = 0;
    time(&(oi->im.time));
    oi->filename = oi->dir = oi->title = NULL;
    oi->x_title = oi->y_title = oi->x_prime_title = oi->y_prime_title = NULL;
    oi->x_prefix = oi->y_prefix = oi->x_unit = oi->y_unit  = NULL;
    oi->z_prefix = oi->t_prefix = oi->z_unit = oi->t_unit  = NULL;

    oi->im.n_special = oi->im.m_special = 0;
    oi->im.special = NULL;
    oi->im.win_flag = X_NOT_PER & Y_NOT_PER;
    oi->im.has_sqaure_pixel = 0;
    oi->im.use.to = 0;
    oi->im.use.stuff = NULL;
    oi->lab = (p_l **)calloc(MAX_DATA, sizeof(p_l *));

    if (oi->lab == NULL)    return xvin_error(Out_Of_Memory);

    oi->n_op = oi->m_op = 0;
    oi->cur_op = -1;
    oi->o_p = NULL;
    oi->im.n_f = oi->im.m_f = oi->im.c_f = 0;
    oi->im.pxl = NULL;
    oi->im.pixel = NULL;
    oi->im.mem = NULL;
    oi->at = oi->ax = oi->ay = oi->az = 0;
    oi->n_xu = oi->m_xu = oi->c_xu = 0;
    oi->n_yu = oi->m_yu = oi->c_yu = 0;
    oi->n_zu = oi->m_zu = oi->c_zu = 0;
    oi->n_tu = oi->m_tu = oi->c_tu = 0;
    oi->xu = NULL;
    oi->yu = NULL;
    oi->zu = NULL;
    oi->tu = NULL;
    oi->rm_background = 0;
    oi->oi_bg = NULL;
    un = build_unit_set(IS_RAW_U, 0, 1, 0, 0, NULL);

    if (un == NULL)     return xvin_error(Out_Of_Memory);

    add_to_one_image(oi, IS_X_UNIT_SET, (void *)un);
    un = build_unit_set(IS_RAW_U, 0, 1, 0, 0, NULL);

    if (un == NULL)     return xvin_error(Out_Of_Memory);

    add_to_one_image(oi, IS_Y_UNIT_SET, (void *)un);
    un = build_unit_set(IS_RAW_U, 0, 1, 0, 0, NULL);

    if (un == NULL)     return xvin_error(Out_Of_Memory);

    add_to_one_image(oi, IS_Z_UNIT_SET, (void *)un);
    un = build_unit_set(IS_RAW_U, 0, 1, 0, 0, NULL);

    if (un == NULL)     return xvin_error(Out_Of_Memory);

    add_to_one_image(oi, IS_T_UNIT_SET, (void *)un);
    oi->bmp.to = 0;
    oi->bmp.stuff = NULL;
    oi->need_to_refresh |= ALL_NEED_REFRESH;
    oi->buisy_in_thread = 0;
    oi->data_changing = 0;
    oi->transfering_data_to_box = 0;
    oi->im.movie_on_disk = 0;
    oi->im.source = NULL;
    oi->im.record_duration = 0;
    oi->oi_idle_action = NULL;
    oi->oi_got_mouse = NULL;
    oi->oi_lost_mouse = NULL;
    oi->oi_end_action = NULL;
    oi->oi_mouse_action = NULL;
    /*
    oi->im.s_l = (S_l ***)calloc(1,sizeof(S_l**));
    oi->im.m_sl = (int*)calloc(1,sizeof(int));
    oi->im.n_sl = (int*)calloc(1,sizeof(int));
    if (oi->im.s_l == NULL) return xvin_error(Out_Of_Memory);
    oi->im.s_l[0] = NULL;
    oi->im.n_sl[0] = oi->im.m_sl[0] = 0;
    */
    oi->im.s_l = NULL;
    oi->im.m_sl = NULL;
    oi->im.n_sl = NULL;
    oi->im.user_ispare = NULL;
    oi->im.user_fspare = NULL;
    oi->im.user_nfpar = oi->im.user_nipar = 0;
    oi->oi_post_display = NULL;
    oi->im.multi_page = 0;

    for (i = 0 ; i < 8 ; i++)
    {
        oi->im.src_parameter_type[i] = NULL;
        oi->im.src_parameter[i] = 0;
    }
    oi->im.save_using_this_frame_as_first = 0;
    return 0;
}

/*  int alloc_image (imreg *imr, int nx, int ny, int type);
 *  DESCRIPTION allocate space for the image
 *
 *  RETURNS     0 on success, 1 on error.
 */
int alloc_one_image(O_i *oi, int nx, int ny, int type)
{
    int i = 0, j;
    int data_len = 1, nf = 0;
    char *buf = NULL;
    size_t memsize = 0;


    if (oi == NULL)     return xvin_error(Wrong_Argument);

    switch (type)
    {
    case IS_CHAR_IMAGE:
        oi->z_min = 0;
        oi->z_max = 255;
        data_len = 1;
        break;

    case IS_RGB_PICTURE:
        oi->z_min = 0;
        oi->z_max = 255;
        oi->z_Rmin = 0;
        oi->z_Rmax = 255;
        oi->z_Gmin = 0;
        oi->z_Gmax = 255;
        oi->z_Bmin = 0;
        oi->z_Bmax = 255;
        oi->im.mode = TRUE_RGB;
        data_len = 3;
        break;

    case IS_RGBA_PICTURE:
        oi->z_min = 0;
        oi->z_max = 255;
        oi->z_Rmin = 0;
        oi->z_Rmax = 255;
        oi->z_Gmin = 0;
        oi->z_Gmax = 255;
        oi->z_Bmin = 0;
        oi->z_Bmax = 255;
        oi->im.mode = TRUE_RGB;
        data_len = 4;
        break;

    case IS_RGB16_PICTURE:
        oi->z_min = -32768;
        oi->z_max = 32767;
        oi->z_Rmin = -32768;
        oi->z_Rmax = 32767;
        oi->z_Gmin = -32768;
        oi->z_Gmax = 32767;
        oi->z_Bmin = -32768;
        oi->z_Bmax = 32767;
        oi->im.mode = TRUE_RGB;
        data_len = 6;
        break;

    case IS_RGBA16_PICTURE:
        oi->z_min = -32768;
        oi->z_max = 32767;
        oi->z_Rmin = -32768;
        oi->z_Rmax = 32767;
        oi->z_Gmin = -32768;
        oi->z_Gmax = 32767;
        oi->z_Bmin = -32768;
        oi->z_Bmax = 32767;
        oi->im.mode = TRUE_RGB;
        data_len = 8;
        break;

    case IS_INT_IMAGE:
        oi->z_min = -32768;
        oi->z_max = 32767;
        data_len = 2;
        break;

    case IS_UINT_IMAGE:
        oi->z_min = 0;
        oi->z_max = 65536;
        data_len = 2;
        break;

    case IS_LINT_IMAGE:
        oi->z_min = -2e30;
        oi->z_max = 2e30;
        data_len = 4;
        break;

    case IS_FLOAT_IMAGE:
        oi->z_min = -1;
        oi->z_max = 1;
        data_len = 4;
        break;

    case IS_COMPLEX_IMAGE:
        oi->z_min = -1;
        oi->z_max = 1;
        data_len = 8;
        break;

    case IS_DOUBLE_IMAGE:
        oi->z_min = -1;
        oi->z_max = 1;
        data_len = 8;
        break;

    case IS_COMPLEX_DOUBLE_IMAGE:
        oi->z_min = -1;
        oi->z_max = 1;
        data_len = 16;
        break;

    default:
        return 1;
        break;
    };

    if (oi->im.pixel != NULL && oi->im.ny != 0 && oi->im.nx != 0)
        buf = (char *)oi->im.mem[0]; //oi->im.pixel[0].ch;

    if (oi->im.movie_on_disk) nf = 1;
    else nf = (oi->im.n_f > 0) ? oi->im.n_f : 1;

    oi->im.pixel = (union pix *)realloc(oi->im.pixel, ny * nf * sizeof(union pix));

    if (oi->im.pixel == NULL)
      { xvin_error(Out_Of_Memory); return 1;}

    if (nf > 1) //oi->im.n_f > 1)
    {
        oi->im.pxl = (union pix **)realloc(oi->im.pxl, nf * sizeof(union pix *));
        oi->im.mem = (void **)realloc(oi->im.mem, nf * sizeof(void *));

        if (oi->im.pxl == NULL  || oi->im.mem == NULL)
	  { xvin_error(Out_Of_Memory); return 1;}
    }
    else
    {
        oi->im.mem = (void **)realloc(oi->im.mem, sizeof(void *));
        oi->im.pxl = (union pix **)realloc(oi->im.pxl, sizeof(union pix *));

        if (oi->im.pxl == NULL  || oi->im.mem == NULL)
	  { xvin_error(Out_Of_Memory); return 1;}

    }

    oi->im.m_f = nf;

    if (oi->im.multi_page == 0)
    {
        memsize = nx * ny;
	memsize *= nf * data_len * sizeof(char);
        buf = (char *) realloc(buf, memsize);

        if (oi->im.pixel == NULL || buf == NULL)
	  { xvin_error(Out_Of_Memory); return 1;}
        oi->im.mem[0] = buf;
    }
    else
    {
        for (i = 0 ; i < nf ; i++)
        {
            memsize = nx * ny;
	    memsize *= data_len;

	      buf = (char *) calloc(memsize,sizeof(char));

            if (buf == NULL)
	      	  { xvin_error(Out_Of_Memory); return 1;}

            oi->im.mem[i] = buf;
        }
    }



    for (i = 0 ; i < ny * nf ; i++)
    {
        if (oi->im.multi_page)
	  {
	    memsize = nx * data_len;
	    memsize *= (i % ny);
            buf = (char *) oi->im.mem[i / ny] + memsize;
	  }

        switch (type)
        {
        case IS_CHAR_IMAGE:
            oi->im.pixel[i].ch = (unsigned char *)buf;

            for (j = 0 ; j < nx ; j++) oi->im.pixel[i].ch[j] = 0;

            break;

        case IS_RGB_PICTURE:
            oi->im.pixel[i].ch = (unsigned char *)buf;

            for (j = 0 ; j < 3 * nx ; j++)   oi->im.pixel[i].ch[j] = 0;

            break;

        case IS_RGBA_PICTURE:
            oi->im.pixel[i].ch = (unsigned char *)buf;

            for (j = 0 ; j < 4 * nx ; j++)   oi->im.pixel[i].ch[j] = 0;

            break;

        case IS_RGB16_PICTURE:
            oi->im.pixel[i].in = (short int *)buf;

            for (j = 0 ; j < 3 * nx ; j++)   oi->im.pixel[i].in[j] = 0;

            break;

        case IS_RGBA16_PICTURE:
            oi->im.pixel[i].in = (short int *)buf;

            for (j = 0 ; j < 4 * nx ; j++)   oi->im.pixel[i].in[j] = 0;

            break;

        case IS_INT_IMAGE:
            oi->im.pixel[i].in = (short int *)buf;

            for (j = 0 ; j < nx ; j++) oi->im.pixel[i].in[j] = 0;

            break;

        case IS_UINT_IMAGE:
            oi->im.pixel[i].ui = (unsigned short int *)buf;

            for (j = 0 ; j < nx ; j++) oi->im.pixel[i].ui[j] = 0;

            break;

        case IS_LINT_IMAGE:
            oi->im.pixel[i].li = (int *)buf;

            for (j = 0 ; j < nx ; j++) oi->im.pixel[i].li[j] = 0;

            break;

        case IS_FLOAT_IMAGE:
            oi->im.pixel[i].fl = (float *)buf;

            for (j = 0 ; j < nx ; j++) oi->im.pixel[i].fl[j] = 0;

            break;

        case IS_COMPLEX_IMAGE:
            oi->im.pixel[i].fl = (float *)buf;

            for (j = 0; j < 2 * nx; j++) oi->im.pixel[i].fl[j] = 0;

            break;

        case IS_DOUBLE_IMAGE:
            oi->im.pixel[i].db = (double *)buf;

            for (j = 0 ; j < nx ; j++) oi->im.pixel[i].db[j] = 0;

            break;

        case IS_COMPLEX_DOUBLE_IMAGE:
            oi->im.pixel[i].db = (double *)buf;

            for (j = 0; j < 2 * nx; j++) oi->im.pixel[i].db[j] = 0;

            break;

        };

        if ((i % ny == 0) && (oi->im.n_f > 0))
        {
            oi->im.pxl[i / ny] = oi->im.pixel + i;

            if (oi->im.multi_page == 0) oi->im.mem[i / ny] = (char *)buf;
        }

        buf += nx * data_len;
    }

    if (oi->im.n_f == 0)        oi->im.pxl[0] = oi->im.pixel;

    oi->im.nxs = oi->im.nys = 0;
    oi->im.nxe = oi->im.nx = nx;
    oi->im.nye = oi->im.ny = ny;
    oi->im.data_type = type;

    if (type == IS_COMPLEX_IMAGE  || type == IS_COMPLEX_DOUBLE_IMAGE)
        oi->im.mode = RE;

    oi->im.s_l = (S_l ** *)calloc(nf, sizeof(S_l **));
    oi->im.m_sl = (int *)calloc(nf, sizeof(int));
    oi->im.n_sl = (int *)calloc(nf, sizeof(int));

    if (oi->im.s_l == NULL || oi->im.n_sl == NULL || oi->im.m_sl == NULL)
      { xvin_error(Out_Of_Memory); return 1;}

    for (i = 0; i < nf; i++)
    {
        oi->im.s_l[i] = NULL;
        oi->im.n_sl[i] = oi->im.m_sl[i] = 0;
    }

    //if (nf != 1) win_printf("nf = %d",nf);
    oi->im.user_ispare = (int **)calloc(nf, sizeof(int *));
    oi->im.user_fspare = (float **)calloc(nf, sizeof(float *));

    if (oi->im.user_ispare == NULL || oi->im.user_fspare == NULL)
      { xvin_error(Out_Of_Memory); return 1;}

    oi->im.user_nfpar = oi->im.user_nipar = 4;

    for (i = 0; i < nf; i++)
    {
        oi->im.user_ispare[i] = (int *)calloc(oi->im.user_nipar, sizeof(int));
        oi->im.user_fspare[i] = (float *)calloc(oi->im.user_nfpar, sizeof(float));

        if (oi->im.user_ispare[i] == NULL || oi->im.user_fspare[i] == NULL)
	  { xvin_error(Out_Of_Memory); return 1;}
    }

    /*
    oi->im.iparam = (int*)calloc(nf,sizeof(int));
    oi->im.i1param = (int*)calloc(nf,sizeof(int));
    oi->im.i2param = (int*)calloc(nf,sizeof(int));
    oi->im.i3param = (int*)calloc(nf,sizeof(int));
    oi->im.fparam = (float*)calloc(nf,sizeof(float));
    if (oi->im.iparam == NULL || oi->im.i1param == NULL || oi->im.i2param == NULL
        || oi->im.i3param == NULL || oi->im.fparam == NULL)
      return xvin_error(Out_Of_Memory);
    */
    oi->need_to_refresh |= ALL_NEED_REFRESH;
    return 0;
}




int set_oi_src_parameter(O_i *oi, int param_nb, float value, char *type)
{
    int i, j;

    if (oi == NULL)
    {
        return -1;
    }

    if (param_nb < 0 || param_nb >= 8)
    {
        if (type == NULL)
        {
            return -2;
        }

        for (i = 0, j = 1; i < 8 && j != 0; i++)
        {
            if (oi->im.src_parameter_type[i] != NULL)
            {
                j = strcmp(type, oi->im.src_parameter_type[i]);
            }

            if (j == 0)
            {
                break;
            }
        }

        if (j == 0)
        {
            oi->im.src_parameter[i] = value;
        }
        else
        {
            for (i = 0; i < 8; i++)
                if (oi->im.src_parameter_type[i] == NULL)
                {
                    break;
                }

            if (i < 8)
            {
                oi->im.src_parameter[i] = value;

                if (strlen(type) > 0)
                {
                    oi->im.src_parameter_type[i] = strdup(type);
                }

                return i;
            }
        }
    }
    else
    {
        oi->im.src_parameter[param_nb] = value;

        if (oi->im.src_parameter_type[param_nb] != NULL)
        {
            free(oi->im.src_parameter_type[param_nb]);
            oi->im.src_parameter_type[param_nb] = NULL;
        }

        if (strlen(type) > 0)
        {
            oi->im.src_parameter_type[param_nb] = strdup(type);
        }

        return param_nb;
    }

    return -3;
}



// add a screen label to current image
struct screen_label *add_screen_label(O_i *oi, float x, float y, int type, char *text,
                                      int (*draw_shape)(S_l *s_l, O_i *oi, void *imr))
{
    struct screen_label *s_l = NULL;

    if (oi == NULL) return NULL;

    if (oi->im.n_sl[oi->im.c_f] >= oi->im.m_sl[oi->im.c_f])
    {
        oi->im.m_sl[oi->im.c_f] += 16;
        oi->im.s_l[oi->im.c_f] = (S_l **)realloc(oi->im.s_l[oi->im.c_f], (oi->im.m_sl[oi->im.c_f] * sizeof(S_l *)));

        if (oi->im.s_l[oi->im.c_f] == NULL) return NULL;
    }

    s_l = (S_l *)calloc(1, sizeof(S_l));

    if (s_l == NULL) return NULL;

    if (text)  s_l->text = strdup(text);

    s_l->xla = x;
    s_l->yla = y;
    s_l->type = type;
    s_l->draw_shape = draw_shape;
    oi->im.s_l[oi->im.c_f][oi->im.n_sl[oi->im.c_f]] = s_l;
    oi->im.n_sl[oi->im.c_f]++;
    return s_l;
}

int remove_all_screen_label_from_one_frame(O_i *oi, int im)
{
    int j;

    if (oi == NULL) return 1;

    if (im < 0 || im >= oi->im.n_f) return 1;

    if (oi->im.s_l != NULL && oi->im.m_sl != NULL && oi->im.n_sl != NULL)
    {
        for (j = 0 ; j < oi->im.n_sl[im] ; j++)
        {
            if (oi->im.s_l[im][j]->text != NULL)  free(oi->im.s_l[im][j]->text);

            free(oi->im.s_l[im][j]);
        }

        oi->im.n_sl[im] = 0;
    }

    return 0;
}


int remove_one_screen_label(O_i *oi, struct screen_label *s_l)
{
    int i, j;
    int done = 0, ii = 0, jj = 0;

    if (oi == NULL || s_l == NULL) return 1;

    if (oi->im.s_l != NULL && oi->im.m_sl != NULL && oi->im.n_sl != NULL)
    {
        for (i = 0, done = 0; i < oi->im.n_f && done == 0; i++)
        {
            for (j = 0 ; j < oi->im.n_sl[i] ; j++)
            {
                if (oi->im.s_l[i][j] == s_l)
                {
                    done = 1;
                    ii = i;
                    jj = j;
                    break;
                }
            }
        }

        if (done)
        {
            if (oi->im.s_l[ii][jj]->text != NULL)  free(oi->im.s_l[ii][jj]->text);

            free(oi->im.s_l[ii][jj]);

            for (; j < oi->im.n_sl[ii] - 1; j++)
            {
                oi->im.s_l[ii][jj] = oi->im.s_l[ii][jj + 1];
            }

            oi->im.n_sl[ii] = (oi->im.n_sl[ii] > 0) ? oi->im.n_sl[ii] - 1 : 0;
        }
        else return 2;
    }

    return 0;
}


int remove_all_screen_label_from_movie(O_i *oi)
{
    int i, j;

    if (oi == NULL) return 1;

    if (oi->im.s_l != NULL && oi->im.m_sl != NULL && oi->im.n_sl != NULL)
    {
        for (i = 0 ; i < oi->im.n_f; i++)
        {
            for (j = 0 ; j < oi->im.n_sl[i] ; j++)
            {
                if (oi->im.s_l[i][j]->text != NULL)  free(oi->im.s_l[i][j]->text);

                free(oi->im.s_l[i][j]);
            }

            oi->im.n_sl[i] = 0;
        }
    }

    return 0;
}

/*  int add_image (imreg *imr, int type, void *stuff);
 *  int remove_image (imreg *imr, int type, void *stuff);
 *  int add_one_image (O_i *oi, int type, void *stuff);
 *  int remove_one_image (O_i *oi, int type, void *stuff);
 *  DESCRIPTION
 *
 *
 *  RETURNS     0 on success, 1
 *
 */


int add_one_image(O_i *oi, int type, void *stuff)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    return add_to_one_image(oi, type, stuff);
}
int add_to_one_image(O_i *oi, int type, void *stuff)
{
    int i = 0;

    if (stuff == NULL || oi == NULL)   return xvin_error(Wrong_Argument);

    if (type == IS_PLOT_LABEL)
    {
        if (oi->n_lab == oi->m_lab)
        {
            oi->m_lab += MAX_DATA;
            oi->lab = (p_l **)realloc(oi->lab, oi->m_lab * sizeof(p_l *));

            if (oi->lab == NULL)        return xvin_error(Out_Of_Memory);
        }

        if (((p_l *)stuff)->text != NULL)   /* no empty label */
        {
            oi->lab[oi->n_lab] = (p_l *)stuff;
            oi->n_lab++;
        }

        oi->need_to_refresh |= PLOT_NEED_REFRESH;
    }
    else    if (type == IS_ONE_PLOT)
    {
        if (oi->n_op >=  oi->m_op)
        {
            oi->m_op += MAX_DATA;
            oi->o_p = (O_p **)realloc(oi->o_p, oi->m_op * sizeof(O_p *));

            if (oi->o_p == NULL)        return xvin_error(Out_Of_Memory);
        }

        oi->o_p[oi->n_op] = (O_p *)stuff;
        oi->cur_op = oi->n_op;
        oi->n_op++;
        oi->need_to_refresh |= EXTRA_PLOT_NEED_REFRESH | PLOT_NEED_REFRESH;         ;
    }
    else    if (type == IS_SPECIAL)
    {
        if (oi->im.n_special >=  oi->im.m_special)
        {
            oi->im.m_special++;
            oi->im.special = (char **)realloc(oi->im.special, oi->im.m_special * sizeof(char *));

            if (oi->im.special == NULL) return xvin_error(Out_Of_Memory);
        }

        oi->im.special[oi->im.n_special] = Mystrdup((char *)stuff);
        oi->im.n_special++;
        oi->need_to_refresh |= PLOT_NEED_REFRESH;
    }
    else if (type == IS_X_UNIT_SET)
    {
        if (oi->n_xu >= oi->m_xu)
        {
            oi->m_xu += MAX_DATA;
            oi->xu = (un_s **)realloc(oi->xu, oi->m_xu * sizeof(un_s *));

            if (oi->xu == NULL)     return xvin_error(Out_Of_Memory);
        }

        oi->xu[oi->n_xu] = (un_s *)stuff;
        ((un_s *)stuff)->axis = IS_X_UNIT_SET;
        oi->n_xu++;
        oi->need_to_refresh |= PLOT_NEED_REFRESH;
    }
    else if (type == IS_Y_UNIT_SET)
    {
        if (oi->n_yu >= oi->m_yu)
        {
            oi->m_yu += MAX_DATA;
            oi->yu = (un_s **)realloc(oi->yu, oi->m_yu * sizeof(un_s *));

            if (oi->yu == NULL)     return xvin_error(Out_Of_Memory);
        }

        oi->yu[oi->n_yu] = (un_s *)stuff;
        ((un_s *)stuff)->axis = IS_Y_UNIT_SET;
        oi->n_yu++;
        oi->need_to_refresh |= PLOT_NEED_REFRESH;
    }
    else if (type == IS_Z_UNIT_SET)
    {
        if (oi->n_zu >= oi->m_zu)
        {
            oi->m_zu += MAX_DATA;
            oi->zu = (un_s **)realloc(oi->zu, oi->m_zu * sizeof(un_s *));

            if (oi->zu == NULL)     return xvin_error(Out_Of_Memory);
        }

        oi->zu[oi->n_zu] = (un_s *)stuff;
        ((un_s *)stuff)->axis = IS_Z_UNIT_SET;
        oi->n_zu++;
        oi->need_to_refresh |= PLOT_NEED_REFRESH;
    }
    else if (type == IS_T_UNIT_SET)
    {
        if (oi->n_tu >= oi->m_tu)
        {
            oi->m_tu += MAX_DATA;
            oi->tu = (un_s **) realloc(oi->tu, oi->m_tu * sizeof(un_s *));

            if (oi->tu == NULL)     return xvin_error(Out_Of_Memory);
        }

        oi->tu[oi->n_tu] = (un_s *)stuff;
        ((un_s *)stuff)->axis = IS_T_UNIT_SET;
        oi->n_tu++;
        oi->need_to_refresh |= PLOT_NEED_REFRESH;
    }
    else    i = 1;

    return i;
}

int remove_one_image(O_i *oi, int type, void *stuff)
{
    return remove_from_one_image(oi, type, stuff);
}
int remove_from_one_image(O_i *oi, int type, void *stuff)
{
    int i = 0, j;

    if (stuff == NULL || oi == NULL)
        return xvin_error(Wrong_Argument);

    if (type == IS_UNIT_SET)    type = ((un_s *)stuff)->axis;

    if (type == IS_PLOT_LABEL)
    {
        for (i = 0, j = -1 ; (i < oi->n_lab) && (j < 0) ; i++)
        {
            if ((void *)(oi->lab[i]) == stuff)
                j = i;
        }

        if (j >= 0)
        {
            if (oi->lab[j]->text != NULL)   free(oi->lab[j]->text);

            if (oi->lab[j] != NULL)     free(oi->lab[j]);

            oi->n_lab--;

            for (i = j ; i < oi->n_lab ; i++)
                oi->lab[i] = oi->lab[i + 1];

            i = 0;
        }

        oi->need_to_refresh |= PLOT_NEED_REFRESH;
    }
    else if (type == IS_SPECIAL)
    {
        for (i = 0, j = -1 ; (i < oi->im.n_special) && (j < 0) ; i++)
        {
            if ((void *)(oi->im.special[i]) == stuff)
                j = i;
        }

        if (j >= 0)
        {
            if (oi->im.special[j] != NULL) free(oi->im.special[j]);

            oi->im.n_special--;

            for (i = j ; i < oi->im.n_special ; i++)
                oi->im.special[i] = oi->im.special[i + 1];

            i = 0;
        }

        oi->need_to_refresh |= PLOT_NEED_REFRESH;
        /* oi->im.special n'est pas libere avec le dernier special */
        /* oi->im.m_special n'est jamais decremente */
    }
    else if (type == ALL_SPECIAL)
    {
        for (i = 0; (oi->im.special != NULL) && (i < oi->im.n_special) ; i++)
            if (oi->im.special[i] != NULL) free(oi->im.special[i]);

        oi->im.n_special = oi->im.m_special = 0;

        if (oi->im.special != NULL) free(oi->im.special);

        oi->im.special = NULL;
    }
    else if (type == IS_ONE_PLOT)
    {
        for (i = 0, j = -1 ; (i < oi->n_op) && (j < 0) ; i++)
        {
            if ((void *)(oi->o_p[i]) == stuff)
                j = i;
        }

        if (j >= 0)
        {
            if (oi->o_p[j] != NULL) free_one_plot(oi->o_p[j]);

            oi->n_op--;

            for (i = j ; i < oi->n_op ; i++)
                oi->o_p[i] = oi->o_p[i + 1];

            if (oi->cur_op >= j && oi->cur_op >= 0) oi->cur_op--;

            i = 0;
        }

        oi->need_to_refresh |= PLOT_NEED_REFRESH;
    }
    else if (type == IS_X_UNIT_SET)
    {
        for (i = 0, j = -1 ; (i < oi->n_xu) && (j < 0) ; i++)
        {
            if ((void *)(oi->xu[i]) == stuff)
                j = i;
        }

        if (j >= 0)
        {
            if (oi->xu[j] != NULL)  free_unit_set(oi->xu[j]);

            oi->n_xu--;

            for (i = j; i < oi->n_xu; i++) oi->xu[i] = oi->xu[i + 1];

            if (oi->c_xu >= j && oi->c_xu > 0)  oi->c_xu--;

            i = 0;
        }

        oi->need_to_refresh |= PLOT_NEED_REFRESH;
    }
    else if (type == IS_Y_UNIT_SET)
    {
        for (i = 0, j = -1 ; (i < oi->n_yu) && (j < 0) ; i++)
        {
            if ((void *)(oi->yu[i]) == stuff)
                j = i;
        }

        if (j >= 0)
        {
            if (oi->yu[j] != NULL)  free_unit_set(oi->yu[j]);

            oi->n_yu--;

            for (i = j; i < oi->n_yu; i++) oi->yu[i] = oi->yu[i + 1];

            if (oi->c_yu >= j && oi->c_yu > 0)  oi->c_yu--;

            i = 0;
        }

        oi->need_to_refresh |= PLOT_NEED_REFRESH;
    }
    else if (type == IS_Z_UNIT_SET)
    {
        for (i = 0, j = -1 ; (i < oi->n_zu) && (j < 0) ; i++)
        {
            if ((void *)(oi->zu[i]) == stuff)
                j = i;
        }

        if (j >= 0)
        {
            if (oi->zu[j] != NULL)  free_unit_set(oi->zu[j]);

            oi->n_zu--;

            for (i = j ; i < oi->n_zu; i++) oi->zu[i] = oi->zu[i + 1];

            if (oi->c_zu >= j && oi->c_zu > 0)  oi->c_zu--;

            i = 0;
        }

        oi->need_to_refresh |= PLOT_NEED_REFRESH;
    }
    else if (type == IS_T_UNIT_SET)
    {
        for (i = 0, j = -1 ; (i < oi->n_tu) && (j < 0) ; i++)
        {
            if ((void *)(oi->tu[i]) == stuff)
                j = i;
        }

        if (j >= 0)
        {
            if (oi->tu[j] != NULL)  free_unit_set(oi->tu[j]);

            oi->n_tu--;

            for (i = j ; i < oi->n_tu; i++) oi->tu[i] = oi->tu[i + 1];

            if (oi->c_tu >= j && oi->c_tu > 0)  oi->c_tu--;

            i = 0;
        }

        oi->need_to_refresh |= PLOT_NEED_REFRESH;
    }
    else    i = 1;

    return i;
}
int alloc_over_plane(O_i *oi)
{
    int i, nx;
    unsigned char *buf = NULL;

    if (oi == NULL || oi->im.over != NULL)      return 3;

    nx = oi->im.nx / 8;
    nx = (oi->im.nx % 8) ? nx + 1 : nx;
    oi->im.over = (unsigned char **)calloc(oi->im.ny, sizeof(unsigned char *));
    buf = (unsigned char *)calloc((oi->im.ny * nx), sizeof(unsigned char));

    if (oi->im.over == NULL || buf == NULL)
        return xvin_error(Out_Of_Memory);

    for (i = 0 ; i < oi->im.ny ; i++)
        oi->im.over[i] = buf + i * nx;

    oi->need_to_refresh |= ALL_NEED_REFRESH;
    return 0;
}
int set_over(O_i *oi, int x, int y)
{
    int j;

    if (oi == NULL) return 1;

    if (oi->im.over == NULL)    alloc_over_plane(oi);

    if (oi->im.over == NULL)        return 1;

    if (x >= oi->im.nx || y >= oi->im.ny || x < 0 || y < 0) return 1;

    j = x >> 3;
    oi->im.over[y][j] |= 1 << (x & 0x07);
    oi->need_to_refresh |= ALL_NEED_REFRESH;
    return 0;
}
int reset_full_over_plane(O_i *oi, int val)
{
    int i, j;
    int k;

    if (oi == NULL) return 1;

    if (oi->im.over == NULL)        return 1;

    k = oi->im.nx / 8;
    k = (oi->im.nx % 8) ? k + 1 : k;
    val = (val) ? 1 : 0;

    for (i = 0 ; i < oi->im.ny ; i++)
        for (j = 0; j < k; j++)     oi->im.over[i][j] = val;

    oi->need_to_refresh |= ALL_NEED_REFRESH;
    return 0;
}
int extract_line(O_i *oi, int n_line, float *z)
{
    int i;

    if (oi == NULL || z == NULL)        return xvin_error(Wrong_Argument);

    i = extract_raw_line(oi, n_line, z);

    if (i)              return i;

    for (i = 0; i < oi->im.nx; i++)    z[i] = oi->az + oi->dz * z[i];

    return 0;
}
int extract_raw_line(O_i *oi, int n_line, float *z)
{
    int i, nl, nx;
    float zr, zi;
    union pix *pi;

    if (oi == NULL || z == NULL)        return xvin_error(Wrong_Argument);

    if (n_line >= oi->im.ny || n_line < 0)  return 1;

    pi = oi->im.pixel;
    nl = n_line;
    nx = oi->im.nx;

    if (oi->im.data_type == IS_CHAR_IMAGE)
        for (i = 0; i < nx; i++) z[i] = (float)pi[nl].ch[i];
    else if (oi->im.data_type == IS_RGB_PICTURE)
    {
        for (i = 0; i < nx; i++)
        {
            //z[i] = ((float)pi[nl].ch[3*i]+(float)pi[nl].ch[(3*i)+1]+(float)pi[nl].ch[(3*i)+2])/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[nl].rgb[i].r + pi[nl].rgb[i].g
                                + pi[nl].rgb[i].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[nl].rgb[i].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[nl].rgb[i].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[nl].rgb[i].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)255;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_RGBA_PICTURE)
    {
        for (i = 0; i < nx; i++)
        {
            //z[i] = ((float)pi[nl].rgba[i].b+(float)pi[nl].rgba[i].g+(float)pi[nl].rgba[i].r)/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[nl].rgba[i].r + pi[nl].rgba[i].g
                                + pi[nl].rgba[i].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[nl].rgba[i].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[nl].rgba[i].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[nl].rgba[i].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)pi[nl].rgba[i].a;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_RGB16_PICTURE)
    {
        for (i = 0; i < nx; i++)
        {
            //z[i] = ((float)pi[nl].ch[3*i]+(float)pi[nl].ch[(3*i)+1]+(float)pi[nl].ch[(3*i)+2])/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[nl].rgb16[i].r + pi[nl].rgb16[i].g
                                + pi[nl].rgb16[i].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[nl].rgb16[i].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[nl].rgb16[i].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[nl].rgb16[i].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)32767;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_RGBA16_PICTURE)
    {
        for (i = 0; i < nx; i++)
        {
            //z[i] = ((float)pi[nl].rgba[i].b+(float)pi[nl].rgba[i].g+(float)pi[nl].rgba[i].r)/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[nl].rgba16[i].r + pi[nl].rgba16[i].g
                                + pi[nl].rgba16[i].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[nl].rgba16[i].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[nl].rgba16[i].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[nl].rgba16[i].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)pi[nl].rgba16[i].a;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
        for (i = 0; i < nx; i++) z[i] = (float)pi[nl].in[i];
    else if (oi->im.data_type == IS_UINT_IMAGE)
        for (i = 0; i < nx; i++) z[i] = (float)pi[nl].ui[i];
    else if (oi->im.data_type == IS_LINT_IMAGE)
        for (i = 0; i < nx; i++) z[i] = (float)pi[nl].li[i];
    else if (oi->im.data_type == IS_FLOAT_IMAGE)
        for (i = 0; i < nx; i++) z[i] = pi[nl].fl[i];
    else if (oi->im.data_type == IS_DOUBLE_IMAGE)
        for (i = 0; i < nx; i++) z[i] = pi[nl].db[i];
    else if (oi->im.data_type == IS_COMPLEX_IMAGE)
    {
        for (i = 0; i < nx; i++)
        {
            switch (oi->im.mode)
            {
            case AMP:
                zr = pi[nl].fl[2 * i];
                zi = pi[nl].fl[2 * i + 1];
                z[i] = sqrt(zr * zr + zi * zi);
                break;

            case LOG_AMP:
                zr = pi[nl].fl[2 * i];
                zi = pi[nl].fl[2 * i + 1];
                z[i] = zr * zr + zi * zi;
                z[i] = (z[i] > 0) ? log10(z[i]) : -40;
                break;

            case AMP_2:
                zr = pi[nl].fl[2 * i];
                zi = pi[nl].fl[2 * i + 1];
                z[i] = zr * zr + zi * zi;
                break;

            case RE:
                z[i] = pi[nl].fl[2 * i];
                break;

            case IM:
                z[i] = pi[nl].fl[2 * i + 1];
                break;
            };
        }
    }
    else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        for (i = 0; i < nx; i++)
        {
            switch (oi->im.mode)
            {
            case AMP:
                zr = pi[nl].db[2 * i];
                zi = pi[nl].db[2 * i + 1];
                z[i] = sqrt(zr * zr + zi * zi);
                break;

            case LOG_AMP:
                zr = pi[nl].db[2 * i];
                zi = pi[nl].db[2 * i + 1];
                z[i] = zr * zr + zi * zi;
                z[i] = (z[i] > 0) ? log10(z[i]) : -40;
                break;

            case AMP_2:
                zr = pi[nl].db[2 * i];
                zi = pi[nl].db[2 * i + 1];
                z[i] = zr * zr + zi * zi;
                break;

            case RE:
                z[i] = pi[nl].db[2 * i];
                break;

            case IM:
                z[i] = pi[nl].db[2 * i + 1];
                break;
            };
        }
    }
    else    return 1;

    return 0;
}
int extract_raw_partial_line(O_i *oi, int n_line, float *z, int start, int size)
{
    register int i, nl, ii;
    int nx;
    float zr, zi;
    union pix *pi = NULL;

    if (oi == NULL || z == NULL)      return xvin_error(Wrong_Argument);

    if (n_line >= oi->im.ny || n_line < 0)    return 1;

    pi = oi->im.pixel;
    nl = n_line;
    nx = oi->im.nx;

    if (oi->im.data_type == IS_CHAR_IMAGE)
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < nx) ? ii : nx;
            z[i] = (float)pi[nl].ch[ii];
        }
    else if (oi->im.data_type == IS_RGB_PICTURE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < nx) ? ii : nx;

            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[nl].rgb[ii].r + pi[nl].rgb[ii].g
                                + pi[nl].rgb[ii].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[nl].rgb[ii].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[nl].rgb[ii].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[nl].rgb[ii].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)255;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_RGBA_PICTURE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < nx) ? ii : nx;

            //z[i] = ((float)pi[nl].rgba[i].b+(float)pi[nl].rgba[i].g+(float)pi[nl].rgba[i].r)/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[nl].rgba[ii].r + pi[nl].rgba[ii].g
                                + pi[nl].rgba[ii].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[nl].rgba[ii].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[nl].rgba[ii].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[nl].rgba[ii].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)pi[nl].rgba[ii].a;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_RGB16_PICTURE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < nx) ? ii : nx;

            //z[i] = ((float)pi[nl].ch[3*i]+(float)pi[nl].ch[(3*i)+1]+(float)pi[nl].ch[(3*i)+2])/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[nl].rgb16[ii].r + pi[nl].rgb16[ii].g
                                + pi[nl].rgb16[ii].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[nl].rgb16[ii].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[nl].rgb16[ii].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[nl].rgb16[ii].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)32767;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_RGBA16_PICTURE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < nx) ? ii : nx;

            //z[i] = ((float)pi[nl].rgba[i].b+(float)pi[nl].rgba[i].g+(float)pi[nl].rgba[i].r)/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[nl].rgba16[ii].r + pi[nl].rgba16[ii].g
                                + pi[nl].rgba16[ii].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[nl].rgba16[ii].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[nl].rgba16[ii].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[nl].rgba16[ii].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)pi[nl].rgba16[ii].a;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < nx) ? ii : nx;
            z[i] = (float)pi[nl].in[ii];
        }
    else if (oi->im.data_type == IS_UINT_IMAGE)
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < nx) ? ii : nx;
            z[i] = (float)pi[nl].ui[ii];
        }
    else if (oi->im.data_type == IS_LINT_IMAGE)
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < nx) ? ii : nx;
            z[i] = (float)pi[nl].li[ii];
        }
    else if (oi->im.data_type == IS_FLOAT_IMAGE)
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < nx) ? ii : nx;
            z[i] = pi[nl].fl[ii];
        }
    else if (oi->im.data_type == IS_DOUBLE_IMAGE)
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < nx) ? ii : nx;
            z[i] = pi[nl].db[ii];
        }
    else if (oi->im.data_type == IS_COMPLEX_IMAGE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < nx) ? ii : nx;

            switch (oi->im.mode)
            {
            case AMP:
                zr = pi[nl].fl[2 * ii];
                zi = pi[nl].fl[2 * ii + 1];
                z[i] = sqrt(zr * zr + zi * zi);
                break;

            case LOG_AMP:
                zr = pi[nl].fl[2 * ii];
                zi = pi[nl].fl[2 * ii + 1];
                z[i] = zr * zr + zi * zi;
                z[i] = (z[i] > 0) ? log10(z[i]) : -40;
                break;

            case AMP_2:
                zr = pi[nl].fl[2 * ii];
                zi = pi[nl].fl[2 * ii + 1];
                z[i] = zr * zr + zi * zi;
                break;

            case RE:
                z[i] = pi[nl].fl[2 * ii];
                break;

            case IM:
                z[i] = pi[nl].fl[2 * ii + 1];
                break;
            };
        }
    }
    else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < nx) ? ii : nx;

            switch (oi->im.mode)
            {
            case AMP:
                zr = pi[nl].db[2 * ii];
                zi = pi[nl].db[2 * ii + 1];
                z[i] = sqrt(zr * zr + zi * zi);
                break;

            case LOG_AMP:
                zr = pi[nl].db[2 * ii];
                zi = pi[nl].db[2 * ii + 1];
                z[i] = zr * zr + zi * zi;
                z[i] = (z[i] > 0) ? log10(z[i]) : -40;
                break;

            case AMP_2:
                zr = pi[nl].db[2 * ii];
                zi = pi[nl].db[2 * ii + 1];
                z[i] = zr * zr + zi * zi;
                break;

            case RE:
                z[i] = pi[nl].db[2 * ii];
                break;

            case IM:
                z[i] = pi[nl].db[2 * ii + 1];
                break;
            };
        }
    }
    else  return 1;

    return 0;
}



int extract_row(O_i *oi, int n_row, float *z)
{
    int i;

    if (oi == NULL || z == NULL)        return xvin_error(Wrong_Argument);

    i = extract_raw_row(oi, n_row, z);

    if (i)              return i;

    for (i = 0; i < oi->im.ny; i++)    z[i] = oi->az + oi->dz * z[i];

    return 0;
}
int extract_raw_row(O_i *oi, int n_row, float *z)
{
    int i, nr, ny;
    float zr, zi;
    union pix *pi = NULL;

    if (oi == NULL || z == NULL)        return xvin_error(Wrong_Argument);

    if (n_row >= oi->im.nx || n_row < 0)    return 1;

    ny = oi->im.ny;
    nr = n_row;
    pi = oi->im.pixel;

    if (oi->im.data_type == IS_CHAR_IMAGE)
        for (i = 0; i < ny; i++) z[i] = (float)pi[i].ch[nr];
    else if (oi->im.data_type == IS_RGB_PICTURE)
    {
        for (i = 0; i < ny; i++)
        {
            //z[i] = ((float)pi[i].ch[3*nr]+(float)pi[i].ch[(3*nr)+1]+(float)pi[i].ch[(3*nr)+2])/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[i].rgb[nr].r + pi[i].rgb[nr].g
                                + pi[i].rgb[nr].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[i].rgb[nr].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[i].rgb[nr].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[i].rgb[nr].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)255;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_RGBA_PICTURE)
    {
        for (i = 0; i < ny; i++)
        {
            //z[i] = ((float)(pi[i].rgba[nr].b)+(float)(pi[i].rgba[nr].g)+(float)(pi[i].rgba[nr].r))/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[i].rgba[nr].r + pi[i].rgba[nr].g
                                + pi[i].rgba[nr].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[i].rgba[nr].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[i].rgba[nr].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[i].rgba[nr].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)pi[i].rgba[nr].a;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_RGB16_PICTURE)
    {
        for (i = 0; i < ny; i++)
        {
            //z[i] = ((float)pi[i].ch[3*nr]+(float)pi[i].ch[(3*nr)+1]+(float)pi[i].ch[(3*nr)+2])/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[i].rgb16[nr].r + pi[i].rgb16[nr].g
                                + pi[i].rgb16[nr].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[i].rgb16[nr].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[i].rgb16[nr].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[i].rgb16[nr].b;
            else if (oi->im.mode & ALPHA_LEVEL)
                z[i] = (float)32767;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_RGBA16_PICTURE)
    {
        for (i = 0; i < ny; i++)
        {
            //z[i] = ((float)(pi[i].rgba[nr].b)+(float)(pi[i].rgba[nr].g)+(float)(pi[i].rgba[nr].r))/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[i].rgba16[nr].r + pi[i].rgba16[nr].g
                                + pi[i].rgba16[nr].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[i].rgba16[nr].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[i].rgba16[nr].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[i].rgba16[nr].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)pi[i].rgba16[nr].a;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
        for (i = 0; i < ny; i++) z[i] = (float)pi[i].in[nr];
    else if (oi->im.data_type == IS_UINT_IMAGE)
        for (i = 0; i < ny; i++) z[i] = (float)pi[i].ui[nr];
    else if (oi->im.data_type == IS_LINT_IMAGE)
        for (i = 0; i < ny; i++) z[i] = (float)pi[i].li[nr];
    else if (oi->im.data_type == IS_FLOAT_IMAGE)
        for (i = 0; i < ny; i++) z[i] = pi[i].fl[nr];
    else if (oi->im.data_type == IS_DOUBLE_IMAGE)
        for (i = 0; i < ny; i++) z[i] = pi[i].db[nr];
    else if (oi->im.data_type == IS_COMPLEX_IMAGE)
    {
        for (i = 0; i < ny; i++)
        {
            switch (oi->im.mode)
            {
            case AMP:
                zr = pi[i].fl[2 * nr];
                zi = pi[i].fl[2 * nr + 1];
                z[i] = sqrt(zr * zr + zi * zi);
                break;

            case LOG_AMP:
                zr = pi[i].fl[2 * nr];
                zi = pi[i].fl[2 * nr + 1];
                z[i] = zr * zr + zi * zi;
                z[i] = (z[i] > 0) ? log10(z[i]) : -40;
                break;

            case AMP_2:
                zr = pi[i].fl[2 * nr];
                zi = pi[i].fl[2 * nr + 1];
                z[i] = zr * zr + zi * zi;
                break;

            case RE:
                z[i] = pi[i].fl[2 * nr];
                break;

            case IM:
                z[i] = pi[i].fl[2 * nr + 1];
                break;
            };
        }
    }
    else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        for (i = 0; i < ny; i++)
        {
            switch (oi->im.mode)
            {
            case AMP:
                zr = pi[i].db[2 * nr];
                zi = pi[i].db[2 * nr + 1];
                z[i] = sqrt(zr * zr + zi * zi);
                break;

            case LOG_AMP:
                zr = pi[i].db[2 * nr];
                zi = pi[i].db[2 * nr + 1];
                z[i] = zr * zr + zi * zi;
                z[i] = (z[i] > 0) ? log10(z[i]) : -40;
                break;

            case AMP_2:
                zr = pi[i].db[2 * nr];
                zi = pi[i].db[2 * nr + 1];
                z[i] = zr * zr + zi * zi;
                break;

            case RE:
                z[i] = pi[i].db[2 * nr];
                break;

            case IM:
                z[i] = pi[i].db[2 * nr + 1];
                break;
            };
        }
    }
    else    return 1;

    return 0;
}
int extract_raw_partial_row(O_i *oi, int n_row, float *z, int start, int size)
{
    register int i, nr, ny;
    int ii;
    float zr, zi;
    union pix *pi = NULL;

    if (oi == NULL || z == NULL)        return xvin_error(Wrong_Argument);

    if (n_row >= oi->im.nx || n_row < 0)    return 1;

    ny = oi->im.ny;
    nr = n_row;
    pi = oi->im.pixel;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < ny) ? ii : ny;
            z[i] = (float)pi[ii].ch[nr];
        }
    }
    else if (oi->im.data_type == IS_RGB_PICTURE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < ny) ? ii : ny;

            //z[i] = ((float)pi[i].ch[3*nr]+(float)pi[i].ch[(3*nr)+1]+(float)pi[i].ch[(3*nr)+2])/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[ii].rgb[nr].r + pi[ii].rgb[nr].g
                                + pi[ii].rgb[nr].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[ii].rgb[nr].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[ii].rgb[nr].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[ii].rgb[nr].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)255;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_RGBA_PICTURE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < ny) ? ii : ny;

            //z[i] = ((float)(pi[i].rgba[nr].b)+(float)(pi[i].rgba[nr].g)+(float)(pi[i].rgba[nr].r))/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[ii].rgba[nr].r + pi[ii].rgba[nr].g
                                + pi[ii].rgba[nr].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[ii].rgba[nr].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[ii].rgba[nr].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[ii].rgba[nr].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)pi[ii].rgba[nr].a;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_RGB16_PICTURE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < ny) ? ii : ny;

            //z[i] = ((float)pi[i].ch[3*nr]+(float)pi[i].ch[(3*nr)+1]+(float)pi[i].ch[(3*nr)+2])/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[ii].rgb16[nr].r + pi[ii].rgb16[nr].g
                                + pi[ii].rgb16[nr].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[ii].rgb16[nr].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[ii].rgb16[nr].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[ii].rgb16[nr].b;
            else if (oi->im.mode & ALPHA_LEVEL)
                z[i] = (float)32767;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_RGBA16_PICTURE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < ny) ? ii : ny;

            //z[i] = ((float)(pi[i].rgba[nr].b)+(float)(pi[i].rgba[nr].g)+(float)(pi[i].rgba[nr].r))/3;
            if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                z[i] = ((float)(pi[ii].rgba16[nr].r + pi[ii].rgba16[nr].g
                                + pi[ii].rgba16[nr].b)) / 3;
            else if (oi->im.mode & R_LEVEL)
                z[i] = (float)pi[ii].rgba16[nr].r;
            else if (oi->im.mode & G_LEVEL)
                z[i] = (float)pi[ii].rgba16[nr].g;
            else if (oi->im.mode & B_LEVEL)
                z[i] = (float)pi[ii].rgba16[nr].b;
            else if (oi->im.mode == ALPHA_LEVEL)
                z[i] = (float)pi[ii].rgba16[nr].a;
            else return 1;
        }
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < ny) ? ii : ny;
            z[i] = (float)pi[ii].in[nr];
        }
    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < ny) ? ii : ny;
            z[i] = (float)pi[ii].ui[nr];
        }
    }
    else if (oi->im.data_type == IS_LINT_IMAGE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < ny) ? ii : ny;
            z[i] = (float)pi[ii].li[nr];
        }
    }
    else if (oi->im.data_type == IS_FLOAT_IMAGE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < ny) ? ii : ny;
            z[i] = pi[ii].fl[nr];
        }
    }
    else if (oi->im.data_type == IS_DOUBLE_IMAGE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < ny) ? ii : ny;
            z[i] = pi[ii].db[nr];
        }
    }
    else if (oi->im.data_type == IS_COMPLEX_IMAGE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < ny) ? ii : ny;

            switch (oi->im.mode)
            {
            case AMP:
                zr = pi[ii].fl[2 * nr];
                zi = pi[ii].fl[2 * nr + 1];
                z[i] = sqrt(zr * zr + zi * zi);
                break;

            case LOG_AMP:
                zr = pi[ii].fl[2 * nr];
                zi = pi[ii].fl[2 * nr + 1];
                z[i] = zr * zr + zi * zi;
                z[i] = (z[i] > 0) ? log10(z[i]) : -40;
                break;

            case AMP_2:
                zr = pi[ii].fl[2 * nr];
                zi = pi[ii].fl[2 * nr + 1];
                z[i] = zr * zr + zi * zi;
                break;

            case RE:
                z[i] = pi[ii].fl[2 * nr];
                break;

            case IM:
                z[i] = pi[ii].fl[2 * nr + 1];
                break;
            };
        }
    }
    else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        for (i = 0; i < size; i++)
        {
            ii = start + i;
            ii = (ii < 0) ? 0 : ii;
            ii = (ii < ny) ? ii : ny;

            switch (oi->im.mode)
            {
            case AMP:
                zr = pi[ii].db[2 * nr];
                zi = pi[ii].db[2 * nr + 1];
                z[i] = sqrt(zr * zr + zi * zi);
                break;

            case LOG_AMP:
                zr = pi[ii].db[2 * nr];
                zi = pi[ii].db[2 * nr + 1];
                z[i] = zr * zr + zi * zi;
                z[i] = (z[i] > 0) ? log10(z[i]) : -40;
                break;

            case AMP_2:
                zr = pi[ii].db[2 * nr];
                zi = pi[ii].db[2 * nr + 1];
                z[i] = zr * zr + zi * zi;
                break;

            case RE:
                z[i] = pi[ii].db[2 * nr];
                break;

            case IM:
                z[i] = pi[ii].db[2 * nr + 1];
                break;
            };
        }
    }
    else    return 1;

    return 0;
}

float   *extract_z_profile_from_movie(O_i *ois, int xc, int yc, float *z)
{
    int  k;
    int nx, ny, nf, im_c;
    double val = 0.;


    if (ois == NULL || ois->im.n_f < 2) return NULL; /* not a movie */

    nx = ois->im.nx;
    ny = ois->im.ny;
    nf = ois->im.n_f;

    if (xc < 0 || xc > nx || yc < 0 || yc > ny) return NULL;

    if (z ==  NULL)     z = (float *)calloc(nf, sizeof(float));

    if (z == NULL)      return NULL;

    im_c = ois->im.c_f;

    for (k = 0; k < nf; k++)
    {
        switch_frame(ois, k);
        get_raw_pixel_value(ois, xc, yc, &val);
        z[k] = val;
    }

    switch_frame(ois, im_c);
    return z;
}


int extract_z_partial_profile_from_movie(O_i *ois, int xc, int yc, float *z, int fbeg, int fend)
{
    int  k;
    int nx, ny, nf, im_c;
    double val = 0.;


    if (z==NULL) return 1;
    if (ois == NULL || ois->im.n_f < 2) return 1; /* not a movie */

    nx = ois->im.nx;
    ny = ois->im.ny;
    nf = ois->im.n_f;

    if (xc < 0 || xc > nx || yc < 0 || yc > ny || fbeg>fend || fbeg<0 || fend >= nf)
        return 1;


    im_c = ois->im.c_f;

    for (k = 0; k <= fend-fbeg+1 ; k++)
    {
        switch_frame(ois, fbeg+k);
        get_raw_pixel_value(ois, xc, yc, &val);
        z[k] = val;
    }

    switch_frame(ois, im_c);
    return 0;
}



int set_z_profile_from_movie(O_i *ois, int xc, int yc, float *z, int nz)
{
    int  k;
    int nx, ny, nf;
    double val[4] = {0};


    if (ois == NULL || ois->im.n_f < 2) return 1; /* not a movie */

    nx = ois->im.nx;
    ny = ois->im.ny;
    nf = ois->im.n_f;

    if (xc < 0 || xc > nx || yc < 0 || yc > ny) return 1;

    if (z == NULL)      return 1;

    for (k = 0; k < nf && k < nz; k++)
    {
        switch_frame(ois, k);
        val[0] = z[k];
        set_raw_pixel_value(ois, xc, yc, val);
    }

    return 0;
}



int     switch_frame(O_i *oi, int n)
{
    O_p *op = NULL;
    if (oi == NULL)     return xvin_error(Wrong_Argument);
    int ret = 0;
    union pix *p = NULL;

    if (oi->im.movie_on_disk > 0)
    {
        if (oi->im.movie_on_disk == 2)   // for complementary disk movie function (tif for ewxample)
        {
          if (switch_frame_of_disk_movie_c != NULL) (*switch_frame_of_disk_movie_c)(oi, n);
        }
        else ret = switch_frame_of_disk_movie(oi, n);   // switch frame of disk movie
        if (ret == -9876)   // if you try to access more than the end of the file give you 9876
        {
          n = 0;          // in this case try to go back to n = 0
          ret = switch_frame_of_disk_movie(oi,n);
          if (ret == -9876) return -1;  // si n'arrive même pas à 0 renvoi -1 gros problème qui ne devrait pas arriver
        }

      }
    else if (n == 0) return 0;
    else if (n >= oi->im.n_f) return -1;
    else
    {
        n = (n >= oi->im.n_f) ? oi->im.n_f - 1 : n;
        n = (n < 0) ? 0 : n;
        oi->im.pixel = oi->im.pxl[n];
	if (oi->o_p != NULL)
	  {
	    op = oi->o_p[oi->cur_op];
	    if ((op->type & DS_OF_OP_SYNCHRONIZE_WITH_MOVIE)
		&& op->n_dat == oi->im.n_f)
	      {
		op->cur_dat = (n < op->n_dat) ? n : op->n_dat-1;
		op->display_single_ds = 1;
	      }
	  }

    }
    oi->im.c_f = n;
    oi->need_to_refresh |= BITMAP_NEED_REFRESH;
    return n;  // ne renvoie la valeur de départ que si ça a bien marché.
}




/* width and height = 1 means 512 screen pixels (for screen_scale = PIXEL_0) */

int     set_oi_horizontal_extend(O_i *oi, float width)
{
    if (oi == NULL) return xvin_error(Wrong_Argument);

    oi->width = width;
    oi->need_to_refresh |= ALL_NEED_REFRESH;
    return 0;
}

int     set_oi_vertical_extend(O_i *oi, float height)
{
    if (oi == NULL) return xvin_error(Wrong_Argument);

    oi->height = height;
    oi->need_to_refresh |= ALL_NEED_REFRESH;
    return 0;
}

int     smart_map_pixel_ratio_of_image_and_screen(O_i *oi)
{
    float ratiox, ratioy;

    if (oi == NULL) return xvin_error(Wrong_Argument);

    ratiox = ((float)(512)) / oi->im.nx;
    ratioy = ((float)(512)) / oi->im.ny;
    ratiox = (ratiox > ratioy) ? ratiox : ratioy;
    ratiox = (int)(0.5 + ratiox);

    if (ratiox != 0)  oi->width = (float)(oi->im.nx * ratiox) / (512);

    if (ratiox != 0)  oi->height = (float)(oi->im.ny) * ratiox / (512);

    oi->need_to_refresh |= ALL_NEED_REFRESH;
    return 0;
}

int     map_pixel_ratio_of_image_and_screen(O_i *oi, float ratiox, float ratioy)
{
    if (oi == NULL) return xvin_error(Wrong_Argument);

    if (ratiox != 0)  oi->width = (float)(oi->im.nx) / (ratiox * 512);

    if (ratiox != 0)  oi->height = (float)(oi->im.ny) / (ratioy * 512);

    oi->need_to_refresh |= ALL_NEED_REFRESH;
    return 0;
}
int     what_is_pixel_ratio_of_image_and_screen(O_i *oi, float *ratiox, float *ratioy)
{
    if (oi == NULL) return xvin_error(Wrong_Argument);

    if (ratiox != NULL && oi->width != 0) *ratiox = (float)(oi->im.nx) / (oi->width * 512);

    if (ratioy != NULL && oi->height != 0) *ratioy = (float)(oi->im.ny) / (oi->height * 512);

    return 0;
}

char *get_oi_x_unit_name(O_i *oi)
{
    return (oi != NULL) ? oi->x_unit : (char *) xvin_ptr_error(Wrong_Argument);
}
char *get_oi_y_unit_name(O_i *oi)
{
    return (oi != NULL) ? oi->y_unit : (char *) xvin_ptr_error(Wrong_Argument);
}
char *get_oi_z_unit_name(O_i *oi)
{
    if (oi == NULL)     return (char *) xvin_ptr_error(Wrong_Argument);

    return (oi->c_zu < 0) ? NULL : get_unit_name(oi->zu[oi->c_zu]);
}
int set_oi_bottom_left_axes(O_i *oi, int mode)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    if (mode != USER && mode != PIXEL && mode != NO_NUMBER)
        return xvin_error(Wrong_Option);

    if (mode == USER)       oi->iopt2 |= IM_USR_COOR_LB;
    else if (mode == PIXEL)     oi->iopt2 &= ~IM_USR_COOR_LB;

    if (mode == NO_NUMBER)      oi->iopt2 &= ~X_NUM & ~Y_NUM;
    else                oi->iopt2 |= X_NUM | Y_NUM;

    oi->need_to_refresh |= PLOT_NEED_REFRESH    | EXTRA_PLOT_NEED_REFRESH;
    return 0;
}
int is_oi_bottom_left_axes_in_pixel(O_i *oi)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    return (oi->iopt2 & IM_USR_COOR_LB) ? NO : YES;
}
int is_oi_bottom_left_axes_numbered(O_i *oi)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    return (oi->iopt2 & (X_NUM | Y_NUM)) ? YES : NO;
}
int set_oi_top_right_axes(O_i *oi, int mode)
{
    if (oi == NULL)         return xvin_error(Wrong_Argument);

    if (mode != USER && mode != PIXEL && mode != NO_NUMBER)
        return xvin_error(Wrong_Option);

    if (mode == USER)       oi->iopt2 |= IM_USR_COOR_RT;
    else if (mode == PIXEL)     oi->iopt2 &= ~IM_USR_COOR_RT;

    if (mode == NO_NUMBER)      oi->iopt2 &= ~X_NUM & ~Y_NUM;
    else                oi->iopt2 |= X_NUM | Y_NUM;

    oi->need_to_refresh |= PLOT_NEED_REFRESH    | EXTRA_PLOT_NEED_REFRESH;
    return 0;
}
int is_oi_top_right_axes_in_pixel(O_i *oi)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    return (oi->iopt2 & IM_USR_COOR_RT) ? NO : YES;
}
int is_oi_top_right_axes_numbered(O_i *oi)
{
    if (oi == NULL)     return xvin_error(Wrong_Argument);

    return (oi->iopt2 & (X_NUM | Y_NUM)) ? YES : NO;
}

int set_oi_plot_axes(O_i *oi, int mode)
{
    O_p *op = find_oi_cur_op(oi);

    if (op == NULL)     return xvin_error(Wrong_Argument);

    if (mode == IM_SAME_Y_AXIS)     op->type |= IM_SAME_Y_AXIS;
    else if (mode == IM_SAME_X_AXIS)    op->type |= IM_SAME_X_AXIS;
    else if (mode == DS_OF_OP_SYNCHRONIZE_WITH_MOVIE)
      op->type |= DS_OF_OP_SYNCHRONIZE_WITH_MOVIE;
    else op->type &= ~IM_SAME_Y_AXIS & ~IM_SAME_X_AXIS;
    oi->need_to_refresh |= PLOT_NEED_REFRESH    | EXTRA_PLOT_NEED_REFRESH;
    return 0;
}
int get_oi_plot_axes(O_i *oi)
{
    O_p *op = find_oi_cur_op(oi);

    if (op == NULL) return xvin_error(Wrong_Argument);

    return (op->type & (IM_SAME_X_AXIS | IM_SAME_Y_AXIS));
}
int is_oi_x_axes_same_as_plot(O_i *oi)
{
    return (get_oi_plot_axes(oi) == IM_SAME_X_AXIS) ? YES : NO;
}
int is_oi_y_axes_same_as_plot(O_i *oi)
{
    return (get_oi_plot_axes(oi) == IM_SAME_Y_AXIS) ? YES : NO;
}

int set_zmin_zmax_values(O_i *oi, float zmin, float zmax)
{
    if (oi == NULL) return xvin_error(Wrong_Argument);

    oi->z_min = zmin;
    oi->z_max = zmax;
    oi->need_to_refresh |= BITMAP_NEED_REFRESH;
    return 0;
}
int set_RGB_zmin_zmax_values(O_i *oi, float Rzmin, float Rzmax, float Gzmin, float Gzmax, float Bzmin, float Bzmax)
{
    if (oi == NULL) return xvin_error(Wrong_Argument);

    oi->z_Rmin = Rzmin;
    oi->z_Rmax = Rzmax;
    oi->z_Gmin = Gzmin;
    oi->z_Gmax = Gzmax;
    oi->z_Bmin = Bzmin;
    oi->z_Bmax = Bzmax;
    oi->need_to_refresh |= BITMAP_NEED_REFRESH;
    return 0;
}
int get_zmin_zmax_values(O_i *oi, float *zmin, float *zmax)
{
    if (oi == NULL) return xvin_error(Wrong_Argument);

    if (zmin) *zmin = oi->z_min;
    if (zmax) *zmax = oi->z_max;
    return 0;
}

int get_RGB_zmin_zmax_values(O_i *oi, float *Rzmin, float *Rzmax, float *Gzmin, float *Gzmax, float *Bzmin, float *Bzmax)
{
    if (oi == NULL) return xvin_error(Wrong_Argument);

    if (Rzmin) *Rzmin = oi->z_Rmin;
    if (Rzmax) *Rzmax = oi->z_Rmax;
    if (Gzmin) *Gzmin = oi->z_Gmin;
    if (Gzmax) *Gzmax = oi->z_Gmax;
    if (Bzmin) *Bzmin = oi->z_Bmin;
    if (Bzmax) *Bzmax = oi->z_Bmax;
    return 0;
}

int set_z_black_z_white_values(O_i *oi, float zb, float zw)
{
    if (oi == NULL) return xvin_error(Wrong_Argument);

    oi->z_black = zb;
    oi->z_white = zw;
    oi->need_to_refresh |= BITMAP_NEED_REFRESH;
    return 0;
}
int get_z_black_z_white_values(O_i *oi, float *zb, float *zw)
{
    if (oi == NULL) return xvin_error(Wrong_Argument);

    *zb = oi->z_black;
    *zw = oi->z_white;
    return 0;
}
O_p *find_oi_cur_op(O_i *oi)
{
    return (oi == NULL || oi->o_p == NULL || oi->cur_op < 0 || oi->cur_op >= oi->n_op) ? NULL : oi->o_p[oi->cur_op];
}

O_p  *create_and_attach_op_to_oi(O_i *oi, int nx, int ny, int type, int axes_type)
{
    O_p *op;

    (void)ny;
    (void)type;

    if (oi == NULL) return NULL;

    op = create_one_plot(nx, nx, 0);

    if (op == NULL) return (O_p *) xvin_ptr_error(Out_Of_Memory);

    op->filename = Transfer_filename(oi->filename);
    inherit_from_im_to_ds(op->dat[0], oi);
    op->width = oi->width;
    op->height = oi->height;
    op->type = axes_type;
    add_one_image(oi, IS_ONE_PLOT, (void *)op);
    return op;
}
d_s  *create_and_attach_ds_to_oi(O_i *oi, int nx, int ny, int type)
{
    d_s *ds = NULL;
    O_p *op = NULL;

    if (oi == NULL) return (d_s *) xvin_ptr_error(Wrong_Argument);

    op = find_oi_cur_op(oi);

    if (op == NULL) return (d_s *) xvin_ptr_error(Wrong_Argument);

    ds = create_and_attach_one_ds(op, nx, ny, type);
    oi->need_to_refresh |= PLOTS_NEED_REFRESH;
    return ds;
}

p_l  *create_and_attach_label_to_oi(O_i *oi, float x, float y, int type, const char *format, ...)
{
    p_l *pl;
    char *c;
    va_list ap;

    if (oi == NULL)     return (p_l *) xvin_ptr_error(Wrong_Argument);

    c = (char *)calloc(2048, sizeof(char));

    if (c == NULL)              return (p_l *) xvin_ptr_error(Out_Of_Memory);

    va_start(ap, format);
    vsnprintf(c, 2048, format, ap);
    va_end(ap);
    pl = create_plot_label(type, x, y, "%s",c);
    free(c);

    if (pl == NULL)     return (p_l *) xvin_ptr_error(Out_Of_Memory);

    add_to_one_image(oi, IS_PLOT_LABEL, pl);
    return pl;
}

un_s    *create_and_attach_unit_set_to_oi(O_i *oi, int type, float ax, float dx, char decade, char mode, char *name,
        int axis)
{
    un_s *un = NULL;

    if (oi == NULL)     return (un_s *) xvin_ptr_error(Wrong_Argument);

    un = build_unit_set(type, ax, dx, decade, mode, name);

    if (un == NULL)     return (un_s *) xvin_ptr_error(Out_Of_Memory);

    add_to_one_image(oi, axis, un);
    return un;
}


un_s    *create_attach_select_x_un_to_oi(O_i *oi, int type, float ax, float dx, char decade, char mode, char *name)
{
    un_s *un  = NULL;
    un = create_and_attach_unit_set_to_oi(oi, type, ax, dx, decade, mode, name, IS_X_UNIT_SET);
    set_oi_x_unit_set(oi, oi->n_xu - 1);
    oi->need_to_refresh |= PLOT_NEED_REFRESH;
    return un;
}
un_s    *create_attach_select_y_un_to_oi(O_i *oi, int type, float ax, float dx, char decade, char mode, char *name)
{
    un_s *un = NULL;
    un = create_and_attach_unit_set_to_oi(oi, type, ax, dx, decade, mode, name, IS_Y_UNIT_SET);
    set_oi_y_unit_set(oi, oi->n_yu - 1);
    oi->need_to_refresh |= PLOT_NEED_REFRESH;
    return un;
}



/*  push_image_label(tmpx,tmpy,c2);
 *  DESCRIPTION
 *
 *
 *
 *  RETURNS     0 on success, 1
 *
 */
int push_image_label(O_i *oi, float tx, float ty, char *label, int type)
{
    p_l *pl = NULL;

    if (oi == NULL || label == NULL)        return xvin_error(Wrong_Argument);

    pl = (p_l *)calloc(1, sizeof(struct plot_label));

    if (pl == NULL)    return xvin_error(Out_Of_Memory);

    pl->xla = tx;
    pl->yla = ty;
    pl->text = strdup(label);
    pl->type = type;

    if (pl->text == NULL)   return xvin_error(Out_Of_Memory);

    return  add_one_image(oi, IS_PLOT_LABEL, (void *)pl);
}

unsigned char   *get_oi_char_line_ptr(O_i *oi, int line)
{
    if (oi == NULL)     return (unsigned char *)xvin_ptr_error(Wrong_Argument);

    for (; line < 0; line += oi->im.ny);

    return   oi->im.pixel[line % oi->im.ny].ch;
}
short int       *get_oi_int_line_ptr(O_i *oi, int line)
{
    if (oi == NULL)     return (short *) xvin_ptr_error(Wrong_Argument);

    for (; line < 0; line += oi->im.ny);

    return   oi->im.pixel[line % oi->im.ny].in;
}
float   *get_oi_float_line_ptr(O_i *oi, int line)
{
    if (oi == NULL)     return (float *) xvin_ptr_error(Wrong_Argument);

    for (; line < 0; line += oi->im.ny);

    return   oi->im.pixel[line % oi->im.ny].fl;
}
float   *get_oi_complex_line_ptr(O_i *oi, int line)
{
    if (oi == NULL)     return (float *) xvin_ptr_error(Wrong_Argument);

    for (; line < 0; line += oi->im.ny);

    return   oi->im.pixel[line % oi->im.ny].fl;
}

int find_zmin_zmax(O_i *oi)
{
    int i, j;
    int mode, *li = NULL;
    unsigned char *ch = NULL;
    rgba_t *rgba = NULL;
    rgb_t *rgb = NULL;
    rgba16_t *rgba16 = NULL;
    rgb16_t *rgb16 = NULL;
    short int *in = NULL;
    unsigned short int *ui = NULL;
    float *fl = NULL, zmin , zmax, t, zr, zi;
    double *db = NULL;

    if (oi == NULL)     return xvin_error(Wrong_Argument);

    mode = oi->im.mode;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {
        zmax = zmin = (float)oi->im.pixel[oi->im.nys].ch[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            ch = oi->im.pixel[i].ch;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(ch[j]);

                if (t > zmax)   zmax = t;

                if (t < zmin)   zmin = t;
            }
        }

        zmax = oi->az + oi->dz * (zmax == zmin) ? zmax + 1 : zmax;
        zmin = oi->az + oi->dz * zmin;
    }
    else    if (oi->im.data_type == IS_RGB_PICTURE)
    {    //YUV conversion
         zmax = zmin = 0.299 * oi->im.pixel[oi->im.nys].rgb[oi->im.nxs].r
                 + 0.587 * oi->im.pixel[oi->im.nys].rgb[oi->im.nxs].g
                 +  0.114 * oi->im.pixel[oi->im.nys].rgb[oi->im.nxs].b;

    //zmax = zmin = (float)(oi->im.pixel[oi->im.nys].ch[3 * (oi->im.nxs)]
    //                        + oi->im.pixel[oi->im.nys].ch[(3 * oi->im.nxs) + 1]
    //                        + oi->im.pixel[oi->im.nys].ch[(3 * oi->im.nxs) + 2]);

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            rgb = oi->im.pixel[i].rgb;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = 0.299 * rgb[j].r + 0.587 * rgb[j].g +  0.114 *  rgb[j].b;

                if (t > zmax)   zmax = t;

                if (t < zmin)   zmin = t;
            }
        }

        zmax = oi->az + oi->dz * (zmax / 3 == zmin / 3) ? (zmax / 3) + 1 : zmax / 3;
        zmin = oi->az + oi->dz * zmin / 3;
    }
    else    if (oi->im.data_type == IS_RGBA_PICTURE)
    {
            zmax = zmin = 0.299 * oi->im.pixel[oi->im.nys].rgba[oi->im.nxs].r
                    + 0.587 * oi->im.pixel[oi->im.nys].rgba[oi->im.nxs].g
                    +  0.114 * oi->im.pixel[oi->im.nys].rgba[oi->im.nxs].b;
             //zmax = zmin = (float)(oi->im.pixel[oi->im.nys].rgba[oi->im.nxs].b
             //                 + oi->im.pixel[oi->im.nys].rgba[oi->im.nxs].g
             //               + oi->im.pixel[oi->im.nys].rgba[oi->im.nxs].r);

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            rgba = oi->im.pixel[i].rgba;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = 0.299 * rgba[j].r + 0.587 * rgba[j].g +  0.114 *  rgba[j].b;
            //t = (float)(rgba[j].b + rgba[j].g + rgba[j].r);

                if (t > zmax)   zmax = t;

                if (t < zmin)   zmin = t;
            }
        }

        zmax = oi->az + oi->dz * (zmax / 3 == zmin / 3) ? (zmax / 3) + 1 : zmax / 3;
        zmin = oi->az + oi->dz * zmin / 3;
    }
    else    if (oi->im.data_type == IS_RGB16_PICTURE)
    {
         zmax = zmin = 0.299 * oi->im.pixel[oi->im.nys].rgb16[oi->im.nxs].r
                 + 0.587 * oi->im.pixel[oi->im.nys].rgb16[oi->im.nxs].g
                 +  0.114 * oi->im.pixel[oi->im.nys].rgb16[oi->im.nxs].b;
//zmax = zmin = (float)(oi->im.pixel[oi->im.nys].in[3 * (oi->im.nxs)]
//                            + oi->im.pixel[oi->im.nys].in[(3 * oi->im.nxs) + 1]
//                            + oi->im.pixel[oi->im.nys].in[(3 * oi->im.nxs) + 2]);

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            in = oi->im.pixel[i].in;
            rgb16 = oi->im.pixel[i].rgb16;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = 0.299 * rgb16[j].r + 0.587 * rgb16[j].g +  0.114 *  rgb16[j].b;
            //t = (float)(in[3 * j] + in[(3 * j) + 1] + in[(3 * j) + 2]);

                if (t > zmax)   zmax = t;

                if (t < zmin)   zmin = t;
            }
        }

        zmax = oi->az + oi->dz * (zmax / 3 == zmin / 3) ? (zmax / 3) + 1 : zmax / 3;
        zmin = oi->az + oi->dz * zmin / 3;
    }
    else    if (oi->im.data_type == IS_RGBA16_PICTURE)
    {
         zmax = zmin = 0.299 * oi->im.pixel[oi->im.nys].rgba16[oi->im.nxs].r
                 + 0.587 * oi->im.pixel[oi->im.nys].rgba16[oi->im.nxs].g
                 +  0.114 * oi->im.pixel[oi->im.nys].rgba16[oi->im.nxs].b;
//zmax = zmin = (float)(oi->im.pixel[oi->im.nys].rgba16[oi->im.nxs].b
//                            + oi->im.pixel[oi->im.nys].rgba16[oi->im.nxs].g
//                            + oi->im.pixel[oi->im.nys].rgba16[oi->im.nxs].r);

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            rgba16 = oi->im.pixel[i].rgba16;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                 t = 0.299 * rgba16[j].r + 0.587 * rgba16[j].g +  0.114 *  rgba16[j].b;
            //t = (float)(rgba16[j].b + rgba16[j].g + rgba16[j].r);

                if (t > zmax)   zmax = t;

                if (t < zmin)   zmin = t;
            }
        }

        zmax = oi->az + oi->dz * (zmax / 3 == zmin / 3) ? (zmax / 3) + 1 : zmax / 3;
        zmin = oi->az + oi->dz * zmin / 3;
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
        zmax = zmin = oi->im.pixel[oi->im.nys].in[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            in = oi->im.pixel[i].in;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(in[j]);

                if (t > zmax)   zmax = t;

                if (t < zmin)   zmin = t;
            }
        }

        zmax = oi->az + oi->dz * (zmax == zmin) ? zmax + 1 : zmax;
        zmin = oi->az + oi->dz * zmin;
    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {
        zmax = zmin = oi->im.pixel[oi->im.nys].ui[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            ui = oi->im.pixel[i].ui;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(ui[j]);

                if (t > zmax)   zmax = t;

                if (t < zmin)   zmin = t;
            }
        }

        zmax = oi->az + oi->dz * (zmax == zmin) ? zmax + 1 : zmax;
        zmin = oi->az + oi->dz * zmin;
    }
    else if (oi->im.data_type == IS_LINT_IMAGE)
    {
        zmax = zmin = oi->im.pixel[oi->im.nys].li[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            li = oi->im.pixel[i].li;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(li[j]);

                if (t > zmax)   zmax = t;

                if (t < zmin)   zmin = t;
            }
        }

        zmax = oi->az + oi->dz * (zmax == zmin) ? zmax + 1 : zmax;
        zmin = oi->az + oi->dz * zmin;
    }
    else if (oi->im.data_type == IS_FLOAT_IMAGE)
    {
        zmax = zmin = oi->im.pixel[oi->im.nys].fl[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            fl = oi->im.pixel[i].fl;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = fl[j];

                if (t > zmax)   zmax = t;

                if (t < zmin)   zmin = t;
            }
        }
    }
    else if (oi->im.data_type == IS_DOUBLE_IMAGE)
    {
        zmax = zmin = oi->im.pixel[oi->im.nys].db[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            db = oi->im.pixel[i].db;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = db[j];

                if (t > zmax)   zmax = t;

                if (t < zmin)   zmin = t;
            }
        }
    }
    else if (oi->im.data_type == IS_COMPLEX_IMAGE)
    {
        zr = oi->im.pixel[oi->im.nys].fl[2 * oi->im.nxs];
        zi = oi->im.pixel[oi->im.nys].fl[2 * oi->im.nxs + 1];

        if (mode == RE)             zmin = zr;
        else if (mode == IM)        zmin = zi;
        else if (mode == AMP)       zmin = sqrt(zr * zr + zi * zi);
        else if (mode == AMP_2)     zmin = zr * zr + zi * zi;
        else if (mode == LOG_AMP)   zmin = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
        else return (win_printf("Unknown mode for complex image"));

        zmax = zmin;

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            fl = oi->im.pixel[i].fl;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                zr = fl[2 * j];
                zi = fl[2 * j + 1];

                if (mode == RE)     t = zr;
                else if (mode == IM)    t = zi;
                else if (mode == AMP)   t = sqrt(zr * zr + zi * zi);
                else if (mode == AMP_2) t = zr * zr + zi * zi;
                else if (mode == LOG_AMP)   t = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
                else return 1;

                if (t > zmax)   zmax = t;

                if (t < zmin)   zmin = t;
            }
        }
    }
    else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        zr = oi->im.pixel[oi->im.nys].db[2 * oi->im.nxs];
        zi = oi->im.pixel[oi->im.nys].db[2 * oi->im.nxs + 1];

        if (mode == RE)             zmin = zr;
        else if (mode == IM)        zmin = zi;
        else if (mode == AMP)       zmin = sqrt(zr * zr + zi * zi);
        else if (mode == AMP_2)     zmin = zr * zr + zi * zi;
        else if (mode == LOG_AMP)   zmin = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
        else return (win_printf("Unknown mode for complex image"));

        zmax = zmin;

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            db = oi->im.pixel[i].db;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                zr = db[2 * j];
                zi = db[2 * j + 1];

                if (mode == RE)     t = zr;
                else if (mode == IM)    t = zi;
                else if (mode == AMP)   t = sqrt(zr * zr + zi * zi);
                else if (mode == AMP_2) t = zr * zr + zi * zi;
                else if (mode == LOG_AMP)   t = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
                else return 1;

                if (t > zmax)   zmax = t;

                if (t < zmin)   zmin = t;
            }
        }
    }
    else return 1;

    oi->z_black = oi->z_min = zmin;
    oi->z_white = oi->z_max = (zmax == zmin) ? zmax + 1 : zmax;
    oi->need_to_refresh |= BITMAP_NEED_REFRESH;
    return 0;
}
int find_zmin_zmax_and_pos(O_i *oi, int *x_min, int *x_max, int *y_min, int *y_max)
{
    int i, j;
    int mode, *li = NULL;
    unsigned char *ch = NULL;
    rgba_t *rgba = NULL;
    rgba16_t *rgba16 = NULL;
    short int *in = NULL;
    unsigned short int *ui = NULL;
    float *fl = NULL, zmin , zmax, t, zr, zi;
    double *db = NULL;

    if (oi == NULL)     return xvin_error(Wrong_Argument);

    mode = oi->im.mode;
    *x_min = *x_max = oi->im.nxs;
    *y_min = *y_max = oi->im.nys;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {
        zmax = zmin = (float)oi->im.pixel[oi->im.nys].ch[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            ch = oi->im.pixel[i].ch;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(ch[j]);

                if (t > zmax)
                {
                    zmax = t;
                    *x_max = j;
                    *y_max = i;
                }

                if (t < zmin)
                {
                    zmin = t;
                    *x_min = j;
                    *y_min = i;
                }
            }
        }

        zmax = oi->az + oi->dz * (zmax == zmin) ? zmax + 1 : zmax;
        zmin = oi->az + oi->dz * zmin;
    }
    else    if (oi->im.data_type == IS_RGB_PICTURE)
    {
        zmax = zmin = (float)(oi->im.pixel[oi->im.nys].ch[3 * (oi->im.nxs)]
                              + oi->im.pixel[oi->im.nys].ch[(3 * oi->im.nxs) + 1]
                              + oi->im.pixel[oi->im.nys].ch[(3 * oi->im.nxs) + 2]);

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            ch = oi->im.pixel[i].ch;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(ch[3 * j] + ch[(3 * j) + 1] + ch[(3 * j) + 2]);

                if (t > zmax)
                {
                    zmax = t;
                    *x_max = j;
                    *y_max = i;
                }

                if (t < zmin)
                {
                    zmin = t;
                    *x_min = j;
                    *y_min = i;
                }
            }
        }

        zmax = oi->az + oi->dz * (zmax / 3 == zmin / 3) ? (zmax / 3) + 1 : zmax / 3;
        zmin = oi->az + oi->dz * zmin / 3;
    }
    else    if (oi->im.data_type == IS_RGBA_PICTURE)
    {
        zmax = zmin = (float)(oi->im.pixel[oi->im.nys].rgba[oi->im.nxs].b
                              + oi->im.pixel[oi->im.nys].rgba[oi->im.nxs].g
                              + oi->im.pixel[oi->im.nys].rgba[oi->im.nxs].r);

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            rgba = oi->im.pixel[i].rgba;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(rgba[j].b + rgba[j].g + rgba[j].r);

                if (t > zmax)
                {
                    zmax = t;
                    *x_max = j;
                    *y_max = i;
                }

                if (t < zmin)
                {
                    zmin = t;
                    *x_min = j;
                    *y_min = i;
                }
            }
        }

        zmax = oi->az + oi->dz * (zmax / 3 == zmin / 3) ? (zmax / 3) + 1 : zmax / 3;
        zmin = oi->az + oi->dz * zmin / 3;
    }
    else    if (oi->im.data_type == IS_RGB16_PICTURE)
    {
        zmax = zmin = (float)(oi->im.pixel[oi->im.nys].in[3 * (oi->im.nxs)]
                              + oi->im.pixel[oi->im.nys].in[(3 * oi->im.nxs) + 1]
                              + oi->im.pixel[oi->im.nys].in[(3 * oi->im.nxs) + 2]);

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            in = oi->im.pixel[i].in;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(in[3 * j] + in[(3 * j) + 1] + in[(3 * j) + 2]);

                if (t > zmax)
                {
                    zmax = t;
                    *x_max = j;
                    *y_max = i;
                }

                if (t < zmin)
                {
                    zmin = t;
                    *x_min = j;
                    *y_min = i;
                }
            }
        }

        zmax = oi->az + oi->dz * (zmax / 3 == zmin / 3) ? (zmax / 3) + 1 : zmax / 3;
        zmin = oi->az + oi->dz * zmin / 3;
    }
    else    if (oi->im.data_type == IS_RGBA_PICTURE)
    {
        zmax = zmin = (float)(oi->im.pixel[oi->im.nys].rgba16[oi->im.nxs].b
                              + oi->im.pixel[oi->im.nys].rgba16[oi->im.nxs].g
                              + oi->im.pixel[oi->im.nys].rgba16[oi->im.nxs].r);

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            rgba16 = oi->im.pixel[i].rgba16;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(rgba16[j].b + rgba16[j].g + rgba16[j].r);

                if (t > zmax)
                {
                    zmax = t;
                    *x_max = j;
                    *y_max = i;
                }

                if (t < zmin)
                {
                    zmin = t;
                    *x_min = j;
                    *y_min = i;
                }
            }
        }

        zmax = oi->az + oi->dz * (zmax / 3 == zmin / 3) ? (zmax / 3) + 1 : zmax / 3;
        zmin = oi->az + oi->dz * zmin / 3;
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
        zmax = zmin = oi->im.pixel[oi->im.nys].in[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            in = oi->im.pixel[i].in;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(in[j]);

                if (t > zmax)
                {
                    zmax = t;
                    *x_max = j;
                    *y_max = i;
                }

                if (t < zmin)
                {
                    zmin = t;
                    *x_min = j;
                    *y_min = i;
                }
            }
        }

        zmax = oi->az + oi->dz * (zmax == zmin) ? zmax + 1 : zmax;
        zmin = oi->az + oi->dz * zmin;
    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {
        zmax = zmin = oi->im.pixel[oi->im.nys].ui[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            ui = oi->im.pixel[i].ui;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(ui[j]);

                if (t > zmax)
                {
                    zmax = t;
                    *x_max = j;
                    *y_max = i;
                }

                if (t < zmin)
                {
                    zmin = t;
                    *x_min = j;
                    *y_min = i;
                }
            }
        }

        zmax = oi->az + oi->dz * (zmax == zmin) ? zmax + 1 : zmax;
        zmin = oi->az + oi->dz * zmin;
    }
    else if (oi->im.data_type == IS_LINT_IMAGE)
    {
        zmax = zmin = oi->im.pixel[oi->im.nys].li[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            li = oi->im.pixel[i].li;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(li[j]);

                if (t > zmax)
                {
                    zmax = t;
                    *x_max = j;
                    *y_max = i;
                }

                if (t < zmin)
                {
                    zmin = t;
                    *x_min = j;
                    *y_min = i;
                }
            }
        }

        zmax = oi->az + oi->dz * (zmax == zmin) ? zmax + 1 : zmax;
        zmin = oi->az + oi->dz * zmin;
    }
    else if (oi->im.data_type == IS_FLOAT_IMAGE)
    {
        zmax = zmin = oi->im.pixel[oi->im.nys].fl[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            fl = oi->im.pixel[i].fl;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = fl[j];

                if (t > zmax)
                {
                    zmax = t;
                    *x_max = j;
                    *y_max = i;
                }

                if (t < zmin)
                {
                    zmin = t;
                    *x_min = j;
                    *y_min = i;
                }
            }
        }
    }
    else if (oi->im.data_type == IS_DOUBLE_IMAGE)
    {
        zmax = zmin = oi->im.pixel[oi->im.nys].db[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            db = oi->im.pixel[i].db;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = db[j];

                if (t > zmax)
                {
                    zmax = t;
                    *x_max = j;
                    *y_max = i;
                }

                if (t < zmin)
                {
                    zmin = t;
                    *x_min = j;
                    *y_min = i;
                }
            }
        }
    }
    else if (oi->im.data_type == IS_COMPLEX_IMAGE)
    {
        zr = oi->im.pixel[oi->im.nys].fl[2 * oi->im.nxs];
        zi = oi->im.pixel[oi->im.nys].fl[2 * oi->im.nxs + 1];

        if (mode == RE)             zmin = zr;
        else if (mode == IM)        zmin = zi;
        else if (mode == AMP)       zmin = sqrt(zr * zr + zi * zi);
        else if (mode == AMP_2)     zmin = zr * zr + zi * zi;
        else if (mode == LOG_AMP)   zmin = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
        else return (win_printf("Unknown mode for complex image"));

        zmax = zmin;

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            fl = oi->im.pixel[i].fl;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                zr = fl[2 * j];
                zi = fl[2 * j + 1];

                if (mode == RE)     t = zr;
                else if (mode == IM)    t = zi;
                else if (mode == AMP)   t = sqrt(zr * zr + zi * zi);
                else if (mode == AMP_2) t = zr * zr + zi * zi;
                else if (mode == LOG_AMP)   t = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
                else return 1;

                if (t > zmax)
                {
                    zmax = t;
                    *x_max = j;
                    *y_max = i;
                }

                if (t < zmin)
                {
                    zmin = t;
                    *x_min = j;
                    *y_min = i;
                }
            }
        }
    }
    else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        zr = oi->im.pixel[oi->im.nys].db[2 * oi->im.nxs];
        zi = oi->im.pixel[oi->im.nys].db[2 * oi->im.nxs + 1];

        if (mode == RE)             zmin = zr;
        else if (mode == IM)        zmin = zi;
        else if (mode == AMP)       zmin = sqrt(zr * zr + zi * zi);
        else if (mode == AMP_2)     zmin = zr * zr + zi * zi;
        else if (mode == LOG_AMP)   zmin = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
        else return (win_printf("Unknown mode for complex image"));

        zmax = zmin;

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            db = oi->im.pixel[i].db;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                zr = db[2 * j];
                zi = db[2 * j + 1];

                if (mode == RE)     t = zr;
                else if (mode == IM)    t = zi;
                else if (mode == AMP)   t = sqrt(zr * zr + zi * zi);
                else if (mode == AMP_2) t = zr * zr + zi * zi;
                else if (mode == LOG_AMP)   t = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
                else return 1;

                if (t > zmax)
                {
                    zmax = t;
                    *x_max = j;
                    *y_max = i;
                }

                if (t < zmin)
                {
                    zmin = t;
                    *x_min = j;
                    *y_min = i;
                }
            }
        }
    }
    else return 1;

    oi->z_black = oi->z_min = zmin;
    oi->z_white = oi->z_max = (zmax == zmin) ? zmax + 1 : zmax;
    oi->need_to_refresh |= BITMAP_NEED_REFRESH;
    return 0;
}


/*  This routine find the max within the ROI
 */
int find_zmax_and_pos_interpolate(O_i *oi, float *max_val, float *x_fmax, float *y_fmax)
{
    int i, j, ii, jj, kx, ky;
    int mode, *li = NULL, x_max, y_max;
    unsigned char *ch = NULL;
    rgba_t *rgba = NULL;
    rgba16_t *rgba16 = NULL;
    rgb_t *rgb = NULL;
    rgb16_t *rgb16 = NULL;
    short int *in = NULL;
    unsigned short int *ui = NULL;
    float *fl = NULL, zmax, t, zr, zi, zmx[5] = {0}, zmy[5] = {0}, maxvx = 0, maxvy = 0;
    double *db = NULL;

    if (oi == NULL)     return xvin_error(Wrong_Argument);

    mode = oi->im.mode;
    x_max = oi->im.nxs;
    y_max = oi->im.nys;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {
        zmax = (float)oi->im.pixel[oi->im.nys].ch[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
	  ch = oi->im.pixel[i].ch;
	  for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
	      t = (float)(ch[j]);
	      if (t > zmax)
                {
		  zmax = t;
		  x_max = j;
		  y_max = i;
                }
            }
        }
	for(i = 0; i < 5; i++) zmx[i] = zmy[i] = 0;
	for (i = y_max - 2, ky = 0 ; i < y_max + 2 ; i++, ky++)
	  {
	    ii = (i < 0) ? 0 : i;
	    ii = (i < oi->im.ny) ? ii : oi->im.ny - 1;
	    ch = oi->im.pixel[i].ch;
	    for (j = x_max - 2, kx = 0; j < x_max + 2 ; j++, kx++)
	      {
		jj = (j < 0) ? 0 : j;
		jj = (j < oi->im.nx) ? jj : oi->im.nx - 1;
		if (ky > 0 && ky < 4) zmx[kx] += ch[jj];
		if (kx > 0 && kx < 4) zmy[ky] += ch[jj];
            }
        }
	find_max_around(zmx, 5, 2, x_fmax, &maxvx, NULL);
	*x_fmax += x_max - 2;
	find_max_around(zmy, 5, 2, y_fmax, &maxvy, NULL);
	*y_fmax += y_max - 2;
	if (max_val) *max_val = (maxvx + maxvy)/8;
    }
    else    if (oi->im.data_type == IS_RGB_PICTURE)
    {
        zmax = (float)(oi->im.pixel[oi->im.nys].rgb[oi->im.nxs].r
		       + oi->im.pixel[oi->im.nys].rgb[oi->im.nxs].g
		       + oi->im.pixel[oi->im.nys].rgb[oi->im.nxs].b);

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            rgb = oi->im.pixel[i].rgb;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(rgb[j].r + rgb[j].g + rgb[j].b);

                if (t > zmax)
                {
                    zmax = t;
                    x_max = j;
                    y_max = i;
                }
            }
        }
	for(i = 0; i < 5; i++) zmx[i] = zmy[i] = 0;
	for (i = y_max - 2, ky = 0 ; i < y_max + 2 ; i++, ky++)
	  {
	    ii = (i < 0) ? 0 : i;
	    ii = (i < oi->im.ny) ? ii : oi->im.ny - 1;
	    rgb = oi->im.pixel[i].rgb;
	    for (j = x_max - 2, kx = 0; j < x_max + 2 ; j++, kx++)
	      {
		jj = (j < 0) ? 0 : j;
		jj = (j < oi->im.nx) ? jj : oi->im.nx - 1;
		t = (float)(rgb[jj].r + rgb[jj].g + rgb[jj].b);
		if (ky > 0 && ky < 4) zmx[kx] += t;
		if (kx > 0 && kx < 4) zmy[ky] += t;
            }
        }
	find_max_around(zmx, 5, 2, x_fmax, &maxvx, NULL);
	*x_fmax += x_max - 2;
	find_max_around(zmy, 5, 2, y_fmax, &maxvy, NULL);
	*y_fmax += y_max - 2;
	if (max_val) *max_val = (maxvx + maxvy)/8;
    }
    else    if (oi->im.data_type == IS_RGBA_PICTURE)
    {
        zmax = (float)(oi->im.pixel[oi->im.nys].rgba[oi->im.nxs].b
                              + oi->im.pixel[oi->im.nys].rgba[oi->im.nxs].g
                              + oi->im.pixel[oi->im.nys].rgba[oi->im.nxs].r);

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            rgba = oi->im.pixel[i].rgba;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(rgba[j].b + rgba[j].g + rgba[j].r);

                if (t > zmax)
                {
                    zmax = t;
                    x_max = j;
                    y_max = i;
                }
            }
        }
	for(i = 0; i < 5; i++) zmx[i] = zmy[i] = 0;
	for (i = y_max - 2, ky = 0 ; i < y_max + 2 ; i++, ky++)
	  {
	    ii = (i < 0) ? 0 : i;
	    ii = (i < oi->im.ny) ? ii : oi->im.ny - 1;
	    rgba = oi->im.pixel[i].rgba;
	    for (j = x_max - 2, kx = 0; j < x_max + 2 ; j++, kx++)
	      {
		jj = (j < 0) ? 0 : j;
		jj = (j < oi->im.nx) ? jj : oi->im.nx - 1;
		t = (float)(rgba[jj].b + rgba[jj].g + rgba[jj].r);
		if (ky > 0 && ky < 4) zmx[kx] += t;
		if (kx > 0 && kx < 4) zmy[ky] += t;
            }
        }
	find_max_around(zmx, 5, 2, x_fmax, &maxvx, NULL);
	*x_fmax += x_max - 2;
	find_max_around(zmy, 5, 2, y_fmax, &maxvy, NULL);
	*y_fmax += y_max - 2;
	if (max_val) *max_val = (maxvx + maxvy)/8;
    }
    else    if (oi->im.data_type == IS_RGB16_PICTURE)
    {
        zmax = (float)(oi->im.pixel[oi->im.nys].in[3 * (oi->im.nxs)]
                              + oi->im.pixel[oi->im.nys].in[(3 * oi->im.nxs) + 1]
                              + oi->im.pixel[oi->im.nys].in[(3 * oi->im.nxs) + 2]);

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            in = oi->im.pixel[i].in;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(in[3 * j] + in[(3 * j) + 1] + in[(3 * j) + 2]);

                if (t > zmax)
                {
                    zmax = t;
                    x_max = j;
                    y_max = i;
                }
            }
        }

	for(i = 0; i < 5; i++) zmx[i] = zmy[i] = 0;
	for (i = y_max - 2, ky = 0 ; i < y_max + 2 ; i++, ky++)
	  {
	    ii = (i < 0) ? 0 : i;
	    ii = (i < oi->im.ny) ? ii : oi->im.ny - 1;
	    rgb16 = oi->im.pixel[i].rgb16;
	    for (j = x_max - 2, kx = 0; j < x_max + 2 ; j++, kx++)
	      {
		jj = (j < 0) ? 0 : j;
		jj = (j < oi->im.nx) ? jj : oi->im.nx - 1;
		t = (float)(rgb16[jj].b + rgb16[jj].g + rgb16[jj].r);
		if (ky > 0 && ky < 4) zmx[kx] += t;
		if (kx > 0 && kx < 4) zmy[ky] += t;
            }
        }
	find_max_around(zmx, 5, 2, x_fmax, &maxvx, NULL);
	*x_fmax += x_max - 2;
	find_max_around(zmy, 5, 2, y_fmax, &maxvy, NULL);
	*y_fmax += y_max - 2;
	if (max_val) *max_val = (maxvx + maxvy)/8;
    }
    else    if (oi->im.data_type == IS_RGBA_PICTURE)
    {
        zmax = (float)(oi->im.pixel[oi->im.nys].rgba16[oi->im.nxs].b
                              + oi->im.pixel[oi->im.nys].rgba16[oi->im.nxs].g
                              + oi->im.pixel[oi->im.nys].rgba16[oi->im.nxs].r);

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            rgba16 = oi->im.pixel[i].rgba16;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(rgba16[j].b + rgba16[j].g + rgba16[j].r);

                if (t > zmax)
                {
                    zmax = t;
                    x_max = j;
                    y_max = i;
                }
            }
        }

	for(i = 0; i < 5; i++) zmx[i] = zmy[i] = 0;
	for (i = y_max - 2, ky = 0 ; i < y_max + 2 ; i++, ky++)
	  {
	    ii = (i < 0) ? 0 : i;
	    ii = (i < oi->im.ny) ? ii : oi->im.ny - 1;
	    rgba16 = oi->im.pixel[i].rgba16;
	    for (j = x_max - 2, kx = 0; j < x_max + 2 ; j++, kx++)
	      {
		jj = (j < 0) ? 0 : j;
		jj = (j < oi->im.nx) ? jj : oi->im.nx - 1;
		t = (float)(rgba16[jj].b + rgba16[jj].g + rgba16[jj].r);
		if (ky > 0 && ky < 4) zmx[kx] += t;
		if (kx > 0 && kx < 4) zmy[ky] += t;
            }
        }
	find_max_around(zmx, 5, 2, x_fmax, &maxvx, NULL);
	*x_fmax += x_max - 2;
	find_max_around(zmy, 5, 2, y_fmax, &maxvy, NULL);
	*y_fmax += y_max - 2;
	if (max_val) *max_val = (maxvx + maxvy)/8;
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
        zmax = oi->im.pixel[oi->im.nys].in[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            in = oi->im.pixel[i].in;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(in[j]);

                if (t > zmax)
                {
                    zmax = t;
                    x_max = j;
                    y_max = i;
                }
            }
        }
	for(i = 0; i < 5; i++) zmx[i] = zmy[i] = 0;
	for (i = y_max - 2, ky = 0 ; i < y_max + 2 ; i++, ky++)
	  {
	    ii = (i < 0) ? 0 : i;
	    ii = (i < oi->im.ny) ? ii : oi->im.ny - 1;
	    in = oi->im.pixel[i].in;
	    for (j = x_max - 2, kx = 0; j < x_max + 2 ; j++, kx++)
	      {
		jj = (j < 0) ? 0 : j;
		jj = (j < oi->im.nx) ? jj : oi->im.nx - 1;
		if (ky > 0 && ky < 4) zmx[kx] += in[jj];
		if (kx > 0 && kx < 4) zmy[ky] += in[jj];
            }
        }
	find_max_around(zmx, 5, 2, x_fmax, &maxvx, NULL);
	*x_fmax += x_max - 2;
	find_max_around(zmy, 5, 2, y_fmax, &maxvy, NULL);
	*y_fmax += y_max - 2;
	if (max_val) *max_val = (maxvx + maxvy)/8;
    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {
        zmax = oi->im.pixel[oi->im.nys].ui[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            ui = oi->im.pixel[i].ui;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(ui[j]);

                if (t > zmax)
                {
                    zmax = t;
                    x_max = j;
                    y_max = i;
                }
            }
        }
	for(i = 0; i < 5; i++) zmx[i] = zmy[i] = 0;
	for (i = y_max - 2, ky = 0 ; i < y_max + 2 ; i++, ky++)
	  {
	    ii = (i < 0) ? 0 : i;
	    ii = (i < oi->im.ny) ? ii : oi->im.ny - 1;
	    ui = oi->im.pixel[i].ui;
	    for (j = x_max - 2, kx = 0; j < x_max + 2 ; j++, kx++)
	      {
		jj = (j < 0) ? 0 : j;
		jj = (j < oi->im.nx) ? jj : oi->im.nx - 1;
		if (ky > 0 && ky < 4) zmx[kx] += ui[jj];
		if (kx > 0 && kx < 4) zmy[ky] += ui[jj];
            }
        }
	find_max_around(zmx, 5, 2, x_fmax, &maxvx, NULL);
	*x_fmax += x_max - 2;
	find_max_around(zmy, 5, 2, y_fmax, &maxvy, NULL);
	*y_fmax += y_max - 2;
	if (max_val) *max_val = (maxvx + maxvy)/8;
    }
    else if (oi->im.data_type == IS_LINT_IMAGE)
    {
        zmax = oi->im.pixel[oi->im.nys].li[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            li = oi->im.pixel[i].li;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = (float)(li[j]);

                if (t > zmax)
                {
                    zmax = t;
                    x_max = j;
                    y_max = i;
                }
            }
        }
	for(i = 0; i < 5; i++) zmx[i] = zmy[i] = 0;
	for (i = y_max - 2, ky = 0 ; i < y_max + 2 ; i++, ky++)
	  {
	    ii = (i < 0) ? 0 : i;
	    ii = (i < oi->im.ny) ? ii : oi->im.ny - 1;
	    li = oi->im.pixel[i].li;
	    for (j = x_max - 2, kx = 0; j < x_max + 2 ; j++, kx++)
	      {
		jj = (j < 0) ? 0 : j;
		jj = (j < oi->im.nx) ? jj : oi->im.nx - 1;
		if (ky > 0 && ky < 4) zmx[kx] += li[jj];
		if (kx > 0 && kx < 4) zmy[ky] += li[jj];
            }
        }
	find_max_around(zmx, 5, 2, x_fmax, &maxvx, NULL);
	*x_fmax += x_max - 2;
	find_max_around(zmy, 5, 2, y_fmax, &maxvy, NULL);
	*y_fmax += y_max - 2;
	if (max_val) *max_val = (maxvx + maxvy)/8;
    }
    else if (oi->im.data_type == IS_FLOAT_IMAGE)
    {
        zmax = oi->im.pixel[oi->im.nys].fl[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            fl = oi->im.pixel[i].fl;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = fl[j];

                if (t > zmax)
                {
                    zmax = t;
                    x_max = j;
                    y_max = i;
                }
            }
        }
	for(i = 0; i < 5; i++) zmx[i] = zmy[i] = 0;
	for (i = y_max - 2, ky = 0 ; i < y_max + 2 ; i++, ky++)
	  {
	    ii = (i < 0) ? 0 : i;
	    ii = (i < oi->im.ny) ? ii : oi->im.ny - 1;
	    fl = oi->im.pixel[i].fl;
	    for (j = x_max - 2, kx = 0; j < x_max + 2 ; j++, kx++)
	      {
		jj = (j < 0) ? 0 : j;
		jj = (j < oi->im.nx) ? jj : oi->im.nx - 1;
		if (ky > 0 && ky < 4) zmx[kx] += fl[jj];
		if (kx > 0 && kx < 4) zmy[ky] += fl[jj];
            }
        }
	find_max_around(zmx, 5, 2, x_fmax, &maxvx, NULL);
	*x_fmax += x_max - 2;
	find_max_around(zmy, 5, 2, y_fmax, &maxvy, NULL);
	*y_fmax += y_max - 2;
	if (max_val) *max_val = (maxvx + maxvy)/8;
    }
    else if (oi->im.data_type == IS_DOUBLE_IMAGE)
    {
        zmax = oi->im.pixel[oi->im.nys].db[oi->im.nxs];

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            db = oi->im.pixel[i].db;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                t = db[j];

                if (t > zmax)
                {
                    zmax = t;
                    x_max = j;
                    y_max = i;
                }
            }
        }
	for(i = 0; i < 5; i++) zmx[i] = zmy[i] = 0;
	for (i = y_max - 2, ky = 0 ; i < y_max + 2 ; i++, ky++)
	  {
	    ii = (i < 0) ? 0 : i;
	    ii = (i < oi->im.ny) ? ii : oi->im.ny - 1;
	    db = oi->im.pixel[i].db;
	    for (j = x_max - 2, kx = 0; j < x_max + 2 ; j++, kx++)
	      {
		jj = (j < 0) ? 0 : j;
		jj = (j < oi->im.nx) ? jj : oi->im.nx - 1;
		if (ky > 0 && ky < 4) zmx[kx] += (float)db[jj];
		if (kx > 0 && kx < 4) zmy[ky] += (float)db[jj];
            }
        }
	find_max_around(zmx, 5, 2, x_fmax, &maxvx, NULL);
	*x_fmax += x_max - 2;
	find_max_around(zmy, 5, 2, y_fmax, &maxvy, NULL);
	*y_fmax += y_max - 2;
	if (max_val) *max_val = (maxvx + maxvy)/8;
    }
    else if (oi->im.data_type == IS_COMPLEX_IMAGE)
    {
        zr = oi->im.pixel[oi->im.nys].fl[2 * oi->im.nxs];
        zi = oi->im.pixel[oi->im.nys].fl[2 * oi->im.nxs + 1];

        if (mode == RE)             zmax = zr;
        else if (mode == IM)        zmax = zi;
        else if (mode == AMP)       zmax = sqrt(zr * zr + zi * zi);
        else if (mode == AMP_2)     zmax = zr * zr + zi * zi;
        else if (mode == LOG_AMP)   zmax = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
        else return (win_printf("Unknown mode for complex image"));

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            fl = oi->im.pixel[i].fl;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                zr = fl[2 * j];
                zi = fl[2 * j + 1];

                if (mode == RE)     t = zr;
                else if (mode == IM)    t = zi;
                else if (mode == AMP)   t = sqrt(zr * zr + zi * zi);
                else if (mode == AMP_2) t = zr * zr + zi * zi;
                else if (mode == LOG_AMP)   t = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
                else return 1;

                if (t > zmax)
                {
                    zmax = t;
                    x_max = j;
                    y_max = i;
                }
            }
        }
	for(i = 0; i < 5; i++) zmx[i] = zmy[i] = 0;
	for (i = y_max - 2, ky = 0 ; i < y_max + 2 ; i++, ky++)
	  {
	    ii = (i < 0) ? 0 : i;
	    ii = (i < oi->im.ny) ? ii : oi->im.ny - 1;
	    fl = oi->im.pixel[i].fl;
	    for (j = x_max - 2, kx = 0; j < x_max + 2 ; j++, kx++)
	      {
		jj = (j < 0) ? 0 : j;
		jj = (j < oi->im.nx) ? jj : oi->im.nx - 1;
                zr = fl[2 * jj];
                zi = fl[2 * jj + 1];

                if (mode == RE)     t = zr;
                else if (mode == IM)    t = zi;
                else if (mode == AMP)   t = sqrt(zr * zr + zi * zi);
                else if (mode == AMP_2) t = zr * zr + zi * zi;
                else if (mode == LOG_AMP)   t = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
                else return 1;
		if (ky > 0 && ky < 4) zmx[kx] += t;
		if (kx > 0 && kx < 4) zmy[ky] += t;
            }
        }
	find_max_around(zmx, 5, 2, x_fmax, &maxvx, NULL);
	*x_fmax += x_max - 2;
	find_max_around(zmy, 5, 2, y_fmax, &maxvy, NULL);
	*y_fmax += y_max - 2;
	if (max_val) *max_val = (maxvx + maxvy)/8;
    }
    else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        zr = oi->im.pixel[oi->im.nys].db[2 * oi->im.nxs];
        zi = oi->im.pixel[oi->im.nys].db[2 * oi->im.nxs + 1];

        if (mode == RE)             zmax = zr;
        else if (mode == IM)        zmax = zi;
        else if (mode == AMP)       zmax = sqrt(zr * zr + zi * zi);
        else if (mode == AMP_2)     zmax = zr * zr + zi * zi;
        else if (mode == LOG_AMP)   zmax = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
        else return (win_printf("Unknown mode for complex image"));

        for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
            db = oi->im.pixel[i].db;

            for (j = oi->im.nxs ; j < oi->im.nxe ; j++)
            {
                zr = db[2 * j];
                zi = db[2 * j + 1];

                if (mode == RE)     t = zr;
                else if (mode == IM)    t = zi;
                else if (mode == AMP)   t = sqrt(zr * zr + zi * zi);
                else if (mode == AMP_2) t = zr * zr + zi * zi;
                else if (mode == LOG_AMP)   t = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
                else return 1;

                if (t > zmax)
                {
                    zmax = t;
                    x_max = j;
                    y_max = i;
                }
            }
        }
	for(i = 0; i < 5; i++) zmx[i] = zmy[i] = 0;
	for (i = y_max - 2, ky = 0 ; i < y_max + 2 ; i++, ky++)
	  {
	    ii = (i < 0) ? 0 : i;
	    ii = (i < oi->im.ny) ? ii : oi->im.ny - 1;
	    db = oi->im.pixel[i].db;
	    for (j = x_max - 2, kx = 0; j < x_max + 2 ; j++, kx++)
	      {
		jj = (j < 0) ? 0 : j;
		jj = (j < oi->im.nx) ? jj : oi->im.nx - 1;
                zr = db[2 * jj];
                zi = db[2 * jj + 1];

                if (mode == RE)     t = zr;
                else if (mode == IM)    t = zi;
                else if (mode == AMP)   t = sqrt(zr * zr + zi * zi);
                else if (mode == AMP_2) t = zr * zr + zi * zi;
                else if (mode == LOG_AMP)   t = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
                else return 1;
		if (ky > 0 && ky < 4) zmx[kx] += t;
		if (kx > 0 && kx < 4) zmy[ky] += t;
            }
        }
	find_max_around(zmx, 5, 2, x_fmax, &maxvx, NULL);
	*x_fmax += x_max - 2;
	find_max_around(zmy, 5, 2, y_fmax, &maxvy, NULL);
	*y_fmax += y_max - 2;
	if (max_val) *max_val = (maxvx + maxvy)/8;
    }
    else return 1;
    oi->need_to_refresh |= BITMAP_NEED_REFRESH;
    return 0;
}

/*  This routine find the zmax excluding a region arround a peak
    the idea is to check how much the maximum found at the peak differs from background
 */
int find_zmax_excluding_an_area(O_i *oi, int xc, int dx, int yc, int dy, float *max_val, float *z2acg)
{
  int i, j, k, nz2;
  int mode, *li = NULL;
  unsigned char *ch = NULL;
  rgba_t *rgba = NULL;
  rgba16_t *rgba16 = NULL;
  rgb_t *rgb = NULL;
  //rgb16_t *rgb16 = NULL;
  short int *in = NULL;
  unsigned short int *ui = NULL;
  float *fl = NULL, zmax = 0, t, zr, zi, z2;
  double *db = NULL;

  if (oi == NULL)     return xvin_error(Wrong_Argument);

  mode = oi->im.mode;
  k = 0;
  z2 = 0;
  nz2 = 0;
  if (oi->im.data_type == IS_CHAR_IMAGE)
    {
      for (i = 0 ; i < oi->im.ny ; i++)
        {
	  if (abs(i - yc) < dy) continue;
	  ch = oi->im.pixel[i].ch;
	  for (j = 0 ; j < oi->im.nx ; j++)
            {
	      if (abs(j - xc) < dx) continue;
	      t = (float)(ch[j]);
	      z2 += t * t;
	      nz2++;
	      if (k == 0 || t > zmax)
                {
		  zmax = t;
		  k++;
                }
            }
        }
    }
  else    if (oi->im.data_type == IS_RGB_PICTURE)
    {
      for (i = 0 ; i < oi->im.ny ; i++)
        {
	  if (abs(i - yc) < dy) continue;
	  rgb = oi->im.pixel[i].rgb;
	  for (j = 0 ; j < oi->im.nx ; j++)
            {
	      if (abs(j - xc) < dx) continue;
	      t = (float)(rgb[j].r + rgb[j].g + rgb[j].b);
	      z2 += t * t;
	      nz2++;
	      if (k == 0 || t > zmax)
                {
		  zmax = t;
		  k++;
                }
            }
        }
    }
  else    if (oi->im.data_type == IS_RGBA_PICTURE)
    {
      for (i = 0 ; i < oi->im.ny ; i++)
        {
	  if (abs(i - yc) < dy) continue;
	  rgba = oi->im.pixel[i].rgba;
	  for (j = 0 ; j < oi->im.nx ; j++)
            {
	      if (abs(j - xc) < dx) continue;
	      t = (float)(rgba[j].b + rgba[j].g + rgba[j].r);
	      z2 += t * t;
	      nz2++;
	      if (k == 0 || t > zmax)
                {
		  zmax = t;
		  k++;
                }
            }
        }
    }
  else    if (oi->im.data_type == IS_RGB16_PICTURE)
    {
      for (i = 0 ; i < oi->im.ny ; i++)
        {
	  if (abs(i - yc) < dy) continue;
	  in = oi->im.pixel[i].in;
	  for (j = 0 ; j < oi->im.nx ; j++)
            {
	      if (abs(j - xc) < dx) continue;
	      t = (float)(in[3 * j] + in[(3 * j) + 1] + in[(3 * j) + 2]);
	      z2 += t * t;
	      nz2++;
	      if (k == 0 || t > zmax)
                {
		  zmax = t;
		  k++;
                }
            }
        }
    }
  else    if (oi->im.data_type == IS_RGBA_PICTURE)
    {
      for (i = 0 ; i < oi->im.ny ; i++)
        {
	  if (abs(i - yc) < dy) continue;
	  rgba16 = oi->im.pixel[i].rgba16;
	  for (j = 0 ; j < oi->im.nx ; j++)
            {
	      if (abs(j - xc) < dx) continue;
	      t = (float)(rgba16[j].b + rgba16[j].g + rgba16[j].r);
	      z2 += t * t;
	      nz2++;
	      if (k == 0 || t > zmax)
                {
		  zmax = t;
		  k++;
                }
            }
        }

    }
  else if (oi->im.data_type == IS_INT_IMAGE)
    {
      for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
	  if (abs(i - yc) < dy) continue;
	  in = oi->im.pixel[i].in;
	  for (j = 0 ; j < oi->im.nx ; j++)
            {
	      if (abs(j - xc) < dx) continue;
	      t = (float)(in[j]);
	      z2 += t * t;
	      nz2++;
	      if (k == 0 || t > zmax)
                {
		  zmax = t;
		  k++;
                }
            }
        }
    }
  else if (oi->im.data_type == IS_UINT_IMAGE)
    {
      for (i = oi->im.nys ; i < oi->im.nye ; i++)
        {
	  if (abs(i - yc) < dy) continue;
	  ui = oi->im.pixel[i].ui;
	  for (j = 0 ; j < oi->im.nx ; j++)
            {
	      if (abs(j - xc) < dx) continue;
	      t = (float)(ui[j]);
	      z2 += t * t;
	      nz2++;
	      if (k == 0 || t > zmax)
                {
		  zmax = t;
		  k++;
                }
            }
        }
    }
  else if (oi->im.data_type == IS_LINT_IMAGE)
    {
      for (i = 0 ; i < oi->im.ny ; i++)
        {
	  if (abs(i - yc) < dy) continue;
	  li = oi->im.pixel[i].li;
	  for (j = 0 ; j < oi->im.nx ; j++)
            {
	      if (abs(j - xc) < dx) continue;
	      t = (float)(li[j]);
	      z2 += t * t;
	      nz2++;
	      if (k == 0 || t > zmax)
                {
		  zmax = t;
		  k++;
                }
            }
        }
    }
  else if (oi->im.data_type == IS_FLOAT_IMAGE)
    {
      for (i = 0 ; i < oi->im.ny ; i++)
        {
	  if (abs(i - yc) < dy) continue;
	  fl = oi->im.pixel[i].fl;
	  for (j = 0 ; j < oi->im.nx ; j++)
            {
	      if (abs(j - xc) < dx) continue;
	      t = fl[j];
	      z2 += t * t;
	      nz2++;
	      if (k == 0 || t > zmax)
                {
		  zmax = t;
		  k++;
                }
            }
        }
    }
  else if (oi->im.data_type == IS_DOUBLE_IMAGE)
    {
      for (i = 0 ; i < oi->im.ny ; i++)
        {
	  if (abs(i - yc) < dy) continue;
	  db = oi->im.pixel[i].db;
	  for (j = 0 ; j < oi->im.nx ; j++)
            {
	      if (abs(j - xc) < dx) continue;
	      t = db[j];
	      z2 += t * t;
	      nz2++;
	      if (k == 0 || t > zmax)
                {
		  zmax = t;
		  k++;
                }
            }
        }
    }
  else if (oi->im.data_type == IS_COMPLEX_IMAGE)
    {
      for (i = 0 ; i < oi->im.ny ; i++)
        {
	  if (abs(i - yc) < dy) continue;
	  fl = oi->im.pixel[i].fl;
	  for (j = 0 ; j < oi->im.nx ; j++)
            {
	      if (abs(j - xc) < dx) continue;
	      zr = fl[2 * j];
	      zi = fl[2 * j + 1];
	      if (mode == RE)     t = zr;
	      else if (mode == IM)    t = zi;
	      else if (mode == AMP)   t = sqrt(zr * zr + zi * zi);
	      else if (mode == AMP_2) t = zr * zr + zi * zi;
	      else if (mode == LOG_AMP)   t = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
	      else return 1;
	      z2 += t * t;
	      nz2++;
	      if (k == 0 || t > zmax)
                {
		  zmax = t;
		  k++;
                }
            }
        }
    }
  else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
      for (i = 0 ; i < oi->im.ny ; i++)
        {
	  if (abs(i - yc) < dy) continue;
	  db = oi->im.pixel[i].db;
	  for (j = 0 ; j < oi->im.nx ; j++)
            {
	      if (abs(j - xc) < dx) continue;
	      zr = db[2 * j];
	      zi = db[2 * j + 1];

	      if (mode == RE)     t = zr;
	      else if (mode == IM)    t = zi;
	      else if (mode == AMP)   t = sqrt(zr * zr + zi * zi);
	      else if (mode == AMP_2) t = zr * zr + zi * zi;
	      else if (mode == LOG_AMP)   t = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
	      else return 1;
	      z2 += t * t;
	      nz2++;
	      if (k == 0 || t > zmax)
		{
		  zmax = t;
		  k++;
                }
            }
        }
    }
  else return 1;
  z2 = (nz2 > 0) ? z2/nz2 : z2;
  if (max_val) *max_val = zmax;
  if (z2acg) *z2acg = z2;
  oi->need_to_refresh |= BITMAP_NEED_REFRESH;
  return 0;
}


//# ifdef ENCOURS


int alloc_im_ext_array(im_ext_ar *iea, int size)
{
    if (iea == NULL || size <= 0) return -1;

    if (iea->ie == NULL)
    {
        iea->ie = (im_ext *)calloc(size, sizeof(struct im_max));
        iea->n_ie =   iea->c_ie = 0;
    }
    else iea->ie = (im_ext *)realloc(iea->ie, size * sizeof(struct im_max));

    if (iea->ie == NULL) return -1;

    iea->m_ie = size;
    return size;
}
int find_in_im_extremum(im_ext_ar *iea, int x, int y, int tolerence)
{
    int i;

    if (iea == NULL || iea->ie == NULL) return -1;

    if (iea->n_ie == 0) return 0;

    for (i = iea->n_ie - 1; i >= 0; i--)
    {
        // we found a maximum already existing
        if ((x > iea->ie[i].x0 - tolerence) && (x < iea->ie[i].x1 + tolerence) &&
                (y > iea->ie[i].y0 - tolerence) && (y < iea->ie[i].y1 + tolerence))
            return i;
    }

    // no maximum known
    return iea->n_ie;
}
int free_im_ext_array(im_ext_ar *iea)
{
    if (iea == NULL) return -1;

    free(iea->ie);
    free(iea);
    return 0;
}
int add_pt_to_im_extremum(im_ext_ar *iea, int i_ie, int x, int y, double data)
{
    im_ext *ie = NULL;
    //  static int verbose = D_O_K;

    if (iea == NULL || iea->ie == NULL || i_ie < 0) return -1;

    //if (verbose != WIN_CANCEL)
    //  verbose = win_printf("adding pixel to maximun i_ie %d n_ie %d\n x %d y %d, val %g\n"
    //             "Pressing WIN_CANCEL will supress further message\n",i_ie,iea->n_ie,x,y,data);
    if (i_ie > (int) iea->n_ie) return -1;

    if (iea->n_ie >= iea->m_ie)
    {
        iea->m_ie = alloc_im_ext_array(iea, 2 * iea->m_ie);

        if (iea->m_ie <= 0) return -2;
    }

    ie = iea->ie + i_ie;

    if (ie->np == 0)
    {
        ie->zmax = data;
        ie->x0 = ie->x1 = ie->xm = x;
        ie->y0 = ie->y1 = ie->ym = y;
    }

    if (data > ie->zmax)
    {
        ie->zmax = data;
        ie->xm = x;
        ie->ym = y;
    }

    ie->weight += data;
    ie->xpos += data * x;
    ie->ypos += data * y;
    ie->np++;
    ie->x0 = (x < ie->x0) ? x : ie->x0;
    ie->x1 = (x < ie->x1) ? ie->x1 : x;
    ie->y0 = (y < ie->y0) ? y : ie->y0;
    ie->y1 = (y < ie->y1) ? ie->y1 : y;

    if (i_ie == (int) iea->n_ie) iea->n_ie++;

    return i_ie;
}

int finalize_im_extremum(im_ext_ar *iea)
{
    int i;
    im_ext *ie = NULL;

    if (iea == NULL || iea->ie == NULL) return -1;

    for (i = iea->n_ie - 1; i >= 0; i--)
    {
        // we found a maximum already existing
        ie = iea->ie + i;

        if (ie->weight > 0)
        {
            ie->xpos /= ie->weight;
            ie->ypos /= ie->weight;
        }
    }

    return 0;
}
im_ext_ar *find_maxima_position(O_i *oi, double threshold,  int tolerence, int max_number, int useROI)
{
    int i, j, k;
    int mode, *li = NULL;
    unsigned char *ch = NULL;
    rgba_t *rgba = NULL;
    rgba16_t *rgba16 = NULL;
    short int *in = NULL;
    unsigned short int *ui = NULL;
    float *fl = NULL, zr, zi;
    double *db = NULL, t;
    int  nmax, nys, nye, nxs, nxe, np = 0;
    im_ext_ar *iea = NULL;
    //  static int verbose = D_O_K;


    if (oi == NULL)
    {
        xvin_error(Wrong_Argument);
        return NULL;
    }

    nmax = (max_number > 0) ? max_number : 16;
    iea = (im_ext_ar *)calloc(1, sizeof(im_ext_ar));

    if (iea == NULL)  return NULL;

    if (alloc_im_ext_array(iea, nmax) < 0) return NULL;

    mode = oi->im.mode;
    nxs = (useROI) ? oi->im.nxs : 0;
    nys = (useROI) ? oi->im.nys : 0;
    nxe = (useROI) ? oi->im.nxe : oi->im.nx;
    nye = (useROI) ? oi->im.nye : oi->im.ny;

    if (oi->im.data_type == IS_CHAR_IMAGE)
    {
        for (i = nys ; i < nye ; i++)
        {
            ch = oi->im.pixel[i].ch;

            for (j = nxs ; j < nxe ; j++)
            {
                t = (double)(ch[j]);

                if (t > threshold)
                {
                    k = find_in_im_extremum(iea, j, i, tolerence);

                    if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);

                    np++;
                }
            }
        }
    }
    else  if (oi->im.data_type == IS_RGB_PICTURE)
    {
        for (i = nys ; i < nye ; i++)
        {
            ch = oi->im.pixel[i].ch;

            for (j = nxs ; j < nxe ; j++)
            {
                t = (double)(ch[3 * j]) + (double)(ch[(3 * j) + 1]) + (double)(ch[(3 * j) + 2]);

                if (t > threshold)
                {
                    k = find_in_im_extremum(iea, j, i, tolerence);

                    if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);

                    np++;
                }
            }
        }
    }
    else  if (oi->im.data_type == IS_RGBA_PICTURE)
    {
        for (i = nys ; i < nye ; i++)
        {
            rgba = oi->im.pixel[i].rgba;

            for (j = nxs ; j < nxe ; j++)
            {
                t = (double)(rgba[j].b) + (double)(rgba[j].g) + (double)(rgba[j].r);

                if (t > threshold)
                {
                    k = find_in_im_extremum(iea, j, i, tolerence);

                    if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);

                    np++;
                }
            }
        }
    }
    else  if (oi->im.data_type == IS_RGB16_PICTURE)
    {
        for (i = nys ; i < nye ; i++)
        {
            in = oi->im.pixel[i].in;

            for (j = nxs ; j < nxe ; j++)
            {
                t = (double)(in[3 * j]) + (double)(in[(3 * j) + 1]) + (double)(in[(3 * j) + 2]);

                if (t > threshold)
                {
                    k = find_in_im_extremum(iea, j, i, tolerence);

                    if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);

                    np++;
                }
            }
        }
    }
    else  if (oi->im.data_type == IS_RGBA_PICTURE)
    {
        for (i = nys ; i < nye ; i++)
        {
            rgba16 = oi->im.pixel[i].rgba16;

            for (j = nxs ; j < nxe ; j++)
            {
                t = (double)(rgba16[j].b) + (double)(rgba16[j].g) + (double)(rgba16[j].r);

                if (t > threshold)
                {
                    k = find_in_im_extremum(iea, j, i, tolerence);

                    if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);

                    np++;
                }
            }
        }
    }
    else if (oi->im.data_type == IS_INT_IMAGE)
    {
        for (i = nys ; i < nye ; i++)
        {
            in = oi->im.pixel[i].in;

            for (j = nxs ; j < nxe ; j++)
            {
                t = (double)(in[j]);

                if (t > threshold)
                {
                    k = find_in_im_extremum(iea, j, i, tolerence);

                    if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);

                    np++;
                }
            }
        }
    }
    else if (oi->im.data_type == IS_UINT_IMAGE)
    {
        for (i = nys ; i < nye ; i++)
        {
            ui = oi->im.pixel[i].ui;

            for (j = nxs ; j < nxe ; j++)
            {
                t = (double)(ui[j]);

                if (t > threshold)
                {
                    k = find_in_im_extremum(iea, j, i, tolerence);

                    if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);

                    np++;
                }
            }
        }
    }
    else if (oi->im.data_type == IS_LINT_IMAGE)
    {
        for (i = nys ; i < nye ; i++)
        {
            li = oi->im.pixel[i].li;

            for (j = nxs ; j < nxe ; j++)
            {
                t = (double)(li[j]);

                if (t > threshold)
                {
                    k = find_in_im_extremum(iea, j, i, tolerence);

                    if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);

                    np++;
                }
            }
        }
    }
    else if (oi->im.data_type == IS_FLOAT_IMAGE)
    {
        for (i = nys ; i < nye ; i++)
        {
            fl = oi->im.pixel[i].fl;

            for (j = nxs ; j < nxe ; j++)
            {
                t = (double)(fl[j]);

                if (t > threshold)
                {
                    k = find_in_im_extremum(iea, j, i, tolerence);

                    // if (verbose != WIN_CANCEL)
                    //  verbose = win_printf("point %d,%d val %g",j,i,t);
                    if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);

                    np++;
                }
            }
        }
    }
    else if (oi->im.data_type == IS_DOUBLE_IMAGE)
    {
        for (i = nys ; i < nye ; i++)
        {
            db = oi->im.pixel[i].db;

            for (j = nxs ; j < nxe ; j++)
            {
                t = db[j];

                if (t > threshold)
                {
                    k = find_in_im_extremum(iea, j, i, tolerence);

                    if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);

                    np++;
                }
            }
        }
    }
    else if (oi->im.data_type == IS_COMPLEX_IMAGE)
    {
        zr = oi->im.pixel[oi->im.nys].fl[2 * oi->im.nxs];
        zi = oi->im.pixel[oi->im.nys].fl[2 * oi->im.nxs + 1];

        for (i = nys ; i < nye ; i++)
        {
            fl = oi->im.pixel[i].fl;

            for (j = nxs ; j < nxe ; j++)
            {
                zr = fl[2 * j];
                zi = fl[2 * j + 1];

                if (mode == RE)       t = (double)zr;
                else if (mode == IM)  t = (double)zi;
                else if (mode == AMP) t = sqrt(zr * zr + zi * zi);
                else if (mode == AMP_2)   t = (double)(zr * zr + zi * zi);
                else if (mode == LOG_AMP) t = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
                else return NULL;

                if (t > threshold)
                {
                    k = find_in_im_extremum(iea, j, i, tolerence);

                    if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);

                    np++;
                }
            }
        }
    }
    else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        zr = oi->im.pixel[oi->im.nys].db[2 * oi->im.nxs];
        zi = oi->im.pixel[oi->im.nys].db[2 * oi->im.nxs + 1];

        for (i = nys ; i < nye ; i++)
        {
            db = oi->im.pixel[i].db;

            for (j = nxs ; j < nxe ; j++)
            {
                zr = db[2 * j];
                zi = db[2 * j + 1];

                if (mode == RE)       t = zr;
                else if (mode == IM)  t = zi;
                else if (mode == AMP) t = sqrt(zr * zr + zi * zi);
                else if (mode == AMP_2)   t = zr * zr + zi * zi;
                else if (mode == LOG_AMP) t = (zr * zr + zi * zi > 0) ? log10(zr * zr + zi * zi) : -40.0;
                else return NULL;

                if (t > threshold)
                {
                    k = find_in_im_extremum(iea, j, i, tolerence);

                    if (k >= 0) add_pt_to_im_extremum(iea, k, j, i, t);

                    np++;
                }

            }
        }
    }
    else return NULL;

    finalize_im_extremum(iea);
    my_set_window_title("%d pixels in %d maxima", np, iea->n_ie);
    oi->need_to_refresh |= BITMAP_NEED_REFRESH;
    return iea;
}
//# endif

int decribe_image(O_i *oi, char *des, int size_des)
{
  char mv[64] = {0}, s[128] = {0};

    if (oi == NULL) return 1;

    if (oi->im.n_f > 1)
        snprintf(mv, 64, "movie %d/%d", oi->im.c_f, oi->im.n_f);
    else if (oi->im.n_f < 0)
        snprintf(mv, 64, "disk movie %d/%d", oi->im.c_f, -oi->im.n_f);
    else  snprintf(mv, 64, "Image");

    if (oi->im.data_type == IS_CHAR_IMAGE)
        snprintf(s, 128, "%s of Uchar", mv);
    else if (oi->im.data_type == IS_INT_IMAGE)
        snprintf(s, 128, "%s of int16", mv);
    else if (oi->im.data_type == IS_UINT_IMAGE)
        snprintf(s, 128, "%s of Uint16", mv);
    else if (oi->im.data_type == IS_LINT_IMAGE)
        snprintf(s, 128, "%s of int32", mv);
    else if (oi->im.data_type == IS_FLOAT_IMAGE)
        snprintf(s, 128, "%s of float", mv);
    else if (oi->im.data_type == IS_DOUBLE_IMAGE)
        snprintf(s, 128, "%s of double", mv);
    else if (oi->im.data_type == IS_RGB_PICTURE)
        snprintf(s, 128, "%s type RGB", mv);
    else if (oi->im.data_type == IS_RGBA_PICTURE)
        snprintf(s, 128, "%s type RGBA", mv);
    else if (oi->im.data_type == IS_RGB16_PICTURE)
        snprintf(s, 128, "%s type RGB 16 bits", mv);
    else if (oi->im.data_type == IS_RGBA16_PICTURE)
        snprintf(s, 128, "%s type RGBA 16 bits", mv);
    else if (oi->im.data_type == IS_COMPLEX_IMAGE)
    {
        if (oi->im.mode == LOG_AMP)
            snprintf(s, 128, "%s Z = log[A.A]", mv);
        else if (oi->im.mode == AMP)
            snprintf(s, 128, "%s Z = A", mv);
        else if (oi->im.mode == AMP_2)
            snprintf(s, 128, "%s Z = A.A", mv);
        else if (oi->im.mode == RE)
            snprintf(s, 128, "%s Z = Re", mv);
        else if (oi->im.mode == IM)
            snprintf(s, 128, "%s Z = Im", mv);
        else  snprintf(s, 128, "%s Z = Unknown !", mv);
    }
    else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        if (oi->im.mode == LOG_AMP)
            snprintf(s, 128, "%s ZD = log[A.A]", mv);
        else if (oi->im.mode == AMP)
            snprintf(s, 128, "%s ZD = A", mv);
        else if (oi->im.mode == AMP_2)
            snprintf(s, 128, "%s ZD = A.A", mv);
        else if (oi->im.mode == RE)
            snprintf(s, 128, "%s ZD = Re", mv);
        else if (oi->im.mode == IM)
            snprintf(s, 128, "%s ZD = Im", mv);
        else  snprintf(s, 128, "%s ZD = Unknown !", mv);
    }

    snprintf(des, size_des, "%s %dx%d", s, oi->im.nx, oi->im.ny);
    return 0;
}
// save image data to an open file stream
int save_one_image_to_fp(O_i *oi, FILE *fp)
{
    int i;
    int nw, nf = 1;
    union pix *px = NULL;
    time_t timer;
    struct plot_label *p_p_l = NULL;
    un_s *un = NULL;

    if (oi == NULL || fp == NULL)       return 1; //xvin_error(Wrong_Argument);

    qkylabl_set((oi->iopt2 & Y_NUM));
    qkxlabl_set((oi->iopt2 & X_NUM));

    if (image_use_2_special != NULL) image_use_2_special(oi);

    /*  setvbuf(fp, NULL, _IOFBF, MAXBUFSIZE);  */

    fprintf(fp, "%% image data \n");

    if (oi->width != 1) fprintf(fp, "-w %g\r\n", oi->width);

    if (oi->height != 1)    fprintf(fp, "-h %g\r\n", oi->height);

    fprintf(fp, "-tk %g\r\n", oi->tick_len);

    if (oi->iopt & NOAXES)  fprintf(fp, "-g 0\r\n");

    if (oi->x_prefix != NULL && oi->x_unit != NULL)
        fprintf(fp, "-px \"%s\" \"%s\"\r\n", oi->x_prefix, oi->x_unit);
    else    if (oi->x_prefix == NULL && oi->x_unit != NULL)
        fprintf(fp, "-px ! \"%s\"\r\n", oi->x_unit);
    else    if (oi->x_prefix != NULL && oi->x_unit == NULL)
        fprintf(fp, "-px \"%s\" !\r\n", oi->x_prefix);

    if (oi->y_prefix != NULL && oi->y_unit != NULL)
        fprintf(fp, "-py \"%s\" \"%s\"\r\n", oi->y_prefix, oi->y_unit);
    else    if (oi->y_prefix == NULL && oi->y_unit != NULL)
        fprintf(fp, "-py ! \"%s\"\r\n", oi->y_unit);
    else    if (oi->y_prefix != NULL && oi->y_unit == NULL)
        fprintf(fp, "-py \"%s\" !\r\n", oi->y_prefix);

    if (oi->x_title != NULL)    fprintf(fp, "-lx \"%s\"\r\n", oi->x_title);

    if (oi->y_title != NULL)    fprintf(fp, "-ly \"%s\"\r\n", oi->y_title);

    if (oi->x_hi > oi->x_lo) fprintf(fp, "-x%c %g %g\r\n", (oi->iopt2 & X_NUM) ? 'n' : ' ', oi->x_lo, oi->x_hi);

    if (oi->y_hi > oi->y_lo) fprintf(fp, "-y%c %g %g\r\n", (oi->iopt2 & Y_NUM) ? 'n' : ' ', oi->y_lo, oi->y_hi);

    if (oi->dx != 1 || oi->ax != 0) fprintf(fp, "-ax %g %g\r\n", oi->dx, oi->ax);

    if (oi->dy != 1 || oi->ay != 0) fprintf(fp, "-ay %g %g\r\n", oi->dy, oi->ay);

    if (oi->dz != 1 || oi->az != 0) fprintf(fp, "-az %g %g\r\n", oi->dz, oi->az);

    if (oi->xu != NULL)
    {
        for (i = 0 ; i < oi->n_xu ; i++)
        {
            un = oi->xu[i];

            if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
                fprintf(fp, "-xus %g %g \"%s\" %d %d %d\r\n", un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
            else if (un != NULL && un->type != IS_RAW_U)
                fprintf(fp, "-xus %g %g \"%s\" %d %d %d\r\n", un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
        }
    }

    if (oi->yu != NULL)
    {
        for (i = 0 ; i < oi->n_yu ; i++)
        {
            un = oi->yu[i];

            if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
                fprintf(fp, "-yus %g %g \"%s\" %d %d %d\r\n", un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
            else if (un != NULL && un->type != IS_RAW_U)
                fprintf(fp, "-yus %g %g \"%s\" %d %d %d\r\n", un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
        }
    }

    if (oi->zu != NULL)
    {
        for (i = 0 ; i < oi->n_zu ; i++)
        {
            un = oi->zu[i];

            if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
                fprintf(fp, "-zus %g %g \"%s\" %d %d %d\r\n", un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
            else if (un != NULL && un->type != IS_RAW_U)
                fprintf(fp, "-zus %g %g \"%s\" %d %d %d\r\n", un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
        }
    }

    if (oi->tu != NULL)
    {
        for (i = 0 ; i < oi->n_tu ; i++)
        {
            un = oi->tu[i];

            if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
                fprintf(fp, "-tus %g %g \"%s\" %d %d %d\r\n", un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
            else if (un != NULL && un->type != IS_RAW_U)
                fprintf(fp, "-tus %g %g \"%s\" %d %d %d\r\n", un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
        }
    }


    fprintf(fp, "-z %g %g\r\n", oi->z_black, oi->z_white);
    fprintf(fp, "-nx %d %d %d\r\n", oi->im.nx, oi->im.nxs, oi->im.nxe);
    fprintf(fp, "-ny %d %d %d\r\n", oi->im.ny, oi->im.nys, oi->im.nye);

    if (oi->im.win_flag & X_PER)    fprintf(fp, "x-periodic\n");

    if (oi->im.win_flag & Y_PER)    fprintf(fp, "y-periodic\n");

    if (oi->im.n_f > 1 || oi->im.movie_on_disk)
    {
        fprintf(fp, "-nf %010d %010d\r\n", oi->im.n_f, oi->im.c_f);
        nf = oi->im.n_f;
    }

    if (oi->y_prime_title != NULL)  fprintf(fp, "-lyp \"%s\"\r\n", oi->y_prime_title);

    if (oi->x_prime_title != NULL)  fprintf(fp, "-lxp \"%s\"\r\n", oi->x_prime_title);

    if (oi->title != NULL)      fprintf(fp, "-lt \"%s\"\r\n", oi->title);

    for (i = 0 ; i < oi->n_lab ; i++)
    {
        p_p_l = oi->lab[i];

        if (p_p_l->type == USR_COORD)
            fprintf(fp, "%g %g \"%s\"\r\n", p_p_l->xla, p_p_l->yla, p_p_l->text);
        else if (p_p_l->type == USR_COORD + WHITE_LABEL)
            fprintf(fp, "-luw %g %g \"%s\"\r\n", p_p_l->xla, p_p_l->yla, p_p_l->text);
        else if (p_p_l->type == VERT_LABEL_USR + WHITE_LABEL)
            fprintf(fp, "-lvw %g %g \"%s\"\r\n", p_p_l->xla, p_p_l->yla, p_p_l->text);
        else if (p_p_l->type == VERT_LABEL_USR)
            fprintf(fp, "-lv %g %g \"%s\"\r\n", p_p_l->xla, p_p_l->yla, p_p_l->text);
        else
            fprintf(fp, "-lr %g %g \"%s\"\r\n", p_p_l->xla, p_p_l->yla, p_p_l->text);
    }

    if (oi->im.time == 0)   time(&timer);
    else            timer = oi->im.time;

    fprintf(fp, "-time %lu %% %s\r\n", timer, ctime(&timer));

    if (oi->im.record_duration != 0)
        fprintf(fp, "-duration %016.6f \r\n", oi->im.record_duration);

    if (oi->im.special != NULL)
    {
        for (i = 0 ; i < oi->im.n_special ; i++)
        {
            if (oi->im.special[i] != NULL)
                fprintf(fp, "-special \"%s\"\r\n", oi->im.special[i]);
        }
    }

    if (oi->im.source != NULL)  fprintf(fp, "-src \"%s\"\r\n", oi->im.source);

    if (oi->im.history != NULL) fprintf(fp, "-his \"%s\"\r\n", oi->im.history);

    if (oi->im.treatement != NULL)  fprintf(fp, "-treat \"%s\"\r\n", oi->im.treatement);

    if (oi->im.data_type == IS_CHAR_IMAGE)      fprintf(fp, "-imcz \r\n%c", CRT_Z);
    else if (oi->im.data_type == IS_INT_IMAGE)  fprintf(fp, "-imiz \r\n%c", CRT_Z);
    else if (oi->im.data_type == IS_UINT_IMAGE) fprintf(fp, "-imuiz \r\n%c", CRT_Z);
    else if (oi->im.data_type == IS_LINT_IMAGE) fprintf(fp, "-imliz \r\n%c", CRT_Z);
    else if (oi->im.data_type == IS_RGB_PICTURE)    fprintf(fp, "-imrgbz \r\n%c", CRT_Z);
    else if (oi->im.data_type == IS_RGBA_PICTURE)   fprintf(fp, "-imrgbaz \r\n%c", CRT_Z);
    else if (oi->im.data_type == IS_RGB16_PICTURE)  fprintf(fp, "-imrgb16z \r\n%c", CRT_Z);
    else if (oi->im.data_type == IS_RGBA16_PICTURE) fprintf(fp, "-imrgba16z \r\n%c", CRT_Z);
    else if (oi->im.data_type == IS_FLOAT_IMAGE)    fprintf(fp, "-imfz \r\n%c", CRT_Z);
    else if (oi->im.data_type == IS_DOUBLE_IMAGE)   fprintf(fp, "-imdbz \r\n%c", CRT_Z);
    else if (oi->im.data_type == IS_COMPLEX_IMAGE)  fprintf(fp, "-imzz \r\n%c", CRT_Z);
    else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)   fprintf(fp, "-imzdbz \r\n%c", CRT_Z);
    else return (win_printf("Unknown image type"));

    px = (oi->im.n_f > 1) ? oi->im.pxl[0] : oi->im.pixel;
    int ii, im, k;
    for (im = 0 ; im < nf ; im++)
      {
	ii = (oi->im.save_using_this_frame_as_first)
	  ? im + oi->im.save_using_this_frame_as_first : im;
	ii = (ii < nf) ? ii : ii - nf;
	for (k = 0 ; k < oi->im.ny; k++)
	  {
	    i = (ii * oi->im.ny) + k;
	    if (oi->im.data_type == IS_CHAR_IMAGE)
	      nw = fwrite(px[i].ch, sizeof(char), oi->im.nx, fp);
	    else if (oi->im.data_type == IS_RGB_PICTURE)
	      nw = fwrite(px[i].rgb, sizeof(rgb_t), oi->im.nx, fp);
	    else if (oi->im.data_type == IS_RGBA_PICTURE)
	      nw = fwrite(px[i].rgba, sizeof(rgba_t), oi->im.nx, fp);
	    else if (oi->im.data_type == IS_RGB16_PICTURE)
	      nw = fwrite(px[i].rgb16, sizeof(rgb16_t), oi->im.nx, fp);
	    else if (oi->im.data_type == IS_RGBA16_PICTURE)
	      nw = fwrite(px[i].rgba16, sizeof(rgba16_t), oi->im.nx, fp);
	    else if (oi->im.data_type == IS_INT_IMAGE)
	      nw = fwrite(px[i].in, sizeof(short int), oi->im.nx, fp);
	    else if (oi->im.data_type == IS_UINT_IMAGE)
	      nw = fwrite(px[i].ui, sizeof(unsigned short int), oi->im.nx, fp);
	    else if (oi->im.data_type == IS_LINT_IMAGE)
	      nw = fwrite(px[i].li, sizeof(int), oi->im.nx, fp);
	    else if (oi->im.data_type == IS_FLOAT_IMAGE)
	      nw = fwrite(px[i].fl, sizeof(float), oi->im.nx, fp);
	    else if (oi->im.data_type == IS_COMPLEX_IMAGE)
	      nw = fwrite(px[i].cp, sizeof(mcomplex), oi->im.nx, fp);
	    else if (oi->im.data_type == IS_DOUBLE_IMAGE)
	      nw = fwrite(px[i].db, sizeof(double), oi->im.nx, fp);
	    else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
	      nw = fwrite(px[i].dcp, sizeof(mdcomplex), oi->im.nx, fp);
	    else    return 3;
	    if (nw != oi->im.nx)    return 3;
	  }
      }
    return 0;
}
/*  int save_one_image (O_i *oi, char *file);
 *  DESCRIPTION
 *
 *  RETURNS     0 on success, 1
 */
int save_one_image(O_i *oi, char *file)
{
    FILE *fp = NULL;

    if (oi == NULL || file == NULL)       return xvin_error(Wrong_Argument);

    qkylabl_set((oi->iopt2 & Y_NUM));
    qkxlabl_set((oi->iopt2 & X_NUM));
    fp = fopen(file, "wb");

    if (fp == NULL)  return (win_printf("Cannot open %s!", file));

    save_one_image_to_fp(oi, fp);
    fclose(fp);
    return 0;
}


int inherit_from_im_to_im(O_i *dest, O_i *src)
{
  char *s = NULL, c[2] = {0};
    int i;

    if (dest == NULL || src == NULL)    return xvin_error(Wrong_Argument);

    dest->im.source = Mystrdupre(dest->im.source, src->im.source);

    if (dest->im.history != NULL)
    {
        free(dest->im.history);
        dest->im.history = NULL;
    }

    i = 0;

    if (src->im.treatement != NULL) i = strlen(src->im.treatement);

    if (src->im.history != NULL)    i += strlen(src->im.history);

    i += 2;
    s = (char *)calloc(i, sizeof(char));

    if (s == NULL)  return xvin_error(Out_Of_Memory);

    if (src->im.treatement != NULL && src->im.history != NULL)
        snprintf(s, i, "%s\n%s", src->im.history, src->im.treatement);
    else if (src->im.treatement != NULL)
        snprintf(s, i, "%s", src->im.treatement);
    else if (src->im.history != NULL)
        snprintf(s, i, "%s", src->im.history);
    else    s[0] = 0;

    if (s[0] != 0) dest->im.history = s;
    else    free(s);

    remove_from_one_image(dest, ALL_SPECIAL, (void *)c);

    for (i = 0; i < src->im.n_special; i++)
        add_to_one_image(dest, IS_SPECIAL, (void *)(src->im.special[i]));

    if (image_duplicate_use != NULL)
        image_duplicate_use(dest, src);

    dest->filename = Transfer_filename(src->filename);
    dest->dir = Mystrdup(src->dir);
    dest->iopt2 = src->iopt2;
    dest->width = src->width;
    dest->height = src->height;
    return 0;
}
int inherit_from_im_to_ds(d_s *dest, O_i *src)
{
  char *s = NULL, c[2] = {0};
    int i;

    if (dest == NULL || src == NULL)    return xvin_error(Wrong_Argument);

    dest->source = Mystrdupre(dest->source, src->im.source);

    if (dest->history != NULL)
    {
        free(dest->history);
        dest->history = NULL;
    }

    i = 0;

    if (src->im.treatement != NULL) i = strlen(src->im.treatement);

    if (src->im.history != NULL)    i += strlen(src->im.history);

    i += 2;
    s = (char *)calloc(i, sizeof(char));

    if (s == NULL)  return xvin_error(Out_Of_Memory);

    if (src->im.treatement != NULL && src->im.history != NULL)
        snprintf(s, i, "%s\n%s", src->im.history, src->im.treatement);
    else if (src->im.treatement != NULL)
        snprintf(s, i, "%s", src->im.treatement);
    else if (src->im.history != NULL)
        snprintf(s, i, "%s", src->im.history);
    else    s[0] = 0;

    if (s[0] != 0) dest->history = s;
    else    free(s);

    remove_from_data_set(dest, ALL_SPECIAL, (void *)c);

    for (i = 0; i < src->im.n_special; i++)
        add_to_data_set(dest, IS_SPECIAL, (void *)(src->im.special[i]));

    if (ds_special_2_use != NULL)   ds_special_2_use(dest);

    return 0;
}
int inherit_from_ds_to_im(O_i *dst, d_s *src)
{
    char *s = NULL;
    int i;

    if (dst == NULL || src == NULL) return xvin_error(Wrong_Argument);

    dst->im.source = Mystrdupre(dst->im.source, src->source);

    if (dst->im.history != NULL)
    {
        free(dst->im.history);
        dst->im.history = NULL;
    }

    i = 0;

    if (src->treatement != NULL)    i = strlen(src->treatement);

    if (src->history != NULL)   i += strlen(src->history);

    i += 2;
    s = (char *)calloc(i, sizeof(char));

    if (s == NULL)  return xvin_error(Out_Of_Memory);

    if (src->treatement != NULL && src->history != NULL)
        snprintf(s, i, "%s\n%s", src->history, src->treatement);
    else if (src->treatement != NULL)
        snprintf(s, i, "%s", src->treatement);
    else if (src->history != NULL)
        snprintf(s, i, "%s", src->history);
    else    s[0] = 0;

    if (s[0] != 0) dst->im.history = s;
    else    free(s);

    if (dst->im.special != NULL)
        remove_from_one_image(dst, ALL_SPECIAL, dst->im.special);

    if (src->special != NULL)
        for (i = 0 ; i < src->n_special ; i++)
            add_to_one_image(dst, IS_SPECIAL, src->special[i]);

    if (image_use_2_special != NULL) image_use_2_special(dst);

    return 0;
}
/*
 * write a new treatment in oi and transfert the old one to history
 */

int update_oi_treatment(O_i *oi, char *format, ...)
{
    char *s = NULL;
    int i;
    va_list ap;

    if (oi == NULL || format == NULL)   return xvin_error(Wrong_Argument);

    i = 0;

    if (oi->im.treatement != NULL)  i = strlen(oi->im.treatement);

    if (oi->im.history != NULL) i += strlen(oi->im.history);

    i += 2;
    s = (char *)calloc(i, sizeof(char));

    if (s == NULL)  return xvin_error(Out_Of_Memory);

    if (oi->im.treatement != NULL && oi->im.history != NULL)
        snprintf(s, i, "%s\n%s", oi->im.history, oi->im.treatement);
    else if (oi->im.treatement != NULL)
        snprintf(s, i, "%s", oi->im.treatement);
    else if (oi->im.history != NULL)
        snprintf(s, i, "%s", oi->im.history);
    else    s[0] = 0;

    if (s[0] != 0)
    {
        if (oi->im.history != NULL) free(oi->im.history);

        oi->im.history = s;
    }
    else    free(s);

    va_start(ap, format);
    vset_formated_string(&(oi->im.treatement), format, ap);
    va_end(ap);
    return 0;
}



/*
 *  DESCRIPTION interpolate an unsigned char image value at position x and y
 *              in pixel value in a fast manner, less accurate, if the
 *              point is outside of the image return out_color * 256
 *              otherwise it output le value * 256
 */
int fast_interpolate_image_point(O_i *ois, float x, float y, int out_color)
{
    int onx, ony, ix, iy, ix1, iy1;
    int ax, ay, tmp;
    unsigned char outx = 0, outy = 0;
    unsigned char ly00, ly01, ly10, ly11;
    union pix *ps = NULL;

    if (ois == NULL || ois->im.pixel == NULL)   return xvin_error(Wrong_Argument);

    if (ois->im.data_type != IS_CHAR_IMAGE)
        return win_printf("I can treat only unsigned char images!");

    onx = ois->im.nx;
    ony = ois->im.ny;
    ps = ois->im.pixel;
    ix = (int)x;
    iy = (int)y;

    if (out_color >= 0)
      {
	if (ix < 0 || ix >= onx || iy < 0 || iy >= ony) return (out_color << 8);
      }
    else
      {
	if (ix < 0)
	  {
	    outx = 1;
	    ix = 0;
	  }
	if (ix >= onx)
	  {
	    outx = 1;
	    ix = onx-1;
	  }
	if (iy < 0)
	  {
	    outy = 1;
	    iy = 0;
	  }
	if (iy >= ony)
	  {
	    outy = 1;
	    iy = ony-1;
	  }

      }
    ax = (outx) ? 8 : 16 - (int)(16 * x) + (ix << 4);
    ay = (outy) ? 8 : 16 - (int)(16 * y) + (iy << 4);
    ix1 = ix + 1;
    iy1 = iy + 1;
    ix1 = (ix1 < onx) ? ix1 : onx - 1;
    iy1 = (iy1 < ony) ? iy1 : ony - 1;
    ly00 = ps[iy].ch[ix];
    ly01 = ps[iy].ch[ix1];
    ly10 = ps[iy1].ch[ix];
    ly11 = ps[iy1].ch[ix1];
    tmp = ay * (ly00 * ax + ly01 * (16 - ax));
    tmp += (16 - ay) * (ly10 * ax + ly11 * (16 - ax));
    return tmp;
}
/*
 *  DESCRIPTION interpolate an image value at position x and y
 *              in pixel value
 *
 */
float   interpolate_image_point(O_i *ois, float x, float y, float *zr, float *zi)
{
    int onx, ony, ix, iy, ix1, iy1;
    float ax, ay, z00 = 0, z01 = 0, z10 = 0, z11 = 0, tmp;
    float zi00 = 0, zi01 = 0, zi10 = 0, zi11 = 0;
    union pix *ps = NULL;

    if (ois == NULL || ois->im.pixel == NULL)   return xvin_error(Wrong_Argument);

    onx = ois->im.nx;
    ony = ois->im.ny;
    ps = ois->im.pixel;
    ix = (int)x;
    iy = (int)y;

    if (ix < 0 || ix >= onx || iy < 0 || iy >= ony)
    {
        if (zr != NULL) *zr = 0;

        if (zi != NULL) *zi = 0;

        return 0;
    }

    ax = x - ix;
    ay = y - iy;
    ix = (ix < 0) ? 0 : ix;
    ix = (ix < onx) ? ix : onx - 1;
    iy = (iy < 0) ? 0 : iy;
    iy = (iy < ony) ? iy : ony - 1;
    ix1 = ix + 1;
    iy1 = iy + 1;
    ix1 = (ix1 < onx) ? ix1 : onx - 1;
    iy1 = (iy1 < ony) ? iy1 : ony - 1;

    if (ois->im.data_type == IS_CHAR_IMAGE)
    {
        z00 = (float)ps[iy].ch[ix];
        z01 = (float)ps[iy].ch[ix1];
        z10 = (float)ps[iy1].ch[ix];
        z11 = (float)ps[iy1].ch[ix1];
    }
    else if (ois->im.data_type == IS_INT_IMAGE)
    {
        z00 = (float)ps[iy].in[ix];
        z01 = (float)ps[iy].in[ix1];
        z10 = (float)ps[iy1].in[ix];
        z11 = (float)ps[iy1].in[ix1];
    }
    else if (ois->im.data_type == IS_UINT_IMAGE)
    {
        z00 = (float)ps[iy].ui[ix];
        z01 = (float)ps[iy].ui[ix1];
        z10 = (float)ps[iy1].ui[ix];
        z11 = (float)ps[iy1].ui[ix1];
    }
    else if (ois->im.data_type == IS_LINT_IMAGE)
    {
        z00 = (float)ps[iy].li[ix];
        z01 = (float)ps[iy].li[ix1];
        z10 = (float)ps[iy1].li[ix];
        z11 = (float)ps[iy1].li[ix1];
    }
    else if (ois->im.data_type == IS_FLOAT_IMAGE)
    {
        z00 = ps[iy].fl[ix];
        z01 = ps[iy].fl[ix1];
        z10 = ps[iy1].fl[ix];
        z11 = ps[iy1].fl[ix1];
    }
    else if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        z00 = ps[iy].fl[2 * ix];
        z01 = ps[iy].fl[2 * ix1];
        z10 = ps[iy1].fl[2 * ix];
        z11 = ps[iy1].fl[2 * ix1];
        zi00 = ps[iy].fl[2 * ix + 1];
        zi01 = ps[iy].fl[2 * ix1 + 1];
        zi10 = ps[iy1].fl[2 * ix + 1];
        zi11 = ps[iy1].fl[2 * ix1 + 1];
    }
    else if (ois->im.data_type == IS_DOUBLE_IMAGE)
    {
        z00 = ps[iy].db[ix];
        z01 = ps[iy].db[ix1];
        z10 = ps[iy1].db[ix];
        z11 = ps[iy1].db[ix1];
    }
    else if (ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        z00 = ps[iy].db[2 * ix];
        z01 = ps[iy].db[2 * ix1];
        z10 = ps[iy1].db[2 * ix];
        z11 = ps[iy1].db[2 * ix1];
        zi00 = ps[iy].db[2 * ix + 1];
        zi01 = ps[iy].db[2 * ix1 + 1];
        zi10 = ps[iy1].db[2 * ix + 1];
        zi11 = ps[iy1].db[2 * ix1 + 1];
    }

    ax = 1 - ax;
    ay = 1 - ay;
    tmp = ay * (z00 * ax + z01 * (1 - ax)) + (1 - ay) * (z10 * ax + z11 * (1 - ax));

    if (zr != NULL) *zr = tmp;

    if (zi != NULL)
        *zi = ay * (zi00 * ax + zi01 * (1 - ax)) + (1 - ay) * (zi10 * ax + zi11 * (1 - ax));

    return tmp;
}

/*
 *  DESCRIPTION interpolate an image value at position x and y
 *              in pixel value
 *
 */
int interpolate_RGB_picture_point(O_i *ois, float x, float y, float *zr, float *zg, float *zb, float *za)
{
    int onx, ony, ix, iy, ix1, iy1;
    float ax, ay, ax_1, ay_1;
    float zr00 = 0, zr01 = 0, zr10 = 0, zr11 = 0;
    float zg00 = 0, zg01 = 0, zg10 = 0, zg11 = 0;
    float zb00 = 0, zb01 = 0, zb10 = 0, zb11 = 0;
    float za00 = 0, za01 = 0, za10 = 0, za11 = 0;
    union pix *ps;

    if (ois == NULL || ois->im.pixel == NULL)   return xvin_error(Wrong_Argument);

    onx = ois->im.nx;
    ony = ois->im.ny;
    ps = ois->im.pixel;
    ix = (int)(x);
    iy = (int)(y);

    if (ix < 0 || ix >= onx || iy < 0 || iy >= ony)
    {
        if (zr != NULL) *zr = 0;

        if (zg != NULL) *zg = 0;

        if (zb != NULL) *zb = 0;

        if (za != NULL) *za = 0;

        return 0;
    }

    ax = x - ix;
    ay = y - iy;
    ix = (ix < 0) ? 0 : ix;
    ix = (ix < onx) ? ix : onx - 1;
    iy = (iy < 0) ? 0 : iy;
    iy = (iy < ony) ? iy : ony - 1;
    ix1 = ix + 1;
    iy1 = iy + 1;
    ix1 = (ix1 < onx) ? ix1 : onx - 1;
    iy1 = (iy1 < ony) ? iy1 : ony - 1;

    if (ois->im.data_type == IS_RGB_PICTURE)
    {
        zr00 = (float)ps[iy].rgb[ix].r;
        zr01 = (float)ps[iy].rgb[ix1].r;
        zr10 = (float)ps[iy1].rgb[ix].r;
        zr11 = (float)ps[iy1].rgb[ix1].r;
        zg00 = (float)ps[iy].rgb[ix].g;
        zg01 = (float)ps[iy].rgb[ix1].g;
        zg10 = (float)ps[iy1].rgb[ix].g;
        zg11 = (float)ps[iy1].rgb[ix1].g;
        zb00 = (float)ps[iy].rgb[ix].b;
        zb01 = (float)ps[iy].rgb[ix1].b;
        zb10 = (float)ps[iy1].rgb[ix].b;
        zb11 = (float)ps[iy1].rgb[ix1].b;
        za00 = (float)255;
        za01 = (float)255;
        za10 = (float)255;
        za11 = (float)255;
    }
    else if (ois->im.data_type == IS_RGBA_PICTURE)
    {
        zr00 = (float)ps[iy].rgba[ix].r;
        zr01 = (float)ps[iy].rgba[ix1].r;
        zr10 = (float)ps[iy1].rgba[ix].r;
        zr11 = (float)ps[iy1].rgba[ix1].r;
        zg00 = (float)ps[iy].rgba[ix].g;
        zg01 = (float)ps[iy].rgba[ix1].g;
        zg10 = (float)ps[iy1].rgba[ix].g;
        zg11 = (float)ps[iy1].rgba[ix1].g;
        zb00 = (float)ps[iy].rgba[ix].b;
        zb01 = (float)ps[iy].rgba[ix1].b;
        zb10 = (float)ps[iy1].rgba[ix].b;
        zb11 = (float)ps[iy1].rgba[ix1].b;
        za00 = (float)ps[iy].rgba[ix].a;
        za01 = (float)ps[iy].rgba[ix1].a;
        za10 = (float)ps[iy1].rgba[ix].a;
        za11 = (float)ps[iy1].rgba[ix1].a;
    }
    else if (ois->im.data_type == IS_RGB16_PICTURE)
    {
        zr00 = (float)ps[iy].rgb16[ix].r;
        zr01 = (float)ps[iy].rgb16[ix1].r;
        zr10 = (float)ps[iy1].rgb16[ix].r;
        zr11 = (float)ps[iy1].rgb16[ix1].r;
        zg00 = (float)ps[iy].rgb16[ix].g;
        zg01 = (float)ps[iy].rgb16[ix1].g;
        zg10 = (float)ps[iy1].rgb16[ix].g;
        zg11 = (float)ps[iy1].rgb16[ix1].g;
        zb00 = (float)ps[iy].rgb16[ix].b;
        zb01 = (float)ps[iy].rgb16[ix1].b;
        zb10 = (float)ps[iy1].rgb16[ix].b;
        zb11 = (float)ps[iy1].rgb16[ix1].b;
        za00 = (float)32767;
        za01 = (float)32767;
        za10 = (float)32767;
        za11 = (float)32767;
    }
    else if (ois->im.data_type == IS_RGBA16_PICTURE)
    {
        zr00 = (float)ps[iy].rgba16[ix].r;
        zr01 = (float)ps[iy].rgba16[ix1].r;
        zr10 = (float)ps[iy1].rgba16[ix].r;
        zr11 = (float)ps[iy1].rgba16[ix1].r;
        zg00 = (float)ps[iy].rgba16[ix].g;
        zg01 = (float)ps[iy].rgba16[ix1].g;
        zg10 = (float)ps[iy1].rgba16[ix].g;
        zg11 = (float)ps[iy1].rgba16[ix1].g;
        zb00 = (float)ps[iy].rgba16[ix].b;
        zb01 = (float)ps[iy].rgba16[ix1].b;
        zb10 = (float)ps[iy1].rgba16[ix].b;
        zb11 = (float)ps[iy1].rgba16[ix1].b;
        za00 = (float)ps[iy].rgba16[ix].a;
        za01 = (float)ps[iy].rgba16[ix1].a;
        za10 = (float)ps[iy1].rgba16[ix].a;
        za11 = (float)ps[iy1].rgba16[ix1].a;
    }
    else return 1;

    ax_1 = ax;
    ax = (float)1 - ax;
    ay_1 = ay;
    ay = (float)1 - ay;

    if (zr != NULL) *zr = ay * (zr00 * ax + zr01 * ax_1) + ay_1 * (zr10 * ax + zr11 * ax_1);

    if (zg != NULL) *zg = ay * (zg00 * ax + zg01 * ax_1) + ay_1 * (zg10 * ax + zg11 * ax_1);

    if (zb != NULL) *zb = ay * (zb00 * ax + zb01 * ax_1) + ay_1 * (zb10 * ax + zb11 * ax_1);

    if (za != NULL) *za = ay * (za00 * ax + za01 * ax_1) + ay_1 * (za10 * ax + za11 * ax_1);

    return 0;
}

int     set_oi_x_unit_set(O_i *oi, int n)
{
    int j, k;
    un_s *lxu = NULL;

    if (oi == NULL) return xvin_error(Wrong_Argument);

    n &= 0xff;

    if (n >= oi->n_xu)  n = 0;

    if (n < 0)  n = 0;

    if (oi->n_xu == 0 || oi->xu == NULL)    return 1;

    oi->c_xu = n;
    lxu = oi->xu[n];
    oi->ax = lxu->ax;
    oi->dx = lxu->dx;
    oi->x_unit = Mystrdupre(oi->x_unit, lxu->name);

    for (j = 0; j < oi->n_op; j++)
    {
        for (k = 0; k < oi->o_p[j]->n_xu && oi->cur_op >= 0; k++)
        {
            if (compare_units(lxu, oi->o_p[j]->xu[k]) == 0)
            {
                change_decade(oi->o_p[j]->xu[k], lxu->decade);
                set_op_x_unit_set(oi->o_p[j], k);
            }
        }
    }

    oi->need_to_refresh |= PLOT_NEED_REFRESH;
    return (0);
}
int     set_oi_y_unit_set(O_i *oi, int n)
{
    int j, k;
    un_s *lyu = NULL;

    if (oi == NULL) return xvin_error(Wrong_Argument);

    n &= 0xff;

    if (n >= oi->n_yu)  n = 0;

    if (n < 0)  n = 0;

    if (oi->n_yu == 0 || oi->yu == NULL)    return 1;

    oi->c_yu = n;
    lyu = oi->yu[n];
    oi->ay = lyu->ax;
    oi->dy = lyu->dx;
    oi->y_unit = Mystrdupre(oi->y_unit, lyu->name);

    for (j = 0; j < oi->n_op && oi->cur_op >= 0; j++)
    {
        for (k = 0; k < oi->o_p[j]->n_yu; k++)
        {
            if (compare_units(lyu, oi->o_p[j]->yu[k]) == 0)
            {
                change_decade(oi->o_p[j]->yu[k], lyu->decade);
                set_op_y_unit_set(oi->o_p[j], k);
            }
        }
    }

    oi->need_to_refresh |= PLOT_NEED_REFRESH;
    return (0);
}
int     set_oi_z_unit_set(O_i *oi, int n)
{
    int j, k;
    un_s *lzu = NULL;

    if (oi == NULL) return xvin_error(Wrong_Argument);

    if (n >= oi->n_zu)  n = 0;

    if (n < 0)  n = 0;

    if (oi->n_zu == 0 || oi->zu == NULL)    return 1;

    oi->c_zu = n;
    lzu = oi->zu[n];
    oi->z_black = (oi->z_black - oi->az);
    oi->z_white = (oi->z_white - oi->az);
    oi->z_min = (oi->z_min - oi->az);
    oi->z_max = (oi->z_max - oi->az);

    if (oi->dz != 0)
    {
        oi->z_black /= oi->dz;
        oi->z_white /= oi->dz;
        oi->z_min /= oi->dz;
        oi->z_max /= oi->dz;
    }

    oi->az = lzu->ax;
    oi->dz = lzu->dx;
    oi->z_black = oi->z_black * lzu->dx + lzu->ax;
    oi->z_white = oi->z_white * lzu->dx + lzu->ax;
    oi->z_min = oi->z_min * lzu->dx + lzu->ax;
    oi->z_max = oi->z_max * lzu->dx + lzu->ax;

    for (j = 0; j < oi->n_op && oi->cur_op >= 0; j++)
    {
        for (k = 0; k < oi->o_p[j]->n_xu; k++)
        {
            if (compare_units(lzu, oi->o_p[j]->xu[k]) == 0)
            {
                change_decade(oi->o_p[j]->xu[k], lzu->decade);
                set_op_x_unit_set(oi->o_p[j], k);
            }
        }

        for (k = 0; k < oi->o_p[j]->n_yu; k++)
        {
            if (compare_units(lzu, oi->o_p[j]->yu[k]) == 0)
            {
                change_decade(oi->o_p[j]->yu[k], lzu->decade);
                set_op_y_unit_set(oi->o_p[j], k);
            }
        }
    }

    oi->need_to_refresh |= PLOT_NEED_REFRESH;
    return 0;
}
int     set_oi_t_unit_set(O_i *oi, int n)
{
    int j, k;
    un_s *ltu = NULL;

    if (oi == NULL) return xvin_error(Wrong_Argument);

    if (n >= oi->n_tu)  n = 0;

    if (n < 0)  n = 0;

    if (oi->n_tu == 0 || oi->tu == NULL)    return 1;

    oi->c_tu = n;
    ltu = oi->tu[n];
    oi->at = ltu->ax;
    oi->dt = ltu->dx;
    oi->t_unit = Mystrdupre(oi->t_unit, ltu->name);


    for (j = 0; j < oi->n_op && oi->cur_op >= 0; j++)
    {
        for (k = 0; k < oi->o_p[j]->n_xu; k++)
        {
            if (compare_units(ltu, oi->o_p[j]->xu[k]) == 0)
            {
                change_decade(oi->o_p[j]->xu[k], ltu->decade);
                set_op_x_unit_set(oi->o_p[j], k);
            }
        }

        for (k = 0; k < oi->o_p[j]->n_yu; k++)
        {
            if (compare_units(ltu, oi->o_p[j]->yu[k]) == 0)
            {
                change_decade(oi->o_p[j]->yu[k], ltu->decade);
                set_op_y_unit_set(oi->o_p[j], k);
            }
        }
    }

    oi->need_to_refresh |= PLOT_NEED_REFRESH;
    return 0;
}


int uns_oi_2_op(O_i *ois, int oi_type, O_p *op, int op_type)
{
    int i = 0, j = 0;
    un_s *us = NULL;


    if (op == NULL || ois == NULL)  return xvin_error(Wrong_Argument);

    if (op_type ==  IS_X_UNIT_SET)
        while (op->n_xu > 0)
            remove_one_plot_data(op, IS_X_UNIT_SET, (void *)op->xu[op->n_xu - 1]);
    else if (op_type ==  IS_Y_UNIT_SET)
        while (op->n_yu > 0)
            remove_one_plot_data(op, IS_Y_UNIT_SET, (void *)op->yu[op->n_yu - 1]);
    else return xvin_error(Wrong_Argument);

    if (oi_type ==  IS_X_UNIT_SET && ois->xu != NULL && ois->n_xu > 0)
    {
        for (i = 0 ; i < ois->n_xu ; i++)
        {
            us = duplicate_unit_set(ois->xu[i], NULL);
            add_data_to_one_plot(op, op_type, (void *)us);
        }

        j = ois->c_xu;
    }
    else if (oi_type ==  IS_Y_UNIT_SET && ois->yu != NULL && ois->n_yu > 0)
    {
        for (i = 0 ; i < ois->n_yu ; i++)
        {
            us = duplicate_unit_set(ois->yu[i], NULL);
            add_data_to_one_plot(op, op_type, (void *)us);
        }

        j = ois->c_yu;
    }
    else if (oi_type ==  IS_Z_UNIT_SET && ois->zu != NULL && ois->n_zu > 0)
    {
        for (i = 0 ; i < ois->n_zu ; i++)
        {
            us = duplicate_unit_set(ois->zu[i], NULL);
            add_data_to_one_plot(op, op_type, (void *)us);
        }

        j = ois->c_zu;

    }
    else if (oi_type ==  IS_T_UNIT_SET && ois->tu != NULL && ois->n_tu > 0)
    {
        for (i = 0 ; i < ois->n_tu ; i++)
        {
            us = duplicate_unit_set(ois->tu[i], NULL);
            add_data_to_one_plot(op, op_type, (void *)us);
        }

        j = ois->c_zu;

    }
    else return -1;

    if (op_type ==  IS_X_UNIT_SET)
    {
        op->c_xu = j;
        set_op_x_unit_set(op, op->c_xu);
    }
    else
    {
        op->c_yu = j;
        set_op_y_unit_set(op, op->c_yu);
    }

    return i;
}
int uns_oi_2_oi(O_i *dest, O_i *src)
{
    int i;
    un_s *us = NULL;

    if (src == NULL || dest == NULL)    return xvin_error(Wrong_Argument);

    while (dest->n_xu > 0)
    {
        remove_from_one_image(dest, IS_X_UNIT_SET, (void *)dest->xu[dest->n_xu - 1]);
    };

    if (src->xu != NULL && src->n_xu > 0)
    {
        for (i = 0 ; i < src->n_xu ; i++)
        {
            us = duplicate_unit_set(src->xu[i], NULL);
            add_to_one_image(dest, IS_X_UNIT_SET, (void *)us);
        }
    }

    dest->c_xu = src->c_xu;

    while (dest->n_yu > 0)
    {
        remove_from_one_image(dest, IS_Y_UNIT_SET, (void *)dest->yu[dest->n_yu - 1]);
    };

    if (src->yu != NULL && src->n_yu > 0)
    {
        for (i = 0 ; i < src->n_yu ; i++)
        {
            us = duplicate_unit_set(src->yu[i], NULL);
            add_to_one_image(dest, IS_Y_UNIT_SET, (void *)us);
        }
    }

    dest->c_yu = src->c_yu;

    while (dest->n_zu > 0)
    {
        remove_from_one_image(dest, IS_Z_UNIT_SET, (void *)dest->zu[dest->n_zu - 1]);
    };

    if (src->zu != NULL && src->n_zu > 0)
    {
        for (i = 0 ; i < src->n_zu ; i++)
        {
            us = duplicate_unit_set(src->zu[i], NULL);
            add_to_one_image(dest, IS_Z_UNIT_SET, (void *)us);
        }
    }

    dest->c_zu = src->c_zu;

    while (dest->n_tu > 0)
    {
        remove_from_one_image(dest, IS_T_UNIT_SET, (void *)dest->tu[dest->n_tu - 1]);
    };

    if (src->tu != NULL && src->n_tu > 0)
    {
        for (i = 0 ; i < src->n_tu ; i++)
        {
            us = duplicate_unit_set(src->tu[i], NULL);
            add_to_one_image(dest, IS_T_UNIT_SET, (void *)us);
        }
    }

    dest->c_tu = src->c_tu;
    set_oi_x_unit_set(dest, dest->c_xu);
    set_oi_y_unit_set(dest, dest->c_yu);
    set_oi_z_unit_set(dest, dest->c_zu);
    set_oi_t_unit_set(dest, dest->c_tu);
    dest->need_to_refresh |= PLOT_NEED_REFRESH;
    return 0;
}
int uns_op_2_oi(O_p *op, int op_type, O_i *ois, int oi_type)
{
    int i = 0, j = 0;
    un_s *us = NULL;

    if (op == NULL || ois == NULL)  return xvin_error(Wrong_Argument);

    if (oi_type ==  IS_X_UNIT_SET)
        while (ois->n_xu > 0)
            remove_from_one_image(ois, IS_X_UNIT_SET, (void *)ois->xu[ois->n_xu - 1]);
    else if (oi_type ==  IS_Y_UNIT_SET)
        while (ois->n_yu > 0)
            remove_from_one_image(ois, IS_Y_UNIT_SET, (void *)ois->yu[ois->n_yu - 1]);
    else return xvin_error(Wrong_Argument);

    if (op_type ==  IS_X_UNIT_SET && op->xu != NULL && op->n_xu > 0)
    {
        for (i = 0 ; i < op->n_xu ; i++)
        {
            us = duplicate_unit_set(op->xu[i], NULL);
            add_to_one_image(ois, oi_type, (void *)us);
        }

        j = op->c_xu;
    }
    else if (op_type ==  IS_Y_UNIT_SET && op->yu != NULL && op->n_yu > 0)
    {
        for (i = 0 ; i < op->n_yu ; i++)
        {
            us = duplicate_unit_set(op->yu[i], NULL);
            add_to_one_image(ois, oi_type, (void *)us);
        }

        j = op->c_yu;
    }
    else return -1;

    if (op_type ==  IS_X_UNIT_SET)
    {
        ois->c_xu = j;
        set_oi_x_unit_set(ois, ois->c_xu);
    }
    else
    {
        ois->c_yu = j;
        set_oi_y_unit_set(ois, ois->c_yu);
    }

    ois->need_to_refresh |= PLOT_NEED_REFRESH;
    return i;
}

int uns_oi_2_oi_by_type(O_i *ois, int ois_type, O_i *oid, int oid_type)
{
    int i = 0, j = 0;
    un_s *us = NULL;

    if (ois == NULL || oid == NULL) return xvin_error(Wrong_Argument);

    if (oid_type ==  IS_X_UNIT_SET)
        while (oid->n_xu > 0)
            remove_from_one_image(oid, IS_X_UNIT_SET, (void *)oid->xu[oid->n_xu - 1]);
    else if (oid_type ==  IS_Y_UNIT_SET)
        while (oid->n_yu > 0)
            remove_from_one_image(oid, IS_Y_UNIT_SET, (void *)oid->yu[oid->n_yu - 1]);
    else if (oid_type ==  IS_Z_UNIT_SET)
        while (oid->n_zu > 0)
            remove_from_one_image(oid, IS_Z_UNIT_SET, (void *)oid->zu[oid->n_zu - 1]);
    else if (oid_type ==  IS_T_UNIT_SET)
        while (oid->n_tu > 0)
            remove_from_one_image(oid, IS_T_UNIT_SET, (void *)oid->tu[oid->n_tu - 1]);
    else return xvin_error(Wrong_Argument);

    oid->need_to_refresh |= PLOT_NEED_REFRESH;

    if (ois_type ==  IS_X_UNIT_SET && ois->xu != NULL && ois->n_xu > 0)
    {
        for (i = 0 ; i < ois->n_xu ; i++)
        {
            us = duplicate_unit_set(ois->xu[i], NULL);
            add_to_one_image(oid, oid_type, (void *)us);
        }

        j = ois->c_xu;
    }
    else if (ois_type ==  IS_Y_UNIT_SET && ois->yu != NULL && ois->n_yu > 0)
    {
        for (i = 0 ; i < ois->n_yu ; i++)
        {
            us = duplicate_unit_set(ois->yu[i], NULL);
            add_to_one_image(oid, oid_type, (void *)us);
        }

        j = ois->c_yu;
    }
    else if (ois_type ==  IS_Z_UNIT_SET && ois->zu != NULL && ois->n_zu > 0)
    {
        for (i = 0 ; i < ois->n_zu ; i++)
        {
            us = duplicate_unit_set(ois->zu[i], NULL);
            add_to_one_image(oid, oid_type, (void *)us);
        }

        j = ois->c_zu;
    }
    else if (ois_type ==  IS_T_UNIT_SET && ois->tu != NULL && ois->n_tu > 0)
    {
        for (i = 0 ; i < ois->n_tu ; i++)
        {
            us = duplicate_unit_set(ois->tu[i], NULL);
            add_to_one_image(oid, oid_type, (void *)us);
        }

        j = ois->c_tu;
    }
    else return xvin_error(Wrong_Argument);

    if (oid_type ==  IS_X_UNIT_SET)
    {
        oid->c_xu = j;
        set_oi_x_unit_set(oid, oid->c_xu);
    }
    else if (oid_type ==  IS_Y_UNIT_SET)
    {
        oid->c_yu = j;
        set_oi_y_unit_set(oid, oid->c_yu);
    }
    else if (oid_type ==  IS_Z_UNIT_SET)
    {
        oid->c_zu = j;
        set_oi_z_unit_set(oid, oid->c_zu);
    }
    else if (oid_type ==  IS_T_UNIT_SET)
    {
        oid->c_tu = j;
        set_oi_t_unit_set(oid, oid->c_tu);
    }
    else    return -1;

    return i;
}

int swap_image(O_i *oi)
{
    char *s = NULL;
    int tmpi, iopt;
    un_s **u = NULL;
    float tmp;

    if (oi == NULL)     return 1;

    s = oi->x_title;
    oi->x_title = oi->y_title;
    oi->y_title = s;
    s = oi->x_prime_title;
    oi->x_prime_title = oi->y_prime_title;
    oi->y_prime_title = s;
    s = oi->x_unit;
    oi->x_unit = oi->y_unit;
    oi->y_unit = s;
    s = oi->x_prefix;
    oi->x_prefix = oi->y_prefix;
    oi->y_prefix = s;
    tmp = oi->x_lo;
    oi->x_lo = oi->y_lo;
    oi->y_lo = tmp;
    tmp = oi->x_hi;
    oi->x_hi = oi->y_hi;
    oi->y_hi = tmp;
    tmp = oi->ax;
    oi->ax = oi->ay;
    oi->ay = tmp;
    tmp = oi->dx;
    oi->dx = oi->dy;
    oi->dy = tmp;
    tmpi = oi->im.nx;
    oi->im.nx = oi->im.ny;
    oi->im.ny = tmpi;
    tmpi = oi->im.nxs;
    oi->im.nxs = oi->im.nys;
    oi->im.nys = tmpi;
    tmpi = oi->im.nxe;
    oi->im.nxe = oi->im.nye;
    oi->im.nye = tmpi;
    iopt = oi->iopt & ~(XLOG + YLOG);

    if (oi->iopt & XLOG)    iopt |= YLOG;

    if (oi->iopt & YLOG)    iopt |= XLOG;

    oi->iopt = iopt;
    iopt = oi->iopt2 & ~(X_LIM | Y_LIM | X_NUM | Y_NUM);

    if (oi->iopt2 & X_LIM)  iopt |= Y_LIM;

    if (oi->iopt2 & Y_LIM)  iopt |= X_LIM;

    if (oi->iopt2 & X_NUM)  iopt |= Y_NUM;

    if (oi->iopt2 & Y_NUM)  iopt |= X_NUM;

    oi->iopt2 = iopt;
    tmp = oi->c_xu;
    oi->c_xu = oi->c_yu;
    oi->c_yu = tmp;
    tmp = oi->n_xu;
    oi->n_xu = oi->n_yu;
    oi->n_yu = tmp;
    tmp = oi->m_xu;
    oi->m_xu = oi->m_yu;
    oi->m_yu = tmp;
    u = oi->xu;
    oi->xu = oi->yu;
    oi->yu = u;
    set_oi_x_unit_set(oi, oi->c_xu);
    set_oi_y_unit_set(oi, oi->c_yu);
    oi->need_to_refresh |= ALL_NEED_REFRESH;
    return 0;
}
int affine_transform_all_image_unitset(O_i *oi, int type, float offset, float multiplier)
{
    int i;

    if (oi != NULL && type == IS_X_UNIT_SET && oi->xu != NULL && oi->n_xu > 0)
    {
        for (i = 0 ; i < oi->n_xu ; i++)
        {
            oi->xu[i]->ax += offset;
            oi->xu[i]->dx *= multiplier;
        }

        oi->ax += offset;
        oi->dx *= multiplier;
    }
    else if (oi != NULL && type == IS_Y_UNIT_SET && oi->yu != NULL && oi->n_yu > 0)
    {
        for (i = 0 ; i < oi->n_yu ; i++)
        {
            oi->yu[i]->ax += offset;
            oi->yu[i]->dx *= multiplier;
        }

        oi->ay += offset;
        oi->dy *= multiplier;
    }
    else if (oi != NULL && type == IS_Z_UNIT_SET && oi->zu != NULL && oi->n_zu > 0)
    {
        for (i = 0 ; i < oi->n_zu ; i++)
        {
            oi->zu[i]->ax += offset;
            oi->zu[i]->dx *= multiplier;
        }

        oi->az += offset;
        oi->dz *= multiplier;
    }
    else return 1;

    return 0;
}
int find_op_nb_of_source_specific_ds_in_oi(O_i *oi, char *source)
{
    int i;
    d_s *dss = NULL;

    if (oi == NULL || source == NULL)   return -1;

    for (i = 0, dss = NULL; i < oi->n_op && dss == NULL; i++)
        dss = find_source_specific_ds_in_op(oi->o_p[i], source);

    return (dss == NULL)  ? -1 : i - 1;
}

int find_op_nb_of_treatement_specific_ds_in_oi(O_i *oi, char *treatement)
{
    int i;
    d_s *dss = NULL;

    if (oi == NULL || treatement == NULL)   return -1;

    for (i = 0, dss = NULL; i < oi->n_op && dss == NULL; i++)
        dss = find_treatement_specific_ds_in_op(oi->o_p[i], treatement);

    return (dss == NULL)  ? -1 : i - 1;
}
/*
    Extract a profile of an image (oi) starting at point (x0, y0)
    ending at (x1, y1). Both points are included. The profile is
    placed in a float array. If z is already allocated (!= NULL),
    the routine use this array otherwise it allocate it (it is your
    resposability fo free this array once used. The number of points
    is calculated using the size of the segment in pixels. If x and y
    scaling are different, the y scaling is the reference one. If the
    number of points (nz) allocated for the array (z) is smaller than
    the required size (*npts), this array is reallocated. The total
    number of points of the profile is returned in (npts).

    on error, npts is set to 0 and the functions return Null.


 */

float *extract_tilted_profile(O_i *oi, float *z, int nz, float x0, float y0, float x1, float y1, int *npts)
{
    int i;
    int np = 0;
    float *zt = NULL;

    if (oi == NULL)     return NULL;

    zt = extract_raw_tilted_profile(oi, z, nz, x0, y0, x1, y1, &np);
    *npts = np;

    if (zt == NULL) return NULL;

    for (i = 0; i < np; i++) zt[i] = oi->az + oi->dz * zt[i];

    return zt;
}
float *extract_raw_tilted_profile(O_i *oi, float *z, int nz, float x0, float y0, float x1, float y1, int *npts)
{
    int i; //, nx, ny;
    float zr, zi;
    int type, n;
    float *zb = NULL, x, y, ratio, dx, dy, nf, zrf = 0, zgf = 0, zbf = 0, zaf = 0;

    *npts = 0;

    if (oi == NULL)     return NULL;

    //nx = oi->im.nx;
    //ny = oi->im.ny;
    ratio = (oi->dy == 0) ? 1 : oi->dx / oi->dy;
    nf = ratio * ratio * (x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0);
    nf = sqrt(nf);
    n = (int)nf + 1;

    if ((zb = z) == NULL)   zb = (float *)calloc(n, sizeof(float));

    if (zb == NULL) return NULL;

    if (nz < n)     zb = (float *)realloc(z, n * sizeof(float));

    if (zb == NULL) return NULL;

    *npts = n;
    x = x0;
    y = y0;
    dx = (x1 - x0) / nf;
    dy = (y1 - y0) / nf;
    type = oi->im.data_type;

    if (type == IS_CHAR_IMAGE)
        for (i = 0; i < n; i++, x += dx, y += dy)
            zb[i] = ((float)fast_interpolate_image_point(oi, x, y, 0)) / 256;
    else if (type == IS_INT_IMAGE || type == IS_UINT_IMAGE ||
             type == IS_LINT_IMAGE || type == IS_FLOAT_IMAGE || type == IS_DOUBLE_IMAGE)
        for (i = 0; i < n; i++, x += dx, y += dy)
            zb[i] = interpolate_image_point(oi, x, y, &zr, NULL);
    else if (type == IS_RGB_PICTURE || type == IS_RGBA_PICTURE
             || type == IS_RGB16_PICTURE || type == IS_RGBA16_PICTURE)
    {
        for (i = 0; i < n; i++, x += dx, y += dy)
        {
            if (interpolate_RGB_picture_point(oi, x, y, &zrf, &zgf, &zbf, &zaf) == 0)
            {
                if (oi->im.mode == GREY_LEVEL || oi->im.mode == TRUE_RGB)
                    zb[i] = (zrf + zgf + zbf) / 3;
                else if (oi->im.mode & R_LEVEL)  zb[i] = zrf;
                else if (oi->im.mode & G_LEVEL)  zb[i] = zgf;
                else if (oi->im.mode & B_LEVEL)  zb[i] = zbf;
                else if (oi->im.mode == ALPHA_LEVEL) zb[i] = zaf;
                else return NULL;
            }
        }
    }
    else if (type == IS_COMPLEX_IMAGE || type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        for (i = 0; i < n; i++, x += dx, y += dy)
        {
            interpolate_image_point(oi, x, y, &zr, &zi);

            switch (oi->im.mode)
            {
            case AMP:
                zb[i] = sqrt(zr * zr + zi * zi);
                break;

            case LOG_AMP:
                zb[i] = zr * zr + zi * zi;
                zb[i] = (zb[i] > 0) ? log10(zb[i]) : -40;
                break;

            case AMP_2:
                zb[i] = zr * zr + zi * zi;
                break;

            case RE:
                zb[i] = zr;
                break;

            case IM:
                zb[i] = zi;
                break;
            };
        }
    }
    else    return NULL;

    return zb;
}
int set_image_starting_time(O_i *oi)
{
    if (oi == NULL) return 1;

    time(&((oi)->im.time));
#ifndef XV_UNIX
#ifndef XV_MAC
    oi->im.record_start =  my_ulclock();
#endif
#endif
    return 0;
}

int set_image_ending_time(O_i *oi)
{
    if (oi == NULL) return 1;

#ifndef XV_MAC
#ifndef XV_UNIX
    oi->im.record_duration = (double)(my_ulclock() - oi->im.record_start) / get_my_ulclocks_per_sec();
#endif
#endif
    return 0;
}


/*
 *		dest = oid
 *
 */
O_i 	*average_partial_movie(O_i *ois1, O_i *oid, int start, int n_frames)
{
    int i, j, im;
    O_i *dest = NULL;
    int  onx, ony, nf;
    int	dest_type = 0;
    float  *z = NULL;
    union pix *pd = NULL, *ps1 = NULL;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    nf = ois1->im.n_f;
    if (nf < 1) nf = 1;
    if (oid != NULL && (oid->im.nx != onx || oid->im.ny != ony))
      {
	    win_printf("Image size do not match");
	    return(NULL);
      }

    if (ois1->im.data_type == IS_CHAR_IMAGE || ois1->im.data_type == IS_INT_IMAGE
	|| ois1->im.data_type == IS_UINT_IMAGE || ois1->im.data_type == IS_FLOAT_IMAGE)
      {
	dest_type = IS_FLOAT_IMAGE;
	if (oid != NULL && oid->im.data_type != dest_type)
	  {
	    win_printf("Image size do not match");
	    return(NULL);
	  }
	dest = (oid != NULL) ? oid : create_one_image(onx, ony, dest_type);
	z = (float*)calloc(onx,sizeof(float));
	if (dest == NULL || z == NULL)
	  {
	    win_printf("Can't create dest image");
	    return(NULL);
	  }
	for (i=0, pd = dest->im.pixel; i< ony; i++)
	  {
	    for (j=0; j<onx; j++)
	      pd[i].fl[j] = 0;
	  }
	for (im = start; im < start + n_frames; im++)
	  {
	    switch_frame(ois1, im);
	    pd = dest->im.pixel;	ps1 = ois1->im.pixel;
	    for (i=0; i< ony; i++)
	      {
		extract_raw_line(ois1, i, z);
		for (j=0; j<onx; j++)
		    pd[i].fl[j] += z[j];
	      }
	  }
	if (z) free(z);
      }
      else if (ois1->im.data_type == IS_COMPLEX_IMAGE)
      {
	dest_type = IS_COMPLEX_IMAGE;
	if (oid != NULL && oid->im.data_type != dest_type)
	  {
	    win_printf("Image size do not match");
	    return(NULL);
	  }
	dest = (oid != NULL) ? oid : create_one_image(onx, ony, dest_type);
	if (dest == NULL)
	  {
	    win_printf("Can't create dest image");
	    return(NULL);
	  }
	for (i=0, pd = dest->im.pixel; i< ony; i++)
	  {
	    for (j=0; j<onx; j++)
	      pd[i].cp[j].re = pd[i].cp[j].im = 0;
	  }
	for (im = start; im < start + n_frames; im++)
	  {
	    switch_frame(ois1, im);
	    pd = dest->im.pixel;	ps1 = ois1->im.pixel;
	    for (i=0; i< ony; i++)
	      {
		for (j=0; j<onx; j++)
		  {
		    pd[i].cp[j].re += ps1[i].cp[j].re;
		    pd[i].cp[j].im += ps1[i].cp[j].im;
		  }
	      }
	  }
      }
      else if (ois1->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
      {
	dest_type = IS_COMPLEX_DOUBLE_IMAGE;
	if (oid != NULL && oid->im.data_type != dest_type)
	  {
	    win_printf("Image size do not match");
	    return(NULL);
	  }
	dest = (oid != NULL) ? oid : create_one_image(onx, ony, dest_type);
	if (dest == NULL)
	  {
	    win_printf("Can't create dest image");
	    return(NULL);
	  }
	for (i=0, pd = dest->im.pixel; i< ony; i++)
	  {
	    for (j=0; j<onx; j++)
	      pd[i].dcp[j].dre = pd[i].dcp[j].dim = 0;
	  }
	for (im = start; im < start + n_frames; im++)
	  {
	    switch_frame(ois1, im);
	    pd = dest->im.pixel;	ps1 = ois1->im.pixel;
	    for (i=0; i< ony; i++)
	      {
		for (j=0; j<onx; j++)
		  {
		    pd[i].dcp[j].dre += ps1[i].dcp[j].dre;
		    pd[i].dcp[j].dim += ps1[i].dcp[j].dim;
		  }
	      }
	  }
      }
      else if (ois1->im.data_type == IS_RGB_PICTURE)
      {
	dest_type = IS_RGB16_PICTURE;
	if (oid != NULL && oid->im.data_type != dest_type)
	  {
	    win_printf("Image size do not match");
	    return(NULL);
	  }
	dest = (oid != NULL) ? oid : create_one_image(onx, ony, dest_type);
	if (dest == NULL)
	  {
	    win_printf("Can't create dest image");
	    return(NULL);
	  }
	for (i=0, pd = dest->im.pixel; i< ony; i++)
	  {
	    for (j=0; j<onx; j++)
	      pd[i].rgb16[j].r = pd[i].rgb16[j].g = pd[i].rgb16[j].b = 0;
	  }
	for (im = start; im < start + n_frames; im++)
	  {
	    switch_frame(ois1, im);
	    pd = dest->im.pixel;	ps1 = ois1->im.pixel;
	    for (i=0; i< ony; i++)
	      {
		for (j=0; j<onx; j++)
		  {
		    pd[i].rgb16[j].r += ps1[i].rgb[j].r;
		    pd[i].rgb16[j].g += ps1[i].rgb[j].g;
		    pd[i].rgb16[j].b += ps1[i].rgb[j].b;
		  }
	      }
	  }
      }
      else if (ois1->im.data_type == IS_RGB16_PICTURE)
      {
	dest_type = IS_RGB16_PICTURE;
		if (oid != NULL && oid->im.data_type != dest_type)
	  {
	    win_printf("Image size do not match");
	    return(NULL);
	  }
	dest = (oid != NULL) ? oid : create_one_image(onx, ony, dest_type);
	if (dest == NULL)
	  {
	    win_printf("Can't create dest image");
	    return(NULL);
	  }
	for (i=0, pd = dest->im.pixel; i< ony; i++)
	  {
	    for (j=0; j<onx; j++)
	      pd[i].rgb16[j].r = pd[i].rgb16[j].g = pd[i].rgb16[j].b = 0;
	  }
	for (im = start; im < start + n_frames; im++)
	  {
	    switch_frame(ois1, im);
	    pd = dest->im.pixel;	ps1 = ois1->im.pixel;
	    for (i=0; i< ony; i++)
	      {
		for (j=0; j<onx; j++)
		  {
		    pd[i].rgb16[j].r += ps1[i].rgb16[j].r;
		    pd[i].rgb16[j].g += ps1[i].rgb16[j].g;
		    pd[i].rgb16[j].b += ps1[i].rgb16[j].b;
		  }
	      }
	  }
      }
      else if (ois1->im.data_type == IS_RGBA_PICTURE)
      {
	dest_type = IS_RGBA16_PICTURE;
	if (oid != NULL && oid->im.data_type != dest_type)
	  {
	    win_printf("Image size do not match");
	    return(NULL);
	  }
	dest = (oid != NULL) ? oid : create_one_image(onx, ony, dest_type);
	if (dest == NULL)
	  {
	    win_printf("Can't create dest image");
	    return(NULL);
	  }
	for (i=0, pd = dest->im.pixel; i< ony; i++)
	  {
	    for (j=0; j<onx; j++)
	      pd[i].rgba16[j].r = pd[i].rgba16[j].g = pd[i].rgba16[j].b = pd[i].rgba16[j].a = 0;
	  }
	for (im = start; im < start + n_frames; im++)
	  {
	    switch_frame(ois1, im);
	    pd = dest->im.pixel;	ps1 = ois1->im.pixel;
	    for (i=0; i< ony; i++)
	      {
		for (j=0; j<onx; j++)
		  {
		    pd[i].rgba16[j].r += ps1[i].rgba[j].r;
		    pd[i].rgba16[j].g += ps1[i].rgba[j].g;
		    pd[i].rgba16[j].b += ps1[i].rgba[j].b;
		    pd[i].rgba16[j].a += ps1[i].rgba[j].a;
		  }
	      }
	  }
      }
      else if (ois1->im.data_type == IS_RGBA16_PICTURE)
      {
	dest_type = IS_RGBA16_PICTURE;
	if (oid != NULL && oid->im.data_type != dest_type)
	  {
	    win_printf("Image size do not match");
	    return(NULL);
	  }
	dest = (oid != NULL) ? oid : create_one_image(onx, ony, dest_type);
	if (dest == NULL)
	  {
	    win_printf("Can't create dest image");
	    return(NULL);
	  }
	for (i=0, pd = dest->im.pixel; i< ony; i++)
	  {
	    for (j=0; j<onx; j++)
	      pd[i].rgba16[j].r = pd[i].rgba16[j].g = pd[i].rgba16[j].b = pd[i].rgba16[j].a = 0;
	  }
	for (im = start; im < start + n_frames; im++)
	  {
	    switch_frame(ois1, im);
	    pd = dest->im.pixel;	ps1 = ois1->im.pixel;
	    for (i=0; i< ony; i++)
	      {
		for (j=0; j<onx; j++)
		  {
		    pd[i].rgba16[j].r += ps1[i].rgba16[j].r;
		    pd[i].rgba16[j].g += ps1[i].rgba16[j].g;
		    pd[i].rgba16[j].b += ps1[i].rgba16[j].b;
		    pd[i].rgba16[j].a += ps1[i].rgba16[j].a;
		  }
	      }
	  }
      }
    inherit_from_im_to_im(dest,ois1);
    uns_oi_2_oi(dest,ois1);
    dest->im.win_flag = ois1->im.win_flag;
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}


# endif /* _IM_OI_C_ */
