#include "xvin.h"
#include "allegro.h"

#ifdef XV_WIN32
#include "winalleg.h"
#endif
#ifdef XV_UNIX
#include <linux/limits.h>
#endif

BITMAP *plt_buffer = NULL;
int plt_buffer_used = 0;
int screen_used = 0;

char prog_dir[PATH_MAX] = {0}; // 2011-04-08, NG : replaced 256 by 512
char first_data_dir[PATH_MAX] = {0}; // the first data directory used


char config_dir[PATH_MAX] = "./"; // where are config file xvin.cfg and log files
// on Linux and MacOS, this directory is not where the application is.
// (it can be user dependent)
//#ifdef XV_WIN32
//  snprintf(config_dir,512,"%s",prog_dir);
//#endif

int  win_color_depth = 0;
int  win_screen_w = 0;
int  win_screen_h = 0;

int               eps_file_flag = 0;

int (*general_idle_action)(DIALOG *d) = NULL;
int (*general_end_action)(DIALOG *d) = NULL;

#include "fonts.h"

FONT *small_font = NULL;
FONT *normalsize_font = NULL;
FONT *large_font = NULL;
int small_font_height = 10;
int normalsize_font_height = 12;
int large_font_height = 16;
int small_font_width = 6;
int normalsize_font_width = 8;
int large_font_width = 10;
int main_menu_height = 22;

DATAFILE *datafile;

bool init_fonts(const char *datafilefullpath)
{
    char fullfile[PATH_MAX] = {0};
    FILE *font_file = NULL;


    if (datafilefullpath == NULL)
    {
        datafilefullpath = get_config_string("FONT", "DATAFILE", "");

        if (datafilefullpath[0] == '\0' || (font_file = fopen(datafilefullpath, "r")) == NULL)
        {
            my_strncat(fullfile, prog_dir, sizeof(fullfile));
            put_path_separator(fullfile, sizeof(fullfile));
            my_strncat(fullfile, "fonts.dat", sizeof(fullfile));
        }
        else
        {
            if (font_file)
            {
                fclose(font_file);
            }
            strncpy(fullfile, datafilefullpath, sizeof(fullfile));
        }
    }
    else
    {
        strncpy(fullfile, datafilefullpath, sizeof(fullfile));
    }


    normalsize_font = font;

    datafile = load_datafile(fullfile);

    if (datafile)
    {
        small_font = (FONT *) datafile[SMALL_FONT].dat;
        font = normalsize_font = (FONT *) datafile[NORMALSIZE_FONT].dat;
        large_font = (FONT *) datafile[LARGE_FONT].dat;

        small_font_height = text_height(small_font);
        normalsize_font_height = text_height(normalsize_font);
        large_font_height = text_height(large_font);

        small_font_width = text_length(small_font, "A"); // assuming Monospace Font
        normalsize_font_width = text_length(normalsize_font, "A");
        large_font_width = text_length(large_font, "A");

        set_config_string("FONT", "DATAFILE", fullfile);
        //if (win_scanf("ttf text scale : %d",&fen) != WIN_CANCEL)
        //{
        //    text_scale = fen;
        //}
        return true;
    }
    else
    {
        win_printf("data not loaded from \n%s", fullfile);
        return false;
    }



}
/* custom dialog procedure for the clock object */
int general_idle(int msg, DIALOG *d, int c)
{
  (void)c;
    /* process the message */
    switch (msg)
    {
    /* respond to idle messages */
    case MSG_IDLE:
        if (general_idle_action)
            general_idle_action(d);

        break;

    case MSG_END:
        if (general_end_action)
            general_end_action(d);

        break;
    }

    /* always return OK status, since the clock doesn't ever need to close
     * the dialog or get the input focus.
     */
    return D_O_K;
}

int switch_allegro_font(int mode)
{
  (void)mode;
    font = normalsize_font;
    //if ((mode == 1) && (large_font != NULL))
    //{
    //    //        win_printf("Big");
    //    font = large_font;
    //}
    //else if (mode == 2) font = small_font;
    //else if (mode == 0)     font = normalsize_font;
    //else
    //    return win_printf("mode %d", mode);

    return 0;
}

/* d_myclear_proc:
 *  Simple dialog procedure which just clears the screen. Useful as the
 *  first object in a dialog.
 */
int d_myclear_proc(int msg, DIALOG *d, int c)
{
    (void) c;
    ASSERT(d);

    if (msg == MSG_DRAW)
    {
# ifdef XV_WIN32

        if (win_get_window() == GetForegroundWindow())
        {
# endif
            //     if (!screen_used)
            //  {
            //    screen_used = 1;
            //acquire_screen();
            set_clip_rect(screen, d->x, d->y, d->x + d->w - 1, d->y + d->h - 1);
            //release_screen();
            //vsync();
            //acquire_screen();
            clear_to_color(screen, makecol(64, 64, 255));
            set_clip_rect(screen, 0, 0, SCREEN_W - 1, SCREEN_H - 1);
            //release_screen();
            //  screen_used = 0;
            // }
# ifdef XV_WIN32
        }

# endif
    }

    return D_O_K;
}

BITMAP *get_plt_buffer(void)
{
    return plt_buffer;
}
DIALOG *get_cur_ac_reg(void)
{
    return cur_ac_reg;
}
