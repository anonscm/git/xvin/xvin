# ifndef _DEV_PCX_C_
# define _DEV_PCX_C_
#include "dev_pcx.h"

# include "xvin/platform.h"
#include "dev_def.h"
#include "plot_opt.h"
#include "allegro.h"
#include "color.h"
# include "box_def.h"
# include "math.h"
# include "xvin.h"

# include <stdlib.h>
# include <string.h>

int 		init_font(void);

static float ix_save = 0, iy_save = 0;

extern int last_color;
int	 dev_color = 1;
static float xclip, yclip, x1clip, y1clip;




struct device q_dev;
int pc_fen;


int pc_move(float ix, float iy)
{
    /*	fprintf (stderr, "move %6d %6d   \r",ix,iy);*/
    ix_save = ix;
    iy_save = iy;
    return 0;
}
int pc_rmove(float ix, float iy)
{
    ix_save += ix;
    iy_save += iy;
    return 0;
}

int pc_color(int cl, int mode)
{
    static int color_mem = 1;

    if (mode >= FORCED_COLOR)
    {
        dev_color = makecol(EXTRACT_R(cl), EXTRACT_G(cl), EXTRACT_B(cl));
        color_mem = FORCED_COLOR;
    }
    else if (mode >= TRUE_COLOR) 	/* && color_mem == FORCED_COLOR)*/
    {
        color_mem = 0;
        dev_color = makecol(EXTRACT_R(cl), EXTRACT_G(cl), EXTRACT_B(cl));
    }
    else if (color_mem != FORCED_COLOR)
    {
        dev_color = makecol(EXTRACT_R(cl), EXTRACT_G(cl), EXTRACT_B(cl));
    }

    last_color = dev_color;
    return 0;
}

int pc_line_size(float size)
{
    (void)size;
    return 0;
}

/* Change between graphics mode and text mode; erase screen */
int pc_mode(int arg)
{
    switch (arg)
    {
    case GRAPHICS:
        q_dev.move = pc_catch1;
        q_dev.line = pc_catch1;
        q_dev.dot = pc_catch2;
        break;

    case ERASE:
        pc_init1();
        break;

    case DONE:
        pc_init1();
        break;
    }

    return 0;
}

/* Place a dot at given position and update current position.
 *
 *	value of iy --> dx
 *	value of ix --> cx
 *	mov	ax,0c0fh	;high = 0c (write), low = 0f (hi int. white)
 *	int	10h
 */
int pc_dot(float ix, float iy)
{
    int  x, y;
    x = ix;
    y = y_offset - iy;
    putpixel(dev_bitmap, (int)x, (int)y, dev_color);
    ix_save = ix;
    iy_save = iy;
    return 0;
}

/* Draw line from current position to (x2, y2) and update current position */
# ifdef PB
int pc_line(float x2, float y2)
{
    int xclipa, yclipa, x1clipa, y1clipa;
    double A, B, C; // line Ax + by + C = 0
    double c00, c01, c10, c11;
    int xc[2] = {0}, yc[2] = {0}, ic = 0, xt, yt, in, xt0, yt0, xt1, yt1, in1;
    xclipa = dev_bitmap->cl;
    x1clipa = dev_bitmap->cr;
    yclipa = dev_bitmap->ct;
    y1clipa = dev_bitmap->cb;
    xt0 = (int)(0.5 + ix_save);
    xt1 = (int)(0.5 + x2);
    yt0 = (int)(0.5 + y_offset - iy_save);
    yt1 = (int)(0.5 + y_offset - y2);
    in = ((xt0 >= xclipa) && (xt0 < x1clipa) && (yt0 >= yclipa) && (yt0 < y1clipa)) ? 1 : 0;
    in1 = ((xt1 >= xclipa) && (xt1 < x1clipa) && (yt1 >= yclipa) && (yt1 < y1clipa)) ? 1 : 0;

    if (in && in1)
    {
        line(dev_bitmap, xt0, yt0, xt1, yt1, dev_color);
        ix_save = x2;
        iy_save = y2;
        return 0;
    }

    // we must clip !
    A = iy_save - y2; // y_offset disaooear
    B = ix_save - x2;
    C = -A * ix_save - B * (y_offset - iy_save);

    if (c00 * c10 <= 0)
    {
        if (A == 0)
        {
            xc[ic] = xclipa;
            yc[ic++] = yclipa;
            xc[ic] = x1clipa;
            yc[ic++] = yclipa;
        }
        else
        {
            xc[ic] = -(int)(0.5 + ((C + B * yclipa) / A));
            yc[ic++] = yclipa;
        }
    }

    if ((ic < 2) && (c01 * c11 <= 0))
    {
        if (A == 0)
        {
            if (ic < 2)
            {
                if ((ic == 0) || (xclipa != xc[0]) || (y1clipa != yc[0]))
                {
                    xc[ic] = xclipa;
                    yc[ic++] = y1clipa;
                }
            }

            if (ic < 2)
            {
                if ((ic == 0) || (x1clipa != xc[0]) || (y1clipa != yc[0]))
                {
                    xc[ic] = x1clipa;
                    yc[ic++] = y1clipa;
                }
            }
        }
        else if (ic < 2)
        {
            xt = -(int)(0.5 + ((C + B * y1clipa) / A));
            yt = y1clipa;

            if ((ic == 0) || (xt != xc[0]) || (yt != yc[0]))
            {
                xc[ic] = xt;
                yc[ic++] = yt;
            }
        }
    }

    if ((ic < 2) && (c00 * c01 <= 0))
    {
        if (B == 0)
        {
            if (ic < 2)
            {
                if ((ic == 0) || (xclipa != xc[0]) || (yclipa != yc[0]))
                {
                    xc[ic] = xclipa;
                    yc[ic++] = yclipa;
                }
            }

            if (ic < 2)
            {
                if ((ic == 0) || (xclipa != xc[0]) || (y1clipa != yc[0]))
                {
                    xc[ic] = xclipa;
                    yc[ic++] = y1clipa;
                }
            }
        }
        else if (ic < 2)
        {
            yt = -(int)(0.5 + ((C + A * xclipa) / B));
            xt = xclipa;

            if ((ic == 0) || (xt != xc[0]) || (yt != yc[0]))
            {
                xc[ic] = xt;
                yc[ic++] = yt;
            }
        }
    }

    if ((ic < 2) && (c10 * c11 <= 0))
    {
        if (B == 0)
        {
            if (ic < 2)
            {
                if ((ic == 0) || (x1clipa != xc[0]) || (yclipa != yc[0]))
                {
                    xc[ic] = x1clipa;
                    yc[ic++] = yclipa;
                }
            }

            if (ic < 2)
            {
                if ((ic == 0) || (x1clipa != xc[0]) || (y1clipa != yc[0]))
                {
                    xc[ic] = x1clipa;
                    yc[ic++] = y1clipa;
                }
            }
        }
        else if (ic < 2)
        {
            yt = -(int)(0.5 + ((C + A * x1clipa) / B));
            xt = x1clipa;

            if ((ic == 0) || (xt != xc[0]) || (yt != yc[0]))
            {
                xc[ic] = xt;
                yc[ic++] = yt;
            }
        }
    }

    if (ic == 2)
    {
        if ((in == 0) && (in1 == 0))
        {
            if (abs(xc[1] - xc[0]) > abs(yc[1] - yc[0]))
            {
                if (((xc[0] - xt0) * (xc[0] - xt1) <= 0) && ((xc[1] - xt0) * (xc[1] - xt1) <= 0))
                {
                    line(dev_bitmap, xc[0], yc[0], xc[1], yc[1], dev_color);
                }
            }
            else
            {
                if (((yc[0] - yt0) * (yc[0] - yt1) <= 0) && ((yc[1] - yt0) * (yc[1] - yt1) <= 0))
                {
                    line(dev_bitmap, xc[0], yc[0], xc[1], yc[1], dev_color);
                }
            }
        }
        else if (in)
        {
            if (abs(xc[1] - xc[0]) > abs(yc[1] - yc[0]))
            {
                if ((xc[0] - xt0) * (xc[0] - xt1) <= 0)
                {
                    line(dev_bitmap, xc[0], yc[0], xt0, yt0, dev_color);
                }
                else
                {
                    line(dev_bitmap, xc[1], yc[1], xt0, yt0, dev_color);
                }
            }
            else
            {
                if ((yc[0] - yt0) * (yc[0] - yt1) <= 0)
                {
                    line(dev_bitmap, xc[0], yc[0], xt0, yt0, dev_color);
                }
                else
                {
                    line(dev_bitmap, xc[1], yc[1], xt0, yt0, dev_color);
                }
            }
        }
        else if (in1)
        {
            if (abs(xc[1] - xc[0]) > abs(yc[1] - yc[0]))
            {
                if ((xc[0] - xt0) * (xc[0] - xt1) <= 0)
                {
                    line(dev_bitmap, xc[0], yc[0], xt1, yt1, dev_color);
                }
                else
                {
                    line(dev_bitmap, xc[1], yc[1], xt1, yt1, dev_color);
                }
            }
            else
            {
                if ((yc[0] - yt0) * (yc[0] - yt1) <= 0)
                {
                    line(dev_bitmap, xc[0], yc[0], xt1, yt1, dev_color);
                }
                else
                {
                    line(dev_bitmap, xc[1], yc[1], xt1, yt1, dev_color);
                }
            }
        }
    }

    //	line (dev_bitmap,(int)ix_save,(int)(y_offset-iy_save),
    //(int)x2,(int)(y_offset-y2),dev_color);
    ix_save = x2;
    iy_save = y2;
    return 0;
}
# endif
#define MY_FLT_MIN        1.1754943508222875e-20 // was e-38
int pc_line(float x2, float y2)
{
    int xclipa, yclipa, x1clipa, y1clipa;
    double A, B, C; // line Ax + by + C = 0
    double tmp, tmp1, fxt0, fyt0, fxt1, fyt1;
    int ic = 0, in,  in1;//, color;
    float xc[2] = {0}, yc[2] = {0}, xt0, yt0, xt1, yt1;
    xclipa = dev_bitmap->cl;
    x1clipa = dev_bitmap->cr;
    yclipa = dev_bitmap->ct;
    y1clipa = dev_bitmap->cb;
    //color = dev_color;
    fxt0 = ix_save;
    fxt1 = x2;
    fyt0 = y_offset - iy_save;
    fyt1 = y_offset - y2;
    xt0 = fxt0;
    xt1 = fxt1; //(int)(0.5 + fxt1);
    yt0 = fyt0; //(int)(0.5 + fyt0);
    yt1 = fyt1; //(int)(0.5 + fyt1);
    in = (((fxt0 - xclipa) * (fxt0 - x1clipa) <= 0) && ((fyt0 - yclipa) * (fyt0 - y1clipa) <= 0)) ? 1 : 0;
    in1 = (((fxt1 - xclipa) * (fxt1 - x1clipa) <= 0) && ((fyt1 - yclipa) * (fyt1 - y1clipa) <= 0)) ? 1 : 0;

    if (in && in1)
    {
        line(dev_bitmap, xt0, yt0, xt1, yt1, dev_color);
        ix_save = x2;
        iy_save = y2;
        return 0;
    }

    // we must clip !
    A = iy_save - y2; // y_offset disaooear
    B = ix_save - x2;
    C = -A * ix_save - B * (y_offset - iy_save);

    if (fabs(A) > MY_FLT_MIN)
    {
        tmp = -(C + B * yclipa) / A;

        if ((ic < 2) && ((tmp - xclipa) * (tmp - x1clipa) <= 0))
        {
	  xc[ic] = tmp;//(int)(tmp + 0.5);
            yc[ic++] = yclipa;
        }

        tmp = -(C + B * y1clipa) / A;

        if ((ic < 2) && (tmp >= xclipa) && (tmp < x1clipa))
        {
	  xc[ic] = tmp;//(int)(tmp + 0.5);
            yc[ic++] = y1clipa;
        }
    }
    else if (fabs(B) > MY_FLT_MIN)
    {
        tmp = -C / B;

        if ((ic == 0) && ((tmp - yclipa) * (tmp - y1clipa) <= 0))
        {
            xc[ic] = xclipa;
            yc[ic++] = tmp;//(int)(tmp + 0.5);
            xc[ic] = x1clipa;
            yc[ic++] = tmp;//(int)(tmp + 0.5);
            //color = Magenta;
        }
    }

    if (fabs(B) > MY_FLT_MIN)
    {
        tmp = -(C + A * xclipa) / B;

        if ((ic < 2) && ((tmp - yclipa) * (tmp - y1clipa) <= 0))
        {
	  yc[ic] = tmp; //(int)(tmp + 0.5);
            xc[ic++] = xclipa;
        }

        tmp = -(C + A * x1clipa) / B;

        if ((ic < 2) && ((tmp - yclipa) * (tmp - y1clipa) <= 0))
        {
	  yc[ic] = tmp;//(int)(tmp + 0.5);
            xc[ic++] = x1clipa;
        }
    }
    else if (fabs(A) > MY_FLT_MIN)
    {
        tmp = -C / A;

        if ((ic == 0) && ((tmp - xclipa) * (tmp - x1clipa) <= 0))
        {
            yc[ic] = yclipa;
            xc[ic++] = tmp;//(int)(tmp + 0.5);
            yc[ic] = y1clipa;
            xc[ic++] = tmp;//(int)(tmp + 0.5);
            //color = Lightred;
        }
    }

    ix_save = x2;
    iy_save = y2;

    if (ic == 0)
    {
        return 0;
    }

    if (ic == 1)
    {
        yc[ic] = yc[ic - 1];
        xc[ic] = xc[ic - 1];
        ic++;
        //color = Lightgreen;
    }

    if ((in == 0) && (in1 == 0))
    {
        tmp = (fxt0 - xc[0]) * (fxt1 - xc[0]) + (fyt0 - yc[0]) * (fyt1 - yc[0]);
        tmp1 = (fxt0 - xc[1]) * (fxt1 - xc[1]) + (fyt0 - yc[1]) * (fyt1 - yc[1]);

        if ((tmp <= 0) && (tmp1 <= 0))
        {
            line(dev_bitmap, xc[0], yc[0], xc[1], yc[1], dev_color);
        }

        return 0;
    }

    if (in) // point xt0, yt0 is inside
    {
        tmp = (fxt1 - xc[0]) * (fxt1 - xc[0]) + (fyt1 - yc[0]) * (fyt1 - yc[0]);
        tmp1 = (fxt1 - xc[1]) * (fxt1 - xc[1]) + (fyt1 - yc[1]) * (fyt1 - yc[1]);

        if (tmp < tmp1)
        {
            line(dev_bitmap, xc[0], yc[0], xt0, yt0, dev_color);
        }
        else
        {
            line(dev_bitmap, xc[1], yc[1], xt0, yt0, dev_color);
        }
    }
    else
    {
        tmp = (xc[0] - xt0) * (xc[0] - xt0) + (yc[0] - yt0) * (yc[0] - yt0);
        tmp1 = (xc[1] - xt0) * (xc[1] - xt0) + (yc[1] - yt0) * (yc[1] - yt0);

        if (tmp < tmp1)
        {
            line(dev_bitmap, xc[0], yc[0], xt1, yt1, dev_color);
        }
        else
        {
            line(dev_bitmap, xc[1], yc[1], xt1, yt1, dev_color);
        }
    }

    return 0;
}


int 	pc_clip(float x0, float y0, float x1, float y1)
{
    float a, b, c , d;
    xclip = dev_bitmap->cl;
    x1clip = dev_bitmap->cr;
    yclip = dev_bitmap->ct;
    y1clip = dev_bitmap->cb;
    a = (x0 < xclip) ? xclip : x0;
    b =  y_offset - y0;
    d =  y_offset - y1;

    if (d < b)
    {
        d =  y_offset - y0;
        b =  y_offset - y1;
    }

    b = (b < yclip) ? yclip : b;
    c = (x1 > x1clip) ? x1clip : x1;
    d = (d > y1clip) ? y1clip : d;
    set_clip_rect(dev_bitmap, (int)a, (int)b, (int)c, (int)d);
    return 0;
}
int 	pc_clip_reset(void)
{
    set_clip_rect(dev_bitmap, (int)xclip, (int)yclip, (int)x1clip, (int)y1clip);
    return 0;
}
int pc_rline(float x2, float y2)
{
    /*
    line (dev_bitmap,(int)ix_save,(int)(y_offset-iy_save),
    (int)(ix_save+x2),(int)(y_offset-iy_save-y2),dev_color);
    ix_save += x2;
    iy_save += y2;
    return 0;


    */
    return pc_line(ix_save + x2, iy_save + y2);
}

/* Set font size, line width, etcetera. PC has none of this */
int pc_size(int arg)
{
    (void) arg;
    return 0;
}

/* Initialize global variables and device structure. */
int pc_init(void)
{
    init_font();
    return (pc_init1());
}
int pc_init1(void)
{
    xclip = yclip = 0;
    x1clip = (float)dev_bitmap->w;
    y1clip = (float)dev_bitmap->h;
    set_clip_rect(dev_bitmap, (int)xclip, (int)yclip, (int)x1clip, (int)y1clip);
    q_dev.move = pc_move;
    q_dev.rmove = pc_rmove;
    q_dev.mode = pc_mode;
    q_dev.dot = pc_dot;
    q_dev.line = pc_line;
    q_dev.rline = pc_rline;
    q_dev.size = pc_size;
    q_dev.line_size = pc_line_size;
    q_dev.color = pc_color;
    q_dev.clip = pc_clip;
    q_dev.clip_reset = pc_clip_reset;
    q_dev.draw_char = NULL;
    q_dev.draw_char_90 = NULL;
    pc_fen = 50;
    dev_color = 1;
    y_offset = dev_bitmap->h;
    return 0;
}

/* These routines are enabled at the start of graphics input. Their purpose is
 * to catch the first point. Consider: when passing a bunch of points to
 * have lines drawn between you do NOT want to draw a line to the first
 * point; the first point is the starting point.
 */
int pc_catch1(float x, float y)
{
    q_dev.move = pc_move;
    q_dev.dot = pc_dot;
    q_dev.line = pc_line;
    ix_save = x;
    iy_save = y;
    return 0;
}
int pc_catch2(float x, float y)
{
    q_dev.move = pc_move;
    q_dev.dot = pc_dot;
    q_dev.line = pc_line;
    return (pc_dot(x, y));
}
int demgr(int color_depth, int gfx_mode, int w, int h)
{
    set_color_depth(color_depth);

    if (set_gfx_mode(gfx_mode, w, h, 0, 0) != 0)
    {
    //Trying rescue mode
    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0) != 0)
        {
            set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
            allegro_message("Error setting rescue mode !\n%s\n", allegro_error);
            return 1;
        }
        else
        {
            allegro_message("Error setting graphics mode !\n%s\n Started in rescue mode", allegro_error);
        }
    }

    dev_bitmap = screen;
    /*	textout(screen, font, "mode set", 16, 16, makecol(128,128,128));*/
    gris(GREY_0, RED_0);
    y_offset = dev_bitmap->h;
    max_x = dev_bitmap->h;
    max_y = dev_bitmap->w;
    return 0;
}

int gris(int zmin, int zmax)
{
    int regc;
    int gris;
    RGB rgb;
    Black			=	0;
    Blue			= BUILD_COLOR(0,  0, 128);
    Green			= BUILD_COLOR(0, 128,  0);
    Cyan			= BUILD_COLOR(0, 128, 128);
    Red				= BUILD_COLOR(128,  0,  0);
    Magenta			= BUILD_COLOR(128,  0, 128);
    Brown			= BUILD_COLOR(128, 128,  0);
    Lightgray		= BUILD_COLOR(171, 171, 171);
    Darkgray		= BUILD_COLOR(85, 85, 85);
    Lightblue		= BUILD_COLOR(64, 64, 255);
    Lightgreen		= BUILD_COLOR(64, 255, 64);
    Lightcyan		= BUILD_COLOR(0, 255, 255);
    Lightred		= BUILD_COLOR(255, 64, 64);
    Lightmagenta	= BUILD_COLOR(255,  0, 255);
    Yellow			= BUILD_COLOR(255, 255,  0);
    White 			= BUILD_COLOR(255, 255, 255);

    if (bitmap_color_depth(dev_bitmap) != 8)
    {
        return 0;
    }

    /* the 16 dos color */
    rgb.r = 0;
    rgb.g = 0;
    rgb.b = 128;
    set_color(1, &rgb);
    rgb.r = 0;
    rgb.g = 128;
    rgb.b = 0;
    set_color(2, &rgb);
    rgb.r = 0;
    rgb.g = 128;
    rgb.b = 128;
    set_color(3, &rgb);
    rgb.r = 128;
    rgb.g = 0;
    rgb.b = 0;
    set_color(4, &rgb);
    rgb.r = 128;
    rgb.g = 0;
    rgb.b = 128;
    set_color(5, &rgb);
    rgb.r = 128;
    rgb.g = 128;
    rgb.b = 0;
    set_color(6, &rgb);
    rgb.r = 170;
    rgb.g = 170;
    rgb.b = 170;
    set_color(7, &rgb);
    rgb.r = 85;
    rgb.g = 85;
    rgb.b = 85;
    set_color(8, &rgb);
    rgb.r = 64;
    rgb.g = 64;
    rgb.b = 225;
    set_color(9, &rgb);
    rgb.r = 64;
    rgb.g = 225;
    rgb.b = 64;
    set_color(10, &rgb);
    rgb.r = 0;
    rgb.g = 255;
    rgb.b = 255;
    set_color(11, &rgb);
    rgb.r = 255;
    rgb.g = 64;
    rgb.b = 64;
    set_color(12, &rgb);
    rgb.r = 255;
    rgb.g = 0;
    rgb.b = 255;
    set_color(13, &rgb);
    rgb.r = 255;
    rgb.g = 255;
    rgb.b = 0;
    set_color(14, &rgb);
    rgb.r = 0;
    rgb.g = 0;
    rgb.b = 128;
    set_color(15, &rgb);
    rgb.r = 255;
    rgb.g = 255;
    rgb.b = 255;
    set_color(16, &rgb);
    /* red over plane */
    rgb.r = 255;
    rgb.g = 0;
    rgb.b = 0;

    for (gris = 0, regc = RED_0; regc < 256; regc++)
    {
        set_color(regc, &rgb);
    }

    /* grey scale */
    for (gris = 0, regc = GREY_0; regc < GREY_0 + 64; regc++)
    {
        if (regc < zmin)
        {
            gris = 0;
        }

        if (regc > zmax)
        {
            gris = 63;
        }

        if (regc > zmin && regc <= zmax)
        {
            gris = (255 * (regc - zmin)) / (zmax - zmin);
        }

        rgb.r = rgb.g = rgb.b = gris;
        set_color(regc, &rgb);
    }

    return 0;
}/*gris*/


# endif



