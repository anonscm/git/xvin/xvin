
#ifndef _PLOT_REG_C_
#define _PLOT_REG_C_

# include <stdlib.h>
# include <string.h>

# include "xvin/platform.h"
# include "plot_reg.h"
# include "xvin.h"

static int first = 0;

/*	build_plot_region ( int x0, int y0, int width, int height)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
pltreg *build_plot_region ( int x0, int y0, int width, int height)
{
  pltreg *pr = NULL;


  if (first == 0)
    {
      PlotBackgroundColor = Black;
      PlotAxesColor = White;
      first = 1;
    }

  pr = (pltreg *)calloc(1,sizeof(pltreg));
  if ( pr == NULL )
    {
      fprintf (stderr, "calloc pb    \n");
      exit(1);
    }
  pr->o_p = (O_p **)calloc(MAX_ONE_PLOT,sizeof(O_p*));
  pr->c_op = (int *)calloc(MAX_ONE_PLOT,sizeof(int));
  pr->one_p = (O_p *)calloc(1,sizeof(O_p));
  pr->l_db = (dybox *)calloc(MAX_ACTIVE_DB,sizeof(dybox));
  if (pr->one_p == NULL || pr->o_p == NULL || pr->l_db == NULL)
    {
      fprintf(stderr,"calloc error \n");
      exit(1);
    }
  pr->m_db = MAX_ACTIVE_DB;
  pr->n_op = 1;
  pr->m_op = MAX_ONE_PLOT;
  pr->cur_op = 0;
  pr->o_p[0] = pr->one_p;
  pr->stack = &(pr->one_p->bplt);
  init_box(pr->stack);
  pr->stack->pt = 10;
  pr->stack->color = Lightgreen;
  setsize_rec(&pr->def, x0, y0, width, height);
  //	setaction_dybox(&pr->db, IS_BOX_REGION, run_pr_db, (void*)pr);
  pr->x0 = pr->y0 = pr->s_nx = pr->s_ny = 0;
  pr->screen_scale = 30;
  pr->auto_scale = 1;
  pr->min_scale = 10;
  pr->max_scale = 100;
  pr->mag_step = 1.21;
  init_one_plot(pr->one_p);
  pr->display_mode = IS_ONE_PLOT;
  pr->filename = NULL;	pr->path = NULL;
  pr->mark_v = pr->mark_h = 20;
  pr->use.to = 0;				pr->use.stuff = NULL;
  /*	pr->dlg.to = 0;				pr->dlg.stuff = NULL;		*/
  pr->plot_end_action = NULL;
  pr->stack_in_use = 0;
  pr->blitting_stack = 0;
  return pr;
}


/*	build_plot_region ( int x0, int y0, int width, int height)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
pltreg *build_an_empty_plot_region ( int x0, int y0, int width, int height)
{
  pltreg *pr = NULL;

  if (first == 0)
    {
      PlotBackgroundColor = Black;
      PlotAxesColor = White;
      first = 1;
    }

  pr = (pltreg *)calloc(1,sizeof(pltreg));
  if ( pr == NULL )
    {
      fprintf (stderr, "calloc pb    \n");
      exit(1);
    }
  pr->o_p = (O_p **)calloc(MAX_ONE_PLOT,sizeof(O_p*));
  pr->c_op = (int *)calloc(MAX_ONE_PLOT,sizeof(int));
  pr->one_p = NULL;
  pr->l_db = (dybox *)calloc(MAX_ACTIVE_DB,sizeof(dybox));
  if (pr->o_p == NULL || pr->l_db == NULL)
    {
      fprintf(stderr,"calloc error \n");
      exit(1);
    }
  pr->n_db = 0;
  pr->m_db = MAX_ACTIVE_DB;
  pr->n_op = 0;
  pr->m_op = MAX_ONE_PLOT;
  pr->cur_op = 0;
  pr->stack = NULL;
  setsize_rec(&pr->def, x0, y0, width, height);
  //	setaction_dybox(&pr->db, IS_BOX_REGION, run_pr_db, (void*)pr);
  pr->x0 = pr->y0 = pr->s_nx = pr->s_ny = 0;
  pr->screen_scale = 30;
  pr->auto_scale = 1;
  pr->min_scale = 10;
  pr->max_scale = 100;
  pr->mag_step = 1.21;
  pr->display_mode = IS_ONE_PLOT;
  pr->filename = NULL;	pr->path = NULL;
  pr->stack_in_use = 0;
  pr->blitting_stack = 0;
  return pr;
}

/*	int free_plot_region ( pltreg* pr);
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int free_plot_region ( pltreg* pr)
{
  register int i;

  if (pr == NULL) return 1;
  for (i=0 ; i< pr->n_op ; i++)	free_one_plot(pr->o_p[i]);
  if (pr->o_p != NULL)		free(pr->o_p);
  if (pr->c_op != NULL)		free(pr->c_op);
  if (pr->l_db != NULL)	 	free(pr->l_db);
  if (pr->filename != NULL)	free(pr->filename);
  if (pr->path != NULL)	    free(pr->path);
  /*	free_box(pr->stack);	*/
  free(pr);
  return 0;
}


O_p* create_and_attach_one_plot(pltreg *pr,int nx, int ny, int data_type)
{
  O_p *op = NULL;
  int c_op, new_idx, i;

  if (nx <= 0 || ny <= 0)		return NULL;
  op = create_one_plot(nx, nx, 0);
  if (op == NULL)			return NULL;
  if ( pr != NULL )
    {
      c_op = pr->cur_op;
      if (add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)op))
	{ free_one_plot(op); return NULL;}
      new_idx = pr->n_op - 1;
      if (data_type == 0)
	{
	  pr->cur_op = pr->n_op - 1;
	}
      else if (data_type & INSERT_HERE)
	{
	  for(i = new_idx; i > c_op+1; i--)
	    pr->o_p[i] = pr->o_p[i-1];	  
	  //for(i  = c_op+1; i < new_idx; i++)
	  //  pr->o_p[i+1] = pr->o_p[i];
	  pr->o_p[c_op+1] = op;
	  pr->cur_op = c_op + 1;
	}
      pr->one_p = pr->o_p[pr->cur_op];

    }
  return op;
}

O_p* create_and_attach_one_empty_plot(pltreg *pr, int data_type)
{
  O_p *op = NULL;
  int c_op, new_idx, i;

  op = create_one_empty_plot();
  if (op == NULL)			return NULL;
  if ( pr != NULL )
    {
      c_op = pr->cur_op;
      if (add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)op))
	{ free_one_plot(op); return NULL;}
      new_idx = pr->n_op - 1;
      if (data_type == 0)
	{
	  pr->cur_op = pr->n_op - 1;
	}
      else if (data_type & INSERT_HERE)
	{
	  for(i = new_idx; i > c_op+1; i--)
	    pr->o_p[i] = pr->o_p[i-1];	  
	  //for(i  = c_op+1; i < new_idx; i++)
	  //  pr->o_p[i+1] = pr->o_p[i];
	  pr->o_p[c_op+1] = op;
	  pr->cur_op = c_op + 1;
	}
      pr->one_p = pr->o_p[pr->cur_op];

    }
  return op;
}

pltreg *create_pltreg_with_op(O_p **op, int nx, int ny, int data_type)
{
  pltreg *pr = NULL;
  O_p *opn = NULL;

  if (op == NULL)   return NULL;
  *op = NULL;
  pr = create_and_register_new_plot_project(0,   32,  900,  668);
  if (pr == NULL)  return NULL;
  opn = create_and_attach_one_plot(pr, nx, ny, data_type);
  if (opn == NULL)  return NULL;
  *op = opn;
  remove_data (pr, IS_ONE_PLOT, (void*)(pr->o_p[0]));
  return pr;
}


pltreg *create_hidden_pltreg_with_op(O_p **op, int nx, int ny, int data_type, char *name)
{
  pltreg *pr = NULL;
  O_p *opn = NULL;

  if (op == NULL)   return NULL;
  *op = NULL;
  pr = create_and_register_hidden_new_plot_project(0,   32,  900,  668, name);
  if (pr == NULL)  return NULL;
  opn = create_and_attach_one_plot(pr, nx, ny, data_type);
  if (opn == NULL)  return NULL;
  *op = opn;
  remove_data (pr, IS_ONE_PLOT, (void*)(pr->o_p[0]));
  return pr;
}

int    switch_plot(pltreg *pr, int n_plot)
{
  if (pr == NULL)        return D_O_K;

  if (n_plot != UNCHANGED && n_plot <= pr->n_op && n_plot >= 0)
    {
      pr->cur_op = n_plot;
      pr->cur_op = (pr->cur_op >= pr->n_op || pr->cur_op < 0) ? 0 : pr->cur_op;
      pr->one_p = pr->o_p[pr->cur_op];
      pr->stack = &(pr->one_p->bplt);
      pr->display_mode = IS_ONE_PLOT;
    }
  else if (n_plot >= ALL_PLOTS && n_plot <= ALL_PLOTS + 4)
    pr->display_mode = n_plot;
  return D_O_K;
}

int	get_op_number_in_pr(pltreg *pr, O_p *op)
{
  register int i, j;

  if (op == NULL || pr == NULL || pr->o_p == NULL)
    return xvin_error(Wrong_Argument);
  for (i = 0, j = - 1; i < pr->n_op && j == -1; i++)
    j = (op == pr->o_p[i]) ? i : j;
  return j;
}


/*	int add_data (pltreg *pr, int type, void *stuff);
 *	int remove_data (pltreg *pr, int type, void *stuff);
 *	DESCRIPTION
 *
 *
 *	RETURNS		0 on success, 1
 *
 */
int add_data (pltreg *pr, int type, void *stuff)
{
  return add_data_to_pltreg (pr, type, stuff);
}
int add_data_to_pltreg (pltreg *pr, int type, void *stuff)
{
  if ( pr == NULL  || stuff == NULL)	return (win_printf ("null pr pointer"));
  if (type == IS_ONE_PLOT)
    {
      if (pr->n_op ==  pr->m_op)
	{
	  pr->m_op += MAX_DATA;
	  pr->o_p = (O_p**)realloc(pr->o_p,pr->m_op*sizeof(O_p*));
	  if (pr->o_p == NULL)		return -1;
	}
      pr->o_p[pr->n_op++] = (O_p*)stuff;
    }
  else 	return add_data_to_one_plot(pr->one_p, type, stuff);
  return 0;
}

int remove_data (pltreg *pr, int type, void *stuff)
{
  register int i, j;

  if ( pr == NULL )
    {
      fprintf (stderr, "null pr pointer   \n");
      return 1;
    }
  else if (type == IS_ONE_PLOT)
    {
      for (i=0, j = -1 ; (i< pr->n_op) && (j < 0) ; i++)
	{
	  if ( (void*)(pr->o_p[i]) == stuff)	j = i;
	}
      if ( j >= 0 )
	{
	  free_one_plot(pr->o_p[j]);
	  pr->n_op--;
	  for (i=j ; i< pr->n_op ; i++)
	    pr->o_p[i] = pr->o_p[i+1];
	  pr->cur_op = (pr->cur_op >= j) ? pr->cur_op - 1 : pr->cur_op;
	  pr->cur_op = (pr->cur_op >= pr->n_op) ?	pr->n_op -1 :pr->cur_op;
	  if (pr->cur_op < 0)		pr->cur_op = (pr->n_op > 0) ? 0 : -1;
	  pr->one_p = (pr->cur_op >= 0) ? pr->o_p[pr->cur_op] : NULL;
	  pr->stack = (pr->cur_op >= 0) ? &(pr->one_p->bplt) : NULL;
	  return 0;
	}
      else	return 1;
    }
  else	return (remove_one_plot_data(pr->one_p, type, stuff));
}



d_s	*find_cur_data_set(pltreg *plt)
{
  d_s *ds = NULL;

  if (plt == NULL || plt->one_p->n_dat == 0)	return NULL;
  ds = plt->one_p->dat[plt->one_p->cur_dat];
  return (ds);
}


int	x_pltdata_2_pr(pltreg *pr, float x)
{
  int w, t;
  O_p *op = NULL;

  if (pr == NULL || pr->one_p == NULL || pr->s_nx == 0)	return 0;
  op = pr->one_p;
  w = pr->s_nx;// - pr->x_off;
  if (op->x_hi <= op->x_lo)	return pr->s_nx;
  if (x > op->x_hi)			return pr->s_nx;
  if (x < op->x_lo)			return pr->x_off;
  if (op->iopt & XLOG)
    {
      if (op->x_lo <= 0)	return pr->s_nx;
      x /= op->x_lo;
      x = w*log10(x)/log10(op->x_hi/op->x_lo);
    }
  else 		x = w * (x - op->x_lo)/(op->x_hi - op->x_lo);
  t = (int)x;
  return (pr->x_off + t);
}
int	y_pltdata_2_pr(pltreg *pr, float y)
{
  int h;
  O_p *op = NULL;

  if (pr == NULL || pr->one_p == NULL || pr->s_ny == 0)	return 0;
  op = pr->one_p;
  //h =  pr->y_off - pr->s_ny;
  h =  pr->s_ny;
  if (op->y_hi <= op->y_lo)	return pr->s_ny;
  if (y > op->y_hi)			return pr->s_ny;
  if (y < op->y_lo)			return pr->y_off;
  if (op->iopt & YLOG)
    {
      if (op->y_lo <= 0)	return pr->s_ny;
      y /= op->y_lo;
      y = h*log10(y)/log10(op->y_hi/op->y_lo);
    }
  else 		y = h * (y - op->y_lo)/(op->y_hi - op->y_lo);
  return (pr->y_off - (int)y);
}
float	x_pr_2_pltdata(pltreg *pr, int xvp)
{
  if (pr == NULL || pr->one_p == NULL || pr->s_nx == 0)	return 0;
  return (pr->one_p->ax + pr->one_p->dx * x_pr_2_pltdata_raw(pr, xvp));
}
float	x_pr_2_pltdata_raw(pltreg *pr, int xvp)
{
  float x;
  int w, t;
  O_p *op = NULL;

  if (pr == NULL || pr->one_p == NULL || pr->s_nx == 0)	return 0;
  op = pr->one_p;
  x = op->x_lo;
  t = xvp - pr->x_off;
  w = pr->s_nx;// - pr->x_off;
  if ( w == 0)	return 0;
  if (op->iopt & XLOG)
    {
      if (op->x_lo != 0)
	x *= pow(10.0,(log10(op->x_hi/op->x_lo))*(float)t/w);
    }
  else	x += (op->x_hi - op->x_lo) * (float)t/w;
  //  draw_bubble(screen, B_LEFT, 100, 100, "xoff %d s_nx %d w %d xvp %d  %d s %d x-> %g"
  //      ,pr->x_off,pr->s_nx,w,xvp,t,x);
  return (x);
}

float	y_pr_2_pltdata(pltreg *pr, int yvp)
{
  if (pr == NULL || pr->one_p == NULL || pr->s_ny == 0)	return 0;
  return (pr->one_p->ay + pr->one_p->dy * y_pr_2_pltdata_raw(pr,yvp));
}
float	y_pr_2_pltdata_raw(pltreg *pr, int yvp)
{
  float y;
  int h;
  O_p *op = NULL;
  DIALOG *d = NULL;

  if (pr == NULL || pr->one_p == NULL || pr->s_ny == 0)	return 0;
  op = pr->one_p;
  y = op->y_lo;
  d = find_dialog_associated_to_pr(pr, NULL);
  if (d == NULL) return -xvin_error(Wrong_Argument);
  //h =  pr->y_off - pr->s_ny;
  h =  pr->s_ny;// - pr->y_off;
  if ( h == 0 )	return 0;
  if (op->iopt & YLOG)
    {
      if (op->y_lo != 0)
	y *= pow(10.0,(log10(op->y_hi/op->y_lo))*(float)(pr->y_off - yvp + d->y)/h); //  pr->y_off - yvp
      //  draw_bubble(screen, B_LEFT, 100, 100, "yoff %d s_ny %d h %d yvp %d d->y %d s %d y-> %g"
      //	  ,pr->y_off,pr->s_ny,h,yvp,d->y,(pr->y_off - yvp + d->y),y);
    }
  else	y += (op->y_hi - op->y_lo) * (float)(pr->y_off -yvp + d->y)/h;// (float)(yvp - pr->y_off)/h;
  //draw_bubble(screen, B_LEFT, 100, 100, "yoff %d s_ny %d h %d yvp %d d->y %d s %d y-> %g"
  //	  ,pr->y_off,pr->s_ny,h,yvp,d->y,(pr->y_off - yvp + d->y),y);

  return (y);
}

int 	set_plt_x_unit_set(pltreg *pr, int n)
{
  register int i;

  i = n & NOT_TO_DISPLAY;
  n &= 0xff;
  set_op_x_unit_set(pr->one_p, n);
  if (i)	return 0;
  return broadcast_dialog_message(MSG_DRAW,0);
}

int 	set_plt_y_unit_set(pltreg *pr, int n)
{
  register int i;

  i = n & NOT_TO_DISPLAY;
  n &= 0xff;
  set_op_y_unit_set(pr->one_p, n);
  if (i)	return 0;
  return broadcast_dialog_message(MSG_DRAW,0);
}

d_s *find_source_specific_ds_in_pr(pltreg *pr, char *source)
{
  register int i;
  d_s *dss = NULL;

  if (pr == NULL || source == NULL)	return NULL;
  for (i = 0, dss = NULL; i < pr->n_op && dss == NULL; i++)
    dss = find_source_specific_ds_in_op(pr->o_p[i], source);
  return dss;
}

int	find_op_nb_of_source_specific_ds_in_pr(pltreg *pr, char *source)
{
  register int i;
  d_s *dss = NULL;

  if (pr == NULL || source == NULL)	return -2;
  for (i = 0, dss = NULL; i < pr->n_op && dss == NULL; i++)
    dss = find_source_specific_ds_in_op(pr->o_p[i], source);
  return (dss == NULL)  ? -1 : i-1;
}

int	find_op_nb_of_treatement_specific_ds_in_pr(pltreg *pr, char *treatement)
{
  register int i;
  d_s *dss = NULL;

  if (pr == NULL || treatement == NULL)	return -2;
  for (i = 0, dss = NULL; i < pr->n_op && dss == NULL; i++)
    dss = find_treatement_specific_ds_in_op(pr->o_p[i], treatement);
  return (dss == NULL)  ? -1 : i-1;
}


# endif /* _PLOT_REG_C_ */
