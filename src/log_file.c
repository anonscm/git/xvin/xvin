#ifndef _LOG_FILE_C_
#define _LOG_FILE_C_


# include "xvin.h"
# include "util.h"
# include "gui_def.h"
# include "xvinerr.h"
# include "log_file.h"

# include <allegro.h>
# include <stdlib.h>
# include <string.h>

# include <stdio.h>
//# include <malloc.h>
//# include <string.h>
# include <ctype.h>


char *log_filename = NULL;
char sample[128] = {0};
char author[128] = {0};


int    select_log_file(void)
{
    register int i;
    FILE *fp = NULL;
    char pathc[512] = {0}, *ext = NULL, fullfile[512] = {0}; // , *fu = NULL
    const char *fuc = NULL;
    char filec[512] = {0};  
    
    if(updating_menu_state != 0)    return D_O_K;    

    if (log_filename != NULL)    fix_filename_slashes(log_filename);
    if ((log_filename == NULL) || (extract_file_path(pathc, 512, log_filename) == NULL))
    {
        fuc = get_config_string("LOG-FILE","last_created",NULL);
        if (fuc != NULL)    
        {
	  strncpy(fullfile,fuc,sizeof(fullfile));
	  //fu = fullfile;
        }
        else
        {
            my_getcwd(fullfile, 512);
            my_strncat(fullfile,"\\",sizeof(fullfile));
            if (extract_file_name(filec, sizeof(filec), fullfile) == NULL)
                strcat(fullfile,"Untitled.tex");
            else  strcat(fullfile,filec);            
        }
    }
    else strncpy(fullfile,log_filename,sizeof(fullfile));
    switch_allegro_font(1);
    i = file_select_ex("Create log fle", fullfile, "tex;log", 512, 0, 0);
    switch_allegro_font(0);                
    if (i != 0)     
    {
        fp = fopen (fullfile, "r");
        if (fp != NULL)    
	{
	    fclose(fp);
	    i = win_printf ("Warning : File %s \n already exist!\n"
                "data will be append!",backslash_to_slash(fullfile));        
	    if (i == WIN_CANCEL)            return D_O_K;
	} 
	set_config_string("LOG-FILE","last_created",fullfile);    
        fp = fopen(fullfile,"w");
        if (fp != NULL)    
        {
            fclose (fp);
            if (log_filename != NULL)      free(log_filename);
            log_filename = strdup(fullfile);
	    ext = get_extension(log_filename);
	    if (ext != NULL && strncmp(ext,"tex",3) == 0)
	      dump_to_log_file_only("\\documentstyle[twoside,arfrench]{article}\n\n"
                      "\\input{lps1.tex}\n\\input{cmp.tex}\n\n"
                      "\\author{\\sc %s}\n\n\\title{%s}\n\n"
                      "\\begin{document}\n\\maketitle\n",author,sample);     
        }
	else return win_printf_OK("Cannot create log file\n%s",backslash_to_slash(fullfile));
    }
    return D_O_K;
}
char *grab_specific_log_file(void)
{
    register int i;
    FILE *fp = NULL;
    char pathc[512] = {0}, *ext, fullfile[512] = {0};
    char filec[512] = {0};  
    char *log_fname = NULL;
    const char *fu = NULL;
    

    if (log_filename != NULL)    fix_filename_slashes(log_filename);
    if ((log_filename == NULL) || (extract_file_path(pathc, 512, log_filename) == NULL))
    {
        fu = get_config_string("LOG-FILE","last_created",NULL);
        if (fu != NULL)    
        {
	  strncpy(fullfile,fu,sizeof(fullfile));
	  //fu = fullfile;
        }
        else
        {
            my_getcwd(fullfile, 512);
            strcat(fullfile,"\\");
            if (extract_file_name(filec, sizeof(filec), fullfile) == NULL)
                strcat(fullfile,"Untitled.tex");
            else  strcat(fullfile,filec);            
        }
    }
    else strncpy(fullfile,log_filename,sizeof(fullfile));
    switch_allegro_font(1);
    i = file_select_ex("Create specific log fle", fullfile, "tex;log", 512, 0, 0);
    switch_allegro_font(0);                
    if (i != 0)     
    {
        fp = fopen (fullfile, "r");
        if (fp != NULL)    
	{
	    fclose(fp);
	    i = win_printf ("Warning : File %s \n already exist!\n"
                "data will be append!",backslash_to_slash(fullfile));        
	    if (i == WIN_CANCEL)            return NULL;
	} 
	set_config_string("LOG-FILE","last_created",fullfile);    
        fp = fopen(fullfile,"w");
        if (fp != NULL)    
        {
            fclose (fp);
            log_fname = strdup(fullfile);
	    ext = get_extension(log_fname);
	    if (ext != NULL && strncmp(ext,"tex",3) == 0)
	      dump_to_specific_log_file_only(log_fname,"\\documentstyle[twoside,arfrench]"
                           "{article}\n\n\\input{lps1.tex}\n"
                           "\\input{cmp.tex}\n\n\\author{\\sc %s}\n\n"
                           "\\title{%s}\n\n\\begin{document}\n"
                           "\\maketitle\n",author,sample);
	    return log_fname;
        }
	else
	{ 
	    win_printf_OK("Cannot create log file\n%s",backslash_to_slash(fullfile));
	    return NULL; 
	}
    }
    return log_fname;
}
char *grab_specific_sub_log_file(char *main_logfile)
{
    register int i;
    FILE *fp = NULL;
    char *path, pathc[512] = {0}, fullfile[512] = {0};
    char filec[512] = {0};  
    char *log_fname = NULL;
    const char  *fu = NULL;
    
    if (main_logfile != NULL)    main_logfile = log_filename;
    if (main_logfile != NULL)    fix_filename_slashes(main_logfile);
    if ((main_logfile == NULL) || (extract_file_path(pathc, 512, main_logfile) == NULL))
    {
        fu = get_config_string("LOG-FILE","last_created",NULL);
        if (fu != NULL)    
        {
	  strncpy(fullfile,fu,sizeof(fullfile));
	  //fu = fullfile;
        }
        else
        {
            my_getcwd(fullfile, 512);
            strcat(fullfile,"\\");
            if (extract_file_name(filec, 256, fullfile) == NULL)
                strcat(fullfile,"Untitled.tex");
            else  strcat(fullfile,filec);            
        }
    }
    else strncpy(fullfile,main_logfile,sizeof(fullfile));
    switch_allegro_font(1);
    i = file_select_ex("Create sub log fle", fullfile, "tex;log", 512, 0, 0);
    switch_allegro_font(0);                
    if (i != 0)     
    {
        fp = fopen (fullfile, "r");
        if (fp != NULL)    
	{
	    fclose(fp);
	    i = win_printf ("Warning : File %s \n already exist!\n"
                "data will be append!",backslash_to_slash(fullfile));        
	    if (i == WIN_CANCEL)            return NULL;
	} 
	set_config_string("LOG-FILE","last_created",fullfile);    
        fp = fopen(fullfile,"w");
        if (fp != NULL)    
        {
            fclose (fp);
            log_fname = strdup(fullfile);

	    path = extract_file_path(pathc, 256, main_logfile);
	    if (path != NULL)
	      {
		backslash_to_slash(path);    
		if (strstr(log_fname,path) != NULL)
		  {    /* relative path */
		    for (i = strlen(path); i >= 0 && log_fname[i] != '/'; i--); 
		    snprintf(pathc,512,"%s",log_fname+i+1);
		  }
		else snprintf(pathc,512,"%s",log_fname);
	      }
	    else strncpy(pathc,log_fname,512);
	    dump_to_specific_log_file_only(main_logfile,"\\input{%s}\n\n",pathc);
	    return log_fname;
        }
	else
	{ 
	    win_printf_OK("Cannot create log file\n%s",backslash_to_slash(fullfile));
	    return NULL; 
	}
    }
    return log_fname;
}


int    dump_to_log_file_with_date_and_time(const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char s[256] = {0};
    struct tm *t = NULL;
    time_t ti;
    char c[2048] = {0};
    
    
    
    if (log_filename == NULL)
    {
        if (select_log_file() == WIN_CANCEL)    return 1;
    }
    if (fmt == NULL)    return 1;
    ti = time(0);
    t = localtime(&ti);
    strftime(s,256,"%d/%m/%Y %H:%M:%S",t);
    fp = fopen(log_filename,"a");
    if (fp == NULL)                return 1;
    fprintf(fp,"%s\n",s);    
    va_start(ap, fmt);
    vsnprintf(c,2048,fmt,ap);
    va_end(ap);    
    fprintf(fp,"%s\n",c);            
    return fclose(fp);    
} 
int    dump_to_log_file_with_time(const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char s[256] = {0};
    struct tm *t = NULL;
    time_t ti;
    char c[2048] = {0};
    
    
    
    if (log_filename == NULL)
    {
        if (select_log_file() == WIN_CANCEL)    return 1;
    }
    if (fmt == NULL)    return 1;
    ti = time(0);
    t = localtime(&ti);
    strftime(s,sizeof(s),"%X",t);
    fp = fopen(log_filename,"a");
    if (fp == NULL)                    return 1;
    fprintf(fp,"%s\n",s);    
    va_start(ap, fmt);
    vsnprintf(c,sizeof(c),fmt,ap);
    va_end(ap);    
    fprintf(fp,"%s\n",c);            
    return fclose(fp);    
} 
int    dump_to_log_file_only(const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char c[2048] = {0};
        
    if (log_filename == NULL)    
    {
        if (select_log_file() == WIN_CANCEL)    return 1;
    }    
    if (fmt == NULL)    return 1;
    fp = fopen(log_filename,"a");
    if (fp == NULL)                    return 1;
    va_start(ap, fmt);
    vsnprintf(c,sizeof(c),fmt,ap);
    va_end(ap);
    fprintf(fp,"%s\n",c);        
    return fclose(fp);    
}     

int    dump_to_specific_log_file_with_date_and_time(char *file, const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char s[256] = {0};
    struct tm *t = NULL;
    time_t ti;
    char c[2048] = {0};
    
    if (fmt == NULL || file == NULL)    return 1;
    ti = time(0);
    t = localtime(&ti);
    strftime(s,sizeof(s),"%d/%m/%Y %H:%M:%S",t);
    fp = fopen(file,"a");
    if (fp == NULL)                return 1;
    fprintf(fp,"%s\n",s);    
    va_start(ap, fmt);
    vsnprintf(c,sizeof(c),fmt,ap);
    va_end(ap);    
    fprintf(fp,"%s\n",c);            
    return fclose(fp);    
} 
int    dump_to_specific_log_file_with_time(char *file, const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char s[256] = {0};
    struct tm *t = NULL;
    time_t ti;
    char c[2048] = {0};
    
    if (fmt == NULL || file == NULL)    return 1;
    ti = time(0);
    t = localtime(&ti);
    strftime(s,sizeof(s),"%X",t);
    fp = fopen(file,"a");
    if (fp == NULL)                return 1;
    fprintf(fp,"%s\n",s);    
    va_start(ap, fmt);
    vsnprintf(c,sizeof(c),fmt,ap);
    va_end(ap);    
    fprintf(fp,"%s\n",c);            
    return fclose(fp);    
} 
int    dump_to_specific_log_file_only(char *file, const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char c[2048] = {0};
        
    if (fmt == NULL || file == NULL)    return 1;
    fp = fopen(file,"a");
    if (fp == NULL)                return 1;
    va_start(ap, fmt);
    vsnprintf(c,sizeof(c),fmt,ap);
    va_end(ap);
    fprintf(fp,"%s\n",c);        
    return fclose(fp);    
}     

#endif
