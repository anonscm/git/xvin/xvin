# ifndef _BOX2ALFN_C_ 
# define _BOX2ALFN_C_ 

# include <stdlib.h>
# include <string.h>

# include "xvin/platform.h"
# include "box2alfn.h"
# include "xv_main.h"
# include "color.h"
# include "plot2box.h"

int		wr_box(struct box *b0);
int		wr_char(struct box *b0);
int		wr_char_90(struct box *b0);
XV_VAR(int, screen_acquired);

extern short int   	last_pt_size;
extern int 	last_color;
extern BITMAP *dev_bitmap;
extern int y_offset;

int		ttf_enable = 0;
int		ttf_init_state = 0;
char 	debug_ttf[256];
ALFONT_FONT *user_font = NULL;

extern int 	x_b, y_b;
static int	A_d, p_d, A_h;  
extern	short int 	*new_t;

int	ttf_init(void)
{
	char fontfile[384]  = {0};
  /* now init AllegroFont */
  if (alfont_init() != ALFONT_OK) {
    for(;screen_acquired;); // we wait for screen_acquired back to 0; 
    screen_acquired = 1;
    acquire_bitmap(screen);
    textout_ex(screen, font, "all font init went wrong", 16, 8, makecol(255,255,255), makecol(0,0,0));
    release_bitmap(screen);
    screen_acquired = 0;
    ttf_enable = 0;
    return 1;
  }

	snprintf(fontfile,384,"%sarial.ttf",prog_dir);

  /* try to load the font given by the parameter*/
  user_font = alfont_load_font(fontfile);
  if (user_font == NULL)   user_font = alfont_load_font(TTFFONT1);
  if (user_font == NULL) {
    acquire_bitmap(screen);
    textout_ex(screen, font, "could not load ttf", 100, 8, makecol(255,255,255),makecol(0,0,0));
    release_bitmap(screen);    
    ttf_enable = 0;
    return 2;
  }
	ttf_init_state = 1;
	alfont_text_mode(-1);
	p_d = (short int)box_font.uch[box_font.new_t[112]+2];
	A_d = (short int)box_font.uch[box_font.new_t[65]+2];
	A_h = (short int)box_font.uch[box_font.new_t[65]+1];	
	
/*	textout(screen, font, "all font init OK", 16, SCREEN_H-20, makecol(255,255,255));	*/
	return 0;
}

int draw_ttf_char(struct box *b0)
{
	register int i;
	int xm = 0, ym = 0, h_pix;
	static int bg = 0;
	char ch[2] = {0};

	if (b0 == NULL) return 1;
	bg = (bg == Darkgray) ? Lightgray : Darkgray;
	
	if (b0->char_val == 0)
	{
		i = ttf_enable;
		ttf_enable = 0;
		wr_char(b0);
		ttf_enable = i;
		return	0;
	}
	if (ttf_init_state == 0) 	
	{
		if (ttf_init()) 	return	wr_char(b0);
	}
	x_b += b0->xc;
	y_b += b0->yc;
	xm = (2*x_b)/pc_fen;
	ym = (2*y_b)/pc_fen;
	i = 1+b0->pt/2;
	(*q_dev.move)(xm, ym);	
	ym = (2*(y_b+b0->d))/pc_fen;
	if ( i != last_pt_size )
	{
		(*q_dev.line_size)(i);		
		last_pt_size = i;
	}
	if ( b0->color != last_color )
	{
		(*q_dev.color)(b0->color,0);
		last_color = b0->color;
	}
	/* font height  bases on A*/
	h_pix = (3*b0->pt*(p_d+A_h))/16;
	h_pix = (5*h_pix)/(2*pc_fen);
	ym = y_b - (int)(3*b0->pt*A_d)/16;
	ym = 2*ym/pc_fen;
	alfont_set_font_size(user_font, h_pix);
	ch[0] = b0->char_val;
	ch[1] = 0;
	
//	alfont_charout_ex(dev_bitmap, user_font, ch, xm, y_offset -ym,dev_color,bg);
	alfont_charout(dev_bitmap, user_font, ch, xm, y_offset -ym,dev_color);
	
	x_b -= b0->xc;
	y_b -= b0->yc;
	(*q_dev.move)((2*x_b)/pc_fen, (2*y_b)/pc_fen);	
	return 0;
}


int draw_ttf_char_90(struct box *b0)
{
	register int i;
	int xm = 0, ym = 0, h_pix;
	char ch[2] = {0};

	if (b0 == NULL) return 1;
	if (b0->char_val == 0) 	
	{
		i = ttf_enable;
		ttf_enable = 0;
		wr_char_90(b0);
		ttf_enable = i;
		return	0;		
	}
	if (ttf_init_state == 0) 	
	{
		if (ttf_init()) 	return	wr_char_90(b0);
	}
	x_b -= b0->yc;
	y_b += b0->xc;
	xm = (2*x_b)/pc_fen;
	ym = (2*y_b)/pc_fen;
	i = 1+b0->pt/2;
	(*q_dev.move)(xm, ym);	
	if ( i != last_pt_size )
	{
		(*q_dev.line_size)(i);		
		last_pt_size = i;
	}
	if ( b0->color != last_color )
	{
		(*q_dev.color)(b0->color,0);
		last_color = b0->color;
	}
	/* font height  bases on A*/	
	
	h_pix = (3*b0->pt*(p_d+A_h))/16;
	h_pix = (5*h_pix)/(2*pc_fen);

	alfont_set_font_size(user_font, h_pix);
	xm = x_b + (int)((3*b0->pt*A_d)/16);
	xm = 2*xm/pc_fen;
	ch[0] = b0->char_val;
	ch[1] = 0;
	alfont_charout_90(dev_bitmap, user_font, ch, xm, y_offset -ym,dev_color);
/*	
	x1 = x_b - b0->h;	x2 = x_b + b0->d; 
	y1 = y_b;	y2 = y_b + b0->w;
	y1 = 2*y1/pc_fen;	y2 = 2*y2/pc_fen;
	x1 = 2*x1/pc_fen;	x2 = 2*x2/pc_fen;	
	rect(dev_bitmap, x1, y_offset - y1, x2, y_offset - y2, dev_color);*/
	x_b += b0->yc;
	y_b -= b0->xc;
	(*q_dev.move)((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	return 0;
}
int give_ttf_char_width(struct box *b0)
{
	int h_pix, w;
	char ch[2] = {0};
	
	if (b0 == NULL) return 1;
	if (ttf_init_state && b0->char_val) 
	{	
		h_pix = (3*b0->pt*(p_d+A_h))/16;
		h_pix = (5*h_pix)/(2*pc_fen);
		alfont_set_font_size(user_font, h_pix);
		ch[0] = b0->char_val;
		ch[1] = 0;
		w = alfont_char_width(user_font, ch);
		w = ((w)*pc_fen+(pc_fen/2))/2; /* w+1 */
	}
	else
	{
		w = (int)(3*b0->pt*(int)box_font.uch[b0->n_char])/16;
		w += (3*b0->pt*b0->slx*(int)box_font.ch[b0->n_char+1])/(16*b0->sly);
		w += (3*b0->pt*b0->slx*(int)box_font.ch[b0->n_char+2])/(16*b0->sly);
	}
	return w;	
	
}

int give_ttf_char_space(struct box *b0)
{
	if (b0 == NULL) return 1;
	return (ttf_init_state && b0->char_val) ? 0 : 1;
}


int ps_draw_char(struct box *b0)
{
	register int i;
	float xm = 0, ym = 0;
	int h_pix;
	static int bg = 0;
	char ch[3] = {0};

	if (b0 == NULL) return 1;
	bg = (bg == Darkgray) ? Lightgray : Darkgray;
	
	if (b0->char_val == 0)
	{
		i = ttf_enable;
		ttf_enable = 0;
		wr_char(b0);
		ttf_enable = i;
		return	0;
	}

	x_b += b0->xc;
	y_b += b0->yc;
	xm = (2*x_b)/pc_fen;
	ym = (2*y_b)/pc_fen;
	i = 1+b0->pt/2;
	(*q_dev.move)(xm, ym);	
	ym = (2*(y_b+b0->d))/pc_fen;
	if ( i != last_pt_size )
	{
		(*q_dev.line_size)(i);		
		last_pt_size = i;
	}
	if ( b0->color != last_color )
	{
		(*q_dev.color)(b0->color,0);
		last_color = b0->color;
	}
	/* font height  bases on A*/
	h_pix = (3*b0->pt*(p_d+A_h))/16;
	h_pix = (5*h_pix)/(2*pc_fen);
	ym = y_b - (int)(3*b0->pt*A_d)/16;
	ym = 2*ym/pc_fen;
	q_dev.move(xm-h_pix/16, ym);	
	fprintf (qd_fp,"/Helvetica findfont\n%d scalefont setfont\n",(int)(0.95*h_pix));
	i = 0;
 	if (b0->char_val == '(' || b0->char_val == ')') ch[i++] = '\\';
	ch[i++] = b0->char_val;
	ch[i++] = 0;
	
	//	alfont_charout(dev_bitmap, user_font, ch, xm, y_offset -ym,dev_color);
	fprintf (qd_fp,"(%s) show\n",ch);
	x_b -= b0->xc;
	y_b -= b0->yc;
	(*q_dev.move)((2*x_b)/pc_fen, (2*y_b)/pc_fen);	
	return 0;
}

int ps_draw_char_90(struct box *b0)
{
	register int i;
	float xm = 0, ym = 0;
	int h_pix;	
	char ch[3] = {0};

	if (b0 == NULL) return 1;
	if (b0->char_val == 0) 	
	{
		i = ttf_enable;
		ttf_enable = 0;
		wr_char_90(b0);
		ttf_enable = i;
		return	0;		
	}
	if (ttf_init_state == 0) 	
	{
		if (ttf_init()) 	return	wr_char_90(b0);
	}
	x_b -= b0->yc;
	y_b += b0->xc;
	xm = (2*x_b)/pc_fen;
	ym = (2*y_b)/pc_fen;
	i = 1+b0->pt/2;
	(*q_dev.move)(xm, ym);	
	if ( i != last_pt_size )
	{
		(*q_dev.line_size)(i);		
		last_pt_size = i;
	}
	if ( b0->color != last_color )
	{
		(*q_dev.color)(b0->color,0);
		last_color = b0->color;
	}
	/* font height  bases on A*/	
	
	h_pix = (3*b0->pt*(p_d+A_h))/16;
	h_pix = (5*h_pix)/(2*pc_fen);



	//	alfont_set_font_size(user_font, h_pix);
	xm = x_b + (int)((3*b0->pt*A_d)/16);
	xm = 2*xm/pc_fen;

	q_dev.move(xm, ym);	
	fprintf (qd_fp,"/Helvetica findfont\n%d scalefont setfont\n90 rotate\n",(int)(0.95*h_pix));

	i = 0;
	if (b0->char_val == '(' || b0->char_val == ')') ch[i++] = '\\';
	ch[i++] = b0->char_val;
	ch[i++] = 0;
	fprintf (qd_fp,"(%s) show\n-90 rotate\n",ch);
	//	alfont_charout_90(dev_bitmap, user_font, ch, xm, y_offset -ym,dev_color);
/*	
	x1 = x_b - b0->h;	x2 = x_b + b0->d; 
	y1 = y_b;	y2 = y_b + b0->w;
	y1 = 2*y1/pc_fen;	y2 = 2*y2/pc_fen;
	x1 = 2*x1/pc_fen;	x2 = 2*x2/pc_fen;	
	rect(dev_bitmap, x1, y_offset - y1, x2, y_offset - y2, dev_color);*/
	x_b += b0->yc;
	y_b -= b0->xc;
	(*q_dev.move)((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	return 0;
}



#endif  




