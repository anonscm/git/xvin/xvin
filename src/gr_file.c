# ifndef _GR_FILE_C_
# define _GR_FILE_C_


# include <stdlib.h>
# include <string.h>

# include "allegro.h"
# include "gr_file.h"
# include "xvin.h"
# include "xvinerr.h"
# include "xvin/platform.h"
/* # include "allegro.h"  just for textout */


/*	int get_label(char **c1, char **c2, char *line)
 *	DESCRIPTION	
 *
 *	RETURNS		If > 0, the number of char read, < 0 error
 *			
 */
int get_label(char **c1, char **c2, char *line)
{
	register int j = 0;
	int  k = 0, out_loop = 1;
	char  ch = '"', last_ch = 0;

	if (c1 == NULL || c2 == NULL || line == NULL)  return -2;

	(*c1)++;
	while ( out_loop ) /* looking for label end */
	{
		if((*c1)[j] == 0)	/* label extend to next line */
		{
			if (((*c1) = get_next_line(line)) == NULL )
			{
				error_in_file("EOF reached before label ended");
				k = -1;
				out_loop = 0;
				return k;
			}
			j=0;
		}
		if ( (*c1)[j] == 0) 				out_loop = 0;
		else if ( ((*c1)[j] == ch) && (last_ch != 92))	out_loop = 0;
		else
		{
			last_ch = (*c1)[j];
			(*c2)[k++] = (*c1)[j++];
		}
		if ( k >= B_LINE )
		{
			error_in_file("this label is too long !...\n%s",(*c2));
			k = -1;
			out_loop = 0;
			return k;
		}
	}
	(*c2)[k]=0;
	(*c1) = (*c1)+j+1;
	return (k);
}
/*	get_next_line(line)
 *	DESCRIPTION	
 *			
 *			
 *
 *	RETURNS		0 on success, 1 
 *			
 */
char *get_next_line(char *line)
{
	char *c = NULL;
	int get_out = 0;
	
	do
	{
		get_out = 0;
		i_f[cur_i_f].n_line++;
		c = fgets(line,B_LINE,i_f[cur_i_f].fpi);
		if ( c == NULL)			get_out = 0;
		else if (strlen(c) == 1)	// added 2005-10-04, to compensate from MacOS behavior
			{	if (c[0]==10) 	get_out = 1;	// only if a blank line is encountered we continue
				else			return(NULL);	// any other single character announces the binary region
			}
		else if (c[0]==26) return(NULL); // added 2006-03-04, for Linux
	} while(get_out);
	return(c); 
}

int error_in_file( char *fmt, ...)
{
	register int i, j;
	va_list ap;	
	char f_error[1024] = {0}, f_tex[128] = {0}, *c, s[1024] = {0};
	
	c = i_f[cur_i_f].filename;
	for (i= 0, j = 0 ; c[i] != 0 && j < 127 ; i++, j++)
	{
		f_tex[j] = c[i];
		if (c[i] == 92) 	f_tex[j] = '/';
		if (c[i] == '_') 	
		{
			f_tex[j] = 92;		
			f_tex[j] = '_';					
		}
		if (c[i] == '^') 	
		{
			f_tex[j] = 92;		
			f_tex[j] = '^';					
		}		
		
	}
	f_tex[i] = 0;
	va_start(ap, fmt);
	i = vsnprintf(s,1024,fmt,ap);
	va_end(ap);			
	snprintf (f_error, 1024, "Error in %s\nat line %d\n%s",f_tex,i_f[cur_i_f].n_line,s);
	/* i = win_ti_printf("Data loading","%s",f_error);	*/
	win_ti_printf("Data loading","%s",f_error);	
	/*textout(screen, font, f_error, 16, 16 + 10 * n_error, makecol(255,255,255));*/
	dump_to_xv_log_file_with_date_and_time("%s",f_error);
	n_error++;
	if ( n_error > MAX_ERROR)
	{
		snprintf (f_error,1024,"too many errors %d...!\nimage loading aborted!..",
			n_error-1);
		dump_to_xv_log_file_with_date_and_time("%s",f_error);
/*		textout(screen, font, f_error, 16, 16 + 10 * n_error, makecol(255,255,255));*/
	/*
		win_ti_printf("Data loading","too many errors %d...!\nimage loading aborted!..",n_error-1);
	if (i  == WIN_CANCEL)		n_error = MAX_ERROR;
	*/
	}
	return n_error;
}


int gr_numb(float *np, int *argcp, char ***argvp)
{
	register int i;

	if (*argcp <= 1)		return(0);
	i = sscanf((*argvp)[1],"%f",np);
	(*argcp)--;
	(*argvp)++;
	return(i);
}
int gr_numbi(int *np, int *argcp, char ***argvp)
{
	register int i;

	if (*argcp <= 1)		return(0);
	i = sscanf((*argvp)[1],"%d",np);
	(*argcp)--;
	(*argvp)++;
	return(i);
}

# endif
