# ifndef _COLOR_C_
# define _COLOR_C_

#include "color.h"
# include "xvin/platform.h"




int	Black			=	0;
int	Blue			= BUILD_COLOR(0,  0, 128);
int	Green			= BUILD_COLOR(0, 128,  0);
int	Cyan			= BUILD_COLOR(0, 128, 128);
int	Red				= BUILD_COLOR(128,  0,  0);
int	Magenta			= BUILD_COLOR(128,  0, 128);
int	Brown			= BUILD_COLOR(128, 128,  0);
int	Lightgray		= BUILD_COLOR(171, 171, 171);
int	Darkgray		= BUILD_COLOR(85, 85, 85);
int	Lightblue		= BUILD_COLOR(64, 64, 255);
int	Lightgreen		= BUILD_COLOR(64, 255, 64);
int	Lightcyan		= BUILD_COLOR(0, 255, 255);
int	Lightred		= BUILD_COLOR(255, 64, 64);
int	Lightmagenta	= BUILD_COLOR(255,  0, 255);
int	Yellow			= BUILD_COLOR(255, 255,  0);
int	White 			= BUILD_COLOR(255, 255, 255);


int aliceblue = BUILD_COLOR(240, 248, 255);
int antiquewhite = BUILD_COLOR(250, 235, 215);
int aqua = BUILD_COLOR(0, 255, 255);
int aquamarine = BUILD_COLOR(127, 255, 212);
int azure = BUILD_COLOR(240, 255, 255);
int beige = BUILD_COLOR(245, 245, 220);
int bisque = BUILD_COLOR(255, 228, 196);
int black = BUILD_COLOR(0, 0, 0);
int blanchedalmond = BUILD_COLOR(255, 235, 205);
int blue = BUILD_COLOR(0, 0, 255);
int blueviolet = BUILD_COLOR(138, 43, 226);
int brown = BUILD_COLOR(165, 42, 42);
int burlywood = BUILD_COLOR(222, 184, 135);
int cadetblue = BUILD_COLOR(95, 158, 160);
int chartreuse = BUILD_COLOR(127, 255, 0);
int chocolate = BUILD_COLOR(210, 105, 30);
int coral = BUILD_COLOR(255, 127, 80);
int cornflowerblue = BUILD_COLOR(100, 149, 237);
int cornsilk = BUILD_COLOR(255, 248, 220);
int crimson = BUILD_COLOR(220, 20, 60);
int cyan = BUILD_COLOR(0, 255, 255);
int darkblue = BUILD_COLOR(0, 0, 139);
int darkcyan = BUILD_COLOR(0, 139, 139);
int darkgoldenrod = BUILD_COLOR(184, 134, 11);
int darkgray = BUILD_COLOR(169, 169, 169);
int darkgreen = BUILD_COLOR(0, 100, 0);
int darkgrey = BUILD_COLOR(169, 169, 169);
int darkkhaki = BUILD_COLOR(189, 183, 107);
int darkmagenta = BUILD_COLOR(139, 0, 139);
int darkolivegreen = BUILD_COLOR(85, 107, 47);
int darkorange = BUILD_COLOR(255, 140, 0);
int darkorchid = BUILD_COLOR(153, 50, 204);
int darkred = BUILD_COLOR(139, 0, 0);
int darksalmon = BUILD_COLOR(233, 150, 122);
int darkseagreen = BUILD_COLOR(143, 188, 143);
int darkslateblue = BUILD_COLOR(72, 61, 139);
int darkslategray = BUILD_COLOR(47, 79, 79);
int darkslategrey = BUILD_COLOR(47, 79, 79);
int darkturquoise = BUILD_COLOR(0, 206, 209);
int darkviolet = BUILD_COLOR(148, 0, 211);
int deeppink = BUILD_COLOR(255, 20, 147);
int deepskyblue = BUILD_COLOR(0, 191, 255);
int dimgray = BUILD_COLOR(105, 105, 105);
int dimgrey = BUILD_COLOR(105, 105, 105);
int dodgerblue = BUILD_COLOR(30, 144, 255);
int firebrick = BUILD_COLOR(178, 34, 34);
int floralwhite = BUILD_COLOR(255, 250, 240);
int forestgreen = BUILD_COLOR(34, 139, 34);
int fuchsia = BUILD_COLOR(255, 0, 255);
int gainsboro = BUILD_COLOR(220, 220, 220);
int ghostwhite = BUILD_COLOR(248, 248, 255);
int gold = BUILD_COLOR(255, 215, 0);
int goldenrod = BUILD_COLOR(218, 165, 32);
int gray = BUILD_COLOR(128, 128, 128);
int grey = BUILD_COLOR(128, 128, 128);
int green = BUILD_COLOR(0, 128, 0);
int greenyellow = BUILD_COLOR(173, 255, 47);
int honeydew = BUILD_COLOR(240, 255, 240);
int hotpink = BUILD_COLOR(255, 105, 180);
int indianred = BUILD_COLOR(205, 92, 92);
int indigo = BUILD_COLOR(75, 0, 130);
int ivory = BUILD_COLOR(255, 255, 240);
int khaki = BUILD_COLOR(240, 230, 140);
int lavender = BUILD_COLOR(230, 230, 250);
int lavenderblush = BUILD_COLOR(255, 240, 245);
int lawngreen = BUILD_COLOR(124, 252, 0);
int lemonchiffon = BUILD_COLOR(255, 250, 205);
int lightblue = BUILD_COLOR(173, 216, 230);
int lightcoral = BUILD_COLOR(240, 128, 128);
int lightcyan = BUILD_COLOR(224, 255, 255);

/* lightgoldenrod*/
int yellow = BUILD_COLOR(250, 250, 210);
int lightgray = BUILD_COLOR(211, 211, 211);
int lightgreen = BUILD_COLOR(144, 238, 144);
int lightgrey = BUILD_COLOR(211, 211, 211);
int lightpink = BUILD_COLOR(255, 182, 193);
int lightsalmon = BUILD_COLOR(255, 160, 122);
int lightseagreen = BUILD_COLOR(32, 178, 170);
int lightskyblue = BUILD_COLOR(135, 206, 250);
int lightslategray = BUILD_COLOR(119, 136, 153);
int lightslategrey = BUILD_COLOR(119, 136, 153);
int lightsteelblue = BUILD_COLOR(176, 196, 222);
int lightyellow = BUILD_COLOR(255, 255, 224);
int lime = BUILD_COLOR(0, 255, 0);
int limegreen = BUILD_COLOR(50, 205, 50);
int linen = BUILD_COLOR(250, 240, 230);
int magenta = BUILD_COLOR(255, 0, 255);
int maroon = BUILD_COLOR(128, 0, 0);
int mediumaquamarine = BUILD_COLOR(102, 205, 170);
int mediumblue = BUILD_COLOR(0, 0, 205);
int mediumorchid = BUILD_COLOR(186, 85, 211);
int mediumpurple = BUILD_COLOR(147, 112, 219);
int mediumseagreen = BUILD_COLOR(60, 179, 113);
int mediumslateblue = BUILD_COLOR(123, 104, 238);
int mediumspringgreen = BUILD_COLOR(0, 250, 154);
int mediumturquoise = BUILD_COLOR(72, 209, 204);
int mediumvioletred = BUILD_COLOR(199, 21, 133);
int midnightblue = BUILD_COLOR(25, 25, 112);
int mintcream = BUILD_COLOR(245, 255, 250);
int mistyrose = BUILD_COLOR(255, 228, 225);
int moccasin = BUILD_COLOR(255, 228, 181);
int navajowhite = BUILD_COLOR(255, 222, 173);
int navy = BUILD_COLOR(0, 0, 128);
int oldlace = BUILD_COLOR(253, 245, 230);
int olive = BUILD_COLOR(128, 128, 0);
int olivedrab = BUILD_COLOR(107, 142, 35);
int orange = BUILD_COLOR(255, 165, 0);
int orangered = BUILD_COLOR(255, 69, 0);
int orchid = BUILD_COLOR(218, 112, 214);
int palegoldenrod = BUILD_COLOR(238, 232, 170);
int palegreen = BUILD_COLOR(152, 251, 152);
int paleturquoise = BUILD_COLOR(175, 238, 238);
int palevioletred = BUILD_COLOR(219, 112, 147);
int papayawhip = BUILD_COLOR(255, 239, 213);
int peachpuff = BUILD_COLOR(255, 218, 185);
int peru = BUILD_COLOR(205, 133, 63);
int pink = BUILD_COLOR(255, 192, 203);
int plum = BUILD_COLOR(221, 160, 221);
int powderblue = BUILD_COLOR(176, 224, 230);
int purple = BUILD_COLOR(128, 0, 128);
int red = BUILD_COLOR(255, 0, 0);
int rosybrown = BUILD_COLOR(188, 143, 143);
int royalblue = BUILD_COLOR(65, 105, 225);
int saddlebrown = BUILD_COLOR(139, 69, 19);
int salmon = BUILD_COLOR(250, 128, 114);
int sandybrown = BUILD_COLOR(244, 164, 96);
int seagreen = BUILD_COLOR(46, 139, 87);
int seashell = BUILD_COLOR(255, 245, 238);
int sienna = BUILD_COLOR(160, 82, 45);
int silver = BUILD_COLOR(192, 192, 192);
int skyblue = BUILD_COLOR(135, 206, 235);
int slateblue = BUILD_COLOR(106, 90, 205);
int slategray = BUILD_COLOR(112, 128, 144);
int slategrey = BUILD_COLOR(112, 128, 144);
int snow = BUILD_COLOR(255, 250, 250);
int springgreen = BUILD_COLOR(0, 255, 127);
int steelblue = BUILD_COLOR(70, 130, 180);
/*int tan = BUILD_COLOR(210, 180, 140);*/
int teal = BUILD_COLOR(0, 128, 128);
int thistle = BUILD_COLOR(216, 191, 216);
int tomato = BUILD_COLOR(255, 99, 71);
int turquoise = BUILD_COLOR(64, 224, 208);
int violet = BUILD_COLOR(238, 130, 238);
int wheat = BUILD_COLOR(245, 222, 179);
int white = BUILD_COLOR(255, 255, 255);
int whitesmoke = BUILD_COLOR(245, 245, 245);
/* int yellow = BUILD_COLOR(255, 255, 0); */
int yellowgreen = BUILD_COLOR(154, 205, 50);


int win_scanf(const char *fmt, ...);

int color_chooser(void)
{
    static int color = 0;
    int ret = 0;
    ret = win_scanf("%R Yellow\n %r Green\n%r Blue\n %r Red\n %r Cyan \n %r Magenta \n %r White \n %r Brown", &color);

    if (ret != 128)
    {
        return -1;
    }

    if (color == 0)
    {
        color = Yellow;
    }
    else if (color == 1)
    {
        color = Lightgreen;
    }
    else if (color == 2)
    {
        color = Lightblue;
    }
    else if (color == 3)
    {
        color = Lightred;
    }
    else if (color == 4)
    {
        color = Lightcyan;
    }
    else if (color == 5)
    {
        color = Lightmagenta;
    }
    else if (color == 6)
    {
        color = White;
    }
    else
    {
        color = Brown;
    }

    return color;
}





# endif
