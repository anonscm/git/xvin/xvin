/*
 *	DESCRIPTION	xvin plot functions related to unit sets
 *
 */
#ifndef _UNITSET_C_
#define _UNITSET_C_


# include "xvin/platform.h"
# include "unitset.h"
# include "xvinerr.h"
# include "util.h"

# include <stdlib.h>
# include <string.h>


/* un_s	*to_modify_un = NULL; */

un_s *build_unit_set(int type, float ax, float dx, char decade, char mode, char *name)
{
	un_s *uns = NULL;

	uns = (un_s*)calloc(1,sizeof(un_s));
	if ( uns == NULL )		return NULL;
	uns->type = type;
	uns->ax = ax;
	uns->dx = dx;
	uns->decade = decade;
	uns->mode = mode;
	if (name == NULL)      uns->name = NULL;
	else if (name != NULL && strncmp(name,"no_name",7) == 0 )
	  uns->name = NULL;
	else	uns->name = Mystrdup(name);
	return uns;
}
int 	free_unit_set(un_s *uns)
{
	if (uns == NULL)	return 1;
	if (uns->name != NULL) 	free(uns->name);
	free(uns);	uns = NULL;
	return 0;
}

un_s *duplicate_unit_set(un_s *src, un_s *dest)
{
	if (dest == NULL)
	{
		dest = (un_s*)calloc(1,sizeof(un_s));
		if ( dest == NULL )		return NULL;
	}
	else if (dest->name != NULL)		free(dest->name);
	*dest = *src;
	dest->name = Mystrdup(src->name);
	return dest;
}

char	*generate_units(int type, int decade)
{
    char unit[32] = {0};

	unit[0] = 0;
	switch (decade)
	{
	case IS_TERRA :		        strncpy(unit,"T",sizeof(unit));	        break;
		case IS_GIGA :		strncpy(unit,"G",sizeof(unit));	        break;
		case IS_MEGA :		strncpy(unit,"M",sizeof(unit));	        break;
		case IS_KILO :		strncpy(unit,"K",sizeof(unit));	        break;
		case 0 :					                break;
		case IS_MILLI :		strncpy(unit,"m",sizeof(unit));	        break;
		case IS_MICRO :		strncpy(unit,"\\mu ",sizeof(unit));	break;
		case IS_NANO : 		strncpy(unit,"n",sizeof(unit));	        break;
		case IS_PICO :		strncpy(unit,"p",sizeof(unit));	        break;
		case IS_FEMTO :		strncpy(unit,"f",sizeof(unit));	        break;
		default :		return NULL;		                break;
	};
	switch (type)
	{
		case IS_VOLT :		my_strncat(unit,"V",sizeof(unit));	break;
		case IS_AMPERE :	my_strncat(unit,"A",sizeof(unit));	break;
		case IS_METER :		my_strncat(unit,"m",sizeof(unit));	break;
		case IS_NEWTON :	my_strncat(unit,"N",sizeof(unit));	break;
		case IS_SECOND :	my_strncat(unit,"s",sizeof(unit));	break;
		case IS_GRAMME :	my_strncat(unit,"g",sizeof(unit));	break;
		case IS_GAUSS :		my_strncat(unit,"G",sizeof(unit));	break;
		case IS_OHM :		my_strncat(unit,"\\Omega ",sizeof(unit));	break;
		default :		return NULL;		                break;
	};
	return strdup(unit);
}
int	unit_to_type(char *unit, int *type, int *decade)
{
	register int t, d;
	int adv = 1;

	if (unit == NULL)	return 1;
	if (strncmp(unit,"\\mu ",4) == 0 )
	{
		d = IS_MICRO;
		unit += 4;
	}
	else
	{
		switch (unit[0])
		{
			case 'T'  :		d = IS_TERRA;		break;
			case 'G' :		d = IS_GIGA;		break;
			case 'M' :		d = IS_MEGA;		break;
			case 'K' :		d = IS_KILO;		break;
			case 'm' :		d = IS_MILLI;		break;
			case 'n' : 		d = IS_NANO;		break;
			case 'p' :		d = IS_PICO;		break;
			case 'f' :		d = IS_FEMTO;		break;
			default :		adv = 0;	d = 0;	break;
		};
		unit += adv;
	}
	if (strncmp(unit,"\\Omega ",6 )== 0 )
	{
		t = IS_OHM;
	}
	else
	{
		switch (unit[0])
		{
			case 'V' :	t = IS_VOLT;		break;
			case 'A' :	t = IS_AMPERE;		break;
			case 'm' :	t = IS_METER;		break;
			case 'N' :	t = IS_NEWTON;		break;
			case 's' :	t = IS_SECOND;		break;
			case 'g' :	t = IS_GRAMME;		break;
			case 'G' :	t = IS_GAUSS;		break;
			default :	return 1;		break;
		};
	}
	*type = t;
	*decade = d;
	return 0;
}
int compare_units (un_s *a,un_s *b)
{
	float c, d;
	if (a == NULL || b == NULL)		return -1;
	if (a->type != b->type)			return 1;
	if (a->ax != b->ax)				return 1;
	if (a->mode != b->mode)			return 1;
	c = (a->dx * (float)pow(10,(double)(a->decade)));
	d = (b->dx * (float)pow(10,(double)(b->decade)));
	c = (d != 0) ? c/d : ((c == 0) ? 1 : 0);
	return (fabs(c-1) < 1e-7) ? 0 : 1;
}
int 	change_decade(un_s *un, int dec)
{
	char prev_dec;
	char *unit = NULL;

	if (un == NULL || un->decade == dec)		return -1;
	prev_dec = un->decade;
	un->decade = (char)dec;
	if ((unit = generate_units(un->type,un->decade)) != NULL)
	{
		if (un->name)  free(un->name);
		un->name = unit;
	}
	else
	{
	  un->decade = prev_dec;
		return -1;
	}
	un->dx *= (float)pow(10,prev_dec - un->decade);
	return 0;
}




int	get_afine_param_from_unit(un_s *un, float *ax, float *dx)
{
	float tmp;
	if (un == NULL) return 1;

	*dx = un->dx;
	*ax = un->ax;
	tmp = (float)pow(10,un->decade);
	*dx *= tmp;
	*ax *= tmp;
	return 0;
}


int	set_afine_param_for_unit_with_decade(un_s *un, float ax, float dx, int decade)
{
	float tmp;
	if (un == NULL) return 1;
	if (decade > 32 || decade < -32) return 1;

	tmp = (float)pow(10,-decade);
	un->dx = dx * tmp;
	un->ax = ax * tmp;
	un->decade = decade;
	return 0;
}



# endif
