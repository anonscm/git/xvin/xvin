/********************************************************************************/
/*	ffw3_lib.c							*/
/********************************************************************************/
/*	DESCRIPTION	Collection of functions performing : 
 *			- PSD computation
 *                      - filtering
 *                      - Hilbert transforms
 *										*
 *	using the fftw3 library
 *                                                                              *
 *      these functions are not accessible from the XVin GUI, but are intended      
 *      to be used by other functions or plugins. they provide a higher level 
 *      interface to the FFTW3 library.
 *                                                                              *
 *      front-end functions (accessible from the GUI) are in the source file
 *      "menus/plot/treat/p_treat_fftw.c"
 *                                                                              *
 *      Read the header file for more info on each function
 */
/********************************************************************************/
/* Vincent Croquette, Nicolas Garnier	nicolas.garnier@ens-lyon.fr		*/
/********************************************************************************/

# include <stdlib.h>
# include <string.h>

#include <gsl/gsl_statistics.h>
#include <fftw3.h>		// include the fftw library v3.0.1 or v3.1.1 or newer for computing spectra:

#include "platform.h"
#include "xvin.h"
#include "fftw3_lib.h"

#include "fft_filter_lib.h"	// (fftw3 is also included from here)
#include "xv_tools_lib.h"




/****************************************************************/
/* the function below computes a PSD from data 'y' 				*/
/* and gives the resulting psd (in V^2/Hz) in 'psd_f' 			*/
/* correct frequencies for the x-axis are retruned in 'freq'	*/
/*																*/
/* 'psd_f' and 'freq' must be already allocated ! 				*/
/****************************************************************/
int compute_psd_of_float_data(float *y, float *psd_f, float *freq, float f_acq, int nx, int n_fft, int n_overlap, int bool_hanning)
{	register int i,j,k;
	double  		*in=NULL;
	fftw_complex	*outc=NULL;
    double  		*psd_d=NULL; 
    fftw_plan plan;     // fft plan for fftw3
    	
	in   = (double *) fftw_malloc( n_fft * sizeof(double));
	outc = (fftw_complex *) fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));
	psd_d= (double *) fftw_malloc((n_fft/2+1)*sizeof(double));
	
	for (i=0; i<(n_fft/2+1); i++) psd_d[i]=0; 
	plan = fftw_plan_dft_r2c_1d(n_fft, in, outc, FFTW_ESTIMATE);

	k=0;
	for (j=0; j<(nx-n_fft)+1; j+=(n_fft-n_overlap) )
	{
	  for (i=0; i<n_fft ; i++)     in[i] = (double)y[j+i];
	
	  if (bool_hanning==1) fft_window_real(n_fft, in, 0.);

	  fftw_execute(plan);
	  
	  psd_d[0] += outc[0][0]*outc[0][0];    		// DC component
	  for (i=1; i<(n_fft+1)/2; ++i)		// k < nx/2 rounded up
	    psd_d[i] += outc[i][0]*outc[i][0] + outc[i][1]*outc[i][1];
	  if (n_fft%2 == 0)			//nx is even
	    psd_d[n_fft/2] += outc[n_fft/2][0]*outc[n_fft/2][0]; // Nyquist frequency
	  k++;
	} // at the end of the loop, we have averaged $k$ spectra
	
	for (i=0 ; i<= n_fft/2 ; i++)
    {   freq[i]  = (float)i*f_acq/(float)n_fft;								// normalization in the frequency direction : OK
       	psd_f[i] = (float)( (psd_d[i]/k)/(n_fft*f_acq/2.) );		// 2004/01/15 : normalisation to have a *density*
    }
    
    fftw_free(in); fftw_free(outc); fftw_free(psd_d);
    fftw_destroy_plan(plan);
	
    return(k); // we return the number of windows operated on
} // end of "compute_psd_of_float_data"







/****************************************************************/
/* the function below computes a cross-PSD from data 'x' and 'y'*/
/* and gives the resulting psd (in V^2/Hz) in 'psd_f' 			*/
/* correct frequencies for the x-axis are retruned in 'freq'	*/
/*																*/
/* 'psd_f' and 'freq' must be already allocated ! 				*/
/****************************************************************/
int compute_cross_psd_of_float_data(float *x, float *y, float *psd_f_real, float *psd_f_imag, float *freq, float f_acq, int nx, int n_fft, int n_overlap, int bool_hanning)
{	register int i,j,k;
	double  		*in1=NULL, *in2=NULL;
	fftw_complex	*out1=NULL, *out2=NULL;
    double  		*psd_r=NULL, *psd_i=NULL; 
    fftw_plan plan1, plan2;     // fft plan for fftw3
    	
	in1  = (double *) fftw_malloc( n_fft     *sizeof(double));
	in2  = (double *) fftw_malloc( n_fft     *sizeof(double));
	out1 = (fftw_complex *) fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));
	out2 = (fftw_complex *) fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));
	psd_r= (double *) fftw_malloc((n_fft/2+1)*sizeof(double));
	psd_i= (double *) fftw_malloc((n_fft/2+1)*sizeof(double));
	
	for (i=0; i<(n_fft/2+1); i++) { psd_r[i]=0.; psd_i[i]=0.; }
	plan1 = fftw_plan_dft_r2c_1d(n_fft, in1, out1, FFTW_ESTIMATE);
	plan2 = fftw_plan_dft_r2c_1d(n_fft, in2, out2, FFTW_ESTIMATE);

	k=0;
	for (j=0; j<(nx-n_fft)+1; j+=(n_fft-n_overlap) )
	{	for (i=0; i<n_fft ; i++)     in1[i] = (double)x[j+i];
		for (i=0; i<n_fft ; i++)     in2[i] = (double)y[j+i];
		
		if (bool_hanning==1) { 	fft_window_real(n_fft, in1, 0.); 
								fft_window_real(n_fft, in2, 0.);
						     }

        fftw_execute(plan1);
        fftw_execute(plan2);

 		psd_r[0] += out1[0][0]*out2[0][0];    		// DC component
 		psd_i[0] += out1[0][1]*out2[0][0];    		// DC component
		
		for (i=1; i<(n_fft+1)/2; ++i)		// k < nx/2 rounded up
        //il manquait peut être {//
		{	psd_r[i] += out1[i][0]*out2[i][0] + out1[i][1]*out2[i][1]; 
        	psd_i[i] += out1[i][1]*out2[i][0] - out1[i][0]*out2[i][1];
    	}
    	if (n_fft % 2 ==0)			// nx is even
        {	psd_r[n_fft/2] += out1[n_fft/2][0]*out2[n_fft/2][0]; // Nyquist frequency
        	psd_i[n_fft/2] += out1[n_fft/2][0]*out2[n_fft/2][0]; // Nyquist frequency
    	}
		k++;
	} // at the end of the loop, we have averaged $k$ spectra

	for (i=0 ; i<= n_fft/2 ; i++)
    {   freq[i]  = (float)i*f_acq/(float)n_fft;								// normalization in the frequency direction : OK
       	psd_f_real[i] = (float)( (psd_r[i]/(double)k)/(n_fft*f_acq/2.) );		// 2004/01/15 : normalisation to have a *density*
       	psd_f_imag[i] = (float)( (psd_i[i]/(double)k)/(n_fft*f_acq/2.) );
    }
    
    fftw_free(in1); 			fftw_free(in2);
    fftw_free(out1); 			fftw_free(out2);
    fftw_free(psd_r);			fftw_free(psd_i);
	fftw_destroy_plan(plan1);	fftw_destroy_plan(plan2);
    
    return(k); // we return the number of windows operated on
} // end of "compute_cross_psd_of_float_data"




/****************************************************************/
/* Filtering in Fourier space                                   */
/*                                                              */
/* float fl  : low  pass cutoff frequency (in Hertz)            */
/* float dfl : low  pass width of filter  (in Hertz)            */
/* float fh  : high pass cutoff frequency (in Hertz)            */
/* float dfh : high pass width of filter  (in Hertz)            */
/*                                                              */
/* if fl is negative, than a HP filter is contructed and used   */
/* if fh is negative, than a LP filter is contructed and used   */
/* if fh>f_acq/2,     than a LP filter is contructed and used   */
/*                                                              */
/* the returned variable is of course allocated by the function */
/****************************************************************/
float *Fourier_filter_float(float *y, int nx, float f_acq, float fl, float dfl, float fh, float dfh, int bool_Hanning)
{	int		i;
	int		n_fft=nx;
	double 		*in;
	fftw_complex 	*out;
        float		*result;
	fftw_plan 	plan;
	fftf 		*filter;
	int    		ml, dml, mh, dmh;
	
	(void)bool_Hanning;
// filter construction:
	ml  = freq_to_mode(fl,  f_acq, n_fft); // converts low-pass  cutoff from Hz to mode number
	dml = freq_to_mode(dfl, f_acq, n_fft); // converts low-pass  width  from Hz to mode number
	mh  = freq_to_mode(fh,  f_acq, n_fft); // converts high-pass cutoff from Hz to mode number
	dmh = freq_to_mode(dfh, f_acq, n_fft); // converts high-pass width  from Hz to mode number

	if ((mh>nx/2) || (mh<0))	filter = build_lowpass_filter (n_fft, ml, dml);
	else if (ml<1)		  	filter = build_highpass_filter(n_fft, mh, dmh);
	else				filter = build_bandpass_filter(n_fft, ml, dml, mh, dmh);
		
// some mallocs
	in     = (double *) fftw_malloc((nx+(nx%2))*sizeof(double));	// to store initial data, and the results in double
    	out    = (fftw_complex *) fftw_malloc((nx/2+1)   *sizeof(fftw_complex));	// to store the complex Fourier transform
    	result = (float *) calloc(nx,              sizeof(float));	// to store the result, in floats
	if ( (in==NULL) || (out==NULL) || (result==NULL))  { win_printf_OK("malloc error"); return(NULL); }
 		
	for (i=0; i<nx ; i++)      in[i] = (double)(y[i]);

// prepare and perform direct FFT
	plan = fftw_plan_dft_r2c_1d(nx, in, out, FFTW_ESTIMATE);	
	fftw_execute(plan);		
	fftw_destroy_plan(plan);

// filter application: 
	apply_filter_complex(filter, out, nx/2+1);	// the fft has nx/2+1 complex points

// prepare and perform inverse FFT
	plan = fftw_plan_dft_c2r_1d(nx, out, in, FFTW_ESTIMATE); 	
	fftw_execute(plan); 	
	fftw_destroy_plan(plan);

	for (i=0; i<nx ; i++)      result[i] = (float)in[i];

	fftw_free(in); 
	fftw_free(out);
	free_filter(filter);
	
	return(result);
}






/****************************************************************/
/* Hilbert transfrom                                            */
/* filters an array of floats and returns an array of complexex */
/*                                                              */
/* float fl  : low  pass cutoff frequency (in Hertz)            */
/* float dfl : low  pass width of filter  (in Hertz)            */
/* float fh  : high pass cutoff frequency (in Hertz)            */
/* float dfh : high pass width of filter  (in Hertz)            */
/*                                                              */
/* the returned variable is of course allocated by the function */
/****************************************************************/
fftw_complex *Hilbert_transform(float *y, int nx, float f_acq, float fl, float dfl, float fh, float dfh, int bool_Hanning)
{
    	register int j;
	int 	nout, n_fft, n_2;
  	double	*in;
	int    ml, dml, mh, dmh;
	fftw_complex *out=NULL, *inc=NULL;
	fftw_plan 	plan_direct, plan_inverse;

	nout = nx;
	n_fft= nx; 
	
	ml  = freq_to_mode(fl,  f_acq, n_fft); // converts low-pass  cutoff from Hz to mode number
	dml = freq_to_mode(dfl, f_acq, n_fft); // converts low-pass  width  from Hz to mode number
	mh  = freq_to_mode(fh,  f_acq, n_fft); // converts high-pass cutoff from Hz to mode number
	dmh = freq_to_mode(dfh, f_acq, n_fft); // converts high-pass width  from Hz to mode number

	n_2 = (n_fft%2==0) ? n_fft/2 : (n_fft-1)/2; 
//	current_filter=construct_filter(nx_2+1, (REAL_INPUT | REAL_OUTPUT | SYM) );

	if (current_filter!=NULL)	free_filter(current_filter);
	current_filter = build_bandpass_filter(n_fft, ml, dml, mh, dmh);
	current_filter->mode = ( NORMAL_F | BAND_PASS | REAL_INPUT | COMPLEX_OUTPUT );
	if (current_filter==NULL)	return(NULL);

    // REAL_INPUT and COMPLEX_OUTPUT
    in  = (double *) fftw_malloc(  nout  *sizeof(double));
	out = (fftw_complex *) fftw_malloc(  nout  *sizeof(fftw_complex));
	inc = (fftw_complex *) fftw_malloc(  nout  *sizeof(fftw_complex));	// to store initial data
	plan_direct  = fftw_plan_dft_r2c_1d(nout, in,  out, FFTW_ESTIMATE);
	plan_inverse = fftw_plan_dft_1d    (nout, out, inc, FFTW_BACKWARD, FFTW_ESTIMATE);   

	if (current_filter==NULL)	   return(NULL);	
	if (current_filter->n==0) 	   return(NULL);
	if (current_filter->f==NULL)	   return(NULL);	
// win_printf("filter is tested");

	for (j=0; j<nx ; j++)         in[j] = (double)y[j];
	if (bool_Hanning==1)	      fft_window_real(nx, in, 0.001);	

	fftw_execute(plan_direct); 
	apply_filter_complex(current_filter, out, n_2+1);	// the fft has n/2+1 complex points
 	fftw_execute(plan_inverse); 


// after Hilbert transform, half the amplitude is missing 
// because negative frequencies have been removed
// so we multiply the output by 2
// and fftw3 conventions are such that we also have to divide by nout
	for (j=0; j<nx ; j++)	
	{ inc[j][0] *= (double)2./(double)nout;
	  inc[j][1] *= (double)2./(double)nout;
	}

	if (bool_Hanning==1)	      fft_unwindow_complex(nx, inc, 0.001);

   	fftw_destroy_plan(plan_direct);
	fftw_destroy_plan(plan_inverse);
	fftw_free(in); fftw_free(out);
	
	return(inc);      
} // end of "Hilbert transform"


