/********************************************************************************/
/*	fft_filter_lib.c							*/
/********************************************************************************/
/*	DESCRIPTION	Collection of function performing :
 *				|lowpass, 	|smooth,
 *				|highpass,	|normal,
 *				|bandpass,	|brickwall,
 *				|rejector,
 *			filtering on the fourier transform of either real or
 *			complex data.
 *										*
 *	filter_init(npts)
 *										*
 *	cutoff, center, width characterize the filter shape, their units
 * 	are in FFT mode number (the same as in the spectrum plot).
 */
/********************************************************************************/
/* Vincent Croquette, Nicolas Garnier	nicolas.garnier@ens-lyon.fr		*/
/********************************************************************************/


#include "xvin.h"
#include "fft_filter_lib.h"

# include <stdlib.h>
# include <string.h>


/********************************************************************************/
struct fft_filter *current_filter=NULL;
	/* the variable containing the filter 					*/
/********************************************************************************/
int fft_filter_mode;
	/* this variable gives the information about the last filter asked for	*/
	/* so, if everything went OK, this must be the same information as the	*/
	/* one given by the subvariable "mode" of the "current_filter" variable */

/********************************************************************************/
/* Implementation								*/
/********************************************************************************/


fftf *init_filter(int npts)
{
    fftf *filter = NULL;

//	if (filter!=NULL)	free_filter(filter);
	filter = (fftf*) calloc(1,sizeof(fftf));
	if (filter==NULL) 	return (fftf *) xvin_ptr_error(Out_Of_Memory);
	filter->f = (float*)calloc(npts, sizeof(float));
	if (filter->f==NULL)	return (fftf *) xvin_ptr_error(Out_Of_Memory);
	filter->n = npts;
	filter->lp= 0;
	filter->wl= 0;
	filter->hp= 0;
	filter->wh= 0;

	return filter;
}


void free_filter(fftf *filter)
{
  if (filter == NULL) return;
  if (filter->f != NULL) free(filter->f);
  free(filter);
  filter=NULL;
  return;
}


/************************************************************************/
/* applique un filtre fil sur une fft reelle x				*/
/* 	cette operation se passe avec des reels  			*/
/*	un filtre est un tableau de reals de taille n			*/
/************************************************************************/
int apply_filter_real(struct fft_filter *fil, double *x ,int n)
{
	register int i;

	for ( i = 0; i<n; i++)
	{	x[2*i]   *= fil->f[i];
		x[2*i+1] *= fil->f[i];
	}
	return 0;
}/* end of the procedure apply_filter */


/************************************************************************/
/* applique un filtre fil sur une fft complexe x			*/
/* 	cette operation se passe avec des complexes  			*/
/*		partie reelle et partie imaginaire se suivent		*/
/*		- n est le nombre de points (complexes) du filtre	*/
/*		- un filtre est un tableau de reals	de taille n	*/
/************************************************************************/
int apply_filter_complex(struct fft_filter *fil, fftw_complex *x ,int n)
{
	register int i;

	for ( i=0; i<n; i++ )
	{	x[i][0] *= fil->f[i];
		x[i][1] *= fil->f[i];
	}
	return 0;
}/* end of the procedure apply_filter */






/********************************************************************************/
/* to build a low pass filter to be applied on FFT data.
	the cutoff is the frequency where f=1/2;
	the width is the distance from the cutoff where f=0 and f=1
	ie f reaches 1 under (cutoff-width) and reaches 0 after (cutoff+width) 	*/
/********************************************************************************/
fftf *build_lowpass_filter(int npts, int cutoff, int width)
{ 	register int i,j;
	fftf	*filter=NULL;

	if ( (cutoff<width) || (width<0) || (cutoff+width>npts) )
	{	win_printf_OK("Error building filter!\ncutoff %d is lower than width %d", cutoff, width);
		return NULL;
	}
	filter = init_filter(npts);
	if (filter==NULL) return NULL;

	filter->f[0]=1;
	for ( i = 1; i < (cutoff-width)+1; i++)
		filter->f[i] = 1;;
	for ( i = (cutoff-width)+1, j=1; i < (cutoff+width); i++, j++)	// non execute si width=0!
		filter->f[i] = (1. + cos ( j * M_PI/(2.*width)))/2.;
	for ( i = (cutoff+width); i < npts ; i++)
		filter->f[i] = 0.;

	filter->hp = 0;
	filter->wh = 0;
	filter->lp = cutoff;
	filter->wl = width;

	filter->mode=fft_filter_mode;

	return filter;
} /* end of the procedure "build_lowpass_filter" */








/********************************************************************************/
/* to construct the filter :
 * 	high-pass filter to be applied on a
 *	Fourier transform of a npts real data 					*/
/********************************************************************************/
struct fft_filter *build_highpass_filter(int npts, int cutoff, int width)
{
	register int i,j;
	struct fft_filter *filter=NULL;

	if ( (cutoff<width) || (width<0) || (cutoff+width>npts) )
	{	win_printf_OK("Error building filter!\ncutoff %d is lower than width %d", cutoff, width);
		return filter;
	}
	filter=init_filter(npts);
	if (filter==NULL) return NULL;

	filter->f[0] = 0.;
	for ( i = 1; i < (cutoff-width)+1; i++)
			filter->f[i] = 0.;
	for ( i = (cutoff-width)+1, j=1; i < (cutoff+width); i++, j++)
			filter->f[i] = (1. - cos ( j * M_PI/(2.*width)))/2.;
	for ( i = (cutoff + width); i < npts ; i++)
			filter->f[i] = 1.;

	filter->lp = 0;
	filter->wl = 0;
	filter->hp = cutoff;
	filter->wh = width;

	filter->mode=fft_filter_mode;
	return filter;
} /* end of the procedure "build_highpass_filter" */



/********************************************************************************/
/* to construct a BP filter to be applied on a npts points FFT					*
 * rejector filter on a Fourier transform of a npts real data points			*
 * The filter is built with cosines, 											*
 * it has a gain of 1 between cutoffl+widthl and cutoffh-width 					*
 * and a gain of 0 below cutoffl-widthl and above cutoffh+widthh				*/
/********************************************************************************/
struct fft_filter *build_bandpass_filter(int npts, int cutoffl, int widthl, int cutoffh, int widthh)
{	register int i,j;
	struct fft_filter *filter=NULL;

	if ( (cutoffl<widthl) || (widthl<0) || (cutoffl+widthl>npts) )
	{	win_printf_OK("Error building filter!\nlow cutoff %d is lower than low width %d", cutoffl, widthl);
		return filter;
	}
	if ( (cutoffh<widthh) || (widthh<0) || (cutoffh+widthh>npts) )
	{	win_printf_OK("Error building filter!\nhigh cutoff %d is lower than high width %d", cutoffh, widthh);
		return filter;
	}
	if ( (cutoffh<cutoffl) || (cutoffh-widthh<cutoffl+widthl) )
	{	win_printf_OK("\\color{yellow} Error building filter!\n"
			"\\color{white} [(high cutoff)-width] should be higher than [(low cutoff)+width]\n"
			"but here, we have %d-%d and %d+%d !", cutoffh, widthh, cutoffl, widthl);
		return filter;
	}
	filter=init_filter(npts);
	if (filter==NULL) return NULL;

	filter->f[0] = 0;
	for (i = 1; i < (cutoffl-widthl)+1; i++)
			filter->f[i] = 0.;
	for (i = (cutoffl-widthl)+1, j=1; i < (cutoffl+widthl); i++, j++)
			filter->f[i] = (1. - cos ( j * M_PI/(2.*widthl)))/2.;
	for (i = (cutoffl+widthl); i <= (cutoffh-widthh); i++)
			filter->f[i] = 1.;
	for (i = (cutoffh-widthh+1), j=1; i < (cutoffh+widthh); i++, j++)
			filter->f[i] = (1. + cos ( j * M_PI/(2.*widthh)))/2.;
	for (i = (cutoffh+widthh); i < npts; i++)
			filter->f[i] = 0.;

	filter->lp = cutoffl;
	filter->wl = widthl;
	filter->hp = cutoffh;
	filter->wh = widthh;

	filter->mode=fft_filter_mode;
	return filter;
} /* end of the procedure "build_bandpass_filter" */





/************************************************************************/
/* to construct a filter : 						*
 * the inverse of a band-pass filter					*/
/************************************************************************/
struct fft_filter *build_rejector_filter(int npts, int cutoffl, int widthl, int cutoffh, int widthh)
{
	register int i;
	struct   fft_filter *filter = NULL;

	filter = build_bandpass_filter(npts, cutoffl, widthl, cutoffh, widthh);
	if (filter==NULL) { return NULL; }

	for (i=0; i<npts ; i++) filter->f[i] = 1. - filter->f[i];

	return filter;
} /* end of the procedure "build_rejector_filter" */










/********************************************************************************/
/* to build a Butterworth low pass filter to be applied on FFT data.
	the cutoff is the frequency where f=1/2;
	the order is the order of the filter */
/********************************************************************************/
fftf *build_lowpass_butter_filter(int npts, int cutoff, int order)
{ 	register int i,j;
	fftf	*filter=NULL;

	if ( (order<1) || (cutoff<=0) )
	{	win_printf_OK("Error building filter!\ncutoff %d or order %d are not correct", cutoff, order);
		return NULL;
	}
	filter = init_filter(npts);
	if (filter==NULL) return NULL;

	for ( i=0; i<npts; i++ )
	{	filter->f[i] = 1.;
		for (j=0; j<order; j++)
		filter->f[i] *= 1./(1.+(float)i/(float)cutoff);
	}
	filter->hp = 0.;
	filter->wh = 0.;
	filter->lp = cutoff;
	filter->wl = order;

	filter->mode=fft_filter_mode;

	return filter;
} /* end of the procedure "build_lowpass_butter_filter" */




/********************************************************************************/
/* to build a Butterworth high pass filter to be applied on FFT data.
	the cutoff is the frequency where f=1/2;
	the order is the order of the filter */
/********************************************************************************/
fftf *build_highpass_butter_filter(int npts, int cutoff, int order)
{ 	register int i;
	fftf	*filter=NULL;

	filter = build_lowpass_butter_filter(npts, cutoff, order);
	if (filter==NULL) return NULL;

	for ( i=0; i<npts; i++ )
	{	filter->f[i] = 1 - filter->f[i];
	}
	filter->hp = cutoff;
	filter->wh = order;
	filter->lp = 0.;
	filter->wl = 0.;

	filter->mode=fft_filter_mode;

	return(filter);
} /* end of the procedure "build_highpass_butter_filter" */




/********************************************************************************/
/* to build a Butterworth bandpass filter to be applied on FFT data.
	the cutoff is the frequency where f=1/2;
	the order is the order of the filter */
/********************************************************************************/
fftf *build_bandpass_butter_filter(int npts, int cl, int ol, int ch, int oh)
{ 	register int i;
	fftf	*filter1=NULL;
	fftf	*filter2=NULL;

	filter1 = build_lowpass_butter_filter(npts, cl, ol);
	if (filter1==NULL) return NULL;

	filter2 = build_highpass_butter_filter(npts, ch, oh);
	if (filter2==NULL) return NULL;

	for ( i=0; i<npts; i++ )
	{	filter1->f[i] *= filter2->f[i];
	}
	filter1->hp = ch;
	filter1->wh = oh;
	filter1->lp = cl;
	filter1->wl = ol;

	filter1->mode=fft_filter_mode;

	free_filter(filter2);
	return(filter1);
} /* end of the procedure "build_bandpass_butter_filter" */



/********************************************************************************/
/* to build a Butterworth rejector filter to be applied on FFT data.
	the cutoff is the frequency where f=1/2;
	the order is the order of the filter */
/********************************************************************************/
fftf *build_rejector_butter_filter(int npts, int cl, int ol, int ch, int oh)
{ 	register int i;
	fftf	*filter=NULL;

	filter = build_bandpass_butter_filter(npts, cl, ol, ch, oh);
	if (filter==NULL) return NULL;

	for ( i=0; i<npts; i++ )
	{	filter->f[i] = 1. - filter->f[i];
	}
	return(filter);
} /* end of the procedure "build_rejector_butter__filter" */




/************************************************************************/
/* 'construct_filter' : to construct a filter,
			asking the user for its parameters		*/
/************************************************************************/
fftf *construct_filter(int npts, int mode, float dx)
{	int ret;
static int shape=2, type=0, freq_units=0;
	static int cl=0, ch=0, wl=0, wh=0;
  static float fcl=0, fch=0, fwl=0, fwh=0;
	static float f_acq=0;
  f_acq=1/dx;
	int	   fd1, fd2, fd3;
	fftf	*filter=NULL;
  int nx_2 = 0;
  nx_2 = (npts%2==0) ? npts/2 : (npts-1)/2;

	if ( (cl==0) || (cl>nx_2) ) { cl=nx_2/2; wl=nx_2/8;}
	if ( (ch==0) || (ch>nx_2) ) { ch=nx_2/2; wh=nx_2/8;}

	fft_filter_mode = 0;

	ret = win_scanf("Select the shape of filter:\n %R brick-wall\n %r smooth (sine)\n %r normal (sine)\n %r Butterworth\n"
			  "Select the type of filter:\n %R low-pass\n %r high-pass\n %r band-pass\n %r rejector\n"
			  "Find acquisition frequency from unit %b \n"
        "If not, give frequency of acquisition here %f\n",			  &shape, &type, &freq_units,&f_acq);
	if (ret==WIN_CANCEL) return(NULL);

  if (freq_units) f_acq = 1/dx;


	if (shape==0) 			fft_filter_mode |= BRICKW_F;
	else if (shape==1) 		fft_filter_mode |= SMOOTH_F;
	else if (shape==2) 		fft_filter_mode |= NORMAL_F;
	else if (shape==3)		fft_filter_mode |= BUTTERWORTH_F;
	else { win_printf_OK("invalid filter shape!"); return(NULL); }

	if (type==0) 			fft_filter_mode |= LOW_PASS;
	else if (type==1) 		fft_filter_mode |= HIGH_PASS;
	else if (type==2) 		fft_filter_mode |= BAND_PASS;
	else if (type==3) 		fft_filter_mode |= REJECTOR;
	else { win_printf_OK("invalid filter type!"); return(NULL); }

	if (fft_filter_mode & BRICKW_F)
	{		if (fft_filter_mode & LOW_PASS)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} BrickWall lowpass filter\n\\pt10 \\color{white} select frequencies\n"
					" %6f : high cutoff\n",&fcl);
          cl = (int) (0.5 + fcl * npts / f_acq);
					filter = build_lowpass_filter(nx_2, cl, 0);
			}
			if (fft_filter_mode & HIGH_PASS)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} BrickWall highpass filter\n\\pt10 \\color{white} select frequencies\n"
					" %6f : high cutoff\n",&fch);
          ch = (int) (0.5 + fch * npts / f_acq);

					filter = build_highpass_filter(nx_2, ch, 0);
			}
			if (fft_filter_mode & BAND_PASS)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} BrickWall bandpass filter\n\\pt10 \\color{white} select frequencies\n"
					" %6f : low cutoff\n %6f : high cutoff\n",&fcl,&fch);
          ch = (int) (0.5 + fch * npts / f_acq);
          cl = (int) (0.5 + fcl * npts / f_acq);

					filter = build_bandpass_filter(npts, cl, 0, ch, 0);
			}
			if (fft_filter_mode & REJECTOR)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} BrickWall rejector filter\n\\pt10 \\color{white} select frequencies\n"
					" %6d : low cutoff\n %6d : high cutoff\n",&fcl,&fch);
          ch = (int) (0.5 + fch * npts / f_acq);
          cl = (int) (0.5 + fcl * npts / f_acq);

					filter = build_rejector_filter(nx_2, cl, 0, ch, 0);
			}
	}
	if (fft_filter_mode & SMOOTH_F)
	{		if (fft_filter_mode & LOW_PASS)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} Smooth lowpass filter\n\\pt10 \\color{white} select frequency\n"
					"%6f : cutoff\n (width is coutoff/2)\n",&fcl);
          cl = (int) (0.5 + fcl * npts / f_acq);

					filter = build_lowpass_filter(nx_2, cl, (int)cl/2);
			}
			if (fft_filter_mode & HIGH_PASS)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} Smooth highpass filter\n\\pt10 \\color{white} select frequency\n"
					" %6d : cutoff\n (width is coutoff/2)\n",&fch);
          ch = (int) (0.5 + fch * npts / f_acq);

					filter = build_highpass_filter(nx_2, ch, (int)ch/2);
			}
			if (fft_filter_mode & BAND_PASS)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} Smooth bandpass filter\n\\pt10 \\color{white} select frequencies\n"
					" %6f : cutoff\n %6f : width\n",&fcl,&fwl);
          cl = (int) (0.5 + fcl * npts / f_acq);
          wl = (int) (0.5 + fwl * npts / f_acq);

          filter = build_bandpass_filter(nx_2, cl-wl/2, wl/2, cl+wl/2, wl/2 );
			}
			if (fft_filter_mode & REJECTOR)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} Smooth rejector filter\n\\pt10 \\color{white} select frequencies\n"
      " %6f : cutoff\n %6f : width\n",&fcl,&fwl);
      cl = (int) (0.5 + fcl * npts / f_acq);
      wl = (int) (0.5 + fwl * npts / f_acq);

					filter = build_rejector_filter(nx_2, cl-wl/2, wl/2, cl+wl/2, wl/2 );
			}
	}
	if (fft_filter_mode & NORMAL_F)
	{		if (fft_filter_mode & LOW_PASS)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} Normal lowpass filter\n\\pt10 \\color{white} select frequencies\n"
      " %6f : cutoff\n %6f : width\n",&fcl,&fwl);
      cl = (int) (0.5 + fcl * npts / f_acq);
      wl = (int) (0.5 + fwl * npts / f_acq);


					filter = build_lowpass_filter(nx_2, cl, wl);
			}
			if (fft_filter_mode & HIGH_PASS)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} Normal highpass filter\n\\pt10 \\color{white} select frequencies\n"
					" %6f : cutoff\n %6f : width\n",&fch,&fwh);
          ch = (int) (0.5 + fch * npts / f_acq);
          wh = (int) (0.5 + fwh * npts / f_acq);
					filter = build_highpass_filter(nx_2, ch, wh);
			}
			if (fft_filter_mode & BAND_PASS)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} Normal bandpass filter\n\\pt10 \\color{white} select frequencies\n"
					" %6f : low cutoff\n %6f : width\n"
					" %6f : high cutoff\n %6f : width\n",&fcl,&fwl,&fch,&fwh);
          cl = (int) (0.5 + fcl * npts / f_acq);
          wl = (int) (0.5 + fwl * npts / f_acq);
          ch = (int) (0.5 + fch * npts / f_acq);
          wh = (int) (0.5 + fwh * npts / f_acq);

					filter = build_bandpass_filter(nx_2, cl, wl, ch, wh);
			}
			if (fft_filter_mode & REJECTOR)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} Normal rejector filter\n\\pt10 \\color{white} select frequencies\n"
      " %6f : low cutoff\n %6f : width\n"
      " %6f : high cutoff\n %6f : width\n",&fcl,&fwl,&fch,&fwh);
      cl = (int) (0.5 + fcl * npts / f_acq);
      wl = (int) (0.5 + fwl * npts / f_acq);
      ch = (int) (0.5 + fch * npts / f_acq);
      wh = (int) (0.5 + fwh * npts / f_acq);

      filter = build_rejector_filter(nx_2, cl, wl, ch, wh);
			}
	}
	if (fft_filter_mode & BUTTERWORTH_F)
	{		if (fft_filter_mode & LOW_PASS)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} Butterworth lowpass filter\n\\pt10 \\color{white} select parameters\n"
      " %6f : cutoff\n %6f : width\n",&fcl,&fwl);
      cl = (int) (0.5 + fcl * npts / f_acq);
      wl = (int) (0.5 + fwl * npts / f_acq);


          filter = build_lowpass_filter(nx_2, cl, wl);
			}
			if (fft_filter_mode & HIGH_PASS)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} Butterworth highpass filter\n\\pt10 \\color{white} select parameters\n"
      " %6f :cutoff\n %6f : width\n",
      &fch,&fwh);

      ch = (int) (0.5 + fch * npts / f_acq);
      wh = (int) (0.5 + fwh * npts / f_acq);
      filter = build_highpass_filter(nx_2, ch, wh);


			}
			if (fft_filter_mode & BAND_PASS)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} Butterworth bandpass filter\n\\pt10 \\color{white} select parameters\n"
      " %6f : low cutoff\n %6f : width\n"
      " %6f : high cutoff\n %6f : width\n",&fcl,&fwl,&fch,&fwh);
      cl = (int) (0.5 + fcl * npts / f_acq);
      wl = (int) (0.5 + fwl * npts / f_acq);
      ch = (int) (0.5 + fch * npts / f_acq);
      wh = (int) (0.5 + fwh * npts / f_acq);
					filter = build_bandpass_butter_filter(nx_2, cl, wl, ch, wh);
			}
			if (fft_filter_mode & REJECTOR)
			{ 		ret=win_scanf("\\pt14 \\color{yellow} Butterworth rejector filter\n\\pt10 \\color{white} select parameters\n"
      " %6f : low cutoff\n %6f : width\n"
      " %6f : high cutoff\n %6f : width\n",&fcl,&fwl,&fch,&fwh);
      cl = (int) (0.5 + fcl * npts / f_acq);
      wl = (int) (0.5 + fwl * npts / f_acq);
      ch = (int) (0.5 + fch * npts / f_acq);
      wh = (int) (0.5 + fwh * npts / f_acq);
					filter = build_rejector_butter_filter(nx_2, cl, wl, ch, wh);
			}
	}
	if ( (filter==NULL) || (ret==WIN_CANCEL))		return(NULL);
	if ( (filter->n==0) || (filter->f==NULL) )	return(NULL);

/*	a rediger : des tests sur les valeurs des cutoffs et des widths....		*/

//	if (fft_filter_mode & REAL_INPUT) symetrize_filter(filter, npts);

	filter->mode = fft_filter_mode
			| (mode & (REAL_INPUT|COMPLEX_INPUT))
			| (mode & (REAL_OUTPUT|COMPLEX_OUTPUT));


	return(filter);
}/*end of the procedure "ng_construct_filter" */






int plot_filter_in_pltreg(struct fft_filter *filter)
{
	register	int i;
	pltreg 		*pr;
	O_p 		*op;
	char 		c1[128] = {0},c2[128] = {0};


	if ( (filter->mode==0) || (filter->f==NULL) ) return (win_printf("No filter in use !"));

	if (filter->mode & SMOOTH_F)
	{	if (filter->mode & LOW_PASS)		snprintf(c2,sizeof(c2),"smooth low-pass  %d",filter->lp);
		else if (filter->mode & HIGH_PASS)	snprintf(c2,sizeof(c2),"smooth high-pass %d",filter->hp);
		else if (filter->mode & BAND_PASS)	snprintf(c2,sizeof(c2),"smooth band-pass %d %d",filter->lp, filter->wl);
		else if (filter->mode & REJECTOR)	snprintf(c2,sizeof(c2),"smooth rejector  %d %d",filter->lp, filter->wl);

	}
	if (filter->mode & NORMAL_F)
	{	if (filter->mode & LOW_PASS)		snprintf(c2,sizeof(c2),"normal low-pass %d,%d",filter->lp,filter->wl);
		else if (filter->mode & HIGH_PASS)	snprintf(c2,sizeof(c2),"normal high-pass %d,%d",filter->hp,filter->wh);
		else if (filter->mode & BAND_PASS)	snprintf(c2,sizeof(c2),"normal band-pass %d %d %d %d",filter->lp, filter->wl, filter->hp, filter->wh);
		else if (filter->mode & REJECTOR)	snprintf(c2,sizeof(c2),"normal rejector %d %d %d %d",filter->lp, filter->wl, filter->hp, filter->wh);
 	}
	if (filter->mode & BRICKW_F)
	{	if (filter->mode & LOW_PASS)		snprintf(c2,sizeof(c2),"brick-wall low-pass %d",filter->lp);
		else if (filter->mode & HIGH_PASS)	snprintf(c2,sizeof(c2),"brick-wall high-pass %d",filter->hp);
		else if (filter->mode & BAND_PASS)	snprintf(c2,sizeof(c2),"brick-wall band-pass %d %d",filter->lp, filter->wl);
		else if (filter->mode & REJECTOR)	snprintf(c2,sizeof(c2),"brick-wall rejector %d %d",filter->lp, filter->wl);
	}
	if (filter->mode & BRICKW_F)
	{	if (filter->mode & LOW_PASS)		snprintf(c2,sizeof(c2),"Butterworth low-pass %d",filter->lp);
		else if (filter->mode & HIGH_PASS)	snprintf(c2,sizeof(c2),"Butterworth high-pass %d",filter->hp);
		else if (filter->mode & BAND_PASS)	snprintf(c2,sizeof(c2),"Butterworth band-pass %d %d",filter->lp, filter->wl);
		else if (filter->mode & REJECTOR)	snprintf(c2,sizeof(c2),"Butterworth rejector %d %d",filter->lp, filter->wl);
	}
	if (filter->mode & REAL_INPUT)
	{	if (filter->mode & REAL_OUTPUT)		snprintf(c1,sizeof(c1),"IN REAL -> OUT REAL");
		else if (filter->mode & COMPLEX_OUTPUT)	snprintf(c1,sizeof(c1),"IN REAL -> OUT COMPLEX");
		else 					snprintf(c1,sizeof(c1),"output undefined");
	}
	else if (filter->mode & COMPLEX_INPUT)
	{	if (filter->mode & SYM)			snprintf(c1,sizeof(c1),"IN & OUT COMPLEX / SYMETRICAL");
		else if (filter->mode & DI_SYM)		snprintf(c1,sizeof(c1),"IN & OUT COMPLEX / NOT SYMET.");
	}
	else 						snprintf(c1,sizeof(c1),"inout and output undefined");

	if (ac_grep(cur_ac_reg,"%pr",&pr) !=1 ) win_printf_OK("cannot find plot region!");

	op   = create_and_attach_one_plot(pr, filter->n, filter->n, 0);
	if (op != NULL)
	  {
	    for (i=0; i<filter->n; i++)
	      {
		op->dat[0]->xd[i] = i;
		op->dat[0]->yd[i] = filter->f[i];
	      }
	    set_plot_title(op,"\\stack{{%s}{%s}}",c2,c1);
	    set_plot_x_title(op,"mode (f_{s}/N_{pts})");
	    set_plot_y_title(op,"gain");
	    op->filename = my_sprintf(op->filename,"filter");
	    op->dir = Mystrdup(pr->o_p[0]->dir);
	  }
	return refresh_plot(pr, UNCHANGED);
}
/* end of the procedure "plot_filter" */



// following function has probably no sense.
// dissymetric filtering (on complex numbers) will be performed differently...
int symetrize_filter(struct fft_filter *filter, int npts, int symetry)
{	register int i;
	int 	n_2=(int)(npts/2);
	float	*tmp = NULL;

	if (filter->n!=n_2+1) return win_printf("Filter has an incorrect size!");

	tmp = (float*)malloc((n_2+1)*sizeof(float));
	if (tmp == NULL) return win_printf("Filter cannot malloc!");
	for (i=0; i<n_2+1; i++)   tmp[i]=filter->f[i];

	filter->f = (float *)realloc(filter->f, npts*sizeof(float));
	if (filter->f==NULL) return win_printf("Filter cannot be reallocated!");

	filter->f[0]  = tmp[0];
	filter->f[n_2]= tmp[n_2];
	for (i=1; i<n_2; i++)
	{	filter->f[i]	  = tmp[i];
		filter->f[npts-i] = tmp[i];
		if (symetry==-1)  filter->f[npts-i]=0;
		if (symetry==+1)  filter->f[i]	   =0;
	}
	if (tmp) {free(tmp); tmp = NULL;}
	filter->n=npts;

	return filter->n;
}



/* Hanning window : equals 'smp' (small) at the boundaries 		*/
double Hamming(int npts, int i, float smp)
{
	return( ((double)(1 - sin((double)M_PI*(0.5+2*(double)i/(double)(npts))) + (double)smp )/(1+smp))/sqrt(1.5) );

	// "smp" allows one to divide by H(x) after coming back to real world
	// we have to divide by the square root of "\int (H(x) )^2 dx" which is 1.5
}


/* Hanning window : equals zero at the boundaries 			*/
double Hanning(int npts, int i)
{
	return Hamming(npts, i, 0);
}


/* apply a window on real data	(HANNING)				*/
/* a shift of the phase of the sinus has been added on 2004/01/15 	*/
/* to have the zeros of the window being on the sides, and on at the 	*/
/* point N/4...								*/
int fft_window_real(int npts, double *x, float smp)
{
	register int i;

	for (i=0; i<npts; i++)
	{	x[i] *= Hamming(npts, i, smp);
	}

	return 0;
}

int fft_unwindow_real(int npts, double *x, float smp)
{
	register int i;

	for (i=0; i<npts; i++)
	{	x[i] /= Hamming(npts, i, smp);
	}

	return 0;
}


int fft_window_complex(int npts, fftw_complex *x, float smp)
{
	register int i;

	for (i=0; i<npts; i++)
	{	x[i][0] *= Hamming(npts, i, smp);
		x[i][1] *= Hamming(npts, i, smp);
	}

	return 0;
}

int fft_unwindow_complex(int npts, fftw_complex *x, float smp)
{
	register int i;

	for (i=0; i<npts; i++)
	{	x[i][0] /= Hamming(npts, i, smp);
		x[i][1] /= Hamming(npts, i, smp);
	}

	return 0;
}


int fft_window_complex_2d_x(int nx, int ny, fftw_complex *x, float smp)
{
	register int i,j;

	for (j=0; j<ny; j++)
	for (i=0; i<nx; i++)
	{	x[j*nx+i][0] *= Hamming(nx, i, smp);
		x[j*nx+i][1] *= Hamming(nx, i, smp);
	}

	return 0;
}

int fft_window_complex_2d_y(int nx, int ny, fftw_complex *x, float smp)
{
	register int i,j;

	for (j=0; j<ny; j++)
	for (i=0; i<nx; i++)
	{	x[j*nx+i][0] *= Hamming(ny, j, smp);
		x[j*nx+i][1] *= Hamming(ny, j, smp);
	}

	return 0;
}

int fft_unwindow_complex_2d_x(int nx, int ny, fftw_complex *x, float smp)
{
	register int i,j;

	for (j=0; j<ny; j++)
	for (i=0; i<nx; i++)
	{	x[j*nx+i][0] /= Hamming(nx, i, smp);
		x[j*nx+i][1] /= Hamming(nx, i, smp);
	}

	return 0;
}

int fft_unwindow_complex_2d_y(int nx, int ny, fftw_complex *x, float smp)
{
	register int i,j;

	for (j=0; j<ny; j++)
	for (i=0; i<nx; i++)
	{	x[j*nx+i][0] /= Hamming(ny, j, smp);
		x[j*nx+i][1] /= Hamming(ny, j, smp);
	}

	return 0;
}

int freq_to_mode(float f, float f_acq, int N)
{	return( (int)(f*(float)N/f_acq) );
}

float mode_to_freq_to_mode(int k, float f_acq, int N)
{	return( (float)k*f_acq/(float)N );
}
