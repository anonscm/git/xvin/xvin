
# include <stdlib.h>
# include <string.h>

#include "xvin.h"
#include "xv_tools_lib.h"

/*****************************************************************************/
/* this file provides a set of usefull tools to                              */
/* - manipulate char strings                                                 */
/* - manipulate periodic data                                                */
/* - swap bytes                                                              */
/* - detect the average frequency or dt in the x-data of a time series       */
/* - ...                                                                     */
/* (see /include/xv_tools_lib.h for more info                                */
/*****************************************************************************/


// transforms a string into an array of integer values.
// example : "1:1:4,10:5:20,3" will give [1 2 3 4 10 15 20 3]
// uefull for manipulating dataset indexes for example
int *str_to_index(char *str, int *N)
{	int	n=0, m=0;
	register int i, k;
	int	j, j2, j3, err;
	int	*index, *index_tmp;
	char	tmp[128]  = {0};
	char	c, c2;
	int	THE_SIZE_MAX=512;

	index_tmp=(int*)calloc(THE_SIZE_MAX, sizeof(int));

	m=strlen(str);
	for (i=0; i<m; i++)
	{
		if (sscanf(str+i,"%d",&j)!=0)		// on a trouve un entier
		{	index_tmp[n]=j;
			n++;

			sprintf(tmp,"%d",j);//	win_printf_OK("i found %d and the size it occupied was %d", j, strlen(tmp));
			i+=strlen(tmp);

		if (sscanf(str+i,"%c%d",&c,&j2)==2)
		{	if ( (c=='-') || (c==':') )
			{//	win_printf("tiret ou : detecte");
				i++;

				sprintf(tmp,"%d",j2);
				i+=strlen(tmp);

				err=sscanf(str+i,"%c%d",&c2,&j3);
				if ( (err==2) && (c2==':') )
		  		{//	win_printf("alors j'ai une boucle de %d a %d avec un pas de %d", j, j3, j2);
					sprintf(tmp,"%d",j3);
					i+=strlen(tmp);

					if ( (j<j3) && (j2>0) && (((j3-j)/j2)+n<THE_SIZE_MAX) )
					for (k=j+j2; k<=j3; k+=j2, n++) index_tmp[n]=k;
					else return(NULL);
				}
				else 
				{//	win_printf("alors j'ai une boucle de %d a %d", j, j2);
					if ( (j<j2) && (j2-j+n<THE_SIZE_MAX) )
					for (k=j+1; k<=j2; k++, n++) index_tmp[n]=k;
					else return(NULL);
				}
				
			}
		}

		}	
	}

	index=(int*)calloc(n, sizeof(int));
	for (i=0; i<n; i++)	index[i]=index_tmp[i]; 
	if (index_tmp) free(index_tmp);

	*N=n;
	return index;
}


// transforms a string into an array of float values.
// example : "1:1:4,10:5:20,3" will give [1 2 3 4 10 15 20 3]
float *str_to_float(char *str, int *N)
{	int	n=0, m=0;
	register int i;
	int	err;
	float x, x2, x3, k;
	float	*index, *index_tmp;
	char	tmp[128]  = {0};
	char	c, c2;
	int	THE_SIZE_MAX=512;

	index_tmp=(float*)calloc(THE_SIZE_MAX, sizeof(float));

	m=strlen(str);
	for (i=0; i<m; i++)
	{
		if (sscanf(str+i,"%f",&x)!=0)		// on a trouve un entier
		{	index_tmp[n]=x;
			n++;

			sprintf(tmp,"%g",x);//	win_printf_OK("i found %d and the size it occupied was %d", j, strlen(tmp));
			i+=strlen(tmp);

		if (sscanf(str+i,"%c%f",&c,&x2)==2)
		{	if ( (c=='-') || (c==':') )
			{//	win_printf("tiret ou : detecte");
				i++;

				sprintf(tmp,"%g",x2);
				i+=strlen(tmp);

				err=sscanf(str+i,"%c%f",&c2,&x3);
				if ( (err==2) && (c2==':') )
		  		{//	win_printf("alors j'ai une boucle de %d a %d avec un pas de %d", j, j3, j2);
					sprintf(tmp,"%g",x3);
					i+=strlen(tmp);

					if ( (x<x3) && (x2>0) && (((x3-x)/x2)+n<THE_SIZE_MAX) )
					for (k=x+x2; k<=x3; k+=x2, n++) index_tmp[n]=k;
					else {       if (index_tmp) free(index_tmp);
                                 return(NULL);
                         }
				}
				else 
				{//	win_printf("alors j'ai une boucle de %d a %d", j, j2);
					if ( (x<x2) && (x2-x+n<THE_SIZE_MAX) )
					for (k=x+1; k<=x2; k++, n++) index_tmp[n]=k;
					else {      if (index_tmp) free(index_tmp);
                                return(NULL);
                         }
				}
				
			}
		}

		}	
	}

	index=(float*)calloc(n, sizeof(float));
	for (i=0; i<n; i++)	index[i]=index_tmp[i]; 
	if (index_tmp) free(index_tmp);

	*N=n;
	return index;
}


// to test the above function
int do_try_str_to_index(void)
{	char	chaine[128]="1,2;10,2:2:8";
	int	ni=0, nf=0;
	int	*index=NULL;
	float *x=NULL;
	int	i;
	char	*message=NULL;
	
	if(updating_menu_state != 0)	return D_O_K;

	if (win_scanf("type in a string\%s", chaine)==WIN_CANCEL) return(D_O_K);
	message = my_sprintf(message, "string was : {\\color{lightgreen}%s}\n", chaine);

	index = str_to_index(chaine, &ni);
	x     = str_to_float(chaine, &nf);
	
    if (index==NULL) message = my_sprintf(message, "cannot convert into integers!\n");
	else 
    { message = my_sprintf(message, "integers found: %d values\n", ni);
	  for (i=0; i<ni; i++)
	  {	message = my_sprintf(message, "%d /", index[i]);
	  }
    }
    message = my_sprintf(message, "\n\n");
    
	if (x==NULL)     message = my_sprintf(message, "cannot convert into floats!\n");
    else 
    { message = my_sprintf(message, "floats found: %d values\n", nf);
	  for (i=0; i<nf; i++)
	  {	message = my_sprintf(message, "%g /", x[i]);
	  }
    }
	
	win_printf("%s",message);
	free(message);
	free(index);
	free(x);
	
    return(D_O_K);
}





// 'parameter' is a string to search for in 'string'
// example : 
//	to find tau (an integer) in ds->treatment, we will call
// 	tau = grep_int_in_string(ds->treatment, "tau=");
//
// we return -1 if something went wrong
int grep_int_in_string(char *string, char *parameter)
{	int	tau=-1, j;
	int	l,m=0,n;
	char	*c;//fc;

	if (string==NULL) return(-1);

	l=strlen(string);
	//fc=parameter[0];
	m=0;
	do 
	{ c=strchr(string+m, parameter[0]);
	  if (c==NULL) m=l;
	  else	{ 	n=strncmp(c, parameter, strlen(parameter));
		/*	win_printf_OK("length of inital string is l=%d\nsearching for letter '%c'\n"
					"m=%d\nn=%d\nlength of c=%d\n\n...%s", l, fc, m, n, strlen(c), c);
		*/	m=(l-strlen(c))+1;
			if (n==0)
			if (sscanf(c+strlen(parameter),"%d",&j)!=0)
			if (j>0)	
			{	tau=j;
				m=l;
			}
		}
	} 
	while (m<l);
	
	return(tau);
}




// 'parameter' is a string to search for in 'string'
// example : 
//	to find x (a float) in ds->treatment, we will call
// 	x = grep_float_in_string(ds->treatment, "x=");
// we return the found value, or 0 if something went wrong...
float grep_float_in_string(char *string, char *parameter)
{	float x=0.;
	int	  l,m=0,n;
	char  *c = NULL;  //, fc;

	if (string==NULL) return(-1);

	l=strlen(string);
	//fc=parameter[0];
	m=0;
	do 
	{ c=strchr(string+m, parameter[0]);
	  if (c==NULL) m=l;
	  else	{ 	n=strncmp(c, parameter, strlen(parameter));
		/*	win_printf_OK("length of inital string is l=%d\nsearching for letter '%c'\n"
					"m=%d\nn=%d\nlength of c=%d\n\n...%s", l, fc, m, n, strlen(c), c);
		*/	m=(l-strlen(c))+1;
			if (n==0)
			if (sscanf(c+strlen(parameter),"%f",&x)!=0)
			{	m=l; // done !
			}
		}
	} 
	while (m<l);
	
	return(x);
}




int *find_interval_double(double *x, int n_x, double x_min, double x_max, int *N)
{	register int i;
	int n=0;
	int	*index = NULL, *index_tmp = NULL;

	index_tmp = (int*)calloc(n_x, sizeof(int));
	n=0;
	for (i=0; i<n_x; i++)
	{	if ( (x_min<=x[i]) && (x[i]<=x_max) )
		{	index_tmp[n] = i;
			n++;
		}
	}

	index = (int*)calloc(n, sizeof(int));
	memcpy(index, index_tmp, n*sizeof(int));
	*N=n;
	return(index);
}

int *find_interval_float(float *x, int n_x, float x_min, float x_max, int *N)
{	register int i;
	int n=0;
	int	*index = NULL, *index_tmp=NULL;

	index_tmp = (int*)calloc(n_x, sizeof(int));
	n=0;
	for (i=0; i<n_x; i++)
	{	if ( (x_min<=x[i]) && (x[i]<=x_max) )
		{	index_tmp[n] = i;
			n++;
		}
	}

	index = (int*)calloc(n, sizeof(int));
	memcpy(index, index_tmp, n*sizeof(int));
	if (index_tmp!=NULL) free(index_tmp);
	*N=n;
	return(index);
}

/* when index is an array of int, this function reduces index in such a way 	*/
/* that only the first indexes of a serie of continuous ones is kept		*/
int *find_and_keep_firsts_int(int *index, int n_in, int *n_out)
{	int *index_tmp=NULL, *index_out;
	int	N;
	register int i;

	index_tmp=(int*)calloc(n_in, sizeof(int));
	index_tmp[0]=index[0];
	N=1;
	for (i=1; i<n_in; i++)
	{	if (index[i]>(index[i-1]+1))
		{	index_tmp[N]=index[i];
			N++;
		}
	}

	index_out = (int*)calloc(N, sizeof(int));
	memcpy(index_out, index_tmp, N*sizeof(int));
	if (index_tmp!=NULL) free(index_tmp);

	*n_out=N;
	return(index_out);
}



/* when index is an array of int, this function reduces index in such a way 	*/
/* that only the last indexes of a serie of continuous ones is kept		*/
int *find_and_keep_lasts_int(int *index, int n_in, int *n_out)
{	int *index_tmp=NULL, *index_out;
	int	N;
	register int i;

	index_tmp=(int*)calloc(n_in, sizeof(int));
	if (index_tmp == NULL) return NULL;  // added vc 
	index_tmp[0]=index[0];
	N=0; // count the number of different compacts that have been already scanned, minus 1
	for (i=1; i<n_in; i++)
	{	if (index[i]==(index[i-1]+1))
		{	index_tmp[N]=index[i];
		}
		else	N++;
	}
	// do not forget the last point:
	if (index[n_in-1]==index_tmp[N]) N++;

	index_out = (int*)calloc(N, sizeof(int));
	memcpy(index_out, index_tmp, N*sizeof(int));
	if (index_tmp!=NULL)  free(index_tmp);

	*n_out=N;
	return(index_out);
}


int *find_and_keep_middles_int(int *index, int n_in, int *n_out)
{	
  int *index_begin = NULL, *index_end = NULL, *index_out = NULL;
  int	n_begin, n_end;
  register int i;
  
  index_begin   = find_and_keep_firsts_int(index, n_in, &n_begin);
  index_end	  = find_and_keep_lasts_int( index, n_in, &n_end);
  if (index_begin == NULL || index_end == NULL) 
    { *n_out=0; return(NULL); }
  if (n_begin!=n_end) { *n_out=0; return(NULL); }
      
  index_out = (int*)calloc(n_begin, sizeof(int));
  if (index_out == NULL) { *n_out=0; return(NULL); }
  for (i=0; i<n_begin; i++)
    index_out[i]=(int)((index_begin[i]+index_end[i])/2);
  
  if (index_begin) free(index_begin);
  if (index_end)   free(index_end);
  
  *n_out=n_begin;
  return(index_out);

}





/* to convert between little-big endian saved data:	*/
void swap_bytes(void *a, int n, int nb) 
{	char tmp[16] = {0}; 
        char* b=(char*)a; 
        int i,j; 
        char* cd=tmp; 
        char* cs; 
			
       for (i=0;i<n;i++)
	{	cs=&b[nb*i];
         	for (j=0;j<nb;j++) cd[j]=cs[nb-1-j];
  		memcpy(cs,cd,nb); 
     } 
}
//	example: swap_bytes( array_of_floats,23, sizeof(float));
//	to convert an array containing 23 floats.




int unwrap(float *phase, int nx)
{	register int i, j;
	for (i=1; i<nx; i++)
	{	if ( fabs((phase)[i]-(phase)[i-1]) >= M_PI)
		{
			if (phase[i]<phase[i-1]) // we are -2\pi to low, so correct this	
				for (j=i; j<nx; j++)	(phase)[j] += 2.*M_PI;
			else
				for (j=i; j<nx; j++)	(phase)[j] -= 2.*M_PI;
		}
	}
	return(0);
}
		
		
float find_dt_average_from_time_array(float *t, int n)
{     
      if (n>1) return( (t[n-1]-t[0])/(n-1) );
      return(0.);
}
	
float find_dt_start_from_time_array(float *t, int n)
{     
      if (n>1) return( (t[1]-t[0]) );
      return(0.);
}


/**************************************************************/
/* to detect the sampling frequency (e.g. acquisition freq.)  */
/* returns positive sampling frequency on success             */
/* returns 0 is vanishing sampling frequency				  */
/* returns the opposite of average sampling frequency if it   */
/*        is not constant along the dataset                   */
/**************************************************************/
float find_sampling_frequency(float *t, int n)
{     float f1, fm, dt;
      
      dt=find_dt_average_from_time_array(t, n);
      if (dt==0) return(0.);
      fm=(float)1./dt;
      
      dt=find_dt_start_from_time_array(t, n);
      if (dt==0) return(0.);
      f1=(float)1./dt;
      
      if (f1==fm) return(fm);
      
      return(-fm);
}

/**************************************************************/
/* to detect the sampling frequency (e.g. acquisition freq.)  */
/* returns positive sampling frequency on success             */
/* returns 0 is vanishing sampling frequency				  */
/* returns the opposite of average sampling frequency if it   */
/*        is not constant along the dataset                   */
/*                                                            */
/* same as above, but with dialogs                            */
/**************************************************************/
float find_sampling_frequency_dialog(float *t, int n)
{	 float f_acq=find_sampling_frequency(t, n);
	
	 if (f_acq==0.) 
	 { if (win_printf("I detected that sampling frequency is vanishing...\n\n"
	 			 "click OK to set it to 1, or WIN_CANCEL to exit")==WIN_CANCEL)
	 	  return(0.);     // if user cancels : we return a negative frequency
	 else return(1.);     // if user wants to set sampling frequency to 1
     }
  
	 if (f_acq<0.) 
	 { if (win_printf("I detected that sampling frequency is not constant...\n\n"
	 			 "click OK to set it to average frequency %f Hz\n"
			  "or click WIN_CANCEL to exit",f_acq)==WIN_CANCEL)
	 	  return(f_acq);  // if user cancels : we return a negative frequency
	 else return(-f_acq); // if user wants average frequency
     }
  
	 return(f_acq); // if everything is correct
}
