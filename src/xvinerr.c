/*
 *      Error handling module
 *
 */

# define _XVINERR_C_

# include "xvin/platform.h"
#   include"xvinerr.h"

# include <stdlib.h>
# include <string.h>

/*  error definition */

xv_err      *xvin_errors = NULL; // = {NULL, 0, 0, 0};
list_xv_err *last_xvin_errors = NULL; //= {NULL, NULL, NULL, 0,0,0};


int     xvin_error_init(void);

int         No_error;
/*      this is the first error returning 0 which corresponds to no error */

int         Wrong_Argument;
int         Out_Of_Memory;
int         Fft_Not_Supported;
int         Wrong_Option;
int         No_Data;

char *xv_log_file = "xvinerr.log";

int     register_error(const char *newtype)
{
  int m;
  char **cp = NULL; 

    if (newtype == NULL)    return -1;

    for (int i = 0; i < xvin_errors->n_err; i++)
        if (strcmp(newtype, xvin_errors->type[i]) == 0)   return i;

    m = xvin_errors->m_err;

    if (xvin_errors->n_err >= m)
      {
        m += 16;
        cp = (char **)realloc(xvin_errors->type, m * sizeof(char *));
	if (cp == NULL) return 0;
	xvin_errors->type = cp;
	xvin_errors->m_err = m;
      }
    xvin_errors->type[xvin_errors->n_err] = strdup(newtype);
    return (xvin_errors->n_err++);
}



char    *describe_error(int ntype)
{
    return (ntype < 0 || ntype >= xvin_errors->n_err) ? NULL :
           xvin_errors->type[ntype];
}

int     xvin_error_occured(int ntype, int line, char *file)
{
    int pb = 0;
    int *np = NULL, *nl = NULL;
    char **ch = NULL;
    xvin_error_init();

    if (ntype < 0 || ntype > xvin_errors->n_err)   return -1;

    for (; last_xvin_errors->m_err <= last_xvin_errors->n_err && pb == 0;)
    {
        last_xvin_errors->m_err += 16;
        np = (int *)realloc(last_xvin_errors->nerr, last_xvin_errors->m_err *
                            sizeof(int));
        nl = (int *)realloc(last_xvin_errors->n_line, last_xvin_errors->m_err *
                            sizeof(int));
        ch = (char **)realloc(last_xvin_errors->file, last_xvin_errors->m_err *
                              sizeof(char *));

        if (np != NULL && nl != NULL && ch != NULL)
        {
            last_xvin_errors->nerr = np;
            last_xvin_errors->n_line = nl;
            last_xvin_errors->file = ch;
        }
        else pb = 1;
    }

    if (pb == 0)
    {
        last_xvin_errors->nerr[last_xvin_errors->n_err] = ntype;
        last_xvin_errors->n_line[last_xvin_errors->n_err] = line;
        last_xvin_errors->file[last_xvin_errors->n_err++] = file;
    }

    return (ntype);
}
void    *xvin_ptr_error_occured(int ntype, int line, char *file)
{
    xvin_error_occured(ntype, line, file);
    return NULL;
}

int     xvin_error_init(void)
{
    static int done = 0;

    if (done)
        return 0;

    xvin_errors = (xv_err *) calloc(1, sizeof(xv_err));
    last_xvin_errors = (list_xv_err *) calloc(1, sizeof(list_xv_err));

    xvin_errors->n_err = 0;
    xvin_errors->c_err = 0;
    xvin_errors->m_err = 16;
    xvin_errors->type = (char **)calloc(xvin_errors->m_err, sizeof(char *));

    if (xvin_errors->type == NULL)
        return 1;

    last_xvin_errors->n_err = 0;
    last_xvin_errors->c_err = 0;
    last_xvin_errors->m_err = 16;
    last_xvin_errors->file = NULL;
    last_xvin_errors->nerr = (int *)calloc(last_xvin_errors->m_err, sizeof(int));

    if (last_xvin_errors->nerr == NULL)
        return 1;

    last_xvin_errors->n_line = (int *)calloc(last_xvin_errors->m_err, sizeof(int));

    if (last_xvin_errors->n_line == NULL)
        return 1;

    last_xvin_errors->file = (char **) calloc(last_xvin_errors->m_err, sizeof(char *));

    if (last_xvin_errors->file == NULL)
        return 1;

    No_error = register_error("No error");
    Wrong_Argument      = register_error("NULL argument in function call");
    Out_Of_Memory       = register_error("Out of memory");
    Fft_Not_Supported   = register_error("You ask to compute fft with a "
                                         "number of point not a power of 2");
    Wrong_Option        = register_error("The option is not supported");
    No_Data             = register_error("No data was found");
    
    done = 1;

    return 0;
}
char    local_error_buffer[2048];

char    *first_xvin_error(void)
{
    if (last_xvin_errors->n_err == 0) return  NULL;

    last_xvin_errors->c_err = 0;
    snprintf(local_error_buffer, 2048, "Error in %s at line %d : %s\n",
             last_xvin_errors->file[0], last_xvin_errors->n_line[0],
             xvin_errors->type[(last_xvin_errors->nerr[0])]);
    return local_error_buffer;
}
char    *next_xvin_error(void)
{
    int i;

    if (last_xvin_errors->c_err >= last_xvin_errors->n_err) return  NULL;

    last_xvin_errors->c_err++;
    i = last_xvin_errors->c_err;
    snprintf(local_error_buffer, 2048, "Error in %s at line %d : %s\n",
             last_xvin_errors->file[i], last_xvin_errors->n_line[i],
             xvin_errors->type[(last_xvin_errors->nerr[i])]);
    return local_error_buffer;
}

int dump_to_xv_log_file_with_date_and_time(const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char s[256] = {0};
    struct tm *t = NULL;
    time_t ti;
    char c[2048] = {0};



    if (fmt == NULL)    return 1;

    ti = time(0);
    t = localtime(&ti);
    strftime(s, 256, "%d/%m/%Y %H:%M:%S", t);
    fp = fopen(xv_log_file, "a");

    if (fp == NULL)                         return 1;

    fprintf(fp, "%s\n", s);
    va_start(ap, fmt);
    vsnprintf(c, 2048, fmt, ap);
    va_end(ap);
    fprintf(fp, "%s\n", c);
    return fclose(fp);
}
int dump_to_xv_log_file_with_time(const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char s[256] = {0};
    struct tm *t = NULL;
    time_t ti;
    char c[2048] = {0};


    if (fmt == NULL)    return 1;

    ti = time(0);
    t = localtime(&ti);
    strftime(s, 256, "%X", t);
    fp = fopen(xv_log_file, "a");

    if (fp == NULL)                         return 1;

    fprintf(fp, "%s\n", s);
    va_start(ap, fmt);
    vsnprintf(c, 2048, fmt, ap);
    va_end(ap);
    fprintf(fp, "%s\n", c);
    return fclose(fp);
}
int dump_to_xv_log_file_only(const char *fmt, ...)
{
    va_list ap;
    FILE *fp = NULL;
    char c[2048] = {0};

    if (fmt == NULL)    return 1;

    fp = fopen(xv_log_file, "a");

    if (fp == NULL)                         return 1;

    va_start(ap, fmt);
    vsnprintf(c, 2048, fmt, ap);
    va_end(ap);
    fprintf(fp, "%s\n", c);
    return fclose(fp);
}



# undef _XVINERR_C_





