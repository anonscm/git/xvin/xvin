#include "ds_viewer.h"
#include "xvin.h"
#include <gtk/gtk.h>
#include <glib.h>

enum
{
    DS_COLUMN_INDEX,
        DS_COLUMN_XD,
        DS_COLUMN_YD,
        DS_COLUMN_XE,
        DS_COLUMN_YE
};

bool is_raw_data_windows_open = false;

void ds_viewer_ok_button_pressed(GtkWidget *object, gpointer user_data)
{
  (void)object;
  (void)user_data;
    is_raw_data_windows_open = false;
}

void display_raw_data(d_s *ds)
{
    GtkBuilder      *builder;
    GtkWidget       *window;
    GtkListStore    *dataset_store;
    GtkLabel        *ds_title;
    GtkTreeIter iter;
    GtkTreeView *treeview;
    GError *err = NULL;

    if (is_raw_data_windows_open || ds == NULL)
    {
        return;
    }

    is_raw_data_windows_open = true;
    gtk_init(0, NULL);
    builder = gtk_builder_new();
    gtk_builder_add_from_file(builder, "ds_viewer.glade", &err);
    if (err != NULL)
    {
        warning_message("can't open %s\n: %s", "ds_viewer.glade", err->message);
        return;
    }
    window = GTK_WIDGET(gtk_builder_get_object(builder, "dataset_viewer_window"));
    treeview = GTK_TREE_VIEW(gtk_builder_get_object(builder, "dataset_treeview"));
    dataset_store = GTK_LIST_STORE(gtk_builder_get_object(builder, "dataset_store"));
    ds_title = GTK_LABEL(gtk_builder_get_object(builder, "dataset_label"));
    gtk_builder_connect_signals(builder, NULL);

    if (ds->source)
    {
        gtk_label_set_text(ds_title, ds->source);
    }

    for (int i = 0; i < ds->nx; ++i)
    {
        gtk_list_store_append(dataset_store, &iter);
        gtk_list_store_set(dataset_store, &iter,
                           DS_COLUMN_INDEX, i,
                           DS_COLUMN_XD, ds->xd[i],
                           DS_COLUMN_YD, ds->yd[i],
                           -1);

        if (ds->xe)
        {
            gtk_list_store_set(dataset_store, &iter, DS_COLUMN_XE, ds->xe[i], -1);
        }

        if (ds->ye)
        {
            gtk_list_store_set(dataset_store, &iter, DS_COLUMN_YE, ds->ye[i], -1);
        }
	// add other error !
    }

    gtk_tree_view_set_model(treeview, GTK_TREE_MODEL(dataset_store));
    gtk_widget_show(window);

    while (is_raw_data_windows_open || gtk_events_pending())
    {
        gtk_main_iteration();
    }

    gtk_widget_hide(window);
    g_object_unref(G_OBJECT(builder));
    gtk_widget_destroy(window);

    while (gtk_events_pending())
    {
        gtk_main_iteration();
    }
}


