#include "file_picker_gui.h"
#include "util.h"
#include<stdlib.h>
#ifdef XV_GTK_DIALOG
#include <glib/gi18n.h>
#endif
#include <assert.h>
#include <unistd.h>
//#define MAX_CONFIG_LIST 16

file_list_t *open_files_internal(const char *dialog_title, const char *current_uri, const char *filter,
                                 bool is_multiple)
{
    char *fulltitle = (char *) malloc(sizeof(char) * (20 + strlen(dialog_title)));
    file_list_t *file_list = NULL;

    char filebuf[16384] = {0};
#ifndef XV_GTK_DIALOG
    char path[512];
#endif
    (void)is_multiple;
    fulltitle = strcpy(fulltitle, "xvin - ");
    fulltitle = strcat(fulltitle, dialog_title);

    if (full_screen)
    {
        switch_allegro_font(1);
        strcpy(filebuf, current_uri);

        if (file_select_ex(dialog_title, filebuf, filter, sizeof(filebuf), 0, 0) != 0)
        {
            file_list = (file_list_t *) malloc(sizeof(file_list_t));
            file_list->data = strdup(filebuf);
            file_list->next = NULL;
        }

        switch_allegro_font(0);
    }
    else
    {
      //#ifndef XV_GTK_DIALOG
# if defined(XV_WIN32) && !defined(XV_GTK_DIALOG)             
        extract_file_path(path, sizeof(path), current_uri);
        extract_file_name(filebuf, sizeof(filebuf), current_uri);
        put_backslash(path);

        char *files_str = DoFileOpen(dialog_title, path, filebuf, sizeof(filebuf), filter, "");
        file_list = win_string_to_file_list(files_str);
#else

        GtkWidget *dialog = NULL;
        dialog = gtk_file_chooser_dialog_new(fulltitle,
                                             NULL,
                                             GTK_FILE_CHOOSER_ACTION_OPEN,
                                             _("_Cancel"), GTK_RESPONSE_CANCEL,
                                             _("_Open"), GTK_RESPONSE_ACCEPT,
                                             NULL);
        //gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), current_uri);
        gtk_file_chooser_select_filename(GTK_FILE_CHOOSER(dialog), current_uri ? current_uri : "");
        gtk_file_chooser_set_select_multiple(GTK_FILE_CHOOSER(dialog), is_multiple);

        add_win_filters_to_gtk_choose_dialog(GTK_FILE_CHOOSER(dialog), filter);

        if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT)
        {

            GSList *fl;

            fl = gtk_file_chooser_get_filenames(GTK_FILE_CHOOSER(dialog));

            file_list = GSList_to_filelist(fl);

        }

        gtk_widget_destroy(dialog);

        while (gtk_events_pending())
            gtk_main_iteration();

#endif

    }

    return file_list;
}

char *select_folder(const char *dialog_title, const char *current_uri)
{
    char *fulltitle = (char *) malloc(sizeof(char) * (20 + strlen(dialog_title)));
    char *res = NULL;

    char filebuf[16384] = {0};
#ifndef XV_GTK_DIALOG
    char path[512];
#endif

    fulltitle = strcpy(fulltitle, "xvin - ");
    fulltitle = strcat(fulltitle, dialog_title);

    if (full_screen)
    {
        switch_allegro_font(1);
        strcpy(filebuf, current_uri);

        if (file_select_ex(dialog_title, filebuf, "*", sizeof(filebuf), 0, 0) != 0)
        {
            res = strdup(filebuf);
        }

        switch_allegro_font(0);
    }
    else
    {
#ifndef XV_GTK_DIALOG
        extract_file_path(path, sizeof(filebuf), current_uri);
        extract_file_name(filebuf, sizeof(filebuf), current_uri);

        char *files_str = DoFileOpen(dialog_title, path, filebuf, sizeof(filebuf), "\x01", "");
        res = files_str;
#else

        GtkWidget *dialog = NULL;
        dialog = gtk_file_chooser_dialog_new(fulltitle,
                                             NULL,
                                             GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                             _("_Cancel"), GTK_RESPONSE_CANCEL,
                                             _("_Open"), GTK_RESPONSE_ACCEPT,
                                             NULL);
        //gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), current_uri);
        gtk_file_chooser_select_filename(GTK_FILE_CHOOSER(dialog), current_uri);

        if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT)
        {

            res = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));

        }

        gtk_widget_destroy(dialog);

        while (gtk_events_pending())
            gtk_main_iteration();

#endif

    }

    return res;
}

char *select_folder_config(const char *dialog_title, const char *current_dir, const char *config_cat,
                           const char *config_var)
{
    char *res = NULL;
    char cwd[256] = {0};

    if (current_dir == NULL || strcmp(current_dir, "") == 0)
    {
        current_dir = get_config_string(config_cat, config_var, "");

        if (strcmp(current_dir, "") == 0)
        {
            current_dir = get_config_string("GR-FILE-PATH", "last_visited_0", "");
            if (strcmp(current_dir, "") == 0)
            {
                if (my_getcwd(cwd, sizeof(cwd)))
                {
                    current_dir = cwd;
                }
            }
        }
    }

    if ((res = select_folder(dialog_title, current_dir)))
    {
        set_config_string(config_cat, config_var, res);
        flush_config_file();
    }

    return res;
}


file_list_t *open_files_config_internal(const char *dialog_title, const char *current_uri, const char *filter,
                                        bool is_multiple, const char *config_cat, const char *config_var)
{
    file_list_t *res = NULL;
    char cwd[256] = {0};

    if (current_uri == NULL || strcmp(current_uri, "") == 0)
    {
        current_uri = get_config_string(config_cat, config_var, "");

        if (strcmp(current_uri, "") == 0)
        {
            if (my_getcwd(cwd, sizeof(cwd)))
            {
                current_uri = cwd;
            }
        }

#ifdef RECENT_LIST_WIP

        if (is_multiple)
        {
            char *nfile = NULL;
            sprintf(nfile, "%s_0", config_var);
            current_uri = get_config_string(config_cat, config_var, my_getcwd());
        }

#endif
    }

    if ((res = open_files_internal(dialog_title, current_uri, filter, is_multiple)))
    {
        set_config_string(config_cat, config_var, res->data);

#ifdef RECENT_LIST_WIP
        int config_list_count = 0;

        if (is_multiple)
        {
            char cur_var[1024] = {'\0'};
            char file_count = 0;
            file_list_t *cursor = res;

            // counting new files
            while (cursor != 0)
            {
                file_count++;
                cursor = cursor->next;
            }

            // making some space on the config list
            for (config_list_count = file_count; config_list_count < MAX_CONFIG_LIST; ++config_list_count)
            {

                snprintf(cur_var, sizeof(cur_var), "%s_%i", config_var, config_list_count - file_count);
                const char *old = get_config_string(config_cat, cur_var, NULL);

                if (old == NULL)
                {
                    break;
                }

                snprintf(cur_var, sizeof(cur_var), "%s_%i", config_var, config_list_count);
                set_config_string(config_cat, cur_var, old);
            }

            file_count = 0;
            cursor = res;

            //inserting new element in config list
            while (cursor != 0)
            {
                file_count++;
                cursor = cursor->next;
                snprintf(cur_var, sizeof(cur_var), "%s_%i", config_var, file_count);
                set_config_string(config_cat, cur_var, cursor->data);
            }

            snprintf(cur_var, 1024, "%s_n", config_var);
            set_config_int(config_cat, cur_var, config_list_count);
        }

#endif
        flush_config_file();
    }

    return res;
}

file_list_t *open_files(const char *dialog_title, const char *current_uri, const char *filter)
{
    return open_files_internal(dialog_title, current_uri, filter, true);
}

char *open_one_file(const char *dialog_title, const char *current_uri, const char *filter)
{
    char *res = NULL;
    file_list_t *fl = open_files_internal(dialog_title, current_uri, filter, false);

    if (fl)
    {
        res = strdup(fl->data);
        free_file_list(fl);
    }

    return res;
}

file_list_t *open_files_config(const char *dialog_title, const char *current_uri, const char *filter,
                               const char *config_cat, const char *config_var)
{
    return open_files_config_internal(dialog_title, current_uri, filter, true, config_cat, config_var);
}

char *open_one_file_config(const char *dialog_title, const char *current_uri, const char *filter,
                           const char *config_cat, const char *config_var)
{
    char *res = NULL;
    file_list_t *fl = open_files_config_internal(dialog_title, current_uri, filter, false, config_cat, config_var);

    if (fl)
    {
        res = strdup(fl->data);
        free_file_list(fl);
    }

    return res;
}

char *save_one_file(const char *dialog_title, const char *current_dir, const char *filename, const char *filter)
{
    char *fulltitle = (char *) malloc(sizeof(char) * (20 + strlen(dialog_title)));
    char *choosen_file = NULL;
    char file[1024] = {0};
    char path[1024] = {0};
    char fullfilebuf[16384] = {0};
    char current_uri[16384] = {0};

    fulltitle = strcpy(fulltitle, "xvin - ");
    fulltitle = strcat(fulltitle, dialog_title);

    if (filename == NULL || filename[0] == '\0')
    {
        char *ext = get_default_extension(filter);
        snprintf(file, sizeof(file), "Untitled.%s", ext);
        filename = file;
        free(ext);
    }

    // unsure correct url.
    extract_file_path(path, sizeof(path), current_dir);
    get_os_specific_path(path);
    put_path_separator(path, sizeof(path));
    strncpy(current_uri, path, sizeof(current_uri));
    my_strncat(current_uri, filename, sizeof(current_uri));
    strncpy(fullfilebuf, current_uri, sizeof(fullfilebuf)); // was filename
    
    if (full_screen)
    {
        switch_allegro_font(1);

        if (current_dir != NULL && current_dir[0] != '\0')
        {
            strcpy(fullfilebuf, current_dir);
            put_backslash(fullfilebuf);
        }

        strcat(fullfilebuf, filename);

        if (file_select_ex(dialog_title, fullfilebuf, filter, sizeof(fullfilebuf), 0, 0) != 0)
        {
            choosen_file = strdup(fullfilebuf);
        }

        switch_allegro_font(0);

        if (choosen_file && !(do_you_want_to_overwrite(choosen_file, NULL, dialog_title) == D_O_K))
        {
            free(choosen_file);
            choosen_file = NULL;
        }
    }
    else
    {
//#ifndef XV_GTK_DIALOG
# if defined(XV_WIN32) && !defined(XV_GTK_DIALOG)       
	
        choosen_file = DoFileSave(dialog_title, path, fullfilebuf, sizeof(fullfilebuf), filter, "");

        if (choosen_file != NULL && choosen_file[0] != '\0')
        {
            choosen_file = strdup(choosen_file);
        }
	
#else

        GtkWidget *dialog = NULL;
        dialog = gtk_file_chooser_dialog_new(fulltitle,
                                             NULL,
                                             GTK_FILE_CHOOSER_ACTION_SAVE,
                                             _("_Cancel"), GTK_RESPONSE_CANCEL,
                                             _("_Save"), GTK_RESPONSE_ACCEPT,
                                             NULL);


        if (file_exists(current_uri, 0, NULL))
        {
            gtk_file_chooser_select_filename(GTK_FILE_CHOOSER(dialog), current_uri);
        }
        else
        {
            gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), path);
            gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), filename);
        }

        add_win_filters_to_gtk_choose_dialog(GTK_FILE_CHOOSER(dialog), filter);

        gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), 1);


        if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT)
        {
            choosen_file = strdup(gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog)));
        }

        gtk_widget_destroy(dialog);

        while (gtk_events_pending())
            gtk_main_iteration();

#endif
    }

    return choosen_file;
}

char *save_one_file_config(const char *dialog_title, const char *current_dir, const char *filename, const char *filter,
                           const char *config_cat, const char *config_var)
{
    char *res = NULL;
    char path[1024] = {0};
    char filen[512] = {0};

    if (current_dir == NULL || strcmp(current_dir, "") == 0)
    {
        current_dir = get_config_string(config_cat, config_var, "");

        if (strcmp(current_dir, "") == 0)
        {
            current_dir = get_config_string("GR-FILE-PATH", "last_visited_0", "");
            if (strcmp(current_dir, "") == 0)
            {
                if (my_getcwd(path, sizeof(path)))
                {
                    current_dir = path;
                }
            }
        }
        else
        {
            extract_file_path(path, sizeof(path), current_dir);
            //extract_file_name(file, sizeof(file), current_dir);

            current_dir = path;
            //filename = NULL;
        }
    }
    if (filename == NULL || strlen(filename) < 1)
      snprintf(filen,sizeof(filen),"Untiteled.gr");
    else snprintf(filen,sizeof(filen),"%s",filename);
    if ((res = save_one_file(dialog_title, current_dir, filen, filter)))
    {
        set_config_string(config_cat, config_var, res);
        flush_config_file();
    }

    return res;
}

#ifndef XV_GTK_DIALOG
file_list_t *win_string_to_file_list(char *files_str)
{
    const char *path = NULL;
    int path_len = 0;
    char *str_cursor = NULL;
    file_list_t *file_list = NULL;
    file_list_t *list_cursor = NULL;

    if (files_str != NULL)
    {
        path = files_str;
        path_len = strlen(path);
        str_cursor = files_str + path_len + 1;

        if (str_cursor[0] != '\0')
        {
            file_list = (file_list_t *) malloc(sizeof(file_list_t));
            list_cursor = file_list;


            do
            {
                list_cursor->next = (file_list_t *) malloc(sizeof(file_list_t));
                list_cursor = list_cursor->next;
                list_cursor->data = (char *) calloc(path_len + strlen(str_cursor) + 2, sizeof(char));
                strcpy(list_cursor->data, path);
                //printf("%s \n",path);
                list_cursor->data[path_len] = get_path_separator();
                strcat(list_cursor->data, str_cursor);
                //win_printf("%s \n", str_cursor);
                //printf("%s \n", list_cursor->data);

                list_cursor->next = NULL;
                str_cursor = str_cursor + strlen(str_cursor) + 1;
            }
            while (str_cursor[0] != '\0');

            list_cursor = file_list;
            file_list = file_list->next;
            free(list_cursor);
        }
        else
        {
            file_list = (file_list_t *) malloc(sizeof(file_list_t));
            file_list->data = strdup(path);
            file_list->next = NULL;

        }
    }

    return file_list;
}
#endif

char *get_default_extension(const char *filters)
{
    const char *str_cursor = filters;
    char tmp[1024];
    const char *pattern = NULL;
    char *res = NULL;

    str_cursor = str_cursor + strlen(str_cursor) + 1;
    assert(str_cursor[0 != '\0']);
    strncpy(tmp, str_cursor, sizeof(tmp));

    pattern = strtok(tmp, ";");

    res = (char *) malloc(strlen(pattern) + 1);
    int j = 0;

    for (int i = 0; pattern[i] != '\0' && pattern[i] != ';'; i++)
    {
        if (pattern[i] != '*' && pattern[i] != '.' && pattern[i] != ' ') // FIXME : it is not very robust
        {
            res[j] = pattern[i];
            j++;
        }
    }

    res[j] = '\0';
    return res;
}

#ifdef XV_GTK_DIALOG
void add_win_filters_to_gtk_choose_dialog(GtkFileChooser *dialog, const char *filters)
{

    const char *str_cursor = filters;
    char tmp[1024];
    const char *pattern = NULL;

    while (str_cursor[0] != '\0')
    {
        GtkFileFilter *gfilter = gtk_file_filter_new();
        gtk_file_filter_set_name(gfilter, str_cursor);
        str_cursor = str_cursor + strlen(str_cursor) + 1;
        assert(str_cursor[0 != '\0']);
        strncpy(tmp, str_cursor, sizeof(tmp));

        pattern = strtok(tmp, ";");

        do
        {
            gtk_file_filter_add_pattern(gfilter, pattern);
        }
        while ((pattern = strtok(NULL, ";")));

        gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), gfilter);
        str_cursor = str_cursor + strlen(str_cursor) + 1;
    }
}


file_list_t *GSList_to_filelist(GSList *gsl)
{
    if (gsl)
    {
        file_list_t *fl = (file_list_t *) malloc(sizeof(file_list_t));
        file_list_t *curs = fl;
        fl->data = strdup((const char *)gsl->data);
        fl->next = NULL;
        gsl = gsl->next;

        while (gsl != NULL)
        {
            file_list_t *tmp = (file_list_t *) malloc(sizeof(file_list_t));
            tmp->data = strdup((const char *)gsl->data);
            tmp->next = NULL;
            curs->next = tmp;
            curs = tmp;
            gsl = gsl->next;
        }

        return fl;
    }

    return NULL;
}
#endif
void free_file_list(file_list_t *file_list)
{
    while (file_list != NULL)
    {
        file_list_t *tmp = file_list;
        file_list = file_list->next;
        free(tmp->data);
        free(tmp);
    }
}

