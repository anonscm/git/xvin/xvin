#include "progress_builder.h"
#include "xvin.h"
#include <glib/gi18n.h>
#include <glib.h>

static void cancel_event(GtkWidget *widget,
                         gpointer   *res)
{
    progress_window_t *progress = (progress_window_t *) res;
    progress->canceled = true;
    gtk_widget_hide(GTK_WIDGET(progress->dialog));
}

static void destroy_event(GtkWidget *widget,
                         gpointer   *res)
{
    progress_window_t *progress = (progress_window_t *) res;
    progress->canceled = true;
    progress->dialog = NULL;
}


progress_window_t *progress_create(const char *title)
{
   // GtkDialogFlags flags = GTK_DIALOG_MODAL; //| GTK_DIALOG_DESTROY_WITH_PARENT;
    GtkWidget *dialog, *bar, *box, *cancel, *label, *info;

    progress_window_t *res = (progress_window_t *) malloc(sizeof(progress_window_t));
    dialog = gtk_dialog_new();
    gtk_window_set_modal(GTK_WINDOW(dialog), false);
    gtk_window_set_keep_above(GTK_WINDOW(dialog), true);

    box = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    label = gtk_label_new(title);
    gtk_container_set_border_width(GTK_CONTAINER(box), 15);
    gtk_box_set_spacing(GTK_BOX(box), 7);
    bar = gtk_progress_bar_new();
    info = gtk_label_new("");
    cancel = gtk_button_new_with_label("Cancel");

    gtk_progress_bar_set_show_text(GTK_PROGRESS_BAR(bar), true);

    gtk_box_pack_start(GTK_BOX(box), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(box), bar, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(box), info, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(box), cancel, TRUE, TRUE, 0);

    g_signal_connect(cancel, "clicked", G_CALLBACK(cancel_event), (gpointer) res);
    g_signal_connect(dialog, "destroy", G_CALLBACK(cancel_event), (gpointer) res);

    res->dialog = GTK_DIALOG(dialog);
    res->bar = GTK_PROGRESS_BAR(bar);
    res->canceled = false;
    res->info_label = GTK_LABEL(info);

    return res;
}

int progress_run(progress_window_t *progress)
{
    progress_update_window(progress);
    gtk_widget_show_all(GTK_WIDGET(progress->dialog));
    gtk_widget_show_all(GTK_WIDGET(progress->bar));
    progress_update_window(progress);
    //gtk_dialog_run(progress->dialog);
    return 0;
}

int progress_free (progress_window_t *progress)
{
    //gtk_signal_emit_by_name(progress->window);
    if (progress->dialog != NULL)
    {
        gtk_widget_hide(GTK_WIDGET(progress->dialog));
        gtk_widget_destroy(GTK_WIDGET(progress->dialog));
    }

    progress_update_window(progress);
    free(progress);
    return 0;
}

int progress_set_fraction(progress_window_t *progress, double fraction)
{
    gtk_progress_bar_set_fraction(progress->bar, fraction);
    progress_update_window(progress);
    return 0;
}

double progress_get_fraction(progress_window_t *progress)
{
    return gtk_progress_bar_get_fraction(progress->bar);
}

int progress_pulse(progress_window_t *progress)
{
    gtk_progress_bar_pulse(progress->bar);
    progress_update_window(progress);
    return 0;
}

int progress_set_info_text(progress_window_t *progress, const char *text)
{
    gtk_label_set_text(progress->info_label, text);
    progress_update_window(progress);
    return 0;
}

int progress_is_canceled(progress_window_t *progress)
{
    return progress->canceled;
}

int progress_update_window(progress_window_t *progress)
{
    while (gtk_events_pending())
    {
        gtk_main_iteration();
    }
    return 0;
}
