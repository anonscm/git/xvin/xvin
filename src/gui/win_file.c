
#ifndef _WIN_FILE_C_
#define _WIN_FILE_C_

#include "platform.h"
# include "allegro.h"
# ifdef XV_WIN32
# include "winalleg.h"
# include "windows.h"

int nFileOffset = 0;

// ext_filter = "Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0\0";

char *DoFileSave(const char *FileTitle, const char *initialdir, char *szFileName, int szFileNameSize, const char *ext_filters, const char *extension)
{
   OPENFILENAME ofn;
   /*
   char lext_filters[512];

   if (ext_filters == NULL) strcpy(lext_filters,ext_filters);
   else lext_filters[0] = 0;*/
   ZeroMemory(&ofn, sizeof(ofn));
   ofn.lStructSize = sizeof(ofn);
   ofn.hwndOwner = win_get_window();
   ofn.lpstrFilter = ext_filters;
   ofn.lpstrCustomFilter = NULL;
   ofn.lpstrFileTitle = NULL;
   ofn.lpstrFile = szFileName;
   ofn.nMaxFile = szFileNameSize;
   ofn.lpstrDefExt = extension;
   ofn.lpstrInitialDir = initialdir; 
   ofn.lpstrTitle = FileTitle; 
   ofn.Flags = OFN_EXPLORER | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
         
   if(GetSaveFileName(&ofn))
   {
       return szFileName;
   }
   else
   {
       return NULL;
   }

}


char *DoFileOpen(const char *FileTitle, const char *initialdir,  char *szFileName, int szFileNameSize, const char *ext_filters, const char *extension)
{
   OPENFILENAME ofn;

   ZeroMemory(&ofn, sizeof(ofn));

   ofn.lStructSize = sizeof(ofn);
   ofn.hwndOwner = win_get_window();
   ofn.lpstrFilter = ext_filters;
   ofn.lpstrFileTitle = szFileName;
   ofn.lpstrFile = szFileName;
   ofn.nMaxFile = szFileNameSize;
   ofn.lpstrDefExt = extension;
   ofn.lpstrInitialDir = initialdir; 
   ofn.lpstrTitle = FileTitle; 

   ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT;
   if(GetOpenFileName(&ofn))  
     {
       nFileOffset = ofn.nFileOffset; 
       return szFileName;
     }
   else return NULL;
}
# endif
# endif











