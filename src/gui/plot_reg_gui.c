
#ifndef _PLOT_REG_GUI_C_
#define _PLOT_REG_GUI_C_






#include "config.h"

# include "allegro.h"
# ifdef XV_WIN32
# include "winalleg.h"
# endif

# include "plot_reg_gui.h"
# include "plot2box.h"
# include "box2alfn.h"
# include "xvin.h"
# include "dev_pcx.h"
# include "ds_viewer.h"
# include <stdlib.h>
# include <string.h>
#include <assert.h>


# include <alfont.h>

#define DS_ALL      0x00100000

int ds_mark_h = 40;
int ds_mark_w = 25;
int op_mark_w = 120;
int op_mark_h = 16;


int last_data_mouse_x = -1;
int last_data_mouse_y = -1;
/*
   int time_debug = 0;
   clock_t de_start = 0;

   unsigned long ucl_box, ucl_poly, ucl_char, ucl_box_start, ucl_poly_start, ucl_char_start;
   double seg_poly;
   */

extern struct plot_label     selected_point;
extern int            selected_point_enable;

extern ALFONT_FONT *user_font;

d_s *ds_copied = NULL;
O_p *opds_copied = NULL;
O_p *op_copied = NULL;
p_l p_l_copied;
d_s *ds_grabbing = NULL;
O_p *op_grabbing = NULL;

un_s *xun_plot = NULL;
un_s *yun_plot = NULL;

float xlimit_lo = 0, xlimit_hi = 0;
float ylimit_lo = 0, ylimit_hi = 0;

int switch_axis_pt_size(void);
int     do_it_one_plot_op_pr(O_p *o_p, pltreg *pr);
pltreg  *find_pr_in_current_dialog(DIALOG *di);
pltreg *pr_last_selected = NULL;

int (*general_pr_action)(pltreg *pr, int x0, int y0, DIALOG *d);
int (*prev_general_pr_action)(pltreg *pr, int x_0, int y_0, DIALOG *d);

int (*vert_marker_plt_action)(pltreg *pr, O_p *op, float y, DIALOG *d);
int (*horz_marker_plt_action)(pltreg *pr, O_p *op, float x, DIALOG *d);
int (*vert_marker_plt_drag_action)(pltreg *pr, O_p *op, float y, DIALOG *d);
int (*horz_marker_plt_drag_action)(pltreg *pr, O_p *op, float x, DIALOG *d);

int (*plot_wheel_action)(pltreg *pr, O_p *op, DIALOG *d, int ticks);
int (*plot_idle_action)(pltreg *pr, O_p *op, DIALOG *d);

int    do_linlin(void)
{
    pltreg *pr = NULL;
    DIALOG *di = NULL;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if (pr != NULL)
        {
            if ((is_plot_x_log(pr->one_p) == 0) && (is_plot_y_log(pr->one_p) == 0))
            {
                active_menu->flags |= D_SELECTED;
            }
            else
            {
                active_menu->flags &= ~D_SELECTED;
            }
        }

        return D_O_K;
    }

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    set_plot_x_lin(pr->one_p);
    set_plot_y_lin(pr->one_p);

    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    /*    broadcast_dialog_message(MSG_DRAW,0);
    */
    return 0;
}
int    do_loglog(void)
{
    pltreg *pr = NULL;
    DIALOG *di = NULL;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if (pr != NULL)
        {
            if ((is_plot_x_log(pr->one_p)) && (is_plot_y_log(pr->one_p)))
            {
                active_menu->flags |= D_SELECTED;
            }
            else
            {
                active_menu->flags &= ~D_SELECTED;
            }
        }

        return D_O_K;
    }

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    set_plot_x_log(pr->one_p);

    set_plot_y_log(pr->one_p);

    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        broadcast_dialog_message(MSG_DRAW, 0);
    }

    return 0;
}
int    do_linlog(void)
{
    pltreg *pr = NULL;
    DIALOG *di = NULL;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if (pr != NULL)
        {
            if ((is_plot_x_log(pr->one_p) == 0) && (is_plot_y_log(pr->one_p)))
            {
                active_menu->flags |= D_SELECTED;
            }
            else
            {
                active_menu->flags &= ~D_SELECTED;
            }
        }

        return D_O_K;
    }

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    set_plot_x_lin(pr->one_p);

    set_plot_y_log(pr->one_p);

    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        broadcast_dialog_message(MSG_DRAW, 0);
    }

    return 0;
}
int    do_loglin(void)
{
    pltreg *pr = NULL;
    DIALOG *di = NULL;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if (pr != NULL)
        {
            if ((is_plot_x_log(pr->one_p)) && (is_plot_y_log(pr->one_p) == 0))
            {
                active_menu->flags |= D_SELECTED;
            }
            else
            {
                active_menu->flags &= ~D_SELECTED;
            }
        }

        return D_O_K;
    }

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    set_plot_x_log(pr->one_p);

    set_plot_y_lin(pr->one_p);

    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        broadcast_dialog_message(MSG_DRAW, 0);
    }

    return 0;
}

int set_y_limit(void)
{
    pltreg *pr = NULL;
    float min, max, minr, maxr;
    static int in_raw = 0;
    char s[256] = {0};
    O_p *op = NULL;
    DIALOG *di = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;

    min = op->ay + op->dy * op->y_lo;

    max = op->ay + op->dy * op->y_hi;

    minr = op->y_lo;

    maxr = op->y_hi;

    if (op->y_unit == NULL)
    {
        snprintf(s, 256, "Change Y limits max%%f min %%f");
    }
    else
    {
        snprintf(s, 256, "Change Y limits max%%f min %%f in %%R %s\nor in %%r raw data max %%f min %%f", op->y_unit);
    }

    if (win_scanf(s, &max, &min, &in_raw, &maxr, &minr) == WIN_OK)
    {
        op->iopt2 |= Y_LIM;

        if (in_raw == 0)
        {
            min -= op->ay;
            op->y_lo = (op->dy != 0) ? min / op->dy : min;
            max -= op->ay;
            op->y_hi = (op->dy != 0) ? max / op->dy : max;
        }
        else
        {
            op->y_lo = minr;
            op->y_hi = maxr;
        }

        op->need_to_refresh = 1;
        di = find_dialog_associated_to_pr(pr, NULL);

        if (di)
        {
            di->proc(MSG_DRAW, di, 0);
        }
        else
        {
            broadcast_dialog_message(MSG_DRAW, 0);
        }

        return D_REDRAW;
    }

    return D_O_K;
}

int set_x_limit(void)
{
    pltreg *pr = NULL;
    float min, max, minr, maxr;
    static int in_raw = 0;
    char s[256] = {0};
    O_p *op = NULL;
    DIALOG *di = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;

    min = op->ax + op->dx * op->x_lo;

    max = op->ax + op->dx * op->x_hi;

    minr = op->x_lo;

    maxr = op->x_hi;

    if (op->x_unit == NULL)
    {
        snprintf(s, 256, "Change X limits max%%f min %%f");
    }
    else
    {
        snprintf(s, 256, "Change X limits max%%f min %%f in %%R %s\nor in %%r raw data max %%f min %%f", op->x_unit);
    }

    if (win_scanf(s, &max, &min, &in_raw, &maxr, &minr) == WIN_OK)
    {
        op->iopt2 |= X_LIM;

        if (in_raw == 0)
        {
            min -= op->ax;
            op->x_lo = (op->dx != 0) ? min / op->dx : min;
            max -= op->ax;
            op->x_hi = (op->dx != 0) ? max / op->dx : max;
        }
        else
        {
            op->x_lo = minr;
            op->x_hi = maxr;
        }

        op->need_to_refresh = 1;
        di = find_dialog_associated_to_pr(pr, NULL);

        if (di)
        {
            di->proc(MSG_DRAW, di, 0);
        }
        else
        {
            broadcast_dialog_message(MSG_DRAW, 0);
        }

        return D_O_K;
    }

    /*    broadcast_dialog_message(MSG_DRAW,0); */
    return D_O_K;
}

int copy_x_limit(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;
    xlimit_lo = op->x_lo;
    xlimit_hi = op->x_hi;
    /*    broadcast_dialog_message(MSG_DRAW,0); */
    return D_O_K;
}

int paste_x_limit(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    DIALOG *di = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;
    op->x_lo = xlimit_lo;
    op->x_hi = xlimit_hi;
    op->need_to_refresh = 1;
    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        broadcast_dialog_message(MSG_DRAW, 0);
    }
    return D_O_K;
}

int copy_y_limit(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;
    ylimit_lo = op->y_lo;
    ylimit_hi = op->y_hi;
    /*    broadcast_dialog_message(MSG_DRAW,0); */
    return D_O_K;
}

int paste_y_limit(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    DIALOG *di = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;
    op->y_lo = ylimit_lo;
    op->y_hi = ylimit_hi;
    op->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);
    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        broadcast_dialog_message(MSG_DRAW, 0);
    }
    return D_O_K;
}


int auto_x_limit(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    DIALOG *di = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;

    op->iopt2 &= ~X_LIM;

    op->x_lo = 0;

    op->x_hi = 0;

    op->need_to_refresh = 1;

    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        broadcast_dialog_message(MSG_DRAW, 0);
    }

    return D_O_K;
}

int auto_y_limit(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    DIALOG *di = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;

    op->iopt2 &= ~Y_LIM;

    op->y_lo = 0;

    op->y_hi = 0;

    op->need_to_refresh = 1;

    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        broadcast_dialog_message(MSG_DRAW, 0);
    }

    return D_O_K;
}



int plot_size(int mn_indexl, float size)
{
    int i;
    static float tmp = 1.0;
    pltreg *pr = NULL;
    DIALOG *di = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    if (mn_indexl  == X_AXIS)
    {
        if (size > 0)
        {
            pr->one_p->width = size;
        }
        else
        {
            tmp = pr->one_p->width;
            i = win_scanf("width size %f", &tmp);

            if (i == WIN_CANCEL)
            {
                return 0;
            }

            pr->one_p->width = (tmp > 0) ? tmp : 1.0;
        }
    }
    else if (mn_indexl  == Y_AXIS)
    {
        if (size > 0)
        {
            pr->one_p->height = size;
        }
        else
        {
            tmp = pr->one_p->height;
            i = win_scanf("heigh size %f", &tmp);

            if (i == WIN_CANCEL)
            {
                return 0;
            }

            pr->one_p->height = (tmp > 0) ? tmp : 1.0;
        }
    }

    pr->one_p->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        broadcast_dialog_message(MSG_DRAW, 0);
    }

    return D_O_K;
}

int    set_plot_height(void)
{
    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    return plot_size(Y_AXIS, -1);
}
int    set_plot_width(void)
{
    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    return plot_size(X_AXIS, -1);
}

int    auto_x_y_limit(void)
{
    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    auto_x_limit();
    return auto_y_limit();
}

MENU plot_axis_menu[32] =
{
    { "L&in-lin\t(ctrl+I)",      do_linlin,       NULL,       0, NULL  },
    { "L&og-log\t(ctrl+O)",      do_loglog,       NULL,       0, NULL  },
    { "Li&n-Log\t(ctrl+n)",      do_linlog,        NULL,       0, NULL  },
    { "Lo&g-lin\t(ctrl+G)",      do_loglin,       NULL,       0, NULL  },
    { "\0",                       NULL,                    NULL,       0, NULL  },
    { "Li&mits in X\t(ctrl+m)",  set_x_limit,     NULL,       0, NULL  },
    { "Limits A&uto in X\t(Ctrl-X)",  auto_x_limit,     NULL,       0, NULL  },
    { "Copy limits in X",  copy_x_limit,     NULL,       0, NULL  },
    { "Paste limits in X",  paste_x_limit,     NULL,       0, NULL  },
    { "\0",                       NULL,                    NULL,       0, NULL  },
    { "Li&mits in Y\t(ctrl+m)",  set_y_limit,     NULL,       0, NULL  },
    { "Limits A&uto in Y\t(Ctrl-Y)",  auto_y_limit,     NULL,       0, NULL  },
    { "Copy limits in Y",  copy_y_limit,     NULL,       0, NULL  },
    { "Paste limits in Y",  paste_y_limit,     NULL,       0, NULL  },
    { "\0",                       NULL,                    NULL,       0, NULL  },
    { "Limits A&uto in X & Y\t(Ctrl-Z)",  auto_x_y_limit,     NULL,       0, NULL  },
    { "\0",                       NULL,                    NULL,       0, NULL  },
    { "Set Plot height",  set_plot_height,     NULL,       0, NULL  },
    { "Set Plot width",  set_plot_width,     NULL,       0, NULL  },
    { NULL,                    NULL,             NULL,       0, NULL  }
};



MENU plot_axis_menu_pop[32] =
{
    { "L&in-lin\t(ctrl+I)",      NULL,     NULL,       0, NULL  },
    { "L&og-log\t(ctrl+O)",      NULL,     NULL,       0, NULL  },
    { "Li&n-Log\t(ctrl+n)",      NULL,     NULL,       0, NULL  },
    { "Lo&g-lin\t(ctrl+G)",      NULL,     NULL,       0, NULL  },
    { "\0",                      NULL,     NULL,       0, NULL  },
    { "Li&mits in X\t(ctrl+m)",  NULL,     NULL,       0, NULL  },
    { "Limits A&uto in X\t(Ctrl-X)", NULL, NULL,       0, NULL  },
    { "Copy limits in X",        NULL,     NULL,       0, NULL  },
    { "Paste limits in X",       NULL,     NULL,       0, NULL  },
    { "\0",               NULL,     NULL,       0, NULL  },
    { "Li&mits in Y\t(ctrl+m)",  NULL,     NULL,       0, NULL  },
    { "Limits A&uto in Y\t(Ctrl-Y)", NULL, NULL,       0, NULL  },
    { "Copy limits in Y",        NULL,     NULL,       0, NULL  },
    { "Paste limits in Y",       NULL,     NULL,       0, NULL  },
    { "\0",               NULL,     NULL,       0, NULL  },
    { "Limits A&uto in X & Y\t(Ctrl-Z)",   NULL, NULL, 0, NULL  },
    { "\0",                      NULL,     NULL,       0, NULL  },
    { "Set Plot height",         NULL,     NULL,       0, NULL  },
    { "Set Plot width",          NULL,     NULL,       0, NULL  },
    { NULL,                      NULL,     NULL,       0, NULL  }
};




int do_plot_axis_menu(int item)
{
    if (item == 0)
    {
        return do_linlin();
    }
    else if (item == 1)
    {
        return do_loglog();
    }
    else if (item == 2)
    {
        return do_linlog();
    }
    else if (item == 3)
    {
        return do_loglin();
    }
    else if (item == 5)
    {
        return set_x_limit();
    }
    else if (item == 6)
    {
        return auto_x_limit();
    }
    else if (item == 7)
    {
        return copy_x_limit();
    }
    else if (item == 8)
    {
        return paste_x_limit();
    }
    else if (item == 10)
    {
        return set_y_limit();
    }
    else if (item == 11)
    {
        return auto_y_limit();
    }
    else if (item == 12)
    {
        return copy_y_limit();
    }
    else if (item == 13)
    {
        return paste_y_limit();
    }
    else if (item == 15)
    {
        auto_x_limit();
        return auto_y_limit();
    }
    else if (item == 17)
    {
        return set_plot_height();
    }
    else if (item == 18)
    {
        return set_plot_width();
    }
    else
    {
        return 0;
    }
}




int    do_select_nearest_ds(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    //  d_s *ds;
    int i, nearest_ds = -1, nearest_point = -1;
    float  xm, ym;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (last_data_mouse_x  < 0 || last_data_mouse_y < 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;

    xm = x_pr_2_pltdata(pr, last_data_mouse_x);

    ym = y_pr_2_pltdata(pr, last_data_mouse_y);

    i = find_nearest_points_in_plot(op, xm, ym, &nearest_ds, &nearest_point, 0);

    if (i != 0)
    {
        return D_O_K;
    }

    if (nearest_ds >= 0 && nearest_ds < op->n_dat)
    {
        op->cur_dat = nearest_ds;
    }

    /*
       ds = op->dat[op->cur_dat];
       x0 = (nearest_point < ds->nx) ? op->ax + op->dx * ds->xd[nearest_point] : op->ax + op->dx * ds->xd[0];
       y0 = (nearest_point < ds->nx) ? op->ay + op->dy * ds->yd[nearest_point] : op->ay + op->dy * ds->yd[0];
       win_printf_OK("Nearest mouse point (x = %g y = %g)\n"
       "Selected ds %d \nPoint index %d x %g y %g\n"
       ,xm,ym,nearest_ds,nearest_point,x0, y0);
       */
    op->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
    //return D_REDRAWME;
}

int    do_remove_nearest_ds(bool confirm_rm)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int i = WIN_OK;
    int nearest_ds = -1;
    int nearest_point = -1;
    float x0 = 0, y0 = 0, xm = 0, ym = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (last_data_mouse_x  < 0 || last_data_mouse_y < 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;
    assert(op == pr->o_p[pr->cur_op]);

    xm = x_pr_2_pltdata(pr, last_data_mouse_x);

    ym = y_pr_2_pltdata(pr, last_data_mouse_y);

    i = find_nearest_points_in_plot(op, xm, ym, &nearest_ds, &nearest_point, 0);

    if (i != 0)
    {
        warning_message("find nearest points failed\n");
        return D_O_K;
    }

    if (nearest_ds >= 0 && nearest_ds < op->n_dat)
    {
        op->cur_dat = nearest_ds;
    }
    else
    {
      warning_message("fail to set dataset\n");//, nearest_ds);
        return D_O_K;
    }

    i = WIN_OK;

    if (confirm_rm)
    {
        ds = op->dat[nearest_ds];
        x0 = (nearest_point < ds->nx) ? op->ax + op->dx * ds->xd[nearest_point] : op->ax + op->dx * ds->xd[0];
        y0 = (nearest_point < ds->nx) ? op->ay + op->dy * ds->yd[nearest_point] : op->ay + op->dy * ds->yd[0];
        i = win_printf("Do you want to remove data set %d selected by\n"
                          "Nearest mouse point (x = %g y = %g)\n"
                          "Point index %d x %g y %g\n"
                          , nearest_ds, xm, ym, nearest_point, x0, y0);
    }

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    remove_ds_n_from_op(op, nearest_ds);
    op->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
    //return D_REDRAWME;
}


int    do_remove_selected_point(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int i, nearest_ds = -1, nearest_point = -1;
    float x0, y0, xm, ym;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (last_data_mouse_x  < 0 || last_data_mouse_y < 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;

    xm = x_pr_2_pltdata(pr, last_data_mouse_x);

    ym = y_pr_2_pltdata(pr, last_data_mouse_y);

    i = find_nearest_points_in_plot(op, xm, ym, &nearest_ds, &nearest_point, 0);

    if (i != 0)
    {
        return D_O_K;
    }

    if (nearest_ds >= 0 && nearest_ds < op->n_dat)
    {
        op->cur_dat = nearest_ds;
        ds = op->dat[nearest_ds];

        if (nearest_point >= 0 && nearest_point < ds->nx)
        {
            x0 = op->ax + op->dx * ds->xd[nearest_point];
            y0 = op->ay + op->dy * ds->yd[nearest_point];
            i = win_printf("Do you want to remove data point %d of data set %d\n"
                              "selected by nearest mouse point (x = %g y = %g)\n"
                              "Point coordinate x %g y %g\n"
                              , nearest_point, nearest_ds, xm, ym,  x0, y0);

            if (i == WIN_CANCEL)
            {
                return D_O_K;
            }

            remove_point_from_data_set(ds, nearest_point);
            op->need_to_refresh = 1;
        }
    }

    return refresh_plot(pr, UNCHANGED);
    //return D_REDRAWME;
}

int    do_edit_selected_point(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int i, nearest_ds = -1, nearest_point = -1;
    float x0, y0, xm, ym, xe = 0, ye = 0;
    char question[1024];

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (last_data_mouse_x  < 0 || last_data_mouse_y < 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;

    xm = x_pr_2_pltdata(pr, last_data_mouse_x);

    ym = y_pr_2_pltdata(pr, last_data_mouse_y);

    i = find_nearest_points_in_plot(op, xm, ym, &nearest_ds, &nearest_point, 0);

    if (i != 0)
    {
        return D_O_K;
    }

    if (nearest_ds >= 0 && nearest_ds < op->n_dat)
    {
        op->cur_dat = nearest_ds;
        ds = op->dat[nearest_ds];

        if (nearest_point >= 0 && nearest_point < ds->nx)
        {
            x0 = op->ax + op->dx * ds->xd[nearest_point];
            y0 = op->ay + op->dy * ds->yd[nearest_point];
	    // add other errors !
            if (ds->xe != NULL)
            {
                xe = op->ax + op->dx * ds->xe[nearest_point];
            }

            if (ds->ye != NULL)
            {
                ye = op->ay + op->dy * ds->ye[nearest_point];
            }

            if (ds->xe == NULL && ds->ye == NULL)
            {
                snprintf(question, sizeof(question), "You are editing data point %d of data set %d\n"
                         "selected by nearest mouse point (x = %g y = %g)\n"
                         "Point coordinates:\n X = %g new value %%10f\n"
                         "Y = %g new value %%10f\n"
                         , nearest_point, nearest_ds, xm, ym, x0, y0);
                i = win_scanf(question, &x0, &y0);

                if (i == WIN_CANCEL)
                {
                    return D_O_K;
                }
            }
            else if (ds->xe != NULL && ds->ye != NULL)
            {
                snprintf(question, sizeof(question), "You are editing data point %d of data set %d\n"
                         "selected by nearest mouse point (x = %g y = %g)\n"
                         "Point coordinates:\nX = %g new value %%10f\n"
                         "error in X = %g new value %%10f\n"
                         "Y = %g new value %%10f\n"
                         "error in Y = %g new value %%10f\n"
                         , nearest_point, nearest_ds, xm, ym, x0, xe, y0, ye);
                i = win_scanf(question, &x0, &xe, &y0, &ye);

                if (i == WIN_CANCEL)
                {
                    return D_O_K;
                }
            }
            else if (ds->xe != NULL && ds->ye == NULL)
            {
                snprintf(question, sizeof(question), "You are editing data point %d of data set %d\n"
                         "selected by nearest mouse point (x = %g y = %g)\n"
                         "Point coordinates:\nX = %g new value %%10f\n"
                         "error in X = %g new value %%10f\n"
                         "Y = %g new value %%10f\n"
                         , nearest_point, nearest_ds, xm, ym, x0, xe, y0);
                i = win_scanf(question, &x0, &xe, &y0);

                if (i == WIN_CANCEL)
                {
                    return D_O_K;
                }
            }
            else if (ds->xe == NULL && ds->ye != NULL)
            {
                snprintf(question, sizeof(question), "You are editing data point %d of data set %d\n"
                         "selected by nearest mouse point (x = %g y = %g)\n"
                         "Point coordinates:\nX = %g new value %%10f\n"
                         "Y = %g new value %%10f\n"
                         "error in Y = %g new value %%10f\n"
                         , nearest_point, nearest_ds, xm, ym, x0, y0, ye);
                i = win_scanf(question, &x0, &y0, &ye);

                if (i == WIN_CANCEL)
                {
                    return D_O_K;
                }
            }

            ds->xd[nearest_point] = (x0 - op->ax);

            if (op->dx != 0)
            {
                ds->xd[nearest_point] /= op->dx;
            }

            ds->yd[nearest_point] = (y0 - op->ay);

            if (op->dy != 0)
            {
                ds->yd[nearest_point] /= op->dy;
            }

            if (ds->xe != NULL)
            {
                ds->xe[nearest_point] = xe;

                if (op->dx != 0)
                {
                    ds->xe[nearest_point] /= op->dx;
                }
            }

            if (ds->ye != NULL)
            {
                ds->ye[nearest_point] = ye;

                if (op->dy != 0)
                {
                    ds->ye[nearest_point] /= op->dy;
                }
            }

            op->need_to_refresh = 1;
        }
    }

    return refresh_plot(pr, UNCHANGED);
}


int    do_insert_selected_point_in_selected_ds(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int i, nearest_smaller = -1, nearest_point = -1;
    float xm, ym, xe = 0, ye = 0;//x0, y0,
    //char question[1024] = {0};

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (last_data_mouse_x  < 0 || last_data_mouse_y < 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);
    if (ac_grep(cur_ac_reg, "ds", &ds) != 1) return(win_printf_OK("No selected dataset\n"));


    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;


    xm = x_pr_2_pltdata(pr, last_data_mouse_x);

    ym = y_pr_2_pltdata(pr, last_data_mouse_y);

    xm -= op->ax;

    if (op->dx != 0)
    {
        xm /= op->dx;
    }

    i =	find_nearest_points_in_x_in_ds(ds,xm, &nearest_smaller, &nearest_point, op->y_lo, op->y_hi);

    if (i != 0)
    {
        return D_O_K;
    }

    if (ds != NULL)
    {

        if (nearest_point >= 0 && nearest_point < ds->nx)
        {
            //x0 = op->ax + op->dx * ds->xd[nearest_point];
            //y0 = op->ay + op->dy * ds->yd[nearest_point];
	    // add other errors !


            ym -= op->ay;

            if (op->dy != 0)
            {
                ym /= op->dy;
            }


            insert_new_point_in_ds(ds, nearest_point, xm, ym, xe, ye);
            op->need_to_refresh = 1;
        }
    }

    return refresh_plot(pr, UNCHANGED);

}
int    do_insert_selected_point(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int i, nearest_ds = -1, nearest_point = -1;
    float x0, y0, xm, ym, xe = 0, ye = 0;
    char question[1024] = {0};

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (last_data_mouse_x  < 0 || last_data_mouse_y < 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;

    xm = x_pr_2_pltdata(pr, last_data_mouse_x);

    ym = y_pr_2_pltdata(pr, last_data_mouse_y);

    i = find_nearest_points_in_plot(op, xm, ym, &nearest_ds, &nearest_point, 0);

    if (i != 0)
    {
        return D_O_K;
    }

    if (nearest_ds >= 0 && nearest_ds < op->n_dat)
    {
        op->cur_dat = nearest_ds;
        ds = op->dat[nearest_ds];

        if (nearest_point >= 0 && nearest_point < ds->nx)
        {
            x0 = op->ax + op->dx * ds->xd[nearest_point];
            y0 = op->ay + op->dy * ds->yd[nearest_point];
	    // add other errors !
            if (ds->xe != NULL)
            {
                xe = op->ax + op->dx * ds->xe[nearest_point];
            }

            if (ds->ye != NULL)
            {
                ye = op->ay + op->dy * ds->ye[nearest_point];
            }

            if (ds->xe == NULL && ds->ye == NULL)
            {
                snprintf(question, sizeof(question), "You are going to insert data point at index %d in data set %d\n"
                         "selected by nearest mouse point (x = %g y = %g)\n"
                         "Nearest point coordinates:\n X = %g Y = %g \n"
                         "Inserted point X = %%10f Y = %%10f\n"
                         , nearest_point, nearest_ds, xm, ym, x0, y0);
                i = win_scanf(question, &xm, &ym);

                if (i == WIN_CANCEL)
                {
                    return D_O_K;
                }
            }
            else if (ds->xe != NULL && ds->ye != NULL)
            {
                snprintf(question, sizeof(question), "You are going to insert data point at index %d in data set %d\n"
                         "selected by nearest mouse point (x = %g y = %g)\n"
                         "Nearest point coordinates:\n X = %g Y = %g \n"
                         "Inserted point X = %%10f Y = %%10f\n"
                         "error in dX = %%10f\n error dY =%%10f\n"
                         , nearest_point, nearest_ds, xm, ym, x0, y0);
                i = win_scanf(question, &xm, &ym, &xe, &ye);

                if (i == WIN_CANCEL)
                {
                    return D_O_K;
                }
            }
            else if (ds->xe != NULL && ds->ye == NULL)
            {
                snprintf(question, sizeof(question), "You are going to insert data point at index %d in data set %d\n"
                         "selected by nearest mouse point (x = %g y = %g)\n"
                         "Nearest point coordinates:\n X = %g Y = %g \n"
                         "Inserted point X = %%10f Y = %%10f\n"
                         "error in dX = %%10f"
                         , nearest_point, nearest_ds, xm, ym, x0, y0);
                i = win_scanf(question, &xm, &ym, &xe);

                if (i == WIN_CANCEL)
                {
                    return D_O_K;
                }
            }
            else if (ds->xe == NULL && ds->ye != NULL)
            {
                snprintf(question, sizeof(question), "You are going to insert data point at index %d in data set %d\n"
                         "selected by nearest mouse point (x = %g y = %g)\n"
                         "Nearest point coordinates:\n X = %g Y = %g \n"
                         "Inserted point X = %%10f Y = %%10f\n"
                         "error dY =%%10f\n"
                         , nearest_point, nearest_ds, xm, ym, x0, y0);
                i = win_scanf(question, &xm, &ym, &ye);

                if (i == WIN_CANCEL)
                {
                    return D_O_K;
                }
            }

            xm -= op->ax;

            if (op->dx != 0)
            {
                xm /= op->dx;
            }

            ym -= op->ay;

            if (op->dy != 0)
            {
                ym /= op->dy;
            }

            if (ds->xe != NULL)
            {
                if (op->dx != 0)
                {
                    xe /= op->dx;
                }
            }

            if (ds->ye != NULL)
            {
                if (op->dy != 0)
                {
                    ye /= op->dy;
                }
            }

            insert_new_point_in_ds(ds, nearest_point, xm, ym, xe, ye);
            op->need_to_refresh = 1;
        }
    }

    return refresh_plot(pr, UNCHANGED);
}


int copy_ds_from_one_op_to_anotherone(d_s *dsc, O_p *src, O_p *dst)
{
    int i;
    un_s *unss = NULL, *unsd = NULL;
    d_s *dds = NULL;
    char *units = NULL, *unitd = NULL;
    float dxs, axs, dxd, axd, dys, ays, dyd, ayd;
    float d_xs, d_xd, d_ys, d_yd;

    if (src == NULL || dst == NULL || dsc == NULL)
    {
        return -1;
    }

    unss = src->xu[src->c_xu];
    unsd = dst->xu[dst->c_xu];

    if (unss->type != unsd->type)
    {
        units = generate_units(unss->type, unss->decade);
        unitd = generate_units(unsd->type, unsd->decade);
        i = win_printf("You are adding data sets with different x units\n"
                       "souce %s \n destination %s\n"
                       "do you really want to transfer !\n"
                       "If you click OK you will probably get garbage \n"
                       " you are warn ...!", (units) ? units : "no_name",
                       (unitd) ? unitd : "no_name");

        if (units)
        {
            free(units);
            units = NULL;
        }

        if (unitd)
        {
            free(unitd);
            unitd = NULL;
        }

        if (i == WIN_CANCEL)
        {
            return 1;
        }

        dxs = dxd = 1;
        axs = axd = 0;
        d_xs = d_xd = 1;
    }
    else
    {
        dxs = unss->dx;
        axs = unss->ax;
        d_xs = 1;

        for (i = 0; i < unss->decade; i++, d_xs *= 10);

        for (i = 0; i > unss->decade; i--, d_xs *= .1);

        dxd = unsd->dx;
        axd = unsd->ax;
        d_xd = 1;

        for (i = 0; i < unsd->decade; i++, d_xd *= 10);

        for (i = 0; i > unsd->decade; i--, d_xd *= .1);
    }

    unss = src->yu[src->c_yu];
    unsd = dst->yu[dst->c_yu];

    if (unss->type != unsd->type)
    {
        units = generate_units(unss->type, unss->decade);
        unitd = generate_units(unsd->type, unsd->decade);
        i = win_printf("You are adding data sets with different y units\n"
                       "souce %s \n destination %s\n"
                       "do you really want to transfer !\n"
                       "If you click OK you will probably get garbage \n"
                       "you are warn ...!", (units) ? units : "no_name",
                       (unitd) ? unitd : "no_name");

        if (units)
        {
            free(units);
            units = NULL;
        }

        if (unitd)
        {
            free(unitd);
            unitd = NULL;
        }

        if (i == WIN_CANCEL)
        {
            return 1;
        }

        dys = dyd = 1;
        ays = ayd = 0;
        d_ys = d_yd = 1;
    }
    else
    {
        dys = unss->dx;
        ays = unss->ax;
        d_ys = 1;

        for (i = 0; i < unss->decade; i++, d_ys *= 10);

        for (i = 0; i > unss->decade; i--, d_ys *= .1);

        dyd = unsd->dx;
        ayd = unsd->ax;
        d_yd = 1;

        for (i = 0; i < unsd->decade; i++, d_yd *= 10);

        for (i = 0; i > unsd->decade; i--, d_yd *= .1);
    }

    dds = duplicate_data_set(dsc, NULL);

    if (dds == NULL)
    {
        return D_O_K;
    }

    if (dxs != dxd || axs != axd || d_xs != d_xd)
    {
        for (i = 0; (dxd != 0) && (i < dds->nx) && (i < dds->ny); i++)
        {
            dds->xd[i] = ((((axs + (dxs * dds->xd[i])) * d_xs) / d_xd) - axd) / dxd;
        }
    }

    if (dys != dyd || ays != ayd || d_ys != d_yd)
    {
        for (i = 0; (dyd != 0) && (i < dds->nx) && (i < dds->ny); i++)
        {
            dds->yd[i] = ((((ays + (dys * dds->yd[i])) * d_ys) / d_yd) - ayd) / dyd;
        }
    }

    dds->color = 0; // We want to have color set by the dest plot
    add_one_plot_data(dst, IS_DATA_SET, (void *)dds);
    dst->need_to_refresh = 1;

    return 0;
}


int copy_point_from_one_op_to_another_one(float x, float y, O_p *src, O_p *dst, d_s *dsd)
{
    int i;
    un_s *unss = NULL, *unsd = NULL;
    char *units = NULL, *unitd = NULL;
    float dxs, axs, dxd, axd, dys, ays, dyd, ayd;
    float d_xs, d_xd, d_ys, d_yd;

    if (src == NULL || dst == NULL || dsd == NULL)
    {
        return -1;
    }

    unss = src->xu[src->c_xu];
    unsd = dst->xu[dst->c_xu];

    if (unss->type != unsd->type)
    {
        units = generate_units(unss->type, unss->decade);
        unitd = generate_units(unsd->type, unsd->decade);
        i = win_printf("You are adding data with different x units\n"
                       "souce %s \n destination %s\n"
                       "do you really want to transfer !\n"
                       "If you click OK you will probably get garbage \n"
                       " you are warn ...!", (units) ? units : "no_name",
                       (unitd) ? unitd : "no_name");

        if (units)
        {
            free(units);
            units = NULL;
        }

        if (unitd)
        {
            free(unitd);
            unitd = NULL;
        }

        if (i == WIN_CANCEL)
        {
            return 1;
        }

        dxs = dxd = 1;
        axs = axd = 0;
        d_xs = d_xd = 1;
    }
    else
    {
        dxs = unss->dx;
        axs = unss->ax;
        d_xs = 1;

        for (i = 0; i < unss->decade; i++, d_xs *= 10);

        for (i = 0; i > unss->decade; i--, d_xs *= .1);

        dxd = unsd->dx;
        axd = unsd->ax;
        d_xd = 1;

        for (i = 0; i < unsd->decade; i++, d_xd *= 10);

        for (i = 0; i > unsd->decade; i--, d_xd *= .1);
    }

    unss = src->yu[src->c_yu];
    unsd = dst->yu[dst->c_yu];

    if (unss->type != unsd->type)
    {
        units = generate_units(unss->type, unss->decade);
        unitd = generate_units(unsd->type, unsd->decade);
        i = win_printf("You are adding data with different y units\n"
                       "souce %s \n destination %s\n"
                       "do you really want to transfer !\n"
                       "If you click OK you will probably get garbage \n"
                       "you are warn ...!", (units) ? units : "no_name",
                       (unitd) ? unitd : "no_name");

        if (units)
        {
            free(units);
            units = NULL;
        }

        if (unitd)
        {
            free(unitd);
            unitd = NULL;
        }

        if (i == WIN_CANCEL)
        {
            return 1;
        }

        dys = dyd = 1;
        ays = ayd = 0;
        d_ys = d_yd = 1;
    }
    else
    {
        dys = unss->dx;
        ays = unss->ax;
        d_ys = 1;

        for (i = 0; i < unss->decade; i++, d_ys *= 10);

        for (i = 0; i > unss->decade; i--, d_ys *= .1);

        dyd = unsd->dx;
        ayd = unsd->ax;
        d_yd = 1;

        for (i = 0; i < unsd->decade; i++, d_yd *= 10);

        for (i = 0; i > unsd->decade; i--, d_yd *= .1);
    }

    if (dxs != dxd || axs != axd || d_xs != d_xd)
    {
        x = ((((axs + (dxs * x)) * d_xs) / d_xd) - axd) / dxd;
    }

    if (dys != dyd || ays != ayd || d_ys != d_yd)
    {
        y = ((((ays + (dys * y)) * d_ys) / d_yd) - ayd) / dyd;
    }

    add_new_point_to_ds(dsd, x, y);
    dst->need_to_refresh = 1;
    return 0;
}
# ifdef ENCOURS
int copy_point_from_one_op_to_copied_op(float x, float y, O_p *src, int newplot)
{
    int i;

    if (newplot)
    {
        if (op_copied != NULL)
        {
            free_one_plot(op_copied);
            op_copied = NULL;
        }

        if ((op_copied = duplicate_plot_data(pr->one_p, NULL)) == NULL)
        {
            return D_O_K;
        }
    }

    int copy_point_from_one_op_to_another_one(float x, float y, O_p * src, O_p * dst, d_s * dsd)
}

# endif

int do_copy_nearest_ds(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int nearest_ds = -1, nearest_point = -1;
    float xm, ym;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (last_data_mouse_x  < 0 || last_data_mouse_y < 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("pr not found");
    };

    op = pr->one_p;

    xm = x_pr_2_pltdata(pr, last_data_mouse_x);

    ym = y_pr_2_pltdata(pr, last_data_mouse_y);

    i = find_nearest_points_in_plot(op, xm, ym, &nearest_ds, &nearest_point, 0);

    if (i != 0)
    {
        return D_O_K;
    }

    if (nearest_ds < 0 && nearest_ds >= op->n_dat)
    {
        return D_O_K;
    }

    ds = op->dat[nearest_ds];

    if (opds_copied != NULL)
    {
        free_one_plot(opds_copied);
        opds_copied = NULL;
        ds_copied = NULL;
    }

    i = op->n_dat;
    op->n_dat = 0;

    if ((opds_copied = duplicate_plot_data(op, NULL)) == NULL)
    {
        op->n_dat = i;
        win_printf_OK("Could not duplicate plot");
    }
    else
    {
        op->n_dat = i;
        ds_copied = duplicate_data_set(ds, NULL);

        if (ds_copied == NULL)
        {
            op->n_dat = i;
            win_printf_OK("Could not duplicate ds");
        }
        else
        {
            if (add_one_plot_data(opds_copied, IS_DATA_SET, (void *)ds_copied))
            {
                op->n_dat = i;
                win_printf_OK("Could not add ds");
            }
        }
    }

    remove_all_keyboard_proc();
    scan_and_update(plot_menu);
    return D_O_K;
}



MENU plot_data_menu_pop[32] =
{
    { "Select nearest data set",      NULL,     NULL,       0, NULL  },
    { "copy nearest data set",      NULL,     NULL,       0, NULL  },
    { "kill nearest data set",      NULL,     NULL,       0, NULL  },
    { "Edit selected point",      NULL,     NULL,       0, NULL  },
    { "Remove selected point",      NULL,     NULL,       0, NULL  },
    { "Insert point",      NULL,     NULL,       0, NULL  },
    { "Insert point here in selected dataset",      NULL,     NULL,       0, NULL  },
    { NULL,                      NULL,     NULL,       0, NULL  }
};

int do_plot_data_menu(int item)
{
    if (item == 0)
    {
        return do_select_nearest_ds();
    }
    else if (item == 1)
    {
        return do_copy_nearest_ds();
    }
    else if (item == 2)
    {
        return do_remove_nearest_ds(true);
    }
    else if (item == 3)
    {
        return do_edit_selected_point();
    }
    else if (item == 4)
    {
        return do_remove_selected_point();
    }
    else if (item == 5)
    {
        return do_insert_selected_point();
    }
    else if (item == 6)
    {
        return do_insert_selected_point_in_selected_ds();
    }
    else
    {
        return 0;
    }
}



int line_style_selected = 0;

int def_line_style_plain(void)
{
    pltreg *pr = NULL;
    DIALOG *di = NULL;
    int index, i;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if ((pr != NULL) && (pr->one_p != NULL) && (pr->one_p->dat != NULL)
                && (pr->one_p->cur_dat < pr->one_p->n_dat) && (pr->one_p->cur_dat >= 0))
        {
            if (pr->one_p->dat[pr->one_p->cur_dat]->m == 1)
            {
                active_menu->flags |= D_SELECTED;
            }
            else
            {
                active_menu->flags &= ~D_SELECTED;
            }
        }

        return D_O_K;
    }

    if ((pr == NULL) || (pr->one_p == NULL) || (pr->one_p->dat == NULL)
            || (pr->one_p->cur_dat >= pr->one_p->n_dat) || (pr->one_p->cur_dat < 0))
    {
        return 0;
    }

    index = RETRIEVE_MENU_INDEX;

    if (index == DS_ALL)
    {
        for (i = 0; i < pr->one_p->n_dat; i++)
        {
            pr->one_p->dat[i]->m = 1;
        }
    }

    pr->one_p->dat[pr->one_p->cur_dat]->m = 1;
    pr->one_p->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    line_style_selected = 1;
    return 0;
}
int def_line_style_plain_all(void)
{
    pltreg *pr = NULL;
    DIALOG *di = NULL;
    int i;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if ((pr != NULL) && (pr->one_p != NULL) && (pr->one_p->dat != NULL)
                && (pr->one_p->cur_dat < pr->one_p->n_dat) && (pr->one_p->cur_dat >= 0))
        {
            if (pr->one_p->dat[pr->one_p->cur_dat]->m == 1)
            {
                active_menu->flags |= D_SELECTED;
            }
            else
            {
                active_menu->flags &= ~D_SELECTED;
            }
        }

        return D_O_K;
    }

    if ((pr == NULL) || (pr->one_p == NULL) || (pr->one_p->dat == NULL)
            || (pr->one_p->cur_dat >= pr->one_p->n_dat) || (pr->one_p->cur_dat < 0))
    {
        return 0;
    }

    for (i = 0; i < pr->one_p->n_dat; i++)
    {
        pr->one_p->dat[i]->m = 1;
    }

    pr->one_p->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    line_style_selected = 1;
    return 0;
}
int def_line_style_dash(void)
{
    pltreg *pr = NULL;
    DIALOG *di = NULL;
    int index, i;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if ((pr != NULL) && (pr->one_p != NULL) && (pr->one_p->dat != NULL)
                && (pr->one_p->cur_dat < pr->one_p->n_dat) && (pr->one_p->cur_dat >= 0))
        {
            if (pr->one_p->dat[pr->one_p->cur_dat]->m == 0)
            {
                active_menu->flags |= D_SELECTED;
            }
            else
            {
                active_menu->flags &= ~D_SELECTED;
            }
        }

        return D_O_K;
    }

    if ((pr == NULL) || (pr->one_p == NULL) || (pr->one_p->dat == NULL)
            || (pr->one_p->cur_dat >= pr->one_p->n_dat) || (pr->one_p->cur_dat < 0))
    {
        return 0;
    }

    index = RETRIEVE_MENU_INDEX;

    if (index == DS_ALL)
    {
        for (i = 0; i < pr->one_p->n_dat; i++)
        {
            pr->one_p->dat[i]->m = 0;
        }
    }

    pr->one_p->dat[pr->one_p->cur_dat]->m = 0;
    pr->one_p->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    line_style_selected = 0;
    return 0;
}
int def_line_style_dash_all(void)
{
    pltreg *pr = NULL;
    DIALOG *di = NULL;
    int i;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if ((pr != NULL) && (pr->one_p != NULL) && (pr->one_p->dat != NULL)
                && (pr->one_p->cur_dat < pr->one_p->n_dat) && (pr->one_p->cur_dat >= 0))
        {
            if (pr->one_p->dat[pr->one_p->cur_dat]->m == 0)
            {
                active_menu->flags |= D_SELECTED;
            }
            else
            {
                active_menu->flags &= ~D_SELECTED;
            }
        }

        return D_O_K;
    }

    if ((pr == NULL) || (pr->one_p == NULL) || (pr->one_p->dat == NULL)
            || (pr->one_p->cur_dat >= pr->one_p->n_dat) || (pr->one_p->cur_dat < 0))
    {
        return 0;
    }

    for (i = 0; i < pr->one_p->n_dat; i++)
    {
        pr->one_p->dat[i]->m = 0;
    }

    pr->one_p->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    line_style_selected = 0;
    return 0;
}
int def_line_style_dot(void)
{
    pltreg *pr = NULL;
    DIALOG *di = NULL;
    int index, i;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if ((pr != NULL) && (pr->one_p != NULL) && (pr->one_p->dat != NULL)
                && (pr->one_p->cur_dat < pr->one_p->n_dat) && (pr->one_p->cur_dat >= 0))
        {
            if (pr->one_p->dat[pr->one_p->cur_dat]->m == 2)
            {
                active_menu->flags |= D_SELECTED;
            }
            else
            {
                active_menu->flags &= ~D_SELECTED;
            }
        }

        return D_O_K;
    }

    if ((pr == NULL) || (pr->one_p == NULL) || (pr->one_p->dat == NULL)
            || (pr->one_p->cur_dat >= pr->one_p->n_dat) || (pr->one_p->cur_dat < 0))
    {
        return 0;
    }

    index = RETRIEVE_MENU_INDEX;

    if (index == DS_ALL)
    {
        for (i = 0; i < pr->one_p->n_dat; i++)
        {
            pr->one_p->dat[i]->m = 2;
        }
    }

    pr->one_p->dat[pr->one_p->cur_dat]->m = 2;
    pr->one_p->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    line_style_selected = 2;
    return 0;
}

int def_line_style_dot_all(void)
{
    pltreg *pr = NULL;
    DIALOG *di = NULL;
    int i;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if ((pr != NULL) && (pr->one_p != NULL) && (pr->one_p->dat != NULL)
                && (pr->one_p->cur_dat < pr->one_p->n_dat) && (pr->one_p->cur_dat >= 0))
        {
            if (pr->one_p->dat[pr->one_p->cur_dat]->m == 2)
            {
                active_menu->flags |= D_SELECTED;
            }
            else
            {
                active_menu->flags &= ~D_SELECTED;
            }
        }

        return D_O_K;
    }

    if ((pr == NULL) || (pr->one_p == NULL) || (pr->one_p->dat == NULL)
            || (pr->one_p->cur_dat >= pr->one_p->n_dat) || (pr->one_p->cur_dat < 0))
    {
        return 0;
    }

    for (i = 0; i < pr->one_p->n_dat; i++)
    {
        pr->one_p->dat[i]->m = 2;
    }

    pr->one_p->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    line_style_selected = 2;
    return 0;
}

MENU line_style_menu[32] =
{
    { "Plain line",      def_line_style_plain,       NULL,       0, NULL  },
    { "Dash line",      def_line_style_dash,       NULL,       0, NULL  },
    { "Simple dots",      def_line_style_dot,       NULL,       0, NULL  },
    { NULL,                    NULL,             NULL,       0, NULL  }
};




MENU line_style_all_ds_menu[32] =
{
    { "Plain line",      def_line_style_plain_all,       NULL,      0, NULL  },
    { "Dash line",      def_line_style_dash_all,       NULL,       0, NULL  },
    { "Simple dots",      def_line_style_dot_all,       NULL,       0, NULL  },
    { NULL,                    NULL,             NULL,       0, NULL  }
};


int    switch_symb(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    char *symb = NULL, sym[128] = {0};
    int pts = 0, i;
    DIALOG *di = NULL;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if ((pr == NULL) || (active_menu->dp == NULL) || (pr->one_p == NULL))
        {
            active_menu->flags |=  D_DISABLED;
            return D_O_K;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        if (pr->one_p == NULL || pr->one_p->dat == NULL || pr->one_p->cur_dat < 0
                || pr->one_p->cur_dat >= pr->one_p->n_dat)
        {
            return 0;
        }

        op = pr->one_p;
        ds = op->dat[op->cur_dat];
        symb = (char *) active_menu->dp;

        if (strncmp(symb, "none", 4) == 0)
        {
            if (ds->symb == NULL)
            {
                active_menu->flags |=  D_SELECTED;
            }
            else
            {
                active_menu->flags &= ~D_SELECTED;
            }
        }
        else if (strncmp(symb, "user", 5) == 0)
        {
            active_menu->flags &= ~D_SELECTED;
        }
        else
        {
            if ((ds->symb != NULL) && (strstr(ds->symb, symb) != NULL))
            {
                active_menu->flags |= D_SELECTED;
            }
            else
            {
                active_menu->flags &= ~D_SELECTED;
            }
        }

        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return D_O_K;
    }

    if (pr->one_p == NULL || pr->one_p->dat == NULL || pr->one_p->cur_dat < 0
            || pr->one_p->cur_dat >= pr->one_p->n_dat)
    {
        return 0;
    }

    op = pr->one_p;
    ds = op->dat[op->cur_dat];

    if ((symb = (char *) active_menu->dp) == NULL)
    {
        return D_O_K;
    }

    if (strncmp(symb, "user", 5) == 0)
    {
        if (ds->symb != NULL)
        {
            strncpy(sym, ds->symb, 128);
        }
        else
        {
            sym[0] = 0;
        }

        i = win_scanf("entrer your point Symbol, default size is 10 points\n"
                      "preceedings your symbol by \\pt8 will adjust it size\n"
                      "to 8 points %ls", sym);

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }

        symb = sym;

        if (ds->symb != NULL)
        {
            free(ds->symb);
        }

        ds->symb = NULL;
    }

    if (strncmp(symb, "none", 4) == 0)
    {
        symb = NULL;
    }

    if (ds->symb != NULL && symb != NULL)
    {
        if (sscanf(ds->symb, "\\pt%d", &pts) == 1)
        {
            snprintf(sym, 128, "\\pt%d %s", pts, symb);
            set_ds_point_symbol(ds, sym);
        }
        else
        {
            set_ds_point_symbol(ds, symb);
        }
    }
    else
    {
        set_ds_point_symbol(ds, symb);
    }

    pr->one_p->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    return D_O_K;
}
int switch_axis_pt_size(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    char *symb = NULL, sym[128] = {0};
    int pts = 0, i, ptsy;
    static int def_size = 10;
    DIALOG *di;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if ((pr == NULL) || (active_menu->text == NULL) || (pr->one_p == NULL))
        {
            active_menu->flags |=  D_DISABLED;
            return D_O_K;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        if (pr->one_p == NULL || pr->one_p->dat == NULL)
        {
            return 0;
        }

        op = pr->one_p;
        symb = active_menu->text;

        if (sscanf(symb, "%d", &pts) == 1)
        {
            if (active_menu->flags == X_AXIS)
            {
                if (op->x_prefix != NULL)
                {
                    ptsy = 10;
                    sscanf(op->x_prefix, "\\pt%d", &ptsy);

                    if (pts == ptsy)
                    {
                        active_menu->flags |=  D_SELECTED;
                    }
                    else
                    {
                        active_menu->flags &= ~D_SELECTED;
                    }
                }
                else
                {
                    if (pts == def_size)
                    {
                        active_menu->flags |=  D_SELECTED;
                    }
                    else
                    {
                        active_menu->flags &= ~D_SELECTED;
                    }
                }
            }

            if (active_menu->flags == Y_AXIS)
            {
                if (op->y_prefix != NULL)
                {
                    ptsy = 10;
                    sscanf(op->y_prefix, "\\pt%d", &ptsy);

                    if (pts == ptsy)
                    {
                        active_menu->flags |=  D_SELECTED;
                    }
                    else
                    {
                        active_menu->flags &= ~D_SELECTED;
                    }
                }
                else
                {
                    if (pts == def_size)
                    {
                        active_menu->flags |=  D_SELECTED;
                    }
                    else
                    {
                        active_menu->flags &= ~D_SELECTED;
                    }
                }
            }
        }
        else if (strncmp(symb, "define", 6) == 0)
        {
            if (active_menu->flags == X_AXIS)
            {
                if (op->x_prefix != NULL)
                {
                    ptsy = 10;
                    sscanf(op->x_prefix, "\\pt%d", &ptsy);

                    if ((ptsy > 14) || (ptsy < 4))
                    {
                        active_menu->flags |= D_SELECTED;
                    }
                    else
                    {
                        active_menu->flags &= ~D_SELECTED;
                    }
                }
                else
                {
                    active_menu->flags &= ~D_SELECTED;
                }
            }

            if (active_menu->flags == Y_AXIS)
            {
                if (op->y_prefix != NULL)
                {
                    ptsy = 10;
                    sscanf(op->y_prefix, "\\pt%d", &ptsy);

                    if ((ptsy > 14) || (ptsy < 4))
                    {
                        active_menu->flags |= D_SELECTED;
                    }
                    else
                    {
                        active_menu->flags &= ~D_SELECTED;
                    }
                }
                else
                {
                    active_menu->flags &= ~D_SELECTED;
                }
            }
        }

        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL || pr->one_p == NULL)
    {
        return D_O_K;
    }

    op = pr->one_p;

    if ((symb = active_menu->text) == NULL)
    {
        return D_O_K;
    }

    i = sscanf(symb, "%d", &pts);

    if ((strncmp(symb, "define", 6) == 0) || (i == 0))
    {
        if (active_menu->flags == X_AXIS)
        {
            i = win_scanf("Size of X labels %d", &pts);

            if (i == WIN_CANCEL)
            {
                return D_O_K;
            }

            i = 1;
        }

        if (active_menu->flags == Y_AXIS)
        {
            i = win_scanf("Size of Y labels %d", &pts);

            if (i == WIN_CANCEL)
            {
                return D_O_K;
            }

            i = 1;
        }
    }
    else
    {
        i = sscanf(symb, "%d", &pts);
    }

    if (i != 1)
    {
        return D_O_K;
    }

    snprintf(sym, 128, "\\pt%d ", pts);

    if (active_menu->flags == X_AXIS)
    {
        if (op->x_prefix)
        {
            free(op->x_prefix);
        }

        op->x_prefix = strdup(sym);
    }

    if (active_menu->flags == Y_AXIS)
    {
        if (op->y_prefix)
        {
            free(op->y_prefix);
        }

        op->y_prefix = strdup(sym);
    }

    pr->one_p->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    return D_O_K;
}

int    switch_symb_size(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    char *symb = NULL, sym[128] = {0}, sym1[128] = {0};
    int pts = 0, i, ptsy;
    static int def_size = 6;
    DIALOG *di = NULL;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if ((pr == NULL) || (active_menu->text == NULL) || (pr->one_p == NULL))
        {
            active_menu->flags |=  D_DISABLED;
            return D_O_K;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        if (pr->one_p == NULL || pr->one_p->dat == NULL || pr->one_p->cur_dat < 0
                || pr->one_p->cur_dat >= pr->one_p->n_dat)
        {
            return 0;
        }

        op = pr->one_p;
        ds = op->dat[op->cur_dat];
        symb = active_menu->text;

        if (sscanf(symb, "%d", &pts) == 1)
        {
            if (ds->symb != NULL)
            {
                ptsy = 10;
                sscanf(ds->symb, "\\pt%d", &ptsy);

                if (pts == ptsy)
                {
                    active_menu->flags |=  D_SELECTED;
                }
                else
                {
                    active_menu->flags &= ~D_SELECTED;
                }
            }
            else
            {
                if (pts == def_size)
                {
                    active_menu->flags |=  D_SELECTED;
                }
                else
                {
                    active_menu->flags &= ~D_SELECTED;
                }
            }
        }
        else if (strncmp(symb, "define", 6) == 0)
        {
            if (ds->symb != NULL)
            {
                ptsy = 10;
                sscanf(ds->symb, "\\pt%d", &ptsy);

                if ((ptsy > 14) || (ptsy < 4))
                {
                    active_menu->flags |= D_SELECTED;
                }
                else
                {
                    active_menu->flags &= ~D_SELECTED;
                }
            }
            else
            {
                active_menu->flags &= ~D_SELECTED;
            }
        }

        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return D_O_K;
    }

    if (pr->one_p == NULL || pr->one_p->dat == NULL || pr->one_p->cur_dat < 0
            || pr->one_p->cur_dat >= pr->one_p->n_dat)
    {
        return 0;
    }

    op = pr->one_p;
    ds = op->dat[op->cur_dat];

    if (ds->symb == NULL)
    {
        return win_printf_OK("No point symbol define yet!");
    }

    if ((symb = active_menu->text) == NULL)
    {
        return D_O_K;
    }

    i = sscanf(symb, "%d", &pts);

    if ((strncmp(symb, "define", 6) == 0) || (i == 0))
    {
        i = win_scanf("Size of data points symbol %d", &pts);

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }
    }

    i = sscanf(ds->symb, "\\pt%d %s", &ptsy, sym);

    //    win_printf("i %d ds->symb %s",i,ds->symb);
    if (i != 2)
    {
        strncpy(sym, ds->symb, 128);
    }
    else
    {
        def_size = ptsy;
    }

    //win_printf("i %d sym %s pts %d",i,sym,pts);
    snprintf(sym1, 128, "\\pt%d %s", pts, sym);
    //win_printf("i %d sym %s pts %d",i,sym1,pts);
    set_ds_point_symbol(ds, sym1);
    pr->one_p->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    return D_O_K;
}



MENU x_axis_point_style_size_menu[32] =
{
    {"4",    switch_axis_pt_size,    NULL,    X_AXIS,    NULL},
    {"5",    switch_axis_pt_size,    NULL,    X_AXIS,    NULL},
    {"6",    switch_axis_pt_size,    NULL,    X_AXIS,    NULL},
    {"7",    switch_axis_pt_size,    NULL,    X_AXIS,    NULL},
    {"8",    switch_axis_pt_size,    NULL,    X_AXIS,    NULL},
    {"9",    switch_axis_pt_size,    NULL,    X_AXIS,    NULL},
    {"10",    switch_axis_pt_size,    NULL,    X_AXIS,    NULL},
    {"11",    switch_axis_pt_size,    NULL,    X_AXIS,    NULL},
    {"12",    switch_axis_pt_size,    NULL,    X_AXIS,    NULL},
    {"13",    switch_axis_pt_size,    NULL,    X_AXIS,    NULL},
    {"14",    switch_axis_pt_size,    NULL,    X_AXIS,    NULL},
    {"define",    switch_axis_pt_size,    NULL,    X_AXIS,    NULL},
    {NULL,        NULL,    NULL,    0,    NULL},
};



MENU y_axis_point_style_size_menu[32] =
{
    {"4",    switch_axis_pt_size,    NULL,    Y_AXIS,    NULL},
    {"5",    switch_axis_pt_size,    NULL,    Y_AXIS,    NULL},
    {"6",    switch_axis_pt_size,    NULL,    Y_AXIS,    NULL},
    {"7",    switch_axis_pt_size,    NULL,    Y_AXIS,    NULL},
    {"8",    switch_axis_pt_size,    NULL,    Y_AXIS,    NULL},
    {"9",    switch_axis_pt_size,    NULL,    Y_AXIS,    NULL},
    {"10",    switch_axis_pt_size,    NULL,    Y_AXIS,    NULL},
    {"11",    switch_axis_pt_size,    NULL,    Y_AXIS,    NULL},
    {"12",    switch_axis_pt_size,    NULL,    Y_AXIS,    NULL},
    {"13",    switch_axis_pt_size,    NULL,    Y_AXIS,    NULL},
    {"14",    switch_axis_pt_size,    NULL,    Y_AXIS,    NULL},
    {"define",    switch_axis_pt_size,    NULL,    Y_AXIS,    NULL},
    {NULL,        NULL,    NULL,    0,    NULL},
};


MENU point_style_size_menu[32] =
{
    {"4",    switch_symb_size,    NULL,    0,    NULL},
    {"5",    switch_symb_size,    NULL,    0,    NULL},
    {"6",    switch_symb_size,    NULL,    0,    NULL},
    {"7",    switch_symb_size,    NULL,    0,    NULL},
    {"8",    switch_symb_size,    NULL,    0,    NULL},
    {"9",    switch_symb_size,    NULL,    0,    NULL},
    {"10",    switch_symb_size,    NULL,    0,    NULL},
    {"11",    switch_symb_size,    NULL,    0,    NULL},
    {"12",    switch_symb_size,    NULL,    0,    NULL},
    {"13",    switch_symb_size,    NULL,    0,    NULL},
    {"14",    switch_symb_size,    NULL,    0,    NULL},
    {"define",    switch_symb_size,    NULL,    0,    NULL},
    {NULL,        NULL,    NULL,    0,    NULL},
};



MENU point_style_menu[32] =
{
    {"Pt size",   NULL,   point_style_size_menu, 0,    NULL},
    {"none",    switch_symb,    NULL,    0, (char *)    "none"}, // unsafe cast
    {"user",    switch_symb,    NULL,    0, (char *)    "user"},
    {"\\pl",    switch_symb,    NULL,    0, (char *)    "\\pl"},
    {"\\cr",    switch_symb,    NULL,    0, (char *)    "\\cr"},
    {"\\di",    switch_symb,    NULL,    0, (char *)    "\\di"},
    {"\\sq",    switch_symb,    NULL,    0, (char *)    "\\sq"},
    {"\\oc",    switch_symb,    NULL,    0, (char *)    "\\oc"},
    {"\\fd",    switch_symb,    NULL,    0, (char *)    "\\fd"},
    {"\\fs",    switch_symb,    NULL,    0, (char *)    "\\fs"},
    {"\\fx",    switch_symb,    NULL,    0, (char *)    "\\fx"},
    {"\\fp",    switch_symb,    NULL,    0, (char *)    "\\fp"},
    {"\\bu",    switch_symb,    NULL,    0, (char *)    "\\bu"},
    {NULL,        NULL,    NULL,    0,    NULL},
};

int    def_line_color(void)
{
    int i;
    pltreg *pr = NULL;
    imreg *imr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    DIALOG *di = NULL;
    pr = find_pr_in_current_dialog(NULL);
    imr = find_imr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        i = ((pr == NULL) && (imr == NULL)) ? 1 : 0;

        if ((i != 0) || (active_menu->dp == NULL))
        {
            active_menu->flags |=  D_DISABLED;
            return D_O_K;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        if (pr != NULL)
        {
            op = pr->one_p;
        }
        else if (imr != NULL)
        {
            op = find_oi_cur_op(imr->one_i);
        }
        else
        {
            return D_O_K;
        }

        if (op == NULL)
        {
            return D_O_K;
        }

        op->cur_dat = (op->cur_dat < 0) ? 0 : op->cur_dat;
        op->cur_dat = (op->cur_dat < op->n_dat) ? op->cur_dat : 0;
        ds = op->dat[op->cur_dat];

        if (ds == NULL || active_menu->dp == NULL)
        {
            return D_O_K;
        }

        if (*(int *)active_menu->dp == ds->color)
        {
            active_menu->flags |=  D_SELECTED;
        }
        else
        {
            active_menu->flags &= ~D_SELECTED;
        }

        return D_O_K;
    }

    if (pr != NULL)
    {
        op = pr->one_p;
    }
    else if (imr != NULL)
    {
        op = find_oi_cur_op(imr->one_i);
    }
    else
    {
        return D_O_K;
    }

    if (op == NULL || active_menu->dp == NULL)
    {
        return D_O_K;
    }

    op->cur_dat = (op->cur_dat < 0) ? 0 : op->cur_dat;
    op->cur_dat = (op->cur_dat < op->n_dat) ? op->cur_dat : 0;
    ds = op->dat[op->cur_dat];

    if (ds == NULL)
    {
        return D_O_K;
    }

    set_ds_line_color(ds, *(int *)active_menu->dp);

    if (pr != NULL)
    {
        di = find_dialog_associated_to_pr(pr, NULL);
        op->need_to_refresh = 1;
    }

    if (imr != NULL)
    {
        di = find_dialog_associated_to_imr(imr, NULL);
        imr->one_i->need_to_refresh |= EXTRA_PLOT_NEED_REFRESH;
    }

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    return D_O_K;
}

int switch_line_color_bright_dark(void)
{
    int i;
    pltreg *pr = NULL;
    imreg *imr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    DIALOG *di = NULL;
    pr = find_pr_in_current_dialog(NULL);
    imr = find_imr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        i = ((pr == NULL) && (imr == NULL)) ? 1 : 0;

        if ((i != 0))
        {
            active_menu->flags |=  D_DISABLED;
            return D_O_K;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        if (pr != NULL)
        {
            op = pr->one_p;
        }
        else if (imr != NULL)
        {
            op = find_oi_cur_op(imr->one_i);
        }
        else
        {
            return D_O_K;
        }

        if (op == NULL)
        {
            return D_O_K;
        }

        active_menu->flags &= ~D_SELECTED;
        return D_O_K;
    }

    if (pr != NULL)
    {
        op = pr->one_p;
    }
    else if (imr != NULL)
    {
        op = find_oi_cur_op(imr->one_i);
    }
    else
    {
        return D_O_K;
    }

    if (op == NULL)
    {
        return D_O_K;
    }

    for (i = 0; i < op->n_dat; i++)
    {
        ds = op->dat[i];

        if (ds == NULL)
        {
            return D_O_K;
        }

        if (ds->color == Yellow)
        {
            ds->color = Brown;
        }
        else if (ds->color == Brown)
        {
            ds->color = Yellow;
        }
        else if (ds->color == Blue)
        {
            ds->color = Lightblue;
        }
        else if (ds->color == Lightblue)
        {
            ds->color = Blue;
        }
        else if (ds->color == Green)
        {
            ds->color = Lightgreen;
        }
        else if (ds->color == Lightgreen)
        {
            ds->color = Green;
        }
        else if (ds->color == Red)
        {
            ds->color = Lightred;
        }
        else if (ds->color == Lightred)
        {
            ds->color = Red;
        }
        else if (ds->color == Cyan)
        {
            ds->color = Lightcyan;
        }
        else if (ds->color == Lightcyan)
        {
            ds->color = Cyan;
        }
        else if (ds->color == Magenta)
        {
            ds->color = Lightmagenta;
        }
        else if (ds->color == Lightmagenta)
        {
            ds->color = Magenta;
        }
        else if (ds->color == Darkgray)
        {
            ds->color = Lightgray;
        }
        else if (ds->color == Lightgray)
        {
            ds->color = Darkgray;
        }
        else if (ds->color == Black)
        {
            ds->color = White;
        }
        else if (ds->color == White)
        {
            ds->color = Black;
        }
    }

    if (pr != NULL)
    {
        di = find_dialog_associated_to_pr(pr, NULL);
        op->need_to_refresh = 1;
    }

    if (imr != NULL)
    {
        di = find_dialog_associated_to_imr(imr, NULL);
        imr->one_i->need_to_refresh |= EXTRA_PLOT_NEED_REFRESH;
    }

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    return D_O_K;
}


int    def_line_invisible(void)
{
    int i;
    pltreg *pr = NULL;
    imreg *imr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    DIALOG *di = NULL;
    pr = find_pr_in_current_dialog(NULL);
    imr = find_imr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        i = ((pr == NULL) && (imr == NULL)) ? 1 : 0;

        if ((i != 0))
        {
            active_menu->flags |=  D_DISABLED;
            return D_O_K;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        if (pr != NULL)
        {
            op = pr->one_p;
        }
        else if (imr != NULL)
        {
            op = find_oi_cur_op(imr->one_i);
        }
        else
        {
            return D_O_K;
        }

        if (op == NULL)
        {
            return D_O_K;
        }

        op->cur_dat = (op->cur_dat < 0) ? 0 : op->cur_dat;
        op->cur_dat = (op->cur_dat < op->n_dat) ? op->cur_dat : 0;
        ds = op->dat[op->cur_dat];

        if (ds == NULL)
        {
            return D_O_K;
        }

        if (ds->invisible)
        {
            active_menu->flags |=  D_SELECTED;
        }
        else
        {
            active_menu->flags &= ~D_SELECTED;
        }

        return D_O_K;
    }

    if (pr != NULL)
    {
        op = pr->one_p;
    }
    else if (imr != NULL)
    {
        op = find_oi_cur_op(imr->one_i);
    }
    else
    {
        return D_O_K;
    }

    if (op == NULL)
    {
        return D_O_K;
    }

    op->cur_dat = (op->cur_dat < 0) ? 0 : op->cur_dat;
    op->cur_dat = (op->cur_dat < op->n_dat) ? op->cur_dat : 0;
    ds = op->dat[op->cur_dat];

    if (ds == NULL)
    {
        return D_O_K;
    }

    ds->invisible = (ds->invisible) ? 0 : 1;

    //if (ds->invisible) win_printf("Data set set invisible");
    if (pr != NULL)
    {
        di = find_dialog_associated_to_pr(pr, NULL);
        op->need_to_refresh = 1;
    }

    if (imr != NULL)
    {
        di = find_dialog_associated_to_imr(imr, NULL);
        imr->one_i->need_to_refresh |= EXTRA_PLOT_NEED_REFRESH;
    }

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    return D_O_K;
}


int    ds_menu_kill_ds(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int i;
    DIALOG *di = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)
    {
        win_printf_OK("Cannot find plot data set!");
    }

    if (pr == NULL)
    {
        return D_O_K;
    }

    if (op->data_changing)
    {
        win_printf_OK("Cannot destroy data set!\nplot is still in construction!");
    }

    i = win_printf("Do you really want to kill\nthe %d data set ?",
                   pr->one_p->cur_dat);

    if (i == WIN_CANCEL)
    {
        return 0;
    }

    if (opds_copied != NULL)
    {
        free_one_plot(opds_copied);
        opds_copied = NULL;
        ds_copied = NULL;
    }

    i = op->n_dat;
    op->n_dat = 0;

    if ((opds_copied = duplicate_plot_data(op, NULL)) == NULL)
    {
        op->n_dat = i;
        win_printf_OK("Could not duplicate plot");
    }
    else
    {
        ds_copied = duplicate_data_set(ds, NULL);

        if (ds_copied == NULL)
        {
            op->n_dat = i;
            win_printf_OK("Could not duplicate ds");
        }
        else
        {
            if (add_one_plot_data(opds_copied, IS_DATA_SET, (void *)ds_copied))
            {
                op->n_dat = i;
                win_printf_OK("Could not add ds");
            }
        }
    }

    op->n_dat = i;
    remove_one_plot_data(pr->one_p, IS_DATA_SET, (void *)ds);
    op->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    return D_O_K;
}


int    ds_menu_kill_multiple_ds(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL, **orgds = NULL;
    int i, nds;
    DIALOG *di = NULL;
    char question[1024] = {0};
    static int ndss = 0, ndse = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)
    {
        win_printf_OK("Cannot find plot data set!");
    }

    if (pr == NULL)
    {
        return D_O_K;
    }

    if (op->data_changing)
    {
        win_printf_OK("Cannot destroy data set!\nplot is still in construction!");
    }

    ndss = (ndss >= pr->one_p->n_dat) ? 0 : ndss;
    ndse = (ndse > pr->one_p->n_dat) ? pr->one_p->n_dat : ndse;
    snprintf(question,sizeof(question),"Your plot contains %d datasets\n"
	     "The current one is %d\n"
	     "I am going to kill a set of them\n"
	     "specify the range you want to kill\n[%%4d,%%4d[\n"
             ,pr->one_p->n_dat,pr->one_p->cur_dat);
    i = win_scanf(question,&ndss,&ndse);

    if (i == WIN_CANCEL)
    {
        return 0;
    }

    if (opds_copied != NULL)
    {
        free_one_plot(opds_copied);
        opds_copied = NULL;
        ds_copied = NULL;
    }
    nds = op->n_dat;
    orgds = op->dat;
    op->dat = orgds + ndss;
    op->n_dat = ndse - ndss;
    if ((opds_copied = duplicate_plot_data(op, NULL)) == NULL)
      {
        op->n_dat = nds;
	op->dat = orgds;
        win_printf_OK("Could not duplicate plot");
      }
    else
      {
        op->n_dat = nds;
	op->dat = orgds;
	for (i = ndse -1; i >= ndss; i--)
	  {
	    remove_one_plot_data(pr->one_p, IS_DATA_SET, (void *)op->dat[i]);
	  }
      }
    op->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    return D_O_K;
}

int    ds_show(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    DIALOG *di = NULL;
    char extra[2048] = {0};
    int i;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("plot not found");
    }

    op = pr->one_p;

    if (op == NULL || op->dat == NULL)
    {
        return D_O_K;
    }

    ds = op->dat[op->cur_dat];

    if (ds == NULL)
    {
        return D_O_K;
    }

    for (i = 0; i < 8; i++)
    {
        if (ds->src_parameter_type[i] != NULL)
        {
            snprintf(extra + strlen(extra), sizeof(extra) - strlen(extra), "%s %g\n"
                     , ds->src_parameter_type[i], ds->src_parameter[i]);
        }
    }

    for (i = 0; i < 16; i++)
    {
        if (ds->user_ispare[i] != 0 || ds->user_fspare[i] != 0)
        {
            snprintf(extra + strlen(extra), sizeof(extra) - strlen(extra), "user spare[%d] int[%d]; float[%g]\n"
                     , i, ds->user_ispare[i], ds->user_fspare[i]);
        }
    }
    // add other errors !
    win_printf("Plot %s %s\n has %d data sets, the current one being %d\n"
               "with %d points (%d allocated) %s %s \n"
               "created {\\color{yellow}%s}\n"
               "{\\color{yellow}Source :}\n %s\n"
               "{\\color{yellow}Treatement :}\n %s\n"
               "{\\color{yellow}History :}\n %s\n%s",
               (op->dir != NULL) ? backslash_to_slash(op->dir) : "Not defined",
               (op->filename != NULL) ? backslash_to_slash(op->filename) :
               "untitled.gr", op->n_dat, op->cur_dat, ds->nx, ds->mx,
               (ds->xe) ? "with error in x" : " ", (ds->ye) ? "with error in y" : " ",
               ctime(&(ds->time)), (ds->source != NULL) ? ds->source : "No source",
               (ds->treatement != NULL) ? ds->treatement : "No treatement",
               (ds->history != NULL) ? ds->history : "No history", extra);
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    return D_O_K;
}



MENU line_color_menu[32] =
{
    { "Yellow",        def_line_color,    NULL,    0, (void *)& Yellow },
    { "Green",        def_line_color,    NULL,    0, (void *)& Green },
    { "Blue",         def_line_color,    NULL,    0, (void *)& Blue },
    { "Red",            def_line_color,    NULL,    0, (void *)& Red },
    { "Cyan",        def_line_color,    NULL,    0, (void *)& Cyan },
    { "Magenta",        def_line_color,    NULL,    0, (void *)& Magenta },
    { "Black",        def_line_color,    NULL,    0, (void *)& Black },
    { "Brown",        def_line_color,    NULL,    0, (void *)& Brown },
    { "Lightgray",    def_line_color,    NULL,    0, (void *)& Lightgray },
    { "Darkgray",     def_line_color,    NULL,    0, (void *)& Darkgray },
    { "Lightblue",    def_line_color,    NULL,    0, (void *)& Lightblue },
    { "Lightgreen",    def_line_color,    NULL,    0, (void *)& Lightgreen },
    { "Lightcyan",    def_line_color,    NULL,    0, (void *)& Lightcyan },
    { "Lightred",    def_line_color,    NULL,    0, (void *)& Lightred },
    { "Lightmagenta",    def_line_color,    NULL,    0, (void *)& Lightmagenta },
    { "White",        def_line_color,    NULL,    0, (void *)& White },
    { "Invisible",        def_line_invisible,    NULL,    0,    NULL },
    { "Dark <=> Bright",        switch_line_color_bright_dark,    NULL,    0,    NULL },
    { NULL,                    NULL,             NULL,       0, NULL  }
};
#ifdef XV_GTK_DIALOG
int ds_view(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("plot not found");
    }

    op = pr->one_p;

    if (op == NULL)
    {
        return D_O_K;
    }

    ds = op->dat[op->cur_dat];
    display_raw_data(ds);
    return 0;
}
#endif
int ds_paste_options(void)
{
    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    win_scanf("Keep color on paste %b\n", &keep_color);
    set_config_int("PASTE_OPTIONS", "KEEP_COLOR", keep_color);
    return 0;
}
int ds_copy(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("plot not found");
    }

    op = pr->one_p;

    if (op == NULL)
    {
        return D_O_K;
    }

    ds = op->dat[op->cur_dat];

    if (opds_copied != NULL)
    {
        free_one_plot(opds_copied);
        opds_copied = NULL;
        ds_copied = NULL;
    }

    i = op->n_dat;
    op->n_dat = 0;

    if ((opds_copied = duplicate_plot_data(op, NULL)) == NULL)
    {
        op->n_dat = i;
        win_printf_OK("Could not duplicate plot");
    }
    else
    {
        ds_copied = duplicate_data_set(ds, NULL);

        if (ds_copied == NULL)
        {
            op->n_dat = i;
            win_printf_OK("Could not duplicate ds");
        }
        else
        {
            if (add_one_plot_data(opds_copied, IS_DATA_SET, (void *)ds_copied))
            {
                op->n_dat = i;
                win_printf_OK("Could not add ds");
            }
        }
    }

    op->n_dat = i;
    remove_all_keyboard_proc();
    scan_and_update(plot_menu);
    return D_O_K;
}



MENU ds_menu[32] =
{
    { "line style",      NULL,       line_style_menu,       0, NULL  },
    { "line style all ds",      NULL,       line_style_all_ds_menu,       0, NULL  },
    { "line color",      NULL,       line_color_menu,       0, NULL  },
    { "point style",     NULL,       point_style_menu,       0, NULL  },
    { "Kill dataset",    ds_menu_kill_ds,       NULL,       0, NULL  },
    { "Kill multiple ds",    ds_menu_kill_multiple_ds,       NULL,       0, NULL  },
    { "dataset &Info\tCtrl-I",    ds_show,    NULL,       0, NULL  },
    { "dataset copy",    ds_copy,    NULL,       0, NULL  },
    { "dataset paste options",    ds_paste_options,    NULL,       0, NULL  },
#ifdef XV_GTK_DIALOG
    { "display data raw", ds_view, NULL, 0, NULL},
#endif
    { NULL,                    NULL,             NULL,       0, NULL  }
};


int    do_ds_menu(int item)
{
    /*
       pltreg *pr = NULL;
       DIALOG *di;


       pr = find_pr_in_current_dialog(NULL);
       if (pr == NULL) return win_printf_OK("could not find plot region!");
       if (item == 0)
       {
       pr->one_p->dat[pr->one_p->cur_dat]->m = line_style_selected;
       pr->one_p->need_to_refresh = 1;
       di = find_dialog_associated_to_pr(pr, NULL);
       if (di) di->proc(MSG_DRAW, di, 0);
       else return win_printf_OK("dialog not found");

       }
       */
    (void)item;
    return D_O_K;
    /*    else return broadcast_dialog_message(MSG_DRAW,0);
          return 1;
          */
}

int    op_menu_kill_op_to_right_ask(int doask)
{
    pltreg *pr = NULL;
    int i;
    DIALOG *di = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("plot region not found");
    }

    if (doask != 0)
    {
        i = win_printf("Do you really want to kill all plot \ngreater that %d plot ?",
                       pr->cur_op);

        if (i == WIN_CANCEL)
        {
            return 0;
        }
    }

    while (pr->n_op > (pr->cur_op + 1))
    {
        O_p *cur_op = pr->o_p[pr->n_op - 1];

        if (pr->o_p[pr->n_op - 1]->data_changing)
        {
            warning_message("Cannot destroy plot, it is still in construction!");
            break;
        }

        remove_data(pr, IS_ONE_PLOT, (void *)cur_op);
    }

    pr->one_p->need_to_refresh = 1;
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    return D_O_K;
}

int    op_menu_kill_op_ask(int doask)
{
    pltreg *pr = NULL;
    int i;
    DIALOG *di = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("plot not found");
    }

    if (pr->one_p->data_changing)
    {
        win_printf_OK("Cannot destroy plot, it is still in construction!");
    }

    if (doask != 0)
    {
        i = win_printf("Do you really want to kill\nthe %d plot ?",
                       pr->cur_op);

        if (i == WIN_CANCEL)
        {
            return 0;
        }
    }

    if (op_copied != NULL)
    {
        free_one_plot(op_copied);
        op_copied = NULL;
    }

    if ((op_copied = duplicate_plot_data(pr->one_p, NULL)) == NULL)
    {
        return D_O_K;
    }

    remove_data(pr, IS_ONE_PLOT, (void *)pr->one_p);

    if (pr->n_op > 0)
    {
        pr->one_p->need_to_refresh = 1;
    }

    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    return D_O_K;
}

int    op_menu_kill_op(void)
{
    return op_menu_kill_op_ask(1);
}
int op_menu_kill_op_to_right(void)
{
    return op_menu_kill_op_to_right_ask(true);
}
int    all_plt(void)
{
    int i;
    pltreg *pr = NULL;
    DIALOG *di = NULL;
    pr = find_pr_in_current_dialog(NULL);

    if (updating_menu_state != 0)
    {
        if (pr == NULL || pr->n_op == 1)
        {
            active_menu->flags |=  D_DISABLED;
            return D_O_K;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    if (pr == NULL)
    {
        return win_printf_OK("can't find plot");
    }

    if (pr->n_op == 1)
    {
        return win_printf_OK("I find only one plot !");
    }

    for (i = 0 ; i < pr->n_op ; i++)
    {
        if (pr->o_p[i]->bplt.n_box == 0)
        {
            return win_printf_OK("plot %d need to be drawn first!", i);
        }
    }

    if (strcmp(active_menu->text, "plots in 1 column") == 0)
    {
        pr->display_mode = ALL_PLOTS + 1;
    }
    else if (strcmp(active_menu->text, "plots in 2 column") == 0)
    {
        pr->display_mode = ALL_PLOTS + 2;
    }
    else if (strcmp(active_menu->text, "plots in 3 column") == 0)
    {
        pr->display_mode = ALL_PLOTS + 3;
    }

    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    return D_O_K;
    /*    return D_REDRAWME;
    */
}

int op_copy(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("plot not found");
    }

    op = pr->one_p;

    if (op == NULL)
    {
        return D_O_K;
    }

    if (op_copied != NULL)
    {
        free_one_plot(op_copied);
        op_copied = NULL;
    }

    if ((op_copied = duplicate_plot_data(op, NULL)) == NULL)
    {
        return D_O_K;
    }

    return D_O_K;
}


MENU op_menu[32] =
{
    { "Kill plot",    op_menu_kill_op,       NULL,       0, NULL  },
    { "Copy plot",    op_copy,       NULL,       0, NULL  },
    { "plots in 1 column",    all_plt,       NULL,       0, NULL  },
    { "plots in 2 column",    all_plt,       NULL,       0, NULL  },
    { "plots in 3 column",    all_plt,       NULL,       0, NULL  },

    { NULL,                    NULL,             NULL,       0, NULL  }
};


int    do_op_menu(int item)
{
    //    pltreg *pr;
    (void)item;
    //pr = find_pr_in_current_dialog(NULL);
    return broadcast_dialog_message(MSG_DRAW, 0);
}


MENU y_axis_label_menu[32] =
{
    { "Change Decade",      NULL,       NULL,       0, NULL  },
    { "Modify Units",      NULL,       NULL,       0, NULL  },
    { "New Units",      NULL,       NULL,       0, NULL  },
    { NULL,                    NULL,             NULL,       0, NULL  }
};


int do_y_axis_label_menu(int item)
{
    mn_index = Y_AXIS;

    if (item == 0)
    {
        return modify_un_decade();
    }

    if (item == 1)
    {
        return modify_units();
    }

    if (item == 2)
    {
        return add_new_units();
    }
    else
    {
        return 0;
    }
}

int build_and_display_y_axis_menu(pltreg *pr, O_p *op)
{
    int i, j, item, mod_un = 0;
    char s[256] = {0};
    DIALOG *di = NULL;
    int action[32] = {0};

    for (i = 0; i < 32; i++)
    {
        y_axis_label_menu[i].child = NULL;
        y_axis_label_menu[i].proc = NULL;
    }

    for (i = 0; i < op->n_yu && i < 24; i++)
    {
        snprintf(s, 256, "  %s", (op->yu[i]->name != NULL) ? op->yu[i]->name : "no name");
        y_axis_label_menu[i].text = strdup(s);
        y_axis_label_menu[i].flags = (i == op->c_yu) ? D_SELECTED : 0;
	action[i] = 1;
    }
    action[i] = 0;
    y_axis_label_menu[i++].text = strdup("\0");
    action[i] = 2;
    y_axis_label_menu[i++].text = strdup("New Units");

    if (op->n_yu > 1 && op->c_yu > 0)
      {
	snprintf(s, 256, "remove %s", ((op->yu[op->c_yu]->name != NULL) &&
				       (strlen(op->yu[op->c_yu]->name) > 0)) ?
		 op->yu[op->c_yu]->name : "no name");
	action[i] = 3;
	y_axis_label_menu[i++].text = strdup(s);
	snprintf(s, 256, "Modify unit %s", ((op->yu[op->c_yu]->name != NULL) &&
				       (strlen(op->yu[op->c_yu]->name) > 0)) ?
		 op->yu[op->c_yu]->name : "no name");
	action[i] = 4;
	y_axis_label_menu[i++].text = strdup(s);
	snprintf(s, 256, "Change decade %s", ((op->yu[op->c_yu]->name != NULL) &&
				       (strlen(op->yu[op->c_yu]->name) > 0)) ?
		 op->yu[op->c_yu]->name : "no name");
	action[i] = 5;
	y_axis_label_menu[i++].text = strdup(s);
	mod_un = 1;
      }
    snprintf(s, 256, "Copy Units %s", ((op->yu[op->c_yu]->name != NULL) &&
				       (strlen(op->yu[op->c_yu]->name) > 0)) ?
	     op->yu[op->c_yu]->name : "no name");
    action[i] = 6;
    y_axis_label_menu[i++].text = strdup(s);
    if (yun_plot != NULL)
      {
	snprintf(s, 256, "Insert Y copied unit %s", ((yun_plot->name != NULL) &&
					    (strlen(yun_plot->name) > 0)) ?
		 yun_plot->name : "no name");
	action[i] = 7;
	y_axis_label_menu[i++].text = strdup(s);
      }
    if (xun_plot != NULL)
      {
	snprintf(s, 256, "Insert X copied unit %s", ((xun_plot->name != NULL) &&
					    (strlen(xun_plot->name) > 0)) ?
		 xun_plot->name : "no name");
	action[i] = 8;
	y_axis_label_menu[i++].text = strdup(s);
      }
    action[i] = 0;
    y_axis_label_menu[i++].text = strdup("\0");
    y_axis_label_menu[i].child = NULL;
    action[i] = 9;
    y_axis_label_menu[i++].text = strdup("select prime");
    action[i] = 10;
    y_axis_label_menu[i].text = strdup("Y Axis pt size");
    y_axis_label_menu[i].child = y_axis_point_style_size_menu;
    y_axis_label_menu[i].proc = NULL;
    y_axis_label_menu[i].flags = 0;
    y_axis_label_menu[i++].dp = NULL;
    action[i] = 0;
    y_axis_label_menu[i].text = NULL;
    y_axis_label_menu[i].text = NULL;
    item = do_menu(y_axis_label_menu, mouse_x, mouse_y);

    for (j = 0; j < i; j++)
    {
        if (y_axis_label_menu[j].text != NULL)
        {
            free(y_axis_label_menu[j].text);
        }

        y_axis_label_menu[j].text = NULL;
    }
    int offset = 5;
    if (item < 0)
    {
        return D_O_K;
    }

    mn_index = Y_AXIS;

    if ((action[item] == 1) && (item < op->n_yu))
    {
        return set_plt_y_unit_set(pr, item);
    }
    else if (action[item] == 2) //(item == op->n_yu + 1)
    {
        return add_new_units();
    }
    else if (action[item] == 3)
    {
        if (mod_un)
        {
            remove_one_plot_data(op, IS_Y_UNIT_SET, (void *)op->yu[op->c_yu]);
            return set_plt_y_unit_set(pr, op->c_yu);
        }
    }
    else if (action[item] == 4)//(item == op->n_yu + 3)
    {
        if (mod_un)
        {
            return modify_units();
        }
        else
        {
            op->c_yu_p = op->c_yu;
            return set_plt_y_unit_set(pr, op->c_yu);
        }
    }
    else if (action[item] == 5)//(item == op->n_yu + 4)
    {
        if (mod_un)
        {
            return modify_un_decade();
        }
    }
    else if (action[item] == 6)//(item == op->n_yu + 5)
    {
      if (yun_plot != NULL)
	{
	  free_unit_set(yun_plot);
	  yun_plot = NULL;
	}
      yun_plot = duplicate_unit_set(op->yu[op->c_yu], NULL);
      //win_printf("Y unitset copied %s\n"
      //		 "type %d ax %g dx %g decade %d mode %d\n"
      //		 ,yun_plot->name,yun_plot->type,yun_plot->ax,yun_plot->dx,yun_plot->decade
      //	 ,yun_plot->mode);

    }
    else if ((action[item] == 7) && (yun_plot != NULL)) //((item == op->n_yu + offset + 1) && (yun_plot != NULL))
    {
      //win_printf("Y unitset pasted %s\n"
      //	 "type %d ax %g dx %g decade %d mode %d\n"
      //	 ,yun_plot->name,yun_plot->type,yun_plot->ax,yun_plot->dx,yun_plot->decade
      //	 ,yun_plot->mode);
      create_attach_select_y_un_to_op(op, yun_plot->type, yun_plot->ax, yun_plot->dx,
				      yun_plot->decade, yun_plot->mode, yun_plot->name);
      offset++;
      di = find_dialog_associated_to_pr(pr, NULL);
      if (di)
        {	  di->proc(MSG_DRAW, di, 0);        }

    }
    else if ((action[item] == 8) && (xun_plot != NULL)) //((item == op->n_yu + offset + 2) && (xun_plot != NULL))
    {
      //win_printf("X unitset pasted %s\n"
      //	 "type %d ax %g dx %g decade %d mode %d\n"
      //	 ,xun_plot->name,xun_plot->type,xun_plot->ax,xun_plot->dx,xun_plot->decade
      //	 ,xun_plot->mode);
      create_attach_select_y_un_to_op(op, xun_plot->type, xun_plot->ax, xun_plot->dx,
				      xun_plot->decade, xun_plot->mode, xun_plot->name);
      offset++;
      di = find_dialog_associated_to_pr(pr, NULL);
      if (di)
        {	  di->proc(MSG_DRAW, di, 0);        }

    }
    else if (action[item] == 9) //(item == op->n_yu + offset + 2)
    {
        op->c_yu_p = op->c_yu; // activate prime menu
        return set_plt_y_unit_set(pr, op->c_yu);
    }
    else if (action[item] == 10)//(item == op->n_yu + offset + 3)
    {
        di = find_dialog_associated_to_pr(pr, NULL);

        if (di)
        {
            di->proc(MSG_DRAW, di, 0);
        }
        else
        {
            display_title_message("dialog not found");
        }

        return D_O_K;
    }
    else
    {
        return D_O_K;
    }
    return D_O_K;
}

int build_and_display_y_axis_prime_menu(pltreg *pr, O_p *op)
{
    int i, j, item, mod_un = 0;
    char s[256] = {0};
    DIALOG *di = NULL;

    for (i = 0; i < 32; i++)
    {
        y_axis_label_menu[i].child = NULL;
        y_axis_label_menu[i].proc = NULL;
    }

    for (i = 0; i < op->n_yu && i < 26; i++)
    {
        snprintf(s, 256, "%s", (op->yu[i]->name != NULL) ? op->yu[i]->name : "no name");
        y_axis_label_menu[i].text = strdup(s);
        y_axis_label_menu[i].flags = (i == op->c_yu_p) ? D_SELECTED : 0;
    }

    y_axis_label_menu[i++].text = strdup("\0");
    y_axis_label_menu[i++].text = strdup("New Units");

    if (op->n_yu > 1 && op->c_yu > 0)
    {
        snprintf(s, 256, "remove %s", ((op->yu[op->c_yu]->name != NULL) &&
                                       (strlen(op->yu[op->c_yu]->name) > 0)) ?
                 op->yu[op->c_yu]->name : "no name");
        y_axis_label_menu[i++].text = strdup(s);
        snprintf(s, 256, "Modify Units %s", ((op->yu[op->c_yu]->name != NULL) &&
                                             (strlen(op->yu[op->c_yu]->name) > 0)) ?
                 op->yu[op->c_yu]->name : "no name");
        y_axis_label_menu[i++].text = strdup(s);
        snprintf(s, 256, "Change Decade %s", ((op->yu[op->c_yu]->name != NULL) &&
                                              (strlen(op->yu[op->c_yu]->name) > 0)) ?
                 op->yu[op->c_yu]->name : "no name");
        y_axis_label_menu[i++].text = strdup(s);
        mod_un = 1;
    }

    y_axis_label_menu[i++].text = strdup("\0");
    y_axis_label_menu[i++].text = strdup("unselect prime");
    y_axis_label_menu[i].text = strdup("Y Axis pt size");
    y_axis_label_menu[i].child = y_axis_point_style_size_menu;
    y_axis_label_menu[i].proc = NULL;
    y_axis_label_menu[i].flags = 0;
    y_axis_label_menu[i++].dp = NULL;
    y_axis_label_menu[i++].text = NULL;
    item = do_menu(y_axis_label_menu, mouse_x, mouse_y);

    for (j = 0; j < i; j++)
    {
        if (y_axis_label_menu[j].text != NULL)
        {
            free(y_axis_label_menu[j].text);
        }

        y_axis_label_menu[j].text = NULL;
    }

    if (item < 0)
    {
        return D_O_K;
    }

    mn_index = Y_AXIS;

    if (item < op->n_yu)
    {
        op->c_yu_p = item;
        return set_plt_y_unit_set(pr, op->c_yu);
    }
    else if (item == op->n_yu + 1)
    {
        return add_new_units();
    }
    else if (item == op->n_yu + 2)
    {
        if (mod_un)
        {
            remove_one_plot_data(op, IS_Y_UNIT_SET, (void *)op->yu[op->c_yu_p]);
            return set_plt_y_unit_set(pr, op->c_yu);
        }
    }
    else if (item == op->n_yu + 3)
    {
        if (mod_un)
        {
            return modify_units();
        }
        else
        {
            op->c_yu_p = op->c_yu;
            return set_plt_y_unit_set(pr, op->c_yu);
        }
    }
    else if (item == op->n_yu + 4)
    {
        if (mod_un)
        {
            return modify_un_decade();
        }
    }
    else if (item == op->n_yu + 6)
    {
        op->c_yu_p = -1;
        return set_plt_y_unit_set(pr, op->c_yu);
    }
    else if (item == op->n_yu + 7)
    {
        di = find_dialog_associated_to_pr(pr, NULL);

        if (di)
        {
            di->proc(MSG_DRAW, di, 0);
        }
        else
        {
            display_title_message("dialog not found");
        }

        return D_O_K;
    }
    else
    {
        return D_O_K;
    }
    return D_O_K;
}



MENU x_axis_label_menu[32] =
{
    { "Change Decade",      NULL,       NULL,       0, NULL  },
    { "Modify Units",      NULL,       NULL,       0, NULL  },
    { "New Units",      NULL,       NULL,       0, NULL  },
    { NULL,                    NULL,             NULL,       0, NULL  }
};


int do_x_axis_label_menu(int item)
{
    mn_index = X_AXIS;

    if (item == 0)
    {
        return modify_un_decade();
    }

    if (item == 1)
    {
        return modify_units();
    }

    if (item == 2)
    {
        return add_new_units();
    }
    else
    {
        return 0;
    }
}

int build_and_display_x_axis_menu(pltreg *pr, O_p *op)
{
    int i, j, item, mod_un = 0;
    char s[256] = {0};
    DIALOG *di = NULL;
    int action[32] = {0};

    for (i = 0; i < 32; i++)
    {
        x_axis_label_menu[i].child = NULL;
        x_axis_label_menu[i].proc = NULL;
    }

    for (i = 0; i < op->n_xu && i < 26; i++)
    {
        snprintf(s, 256, "%s", (op->xu[i]->name != NULL) ? op->xu[i]->name : "no name");
        x_axis_label_menu[i].text = strdup(s);
        x_axis_label_menu[i].flags = (i == op->c_xu) ? D_SELECTED : 0;
	action[i] = 1;
    }
    action[i] = 0;
    x_axis_label_menu[i++].text = strdup("\0");
    action[i] = 2;
    x_axis_label_menu[i++].text = strdup("New Units");

    if (op->n_xu > 1 && op->c_xu > 0)
      {
	snprintf(s, 256, "remove %s", ((op->xu[op->c_xu]->name != NULL) &&
				       (strlen(op->xu[op->c_xu]->name) > 0)) ?
		 op->xu[op->c_xu]->name : "no name");
	action[i] = 3;
	x_axis_label_menu[i++].text = strdup(s);
	snprintf(s, 256, "Modify unit %s", ((op->xu[op->c_xu]->name != NULL) &&
				       (strlen(op->xu[op->c_xu]->name) > 0)) ?
		 op->xu[op->c_xu]->name : "no name");
	action[i] = 4;
	x_axis_label_menu[i++].text = strdup(s);
	snprintf(s, 256, "Change decade %s", ((op->xu[op->c_xu]->name != NULL) &&
				       (strlen(op->xu[op->c_xu]->name) > 0)) ?
		 op->xu[op->c_xu]->name : "no name");
	action[i] = 5;
	x_axis_label_menu[i++].text = strdup(s);
	mod_un = 1;
      }
    snprintf(s, 256, "Copy units %s", ((op->xu[op->c_xu]->name != NULL) &&
				   (strlen(op->xu[op->c_xu]->name) > 0)) ?
	     op->xu[op->c_xu]->name : "no name");

    action[i] = 6;
    x_axis_label_menu[i++].text = strdup(s);
    if (yun_plot != NULL)
      {
	snprintf(s, 256, "Insert Y copied unit %s", ((yun_plot->name != NULL) &&
					    (strlen(yun_plot->name) > 0)) ?
		 yun_plot->name : "no name");
	action[i] = 7;
	x_axis_label_menu[i++].text = strdup(s);
      }
    if (xun_plot != NULL)
      {
	snprintf(s, 256, "Insert X copied unit %s", ((xun_plot->name != NULL) &&
					    (strlen(xun_plot->name) > 0)) ?
		 xun_plot->name : "no name");
	action[i] = 8;
	x_axis_label_menu[i++].text = strdup(s);
      }
    action[i] = 0;
    x_axis_label_menu[i++].text = strdup("\0");
    x_axis_label_menu[i].child = NULL;
    action[i] = 9;
    x_axis_label_menu[i++].text = strdup("select prime");
    x_axis_label_menu[i].text = strdup("Axis pt size");
    x_axis_label_menu[i].child = x_axis_point_style_size_menu;
    x_axis_label_menu[i].proc = NULL;
    x_axis_label_menu[i].flags = 0;
    action[i] = 10;
    x_axis_label_menu[i++].dp = NULL;
    x_axis_label_menu[i].text = NULL;
    item = do_menu(x_axis_label_menu, mouse_x, mouse_y-200);

    for (j = 0; j < i; j++)
    {
        if (x_axis_label_menu[j].text != NULL)
        {
            free(x_axis_label_menu[j].text);
        }

        x_axis_label_menu[j].text = NULL;
    }
    int offset = 5;
    if (item < 0)
    {
        return D_O_K;
    }

    mn_index = X_AXIS;

    if ((action[item] == 1) && (item < op->n_xu))
    {
        return set_plt_x_unit_set(pr, item);
    }
    else if (action[item] == 2) //(item == op->n_xu + 1)
    {
        return add_new_units();
    }
    else if (action[item] == 3) //(item == op->n_xu + 2)
    {
        if (mod_un)
        {
            remove_one_plot_data(op, IS_X_UNIT_SET, (void *)op->xu[op->c_xu]);
            return set_plt_x_unit_set(pr, op->c_xu);
        }
    }
    else if (action[item] == 4) //(item == op->n_xu + 3)
    {
        if (mod_un)
        {
            return modify_units();
        }
        else
        {
            op->c_xu_p = op->c_xu;
            return set_plt_x_unit_set(pr, op->c_xu);
        }
    }
    else if (action[item] == 5) //(item == op->n_xu + 4)
    {
        if (mod_un)
        {
            return modify_un_decade();
        }
    }
    else if (action[item] == 6) //(item == op->n_xu + 5)
    {
      if (xun_plot != NULL)
	{
	  free_unit_set(xun_plot);
	  xun_plot = NULL;
	}
      xun_plot = duplicate_unit_set(op->xu[op->c_xu], NULL);
      //      win_printf("X unitset copied %s\n"
      //	 "type %d ax %g dx %g decade %d mode %d\n"
      //	 ,xun_plot->name,xun_plot->type,xun_plot->ax,xun_plot->dx,xun_plot->decade
      //	 ,xun_plot->mode);
    }
    else if ((action[item] == 7) && (yun_plot != NULL))// (item == op->n_xu + offset + 1) && (yun_plot != NULL))
    {
      //win_printf("Y unitset pasted %s\n"
      //	 "type %d ax %g dx %g decade %d mode %d\n"
      //	 ,yun_plot->name,yun_plot->type,yun_plot->ax,yun_plot->dx,yun_plot->decade
      //	 ,yun_plot->mode);
      create_attach_select_x_un_to_op(op, yun_plot->type, yun_plot->ax, yun_plot->dx,
				      yun_plot->decade, yun_plot->mode, yun_plot->name);
      di = find_dialog_associated_to_pr(pr, NULL);
      if (di)
	{            di->proc(MSG_DRAW, di, 0);        }
      offset++;
    }
    else if ((action[item] == 8) && (xun_plot != NULL)) //((item == op->n_xu + offset + 1) && (xun_plot != NULL))
    {
      //win_printf("X unitset pasted %s\n"
      //		 "type %d ax %g dx %g decade %d mode %d\n"
      //	 ,xun_plot->name,xun_plot->type,xun_plot->ax,xun_plot->dx,xun_plot->decade
      //	 ,xun_plot->mode);
      create_attach_select_x_un_to_op(op, xun_plot->type, xun_plot->ax, xun_plot->dx,
				      xun_plot->decade, xun_plot->mode, xun_plot->name);
      di = find_dialog_associated_to_pr(pr, NULL);
      if (di)
	{            di->proc(MSG_DRAW, di, 0);        }
      offset++;
    }

    else if (action[item] == 9) //(item == op->n_xu + offset + 2)
    {
        op->c_xu_p = op->c_xu;
        return set_plt_x_unit_set(pr, op->c_xu);
    }
    else if (action[item] == 10) // (item == op->n_xu + offset + 3)
    {
        di = find_dialog_associated_to_pr(pr, NULL);
        if (di)
	  {            di->proc(MSG_DRAW, di, 0);        }
        else
        {            display_title_message("dialog not found");         }
        return D_O_K;
    }
    else
    {
        return D_O_K;
    }
    return D_O_K;
}

int build_and_display_x_axis_prime_menu(pltreg *pr, O_p *op)
{
    int i, j, item, mod_un = 0;
    char s[256] = {0};

    for (i = 0; i < 32; i++)
    {
        x_axis_label_menu[i].child = NULL;
        x_axis_label_menu[i].proc = NULL;
    }

    for (i = 0; i < op->n_xu && i < 24; i++)
    {
        snprintf(s, 256, "%s", (op->xu[i]->name != NULL) ? op->xu[i]->name : "no name");
        x_axis_label_menu[i].text = strdup(s);
        x_axis_label_menu[i].flags = (i == op->c_xu_p) ? D_SELECTED : 0;
    }

    x_axis_label_menu[i++].text = strdup("\0");
    x_axis_label_menu[i++].text = strdup("New Units");

    if (op->n_xu > 1 && op->c_xu > 0)
    {
        snprintf(s, 256, "remove %s", ((op->xu[op->c_xu]->name != NULL) &&
                                       (strlen(op->xu[op->c_xu]->name) > 0)) ?
                 op->xu[op->c_xu]->name : "no name");
        x_axis_label_menu[i++].text = strdup(s);
        snprintf(s, 256, "Modify Units %s", ((op->xu[op->c_xu]->name != NULL) &&
                                             (strlen(op->xu[op->c_xu]->name) > 0)) ?
                 op->xu[op->c_xu]->name : "no name");
        x_axis_label_menu[i++].text = strdup(s);
        snprintf(s, 256, "Change Decade %s", ((op->xu[op->c_xu]->name != NULL) &&
                                              (strlen(op->xu[op->c_xu]->name) > 0)) ?
                 op->xu[op->c_xu]->name : "no name");
        x_axis_label_menu[i++].text = strdup(s);
        mod_un = 1;
    }

    x_axis_label_menu[i++].text = strdup("\0");
    x_axis_label_menu[i++].text = strdup("unselect prime");
    x_axis_label_menu[i].text = strdup("Axis pt size");
    x_axis_label_menu[i].child = x_axis_point_style_size_menu;
    x_axis_label_menu[i].proc = NULL;
    x_axis_label_menu[i].flags = 0;
    x_axis_label_menu[i++].dp = NULL;
    x_axis_label_menu[i++].text = NULL;
    item = do_menu(x_axis_label_menu, mouse_x, mouse_y);

    for (j = 0; j < i; j++)
    {
        if (x_axis_label_menu[j].text != NULL)
        {
            free(x_axis_label_menu[j].text);
        }

        x_axis_label_menu[j].text = NULL;
    }

    if (item < 0)
    {
        return D_O_K;
    }

    mn_index = X_AXIS;

    if (item < op->n_xu)
    {
        op->c_xu_p = item;
        return set_plt_x_unit_set(pr, op->c_xu);
    }
    else if (item == op->n_xu + 1)
    {
        return add_new_units();
    }
    else if (item == op->n_xu + 2)
    {
        if (mod_un)
        {
            remove_one_plot_data(op, IS_X_UNIT_SET, (void *)op->xu[op->c_xu_p]);
            return set_plt_x_unit_set(pr, op->c_xu);
        }
    }
    else if (item == op->n_xu + 3)
    {
        if (mod_un)
        {
            return modify_units();
        }
        else
        {
            op->c_xu_p = -1;
            return set_plt_x_unit_set(pr, op->c_xu);
        }
    }
    else if (item == op->n_xu + 4)
    {
        if (mod_un)
        {
            return modify_un_decade();
        }
    }
    else if (item == op->n_xu + 6)
    {
        op->c_xu_p = -1;
        return set_plt_x_unit_set(pr, op->c_xu);
    }
    else
    {
        return D_O_K;
    }
    return D_O_K;
}


/*
   int    find_plot_box_from_screen(plyreg *pr, int px0, int py0)
   {
   int x = 0, u = 0, y, v, sc, h;
   struct box *ba;
   O_p *op;
   dybox *b;

   if (pr == NULL || pr->one_p == NULL)        return NULL;
   op = pr->one_p;
   sc = pr->screen_scale & ~AUTO_SCALE;
   h = cur_ac_reg->area.y1 - cur_ac_reg->area.y0;
   if ((ba=locate_box(pr->stack,type, &x, &u, &y, &v, mode)) != NULL)
   {


   }
   */

int    switch_to_memory_buffer(BITMAP *buf)
{
  //extern BITMAP *dev_bitmap;
    dev_bitmap = buf;
    y_offset = dev_bitmap->h;
    max_x = dev_bitmap->h;
    max_y = dev_bitmap->w;
    return 0;
}

int    mk_ds_mark(O_p *op, int n_ds, int x, int y)
{
    struct box b;
    struct box *li = NULL, *sym = NULL, *bc = NULL, *bd = NULL;
    d_s *ds = NULL;
    int ttf;
    BX_AR *xr = NULL, *yr = NULL;
    char ch[128] = { 0 };
    n_ds = (n_ds < 0) ? 0 : n_ds;
    ds = op->dat[n_ds % op->n_dat];
    b.parent = NULL;
    init_box(&b);
    b.color = ds->color;
    li = mkpolybox(&b, 3);

    if (li == NULL)
    {
        return D_O_K;
    }

    xr = (BX_AR *)li->child[0];
    yr = (BX_AR *)li->child[1];
    xr[0] = 0;
    yr[0] = 500;
    yr[2] = -500;

    if (ds->m == 1)
    {
        xr[1] |= 0x0001;
        xr[2] |= 0x0001;
    }

    if (ds->m == 0)
    {
        xr[1] |= 0x0001;
    }

    if (ds->symb != NULL)
    {
        sym = (struct box *)calloc(1, sizeof(struct box));

        if (sym == NULL)
        {
            return D_O_K;
        }

        sym->parent = &b;
        sym->n_hic = b.n_hic + 1;
        sym->pt = b.pt;
        sym->color = ds->color;
        sym->i_box = 0;
        sym->slx = b.slx;
        sym->sly = b.sly;
        string_to_box(ds->symb, sym);
        sym->xc = - sym->w / 2;
        sym->yc = (sym->d - sym->h)  / 2;
        li->child[2] = sym;
        set_box_size(sym, li);
    }

    ttf = ttf_enable;
    ttf_enable = 1;
    pc_fen = text_scale;
    set_box_size(li, &b);
    bc = mknewbox(&b);
    snprintf(ch, 128, "%d", n_ds);

    if ((bd = mknewbox(bc)) == NULL)
    {
        return D_O_K;
    }

    bc->n_box = TURN_90;
    bd->color = bc->color = b.color;
    string_to_box(ch, bd);
    bc->xc = (bd->h + bd->d);
    bc->yc = -bd->w / 2;

    if (sym != NULL)
    {
        bc->xc += sym->w / 2;
    }

    set_box_size(bd, bc);
    set_box_size(bc, &b);
    set_box_origin(&b, x, y, text_scale);
    display_box(&b, text_scale, White, TRUE_COLOR);
    free_box(&b);
    ttf_enable = ttf;
    return 0;
}


int display_picotex(char *mes, int color, int x, int y)
{
    struct box b;
    init_box(&b);
    b.color = color;
    string_to_box(mes, &b);
    set_box_origin(&b, x, y, text_scale);
    display_box(&b, text_scale, White, TRUE_COLOR);
    return 0;
}


int     draw_vert_marker(BITMAP *plt_bufferl, DIALOG *d, int pos, int mode)
{
    int color;
    color = makecol(255, 64, 64);
    line(plt_bufferl, d->x, d->y + d->h - pos + 8, d->x + 16, d->y + d->h - pos, color);
    line(plt_bufferl, d->x, d->y + d->h - pos - 8, d->x + 16, d->y + d->h - pos, color);

    if (mode)
    {
        line(plt_bufferl, d->x, d->y + d->h - pos, d->x + d->w, d->y + d->h - pos, color);
    }

    return 0;
}
int     draw_horz_marker(BITMAP *plt_bufferl, DIALOG *d, int pos, int mode)
{
    int color;
    color = makecol(255, 64, 64);
    line(plt_bufferl, d->x + pos + 8, d->y + d->h, d->x + pos, d->y + d->h - 16, color);
    line(plt_bufferl, d->x + pos - 8, d->y + d->h, d->x + pos, d->y + d->h - 16, color);

    if (mode)
    {
        line(plt_bufferl, d->x + pos, d->x, d->x + pos, d->x + d->w, color);
    }

    return 0;
}

int    blit_plot_reg(DIALOG *d)
{
# ifdef XV_WIN32
    scare_mouse();
#endif
    //# ifdef XV_WIN32
    //if (win_get_window() == GetForegroundWindow() && !plt_buffer_used)
    //  {
    //# endif
    //if (!screen_used)
    //{
    //  screen_used = 1;
    plt_buffer_used = 1;
    vsync();
    for(;screen_acquired;); // we wait for screen_acquired back to 0;
    screen_acquired = 1;
    acquire_bitmap(screen);
    blit(plt_buffer, screen, d->x, d->y, d->x, d->y, d->w, d->h);
    release_bitmap(screen);
    screen_acquired = 0;
    plt_buffer_used = 0;
    //  screen_used = 0;
    //}
    //# ifdef XV_WIN32
    //}
    //# endif
    switch_to_memory_buffer(screen);
# ifdef XV_WIN32
    unscare_mouse();
#endif
    return 0;
}

int    write_and_blit_box(struct box *b, int scale, BITMAP *plt_bufferl, DIALOG *d, pltreg *pr)
{
    int i, j;
    int x = 0, y = 0, w = 0, h = 0, u = 0, v = 0, xt = 0, mode, n_opd, n_dsd, i_opd, i_dsd;
    O_p *op = NULL;
    char buf[256] = {0};
    //    struct box *xt;

    if (pr == NULL || plt_buffer_used)
    {
        return 0;
    }

    if (pr->one_p->data_changing || pr->one_p->transfering_data_to_box)
    {
        return 0;
    }

    if (pr->stack_in_use ||     pr->blitting_stack)
    {
        return 0;
    }

    pr->blitting_stack = 1;
    op = pr->one_p;
# ifdef XV_WIN32
    scare_mouse();
#endif
    switch_to_memory_buffer(plt_bufferl);
    acquire_bitmap(plt_bufferl);
    plt_buffer_used = 1;
    set_clip_rect(plt_bufferl, d->x, d->y, d->x + d->w - 1, d->y + d->h - 1);
    clear_to_color(plt_bufferl, Black);
    release_bitmap(plt_bufferl);
# ifdef XV_WIN32
    unscare_mouse();
#endif

    if (op == NULL || pr->n_op <= 0)
    {
        display_title_message("No data");
        set_clip_rect(plt_bufferl, 0, 0, SCREEN_W - 1, SCREEN_H - 1);
        //if (!screen_used)
        //{
        //  screen_used = 1;
        //# ifdef XV_WIN32
        //if (win_get_window() == GetForegroundWindow())
        //{
        //# endif
        vsync();
# ifdef XV_WIN32
        scare_mouse();
#endif
	for(;screen_acquired;); // we wait for screen_acquired back to 0;
	screen_acquired = 1;
        acquire_bitmap(screen);
        blit(plt_bufferl, screen, d->x, d->y, d->x, d->y, d->w, d->h);
        release_bitmap(screen);
	screen_acquired = 0;
        switch_to_memory_buffer(screen);
        //  screen_used = 0;
        //}
        //# ifdef XV_WIN32
        //}
        //# endif
# ifdef XV_WIN32
        unscare_mouse();
#endif
        plt_buffer_used = 0;
        pr->blitting_stack = 0;
        return D_O_K;
    }

    if (user_font == NULL)
    {
        ttf_init();
    }

    n_dsd = (d->h - op_mark_h) / ds_mark_h;

    if (op->n_dat > n_dsd && n_dsd > 1)
    {
        n_dsd--;
    }

    i_dsd = n_dsd * (op->cur_dat / n_dsd);
    n_opd = (d->w - ds_mark_w) / op_mark_w;

    if (pr->n_op > n_opd && n_opd > 1)
    {
        n_opd--;
    }

    i_opd = n_opd * (pr->cur_op / n_opd);
    acquire_bitmap(plt_bufferl);

    /* we fill the data sets label vertically on the right */
    for (i = i_dsd, j = ds_mark_h / 2; i < op->n_dat && i < i_dsd + n_dsd && j < d->h - ds_mark_h; i++, j += ds_mark_h)
    {
        rectfill(plt_bufferl, d->x + d->w - ds_mark_w, d->y + d->h - j + 2 - ds_mark_h / 2, d->x + d->w - 1,
                 d->y + d->h - j - 2 + ds_mark_h / 2,
                 (i == pr->one_p->cur_dat) ? gray : Darkgray);
        mk_ds_mark(op, i, d->x + d->w - ds_mark_h / 2, SCREEN_H - d->y - d->h + j);
    }

    if (user_font != NULL)
    {
        alfont_set_font_size(user_font, normalsize_font_height);
    }

    if (op->n_dat > n_dsd)
    {
        j -= ds_mark_h / 3;
        rectfill(plt_bufferl, d->x + d->w - ds_mark_w, d->y + d->h - j + 1 - ds_mark_h / 6, d->x + d->w - 1,
                 d->y + d->h - j - 1 + ds_mark_h / 6,
                 Darkgray);
        alfont_textout(plt_bufferl, user_font, " -", d->x + d->w - ds_mark_w, d->y + d->h - j - 4, White);
        j += ds_mark_h / 3;
        rectfill(plt_bufferl, d->x + d->w - ds_mark_w, d->y + d->h - j + 1 - ds_mark_h / 6, d->x + d->w - 1,
                 d->y + d->h - j - 1 + ds_mark_h / 6,
                 Darkgray);

        if (user_font != NULL)
        {
            snprintf(buf, 256, " %d", op->cur_dat);
            alfont_textout(plt_bufferl, user_font, buf, d->x + d->w - ds_mark_w, d->y + d->h - j - 4, White);
        }

        j += ds_mark_h / 3;
        rectfill(plt_bufferl, d->x + d->w - ds_mark_w, d->y + d->h - j + 1 - ds_mark_h / 6, d->x + d->w - 1,
                 d->y + d->h - j - 1 + ds_mark_h / 6,
                 Darkgray);
        alfont_textout(plt_bufferl, user_font, " +", d->x + d->w - ds_mark_w, d->y + d->h - j - 4, White);
    }

    /* we fill the plots label horizontally on the top */
    for (i = i_opd, j = d->x; i < pr->n_op && i < i_opd + n_opd
            && j + op_mark_w < d->x + d->w - ds_mark_w; i++, j += op_mark_w)
    {
        rectfill(plt_bufferl, j - 4, d->y, j + 2, d->y + op_mark_h, Black);

        if (i == pr->cur_op)
        {
            rectfill(plt_bufferl, j + 2, d->y, j + op_mark_w - 4, d->y + op_mark_h, Lightgray);

            if (user_font != NULL && pr->o_p[i]->filename != NULL)
            {
                alfont_textout(plt_bufferl, user_font, pr->o_p[i]->filename, j + 4, d->y + 2, Darkgray);
            }
            else if (user_font != NULL)
            {
                snprintf(buf, 256, "Plot %d", i);
                alfont_textout(plt_bufferl, user_font, buf, j + 4, d->y + 2, Darkgray);
            }
        }
        else
        {
            rectfill(plt_bufferl, j + 2, d->y, j + op_mark_w - 4, d->y + op_mark_h, gray);

            if (user_font != NULL && pr->o_p[i]->filename != NULL)
            {
                alfont_textout(plt_bufferl, user_font, pr->o_p[i]->filename, j + 4, d->y + 2, White);
            }
            else if (user_font != NULL)
            {
                snprintf(buf, 256, "Plot %d", i);
                alfont_textout(plt_bufferl, user_font, buf, j + 4, d->y + 2, White);
            }
        }
    }

    if (pr->n_op > n_opd)
    {
        rectfill(plt_bufferl, j + 2, d->y, j + op_mark_w / 3 - 4, d->y + op_mark_h, gray);

        if (user_font != NULL)
        {
            alfont_textout(plt_bufferl, user_font, "   << ", j + 4, d->y + 2, White);
        }

        j += op_mark_w / 3 - 2;
        rectfill(plt_bufferl, j + 2, d->y, j + op_mark_w / 3 - 4, d->y + op_mark_h, gray);

        if (user_font != NULL)
        {
            snprintf(buf, 256, "Plot %d", pr->cur_op);
            alfont_textout(plt_bufferl, user_font, buf, j + 4, d->y + 2, White);
        }

        j += op_mark_w / 3 - 2;
        rectfill(plt_bufferl, j + 2, d->y, j + op_mark_w / 3 - 4, d->y + op_mark_h, gray);

        if (user_font != NULL)
        {
            alfont_textout(plt_bufferl, user_font, " >>   ", j + 4, d->y + 2, White);
        }
    }

    //    if (time_debug)  de_start = clock();
    if (b != pr->stack)
    {
        win_printf("Pb plot stack pointer!");
    }

    b->xc = d->x * scale / 2; // new dec 2004
    b->yc = ((screen->h - d->y - d->h) * scale) / 2;
    display_box(b, scale, White, TRUE_COLOR); // checking switching windows bug
    //if (time_debug)  win_printf("display box %g",(float)(clock()-de_start)/CLOCKS_PER_SEC);;
    mode = (abs(mouse_x - d->x - 8) < 8 && (mouse_b & 0x3) &&
            abs(d->y + d->h - pr->mark_v - mouse_y) < 8) ? 1 : 0;
    draw_vert_marker(plt_bufferl, d, pr->mark_v, mode);
    mode = (abs(mouse_x - d->x - pr->mark_h) < 8 && (mouse_b & 0x3) &&
            abs(d->y + d->h - mouse_y - 8) < 8) ? 1 : 0;
    draw_horz_marker(plt_bufferl, d, pr->mark_h, mode);
    set_clip_rect(plt_bufferl, 0, 0, SCREEN_W - 1, SCREEN_H - 1);
    release_bitmap(plt_bufferl);

    if (op->op_post_display != NULL)
    {
        op->op_post_display(op, d);
    }

    //# ifdef XV_WIN32
    //if (win_get_window() == GetForegroundWindow())
    //{
    //# endif
    //if (!screen_used)
    //{
    //  screen_used = 1;
    vsync();
# ifdef XV_WIN32
    scare_mouse();
#endif
    for(;screen_acquired;); // we wait for screen_acquired back to 0;
    screen_acquired = 1;
    acquire_bitmap(screen);
    blit(plt_bufferl, screen, d->x, d->y, d->x, d->y, d->w, d->h);
    release_bitmap(screen);
    screen_acquired = 0;
    //  screen_used = 0;
    //}
    //# ifdef XV_WIN32
    //}
    //# endif
    plt_buffer_used = 0;
    switch_to_memory_buffer(screen);
# ifdef XV_WIN32
    unscare_mouse();
#endif

    if (pr->plt != NULL)
    {
        x = pr->plt->xc;
        y = pr->plt->yc;
        w = pr->plt->w;
        h = pr->plt->h + pr->plt->d;
    }

    if (scale != 0)
    {
        /*        x = (2 * (x + pr->stack->xc));
                  y = (2 * (y + pr->stack->yc));
                  w = (x + (2 * w)) / scale;
                  h = (y + (2 * h)) / scale;
                  x /= scale;
                  y /= scale;*/
        /*
           if ((xt = locate_box(pr->stack,QKD_B, &x, &u, &y, &v, FIRST)) != NULL)
           {

        */
        if ((locate_box(pr->stack, QKD_B_XTK_B, &x, &u, &y, &v, FIRST)) != NULL)
        {
            w = (2 * u - 2 * x + scale / 2) / scale;
            x = ((scale / 2) + (2 * x)) / scale;
        }

        if ((locate_box(pr->stack, QKD_B_YTK_L, &xt, &u, &y, &v, FIRST)) != NULL)
       {
            h = (2 * v - 2 * y + scale / 2) / scale;
            y = (2 * y + (scale / 2)) / scale;;
        }
        else
        {
            x = (scale / 2) + 2 * (x + pr->stack->xc);
            y =  2 * (y + pr->stack->yc);
            w = (2 * w) + x;
            h = (2 * h) + y;
            w /= scale;
            h /= scale;
            x /= scale;
            y /= scale;
        }
    }

    //y =  SCREEN_H  - y - 1;
    //h =  SCREEN_H  - h - 1; /* was SCREEN_H */
    y =  d->h  - y;
    //win_printf("x %d y %d w %d h %d",x,y,w,h);
    //h =  d->h  - h - 1; /* was SCREEN_H */
    pr->x_off =  x;
    pr->y_off = y;
    pr->s_nx = w;
    pr->s_ny = h;
    pr->blitting_stack = 0;
    return 0;
}

int        move_label(struct box *xt, struct box *b, int sc, BITMAP *plt_bufferl,
                      DIALOG *d, pltreg *pr, int xm_s, int ym_s, int x, int y)
{
    int i, once, j;
    int  xts, yts, pl_action = 0;
    int dx, dxp, dy, dyp, i_label;
    O_p *op = NULL;
    p_l *pl = NULL, *pld = NULL, *plt = NULL;
    xts = xt->xc;
    yts = xt->yc;
    dx = dxp = dy = dyp = 0;
    op = pr->one_p;

    for (i = 0, i_label = -1; ((i < op->n_lab) && (i_label == -1)); i++)
    {
        i_label = (xt == (void *)op->lab[i]->b) ? i : -1;
    }

    if (i_label >= -0)
    {
        pl = op->lab[i_label];
    }
    else
    {
        for (i = 0; i < op->n_dat && pl == NULL; i++)
        {
            for (j = 0 ; j < op->dat[i]->n_lab && pl == NULL; j++)
            {
                if (xt == (void *)op->dat[i]->lab[j]->b)
                {
                    pl = op->dat[i]->lab[j];
                }
            }
        }
    }

    if (pl == NULL)
    {
        return D_O_K;
    }

    once = 1;

    while ((mouse_b & 0x3) && (pl_action == 0))
    {
        dx = mouse_x - xm_s;
        dy = mouse_y - ym_s;

        if (dx != dxp || dy != dyp)
        {
            xt->xc = xts + (dx * sc) / 2;
            xt->yc = yts - (dy * sc) / 2;
            write_and_blit_box(b, sc, plt_bufferl, d, pr);
            dxp = dx;
            dyp = dy;
        }

        if (key[KEY_C] && once)
        {
            pld = duplicate_plot_label(pl);

            if (pld != NULL)
            {
                add_plot_label_to_op(op, pld);
            }

            plt = pl;
            pl = pld;
            pld = plt;
            once = 0;
        }

        if (key[KEY_E])
        {
            pl_action = KEY_E;
        }

        if (key[KEY_DEL])
        {
            pl_action = KEY_DEL;
        }
    }

    clear_keybuf();

    if (pl_action == KEY_DEL)
    {
        i = win_printf("Do you really want to remove this plot label ?");

        if (i == WIN_CANCEL)
        {
            return D_REDRAWME;
        }

        remove_plot_label_from_op(op, pl);
        return D_REDRAWME;
    }

    if (pl_action == KEY_E)
    {
        TeX_modify(pl->text);

        if (last_answer == NULL || strlen(last_answer) == 0)
        {
            remove_plot_label_from_op(op, pl);
            return D_REDRAWME;
        }
        else
        {
            if (pl->text)
            {
                free(pl->text);
            }

            pl->text = strdup(last_answer);
        }
    }

    //pl->yla = y_pr_2_pltdata_raw(pr, (float)(xt->yc*2)/sc);
    //pl->xla = x_pr_2_pltdata_raw(pr, (float)(xt->xc*2)/sc);
    //y = y_pltdata_2_pr(pr, pl->yla);
    //pl->yla = y_pr_2_pltdata_raw(pr, y + dy);
    //y = ((y + xt->d)*2)/sc;
    //y -= dy;
    //pl->yla = y_pr_2_pltdata_raw(pr, y);
    y = ((xt->yc) * 2) / sc;
    pl->yla = y_pr_2_pltdata_raw(pr, d->y + pr->y_off - y);
    //y += dy;
    //dyp -= y_pr_2_pltdata_raw(pr, dy);
    //pl->yla += dyp;
    //win_printf("label y = %g",pl->yla);
    x = (x * 2) / sc;
    x += dx;
    pl->xla = x_pr_2_pltdata_raw(pr, x);
    op->need_to_refresh = 1;
    return D_REDRAWME;
}
int    edit_xlabel(O_p *op, int c_lab)
{
    int i, j, k;

    for (i = k = -1, j = 0 ; j < op->n_lab && i < c_lab; j++)
    {
        if (op->lab[j]->type == USR_COORD || op->lab[j]->type == ABS_COORD)
        {
            i++;
        }

        if (i == c_lab)
        {
            k = j;
        }
    }

    if (i == c_lab)
    {
        TeX_modify(op->lab[k]->text);

        if (last_answer != NULL)
        {
            if (op->lab[k]->text)
            {
                free(op->lab[k]->text);
            }

            op->lab[k]->text = strdup(last_answer);
        }
        else
        {
            remove_plot_label_from_op(op, op->lab[k]);
        }

        op->need_to_refresh = 1;
    }

    return D_REDRAWME;
}
int    edit_ylabel(O_p *op, int c_lab)
{
    int i, j, k;

    for (i = k = -1, j = 0 ; j < op->n_lab && i < c_lab; j++)
    {
        if (op->lab[j]->type == VERT_LABEL_USR
                || op->lab[j]->type == VERT_LABEL_ABS)
        {
            i++;
        }

        if (i == c_lab)
        {
            k = j;
        }
    }

    if (i == c_lab)
    {
        TeX_modify(op->lab[k]->text);

        if (last_answer != NULL)
        {
            if (op->lab[k]->text)
            {
                free(op->lab[k]->text);
            }

            op->lab[k]->text = strdup(last_answer);
        }
        else
        {
            remove_plot_label_from_op(op, op->lab[k]);
        }

        op->need_to_refresh = 1;
    }

    return D_REDRAWME;
}
int just_blit(DIALOG *d)
{
    //# ifdef XV_WIN32
    //if (win_get_window() == GetForegroundWindow())
    // {
    //# endif
    vsync();
    for(;screen_acquired;); // we wait for screen_acquired back to 0;
    screen_acquired = 1;
    acquire_bitmap(screen);
    blit(plt_buffer, screen, d->x, d->y, d->x, d->y, d->w, d->h);
    release_bitmap(screen);
    screen_acquired = 0;
    //# ifdef XV_WIN32
    //}
    //# endif
    return 0;
}

int d_draw_Op_proc(int msg, DIALOG *d, int c)
{
    int i;
    struct box *b = NULL, *xt = NULL;
    pltreg *pr = NULL;
    O_p *op = NULL, *opc = NULL, *opd = NULL;
    d_s *ds = NULL, *dds = NULL, *dsc = NULL;
    int sc, sc2, cl, color;
    int x = 0, u = 0, y, v, y_off = 0, xts, yts, ds_action = 0, op_action = 0;
    int xm_s, ym_s, dx = 0, dxp, dy = 0, dyp, n_lab;
    int xb, yb, xc_tmp, yc_tmp;
    int x1_s, y1_s, x2_s, y2_s;
    BITMAP *ds_bitmap = NULL;
    //extern BITMAP *dev_bitmap;
    //extern BITMAP *plt_buffer;
    un_s *unss = NULL, *unsd = NULL;
    DIALOG *di = NULL, *di_mn = NULL;
    char *units = NULL, *unitd = NULL;
    float dxs, axs, dxd, axd, dys, ays, dyd, ayd;
    float d_xs, d_xd, d_ys, d_yd, xm, ym;
    char buf[1024] = {0};
    int n_dsd, n_opd, i_dsd, i_opd, c_opd, c_dsd;
    int  nearest_in_x, nearest_in_y, nearest_ds, nearest_point;
    static int   idle_event = 0, last_idle_event = 1, idle_display = 0;
    //
    static int screen_w = 0;
# ifdef XV_WIN32
    static clock_t idle_time = 0;
# else
    struct timespec idle_tspec, cur_tspec;
    static double cur_t = 0, idle_t = 0;
# endif
    ASSERT(d);

    if (screen_w != SCREEN_W)
    {
        int scw = (SCREEN_W < 1) ? 640 : SCREEN_W;
        ds_mark_h = (int)(40 * sqrt((float)scw / 1280));
        ds_mark_w = (int)(25 * sqrt((float)scw / 1280));
        op_mark_w = (int)(120 * sqrt((float)scw / 1280));
        op_mark_h = normalsize_font_height + 2;//(int)(normalsize_font_height * sqrt((float)SCREEN_W / 1280));
        screen_w = SCREEN_W;
    }

    if (dev_bitmap != NULL)
    {
        y_off = dev_bitmap->h;
    }

    if (d->dp == NULL)
    {
        return D_O_K;
    }

    pr = (pltreg *)d->dp;       /* the plot region is here */
    op = pr->one_p;         /* the plot is here */
    if (op == NULL) return 0;
    pr->one_p->cur_dat = (pr->one_p->cur_dat < 0) ? 0 : pr->one_p->cur_dat;
    pr->one_p->cur_dat = (pr->one_p->cur_dat < pr->one_p->n_dat) ? pr->one_p->cur_dat : 0;

    if (pr->n_op <= 0)
    {
        write_and_blit_box(pr->stack, pr->screen_scale, plt_buffer, d, pr);
        return D_O_K;
    }

    sc = pr->screen_scale;
    b = pr->stack;
    n_dsd = (d->h - op_mark_h) / ds_mark_h;

    if (op->n_dat > n_dsd && n_dsd > 1)
    {
        n_dsd--;
    }

    i_dsd = n_dsd * (op->cur_dat / n_dsd);
    n_opd = (d->w - ds_mark_w) / op_mark_w;

    if (pr->n_op > n_opd && n_opd > 1)
    {
        n_opd--;
    }

    i_opd = n_opd * (pr->cur_op / n_opd);

    if (msg == MSG_DRAW)
    {
        //        win_printf("scale %d",sc);

        //ttf_enable = 0;
        if (pr->display_mode >= ALL_PLOTS)
        {
            prepare_all_plots(pr, pr->display_mode - ALL_PLOTS);

            if (win_title_used == 0)
            {
                if (pr->filename != NULL && pr->path != NULL)
                {
                    snprintf(buf, 1024, "%s %s", pr->filename, pr->path);
                }
                else if (pr->filename != NULL)
                {
                    snprintf(buf, 1024, "%s", pr->filename);
                }
                else
                {
                    snprintf(buf, 1024, "Untitled.cgr");
                }

                my_set_window_title("%s",buf);
            }
        }
        else
        {
            if (pr->stack == NULL)
            {
                pr->one_p = pr->o_p[pr->cur_op];
                pr->stack = &(pr->one_p->bplt);
            }

            if (pr->stack->n_box == 0 || op->need_to_refresh == 1)
            {
                if (op->data_changing)
                {
                    return D_O_K;
                }

                do_one_plot(pr);
            }

            if (win_title_used == 0)
            {
                if (pr->one_p->filename != NULL && pr->one_p->dir != NULL)
                {
                    snprintf(buf, 1024, "%s %s", pr->one_p->filename, pr->one_p->dir);
                }
                else if (pr->one_p->filename != NULL)
                {
                    snprintf(buf, 1024, "%s", pr->one_p->filename);
                }
                else
                {
                    snprintf(buf, 1024, "Untitled.gr");
                }

                //            win_printf("bef window title %s",buf);
                //            if (os_type == OSTYPE_WIN2000 || os_type == OSTYPE_WINXP)
                my_set_window_title("%s",buf);
                //            win_printf("after window title %s",buf);
            }
        }

        b = pr->stack;

        if (pr->auto_scale)
        {
            sc = (2 * (b->h + b->d)) / (d->h - 32); //(2 * (b->h + b->d))/((d->h-d->y)-32);
            sc2 = (2 * b->w) / (d->w - ds_mark_w);
            sc = (sc2 > sc) ? sc2 : sc;
            sc++;
            pr->screen_scale = sc;
            //display_title_message("scale %d",sc);
        }

        /*
           if (time_debug)
           {
           de_start = clock();
           ucl_box = ucl_poly = ucl_char = 0;
           seg_poly = 0;
           }
           */
        if (op->need_to_refresh || pr->display_mode >= ALL_PLOTS)
        {
            write_and_blit_box(b, sc, plt_buffer, d, pr);
        }
        else
        {
            just_blit(d);
        }

        op->need_to_refresh = 0;

        /*
           if (time_debug)
           {
           win_printf("blit %g\nbox %g char %g\n poly %g seg %g",(float)(clock()-de_start)/CLOCKS_PER_SEC,
           ((float)ucl_box)/get_my_uclocks_per_sec(),
           ((float)ucl_char)/get_my_uclocks_per_sec(),
           ((float)ucl_poly)/get_my_uclocks_per_sec(),seg_poly);
           }
           */
        if (the_dialog[1].dp == plot_menu)
        {
            remove_all_keyboard_proc();
            scan_and_update(plot_menu);
        }
    }
    else if (msg == MSG_DCLICK)
    {
        pr_last_selected = pr;
        di_mn = find_dialog_associated_to_menu(NULL, NULL);

        if (di_mn != NULL && di_mn->dp != plot_menu)
        {
            //if (!screen_used)
            //{
            //  screen_used = 1;
	  for(;screen_acquired;); // we wait for screen_acquired back to 0;
	  screen_acquired = 1;
            acquire_bitmap(screen);
            rectfill(screen, di_mn->x, di_mn->y, di_mn->x + di_mn->w - 1, di_mn->y + di_mn->h - 1, makecol(255, 255, 255));
            release_bitmap(screen);
	  screen_acquired = 0;
            //  screen_used = 0;
            //}
            di_mn->w = 0;
            di_mn->h = 0;
            di_mn->dp = plot_menu;
            xvin_d_menu_proc(MSG_START, di_mn, 0);
            xvin_d_menu_proc(MSG_DRAW, di_mn, 0);
        }

        if (mouse_x > d->x + d->w - ds_mark_w && mouse_x < d->x + d->w)
        {
            y = d->y + d->h - mouse_y;

            if (y < d->h)
            {
                i = y / ds_mark_h;
                i += i_dsd;

                if (i < i_dsd + n_dsd && i < pr->one_p->n_dat)
                {
                    if (i ==  pr->one_p->cur_dat)
                    {
                        // the 250 pixel shift avoid double click in the menu this depend on menu size!
                        return    do_ds_menu(do_menu(ds_menu, ((mouse_x - 20 - normalsize_font_width * 31 > 0) ?
                                                               mouse_x - 20 - normalsize_font_width * 31 : 5), mouse_y));
                    }
                }
            }
        }

        /*  strange bug ! to look closely
            if (mouse_y < d->y + op_mark_h && mouse_y > d->y)
            {
                i = (mouse_x - d->x) / op_mark_w;
                i += i_opd;

                if (i < i_opd + n_opd && i < pr->n_op)
                {
                    if (i ==  pr->cur_op)
                    {
        	  return    do_op_menu(do_menu(op_menu, ((mouse_x - normalsize_font_width * 31 > 0 ) ?
        						 mouse_x - normalsize_font_width * 31 : 5), mouse_y));
                    }
                }
            }
        */
        yb = y_off - mouse_y;
        yb *= sc;
        yb /= 2;
        xb = mouse_x;
        xb *= sc;
        xb /= 2;

        /*        win_printf("Pr stack at xc = %d yc %d\nmouse x = %d y = %d",
                  pr->stack->xc,pr->stack->yc,mouse_x,mouse_y);*/
        if ((locate_box(pr->stack, QKD_B_XTITL, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                TeX_modify(pr->one_p->x_title);
                set_plot_x_title(pr->one_p, "%s",last_answer);
                return D_REDRAWME;
            }

            /*            win_printf("xtitle at xc = %d yc %d\nw = %d h = %d",x,y,u,v);*/
        }

        if ((locate_box(pr->stack, QKD_B_YTITL, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                TeX_modify(pr->one_p->y_title);
                set_plot_y_title(pr->one_p, "%s", last_answer);
                return D_REDRAWME;
            }

            /*        win_printf("ytitle at xc = %d yc %d\nw = %d h = %d",x,y,u,v);*/
        }

        if ((locate_box(pr->stack, QKD_B_TITL, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                TeX_modify(pr->one_p->title);
                set_plot_title(pr->one_p, "%s", last_answer);
                return D_REDRAWME;
            }

            /*                win_printf("title at xc = %d yc %d\nw = %d h = %d",x,y,u,v);*/
        }

        if ((locate_box(pr->stack, QKD_B_XTK_B, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                do_plot_axis_menu(do_menu(plot_axis_menu_pop, mouse_x, mouse_y));
            }
        }

        if ((locate_box(pr->stack, QKD_B_XTK_T, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                do_plot_axis_menu(do_menu(plot_axis_menu_pop, mouse_x, mouse_y));
            }
        }

        if ((locate_box(pr->stack, QKD_B_YTK_R, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                do_plot_axis_menu(do_menu(plot_axis_menu_pop, mouse_x, mouse_y));
            }
        }

        if ((locate_box(pr->stack, QKD_B_YTK_L, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                do_plot_axis_menu(do_menu(plot_axis_menu_pop, mouse_x, mouse_y));
            }
        }

        if ((locate_box(pr->stack, QKD_B_XLABL, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                build_and_display_x_axis_menu(pr, pr->one_p);
            }

            if ((locate_box(pr->stack, QKD_B_XLABL, &x, &u, &y, &v, NEXT)) != NULL)
            {
                if (xb >= x && xb < u && yb >= y && yb < v)
                {
                    build_and_display_x_axis_prime_menu(pr, pr->one_p);
                }
            }
        }

        if ((locate_box(pr->stack, QKD_B_YLABL, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                build_and_display_y_axis_menu(pr, pr->one_p);
            }

            if ((locate_box(pr->stack, QKD_B_YLABL, &x, &u, &y, &v, NEXT)) != NULL)
            {
                if (xb >= x && xb < u && yb >= y && yb < v)
                {
                    build_and_display_y_axis_prime_menu(pr, pr->one_p);
                }
            }
        }

        n_lab = 0;

        if ((locate_box(pr->stack, QKD_B_XLABEL, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                return edit_xlabel(pr->one_p, n_lab);
            }

            /*                win_printf("QKD_B_XLABEL at xc = %d yc %d\nw = %d h = %d",x,y,u,v);*/

            while ((locate_box(pr->stack, QKD_B_XLABEL, &x, &u, &y, &v, NEXT)) != NULL)
            {
                n_lab++;

                if (xb >= x && xb < u && yb >= y && yb < v)
                {
                    return edit_xlabel(pr->one_p, n_lab);
                }

                /*                    win_printf("QKD_B_XLABEL at xc = %d yc %d\nw = %d h = %d",
                                      x,y,u,v);*/
            }
        }

        n_lab = 0;

        if ((locate_box(pr->stack, QKD_B_YLABEL, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                return edit_ylabel(pr->one_p, n_lab);
            }

            /*                win_printf("QKD_B_YLABEL at xc = %d yc %d\nw = %d h = %d",x,y,u,v);*/
            while ((locate_box(pr->stack, QKD_B_YLABEL, &x, &u, &y, &v, NEXT)) != NULL)
            {
                n_lab++;

                if (xb >= x && xb < u && yb >= y && yb < v)
                {
                    return edit_ylabel(pr->one_p, n_lab);
                }

                /*                    win_printf("QKD_B_YLABEL at xc = %d yc %d\nw = %d h = %d",
                                      x,y,u,v);*/
            }
        }

        if ((locate_box(pr->stack, QKD_B, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                last_data_mouse_x = mouse_x;
                last_data_mouse_y = mouse_y;
                do_plot_data_menu(do_menu(plot_data_menu_pop, mouse_x, mouse_y));
            }
        }
    }
    else if (msg == MSG_IDLE)
    {
        i = (op->op_idle_action != NULL) ? op->op_idle_action(op, d) : 0;
        if (i != 0)
        {
            return D_O_K;
        }

        //return 0; // test
        if (mouse_x < d->x || mouse_x >= d->x + d->w
                || mouse_y >= d->y + d->h || mouse_y < d->y)
        {
            return D_O_K;
        }

        if (mouse_x > d->x + d->w - ds_mark_w && mouse_x < d->x + d->w)
        {
            y = d->y + d->h - mouse_y;

            if (y < d->h)
            {
                i = y / ds_mark_h;
                i += i_dsd;

                if (i < i_dsd + n_dsd && i < pr->one_p->n_dat)
                {
                    idle_event = 0x1000000 + i;
                    //return    do_ds_menu(do_menu(ds_menu,mouse_x-200,mouse_y));
                }
            }
        }
        else if (mouse_y < d->y + op_mark_h && mouse_y > d->y)
        {
            i = (mouse_x - d->x) / op_mark_w;
            i += i_opd;

            if (i < i_opd + n_opd && i < pr->n_op)
            {
                idle_event = 0x2000000 + i;
                // return    do_op_menu(do_menu(op_menu,mouse_x,mouse_y));
            }
        }
        else if ((xt = locate_box(pr->stack, QKD_B, &x, &u, &y, &v, FIRST)) != NULL)
        {
            ym_s = mouse_y;
            yb = y_off - mouse_y;
            yb *= sc;
            yb /= 2;
            xm_s = xb = mouse_x;
            xb *= sc;
            xb /= 2;

            if ((xb + sc >= x && xb <= u + sc && yb >= y - sc && yb <= v + (2 * sc)))
            {
                idle_event = 0x4000000 + ((ym_s & 0xFFF) << 12) + (xm_s & 0xFFF);
            }
            else
            {
                idle_event = 0;
            }
        }
        else
        {
            idle_event = 0;
        }

        if (idle_event != last_idle_event)
        {

#ifdef XV_WIN32
            idle_time = clock();
#else
            clock_gettime(CLOCK_MONOTONIC, &idle_tspec);
            idle_t = (double)idle_tspec.tv_sec + 1e-9 * (double)idle_tspec.tv_nsec;
# endif
            last_idle_event = idle_event;

            if (idle_display) // was write_and_blit_box(b, sc, plt_buffer, d, pr);
            {
                just_blit(d);
            }

            idle_display = 0;
        }

        //my_set_window_title("plot idle op %d delta %d",(int)clock(),clock() - idle_time);
# ifdef XV_WIN32

if (((clock() - idle_time) << 2) >  CLOCKS_PER_SEC)
{

#else
clock_gettime(CLOCK_MONOTONIC, &cur_tspec);
cur_t = (double)cur_tspec.tv_sec + 1e-9 * (double)cur_tspec.tv_nsec;

if (cur_t - idle_t > 0.25)
{
    idle_t = cur_t;

# endif

            if (idle_event & 0x2000000)
            {
                if ((idle_display == 0) && ((idle_event & 0xFFFF) >= 0) && ((idle_event & 0xFFFF) < pr->n_op))
                {
                    just_blit(d);//write_and_blit_box(b, sc, plt_buffer, d, pr);

                    if (pr->o_p[idle_event & 0xFFFF]->filename != NULL)
                    {
                        strncpy(buf, pr->o_p[idle_event & 0xFFFF]->filename, 64);
                    }
                    else
                    {
                        strcpy(buf, "untitled");
                    }

                    buf[64] = 0;

                    if (strlen(buf) > 63)
                    {
                        strcat(buf, "...");
                    }

                    //if (!screen_used)
                    // {
                    //screen_used = 1;
                    get_clip_rect(screen, &x1_s, &y1_s, &x2_s, &y2_s);
                    set_clip_rect(screen, d->x, d->y, d->x + d->w, d->y + d->h);
                    draw_bubble(screen, B_CENTER, mouse_x, mouse_y + 1.5 * normalsize_font_height, "op = %d, %d ds %s",
                                idle_event & 0xFFFF, pr->one_p->n_dat, buf);

                    if (pr->o_p[idle_event & 0xFFFF]->dir != NULL)
                    {
                        strncpy(buf, pr->o_p[idle_event & 0xFFFF]->dir, 64);
                    }
                    else
                    {
                        strcpy(buf, "unknown");
                    }

                    buf[64] = 0;

                    if (strlen(buf) > 63)
                    {
                        strcat(buf, "...");
                    }

                    draw_bubble(screen, B_CENTER, mouse_x, mouse_y + 2.5 * normalsize_font_height, "%s", buf);
                    set_clip_rect(screen, x1_s, y1_s, x2_s, y2_s);
                    //screen_used = 0;
                    //}
                    //my_set_window_title("plot idle op %d",(int)clock());
                    idle_display = 1;
                }
            }
            else if (idle_event & 0x1000000)
            {
                if ((idle_display == 0) && ((idle_event & 0xFFFF) >= 0) && ((idle_event & 0xFFFF) < pr->one_p->n_dat))
                {
                    if (pr->one_p->dat[idle_event & 0xFFFF]->source != NULL)
                    {
                        strncpy(buf, pr->one_p->dat[idle_event & 0xFFFF]->source, 64);
                    }
                    else
                    {
                        strcpy(buf, "No source");
                    }

                    buf[64] = 0;

                    if (strlen(buf) > 63)
                    {
                        strcat(buf, "...");
                    }

                    just_blit(d);//write_and_blit_box(b, sc, plt_buffer, d, pr);
                    //if (!screen_used)
                    // {
                    //screen_used = 1;
                    get_clip_rect(screen, &x1_s, &y1_s, &x2_s, &y2_s);
                    set_clip_rect(screen, d->x, d->y, d->x + d->w, d->y + d->h);
                    draw_bubble(screen, B_RIGHT, mouse_x - 50, mouse_y - 0.5 * normalsize_font_height, "ds = %d %d points",
                                idle_event & 0xFFFF, pr->one_p->dat[idle_event & 0xFFFF]->nx);
                    draw_bubble(screen, B_RIGHT, mouse_x - 50, mouse_y + 0.5 * normalsize_font_height, "%s", buf);
                    set_clip_rect(screen, x1_s, y1_s, x2_s, y2_s);
                    //screen_used = 0;
                    //}
                    //my_set_window_title("plot idle ds %d",(int)clock());
                    idle_display = 1;
                }
            }
            else if (idle_event & 0x4000000)
            {
                if (idle_display == 0)
                {
                    //ds = pr->one_p->dat[pr->one_p->cur_dat];
                    //if (ds->nx > 16384) return 0;   // we skip long file
                    just_blit(d);//write_and_blit_box(b, sc, plt_buffer, d, pr);
                    //if (!screen_used)
                    //  {
                    //screen_used = 1;
                    get_clip_rect(screen, &x1_s, &y1_s, &x2_s, &y2_s);
                    set_clip_rect(screen, d->x, d->y, d->x + d->w, d->y + d->h);
                    xm = x_pr_2_pltdata(pr, (idle_event & 0xFFF));
                    ym = y_pr_2_pltdata(pr, ((idle_event & 0xFFF000) >> 12));
                    pr->one_p->cur_dat = (pr->one_p->cur_dat < 0) ? 0 : pr->one_p->cur_dat;
                    pr->one_p->cur_dat = (pr->one_p->cur_dat < pr->one_p->n_dat) ? pr->one_p->cur_dat : 0;
                    ds = pr->one_p->dat[pr->one_p->cur_dat];
                    op = pr->one_p;
                    xm = (op->dx != 0) ? (xm - op->ax) / op->dx : xm - op->ax;
                    ym = (op->dy != 0) ? (ym - op->ay) / op->dy : ym - op->ay;

                    if (find_nearest_points_in_in_ds(ds, xm, ym, &nearest_in_x, &nearest_in_y) == 0)
                    {
                        draw_bubble(screen, B_LEFT, 10, d->y + 2 * normalsize_font_height, "X index %d, (%g, %g)",
                                    nearest_in_x, op->ax + op->dx * ds->xd[nearest_in_x],
                                    op->ay + op->dy * ds->yd[nearest_in_x]);
                        draw_bubble(screen, B_RIGHT, d->x + d->w - 100, d->y + 2 * normalsize_font_height, "Y index %d, (%g, %g)",
                                    nearest_in_y, op->ax + op->dx * ds->xd[nearest_in_y],
                                    op->ay + op->dy * ds->yd[nearest_in_y]);
                        xm = x_pr_2_pltdata(pr, (idle_event & 0xFFF));
                        ym = y_pr_2_pltdata(pr, ((idle_event & 0xFFF000) >> 12));

                        if ((find_nearest_points_in_plot(op, xm, ym, &nearest_ds, &nearest_point, 1) == 0)
                                && (nearest_ds >= 0) && (nearest_ds < op->n_dat))
                        {
			  for(;screen_acquired;); // we wait for screen_acquired back to 0;
			  screen_acquired = 1;

                            acquire_bitmap(screen);
                            circle(screen, x_pltdata_2_pr(pr, op->dat[nearest_ds]->xd[nearest_point])
                                   , d->y + y_pltdata_2_pr(pr, op->dat[nearest_ds]->yd[nearest_point]), 4, Yellow);
                            release_bitmap(screen);
			    screen_acquired = 0;
                            int bubble_ypos = - 0.5;

                            if (mouse_x < d->x + (2 * d->w) / 3)
                            {
                                draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y + bubble_ypos * normalsize_font_height, "ds %d; i %d", nearest_ds,
                                            nearest_point);
                                bubble_ypos += 1;
                                draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y + bubble_ypos  * normalsize_font_height, "x %g"
                                            , op->ax + op->dx * op->dat[nearest_ds]->xd[nearest_point]);

                                if (op->dat[nearest_ds]->xbu)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y + bubble_ypos * normalsize_font_height, "x sigma %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->xbu[nearest_point]);
                                }

                                if (op->dat[nearest_ds]->xbd)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y + bubble_ypos * normalsize_font_height, "x sigma (down) %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->xbd[nearest_point]);
                                }

                                if (op->dat[nearest_ds]->xe)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y + bubble_ypos * normalsize_font_height, "x error %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->xe[nearest_point]);
                                }

                                if (op->dat[nearest_ds]->xed)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y + bubble_ypos * normalsize_font_height, "x error (down) %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->xed[nearest_point]);
                                }
                                bubble_ypos += 1;
                                draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y + bubble_ypos * normalsize_font_height, "y %g"
                                            , op->ay + op->dy * op->dat[nearest_ds]->yd[nearest_point]);

                                if (op->dat[nearest_ds]->ybu)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y + bubble_ypos * normalsize_font_height, "y sigma %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->ybu[nearest_point]);
                                }

                                if (op->dat[nearest_ds]->ybd)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y + bubble_ypos * normalsize_font_height, "y sigma (down) %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->ybd[nearest_point]);
                                }

                                if (op->dat[nearest_ds]->ye)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y + bubble_ypos * normalsize_font_height, "y error %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->ye[nearest_point]);
                                }

                                if (op->dat[nearest_ds]->yed)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y + bubble_ypos * normalsize_font_height, "y error (down) %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->yed[nearest_point]);
                                }
                                if (op->op_idle_point_add_display)

                                    draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y + 45,
                                                "%s",op->op_idle_point_add_display(op, d, nearest_ds, nearest_point));
                            }
                            else
                            {
                                draw_bubble(screen, B_RIGHT, mouse_x + 10, mouse_y + bubble_ypos * normalsize_font_height, "ds %d; i %d", nearest_ds,
                                            nearest_point);
                                bubble_ypos += 1;
                                draw_bubble(screen, B_RIGHT, mouse_x - 10, mouse_y + bubble_ypos * normalsize_font_height, "x %g"
                                            , op->ax + op->dx * op->dat[nearest_ds]->xd[nearest_point]);

                                if (op->dat[nearest_ds]->xe)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_RIGHT, mouse_x - 10, mouse_y + bubble_ypos * normalsize_font_height, "x error %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->xe[nearest_point]);
                                }

                                if (op->dat[nearest_ds]->xed)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_RIGHT, mouse_x - 10, mouse_y + bubble_ypos * normalsize_font_height, "x error (down) %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->xed[nearest_point]);
                                }

                                if (op->dat[nearest_ds]->xbu)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_RIGHT, mouse_x - 10, mouse_y + bubble_ypos * normalsize_font_height, "x sigma %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->xbu[nearest_point]);
                                }

                                if (op->dat[nearest_ds]->xbd)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_RIGHT, mouse_x - 10, mouse_y + bubble_ypos * normalsize_font_height, "x sigma (down) %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->xbd[nearest_point]);
                                }
				bubble_ypos += 1;
                                draw_bubble(screen, B_RIGHT, mouse_x - 10, mouse_y + bubble_ypos * normalsize_font_height, "y %g"
                                            , op->ay + op->dy * op->dat[nearest_ds]->yd[nearest_point]);

                                if (op->dat[nearest_ds]->ye)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_RIGHT, mouse_x - 10, mouse_y + bubble_ypos * normalsize_font_height, "y error %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->ye[nearest_point]);
                                }

                                if (op->dat[nearest_ds]->yed)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_RIGHT, mouse_x - 10, mouse_y + bubble_ypos * normalsize_font_height, "y error (down) %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->yed[nearest_point]);
                                }

                                if (op->dat[nearest_ds]->ybu)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_RIGHT, mouse_x - 10, mouse_y + bubble_ypos * normalsize_font_height, "y sigma %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->ybu[nearest_point]);
                                }

                                if (op->dat[nearest_ds]->ybd)
                                {
                                    bubble_ypos += 1;
                                    draw_bubble(screen, B_RIGHT, mouse_x - 10, mouse_y + bubble_ypos * normalsize_font_height, "y sigma (down) %g"
                                                , op->ay + op->dy * op->dat[nearest_ds]->ybd[nearest_point]);
                                }

                                if (op->op_idle_point_add_display)
                                    draw_bubble(screen, B_RIGHT, mouse_x - 10, mouse_y + 45,
                                                "%s",op->op_idle_point_add_display(op, d, nearest_ds, nearest_point));
                            }
                        }
                        else
                        {
                            if (mouse_x < d->x + (2 * d->w) / 3)
                            {
                                //draw_bubble(screen, B_LEFT, mouse_x+10, mouse_y-30, "%d %d",
                                //    (idle_event&0xFFF),(idle_event&0xFFF000)>>12);
                                draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y - 10, "x %g", xm);
                                draw_bubble(screen, B_LEFT, mouse_x + 10, mouse_y + 10, "y %g", ym);
                            }
                            else
                            {
                                draw_bubble(screen, B_RIGHT, mouse_x - 10, mouse_y - 10, "x %g", xm);
                                draw_bubble(screen, B_RIGHT, mouse_x - 10, mouse_y + 10, "y %g", ym);
                            }
                        }
                    }

                    set_clip_rect(screen, x1_s, y1_s, x2_s, y2_s);
                    //screen_used = 0;
                    //}
                    //my_set_window_title("plot idle in %d",(int)clock());
                    idle_display = 1;
                    //return D_REDRAWME;
                }
            }
        }
    }
    else if (msg == MSG_END)
    {
        if (op->op_end_action != NULL)
        {
            op->op_end_action(op, d);
        }

        if (pr->plot_end_action != NULL)
        {
            pr->plot_end_action(pr, d);
        }

        return D_O_K;
    }
    else if (msg == MSG_CLICK)
    {
        pr_last_selected = pr;
        di_mn = find_dialog_associated_to_menu(NULL, NULL);

        if (di_mn != NULL && di_mn->dp != plot_menu)
        {
            //if (!screen_used)
            //{
            //  screen_used = 1;
	  for(;screen_acquired;); // we wait for screen_acquired back to 0;
	  screen_acquired = 1;

            acquire_bitmap(screen);
            rectfill(screen, di_mn->x, di_mn->y, di_mn->x + di_mn->w - 1, di_mn->y + di_mn->h - 1, makecol(255, 255, 255));
            release_bitmap(screen);
	  screen_acquired = 0;
            //  screen_used = 0;
            //}
            di_mn->w = 0;
            di_mn->h = 0;
            di_mn->dp = plot_menu;
            xvin_d_menu_proc(MSG_START, di_mn, 0);
            xvin_d_menu_proc(MSG_DRAW, di_mn, 0);
        }

        ym_s = mouse_y;
        xm_s = mouse_x;

        if (abs(mouse_x - d->x - 8) < 8 &&
                abs(d->y + d->h - pr->mark_v - mouse_y) < 8)
        {
            ym = y_pr_2_pltdata_raw(pr, mouse_y); //-d->y);

            while (mouse_b & 0x3)
            {
                if (pr->mark_v != d->y + d->h - mouse_y)
                {
                    pr->mark_v = d->y + d->h - mouse_y;
                    ym = y_pr_2_pltdata_raw(pr, mouse_y); //-d->y);
                    write_and_blit_box(b, sc, plt_buffer, d, pr);

                    if (vert_marker_plt_drag_action != NULL)
                    {
                        vert_marker_plt_drag_action(pr, pr->one_p, ym, d);

                        if (pr->one_p->need_to_refresh)
                        {
                            do_one_plot(pr);
                            pr->one_p->need_to_refresh = 0;
                        }
                    }
                }
            }

            if (vert_marker_plt_action != NULL)
            {
                vert_marker_plt_action(pr, pr->one_p, ym, d);

                if (pr->one_p->need_to_refresh)
                {
                    do_one_plot(pr);
                    pr->one_p->need_to_refresh = 0;
                }
            }

            write_and_blit_box(b, sc, plt_buffer, d, pr);
        }

        if (abs(mouse_x - d->x - pr->mark_h) < 8 &&
                abs(d->y + d->h - mouse_y - 8) < 8)
        {
            xm = x_pr_2_pltdata_raw(pr, mouse_x);//-d->x);

            while (mouse_b & 0x3)
            {
                if (pr->mark_h != mouse_x - d->x)
                {
                    pr->mark_h = mouse_x - d->x;
                    xm = x_pr_2_pltdata_raw(pr, mouse_x); //-d->x);
                    write_and_blit_box(b, sc, plt_buffer, d, pr);

                    if (horz_marker_plt_drag_action != NULL)
                    {
                        horz_marker_plt_drag_action(pr, pr->one_p, xm, d);

                        if (pr->one_p->need_to_refresh)
                        {
                            do_one_plot(pr);
                            pr->one_p->need_to_refresh = 0;
                        }
                    }
                }
            }

            if (horz_marker_plt_action != NULL)
            {
                horz_marker_plt_action(pr, pr->one_p, xm, d);

                if (pr->one_p->need_to_refresh)
                {
                    do_one_plot(pr);
                    pr->one_p->need_to_refresh = 0;
                }
            }

            write_and_blit_box(b, sc, plt_buffer, d, pr);
        }

        if (mouse_y < d->y + op_mark_h && mouse_y >= d->y &&
                mouse_x >= d->x && mouse_x < d->x + d->w)
        {
            i = (mouse_x - d->x) / op_mark_w;
            i += i_opd;

            if (i < i_opd + n_opd && i < pr->n_op)
            {
                if (i != pr->cur_op)
                {
                    pr->display_mode = IS_ONE_PLOT;
                    pr->cur_op = (i < 0) ? 0 : i;
                    pr->one_p = pr->o_p[pr->cur_op];
                    pr->stack = &(pr->one_p->bplt);
                    pr->one_p->need_to_refresh = 1;
                    return D_REDRAWME;
                    // broadcast_dialog_message(MSG_DRAW,0);
                }
            }
            else if (pr->n_op > n_opd)
            {
                i = pr->n_op - i_opd;
                i = (i < n_opd) ? i : n_opd;
                i = (mouse_x - d->x) - (i * op_mark_w);
                i = (3 * i) / op_mark_w;

                if (i == 1)
                {
                    c_opd = pr->cur_op;
                    snprintf(buf, 1024, "Present selected plot nb. is %d -> file %s\nSelect the active plot nb. %%d",
                             c_opd, (pr->o_p[c_opd]->filename) ? pr->o_p[c_opd]->filename : "untilted.gr");
                    i = win_scanf(buf, &c_opd);

                    if (i == WIN_CANCEL || c_opd >= pr->n_op || c_opd < 0)
                    {
                        return D_O_K;
                    }

                    pr->cur_op = c_opd;
                    pr->display_mode = IS_ONE_PLOT;
                    pr->one_p = pr->o_p[pr->cur_op];
                    pr->stack = &(pr->one_p->bplt);
                    pr->one_p->need_to_refresh = 1;
                    return D_REDRAWME;
                }
                else if (i == 0)
                {
                    pr->cur_op = (i_opd > 0) ? i_opd - 1 : i_opd;
                    pr->display_mode = IS_ONE_PLOT;
                    pr->one_p = pr->o_p[pr->cur_op];
                    pr->stack = &(pr->one_p->bplt);
                    pr->one_p->need_to_refresh = 1;
                    return D_REDRAWME;
                }
                else if (i == 2)
                {
                    pr->cur_op = i_opd + n_opd;

                    if (pr->cur_op >= pr->n_op)
                    {
                        pr->cur_op = pr->n_op - 1;
                    }

                    pr->display_mode = IS_ONE_PLOT;
                    pr->one_p = pr->o_p[pr->cur_op];
                    pr->stack = &(pr->one_p->bplt);
                    pr->one_p->need_to_refresh = 1;
                    return D_REDRAWME;
                }
            }

            dxp = dyp = 0;

            while (mouse_b & 0x3 && op_action == 0)
            {
                dy = mouse_y - ym_s;
                dx = mouse_y - xm_s;
                opc = pr->one_p;
                pr->one_p->cur_dat = (pr->one_p->cur_dat < 0) ? 0 : pr->one_p->cur_dat;
                pr->one_p->cur_dat = (pr->one_p->cur_dat < pr->one_p->n_dat) ? pr->one_p->cur_dat : 0;
                dsc = pr->one_p->dat[pr->one_p->cur_dat];

                if (ds_bitmap == NULL)
                {
                    ds_bitmap = create_bitmap(128, 92);
                    cl = Lightmagenta;
                    color = makecol(EXTRACT_R(cl), EXTRACT_G(cl), EXTRACT_B(cl));
                    clear_to_color(ds_bitmap, color);
                    u = pr->stack->w;
                    v = pr->stack->h + pr->stack->d;
                    sc = 2 * u / 126;
                    sc = (2 * v / 90 > sc) ? 2 * v / 90 : sc;
                    switch_to_memory_buffer(ds_bitmap);
                    xc_tmp = pr->stack->xc;
                    yc_tmp = pr->stack->yc;
                    pr->stack->xc = 0;
                    pr->stack->yc = 0;  // stack is centered in screen
                    display_box(pr->stack, sc, White, TRUE_COLOR);
                    pr->stack->xc = xc_tmp;
                    pr->stack->yc = yc_tmp;
                    //if (!screen_used)
                    //{
                    //screen_used = 1;
                    switch_to_memory_buffer(screen);
# ifdef XV_WIN32
                    scare_mouse();
#endif
		    for(;screen_acquired;); // we wait for screen_acquired back to 0;
		    screen_acquired = 1;

                    acquire_bitmap(screen);
                    set_mouse_sprite(ds_bitmap);
                    release_bitmap(screen);
		    screen_acquired = 0;

# ifdef XV_WIN32
                    unscare_mouse();
#endif
                    //screen_used = 0;
                    //}
                }

                if (key[KEY_C])
                {
                    op_action = KEY_C;
                }

                if (key[KEY_DEL])
                {
                    op_action = KEY_DEL;
                }
            }

            clear_keybuf();
# ifdef XV_WIN32
            scare_mouse();
#endif
            set_mouse_sprite(NULL);
# ifdef XV_WIN32
            unscare_mouse();
#endif

            if (ds_bitmap != NULL)
            {
                destroy_bitmap(ds_bitmap);
                ds_bitmap = NULL;
            }

            if (op_action == KEY_DEL)
            {
                op_menu_kill_op();
                di = find_dialog_associated_to_pr(pr, NULL);

                if (di)
                {
                    di->proc(MSG_DRAW, di, 0);
                }
                else
                {
                    broadcast_dialog_message(MSG_DRAW, 0);
                }

                return D_O_K;
            }

            if (abs(dx) > 5 || abs(dy) > 5 || op_action == KEY_C)
            {
                if (mouse_y > d->y + op_mark_h && mouse_y < d->y + d->h &&
                        mouse_x > d->x && mouse_x < d->x + d->w)
                {
                    op = duplicate_plot_data(pr->one_p, NULL);

                    if (op == NULL)
                    {
                        return D_O_K;
                    }

                    add_data(pr, IS_ONE_PLOT, (void *)op);
                    pr->cur_op = pr->n_op - 1;
                    pr->one_p = pr->o_p[pr->cur_op];
                    pr->stack = &(pr->one_p->bplt);
                    pr->one_p->need_to_refresh = 1;

                    if (win_title_used == 0)
                    {
                        if (pr->one_p->filename != NULL && pr->one_p->dir != NULL)
                        {
                            snprintf(buf, 1024, "%s %s", pr->one_p->filename, pr->one_p->dir);
                        }
                        else if (pr->one_p->filename != NULL)
                        {
                            snprintf(buf, 1024, "%s", pr->one_p->filename);
                        }
                        else
                        {
                            snprintf(buf, 1024, "Untitled.gr");
                        }

                        //                    if (os_type == OSTYPE_WIN2000 || os_type == OSTYPE_WINXP)
                        my_set_window_title("%s", buf);
                        return D_REDRAWME;
                    }
                }
            }
        }

        if (mouse_x > d->x + d->w - ds_mark_w && mouse_x < d->x + d->w &&
                mouse_y < d->y + d->h && mouse_y >= d->y)
            // ds selection
        {
            y = d->y + d->h - mouse_y;

            if (y < d->h)
            {
                i = y / ds_mark_h;
                i += i_dsd;

                if (i < i_dsd + n_dsd && i < pr->one_p->n_dat)
                {
                    if (i != pr->one_p->cur_dat)
                    {
                        op->cur_dat = (op->cur_dat < 0) ? 0 : op->cur_dat;
                        op->cur_dat = (op->cur_dat < op->n_dat) ? op->cur_dat : 0;
                        pr->one_p->cur_dat = (i < 0) ? 0 : i;
                        pr->one_p->need_to_refresh = 1;
                        return D_REDRAWME;
                    }
                }
                else
                {
                    i = pr->one_p->n_dat - i_dsd;
                    i = (i < n_dsd) ? i : n_dsd;
                    i = y - (i * ds_mark_h);
                    i = (3 * i) / ds_mark_h;

                    if (i == 1)
                    {
                        op->cur_dat = (op->cur_dat < 0) ? 0 : op->cur_dat;
                        op->cur_dat = (op->cur_dat < op->n_dat) ? op->cur_dat : 0;
                        c_dsd = pr->one_p->cur_dat;
                        snprintf(buf, 1024, "Present selected data set nb. is %d in %d\nSelect the active data set nb. %%d",
                                 c_dsd, pr->one_p->n_dat);
                        i = win_scanf(buf, &c_dsd);

                        if (i == WIN_CANCEL || c_dsd >= pr->one_p->n_dat || c_dsd < 0)
                        {
                            return D_O_K;
                        }

                        pr->one_p->cur_dat = c_dsd;
                        pr->one_p->need_to_refresh = 1;
                        return D_REDRAWME;
                    }
                    else if (i == 0)
                    {
                        pr->one_p->cur_dat = (i_dsd > 0) ? i_dsd - 1 : i_dsd;
                        pr->one_p->need_to_refresh = 1;
                        return D_REDRAWME;
                    }
                    else if (i == 2)
                    {
                        pr->one_p->cur_dat = i_dsd + n_dsd;

                        if (pr->one_p->cur_dat >= pr->one_p->n_dat)
                        {
                            pr->one_p->cur_dat = pr->one_p->n_dat - 1;
                        }

                        pr->one_p->need_to_refresh = 1;
                        return D_REDRAWME;
                    }
                }

                dxp = dyp = 0;

                while (mouse_b & 0x3 && ds_action == 0)
                {
                    dy = mouse_y - ym_s;
                    dx = mouse_y - xm_s;

                    if (ds_bitmap == NULL)
                    {
                        op->cur_dat = (op->cur_dat < 0) ? 0 : op->cur_dat;
                        op->cur_dat = (op->cur_dat < op->n_dat) ? op->cur_dat : 0;
                        ds = pr->one_p->dat[pr->one_p->cur_dat];
                        opc = pr->one_p;
                        dsc = pr->one_p->dat[pr->one_p->cur_dat];
                        ds_bitmap = create_bitmap(80, 60);
                        cl = Lightmagenta;
                        color = makecol(EXTRACT_R(cl), EXTRACT_G(cl), EXTRACT_B(cl));
                        clear_to_color(ds_bitmap, color);

                        if (locate_box(pr->stack, QKD_B, &x, &u, &y, &v, FIRST) != NULL)
                        {
                            u -= x - 100;
                            v -= y - 200;
                            sc = 2 * u / 75;
                            sc = (2 * v / 55 > sc) ? 2 * v / 55 : sc;
                            switch_to_memory_buffer(ds_bitmap);
                            xt = locate_box(pr->stack, QKD_B_XTK_B, &x, &u, &y, &v, FIRST);

                            if (xt != NULL)
                            {
                                xt->xc += 100;
                                xt->yc += 200;
                                display_box(xt, sc, White, TRUE_COLOR);
                                xt->xc -= 100;
                                xt->yc -= 200;
                            }

                            xt = locate_box(pr->stack, QKD_B_YTK_L, &x, &u, &y, &v, FIRST);

                            if (xt != NULL)
                            {
                                xt->xc += 100;
                                xt->yc += 200;
                                display_box(xt, sc, White, TRUE_COLOR);
                                xt->xc -= 100;
                                xt->yc -= 200;
                            }

                            xt = locate_box(pr->stack, QKD_B_PLOT, &x, &u, &y, &v, FIRST);

                            for (i = -1; i <= pr->one_p->cur_dat && xt != NULL && i < pr->one_p->n_dat; i++)
                            {
                                xt = locate_box(pr->stack, QKD_B_PLOT, &x, &u, &y, &v, NEXT);

                                if (pr->one_p->dat[(i + 1) % pr->one_p->n_dat]->xe != NULL)
                                {
                                    locate_box(pr->stack, QKD_B_PLOT, &x, &u, &y, &v, NEXT);
                                }

                                if (pr->one_p->dat[(i + 1) % pr->one_p->n_dat]->ye != NULL)
                                {
                                    locate_box(pr->stack, QKD_B_PLOT, &x, &u, &y, &v, NEXT);
                                }
                            }

                            if (xt != NULL)
                            {
                                xt->xc += 100;
                                xt->yc += 200;

                                if (ds->color != Lightmagenta)
                                {
                                    display_box(xt, sc, White, TRUE_COLOR);
                                }
                                else
                                {
                                    xt->color -= 1;
                                    display_box(xt, sc, White, TRUE_COLOR);
                                    xt->color += 1;
                                }

                                xt->xc -= 100;
                                xt->yc -= 200;
                            }

                            color = makecol(255, 255, 255);
                            acquire_bitmap(ds_bitmap);
                            line(ds_bitmap, 79, 2, 79, 12, color);
                            line(ds_bitmap, 79, 2, 71, 8, color);
                            line(ds_bitmap, 79, 12, 77, 10, color);
                            line(ds_bitmap, 71, 8, 73, 9, color);
                            line(ds_bitmap, 70, 15, 73, 9, color);
                            line(ds_bitmap, 74, 15, 77, 10, color);
                            line(ds_bitmap, 74, 15, 70, 15, color);
                            release_bitmap(ds_bitmap);
                            switch_to_memory_buffer(screen);
                        }

                        //if (!screen_used)
                        //{
                        //  screen_used = 1;
# ifdef XV_WIN32
                        scare_mouse();
#endif
                        set_mouse_sprite_focus(80, 0);
                        set_mouse_sprite(ds_bitmap);
# ifdef XV_WIN32
                        unscare_mouse();
#endif
                        //  screen_used = 0;
                        //}
                    }

                    if (mouse_y < d->y + op_mark_h && mouse_y > d->y &&
                            mouse_x < d->x + d->w && mouse_x >= d->x)
                    {
                        n_dsd = (d->h - op_mark_h) / ds_mark_h;

                        if (op->n_dat > n_dsd && n_dsd > 1)
                        {
                            n_dsd--;
                        }

                        i_dsd = n_dsd * (op->cur_dat / n_dsd);
                        n_opd = (d->w - ds_mark_w) / op_mark_w;

                        if (pr->n_op > n_opd && n_opd > 1)
                        {
                            n_opd--;
                        }

                        i_opd = n_opd * (pr->cur_op / n_opd);
                        i = (mouse_x - d->x) / op_mark_w;
                        i += i_opd;

                        if (i < i_opd + n_opd && i < pr->n_op)
                        {
                            if (i != pr->cur_op)
                            {
                                pr->display_mode = IS_ONE_PLOT;
                                pr->cur_op = (i < 0) ? 0 : i;
                                pr->one_p = pr->o_p[pr->cur_op];
                                pr->stack = &(pr->one_p->bplt);
                                pr->one_p->need_to_refresh = 1;
                                d_draw_Op_proc(MSG_DRAW, d, c);
                                display_title_message("plot %d", pr->cur_op);
                            }
                        }
                        else if (pr->n_op > n_opd)
                        {
                            i = pr->n_op - i_opd;
                            i = (i < n_opd) ? i : n_opd;
                            i = (mouse_x - d->x) - (i * op_mark_w);
                            i = (3 * i) / op_mark_w;

                            if (i == 0)
                            {
                                pr->cur_op = (i_opd > 0) ? i_opd - 1 : i_opd;
                                pr->display_mode = IS_ONE_PLOT;
                                pr->one_p = pr->o_p[pr->cur_op];
                                pr->stack = &(pr->one_p->bplt);
                                pr->one_p->need_to_refresh = 1;
                                d_draw_Op_proc(MSG_DRAW, d, c);
                                mouse_y += 20;
                            }
                            else if (i == 2)
                            {
                                pr->cur_op = i_opd + n_opd;

                                if (pr->cur_op >= pr->n_op)
                                {
                                    pr->cur_op = pr->n_op - 1;
                                }

                                pr->display_mode = IS_ONE_PLOT;
                                pr->one_p = pr->o_p[pr->cur_op];
                                pr->stack = &(pr->one_p->bplt);
                                pr->one_p->need_to_refresh = 1;
                                d_draw_Op_proc(MSG_DRAW, d, c);
                                mouse_y += 20;
                            }
                        }
                    }

                    if (key[KEY_C])
                    {
                        ds_action = KEY_C;
                    }

                    if (key[KEY_P])
                    {
                        ds_action = KEY_P;
                    }

                    if (key[KEY_DEL])
                    {
                        ds_action = KEY_DEL;
                    }
                }

                clear_keybuf();
# ifdef XV_WIN32
                scare_mouse();
#endif
                set_mouse_sprite(NULL);
                set_mouse_sprite_focus(0, 0);
# ifdef XV_WIN32
                unscare_mouse();
#endif

                if (ds_bitmap != NULL)
                {
                    destroy_bitmap(ds_bitmap);
                    ds_bitmap = NULL;
                }

                if (ds_action == KEY_DEL)
                {
                    ds_menu_kill_ds();
                    di = find_dialog_associated_to_pr(pr, NULL);

                    if (di)
                    {
                        di->proc(MSG_DRAW, di, 0);
                    }
                    else
                    {
                        broadcast_dialog_message(MSG_DRAW, 0);
                    }

                    return D_O_K;
                }

                if (abs(dx) > 5 || abs(dy) > 5 || ds_action == KEY_C
                        || ds_action == KEY_P)
                {
                    /*        win_printf("x rel %d op -> %d\nm y %d d->y %d",
                              mouse_x - d->x,(mouse_x - d->x)/120,mouse_y , d->y);*/
                    if (mouse_x >= d->x && mouse_x < d->x + d->w - ds_mark_w &&
                            mouse_y < d->y + d->h && mouse_y > d->y && dsc != NULL)
                    {
                        /*                win_printf("x rel %d op -> %d",mouse_x - d->x,(mouse_x - d->x)/120); */
                        i = (mouse_y < d->y + op_mark_h && mouse_y >= d->y &&
                             mouse_x >= d->x && mouse_x < d->x + d->w &&
                             (mouse_x - d->x) / op_mark_w >= pr->n_op) ? 1 : 0;

                        if (i == 1 || ds_action == KEY_P)
                        {
                            //                            win_printf("create op from ds key %d",ds_action);
                            i = opc->n_dat;
                            opc->n_dat = 0;

                            if ((opd = duplicate_plot_data(opc, NULL)) == NULL)
                            {
                                return D_O_K;
                            }

                            dds = duplicate_data_set(dsc, NULL);

                            if (dds == NULL)
                            {
                                return D_O_K;
                            }

                            if (add_one_plot_data(opd, IS_DATA_SET, (void *)dds))
                            {
                                return D_O_K;
                            }

                            opc->n_dat = i;
                            add_data(pr, IS_ONE_PLOT, (void *)opd);
                            pr->cur_op = pr->n_op - 1;
                            pr->one_p = pr->o_p[pr->cur_op];
                            pr->stack = &(pr->one_p->bplt);
                            pr->one_p->need_to_refresh = 1;
                            return D_REDRAWME;
                        }

                        if (opc == pr->one_p)
                        {
                            dds = duplicate_data_set(dsc, NULL);

                            if (dds == NULL)
                            {
                                return D_O_K;
                            }

                            add_data(pr, IS_DATA_SET, (void *)dds);
                            pr->one_p->need_to_refresh = 1;
                            return D_REDRAWME;
                        }
                        else
                        {
                            unss = opc->xu[opc->c_xu];
                            unsd = pr->one_p->xu[pr->one_p->c_xu];

                            if (unss->type != unsd->type)
                            {
                                units = generate_units(unss->type, unss->decade);
                                unitd = generate_units(unsd->type, unsd->decade);
                                i = win_printf("You are adding data sets with different x units\n"
                                               "souce %s \n destination %s\n"
                                               "do you really want to transfer !\n"
                                               "If you click OK you will probably get garbage \n"
                                               " you are warn ...!", (units) ? units : "no_name",
                                               (unitd) ? unitd : "no_name");

                                if (units)
                                {
                                    free(units);
                                    units = NULL;
                                }

                                if (unitd)
                                {
                                    free(unitd);
                                    unitd = NULL;
                                }

                                if (i == WIN_CANCEL)
                                {
                                    return D_O_K;
                                }

                                dxs = dxd = 1;
                                axs = axd = 0;
                                d_xs = d_xd = 1;
                            }
                            else
                            {
                                dxs = unss->dx;
                                axs = unss->ax;
                                d_xs = 1;

                                for (i = 0; i < unss->decade; i++, d_xs *= 10);

                                for (i = 0; i > unss->decade; i--, d_xs *= .1);

                                dxd = unsd->dx;
                                axd = unsd->ax;
                                d_xd = 1;

                                for (i = 0; i < unsd->decade; i++, d_xd *= 10);

                                for (i = 0; i > unsd->decade; i--, d_xd *= .1);
                            }

                            unss = opc->yu[opc->c_yu];
                            unsd = pr->one_p->yu[pr->one_p->c_yu];

                            if (unss->type != unsd->type)
                            {
                                units = generate_units(unss->type, unss->decade);
                                unitd = generate_units(unsd->type, unsd->decade);
                                i = win_printf("You are adding data sets with different y units\n"
                                               "souce %s \n destination %s\n"
                                               "do you really want to transfer !\n"
                                               "If you click OK you will probably get garbage \n"
                                               "you are warn ...!", (units) ? units : "no_name",
                                               (unitd) ? unitd : "no_name");

                                if (units)
                                {
                                    free(units);
                                    units = NULL;
                                }

                                if (unitd)
                                {
                                    free(unitd);
                                    unitd = NULL;
                                }

                                if (i == WIN_CANCEL)
                                {
                                    return D_O_K;
                                }

                                dys = dyd = 1;
                                ays = ayd = 0;
                                d_ys = d_yd = 1;
                            }
                            else
                            {
                                dys = unss->dx;
                                ays = unss->ax;
                                d_ys = 1;

                                for (i = 0; i < unss->decade; i++, d_ys *= 10);

                                for (i = 0; i > unss->decade; i--, d_ys *= .1);

                                dyd = unsd->dx;
                                ayd = unsd->ax;
                                d_yd = 1;

                                for (i = 0; i < unsd->decade; i++, d_yd *= 10);

                                for (i = 0; i > unsd->decade; i--, d_yd *= .1);
                            }

                            dds = duplicate_data_set(dsc, NULL);

                            if (dds == NULL)
                            {
                                return D_O_K;
                            }

                            if (dxs != dxd || axs != axd || d_xs != d_xd)
                            {
                                for (i = 0; (dxd != 0) && (i < dds->nx) && (i < dds->ny); i++)
                                {
                                    dds->xd[i] = ((((axs + (dxs * dds->xd[i])) * d_xs) / d_xd) - axd) / dxd;
                                }
                            }

                            if (dys != dyd || ays != ayd || d_ys != d_yd)
                            {
                                for (i = 0; (dyd != 0) && (i < dds->nx) && (i < dds->ny); i++)
                                {
                                    dds->yd[i] = ((((ays + (dys * dds->yd[i])) * d_ys) / d_yd) - ayd) / dyd;
                                }
                            }

                            add_data(pr, IS_DATA_SET, (void *)dds);
                            pr->one_p->need_to_refresh = 1;
                            return D_REDRAWME;
                        }
                    }
                }
            }
        }

        yb = y_off - mouse_y;
        yb *= sc;
        yb /= 2;
        xm_s = xb = mouse_x;
        xb *= sc;
        xb /= 2;

        if ((xt = locate_box(pr->stack, QKD_B_XTK_B, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                xts = xt->xc;
                yts = xt->yc - xt->d;
                dy = dyp = 0;

                while (mouse_b & 0x3)
                {
                    dy = mouse_y - ym_s;

                    if (dy != dyp)
                    {
                        xt->yc = yts - (dy * sc) / 2;
                        //just_blit(d);//
                        write_and_blit_box(b, sc, plt_buffer, d, pr);
                        dyp = dy;
                        //if (!screen_used)
                        // {
                        //  screen_used = 1;
                        get_clip_rect(screen, &x1_s, &y1_s, &x2_s, &y2_s);
                        set_clip_rect(screen, d->x, d->y, d->x + d->w, d->y + d->h);

                        if (op->y_unit != NULL)
                            draw_bubble(screen, B_CENTER, mouse_x, mouse_y - 20, "y = %g %s",
                                        y_pr_2_pltdata(pr, SCREEN_H - (y * 2) / sc + dy), op->y_unit);
                        //y_pr_2_pltdata(pr, (y*2)/sc - dy),op->y_unit);
                        else
                            draw_bubble(screen, B_CENTER, mouse_x, mouse_y - 20, "y = %g",
                                        y_pr_2_pltdata(pr, SCREEN_H - (y * 2) / sc + dy));

                        //y_pr_2_pltdata(pr,(v*2)/sc - dy));
                        set_clip_rect(screen, x1_s, y1_s, x2_s, y2_s);
                        //  screen_used = 0;
                        //}
                    }
                }

                //op->y_lo = y_pr_2_pltdata_raw(pr, y);
                //op->y_lo = y_pr_2_pltdata_raw(pr, SCREEN_H - y);
                y = (y * 2) / sc;
                y -= dy;
                op->y_lo = y_pr_2_pltdata_raw(pr, SCREEN_H - y);
                op->iopt2 |= Y_LIM;
                op->need_to_refresh = 1;
                return D_REDRAWME;
            }
        }

        if ((xt = locate_box(pr->stack, QKD_B_XTK_T, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                xts = xt->xc;
                yts = xt->yc;
                dy = dyp = 0;

                while (mouse_b & 0x3)
                {
                    dy = mouse_y - ym_s;

                    if (dy != dyp)
                    {
                        xt->yc = yts - (dy * sc) / 2;
                        //just_blit(d);//
                        write_and_blit_box(b, sc, plt_buffer, d, pr);
                        dyp = dy;
                        //if (!screen_used)
                        //  {
                        //  screen_used = 1;
                        get_clip_rect(screen, &x1_s, &y1_s, &x2_s, &y2_s);
                        set_clip_rect(screen, d->x, d->y, d->x + d->w, d->y + d->h);

                        if (op->y_unit != NULL)
                            draw_bubble(screen, B_CENTER, mouse_x, mouse_y - 20, "y = %g %s",
                                        y_pr_2_pltdata(pr, SCREEN_H - (v * 2) / sc + dy), op->y_unit);
                        else
                            draw_bubble(screen, B_CENTER, mouse_x, mouse_y + 20, "y = %g",
                                        y_pr_2_pltdata(pr, SCREEN_H - (v * 2) / sc + dy));

                        set_clip_rect(screen, x1_s, y1_s, x2_s, y2_s);
                        //  screen_used = 0;
                        //}
                    }
                }

                v = (v * 2) / sc;
                v -= dy;
                op->y_hi = y_pr_2_pltdata_raw(pr, SCREEN_H - v);
                op->iopt2 |= Y_LIM;
                op->need_to_refresh = 1;
                return D_REDRAWME;
            }
        }

        if ((xt = locate_box(pr->stack, QKD_B_YTK_R, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                xts = xt->xc;
                yts = xt->yc;
                dx = dxp = 0;

                while (mouse_b & 0x3)
                {
                    dx = mouse_x - xm_s;

                    if (dx != dxp)
                    {
                        xt->xc = xts + (dx * sc) / 2;
                        //just_blit(d);//
                        write_and_blit_box(b, sc, plt_buffer, d, pr);
                        dxp = dx;
                        //if (!screen_used)
                        //  {
                        //    screen_used = 1;
                        get_clip_rect(screen, &x1_s, &y1_s, &x2_s, &y2_s);
                        set_clip_rect(screen, d->x, d->y, d->x + d->w, d->y + d->h);

                        if (op->x_unit != NULL)
                            draw_bubble(screen, B_RIGHT, mouse_x - 20, mouse_y, "x = %g %s",
                                        x_pr_2_pltdata(pr, (u * 2) / sc + dx), op->x_unit);
                        else
                            draw_bubble(screen, B_RIGHT, mouse_x - 20, mouse_y, "x = %g",
                                        x_pr_2_pltdata(pr, (u * 2) / sc + dx));

                        set_clip_rect(screen, x1_s, y1_s, x2_s, y2_s);
                        //  screen_used = 0;
                        //}
                    }
                }

                u = (u * 2) / sc;
                u += dx;
                op->x_hi = x_pr_2_pltdata_raw(pr, u);
                op->iopt2 |= X_LIM;
                op->need_to_refresh = 1;
                return D_REDRAWME;
            }
        }

        if ((xt = locate_box(pr->stack, QKD_B_YTK_L, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                xts = xt->xc;
                yts = xt->yc;
                dx = dxp = 0;

                while (mouse_b & 0x3)
                {
                    dx = mouse_x - xm_s;

                    if (dx != dxp)
                    {
                        xt->xc = xts + (dx * sc) / 2;
                        //just_blit(d);//
                        write_and_blit_box(b, sc, plt_buffer, d, pr);
                        dxp = dx;
                        //if (!screen_used)
                        //{
                        //  screen_used = 1;
                        get_clip_rect(screen, &x1_s, &y1_s, &x2_s, &y2_s);
                        set_clip_rect(screen, d->x, d->y, d->x + d->w, d->y + d->h);

                        if (op->x_unit != NULL)
                            draw_bubble(screen, B_LEFT, mouse_x + 20, mouse_y, "x = %g %s",
                                        x_pr_2_pltdata(pr, (x * 2) / sc + dx), op->x_unit);
                        else
                            draw_bubble(screen, B_LEFT, mouse_x + 20, mouse_y, "x left = %g",
                                        x_pr_2_pltdata(pr, (x * 2) / sc + dx));

                        set_clip_rect(screen, x1_s, y1_s, x2_s, y2_s);
                        //  screen_used = 0;
                        //}
                    }
                }

                x = (x * 2) / sc;
                x += dx;
                op->x_lo = x_pr_2_pltdata_raw(pr, x);
                op->iopt2 |= X_LIM;
                op->need_to_refresh = 1;
                return D_REDRAWME;
            }
        }

        if ((xt = locate_box(pr->stack, QKD_B_XLABEL, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                return move_label(xt, b, sc, plt_buffer, d, pr, xm_s, ym_s, x, y);    // was ,v);
            }

            while ((xt = locate_box(pr->stack, QKD_B_XLABEL, &x, &u, &y, &v, NEXT)) != NULL)
            {
                if (xb >= x && xb < u && yb >= y && yb < v)
                {
                    return move_label(xt, b, sc, plt_buffer, d, pr, xm_s, ym_s, x, y);    // was ,v);
                }
            }
        }

        if ((xt = locate_box(pr->stack, QKD_B_YLABEL, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                return move_label(xt, b, sc, plt_buffer, d, pr, xm_s, ym_s, x, y);
            }

            while ((xt = locate_box(pr->stack, QKD_B_YLABEL, &x, &u, &y, &v, NEXT)) != NULL)
            {
                if (xb >= x && xb < u && yb >= y && yb < v)
                {
                    return move_label(xt, b, sc, plt_buffer, d, pr, xm_s, ym_s, x, y);
                }
            }
        }

        if ((xt = locate_box(pr->stack, QKD_B_PLOT, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if ((xb >= x && xb < u && yb >= y && yb < v)
                    && (general_pr_action != NULL))
            {
                /*                win_printf("general pr action");*/
                return general_pr_action(pr, xm_s, ym_s, d);
            }
        }
    }
    else if (msg == MSG_WHEEL)
    {
        if (plot_wheel_action != NULL)
        {
            plot_wheel_action(pr, pr->one_p, d, c);
        }
    }
    else if (msg == MSG_IDLE)
    {
        if (plot_idle_action != NULL)
        {
            plot_idle_action(pr, pr->one_p, d);
        }
    }
    else if (msg == MSG_LOSTMOUSE)
    {
        if (pr->one_p->op_lost_mouse != NULL)
        {
            pr->one_p->op_lost_mouse(pr->one_p, mouse_x, mouse_y, 0);
        }
    }
    else if (msg == MSG_GOTMOUSE)
    {
        //Display_title_message("image got mouse");
        if (pr->one_p->op_got_mouse != NULL)
        {
            pr->one_p->op_got_mouse(pr->one_p, mouse_x, mouse_y, 0);
        }
    }
    else if (msg == MSG_WANTFOCUS)
    {
        return D_WANTFOCUS;
    }
    else if (msg == MSG_LOSTFOCUS)
    {
        return D_WANTFOCUS;
    }
    else if (msg == MSG_KEY)
    {
        return D_WANTFOCUS;
    }

    return D_O_K;
}




pltreg *find_pr_in_current_dialog(DIALOG *di)
{
    int i;
    pltreg *pr = NULL;

    if (di == NULL)
    {
        di = active_dialog;
    }

    for (i = 0, pr = NULL; di[i].proc != NULL ; i++)
    {
        if (di[i].proc == d_draw_Op_proc)
        {
            if (pr == NULL)
            {
                pr = (pltreg *) di[i].dp;    // UNSAFE�CAST
            }
            else if (di->flags & D_SELECTED)
            {
                pr = (pltreg *) di[i].dp;    // UNSAFE�CAST
            }
        }
    }

    return pr;
}
DIALOG *attach_new_plot_region_to_dialog(int key, int flags, int d1, int d2)
{
    return attach_new_item_to_dialog(d_draw_Op_proc, key, flags, d1, d2);
}

int re_attach_plot_region_to_dialog(DIALOG *di, pltreg *pr)
{
    if (pr == NULL || di == NULL)
    {
        return 1;
    }

    di->dp = pr;
    di->proc = d_draw_Op_proc;
    return 0;
}


DIALOG *find_dialog_associated_to_pr(pltreg *pr, DIALOG *di)
{
    int i;

    if (di == NULL)
    {
        di = active_dialog;
    }

    for (i = 0; (di[i].proc != NULL) && (i < 256); i++)
    {
        if (di[i].proc == d_draw_Op_proc)
        {
            if (pr == di[i].dp)
            {
                return di + i;
            }
        }
    }

    if (i >= 256)
    {
        allegro_message("Hi dialog index %d", i);
    }

    return NULL;
}



int    refresh_plot(pltreg *pr, int n_plot)
{
    DIALOG *di = NULL;

    if (pr == NULL)
    {
        return D_O_K;
    }

    if (n_plot != UNCHANGED && n_plot <= pr->n_op && n_plot >= 0)
    {
        pr->cur_op = n_plot;
        pr->cur_op = (pr->cur_op >= pr->n_op || pr->cur_op < 0) ? 0 : pr->cur_op;
        pr->one_p = pr->o_p[pr->cur_op];
        pr->stack = &(pr->one_p->bplt);
        pr->display_mode = IS_ONE_PLOT;
    }
    else if (n_plot >= ALL_PLOTS && n_plot <= ALL_PLOTS + 4)
    {
        pr->display_mode = n_plot;
    }

    /*    return broadcast_dialog_message(MSG_DRAW,0);
    */
    di = find_dialog_associated_to_pr(pr, NULL);

    if (di)
    {
        di->proc(MSG_DRAW, di, 0);
    }
    else
    {
        display_title_message("dialog not found");
    }

    return D_O_K;
}

/*    int do_one_plot (pltreg *pr);
 *    DESCRIPTION
 *
 *
 *
 *    RETURNS        0 on success, 1
 *
 */
int do_one_plot(pltreg *pr)
{
    if (pr == NULL)
    {
        return D_O_K;
    }

    if (pr->one_p->data_changing || pr->one_p->transfering_data_to_box)
    {
        return D_O_K;
    }

    if (pr->stack_in_use || pr->blitting_stack)
    {
        return D_O_K;
    }

    pr->one_p->transfering_data_to_box = 1;
    pr->stack_in_use = 1;
    pr->one_p = pr->o_p[pr->cur_op];
    pr->stack = &(pr->one_p->bplt);
    free_box(pr->stack);  // added nov 2005
    qterm(pr->stack);
    free_qkd_box();
    init_box(pr->stack);
    pr->stack->color = Lightgreen;
    qterm(pr->stack);
    do_it_one_plot(pr);
    pr->one_p->transfering_data_to_box = 0;
    pr->stack_in_use = 0;
    return (0);
}
int do_it_one_plot(pltreg *pr)
{
    int x0 = 0, y0 = 0, x1, y1;

    if (pr == NULL)
    {
        return D_O_K;
    }

    do_it_one_plot_op_pr(pr->one_p, pr);
    pr->plt = locate_box(pr->stack, QKD_B, &x0, &x1, &y0, &y1, FIRST);
    return 0;
}

# ifdef OLD
int    refresh_plot(pltreg *pr, int n_plot)
{
    if (pr == NULL)
    {
        return -1;
    }

    erase_pr_box(pr);

    if (n_plot != UNCHANGED && n_plot <= pr->n_op && n_plot >= 0)
    {
        pr->cur_op = n_plot;
        pr->cur_op = (pr->cur_op >= pr->n_op || pr->cur_op < 0) ? 0 : pr->cur_op;
        pr->one_p = pr->o_p[pr->cur_op];
        pr->stack = &(pr->one_p->bplt);
        pr->display_mode = IS_ONE_PLOT;
        do_one_plot(pr);
    }
    else if (n_plot >= ALL_PLOTS && n_plot <= ALL_PLOTS + 4)
    {
        prepare_all_plots(pr, n_plot - ALL_PLOTS);
    }
    else
    {
        do_one_plot(pr);
    }

    draw_pr_box(pr, UNCHANGED, UNCHANGED, 0, Lightgreen);
    switch_plot(0);
    switch_data_set(0);
    return pr->cur_op;
}
# endif

int do_it_one_plot_op_pr(O_p *o_p, pltreg *pr)
{
    int i, j, k;
    float    x[2] = {0}, y[2] = {0}, *xe = NULL, *ye = NULL;
    struct data_set *p_d_s = NULL;
    int iopt, min, tmp = 0, fg_keep;
    struct plot_label *p_p_l = NULL;
    un_s *un = NULL;

    if (o_p == NULL || o_p->n_dat == 0)
    {
        return D_O_K;
    }

    if (o_p->width != 1 || o_p->height != 1)
    {
        qdset_abs(o_p->width, 0, o_p->height, 0);
    }

    qdset_ticklen(o_p->tick_len);

    if (o_p->tick_len_x != 1)
    {
        qdset_ticklen_x(o_p->tick_len_x);
    }

    if (o_p->tick_len_y != 1)
    {
        qdset_ticklen_y(o_p->tick_len_y);
    }

    qkylabl_set((o_p->iopt2 & Y_NUM));
    qkxlabl_set((o_p->iopt2 & X_NUM));
    qkyunit(o_p->y_prefix, o_p->y_unit);
    qkxunit(o_p->x_prefix, o_p->x_unit);

    if (!(o_p->iopt2 & X_LIM))
    {
        find_x_limits(o_p);
    }
    else if (o_p->iopt & XLOG)
        if (o_p->x_lo <= 0 || o_p->x_hi <= 0)
        {
            find_x_limits(o_p);
        }

    if (!(o_p->iopt2 & Y_LIM))
    {
        find_y_limits(o_p);
    }
    else if (o_p->iopt & YLOG)
        if (o_p->y_lo <= 0 || o_p->y_hi <= 0)
        {
            find_y_limits(o_p);
        }

    //restricted accuracy of float
    if (((o_p->x_hi - o_p->x_lo) > 0) && (o_p->x_lo > 0) && ((o_p->x_hi - o_p->x_lo) < 5e-6 * (o_p->x_hi + o_p->x_lo)))
    {
        o_p->x_hi = o_p->x_lo + 5e-6 * (o_p->x_lo + o_p->x_lo);
    }

    if (((o_p->y_hi - o_p->y_lo) > 0) && (o_p->y_lo) && ((o_p->y_hi - o_p->y_lo) < 5e-6 * (o_p->y_hi + o_p->y_lo)))
    {
        o_p->y_hi = o_p->y_lo + 5e-6 * (o_p->y_lo + o_p->y_lo);
    }

    /* draw axes with a scale change if nessecery */
    qkd_color(PlotAxesColor, Yellow);
    iopt = NOCLEAR;
    iopt |= (o_p->iopt & (GRID));
    iopt |= o_p->iopt & (XLOG + YLOG);
    iopt |= DOT;
    iopt |= TRIM;
    x[0] = o_p->dx * o_p->x_lo + o_p->ax;
    x[0] = (fpclassify(x[0]) & (FP_NAN)) ? ((o_p->iopt & XLOG) ? 0.01 : -0.1) : x[0];
    x[1] = o_p->dx * o_p->x_hi + o_p->ax;
    x[1] = (fpclassify(x[1]) & (FP_NAN)) ? 0.1 : x[1];
    y[0] = o_p->dy * o_p->y_lo + o_p->ay;
    y[0] = (fpclassify(y[0]) & (FP_NAN)) ? ((o_p->iopt & YLOG) ? 0.01 : -0.1) : y[0];
    y[1] = o_p->dy * o_p->y_hi + o_p->ay;
    y[1] = (fpclassify(y[1]) & (FP_NAN)) ? 0.1 : y[1];
    qkdraw(2, x, y, iopt, x, x + 1, y, y + 1);

    //win_printf("After draw");
    if (o_p->x_title != NULL)
    {
        qxlabl(o_p->x_title);
    }

    if (o_p->y_title != NULL)
    {
        qylabl(o_p->y_title);
    }

    /*
          if (o_p->x_prime_title == NULL     && o_p->title != NULL)
          qdtitl(o_p->title);*/
    /*    pr->plt = last_q_b();*/
    iopt = NOCLEAR;
    iopt |= (o_p->iopt & (GRID));
    iopt |= o_p->iopt & (XLOG + YLOG);
    iopt |= DOT;
    iopt |= AXES_PRIME;

    if (o_p->c_xu_p >= 0 && o_p->c_xu_p < o_p->n_xu)
    {
        un = o_p->xu[o_p->c_xu_p];
        qkxunit(o_p->x_prefix, un->name);
        qkxlabl_set(X_NUM);
        x[0] = un->dx * o_p->x_lo + un->ax;
        x[1] = un->dx * o_p->x_hi + un->ax;
    }
    else
    {
        qkxlabl_set(0);
        x[0] = o_p->dx * o_p->x_lo + o_p->ax;
        x[1] = o_p->dx * o_p->x_hi + o_p->ax;
    }

    if (o_p->c_yu_p >= 0 && o_p->c_yu_p < o_p->n_yu)
    {
        un = o_p->yu[o_p->c_yu_p];
        qkyunit(o_p->y_prefix, un->name);
        qkylabl_set(Y_NUM);
        y[0] = un->dx * o_p->y_lo + un->ax;
        y[1] = un->dx * o_p->y_hi + un->ax;
    }
    else
    {
        qkylabl_set(0);
        y[0] = o_p->dy * o_p->y_lo + o_p->ay;
        y[1] = o_p->dy * o_p->y_hi + o_p->ay;
    }

    fg_keep = fine_grid;
    fine_grid = 0;
    qkdraw(2, x, y, iopt, x, x + 1, y, y + 1);
    fine_grid = fg_keep;

    if (o_p->y_prime_title != NULL)
    {
        qylabl(o_p->y_prime_title);
    }

    if (o_p->x_prime_title != NULL)
    {
        qxlabl(o_p->x_prime_title);
    }

    /*    if (o_p->title != NULL && o_p->x_prime_title != NULL)
          qdtitl(o_p->title);*/

    if (o_p->title != NULL)
    {
        qdtitl(o_p->title);
    }

    /*    pr->plt = last_q_b();*/
# ifdef OLD

    for (i = 0, iopt = 0 ; i < o_p->n_dat ; i++)
    {
        if (o_p->dat[i]->invisible)
        {
            continue;
        }

        p_d_s.symb = o_p->dat[i]->symb;
        p_d_s.m = o_p->dat[i]->m;
        p_d_s.xd = o_p->dat[i]->xd;
        p_d_s.yd = o_p->dat[i]->yd;
        p_d_s.nx = o_p->dat[i]->nx;
        p_d_s.ny = o_p->dat[i]->ny;
        qkd_color(PlotAxesColor, o_p->dat[i]->color);
        qdbox(p_d_s.symb);
        iopt = NOAXES + NOCLEAR;
        iopt |= o_p->iopt & (XLOG + YLOG);

        if (p_d_s.m == 2)
        {
            iopt |= DOT;
        }

        if (p_d_s.m == 0)
        {
            iopt |= DASH;
        }

        min = (p_d_s.nx > p_d_s.ny) ? p_d_s.ny : p_d_s.nx;
        min = (p_d_s->mx > min) ? min : p_d_s->mx;
        min = (p_d_s->my > min) ? min : p_d_s->my;
        qkdraw(min, p_d_s.xd, p_d_s.yd, iopt, &(o_p->x_lo), &(o_p->x_hi), &(o_p->y_lo), &(o_p->y_hi));
    }

# endif

    for (i = 0, iopt = 0 ; i < o_p->n_dat ; i++)
    {
        if (o_p->dat[i]->invisible)
        {
            continue;
        }

        p_d_s = o_p->dat[i];
        /*        win_printf("plot ds %d error in y %s",i,(p_d_s->ye != NULL)?"yese":"no");*/
        qkd_color(PlotAxesColor, o_p->dat[i]->color);
        qdbox(p_d_s->symb);
        iopt = NOAXES + NOCLEAR;
        iopt |= o_p->iopt & (XLOG + YLOG);

        if (p_d_s->m == 2)
        {
            iopt |= DOT;
        }

        if (p_d_s->m == 0)
        {
            iopt |= DASH;
        }

        min = (p_d_s->nx > p_d_s->ny) ? p_d_s->ny : p_d_s->nx;
        min = (p_d_s->mx > min) ? min : p_d_s->mx;
        min = (p_d_s->my > min) ? min : p_d_s->my;
        qkdraw(min, p_d_s->xd, p_d_s->yd, iopt, &(o_p->x_lo), &(o_p->x_hi), &(o_p->y_lo), &(o_p->y_hi));

        if (p_d_s->xe != NULL && min > 0)
        {
            iopt = NOAXES + NOCLEAR;
            iopt |= o_p->iopt & (XLOG + YLOG);
            iopt |= DASH;

            if (xe == NULL)
            {
                xe = (float *)calloc(2 * min, sizeof(float));
            }

            if (ye == NULL)
            {
                ye = (float *)calloc(2 * min, sizeof(float));
            }

            if (xe == NULL || ye == NULL)
            {
                break;
            }

            if (p_d_s->xed == NULL)
            {
                for (j = k = 0; j < min ; j++)
                {
                    ye[k] = p_d_s->yd[j];
                    xe[k++] = p_d_s->xd[j] - p_d_s->xe[j];
                    ye[k] = p_d_s->yd[j];
                    xe[k++] = p_d_s->xd[j] + p_d_s->xe[j];
                }
            }
            else
            {
                //asymetric error
                for (j = k = 0; j < min ; j++)
                {
                    ye[k] = p_d_s->yd[j];
                    xe[k++] = p_d_s->xd[j] - p_d_s->xed[j];
                    ye[k] = p_d_s->yd[j];
                    xe[k++] = p_d_s->xd[j] + p_d_s->xe[j];
                }
            }

            qdbox("\\pt4|");
            qkdraw(2 * min, xe, ye, iopt, &(o_p->x_lo), &(o_p->x_hi), &(o_p->y_lo), &(o_p->y_hi));
        }

        if (p_d_s->ye != NULL && min > 0)
        {
            //            win_printf("plot error in y");
            //            alert("plot error in y","2","3","Ok",NULL,0,0);
            qkd_color(PlotAxesColor, o_p->dat[i]->color);
            iopt = NOAXES + NOCLEAR;
            iopt |= o_p->iopt & (XLOG + YLOG);
            iopt |= DASH;

            if (xe == NULL)
            {
                xe = (float *)calloc(2 * min, sizeof(float));
            }

            if (ye == NULL)
            {
                ye = (float *)calloc(2 * min, sizeof(float));
            }

            if (xe == NULL || ye == NULL)
            {
                break;
            }

            if (p_d_s->yed == NULL)
            {
                for (j = k = 0; j < min ; j++)
                {
                    xe[k] = p_d_s->xd[j];
                    ye[k++] = p_d_s->yd[j] - p_d_s->ye[j];
                    xe[k] = p_d_s->xd[j];
                    ye[k++] = p_d_s->yd[j] + p_d_s->ye[j];
                }
            }
            else
            {
                //asymetric error
                for (j = k = 0; j < min ; j++)
                {
                    xe[k] = p_d_s->xd[j];
                    ye[k++] = p_d_s->yd[j] - p_d_s->yed[j];
                    xe[k] = p_d_s->xd[j];
                    ye[k++] = p_d_s->yd[j] + p_d_s->ye[j];
                }
            }

            //            alert("before qkb","2","3","Ok",NULL,0,0);
            qdbox("\\pt4\\line");
            qkdraw(2 * min, xe, ye, iopt, &(o_p->x_lo), &(o_p->x_hi), &(o_p->y_lo), &(o_p->y_hi));
            //            qkdraw(min,p_d_s->xd,p_d_s->yd,iopt,&(o_p->x_lo),&(o_p->x_hi),&(o_p->y_lo),&(o_p->y_hi));
        }

        if (xe != NULL)
        {
            free(xe);
            xe = NULL;
        }

        if (ye != NULL)
        {
            free(ye);
            ye = NULL;
        }

        if (p_d_s->xbu != NULL)
        {
            iopt = NOAXES + NOCLEAR;
            iopt |= o_p->iopt & (XLOG + YLOG);

            for (j = k = 0; j < min ; j++)
            {
                float curx_1 = p_d_s->xbd ? p_d_s->xd[j] - p_d_s->xbd[j] : p_d_s->xd[j] - p_d_s->xbu[j];
                float curx = p_d_s->xd[j];
                float curx1 = p_d_s->xd[j] + p_d_s->xbu[j];
                float cury1 = p_d_s->yd[j] + p_d_s->boxplot_width / 2;
                float cury_1 = p_d_s->yd[j] - p_d_s->boxplot_width / 2;
                float xb[7] = {curx, curx1, curx1, curx_1, curx_1, curx, curx};
                float yb[7] = {cury_1, cury_1, cury1, cury1, cury_1, cury_1, cury1};
                qdbox("\\pt4");
                qkdraw(7, xb, yb, iopt, &(o_p->x_lo), &(o_p->x_hi), &(o_p->y_lo), &(o_p->y_hi));
            }
        }

        if (p_d_s->ybu != NULL)
        {
            iopt = NOAXES + NOCLEAR;
            iopt |= o_p->iopt & (XLOG + YLOG);

            for (j = k = 0; j < min ; j++)
            {
                float curx1 = p_d_s->xd[j] + p_d_s->boxplot_width / 2;
                float curx_1 = p_d_s->xd[j] - p_d_s->boxplot_width / 2;
                float cury_1 = p_d_s->ybd ? p_d_s->yd[j] - p_d_s->ybd[j] : p_d_s->yd[j] - p_d_s->ybu[j];
                float cury = p_d_s->yd[j];
                float cury1 = p_d_s->yd[j] + p_d_s->ybu[j];
                float xb[7] = {curx_1, curx_1, curx1, curx1, curx_1, curx_1, curx1};
                float yb[7] = {cury, cury1, cury1, cury_1, cury_1, cury, cury};
                qdbox("\\pt4");
                qkdraw(7, xb, yb, iopt, &(o_p->x_lo), &(o_p->x_hi), &(o_p->y_lo), &(o_p->y_hi));
            }
        }
    }

    for (i = 0 ; i < o_p->n_lab ; i++)
    {
        qkd_lab_color(Lightred);
        p_p_l = o_p->lab[i];

        if (p_p_l->text == NULL)
        {
            continue;
        }

        if (strncmp(p_p_l->text, "\\plot{", 6) == 0)
        {
            j = sscanf(p_p_l->text + 6, "%d", &tmp);

            if (j != 1)
            {
                return    win_printf_OK("wrong \\\\ plot command \n %s", p_p_l->text + 6);
            }

            if (pr == NULL || tmp >= pr->n_op)
            {
                return win_printf_OK("cannot find plot");
            }

            if (o_p == pr->o_p[tmp])
            {
                return win_printf_OK("you cannot include a plot in itself!\n silly guy...");
            }

            if (p_p_l->type == USR_COORD)
            {
                p_p_l->b = xlabel("a", p_p_l->xla, p_p_l->yla);
            }
            else    if (p_p_l->type == ABS_COORD)
                p_p_l->b = xlabel("a", o_p->x_lo + (o_p->x_hi
                                                    - o_p->x_lo) * p_p_l->xla, o_p->y_lo + (o_p->y_hi - o_p->y_lo) * p_p_l->yla);
            else    if (p_p_l->type ==  VERT_LABEL_USR)
            {
                p_p_l->b = ylabel("a", p_p_l->xla, p_p_l->yla);
            }
            else    if (p_p_l->type ==  VERT_LABEL_ABS)
                p_p_l->b = ylabel("a", o_p->x_lo + (o_p->x_hi
                                                    - o_p->x_lo) * p_p_l->xla, o_p->y_lo + (o_p->y_hi - o_p->y_lo) * p_p_l->yla);

            if (p_p_l->b == NULL || p_p_l->b->child == NULL || p_p_l->b->child[0] == NULL)
            {
                return win_printf_OK("wrong label");
            }

            p_p_l->b->child[0] = duplicate_box(p_p_l->b->child[0], &(pr->o_p[tmp]->bplt));

            if (p_p_l->b->child[0] == NULL)
            {
                return win_printf_OK("cannot duplicate plot");
            }

            p_p_l->b->w = p_p_l->b->child[0]->w;
            p_p_l->b->h = p_p_l->b->child[0]->h;
            p_p_l->b->d = p_p_l->b->child[0]->d;
            p_p_l->b->child[0]->parent = p_p_l->b;
            rearrange_hic(p_p_l->b);
        }
        else
        {
            if (p_p_l->type == USR_COORD)
            {
                p_p_l->b = xlabel(p_p_l->text, p_p_l->xla, p_p_l->yla);
            }
            else    if (p_p_l->type == ABS_COORD)
                p_p_l->b = xlabel(p_p_l->text, o_p->x_lo + (o_p->x_hi
                                  - o_p->x_lo) * p_p_l->xla, o_p->y_lo + (o_p->y_hi - o_p->y_lo) * p_p_l->yla);
            else    if (p_p_l->type ==  VERT_LABEL_USR)
            {
                p_p_l->b = ylabel(p_p_l->text, p_p_l->xla, p_p_l->yla);
            }
            else    if (p_p_l->type ==  VERT_LABEL_ABS)
                p_p_l->b = ylabel(p_p_l->text, o_p->x_lo + (o_p->x_hi
                                  - o_p->x_lo) * p_p_l->xla, o_p->y_lo + (o_p->y_hi - o_p->y_lo) * p_p_l->yla);
        }
    }

    for (i = 0; i < o_p->n_dat; i++)
    {
        p_d_s = o_p->dat[i];

        for (j = 0 ; j < p_d_s->n_lab ; j++)
        {
            qkd_lab_color(Lightred);
            p_p_l = p_d_s->lab[j];

            if (p_p_l->type == USR_COORD)
            {
                p_p_l->b = xlabel(p_p_l->text, p_p_l->xla, p_p_l->yla);
            }
            else    if (p_p_l->type == ABS_COORD)
                p_p_l->b = xlabel(p_p_l->text, o_p->x_lo + (o_p->x_hi - o_p->x_lo)
                                  * p_p_l->xla, o_p->y_lo + (o_p->y_hi - o_p->y_lo) * p_p_l->yla);
            else    if (p_p_l->type ==  VERT_LABEL_USR)
            {
                p_p_l->b = ylabel(p_p_l->text, p_p_l->xla, p_p_l->yla);
            }
            else    if (p_p_l->type ==  VERT_LABEL_ABS)
                p_p_l->b = ylabel(p_p_l->text, o_p->x_lo + (o_p->x_hi - o_p->x_lo)
                                  * p_p_l->xla, o_p->y_lo + (o_p->y_hi - o_p->y_lo) * p_p_l->yla);
        }
    }

    if (selected_point.text != NULL && selected_point_enable)
    {
        selected_point.b = xlabel(selected_point.text, selected_point.xla, selected_point.yla);
    }

    return 0;
}

/*    int find_box_scale (struct box *bo, dybox *db);
 *    DESCRIPTION    find the scaling to fit a box in a rectangle
 *
 *    RETURNS        the scale, 0 on null box
 */
int find_box_scale(struct box *bo, dybox *db)
{
    int xs, ys, dw, dh, bh, sc;
    bh = bo->d + bo->h;
    dw = db->x1 - db->x0;
    dh = db->y1 - db->y0;

    if (dw == 0 || dh == 0)
    {
        return 0;
    }

    xs = (2 * bo->w) / dw;
    ys = (2 * bh) / dh;
    sc = (xs < ys) ? ys : xs;
    sc++;
    return sc;
}


int    prepare_all_plots(pltreg *pr, int n_col)
{
    int i;
    static char set_of_op_in_multiop[1024] = {0};
    char question[2048] = {0},*token = NULL;
    static int n_set_of_op_in_multiop = 0;
    int *set_of_op_array = NULL;

    if (pr == NULL || pr->n_op < 2)
    {
        return D_O_K;
    }

    if (pr->all.child != NULL)
    {
        free(pr->all.child);
        pr->all.child = NULL;
    }
    if (n_set_of_op_in_multiop == 0 || n_set_of_op_in_multiop > pr->n_op)
        {
            for (i = 0; i < pr->n_op; i++)
                {
                    snprintf(set_of_op_in_multiop+strlen(set_of_op_in_multiop)
                             ,sizeof(set_of_op_in_multiop)-strlen(set_of_op_in_multiop)
                             ,"%s%d",(i)?",":"",i);
                }
        }
    if (eps_file_flag == 0)
        {
            snprintf(question,sizeof(question),
                     "You are going to assemble multiple plots in %d colum(s).\n"
                     "You can select the list and order of plots in [0,%d[\n"
                     "by placing their numbers separated by a coma:\n"
                     "%%32s\n",n_col,pr->n_op);
            i = win_scanf(question,set_of_op_in_multiop);
            if (i == WIN_CANCEL)     return 0;
        }
    set_of_op_array = (int*)calloc(pr->n_op,sizeof(int));
    if (set_of_op_array == NULL)     return 0;
    char *local_set = strdup(set_of_op_in_multiop);
    for (n_set_of_op_in_multiop = 0, token = strtok(local_set, ","); token != NULL && n_set_of_op_in_multiop < pr->n_op
             ; token = strtok(NULL, ","))
        {
            set_of_op_array[n_set_of_op_in_multiop++] = atoi(token);
            //win_printf("token %d %s",n_set_of_op_in_multiop,token);

        }
    for (i = 0; i < n_set_of_op_in_multiop; i++)
        {
            if (set_of_op_array[i] < 0
                || set_of_op_array[i] >= pr->n_op)
                return win_printf("Plot %d out of range [0,%d[!"
                                  ,set_of_op_array[i],pr->n_op);
        }
    init_box(&pr->all);
    pr->all.pt = 10;
    pr->all.color = Lightgreen;
    pr->all.child = (struct box **)calloc(n_set_of_op_in_multiop, sizeof(struct box *));
    if (pr->all.child == NULL)
    {
        return D_O_K;
    }

    pr->all.n_box = n_set_of_op_in_multiop;

    for (i = 0 ; i < n_set_of_op_in_multiop ; i++)
    {
        pr->all.child[i] = &(pr->o_p[set_of_op_array[i]]->bplt);
        pr->all.child[i]->xc = 0;
        pr->all.child[i]->yc = 0;
    }

    if (n_col == 3 && pr->n_op >= 3)
    {
        plots_in_three_columns(&pr->all);
        rearrange_hic(&pr->all);
        /*
        for (i = 0; i < pr->all.n_box ; i++)
            win_printf("box %d xc=%d yc=%d, h=%d d=%d w=%d\n"
                       ,i,pr->all.child[i]->xc, pr->all.child[i]->yc
                       ,pr->all.child[i]->d, pr->all.child[i]->h, pr->all.child[i]->w);
        */
    }
    else if (n_col == 2)
    {
        plots_in_two_columns(&pr->all);
        rearrange_hic(&pr->all);
    }
    else
    {
        plots_in_one_column(&pr->all);
        rearrange_hic(&pr->all);
    }
    pr->stack = &pr->all;
    if(local_set) free(local_set);
    if (set_of_op_array) free(set_of_op_array);
    return 0;
}
int plots_in_one_column(struct box *bo)
{
    int i;
    int  xcm = 0, x0, y0, x1, y1;
    struct box *b = NULL, *plot = NULL;

    for (i = 0 ; i < bo->n_box ; i++)
    {
        b = bo->child[i];
        x0 = 0;
        y0 = 0;
        plot = locate_box(b, QKD_B_PLOT, &x0, &x1, &y0, &y1, FIRST);

        if (plot != NULL)
        {
            xcm = (x0 > xcm) ? x0 : xcm;
        }
    }

    for (i = 0 ; i < bo->n_box ; i++)
    {
        b = bo->child[i];
        x0 = 0;
        y0 = 0;
        plot = locate_box(b, QKD_B_PLOT, &x0, &x1, &y0, &y1, FIRST);

        if (plot != NULL)
        {
            b->xc = xcm - x0;
        }

        b->yc = bo->h;
        bo->h += b->h + b->d + 100;
        bo->w = (bo->w > (b->w + b->xc)) ? bo->w : (b->w + b->xc);
    }

    return 0;
}
int plots_in_two_columns(struct box *bo)
{
    int i;
    int  xcm[2] = {0}, wm[2] = {0};
    int x0, y0, x1, y1, dh, mh;
    struct box *b = NULL, *plot = NULL;


    for (i = 0 ; i < 2 * (bo->n_box / 2) ; i++)
    {
        b = bo->child[i];
        x0 = 0;
        y0 = 0;
        plot = locate_box(b, QKD_B_PLOT, &x0, &x1, &y0, &y1, FIRST);

        if (plot != NULL)
        {
            xcm[i % 2] = (x0 > xcm[i % 2]) ? x0 : xcm[i % 2];
            wm[i % 2] = (b->w - x0 > wm[i % 2]) ? b->w - x0 : wm[i % 2];
        }
    }

    xcm[1] += xcm[0] + wm[0] + 100;

    for (i = 0, dh = 0, mh = 0 ; i < (bo->n_box / 2) ; i++)
    {
        b = bo->child[2 * i];
        x0 = 0;
        y0 = 0;
        plot = locate_box(b, QKD_B_PLOT, &x0, &x1, &y0, &y1, FIRST);
        if (plot != NULL)
        {
            b->xc = xcm[0] - x0;
            dh = y0;
            mh = b->d + b->h - y0;
        }
        b->yc = bo->h - y0;

        b = bo->child[2 * i + 1];
        x0 = 0;
        y0 = 0;
        plot = locate_box(b, QKD_B_PLOT, &x0, &x1, &y0, &y1, FIRST);
        if (plot != NULL)
        {
            b->xc = xcm[1] - x0;
            dh = (y0 > dh) ? y0 : dh;
            mh = (b->d + b->h - y0 > mh) ? b->d + b->h - y0 : mh;
        }
        b->yc = bo->h + dh - y0;

        bo->child[2 * i]->yc += dh;
        bo->h += dh + mh + 100;
    }

    bo->w = xcm[1] + wm[1];

    if (bo->n_box > 2 && bo->n_box % 2 == 1)
    {
        b = bo->child[bo->n_box - 1];
        b->yc = bo->h;
        b->xc = ((bo->w - b->w) / 2 > 0) ? (bo->w - b->w) / 2 : 0;
        bo->w = (b->w > bo->w) ? b->w : bo->w;
        bo->h += b->h + 100;
    }
    /*
    for (i = 0; i < bo->n_box ; i++)
        win_printf("box %d xc=%d yc=%d, h=%d d=%d w=%d\n"
                   ,i,bo->child[i]->xc, bo->child[i]->yc
                   ,bo->child[i]->d, bo->child[i]->h, bo->child[i]->w);
    */

    return 0;
}
int plots_in_three_columns(struct box *bo)
{
    int i;
    int  xcm[3] = {0}, wm[3] = {0};
    int x0, y0, x1, y1, dh, mh;
    struct box *b = NULL, *plot = NULL, *b1 = NULL;

    bo->h = 0;

    for (i = 0 ; i < 3 * (bo->n_box / 3) ; i++)
    {
        b = bo->child[i];
        x0 = 0;
        y0 = 0;
        plot = locate_box(b, QKD_B_PLOT, &x0, &x1, &y0, &y1, FIRST);

        if (plot != NULL)
        {
            xcm[i % 3] = (x0 > xcm[i % 3]) ? x0 : xcm[i % 3];
            wm[i % 3] = (b->w - x0 > wm[i % 3]) ? b->w - x0 : wm[i % 3];
        }
    }

    xcm[1] += xcm[0] + wm[0] + 100;
    xcm[2] += xcm[1] + wm[1] + 100;

    for (i = 0, dh = 0, mh = 0 ; i < (bo->n_box / 3) ; i++)
    {
        b = bo->child[3 * i];
        x0 = 0;
        y0 = 0;
        plot = locate_box(b, QKD_B_PLOT, &x0, &x1, &y0, &y1, FIRST);
        if (plot != NULL)
        {
            b->xc = xcm[0] - x0;
            dh = y0;
            mh = b->d + b->h - y0;
        }
        //win_printf("box %d yc %d, dh %d mh %d new yc=%d",3*i,b->yc,dh,mh,bo->h - y0);
        b->yc = bo->h - y0;

        b = bo->child[3 * i + 1];
        x0 = 0;
        y0 = 0;
        plot = locate_box(b, QKD_B_PLOT, &x0, &x1, &y0, &y1, FIRST);
        if (plot != NULL)
        {
            b->xc = xcm[1] - x0;
            dh = (y0 > dh) ? y0 : dh;
            mh = (b->d + b->h - y0 > mh) ? b->d + b->h - y0 : mh;
        }
        //b->yc = bo->h + dh - y0;
        //win_printf("box %d yc %d, dh %d mh %d new yc=%d",3*i+1,b->yc,dh,mh,bo->h - y0);
        b->yc = bo->h - y0;

        b = bo->child[3 * i + 2];
        x0 = 0;
        y0 = 0;
        plot = locate_box(b, QKD_B_PLOT, &x0, &x1, &y0, &y1, FIRST);
        if (plot != NULL)
        {
            b->xc = xcm[2] - x0;
            dh = (y0 > dh) ? y0 : dh;
            mh = (b->d + b->h - y0 > mh) ? b->d + b->h - y0 : mh;
        }
        b->yc = bo->h  + dh - y0;
        /*
        win_printf("box %d yc %d, dh %d mh %d new yc=%d\n"
                   "box %d yc %d\n"
                   "box %d yc %d\n"
                   ,3*i+2,b->yc,dh,mh,bo->h + dh - y0
                   ,3*i,bo->child[3 * i]->yc + dh
                   ,3*i+1,bo->child[3 * i+1]->yc + dh);
        */
        bo->child[3 * i]->yc += dh;
        bo->child[3 * i + 1]->yc += dh;
        bo->h += dh + mh + 100;
    }

    bo->w = xcm[2] + wm[2];

    if (bo->n_box > 3 && bo->n_box % 3 == 1)
    {
        b = bo->child[bo->n_box - 1];
        x0 = 0;
        y0 = 0;
        plot = locate_box(b, QKD_B_PLOT, &x0, &x1, &y0, &y1, FIRST);

        if (plot != NULL)
        {
            b->xc = xcm[1] - x0;
        }

        b->yc = bo->h;
        bo->w = (b->w > bo->w) ? b->w : bo->w;
        bo->h += b->h + 100;
    }

    if (bo->n_box > 3 && bo->n_box % 3 == 2)
    {
        b = bo->child[bo->n_box - 1];
        b1 = bo->child[bo->n_box - 2];
        x0 = 0;
        y0 = 0;
        plot = locate_box(b, QKD_B_PLOT, &x0, &x1, &y0, &y1, FIRST);

        if (plot != NULL)
        {
            dh = y0;
            mh = b->d + b->h - y0;
        }

        b->yc = bo->h - y0;
        x0 = 0;
        y0 = 0;
        plot = locate_box(b1, QKD_B_PLOT, &x0, &x1, &y0, &y1, FIRST);

        if (plot != NULL)
        {
            dh = (y0 > dh) ? y0 : dh;
            mh = (b1->d + b1->h - y0 > mh) ? b1->d + b1->h - y0 : mh;
        }

        b->yc = bo->h + dh - y0;
        b1->yc += dh;
        bo->h += dh + mh + 100;
        x0 = (bo->w - b->w - b1->w > 0) ? bo->w - b->w - b1->w : 0;
        b1->xc = x0 / 3;
        b->xc = b1->xc + b1->w + x0 / 3;
        bo->w = (b->w + b1->w + 2 * x0 / 3 > bo->w) ? b->w + b1->w + 2 * x0 / 3 : bo->w;
    }
    /*
    for (i = 0; i < bo->n_box ; i++)
        win_printf("box %d xc=%d yc=%d, h=%d d=%d w=%d\n"
                   ,i,bo->child[i]->xc, bo->child[i]->yc
                   ,bo->child[i]->d, bo->child[i]->h, bo->child[i]->w);
    */
    return 0;
}


# endif /* _PLOT_REG_GUI_C_ */
