#ifndef _GUI_DEF_C_
#define _GUI_DEF_C_


# include "ctype.h"
#include "platform.h"
# include "gui_def.h"
#include "xvin.h"

# include <stdlib.h>
# include <string.h>

int (*before_menu_proc) (int msg, DIALOG *d, int c);
int (*after_menu_proc) (int msg, DIALOG *d, int c);



/*	setsize_dybox(dybox *db, int x0, int y0, int x1, int y1);
 *	setaction_dybox(dybox *db, int type, int (*do)(char), void* parent)
 *	setsize_rec(rec* def, int x0, int y0, int width, int height);
 *	DESCRIPTION	
 *
 *	RETURNS		0 on success, 1 
 */
int	setsize_dybox(dybox *db, int x0, int y0, int x1, int y1)
{
	db->x0 = (x0 <= x1) ? x0 : x1;
	db->y0 = (y0 <= y1) ? y0 : y1;
	db->x1 = (x0 <= x1) ? x1 : x0;
	db->y1 = (y0 <= y1) ? y1 : y0;
	return 0;
}

int 	setsize_rec(rec* def, int x0, int y0, int width, int height)
{
	def->x0 = x0;
	def->y0 = y0;
	def->h = height;
	def->w =  width;
	return 0;
}


DIALOG* attach_new_item_to_dialog(int (*proc)(int, DIALOG *, int), int key, int flags, int d1, int d2)
{
  register int i;
  //	int was_active_dialog = 0;
  DIALOG* di = NULL;
  
  if (the_dialog == NULL)
    {
      m_the_dialog = 256;
      di = the_dialog = (DIALOG *)calloc(m_the_dialog,sizeof(DIALOG));
      if (di == NULL)	return NULL;
    }
  //	else was_active_dialog = (active_dialog == the_dialog) ? 1 : 0;
  for (i = 0; (the_dialog[i].proc != NULL) && (i < m_the_dialog-2); i++);
  if (i >= m_the_dialog-2) 
    {
      win_printf("Out of dialog!");
      return NULL;
    }
  
# ifdef PB_IF_the_dialog_IS_REGISTER	
  if (i >= m_the_dialog-32)
    {
      m_the_dialog += 64;
      di = (DIALOG *)realloc(the_dialog,m_the_dialog*sizeof(DIALOG));
      if (di == NULL)	return NULL;
      the_dialog = di;
      if (was_active_dialog)  active_dialog = the_dialog;
    }
# endif
  di = the_dialog + i;
  di->proc = proc;
  di->key = key;
  di->flags = flags;
  di->d1 = d1;
  di->d2 = d2;
  di->dp = NULL;
  di->dp2 = NULL;
  di->dp3 = NULL;	
  di->x = 0;
  di->y = 0;
  di->w = 0;
  di->h = 0;
  di->fg = 0;
  di->bg = 0;	
  
  /*	allegro_message("dialog alloc %d cell %d proc %d proc 2 %d",m_the_dialog,i,(the_dialog[i].proc != NULL)?1:0,(proc != NULL)?1:0);	*/
  the_dialog[i+1].proc = NULL;
  
  return di;
}
int set_dialog_size_and_color(DIALOG* di, int x, int y, int w, int h, int fg, int bg)
{
  if (di == NULL)	return 1;
  di->x = x;
  di->y = y;
  di->w = w;
  di->h = h;
  di->fg = fg;
  di->bg = bg;	
  return 0;
}
int set_dialog_properties(DIALOG* di, void* dp, void* dp2, void* *dp3)
{
  if (di == NULL)	return 1;
  di->dp = dp;
  di->dp2 = dp2;
  di->dp3 = dp3;
  return 0;
}

int remove_item_from_dialog(int (*proc)(int, DIALOG *, int), void* dp)
{
  register int i, ndi, j;
  int ret;
  
  if (the_dialog == NULL) return -1;
  for (ndi = 0; (ndi < m_the_dialog) && (the_dialog[ndi].proc != NULL); ndi++);
 ndi = (ndi < m_the_dialog) ? ndi : m_the_dialog - 1;
  if (dp != NULL)
    {
      for (i = 0; ((the_dialog[i].proc != proc) || (the_dialog[i].dp != dp)) 
	     && (i <= ndi); i++);
    }
  else
    {
      for (i = 0; (the_dialog[i].proc != proc) && (i <= ndi); i++);	
    }
  ret = (the_dialog[i].proc == NULL) ? -1 : i;
  for (j = i; (j < ndi) && ((j+1) < m_the_dialog); j++)
    the_dialog[j] = the_dialog[j+1];
  if (j < m_the_dialog) the_dialog[j].proc = NULL;
  return ret;
}

int remove_item_from_dialog_verbose(int (*proc)(int, DIALOG *, int), void* dp)
{
  register int i, ndi, j, k = 0;
  int ret;
  
  if (the_dialog == NULL) return -1;
  for (ndi = 0; (ndi < m_the_dialog) && (the_dialog[ndi].proc != NULL); ndi++);
  ndi = (ndi < m_the_dialog) ? ndi : m_the_dialog - 1;
  if (dp != NULL)
    {
      for (i = 0, k = 0; ((the_dialog[i].proc != proc) || (the_dialog[i].dp != dp)) 
	     && (i <= ndi); i++);
    }
  else
    {
      for (i = 0, k = 1; (the_dialog[i].proc != proc) && (i <= ndi); i++);	
    }
  ret = (the_dialog[i].proc == NULL) ? -1 : i;
  win_printf("Remodeling dialog : of %d �l�ments\n"
	     "starting at element %d\n"
	     "passed in loop %d",ndi,i,k);
  for (j = i; (j < ndi) && ((j+1) < m_the_dialog); j++)
    the_dialog[j] = the_dialog[j+1];
  if (j < m_the_dialog) the_dialog[j].proc = NULL;
  return ret;
}


int remove_short_cut_from_dialog(int key, int d1)
{
  register int i, ndi, j;
  int ret;
	
  if (the_dialog == NULL) return -1;
  for (ndi = 0; (ndi < m_the_dialog) && (the_dialog[ndi].proc != NULL); ndi++);
  ndi = (ndi < m_the_dialog) ? ndi : m_the_dialog - 1;
  for (i = 0; ((the_dialog[i].proc != d_keyboard_proc) || (the_dialog[i].key != key)
	       || (the_dialog[i].d1 != d1)) 
	 && (i <= ndi); i++);
  ret = (the_dialog[i].proc == NULL) ? -1 : i;
  for (j = i; (j < ndi) && ((j+1) < m_the_dialog); j++)
    the_dialog[j] = the_dialog[j+1];
  if (j < m_the_dialog) the_dialog[j].proc = NULL;
  return ret;
}

int remove_short_cut_from_dialog_verbose(int key, int d1)
{
  register int i, ndi, j;
  int ret;
	
  if (the_dialog == NULL) return -1;
  for (ndi = 0; (ndi < m_the_dialog) && (the_dialog[ndi].proc != NULL); ndi++);
  ndi = (ndi < m_the_dialog) ? ndi : m_the_dialog - 1;
  for (i = 0; ((the_dialog[i].proc != d_keyboard_proc) || (the_dialog[i].key != key)
	       || (the_dialog[i].d1 != d1)) 
	 && (i <= ndi); i++);
  ret = (the_dialog[i].proc == NULL) ? -1 : i;
  win_printf("Remodeling dialog by key : of %d �l�ments\n"
	     "starting at element %d\n"
	     "ret %d",ndi,i,ret);
  for (j = i; (j < ndi) && ((j+1) < m_the_dialog); j++)
    the_dialog[j] = the_dialog[j+1];
  if (j < m_the_dialog) the_dialog[j].proc = NULL;
  return ret;
}



DIALOG* retrive_short_cut_from_dialog(int key, int d1)
{
  register int i, ndi;
  
  if (the_dialog == NULL) return NULL;
  for (ndi = 0; (ndi < m_the_dialog) && (the_dialog[ndi].proc != NULL); ndi++);
  ndi = (ndi < m_the_dialog) ? ndi : m_the_dialog - 1;
  for (i = 0; ((the_dialog[i].proc != d_keyboard_proc) || (the_dialog[i].key != key)
	       || (the_dialog[i].d1 != d1)) 
	 && (i <= ndi); i++);
  return  (the_dialog[i].proc == NULL) ? the_dialog + i : NULL;
}

int retrive_short_cut_index_from_dialog(int key, int d1)
{
  register int i, ndi;
  
  if (the_dialog == NULL) return -1;
  for (ndi = 0; (ndi < m_the_dialog) && (the_dialog[ndi].proc != NULL); ndi++);
  ndi = (ndi < m_the_dialog) ? ndi : m_the_dialog - 1;
  for (i = 0; ((the_dialog[i].proc != d_keyboard_proc) || (the_dialog[i].key != key)
	       || (the_dialog[i].d1 != d1)) 
	 && (i <= ndi); i++);
  return  (the_dialog[i].proc == NULL) ? i : -1;
}

int count_keyboard_short_cut(void)
{
  register int i, ndi, j;
  
  if (the_dialog == NULL) return -1;
  for (ndi = 0; (ndi < m_the_dialog) && (the_dialog[ndi].proc != NULL); ndi++);
  ndi = (ndi < m_the_dialog) ? ndi : m_the_dialog - 1;
  for (i = j = 0; i <= ndi; i++)
    j = (the_dialog[i].proc == d_keyboard_proc) ? j+1 : j;
  return  j;
}



int remove_all_keyboard_proc(void)
{
  int i;
  DIALOG* di = NULL;
  //extern int	do_quit(void);
  
  for (i = 0;i >= 0; i = remove_item_from_dialog(d_keyboard_proc, NULL));
  di = attach_new_item_to_dialog(d_keyboard_proc, 27, 0, 0, 0);
  if (di != NULL)	set_dialog_properties(di, (void *) do_quit, NULL, NULL);	
  return 0;
}


int add_keyboard_short_cut(int key, int d1, int d2, int (*menu_proc)(void))
{
  int i, j;
  DIALOG* di = NULL;
  
  //for (j = -1, i = 0;i >= 0; i = remove_item_from_dialog(d_keyboard_proc, menu_proc)) j++;
  for (j = -1, i = 0;i >= 0; i = remove_short_cut_from_dialog(key, d1)) j++;
  di = attach_new_item_to_dialog(d_keyboard_proc, key, 0, d1, d2);
  if (di != NULL)	set_dialog_properties(di, (void *) menu_proc, NULL, NULL);	
  
  return (di != NULL) ? 0: 1;    
}

int add_keyboard_short_cut_verbose(int key, int d1, int d2, int (*menu_proc)(void))
{
  int i, j;
  DIALOG* di = NULL;
  
  //for (j = -1, i = 0;i >= 0; i = remove_item_from_dialog_verbose(d_keyboard_proc, menu_proc)) j++;
  for (j = -1, i = 0;i >= 0; i = remove_short_cut_from_dialog_verbose(key, d1)) j++;


  win_printf("%d item removed from dialog",j);
  di = attach_new_item_to_dialog(d_keyboard_proc, key, 0, d1, d2);
  if (di != NULL)	set_dialog_properties(di, (void *) menu_proc, NULL, NULL);	
  else win_printf("short cut %d, %d not added",key,d1);
  return (di != NULL) ? 0: 1;    
}



int retrieve_item_from_dialog(int (*proc)(int, DIALOG *, int), void* dp)
{
  register int i;
  
  if (the_dialog == NULL) return -1;
  for (i = 0; (the_dialog[i].proc != NULL) && (i < m_the_dialog); i++)
    {
      if (dp == NULL)
	{
	  if (the_dialog[i].proc == proc) break;
	}
      else
	{
	  if ((the_dialog[i].proc == proc) && (the_dialog[i].dp == dp)) break;
	}
    }
  return (the_dialog[i].proc == NULL) ? -1 : i;
}

int xvin_d_menu_proc(int msg, DIALOG *d, int c)
{
  int ret;

  if (before_menu_proc != NULL) before_menu_proc(msg, d, c);
  ret = d_menu_proc(msg, d, c);
  if (after_menu_proc != NULL) after_menu_proc(msg, d, c);
  return ret;
}

int retrieve_index_of_menu_from_dialog(void)
{ 
  int i;
  i = retrieve_item_from_dialog(xvin_d_menu_proc, NULL);
  if (i >= 0) return i;
  return retrieve_item_from_dialog(d_menu_proc, NULL);
}
int	do_update_menu(void)
{
  DIALOG* di = NULL;
  int i;
  if (active_dialog != the_dialog) return 1;
  i = retrieve_item_from_dialog(xvin_d_menu_proc, NULL);
  if (i < 0)	return 1;
  di = the_dialog + i;
  if (di != NULL && di->dp != NULL)
    {
      remove_all_keyboard_proc();	
      scan_and_update((MENU*)di->dp);
    }
  else return 1;
  return 0;
}
int number_of_item_from_dialog(void)
{
  register int i;
  
  for (i = 0; (i < m_the_dialog) && (the_dialog[i].proc != NULL); i++);
  return i;
}

XV_FUNC(int, do_save_bin, (void));

int	scan_and_update(MENU* mn)
{
  register int i;
  int imn[32] = {0}, imnh = 0, ct;
  MENU *mnc = NULL, *active_menu_bk = NULL;
  MENU *mnk[32] = {0};
  DIALOG* di = NULL;
  char *sc = NULL, *tab = NULL;
  char debug[256] = {0};
  
  if (mn == NULL)	return -1;
  
  active_menu_bk = active_menu;
  updating_menu_state = 1;
  
  for (i = 0; i < 32; i++)	
    {
      imn[i] = 0;
      mnk[i] = NULL;
    }
  for(mnc = mnk[0] = mn, imnh = 0; imnh >= 0 && imnh < 32; )
    {
      //	win_printf("loop hic %d item %d",imnh,imn[imnh]);
      if ((imnh < 30) && (imn[imnh] < 31) && (mnc[imn[imnh]].child != NULL)) /* a sub menu */
	{
	  // win_printf("Entering sub menu %d of hic %d\n%s",imn[imnh],imnh+1,mnc[imn[imnh]].text);
	  // dump_to_xv_log_file_with_time ("Entering sub menu %d of hic %d\n%s",imn[imnh],imnh+1,mnc[imn[imnh]].text);
	  mnc = mnk[imnh+1] = mnc[imn[imnh]].child;
	  imnh++;
	  //	imn[imnh] = (imn[imnh] == 0) ? -1 : imn[imnh];
	}
      else if ((imn[imnh] < 32) && (mnc[imn[imnh]].text == NULL))
	{

	  // win_printf("leaving sub menu %d of hic %d",imn[imnh],imnh);
	  // dump_to_xv_log_file_with_time("leaving sub menu %d of hic %d",imn[imnh],imnh);
	  imn[imnh] = 0;		
	  imnh--;
	  if (imnh >= 0) mnc = mnk[imnh];
	  if (imnh >= 0)  imn[imnh]++;
	}
      else
	{
	  active_menu = &mnc[imn[imnh]];
	  if (active_menu->text != NULL) strncpy(debug,active_menu->text,sizeof(debug));
	  else debug[0] = 0;
	  tab = strstr(debug,"\t");
	  if (tab != NULL)	tab[0] = ' ';
	  //win_printf("dealing menu item \n%s\n%d of hic %d",debug,imn[imnh],imnh);   
	  //dump_to_xv_log_file_with_time("dealing menu item \n%s\n%d of hic %d",debug,imn[imnh],imnh);*/
	  if (active_menu->proc != NULL && active_menu->text != NULL) active_menu->proc();
	  
	  tab = (active_menu->text) ? strstr(active_menu->text,"\t") : NULL;
	  if (tab != NULL && tab[1] != 0 && active_menu->proc != NULL) 
	    {	/* we have a short cut */
	      tab++;
	      sc = strstr(tab,"Ctrl-");
	      di = NULL;
	      if (sc != NULL && sc[5] != 0)
		{
		  ct = sc[5]-'A'+1;
		  di = attach_new_item_to_dialog(d_keyboard_proc, ct, 0, 0, 0);
		  if (di != NULL) 
		    set_dialog_properties(di, (void *) active_menu->proc, NULL, NULL);
		  /*						
		else win_printf("pb attaching keyboard short cut\n%s ->%d",sc,ct);	
		win_printf("attaching keyboard short cut\n%s ->%d\nis do save %d",sc,ct,(active_menu->proc == do_save_bin)?1:0);		*/
	  //	dump_to_xv_log_file_with_time("attaching keyboard short cut\n%s ->%d\nis do save %d",sc,ct,(active_menu->proc == do_save_bin)?1:0);	
		}
	   /*  else 
	       {
	          di = attach_new_item_to_dialog(d_keyboard_proc, tab[1], 0, 0, 0);
		  win_printf("attaching keyboard short cut\n%s",tab+1);	
	       }
	       if (di != NULL)
	       set_dialog_properties(di, mnc[imn[imnh]].proc, NULL, NULL);
	   */
	    }			
	  if (imnh >= 0)  imn[imnh]++;
	}
      
    }
  active_menu = active_menu_bk;   
  updating_menu_state = 0;
  return 0;
}

DIALOG *get_the_dialog(void)
{
    return the_dialog;
}



# endif  /* _GUI_DEF_C_ */
