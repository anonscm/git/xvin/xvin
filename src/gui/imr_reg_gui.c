#ifndef _IM_REG_GUI_C_
#define _IM_REG_GUI_C_

#include "config.h"

#include "platform.h"

#include "allegro.h"
# ifdef XV_WIN32
# include "winalleg.h"
# endif

# include "plot_reg_gui.h"
# include "box2alfn.h"
# include "dev_pcx.h"
# include "op2box.h"
# include "plot2box.h"



# include "xvin.h"

# include <stdlib.h>
# include <string.h>
# include <alfont.h>

int        (*image_special_display)(imreg *imr);
int        (*vert_marker_action)(O_i *oi, int n_line, DIALOG *d);
int        (*horz_marker_action)(O_i *oi, int n_row, DIALOG *d);
int        (*vert_marker_drag_action)(O_i *oi, int n_line, DIALOG *d);
int        (*horz_marker_drag_action)(O_i *oi, int n_row, DIALOG *d);


int        (*prev_general_im_action)(imreg *imr, int x_0, int y_0, int mode);
int        (*general_im_action)(imreg *imr, int x0, int y0, int mode);


int        (*image_wheel_action)(imreg *pr, O_i *oi, DIALOG *d, int ticks);



int opoi_mark_h = 40;
int opoi_mark_w = 25;
int oi_mark_w = 120;
int oi_mark_h = 16;
O_i *oi_copied = NULL;
int smooth_interpol = 1; // do linear interpollation to display screen pixels

int z_x_avg_profile_size = 4;
int z_y_avg_profile_size = 4;




int image_show(void);
int    set_point_value(void);
int    set_point_to_plot(void);
int    do_tilted_profile(void);
int restore_image_parameter(imreg *imr,  DIALOG *d);
int    set_z_profile_on_point(void);
int    set_z_avg_profile_on_point(void);
int find_z_profile_on_point_by_menu(void);

int    draw_bubble(BITMAP* b, int mode, int x, int y, char *format, ...)
{
    int prev;
    static char *c = NULL;

    va_list ap;
    if (c == NULL)
        c = (char *)calloc(2048,sizeof(char));
    if (c == NULL)
        return 1;
    va_start(ap, format);
    vsnprintf(c,2048,format,ap);
    va_end(ap);
    if (user_font == NULL)
    {
        ttf_init();
    }
    prev = alfont_text_mode(makecol(0, 0, 0));
    alfont_set_font_size(user_font, normalsize_font_height);

    y -= alfont_text_height(user_font)/2;
    /*    b = uconvert(mes, U_ASCII, buf, U_UTF8, 256);
    */
    /* output in the middle of the screen, upper part, without antialiasing */

# ifdef XV_WIN32
    if (win_get_window() == GetForegroundWindow())
# endif
    {
        //if (!screen_used)
        //{
        //  screen_used = 1;
      for(;screen_acquired;); // we wait for screen_acquired back to 0;
      acquire_bitmap(screen);
      screen_acquired = 1;
      if (mode == B_RIGHT)
	alfont_textout_right(b, user_font, c, x, y, makecol(255, 255, 0));

      else if (mode == B_LEFT)
	alfont_textout(b, user_font, c, x, y, makecol(255, 255, 0));

      else       alfont_textout_centre(b, user_font, c, x, y, makecol(255, 255, 0));

      release_bitmap(screen);
      screen_acquired = 0;

        //  screen_used = 0;
        //}
    }
    alfont_text_mode(prev);
    return 0;
}




imreg *find_imr_in_current_dialog(DIALOG *di)
{
    int i;
    imreg *imr = NULL;

    if (di == NULL) di = active_dialog;
    for (i = 0, imr = NULL; di[i].proc != NULL ; i++)
    {
        if (di[i].proc == d_draw_Im_proc)
        {
            if (imr == NULL)    imr = (imreg *)di[i].dp;
            else if (di->flags & D_SELECTED) imr = (imreg *) di[i].dp;
        }
    }
    return imr;
}


DIALOG *find_dialog_associated_to_imr(imreg *imr, DIALOG *di)
{
    int i;

    if (di == NULL) di = active_dialog;
    for (i = 0; (di[i].proc != NULL) && (i < 256); i++)
    {
        if (di[i].proc == d_draw_Im_proc)
        {
            if (imr == di[i].dp)    return di+i;
        }
    }
    if (i >= 256)   allegro_message("Hi dialog index %d",i);
    return NULL;
}


int find_imr_index_in_current_dialog(DIALOG *di)
{
    int i, j;
    imreg *imr = NULL;

    if (di == NULL) di = active_dialog;
    for (i = 0, imr = NULL, j = -1; di[i].proc != NULL ; i++)
    {
        if (di[i].proc == d_draw_Im_proc)
        {
            if (imr == NULL)
            {
                imr = (imreg *) di[i].dp;
                j = i;
            }
            else
            {
                if (di->flags & D_SELECTED)
                {
                    imr = (imreg *) di[i].dp;
                    j = i;
                }
            }
        }
    }
    return j;
}

DIALOG *attach_new_image_region_to_dialog(int key, int flags, int d1, int d2)
{
    return attach_new_item_to_dialog(d_draw_Im_proc, key, flags, d1, d2);
}

int re_attach_image_region_to_dialog (DIALOG *di, imreg *imr)
{
    if (imr == NULL || di == NULL) return 1;
    di->dp = imr;
    di->proc = d_draw_Im_proc;
    return 0;
}


int    im_menu_kill_op(void)
{
    imreg *imr = NULL;
    int i;

    if(updating_menu_state != 0)    return D_O_K;
    imr = find_imr_in_current_dialog(NULL);

    if (imr == NULL)    return win_printf_OK("Could not find image data");

    i = win_printf("Do you really want to kill\nthe %d data set ?",
                   imr->one_i->cur_op);
    if (i == WIN_CANCEL)    return 0;

    remove_from_one_image (imr->one_i, IS_ONE_PLOT,
                           (void *)imr->one_i->o_p[imr->one_i->cur_op]);


    return D_REDRAWME;
}


int    imop_show(void)
{
    imreg *imr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;

    if(updating_menu_state != 0)    return D_O_K;

    imr = find_imr_in_current_dialog(NULL);
    if (imr == NULL)    return win_printf_OK("Could not find image data");
    op = imr->one_i->o_p[imr->one_i->cur_op];
    ds = op->dat[op->cur_dat];

    win_printf("Plot %d of image %s"
               "Data set %d with %d points created {\\color{yellow}%s}\n"
               "{\\color{yellow}Source :}\n %s\n"
               "{\\color{yellow}Treatement :}\n %s\n"
               "{\\color{yellow}History :}\n %s\n",
               imr->one_i->cur_op, imr->one_i->filename,
               op->cur_dat,ds->nx,ctime(&(ds->time)),
               (ds->source != NULL) ? ds->source : "No source",
               (ds->treatement != NULL) ? ds->treatement : "No treatement",
               (ds->history != NULL) ? ds->history : "No history");
    return 0;
}


int copy_plot_to_new_pr(void)
{
    pltreg *pr = NULL;
    imreg *imr = NULL;
    O_p *opi = NULL, *op = NULL;

    imr = find_imr_in_current_dialog(NULL);
    if(updating_menu_state != 0)
    {
        if ((imr == NULL) || (imr->one_i->cur_op < 0))
        {
            active_menu->flags |=  D_DISABLED;
            return D_O_K;
        }
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }
    if (imr == NULL)
    {
        win_printf("cannot find image region");
        return D_O_K;
    }
    if (imr->one_i->cur_op < 0)         return D_O_K;
    opi = imr->one_i->o_p[imr->one_i->cur_op];

    pr = create_and_register_new_plot_project(0,   32,  900,  668);
    if (pr == NULL)
    {
        win_printf("cannot create plot region");
        return D_O_K;
    }
    op = duplicate_plot_data(opi, NULL);
    if (op == NULL)     return D_O_K;
    add_data(pr, IS_ONE_PLOT, (void*)op);
    remove_data_from_pltreg(pr,IS_ONE_PLOT,(void*)pr->o_p[0]);
    pr->cur_op = pr->n_op-1;
    pr->one_p = pr->o_p[pr->cur_op];
    pr->stack = &(pr->one_p->bplt);
    pr->one_p->need_to_refresh = 1;
    return broadcast_dialog_message(MSG_DRAW,0);
}

int    switch_implot_ds(void)
{
    int i;
    imreg *imr = NULL;
    int cdat;
    O_p *op = NULL;
    char message[512] = {0};

    imr = find_imr_in_current_dialog(NULL);
    if(updating_menu_state != 0)
    {
        if (imr == NULL)
        {
            active_menu->flags |=  D_DISABLED;
            return D_O_K;
        }
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    op = find_oi_cur_op(imr->one_i);
    if (op == NULL)  return D_O_K;
    cdat = op->cur_dat;
    snprintf(message,sizeof(message),"Select active data set in [0,%d]\n"
             "new data set %%8d\n",op->n_dat-1);
    i = win_scanf(message,&cdat);
    if (i == WIN_CANCEL) return 0;
    cdat = (cdat < 0) ? 0 : cdat;
    cdat = (cdat < op->n_dat) ? cdat : op->n_dat-1;
    op->cur_dat = cdat;
    return D_REDRAWME;
}


MENU imop_menu[32] =
{
    { "Kill plot",    im_menu_kill_op,       NULL,       0, NULL  },
        { "show plot",    imop_show,               NULL,       0, NULL  },
        { "copy to Plot region",    copy_plot_to_new_pr,      NULL,       0, NULL  },
        { "line color",      NULL,       line_color_menu,       0, NULL  },
        { "Select ds",    switch_implot_ds,  NULL,       0, NULL  },
        { NULL,                    NULL,             NULL,       0, NULL  }
};



int    do_imop_menu(int item)
{
  (void)item;
  return broadcast_dialog_message(MSG_DRAW,0);
}

int    oi_menu_kill_im(void)
{
    imreg *imr = NULL;
    int i;

    if(updating_menu_state != 0)    return D_O_K;
    imr = find_imr_in_current_dialog(NULL);

    if (imr == NULL)    return win_printf_OK("Could not find image data");
    i = win_printf("Do you really want to kill\nthe %d image ?",
                   imr->cur_oi);
    if (i == WIN_CANCEL)    return 0;

    remove_from_image (imr, IS_ONE_IMAGE, (void *)imr->one_i);


    return D_REDRAWME;

}



MENU oi_menu[32] =
{
    { "image properties",    image_show,       NULL,       0, NULL  },
        { "Kill image",    oi_menu_kill_im,       NULL,       0, NULL  },
        { "Horizontal profile",    set_profile,      NULL,       0, (void *) "line profile"  },
        { "vertical profile",    set_profile,      NULL,       0, (void *) "row profile"  },
        { "point value",    set_point_value,      NULL,       0, NULL  },
        { "point to plot",    set_point_to_plot,      NULL,       0, NULL  },
        { "point Z profile",    set_z_profile_on_point,      NULL,       0, NULL  },
        { "point Z profile x,y asked",    find_z_profile_on_point_by_menu,      NULL,       0, NULL  },
        { "point Z avg. profile",    set_z_avg_profile_on_point,      NULL,       0, NULL  },
        { "Tilted profile",   do_tilted_profile,      NULL,       0, NULL  },
        { NULL,                    NULL,             NULL,       0, NULL  }
};


int    do_oi_menu(int item)
{  // utility to be checked vc
    //imreg *imr;

    //imr = find_imr_in_current_dialog(NULL);
  (void)item;
  return broadcast_dialog_message(MSG_DRAW,0);
}


MENU im_y_axis_label_menu[32] =
{
    { "Change Decade",      NULL,       NULL,       0, NULL  },
        { "Modify Units",      NULL,       NULL,       0, NULL  },
        { "New Units",      NULL,       NULL,       0, NULL  },
        { NULL,                    NULL,             NULL,       0, NULL  }
};


int do_im_y_axis_label_menu(int item)
{
    mn_index = Y_AXIS;
    if (item == 0)        return modify_un_decade();
    if (item == 1)        return modify_units();
    if (item == 2)        return add_new_units();
    else return 0;
}


int build_and_display_im_y_axis_menu(imreg *imr, O_i *oi)
{
    int i, j, item;
    char s[256] = {0};
    DIALOG *di = NULL;

    i = find_dialog_focus(active_dialog);
    di = (i >= 0) ? &active_dialog[i] : NULL;

    for (i = 0; i < oi->n_yu && i < 26; i++)
    {
        snprintf(s,256,"  %s",(oi->yu[i]->name != NULL) ? oi->yu[i]->name : "no name");
        im_y_axis_label_menu[i].text = strdup(s);
        // we must check if image use user unit ! to be done
        im_y_axis_label_menu[i].flags = (i == oi->c_yu)? D_SELECTED : 0;
    }
    im_y_axis_label_menu[i++].text = strdup("New Units");
    im_y_axis_label_menu[i++].text = strdup("Modify Units");
    im_y_axis_label_menu[i++].text = strdup("Change Decade");
    snprintf(s,256,"remove %s",(oi->yu[oi->c_yu]->name != NULL) ?
             oi->yu[oi->c_yu]->name : "no name");
    im_y_axis_label_menu[i++].text = strdup(s);
    im_y_axis_label_menu[i++].text = NULL;

    item = do_menu(im_y_axis_label_menu,mouse_x,mouse_y);
    for (j = 0; j < i; j++)
    {
        if (im_y_axis_label_menu[j].text != NULL) free(im_y_axis_label_menu[j].text);
        im_y_axis_label_menu[j].text = NULL;
    }
    if (item < 0)    return D_O_K;
    mn_index = Y_AXIS;
    if (item < oi->n_yu)
    {
        set_im_y_unit_set(imr, item);
        if (di != NULL)        return object_message(di,MSG_DRAW,0);
        else    return broadcast_dialog_message(MSG_DRAW,0);
        /* broadcast_dialog_message(MSG_DRAW,0); */
    }
    else if (item == oi->n_yu)
    {
        //      win_printf("item %d add new units nyu %d",item,oi->n_yu);
        add_new_units();
        di = find_dialog_associated_to_imr(imr, NULL);
        if (di) di->proc(MSG_DRAW, di, 0);
        else broadcast_dialog_message(MSG_DRAW,0);
        return D_O_K;
    }
    else if (item == oi->n_yu + 1)
    {
        //win_printf("item %d modify units nyu %d",item,oi->n_yu);
        modify_units();
        di = find_dialog_associated_to_imr(imr, NULL);
        if (di) di->proc(MSG_DRAW, di, 0);
        else broadcast_dialog_message(MSG_DRAW,0);
        return D_O_K;
    }
    else if (item == oi->n_yu + 2)
    {
        //win_printf("item %d modify decade  nyu %d",item,oi->n_yu);
        modify_un_decade();
        di = find_dialog_associated_to_imr(imr, NULL);
        if (di) di->proc(MSG_DRAW, di, 0);
        else broadcast_dialog_message(MSG_DRAW,0);
        return D_O_K;
    }
    else if (item == oi->n_yu + 3)
    {
        remove_from_one_image (oi, IS_Y_UNIT_SET, (void *)oi->yu[oi->c_yu]);
        set_im_y_unit_set(imr, oi->c_yu);
        di = find_dialog_associated_to_imr(imr, NULL);
        if (di) di->proc(MSG_DRAW, di, 0);
        else broadcast_dialog_message(MSG_DRAW,0);
        return D_O_K;
    }
    else return D_O_K;
}


int build_and_display_im_y_prime_axis_menu(imreg *imr, O_i *oi)
{
    int i, j, item;
    char s[256] = {0};
    DIALOG *di = NULL;

    i = find_dialog_focus(active_dialog);
    di = (i >= 0) ? &active_dialog[i] : NULL;

    for (i = 0; i < oi->n_yu && i < 26; i++)
    {
        snprintf(s,256,"  %s",(oi->yu[i]->name != NULL) ? oi->yu[i]->name : "no name");
        im_y_axis_label_menu[i].text = strdup(s);
        im_y_axis_label_menu[i].flags = (i == oi->c_yu)? D_SELECTED : 0;
    }
    im_y_axis_label_menu[i++].text = strdup("New Units");
    im_y_axis_label_menu[i++].text = strdup("Modify Units");
    im_y_axis_label_menu[i++].text = strdup("Change Decade");
    snprintf(s,256,"remove %s",(oi->yu[oi->c_yu]->name != NULL) ?
             oi->yu[oi->c_yu]->name : "no name");
    im_y_axis_label_menu[i++].text = strdup(s);
    im_y_axis_label_menu[i++].text = NULL;

    item = do_menu(im_y_axis_label_menu,mouse_x,mouse_y);
    for (j = 0; j < i; j++)
    {
        if (im_y_axis_label_menu[j].text != NULL) free(im_y_axis_label_menu[j].text);
        im_y_axis_label_menu[j].text = NULL;
    }
    if (item < 0)    return D_O_K;
    mn_index = Y_AXIS;
    if (item < oi->n_yu)
    {
        set_im_y_unit_set(imr, item);
        if (di != NULL)        return object_message(di,MSG_DRAW,0);
        else    return broadcast_dialog_message(MSG_DRAW,0);
        /* broadcast_dialog_message(MSG_DRAW,0); */
    }
    else if (item == oi->n_yu)
    {
        //win_printf("item %d add new units nyu %d",item,oi->n_yu);
        add_new_units();
        di = find_dialog_associated_to_imr(imr, NULL);
        if (di) di->proc(MSG_DRAW, di, 0);
        else broadcast_dialog_message(MSG_DRAW,0);
        return D_O_K;
    }
    else if (item == oi->n_yu + 1)
    {
        //win_printf("item %d modify units nyu %d",item,oi->n_yu);
        modify_units();
        di = find_dialog_associated_to_imr(imr, NULL);
        if (di) di->proc(MSG_DRAW, di, 0);
        else broadcast_dialog_message(MSG_DRAW,0);
        return D_O_K;
    }
    else if (item == oi->n_yu + 2)
    {
        //win_printf("item %d modify decade  nyu %d",item,oi->n_yu);
        modify_un_decade();
        di = find_dialog_associated_to_imr(imr, NULL);
        if (di) di->proc(MSG_DRAW, di, 0);
        else broadcast_dialog_message(MSG_DRAW,0);
        return D_O_K;
    }
    else if (item == oi->n_yu + 3)
    {
        remove_from_one_image (oi, IS_Y_UNIT_SET, (void *)oi->yu[oi->c_yu]);
        set_im_y_unit_set(imr, oi->c_yu);
        di = find_dialog_associated_to_imr(imr, NULL);
        if (di) di->proc(MSG_DRAW, di, 0);
        else broadcast_dialog_message(MSG_DRAW,0);
        return D_O_K;
    }
    else return D_O_K;
}


MENU im_x_axis_label_menu[32] =
{
    { "Change Decade",      NULL,       NULL,       0, NULL  },
        { "Modify Units",      NULL,       NULL,       0, NULL  },
        { "New Units",      NULL,       NULL,       0, NULL  },
        { NULL,                    NULL,             NULL,       0, NULL  }
};


int do_im_x_axis_label_menu(int item)
{
    mn_index = X_AXIS;
    if (item == 0)        return modify_un_decade();
    if (item == 1)        return modify_units();
    if (item == 2)        return add_new_units();
    else return 0;
}

int build_and_display_im_x_axis_menu(imreg *imr, O_i *oi)
{
    int i, j, item;
    char s[256] = {0};
    DIALOG *di = NULL;

    for (i = 0; i < oi->n_xu && i < 26; i++)
    {
        snprintf(s,256,"%s",(oi->xu[i]->name != NULL) ? oi->xu[i]->name : "no name");
        im_x_axis_label_menu[i].text = strdup(s);
        im_x_axis_label_menu[i].flags = (i == oi->c_xu)? D_SELECTED : 0;                }
    im_x_axis_label_menu[i++].text = strdup("New Units");
    im_x_axis_label_menu[i++].text = strdup("Modify Units");
    im_x_axis_label_menu[i++].text = strdup("Change Decade");
    snprintf(s,256,"remove %s",(oi->xu[oi->c_xu]->name != NULL) ?
             oi->xu[oi->c_xu]->name : "no name");
    im_x_axis_label_menu[i++].text = strdup(s);
    im_x_axis_label_menu[i++].text = NULL;

    item = do_menu(im_x_axis_label_menu,mouse_x,mouse_y);
    for (j = 0; j < i; j++)
    {
        if (im_x_axis_label_menu[j].text != NULL) free(im_x_axis_label_menu[j].text);
        im_x_axis_label_menu[j].text = NULL;
    }
    if (item < 0)    return D_O_K;
    mn_index = X_AXIS;
    if (item < oi->n_xu)
    {
        set_im_x_unit_set(imr, item);
        di = find_dialog_associated_to_imr(imr, NULL);
        if (di) di->proc(MSG_DRAW, di, 0);
        else broadcast_dialog_message(MSG_DRAW,0);
        return D_O_K;
        //        return D_REDRAWME;
    }
    else if (item == oi->n_xu)
    {
        add_new_units();
        di = find_dialog_associated_to_imr(imr, NULL);
        if (di) di->proc(MSG_DRAW, di, 0);
        else broadcast_dialog_message(MSG_DRAW,0);
        return D_O_K;
    }
    else if (item == oi->n_xu + 1)
    {
        modify_units();
        di = find_dialog_associated_to_imr(imr, NULL);
        if (di) di->proc(MSG_DRAW, di, 0);
        else broadcast_dialog_message(MSG_DRAW,0);
        return D_O_K;
        //        return broadcast_dialog_message(MSG_DRAW,0);
    }
    else if (item == oi->n_xu + 2)
    {
        modify_un_decade();
        return broadcast_dialog_message(MSG_DRAW,0);
    }
    else if (item == oi->n_xu + 3)
    {
        remove_from_one_image (oi, IS_X_UNIT_SET, (void *)oi->xu[oi->c_xu]);
        set_im_x_unit_set(imr, oi->c_xu);
        di = find_dialog_associated_to_imr(imr, NULL);
        if (di) di->proc(MSG_DRAW, di, 0);
        else broadcast_dialog_message(MSG_DRAW,0);
        return D_O_K;
        //        return broadcast_dialog_message(MSG_DRAW,0);
        //return D_REDRAWME;
    }
    else return D_O_K;
}


int    edit_im_xlabel(O_i *oi, int c_lab)
{
    int i, j, k;

    for(i = k = -1, j = 0 ; j < oi->n_lab && i < c_lab; j++)
    {
        if (oi->lab[j]->type == USR_COORD || oi->lab[j]->type == ABS_COORD)
            i++;
        if (i == c_lab) k = j;
    }
    if (i == c_lab)
    {
        TeX_modify(oi->lab[k]->text);
        if (last_answer != NULL)
        {
            if (oi->lab[k]->text) free(oi->lab[k]->text);
            oi->lab[k]->text = strdup(last_answer);
        }
        else remove_label_from_oi(oi, oi->lab[k]);
        oi->need_to_refresh |= PLOTS_NEED_REFRESH;
    }
    return D_REDRAWME;
}
int    edit_im_ylabel(O_i *oi, int c_lab)
{
    int i, j, k;

    for(i = k = -1, j = 0 ; j < oi->n_lab && i < c_lab; j++)
    {
        if (oi->lab[j]->type == VERT_LABEL_USR
            || oi->lab[j]->type == VERT_LABEL_ABS)
            i++;
        if (i == c_lab) k = j;
    }
    if (i == c_lab)
    {
        TeX_modify(oi->lab[k]->text);
        if (last_answer != NULL)
        {
            if (oi->lab[k]->text)  free(oi->lab[k]->text);
            oi->lab[k]->text = strdup(last_answer);
        }
        else remove_label_from_oi(oi, oi->lab[k]);
        oi->need_to_refresh |= PLOTS_NEED_REFRESH;
    }
    return D_REDRAWME;
}

int        move_im_label(struct box *xt, struct box *b, int sc, BITMAP *plt_bufferl,
                         DIALOG *d, imreg *imr, int xm_s, int ym_s, int x, int y)
{
    int i;
    int  xts, yts;
    int dx, dxp, dy, dyp, i_label;
    O_i *oi = NULL;
    p_l *pl = NULL;

    (void)b;
    xts = xt->xc; yts = xt->yc;
    dx = dxp = dy = dyp =0;
    while (mouse_b & 0x3)
    {
        dx = mouse_x - xm_s;
        dy = mouse_y - ym_s;
        if (dx != dxp || dy != dyp)
        {
            xt->xc = xts + (dx * sc)/2;
            xt->yc = yts - (dy * sc)/2;
            write_and_blit_im_box( plt_bufferl, d, imr);
            dxp = dx;
            dyp = dy;

        }

    }

    oi = imr->one_i;
    for (i = 0, i_label = -1;((i < oi->n_lab) && (i_label == -1)); i++)
        i_label = (xt == (void*)oi->lab[i]->b) ? i : -1;
    if (i_label == -1)             return D_O_K;

    pl = oi->lab[i_label];


    y = y_imdata_2_imr(imr, pl->yla);
    y = SCREEN_H - y;
    y -= dy;
    pl->yla = y_imr_2_imdata(imr, SCREEN_H - y);

    x = (x*2)/sc;
    x += dx;
    pl->xla = x_imr_2_imdata(imr, x);
    oi->need_to_refresh |= PLOT_NEED_REFRESH;
    return D_REDRAWME;
}

int   select_image_wheel_action(imreg *imr, O_i *oi, DIALOG *d, int ticks);


O_i *convert_bitmap_to_oi(BITMAP *bmp, O_i *dest, int x0, int y0, int x1, int y1)
{
        int iy, ix, color_value, bpp, i, j;
      O_i *oid = NULL;



      if (bmp == NULL || x1 <= x0 || y1 <= y0 || bmp->w <= 0 || bmp-> h <= 0) return NULL;
      x0 = (x0 < 0) ? 0 : x0;
      y0 = (y0 < 0) ? 0 : y0;
      x1 = (x1 <= bmp->w) ? x1 : bmp->w;
      y1 = (y1 <= bmp->h) ? y1 : bmp->h;
      bpp = bitmap_color_depth(bmp);
      if (dest == NULL)
         {
            oid = create_one_image(x1-x0, y1-y0, IS_RGB_PICTURE);
            if (oid == NULL) return NULL;
         }
      else if (dest->im.data_type != IS_RGB_PICTURE) return NULL;
      else oid = dest;
      acquire_bitmap(bmp);
      for (iy = y1-1, i = 0; iy >= y0 && i < oid->im.ny; iy--, i++)
         {
             for (ix = x0, j = 0; ix < x1 && j < oid->im.nx; ix++, j++)
                {
                    color_value = getpixel(bmp, ix, iy);
                    oid->im.pixel[i].rgb[j].r = getr_depth(bpp, color_value);
                    oid->im.pixel[i].rgb[j].g = getg_depth(bpp, color_value);
                    oid->im.pixel[i].rgb[j].b = getb_depth(bpp, color_value);
                }
         }
      release_bitmap(bmp);
      return oid;
}

int d_draw_Im_proc(int msg, DIALOG *d, int c)
{
    int i, k;
    struct box *b = NULL, *xt = NULL;
    imreg* imr = NULL;
    O_i *oi = NULL, *oic = NULL;
    O_p  *opc = NULL, *opd = NULL;
    //d_s  *dsc = NULL; // *ds,
    int sc, cl, color, n_line, n_row;
    int x = 0, u = 0, y, v, y_off = 0, xts, yts;
    int xm_s, ym_s, dx = 0, dxp, dy = 0, dyp, n_lab;
    int xb, yb;
    BITMAP *ds_bitmap = NULL;
    //extern BITMAP *dev_bitmap;
    BITMAP *plt_bufferl = get_plt_buffer();
    DIALOG *di_mn = NULL;
    char buf[1024] = {0};
    int n_opoid, i_opoid, n_oid, i_oid, c_oid, c_opoid;
    static int   idle_event = 0, last_idle_event = 1, idle_display = 0;
    static int screen_w = 0;
    static clock_t idle_time = 0;

    if (dev_bitmap != NULL) y_off = dev_bitmap->h;


    if (screen_w != SCREEN_W)
      {
	opoi_mark_h = (int)(40 * sqrt((float)SCREEN_W/1280));
	opoi_mark_w = (int)(25 * sqrt((float)SCREEN_W/1280));
	oi_mark_w = (int)(120 * sqrt((float)SCREEN_W/1280));
	oi_mark_h = (int)(16 * sqrt((float)SCREEN_W/1280));
	opoi_mark_h = (opoi_mark_h < 10) ? 40 : opoi_mark_h;
	opoi_mark_w = (opoi_mark_w < 7) ? 25 : opoi_mark_w;
	oi_mark_w = (oi_mark_w < 60) ? 120 : oi_mark_w;
	oi_mark_h = (oi_mark_h < 7) ? 16 : oi_mark_h;
	screen_w = SCREEN_W;
	screen_w = (screen_w < 640) ? 640 : screen_w;
      }

    image_wheel_action = select_image_wheel_action;

    if (d->dp == NULL)    return D_O_K;
    imr = (imreg*)d->dp;        /* the image region is here */
    oi = imr->one_i;         /* the plot is here */
    if (imr->one_i == NULL)
    {
        display_title_message("Image does not exists!");
        return D_O_K;
    }
    if (imr->n_oi == 0)        return D_O_K;
    sc = imr->screen_scale;
    b = &imr->stack;

    n_opoid = (d->h - oi_mark_h)/opoi_mark_h;
    if (oi->n_op > n_opoid && n_opoid > 1) n_opoid--;
    i_opoid = n_opoid*(oi->cur_op/n_opoid);

    n_oid = (d->w - opoi_mark_w)/oi_mark_w;
    if (imr->n_oi > n_oid && n_oid > 1) n_oid--;
    i_oid = n_oid*(imr->cur_oi/n_oid);



    ASSERT(d);
    if (msg == MSG_DRAW)
    {


        /*        check_image_menu();*/
        ttf_enable = 0;
        /*        dump_to_xv_log_file_with_time("%d need to refresh for %d plots refresh %d"
                  ,oi->need_to_refresh,imr->cur_oi,(oi->need_to_refresh & PLOTS_NEED_REFRESH));*/
        if (b == NULL)	  oi = imr->o_i[imr->cur_oi];
        if ((b->n_box == 0) || ((oi->need_to_refresh & PLOTS_NEED_REFRESH) != 0))
        {
            do_one_image (imr);
            oi->need_to_refresh &= BITMAP_NEED_REFRESH;
            /*            dump_to_xv_log_file_with_time("did one image for %d",imr->cur_oi);*/
        }
        adj_image_pos(imr, d);
        b = &imr->stack;
        sc = imr->screen_scale;
        restore_image_parameter(imr,  d);
        write_and_blit_im_box( plt_bufferl, d, imr);
        if (win_title_used == 0)
        {
            if (imr->one_i->filename != NULL && imr->one_i->dir != NULL)
                snprintf(buf,1024,"%s %s",imr->one_i->filename, imr->one_i->dir);
            else if (imr->one_i->filename != NULL)
                snprintf(buf,1024,"%s",imr->one_i->filename);
            else snprintf(buf,1024,"Untitled.gr");
            if (os_type == OSTYPE_WIN2000 || os_type == OSTYPE_WINXP)
	      my_set_window_title("%s",buf);
        }
        remove_all_keyboard_proc();
        scan_and_update(image_menu);
    }
    else if (msg == MSG_DCLICK)
    {
        di_mn = find_dialog_associated_to_menu(NULL, NULL);
        if (di_mn != NULL && di_mn->dp != image_menu)
        {
            //if (!screen_used)
            //{
            //  screen_used = 1;
	  for(;screen_acquired;); // we wait for screen_acquired back to 0;
	  acquire_bitmap(screen);
	  screen_acquired = 1;
            rectfill(screen,di_mn->x,di_mn->y,di_mn->x+di_mn->w-1,di_mn->y+di_mn->h-1, makecol(255,255,255));
            release_bitmap(screen);
	    screen_acquired = 0;
            //  screen_used = 0;
            //}

            di_mn->w = 0; 	   di_mn->h = 0;
            di_mn->dp = image_menu;
            xvin_d_menu_proc(MSG_START,di_mn,0);
            xvin_d_menu_proc(MSG_DRAW,di_mn,0);
        }
        /* we test for op switch */
        if (mouse_x > d->w - opoi_mark_w && mouse_x < d->w)
        {
            y = d->y + d->h - mouse_y;
            i = y/opoi_mark_h;
            i += i_opoid;
            if (i < i_opoid + n_opoid && i < imr->one_i->n_op)
            {
                if (i == oi->cur_op)
                {
		  return    do_imop_menu(do_menu(imop_menu,((mouse_x-200 > 0) ? mouse_x-200 : 5),mouse_y));
                }
            }
        }
        /* we test for oi switch */
        if (mouse_y < d->y + oi_mark_h && mouse_y > d->y)
        {
            i = (mouse_x - d->x) /oi_mark_w;
            i += i_oid;
            if (i < i_oid + n_oid && i< imr->n_oi) /* we switch image */
            {
                if (i ==  imr->cur_oi)
                {
                    return    do_oi_menu(do_menu(oi_menu,mouse_x,mouse_y));
                }
            }
        }
        yb = y_off - mouse_y;
        yb *= sc;
        yb /= 2;
        xb = mouse_x;
        xb *= sc;
        xb /= 2;

        if (locate_box(b,QKD_B_XTITL, &x, &u, &y, &v, FIRST) != NULL)
        {
	  if ((xb >= x) && (xb < u) && (yb >= y) && (yb < v))
            {
                TeX_modify(oi->x_title);
                set_im_x_title(oi,"%s", last_answer);
                return D_REDRAWME;
            }
        }
        if (locate_box(b,QKD_B_YTITL, &x, &u, &y, &v, FIRST) != NULL)
        {
	  if ((xb >= x) && (xb < u) && (yb >= y) && (yb < v))
            {
                TeX_modify(oi->y_title);
                set_im_y_title(oi, "%s", last_answer);
                return D_REDRAWME;
            }
        }
        if (locate_box(b,QKD_B_TITL, &x, &u, &y, &v, FIRST) != NULL)
        {
	  if ((xb >= x) && (xb < u) && (yb >= y) && (yb < v))
            {
                TeX_modify(oi->title);
                set_im_title(oi, "%s", last_answer);
                return D_REDRAWME;
            }
        }
        if (locate_box(b,QKD_B_XLABL, &x, &u, &y, &v, FIRST) != NULL)
        {
	  if ((xb >= x) && (xb < u) && (yb >= y) && (yb < v))
                build_and_display_im_x_axis_menu(imr, oi);
            if (locate_box(b,QKD_B_XLABL, &x, &u, &y, &v, NEXT) != NULL)
            {
	      if ((xb >= x) && (xb < u) && (yb >= y) && (yb < v))
                    build_and_display_im_x_axis_menu(imr, oi);
            }
        }
        if (locate_box(b,QKD_B_YLABL, &x, &u, &y, &v, FIRST) != NULL)
        {
	  if ((xb >= x) && (xb < u) && (yb >= y) && (yb < v))
                build_and_display_im_y_axis_menu(imr,oi);
            if (locate_box(b,QKD_B_YLABL, &x, &u, &y, &v, NEXT) != NULL)
            {
	      if ((xb >= x) && (xb < u) && (yb >= y) && (yb < v))
                    build_and_display_im_y_prime_axis_menu(imr,oi);
            }
        }
        n_lab = 0;
        if (locate_box(b,QKD_B_XLABEL, &x, &u, &y, &v, FIRST) != NULL)
        {
	  if ((xb >= x) && (xb < u) && (yb >= y) && (yb < v))
                return edit_im_xlabel(oi,n_lab);

            while (locate_box(b,QKD_B_XLABEL, &x, &u, &y, &v, NEXT) != NULL)
            {
                n_lab++;
                if ((xb >= x) && (xb < u) && (yb >= y) && (yb < v))
                    return edit_im_xlabel(oi,n_lab);
            }
        }
        n_lab = 0;
        if (locate_box(b,QKD_B_YLABEL, &x, &u, &y, &v, FIRST) != NULL)
        {
	  if ((xb >= x) && (xb < u) && (yb >= y) && (yb < v))
                return edit_im_ylabel(oi,n_lab);

            while (locate_box(b,QKD_B_YLABL, &x, &u, &y, &v, NEXT) != NULL)
            {
                n_lab++;
                if ((xb >= x) && (xb < u) && (yb >= y) && (yb < v))
                    return edit_im_ylabel(oi,n_lab);
            }
        }

    }
    else if (msg == MSG_IDLE)
    {
        i = (oi->oi_idle_action != NULL) ? oi->oi_idle_action(oi,d) : 0;
        if (i != 0) return D_O_K;
        if (mouse_x < d->x || mouse_x >= d->x + d->w
            || mouse_y >= d->y + d->h || mouse_y < d->y)
            return D_O_K;
        if ((mouse_x > d->x + d->w - opoi_mark_w) && (mouse_x < d->x + d->w))
        {
            y = d->y + d->h - mouse_y;
            i = y/opoi_mark_h;
            i += i_opoid;
            if (i < i_opoid + n_opoid && i < imr->one_i->n_op)
            {
                idle_event = 0x10000 + i;
            }
        }
        /* we test for oi switch */
        else if ((mouse_y < d->y + oi_mark_h) && (mouse_y > d->y))
        {
            i = (mouse_x - d->x) /oi_mark_w;
            i += i_oid;
            if (i < i_oid + n_oid && i< imr->n_oi) /* we switch image */
            {
                idle_event = 0x20000 + i;
            }
        }
        else idle_event = 0;
        if (idle_event != last_idle_event)
        {
            idle_time = clock();
            last_idle_event = idle_event;
            idle_display = 0;
            write_and_blit_im_box( plt_bufferl, d, imr);
        }
        if (((clock() - idle_time)<<2) >  CLOCKS_PER_SEC)
        {
            if (idle_event & 0x20000)
            {
                if ((idle_display == 0) && ((idle_event&0xFFFF) >= 0) && ((idle_event&0xFFFF) < imr->n_oi))
                {
                    write_and_blit_im_box( plt_bufferl, d, imr);
                    if (imr->o_i[idle_event&0xFFFF]->filename != NULL)
                        strncpy(buf, imr->o_i[idle_event&0xFFFF]->filename, 64);
                    else strcpy(buf,"untitled");
                    buf[64] = 0;
                    if (strlen(buf) > 63) strcat(buf,"...");
                    //if (!screen_used)
                    //{
                    //screen_used = 1;

                    draw_bubble(screen, B_CENTER, mouse_x, mouse_y+20, "oi = %d, %d op %s",
                                idle_event&0xFFFF, imr->one_i->n_op, buf);
                    if (imr->o_i[idle_event&0xFFFF]->dir != NULL)
                        strncpy(buf, imr->o_i[idle_event&0xFFFF]->dir, 64);
                    else strcpy(buf,"unknown");
                    buf[64] = 0;
                    if (strlen(buf) > 63) strcat(buf,"...");
                    draw_bubble(screen, B_CENTER, mouse_x, mouse_y+36, "%s", buf);
                    decribe_image(imr->o_i[idle_event&0xFFFF], buf, 1024);
                    draw_bubble(screen, B_CENTER, mouse_x, mouse_y+52, "%s", buf);
                    //screen_used = 0;
                    //}
                    idle_display = 1;
                }
            }
            if (idle_event & 0x10000)
            {
                if ((idle_display == 0) && ((idle_event&0xFFFF) >= 0) && ((idle_event&0xFFFF) < imr->one_i->n_op))
                {
                    if (imr->one_i->o_p[idle_event&0xFFFF]->dat[0]->source != NULL)
                        strncpy(buf, imr->one_i->o_p[idle_event&0xFFFF]->dat[0]->source, 64);
                    else strcpy(buf,"No source");
                    buf[64] = 0;
                    if (strlen(buf) > 63) strcat(buf,"...");
                    write_and_blit_im_box( plt_bufferl, d, imr);
                    //if (!screen_used)
                    //{
                    //screen_used = 1;

                    draw_bubble(screen, B_RIGHT, mouse_x- 50, mouse_y-10, "op = %d %d points",
                                idle_event&0xFFFF,imr->one_i->o_p[idle_event&0xFFFF]->dat[0]->nx);
                    draw_bubble(screen, B_RIGHT, mouse_x- 50, mouse_y+10, "%s",buf);
                    //screen_used = 0;
                    //}
                    idle_display = 1;
                }
            }
        }
    }
    else if (msg == MSG_END)
    {
        if (oi->oi_end_action != NULL)  oi->oi_end_action(oi,d);
        if (imr->image_end_action != NULL)  imr->image_end_action(imr,d);
        return D_O_K;
    }
    else if (msg == MSG_CLICK)
    {
        di_mn = find_dialog_associated_to_menu(NULL, NULL);
        if (di_mn != NULL && di_mn->dp != image_menu)
        {
            //if (!screen_used)
            //{
            //  screen_used = 1;
	  for(;screen_acquired;); // we wait for screen_acquired back to 0;
	  screen_acquired = 1;
            acquire_bitmap(screen);
            rectfill(screen,di_mn->x,di_mn->y,di_mn->x+di_mn->w-1,di_mn->y+di_mn->h-1, makecol(255,255,255));
            release_bitmap(screen);
	  screen_acquired = 0;
            //  screen_used = 0;
            //}
            di_mn->w = 0; 	   di_mn->h = 0;
            di_mn->dp = image_menu;
            xvin_d_menu_proc(MSG_START,di_mn,0);
            xvin_d_menu_proc(MSG_DRAW,di_mn,0);
        }

        ym_s = mouse_y;
        xm_s = mouse_x;
        /* we check for markers */
        if (abs(mouse_x - d->x -8) < 8 &&
            abs(d->y + d->h - imr->mark_v - mouse_y) < 8)
        {        /* we are moving vertical marker */
            n_line = (int)(y_imr_2_imdata(imr, mouse_y)+.5);
            while (mouse_b & 0x3)
            {
                if (imr->mark_v != d->y + d->h - mouse_y)
                {
                    imr->mark_v = d->y + d->h - mouse_y;
                    n_line = (int)(y_imr_2_imdata(imr, mouse_y)+.5);
                    if (vert_marker_drag_action != NULL)
                    {
                        vert_marker_drag_action(imr->one_i,n_line, d);
                        if (imr->one_i->need_to_refresh & PLOTS_NEED_REFRESH)
                        {
                            do_one_image (imr);
                            imr->one_i->need_to_refresh &= BITMAP_NEED_REFRESH;
                        }
                    }
                    write_and_blit_im_box( plt_bufferl, d, imr);
                }
            }
            if (vert_marker_action != NULL)
            {
                vert_marker_action(imr->one_i, n_line, d);
                if (imr->one_i->need_to_refresh & PLOTS_NEED_REFRESH)
                {
                    do_one_image (imr);
                    imr->one_i->need_to_refresh &= BITMAP_NEED_REFRESH;
                    write_and_blit_im_box( plt_bufferl, d, imr);
                }
            }

        }
        if (abs(mouse_x - d->x - imr->mark_h) < 8 &&
            abs(d->y + d->h - mouse_y - 8) < 8)
        {    /* we are moving horizontal marker */
            n_row = (int)(x_imr_2_imdata(imr, mouse_x)+.5);
            while (mouse_b & 0x3)
            {
                if (imr->mark_h != mouse_x - d->x)
                {
                    imr->mark_h = mouse_x - d->x;
                    n_row = (int)(x_imr_2_imdata(imr, mouse_x)+.5);
                    if (horz_marker_drag_action != NULL)
                    {
                        horz_marker_drag_action(imr->one_i,n_row, d);
                        if (imr->one_i->need_to_refresh & PLOTS_NEED_REFRESH)
                        {
                            do_one_image (imr);
                            imr->one_i->need_to_refresh &= BITMAP_NEED_REFRESH;
                        }
                    }
                    write_and_blit_im_box( plt_bufferl, d, imr);
                }
            }
            if (horz_marker_action != NULL)
            {
                horz_marker_action(oi, n_row, d);
                if (imr->one_i->need_to_refresh & PLOTS_NEED_REFRESH)
                {
                    do_one_image (imr);
                    imr->one_i->need_to_refresh &= BITMAP_NEED_REFRESH;
                    write_and_blit_im_box( plt_bufferl, d, imr);
                }
            }
        }

        /* we check if mouse is in upper part of imr : image selection*/
        if (mouse_y < d->y + 16 && mouse_y >= d->y &&
            mouse_x >= d->x && mouse_x < d->x + d->w)
        {
            i = (mouse_x - d->x) /oi_mark_w;
            i += i_oid;
            if (i < i_oid + n_oid && i< imr->n_oi) /* we switch image */
            {
                if (i != imr->cur_oi)
                {
                    imr->cur_oi = (i < 0) ? 0 : i;
                    imr->one_i = imr->o_i[imr->cur_oi];
                    imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                    return D_REDRAWME; //broadcast_dialog_message(MSG_DRAW,0);
                }
            }
            else
            {
                i = imr->n_oi - i_oid;
                i = (i < n_oid) ? i : n_oid;
                i = (mouse_x - d->x) - (i * oi_mark_w);
                i = (3*i)/oi_mark_w;
                if (i == 1)
                {
                    c_oid = imr->cur_oi;
                    snprintf(buf,1024,"Present selected image nb. is %d -> file %s\nSelect the active image nb. %%d",
                             c_oid,(imr->o_i[c_oid]->filename)?imr->o_i[c_oid]->filename :"untilted.gr");
                    i = win_scanf(buf,&c_oid);
                    if (i == WIN_CANCEL || c_oid >= imr->n_oi || c_oid < 0)  return D_O_K;
                    imr->cur_oi = c_oid;
                    imr->one_i = imr->o_i[imr->cur_oi];
                    imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                    return D_REDRAWME;
                }
                else if (i == 0)
                {
                    imr->cur_oi = (i_oid > 0) ? i_oid - 1: i_oid;
                    imr->one_i = imr->o_i[imr->cur_oi];
                    imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                    return D_REDRAWME;
                }
                else if (i == 2)
                {
                    imr->cur_oi = i_oid + n_oid;
                    if (imr->cur_oi >= imr->n_oi)
                        imr->cur_oi = imr->n_oi-1;
                    imr->one_i = imr->o_i[imr->cur_oi];
                    imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                    return D_REDRAWME;
                }
            }
            dxp = dyp = 0;
            /* we drag image */
            while (mouse_b & 0x3)
            {
                dy = mouse_y - ym_s;
                dx = mouse_y - xm_s;
                oic = imr->one_i;
                if (ds_bitmap == NULL)
                {
                    ds_bitmap = create_bitmap(128,92);
                    stretch_blit(plt_bufferl,ds_bitmap,d->x,d->y,(d->w-25 > 0) ?
                                 d->w-25: 1, (d->h-16 > 0) ? d->h-16: 1,0,0,128,92);
# ifdef XV_WIN32
                    scare_mouse();
#endif
                    set_mouse_sprite(ds_bitmap);
# ifdef XV_WIN32
                    unscare_mouse();
#endif
                }
            }
# ifdef XV_WIN32
            scare_mouse();
#endif
            set_mouse_sprite(NULL);
# ifdef XV_WIN32
            unscare_mouse();
#endif
            if (ds_bitmap != NULL)
            {
                destroy_bitmap(ds_bitmap);
                ds_bitmap = NULL;
            }
            if (abs(dx) > 5 || abs(dy) > 5)
            {
                /* we copy the image */
                if (mouse_y > d->y+16 && mouse_y < d->y + d->h &&
                    mouse_x > d->x && mouse_x < d->x + d->w)
                {
                    oi = duplicate_image(oic, NULL);
                    if (oi == NULL)     return D_O_K;
                    add_to_image(imr, IS_ONE_IMAGE, (void*)oi);
                    imr->cur_oi = imr->n_oi-1;
                    imr->one_i = imr->o_i[imr->cur_oi];
                    imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                    /*                    return broadcast_dialog_message(MSG_DRAW,0);            */
                    return D_REDRAWME;
                }
            }
        }
        /* we check if mouse is on the right side of imr (plot selection)*/
        if (mouse_x > d->x + d->w - 25 && mouse_x < d->x + d->w &&
            mouse_y < d->y + d->h && mouse_y >= d->y)
        {
            y = d->y + d->h - mouse_y;
            if (y < d->h)
            {
                i = y/opoi_mark_h;
                i += i_opoid;
                k = i;
                if (i < i_opoid + n_opoid && i < imr->one_i->n_op) /* we change plot selected */
                {
                    if (i != imr->one_i->cur_op)
                    {
                        imr->one_i->cur_op = (i < 0) ? 0 : i;
                        if (imr->one_i->cur_op >= imr->one_i->n_op)
                            imr->one_i->cur_op = imr->one_i->n_op-1;
                        imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                        return D_REDRAWME;
                    }
                }
                else
                {
                    i = imr->one_i->n_op - i_opoid;
                    i = (i < n_opoid) ? i : n_opoid;
                    i = y - (i * opoi_mark_h);
                    i = (3*i)/opoi_mark_h;
                    if (i == 1)
                    {
                        c_opoid = imr->one_i->cur_op;
                        snprintf(buf,1024,"Present selected plot nb. is %d in [0,%d[\n"
                                 "Select the active plot set nb. \nor -1 to deselect %%8d\n",
                                 c_opoid,imr->one_i->n_op);
                        i = win_scanf(buf,&c_opoid);
                        if (i == WIN_CANCEL || c_opoid >= imr->one_i->n_op || c_opoid < -2)  return D_O_K;
                        imr->one_i->cur_op = c_opoid;
                        imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                        return D_REDRAWME;
                    }
                    else if (i == 0)
                    {
                        imr->one_i->cur_op = (i_opoid > 0) ? i_opoid - 1: i_opoid;
                        if (imr->one_i->cur_op >= imr->one_i->n_op)
                            imr->one_i->cur_op = imr->one_i->n_op-1;
                        imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                        return D_REDRAWME;
                    }
                    else if (i == 2)
                    {
                        imr->one_i->cur_op = i_opoid + n_opoid;
                        if (imr->one_i->cur_op >= imr->one_i->n_op)
                            imr->one_i->cur_op = imr->one_i->n_op-1;
                        imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                        return D_REDRAWME;
                    }
                }
                dxp = mouse_x; dyp = mouse_y;
                /* we drag plot */
                while (mouse_b & 0x3 && imr->one_i->n_op > 0)
                {
                    dy = mouse_y - ym_s;
                    dx = mouse_y - xm_s;
                    if (ds_bitmap == NULL)
                    {
                        opc = imr->one_i->o_p[imr->one_i->cur_op];
                        //ds = dsc = opc->dat[opc->cur_dat];

                        ds_bitmap = create_bitmap(128,92);
                        cl = Lightmagenta;
                        color = makecol(EXTRACT_R(cl), EXTRACT_G(cl), EXTRACT_B(cl));
                        clear_to_color(ds_bitmap, color);
                        u = imr->stack.w;
                        v = imr->stack.h + imr->stack.d;
                        sc = 2*u/126;
                        sc = (2*v/90 > sc) ? 2*v/90 : sc;
                        switch_to_memory_buffer(ds_bitmap);
                        display_box(&imr->stack, sc, White, TRUE_COLOR);
                        display_box(&imr->stack2, sc, White, TRUE_COLOR);
                        //if (!screen_used)
                        //{
                        //  screen_used = 1;
                        switch_to_memory_buffer(screen);
# ifdef XV_WIN32
                        scare_mouse();
#endif
                        set_mouse_sprite(ds_bitmap);
# ifdef XV_WIN32
                        unscare_mouse();
#endif
                        //  screen_used = 0;
                        //}
                    }
                    /* if we come over another image we display it */
                    if (mouse_y < d->y + 16 && mouse_y > d->y &&
                        mouse_x < d->x + d->w && mouse_x >= d->x)
                    {
                        n_opoid = (d->h - oi_mark_h)/opoi_mark_h;
                        if (oi->n_op > n_opoid && n_opoid > 1) n_opoid--;
                        i_opoid = n_opoid*(oi->cur_op/n_opoid);

                        n_oid = (d->w - opoi_mark_w)/oi_mark_w;
                        if (imr->n_oi > n_oid && n_oid > 1) n_oid--;
                        i_oid = n_oid*(imr->cur_oi/n_oid);

                        i = (mouse_x - d->x) /oi_mark_w;
                        i += i_oid;
                        if (i < i_oid + n_oid && i< imr->n_oi)
                        {
                            if (i != imr->cur_oi)
                            {
                                imr->cur_oi = (i < 0) ? 0 : i;
                                imr->one_i = imr->o_i[imr->cur_oi];
                                imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                                d_draw_Im_proc(MSG_DRAW, d, c);
                                display_title_message("im %d",imr->cur_oi);
                            }
                        }
                        else
                        {
                            i = imr->n_oi - i_oid;
                            i = (i < n_oid) ? i : n_oid;
                            i = (mouse_x - d->x) - (i * oi_mark_w);
                            i = (3*i)/oi_mark_w;
                            if (i == 0)
                            {
                                imr->cur_oi = (i_oid > 0) ? i_oid - 1: i_oid;
                                imr->one_i = imr->o_i[imr->cur_oi];
                                imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                                d_draw_Im_proc(MSG_DRAW, d, c);
                            }
                            else if (i == 2)
                            {
                                imr->cur_oi = i_oid + n_oid;
                                if (imr->cur_oi >= imr->n_oi)  	    imr->cur_oi = imr->n_oi-1;
                                imr->one_i = imr->o_i[imr->cur_oi];
                                imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                                d_draw_Im_proc(MSG_DRAW, d, c);
                            }
                        }
                    }

                } /* finish dragging */
# ifdef XV_WIN32
                scare_mouse();
#endif
                set_mouse_sprite(NULL);
                set_mouse_sprite_focus(0,0);
# ifdef XV_WIN32
                unscare_mouse();
#endif
                if (ds_bitmap != NULL)
                {
                    destroy_bitmap(ds_bitmap);
                    ds_bitmap = NULL;
                }
                if (abs(dx) > 5 || abs(dy) > 5) /* we moved significantly */
                {
                    /* we are in the imr */
                    if (mouse_x >= d->x && mouse_x < d->x + d->w - opoi_mark_w &&
                        opc != NULL && mouse_y < d->y + d->h && mouse_y > d->y)
                    {
                        /*                win_printf("x rel %d op -> %d",mouse_x - d->x,(mouse_x - d->x)/120); */
                        if (mouse_y < d->y + oi_mark_h && mouse_y >= d->y &&
                            mouse_x >= d->x && mouse_x < d->x + d->w)
                            //            && (mouse_x - d->x)/oi_mark_w < imr->n_oi)
                        {
                            i = (mouse_x - d->x) /oi_mark_w;
                            i += i_oid;
                            if (i < i_oid + n_oid && i< imr->n_oi)
                            {
                                i = (i < 0)    ? 0 : i;
                                i = (i < imr->n_oi)    ? i : imr->n_oi-1;
                                if ((opd = duplicate_plot_data(opc, NULL)) == NULL)
                                    return D_O_K;
                                add_to_one_image(imr->o_i[i], IS_ONE_PLOT, (void*)opd);
                                imr->o_i[i]->cur_op = imr->o_i[i]->n_op - 1;
                                /*                            win_printf("oi %d op nb %d",i,imr->o_i[i]->cur_op);*/
                                imr->o_i[i]->need_to_refresh |= PLOTS_NEED_REFRESH;
                                return D_REDRAWME;
                            }
                        }
                        else if (mouse_y > d->y + oi_mark_h && mouse_y < d->y + d->h &&
                                 mouse_x > d->x && mouse_x < d->x + d->w - opoi_mark_w)
                        {
                            if ((opd = duplicate_plot_data(opc, NULL)) == NULL)
                                return D_O_K;
                            add_to_one_image(imr->one_i, IS_ONE_PLOT, (void*)opd);
                            imr->one_i->cur_op = imr->one_i->n_op - 1;
                            imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                            return D_REDRAWME;
                        }


                    }
                }
                if (k == imr->cur_oi && abs(mouse_x - dxp) < 10
                    && abs(mouse_y - dyp) < 10) /* we unselect plot */
                {
                    imr->one_i->cur_op = -1;
                    imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
                    return D_REDRAWME; //broadcast_dialog_message(MSG_DRAW,0);
                }
            }
        }
        yb = y_off - mouse_y;
        yb *= sc;
        yb /= 2;
        xm_s = xb = mouse_x;
        xb *= sc;
        xb /= 2;
        /* we change the region of interest by mouse */
        if ((xt = locate_box(b,QKD_B_XTK_B, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                xts = xt->xc; yts = xt->yc;
                dy = dyp = 0;
                while (mouse_b & 0x3)
                {
                    dy = mouse_y - ym_s;
                    if (dy != dyp)
                    {
                        xt->yc = yts - (dy * sc)/2;
                        write_and_blit_im_box( plt_bufferl, d, imr);
                        dyp = dy;
                        draw_bubble(screen, B_CENTER, mouse_x, mouse_y-30,
                                    "nys = %d",(int)(y_imr_2_imdata(imr, SCREEN_H - (v*2)/sc + dy)+.5));
                    }

                }
                v = (v*2)/sc;
                v -= dy;
                oi->im.nys = (int)(y_imr_2_imdata(imr, SCREEN_H - v)+.5);
                if (oi->im.nys < 0)
                {
                    oi->im.nye = oi->im.ny;
                    oi->im.nys = 0;
                }
                oi->im.nys = (oi->im.nys < 0) ? 0 : oi->im.nys;
                oi->im.nys = (oi->im.nys < oi->im.ny) ? oi->im.nys : oi->im.ny -1;
                /*                win_printf("pos y %f",y_imr_2_imdata(imr, SCREEN_H - v));    */
                oi->need_to_refresh = ALL_NEED_REFRESH;
                return D_REDRAWME;
            }
        }
        if ((xt = locate_box(b,QKD_B_XTK_T, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                xts = xt->xc; yts = xt->yc;
                dy = dyp = 0;
                while (mouse_b & 0x3)
                {
                    dy = mouse_y - ym_s;
                    if (dy != dyp)
                    {
                        xt->yc = yts - (dy * sc)/2;
                        write_and_blit_im_box( plt_bufferl, d, imr);
                        dyp = dy;
                        draw_bubble(screen, B_CENTER, mouse_x, mouse_y+30, "nye = %d",
                                    (int)(y_imr_2_imdata(imr, SCREEN_H - (y*2)/sc + dy)+.5));
                    }

                }

                y = (y*2)/sc;
                y -= dy;
                oi->im.nye = (int)(y_imr_2_imdata(imr, SCREEN_H - y)+.5);
                if (oi->im.nye <= oi->im.nys)
                {
                    oi->im.nye = oi->im.ny;
                    oi->im.nys = 0;
                }
                oi->im.nye = (oi->im.nye < 0) ? 1 : oi->im.nye;
                oi->im.nye = (oi->im.nye > oi->im.ny) ? oi->im.ny : oi->im.nye;

                /*                win_printf("pos y %f",y_imr_2_imdata(imr, SCREEN_H - y));    */
                oi->need_to_refresh = ALL_NEED_REFRESH;
                return D_REDRAWME;
            }
        }
        if ((xt = locate_box(b,QKD_B_YTK_R, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                xts = xt->xc; yts = xt->yc;
                dx = dxp = 0;
                while (mouse_b & 0x3)
                {
                    dx = mouse_x - xm_s;
                    if (dx != dxp)
                    {
                        xt->xc = xts + (dx * sc)/2;
                        write_and_blit_im_box( plt_bufferl, d, imr);
                        dxp = dx;
                        draw_bubble(screen, B_RIGHT, mouse_x-10, mouse_y, "nxe = %d"
                                    ,(int)(x_imr_2_imdata(imr, (x*2)/sc + dx)+.5));
                    }

                }
                x = (x*2)/sc;
                x += dx;
                oi->im.nxe = (int)(x_imr_2_imdata(imr, x)+.5);
                if (oi->im.nxe <= oi->im.nxs)
                {
                    oi->im.nxe = oi->im.nx;
                    oi->im.nxs = 0;
                }
                oi->im.nxe = (oi->im.nxe < 0) ? 1 : oi->im.nxe;
                oi->im.nxe = (oi->im.nxe > oi->im.nx) ? oi->im.nx : oi->im.nxe;
                /*                win_printf("u %d pos x %f",x,x_imr_2_imdata(imr, x));*/
                oi->need_to_refresh = ALL_NEED_REFRESH;
                return D_REDRAWME;
            }
        }
        if ((xt = locate_box(b,QKD_B_YTK_L, &x, &u, &y, &v, FIRST)) != NULL)
        {
            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                xts = xt->xc; yts = xt->yc;
                dx = dxp = 0;
                while (mouse_b & 0x3)
                {
                    dx = mouse_x - xm_s;
                    if (dx != dxp)
                    {
                        xt->xc = xts + (dx * sc)/2;
                        write_and_blit_im_box( plt_bufferl, d, imr);
                        dxp = dx;
                        draw_bubble(screen, B_LEFT, mouse_x+10, mouse_y, "nxs = %d"
                                    ,(int)(x_imr_2_imdata(imr, (u*2)/sc + dx)+.5));
                    }

                }
                u = (u*2)/sc;
                u += dx;
                oi->im.nxs = (int)(x_imr_2_imdata(imr, u)+.5);
                if (oi->im.nxe <= oi->im.nxs)
                {
                    oi->im.nxe = oi->im.nx;
                    oi->im.nxs = 0;
                }
                oi->im.nxs = (oi->im.nxs < 0) ? 0 : oi->im.nxs;
                oi->im.nxs = (oi->im.nxs > oi->im.nx-1) ? oi->im.nx : oi->im.nxs-1;
                /*                win_printf("x %d pos x %f",u,x_imr_2_imdata(imr, u));*/
                oi->need_to_refresh = ALL_NEED_REFRESH;
                return D_REDRAWME;
            }
        }
        if ((xt = locate_box(&(imr->stack2),QKD_B_XLABEL, &x, &u, &y, &v, FIRST)) != NULL)
        {

            if (xb >= x && xb < u && yb >= y && yb < v)
            {
                //                win_printf("label in X");
                return move_im_label(xt, b, sc, plt_bufferl, d, imr, xm_s, ym_s,x,y);            }

            while ((xt = locate_box(&(imr->stack2),QKD_B_XLABEL, &x, &u, &y, &v, NEXT)) != NULL)
            {
                if (xb >= x && xb < u && yb >= y && yb < v)
                    return move_im_label(xt, b, sc, plt_bufferl, d, imr, xm_s, ym_s,x,y);
            }
        }
        if ((xt = locate_box(&(imr->stack2),QKD_B_YLABEL, &x, &u, &y, &v, FIRST)) != NULL)
        {

            if (xb >= x && xb < u && yb >= y && yb < v)
                return move_im_label(xt, b, sc, plt_bufferl, d, imr, xm_s, ym_s,x,y);
            while ((xt = locate_box(&(imr->stack2),QKD_B_YLABL, &x, &u, &y, &v, NEXT)) != NULL)
            {
                if (xb >= x && xb < u && yb >= y && yb < v)
                    return move_im_label(xt, b, sc, plt_bufferl, d, imr, xm_s, ym_s,x,y);
            }
        }
        if (oi->oi_mouse_action != NULL)
            oi->oi_mouse_action(oi, xm_s, ym_s, 0, d);
        else if (imr->image_mouse_action != NULL)
            imr->image_mouse_action(imr, xm_s, ym_s, 0, d);
        else if ((xt = locate_box(b,QKD_B_PLOT, &x, &u, &y, &v, FIRST)) != NULL)
        {
            while (mouse_b & 0x3)    /* mouse button dwn */
            {
                write_and_blit_im_box( plt_bufferl, d, imr);
                if (general_im_action != NULL)
                    general_im_action(imr, xm_s, ym_s, 1);
            }
            write_and_blit_im_box( plt_bufferl, d, imr);
            if (general_im_action != NULL)
                general_im_action(imr, xm_s, ym_s, 0);
        }
    }
    else if (msg == MSG_WHEEL)
    {
        if (image_wheel_action != NULL)
        {
            image_wheel_action(imr, imr->one_i, d, c);
        }
    }
    else if (msg == MSG_LOSTFOCUS)
    {
        //display_title_message("image lost focus");
    }
    else if (msg == MSG_GOTFOCUS)
    {
        //display_title_message("image got focus");
    }
    else if (msg == MSG_LOSTMOUSE)
    {
        //display_title_message("image lost mouse");
        if (imr->one_i != NULL && imr->one_i->oi_lost_mouse != NULL)
            imr->one_i->oi_lost_mouse(imr->one_i, mouse_x, mouse_y, 0);
        else if (imr->image_lost_mouse != NULL)
            imr->image_lost_mouse(imr, mouse_x, mouse_y, 0);
    }
    else if (msg == MSG_GOTMOUSE)
    {
        //Display_title_message("image got mouse");
        if (imr->one_i != NULL && imr->one_i->oi_got_mouse != NULL)
            imr->one_i->oi_got_mouse(imr->one_i, mouse_x, mouse_y, 0);
        else if (imr->image_got_mouse != NULL)
            imr->image_got_mouse(imr, mouse_x, mouse_y, 0);
    }
    if (msg == MSG_WANTFOCUS) return D_WANTFOCUS;
    if (msg == MSG_LOSTFOCUS) return D_WANTFOCUS;
    if (msg == MSG_KEY) return D_WANTFOCUS;

    return D_O_K;
}


int draw_im_screen_label(imreg *imr, O_i *oi, int cur_f)
{
    int j;
    struct screen_label *s_l = NULL;

    if (oi->im.s_l != NULL && oi->im.m_sl != NULL && oi->im.n_sl != NULL)
    {
        if (cur_f < 0 || cur_f >= oi->im.n_f) return 1;
        for (j=0 ; j < oi->im.n_sl[cur_f] ; j++)
        {
            s_l = oi->im.s_l[cur_f][j];
            if (s_l != NULL && s_l->draw_shape != NULL)
                s_l->draw_shape(s_l, oi, imr);
        }
    }
    return 0;
}

int    mk_op_mark(int n_op, O_p *op, int x, int y)
{
    struct box b, *li = NULL, *sym = NULL, *bc = NULL, *bd = NULL;
    d_s *ds = NULL;
    int ttf;
    BX_AR *xr = NULL, *yr = NULL;
    char ch[128] = {0};


    init_box(&b);

    ds = op->dat[op->cur_dat];
    b.color = ds->color;
    li = mkpolybox(&b,3);
    if (li == NULL)        return 1;

    xr = (BX_AR *)li->child[0];
    yr = (BX_AR *)li->child[1];
    xr[0] = 0;
    yr[0] = 500;
    yr[2] = -500;
    if (ds->m == 1)
    {
        xr[1] |= 0x0001;
        xr[2] |= 0x0001;
    }
    if (ds->m == 0)
    {
        xr[1] |= 0x0001;
    }
    if (ds->symb != NULL)
    {
        sym = (struct box *)calloc(1,sizeof(struct box));
        if (sym == NULL)        return 1;
        sym->parent = &b;
        sym->n_hic = b.n_hic + 1;
        sym->pt = b.pt;
        sym->color = ds->color;
        sym->i_box = 0;
        sym->slx = b.slx;
        sym->sly = b.sly;
        string_to_box(ds->symb,sym);
        sym->xc = - sym->w / 2;
        sym->yc = (sym->d - sym->h)  / 2;
        li->child[2] = sym;
        set_box_size(sym, li);
    }
    ttf = ttf_enable;
    ttf_enable = 1;
    pc_fen = text_scale;
    set_box_size(li, &b);
    bc = mknewbox(&b);
    snprintf(ch,128,"%d",n_op);

    if ((bd = mknewbox(bc)) == NULL)        return 1;

    bc->n_box = TURN_90;
    bd->color = bc->color = b.color;
    string_to_box(ch,bd);
    bc->xc = (bd->h+bd->d);
    bc->yc = -bd->w/2;
    if (sym != NULL)    bc->xc += sym->w/2;
    set_box_size(bd, bc);
    set_box_size(bc, &b);
    set_box_origin(&b, x, y, text_scale);
    display_box(&b, text_scale, White, TRUE_COLOR);
    free_box(&b);
    ttf_enable = ttf;
    return 0;

}



int    write_and_blit_im_box(BITMAP *lplt_buffer, DIALOG *d, imreg *imr)
{
    int i, j;
    int sc,  mode, n_opoid, i_opoid, n_oid, i_oid;
    BITMAP *imb = NULL;
    O_i* oi = NULL;
    char buf[256] = {0};

    if (imr == NULL || 	plt_buffer_used)    xvin_error(Wrong_Argument);
    oi = imr->one_i;
    if (IS_BITMAP_IN_USE(oi))   return 0;


    if (imr->one_i->data_changing || imr->one_i->transfering_data_to_box)   return 0;
    if (imr->stack_in_use || 	imr->blitting_stack)   return 0;

    imr->blitting_stack = 1;

    sc = imr->screen_scale &~AUTO_SCALE;
# ifdef XV_WIN32
    scare_mouse();
#endif
    switch_to_memory_buffer(lplt_buffer);
    acquire_bitmap(lplt_buffer);
    plt_buffer_used = 1;
    set_clip_rect(lplt_buffer, d->x, d->y, d->x+d->w-1, d->y+d->h-1);
    clear_to_color(lplt_buffer, makecol(85,85,85));
    if (user_font == NULL)
        ttf_init();



    n_opoid = (d->h - oi_mark_h)/opoi_mark_h;
    if (oi->n_op > n_opoid && n_opoid > 1) n_opoid--;
    i_opoid = n_opoid*(oi->cur_op/n_opoid);

    n_oid = (d->w - opoi_mark_w)/oi_mark_w;
    if (imr->n_oi > n_oid && n_oid > 1) n_oid--;
    i_oid = n_oid*(imr->cur_oi/n_oid);


    /* we fill the data sets label vertically on the right */
    for (i = i_opoid,j = opoi_mark_h/2; i < oi->n_op && i < i_opoid + n_opoid && j < d->h - opoi_mark_h;
         i++, j += opoi_mark_h)
    {
        rectfill(lplt_buffer,d->x+d->w-opoi_mark_w,d->y+d->h-j+2-opoi_mark_h/2,
                 d->x+d->w-1,d->y+d->h-j-2+opoi_mark_h/2,
                 (i== imr->one_i->cur_op    ) ? makecol(128,128,128) : makecol(64,64,64));
        mk_op_mark(i, oi->o_p[i], d->x+d->w-opoi_mark_h/2 ,SCREEN_H - d->y - d->h + j);
    }
    if (user_font != NULL) alfont_set_font_size(user_font, 12);
    if (oi->n_op > n_opoid)
    {
        j -= opoi_mark_h/3;
        rectfill(lplt_buffer,d->x+d->w-opoi_mark_w,d->y+d->h-j+1-opoi_mark_h/6,
                 d->x+d->w-1,d->y+d->h-j-1+opoi_mark_h/6, makecol(64,64,64));
        alfont_textout(lplt_buffer,user_font," -",d->x+d->w-opoi_mark_w,d->y+d->h-j-4,
                       makecol(255, 255, 255));
        j += opoi_mark_h/3;
        rectfill(lplt_buffer,d->x+d->w-opoi_mark_w,d->y+d->h-j+1-opoi_mark_h/6,
                 d->x+d->w-1,d->y+d->h-j-1+opoi_mark_h/6, makecol(64,64,64));
        if (user_font != NULL)
        {
            snprintf(buf,256," %d",oi->cur_op);
            alfont_textout(lplt_buffer,user_font,buf,d->x+d->w-opoi_mark_w,d->y+d->h-j-4,
                           makecol(255, 255, 255));
        }
        j += opoi_mark_h/3;
        rectfill(lplt_buffer,d->x+d->w-opoi_mark_w,d->y+d->h-j+1-opoi_mark_h/6,d->x+d->w-1,
                 d->y+d->h-j-1+opoi_mark_h/6, makecol(64,64,64));
        alfont_textout(lplt_buffer,user_font," +",d->x+d->w-opoi_mark_w,d->y+d->h-j-4,
                       makecol(255, 255, 255));
    }

    /* we fill the plots label horizontally on the top */
    for (i = i_oid,j = d->x; i < imr->n_oi && i < i_oid + n_oid && j + oi_mark_w < d->x+d->w-opoi_mark_w; i++, j += oi_mark_w)
    {
        if (i == imr->cur_oi)
        {
            rectfill(lplt_buffer,j+2,d->y,j+oi_mark_w-4,d->y+oi_mark_h, makecol(196,196,196));
            if (user_font != NULL && imr->o_i[i]->filename != NULL)
                alfont_textout(lplt_buffer,user_font,imr->o_i[i]->filename,j+4,d->y+2,
                               makecol(64, 64, 64));
            else if (user_font != NULL )
            {
                snprintf (buf,256,"Image %d",i);
                alfont_textout(lplt_buffer,user_font,buf,j+4,d->y+2,
                               makecol(64, 64, 64));
            }
        }
        else
        {
            rectfill(lplt_buffer,j+2,d->y,j+oi_mark_w-4,d->y+oi_mark_h, makecol(128,128,128));
            if (user_font != NULL && imr->o_i[i]->filename != NULL)
                alfont_textout(lplt_buffer,user_font,imr->o_i[i]->filename,j+4,d->y+2,
                               makecol(255, 255, 255));
            else if (user_font != NULL )
            {
                snprintf (buf,256,"Image %d",i);
                alfont_textout(lplt_buffer,user_font,buf,j+4,d->y+2,
                               makecol(255, 255, 255));
            }
        }
    }
    if (imr->n_oi > n_oid)
    {
        rectfill(lplt_buffer,j+2,d->y,j+oi_mark_w/3-4,d->y+oi_mark_h, makecol(128,128,128));
        if (user_font != NULL)
            alfont_textout(lplt_buffer,user_font,"   << ",j+4,d->y+2, makecol(255, 255, 255));
        j += oi_mark_w/3-2;
        rectfill(lplt_buffer,j+2,d->y,j+oi_mark_w/3-4,d->y+oi_mark_h, makecol(128,128,128));
        if (user_font != NULL)
        {
            snprintf(buf,256,"Im. %d",imr->cur_oi);
            alfont_textout(lplt_buffer,user_font,buf,j+4,d->y+2, makecol(255, 255, 255));
        }
        j += oi_mark_w/3-2;
        rectfill(lplt_buffer,j+2,d->y,j+oi_mark_w/3-4,d->y+oi_mark_h, makecol(128,128,128));
        if (user_font != NULL)
            alfont_textout(lplt_buffer,user_font," >>   ",j+4,d->y+2, makecol(255, 255, 255));

    }

    set_clip_rect(lplt_buffer, d->x, d->y+18, d->x+d->w-27, d->y+d->h);


    if ((oi->bmp.to != IS_BITMAP) || (oi->bmp.stuff == NULL)
        || (oi->bmp.sub_type != sc)) oi->need_to_refresh |= BITMAP_NEED_REFRESH;

    if (!(oi->iopt2 & NOT_TO_DISPLAY) && oi->need_to_refresh&BITMAP_NEED_REFRESH)
        display_image_stuff_16M(imr,d);


    /*    if (image_special_display != NULL)    image_special_display(imr); */



    imr->stack.xc = d->x*sc/2; // new dec 2004
    imr->stack.yc = ((screen->h - d->y - d->h)*sc)/2;
    adj_image_pos(imr, d);

    if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL))
    {
        imb = (BITMAP*)oi->bmp.stuff;
        /* d->y + d->h */
        //acquire_bitmap(imb);
        blit(imb,lplt_buffer,0,0,imr->x_off //+ d->x
             , imr->y_off - imb->h + d->y, imb->w, imb->h);
        //release_bitmap(imb);
    }
    set_clip_rect(lplt_buffer, d->x, d->y+18, d->x+d->w-27, d->y+d->h);



    display_box(&imr->stack, sc, White, TRUE_COLOR);
    mode = (abs(mouse_x - d->x -8) < 8 && (mouse_b & 0x3) &&
            abs(d->y + d->h - imr->mark_v - mouse_y) < 8) ? 1 : 0;
    draw_vert_marker(lplt_buffer, d, imr->mark_v, mode);
    mode = (abs(mouse_x - d->x - imr->mark_h) < 8 && (mouse_b & 0x3) &&
            abs(d->y + d->h - mouse_y - 8) < 8) ? 1 : 0;
    draw_horz_marker(lplt_buffer, d, imr->mark_h, mode);
    /*
       if (locate_box(&imr->stack,QKD_B_PLOT, &x2, &x3, &y2, &y3, FIRST) == NULL)
       return 1;
       x3 = imr->stack2.xc;
       y3 = imr->stack2.yc;
       imr->stack2.xc += x2;
       imr->stack2.yc += y2;
       */
    display_box(&imr->stack2,sc,White, TRUE_COLOR);
    /*    pc_color(TRUE_COLOR);*/
    /*
          imr->stack2.xc = x2;
          imr->stack2.yc = y2;
          */
    /* it was x3 and y3*/

    set_clip_rect(lplt_buffer, 0, 0, SCREEN_W-1, SCREEN_H-1);
    release_bitmap(lplt_buffer);
    if (oi->oi_post_display != NULL)      oi->oi_post_display(oi, d);
# ifdef XV_WIN32
    if (win_get_window() == GetForegroundWindow())
    {
# endif
        //if (!screen_used)
        // {
        //  screen_used = 1;
        vsync();
	for(;screen_acquired;); // we wait for screen_acquired back to 0;
	screen_acquired = 1;
        acquire_bitmap(screen);
        blit(lplt_buffer, screen, d->x, d->y, d->x, d->y, d->w, d->h);
        release_bitmap(screen);
	screen_acquired = 0;
        //  screen_used = 0;
        //}
# ifdef XV_WIN32
    }
# endif
    plt_buffer_used = 0;
    switch_to_memory_buffer(screen);
    draw_im_screen_label(imr, oi, oi->im.c_f);
# ifdef XV_WIN32
    unscare_mouse();
#endif
    imr->blitting_stack = 0;
    /*    if (!(oi->iopt2 & NOT_TO_DISPLAY))
          display_image_stuff_16M(imr,d);*/
    return 0;
}

int adj_image_pos(imreg *imr, DIALOG *d)
{
    int x_off = 0, y_off = 0, sc;

    if (imr == NULL || imr->one_i == NULL || imr->one_i->im.pixel == NULL)
        return win_printf_OK("Could not find image data");
    sc = imr->screen_scale;
    if (imr->plt != NULL)
    {
        x_off = imr->plt->xc;
        y_off = imr->plt->yc;
    }
    x_off = (2 * (x_off + imr->stack.xc) + 3 * PIXEL_1)/sc;// was 3 * PIXEL_1
    y_off = (2 * (y_off + imr->stack.yc) + 3 * PIXEL_1)/sc;// was 3 * PIXEL_1

    /*    y_off =  cur_ac_reg->area.y1 - cur_ac_reg->area.y0 - y_off ;*/
    y_off = d->h  - y_off; /* + d->y */

    imr->x_off = x_off;
    imr->y_off = y_off;
    return 0;
}
int restore_image_parameter(imreg *imr,  DIALOG *d)
{
    O_i *oi = NULL;
    int x_off = 0, y_off = 0, sc;

    if (imr == NULL || imr->one_i == NULL || imr->one_i->im.pixel == NULL)
        return win_printf_OK("Could not find image data");
    if (imr->one_i->iopt2 & NOT_TO_DISPLAY)            return 0;
    if (d->w <= 0 || d->h <= 0)        return 2;
    oi = imr->one_i;

    sc = imr->screen_scale;
    if (imr->plt != NULL)
    {
        x_off = imr->plt->xc;
        y_off = imr->plt->yc;
    }
    x_off = (2 * (x_off + imr->stack.xc) + 3 * PIXEL_1)/sc;// was 3 * PIXEL_1
    y_off = (2 * (y_off + imr->stack.yc) + 3 * PIXEL_1)/sc;// was 3 * PIXEL_1
    y_off = d->h  - y_off;
    imr->x_off = x_off;
    imr->y_off = y_off;
    if (sc != 0)
    {
        imr->s_nx = (int)(512 * PIXEL_1 * oi->width) / sc;
        imr->s_ny = (int)(512 * PIXEL_1 * oi->height) / sc;
    }
    return 0;
}

int display_image_stuff_16M(imreg *imr,  DIALOG *d)
{
    int i, j;
    unsigned char c, *imc = NULL, *imc1 = NULL, cr, cg, cb;
    short int *imin = NULL;
    short int *imin1 = NULL;
    unsigned short int *imui = NULL;
    unsigned short int *imui1 = NULL;
    int *imli = NULL;
    int *imli1 = NULL;
    int x_off = 0, y_off = 0, sc, k, mode;
    int w_im, h_im;
    int zwi, zbi, rzi, gzi, bzi;
    long int zi, zgi;
    int lx, lx1, lpx, ly, ly1, lpy;
    int pxs, pxe, pys, pye, nxef, nyef;
    int spxs, spxe, spys, spye, snxef, snyef, v_bmp = 0;    /* screen pixel */
    float z = 0, z1, z2, zg, *imfl, *imfl1;
    double *imdb = NULL, *imdb1 = NULL;
    rgba_t *imrgba = NULL, *imrgba1 = NULL;
    rgb16_t *imrgb16 = NULL, *imrgb16_1 = NULL;
    rgba16_t *imrgba16 = NULL, *imrgba16_1 = NULL;
    O_i *oi = NULL;
    unsigned int *zl1 = NULL, *zl2 = NULL, c32;
    BITMAP *bmp = NULL;
    union pix *ps = NULL;



    if (imr == NULL || imr->one_i == NULL || imr->one_i->im.pixel == NULL)
        return win_printf_OK("Could not find image data");
    if (imr->one_i->iopt2 & NOT_TO_DISPLAY)            return 0;
    /*    if ((imr->db.x1 <= imr->db.x0)||(imr->db.y1 <= imr->db.y0)) return 2; */
    if (d->w <= 0 || d->h <= 0)        return 2;
    oi = imr->one_i;
    /* do we need to work ? */
    if ((oi->bmp.stuff != NULL) && (oi->bmp.to == IS_BITMAP) &&
        ((oi->need_to_refresh&BITMAP_NEED_REFRESH) == 0))        return 0;
    if (IS_DATA_IN_USE(oi) || IS_BITMAP_IN_USE(oi))   return 0;
    SET_BITMAP_IN_USE(oi);
    sc = imr->screen_scale;
    if (imr->plt != NULL)
    {
        x_off = imr->plt->xc;
        y_off = imr->plt->yc;
    }
    x_off = (2 * (x_off + imr->stack.xc) + 3 * PIXEL_1)/sc; // was 3 * PIXEL_1
    y_off = (2 * (y_off + imr->stack.yc) + 3 * PIXEL_1)/sc; // was 3 * PIXEL_1

    /*    y_off =  cur_ac_reg->area.y1 - cur_ac_reg->area.y0 - y_off ;*/
    y_off = d->h  - y_off; /* + d->y */

    imr->x_off = x_off;
    imr->y_off = y_off;

    zg = 255;
    if ((oi->z_max < oi->z_white && oi->z_max < oi->z_black)
        || (oi->z_min > oi->z_white && oi->z_min >oi->z_black))
    {
        oi->z_max = (oi->z_white > oi->z_black) ? oi->z_white : oi->z_black;
        oi->z_min = (oi->z_white > oi->z_black) ? oi->z_black : oi->z_white;
    }

    if ((oi->z_white - oi->z_black) != 0)    zg /= (oi->z_white - oi->z_black);
    zwi = (oi->dz != 0) ? (int)((oi->z_white - oi->az)/oi->dz) :
        (int)(oi->z_white - oi->az);
    zbi = (oi->dz != 0) ? (int)((oi->z_black - oi->az)/oi->dz) :
        (int)(oi->z_black - oi->az);
    //	set_im_title(oi, "black %d Z_black %g white %d Z_white %g",zwi,oi->z_white,zbi,oi->z_black);
    zgi = 65536 * 255;
    if ( (zwi - zbi) != 0)     zgi /= (zwi - zbi);
    nxef = oi->im.nxe - oi->im.nxs;
    nyef = oi->im.nye - oi->im.nys;
    if (sc != 0)
    {
        imr->s_nx = (int)(512 * PIXEL_1 * oi->width) / sc;
        imr->s_ny = (int)(512 * PIXEL_1 * oi->height) / sc;
    }
    /* the image strarting in screen pixel */
    /*    spxs = ((imr->db.x0+1 - x_off) > 0) ? imr->db.x0+1 - x_off : 0;
          spxe = imr->db.x1 - 1 - x_off;
          spys = ((y_off+1 - imr->db.y1) > 0) ? y_off+1 - imr->db.y1 : 0;
          spye = y_off - imr->db.y0 - 1;    */

    spxs = ((1 - x_off) > 0) ? 1 - x_off : 0;
    spxe = d->w - 1 - x_off;
    spys =     (y_off+1 - d->h) > 0 ? y_off+1 - d->h : 0;
    spye = y_off - 1;
    /*    win_printf("bef acquire");    */
    if (spxe <= 0 || spye <= 0)
    {
        SET_BITMAP_NO_MORE_IN_USE(oi);
        return D_O_K;
    }

    mode = oi->im.mode;
    // we freeze the movie image in the movie
    ps = (oi->im.n_f > 1) ? oi->im.pxl[oi->im.c_f] : oi->im.pixel;

    if ( sc * nxef == (int)(PIXEL_1 * 512 * oi->width +.5) &&
         sc * nyef == (int)(PIXEL_1 * 512 * oi->height +.5))
    {
        w_im = nxef;
        spxe = (spxe > w_im) ? w_im : spxe;
        h_im = nyef;
        spye = (spye > h_im) ? h_im : spye;
        pxs = spxs;
        pys = spys;
        pxe = (nxef < spxe) ? nxef : spxe;
        pye = (nyef < spye) ? nyef : spye;

        //modif if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL) && (oi->bmp.sub_type != sc))
        if (oi->bmp.stuff == NULL)
        {
            bmp = create_bitmap(spxe-spxs+1,spye-spys+1);
            oi->bmp.stuff = (void*)bmp;
            if (bmp == NULL)
            {
                SET_BITMAP_NO_MORE_IN_USE(oi);
                return xvin_error(Out_Of_Memory);
            }
        }
        else bmp = (BITMAP*)oi->bmp.stuff;

        if (bmp->w != spxe-spxs+1 || bmp->h != spye-spys+1)
        {
            v_bmp = (is_video_bitmap((BITMAP*)oi->bmp.stuff) == TRUE) ? 1 : 0;
            destroy_bitmap((BITMAP*)oi->bmp.stuff);
            bmp = NULL;
            if (v_bmp) bmp = create_video_bitmap(spxe-spxs+1,spye-spys+1);
            if (bmp == NULL) bmp = create_bitmap(spxe-spxs+1,spye-spys+1);
            oi->bmp.stuff = (void*)bmp;
            if (bmp == NULL)
            {
                SET_BITMAP_NO_MORE_IN_USE(oi);
                return xvin_error(Out_Of_Memory);
            }
        }
        else bmp = (BITMAP*)oi->bmp.stuff;
        oi->bmp.to = IS_BITMAP;
        oi->bmp.sub_type = sc;

        acquire_bitmap(bmp);
        if (oi->im.data_type == IS_CHAR_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imc = ps[ly].ch;
                zl1 = (unsigned int*)(bmp->line[pye-i-1]); // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    zi = (imc[k] - zbi)* zgi;
                    zi = (zi < 0 ) ? 0 : (zi >> 16);
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol(c,c,c);
                }
            }
        }
        else    if (oi->im.data_type == IS_RGB_PICTURE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imc = ps[ly].ch;
                zl1 = (unsigned int*)(bmp->line[pye-i-1]); // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = 3*(j + oi->im.nxs);
		    k = (k < 0) ? 0 : k;
		    k = (k+2 < 3*oi->im.nx) ? k : 3*oi->im.nx-3;
                    switch (mode)
                    {
                        case GREY_LEVEL:
                            rzi = (imc[k] - zbi)* zgi;
                            rzi += (imc[k+1] - zbi)* zgi;
                            rzi += (imc[k+2] - zbi)* zgi;
                            rzi /= 3;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case R_LEVEL:
                            rzi = (imc[k] - zbi)* zgi;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case RED_ONLY:
                            rzi = (imc[k] - zbi)* zgi;
                            gzi = 0;
                            bzi = 0;
                            break;
                        case G_LEVEL:
                            gzi = (imc[k+1] - zbi)* zgi;
                            bzi = gzi;
                            rzi = gzi;
                            break;
                        case GREEN_ONLY:
                            gzi = (imc[k+1] - zbi)* zgi;
                            bzi = 0;
                            rzi = 0;
                            break;
                        case B_LEVEL:
                            bzi = (imc[k+2] - zbi)* zgi;
                            gzi = bzi;
                            rzi = bzi;
                            break;
                        case BLUE_ONLY:
                            bzi = (imc[k+2] - zbi)* zgi;
                            gzi = 0;
                            rzi = 0;
                            break;
                        default:
                            rzi = (imc[k] - zbi)* zgi;
                            gzi = (imc[k+1] - zbi)* zgi;
                            bzi = (imc[k+2] - zbi)* zgi;
                            break;
                    }
                    rzi = (rzi < 0 ) ? 0 : (rzi >> 16);
                    gzi = (gzi < 0 ) ? 0 : (gzi >> 16);
                    bzi = (bzi < 0 ) ? 0 : (bzi >> 16);
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    zl1[j-spxs] = makecol32(cr,cg,cb);
                }
            }
        }
        else    if (oi->im.data_type == IS_RGB16_PICTURE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imrgb16 = ps[ly].rgb16;
                zl1 = (unsigned int*)(bmp->line[pye-i-1]); // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    switch (mode)
                    {
                        case GREY_LEVEL:
                            rzi = (imrgb16[k].r - zbi)* zgi;
                            rzi += (imrgb16[k].g - zbi)* zgi;
                            rzi += (imrgb16[k].b - zbi)* zgi;
                            rzi /= 3;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case R_LEVEL:
                            rzi = (imrgb16[k].r - zbi)* zgi;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case RED_ONLY:
                            rzi = (imrgb16[k].r - zbi)* zgi;
                            gzi = 0;
                            bzi = 0;
                            break;
                        case G_LEVEL:
                            gzi = (imrgb16[k].g - zbi)* zgi;
                            bzi = gzi;
                            rzi = gzi;
                            break;
                        case GREEN_ONLY:
                            gzi = (imrgb16[k].g - zbi)* zgi;
                            bzi = 0;
                            rzi = 0;
                            break;
                        case B_LEVEL:
                            bzi = (imrgb16[k].b - zbi)* zgi;
                            gzi = bzi;
                            rzi = bzi;
                            break;
                        case BLUE_ONLY:
                            bzi = (imrgb16[k].b - zbi)* zgi;
                            gzi = 0;
                            rzi = 0;
                            break;
                        default:
                            rzi = (imrgb16[k].r - zbi)* zgi;
                            gzi = (imrgb16[k].g - zbi)* zgi;
                            bzi = (imrgb16[k].b - zbi)* zgi;
                            break;
                    }
                    rzi = (rzi < 0 ) ? 0 : (rzi >> 16);
                    gzi = (gzi < 0 ) ? 0 : (gzi >> 16);
                    bzi = (bzi < 0 ) ? 0 : (bzi >> 16);
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    zl1[j-spxs] = makecol32(cr,cg,cb);
                }
            }
        }
        else    if (oi->im.data_type == IS_RGBA_PICTURE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imrgba = ps[ly].rgba;
                zl1 = (unsigned int*)(bmp->line[pye-i-1]); // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    switch (mode)
                    {
                        case GREY_LEVEL:
                            rzi = (imrgba[k].r - zbi)* zgi;
                            rzi += (imrgba[k].g - zbi)* zgi;
                            rzi += (imrgba[k].b - zbi)* zgi;
                            rzi /= 3;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case R_LEVEL:
                            rzi = (imrgba[k].r - zbi)* zgi;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case RED_ONLY:
                            rzi = (imrgba[k].r - zbi)* zgi;
                            gzi = 0;
                            bzi = 0;
                            break;
                        case G_LEVEL:
                            gzi = (imrgba[k].g - zbi)* zgi;
                            bzi = gzi;
                            rzi = gzi;
                            break;
                        case GREEN_ONLY:
                            gzi = (imrgba[k].g - zbi)* zgi;
                            bzi = 0;
                            rzi = 0;
                            break;
                        case B_LEVEL:
                            bzi = (imrgba[k].b - zbi)* zgi;
                            gzi = bzi;
                            rzi = bzi;
                            break;
                        case BLUE_ONLY:
                            bzi = (imrgba[k].b - zbi)* zgi;
                            gzi = 0;
                            rzi = 0;
                            break;
                        default:
                            rzi = (imrgba[k].r - zbi)* zgi;
                            gzi = (imrgba[k].g - zbi)* zgi;
                            bzi = (imrgba[k].b - zbi)* zgi;
                            break;
                    }
                    rzi = (rzi < 0 ) ? 0 : (rzi >> 16);
                    gzi = (gzi < 0 ) ? 0 : (gzi >> 16);
                    bzi = (bzi < 0 ) ? 0 : (bzi >> 16);
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    zl1[j-spxs] = makecol32(cr,cg,cb);
                }
            }
        }
        else    if (oi->im.data_type == IS_RGBA16_PICTURE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imrgba16 = ps[ly].rgba16;
                zl1 = (unsigned int*)(bmp->line[pye-i-1]); // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    switch (mode)
                    {
                        case GREY_LEVEL:
                            rzi = (imrgba16[k].r - zbi)* zgi;
                            rzi += (imrgba16[k].g - zbi)* zgi;
                            rzi += (imrgba16[k].b - zbi)* zgi;
                            rzi /= 3;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case R_LEVEL:
                            rzi = (imrgba16[k].r - zbi)* zgi;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case RED_ONLY:
                            rzi = (imrgba16[k].r - zbi)* zgi;
                            gzi = 0;
                            bzi = 0;
                            break;
                        case G_LEVEL:
                            gzi = (imrgba16[k].g - zbi)* zgi;
                            bzi = gzi;
                            rzi = gzi;
                            break;
                        case GREEN_ONLY:
                            gzi = (imrgba16[k].g - zbi)* zgi;
                            bzi = 0;
                            rzi = 0;
                            break;
                        case B_LEVEL:
                            bzi = (imrgba16[k].b - zbi)* zgi;
                            gzi = bzi;
                            rzi = bzi;
                            break;
                        case BLUE_ONLY:
                            bzi = (imrgba16[k].b - zbi)* zgi;
                            gzi = 0;
                            rzi = 0;
                            break;
                        default:
                            rzi = (imrgba16[k].r - zbi)* zgi;
                            gzi = (imrgba16[k].g - zbi)* zgi;
                            bzi = (imrgba16[k].b - zbi)* zgi;
                            break;
                    }
                    rzi = (rzi < 0 ) ? 0 : (rzi >> 16);
                    gzi = (gzi < 0 ) ? 0 : (gzi >> 16);
                    bzi = (bzi < 0 ) ? 0 : (bzi >> 16);
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    zl1[j-spxs] = makecol32(cr,cg,cb);
                }
            }
        }
        else     if (oi->im.data_type == IS_INT_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imin = ps[ly].in;
                zl1 = (unsigned int*)(bmp->line[pye-i-1]);                 // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    zi = (imin[k] - zbi)*zgi;
                    zi = (zi < 0 ) ? 0 : (zi >> 16);
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        else     if (oi->im.data_type == IS_UINT_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imui = ps[ly].ui;
                zl1 = (unsigned int*)(bmp->line[pye-i-1]);                 // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    zi = (imui[k] - zbi)*zgi;
                    zi = (zi < 0 ) ? 0 : (zi >> 16);
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        else     if (oi->im.data_type == IS_LINT_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imli = ps[ly].li;
                zl1 = (unsigned int*)(bmp->line[pye-i-1]);                 // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    z = (imli[k] - oi->z_black)*zg;
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        else     if (oi->im.data_type == IS_FLOAT_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imfl = ps[ly].fl;
                zl1 = (unsigned int*)(bmp->line[pye-i-1]);                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    z = (imfl[k] - oi->z_black)*zg;
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        else     if (oi->im.data_type == IS_DOUBLE_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imdb = ps[ly].db;
                zl1 = (unsigned int*)(bmp->line[pye-i-1]);                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    z = (imdb[k] - oi->z_black)*zg;
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        else     if (oi->im.data_type == IS_COMPLEX_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imfl = ps[ly].fl;
                zl1 = (unsigned int*)(bmp->line[pye-i-1]);                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    switch (mode)
                    {
                        case AMP:
                            z = imfl[2*(k)];
                            z *= z;
                            z1 = imfl[2*(k)+1];
                            z1 *= z1;
                            z += z1;
                            z = sqrt(z);
                            z = (z - oi->z_black)*zg;
                            break;
                        case LOG_AMP:
                            z = imfl[2*(k)];
                            z *= z;
                            z1 = imfl[2*(k)+1];
                            z1 *= z1;
                            z += z1;
                            z = (z > 0) ? log10(z) : -40;
                            z = (z - oi->z_black)*zg;
                            break;
                        case AMP_2:
                            z = imfl[2*(k)];
                            z *= z;
                            z1 = imfl[2*(k)+1];
                            z1 *= z1;
                            z += z1;
                            z = (z - oi->z_black)*zg;
                            break;
                        case RE:
                            z = imfl[2*(k)];
                            z = (z - oi->z_black)*zg;
                            break;
                        case IM:
                            z = imfl[2*(k)+1];
                            z = (z - oi->z_black)*zg;
                            break;
                    };
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        else     if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imdb = ps[ly].db;
                zl1 = (unsigned int*)(bmp->line[pye-i-1]);                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    switch (mode)
                    {
                        case AMP:
                            z = imdb[2*(k)];
                            z *= z;
                            z1 = imdb[2*(k)+1];
                            z1 *= z1;
                            z += z1;
                            z = sqrt(z);
                            z = (z - oi->z_black)*zg;
                            break;
                        case LOG_AMP:
                            z = imdb[2*(k)];
                            z *= z;
                            z1 = imdb[2*(k)+1];
                            z1 *= z1;
                            z += z1;
                            z = (z > 0) ? log10(z) : -40;
                            z = (z - oi->z_black)*zg;
                            break;
                        case AMP_2:
                            z = imdb[2*(k)];
                            z *= z;
                            z1 = imdb[2*(k)+1];
                            z1 *= z1;
                            z += z1;
                            z = (z - oi->z_black)*zg;
                            break;
                        case RE:
                            z = imdb[2*(k)];
                            z = (z - oi->z_black)*zg;
                            break;
                        case IM:
                            z = imdb[2*(k)+1];
                            z = (z - oi->z_black)*zg;
                            break;
                    };
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        release_bitmap(bmp);
    }
    else if ( sc * nxef == (int)(PIXEL_0 * 512 * oi->width +.5) &&
              sc * nyef == (int)(PIXEL_0 * 512 * oi->height +.5))
    {
        w_im = 2 * nxef;
        spxe = (spxe > w_im) ? w_im : spxe;
        h_im = 2 * nyef;
        spye = (spye > h_im) ? h_im : spye;
        pxs = spxs/2;
        pys = spys/2;
        pxe = spxe/2;
        pye = spye/2;

        if (oi->bmp.stuff == NULL)
        {
            bmp = create_bitmap(spxe-spxs+1,spye-spys+1);
            oi->bmp.stuff = (void*)bmp;
            if (bmp == NULL)
            {
                SET_BITMAP_NO_MORE_IN_USE(oi);
                return xvin_error(Out_Of_Memory);
            }
        }
        else bmp = (BITMAP*)oi->bmp.stuff;

        if (bmp->w != spxe-spxs+1 || bmp->h != spye-spys+1)
            //else if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL) && (oi->bmp.sub_type != sc))
        {
            v_bmp = (is_video_bitmap((BITMAP*)oi->bmp.stuff) == TRUE) ? 1 : 0;
            destroy_bitmap((BITMAP*)oi->bmp.stuff);
            bmp = NULL;
            if (v_bmp) bmp = create_video_bitmap(spxe-spxs+1,spye-spys+1);
            if (bmp == NULL) bmp = create_bitmap(spxe-spxs+1,spye-spys+1);
            oi->bmp.stuff = (void*)bmp;
            if (bmp == NULL)
            {
                SET_BITMAP_NO_MORE_IN_USE(oi);
                return xvin_error(Out_Of_Memory);
            }
        }
        else bmp = (BITMAP*)oi->bmp.stuff;
        oi->bmp.to = IS_BITMAP;
        oi->bmp.sub_type = sc;
        acquire_bitmap(bmp);
        if (oi->im.data_type == IS_CHAR_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imc = ps[ly].ch;
                zl1 = (unsigned int*)(bmp->line[2*pye-2*i-1]);                                         // was unsigned long*
                zl2 = (unsigned int*)(bmp->line[2*pye-2*i-2]);                                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    zi = (imc[k] - zbi)*zgi;
                    zi = (zi < 0 ) ? 0 : (zi >> 16);
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    c32 = makecol32(c,c,c);
                    zl1[2*j-spxs] = zl1[2*j+1-spxs] = c32;
                    zl2[2*j-spxs] = zl2[2*j+1-spxs] = c32;
                }
            }
        }
        else            if (oi->im.data_type == IS_RGB_PICTURE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imc = ps[ly].ch;
                zl1 = (unsigned int*)(bmp->line[2*pye-2*i-1]);                                         // was unsigned long*
                zl2 = (unsigned int*)(bmp->line[2*pye-2*i-2]);                                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    switch (mode)
                    {
                        case GREY_LEVEL:
                            rzi = (imc[3*(k)] - zbi)* zgi;
                            rzi += (imc[3*(k)+1] - zbi)* zgi;
                            rzi += (imc[3*(k)+2] - zbi)* zgi;
                            rzi /= 3;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case R_LEVEL:
                            rzi = (imc[3*(k)] - zbi)* zgi;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case RED_ONLY:
                            rzi = (imc[3*(k)] - zbi)* zgi;
                            gzi = 0;
                            bzi = 0;
                            break;
                        case G_LEVEL:
                            gzi = (imc[3*(k)+1] - zbi)* zgi;
                            bzi = gzi;
                            rzi = gzi;
                            break;
                        case GREEN_ONLY:
                            gzi = (imc[3*(k)+1] - zbi)* zgi;
                            bzi = 0;
                            rzi = 0;
                            break;
                        case B_LEVEL:
                            bzi = (imc[3*(k)+2] - zbi)* zgi;
                            gzi = bzi;
                            rzi = bzi;
                            break;
                        case BLUE_ONLY:
                            bzi = (imc[3*(k)+2] - zbi)* zgi;
                            gzi = 0;
                            rzi = 0;
                            break;
                        default:
                            rzi = (imc[3*(k)] - zbi)* zgi;
                            gzi = (imc[3*(k)+1] - zbi)* zgi;
                            bzi = (imc[3*(k)+2] - zbi)* zgi;
                            break;
                    }
                    rzi = (rzi < 0 ) ? 0 : (rzi >> 16);
                    gzi = (gzi < 0 ) ? 0 : (gzi >> 16);
                    bzi = (bzi < 0 ) ? 0 : (bzi >> 16);
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    c32 = makecol32(cr,cg,cb);
                    zl1[2*j-spxs] = zl1[2*j+1-spxs] = c32;
                    zl2[2*j-spxs] = zl2[2*j+1-spxs] = c32;
                }
            }
        }
        else            if (oi->im.data_type == IS_RGB16_PICTURE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imrgb16 = ps[ly].rgb16;
                zl1 = (unsigned int*)(bmp->line[2*pye-2*i-1]);                                         // was unsigned long*
                zl2 = (unsigned int*)(bmp->line[2*pye-2*i-2]);                                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    switch (mode)
                    {
                        case GREY_LEVEL:
                            rzi = (imrgb16[k].r - zbi)* zgi;
                            rzi += (imrgb16[k].g - zbi)* zgi;
                            rzi += (imrgb16[k].b - zbi)* zgi;
                            rzi /= 3;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case R_LEVEL:
                            rzi = (imrgb16[k].r - zbi)* zgi;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case RED_ONLY:
                            rzi = (imrgb16[k].r - zbi)* zgi;
                            gzi = 0;
                            bzi = 0;
                            break;
                        case G_LEVEL:
                            gzi = (imrgb16[k].g - zbi)* zgi;
                            bzi = gzi;
                            rzi = gzi;
                            break;
                        case GREEN_ONLY:
                            gzi = (imrgb16[k].g - zbi)* zgi;
                            bzi = 0;
                            rzi = 0;
                            break;
                        case B_LEVEL:
                            bzi = (imrgb16[k].b - zbi)* zgi;
                            gzi = bzi;
                            rzi = bzi;
                            break;
                        case BLUE_ONLY:
                            bzi = (imrgb16[k].b - zbi)* zgi;
                            gzi = 0;
                            rzi = 0;
                            break;
                        default:
                            rzi = (imrgb16[k].r - zbi)* zgi;
                            gzi = (imrgb16[k].g - zbi)* zgi;
                            bzi = (imrgb16[k].b - zbi)* zgi;
                            break;
                    }
                    rzi = (rzi < 0 ) ? 0 : (rzi >> 16);
                    gzi = (gzi < 0 ) ? 0 : (gzi >> 16);
                    bzi = (bzi < 0 ) ? 0 : (bzi >> 16);
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    c32 = makecol32(cr,cg,cb);
                    zl1[2*j-spxs] = zl1[2*j+1-spxs] = c32;
                    zl2[2*j-spxs] = zl2[2*j+1-spxs] = c32;
                }
            }
        }
        else            if (oi->im.data_type == IS_RGBA_PICTURE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imrgba = ps[ly].rgba;
                zl1 = (unsigned int*)(bmp->line[2*pye-2*i-1]);                                         // was unsigned long*
                zl2 = (unsigned int*)(bmp->line[2*pye-2*i-2]);                                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    switch (mode)
                    {
                        case GREY_LEVEL:
                            rzi = (imrgba[k].r - zbi)* zgi;
                            rzi += (imrgba[k].g - zbi)* zgi;
                            rzi += (imrgba[k].b - zbi)* zgi;
                            rzi /= 3;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case R_LEVEL:
                            rzi = (imrgba[k].r - zbi)* zgi;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case RED_ONLY:
                            rzi = (imrgba[k].r - zbi)* zgi;
                            gzi = 0;
                            bzi = 0;
                            break;
                        case G_LEVEL:
                            gzi = (imrgba[k].g - zbi)* zgi;
                            bzi = gzi;
                            rzi = gzi;
                            break;
                        case GREEN_ONLY:
                            gzi = (imrgba[k].g - zbi)* zgi;
                            bzi = 0;
                            rzi = 0;
                            break;
                        case B_LEVEL:
                            bzi = (imrgba[k].b - zbi)* zgi;
                            gzi = bzi;
                            rzi = bzi;
                            break;
                        case BLUE_ONLY:
                            bzi = (imrgba[k].b - zbi)* zgi;
                            gzi = 0;
                            rzi = 0;
                            break;
                        default:
                            rzi = (imrgba[k].r - zbi)* zgi;
                            gzi = (imrgba[k].g - zbi)* zgi;
                            bzi = (imrgba[k].b - zbi)* zgi;
                            break;
                    }
                    rzi = (rzi < 0 ) ? 0 : (rzi >> 16);
                    gzi = (gzi < 0 ) ? 0 : (gzi >> 16);
                    bzi = (bzi < 0 ) ? 0 : (bzi >> 16);
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    c32 = makecol32(cr,cg,cb);
                    zl1[2*j-spxs] = zl1[2*j+1-spxs] = c32;
                    zl2[2*j-spxs] = zl2[2*j+1-spxs] = c32;
                }
            }
        }
        else            if (oi->im.data_type == IS_RGBA16_PICTURE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imrgba16 = ps[ly].rgba16;
                zl1 = (unsigned int*)(bmp->line[2*pye-2*i-1]);                                         // was unsigned long*
                zl2 = (unsigned int*)(bmp->line[2*pye-2*i-2]);                                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j + oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx-1;
                    switch (mode)
                    {
                        case GREY_LEVEL:
                            rzi = (imrgba16[k].r - zbi)* zgi;
                            rzi += (imrgba16[k].g - zbi)* zgi;
                            rzi += (imrgba16[k].b - zbi)* zgi;
                            rzi /= 3;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case R_LEVEL:
                            rzi = (imrgba16[k].r - zbi)* zgi;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case RED_ONLY:
                            rzi = (imrgba16[k].r - zbi)* zgi;
                            gzi = 0;
                            bzi = 0;
                            break;
                        case G_LEVEL:
                            gzi = (imrgba16[k].g - zbi)* zgi;
                            bzi = gzi;
                            rzi = gzi;
                            break;
                        case GREEN_ONLY:
                            gzi = (imrgba16[k].g - zbi)* zgi;
                            bzi = 0;
                            rzi = 0;
                            break;
                        case B_LEVEL:
                            bzi = (imrgba16[k].b - zbi)* zgi;
                            gzi = bzi;
                            rzi = bzi;
                            break;
                        case BLUE_ONLY:
                            bzi = (imrgba16[k].b - zbi)* zgi;
                            gzi = 0;
                            rzi = 0;
                            break;
                        default:
                            rzi = (imrgba16[k].r - zbi)* zgi;
                            gzi = (imrgba16[k].g - zbi)* zgi;
                            bzi = (imrgba16[k].b - zbi)* zgi;
                            break;
                    }
                    rzi = (rzi < 0 ) ? 0 : (rzi >> 16);
                    gzi = (gzi < 0 ) ? 0 : (gzi >> 16);
                    bzi = (bzi < 0 ) ? 0 : (bzi >> 16);
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    c32 = makecol32(cr,cg,cb);
                    zl1[2*j-spxs] = zl1[2*j+1-spxs] = c32;
                    zl2[2*j-spxs] = zl2[2*j+1-spxs] = c32;
                }
            }
        }
        else     if (oi->im.data_type == IS_INT_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imin = ps[ly].in;
                zl1 = (unsigned int*)(bmp->line[2*pye-2*i-1]);                                         // was unsigned long*
                zl2 = (unsigned int*)(bmp->line[2*pye-2*i-2]);                                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
                    zi = (imin[j+oi->im.nxs] - zbi) * zgi;
                    zi = (zi < 0 ) ? 0 : (zi >> 16);
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    c32 = makecol32(c,c,c);
                    zl1[2*j-spxs] = zl1[2*j+1-spxs] = c32;
                    zl2[2*j-spxs] = zl2[2*j+1-spxs] = c32;
                }
            }
        }
        else     if (oi->im.data_type == IS_UINT_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imui = ps[ly].ui;
                zl1 = (unsigned int*)(bmp->line[2*pye-2*i-1]);                                         // was unsigned long*
                zl2 = (unsigned int*)(bmp->line[2*pye-2*i-2]);                                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
                    zi = (imui[j+oi->im.nxs] - zbi) * zgi;
                    zi = (zi < 0 ) ? 0 : (zi >> 16);
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    c32 = makecol32(c,c,c);
                    zl1[2*j-spxs] = zl1[2*j+1-spxs] = c32;
                    zl2[2*j-spxs] = zl2[2*j+1-spxs] = c32;
                }
            }
        }
        else     if (oi->im.data_type == IS_LINT_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imli = ps[ly].li;
                zl1 = (unsigned int*)(bmp->line[2*pye-2*i-1]);                                         // was unsigned long*
                zl2 = (unsigned int*)(bmp->line[2*pye-2*i-2]);                                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
                    z = (imli[j+oi->im.nxs] - oi->z_black) * zg;
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    c32 = makecol32(c,c,c);
                    zl1[2*j-spxs] = zl1[2*j+1-spxs] = c32;
                    zl2[2*j-spxs] = zl2[2*j+1-spxs] = c32;
                }
            }
        }
        else     if (oi->im.data_type == IS_FLOAT_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imfl = ps[ly].fl;
                zl1 = (unsigned int*)(bmp->line[2*pye-2*i-1]);                                         // was unsigned long*
                zl2 = (unsigned int*)(bmp->line[2*pye-2*i-2]);                                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
                    z = (imfl[j+oi->im.nxs] - oi->z_black) * zg;
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    c32 = makecol32(c,c,c);
                    zl1[2*j-spxs] = zl1[2*j+1-spxs] = c32;
                    zl2[2*j-spxs] = zl2[2*j+1-spxs] = c32;
                }
            }
        }
        else     if (oi->im.data_type == IS_DOUBLE_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imdb = ps[ly].db;
                zl1 = (unsigned int*)(bmp->line[2*pye-2*i-1]);                                         // was unsigned long*
                zl2 = (unsigned int*)(bmp->line[2*pye-2*i-2]);                                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
                    z = (imdb[j+oi->im.nxs] - oi->z_black) * zg;
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    c32 = makecol32(c,c,c);
                    zl1[2*j-spxs] = zl1[2*j+1-spxs] = c32;
                    zl2[2*j-spxs] = zl2[2*j+1-spxs] = c32;
                }
            }
        }
        else     if (oi->im.data_type == IS_COMPLEX_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imfl = ps[ly].fl;
                zl1 = (unsigned int*)(bmp->line[2*pye-2*i-1]);                                         // was unsigned long*
                zl2 = (unsigned int*)(bmp->line[2*pye-2*i-2]);                                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j+oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx;
                    switch (mode)
                    {
                        case AMP:
                            z = imfl[2*k];
                            z *= z;
                            z1 = imfl[2*k+1];
                            z1 *= z1;
                            z += z1;
                            z = sqrt(z);
                            z = (z - oi->z_black)*zg;
                            break;
                        case LOG_AMP:
                            z = imfl[2*k];
                            z *= z;
                            z1 = imfl[2*k+1];
                            z1 *= z1;
                            z += z1;
                            z = (z > 0) ? log10(z) : -40;
                            z = (z - oi->z_black)*zg;
                            break;
                        case AMP_2:
                            z = imfl[2*k];
                            z *= z;
                            z1 = imfl[2*k+1];
                            z1 *= z1;
                            z += z1;
                            z = (z - oi->z_black)*zg;
                            break;
                        case RE:
                            z = imfl[2*k];
                            z = (z - oi->z_black)*zg;
                            break;
                        case IM:
                            z = imfl[2*k+1];
                            z = (z - oi->z_black)*zg;
                            break;
                    };
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    c32 = makecol32(c,c,c);
                    zl1[2*j-spxs] = zl1[2*j+1-spxs] = c32;
                    zl2[2*j-spxs] = zl2[2*j+1-spxs] = c32;
                }
            }
        }
        else     if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
        {
            for (i = pys; i < pye; i++)
            {
	        ly = i+oi->im.nys;
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny -1;
                imdb = ps[ly].db;
                zl1 = (unsigned int*)(bmp->line[2*pye-2*i-1]);                                         // was unsigned long*
                zl2 = (unsigned int*)(bmp->line[2*pye-2*i-2]);                                         // was unsigned long*
                for (j = pxs; j < pxe; j++)
                {
		    k = j+oi->im.nxs;
		    k = (k < 0) ? 0 : k;
		    k = (k < oi->im.nx) ? k : oi->im.nx;
                    switch (mode)
                    {
                        case AMP:
                            z = imdb[2*k];
                            z *= z;
                            z1 = imdb[2*k+1];
                            z1 *= z1;
                            z += z1;
                            z = sqrt(z);
                            z = (z - oi->z_black)*zg;
                            break;
                        case LOG_AMP:
                            z = imdb[2*k];
                            z *= z;
                            z1 = imdb[2*k+1];
                            z1 *= z1;
                            z += z1;
                            z = (z > 0) ? log10(z) : -40;
                            z = (z - oi->z_black)*zg;
                            break;
                        case AMP_2:
                            z = imdb[2*k];
                            z *= z;
                            z1 = imdb[2*k+1];
                            z1 *= z1;
                            z += z1;
                            z = (z - oi->z_black)*zg;
                            break;
                        case RE:
                            z = imdb[2*k];
                            z = (z - oi->z_black)*zg;
                            break;
                        case IM:
                            z = imdb[2*k+1];
                            z = (z - oi->z_black)*zg;
                            break;
                    };
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    c32 = makecol32(c,c,c);
                    zl1[2*j-spxs] = zl1[2*j+1-spxs] = c32;
                    zl2[2*j-spxs] = zl2[2*j+1-spxs] = c32;
                }
            }
        }
        release_bitmap(bmp);
    }
    else if ( sc * nxef == (int)(PIXEL_2 * 512 * oi->width +.5) &&
              sc * nyef == (int)(PIXEL_2 * 512 * oi->height +.5))
    {
        w_im = oi->im.nxe-oi->im.nxs;
        spxe = (spxe > w_im/2) ? w_im/2 : spxe;
        h_im = oi->im.nye-oi->im.nys;
        spye = (spye > h_im/2) ? h_im/2 : spye;
        pxs = 2*spxs;
        pys = 2*spys;
        pxe = 2*spxe;
        pye = 2*spye;
        if (oi->bmp.stuff == NULL)
        {
            bmp = create_bitmap(spxe-spxs+1,spye-spys+1);
            oi->bmp.stuff = (void*)bmp;
            if (bmp == NULL)
            {
                SET_BITMAP_NO_MORE_IN_USE(oi);
                return xvin_error(Out_Of_Memory);
            }
        }
        else bmp = (BITMAP*)oi->bmp.stuff;

        if (bmp->w != spxe-spxs+1 || bmp->h != spye-spys+1)
            //else if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL) && (oi->bmp.sub_type != sc))
        {
            v_bmp = (is_video_bitmap((BITMAP*)oi->bmp.stuff) == TRUE) ? 1 : 0;
            destroy_bitmap((BITMAP*)oi->bmp.stuff);
            bmp = NULL;
            if (v_bmp) bmp = create_video_bitmap(spxe-spxs+1,spye-spys+1);
            if (bmp == NULL) bmp = create_bitmap(spxe-spxs+1,spye-spys+1);
            oi->bmp.stuff = (void*)bmp;
            if (bmp == NULL)
            {
                SET_BITMAP_NO_MORE_IN_USE(oi);
                return xvin_error(Out_Of_Memory);
            }
        }
        else bmp = (BITMAP*)oi->bmp.stuff;
        oi->bmp.to = IS_BITMAP;
        acquire_bitmap(bmp);
        oi->bmp.sub_type = sc;
        if (oi->im.data_type == IS_CHAR_IMAGE)
        {
            for (i = spys; i < spye; i++)
            {
	        ly = 2*(i+oi->im.nys);
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
		imc = ps[ly].ch;
		ly++;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imc1 = ps[ly].ch;
                zl1 = (unsigned int*)(bmp->line[spye-i-1]);                                         // was unsigned long*
                for (j = spxs; j < spxe; j++)
                {
                    k = 2*(j + oi->im.nxs);
		    k = (k < 0) ? 0 : k;
		    k = (k+1 < oi->im.nx) ? k : oi->im.nx-2;
                    zi = imc[k] + imc1[k] + imc[k+1] + imc1[k+1];
                    zi = (zi - (zbi << 2)) * zgi;
                    zi = (zi < 0 ) ? 0 : (zi >> 18);
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        else if (oi->im.data_type == IS_RGB_PICTURE)
        {
            for (i = spys; i < spye; i++)
            {
	        ly = 2*(i+oi->im.nys);
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
		imc = ps[ly].ch;
		ly++;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imc1 = ps[ly].ch;
                zl1 = (unsigned int*)(bmp->line[spye-i-1]);                                         // was unsigned long*
                for (j = spxs; j < spxe; j++)
                {
                    k = 6*(j + oi->im.nxs);
		    k = (k < 0) ? 0 : k;
		    k = (k+5 < 3*oi->im.nx) ? k : 3*oi->im.nx-6;
                    switch (mode)
                    {
                        case GREY_LEVEL:
                            rzi = imc[k] + imc1[k] + imc[k+3] + imc1[k+3];
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = imc[k+1] + imc1[k+1] + imc[k+4] + imc1[k+4];
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = imc[k+2] + imc1[k+2] + imc[k+5] + imc1[k+5];
                            bzi = (bzi - (zbi << 2))* zgi;
                            rzi += gzi + bzi;
                            rzi /= 3;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case R_LEVEL:
                            rzi = imc[k] + imc1[k] + imc[k+3] + imc1[k+3];
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case RED_ONLY:
                            rzi = imc[k] + imc1[k] + imc[k+3] + imc1[k+3];
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = 0;
                            bzi = 0;
                            break;
                        case G_LEVEL:
                            gzi = imc[k+1] + imc1[k+1] + imc[k+4] + imc1[k+4];
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = gzi;
                            rzi = gzi;
                            break;
                        case GREEN_ONLY:
                            gzi = imc[k+1] + imc1[k+1] + imc[k+4] + imc1[k+4];
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = 0;
                            rzi = 0;
                            break;
                        case B_LEVEL:
                            bzi = imc[k+2] + imc1[k+2] + imc[k+5] + imc1[k+5];
                            bzi = (bzi - (zbi << 2))* zgi;
                            gzi = bzi;
                            rzi = bzi;
                            break;
                        case BLUE_ONLY:
                            bzi = imc[k+2] + imc1[k+2] + imc[k+5] + imc1[k+5];
                            bzi = (bzi - (zbi << 2))* zgi;
                            gzi = 0;
                            rzi = 0;
                            break;
                        default:
                            rzi = imc[k] + imc1[k] + imc[k+3] + imc1[k+3];
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = imc[k+1] + imc1[k+1] + imc[k+4] + imc1[k+4];
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = imc[k+2] + imc1[k+2] + imc[k+5] + imc1[k+5];
                            bzi = (bzi - (zbi << 2))* zgi;
                            break;
                    }
                    rzi = (rzi < 0 ) ? 0 : (rzi >> 18);
                    gzi = (gzi < 0 ) ? 0 : (gzi >> 18);
                    bzi = (bzi < 0 ) ? 0 : (bzi >> 18);
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    zl1[j-spxs] = makecol32(cr,cg,cb);
                }
            }
        }
        else if (oi->im.data_type == IS_RGB16_PICTURE)
        {
            for (i = spys; i < spye; i++)
            {
	        ly = 2*(i+oi->im.nys);
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
		imrgb16 = ps[ly].rgb16;
		ly++;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imrgb16_1 = ps[ly].rgb16;
                zl1 = (unsigned int*)(bmp->line[spye-i-1]);                                         // was unsigned long*
                for (j = spxs; j < spxe; j++)
                {
                    k = 2*(j + oi->im.nxs);
		    k = (k < 0) ? 0 : k;
		    k = (k+1 < oi->im.nx) ? k : oi->im.nx-2;
                    switch (mode)
                    {
                        case GREY_LEVEL:
                            rzi = imrgb16[k].r + imrgb16_1[k].r + imrgb16[k+1].r + imrgb16_1[k+1].r;
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = imrgb16[k].g + imrgb16_1[k].g + imrgb16[k+1].g + imrgb16_1[k+1].g;
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = imrgb16[k].b + imrgb16_1[k].b + imrgb16[k+1].b + imrgb16_1[k+1].b;
                            bzi = (bzi - (zbi << 2))* zgi;
                            rzi += gzi + bzi;
                            rzi /= 3;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case R_LEVEL:
                            rzi = imrgb16[k].r + imrgb16_1[k].r + imrgb16[k+1].r + imrgb16_1[k+1].r;
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case RED_ONLY:
                            rzi = imrgb16[k].r + imrgb16_1[k].r + imrgb16[k+1].r + imrgb16_1[k+1].r;
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = 0;
                            bzi = 0;
                            break;
                        case G_LEVEL:
                            gzi = imrgb16[k].g + imrgb16_1[k].g + imrgb16[k+1].g + imrgb16_1[k+1].g;
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = gzi;
                            rzi = gzi;
                            break;
                        case GREEN_ONLY:
                            gzi = imrgb16[k].g + imrgb16_1[k].g + imrgb16[k+1].g + imrgb16_1[k+1].g;
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = 0;
                            rzi = 0;
                            break;
                        case B_LEVEL:
                            bzi = imrgb16[k].b + imrgb16_1[k].b + imrgb16[k+1].b + imrgb16_1[k+1].b;
                            bzi = (bzi - (zbi << 2))* zgi;
                            gzi = bzi;
                            rzi = bzi;
                            break;
                        case BLUE_ONLY:
                            bzi = imrgb16[k].b + imrgb16_1[k].b + imrgb16[k+1].b + imrgb16_1[k+1].b;
                            bzi = (bzi - (zbi << 2))* zgi;
                            gzi = 0;
                            rzi = 0;
                            break;
                        default:
                            rzi = imrgb16[k].r + imrgb16_1[k].r + imrgb16[k+1].r + imrgb16_1[k+1].r;
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = imrgb16[k].g + imrgb16_1[k].g + imrgb16[k+1].g + imrgb16_1[k+1].g;
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = imrgb16[k].b + imrgb16_1[k].b + imrgb16[k+1].b + imrgb16_1[k+1].b;
                            bzi = (bzi - (zbi << 2))* zgi;
                            break;
                    }
                    rzi = (rzi < 0 ) ? 0 : (rzi >> 18);
                    gzi = (gzi < 0 ) ? 0 : (gzi >> 18);
                    bzi = (bzi < 0 ) ? 0 : (bzi >> 18);
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    zl1[j-spxs] = makecol32(cr,cg,cb);
                }
            }
        }
        else if (oi->im.data_type == IS_RGBA_PICTURE)
        {
            for (i = spys; i < spye; i++)
            {
	        ly = 2*(i+oi->im.nys);
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imrgba = ps[ly].rgba;
		ly++;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imrgba1 = ps[ly].rgba;
                zl1 = (unsigned int*)(bmp->line[spye-i-1]);                                         // was unsigned long*
                for (j = spxs; j < spxe; j++)
                {
                    k = 2*(j + oi->im.nxs);
		    k = (k < 0) ? 0 : k;
		    k = (k+1 < oi->im.nx) ? k : oi->im.nx-2;
                    switch (mode)
                    {
                        case GREY_LEVEL:
                            rzi = imrgba[k].r + imrgba1[k].r + imrgba[k+1].r + imrgba1[k+1].r;
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = imrgba[k].g + imrgba1[k].g + imrgba[k+1].g + imrgba1[k+1].g;
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = imrgba[k].b + imrgba1[k].b + imrgba[k+1].b + imrgba1[k+1].b;
                            bzi = (bzi - (zbi << 2))* zgi;
                            rzi += gzi + bzi;
                            rzi /= 3;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case R_LEVEL:
                            rzi = imrgba[k].r + imrgba1[k].r + imrgba[k+1].r + imrgba1[k+1].r;
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case RED_ONLY:
                            rzi = imrgba[k].r + imrgba1[k].r + imrgba[k+1].r + imrgba1[k+1].r;
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = 0;
                            bzi = 0;
                            break;
                        case G_LEVEL:
                            gzi = imrgba[k].g + imrgba1[k].g + imrgba[k+1].g + imrgba1[k+1].g;
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = gzi;
                            rzi = gzi;
                            break;
                        case GREEN_ONLY:
                            gzi = imrgba[k].g + imrgba1[k].g + imrgba[k+1].g + imrgba1[k+1].g;
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = 0;
                            rzi = 0;
                            break;
                        case B_LEVEL:
                            bzi = imrgba[k].b + imrgba1[k].b + imrgba[k+1].b + imrgba1[k+1].b;
                            bzi = (bzi - (zbi << 2))* zgi;
                            gzi = bzi;
                            rzi = bzi;
                            break;
                        case BLUE_ONLY:
                            bzi = imrgba[k].b + imrgba1[k].b + imrgba[k+1].b + imrgba1[k+1].b;
                            bzi = (bzi - (zbi << 2))* zgi;
                            gzi = 0;
                            rzi = 0;
                            break;
                        default:
                            rzi = imrgba[k].r + imrgba1[k].r + imrgba[k+1].r + imrgba1[k+1].r;
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = imrgba[k].g + imrgba1[k].g + imrgba[k+1].g + imrgba1[k+1].g;
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = imrgba[k].b + imrgba1[k].b + imrgba[k+1].b + imrgba1[k+1].b;
                            bzi = (bzi - (zbi << 2))* zgi;
                            break;
                    }
                    rzi = (rzi < 0 ) ? 0 : (rzi >> 18);
                    gzi = (gzi < 0 ) ? 0 : (gzi >> 18);
                    bzi = (bzi < 0 ) ? 0 : (bzi >> 18);
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    zl1[j-spxs] = makecol32(cr,cg,cb);
                }
            }
        }
        else if (oi->im.data_type == IS_RGBA16_PICTURE)
        {
            for (i = spys; i < spye; i++)
            {
	        ly = 2*(i+oi->im.nys);
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imrgba16 = ps[ly].rgba16;
		ly++;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imrgba16_1 = ps[ly].rgba16;
                zl1 = (unsigned int*)(bmp->line[spye-i-1]);                                         // was unsigned long*
                for (j = spxs; j < spxe; j++)
                {
                    k = 2*(j + oi->im.nxs);
		    k = (k < 0) ? 0 : k;
		    k = (k+1 < oi->im.nx) ? k : 2*oi->im.nx-1;
                    switch (mode)
                    {
                        case GREY_LEVEL:
                            rzi = imrgba16[k].r + imrgba16_1[k].r + imrgba16[k+1].r + imrgba16_1[k+1].r;
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = imrgba16[k].g + imrgba16_1[k].g + imrgba16[k+1].g + imrgba16_1[k+1].g;
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = imrgba16[k].b + imrgba16_1[k].b + imrgba16[k+1].b + imrgba16_1[k+1].b;
                            bzi = (bzi - (zbi << 2))* zgi;
                            rzi += gzi + bzi;
                            rzi /= 3;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case R_LEVEL:
                            rzi = imrgba16[k].r + imrgba16_1[k].r + imrgba16[k+1].r + imrgba16_1[k+1].r;
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = rzi;
                            bzi = rzi;
                            break;
                        case RED_ONLY:
                            rzi = imrgba16[k].r + imrgba16_1[k].r + imrgba16[k+1].r + imrgba16_1[k+1].r;
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = 0;
                            bzi = 0;
                            break;
                        case G_LEVEL:
                            gzi = imrgba16[k].g + imrgba16_1[k].g + imrgba16[k+1].g + imrgba16_1[k+1].g;
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = gzi;
                            rzi = gzi;
                            break;
                        case GREEN_ONLY:
                            gzi = imrgba16[k].g + imrgba16_1[k].g + imrgba16[k+1].g + imrgba16_1[k+1].g;
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = 0;
                            rzi = 0;
                            break;
                        case B_LEVEL:
                            bzi = imrgba16[k].b + imrgba16_1[k].b + imrgba16[k+1].b + imrgba16_1[k+1].b;
                            bzi = (bzi - (zbi << 2))* zgi;
                            gzi = bzi;
                            rzi = bzi;
                            break;
                        case BLUE_ONLY:
                            bzi = imrgba16[k].b + imrgba16_1[k].b + imrgba16[k+1].b + imrgba16_1[k+1].b;
                            bzi = (bzi - (zbi << 2))* zgi;
                            gzi = 0;
                            rzi = 0;
                            break;
                        default:
                            rzi = imrgba16[k].r + imrgba16_1[k].r + imrgba16[k+1].r + imrgba16_1[k+1].r;
                            rzi = (rzi - (zbi << 2))* zgi;
                            gzi = imrgba16[k].g + imrgba16_1[k].g + imrgba16[k+1].g + imrgba16_1[k+1].g;
                            gzi = (gzi - (zbi << 2))* zgi;
                            bzi = imrgba16[k].b + imrgba16_1[k].b + imrgba16[k+1].b + imrgba16_1[k+1].b;
                            bzi = (bzi - (zbi << 2))* zgi;
                            break;
                    }
                    rzi = (rzi < 0 ) ? 0 : (rzi >> 18);
                    gzi = (gzi < 0 ) ? 0 : (gzi >> 18);
                    bzi = (bzi < 0 ) ? 0 : (bzi >> 18);
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    zl1[j-spxs] = makecol32(cr,cg,cb);
                }
            }
        }
        else     if (oi->im.data_type == IS_INT_IMAGE)
        {
            for (i = spys; i < spye; i++)
            {
	        ly = 2*(i+oi->im.nys);
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imin = ps[ly].in;
		ly++;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imin1 = ps[ly].in;
                zl1 = (unsigned int*)(bmp->line[spye-i-1]);                                         // was unsigned long*
                for (j = spxs; j < spxe; j++)
                {
                    k = 2*(j + oi->im.nxs);
		    k = (k < 0) ? 0 : k;
		    k = (k+1 < oi->im.nx) ? k : oi->im.nx-2;
                    zi = imin[k] + imin1[k] + imin[k+1] + imin1[k+1];
                    zi = (zi - (zbi << 2)) * zgi;
                    zi = (zi < 0 ) ? 0 : (zi >> 18);
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        else     if (oi->im.data_type == IS_UINT_IMAGE)
        {
            for (i = spys; i < spye; i++)
            {
	        ly = 2*(i+oi->im.nys);
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
	        imui = ps[ly].ui;
		ly++;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imui1 = ps[ly].ui;
                zl1 = (unsigned int*)(bmp->line[spye-i-1]);                                         // was unsigned long*
                for (j = spxs; j < spxe; j++)
                {
                    k = 2*(j + oi->im.nxs);
		    k = (k < 0) ? 0 : k;
		    k = (k+1 < oi->im.nx) ? k : oi->im.nx-2;
                    zi = imui[k] + imui1[k] + imui[k+1] + imui1[k+1];
                    zi = (zi - (zbi << 2)) * zgi;
                    zi = (zi < 0 ) ? 0 : (zi >> 18);
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        else     if (oi->im.data_type == IS_LINT_IMAGE)
        {
            for (i = spys; i < spye; i++)
            {
	        ly = 2*(i+oi->im.nys);
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imli = ps[ly].li;
		ly++;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imli1 = ps[ly].li;
                zl1 = (unsigned int*)(bmp->line[spye-i-1]);    // was unsigned long*
                for (j = spxs; j < spxe; j++)
                {
                    k = 2*(j + oi->im.nxs);
		    k = (k < 0) ? 0 : k;
		    k = (k+1 < oi->im.nx) ? k : oi->im.nx-2;
                    z = (float)imli[k] + (float)imli1[k]
                        + (float)imli[k+1] + (float)imli1[k+1];
                    z = ((z/4) - oi->z_black) * zg;
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        else     if (oi->im.data_type == IS_FLOAT_IMAGE)
        {
            for (i = spys; i < spye; i++)
            {
	        ly = 2*(i+oi->im.nys);
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imfl = ps[ly].fl;
		ly++;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imfl1 = ps[ly].fl;
                zl1 = (unsigned int*)(bmp->line[spye-i-1]);    // was unsigned long*
                for (j = spxs; j < spxe; j++)
                {
                    k = 2*(j + oi->im.nxs);
		    k = (k < 0) ? 0 : k;
		    k = (k+1 < oi->im.nx) ? k : oi->im.nx-2;
                    z = imfl[k] + imfl1[k] + imfl[k+1] + imfl1[k+1];
                    z = ((z/4) - oi->z_black) * zg;
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        else     if (oi->im.data_type == IS_DOUBLE_IMAGE)
        {
            for (i = spys; i < spye; i++)
            {
	        ly = 2*(i+oi->im.nys);
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imdb = ps[ly].db;
		ly++;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imdb1 = ps[ly].db;
                zl1 = (unsigned int*)(bmp->line[spye-i-1]);    // was unsigned long*
                for (j = spxs; j < spxe; j++)
                {
                    k = 2*(j + oi->im.nxs);
		    k = (k < 0) ? 0 : k;
		    k = (k+1 < oi->im.nx) ? k : oi->im.nx-2;
                    z = imdb[k] + imdb1[k] + imdb[k+1] + imdb1[k+1];
                    z = ((z/4) - oi->z_black) * zg;
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        else     if (oi->im.data_type == IS_COMPLEX_IMAGE)
        {
            for (i = spys; i < spye; i++)
            {
	        ly = 2*(i+oi->im.nys);
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imfl = ps[ly].fl;
		ly++;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imfl1 = ps[ly].fl;
                zl1 = (unsigned int*)(bmp->line[spye-i-1]);                                         // was unsigned long*
                for (j = spxs; j < spxe; j++)
                {
                    k = 4*(j + oi->im.nxs);
		    k = (k < 0) ? 0 : k;
		    k = (k+3 < 2*oi->im.nx) ? k : (2*oi->im.nx)-4;
		    switch (mode)
                    {
                        case AMP:
                            z = imfl[k]*imfl[k];
                            z += imfl[k+1]*imfl[k+1];
                            z += imfl[k+2]*imfl[k+2];
                            z += imfl[k+3]*imfl[k+3];
                            z += imfl1[k]*imfl1[k];
                            z += imfl1[k+1]*imfl1[k+1];
                            z += imfl1[k+2]*imfl1[k+2];
                            z += imfl1[k+3]*imfl1[k+3];
                            z = sqrt(z);
                            z = (z/4 - oi->z_black)*zg;
                            break;
                        case LOG_AMP:
                            z = imfl[k]*imfl[k];
                            z += imfl[k+1]*imfl[k+1];
                            z += imfl[k+2]*imfl[k+2];
                            z += imfl[k+3]*imfl[k+3];
                            z += imfl1[k]*imfl1[k];
                            z += imfl1[k+1]*imfl1[k+1];
                            z += imfl1[k+2]*imfl1[k+2];
                            z += imfl1[k+3]*imfl1[k+3];
                            z = (z/4 > 0) ? log10(z/4) : -40;
                            z = (z - oi->z_black)*zg;
                            break;
                        case AMP_2:
                            z = imfl[k]*imfl[k];
                            z += imfl[k+1]*imfl[k+1];
                            z += imfl[k+2]*imfl[k+2];
                            z += imfl[k+3]*imfl[k+3];
                            z += imfl1[k]*imfl1[k];
                            z += imfl1[k+1]*imfl1[k+1];
                            z += imfl1[k+2]*imfl1[k+2];
                            z += imfl1[k+3]*imfl1[k+3];
                            z = (z/4 - oi->z_black)*zg;
                            break;
                        case RE:
                            z = imfl[k] + imfl[k+2];
                            z = imfl1[k] + imfl1[k+2];
                            z = (z/4 - oi->z_black)*zg;
                            break;
                        case IM:
                            z = imfl[k+1] + imfl[k+3];
                            z = imfl1[k+1] + imfl1[k+3];
                            z = (z/4 - oi->z_black)*zg;
                            break;
                    };
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        else     if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
        {
            for (i = spys; i < spye; i++)
            {
	        ly = 2*(i+oi->im.nys);
		ly = (ly < 0) ? 0 : ly;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imdb = ps[ly].db;
		ly++;
		ly = (ly < oi->im.ny) ? ly : oi->im.ny-1;
                imdb1 = ps[ly].db;
                zl1 = (unsigned int*)(bmp->line[spye-i-1]);                                         // was unsigned long*
                for (j = spxs; j < spxe; j++)
                {
                    k = 4*(j + oi->im.nxs);
		    k = (k < 0) ? 0 : k;
		    k = (k+3 < oi->im.nx) ? k : (oi->im.nx)-4;
                    switch (mode)
                    {
                        case AMP:
                            z = imdb[k]*imdb[k];
                            z += imdb[k+1]*imdb[k+1];
                            z += imdb[k+2]*imdb[k+2];
                            z += imdb[k+3]*imdb[k+3];
                            z += imdb1[k]*imdb1[k];
                            z += imdb1[k+1]*imdb1[k+1];
                            z += imdb1[k+2]*imdb1[k+2];
                            z += imdb1[k+3]*imdb1[k+3];
                            z = sqrt(z);
                            z = (z/4 - oi->z_black)*zg;
                            break;
                        case LOG_AMP:
                            z = imdb[k]*imdb[k];
                            z += imdb[k+1]*imdb[k+1];
                            z += imdb[k+2]*imdb[k+2];
                            z += imdb[k+3]*imdb[k+3];
                            z += imdb1[k]*imdb1[k];
                            z += imdb1[k+1]*imdb1[k+1];
                            z += imdb1[k+2]*imdb1[k+2];
                            z += imdb1[k+3]*imdb1[k+3];
                            z = (z/4 > 0) ? log10(z/4) : -40;
                            z = (z - oi->z_black)*zg;
                            break;
                        case AMP_2:
                            z = imdb[k]*imdb[k];
                            z += imdb[k+1]*imdb[k+1];
                            z += imdb[k+2]*imdb[k+2];
                            z += imdb[k+3]*imdb[k+3];
                            z += imdb1[k]*imdb1[k];
                            z += imdb1[k+1]*imdb1[k+1];
                            z += imdb1[k+2]*imdb1[k+2];
                            z += imdb1[k+3]*imdb1[k+3];
                            z = (z/4 - oi->z_black)*zg;
                            break;
                        case RE:
                            z = imdb[k] + imdb[k+2];
                            z = imdb1[k] + imdb1[k+2];
                            z = (z/4 - oi->z_black)*zg;
                            break;
                        case IM:
                            z = imdb[k+1] + imdb[k+3];
                            z = imdb1[k+1] + imdb1[k+3];
                            z = (z/4 - oi->z_black)*zg;
                            break;
                    };
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        release_bitmap(bmp);
    }
    else if ( sc != 0)
    {
        snxef = (int)(512 * PIXEL_1 * oi->width) / sc;
        snyef = (int)(512 * PIXEL_1 * oi->height) / sc;
        spxe = (spxe >= snxef) ? snxef : spxe;
        spye = (spye >= snyef) ? snyef : spye;
        snxef = ( snxef == 0) ? 1 : snxef;
        snyef = ( snyef == 0) ? 1 : snyef;
        pxs = (spxs * nxef) / snxef;
        pxe = (spxe * nxef) / snxef;
        pys = (spys * nyef) / snyef;
        pye = (spye * nyef) / snyef;
        if ( spxe - spxs <= 0)
        {
            SET_BITMAP_NO_MORE_IN_USE(oi);
            return 0;
        }
        /*        win_printf("bef alloc bmp");*/

        if (oi->bmp.stuff == NULL)
        {
            bmp = create_bitmap(spxe-spxs+1,spye-spys+1);
            oi->bmp.stuff = (void*)bmp;
            if (bmp == NULL)
            {
                SET_BITMAP_NO_MORE_IN_USE(oi);
                return xvin_error(Out_Of_Memory);
            }
        }
        else bmp = (BITMAP*)oi->bmp.stuff;

        if (bmp->w != spxe-spxs+1 || bmp->h != spye-spys+1)
            //        else if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL) && (oi->bmp.sub_type != sc))
        {
            v_bmp = (is_video_bitmap((BITMAP*)oi->bmp.stuff) == TRUE) ? 1 : 0;
            destroy_bitmap((BITMAP*)oi->bmp.stuff);
            bmp = NULL;
            if (v_bmp) bmp = create_video_bitmap(spxe-spxs+1,spye-spys+1);
            if (bmp == NULL) bmp = create_bitmap(spxe-spxs+1,spye-spys+1);
            oi->bmp.stuff = (void*)bmp;
            if (bmp == NULL)
            {
                SET_BITMAP_NO_MORE_IN_USE(oi);
                return xvin_error(Out_Of_Memory);
            }
        }
        else bmp = (BITMAP*)oi->bmp.stuff;
        /*        win_printf("after alloc bmp");*/
        oi->bmp.to = IS_BITMAP;
        oi->bmp.sub_type = sc;
        acquire_bitmap(bmp);
        for (i = spys; i < spye; i++)
        {
            /*            if (i%64 == 63)    win_printf("line %d",i);*/
            ly = oi->im.nys + (i * nyef) / snyef;
	    ly = (ly < 0) ? 0 : ly;
            ly = ( ly >= oi->im.ny ) ? oi->im.ny - 1 : ly;
            lpy = 8 - (((8 * i * nyef) / snyef) % 8);
            ly1 = oi->im.nys + ((i + 1) * nyef) / snyef;
            ly1 = (ly1 == ly) ? ly + 1 : ly1;
	    ly1 = (ly1 < 0) ? 0 : ly1;
            ly1 = ( ly1 >= oi->im.ny ) ? oi->im.ny - 1 : ly1;
            zl1 = (unsigned int*)(bmp->line[spye-i-1]);             // was unsigned long*
            if (oi->im.data_type == IS_CHAR_IMAGE)
            {
                if (smooth_interpol)
                {
                    imc = ps[ly].ch;
                    imc1 = ps[ly1].ch;
                }
                else imc1 = imc = ps[(ly+ly1)/2].ch;
                for (j = spxs; j < spxe; j++)
                {
                    lx = oi->im.nxs + (j * nxef) / snxef;
		    lx = (lx < 0) ? 0 : lx;
                    lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                    lx1 = oi->im.nxs + ((j + 1) * nxef) / snxef;
                    lx1 = (lx1 == lx) ? lx + 1 : lx1;
		    lx1 = (lx1 < 0) ? 0 : lx1;
                    lx1 = ( lx1 >= oi->im.nx ) ? oi->im.nx - 1 : lx1;
                    if (smooth_interpol)
                    {
                        lpx = 8 -(((8 * j * nxef) / snxef) % 8);
                        zi = lpy * (lpx * (int)imc[lx] + (8-lpx) * (int)imc[lx1]);
                        zi += (8-lpy) * (lpx * (int)imc1[lx] + (8-lpx) * (int)imc1[lx1]);
                        zi = (zi - (zbi << 6)) * zgi;
                        zi = (zi < 0 ) ? 0 : (zi >> 22);
                    }
                    else
                    {
                        zi = (imc[(lx+lx1)/2] - zbi)* zgi;
                        zi = (zi < 0 ) ? 0 : (zi >> 16);
                    }
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
            else if (oi->im.data_type == IS_RGB_PICTURE)
            {
                if (smooth_interpol)
                {
                    imc = ps[ly].ch;
                    imc1 = ps[ly1].ch;
                }
                else imc1 = imc = ps[(ly+ly1)/2].ch;
                for (j = spxs; j < spxe; j++)
                {
                    lx = oi->im.nxs + (j * nxef) / snxef;
		    lx = (lx < 0) ? 0 : lx;
                    lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                    lx1 = oi->im.nxs + ((j + 1) * nxef) / snxef;
                    lx1 = (lx1 == lx) ? lx + 1 : lx1;
		    lx1 = (lx1 < 0) ? 0 : lx1;
                    lx1 = ( lx1 >= oi->im.nx ) ? oi->im.nx - 1 : lx1;
                    if (smooth_interpol)
                    {
                        lpx = 8 -(((8 * j * nxef) / snxef) % 8);
                        switch (mode)
                        {
                            case GREY_LEVEL:
                                rzi = lpy * (lpx * (int)imc[3*lx] + (8-lpx) * (int)imc[3*lx1]);
                                rzi += (8-lpy) * (lpx * (int)imc1[3*lx] + (8-lpx) * (int)imc1[3*lx1]);
                                gzi = lpy * (lpx * (int)imc[(3*lx)+1] + (8-lpx) * (int)imc[(3*lx1)+1]);
                                gzi += (8-lpy) * (lpx * (int)imc1[(3*lx)+1] + (8-lpx) * (int)imc1[(3*lx1)+1]);
                                bzi = lpy * (lpx * (int)imc[(3*lx)+2] + (8-lpx) * (int)imc[(3*lx1)+2]);
                                bzi += (8-lpy) * (lpx * (int)imc1[(3*lx)+2] + (8-lpx) * (int)imc1[(3*lx1)+2]);
                                rzi += gzi + bzi;
                                rzi /= 3;
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case R_LEVEL:
                                rzi = lpy * (lpx * (int)imc[3*lx] + (8-lpx) * (int)imc[3*lx1]);
                                rzi += (8-lpy) * (lpx * (int)imc1[3*lx] + (8-lpx) * (int)imc1[3*lx1]);
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case RED_ONLY:
                                rzi = lpy * (lpx * (int)imc[3*lx] + (8-lpx) * (int)imc[3*lx1]);
                                rzi += (8-lpy) * (lpx * (int)imc1[3*lx] + (8-lpx) * (int)imc1[3*lx1]);
                                gzi = 0;
                                bzi = 0;
                                break;
                            case G_LEVEL:
                                gzi = lpy * (lpx * (int)imc[(3*lx)+1] + (8-lpx) * (int)imc[(3*lx1)+1]);
                                gzi += (8-lpy) * (lpx * (int)imc1[(3*lx)+1] + (8-lpx) * (int)imc1[(3*lx1)+1]);
                                bzi = gzi;
                                rzi = gzi;
                                break;
                            case GREEN_ONLY:
                                gzi = lpy * (lpx * (int)imc[(3*lx)+1] + (8-lpx) * (int)imc[(3*lx1)+1]);
                                gzi += (8-lpy) * (lpx * (int)imc1[(3*lx)+1] + (8-lpx) * (int)imc1[(3*lx1)+1]);
                                gzi = (gzi - (zbi << 2))* zgi;
                                bzi = 0;
                                rzi = 0;
                                break;
                            case B_LEVEL:
                                bzi = lpy * (lpx * (int)imc[(3*lx)+2] + (8-lpx) * (int)imc[(3*lx1)+2]);
                                bzi += (8-lpy) * (lpx * (int)imc1[(3*lx)+2] + (8-lpx) * (int)imc1[(3*lx1)+2]);
                                gzi = bzi;
                                rzi = bzi;
                                break;
                            case BLUE_ONLY:
                                bzi = lpy * (lpx * (int)imc[(3*lx)+2] + (8-lpx) * (int)imc[(3*lx1)+2]);
                                bzi += (8-lpy) * (lpx * (int)imc1[(3*lx)+2] + (8-lpx) * (int)imc1[(3*lx1)+2]);
                                gzi = 0;
                                rzi = 0;
                                break;
                            default:
                                rzi = lpy * (lpx * (int)imc[3*lx] + (8-lpx) * (int)imc[3*lx1]);
                                rzi += (8-lpy) * (lpx * (int)imc1[3*lx] + (8-lpx) * (int)imc1[3*lx1]);
                                gzi = lpy * (lpx * (int)imc[(3*lx)+1] + (8-lpx) * (int)imc[(3*lx1)+1]);
                                gzi += (8-lpy) * (lpx * (int)imc1[(3*lx)+1] + (8-lpx) * (int)imc1[(3*lx1)+1]);
                                bzi = lpy * (lpx * (int)imc[(3*lx)+2] + (8-lpx) * (int)imc[(3*lx1)+2]);
                                bzi += (8-lpy) * (lpx * (int)imc1[(3*lx)+2] + (8-lpx) * (int)imc1[(3*lx1)+2]);
                                break;
                        }
                        rzi = (rzi - (zbi << 6))* zgi;
                        rzi = (rzi < 0 ) ? 0 : (rzi >> 22);
                        gzi = (gzi - (zbi << 6))* zgi;
                        gzi = (gzi < 0 ) ? 0 : (gzi >> 22);
                        bzi = (bzi - (zbi << 6))* zgi;
                        bzi = (bzi < 0 ) ? 0 : (bzi >> 22);
                    }
                    else
                    {
                        lx = (lx + lx1)/2;
			lx = (lx < 0) ? 0 : lx;
			lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                        switch (mode)
                        {
                            case GREY_LEVEL:
                                rzi = (imc[3*lx] - zbi)* zgi;
                                gzi = (imc[(3*lx)+1] - zbi)* zgi;
                                bzi = (imc[(3*lx)+2] - zbi)* zgi;
                                rzi += gzi + bzi;
                                rzi /= 3;
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case R_LEVEL:
                                rzi = (imc[3*lx] - zbi)* zgi;
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case RED_ONLY:
                                rzi = (imc[3*lx] - zbi)* zgi;
                                gzi = 0;
                                bzi = 0;
                                break;
                            case G_LEVEL:
                                gzi = (imc[(3*lx)+1] - zbi)* zgi;
                                bzi = gzi;
                                rzi = gzi;
                                break;
                            case GREEN_ONLY:
                                gzi = (imc[(3*lx)+1] - zbi)* zgi;
                                bzi = 0;
                                rzi = 0;
                                break;
                            case B_LEVEL:
                                bzi = (imc[(3*lx)+2] - zbi)* zgi;
                                gzi = bzi;
                                rzi = bzi;
                                break;
                            case BLUE_ONLY:
                                bzi = (imc[(3*lx)+2] - zbi)* zgi;
                                bzi = (bzi - (zbi << 2))* zgi;
                                gzi = 0;
                                rzi = 0;
                                break;
                            default:
                                rzi = (imc[3*lx] - zbi)* zgi;
                                gzi = (imc[(3*lx)+1] - zbi)* zgi;
                                bzi = (imc[(3*lx)+2] - zbi)* zgi;
                                break;
                        }
                        rzi = (rzi < 0 ) ? 0 : (rzi >> 16);
                        gzi = (gzi < 0 ) ? 0 : (gzi >> 16);
                        bzi = (bzi < 0 ) ? 0 : (bzi >> 16);
                    }
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    zl1[j-spxs] = makecol32(cr,cg,cb);
                }
            }
            else if (oi->im.data_type == IS_RGB16_PICTURE)
            {
                if (smooth_interpol)
                {
                    imrgb16 = ps[ly].rgb16;
                    imrgb16_1 = ps[ly1].rgb16;
                }
                else imrgb16 = imrgb16_1 = ps[(ly+ly1)/2].rgb16;
                for (j = spxs; j < spxe; j++)
                {
                    lx = oi->im.nxs + (j * nxef) / snxef;
		    lx = (lx < 0) ? 0 : lx;
                    lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                    lx1 = oi->im.nxs + ((j + 1) * nxef) / snxef;
                    lx1 = (lx1 == lx) ? lx + 1 : lx1;
		    lx1 = (lx1 < 0) ? 0 : lx1;
                    lx1 = ( lx1 >= oi->im.nx ) ? oi->im.nx - 1 : lx1;
                    if (smooth_interpol)
                    {
                        lpx = 8 -(((8 * j * nxef) / snxef) % 8);
                        switch (mode)
                        {
                            case GREY_LEVEL:
                                rzi = lpy * (lpx * (int)imrgb16[lx].r + (8-lpx) * (int)imrgb16[lx1].r);
                                rzi += (8-lpy) * (lpx * (int)imrgb16_1[lx].r + (8-lpx) * (int)imrgb16_1[lx1].r);
                                gzi = lpy * (lpx * (int)imrgb16[lx].g + (8-lpx) * (int)imrgb16[lx1].g);
                                gzi += (8-lpy) * (lpx * (int)imrgb16_1[lx].g + (8-lpx) * (int)imrgb16_1[lx1].g);
                                bzi = lpy * (lpx * (int)imrgb16[lx].b + (8-lpx) * (int)imrgb16[lx1].b);
                                bzi += (8-lpy) * (lpx * (int)imrgb16_1[lx].b + (8-lpx) * (int)imrgb16_1[lx1].b);
                                rzi += gzi + bzi;
                                rzi /= 3;
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case R_LEVEL:
                                rzi = lpy * (lpx * (int)imrgb16[lx].r + (8-lpx) * (int)imrgb16[lx1].r);
                                rzi += (8-lpy) * (lpx * (int)imrgb16_1[lx].r + (8-lpx) * (int)imrgb16_1[lx1].r);
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case RED_ONLY:
                                rzi = lpy * (lpx * (int)imrgb16[lx].r + (8-lpx) * (int)imrgb16[lx1].r);
                                rzi += (8-lpy) * (lpx * (int)imrgb16_1[lx].r + (8-lpx) * (int)imrgb16_1[lx1].r);
                                gzi = 0;
                                bzi = 0;
                                break;
                            case G_LEVEL:
                                gzi = lpy * (lpx * (int)imrgb16[lx].g + (8-lpx) * (int)imrgb16[lx1].g);
                                gzi += (8-lpy) * (lpx * (int)imrgb16_1[lx].g + (8-lpx) * (int)imrgb16_1[lx1].g);
                                bzi = gzi;
                                rzi = gzi;
                                break;
                            case GREEN_ONLY:
                                gzi = lpy * (lpx * (int)imrgb16[lx].g + (8-lpx) * (int)imrgb16[lx1].g);
                                gzi += (8-lpy) * (lpx * (int)imrgb16_1[lx].g + (8-lpx) * (int)imrgb16_1[lx1].g);
                                bzi = 0;
                                rzi = 0;
                                break;
                            case B_LEVEL:
                                bzi = lpy * (lpx * (int)imrgb16[lx].b + (8-lpx) * (int)imrgb16[lx1].b);
                                bzi += (8-lpy) * (lpx * (int)imrgb16_1[lx].b + (8-lpx) * (int)imrgb16_1[lx1].b);
                                gzi = bzi;
                                rzi = bzi;
                                break;
                            case BLUE_ONLY:
                                bzi = lpy * (lpx * (int)imrgb16[lx].b + (8-lpx) * (int)imrgb16[lx1].b);
                                bzi += (8-lpy) * (lpx * (int)imrgb16_1[lx].b + (8-lpx) * (int)imrgb16_1[lx1].b);
                                gzi = 0;
                                rzi = 0;
                                break;
                            default:
                                rzi = lpy * (lpx * (int)imrgb16[lx].r + (8-lpx) * (int)imrgb16[lx1].r);
                                rzi += (8-lpy) * (lpx * (int)imrgb16_1[lx].r + (8-lpx) * (int)imrgb16_1[lx1].r);
                                gzi = lpy * (lpx * (int)imrgb16[lx].g + (8-lpx) * (int)imrgb16[lx1].g);
                                gzi += (8-lpy) * (lpx * (int)imrgb16_1[lx].g + (8-lpx) * (int)imrgb16_1[lx1].g);
                                bzi = lpy * (lpx * (int)imrgb16[lx].b + (8-lpx) * (int)imrgb16[lx1].b);
                                bzi += (8-lpy) * (lpx * (int)imrgb16_1[lx].b + (8-lpx) * (int)imrgb16_1[lx1].b);
                                break;
                        }
                        rzi = (rzi - (zbi << 6))* zgi;
                        rzi = (rzi < 0 ) ? 0 : (rzi >> 22);
                        gzi = (gzi - (zbi << 6))* zgi;
                        gzi = (gzi < 0 ) ? 0 : (gzi >> 22);
                        bzi = (bzi - (zbi << 6))* zgi;
                        bzi = (bzi < 0 ) ? 0 : (bzi >> 22);
                    }
                    else
                    {
                        lx = (lx + lx1)/2;
			lx = (lx < 0) ? 0 : lx;
			lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                        switch (mode)
                        {
                            case GREY_LEVEL:
                                rzi = (imrgb16[lx].r - zbi)* zgi;
                                gzi = (imrgb16[lx].g - zbi)* zgi;
                                bzi = (imrgb16[lx].b - zbi)* zgi;
                                rzi += gzi + bzi;
                                rzi /= 3;
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case R_LEVEL:
                                rzi = (imrgb16[lx].r - zbi)* zgi;
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case RED_ONLY:
                                rzi = (imrgb16[lx].r - zbi)* zgi;
                                gzi = 0;
                                bzi = 0;
                                break;
                            case G_LEVEL:
                                gzi = (imrgb16[lx].g - zbi)* zgi;
                                bzi = gzi;
                                rzi = gzi;
                                break;
                            case GREEN_ONLY:
                                gzi = (imrgb16[lx].g - zbi)* zgi;
                                bzi = 0;
                                rzi = 0;
                                break;
                            case B_LEVEL:
                                bzi = (imrgb16[lx].b - zbi)* zgi;
                                gzi = bzi;
                                rzi = bzi;
                                break;
                            case BLUE_ONLY:
                                bzi = (imrgb16[lx].b - zbi)* zgi;
                                gzi = 0;
                                rzi = 0;
                                break;
                            default:
                                rzi = (imrgb16[lx].r - zbi)* zgi;
                                gzi = (imrgb16[lx].g - zbi)* zgi;
                                bzi = (imrgb16[lx].b - zbi)* zgi;
                                break;
                        }

                        rzi = (rzi < 0 ) ? 0 : (rzi >> 16);
                        gzi = (gzi < 0 ) ? 0 : (gzi >> 16);
                        bzi = (bzi < 0 ) ? 0 : (bzi >> 16);
                    }
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    zl1[j-spxs] = makecol32(cr,cg,cb);
                }
            }
            else if (oi->im.data_type == IS_RGBA_PICTURE)
            {
                if (smooth_interpol)
                {
                    imrgba = ps[ly].rgba;
                    imrgba1 = ps[ly1].rgba;
                }
                else imrgba = imrgba1 = ps[(ly+ly1)/2].rgba;
                for (j = spxs; j < spxe; j++)
                {
                    lx = oi->im.nxs + (j * nxef) / snxef;
		    lx = (lx < 0) ? 0 : lx;
                    lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                    lx1 = oi->im.nxs + ((j + 1) * nxef) / snxef;
                    lx1 = (lx1 == lx) ? lx + 1 : lx1;
		    lx1 = (lx1 < 0) ? 0 : lx1;
                    lx1 = ( lx1 >= oi->im.nx ) ? oi->im.nx - 1 : lx1;
                    if (smooth_interpol)
                    {
                        lpx = 8 -(((8 * j * nxef) / snxef) % 8);
                        switch (mode)
                        {
                            case GREY_LEVEL:
                                rzi = lpy * (lpx * (int)imrgba[lx].r + (8-lpx) * (int)imrgba[lx1].r);
                                rzi += (8-lpy) * (lpx * (int)imrgba1[lx].r + (8-lpx) * (int)imrgba1[lx1].r);
                                gzi = lpy * (lpx * (int)imrgba[lx].g + (8-lpx) * (int)imrgba[lx1].g);
                                gzi += (8-lpy) * (lpx * (int)imrgba1[lx].g + (8-lpx) * (int)imrgba1[lx1].g);
                                bzi = lpy * (lpx * (int)imrgba[lx].b + (8-lpx) * (int)imrgba[lx1].b);
                                bzi += (8-lpy) * (lpx * (int)imrgba1[lx].b + (8-lpx) * (int)imrgba1[lx1].b);
                                rzi += gzi + bzi;
                                rzi /= 3;
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case R_LEVEL:
                                rzi = lpy * (lpx * (int)imrgba[lx].r + (8-lpx) * (int)imrgba[lx1].r);
                                rzi += (8-lpy) * (lpx * (int)imrgba1[lx].r + (8-lpx) * (int)imrgba1[lx1].r);
                                rzi = (rzi - (zbi << 2))* zgi;
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case RED_ONLY:
                                rzi = lpy * (lpx * (int)imrgba[lx].r + (8-lpx) * (int)imrgba[lx1].r);
                                rzi += (8-lpy) * (lpx * (int)imrgba1[lx].r + (8-lpx) * (int)imrgba1[lx1].r);
                                gzi = 0;
                                bzi = 0;
                                break;
                            case G_LEVEL:
                                gzi = lpy * (lpx * (int)imrgba[lx].g + (8-lpx) * (int)imrgba[lx1].g);
                                gzi += (8-lpy) * (lpx * (int)imrgba1[lx].g + (8-lpx) * (int)imrgba1[lx1].g);
                                bzi = gzi;
                                rzi = gzi;
                                break;
                            case GREEN_ONLY:
                                gzi = lpy * (lpx * (int)imrgba[lx].g + (8-lpx) * (int)imrgba[lx1].g);
                                gzi += (8-lpy) * (lpx * (int)imrgba1[lx].g + (8-lpx) * (int)imrgba1[lx1].g);
                                bzi = 0;
                                rzi = 0;
                                break;
                            case B_LEVEL:
                                bzi = lpy * (lpx * (int)imrgba[lx].b + (8-lpx) * (int)imrgba[lx1].b);
                                bzi += (8-lpy) * (lpx * (int)imrgba1[lx].b + (8-lpx) * (int)imrgba1[lx1].b);
                                gzi = bzi;
                                rzi = bzi;
                                break;
                            case BLUE_ONLY:
                                bzi = lpy * (lpx * (int)imrgba[lx].b + (8-lpx) * (int)imrgba[lx1].b);
                                bzi += (8-lpy) * (lpx * (int)imrgba1[lx].b + (8-lpx) * (int)imrgba1[lx1].b);
                                gzi = 0;
                                rzi = 0;
                                break;
                            default:
                                rzi = lpy * (lpx * (int)imrgba[lx].r + (8-lpx) * (int)imrgba[lx1].r);
                                rzi += (8-lpy) * (lpx * (int)imrgba1[lx].r + (8-lpx) * (int)imrgba1[lx1].r);
                                gzi = lpy * (lpx * (int)imrgba[lx].g + (8-lpx) * (int)imrgba[lx1].g);
                                gzi += (8-lpy) * (lpx * (int)imrgba1[lx].g + (8-lpx) * (int)imrgba1[lx1].g);
                                bzi = lpy * (lpx * (int)imrgba[lx].b + (8-lpx) * (int)imrgba[lx1].b);
                                bzi += (8-lpy) * (lpx * (int)imrgba1[lx].b + (8-lpx) * (int)imrgba1[lx1].b);
                                break;
                        }


                        rzi = (rzi - (zbi << 6))* zgi;
                        rzi = (rzi < 0 ) ? 0 : (rzi >> 22);
                        gzi = (gzi - (zbi << 6))* zgi;
                        gzi = (gzi < 0 ) ? 0 : (gzi >> 22);
                        bzi = (bzi - (zbi << 6))* zgi;
                        bzi = (bzi < 0 ) ? 0 : (bzi >> 22);
                    }
                    else
                    {
                        lx = (lx + lx1)/2;
			lx = (lx < 0) ? 0 : lx;
			lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                        switch (mode)
                        {
                            case GREY_LEVEL:
                                rzi = (imrgba[lx].r - zbi)* zgi;
                                gzi = (imrgba[lx].g - zbi)* zgi;
                                bzi = (imrgba[lx].b - zbi)* zgi;
                                rzi += gzi + bzi;
                                rzi /= 3;
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case R_LEVEL:
                                rzi = (imrgba[lx].r - zbi)* zgi;
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case RED_ONLY:
                                rzi = (imrgba[lx].r - zbi)* zgi;
                                gzi = 0;
                                bzi = 0;
                                break;
                            case G_LEVEL:
                                gzi = (imrgba[lx].g - zbi)* zgi;
                                bzi = gzi;
                                rzi = gzi;
                                break;
                            case GREEN_ONLY:
                                gzi = (imrgba[lx].g - zbi)* zgi;
                                bzi = 0;
                                rzi = 0;
                                break;
                            case B_LEVEL:
                                bzi = (imrgba[lx].b - zbi)* zgi;
                                gzi = bzi;
                                rzi = bzi;
                                break;
                            case BLUE_ONLY:
                                bzi = (imrgba[lx].b - zbi)* zgi;
                                gzi = 0;
                                rzi = 0;
                                break;
                            default:
                                rzi = (imrgba[lx].r - zbi)* zgi;
                                gzi = (imrgba[lx].g - zbi)* zgi;
                                bzi = (imrgba[lx].b - zbi)* zgi;
                                break;
                        }
                        rzi = (rzi < 0 ) ? 0 : (rzi >> 16);
                        gzi = (gzi < 0 ) ? 0 : (gzi >> 16);
                        bzi = (bzi < 0 ) ? 0 : (bzi >> 16);
                    }
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    zl1[j-spxs] = makecol32(cr,cg,cb);
                }
            }
            else if (oi->im.data_type == IS_RGBA16_PICTURE)
            {
                if (smooth_interpol)
                {
                    imrgba16 = ps[ly].rgba16;
                    imrgba16_1 = ps[ly1].rgba16;
                }
                else imrgba16 = imrgba16_1 = ps[(ly+ly1)/2].rgba16;
                for (j = spxs; j < spxe; j++)
                {
                    lx = oi->im.nxs + (j * nxef) / snxef;
		    lx = (lx < 0) ? 0 : lx;
                    lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                    lx1 = oi->im.nxs + ((j + 1) * nxef) / snxef;
                    lx1 = (lx1 == lx) ? lx + 1 : lx1;
		    lx1 = (lx1 < 0) ? 0 : lx1;
                    lx1 = ( lx1 >= oi->im.nx ) ? oi->im.nx - 1 : lx1;
                    if (smooth_interpol)
                    {
                        lpx = 8 -(((8 * j * nxef) / snxef) % 8);
                        switch (mode)
                        {
                            case GREY_LEVEL:
                                rzi = lpy * (lpx * (int)imrgba16[lx].r + (8-lpx) * (int)imrgba16[lx1].r);
                                rzi += (8-lpy) * (lpx * (int)imrgba16_1[lx].r + (8-lpx) * (int)imrgba16_1[lx1].r);
                                gzi = lpy * (lpx * (int)imrgba16[lx].g + (8-lpx) * (int)imrgba16[lx1].g);
                                gzi += (8-lpy) * (lpx * (int)imrgba16_1[lx].g + (8-lpx) * (int)imrgba16_1[lx1].g);
                                bzi = lpy * (lpx * (int)imrgba16[lx].b + (8-lpx) * (int)imrgba16[lx1].b);
                                bzi += (8-lpy) * (lpx * (int)imrgba16_1[lx].b + (8-lpx) * (int)imrgba16_1[lx1].b);
                                rzi += gzi + bzi;
                                rzi /= 3;
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case R_LEVEL:
                                rzi = lpy * (lpx * (int)imrgba16[lx].r + (8-lpx) * (int)imrgba16[lx1].r);
                                rzi += (8-lpy) * (lpx * (int)imrgba16_1[lx].r + (8-lpx) * (int)imrgba16_1[lx1].r);
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case RED_ONLY:
                                rzi = lpy * (lpx * (int)imrgba16[lx].r + (8-lpx) * (int)imrgba16[lx1].r);
                                rzi += (8-lpy) * (lpx * (int)imrgba16_1[lx].r + (8-lpx) * (int)imrgba16_1[lx1].r);
                                gzi = 0;
                                bzi = 0;
                                break;
                            case G_LEVEL:
                                gzi = lpy * (lpx * (int)imrgba16[lx].g + (8-lpx) * (int)imrgba16[lx1].g);
                                gzi += (8-lpy) * (lpx * (int)imrgba16_1[lx].g + (8-lpx) * (int)imrgba16_1[lx1].g);
                                gzi = (gzi - (zbi << 2))* zgi;
                                bzi = gzi;
                                rzi = gzi;
                                break;
                            case GREEN_ONLY:
                                gzi = lpy * (lpx * (int)imrgba16[lx].g + (8-lpx) * (int)imrgba16[lx1].g);
                                gzi += (8-lpy) * (lpx * (int)imrgba16_1[lx].g + (8-lpx) * (int)imrgba16_1[lx1].g);
                                bzi = 0;
                                rzi = 0;
                                break;
                            case B_LEVEL:
                                bzi = lpy * (lpx * (int)imrgba16[lx].b + (8-lpx) * (int)imrgba16[lx1].b);
                                bzi += (8-lpy) * (lpx * (int)imrgba16_1[lx].b + (8-lpx) * (int)imrgba16_1[lx1].b);
                                gzi = bzi;
                                rzi = bzi;
                                break;
                            case BLUE_ONLY:
                                bzi = lpy * (lpx * (int)imrgba16[lx].b + (8-lpx) * (int)imrgba16[lx1].b);
                                bzi += (8-lpy) * (lpx * (int)imrgba16_1[lx].b + (8-lpx) * (int)imrgba16_1[lx1].b);
                                bzi = (bzi - (zbi << 2))* zgi;
                                gzi = 0;
                                rzi = 0;
                                break;
                            default:
                                rzi = lpy * (lpx * (int)imrgba16[lx].r + (8-lpx) * (int)imrgba16[lx1].r);
                                rzi += (8-lpy) * (lpx * (int)imrgba16_1[lx].r + (8-lpx) * (int)imrgba16_1[lx1].r);
                                gzi = lpy * (lpx * (int)imrgba16[lx].g + (8-lpx) * (int)imrgba16[lx1].g);
                                gzi += (8-lpy) * (lpx * (int)imrgba16_1[lx].g + (8-lpx) * (int)imrgba16_1[lx1].g);
                                bzi = lpy * (lpx * (int)imrgba16[lx].b + (8-lpx) * (int)imrgba16[lx1].b);
                                bzi += (8-lpy) * (lpx * (int)imrgba16_1[lx].b + (8-lpx) * (int)imrgba16_1[lx1].b);
                                break;
                        }
                        rzi = (rzi - (zbi << 6))* zgi;
                        rzi = (rzi < 0 ) ? 0 : (rzi >> 22);
                        gzi = (gzi - (zbi << 6))* zgi;
                        gzi = (gzi < 0 ) ? 0 : (gzi >> 22);
                        bzi = (bzi - (zbi << 6))* zgi;
                        bzi = (bzi < 0 ) ? 0 : (bzi >> 22);
                    }
                    else
                    {
                        lx = (lx + lx1)/2;
			lx = (lx < 0) ? 0 : lx;
			lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                        switch (mode)
                        {
                            case GREY_LEVEL:
                                rzi = (imrgba16[lx].r - zbi)* zgi;
                                gzi = (imrgba16[lx].g - zbi)* zgi;
                                bzi = (imrgba16[lx].b - zbi)* zgi;
                                rzi += gzi + bzi;
                                rzi /= 3;
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case R_LEVEL:
                                rzi = (imrgba16[lx].r - zbi)* zgi;
                                gzi = rzi;
                                bzi = rzi;
                                break;
                            case RED_ONLY:
                                rzi = (imrgba16[lx].r - zbi)* zgi;
                                gzi = 0;
                                bzi = 0;
                                break;
                            case G_LEVEL:
                                gzi = (imrgba16[lx].g - zbi)* zgi;
                                bzi = gzi;
                                rzi = gzi;
                                break;
                            case GREEN_ONLY:
                                gzi = (imrgba16[lx].g - zbi)* zgi;
                                bzi = 0;
                                rzi = 0;
                                break;
                            case B_LEVEL:
                                bzi = (imrgba16[lx].b - zbi)* zgi;
                                gzi = bzi;
                                rzi = bzi;
                                break;
                            case BLUE_ONLY:
                                bzi = (imrgba16[lx].b - zbi)* zgi;
                                gzi = 0;
                                rzi = 0;
                                break;
                            default:
                                rzi = (imrgba16[lx].r - zbi)* zgi;
                                gzi = (imrgba16[lx].g - zbi)* zgi;
                                bzi = (imrgba16[lx].b - zbi)* zgi;
                                break;
                        }

                        rzi = (rzi < 0 ) ? 0 : (rzi >> 16);
                        gzi = (gzi < 0 ) ? 0 : (gzi >> 16);
                        bzi = (bzi < 0 ) ? 0 : (bzi >> 16);
                    }
                    cr = (rzi > 255) ? 255 :(unsigned char)rzi;
                    cg = (gzi > 255) ? 255 :(unsigned char)gzi;
                    cb = (bzi > 255) ? 255 :(unsigned char)bzi;
                    zl1[j-spxs] = makecol32(cr,cg,cb);
                }
            }
            else     if (oi->im.data_type == IS_INT_IMAGE)
            {
                if (smooth_interpol)
                {
                    imin = ps[ly].in;
                    imin1 = ps[ly1].in;
                }
                else imin = imin1 = ps[(ly+ly1)/2].in;
                for (j = spxs; j < spxe; j++)
                {
                    lx = oi->im.nxs + (j * nxef) / snxef;
		    lx = (lx < 0) ? 0 : lx;
                    lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                    lx1 = oi->im.nxs + ((j + 1) * nxef) / snxef;
                    lx1 = (lx1 == lx) ? lx + 1 : lx1;
		    lx1 = (lx1 < 0) ? 0 : lx1;
                    lx1 = ( lx1 >= oi->im.nx ) ? oi->im.nx - 1 : lx1;
                    if (smooth_interpol)
                    {
                        lpx = 8 -(((8 * j * nxef) / snxef) % 8);
                        zi = lpy * (lpx * imin[lx] + (8-lpx) * imin[lx1]);
                        zi += (8-lpy) * (lpx * imin1[lx] + (8-lpx) * imin1[lx1]);
                        zi = (zi - (zbi << 6)) * zgi;
                        zi = (zi < 0 ) ? 0 : (zi >> 22);
                    }
                    else
                    {
                        zi = (imin[(lx+lx1)/2] - zbi)* zgi;
                        zi = (zi < 0 ) ? 0 : (zi >> 16);
                    }
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
            else     if (oi->im.data_type == IS_UINT_IMAGE)
            {
                if (smooth_interpol)
                {
                    imui = ps[ly].ui;
                    imui1 = ps[ly1].ui;
                }
                else imui = imui1 = ps[(ly+ly1)/2].ui;
                for (j = spxs; j < spxe; j++)
                {
                    lx = oi->im.nxs + (j * nxef) / snxef;
		    lx = (lx < 0) ? 0 : lx;
                    lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                    lx1 = oi->im.nxs + ((j + 1) * nxef) / snxef;
                    lx1 = (lx1 == lx) ? lx + 1 : lx1;
		    lx1 = (lx1 < 0) ? 0 : lx1;
                    lx1 = ( lx1 >= oi->im.nx ) ? oi->im.nx - 1 : lx1;
                    if (smooth_interpol)
                    {
                        lpx = 8 -(((8 * j * nxef) / snxef) % 8);
                        zi = lpy * (lpx * imui[lx] + (8-lpx) * imui[lx1]);
                        zi += (8-lpy) * (lpx * imui1[lx] + (8-lpx) * imui1[lx1]);
                        zi = (zi - (zbi << 6)) * zgi;
                        zi = (zi < 0 ) ? 0 : (zi >> 22);
                    }
                    else
                    {
                        zi = (imui[(lx+lx1)/2] - zbi)* zgi;
                        zi = (zi < 0 ) ? 0 : (zi >> 16);
                    }
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
            else     if (oi->im.data_type == IS_LINT_IMAGE)
            {
                if (smooth_interpol)
                {
                    imli = ps[ly].li;
                    imli1 = ps[ly1].li;
                }
                else imli = imli1 = ps[(ly+ly1)/2].li;
                for (j = spxs; j < spxe; j++)
                {
                    lx = oi->im.nxs + (j * nxef) / snxef;
		    lx = (lx < 0) ? 0 : lx;
                    lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                    lx1 = oi->im.nxs + ((j + 1) * nxef) / snxef;
                    lx1 = (lx1 == lx) ? lx + 1 : lx1;
		    lx1 = (lx1 < 0) ? 0 : lx1;
                    lx1 = ( lx1 >= oi->im.nx ) ? oi->im.nx - 1 : lx1;
                    if (smooth_interpol)
                    {
                        lpx = 8 -(((8 * j * nxef) / snxef) % 8);
                        z = lpy * (lpx * (float)imli[lx] + (8-lpx) * (float)imli[lx1]);
                        z += (8-lpy) * (lpx * (float)imli1[lx] + (8-lpx) * (float)imli1[lx1]);
                        z = ((z/64) - oi->z_black) * zg;
                    }
                    else 	z = (imli[(lx+lx1)/2] - oi->z_black)*zg;
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
            else     if (oi->im.data_type == IS_FLOAT_IMAGE)
            {
                if (smooth_interpol)
                {
                    imfl = ps[ly].fl;
                    imfl1 = ps[ly1].fl;
                }
                else imfl = imfl1 = ps[(ly+ly1)/2].fl;
                for (j = spxs; j < spxe; j++)
                {
                    lx = oi->im.nxs + (j * nxef) / snxef;
		    lx = (lx < 0) ? 0 : lx;
                    lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                    lx1 = oi->im.nxs + ((j + 1) * nxef) / snxef;
                    lx1 = (lx1 == lx) ? lx + 1 : lx1;
		    lx1 = (lx1 < 0) ? 0 : lx1;
                    lx1 = ( lx1 >= oi->im.nx ) ? oi->im.nx - 1 : lx1;
                    if (smooth_interpol)
                    {
                        lpx = 8 -(((8 * j * nxef) / snxef) % 8);
                        z = lpy * (lpx * imfl[lx] + (8-lpx) * imfl[lx1]);
                        z += (8-lpy) * (lpx * imfl1[lx] + (8-lpx) * imfl1[lx1]);
                        z = ((z/64) - oi->z_black) * zg;
                    }
                    else  z = (imfl[(lx+lx1)/2] - oi->z_black)*zg;
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
            else     if (oi->im.data_type == IS_DOUBLE_IMAGE)
            {
                if (smooth_interpol)
                {
                    imdb = ps[ly].db;
                    imdb1 = ps[ly1].db;
                }
                else imdb = imdb1 = ps[(ly+ly1)/2].db;
                for (j = spxs; j < spxe; j++)
                {
                    lx = oi->im.nxs + (j * nxef) / snxef;
		    lx = (lx < 0) ? 0 : lx;
                    lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                    lx1 = oi->im.nxs + ((j + 1) * nxef) / snxef;
                    lx1 = (lx1 == lx) ? lx + 1 : lx1;
		    lx1 = (lx1 < 0) ? 0 : lx1;
                    lx1 = ( lx1 >= oi->im.nx ) ? oi->im.nx - 1 : lx1;
                    if (smooth_interpol)
                    {
                        lpx = 8 -(((8 * j * nxef) / snxef) % 8);
                        z = lpy * (lpx * imdb[lx] + (8-lpx) * imdb[lx1]);
                        z += (8-lpy) * (lpx * imdb1[lx] + (8-lpx) * imdb1[lx1]);
                        z = ((z/64) - oi->z_black) * zg;
                    }
                    else  z = (imdb[(lx+lx1)/2] - oi->z_black)*zg;
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
            else     if (oi->im.data_type == IS_COMPLEX_IMAGE)
            {
                if (smooth_interpol)
                {
                    imfl = ps[ly].fl;
                    imfl1 = ps[ly1].fl;
                }
                else imfl = imfl1 = ps[(ly+ly1)/2].fl;
                for (j = spxs; j < spxe; j++)
                {
                    lx = oi->im.nxs + (j * nxef) / snxef;
		    lx = (lx < 0) ? 0 : lx;
                    lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                    lpx = 8 -(((8 * j * nxef) / snxef) % 8);
                    lx1 = oi->im.nxs + ((j + 1) * nxef) / snxef;
                    lx1 = (lx1 == lx) ? lx + 1 : lx1;
		    lx1 = (lx1 < 0) ? 0 : lx1;
                    lx1 = ( lx1 >= oi->im.nx ) ? oi->im.nx - 1 : lx1;
                    lx *= 2;
                    lx1 *= 2;
                    switch (mode)
                    {
                        case AMP:
                            if (smooth_interpol)
                            {
                                z1 = imfl[lx]*imfl[lx];
                                z1 += imfl[lx+1]*imfl[lx+1];
                                z2 = imfl[lx1]*imfl[lx1];
                                z2 += imfl[lx1+1]*imfl[lx1+1];
                                z = lpy*(lpx*sqrt(z1) + (8-lpx)*sqrt(z2));
                                z1 = imfl1[lx]*imfl1[lx];
                                z1 += imfl1[lx+1]*imfl1[lx+1];
                                z2 = imfl1[lx1]*imfl1[lx1];
                                z2 += imfl1[lx1+1]*imfl1[lx1+1];
                                z += (8-lpy)*(lpx*sqrt(z1)+(8-lpx)*sqrt(z2));
                                z = ((z/64) - oi->z_black)*zg;
                            }
                            else
                            {
                                lx = (lx+lx1)/2;
                                z1 = fabs(imfl[lx]);
                                z = ((z1) - oi->z_black)*zg;
                            }
                            break;
                        case LOG_AMP:
                            if (smooth_interpol)
                            {
                                z1 = imfl[lx]*imfl[lx];
                                z1 += imfl[lx+1]*imfl[lx+1];
                                z2 = imfl[lx1]*imfl[lx1];
                                z2 += imfl[lx1+1]*imfl[lx1+1];
                                z = lpy*(lpx*z1 + (8-lpx)*z2);
                                z1 = imfl1[lx]*imfl1[lx];
                                z1 += imfl1[lx+1]*imfl1[lx+1];
                                z2 = imfl1[lx1]*imfl1[lx1];
                                z2 += imfl1[lx1+1]*imfl1[lx1+1];
                                z += (8-lpy)*(lpx*z1+(8-lpx)*z2);
                                z = (z/64 > 0) ? log10(z/64) : -40;
                                z = (z - oi->z_black)*zg;   // correction 11/2009
                            }
                            else
                            {
                                lx = (lx+lx1)/2;
                                z1 = imfl[lx]*imfl[lx];
                                z = (z1 > 0) ? log10(z1) : -40;
                                z = (z - oi->z_black)*zg;
                            }
                            break;
                        case AMP_2:
                            if (smooth_interpol)
                            {
                                z1 = imfl[lx]*imfl[lx];
                                z1 += imfl[lx+1]*imfl[lx+1];
                                z2 = imfl[lx1]*imfl[lx1];
                                z2 += imfl[lx1+1]*imfl[lx1+1];
                                z = lpy*(lpx*z1 + (8-lpx)*z2);
                                z1 = imfl1[lx]*imfl1[lx];
                                z1 += imfl1[lx+1]*imfl1[lx+1];
                                z2 = imfl1[lx1]*imfl1[lx1];
                                z2 += imfl1[lx1+1]*imfl1[lx1+1];
                                z += (8-lpy)*(lpx*z1+(8-lpx)*z2);
                                z = ((z/64) - oi->z_black)*zg;
                            }
                            else
                            {
                                lx = (lx+lx1)/2;
                                z = imfl[lx]*imfl[lx];
                                z = (z - oi->z_black)*zg;
                            }
                            break;
                        case RE:
                            if (smooth_interpol)
                            {
                                z = lpy * (lpx * imfl[lx] + (8-lpx) * imfl[lx1]);
                                z += (8-lpy) * (lpx * imfl1[lx] + (8-lpx) * imfl1[lx1]);
                                z = ((z/64) - oi->z_black)*zg;
                            }
                            else
                            {
                                lx = (lx+lx1)/2;
                                z = imfl[lx];
                                z = (z - oi->z_black)*zg;
                            }
                            break;
                        case IM:
                            if (smooth_interpol)
                            {
                                z = lpy * (lpx * imfl[lx+1] + (8-lpx) * imfl[lx1+1]);
                                z += (8-lpy) * (lpx * imfl1[lx+1] + (8-lpx) * imfl1[lx1+1]);
                                z = ((z/64) - oi->z_black)*zg;
                            }
                            else
                            {
                                lx = (lx+lx1)/2;
                                z = imfl[lx+1];
                                z = (z - oi->z_black)*zg;
                            }
                            break;
                    };
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
            else     if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
            {
                if (smooth_interpol)
                {
                    imdb = ps[ly].db;
                    imdb1 = ps[ly1].db;
                }
                else imdb = imdb1 = ps[(ly+ly1)/2].db;
                for (j = spxs; j < spxe; j++)
                {
                    lx = oi->im.nxs + (j * nxef) / snxef;
		    lx = (lx < 0) ? 0 : lx;
                    lx = ( lx >= oi->im.nx ) ? oi->im.nx - 1 : lx;
                    lpx = 8 -(((8 * j * nxef) / snxef) % 8);
                    lx1 = oi->im.nxs + ((j + 1) * nxef) / snxef;
                    lx1 = (lx1 == lx) ? lx + 1 : lx1;
		    lx1 = (lx1 < 0) ? 0 : lx1;
                    lx1 = ( lx1 >= oi->im.nx ) ? oi->im.nx - 1 : lx1;
                    lx *= 2;
                    lx1 *= 2;
                    switch (mode)
                    {
                        case AMP:
                            if (smooth_interpol)
                            {
                                z1 = imdb[lx]*imdb[lx];
                                z1 += imdb[lx+1]*imdb[lx+1];
                                z2 = imdb[lx1]*imdb[lx1];
                                z2 += imdb[lx1+1]*imdb[lx1+1];
                                z = lpy*(lpx*sqrt(z1) + (8-lpx)*sqrt(z2));
                                z1 = imdb1[lx]*imdb1[lx];
                                z1 += imdb1[lx+1]*imdb1[lx+1];
                                z2 = imdb1[lx1]*imdb1[lx1];
                                z2 += imdb1[lx1+1]*imdb1[lx1+1];
                                z += (8-lpy)*(lpx*sqrt(z1)+(8-lpx)*sqrt(z2));
                                z = ((z/64) - oi->z_black)*zg;
                            }
                            else
                            {
                                lx = (lx+lx1)/2;
                                z = fabs(imdb[lx]);
                                z = (z - oi->z_black)*zg;
                            }
                            break;
                        case LOG_AMP:
                            if (smooth_interpol)
                            {
                                z1 = imdb[lx]*imdb[lx];
                                z1 += imdb[lx+1]*imdb[lx+1];
                                z2 = imdb[lx1]*imdb[lx1];
                                z2 += imdb[lx1+1]*imdb[lx1+1];
                                z = lpy*(lpx*z1 + (8-lpx)*z2);
                                z1 = imdb1[lx]*imdb1[lx];
                                z1 += imdb1[lx+1]*imdb1[lx+1];
                                z2 = imdb1[lx1]*imdb1[lx1];
                                z2 += imdb1[lx1+1]*imdb1[lx1+1];
                                z += (8-lpy)*(lpx*z1+(8-lpx)*z2);
                                z = (z/64 > 0) ? log10(z/64) : -40;
                                z = (z - oi->z_black)*zg;
                            }
                            else
                            {
                                lx = (lx+lx1)/2;
                                z = imdb[lx]*imdb[lx];
                                z = (z > 0) ? log10(z) : -40;
                                z = (z - oi->z_black)*zg;
                            }
                            break;
                        case AMP_2:
                            if (smooth_interpol)
                            {
                                z1 = imdb[lx]*imdb[lx];
                                z1 += imdb[lx+1]*imdb[lx+1];
                                z2 = imdb[lx1]*imdb[lx1];
                                z2 += imdb[lx1+1]*imdb[lx1+1];
                                z = lpy*(lpx*z1 + (8-lpx)*z2);
                                z1 = imdb1[lx]*imdb1[lx];
                                z1 += imdb1[lx+1]*imdb1[lx+1];
                                z2 = imdb1[lx1]*imdb1[lx1];
                                z2 += imdb1[lx1+1]*imdb1[lx1+1];
                                z += (8-lpy)*(lpx*z1+(8-lpx)*z2);
                                z = ((z/64) - oi->z_black)*zg;
                            }
                            else
                            {
                                lx = (lx+lx1)/2;
                                z = imdb[lx]*imdb[lx];
                                z = (z - oi->z_black)*zg;
                            }
                            break;
                        case RE:
                            if (smooth_interpol)
                            {
                                z = lpy * (lpx * imdb[lx] + (8-lpx) * imdb[lx1]);
                                z += (8-lpy) * (lpx * imdb1[lx] + (8-lpx) * imdb1[lx1]);
                                z = ((z/64) - oi->z_black)*zg;
                            }
                            else
                            {
                                lx = (lx+lx1)/2;
                                z = imdb[lx];
                                z = (z - oi->z_black)*zg;
                            }
                            break;
                        case IM:
                            if (smooth_interpol)
                            {
                                z = lpy * (lpx * imdb[lx+1] + (8-lpx) * imdb[lx1+1]);
                                z += (8-lpy) * (lpx * imdb1[lx+1] + (8-lpx) * imdb1[lx1+1]);
                                z = ((z/64) - oi->z_black)*zg;
                            }
                            else
                            {
                                lx = (lx+lx1)/2;
                                z = imdb[lx+1];
                                z = (z - oi->z_black)*zg;
                            }
                            break;
                    };
                    zi = (z < 0 ) ? 0 : (int)z;
                    c = (zi > 255) ? 255 :(unsigned char)zi;
                    zl1[j-spxs] = makecol32(c,c,c);
                }
            }
        }
        /*        win_printf("bef release");*/
        release_bitmap(bmp);
    }
    else
    {
        SET_BITMAP_NO_MORE_IN_USE(oi);
        return xvin_error(Wrong_Argument);
    }
    oi->need_to_refresh &= PLOTS_NEED_REFRESH;
    SET_BITMAP_NO_MORE_IN_USE(oi);
    return 0;
}

int    do_line_profile(O_i *oi, int n_line, DIALOG *d)
{
    //char s[128];
    imreg *imr = NULL;
    O_p *op = NULL;

    imr = find_imr_in_current_dialog(NULL);
    if (imr == NULL || oi == NULL)        return D_O_K;
    //    win_printf("entering line profile");

    if (oi->o_p == NULL || oi->cur_op < 0 || oi->cur_op >= oi->n_op) // || oi->im.source == NULL)
    {
        win_printf("no plot or no image source");
        return D_O_K;
    }
    op = oi->o_p[oi->cur_op];
    if (op == NULL || op->dat == NULL || op->n_dat == 0)
    {
        win_printf("no ds");
        return  D_O_K;
    }
    /*
       if ( op->dat[0]->treatement == NULL)
       {
       win_printf("no plot treat");
       return D_O_K;
       }
       */
    if ( oi->im.source != NULL && op->dat[0]->source != NULL)
    {
        if (strcmp(oi->im.source,op->dat[0]->source) != 0)
        {
            win_printf("plot and im have not the same source");
            return D_O_K;
        }
    }
    if (op->dat[0]->treatement != NULL && strncmp(op->dat[0]->treatement,"line profile",12) != 0)
    {
        win_printf("no the right plot treat");
        return D_O_K;
    }
    if (op->dat == NULL || op->dat[0]->nx < oi->im.nx)
    {
        win_printf("no the right nb of points");
        return D_O_K;
    }
    n_line = (n_line < 0) ? 0 : n_line;
    n_line = (n_line < oi->im.ny) ? n_line : oi->im.ny - 1;
    if (extract_raw_line(oi, n_line, op->dat[0]->yd))
    {
        win_printf("could not extract profile");
        return D_O_K;
    }
    if (oi->n_yu > 0)
        set_plot_title(op,"Line profile at %d -> %g %s",
                       n_line,oi->ay+oi->dy*n_line,(oi->yu[oi->c_yu]->name != NULL) ?
                       oi->yu[oi->c_yu]->name : " ");

    else set_plot_title(op,"Line profile at %d",n_line);
    set_ds_treatement(op->dat[0], "line profile at %d",n_line);
    //if (op->title) op->dat[0]->treatement = strdup(op->title);
    /*
       inherit_from_im_to_ds(op->dat[0], oi);
       uns_oi_2_op(oi, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
       uns_oi_2_op(oi, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
       */
    op->y_lo = (op->dy != 0) ? (oi->z_black - op->ay)/op->dy : (oi->z_black - op->ay);
    op->y_hi = (op->dy != 0) ? (oi->z_white - op->ay)/op->dy : (oi->z_white - op->ay);
    oi->need_to_refresh |= PLOTS_NEED_REFRESH;
    d_draw_Im_proc(MSG_DRAW, d, 0);
    return D_REDRAWME;
}

int    do_row_profile(O_i *oi, int n_row, DIALOG *d)
{
    //char s[128];
    O_p *op = NULL;
    imreg *imr = NULL;

    (void)d;
    imr = find_imr_in_current_dialog(NULL);
    if (imr == NULL || oi == NULL)        return D_O_K;

    if (oi->cur_op < 0 || oi->cur_op >= oi->n_op || oi->o_p == NULL) // || oi->im.source == NULL)
    {
        win_printf("no plot or no image source");
        return D_O_K;
    }
    op = oi->o_p[oi->cur_op];
    if (op == NULL || op->dat == NULL || op->n_dat == 0)
    {
        win_printf("no ds");
        return  D_O_K;
    }
    /*
       if ( op->dat[0]->treatement == NULL)
       {
       win_printf("no plot treat");
       return D_O_K;
       }
       */
    if ( oi->im.source != NULL && op->dat[0]->source != NULL)
    {
        if (strcmp(oi->im.source,op->dat[0]->source) != 0)
        {
            win_printf("plot and im have not the same source");
            return D_O_K;
        }
    }
    if (op->dat[0]->treatement != NULL && strncmp(op->dat[0]->treatement,"row profile",11) != 0)
    {
        win_printf("no the right plot treat");
        return D_O_K;
    }
    if (op->dat == NULL || op->dat[0]->nx < oi->im.ny)
    {
        win_printf("no the right nb of points");
        return D_O_K;
    }
    n_row = (n_row < 0) ? 0 : n_row;
    n_row = (n_row < oi->im.nx) ? n_row : oi->im.nx - 1;
    if (extract_raw_row(oi, n_row, op->dat[0]->xd))
    {
        win_printf("could not extract profile");
        return D_O_K;
    }
    if (oi->n_xu > 0)
        set_plot_title(op,"Row profile at %d -> %g %s",
                       n_row,oi->ax+oi->dx*n_row,(oi->xu[oi->c_xu]->name != NULL) ?
                       oi->xu[oi->c_xu]->name : " ");

    else set_plot_title(op,"Row profile at %d",n_row);
    set_ds_treatement(op->dat[0], "row profile at %d",n_row);

    /*
       inherit_from_im_to_ds(op->dat[0], oi);
       uns_oi_2_op(oi, IS_Z_UNIT_SET, op, IS_X_UNIT_SET);
       uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
       */
    op->x_lo = (op->dx != 0) ? (oi->z_black - op->ax)/op->dx : (oi->z_black - op->ax);
    op->x_hi = (op->dx != 0) ? (oi->z_white - op->ax)/op->dx : (oi->z_white - op->ax);
    oi->need_to_refresh |= PLOTS_NEED_REFRESH;
    return D_REDRAWME;
}

int check_profile(MENU* mn)
{
    int i, j, l;
    imreg *imr = NULL;
    O_i *ois = NULL;
    O_p *op = NULL;

    imr = find_imr_in_current_dialog(NULL);
    if (imr == NULL || imr->one_i == NULL)        return D_O_K;
    ois = imr->one_i;
    if (ois->cur_op < 0 || ois->cur_op >= ois->n_op)    return D_O_K;
    op = ois->o_p[ois->cur_op];
    if (op == NULL || op->dat[0] == NULL)                return D_O_K;

    for (j = 0; mn[j].text != NULL; j++)
    {
        if (mn[j].dp != NULL)
        {
            l = strlen((const char *)mn[j].dp);
            i = strncmp(op->dat[0]->treatement,(const char *) mn[j].dp,l);
            if (i == 0)     mn[j].flags |= D_SELECTED;
            else mn[j].flags &= !D_SELECTED;
        }
    }
    return D_O_K;
}
int set_profile(void)
{
    int i;
    imreg *imr = NULL;
    O_i *ois = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    char* treat = NULL;
    int index;

    if(updating_menu_state != 0)    return D_O_K;
    imr = find_imr_in_current_dialog(NULL);
    if (imr == NULL || imr->one_i == NULL)        return D_O_K;
    ois = imr->one_i;
    if (active_menu == NULL || active_menu->dp == NULL)    return D_O_K;


    treat = (char*)active_menu->dp;
    if (strlen(treat) < 2)                                return D_O_K;

    index = 0;
    if (strncmp(treat,"line profile",12) == 0)            index = P_LINE;
    else if    (strncmp(treat,"row profile",11) == 0)        index = P_COL;
    else
    {
        win_printf("invalid profile!");
        return D_O_K;
    }

    i = find_op_nb_of_treatement_specific_ds_in_oi(ois,treat);
    if (i >= 0)
    {
        ois->cur_op = i;
        if (index == P_LINE)        vert_marker_action = do_line_profile;
        else if (index == P_COL)    horz_marker_action = do_row_profile;
        ois->need_to_refresh |= PLOTS_NEED_REFRESH;
        return D_REDRAWME;
    }

    if (index == P_LINE)
    {
        op = create_one_plot(ois->im.nx,ois->im.nx,0);
        if (op == NULL)
        {
            win_printf("cant create plot!");
            return D_O_K;
        }
        ds = op->dat[0];
        for (i=0 ; i< ds->nx ; i++)    ds->xd[i] = i;
        op->y_lo = ois->z_black;
        op->y_hi = ois->z_white;
        op->iopt2 |= Y_LIM;
        op->type = IM_SAME_X_AXIS;
        set_ds_line_color(ds, Lightred);
        set_ds_treatement(ds,"line profile");
        set_plot_x_title(op, "Position along line");
        set_ds_source(ds, "%s",ois->im.source);
        op->filename = Transfer_filename(ois->filename);
        uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
        uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
        vert_marker_action = do_line_profile;
        vert_marker_drag_action = do_line_profile;
    }
    else if (index == P_COL)
    {
        op = create_one_plot(ois->im.ny,ois->im.ny,0);
        if (op == NULL)
        {
            win_printf("cant create plot!");
            return D_O_K;
        }
        ds = op->dat[0];
        for (i=0 ; i< ds->ny ; i++)    ds->yd[i] = i;
        op->x_lo = ois->z_black;
        op->x_hi = ois->z_white;
        op->iopt2 |= X_LIM;
        op->type = IM_SAME_Y_AXIS;
        set_ds_line_color(ds, Lightred);
        set_ds_treatement(ds,"row profile");
        set_plot_y_title(op, "Position");
        set_ds_source(ds, "%s", ois->im.source);
        op->filename = Transfer_filename(ois->filename);
        uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_X_UNIT_SET);
        uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
        horz_marker_action = do_row_profile;
        horz_marker_drag_action    = do_row_profile;
    }
    else return D_O_K;
    add_one_image (ois, IS_ONE_PLOT, (void *)op);
    ois->cur_op = ois->n_op - 1;
    return 0;
}


int find_selected_point_value(imreg *imr, int x_0, int y_0, int mode)
{
    const char  *xun = NULL, *yun = NULL, *zun = NULL;
    int x, y;
    static int old_x = 0, old_y = 0;
    float tmpx, tmpy,   px1, py1, zr = 0, zi = 0, zb = 0, zg = 0, za = 0;
    O_i *oi = NULL;


    (void)x_0;
    (void)y_0;

    if (imr->one_i == NULL)        return win_printf_OK("Could not find image data");
    oi = imr->one_i;
    x = mouse_x; y = mouse_y;
    px1 = x_imr_2_imdata(imr, mouse_x);
    py1 = y_imr_2_imdata(imr, mouse_y);

    if (mode == 1)
    {
        if(x != old_x || x != old_y)
        {
            tmpx = oi->ax + oi->dx * px1;
            tmpy = oi->ay + oi->dy * py1;
            xun = (oi->x_unit != NULL) ? oi->x_unit : " ";
            yun = (oi->y_unit != NULL) ? oi->y_unit : " ";
            zun = (oi->c_zu >= 0 && oi->zu[oi->c_zu]->name != NULL)
                ? oi->zu[oi->c_zu]->name : " ";
            if (oi->im.data_type == IS_COMPLEX_IMAGE || oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
            {
                interpolate_image_point(oi, px1, py1, &zr, &zi);
                zr =  oi->az + oi->dz * zr;
                zi =  oi->az + oi->dz * zi;
                draw_bubble(screen, B_CENTER, x, y+20,"x %g %s y %g %s z %g i %g %s",
                            tmpx,xun,tmpy,yun,zr,zi,zun);
            }
            else if (oi->im.data_type == IS_RGB_PICTURE || oi->im.data_type == IS_RGBA_PICTURE ||
                     oi->im.data_type == IS_RGB16_PICTURE || oi->im.data_type == IS_RGBA16_PICTURE)
            {
                interpolate_RGB_picture_point(oi, px1, py1, &zr, &zg, &zb, &za);
                zr =  oi->az + oi->dz * zr;
                zg =  oi->az + oi->dz * zg;
                zb =  oi->az + oi->dz * zb;
                za =  oi->az + oi->dz * za;
                draw_bubble(screen, B_CENTER, x, y+20,"x %g %s y %g %s (R %g G %g B %g A %g) %s",
                            tmpx,xun,tmpy,yun,zr,zg,zb,za,zun);
            }
            else
            {
                interpolate_image_point(oi, px1, py1, &zr, NULL);
                zr =  oi->az + oi->dz * zr;
                draw_bubble(screen, B_CENTER, x, y+20,"x %g %s y %g %s z %g %s",
                            tmpx,xun,tmpy,yun,zr,zun);
            }
        }
    }
    old_x = x;
    old_y = y;
    return 0;
}


int    set_point_value(void)
{
    if(updating_menu_state != 0)    return D_O_K;
    general_im_action = find_selected_point_value;
    return D_O_K;
}



int find_z_profile_on_point(imreg *imr, int x_0, int y_0, int mode)
{
    int i, x, y, nf, xc, yc;
    static int old_x = 0, old_y = 0;
    O_i *oi = NULL;
    float px1, py1;
    O_p *op = NULL;
    d_s *ds = NULL;
    DIALOG *di = NULL;

    (void)x_0;
    (void)y_0;

    if (imr->one_i == NULL)        return win_printf_OK("Could not find image data");
    oi = imr->one_i;
    di = find_dialog_associated_to_imr(imr, NULL);
    nf = abs(oi->im.n_f);
    if (nf <= 0)
    {
        my_set_window_title("This is not a movie");
        return D_O_K;
    }

    x = mouse_x; y = mouse_y;
    px1 = x_imr_2_imdata(imr, mouse_x);
    py1 = y_imr_2_imdata(imr, mouse_y);

    i = find_op_nb_of_treatement_specific_ds_in_oi(oi,"Movie Z profile at xc ");
    if (i < 0)
    {
        op = create_and_attach_op_to_oi(oi,oi->im.n_f,oi->im.n_f,0,0);
        if (op == NULL) return (win_printf_OK("cant create plot!"));
        ds = op->dat[0];
        op->y_lo = oi->z_black;	op->y_hi = oi->z_white;
	set_ds_treatement(ds,"Movie Z profile at xc ");
        inherit_from_im_to_ds(ds, oi);
        op->filename = Transfer_filename(oi->filename);
        uns_oi_2_op(oi, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
        uns_oi_2_op(oi, IS_T_UNIT_SET, op, IS_X_UNIT_SET);
        oi->cur_op = oi->n_op-1;
    }
    else
    {
        op = oi->o_p[i];
        oi->cur_op = i;
        ds = find_treatement_specific_ds_in_op(op,"Movie Z profile at xc ");
    }
    if (ds == NULL) return win_printf_OK("cannot find specific data set!");

    if (mode == 1)
    {
        if(x != old_x || x != old_y)
        {
            xc = (int)(px1+0.5); yc = (int)(py1+0.5);
	    if (xc >= 0 && yc >= 0 && xc < oi->im.nx && yc < oi->im.ny)
	      {
		extract_z_profile_from_movie(oi, xc, yc, ds->yd);
		for (i = 0; i < nf; i++) ds->xd[i] = i;
		set_plot_title(op,"Movie profile xc %d yc %d",xc, yc);
		set_ds_treatement(ds,"Movie Z profile at xc %d yc %d",xc, yc);
		old_x = x;
		old_y = y;
		oi->need_to_refresh |= PLOTS_NEED_REFRESH;
		d_draw_Im_proc(MSG_DRAW, di, 0);
	      }
        }
    }
    oi->need_to_refresh |= PLOTS_NEED_REFRESH;
    return D_REDRAWME;
}


int find_z_profile_on_point_by_menu(void)
{
  imreg *imr;
    O_i *ois = NULL;
    static int xc, yc;
    int i, nf;
    O_p *op = NULL;
    d_s *ds = NULL;


    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
        active_menu->flags |=  D_DISABLED;
        return D_O_K;
    }
    if(updating_menu_state != 0)
    {
        if (ois->im.n_f == 1 || ois->im.n_f == 0)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }
    nf = abs(ois->im.n_f);
    if (nf <= 0)
    {
        my_set_window_title("This is not a movie");
        return D_O_K;
    }
    i = win_scanf("Movie z profile to be taken at"
		  "X_0 = 6%d and Y_0 = 6%d\n",&xc,&yc);
    if (i == WIN_CANCEL) return D_O_K;

    i = find_op_nb_of_treatement_specific_ds_in_oi(ois,"Movie Z profile at xc ");
    if (i < 0)
    {
        op = create_and_attach_op_to_oi(ois,ois->im.n_f,ois->im.n_f,0,0);
        if (op == NULL) return (win_printf_OK("cant create plot!"));
        ds = op->dat[0];
        op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
        inherit_from_im_to_ds(ds, ois);
        op->filename = Transfer_filename(ois->filename);
        uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
        uns_oi_2_op(ois, IS_T_UNIT_SET, op, IS_X_UNIT_SET);
        ois->cur_op = ois->n_op-1;
    }
    else
    {
        op = ois->o_p[i];
        ois->cur_op = i;
        ds = find_treatement_specific_ds_in_op(op,"Movie Z profile at xc ");
    }
    if (ds == NULL) return win_printf_OK("cannot find specific data set!");

    extract_z_profile_from_movie(ois, xc, yc, ds->yd);
    for (i = 0; i < nf; i++) ds->xd[i] = i;
    set_plot_title(op,"Movie profile xc %d yc %d",xc, yc);
    set_formated_string(&ds->treatement,"Movie Z profile at xc %d yc %d",xc, yc);
    ois->need_to_refresh |= PLOTS_NEED_REFRESH;
    return D_REDRAWME;
}


int    set_z_profile_on_point(void)
{
    imreg *imr = NULL;
    O_i *ois = NULL;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
        active_menu->flags |=  D_DISABLED;
        return D_O_K;
    }
    if(updating_menu_state != 0)
    {
        if (ois->im.n_f == 1 || ois->im.n_f == 0)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }
    general_im_action = find_z_profile_on_point;
    return D_O_K;
}


int find_z_avg_profile_on_point(imreg *imr, int x_0, int y_0, int mode)
{
    int i, x, y, nf, xc, yc, ix, iy, nm, onx, ony;
    static int old_x = 0, old_y = 0;
    O_i *oi = NULL;
    float px1, py1;
    O_p *op = NULL;
    d_s *ds = NULL;
    DIALOG *di = NULL;

    (void)x_0;
    (void)y_0;
    if (imr->one_i == NULL)        return win_printf_OK("Could not find image data");
    oi = imr->one_i;
    di = find_dialog_associated_to_imr(imr, NULL);
    nf = abs(oi->im.n_f);
    ony = oi->im.ny;
    onx = oi->im.nx;

    if (nf <= 0)
    {
        my_set_window_title("This is not a movie");
        return D_O_K;
    }

    x = mouse_x; y = mouse_y;
    px1 = x_imr_2_imdata(imr, mouse_x);
    py1 = y_imr_2_imdata(imr, mouse_y);

    i = find_op_nb_of_treatement_specific_ds_in_oi(oi,"Movie Z avg profile at xc ");
    if (i < 0)
    {
        op = create_and_attach_op_to_oi(oi,oi->im.n_f,oi->im.n_f,0,0);
        if (op == NULL) return (win_printf_OK("cant create plot!"));
        ds = op->dat[0];
        op->y_lo = oi->z_black;	op->y_hi = oi->z_white;
        inherit_from_im_to_ds(ds, oi);
        op->filename = Transfer_filename(oi->filename);
        uns_oi_2_op(oi, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
        uns_oi_2_op(oi, IS_T_UNIT_SET, op, IS_X_UNIT_SET);
        oi->cur_op = oi->n_op-1;
    }
    else
    {
        op = oi->o_p[i];
        oi->cur_op = i;
        ds = find_treatement_specific_ds_in_op(op,"Movie Z avg profile at xc ");
    }
    if (ds == NULL) return win_printf_OK("cannot find specific data set!");

    if (mode == 1)
    {
        if(x != old_x || x != old_y)
        {
            xc = (int)(px1+0.5); yc = (int)(py1+0.5);
            for (i = 0; i < nf; i++) ds->xd[i] = 0;
            for (iy = yc - z_y_avg_profile_size, nm = 0; iy <= yc + z_y_avg_profile_size; iy++)
            {
                for (ix = xc - z_x_avg_profile_size; ix <= xc + z_x_avg_profile_size; ix++)
                {
                    if (ix >= 0 && iy >= 0 && ix < onx && iy < ony)
                    {
                        extract_z_profile_from_movie(oi, ix, iy, ds->yd);
                        for (i = 0; i < nf; i++) ds->xd[i] += ds->yd[i];
                        nm++;
                    }
                }
            }
            for (i = 0; i < nf && nm > 0; i++)
            {
                ds->yd[i] = ds->xd[i]/nm;
                ds->xd[i] = i;
            }
            set_plot_title(op,"\\stack{{Movie avg profile}"
                           "{\\pt8 xc %d avg %d yc %d avg %d}"
                           "{\\pt8 %d effective avarage}}"
                           ,xc ,1 + 2 * z_x_avg_profile_size, yc ,1 + 2 * z_y_avg_profile_size, nm);
            set_formated_string(&ds->treatement,"Movie Z avg profile at xc %d over %d "
                                "yc %d over %d effective averaging over %d profiles"
                                ,xc ,1 + 2 * z_x_avg_profile_size
                                ,yc ,1 + 2 * z_y_avg_profile_size, nm);
            old_x = x;
            old_y = y;
            oi->need_to_refresh |= PLOTS_NEED_REFRESH;
            d_draw_Im_proc(MSG_DRAW, di, 0);
        }
    }

    oi->need_to_refresh |= PLOTS_NEED_REFRESH;
    return D_REDRAWME;
}


int    set_z_avg_profile_on_point(void)
{
    imreg *imr = NULL;
    O_i *ois = NULL;
    int sx, sy, i;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
    {
        active_menu->flags |=  D_DISABLED;
        return D_O_K;
    }
    if(updating_menu_state != 0)
    {
        if (ois->im.n_f == 1 || ois->im.n_f == 0)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }
    sx = z_x_avg_profile_size;
    sy = z_y_avg_profile_size;
    i = win_scanf("I shall coupute an averaged profile over\n"
                  "a rectangular area centered on the selected point\n"
                  "extending over 1 + 2 x Nx along x: Nx %8d (0,32)\n"
                  "extending over 1 + 2 x Ny along y: Ny %8d (0,32)\n"
                  ,&sx,&sy);
    if (i == WIN_CANCEL)   return D_O_K;
    z_x_avg_profile_size = (sx <= 32) ? sx : 32;
    z_x_avg_profile_size = (z_x_avg_profile_size < 0) ? 0 : z_x_avg_profile_size;
    z_y_avg_profile_size = (sy <= 32) ? sy : 32;
    z_y_avg_profile_size = (z_y_avg_profile_size < 0) ? 0 : z_y_avg_profile_size;
    general_im_action = find_z_avg_profile_on_point;
    return D_O_K;
}



int place_selected_point_in_plot(imreg *imr, int x_0, int y_0, int mode)
{
    static int nop;
    float px1, py1;
    static float pxp = 0, pyp = 0;
    O_i *oi = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;


    (void)mode;
    if (imr->one_i == NULL)   return win_printf_OK("Could not find image data");
    oi = imr->one_i;
    nop = find_op_nb_of_source_specific_ds_in_oi(oi, "X-Y points from image");
    if (nop >= 0)
    {
        op = oi->o_p[nop];
        ds = find_source_specific_ds_in_op(op, "X-Y points from image");
    }
    else
    {
        op = create_and_attach_op_to_oi(oi, 1024, 1024, 0, IM_SAME_Y_AXIS|IM_SAME_X_AXIS);
        if (op == NULL)	return win_printf_OK("Could not create plot data");
        ds = op->dat[0];
        set_ds_dot_line(ds);
        set_ds_point_symbol(ds, "\\pt4\\oc");
        set_ds_source(ds, "X-Y points from image");
        ds->nx = ds->ny = 0;
    }
    px1 = x_imr_2_imdata(imr, x_0);
    py1 = y_imr_2_imdata(imr, y_0);
    if (px1 != pxp || py1 != pyp)
    {
        if (ds->nx >= ds->mx || ds->ny >= ds->my)
            ds = build_adjust_data_set(ds, ds->mx + 256, ds->mx + 256);
        if (ds == NULL)	return win_printf_OK("Could not adjust plot data");
        pxp = ds->xd[ds->nx++] = px1;
        pyp = ds->yd[ds->ny++] = py1;
    }
    return 0;
}


int    set_point_to_plot(void)
{
    if(updating_menu_state != 0)
    {
        if (general_im_action == place_selected_point_in_plot)
            active_menu->flags |= D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;
        return D_O_K;
    }
    if (general_im_action != place_selected_point_in_plot)
    {
        prev_general_im_action = general_im_action;
        general_im_action = place_selected_point_in_plot;
    }
    return D_O_K;
}


int    refresh_image(imreg *imr, int n_im)
{
    DIALOG *di = NULL;

    if (imr == NULL)        return -1;

    if (n_im != UNCHANGED && n_im <= imr->n_oi && n_im >= 0)
    {
        imr->cur_oi = n_im;
        imr->cur_oi = (imr->cur_oi >= imr->n_oi || imr->cur_oi < 0)
            ? 0 : imr->cur_oi;
        imr->one_i = imr->o_i[imr->cur_oi];
    }
    /*    dump_to_xv_log_file_only("refresh_image %d state %d",imr->cur_oi,imr->one_i->need_to_refresh);*/
    di = find_dialog_associated_to_imr(imr, NULL);
    if (di) di->proc(MSG_DRAW, di, 0);
    else broadcast_dialog_message(MSG_DRAW,0);
    return D_O_K;
}

int    refresh_im_plot(imreg *imr, int n)
{
    DIALOG *di = NULL;

    if (n != UNCHANGED)
    {
        n = (n < 0) ? 0 : n;
        n = (n >= imr->one_i->n_op) ? imr->one_i->n_op -1 : n;
        imr->one_i->cur_op = n;
    }
    imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
    di = find_dialog_associated_to_imr(imr, NULL);
    if (di) di->proc(MSG_DRAW, di, 0);
    else broadcast_dialog_message(MSG_DRAW,0);
    return D_O_K;
}



int find_tilted_profile(imreg *imr, int x_0, int y_0, int mode)
{
    int i;
    const char  *xun = NULL, *yun = NULL;
    int x, y, xw, yw, np, ndi;
    int old_x = 0, old_y, color; // , nx
    float tmpx, tmpy,  px0, py0, px1, py1;
    float  *zt = NULL;
    O_i *oi = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;

    if (mode == 0) return D_O_K;
    if (imr->one_i == NULL)    return win_printf_OK("Could not find image data");
    oi = imr->one_i;
    //nx = oi->im.nx;
    old_x = x = mouse_x; old_y = y = mouse_y;
    px1 = px0 = x_imr_2_imdata(imr, mouse_x);
    py1 = py0 = y_imr_2_imdata(imr, mouse_y);

    np = 1 + (int)sqrt(oi->im.nx * oi->im.nx + oi->im.ny * oi->im.ny);
    op = create_and_attach_op_to_oi(oi, np, np, 0, 0);
    oi->cur_op = oi->n_op - 1;
    ds = op->dat[0];
    color = makecol(255, 64, 64);
    ndi = retrieve_item_from_dialog (d_draw_Im_proc, NULL);
    if (ndi < 0) return win_printf_OK("Could not find image dialog");
    while (mouse_b & 0x3)
    {
        xw = x = mouse_x  - imr->x_off;
        yw = y = imr->y_off - mouse_y;
        if (x  < 0)         xw = 0;
        if (x > imr->s_nx)     xw = imr->s_nx;
        if (y  < 0)         yw = 0;
        if (y > imr->s_ny)    yw = imr->s_ny;
        if ( x != xw || y != yw)
        {
            x = xw; y = yw;
            xw += imr->x_off;
            yw = imr->y_off - yw;
            mouse_x = xw;
            mouse_y = yw;
        }
        if(x != old_x || x != old_y)
        {
            px1 = x_imr_2_imdata(imr, mouse_x);
            py1 = y_imr_2_imdata(imr, mouse_y);

            tmpx = oi->dx * (px1 - px0);
            tmpy = oi->dy * (py1 - py0);
            xun = (oi->x_unit != NULL) ? oi->x_unit : " ";
            yun = (oi->y_unit != NULL) ? oi->y_unit : " ";

            zt = extract_raw_tilted_profile(oi, ds->yd, ds->mx, px0, py0,
                                            px1, py1, &(ds->nx));
            if (zt != NULL)
            {
                for (i = 0; i < ds->nx && i < ds->mx; i++) ds->xd[i] = i;
                ds->yd = zt;
                ds->ny = ds->nx;
            }
            oi->need_to_refresh |= PLOTS_NEED_REFRESH;
            do_one_image (imr);
            oi->need_to_refresh &= BITMAP_NEED_REFRESH;
            write_and_blit_im_box(plt_buffer, active_dialog + ndi, imr);
            draw_bubble(screen, B_CENTER, mouse_x, mouse_y-20,
                        //                "px0 %g py0 %g px1 %g py1 %g",px0,py0,px1,py1);
                "w = %g %s h = %g %s",tmpx,xun,tmpy,yun);
	    for(;screen_acquired;); // we wait for screen_acquired back to 0;
	    screen_acquired = 1;
            acquire_bitmap(screen);
            line(screen,x_0,y_0,mouse_x,mouse_y,color);
            release_bitmap(screen);
	    screen_acquired = 0;
            old_x = x;
            old_y = y;

        }
    }
    set_formated_string (&(ds->source), "Tilted profile of image %s %s\n"
                         "starting at (%g,%g) ending at (%g,%g)", oi->filename, oi->dir,
                         px0,py0,px1,py1);
    return D_O_K;
}

int     do_tilted_profile(void)
{
    if(updating_menu_state != 0)    return D_O_K;
    prev_general_im_action = general_im_action;
    general_im_action = find_tilted_profile;
    return 0;
}
#endif
