#pragma once

#ifndef XV_WIN32
# include "config.h"
#endif

#include <vector>
#include <string>
#include <gtk/gtk.h>

class DataList
{
  public:
    DataList();
    virtual ~DataList();

    int setTitle(std::string& title);
    int setHeaders(std::vector<std::string>& text, std::vector<GType>& types);
    int addRow(...);
    int pushLevel();
    int popLevel();

    int run();
    static int dumpCsv(GtkTreeView *treeView);


  private:
    GtkTreeView *treeView_ = 0;
    GtkWidget *window_ = 0;
    GtkCellRenderer *cellRenderer_ = 0;
    GtkTreeStore *treeStore_ = 0;
    std::string title_ = "List";
    std::vector<GtkTreeIter> parents_ = std::vector<GtkTreeIter> ();
    GtkTreeIter lastItem_;
};
