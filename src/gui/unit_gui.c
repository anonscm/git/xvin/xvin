/*	
 *	DESCRIPTION	xvin plot functions related to unit sets
 *			
 */
#ifndef _UNIT_GUI_C_
#define _UNIT_GUI_C_

#include "platform.h"




# include "unitset.h"
# include "xvinerr.h"
# include "util.h"
# include "imr_reg_gui.h"
# include "xvin.h"

# include <stdlib.h>
# include <string.h>

/*

# include "unitset.h"
# include "xvinerr.h"
# include "util.h"
# include "xvin.h"



# ifdef XV_WIN32
# include <allegro.h>
# include "winalleg.h"
# endif
# include "imr_reg_gui.h"

# include <stdlib.h>
# include <string.h>

*/
un_s	*to_modify_un = NULL;

int decase_to_index[] = {-1,12,9,6,3,0,-3,-6,-9,-12,-15,0};

MENU decade_menu[32] =
{
   { "  Other",      NULL,       NULL,       0, NULL  },
   { "  Tera",      NULL,       NULL,       0, NULL  },
   { "  Giga",      NULL,    	NULL,       0, NULL  },
   { "  Mega",      NULL,       NULL,       0, NULL  },
   { "  kilo",  NULL,     NULL,       0, NULL  },
   { "  One",  NULL,     NULL,       0, NULL  },
   { "  Milli",  NULL,     NULL,       0, NULL  },
   { "  micro",  NULL,     NULL,       0, NULL  },
   { "  Nano",  NULL,     NULL,       0, NULL  },
   { "  Pico",  NULL,     NULL,       0, NULL  },
   { "  Femto",  NULL,     NULL,       0, NULL  },
   { NULL,                    NULL,             NULL,       0, NULL  }
};

int do_decade_menu(int item)
{
	if (item == 0)		return -1;
	else if (item == 1)	return 12;
	else if (item == 2)	return 9;
	else if (item == 3)	return 6;
	else if (item == 4)	return 3;
	else if (item == 5)	return 0;
	else if (item == 6)	return -3;
	else if (item == 7)	return -6;
	else if (item == 8)	return -9;
	else if (item == 9)	return -12;
	else if (item == 10)	return -15;	
	else return -1; 
}



int	pop_unit_decade_mn(void)
{
	register int i;
	if(updating_menu_state != 0)	return D_O_K;		
	for (i = 0; decade_menu[i].text != NULL && i < 11; i++)
		decade_menu[i].flags = (last_popup_index == decase_to_index[i]) 
									? D_SELECTED : 0;
	return do_decade_menu(do_menu(decade_menu,mouse_x,mouse_y));
}



MENU unit_type_menu[32] =
{
   { "  Other",      NULL,       NULL,       0, NULL  },
   { "  Volt",      NULL,       NULL,       0, NULL  },
   { "  Meter",      NULL,    	NULL,       0, NULL  },
   { "  Newton",      NULL,    	NULL,       0, NULL  },
   { "  Second",      NULL,       NULL,       0, NULL  },
   { "  Gram",  NULL,     NULL,       0, NULL  },
   { "  Gauss",  NULL,     NULL,       0, NULL  },
   { "  Ohm",  NULL,     NULL,       0, NULL  },
   { "  Ampere",  NULL,     NULL,       0, NULL  },
   { NULL,                    NULL,             NULL,       0, NULL  }
};


int do_unit_type_menu(int item)
{
	if (item == 0)		return 0;
	else if (item == 1)	return IS_VOLT;
	else if (item == 2)	return IS_METER;
	else if (item == 3)	return IS_NEWTON;
	else if (item == 4)	return IS_SECOND;
	else if (item == 5)	return IS_GRAMME;
	else if (item == 6)	return IS_GAUSS;
	else if (item == 7)	return IS_OHM;
	else if (item == 8)	return IS_AMPERE;
	else return -1; 
}



int	pop_unit_type_mn(void)
{
	register int i;
	if(updating_menu_state != 0)	return D_O_K;		
	for (i = 0; unit_type_menu[i].text != NULL && i < 7; i++)
		unit_type_menu[i].flags = (last_popup_index == i) ? D_SELECTED : 0;
	return do_unit_type_menu(do_menu(unit_type_menu,mouse_x,mouse_y));
}




int 	modify_un_decade(void)
{
	register int i;
	char s[128] = {0};
	int index, prev_dec;
	pltreg *pr = NULL;
	imreg *imr = NULL; 

	if(updating_menu_state != 0)	return D_O_K;		
	
	pr = find_pr_in_current_dialog(NULL);
	imr = find_imr_in_current_dialog(NULL);	
	if (pr == NULL && imr == NULL)	
		return (win_printf("cannot find plot or image"));
	index = mn_index;
	if (index == X_AXIS && pr != NULL)	
		to_modify_un = pr->one_p->xu[pr->one_p->c_xu];	
	else if (index == Y_AXIS && pr != NULL)
		to_modify_un = pr->one_p->yu[pr->one_p->c_yu];	
	else if (index == X_AXIS && imr != NULL)
		to_modify_un = imr->one_i->xu[imr->one_i->c_xu];	
	else if (index == Y_AXIS && imr != NULL)
		to_modify_un = imr->one_i->yu[imr->one_i->c_yu];
	else if (index == Z_AXIS && imr != NULL)
		to_modify_un = imr->one_i->zu[imr->one_i->c_zu]; 
	else return win_printf("Not a nice unit!\n index %d\n",index);
	if (to_modify_un == NULL)	win_printf("Unit not found!");
	if (generate_units(to_modify_un->type,to_modify_un->decade) == NULL)
		return win_printf("Not a nice unit\ntype %d\ndecade %d!",to_modify_un->type,to_modify_un->decade);
	s[0] = 0;
	prev_dec = to_modify_un->decade;
/*	
	i = win_scanf("Choose decade%d%m",&to_modify_un->decade,unit_decade_mn());
	if (i == WIN_CANCEL)	goto aborting;
*/	
	last_popup_index = to_modify_un->decade;
	to_modify_un->decade = pop_unit_decade_mn();		
	if (generate_units(to_modify_un->type,to_modify_un->decade) != NULL)
		free(to_modify_un->name);
	to_modify_un->name = generate_units(to_modify_un->type, to_modify_un->decade);
	if (to_modify_un->name == NULL)
	{
		i = win_scanf("X unit set type%d offset%f increment%f name %ls",&to_modify_un->type,&to_modify_un->ax, &to_modify_un->dx, s);			
		if (i == WIN_CANCEL)	goto aborting;	
		if (to_modify_un->name != NULL)	free(to_modify_un->name);
		to_modify_un->name = Mystrdup(s);			
	}	
	if (to_modify_un->decade != prev_dec)
		to_modify_un->dx *= pow(10,prev_dec - to_modify_un->decade);
	to_modify_un = NULL;
	if (index == X_AXIS && pr != NULL)
		return set_plt_x_unit_set(pr, pr->one_p->c_xu);	
	else if (index == Y_AXIS && pr != NULL)
		return set_plt_y_unit_set(pr, pr->one_p->c_yu);	
	if (index == X_AXIS && imr != NULL)
		return set_im_x_unit_set(imr, imr->one_i->c_xu);	
	else if (index == Y_AXIS && imr != NULL)
		return set_im_y_unit_set(imr, imr->one_i->c_yu);	
	else if (index == Z_AXIS && imr != NULL)
		return set_im_y_unit_set(imr, imr->one_i->c_zu);	
	else return 1;	
	return 0;
aborting :	to_modify_un = NULL;
		return 0;	
}

int 	modify_un_decade_prime(void)
{
	register int i;
	char s[128] = {0};
	int index, prev_dec;
	pltreg *pr = NULL;
	imreg *imr = NULL;

	if(updating_menu_state != 0)	return D_O_K;		
	pr = find_pr_in_current_dialog(NULL);		
	imr = find_imr_in_current_dialog(NULL);			
	if (pr == NULL && imr == NULL)	
		return (win_printf("cannot find plot or image"));	
	index = mn_index;
	if (index == X_AXIS && pr != NULL)	
		to_modify_un = pr->one_p->xu[pr->one_p->c_xu_p];	
	else if (index == Y_AXIS && pr != NULL)
		to_modify_un = pr->one_p->yu[pr->one_p->c_yu_p];	
	else if (index == X_AXIS && imr != NULL)
		to_modify_un = imr->one_i->xu[imr->one_i->c_xu];	
	else if (index == Y_AXIS && imr != NULL)
		to_modify_un = imr->one_i->yu[imr->one_i->c_yu];
	else if (index == Z_AXIS && imr != NULL)
		to_modify_un = imr->one_i->zu[imr->one_i->c_zu];
	else return win_printf("Not a nice unit!");
	if (to_modify_un == NULL)	win_printf("Unit not found!");
	if (generate_units(to_modify_un->type,to_modify_un->decade) == NULL)
		return win_printf("Not a nice unit\ntype %d\ndecade %d!",to_modify_un->type,to_modify_un->decade);
	s[0] = 0;
	prev_dec = to_modify_un->decade;
/*	
	i = win_scanf("Choose decade%d%m",&to_modify_un->decade,unit_decade_mn());
	if (i == WIN_CANCEL)	goto aborting;
*/	
	last_popup_index = to_modify_un->decade;
	to_modify_un->decade = pop_unit_decade_mn();			
	if (generate_units(to_modify_un->type,to_modify_un->decade) != NULL)
		free(to_modify_un->name);
	to_modify_un->name = generate_units(to_modify_un->type, to_modify_un->decade);
	if (to_modify_un->name == NULL)
	{
		i = win_scanf("X unit set type%d offset%f increment%f name %ls",&to_modify_un->type,&to_modify_un->ax, &to_modify_un->dx, s);			
		if (i == WIN_CANCEL)	goto aborting;	
		if (to_modify_un->name != NULL)	free(to_modify_un->name);
		to_modify_un->name = Mystrdup(s);			
	}	
	if (to_modify_un->decade != prev_dec)
		to_modify_un->dx *= pow(10,prev_dec - to_modify_un->decade);
	to_modify_un = NULL;
	if (index == X_AXIS && pr != NULL)
		return set_plt_x_unit_set(pr, pr->one_p->c_xu);	
	else if (index == Y_AXIS && pr != NULL)
		return set_plt_y_unit_set(pr, pr->one_p->c_yu);	
	if (index == X_AXIS && imr != NULL)
		return set_im_x_unit_set(imr, imr->one_i->c_xu);	
	else if (index == Y_AXIS && imr != NULL)
		return set_im_y_unit_set(imr, imr->one_i->c_yu);	
	else if (index == Z_AXIS && imr != NULL)
		return set_im_y_unit_set(imr, imr->one_i->c_zu);	
	else return 1;	
	return 0;
aborting :	to_modify_un = NULL;
		return 0;	
}
int 	add_new_units(void)
{
  un_s *lux = NULL;
  register int i;
  char s[128] = {0};
  static char uname[64] = "no_name";	
  int index;
  pltreg *pr = NULL;
  imreg *imr = NULL; 
  //	int type;
  static float ax = 0, dx = 1;
  
  if(updating_menu_state != 0)	return D_O_K;		
  pr = find_pr_in_current_dialog(NULL);		
  imr = find_imr_in_current_dialog(NULL);		
  if (pr == NULL && imr == NULL)	
    return (win_printf("cannot find plot or image"));
  index = mn_index;
  lux = build_unit_set(0, 0, 1, 0, 0, "no_name");
  strcpy(s,uname);
  if (lux == NULL)	return (win_printf("cannot create unit set"));
  to_modify_un = lux;
  /*
    i = win_scanf("Type of unit%d%m",&lux->type,unit_type_mn());
    if (i == WIN_CANCEL)	goto aborting;	
  */	
  last_popup_index = 0;
  lux->type = pop_unit_type_mn();
  if (lux->type < 0)  
    {
      free_unit_set(lux);
      return win_printf_OK("No unit set created");
    }
  if (lux->type == 0)
    {
      i = win_scanf("X unit set of type other\noffset %12f\nincrement%12f\n"
		    "name (no\\_name suppreses any indication) %ls",
		    &ax, &dx, s);
      if (i == WIN_CANCEL)	goto aborting;	
		lux->ax = ax;
		lux->dx = dx;
		strcpy(uname,s);
		lux->name = (strncmp(s,"no_name",7) == 0) ? NULL : Mystrdup(s);		
    }
  else 
    {
      /*		
			i = win_scanf("Choose decade%d%m",&lux->decade,unit_decade_mn());
			if (i == WIN_CANCEL)	goto aborting;			
      */		
      last_popup_index = to_modify_un->decade;
      lux->decade = pop_unit_decade_mn();	
      lux->name = generate_units(lux->type, lux->decade);
      if (lux->name == NULL)
	{
	  i = win_scanf("X unit set type%d offset%f increment%f"
			"name (no\\_name suppreses any indication) %ls",
			&lux->type,&ax, &dx, s);
	  if (i == WIN_CANCEL)	goto aborting;	
	  lux->ax = ax;
	  lux->dx = dx;
	  strcpy(uname,s);
	  if (lux->name != NULL)	free(lux->name);
	  lux->name = (strncmp(s,"no_name",7) == 0) ? NULL : Mystrdup(s);				}
      else
	{
	  i = win_scanf("offset%f increment%f",&ax,&dx);			
	  if (i == WIN_CANCEL)	goto aborting;	
	  lux->ax = ax;
	  lux->dx = dx;			
	}
    }
  to_modify_un = NULL;
  if (index == X_AXIS && pr != NULL)
    {
      add_one_plot_data (pr->one_p, IS_X_UNIT_SET, (void *)lux);
      return set_plt_x_unit_set(pr, pr->one_p->n_xu-1);	
    }
  else if (index == Y_AXIS && pr != NULL)
    {
      add_one_plot_data (pr->one_p, IS_Y_UNIT_SET, (void *)lux);
      return set_plt_y_unit_set(pr, pr->one_p->n_yu-1);	
    }	
  else if (index == X_AXIS && imr != NULL)
    {
      add_one_image (imr->one_i, IS_X_UNIT_SET, (void *)lux);
      return set_im_x_unit_set(imr, imr->one_i->n_xu-1);	
    }
  else if (index == Y_AXIS && imr != NULL)
    {
      add_one_image (imr->one_i, IS_Y_UNIT_SET, (void *)lux);
      return set_im_y_unit_set(imr, imr->one_i->n_yu-1);	
    }		
  else if (index == Z_AXIS && imr != NULL)
    {
      add_one_image (imr->one_i, IS_Z_UNIT_SET, (void *)lux);
      return set_im_z_unit_set(imr, imr->one_i->n_zu-1);	
    }				
  else if (index == T_AXIS && imr != NULL)
    {
      add_one_image (imr->one_i, IS_T_UNIT_SET, (void *)lux);
      return set_oi_t_unit_set(imr->one_i, imr->one_i->n_tu-1);	
    }				
  else return 1;
  return 0;
 aborting :	if (lux)  free(lux);
  to_modify_un = NULL;
  return 0;
}



int 	modify_units(void)
{
	un_s *lux = NULL;
	register int i;
	int index;
	pltreg *pr;
	float ax, dx;
	imreg *imr = NULL;

	if(updating_menu_state != 0)	return D_O_K;		
	pr = find_pr_in_current_dialog(NULL);		
	imr = find_imr_in_current_dialog(NULL);			
	if (pr == NULL && imr == NULL)	
		return (win_printf("cannot find plot or image"));	
	index = mn_index;
	
	if (index == X_AXIS && pr != NULL)
		lux = pr->one_p->xu[pr->one_p->c_xu];	
	else if (index == Y_AXIS && pr != NULL)
		lux = pr->one_p->yu[pr->one_p->c_yu];	
	if (index == X_AXIS && imr != NULL)
		lux =  imr->one_i->xu[imr->one_i->c_xu];	
	else if (index == Y_AXIS && imr != NULL)
		lux =  imr->one_i->yu[imr->one_i->c_yu];	
	else if (index == Z_AXIS && imr != NULL)
		lux =  imr->one_i->zu[imr->one_i->c_zu];	
	if (lux == NULL)	return (win_printf("cannot find unit set"));

	ax = lux->ax;
	dx = lux->dx;	
	i = win_scanf("Modify current unit set\noffset %12f\nincrement%12f\n",&ax,&dx);			
	if (i == WIN_CANCEL)	return 0;	
	lux->ax = ax;
	lux->dx = dx;			
	
	if (index == X_AXIS && pr != NULL)
		return set_plt_x_unit_set(pr, pr->one_p->c_xu);	
	else if (index == Y_AXIS && pr != NULL)
		return set_plt_y_unit_set(pr, pr->one_p->c_yu);	
	if (index == X_AXIS && imr != NULL)
		return set_im_x_unit_set(imr, imr->one_i->c_xu);	
	else if (index == Y_AXIS && imr != NULL)
		return set_im_y_unit_set(imr, imr->one_i->c_yu);	
	else if (index == Z_AXIS && imr != NULL)
		return set_im_y_unit_set(imr, imr->one_i->c_zu);	
		
	else return 1;
	return 0;
}

int 	modify_units_prime(void)
{
	un_s *lux = NULL;
	register int i;
	int index;
	pltreg *pr = NULL;
	float ax, dx;
	imreg *imr = NULL;

	
	if(updating_menu_state != 0)	return D_O_K;	

	pr = find_pr_in_current_dialog(NULL);		
	imr = find_imr_in_current_dialog(NULL);			
	if (pr == NULL && imr == NULL)	
		return (win_printf("cannot find plot or image"));	
	index = mn_index;
	
	if (index == X_AXIS && pr != NULL)
		lux = pr->one_p->xu[pr->one_p->c_xu_p];	
	else if (index == Y_AXIS && pr != NULL)
		lux = pr->one_p->yu[pr->one_p->c_yu_p];	
	if (index == X_AXIS && imr != NULL)
		lux =  imr->one_i->xu[imr->one_i->c_xu];	
	else if (index == Y_AXIS && imr != NULL)
		lux =  imr->one_i->yu[imr->one_i->c_yu];	
	else if (index == Z_AXIS && imr != NULL)
		lux =  imr->one_i->zu[imr->one_i->c_zu];	

	if (lux == NULL)	return (win_printf("cannot find unit set"));

	ax = lux->ax;
	dx = lux->dx;	
	i = win_scanf("Modify current unit set\noffset %12f\n increment%12f\n",&ax,&dx);			
	if (i == WIN_CANCEL)	return 0;	
	lux->ax = ax;
	lux->dx = dx;			
	
	if (index == X_AXIS && pr != NULL)
		return set_plt_x_unit_set(pr, pr->one_p->c_xu);	
	else if (index == Y_AXIS && pr != NULL)
		return set_plt_y_unit_set(pr, pr->one_p->c_yu);	
	if (index == X_AXIS && imr != NULL)
		return set_im_x_unit_set(imr, imr->one_i->c_xu);	
	else if (index == Y_AXIS && imr != NULL)
		return set_im_y_unit_set(imr, imr->one_i->c_yu);	
	else if (index == Z_AXIS && imr != NULL)
		return set_im_y_unit_set(imr, imr->one_i->c_zu);	
	else return 1;
	return 0;
}

# endif
