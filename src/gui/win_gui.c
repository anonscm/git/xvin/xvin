#ifndef _WIN_GUI_C_
#define _WIN_GUI_C_

#include "xvin.h"

#include <allegro.h>

#ifdef XV_WIN32
#include <winalleg.h> // for 'win_get_window()'
#endif

#include "xv_main.h"
# include <alfont.h>
#include "fonts.h"
# include "box2alfn.h"
#include "locale.h"
# include <stdlib.h>
# include <string.h>

#ifdef XV_GTK_DIALOG
#include "form_builder.h"
#endif

/* we need to load fonts.dat to access the big font */


/*    int win_print(char *fmt, ...)
 *    DESCRIPTION    Print message in a window in a way similar to printf.
 *            The format "fmt" may contain messages with the
 *            %s, %d, %f etc formatting info of printf and the
 *            associated parameters.
 *            "\n"    produces a new line.
 *            "\t"    produces a tab spacing.
 *             The size of the window adapts to the number and size
 *            of the messages.
 *
 *    EXEMPLE        win_printf("\t\tHello world\n good morning M %s",name)
 *
 *    RETURNS        OK on success, WIN_CANCEL otherwise
 */


char *win_printf_message = NULL;

int d_myclear_proc(int msg, DIALOG *d, int c);
int debug_message(const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));


static int verror_message_log(const char *log_file, const char *fmt, va_list ap) __attribute__ ((format (printf, 2, 0)));
static int vmessage_log(const char *log_file, const char *message_type, const char *fmt, va_list ap) __attribute__((format(printf, 3, 0)));
static int vwarning_message_log(const char *log_file, const char *fmt, va_list ap) __attribute__((format(printf, 2, 0)));
static int vdebug_message_log(const char *log_file, const char *fmt, va_list ap) __attribute__ ((format (printf, 2, 0)));



#ifdef XV_MAC
static int d_print_slash_proc(int msg, DIALOG *d, int c)
{
    int ret;
    //   ASSERT(d); // debuging
    /* call the parent object */
    ret = d_button_proc(msg, d, c);

    if (msg == MSG_CLICK)
    {
        d->dp2 = my_sprintf(d->dp2, "\\");
    }

    /* trap the close return value and change the font */
    if (ret == D_CLOSE)
    {
        return D_REDRAW;
    }

    /* otherwise just return */
    return ret;
}
#endif



static int d_picoTeX_proc(int msg, DIALOG *d, int c)
{
    struct box *b = NULL;
    int tf = ttf_enable;
    ASSERT(d);

    (void)c;

    if (msg == MSG_DRAW)
    {
        if (d->dp2 == NULL)
        {
            b = (struct box *)calloc(1, sizeof(struct box));

            if (b == NULL)
            {
                return D_O_K;
            }

            ttf_enable = 0;
            init_box(b);
            b->color = White;
            string_to_box((char *) d->dp, b);  // UNSAFE CAST
            set_box_origin(b, d->x, d->y + d->h - 20, screen_to_box);
            d->dp2 = (void *)b;
        }

        b = (struct box *)d->dp2;
        //    set_clip(screen, d->x, d->y, d->x+d->w-1, d->y+d->h-1);
        set_box_origin(b, d->x, SCREEN_H - d->y, text_scale);

# ifdef XV_WIN32

        if (win_get_window() == GetForegroundWindow())
# endif
        {
            //  if ( screen_used)
            //  {
            //screen_used = 1;
            //scare_mouse();
            //acquire_screen();
            display_box(b, text_scale, White, TRUE_COLOR);
            /*    wr_box(b); */
            //release_screen();
            //unscare_mouse();
            //screen_used = 0;
            // }
        }

        set_clip_rect(screen, 0, 0, SCREEN_W - 1, SCREEN_H - 1);
    }

    ttf_enable = tf;
    return D_O_K;
}

static int d_title_proc(int msg, DIALOG *d, int c)
{
    ASSERT(d);

    (void)c;

    if (msg == MSG_CLICK)
    {
        return D_EXIT;
    }

    return d_button_proc(msg, d, 0);
}




/*    add_text_to_box (struct box* ba, int x0, int y0, char *text);
 *    DESCRIPTION    Add a text to be display in x0, y0, y0 goes
 *            in viewport unit
 *
 *    RETURNS        0 on success, 1
 */
static struct box *add_simple_text_to_box(struct box *ba, int x0, int y0, char *text)
{
    struct box *bo = NULL;

    if (ba->n_box == 0)
    {
        init_box(ba);
    }

    bo = mknewbox(ba);

    if (bo == NULL)
    {
        return NULL;
    }

    bo->color = White;
    simple_string_to_box(text, bo);
    set_box_origin(bo, x0, -y0, text_scale);
    ba->w = (ba->w < bo->xc + bo->w) ? bo->xc + bo->w : ba->w;
    ba->d = (ba->d < - bo->yc + bo->d) ? - bo->yc + bo->d : ba->d;
    ba->h = (ba->h < bo->yc + bo->h) ? bo->yc + bo->h : ba->h;
    return bo;
}

/*    add_text_to_box (struct box* ba, int x0, int y0, char *text);
 *    DESCRIPTION    Add a text to be display in x0, y0, y0 goes
 *            in viewport unit
 *
 *    RETURNS        0 on success, 1
 */
static struct box *add_text_to_box(struct box *ba, int x0, int y0, char *text)
{
    struct box *bo = NULL;

    if (ba->n_box == 0)
    {
        init_box(ba);
    }

    bo = mknewbox(ba);

    if (bo == NULL)
    {
        return NULL;
    }

    bo->color = White;
    string_to_box(text, bo);
    set_box_origin(bo, x0, -y0, text_scale);
    ba->w = (ba->w < bo->xc + bo->w) ? bo->xc + bo->w : ba->w;
    ba->d = (ba->d < - bo->yc + bo->d) ? - bo->yc + bo->d : ba->d;
    ba->h = (ba->h < bo->yc + bo->h) ? bo->yc + bo->h : ba->h;
    return bo;
}

static int vmessage_log(const char *log_file, const char *message_type, const char *fmt, va_list ap)
{
    FILE *file = NULL;

    if (log_file)
    {
        file = fopen(log_file, "a");

        if (file != NULL)
        {
            fprintf(file, "%s", message_type);
            vfprintf(file, fmt, ap);
            fprintf(file, "%s", "\n");
            fclose(file);
        }
    }

    return file != NULL;
}


static int vwarning_message_log(const char *log_file, const char *fmt, va_list ap)
{
    va_list ap_log;

    va_copy(ap_log, ap);

#ifndef NO_WARNING_MESSAGES
#ifndef NO_WARNING_WINDOWS
    win_ti_vprintf("Warning", fmt, ap);
#else
    vprintf(fmt, ap);
    printf("\n");
#endif
    va_end(ap);
#endif

    vmessage_log(log_file, "Warning: ", fmt, ap_log);
    va_end(ap_log);

    return D_O_K;
}


int warning_message(const char *fmt, ...)
{
    va_list ap;
#ifndef NO_WARNING_MESSAGES
    va_start(ap, fmt);
    vwarning_message_log(NULL, fmt, ap);
    va_end(ap);
#endif
    return D_O_K;
}
int warning_message_log(const char *log_file, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vwarning_message_log(log_file, fmt, ap);
    va_end(ap);
    return D_O_K;

}

static int vdebug_message_log(const char *log_file, const char *fmt, va_list ap)
{
    va_list ap_log;

    va_copy(ap_log, ap);

#ifdef DEBUG_MESSAGES
#ifdef DEBUG_WINDOWS
    win_ti_vprintf("Debug", fmt, ap);
#else
    vprintf(fmt, ap);
    printf("\n");
#endif
    va_end(ap);
#endif

    vmessage_log(log_file, "Debug: ", fmt, ap_log);
    va_end(ap_log);

    return D_O_K;
}


#ifdef DEBUG_MESSAGES
int debug_message(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vdebug_message_log(NULL, fmt, ap);
    va_end(ap);
    return D_O_K;
}
#else
int debug_message(const char *fmt, ...)
{
  (void)*fmt;
  return D_O_K;
}
#endif


int debug_message_log(const char *log_file, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    vdebug_message_log(log_file, fmt, ap);
    va_end(ap);
    return D_O_K;

}


static int verror_message_log(const char *log_file, const char *fmt, va_list ap)
{
    va_list ap_log;

    va_copy(ap_log, ap);

#ifndef NO_ERROR_WINDOWS
    win_ti_vprintf("Error", fmt, ap);
#else
    vprintf(fmt, ap);
#endif

    vmessage_log(log_file, "Error: ", fmt, ap_log);
    va_end(ap_log);

    return D_O_K;
}


int error_message(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    verror_message_log(NULL, fmt, ap);
    va_end(ap);
    return D_O_K;
}

int error_message_log(const char *log_file, const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    verror_message_log(log_file, fmt, ap);
    va_end(ap);
    return D_O_K;

}

int win_printf_OK(const char *fmt, ...)
{
    //int i;
    va_list ap;
    va_start(ap, fmt);
    win_ti_vprintf("Message", fmt, ap);
    va_end(ap);
    return D_O_K;
}

int win_printf(const char *fmt, ...)
{
    int i;
    va_list ap;
    va_start(ap, fmt);
    i = win_ti_vprintf("Message", fmt, ap);
    va_end(ap);
    return i;
}
int win_ti_printf(const char *title, const char *fmt, ...)
{
    int i;
    va_list ap;
    va_start(ap, fmt);
    i = win_ti_vprintf(title, fmt, ap);
    va_end(ap);
    return i;
}

#ifdef XV_GTK_DIALOG
static char *tex_to_pango_markup(const char *src)
{
    GError *err = NULL;
    gchar *res = NULL;
    gchar *tmp = g_markup_escape_text(src, -1);
    size_t i = 0;
    int    done = 0;
    char *reg[] = { "{\\\\raw (.*)}",                  "\\1",
                    "_\\{(.*)\\}|_(.)",              "<sub>\\1\\2</sub>",
                    "\\^\\{(.*)\\}|\\^(.)",          "<sup>\\1\\2</sup>",
                    "\\\\frac\\{(.*)\\}\\{(.*)\\}",  "\\1 / \\2",
                    "\\{\\\\color\\{(.*)\\}(.*)\\}", "<b>\\2</b>",
                    "\\\\sigma",                     "&#963;",
                    "\\\\Sigma",                     "&#931;",
                    "\\\\delta",                     "&#916;",
                    "\\\\[Gg]amma",                  "&#947;",
                    "\\\\Mu",                     "&#924;",
                    "\\\\mu",                     "&#956;",
                    "\\\\[Oo]c",                     "&#8226;",
                  };

    for(; i < sizeof(reg)/sizeof(char*) && done == 0; i += 2)
    {
        GRegex *gex = g_regex_new(reg[i], G_REGEX_UNGREEDY, G_REGEX_MATCH_NEWLINE_ANY, &err);
        res = g_regex_replace(gex, tmp, -1, 0, reg[i+1],    G_REGEX_MATCH_NEWLINE_ANY, &err);
        if(i == 0 && strcmp(tmp, res) != 0)
            done = 1;

        free(tmp);
        g_regex_unref(gex);
        tmp = res;

    }
    return res;
}

static int win_ti_form_vprintf(const char *title, const char *fmt, va_list ap)
{
    int i;
    char *pango_fmt = NULL;
    setlocale(LC_ALL, "C");
    form_window_t *form = NULL;

    form = form_create(title);
    pango_fmt = tex_to_pango_markup(fmt);
//    printf("%s\n",fmt);
//    printf("%s\n",pango_fmt);
    form_push_label_ap(form, pango_fmt, ap);
    i = form_run(form);
    form_free(form);

    return i;
}
#endif

int IS_BATCH_SCRIPT = 0;
int win_ti_vprintf(const char *title, const char *fmt, va_list ap)
{
    int i;
    char *p = NULL, *c = NULL, *s = NULL;
    DIALOG *win_dialog;
    int  n_dia = 0;
    struct box *bo, *ba = NULL;
    int wth, hg, Lw, bg, fg, x0 = 150, y0 = 350; //l
    int col = 1, n_char = 0, hl = 0, x, y, w, h;
    int tf = ttf_enable;
    DIALOG_PLAYER *player = NULL;
    BITMAP *bmp = NULL, *bmps = NULL;
    int finish, m_x, m_y;

    if(IS_BATCH_SCRIPT == 1)
        return WIN_OK;

    setlocale(LC_ALL, "C");

#ifdef XV_GTK_DIALOG
#ifndef XV_NO_GTK_WIN_PRINTF
    //if (strstr(fmt, "{") == NULL)
    //{
    return win_ti_form_vprintf(title, fmt, ap);
    //}
#endif
#endif

    if ((win_dialog = (DIALOG *)calloc(64, sizeof(DIALOG))) == NULL)
    {
        return 1;
    }

    wth = 3 * menu_x0 / 8;
    hg = max_y / 3;
    Lw = Lift_W;
    //l = Lift_W/2;
    ttf_enable = 1;
    pc_fen = text_scale;

    if (win_printf_message == NULL)
    {
        if ((win_printf_message = (char *)calloc(8192, sizeof(char))) == NULL)
        {
            return 1;
        }
    }

    c = win_printf_message;
    vsnprintf(c, 8192, fmt, ap);
    ba = (struct box *)calloc(1, sizeof(struct box));

    if (ba == NULL)
    {
        return 1;
    }

    ba->color = White;

    for (p = c, s = c, hg = 5 * Lw / 2; *p ; p++)
    {
        switch (*p)
        {
        case '\n':
            *p = 0;
            bo = add_text_to_box(ba , Lw * col, hg + Lw / 2, s);
            i = (bo->h + bo->d) / screen_to_box;
            hl = (i > hl) ? i : hl;
            hg += Lw / 2 + hl;
            hl = 0;
            i = Lw * col + (bo->w) / screen_to_box + Lw;
            wth = (i > wth) ? i : wth;
            col = 1;
            n_char = 0;
            s = p + 1;
            break;

        case '\t':
            *p = 0;
            s = (*s == 0) ? s + 1 : s;

            if (n_char)
            {
                bo = add_text_to_box(ba , Lw * col, hg + Lw / 2, s);
                n_char = 0;
                i = (bo->h + bo->d) / screen_to_box;
                hl = (i > hl) ? i : hl;
                i = Lw * col + (bo->w) / screen_to_box + Lw;
                wth = (i > wth) ? i : wth;
                s = p + 1;
            }

            col++;
            break;

        default :        /* hoops    */
            n_char = (n_char == 0) ? 1 : n_char;
            break;
        };
    }

    if (n_char)
    {
        bo = add_text_to_box(ba , Lw * col, hg + Lw / 2, s);
        i = (bo->h + bo->d) / screen_to_box;
        hl = (i > hl) ? i : hl;
        hg += Lw / 2 + hl;
        i = Lw * col + (bo->w) / screen_to_box + Lw;
        wth = (i > wth) ? i : wth;
    }

    hg += 3 * Lw;
    h = (hg > SCREEN_H) ? SCREEN_H : hg;
    w = (wth > SCREEN_W) ? SCREEN_W : wth;
    x0 = x = (SCREEN_W - w) / 2;
    y0 = y = (SCREEN_H - h) / 2;
    set_box_origin(ba, x0, SCREEN_H - y0, text_scale);
    /*
          if ( hg >  ac->sc.max_y - Lw)
          {
          wth += Lw;
          hg = ac->sc.max_y - Lw;
          li = build_vert_lift(RIGHT_SIDE-Lw, 2*l, BOTTOM_LINE -4*l, 0);
          li->frame.action = move_text;
          hook_it_up(ac, IS_LIFT, (void *)li, TEXT_LIFT);
          }
          */
    bg = makecol(200, 200, 200);
    fg = makecol(32, 32, 32);
    win_dialog[n_dia++] = (DIALOG)
    {
        d_myclear_proc, 0, 0, w, h, fg, bg, 0, 0, 0, 0, NULL, NULL, NULL
    };
    win_dialog[n_dia++] = (DIALOG)
    {
        d_title_proc, 0, 0, w, 2 * Lw, fg, bg, 0, 0, 0, 0, title, NULL, NULL
    };
    h -= (11 * Lw) / 2;
    win_dialog[n_dia++] = (DIALOG)
    {
        d_picoTeX_proc, 0, 0, w, h, fg, bg, 0, D_DISABLED, 0, 0, c, ba, NULL
    };
    y = h + (3 * Lw);
    x = w / 4 - 50;
    win_dialog[n_dia++] = (DIALOG)
    {
        d_yield_proc, 0, 0, 0, 0, 255, 0, 't', 0, 0, 0, NULL, NULL, NULL
    };
    win_dialog[n_dia++] = (DIALOG)
    {
        d_button_proc, x, y, 100, 2 * Lw, fg, bg, '\r', D_EXIT, 0, 0, (void *)"Yes We Can", NULL, NULL
    };
    x = 3 * w / 4 - 50;
    win_dialog[n_dia++] = (DIALOG)
    {
        d_button_proc, x, y, 100, 2 * Lw, fg, bg, 27, D_EXIT, 0, 0, (void *) "Cancel", NULL, NULL
    };
    //win_dialog[n_dia++] = (DIALOG){general_idle,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL};
    win_dialog[n_dia++] = (DIALOG)
    {
        NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL
    };
    position_dialog(win_dialog, x0, y0);
    clear_keybuf();

    do
    {
    }
    while (gui_mouse_b());

    for (i = -1; i == -1;)
    {
        //does not work with create_video_bitmap
        bmp = create_bitmap(SCREEN_W, SCREEN_H);

        if (bmp == NULL)
        {
            bmp = create_bitmap(SCREEN_W, SCREEN_H);
        }

        bmps = create_bitmap(win_dialog->w, win_dialog->h);

        if (bmps == NULL)
        {
            bmps = create_bitmap(win_dialog->w, win_dialog->h);
        }

        if (bmp == NULL || bmps == NULL)
        {
            return win_printf("out of mem");
        }

        //if(!screen_used)
        //{
        //  screen_used = 1
#ifdef XV_WIN32
        scare_mouse();
#endif
	for(;screen_acquired;); // we wait for screen_acquired back to 0;
	screen_acquired = 1;
        acquire_screen(); // 2013-10-06 vc;
        set_clip_rect(screen, 0, 0, SCREEN_W - 1, SCREEN_H - 1);
        blit(screen, bmp, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
        release_screen(); // 2013-10-06 vc
	screen_acquired = 0;
#ifdef XV_WIN32
        unscare_mouse();
#endif
        //  screen_used = 0;
        //  }
        player = init_dialog(win_dialog, 2);

        for (finish = 0 ; finish == 0;)
        {
            while (update_dialog(player));

            if (player->obj == 1)
            {
                m_x = player->mouse_ox;
                m_y = player->mouse_oy;

                while (mouse_b)
                {
                    if ((m_x != mouse_x) || (m_y != mouse_y))
                    {
                        vsync();
                        //if (!screen_used)
                        //{
                        //  screen_used = 1;
                        //
#ifdef XV_WIN32
                        scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
			for(;screen_acquired;); // we wait for screen_acquired back to 0;
			screen_acquired = 1;
                        acquire_screen(); // 2013-10-06 vc
                        blit(screen, bmps, win_dialog->x, win_dialog->y, 0, 0, win_dialog->w, win_dialog->h);
                        blit(bmp, screen, win_dialog->x, win_dialog->y, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
                        release_screen(); // 2013-10-06 vc
			screen_acquired = 0;

#ifdef XV_WIN32
                        unscare_mouse();
#endif
                        x0 += mouse_x - m_x;
                        y0 += mouse_y - m_y;
                        position_dialog(win_dialog, x0, y0);
                        m_x = mouse_x;
                        m_y = mouse_y;

#ifdef XV_WIN32
                        scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
			for(;screen_acquired;); // we wait for screen_acquired back to 0;
			screen_acquired = 1;
                        acquire_screen(); // 2013-10-06 vc
                        blit(bmps, screen, 0, 0, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
                        release_screen(); // 2013-10-06 vc
			screen_acquired = 0;
#ifdef XV_WIN32
                        unscare_mouse();
#endif
                        //  screen_used = 0;
                        //}
                    }
                }
            }
            else
            {
                finish = 1;
                i = player->obj;
            }
        }
    }

    //if(!screen_used)
    // {
    //screen_used = 1;

#ifdef XV_WIN32
    scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
    for(;screen_acquired;); // we wait for screen_acquired back to 0;
    screen_acquired = 1;
    acquire_screen(); // 2013-10-06 vc
    blit(bmp, screen, win_dialog->x, win_dialog->y, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
    release_screen(); // 2013-10-06 vc
    screen_acquired = 0;
#ifdef XV_WIN32
    unscare_mouse();
#endif
    //screen_used = 1;
    //}
    destroy_bitmap(bmp);
    destroy_bitmap(bmps);
    shutdown_dialog(player);

    //position_dialog(win_dialog,x0,y0);
    //i = popup_dialog(win_dialog,3);
    if (win_dialog)
    {
        free(win_dialog);
    }

    free_box(ba);

    if (ba)
    {
        free(ba);
    }

    ttf_enable = tf;
    return (i == 4) ? WIN_OK : WIN_CANCEL;
}

void *win_printf_ptr(const char *fmt, ...)
{
    //int i;
    va_list ap;
    setlocale(LC_ALL, "C");
    va_start(ap, fmt);
    win_ti_vprintf("Message", fmt, ap);
    va_end(ap);
    return NULL;
}


int win_simple_printf_OK(const char *fmt, ...)
{
    //int i;
    va_list ap;
    va_start(ap, fmt);
    win_ti_vprintf("Message", fmt, ap);
    va_end(ap);
    return D_O_K;
}

int win_simple_printf(const char *fmt, ...)
{
    int i;
    va_list ap;
    va_start(ap, fmt);
    i = win_ti_simple_vprintf("Message", fmt, ap);
    va_end(ap);
    return i;
}
void *win_simple_printf_ptr(const char *fmt, ...)
{
    //int i;
    va_list ap;
    setlocale(LC_ALL, "C");
    va_start(ap, fmt);
    win_ti_simple_vprintf("Message", fmt, ap);
    va_end(ap);
    return NULL;
}
int win_ti_simple_printf(const char *title, const char *fmt, ...)
{
    int i;
    va_list ap;
    va_start(ap, fmt);
    i = win_ti_simple_vprintf(title, fmt, ap);
    va_end(ap);
    return i;
}

int win_ti_simple_vprintf(const char *title, const char *fmt, va_list ap)
{
    int i;
    char *p = NULL, *c = NULL, *s = NULL;
    DIALOG *win_dialog = NULL;
    int  n_dia = 0;
    struct box *bo = NULL, *ba = NULL;
    int wth, hg, Lw, bg, fg, x0 = 150, y0 = 350; //l
    int col = 1, n_char = 0, hl = 0, x, y, w, h;
    int tf = ttf_enable;
    DIALOG_PLAYER *player;
    BITMAP *bmp = NULL, *bmps = NULL;
    int finish, m_x, m_y;
    setlocale(LC_ALL, "C");

    if ((win_dialog = (DIALOG *)calloc(64, sizeof(DIALOG))) == NULL)
    {
        return 1;
    }

    wth = 3 * menu_x0 / 8;
    hg = max_y / 3;
    Lw = Lift_W;
    //l = Lift_W/2;
    ttf_enable = 1;
    pc_fen = text_scale;

    if (win_printf_message == NULL)
    {
        if ((win_printf_message = (char *)calloc(8192, sizeof(char))) == NULL)
        {
            return 1;
        }
    }

    c = win_printf_message;
    vsnprintf(c, 8192, fmt, ap);
    ba = (struct box *)calloc(1, sizeof(struct box));

    if (ba == NULL)
    {
        return 1;
    }

    ba->color = White;

    for (p = c, s = c, hg = 5 * Lw / 2; *p ; p++)
    {
        switch (*p)
        {
        case '\n':
            *p = 0;
            bo = add_simple_text_to_box(ba , Lw * col, hg + Lw / 2, s);
            i = (bo->h + bo->d) / screen_to_box;
            hl = (i > hl) ? i : hl;
            hg += Lw / 2 + hl;
            hl = 0;
            i = Lw * col + (bo->w) / screen_to_box + Lw;
            wth = (i > wth) ? i : wth;
            col = 1;
            n_char = 0;
            s = p + 1;
            break;

        case '\t':
            *p = 0;
            s = (*s == 0) ? s + 1 : s;

            if (n_char)
            {
                bo = add_simple_text_to_box(ba , Lw * col, hg + Lw / 2, s);
                n_char = 0;
                i = (bo->h + bo->d) / screen_to_box;
                hl = (i > hl) ? i : hl;
                i = Lw * col + (bo->w) / screen_to_box + Lw;
                wth = (i > wth) ? i : wth;
                s = p + 1;
            }

            col++;
            break;

        default :        /* hoops    */
            n_char = (n_char == 0) ? 1 : n_char;
            break;
        };
    }

    if (n_char)
    {
        bo = add_simple_text_to_box(ba , Lw * col, hg + Lw / 2, s);
        i = (bo->h + bo->d) / screen_to_box;
        hl = (i > hl) ? i : hl;
        hg += Lw / 2 + hl;
        i = Lw * col + (bo->w) / screen_to_box + Lw;
        wth = (i > wth) ? i : wth;
    }

    hg += 3 * Lw;
    h = (hg > SCREEN_H) ? SCREEN_H : hg;
    w = (wth > SCREEN_W) ? SCREEN_W : wth;
    x0 = x = (SCREEN_W - w) / 2;
    y0 = y = (SCREEN_H - h) / 2;
    set_box_origin(ba, x0, SCREEN_H - y0, text_scale);
    /*
          if ( hg >  ac->sc.max_y - Lw)
          {
          wth += Lw;
          hg = ac->sc.max_y - Lw;
          li = build_vert_lift(RIGHT_SIDE-Lw, 2*l, BOTTOM_LINE -4*l, 0);
          li->frame.action = move_text;
          hook_it_up(ac, IS_LIFT, (void *)li, TEXT_LIFT);
          }
          */
    bg = makecol(200, 200, 200);
    fg = makecol(32, 32, 32);
    win_dialog[n_dia++] = (DIALOG)
    {
        d_myclear_proc, 0, 0, w, h, fg, bg, 0, 0, 0, 0, NULL, NULL, NULL
    };
    win_dialog[n_dia++] = (DIALOG)
    {
        d_title_proc, 0, 0, w, 2 * Lw, fg, bg, 0, 0, 0, 0, title, NULL, NULL
    };
    h -= (11 * Lw) / 2;
    win_dialog[n_dia++] = (DIALOG)
    {
        d_picoTeX_proc, 0, 0, w, h, fg, bg, 0, D_DISABLED, 0, 0, c, ba, NULL
    };
    y = h + (3 * Lw);
    x = w / 4 - 50;
    win_dialog[n_dia++] = (DIALOG)
    {
        d_yield_proc, 0, 0, 0, 0, 255, 0, 't', 0, 0, 0, NULL, NULL, NULL
    };
    win_dialog[n_dia++] = (DIALOG)
    {
        d_button_proc, x, y, 100, 2 * Lw, fg, bg, '\r', D_EXIT, 0, 0, (void *)"Yes We Can", NULL, NULL
    };
    x = 3 * w / 4 - 50;
    win_dialog[n_dia++] = (DIALOG)
    {
        d_button_proc, x, y, 100, 2 * Lw, fg, bg, 27, D_EXIT, 0, 0, (void *) "Cancel", NULL, NULL
    };
    //win_dialog[n_dia++] = (DIALOG){general_idle,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL};
    win_dialog[n_dia++] = (DIALOG)
    {
        NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL
    };
    position_dialog(win_dialog, x0, y0);
    clear_keybuf();

    do
    {
    }
    while (gui_mouse_b());

    for (i = -1; i == -1;)
    {
        //does not work with create_video_bitmap
        bmp = create_bitmap(SCREEN_W, SCREEN_H);

        if (bmp == NULL)
        {
            bmp = create_bitmap(SCREEN_W, SCREEN_H);
        }

        bmps = create_bitmap(win_dialog->w, win_dialog->h);

        if (bmps == NULL)
        {
            bmps = create_bitmap(win_dialog->w, win_dialog->h);
        }

        if (bmp == NULL || bmps == NULL)
        {
            return win_printf("out of mem");
        }

        //if(!screen_used)
        //{
        //  screen_used = 1
        //
# ifdef XV_WIN32
        scare_mouse();
#endif
	for(;screen_acquired;); // we wait for screen_acquired back to 0;
	screen_acquired = 1;
        acquire_screen(); // 2013-10-06 vc;
        set_clip_rect(screen, 0, 0, SCREEN_W - 1, SCREEN_H - 1);
        blit(screen, bmp, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
        release_screen(); // 2013-10-06 vc
	screen_acquired = 0;
# ifdef XV_WIN32
        unscare_mouse();
#endif
        //  screen_used = 0;
        //  }
        player = init_dialog(win_dialog, 2);

        for (finish = 0 ; finish == 0;)
        {
            while (update_dialog(player));

            if (player->obj == 1)
            {
                m_x = player->mouse_ox;
                m_y = player->mouse_oy;

                while (mouse_b)
                {
                    if ((m_x != mouse_x) || (m_y != mouse_y))
                    {
                        vsync();
                        //if (!screen_used)
                        //{
                        //  screen_used = 1;
# ifdef XV_WIN32
                        scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
			for(;screen_acquired;); // we wait for screen_acquired back to 0;
			screen_acquired = 1;

                        acquire_screen(); // 2013-10-06 vc
                        blit(screen, bmps, win_dialog->x, win_dialog->y, 0, 0, win_dialog->w, win_dialog->h);
                        blit(bmp, screen, win_dialog->x, win_dialog->y, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
                        release_screen(); // 2013-10-06 vc
			screen_acquired = 0;


# ifdef XV_WIN32
                        unscare_mouse();
#endif
                        x0 += mouse_x - m_x;
                        y0 += mouse_y - m_y;
                        position_dialog(win_dialog, x0, y0);
                        m_x = mouse_x;
                        m_y = mouse_y;
# ifdef XV_WIN32
                        scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
			for(;screen_acquired;); // we wait for screen_acquired back to 0;
			screen_acquired = 1;
                        acquire_screen(); // 2013-10-06 vc
                        blit(bmps, screen, 0, 0, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
                        release_screen(); // 2013-10-06 vc
			screen_acquired = 0;
# ifdef XV_WIN32
                        unscare_mouse();
#endif
                        //  screen_used = 0;
                        //}
                    }
                }
            }
            else
            {
                finish = 1;
                i = player->obj;
            }
        }
    }

    //if(!screen_used)
    // {
    //screen_used = 1;
# ifdef XV_WIN32
    scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
    for(;screen_acquired;); // we wait for screen_acquired back to 0;
    screen_acquired = 1;
    acquire_screen(); // 2013-10-06 vc
    blit(bmp, screen, win_dialog->x, win_dialog->y, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
    release_screen(); // 2013-10-06 vc
    screen_acquired = 0;
# ifdef XV_WIN32
    unscare_mouse();
#endif
    //screen_used = 1;
    //}
    destroy_bitmap(bmp);
    destroy_bitmap(bmps);
    shutdown_dialog(player);

    //position_dialog(win_dialog,x0,y0);
    //i = popup_dialog(win_dialog,3);
    if (win_dialog)
    {
        free(win_dialog);
    }

    free_box(ba);

    if (ba)
    {
        free(ba);
    }

    ttf_enable = tf;
    return (i == 4) ? WIN_OK : WIN_CANCEL;
}



static int no_ac(void)
{
    return D_O_K;
}

# ifdef DEBUG
static int check_find_mouse_object(DIALOG *d, int x, int y)
{
    int mouse_object = -1;
    int c;
    ASSERT(d);

    for (c = 0; d[c].proc; c++)
        if ((x >= d[c].x) && (y >= d[c].y) &&
                (x < d[c].x + d[c].w) && (y < d[c].y + d[c].h) &&
                (!(d[c].flags & (D_HIDDEN | D_DISABLED))))
        {
            mouse_object = c;
        }

    return mouse_object;
}
#endif

int win_scanf(const char *fmt, ...)
{
    int ret = 0;
    va_list ap;
    va_start(ap, fmt);
    ret = win_vscanf(fmt, ap);
    va_end(ap);
    return ret;
}
#ifdef XV_GTK_DIALOG
//TODO : add field size, implement last functionalities
static int win_form_vscanf(const char *fmt, va_list ap)
{
    const char *p = NULL;
    char str[1024] = {0};
    char *pango_str = NULL;
    int i = 0;
    int n_format = 0;
    char *bp = NULL;
    int *radio_ptr = NULL;
    char *serial_b_ptr = NULL;
    int serial_b_count = 0;
    MENU *menu = NULL;
    char *title = NULL;

    setlocale(LC_ALL, "C");
    form_window_t *form = NULL;

    form = form_create("Enter Information");


    for (p = fmt; *p ; p++)
    {
        if (*p == '\n')
        {
            str[i] = 0;
            pango_str = tex_to_pango_markup(str);
            form_push_label(form, pango_str);
            free(pango_str);
            form_newline(form);
            i = 0;

        }
        else if (*p != '%')
        {
            str[i] = *p;
            i = (i < 1024) ? i + 1 : i;
            continue;
        }
        else
        {
            n_format = (int)strtol(p + 1, &bp, 10);

            if (n_format != 0 && bp != NULL)
            {
                p = bp - 1;
            }
            else
            {
                n_format = 0;
            }

            switch (*++p)
            {
            case 'd':
            {
                int *var_int = va_arg(ap, int *);
                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_int(form, var_int);
                i = 0;
                break;
            }

            case 's':
            {
                char *var_str = va_arg(ap, char *);
                str[i] = 0;

                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_text(form, var_str);

                i = 0;
                break;
            }

            case 'f':
            {
                float *var_float = va_arg(ap, float *);
                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_float(form, 0.000001, var_float); //TODO change precision
                i = 0;
                break;
            }

            case 'b':
            {
                int *var_bool = (int *) va_arg(ap, int *);
                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_int_as_bool(form, var_bool);
                i = 0;
                break;
            }

            case 'Q':
            {
                serial_b_ptr = va_arg(ap, char *);
                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_checkbox_char_as_bool(form, &(serial_b_ptr[serial_b_count]));
                serial_b_count++;
                i = 0;
                break;
            }

            case 'q':
            {
                if (!serial_b_ptr)
                {
                    serial_b_ptr = va_arg(ap, char *);
                }

                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_checkbox_char_as_bool(form, &(serial_b_ptr[serial_b_count]));
                serial_b_count++;
                i = 0;
                break;
            }

            case 'R':
            {
                radio_ptr = va_arg(ap, int *);
                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_option(form, "", radio_ptr);

                i = 0;
                break;
            }

            case 'r':
            {
                if (radio_ptr == NULL)
                {
                    radio_ptr = va_arg(ap, int *);
                }

                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_option(form, "", radio_ptr);

                i = 0;
                break;
            }

            case 'l':
            {
                if (p[1] == 'f')
                {
                    double *var_double = va_arg(ap, double *);
                    str[i] = 0;
                    pango_str = tex_to_pango_markup(str);
                    form_push_label(form, pango_str);
                    free(pango_str);
                    form_push_double(form, 0.000001, var_double); // TODO : change precision
                    i = 0;

                    p++;
                }
                else if (p[1] == 's')
                {
                    char *var_char = va_arg(ap, char *);
                    str[i] = 0;
                    pango_str = tex_to_pango_markup(str);
                    form_push_label(form, pango_str);
                    free(pango_str);
                    form_push_text(form, var_char);
                    i = 0;
                    p++;
                }

                break;
            }

            case 'm':
            {
                menu = va_arg(ap, MENU *); // TODO NOT IMPLEMENTED
                break;
            }

            case 't':
            {
                title = va_arg(ap, char *);
                form_set_title(form, title);
                break;
            }

            default :        /* hoops    */
            {
                str[i] = *p;
                i = (i < 255) ? i + 1 : i;
                break;
            }
            };
        }
    }

    if (i > 0)
    {
        str[i] = 0;
        pango_str = tex_to_pango_markup(str);
        form_push_label(form, pango_str);
        free(pango_str);
    }

    i = form_run(form);
    form_free(form);
    return i;

}


static int win_form_vscanf_in_list(const char *fmt, const int n_arg, void *arg_array)
{
    const char *p = NULL;
    char str[1024] = {0};
    char *pango_str = NULL;
    int i = 0;
    int i_arg = 0;
    int n_format = 0;
    char *bp = NULL;
    int *radio_ptr = NULL;
    char *serial_b_ptr = NULL;
    int serial_b_count = 0;
    MENU *menu = NULL;
    char *title = NULL;

    setlocale(LC_ALL, "C");
    form_window_t *form = NULL;

    form = form_create("Enter Information");


    for (p = fmt; *p ; p++)
    {

        if (*p == '\n')
        {
            str[i] = 0;
            pango_str = tex_to_pango_markup(str);
            form_push_label(form, pango_str);
            free(pango_str);
            form_newline(form);
            i = 0;

        }
        else if (*p != '%')
        {
            str[i] = *p;
            i = (i < 1024) ? i + 1 : i;
            continue;
        }
        else
        {
            n_format = (int)strtol(p + 1, &bp, 10);

            if (n_format != 0 && bp != NULL)
            {
                p = bp - 1;
            }
            else
            {
                n_format = 0;
            }

            if (i_arg == n_arg)
            {
              return(win_printf_OK("Error :  more arguments than pointers\n"));
            }

            switch (*++p)
            {
            case 'd':
            {
                int *var_int = (int *) arg_array + i_arg;
                i_arg++;
                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_int(form, var_int);
                i = 0;
                break;
            }

            case 's':
            {
                char *var_str = (char *) arg_array + i_arg;
                i_arg++;
                str[i] = 0;

                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_text(form, var_str);

                i = 0;
                break;
            }

            case 'f':
            {
                float *var_float = (float *) arg_array + i_arg;
                i_arg++;
                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_float(form, 0.000001, var_float); //TODO change precision
                i = 0;
                break;
            }

            case 'b':
            {
                int *var_bool = (int *) arg_array + i_arg;
                i_arg++;
                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_int_as_bool(form, var_bool);
                i = 0;
                break;
            }

            case 'Q':
            {
                serial_b_ptr = (char *) arg_array + i_arg;
                i_arg++;
                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_checkbox_char_as_bool(form, &(serial_b_ptr[serial_b_count]));
                serial_b_count++;
                i = 0;
                break;
            }

            case 'q':
            {
                if (!serial_b_ptr)
                {
                    serial_b_ptr = (char *) arg_array + i_arg;
                    i_arg++;
                }

                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_checkbox_char_as_bool(form, &(serial_b_ptr[serial_b_count]));
                serial_b_count++;
                i = 0;
                break;
            }

            case 'R':
            {
                radio_ptr = (int *) arg_array + i_arg;
                i_arg++;
                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_option(form, "", radio_ptr);

                i = 0;
                break;
            }

            case 'r':
            {
                if (radio_ptr == NULL)
                {
                    radio_ptr = (int *) arg_array + i_arg;
                    i_arg++;
                }

                str[i] = 0;
                pango_str = tex_to_pango_markup(str);
                form_push_label(form, pango_str);
                free(pango_str);
                form_push_option(form, "", radio_ptr);

                i = 0;
                break;
            }

            case 'l':
            {
                if (p[1] == 'f')
                {
                    double *var_double = (double *) arg_array + i_arg;
                    i_arg++;
                    str[i] = 0;
                    pango_str = tex_to_pango_markup(str);
                    form_push_label(form, pango_str);
                    free(pango_str);
                    form_push_double(form, 0.000001, var_double); // TODO : change precision
                    i = 0;

                    p++;
                }
                else if (p[1] == 's')
                {
                    char *var_char = (char *) arg_array + i_arg;
                    i_arg++;
                    str[i] = 0;
                    pango_str = tex_to_pango_markup(str);
                    form_push_label(form, pango_str);
                    free(pango_str);
                    form_push_text(form, var_char);
                    i = 0;
                    p++;
                }

                break;
            }

            case 'm':
            {
                menu = (MENU *) arg_array + i_arg;
                i_arg++; // TODO NOT IMPLEMENTED
                break;
            }

            case 't':
            {
                title = (char *) arg_array + i_arg;
                i_arg++;
                form_set_title(form, title);
                break;
            }

            default :        /* hoops    */
            {
                str[i] = *p;
                i = (i < 255) ? i + 1 : i;
                break;
            }
            };
        }
    }

    if (i > 0)
    {
        str[i] = 0;
        pango_str = tex_to_pango_markup(str);
        form_push_label(form, pango_str);
        free(pango_str);
    }

    i = form_run(form);
    form_free(form);
    return i;

}
#endif
/*    int win_scanf(char *fmt, ...)
 *    DESCRIPTION    Get input from a window in a way similar to scanf.
 *            The format "fmt" may contain messages separated by
 *            %s, %d, %f; a line is print with the message, an
 *            input-line follows providing input for this parameter.
 *            The address of the parameter must be supply, the
 *            initial value is displayed. A final %m in the format
 *             allow to attach a menu to the window. %t customizes
 *            the title of the alert window. The size of the
 *            window adapts to the number and size of the messages.
 *
 *    EXEMPLE        win_scanf("Enter your name %s and your age %d%m",name,
 *                &age,quidam_menu);
 *
 *    RETURNS        OK on success, WIN_CANCEL otherwise
 *
 */
int win_vscanf(const char *fmt, va_list ap)
{
    int i, j, k;
    const char *p = NULL;
    char c[256] = {0}, *title = NULL;
    struct box *bo = NULL, *ba = NULL;
    MENU *menu = NULL;
    int wth, hg, Lw, x0 = 50, y0 = 50, nw, txt_start, n_format = 0; //l
    void *vp = NULL, *radio_ptr = NULL, *serial_b_ptr = NULL;
    DIALOG *win_dialog = NULL;
    int n_dia = 0, x, y, w, h, n_grp = -1, n_radio = -1, n_serial_b = -1;
    char *buf[256] = {0};
    int tf = ttf_enable;
    int dia_in_0, dia_in_n, fg, bg, cf, cb;
    char *bp = NULL;
# ifdef DEBUG
    char debug[256] = {0};
# endif
    DIALOG_PLAYER *player = NULL;
    BITMAP *bmp = NULL, *bmps = NULL;
    int finish, m_x, m_y;
    setlocale(LC_ALL, "C");

#ifdef XV_GTK_DIALOG //_SCAN
#ifndef XV_NO_GTK_WIN_PRINTF
    //if (strstr(fmt, "{") == NULL)
    //{
    return win_form_vscanf(fmt, ap);
    //}

#endif
#endif

    if ((win_dialog = (DIALOG *)calloc(256, sizeof(DIALOG))) == NULL)
    {
        return 1;
    }

    wth = 3 * menu_x0 / 8;
    hg = max_y / 3;
    Lw = Lift_W;
    //l = Lift_W/2;
    switch_allegro_font(2);
    ttf_enable = 1;
    pc_fen = text_scale;
    ba = (struct box *)calloc(1, sizeof(struct box));

    if (ba == NULL)
    {
        return 1;
    }

    ba->color = White;
    bg = makecol(200, 200, 200);
    fg = makecol(32, 32, 32);
    cf = makecol(0, 0, 0);
    cb = makecol(255, 255, 255);
    win_dialog[n_dia++] = (DIALOG)
    {
        d_myclear_proc, 0, 0, 200, 200, fg, bg, 0, 0, 0, 0, NULL, NULL, NULL
    };
    win_dialog[n_dia++] = (DIALOG)
    {
        d_title_proc, 0, 0, 200, 2 * Lw, fg, bg, 0, D_EXIT, 0, 0, title, NULL, NULL
    };
    //win_dialog[n_dia++] = (DIALOG){general_idle,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL};
    dia_in_0 = n_dia;
    txt_start = Lw;

    for (p = fmt, c[0] = 0, j = 0, hg = 2 * Lw, i = 0 ; *p ; p++)
    {
        if (*p == '\n')
        {
            c[i] = 0;
            bo = add_text_to_box(ba , txt_start, hg + Lw, c); /*add_text_to_box(ba ,Lw, hg + Lw, c);*/
            nw = txt_start + 2 * Lw + bo->w / screen_to_box;
            wth = (nw > wth) ? nw : wth;
            i = 0;
            hg += 3 * Lw / 2;
            txt_start = Lw;
        }
        else    if (*p != '%')
        {
            c[i] = *p;
            i = (i < 255) ? i + 1 : i;
            continue;
        }
        else
        {
            n_format = (int)strtol(p + 1, &bp, 10);

            if (n_format != 0 && bp != NULL)
            {
                p = bp - 1;
            }
            else
            {
                n_format = 0;
            }

            switch (*++p)
            {
            case 'd':
                vp = va_arg(ap, int *);
                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                txt_start = (n_format) ? txt_start + nw - 2 * Lw : Lw;
                nw +=  n_format * Lw + 6;
                wth = (nw > wth) ? nw : wth;
                buf[n_dia] = (char *)calloc(32, sizeof(char));
                snprintf(buf[n_dia], 32, "%d", *((int *)vp));

                if (n_format == 0)
                {
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, 3 * Lw, hg + 2 * Lw + 6, 4000, 30, cf, cb,
                                     0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"int"
                    };
                    txt_start = Lw;
                    hg += 4 * Lw;
                }
                else
                {
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, txt_start, hg + 6, n_format *Lw, 20,
                                     cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *) "int"
                    };
                    txt_start += n_format * Lw + 6;
                }

                n_dia++;
                i = 0;
                break;

            case 's':
                vp = va_arg(ap, char *);
                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                txt_start = (n_format) ? txt_start + nw - 2 * Lw : Lw;
                nw +=  n_format * Lw + 6;
                wth = (nw > wth) ? nw : wth;
                buf[n_dia] = (char *)calloc(256, sizeof(char));
                snprintf(buf[n_dia], 256, "%s", (char *)vp);

                if (n_format == 0)
                {
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, 3 * Lw, hg + 2 * Lw + 6, 4000, 30, cf, cb,
                                     0, 0, 32, 0, buf[n_dia], (void *)vp, (void *) "word"
                    };
                    txt_start = Lw;
                    hg += 4 * Lw;
                }
                else
                {
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, txt_start, hg + 6, n_format *Lw, 20,
                                     cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *) "word"
                    };
                    txt_start += n_format * Lw + 6;
                }

                n_dia++;
                i = 0;
                break;

            case 'f':
                vp = va_arg(ap, float *);
                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                txt_start = (n_format) ? txt_start + nw - 2 * Lw : Lw;
                nw +=  n_format * Lw + 6;
                wth = (nw > wth) ? nw : wth;
                buf[n_dia] = (char *)calloc(32, sizeof(char));
                snprintf(buf[n_dia], 32, "%g", *((float *)vp));

                if (n_format == 0)
                {
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, 3 * Lw, hg + 2 * Lw + 6, 4000, 30, cf, cb,
                                     0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"float"
                    };
                    txt_start = Lw;
                    hg += 4 * Lw;
                }
                else
                {
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, txt_start, hg + 6, n_format *Lw, 20,
                                     cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"float"
                    };
                    txt_start += n_format * Lw + 6;
                }

                n_dia++;
                i = 0;
                break;

            case 'b':
                vp = va_arg(ap, int *);
                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                txt_start = txt_start + nw - 2 * Lw;
                nw +=  n_format * Lw + 6;
                wth = (nw > wth) ? nw : wth;
                buf[n_dia] = (char *)calloc(32, sizeof(char));
                buf[n_dia][0] = 0;
                win_dialog[n_dia] = (DIALOG)
                {
                    d_check_proc, txt_start, hg + 10, Lw, Lw, cf, cb,
                                  0, ((*(int *)vp) != 0) ? D_SELECTED : 0, 32, 0, buf[n_dia], (void *)vp, (void *)"bool"
                };
                txt_start += Lw + 6;
                n_dia++;
                i = 0;
                break;

            case 'Q':
                serial_b_ptr = vp = va_arg(ap, char *);
                n_serial_b = 0;
                n_grp++;
                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                wth = (nw > wth) ? nw : wth;
                txt_start = txt_start + nw - 2 * Lw;

                if (serial_b_ptr != NULL)
                {
                    buf[n_dia] = (char *)calloc(256, sizeof(char));
                    buf[n_dia][0] = 0;
                    //buf[n_dia][1] = 'but';
                    //strcpy(buf[n_dia],"but");
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_check_proc, txt_start, hg + 10, Lw, Lw, cf, cb,
                                      0, (((char *)vp)[n_serial_b] == 1) ? D_SELECTED : 0,
                                      n_grp, 0, buf[n_dia], (char *) vp + n_serial_b, (void *)"serial_b"
                    };
                    txt_start += Lw + 6;
                    n_dia++;
                    n_serial_b++;
                }

                i = 0;
                break;

            case 'q':
                if (n_grp < 0 || serial_b_ptr == NULL)
                {
                    serial_b_ptr = vp = va_arg(ap, char *);
                    n_serial_b = 0;
                    n_grp++;
                }

                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                wth = (nw > wth) ? nw : wth;
                txt_start = txt_start + nw - 2 * Lw;

                if (serial_b_ptr != NULL)
                {
                    buf[n_dia] = (char *)calloc(32, sizeof(char));
                    buf[n_dia][0] = 0;
                    //buf[n_dia][1] = 'but';
                    //strcpy(buf[n_dia],"but");
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_check_proc, txt_start, hg + 10, Lw, Lw, cf, cb,
                                      0, (((char *)vp)[n_serial_b] == 1) ? D_SELECTED : 0,
                                      n_grp, 0, buf[n_dia], (char *) vp + n_serial_b, (void *)"serial_b"
                    };
                    txt_start += Lw + 6;
                    n_dia++;
                    n_serial_b++;
                }

                i = 0;
                break;

            case 'R':
                radio_ptr = vp = va_arg(ap, int *);
                n_radio = 0;
                n_grp++;
                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                wth = (nw > wth) ? nw : wth;
                txt_start = txt_start + nw - 2 * Lw;

                if (radio_ptr != NULL)
                {
                    buf[n_dia] = (char *)calloc(32, sizeof(char));
                    buf[n_dia][0] = 0;
                    //buf[n_dia][1] = 'but';
                    //strcpy(buf[n_dia],"but");
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_radio_proc, txt_start, hg + 10, Lw, Lw, cf, cb,
                                      0, ((*(int *)vp) == n_radio) ? D_SELECTED : 0, n_grp, 0, buf[n_dia], (void *)vp, (void *)"radio"
                    };
                    txt_start += Lw + 6;
                    n_dia++;
                    n_radio++;
                }

                i = 0;
                break;

            case 'r':
                if (n_grp < 0 || radio_ptr == NULL)
                {
                    radio_ptr = vp = va_arg(ap, int *);
                    n_radio = 0;
                    n_grp++;
                }

                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                wth = (nw > wth) ? nw : wth;
                txt_start = txt_start + nw - 2 * Lw;

                if (radio_ptr != NULL)
                {
                    buf[n_dia] = (char *)calloc(32, sizeof(char));
                    buf[n_dia][0] = 0;
                    //buf[n_dia][1] = 'but';
                    //strcpy(buf[n_dia],"but");
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_radio_proc, txt_start, hg + 10, Lw, Lw, cf, cb,
                                      0, ((*(int *)vp) == n_radio) ? D_SELECTED : 0, n_grp, 0, buf[n_dia], (void *)vp, (void *)"radio"
                    };
                    txt_start += Lw + 6;
                    n_dia++;
                    n_radio++;
                }

                i = 0;
                break;

            case 'l':
                if (p[1] == 'f')
                {
                    vp = va_arg(ap, double *);
                    c[i] = 0;
                    bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                    nw = 2 * Lw + bo->w / screen_to_box;
                    txt_start = (n_format) ? txt_start + nw - 2 * Lw : Lw;
                    nw +=  n_format * Lw + 6;
                    wth = (nw > wth) ? nw : wth;
                    buf[n_dia] = (char *)calloc(32, sizeof(char));
                    snprintf(buf[n_dia], 32, "%g", *((double *)vp));

                    if (n_format == 0)
                    {
                        win_dialog[n_dia] = (DIALOG)
                        {
                            d_edit_proc, 3 * Lw, hg + 2 * Lw + 6,
                                         4000, 30, cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"double"
                        };
                        txt_start = Lw;
                        hg += 4 * Lw;
                    }
                    else
                    {
                        win_dialog[n_dia] = (DIALOG)
                        {
                            d_edit_proc, txt_start, hg + 6,
                                         n_format *Lw, 20, cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"double"
                        };
                        txt_start += n_format * Lw + 6;
                    }

                    n_dia++;
                    i = 0;
                    p++;
                }
                else if (p[1] == 's')
                {
                    vp = va_arg(ap, char *);
                    c[i] = 0;
                    bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                    nw = 2 * Lw + bo->w / screen_to_box;
                    txt_start = (n_format) ? txt_start + nw - 2 * Lw : Lw;
                    nw +=  n_format * Lw + 6;
                    wth = (nw > wth) ? nw : wth;
                    buf[n_dia] = (char *)calloc(1024, sizeof(char));
                    snprintf(buf[n_dia], 1024, "%s", (char *)vp);

                    if (n_format == 0)
                    {
                        win_dialog[n_dia] = (DIALOG)
                        {
                            d_edit_proc, 3 * Lw, hg + 2 * Lw + 6,
                                         4000, 30, cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"string"
                        };
                        txt_start = Lw;
                        hg += 4 * Lw;
                    }
                    else
                    {
                        win_dialog[n_dia] = (DIALOG)
                        {
                            d_edit_proc, txt_start, hg + 6,
                                         n_format *Lw, 20, cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"string"
                        };
                        txt_start += n_format * Lw + 6;
                    }

                    n_dia++;
                    i = 0;
                    p++;
                }

                break;

            case 'm':
                menu = va_arg(ap, MENU *);
                break;

            case 't':
                title = va_arg(ap, char *);
                break;

            default :        /* hoops    */
                c[i] = *p;
                i = (i < 255) ? i + 1 : i;
                break;
            };
        }
    }

    if (i != 0)
    {
        c[i] = 0;
        bo = add_text_to_box(ba , txt_start, hg + Lw, c);
        i = 0;
        hg += 2 * Lw;
        nw = 2 * Lw + bo->w / screen_to_box;
        wth = (nw > wth) ? nw : wth;
    }

    hg += 5 * Lw;

    if (title == NULL)
    {
        title = "Enter information";
    }

    h = (hg > SCREEN_H) ? SCREEN_H : hg;
    w = (wth > SCREEN_W) ? SCREEN_W : wth;
    x0 = x = (SCREEN_W - w) / 2;
    y0 = y = (SCREEN_H - h) / 2;
    set_box_origin(ba, x0, SCREEN_H - y0, text_scale);
    win_dialog[0].w = win_dialog[1].w = w;
    win_dialog[0].h = h;
    win_dialog[1].dp = title;
    dia_in_n = n_dia;

    for (i = dia_in_0; i < dia_in_n; i++)
        if (win_dialog[i].w >= w)
        {
            win_dialog[i].w = w - 100;
        }

    h -= (11 * Lw) / 2;
    win_dialog[n_dia++] = (DIALOG)
    {
        d_picoTeX_proc, 0, 0, w, h, fg, bg, 0, D_DISABLED, 0, 0, c, ba, NULL
    };

    if (menu != NULL)
        win_dialog[n_dia++] = (DIALOG)
    {
        xvin_d_menu_proc, 0, 2 * Lw, 0, 0, fg, bg, 0, 0, 0, 0, menu, NULL, NULL
    };

    //    win_dialog[n_dia++] = (DIALOG){d_keyboard_proc,     0,   0,    0,    0,   0,  0,    0,      0,  KEY_F1,   0,    NULL,                  NULL, NULL  };
    // win_dialog[n_dia++] = (DIALOG){ d_yield_proc,        0,   0,    0,    0,   0,  0,    0,      0,       0,   0,    NULL,                   NULL, NULL  };
    y = h + (3 * Lw);

    x = w / 4 - 50;

    win_dialog[n_dia++] = (DIALOG)
    {
        d_yield_proc, 0, 0, 0, 0, 255, 0, 't', 0, 0, 0, NULL, NULL, NULL
    };

    win_dialog[n_dia++] = (DIALOG)
    {
        d_keyboard_proc, 0, 0, 0, 0, 0, 0, 27, 0, 0, 0, (void *) no_ac, NULL, NULL
    }; // UNSAFE CAST

    win_dialog[n_dia++] = (DIALOG)
    {
        d_button_proc, x, y, 100, 2 * Lw, fg, bg, '\r', D_EXIT, 0, 0, (void *)"Yes We Can", NULL, NULL
    };

    x = 3 * w / 4 - 50;

    win_dialog[n_dia++] = (DIALOG)
    {
        d_button_proc, x, y, 100, 2 * Lw, fg, bg, 27, D_EXIT, 0, 0, (void *)"Cancel", NULL, NULL
    };

    win_dialog[n_dia] = (DIALOG)
    {
        NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL
    };

# ifdef DEBUG
    for (i = 0; i < n_dia ; i++)
    {
        snprintf(debug, 256, "%d found at %d flag %x x %d y %d w %d h %d", i,
                 check_find_mouse_object(win_dialog, win_dialog[i].x + 2, win_dialog[i].y + 2),
                 win_dialog[i].flags, win_dialog[i].x, win_dialog[i].y
                 , win_dialog[i].w, win_dialog[i].h);
        alert(debug, (win_dialog[i].proc == d_picoTeX_proc) ? "pico" : "other", "test", "Ok", "Cancel", 13, 27);
    }

    for (i = 0; i < n_dia && win_dialog[i].proc != d_radio_proc1; i++);

    if (win_dialog[i].proc == d_radio_proc1)
    {
        snprintf(debug, 256, "%d radio x %d y %d w %d h %d", i, win_dialog[i].x, win_dialog[i].y
                 , win_dialog[i].w, win_dialog[i].h);
        alert(debug, "test", "test", "Ok", "Cancel", 13, 27);
    }

# endif
    position_dialog(win_dialog, x0, y0);
    clear_keybuf();

    do
    {
    }
    while (gui_mouse_b());

    for (i = -1; i == -1;)
    {
        //        win_printf(" i equal %d",i);
        //does not work with create_video_bitmap
        bmp = create_bitmap(SCREEN_W, SCREEN_H);

        if (bmp == NULL)
        {
            bmp = create_bitmap(SCREEN_W, SCREEN_H);
        }

        bmps = create_bitmap(win_dialog->w, win_dialog->h);

        if (bmps == NULL)
        {
            bmps = create_bitmap(win_dialog->w, win_dialog->h);
        }

        if (bmp == NULL || bmps == NULL)
        {
            return win_printf("out of mem");
        }


# ifdef XV_WIN32
        scare_mouse();
#endif
	for(;screen_acquired;); // we wait for screen_acquired back to 0;
	screen_acquired = 1;
        acquire_screen(); // 2013-10-06 vc
        set_clip_rect(screen, 0, 0, SCREEN_W - 1, SCREEN_H - 1);
        blit(screen, bmp, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
        release_screen(); // 2013-10-06 vc
	screen_acquired = 0;
# ifdef XV_WIN32
        unscare_mouse();
#endif
        /*
           alert("Hello","a","b","Ok","Esc",13,27);
           scare_mouse();
           blit(bmp, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
           unscare_mouse();
           alert("after blit","a","b","Ok","Esc",13,27);
           */
        player = init_dialog(win_dialog, 2);

        for (finish = 0 ; finish == 0;)
        {
            while (update_dialog(player));

            if (player->obj == 1)
            {
                m_x = player->mouse_ox;
                m_y = player->mouse_oy;

                while (mouse_b)
                {
                    if ((m_x != mouse_x) || (m_y != mouse_y))
                    {
                        vsync();
# ifdef XV_WIN32
                        scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
			for(;screen_acquired;); // we wait for screen_acquired back to 0;
			screen_acquired = 1;

                        acquire_screen(); // 2013-10-06 vc
                        blit(screen, bmps, win_dialog->x, win_dialog->y, 0, 0, win_dialog->w, win_dialog->h);
                        blit(bmp, screen, win_dialog->x, win_dialog->y, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
                        release_screen(); // 2013-10-06 vc
			screen_acquired = 0;
# ifdef XV_WIN32
                        unscare_mouse();
#endif
                        x0 += mouse_x - m_x;
                        y0 += mouse_y - m_y;
                        position_dialog(win_dialog, x0, y0);
                        m_x = mouse_x;
                        m_y = mouse_y;
# ifdef XV_WIN32
                        scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
                        //            blit(screen, bmp, win_dialog->x, win_dialog->y, 0, 0, win_dialog->w, win_dialog->h);
			for(;screen_acquired;); // we wait for screen_acquired back to 0;
			screen_acquired = 1;

                        acquire_screen(); // 2013-10-06 vc
                        blit(bmps, screen, 0, 0, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
                        release_screen(); // 2013-10-06 vc
			screen_acquired = 0;
# ifdef XV_WIN32
                        unscare_mouse();
#endif
                        //broadcast_dialog_message(MSG_DRAW,0);
                    }
                }
            }
            else
            {
                finish = 1;
                i = player->obj;
            }
        }

# ifdef XV_WIN32
        scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
	for(;screen_acquired;); // we wait for screen_acquired back to 0;
	screen_acquired = 1;

        acquire_screen(); // 2013-10-06 vc
        blit(bmp, screen, win_dialog->x, win_dialog->y, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
        release_screen(); // 2013-10-06 vc
	screen_acquired = 0;

# ifdef XV_WIN32
        unscare_mouse();
#endif
        /*
           alert("bef blit","a","b","Ok","Esc",13,27);
           scare_mouse();
           blit(bmp, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
           unscare_mouse();
           alert("after blit","a","b","Ok","Esc",13,27);
           */
        destroy_bitmap(bmp);
        destroy_bitmap(bmps);
        shutdown_dialog(player);
        //  i = popup_dialog(win_dialog,2);
        i = (i == n_dia - 2) ? WIN_OK : WIN_CANCEL;
        n_grp = n_radio = -1;

        for (j = dia_in_0; j < dia_in_n; j++)
        {
            k = 0;

            if (strcmp("int", (const char *)win_dialog[j].dp3) == 0)
            {
                k = sscanf((const char *) win_dialog[j].dp, "%d", (int *)win_dialog[j].dp2);
            }
            else if (strcmp("word", (const char *)win_dialog[j].dp3) == 0)
            {
                k = sscanf((const char *) win_dialog[j].dp, "%s", (char *)win_dialog[j].dp2);
            }
            else if (strcmp("float", (const char *)win_dialog[j].dp3) == 0)
            {
                k = sscanf((const char *) win_dialog[j].dp, "%g", (float *)win_dialog[j].dp2);
            }
            else if (strcmp("bool", (const char *)win_dialog[j].dp3) == 0)
            {
                *(int *)win_dialog[j].dp2 = (win_dialog[j].flags & D_SELECTED) ? 1 : 0;
                k = 1;
            }
            else if (strcmp("radio", (const char *) win_dialog[j].dp3) == 0)
            {
                if (win_dialog[j].d1 == n_grp)
                {
                    n_radio++;
                }
                else
                {
                    n_grp = win_dialog[j].d1;
                    n_radio = 0;
                }

                if (win_dialog[j].flags & D_SELECTED)
                {
                    *(int *)win_dialog[j].dp2 = n_radio;
                }

                k = 1;
            }
            else if (strcmp("serial_b", (const char *)win_dialog[j].dp3) == 0)
            {
                *(char *)win_dialog[j].dp2 = (win_dialog[j].flags & D_SELECTED) ? 1 : 0;
                k = 1;
            }
            else if (strcmp("string", (const char *)win_dialog[j].dp3) == 0)
            {
                if (strlen((const char *)win_dialog[j].dp) == 0)
                {
                    k = 0;
                }
                else
                {
                    strcpy((char *)win_dialog[j].dp2, (char *) win_dialog[j].dp);
                    k = 1;
                }
            }
            else if (strcmp("double", (const char *)win_dialog[j].dp3) == 0)
            {
                k = sscanf((const char *)win_dialog[j].dp, "%lg", (double *)win_dialog[j].dp2);
            }

            if (k != 1)
            {
                snprintf((char *)win_dialog[j].dp, 32, "Invalid entry!");
                i = -1;
            }

            //             win_printf(" k equal %d\ni equal %d",k,i);
        }
    }

    for (j = dia_in_0; j < dia_in_n; j++)
        if (win_dialog[j].dp)
        {
            free(win_dialog[j].dp);
        }

    if (win_dialog)
    {
        free(win_dialog);
    }

    free_box(ba);

    if (ba)
    {
        free(ba);
    }

    ttf_enable = tf;
    switch_allegro_font(0);
    return i;
}


int win_scanf_in_list(const char *fmt, int n_arg, void *arg_array)
{
    int i, j, k;
    const char *p = NULL;
    char c[256] = {0}, *title = NULL;
    struct box *bo = NULL, *ba = NULL;
    MENU *menu = NULL;
    int wth, hg, Lw, x0 = 50, y0 = 50, nw, txt_start, n_format = 0; //l
    void *vp = NULL, *radio_ptr = NULL, *serial_b_ptr = NULL;
    DIALOG *win_dialog = NULL;
    int n_dia = 0, x, y, w, h, n_grp = -1, n_radio = -1, n_serial_b = -1;
    char *buf[256] = {0};
    int tf = ttf_enable;
    int dia_in_0, dia_in_n, fg, bg, cf, cb;
    char *bp = NULL;
    int i_arg = 0;
# ifdef DEBUG
    char debug[256] = {0};
# endif
    DIALOG_PLAYER *player = NULL;
    BITMAP *bmp = NULL, *bmps = NULL;
    int finish, m_x, m_y;
    setlocale(LC_ALL, "C");

#ifdef XV_GTK_DIALOG //_SCAN
#ifndef XV_NO_GTK_WIN_PRINTF
    //if (strstr(fmt, "{") == NULL)
    //{
    return win_form_vscanf_in_list(fmt, n_arg,arg_array);
    //}

#endif
#endif

    if ((win_dialog = (DIALOG *)calloc(256, sizeof(DIALOG))) == NULL)
    {
        return 1;
    }

    wth = 3 * menu_x0 / 8;
    hg = max_y / 3;
    Lw = Lift_W;
    //l = Lift_W/2;
    switch_allegro_font(2);
    ttf_enable = 1;
    pc_fen = text_scale;
    ba = (struct box *)calloc(1, sizeof(struct box));

    if (ba == NULL)
    {
        return 1;
    }

    ba->color = White;
    bg = makecol(200, 200, 200);
    fg = makecol(32, 32, 32);
    cf = makecol(0, 0, 0);
    cb = makecol(255, 255, 255);
    win_dialog[n_dia++] = (DIALOG)
    {
        d_myclear_proc, 0, 0, 200, 200, fg, bg, 0, 0, 0, 0, NULL, NULL, NULL
    };
    win_dialog[n_dia++] = (DIALOG)
    {
        d_title_proc, 0, 0, 200, 2 * Lw, fg, bg, 0, D_EXIT, 0, 0, title, NULL, NULL
    };
    //win_dialog[n_dia++] = (DIALOG){general_idle,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL};
    dia_in_0 = n_dia;
    txt_start = Lw;

    for (p = fmt, c[0] = 0, j = 0, hg = 2 * Lw, i = 0 ; *p ; p++)
    {
        if (*p == '\n')
        {
            c[i] = 0;
            bo = add_text_to_box(ba , txt_start, hg + Lw, c); /*add_text_to_box(ba ,Lw, hg + Lw, c);*/
            nw = txt_start + 2 * Lw + bo->w / screen_to_box;
            wth = (nw > wth) ? nw : wth;
            i = 0;
            hg += 3 * Lw / 2;
            txt_start = Lw;
        }
        else    if (*p != '%')
        {
            c[i] = *p;
            i = (i < 255) ? i + 1 : i;
            continue;
        }
        else
        {
            n_format = (int)strtol(p + 1, &bp, 10);

            if (n_format != 0 && bp != NULL)
            {
                p = bp - 1;
            }
            else
            {
                n_format = 0;
            }

            switch (*++p)
            {

            if (i_arg == n_arg) return("too many arguments in form\n");
            case 'd':
                vp = arg_array + i_arg;
                i_arg++;
                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                txt_start = (n_format) ? txt_start + nw - 2 * Lw : Lw;
                nw +=  n_format * Lw + 6;
                wth = (nw > wth) ? nw : wth;
                buf[n_dia] = (char *)calloc(32, sizeof(char));
                snprintf(buf[n_dia], 32, "%d", *((int *)vp));

                if (n_format == 0)
                {
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, 3 * Lw, hg + 2 * Lw + 6, 4000, 30, cf, cb,
                                     0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"int"
                    };
                    txt_start = Lw;
                    hg += 4 * Lw;
                }
                else
                {
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, txt_start, hg + 6, n_format *Lw, 20,
                                     cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *) "int"
                    };
                    txt_start += n_format * Lw + 6;
                }

                n_dia++;
                i = 0;
                break;

            case 's':
                vp = arg_array + i_arg;
                i_arg++;
                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                txt_start = (n_format) ? txt_start + nw - 2 * Lw : Lw;
                nw +=  n_format * Lw + 6;
                wth = (nw > wth) ? nw : wth;
                buf[n_dia] = (char *)calloc(256, sizeof(char));
                snprintf(buf[n_dia], 256, "%s", (char *)vp);

                if (n_format == 0)
                {
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, 3 * Lw, hg + 2 * Lw + 6, 4000, 30, cf, cb,
                                     0, 0, 32, 0, buf[n_dia], (void *)vp, (void *) "word"
                    };
                    txt_start = Lw;
                    hg += 4 * Lw;
                }
                else
                {
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, txt_start, hg + 6, n_format *Lw, 20,
                                     cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *) "word"
                    };
                    txt_start += n_format * Lw + 6;
                }

                n_dia++;
                i = 0;
                break;

            case 'f':
                vp = arg_array + i_arg;
                i_arg++;
                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                txt_start = (n_format) ? txt_start + nw - 2 * Lw : Lw;
                nw +=  n_format * Lw + 6;
                wth = (nw > wth) ? nw : wth;
                buf[n_dia] = (char *)calloc(32, sizeof(char));
                snprintf(buf[n_dia], 32, "%g", *((float *)vp));

                if (n_format == 0)
                {
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, 3 * Lw, hg + 2 * Lw + 6, 4000, 30, cf, cb,
                                     0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"float"
                    };
                    txt_start = Lw;
                    hg += 4 * Lw;
                }
                else
                {
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, txt_start, hg + 6, n_format *Lw, 20,
                                     cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"float"
                    };
                    txt_start += n_format * Lw + 6;
                }

                n_dia++;
                i = 0;
                break;

            case 'b':
                vp = arg_array + i_arg;
                i_arg++;
                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                txt_start = txt_start + nw - 2 * Lw;
                nw +=  n_format * Lw + 6;
                wth = (nw > wth) ? nw : wth;
                buf[n_dia] = (char *)calloc(32, sizeof(char));
                buf[n_dia][0] = 0;
                win_dialog[n_dia] = (DIALOG)
                {
                    d_check_proc, txt_start, hg + 10, Lw, Lw, cf, cb,
                                  0, ((*(int *)vp) != 0) ? D_SELECTED : 0, 32, 0, buf[n_dia], (void *)vp, (void *)"bool"
                };
                txt_start += Lw + 6;
                n_dia++;
                i = 0;
                break;

            case 'Q':
                serial_b_ptr = vp = arg_array + i_arg;
                i_arg++;
                n_serial_b = 0;
                n_grp++;
                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                wth = (nw > wth) ? nw : wth;
                txt_start = txt_start + nw - 2 * Lw;

                if (serial_b_ptr != NULL)
                {
                    buf[n_dia] = (char *)calloc(256, sizeof(char));
                    buf[n_dia][0] = 0;
                    //buf[n_dia][1] = 'but';
                    //strcpy(buf[n_dia],"but");
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_check_proc, txt_start, hg + 10, Lw, Lw, cf, cb,
                                      0, (((char *)vp)[n_serial_b] == 1) ? D_SELECTED : 0,
                                      n_grp, 0, buf[n_dia], (char *) vp + n_serial_b, (void *)"serial_b"
                    };
                    txt_start += Lw + 6;
                    n_dia++;
                    n_serial_b++;
                }

                i = 0;
                break;

            case 'q':
                if (n_grp < 0 || serial_b_ptr == NULL)
                {
                    serial_b_ptr = vp = arg_array + i_arg;
                    i_arg++;
                    n_serial_b = 0;
                    n_grp++;
                }

                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                wth = (nw > wth) ? nw : wth;
                txt_start = txt_start + nw - 2 * Lw;

                if (serial_b_ptr != NULL)
                {
                    buf[n_dia] = (char *)calloc(32, sizeof(char));
                    buf[n_dia][0] = 0;
                    //buf[n_dia][1] = 'but';
                    //strcpy(buf[n_dia],"but");
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_check_proc, txt_start, hg + 10, Lw, Lw, cf, cb,
                                      0, (((char *)vp)[n_serial_b] == 1) ? D_SELECTED : 0,
                                      n_grp, 0, buf[n_dia], (char *) vp + n_serial_b, (void *)"serial_b"
                    };
                    txt_start += Lw + 6;
                    n_dia++;
                    n_serial_b++;
                }

                i = 0;
                break;

            case 'R':
                radio_ptr = vp = arg_array + i_arg;
                i_arg++;
                n_radio = 0;
                n_grp++;
                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                wth = (nw > wth) ? nw : wth;
                txt_start = txt_start + nw - 2 * Lw;

                if (radio_ptr != NULL)
                {
                    buf[n_dia] = (char *)calloc(32, sizeof(char));
                    buf[n_dia][0] = 0;
                    //buf[n_dia][1] = 'but';
                    //strcpy(buf[n_dia],"but");
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_radio_proc, txt_start, hg + 10, Lw, Lw, cf, cb,
                                      0, ((*(int *)vp) == n_radio) ? D_SELECTED : 0, n_grp, 0, buf[n_dia], (void *)vp, (void *)"radio"
                    };
                    txt_start += Lw + 6;
                    n_dia++;
                    n_radio++;
                }

                i = 0;
                break;

            case 'r':
                if (n_grp < 0 || radio_ptr == NULL)
                {
                    radio_ptr = vp = arg_array + i_arg;
                    i_arg++;
                    n_radio = 0;
                    n_grp++;
                }

                c[i] = 0;
                bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                nw = 2 * Lw + bo->w / screen_to_box;
                wth = (nw > wth) ? nw : wth;
                txt_start = txt_start + nw - 2 * Lw;

                if (radio_ptr != NULL)
                {
                    buf[n_dia] = (char *)calloc(32, sizeof(char));
                    buf[n_dia][0] = 0;
                    //buf[n_dia][1] = 'but';
                    //strcpy(buf[n_dia],"but");
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_radio_proc, txt_start, hg + 10, Lw, Lw, cf, cb,
                                      0, ((*(int *)vp) == n_radio) ? D_SELECTED : 0, n_grp, 0, buf[n_dia], (void *)vp, (void *)"radio"
                    };
                    txt_start += Lw + 6;
                    n_dia++;
                    n_radio++;
                }

                i = 0;
                break;

            case 'l':
                if (p[1] == 'f')
                {
                    vp = arg_array + i_arg;
                    i_arg++;
                    c[i] = 0;
                    bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                    nw = 2 * Lw + bo->w / screen_to_box;
                    txt_start = (n_format) ? txt_start + nw - 2 * Lw : Lw;
                    nw +=  n_format * Lw + 6;
                    wth = (nw > wth) ? nw : wth;
                    buf[n_dia] = (char *)calloc(32, sizeof(char));
                    snprintf(buf[n_dia], 32, "%g", *((double *)vp));

                    if (n_format == 0)
                    {
                        win_dialog[n_dia] = (DIALOG)
                        {
                            d_edit_proc, 3 * Lw, hg + 2 * Lw + 6,
                                         4000, 30, cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"double"
                        };
                        txt_start = Lw;
                        hg += 4 * Lw;
                    }
                    else
                    {
                        win_dialog[n_dia] = (DIALOG)
                        {
                            d_edit_proc, txt_start, hg + 6,
                                         n_format *Lw, 20, cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"double"
                        };
                        txt_start += n_format * Lw + 6;
                    }

                    n_dia++;
                    i = 0;
                    p++;
                }
                else if (p[1] == 's')
                {
                  vp = arg_array + i_arg;
                  i_arg++;
                    c[i] = 0;
                    bo = add_text_to_box(ba , txt_start, hg + Lw, c);
                    nw = 2 * Lw + bo->w / screen_to_box;
                    txt_start = (n_format) ? txt_start + nw - 2 * Lw : Lw;
                    nw +=  n_format * Lw + 6;
                    wth = (nw > wth) ? nw : wth;
                    buf[n_dia] = (char *)calloc(1024, sizeof(char));
                    snprintf(buf[n_dia], 1024, "%s", (char *)vp);

                    if (n_format == 0)
                    {
                        win_dialog[n_dia] = (DIALOG)
                        {
                            d_edit_proc, 3 * Lw, hg + 2 * Lw + 6,
                                         4000, 30, cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"string"
                        };
                        txt_start = Lw;
                        hg += 4 * Lw;
                    }
                    else
                    {
                        win_dialog[n_dia] = (DIALOG)
                        {
                            d_edit_proc, txt_start, hg + 6,
                                         n_format *Lw, 20, cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, (void *)"string"
                        };
                        txt_start += n_format * Lw + 6;
                    }

                    n_dia++;
                    i = 0;
                    p++;
                }

                break;

            case 'm':
                menu = arg_array + i_arg;
                i_arg++;
                break;

            case 't':
                title = arg_array + i_arg;
                i_arg++;
                break;

            default :        /* hoops    */
                c[i] = *p;
                i = (i < 255) ? i + 1 : i;
                break;
            };
        }
    }

    if (i != 0)
    {
        c[i] = 0;
        bo = add_text_to_box(ba , txt_start, hg + Lw, c);
        i = 0;
        hg += 2 * Lw;
        nw = 2 * Lw + bo->w / screen_to_box;
        wth = (nw > wth) ? nw : wth;
    }

    hg += 5 * Lw;

    if (title == NULL)
    {
        title = "Enter information";
    }

    h = (hg > SCREEN_H) ? SCREEN_H : hg;
    w = (wth > SCREEN_W) ? SCREEN_W : wth;
    x0 = x = (SCREEN_W - w) / 2;
    y0 = y = (SCREEN_H - h) / 2;
    set_box_origin(ba, x0, SCREEN_H - y0, text_scale);
    win_dialog[0].w = win_dialog[1].w = w;
    win_dialog[0].h = h;
    win_dialog[1].dp = title;
    dia_in_n = n_dia;

    for (i = dia_in_0; i < dia_in_n; i++)
        if (win_dialog[i].w >= w)
        {
            win_dialog[i].w = w - 100;
        }

    h -= (11 * Lw) / 2;
    win_dialog[n_dia++] = (DIALOG)
    {
        d_picoTeX_proc, 0, 0, w, h, fg, bg, 0, D_DISABLED, 0, 0, c, ba, NULL
    };

    if (menu != NULL)
        win_dialog[n_dia++] = (DIALOG)
    {
        xvin_d_menu_proc, 0, 2 * Lw, 0, 0, fg, bg, 0, 0, 0, 0, menu, NULL, NULL
    };

    //    win_dialog[n_dia++] = (DIALOG){d_keyboard_proc,     0,   0,    0,    0,   0,  0,    0,      0,  KEY_F1,   0,    NULL,                  NULL, NULL  };
    // win_dialog[n_dia++] = (DIALOG){ d_yield_proc,        0,   0,    0,    0,   0,  0,    0,      0,       0,   0,    NULL,                   NULL, NULL  };
    y = h + (3 * Lw);

    x = w / 4 - 50;

    win_dialog[n_dia++] = (DIALOG)
    {
        d_yield_proc, 0, 0, 0, 0, 255, 0, 't', 0, 0, 0, NULL, NULL, NULL
    };

    win_dialog[n_dia++] = (DIALOG)
    {
        d_keyboard_proc, 0, 0, 0, 0, 0, 0, 27, 0, 0, 0, (void *) no_ac, NULL, NULL
    }; // UNSAFE CAST

    win_dialog[n_dia++] = (DIALOG)
    {
        d_button_proc, x, y, 100, 2 * Lw, fg, bg, '\r', D_EXIT, 0, 0, (void *)"Yes We Can", NULL, NULL
    };

    x = 3 * w / 4 - 50;

    win_dialog[n_dia++] = (DIALOG)
    {
        d_button_proc, x, y, 100, 2 * Lw, fg, bg, 27, D_EXIT, 0, 0, (void *)"Cancel", NULL, NULL
    };

    win_dialog[n_dia] = (DIALOG)
    {
        NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL
    };

# ifdef DEBUG
    for (i = 0; i < n_dia ; i++)
    {
        snprintf(debug, 256, "%d found at %d flag %x x %d y %d w %d h %d", i,
                 check_find_mouse_object(win_dialog, win_dialog[i].x + 2, win_dialog[i].y + 2),
                 win_dialog[i].flags, win_dialog[i].x, win_dialog[i].y
                 , win_dialog[i].w, win_dialog[i].h);
        alert(debug, (win_dialog[i].proc == d_picoTeX_proc) ? "pico" : "other", "test", "Ok", "Cancel", 13, 27);
    }

    for (i = 0; i < n_dia && win_dialog[i].proc != d_radio_proc1; i++);

    if (win_dialog[i].proc == d_radio_proc1)
    {
        snprintf(debug, 256, "%d radio x %d y %d w %d h %d", i, win_dialog[i].x, win_dialog[i].y
                 , win_dialog[i].w, win_dialog[i].h);
        alert(debug, "test", "test", "Ok", "Cancel", 13, 27);
    }

# endif
    position_dialog(win_dialog, x0, y0);
    clear_keybuf();

    do
    {
    }
    while (gui_mouse_b());

    for (i = -1; i == -1;)
    {
        //        win_printf(" i equal %d",i);
        //does not work with create_video_bitmap
        bmp = create_bitmap(SCREEN_W, SCREEN_H);

        if (bmp == NULL)
        {
            bmp = create_bitmap(SCREEN_W, SCREEN_H);
        }

        bmps = create_bitmap(win_dialog->w, win_dialog->h);

        if (bmps == NULL)
        {
            bmps = create_bitmap(win_dialog->w, win_dialog->h);
        }

        if (bmp == NULL || bmps == NULL)
        {
            return win_printf("out of mem");
        }


# ifdef XV_WIN32
        scare_mouse();
#endif
	for(;screen_acquired;); // we wait for screen_acquired back to 0;
	screen_acquired = 1;
        acquire_screen(); // 2013-10-06 vc
        set_clip_rect(screen, 0, 0, SCREEN_W - 1, SCREEN_H - 1);
        blit(screen, bmp, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
        release_screen(); // 2013-10-06 vc
	screen_acquired = 0;
# ifdef XV_WIN32
        unscare_mouse();
#endif
        /*
           alert("Hello","a","b","Ok","Esc",13,27);
           scare_mouse();
           blit(bmp, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
           unscare_mouse();
           alert("after blit","a","b","Ok","Esc",13,27);
           */
        player = init_dialog(win_dialog, 2);

        for (finish = 0 ; finish == 0;)
        {
            while (update_dialog(player));

            if (player->obj == 1)
            {
                m_x = player->mouse_ox;
                m_y = player->mouse_oy;

                while (mouse_b)
                {
                    if ((m_x != mouse_x) || (m_y != mouse_y))
                    {
                        vsync();
# ifdef XV_WIN32
                        scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
			for(;screen_acquired;); // we wait for screen_acquired back to 0;
			screen_acquired = 1;

                        acquire_screen(); // 2013-10-06 vc
                        blit(screen, bmps, win_dialog->x, win_dialog->y, 0, 0, win_dialog->w, win_dialog->h);
                        blit(bmp, screen, win_dialog->x, win_dialog->y, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
                        release_screen(); // 2013-10-06 vc
			screen_acquired = 0;
# ifdef XV_WIN32
                        unscare_mouse();
#endif
                        x0 += mouse_x - m_x;
                        y0 += mouse_y - m_y;
                        position_dialog(win_dialog, x0, y0);
                        m_x = mouse_x;
                        m_y = mouse_y;
# ifdef XV_WIN32
                        scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
                        //            blit(screen, bmp, win_dialog->x, win_dialog->y, 0, 0, win_dialog->w, win_dialog->h);
			for(;screen_acquired;); // we wait for screen_acquired back to 0;
			screen_acquired = 1;

                        acquire_screen(); // 2013-10-06 vc
                        blit(bmps, screen, 0, 0, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
                        release_screen(); // 2013-10-06 vc
			screen_acquired = 0;
# ifdef XV_WIN32
                        unscare_mouse();
#endif
                        //broadcast_dialog_message(MSG_DRAW,0);
                    }
                }
            }
            else
            {
                finish = 1;
                i = player->obj;
            }
        }

# ifdef XV_WIN32
        scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
	for(;screen_acquired;); // we wait for screen_acquired back to 0;
	screen_acquired = 1;

        acquire_screen(); // 2013-10-06 vc
        blit(bmp, screen, win_dialog->x, win_dialog->y, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
        release_screen(); // 2013-10-06 vc
	screen_acquired = 0;

# ifdef XV_WIN32
        unscare_mouse();
#endif
        /*
           alert("bef blit","a","b","Ok","Esc",13,27);
           scare_mouse();
           blit(bmp, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
           unscare_mouse();
           alert("after blit","a","b","Ok","Esc",13,27);
           */
        destroy_bitmap(bmp);
        destroy_bitmap(bmps);
        shutdown_dialog(player);
        //  i = popup_dialog(win_dialog,2);
        i = (i == n_dia - 2) ? WIN_OK : WIN_CANCEL;
        n_grp = n_radio = -1;

        for (j = dia_in_0; j < dia_in_n; j++)
        {
            k = 0;

            if (strcmp("int", (const char *)win_dialog[j].dp3) == 0)
            {
                k = sscanf((const char *) win_dialog[j].dp, "%d", (int *)win_dialog[j].dp2);
            }
            else if (strcmp("word", (const char *)win_dialog[j].dp3) == 0)
            {
                k = sscanf((const char *) win_dialog[j].dp, "%s", (char *)win_dialog[j].dp2);
            }
            else if (strcmp("float", (const char *)win_dialog[j].dp3) == 0)
            {
                k = sscanf((const char *) win_dialog[j].dp, "%g", (float *)win_dialog[j].dp2);
            }
            else if (strcmp("bool", (const char *)win_dialog[j].dp3) == 0)
            {
                *(int *)win_dialog[j].dp2 = (win_dialog[j].flags & D_SELECTED) ? 1 : 0;
                k = 1;
            }
            else if (strcmp("radio", (const char *) win_dialog[j].dp3) == 0)
            {
                if (win_dialog[j].d1 == n_grp)
                {
                    n_radio++;
                }
                else
                {
                    n_grp = win_dialog[j].d1;
                    n_radio = 0;
                }

                if (win_dialog[j].flags & D_SELECTED)
                {
                    *(int *)win_dialog[j].dp2 = n_radio;
                }

                k = 1;
            }
            else if (strcmp("serial_b", (const char *)win_dialog[j].dp3) == 0)
            {
                *(char *)win_dialog[j].dp2 = (win_dialog[j].flags & D_SELECTED) ? 1 : 0;
                k = 1;
            }
            else if (strcmp("string", (const char *)win_dialog[j].dp3) == 0)
            {
                if (strlen((const char *)win_dialog[j].dp) == 0)
                {
                    k = 0;
                }
                else
                {
                    strcpy((char *)win_dialog[j].dp2, (char *) win_dialog[j].dp);
                    k = 1;
                }
            }
            else if (strcmp("double", (const char *)win_dialog[j].dp3) == 0)
            {
                k = sscanf((const char *)win_dialog[j].dp, "%lg", (double *)win_dialog[j].dp2);
            }

            if (k != 1)
            {
                snprintf((char *)win_dialog[j].dp, 32, "Invalid entry!");
                i = -1;
            }

            //             win_printf(" k equal %d\ni equal %d",k,i);
        }
    }

    for (j = dia_in_0; j < dia_in_n; j++)
        if (win_dialog[j].dp)
        {
            free(win_dialog[j].dp);
        }

    if (win_dialog)
    {
        free(win_dialog);
    }

    free_box(ba);

    if (ba)
    {
        free(ba);
    }

    ttf_enable = tf;
    switch_allegro_font(0);
    return i;
}
# ifdef EN_COURS
int win_scanf2(char *fmt, ...)
{
    int i, j, k;
    va_list ap;
    char *p, c[256] = {0}, *title = NULL;
    struct box *bo, *ba;
    MENU *menu = NULL;
    int wth, hg, Lw, l, x0 = 50, y0 = 50, nw, txt_start, n_format = 0;
    void *vp = NULL, *radio_ptr = NULL, *serial_b_ptr = NULL;
    DIALOG *win_dialog;
    int n_dia = 0, n_line = 0, x, y, w, h, n_grp = -1, n_radio = -1, n_serial_b = -1;
    char *buf[256] = {0}, *line_txt[256] = {0};
    int line_dia_n[256] = {0}, size_dia_n[256] = {0};
    int tf = ttf_enable;
    int dia_in_0, dia_in_n, fg, bg, cf, cb;
    char *bp;
# ifdef DEBUG
    char debug[256] = {0};
# endif
    DIALOG_PLAYER *player = NULL;
    BITMAP *bmp = NULL, *bmps = NULL;
    int finish, m_x, m_y;

    if ((win_dialog = (DIALOG *)calloc(256, sizeof(DIALOG))) == NULL)
    {
        return 1;
    }

    wth = 3 * menu_x0 / 8;
    hg = max_y / 3;
    Lw = Lift_W;
    l = Lift_W / 2;
    switch_allegro_font(2);
    ttf_enable = 1;
    pc_fen = text_scale;
    ba = (struct box *)calloc(1, sizeof(struct box));

    if (ba == NULL)
    {
        return 1;
    }

    ba->color = White;
    bg = makecol(200, 200, 200);
    fg = makecol(32, 32, 32);
    cf = makecol(0, 0, 0);
    cb = makecol(255, 255, 255);
    win_dialog[n_dia++] = (DIALOG)
    {
        d_myclear_proc, 0, 0, 200, 200, fg, bg, 0, 0, 0, 0, NULL, NULL, NULL
    };
    win_dialog[n_dia++] = (DIALOG)
    {
        d_title_proc, 0, 0, 200, 2 * Lw, fg, bg, 0, D_EXIT, 0, 0, title, NULL, NULL
    };
    //win_dialog[n_dia++] = (DIALOG){general_idle,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL};
    va_start(ap, fmt);
    dia_in_0 = n_dia;
    txt_start = Lw;

    for (p = fmt, c[0] = 0, j = 0, hg = 2 * Lw, i = 0 ; *p ; p++)
    {
        if (*p == '\n')
        {
            c[i] = 0;
            line_txt[n_line] = strdup(c);
            line_dia_n[n_line++] = -1;
            i = 0;
        }
        else    if (*p != '%')
        {
            c[i] = *p;
            i = (i < 255) ? i + 1 : i;
            continue;
        }
        else
        {
            n_format = (int)strtol(p + 1, &bp, 10);

            if (n_format != 0 && bp != NULL)
            {
                p = bp - 1;
            }
            else
            {
                n_format = 0;
            }

            switch (*++p)
            {
            case 'd':
                vp = va_arg(ap, int *);
                c[i] = 0;
                line_txt[n_line] = strdup(c);
                size_dia_n[n_line] = n_format;
                line_dia_n[n_line++] = n_dia;
                buf[n_dia] = (char *)calloc(32, sizeof(char));
                snprintf(buf[n_dia], 32, "%d", *((int *)vp));
                win_dialog[n_dia] = (DIALOG)
                {
                    d_edit_proc, 0, 0, 0, 0, cf, cb,
                                 0, 0, 32, 0, buf[n_dia], (void *)vp, "int"
                };
                n_dia++;
                i = 0;
                break;

            case 's':
                vp = va_arg(ap, char *);
                c[i] = 0;
                line_txt[n_line] = strdup(c);
                size_dia_n[n_line] = n_format;
                line_dia_n[n_line++] = n_dia;
                buf[n_dia] = (char *)calloc(256, sizeof(char));
                snprintf(buf[n_dia], 256, "%s", (char *)vp);
                win_dialog[n_dia] = (DIALOG)
                {
                    d_edit_proc, 0, 0, 0, 0, cf, cb,
                                 0, 0, 32, 0, buf[n_dia], (void *)vp, "word"
                };
                n_dia++;
                i = 0;
                break;

            case 'f':
                vp = va_arg(ap, float *);
                c[i] = 0;
                line_txt[n_line] = strdup(c);
                size_dia_n[n_line] = n_format;
                line_dia_n[n_line++] = n_dia;
                buf[n_dia] = (char *)calloc(32, sizeof(char));
                snprintf(buf[n_dia], 32, "%g", *((float *)vp));
                win_dialog[n_dia] = (DIALOG)
                {
                    d_edit_proc, 0, 0, 0, 0, cf, cb,
                                 0, 0, 32, 0, buf[n_dia], (void *)vp, "float"
                };
                n_dia++;
                i = 0;
                break;

            case 'b':
                vp = va_arg(ap, int *);
                c[i] = 0;
                line_txt[n_line] = strdup(c);
                size_dia_n[n_line] = 0;
                line_dia_n[n_line++] = n_dia;
                buf[n_dia] = (char *)calloc(32, sizeof(char));
                buf[n_dia][0] = 0;
                win_dialog[n_dia] = (DIALOG)
                {
                    d_check_proc, 0, 0, 0, 0, cf, cb,
                                  0, ((*(int *)vp) != 0) ? D_SELECTED : 0, 32, 0, buf[n_dia], (void *)vp, "bool"
                };
                n_dia++;
                i = 0;
                break;

            case 'Q':
                serial_b_ptr = vp = va_arg(ap, char *);
                n_serial_b = 0;
                n_grp++;
                c[i] = 0;
                line_txt[n_line] = strdup(c);
                size_dia_n[n_line] = 0;
                line_dia_n[n_line++] = n_dia;

                if (serial_b_ptr != NULL)
                {
                    buf[n_dia] = (char *)calloc(256, sizeof(char));
                    buf[n_dia][0] = 0;
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_check_proc, 0, 0, 0, 0, cf, cb,
                                      0, (((char *)vp)[n_serial_b] == 1) ? D_SELECTED : 0,
                                      n_grp, 0, buf[n_dia], (void *)vp + n_serial_b, "serial_b"
                    };
                    n_dia++;
                    n_serial_b++;
                }

                i = 0;
                break;

            case 'q':
                if (n_grp < 0 || serial_b_ptr == NULL)
                {
                    serial_b_ptr = vp = va_arg(ap, char *);
                    n_serial_b = 0;
                    n_grp++;
                }

                c[i] = 0;
                line_txt[n_line] = strdup(c);
                size_dia_n[n_line] = 0;
                line_dia_n[n_line++] = n_dia;

                if (serial_b_ptr != NULL)
                {
                    buf[n_dia] = (char *)calloc(32, sizeof(char));
                    buf[n_dia][0] = 0;
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_check_proc, 0, 0, 0, 0, cf, cb,
                                      0, (((char *)vp)[n_serial_b] == 1) ? D_SELECTED : 0,
                                      n_grp, 0, buf[n_dia], (void *)vp + n_serial_b, "serial_b"
                    };
                    n_dia++;
                    n_serial_b++;
                }

                i = 0;
                break;

            case 'R':
                radio_ptr = vp = va_arg(ap, int *);
                n_radio = 0;
                n_grp++;
                c[i] = 0;
                line_txt[n_line] = strdup(c);
                size_dia_n[n_line] = 0;
                line_dia_n[n_line++] = n_dia;

                if (radio_ptr != NULL)
                {
                    buf[n_dia] = (char *)calloc(32, sizeof(char));
                    buf[n_dia][0] = 0;
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_radio_proc, 0, 0, 0, 0, cf, cb,
                                      0, ((*(int *)vp) == n_radio) ? D_SELECTED : 0, n_grp
                                      , 0, buf[n_dia], (void *)vp, "radio"
                    };
                    n_dia++;
                    n_radio++;
                }

                i = 0;
                break;

            case 'r':
                if (n_grp < 0 || radio_ptr == NULL)
                {
                    radio_ptr = vp = va_arg(ap, int *);
                    n_radio = 0;
                    n_grp++;
                }

                c[i] = 0;
                line_txt[n_line] = strdup(c);
                size_dia_n[n_line] = 0;
                line_dia_n[n_line++] = n_dia;

                if (radio_ptr != NULL)
                {
                    buf[n_dia] = (char *)calloc(32, sizeof(char));
                    buf[n_dia][0] = 0;
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_radio_proc, txt_start, hg + 10, Lw, Lw, cf, cb,
                                      0, ((*(int *)vp) == n_radio) ? D_SELECTED : 0, n_grp
                                      , 0, buf[n_dia], (void *)vp, "radio"
                    };
                    n_dia++;
                    n_radio++;
                }

                i = 0;
                break;

            case 'l':
                if (p[1] == 'f')
                {
                    vp = va_arg(ap, double *);
                    c[i] = 0;
                    line_txt[n_line] = strdup(c);
                    size_dia_n[n_line] = n_format;
                    line_dia_n[n_line++] = n_dia;
                    buf[n_dia] = (char *)calloc(32, sizeof(char));
                    snprintf(buf[n_dia], 32, "%g", *((double *)vp));
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, 0, 0,
                                     0, 0, cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, "double"
                    };
                    n_dia++;
                    i = 0;
                    p++;
                }
                else if (p[1] == 's')
                {
                    vp = va_arg(ap, char *);
                    c[i] = 0;
                    line_txt[n_line] = strdup(c);
                    size_dia_n[n_line] = n_format;
                    line_dia_n[n_line++] = n_dia;
                    buf[n_dia] = (char *)calloc(1024, sizeof(char));
                    snprintf(buf[n_dia], 1024, "%s", (char *)vp);
                    win_dialog[n_dia] = (DIALOG)
                    {
                        d_edit_proc, 0, 0,
                                     0, 0, cf, cb, 0, 0, 32, 0, buf[n_dia], (void *)vp, "string"
                    };
                    n_dia++;
                    i = 0;
                    p++;
                }

                break;

            case 'm':
                menu = va_arg(ap, MENU *);
                break;

            case 't':
                title = va_arg(ap, char *);
                break;

            default :
                c[i] = *p;
                i = (i < 255) ? i + 1 : i;
                break;
            };
        }
    }

    if (i != 0)
    {
        c[i] = 0;
        line_txt[n_line] = strdup(c);
        line_dia_n[n_line++] = -1;
        i = 0;
    }

    va_end(ap);
    hg += 5 * Lw;

    if (title == NULL)
    {
        title = "Enter information";
    }

    h = (hg > SCREEN_H) ? SCREEN_H : hg;
    w = (wth > SCREEN_W) ? SCREEN_W : wth;
    x0 = x = (SCREEN_W - w) / 2;
    y0 = y = (SCREEN_H - h) / 2;
    set_box_origin(ba, x0, SCREEN_H - y0, text_scale);
    win_dialog[0].w = win_dialog[1].w = w;
    win_dialog[0].h = h;
    win_dialog[1].dp = title;
    dia_in_n = n_dia;

    for (i = dia_in_0; i < dia_in_n; i++)
        if (win_dialog[i].w >= w)
        {
            win_dialog[i].w = w - 100;
        }

    h -= (11 * Lw) / 2;
    win_dialog[n_dia++] = (DIALOG)
    {
        d_picoTeX_proc, 0, 0, w, h, fg, bg, 0, D_DISABLED, 0, 0, c, ba, NULL
    };

    if (menu != NULL)
        win_dialog[n_dia++] = (DIALOG)
    {
        xvin_d_menu_proc, 0, 2 * Lw, 0, 0, fg, bg, 0, 0, 0, 0, menu, NULL, NULL
    };

    //    win_dialog[n_dia++] = (DIALOG){d_keyboard_proc,     0,   0,    0,    0,   0,  0,    0,      0,  KEY_F1,   0,    NULL,                  NULL, NULL  };
    // win_dialog[n_dia++] = (DIALOG){ d_yield_proc,        0,   0,    0,    0,   0,  0,    0,      0,       0,   0,    NULL,                   NULL, NULL  };
    y = h + (3 * Lw);

    x = w / 4 - 50;

    win_dialog[n_dia++] = (DIALOG)
    {
        d_yield_proc, 0, 0, 0, 0, 255, 0, 't', 0, 0, 0, NULL, NULL, NULL
    };

    win_dialog[n_dia++] = (DIALOG)
    {
        d_keyboard_proc, 0, 0, 0, 0, 0, 0, 27, 0, 0, 0, no_ac, NULL, NULL
    };

    win_dialog[n_dia++] = (DIALOG)
    {
        d_button_proc, x, y, 100, 2 * Lw, fg, bg, '\r', D_EXIT, 0, 0, "Yes We Can", NULL, NULL
    };

    x = 3 * w / 4 - 50;

    win_dialog[n_dia++] = (DIALOG)
    {
        d_button_proc, x, y, 100, 2 * Lw, fg, bg, 27, D_EXIT, 0, 0, "Cancel", NULL, NULL
    };

    win_dialog[n_dia] = (DIALOG)
    {
        NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL
    };

# ifdef DEBUG
    for (i = 0; i < n_dia ; i++)
    {
        snprintf(debug, 256, "%d found at %d flag %x x %d y %d w %d h %d", i,
                 check_find_mouse_object(win_dialog, win_dialog[i].x + 2, win_dialog[i].y + 2),
                 win_dialog[i].flags, win_dialog[i].x, win_dialog[i].y
                 , win_dialog[i].w, win_dialog[i].h);
        alert(debug, (win_dialog[i].proc == d_picoTeX_proc) ? "pico" : "other", "test", "Ok", "Cancel", 13, 27);
    }

    for (i = 0; i < n_dia && win_dialog[i].proc != d_radio_proc1; i++);

    if (win_dialog[i].proc == d_radio_proc1)
    {
        snprintf(debug, 256, "%d radio x %d y %d w %d h %d", i, win_dialog[i].x, win_dialog[i].y
                 , win_dialog[i].w, win_dialog[i].h);
        alert(debug, "test", "test", "Ok", "Cancel", 13, 27);
    }

# endif
    position_dialog(win_dialog, x0, y0);
    clear_keybuf();

    do
    {
    }
    while (gui_mouse_b());

    for (i = -1; i == -1;)
    {
        //        win_printf(" i equal %d",i);
        //does not work with create_video_bitmap
        bmp = create_bitmap(SCREEN_W, SCREEN_H);

        if (bmp == NULL)
        {
            bmp = create_bitmap(SCREEN_W, SCREEN_H);
        }

        bmps = create_bitmap(win_dialog->w, win_dialog->h);

        if (bmps == NULL)
        {
            bmps = create_bitmap(win_dialog->w, win_dialog->h);
        }

        if (bmp == NULL || bmps == NULL)
        {
            return win_printf("out of mem");
        }

# ifdef XV_WIN32
        scare_mouse();
#endif
	for(;screen_acquired;); // we wait for screen_acquired back to 0;
	screen_acquired = 1;
        acquire_screen(); // 2013-10-06 vc
        set_clip_rect(screen, 0, 0, SCREEN_W - 1, SCREEN_H - 1);
        blit(screen, bmp, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
        release_screen(); // 2013-10-06 vc
	screen_acquired = 0;

# ifdef XV_WIN32
        unscare_mouse();
#endif
        /*
           alert("Hello","a","b","Ok","Esc",13,27);
           scare_mouse();
           blit(bmp, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
           unscare_mouse();
           alert("after blit","a","b","Ok","Esc",13,27);
           */
        player = init_dialog(win_dialog, 2);

        for (finish = 0 ; finish == 0;)
        {
            while (update_dialog(player));

            if (player->obj == 1)
            {
                m_x = player->mouse_ox;
                m_y = player->mouse_oy;

                while (mouse_b)
                {
                    if ((m_x != mouse_x) || (m_y != mouse_y))
                    {
                        vsync();
# ifdef XV_WIN32
                        scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
			for(;screen_acquired;); // we wait for screen_acquired back to 0;
			screen_acquired = 1;
                        acquire_screen(); // 2013-10-06 vc
                        blit(screen, bmps, win_dialog->x, win_dialog->y, 0, 0, win_dialog->w, win_dialog->h);
                        blit(bmp, screen, win_dialog->x, win_dialog->y, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
                        release_screen(); // 2013-10-06 vc
			screen_acquired = 0;
# ifdef XV_WIN32
                        unscare_mouse();
#endif
                        x0 += mouse_x - m_x;
                        y0 += mouse_y - m_y;
                        position_dialog(win_dialog, x0, y0);
                        m_x = mouse_x;
                        m_y = mouse_y;
# ifdef XV_WIN32
                        scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
                        //            blit(screen, bmp, win_dialog->x, win_dialog->y, 0, 0, win_dialog->w, win_dialog->h);
			for(;screen_acquired;); // we wait for screen_acquired back to 0;
			screen_acquired = 1;

                        acquire_screen(); // 2013-10-06 vc
                        blit(bmps, screen, 0, 0, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
                        release_screen(); // 2013-10-06 vc
			screen_acquired = 0;
# ifdef XV_WIN32
                        unscare_mouse();
#endif
                        //broadcast_dialog_message(MSG_DRAW,0);
                    }
                }
            }
            else
            {
                finish = 1;
                i = player->obj;
            }
        }

# ifdef XV_WIN32
        scare_mouse_area(win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
#endif
	for(;screen_acquired;); // we wait for screen_acquired back to 0;
	screen_acquired = 1;
        acquire_screen(); // 2013-10-06 vc
        blit(bmp, screen, win_dialog->x, win_dialog->y, win_dialog->x, win_dialog->y, win_dialog->w, win_dialog->h);
        release_screen(); // 2013-10-06 vc
	screen_acquired = 0;
# ifdef XV_WIN32
        unscare_mouse();
#endif
        /*
           alert("bef blit","a","b","Ok","Esc",13,27);
           scare_mouse();
           blit(bmp, screen, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
           unscare_mouse();
           alert("after blit","a","b","Ok","Esc",13,27);
           */
        destroy_bitmap(bmp);
        destroy_bitmap(bmps);
        shutdown_dialog(player);
        //  i = popup_dialog(win_dialog,2);
        i = (i == n_dia - 2) ? WIN_OK : WIN_CANCEL;
        n_grp = n_radio = -1;

        for (j = dia_in_0; j < dia_in_n; j++)
        {
            k = 0;

            if (strcmp("int", win_dialog[j].dp3) == 0)
            {
                k = sscanf(win_dialog[j].dp, "%d", (int *)win_dialog[j].dp2);
            }
            else if (strcmp("word", win_dialog[j].dp3) == 0)
            {
                k = sscanf(win_dialog[j].dp, "%s", (char *)win_dialog[j].dp2);
            }
            else if (strcmp("float", win_dialog[j].dp3) == 0)
            {
                k = sscanf(win_dialog[j].dp, "%g", (float *)win_dialog[j].dp2);
            }
            else if (strcmp("bool", win_dialog[j].dp3) == 0)
            {
                *(int *)win_dialog[j].dp2 = (win_dialog[j].flags & D_SELECTED) ? 1 : 0;
                k = 1;
            }
            else if (strcmp("radio", win_dialog[j].dp3) == 0)
            {
                if (win_dialog[j].d1 == n_grp)
                {
                    n_radio++;
                }
                else
                {
                    n_grp = win_dialog[j].d1;
                    n_radio = 0;
                }

                if (win_dialog[j].flags & D_SELECTED)
                {
                    *(int *)win_dialog[j].dp2 = n_radio;
                }

                k = 1;
            }
            else if (strcmp("serial_b", win_dialog[j].dp3) == 0)
            {
                *(char *)win_dialog[j].dp2 = (win_dialog[j].flags & D_SELECTED) ? 1 : 0;
                k = 1;
            }
            else if (strcmp("string", win_dialog[j].dp3) == 0)
            {
                if (strlen(win_dialog[j].dp) == 0)
                {
                    k = 0;
                }
                else
                {
                    strcpy((char *)win_dialog[j].dp2, win_dialog[j].dp);
                    k = 1;
                }
            }
            else if (strcmp("double", win_dialog[j].dp3) == 0)
            {
                k = sscanf(win_dialog[j].dp, "%lg", (double *)win_dialog[j].dp2);
            }

            if (k != 1)
            {
                snprintf(win_dialog[j].dp, 32, "Invalid entry!");
                i = -1;
            }

            //             win_printf(" k equal %d\ni equal %d",k,i);
        }
    }

    for (j = dia_in_0; j < dia_in_n; j++)
        if (win_dialog[j].dp)
        {
            free(win_dialog[j].dp);
        }

    if (win_dialog)
    {
        free(win_dialog);
    }

    free_box(ba);

    if (ba)
    {
        free(ba);
    }

    ttf_enable = tf;
    switch_allegro_font(0);
    return i;
}

# endif

static int    d_redraw_proc(int msg, DIALOG *d, int c)
{
    int ret;
    /* call the parent object */
    ret = d_button_proc(msg, d, c);

    /* trap the close return value and change the font */
    if (ret == D_CLOSE)
    {
        return D_REDRAW;
    }

    /* otherwise just return */
    return ret;
}

int TeX_modify(const char *stuff)
{
    int i;
    struct box  *ba = NULL;
    int tf = ttf_enable;
    DIALOG *win_dialog = NULL;
    char *c = NULL;
    int n_dia = 0, w, h, fg, bg, cf, cb, Lw;   // , x, y
    int x0, y0;

    if ((win_dialog = (DIALOG *)calloc(64, sizeof(DIALOG))) == NULL)
    {
        return 1;
    }

    if (win_printf_message == NULL)
    {
        if ((win_printf_message = (char *)calloc(8192, sizeof(char))) == NULL)
        {
            return 1;
        }
    }

    c = win_printf_message;

    if (stuff != NULL)
    {
        strcpy(c, stuff);
    }
    else
    {
        c[0] = 0;
    }

    switch_allegro_font(1);
    ttf_enable = 1;
    pc_fen = text_scale;
    ba = (struct box *)calloc(1, sizeof(struct box));

    if (ba == NULL)
    {
        return 1;
    }

    ba->color = White;
    Lw = Lift_W;
    add_text_to_box(ba , Lw, 3 * Lw, c);
    bg = makecol(200, 200, 200);
    fg = makecol(32, 32, 32);
    cf = makecol(0, 0, 0);
    cb = makecol(255, 255, 255);
    //x = 0; y = 0;
    w = 6 * SCREEN_W / 8;
    h = 6 * SCREEN_H / 8;
    win_dialog[n_dia++] = (DIALOG)
    {
        d_myclear_proc, 0, 0, w, h, fg, bg, 0, 0, 0, 0,
                        NULL, NULL, NULL
    };
    win_dialog[n_dia++] = (DIALOG)
    {
        d_button_proc, 0, 0, w, 2 * Lw, fg, bg, 0, 0, 0, 0,
                       (void *)"PicoTeX", NULL, NULL
    };
    win_dialog[n_dia++] = (DIALOG)
    {
        d_picoTeX_proc, Lw, 3 * Lw, w - 2 * Lw, h / 2 - Lw, fg, bg,
                        0, 0, 0, 0, c, ba, NULL
    };
    win_dialog[n_dia++] = (DIALOG)
    {
        d_edit_proc, Lw, h / 2 + 2 * Lw, w - 2 * Lw, h / 4, cf, cb, 0, 0,
                     8192, 0, c, NULL, NULL
    };
    win_dialog[n_dia++] = (DIALOG)
    {
        d_button_proc, 5 * Lw, h - 3 * Lw, 10 * Lw, 2 * Lw, fg, bg, 0,
                       D_EXIT, 0, 0, (void *)"OK", NULL, NULL
    };
    win_dialog[n_dia++] = (DIALOG)
    {
        d_redraw_proc, w / 2 - 5 * Lw, h - 3 * Lw, 10 * Lw, 2 * Lw, fg, bg,
                       0, D_EXIT, 0, 0, (void *)"PicoTeX", NULL, NULL
    };
    win_dialog[n_dia++] = (DIALOG)
    {
        d_button_proc, w / 2 + 10 * Lw, h - 3 * Lw, 10 * Lw, 2 * Lw,
                       fg, bg, 0, D_EXIT, 0, 0, (void *)"Cancel", NULL, NULL
    };
#ifdef XV_MAC
    win_dialog[n_dia++] = (DIALOG)
    {
        d_print_slash_proc, 16 * Lw, h - 3 * Lw, 4 * Lw, 2 * Lw, fg, bg, 0,
                            D_EXIT, 0, 0, "\\", c, NULL
    };
#endif
    win_dialog[n_dia] = (DIALOG)
    {
        NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL
    };
    x0 = (SCREEN_W - w) / 2;
    y0 = (SCREEN_H - h) / 2;
    set_box_origin(ba, x0, SCREEN_H - y0, text_scale);
    position_dialog(win_dialog, x0, y0);
    i = popup_dialog(win_dialog, 3);
#ifdef XV_MAC // 4 buttons
    last_answer = (i == n_dia - 4) ? c : NULL;
#else // 3 buttons
    last_answer = (i == n_dia - 3) ? c : NULL;
#endif

    if (win_dialog)
    {
        free(win_dialog);
    }

    free_box(ba);

    if (ba)
    {
        free(ba);
    }

    ttf_enable = tf;
    switch_allegro_font(0);
    return 0;
}

int    display_title_message(const char *format, ...)
{
    static char *c = NULL;
    va_list ap;
    //extern FONT *original_font;

    if (win_printf_message == NULL)
    {
        if ((win_printf_message = (char *)calloc(8192, sizeof(char))) == NULL)
        {
            return 1;
        }
    }

    c = win_printf_message;
    va_start(ap, format);
    vsnprintf(c, 8192, format, ap);
    va_end(ap);
# ifdef XV_WIN32

    if (win_get_window() == GetForegroundWindow())
    {
# endif

        if (!screen_used)
        {
            screen_used = 1;
	    for(;screen_acquired;); // we wait for screen_acquired back to 0;
	    screen_acquired = 1;
            acquire_bitmap(screen);
            textout_centre_ex(screen, normalsize_font, c, SCREEN_W / 2, SCREEN_H / 16,
                              makecol(255, 255, 255), makecol(0, 0, 0));
            release_bitmap(screen);
	    screen_acquired = 0;
            screen_used = 0;
        }

# ifdef XV_WIN32
    }

# endif
    return 0;
}

# endif /* _WIN_GUI_C_ */
