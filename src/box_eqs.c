
/*	program		Try to build an equation, using a simili TeX input
 *	DESCRIPTION	
 *			
 *			
 *
 *	
 *			
 */
# ifndef _BOX_EQS_C_ 
# define _BOX_EQS_C_


	

/* other includes above ^^^ */





# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <ctype.h>

# ifdef GCC_COMPIL
# include <sys/stat.h>
# include <malloc.h>
# endif


// following line is because win_printf() is used here, but xvin.h is not included.
//int win_printf(char *fmt, ...);

# include "xvin/platform.h"
# include "xvinerr.h"

/* # define DEBUG */

# include "box_def.h"
# include "color.h"
# include "util.h"
# include "xvin.h"

# ifdef FORTIFY
# include "fortify.h"
# endif


int 	close_frac(struct box *ba);
int 	close_paragraph(struct box *ba);
int 	close_centerparagraph(struct box *ba);
int 	close_leftparagraph(struct box *ba);
int 	close_rightparagraph(struct box *ba);
int		close_subscript(struct box *ba);
int		close_exponent(struct box *ba);
int		close_over(struct box *ba);
int		close_stack(struct box *ba);
int		close_left(struct box *ba);
int		close_fbox(struct box *ba);
int		close_center(struct box *ba);
int		close_top(struct box *ba);
int		close_bottom(struct box *ba);
int		close_vec(struct box *ba);
int		close_sqrt(struct box *ba);
int 	close_tilde(struct box *ba);

struct box *closing_special(struct box *ba);
struct box *closing_special2(struct box *ba);

int line_width = 9600;



/* short int   	A_d, w_white;*/




int simple_string_to_box(char *st,struct box *b0)
{
	char k;
	int st_n = 0;
	char ch[8] = {0};
	struct box *ba = NULL, *bc = NULL;

	if (b0 == NULL || st == NULL) return 1;	
	ba = b0;
		
	while ((k = st[st_n++]) != 0 )
	{
		if ( k != '\r' && k != '\n') 
		{
			ch[0] = (char)k;
			ch[1] = 0;
			if ( ch[0] == '\\' )
			{
				ch[1] = '\\';
				ch[2] = 0;
			}
			if (attach_ch(ch, ba) == NULL)		return 2;
			if ((bc = set_origin(ba)) == NULL)	return 2;
			bc->yc = 3*d_off/16;
			set_box_size(bc, ba);
			adjust_width(ba);
		}
	}
	return 0;	
}


/*	string_to_box(st,b0);
 *	int simple_string_to_box(char *st,struct box *b0);
 *	DESCRIPTION	convert string and make boxes
 *			
 *			
 *
 *	RETURNS		0 on success, 1 
 *			
 */
int string_to_box(char *st,struct box *b0)
{
	int k, l, st_n = 0, ch_n, start_hic;
	int color, len, itmp;
	short int *x = NULL, *y = NULL;
	static char ch[128] = {0}, colorname[128] = {0};
	struct box *ba = NULL, *bc = NULL;

	if (b0 == NULL || st == NULL) return 1;
	ba = b0;
	ch[0] = 0;
	start_hic = ba->n_hic;
	while ((k = st[st_n++]) != 0 )
	{
		l = 255;
		if ( k == 125 )
		{
			if ( ba->n_hic == 0 )
			{
				snprintf (eqs_err,128, "unmatched }!.. \n");
				simple_string_to_box ("unmatched }!..",b0);
				return 1;
			}
			ba = ba->parent;
			adjust_width(ba);
			ba = closing_special(ba);
		}
		else if ( k == '_' )	/* subscript */
		{
			if (ba->n_box == 0)
			{
				if (attach_ch("\\_", ba) == NULL)	return 2;
			}
			else
			{
				bc = ba->child[ba->n_box-1];
				if ( (bc->i_box & EXPONENT))
				{
					ba->w -= bc->w;
					ba = bc ;
				}
				else
				{
					if ((ba = catch_last_box(ba)) == NULL)	return 2;
					ba->pt *= 7;
					ba->pt /= 10;
				}
				ba->i_box &= !EXPONENT;
				ba->i_box |= SUBSCRIPT;
			}
		}
		else if ( k == '^' )	
		{
			if (ba->n_box == 0)
			{
				if (attach_ch("\\^", ba) == NULL)	return 2;
			}
			else			
			{
				bc = ba->child[ba->n_box-1];
				if ( bc->i_box & SUBSCRIPT)
				{
					ba->w -= bc->w;
					ba = bc ;
				}
				else
				{
					if ((ba = catch_last_box(ba)) == NULL)	return 2;;
					ba->pt *= 7;
					ba->pt /= 10;
				}
				ba->i_box &= !SUBSCRIPT;
				ba->i_box |= EXPONENT;
			}
		}
		else if ( k != 13 && k != 10) 
		{
			if ( k == 92 )
			{
				ch_n = 1;
				if( st[st_n] == '{' || st[st_n] == '^' || st[st_n] == '}' )
					ch[ch_n++]=st[st_n++];
				else
				{
					while ( isalnum(st[st_n]) )
						ch[ch_n++]=st[st_n++];
				}
				ch[0] = (char)92;
				ch[ch_n] = 0;
				if (st[st_n] == ' ')
					st_n++;
				l=255;
				if ( (ch[1] == 'i') && (ch[2] == 't') )
				{
					ba->slx = 4;
					ba->sly = 8;
					l=0;
				}
				else if ( strcmp(ch+1,"sl") == 0 )
				{
					ba->slx = 2;
					ba->sly = 8;
					l=0;
				}
				else if ( strcmp(ch+1,"rm") == 0 )
				{
					ba->slx = 0;
					ba->sly = 1;
					l=0;
				}
				else if ( ch[1] == 'p' && ch[2] == 't' )
				{
/*					j = ba->pt;*/
					ba->pt = atoi(ch+3);
					l=0;
/*					d_off += (j-ba->pt)*A_d;*/
				}
				else if ( strcmp(ch+1,"color") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&color) == 1)
					{
						ba->color = color;
						for (; st[st_n++]!= '}';);
					}
					else if (sscanf(st+st_n,"{%s}",colorname) == 1)
					{
						if (strncmp(colorname,"black",5) == 0)	
							ba->color = Black;
						else if (strncmp(colorname,"blue",4) == 0)	
							ba->color =  Blue;
						else if (strncmp(colorname,"green",5) == 0)	
							ba->color =  Green;															else if (strncmp(colorname,"cyan",4) == 0)	
							ba->color =  Cyan;
						else if (strncmp(colorname,"red",3) == 0)	
							ba->color =  Red;
						else if (strncmp(colorname,"magenta",7) == 0)	
							ba->color =  Magenta;		
						else if (strncmp(colorname,"brown",5) == 0)	
							ba->color =  Brown;
						else if (strncmp(colorname,"lightgray",9) == 0)	
							ba->color =  Lightgray;
						else if (strncmp(colorname,"darkgray",8) == 0)	
							ba->color =  Darkgray;
						else if (strncmp(colorname,"lightblue",9) == 0)	
							ba->color =  Lightblue;		
						else if (strncmp(colorname,"lightgreen",10) == 0)	
							ba->color =  Lightgreen;
						else if (strncmp(colorname,"lightcyan",9) == 0)	
							ba->color =  Lightcyan;	
						else if (strncmp(colorname,"lightred",8) == 0)	
							ba->color =  Lightred;
						else if (strncmp(colorname,"lightmagenta",12) == 0)	
							ba->color =  Lightmagenta;			
						else if (strncmp(colorname,"yellow",6) == 0)	
							ba->color =  Yellow;
						else if (strncmp(colorname,"white",5) == 0)	
							ba->color =  White;
						for (; st[st_n++]!= '}';);
/*						printf("color by name %s %d",colorname,ba->color);*/
					}
					l=0;
				}				
				else if ( strcmp(ch+1,"harrows") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&len) == 1)
					{
						if (mknewbox(ba) == NULL) 			return 2;
						if ((ba = set_origin(ba)) == NULL)	return 2;
						if (attach_ch("\\leftarrow", ba) == NULL)	return 2;
						if ((bc = set_origin(ba)) == NULL)	return 2;
						set_box_size(bc, ba);						
						if (attach_ch("\\rightarrow", ba) == NULL)	return 2;
						if ((bc = set_origin(ba)) == NULL)	return 2;
						set_box_size(bc, ba);								
						ba->child[1]->xc = ba->child[0]->w; 
						ba->child[1]->w = len - ba->child[1]->xc;
						ba->child[1]->yc = ba->child[0]->yc;
						ba->w = len;
						ba->d = ba->child[0]->d;
						ba->h = ba->child[0]->h;
						adjust_width(ba->parent);	
					}
					for (; st[st_n++]!= '}';);
					l=0;
				}	
				else if ( strcmp(ch+1,"hspace") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&len) == 1)
					{
						if (mknewbox(ba) == NULL) 			return 2;
						if ((ba = set_origin(ba)) == NULL)	return 2;
						if (attach_ch(" ", ba) == NULL)	 return 2;
						if ((bc = set_origin(ba)) == NULL)	return 2;
						bc->w = ba->w = len;
						adjust_width(ba);	
						adjust_width(ba->parent);	
					}
					for (; st[st_n++]!= '}';);
					l=0;
				}		
				else if ( strcmp(ch+1,"vspace") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&len) == 1)
					{
						if (mknewbox(ba) == NULL) 			return 2;
						if ((ba = set_origin(ba)) == NULL)	return 2;
						if (attach_ch(" ", ba) == NULL)	 return 2;
						if ((bc = set_origin(ba)) == NULL)	return 2;
						bc->h = ba->h = len - bc->d;
						adjust_width(ba);	
						adjust_width(ba->parent);	
					}
					for (; st[st_n++]!= '}';);
					l=0;
				}								
				else if ( strcmp(ch+1,"hfatarrows") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&len) == 1)
					{
						if (mknewbox(ba) == NULL) 			return 2;
						if ((ba = set_origin(ba)) == NULL)	return 2;
						if ((bc = mkpolybox(ba, 11)) == NULL) return 2;
						bc->n_box = RE_POLYGONE_BOX;
						itmp = (ba->pt*box_font.w_white)/4;
						x = (short int*)bc->child[0];
						y = (short int*)bc->child[1];	
						x[0] = 2*itmp;
						x[1] = 2*(len - 2*itmp) + 1;
						x[2] = 1;
						x[3] = 2*itmp + 1;
						x[4] = -(2*itmp) + 1;
						x[5] = 1;
						x[6] = - 2*(len - 2*itmp) + 1;
						x[7] = 1;
						x[8] = -(2*itmp) + 1;
						x[9] = (2*itmp) + 1;
						x[10] = 1;
						y[0] = 2*(itmp+(itmp/2));
						y[2] = 2*(itmp/2);
						y[3] = -2*itmp;
						y[4] = -2*itmp;
						y[5] = 2*(itmp/2);
						y[7] = -2*(itmp/2);
						y[8] = 2*itmp;
						y[9] = 2*itmp;
						y[10] = -2*(itmp/2);
						bc->color = ba->color;
						ba->w = len;
						ba->d = 0;
						ba->h = 2*itmp;
						adjust_width(ba->parent);	
					}
					else win_printf("could not get arrow length");
					for (; st[st_n++]!= '}';);
					l=0;
				}	
				else if ( strcmp(ch+1,"leftfatarrow") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&len) == 1)
					{
						if (mknewbox(ba) == NULL) 			return 2;
						if ((ba = set_origin(ba)) == NULL)	return 2;
						if ((bc = mkpolybox(ba, 8)) == NULL) return 2;
						bc->n_box = RE_POLYGONE_BOX;
						itmp = (ba->pt*box_font.w_white)/4;
						x = (short int*)bc->child[0];
						y = (short int*)bc->child[1];	
						x[0] = 2*itmp;
						x[1] = 2*(len - itmp) + 1;
						x[2] = 1;
						x[3] = -2*(len - itmp) + 1;
						x[4] = 1;
						x[5] = -(2*itmp) + 1;
						x[6] = (2*itmp) + 1;
						x[7] = 1;						
						y[0] = 2*(itmp+(itmp/2));
						y[2] = -2*(itmp);
						y[4] = -2*(itmp/2);
						y[5] = 2*itmp;
						y[6] = 2*itmp;
						y[7] = -2*(itmp/2);
						ba->w = len;
						ba->d = 0;
						ba->h = 2*itmp;
						adjust_width(ba->parent);	
					}
					for (; st[st_n++]!= '}';);
					l=0;
				}	
				else if ( strcmp(ch+1,"rightfatarrow") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&len) == 1)
					{
						if (mknewbox(ba) == NULL) 			return 2;
						if ((ba = set_origin(ba)) == NULL)	return 2;
						if ((bc = mkpolybox(ba, 8)) == NULL) return 2;
						bc->n_box = RE_POLYGONE_BOX;
						itmp = (ba->pt*box_font.w_white)/4;
						x = (short int*)bc->child[0];
						y = (short int*)bc->child[1];	
						x[0] = 0;
						x[1] = 2*(len - itmp) + 1;
						x[2] = 1;
						x[3] = 2*itmp + 1;
						x[4] = -(2*itmp) + 1;
						x[5] = 1;
						x[6] = - 2*(len - itmp) + 1;
						x[7] = 1;
						y[0] = 2*(itmp+(itmp/2));
						y[2] = 2*(itmp/2);
						y[3] = -2*itmp;
						y[4] = -2*itmp;
						y[5] = 2*(itmp/2);
						y[7] = 2*(itmp);
						ba->w = len;
						ba->d = 0;
						ba->h = 2*itmp;
						adjust_width(ba->parent);	
					}
					for (; st[st_n++]!= '}';);
					l=0;
				}	
				else if ( strcmp(ch+1,"upfatarrow") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&len) == 1)
					{
						if (mknewbox(ba) == NULL) 			return 2;
						if ((ba = set_origin(ba)) == NULL)	return 2;
						if ((bc = mkpolybox(ba, 8)) == NULL) return 2;
						bc->n_box = RE_POLYGONE_BOX;
						itmp = (ba->pt*box_font.w_white)/4;
						x = (short int*)bc->child[0];
						y = (short int*)bc->child[1];	
						y[1] = 2*(len - itmp);
						y[3] = 2*itmp;
						y[4] = -(2*itmp);
						y[6] = - 2*(len - itmp);
						x[0] = 2*(itmp+(itmp/2));
						x[1] = 1;
						x[2] = 2*(itmp/2) + 1;
						x[3] = -2*itmp + 1;
						x[4] = -2*itmp + 1;
						x[5] = 2*(itmp/2) + 1;
						x[6] = 1;
						x[7] = 2*itmp+1;
						ba->h = len;
						ba->d = 0;
						ba->w = 2*itmp;
						adjust_width(ba->parent);	
					}
					for (; st[st_n++]!= '}';);
					l=0;
				}	
				else if ( strcmp(ch+1,"downfatarrow") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&len) == 1)
					{
						if (mknewbox(ba) == NULL) 			return 2;
						if ((ba = set_origin(ba)) == NULL)	return 2;
						if ((bc = mkpolybox(ba, 8)) == NULL) return 2;
						bc->n_box = RE_POLYGONE_BOX;
						itmp = (ba->pt*box_font.w_white)/4;
						x = (short int*)bc->child[0];
						y = (short int*)bc->child[1];	
						y[0] = 2*itmp;						
						y[1] = 2*(len - itmp);
						y[3] = -2*(len - itmp);
						y[5] = -(2*itmp);
						y[6] = (2*itmp);
						x[0] = 2*(itmp+(itmp/2));
						x[1] = 1;
						x[2] = -2*itmp + 1;
						x[3] = 1;
						x[4] = -2*(itmp/2) + 1;
						x[5] = 2*itmp + 1;
						x[6] = 2*itmp + 1;
						x[7] = -2*(itmp/2) + 1;
						ba->h = len;
						ba->d = 0;
						ba->w = 2*itmp;
						adjust_width(ba->parent);	
					}
					for (; st[st_n++]!= '}';);
					l=0;
				}					
				else if ( strcmp(ch+1,"vfatarrows") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&len) == 1)
					{
						if (mknewbox(ba) == NULL) 			return 2;
						if ((ba = set_origin(ba)) == NULL)	return 2;
						if ((bc = mkpolybox(ba, 11)) == NULL) return 2;
						bc->n_box = RE_POLYGONE_BOX;
						itmp = (ba->pt*box_font.w_white)/4;
						x = (short int*)bc->child[0];
						y = (short int*)bc->child[1];	
						y[0] = 2*itmp;
						y[1] = 2*(len - 2*itmp);
						y[3] = 2*itmp;
						y[4] = -(2*itmp);
						y[6] = - 2*(len - 2*itmp);
						y[8] = -(2*itmp);
						y[9] = (2*itmp);
						x[0] = 2*(itmp+(itmp/2));
						x[1] = 1;
						x[2] = 2*(itmp/2) + 1;
						x[3] = -2*itmp + 1;
						x[4] = -2*itmp + 1;
						x[5] = 2*(itmp/2) + 1;
						x[6] = 1;
						x[7] = -2*(itmp/2) + 1;
						x[8] = 2*itmp + 1;
						x[9] = 2*itmp + 1;
						y[10] = -2*(itmp/2) + 1;
						ba->h = len;
						ba->d = 0;
						ba->w = 2*itmp;
						adjust_width(ba->parent);	
					}
					for (; st[st_n++]!= '}';);
					l=0;
				}					
				else if ( strcmp(ch+1,"varrows") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&len) == 1)
					{
						if (mknewbox(ba) == NULL) 			return 2;
						if ((ba = set_origin(ba)) == NULL)	return 2;
						if (attach_ch("\\downarrow", ba) == NULL)	return 2;
						if ((bc = set_origin(ba)) == NULL)	return 2;
						set_box_size(bc, ba);	
						if ((bc = mkpolybox(ba, 2)) == NULL) return 2;
						((short int *)bc->child[0])[0] = 0; 					
						((short int *)bc->child[0])[1] = 0x0001;
						((short int *)bc->child[1])[0] = 0; 					
						((short int *)bc->child[1])[1] = 2*(len - ba->child[0]->d - - ba->child[0]->h);						
						if (attach_ch("\\uparrow", ba) == NULL)	return 2;
						if ((bc = set_origin(ba)) == NULL)	return 2;
						set_box_size(bc, ba);								
						ba->child[2]->yc = len - ba->child[2]->h; 
						ba->child[0]->yc = ba->child[0]->d;
						ba->child[2]->xc = ba->child[0]->xc = 0;
						ba->child[1]->xc = ba->child[0]->w/2;
;						ba->h = len - ba->d;
						ba->w = ba->child[0]->w;
						adjust_width(ba->parent);		
					}
					for (; st[st_n++]!= '}';);
					l=0;
				}					
				else if ( strcmp(ch+1,"over") == 0 )
				{
					if ( ba->n_hic == 0 )
					{
						snprintf (eqs_err, 128,"unmatched }!.. \n");
						simple_string_to_box ("unmatched }!..",b0);
						return 1;
					}
					ba = ba->parent;
					adjust_width(ba);
					ba = catch_last_box(ba);
					if (ba == NULL)		return 2;
					if (attach_ch("\\line", ba) == NULL)	return 2;
					ba->i_box |= OVER;
					k = 123;
				}
				else if ( strcmp(ch+1,"frac") == 0 )
				{
					if (mknewbox(ba) == NULL) 			return 2;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					ba->i_box |= FRAC;
					if (attach_ch("\\line", ba) == NULL) return 2;
					l=0;
				}				
				else if ( strcmp(ch+1,"vec") == 0 )
				{
					if (mknewbox(ba) == NULL) 			return 2;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					ba->i_box |= VEC;
					if (attach_ch("\\rightarrow", ba) == NULL) return 2;
					l=0;
				}
				else if ( strcmp(ch+1,"tilde") == 0 )
				{
					if (mknewbox(ba) == NULL) 			return 2;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					ba->i_box |= TILDE;
					if (mkpolybox(ba,10) == NULL)		return 2;
					l=0;
				}				
				else if ( strcmp(ch+1,"stack") == 0 )
				{
					if (mknewbox(ba) == NULL) 			return 2;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					ba->i_box |= STACK;
					l=0;
				}	
				else if ( strcmp(ch+1,"paragraph") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&line_width) == 1)
					{
						for (; st[st_n++]!= '}';);
					}
					if (mknewbox(ba) == NULL) 			return 2;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					ba->i_box |= PARAGRAPH;					
					l=0;
				}
				else if ( strcmp(ch+1,"centerparagraph") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&line_width) == 1)
					{
						for (; st[st_n++]!= '}';);
					}
					if (mknewbox(ba) == NULL) 			return 2;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					ba->i_box |= CENTERPARAGRAPH;					
					l=0;
				}	
				else if ( strcmp(ch+1,"leftparagraph") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&line_width) == 1)
					{
						for (; st[st_n++]!= '}';);
					}
					if (mknewbox(ba) == NULL) 			return 2;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					ba->i_box |= LEFTPARAGRAPH;					
					l=0;
				}	
				else if ( strcmp(ch+1,"rightparagraph") == 0 )
				{
					if (sscanf(st+st_n,"{%d}",&line_width) == 1)
					{
						for (; st[st_n++]!= '}';);
					}
					if (mknewbox(ba) == NULL) 			return 2;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					ba->i_box |= RIGHTPARAGRAPH;					
					l=0;
				}																	
				else if ( strcmp(ch+1,"fbox") == 0 )
				{
					if (mknewbox(ba) == NULL) 			return 2;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					ba->i_box |= FBOX;
					l=0;
				}				
				else if ( strcmp(ch+1,"left") == 0 )
				{
					if (mknewbox(ba) == NULL) 			return 2;
					ba->child[ba->n_box-1]->i_box |= LEFT;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					l=0;
				}				
				else if ( strcmp(ch+1,"center") == 0 )
				{
					if (mknewbox(ba) == NULL) 			return 2;
					ba->child[ba->n_box-1]->i_box|=CENTER;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					l=0;
				}				
				else if ( strcmp(ch+1,"top") == 0 )
				{
					if (mknewbox(ba) == NULL) 			return 2;
					ba->child[ba->n_box-1]->i_box |= TOP;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					l=0;
				}				
				else if ( strcmp(ch+1,"bottom") == 0 )
				{
					if (mknewbox(ba) == NULL) 			return 2;
					ba->child[ba->n_box-1]->i_box|=BOTTOM;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					l=0;
				}				
				else if ( strcmp(ch+1,"sqrt") == 0 )
				{
					if (mknewbox(ba) == NULL) 			return 2;
					if ((ba = set_origin(ba)) == NULL)	return 2;
					ba->i_box |= SQRT;
					if (attach_ch("\\line", ba) == NULL)	return 2;
					strcpy(ch,"\\sqrt");
				}
				else if ( strcmp(ch+1,"sum") == 0 )
				{
					l = ba->pt;
					ba->pt *= 10;
					ba->pt /= 7;
					if (attach_ch("\\Sigma", ba) == NULL) 	return 2;
					if ((bc = set_origin(ba)) == NULL)	return 2;
					bc->i_box |= SUM;
					set_box_size(bc, ba);
					adjust_width(ba);
					ba->pt = l;
					bc->pt = l;
					l = 0;
				}
				if (l != 0 && k == 92)
				{
					l= find_char(ch);
					if ( l==0 )
					{
						snprintf (eqs_err, 128,"unknown char %s!\n",ch);
						simple_string_to_box (eqs_err,b0);
						return 1;
					}
				}
			}
			if ( l != 0)
			{
				if ( k != 92 && k != 123 )
				{
					ch[0] = (char)k;
					ch[1] = 0;
				}
				if( k != 123 )
				{
					if (attach_ch(ch, ba) == NULL)		return 2;			
				}
				else if( k == 123 )
				{
					if (mknewbox(ba) == NULL) 			return 2;
				}
				if ((bc = set_origin(ba)) == NULL) return 2;
				if ( k == 92 )
				{
					bc->slx = 0;
					bc->sly = 1;
				}
				if( k != 123 )
				{
					bc->yc = 3*d_off/16;
					set_box_size(bc, ba);
					adjust_width(ba);
				}
				if ( k == 123 )
					ba = bc;
				ba = closing_special2(ba);
			}
		}
	}
	if ( ba->n_hic > start_hic)
	{
		snprintf (eqs_err, 128,"unmatched {!.. \n");
		return 3;		
	}
	else		return 0;
}

struct box *closing_special(struct box *ba)
{
	if (ba == NULL) return NULL;
	if ( ba->i_box & OVER )
	{
		close_over(ba);
		ba = ba->parent;
		adjust_width(ba);
	}
	else if ( ba->i_box & FRAC && ba->n_box > 2)
	{
		close_frac(ba);
		ba = ba->parent;
		adjust_width(ba);
	}	
	ba = closing_special2(ba);
	return (ba);
}
struct box *closing_special2(struct box *ba)
{
	if (ba == NULL) return NULL;
	if ( ba->i_box & VEC )
	{
		close_vec(ba);
		ba = ba->parent;
		adjust_width(ba);
	}	
	if ( ba->i_box & SQRT && ba->n_box > 2 )
	{
		close_sqrt(ba);
		ba = ba->parent;
		adjust_width(ba);		
	}	
	if ( ba->i_box & FBOX )
	{
		close_fbox(ba);
		ba = ba->parent;
		adjust_width(ba);
	}		
	if ( ba->i_box & SUBSCRIPT )
	{
		close_subscript(ba);
		ba = ba->parent;
		adjust_width(ba);
	}
	if ( ba->i_box & EXPONENT )
	{
		close_exponent(ba);
		ba = ba->parent;
		adjust_width(ba);
	}
	if ( ba->i_box & STACK )
	{
		close_stack(ba);
		ba = ba->parent;
		adjust_width(ba);
	}	
	if ( ba->i_box & PARAGRAPH )
	{
		close_paragraph(ba);
		ba = ba->parent;
		adjust_width(ba);
	}	
	if ( ba->i_box & CENTERPARAGRAPH )
	{
		close_centerparagraph(ba);
		ba = ba->parent;
		adjust_width(ba);
	}	
	if ( ba->i_box & LEFTPARAGRAPH )
	{
		close_leftparagraph(ba);
		ba = ba->parent;
		adjust_width(ba);
	}	
	if ( ba->i_box & RIGHTPARAGRAPH )
	{
		close_rightparagraph(ba);
		ba = ba->parent;
		adjust_width(ba);
	}				
	if ( ba->i_box & LEFT )
	{
		close_left(ba);
		ba = ba->parent;
		adjust_width(ba);
	}	
	if ( ba->i_box & CENTER )
	{
		close_center(ba);
		ba = ba->parent;
		adjust_width(ba);
	}	
	if ( ba->i_box & TOP )
	{
		close_top(ba);
		ba = ba->parent;
		adjust_width(ba);
	}	
	if ( ba->i_box & BOTTOM )
	{
		close_bottom(ba);
		ba = ba->parent;
		adjust_width(ba);
	}	
	if ( ba->i_box & TILDE)
	{
		close_tilde(ba);
		ba = ba->parent;
		adjust_width(ba);
	}		
	return (ba);
}
int close_subscript(struct box *ba)
{
	struct box *bc = NULL;
	register int i, j;
	
	if (ba == NULL) return 1;
	bc = ba->child[ba->n_box - 1];
	if ( ba->child[0]->i_box & SUM )
	{
		bc->yc = -ba->child[0]->d - (14*bc->h)/10;
		j = ba->child[0]->w;
		if( j < bc->w )
		{
			j = bc->w;
			ba->child[0]->xc = (j - ba->child[0]->w)/2;
			bc->xc = 0;
		}
		else	bc->xc = (j - bc->w)/2;
	}
	else
	{
		bc->yc = -ba->child[0]->d;
		j = ba->child[0]->w + bc->w;
	}
	if ( ( i =  bc->d - bc->yc) > ba->d )	ba->d = i;
	if( ba->n_box  > 2)
	{
		ba->w -= bc->w;
		if (  j > ba->w )	ba->w = j;
	}
	return 0;	
}
int close_exponent(struct box *ba)
{
	struct box *bc = NULL;
	register int i;
	
	if (ba == NULL) return 1;
	bc = ba->child[ba->n_box - 1];
	bc->yc = ba->child[0]->h;
	bc->xc = ba->child[0]->w;	
	if ( ( i = ba->child[0]->h + bc->h) > ba->h )
		ba->h = i;
	if( ba->n_box > 2)
	{
		ba->w -= bc->w;
		if (( i = ba->child[0]->w + bc->w) > ba->w)	ba->w = i;
	}
	return 0;	
}
int close_over(struct box *ba)
{
	struct box *bn = NULL, *bd = NULL, *bt = NULL;
	register int j;
	
	if (ba == NULL) return 1;
	if ((j=((3*ba->pt*box_font.w_white)/64)) != 0)
		j=1;
	bn = ba->child[0];
	bt = ba->child[1];
	bd = ba->child[2];	
	bn->xc = j;
	bt->xc = j;
	bd->xc = j;
	bn->yc = bn->d + (2 * (bn->d + bn->h))/10;
	bd->yc = -bd->h - (2 * (bd->h + bd->d))/10;
	if( bn->w > bd->w )
	{
		bn->w += j;
		bd->xc = j + ((bn->w - bd->w)/2);
		bt->w = bn->w;
		ba->w = bn->w + j;
	}
	else
	{
		bd->w += j;
		bn->xc = j +((bd->w - bn->w)/2);
		bt->w = bd->w;
		ba->w = bd->w + j;
	}
	ba->h = bn->yc + bn->h;
	ba->d = bd->d - bd->yc;
	return 0;	
}
int close_frac(struct box *ba)
{
	struct box *bn = NULL, *bd = NULL, *bt = NULL;
	register int j;
	
	if (ba == NULL) return 1;
	if ((j=((3*ba->pt*box_font.w_white)/64)) != 0)
		j=1;
	bt = ba->child[0];
	bn = ba->child[1];
	bd = ba->child[2];	
	bn->xc = j;
	bt->xc = j;
	bd->xc = j;
	bn->yc = bn->d + (2 * (bn->d + bn->h))/10;
	bd->yc = -bd->h - (2 * (bd->h + bd->d))/10;
	if( bn->w > bd->w )
	{
		bn->w += j;
		bd->xc = j + ((bn->w - bd->w)/2);
		bt->w = bn->w;
		ba->w = bn->w + j;
	}
	else
	{
		bd->w += j;
		bn->xc = j +((bd->w - bn->w)/2);
		bt->w = bd->w;
		ba->w = bd->w + j;
	}
	ba->h = bn->yc + bn->h;
	ba->d = bd->d - bd->yc;
	return 0;	
}
int close_stack(struct box *ba)
{
	struct box *bc = NULL, *bp = NULL, *bt = NULL;
	register int i, j;
	long int w_max=0;
	
	if (ba == NULL) return 1;
	if (ba->n_box == 0)	return 1;
	bt = ba->child[0];
	if ((j=((3*ba->pt*box_font.w_white)/64)) != 0)		j=1;
	for (i=0 ; i< bt->n_box ; i++)
	{
		bc = bt->child[i];
		bc->xc = j;
		w_max = (bc->w > w_max ) ? bc->w : w_max;
		if (i != 0)
		{
			bp = bt->child[i-1];			
			bc->yc = bp->yc - bp->d - bc->h;
			bc->yc -= (2 * (bc->h + bp->d))/6;
		}
	}
	for (i=0 ; i< bt->n_box ; i++)
	{
		bc = bt->child[i];
		bc->xc = j + ((w_max - bc->w)/2);
	}
	bp = bt->child[0];
	bc = bt->child[bt->n_box - 1];	
	ba->w = w_max;
	ba->d = bp->yc - bc->yc + bc->d;
	ba->h = bp->h;
	return 0;	
}

int close_paragraph(struct box *ba)
{
	struct box *bc = NULL, *bt = NULL;
	register int i, j, n;
	int nb_box, last_xc_space = 0, i_space = 0, line_break = 0;
	int	last_line = 0, last_box, n_line, h, d, n_space, fill_space, dx;
	int sp1, sp2, sp3;
	
	if (ba == NULL) return 1;
	if (ba->n_box == 0)	return 1;
	
	h = (long int)(3*ba->pt*(long int)box_font.ch[find_char("H")+1])/16;
	d = (long int)(3*ba->pt*(long int)box_font.ch[find_char("p")+2])/16;
	bt = ba->child[0];

	sp1 = find_char(" ");
	sp2 = find_char("\n");
	sp3 = find_char("\t");
			
	nb_box = bt->n_box;
	if ((j=((3*ba->pt*box_font.w_white)/64)) != 0)		j=1;
/*	printf("para ba->n_bax = %d bc->n_box = %d width %d\n",ba->n_box,bt->n_box,ba->w);*/
	for(last_box = n_line = i_space = last_line = 0 ; last_line == 0; n_line++)
	{
		for (i=last_box, line_break = 0 ; (i< nb_box) && (line_break == 0) ; i++)
		{
			line_break = (bt->child[i]->xc - bt->child[last_box]->xc < line_width) ? 0 : 1;		
/*			if (bt->child[i]->id == SPACE_CHAR)*/
			if (bt->child[i]->n_char == sp1 || bt->child[i]->n_char == sp2 || bt->child[i]->n_char == sp3)
				i_space = (i+1< nb_box) ? i+1 : i;
/*			printf("ch %d type %d %d\n",i,bt->child[i]->id,bt->child[i]->n_char); */
			
		}
		last_line = (i == nb_box) ? 1 : 0;
		i_space = (last_line) ? nb_box : i_space;
		if (n_line)		/* we add a new line */
		{
			bc = attach_child(ba, NULL);
			if (bc == NULL)	return 1;
			bc->yc = ba->child[ba->n_box-2]->yc - (3*(h+d))/2;
/*			printf("line %d space foud at %d yc %d \n",n_line,i_space,bc->yc);*/
			
			last_xc_space = bt->child[last_box]->xc;
			for (i=last_box ; (i< nb_box) && (i < i_space) ; i++)
			{
				bt->child[i]->xc -= last_xc_space;
				attach_child(bc, bt->child[i]);
			}
			ba->d = bc->d + -bc->yc;
		}
		else	/* first line */
		{
			bt->n_box = i_space;
			if (i_space < nb_box)	bt->w = bt->child[i_space]->xc;
			else bt->w = bt->child[i_space-1]->xc + bt->child[i_space-1]->w;
		}
		for (i=last_box, n_space = 0 ; (i< nb_box) && (i < i_space-1) ; i++)
		{
			if (bt->child[i]->n_char == sp1 || bt->child[i]->n_char == sp2 || 
			bt->child[i]->n_char == sp3)	n_space++;
		}
		i = (i_space > 0) ? i_space - 1 : 0;
		dx = fill_space = line_width - bt->child[i]->xc;
		i = (i > 0) ? i-1: 0;
/*		printf("line %d nspace foud at %d fill space %d \n",n_line,n_space,fill_space);*/
		for (n = n_space ; (last_line == 0) && (n_space != 0) && (i< nb_box) && (i >= last_box) ; i--)
		{
			if (bt->child[i]->n_char == sp1 || bt->child[i]->n_char == sp2 || 
			bt->child[i]->n_char == sp3)	
			{
				n--;
				dx = (fill_space*n)/n_space;
/*				printf("box %d nspace foud at %d fill space %d \n",i,n_space,dx);	*/			
			}		
			bt->child[i]->xc += dx;
		}		
		last_box = i_space;
	}
	ba->w = line_width;
	return 0;	
}
int close_centerparagraph(struct box *ba)
{
	struct box *bc = NULL, *bt = NULL;
	register int i, j;
	int nb_box, last_xc_space = 0, i_space = 0, line_break = 0;
	int	last_line = 0, last_box, n_line, h, d;
	int sp1, sp2, sp3;
	
	if (ba == NULL) return 1;
	if (ba->n_box == 0)	return 1;
	
	h = (long int)(3*ba->pt*(long int)box_font.ch[find_char("H")+1])/16;
	d = (long int)(3*ba->pt*(long int)box_font.ch[find_char("p")+2])/16;
	bt = ba->child[0];

	sp1 = find_char(" ");
	sp2 = find_char("\n");
	sp3 = find_char("\t");
			
	nb_box = bt->n_box;
	if ((j=((3*ba->pt*box_font.w_white)/64)) != 0)		j=1;
	for(last_box = n_line = i_space = last_line = 0 ; last_line == 0; n_line++)
	{
		for (i=last_box,line_break = 0 ;(i< nb_box) && (line_break == 0) ; i++)
		{
			line_break = (bt->child[i]->xc - bt->child[last_box]->xc < line_width) ? 0 : 1;		
			if (bt->child[i]->n_char == sp1 || bt->child[i]->n_char == sp2 || bt->child[i]->n_char == sp3)
				i_space = (i+1< nb_box) ? i+1 : i;
		}
		last_line = (i == nb_box) ? 1 : 0;
		i_space = (last_line) ? nb_box : i_space;
/*		printf("line %d space foud at %d\n",n_line,i_space);*/
		if (n_line)		/* we add a new line */
		{
			bc = attach_child(ba, NULL);
			if (bc == NULL)	return 1;
			bc->yc = ba->child[ba->n_box-2]->yc - (3*(h+d))/2;
			last_xc_space = bt->child[last_box]->xc;
			for (i=last_box ; (i< nb_box) && (i < i_space) ; i++)
			{
				bt->child[i]->xc -= last_xc_space;
				attach_child(bc, bt->child[i]);
			}
			ba->d = bc->d + -bc->yc;
			bc->xc = (line_width - bc->w)/2; /* center line */
		}
		else	/* first line */
		{
			bt->n_box = i_space;
			if(i_space < nb_box)
				bt->w = bt->child[i_space]->xc;
			bt->xc = (line_width - bt->w)/2;	/* center line */
		}
		last_box = i_space;
	}
	ba->w = line_width;
	return 0;	
}
int close_leftparagraph(struct box *ba)
{
	struct box *bc = NULL, *bt = NULL;
	register int i, j;
	int nb_box, last_xc_space = 0, i_space = 0, line_break = 0;
	int	last_line = 0, last_box, n_line, h, d;
	int sp1, sp2, sp3;
	
	if (ba == NULL) return 1;
	if (ba->n_box == 0)	return 1;
	
	h = (long int)(3*ba->pt*(long int)box_font.ch[find_char("H")+1])/16;
	d = (long int)(3*ba->pt*(long int)box_font.ch[find_char("p")+2])/16;
	bt = ba->child[0];

	sp1 = find_char(" ");
	sp2 = find_char("\n");
	sp3 = find_char("\t");
			
	nb_box = bt->n_box;
	if ((j=((3*ba->pt*box_font.w_white)/64)) != 0)		j=1;
	for(last_box = n_line = i_space = last_line = 0 ; last_line == 0; n_line++)
	{
		for (i=last_box,line_break = 0 ;(i< nb_box) && (line_break == 0) ; i++)
		{
			line_break = (bt->child[i]->xc - bt->child[last_box]->xc < line_width) ? 0 : 1;		
			if (bt->child[i]->n_char == sp1 || bt->child[i]->n_char == sp2 || bt->child[i]->n_char == sp3)
				i_space = (i+1< nb_box) ? i+1 : i;
		}
		last_line = (i == nb_box) ? 1 : 0;
		i_space = (last_line) ? nb_box : i_space;
/*		printf("line %d space foud at %d\n",n_line,i_space);*/
		if (n_line)		/* we add a new line */
		{
			bc = attach_child(ba, NULL);
			if (bc == NULL)	return 1;
			bc->yc = ba->child[ba->n_box-2]->yc - (3*(h+d))/2;
			last_xc_space = bt->child[last_box]->xc;
			for (i=last_box ; (i< nb_box) && (i < i_space) ; i++)
			{
				bt->child[i]->xc -= last_xc_space;
				attach_child(bc, bt->child[i]);
			}
			ba->d = bc->d + -bc->yc;
		}
		else	/* first line */
		{
			bt->n_box = i_space;
			if(i_space < nb_box)
				bt->w = bt->child[i_space]->xc;
		}
		last_box = i_space;
	}
	ba->w = line_width;
	return 0;	
}
int close_rightparagraph(struct box *ba)
{
	struct box *bc = NULL, *bt = NULL;
	register int i, j;
	int nb_box, last_xc_space = 0, i_space = 0, line_break = 0;
	int	last_line = 0, last_box, n_line, h, d;
	int sp1, sp2, sp3;
	
	if (ba == NULL) return 1;
	if (ba->n_box == 0)	return 1;
	
	h = (long int)(3*ba->pt*(long int)box_font.ch[find_char("H")+1])/16;
	d = (long int)(3*ba->pt*(long int)box_font.ch[find_char("p")+2])/16;
	bt = ba->child[0];

	sp1 = find_char(" ");
	sp2 = find_char("\n");
	sp3 = find_char("\t");
			
	nb_box = bt->n_box;
	if ((j=((3*ba->pt*box_font.w_white)/64)) != 0)		j=1;
	for(last_box = n_line = i_space = last_line = 0 ; last_line == 0; n_line++)
	{
		for (i=last_box,line_break = 0 ;(i< nb_box) && (line_break == 0) ; i++)
		{
			line_break = (bt->child[i]->xc - bt->child[last_box]->xc < line_width) ? 0 : 1;		
			if (bt->child[i]->n_char == sp1 || bt->child[i]->n_char == sp2 || bt->child[i]->n_char == sp3)
				i_space = (i+1< nb_box) ? i+1 : i;
		}
		last_line = (i == nb_box) ? 1 : 0;
		i_space = (last_line) ? nb_box : i_space;
/*		printf("line %d space foud at %d\n",n_line,i_space);*/
		if (n_line)		/* we add a new line */
		{
			bc = attach_child(ba, NULL);
			if (bc == NULL)	return 1;
			bc->yc = ba->child[ba->n_box-2]->yc - (3*(h+d))/2;
			last_xc_space = bt->child[last_box]->xc;
			for (i=last_box ; (i< nb_box) && (i < i_space) ; i++)
			{
				bt->child[i]->xc -= last_xc_space;
				attach_child(bc, bt->child[i]);
			}
			ba->d = bc->d + -bc->yc;
			bc->xc = (line_width - bc->w); /* right line */
		}
		else	/* first line */
		{
			bt->n_box = i_space;
			if(i_space < nb_box)
				bt->w = bt->child[i_space]->xc;
			bt->xc = (line_width - bt->w);	/* right line */
		}
		last_box = i_space;
	}
	ba->w = line_width;
	return 0;	
}
int close_left(struct box *ba)
{
	if (ba == NULL) return 1;
	ba->xc -= ba->w;
	return 0;	
}
int close_fbox(struct box *ba)
{
	struct box *bc = NULL, *bt = NULL;
	register int i, j;
	BX_AR *x = NULL, *y = NULL;
	
	if (ba == NULL || ba->child) return 1;
	bt = ba->child[0];
	if ((j=((ba->pt*box_font.w_white)/16)) == 0)		j=1;
	for (i=0 ; i< bt->n_box ; i++)
	{
		bc = bt->child[i];
		bc->xc += j;
	}
	bt->w += 2*j;
	bt->d += j;
	bt->h += j;	
	ba->w += 2*j;
	ba->d += j;
	ba->h += j;		
	bc = mkpolybox( ba, 5);
	bc->n_box = RE_POLYGONE_BOX;
	x = (BX_AR*)bc->child[0];
	y = (BX_AR*)bc->child[1];	
	x[1] = 2*bt->w + 1;
	x[2] = 1;
	x[3] = - 2*bt->w + 1;
	x[4] = 1;
	y[0] = 2*bt->h;
	y[2] = -2 * (bt->d + bt->h);
	y[4] = -y[2];
	ba->child[0] = bc;
	ba->child[ba->n_box-1] = bt;	
	return 0;	
}
int close_center(struct box *ba)
{
	if (ba == NULL) return 1;
	ba->xc -= ba->w/2;
	return 0;	
}
int close_top(struct box *ba)
{
	if (ba == NULL) return 1;
	ba->yc -= ba->h;
	ba->d +=ba->h;
	ba->h = 0;
	ba->xc -= ba->w/2;
	return 0;	
}
int close_bottom(struct box *ba)
{
	if (ba == NULL) return 1;
	ba->yc += ba->d;
	ba->h +=ba->d;
	ba->d = 0;
	ba->xc -= ba->w/2;	
	return 0;	
}

int close_vec(struct box *ba)
{
	struct box *bf = NULL, *bc = NULL;
	
	if (ba == NULL) return 1;
	bc = ba->child[ba->n_box - 1];
	bf = ba->child[0];	
	set_box_size(bf, ba);
	bf->yc = 14 * bc->h;
	bf->yc /=10;
	bf->yc += bf->d;
	ba->h = bf->yc + bf->h;
	bf->w = bc->w;
	return 0;	
}

int close_tilde(struct box *ba)
{
	register int i, w;
	struct box *bf = NULL, *bc = NULL;
	
	if (ba == NULL) return 1;
	bc = ba->child[ba->n_box - 1];
	bf = ba->child[0];	
	set_box_size(bf, ba);
	bf->yc = 14 * bc->h;
	bf->yc /=10;
	w = bc->w;
	bf->xc = bc->xc;
	for (i = 0; (bf->n_box == POLYGONE_BOX) && (i < bf->n_char); i++)
	{
		((BX_AR*)bf->child[0])[i] = (w * i)/(bf->n_char-1);
		((BX_AR*)bf->child[1])[i] = (int)(0.3*bc->h*sin((M_PI*2*i)/(bf->n_char-1)));	
		((BX_AR*)bf->child[0])[i] *= 2;
		if (i != 0) ((BX_AR*)bf->child[0])[i] += 1;
		((BX_AR*)bf->child[1])[i] *= 2;
	}
	bf->h = bf->d = (3*bc->h)/10;
	bf->yc += bf->d;
	ba->h = bf->yc + bf->h;	
	bf->w = bc->w;
	return 0;	
}

int close_sqrt(struct box *ba)
{
	struct box *bs = NULL, *bc = NULL, *bt = NULL;
	register int i, j;
	
	if (ba == NULL) return 1;
	bc = ba->child[ba->n_box - 1];
	bt = ba->child[0];
	bs = ba->child[1];
	j = bc->h + bc->d;
	j *=13;
	j /=10;
	i = bs->h + bs->d;
	j *= bs->pt;
	if ( j % i )
	{
		j/= i;
		j++;
	}
	else		j/= i;
	bs->pt = j;
	set_box_size(bs, bs);
	bs->yc = bs->d - ((13*bc->d)/10);
	bt->xc = bs->w;
	bt->yc = bs->h + bs->yc;
	bt->w = bc->w;
	bc->xc = bs->w;
	ba->w = bs->w + bc->w;
	ba->h = bt->yc;
	ba->d = bs->d + bs->h - ba->h;
	return 0;	
}

# endif
