/*	program		Try to build an equation, using a simili TeX input
 *	DESCRIPTION	
 *			
 *			
 *
 *	
 *			
 */
# ifndef _BOX_DEF_C_ 
# define _BOX_DEF_C_

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <ctype.h>
#

# ifdef GCC_COMPIL
# include <sys/stat.h>
# include <malloc.h>
# endif



/* # define DEBUG */

# include "xvin/platform.h"
# include "box_def.h"
# include "fops2.h"
# include "color.h"
# include "box2alfn.h"

# ifdef FORTIFY
# include "fortify.h"
# endif

# define NORMAL_CHAR 256
# define SPACE_CHAR 128



/* unsigned char 	*uc, uch;*/


int 		box_cnt = 0;
struct box 	*last_box_found;

/* extern short int pc_fen;*/

int			copy_box_param(struct box *dest, struct box *src);
struct box 	*duplicate_box(struct box *dest, struct box *src);

extern int normalsize_font_height;


int init_font (void)
{
	register int i, j;
	unsigned char uc1, uc2;
	short int *new_t = NULL;

		
	if (box_font.ch != NULL)		return(1);
	box_font.ch = (char *)ufont; /* fops2.h */
	box_font.uch = ufont;
	new_t = (short int *)calloc(AR_SIZE,sizeof(short int));	
	if (box_font.ch == NULL || new_t == NULL)
	{
		fprintf(stderr,"calloc error n");
		exit(1);
	}
	for (i=0, j=0; i< AR_SIZE ; i++, j++)
	{
		uc1 = (short int)box_font.ch[j++];
		uc2 = (short int)box_font.ch[j];
		new_t[i] = 256*uc2 + uc1;
	}
	box_font.new_t = new_t;
	box_font.w_white = (int)box_font.uch[new_t[32]];
	box_font.Max_d = box_font.uch[new_t[112]+2];
/*	box_font.Max_d -= box_font.uch[new_t[65]+2];*/
	
	box_font.Max_h = box_font.uch[new_t[65]+1];
/*	box_font.Max_h += box_font.uch[new_t[65]+2];*/
	
	/*
	box_font.Max_d = box_font.uch[new_t[0]+2];
	box_font.Max_h = box_font.uch[new_t[0]+1];	
	for (i=0; i< FT_SIZE ; i++)
	{
		j = box_font.ch[new_t[i]+2];
		box_font.Max_d = (box_font.Max_d < j) ? j: box_font.Max_d;
		j = box_font.ch[new_t[i]+1];
		box_font.Max_h = (box_font.Max_h < j) ? j: box_font.Max_h;		
	}
	*/
/*	box_font.Max_d = (int)box_font.ch[new_t[65]+2];	
	temp = (short int*)box_font.ch;*/
	return 0;
}

/*	mkpolybox(struct box *b0, int nx);
 *	DESCRIPTION	attach a box of RE_POLYGONE_BOX type to b0 and alloc
 *			nx int space in both x and y array of this box
 *
 *	RETURNS		the pointer to the polygone box on success, else NULL
 *			
 */
struct box *mkpolybox(struct box *b0, int nx)
{
	struct box *bc = NULL;
	BX_AR *x = NULL, *y = NULL;
	
	if (b0 == NULL || nx <= 0)		return NULL;
	bc = mknewbox(b0);
	if (bc == NULL)				return NULL;
	bc->n_box = POLYGONE_BOX;
	bc->child = (struct box **)calloc(3,sizeof(struct box*));
	if (bc->child == NULL)	    return NULL;
	x = (BX_AR *)calloc(nx,sizeof(BX_AR));
	if (x == NULL)		return NULL;
	y = (BX_AR *)calloc(nx,sizeof(BX_AR));	
	if (y == NULL)		
	  {
	    free(x);
	    return NULL;
	  }
	bc->n_char = nx;	
	bc->child[0] = (struct box*)x;
	bc->child[1] = (struct box*)y;	
	bc->child[2] = NULL;
	return bc;
}
/*	init_box (b0)
 *	DESCRIPTION	
 *			
 *			
 *
 *	RETURNS		0 on success, 1 
 *			
 */
int init_box (struct box *b0)
{
	if (b0 == NULL) return 1;
	b0->xc=0;
	b0->yc=0;
	b0->slx=0;
	b0->w=0;	
	b0->h=0;
	b0->d=0;
	b0->sly=1;
	b0->pt = 10;
	b0->color=1;	
	b0->n_hic=0;
	b0->n_box=0;
	b0->n_char=0;	
	b0->i_box=0;
	b0->id = 0;	
	b0->char_val=0;
	b0->child=NULL;
	if ( b0->parent == NULL)
        b0->parent = b0;
	return 0;
}
int	copy_box_param(struct box *dest, struct box *src)
{
	if (src == NULL || src == NULL)	return -1;
	dest->n_box = (src->n_box < 0) ? src->n_box : dest->n_box;
	dest->xc = src->xc;
	dest->yc = src->yc;
	dest->slx = src->slx;
	dest->w = src->w;	
	dest->h = src->h;
	dest->d = src->d;
	dest->sly = src->sly;
	dest->pt = src->pt;
	dest->color = src->color;	
/*	dest->n_hic = src->n_hic;	*/
	dest->n_char = src->n_char;	
	dest->i_box = src->i_box;
	dest->id = src->id;	
	dest->char_val = src->char_val;
	return 0;
}
struct box *duplicate_box(struct box *dest, struct box *src)
{
	register int i;
	int hic_start;
	struct box *bc = NULL, *ba = NULL, *da = NULL, *dc = NULL;
	BX_AR *x = NULL, *y = NULL;
	
	if (src == NULL)	return NULL;
	if (dest != NULL)	free_box(dest);
	else
	{
		dest = (struct box *)calloc( 1,sizeof(struct box));
		if ( dest == NULL )		return NULL;
		init_box(dest);
	} 	
	ba = src;
	ba->i_box = 0;
	hic_start = ba->n_hic;
	da = dest;
	
	if (ba->n_box < 0)
	{
		copy_box_param(dest,ba);
		if (ba->n_box == TURN_90)
		{
			dest->child = (struct box **)calloc(1,sizeof(struct box*));
			if (dest->child == NULL)			return NULL;
			if (ba->child[0] != NULL)
			{
				bc = duplicate_box(NULL,ba->child[0]);
				if (bc == NULL)		return NULL;
				bc->n_hic = dest->n_hic + 1;
				dest->child[0] = bc;
				rearrange_hic(dest->child[0]);
			}
		}
		else if (ba->n_box == RE_POLYGONE_BOX 
			|| ba->n_box == POLYGONE_BOX
			|| ba->n_box == CLIPED_RE_POLYGONE_BOX 
			|| ba->n_box == CLIPED_POLYGONE_BOX)
/*			|| ba->n_box == FILL_RE_POLYGONE_BOX 
			|| ba->n_box == FILL_POLYGONE_BOX
			|| ba->n_box == FILL_CLIPED_RE_POLYGONE_BOX
			|| ba->n_box == FILL_CLIPED_POLYGONE_BOX)*/
		{
			dest->child = (struct box **)calloc(3,sizeof(struct box*));
			if (dest->child == NULL || ba->n_char <= 0)    return NULL;
			x = (BX_AR *)calloc(ba->n_char,sizeof(BX_AR));
			y = (BX_AR *)calloc(ba->n_char,sizeof(BX_AR));
			if (x == NULL || y == NULL)		return NULL;
			dest->child[0] = (struct box*)x;
			dest->child[1] = (struct box*)y;	
			for (i = 0; i < ba->n_char; i++)
			{
				x[i] = ((BX_AR*)ba->child[0])[i];
				y[i] = ((BX_AR*)ba->child[1])[i];
			}
			if (ba->child[2] != NULL)
			{
				bc = duplicate_box(NULL,ba->child[2]);
				if (bc == NULL)		return NULL;
				dest->child[2] = bc;
				bc->n_hic = dest->n_hic + 1;
				rearrange_hic(dest->child[2]);
			}
		}
	}	
	while(ba->n_hic > hic_start || ba->i_box < ba->n_box)
	{
		if ( ba->i_box == ba->n_box )	/* all childs printed */
		{
			bc = ba;
			ba= ba->parent;
			da = da->parent;
			ba->i_box++;
		}
		else if ( ba->child[ba->i_box]->n_box >= 0 )	/* one step down */
		{
			ba = ba->child[ba->i_box];
			bc = mknewbox(da);
			if (bc == NULL)		return NULL;
			copy_box_param(bc,ba);
			da = bc;
			ba->i_box = 0;
		}
		else if ( ba->child[ba->i_box]->n_box == CHAR_BOX )
		{
			bc = mknewbox(da);
			if (bc == NULL)		return NULL;
			copy_box_param(bc,ba->child[ba->i_box]);
			ba->i_box++;
		}
		else if ( ba->child[ba->i_box]->n_box == RE_POLYGONE_BOX 
			|| ba->child[ba->i_box]->n_box == POLYGONE_BOX 
			|| ba->child[ba->i_box]->n_box == CLIPED_RE_POLYGONE_BOX 
			|| ba->child[ba->i_box]->n_box == CLIPED_POLYGONE_BOX)
/*			
			|| ba->child[ba->i_box]->n_box == FILL_RE_POLYGONE_BOX 
			|| ba->child[ba->i_box]->n_box == FILL_POLYGONE_BOX
			|| ba->child[ba->i_box]->n_box == FILL_CLIPED_RE_POLYGONE_BOX
			|| ba->child[ba->i_box]->n_box == FILL_CLIPED_POLYGONE_BOX)*/
		{
			bc = mkpolybox(da,ba->child[ba->i_box]->n_char);
			if (bc == NULL)		return NULL;
			copy_box_param(bc,ba->child[ba->i_box]);
			x = (BX_AR*)(ba->child[ba->i_box]->child[0]);
			y = (BX_AR*)(ba->child[ba->i_box]->child[1]);
			for (i = 0; i < ba->child[ba->i_box]->n_char; i++)
			{
				((BX_AR*)bc->child[0])[i] = x[i];
				((BX_AR*)bc->child[1])[i] = y[i];
			}
			if (ba->child[ba->i_box]->child[2] != NULL)
			{
				dc = duplicate_box(NULL,ba->child[ba->i_box]->child[2]);
				if (dc == NULL)		return NULL;
				bc->child[2] = dc;
				dc->n_hic = bc->n_hic + 1;
				rearrange_hic(dc);
			}
			ba->i_box++;
		}
		else if ( ba->child[ba->i_box]->n_box == TURN_90 )
		{
			bc = mknewbox(da);
			if (bc == NULL)		return NULL;
			copy_box_param(bc,ba->child[ba->i_box]);
			bc->child = (struct box **)calloc(1,sizeof(struct box*));
			if (bc->child == NULL)			return NULL;
			if (ba->child[ba->i_box]->child[0] != NULL)
			{
				dc = duplicate_box(NULL,ba->child[ba->i_box]->child[0]);
				if (dc == NULL)		return NULL;
				bc->child[0] = dc;
				dc->n_hic = bc->n_hic + 1;
				rearrange_hic(dc);
			}
			ba->i_box++;
		}
	}
	copy_box_param(dest,src);
	return dest;
}
/*	free_box(b0);
 *	DESCRIPTION	Free memory allocated for all child boxes
 *			
 *			
 *
 *	RETURNS		0 on success, 1 
 *			
 */
int free_box(struct box *b0)
{
	int hic_start;
	struct box *bc = NULL, *ba = NULL;
	
	if (b0 == NULL)		return 1;
	ba = b0;
	ba->i_box = 0;
	hic_start = ba->n_hic;
	
	if (ba->n_box < 0)
	{
		if (ba->n_box == TURN_90)
		{
			if (ba->child[0] != NULL)	
			{
				free_box(ba->child[0]);
				free(ba->child[0]);
				ba->child[0] = NULL;
			}
		}
		else if (ba->n_box == RE_POLYGONE_BOX 
			|| ba->n_box == POLYGONE_BOX
			|| ba->n_box == CLIPED_RE_POLYGONE_BOX 
			|| ba->n_box == CLIPED_POLYGONE_BOX)
/*			|| ba->n_box == FILL_RE_POLYGONE_BOX 
			|| ba->n_box == FILL_POLYGONE_BOX
			|| ba->n_box == FILL_CLIPED_RE_POLYGONE_BOX
			|| ba->n_box == FILL_CLIPED_POLYGONE_BOX)			*/
		{
			if (ba->child[0] != NULL)	
			  {
			    free(ba->child[0]);
			    ba->child[0] = NULL;
			  }
			if (ba->child[1] != NULL)
			  {	
			    free(ba->child[1]);
			    ba->child[1] = NULL;
			  }
			if (ba->child[2] != NULL)
			{
				free_box(ba->child[2]);
				free(ba->child[2]);			
				ba->child[2] = NULL;
			}
		}
	}	
	
	while(ba->n_hic > hic_start || ba->i_box < ba->n_box)
	{
		if ( ba->i_box == ba->n_box )	/* all childs printed */
		{
			bc = ba;
#ifdef DEBUG
			printf("f%d->%d\n",ba->n_hic,ba->i_box-1);
			box_cnt--;
#endif			
			ba= ba->parent;
			if (bc->child != NULL)	
			  {
			    free(bc->child);
			    bc->child = NULL;
			  }
			if (bc != NULL)	
			  {
			    free(bc);
			    bc = NULL;
			  }
			ba->i_box++;
		}
		else if ( ba->child[ba->i_box]->n_box >= 0 )	/* one step down */
		{
			ba = ba->child[ba->i_box];
			ba->i_box = 0;
		}
		else if ( ba->child[ba->i_box]->n_box == CHAR_BOX )
		{
#ifdef DEBUG
			printf("f%d->%d\n",ba->n_hic,ba->i_box);
			box_cnt--;			
#endif
			if (ba->child[ba->i_box] != NULL)	
			  {
			    free(ba->child[ba->i_box]);
			    ba->child[ba->i_box] = NULL;
			  }
			ba->i_box++;
		}
		else if ( ba->child[ba->i_box]->n_box == RE_POLYGONE_BOX 
			|| ba->child[ba->i_box]->n_box == POLYGONE_BOX 
			|| ba->child[ba->i_box]->n_box == CLIPED_RE_POLYGONE_BOX 
			|| ba->child[ba->i_box]->n_box == CLIPED_POLYGONE_BOX)
/*			|| ba->child[ba->i_box]->n_box == FILL_RE_POLYGONE_BOX 
			|| ba->child[ba->i_box]->n_box == FILL_POLYGONE_BOX
			|| ba->child[ba->i_box]->n_box == FILL_CLIPED_RE_POLYGONE_BOX
			|| ba->child[ba->i_box]->n_box == FILL_CLIPED_POLYGONE_BOX)		*/	
		{
		  if (ba->child[ba->i_box]->child != NULL) // apparent bug
		    {
			if (ba->child[ba->i_box]->child[0] != NULL)
			  {
			    free(ba->child[ba->i_box]->child[0]);
			    ba->child[ba->i_box]->child[0] = NULL;
			  }
			if (ba->child[ba->i_box]->child[1] != NULL)
			  {
			    free(ba->child[ba->i_box]->child[1]);
			    ba->child[ba->i_box]->child[1] = NULL;
			  }
			if (ba->child[ba->i_box]->child[2] != NULL)
			{
				free_box(ba->child[ba->i_box]->child[2]);
				free(ba->child[ba->i_box]->child[2]);
				ba->child[ba->i_box]->child[2] = NULL;				
			}
		    }
#ifdef DEBUG
			printf("f%d->%d\n",ba->n_hic,ba->i_box);
			box_cnt--;
#endif
			if (ba->child[ba->i_box]->child != NULL)
			  {
			    free(ba->child[ba->i_box]->child);
			    ba->child[ba->i_box]->child = NULL;
			  }
			if (ba->child[ba->i_box] != NULL)
				free(ba->child[ba->i_box]);
			ba->child[ba->i_box] = NULL;
			ba->i_box++;
		}
		else if ( ba->child[ba->i_box]->n_box == TURN_90 )
		{
		  if (ba->child[ba->i_box]->child != NULL) // apparent bug
		    {
			free_box(ba->child[ba->i_box]->child[0]);
			if (ba->child[ba->i_box]->child[0] != NULL)
			  {
			    free(ba->child[ba->i_box]->child[0]);	  
			    ba->child[ba->i_box]->child[0] = NULL;
			  }
		    }
		  if (ba->child[ba->i_box]->child != NULL)
		    {
		      free(ba->child[ba->i_box]->child);
		      ba->child[ba->i_box]->child = NULL;
		    }
		  if (ba->child[ba->i_box] != NULL)
		    {
		      /*				free_box(ba->child[ba->i_box]);	*/
		      free(ba->child[ba->i_box]);
		    }
		  ba->child[ba->i_box] = NULL;
		  ba->i_box++;
		}
		
	}
	if (ba->child != NULL)		free(ba->child);	
	ba->child = NULL;
	ba->n_box = 0;
	ba->w = 0;
	ba->d = 0;
	ba->h = 0;
	ba->xc = 0;
	ba->yc = 0;
	ba->parent = ba;
	ba->n_hic = hic_start;
	ba->i_box = 0;	
	ba->slx = 0;
	ba->sly = 1;
	ba->id = 1;	
	return 0;
}

struct box * attach_child(struct box *parent, struct box *child)
{
	if ( parent == NULL)		return NULL;
	if ( parent->n_box <0)
	{
		snprintf (eqs_err, 128, "negative box error \n");
		return NULL;
	}
	parent->n_box++;
	if ( parent->n_box == 1)
		parent->child=(struct box **)calloc(parent->n_box,sizeof(struct box*));
	else
		parent->child=(struct box **)realloc(parent->child, parent->n_box*sizeof(struct box*));
	if (parent->child == NULL) 	return NULL;
	if (child == NULL)	
	{
		child = (struct box *)calloc( 1,sizeof(struct box));
		if (child == NULL)		return NULL;
		child->n_box = 0;
		child->w = 0;
		child->d = 0;
		child->h = 0;
		child->xc = 0;
		child->yc = 0;
		child->pt = parent->pt;
		child->color = parent->color;
		child->i_box = 0;	
		child->n_char = 0;		
		child->slx = parent->slx;
		child->sly = parent->sly;
		child->id = 0;	
		box_cnt++;
	}	
	parent->child[parent->n_box - 1] = child;
	child->parent = parent;
	child->n_hic = parent->n_hic + 1;
	
	parent->w = child->w + child->xc;
	if (child->h + child->yc > parent->h)	parent->h = child->h + child->yc;
	if (child->d - child->yc > parent->d)	parent->d = child->d - child->yc;	
	
/*
# ifdef DEBUG
	printf("m%d->%d\n",parent->n_hic,parent->n_box-1);
# endif
*/
	return (child);
}


/* struct box *locate_box(struct box *b0,int id,int *x0,int *x1,int *y0,int *y1);
 * struct box *locate_box_90(struct box *b0,int id,int *x0,int *x1,int *y0,int *y1);
 *
 *	DESCRIPTION	try to find a box in the box tree b0, having the
 *			identifier id, if b0->n_hic == 0 the origin is set
 *			to 0, otherwise the position is relative to the 
 *			original values of x0, y0. The mode indicatate
 *			first box or next one.
 *	RETURNS		the ptr on success, NULL on box not found
 *			
 */
struct box *locate_box(struct box *b0, int id, int *x0, int *x1, int *y0, int *y1, int mode)
{
	int hic_start, not_found = 1, iter=0, scanning = 1;
	int tx, ty, next_one = 0;
	struct box *bc = NULL, *ba = NULL, *lb = NULL;
	
	if (b0 == NULL) return NULL;
	ba = b0;
	ba->i_box = 0;
	hic_start = ba->n_hic;
	tx = *x0;
	ty = *y0;	
	if ( hic_start == 0)
	{
		tx = 0;
		ty = 0;
	}
	tx += ba->xc;
	ty += ba->yc;
	if (mode != FIRST && mode != NEXT)	mode = FIRST;
	if (mode == FIRST)		last_box_found = NULL;
	if (ba->n_box < 0)
	{
		if (ba->n_box == TURN_90)
		{
			bc = ba->child[ba->i_box];
			if (ba->id == id)
			{
				if ( mode == FIRST)		not_found = 0;
				else if (ba == last_box_found)	next_one = 1;
				else if (next_one && ba != last_box_found)
					not_found = 0;
			}
			else
			{
				lb = last_box_found;
				not_found = (locate_box_90(bc,id,x0,x1,y0,y1,mode) == NULL) ? 1 : 0;
				last_box_found = lb;
			}
		}
		if (ba->n_box == RE_POLYGONE_BOX  
			|| ba->n_box == CLIPED_RE_POLYGONE_BOX 
			|| ba->n_box == POLYGONE_BOX 
			|| ba->n_box == CLIPED_POLYGONE_BOX 
			|| ba->n_box == CHAR_BOX)
/*			|| ba->n_box == FILL_RE_POLYGONE_BOX 
			|| ba->n_box == FILL_POLYGONE_BOX
			|| ba->n_box == FILL_CLIPED_RE_POLYGONE_BOX
			|| ba->n_box == FILL_CLIPED_POLYGONE_BOX)		*/
		{
			if (ba->id == id)
			{
				if ( mode == FIRST)		not_found = 0;
				else if (ba == last_box_found)	next_one = 1;
				else if (next_one && ba != last_box_found)
					not_found = 0;
			}
		}
	}
	while (not_found && scanning )
	{
		iter++;
		if (ba->id == id && ba->i_box < ba->n_box)
		{
			if ( mode == FIRST)		not_found = 0;
			else if (ba == last_box_found)	next_one = 1;
			else if (next_one && ba != last_box_found)
				not_found = 0;
			if (not_found) 	ba->i_box++;
		}
		else if ( ba->i_box >= ba->n_box) /* all childs printed */
		{
			if (ba->n_hic <=  hic_start)	scanning = 0;
			else 
			{
				tx -= ba->xc;
				ty -= ba->yc;
				ba= ba->parent;
				ba->i_box++;
			}
		}
		else if ( ba->child[ba->i_box]->n_box >= 0 )
		{	/* one step down */
			ba = ba->child[ba->i_box];
			if (ba->id == id)
			{
				if ( mode == FIRST)		not_found = 0;
				else if (ba == last_box_found)	next_one = 1;
				else if (next_one && ba != last_box_found)
					not_found = 0;
			}
			if (not_found)
			{
				ba->i_box = 0;
				tx += ba->xc;
				ty += ba->yc;
			}
		}
		else if ( ba->child[ba->i_box]->n_box == TURN_90 )
		{
			bc = ba->child[ba->i_box];
			if (bc->id == id)
			{
				if ( mode == FIRST)		not_found = 0;
				else if (bc == last_box_found)	next_one = 1;
				else if (next_one && bc != last_box_found)
					not_found = 0;
				if (not_found) 	ba->i_box++;
				else 		ba = bc;
			}
			else
			{
				lb = last_box_found;			
				not_found = (locate_box_90(bc,id,x0,x1,y0,y1,mode) == NULL) ? 1 : 0;
				last_box_found = lb;
				ba->i_box++;
			}
		}		
		else if ( ba->child[ba->i_box]->n_box == CHAR_BOX 
			|| ba->child[ba->i_box]->n_box == RE_POLYGONE_BOX 
			|| ba->child[ba->i_box]->n_box == POLYGONE_BOX 
			|| ba->child[ba->i_box]->n_box == CLIPED_RE_POLYGONE_BOX 
			|| ba->child[ba->i_box]->n_box == CLIPED_POLYGONE_BOX)
/*			|| ba->child[ba->i_box]->n_box == FILL_RE_POLYGONE_BOX 
			|| ba->child[ba->i_box]->n_box == FILL_POLYGONE_BOX
			|| ba->child[ba->i_box]->n_box == FILL_CLIPED_RE_POLYGONE_BOX
			|| ba->child[ba->i_box]->n_box == FILL_CLIPED_POLYGONE_BOX)	*/			
		{
			if (ba->child[ba->i_box]->id == id)
			{
				bc = ba->child[ba->i_box];
				if ( mode == FIRST)		not_found = 0;
				else if (bc == last_box_found)	next_one = 1;
				else if (next_one && bc != last_box_found)
					not_found = 0;
				if (not_found) 	ba->i_box++;
				else 		ba = bc;
			}
			else ba->i_box++;			
		}
		if (not_found && ba->id == id)	
		{
/*			ba = ba->child[ba->i_box];*/
			if ( mode == FIRST)		not_found = 0;
			else if (ba == last_box_found)	next_one = 1;
			else if (next_one && ba != last_box_found)
				not_found = 0;
		}
/*
		if (iter > 1000)
		{
			fprintf (stderr, "lacate hic %d i_box %d n_box %d   \n",ba->n_hic,ba->i_box, ba->n_box);
		}
*/
	}
	if (not_found)		
	{
		last_box_found = NULL;
		return NULL;
	}
	if (ba != b0)
	{
		tx += ba->xc;
		ty += ba->yc;
	}
	*x0 = tx;
	*x1 = tx + ba->w;
	*y0 = ty - ba->d;
	*y1 = ty + ba->h;
	last_box_found = ba;
	return ba;
}
struct box *locate_box_90(struct box *b0, int id, int *x0, int *x1, int *y0, int *y1, int mode)
{
	int hic_start, not_found = 1, scanning = 1;
	int tx, ty, next_one = 0;
	struct box *bc = NULL, *ba = NULL;
	
	if (b0 == NULL) return NULL;
	ba = b0;
	ba->i_box = 0;
	hic_start = ba->n_hic;
	tx = *x0;
	ty = *y0;	
	if ( hic_start == 0)
	{
		tx = 0;
		ty = 0;
	}
	tx += ba->xc;
	ty += ba->yc;
	if (mode != FIRST && mode != NEXT)	mode = FIRST;
	if (mode == FIRST)		last_box_found = NULL;
	if (ba->n_box < 0)	
		if (ba->id == id && last_box_found != ba)	not_found = 0;
	while (not_found && scanning)
	{
		if (ba->id == id && ba->i_box < ba->n_box)
		{
			if ( mode == FIRST)		not_found = 0;
			else if (ba == last_box_found)	next_one = 1;
			else if (next_one && ba != last_box_found)
				not_found = 0;
			if (not_found) 	ba->i_box++;
		}
		else if (ba->i_box >= ba->n_box)  /* all childs printed */
		{
			if (ba->n_hic <=  hic_start)	scanning = 0;
			else 
			{
				tx += ba->yc;
				ty -= ba->xc;
				ba= ba->parent;
				ba->i_box++;
			}
		}
		else if ( ba->child[ba->i_box]->n_box >= 0 )	
		{	/* one step down */
			ba = ba->child[ba->i_box];
			if (ba->id == id)
			{			
				if ( mode == FIRST)		not_found = 0;
				else if (ba == last_box_found)	next_one = 1;
				else if (next_one && ba != last_box_found)
					not_found = 0;
			}
			if (not_found)
			{
				ba->i_box = 0;
				tx -= ba->yc;
				ty += ba->xc;
			}
		}
		else if (ba->child[ba->i_box]->n_box < 0)
		{
			if (ba->child[ba->i_box]->id == id)
			{
				bc = ba->child[ba->i_box];
				if ( mode == FIRST)		not_found = 0;
				else if (bc == last_box_found)	next_one = 1;
				else if (next_one && bc != last_box_found)
					not_found = 0;
				if (not_found) 	ba->i_box++;
				else 		ba = bc;
			}
			else ba->i_box++;			
		}
	}
	if (not_found)		
	{
		return NULL;
		last_box_found = NULL;
	}
	if (ba != b0)
	{
		tx -= ba->yc;
		ty += ba->xc;
	}
	*x0 = tx - ba->h;
	*x1 = tx + ba->d;
	*y0 = ty;
	*y1 = ty + ba->w;
	last_box_found = ba;
	return ba;	
}

int	where_is_box(struct box *b0, int id, int *x0, int *x1, int *y0, int *y1)
{
	int hic_start, not_found = 1, iter=0, scanning = 1;
	int tx, ty;
	struct box *bc = NULL, *ba = NULL;
	
	if (b0 == NULL) return 1;
	ba = b0;
	ba->i_box = 0;
	hic_start = ba->n_hic;
	tx = *x0;
	ty = *y0;	
	if ( hic_start == 0)		tx = ty = 0;
	tx += ba->xc;
	ty += ba->yc;
	if (ba->n_box < 0)
	{
		if (ba->n_box == TURN_90)
		{
			bc = ba->child[ba->i_box];
			not_found = where_is_box_90(bc,id,x0,x1,y0,y1);
		}
		if (ba->n_box == RE_POLYGONE_BOX || 
			ba->n_box == POLYGONE_BOX || ba->n_box == CHAR_BOX)
			not_found = (ba->id == id) ? 0 : 1;
	}
	while (not_found && scanning )
	{
		iter++;
		if (ba->id == id && ba->i_box < ba->n_box)
		{
			not_found = 0;
		}
		else if ( ba->i_box >= ba->n_box) /* all childs printed */
		{
			if (ba->n_hic <=  hic_start)	scanning = 0;
			else 
			{
				tx -= ba->xc;
				ty -= ba->yc;
				ba= ba->parent;
				ba->i_box++;
			}
		}
		else if ( ba->child[ba->i_box]->n_box >= 0 )
		{	/* one step down */
			ba = ba->child[ba->i_box];
			if (ba->id == id)		not_found = 0;
			if (not_found)
			{
				ba->i_box = 0;
				tx += ba->xc;
				ty += ba->yc;
			}
		}
		else if ( ba->child[ba->i_box]->n_box == TURN_90 )
		{
			bc = ba->child[ba->i_box];
			not_found = where_is_box_90(bc,id,x0,x1,y0,y1);
			ba->i_box++;
		}		
		else if ( ba->child[ba->i_box]->n_box == CHAR_BOX 
			|| ba->child[ba->i_box]->n_box == RE_POLYGONE_BOX 
			|| ba->child[ba->i_box]->n_box == POLYGONE_BOX )
		{
			if (ba->child[ba->i_box]->id == id)
			{
				ba = bc = ba->child[ba->i_box];
				not_found = 0;
			}
			else ba->i_box++;			
		}
		if (not_found && ba->id == id)		not_found = 0;
	}
	if (not_found)				return 1;
	if (ba != b0)
	{
		tx += ba->xc;		ty += ba->yc;
	}
	*x0 = tx;		*x1 = tx + ba->w;
	*y0 = ty - ba->d;	*y1 = ty + ba->h;
	return 0;
}
int	where_is_box_90(struct box *b0, int id, int *x0, int *x1, int *y0, int *y1)
{
	int hic_start, not_found = 1, scanning = 1;
	int tx, ty;
	struct box *bc = NULL, *ba = NULL;
	
	if (b0 == NULL) return 1;
	ba = b0;
	ba->i_box = 0;
	hic_start = ba->n_hic;
	tx = *x0;
	ty = *y0;	
	if ( hic_start == 0)		tx = ty = 0;
	tx += ba->xc;			ty += ba->yc;
	if (ba->n_box < 0 && ba->id == id)	not_found = 0;
	while (not_found && scanning)
	{
		if (ba->id == id && ba->i_box < ba->n_box)	not_found = 0;
		else if (ba->i_box >= ba->n_box)  /* all childs printed */
		{
			if (ba->n_hic <=  hic_start)	scanning = 0;
			else 
			{
				tx += ba->yc;
				ty -= ba->xc;
				ba= ba->parent;
				ba->i_box++;
			}
		}
		else if ( ba->child[ba->i_box]->n_box >= 0 )	
		{	/* one step down */
			ba = ba->child[ba->i_box];
			if (ba->id == id)		not_found = 0;
			if (not_found)
			{
				ba->i_box = 0;
				tx -= ba->yc;
				ty += ba->xc;
			}
		}
		else if (ba->child[ba->i_box]->n_box < 0)
		{
			if (ba->child[ba->i_box]->id == id)
			{
				ba = bc = ba->child[ba->i_box];
				not_found = 0;
			}
			else ba->i_box++;			
		}
	}
	if (not_found)		return 1;
	if (ba != b0)
	{
		tx -= ba->yc;		ty += ba->xc;
	}
	*x0 = tx - ba->h;	*x1 = tx + ba->d;
	*y0 = ty;		*y1 = ty + ba->w;
	return 0;	
}


/*	find_char(ch)
 *	DESCRIPTION	
 *			
 *			
 *
 *	RETURNS		0 on success, 1 
 *			
 */
short int find_char(char *ch)
{
	register int j;
	unsigned char *uc = NULL;
	short int  nch; /* *temp, */
	char *bt = NULL;
		
	if (ch == NULL) return 0;
	if (box_font.new_t == NULL) 
	  {
	    allegro_message("No font found!");
	    return 0;
	  }
	uc = (unsigned char *)ch;
	if ( ch[0] == 92 )
	{
		j=0;
		bt = box_font.ch + box_font.new_t[FT_SIZE+j];
		nch = box_font.new_t[FT_SIZE+j];
		while ( strcmp(ch+1,bt) != 0 && j < (AR_SIZE-FT_SIZE) )
		{
			j++;
			bt = box_font.ch + box_font.new_t[FT_SIZE+j];
			nch = box_font.new_t[FT_SIZE+j];
		}
		bt += strlen(ch);
		nch += strlen(ch);
		if ( j == 128 && strlen(ch) > 2 )
		{
			nch = 0;
		}
		if ( strlen(ch) == 2 )
			nch = box_font.new_t[(int)ch[1]];
	}
	else if ( strlen(ch) == 1 )		nch =  box_font.new_t[(int)uc[0]];
	else					nch = 0;
	return nch;
}
int rearrange_hic(struct box *b0)
{
	struct box *ba = NULL;
	
	if (b0 == NULL) return 1;
	ba = b0;
	if ( ba->n_box < 0 )		return 1;
	ba->n_char = 0;
	while (ba->n_hic != b0->n_hic || ba->n_char < ba->n_box )
	{
		if ( ba->n_char == ba->n_box )	/* all childs printed */
		{
			ba= ba->parent;
			ba->n_char++;
		}
		else if ( ba->child[ba->n_char]->n_box >= 0 )	/* one step down */
		{
			ba->child[ba->n_char]->parent = ba;
			ba->child[ba->n_char]->n_hic = ba->n_hic + 1;
			ba = ba->child[ba->n_char];
			ba->n_char = 0;
		}
		else if ( ba->child[ba->n_char]->n_box < 0 )
		{
			ba->child[ba->n_char]->parent = ba;
			ba->child[ba->n_char]->n_hic = ba->n_hic + 1;
			ba->n_char++;
		}
	}
	return 0;	
}

/*	set_box_size(bc, ba);
 *	DESCRIPTION	Set the size of the last child of "ba" according 
 *			to the char used and the point size etc. of ba
 *			
 *
 *	RETURNS		nothing
 *			
 */
#ifdef OLD
int set_box_size(struct box *bc, struct box *ba)
{

	bc->w = (int)(3*ba->pt*(int)box_font.uch[bc->n_char])/16;
	bc->w += (3*ba->pt*ba->slx*(int)box_font.ch[bc->n_char+1])/(16*ba->sly);
	bc->w += (3*ba->pt*ba->slx*(int)box_font.ch[bc->n_char+2])/(16*ba->sly);
	bc->h = (int)(3*ba->pt*(int)box_font.ch[bc->n_char+1])/16;
	bc->d = (int)(3*ba->pt*(int)box_font.ch[bc->n_char+2])/16;
	return 0;	
}

# endif
int set_box_size(struct box *bc, struct box *ba)
{

	if (bc == NULL || ba == NULL) return 1;	
	bc->h = (int)(3*ba->pt*(int)box_font.ch[bc->n_char+1])/16;
	bc->d = (int)(3*ba->pt*(int)box_font.ch[bc->n_char+2])/16;	
	if(ttf_enable && bc->char_val) bc->w = give_ttf_char_width(bc);
	else
	{
		bc->w = (int)(3*ba->pt*(int)box_font.uch[bc->n_char])/16;
		bc->w += (3*ba->pt*ba->slx*(int)box_font.ch[bc->n_char+1])/(16*ba->sly);
		bc->w += (3*ba->pt*ba->slx*(int)box_font.ch[bc->n_char+2])/(16*ba->sly);
	}
	return 0;	
}

/*	attach_ch(ch, ba);
 *	DESCRIPTION	attach the char "ch" to the box created as the last 
 *			child of ba uses the find_char function to scan the 
 *			font
 *
 *	RETURNS		nothing
 *			
 */
# undef attach_ch
struct box *attach_ch(char *ch, struct box *ba)
{
	struct box *bc = NULL;
		
	if (ba == NULL) return NULL;
	if (mknewbox(ba) == NULL)		return NULL;
	bc = ba->child[ba->n_box-1];
	bc->n_char = find_char(ch);
	bc->n_box = CHAR_BOX;
	bc->char_val = (ch[0] != 92 && strlen(ch) == 1) ? ch[0] : 0;
	return bc;
}

/*	struct box *set_origin(ba);
 *	DESCRIPTION	Set the origin of the last child of "ba"
 *			
 *			
 *
 *	RETURNS		the pointer of this last child
 *			
 */

struct box *set_origin(struct box *ba)
{
	register int j;
	struct box *bc = NULL, *bc2 = NULL;
	
	if ( ba == NULL )
	{
		fprintf (stderr, "pb set origin   \n");
		return NULL;
	}
	if ( ba->n_box < 1 || ba->child == NULL)
	{
		fprintf (stderr, "pb set origin   \n");
		return NULL;
	}	
	bc = ba->child[ba->n_box-1];
	if (ba->n_box > 1 )
	{
		bc2 = ba->child[ba->n_box-2];
		bc->xc = bc2->xc;
		bc->xc += bc2->w;
		bc->yc = bc2->yc;
	}
	else if ( (bc->i_box & (LEFT+CENTER+BOTTOM+TOP)) == 0)
	{
		if (ttf_enable && give_ttf_char_space(bc))
		{
			if ((j=((ba->pt*box_font.w_white)/16)) != 0)	bc->xc+=j;
			else					bc->xc+=1;
		}
		bc->yc += 3*d_off/16;
	}
	return(bc);
}

/*	adjust_width(ba)
 *	DESCRIPTION	adjust the size of box "ba" by the size of its last
 *			child
 *			
 *
 *	RETURNS		
 *			
 */
int adjust_width(struct box *ba)
{
	register int j, i;
	struct box *bc = NULL;

	if (ba == NULL || ba->n_box <= 0 || ba->child == NULL)	return 1;
	bc = ba->child[ba->n_box-1];
	i = (ttf_enable) ? give_ttf_char_space(bc) : 1;
	if ((bc->i_box & (TOP+BOTTOM+CENTER+LEFT)) == 0 && i == 1)
	{
		if ((j=((ba->pt*box_font.w_white)/32)) != 0)	
		{
			bc->w += j;
			bc->xc += j;
		}
		else					
		{
			bc->w += 1;
			bc->xc += 1;
		}
	}
	ba->w = bc->w + bc->xc;
	if ( bc->h + bc->yc > ba->h )		ba->h = bc->h + bc->yc;
	if ( bc->d - bc->yc > ba->d )		ba->d = bc->d - bc->yc;
	return 0;	
}

int pr_b(struct box *b)
{
	if (b == NULL) return 1;
	printf ("box %d-%d ",b->n_hic,b->n_box);
	printf ("id %d ",b->id);	
	printf ("xc=%d ",b->xc);
	printf ("yc=%d ",b->yc);
	printf ("w=%d ",b->w);
	printf ("h=%d ",b->h);
	printf ("d=%d ",b->d);
	printf ("pt=%d ",b->pt);
	printf ("color=%d ",b->color);	
	printf ("slx=%d ",b->slx);
	printf ("sly=%d ",b->sly);
	printf ("n_char=%d ",b->n_char);
	printf ("i_box=%d\n",b->i_box);	
	return 0;	
}

/*	struct box *catch_last_box(ba);
 *	DESCRIPTION	catch the last box, create a new one and attach this
 *			last box as a child of the new one. This brings the
 *			last box one step down in hierachy
 *
 *	RETURNS		the pointer to the new box
 *			
 */
struct box *catch_last_box(struct box *ba)
{
	struct box *bc = NULL, *temp_box = NULL;
	
	if (ba == NULL) return NULL;
	bc = ba->child[ba->n_box-1];
	ba->w -= bc->w;
	if ( bc->n_box == CHAR_BOX)
	{
		bc->n_box = 0;
		if (mknewbox(bc) == NULL) 	return NULL;
		bc->child[0]->w = bc->w;
		bc->child[0]->h = bc->h;
		bc->child[0]->d = bc->d;
		bc->child[0]->xc = 0;
		bc->child[0]->yc = 0;				
		bc->child[0]->n_char = bc->n_char;
		bc->child[0]->i_box = bc->i_box;		
		bc->child[0]->char_val = bc->char_val;		
		bc->child[0]->n_box = CHAR_BOX;
		bc->i_box =0;
	}
	else
	{
		temp_box = bc;
		ba->n_box--;
		if (mknewbox(ba) == NULL)	return NULL;
		bc = ba->child[ba->n_box-1];
		bc->w = temp_box->w;
		bc->d = temp_box->d;
		bc->h = temp_box->h;
		bc->xc = temp_box->xc;
		temp_box->xc = 0;
		bc->yc = temp_box->yc;
		temp_box->yc = 0;
		bc->slx = temp_box->slx;
		bc->sly = temp_box->sly;
		bc->pt = temp_box->pt;
		bc->i_box = temp_box->i_box;		
		bc->char_val = temp_box->char_val;		
		bc->n_box = 1;
		temp_box->i_box = 0;		
		if (bc->n_box <= 0) return NULL;
		bc->child=(struct box**)calloc(bc->n_box,sizeof(struct box*));
		if ( ba->child == NULL )
		{
			snprintf (eqs_err, 128, "calloc subscript error \n");
			return NULL;
		}
		bc->child[0] = temp_box;
		rearrange_hic(bc);
	}
	return(bc);
}
# undef mknewbox
struct box *mknewbox(struct box *ba)
{
	struct box *bc = NULL;
	
	if ( ba == NULL)		return NULL;
	if ( ba->n_box <0)
	{
		snprintf (eqs_err,128, "negative box error \n");
		return NULL;
	}
	ba->n_box++;
	if ( ba->n_box == 1)
		ba->child=(struct box **)calloc(ba->n_box,sizeof(struct box*));
	else
		ba->child=(struct box **)realloc(ba->child, ba->n_box*sizeof(struct box*));
	if ( ba->child == NULL ) 	return NULL;
	bc = (struct box *)calloc( 1,sizeof(struct box));
	if ( bc == NULL )		return NULL;
	ba->child[ba->n_box - 1] = bc;
	bc->n_box = 0;
	bc->w = 0;
	bc->d = 0;
	bc->h = 0;
	bc->xc = 0;
	bc->yc = 0;
	bc->parent = ba;
	bc->n_hic = ba->n_hic + 1;
	bc->pt = ba->pt;
	bc->color = ba->color;
	bc->i_box = 0;	
	bc->n_char = 0;		
	bc->slx = ba->slx;
	bc->sly = ba->sly;
	bc->id = 0;
	box_cnt++;
#ifdef DEBUG
	printf("m%d->%d\n",ba->n_hic,ba->n_box-1);
#endif
	return (bc);
}
int mkeqs5_main(void)
{
	return 0;
}

# endif










