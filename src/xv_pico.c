/*
 *    Example program for the Allegro library, by Shawn Hargreaves.
 *
 *    This program demonstrates how to use an offscree part of the
 *    video memory to store source graphics for a hardware accelerated
 *    graphics driver.
 get_desktop_resolution(int *width, int *height);
 */

# ifndef _XV_MAIN_C_
# define _XV_MAIN_C_

# include <stdlib.h>
# include <string.h>

#include "platform.h"
#include "allegro.h"

#ifdef XV_WIN32
#include "winalleg.h"
#endif

#ifdef XV_UNIX
#include "xalleg.h"
#endif


#ifdef BUILDING_DLL
#include "xv_plugins.h"
#endif

#include "xvin.h"
#include "plot_op.h"
#include "dev_pcx.h"
#include "gr_file.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "xv_main.h"
# include "im_mn.h"
# include "plot_mn.h"
# include "plot_reg_gui.h"
# include "imr_reg_gui.h"
# include "im_gr.h"
# include "gui_def.h"
# include <locale.h>
#include "../plug-ins-src/trackBead/trackBead.h"

#include "../plug-ins-src/trackBead/pico_serial/pico_serial.h"
#include "../plug-ins-src/cfg_file/Pico_cfg.h"
int SCRIPT_GO_ON;
PXV_FUNC(int, zdet_topo_main, (int argc, char **argv));
PXV_FUNC(int, shut_down_trk, (void));
PXV_FUNC(int, PlayItAgainSam_main, (int argc, char **argv));

int d_draw_Op_proc(int msg, DIALOG *d, int c);

int pltreadfile(O_p *op, char *file_name, int check);
int one_plot_to_box(O_p *o_p, struct box *b);
int draw_pr_box(struct box *b, int dx, int dy, int scale, int color);

int d_picoTeX_proc(int msg, DIALOG *d, int c);


//PXV_FUNC(int, dialog_set_magnets_rot, (char *rots));
//PXV_FUNC(int, dialog_set_magnets_z, (char *zmags));
//PXV_FUNC(int, dialog_set_magnets_z_to_force, (char *forces));
//PXV_FUNC(int, dialog_set_focus, (char *focs));
//PXV_FUNC(int, switch_motor_pid,(int state));
//XV_FUNC(int,  trackBead_main, (int argc, char **argv));

extern int ttf_enable;
extern BITMAP *dev_bitmap;
extern char *debug_ttf;

// folowing has been commented out (NG, 2006-03-14) useless, defined in xv_plugins.h
// typedef int (*PL_MAIN)(int, char**);

/* we need to load fonts.dat to access the big font */
extern BITMAP *plt_buffer;
extern int plt_buffer_used;
extern int screen_used;

extern char prog_dir[PATH_MAX];
extern char config_dir[PATH_MAX];

extern int  win_color_depth;
extern int  win_screen_w;
extern int  win_screen_h;


extern int eps_file_flag;

extern FONT *normalsize_font;
extern FONT *small_font;
extern FONT *large_font;


static void close_hook(void)
{
    simulate_keypress(27 + (KEY_ESC << 8));
}


void    restore_screen(void)
{
  int win_color_depth_l = 0;
  int win_screen_w_l = 0;
  int win_screen_h_l = 0;
  int sw = 0, sh = 0;
  //char message[256] = {0};

  //win_printf("restore");
    screen_restoring = 1;
    //allegro_message("restoring about to proceed");
    win_color_depth_l = desktop_color_depth();

    if (get_desktop_resolution(&win_screen_w_l, &win_screen_h_l) != 0)
    {
        allegro_message("Cannot get the video mode property!");
    }
    if (win_color_depth_l != win_color_depth || win_screen_w_l != win_screen_w
	|| win_screen_h_l != win_screen_h)
      {
	screen = gui_get_screen();
	win_color_depth = win_color_depth_l;
	win_screen_w = win_screen_w_l;
	win_screen_h = win_screen_h_l;
	//snprintf(message,sizeof(message),"%d x %d x %d",win_screen_w,win_screen_h,win_color_depth);
# ifdef XV_WIN32
	show_mouse(NULL);
#endif
	set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
	if (win_screen_w_l > 3800 && win_screen_h_l > 2100)
	  {
	    sh = 3800;
	    sw = 2100;
	  }
	else if (win_screen_w_l > 3600 && win_screen_h_l > 2000)
	  {
	    sh = 3600;
	    sw = 2000;
	  }
	else if (win_screen_w_l > 3100 && win_screen_h_l > 1700)
	  {
	    sh = 3100;
	    sw = 1700;
	  }
	else if (win_screen_w_l > 2500 && win_screen_h_l > 1400)
	  {
	    sh = 2500;
	    sw = 1400;
	  }
	else if (win_screen_w_l > 2450 && win_screen_h_l > 1550)
	  {
	    sh = 2450;
	    sw = 1550;
	  }
	else if (win_screen_w_l > 2400 && win_screen_h_l > 1550)
	  {
	    sh = 2400;
	    sw = 1550;
	  }
	else if (win_screen_w_l > 1900 && win_screen_h_l > 1050)
	  {
	    sh = 1900;
	    sw = 1050;
	  }
	else if (win_screen_w_l > 1660 && win_screen_h_l > 1160)
	  {
	    sh = 1660;
	    sw = 1160;
	  }
	else if (win_screen_w_l > 1660 && win_screen_h_l > 1020)
	  {
	    sh = 1660;
	    sw = 1020;
	  }
	else if (win_screen_w_l > 1420 && win_screen_h_l > 850)
	  {
	    sh = 1420;
	    sw = 850;
	  }
	else if (win_screen_w_l > 1260 && win_screen_h_l > 974)
	  {
	    sh = 1260;
	    sw = 974;
	  }
	else if (win_screen_w_l > 1240 && win_screen_h_l > 768)
	  {
	    sh = 1240;
	    sw = 768;
	  }
	else if (win_screen_w_l > 1000 && win_screen_h_l > 718)
	  {
	    sh = 1000;
	    sw = 718;
	  }
	else if (win_screen_w_l > 900 && win_screen_h_l > 700)
	  {
	    sh = 900;
	    sw = 700;
	  }
	else if (win_screen_w_l >= 640 && win_screen_h_l >= 480)
	  {
	    sh = 640;
	    sw = 480;
	  }

	if (start_xvin(32, GFX_AUTODETECT_WINDOWED, sh, sw) != 0)
	  {
	    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
	    allegro_message("Error setting graphics mode\n%s\n", allegro_error);
	    return;
	  }
	set_XVin_resolution(sh, sw, 0);
	dev_bitmap = screen;
	y_offset = dev_bitmap->h;
	max_x = dev_bitmap->h;
	max_y = dev_bitmap->w;


	//allegro_message(message);
	set_display_switch_callback(SWITCH_IN, restore_screen);
	//set_display_switch_callback(SWITCH_OUT, quit_screen);
	set_close_button_callback(close_hook);
      }

#ifdef XV_WIN32
    broadcast_dialog_message(MSG_DRAW, 0);
#endif
    screen_restoring = 0;
}

int    init_config(int argc, char **argv)
{
    int i, j;
    char all[256] = {0};
    char program_fullfile[PATH_MAX] = {0};
#ifdef FORTIFY
    FILE *fp = NULL;
#endif

    if (argc < 1) return 1;

    //for (i = j = 0; argv[0][i] != 0 && i < 256; i++)
    //{
    //    prog_dir[i] = argv[0][i];
    //    j = (argv[0][i] == '/' || argv[0][i] == 92) ? i : j;
    //}

#ifndef XV_WIN32
    realpath(argv[0], program_fullfile);
#else
    _fullpath(program_fullfile, argv[0], sizeof(program_fullfile));
#endif

    for (i = j = 0; program_fullfile[i] != 0 && i < (int) my_min(sizeof(prog_dir), sizeof(program_fullfile)); i++)
    {
        prog_dir[i] = program_fullfile[i];
        j = (program_fullfile[i] == '/' || program_fullfile[i] == 92) ? i : j;
    }

    prog_dir[i] = 0;

    if (j > 0 && j < i) prog_dir[j + 1] = 0;

#ifdef XV_WIN32
    snprintf(config_dir, PATH_MAX, "%s", prog_dir);
#else
#ifdef UNIQUE_CONFIG_DIR
    snprintf(config_dir, PATH_MAX, "%s%s", getenv("HOME"), "/.config/xvin/");
#endif
#endif


#ifdef FORTIFY
    snprintf(all, 256, "%s%s", prog_dir, "fortify-on.txt");
    fp = fopen(all, "r");

    if (fp == NULL)
    {
        Fortify_Disable();
        fortify_menu[1].text = NULL;
        fortify_menu[1].proc = NULL;
        fortify_menu[0].text = "Fortify next Xvin";
        fortify_menu[0].proc = set_fortify_next;
    }
    else fclose(fp);

#endif

    if (getenv("ALLEGRO") == NULL)
    {
        /* we get config from working directory */
        snprintf(all, sizeof(all), "ALLEGRO=%s", prog_dir);
        putenv(all);
        printf("%s all %s\n", prog_dir, all);
        return 0;
    }

    return 1;

}

int do_trackBead(void)
{
    if (updating_menu_state != 0)  return D_O_K;

# if !defined(PLAYITSAM) && !defined(PIAS)
    trackBead_main(0, NULL);
# endif
    return 0;
}

int     pias_argc = 0;
char ** pias_argv = NULL;
int (*pias_call)(char const *) = NULL;
int xvin_pico(int argc, char *argv[])
{
    /*    struct box b;*/
    /*   PALETTE pal;*/
    pias_argc = argc;
    pias_argv = argv;

    imreg *imr = NULL;
    O_i *oi = NULL;
    //# ifdef GAME
    //  char szFileName[MAX_PATH] = "test.c";
    char  path[512];
    //# endif
    int i;
    char  file[256]; /*, buf[1024]; */
    //extern char fullfile[], *fu;
    /*    clock_t t0; */
    int ret, mode, mode_h, quit = 0;

# ifdef BUILDING_DLL
    int  argcp, np;
    char **argvp, pl_name[64];
#else
    //extern int  track_ifc_main(int argc, char **argv);
    //extern int random_main(int argc, char **argv);
    //int EveryN_main(int argc, char **argv);
    //    int   track_ifc_main(int argc, char **argv);
    //    int random_main(int argc, char **argv);


# endif
    //char fullfile[PATH_MAX] = {0};

    DIALOG_PLAYER *player = NULL;

    #ifdef XV_UNIX
    #ifdef PIAS
    xwin_set_window_name("Pias","pias");
    #else
    xwin_set_window_name("Pico","pico");
    #endif
    #endif

    allegro_init();
    install_keyboard();
    install_timer();
    install_mouse();
    init_config(argc, argv);
#ifdef XV_GTK_DIALOG
    gtk_init(0, NULL);
#endif
    setlocale(LC_ALL, "C");
    printf("%s\n", config_dir);
    preload_config_file();
    //set_uformat(U_ASCII);
    before_menu_proc = NULL;
    after_menu_proc = NULL;

#ifdef XV_UNIX
    enable_hardware_cursor();
    show_os_cursor(1);
#endif

    win_color_depth = desktop_color_depth();

    if (get_desktop_resolution(&win_screen_w, &win_screen_h) != 0)
        allegro_message("Cannot get the video mode property!");

# ifdef FORTIFY
    Fortify_SetOutputFunc(fortify_error);
# endif

    //my_strncat(fullfile, prog_dir, sizeof(fullfile));
    //put_path_separator(fullfile, sizeof(fullfile));
    //my_strncat(fullfile, "fonts.dat", sizeof(fullfile));

    //init_fonts(fullfile);
    init_fonts(NULL);
    main_menu_height = normalsize_font_height + large_font_height * 1.2 + 10;

    if (win_color_depth != 32)
        allegro_message("Your video mode is not in 32 Bits !\n pico.exe might not work properly");

    mode = get_config_int("WINDOW_MODE", "size", 0);
    mode_h = get_config_int("WINDOW_MODE", "height", 0);
    full_screen = get_config_int("WINDOW_MODE", "full-screen", 0);

    if (mode == 0)
    {
        mode = 1024;
        mode_h = 768;
    }

    generate_plot_dialog(mode, mode_h);


    if (full_screen == 0)
        start_xvin(32, GFX_AUTODETECT_WINDOWED, mode, mode_h);
    //GFX_DIRECTX_OVL
    else
        start_xvin(32, GFX_AUTODETECT_FULLSCREEN , mode, mode_h);

    //GFX_DIRECTX_OVL
    Open_Pico_config_file();

    /* We set up colors to match screen color depth (in case it changed) */
    for (ret = 0; the_dialog[ret].proc; ret++)
    {
        if (the_dialog[ret].proc == d_my_edit_proc)
        {
            set_dialog_size_and_color(the_dialog + ret, the_dialog[ret].x, normalsize_font_height + 10 , the_dialog[ret].w, large_font_height,
                                      makecol32(0, 0, 255), makecol32(240, 240, 240));
        }
        else if (the_dialog[ret].proc == d_text_proc)
        {
            set_dialog_size_and_color(the_dialog + ret, the_dialog[ret].x, normalsize_font_height + 10, the_dialog[ret].w, large_font_height,
                                      makecol32(0, 0, 0), makecol32(240, 240, 240));
        }
        else
        {
            the_dialog[ret].fg = makecol(0, 0, 0);
            the_dialog[ret].bg = makecol(255, 255, 255);
        }

    }

    the_dialog[0].w = SCREEN_W;
    the_dialog[0].h = SCREEN_H;
    the_dialog[2].w = SCREEN_W;
    the_dialog[2].h = SCREEN_H - main_menu_height; // 16
    the_dialog[2].y = main_menu_height; // 16


    plt_buffer = create_bitmap(SCREEN_W, SCREEN_H);




    proj_mn_n = -1;
    im_project_n = -1;
    //extract_file_name(file, 256, "h:\\timothee\\bead000quater.gr");
    //extract_file_path(path, 512, "h:\\timothee\\bead000quater.gr");
    //# ifdef GAME
    strncpy(file, "bead000quater.gr", sizeof(file));
    strncpy(path, prog_dir, sizeof(path));
    //strcat(path,"\\");


    oi = load_im_file_in_oi(file, path);

    if (oi == NULL)
    {
        quit = 1;
        win_printf_OK("could not load image\n%s\n"
                      "from path %s", file, backslash_to_slash(path));
    }

    //load_im_file_in_imreg(imr, active_menu->text, (char*)active_menu->dp);
    //# endif
    imr = create_and_register_new_image_project(0,   32,  900,  668);

    if (imr == NULL)
    {
        quit = 1;
#ifdef XV_UNIX
        enable_hardware_cursor();
        show_os_cursor(1);
#else
        show_mouse(screen);

#endif
        win_printf_OK("could not create image refion");
    }
    else if (oi != NULL)
    {
        add_image(imr, oi->im.data_type, (void *)oi);
        remove_from_image(imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
    }




    //  win_printf("display switch mode : %d", get_display_switch_mode());
    i = SWITCH_BACKGROUND; // with this option, XVin will still wor, even if put in the bacground

    if (set_display_switch_mode(i) != 0) win_printf("cannot set display switch mode to %d", i);

    //  win_printf("display switch mode : %d", get_display_switch_mode());
    // # SWITCH_NONE = 0 not OK on win32/windowed
    // # SWITCH_PAUSE = 1 default on win32/windowed
    // # SWITCH_AMNESIA = 2, not OK on win32/windowed
    // # SWITCH_BACKGROUND = 3, OK
    // # SWITCH_BACKAMNESIA =5, not OK on win32/windowed

    set_display_switch_callback(SWITCH_IN, restore_screen);
    set_close_button_callback(close_hook);

    //set_window_close_hook(close_hook);

    gui_fg_color = makecol(0, 0, 0);
    gui_mg_color = makecol(160, 160, 160);
    gui_bg_color = makecol(220, 220, 240);

    //    register_trace_handler(win_printf);
    //    register_assert_handler(win_printf);



#ifdef XV_UNIX
    show_os_cursor(1);
#else
    show_mouse(screen);
#endif

# ifdef XV_WIN32
    LoadLibrary("exchndl.dll");  // Dr mingw exception handler
# endif



    //add_image_treat_menu_item ( "start trackBead", do_trackBead, NULL, 0, NULL);


    //  DoFileSave("Test save 2", NULL, szFileName,MAX_PATH,
    //   "Tracking data Files (*.trk)\0*.trk\0All Files (*.*)\0*.*\0\0", "trk");
    if (quit == 0)
    {
        player = init_dialog(the_dialog, -1);
        //das1602_main(argc, argv);
        //track_ifc_main(argc, argv);
        //random_main(argc, argv);
        //EveryN_main(argc, argv);
        update_dialog(player);
        broadcast_dialog_message(MSG_DRAW, 0);
# if !defined(PLAYITSAM) && !defined(PIAS)
        trackBead_main(0, NULL);
        zdet_topo_main(0, NULL);
# endif

        SCRIPT_GO_ON = 1;
# ifdef PIAS
        PlayItAgainSam_main(argc, argv);
#else
        //pico_serial_main(0, NULL);
# endif

# ifdef PLAYITSAM
        playitsam_main(0, NULL);
# endif
#ifdef BUILDING_DLL
        autoload_plugins();
        do_update_menu();
#endif

        while (update_dialog(player) && SCRIPT_GO_ON == 1)
        {
            //rest(1); //avoid cpu usage 100%
        }

        shutdown_dialog(player);

        //ret = do_dialog(the_dialog, -1);
#ifdef XV_UNIX
        show_os_cursor(1);
#else
        show_mouse(screen);
#endif
    }

# if !defined(PLAYITSAM) && !defined(PIAS)
    my_set_window_title("Shutting down track");
    shut_down_trk();

# endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
    allegro_exit();

    /*      allegro_message("1 plot in %ld ticks with %ld tics per sec \n%s",
            t0,CLOCKS_PER_SEC,message);
            allegro_message("%s",debug_ttf);*/
    return 0;
}

# include "fftl32n.h"
float *get_fftsin(void)
{
    return fftsin;
}

# endif
