/*
 *    main.c for XVin
 */

// #include <stdlib.h>
// #include <string.h>

#include "allegro.h"
#undef BUILDING_DLL
#include "platform.h"

#include "xvin.h"

#include "xv_main.h"

int main(int argc, char *argv[])
{
  //FILE *fp;
  //fp = fopen("c:\\junk.er","w");
  // allegro_message("starting\r\n");	
  //fprintf(fp,"starting\r\n");	
  //fclose(fp);
  return(xvin_pico(argc, argv));
}

END_OF_MAIN()  // le point virgule n'est pas recommandé (cf Allegro doc)
