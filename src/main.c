/*
 *    main.c for XVin
 */

// #include <stdlib.h>
// #include <string.h>

#include <allegro.h>
#include "platform.h"

#include "xvin.h"
#include "xv_main.h"

int main(int argc, char *argv[])
{
  return(xvin_main(argc, argv));
}
END_OF_MAIN();  // le point virgule n'est pas recommandé (cf Allegro doc)
