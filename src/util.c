# ifndef _UTIL_C_
# define _UTIL_C_

# include "xvin/platform.h"
# include "util.h"
#include "xvin.h"

# include <stdarg.h>
# include <stdio.h>
# include <malloc.h>
# include <string.h>
# include <ctype.h>
# include <allegro.h>
# include <math.h>
# include <float.h>
#ifndef XV_UNIX
#ifndef XV_MAC
# include "winalleg.h"
# include <io.h>
#endif
#endif


#ifdef XV_MAC
#include <time.h>
#include <sys/time.h>
#include <mach/mach_time.h>
#endif


#ifdef XV_UNIX
#include <linux/unistd.h>
#include <time.h>
#endif

//already in allegro
//bool file_exists(const char *filename)
//{
//    if (FILE * file = fopen(filename, "r"))
//    {
//        fclose(file);
//        return true;
//    }
//    return false;
//}

#if defined(XV_MAC)

int clock_gettime(int param, struct timespec* t) {
   struct timeval now;
   int rv = gettimeofday(&now, NULL);
   if (rv) return rv;
   t->tv_sec  = now.tv_sec;
   t->tv_nsec = now.tv_usec * 1000;
   return 0;
};
#endif

char *my_strdup(const char *s)
{
    char *p = NULL;

    if (s != NULL)
    {
        p = (char *) malloc(strlen(s) + 1);

        if (p)
        {
            strcpy(p, s);
        }
    }

    return p;
}
char *my_strupr(char *s)
{
    int i = 0;

    for (i = 0; s[i] != 0; i++)
    {
        s[i] = s[i] & ~('a' - 'A');
    }

    return s;
}

char *my_sprintf(char *dest, char *format, ...)
{
    int len;
    char *c = NULL;
    char *tch = NULL;
    va_list ap;
    c = (char *)calloc(2048, sizeof(char));

    if (c == NULL)
    {
        return NULL;
    }

    va_start(ap, format);
    vsnprintf(c, 2048, format, ap);
    va_end(ap);

    if (dest == NULL)
    {
        dest = strdup(c);
    }
    else
    {
        len = strlen(dest) + strlen(c) + 1;
        tch = (char *)realloc(dest, len * sizeof(char));

        if (tch == NULL)
        {
            free(c);
            return NULL;
        }

        dest = tch;
        my_strncat(dest, c, len);
    }

    free(c);
    return dest;
}

char *get_os_specific_path(char *path)
{
#ifdef XV_WIN32
    return slash_to_backslash(path);
#else
    return backslash_to_slash(path);
#endif
}

bool is_path_separator(char c)
{
    return c == '\\' || c == '/';
}
char get_path_separator(void)
{
#ifdef XV_WIN32
    return '\\';
#else
    return '/';
#endif
}
void put_path_separator(char *path, int n_path)
{
    size_t len = 0;

    if (path == NULL) return;
    len = strlen(path);
#ifdef XV_WIN32
    if (len > 0 && path[len-1] == '\\') return;
    my_strncat(path, "\\", n_path);
#else
    if (len > 0 && path[len-1] == '/') return;
    my_strncat(path, "/", n_path);
#endif
}


int my_set_window_title(char *format, ...)
{
    char c[1024] = { 0 };
    va_list ap;

    if (format != NULL)
    {
        va_start(ap, format);
#ifdef NO_SET_WINDOW_TITLE
        debug_message(format, ap);
#else
        vsnprintf(c, 1024, format, ap);
        set_window_title(c);
#endif
        va_end(ap);
    }

    return 0;
}


int set_formated_string(char **dest, char *format, ...)
{
    char *c = NULL;
    va_list ap;
    c = (char *)calloc(2048, sizeof(char));

    if (c == NULL)
    {
        return 1;
    }

    va_start(ap, format);
    vsnprintf(c, 2048, format, ap);
    va_end(ap);

    if (*dest != NULL && strlen(*dest) > 0)
    {
        free(*dest);
        *dest = NULL;
    }

    if (strlen(c) > 0)
    {
        *dest = strdup(c);
    }

    free(c);

    if (*dest == NULL)
    {
        return 1;
    }

    return 0;
}

int vset_formated_string(char **dest, char *format, va_list ap)
{
    char *c = NULL;
    c = (char *)calloc(2048, sizeof(char));

    if (c == NULL)
    {
        return 1;
    }

    vsnprintf(c, 2048, format, ap);
    va_end(ap);

    if (*dest != NULL && strlen(*dest) > 0)
    {
        free(*dest);
        *dest = NULL;
    }

    if (strlen(c) > 0)
    {
        *dest = strdup(c);
    }

    free(c);

    if (*dest == NULL)
    {
        return 1;
    }

    return 0;
}


/*  char    *Mystrdupre(char *dest, char *src)
 *  DESCRIPTION place a copy of src into dest, first free dest,
 *          accept a NULL src
 *  RETURNS     dest
 */
char    *Mystrdupre(char *dest, const  char *src)
{
    if (dest != NULL)
    {
        free(dest);
    }

    if (src != NULL && strlen(src) > 0)
    {
        dest = strdup(src);
    }
    else
    {
        dest = NULL;
    }

    return dest;
}

char    *slash_to_backslash(char *path)
{
    int i;

    if (path == NULL)
    {
        return NULL;
    }

    for (i = 0; path[i] != 0; i++)
        if (path[i] == '/')
        {
            path[i] = '\\';
        }

    return path;
}
char    *backslash_to_slash(char *path)
{
    int i;

    if (path == NULL)
    {
        return NULL;
    }

    for (i = 0; path[i] != 0; i++)
        if (path[i] == '\\')
        {
            path[i] = '/';
        }

    return path;
}
char *my_getcwd(char *path, int size)
{
    if (path == NULL || size < 3)
    {
        return NULL;
    }

#ifdef XV_UNIX
    return getcwd(path , size);
#endif
#ifdef XV_MAC
    return getcwd(path , size);
#endif
#ifndef XV_UNIX
#ifndef XV_MAC
    return _getcwd(path , size);
#endif
#endif
}
char    *extract_file_name(char *file, int n_file, const char *full_name)
{
    int i, j, k;
    const char *c = full_name;
    char tpath[DIR_LEN] = { 0 };

    if (full_name == NULL)
    {
        return NULL;
    }

    for (k = j = i = 0; c[i] != 0; i++)
    {
        k = j = i + 1;
    }

    for (i = i - 1; i >= 0 && c[i] != '/' && c[i] != '\\' && c[i] != ':'; i--);

    j = (i < 0) ? 0 : i + 1;

    if (j >= k)
    {
        return NULL;
    }

    for (i = 0;  j + i < k; i++)
    {
        tpath[i] = c[i + j];
    }

    tpath[i] = 0;

    if (file == NULL || n_file < i)
    {
        return strdup(tpath);
    }
    else
    {
        strncpy(file, tpath, n_file - 1);
        file[n_file - 1] = '\0';
    }

    return file;
}

char    *extract_file_path(char *path, int n_path, const char *full_name)
{
    int i, j;
    const char *c = full_name;
    char tpath[DIR_LEN] = { 0 };

    if (full_name == NULL)
    {
        return NULL;
    }

    for (j = i = 0; c[i] != 0; i++)
    {
        j = i + 1;
    }

    for (i = i - 1; i >= 0 && c[i] != '/' && c[i] != '\\' && c[i] != ':'; i--);

    if (i <= 0 || c[i] == '\\' || c[i] == '/')
    {
        j = (i < 0) ? 0 : i;
    }

    if (i < 0)
    {
        return NULL;
    }

    if (c[i] == ':')
    {
        j = i + 1;
    }

    for (i = 0; i <= j; i++)
    {
        tpath[i] = c[i];
    }

    tpath[j] = 0;
    put_backslash(tpath);

    //cas MACOS
    //slash_to_backslash(tpath);

    if ((path == NULL) || (n_path < j))
    {
        /*
           win_printf("new buffer alloc\n j = %d n_path = %d\n fulname %s\npath >%s\n path == %d<",j,n_path,full_name,tpath,(path == NULL));
           */
        return strdup(tpath);
    }
    else
    {
        /* win_printf("old buffer used"); */
        strncpy(path, tpath, n_path);
    }

    return path;
}


/**
 * @brief
 *
 * @param name Output fullname
 * @param n_name size of the fullname variable
 * @param path path
 * @param file filename
 *
 * @return name
 */
char    *build_full_file_name(char *name, int n_name, const char *path, const char *file)
{
    int i;
    char tpath[DIR_LEN] = {0};

    if (file == NULL)
    {
        return NULL;
    }

    if ((path != NULL) && (strlen(path) >= DIR_LEN))
    {
        return NULL;
    }

    if (path == NULL)
    {
        if (my_getcwd(tpath, DIR_LEN) == NULL)
        {
            return NULL;
        }
    }
    else
    {
        strncpy(tpath, path, DIR_LEN);
    }

    get_os_specific_path(tpath);

    for (i = 0; tpath[i] != 0; i++);

    for (; i > 0 && isspace(tpath[i - 1]); i--);

    if (i > 1 && !is_path_separator(tpath[i - 1]))
    {
        tpath[i] = get_path_separator();
        tpath[i + 1] = 0;
    }

    my_strncat(tpath, file, sizeof(tpath));

    if (name == NULL || n_name < (int)strlen(tpath))
    {
        return strdup(tpath);
    }
    else
    {
        strncpy(name, tpath, n_name);
    }

    return name;
}

char    *Transfer_filename(char *src)
{
    int i;
    char *dest = NULL;

    if (src != NULL)
    {
        dest = strdup(src);

        for (i = 0;  dest[i] != 0; i++)
        {
            if (dest[i] == '.')
            {
                dest[i--] = 0;
            }
        }
    }
    else
    {
        dest = strdup("untitled");
    }

    return dest;
}
// more simple behavior than strncat
char *my_strncat(char *dest, const char *src, size_t dest_size)
{
    size_t len;

    if (dest == NULL)
    {
        if (src == NULL)
        {
            return NULL;
        }
        else
        {
            return strdup(src);
        }
    }

    if (src == NULL)
    {
        return dest;
    }

    len = strlen(dest);

    if (dest_size > len)
    {
        return strncat(dest, src, dest_size - len);
    }
    else
    {
        return dest;
    }
}


# ifdef XV_WIN32

unsigned int my_uclock(void)
{
    LARGE_INTEGER t0;
    QueryPerformanceCounter(&t0);
    return (unsigned int)(t0.QuadPart / 16);
}

unsigned int my_uclocks_per_sec = 0;

unsigned int get_my_uclocks_per_sec(void)
{
    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);
    my_uclocks_per_sec = (unsigned int)(freq.QuadPart / 16);
    return my_uclocks_per_sec;
}


long long my_ulclock(void)
{
    LARGE_INTEGER t0;
    QueryPerformanceCounter(&t0);
    return (long long)(t0.QuadPart);
}

long long my_ulclocks_per_sec = 0;

long long get_my_ulclocks_per_sec(void)
{
    LARGE_INTEGER freq;
    QueryPerformanceFrequency(&freq);
    my_ulclocks_per_sec = (long long)(freq.QuadPart);
    return my_ulclocks_per_sec;
}

#else // non-Win32 systems, therefore Unix:



long long my_ulclock(void)
{
    struct timespec tspec;
    clock_gettime(CLOCK_MONOTONIC, &tspec);
    return  1e6 * (unsigned int) tspec.tv_sec + ((unsigned int) tspec.tv_nsec / 1e3);
}

long long my_ulclocks_per_sec = 1e6;

long long get_my_ulclocks_per_sec(void)
{
    return my_ulclocks_per_sec;
}

unsigned int my_uclock(void)
{
    return my_ulclock() / 4;
}

unsigned int my_uclocks_per_sec = 1e6 / 4;

unsigned int get_my_uclocks_per_sec(void)
{
    return my_uclocks_per_sec;
}


# endif
# endif


char *protect_string(const char *str, int size)
{
    int res_size = size + 1 + 32;
    char *res = (char *) calloc(size + 1 + 32, sizeof(char));
    int res_i = 0;

    for (int i = 0; i < size; ++i)
    {
        if (res_size - res_i < 4)
        {
            res_size += 32;
            res = (char *)realloc(res, res_size);
        }

        if (str[i] == '^' || str[i] == '_')
        {
            res[res_i] = '\\';
            res_i ++;
            res[res_i] = '\\';
            res_i ++;
        }

        res[res_i] = str[i];
        res_i++;
    }

    res[res_i] = '\0';
    return res;
}

bool nearly_equal(float a, float b, float epsilon)
{
    float abs_a = fabsf(a);
    float abs_b = fabsf(b);
    float diff = fabsf(a - b);

    if (a == b)   // shortcut, handles infinities
    {
        return true;
    }
    else if (a == 0 || b == 0 || diff < FLT_EPSILON)
    {
        // a or b is zero or both are extremely close to it
        // relative error is less meaningful here
        return diff < (epsilon * FLT_EPSILON);
    }
    else
    {
        // use relative error
        return diff / (abs_a + abs_b) < epsilon;
    }
}


int my_cp(const char *to, const char *from)
{
    FILE *fd_to = NULL, *fd_from = NULL;
    char buf[4096] = {0};
    ssize_t nread;
    int saved_errno;
    fd_from = fopen(from, "r");

    if (fd_from == NULL)
    {
        return -1;
    }

    fd_to = fopen(to, "w");

    if (fd_to == NULL)
    {
        return -1;
    }

    while (nread = fread(buf, sizeof(char),  sizeof(buf), fd_from), nread > 0)
    {
        char *out_ptr = buf;
        ssize_t nwritten;

        do
        {
            nwritten = fwrite(out_ptr, sizeof(char), nread, fd_to);

            if (nwritten >= 0)
            {
                nread -= nwritten;
                out_ptr += nwritten;
            }
            else if (errno != EINTR)
            {
                goto out_error;
            }
        }
        while (nread > 0);
    }

    if (nread == 0)
    {
        if (fclose(fd_to) < 0)
        {
            fd_to = NULL; // was -1 !
            goto out_error;
        }

        fclose(fd_from);
        /* Success! */
        return 0;
    }

out_error:
    saved_errno = errno;
    fclose(fd_from);

    if (fd_to != NULL) //>= 0)
    {
        fclose(fd_to);
    }

    errno = saved_errno;
    return -1;
}
