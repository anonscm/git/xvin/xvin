#ifndef _PLOT_DS_C_
#define _PLOT_DS_C_
# include "xvin/platform.h"
# include "util.h"
# include "plot_ds.h"

# include <stdlib.h>
# include <string.h>
#include <float.h>

int     *data_color;
int     max_data_color;
char **data_symbols;
int max_data_symbols;
/*
# include "color.h"
int     data_color[] = {Yellow, Lightgreen, Lightred, Lightblue};
int     max_data_color = 4;
*/
# define DS_COLOR  0

p_l *create_plot_label(int type, float x, float y, char *format, ...)
{
    char *c = NULL;
    va_list ap;
    p_l *pl = NULL;
    pl = (p_l *)calloc(1, sizeof(p_l));

    if (pl == NULL)
    {
        return (p_l *) xvin_ptr_error(Out_Of_Memory);
    }

    c = (char *)calloc(2048, sizeof(char));

    if (c == NULL)
    {
        if (pl)
        {
            free(pl);
        }

        return (p_l *) xvin_ptr_error(Out_Of_Memory);
    }

    va_start(ap, format);
    vsnprintf(c, 2048, format, ap);
    va_end(ap);
    pl->type = type;
    pl->xla = x;
    pl->yla = y;

    if (strlen(c) > 0)
    {
        pl->text = strdup(c);
    }

    free(c);

    if (pl->text == NULL)
    {
        if (pl)
        {
            free(pl);
        }

        return (p_l *) xvin_ptr_error(Out_Of_Memory);
    }

    return pl;
}
p_l *duplicate_plot_label(p_l *pls)
{
    p_l *pl = NULL;

    if (pls == NULL)
    {
        return NULL;
    }

    pl = (p_l *)calloc(1, sizeof(p_l));

    if (pl == NULL)
    {
        return (p_l *) xvin_ptr_error(Out_Of_Memory);
    }

    *pl = *pls;
    pl->text = ((pls->text != NULL) && (strlen(pls->text) > 0)) ? strdup(pls->text) : NULL;
    return pl;
}
int free_plot_label(p_l *pl)
{
    if (pl == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    if (pl->text)
    {
        free(pl->text);
        pl->text = NULL;
    }

    free(pl);
    return 0;
}
int free_data_set(d_s *ds)
{
    int i;

    if (ds == NULL)
    {
        return 1;
    }

    if (ds->xd != NULL)
    {
        free(ds->xd);
        ds->xd = NULL;
    }

    if (ds->yd != NULL)
    {
        free(ds->yd);
        ds->yd = NULL;
    }

    if (ds->xe != NULL)
    {
        free(ds->xe);
        ds->xe = NULL;
    }

    if (ds->ye != NULL)
    {
        free(ds->ye);
        ds->ye = NULL;
    }

    if (ds->xed != NULL)
    {
        free(ds->xed);
        ds->xed = NULL;
    }

    if (ds->yed != NULL)
    {
        free(ds->yed);
        ds->yed = NULL;
    }

    if (ds->xbu != NULL)
    {
        free(ds->xbu);
        ds->xbu = NULL;
    }

    if (ds->ybu != NULL)
    {
        free(ds->ybu);
        ds->yed = NULL;
    }

    if (ds->xbd != NULL)
    {
        free(ds->xbd);
        ds->xbd = NULL;
    }

    if (ds->ybd != NULL)
    {
        free(ds->ybd);
        ds->ybd = NULL;
    }

    if (ds->symb != NULL)
    {
        free(ds->symb);
        ds->symb = NULL;
    }

    if (ds->source != NULL)
    {
        free(ds->source);
        ds->source = NULL;
    }

    if (ds->history != NULL)
    {
        free(ds->history);
        ds->history = NULL;
    }

    if (ds->treatement != NULL)
    {
        free(ds->treatement);
        ds->treatement = NULL;
    }

    if (ds->special != NULL)
    {
        for (i = 0 ; i < ds->n_special ; i++)
            if (ds->special[i] != NULL)
            {
                free(ds->special[i]);
                ds->special[i] = NULL;
            }

        free(ds->special);
        ds->special = NULL;
    }

    for (i = 0 ; i < ds->n_lab ; i++)
    {
        if (ds->lab[i]->text != NULL)
        {
            free(ds->lab[i]->text);
            ds->lab[i]->text = NULL;
        }

        if (ds->lab[i] != NULL)
        {
            free(ds->lab[i]);
            ds->lab[i] = NULL;
        }
    }

    if (ds->lab)
    {
        free(ds->lab);
        ds->lab = NULL;
    }

    for (i = 0 ; i < 8 ; i++)
    {
        if (ds->src_parameter_type[i] != NULL)
        {
            free(ds->src_parameter_type[i]);
            ds->src_parameter_type[i] = NULL;
        }
    }

    if (ds_free_use != NULL)
    {
        ds_free_use(ds);
    }

    free(ds);
    ds = NULL;
    return 0;
}
d_s *build_data_set(int nx, int ny)
{
    int i;
    d_s *ds = NULL;

    if (nx <= 0 || ny <= 0)
    {
        return NULL;
    }

    ds = (d_s *)calloc(1, sizeof(d_s));

    if (ds == NULL)
    {
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    ds->xd = (float *)calloc(nx, sizeof(float));
    ds->yd = (float *)calloc(ny, sizeof(float));

    if (ds->xd == NULL || ds->yd == NULL)
    {
        free_data_set(ds);
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    ds->mx = nx;
    ds->my = ny;
    ds->m = 1;
    ds->xe = ds->ye = NULL;
    ds->xed = ds->yed = NULL;
    ds->xbu = ds->ybu = NULL;
    ds->xbd = ds->ybd = NULL;
    ds->color = DS_COLOR;
    ds->n_special = ds->m_special = 0;
    ds->special = NULL;
    ds->use.to = 0;
    ds->use.stuff = NULL;
    ds->symb = NULL;
    ds->source = NULL;
    ds->history = NULL;
    ds->treatement = NULL;
    ds->special = NULL;
    ds->lab = (p_l **)calloc(MAX_DATA, sizeof(p_l *));
    ds->boxplot_width = 1;

    if (ds->lab == NULL)
    {
        free_data_set(ds);
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    ds->m_lab = MAX_DATA;
    set_ds_starting_time(ds);
    // following line added 2007/10/16, N.G.:
    ds->nx = nx;
    ds->ny = ny;

    for (i = 0 ; i < 8 ; i++)
    {
        ds->src_parameter_type[i] = NULL;
        ds->src_parameter[i] = 0;
    }

    ds->invisible = 0;
    return ds;
}
d_s *build_adjust_data_set(d_s *ds, int nx, int ny)
{
    if (ds == NULL || nx <= 0 || ny <= 0)
    {
        return (build_data_set(nx, ny));
    }

    // if (nx < ds->mx || ny < ds->my)
    // {
    //     ds->nx = nx;
    //     ds->ny  = ny;
    //     return ds;
    // }

    ds->xd = (float *)realloc(ds->xd, nx * sizeof(float));
    ds->yd = (float *)realloc(ds->yd, ny * sizeof(float));

    if (ds->xd == NULL || ds->yd == NULL)
    {
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    ds->mx = nx;
    ds->my = ny;

    if (ds->xe != NULL)
    {
        ds->xe = (float *)realloc(ds->xe, nx * sizeof(float));

        if (ds->xe == NULL)
        {
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (ds->ye != NULL)
    {
        ds->ye = (float *)realloc(ds->ye, ny * sizeof(float));

        if (ds->ye == NULL)
        {
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (ds->xed != NULL)
    {
        ds->xed = (float *)realloc(ds->xed, nx * sizeof(float));

        if (ds->xed == NULL)
        {
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (ds->yed != NULL)
    {
        ds->yed = (float *)realloc(ds->yed, ny * sizeof(float));

        if (ds->yed == NULL)
        {
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (ds->xbu != NULL)
    {
        ds->xbu = (float *)realloc(ds->xbu, nx * sizeof(float));

        if (ds->xbu == NULL)
        {
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (ds->ybu != NULL)
    {
        ds->ybu = (float *)realloc(ds->ybu, ny * sizeof(float));

        if (ds->ybu == NULL)
        {
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (ds->xbd != NULL)
    {
        ds->xbd = (float *)realloc(ds->xbd, nx * sizeof(float));

        if (ds->xbd == NULL)
        {
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (ds->ybd != NULL)
    {
        ds->ybd = (float *)realloc(ds->ybd, ny * sizeof(float));

        if (ds->ybd == NULL)
        {
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    return ds;
}

d_s *alloc_data_set_y_error(d_s *ds)
{
    if (ds == NULL)
    {
        return (d_s *) xvin_ptr_error(Wrong_Argument);
    }

    if (ds->ye != NULL)
    {
        return ds;
    }

    ds->ye = (float *)calloc(ds->my, sizeof(float));

    if (ds->ye == NULL)
    {
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    return ds;
}

d_s *alloc_data_set_x_error(d_s *ds)
{
    if (ds == NULL)
    {
        return (d_s *) xvin_ptr_error(Wrong_Argument);
    }

    if (ds->xe != NULL)
    {
        return ds;
    }

    ds->xe = (float *)calloc(ds->mx, sizeof(float));

    if (ds->xe == NULL)
    {
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    return ds;
}

int free_data_set_y_error(d_s *ds)
{
    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    if (ds->ye != NULL)
    {
        free(ds->ye);
    }

    ds->ye = NULL;
    return 0;
}
int free_data_set_x_error(d_s *ds)
{
    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    if (ds->xe != NULL)
    {
        free(ds->xe);
    }

    ds->xe = NULL;
    return 0;
}

d_s *alloc_data_set_y_down_error(d_s *ds)
{
    if (ds == NULL)
    {
        return (d_s *) xvin_ptr_error(Wrong_Argument);
    }

    if (ds->yed != NULL)
    {
        return ds;
    }

    ds->yed = (float *)calloc(ds->my, sizeof(float));

    if (ds->yed == NULL)
    {
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    return ds;
}

d_s *alloc_data_set_x_down_error(d_s *ds)
{
    if (ds == NULL)
    {
        return (d_s *) xvin_ptr_error(Wrong_Argument);
    }

    if (ds->xed != NULL)
    {
        return ds;
    }

    ds->xed = (float *)calloc(ds->mx, sizeof(float));

    if (ds->xed == NULL)
    {
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    return ds;
}

int free_data_set_y_down_error(d_s *ds)
{
    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    if (ds->yed != NULL)
    {
        free(ds->yed);
    }

    ds->yed = NULL;
    return 0;
}
int free_data_set_x_down_error(d_s *ds)
{
    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    if (ds->xed != NULL)
    {
        free(ds->xed);
    }

    ds->xed = NULL;
    return 0;
}

d_s *alloc_data_set_y_box(d_s *ds)
{
    if (ds == NULL)
    {
        return (d_s *) xvin_ptr_error(Wrong_Argument);
    }

    if (ds->ybu != NULL || ds->ybd != NULL)
    {
        return ds;
    }

    ds->ybu = (float *)calloc(ds->my, sizeof(float));
    ds->ybd = (float *)calloc(ds->my, sizeof(float));

    if (ds->ybu == NULL || ds->ybd == NULL)
    {
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    return ds;
}
d_s *alloc_data_set_y_box_up(d_s *ds)
{
    if (ds == NULL)
    {
        return (d_s *) xvin_ptr_error(Wrong_Argument);
    }

    if (ds->ybd != NULL)
    {
        return ds;
    }

    ds->ybd = (float *)calloc(ds->my, sizeof(float));

    if (ds->ybd == NULL)
    {
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    return ds;
}
d_s *alloc_data_set_y_box_dwn(d_s *ds)
{
    if (ds == NULL)
    {
        return (d_s *) xvin_ptr_error(Wrong_Argument);
    }

    if (ds->ybu != NULL)
    {
        return ds;
    }

    ds->ybu = (float *)calloc(ds->my, sizeof(float));

    if (ds->ybu == NULL)
    {
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    return ds;
}


d_s *alloc_data_set_x_box(d_s *ds)
{
    if (ds == NULL)
    {
        return (d_s *) xvin_ptr_error(Wrong_Argument);
    }

    if (ds->xbu != NULL || ds->xbd != NULL)
    {
        return ds;
    }

    ds->xbu = (float *)calloc(ds->my, sizeof(float));
    ds->xbd = (float *)calloc(ds->my, sizeof(float));

    if (ds->xbu == NULL || ds->xbd == NULL)
    {
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    return ds;
}

d_s *alloc_data_set_x_box_up(d_s *ds)
{
    if (ds == NULL)
    {
        return (d_s *) xvin_ptr_error(Wrong_Argument);
    }

    if (ds->xbu != NULL)
    {
        return ds;
    }

    ds->xbu = (float *)calloc(ds->my, sizeof(float));

    if (ds->xbu == NULL)
    {
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    return ds;
}

d_s *alloc_data_set_x_box_dwn(d_s *ds)
{
    if (ds == NULL)
    {
        return (d_s *) xvin_ptr_error(Wrong_Argument);
    }

    if (ds->xbd != NULL)
    {
        return ds;
    }

    ds->xbd = (float *)calloc(ds->my, sizeof(float));

    if (ds->xbd == NULL)
    {
        return (d_s *) xvin_ptr_error(Out_Of_Memory);
    }

    return ds;
}




int free_data_set_y_box(d_s *ds)
{
    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    if (ds->ybu != NULL)
    {
        free(ds->ybu);
    }

    if (ds->ybd != NULL)
    {
        free(ds->ybd);
    }

    ds->ybu = NULL;
    ds->ybd = NULL;
    return 0;
}


int free_data_set_y_box_up(d_s *ds)
{
    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    if (ds->ybu != NULL)
    {
        free(ds->ybu);
    }

    ds->ybu = NULL;
    return 0;
}

int free_data_set_y_box_dwn(d_s *ds)
{
    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    if (ds->ybd != NULL)
    {
        free(ds->ybd);
    }

    ds->ybd = NULL;
    return 0;
}

int free_data_set_x_box(d_s *ds)
{
    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    if (ds->xbu != NULL)
    {
        free(ds->xbu);
    }

    if (ds->xbd != NULL)
    {
        free(ds->xbd);
    }

    ds->xbu = NULL;
    ds->xbd = NULL;
    return 0;
}

int free_data_set_x_box_up(d_s *ds)
{
    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    if (ds->xbu != NULL)
    {
        free(ds->xbu);
    }

    ds->xbu = NULL;
    return 0;
}

int free_data_set_x_box_dwn(d_s *ds)
{
    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }


    if (ds->xbd != NULL)
    {
        free(ds->xbd);
    }

    ds->xbd = NULL;
    return 0;
}


/*  d_s *duplicate_data_set(d_s *src, d_s *dest);
 *  DESCRIPTION Copy the data of src data_set to dest, if dest == NULL
 *          allocate space for the new one_plot
 *
 *
 *  RETURNS     the dest data_set on success, NULL otherwise
 *
 */
d_s *duplicate_data_set(const d_s *src, d_s *dest)
{
    int i, j;
    p_l *sp = NULL, *dp = NULL;
    int n_min, m_min;

    if (src == NULL)
    {
        return (d_s *) xvin_ptr_error(Wrong_Argument);
    }

    n_min = (src->nx < src->ny) ? src->nx : src->ny;
    m_min = (src->mx < src->my) ? src->mx : src->my;
    m_min = (m_min <= 0) ? 16 : m_min;

    if ((dest == NULL) || (dest->mx < n_min) || (dest->my < n_min))
    {
        if (n_min > 0)
        {
            dest = build_adjust_data_set(dest, n_min, n_min);
        }
        else
        {
            dest = build_adjust_data_set(dest, m_min, m_min);

            if (dest != NULL)
            {
                dest->nx = dest->ny = n_min;
            }
        }

        if (dest == NULL)
        {
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->xe != NULL && dest->xe == NULL)
    {
        if (src->nx > 0)
        {
            dest->xe = (float *)realloc(dest->xe, src->nx * sizeof(float));
        }
        else
        {
            dest->xe = (float *)realloc(dest->xe, m_min * sizeof(float));
        }

        if (dest->xe == NULL)
        {
            free_data_set(dest);
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->ye != NULL && dest->ye == NULL)
    {
        if (src->ny > 0)
        {
            dest->ye = (float *)realloc(dest->ye, src->ny * sizeof(float));
        }
        else
        {
            dest->ye = (float *)realloc(dest->ye, m_min * sizeof(float));
        }

        if (dest->ye == NULL)
        {
            free_data_set(dest);
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->xed != NULL && dest->xed == NULL)
    {
        if (src->nx > 0)
        {
            dest->xed = (float *)realloc(dest->xed, src->nx * sizeof(float));
        }
        else
        {
            dest->xed = (float *)realloc(dest->xed, m_min * sizeof(float));
        }

        if (dest->xed == NULL)
        {
            free_data_set(dest);
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->yed != NULL && dest->yed == NULL)
    {
        if (src->ny > 0)
        {
            dest->yed = (float *)realloc(dest->yed, src->ny * sizeof(float));
        }
        else
        {
            dest->yed = (float *)realloc(dest->yed, m_min * sizeof(float));
        }

        if (dest->yed == NULL)
        {
            free_data_set(dest);
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->xbu != NULL && dest->xbu == NULL)
    {
        if (src->nx > 0)
        {
            dest->xbu = (float *)realloc(dest->xbu, src->nx * sizeof(float));
        }
        else
        {
            dest->xbu = (float *)realloc(dest->xbu, m_min * sizeof(float));
        }

        if (dest->xbu == NULL)
        {
            free_data_set(dest);
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->ybu != NULL && dest->ybu == NULL)
    {
        if (src->ny > 0)
        {
            dest->ybu = (float *)realloc(dest->ybu, src->ny * sizeof(float));
        }
        else
        {
            dest->ybu = (float *)realloc(dest->ybu, m_min * sizeof(float));
        }

        if (dest->ybu == NULL)
        {
            free_data_set(dest);
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->xbd != NULL && dest->xbd == NULL)
    {
        if (src->nx > 0)
        {
            dest->xbd = (float *)realloc(dest->xbd, src->nx * sizeof(float));
        }
        else
        {
            dest->xbd = (float *)realloc(dest->xbd, m_min * sizeof(float));
        }

        if (dest->xbd == NULL)
        {
            free_data_set(dest);
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->ybd != NULL && dest->ybd == NULL)
    {
        if (src->ny > 0)
        {
            dest->ybd = (float *)realloc(dest->ybd, src->ny * sizeof(float));
        }
        else
        {
            dest->ybd = (float *)realloc(dest->ybd, m_min * sizeof(float));
        }

        if (dest->ybd == NULL)
        {
            free_data_set(dest);
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    dest->boxplot_width = src->boxplot_width;
    dest->symb = Mystrdup(src->symb);
    dest->m = src->m;
    dest->color = src->color;
    dest->time = src->time;
    dest->nx = n_min;
    dest->ny = n_min;

    for (j = 0 ; j < n_min ; j++)
    {
        dest->xd[j] = src->xd[j];
        dest->yd[j] = src->yd[j];
    }

    if (src->xe != NULL)
    {
        for (j = 0 ; j < dest->nx ; j++)
        {
            dest->xe[j] = src->xe[j];
        }
    }

    if (src->ye != NULL)
    {
        for (j = 0 ; j < dest->ny ; j++)
        {
            dest->ye[j] = src->ye[j];
        }
    }

    if (src->xed != NULL)
    {
        for (j = 0 ; j < dest->nx ; j++)
        {
            dest->xed[j] = src->xed[j];
        }
    }

    if (src->yed != NULL)
    {
        for (j = 0 ; j < dest->ny ; j++)
        {
            dest->yed[j] = src->yed[j];
        }
    }

    if (src->xbu != NULL)
    {
        for (j = 0 ; j < dest->nx ; j++)
        {
            dest->xbu[j] = src->xbu[j];
        }
    }

    if (src->ybu != NULL)
    {
        for (j = 0 ; j < dest->ny ; j++)
        {
            dest->ybu[j] = src->ybu[j];
        }
    }

    if (src->xbd != NULL)
    {
        for (j = 0 ; j < dest->nx ; j++)
        {
            dest->xbd[j] = src->xbd[j];
        }
    }

    if (src->ybd != NULL)
    {
        for (j = 0 ; j < dest->ny ; j++)
        {
            dest->ybd[j] = src->ybd[j];
        }
    }

    dest->boxplot_width = src->boxplot_width;
    dest->source = Mystrdup(src->source);
    dest->history = Mystrdup(src->history);
    dest->treatement = Mystrdup(src->treatement);

    if (src->special != NULL)
    {
        for (i = 0 ; i < src->n_special ; i++)
        {
            add_to_data_set(dest, IS_SPECIAL, (void *)(src->special[i]));
        }
    }

    for (i = 0 ; i < src->n_lab ; i++)
    {
        sp = src->lab[i];
        dp = (p_l *)calloc(1, sizeof(p_l));

        if (dp == NULL)
        {
            return NULL;
        }

        *dp = *sp;
        dp->text = Mystrdup(sp->text);

        if (add_to_data_set(dest, IS_PLOT_LABEL, (void *)dp))
        {
            return NULL;
        }
    }

    if (ds_duplicate_use != NULL)
    {
        ds_duplicate_use(dest, src);
    }
    for (i = 0 ; i < 16 ; i++)
    {
        dest->user_ispare[i] = src->user_ispare[i];
        dest->user_fspare[i] = src->user_fspare[i];
    }
    for (i = 0 ; i < 8 ; i++)
    {
        dest->src_parameter[i] = src->src_parameter[i];

        if (src->src_parameter_type[i] != NULL && strlen(src->src_parameter_type[i]) > 0)
        {
            dest->src_parameter_type[i] = strdup(src->src_parameter_type[i]);
        }
    }

    return dest;
}


/*  d_s *duplicate_data_set(d_s *src, d_s *dest);
 *  DESCRIPTION Copy the data of src data_set to dest, if dest == NULL
 *          allocate space for the new one_plot
 *
 *
 *  RETURNS     the dest data_set on success, NULL otherwise
 *
 */
d_s *duplicate_partial_data_set(const d_s *src, d_s *dest, int istart, int size)
{
    int i, j;
    p_l *sp = NULL, *dp = NULL;
    int n_min, m_min;

    if (src == NULL)
    {
        return (d_s *) xvin_ptr_error(Wrong_Argument);
    }

    n_min = (src->nx < src->ny) ? src->nx : src->ny;
    m_min = (src->mx < src->my) ? src->mx : src->my;
    m_min = (m_min <= 0) ? 16 : m_min;
    if (istart > m_min) {printf("error_copy 1 \n");return NULL;}

    if (istart + size > m_min)
      size = n_min - istart;



    if ((dest == NULL) || (dest->mx < size) || (dest->my < size))
    {

        if (size > 0)
        {
            dest = build_adjust_data_set(dest, size, size);
        }
        else
        {
            dest = build_adjust_data_set(dest, size, size);

            if (dest != NULL)
            {
                dest->nx = dest->ny = size;
            }
        }

        if (dest == NULL)
        {
          printf("error_copy 5 size=%d\n",size);
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    dest->nx = size;
    dest->ny = size;

    if (src->xe != NULL && dest->xe == NULL)
    {
        if (dest->nx > 0)
        {
            dest->xe = (float *)realloc(dest->xe, dest->nx * sizeof(float));
        }
        if (dest->xe == NULL)
        {
            free_data_set(dest);
            printf("error_copy 6 \n");
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->ye != NULL && dest->ye == NULL)
    {
        if (dest->ny > 0)
        {
            dest->ye = (float *)realloc(dest->ye, dest->ny * sizeof(float));
        }
        if (dest->ye == NULL)
        {
            free_data_set(dest);
            printf("error_copy 7 \n");
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->xed != NULL && dest->xed == NULL)
    {
        if (dest->nx > 0)
        {
            dest->xed = (float *)realloc(dest->xed, dest->nx * sizeof(float));
        }
        if (dest->xed == NULL)
        {
            free_data_set(dest);
            printf("error_copy 8 \n");
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->yed != NULL && dest->yed == NULL)
    {
        if (dest->ny > 0)
        {
            dest->yed = (float *)realloc(dest->yed, dest->ny * sizeof(float));
        }
        if (dest->yed == NULL)
        {
            free_data_set(dest);
            printf("error_copy 9 \n");
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->xbu != NULL && dest->xbu == NULL)
    {
        if (dest->nx > 0)
        {
            dest->xbu = (float *)realloc(dest->xbu, dest->nx * sizeof(float));
        }
        if (dest->xbu == NULL)
        {
            free_data_set(dest);
            printf("error_copy 10 \n");
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->ybu != NULL && dest->ybu == NULL)
    {
        if (dest->ny > 0)
        {
            dest->ybu = (float *)realloc(dest->ybu, dest->ny * sizeof(float));
        }
        if (dest->ybu == NULL)
        {
            free_data_set(dest);
            printf("error_copy 11 \n");
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->xbd != NULL && dest->xbd == NULL)
    {
        if (dest->nx > 0)
        {
            dest->xbd = (float *)realloc(dest->xbd, dest->nx * sizeof(float));
        }
        if (dest->xbd == NULL)
        {
            free_data_set(dest);
            printf("error_copy 12 \n");
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    if (src->ybd != NULL && dest->ybd == NULL)
    {
        if (dest->ny > 0)
        {
            dest->ybd = (float *)realloc(dest->ybd, dest->ny * sizeof(float));
        }
        if (dest->ybd == NULL)
        {
            free_data_set(dest);
            printf("error_copy 13 \n");
            return (d_s *) xvin_ptr_error(Out_Of_Memory);
        }
    }

    dest->boxplot_width = src->boxplot_width;
    dest->symb = Mystrdup(src->symb);
    dest->m = src->m;
    dest->color = src->color;
    dest->time = src->time;

    for (j = 0 ; istart + j < n_min && j < size ; j++)
    {
        dest->xd[j] = src->xd[j+istart];
        dest->yd[j] = src->yd[j+istart];
    }

    if (src->xe != NULL)
    {
      for (j = 0 ; istart + j < n_min && j < size ; j++)
        {
            dest->xe[j] = src->xe[j + istart];
        }
    }

    if (src->ye != NULL)
    {
      for (j = 0 ; istart + j < n_min && j < size ; j++)
        {
            dest->ye[j] = src->ye[j + istart];
        }
    }

    if (src->xed != NULL)
    {
      for (j = 0 ; istart + j < n_min && j < size ; j++)
        {
            dest->xed[j] = src->xed[j + istart];
        }
    }

    if (src->yed != NULL)
    {
      for (j = 0 ; istart + j < n_min && j < size ; j++)
        {
            dest->yed[j] = src->yed[j + istart];
        }
    }

    if (src->xbu != NULL)
    {
      for (j = 0 ; istart + j < n_min && j < size ; j++)
        {
            dest->xbu[j] = src->xbu[j + istart];
        }
    }

    if (src->ybu != NULL)
    {
      for (j = 0 ; istart + j < n_min && j < size ; j++)
        {
            dest->ybu[j] = src->ybu[j + istart];
        }
    }

    if (src->xbd != NULL)
    {
      for (j = 0 ; istart + j < n_min && j < size ; j++)
        {
            dest->xbd[j] = src->xbd[j + istart];
        }
    }

    if (src->ybd != NULL)
    {
      for (j = 0 ; istart + j < n_min && j < size ; j++)
        {
            dest->ybd[j] = src->ybd[j + istart];
        }
    }

    dest->boxplot_width = src->boxplot_width;
    dest->source = Mystrdup(src->source);
    dest->history = Mystrdup(src->history);
    dest->treatement = Mystrdup(src->treatement);

    if (src->special != NULL)
    {
        for (i = 0 ; i < src->n_special ; i++)
        {
            add_to_data_set(dest, IS_SPECIAL, (void *)(src->special[i]));
        }
    }

    for (i = 0 ; i < src->n_lab ; i++)
    {
        sp = src->lab[i];
        dp = (p_l *)calloc(1, sizeof(p_l));

        if (dp == NULL)
        {
            printf("error_copy 2\n");
            return NULL;
        }

        *dp = *sp;
        dp->text = Mystrdup(sp->text);

        if (add_to_data_set(dest, IS_PLOT_LABEL, (void *)dp))
        {
            printf("error_copy 3\n");
            return NULL;
        }
    }

    if (ds_duplicate_use != NULL)
    {
        ds_duplicate_use(dest, src);
    }
    for (i = 0 ; i < 16 ; i++)
    {
        dest->user_ispare[i] = src->user_ispare[i];
        dest->user_fspare[i] = src->user_fspare[i];
    }
    for (i = 0 ; i < 8 ; i++)
    {
        dest->src_parameter[i] = src->src_parameter[i];

        if (src->src_parameter_type[i] != NULL && strlen(src->src_parameter_type[i]) > 0)
        {
            dest->src_parameter_type[i] = strdup(src->src_parameter_type[i]);
        }
    }
printf("copy good \n");
    return dest;
}

int set_src_parameter(d_s *ds, int param_nb, float value, char *type)
{
    int i, j;

    if (ds == NULL)
    {
        return -1;
    }

    if (param_nb < 0 || param_nb >= 8)
    {
        if (type == NULL)
        {
            return -2;
        }

        for (i = 0, j = 1; i < 8 && j != 0; i++)
        {
            if (ds->src_parameter_type[i] != NULL)
            {
                j = strcmp(type, ds->src_parameter_type[i]);
            }

            if (j == 0)
            {
                break;
            }
        }

        if (j == 0)
        {
            ds->src_parameter[i] = value;
        }
        else
        {
            for (i = 0; i < 8; i++)
                if (ds->src_parameter_type[i] == NULL)
                {
                    break;
                }

            if (i < 8)
            {
                ds->src_parameter[i] = value;

                if (strlen(type) > 0)
                {
                    ds->src_parameter_type[i] = strdup(type);
                }

                return i;
            }
        }
    }
    else
    {
        ds->src_parameter[param_nb] = value;

        if (ds->src_parameter_type[param_nb] != NULL)
        {
            free(ds->src_parameter_type[param_nb]);
            ds->src_parameter_type[param_nb] = NULL;
        }

        if (strlen(type) > 0)
        {
            ds->src_parameter_type[param_nb] = strdup(type);
        }

        return param_nb;
    }

    return -3;
}

int     remove_point_from_data_set(d_s *src, int n_one)
{
    int n_min, j;

    if (src == NULL)
    {
        return 1;
    }

    n_min = (src->nx < src->ny) ? src->nx : src->ny;

    if (n_one < 0 || n_one >= n_min)
    {
        return 1;
    }

    if (n_min < 1)
    {
        return 1;
    }

    for (j = n_one ; j < n_min - 1 ; j++)
    {
        src->xd[j] = src->xd[j + 1];
        src->yd[j] = src->yd[j + 1];

        if (src->xe != NULL)
        {
            src->xe[j] = src->xe[j + 1];
        }

        if (src->ye != NULL)
        {
            src->ye[j] = src->ye[j + 1];
        }

        if (src->xed != NULL)
        {
            src->xed[j] = src->xed[j + 1];
        }

        if (src->yed != NULL)
        {
            src->yed[j] = src->yed[j + 1];
        }

        if (src->xbu != NULL)
        {
            src->xbu[j] = src->xbu[j + 1];
        }

        if (src->ybu != NULL)
        {
            src->ybu[j] = src->ybu[j + 1];
        }

        if (src->xbd != NULL)
        {
            src->xbd[j] = src->xbd[j + 1];
        }

        if (src->ybd != NULL)
        {
            src->ybd[j] = src->ybd[j + 1];
        }
    }

    src->nx--;
    src->ny--;
    return 0;
}

int read_bead_number_from_ds_source(d_s *src,int *bead_nbr)
{

  bool is_digit=1;
  int bead_nb=0,iter=0;
  const char bead[8]="bead";
  char* pos=NULL;
  //int temp=0;


  if (src==NULL) return 1;
  if (src->source == NULL) return 1;
  pos=strstr(src->source,bead);


  if (pos==NULL) return 1;
  pos+=5; //position du premier chiffre avprès l'espace
  // s'il ne trouve pas bead dans l'espa
  for (iter=0;is_digit==1;iter++) //regarde le nombre de chiffres
{
  if (*(pos+iter)>='0' && *(pos+iter)<='9') ;
  else is_digit=0;
}
iter-=1;
if (iter==0) return 1;

for (int iter2=0;iter2<iter;iter2++)
{

  bead_nb+=pow(10,iter-iter2-1)*((int)(*(pos+iter2)-'0'));
}
 *bead_nbr=bead_nb;

return 0;
}



int     remove_all_point_from_data_set(d_s *src)
{
    if (src == NULL)
    {
        return 1;
    }

    src->nx = 0;
    src->ny = 0;
    return 0;
}
int    insert_new_point_in_ds(d_s *ds,
                  int pt_index,      // place to insert point
                  float x, float y,  // x and y value of the new point
                  float dx, float dy)// if the data set has error in x then dx will beused as error (same for dy)
{
    int i, nxn, nyn;  // pb refresh ....

    if (ds == NULL)       return xvin_error(Wrong_Argument);
    if (pt_index < 0 || pt_index > ds->nx)  return 1;

    for (nxn = ds->nx; ds->nx >= ds->mx; ds->mx += 16, nxn = ds->mx);

    for (nyn = ds->ny; ds->ny >= ds->my; ds->my += 16, nyn = ds->my);

    if (nxn > ds->nx || nyn > ds->ny)
    {
        ds = build_adjust_data_set(ds, nxn, nyn);
    }
    if (ds == NULL)   return xvin_error(Out_Of_Memory);
    for (i = ds->nx-1; i >= pt_index; i--)
      {   // we move existing data to index + 1 to make room for the new point
    ds->xd[i+1] = ds->xd[i];
    ds->yd[i+1] = ds->yd[i];
    if (ds->xe != NULL) ds->xe[i+1] = ds->xe[i];
    if (ds->ye != NULL) ds->ye[i+1] = ds->ye[i];
    if (ds->xed != NULL) ds->xed[i+1] = ds->xed[i];
    if (ds->yed != NULL) ds->yed[i+1] = ds->yed[i];
    if (ds->xbu != NULL) ds->xbu[i+1] = ds->xbu[i];
    if (ds->ybu != NULL) ds->ybu[i+1] = ds->ybu[i];
    if (ds->xbd != NULL) ds->xbd[i+1] = ds->xbd[i];
    if (ds->ybd != NULL) ds->ybd[i+1] = ds->ybd[i];
      }
    ds->xd[pt_index] = x;
    ds->yd[pt_index] = y;
    if (ds->xe != NULL) ds->xe[pt_index] = dx;
    if (ds->ye != NULL) ds->ye[pt_index] = dy;
    ds->nx++;
    ds->ny++;
    return 0;
}

int     add_new_point_to_ds(d_s *ds, float x, float y)
{
    int i, nxn, nyn;  // pb refresh ....

    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    for (nxn = ds->nx; ds->nx >= ds->mx; ds->mx += 16, nxn = ds->mx);

    for (nyn = ds->ny; ds->ny >= ds->my; ds->my += 16, nyn = ds->my);

    if (nxn > ds->nx || nyn > ds->ny)
    {
        ds = build_adjust_data_set(ds, nxn, nyn);
    }

    if (ds == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    i = (ds->nx < ds->ny) ? ds->nx : ds->ny;
    ds->xd[i] = x;
    ds->yd[i] = y;
    ds->nx++;
    ds->ny++;
    return 0;
}

int    add_new_point_with_y_error_to_ds(d_s *ds, float x, float y, float dy)
{
    int i, nxn, nyn;  // pb refresh ....

    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    for (nxn = ds->nx; ds->nx >= ds->mx; ds->mx += 16, nxn = ds->mx);

    for (nyn = ds->ny; ds->ny >= ds->my; ds->my += 16, nyn = ds->my);

    if (nxn > ds->nx || nyn > ds->ny)
    {
        //win_printf("y error build adj\n nx %d nxn %d mx %d\n ny %d nyn %d my %d"
        //     ,ds->nx,nxn,ds->mx,ds->ny,nyn,ds->my);
        ds = build_adjust_data_set(ds, nxn, nyn);
    }

    if (ds == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    if (ds->ye == NULL)
    {
        ds = alloc_data_set_y_error(ds);
    }

    if (ds == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    i = (ds->nx < ds->ny) ? ds->nx : ds->ny;
    ds->xd[i] = x;
    ds->yd[i] = y;
    ds->ye[i] = dy;
    ds->nx++;
    ds->ny++;
    return 0;
}

int    add_new_point_with_x_error_to_ds(d_s *ds, float x, float dx, float y)
{
    int i, nxn, nyn;  // pb refresh ....

    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    for (nxn = ds->nx; ds->nx >= ds->mx; ds->mx += 16, nxn = ds->mx);

    for (nyn = ds->ny; ds->ny >= ds->my; ds->my += 16, nyn = ds->my);

    if (nxn > ds->nx || nyn > ds->ny)
    {
        ds = build_adjust_data_set(ds, nxn, nyn);
    }

    if (ds == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    if (ds->xe == NULL)
    {
        ds = alloc_data_set_x_error(ds);
    }

    if (ds == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    i = (ds->nx < ds->ny) ? ds->nx : ds->ny;
    ds->xd[i] = x;
    ds->yd[i] = y;
    ds->xe[i] = dx;
    ds->nx++;
    ds->ny++;
    return 0;
}

int    add_new_point_with_xy_error_to_ds(d_s *ds, float x, float dx, float y, float dy)
{
    int i, nxn, nyn;  // pb refresh ....

    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    for (nxn = ds->nx; ds->nx >= ds->mx; ds->mx += 16, nxn = ds->mx);

    for (nyn = ds->ny; ds->ny >= ds->my; ds->my += 16, nyn = ds->my);

    if (nxn > ds->nx || nyn > ds->ny)
    {
        ds = build_adjust_data_set(ds, nxn, nyn);
    }

    if (ds == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    if (ds->ye == NULL)
    {
        ds = alloc_data_set_y_error(ds);
    }

    if (ds == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    if (ds->xe == NULL)
    {
        ds = alloc_data_set_x_error(ds);
    }

    if (ds == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    i = (ds->nx < ds->ny) ? ds->nx : ds->ny;
    ds->xd[i] = x;
    ds->yd[i] = y;
    ds->ye[i] = dy;
    ds->xe[i] = dx;
    ds->nx++;
    ds->ny++;
    return 0;
}



float       *get_ds_x_ptr(d_s *ds)
{
    if (ds == NULL)
    {
        return (float *) xvin_ptr_error(Wrong_Argument);
    }

    return (ds->xd);
}
float       *get_ds_y_ptr(d_s *ds)
{
    if (ds == NULL)
    {
        return (float *) xvin_ptr_error(Wrong_Argument);
    }

    return (ds->yd);
}
int set_ds_dash_line(d_s *ds)
{
    if (ds == NULL)
    {
        return 1;
    }
    else
    {
        ds->m = 0;
    }

    return 0;
}
int set_ds_dot_line(d_s *ds)
{
    if (ds == NULL)
    {
        return 1;
    }
    else
    {
        ds->m = 2;
    }

    return 0;
}
int set_ds_plain_line(d_s *ds)
{
    if (ds == NULL)
    {
        return 1;
    }
    else
    {
        ds->m = 1;
    }

    return 0;
}
int     set_ds_point_symbol(d_s *ds, char *symb)
{
    if (ds == NULL)
    {
        return 1;
    }
    else if (symb == NULL)
    {
        if (ds->symb != NULL)
        {
            free(ds->symb);
            ds->symb = NULL;
        }

        ds->symb = NULL;
    }
    else if (strlen(symb) > 0)
    {
        ds->symb = strdup(symb);
    }

    return 0;
}
int     set_ds_line_color(d_s *ds, int color)
{
    if (ds == NULL)
    {
        return 1;
    }
    else
    {
        ds->color = color;
    }

    return 0;
}
int inherit_from_ds_to_ds(d_s *dest, const d_s *src)
{
    char *s = NULL, c[2] = {0};
    int i;

    if (dest == NULL || src == NULL)
    {
        return 1;
    }

    dest->source = Mystrdupre(dest->source, src->source);

    if (dest->history != NULL)
    {
        free(dest->history);
        dest->history = NULL;
    }

    i = 0;

    if (src->treatement != NULL)
    {
        i = strlen(src->treatement);
    }

    if (src->history != NULL)
    {
        i += strlen(src->history);
    }

    i += 2;
    s = (char *)calloc(i, sizeof(char));

    if (s == NULL)
    {
        return 1;
    }

    if (src->treatement != NULL && src->history != NULL)
    {
        snprintf(s, i, "%s\n%s", src->history, src->treatement);
    }
    else if (src->treatement != NULL)
    {
        snprintf(s, i, "%s", src->treatement);
    }
    else if (src->history != NULL)
    {
        snprintf(s, i, "%s", src->history);
    }
    else
    {
        s[0] = 0;
    }

    if (s[0] != 0)
    {
        dest->history = s;
    }
    else
    {
        free(s);
    }

    remove_from_data_set(dest, ALL_SPECIAL, (void *)c);

    for (i = 0; i < src->n_special; i++)
    {
        add_to_data_set(dest, IS_SPECIAL, (void *)(src->special[i]));
    }

    if (ds_duplicate_use != NULL)
    {
        ds_duplicate_use(dest, src);
    }

    for (i = 0 ; i < 16 ; i++)
    {
        dest->user_ispare[i] = src->user_ispare[i];
        dest->user_fspare[i] = src->user_fspare[i];
    }
    for (i = 0 ; i < 8 ; i++)
    {
        dest->src_parameter[i] = src->src_parameter[i];

        if (src->src_parameter_type[i] != NULL && strlen(src->src_parameter_type[i]) > 0)
        {
            dest->src_parameter_type[i] = strdup(src->src_parameter_type[i]);
        }
    }



    return 0;
}
//int     set_ds_source(d_s *ds, char *format, ...) __attribute__ ((format (gnu_printf, 2, 3)));
int     set_ds_source(d_s *ds, char *format, ...)
{
    char *c = NULL;
    va_list ap;

    if (ds == NULL || format == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    c = (char *)calloc(2048, sizeof(char));

    if (c == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    va_start(ap, format);
    vsnprintf(c, 2048, format, ap);
    va_end(ap);

    if (ds->source != NULL)
    {
        free(ds->source);
        ds->source = NULL;
    }

    if (strlen(c) > 0)
    {
        ds->source = strdup(c);
    }

    free(c);

    if (ds->source == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    return 0;
}
int     set_ds_history(d_s *ds, char *format, ...)
{
    char *c = NULL;
    va_list ap;

    if (ds == NULL || format == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    c = (char *)calloc(8192, sizeof(char));

    if (c == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    va_start(ap, format);
    vsnprintf(c, 8192, format, ap);
    va_end(ap);

    if (ds->history != NULL)
    {
        free(ds->history);
        ds->history = NULL;
    }

    if (strlen(c) > 0)
    {
        ds->history = strdup(c);
    }

    free(c);

    if (ds->history == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    return 0;
}
int update_ds_treatement(d_s *ds, char *format, ...)
{
    char *s = NULL;
    int i;
    va_list ap;

    if (ds == NULL || format == NULL)   return xvin_error(Wrong_Argument);

    i = 0;

    if (ds->treatement != NULL)  i = strlen(ds->treatement);

    if (ds->history != NULL) i += strlen(ds->history);

    i += 2;
    s = (char *)calloc(i, sizeof(char));

    if (s == NULL)  return xvin_error(Out_Of_Memory);

    if (ds->treatement != NULL && ds->history != NULL)
        snprintf(s, i, "%s\n%s", ds->history, ds->treatement);
    else if (ds->treatement != NULL)
        snprintf(s, i, "%s", ds->treatement);
    else if (ds->history != NULL)
        snprintf(s, i, "%s", ds->history);
    else    s[0] = 0;

    if (s[0] != 0)
    {
        if (ds->history != NULL) free(ds->history);

        ds->history = s;
    }
    else    free(s);

    va_start(ap, format);
    vset_formated_string(&(ds->treatement), format, ap);
    va_end(ap);
    return 0;
}




int     set_ds_treatement(d_s *ds, char *format, ...)
{
    char *c = NULL;
    va_list ap;

    if (ds == NULL || format == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    c = (char *)calloc(2048, sizeof(char));

    if (c == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    va_start(ap, format);
    vsnprintf(c, 2048, format, ap);
    va_end(ap);

    if (ds->treatement != NULL)
    {
        free(ds->treatement);
        ds->treatement = NULL;
    }

    if (strlen(c) > 0)
    {
        ds->treatement = strdup(c);
    }

    free(c);

    if (ds->treatement == NULL)
    {
        return xvin_error(Out_Of_Memory);
    }

    return 0;
}
/*  int add_to_data_set(d_s *ds, int type, void *stuff);
 *  int remove_from_data_set(d_s *ds, int type, void *stuff);
 *  DESCRIPTION
 *
 *  AC 01.93
 *
 *  RETURNS     0 on success, 1
 *
 */
int add_to_data_set(d_s *ds, int type, void *stuff)
{
    if (stuff == NULL || ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }
    else if (type == IS_SPECIAL && strlen((char *)stuff) > 0)
    {
        if (ds->n_special >=  ds->m_special)
        {
            ds->m_special++;
            ds->special = (char **)realloc
                          (ds->special, ds->m_special * sizeof(char *));

            if (ds->special == NULL)
            {
                return xvin_error(Out_Of_Memory);
            }
        }

        ds->special[ds->n_special] = strdup((char *)stuff);
        ds->n_special++;
    }
    else if (type == IS_PLOT_LABEL)
    {
        if (ds->n_lab >= ds->m_lab)
        {
            ds->m_lab += MAX_DATA;
            ds->lab = (p_l **)realloc(ds->lab, ds->m_lab * sizeof(p_l *));

            if (ds->lab == NULL)
            {
                return xvin_error(Out_Of_Memory);
            }
        }

        if (((p_l *)stuff)->text != NULL)   /* no empty label */
        {
            ds->lab[ds->n_lab] = (p_l *)stuff;
            ds->n_lab++;
        }

        ds->need_to_refresh = 1;
    }

    return 0;
}

int remove_plot_label_from_ds(d_s *ds, p_l *pl)
{
    int i = 0, j;

    if (ds == NULL || pl == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    for (i = 0, j = -1 ; (i < ds->n_lab) && (j < 0) ; i++)
    {
        j = (ds->lab[i] == pl) ? i : j;
    }

    if (j >= 0)
    {
        if (ds->lab[j]->text != NULL)
        {
            free(ds->lab[j]->text);
            ds->lab[j]->text = NULL;
        }

        if (ds->lab[j] != NULL)
        {
            free(ds->lab[j]);
            ds->lab[j] = NULL;
        }

        ds->n_lab--;

        for (i = j ; i < ds->n_lab ; i++)
        {
            ds->lab[i] = ds->lab[i + 1];
        }

        ds->need_to_refresh = 1;
    }

    return 0;
}

int remove_all_plot_label_from_ds(d_s *ds)
{
    int i = 0;

    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    if (ds->n_lab == 0)
    {
        return 0;
    }

    for (i = ds->n_lab - 1; i >= 0; i--)
    {
        if (ds->lab[i]->text != NULL)
        {
            free(ds->lab[i]->text);
            ds->lab[i]->text = NULL;
        }

        if (ds->lab[i] != NULL)
        {
            free(ds->lab[i]);
            ds->lab[i] = NULL;
        }

        ds->n_lab--;
    }

    ds->n_lab = 0;
    ds->need_to_refresh = 1;
    return 0;
}

int remove_from_data_set(d_s *ds, int type, void *stuff)
{
    int i = 0, j;

    if (stuff == NULL || ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }
    else if (type == IS_SPECIAL)
    {
        for (i = 0, j = -1; (i < ds->n_special) && (j < 0); i++)
        {
            if ((void *)(ds->special[i]) == stuff)
            {
                j = i;
            }
        }

        if (j >= 0)
        {
            if (ds->special[j] != NULL)
            {
                free(ds->special[j]);
                ds->special[j] = NULL;
            }

            ds->n_special--;

            for (i = j ; i < ds->n_special ; i++)
            {
                ds->special[i] = ds->special[i + 1];
            }

            i = 0;
        }

        /* ds->special n'est pas libere avec le dernier special */
        /* ds->m_special n'est jamais decremente */
    }
    else if (type == ALL_SPECIAL)
    {
        for (i = 0; i < ds->n_special; i++)
        {
            if (ds->special[i] != NULL)
            {
                free(ds->special[i]);
                ds->special[i] = NULL;
            }
        }

        ds->n_special = ds->m_special = 0;

        if (ds->special != NULL)
        {
            free(ds->special);
            ds->special = NULL;
        }
    }
    else if (type == IS_PLOT_LABEL)
    {
        for (i = 0, j = -1 ; (i < ds->n_lab) && (j < 0) ; i++)
        {
            if ((void *)(ds->lab[i]) == stuff)
            {
                j = i;
            }
        }

        if (j >= 0)
        {
            if (ds->lab[j]->text != NULL)
            {
                free(ds->lab[j]->text);
                ds->lab[j]->text = NULL;
            }

            if (ds->lab[j] != NULL)
            {
                free(ds->lab[j]);
                ds->lab[j] = NULL;
            }

            ds->n_lab--;

            for (i = j ; i < ds->n_lab ; i++)
            {
                ds->lab[i] = ds->lab[i + 1];
            }

            ds->need_to_refresh = 1;
        }
    }

    return 0;
}


int push_plot_label_in_ds(d_s *ds, float tx, float ty, char *label, int type)
{
    p_l *pl = NULL;

    if (ds == NULL || label == NULL || strlen(label) < 1)
    {
        return 1;
    }

    pl = (p_l *)calloc(1, sizeof(struct plot_label));

    if (pl == NULL)
    {
        fprintf(stderr, "realloc error \n");
        exit(1);
    }

    pl->xla = tx;
    pl->yla = ty;
    pl->text = (char *)strdup(label);
    pl->type = type;

    if (pl->text == NULL)
    {
        fprintf(stderr, "strdup error \n");
        return 1; //exit(1);
    }

    return add_to_data_set(ds, IS_PLOT_LABEL, (void *)pl);
}

int set_ds_plot_label(d_s *ds, float tx, float ty, int type, const char *format, ...)
{
    char *c = NULL;
    va_list ap;
    p_l *pl = NULL;

    if (ds == NULL)
    {
        return xvin_error(Wrong_Argument);
    }

    if (format != NULL)
    {
        c = (char *)calloc(2048, sizeof(char));

        if (c == NULL)
        {
            return xvin_error(Out_Of_Memory);
        }

        va_start(ap, format);
        vsnprintf(c, 2048, format, ap);
        va_end(ap);

        if (strlen(c) < 1)
        {
            return xvin_error(Wrong_Argument);
        }

        pl = (p_l *)calloc(1, sizeof(struct plot_label));

        if (pl == NULL)
        {
            free(c);
            return xvin_error(Out_Of_Memory);
        }

        pl->xla = tx;
        pl->yla = ty;

        if (strlen(c) > 0)
        {
            pl->text = (char *)strdup(c);
        }

        pl->type = type;

        if (pl->text == NULL)
        {
            free(c);
            free(pl);
            return xvin_error(Out_Of_Memory);
        }

        add_to_data_set(ds, IS_PLOT_LABEL, (void *)pl);
    }
    else
    {
        return xvin_error(Wrong_Argument);
    }

    ds->need_to_refresh = 1;

    if (c != NULL)
    {
        free(c);
    }

    return 0;
}

int set_ds_starting_time(d_s *ds)
{
    if (ds == NULL)
    {
        return 1;
    }

#ifdef XV_WIN32
    time(&(ds->time));
    ds->record_start =  my_ulclock();
#endif
    return 0;
}

int set_ds_ending_time(d_s *ds)
{
    if (ds == NULL)
    {
        return 1;
    }

#ifdef XV_WIN32
    ds->record_duration = (double)(my_ulclock() - ds->record_start) / get_my_ulclocks_per_sec();
#endif
    return 0;
}


# endif



int swap_ds_x_y(d_s *ds)
{
    float *tmp = NULL;
    int tmpi = 0;

    if (!ds || ds->nx != ds->ny)
    {
        return -1;
    }

    tmp = ds->xd;
    ds->xd = ds->yd;
    ds->yd = tmp;
    tmp = ds->xe;
    ds->xe = ds->ye;
    ds->ye = tmp;
    tmp = ds->xed;
    ds->xed = ds->yed;
    ds->yed = tmp;
    tmp = ds->xbu;
    ds->xbu = ds->ybu;
    ds->ybu = tmp;
    tmp = ds->xbd;
    ds->xbd = ds->ybd;
    ds->ybd = tmp;
    tmpi = ds->mx;
    ds->mx = ds->my;
    ds->my = tmpi;
    tmpi = ds->nx;
    ds->nx = ds->ny;
    ds->ny = tmpi;
    return 0;
}

d_s *ds_1_over_y(d_s *dsi1)
{
    d_s *dss;
    int i;
    double tmp;


    if (dsi1 == NULL) return NULL;

    dss = duplicate_data_set(dsi1, NULL);
    if (dss->ye != NULL && dss->yed == NULL) alloc_data_set_y_down_error(dss);
    if (dss->ybu != NULL && dss->ybd == NULL) alloc_data_set_y_box_dwn(dss);
    if (dss == NULL)
      {
	return NULL; //(win_printf_OK("can't create data set"));
      }

    for (i = 0 ; i < dsi1->nx ; i++)
      {
	dss->yd[i] = (dsi1->yd[i] != 0) ? (float)1/(dsi1->yd[i]) : 0;
	if (dss->ye)
	  {
	    if (dsi1->yd[i] > 0)
	      {
		if (dsi1->yed == NULL)
		  {
		    tmp = (double)1/(dsi1->yd[i]-fabs(dsi1->ye[i]));
		    tmp -= (double)1/(dsi1->yd[i]);
		    dss->ye[i] = (float)tmp;
		  }
		else
		  {
		    tmp = (double)1/(dsi1->yd[i]-fabs(dsi1->yed[i]));
		    tmp -= (double)1/(dsi1->yd[i]);
		    dss->ye[i] = (float)tmp;
		  }
		tmp = (double)1/(dsi1->yd[i]);
		tmp -= (double)1/(dsi1->yd[i]+fabs(dsi1->ye[i]));
		dss->yed[i] = (float)tmp;
	      }
	    else
	      {
		if (dsi1->yed == NULL)
		  {
		    tmp = (double)1/(dsi1->yd[i]+fabs(dsi1->ye[i]));
		    tmp -= (double)1/(dsi1->yd[i]);
		    dss->ye[i] = (float)tmp;
		  }
		else
		  {
		    tmp = (double)1/(dsi1->yd[i]+fabs(dsi1->yed[i]));
		    tmp -= (double)1/(dsi1->yd[i]);
		    dss->ye[i] = (float)tmp;
		  }
		tmp = (double)1/(dsi1->yd[i]);
		tmp -= (double)1/(dsi1->yd[i]-fabs(dsi1->ye[i]));
		dss->yed[i] = (float)tmp;
	      }
	  }
	if (dss->ybu)
	  {
	    if (dsi1->yd[i] > 0)
	      {
		if (dsi1->ybd == NULL)
		  {
		    tmp = (double)1/(dsi1->yd[i]-fabs(dsi1->ybu[i]));
		    tmp -= (double)1/(dsi1->yd[i]);
		    dss->ybu[i] = (float)tmp;
		  }
		else
		  {
		    tmp = (double)1/(dsi1->yd[i]-fabs(dsi1->ybd[i]));
		    tmp -= (double)1/(dsi1->yd[i]);
		    dss->ybu[i] = (float)tmp;
		  }
		tmp = (double)1/(dsi1->yd[i]);
		tmp -= (double)1/(dsi1->yd[i]+fabs(dsi1->ybu[i]));
		dss->ybd[i] = (float)tmp;
	      }
	    else
	      {
		if (dsi1->ybd == NULL)
		  {
		    tmp = (double)1/(dsi1->yd[i]+fabs(dsi1->ybu[i]));
		    tmp -= (double)1/(dsi1->yd[i]);
		    dss->ybu[i] = (float)tmp;
		  }
		else
		  {
		    tmp = (double)1/(dsi1->yd[i]+fabs(dsi1->ybd[i]));
		    tmp -= (double)1/(dsi1->yd[i]);
		    dss->ybu[i] = (float)tmp;
		  }
		tmp = (double)1/(dsi1->yd[i]);
		tmp -= (double)1/(dsi1->yd[i]-fabs(dsi1->ybu[i]));
		dss->ybd[i] = (float)tmp;
	      }
	  }
      }
    inherit_from_ds_to_ds(dss, dsi1);
    set_ds_treatement(dss, "ds inversed 1/y");
    return dss;
}



d_s *ds_log10(d_s *dsi1)
{
    d_s *dss;
    int i;

    if (dsi1 == NULL) return NULL;

    dss = duplicate_data_set(dsi1, NULL);
    if (dss->ye != NULL && dss->yed == NULL) alloc_data_set_y_down_error(dss);
    if (dss->ybu != NULL && dss->ybd == NULL) alloc_data_set_y_box_dwn(dss);
    if (dss == NULL)
      {
	return NULL;//(win_printf_OK("can't create data set"));
      }

    for (i = 0 ; i < dsi1->nx ; i++)
      {
	dss->yd[i] = (dsi1->yd[i] > 0) ? log10(dsi1->yd[i]) : -20;

	if (dss->ye)
	  {
	    dss->ye[i] = (dsi1->yd[i] >= 0)
	      ? log10(dsi1->yd[i]+fabs(dsi1->ye[i])) - log10(dsi1->yd[i]) : 0;
	    if (dsi1->yed == NULL)
	      {
		dss->yed[i] = (dsi1->yd[i] >= 0)
		  ? log10(dsi1->yd[i]) - log10(dsi1->yd[i]-fabs(dsi1->ye[i])) : 0;
	      }
	    else
	      {
		dss->yed[i] = (dsi1->yd[i] >= 0)
		  ? log10(dsi1->yd[i]) - log10(dsi1->yd[i]-fabs(dsi1->yed[i])) : 0;
	      }
	  }
	if (dss->ybu)
	  {
	    dss->ybu[i] = (dsi1->yd[i] >= 0)
	      ? log10(dsi1->yd[i]+fabs(dsi1->ybu[i])) - log10(dsi1->yd[i]) : 0;
	    if (dsi1->ybd == NULL)
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0)
		  ? log10(dsi1->yd[i]) - log10(dsi1->yd[i]-fabs(dsi1->ybu[i])) : 0;
	      }
	    else
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0)
		  ? log10(dsi1->yd[i]) - log10(dsi1->yd[i]-fabs(dsi1->ybd[i])) : 0;
	      }
	  }
      }

    inherit_from_ds_to_ds(dss, dsi1);
    set_ds_treatement(dss, "ds log10 in y");
    return dss;
}


d_s *ds_log(d_s *dsi1)
{
    d_s *dss;
    int i;

    if (dsi1 == NULL) return NULL;

    dss = duplicate_data_set(dsi1, NULL);
    if (dss->ye != NULL && dss->yed == NULL) alloc_data_set_y_down_error(dss);
    if (dss->ybu != NULL && dss->ybd == NULL) alloc_data_set_y_box_dwn(dss);
    if (dss == NULL)
      {
	return NULL; //(win_printf_OK("can't create data set"));
      }

    for (i = 0 ; i < dsi1->nx ; i++)
      {
	dss->yd[i] = (dsi1->yd[i] > 0) ? log(dsi1->yd[i]) : -20;

	if (dss->ye)
	  {
	    dss->ye[i] = (dsi1->yd[i] >= 0)
	      ? log(dsi1->yd[i]+fabs(dsi1->ye[i])) - log(dsi1->yd[i]) : 0;
	    if (dsi1->yed == NULL)
	      {
          if (dsi1->yd[i] >= 0)
          {
            if (dsi1->yd[i]-fabs(dsi1->ye[i]) > 0)
            {
              dss->yed[i] = log(dsi1->yd[i]) - log(dsi1->yd[i]-fabs(dsi1->ye[i]));
            }
            else   dss->yed[i] = log(FLT_MIN);

          }
          else dss->yed[i] = 0;
		dss->yed[i] = (dsi1->yd[i] >= 0)
		  ? log(dsi1->yd[i]) - log(dsi1->yd[i]-fabs(dsi1->ye[i])) : 0;
	      }
	    else
	      {
		dss->yed[i] = (dsi1->yd[i] >= 0)
		  ? log(dsi1->yd[i]) - log(dsi1->yd[i]-fabs(dsi1->yed[i])) : 0;
	      }
	  }
	if (dss->ybu)
	  {
	    dss->ybu[i] = (dsi1->yd[i] >= 0)
	      ? log(dsi1->yd[i]+fabs(dsi1->ybu[i])) - log(dsi1->yd[i]) : 0;
	    if (dsi1->ybd == NULL)
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0)
		  ? log(dsi1->yd[i]) - log(dsi1->yd[i]-fabs(dsi1->ybu[i])) : 0;
	      }
	    else
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0)
		  ? log(dsi1->yd[i]) - log(dsi1->yd[i]-fabs(dsi1->ybd[i])) : 0;
	      }
	  }
      }

    inherit_from_ds_to_ds(dss, dsi1);
    set_ds_treatement(dss, "ds log in y");
    return dss;
}

d_s *ds_log2(d_s *dsi1)
{
    d_s *dss;
    int i;

    if (dsi1 == NULL) return NULL;

    dss = duplicate_data_set(dsi1, NULL);
    if (dss->ye != NULL && dss->yed == NULL) alloc_data_set_y_down_error(dss);
    if (dss->ybu != NULL && dss->ybd == NULL) alloc_data_set_y_box_dwn(dss);
    if (dss == NULL)
      {
	return NULL;//(win_printf_OK("can't create data set"));
      }

    for (i = 0 ; i < dsi1->nx ; i++)
      {
	dss->yd[i] = (dsi1->yd[i] > 0) ? log2(dsi1->yd[i]) : -20;

	if (dss->ye)
	  {
	    dss->ye[i] = (dsi1->yd[i] >= 0)
	      ? log2(dsi1->yd[i]+fabs(dsi1->ye[i])) - log2(dsi1->yd[i]) : 0;
	    if (dsi1->yed == NULL)
	      {
		dss->yed[i] = (dsi1->yd[i] >= 0)
		  ? log2(dsi1->yd[i]) - log2(dsi1->yd[i]-fabs(dsi1->ye[i])) : 0;
	      }
	    else
	      {
		dss->yed[i] = (dsi1->yd[i] >= 0)
		  ? log2(dsi1->yd[i]) - log2(dsi1->yd[i]-fabs(dsi1->yed[i])) : 0;
	      }
	  }
	if (dss->ybu)
	  {
	    dss->ybu[i] = (dsi1->yd[i] >= 0)
	      ? log2(dsi1->yd[i]+fabs(dsi1->ybu[i])) - log2(dsi1->yd[i]) : 0;
	    if (dsi1->ybd == NULL)
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0)
		  ? log2(dsi1->yd[i]) - log2(dsi1->yd[i]-fabs(dsi1->ybu[i])) : 0;
	      }
	    else
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0)
		  ? log2(dsi1->yd[i]) - log2(dsi1->yd[i]-fabs(dsi1->ybd[i])) : 0;
	      }
	  }
      }

    inherit_from_ds_to_ds(dss, dsi1);
    set_ds_treatement(dss, "ds log2 in y");
    return dss;
}

d_s *ds_exp(d_s *dsi1)
{
    d_s *dss;
    int i;

    if (dsi1 == NULL) return NULL;

    dss = duplicate_data_set(dsi1, NULL);
    if (dss->ye != NULL && dss->yed == NULL) alloc_data_set_y_down_error(dss);
    if (dss->ybu != NULL && dss->ybd == NULL) alloc_data_set_y_box_dwn(dss);
    if (dss == NULL)
      {
	return NULL; //(win_printf_OK("can't create data set"));
      }

    for (i = 0 ; i < dsi1->nx ; i++)
      {
	dss->yd[i] = (dsi1->yd[i] >= 88) ? 1e35 : ((dsi1->yd[i] <= -88) ? 1e-35 : exp(dsi1->yd[i]));

	if (dss->ye)
	  {
	    dss->ye[i] = (dsi1->yd[i] >= 0)
	      ? exp(dsi1->yd[i]+fabs(dsi1->ye[i])) - exp(dsi1->yd[i]) : 0;
	    if (dsi1->yed == NULL)
	      {
		dss->yed[i] = (dsi1->yd[i] >= 0)
		  ? exp(dsi1->yd[i]) - exp(dsi1->yd[i]-fabs(dsi1->ye[i])) : 0;
	      }
	    else
	      {
		dss->yed[i] = (dsi1->yd[i] >= 0)
		  ? exp(dsi1->yd[i]) - exp(dsi1->yd[i]-fabs(dsi1->yed[i])) : 0;
	      }
	  }
	if (dss->ybu)
	  {
	    dss->ybu[i] = (dsi1->yd[i] >= 0)
	      ? exp(dsi1->yd[i]+fabs(dsi1->ybu[i])) - exp(dsi1->yd[i]) : 0;
	    if (dsi1->ybd == NULL)
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0)
		  ? exp(dsi1->yd[i]) - exp(dsi1->yd[i]-fabs(dsi1->ybu[i])) : 0;
	      }
	    else
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0)
		  ? exp(dsi1->yd[i]) - exp(dsi1->yd[i]-fabs(dsi1->ybd[i])) : 0;
	      }
	  }
      }

    inherit_from_ds_to_ds(dss, dsi1);
    set_ds_treatement(dss, "ds exp in y");
    return dss;
}

d_s *ds_exp2(d_s *dsi1)
{
    d_s *dss;
    int i;

    if (dsi1 == NULL) return NULL;

    dss = duplicate_data_set(dsi1, NULL);
    if (dss->ye != NULL && dss->yed == NULL) alloc_data_set_y_down_error(dss);
    if (dss->ybu != NULL && dss->ybd == NULL) alloc_data_set_y_box_dwn(dss);
    if (dss == NULL)
      {
	return NULL;//(win_printf_OK("can't create data set"));
      }

    for (i = 0 ; i < dsi1->nx ; i++)
      {
	dss->yd[i] = (dsi1->yd[i] >= 88) ? 1e35 : ((dsi1->yd[i] <= -88) ? 1e-35 : pow(2, dsi1->yd[i]));

	if (dss->ye)
	  {
	    dss->ye[i] = (dsi1->yd[i] >= 0)
	      ? pow(2, dsi1->yd[i]+fabs(dsi1->ye[i])) - pow(2, dsi1->yd[i]) : 0;
	    if (dsi1->yed == NULL)
	      {
		dss->yed[i] = (dsi1->yd[i] >= 0)
		  ? pow(2, dsi1->yd[i]) - pow(2, dsi1->yd[i]-fabs(dsi1->ye[i])) : 0;
	      }
	    else
	      {
		dss->yed[i] = (dsi1->yd[i] >= 0)
		  ? pow(2, dsi1->yd[i]) - pow(2, dsi1->yd[i]-fabs(dsi1->yed[i])) : 0;
	      }
	  }
	if (dss->ybu)
	  {
	    dss->ybu[i] = (dsi1->yd[i] >= 0)
	      ? pow(2, dsi1->yd[i]+fabs(dsi1->ybu[i])) - pow(2, dsi1->yd[i]) : 0;
	    if (dsi1->ybd == NULL)
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0)
		  ? pow(2, dsi1->yd[i]) - pow(2, dsi1->yd[i]-fabs(dsi1->ybu[i])) : 0;
	      }
	    else
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0)
		  ? pow(2, dsi1->yd[i]) - pow(2, dsi1->yd[i]-fabs(dsi1->ybd[i])) : 0;
	      }
	  }
      }

    inherit_from_ds_to_ds(dss, dsi1);
    set_ds_treatement(dss, "ds 2^y");
    return dss;
}

d_s *ds_exp10(d_s *dsi1)
{
    d_s *dss;
    int i;

    if (dsi1 == NULL) return NULL;

    dss = duplicate_data_set(dsi1, NULL);
    if (dss->ye != NULL && dss->yed == NULL) alloc_data_set_y_down_error(dss);
    if (dss->ybu != NULL && dss->ybd == NULL) alloc_data_set_y_box_dwn(dss);
    if (dss == NULL)
      {
	return NULL;//(win_printf_OK("can't create data set"));
      }

    for (i = 0 ; i < dsi1->nx ; i++)
      {
	dss->yd[i] = (dsi1->yd[i] >= 35) ? 1e35 : ((dsi1->yd[i] <= -35) ? 1e-35 : pow(10, dsi1->yd[i]));

	if (dss->ye)
	  {
	    dss->ye[i] = (dsi1->yd[i] >= 0)
	      ? pow(10, dsi1->yd[i]+fabs(dsi1->ye[i])) - pow(10, dsi1->yd[i]) : 0;
	    if (dsi1->yed == NULL)
	      {
		dss->yed[i] = (dsi1->yd[i] >= 0)
		  ? pow(10, dsi1->yd[i]) - pow(10, dsi1->yd[i]-fabs(dsi1->ye[i])) : 0;
	      }
	    else
	      {
		dss->yed[i] = (dsi1->yd[i] >= 0)
		  ? pow(10, dsi1->yd[i]) - pow(10, dsi1->yd[i]-fabs(dsi1->yed[i])) : 0;
	      }
	  }
	if (dss->ybu)
	  {
	    dss->ybu[i] = (dsi1->yd[i] >= 0)
	      ? pow(10, dsi1->yd[i]+fabs(dsi1->ybu[i])) - pow(10, dsi1->yd[i]) : 0;
	    if (dsi1->ybd == NULL)
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0)
		  ? pow(10, dsi1->yd[i]) - pow(10, dsi1->yd[i]-fabs(dsi1->ybu[i])) : 0;
	      }
	    else
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0)
		  ? pow(10, dsi1->yd[i]) - pow(10, dsi1->yd[i]-fabs(dsi1->ybd[i])) : 0;
	      }
	  }
      }

    inherit_from_ds_to_ds(dss, dsi1);
    set_ds_treatement(dss, "ds 10^y");
    return dss;
}
d_s *ds_squareroot(d_s *dsi1)
{
    d_s *dss;
    int i;

    if (dsi1 == NULL) return NULL;

    dss = duplicate_data_set(dsi1, NULL);
    if (dss->ye != NULL && dss->yed == NULL) alloc_data_set_y_down_error(dss);
    if (dss->ybu != NULL && dss->ybd == NULL) alloc_data_set_y_box_dwn(dss);
    if (dss == NULL)
      {
	return NULL;//(win_printf_OK("can't create data set"));
      }

    for (i = 0 ; i < dsi1->nx ; i++)
      {
	dss->yd[i] = (dsi1->yd[i] >= 0) ? sqrtf(dsi1->yd[i]) : -1;

	if (dss->ye)
	  {
	    dss->ye[i] = (dsi1->yd[i] >= 0) ? sqrtf(dsi1->yd[i] + fabs(dsi1->ye[i])) - sqrtf(dsi1->yd[i]) : 0;

	    if (dsi1->yed == NULL)
	      {
		dss->yed[i] = (dsi1->yd[i] >= 0) ? sqrtf(dsi1->yd[i]) - sqrtf(dsi1->yd[i] - fabs(dsi1->ye[i])) : 0;
	      }
	    else
	      {
		dss->yed[i] = (dsi1->yd[i] >= 0) ? sqrtf(dsi1->yd[i]) - sqrtf(dsi1->yd[i] - fabs(dsi1->yed[i])) : 0;
	      }
	  }
	if (dss->ybu)
	  {
	    dss->ybu[i] = (dsi1->yd[i] >= 0) ? sqrtf(dsi1->yd[i] + fabs(dsi1->ybu[i])) - sqrtf(dsi1->yd[i]) : 0;
	    if (dsi1->ybd == NULL)
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0) ? sqrtf(dsi1->yd[i]) - sqrtf(dsi1->yd[i] - fabs(dsi1->ybu[i])) : 0;
	      }
	    else
	      {
		dss->ybd[i] = (dsi1->yd[i] >= 0) ? sqrtf(dsi1->yd[i]) - sqrtf(dsi1->yd[i] - fabs(dsi1->ybd[i])) : 0;
	      }
	  }
      }

    inherit_from_ds_to_ds(dss, dsi1);
    set_ds_treatement(dss, "ds squareroot");
    return dss;
}

d_s *ds_abs(d_s *dsi1)
{
    d_s *dss;
    int i;

    if (dsi1 == NULL) return NULL;

    dss = duplicate_data_set(dsi1, NULL);
    if (dss == NULL)
      {
	return NULL;//win_printf_ptr("can't create data set");
      }

    for (i = 0 ; i < dsi1->nx ; i++)
      {
	dss->yd[i] = fabs(dsi1->yd[i]);

	if (dss->ye)
	  {
	    dss->ye[i] = fabs(dsi1->ye[i]);
	  }
	if (dss->yed == NULL)
	  {
	    dss->yed[i] = fabs(dsi1->yed[i]);
	  }
	if (dss->ybu)
	  {
	    dss->ybu[i] = fabs(dsi1->ybu[i]);
	  }
	if (dss->ybd == NULL)
	  {
	    dss->ybd[i] = fabs(dsi1->ybd[i]);
	  }
      }

    inherit_from_ds_to_ds(dss, dsi1);
    set_ds_treatement(dss, "ds abs");
    return dss;
}


d_s *ds_square(d_s *dsi1)
{
    d_s *dss;
    double tmp;
    int i;

    if (dsi1 == NULL) return NULL;

    dss = duplicate_data_set(dsi1, NULL);
    if (dss->ye != NULL && dss->yed == NULL) alloc_data_set_y_down_error(dss);
    if (dss->ybu != NULL && dss->ybd == NULL) alloc_data_set_y_box_dwn(dss);
    if (dss == NULL)
      {
	return NULL; //(win_printf_OK("can't create data set"));
      }

    for (i = 0 ; i < dsi1->nx ; i++)
      {
	dss->yd[i] = dsi1->yd[i] * dsi1->yd[i];
	if (dss->ye)
	  {
	    tmp = 2 * (fabs(dsi1->yd[i]) * fabs(dsi1->ye[i])) + (dsi1->ye[i]*dsi1->ye[i]);
	    dss->ye[i] = (float)tmp;
	    if (dsi1->yed == NULL)
	      {
		tmp = 2 * (fabs(dsi1->yd[i]) * fabs(dsi1->ye[i])) - (dsi1->ye[i]*dsi1->ye[i]);
		dss->yed[i] = tmp;
	      }
	    else
	      {
		tmp = 2 * (fabs(dsi1->yd[i]) * fabs(dsi1->yed[i])) - (dsi1->yed[i]*dsi1->yed[i]);
		dss->yed[i] = tmp;
	      }
	  }
	if (dss->ybu)
	  {
	    tmp = 2 * (fabs(dsi1->yd[i]) * fabs(dsi1->ybu[i])) + (dsi1->ybu[i]*dsi1->ybu[i]);
	    dss->ybu[i] = (float)tmp;
	    if (dsi1->ybd == NULL)
	      {
		tmp = 2 * (fabs(dsi1->yd[i]) * fabs(dsi1->ybu[i])) - (dsi1->ybu[i]*dsi1->ybu[i]);
		dss->ybd[i] = (float)tmp;
	      }
	    else
	      {
		tmp = 2 * (fabs(dsi1->yd[i]) * fabs(dsi1->ybd[i])) - (dsi1->ybd[i]*dsi1->ybd[i]);
		dss->ybd[i] = (float)tmp;
	      }
	  }
      }

    inherit_from_ds_to_ds(dss, dsi1);
    set_ds_treatement(dss, "ds square");
    return dss;
}
