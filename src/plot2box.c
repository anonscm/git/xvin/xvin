/*
 *	DESCRIPTION
 *
 *	qterm(struct box*);			set the main box
 *
 *
 *	qdchar(char charac);			indicate a single char to
 *						draw at each point
 *	qkd_color(ax_color,gr_color);		set axes and graph color
 *	qkd_lab_color(color);			set labels color
 *	qdbox(char *ch);			indicate a symbol to be plot
 *						using a complete string, ex:
 *						"\pt8 \bu" burst in 8 points
 *
 *	qkxlabl_set(int flag);			do print plot x labeling
 *	qkylabl_set(int flag);
 *	qkxunit(char *prefix, char *unit);
 *	qkyunit(char *prefix, char *unit);
 *	qdset(float axmin, float axmax, float aymin, float aymax);
 *	qdset_abs(float sizex, float offsetx, float  sizey, float offsety);
 *	qdfile(char *fname);
 *	qdset_ticklen(float len);		 set tick length
 *	qkdraw(npts, x, y, opt, xmin, xmax, ymin, ymax)
 *		int npts, opt;
 *		float x[], y[];
 *		float *xmin, *xmax, *ymin, *ymax;
 *	qkdim(int opt, float xmin, float xmax, float ymin, float ymax);
 *	qxlabl(char *string);
 *	qdtitl( char *string);
 *	qylabl(char *string)
 *
 *	free_qkd_box ();
 *	xlabel(char *string, float xpos, float ypos);
 *	ylabel(char *string, float xpos, float ypos);
 *	qkd_get_pos (int ix, int iy, float *xpos, float *ypos, int iopt);
 *
 *	draw(int npts, float *x, float *y, int iopt)
 *	qdaxes(int trim);
 *	qdylab(trim);
 *	qdxlab(trim);
 *	decide_format(vmin,vmax);
 *	qdxtk(int trim)
 *	qdytk(int trim)
 *	set_size(vmin, vmax, rmin, rstep);
 *	qdone();
 *	qkmax(int x, int y);
 *	qkmin(int x, int y);
 *	xloglab(int trim)
 *	xlogtk(int trim);
 *	ylogtk(int trim);
 *	yloglab(int trim);
 *	init_box (struct box *b0);
 *
 */

# ifndef _PLOT2BOX_C_
# define _PLOT2BOX_C_

# define TKLEN 0.15
# define N2 5
# define N1 6

# define PC_FEN 15
# define LINE_B 1
# define MOVE_B 0

/* a virer wr_polygone dans erase_redraw */

# include <stdio.h>
# include <math.h>
# include <string.h>
# include <malloc.h>
# include <stdlib.h>
# include <limits.h>
//# include <float.h>

#define RANGE_FLT_MIN        1e-5//1.1754943508222875e-20 // was e-38

# include "xvin/platform.h"
# include "plot_opt.h"
# include "dev_def.h"
# include "box_def.h"
# include "color.h"
# include "plot2box.h"



static char format[64] = {0};
static int old_npts;

int only_dec_x = 10;
int	only_2_5_digit_dec_x = 5;
int	all_digit_dec_x = 1;
int	only_dec_y = 10;
int	only_2_5_digit_dec_y = 5;
int	all_digit_dec_y = 1;
int fine_grid = 1;

/* 6 mai 2003 */
float	plt_width = 1;
float	plt_height = 1;

struct box *last_plot_box(void)
{
  return q_b_plot;
}
struct box *last_q_b(void)
{
  return q_b;
}
int erase_redraw(int npts, float *x, float *y, int iopt)
{
  int xs, ys;

  q_b_plot->color = Black;
  xs = q_b_plot->xc;
  ys = q_b_plot->yc;
  q_b_plot->xc = q_b->xc;
  q_b_plot->yc = q_b->yc;
  wr_polygone(q_b_plot);
  if ( npts > old_npts)
    {
      if (q_b_plot->child[0])  free(q_b_plot->child[0]);
      if (q_b_plot->child[1])  free(q_b_plot->child[1]);
      if (q_b_plot->child) free(q_b_plot->child);
      if (q_b_plot)  free(q_b_plot);
      q_b_plot = mkpolybox(q_b,npts+1);
      old_npts = npts;
      if (q_b_plot != NULL)
	{
	  q_b_plot->w = q_pxxmax - q_pxxmin;
	  q_b_plot->h = q_pxymax - q_pxymin;
	  if (q_b_symb != NULL)
	    {
	      q_b_plot->child[2] = q_b_symb;
	      q_b_symb = NULL;
	      q_symb = 0;
	    }
	}
    }
  draw(npts, x, y, iopt);
  wr_polygone(q_b_plot);
  q_b_plot->xc = xs;
  q_b_plot->yc = ys;
  return 0;
}
/*	draw(npts, x, y, idraw)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int draw(int npts, float *x, float *y, int opt)
{
  int ix = 0, iy = 0; //, p_ix = 0, p_iy = 0;
  int i, n_eff=0;
  float tmp;
  char dotted = 0;
  char dashed = 0;
  char previous_hidden = 0, pt_hidden = 0, pen_dwn = 0; //, previous_drawn = 0;
  BX_AR *xp = NULL, *yp = NULL;

  if (npts == 0)				return 0;
  if(opt & DASH) 				dashed = 1;
  if(!dashed && (opt & DOT)) 		dotted = 1;
  xp = (BX_AR *)q_b_plot->child[0];
  yp = (BX_AR *)q_b_plot->child[1];
  q_b_plot->color = qkgr_color;
  if (q_b_symb != NULL)
    {
      q_b_plot->child[2] = q_b_symb;
      q_b_symb = NULL;
      q_symb = 0;
    }
  for(i = 0, previous_hidden = 1; i<npts; i++)
    {
      if ((isnan(x[i]) == 0) && (isnan(y[i]) == 0))
	{
	  //p_ix = ix;
	  //p_iy = iy;
	  tmp = q_ax*x[i]+q_bx + .5;
	  /*		tmp = (tmp >= 16384) ? 16383 : tmp;
			tmp = (tmp <= -16384) ? -16383 : tmp;	short int	*/
	  ix = (int)tmp;
	  tmp = q_ay*y[i]+q_by + .5;
	  /*		tmp = (tmp >= 16384) ? 16383 : tmp;
			tmp = (tmp <= -16384) ? -16383 : tmp;			*/
	  iy = (int)tmp;
	  pt_hidden = (ix > q_pxxmax || ix < q_pxxmin || iy > q_pxymax || iy < q_pxymin) ? 1 : 0;
	  if (pt_hidden && previous_hidden)
	    {
	      pen_dwn = 0;
	      /*
					previous_drawn = 0;
					continue;
	      */
	    }
	  if (dashed && (i%2 == 0)) 	pen_dwn = 0;	/* 12 oct 97 */
	  if (dotted)
	    {
	      push_poly_point(xp+n_eff,yp+n_eff,ix,iy,MOVE_B);
	      yp[n_eff]  |= 0x00000001;
	    }
	  else if (dashed)
	    {
	      if (pen_dwn) push_poly_point(xp+n_eff,yp+n_eff,ix,iy,LINE_B);
	      else push_poly_point(xp +n_eff,yp+n_eff,ix,iy,MOVE_B);
	      pen_dwn = (pen_dwn) ? 0 : 1;
	    }
	  else
	    {
	      /*
		if (!previous_drawn && i != 0)
		{
		push_poly_point(xp+n_eff, yp+n_eff,p_ix,p_iy,MOVE_B);
		n_eff++;
		}
	      */
	      if (i == 0) push_poly_point(xp+n_eff,yp+n_eff,ix,iy,MOVE_B);
	      else push_poly_point(xp+n_eff, yp+n_eff, ix, iy, LINE_B);
	    }
	  n_eff++;
	  previous_hidden = pt_hidden;
	  //previous_drawn = 1;
	}
    }
  if ( n_eff > npts)	fprintf(stderr,"trop de points ds draw !");
  q_b_plot->n_char = n_eff;
  return 0;
}
/*	qdchar(char charac)
 *	DESCRIPTION	define the single plot symbol
 */
int qdchar(char charac)
{
  q_symb = charac;
  return 0;
}
/*	qdbox(char *ch)
 *	DESCRIPTION	defines the plot simbol with a full string
 */
int qdbox(char *ch)
{
  if ( ch == NULL)
    {
      q_symb = 0;
      if (q_b_symb != NULL)
	{
	  free_box(q_b_symb);
	  free(q_b_symb);
	  q_b_symb = NULL;
	}
    }
  else
    {
      q_symb = 1;
      q_b_symb = (struct box *)calloc(1,sizeof(struct box));
      if (q_b_symb == NULL)		return 1;
      q_b_symb->parent = q_b;
      q_b_symb->n_hic = q_b->n_hic + 1;
      q_b_symb->pt = q_b->pt;
      q_b_symb->color = qkgr_color;
      q_b_symb->i_box = 0;
      q_b_symb->slx = q_b->slx;
      q_b_symb->sly = q_b->sly;
      string_to_box(ch,q_b_symb);
      q_b_symb->xc = - q_b_symb->w / 2;
      q_b_symb->yc = (q_b_symb->d - q_b_symb->h)  / 2;
    }
  return 0;
}
/*	qdylab(int trim)
 *	DESCRIPTION	place the y labels on ticks
 *
 *	RETURNS		0 on success, 1
 */
int qdylab(int triml)
{
  int i, length, ix, iy, len;
  char text[128] = {0}, all_text[128] = {0};
  int tick;
  struct box *bc = NULL;

  if (qkylabl_flag)
    {
      q_b_ylabl = mknewbox(q_b);
      if (q_b_ylabl == NULL) return 1;
      q_b_ylabl->pt *= 7;
      q_b_ylabl->pt /= 10;
      q_b_ylabl->color = qkax_color;
      q_b_ylabl->id = QKD_B_YLABL;
    }
  decide_format(q_y1,q_y2); 	/*y tick labels*/
  for(i = 0; i < q_nytick+1; i++)
    {
      /* Get the value q_ytick[i] into text by calling
       * qdcode(). Qdcode returns the length of the string.
       * Format is the format used in sprintf (in qdcode).
       */
      if (qkylabl_flag) 	       bc = mknewbox(q_b_ylabl);
      all_text[0] = '\0';
      length = qdcode(q_ytick[i], text, format);
      if ( q_unit_ylabl == NULL)	text[length++] = ' ';
      text[length] = '\0';
      strncat(all_text, " ",sizeof(all_text)-1);
      if ( q_pre_ylabl != NULL)
	{
	  len = sizeof(all_text) - strlen(all_text);
	  strncat(all_text, q_pre_ylabl, len);
	}
      if ( q_unit_ylabl != NULL )
	{
	  /*		remove 1 for math units
			if ( text[0] == '1' && length == 1 )
			{
			strcat(all_text, q_unit_ylabl);
			strcat(all_text, " ");
			}
			else if (text[0] == '-' && text[1] == '1' && length == 2)
			{
			strcat(all_text, "-");
			strcat(all_text, q_unit_ylabl);
			strcat(all_text, " ");
			}
			else
	  */
	  if (text[0] == '0'  && length == 1)
	    {
	      len = sizeof(all_text) - strlen(all_text);
	      strncat(all_text, text, len);
	      len = sizeof(all_text) - strlen(all_text);
	      strncat(all_text, " ", len);
	    }
	  else
	    {
	      len = sizeof(all_text) - strlen(all_text);
	      strncat(all_text, text, len);
	      len = sizeof(all_text) - strlen(all_text);
	      strncat(all_text, q_unit_ylabl, len);
	      len = sizeof(all_text) - strlen(all_text);
	      strncat(all_text, " ", len);
	    }
	}
      else
	{
	  len = sizeof(all_text) - strlen(all_text);
	  strncat(all_text, text, len);
	}
      q_ytick[i] = q_ay*q_ytick[i]+q_by;
      iy = q_ytick[i];
      if (bc != NULL)	/*qkylabl_flag*/
	{
	  string_to_box(all_text,bc);
	  bc->yc=iy;
	  if (bc->w > q_b_ylabl->w)	q_b_ylabl->w = bc->w;
	  if (bc->yc+ bc->h > q_b_ylabl->h) 	q_b_ylabl->h = bc->yc + bc->h;
	}
    }
  if (qkylabl_flag && bc != NULL)
    {
      tick = (int)(tk_len_y*q_scx/q_pxdx);
      if ( triml != AXES_PRIME )
	{
	  for(i = 0; i < q_nytick+1; i++)
	    {
	      bc = q_b_ylabl->child[i];
	      bc->xc = q_b_ylabl->w - bc->w;
	    }
	  ix = - q_b->xc;
	  q_b_ylabl->xc = ix - q_b_ylabl->w;
	  q_b->xc += q_b_ylabl->w;
	}
      else
	{
	  ix = q_pxxmax - q_pxxmin;
	  if ( tick < 0 )				ix -= 3*tick;
	  q_b_ylabl->xc=ix;
	}
      q_f->w += q_b_ylabl->w;
    }
  return 0;
}
/*	qdxlab(trim)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int qdxlab(int triml)
{
  int i, length, ix, iy, len;
  char text[128] = {0}, all_text[128] = {0};
  int tick;
  struct box *bc = NULL;

  if(qkxlabl_flag)
    {
      q_b_xlabl = mknewbox(q_b);
      if (q_b_xlabl != NULL)
	{
	  q_b_xlabl->pt *= 7;
	  q_b_xlabl->pt /= 10;
	  q_b_xlabl->color = qkax_color;
	  q_b_xlabl->id = QKD_B_XLABL;
	}
    }
  decide_format(q_x1,q_x2);		/*x tick labels*/
  for(i = 0; i < q_nxtick+1; i++)
    {
      if(qkxlabl_flag)		bc = mknewbox(q_b_xlabl);
      all_text[0] = '\0';
      length = qdcode(q_xtick[i], text, format);
      q_xtick[i] = q_ax*q_xtick[i]+q_bx;
      if ( q_pre_xlabl != NULL)
	{
	  len = sizeof(all_text) - strlen(all_text);
	  strncat(all_text, q_pre_xlabl, len);
	}
      if ( q_unit_xlabl != NULL )
	{
	  /*
			if ( text[0] == '1' && length == 1 )
			{
			strcat(all_text, q_unit_xlabl);
			strcat(all_text, " ");
			}
			else if (text[0] == '-' && text[1] == '1' && length == 2)
			{
			strcat(all_text, "-");
			strcat(all_text, q_unit_xlabl);
			strcat(all_text, " ");
			}
			else
	  */
	  if (text[0] == '0'  && length == 1)
	    {
	      len = sizeof(all_text) - strlen(all_text);
	      strncat(all_text, text, len);
	      len = sizeof(all_text) - strlen(all_text);
	      strncat(all_text, " ", len);
	    }
	  else
	    {
	      len = sizeof(all_text) - strlen(all_text);
	      strncat(all_text, text,len);
	      len = sizeof(all_text) - strlen(all_text);
	      strncat(all_text, q_unit_xlabl, len);
	      len = sizeof(all_text) - strlen(all_text);
	      strncat(all_text, " ", len);
	    }
	}
      else
	{
	  len = sizeof(all_text) - strlen(all_text);
	  strncat(all_text, text, len);
	}
      ix = q_xtick[i];
      if(bc != NULL)		/*qkxlabl_flag*/
	{
	  string_to_box(all_text,bc);
	  bc->xc=ix-(bc->w/2);
	  if (ix + bc->w/2 > q_b_xlabl->w) q_b_xlabl->w = ix + bc->w/2;
	  if ( bc->h > q_b_xlabl->h)	q_b_xlabl->h = bc->h;
	  if ( bc->d > q_b_xlabl->d)	q_b_xlabl->d = bc->d;
	}
    }
  if(bc != NULL)	/*qkxlabl_flag*/
    {
      tick = (int)(tk_len_x*q_scy/q_pxdx);
      if ( triml != AXES_PRIME)
	{
	  iy = 0;
	  if ( tick < 0 )				iy += 3*tick;
	  q_b_xlabl->yc = iy - 2*q_b_xlabl->h;
	  q_b->yc += 2*q_b_xlabl->h + q_b_xlabl->d;
	}
      else
	{
	  iy = q_pxymax-q_ymark;
	  if ( tick < 0 )				iy -= 3*tick;
	  q_b_xlabl->yc = iy + 2*q_b_xlabl->d;
	}
      q_f->h += 2*q_b_xlabl->h + q_b_xlabl->d;
    }
  return 0;
}
/*	decide_format(vmin,vmax)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int decide_format_old(float vmin, float vmax)
{
  float big,range1;
  int iform;

  big = (fabs(vmin) > fabs(vmax)) ? fabs(vmin) : fabs(vmax);
  range1 = fabs(vmax-vmin);
  if(big < 1.e7 && big > 1.e-2)
    {
      iform = 1;
      if(big < 1000)		iform = (range1 < 1.e-1) ? 3 : 2;
      if(big < 100)		iform = (range1 < 1.e-2) ? 4 : 3;
      if(big < 10)		iform = (range1 < 1.e-3) ? 5 : 4;
      snprintf(format, 64,"%%.%df", iform);
    }
  else		snprintf(format, 64, "%%.2e");
  return 0;
}
int decide_format(float vmin, float vmax)
{
  float big,range1;
  int iform;

  big = (fabs(vmin) > fabs(vmax)) ? fabs(vmin) : fabs(vmax);
  range1 = fabs(vmax-vmin);
  if(big < 1.e7 && big > 1.e-2)
    {
      iform = 3;
      if(big < 1000)		iform = (range1 < 1.e-1) ? 5 : 4;
      if(big < 100)		iform = (range1 < 1.e-2) ? 6 : 5;
      if(big < 10)		iform = (range1 < 1.e-3) ? 7 : 6;
      snprintf(format, 64,"%%.%df", iform);
    }
  else		snprintf(format, 64, "%%.4e");
  return 0;
}
/*	qdcode(value, text, format)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int qdcode(float value, char *text, char *format1)
{
  int length, i;
  for(i = 0; i < 12; i++)
    text[i] = '\0';
  if(value == 0)
    {
      text[0] = '0';
      text[1] = '\0';
      return(1);
    }
  snprintf(text, 12, format1, value);
  length = 11;
  for(i=11; i > 0; i--)
    {
      if(text[i] == '0' || text[i] == '\0')		length--;
      else break;
    }
  if(text[i] != '.') 		length++;
  text[length] = '\0';
  return (length);
}
/*	qdone()
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int qdone(void)
{
  return 0;
}
/*	qdset(axmin, axmax, aymin, aymax)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int qdset(float axmin, float axmax, float aymin, float aymax)
{
  q_pxxmin = axmin/q_pxdx;
  q_pxxmax = axmax/q_pxdx;
  q_pxymin = aymin/q_pxdx;
  q_pxymax = aymax/q_pxdx;
  q_pxxmin = qkmin(qkmax(q_pxxmin, 0), q_maxx);
  q_pxxmax = qkmin(qkmax(q_pxxmax, 0), q_maxx);
  q_pxymin = qkmin(qkmax(q_pxymin, 0), q_maxy);
  q_pxymax = qkmin(qkmax(q_pxymax, 0), q_maxy);
  /* Set q_scx and y to be used for tick mark lengths. The smaller
   * the new plot, the smaller q_scx and y
   * and the smaller the tick lengths.
   */
  //q_scx = (q_pxxmax-q_pxxmin)/(.85*(q_maxx-q_minx));
  //q_scy = (q_pxymax-q_pxymin)/((.85*(q_maxy-q_miny)));
  q_scx = q_scy = 1;
  return 0;
}

/*	qdset_abs(axmin, axmax, aymin, aymax)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int qdset_abs(float sizex, float offsetx, float sizey, float offsety)
{
  q_pxxmin = q_pxxmin_sv + offsetx * (q_pxxmax_sv - q_pxxmin_sv);
  q_pxxmax = q_pxxmin + sizex * (q_pxxmax_sv - q_pxxmin_sv);
  q_pxymin = q_pxymin_sv + offsety * (q_pxymax_sv - q_pxymin_sv);
  q_pxymax = q_pxymin + sizey * (q_pxymax_sv - q_pxymin_sv);
  //q_scx = (q_pxxmax-q_pxxmin)/(.85*(q_maxx-q_minx));
  //q_scy = (q_pxymax-q_pxymin)/((.85*(q_maxy-q_miny)));
  q_scx = q_scy = 1;
  /* 6 mai 2003 */
  plt_width = sizex;
  plt_height = sizey;
  /*   */
  return 0;
}

/*	qdytk(trim)
 *	DESCRIPTION	Ticks along Y axis
 *
 *	RETURNS		0 on success, 1
 */
int qdytk(int triml)
{
  register int k, j;
  float xvmin, xvmax;
  double value, rmin, rstep;
  int x1, x2, y;
  int itype, tick, nn, i, tw;
  BX_AR *yl = NULL, *xl = NULL, *yr = NULL, *xr = NULL;
  int nbt, nst, nboxtl = -1, nboxtr = -1, nboxl = -1, nboxsl = -1;
  struct box *tk_line = NULL, *tk_sline = NULL;

  if(q_scx < .40) q_scx = .40;
  /* Scalex is used to modify tick sizes if the plot
   * is a different size from a call to qdset. Same
   * with q_scy.
   */
  tick = (int)(tk_len_y*q_scx/q_pxdx);
  q_nytick = -1;
  set_size(q_y1, q_y2, &rmin, &rstep, plt_height);
  rstep /= N2;
  xvmax = q_y2 + (q_y2 - q_y1)*1.0e-5;
  xvmin = q_y1 - (q_y2 - q_y1)*1.0e-5;
  i = (xvmin - rmin)/rstep + 0.999999;
  rmin += i*rstep;
  nn = i % N2;
  k = (int)(4+2*(xvmax-rmin)/rstep);
  if (k <= 0) return 1;
  tw = (tick > 0) ? 3*tick : -3*tick;
  if ( triml != AXES_PRIME)
    {
      nboxtl = q_b->n_box;
      q_b_ytk_l = mkpolybox(q_b,k);
      if (q_b_ytk_l != NULL)
	{
	  q_b_ytk_l->id = QKD_B_YTK_L;
	  q_b_ytk_l->color = qkax_color;
	  xl = (BX_AR *)q_b_ytk_l->child[0];
	  yl = (BX_AR *)q_b_ytk_l->child[1];
	  q_b_ytk_l->xc = q_pxxmin;
	  q_b_ytk_l->yc = q_pxymin;
	  q_b_ytk_l->h = q_pxymax - q_pxymin;
	  if (tick < 0)
	    {
	      q_b_ytk_l->w = -3 * tick;
	      q_b_ytk_l->xc += 3 * tick;
	      q_b->xc += -3 * tick;
	      q_f->w += -3 * tick;
	      push_poly_point(xl, yl, tw, 0, MOVE_B);
	      push_poly_point(xl+1, yl+1, tw, q_pxymax - q_pxymin, LINE_B);
	    }
	  else
	    {
	      q_b_ytk_l->w = 3 * tick;
	      push_poly_point(xl, yl, 0, 0, MOVE_B);
	      push_poly_point(xl+1, yl+1, 0, q_pxymax - q_pxymin, LINE_B);
	    }
	}

    }
  if ( triml != TRIM)
    {
      nboxtr = q_b->n_box;
      q_b_ytk_r = mkpolybox(q_b,k);
      if (q_b_ytk_r != NULL)
	{
	  q_b_ytk_r->id = QKD_B_YTK_R;
	  q_b_ytk_r->color = qkax_color;
	  xr = (BX_AR *)q_b_ytk_r->child[0];
	  yr = (BX_AR *)q_b_ytk_r->child[1];
	  q_b_ytk_r->xc = q_pxxmax;
	  q_b_ytk_r->yc = q_pxymin;
	  q_b_ytk_r->h = q_pxymax - q_pxymin;
	  if (tick < 0)
	    {
	      q_b_ytk_r->w = -3 * tick;
	      q_f->w += -3 * tick;
	      push_poly_point(xr, yr, 0, 0, MOVE_B);
	      push_poly_point(xr+1, yr+1, 0, q_pxymax - q_pxymin, LINE_B);
	    }
	  else
	    {
	      q_b_ytk_r->w = 3 * tick;
	      q_b_ytk_r->xc -= 3 * tick;
	      push_poly_point(xr, yr, tw, 0, MOVE_B);
	      push_poly_point(xr+1, yr+1, tw, q_pxymax - q_pxymin, LINE_B);
	    }
	}
    }
  for(nbt = nst = 0, value = rmin, k=2; value <= xvmax; value += rstep, k+=2)
    {
      itype = 1;
      if(nn == 0)	/*Big tick*/
	{
	  q_nytick++;
	  /* Store the value in an array used for
	   * qdlabl. Qdlabl writes out the values next
	   * to the big ticks.
	   */
	  q_ytick[q_nytick] = value;
	  if (fabs(value) < 1.0e-3*(fabs(xvmax-xvmin)))
	    q_ytick[q_nytick] = 0.0;
	  itype = 3;
	  nbt++;
	}
      x1 = 0;
      y = (int)(q_ay*value+q_by);
      x2 = itype*tick;
      nst += (itype == 1) ? 1 : 0;
      if ( triml != AXES_PRIME && xl != NULL && yl != NULL)
	{
	  if ( tick > 0)
	    {
	      push_poly_point(xl+k, yl+k, x1, y, MOVE_B);
	      push_poly_point(xl+k+1, yl+k+1, x2, y, LINE_B);
	    }
	  else
	    {
	      push_poly_point(xl+k, yl+k, tw+x1, y, MOVE_B);
	      push_poly_point(xl+k+1, yl+k+1, tw+x2, y, LINE_B);
	    }
	}
      if (trim != TRIM && xr != NULL && yr != NULL)
	{
	  if ( tick < 0)
	    {
	      push_poly_point(xr+k, yr+k, -x1, y, MOVE_B);
	      push_poly_point(xr+k+1, yr+k+1, -x2, y, LINE_B);
	    }
	  else
	    {
	      push_poly_point(xr+k, yr+k, tw-x1, y, MOVE_B);
	      push_poly_point(xr+k+1, yr+k+1, tw-x2, y, LINE_B);
	    }
	}
      nn++;
      if(nn == N2) nn = 0;
    }	/*end of do loop*/
  if (fine_grid == 0)	return 0;
  nboxl = q_b->n_box;
  tk_line = mkpolybox(q_b,2*nbt);
  nboxsl = q_b->n_box;
  tk_sline = mkpolybox(q_b,2*nst);
  if (tk_line != NULL)
    {
      tk_line->color = Lightgray;
      tk_line->pt *= 2;
      tk_line->pt /= 3;
      xl = (BX_AR *)tk_line->child[0];
      yl = (BX_AR *)tk_line->child[1];
      tk_line->xc = q_pxxmin;
      tk_line->yc = q_pxymin;
    }
  if (tk_sline != NULL)
    {
      tk_sline->color = Darkgray;
      tk_sline->pt = (4*tk_sline->pt)/9;
      xr = (BX_AR *)tk_sline->child[0];
      yr = (BX_AR *)tk_sline->child[1];
      tk_sline->xc = q_pxxmin;
      tk_sline->yc = q_pxymin;
    }
  /*	win_printf("nb big tick %d",q_nxtick);*/
  nn = i % N2;
  for(value = rmin, j = k = 0; value <= xvmax; value += rstep)
    {
      x1 = 0;
      y = (int)(q_ay*value+q_by);
      x2 = q_pxxmax - q_pxxmin; /* 		x2 = q_pxymax - q_pxymin;*/
      if (nn == 0)
	{
	  push_poly_point(xl+k, yl+k, x1, y, MOVE_B);
	  push_poly_point(xl+k+1, yl+k+1, x2, y, LINE_B);
	  k +=  (k < 2*nbt) ? 2 : 0;
	}
      else
	{
	  push_poly_point(xr+j, yr+j, x1, y, MOVE_B);
	  push_poly_point(xr+j+1, yr+j+1, x2, y, LINE_B);
	  j +=  (j < 2*nst) ? 2 : 0;
	}
      nn++;
      if(nn == N2) nn = 0;
    }

  if (nboxtl != -1 && nboxl != -1 && nboxtr != -1 && nboxsl != -1)
    {
      q_b->child[nboxtl] = tk_line;
      q_b->child[nboxl] = q_b_ytk_l;
      q_b->child[nboxtr] = tk_sline;
      q_b->child[nboxsl] = q_b_ytk_r;
    }
  else if (nboxtl != -1 && nboxtr == -1 && nboxl != -1 && nboxsl != -1)
    {
      q_b->child[nboxtl] = tk_sline;
      q_b->child[nboxsl] = q_b_ytk_l;
    }
  else if (nboxtl == -1 && nboxtr != -1 && nboxl != -1 && nboxsl != -1)
    {
      q_b->child[nboxtr] = tk_sline;
      q_b->child[nboxsl] = q_b_ytk_r;
    }

  return 0;
}
/*	qdxtk(trim)
 *	DESCRIPTION	Ticks along X axis
 *
 *
 *	RETURNS		0 on success, 1
 *
 */
int qdxtk(int triml)		/* Ticks along X axis */
{
  register int k, j;
  float xvmax, xvmin;
  double value, rmin, rstep;
  int x, y1, y2;
  int itype, tick, nn, i;
  BX_AR *yb = NULL, *xb = NULL, *yt = NULL, *xt = NULL;
  int nbt, nst, nboxtb = -1, nboxtt = -1, nboxl = -1, nboxsl = -1;
  struct box *tk_line = NULL, *tk_sline = NULL;

  if(q_scy < .40) q_scy = .40;
  tick = (int)(tk_len_x*q_scy/q_pxdx);
  q_nxtick = -1;
  set_size(q_x1, q_x2, &rmin, &rstep, plt_width);
  rstep /= N2;
  xvmax = q_x2 + (q_x2 - q_x1)*1.0e-5;
  xvmin = q_x1 - (q_x2 - q_x1)*1.0e-5;
  i = (xvmin - rmin)/rstep + 0.999999;
  rmin += i*rstep;
  nn = i % N2;
  k = (int)(4+2*(xvmax-rmin)/rstep);
  if (k <= 0) return 1;
  if ( triml != AXES_PRIME)
    {
      nboxtb = q_b->n_box;
      q_b_xtk_b = mkpolybox(q_b,k);
      if (q_b_xtk_b != NULL)
	{
	  q_b_xtk_b->id = QKD_B_XTK_B;
	  q_b_xtk_b->color = qkax_color;
	  xb = (BX_AR *)q_b_xtk_b->child[0];
	  yb = (BX_AR *)q_b_xtk_b->child[1];
	  q_b_xtk_b->xc = q_pxxmin;
	  q_b_xtk_b->yc = q_pxymin;
	  q_b_xtk_b->w = q_pxxmax - q_pxxmin;
	  if (tick < 0)
	    {
	      q_b_xtk_b->d = -3 * tick;
	      q_b->yc += -3 * tick;
	      q_f->h += -3 * tick;
	    }
	  else	q_b_xtk_b->h = 3 * tick;
	  push_poly_point(xb, yb, 0, 0, MOVE_B);
	  push_poly_point(xb+1, yb+1, q_pxxmax - q_pxxmin, 0, LINE_B);
	}
    }
  if ( triml != TRIM)
    {
      nboxtt = q_b->n_box;
      q_b_xtk_t = mkpolybox(q_b,k);
      if (q_b_xtk_t != NULL)
	{
	  q_b_xtk_t->id = QKD_B_XTK_T;
	  q_b_xtk_t->color = qkax_color;
	  xt = (BX_AR *)q_b_xtk_t->child[0];
	  yt = (BX_AR *)q_b_xtk_t->child[1];
	  q_b_xtk_t->xc = q_pxxmin;
	  q_b_xtk_t->yc = q_pxymax;
	  q_b_xtk_t->w = q_pxxmax - q_pxxmin;
	  if (tick < 0)
	    {
	      q_b_xtk_t->h = -3 * tick;
	      q_f->h += -3 * tick;
	    }
	  else	q_b_xtk_t->d = 3 * tick;
	  push_poly_point(xt, yt, 0, 0, MOVE_B);
	  push_poly_point(xt+1, yt+1, q_pxxmax - q_pxxmin, 0, LINE_B);
	}
    }
  nbt = nst = 0;
  for(value = rmin, k = 2; value <= xvmax; value += rstep, k+=2)
    {
      itype = 1;
      if(nn == 0)	/*Big tick*/
	{
	  q_nxtick++;
	  /* Store the value in an array used for
	   * qdlabl. Qdlabl writes out the values next
	   * to the big ticks.
	   */
	  q_xtick[q_nxtick] = value;
	  if (fabs(value) < 1.0e-3*(fabs(xvmax-xvmin)))
	    q_xtick[q_nxtick] = 0.0;
	  itype = 3;
	  nbt++;
	}
      x = (int)(q_ax*value+q_bx);
      y1 = 0;
      y2 = itype*tick;
      nst += (itype == 1) ? 1 : 0;
      if ( triml != AXES_PRIME && xb != NULL && yb != NULL)
	{
	  push_poly_point(xb+k, yb+k, x, y1, MOVE_B);
	  push_poly_point(xb+k+1, yb+k+1, x, y2, LINE_B);
	}
      if (triml != TRIM && xt != NULL && yt != NULL)
	{
	  push_poly_point(xt+k, yt+k, x, y1, MOVE_B);
	  push_poly_point(xt+k+1, yt+k+1, x, -y2, LINE_B);
	}
      nn++;
      if(nn == N2) nn = 0;
    }
  if (fine_grid == 0)	return 0;
  nboxl = q_b->n_box;
  tk_line = mkpolybox(q_b,2*nbt);
  nboxsl = q_b->n_box;
  tk_sline = mkpolybox(q_b,2*nst);
  if (tk_line != NULL)
    {
      tk_line->color = Lightgray;
      tk_line->pt *= 2;
      tk_line->pt /= 3;
      xb = (BX_AR *)tk_line->child[0];
      yb = (BX_AR *)tk_line->child[1];
      tk_line->xc = q_pxxmin;
      tk_line->yc = q_pxymin;
    }
  if (tk_sline != NULL)
    {
      tk_sline->color = Darkgray;
      tk_sline->pt = (4*tk_sline->pt)/9;
      xt = (BX_AR *)tk_sline->child[0];
      yt = (BX_AR *)tk_sline->child[1];
      tk_sline->xc = q_pxxmin;
      tk_sline->yc = q_pxymin;
    }
  /*	win_printf("nb big tick %d",q_nxtick);*/
  nn = i % N2;
  for(value = rmin, j = k = 0; value <= xvmax; value += rstep)
    {
      x = (int)(q_ax*value+q_bx);
      y1 = 0;
      y2 = q_pxymax - q_pxymin;
      if (nn == 0 && xb != NULL && yb != NULL)
	{
	  push_poly_point(xb+k, yb+k, x, y1, MOVE_B);
	  push_poly_point(xb+k+1, yb+k+1, x, y2, LINE_B);
	  k +=  (k < 2*nbt) ? 2 : 0;
	}
      else
	{
	  if (xt != NULL && yt != NULL)
	    {
	      push_poly_point(xt+j, yt+j, x, y1, MOVE_B);
	      push_poly_point(xt+j+1, yt+j+1, x, y2, LINE_B);
	      j +=  (j < 2*nst) ? 2 : 0;
	    }
	}
      nn++;
      if(nn == N2) nn = 0;
    }
  if (nboxtb != -1 && nboxl != -1 && nboxtt != -1 && nboxsl != -1)
    {
      q_b->child[nboxtb] = tk_line;
      q_b->child[nboxl] = q_b_xtk_b;
      q_b->child[nboxtt] = tk_sline;
      q_b->child[nboxsl] = q_b_xtk_t;
    }
  else if (nboxtb != -1 && nboxl != -1 && nboxtt == -1 && nboxsl != -1)
    {
      q_b->child[nboxtb] = tk_sline;
      q_b->child[nboxsl] = q_b_xtk_b;
    }
  else if (nboxtb == -1 && nboxl != -1 && nboxtt != -1 && nboxsl != -1)
    {
      q_b->child[nboxtt] = tk_sline;
      q_b->child[nboxsl] = q_b_xtk_t;
    }
  return 0;
}
/*	set_size(vmin, vmax, rmin, rstep)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int set_size(float vmin, float vmax, double *rmin, double *rstep, float size)
{
  int i, icons, nround=6;
  float vdif, pstep, log10, order, power, smin;
  float almos1=.9999999;
  float round[6];
  long int myicons;

  round[0] = 1.;
  round[1] = 2.;
  round[2] = 2.5;
  round[3] = 5.;
  round[4] = 10.;
  round[5] = 20.;
  vdif = vmax-vmin;
  pstep = fabs(vdif)/((N1*size) > 3 ? (N1*size) : 3);
  /* 6 mai 03 was fabs(vdif)/N1 */
  log10 = log(10.0);
  order = log(pstep)/log10;
  if(order < 0) order = order-almos1;
  icons = order;		/*Convert to  integer*/
  order = icons;		/*Convert back; arg for pow must be float*/
  power = pow(10.0, order);
  for(i = 0; i < nround; i++)
    {
      *rstep = round[i]*power;
      if (*rstep >= pstep)			break;
    }
  smin = vmin/(*rstep);
  if (smin < 0) smin -= almos1;
  myicons = (long int)smin;
  *rmin = (*rstep)*myicons;
  return 0;
}
/*	qdtitl(string)
 *	DESCRIPTION	place the title
 *
 *	RETURNS		0 on success, 1
 */
int qdtitl(char *string)
{
  register int i=0;
  int ix, iy, l;
  int tick;

  l = strlen(string);
  if (l == 0)		return 0;
  tick = (int)(tk_len_x*q_scy/q_pxdx);
  iy = q_pxymax ;
  ix = (q_pxxmax+q_pxxmin)/2;
  ix = qkmax(ix, q_minx);
  if ((q_b_titl = mknewbox(q_b)) == NULL)		return 1;
  q_b_titl->id = QKD_B_TITL;
  q_b_titl->pt *= 12;
  q_b_titl->pt /= 10;
  q_b_titl->color = qklab_color;
  string_to_box(string, q_b_titl);
  q_b_titl->xc = ix - (q_b_titl->w/2);
  q_b_titl->yc = iy ;
  if ( trim == AXES_PRIME )
    {
      if ( q_b_xtitl != NULL && q_b_xtitl->yc + q_b_xtitl->h > q_b->d + q_b->h)
	q_b_titl->yc = q_b_xtitl->yc + q_b_xtitl->h ;
      /*		else 	q_b_titl->yc = q_b->h;*/
      else 	q_b_titl->yc = q_b->d + q_b->h;
      i = 1;
    }
  if ((q_b_xlabl != NULL) && (q_b_xlabl->yc + q_b_xlabl->h) > q_b_titl->yc - q_b_titl->d )
    {
      q_b_titl->yc = q_b_xlabl->yc + q_b_xlabl->h;
      i = 1;
    }
  if ( tick < 0 && i == 0 )
    {
      q_b_titl->yc -= 3*tick;
    }
  q_b_titl->yc += q_b_titl->d + title_vspace;
  q_f->h +=  q_b_titl->d + q_b_titl->h + 2 * title_vspace;
  q_f->w = (q_b_titl->xc + q_b_titl->w > q_f->w) ? q_b_titl->xc + q_b_titl->w : q_f->w;
  return 0;
}

/* This is the only QKDRAW routine that makes direct access to the device
 * files.
 */
/*	qterm(struct box *b);		set the box to contain the plot
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int qterm(struct box *b)
{
  register int i, j;

  qd_fp = stdout;
  if (b == NULL)		return 1;
  q_f = b;
  if (q_f->id == QKD_F)
    {
      for (i = 0, j = -1; i < q_f->n_box && j < 0; i++)
	j = (q_f->child[i]->id == QKD_B) ? i : -1;
      if (j >= 0)
	{
	  q_b = q_f->child[j];
	  title_vspace = q_b->pt * 15;
	  xtitle_vspace = q_b->pt * 15;
	  ytitle_hspace = q_b->pt * 15;
	  qkax_color = White;
	  qkgr_color = Yellow;
	  qklab_color = Lightred;
	}
      else	free_box(b);
    }
  if (q_f->id != QKD_F)
    {
      q_f->id = QKD_F;
      if ((q_b = mknewbox(q_f)) == NULL) return 1;
      dev_box_init(q_b);
      q_f->id = QKD_F;
      q_b->id = QKD_B;
    }
  qdset_abs(1,0,1,0);
  return 0;
}
/*	qxlabl(string)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int qxlabl(char *string)
{
  int l, ix;

  l = strlen(string);
  if(l == 0)			return 0;
  ix = (q_pxxmin+q_pxxmax)/2;
  ix = qkmax(ix, q_minx);
  if ((q_b_xtitl = mknewbox(q_b)) == NULL) 	return 1;
  q_b_xtitl->id = QKD_B_XTITL;
  q_b_xtitl->color = qklab_color;
  string_to_box(string,q_b_xtitl);
  q_b_xtitl->xc = ix-(q_b_xtitl->w)/2;
  if ( trim == AXES_PRIME )
    {
      q_b_xtitl->id = QKD_B_XTITL_PRIME;
      if ((q_b_xlabl != NULL) && (q_b_xlabl->yc + q_b_xlabl->h > q_b->h))
	q_b_xtitl->yc =	q_b_xlabl->yc + q_b_xlabl->h + q_b_xtitl->d + xtitle_vspace;
      else q_b_xtitl->yc = q_b->h + q_b_xtitl->d + xtitle_vspace; /* was q_f->b */
    }
  else
    {
      q_b_xtitl->yc = - q_b->yc - q_b_xtitl->h - xtitle_vspace;
      q_b->yc += q_b_xtitl->h + q_b_xtitl->d + 2* xtitle_vspace;
    }
  q_f->h += q_b_xtitl->h + q_b_xtitl->d + 2 * xtitle_vspace;
  return 0;
}

/*	qdset_ticklen(float len);
 *	DESCRIPTION	set the ticklength, negative values suported
 *
 *	RETURNS		0 on success, 1
 *
 */
int qdset_ticklen(float len)
{
  tk_len = len * TKLEN;
  tk_len_x = tk_len;
  tk_len_y = tk_len;
  return 0;
}
int qdset_ticklen_x(float len)
{
  tk_len_x = len * TKLEN;
  return 0;
}
int qdset_ticklen_y(float len)
{
  tk_len_y = len * TKLEN;
  return 0;
}

/*	qkxlabl_set(flag); qkylabl_set(flag);
 *	DESCRIPTION	set flag for labeling ticks
 *
 *
 */
int qkxlabl_set(int flag)
{
  qkxlabl_flag = flag;
  return 0;
}
int qkylabl_set(int flag)
{
  qkylabl_flag = flag;
  return 0;
}
/*	qkxunit(prefix,unit); qkyunit(prefix,unit);
 *	DESCRIPTION	defines the prefix and units of labeling
 *
 */
int qkxunit(char *prefix, char *unit)
{
  if ( prefix != NULL && prefix[0] != 0)		q_pre_xlabl = prefix;
  else q_pre_xlabl = NULL;
  if ( unit != NULL && unit[0] != 0)		q_unit_xlabl = unit;
  else q_unit_xlabl = NULL;
  return 0;
}
int qkyunit(char *prefix, char *unit)
{
  if ( prefix != NULL && prefix[0] != 0)		q_pre_ylabl = prefix;
  else q_pre_ylabl = NULL;
  if ( unit != NULL && unit[0] != 0)		q_unit_ylabl = unit;
  else q_unit_ylabl = NULL;
  return 0;
}
/*	qkmax(int x, int y); qkmin(int x, int y);
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int qkmax(int x, int y)
{
  return((x > y) ? x : y);
}
int qkmin(int x, int y)
{
  return((x < y) ? x : y);
}
/*	qylabl(string)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int qylabl(char *string)
{
  int l, iy;
  struct box *ba = NULL;

  l = strlen(string);
  if(l == 0) 		return 0;
  iy = (q_pxymax+q_pxymin)/2;
  if ((ba = mknewbox(q_b)) == NULL)		return 1;
  if ((q_b_ytitl =  mknewbox(ba)) == NULL)	return 1;
  ba->n_box = TURN_90;
  ba->id = QKD_B_YTITL;	/*q_b_ytitl->id = QKD_B_YTITL;*/
  ba->color = q_b_ytitl->color = qklab_color;
  string_to_box(string,q_b_ytitl);
  if(trim == AXES_PRIME)
    {
      ba->id = QKD_B_YTITL_PRIME;
      ba->xc =  q_f->w - q_b->xc + ytitle_hspace + q_b_ytitl->h;  /* was q_f->w */

    }
  else
    {
      ba->xc = - q_b->xc - q_b_ytitl->d - ytitle_hspace;
      q_b->xc += q_b_ytitl->h + q_b_ytitl->d + 2 * ytitle_hspace;
    }
  ba->w = q_b_ytitl->h + q_b_ytitl->d;
  ba->h = q_b_ytitl->w;
  q_f->w += q_b_ytitl->h + q_b_ytitl->d + 2 * ytitle_hspace;
  ba->yc = iy - q_b_ytitl->w/2;
  return 0;
}

/*	qdfile(fname)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int qdfile(char *fname)
{
  char *home = NULL;
  char concat[512] = {0};

  if (qd_fp != NULL && qd_fp != stdout)
      fclose(qd_fp);
  if (fname == NULL)
    {
      qd_fp = stdout;
      return 1;
    }
  if ((qd_fp = fopen(fname, "wb")) == NULL)
    {
      home = getenv("HOME");
      snprintf(concat, 512, "%s/%s", home, fname);
      if((qd_fp = fopen(concat, "wb")) == NULL)
	{
	  fprintf(stderr,"Qkdraw: Cannot create output file\n");
	  qd_fp = stdout;
	  return 0;
	}
    }
  return 1;
}
/*	xlabel(char *string, float xpos, float ypos);
 *	ylabel(char *string, float xpos, float ypos);
 *	DESCRIPTION
 *
 *
 *	RETURNS		0 on success, 1
 */
struct box *xlabel(char *string, float xpos, float ypos)
{
  int l, ix, iy;
  struct box *b0 = NULL;

  if ((b0 = mknewbox(q_b)) == NULL)	return NULL;
  b0->id = QKD_B_XLABEL;
  b0->color = qklab_color;
  if(q_logx)
    {
      if(xpos <= 0)			return NULL;
      xpos = log10((double)xpos);
    }
  ix = q_ax*xpos+q_bx + .5;
  if(q_logy)
    {
      if(ypos <= 0)			return NULL;
      ypos = log10((double)ypos);
    }
  iy = q_ay*ypos+q_by + .5;
  if (string == NULL)			return NULL;
  l = strlen(string);
  if (l == 0)				return NULL;
  string_to_box(string,b0);
  /*
    b0->xc = qkmin(qkmax(ix, q_minx), q_maxx);
    b0->yc = qkmin(qkmax(iy, q_miny), q_maxy);
  */
  b0->xc = ix;
  b0->yc = iy;
  return b0;
}
struct box *ylabel(char *string, float xpos, float ypos)
{
  int l, ix, iy;
  struct box *b0 = NULL, *b1 = NULL;

  if ((b0 = mknewbox(q_b)) == NULL)	return NULL;
  b0->id = QKD_B_YLABEL;
  if ((b1 =  mknewbox(b0)) == NULL)	return NULL;
  b0->n_box = TURN_90;
  b0->color = b1->color = qklab_color;
  if (q_logx)
    {
      if(xpos <= 0)			return NULL;
      xpos = log10((double)xpos);
    }
  ix = q_ax*xpos+q_bx + .5;
  if (q_logy)
    {
      if(ypos <= 0)			return NULL;
      ypos = log10((double)ypos);
    }
  iy = q_ay*ypos+q_by + .5;
  l = strlen(string);
  if (l == 0)				return NULL;
  string_to_box(string,b1);
  b0->w = b1->d + b1->h;
  b0->h = b1->w;
  /*
    b0->xc = qkmin(qkmax(ix, q_minx), q_maxx);
    b0->yc = qkmin(qkmax(iy, q_miny), q_maxy);
  */
  b0->xc = ix;
  b0->yc = iy;
  return b0;
}
/*	xlogtk(trim)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int xlogtk(int triml)
{
  int range;
  float junk;
  float fac, posib, value, value0;
  int itype, k, nn, tick, x, y1, y2, i, ic1, ic2, i_odd = 1, nn0, ic10, j;
  BX_AR *xb = NULL, *yb = NULL, *xt = NULL, *yt = NULL;
  int nbt, nst, nboxtb = -1, nboxtt = -1, nboxl = -1, nboxsl = -1;
  struct box *tk_line = NULL, *tk_sline = NULL;


  ic1 = (q_x1 >0) ? q_x1 +.999999 : q_x1;
  ic2 = (q_x2 <0) ? q_x2 -.999999 : q_x2;
  tick = (int)(tk_len_x*q_scy/q_pxdx);
  range = ic2-ic1;
  range = (range < 0) ? -range : range;
  junk = ic1-1.0;
  fac = pow(10.0, junk);
  value = ic1;
  i = 9;
  for(nn = -1; nn < 10; nn++)
    {
      posib = log10(i*fac);
      i--;
      if(posib < q_x1)			break;
      value = posib;
    }
  i += 2;
  if(i == 10)		i = 1;
  q_xsave = i;
  if(nn == -1)
    {
      ic1++;
      nn = 8;
    }
  k = 802;
  value0 = value;
  ic10 = ic1;
  nn0 = nn;
  if ( triml != AXES_PRIME)
    {
      nboxtb = q_b->n_box;
      q_b_xtk_b = mkpolybox(q_b,k);
      if (q_b_xtk_b != NULL)
	{
	  q_b_xtk_b->id = QKD_B_XTK_B;
	  q_b_xtk_b->color = qkax_color;
	  xb = (BX_AR *)q_b_xtk_b->child[0];
	  yb = (BX_AR *)q_b_xtk_b->child[1];
	  q_b_xtk_b->xc = q_pxxmin;
	  q_b_xtk_b->yc = q_pxymin;
	  q_b_xtk_b->w = q_pxxmax - q_pxxmin;
	  if (tick < 0)
	    {
	      q_b_xtk_b->d = -3 * tick;
	      q_b->yc += -3 * tick;
	      q_f->h += -3 * tick;
	    }
	  else	q_b_xtk_b->h = 3 * tick;
	  push_poly_point(xb, yb, 0, 0, MOVE_B);
	  push_poly_point(xb+1, yb+1, q_pxxmax - q_pxxmin, 0, LINE_B);
	}
    }
  if ( triml != TRIM)
    {
      nboxtt = q_b->n_box;
      q_b_xtk_t = mkpolybox(q_b,k);
      if (q_b_xtk_t != NULL)
	{
	  q_b_xtk_t->id = QKD_B_XTK_T;
	  q_b_xtk_t->color = qkax_color;
	  xt = (BX_AR *)q_b_xtk_t->child[0];
	  yt = (BX_AR *)q_b_xtk_t->child[1];
	  q_b_xtk_t->xc = q_pxxmin;
	  q_b_xtk_t->yc = q_pxymax;
	  q_b_xtk_t->w = q_pxxmax - q_pxxmin;
	  if (tick < 0)
	    {
	      q_b_xtk_t->h = -3 * tick;
	      q_f->h += -3 * tick;
	    }
	  else	q_b_xtk_t->d = 3 * tick;
	  push_poly_point(xt, yt, 0, 0, MOVE_B);
	  push_poly_point(xt+1, yt+1, q_pxxmax - q_pxxmin, 0, LINE_B);
	}
    }
  q_nxtick = 0;
  ic1--;
  for(i = 0, k = 2, nbt = nst = 0; i < 400; i++, k+=2)
    {
      itype = 1;
      if(nn == 8) 			itype = 3;
      if(nn == 4)				itype = 2;
      x = (int)(q_ax*value+q_bx + .5);
      y1 = 0;
      y2 = itype*tick;
      if (itype == 3)		nbt++;		else nst++;
      if ( triml != AXES_PRIME && xb != NULL && yb != NULL)
	{
	  push_poly_point(xb+k, yb+k, x, y1, MOVE_B);
	  push_poly_point(xb+k+1, yb+k+1, x, y2, LINE_B);
	}
      if ( triml != TRIM && xt != NULL && yt != NULL)
	{
	  push_poly_point(xt+k, yt+k, x, y1, MOVE_B);
	  push_poly_point(xt+k+1, yt+k+1, x, -y2, LINE_B);
	}
      if(range < all_digit_dec_x) /*  < 2  < 4 */
	{
	  q_xltick[q_nxtick] = value;
	  q_xltick_t[q_nxtick] = itype;
	  q_nxtick++;
	}
      else if((range >=all_digit_dec_x) && (range < only_2_5_digit_dec_x))			{
	if(itype != 1 || nn == 7)
	  {
	    q_xltick[q_nxtick] = value;
	    q_xltick_t[q_nxtick] = itype;
	    q_nxtick++;
	  }
      }
      else if((range >= only_2_5_digit_dec_x) && (range < only_dec_x))
	{
	  if(itype == 3)
	    {
	      q_xltick[q_nxtick] = value;
	      q_xltick_t[q_nxtick] = itype;
	      q_nxtick++;
	    }
	}
      else if(range >= only_dec_x)
	{
	  if(itype == 3)
	    {
	      if (i_odd%2)
		{
		  q_xltick[q_nxtick] = value;
		  q_xltick_t[q_nxtick] = itype;
		  q_nxtick++;
		}
	      i_odd++;
	    }
	}
      if(nn == 0)
	{
	  nn = 9;
	  ic1++;
	}
      junk = ic1;
      nn--;
      value = (9.0-nn)*pow(10.0, junk);
      value = log10(value);
      if(value > q_x2)
	break;
    }
  if ( triml != AXES_PRIME)
    {
      q_b_xtk_b->n_char = k;
      q_b_xtk_b->child[0] = (struct box *)duplicate_realloc((BX_AR *)q_b_xtk_b->child[0],k);
      q_b_xtk_b->child[1] = (struct box *)duplicate_realloc((BX_AR *)q_b_xtk_b->child[1],k);
    }
  if ( triml != TRIM)
    {
      q_b_xtk_t->n_char = k;
      q_b_xtk_t->child[0] = (struct box *)duplicate_realloc((BX_AR *)q_b_xtk_t->child[0],k);
      q_b_xtk_t->child[1] = (struct box *)duplicate_realloc((BX_AR *)q_b_xtk_t->child[1],k);
    }
  if (fine_grid == 0)	return 0;
  nboxl = q_b->n_box;
  tk_line = mkpolybox(q_b,2*nbt);
  if (tk_line != NULL)
    {
      tk_line->color = Lightgray;
      tk_line->pt *= 2;
      tk_line->pt /= 3;
      xb = (BX_AR *)tk_line->child[0];
      yb = (BX_AR *)tk_line->child[1];
      tk_line->xc = q_pxxmin;
      tk_line->yc = q_pxymin;
    }
  if (range < only_2_5_digit_dec_x)
    {
      nboxsl = q_b->n_box;
      tk_sline = mkpolybox(q_b,2*nst);
      if (tk_sline != NULL)
	{
	  tk_sline->color = Darkgray;
	  tk_sline->pt = (4*tk_sline->pt)/9;
	  xt = (BX_AR *)tk_sline->child[0];
	  yt = (BX_AR *)tk_sline->child[1];
	  tk_sline->xc = q_pxxmin;
	  tk_sline->yc = q_pxymin;
	}
    }
  value = value0;
  ic1 = ic10;
  ic1--;
  nn = nn0;
  for(i = j = k = 0; i < 400; i++)
    {
      itype = 1;
      if(nn == 8) 			itype = 3;
      if(nn == 4)				itype = 2;
      x = (int)(q_ax*value+q_bx + .5);
      y1 = 0;
      y2 = q_pxymax - q_pxymin;
      if (itype == 3  && xb != NULL && yb != NULL)
	{
	  push_poly_point(xb+k, yb+k, x, y1, MOVE_B);
	  push_poly_point(xb+k+1, yb+k+1, x, y2, LINE_B);
	  k +=  (k < 2*nbt) ? 2 : 0;
	}
      else if (range < only_2_5_digit_dec_x  && xt != NULL && yt != NULL)
	{
	  push_poly_point(xt+j, yt+j, x, y1, MOVE_B);
	  push_poly_point(xt+j+1, yt+j+1, x, y2, LINE_B);
	  j +=  (j < 2*nst) ? 2 : 0;
	}
      if(nn == 0)
	{
	  nn = 9;
	  ic1++;
	}
      junk = ic1;
      nn--;
      value = (9.0-nn)*pow(10.0, junk);
      value = log10(value);
      if(value > q_x2)			break;
    }
  if (nboxtb != -1 && nboxl != -1 && nboxtt != -1 && nboxsl != -1)
    {
      q_b->child[nboxtb] = tk_line;
      q_b->child[nboxl] = q_b_xtk_b;
      q_b->child[nboxtt] = tk_sline;
      q_b->child[nboxsl] = q_b_xtk_t;
    }
  else if (nboxtb != -1 && nboxl != -1 && nboxtt == -1 && nboxsl != -1)
    {
      q_b->child[nboxtb] = tk_sline;
      q_b->child[nboxsl] = q_b_xtk_b;
    }
  else if (nboxtb == -1 && nboxl != -1 && nboxtt != -1 && nboxsl != -1)
    {
      q_b->child[nboxtt] = tk_sline;
      q_b->child[nboxsl] = q_b_xtk_t;
    }

  return 0;
}
/*	xloglab(trim)
 *	DESCRIPTION
 *
 *
 *	RETURNS		0 on success, 1
 */
int xloglab(int triml)
{
  int ix, iy;
  int i, len;
  int range;
  int tick;
  int ic1, ic2;
  char out_string[128] = {0}, all_text[128] = {0};
  struct box *bc = NULL;

  if(qkxlabl_flag)
    {
      q_b_xlabl = mknewbox(q_b);
      if (q_b_xlabl == NULL)  return 1;
      q_b_xlabl->id = QKD_B_XLABL;
      q_b_xlabl->color = qkax_color;
    }
  ic1 = (q_x1 > 0) ? q_x1 +.999999 : q_x1;
  ic2 = (q_x2 < 0) ? q_x2 -.999999 : q_x2;
  range = ic2-ic1;
  range = (range < 0) ? -range : range;
  for(i = 0; i < q_nxtick; i++)
    {
      if(qkxlabl_flag)		bc = mknewbox(q_b_xlabl);
      all_text[0] = '\0';
      if ( q_pre_xlabl != NULL)
	{
	  len = sizeof(all_text) - strlen(all_text);
	  strncat(all_text, q_pre_xlabl, len);
	}
      if(q_xltick_t[i] == 3) /* (q_xsave == 1 || range >= 4) */
	{	/* Big ticks labels */
	  snprintf(out_string, 128,"10^{%.0f}", q_xltick[i]);
	  q_xsave = 2;
	}
      else if (q_xltick_t[i] == 2)
	{
	  snprintf(out_string, 128, "\\sl 5");
	  if(bc != NULL)	/*qkxlabl_flag*/
	    {
	      bc->pt *= 7;
	      bc->pt /= 10;
	    }
	  q_xsave++;
	  if(q_xsave == 10)	q_xsave = 1;
	}
      else
	{	/* small tick labels */
	  snprintf(out_string, 128, "\\sl %d", q_xsave);
	  if(bc != NULL)	/*qkxlabl_flag*/
	    {
	      bc->pt *= 7;
	      bc->pt /= 10;
	    }
	  q_xsave++;
	  if(q_xsave == 10)	q_xsave = 1;
	}
      len = 128 - strlen(all_text);
      if (len > 0) strncat(all_text, out_string, len);
      if (q_unit_xlabl != NULL)
	{
	  len = 128 - strlen(all_text);
	  strncat(all_text,q_unit_xlabl, len);
	}
      if (bc != NULL)	/*qkxlabl_flag*/
	{
	  string_to_box(all_text,bc);
	  ix = q_ax*q_xltick[i]+q_bx;
	  bc->xc = ix - bc->w/2;
	  if(q_xltick_t[i] != 3)	bc->yc -= bc->h; 	/* just added */
	  if (ix + bc->w/2 > q_b_xlabl->w) q_b_xlabl->w = ix + bc->w/2;
	  if ( bc->h > q_b_xlabl->h)	q_b_xlabl->h = bc->h;
	  if ( bc->d > q_b_xlabl->d)	q_b_xlabl->d = bc->d;
	}
    }
  tick = (int)(tk_len_x*q_scy/q_pxdx);
  if(qkxlabl_flag && bc != NULL)
    {
      tick = (int)(tk_len_x*q_scy/q_pxdx);
      if ( triml != AXES_PRIME)
	{
	  iy = ( tick < 0 ) ? 3 * tick : 0;
	  q_b_xlabl->yc = iy - 2*q_b_xlabl->h;
	  q_b->yc += 2*q_b_xlabl->h + q_b_xlabl->d;
	  for (i=0 ; i< q_b_xlabl->n_box ; i++)
	    q_b_xlabl->child[i]->yc = q_b_xlabl->h - q_b_xlabl->child[i]->h;
	}
      else
	{
	  iy = (tick < 0) ? q_pxymax-q_ymark-3*tick :q_pxymax-q_ymark;
	  q_b_xlabl->yc = iy + 2*q_b_xlabl->d;
	}
      q_f->h += 2*q_b_xlabl->h + q_b_xlabl->d;
    }
  return 0;
}
/*	ylogtk(trim)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int ylogtk(int triml)
{
  int range, k;
  float junk, fac, posib, value, value0;
  int itype, nn, x1, x2, y, tick, i, ic1, ic2, tw, i_odd = 1, ic10, nn0, j;
  BX_AR *xr = NULL, *yr = NULL, *xl = NULL, *yl = NULL;
  int nbt, nst, nboxtl = -1, nboxtr = -1, nboxl = -1, nboxsl = -1;
  struct box *tk_line = NULL, *tk_sline = NULL;

  ic1 = (q_y1 >0) ? q_y1 +.999999 : q_y1;
  ic2 = (q_y2 <0) ? q_y2 -.999999 : q_y2;
  tick = (int)(tk_len_y*q_scx/q_pxdx);
  range = ic2-ic1;
  range = (range < 0) ? -range : range;
  junk = ic1-1.0;
  fac = pow(10.0, junk);
  value = ic1;
  i = 9;
  for(nn = -1; nn < 10; nn++)
    {
      posib = log10(i*fac);
      i--;
      if(posib < q_y1)		break;
      value = posib;
    }
  i += 2;
  if(i == 10)				i = 1;
  q_ysave = i;
  if(nn == -1)
    {
      ic1++;
      nn = 8;
    }
  k = 802;
  tw = (tick > 0) ? 3*tick : -3*tick;
  if ( triml != AXES_PRIME)
    {
      nboxtl = q_b->n_box;
      q_b_ytk_l = mkpolybox(q_b,k);
      if (q_b_ytk_l != NULL)
	{
	  q_b_ytk_l->id = QKD_B_YTK_L;
	  q_b_ytk_l->color = qkax_color;
	  xl = (BX_AR *)q_b_ytk_l->child[0];
	  yl = (BX_AR *)q_b_ytk_l->child[1];
	  q_b_ytk_l->xc = q_pxxmin;
	  q_b_ytk_l->yc = q_pxymin;
	  q_b_ytk_l->h = q_pxymax - q_pxymin;
	}
      if (tick < 0)
	{
	  q_b_ytk_l->w = -3 * tick;
	  q_b_ytk_l->xc -= -3 * tick;
	  q_b->xc += -3 * tick;
	  q_f->w += -3 * tick;
	  push_poly_point(xl, yl, tw, 0, MOVE_B);
	  push_poly_point(xl+1, yl+1, tw, q_pxymax - q_pxymin, LINE_B);
	}
      else
	{
	  q_b_ytk_l->w = 3 * tick;
	  push_poly_point(xl, yl, 0, 0, MOVE_B);
	  push_poly_point(xl+1, yl+1, 0, q_pxymax - q_pxymin, LINE_B);
	}
    }
  if ( triml != TRIM)
    {
      nboxtr = q_b->n_box;
      q_b_ytk_r = mkpolybox(q_b,k);
      if (q_b_ytk_r != NULL)
	{
	  q_b_ytk_r->id = QKD_B_YTK_R;
	  q_b_ytk_r->color = qkax_color;
	  xr = (BX_AR *)q_b_ytk_r->child[0];
	  yr = (BX_AR *)q_b_ytk_r->child[1];
	  q_b_ytk_r->xc = q_pxxmax;
	  q_b_ytk_r->yc = q_pxymin;
	  q_b_ytk_r->h = q_pxymax - q_pxymin;
	}
      if (tick < 0)
	{
	  q_b_ytk_r->w = -3 * tick;
	  q_f->w += -3 * tick;
	  push_poly_point(xr, yr, 0, 0, MOVE_B);
	  push_poly_point(xr+1, yr+1, 0, q_pxymax - q_pxymin, LINE_B);
	}
      else
	{
	  q_b_ytk_r->w = 3 * tick;
	  q_b_ytk_r->xc -= 3 * tick;
	  push_poly_point(xr, yr, tw, 0, MOVE_B);
	  push_poly_point(xr+1, yr+1, tw, q_pxymax - q_pxymin, LINE_B);
	}
    }
  q_nytick = 0;
  ic1--;
  value0 = value;
  nn0 = nn;
  ic10 = ic1;
  for(i = 0, k = 2, nbt = nst = 0; i < 400; i++, k+=2)
    {
      itype = 1;
      if(nn == 8) 			itype = 3;
      if(nn == 4)			itype = 2.0;
      y = (int)(q_ay*value+q_by + .5);
      x1 = 0;
      x2 = itype*tick;
      if (itype == 3) nbt++; else nst++;
      if ( triml != AXES_PRIME && xl != NULL && yl != NULL)
	{
	  if ( tick > 0)
	    {
	      push_poly_point(xl+k, yl+k, x1, y, MOVE_B);
	      push_poly_point(xl+k+1, yl+k+1, x2, y, LINE_B);
	    }
	  else
	    {
	      push_poly_point(xl+k, yl+k, tw+x1, y, MOVE_B);
	      push_poly_point(xl+k+1, yl+k+1, tw+x2, y, LINE_B);
	    }
	}
      if (triml != TRIM && xr != NULL && yr != NULL)
	{
	  if ( tick < 0)
	    {
	      push_poly_point(xr+k, yr+k, -x1, y, MOVE_B);
	      push_poly_point(xr+k+1, yr+k+1, -x2, y, LINE_B);
	    }
	  else
	    {
	      push_poly_point(xr+k, yr+k, tw-x1, y, MOVE_B);
	      push_poly_point(xr+k+1, yr+k+1, tw-x2, y, LINE_B);
	    }
	}
      if(range < all_digit_dec_y) /* = 1  < 2 < 4 */
	{
	  q_yltick[q_nytick] = value;
	  q_yltick_t[q_nytick] = itype;
	  q_nytick++;
	}
      else if((range >= all_digit_dec_y) && (range < only_2_5_digit_dec_y))
	{		/*	only_2_5_digit_dec_x = 5 */
	  if(itype != 1 || nn == 7)
	    {
	      q_yltick[q_nytick] = value;
	      q_yltick_t[q_nytick] = itype;
	      q_nytick++;
	    }
	}
      else if((range >= only_2_5_digit_dec_y) && (range < only_dec_y))
	{
	  if(itype == 3)
	    {
	      q_yltick[q_nytick] = value;
	      q_yltick_t[q_nytick] = itype;
	      q_nytick++;
	    }
	}
      else if(range >= only_dec_y)
	{
	  if(itype == 3)
	    {
	      if (i_odd%2)
		{
		  q_yltick[q_nytick] = value;
		  q_yltick_t[q_nytick] = itype;
		  q_nytick++;
		}
	      i_odd++;
	    }
	}
      if(nn == 0)
	{
	  nn = 9;
	  ic1++;
	}
      junk = ic1;
      nn--;
      value = (9.0-nn)*pow(10.0, junk);
      value = log10(value);
      if(value > q_y2)			break;
    }
  if ( triml != AXES_PRIME)
    {
      q_b_ytk_l->n_char = k;
      q_b_ytk_l->child[0] = (struct box *)duplicate_realloc((BX_AR *)q_b_ytk_l->child[0],k);
      q_b_ytk_l->child[1] = (struct box *)duplicate_realloc((BX_AR *)q_b_ytk_l->child[1],k);
    }
  if ( triml != TRIM)
    {
      q_b_ytk_r->n_char = k;
      q_b_ytk_r->child[0] = (struct box *)duplicate_realloc((BX_AR *)q_b_ytk_r->child[0],k);
      q_b_ytk_r->child[1] = (struct box *)duplicate_realloc((BX_AR *)q_b_ytk_r->child[1],k);
    }
  if (fine_grid == 0)	return 0;
  nboxl = q_b->n_box;
  tk_line = mkpolybox(q_b,2*nbt);
  if (tk_line != NULL)
    {
      tk_line->color = Lightgray;
      tk_line->pt *= 2;
      tk_line->pt /= 3;
      xl = (BX_AR *)tk_line->child[0];
      yl = (BX_AR *)tk_line->child[1];
      tk_line->xc = q_pxxmin;
      tk_line->yc = q_pxymin;
    }
  if (range < only_2_5_digit_dec_y)
    {
      nboxsl = q_b->n_box;
      tk_sline = mkpolybox(q_b,2*nst);
      if (tk_sline != NULL)
	{
	  tk_sline->color = Darkgray;
	  tk_sline->pt = (4*tk_sline->pt)/9;
	  xr = (BX_AR *)tk_sline->child[0];
	  yr = (BX_AR *)tk_sline->child[1];
	  tk_sline->xc = q_pxxmin;
	  tk_sline->yc = q_pxymin;
	}
    }
  value = value0;
  nn = nn0;
  ic1 = ic10;
  for(i = 0, k = j = 0; i < 400; i++)
    {
      itype = 1;
      if(nn == 8) 			itype = 3;
      if(nn == 4)			itype = 2.0;
      x1 = 0;
      y = (int)(q_ay*value+q_by + .5);
      x2 = q_pxxmax - q_pxxmin; /* q_pxymax - q_pxymin; */
      if (itype == 3)
	{
	  push_poly_point(xl+k, yl+k, x1, y, MOVE_B);
	  push_poly_point(xl+k+1, yl+k+1, x2, y, LINE_B);
	  k +=  (k < 2*nbt) ? 2 : 0;
	}
      else if (range < only_2_5_digit_dec_y)
	{
	  push_poly_point(xr+j, yr+j, x1, y, MOVE_B);
	  push_poly_point(xr+j+1, yr+j+1, x2, y, LINE_B);
	  j +=  (j < 2*nst) ? 2 : 0;
	}
      if(nn == 0)
	{
	  nn = 9;
	  ic1++;
	}
      junk = ic1;
      nn--;
      value = (9.0-nn)*pow(10.0, junk);
      value = log10(value);
      if(value > q_y2)			break;
    }
  if (nboxtl != -1 && nboxl != -1 && nboxtr != -1 && nboxsl != -1)
    {
      q_b->child[nboxtl] = tk_line;
      q_b->child[nboxl] = q_b_ytk_l;
      q_b->child[nboxtr] = tk_sline;
      q_b->child[nboxsl] = q_b_ytk_r;
    }
  else if (nboxtl != -1 && nboxtr == -1 && nboxl != -1 && nboxsl != -1)
    {
      q_b->child[nboxtl] = tk_sline;
      q_b->child[nboxsl] = q_b_ytk_l;
    }
  else if (nboxtl == -1 && nboxtr != -1 && nboxl != -1 && nboxsl != -1)
    {
      q_b->child[nboxtr] = tk_sline;
      q_b->child[nboxsl] = q_b_ytk_r;
    }

  return 0;
}
/*	yloglab(trim)
 *	DESCRIPTION
 *
 *
 *	RETURNS		0 on success, 1
 */
int yloglab(int triml)
{
  int ix, iy;
  int i, len;
  int range;
  int ic1, ic2;
  int tick;
  char out_string[128] = {0}, all_text[128] = {0};
  struct box *bc = NULL;

  if (qkylabl_flag)
    {
      q_b_ylabl = mknewbox(q_b);
      if (q_b_ylabl == NULL)  return 1;
      q_b_ylabl->id = QKD_B_YLABL;
      q_b_ylabl->color = qkax_color;
    }
  ic1 = (q_y1 >0) ? q_y1 +.999999 : q_y1;
  ic2 = (q_y2 <0) ? q_y2 -.999999 : q_y2;
  range = ic2-ic1;
  range = (range < 0) ? -range : range;
  for(i = 0; i < q_nytick; i++)
    {
      if (qkylabl_flag)		bc = mknewbox(q_b_ylabl);
      all_text[0] = '\0';
      strncat(all_text, " ",sizeof(all_text));
      if ( q_pre_ylabl != NULL)
	{
	  len = 128 - strlen(all_text);
	  strncat(all_text, q_pre_ylabl, len);
	}
      if(q_yltick_t[i] == 3) /* (q_xsave == 1 || range >= 4) */
	{	/* Big ticks labels */
	  //snprintf(out_string, "10^{");
	  snprintf(out_string, 128, "10^{%.0f}", q_yltick[i]);
	  q_ysave = 2;
	}
      else if (q_yltick_t[i] == 2)
	{
	  snprintf(out_string, 128, "\\sl 5 ");
	  if (bc != NULL)	/*qkylabl_flag*/
	    {
	      bc->pt *= 7;
	      bc->pt /= 10;
	    }
	  q_ysave++;
	  if(q_ysave == 10)	q_ysave = 1;
	}
      else
	{	/* small tick labels */
	  snprintf(out_string, 128, "\\sl %d ", q_ysave);
	  if (bc != NULL)	/*qkylabl_flag*/
	    {
	      bc->pt *= 7;
	      bc->pt /= 10;
	    }
	  q_ysave++;
	  if(q_ysave == 10)	q_ysave = 1;
	}
      len = 128 - strlen(all_text);
      strncat(all_text, out_string, len);
      if (q_unit_ylabl != NULL)
	{
	  len = 128 - strlen(all_text);
	  strncat(all_text,q_unit_ylabl, len);
	}
      if (bc != NULL)	/*qkylabl_flag*/
	{
	  string_to_box(all_text,bc);
	  iy = q_ay*q_yltick[i]+q_by;
	  bc->yc = iy;
	  if (bc->w > q_b_ylabl->w)	q_b_ylabl->w = bc->w;
	  if (bc->yc+ bc->h > q_b_ylabl->h) 	q_b_ylabl->h = bc->yc + bc->h;
	}
    }
  if (qkylabl_flag && bc != NULL)
    {
      tick = (int)(tk_len_y*q_scx/q_pxdx);
      if (triml != AXES_PRIME)
	{
	  for(i = 0; i < q_nytick; i++)
	    {
	      bc = q_b_ylabl->child[i];
	      bc->xc = q_b_ylabl->w - bc->w;
	    }
	  ix = - q_b->xc;
	  q_b_ylabl->xc = ix - q_b_ylabl->w;
	  q_b->xc += q_b_ylabl->w;
	}
      else
	{
	  ix = q_pxxmax - q_pxxmin;
	  if ( tick < 0 )		ix -= 3*tick;
	  q_b_ylabl->xc=ix;
	}
      q_f->w += q_b_ylabl->w;
    }
  return 0;
}
/*	Bugs correction corresponding to H.Williams email of 21 april 89 */
/*	qkdraw(npts, x, y, opt, xmin, xmax, ymin, ymax)
 *	DESCRIPTION
 *
 *
 *
 *	RETURNS		0 on success, 1 malloc error, 2 q_f NULL box
 *
 */
int qkdraw(int npts, float *x, float *y, int opt, float *xmin, float *xmax, float *ymin, float *ymax)
{
  float *xp = NULL, *yp = NULL;
  char symbsave;
  float fac, big, min;
  int i;
  float *xl = NULL, *yl = NULL;

  if (q_f == NULL)	return 2;
  big = 1.e37;
  xp = x;
  yp = y;
  trim=0;
  q_x1 = *xmin;
  q_x2 = *xmax;
  q_y1 = *ymin;
  q_y2 = *ymax;
  /* Translate the options into yes-no decisions.(0 or 1).*/
  /*	if(!(opt & NOCLEAR))		(*q_dev.mode)(ERASE);*/
  if(opt & BIGCHAR)			q_f->pt = (q_f->pt * 12 * default_line_size )/70;
  if(opt & SMALLCHAR)			q_f->pt = (q_f->pt * default_line_size )/10;
  q_f->pt = (q_f->pt * default_line_size )/10; // 2017 test
  if(opt & TRIM)				trim = TRIM;
  else if(opt & AXES_PRIME)	trim = AXES_PRIME;
  else						trim = 0;
  q_logx = (opt & XLOG) ? 1 : 0;
  q_logy = (opt & YLOG) ? 1 : 0;
  if(opt & CROSS)
    {
      q_cross = 1;
      trim = 1;
    }
  else		q_cross = 0;
  if(q_logx || q_logy)		q_cross = 0;
  if(q_logy && npts > 0)
    {
      if((yl = (float*)calloc(npts,sizeof(float))) == NULL)
	{
	  fprintf(stderr, "Qkdraw: Malloc error(1).\n");
	  return 1;
	}
      yp = yl;
      min = big;
      for (i=0; i<npts; i++)	/* find minimum postive value */
	{
	  if (isnormal(y[i]) && y[i] > 0)
	    min = (y[i] < min) ? y[i] : min;
	}
      min = log10(min);
      for (i=0; i<npts; i++)	/* convert to log or minimum */
	yp[i] = (isnormal(y[i]) && y[i] > 0) ? log10(y[i]) : min;
      /* convert boundaries to log */
      if(*ymin <= 0 || *ymax <= 0)			q_y1 = q_y2 = 1.0;
      else
	{
	  q_y1 =  log10(*ymin);
	  q_y2 =  log10(*ymax);
	}
    }
  if(q_logx && npts > 0)
    {
      if((xl = (float*)calloc(npts,sizeof(float))) == NULL)
	{
	  fprintf(stderr, "Qkdraw: Malloc error(2).\n");
	  return 1;
	}
      xp = xl;
      min = big;
      for (i=0; i<npts; i++)	/* find minimum positive value */
	{
	  if ((isnan(x[i]) == 0) && x[i] > 0)
	    min = (x[i] < min) ? x[i] : min;
	}
      min = log10(min);
      for (i=0; i<npts; i++)	/* convert to log or minimum */
	xp[i] = (isnormal(x[i]) && x[i] > 0) ? log10(x[i]) : min;
      /* convert boundaries to log */
      if(*xmin <= 0 || *xmax <= 0) 			q_x1 = q_x2 = 1.0;
      else
	{
	  q_x1 =  log10(*xmin);
	  q_x2 =  log10(*xmax);
	}
    }
  /* Automatic scaling. If xmin>=xmax, then scaling is done */
  if(q_x1 >= q_x2)
    {
      q_x1 = big;
      q_x2 = -big;
      for(i = 0; i<npts; i++)
	{
	  if (isnan(xp[i]) == 0)
	    {
	      q_x2 = (q_x2 > *(xp+i)) ? q_x2 : *(xp+i);
	      q_x1 = (q_x1 < *(xp+i)) ? q_x1 : *(xp+i);
	    }
	}
      fac = (q_x2-q_x1)*.1;
      if(fac == 0)
	{
	  fac = fabs(q_x2);
	  if(fac == 0) fac = 1;
	}
      q_x2 += fac;
      q_x1 -= fac;
      fac = fabs(fac);
      if(fabs(q_x2) < fac) q_x2 = 0;
      if(fabs(q_x1) < fac) q_x1 = 0;
      *xmin = q_x1;
      *xmax = q_x2;
      if(q_logx)
	{
	  *xmin = pow(10.0, *xmin);
	  *xmax = pow(10.0, *xmax);
	}
    }
  /* This section duplicates above - except the Y axis
   * parameters are computed.
   */
  if(q_y1 >= q_y2)
    {
      q_y1 = big;
      q_y2 = -big;
      for(i = 0; i<npts; i++)
	{
	  if (isnan(yp[i]) == 0)
	    {
	      q_y2 = (q_y2 > *(yp+i)) ? q_y2 : *(yp+i);
	      q_y1 = (q_y1 < *(yp+i)) ? q_y1 : *(yp+i);
	    }
	}
      fac = (q_y2-q_y1)*.1;
      if(fac == 0)
	{
	  fac = fabs(q_y2);
	  if(fac == 0) fac = 1;
	}
      q_y2 += fac;
      q_y1 -= fac;
      fac = fabs(fac);
      if(fabs(q_y2) < fac) q_y2 = 0;
      if(fabs(q_y1) < fac) q_y1 = 0.;
      *ymin = q_y1;
      *ymax = q_y2;
      if(q_logy)
	{
	  *ymin = pow(10.0, *ymin);
	  *ymax = pow(10.0, *ymax);
	}
    }
  /* Set scaling constants to be used throughout program. */
  if (fabs(q_x2 - q_x1) > RANGE_FLT_MIN)
    {
      q_ax = (q_pxxmax-q_pxxmin)/(q_x2-q_x1);
      q_bx = (q_pxxmin*q_x2-q_pxxmax*q_x1)/(q_x2-q_x1);
    }
  else
    {
      q_x2 = q_x1 + 1e-5;
      q_ax = (q_pxxmax-q_pxxmin)/1e-5;
      q_bx = (q_pxxmin*q_x2-q_pxxmax*q_x1)/1e-5;
    }
  if (fabs(q_y2 - q_y1) > RANGE_FLT_MIN)
    {
      q_ay = (q_pxymax-q_pxymin)/(q_y2-q_y1);
      q_by = (q_pxymin*q_y2-q_pxymax*q_y1)/(q_y2-q_y1);
    }
  else
    {
      q_y2 = q_y1 + 1e-5;
      q_ay = (q_pxymax-q_pxymin)/1e-5;
      q_by = (q_pxymin*q_y2-q_pxymax*q_y1)/1e-5;
    }
  if(q_cross)
    {
      q_xmark = (q_x1 < 0.0 && q_x2 > 0.0) ? q_bx : q_pxxmin;
      q_ymark = (q_y1 < 0.0 && q_y2 > 0.0) ? q_by : q_pxymin;
    }
  else
    {
      q_xmark = q_pxxmin;
      q_ymark = q_pxymin;
    }
  if ( npts > 0)			/*Send off the data to be drawn.*/
    {
      if ( q_b_plot == NULL )
	{
	  q_b->w += q_pxxmax - q_pxxmin;
	  q_b->h += q_pxymax - q_pxymin;
	  q_b->parent->w += q_pxxmax - q_pxxmin;
	  q_b->parent->h += q_pxymax - q_pxymin;
	}
      q_b_plot = mkpolybox(q_b,npts+1);
      if (q_b_plot == NULL)		return 1;
      q_b_plot->n_box  = CLIPED_POLYGONE_BOX;
      q_b_plot->id = QKD_B_PLOT;
      old_npts = npts;
      q_b_plot->w = q_b->w;
      q_b_plot->h = q_b->h;

      /*		fprintf (stderr, "qfw %d qbw %d  \n",q_b->parent->w,q_b->w);*/
      draw(npts, xp, yp, opt);
    }
  if(!(opt & NOAXES)) /* Draw the tick marks and labels on big ticks */
    {
      symbsave = q_symb;
      q_symb = 0;
      //win_printf("bef qdxtk");
      q_logx ? xlogtk(trim) : qdxtk(trim);
      //win_printf("bef qdytk");
      q_logy ? ylogtk(trim) : qdytk(trim);
      //win_printf("bef qdxlab %d tic",q_nxtick);
      q_logx ? xloglab(trim) : qdxlab(trim);
      //win_printf("bef qdylab %d tic",q_nytick);
      q_logy ? yloglab(trim) : qdylab(trim);
      q_symb = symbsave;
    }
  if(xl != NULL)		free(xl);
  if(yl != NULL)		free(yl);
  return 0;
}

int qkd_color(int ax_color, int gr_color)
{
  qkax_color = ax_color;
  qkgr_color = gr_color;
  return 0;
}
int qkd_lab_color(int color)
{
  qklab_color = color;
  return (color);
}
/*	qkd_get_pos (ix, iy, xpos, ypos, iopt)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int qkd_get_pos (int ix, int iy, float *xpos, float *ypos, int iopt)
{
  register int i = 0;

  *xpos = (float)ix - q_bx;
  if (q_ax != 0.0 )
    {
      *xpos /= q_ax;
      if (iopt & XLOG )		*xpos = pow(10.0, *xpos);
    }
  else		i = 1;
  *ypos = (float)iy - q_by;
  if (q_ay != 0.0 )
    {
      *ypos /= q_ay;
      if (iopt & YLOG )		*ypos = pow(10.0, *ypos);
    }
  else		i = 1;
  return (i);
}
/*	free_qkd_box ();
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int free_qkd_box (void)
{
  q_pre_xlabl = NULL;
  q_unit_xlabl = NULL;
  q_pre_ylabl = NULL;
  q_unit_ylabl = NULL;
  qkylabl_flag = 1;
  qkxlabl_flag = 1;
  q_cross = 0;
  q_symb = 0;
  q_logy = 0;
  q_logx = 0;
  if (q_f == NULL)
    return 1;
  free_box(q_f);
  if (q_b_symb != NULL)
    {
      free_box(q_b_symb);
      free(q_b_symb);
    }
  q_b = NULL;
  q_b_plot = NULL;
  q_b_xlabl = NULL;
  q_b_ylabl = NULL;
  q_b_xtk_t = NULL;
  q_b_xtk_b = NULL;
  q_b_ytk_l = NULL;
  q_b_ytk_r = NULL;
  q_b_xtitl = NULL;
  q_b_ytitl = NULL;
  q_b_titl = NULL;
  q_b_symb = NULL;
  return 0;
}

/*	BX_AR *duplicate_realloc(BX_AR *xl, int k);
 *	DESCRIPTION	duplicate an array and realloc the correct size
 *
 *
 *
 *	RETURNS		the pointer on success, NULL on malloc pb.
 *
 */
BX_AR *duplicate_realloc(BX_AR *xl, int k)
{
  register int i;
  BX_AR *tmp = NULL;

  if (xl == NULL || k <= 0)   return NULL;
  tmp = (BX_AR*)calloc(k,sizeof(BX_AR));
  if (tmp == NULL)
    {
      fprintf(stderr,"calloc error \n");
      return NULL;
    }
  for (i=0 ; i< k ; i++)
    tmp[i] = xl[i];
  free(xl);
  return tmp;
}
int dev_box_init(struct box *bc)
{
  (void)bc;
  qkax_color = White;
  qkgr_color = Yellow;
  qklab_color = Lightred;
  q_minx = 0;			/* device limits */
  q_maxx = 1200 * 8;
  q_miny = 0;
  q_maxy = 1200 * 11;
  q_pxdx = 0.002116;		/* scale factor for qdset()in cm per pixel */
  q_pxxmin = 0;			/* default boundaries for plot axes */
  q_pxymin = 0;
  q_pxymax = 7252;		/*14 * (512+3+3) old 1200 * 6; */
  q_pxxmax = 7252;		/* 1200 * 6;*/
  q_pxxmin_sv = q_pxxmin;	/* default boundaries for plot axes */
  q_pxxmax_sv = q_pxxmax;
  q_pxymin_sv = q_pxymin;
  q_pxymax_sv = q_pxymax;
  /*	init_box(bc);*/
  q_b->w = 0;	/* q_pxxmax; */
  q_b->h = 0;	/* q_pxymax; */
  q_b->parent->w = 0;	/* q_pxxmax; */
  q_b->parent->h = 0;	/* q_pxymax; */
  title_vspace = q_b->pt * 15;
  xtitle_vspace = q_b->pt * 15;
  ytitle_hspace = q_b->pt * 15;
  plt_width = 1;
  plt_height = 1;
  return 0;
}
int push_poly_point(BX_AR *x, BX_AR *y, BX_AR xp, BX_AR yp, int flag)
{
  if (x == NULL || y == NULL) return 1;
  // we avoid too big numbers
  xp = (xp < INT_MAX>>2) ? xp : INT_MAX>>2;
  xp = (xp < (-(INT_MAX>>2) -1)) ? (-(INT_MAX>>2) -1) : xp;
  *x = 2*xp;
  yp = (yp < INT_MAX>>2) ? yp : INT_MAX>>2;
  yp = (yp < (-(INT_MAX>>2) -1)) ? (-(INT_MAX>>2) -1) : yp;
  *y = 2*yp;
  if (flag == LINE_B)
    *x |= 0x00000001;
  return 0;
}

struct box *qkdim(int opt, float xmin, float xmax, float ymin, float ymax)
{
  char symbsave;
  struct box *b = NULL;

  if (q_f == NULL)	return NULL;
  trim = 0;
  q_x1 = xmin - (42 * xmin/(q_pxxmax - q_pxxmin - 84));
  q_x2 = xmax + (42 * xmax/(q_pxxmax - q_pxxmin - 84));
  q_y1 = ymin - (42 * ymin/(q_pxymax - q_pxymin - 84));
  q_y2 = ymax + (42 * ymax/(q_pxymax - q_pxymin - 84));
  if(opt & BIGCHAR)			q_f->pt = (q_f->pt * 12 * default_line_size)/70;
  if(opt & SMALLCHAR)			q_f->pt = (q_f->pt * default_line_size)/10;
  q_f->pt = (q_f->pt * default_line_size)/10;  // 2017 test
  if(opt & TRIM)				trim = TRIM;
  else if(opt & AXES_PRIME)		trim = AXES_PRIME;
  else					trim = 0;
  q_cross = 0;
  /* Set scaling constants to be used throughout program. */
  if (fabs(q_x2 - q_x1) > RANGE_FLT_MIN)
    {
      q_ax = (q_pxxmax-q_pxxmin)/(q_x2-q_x1);
      q_bx = (q_pxxmin*q_x2-q_pxxmax*q_x1)/(q_x2-q_x1);
    }
  else
    {
      q_x2 = q_x1 + 1e-5;
      q_ax = (q_pxxmax-q_pxxmin)/1e-5;
      q_bx = (q_pxxmin*q_x2-q_pxxmax*q_x1)/1e-5;
    }
  if (fabs(q_y2 - q_y1) > RANGE_FLT_MIN)
    {
      q_ay = (q_pxymax-q_pxymin)/(q_y2-q_y1);
      q_by = (q_pxymin*q_y2-q_pxymax*q_y1)/(q_y2-q_y1);
    }
  else
    {
      q_y2 = q_y1 + 1e-5;
      q_ay = (q_pxymax-q_pxymin)/1e-5;
      q_by = (q_pxymin*q_y2-q_pxymax*q_y1)/1e-5;
    }
  q_xmark = q_pxxmin;
  q_ymark = q_pxymin;
  q_b_plot = mknewbox(q_b);
  if (q_b_plot == NULL)		return NULL;
  q_b_plot->id = QKD_B_PLOT;
  q_b_plot->w = q_b->w;
  q_b_plot->h = q_b->h;
  b = mkpolybox(q_b_plot,1);
  if (b == NULL)			return NULL;
  b->n_box  = CLIPED_POLYGONE_BOX;
  b->n_char = 0;
  b->id = QKD_B_IM;
  b->xc = 42;	b->yc = 42;
  b->w = q_pxxmax - q_pxxmin - 84;
  b->h = q_pxymax - q_pxymin - 84;
  if(!(opt & NOAXES)) /* Draw the tick marks and labels on big ticks */
    {
      symbsave = q_symb;
      q_symb = 0;
      q_logx ? xlogtk(trim) : qdxtk(trim);
      q_logy ? ylogtk(trim) : qdytk(trim);
      q_logx ? xloglab(trim) : qdxlab(trim);
      q_logy ? yloglab(trim) : qdylab(trim);
      q_symb = symbsave;
    }
  return b;
}



# endif
