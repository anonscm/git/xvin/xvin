
# include "xvin.h"

# include "xvin/platform.h"
#include "fonts.h" /* needed for BIG_FONT */

# include <float.h>
# include <time.h>
# include <stdlib.h>
# include <string.h>
#include "util.h"

extern FONT *normalsize_font, *llargee_font;


pltreg *load_cgr_file_in_pltreg(pltreg *pr, char *file_path, char *file);
int	load_cgr_multi_plots(void);

char /* fullfile[512],*/ *fuc = NULL;



int	convert_plot_to_box(pltreg *pr, int n_plot)
{
    if (pr == NULL)		return -1;
    if (n_plot != UNCHANGED && n_plot <= pr->n_op && n_plot >= 0)
    {
        pr->cur_op = n_plot;
        pr->cur_op = (pr->cur_op >= pr->n_op || pr->cur_op < 0) ? 0 : pr->cur_op;
        pr->one_p = pr->o_p[pr->cur_op];
        pr->stack = &(pr->one_p->bplt);
        pr->display_mode = IS_ONE_PLOT;
        do_one_plot(pr);
    }
    else if (n_plot >= ALL_PLOTS && n_plot <= ALL_PLOTS + 4)
        prepare_all_plots(pr, n_plot - ALL_PLOTS);
    else				do_one_plot(pr);
    return pr->cur_op;
}


int	find_multi_plots_status(pltreg *pr)
{
    int i, j;
    int  xcm = 0, hm = 0, x0, y0, x1, y1;
    struct box *bo = NULL, *b = NULL, *plot = NULL;

    if (pr == NULL) return 0;
    if (pr->n_op <= 1)	return 0;
    if (pr->stack != &pr->all) return 0;
    bo = &pr->all;
    for (i=0, j = 0 ; i< bo->n_box  ; i++)
    {
        b = bo->child[i];	x0 = 0; 	y0 = 0;
        plot = locate_box(b,QKD_B_PLOT, &x0, &x1, &y0, &y1,FIRST);
        if (plot != NULL)
        {
            if (i == 0)
            {
                xcm = x0;
                hm = y1;
            }
            else
            {
                if (x0 > xcm && y0 < hm)
                {
                    xcm = b->xc;
                    hm = (y1 > hm) ? y1 : hm;
                    j = i;
                }
            }
        }

    }
    return j+1;
}



pltreg *load_cgr_file_in_pltreg(pltreg *pr, char *file_path, char *file)
{
    int i, n;
    char s[384] = {0}, *c = NULL, line[1024] = {0}, plotfile[256] = {0};
    int n_plot;
    FILE *fp = NULL;
    O_p *op;

    if (file_path == NULL || file == NULL)   return NULL;
    snprintf(s,384,"%s%s",file_path,file);
    backslash_to_slash(s);
    fp = fopen(s,"r");
    if (fp == NULL)   return (pltreg *) win_printf_ptr("cannot open file:\n%s",s);

    if (pr == NULL)
    {
        pr = create_and_register_new_plot_project(0, 32, SCREEN_H, SCREEN_H - 32);
        if (pr == NULL)	return (pltreg *) win_printf_ptr("cannot create plot project\n"
                                                         "for file:\n%s",backslash_to_slash(s));
    }

    for(c = fgets(line,1023,fp), n = 0; c != NULL; n++)
    {
        if ((i = sscanf(line,"plot %d %s",&n_plot,plotfile))  >= 2)
        {
          op = create_plot_from_gr_file(plotfile, file_path);
          add_data( pr, IS_ONE_PLOT, op);
          convert_plot_to_box(pr, pr->cur_op);
        }
        if ((i = sscanf(line,"multi-plots %d",&n_plot))  >= 1)
        {
            pr->display_mode = ALL_PLOTS + n_plot;
        }
        if ((i = sscanf(line,"current-plot %d",&n_plot))  >= 1)
        {
            pr->display_mode = IS_ONE_PLOT;
            pr->cur_op = (n_plot < 0) ? 0 : n_plot%pr->n_op;
            pr->one_p = pr->o_p[pr->cur_op];
            pr->one_p->need_to_refresh = 1;
        }
        c = fgets(line,1023,fp);
    }
    if (pr->n_op > 1) remove_data (pr, IS_ONE_PLOT, (void *)pr->o_p[0]);

    pr->path = my_sprintf(pr->path,"%s",file_path);
    pr->filename = my_sprintf(pr->filename,"%s",file);
    fclose(fp);
    return pr;
}

pltreg *load_cgrfile_in_pltreg(pltreg *pr, char *fullfile)
{
    char path[512] = {0}, file[512] = {0};

    extract_file_name(file, sizeof(file), fullfile);
    extract_file_path(path, sizeof(path), fullfile);
    put_path_separator(path, sizeof(path));
    pr = load_cgr_file_in_pltreg(pr, path, file);
    return pr;
}



int	load_cgr_multi_plots(void)
{
    int i;
    char path[512] = {0}, file[256] = {0}, fullfile[512] = {0};
    //pltreg *pr = NULL;
    const char *ful = NULL;

    if(updating_menu_state != 0)	return D_O_K;


    if (fuc == NULL)
    {
        ful = get_config_string("PLOT-CGR-FILE","last_loaded",NULL);
        if (ful != NULL)
        {
            strncpy(fullfile,ful,sizeof(fullfile));
            fuc = fullfile;
        }
        else
        {
            my_getcwd(fullfile, sizeof(fullfile));
            put_path_separator(path, sizeof(path));
        }
    }
    /*
        if (fu == NULL)
        {
        my_getcwd(fullfile, 512);
        put_path_separator(path, sizeof(path));
        }
        */
    //switch_allegro_font(1);
    //i = file_select_ex("Load multiple plots Project (*.cgr)", fullfile, "cgr", 512, 0, 0);
    //switch_allegro_font(0);

    i = do_select_file(fullfile, sizeof(fullfile), "PLOT-CGR-FILE\0", "*.cgr", "Loading multi plots project", 1);



    if (i == 0)
    {
        fuc = fullfile;
        set_config_string("PLOT-CGR-FILE","last_loaded",fuc);
        extract_file_name(file, sizeof(file), fullfile);
        extract_file_path(path, sizeof(path), fullfile);
        put_path_separator(path, sizeof(path));
        //pr =
        load_cgr_file_in_pltreg(NULL, path, file);
    }

    broadcast_dialog_message(MSG_DRAW,0);
    return 0;
}


int		do_save_all_plots_from_project(void)
{
    int i, j;
    char c[512] = {0}, c_dir[512] = {0}, plotname[512] = {0};
    char plotfullname[512] = {0}, fullfile[512] = {0};
    char list_of_file[128][512]={0};
    static char plt_dir[256] = {0};
    int no_file = 1;
    pltreg *pr = NULL;
    size_t len;
    O_p *op = NULL;
    //char *my_getcwd(char *path, int size);
    const char *pa = NULL;
    FILE *fpc = NULL;

    if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (mouse_b & 0x02)
        return win_printf("This routine saves all plots in a project\n"
                          "into the same new directory with a file list files.cgr\n"
                          "so that all plots may be reloaded in one operation\n");
    /* we first find the data that we need to transform */

    pr = find_pr_in_current_dialog(NULL);
    if (pr == NULL)	return win_printf("cannot find plot");
    op = pr->one_p;

    if (pr->path == NULL)
    {
        pa = get_config_string("PLOT-CGR-FILE","last_saved",NULL);
        if (pa != NULL)
        {
            extract_file_path(fullfile, sizeof(fullfile), pa);
        }
        else
        {
            my_getcwd(fullfile, 512);
        put_path_separator(fullfile, sizeof(fullfile));
        }
    }
    else
    {
        strncpy(fullfile,pr->path,sizeof(fullfile));
        put_path_separator(fullfile, sizeof(fullfile));
    }
    if (pr->filename == NULL)
        my_strncat(fullfile,"Untitled.cgr",sizeof(fullfile));
    else my_strncat(fullfile,pr->filename,sizeof(fullfile));

    /*

       strcpy(c,(pr->filename != NULL)? pr->filename:"untitle.cgr");
       if (pr->path != NULL) strcpy(c_dir, pr->path);
       else
       {
       if (plt_dir == NULL)
       {
       my_getcwd(c_dir ,DIR_LEN);
       slash_to_backslash(c_dir);
       plt_dir = (char *)calloc(256,sizeof(char));
       strcpy(plt_dir,c_dir);
       }
       else strcpy(c_dir,plt_dir);
       }
       */



    while (no_file)
    {
        //snprintf(s,384,"%s%s",c_dir,c);
        //switch_allegro_font(1);
        //i = file_select_ex("Save all files of project (.cgr)", fullfile, "cgr", sizeof(fullfile), 0, 0);
        //switch_allegro_font(0);
        i = do_select_file(fullfile, sizeof(fullfile), "PLOT-CGR-FILE\0", "*.cgr", "Saving multi plots project", 0);
        char *finds = NULL;
        if (i == 0)
        {
            set_config_string("PLOT-CGR-FILE","last_saved",fullfile);
            extract_file_name(c, sizeof(c), fullfile);
            extract_file_path(c_dir, sizeof(c_dir), fullfile);
            put_path_separator(c_dir, sizeof(c_dir));
            if (pr->path == NULL) 	strncpy(plt_dir,c_dir,sizeof(plt_dir));
            for (i=0 ; c_dir[i] != 0; i++);
            if (i > 1 && is_path_separator(c_dir[i-1]))
            {
                c_dir[i] = get_path_separator();
                c_dir[i+1] = 0;
            }
            snprintf(fullfile,sizeof(fullfile),"%s%s",c_dir,c);
            i =	do_you_want_to_overwrite(fullfile, NULL, NULL);
            if (i == WIN_CANCEL) return D_O_K;
            backslash_to_slash(fullfile);
            fpc = fopen(fullfile,"w");
            if (fpc == NULL)	return win_printf("cannot open file:\n%s",backslash_to_slash(fullfile));
            else no_file = 0;
            fclose(fpc);
            for(j = 0; j < pr->n_op; j++)
            {
                op = pr->o_p[j];
                fpc = fopen(fullfile,"a");
                if (fpc == NULL)      return win_printf("cannot open file:\n%s",backslash_to_slash(fullfile));
                if (op->filename != NULL)
                {
                   strncpy(plotname,op->filename,sizeof(plotname));
                   finds = strstr(plotname,".gr");
                   if (finds) *finds='\0';
                   for (int jj=0;jj<j;jj++)
                   {
                     if (strcmp(plotname,list_of_file[jj])==0)
                      {
                        len = strlen(plotname);
                        if (len+1<sizeof(plotname))
                        {
                        plotname[len]='i';
                        plotname[len+1]='\0';
                      }
                    }



                   }
                   strcpy(list_of_file[j],plotname);
                   strcat(plotname,".gr");

                }
                else  snprintf(plotname,sizeof(plotname),"plot%04d.gr",j);
                fprintf(fpc,"plot %d %s\n",j,plotname);
                fclose(fpc);
                snprintf(plotfullname,sizeof(plotfullname),"%s%s",c_dir,plotname);
                i = save_one_plot_bin(op,plotfullname);
                backslash_to_slash(plotfullname);
                if (i)	win_printf ("Cannot open\n%s !...",plotfullname);
                if (op->filename != NULL) free(op->filename);
                op->filename = strdup(plotname);
                if (op->dir != NULL) 	free(op->dir);
                op->dir = strdup(c_dir);
            }

            /* save multiplots status */
            fpc = fopen(fullfile,"a");
            if (fpc == NULL) return win_printf("cannot open file:\n%s",backslash_to_slash(fullfile));
            i = find_multi_plots_status(pr);
            if (i)	fprintf(fpc,"multi-plots %d\n",i);
            else fprintf(fpc,"current-plot %d\n",pr->cur_op);
            fclose(fpc);
            //			win_printf("multi plots in %d columns",i);
        }
        else no_file = 0;
    }
    return 0;
}
