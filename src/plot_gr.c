# ifndef _PLOT_GR_C_
# define _PLOT_GR_C_


# include "xvin.h"
# include "color.h"
# include "gr_file.h"
# include "unitset.h"
# include "plot_ds.h"
# include "plot_op.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "util.h"
# include "xv_tools_lib.h"
# ifndef XV_WIN32
#include <sys/stat.h>
# endif
# include <locale.h>
# include <stdlib.h>
# include <string.h>

long    counter = 0;
char    *print_file = NULL;
int     iopt;           /* options variable for qkdraw */
float   abslow, dx;     /* offset and scale for automatic abscissas */
int     absf;           /* flag for automatic abscissas */
int     sizef = 0;          /* flag for changing plot size */
int     tflag = 0;                  /* flag for transposing axes */
int     aflag = 0;
char    plt_data_path[512] = {0};


int     pltreadfile(O_p *op, char *file_name, int check);
int     set_plot_opts(O_p *op, int argc, char **argv, char *line, int check);
int     pltxlimread(O_p *op, int *argcp, char ***argvp);
int     pltylimread(O_p *op, int *argcp, char ***argvp);

int     push_new_data(O_p *op, float tx, float ty);
int     push_bin_float_data(O_p *op, char *filename);
int     push_bin_int_data(O_p *op, char *filename);
int     push_plot_label(O_p *op, float tx, float ty, char *label, int type);
int     init_data_set(O_p *op);
int     close_data_set(O_p *op);
int     push_bin_float_data_z(O_p *op, char *filename, int offset, int nx);
int     push_bin_float_error_z(O_p *op, char *filename, int offset, int nx, int axis);
int     push_bin_float_dwn_error_z(O_p *op, char *filename, int offset, int nx, int axis);
int     push_bin_float_box_up_error_z(O_p *op, char *filename, int offset, int nx, int axis);
int     push_bin_float_box_dwn_error_z(O_p *op, char *filename, int offset, int nx, int axis);


# ifdef NEW

pr = build_plot_region(max_x *x0, max_y *y0, max_x *w, max_y *h);
hook_it_up(ac, IS_BOX_REGION, (void *)pr, 0);
if (scale == 0) pr->screen_scale |= AUTO_SCALE;
else pr->screen_scale = (int)(0.5 + ((float)28 / scale))
if (strstr(file, ".cgr") != NULL || strstr(file, ".CGR") != NULL)
{
    /*      win_printf("loading cgr file %s",file);  */
    load_cgr_file_in_pltreg(pr, file, path);
}
else
{
    /*     win_printf("loading gr file %s",file);   */
    load_plt_file_in_pltreg(pr, file, path);
}

# endif


O_p *create_plot_from_gr_file(const char *file, const char *path)
{
    int i;
    O_p *op = NULL;
    d_s *ds = NULL;

    if (file == NULL)  return NULL;

    op = (O_p *)calloc(1, sizeof(O_p));
    if (op == NULL)   return NULL;
    init_one_plot(op);

    if ((ds = create_and_attach_one_ds(op, GR_SIZE, GR_SIZE, 0)) == NULL)
    {
        free_one_plot(op);
        return NULL;
    }
    ds->nx = ds->ny = 0;
    snprintf(f_in, 256, "%s%s", path, file);
    if (check_if_gr_file_is_an_image(f_in))
    {
        i = win_printf("%s\nlooks like gr file containing an image!\n"
                       "I recommend to skip loading by pressing WIN_CANCEL\n"
                       , backslash_to_slash(f_in));
        if (i == WIN_CANCEL) return NULL;
    }
    if ( pltreadfile(op, f_in, 0) < MAX_ERROR)
    {
        op->filename = strdup(file);
        op->dir = Mystrdup(path);
        for (i = 0 ; i < op->n_dat ; i++)
        {
            if (op->dat[i]->source == NULL)
                op->dat[i]->source = strdup(file);
            if (ds_special_2_use != NULL)
                ds_special_2_use(op->dat[i]);
        }
        op->need_to_refresh = 1;
        return op;
    }
    else
    {
        free_one_plot(op);
        win_printf("(create_plot_from_gr_file) \n file %s \n not loaded", backslash_to_slash(f_in));
        return NULL;
    }
    return op;
}


int load_plt_file_in_pltreg(pltreg *pr, char *file, char *path)
{
    register int i;
    O_p *op = NULL;
    d_s *ds = NULL;

    if (pr == NULL || file == NULL || path == NULL)       return 2;


    if (pr->n_op != 1 || pr->one_p->n_dat != 0)
    {
        op = (O_p *)calloc(1, sizeof(O_p));
        if (op == NULL)       return 1;
        init_one_plot(op);
        add_data( pr, IS_ONE_PLOT, (void *)op);
    }
    pr->cur_op = pr->n_op - 1;
    pr->one_p = pr->o_p[pr->cur_op];
    ds = build_adjust_data_set(NULL, GR_SIZE, GR_SIZE);
    ds->nx = 0;
    ds->ny = 0;
    add_data( pr, IS_DATA_SET, (void *)ds);
    snprintf(f_in, 256, "%s%s", path, file);
    if ( pltreadfile(pr->one_p, f_in, 0) < MAX_ERROR)
    {
        pr->one_p->filename = strdup(file);
        pr->one_p->dir = Mystrdup(path);
        for (i = 0 ; i < pr->one_p->n_dat ; i++)
        {
            if (pr->one_p->dat[i]->source == NULL)
                pr->one_p->dat[i]->source = strdup(file);
            if (ds_special_2_use != NULL)
                ds_special_2_use(pr->one_p->dat[i]);
        }
        //remove_one_plot_data (pr->one_p, IS_DATA_SET, (void*)ds);
        /*        refresh_plot(pr, UNCHANGED); */
        return 0;
    }
    else
    {
        if (ds != NULL) remove_one_plot_data (pr->one_p, IS_DATA_SET, (void *)ds);
        refresh_plot(pr, UNCHANGED);
        win_printf("(load_plt_file_in_pltreg()) \n file %s \n not loaded", backslash_to_slash(f_in));
        return 1;
    }
    return 0;
}



/*  int pltreadfile(O_p *op, char *file_name)
 *  DESCRIPTION read input and translate graph option and data
 *
 *
 *
 *  RETURNS     0 on success, 1
 *
 */
int pltreadfile(O_p *op, char *file_name, int check)
{
    register int i, j, k;
    int load_abort = 0, total_line_read = 0;
    float tmpx, tmpy;
    char *c1 = NULL, *c2 = NULL;
    char *line = NULL, *line1 = NULL;
    char **agv = NULL, **agvk = NULL;
    int agc = 0;

    setlocale(LC_ALL, "C");

    if (op == NULL)    return MAX_ERROR;
    absf = cur_i_f = n_error = iopt = 0;
    counter = 0;
    abslow = 0;
    dx = 1;
    if (aflag)
    {
        absf = 1;
        counter = 0;
        abslow = 0;
        dx = 1;
    }


    i_f[cur_i_f].n_line = 0;
    i_f[cur_i_f].filename = strdup(file_name);
    i_f[cur_i_f].fpi = fopen(i_f[cur_i_f].filename, "r");
    if ( i_f[cur_i_f].fpi == NULL)
    {
        error_in_file("cannot open file\n%s", i_f[cur_i_f].filename);
        return MAX_ERROR;
    }

    line = (char *)calloc(B_LINE, sizeof(char));
    line1 = (char *)calloc(B_LINE, sizeof(char));
    lferror = (char *)calloc(B_LINE, sizeof(char));
    agv = (char **)calloc(OP_SIZE, sizeof(char *));
    agvk = (char **)calloc(OP_SIZE, sizeof(char *));

    if ( line == NULL || line1 == NULL || lferror == NULL || agv == NULL || agvk == NULL)
    {
        error_in_file("malloc error \n");
        return MAX_ERROR;
    }
    if (plt_data_path == NULL) error_in_file("null plt_data_path buffer");


    if (extract_file_path(plt_data_path, 256, file_name) != plt_data_path)
    {
        if (my_getcwd(plt_data_path, 256) == NULL)
            error_in_file("could not find path!");
    }
    for (i = 0; plt_data_path[i] != 0; i++);
    i = (i > 0) ? i - 1 : i;
    if (plt_data_path[i] != '\\' && plt_data_path[i] != '/')
        my_strncat(plt_data_path, "\\", sizeof(plt_data_path));

    /*        win_printf("file_name >%s<\n cannot find plot path >%s<",file_name,plt_data_path); */


    while (load_abort < MAX_ERROR && total_line_read <= check)
    {
        while ((c1 = get_next_line(line)) == NULL && (cur_i_f > 0))
        {
            if (i_f[cur_i_f].fpi != NULL)
                fclose(i_f[cur_i_f].fpi);
            i_f[cur_i_f].fpi = NULL;
            cur_i_f--;
        }
        //        win_printf("c1   = %s\nline = %s", c1, line);

        if (check)    total_line_read++;
        if ( c1 == NULL)  break;
        line1[0] = 0;
        i = sscanf(line, "%f", &tmpx);
        if (i == 1)   i = sscanf(line, "%f%f", &tmpx, &tmpy);
        if (i == 2)   i = sscanf(line, "%f%f%s", &tmpx, &tmpy, line1);
        if (i == 3) /* may be a label */
        {
            if ( line1[0] == '%') /* start a comment */
            {
                i = 2;
                j = 0;
            }
            else if ( line1[0] == '"')/*start as a label*/
            {
                c1 = strchr(line, '"');
                c2 = line1;
                j = get_label(&c1, &c2, line);
            }
            else
            {
                load_abort = error_in_file("a label must start and end\nwith a double quote !...\n->%s", line);
                j = 0;
            }
            if (j != 0)
            {
                if (check == 0)
                {
                    if (op->n_dat > 0 && op->dat[op->n_dat - 1]->nx == 0)
                    {
                        push_plot_label_in_ds(op->dat[op->n_dat - 1], tmpx, tmpy, c2, USR_COORD);
                    }
                    else push_plot_label(op, tmpx, tmpy, c2, USR_COORD);
                    /*                    itmp = op->n_dat;
                                  win_printf("plot label n_ dat %d\nds %d nx %d",itmp,itmp - 1, op->dat[itmp - 1]->nx);                   */
                }
            }
            else if ( i == 3 )
            {
                load_abort = error_in_file ("empty label !...\n->%s", line);
            }
        }
        if ( i == 2)  /* may be a data point given by x,y */
        {
            if ( absf == 0)
            {
                if (check == 0) push_new_data(op, tmpx, tmpy);
            }
            else
            {
                load_abort = error_in_file ("you can't input an x,y data point\n when you have specified a -a option\n->%s", line);
            }
        }
        if ( i == 1)  /* may be a data point given by y only */
        {
            if ( absf == 1 || absf == 2 )
            {
                tmpy = counter * dx + abslow;
                counter++;
                if (check == 0) push_new_data(op, tmpy, tmpx);
            }
            else
            {
                load_abort = error_in_file ("you can't input data point with only y\nwithout specifying the -a option\n->%s", line);
            }
        }
        if ( i == 0 ) /* a command line */
        {
            /* advance to 1st item */
            while (*c1 == ' ' || *c1 == '\t' || *c1 == '#') c1++;
            for (k = 0 ; k < agc ; k++)
            {
                if (agvk[k] != NULL)      free(agvk[k]);
                agvk[k] = NULL;
            }
            agc = 1;

            while ( (c1 != NULL) && (*c1 != 0) )
            {
                if ( *c1 == '%')  *c1 = 0;
                else if ( *c1 != '"')
                {
                    if (sscanf(c1, "%s", line1) == 1)
                    {
                        agvk[agc] = agv[agc] = strdup(line1);
                        agc++;
                    }
                    if (agc >= OP_SIZE)
                    {
                        error_in_file ("too many options\nin input line\n->%s", line);
                        load_abort = MAX_ERROR;
                    }
                    if (c1 != NULL && strchr(c1, ' ') != NULL)  c1 = strchr ( c1, ' ');
                    else if (c1 != NULL && strchr(c1, '\t') != NULL) c1 = strchr (c1, '\t');
                    else  *c1 = 0;
                }
                else
                {
                    c2 = line1;
                    c2[0] = 0;
                    if (get_label(&c1, &c2, line))
                    {
                        agvk[agc] = agv[agc] = strdup(c2);
                        agc++;
                    }
                    if (agc >= OP_SIZE)
                    {
                        error_in_file("too many options\nin input line\n->%s", line);
                        load_abort = MAX_ERROR;
                    }
                }
                while (*c1 == ' ' || *c1 == '\t' || *c1 == '\n')
                {
                    c1++;
                }
            }
            if (agc > 1)
            {
                if (set_plot_opts(op, agc, agv, line, check) == MAX_ERROR)
                {
                    //                  win_printf("coucou456 \n check = %d \n agc = %d \n agv = %s \n line = %s", check,agc,agv,line);
                    load_abort = MAX_ERROR;
                }
            }
            for (k = 0 ; k < agc ; k++)
            {
                if (agvk[k] != NULL)      free(agvk[k]);
                agvk[k] = NULL;
            }
            /*            for (k = 1 ; k < agc ; k++) free(agvk[k]);              */
            agc = 1;
        }
    }
    if (line) free(line);
    if (line1) free(line1);
    if (agv) free(agv);
    if (agvk) free(agvk);
    if (lferror) free(lferror);
    while ( cur_i_f >= 0 )
    {
        if (i_f[cur_i_f].fpi != NULL)     fclose( i_f[cur_i_f].fpi);
        if (i_f[cur_i_f].filename != NULL)    free(i_f[cur_i_f].filename);
        cur_i_f--;
    }
    if (check == 0)
    {
        for (i = 0, j = 1, op->c_xu = 0; i < op->n_xu && j != 0 ; i++)
        {
            j = 0;
            if (op->ax != op->xu[i]->ax)  j = 1;
            if (op->dx != op->xu[i]->dx)  j = 1;
            if (op->x_unit == NULL || op->xu[i]->name == NULL)    j = 1;
            else if (strncmp(op->xu[i]->name, op->x_unit, strlen(op->x_unit)) != 0) j = 1;
            if (j == 0)   op->c_xu = i;
        }
        for (i = 0, j = 1, op->c_yu = 0; i < op->n_yu && j != 0 ; i++)
        {
            j = 0;
            if (op->ay != op->yu[i]->ax)  j = 1;
            if (op->dy != op->yu[i]->dx)  j = 1;
            if (op->y_unit == NULL || op->yu[i]->name == NULL)    j = 1;
            else if (strncmp(op->yu[i]->name, op->y_unit, strlen(op->y_unit)) != 0) j = 1;
            if (j == 0)   op->c_yu = i;
        }
        for (i = 0, j = 1, op->c_tu = 0; i < op->n_tu && j != 0 ; i++)
        {
            j = 0;
            if (op->at != op->tu[i]->ax)  j = 1;
            if (op->dt != op->tu[i]->dx)  j = 1;
            if (op->t_unit == NULL || op->tu[i]->name == NULL)    j = 1;
            else if (strncmp(op->tu[i]->name, op->t_unit, strlen(op->t_unit)) != 0) j = 1;
            if (j == 0)   op->c_tu = i;
        }
    }
    return n_error;
}
int set_plot_opts(O_p *op, int argc, char **argv, char *line, int check)
{
  char file_name[66] = {0}, *cmd, *tmpch;
    float templ, temp1;
    int itemp, decade = 0, type = 0, subtype = 0, offset = 0, n_item = 0;
    un_s *un = NULL;


    if (op == NULL)       return xvin_error(Wrong_Argument);
    file_name[0] = 0;
    while (--argc > 0)
    {
        argv++;
        cmd = argv[0];

again:
        switch (argv[0][0])
        {
        case '-':       /* option delimeter */
            argv[0]++;
            goto again;
        case 'i':       /* input file */
            if ( argv[0][1] == 'b' && argv[0][2] == 'f' && argv[0][3] == 'z')
            {
                if (!gr_numbi(&offset, &argc, &argv))   break;
                if (!gr_numbi(&n_item, &argc, &argv))   break;

                if (check == 0)
                    push_bin_float_data_z(op, i_f[cur_i_f].filename, offset, n_item);
            }
            else if ( argv[0][1] == 'b' && argv[0][2] == 'f'  )
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if (plt_data_path[0] != 0)
                        snprintf(file_name, sizeof(file_name), "%s%s", plt_data_path, argv[0]);
                    else snprintf(file_name, sizeof(file_name), "%s", argv[0]);
                    if (check == 0)       push_bin_float_data(op, file_name);
                }
            }
            else if ( argv[0][1] == 'b' && argv[0][2] == 'i' )
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if (plt_data_path[0] != 0)
                        snprintf(file_name, sizeof(file_name), "%s%s", plt_data_path, argv[0]);
                    else snprintf(file_name, sizeof(file_name), "%s", argv[0]);
                    if (check == 0)       push_bin_int_data(op, file_name);
                }
            }
            /* send directly to the output */
            else if ( argv[0][1] == 'd' && argv[0][2] == 'u' )
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    printf("%s\n", argv[0]);
                }
            }
            else if (argc >= 2)
            {
                argc--;
                argv++;
                cur_i_f++;
                if (cur_i_f < I_F_SIZE)
                {
                    if (plt_data_path[0] != 0)
                        snprintf(file_name, sizeof(file_name), "%s%s", plt_data_path, argv[0]);
                    else snprintf(file_name, sizeof(file_name), "%s", argv[0]);
                    i_f[cur_i_f].n_line = 0;
                    i_f[cur_i_f].filename = strdup(file_name);
                    i_f[cur_i_f].fpi = fopen(i_f[cur_i_f].filename, "r");
                    if ( i_f[cur_i_f].fpi == NULL)
                    {
                        error_in_file("cannot open file\n %s", i_f[cur_i_f].filename);
                        return MAX_ERROR;
                    }
                }
                else
                {
                    error_in_file("I cannot handle more\nthan %d nested files", I_F_SIZE);
                    return MAX_ERROR;
                }
            }
            break;


        case 'e':       /* input error from file */
            if ( argv[0][1] == 'x' && argv[0][2] == 'b' && argv[0][3] == 'f' && argv[0][4] == 'z')
            {
                if (!gr_numbi(&offset, &argc, &argv))   break;
                if (!gr_numbi(&n_item, &argc, &argv))   break;

                if (check == 0)
                    push_bin_float_error_z(op, i_f[cur_i_f].filename, offset, n_item, X_AXIS);
            }
            if ( argv[0][1] == 'y' && argv[0][2] == 'b' && argv[0][3] == 'f' && argv[0][4] == 'z')
            {
                if (!gr_numbi(&offset, &argc, &argv))   break;
                if (!gr_numbi(&n_item, &argc, &argv))   break;

                if (check == 0)
                    push_bin_float_error_z(op, i_f[cur_i_f].filename, offset, n_item, Y_AXIS);
            }
            if ( argv[0][1] == 'd' && argv[0][2] == 'x' && argv[0][3] == 'b' && argv[0][4] == 'f' && argv[0][5] == 'z')
            {
                if (!gr_numbi(&offset, &argc, &argv))   break;
                if (!gr_numbi(&n_item, &argc, &argv))   break;

                if (check == 0)
                    push_bin_float_dwn_error_z(op, i_f[cur_i_f].filename, offset, n_item, X_AXIS);
            }
            if ( argv[0][1] == 'd' && argv[0][2] == 'y' && argv[0][3] == 'b' && argv[0][4] == 'f' && argv[0][5] == 'z')
            {
                if (!gr_numbi(&offset, &argc, &argv))   break;
                if (!gr_numbi(&n_item, &argc, &argv))   break;

                if (check == 0)
                    push_bin_float_dwn_error_z(op, i_f[cur_i_f].filename, offset, n_item, Y_AXIS);
            }
            if ( argv[0][1] == 'b' && argv[0][2] == 'u' && argv[0][3] == 'x' && argv[0][4] == 'b'
		 && argv[0][5] == 'f' && argv[0][6] == 'z')
            {
                if (!gr_numbi(&offset, &argc, &argv))   break;
                if (!gr_numbi(&n_item, &argc, &argv))   break;

                if (check == 0)
                    push_bin_float_box_up_error_z(op, i_f[cur_i_f].filename, offset, n_item, X_AXIS);
            }
            if ( argv[0][1] == 'b' && argv[0][2] == 'u' && argv[0][3] == 'y' && argv[0][4] == 'b'
		 && argv[0][5] == 'f' && argv[0][6] == 'z')
            {
                if (!gr_numbi(&offset, &argc, &argv))   break;
                if (!gr_numbi(&n_item, &argc, &argv))   break;

                if (check == 0)
                    push_bin_float_box_up_error_z(op, i_f[cur_i_f].filename, offset, n_item, Y_AXIS);
            }
            if ( argv[0][1] == 'b' && argv[0][2] == 'd' && argv[0][3] == 'x' && argv[0][4] == 'b'
		 && argv[0][5] == 'f' && argv[0][6] == 'z')
            {
                if (!gr_numbi(&offset, &argc, &argv))   break;
                if (!gr_numbi(&n_item, &argc, &argv))   break;

                if (check == 0)
                    push_bin_float_box_dwn_error_z(op, i_f[cur_i_f].filename, offset, n_item, X_AXIS);
            }
            if ( argv[0][1] == 'b' && argv[0][2] == 'd' && argv[0][3] == 'y' && argv[0][4] == 'b'
		 && argv[0][5] == 'f' && argv[0][6] == 'z')
            {
                if (!gr_numbi(&offset, &argc, &argv))   break;
                if (!gr_numbi(&n_item, &argc, &argv))   break;

                if (check == 0)
                    push_bin_float_box_dwn_error_z(op, i_f[cur_i_f].filename, offset, n_item, Y_AXIS);
            }
            break;


        case 'l':       /* label for plot */
            if ( strncmp(argv[0], "lxp", 3) == 0 )
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if (check == 0)
                        op->x_prime_title = Mystrdupre(op->x_prime_title, argv[0]);
                }
            }
            else if ( strncmp(argv[0], "lx", 2) == 0 )
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if (check == 0)
                        op->x_title = Mystrdupre(op->x_title, argv[0]);
                }
            }
            else if ( strncmp(argv[0], "lyp", 3) == 0 )
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if (check == 0)
                        op->y_prime_title = Mystrdupre(op->y_prime_title, argv[0]);
                }
            }
            else if ( strncmp(argv[0], "ly", 2) == 0 )
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if (check == 0)
                        op->y_title = Mystrdupre(op->y_title, argv[0]);
                }
            }
            else if ( strncmp(argv[0], "lr", 2) == 0 )
            {
                if (argc >= 4)
                {
                    if (!gr_numb(&templ, &argc, &argv))   break;
                    if (!gr_numb(&temp1, &argc, &argv)) break;
                    argc--;
                    argv++;
                    if (check == 0)
                    {
                        if (op->dat[op->n_dat - 1]->nx == 0)
                        {
                            push_plot_label_in_ds(op->dat[op->n_dat - 1], templ, temp1, argv[0], ABS_COORD);
                        }
                        else push_plot_label(op, templ, temp1, argv[0], ABS_COORD);
                    }
                }
            }
            else if ( strncmp(argv[0], "layr", 4) == 0 )
            {
                if (argc >= 4)
                {
                    if (!gr_numb(&templ, &argc, &argv))   break;
                    if (!gr_numb(&temp1, &argc, &argv)) break;
                    argc--;
                    argv++;
                    if (check == 0)
                    {
                        if (op->dat[op->n_dat - 1]->nx == 0)
                        {
                            push_plot_label_in_ds(op->dat[op->n_dat - 1], templ, temp1, argv[0], VERT_LABEL_ABS);
                        }
                        else push_plot_label(op, templ, temp1, argv[0], VERT_LABEL_ABS);
                    }
                }
            }
            else if ( strncmp(argv[0], "lay", 3) == 0 )
            {
                if (argc >= 4)
                {
                    if (!gr_numb(&templ, &argc, &argv))   break;
                    if (!gr_numb(&temp1, &argc, &argv)) break;
                    argc--;
                    argv++;
                    if (check == 0)
                    {
                        if (op->dat[op->n_dat - 1]->nx == 0)
                        {
                            push_plot_label_in_ds(op->dat[op->n_dat - 1], templ, temp1, argv[0], VERT_LABEL_USR);
                        }
                        else push_plot_label(op, templ, temp1, argv[0], VERT_LABEL_USR);
                    }
                }
            }

            else
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if (check == 0)
                        op->title = Mystrdupre(op->title, argv[0]);
                }
            }
            break;
        case 'p':       /* prefix and unit */
            if ( argv[0][1] == 'x' )
            {
                if (argc >= 3)
                {
                    argc--;
                    argv++;
                    if ((argv[0][0] != '!' ) && (check == 0))
                        op->x_prefix = Mystrdupre(op->x_prefix, argv[0]);
                    argc--;
                    argv++;
                    if ((argv[0][0] != '!' ) && (check == 0))
                        op->x_unit = Mystrdupre(op->x_unit, argv[0]);
                }
                else
                {
                    return error_in_file("-p prefix unit:\nInvalid argument\n"
                                         "%s\nline %s", cmd, line);
                }
            }
            else if ( argv[0][1] == 'y' )
            {
                if (argc >= 3)
                {
                    argc--;
                    argv++;
                    if ((argv[0][0] != '!' ) && (check == 0))
                        op->y_prefix = Mystrdupre(op->y_prefix, argv[0]);
                    argc--;
                    argv++;
                    if ((argv[0][0] != '!' ) && (check == 0))
                        op->y_unit = Mystrdupre(op->y_unit, argv[0]);
                }
                else
                {
                    return error_in_file("-p prefix unit:\nInvalid argument\n"
                                         "%s\nline %s", cmd, line);
                }
            }
            else if ( argv[0][1] == 't' )
            {
                if (argc >= 3)
                {
                    argc--;
                    argv++;
                    if ((argv[0][0] != '!' ) && (check == 0))
                        op->t_prefix = Mystrdupre(op->t_prefix, argv[0]);
                    argc--;
                    argv++;
                    if ((argv[0][0] != '!' ) && (check == 0))
                        op->t_unit = Mystrdupre(op->t_unit, argv[0]);
                }
                else
                {

                    return error_in_file("-p prefix unit:\nInvalid argument\n"
                                         "%s\nline %s", cmd, line);
                }
            }
            break;
        case 'd':       /* output device */
            if ( argv[0][1] == 'p' )
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if ( strncpy(plt_data_path, argv[0], sizeof(plt_data_path)) == NULL)
                        fprintf (stderr, "not valid data path %s   \n", argv[0]);
                }
                else
                {
                    return error_in_file("-dp plt_data_path:\nInvalid argument\n"
                                         "%s\nline %s", cmd, line);
                }
            }
            break;
        case 'm':       /* line mode */
            if ((check == 0) && (op->n_dat != 0))
            {
                close_data_set(op);
                init_data_set(op);
            }
            if (!gr_numb(&templ, &argc, &argv))
                itemp = argv[0][1] - '0';
            else
                itemp = (int)templ;
            switch (itemp)
            {
            case -'0':  /* null character */
            case 0:
                if (check == 0) op->dat[op->n_dat - 1]->m = 0; /*iopt += DOT*/
                break;
            case 2:
                if (check == 0) op->dat[op->n_dat - 1]->m = 2; /*iopt+=DASH;*/
                break;
            }
            break;
        case 'a':       /* automatic abscissas */
            if ( strncmp(argv[0], "axp", 3) == 0 )
            {
                if (!gr_numbi(&itemp, &argc, &argv))    break;
                if (check == 0) op->c_xu_p = itemp;
            }
            else if ( strncmp(argv[0], "ayp", 3) == 0 )
            {
                if (!gr_numbi(&itemp, &argc, &argv))    break;
                if (check == 0) op->c_yu_p = itemp;
            }
            else if ( argv[0][1] == '!')          absf = 0;
            else if ( strncmp(argv[0], "ax", 2) == 0 )
            {
                if (check == 0) op->dx = 1;
                if (!gr_numb(&templ, &argc, &argv))  break;
                if (check == 0) op->dx = templ;
                if (!gr_numb(&templ, &argc, &argv))  break;
                if (check == 0) op->ax = templ;
            }
            else if ( strncmp(argv[0], "ay", 2) == 0 )
            {
                if (check == 0) op->dy = 1;
                if (!gr_numb(&templ, &argc, &argv))  break;
                if (check == 0) op->dy = templ;
                if (!gr_numb(&templ, &argc, &argv))  break;
                if (check == 0) op->ay = templ;
            }
            else
            {
                absf = 1;
                counter = 0;
                abslow = 0;
                dx = 1;
                if (!gr_numb(&dx, &argc, &argv))
                    break;
                if (gr_numb(&abslow, &argc, &argv))
                    absf = 2;
            }
            break;
        case 'g':       /* grid style */
            if (!gr_numb(&templ, &argc, &argv))
                itemp = argv[0][1] - '0';
            else
                itemp = (int)templ;
            switch (itemp)
            {
            case -'0':  /* null character */
            case 0:
                if (check == 0)   op->iopt |= NOAXES;/*iopt+=NOAXES;*/
                break;
            case 1:
                if (check == 0)   op->iopt |= TRIM;/*iopt += TRIM;*/
                break;
            case 3:
                if (check == 0)   op->iopt |= AXES_PRIME;
                break;
            }
            break;
        case 'c':       /* plotting characters */
            if ( strncmp(argv[0], "color", 5) == 0 )
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if (check == 0)
                    {
                        if (strncmp(argv[0], "black", 5) == 0)
                            op->dat[op->n_dat - 1]->color =  Black;
                        else if (strncmp(argv[0], "blue", 4) == 0)
                            op->dat[op->n_dat - 1]->color =  Blue;
                        else if (strncmp(argv[0], "green", 5) == 0)
                            op->dat[op->n_dat - 1]->color =  Green;
                        else if (strncmp(argv[0], "cyan", 4) == 0)
                            op->dat[op->n_dat - 1]->color =  Cyan;
                        else if (strncmp(argv[0], "red", 3) == 0)
                            op->dat[op->n_dat - 1]->color =  Red;
                        else if (strncmp(argv[0], "magenta", 7) == 0)
                            op->dat[op->n_dat - 1]->color =  Magenta;
                        else if (strncmp(argv[0], "brown", 5) == 0)
                            op->dat[op->n_dat - 1]->color =  Brown;
                        else if (strncmp(argv[0], "lightgray", 9) == 0)
                            op->dat[op->n_dat - 1]->color =  Lightgray;
                        else if (strncmp(argv[0], "darkgray", 8) == 0)
                            op->dat[op->n_dat - 1]->color =  Darkgray;
                        else if (strncmp(argv[0], "lightblue", 9) == 0)
                            op->dat[op->n_dat - 1]->color =  Lightblue;
                        else if (strncmp(argv[0], "lightgreen", 10) == 0)
                            op->dat[op->n_dat - 1]->color =  Lightgreen;
                        else if (strncmp(argv[0], "lightcyan", 9) == 0)
                            op->dat[op->n_dat - 1]->color =  Lightcyan;
                        else if (strncmp(argv[0], "lightred", 8) == 0)
                            op->dat[op->n_dat - 1]->color =  Lightred;
                        else if (strncmp(argv[0], "lightmagenta", 12) == 0)
                            op->dat[op->n_dat - 1]->color =  Lightmagenta;
                        else if (strncmp(argv[0], "yellow", 6) == 0)
                            op->dat[op->n_dat - 1]->color =  Yellow;
                        else if (strncmp(argv[0], "white", 5) == 0)
                            op->dat[op->n_dat - 1]->color =  White;
                    }
                }
            }
            else
            {
                if ((check == 0) && (op->n_dat != 0))
                {
                    close_data_set(op);
                    init_data_set(op);
                }
                if (argc >= 2)
                {
                    if (check == 0)
                        op->dat[op->n_dat - 1]->symb = Mystrdupre(op->dat[op->n_dat - 1]->symb, argv[1]);
                    argv++;
                    argc--;
                }
            }
            break;
        case 's':
            if ( strncmp(argv[0], "src", 3) == 0 )
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if (check == 0)
                        op->dat[op->n_dat - 1]->source = Mystrdupre(op->dat[op->n_dat - 1]->source, argv[0]);
                }
            }
            if ( strncmp(argv[0], "special", 7) == 0 )
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if (check == 0)
                        add_one_plot_data(op, IS_SPECIAL, (void *)argv[0]);
                }
            }
            break;

        case 't':       /* transpose x and y */
            if ( strncmp(argv[0], "tus", 3) == 0 )
            {
                if (argc < 4)
                {
                    return error_in_file("-tus :\nInvalid argument\n"
                                         "%s\nline %s", cmd, line);
                }
                itemp = 0;
                templ = 1;
                temp1 = 0;
                if (!gr_numb(&templ, &argc, &argv))      itemp = 1;
                if (!gr_numb(&temp1, &argc, &argv))     itemp = 1;
                if (itemp)
                {
                    return error_in_file("-xus :\nInvalid argument\n"
                                         "%s\nline %s", cmd, line);
                }
                argc--;
                argv++;
                tmpch = strdup(argv[0]);
                type = 0;
                decade = 0;
                subtype = 0;
                if (argc >= 3)
                {
                    if (!gr_numbi(&type, &argc, &argv))     itemp = 1;
                    if (!gr_numbi(&decade, &argc, &argv))   itemp = 1;
                    if (!gr_numbi(&subtype, &argc, &argv))  itemp = 2;
                    if (itemp == 1)
                    {
                        return error_in_file("-xus :\nInvalid argument\n"
                                             "%s\nline %s", cmd, line);
                    }
                }
                else if (unit_to_type(tmpch, &type, &decade))
                {
                    return error_in_file("-xus :\nInvalid argument\n"
                                         "%s\nline %s", cmd, line);
                }
                un = build_unit_set(type, temp1, templ, (char)decade, 0, tmpch);
                if (tmpch)
                {
                    free(tmpch);
                    tmpch = NULL;
                }
                if (un == NULL)
                {
                    return error_in_file("-tus :\ncan't create\n%s\nline %s"
                                         , cmd, line);
                }
                un->sub_type = subtype;
                if (check == 0)
                    add_one_plot_data (op, IS_T_UNIT_SET, (void *)un);

            }
            else if ( argv[0][1] == 'k' && argv[0][2] == 'x')
            {
                gr_numb(&templ, &argc, &argv);
                if (check == 0) op->tick_len_x = templ;
            }
            else if ( argv[0][1] == 'k' && argv[0][2] == 'y')
            {
                gr_numb(&templ, &argc, &argv);
                if (check == 0) op->tick_len_y = templ;
            }
            else if ( argv[0][1] == 'k')
            {
                gr_numb(&templ, &argc, &argv);
                if (check == 0) op->tick_len = templ;
            }
            else  if ( strncmp(argv[0], "treat", 5) == 0 )
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if (check == 0)
                        op->dat[op->n_dat - 1]->treatement = Mystrdupre(op->dat[op->n_dat - 1]->treatement, argv[0]);
                }
            }
            else  if ( strncmp(argv[0], "time", 5) == 0 )
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if (check == 0)
                    {
                        if (sscanf(argv[0], "%lu", &(op->dat[op->n_dat - 1]->time)) != 1)
                        {
                            return error_in_file("Improper time\n\\it %s %s\n%s",
                                                 cmd, argv[0], line);
                        }
                    }
                }
            }
            else
            {
                if (check == 0) op->iopt |= CROSS;
            }
            break;
        case 'x':       /* x limits */
            if ( strncmp(argv[0], "xus", 3) == 0 )
            {
                if (argc < 4)
                {
                    return error_in_file("-xus :\nInvalid argument\n"
                                         "%s\nline %s", cmd, line);
                }
                itemp = 0;
                templ = 1;
                temp1 = 0;
                if (!gr_numb(&templ, &argc, &argv))      itemp = 1;
                if (!gr_numb(&temp1, &argc, &argv))     itemp = 1;
                if (itemp)
                {
                    return error_in_file("-xus :\nInvalid argument\n"
                                         "%s\nline %s", cmd, line);
                }
                argc--;
                argv++;
                tmpch = strdup(argv[0]);
                type = 0;
                decade = 0;
                subtype = 0;
                if (argc >= 3)
                {
                    if (!gr_numbi(&type, &argc, &argv))     itemp = 1;
                    if (!gr_numbi(&decade, &argc, &argv))   itemp = 1;
                    if (!gr_numbi(&subtype, &argc, &argv))  itemp = 2;
                    if (itemp == 1)
                    {
                        return error_in_file("-xus :\nInvalid argument\n"
                                             "%s\nline %s", cmd, line);
                    }
                }
                else if (unit_to_type(tmpch, &type, &decade))
                {
                    return error_in_file("-xus :\nInvalid argument\n"
                                         "%s\nline %s", cmd, line);
                }
                un = build_unit_set(type, temp1, templ, (char)decade, 0, tmpch);
                if (tmpch)
                {
                    free(tmpch);
                    tmpch = NULL;
                }
                if (un == NULL)
                {
                    return error_in_file("-xus :\ncan't create\n%s\nline %s"
                                         , cmd, line);
                }
                un->sub_type = subtype;
                if (check == 0)
                    add_one_plot_data (op, IS_X_UNIT_SET, (void *)un);

            }
            else
            {
                if (check == 0)
                {
                    if ( argv[0][1] == 'n') op->iopt2 &= ~X_NUM;
                    else          op->iopt2 |= X_NUM;
                }
                if (argc > 1 && argv[1][0] == 'l')
                {
                    argc--;
                    argv++;
                    if (check == 0) op->iopt |= XLOG;
                }
                if (check == 0)       pltxlimread(op, &argc, &argv);
                else
                {
                    gr_numb(&templ, &argc, &argv);
                    gr_numb(&templ, &argc, &argv);
                }
            }
            break;
        case 'y':       /* y limits */
            if ( strncmp(argv[0], "yus", 3) == 0 )
            {
                if (argc < 4)
                {
                    return error_in_file("-yus :\nInvalid argument\n"
                                         "%s\nline %s", cmd, line);
                }
                itemp = 0;
                templ = 1;
                temp1 = 0;
                if (!gr_numb(&templ, &argc, &argv))      itemp = 1;
                if (!gr_numb(&temp1, &argc, &argv))     itemp = 1;
                if (itemp)
                {
                    return error_in_file("-yus :\nInvalid argument\n"
                                         "%s\nline %s", cmd, line);
                }
                argc--;
                argv++;
                tmpch = strdup(argv[0]);
                type = 0;
                decade = 0;
                subtype = 0;
                if (argc >= 3)
                {
                    if (!gr_numbi(&type, &argc, &argv))     itemp = 1;
                    if (!gr_numbi(&decade, &argc, &argv))   itemp = 1;
                    if (!gr_numbi(&subtype, &argc, &argv))  itemp = 2;
                    if (itemp == 1)
                    {
                        return error_in_file("-yus :\nInvalid argument\n"
                                             "%s\nline %s", cmd, line);
                    }
                }
                else if (unit_to_type(tmpch, &type, &decade))
                {
                    return error_in_file("-yus :\nInvalid argument\n"
                                         "%s\nline %s", cmd, line);
                }
                un = build_unit_set(type, temp1, templ, (char)decade, 0, tmpch);
                if (un == NULL)
                {
                    return error_in_file("-yus :\ncant create\n"
                                         "%s\nline %s", cmd, line);
                }
                if (tmpch)
                {
                    free(tmpch);
                    tmpch = NULL;
                }
                un->sub_type = subtype;
                if (check == 0)
                    add_one_plot_data (op, IS_Y_UNIT_SET, (void *)un);
            }
            else
            {
                if (check == 0)
                {
                    if ( argv[0][1] == 'n')       op->iopt2 &= ~Y_NUM;
                    else                      op->iopt2 |= Y_NUM;
                }
                if (argc > 1 && argv[1][0] == 'l')
                {
                    argc--;
                    argv++;
                    if (check == 0) op->iopt |= YLOG;
                }
                if (check == 0)   pltylimread(op, &argc, &argv);
                else
                {
                    gr_numb(&templ, &argc, &argv);
                    gr_numb(&templ, &argc, &argv);
                }
            }
            break;
        case 'h':
            if (strncmp(argv[0], "his", 3) == 0)
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                    if (check == 0)
                        op->dat[op->n_dat - 1]->history = Mystrdupre(op->dat[op->n_dat - 1]->history, argv[0]);
                }
            }
            else if (gr_numb(&templ, &argc, &argv))
            {
                if (check == 0) op->height = templ;
                sizef++;
            }
            break;
        case 'w':
            if (gr_numb(&templ, &argc, &argv))
            {
                if (check == 0)   op->width = templ;
                sizef++;
            }
            break;
        case 'r':
            if (gr_numb(&templ, &argc, &argv))
            {
                if (check == 0)   op->right = templ;
                sizef++;
            }
            break;
        case 'u':
            if (gr_numb(&templ, &argc, &argv))
            {
                if (check == 0)   op->up = templ;
                sizef++;
            }
            break;
        default:
            if (check == 0)
            {
                win_printf("Plot options  invalid\n %s\n %s", cmd, line);
                return error_in_file("Plot Invalid argument\n"
                                     "%s\nline %s", cmd, line);
            }
            else
            {
                win_printf("Plot options  invalid\n %s\n %s", cmd, line);
                return ++n_error;
            }
        }
    }
    return 0;
}

# ifdef TEST
int set_plot_opts_check(O_p *op, int argc, char **argv, char *line)
{
    char ch[256] = {0}, *cmd = NULL, *tmpch = NULL;
    float templ, temp1;
    int i, itemp, decade = 0, type = 0, subtype = 0, offset = 0, n_item = 0;
    un_s *un = NULL;

    i = sscanf(argv[0], "%s", ch);
    if (i != 1)   return ;
    cmd = (ch[0] == '-') ? ch + 1 : ch;
    if (strlen(cmd) == 1)
    {

    }
    while (--argc > 0)
    {
        argv++;
        cmd = argv[0];
again1:
        switch (argv[0][0])
        {
        case '-':       /* option delimeter */
            argv[0]++;
            goto again1;
        case 'i':       /* input file */
            if (strncmp(argv[0], "ibfz", 4) == 0)
            {
                if (argc > 2)
                {
                    argc--;
                    argv++;
                    argc--;
                    argv++;
                }
                else break;
            }
            if (strncmp(argv[0], "ibf") == 0
                    || strncmp(argv[0], "ibi") == 0 || strncmp(argv[0], "idu") == 0)
            {
                if (argc > 1)
                {
                    argc--;
                    argv++;
                }
                else break;
            }
            else if (argc >= 2)
            {
                argc--;
                argv++;
                cur_i_f++;
                if (cur_i_f < I_F_SIZE)
                {
                    if (plt_data_path[0] != 0)
                        snprintf(file_name, sizeof(file_name), "%s%s", plt_data_path, argv[0]);
                    else snprintf(file_name, sizeof(file_name), "%s", plt_data_path, argv[0]);
                    i_f[cur_i_f].n_line = 0;
                    i_f[cur_i_f].filename = strdup(file_name);
                    i_f[cur_i_f].fpi = fopen(i_f[cur_i_f].filename, "r");
                    if ( i_f[cur_i_f].fpi == NULL)
                    {
                        return MAX_ERROR;
                    }
                }
                else
                {
                    return MAX_ERROR;
                }
            }
            break;
        case 'l':       /* label for plot */
            if (strncmp(argv[0], "lxp", 3) == 0 || strncmp(argv[0], "lx", 2) == 0
                    || strncmp(argv[0], "lyp", 3) == 0 || strncmp(argv[0], "ly", 2) == 0)
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                }
                break;
            }
            else if (strncmp(argv[0], "lr", 2) == 0)strncmp(argv[0], "layr", 4) == 0
                || strncmp(argv[0], "lay", 3) == 0 )
            {
                if (argc >= 4)
                {
                    argc -= 3;
                    argv += 3;
                }
                break;
            }
            else
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                }
                break;
            }
            break;
        case 'p':       /* prefix and unit */
                if ( strncmp(argv[0], "px", 2) == 0 || strncmp(argv[0], "py", 2) == 0)
                {
                    if (argc >= 3)
                    {
                        argc -= 2;
                        argv += 2;
                    }
                    break;
                }
                else  return 1;
                    break;
                case 'd':       /* output device */
                        if ( strncmp(argv[0], "dp", 2) == 0)
                        {
                            if (argc >= 2)
                            {
                                argc--;
                                argv++;
                            }
                            break;
                        }
                        else return 1;
                            break;
                        case 'm':       /* line mode */
                                if (argc >= 2)
                            {
                                argc--;
                                argv++;
                            }
        break;
    case 'a':       /* automatic abscissas */
            if (strncmp(argv[0], "a!", 2) == 0) break;
                                        else if (strncmp(argv[0], "axp", 3) == 0 || strncmp(argv[0], "ayp", 3) == 0)
                                        {
                                            if (argc >= 2)
                                            {
                                                argc--;
                                                argv++;
                                            }
                                            break;
                                        }
                                        else if (strncmp(argv[0], "ax", 2) == 0 || strncmp(argv[0], "ay", 2) == 0)
                                        {
                                            if (argc > 2)
                                            {
                                                argc -= 2;
                                                argv += 2;
                                            }
                                            break;
                                        }
                                        else
                                        {
                                            if (argc > 2)
                                            {
                                                argc -= 2;
                                                argv += 2;
                                            }
                                            if (argc >= 1)
                                            {
                                                argc--;
                                                argv++;
                                            }
                                            break;
                                        }
            break;
        case 'g':       /* grid style */
                if (argc >= 2)
            {
                argc--;
                argv++;
            }
        break
    case 'c':       /* plotting characters */
            if (argc >= 2)
            {
                argc--;
                argv++;
            }
        break
    case 's':
            if ( strncmp(argv[0], "src", 3) == 0 ||  strncmp(argv[0], "special", 7)
                {
                    if (argc >= 2)
                    {
                        argc--;
                        argv++;
                    }
                    break;
                }
                else return 1;
            break;
        case 't':       /* transpose x and y */
            if (strncmp(argv[0], "tkx", 3) == 0 || strncmp(argv[0], "tky", 3) == 0
                    || strncmp(argv[0], "ta!", 3) == 0 || strncmp(argv[0], "treat", 2) == 0
                    || strncmp(argv[0], "treat", 5) == 0 || strncmp(argv[0], "time", 5) == 0)
            {
                if (argc >= 2)
                {
                    argc--;
                    argv++;
                }
                break;
            }
            else return 1;
            break;
        case 'x':       /* x limits */
            if (strncmp(argv[0], "xus", 3) == 0)
            {
                if (argc >= 8)
                {
                    argc -= 6;
                    argv += 6;
                }
                break;
            }
            else
            {
                if (argc > 1 && argv[1][0] == 'l')
                {
                    argc--;
                    argv++;
                }
                if (argc >= 3)
                {
                    argc -= 2;
                    argv += 2;
                }
                break;
            }
            break;
        case 'y':       /* y limits */
            if (strncmp(argv[0], "yus", 3) == 0)
            {
                if (argc >= 8)
                {
                    argc -= 6;
                    argv += 6;
                }
                break;
            }
            else
            {
                if (argc > 1 && argv[1][0] == 'l')
                {
                    argc--;
                    argv++;
                }
                if (argc >= 3)
                {
                    argc -= 2;
                    argv += 2;
                }
                break;
            }
            break;
        case 'h':
            if (argc >= 2)
            {
                argc--;
                argv++;
            }
            break
        case 'w':
            if (argc >= 2)
            {
                argc--;
                argv++;
            }
            break
        case 'r':
            if (argc >= 2)
            {
                argc--;
                argv++;
            }
            break
        case 'u':
            if (argc >= 2)
            {
                argc--;
                argv++;
            }
            break
        default:
            return 1;
        }
    }
    return 0;
}
# endif
int pltxlimread(O_p *op, int *argcp, char ***argvp)
{
    if (op == NULL || argcp == NULL)  return -1;
    if (!gr_numb(&(op->x_lo), argcp, argvp))     return 0;
    if (!gr_numb(&(op->x_hi), argcp, argvp))     return 1;
    op->iopt2 |= X_LIM;
    return 2;
}
int pltylimread(O_p *op, int *argcp, char ***argvp)
{
    if (op == NULL || argcp == NULL)  return -1;
    if (!gr_numb(&(op->y_lo), argcp, argvp))     return 0;
    if (!gr_numb(&(op->y_hi), argcp, argvp))     return 1;
    op->iopt2 |= Y_LIM;
    return 2;
}
/*  push_new_data(tx,ty);
 *  DESCRIPTION
 *
 *
 *
 *  RETURNS     0 on success, 1
 *
 */
int push_new_data(O_p *op, float tx, float ty)
{
    d_s *ds = NULL;

    if (op == NULL)  return 1;
    if (op->n_dat == 0)   if (init_data_set(op)) return 1;
    ds = op->dat[op->n_dat - 1];
    if ( ds->nx >= ds->mx || ds->ny >= ds->my)
    {
        ds = build_adjust_data_set(ds, ds->mx + GR_SIZE, ds->my + GR_SIZE);
        if (ds == NULL)       return 1;
    }
    ds->xd[ds->nx] = tx;
    ds->yd[ds->ny] = ty;
    ds->nx++;
    ds->ny++;
    return 0;
}
int close_data_set(O_p *op)
{
    d_s *ds = NULL, *dd = NULL;

    if (op == NULL)  return 1;
    if (op->n_dat == 0)           return 1;
    ds = op->dat[op->n_dat - 1];
    if (ds->nx == 0 || ds->ny == 0)       return 1;
    dd = duplicate_data_set(ds, NULL);
    if (dd == NULL)               return 1;
    dd->time = ds->time;
    op->dat[op->n_dat - 1] = dd;
    free_data_set(ds);
    return 0;
}
int init_data_set(O_p *op)
{
    d_s *ds = NULL;

    if (op == NULL)  return 1;
    if (op->n_dat > 0)
    {
        ds = op->dat[op->n_dat - 1];
        if (ds->nx == 0 || ds->ny == 0)       return 0;
    }
    ds = build_adjust_data_set(NULL, GR_SIZE, GR_SIZE);
    if (ds == NULL)       return 1;
    ds->nx = 0;
    ds->ny = 0;
    add_one_plot_data(op, IS_DATA_SET, (void *)ds);
    return 0;
}

/*  push_bin_float_data_z(O_p *op, char *filename,int offset,int n_item);
 *  DESCRIPTION read binary n_item float data starting at CRT-Z + offset

 *  RETURNS     0 on success, 1
 *
 */
int push_bin_float_data_z(O_p *op, char *filename, int offset, int nx)
{
    register int i, j;
    char ch;
    int ret = 0;
    size_t len;
    FILE *binfp = NULL;
    d_s *ds = NULL;
    static char previous_file[512] = {0};
    static int first = 1;
    static long int cz = 0;
    long long filesize;
    static unsigned long t0 = 0, dt;

    if (op == NULL)  return 1;
    binfp = fopen (filename, "rb");
    if ( binfp == NULL )
        return error_in_file ("%s binary file not found!...  \n", backslash_to_slash(filename));

# ifdef XV_WIN32
    filesize = _filelengthi64(fileno(binfp));
#else
    struct stat st;
    stat(filename, &st);
    filesize = st.st_size;
#endif
    dt = get_my_uclocks_per_sec() / 4;

    if (my_uclock() - t0 > dt)
    {
        t0 = my_uclock();
        my_set_window_title("Loading %s %6.2f / %6.2f Mb", backslash_to_slash(filename), (double)offset / 1048576,
                            (double)filesize / 1048576);
    }

    //my_set_window_title("Reading %s offset %d size %d",backslash_to_slash(filename),offset,nx);
    if (first)
    {
        strncpy(previous_file, filename, sizeof(previous_file));
        first = 0;
    }
    len = strlen(filename);
    len = (len > strlen(previous_file)) ? strlen(previous_file) : len;
    if (cz == 0 || strncmp(previous_file, filename, len) != 0 )
    {
        strncpy(previous_file, filename, sizeof(previous_file));
        while (fread (&ch, sizeof (char), 1, binfp) == 1 && ch != CRT_Z);
        cz = ftell(binfp);
        fseek(binfp, offset, SEEK_CUR);
    }
    else fseek(binfp, cz + offset, SEEK_SET);

    if (op->n_dat == 0)   if (init_data_set(op)) return 1;
    ds = op->dat[op->n_dat - 1];
    if ( nx >= ds->mx || nx >= ds->my)
        ds = build_adjust_data_set(ds, nx, nx);
    if (ds == NULL)
    {
        fclose(binfp);
        return  error_in_file ("could not create data set in binary input file \n%s \n", backslash_to_slash(filename));
    }
    if ( absf == 0 )
    {
        if (fread (ds->xd, sizeof(float), nx, binfp) != (size_t) nx)
            error_in_file ("could not read x binary data in file \n%s \n", backslash_to_slash(filename));
        if (fread (ds->yd, sizeof(float), nx, binfp) != (size_t) nx)
            error_in_file ("could not read y binary data in file \n%s \n", backslash_to_slash(filename));

#ifdef XV_MAC
#ifdef MAC_POWERPC
        swap_bytes(ds->xd, nx, sizeof(float));
        swap_bytes(ds->yd, nx, sizeof(float));
#endif
#endif
        for (i = j = 0; i < nx; i++)
            j = (isnan(ds->yd[i]) || isnan(ds->xd[i])) ? j + 1 : j;
        if (j) win_printf("Corrupted data in binary read!");

    }
    else if ( absf == 1 || absf == 2 )
    {
        if (fread (ds->yd, sizeof(float), nx, binfp) != (size_t) nx)
            error_in_file ("could not read y binary data in file \n%s \n", backslash_to_slash(filename));
#ifdef XV_MAC
#ifdef MAC_POWERPC
        swap_bytes(ds->xd, nx, sizeof(float));
#endif
#endif
        for (i = 0; i < nx; i++)
            ds->xd[i] = i * dx + abslow ;
        for (i = j = 0; i < nx; i++)
            j = (isnan(ds->yd[i]) || isnan(ds->xd[i])) ? j + 1 : j;
        if (j) win_printf("Corrupted data in binary read!");
    }
    else ret = 1;
    ds->nx = nx;
    ds->ny = nx;
    fclose (binfp);
    return (ret) ? -1 : nx;
}






/*  push_bin_float_data_z(O_p *op, char *filename,int offset,int n_item);
 *  DESCRIPTION read binary n_item float data starting at CRT-Z + offset

 *  RETURNS     0 on success, 1
 *
 */
int push_bin_float_error_z(O_p *op, char *filename, int offset, int nx, int axis)
{
    //register int i;
    char ch;
    int ret = 0;
    size_t len;
    float *array = NULL;
    FILE *binfp = NULL;
    d_s *ds = NULL;
    static char previous_file[512] = {0};
    static int first = 1;
    static long int cz = 0;

    if (op == NULL)  return 1;
    binfp = fopen (filename, "rb");
    if ( binfp == NULL )
        return error_in_file ("%s binary file not found!...  \n", backslash_to_slash(filename));
    //my_set_window_title("Reading %s offset %d size %d",backslash_to_slash(filename),offset,nx);
    if (first)
    {
        strncpy(previous_file, filename, sizeof(previous_file));
        first = 0;
    }
    len = strlen(filename);
    len = (len > strlen(previous_file)) ? strlen(previous_file) : len;
    if (cz == 0 || strncmp(previous_file, filename, len) != 0 )
    {
        strncpy(previous_file, filename, sizeof(previous_file));
        while (fread (&ch, sizeof (char), 1, binfp) == 1 && ch != CRT_Z);
        cz = ftell(binfp);
        fseek(binfp, offset, SEEK_CUR);
    }
    else fseek(binfp, cz + offset, SEEK_SET);
    if (op->n_dat == 0)   if (init_data_set(op)) return 1;
    ds = op->dat[op->n_dat - 1];

    if (axis == X_AXIS)
    {
        if (ds->xe != NULL) free(ds->xe);
        if ((alloc_data_set_x_error(ds) == NULL) || (ds->xe == NULL))
            return win_printf_OK("I can't create x std. errors !");
        array = ds->xe;
    }
    else if (axis == Y_AXIS)
    {
        if (ds->ye != NULL) free(ds->ye);
        if ((alloc_data_set_y_error(ds) == NULL) || (ds->ye == NULL))
            return win_printf_OK("I can't create y std. errors !");
        array = ds->ye;
    }

    if (array == NULL)
    {
        fclose(binfp);
        return  error_in_file ("could not create data set error in binary input file \n%s \n", backslash_to_slash(filename));
    }

    if (fread (array, sizeof(float), nx, binfp) != (size_t) nx)
        error_in_file ("could not read x binary data in file \n%s \n", backslash_to_slash(filename));
    else ret = 1;
    fclose (binfp);
    return (ret) ? -1 : nx;
}

int push_bin_float_dwn_error_z(O_p *op, char *filename, int offset, int nx, int axis)
{
    //register int i;
    char ch;
    int ret = 0;
    size_t len;
    float *array = NULL;
    FILE *binfp = NULL;
    d_s *ds = NULL;
    static char previous_file[512] ={0};
    static int first = 1;
    static long int cz = 0;

    if (op == NULL)  return 1;
    binfp = fopen (filename, "rb");
    if ( binfp == NULL )
        return error_in_file ("%s binary file not found!...  \n", backslash_to_slash(filename));
    //my_set_window_title("Reading %s offset %d size %d",backslash_to_slash(filename),offset,nx);
    if (first)
    {
        strncpy(previous_file, filename, sizeof(previous_file));
        first = 0;
    }
    len = strlen(filename);
    len = (len > strlen(previous_file)) ? strlen(previous_file) : len;
    if (cz == 0 || strncmp(previous_file, filename, len) != 0 )
    {
        strncpy(previous_file, filename, sizeof(previous_file));
        while (fread (&ch, sizeof (char), 1, binfp) == 1 && ch != CRT_Z);
        cz = ftell(binfp);
        fseek(binfp, offset, SEEK_CUR);
    }
    else fseek(binfp, cz + offset, SEEK_SET);
    if (op->n_dat == 0)   if (init_data_set(op)) return 1;
    ds = op->dat[op->n_dat - 1];

    if (axis == X_AXIS)
    {
        if (ds->xed != NULL) free(ds->xed);
        if ((alloc_data_set_x_down_error(ds) == NULL) || (ds->xed == NULL))
            return win_printf_OK("I can't create x down std. errors !");
        array = ds->xed;
    }
    else if (axis == Y_AXIS)
    {
        if (ds->yed != NULL) free(ds->yed);
        if ((alloc_data_set_y_down_error(ds) == NULL) || (ds->yed == NULL))
            return win_printf_OK("I can't create y down std. errors !");
        array = ds->yed;
    }

    if (array == NULL)
    {
        fclose(binfp);
        return  error_in_file ("could not create data set down error in binary input file \n%s \n", backslash_to_slash(filename));
    }

    if (fread (array, sizeof(float), nx, binfp) != (size_t) nx)
        error_in_file ("could not read x binary data in file \n%s \n", backslash_to_slash(filename));
    else ret = 1;
    fclose (binfp);
    return (ret) ? -1 : nx;
}



int push_bin_float_box_up_error_z(O_p *op, char *filename, int offset, int nx, int axis)
{
    //register int i;
    char ch;
    int ret = 0;
    size_t len;
    float *array = NULL;
    FILE *binfp = NULL;
    d_s *ds = NULL;
    static char previous_file[512] = {0};
    static int first = 1;
    static long int cz = 0;

    if (op == NULL)  return 1;
    binfp = fopen (filename, "rb");
    if ( binfp == NULL )
        return error_in_file ("%s binary file not found!...  \n", backslash_to_slash(filename));
    //my_set_window_title("Reading %s offset %d size %d",backslash_to_slash(filename),offset,nx);
    if (first)
    {
        strncpy(previous_file, filename, sizeof(previous_file));
        first = 0;
    }
    len = strlen(filename);
    len = (len > strlen(previous_file)) ? strlen(previous_file) : len;
    if (cz == 0 || strncmp(previous_file, filename, len) != 0 )
    {
        strncpy(previous_file, filename, sizeof(previous_file));
        while (fread (&ch, sizeof (char), 1, binfp) == 1 && ch != CRT_Z);
        cz = ftell(binfp);
        fseek(binfp, offset, SEEK_CUR);
    }
    else fseek(binfp, cz + offset, SEEK_SET);
    if (op->n_dat == 0)   if (init_data_set(op)) return 1;
    ds = op->dat[op->n_dat - 1];

    if (axis == X_AXIS)
    {
        if (ds->xbu != NULL) free(ds->xbu);
        if ((alloc_data_set_x_box_up(ds) == NULL) || (ds->xbu == NULL))
            return win_printf_OK("I can't create x box up errors !");
        array = ds->xbu;
    }
    else if (axis == Y_AXIS)
    {
        if (ds->ybu != NULL) free(ds->ybu);
        if ((alloc_data_set_y_box_up(ds) == NULL) || (ds->ybu == NULL))
            return win_printf_OK("I can't create y box up errors !");
        array = ds->ybu;
    }

    if (array == NULL)
    {
        fclose(binfp);
        return  error_in_file ("could not create data set error in binary input file \n%s \n", backslash_to_slash(filename));
    }

    if (fread (array, sizeof(float), nx, binfp) != (size_t) nx)
        error_in_file ("could not read x binary data in file \n%s \n", backslash_to_slash(filename));
    else ret = 1;
    fclose (binfp);
    return (ret) ? -1 : nx;
}


int push_bin_float_box_dwn_error_z(O_p *op, char *filename, int offset, int nx, int axis)
{
    //register int i;
    char ch;
    int ret = 0;
    size_t len;
    float *array = NULL;
    FILE *binfp = NULL;
    d_s *ds = NULL;
    static char previous_file[512] = {0};
    static int first = 1;
    static long int cz = 0;

    if (op == NULL)  return 1;
    binfp = fopen (filename, "rb");
    if ( binfp == NULL )
        return error_in_file ("%s binary file not found!...  \n", backslash_to_slash(filename));
    //my_set_window_title("Reading %s offset %d size %d",backslash_to_slash(filename),offset,nx);
    if (first)
    {
        strncpy(previous_file, filename, sizeof(previous_file));
        first = 0;
    }
    len = strlen(filename);
    len = (len > strlen(previous_file)) ? strlen(previous_file) : len;
    if (cz == 0 || strncmp(previous_file, filename, len) != 0 )
    {
        strncpy(previous_file, filename, sizeof(previous_file));
        while (fread (&ch, sizeof (char), 1, binfp) == 1 && ch != CRT_Z);
        cz = ftell(binfp);
        fseek(binfp, offset, SEEK_CUR);
    }
    else fseek(binfp, cz + offset, SEEK_SET);
    if (op->n_dat == 0)   if (init_data_set(op)) return 1;
    ds = op->dat[op->n_dat - 1];

    if (axis == X_AXIS)
    {
        if (ds->xbd != NULL) free(ds->xbd);
        if ((alloc_data_set_x_box_dwn(ds) == NULL) || (ds->xbd == NULL))
            return win_printf_OK("I can't create x box down errors!");
        array = ds->xbd;
    }
    else if (axis == Y_AXIS)
    {
        if (ds->ybd != NULL) free(ds->ybd);
        if ((alloc_data_set_y_box_dwn(ds) == NULL) || (ds->ybd == NULL))
            return win_printf_OK("I can't create y box down errors!");
        array = ds->ybd;
    }

    if (array == NULL)
    {
        fclose(binfp);
        return  error_in_file ("could not create data set error in binary input file \n%s \n", backslash_to_slash(filename));
    }

    if (fread (array, sizeof(float), nx, binfp) != (size_t) nx)
        error_in_file ("could not read x binary data in file \n%s \n", backslash_to_slash(filename));
    else ret = 1;
    fclose (binfp);
    return (ret) ? -1 : nx;
}




/*  push_bin_float_data(filename);
 *  DESCRIPTION
 *
 *
 *
 *  RETURNS     0 on success, 1
 *
 */
int push_bin_float_data(O_p *op, char *filename)
{
    register int i;
    int n_read, ntot = 0;
    float *tmp_y = NULL;
    FILE *binfp = NULL;

    if (op == NULL)  return 1;
    binfp = fopen (filename, "rb");
    if ( binfp == NULL )
        return error_in_file ("%s file not found!...  \n", filename);
    tmp_y = (float *)calloc(GR_SIZE, sizeof(float));
    if (tmp_y == NULL)
    {
        fprintf(stderr, "calloc error \n");
        exit(1);
    }
    do
    {
        n_read = fread ( tmp_y , sizeof (float),  GR_SIZE, binfp);
#ifdef XV_MAC
#ifdef MAC_POWERPC
        win_printf("%g - %g\n%g - %g", tmp_y[0], tmp_y[1], tmp_y[2], tmp_y[3]);
        swap_bytes(tmp_y, GR_SIZE, sizeof(float));
        win_printf("%g - %g\n%g - %g", tmp_y[0], tmp_y[1], tmp_y[2], tmp_y[3]);
#endif
#endif
        if ( absf == 0 )
        {
            for ( i = 0; i < n_read ; i++, i++)
            {

                push_new_data(op, tmp_y[i], tmp_y[i + 1]);
            }
        }
        else if ( absf == 1 || absf == 2 )
        {
            for ( i = 0; i < n_read ; i++)
            {


                push_new_data(op, counter * dx + abslow, tmp_y[i]);
                counter++;
            }
        }
        ntot += n_read;
    }
    while (n_read == GR_SIZE);
    if (tmp_y)   free(tmp_y);
    fclose (binfp);
    return ntot;
}
/*  push_bin_int_data(filename);
 *  DESCRIPTION
 *
 *
 *
 *  RETURNS     0 on success, 1
 *
 */
int push_bin_int_data(O_p *op, char *filename)
{
    register int i;
    int n_read, ntot = 0;
    short int *tmp_y = NULL;
    FILE *binfp = NULL;

    if (op == NULL)  return 1;
    binfp = fopen (filename, "rb");
    if ( binfp == NULL )
        return error_in_file ("%s file not found!...  \n", filename);
    tmp_y = (short int *)calloc(GR_SIZE, sizeof(int));
    if (tmp_y == NULL)
    {
        fprintf(stderr, "calloc error \n");
        exit(1);
    }
    do
    {
        n_read = fread ( tmp_y , sizeof (short int),  GR_SIZE, binfp);
#ifdef XV_MAC
#ifdef MAC_POWERPC
        win_printf("%g - %g\n%g - %g", tmp_y[0], tmp_y[1], tmp_y[2], tmp_y[3]);
        swap_bytes(tmp_y, GR_SIZE, sizeof(short int));
        win_printf("%g - %g\n%g - %g", tmp_y[0], tmp_y[1], tmp_y[2], tmp_y[3]);
#endif
#endif
        if ( absf == 0 )
        {
            for ( i = 0; i < n_read ; i++, i++)
                push_new_data(op, (float)tmp_y[i], (float)tmp_y[i + 1]);
        }
        else if ( absf == 1 || absf == 2 )
        {
            for ( i = 0; i < n_read ; i++)
            {
                push_new_data(op, counter * dx + abslow, (float)tmp_y[i]);
                counter++;
            }
        }
        ntot += n_read;
    }
    while (n_read == GR_SIZE);
    if (tmp_y)  free(tmp_y);
    fclose (binfp);
    return ntot;
}

/*  push_plot_label(tmpx,tmpy,c2);
 *  DESCRIPTION
 *
 *
 *
 *  RETURNS     0 on success, 1
 *
 */
int push_plot_label(O_p *op, float tx, float ty, char *label, int type)
{
    p_l *pl = NULL;

    if (op == NULL || label == NULL)  return 1;
    pl = (p_l *)calloc(1, sizeof(struct plot_label));
    if ( pl == NULL )
    {
        fprintf (stderr, "realloc error \n");
        exit(1);
    }
    pl->xla = tx;
    pl->yla = ty;
    pl->text = (char *)strdup(label);
    pl->type = type;
    if (pl->text == NULL)
    {
        fprintf(stderr, "strdup error \n");
        exit(1);
    }
    return    add_one_plot_data (op, IS_PLOT_LABEL, (void *)pl);
}

/**
 * @brief Save a plot to file
 *
 * @param op the plot to save
 * @param ifile full file name, can't be NULL
 *
 * @return 0 on success.
 */
int save_one_plot(O_p *op, const char *ifile)
{
    int i, j;
    un_s *un = NULL;
    int n_min, x_auto = 1;
    d_s *ds = NULL;
    FILE *fp = NULL;
    time_t timer;
    struct plot_label *pl = NULL;
    char file[512] = {0};

    if (op == NULL || file == NULL)   return -1;
    //if (op == NULL) return -1;

    strncpy(file, ifile, sizeof(file));

    get_os_specific_path(file);
    fp = fopen (file, "wb");
    if ( fp == NULL )     return win_printf("Cannot open %s!", backslash_to_slash(file));
    if (ds_use_2_special != NULL)
    {
        for (i = 0 ; i < op->n_dat ; i++)
            ds_use_2_special(op->dat[i]);
    }
    fprintf (fp, "%% plot data \n");

    if (op->title != NULL)     fprintf (fp, "-lt \"%s\"\n", op->title);
    if (op->x_title != NULL) fprintf (fp, "-lx \"%s\"\n", op->x_title);
    if (op->y_title != NULL) fprintf (fp, "-ly \"%s\"\n", op->y_title);
    if (op->x_prime_title != NULL) fprintf (fp, "-lxp \"%s\"\n", op->x_prime_title);
    if (op->y_prime_title != NULL) fprintf (fp, "-lyp \"%s\"\n", op->y_prime_title);
    if (op->c_xu_p >= 0 && op->c_xu_p < op->n_xu)
    {
        fprintf(fp, "-axp %d\n", op->c_xu_p);
    }
    if (op->c_yu_p >= 0 && op->c_yu_p < op->n_yu)
    {
        fprintf(fp, "-ayp %d\n", op->c_yu_p);
    }


    if (op->x_prefix != NULL && op->x_unit != NULL)
        fprintf (fp, "-px \"%s\" \"%s\"\n", op->x_prefix, op->x_unit);
    else if (op->x_prefix != NULL && op->x_unit == NULL)
        fprintf (fp, "-px \"%s\" !\n", op->x_prefix);
    else if (op->x_prefix == NULL && op->x_unit != NULL)
        fprintf (fp, "-px ! \"%s\"\n", op->x_unit);
    if (op->y_prefix != NULL && op->y_unit != NULL)
        fprintf (fp, "-py \"%s\" \"%s\"\n", op->y_prefix, op->y_unit);
    else if (op->y_prefix != NULL && op->y_unit == NULL)
        fprintf (fp, "-py \"%s\" !\n", op->y_prefix);
    else if (op->y_prefix == NULL && op->y_unit != NULL)
        fprintf (fp, "-py ! \"%s\"\n", op->y_unit);
    if (op->dx != 1 || op->ax != 0) fprintf(fp, "-ax %g %g\n", op->dx, op->ax);
    if (op->dy != 1 || op->ay != 0) fprintf(fp, "-ay %g %g\n", op->dy, op->ay);

    if (op->xu != NULL)
    {
        for (i = 0 ; i < op->n_xu ; i++)
        {
            un = op->xu[i];
            if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
                fprintf(fp, "-xus %g %g \"%s\" %d %d %d\n", un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
            else if (un != NULL && un->type != IS_RAW_U)
                fprintf(fp, "-xus %g %g \"%s\" %d %d %d\n", un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
        }
    }
    if (op->yu != NULL)
    {
        for (i = 0 ; i < op->n_yu ; i++)
        {
            un = op->yu[i];
            if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
                fprintf(fp, "-yus %g %g \"%s\" %d %d %d\n", un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
            else if (un != NULL && un->type != IS_RAW_U)
                fprintf(fp, "-yus %g %g \"%s\" %d %d %d\n", un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
        }
    }
    if (op->tu != NULL)
    {
        for (i = 0 ; i < op->n_tu ; i++)
        {
            un = op->tu[i];
            if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
                fprintf(fp, "-tus %g %g \"%s\" %d %d %d\n", un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
            else if (un != NULL && un->type != IS_RAW_U)
                fprintf(fp, "-tus %g %g \"%s\" %d %d %d\n", un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
        }
    }
    if ( op->width != 1)  fprintf (fp, "-w %g\n", op->width);
    if ( op->height != 1)     fprintf (fp, "-h %g\n", op->height);
    if ( op->right != 0)  fprintf (fp, "-r %g\n", op->right);
    if ( op->up != 0)     fprintf (fp, "-u %g\n", op->up);
    if ( op->tick_len != 1)   fprintf (fp, "-tk %g\n", op->tick_len);
    if ( op->tick_len_x != 1) fprintf (fp, "-tkx %g\n", op->tick_len_x);
    if ( op->tick_len_y != 1) fprintf (fp, "-tky %g\n", op->tick_len_y);
    if ( op->iopt & NOAXES)   fprintf (fp, "-g 0\n");
    else if ( op->iopt & AXES_PRIME)  fprintf (fp, "-g 1\n");

    fprintf(fp, "-x%c %c %g %g\n", (op->iopt2 & X_NUM) ? ' ' : 'n', (op->iopt & XLOG) ? 'l' : ' ', op->x_lo, op->x_hi);
    fprintf(fp, "-y%c %c %g %g\n", (op->iopt2 & Y_NUM) ? ' ' : 'n', (op->iopt & YLOG) ? 'l' : ' ', op->y_lo, op->y_hi);
    /*
      if ( op->iopt & XLOG)   fprintf(fp, "-x l %g %g\n",op->x_lo,op->x_hi);
      else fprintf(fp, "-x %g %g\n",op->x_lo,op->x_hi);
      if ( op->iopt & YLOG)   fprintf(fp, "-y l %g %g\n",op->y_lo,op->y_hi);
      else fprintf(fp, "-y %g %g\n",op->y_lo,op->y_hi);
    */

    for (i = 0 ; i < op->n_dat ; i++)
    {
        fprintf (fp, "\n");
        ds = op->dat[i];
        fprintf(fp, "-m %d\n", ds->m);
        if (ds->symb != NULL)
            fprintf(fp, "-c \"%s\"\n", ds->symb);

        if (ds->color == Black)               fprintf(fp, "-color \"black\"\n");
        else if (ds->color == Blue)           fprintf(fp, "-color \"blue\"\n");
        else if (ds->color == Green)      fprintf(fp, "-color \"green\"\n");
        else if (ds->color == Cyan)           fprintf(fp, "-color \"cyan\"\n");
        else if (ds->color == Red)            fprintf(fp, "-color \"red\"\n");
        else if (ds->color == Magenta)        fprintf(fp, "-color \"magenta\"\n");
        else if (ds->color == Brown)      fprintf(fp, "-color \"brown\"\n");
        else if (ds->color == Lightgray)  fprintf(fp, "-color \"lightgray\"\n");
        else if (ds->color == Darkgray)       fprintf(fp, "-color \"darkgray\"\n");
        else if (ds->color == Lightblue)  fprintf(fp, "-color \"lightblue\"\n");
        else if (ds->color == Lightgreen) fprintf(fp, "-color \"lightgreen\"\n");
        else if (ds->color == Lightcyan)  fprintf(fp, "-color \"lightcyan\"\n");
        else if (ds->color == Lightred)       fprintf(fp, "-color \"lightred\"\n");
        else if (ds->color == Lightmagenta)   fprintf(fp, "-color \"lightmagenta\"\n");
        else if (ds->color == Yellow)     fprintf(fp, "-color \"yellow\"\n");
        else if (ds->color == White)      fprintf(fp, "-color \"white\"\n");

        if (ds->source != NULL)   fprintf(fp, "-src \"%s\"\n", ds->source);
        if (ds->history != NULL)  fprintf(fp, "-his \"%s\"\n", ds->history);
        if (ds->treatement != NULL)   fprintf(fp, "-treat \"%s\"\n", ds->treatement);
        if (ds->time == 0)    time(&timer);
        else          timer = ds->time;
        fprintf(fp, "-time %lu %% %s\n", timer, ctime(&timer));
        if (ds->special != NULL)
        {
            for (j = 0 ; j < ds->n_special ; j++)
            {
                if (ds->special[j] != NULL)
                    fprintf(fp, "-special \"%s\"\n", ds->special[j]);
            }
        }
        for (j = 0 ; j < ds->n_lab ; j++)
        {
            pl = ds->lab[j];
            if ( pl->type == USR_COORD)
                fprintf (fp, "%g %g \"%s\"\n", pl->xla, pl->yla, pl->text);
            else if ( pl->type == ABS_COORD)
                fprintf (fp, "-lr %g %g \"%s\"\n", pl->xla, pl->yla, pl->text);
            else if ( pl->type == VERT_LABEL_USR)
                fprintf (fp, "-lay %g %g \"%s\"\n", pl->xla, pl->yla, pl->text);
            else if ( pl->type == VERT_LABEL_ABS)
                fprintf (fp, "-layr %g %g \"%s\"\n", pl->xla, pl->yla, pl->text);
        }

        n_min = ( ds->nx < ds->ny) ? ds->nx : ds->ny;
        for (j = 0, x_auto = 1; j < n_min && x_auto; j++)
            x_auto = (ds->xd[j] == (float)j) ? 1 : 0;
        fprintf(fp, "-a%c\n", (x_auto) ? ' ' : '!');

        for (j = 0 ; j < n_min ; j++)
        {
            if (x_auto)    fprintf (fp, "%g\n", ds->yd[j]);
            else          fprintf (fp, "%g %g\n", ds->xd[j], ds->yd[j]);
        }
        if (ds->xe != NULL)
        {

            fprintf (fp, "\n");
            ds = op->dat[i];
            fprintf(fp, "-m %d\n", ds->m);
            if (ds->symb != NULL)
                fprintf(fp, "-c \"%s\"\n", ds->symb);

            if (ds->color == Black)               fprintf(fp, "-color \"black\"\n");
            else if (ds->color == Blue)           fprintf(fp, "-color \"blue\"\n");
            else if (ds->color == Green)      fprintf(fp, "-color \"green\"\n");
            else if (ds->color == Cyan)           fprintf(fp, "-color \"cyan\"\n");
            else if (ds->color == Red)            fprintf(fp, "-color \"red\"\n");
            else if (ds->color == Magenta)        fprintf(fp, "-color \"magenta\"\n");
            else if (ds->color == Brown)      fprintf(fp, "-color \"brown\"\n");
            else if (ds->color == Lightgray)  fprintf(fp, "-color \"lightgray\"\n");
            else if (ds->color == Darkgray)       fprintf(fp, "-color \"darkgray\"\n");
            else if (ds->color == Lightblue)  fprintf(fp, "-color \"lightblue\"\n");
            else if (ds->color == Lightgreen) fprintf(fp, "-color \"lightgreen\"\n");
            else if (ds->color == Lightcyan)  fprintf(fp, "-color \"lightcyan\"\n");
            else if (ds->color == Lightred)       fprintf(fp, "-color \"lightred\"\n");
            else if (ds->color == Lightmagenta)   fprintf(fp, "-color \"lightmagenta\"\n");
            else if (ds->color == Yellow)     fprintf(fp, "-color \"yellow\"\n");
            else if (ds->color == White)      fprintf(fp, "-color \"white\"\n");

            if (ds->source != NULL)   fprintf(fp, "-src \"error bars in X of %s\"\n", ds->source);
            if (ds->history != NULL)  fprintf(fp, "-his \"%s\"\n", ds->history);
            if (ds->treatement != NULL)   fprintf(fp, "-treat \"%s\"\n", ds->treatement);
            if (ds->time == 0)    time(&timer);
            else          timer = ds->time;
            fprintf(fp, "-time %lu %% %s\n", timer, ctime(&timer));
            if (ds->special != NULL)
            {
                for (j = 0 ; j < ds->n_special ; j++)
                {
                    if (ds->special[j] != NULL)
                        fprintf(fp, "-special \"%s\"\n", ds->special[j]);
                }
            }

            n_min = ( ds->nx < ds->ny) ? ds->nx : ds->ny;
            for (j = 0, x_auto = 1; j < n_min && x_auto; j++)
                x_auto = (ds->xd[j] == (float)j) ? 1 : 0;
            fprintf(fp, "-a%c\n", (x_auto) ? ' ' : '!');

            for (j = 0 ; j < n_min ; j++)
            {
                if (x_auto)    fprintf (fp, "%g\n", ds->yd[j]);
                else          fprintf (fp, "%g %g\n", ds->xe[j], ds->yd[j]);
            }

        }
        if (ds->ye != NULL)
        {

            fprintf (fp, "\n");
            ds = op->dat[i];
            fprintf(fp, "-m %d\n", ds->m);
            if (ds->symb != NULL)
                fprintf(fp, "-c \"%s\"\n", ds->symb);

            if (ds->color == Black)               fprintf(fp, "-color \"black\"\n");
            else if (ds->color == Blue)           fprintf(fp, "-color \"blue\"\n");
            else if (ds->color == Green)      fprintf(fp, "-color \"green\"\n");
            else if (ds->color == Cyan)           fprintf(fp, "-color \"cyan\"\n");
            else if (ds->color == Red)            fprintf(fp, "-color \"red\"\n");
            else if (ds->color == Magenta)        fprintf(fp, "-color \"magenta\"\n");
            else if (ds->color == Brown)      fprintf(fp, "-color \"brown\"\n");
            else if (ds->color == Lightgray)  fprintf(fp, "-color \"lightgray\"\n");
            else if (ds->color == Darkgray)       fprintf(fp, "-color \"darkgray\"\n");
            else if (ds->color == Lightblue)  fprintf(fp, "-color \"lightblue\"\n");
            else if (ds->color == Lightgreen) fprintf(fp, "-color \"lightgreen\"\n");
            else if (ds->color == Lightcyan)  fprintf(fp, "-color \"lightcyan\"\n");
            else if (ds->color == Lightred)       fprintf(fp, "-color \"lightred\"\n");
            else if (ds->color == Lightmagenta)   fprintf(fp, "-color \"lightmagenta\"\n");
            else if (ds->color == Yellow)     fprintf(fp, "-color \"yellow\"\n");
            else if (ds->color == White)      fprintf(fp, "-color \"white\"\n");

            if (ds->source != NULL)   fprintf(fp, "-src \"error bars in Y of %s\"\n", ds->source);
            if (ds->history != NULL)  fprintf(fp, "-his \"%s\"\n", ds->history);
            if (ds->treatement != NULL)   fprintf(fp, "-treat \"%s\"\n", ds->treatement);
            if (ds->time == 0)    time(&timer);
            else          timer = ds->time;
            fprintf(fp, "-time %lu %% %s\n", timer, ctime(&timer));
            if (ds->special != NULL)
            {
                for (j = 0 ; j < ds->n_special ; j++)
                {
                    if (ds->special[j] != NULL)
                        fprintf(fp, "-special \"%s\"\n", ds->special[j]);
                }
            }

            n_min = ( ds->nx < ds->ny) ? ds->nx : ds->ny;
            for (j = 0, x_auto = 1; j < n_min && x_auto; j++)
                x_auto = (ds->xd[j] == (float)j) ? 1 : 0;
            fprintf(fp, "-a%c\n", (x_auto) ? ' ' : '!');

            for (j = 0 ; j < n_min ; j++)
            {
                if (x_auto)    fprintf (fp, "%g\n", ds->ye[j]);
                else          fprintf (fp, "%g %g\n", ds->xd[j], ds->ye[j]);
            }

        }

    }
    for (i = 0 ; i < op->n_lab ; i++)
    {
        if ( op->lab[i]->type == USR_COORD)
            fprintf (fp, "%g %g \"%s\"\n", op->lab[i]->xla, op->lab[i]->yla, op->lab[i]->text);
        else if ( op->lab[i]->type == ABS_COORD)
            fprintf (fp, "-lr %g %g \"%s\"\n", op->lab[i]->xla, op->lab[i]->yla, op->lab[i]->text);
        else if ( op->lab[i]->type == VERT_LABEL_USR)
            fprintf (fp, "-lay %g %g \"%s\"\n", op->lab[i]->xla, op->lab[i]->yla, op->lab[i]->text);
        else if ( op->lab[i]->type == VERT_LABEL_ABS)
            fprintf (fp, "-layr %g %g \"%s\"\n", op->lab[i]->xla, op->lab[i]->yla, op->lab[i]->text);
    }
    fclose(fp);
    return 0;
}



int save_one_plot_bin(O_p *op, char *file)
{
    register int i, j;
    un_s *un = NULL;
    size_t n_min;
    int  offset = 0, x_auto = 1;
    d_s *ds = NULL;
    FILE *fp = NULL;
    time_t timer;
    struct plot_label *pl = NULL;
    float *data = NULL; // the binary data to be saved

    if (op == NULL || file == NULL)   return -1;


    fp = fopen (file, "wb");
    if ( fp == NULL )     return win_printf("Cannot open %s!", backslash_to_slash(file));
    if (ds_use_2_special != NULL)
    {
        for (i = 0 ; i < op->n_dat ; i++)
            ds_use_2_special(op->dat[i]);
    }
    fprintf (fp, "%% plot data \n");

    if (op->title != NULL)     fprintf (fp, "-lt \"%s\"\n", op->title);
    if (op->x_title != NULL) fprintf (fp, "-lx \"%s\"\n", op->x_title);
    if (op->y_title != NULL) fprintf (fp, "-ly \"%s\"\n", op->y_title);
    if (op->x_prime_title != NULL) fprintf (fp, "-lxp \"%s\"\n", op->x_prime_title);
    if (op->y_prime_title != NULL) fprintf (fp, "-lyp \"%s\"\n", op->y_prime_title);

    if (op->c_xu_p >= 0 && op->c_xu_p < op->n_xu)
    {
        fprintf(fp, "-axp %d\n", op->c_xu_p);
    }
    if (op->c_yu_p >= 0 && op->c_yu_p < op->n_yu)
    {
        fprintf(fp, "-ayp %d\n", op->c_yu_p);
    }

    if (op->x_prefix != NULL && op->x_unit != NULL)
        fprintf (fp, "-px \"%s\" \"%s\"\n", op->x_prefix, op->x_unit);
    else if (op->x_prefix != NULL && op->x_unit == NULL)
        fprintf (fp, "-px \"%s\" !\n", op->x_prefix);
    else if (op->x_prefix == NULL && op->x_unit != NULL)
        fprintf (fp, "-px ! \"%s\"\n", op->x_unit);
    if (op->y_prefix != NULL && op->y_unit != NULL)
        fprintf (fp, "-py \"%s\" \"%s\"\n", op->y_prefix, op->y_unit);
    else if (op->y_prefix != NULL && op->y_unit == NULL)
        fprintf (fp, "-py \"%s\" !\n", op->y_prefix);
    else if (op->y_prefix == NULL && op->y_unit != NULL)
        fprintf (fp, "-py ! \"%s\"\n", op->y_unit);
    if (op->dx != 1 || op->ax != 0) fprintf(fp, "-ax %g %g\n", op->dx, op->ax);
    if (op->dy != 1 || op->ay != 0) fprintf(fp, "-ay %g %g\n", op->dy, op->ay);

    if (op->xu != NULL)
    {
        for (i = 0 ; i < op->n_xu ; i++)
        {
            un = op->xu[i];
            if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
                fprintf(fp, "-xus %g %g \"%s\" %d %d %d\n", un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
            else if (un != NULL && un->type != IS_RAW_U)
                fprintf(fp, "-xus %g %g \"%s\" %d %d %d\n", un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
        }
    }
    if (op->yu != NULL)
    {
        for (i = 0 ; i < op->n_yu ; i++)
        {
            un = op->yu[i];
            if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
                fprintf(fp, "-yus %g %g \"%s\" %d %d %d\n", un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
            else if (un != NULL && un->type != IS_RAW_U)
                fprintf(fp, "-yus %g %g \"%s\" %d %d %d\n", un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
        }
    }
    if (op->tu != NULL)
    {
        for (i = 0 ; i < op->n_tu ; i++)
        {
            un = op->tu[i];
            if (un != NULL && un->type != IS_RAW_U && un->name != NULL)
                fprintf(fp, "-tus %g %g \"%s\" %d %d %d\n", un->dx, un->ax, un->name, un->type, un->decade, un->sub_type);
            else if (un != NULL && un->type != IS_RAW_U)
                fprintf(fp, "-tus %g %g \"%s\" %d %d %d\n", un->dx, un->ax, "no_name", un->type, un->decade, un->sub_type);
        }
    }
    if ( op->width != 1)  fprintf (fp, "-w %g\n", op->width);
    if ( op->height != 1)     fprintf (fp, "-h %g\n", op->height);
    if ( op->right != 0)  fprintf (fp, "-r %g\n", op->right);
    if ( op->up != 0)     fprintf (fp, "-u %g\n", op->up);
    if ( op->tick_len != 1)   fprintf (fp, "-tk %g\n", op->tick_len);
    if ( op->tick_len_x != 1) fprintf (fp, "-tkx %g\n", op->tick_len_x);
    if ( op->tick_len_y != 1) fprintf (fp, "-tky %g\n", op->tick_len_y);
    if ( op->iopt & NOAXES)   fprintf (fp, "-g 0\n");
    else if ( op->iopt & AXES_PRIME)  fprintf (fp, "-g 1\n");
    fprintf(fp, "-x%c %c %g %g\n", (op->iopt2 & X_NUM) ? ' ' : 'n', (op->iopt & XLOG) ? 'l' : ' ', op->x_lo, op->x_hi);
    fprintf(fp, "-y%c %c %g %g\n", (op->iopt2 & Y_NUM) ? ' ' : 'n', (op->iopt & YLOG) ? 'l' : ' ', op->y_lo, op->y_hi);
    /*
    if ( op->iopt & XLOG) fprintf(fp, "-x l %g %g\n",op->x_lo,op->x_hi);
    else fprintf(fp, "-x %g %g\n",op->x_lo,op->x_hi);
    if ( op->iopt & YLOG) fprintf(fp, "-y l %g %g\n",op->y_lo,op->y_hi);
    else fprintf(fp, "-y %g %g\n",op->y_lo,op->y_hi);
    */
    for (i = 0 ; i < op->n_dat ; i++)
    {
        fprintf (fp, "\n");
        ds = op->dat[i];
        fprintf(fp, "-m %d\n", ds->m);
        if (ds->symb != NULL)
            fprintf(fp, "-c \"%s\"\n", ds->symb);
        if (ds->color == Black)               fprintf(fp, "-color \"black\"\n");
        else if (ds->color == Blue)           fprintf(fp, "-color \"blue\"\n");
        else if (ds->color == Green)      fprintf(fp, "-color \"green\"\n");
        else if (ds->color == Cyan)           fprintf(fp, "-color \"cyan\"\n");
        else if (ds->color == Red)            fprintf(fp, "-color \"red\"\n");
        else if (ds->color == Magenta)        fprintf(fp, "-color \"magenta\"\n");
        else if (ds->color == Brown)      fprintf(fp, "-color \"brown\"\n");
        else if (ds->color == Lightgray)  fprintf(fp, "-color \"lightgray\"\n");
        else if (ds->color == Darkgray)       fprintf(fp, "-color \"darkgray\"\n");
        else if (ds->color == Lightblue)  fprintf(fp, "-color \"lightblue\"\n");
        else if (ds->color == Lightgreen) fprintf(fp, "-color \"lightgreen\"\n");
        else if (ds->color == Lightcyan)  fprintf(fp, "-color \"lightcyan\"\n");
        else if (ds->color == Lightred)       fprintf(fp, "-color \"lightred\"\n");
        else if (ds->color == Lightmagenta)   fprintf(fp, "-color \"lightmagenta\"\n");
        else if (ds->color == Yellow)     fprintf(fp, "-color \"yellow\"\n");
        else if (ds->color == White)      fprintf(fp, "-color \"white\"\n");

        if (ds->source != NULL)   fprintf(fp, "-src \"%s\"\n", ds->source);
        if (ds->history != NULL)  fprintf(fp, "-his \"%s\"\n", ds->history);
        if (ds->treatement != NULL)   fprintf(fp, "-treat \"%s\"\n", ds->treatement);
        if (ds->time == 0)    time(&timer);
        else          timer = ds->time;
        fprintf(fp, "-time %lu %% %s\n", timer, ctime(&timer));
        if (ds->special != NULL)
        {
            for (j = 0 ; j < ds->n_special ; j++)
            {
                if (ds->special[j] != NULL)
                    fprintf(fp, "-special \"%s\"\n", ds->special[j]);
            }
        }
        for (j = 0 ; j < ds->n_lab ; j++)
        {
            pl = ds->lab[j];
            if ( pl->type == USR_COORD)
                fprintf (fp, "%g %g \"%s\"\n", pl->xla, pl->yla, pl->text);
            else if ( pl->type == ABS_COORD)
                fprintf (fp, "-lr %g %g \"%s\"\n", pl->xla, pl->yla, pl->text);
            else if ( pl->type == VERT_LABEL_USR)
                fprintf (fp, "-lay %g %g \"%s\"\n", pl->xla, pl->yla, pl->text);
            else if ( pl->type == VERT_LABEL_ABS)
                fprintf (fp, "-layr %g %g \"%s\"\n", pl->xla, pl->yla, pl->text);
        }

        n_min = ( ds->nx < ds->ny) ? (size_t)ds->nx : (size_t)ds->ny;
        for (j = 0, x_auto = 1; j < (int)n_min && x_auto; j++)
            x_auto = (ds->xd[j] == (float)j) ? 1 : 0;
        fprintf(fp, "-a%c\n", (x_auto) ? ' ' : '!');
        fprintf (fp, "-ibfz %d %d\n", offset, (int) n_min);
        offset += ((x_auto) ? 1 : 2) * sizeof(float) * n_min;
        if (ds->xe != NULL)
        {
            fprintf (fp, "-exbfz %d %d\n", offset, (int) n_min);
            offset += sizeof(float) * n_min;
        }
        if (ds->xed != NULL)
        {
            fprintf (fp, "-edxbfz %d %d\n", offset, (int) n_min);
            offset += sizeof(float) * n_min;
        }
        if (ds->xbu != NULL)
        {
            fprintf (fp, "-ebuxbfz %d %d\n", offset, (int) n_min);
            offset += sizeof(float) * n_min;
        }
        if (ds->xbd != NULL)
        {
            fprintf (fp, "-ebdxbfz %d %d\n", offset, (int) n_min);
            offset += sizeof(float) * n_min;
        }		
        if (ds->ye != NULL)
        {
            fprintf (fp, "-eybfz %d %d\n", offset, (int) n_min);
            offset += sizeof(float) * n_min;
        }
        if (ds->yed != NULL)
        {
            fprintf (fp, "-edybfz %d %d\n", offset, (int) n_min);
            offset += sizeof(float) * n_min;
        }
        if (ds->ybu != NULL)
        {
            fprintf (fp, "-ebuybfz %d %d\n", offset, (int) n_min);
            offset += sizeof(float) * n_min;
        }
        if (ds->ybd != NULL)
        {
            fprintf (fp, "-ebdybfz %d %d\n", offset, (int) n_min);
            offset += sizeof(float) * n_min;
        }	

    }
    for (i = 0 ; i < op->n_lab ; i++)
    {
        if ( op->lab[i]->type == USR_COORD)
            fprintf (fp, "%g %g \"%s\"\n", op->lab[i]->xla, op->lab[i]->yla, op->lab[i]->text);
        else if ( op->lab[i]->type == ABS_COORD)
            fprintf (fp, "-lr %g %g \"%s\"\n", op->lab[i]->xla, op->lab[i]->yla, op->lab[i]->text);
        else if ( op->lab[i]->type == VERT_LABEL_USR)
            fprintf (fp, "-lay %g %g \"%s\"\n", op->lab[i]->xla, op->lab[i]->yla, op->lab[i]->text);
        else if ( op->lab[i]->type == VERT_LABEL_ABS)
            fprintf (fp, "-layr %g %g \"%s\"\n", op->lab[i]->xla, op->lab[i]->yla, op->lab[i]->text);
    }
    fprintf (fp, "%c", 26);
    for (i = 0 ; i < op->n_dat ; i++)
    {
        ds = op->dat[i];
        n_min = ( ds->nx < ds->ny) ? (size_t)ds->nx : (size_t)ds->ny;
        for (j = 0, x_auto = 1; j < (int) n_min && x_auto; j++)
            x_auto = (ds->xd[j] == (float)j) ? 1 : 0;

        data = (float *)calloc(n_min, sizeof(float));
        if (!x_auto)
        {
            memcpy(data, ds->xd, n_min * sizeof(float));

#ifdef XV_MAC
#ifdef MAC_POWERPC
            swap_bytes(data, n_min, sizeof(float));
#endif
#endif
            if (fwrite(data, sizeof(float), n_min, fp) != n_min)
                win_printf("error writing binary data in \n%s", backslash_to_slash(file));
        }

        memcpy(data, ds->yd, n_min * sizeof(float));
#ifdef XV_MAC
#ifdef MAC_POWERPC
        swap_bytes(data, n_min, sizeof(float));
#endif
#endif
        if (fwrite(data, sizeof(float), n_min, fp) != n_min)
            win_printf("error writing binary data in \n%s", backslash_to_slash(file));

        if (ds->xe != NULL)
        {
            memcpy(data, ds->xe, n_min * sizeof(float));
#ifdef XV_MAC
#ifdef MAC_POWERPC
            swap_bytes(data, n_min, sizeof(float));
#endif
#endif
            if (fwrite(data, sizeof(float), n_min, fp) != n_min)
                win_printf("error writing binary data in \n%s", backslash_to_slash(file));
        }
        if (ds->xed != NULL)
        {
            memcpy(data, ds->xed, n_min * sizeof(float));
#ifdef XV_MAC
#ifdef MAC_POWERPC
            swap_bytes(data, n_min, sizeof(float));
#endif
#endif
            if (fwrite(data, sizeof(float), n_min, fp) != n_min)
                win_printf("error writing binary data in \n%s", backslash_to_slash(file));
        }
        if (ds->xbu != NULL)
        {
            memcpy(data, ds->xbu, n_min * sizeof(float));
#ifdef XV_MAC
#ifdef MAC_POWERPC
            swap_bytes(data, n_min, sizeof(float));
#endif
#endif
            if (fwrite(data, sizeof(float), n_min, fp) != n_min)
                win_printf("error writing binary data in \n%s", backslash_to_slash(file));
        }
        if (ds->xbd != NULL)
        {
            memcpy(data, ds->xbd, n_min * sizeof(float));
#ifdef XV_MAC
#ifdef MAC_POWERPC
            swap_bytes(data, n_min, sizeof(float));
#endif
#endif
            if (fwrite(data, sizeof(float), n_min, fp) != n_min)
                win_printf("error writing binary data in \n%s", backslash_to_slash(file));
        }	
        if (ds->ye != NULL)
        {
            memcpy(data, ds->ye, n_min * sizeof(float));
#ifdef XV_MAC
#ifdef MAC_POWERPC
            swap_bytes(data, n_min, sizeof(float));
#endif
#endif
            if (fwrite(data, sizeof(float), n_min, fp) != n_min)
                win_printf("error writing binary data in \n%s", backslash_to_slash(file));
        }
        if (ds->yed != NULL)
        {
            memcpy(data, ds->yed, n_min * sizeof(float));
#ifdef XV_MAC
#ifdef MAC_POWERPC
            swap_bytes(data, n_min, sizeof(float));
#endif
#endif
            if (fwrite(data, sizeof(float), n_min, fp) != n_min)
                win_printf("error writing binary data in \n%s", backslash_to_slash(file));
        }
        if (ds->ybu != NULL)
        {
            memcpy(data, ds->ybu, n_min * sizeof(float));
#ifdef XV_MAC
#ifdef MAC_POWERPC
            swap_bytes(data, n_min, sizeof(float));
#endif
#endif
            if (fwrite(data, sizeof(float), n_min, fp) != n_min)
                win_printf("error writing binary data in \n%s", backslash_to_slash(file));
        }
        if (ds->ybd != NULL)
        {
            memcpy(data, ds->ybd, n_min * sizeof(float));
#ifdef XV_MAC
#ifdef MAC_POWERPC
            swap_bytes(data, n_min, sizeof(float));
#endif
#endif
            if (fwrite(data, sizeof(float), n_min, fp) != n_min)
                win_printf("error writing binary data in \n%s", backslash_to_slash(file));
        }
	

    }
    fclose(fp);
    return 0;
}



# endif



