/*
 *    Example program for the Allegro library, by Shawn Hargreaves.
 *
 *    This program demonstrates how to use an offscree part of the
 *    video memory to store source graphics for a hardware accelerated
 *    graphics driver.
 get_desktop_resolution(int *width, int *height);
 */

# ifndef _XV_MAIN_C_
# define _XV_MAIN_C_


#include "platform.h"


#ifdef XV_MAC
#include "CoreFoundation/CFBundle.h" // to access bundle info, like path and resources...
#endif

#ifdef BUILDING_DLL
#include "xv_plugins.h"
#endif

#include "xvin.h"
#include "plot_op.h"
#include "gr_file.h"
#include "dev_pcx.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "xv_main.h"
# include "im_mn.h"
# include "plot_mn.h"
# include "plot_reg_gui.h"
# include "imr_reg_gui.h"
# include "im_gr.h"
# include "gui_def.h"
# include "xv_main.h"
#include "fortify_mn.h"
#include <locale.h>
# include <stdlib.h>
# include <string.h>

# ifdef USE_OPENMP
#include <omp.h>
# endif
#include "allegro.h"

#ifdef XV_WIN32
#include "winalleg.h"
#endif
#ifdef XV_UNIX
#include <xalleg.h>
#endif



extern int ttf_enable;
extern BITMAP *dev_bitmap;
extern char *debug_ttf;

// following has been commented out (NG, 2006-03-14) useless, defined in xv_plugins.h
// typedef int (*PL_MAIN)(int, char**);

/* we need to load fonts.dat to access the big font */
extern BITMAP *plt_buffer;
extern int plt_buffer_used;
extern int screen_used;

extern char prog_dir[PATH_MAX];
extern char first_data_dir[PATH_MAX];

extern char config_dir[PATH_MAX]; // where are config file xvin.cfg and log files
// on Linux and MacOS, this directory is not where the application is.
// (it can be user dependent)
//#ifdef XV_WIN32
//  snprintf(config_dir,512,"%s",prog_dir);
//#endif

extern int  win_color_depth;
extern int  win_screen_w;
extern int  win_screen_h;

extern int eps_file_flag;

extern FONT *small_font;
extern FONT *normalsize_font;
extern FONT *large_font;


//pltreg *find_pr_in_current_dialog(DIALOG *di);
//extern pltreg *project[];
//extern int    project_cur, project_n, project_m;
//
//extern int (*general_idle_action)(DIALOG *d);
//extern int (*general_end_action)(DIALOG *d);


int    generate_plot_dialog_xvin(void)
{
    DIALOG *di = NULL;
    di = attach_new_item_to_dialog(d_clear_proc, 0, 0, 0, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    set_dialog_size_and_color(di, 0, 0, 900, 700, 0, 0);
    di = attach_new_item_to_dialog(xvin_d_menu_proc, 0, 0, 0, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    set_dialog_size_and_color(di, 0, 0, 0, normalsize_font_height + 10, 0, 32);
    set_dialog_properties(di, plot_menu, NULL, NULL);
    di = attach_new_plot_region_to_dialog(0, 0, 0, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    set_dialog_size_and_color(di, 0, 1.2 * normalsize_font_height + 10, 900, 668, 255, 0);
    /*    i = retrieve_item_from_dialog(d_draw_Op_proc, NULL);
          allegro_message("plot in %d",i); */
    di = attach_new_item_to_dialog(d_keyboard_proc, 12, 0, 0, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    set_dialog_properties(di, (void *) do_load, NULL, NULL); // unsafe cast
    di = attach_new_item_to_dialog(general_idle, 0, 0, 0, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    di = attach_new_item_to_dialog(d_yield_proc, 0, 0, 0, 0);

    if (di == NULL)
    {
        allegro_message("dialog pb");
        return 1;
    }

    set_dialog_size_and_color(di, 0, 0, 0, 0, 0, 0);
    //# endif
    return 0;
}




static void close_hook(void)
{
    simulate_keypress(27 + (KEY_ESC << 8));
}

void    quit_screen(void)
{
    //#ifdef XV_UNIX
    //    XUNLOCK();
    //#endif
    //    show_mouse(NULL);
    //#ifdef XV_UNIX
    //    XLOCK();
    //#endif
}
void    restore_screen(void)
{
  int win_color_depth_l = 0;
  int win_screen_w_l = 0;
  int win_screen_h_l = 0;
  int sw = 0, sh = 0;
  //char message[256] = {0};

  //win_printf("restore");
    screen_restoring = 1;
    //allegro_message("restoring about to proceed");
    win_color_depth_l = desktop_color_depth();

    if (get_desktop_resolution(&win_screen_w_l, &win_screen_h_l) != 0)
    {
        allegro_message("Cannot get the video mode property!");
    }
    if (win_color_depth_l != win_color_depth || win_screen_w_l != win_screen_w
	|| win_screen_h_l != win_screen_h)
      {
	screen = gui_get_screen();
	win_color_depth = win_color_depth_l;
	win_screen_w = win_screen_w_l;
	win_screen_h = win_screen_h_l;
	//snprintf(message,sizeof(message),"%d x %d x %d",win_screen_w,win_screen_h,win_color_depth);
# ifdef XV_WIN32
	show_mouse(NULL);
#endif
	set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
	if (win_screen_w_l > 3800 && win_screen_h_l > 2100)
	  {
	    sh = 3800;
	    sw = 2100;
	  }
	else if (win_screen_w_l > 3600 && win_screen_h_l > 2000)
	  {
	    sh = 3600;
	    sw = 2000;
	  }
	else if (win_screen_w_l > 3100 && win_screen_h_l > 1700)
	  {
	    sh = 3100;
	    sw = 1700;
	  }
	else if (win_screen_w_l > 2500 && win_screen_h_l > 1400)
	  {
	    sh = 2500;
	    sw = 1400;
	  }
	else if (win_screen_w_l > 2450 && win_screen_h_l > 1550)
	  {
	    sh = 2450;
	    sw = 1550;
	  }
	else if (win_screen_w_l > 2400 && win_screen_h_l > 1550)
	  {
	    sh = 2400;
	    sw = 1550;
	  }
	else if (win_screen_w_l > 1900 && win_screen_h_l > 1050)
	  {
	    sh = 1900;
	    sw = 1050;
	  }
	else if (win_screen_w_l > 1660 && win_screen_h_l > 1160)
	  {
	    sh = 1660;
	    sw = 1160;
	  }
	else if (win_screen_w_l > 1660 && win_screen_h_l > 1020)
	  {
	    sh = 1660;
	    sw = 1020;
	  }
	else if (win_screen_w_l > 1420 && win_screen_h_l > 850)
	  {
	    sh = 1420;
	    sw = 850;
	  }
	else if (win_screen_w_l > 1260 && win_screen_h_l > 974)
	  {
	    sh = 1260;
	    sw = 974;
	  }
	else if (win_screen_w_l > 1240 && win_screen_h_l > 768)
	  {
	    sh = 1240;
	    sw = 768;
	  }
	else if (win_screen_w_l > 1000 && win_screen_h_l > 718)
	  {
	    sh = 1000;
	    sw = 718;
	  }
	else if (win_screen_w_l > 900 && win_screen_h_l > 700)
	  {
	    sh = 900;
	    sw = 700;
	  }
	else if (win_screen_w_l >= 640 && win_screen_h_l >= 480)
	  {
	    sh = 640;
	    sw = 480;
	  }

	if (start_xvin(32, GFX_AUTODETECT_WINDOWED, sh, sw) != 0)
	  {
	    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
	    allegro_message("Error setting graphics mode\n%s\n", allegro_error);
	    return;
	  }
	set_XVin_resolution(sh, sw, 0);

	if (the_dialog != NULL)
	  {
	    the_dialog[0].w = sh;
	    the_dialog[0].h = sw;
	    the_dialog[2].w = sh;
	    the_dialog[2].h = sw - main_menu_height;
	    the_dialog[2].y = main_menu_height;
	  }


	dev_bitmap = screen;
	y_offset = dev_bitmap->h;
	max_x = dev_bitmap->h;
	max_y = dev_bitmap->w;

	//allegro_message(message);
	set_display_switch_callback(SWITCH_IN, restore_screen);
	set_display_switch_callback(SWITCH_OUT, quit_screen);
	set_close_button_callback(close_hook);
      }

#ifdef XV_WIN32
    broadcast_dialog_message(MSG_DRAW, 0);
#endif
    screen_restoring = 0;
}




int    init_config(int argc, char **argv)
{
    (void) argc;
    int i, j;
    char all[256] = {0};
    char program_fullfile[PATH_MAX] = {0};
#ifdef FORTIFY
    FILE *fp = NULL;
#endif
#ifndef XV_WIN32
    realpath(argv[0], program_fullfile);
#else
    _fullpath(program_fullfile, argv[0], sizeof(program_fullfile));
#endif

    for (i = j = 0; program_fullfile[i] != 0 && i < (int) my_min(sizeof(prog_dir), sizeof(program_fullfile)); i++)
    {
        prog_dir[i] = program_fullfile[i];
        j = (program_fullfile[i] == '/' || program_fullfile[i] == 92) ? i : j;
    }

    prog_dir[i] = 0;

    if (j > 0 && j < i)
    {
        prog_dir[j + 1] = 0;
    }

#ifdef XV_WIN32
    snprintf(config_dir, PATH_MAX, "%s", prog_dir);
#else
#ifdef UNIQUE_CONFIG_DIR
    snprintf(config_dir, PATH_MAX, "%s%s", getenv("HOME"), "/.config/xvin/");
#endif
#endif
#ifdef XV_MAC
    CFBundleRef CFBundleGetMainBundle(void);
    int len;
    CFBundleRef mainBundle = CFBundleGetMainBundle();

    if (mainBundle)
    {
        CFURLRef url = CFBundleCopyResourcesDirectoryURL(mainBundle);

        if (url)
        {
            if (CFURLGetFileSystemRepresentation(url, true, (unsigned char *)prog_dir, sizeof(prog_dir)))
            {
                len = strlen(prog_dir);
                prog_dir[len++] = '/';
                prog_dir[len] = 0;
            }

            CFRelease(url);
        }
    }

#endif
#ifdef FORTIFY
    snprintf(all, sizeof(all), "%s%s", prog_dir, "fortify-on.txt");
    fp = fopen(all, "r");

    if (fp == NULL)
    {
        Fortify_Disable();
        fortify_menu[1].text = NULL;
        fortify_menu[1].proc = NULL;
        fortify_menu[0].text = "Fortify next Xvin";
        fortify_menu[0].proc = set_fortify_next;
    }
    else
    {
        fclose(fp);
    }

#endif

    if (getenv("ALLEGRO") == NULL)
    {
        /* we get config from working directory */
        snprintf(all, 256, "ALLEGRO=%s", prog_dir);
        putenv(all);
        //printf("%s all %s\n",prog_dir,all);
        return 0;
    }

    return 1;
}


int xvin_main(int argc, char *argv[])
{
    /*    struct box b;*/
    /*   PALETTE pal;*/
    pltreg *pr = NULL;
    imreg *imr = NULL;
    O_p *op = NULL;
    O_i *oi = NULL;
    d_s *ds = NULL;
    int i;
    char  path[PATH_MAX] = { 0 };
    char file[PATH_MAX] = { 0 };
    //char *fi = NULL;
    //char ffile[PATH_MAX] = { 0 }; /*, buf[1024]; */
    //extern char fullfile[], *fu;
    /*    clock_t t0; */
    int ret, mode, mode_h, quit = 0;
    bool cgrfile = false;
    bool imagefile = false;
# ifdef BUILDING_DLL
    //int  argcp, np;
    //char **argvp, pl_name[64];
#else
    //extern int    track_ifc_main(int argc, char **argv);
    //extern int random_main(int argc, char **argv);
    //int EveryN_main(int argc, char **argv);
    //    int   track_ifc_main(int argc, char **argv);
    //    int random_main(int argc, char **argv);
    //int findSeq_main(int argc, char **argv);
# endif
    //int win_open = 0;
    DIALOG_PLAYER *player = NULL;

    #ifdef XV_UNIX
    xwin_set_window_name("Xvin","xvin");
    #endif

    if (allegro_init() != 0)
    {
        exit(1);
    }

    install_keyboard();
    install_timer();
    install_mouse();
    setlocale(LC_ALL, "C");
    init_config(argc, argv);
#ifdef XV_GTK_DIALOG
    gtk_init(0, NULL);
#endif
    //set_uformat(U_ASCII);
    before_menu_proc = NULL;
    after_menu_proc = NULL;
    snprintf(file, sizeof(config_dir), "%sxvin.cfg", config_dir);
    set_config_file(file);
    //  allegro_message("welcome boy\n");
    win_color_depth = desktop_color_depth();

    if (get_desktop_resolution(&win_screen_w, &win_screen_h) != 0)
    {
        allegro_message("Cannot get the video mode property!");
    }

    //printf("debug : config file : %s\n", file);
# ifdef FORTIFY
    //Fortify_SetOutputFunc(allegro_message);
    Fortify_SetOutputFunc(fortify_error);
# endif
    init_fonts(NULL);
    main_menu_height = normalsize_font_height + 10;
    //  if (win_color_depth != 32)
    //    allegro_message("Your video mode is not in 32 Bits !\n XVin might not work properly");
    mode = get_config_int("WINDOW_MODE", "size", 0);
    mode_h = get_config_int("WINDOW_MODE", "height", 0);
    full_screen = get_config_int("WINDOW_MODE", "full-screen", 0);
    keep_color = get_config_int("PASTE_OPTIONS", "KEEP_COLOR", 0);

    if (mode == 0)
    {
        mode = 1024;
        mode_h = 768;
    }

    generate_plot_dialog_xvin();

    if (full_screen == 0)
    {
        start_xvin(32, GFX_AUTODETECT_WINDOWED, mode, mode_h);
    }
    //GFX_DIRECTX_OVL
    else
    {
        start_xvin(32, GFX_AUTODETECT_FULLSCREEN , mode, mode_h);
    }

    //GFX_DIRECTX_OVL
    //
#ifdef XV_UNIX
    show_os_cursor(1);
#else
    show_mouse(screen);
#endif

    /* We set up colors to match screen color depth (in case it changed) */
    for (ret = 0; the_dialog[ret].proc; ret++)
    {
        the_dialog[ret].fg = makecol(0, 0, 0);
        the_dialog[ret].bg = makecol(255, 255, 255);
    }

    the_dialog[0].w = SCREEN_W;
    the_dialog[0].h = SCREEN_H;
    the_dialog[2].w = SCREEN_W;
    the_dialog[2].h = SCREEN_H - main_menu_height;
    the_dialog[2].y = main_menu_height;
    plt_buffer = create_bitmap(SCREEN_W, SCREEN_H);

    char d_fullfile[PATH_MAX] = {0};

    if (argc > 1)
    {

#ifndef XV_WIN32
	realpath(argv[1], d_fullfile);
#else
	_fullpath(d_fullfile, argv[1], sizeof(d_fullfile));
#endif

        if (strstr(d_fullfile, ".cgr") != NULL || strstr(d_fullfile, ".CGR") != NULL)
        {
            cgrfile = 1;
            pr = load_cgrfile_in_pltreg(NULL, d_fullfile);
        }

        if (!cgrfile)
        {
            imagefile = check_if_gr_file_is_an_image(d_fullfile);
        }
    }

    if (!cgrfile)
    {
        // Load plot
        if (!imagefile)
        {
            if (pr == NULL)
            {
                pr = build_plot_region(0,   32,  900,  668);

                // create plot_region
                if (pr == NULL)
                {
# ifdef XV_WIN32
                    show_mouse(screen);
#endif
                    win_printf_OK("Could not find or allocate plot region!");
                    quit = 1;
                }

                pr->auto_scale = 1;
                op = create_one_plot(GR_SIZE, GR_SIZE, 0);
                ds = get_op_cur_ds(op);
                add_data(pr, IS_ONE_PLOT, (void *)op);
                remove_data(pr, IS_ONE_PLOT, (void *)pr->o_p[0]);
                pr->cur_op = pr->n_op - 1;
                pr->one_p = pr->o_p[pr->cur_op];




            }

            // Load file in arguments
            if (argc > 1)
            {
                if (pltreadfile(op, argv[argc - 1], 0)  >= MAX_ERROR)
                {
                    op = create_one_plot(GR_SIZE, GR_SIZE, 0);
                    ds = get_op_cur_ds(op);
                    add_data(pr, IS_ONE_PLOT, (void *)op);
                    remove_data(pr, IS_ONE_PLOT, (void *)pr->o_p[0]);
                    pr->cur_op = pr->n_op - 1;
                    pr->one_p = pr->o_p[pr->cur_op];
                    remove_one_plot_data(op, IS_ONE_PLOT, (void *)pr->o_p[0]);
# ifdef XV_WIN32
                    show_mouse(screen);
#endif
                    win_printf_OK("Could not loaded plot from \n%s",
                                  backslash_to_slash(argv[argc - 1]));
                    quit = 1;
                }
                else
                {
                    //win_printf("argv %s",argv[argc - 1]);
                    extract_file_name(file, sizeof(file), d_fullfile);
                    extract_file_path(path, sizeof(path), d_fullfile);
                    reorder_loaded_plot_file_list(file, path);
                    reorder_visited_path_list(path);
                    op->filename = Mystrdup(file);
                    op->dir = Mystrdup(path);
		    strncpy(first_data_dir,path,sizeof(first_data_dir));
                    pr->one_p = op;
                    remove_one_plot_data(op, IS_DATA_SET, (void *)op->dat[0]);

                }
            }
            // No argument -- load xvin homepage
            else
            {
                    ds = get_op_cur_ds(op);

                    for (i = 0; i < ds->nx; i++)
                    {
                        ds->xd[i] = cos(26 * i * M_PI / ds->nx);
                        ds->yd[i] = cos(14 * i * M_PI / ds->nx);
                    }

                    set_plot_title(op, "\\pt20 Xvin");
                    set_plot_x_title(op, "Axe gY");
                    set_plot_y_title(op, "-Axe gX \\alpha \\Sigma ");
                    set_ds_plain_line(ds);
            }

            the_dialog[2].dp = pr;

            if (project_n < project_m)
            {
                //            project_n++;
                project_cur = project_n;
                project[project_n] = pr;
            }
        }
        // Load image
        else
        {
            proj_mn_n = -1;
            im_project_n = -1;
            extract_file_name(file, sizeof(file), argv[argc - 1]);
            extract_file_path(path, sizeof(path), argv[argc - 1]);
            put_path_separator(path, sizeof(path)); // changed 2005-10-05
            //win_printf("trying to load image %s",backslash_to_slash(argv[argc-1]));
            oi = load_im_file_in_oi(file, path);

            if (oi == NULL)
            {
                quit = 1;
                win_printf_OK("could not load image\n%s\n"
                              "from path %s", file, backslash_to_slash(path));
            }

            //load_im_file_in_imreg(imr, active_menu->text, (char*)active_menu->dp);
            imr = create_and_register_new_image_project(0,   32,  900,  668);

            if (imr == NULL)
            {
                quit = 1;
# ifdef XV_WIN32
                show_mouse(screen);
#endif
                win_printf_OK("could not create image region");
            }
            else if (oi != NULL)
            {
                add_image(imr, oi->im.data_type, (void *)oi);
                remove_from_image(imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
            }

            //load_im_file_in_imreg(imr, file, path);
        }
    }

    //  win_printf("display switch mode : %d", get_display_switch_mode());
    i = SWITCH_BACKGROUND; // with this option, XVin will still wor, even if put in the bacground

    if (set_display_switch_mode(i) != 0)
    {
        win_printf("cannot set display switch mode to %d", i);
    }

    //  win_printf("display switch mode : %d", get_display_switch_mode());
    // # SWITCH_NONE = 0 not OK on win32/windowed
    // # SWITCH_PAUSE = 1 default on win32/windowed
    // # SWITCH_AMNESIA = 2, not OK on win32/windowed
    // # SWITCH_BACKGROUND = 3, OK
    // # SWITCH_BACKAMNESIA =5, not OK on win32/windowed
    set_display_switch_callback(SWITCH_IN, restore_screen);
    set_display_switch_callback(SWITCH_OUT, quit_screen);
    set_close_button_callback(close_hook);
    //set_window_close_hook(close_hook);
    gui_fg_color = makecol(0, 0, 0);
    gui_mg_color = makecol(160, 160, 160);
    gui_bg_color = makecol(220, 220, 240);
    //    register_trace_handler(win_printf);
    //    register_assert_handler(win_printf);
    // below, we automatically load the plugins refered to in 'xvin.cfg' :
# ifdef XV_WIN32
    show_mouse(screen);
#endif

    if (quit == 0)
    {
        player = init_dialog(the_dialog, -1);
        update_dialog(player);
        broadcast_dialog_message(MSG_DRAW, 0);
# ifdef XVIN_STATICLINK
        //das1602_main(argc, argv);
        //track_ifc_main(argc, argv);
        //random_main(argc, argv);
        //EveryN_main(argc, argv);
        //findSeq_main(argc, argv);
# endif
        //run_module();
#ifdef BUILDING_DLL
    autoload_plugins();
    do_update_menu();
#endif

        while (update_dialog(player))
        {
            //rest(1);
        }

        shutdown_dialog(player);
        flush_config_file();
    }

    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
    allegro_exit();
    /*      allegro_message("1 plot in %ld ticks with %ld tics per sec \n%s",
            t0,CLOCKS_PER_SEC,message);
            allegro_message("%s",debug_ttf);*/
    return 0;
}

# include "fftl32n.h"
float *get_fftsin(void)
{
    return fftsin;
}

# endif
