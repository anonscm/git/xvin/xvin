#ifndef _PLOT_OP_C_
#define _PLOT_OP_C_

# include "xvin/platform.h"
# include "color.h"
# include "plot_op.h"
#include "xvin.h" // To have display_title_message
# include <stdlib.h>
# include <string.h>

// int 	*data_color;			/* possible data color */

# define 	BOX_COLOR 12

int		(*ds_special_2_use)(d_s *ds);
int		(*ds_use_2_special)(d_s *ds);
int		(*ds_free_use)(d_s *ds);
int		(*ds_duplicate_use)(d_s *dsd, const d_s *dss);
bool keep_color = true;


O_p* create_one_empty_plot(void)
{
	O_p *op = NULL;
	op = (O_p *)calloc(1,sizeof(O_p));
	if (op == NULL)			return (O_p *) xvin_ptr_error(Out_Of_Memory);
	if (init_one_plot(op))	return (O_p *) xvin_ptr_error(Out_Of_Memory);
	return op;
}

O_p* create_one_plot(int nx, int ny, int data_type)
{
	O_p *op = NULL;
	d_s *ds = NULL;
	(void)data_type;
	op = (O_p *)calloc(1,sizeof(O_p));
	if (op == NULL)			return (O_p *) xvin_ptr_error(Out_Of_Memory);
	if (init_one_plot(op))	return (O_p *) xvin_ptr_error(Out_Of_Memory);
	ds = build_data_set(nx, ny);
	if (ds  == NULL)		return (O_p *) xvin_ptr_error(Out_Of_Memory);
	if (add_one_plot_data(op, IS_DATA_SET, (void*)ds))
		return (O_p *) xvin_ptr_error(Out_Of_Memory);
	ds->nx = nx;	ds->ny = ny;
	return op;
}
int init_one_plot(O_p *op)
{
        int i;
	un_s	*un = NULL;

	if (op == NULL) return 1;
	op->width = op->height = 1;
	op->right = op->up = 0;
	op->iopt = 0;
	op->iopt2 = X_NUM + Y_NUM;
	op->tick_len = op->tick_len_x = op->tick_len_y = 1;
	op->x_lo = op->x_hi = op->y_lo = op->y_hi = 0;
	op->n_dat = op->cur_dat = op->n_lab = op->display_single_ds = 0;
	op->dx = op->dy = op->dt = 1;
	op->dat = (d_s **)calloc(MAX_DATA,sizeof(d_s*));
	op->lab = (p_l **)calloc(MAX_DATA,sizeof(p_l*));
	if (op->dat == NULL || op->lab == NULL) 	return xvin_error(Out_Of_Memory);
	op->m_lab = op->m_dat = MAX_DATA;
	op->n_xu = op->m_xu = op->c_xu = 0;
	op->n_yu = op->m_yu = op->c_yu = 0;
	op->n_tu = op->m_tu = op->c_tu = 0;
	op->c_xu_p = op->c_yu_p = op->c_tu_p = -1;
	op->filename = NULL;	op->dir = NULL;
	op->title = NULL;	op->x_title = NULL;	op->y_title = NULL;
	op->x_prime_title = op->y_prime_title = NULL;
	op->y_prefix = NULL;	op->x_prefix = NULL;   op->t_prefix = NULL;
	op->x_unit = NULL;	op->y_unit = NULL;     op->t_unit = NULL;
	op->xu = NULL;	op->yu = NULL;  op->tu = NULL;
	un = build_unit_set(IS_RAW_U, 0, 1, 0, 0, NULL);
	if (un == NULL)		return xvin_error(Out_Of_Memory);
	add_one_plot_data (op, IS_X_UNIT_SET, (void *)un);
	un = build_unit_set(IS_RAW_U, 0, 1, 0, 0, NULL);
	if (un == NULL)		return xvin_error(Out_Of_Memory);
	add_one_plot_data (op, IS_Y_UNIT_SET, (void *)un);
	un = build_unit_set(IS_RAW_U, 0, 1, 0, 0, NULL);
	if (un == NULL)		return xvin_error(Out_Of_Memory);
	add_one_plot_data (op, IS_T_UNIT_SET, (void *)un);
	init_box(&(op->bplt));
	op->bplt.pt = 10;
	op->bplt.color = BOX_COLOR;
	op->need_to_refresh = 1;
        op->data_changing = 0;
        op->transfering_data_to_box = 0;
	op->user_id = 0;
    op->read_only = false;
	for (i = 0; i < 16; i++)
	  {
	    op->user_ispare[i] = 0;
	    op->user_fspare[i] = 0;
	  }
        op->op_idle_action = NULL;
	op->op_got_mouse = NULL;
	op->op_lost_mouse = NULL;
        op->op_end_action = NULL;
	return 0;
}
int free_one_plot(O_p *op)
{
	int i;

	if (op == NULL) 	return xvin_error(Wrong_Argument);
	for (i=0 ; i< op->n_dat ; i++)	free_data_set(op->dat[i]);
	if (op->dat)  {free(op->dat);  op->dat = NULL;}
	for (i=0 ; i< op->n_lab ; i++)
	{
		if (op->lab[i]->text != NULL) 	free(op->lab[i]->text);
		if (op->lab[i] != NULL) 	free(op->lab[i]);
	}
	if (op->lab)	{free(op->lab);	   op->lab = NULL;}
	if (op->filename != NULL)	free(op->filename);
	if (op->dir != NULL)		free(op->dir);
	if (op->title != NULL)		free(op->title);
	if (op->x_title != NULL)	free(op->x_title);
	if (op->y_title != NULL)	free(op->y_title);
	if (op->x_prime_title != NULL)	free(op->x_prime_title);
	if (op->y_prime_title != NULL)	free(op->y_prime_title);
	if (op->y_prefix != NULL)	free(op->y_prefix);
	if (op->x_prefix != NULL)	free(op->x_prefix);
	if (op->t_prefix != NULL)	free(op->t_prefix);
	if (op->x_unit != NULL)		free(op->x_unit);
	if (op->y_unit != NULL)		free(op->y_unit);
	if (op->t_unit != NULL)		free(op->t_unit);
	if (op->xu != NULL)
	{
		for (i=0 ; i< op->n_xu ; i++)	free_unit_set(op->xu[i]);
		free(op->xu);
	}
	if (op->yu != NULL)
	{
		for (i=0 ; i< op->n_yu ; i++)	free_unit_set(op->yu[i]);
		free(op->yu);
	}
	if (op->tu != NULL)
	{
		for (i=0 ; i< op->n_tu ; i++)	free_unit_set(op->tu[i]);
		free(op->tu);
	}
	if (&op->bplt)  free_box(&op->bplt);
	free(op);	op = NULL;
	return 0;
}
d_s* create_and_attach_one_ds(O_p *op,int nx, int ny, int data_type)
{
	d_s *ds = NULL;
	(void)data_type;
	if ( op == NULL || nx <= 0 || ny <= 0)
		return (d_s *) xvin_ptr_error(Out_Of_Memory);
	ds = build_data_set(nx, ny);
	if (ds == NULL)		return (d_s *) xvin_ptr_error(Out_Of_Memory);
	ds->nx = nx;	ds->ny = ny;
	if (add_data_to_one_plot(op, IS_DATA_SET, (void*)ds))
		return (d_s *) xvin_ptr_error(Out_Of_Memory);
	op->cur_dat = op->n_dat - 1;
	op->need_to_refresh = 1;
	return ds;
}
int create_and_attach_n_ds(int nop, O_p *op,int nx, int ny, int data_type)
{
	d_s *ds = NULL;
	int i;
	(void)data_type;
	if ( op == NULL || nx <= 0 || ny <= 0)
		return xvin_error(Out_Of_Memory);
	for (i = 0; i < nop; i++)
	  {
	    ds = build_data_set(nx, ny);
	    if (ds == NULL)		return xvin_error(Out_Of_Memory);
	    ds->nx = nx;	ds->ny = ny;
	    if (add_data_to_one_plot(op, IS_DATA_SET, (void*)ds))
	      return xvin_error(Out_Of_Memory);
	    op->cur_dat = op->n_dat - 1;
	  }
	op->need_to_refresh = 1;
	return 0;
}

int	set_op_filename(O_p *op, const char *format, ...)
{
	char *c = NULL;
	va_list ap;

	if (op == NULL)	return xvin_error(Wrong_Argument);
	if (op->filename != NULL)		free(op->filename);

	if (format != NULL)
	{
		c = (char *)calloc(512,sizeof(char));
		if (c == NULL)			return xvin_error(Out_Of_Memory);
		va_start(ap, format);
		vsnprintf(c,512,format,ap);
		va_end(ap);
		op->filename = strdup(c);
		if (op->filename == NULL)		return xvin_error(Out_Of_Memory);
	}
	else 	op->filename = NULL;
	op->need_to_refresh = 1;
	if (c!= NULL)	free(c);
	return 0;
}

int	set_op_path(O_p *op, const char *format, ...)
{
	char *c = NULL;
	va_list ap;

	if (op == NULL)	return xvin_error(Wrong_Argument);
	if (op->dir != NULL)		free(op->dir);

	if (format != NULL)
	{
		c = (char *)calloc(512,sizeof(char));
		if (c == NULL)			return xvin_error(Out_Of_Memory);
		va_start(ap, format);
		vsnprintf(c,512,format,ap);
		va_end(ap);
		op->dir = strdup(c);
		if (op->dir == NULL)		return xvin_error(Out_Of_Memory);
	}
	else 	op->dir = NULL;
	op->need_to_refresh = 1;
	if (c!= NULL)	free(c);
	return 0;
}


char *get_op_filename(O_p *op)
{
	return (op != NULL) ? op->filename : (char *) xvin_ptr_error(Wrong_Argument);
}
char *get_op_path(O_p *op)
{
	return (op != NULL) ? op->dir : (char *) xvin_ptr_error(Wrong_Argument);
}
int	set_op_width(O_p *op, float width)
{
	if (op == NULL) return xvin_error(Wrong_Argument);
	op->width = width;
	op->need_to_refresh = 1;
	return 0;
}
int	set_op_height(O_p *op, float height)
{
	if (op == NULL) return xvin_error(Wrong_Argument);
	op->height = height;
	op->need_to_refresh = 1;
	return 0;
}
float 	get_op_width(O_p *op)
{
	if (op == NULL)
	{
		xvin_error(Wrong_Argument);
		return 0;
	}
	return op->width;
}
float 	get_op_height(O_p *op)
{
	if (op == NULL)
	{
		xvin_error(Wrong_Argument);
		return 0;
	}
	return op->height;
}
d_s	*get_op_cur_ds(O_p *op)
{
	if (op == NULL) return (d_s *) xvin_ptr_error(Wrong_Argument);
	if (op->dat == NULL || op->cur_dat < 0
	|| op->cur_dat >= op->n_dat)	return NULL;
	return op->dat[op->cur_dat];
}
int	get_ds_number_in_op(O_p *op, d_s *ds)
{
  int i, j;

	if (op == NULL || ds == NULL || op->dat == NULL)
	  return xvin_error(Wrong_Argument);
	for (i = 0, j = - 1; i < op->n_dat && j == -1; i++)
	  j = (ds == op->dat[i]) ? i : j;
	return j;
}

int	add_ds_to_op(O_p *op, d_s *ds)
{

	if (ds == NULL || op == NULL)	return xvin_error(Wrong_Argument);
	if (op->n_dat >= op->m_dat )
	{
		op->m_dat += MAX_DATA;
		op->dat = (d_s**)realloc(op->dat,op->m_dat*sizeof(d_s*));
		if (op->dat == NULL)	return xvin_error(Out_Of_Memory);
	}
	op->dat[op->n_dat] = ds;
	op->n_dat++;
	op->need_to_refresh = 1;
	return 0;
}


int	add_plot_label_to_op(O_p *op, p_l *pl)
{

	if (pl == NULL || op == NULL)	return xvin_error(Wrong_Argument);
	if (op->n_lab >= op->m_lab )
	{
		op->m_lab += MAX_DATA;
		op->lab = (p_l**)realloc(op->lab,op->m_lab*sizeof(p_l*));
		if (op->lab == NULL)	return xvin_error(Out_Of_Memory);
	}
	if (pl->text != NULL)	/* no empty label */
	{
		op->lab[op->n_lab] = pl;
		op->n_lab++;
	}
	else return xvin_error(Wrong_Argument);
	op->need_to_refresh = 1;
	return 0;
}

int	set_op_plot_label(O_p *op, float tx, float ty, int type, char *format, ...)
{
	char *c = NULL;
	va_list ap;
	p_l *pl = NULL;


	if (op == NULL)	return xvin_error(Wrong_Argument);

	if (format != NULL)
	{
		c = (char *)calloc(2048,sizeof(char));
		if (c == NULL)			return xvin_error(Out_Of_Memory);
		va_start(ap, format);
		vsnprintf(c,2048,format,ap);
		va_end(ap);
		pl = (p_l*)calloc(1,sizeof(struct plot_label));
		if ( pl == NULL )	return xvin_error(Out_Of_Memory);
		pl->xla = tx;
		pl->yla = ty;
		pl->text = (char *)strdup(c);
		pl->type = type;
		if (pl->text == NULL)	return xvin_error(Out_Of_Memory);
		add_plot_label_to_op(op, pl);
	}
	else 	return xvin_error(Wrong_Argument);

	op->need_to_refresh = 1;
	if (c!= NULL)	free(c);
	return 0;
}

char *create_description_label(O_p *op, int n_max_ds)
{
  char buf[128] = {0}, stuff[4096] = {0}, *s = NULL;
  int i;
  d_s *ds = NULL;

  if (op == NULL)   return "Op was NULL!";
  snprintf(stuff,4096,"\\pt8\\fbox{\\stack{");
  s = stuff + strlen(stuff);
  for (i = 0; i < op->n_dat && i < n_max_ds; i++)
    {
      ds = op->dat[i];
      if (ds->color == Black)			snprintf(s,4096-strlen(stuff),"{\\color{black}");
      else if (ds->color == Blue)		snprintf(s,4096-strlen(stuff),"{\\color{blue}");
      else if (ds->color == Green)		snprintf(s,4096-strlen(stuff),"{\\color{green}");
      else if (ds->color == Cyan)		snprintf(s,4096-strlen(stuff),"{\\color{cyan}");
      else if (ds->color == Red)		snprintf(s,4096-strlen(stuff),"{\\color{red}");
      else if (ds->color == Magenta)		snprintf(s,4096-strlen(stuff),"{\\color{magenta}");
      else if (ds->color == Brown)		snprintf(s,4096-strlen(stuff),"{\\color{brown}");
      else if (ds->color == Lightgray)	        snprintf(s,4096-strlen(stuff),"{\\color{lightgray}");
      else if (ds->color == Darkgray)		snprintf(s,4096-strlen(stuff),"{\\color{darkgray}");
      else if (ds->color == Lightblue) 	        snprintf(s,4096-strlen(stuff),"{\\color{lightblue}");
      else if (ds->color == Lightgreen)	        snprintf(s,4096-strlen(stuff),"{\\color{lightgreen}");
      else if (ds->color == Lightcyan)	        snprintf(s,4096-strlen(stuff),"{\\color{lightcyan}");
      else if (ds->color == Lightred)		snprintf(s,4096-strlen(stuff),"{\\color{lightred}");
      else if (ds->color == Lightmagenta)	snprintf(s,4096-strlen(stuff),"{\\color{lightmagenta}");
      else if (ds->color == Yellow)		snprintf(s,4096-strlen(stuff),"{\\color{yellow}");
      else if (ds->color == White)		snprintf(s,4096-strlen(stuff),"{\\color{white}");
      s = stuff + strlen(stuff);
      if (ds->symb != NULL)   		snprintf(s,4096-strlen(stuff), " %s ",ds->symb);
      s = stuff + strlen(stuff);
      if (op->dat[i]->source != NULL)
	strncpy(buf, op->dat[i]->source, 64);
      else if (op->dat[i]->treatement != NULL)
	strncpy(buf, op->dat[i]->treatement, 64);
      else strncpy(buf,"No source",64);
      buf[64] = 0;
      if (strlen(buf) > 63) my_strncat(buf,"...",sizeof(buf));
      snprintf(s,4096-strlen(stuff), "%s }",buf);
      s = stuff + strlen(stuff);
    }
  snprintf(s,4096-strlen(stuff),"}}");
  return strdup(stuff);
}


int	cancel_offset_in_op_unit_set(O_p *op, int type)
{
  int i;

  if (op == NULL)	return xvin_error(Wrong_Argument);
  if (type == IS_X_UNIT_SET)      for (i = 0, op->ax = 0; i < op->n_xu; i++) 	  op->xu[i]->ax = 0;
  else if (type == IS_Y_UNIT_SET) for (i = 0, op->ay = 0; i < op->n_yu; i++) 	  op->yu[i]->ax = 0;
  else if (type == IS_T_UNIT_SET) for (i = 0, op->at = 0; i < op->n_tu; i++) 	  op->tu[i]->ax = 0;
  else return 1;
  op->need_to_refresh = 1;
  return 0;
}

int	add_x_unit_set_to_op(O_p *op, un_s *un)
{

	if (un == NULL || op == NULL)	return xvin_error(Wrong_Argument);
	if (op->n_xu >= op->m_xu )
	{
		op->m_xu += MAX_DATA;
		op->xu = (un_s**)realloc(op->xu,op->m_xu*sizeof(un_s*));
		if (op->xu == NULL)	return xvin_error(Out_Of_Memory);
	}
	op->xu[op->n_xu] = un;
	un->axis = IS_X_UNIT_SET;
	op->n_xu++;
	op->need_to_refresh = 1;
	return 0;
}
int	add_y_unit_set_to_op(O_p *op, un_s *un)
{

	if (un == NULL || op == NULL)	return xvin_error(Wrong_Argument);
	if (op->n_yu >= op->m_yu )
	{
		op->m_yu += MAX_DATA;
		op->yu = (un_s**)realloc(op->yu,op->m_yu*sizeof(un_s*));
		if (op->yu == NULL)	return xvin_error(Out_Of_Memory);
	}
	op->yu[op->n_yu] = un;
	un->axis = IS_Y_UNIT_SET;
	op->n_yu++;
	op->need_to_refresh = 1;
	return 0;
}
int	add_t_unit_set_to_op(O_p *op, un_s *un)
{

	if (un == NULL || op == NULL)	return xvin_error(Wrong_Argument);
	if (op->n_tu >= op->m_tu )
	{
		op->m_tu += MAX_DATA;
		op->tu = (un_s**)realloc(op->tu,op->m_tu*sizeof(un_s*));
		if (op->tu == NULL)	return xvin_error(Out_Of_Memory);
	}
	op->tu[op->n_tu] = un;
	un->axis = IS_T_UNIT_SET;
	op->n_tu++;
	op->need_to_refresh = 1;
	return 0;
}

int add_one_plot_data (O_p *op, int type, void *stuff)
{
	return add_data_to_one_plot (op, type, stuff);
}
int add_data_to_one_plot (O_p *op, int type, void *stuff)
{
	int i = 0;
	d_s *ds = NULL;

	if (stuff == NULL || op == NULL)
		return xvin_error(Wrong_Argument);
	if (type == IS_DATA_SET)
	{
		if (op->n_dat >= op->m_dat )
		{
			op->m_dat += MAX_DATA;
			op->dat = (d_s**)realloc(op->dat,op->m_dat*sizeof(d_s*));
			if (op->dat == NULL)	return xvin_error(Out_Of_Memory);
		}
		op->dat[op->n_dat] = (d_s*)stuff;
        //printf("%d\n", ((d_s*)stuff)->color);
        if (((d_s*)stuff)->color == 0 || !keep_color)
        {
		    ((d_s*)stuff)->color = data_color[(op->n_dat)%max_data_color];
        }
		op->n_dat++;
		op->need_to_refresh = 1;
	}
	else if (type == IS_SPECIAL)
	{
		ds = op->dat[op->cur_dat];
		return add_to_data_set(ds, type, stuff);
	}
/*	else	if (type == IS_SPECIAL)
	{
		ds = op->dat[op->cur_dat];
		if (ds->n_special >=  ds->m_special)
		{
			ds->m_special++;
			ds->special = (char**)realloc(ds->special,ds->m_special*sizeof(char*));
			if (ds->special == NULL)	return xvin_error(Out_Of_Memory);
		}
		ds->special[ds->n_special] = strdup((char*)stuff);
		if (ds->special[ds->n_special] == NULL)	return -1;
		ds->n_special++;
	}
*/
	else if (type == IS_PLOT_LABEL)
	{
		if (op->n_lab >= op->m_lab )
		{
			op->m_lab += MAX_DATA;
			op->lab = (p_l**)realloc(op->lab,op->m_lab*sizeof(p_l*));
			if (op->lab == NULL)	return xvin_error(Out_Of_Memory);
		}
		if (((p_l*)stuff)->text != NULL)	/* no empty label */
		{
			op->lab[op->n_lab] = (p_l*)stuff;
			op->n_lab++;
		}
		op->need_to_refresh = 1;
	}
	else if (type == IS_X_UNIT_SET)
	{
		if (op->n_xu >= op->m_xu )
		{
			op->m_xu += MAX_DATA;
			op->xu = (un_s**)realloc(op->xu,op->m_xu*sizeof(un_s*));
			if (op->xu == NULL)		return xvin_error(Out_Of_Memory);
		}
		op->xu[op->n_xu] = (un_s*)stuff;
		((un_s*)stuff)->axis = IS_X_UNIT_SET;
		op->n_xu++;
		op->need_to_refresh = 1;
	}
	else if (type == IS_Y_UNIT_SET)
	{
		if (op->n_yu >= op->m_yu )
		{
			op->m_yu += MAX_DATA;
			op->yu = (un_s**)realloc(op->yu,op->m_yu*sizeof(un_s*));
			if (op->yu == NULL)		return xvin_error(Out_Of_Memory);
		}
		op->yu[op->n_yu] = (un_s*)stuff;
		((un_s*)stuff)->axis = IS_Y_UNIT_SET;
		op->n_yu++;
		op->need_to_refresh = 1;
	}
	else if (type == IS_T_UNIT_SET)
	{
		if (op->n_tu >= op->m_tu )
		{
			op->m_tu += MAX_DATA;
			op->tu = (un_s**)realloc(op->tu,op->m_tu*sizeof(un_s*));
			if (op->tu == NULL)		return xvin_error(Out_Of_Memory);
		}
		op->tu[op->n_tu] = (un_s*)stuff;
		((un_s*)stuff)->axis = IS_T_UNIT_SET;
		op->n_tu++;
		op->need_to_refresh = 1;
	}
	else 	i = 1;
	return i;
}


int	remove_ds_from_op(O_p *op, d_s *ds)
{
	int i = 0, j;
	if (op == NULL || ds == NULL)	return xvin_error(Wrong_Argument);
	for (i=0, j = -1 ; (i< op->n_dat) && (j < 0) ; i++)
		j = (op->dat[i] == ds) ? i : j;
	if ( (j >= 0) && (op->n_dat > 1) )
	{
		free_data_set(op->dat[j]);
/*		if (op->dat[j] != NULL) 	free(op->dat[j]);
 */
		op->n_dat--;
		for (i=j ; i< op->n_dat ; i++)
			op->dat[i] = op->dat[i+1];
		if (op->cur_dat >= j && op->cur_dat > 0)op->cur_dat--;
		op->need_to_refresh = 1;
	}
	return 0;
}
int	remove_ds_n_from_op(O_p *op, int n)
{
	if (op == NULL)	return xvin_error(Wrong_Argument);
	if (n < 0 || n >= op->n_dat) return xvin_error(Wrong_Argument);
	return remove_ds_from_op(op, op->dat[n]);
}
int	remove_plot_label_from_op(O_p *op, p_l *pl)
{
	int i = 0, j, k;
	if (op == NULL || pl == NULL)	return xvin_error(Wrong_Argument);

	for (i=0, j = -1 ; (i< op->n_lab) && (j < 0) ; i++)
		j = (op->lab[i] == pl) ? i : j;
	if ( j >= 0 )
	{
		if (op->lab[j]->text != NULL) 	free(op->lab[j]->text);
		if (op->lab[j] != NULL) 	free(op->lab[j]);
		op->n_lab--;
		for (i=j ; i< op->n_lab ; i++)
			op->lab[i] = op->lab[i+1];
		op->need_to_refresh = 1;
		return 0;
	}
	for (i = 0; i < op->n_dat; i++)
	{
		for (j=0 ; j < op->dat[i]->n_lab; j++)
		{
			if (pl == op->dat[i]->lab[j])
			{
				if (op->dat[i]->lab[j]->text != NULL)
					free(op->dat[i]->lab[j]->text);
				if (op->dat[i]->lab[j] != NULL) 	free(op->dat[i]->lab[j]);
				op->dat[i]->n_lab--;
				for (k = j ; k < op->dat[i]->n_lab ; k++)
					op->dat[i]->lab[k] = op->dat[i]->lab[k+1];
				op->need_to_refresh = 1;
				return 0;
			}
		}
	}

	/*	xvin_error(Wrong_Argument);*/
	return 0;
}
int	remove_x_unit_set_from_op(O_p *op, un_s *un)
{
	int i = 0, j;
	if (op == NULL || un == NULL)	return xvin_error(Wrong_Argument);

	for (i=0, j = -1 ; (i< op->n_xu) && (j < 0) ; i++)
		j = (op->xu[i] == un) ? i : j;
	if ( j >= 0 )
	{
		if (op->xu[j] != NULL)	free_unit_set(op->xu[j]);
		op->n_xu--;
		for (i=j ; i< op->n_xu ; i++)
			op->xu[i] = op->xu[i+1];
		if (op->c_xu >= j && op->c_xu > 0)	op->c_xu--;
		if (op->c_xu_p >= j && op->c_xu_p > 0)	op->c_xu_p--;
		op->need_to_refresh = 1;
	}
	return 0;
}
int	remove_y_unit_set_from_op(O_p *op, un_s *un)
{
	int i = 0, j;
	if (op == NULL || un == NULL)	return xvin_error(Wrong_Argument);

	for (i=0, j = -1 ; (i< op->n_yu) && (j < 0) ; i++)
		j = (op->yu[i] == un) ? i : j;
	if ( j >= 0 )
	{
		if (op->yu[j] != NULL)	free_unit_set(op->yu[j]);
		op->n_yu--;
		for (i=j ; i< op->n_yu ; i++)
			op->yu[i] = op->yu[i+1];
		if (op->c_yu >= j && op->c_yu > 0)	op->c_yu--;
		if (op->c_yu_p >= j && op->c_yu_p > 0)	op->c_yu_p--;
		op->need_to_refresh = 1;
	}
	return 0;
}
int	remove_t_unit_set_from_op(O_p *op, un_s *un)
{
	int i = 0, j;
	if (op == NULL || un == NULL)	return xvin_error(Wrong_Argument);

	for (i=0, j = -1 ; (i< op->n_tu) && (j < 0) ; i++)
		j = (op->tu[i] == un) ? i : j;
	if ( j >= 0 )
	{
		if (op->tu[j] != NULL)	free_unit_set(op->tu[j]);
		op->n_tu--;
		for (i=j ; i< op->n_tu ; i++)
			op->tu[i] = op->tu[i+1];
		if (op->c_tu >= j && op->c_tu > 0)	op->c_tu--;
		if (op->c_tu_p >= j && op->c_tu_p > 0)	op->c_tu_p--;
		op->need_to_refresh = 1;
	}
	return 0;
}


int remove_one_plot_data (O_p *op, int type, void *stuff)
{
	int i = 0, j, k;
	int ret = 0;
	d_s *ds = NULL;

	if (op == NULL || stuff == NULL)
	{
		return xvin_error(Wrong_Argument);
	}
	if (type == IS_UNIT_SET)	type = ((un_s*)stuff)->axis;
	if (type == IS_DATA_SET)
	{
		for (i=0, j = -1 ; (i< op->n_dat) && (j < 0) ; i++)
		{
			if ( (void*)(op->dat[i]) == stuff)
				j = i;
		}
		if ( (j >= 0) && (op->n_dat > 1) )
		{
			free_data_set(op->dat[j]);
/*			if (op->dat[j] != NULL) 	free(op->dat[j]);
 */
			op->n_dat--;
			for (i=j ; i< op->n_dat ; i++)
				op->dat[i] = op->dat[i+1];
			if (op->cur_dat >= j && op->cur_dat > 0)op->cur_dat--;
			op->need_to_refresh = 1;
		}
	}
	else if (type == IS_PLOT_LABEL)
	{
		for (i=0, j = -1 ; (i< op->n_lab) && (j < 0) ; i++)
		{
			if ( (void*)(op->lab[i]) == stuff)
				j = i;
		}
		if ( j >= 0 )
		{
			if (op->lab[j]->text != NULL) 	free(op->lab[j]->text);
			if (op->lab[j] != NULL) 	free(op->lab[j]);
			op->n_lab--;
			for (i=j ; i< op->n_lab ; i++)
				op->lab[i] = op->lab[i+1];
			op->need_to_refresh = 1;
			return 0;
		}

		for (i = 0; i < op->n_dat; i++)
		{
			for (j=0 ; j < op->dat[i]->n_lab; j++)
			{
				if (stuff == (void*)op->dat[i]->lab[j])
				{
					if (op->dat[i]->lab[j]->text != NULL)
						free(op->dat[i]->lab[j]->text);
					if (op->dat[i]->lab[j] != NULL) 	free(op->dat[i]->lab[j]);
					op->dat[i]->n_lab--;
					for (k = j ; k < op->dat[i]->n_lab ; k++)
						op->lab[k] = op->lab[k+1];
					op->need_to_refresh = 1;
					return 0;
				}
			}
		}
/*		win_printf_OK("plot label not removed");*/
		return xvin_error(Wrong_Argument);
	}
	else if (type == IS_SPECIAL || type == ALL_SPECIAL)
	{
		ds = op->dat[op->cur_dat];
		return remove_from_data_set(ds, type, stuff);
	}
	else if (type == IS_X_UNIT_SET)
	{
		for (i=0, j = -1 ; (i< op->n_xu) && (j < 0) ; i++)
			j = ((void*)(op->xu[i]) == stuff) ? i : j;
		if ( j >= 0 )
		{
			if (op->xu[j] != NULL)	free_unit_set(op->xu[j]);
			op->n_xu--;
			for (i=j ; i< op->n_xu ; i++)
				op->xu[i] = op->xu[i+1];
			if (op->c_xu >= j && op->c_xu > 0)	op->c_xu--;
			if (op->c_xu_p >= j && op->c_xu_p > 0)	op->c_xu_p--;
			op->need_to_refresh = 1;
		}
	}
	else if (type == IS_Y_UNIT_SET)
	{
		for (i=0, j = -1 ; (i< op->n_yu) && (j < 0) ; i++)
			j = ((void*)(op->yu[i]) == stuff) ? i : j;
		if ( j >= 0 )
		{
			if (op->yu[j] != NULL)	free_unit_set(op->yu[j]);
			op->n_yu--;
			for (i=j ; i< op->n_yu ; i++)
				op->yu[i] = op->yu[i+1];
			if (op->c_yu >= j && op->c_yu > 0)	op->c_yu--;
			if (op->c_yu_p >= j && op->c_yu_p > 0)	op->c_yu_p--;
			op->need_to_refresh = 1;
		}
	}
	else if (type == IS_T_UNIT_SET)
	{
		for (i=0, j = -1 ; (i< op->n_tu) && (j < 0) ; i++)
			j = ((void*)(op->tu[i]) == stuff) ? i : j;
		if ( j >= 0 )
		{
			if (op->tu[j] != NULL)	free_unit_set(op->tu[j]);
			op->n_tu--;
			for (i=j ; i< op->n_tu ; i++)
				op->tu[i] = op->tu[i+1];
			if (op->c_tu >= j && op->c_tu > 0)	op->c_tu--;
			if (op->c_tu_p >= j && op->c_tu_p > 0)	op->c_tu_p--;
			op->need_to_refresh = 1;
		}
	}
	else 	ret = 1;
	return ret;
}
int	set_plot_title(O_p *op, const char *format, ...)
{
	char *c = NULL;
	va_list ap;

	if (op == NULL)	return xvin_error(Wrong_Argument);
	if (op->title != NULL)		free(op->title);

	if (format != NULL)
	{
		c = (char *)calloc(2048,sizeof(char));
		if (c == NULL)			return xvin_error(Out_Of_Memory);
		va_start(ap, format);
		vsnprintf(c,2048,format,ap);
		va_end(ap);
		op->title = strdup(c);
		if (op->title == NULL)		return xvin_error(Out_Of_Memory);
	}
	else 	op->title = NULL;
	op->need_to_refresh = 1;
	if (c!= NULL)	free(c);
	return 0;
}
int	set_plot_x_title(O_p *op, const char *format, ...)
{
	char *c = NULL;
	va_list ap;

	if (op == NULL)	return xvin_error(Wrong_Argument);
	if (op->x_title != NULL)	free(op->x_title);
	if (format != NULL)
	{
		c = (char *)calloc(2048,sizeof(char));
		if (c == NULL)			return xvin_error(Out_Of_Memory);
		va_start(ap, format);
		vsnprintf(c,2048,format,ap);
		va_end(ap);
		op->x_title = strdup(c);
		if (op->x_title == NULL)	return xvin_error(Out_Of_Memory);
	}
	else 	op->x_title = NULL;
	op->need_to_refresh = 1;
	free(c);	return 0;
}
int	set_plot_y_title(O_p *op, const char *format, ...)
{
	char *c = NULL;
	va_list ap;

	if (op == NULL)	return xvin_error(Wrong_Argument);
	if (op->y_title != NULL)	free(op->y_title);
	if (format != NULL)
	{
		c = (char *)calloc(2048,sizeof(char));
		if (c == NULL)			return xvin_error(Out_Of_Memory);
		va_start(ap, format);
		vsnprintf(c,2048,format,ap);
		va_end(ap);
		op->y_title = strdup(c);
		if (op->y_title == NULL)	return xvin_error(Out_Of_Memory);
	}
	else 	op->y_title = NULL;
	op->need_to_refresh = 1;
	free(c);	return 0;
}

int	set_plot_x_prime_title(O_p *op, const char *format, ...)
{
	char *c = NULL;
	va_list ap;

	if (op == NULL)	return xvin_error(Wrong_Argument);
	if (op->x_prime_title != NULL)	free(op->x_prime_title);
	if (format != NULL)
	{
		c = (char *)calloc(2048,sizeof(char));
		if (c == NULL)			return xvin_error(Out_Of_Memory);
		va_start(ap, format);
		vsnprintf(c,2048,format,ap);
		va_end(ap);
		op->x_prime_title = strdup(c);
		if (op->x_prime_title == NULL)	return xvin_error(Out_Of_Memory);
	}
	else 	op->x_prime_title = NULL;
	op->need_to_refresh = 1;
	free(c);	return 0;
}
int	set_plot_y_prime_title(O_p *op, const char *format, ...)
{
	char *c = NULL;
	va_list ap;

	if (op == NULL)	return xvin_error(Wrong_Argument);
	if (op->y_prime_title != NULL)	free(op->y_prime_title);
	if (format != NULL)
	{
		c = (char *)calloc(2048,sizeof(char));
		if (c == NULL)			return xvin_error(Out_Of_Memory);
		va_start(ap, format);
		vsnprintf(c,2048,format,ap);
		va_end(ap);
		op->y_prime_title = strdup(c);
		if (op->y_prime_title == NULL)	return xvin_error(Out_Of_Memory);
	}
	else 	op->y_prime_title = NULL;
	op->need_to_refresh = 1;
	free(c);	return 0;
}
int	set_plot_y_log(O_p *op)
{
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	op->need_to_refresh = 1;
	op->iopt |= YLOG;	return 0;
}
int	set_plot_x_log(O_p *op)
{
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	op->need_to_refresh = 1;
	op->iopt |= XLOG;	return 0;
}
int	is_plot_x_log(O_p *op)
{
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	return (op->iopt & XLOG) ? 1 : 0;
}
int	is_plot_y_log(O_p *op)
{
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	return (op->iopt & YLOG) ? 1 : 0;
}
int	set_plot_y_lin(O_p *op)
{
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	op->need_to_refresh = 1;
	op->iopt &= ~YLOG;	return 0;
}
int	set_plot_x_lin(O_p *op)
{
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	op->need_to_refresh = 1;
	op->iopt &= ~XLOG;	return 0;
}
int	set_plot_x_fixed_range(O_p *op)
{
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	op->need_to_refresh = 1;
	op->iopt2 |= X_LIM;	return 0;
}
int	set_plot_x_auto_range(O_p *op)
{
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	op->need_to_refresh = 1;
	op->iopt2 &= ~X_LIM;	return 0;
}
int	is_plot_x_auto_range(O_p *op)
{
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	op->need_to_refresh = 1;
	return (op->iopt2 & X_LIM) ? 0 : 1;
}
int	set_plot_y_fixed_range(O_p *op)
{
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	op->need_to_refresh = 1;
	op->iopt2 |= Y_LIM;	return 0;
}
int	set_plot_y_auto_range(O_p *op)
{
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	op->need_to_refresh = 1;
	op->iopt2 &= ~Y_LIM;	return 0;
}
int	is_plot_y_auto_range(O_p *op)
{
	if (op == NULL) 	return xvin_error(Wrong_Argument);
	op->need_to_refresh = 1;
	return (op->iopt2 & Y_LIM) ? 0 : 1;
}

int		adjust_limits_on_ds_range(O_p* op, int dsidx)
{
	int imin, imax, j;
	d_s *dsi = NULL;
	int nf;
	float min, max, lo, hi, xmin, xmax, lot, hit;
	

    if (dsidx < 0 || dsidx >= op->n_dat)
        return -1;

    dsi = op->dat[dsidx];

	nf = dsi->nx;	/* this is the number of points in the data set */
	
	hi = lo = dsi->xd[0]; 
	min = max = dsi->yd[0]; 			
	for (imin = imax = xmin = xmax = -1, j = 0; j < nf; j++)
	{
		if (imin == -1 || dsi->yd[j] < min)	
		{
			min = dsi->yd[j];
			imin = 0;
		}
		if (imax == -1 || dsi->yd[j] > max)
		{
			max = dsi->yd[j];
			imax = 0;
		}
		if (xmin == -1 || dsi->xd[j] < lo)	
		{
			lo = dsi->xd[j];
			xmin = 0;
		}
		if (xmax == -1 || dsi->xd[j] > hi)
		{
			hi = dsi->xd[j];
			xmax = 0;
		}
	}
	op->iopt2 |= X_LIM;
	lot = (min + max)/2;
	hit = (max - min)/2;
	min = lot - 1.05 * hit;
	max = lot + 1.05 * hit;
	if (imin == 0 && imax == 0)
	{
		op->y_lo = min;
		op->y_hi = max;
		op->iopt2 |= Y_LIM;
		display_title_message("Y changed lo %g hi %g",op->y_lo,op->y_hi);
        }
	lot = (lo + hi)/2;
	hit = (hi - lo)/2;
	lo = lot - 1.05 * hit;
	hi = lot + 1.05 * hit;
	if (xmin == 0 && xmax == 0)
	{
		op->x_lo = lo;
		op->x_hi = hi;
		op->iopt2 |= X_LIM;
		display_title_message("X changed lo %g hi %g",op->y_lo,op->y_hi);
        }
	op->need_to_refresh = 1;		
	return 0;
}
int	create_attach_select_x_un_to_op(O_p *op, int type, float ax, float dx, char decade, char mode, char *name)
{
	un_s *un = NULL;

	if (op == NULL)		return xvin_error(Wrong_Argument);
	un = build_unit_set(type, ax, dx, decade, mode, name);
	if (un == NULL)		return xvin_error(Out_Of_Memory);
	add_data_to_one_plot (op, IS_X_UNIT_SET, (void *)un);
	set_op_x_unit_set(op, op->n_xu-1);
	op->need_to_refresh = 1;
	return 0;
}
int	create_attach_select_y_un_to_op(O_p *op, int type, float ax, float dx, char decade, char mode, char *name)
{
	un_s *un = NULL;

	if (op == NULL)		return xvin_error(Wrong_Argument);
	un = build_unit_set(type, ax, dx, decade, mode, name);
	if (un == NULL)		return xvin_error(Out_Of_Memory);
	add_data_to_one_plot (op, IS_Y_UNIT_SET, (void *)un);
	set_op_y_unit_set(op, op->n_yu-1);
	op->need_to_refresh = 1;
	return 0;
}
int	create_attach_select_t_un_to_op(O_p *op, int type, float ax, float dx, char decade, char mode, char *name)
{
	un_s *un = NULL;

	if (op == NULL)		return xvin_error(Wrong_Argument);
	un = build_unit_set(type, ax, dx, decade, mode, name);
	if (un == NULL)		return xvin_error(Out_Of_Memory);
	add_data_to_one_plot (op, IS_T_UNIT_SET, (void *)un);
	set_op_t_unit_set(op, op->n_tu-1);
	op->need_to_refresh = 1;
	return 0;
}

d_s *find_source_specific_ds_in_op(O_p *op, char *source)
{
	int i, icp;
	int l;
	char *c = NULL;
	d_s *dss = NULL;

	if (op == NULL || source == NULL)	return NULL;
	l = strlen(source);
	for (i = 0, icp = -1; i < op->n_dat && icp == -1; i++)
	{
		if (op->dat[i] != NULL)
		{
			dss = op->dat[i];
			c = dss->source;
			icp = ((c != NULL) && strncmp(c,source,l) == 0) ? i : -1;
		}
	}
	return (icp == -1) ? NULL : dss;
}
d_s *find_treatement_specific_ds_in_op(O_p *op, char *treatement)
{
	int i, icp;
	int l;
	char *c = NULL;
	d_s *dss = NULL;

	if (op == NULL || treatement == NULL)	return NULL;
	l = strlen(treatement);
	for (i = 0, icp = -1; i < op->n_dat && icp == -1; i++)
	{
		if (op->dat[i] != NULL)
		{
			dss = op->dat[i];
			c = dss->treatement;
			icp = ((c != NULL) && strncmp(c,treatement,l) == 0) ? i : -1;
		}
	}
	return (icp == -1) ? NULL : dss;
}


int 	set_op_x_unit_set(O_p *op, int n)
{
	un_s *lxu = NULL;

	if (op == NULL) return 1;
	if (n >= op->n_xu)			n = 0;
	if (n < 0)	n = 0;
	if (op->n_xu == 0 || op->xu == NULL)	return 1;
	op->c_xu = n;
	lxu = op->xu[n];
	op->ax = lxu->ax;
	op->dx = lxu->dx;
	if (op->x_unit != NULL)		free(op->x_unit);
	op->x_unit = Mystrdup(lxu->name);
	op->need_to_refresh = 1;
	return 0;
}

int	set_op_y_unit_set(O_p *op, int n)
{
	un_s *lyu = NULL;

	if (op == NULL)							return 1;
	if (n >= op->n_yu)						n = 0;
	if (n < 0)	n = 0;
	if (op->n_yu == 0 || op->yu == NULL)	return 1;
	op->c_yu = n;
	lyu = op->yu[n];
	op->ay = lyu->ax;
	op->dy = lyu->dx;
	if (op->y_unit != NULL)		free(op->y_unit);
	op->y_unit = Mystrdup(lyu->name);
	op->need_to_refresh = 1;
	return 0;
}
int	set_op_t_unit_set(O_p *op, int n)
{
	un_s *ltu = NULL;

	if (op == NULL)							return 1;
	if (n >= op->n_tu)						n = 0;
	if (n < 0)	n = 0;
	if (op->n_tu == 0 || op->tu == NULL)	return 1;
	op->c_tu = n;
	ltu = op->tu[n];
	op->at = ltu->ax;
	op->dt = ltu->dx;
	if (op->t_unit != NULL)		free(op->t_unit);
	op->t_unit = Mystrdup(ltu->name);
	op->need_to_refresh = 1;
	return 0;
}

int swap_x_y_in_plot(O_p *op)
{
	int i, j;
	int iopt;
	float tmp; // *ptmp
	char *s = NULL;
	un_s **u = NULL;
	d_s *ds = NULL;

	if (op == NULL) return 1;
	op->data_changing = 1;
	s = op->x_title;
	op->x_title = op->y_title;
	op->y_title = s;
	s = op->x_prime_title;
	op->x_prime_title = op->y_prime_title;
	op->y_prime_title = s;
	s = op->x_unit;
	op->x_unit = op->y_unit;
	op->y_unit = s;
	s = op->x_prefix;
	op->x_prefix = op->y_prefix;
	op->y_prefix = s;
	tmp = op->x_lo;
	op->x_lo = op->y_lo;
	op->y_lo = tmp;
	tmp = op->x_hi;
	op->x_hi = op->y_hi;
	op->y_hi = tmp;
	tmp = op->dx;
	op->dx = op->dy;
	op->dy = tmp;
	tmp = op->ax;
	op->ax = op->ay;
	op->ay = tmp;
	tmp = op->width;
	op->width = op->height;
	op->height = tmp;
	iopt = op->iopt & ~(XLOG + YLOG);
	if (op->iopt & XLOG)	iopt |= YLOG;
	if (op->iopt & YLOG)	iopt |= XLOG;
	op->iopt = iopt;
	iopt = op->iopt2 & ~(X_LIM | Y_LIM | X_NUM | Y_NUM);
	if (op->iopt2 & X_LIM)	iopt |= Y_LIM;
	if (op->iopt2 & Y_LIM)	iopt |= X_LIM;
	if (op->iopt2 & X_NUM)	iopt |= Y_NUM;
	if (op->iopt2 & Y_NUM)	iopt |= X_NUM;
	op->iopt2 = iopt;
	for (i=0 ; i< op->n_dat ; i++)
	{
		ds = op->dat[i];
		swap_ds_x_y(ds);
		/*		
		j = ds->nx;
		ds->nx = ds->ny;
		ds->ny = j;
		j = ds->mx;
		ds->mx = ds->my;
		ds->my = j;
		ptmp = ds->xd;
		ds->xd = ds->yd;
		ds->yd = ptmp;
		ptmp = ds->xe;
		ds->xe = ds->ye;
		ds->ye = ptmp;
		*/
	}
	j = op->c_xu;
	op->c_xu = op->c_yu;
	op->c_yu = j;
	j = op->c_xu_p;
	op->c_xu_p = op->c_yu_p;
	op->c_yu_p = j;
	j = op->n_xu;
	op->n_xu = op->n_yu;
	op->n_yu = j;
	j = op->m_xu;
	op->m_xu = op->m_yu;
	op->m_yu = j;
	u = op->xu;
	op->xu = op->yu;
	op->yu = u;
	set_op_x_unit_set(op, op->c_xu);
	set_op_y_unit_set(op, op->c_yu);
	op->data_changing = 0;
	op->need_to_refresh = 1;
	return 0;
}

/*	O_p *duplicate_plot_data(O_p *src, O_p *dest);
 *	DESCRIPTION	Copy the data of src one_plot to dest, if dest == NULL
 *			allocate space for the new one_plot
 *
 *
 *	RETURNS		the dest one_plot on success, NULL otherwise
 *
 */
O_p *duplicate_plot_data(O_p *src, O_p *dest)
{
	int i;
	d_s *dd = NULL;
	p_l *sp = NULL, *dp = NULL;
	un_s *us = NULL;

	if (src == NULL) 		return NULL;
	if (dest == NULL)
	{
		dest = (O_p *)calloc(1,sizeof(O_p));
		if (dest == NULL)		return NULL;
		init_one_plot(dest);
	}
	dest->data_changing = 1;
	dest->width = src->width; dest->height = src->height;
	dest->right = src->right; dest->up = src->up;
	dest->iopt = src->iopt; dest->iopt2 = src->iopt2;
	dest->tick_len = src->tick_len; dest->type = src->type;
	dest->tick_len_x = src->tick_len_x;dest->tick_len_y = src->tick_len_y;
	dest->x_lo = src->x_lo; dest->x_hi = src->x_hi;
	dest->y_lo = src->y_lo; dest->y_hi = src->y_hi;
	dest->ax = src->ax; dest->dx = src->dx;
	dest->ay = src->ay; dest->dy = src->dy;
	dest->title = Mystrdup(src->title);
	dest->x_title = Mystrdup(src->x_title);
	dest->y_title = Mystrdup(src->y_title);
	dest->x_prime_title = Mystrdup(src->x_prime_title);
	dest->y_prime_title = Mystrdup(src->y_prime_title);
	dest->x_prefix = Mystrdup(src->x_prefix);
	dest->y_prefix = Mystrdup(src->y_prefix);
	dest->t_prefix = Mystrdup(src->t_prefix);
	dest->x_unit = Mystrdup(src->x_unit);
	dest->y_unit = Mystrdup(src->y_unit);
	dest->t_unit = Mystrdup(src->t_unit);
	for (i=0 ; i< src->n_dat ; i++)
	{
		dd = duplicate_data_set(src->dat[i], NULL);
		if (dd == NULL)
		  {
		    dest->data_changing = 0;
		    return NULL;
		  }
		if (add_one_plot_data(dest, IS_DATA_SET, (void*)dd))
		  {
		    dest->data_changing = 0;
		    return NULL;
		  }
	}
	for (i=0 ; i< src->n_lab ; i++)
	{
		sp = src->lab[i];
		dp = (p_l*)calloc(1,sizeof(p_l));
		if ( dp == NULL )
		  {
		    dest->data_changing = 0;
		    return NULL;
		  }
		*dp = *sp;
		dp->text = Mystrdup(sp->text);
		if (add_one_plot_data(dest, IS_PLOT_LABEL, (void*)dp))
		  {
		    dest->data_changing = 0;
		    return NULL;
		  }
	}
	while (dest->n_xu > 0)
	{
		remove_one_plot_data(dest, IS_X_UNIT_SET, (void*)dest->xu[dest->n_xu-1]);
	};
	if (src->xu != NULL && src->n_xu > 0)
	{
		for (i=0 ; i< src->n_xu ; i++)
		{
			us = duplicate_unit_set(src->xu[i], NULL);
			add_data_to_one_plot(dest, IS_X_UNIT_SET, (void*)us);
		}
	}
	dest->c_xu = src->c_xu;
	dest->c_xu_p = src->c_xu_p;
	while (dest->n_yu > 0)
	{
		remove_one_plot_data(dest, IS_Y_UNIT_SET, (void*)dest->yu[dest->n_yu-1]);
	};
	if (src->yu != NULL && src->n_yu > 0)
	{
		for (i=0 ; i< src->n_yu ; i++)
		{
			us = duplicate_unit_set(src->yu[i], NULL);
			add_data_to_one_plot(dest, IS_Y_UNIT_SET, (void*)us);
		}
	}
	dest->c_yu = src->c_yu;
	dest->c_yu_p = src->c_yu_p;
	while (dest->n_tu > 0)
	{
		remove_one_plot_data(dest, IS_T_UNIT_SET, (void*)dest->tu[dest->n_tu-1]);
	};
	if (src->tu != NULL && src->n_tu > 0)
	{
		for (i=0 ; i< src->n_tu ; i++)
		{
			us = duplicate_unit_set(src->tu[i], NULL);
			add_data_to_one_plot(dest, IS_T_UNIT_SET, (void*)us);
		}
	}
	dest->c_tu = src->c_tu;
	dest->c_tu_p = src->c_tu_p;
	dest->filename = Transfer_filename(src->filename);
	dest->dir = Mystrdup(src->dir);
	dest->data_changing = 0;
	return dest;
}
int	affine_transform_all_plot_unitset(O_p *op, int type, float offset, float multiplier)
{
	int i;

	if (op != NULL && type == IS_X_UNIT_SET && op->xu != NULL && op->n_xu > 0)
	{
		for (i=0 ; i< op->n_xu ; i++)
		{
			op->xu[i]->ax += offset;
			op->xu[i]->dx *= multiplier;
		}
		op->ax += offset;
		op->dx *= multiplier;
	}
	else if (op != NULL && type == IS_Y_UNIT_SET && op->yu != NULL && op->n_yu > 0)
	{
		for (i=0 ; i< op->n_yu ; i++)
		{
			op->yu[i]->ax += offset;
			op->yu[i]->dx *= multiplier;
		}
		op->ay += offset;
		op->dy *= multiplier;
	}
	else return 1;
	return 0;
}

int	get_afine_param_from_op(O_p *op, int axis, float *ax, float *dx)
{
  un_s *un = NULL;

  if (op == NULL) return 1;
  if (axis == IS_X_UNIT_SET)      un = op->xu[op->c_xu];
  else if (axis == IS_Y_UNIT_SET) un = op->yu[op->c_yu];
  else return 1;
  return get_afine_param_from_unit(un, ax, dx);
}
int	switch_plot_data_set(O_p *op, int n_dat)
{
	if (op == NULL)		return -1;
	if (n_dat >= op->n_dat || n_dat < 0)	return 1;
	op->cur_dat =  n_dat;
	return 0;
}
int	uns_op_2_op(O_p *dest, const O_p *src)
{
	int i;

	i = uns_op_2_op_by_type(src, IS_X_UNIT_SET, dest, IS_X_UNIT_SET);
	i += uns_op_2_op_by_type(src, IS_Y_UNIT_SET, dest, IS_Y_UNIT_SET);
	i += uns_op_2_op_by_type(src, IS_T_UNIT_SET, dest, IS_T_UNIT_SET);
	return i;
}
int	uns_op_2_op_by_type(const O_p *ops, int ops_type, O_p *opd, int opd_type)
{
	int i = 0, j = 0;
	un_s *us = NULL;

	if (ops == NULL || opd == NULL)	return -1;
	if (opd_type ==  IS_X_UNIT_SET)
		while (opd->n_xu > 0)
			remove_one_plot_data(opd, IS_X_UNIT_SET, (void*)opd->xu[opd->n_xu-1]);
	else if (opd_type ==  IS_Y_UNIT_SET)
		while (opd->n_yu > 0)
			remove_one_plot_data(opd, IS_Y_UNIT_SET, (void*)opd->yu[opd->n_yu-1]);
	else if (opd_type ==  IS_T_UNIT_SET)
		while (opd->n_tu > 0)
			remove_one_plot_data(opd, IS_T_UNIT_SET, (void*)opd->tu[opd->n_tu-1]);
	else return -1;

	if (ops_type ==  IS_X_UNIT_SET && ops->xu != NULL && ops->n_xu > 0)
	{
		for (i=0 ; i< ops->n_xu ; i++)
		{
			us = duplicate_unit_set(ops->xu[i], NULL);
			add_data_to_one_plot(opd, opd_type, (void*)us);
		}
		j = ops->c_xu;
	}
	else if(ops_type ==  IS_Y_UNIT_SET && ops->yu != NULL && ops->n_yu > 0)
	{
		for (i=0 ; i< ops->n_yu ; i++)
		{
			us = duplicate_unit_set(ops->yu[i], NULL);
			add_data_to_one_plot(opd, opd_type, (void*)us);
		}
		j = ops->c_yu;
	}
	else if(ops_type ==  IS_T_UNIT_SET && ops->tu != NULL && ops->n_tu > 0)
	{
		for (i=0 ; i< ops->n_tu ; i++)
		{
			us = duplicate_unit_set(ops->tu[i], NULL);
			add_data_to_one_plot(opd, opd_type, (void*)us);
		}
		j = ops->c_tu;
	}
	else return -1;
	if (opd_type ==  IS_X_UNIT_SET)
	{
		opd->c_xu = j;
		set_op_x_unit_set(opd, opd->c_xu);
	}
	else	if (opd_type ==  IS_Y_UNIT_SET)
	{
		opd->c_yu = j;
		set_op_y_unit_set(opd, opd->c_yu);
	}
	else	if (opd_type ==  IS_T_UNIT_SET)
	{
		opd->c_tu = j;
		set_op_t_unit_set(opd, opd->c_tu);
	}
	return i;
}

int  add_new_point_to_op_and_ds(O_p *op, d_s *ds, float x, float y)
{
  int i, nxn, nyn;  // pb refresh ....




  if (ds == NULL || op == NULL)		return xvin_error(Wrong_Argument);
  op->data_changing = 1;
  for(nxn = ds->nx; ds->nx >= ds->mx; ds->mx += 16, nxn = ds->mx);
  for(nyn = ds->ny; ds->ny >= ds->my; ds->my += 16, nyn = ds->my);

  if (nxn > ds->nx || nyn > ds->ny)
    ds = build_adjust_data_set(ds, nxn, nyn);

  if (ds == NULL)
    {
      op->data_changing = 0;
      return xvin_error(Out_Of_Memory);
    }
  i = (ds->nx < ds->ny) ? ds->nx : ds->ny;
  ds->xd[i] = x; ds->yd[i] = y;
  ds->nx++; ds->ny++;
  op->data_changing = 0;
  op->need_to_refresh = 1;
  return 0;
}


# endif
