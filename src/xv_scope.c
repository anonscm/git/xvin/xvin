/*
 *    Example program for the Allegro library, by Shawn Hargreaves.
 *
 *    This program demonstrates how to use an offscree part of the 
 *    video memory to store source graphics for a hardware accelerated 
 *    graphics driver.
get_desktop_resolution(int *width, int *height);
 */

# ifndef _XV_MAIN_C_
# define _XV_MAIN_C_

# include <stdlib.h>
# include <string.h>

#include "platform.h"
#include "allegro.h"

#ifdef XV_WIN32
#include "winalleg.h"
#endif

#ifdef BUILDING_DLL
#include "xv_plugins.h"
#endif

#include "xvin.h"
#include "plot_op.h"
#include "gr_file.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "xv_main.h"
# include "im_mn.h"
# include "plot_mn.h"
# include "plot_reg_gui.h"
# include "imr_reg_gui.h"
# include "im_gr.h"
# include "gui_def.h"


int d_draw_Op_proc(int msg, DIALOG *d, int c);

int pltreadfile(O_p *op, char *file_name, int check);
int one_plot_to_box (O_p *o_p, struct box *b);
int draw_pr_box (struct box *b, int dx, int dy, int scale, int color);

int d_picoTeX_proc(int msg, DIALOG *d, int c);


//PXV_FUNC(int, dialog_set_magnets_rot, (char *rots));
//PXV_FUNC(int, dialog_set_magnets_z, (char *zmags));
//PXV_FUNC(int, dialog_set_magnets_z_to_force, (char *forces));
//PXV_FUNC(int, dialog_set_focus, (char *focs));
//PXV_FUNC(int, switch_motor_pid,(int state));
XV_FUNC(int,	trackBead_main, (int argc, char **argv));

extern int ttf_enable;
extern BITMAP *dev_bitmap;
extern char *debug_ttf;

// folowing has been commented out (NG, 2006-03-14) useless, defined in xv_plugins.h
// typedef int (*PL_MAIN)(int, char**); 

#include "example.h"
/* we need to load fonts.dat to access the big font */
DATAFILE *datafile;
BITMAP *plt_buffer = NULL;
int plt_buffer_used = 0;
int screen_used = 0;

char prog_dir[256] = "";

int  win_color_depth = 0;
int  win_screen_w = 0;
int  win_screen_h = 0;

# define MEDIUM_FONT 1
# define FONT_18PT 2

pltreg *pr_SCOPE = NULL;
O_p *op_SCOPE = NULL;

int scope_mode = 1;

int               eps_file_flag = 0;

FONT *original_font, *big_font = NULL, *font_18pt = NULL;


pltreg *find_pr_in_current_dialog(DIALOG *di);
extern pltreg *project[];
extern int    project_cur, project_n, project_m;

char amp_noise_value[128];
DIALOG *amp_noise_dialog = NULL;
float amp_noise_val = 0.01;

char zmag_label[128];
DIALOG *zmag_label_di = NULL;

char amp_sig_value[128];
DIALOG *amp_sig_dialog = NULL;
float amp_sig_val = 1;


char freq_sig_value[128];
DIALOG *freq_sig_dialog = NULL;
float freq_sig_val = 2000;

char focus_value[128];
DIALOG *focus_dialog = NULL;

char focus_label[128];
DIALOG *focus_label_di = NULL;

char T0_value[128];
DIALOG *T0_dialog = NULL;

char param_label[256];
DIALOG *param_label_di = NULL;


DIALOG *slider_di = NULL;

# ifdef PREVIOUS


DIALOG the_dialog[256] =
{
   /* (dialog proc)     (x)   (y)   (w)   (h)   (fg)  (bg)  (key) (flags)  (d1)  (d2)  (dp)              (dp2) (dp3) */
   { d_clear_proc,      0,    0,    900,  700,   255,  0,    0,    0,       0,    0,    NULL,             NULL, NULL  },
   { xvin_d_menu_proc,       0,    0,    0,     19,     0, 32,    0,    0,       0,    0,    plot_menu,             NULL, NULL  },
   { d_draw_Op_proc,    0,   19,  900,  668,   255,    0,    't',  0,       0,    0,    NULL,             NULL, NULL  },  
   { d_yield_proc,    0,   0,  0,  0,   255,    0,    't',  0,       0,    0,    NULL,             NULL, NULL  },
   { NULL,              0,    0,    0,    0,     0,    0,    0,    0,       0,    0,    NULL,             NULL, NULL  }
},

  (d_keyboard_proc,   0,    0,    0,    0,    0,    0,    27,      0,          0,             0,       do_quit,          NULL, NULL  };
  //   { d_keyboard_proc,   0,    0,    0,    0,    0,    0,    C('l'),  0,          0,             0,       do_load,           NULL, NULL  },
 

# endif




#ifdef FORTIFY

int check_all(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  Fortify_CheckAllMemory();
  return D_O_K;
}
int out_all(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  Fortify_OutputAllMemory();
  return D_O_K;
}
int enter_scope(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  return win_printf_OK("Scope number %d",Fortify_EnterScope());
}
int leave_scope(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  Fortify_LeaveScope();
  return D_O_K;
}
int dump_scope(void)
{
	int scope = 0;
	
  if(updating_menu_state != 0)	return D_O_K;
  if (win_scanf("scope number %d",&scope) == WIN_CANCEL)	return D_O_K; 
  Fortify_DumpAllMemory(scope);
  return D_O_K;
}



void fortify_error(const char *mes)
{
  static char *file = NULL;
  static int mesid = 0, respond_type = OK;
  FILE *fp = NULL;

  if (file == NULL)
    {
        file = my_sprintf(file,"%sfortify.log",prog_dir);
	fp = fopen(file,"w");
	if (fp != NULL)
	  {
	    fprintf(fp,"Starting\n");
	    fclose(fp);
	  }
    }
  if (mes != NULL)
    {
      fp = fopen(file,"a");
      if (fp != NULL)
	{
	  fprintf(fp,"%d : %s\n",mesid++,mes);
	  fclose(fp);
	}
    }
  if (respond_type == WIN_CANCEL && mes != NULL) return;
  if (mes) respond_type = win_printf("%s\n to get next message press OK to store them Cancel",mes);
  return;
}


int display_fortify_message(void)
{
  if(updating_menu_state != 0)	return D_O_K;
  fortify_error(NULL);
  return 0;  
}

int set_fortify_next(void)
{
  char all[256] = {0};
  FILE *fp = NULL;

   if(updating_menu_state != 0)	return D_O_K;
   snprintf(all,256,"%s%s",prog_dir,"fortify-on.txt");
   fp = fopen(all,"w");
   if (fp) 
     {
       fprintf(fp,"On\n");
       fclose(fp);
       return win_printf_OK("Your have activated the Fortify memory\nchecker for your next Xvin Session ");
     }
   else return win_printf_OK("Could not open file %s",backslash_to_slash(all));
   
}
int disable_fortify_next(void)
{
  char all[256] = {0};
   if(updating_menu_state != 0)	return D_O_K;
   snprintf(all,256,"%s%s",prog_dir,"fortify-on.txt");
   if (delete_file(all))
     return win_printf_OK("Could not delete file %s",backslash_to_slash(all));
   return win_printf_OK("Your have disabled the Fortify memory\nchecker for your next Xvin Session ");
}

MENU fortify_menu[32] =
{
	{"Enter scope",			enter_scope,  	NULL,	0,	NULL},	
	{"Leave scope",	                leave_scope, 	NULL,0,	NULL},
	{"Display messages",            display_fortify_message, 	NULL,0,	NULL},
	{"Dump Scope",		        dump_scope,  	NULL,	0,	NULL},	
	{"Check all",			check_all,  	NULL,	0,	NULL},	
	{"Dump all",	                out_all, 	NULL,0,	NULL},
	{"Disable Fortify",	        disable_fortify_next, 	NULL,0,	NULL},
	{NULL,                      NULL,       NULL,   0, NULL }
};






#endif	



#ifdef XV_WIN32 

int help_me_pico(void)
{
  char htmlhelp[512] = {0}, *c  = NULL;

  if(updating_menu_state != 0)	return D_O_K;
  snprintf(htmlhelp,512,prog_dir);
  c = strstr(htmlhelp,"bin");
  if (c) c[0] = 0;
  strcat(htmlhelp,"docs\\html\\picotw.html");

   ShellExecute(
        HWND_DESKTOP, //Parent window
        "open",       //Operation to perform
        htmlhelp,     //Path to program
        NULL,         //Parameters
        NULL,         //Default directory
        SW_SHOW);     //How to open

  return D_O_K;
}

int help_me_xv(void)
{
  char htmlhelp[512] = {0}, *c = NULL;

  if(updating_menu_state != 0)	return D_O_K;
  snprintf(htmlhelp,512,prog_dir);
  c = strstr(htmlhelp,"bin");
  if (c) c[0] = 0;
  strcat(htmlhelp,"docs\\html\\xvin.html");
  //spawnlp(P_NOWAIT,"c:\\Program Files\\Internet Explorer\\iexplore.exe","iexplore.exe",htmlhelp,NULL);
  //win_printf("file %s",backslash_to_slash(htmlhelp));

   ShellExecute(
        HWND_DESKTOP, //Parent window
        "open",       //Operation to perform
        htmlhelp,     //Path to program
        NULL,         //Parameters
        NULL,         //Default directory
        SW_SHOW);     //How to open

  return D_O_K;
}
int help_me_al(void)
{
  char htmlhelp[512] = {0}, *c = NULL;

  if(updating_menu_state != 0)	return D_O_K;
  snprintf(htmlhelp,512,prog_dir);
  c = strstr(htmlhelp,"bin");
  if (c) c[0] = 0;
  strcat(htmlhelp,"docs\\allegro\\html\\allegro.html");
  //  spawnlp(P_NOWAIT,"c:\\Program Files\\Internet Explorer\\iexplore.exe","iexplore.exe",htmlhelp,NULL);

   ShellExecute(
        HWND_DESKTOP, //Parent window
        "open",       //Operation to perform
        htmlhelp,     //Path to program
        NULL,         //Parameters
        NULL,         //Default directory
        SW_SHOW);     //How to open

  return D_O_K;
}


int help_me_pico_chm(void)
{
  char htmlhelp[512] = {0}, *c = NULL;

  if(updating_menu_state != 0)	return D_O_K;
  snprintf(htmlhelp,512,prog_dir);
  c = strstr(htmlhelp,"bin");
  if (c) c[0] = 0;
  strcat(htmlhelp,"docs\\chm\\picotw.chm");

   ShellExecute(
        HWND_DESKTOP, //Parent window
        "open",       //Operation to perform
        htmlhelp,     //Path to program
        NULL,         //Parameters
        NULL,         //Default directory
        SW_SHOW);     //How to open

  return D_O_K;
}


int help_me_pico_device_chm(void)
{
  char htmlhelp[512] = {0}, *c = NULL;

  if(updating_menu_state != 0)	return D_O_K;
  snprintf(htmlhelp,512,prog_dir);
  c = strstr(htmlhelp,"bin");
  if (c) c[0] = 0;
  strcat(htmlhelp,"docs\\chm\\picodv.chm");

   ShellExecute(
        HWND_DESKTOP, //Parent window
        "open",       //Operation to perform
        htmlhelp,     //Path to program
        NULL,         //Parameters
        NULL,         //Default directory
        SW_SHOW);     //How to open

  return D_O_K;
}

int help_me_pico_bio_chm(void)
{
  char htmlhelp[512] = {0}, *c = NULL;

  if(updating_menu_state != 0)	return D_O_K;
  snprintf(htmlhelp,512,prog_dir);
  c = strstr(htmlhelp,"bin");
  if (c) c[0] = 0;
  strcat(htmlhelp,"docs\\chm\\picobi.chm");

   ShellExecute(
        HWND_DESKTOP, //Parent window
        "open",       //Operation to perform
        htmlhelp,     //Path to program
        NULL,         //Parameters
        NULL,         //Default directory
        SW_SHOW);     //How to open

  return D_O_K;
}

int help_me_xv_chm(void)
{
  char htmlhelp[512] = {0}, *c = NULL;

  if(updating_menu_state != 0)	return D_O_K;
  snprintf(htmlhelp,512,prog_dir);
  c = strstr(htmlhelp,"bin");
  if (c) c[0] = 0;
  strcat(htmlhelp,"docs\\chm\\xvin.chm");
  //spawnlp(P_NOWAIT,htmlhelp,htmlhelp,NULL);
  //win_printf("file %s",backslash_to_slash(htmlhelp));

   ShellExecute(
        HWND_DESKTOP, //Parent window
        "open",       //Operation to perform
        htmlhelp,     //Path to program
        NULL,         //Parameters
        NULL,         //Default directory
        SW_SHOW);     //How to open

  return D_O_K;
}
int help_me_al_chm(void)
{
  char htmlhelp[512] = {0}, *c = NULL;

  if(updating_menu_state != 0)	return D_O_K;
  snprintf(htmlhelp,512,prog_dir);
  c = strstr(htmlhelp,"bin");
  if (c) c[0] = 0;
  strcat(htmlhelp,"docs\\allegro\\chm\\allegro.chm");
  //spawnlp(P_NOWAIT,htmlhelp,htmlhelp,NULL);

   ShellExecute(
        HWND_DESKTOP, //Parent window
        "open",       //Operation to perform
        htmlhelp,     //Path to program
        NULL,         //Parameters
        NULL,         //Default directory
        SW_SHOW);     //How to open

  return D_O_K;
}

MENU help_menu[32] = 
{ 
  //{ "Pico Html",          help_me_pico,        NULL,  0, NULL  },
   { "Software help (CHM)",	   help_me_pico_chm,    NULL,  0, NULL  },
   { "Harware help (CHM)",	   help_me_pico_device_chm,    NULL,  0, NULL  },
   { "Wet stuff help (CHM)",	   help_me_pico_bio_chm,    NULL,  0, NULL  },
   //  { "Xvin Html",          help_me_xv,        NULL,  0, NULL  },
   //{ "Xvin CHM",	   help_me_xv_chm,    NULL,  0, NULL  },
   // { "Allegro Html",       help_me_al,        NULL,  0, NULL  },
   //{ "Allegro CHM",        help_me_al_chm,    NULL,   0, NULL  },
   { NULL,               NULL,                NULL,       0, NULL  }
};

#endif





int    (*general_idle_action)(DIALOG *d);
int    (*general_end_action)(DIALOG *d);

/* custom dialog procedure for the clock object */
int general_idle(int msg, DIALOG *d, int c)
{
   /* process the message */
   switch (msg) {
      /* respond to idle messages */
      case MSG_IDLE:
	if (general_idle_action) general_idle_action(d);
	 break; 
      case MSG_END:
	if (general_end_action) general_end_action(d);
	 break; 
   }
   /* always return OK status, since the clock doesn't ever need to close 
    * the dialog or get the input focus.
    */
   return D_O_K;
}


int   change_param(DIALOG *d)
{
  if (d == amp_noise_dialog)        sscanf(d->dp,"%f",&amp_noise_val);
  else if (d == amp_sig_dialog)     sscanf(d->dp,"%f",&amp_sig_val);
  else if (d == freq_sig_dialog)    sscanf(d->dp,"%f",&freq_sig_val);
  return 0;
}

/* d_edit_proc:
 *  An editable text object (the dp field points to the string). When it
 *  has the input focus (obtained by clicking on it with the mouse), text
 *  can be typed into this object. The d1 field specifies the maximum
 *  number of characters that it will accept, and d2 is the text cursor 
 *  position within the string.
 */
int d_my_edit_proc(int msg, DIALOG *d, int c)
{
   static int ignore_next_uchar = FALSE;
   BITMAP *gui_bmp = NULL;
   int last_was_space, new_pos, i, k;
   int f, l, p, w, x, fg, b, scroll;
   char buf[16] = {0};
   char *s = NULL, *t = NULL;
   FONT *oldfont = font;


   ASSERT(d);
   if (d->dp2) 	 font = d->dp2;
   
   gui_bmp = gui_get_screen();

   s = d->dp;
   l = ustrlen(s);
   if (d->d2 > l) 
      d->d2 = l;

   /* calculate maximal number of displayable characters */
   if (d->d2 == l)  {
      usetc(buf+usetc(buf, ' '), 0);
      x = text_length(font, buf);
   }
   else
      x = 0;

   b = 0;

   for (p=d->d2; p>=0; p--) {
      usetc(buf+usetc(buf, ugetat(s, p)), 0);
      x += text_length(font, buf);
      b++;
      if (x > d->w) 
	 break;
   }

   if (x <= d->w) {
      b = l; 
      scroll = FALSE;
   }
   else {
      b--; 
      scroll = TRUE;
   }

   switch (msg) {

      case MSG_START:
	 d->d2 = l;
	 break;

      case MSG_DRAW:
	 fg = (d->flags & D_DISABLED) ? gui_mg_color : d->fg;
	 x = 0;

	 if (scroll) {
	    p = d->d2-b+1; 
	    b = d->d2; 
	 }
	 else 
	    p = 0; 

	 for (; p<=b; p++) {
	    f = ugetat(s, p);
	    usetc(buf+usetc(buf, (f) ? f : ' '), 0);
	    w = text_length(font, buf);
	    if (x+w > d->w)
	       break;
	    f = ((p == d->d2) && (d->flags & D_GOTFOCUS));
	    textout_ex(gui_bmp, font, buf, d->x+x, d->y, (f) ? d->bg : fg, (f) ? fg : d->bg);
	    x += w;
	 }
	 if (x < d->w)
	    rectfill(gui_bmp, d->x+x, d->y, d->x+d->w-1, d->y+text_height(font)-1, d->bg);
	 break;

      case MSG_CLICK:
	 x = d->x;

	 if (scroll) {
	    p = d->d2-b+1; 
	    b = d->d2; 
	 }
	 else
	    p = 0; 

	 for (; p<b; p++) {
	    usetc(buf+usetc(buf, ugetat(s, p)), 0);
	    x += text_length(font, buf);
	    if (x > gui_mouse_x()) 
	       break;
	 }
	 d->d2 = MID(0, p, l);
	 object_message(d, MSG_DRAW, 0);
	 break;

      case MSG_WANTFOCUS:
      case MSG_LOSTFOCUS:
      case MSG_KEY:
	font = oldfont;
	return D_WANTFOCUS;

      case MSG_CHAR:
	 ignore_next_uchar = FALSE;

	 if ((c >> 8) == KEY_LEFT) {
	    if (d->d2 > 0) {
	       if (key_shifts & KB_CTRL_FLAG) {
		  last_was_space = TRUE;
		  new_pos = 0;
		  t = s;
		  for (i = 0; i < d->d2; i++) {
		     k = ugetx(&t);
		     if (uisspace(k))
			last_was_space = TRUE;
		     else if (last_was_space) {
			last_was_space = FALSE;
			new_pos = i;
		     }
		  }
		  d->d2 = new_pos;
	       }
	       else
		  d->d2--;
	    }
	 }
	 else if ((c >> 8) == KEY_RIGHT) {
	    if (d->d2 < l) {
	       if (key_shifts & KB_CTRL_FLAG) {
		  t = s + uoffset(s, d->d2);
		  for (k = ugetx(&t); uisspace(k); k = ugetx(&t))
		     d->d2++;
		  for (; k && !uisspace(k); k = ugetx(&t))
		     d->d2++;
	       }
	       else
		  d->d2++;
	    }
	 }
	 else if ((c >> 8) == KEY_HOME) {
	    d->d2 = 0;
	 }
	 else if ((c >> 8) == KEY_END) {
	    d->d2 = l;
	 }
	 else if ((c >> 8) == KEY_DEL) {
	    if (d->d2 < l)
	       uremove(s, d->d2);
	 }
	 else if ((c >> 8) == KEY_BACKSPACE) {
	    if (d->d2 > 0) {
	       d->d2--;
	       uremove(s, d->d2);
	    }
	 }
	 else if ((c >> 8) == KEY_ENTER) {
	    if (d->flags & D_EXIT) {
	       object_message(d, MSG_DRAW, 0);
	       font = oldfont;
	       return D_CLOSE;
	    }
	    else
	      font = oldfont;
	    change_param(d);
	    return D_O_K;
	 }
	 else if ((c >> 8) == KEY_TAB) {
	    ignore_next_uchar = TRUE;
	    font = oldfont;
	    return D_O_K;
	 }
	 else {
	    /* don't process regular keys here: MSG_UCHAR will do that */
	    break;
	 }
	 object_message(d, MSG_DRAW, 0);
	 font = oldfont;
	 return D_USED_CHAR;

      case MSG_UCHAR:
	 if ((c >= ' ') && (uisok(c)) && (!ignore_next_uchar)) {
	    if (l < d->d1) {
	       uinsert(s, d->d2, c);
	       d->d2++;

	       object_message(d, MSG_DRAW, 0);
	    }
	    font = oldfont;
	    return D_USED_CHAR;
	 }
	 break;
   }
   font = oldfont;
   return D_O_K;
}


void dotted_rect(int x1, int y1, int x2, int y2, int fg, int bg)
{
   BITMAP *gui_bmp = gui_get_screen();
   int x = ((x1+y1) & 1) ? 1 : 0;
   int c;

   /* two loops to avoid bank switches */
   for (c=x1; c<=x2; c++)
      putpixel(gui_bmp, c, y1, (((c+y1) & 1) == x) ? fg : bg);
   for (c=x1; c<=x2; c++)
      putpixel(gui_bmp, c, y2, (((c+y2) & 1) == x) ? fg : bg);

   for (c=y1+1; c<y2; c++) {
      putpixel(gui_bmp, x1, c, (((c+x1) & 1) == x) ? fg : bg);
      putpixel(gui_bmp, x2, c, (((c+x2) & 1) == x) ? fg : bg);
   }
}




/* d_button_proc:
 *  A button object (the dp field points to the text string). This object
 *  can be selected by clicking on it with the mouse or by pressing its 
 *  keyboard shortcut. If the D_EXIT flag is set, selecting it will close 
 *  the dialog, otherwise it will toggle on and off.
 */
int my_d_button_proc(int msg, DIALOG *d, int c)
{
   BITMAP *gui_bmp = NULL;
   int state1, state2;
   int black;
   int swap;
   int g;
   ASSERT(d);
   
   gui_bmp = gui_get_screen();

   switch (msg) {

      case MSG_DRAW:
	 if (d->flags & D_SELECTED) {
	    g = 1;
	    state1 = d->bg;
	    state2 = (d->flags & D_DISABLED) ? gui_mg_color : d->fg;
	    state1 = makecol32(255, 255, 255);
	    state2 = makecol32(180, 0, 0);
	    scope_mode = 0;
	    //switch_motors_pid(0);
	 }
	 else {
	    g = 0;
	    state1 = (d->flags & D_DISABLED) ? gui_mg_color : d->fg;
	    state2 = d->bg;
	    state1 = makecol32(255, 255, 255);
	    state2 = makecol32(0, 180, 0);
	    scope_mode = 1;
	    //switch_motors_pid(1);
	 }

	 rectfill(gui_bmp, d->x+1+g, d->y+1+g, d->x+d->w-3+g, d->y+d->h-3+g, state2);
	 rect(gui_bmp, d->x+g, d->y+g, d->x+d->w-2+g, d->y+d->h-2+g, state1);
	 if (d->flags & D_SELECTED)
	   gui_textout_ex(gui_bmp, "Stop", d->x+d->w/2+g, d->y+d->h/2-text_height(font)/2+g, state1, -1, TRUE);
	 else
	   gui_textout_ex(gui_bmp, "Run", d->x+d->w/2+g, d->y+d->h/2-text_height(font)/2+g, state1, -1, TRUE);


	 if (d->flags & D_SELECTED) {
	    vline(gui_bmp, d->x, d->y, d->y+d->h-2, d->bg);
	    hline(gui_bmp, d->x, d->y, d->x+d->w-2, d->bg);
	 }
	 else {
	    black = makecol(0,0,0);
	    vline(gui_bmp, d->x+d->w-1, d->y+1, d->y+d->h-2, black);
	    hline(gui_bmp, d->x+1, d->y+d->h-1, d->x+d->w-1, black);
	 }
	 if ((d->flags & D_GOTFOCUS) && 
	     (!(d->flags & D_SELECTED) || !(d->flags & D_EXIT)))
	    dotted_rect(d->x+1+g, d->y+1+g, d->x+d->w-3+g, d->y+d->h-3+g, state1, state2);
	 break;

      case MSG_WANTFOCUS:
	 return D_WANTFOCUS;

      case MSG_KEY:
	 /* close dialog? */
	 if (d->flags & D_EXIT) {
	    return D_CLOSE;
	 }

	 /* or just toggle */
	 d->flags ^= D_SELECTED;
	 object_message(d, MSG_DRAW, 0);
	 break;

      case MSG_CLICK:
	 /* what state was the button originally in? */
	 state1 = d->flags & D_SELECTED;
	 if (d->flags & D_EXIT)
	    swap = FALSE;
	 else
	    swap = state1;

	 /* track the mouse until it is released */
	 while (gui_mouse_b()) {
	    state2 = ((gui_mouse_x() >= d->x) && (gui_mouse_y() >= d->y) &&
		      (gui_mouse_x() < d->x + d->w) && (gui_mouse_y() < d->y + d->h));
	    if (swap)
	       state2 = !state2;

	    /* redraw? */
	    if (((state1) && (!state2)) || ((state2) && (!state1))) {
	       d->flags ^= D_SELECTED;
	       state1 = d->flags & D_SELECTED;
	       object_message(d, MSG_DRAW, 0);
	    }

	    /* let other objects continue to animate */
	    broadcast_dialog_message(MSG_IDLE, 0);
	 }

	 /* should we close the dialog? */
	 if ((d->flags & D_SELECTED) && (d->flags & D_EXIT)) {
	    d->flags ^= D_SELECTED;
	    return D_CLOSE;
	 }
	 break; 
   }

   return D_O_K;
}



/* d_slider_proc:
 *  A slider control object. This object returns a value in d2, in the
 *  range from 0 to d1&0xFFFF. It will display as a vertical slider if h is
 *  greater than or equal to w; otherwise, it will display as a horizontal
 *  slider. dp can contain an optional bitmap to use for the slider handle;
 *  the width of the slider may be set in (d1&0xFFFF0000)>>16  
 *  dp2 can contain an optional callback function, which is called each 
 *  time d2 changes. The callback function should have the following
 *  prototype:
 *
 *  int function(void *dp3, int d2);
 *
 *  The d_slider_proc object will return the value of the callback function.
 */
int my_d_slider_proc(int msg, DIALOG *d, int c)
{
   BITMAP *gui_bmp = gui_get_screen();
   BITMAP *slhan = NULL;
   int oldpos, newpos;
   int sfg;                /* slider foreground color */
   int vert = TRUE;        /* flag: is slider vertical? */
   int hh = 7;             /* handle height (width for horizontal sliders) */
   int hmar;               /* handle margin */
   int slp;                /* slider position */
   int mp;                 /* mouse position */
   int irange;
   int slx, sly, slh, slw;
   int msx, msy;
   int retval = D_O_K;
   int upkey, downkey;
   int pgupkey, pgdnkey;
   int homekey, endkey;
   int delta;
   fixed slratio, slmax, slpos;
   int (*proc)(void *cbpointer, int d2value);
   int oldval;
   ASSERT(d);

   /* check for slider direction */
   if (d->h < d->w)
      vert = FALSE;

   irange = (vert) ? d->h : d->w;
   /* set up the metrics for the control */
   if (d->dp != NULL) {
      slhan = (BITMAP *)d->dp;
      if (vert)
	 hh = slhan->h;
      else
	 hh = slhan->w;
   }
   else
     {
       hh = (d->d1&0xFFFF0000)>> 16;
       hh = (hh < irange && hh >= 7) ? hh : 7;
     }
   
   hmar = hh/2;
   irange = (vert) ? d->h : d->w;
   slmax = itofix(irange-hh);
   slratio = slmax / ((d->d1&0xFFFF));
   slpos = slratio * d->d2;
   slp = fixtoi(slpos);

   switch (msg) {

      case MSG_DRAW:
	 sfg = (d->flags & D_DISABLED) ? gui_mg_color : d->fg;

	 if (vert) {
	    rectfill(gui_bmp, d->x, d->y, d->x+d->w/2-2, d->y+d->h-1, d->bg);
	    rectfill(gui_bmp, d->x+d->w/2-1, d->y, d->x+d->w/2+1, d->y+d->h-1, sfg);
	    rectfill(gui_bmp, d->x+d->w/2+2, d->y, d->x+d->w-1, d->y+d->h-1, d->bg);
	 }
	 else {
	    rectfill(gui_bmp, d->x, d->y, d->x+d->w-1, d->y+d->h/2-2, d->bg);
	    rectfill(gui_bmp, d->x, d->y+d->h/2-1, d->x+d->w-1, d->y+d->h/2+1, sfg);
	    rectfill(gui_bmp, d->x, d->y+d->h/2+2, d->x+d->w-1, d->y+d->h-1, d->bg);
	 }

	 /* okay, background and slot are drawn, now draw the handle */
	 if (slhan) {
	    if (vert) {
	       slx = d->x+(d->w/2)-(slhan->w/2);
	       sly = d->y+(d->h-1)-(hh+slp);
	    } 
	    else {
	       slx = d->x+slp;
	       sly = d->y+(d->h/2)-(slhan->h/2);
	    }
	    draw_sprite(gui_bmp, slhan, slx, sly);
	 } 
	 else {
	    /* draw default handle */
	    if (vert) {
	       slx = d->x;
	       sly = d->y+(d->h)-(hh+slp);
	       slw = d->w-1;
	       slh = hh-1;
	    }
            else {
	       slx = d->x+slp;
	       sly = d->y;
	       slw = hh-1;
	       slh = d->h-1;
	    }

	    /* draw body */
	    rectfill(gui_bmp, slx+2, sly, slx+(slw-2), sly+slh, sfg);
	    vline(gui_bmp, slx+1, sly+1, sly+slh-1, sfg);
	    vline(gui_bmp, slx+slw-1, sly+1, sly+slh-1, sfg);
	    vline(gui_bmp, slx, sly+2, sly+slh-2, sfg);
	    vline(gui_bmp, slx+slw, sly+2, sly+slh-2, sfg);
	    vline(gui_bmp, slx+1, sly+2, sly+slh-2, d->bg);
	    hline(gui_bmp, slx+2, sly+1, slx+slw-2, d->bg);
	    putpixel(gui_bmp, slx+2, sly+2, d->bg);
	 }

	 if (d->flags & D_GOTFOCUS)
	    dotted_rect(d->x, d->y, d->x+d->w-1, d->y+d->h-1, sfg, d->bg);
	 break;

      case MSG_WANTFOCUS:
      case MSG_LOSTFOCUS:
	 return D_WANTFOCUS;

      case MSG_KEY:
	 if (!(d->flags & D_GOTFOCUS))
	    return D_WANTFOCUS;
	 else
	    return D_O_K;

      case MSG_CHAR:
	 /* handle movement keys to move slider */
	 c >>= 8;

	 if (vert) {
	    upkey = KEY_UP;
	    downkey = KEY_DOWN;
	    pgupkey = KEY_PGUP;
	    pgdnkey = KEY_PGDN;
	    homekey = KEY_END;
	    endkey = KEY_HOME;
	 } 
	 else {
	    upkey = KEY_RIGHT;
	    downkey = KEY_LEFT;
	    pgupkey = KEY_PGDN;
	    pgdnkey = KEY_PGUP;
	    homekey = KEY_HOME;
	    endkey = KEY_END;
	 }

	 if (c == upkey)
	    delta = 1;
	 else if (c == downkey)
	    delta = -1;
	 else if (c == pgdnkey)
	    delta = -d->d1 / 16;
	 else if (c == pgupkey)
	    delta = d->d1 / 16;
	 else if (c == homekey)
	    delta = -d->d2;
	 else if (c == endkey)
	    delta = d->d1 - d->d2;
	 else
	    delta = 0;

	 if (delta) {
	    oldpos = slp;
	    oldval = d->d2;

	    while (1) {
	       d->d2 = d->d2+delta;
	       slpos = slratio*d->d2;
	       slp = fixtoi(slpos);
	       if ((slp != oldpos) || (d->d2 <= 0) || (d->d2 >= d->d1))
		  break;
	    }

	    if (d->d2 < 0)
	       d->d2 = 0;
	    if (d->d2 > d->d1)
	       d->d2 = d->d1;

	    retval = D_USED_CHAR;

	    if (d->d2 != oldval) {
	       /* call callback function here */
	       if (d->dp2) {
		  proc = d->dp2;
		  retval |= (*proc)(d->dp3, d->d2);
	       }

	       object_message(d, MSG_DRAW, 0);
	    }
	 }
	 break;
      
      case MSG_WHEEL: 
	 oldval = d->d2;
	 d->d2 = MID(0, d->d2+c, d->d1);
	 if (d->d2 != oldval) {
	    /* call callback function here */
	    if (d->dp2) {
	       proc = d->dp2;
	       retval |= (*proc)(d->dp3, d->d2);
	    }

	    object_message(d, MSG_DRAW, 0);
	 }
	 break;

      case MSG_CLICK:
	 /* track the mouse until it is released */
	 mp = slp;

	 while (gui_mouse_b()) {
	    msx = gui_mouse_x();
	    msy = gui_mouse_y();
	    oldval = d->d2;
	    if (vert)
	       mp = (d->y+d->h-hmar)-msy;
	    else
	       mp = msx-(d->x+hmar);
	    if (mp < 0)
	       mp = 0;
	    if (mp > irange-hh)
	       mp = irange-hh;
	    slpos = itofix(mp);
	    slmax = fixdiv(slpos, slratio);
	    newpos = fixtoi(slmax);
	    if (newpos != oldval) {
	       d->d2 = newpos;

	       /* call callback function here */
	       if (d->dp2 != NULL) {
		  proc = d->dp2;
		  retval |= (*proc)(d->dp3, d->d2);
	       }

	       object_message(d, MSG_DRAW, 0);
	    }

	    /* let other objects continue to animate */
	    broadcast_dialog_message(MSG_IDLE, 0);
	 }
	 break;
   }

   return retval;
}


DIALOG *get_time_slider(void)
{
  return slider_di;
}


XV_FUNC(int,    do_load, (void));
int    generate_plot_dialog(int w, int h)
{
    DIALOG* di;
    int xpos = 0;
//    int i;
    
    di = attach_new_item_to_dialog(d_clear_proc, 0, 0, 0, 0);
    if (di == NULL)    
    {
        allegro_message("dialog pb");
        return 1;
    }
    set_dialog_size_and_color(di, 0, 0, 900, 700, 0, 0);
    
    
    di = attach_new_item_to_dialog(xvin_d_menu_proc, 0, 0, 0, 0);
    if (di == NULL)    
    {
        allegro_message("dialog pb");
        return 1;
    }
    set_dialog_size_and_color(di, 0, 0, 0, 19, 0, 32);
    set_dialog_properties(di, plot_menu, NULL, NULL);


    di = attach_new_item_to_dialog(d_draw_Op_proc, 0, 0, 0, 0);
    if (di == NULL)    
    {
        allegro_message("dialog pb");
        return 1;
    }
    set_dialog_size_and_color(di, 0, 38, 900, 668, 255, 0); //19

    xpos = 0;
    di = attach_new_item_to_dialog(my_d_button_proc, 0, 0, 127, 0);
    if (di == NULL)    
    {
        allegro_message("dialog pb");
        return 1;
    }


    set_dialog_size_and_color(di, xpos, 19, 40, 19,  makecol32(0, 0, 0), makecol32(0, 180, 0));
    set_dialog_properties(di, "Stop", font_18pt, NULL);
    xpos = 40;


    di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);
    if (di == NULL)    
    {
        allegro_message("dialog pb");
        return 1;
    }
    set_dialog_size_and_color(di, xpos, 19, 160, 19,  makecol32(255, 40, 0), makecol32(180, 180, 180));
    snprintf(zmag_label,128,"Amplitude sig. =");
    set_dialog_properties(di, zmag_label, font_18pt, NULL);
    zmag_label_di = di;


    snprintf(amp_sig_value,128,"0");
    di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);
    if (di == NULL)    
    {
        allegro_message("dialog pb");
        return 1;
    }
    //set_dialog_size_and_color(di, xpos+40, 0, 80, 19,  makecol32(225, 40, 0), makecol32(180, 180, 180));
    xpos += 160;
    set_dialog_size_and_color(di, xpos, 19, 80, 19,  makecol32(40, 225, 40), makecol32(180, 180, 180));
    sprintf(amp_sig_value,"%g",amp_sig_val);
    set_dialog_properties(di, amp_sig_value,  font_18pt, NULL);

    amp_sig_dialog = di;



    di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);
    if (di == NULL)    
    {
        allegro_message("dialog pb");
        return 1;
    }
    xpos += 80;
    set_dialog_size_and_color(di, xpos, 19, 200, 19,  makecol32(255, 40, 0), makecol32(180, 180, 180));
    set_dialog_properties(di, "Frequence sig. =", font_18pt, NULL);


    snprintf(freq_sig_value,128,"0");
    di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);
    if (di == NULL)    
    {
        allegro_message("dialog pb");
        return 1;
    }
    xpos += 200;
    set_dialog_size_and_color(di, xpos, 19, 80, 19,  makecol32(225, 40, 0), makecol32(180, 180, 180));
    sprintf(freq_sig_value,"%g",freq_sig_val);
    set_dialog_properties(di, freq_sig_value,  font_18pt, NULL);

    freq_sig_dialog = di;


    di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);
    if (di == NULL)    
    {
        allegro_message("dialog pb");
        return 1;
    }
    xpos += 80;
    set_dialog_size_and_color(di, xpos, 19, 120, 19,  makecol32(255, 40, 0), makecol32(180, 180, 180));
    set_dialog_properties(di, "Amplitude Bruit =", font_18pt, NULL);


    snprintf(amp_noise_value,128,"0");
    di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);
    if (di == NULL)    
    {
        allegro_message("dialog pb");
        return 1;
    }
    xpos += 120;
    set_dialog_size_and_color(di, xpos,0, 80, 19,  makecol32(225, 40, 0), makecol32(180, 180, 180));
    sprintf(amp_noise_value,"%g",amp_noise_val);
    set_dialog_properties(di, amp_noise_value,  font_18pt, NULL);

    amp_noise_dialog = di;



    //if ((w - 4*120 - 130) > 680)
    //{
	di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);
	if (di == NULL)    
	  {
	    allegro_message("dialog pb");
	    return 1;
	  }
	//xpos =  w - 120 - 130;
	xpos += 80;
	set_dialog_size_and_color(di, xpos, 19, 60, 19,  makecol32(255, 40, 0), makecol32(180, 180, 180));
	snprintf(focus_label,128,"F. echan. =");
	set_dialog_properties(di, focus_label, font_18pt, NULL);
	focus_label_di = di;

	snprintf(focus_value,128,"0");
	di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);
	if (di == NULL)    
	  {
	    allegro_message("dialog pb");
	    return 1;
	  }
	xpos += 60;
	set_dialog_size_and_color(di, xpos, 19, 100, 19,  makecol32(225, 40, 0), makecol32(180, 180, 180));
	set_dialog_properties(di, focus_value,  font_18pt, NULL);

	focus_dialog = di;

	di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);
	if (di == NULL)    
	  {
	    allegro_message("dialog pb");
	    return 1;
	  }
	//xpos =  w - 120;
	xpos += 100;
	set_dialog_size_and_color(di, xpos, 19, 80, 19,  makecol32(255, 40, 0), makecol32(180, 180, 180));
	set_dialog_properties(di, "Taille de l'echan. =", font_18pt, NULL);


	snprintf(T0_value,128,"0");
	di = attach_new_item_to_dialog(d_my_edit_proc, 0, 0, 127, 0);
	if (di == NULL)    
	  {
	    allegro_message("dialog pb");
	    return 1;
	  }
	xpos += 80;
	set_dialog_size_and_color(di, xpos, 19, 80, 19,  makecol32(225, 40, 0), makecol32(180, 180, 180));
	set_dialog_properties(di, T0_value,  font_18pt, NULL);

	T0_dialog = di;


	/*

	di = attach_new_item_to_dialog(d_text_proc, 0, 0, 127, 0);
	if (di == NULL)    
	  {
	    allegro_message("dialog pb");
	    return 1;
	  }
	//xpos =  w - 120 - 130;
	xpos += 80;
	set_dialog_size_and_color(di, xpos, 19, w - xpos - 80, 19,  makecol32(255, 40, 0), makecol32(180, 180, 180));
	snprintf(param_label,128," ");
	set_dialog_properties(di, param_label, font_18pt, NULL);
	param_label_di = di;

	*/


	di = attach_new_item_to_dialog(my_d_slider_proc, 0, 0,  1024, 512);
	if (di == NULL)    
	  {
	    allegro_message("dialog pb");
	    return 1;
	  }
	xpos += 90;
	set_dialog_size_and_color(di, xpos, 19, w - xpos - 10, 19,  makecol32(255, 40, 0), makecol32(180, 180, 180));
	snprintf(param_label,128," ");
	set_dialog_properties(di, NULL, NULL, NULL);
	slider_di = di;



	//}




    di = attach_new_item_to_dialog(d_yield_proc, 0, 0, 0, 0);
    if (di == NULL)    
    {
        allegro_message("dialog pb");
        return 1;
    }
/*    i = retrieve_item_from_dialog(d_draw_Op_proc, NULL);
    allegro_message("plot in %d",i); */


    di = attach_new_item_to_dialog(d_keyboard_proc, 12, 0, 0, 0);
    if (di == NULL)    
    {
        allegro_message("dialog pb");
        return 1;
    }
    set_dialog_properties(di, do_load, NULL, NULL);

    di = attach_new_item_to_dialog(general_idle, 0, 0, 0, 0);
    if (di == NULL)    
    {
        allegro_message("dialog pb");
        return 1;
    }
    set_dialog_size_and_color(di, 0, 0, 0, 0, 0, 0);
    
    return 0;

}

int change_amp_noise_string(float rot)
{
  snprintf(amp_noise_value,128,"%g",rot);
  if (amp_noise_dialog) amp_noise_dialog->proc(MSG_DRAW, amp_noise_dialog, 0);
  return 0;
}


int change_amp_sig_string(float rot)
{
  snprintf(amp_sig_value,128,"%g",rot);
  if (amp_sig_dialog) amp_sig_dialog->proc(MSG_DRAW, amp_sig_dialog, 0);
  return 0;
}


int change_zmag_label_string(char *label, int fg, int bg, int mode)
{
  static int nfg = 0, nbg = 0, uptodate = 0;
  static char ntext[128];

  if (zmag_label_di == NULL)    return 1;
  if (mode == 0)
    {
      uptodate = 0;
      nfg = fg;
      nbg = bg;
      if (label == NULL) ntext[0] = 0;
      else snprintf(ntext,128,"%s",label);
      return 0;
    }
  else if (uptodate == 0)  // we really display stuff
    {
      if (ntext[0] != 0)  
	{
	  zmag_label_di->fg = zmag_label_di->bg; // we erase text
	  if (zmag_label_di) zmag_label_di->proc(MSG_DRAW, zmag_label_di, 0);
	  snprintf(zmag_label,128,"%s",ntext);
	  zmag_label_di->fg = nfg;
	  zmag_label_di->bg = nbg;	
	}
      else   
	{
	  zmag_label_di->fg = zmag_label_di->bg; // we erase text
	  if (zmag_label_di) zmag_label_di->proc(MSG_DRAW, zmag_label_di, 0);
	  snprintf(zmag_label,128,"Magnet Z (mn)");
	  zmag_label_di->fg = makecol32(0, 0, 0);
	  zmag_label_di->bg = makecol32(240, 240, 240);
	}
      if (zmag_label_di) zmag_label_di->proc(MSG_DRAW, zmag_label_di, 0);
      uptodate = 1;
    }
  return 0;
}


int change_focus_label_string(char *label, int fg, int bg, int mode)
{
  static int nfg = 0, nbg = 0, uptodate = 0;
  static char ntext[128] = {0};

  if (focus_label_di == NULL)   return 1;
  if (mode == 0)
    {
      uptodate = 0;
      nfg = fg;
      nbg = bg;
      if (label == NULL) ntext[0] = 0;
      else snprintf(ntext,128,"%s",label);
      return 0;
    }
  else if (uptodate == 0)  // we really display stuff
    {
      if (ntext[0] != 0)  
	{
	  focus_label_di->fg = focus_label_di->bg; // we erase text
	  if (focus_label_di) focus_label_di->proc(MSG_DRAW, focus_label_di, 0);
	  snprintf(focus_label,128,"%s",ntext);
	  focus_label_di->fg = nfg;
	  focus_label_di->bg = nbg;	
	}
      else   
	{
	  focus_label_di->fg = focus_label_di->bg = makecol32(240, 240, 240); // we erase text
	  if (focus_label_di) focus_label_di->proc(MSG_DRAW, focus_label_di, 0);
	  snprintf(focus_label,128,"Focus =");
	  focus_label_di->fg = makecol32(0, 0, 0);
	  focus_label_di->bg = makecol32(240, 240, 240);
	}
      if (focus_label_di) focus_label_di->proc(MSG_DRAW, focus_label_di, 0);
      uptodate = 1;
    }
  return 0;
}


int change_param_label_string(char *label, int fg, int bg, int mode)
{
  static int nfg = 0, nbg = 0, uptodate = 0;
  static char ntext[128] = {0};

  if (param_label_di == NULL)  return 1;
  if (mode == 0)
    {
      uptodate = 0;
      nfg = fg;
      nbg = bg;
      if (label == NULL) ntext[0] = 0;
      else snprintf(ntext,128,"%s",label);
      return 0;
    }
  else if (uptodate == 0)  // we really display stuff
    {
      if (ntext[0] != 0)  
	{
	  param_label_di->fg = param_label_di->bg;
	  if (param_label_di) param_label_di->proc(MSG_DRAW, param_label_di, 0);
	  snprintf(param_label,128,"%s",ntext);
	  param_label_di->fg = nfg;
	  param_label_di->bg = nbg;	
	}
      else   
	{
	  param_label_di->fg = param_label_di->bg = makecol32(240, 240, 240);
	  if (param_label_di) param_label_di->proc(MSG_DRAW, param_label_di, 0);
	  snprintf(param_label,128," ");
	  param_label_di->fg = makecol32(0, 0, 0);
	  param_label_di->bg = makecol32(240, 240, 240);
	}
      if (param_label_di->proc) param_label_di->proc(MSG_DRAW, param_label_di, 0);
      uptodate = 1;
    }
  return 0;
}



int change_freq_sig_string(float force)
{
  snprintf(freq_sig_value,128,"%g",force);
  if (freq_sig_dialog) freq_sig_dialog->proc(MSG_DRAW, freq_sig_dialog, 0);
  return 0;
}

int change_focus_string(float focus)
{
  snprintf(focus_value,128,"%6.3f",focus);
  if (focus_dialog) focus_dialog->proc(MSG_DRAW, focus_dialog, 0);
  //else win_printf("No focus dialog %g",focus);
  return 0;
}

int change_T0_string(float T0)
{
  snprintf(T0_value,128,"%6.3f",T0);
  if (T0_dialog) T0_dialog->proc(MSG_DRAW, T0_dialog, 0);
  return 0;
}


static void close_hook(void)
{  
   simulate_keypress(27 + (KEY_ESC << 8));   
}

void    restore_screen(void)
{
//    win_printf("restore");
  screen_restoring = 1;
  // allegro_message("restoring about to proceed");
  #ifdef XV_WIN32
  broadcast_dialog_message(MSG_DRAW,0);   
  #endif
  screen_restoring = 0;
}

/* d_myclear_proc:
 *  Simple dialog procedure which just clears the screen. Useful as the
 *  first object in a dialog.
 */
int d_myclear_proc(int msg, DIALOG *d, int c)
{
   ASSERT(d);
   if (msg == MSG_DRAW) 
     {
# ifdef XV_WIN32
       if (win_get_window() == GetForegroundWindow())
	 {
# endif
	   //	   if (!screen_used)
	   //  {
	   //    screen_used = 1;
	       acquire_screen();
	       set_clip_rect(screen, d->x, d->y, d->x+d->w-1, d->y+d->h-1);
	       vsync();
	       clear_to_color(screen, makecol(64,64,255));
	       set_clip_rect(screen, 0, 0, SCREEN_W-1, SCREEN_H-1);      
	       release_screen();
	       //  screen_used = 0;
	       // }
# ifdef XV_WIN32
	 }
# endif
     }
   return D_O_K;
}

int    init_config(int argc, char **argv)
{
    register int i, j;
    char all[256] = {0};
#ifdef FORTIFY
    FILE *fp = NULL;
#endif    
    
    for (i = j = 0; argv[0][i] != 0 && i < 256; i++)
    {
        prog_dir[i] = argv[0][i];
        j = (argv[0][i] == '/' || argv[0][i] == 92) ? i : j;
    }
    prog_dir[i] = 0;
    if (j > 0 && j < i) prog_dir[j+1] = 0;
         

#ifdef FORTIFY
    snprintf(all,256,"%s%s",prog_dir,"fortify-on.txt");
    fp = fopen(all,"r");
    if (fp == NULL)
    {
	Fortify_Disable();
	fortify_menu[1].text = NULL;
	fortify_menu[1].proc = NULL;
	fortify_menu[0].text = "Fortify next Xvin";
	fortify_menu[0].proc = set_fortify_next;
    }
    else fclose(fp);	
#endif
    if (getenv("ALLEGRO") == NULL)
    {
        /* we get config from working directory */
        snprintf(all,256,"ALLEGRO=%s",prog_dir);
        putenv(all);
        printf("%s all %s\n",prog_dir,all);
		#ifndef XV_MAC
        getchar();
		#endif
        return 0;
    }
    return 1;
}

int switch_allegro_font (int mode)
{
    if ((mode == 1) && (big_font != NULL))                    
    {
//        win_printf("Big");
        font = big_font;
    }
    else if (mode == 2) font = font_18pt;
    else if (mode == 0)     font = original_font;
    else    return win_printf("mode %d",mode);
    return 0;    
}
/*
int do_trackBead(void)
{
  if(updating_menu_state != 0)	return D_O_K;
# if !defined(PLAYITSAM) && !defined(PIAS)
    trackBead_main(0, NULL);    
# endif
  return 0;
}
*/





int xvin_pico(int argc, char *argv[])
{
  /*    struct box b;*/
  /*   PALETTE pal;*/

  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *ds = NULL;
  register int i;
  char  file[256] = {0}; /*, buf[1024]; */
  //extern char fullfile[], *fu;
  /*    clock_t t0; */
  int ret, mode, mode_h, quit = 0;
# ifdef BUILDING_DLL
  int  argcp, np;
  char **argvp, pl_name[64] = {0};
#else
  //extern int	track_ifc_main(int argc, char **argv);
  //extern int random_main(int argc, char **argv);
  //int EveryN_main(int argc, char **argv);
//    int	track_ifc_main(int argc, char **argv);
//    int random_main(int argc, char **argv);


# endif


  DIALOG_PLAYER *player = NULL;

  init_config(argc, argv);
  set_uformat(U_ASCII);
  before_menu_proc = NULL;
  after_menu_proc = NULL;

  if (allegro_init() != 0) exit(1);
  install_keyboard(); 
  install_timer();
  install_mouse();

  //	allegro_message("welcome boy\n");
  win_color_depth = desktop_color_depth();
  if (get_desktop_resolution(&win_screen_w, &win_screen_h) != 0) 
    allegro_message("Cannot get the video mode property!");


  snprintf(file,256,"%sScope.cfg",prog_dir);

  set_config_file(file);
   
# ifdef FORTIFY
  //Fortify_SetOutputFunc(allegro_message);
  Fortify_SetOutputFunc(fortify_error);
# endif


  snprintf(file,256,"%sfonts.dat",prog_dir);
  original_font = font;
  datafile = load_datafile(file);
  if (datafile) {
    //        win_printf("loading big font %d", (big_font != NULL) ? 1 : 0);   
    big_font = datafile[BIG_FONT].dat;
    font_18pt = datafile[FONT_18PT].dat;
    font = original_font = datafile[MEDIUM_FONT].dat;}
  else 
    win_printf("data not loaded from \n%s",backslash_to_slash(file));
        



  if (win_color_depth != 32)
    allegro_message("Your video mode is not in 32 Bits !\n pico.exe might not work properly");

  mode = get_config_int("WINDOW_MODE","size",0);
  mode_h = get_config_int("WINDOW_MODE","height",0);
  full_screen = get_config_int("WINDOW_MODE","full-screen",0);

  if (mode == 0)
    {
      mode = 800;
      mode_h = 600;
    }
  if (mode_h == 0 && mode)
    {
      if (mode == 640) mode_h = 480;
      else if (mode == 800) mode_h = 600;
      else if (mode == 900) 
	{
	  mode_h = 700;
	  full_screen = 0;
	}
      else if (mode == 1024) mode_h = 768;
      else if (mode == 1280) mode_h = 1024;
    }

  if (mode > win_screen_w || mode_h > win_screen_h)
    {      mode = 1660; mode_h = 1100;}
  if (mode > win_screen_w || mode_h > win_screen_h)
    {    mode = 1660; mode_h = 1100;  }
  if (mode > win_screen_w || mode_h > win_screen_h)
    {    mode = 1420; mode_h = 850;  }
  if (mode > win_screen_w || mode_h > win_screen_h)
    {    mode = 1260; mode_h = 974;  }
  if (mode > win_screen_w || mode_h > win_screen_h)
    {    mode = 1240; mode_h = 768;  }
  if (mode > win_screen_w || mode_h > win_screen_h)
    {    mode = 1000; mode_h = 718;  }
  if (mode > win_screen_w || mode_h > win_screen_h)
    {    mode = 900; mode_h = 700;     }
  if (mode > win_screen_w || mode_h > win_screen_h)
    {    mode = 640; mode_h = 480;     }



  generate_plot_dialog(mode, mode_h);




  if (full_screen == 0)      start_xvin(32, GFX_AUTODETECT_WINDOWED, mode, mode_h);
  //GFX_DIRECTX_OVL
  else	               start_xvin(32, GFX_AUTODETECT_FULLSCREEN , mode, mode_h); 
  //GFX_DIRECTX_OVL
  
#ifdef XV_UNIX
 // a test : could this improve the behavior when minimizing the window ?
 // answer is: NO
 // xwin_set_window_name("XVin", "Allegro"); // name and group for the window manager
#endif



    
  /* We set up colors to match screen color depth (in case it changed) */
  for (ret = 0; the_dialog[ret].proc; ret++) 
    {
      if (the_dialog[ret].proc == d_my_edit_proc)
	{
	  set_dialog_size_and_color(the_dialog + ret, the_dialog[ret].x, 19, the_dialog[ret].w, 19,  
				    makecol32(0, 0, 255), makecol32(240, 240, 240));
	}
      else if (the_dialog[ret].proc == d_text_proc)
	{
	  set_dialog_size_and_color(the_dialog + ret, the_dialog[ret].x, 19, the_dialog[ret].w, 19,  
				    makecol32(0, 0, 0), makecol32(240, 240, 240));
	}
      else
	{ 
	  the_dialog[ret].fg = makecol(0, 0, 0);
	  the_dialog[ret].bg = makecol(255, 255, 255);
	}

    }
  the_dialog[0].w = SCREEN_W;    
  the_dialog[0].h = SCREEN_H;    
  the_dialog[2].w = SCREEN_W;    
  the_dialog[2].h = SCREEN_H-38;// 16    

  plt_buffer = create_bitmap(SCREEN_W, SCREEN_H);




  proj_mn_n = -1;
  im_project_n = -1;

  pr_SCOPE = pr = create_pltreg_with_op(&op, 4096, 4096, 0);
  if (pr == NULL)
    {
      quit = 1;
      show_mouse(screen);
      win_printf_OK("could not create image refion");
    }
  else if (op != NULL)
    {
      op_SCOPE = op;
      for (i = 0, ds = op->dat[0]; i < ds->nx; i++)
	{
	  ds->xd[i] = i;
	  ds->yd[i] = sin(M_PI*i*128/ds->nx);
	}
      set_plot_title(op, "Test");
      set_plot_x_title(op, "Time");    
      set_plot_y_title(op, "Signal");

    }




//  win_printf("display switch mode : %d", get_display_switch_mode());
    i=SWITCH_BACKGROUND; // with this option, XVin will still work, even if put in the bacground
    if (set_display_switch_mode(i) != 0) win_printf("cannot set display switch mode to %d", i);
//  win_printf("display switch mode : %d", get_display_switch_mode());        
// # SWITCH_NONE = 0 not OK on win32/windowed
// # SWITCH_PAUSE = 1 default on win32/windowed
// # SWITCH_AMNESIA = 2, not OK on win32/windowed
// # SWITCH_BACKGROUND = 3, OK 
// # SWITCH_BACKAMNESIA =5, not OK on win32/windowed

  set_display_switch_callback(SWITCH_IN,restore_screen);
  set_close_button_callback(close_hook);  
  
  //set_window_close_hook(close_hook);
  
  gui_fg_color = makecol(0, 0, 0);
  gui_mg_color = makecol(160, 160, 160);
  gui_bg_color = makecol(220, 220, 240);
  
  //    register_trace_handler(win_printf);
  //    register_assert_handler(win_printf);
  
  
  
// below, we automatically load the plugins refered to in 'xvin.cfg' :
#ifdef BUILDING_DLL
	np = get_config_int("PLUGINS-AUTO","plugins_n",0);
	for (i=0; i<np; i++)
	{
		snprintf(pl_name,64,"plugins_%04d",i);
		argvp = get_config_argv("PLUGINS-AUTO", pl_name, &argcp);        
		if (argcp > 0)
		{	if (load_plugin(argcp, argvp)!=0) 
			return(win_printf_OK("error when auto-loading plugin %s",argvp[0])); // error : we exit
        	}
	}     
#endif   
    
    show_mouse(screen);

# ifdef XV_WIN32
    LoadLibrary("exchndl.dll");  // Dr mingw exception handler
# endif



    //  DoFileSave("Test save 2", NULL, szFileName,MAX_PATH, 
    //   "Tracking data Files (*.trk)\0*.trk\0All Files (*.*)\0*.*\0\0", "trk");
  if (quit == 0)
    {
      player = init_dialog(the_dialog, -1);
# ifdef XVIN_STATICLINK
      //das1602_main(argc, argv);    
      //track_ifc_main(argc, argv);    
      //random_main(argc, argv);
      //EveryN_main(argc, argv);
      scope_main(argc, argv);
      create_scope_thread();
      update_dialog(player);
      broadcast_dialog_message(MSG_DRAW,0);   
      stat_main(0, NULL);    
# endif
      while (update_dialog(player));
      
      shutdown_dialog(player);
      
      //ret = do_dialog(the_dialog, -1);
      show_mouse(screen);
    }
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_exit();
  
  /*      allegro_message("1 plot in %ld ticks with %ld tics per sec \n%s",
	  t0,CLOCKS_PER_SEC,message);
	  allegro_message("%s",debug_ttf);*/
  return 0;
}

DIALOG* get_the_dialog (void)
{    
    return the_dialog;
}
BITMAP* get_plt_buffer (void)
{
    return plt_buffer;
}
DIALOG* get_cur_ac_reg (void)
{
    return cur_ac_reg;
}
# include "fftl32n.h"
float* get_fftsin(void)
{
    return fftsin;
}

# endif


