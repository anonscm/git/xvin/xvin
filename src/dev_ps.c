# ifndef _DEV_PS_C_ 
# define _DEV_PS_C_ 

#include "dev_ps.h"

#include "stdio.h"
#include "dev_def.h"
#include "plot_opt.h"
#include "color.h"
# include "box_def.h"
# include "xvin/box2alfn.h"
# include "plot2box.h"

# define B_LINE 128	
# define OVER_WHITE  32
# define COLOR_WHITE_TO_BLACK 64
# define COLOR 128
# define BLACK 0

extern FILE *qd_fp;
static int first_page=1;
extern short int  last_pt_size;
extern int last_color;
int	color_enable = 0;

int		max_clip_enable = 0;
float	max_x0_clip, max_x1_clip, max_y0_clip, max_y1_clip;


/* Move current position to (ix, iy) without drawing a line */
int ps_move(float ix, float iy)
{
	if (qd_fp == NULL)	return 1;
	return fprintf(qd_fp,"%g %g m\n",ix, iy);
}
int ps_rmove(float ix, float iy)
{
	if (qd_fp == NULL)	return 1;
	return fprintf(qd_fp,"%g %g o\n",ix, iy);
}
int ps_line_size(float size)
{
	if (qd_fp == NULL)	return 1;
	return fprintf(qd_fp,"%g ls\n",size);
}
/* Change between graphics mode and text mode; erase screen; etc. */
int ps_mode(int arg)
{
	switch (arg)
	{
		case GRAPHICS:
			q_dev.move = ps_catch1;
			q_dev.line = ps_catch1;
			q_dev.dot = ps_catch2;
			break;
		case DONE:
			ps_init();
			break;
		case ERASE:
			if ( first_page == 0)
			{
				fprintf(qd_fp,"e\n");
			}
			else
				first_page = 0; /* do not send a blank page */
			break;
	}
	return 0;
}

/* Place a dot at given position and update current position. */
int ps_dot(float ix,float iy)
{
	ps_move(ix,iy);
	ps_line(ix,iy);	
	return 0;
}

/* Draw line from current position to (ix, iy) and update current position */
int ps_line(float ix, float iy)
{
	if (qd_fp == NULL)	return 1;
	return (fprintf(qd_fp,"%g %g n\n",ix, iy));
}
int ps_rline(float ix, float iy)
{
	if (qd_fp == NULL)	return 1;
	return (fprintf(qd_fp,"%g %g q\n",ix, iy));
}
int ps_color(int color, int mode)
{
	static int old_col = BLACK, color_mem = 1;
	float r, g, b;

	if (mode == TRUE_COLOR_WHITE_TO_BLACK ) 	
	{
		color_mem = TRUE_COLOR_WHITE_TO_BLACK;
		if (color == White) color = Black;
		else	if (color == Lightgray) color = Darkgray;
		else	if (color == Darkgray) color = Lightgray;
		if (color != old_col) 
		{
			r = (float)(EXTRACT_R(color))/255;
			g = (float)(EXTRACT_G(color))/255;
			b = (float)(EXTRACT_B(color))/255;
			fprintf(qd_fp,"%g %g %g srgb\n", r,g,b);
			old_col = color;
		}		
	}
	else if (mode == TRUE_COLOR) 	
	{
		color_mem = TRUE_COLOR;
		if (color != old_col) 
		{
			r = (float)(EXTRACT_R(color))/255;
			g = (float)(EXTRACT_G(color))/255;
			b = (float)(EXTRACT_B(color))/255;
			fprintf(qd_fp,"%g %g %g srgb\n", r,g,b);
			old_col = color;
		}		
	}	
	else if (mode == BW_WHITE_TO_BLACK)
	{
		color_mem = BW_WHITE_TO_BLACK;
		if (color == White) color = Black;
		else	if (color == Lightgray) color = Darkgray;
		else	if (color == Darkgray) color = Lightgray;
		color = (color == White) ? Black : color;
		if (color != old_col) 
		{
			fprintf(qd_fp,"%d setgray\n",(color == White) ? 1 : 0);
			old_col = color;
		}	
	}
	else if (mode == GRAY_WHITE_TO_BLACK)
	{
		color_mem = GRAY_WHITE_TO_BLACK;
		if (color == White) color = Black;
		else	if (color == Lightgray) color = Darkgray;
		else	if (color == Darkgray) color = Lightgray;	
		color = (color == White) ? Black : color;
		if (color != old_col) 
		{
			r = (float)(EXTRACT_R(color))/255;
			g = (float)(EXTRACT_G(color))/255;
			b = (float)(EXTRACT_B(color))/255;
			r = (r+b+g)/3;
			fprintf(qd_fp,"%g setgray\n",r);
			old_col = color;
		}	
	}
	else if (color_mem == TRUE_COLOR_WHITE_TO_BLACK )
	{
		if (color == White) color = Black;
		else	if (color == Lightgray) color = Darkgray;
		else	if (color == Darkgray) color = Lightgray;		
		if (color != old_col) 
		{
			r = (float)(EXTRACT_R(color))/255;
			g = (float)(EXTRACT_G(color))/255;
			b = (float)(EXTRACT_B(color))/255;
			fprintf(qd_fp,"%g %g %g srgb\n", r,g,b);
			old_col = color;
		}		
	}
	else if (color_mem == TRUE_COLOR) 	
	{
		color_mem = TRUE_COLOR;
		if (color != old_col) 
		{
			r = (float)(EXTRACT_R(color))/255;
			g = (float)(EXTRACT_G(color))/255;
			b = (float)(EXTRACT_B(color))/255;
			fprintf(qd_fp,"%g %g %g srgb\n", r,g,b);
			old_col = color;
		}		
	}	
	else if (color_mem == BW_WHITE_TO_BLACK)
	{
		color_mem = BW_WHITE_TO_BLACK;
		if (color == White) color = Black;
		else	if (color == Lightgray) color = Darkgray;
		else	if (color == Darkgray) color = Lightgray;		
		
		if (color != old_col) 
		{
			fprintf(qd_fp,"%d setgray\n",(color == White) ? 1 : 0);
			old_col = color;
		}	
	}	
	else if (color_mem == GRAY_WHITE_TO_BLACK)
	{
		if (color == White) color = Black;
		else	if (color == Lightgray) color = Darkgray;
		else	if (color == Darkgray) color = Lightgray;	
		color = (color == White) ? Black : color;
		if (color != old_col) 
		{
			r = (float)(EXTRACT_R(color))/255;
			g = (float)(EXTRACT_G(color))/255;
			b = (float)(EXTRACT_B(color))/255;
			r = (r+b+g)/3;
			fprintf(qd_fp,"%g setgray\n",r);
			old_col = color;
		}	
	}
	last_color = dev_color;	

	return 0;
}
int 	ps_clip_abs(float x0, float y0, float x1, float y1) /* cannot be override */
{
	max_clip_enable = 1;
	max_x0_clip = (x0 < x1) ? x0 : x1;
	max_x1_clip = (x0 < x1) ? x1 : x0;
	max_y0_clip = (y0 < y1) ? y0 : y1;
	max_y1_clip = (y0 < y1) ? y1 : y0;
	return 0;
}

int 	ps_clip_abs_reset(void) /* cannot be override */
{
	max_clip_enable = 0;
	return 0;
}
int 	ps_clip(float x0, float y0, float x1, float y1)
{
	float a, b, c , d;
	if (x0 < x1)
	{
		a = x0;
		c = x1;
	}
	else
	{
		a = x1;
		c = x0;		
	}
	if (y0 < y1)
	{
		b = y0;
		d = y1;
	}
	else
	{
		b = y1;
		d = y0;		
	}	
	if (max_clip_enable)
	{
		a = (a > max_x0_clip) ? a : max_x0_clip;
		c = (c > max_x1_clip) ? max_x1_clip : c;
		b = (b > max_y0_clip) ? b : max_y0_clip;
		d = (d > max_y1_clip) ? max_y1_clip : d;
	}
		
	fprintf(qd_fp,"newpath\n %g %g moveto\n",a, b);		
	fprintf(qd_fp,"%g %g lineto\n",a, d);		
	fprintf(qd_fp,"%g %g lineto\n",c, d);			
	fprintf(qd_fp,"%g %g lineto\n",c, b);				
	fprintf(qd_fp,"closepath\nclip\nnewpath\n");				
	return (0);
}


int 	ps_clip_reset(void)
{
	if (qd_fp == NULL)	return 1;
	return (fprintf(qd_fp,"initclip\n"));	
}



/* Set font size, line width, etcetera. */
int ps_size(int arg)
{
  (void)arg;
	return 0;
}

/* Initialize global variables and device structure. */
int ps_init(void)
{
	init_font();	
	return (ps_init1());
}
int ps_init1(void)
{
	q_dev.move = ps_move;
	q_dev.rmove = ps_rmove;	
	q_dev.mode = ps_mode;
	q_dev.dot = ps_dot;
	q_dev.line = ps_line;
	q_dev.rline = ps_rline;	
	q_dev.size = ps_size;
	q_dev.line_size = ps_line_size;	
	q_dev.color = ps_color;		
	q_dev.clip = ps_clip;			
	q_dev.clip_reset = ps_clip_reset;				
	last_color = 1;
	ps_color(BLACK,0);
	pc_fen = 5;
        q_dev.draw_char = ps_draw_char;
        q_dev.draw_char_90 = ps_draw_char_90;

	return 0;
}
/* These routines are enabled at the start of graphics input. Their purpose is
 * to catch the first point. Consider: when passing a bunch of points to
 * have lines drawn between you do NOT want to draw a line to the first
 * point; the first point is the starting point.
 */
int ps_catch1(float x, float y)
{
	q_dev.move = ps_move;
	q_dev.dot = ps_dot;
	q_dev.line = ps_line;
	return (ps_move(x,y));
}

int ps_catch2(float x, float y)
{
	q_dev.move = ps_move;
	q_dev.dot = ps_dot;
	q_dev.line = ps_line;
	return (ps_dot(x, y));
}

int ps_start_bounding_box_2(char *file, char *infile, char *inpath, 
	time_t *timer, float x0, float w, float y0, float h, float sc)
{
	if (qdfile(file) == 0) 	return 1;
	last_pt_size = 7;
	fprintf (qd_fp, "%%!PS-Adobe-2.0 EPSF-1.2\n%%%%DocumentFonts: Courier\n"
	"%%%%Title: %s%s\n%%%%Creator: xvin\n%%%%CreationDate:%s%%%%Pages: 0\n"
	"%%%%BoundingBox: %d %d %d %d\n%%%%EndComments\n"
	"%% lib/psplot.pro -- included prolog for plot files\n"
	"%% Copyright (c) 1984 Adobe Systems, Inc.  All Rights Reserved.\n"
	"save 50 dict begin /psplot exch def\n/StartPSPlot\n"
	"   {newpath 0 0 moveto 7 setlinewidth 0 setgray 1 setlinecap\n"
	"    /imtx matrix currentmatrix def /dmtx matrix defaultmatrix def\n"
	"    /fnt /Courier findfont def /smtx matrix def fnt 8 "
	"scalefont setfont}def\n"
	"/solid {{}0}def\n/ls {setlinewidth}def\n/mv {moveto}def\n"
	"/li {lineto}def\n/m {newpath moveto}def\n"
	"/o {currentpoint newpath moveto rmoveto}def\n"
	"/n {lineto currentpoint stroke moveto}def\n"
	"/q {rlineto currentpoint stroke moveto}def\n"
	"/p {newpath moveto gsave 1 setlinecap solid setdash\n"
	"    dmtx setmatrix .4 nail setlinewidth\n"
	"    .05 0 idtransform rlineto stroke grestore}def\n"
	"/l {moveto lineto currentpoint stroke moveto}def\n"
	"/t {smtx currentmatrix pop imtx setmatrix show smtx setmatrix}def\n"
	"/c {gsave newpath 0 360 arc stroke grestore}def\n"
	"/e {gsave showpage grestore newpath 0 0 moveto}def\n"
	"/f {load exec setdash}def\n/EndPSPlot {clear psplot end restore}def\n"
	"/srgb {setrgbcolor} bind def\n/col-1 {0 setgray} bind def\n"
	"/col0 {0.000 0.000 0.000 srgb} bind def\n"
	"/col1 {0.000 0.000 .5 srgb} bind def\n"
	"/col2 {0.000 .5 0.000 srgb} bind def\n"
	"/col3 {0.000 .5 .5 srgb} bind def\n"
	"/col4 {.5 0.000 0.000 srgb} bind def\n"
	"/col5 {.5 0.000 .5 srgb} bind def\n"
	"/col6 {.5 .5 0.000 srgb} bind def\n"
	"/col7 {.67 .67 .67 srgb} bind def\n"
	"/col8 {0.33 0.33 0.33 srgb} bind def\n"
	"/col9 {0.25 0.25 1 srgb} bind def\n"
	"/col10 {0.25 1 0.25 srgb} bind def\n"
	"/col11 {0.0 1.0 1.0 srgb} bind def\n"
	"/col12 {1.0 0.25 0.25 srgb} bind def\n"
	"/col13 {1.0 0.0 1.0 srgb} bind def\n"
	"/col14 {1.0 1.0 0.000 srgb} bind def\n"
	"/col15 {1.0 1.0 1.0 srgb} bind def\n"
	"/col16 {0.000 0.690 0.690 srgb} bind def\n"
	"/col17 {0.000 0.820 0.820 srgb} bind def\n"
	"/col18 {0.560 0.000 0.000 srgb} bind def\n"
	"/col19 {0.690 0.000 0.000 srgb} bind def\n"
	"/col20 {0.820 0.000 0.000 srgb} bind def\n"
	"/col21 {0.560 0.000 0.560 srgb} bind def\n"
	"/col22 {0.690 0.000 0.690 srgb} bind def\n"
	"/col23 {0.820 0.000 0.820 srgb} bind def\n"
	"/col24 {0.500 0.190 0.000 srgb} bind def\n"
	"/col25 {0.630 0.250 0.000 srgb} bind def\n"
	"/col26 {0.750 0.380 0.000 srgb} bind def\n"
	"/col27 {1.000 0.500 0.500 srgb} bind def\n"
	"/col28 {1.000 0.630 0.630 srgb} bind def\n"
	"/col29 {1.000 0.750 0.750 srgb} bind def\n"
	"/col30 {1.000 0.880 0.880 srgb} bind def\n"
	"/col31 {1.000 0.840 0.000 srgb} bind def\n"
	"/gr {grestore} bind def\n/gs {gsave} bind def\n"
	"/s {stroke} bind def\n%% end fixed prolog\n\n"
	"StartPSPlot\n%%%%EndProlog\n %g %g translate\n %g %g scale\n",
	inpath,infile,ctime(timer),(int)(x0+1),(int)(y0+1),(int)(x0+w+1),
	(int)(y0+h+1),x0,y0,sc,sc);
	return 0;
}

int ps_close(void)
{
	if (qd_fp == NULL)	return 1;
	fputs("e\n%%Trailer\nEndPSPlot\n",qd_fp);
	fclose(qd_fp);
    qd_fp = NULL;
	return 0;
}

# endif




