/*	fillib.c
 *
 *	5 fev 92	VC
 *			revision JMF
 *	 
 *	DESCRIPTION	Collection of function performing lowpass, highpass
 *			and bandpass filtering on the fourier transform of
 *			either real or complex data. 
 *
 *
 *	filter_init(npts)
 *
 *	lowpass_smooth_half (npts, x, cutoff) 
 *	hipass_smooth_half (npts, x, cutoff)
 *	bandpass_smooth_half (npts, x, center, width)
 *	(for filtering Fourier transform data of real serie)
 *
 *	lowpass_smooth_sym (npts, x, cutoff)
 *	hipass_smooth_sym (npts, x, cutoff)
 *	bandpass_smooth_sym (npts, x, center, width)
 *	(for filtering Fourier transform data of complex serie)	
 *
 *	npts is the number of memories required to store the Fourier transform
 *	corresponding to npts points of real data, or npts/2 points of
 *	complex data. npts should be a multiple of 4. npts may be
 *	changed from one call to the other, asking for a smaller
 *	array has no major consequences, increasing npts to a value
 *	larger than anyone previously invoked, reallocates memory and
 *	thus is not not speed efficient. It is wise to call
 *	filter-init with the largest n used later on.
 *	
 * 
 *	cutoff, center, width characterize the filter shape, their units 
 * 	are in FFT mode number
 * 
 * 	When ever one filter is call, the filter parameter are
 *	compared with those stored in memory, if they differ, the
 *	filter array is recomputed, otherwise the Fourier array is
 *	just multiplied by the filter array.
 * 
 * 
 */
# include <stdio.h>
# include <math.h>
# include <malloc.h>
# include <stdlib.h>
# include "fillib.h"
# include "util.h"

# include "xvin/platform.h"
# ifdef FORTIFY
# include "fortify.h"
# endif

static struct filter fil; 

static int fillib_init=0;

/*	filter_init(npts)
 *	DESCRIPTION	Allocate memory to the float array fil.f[] 
 *			used for filtering the data Fourier transformed,
 *			this array is static.
 *
 *	RETURNS		0 on success, 1 on  malloc error
 *			
 */

int filter_init(int npts)
{
	int n_2 = npts/2;

	if (fillib_init == 0)	/*first initialisation*/
	{
		fillib_init = 1;
		fil.n = 0;
		fil.m = 0;
		fil.lp = 0;
		fil.wl = 0;
		fil.hp = 0;
		fil.wh = 0;
	}
	if ( fil.n != n_2 )
	{
		if (fil.n != 0 && fil.m < n_2)
		{
			if (fil.f) free (fil.f);
			fil.f = NULL;
		}
		if (fil.m < n_2)
		{
			fil.f = (float *)malloc((n_2 +1)*sizeof(float));
			if (fil.f == NULL)
			{
				fprintf(stderr,"malloc error n");
				return 1;
			}
			else
			{
				fil.n = n_2;
				fil.m = n_2;
			}
		}
		else 
			fil.n = n_2;
	}
	return 0;
}
		

/*	lowpass_smooth_half (npts, x, cutoff)
 *	DESCRIPTION	Subroutine performing a low-pass filtering on a 
 *			Fourier transform of a npts real data points stored
 *			in the x array. The filter is built with cosine, 
 *			starts at 1 at zero frequency, 1/2 at cutoff,
 *			and reaches 0 at 2*cutoff.
 *	 
 *	RETURNS		0 on success, 3 on incompatible cutoff frequency,
 *			1 on  malloc error (in filter_init)
 */

int lowpass_smooth_half (int npts, float *x, int cutoff)
{
	register int i, j;
	int n_4, n_2, c;
	
	c = cutoff;
	n_2 = npts /2;
	n_4 = npts /4;
	
	if ( cutoff > n_4 || cutoff <0 )
		return 3;
	
	if (fil.n != n_2)
	{
		if ( (i = filter_init (n_2)) != 0)
			return i;
		fil.n = n_2;
	}
		
	if (fil.hp!=0 || fil.wh!=0 || fil.lp!=c || fil.wl!=c || fil.fl!=0)
	{	
		fil.f[0]=1;
		for ( i = 1; i < 2* cutoff ; i++)
			fil.f[i]= (1 + cos ( i * M_PI_2/cutoff))/2;
		for ( i = 2* cutoff; i < npts/2 ; i++)
			fil.f[i] = 0;
		fil.f[npts/2]= 0;
		fil.hp = 0;
		fil.wh = 0;
		fil.lp = cutoff;
		fil.wl = cutoff;
		fil.fl = 0;
	}
	x[0] *= fil.f[0];
	x[1] *= fil.f[n_2];
	for ( i = 1, j=2; i < n_2; i++)
	{
		x[j++] *= fil.f[i];
		x[j++] *= fil.f[i];
	}
	return 0;
}

/*	hipass_smooth_half (npts, x, cutoff)
 *	DESCRIPTION	Subroutine performing a high-pass filtering on a 
 *			Fourier transform of a npts real data points stored
 *			in the x array. The filter is built with cosine, 
 *			starts at 0 at zero frequency, 1/2 at cutoff,
 *			and reaches 1 at 2*cutoff.
 *	 
 *	RETURNS		0 on success, 3 on incompatible cutoff frequency,
 *			1 on  malloc error (in filter_init)
 */

int hipass_smooth_half (int npts, float *x, int cutoff)
{
	register int i, j;
	int n_4, n_2, c;
	
	c = cutoff;
	n_2 = npts /2;
	n_4 = npts /4;

	if ( cutoff > n_4 || cutoff <0 )
		return 3;
	
	if (fil.n != n_2)
	{
		if ( (i = filter_init (n_2)) != 0)
			return i;
		fil.n = n_2;
	}

	if(fil.lp !=0 || fil.wl !=0 || fil.hp !=c || fil.wh !=c || fil.fl !=0)
	{	
		fil.f[0] = 0;
		for ( i = 1; i < 2* cutoff ; i++)
			fil.f[i] = (1 - cos ( i * M_PI_2/cutoff))/2;
		for ( i = 2* cutoff; i < n_2 ; i++)
			fil.f[i] = 1;
		fil.f[n_2]= 1;
		fil.lp = 0;
		fil.wl = 0;
		fil.hp = cutoff;
		fil.wh = cutoff;
		fil.fl = 0;
	}
	x[0] *= fil.f[0];
	x[1] *= fil.f[n_2];
	for ( i = 1, j=2; i < n_2; i++)
	{
		x[j++] *= fil.f[i];
		x[j++] *= fil.f[i];
	}
	return 0;
}
/*	bandpass_smooth_half (npts, x, center, width)
 *	DESCRIPTION	Subroutine performing a band-pass filtering on a 
 *			Fourier transform of a npts real data points stored
 *			in the x array. The filter is built with cosine, 
 *			has a gain of 1 at center frequency, falls to 1/2
 *			at center +/- width/2 and reaches 0 at 
 *			center +/- width.
 *	 
 *	RETURNS		0 on success, 3 on incompatible cutoff frequency,
 *			1 on  malloc error (in filter_init)
 */
int bandpass_smooth_half (int npts, float *x, int center, int width)
{
	register int i, j;
	int n_4, n_2, c1, c2, w;
	
	n_2 = npts /2;
	n_4 = npts /4;
	c1 = center - width;
	c2 = center + width;
	w = width;
	
	if ( center > n_4 || width >= center || center <0 || width <0 )
		return 3;
	
	if (fil.n != n_2)
	{
		if ( (i = filter_init (n_2)) != 0)
			return i;
		fil.n = n_2;
	}
		
	if (fil.hp!=c1 || fil.wh!=w || fil.lp!=c2 || fil.wl!=w || fil.fl!=0)
	{
		for ( i = 0; i < c1 ; i++)
			fil.f[i]=0;
		for ( i = 0 ; i < 2*width ; i++)
			fil.f[i + c1] = (1- cos ( i * M_PI/width))/2;
		for ( i = c2; i < n_2 ; i++)
			fil.f[i] = 0;
		fil.f[n_2]=0;
		fil.hp = c1;
		fil.wh = width;
		fil.lp = c2;
		fil.wl = width;
		fil.fl = 0;
	}
	x[0] *= fil.f[0];
	x[1] *= fil.f[n_2];
	for ( i = 1, j=2; i < n_2; i++)
	{
		x[j++] *= fil.f[i];
		x[j++] *= fil.f[i];
	}
	return 0;
}


/*	lowpass_smooth_sym (npts, x, cutoff)
 *	DESCRIPTION	Subroutine performing a low-pass filtering on a 
 *			Fourier transform of a npts complex data points stored
 *			in the x array. The filter is built with cosine, 
 *			starts at 1 at zero frequency, 1/2 at +/-cutoff,
 *			and reaches 0 at +/-   2*cutoff.
 *	 
 *	RETURNS		0 on success, 3 on incompatible cutoff frequency,
 *			1 on  malloc error (in filter_init)
 */
int lowpass_smooth_sym (int npts, float *x, int cutoff)
{
	register int i, j;
	int n_4, n_2;
	
	n_2 = npts /2;
	n_4 = npts /4;
	
	if ( 2*cutoff > n_4  || cutoff <0)
		return 3;
	
	if (fil.n != n_2)
	{
		if ( (i = filter_init (n_2)) != 0)
			return i;
		fil.n = n_2;
	}
		
	if (fil.hp!=0||fil.wh!=0||fil.lp!=cutoff||fil.wl!=cutoff||fil.fl!=1)
	{	
		fil.f[0]=1;
		for ( i = 1; i < 2* cutoff ; i++)
		{
			fil.f[i] = (1 + cos ( i * M_PI_2/cutoff))/2;
			fil.f[n_2 - i] = fil.f[i];
		}
		for ( i = 2* cutoff; i < n_4 ; i++)
		{
			fil.f[i] = 0;
			fil.f[n_2 - i] = 0;
		}
		fil.f[n_4]= 0;
		fil.hp = 0;
		fil.wh = 0;
		fil.lp = cutoff;
		fil.wl = cutoff;
		fil.fl = 1;
	}
	for ( i = 0, j = 0; i < n_2; i++)
	{
		x[j++] *= fil.f[i];
		x[j++] *= fil.f[i];
	}
	return 0;
}
/*	hipass_smooth_sym (npts, x, cutoff)
 *	DESCRIPTION	Subroutine performing a high-pass filtering on a 
 *			Fourier transform of a npts complex data points stored
 *			in the x array. The filter is built with cosine, 
 *			starts at 0 at zero frequency, 1/2 at +/-cutoff,
 *			and reaches 1 at +/-2*cutoff.
 *	 
 *	RETURNS		0 on success, 3 on incompatible cutoff frequency,
 *			1 on  malloc error (in filter_init)
 */
int hipass_smooth_sym (int npts, float *x, int cutoff)
{
	register int i, j;
	int n_4, n_2;
	
	n_2 = npts /2;
	n_4 = npts /4;
	
	if ( 2*cutoff > n_4 || cutoff <0 )
		return 3;
	
	if (fil.n != n_2)
	{
		if ( (i = filter_init (n_2)) != 0)
			return i;
		fil.n = n_2;
	}
		
	if (fil.lp!=0||fil.wl!=0||fil.hp!=cutoff||fil.wh!=cutoff||fil.fl!=1)
	{	
		fil.f[0] = 0;
		for ( i = 1; i < 2* cutoff ; i++)
		{
			fil.f[i] = (1 - cos ( i * M_PI_2/cutoff))/2;
			fil.f[n_2 - i] = (1 - cos ( i * M_PI_2/cutoff))/2;
		}
		for ( i = 2* cutoff; i < n_4 ; i++)
		{
			fil.f[i] = 1;
			fil.f[n_2 - i] = 1;
		}
		fil.f[n_4]= 1;
		fil.lp = 0;
		fil.wl = 0;
		fil.hp = cutoff;
		fil.wh = cutoff;
		fil.fl = 1;
	}
	for ( i = 0, j = 0; i < n_2; i++)
	{
		x[j++] *= fil.f[i];
		x[j++] *= fil.f[i];
	}
	return 0;
}
/*	bandpass_smooth_sym (npts, x, center, width)
 *	DESCRIPTION	Subroutine performing a band-pass filtering on a 
 *			Fourier transform of a npts complex data points stored
 *			in the x array. The filter is built with cosine, 
 *			has a gain of 1 at +/- center frequency, falls to 1/2
 *			at +/-(center +/- width/2) and reaches 0 at 
 *			+/-(center +/- width).
 *	 
 *	RETURNS		0 on success, 3 on incompatible cutoff frequency,
 *			1 on  malloc error (in filter_init)
 */
int bandpass_smooth_sym (int npts, float *x, int center, int width)
{
	register int i, j;
	int n_4, n_2, c1, c2, w;
	
	n_2 = npts /2;
	n_4 = npts /4;
	c1 = center - width;
	c2 = center + width;
	w = width;
	
	if ( 2*center > n_4 || width >= center || center <0 || width <0 )
		return 3;
	
	if (fil.n != n_2)
	{
		if ( (i = filter_init (n_2)) != 0)
			return i;
		fil.n = n_2;
	}
		
	if (fil.hp!=c1||fil.wh!=w ||fil.lp!=c2 ||fil.wl!=w ||fil.fl!=1)
	{
		fil.f[0] = 0;
		for ( i = 1; i < c1 ; i++)
		{
			fil.f[i] = 0;
			fil.f[n_2 - i] = 0;
		}
		for ( i = 0 ; i < 2*width ; i++)
		{
			fil.f[i + c1] = (1- cos ( i * M_PI/width))/2;
			fil.f[n_2 - i - c1] = fil.f[i + c1];
		}
		for ( i = c2; i < n_4 ; i++)
		{
			fil.f[i] = 0;
			fil.f[n_2 - i] = 0;
		}
		fil.f[n_4]=0;
		fil.hp = c1;
		fil.wh = width;
		fil.lp = c2;
		fil.wl = width;
		fil.fl = 1;
	}
	for ( i = 0, j = 0; i < n_2; i++)
	{
		x[j++] *= fil.f[i];
		x[j++] *= fil.f[i];
	}
	return 0;
}

/*	lowpass_smooth_dissym (npts, x, cutoff)
 *	DESCRIPTION	Subroutine performing a low-pass filtering on a 
 *			Fourier transform of a npts complex data points stored
 *			in the x array. The filter is built with cosine, 
 *			starts at 1 at zero frequency, 1/2 at cutoff,
 *			and reaches 0 at 2*cutoff. All modes which
 *			sign differs from that of cutoff are set to 0
 *	 
 *	RETURNS		0 on success, 3 on incompatible cutoff frequency,
 *			1 on  malloc error (in filter_init)
 */
int lowpass_smooth_dissym (int npts, float *x, int cutoff)
{
	register int i, j;
	int n_4, n_2;
	
	n_2 = npts /2;
	n_4 = npts /4;
	j = abs (cutoff);
	
	if ( 2*j > n_4 )
		return 3;
	
	if (fil.n != n_2)
	{
		if ( (i = filter_init (n_2)) != 0)
			return i;
		fil.n = n_2;
	}
		
	if (fil.hp!=0||fil.wh!=0||fil.lp!=cutoff||fil.wl!=cutoff||fil.fl!=2)
	{	
		fil.f[0]=1;
		for ( i = 1; i < 2* j ; i++)
		{
			fil.f[i] = (1 + cos ( i * M_PI_2/j))/2;
			fil.f[n_2 - i] = fil.f[i];
		}
		if (cutoff >=0 )
		{
			for ( i = 1; i < 2* j ; i++)
				fil.f[n_2 - i] = 0;
		}
		else
		{
			for ( i = 1; i < 2* j ; i++)
				fil.f[i] = 0;
		}
		for ( i = 2* j; i < n_4 ; i++)
		{
			fil.f[i] = 0;
			fil.f[n_2 - i] = 0;
		}
		fil.f[n_4]= 0;
		fil.hp = 0;
		fil.wh = 0;
		fil.lp = cutoff;
		fil.wl = cutoff;
		fil.fl = 2;
	}
	for ( i = 0, j = 0; i < n_2; i++)
	{
		x[j++] *= fil.f[i];
		x[j++] *= fil.f[i];
	}
	return 0;
}
/*	hipass_smooth_dissym (npts, x, cutoff)
 *	DESCRIPTION	Subroutine performing a high-pass filtering on a 
 *			Fourier transform of a npts complex data points stored
 *			in the x array. The filter is built with cosine, 
 *			starts at 0 at zero frequency, 1/2 at cutoff,
 *			and reaches 1 at 2*cutoff.  All modes which
 *			sign differs from that of cutoff are set to 0
 *	 
 *	RETURNS		0 on success, 3 on incompatible cutoff frequency,
 *			1 on  malloc error (in filter_init)
 */
int hipass_smooth_dissym (int npts, float *x, int cutoff)
{
	register int i, j;
	int n_4, n_2;
	
	n_2 = npts /2;
	n_4 = npts /4;
	j = abs (cutoff);
	
	if ( 2*j > n_4 )
		return 3;
	
	if (fil.n != n_2)
	{
		if ( (i = filter_init (n_2)) != 0)
			return i;
		fil.n = n_2;
	}
		
	if (fil.lp!=0||fil.wl!=0||fil.hp!=cutoff||fil.wh!=cutoff||fil.fl!=2)
	{	
		fil.f[0] = 0;
		for ( i = 1; i < 2* j ; i++)
		{
			fil.f[i] = (1 - cos ( i * M_PI_2/j))/2;
			fil.f[n_2 - i] = fil.f[i];
		}
		for ( i = 2*j; i < n_4 ; i++)
		{
			fil.f[i] = 1;
			fil.f[n_2 - i] = 1;
		}
		if (cutoff >=0 )
		{
			for ( i = 1; i < n_4 ; i++)
				fil.f[n_2 - i] = 0;
		}
		else
		{
			for ( i = 1; i < n_4 ; i++)
				fil.f[i] = 0;
		}
		fil.f[n_4]= 1;
		fil.lp = 0;
		fil.wl = 0;
		fil.hp = cutoff;
		fil.wh = cutoff;
		fil.fl = 2;
	}
	for ( i = 0, j = 0; i < n_2; i++)
	{
		x[j++] *= fil.f[i];
		x[j++] *= fil.f[i];
	}
	return 0;
}
/*	bandpass_smooth_dissym (npts, x, center, width)
 *	DESCRIPTION	Subroutine performing a band-pass filtering on a 
 *			Fourier transform of a npts complex data points stored
 *			in the x array. The filter is built with cosine, 
 *			has a gain of 1 at center frequency, falls to 1/2
 *			at (center +/- width/2) and reaches 0 at 
 *			(center +/- width).
 *	 
 *	RETURNS		0 on success, 3 on incompatible cutoff frequency,
 *			1 on  malloc error (in filter_init)
 */
int bandpass_smooth_dissym (int npts, float *x, int center, int width)
{
	register int i, j;
	int n_4, n_2, c1, c2, w;
	
	n_2 = npts /2;
	n_4 = npts /4;
	c1 = center - width;
	c2 = center + width;
	w = width;
	
	if (abs(c1) > n_4 || abs(c2) > n_4 || (abs(center) -abs(width)) < 0 || width <0 )
		return 3;
	
	if (fil.n != n_2)
	{
		if ( (i = filter_init (n_2)) != 0)
			return i;
		fil.n = n_2;
	}
		
	if (fil.hp!=c1||fil.wh!=w ||fil.lp!=c2 ||fil.wl!=w ||fil.fl!=2)
	{
		fil.f[0] = 0;
		fil.f[1] = 0;
		c1 = abs(center) - abs(w);
		for ( i = 1; i < c1 ; i++)
		{
			fil.f[i] = 0;
			fil.f[n_2 - i] = 0;
		}
		for ( i = 0 ; i < 2*width ; i++)
		{
			fil.f[i + c1] = (1- cos ( i * M_PI/width))/2;
			fil.f[n_2 - i - c1] = fil.f[i + c1];
		}
		for ( i = abs(center) + abs(w); i < n_4 ; i++)
		{
			fil.f[i] = 0;
			fil.f[n_2 - i] = 0;
		}	
		if (center >= 0) for (i = n_4; i < n_2; i++)		fil.f[i] = 0;
		else for (i = 2; i < n_4; i++)		fil.f[i] = 0;
		c1 = center - width;
		fil.f[n_4]=0;
		fil.hp = c1;
		fil.wh = width;
		fil.lp = c2;
		fil.wl = width;
		fil.fl = 2;
	}
	for ( i = 0, j = 0; i < n_2; i++)
	{
		x[j++] *= fil.f[i];
		x[j++] *= fil.f[i];
	}
	return 0;
}
