# ifndef _OP2BOX_C_
# define _OP2BOX_C_

# include <stdlib.h>
# include <string.h>

# include "op2box.h"
# include "xvin/platform.h"

extern int  max_x, max_y;

// int PlotAxesColor;
struct plot_label   selected_point;
int         selected_point_enable = 0;

int draw_pr_box(struct box *b, int dx, int dy, int scale, int color)
{
    set_box_origin(b, dx, dy, scale);
    display_box(b, scale, color, TRUE_COLOR);
    return 0;
}



int one_plot_to_box(O_p *o_p, struct box *b)
{
    if (o_p == NULL)        return 1;

    qterm(b);
    free_qkd_box();
    init_box(b);
    b->color = Lightgreen;
    qterm(b);
    do_it_one_plot_op(o_p);

    return (0);
}


int do_it_one_plot_op(O_p *o_p)
{
    register int i, j, k;
    int iopt, min;
    float   x[2] = {0}, y[2] = {0}, *xe = NULL, *ye = NULL;
    struct data_set *p_d_s = NULL;
    struct plot_label *p_p_l = NULL;
    un_s *un = NULL;

    PlotAxesColor = White;

    if (o_p == NULL || o_p->n_dat == 0) return 1;

    if (o_p->width != 1 || o_p->height != 1)
        qdset_abs(o_p->width, 0, o_p->height, 0);

    qdset_ticklen(o_p->tick_len);

    if (o_p->tick_len_x != 1)    qdset_ticklen_x(o_p->tick_len_x);

    if (o_p->tick_len_y != 1)    qdset_ticklen_y(o_p->tick_len_y);

    qkylabl_set((o_p->iopt2 & Y_NUM));
    qkxlabl_set((o_p->iopt2 & X_NUM));
    qkyunit(o_p->y_prefix, o_p->y_unit);
    qkxunit(o_p->x_prefix, o_p->x_unit);

    if (!(o_p->iopt2 & X_LIM))                  find_x_limits(o_p);
    else if (o_p->iopt & XLOG)
        if (o_p->x_lo <= 0 || o_p->x_hi <= 0)   find_x_limits(o_p);

    if (!(o_p->iopt2 & Y_LIM))             find_y_limits(o_p);
    else if (o_p->iopt & YLOG)
        if (o_p->y_lo <= 0 || o_p->y_hi <= 0)   find_y_limits(o_p);

    /* draw axes with a scale change if nessecery */

    qkd_color(PlotAxesColor, Yellow);
    iopt = NOCLEAR;
    iopt |= (o_p->iopt & (GRID));
    iopt |= o_p->iopt & (XLOG + YLOG);
    iopt |= DOT;
    iopt |= TRIM;
    x[0] = o_p->dx * o_p->x_lo + o_p->ax;
    x[1] = o_p->dx * o_p->x_hi + o_p->ax;
    y[0] = o_p->dy * o_p->y_lo + o_p->ay;
    y[1] = o_p->dy * o_p->y_hi + o_p->ay;
    qkdraw(2, x, y, iopt, x, x + 1, y, y + 1);

    if (o_p->x_title != NULL)   qxlabl(o_p->x_title);

    if (o_p->y_title != NULL)   qylabl(o_p->y_title);

    if (o_p->x_prime_title == NULL  && o_p->title != NULL)
        qdtitl(o_p->title);

    /*  pr->plt = last_q_b();*/


    iopt = NOCLEAR;
    iopt |= (o_p->iopt & (GRID));
    iopt |= o_p->iopt & (XLOG + YLOG);
    iopt |= DOT;
    iopt |= AXES_PRIME;

    if (o_p->c_xu_p >= 0 && o_p->c_xu_p < o_p->n_xu)
    {
        un = o_p->xu[o_p->c_xu_p];
        qkxunit(o_p->x_prefix, un->name);
        qkxlabl_set(X_NUM);
        x[0] = un->dx * o_p->x_lo + un->ax;
        x[1] = un->dx * o_p->x_hi + un->ax;
    }
    else
    {
        qkxlabl_set(0);
        x[0] = o_p->dx * o_p->x_lo + o_p->ax;
        x[1] = o_p->dx * o_p->x_hi + o_p->ax;
    }

    if (o_p->c_yu_p >= 0 && o_p->c_yu_p < o_p->n_yu)
    {
        un = o_p->yu[o_p->c_yu_p];
        qkyunit(o_p->y_prefix, un->name);
        qkylabl_set(Y_NUM);
        y[0] = un->dx * o_p->y_lo + un->ax;
        y[1] = un->dx * o_p->y_hi + un->ax;
    }
    else
    {
        qkylabl_set(0);
        y[0] = o_p->dy * o_p->y_lo + o_p->ay;
        y[1] = o_p->dy * o_p->y_hi + o_p->ay;
    }

    qkdraw(2, x, y, iopt, x, x + 1, y, y + 1);

    if (o_p->y_prime_title != NULL) qylabl(o_p->y_prime_title);

    if (o_p->x_prime_title != NULL) qxlabl(o_p->x_prime_title);

    if (o_p->title != NULL && o_p->x_prime_title != NULL)
        qdtitl(o_p->title);



    /*  pr->plt = last_q_b();*/

    for (i = 0, iopt = 0 ; i < o_p->n_dat ; i++)
    {
        p_d_s = o_p->dat[i];
//      win_printf("plot ds %d error in y %s",i,(p_d_s->ye != NULL)?"yese":"no");
        qkd_color(PlotAxesColor, o_p->dat[i]->color);
        qdbox(p_d_s->symb);
        iopt = NOAXES + NOCLEAR;
        iopt |= o_p->iopt & (XLOG + YLOG);

        if (p_d_s->m == 2)     iopt |= DOT;

        if (p_d_s->m == 0)     iopt |= DASH;

        min = (p_d_s->nx > p_d_s->ny) ? p_d_s->ny : p_d_s->nx;

        qkdraw(min, p_d_s->xd, p_d_s->yd, iopt, &(o_p->x_lo), &(o_p->x_hi), &(o_p->y_lo), &(o_p->y_hi));

        if (p_d_s->xe != NULL)
        {
            iopt = NOAXES + NOCLEAR;
            iopt |= o_p->iopt & (XLOG + YLOG);
            iopt |= DASH;

            if (xe == NULL) xe = (float *)calloc(2 * min, sizeof(float));

            if (ye == NULL) ye = (float *)calloc(2 * min, sizeof(float));

            if (xe == NULL || ye == NULL) break;

            if (p_d_s->xed == NULL)
            {
                for (j = k = 0; j < min ; j++)
                {
                    ye[k] = p_d_s->yd[j];
                    xe[k++] = p_d_s->xd[j] - p_d_s->xe[j];
                    ye[k] = p_d_s->yd[j];
                    xe[k++] = p_d_s->xd[j] + p_d_s->xe[j];
                }
            }
            else
            {
                //asymetric error
                for (j = k = 0; j < min ; j++)
                {
                    ye[k] = p_d_s->yd[j];
                    xe[k++] = p_d_s->xd[j] - p_d_s->xed[j];
                    ye[k] = p_d_s->yd[j];
                    xe[k++] = p_d_s->xd[j] + p_d_s->xe[j];
                }

            }

            qdbox("\\pt4|");
            qkdraw(2 * min, xe, ye, iopt, &(o_p->x_lo), &(o_p->x_hi), &(o_p->y_lo), &(o_p->y_hi));
        }

        if (p_d_s->ye != NULL)
        {
//          win_printf("plot error in y");
            iopt = NOAXES + NOCLEAR;
            iopt |= o_p->iopt & (XLOG + YLOG);
            iopt |= DASH;

            if (xe == NULL) xe = (float *)calloc(2 * min, sizeof(float));

            if (ye == NULL) ye = (float *)calloc(2 * min, sizeof(float));

            if (xe == NULL || ye == NULL) break;

            if (p_d_s->yed == NULL)
            {
                for (j = k = 0; j < min ; j++)
                {
                    xe[k] = p_d_s->xd[j];
                    ye[k++] = p_d_s->yd[j] - p_d_s->ye[j];
                    xe[k] = p_d_s->xd[j];
                    ye[k++] = p_d_s->yd[j] + p_d_s->ye[j];
                }
            }
            else
            {
                //asymetric error
                for (j = k = 0; j < min ; j++)
                {
                    xe[k] = p_d_s->xd[j];
                    ye[k++] = p_d_s->yd[j] - p_d_s->yed[j];
                    xe[k] = p_d_s->xd[j];
                    ye[k++] = p_d_s->yd[j] + p_d_s->ye[j];
                }
            }

            qdbox("\\pt4\\line");
            qkdraw(2 * min, xe, ye, iopt, &(o_p->x_lo), &(o_p->x_hi), &(o_p->y_lo), &(o_p->y_hi));

        }

        if (xe != NULL)
        {
            free(xe);
            xe = NULL;
        }

        if (ye != NULL)
        {
            free(ye);
            ye = NULL;
        }


        if (p_d_s->xbu != NULL)
        {
            iopt = NOAXES + NOCLEAR;
            iopt |= o_p->iopt & (XLOG + YLOG);

            for (j = k = 0; j < min ; j++)
            {
                float curx_1 = p_d_s->xbd ? p_d_s->xd[j] - p_d_s->xbd[j] : p_d_s->xd[j] - p_d_s->xbu[j];
                float curx = p_d_s->xd[j];
                float curx1 = p_d_s->xd[j] + p_d_s->xbu[j];
                float cury1 = p_d_s->yd[j] + p_d_s->boxplot_width / 2;
                float cury_1 = p_d_s->yd[j] - p_d_s->boxplot_width / 2;

                float xb[7] = {curx, curx1, curx1, curx, curx_1, curx, curx};
                float yb[7] = {cury_1, cury_1, cury1, cury1, cury_1, cury_1, cury1};

                qdbox("\\pt4\\line");
                qkdraw(7, xb, yb, iopt, &(o_p->x_lo), &(o_p->x_hi), &(o_p->y_lo), &(o_p->y_hi));
            }

        }

        if (p_d_s->ybu != NULL)
        {
            iopt = NOAXES + NOCLEAR;
            iopt |= o_p->iopt & (XLOG + YLOG);

            for (j = k = 0; j < min ; j++)
            {
                float curx1 = p_d_s->xd[j] + p_d_s->boxplot_width / 2;
                float curx_1 = p_d_s->xd[j] - p_d_s->boxplot_width / 2;
                float cury_1 = p_d_s->ybd ? p_d_s->yd[j] - p_d_s->ybd[j] : p_d_s->yd[j] - p_d_s->ybu[j];
                float cury = p_d_s->yd[j];
                float cury1 = p_d_s->yd[j] + p_d_s->ybu[j];

                float xb[7] = {curx_1, curx_1, curx1, curx1, curx_1, curx_1, curx1};
                float yb[7] = {cury, cury1, cury1, cury, cury_1, cury, cury};

                qdbox("\\pt4\\line");
                qkdraw(7, xb, yb, iopt, &(o_p->x_lo), &(o_p->x_hi), &(o_p->y_lo), &(o_p->y_hi));
            }

        }
    }

    for (i = 0 ; i < o_p->n_lab ; i++)
    {
        qkd_lab_color(Lightred);
        p_p_l = o_p->lab[i];

        if (p_p_l->text == NULL)    continue;

# ifdef PR_DEFINE

        if (strncmp(p_p_l->text, "\\plot{", 6) == 0)
        {
            j = sscanf(p_p_l->text + 6, "%d", &tmp);

            if (j != 1)
                return  win_printf("wrong \\\\ plot command \n %s", p_p_l->text + 6);

            if (pr == NULL || tmp >= pr->n_op)
                return win_printf("cannot find plot");

            if (o_p == pr->o_p[tmp])
                return win_printf("you cannot include a plot in itself!\n silly guy...");

            if (p_p_l->type == USR_COORD)
                p_p_l->b = xlabel("a", p_p_l->xla, p_p_l->yla);
            else    if (p_p_l->type == ABS_COORD)
                p_p_l->b = xlabel("a", o_p->x_lo + (o_p->x_hi
                                                    - o_p->x_lo) * p_p_l->xla, o_p->y_lo + (o_p->y_hi - o_p->y_lo) * p_p_l->yla);
            else    if (p_p_l->type ==  VERT_LABEL_USR)
                p_p_l->b = ylabel("a", p_p_l->xla, p_p_l->yla);
            else    if (p_p_l->type ==  VERT_LABEL_ABS)
                p_p_l->b = ylabel("a", o_p->x_lo + (o_p->x_hi
                                                    - o_p->x_lo) * p_p_l->xla, o_p->y_lo + (o_p->y_hi - o_p->y_lo) * p_p_l->yla);


            if (p_p_l->b == NULL || p_p_l->b->child == NULL || p_p_l->b->child[0] == NULL)
                return win_printf("wrong label");

            p_p_l->b->child[0] = duplicate_box(p_p_l->b->child[0], &(pr->o_p[tmp]->bplt));

            if (p_p_l->b->child[0] == NULL)
                return win_printf("cannot duplicate plot");

            p_p_l->b->w = p_p_l->b->child[0]->w;
            p_p_l->b->h = p_p_l->b->child[0]->h;
            p_p_l->b->d = p_p_l->b->child[0]->d;
            p_p_l->b->child[0]->parent = p_p_l->b;
            rearrange_hic(p_p_l->b);
        }
        else
        {
# endif

            if (p_p_l->type == USR_COORD)
                p_p_l->b = xlabel(p_p_l->text, p_p_l->xla, p_p_l->yla);
            else    if (p_p_l->type == ABS_COORD)
                p_p_l->b = xlabel(p_p_l->text, o_p->x_lo + (o_p->x_hi - o_p->x_lo)
                                  * p_p_l->xla, o_p->y_lo + (o_p->y_hi - o_p->y_lo) * p_p_l->yla);
            else    if (p_p_l->type ==  VERT_LABEL_USR)
                p_p_l->b = ylabel(p_p_l->text, p_p_l->xla, p_p_l->yla);
            else    if (p_p_l->type ==  VERT_LABEL_ABS)
                p_p_l->b = ylabel(p_p_l->text, o_p->x_lo + (o_p->x_hi - o_p->x_lo)
                                  * p_p_l->xla, o_p->y_lo + (o_p->y_hi - o_p->y_lo) * p_p_l->yla);

# ifdef PR_DEFINE
        }

# endif
    }

    for (i = 0; i < o_p->n_dat; i++)
    {
        p_d_s = o_p->dat[i];

        for (j = 0 ; j < p_d_s->n_lab ; j++)
        {
            qkd_lab_color(Lightred);
            p_p_l = p_d_s->lab[j]; // was i !

            if (p_p_l->type == USR_COORD)
                p_p_l->b = xlabel(p_p_l->text, p_p_l->xla, p_p_l->yla);
            else    if (p_p_l->type == ABS_COORD)
                p_p_l->b = xlabel(p_p_l->text, o_p->x_lo + (o_p->x_hi - o_p->x_lo)
                                  * p_p_l->xla, o_p->y_lo + (o_p->y_hi - o_p->y_lo) * p_p_l->yla);
            else    if (p_p_l->type ==  VERT_LABEL_USR)
                p_p_l->b = ylabel(p_p_l->text, p_p_l->xla, p_p_l->yla);
            else    if (p_p_l->type ==  VERT_LABEL_ABS)
                p_p_l->b = ylabel(p_p_l->text, o_p->x_lo + (o_p->x_hi - o_p->x_lo)
                                  * p_p_l->xla, o_p->y_lo + (o_p->y_hi - o_p->y_lo) * p_p_l->yla);
        }
    }

    if (selected_point.text != NULL && selected_point_enable)
    {
        selected_point.b = xlabel(selected_point.text, selected_point.xla, selected_point.yla);
    }

    return 0;
}
int find_x_limits(O_p *op)
{
    register int i, j;
    int min;
    struct data_set *dsp = NULL;
    float tmp, minp = 1e34, tmpf;

    if (op == NULL || op->n_dat == 0)           return 1;

    for (i = 0 ; i < op->n_dat ; i++)
    {
        dsp = op->dat[i];

        if (dsp->invisible) continue;

        op->x_lo = op->x_hi = dsp->xd[0];
        break;
    }

    for (i = 0; i < op->n_dat ; i++)
    {
        dsp = op->dat[i];

        if (dsp->invisible) continue;

        min = (dsp->nx > dsp->ny) ? dsp->ny : dsp->nx;
        min = (dsp->mx > min) ? min : dsp->mx;
        min = (dsp->my > min) ? min : dsp->my;

        if (dsp->xe == NULL)
        {
            for (j = 0; j < min; j++)
            {
                if (dsp->xd[j] > op->x_hi) op->x_hi = dsp->xd[j];

                if (dsp->xd[j] < op->x_lo) op->x_lo = dsp->xd[j];

                minp = (dsp->xd[j] < minp && dsp->xd[j] > 0) ? dsp->xd[j] : minp;
            }
        }
        else
        {
            for (j = 0; j < min; j++)
            {
                tmpf = dsp->xd[j] + fabs(dsp->xe[j]);

                if (tmpf > op->x_hi) op->x_hi = tmpf;

                tmpf = dsp->xd[j] - fabs(dsp->xe[j]);

                if (tmpf < op->x_lo) op->x_lo = tmpf;

                minp = (tmpf < minp && tmpf > 0) ? tmpf : minp;
            }
        }
    }

    if (op->iopt & XLOG) op->x_lo = minp;

    tmp = op->x_hi - op->x_lo;

    if (tmp == 0)       tmp = op->x_hi + op->x_lo;

    tmp = (tmp >= 0) ? tmp : -tmp;

    if (op->iopt & XLOG)
    {
        if (tmp == 0)       tmp = 1e-12;

        tmp = (op->x_lo > 0) ? op->x_hi / op->x_lo : 1e-3;
        tmp = (tmp > 0) ? (log(tmp)) / 20 : (log(-tmp)) / 20;
        op->x_hi *= exp(tmp);
        op->x_lo *= exp(-tmp);
    }
    else
    {
        if (tmp == 0)       tmp = 1e-2;

        op->x_lo -= tmp / 20;
        op->x_hi += tmp / 20;
    }

    return 0;
}
int find_y_limits(O_p *op)
{
    register int i, j;
    int min;
    struct data_set *dsp = NULL;
    float tmp, minp = 1e34, tmpf;

    if (op == NULL || op->n_dat == 0)           return 1;

    for (i = 0, op->y_lo = op->y_hi = 0; i < op->n_dat ; i++)
    {
        dsp = op->dat[i];

        if (dsp->invisible) continue;

        op->y_lo = op->y_hi = dsp->yd[0];
        break;
    }

    for (i = 0; i < op->n_dat ; i++)
    {
        dsp = op->dat[i];

        if (dsp->invisible) continue;

        min = (dsp->nx > dsp->ny) ? dsp->ny : dsp->nx;
        min = (dsp->mx > min) ? min : dsp->mx;
        min = (dsp->my > min) ? min : dsp->my;

        if (dsp->ye == NULL)
        {
            for (j = 0; j < min; j++)
            {
                if (dsp->yd[j] > op->y_hi) op->y_hi = dsp->yd[j];

                if (dsp->yd[j] < op->y_lo) op->y_lo = dsp->yd[j];

                minp = (dsp->yd[j] < minp && dsp->yd[j] > 0) ? dsp->yd[j] : minp;
            }
        }
        else
        {
            for (j = 0; j < min; j++)
            {
                tmpf = dsp->yd[j] + fabs(dsp->ye[j]);

                if (tmpf > op->y_hi) op->y_hi = tmpf;

                tmpf = dsp->yd[j] - fabs(dsp->ye[j]);

                if (tmpf < op->y_lo) op->y_lo = tmpf;

                minp = (tmpf < minp && tmpf > 0) ? tmpf : minp;
            }
        }

    }

    if (op->iopt & YLOG) op->y_lo = minp;

    tmp = op->y_hi - op->y_lo;

    if (tmp == 0)       tmp = op->y_hi + op->y_lo;

    tmp = (tmp >= 0) ? tmp : -tmp;

    if (op->iopt & YLOG)
    {
        if (tmp == 0)       tmp = 1e-12;

        tmp = (op->y_lo > 0) ? op->y_hi / op->y_lo : 1e-3;
        tmp = (tmp > 0) ? (log(tmp)) / 20 : (log(-tmp)) / 20;
        op->y_hi *= exp(tmp);
        op->y_lo *= exp(-tmp);
    }
    else
    {
        if (tmp == 0)       tmp = 1e-2;

        op->y_lo -= tmp / 20;
        op->y_hi += tmp / 20;
    }

    return 0;
}
# endif
