# ifndef _IM_GR_C_
# define _IM_GR_C_

# include "im_gr.h"
# include "xvin.h"

# include <stdlib.h>
# include <string.h>
#include <locale.h>
#ifdef XV_MAC
#include <stdio.h>
#endif

char 	data_path[512];
int 	swap_mode = 0;
int		movie_buffer_on_disk = 0;

int load_im_file_in_imreg(imreg *imr, char *file, char *path)
{
  register int i;
	O_i *oi = NULL;

	if (imr == NULL || file == NULL || path == NULL)
		return xvin_error(Wrong_Argument);
	snprintf(f_in,256,"%s%s",path,file);
	if (check_if_gr_file_is_an_image(f_in) == 0)
	  {
	    i = win_printf("%s\nlooks like gr file containing a plot!\n"
			   "I recommend to skip loading by pressing WIN_CANCEL\n"
			   ,backslash_to_slash(f_in));
	    if (i == WIN_CANCEL) return D_O_K;
	  }

	if (imr->n_oi == 1 && imr->one_i->im.pixel == NULL) 	oi = imr->one_i;
	else
	{
		oi = (O_i *)calloc(1,sizeof(O_i));
		init_one_image(oi, 0);
		add_image (imr, IS_CHAR_IMAGE, (void *)oi);
	}
	imr->cur_oi = imr->n_oi - 1;
	imr->one_i = imr->o_i[imr->cur_oi];
	imr->one_i->need_to_refresh |= ALL_NEED_REFRESH;
	if (movie_buffer_on_disk) oi->im.movie_on_disk = 1;
	if ( imreadfile(oi,f_in) < MAX_ERROR)
	{
		oi->filename = strdup(file);
		oi->dir = Mystrdup(path);
/*		win_printf("source %s",(oi->im.source == NULL)?"null":oi->im.source);*/
		if (oi->im.source == NULL)		oi->im.source = strdup(file);
		if (image_special_2_use != NULL) image_special_2_use(oi);
/*		refresh_image(imr, imr->n_oi - 1);	*/
	}
	else
	{
		if (imr->n_oi > 1) remove_from_image (imr, oi->im.data_type, (void *)oi);
		//win_printf("bef remove oi");
		//refresh_image(imr, UNCHANGED);
		win_printf ("cannot open file\n%s",backslash_to_slash(i_f[cur_i_f].filename));

/*		win_printf("file %s \n not loaded",backslash_to_slash(f_in)); */
		return 1;
	}
	return 0;
}


O_i *load_im_fullfile_in_oi(char *fullfile)
{
  O_i *oi = NULL;
  char path[1024] = {0}, file[1024] = {0};

  if (fullfile == NULL)  return NULL;
  oi = (O_i *)calloc(1,sizeof(O_i));
  if (oi == NULL) return NULL;
  init_one_image(oi, 0);

  if ( imreadfile(oi,fullfile) < MAX_ERROR)
    {
      extract_file_path(path, 1024, fullfile);
      put_backslash(path);
      extract_file_name(file, 1024, fullfile);
      oi->filename = strdup(file);
      oi->dir = Mystrdup(path);
      if (oi->im.source == NULL)     oi->im.source = strdup(file);
      if (image_special_2_use != NULL) image_special_2_use(oi);
    }
  else
    {
      free_one_image(oi);
      return NULL;
    }
  return oi;
}


O_i *load_im_file_in_oi(char *file, char *path)
{
  O_i *oi = NULL;

  if (file == NULL)  return NULL;
  if (path != NULL)	snprintf(f_in,256,"%s%s",path,file);
  else snprintf(f_in,256,"%s",file);
  oi = (O_i *) calloc(1,sizeof(O_i));
  if (oi == NULL) return NULL;
  init_one_image(oi, 0);
  if (movie_buffer_on_disk) oi->im.movie_on_disk = 1;

  if ( imreadfile(oi,f_in) < MAX_ERROR)
    {
      oi->filename = strdup(file);
      oi->dir = Mystrdup(path);
      if (oi->im.source == NULL)     oi->im.source = strdup(file);
      if (image_special_2_use != NULL) image_special_2_use(oi);
    }
  else
    {
      free_one_image(oi);
      return NULL;
    }
  return oi;
}

int check_if_gr_file_is_an_image(char *file_name)
{
  register int i;
  char buf[2048] = {0};
  int nx, nxs,  done = 0, is_im = 0;
  FILE *fp = NULL;

  if (file_name == NULL) return -1;
  fp = fopen(file_name,"r");
  if (fp == NULL)	 return -1;
  for (i = 0, done = 0; (i < 64) && (done == 0); i++)
    {
      if (fgets(buf,2048,fp) == NULL)	done = 1;
      i = sscanf(buf,"-nx %d %d",&nx,&nxs);
      //win_printf("%s\n%d",buf,i);
      if (i > 1)
	{
	  done = 1;
	  is_im = 1;
	}
    }
  //win_printf("-nx search %d ",is_im);
  fclose(fp);
  return is_im;
}

/*	int imreadfile(O_i *oi, char *file_name)
 *	DESCRIPTION	read input and translate graph option and data
 *
 *
 *
 *	RETURNS		0 on success, 1
 *
 */
int imreadfile(O_i *oi, char *file_name)
{
    register int i, j, k;
    int load_abort = 0;
    float tmpx, tmpy;
    char *c1 = NULL;
    char *c2 = NULL;
    char *line = NULL;
    char *line1 = NULL;
    char 	**agv = NULL;
    char **agvk = NULL;
    int 	agc = 0;

    setlocale(LC_ALL, "C");
	if (oi == NULL) return MAX_ERROR;
	cur_i_f = 0;
	n_error = 0;
	oi->z_black = oi->z_white = 0;
	i_f[cur_i_f].n_line = 0;
	i_f[cur_i_f].filename = strdup(file_name);
	i_f[cur_i_f].fpi = fopen(i_f[cur_i_f].filename,"r");
	if ( i_f[cur_i_f].fpi == NULL)
	{
		return MAX_ERROR;
	}
	line = (char *)calloc(B_LINE,sizeof(char));
	line1 = (char *)calloc(B_LINE,sizeof(char));
	lferror = (char *)calloc(B_LINE,sizeof(char));
	agv = (char **)calloc(OP_SIZE,sizeof(char*));
	agvk = (char **)calloc(OP_SIZE,sizeof(char*));

	if ( line == NULL || line1 == NULL || lferror == NULL || agv == NULL || agvk == NULL)
	{
		win_printf("malloc error");
		return MAX_ERROR;
	}
	if (extract_file_path(data_path, sizeof(data_path), file_name) == NULL)
		win_printf("cannot find im path");

	while ( load_abort < MAX_ERROR)
	{
		while ((c1 = get_next_line(line)) == NULL && (cur_i_f > 0))
		{
			if (i_f[cur_i_f].fpi != NULL)
				fclose(i_f[cur_i_f].fpi);
			i_f[cur_i_f].fpi = NULL;
			cur_i_f--;
		}
		if ( c1 == NULL)	break;
		line1[0]=0;
		i = sscanf(line,"%f", &tmpx);
		if (i == 1)	i = sscanf(line,"%f%f", &tmpx, &tmpy);
		if (i == 2)	i = sscanf(line,"%f%f%s",&tmpx,&tmpy,line1);
		if (i == 3) /* may be a label */
		{
			if ( line1[0] == '%') /* start a comment */
			{
				i = 2;
				j = 0;
			}
			else if ( line1[0] == '"')/*start as a label*/
			{
				c1 = strchr(line,'"');
				c2 = line1;
				j = get_label(&c1, &c2, line);
			}
			else
			{
				load_abort = error_in_file ("a label must start and end\nwith a double quote !...\n->%s",line);
				j = 0;
			}
			if ( j ) push_image_label(oi,tmpx,tmpy,c2,USR_COORD);
			else if ( i == 3 )
			{
				load_abort = error_in_file("empty label !...\n->%s\n",line);
			}
		}
		if ( i == 2 || i == 1)	/* may be a data point x,y !*/
		{
			load_abort = error_in_file("you can't input an x,y data point\nfor an image... use a gr region\n->%s",line);
		}
		if ( i == 0 )	/* a command line */
		{
			/* advance to 1st item */
		  if (c1 != NULL)
		    {
			while (*c1 == ' '||*c1 == '\t'||*c1 == '#') c1++;
			for (k = 0 ; k < agc ; k++)
			{
				if (agvk[k] != NULL) 		free(agvk[k]);
				agvk[k] = NULL;
			}
			agc = 1;
			while (  (*c1 != 0) )
			{
				if ( *c1 == '%')	*c1 = 0;
				else if ( *c1 != '"')
				{
					if (sscanf(c1,"%s",line1) == 1)
					{
						agvk[agc] = agv[agc] = strdup(line1);
            printf("%s",line1);
						agc++;
					}
					if (agc >= OP_SIZE)
					{
						error_in_file("too many options\nin input line\n->%s",line);
						load_abort = MAX_ERROR;
					}
					if (c1 != NULL && strchr(c1,' ') !=NULL)	c1 = strchr ( c1,' ');
					else if (c1 != NULL && strchr(c1,'\t') !=NULL) c1 = strchr (c1,'\t');
					else	*c1 = 0;
				}
				else
				{
					c2 = line1;
					c2[0] = 0;
					if (get_label(&c1,&c2, line))
					{
						agvk[agc] = agv[agc] = strdup(c2);
						agc++;
					}
					if (agc >= OP_SIZE)
					{
						error_in_file("too many options\nin input line\n->%s",line);
						load_abort = MAX_ERROR;
					}
				}
				if (c1 != NULL)
				  {
				    while ( *c1 == ' ' || *c1 == '\t' || *c1 == '\n' )
					c1++;
				  }
			}
		    }
		  if (agc > 1)
		    {
		      if (set_image_opts(oi, agc, agv) == MAX_ERROR)
			load_abort = MAX_ERROR;
		    }
		  for (k = 0 ; k < agc ; k++)
		    {
		      if (agvk[k] != NULL) 		free(agvk[k]);
		      agvk[k] = NULL;
		    }

		  /*			for ( ; agc > 0 ; agc--)	free(agvk[agc]);*/
		  agc = 1;
		}
	}
	if (line) free(line);
	if (line1) free(line1);
	if (agv) free(agv);
	if (agvk) free(agvk);
	if (lferror) free(lferror);

	while ( cur_i_f >= 0 )
	{
		if (i_f[cur_i_f].fpi != NULL)		fclose(	i_f[cur_i_f].fpi);
		if (i_f[cur_i_f].filename != NULL)
		  {
		    free(i_f[cur_i_f].filename);
		    i_f[cur_i_f].filename = NULL;
		  }
		cur_i_f--;
	}
	for (i = 0, j = 1, oi->c_xu = 0; i < oi->n_xu && j != 0 ; i++)
	{
		j = 0;
		if (oi->ax != oi->xu[i]->ax) 	j = 1;
		if (oi->dx != oi->xu[i]->dx) 	j = 1;
		if (oi->x_unit == NULL || oi->xu[i]->name == NULL)	j = 1;
	 	else if (strncmp(oi->xu[i]->name,oi->x_unit,strlen(oi->x_unit)) != 0)	j = 1;
		if (j == 0) 	oi->c_xu = i;
	}
	for (i = 0, j = 1, oi->c_yu = 0; i < oi->n_yu && j != 0 ; i++)
	{
		j = 0;
		if (oi->ay != oi->yu[i]->ax) 	j = 1;
		if (oi->dy != oi->yu[i]->dx) 	j = 1;
		if (oi->y_unit == NULL || oi->yu[i]->name == NULL)	j = 1;
	 	else if (strncmp(oi->yu[i]->name,oi->y_unit,strlen(oi->y_unit)) != 0)	j = 1;
		if (j == 0) 	oi->c_yu = i;
	}
	for (i = 0, j = 1, oi->c_zu = 0; i < oi->n_zu && j != 0 ; i++)
	{
		j = 0;
		if (oi->az != oi->zu[i]->ax) 	j = 1;
		if (oi->dz != oi->zu[i]->dx) 	j = 1;
		if (oi->z_unit == NULL || oi->zu[i]->name == NULL)	j = 1;
	 	else if (strncmp(oi->zu[i]->name,oi->z_unit,strlen(oi->z_unit)) != 0)	j = 1;
		if (j == 0) 	oi->c_zu = i;
	}
	for (i = 0, j = 1, oi->c_tu = 0; i < oi->n_tu && j != 0 ; i++)
	{
		j = 0;
		if (oi->at != oi->tu[i]->ax) 	j = 1;
		if (oi->dt != oi->tu[i]->dx) 	j = 1;
		if (oi->t_unit == NULL || oi->tu[i]->name == NULL)	j = 1;
	 	else if (strncmp(oi->tu[i]->name,oi->t_unit,strlen(oi->t_unit)) != 0)	j = 1;
		if (j == 0) 	oi->c_tu = i;
	}
	return n_error;
}

int set_image_opts(O_i *oi, int argc, char **argv)
{
	char file_name[512] = {0}, *cmd	= NULL, *tmpch	= NULL;
	float templ, temp1;
	int itemp, decade = 0, type = 0, subtype = 0;
	un_s *un = NULL;

	if (oi == NULL) return MAX_ERROR;
	file_name[0] = 0;
	while(--argc > 0)
	{
		argv++;
		cmd = argv[0];
again:		switch(argv[0][0])
		{
			case '-':		/* option delimeter */
			argv[0]++;
			goto again;
			case 'i':		/* input file */
			if ( strncmp(argv[0],"imzz",4) == 0 )
			{
				push_image(oi,f_in,IS_COMPLEX_IMAGE,CRT_Z);
			}
			if ( strncmp(argv[0],"imzdbz",6) == 0 )
			{
				push_image(oi,f_in,IS_COMPLEX_DOUBLE_IMAGE,CRT_Z);
			}
			else if ( strncmp(argv[0],"imzdb",5) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					if (data_path[0] != 0)
					  snprintf(file_name,sizeof(file_name),"%s%s",data_path,argv[0]);
					else snprintf(file_name,sizeof(file_name),"%s",argv[0]);
					push_image(oi,file_name,IS_COMPLEX_DOUBLE_IMAGE,0);
				}
			}
			else if ( strncmp(argv[0],"imz",3) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					if (data_path[0] != 0)
					  snprintf(file_name,sizeof(file_name),"%s%s",data_path,argv[0]);
					else snprintf(file_name,sizeof(file_name),"%s",argv[0]);
					push_image(oi,file_name,IS_COMPLEX_IMAGE,0);
				}
			}
			if ( strncmp(argv[0],"imfz",4) == 0 )
			{
				push_image(oi,f_in,IS_FLOAT_IMAGE,CRT_Z);
			}
			else if ( strncmp(argv[0],"imf",3) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					if (data_path[0] != 0)
					  snprintf(file_name,sizeof(file_name),"%s%s",data_path,argv[0]);
					else snprintf(file_name,sizeof(file_name),"%s",argv[0]);
					push_image(oi,file_name,IS_FLOAT_IMAGE,0);
				}
			}
			if ( strncmp(argv[0],"imdbz",5) == 0 )
			{
				push_image(oi,f_in,IS_DOUBLE_IMAGE,CRT_Z);
			}
			else if ( strncmp(argv[0],"imdb",4) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					if (data_path[0] != 0)
					  snprintf(file_name,sizeof(file_name),"%s%s",data_path,argv[0]);
					else snprintf(file_name,sizeof(file_name),"%s",argv[0]);
					push_image(oi,file_name,IS_DOUBLE_IMAGE,0);
				}
			}
			else 	if ( strncmp(argv[0],"imiz",4) == 0 )
			{
				push_image(oi,f_in,IS_INT_IMAGE,CRT_Z);
			}
			else 	if ( strncmp(argv[0],"imi",3) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
/*
					if (data_path[0] != 0)
						strcpy(file_name,data_path);
					strcat(file_name,argv[0]);*/
					build_full_file_name(file_name, 512, data_path, argv[0]);
					push_image(oi,file_name,IS_INT_IMAGE,0);
				}
			}
			else 	if ( strncmp(argv[0],"imuiz",4) == 0 )
			{
				push_image(oi,f_in,IS_UINT_IMAGE,CRT_Z);
			}
			else 	if ( strncmp(argv[0],"imui",3) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
/*
					if (data_path[0] != 0)
						strcpy(file_name,data_path);
					strcat(file_name,argv[0]);*/
					build_full_file_name(file_name, 512, data_path, argv[0]);
					push_image(oi,file_name,IS_UINT_IMAGE,0);
				}
			}
			else 	if ( strncmp(argv[0],"imliz",4) == 0 )
			{
				push_image(oi,f_in,IS_LINT_IMAGE,CRT_Z);
			}
			else 	if ( strncmp(argv[0],"imli",3) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
/*
					if (data_path[0] != 0)
						strcpy(file_name,data_path);
					strcat(file_name,argv[0]);*/
					build_full_file_name(file_name, 512, data_path, argv[0]);
					push_image(oi,file_name,IS_LINT_IMAGE,0);
				}
			}
			else 	if ( strncmp(argv[0],"imcz",4) == 0 )
			{
				push_image(oi,f_in,IS_CHAR_IMAGE,CRT_Z);
			}
			else 	if ( strncmp(argv[0],"imc",3) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					if (data_path[0] != 0)
					  snprintf(file_name,sizeof(file_name),"%s%s",data_path,argv[0]);
					else snprintf(file_name,sizeof(file_name),"%s",argv[0]);
					push_image(oi,file_name,IS_CHAR_IMAGE,0);
				}
			}
			else 	if ( strncmp(argv[0],"imrgbz",6) == 0 )
			{
				push_image(oi,f_in,IS_RGB_PICTURE,CRT_Z);
			}
			else 	if ( strncmp(argv[0],"imrgb16z",8) == 0 )
			{
				push_image(oi,f_in,IS_RGB16_PICTURE,CRT_Z);
			}
			else 	if ( strncmp(argv[0],"imrgbaz",7) == 0 )
			{
				push_image(oi,f_in,IS_RGBA_PICTURE,CRT_Z);
			}
			else 	if ( strncmp(argv[0],"imrgba16z",9) == 0 )
			{
				push_image(oi,f_in,IS_RGBA16_PICTURE,CRT_Z);
			}
			else 	if ( strncmp(argv[0],"imrgba",6) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					if (data_path[0] != 0)
					  snprintf(file_name,sizeof(file_name),"%s%s",data_path,argv[0]);
					else snprintf(file_name,sizeof(file_name),"%s",argv[0]);
					push_image(oi,file_name,IS_RGBA_PICTURE,0);
				}
			}
			else 	if ( strncmp(argv[0],"imrgb",5) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					if (data_path[0] != 0)
					  snprintf(file_name,sizeof(file_name),"%s%s",data_path,argv[0]);
					else snprintf(file_name,sizeof(file_name),"%s",argv[0]);
					push_image(oi,file_name,IS_RGB_PICTURE,0);
				}
			}
			else 	if ( strncmp(argv[0],"imrgba16",8) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					if (data_path[0] != 0)
					  snprintf(file_name,sizeof(file_name),"%s%s",data_path,argv[0]);
					else snprintf(file_name,sizeof(file_name),"%s",argv[0]);
					push_image(oi,file_name,IS_RGBA16_PICTURE,0);
				}
			}
			else 	if ( strncmp(argv[0],"imrgb16",7) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					if (data_path[0] != 0)
					  snprintf(file_name,sizeof(file_name),"%s%s",data_path,argv[0]);
					else snprintf(file_name,sizeof(file_name),"%s",argv[0]);
					push_image(oi,file_name,IS_RGB16_PICTURE,0);
				}
			}

			else if (argc >= 2)
			{
				argc--;
				argv++;
				cur_i_f++;
				if (cur_i_f < I_F_SIZE)
				{
					if (data_path[0] != 0)
					  snprintf(file_name,sizeof(file_name),"%s%s",data_path,argv[0]);
					else snprintf(file_name,sizeof(file_name),"%s",argv[0]);
					i_f[cur_i_f].n_line = 0;
					i_f[cur_i_f].filename = strdup(file_name);
					i_f[cur_i_f].fpi = fopen(i_f[cur_i_f].filename,"r");
					if ( i_f[cur_i_f].fpi == NULL)
					{
						error_in_file("cannot open file\n%s",i_f[cur_i_f].filename);
						return MAX_ERROR;
					}
				}
				else
				{
					error_in_file("I cannot handle more\nthan %d nested files",I_F_SIZE);
					return MAX_ERROR;
				}
			}
			break;
			case 'l':		/* label for plot */
			if ( strncmp(argv[0],"lxp",3) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					oi->x_prime_title = Mystrdupre(oi->x_prime_title, argv[0]);
				}
			}
			else if ( strncmp(argv[0],"lx",2) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					oi->x_title = Mystrdupre(oi->x_title, argv[0]);
				}
			}
			else if ( strncmp(argv[0],"lyp",3) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					oi->y_prime_title = Mystrdupre(oi->y_prime_title, argv[0]);
				}
			}
			else if ( strncmp(argv[0],"ly",2) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					oi->y_title = Mystrdupre(oi->y_title, argv[0]);
				}
			}
			else if ( strncmp(argv[0],"luw",3) == 0 )
			{
				if (argc >= 4)
				{
					if(!gr_numb(&templ,&argc,&argv))	break;
					if(!gr_numb(&temp1,&argc,&argv)) break;
					argc--;
					argv++;
					push_image_label(oi,templ,temp1,argv[0],USR_COORD+WHITE_LABEL);
				}
			}
			else if ( strncmp(argv[0],"lrw",3) == 0 )
			{
				if (argc >= 4)
				{
					if(!gr_numb(&templ,&argc,&argv))	break;
					if(!gr_numb(&temp1,&argc,&argv)) break;
					argc--;
					argv++;
					push_image_label(oi,templ,temp1,argv[0],ABS_COORD+WHITE_LABEL);
				}
			}
			else if ( strncmp(argv[0],"lvw",3) == 0 )
			{
				if (argc >= 4)
				{
					if(!gr_numb(&templ,&argc,&argv))	break;
					if(!gr_numb(&temp1,&argc,&argv)) break;
					argc--;
					argv++;
					push_image_label(oi,templ,temp1,argv[0],VERT_LABEL_USR+WHITE_LABEL);
				}
			}
			else if ( strncmp(argv[0],"lv",2) == 0 )
			{
				if (argc >= 4)
				{
					if(!gr_numb(&templ,&argc,&argv))	break;
					if(!gr_numb(&temp1,&argc,&argv)) break;
					argc--;
					argv++;
					push_image_label(oi,templ,temp1,argv[0],VERT_LABEL_USR);
				}
			}
			else if ( strncmp(argv[0],"lr",2) == 0 )
			{
				if (argc >= 4)
				{
					if(!gr_numb(&templ,&argc,&argv))	break;
					if(!gr_numb(&temp1,&argc,&argv)) break;
					argc--;
					argv++;
					push_image_label(oi,templ,temp1,argv[0],ABS_COORD);
				}
			}
			else
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					oi->title = Mystrdupre(oi->title, argv[0]);
				}
			}
			break;
			case 's':
			if ( strncmp(argv[0],"src",3) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
/*					win_printf("source0 %s",(oi->im.source == NULL)?"null":oi->im.source);			*/
					oi->im.source = Mystrdupre(oi->im.source, argv[0]);
/*					win_printf("source1 %s",(oi->im.source == NULL)?"null":oi->im.source);*/
				}
			}
			if ( strncmp(argv[0],"special",7) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					add_one_image(oi,IS_SPECIAL,(void*)argv[0]);
				}
			}
			break;
			case 'n':
			if ( argv[0][1] == 'x')
			{
				if (!gr_numbi(&(oi->im.nx),&argc,&argv))	break;
				if (!gr_numbi(&(oi->im.nxs),&argc,&argv))	break;
				if (!gr_numbi(&(oi->im.nxe),&argc,&argv))	break;
			}
			else if ( argv[0][1] == 'y')
			{
				if (!gr_numbi(&(oi->im.ny),&argc,&argv))		break;
				if (!gr_numbi(&(oi->im.nys),&argc,&argv))		break;
				if (!gr_numbi(&(oi->im.nye),&argc,&argv))		break;
			}
			else if ( argv[0][1] == 'f')
			{
				if (!gr_numbi(&(oi->im.n_f),&argc,&argv))		break;
				if (oi->im.movie_on_disk) oi->im.n_f = -abs(oi->im.n_f);
				if (!gr_numbi(&(oi->im.c_f),&argc,&argv))		break;
			}
			else
			{
				return error_in_file("%s :Invalid argument\n",cmd);
			}
			break;
			case 'p':		/* prefix and unit */
			if ( argv[0][1] == 'x' )
			{
				if (argc >= 3)
				{
					argc--;
					argv++;
					if (argv[0][0] != '!')
						oi->x_prefix = Mystrdupre(oi->x_prefix, argv[0]);
					argc--;
					argv++;
					if (argv[0][0] != '!')
						oi->x_unit = Mystrdupre(oi->x_unit, argv[0]);
				}
				else
				{
					return error_in_file("-p prefix unit:\nInvalid argument\n%s",cmd);
				}
			}
			else if ( argv[0][1] == 'y' )
			{
				if (argc >= 3)
				{
					argc--;
					argv++;
					if ( argv[0][0] != '!' )
						oi->y_prefix = Mystrdupre(oi->y_prefix, argv[0]);
					argc--;
					argv++;
					if ( argv[0][0] != '!' )
						oi->y_unit = Mystrdupre(oi->y_unit, argv[0]);
				}
				else
				{
					return error_in_file("-p prefix unit:\nInvalid argument\n%s",cmd);
				}
			}
			break;
			case 'd':		/* output device */
			if ( strncmp(argv[0],"duration",8) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					if (sscanf(argv[0],"%lf",&(oi->im.record_duration)) != 1)
					{
						return error_in_file("Improper duration\n\\it %s %s",cmd,argv[0]);
					}
				}
			}

			else if ( argv[0][1] == 'p' )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					if (strncpy(data_path,argv[0],sizeof(data_path)) == NULL)
						fprintf(stderr,"not a valid path %s\n",argv[0]);
				}
				else
				{
					return error_in_file("-dp data_path:\nInvalid argument\n%s",cmd);
				}
			}
			break;
			case 'a':		/* automatic abscissas */
			if ( argv[0][1] == 'x')
			{
				oi->dx = 1;
				if (!gr_numb(&(oi->dx),&argc,&argv))	break;
				if (!gr_numb(&(oi->ax),&argc,&argv))	break;
			}
			else if ( argv[0][1] == 'y')
			{
				oi->dy = 1;
				if (!gr_numb(&(oi->dy),&argc,&argv))	break;
				if (!gr_numb(&(oi->ay),&argc,&argv))	break;
			}
			else if ( argv[0][1] == 'z')
			{
				oi->dz = 1;
				if (!gr_numb(&(oi->dz),&argc,&argv))	break;
				if (!gr_numb(&(oi->az),&argc,&argv))	break;
			}
			else
			{
				return error_in_file("Invalid option\n%s",cmd);
			}
			break;
			case 'g':		/* grid style */
			if (!gr_numb(&templ,&argc,&argv))
				itemp = argv[0][1]-'0';
			else
				itemp = templ;
			switch (itemp)
			{
				case -'0':	/* null character */
				case 0:
					oi->iopt |= NOAXES;/*ioit+=NOAXES;*/
					break;
				case 1:
					oi->iopt |= TRIM;/*ioit += TRIM;*/
					break;
				case 3:
					oi->iopt |= AXES_PRIME;
					break;
			}
			break;
			case 't':		/* transpose x and y */
			if ( strncmp(argv[0],"tus",3) == 0 )
			{
				if (argc < 4)
					return error_in_file("-tus :\nInvalid argument\n%s",cmd);
				itemp = 0; templ = 1; temp1 = 0;
				if (!gr_numb(&templ,&argc,&argv))		itemp = 1;
				if (!gr_numb(&temp1,&argc,&argv))		itemp = 1;
				if(itemp)
					return error_in_file("-tus :\nInvalid argument\n%s",cmd);
				argc--;				argv++;
				tmpch = strdup(argv[0]);
				type = 0; decade = 0; subtype = 0;
				if (argc >= 3)
				{
					if (!gr_numbi(&type,&argc,&argv))		itemp = 1;
					if (!gr_numbi(&decade,&argc,&argv))	itemp = 1;
					if (!gr_numbi(&subtype,&argc,&argv))	itemp = 2;
					if (itemp == 1)
						return error_in_file("-tus :\nInvalid argument\n%s",cmd);
				}
				else if (unit_to_type(tmpch,&type,&decade))
					return error_in_file("-tus :\nInvalid argument\n%s",cmd);
				un = build_unit_set(type, temp1, templ, (char)decade, 0, tmpch);
				if (tmpch)  {free(tmpch); tmpch = NULL;}
				if (un == NULL)		return error_in_file("-tus :\ncant create\n%s",cmd);
				un->sub_type = subtype;
				add_one_image (oi, IS_T_UNIT_SET, (void *)un);
			}

			if ( argv[0][1] == 'k')
				gr_numb(&(oi->tick_len),&argc,&argv);
			else	if ( strncmp(argv[0],"treat",5) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					oi->im.treatement = Mystrdupre(oi->im.treatement, argv[0]);
				}
			}
			else	if ( strncmp(argv[0],"time",5) == 0 )
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					if (sscanf(argv[0],"%lu",&(oi->im.time)) != 1)
					{
						return error_in_file("Improper time\n\\it %s %s",cmd,argv[0]);
					}
				}
			}
			else		oi->iopt |= CROSS;
			break;
			case 'x':		/* x limits */
			if ( strncmp(argv[0],"x-periodic",10) == 0 )
			{
				oi->im.win_flag |= X_PER;
			}
			else if ( strncmp(argv[0],"xus",3) == 0 )
			{
				if (argc < 4)
					return error_in_file("-xus :\nInvalid argument\n%s",cmd);
				itemp = 0; templ = 1; temp1 = 0;
				if (!gr_numb(&templ,&argc,&argv))		itemp = 1;
				if (!gr_numb(&temp1,&argc,&argv))		itemp = 1;
				if(itemp)
					return error_in_file("-xus :\nInvalid argument\n%s",cmd);
				argc--;				argv++;
				tmpch = strdup(argv[0]);
				type = 0; decade = 0; subtype = 0;
				if (argc >= 3)
				{
					if (!gr_numbi(&type,&argc,&argv))		itemp = 1;
					if (!gr_numbi(&decade,&argc,&argv))	itemp = 1;
					if (!gr_numbi(&subtype,&argc,&argv))	itemp = 2;
					if (itemp == 1)
						return error_in_file("-xus :\nInvalid argument\n%s",cmd);
				}
				else if (unit_to_type(tmpch,&type,&decade))
					return error_in_file("-xus :\nInvalid argument\n%s",cmd);
				un = build_unit_set(type, temp1, templ, (char)decade, 0, tmpch);
				if (tmpch) {free(tmpch);  tmpch = NULL;}
				if (un == NULL)		return error_in_file("-xus :\ncant create\n%s",cmd);
				un->sub_type = subtype;
				add_one_image (oi, IS_X_UNIT_SET, (void *)un);
			}
			else
			{
				if ( argv[0][1] == 'n')	oi->iopt2 &= ~X_NUM;
				else			oi->iopt2 |= X_NUM;
				imxlimread(oi,&argc,&argv);
			}
			break;
			case 'y':		/* y limits */
			if ( strncmp(argv[0],"y-periodic",10) == 0 )
			{
				oi->im.win_flag |= Y_PER;
			}
			else if ( strncmp(argv[0],"yus",3) == 0 )
			{
				if (argc < 4)
					return error_in_file("-yus :\nInvalid argument\n%s",cmd);
				itemp = 0; templ = 1; temp1 = 0;
				if (!gr_numb(&templ,&argc,&argv))	itemp = 1;
				if (!gr_numb(&temp1,&argc,&argv))	itemp = 1;
				if(itemp)
					return error_in_file("-yus :\nInvalid argument\n%s",cmd);
				argc--;				argv++;
				tmpch = strdup(argv[0]);
				type = 0; decade = 0; subtype = 0;
				if (argc >= 3)
				{
					if (!gr_numbi(&type,&argc,&argv))		itemp = 1;
					if (!gr_numbi(&decade,&argc,&argv))	itemp = 1;
					if (!gr_numbi(&subtype,&argc,&argv))	itemp = 2;
					if (itemp == 1)
						return error_in_file("-yus :\nInvalid argument\n%s",cmd);
				}
				else if (unit_to_type(tmpch,&type,&decade))
					return error_in_file("-yus :\nInvalid argument\n%s",cmd);
				un = build_unit_set(type, temp1, templ, (char)decade, 0, tmpch);
				if (tmpch) {free(tmpch);  tmpch = NULL;}
				if (un == NULL)		return error_in_file("-yus :\ncant create\n%s",cmd);
				un->sub_type = subtype;
				add_one_image (oi, IS_Y_UNIT_SET, (void *)un);
			}
			else
			{
				if ( argv[0][1] == 'n')	oi->iopt2 &= ~Y_NUM;
				else			oi->iopt2 |= Y_NUM;
				imylimread(oi,&argc,&argv);
			}
			break;
			case 'z':		/* y limits */
			if ( strncmp(argv[0],"zus",3) == 0 )
			{
				if (argc < 4)
					return error_in_file("-zus :\nInvalid argument\n%s",cmd);
				itemp = 0; templ = 1; temp1 = 0;
				if (!gr_numb(&templ,&argc,&argv))		itemp = 1;
				if (!gr_numb(&temp1,&argc,&argv))		itemp = 1;
				if(itemp)
					return error_in_file("-zus :\nInvalid argument\n%s",cmd);
				argc--;				argv++;
				tmpch = strdup(argv[0]);
				type = 0; decade = 0; subtype = 0;
				if (argc >= 3)
				{
					if (!gr_numbi(&type,&argc,&argv))		itemp = 1;
					if (!gr_numbi(&decade,&argc,&argv))	itemp = 1;
					if (!gr_numbi(&subtype,&argc,&argv))	itemp = 2;
					if (itemp == 1)
						return error_in_file("-zus :\nInvalid argument\n%s",cmd);
				}
				else if (unit_to_type(tmpch,&type,&decade))
					return error_in_file("-zus :\nInvalid argument\n%s",cmd);
				un = build_unit_set(type, temp1, templ, (char)decade, 0, tmpch);
				if (tmpch) {free(tmpch);  tmpch = NULL;}
				if (un == NULL)		return error_in_file("-zus :\ncant create\n%s",cmd);
				un->sub_type = subtype;
				add_one_image (oi, IS_Z_UNIT_SET, (void *)un);
			}
			else 		imzlimread(oi,&argc,&argv);
			break;
			case 'h':
			if (strncmp(argv[0],"his",3) == 0)
			{
				if (argc >= 2)
				{
					argc--;
					argv++;
					oi->im.history = Mystrdupre(oi->im.history, argv[0]);
				}
			}
			else gr_numb(&(oi->height),&argc,&argv);
			break;
			case 'w':
			gr_numb(&(oi->width),&argc,&argv);
			break;
			case 'r':
			gr_numb(&(oi->right),&argc,&argv);
			break;
			case 'u':
			gr_numb(&(oi->up),&argc,&argv);
			break;
			default:
			return error_in_file("image Invalid argument\n%s",cmd);
		}
	}
	return 0;
}

int imxlimread(O_i *oi, int *argcp, char ***argvp)
{
  if (oi == NULL || argcp == NULL)      return 1;
	if(!gr_numb(&(oi->x_lo),argcp,argvp))		return 1;
	if(!gr_numb(&(oi->x_hi),argcp,argvp))		return 1;
	oi->iopt2 |= X_LIM;
	return 0;
}
int imylimread(O_i *oi, int *argcp, char ***argvp)
{
  if (oi == NULL || argcp == NULL)      return 1;
	if(!gr_numb(&(oi->y_lo),argcp,argvp))		return 1;
	if(!gr_numb(&(oi->y_hi),argcp,argvp))		return 1;
	oi->iopt2 |= Y_LIM;
	return 0;
}
int imzlimread(O_i *oi, int *argcp, char ***argvp)
{
  if (oi == NULL || argcp == NULL)      return 1;
	if(!gr_numb(&(oi->z_black),argcp,argvp))	return 1;
	if(!gr_numb(&(oi->z_white),argcp,argvp))	return 1;
	return 0;
}
char *read_image_header(O_i* oi, int *size)
{
	FILE *fp = NULL;
	char filename[512] = {0}, *buf = NULL;

	if (oi == NULL || oi->dir == NULL || oi->filename == NULL)	return NULL;
	build_full_file_name(filename, 512, oi->dir, oi->filename);
	fp = fopen (filename, "rb");
	if ( fp == NULL )
	{
		error_in_file("%s\nfile not found!...",filename);
		return NULL;
	}
	if ((*size = find_CTRL_Z_offset(oi, fp)) > 0)
	  {
	    //win_printf("size = %d",*size);
	    fseek(fp, 0, SEEK_SET);
	    buf = (char*)calloc(*size+1,sizeof(char));
	    if (buf == NULL) return NULL;
	    if (fread (buf, sizeof (char), *size, fp) != (size_t) *size)
	      {
	  	win_printf("Va donc chez speedy!");
		if (buf)  {free(buf);  buf = NULL;}
		return NULL;
	      }
	    // win_printf("%s",buf);
	  }
	return buf;
}
int save_image_header(O_i* oi, char *head, int size)
{
	FILE *fp = NULL;
	int nh;
	char filename[512] = {0};
  char disperr[1024] = {0};

	if (oi == NULL || oi->dir == NULL || oi->filename == NULL)	return 1;
	build_full_file_name(filename, 512, oi->dir, oi->filename);
	//win_printf("%s",backslash_to_slash(filename));
	fp = fopen (filename, "r+b");
	if ( fp == NULL )
	{
		error_in_file("%s\nfile not found!...",filename);
		return 1;
	}
	nh = find_CTRL_Z_offset(oi, fp);
//	fclose(fp);
	if (nh < 0 || nh != size)
	  {
	    win_printf("the size of headers disagree!");
	    return 1;
	  }

 	  //win_printf("%d",strlen(head));
	/* 	  fp = fopen (filename, "ab");
	if ( fp == NULL )
	{
		error_in_file("%s\nfile not found!...",filename);
		return 1;
	}
	*/
	fseek(fp, 0, SEEK_SET);
	if (fwrite (head, sizeof (char), size, fp) != (size_t) size)
	  {
	    win_printf("Pb while writing header!");
	    return 1;
	  }
  strncpy(disperr,head,size);
  printf("written header in file %s %s",filename,disperr);
	return 0;
}
int  close_movie_on_disk(O_i *oi, int nf)
{
  char *head = NULL, *found = NULL, tmp[256] = {0};
  int size = 0, nff = -1, cff = -1, i;

  head = read_image_header(oi, &size);
  if (head == NULL)  return 1;
  found = strstr(head,"-nf");
  if (found != NULL)
  {
  	snprintf(tmp,256,"-nf %010d %010d\r\n",nf, oi->im.c_f);
    printf("trying to write nf %d\n",nf);
  	for (i = 0; (i < 256) && (tmp[i] != 0); i++) found[i] = tmp[i];
  }
  else {win_printf("did not found -nf symbol"); return 1;}
  sscanf(found,"-nf %010d %010d",&nff,&cff);
  //win_printf("nff %d cff %d\n%s",nff,cff,found);

  if (oi->im.record_duration != 0)
    {
      found = strstr(head,"-duration");
      if (found != NULL)
      {
      	  snprintf(tmp,256,"-duration %016.6f \r\n",oi->im.record_duration);
      	  for (i = 0; (i < 256) && (tmp[i] != 0); i++) found[i] = tmp[i];
   	  }
      else win_printf("did not found -duration symbol");
    }
  printf("%s",head);
  save_image_header(oi, head,  size);
  if (head) {free(head); head = NULL;}
  return 0;
}

int 	find_CTRL_Z_offset(O_i *oi, FILE *fp)
{
  int n;
  char ch;

  if ( fp == NULL ) return -1;
  if (oi == NULL)      return -1;
  for (n = 0; (fread (&ch, sizeof (char), 1, fp) == 1) && (ch != CRT_Z); n++);
  if ( ch != CRT_Z)
    {
      error_in_file("no CRT_Z in %s\nbefore file EOF!...",oi->filename);
      return -1;
    }
  return n;
}

int	switch_frame_of_disk_movie(O_i *oi, int f)
{
	register int i, j;
	int knx, kny, ret;
	long long filesize, offset, tmpl;
	union pix *p = NULL;

	FILE *fp = NULL;
	char filename[512] = {0};




	if (oi == NULL || oi->im.movie_on_disk == 0
	    || oi->dir == NULL || oi->filename == NULL)	return 1;
	build_full_file_name(filename, 512, oi->dir, oi->filename);
	fp = fopen (filename, "rb");
	if ( fp == NULL )
	{
		error_in_file("%s\nfile not found!...",filename);
		return -1;
	}
	knx = oi->im.nx;
	kny = oi->im.ny;
	p = oi->im.pixel;
	ret = 0;
	if (oi->im.movie_on_disk)
	  oi->im.movie_on_disk = find_CTRL_Z_offset(oi, fp);
	offset = oi->im.movie_on_disk;
#ifdef XV_WIN32	 // NG, 2006-12-03 because _filelengthi64 is Windows specific (visual C++)
	filesize = _filelengthi64(fileno(fp));  // vc
#else
	// Garnier 7/7/2005
	fseek(fp,0, SEEK_END);
	filesize = ftell(fp);
#endif

	tmpl = f;
	if (oi->im.data_type == IS_CHAR_IMAGE)
	{
	  tmpl *= knx*kny;
	  offset += tmpl;
	  if (offset < filesize)
	  {
#if defined XV_MAC || defined XV_UNIX
      fseek(fp, offset, SEEK_SET);
#else
	  	fseeko64(fp, offset, SEEK_SET);
#endif
	  }
	  else ret = -9876; // if offset > filesize
		for (i=0 ; i< kny && ret == 0; i++)
		{
			if ( (j = fread ( p[i].ch , sizeof (unsigned char), knx, fp)) != knx)
			  ret = 1;
		}
    if (ret == 0)
    {
      if (oi->rm_background && oi->oi_bg != NULL)
      {
        for (i = 0 ; i< oi->im.ny; i++)
        {
          for (j = 0; j<oi->im.nx;j++)
          {
            if   (p[i].ch[j] <= oi->oi_bg->im.pixel[i].ch[j]) p[i].ch[j] = 0;
            else p[i].ch[j] -= oi->oi_bg->im.pixel[i].ch[j];
        }

        }
      }
      if (ret == 0 && f>=abs(oi->im.n_f))  oi->im.n_f = -(f + 1);

	}
}
	else if (oi->im.data_type == IS_RGB_PICTURE)
	{
	  tmpl *= knx*kny*3;
	  offset += tmpl;
	  if (offset < filesize)
#if defined XV_MAC || defined XV_UNIX
      fseek(fp, offset, SEEK_SET);
#else
      fseeko64(fp, offset, SEEK_SET);
#endif
	  else ret = -9876;
		for (i=0 ; i< kny && ret == 0; i++)
		{
			if ( (j = fread ( p[i].ch , sizeof (unsigned char), 3*knx, fp)) != 3*knx)
			  ret = 1;
		}
    if (ret == 0 && f>=abs(oi->im.n_f))
    {
      if (oi->rm_background && oi->oi_bg != NULL)
      {
        for (i = 0 ; i< oi->im.ny; i++)
        {
          for (j = 0; j<oi->im.nx;j++)
          {
          if   (p[i].ch[j] < oi->oi_bg->im.pixel[i].ch[j]) p[i].ch[j] = 0;
          else p[i].ch[j] -= oi->oi_bg->im.pixel[i].ch[j];
        }

        }
      }
      oi->im.n_f = -(f + 1);

  }

	}
	else if (oi->im.data_type == IS_RGBA_PICTURE)
	{
	  tmpl *= knx*kny*4;
	  offset += tmpl;
	  if (offset < filesize)
#if defined XV_MAC || defined XV_UNIX
      fseek(fp, offset, SEEK_SET);
#else
      fseeko64(fp, offset, SEEK_SET);
#endif
	  else ret = -9876;
		for (i=0 ; i< kny && ret == 0; i++)
		{
			if ( (j = fread ( p[i].ch , sizeof (unsigned char), 4*knx, fp)) != 3*knx)
			  ret = 1;
		}
    if (ret == 0 && f>=abs(oi->im.n_f))
    {
      if (oi->rm_background && oi->oi_bg != NULL)
      {
        for (i = 0 ; i< oi->im.ny; i++)
        {
          for (j = 0; j<oi->im.nx;j++)
          {
          p[i].ch[j] -= oi->oi_bg->im.pixel[i].ch[j];
        }

        }
      }
      oi->im.n_f = -(f + 1);

  }

	}
	else if (oi->im.data_type == IS_RGB16_PICTURE)
	{
	  tmpl *= knx*kny*6;
	  offset += tmpl;
	  if (offset < filesize)
#if defined XV_MAC || defined XV_UNIX
        fseek(fp, offset, SEEK_SET);
#else
      fseeko64(fp, offset, SEEK_SET);
#endif
	  else ret = -9876;
		for (i=0 ; i< kny && ret == 0; i++)
		{
			if ( (j = fread ( p[i].in , sizeof (short int), 3*knx, fp)) != 3*knx)
			  ret = 1;
		}
    if (ret == 0 && f>=abs(oi->im.n_f))
    {
      if (oi->rm_background && oi->oi_bg != NULL)
      {
        for (i = 0 ; i< oi->im.ny; i++)
        {
          for (j = 0; j<oi->im.nx;j++)
          {
          p[i].in[j] -= oi->oi_bg->im.pixel[i].in[j];
        }

        }
      }
      oi->im.n_f = -(f + 1);

  }

	}
	else if (oi->im.data_type == IS_RGBA16_PICTURE)
	{
	  tmpl *= knx*kny*8;
	  offset += tmpl;
	  if (offset < filesize)
#if defined XV_MAC || defined XV_UNIX
    fseek(fp, offset, SEEK_SET);
#else
      fseeko64(fp, offset, SEEK_SET);
#endif
	  else ret = -9876;
		for (i=0 ; i< kny && ret == 0; i++)
		{
			if ( (j = fread ( p[i].in , sizeof (short int), 4*knx, fp)) != 3*knx)
			  ret = 1;
		}
    if (ret == 0 && f>=abs(oi->im.n_f))
    {
      if (oi->rm_background && oi->oi_bg != NULL)
      {
        for (i = 0 ; i< oi->im.ny; i++)
        {
          for (j = 0; j<oi->im.nx;j++)
          {
          p[i].in[j] -= oi->oi_bg->im.pixel[i].in[j];
        }

        }
      }
      oi->im.n_f = -(f + 1);

  }

	}
	else if (oi->im.data_type == IS_FLOAT_IMAGE)
	{
	  tmpl *= knx*kny*4;
	  offset += tmpl;
	  if (offset < filesize)

#if defined XV_MAC || defined XV_UNIX
     fseek(fp, offset, SEEK_SET);
#else
      fseeko64(fp, offset, SEEK_SET);
#endif
	  else ret = -9876;
		for (i=0 ; i< kny  && ret == 0; i++)
		{
			if ( (j = fread ( p[i].fl , sizeof (float), knx, fp)) != knx)
			  ret = 1;
		}
    if (ret == 0 && f>=abs(oi->im.n_f))
    {
      if (oi->rm_background && oi->oi_bg != NULL)
      {
        for (i = 0 ; i< oi->im.ny; i++)
        {
          for (j = 0; j<oi->im.nx;j++)
          {
          p[i].fl[j] -= oi->oi_bg->im.pixel[i].fl[j];
        }

        }
      }
      oi->im.n_f = -(f + 1);

  }
	}
	else if (oi->im.data_type == IS_DOUBLE_IMAGE)
	{
	  tmpl *= knx*kny*8;
	  offset += tmpl;
	  if (offset < filesize)
#if defined XV_MAC || defined XV_UNIX
     fseek(fp, offset, SEEK_SET);
#else
      fseeko64(fp, offset, SEEK_SET);
#endif
	  else ret = -9876;
		for (i=0 ; i< kny && ret == 0; i++)
		{
			if ( (j = fread ( p[i].db , sizeof (double), knx, fp)) != knx)
			  ret = 1;
		}
    if (ret == 0 && f>=abs(oi->im.n_f))
    {
      if (oi->rm_background && oi->oi_bg != NULL)
      {
        for (i = 0 ; i< oi->im.ny; i++)
        {
          for (j = 0; j<oi->im.nx;j++)
          {
          p[i].db[j] -= oi->oi_bg->im.pixel[i].db[j];
        }

        }
      }
      oi->im.n_f = -(f + 1);

  }

	}
	else if (oi->im.data_type == IS_COMPLEX_IMAGE)
	{
	  tmpl *= knx*kny*8;
	  offset += tmpl;
	  if (offset < filesize)
#if defined XV_MAC || defined XV_UNIX
     fseek(fp, offset, SEEK_SET);
#else
      fseeko64(fp, offset, SEEK_SET);
#endif
	  else ret = -9876;
		for (i=0 ; i< kny && ret == 0; i++)
		{
			if ( (j = fread ( p[i].fl , sizeof (float), 2*knx, fp)) != 2*knx)
			  ret = 1;
		}
    if (ret == 0 && f>=abs(oi->im.n_f))
    {
      if (oi->rm_background && oi->oi_bg != NULL)
      {
        for (i = 0 ; i< oi->im.ny; i++)
        {
          for (j = 0; j<oi->im.nx;j++)
          {
          p[i].fl[j] -= oi->oi_bg->im.pixel[i].fl[j];
        }

        }
      }
      oi->im.n_f = -(f + 1);

  }
	}
	else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
	  tmpl *= knx*kny*16;
	  offset += tmpl;
	  if (offset < filesize)
#if defined XV_MAC || defined XV_UNIX
      fseek(fp, offset, SEEK_SET);
#else
      fseeko64(fp, offset, SEEK_SET);
#endif
	  else ret = -9876;
		for (i=0 ; i< kny && ret == 0; i++)
		{
			if ( (j = fread ( p[i].db , sizeof (double), 2*knx, fp)) != 2*knx)
			  ret = 1;
		}
    if (ret == 0 && f>=abs(oi->im.n_f))
    {
      if (oi->rm_background && oi->oi_bg != NULL)
      {
        for (i = 0 ; i< oi->im.ny; i++)
        {
          for (j = 0; j<oi->im.nx;j++)
          {
          p[i].db[j] -= oi->oi_bg->im.pixel[i].db[j];
        }

        }
      }
      oi->im.n_f = -(f + 1);

  }
	}
	else if (oi->im.data_type == IS_INT_IMAGE)
	{
	  tmpl *= knx*kny*2;
	  offset += tmpl;
	  if (offset < filesize)
#if defined XV_MAC || defined XV_UNIX
      fseek(fp, offset, SEEK_SET);
#else
      fseeko64(fp, offset, SEEK_SET);
#endif
	  else ret = -9876;
		for (i=0 ; i< kny && ret == 0; i++)
		{
			if ( (j = fread ( p[i].in , sizeof (short int), knx, fp)) != knx)
			  ret = 1;
		}
    if (ret == 0 && f>=abs(oi->im.n_f))
    {
      if (oi->rm_background && oi->oi_bg != NULL)
      {
        for (i = 0 ; i< oi->im.ny; i++)
        {
          for (j = 0; j<oi->im.nx;j++)
          {
          p[i].in[j] -= oi->oi_bg->im.pixel[i].in[j];
        }

        }
      }
      oi->im.n_f = -(f + 1);

  }
	}
	else if (oi->im.data_type == IS_UINT_IMAGE)
	{
	  tmpl *= knx*kny*2;
	  offset += tmpl;
	  if (offset < filesize)
#if defined XV_MAC || defined XV_UNIX
      fseek(fp, offset, SEEK_SET);
#else
      fseeko64(fp, offset, SEEK_SET);
#endif
	  else ret = -9876;
		for (i=0 ; i< kny && ret == 0; i++)
		{
			if ( (j = fread ( p[i].ui , sizeof (unsigned short int), knx, fp)) != knx)
			  ret = 1;
		}
    if (ret == 0 && f>=abs(oi->im.n_f))
    {
      if (oi->rm_background && oi->oi_bg != NULL)
      {
        for (i = 0 ; i< oi->im.ny; i++)
        {
          for (j = 0; j<oi->im.nx;j++)
          {
          p[i].ui[j] -= oi->oi_bg->im.pixel[i].ui[j];
        }

        }
      }
      oi->im.n_f = -(f + 1);

  }
	}
	else if (oi->im.data_type == IS_LINT_IMAGE)
	{
	  tmpl *= knx*kny*4;
	  offset += tmpl;
	  if (offset < filesize)
#if defined XV_MAC || defined XV_UNIX
      fseek(fp, offset, SEEK_SET);
#else
      fseeko64(fp, offset, SEEK_SET);
#endif
	  else ret = -9876;
		for (i=0 ; i< kny && ret == 0; i++)
		{
			if ( (j = fread ( p[i].li , sizeof (int), knx, fp)) != knx)
			  ret = 1;
		}
    if (ret == 0 && f>=abs(oi->im.n_f))
    {
      if (oi->rm_background && oi->oi_bg != NULL)
      {
        for (i = 0 ; i< oi->im.ny; i++)
        {
          for (j = 0; j<oi->im.nx;j++)
          {
          p[i].li[j] -= oi->oi_bg->im.pixel[i].li[j];
        }

        }
      }
      oi->im.n_f = -(f + 1);

  }
	}
	else
	{
		ret = -2;
	}
	fclose(fp);
	return ret;
}


/*	int 	push_image(O_i *oi, char *filename, int type, int mode);
 *	DESCRIPTION
 *
 *
 *
 *	RETURNS		0 on success, 1
 *
 */
int 	push_image(O_i *oi, char *filename, int type, int mode)
{
	register int i, j;
	char ch;
	int knx, knxs, knxe, kny, knys, knye, nf = 1;
	float kz_black, kz_white;
	FILE *fp = NULL;
	union pix *p;
	long long filesize;
	unsigned long pos = 0, posz = 0;
	unsigned long t0, dt;

	if (swap_mode == SWAP_XY)
		return push_swap_image(oi,filename,type,mode);
	if (oi == NULL)		return -1;
	if (oi->im.nx <= 0 || oi->im.ny <= 0)	return -1;
	knx = oi->im.nx;
	knxs = oi->im.nxs;
	knxe = oi->im.nxe;
	kny = oi->im.ny;
	knys = oi->im.nys;
	knye = oi->im.nye;
	nf = (oi->im.n_f > 0) ? oi->im.n_f : 1;
	kz_white = oi->z_white;
	kz_black = oi->z_black;
	fp = fopen (filename, "rb");
	if ( fp == NULL )
	{
		error_in_file("%s\nfile not found!...",filename);
		return -1;
	}
/* 	setvbuf(fp, NULL, _IOFBF, MAXBUFSIZE); DJGPP */
/*	set_cursor_glass(); */
	//	if (movie_buffer_on_disk == 1)	oi->im.n_f = 1;
	if (alloc_one_image(oi, oi->im.nx, oi->im.ny, type))
	{
/*		reset_cursor(); */
		return -1;
	}
	if (oi->im.movie_on_disk)
	{
	  //oi->im.n_f = -abs(nf);
		nf = 1;
	}
	if ( mode == CRT_Z)
	{
		while (fread (&ch, sizeof (char), 1, fp) == 1 && ch != CRT_Z);
		if ( ch != CRT_Z)
		{
			error_in_file("no CRT_Z in %s\nbefore file EOF!...",filename);
/*			reset_cursor(); */
			return -1;
		}
		posz=ftell(fp);
	}
	else if ( mode == CMID)
	{
		for (i = 0; i < 4 && (fread (&ch, sizeof (char), 1, fp) == 1); i++);
	}

#ifdef XV_WIN32 // NG, 2006-12-03 because _filelengthi64 is Windows specific (visual C++)
	filesize = _filelengthi64(fileno(fp));
	(void)posz;
#else // NG, 2009/05/05
	fseek(fp,0, SEEK_END);
	filesize = ftell(fp);
	fseek(fp,posz, SEEK_SET);
#endif
	dt = get_my_uclocks_per_sec()/4;


	if (type & IS_CHAR_IMAGE)
	{
		p = oi->im.pixel;
		for (i=0,  t0 = my_uclock() ; i< kny * nf ; i++)
		{
			if ( (j = fread ( p[i].ch , sizeof (unsigned char), knx, fp)) != knx)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,knx);
/*				reset_cursor(); */
				return -1;
			}
			if (my_uclock() - t0 > dt)
			  {
			    t0 = my_uclock();
                pos=ftell(fp);
			    my_set_window_title("Loading %s %6.2f / %6.2f Mb",filename,
						(double)pos/1048576,(double)filesize/1048576);
			  }
		}
	}
	else if (type & IS_RGB_PICTURE)
	{
		p = oi->im.pixel;
		for (i=0,  t0 = my_uclock() ; i< kny * nf ; i++)
		{
			if ( (j = fread ( p[i].ch , sizeof (unsigned char), 3*knx, fp)) != 3*knx)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,knx);
/*				reset_cursor(); */
				return -1;
			}
			if (my_uclock() - t0 > dt)
			  {
			    t0 = my_uclock();
                pos=ftell(fp);
			    my_set_window_title("Loading %s %6.2f / %6.2f Mb",filename,
						(double)pos/1048576,(double)filesize/1048576);
			  }
		}
	}
	else if (type & IS_RGBA_PICTURE)
	{
		p = oi->im.pixel;
		for (i=0 ,  t0 = my_uclock(); i< kny * nf ; i++)
		{
			if ( (j = fread ( p[i].ch , sizeof (unsigned char), 4*knx, fp)) != 4*knx)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,knx);
/*				reset_cursor(); */
				return -1;
			}
			if (my_uclock() - t0 > dt)
			  {
			    t0 = my_uclock();
                pos=ftell(fp);
			    my_set_window_title("Loading %s %6.2f / %6.2f Mb",filename,
						(double)pos/1048576,(double)filesize/1048576);
			  }
		}
	}
	else if (type & IS_RGB16_PICTURE)
	{
		p = oi->im.pixel;
		for (i=0,  t0 = my_uclock() ; i< kny * nf ; i++)
		{
			if ( (j = fread ( p[i].in , sizeof (short int), 3*knx, fp)) != 3*knx)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,knx);
/*				reset_cursor(); */
				return -1;
			}
			if (my_uclock() - t0 > dt)
			  {
			    t0 = my_uclock();
                pos=ftell(fp);
			    my_set_window_title("Loading %s %6.2f / %6.2f Mb",filename,
						(double)pos/1048576,(double)filesize/1048576);
			  }
		}
	}
	else if (type & IS_RGBA16_PICTURE)
	{
		p = oi->im.pixel;
		for (i=0,  t0 = my_uclock() ; i< kny * nf ; i++)
		{
			if ( (j = fread ( p[i].in , sizeof (short int), 4*knx, fp)) != 4*knx)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,knx);
/*				reset_cursor(); */
				return -1;
			}
			if (my_uclock() - t0 > dt)
			  {
			    t0 = my_uclock();
                pos=ftell(fp);
			    my_set_window_title("Loading %s %6.2f / %6.2f Mb",filename,
						(double)pos/1048576,(double)filesize/1048576);
			  }
		}
	}
	else if (type & IS_FLOAT_IMAGE)
	{
		p = oi->im.pixel;
		for (i=0,  t0 = my_uclock() ; i< kny * nf; i++)
		{
			if ( (j = fread ( p[i].fl , sizeof (float), knx, fp)) != knx)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,knx);
/*				reset_cursor(); */
				return -1;
			}
			if (my_uclock() - t0 > dt)
			  {
			    t0 = my_uclock();
                pos=ftell(fp);
			    my_set_window_title("Loading %s %6.2f / %6.2f Mb",filename,
						(double)pos/1048576,(double)filesize/1048576);
			  }
		}
	}
	else if (type & IS_DOUBLE_IMAGE)
	{
		p = oi->im.pixel;
		for (i=0,  t0 = my_uclock() ; i< kny * nf; i++)
		{
			if ( (j = fread ( p[i].db , sizeof (double), knx, fp)) != knx)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,knx);
/*				reset_cursor(); */
				return -1;
			}
			if (my_uclock() - t0 > dt)
			  {
			    t0 = my_uclock();
                pos=ftell(fp);
			    my_set_window_title("Loading %s %6.2f / %6.2f Mb",filename,
						(double)pos/1048576,(double)filesize/1048576);
			  }
		}
	}
	else if (type & IS_COMPLEX_IMAGE)
	{
		p = oi->im.pixel;
		for (i=0,  t0 = my_uclock() ; i< kny * nf ; i++)
		{
			if ( (j = fread ( p[i].fl , sizeof (float), 2*knx, fp)) != 2*knx)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,knx);
/*				reset_cursor(); */
				return -1;
			}
			if (my_uclock() - t0 > dt)
			  {
			    t0 = my_uclock();
                pos=ftell(fp);
			    my_set_window_title("Loading %s %6.2f / %6.2f Mb",filename,
						(double)pos/1048576,(double)filesize/1048576);
			  }
		}
	}
	else if (type & IS_COMPLEX_DOUBLE_IMAGE)
	{
		p = oi->im.pixel;
		for (i=0,  t0 = my_uclock() ; i< kny * nf ; i++)
		{
			if ( (j = fread ( p[i].db , sizeof (double), 2*knx, fp)) != 2*knx)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,knx);
/*				reset_cursor(); */
				return -1;
			}
			if (my_uclock() - t0 > dt)
			  {
			    t0 = my_uclock();
                pos=ftell(fp);
			    my_set_window_title("Loading %s %6.2f / %6.2f Mb",filename,
						(double)pos/1048576,(double)filesize/1048576);
			  }
		}
	}
	else if (type & IS_INT_IMAGE)
	{
		p = oi->im.pixel;
		for (i=0,  t0 = my_uclock() ; i< kny * nf; i++)
		{
			if ( (j = fread ( p[i].in , sizeof (short int), knx, fp)) != knx)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,knx);
/*				reset_cursor(); */
				return -1;
			}
			if (my_uclock() - t0 > dt)
			  {
			    t0 = my_uclock();
                pos=ftell(fp);
			    my_set_window_title("Loading %s %6.2f / %6.2f Mb",filename,
						(double)pos/1048576,(double)filesize/1048576);
			  }
		}
	}
	else if (type & IS_UINT_IMAGE)
	{
		p = oi->im.pixel;
		for (i=0,  t0 = my_uclock() ; i< kny * nf; i++)
		{
			if ( (j = fread ( p[i].ui , sizeof (unsigned short int), knx, fp)) != knx)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,knx);
/*				reset_cursor(); */
				return -1;
			}
			if (my_uclock() - t0 > dt)
			  {
			    t0 = my_uclock();
                pos=ftell(fp);
			    my_set_window_title("Loading %s %6.2f / %6.2f Mb",filename,
						(double)pos/1048576,(double)filesize/1048576);
			  }
		}
	}
	else if (type & IS_LINT_IMAGE)
	{
		p = oi->im.pixel;
		for (i=0,  t0 = my_uclock() ; i< kny * nf; i++)
		{
			if ( (j = fread ( p[i].li , sizeof (int), knx, fp)) != knx)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,knx);
/*				reset_cursor(); */
				return -1;
			}
			if (my_uclock() - t0 > dt)
			  {
			    t0 = my_uclock();
                pos=ftell(fp);
			    my_set_window_title("Loading %s %6.2f / %6.2f Mb",filename,
						(double)pos/1048576,(double)filesize/1048576);
			  }
		}
	}
	else
	{
		error_in_file("Unknown type of image !");
		return -1;
	}
	oi->im.nx = knx;
	oi->im.nxs = (knxs <= 0) ? 0 : knxs;
	oi->im.nxe = (knxe <= 0) ? oi->im.nx : knxe;
	oi->im.ny = kny;
	oi->im.nys = (knys <= 0) ? 0 : knys;
	oi->im.nye = (knye <= 0) ? oi->im.ny : knye;
	if (kz_white != 0)	oi->z_white = kz_white;
	if (kz_black != 0)	oi->z_black = kz_black;
	if (oi->im.n_f > 1)	switch_frame(oi,oi->im.c_f);
	if (kz_white == 0 && kz_black == 0)
	{
		find_zmin_zmax(oi);
		oi->z_black = oi->z_min;
		oi->z_white = oi->z_max;
	}
	if (oi->dx != 1 || oi->dy != 1) oi->iopt2 |= IM_USR_COOR_RT;
	if (oi->ax != 0 || oi->ay != 0) oi->iopt2 |= IM_USR_COOR_RT;
	fclose(fp);
/*	reset_cursor(); */
	return 0;
}
int 	push_swap_image(O_i *oi, char *filename, int type, int mode)
{
	register int i, j;
	char ch;
	unsigned char *imc = NULL;
	int knx, knxs, knxe, kny, knys, knye, nf;
	short int *imi = NULL;
	unsigned short int *imui;
	int *imli = NULL;
	float kz_black, kz_white, *imf;
	double *imdb;
	FILE *fp = NULL;
	union pix *p = NULL;

	if (oi == NULL)		return -1;
	if (oi->im.nx <= 0 || oi->im.ny <= 0)	return -1;
	swap_image(oi);
	knx = oi->im.nx;
	knxs = oi->im.nxs;
	knxe = oi->im.nxe;
	kny = oi->im.ny;
	knys = oi->im.nys;
	knye = oi->im.nye;
	nf = (oi->im.n_f > 0) ? oi->im.n_f : 1;
	kz_white = oi->z_white;
	kz_black = oi->z_black;
	fp = fopen (filename, "rb");
	if ( fp == NULL )
	{
		error_in_file("%s\nfile not found!...",filename);
		return -1;
	}
/* 	setvbuf(fp, NULL, _IOFBF, MAXBUFSIZE); DJGPP */
	if (alloc_one_image(oi, oi->im.nx, oi->im.ny, type))	return -1;
/*	set_cursor_glass(); */
	if ( mode == CRT_Z)
	{
		while (fread (&ch, sizeof (char), 1, fp) == 1 && ch != CRT_Z);
		if ( ch != CRT_Z)
		{
			win_printf("no CRT_Z in %s\nbefore file EOF!...",filename);
/*			reset_cursor();*/
			return -1;
		}
	}
	else if ( mode == CMID)
	{
		for (i = 0; i < 4 && (fread (&ch, sizeof (char), 1, fp) == 1); i++);
	}
	if (type & IS_CHAR_IMAGE)
	{
		p = oi->im.pixel;
		imc = (unsigned char *)calloc(kny,sizeof(unsigned char));
		if (imc == NULL)
		{
/*			reset_cursor(); */
			return 1;
		}
		for (i=0 ; i< knx * nf ; i++)
		{
			if ((j = fread(imc, sizeof(unsigned char), kny, fp)) != kny)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,kny);
/*				reset_cursor();*/
				return -1;
			}
			for (j=0 ; j< kny ; j++)	p[j+(kny * (i/knx))].ch[i%knx] = imc[j];
		}
		if (imc) {free(imc); imc = NULL;}
	}
	if (type & IS_RGB_PICTURE)
	{
		p = oi->im.pixel;
		imc = (unsigned char *)calloc(3*kny,sizeof(unsigned char));
		if (imc == NULL)
		{
/*			reset_cursor(); */
			return 1;
		}
		for (i=0 ; i< knx * nf ; i++)
		{
			if ((j = fread(imc, sizeof(unsigned char), 3*kny, fp)) != 3*kny)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,kny);
/*				reset_cursor();*/
				return -1;
			}
			for (j=0 ; j< kny ; j++)
			  {
			    p[j+(kny * (i/knx))].rgb[i%knx].r = imc[3*j];
			    p[j+(kny * (i/knx))].rgb[i%knx].g = imc[3*j+1];
			    p[j+(kny * (i/knx))].rgb[i%knx].b = imc[3*j+2];
			  }
		}
		if (imc) {free(imc); imc = NULL;}
	}
	if (type & IS_RGBA_PICTURE)
	{
		p = oi->im.pixel;
		imc = (unsigned char *)calloc(4*kny,sizeof(unsigned char));
		if (imc == NULL)
		{
/*			reset_cursor(); */
			return 1;
		}
		for (i=0 ; i< knx * nf ; i++)
		{
			if ((j = fread(imc, sizeof(unsigned char), 4*kny, fp)) != 4*kny)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,kny);
/*				reset_cursor();*/
				return -1;
			}
			for (j=0 ; j< kny ; j++)
			  {
			    p[j+(kny * (i/knx))].rgba[i%knx].a = imc[3*j];
			    p[j+(kny * (i/knx))].rgba[i%knx].b = imc[3*j+1];
			    p[j+(kny * (i/knx))].rgba[i%knx].g = imc[3*j+2];
			    p[j+(kny * (i/knx))].rgba[i%knx].r = imc[3*j+3];
			  }
		}
		if (imc) {free(imc); imc = NULL;}
	}
	if (type & IS_RGB16_PICTURE)
	{
		p = oi->im.pixel;
		imi = (short int *)calloc(3*kny,sizeof(short int));
		if (imi == NULL)
		{
/*			reset_cursor(); */
			return 1;
		}
		for (i=0 ; i< knx * nf ; i++)
		{
			if ((j = fread(imi, sizeof(short int), 3*kny, fp)) != 3*kny)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,kny);
/*				reset_cursor();*/
				return -1;
			}
			for (j=0 ; j< kny ; j++)
			  {
			    p[j+(kny * (i/knx))].rgb16[i%knx].r = imi[3*j];
			    p[j+(kny * (i/knx))].rgb16[i%knx].g = imi[3*j+1];
			    p[j+(kny * (i/knx))].rgb16[i%knx].b = imi[3*j+2];
			  }
		}
		if (imi) {free(imi); imi = NULL;}
	}
	if (type & IS_RGBA16_PICTURE)
	{
		p = oi->im.pixel;
		imi = (short int *)calloc(4*kny,sizeof(short int));
		if (imi == NULL)
		{
/*			reset_cursor(); */
			return 1;
		}
		for (i=0 ; i< knx * nf ; i++)
		{
			if ((j = fread(imi, sizeof(short int), 4*kny, fp)) != 4*kny)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,kny);
/*				reset_cursor();*/
				return -1;
			}
			for (j=0 ; j< kny ; j++)
			  {
			    p[j+(kny * (i/knx))].rgba16[i%knx].a = imi[3*j];
			    p[j+(kny * (i/knx))].rgba16[i%knx].b = imi[3*j+1];
			    p[j+(kny * (i/knx))].rgba16[i%knx].g = imi[3*j+2];
			    p[j+(kny * (i/knx))].rgba16[i%knx].r = imi[3*j+3];
			  }
		}
		if (imi) {free(imi); imi = NULL;}
	}
	else if (type & IS_FLOAT_IMAGE)
	{
		p = oi->im.pixel;
		imf = (float*)calloc(kny,sizeof(float));
		if (imf == NULL)		return 1;
		for (i=0 ; i< knx * nf ; i++)
		{
			if ((j = fread(imf, sizeof (float), kny, fp)) != kny)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,kny);
/*				reset_cursor();*/
				return -1;
			}
			for (j=0 ; j< kny ; j++)	p[j+(kny * (i/knx))].fl[i%knx] = imf[j];
		}
		if (imf) {free(imf); imf = NULL;}
	}
	else if (type & IS_DOUBLE_IMAGE)
	{
		p = oi->im.pixel;
		imdb = (double*)calloc(kny,sizeof(double));
		if (imdb == NULL)		return 1;
		for (i=0 ; i< knx * nf ; i++)
		{
			if ((j = fread(imdb, sizeof (double), kny, fp)) != kny)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,kny);
/*				reset_cursor();*/
				return -1;
			}
			for (j=0 ; j< kny ; j++)	p[j+(kny * (i/knx))].db[i%knx] = imdb[j];

		}
		if (imdb) {free(imdb); imdb = NULL;}
	}
	else if (type & IS_COMPLEX_IMAGE)
	{
		p = oi->im.pixel;
		imf = (float*)calloc(2*kny,sizeof(float));
		if (imf == NULL)		return 1;
		for (i=0 ; i< knx * nf ; i++)
		{
			if ((j = fread(imf, sizeof (float), 2*kny, fp)) != 2*kny)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,kny);
/*				reset_cursor();*/
				return -1;
			}
			for (j=0 ; j< kny ; j++) p[j+(kny * (i/knx))].fl[2*(i%knx)] = imf[2*j];
			for (j=0 ; j< kny ; j++) p[j+(kny * (i/knx))].fl[2*(i%knx)+1] = imf[2*j+1];
		}
		if (imf) {free(imf); imf = NULL;}
	}
	else if (type & IS_COMPLEX_DOUBLE_IMAGE)
	{
		p = oi->im.pixel;
		imdb = (double*)calloc(2*kny,sizeof(double));
		if (imdb == NULL)		return 1;
		for (i=0 ; i< knx * nf ; i++)
		{
			if ((j = fread(imdb, sizeof (double), 2*kny, fp)) != 2*kny)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,kny);
/*				reset_cursor();*/
				return -1;
			}
			for (j=0 ; j< kny ; j++) p[j+(kny * (i/knx))].db[2*(i%knx)] = imdb[2*j];
			for (j=0 ; j< kny ; j++) p[j+(kny * (i/knx))].db[2*(i%knx)+1] = imdb[2*j+1];
		}
		if (imdb) {free(imdb); imdb = NULL;}
	}
	else if (type & IS_INT_IMAGE)
	{
		p = oi->im.pixel;
		imi = (short int*)calloc(kny,sizeof(short int));
		if (imi == NULL)		return 1;
		for (i=0 ; i< knx * nf ; i++)
		{
			if ((j = fread(imi,sizeof (short int), kny, fp)) != kny)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,kny);
/*				reset_cursor();*/
				return -1;
			}
			for (j=0 ; j< kny ; j++)	p[j+(kny * (i/knx))].in[i%knx] = imi[j];
		}
		if (imi) {free(imi); imi = NULL;}
	}
	else if (type & IS_LINT_IMAGE)
	{
		p = oi->im.pixel;
		imli = (int*)calloc(kny,sizeof(int));
		if (imli == NULL)		return 1;
		for (i=0 ; i< knx * nf ; i++)
		{
			if ((j = fread(imli,sizeof (int), kny, fp)) != kny)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,kny);
/*				reset_cursor();*/
				return -1;
			}
			for (j=0 ; j< kny ; j++)	p[j+(kny * (i/knx))].li[i%knx] = imli[j];
		}
		if (imli) {free(imli); imli = NULL;}
	}
	else if (type & IS_UINT_IMAGE)
	{
		p = oi->im.pixel;
		imui = (unsigned short int*)calloc(kny,sizeof(unsigned short int));
		if (imui == NULL)		return 1;
		for (i=0 ; i< knx * nf ; i++)
		{
			if ((j = fread(imui,sizeof (unsigned short int), kny, fp)) != kny)
			{
				win_ti_printf("load error","line %d\nbinary lecture pb !\n"
					      "%d item read instead of %d",i,j,kny);
/*				reset_cursor();*/
				return -1;
			}
			for (j=0 ; j< kny ; j++)	p[j+(kny * (i/knx))].ui[i%knx] = imui[j];
		}
		if (imui) {free(imui); imui = NULL;}
	}
	else
	{
		win_printf("Unknown type of image !");
/*		reset_cursor();*/
		return -1;
	}
	oi->im.nx = knx;
	oi->im.nxs = (knxs <= 0) ? 0 : knxs;
	oi->im.nxe = (knxe <= 0) ? oi->im.nx : knxe;
	oi->im.ny = kny;
	oi->im.nys = (knys <= 0) ? 0 : knys;
	oi->im.nye = (knye <= 0) ? oi->im.ny : knye;
	if (kz_white != 0)	oi->z_white = kz_white;
	if (kz_black != 0)	oi->z_black = kz_black;
	if (oi->im.n_f > 1)	switch_frame(oi,oi->im.c_f);
	if (kz_white == 0 && kz_black == 0)
	{
		find_zmin_zmax(oi);
		oi->z_black = oi->z_min;
		oi->z_white = oi->z_max;
	}
	if (oi->dx != 1 || oi->dy != 1) oi->iopt2 |= IM_USR_COOR_RT;
	if (oi->ax != 0 || oi->ay != 0) oi->iopt2 |= IM_USR_COOR_RT;
	fclose(fp);
/*	reset_cursor();*/
	return 0;
}

# endif
