#ifndef _IM_REG_C_
#define _IM_REG_C_

# include <stdlib.h>
# include <string.h>

# include "im_reg.h"
# include "xvin.h"

imreg 	*last_selected_imr = NULL;

/*	build_image_region ( int x0, int y0, int width, int height, int type)
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
imreg *build_image_region ( int x0, int y0, int width, int height, int type)
{
	imreg *imr = NULL;

	imr = (imreg *)calloc(1,sizeof(imreg));
	if ( imr == NULL )			return (imreg *) xvin_ptr_error(Out_Of_Memory);
	imr->o_i = (O_i **)calloc(MAX_ONE_PLOT,sizeof(O_i*));
	imr->one_i = (O_i *)calloc(1,sizeof(O_i));
	imr->l_db = (dybox *)calloc(MAX_ACTIVE_DB,sizeof(dybox));
	if (imr == NULL || imr->o_i == NULL || imr->one_i == NULL || imr->l_db == NULL)
		return (imreg *) xvin_ptr_error(Out_Of_Memory);
	imr->cur_oi = imr->n_db = 0;
	imr->m_db = MAX_ACTIVE_DB;
	imr->n_oi = 1;
	imr->m_oi = MAX_ONE_PLOT;
	imr->o_i[0] = imr->one_i;
	init_box(&(imr->stack));
	imr->stack2.pt = imr->stack.pt = 10;
	imr->stack2.color = imr->stack.color = Lightgreen;
	init_box(&(imr->stack2));
	setsize_rec(&imr->def, x0, y0, width, height);
/*	setaction_dybox(&imr->db, IS_IMAGE_REGION, run_imr_db, (void*)imr); */
	imr->x0 = imr->y0 = 0;
	imr->screen_scale = 28;
	imr->min_scale = 7;
	imr->max_scale = 160;
	imr->mag_step = 2;
	init_one_image(imr->one_i, type);
	imr->mark_v = imr->mark_h = 20;
	imr->use.to = 0;
	imr->use.stuff = NULL;
	imr->image_got_mouse = NULL;
	imr->image_lost_mouse = NULL;
	imr->image_end_action = NULL;
	imr->image_mouse_action = NULL;
	return imr;
}

imreg *build_an_empty_image_region ( int x0, int y0, int width, int height)
{
	imreg *imr = NULL;

	imr = (imreg *)calloc(1,sizeof(imreg));
	if (imr == NULL)   return (imreg *) xvin_ptr_error(Out_Of_Memory);
	imr->o_i = (O_i **)calloc(MAX_ONE_PLOT,sizeof(O_i*));
	imr->one_i = NULL;
	imr->l_db = (dybox *)calloc(MAX_ACTIVE_DB,sizeof(dybox));
	if (imr == NULL || imr->o_i == NULL || imr->l_db == NULL)
		return (imreg *) xvin_ptr_error(Out_Of_Memory);
	imr->cur_oi = imr->n_db = 0;
	imr->m_db = MAX_ACTIVE_DB;
	imr->n_oi = 0;
	imr->m_oi = MAX_ONE_PLOT;
	init_box(&(imr->stack));
	imr->stack2.pt = imr->stack.pt = 10;
	imr->stack2.color = imr->stack.color = Lightgreen;
	init_box(&(imr->stack2));
	setsize_rec(&imr->def, x0, y0, width, height);
/*	setaction_dybox(&imr->db, IS_IMAGE_REGION, run_imr_db, (void*)imr); */
	imr->x0 = imr->y0 = 0;
	imr->screen_scale = 28;
	imr->min_scale = 7;
	imr->max_scale = 80;
	imr->mag_step = 2;
	imr->mark_v = imr->mark_h = 20;
	imr->use.to = 0;
	imr->use.stuff = NULL;	
	imr->image_got_mouse = NULL;
	imr->image_lost_mouse = NULL;
	imr->image_end_action = NULL;
	imr->image_mouse_action = NULL;	
	return imr;
}

/*	int free_image_region ( imreg* imr);
 *	DESCRIPTION
 *
 *	RETURNS		0 on success, 1
 */
int free_image_region ( imreg* imr)
{
	int i;

	if (imr == NULL) return 1;
	free_box(&(imr->stack));
	free_box(&(imr->stack2));
	for (i=0 ; i< imr->n_oi ; i++)	free_one_image(imr->o_i[i]);
	free(imr->o_i);
	free(imr->l_db);
	free(imr);
	return 0;
}

O_i  *create_and_attach_oi_to_imr(imreg *imr, int nx, int ny, int type)
{
	O_i *oi = NULL;

	if (imr == NULL)	return NULL;
	oi = create_one_image(nx, ny, type);
	if (oi == NULL) return (O_i *) xvin_ptr_error(Out_Of_Memory);
	add_image(imr, type, (void*)oi);
	return oi;
}

O_i  *create_and_attach_movie_to_imr(imreg *imr, int nx, int ny, int type, int nf)
{
	O_i *oi = NULL;

	if (imr == NULL)	return NULL;
	oi = create_one_movie(nx, ny, type, nf);
	if (oi == NULL) return (O_i *) xvin_ptr_error(Out_Of_Memory);
	add_image(imr, type, (void*)oi);
	return oi;
}


O_i  *create_and_attach_continuous_movie_to_imr(imreg *imr, int nx, int ny, int type, int nf)
{
	O_i *oi = NULL;

	if (imr == NULL)	return NULL;
	oi = create_one_continuous_movie(nx, ny, type, nf);
	if (oi == NULL) return (O_i *) xvin_ptr_error(Out_Of_Memory);
	add_image(imr, type, (void*)oi);
	return oi;
}


int alloc_image (imreg *imr, int nx, int ny, int type)
{
	return alloc_one_image (imr->one_i, nx, ny, type);
}

/*	int add_image (imreg *imr, int type, void *stuff);
 *	int remove_image (imreg *imr, int type, void *stuff);
 *	int add_one_image (O_i *oi, int type, void *stuff);
 *	int remove_one_image (O_i *oi, int type, void *stuff);
 *	DESCRIPTION
 *
 *
 *	RETURNS		0 on success, 1
 *
 */
int add_image (imreg *imr, int type, void *stuff)
{
	return add_to_image (imr, type, stuff);
}
int add_to_image (imreg *imr, int type, void *stuff)
{
	if ( imr == NULL ) return xvin_error(Wrong_Argument);
	if ((type == IS_CHAR_IMAGE) || (type == IS_INT_IMAGE)
	|| (type == IS_UINT_IMAGE) || (type == IS_LINT_IMAGE)
	|| (type == IS_FLOAT_IMAGE) || (type == IS_COMPLEX_IMAGE)
	|| (type == IS_DOUBLE_IMAGE) || (type == IS_COMPLEX_DOUBLE_IMAGE)
	|| (type == IS_RGB_PICTURE) || (type == IS_ONE_IMAGE)
	|| (type == IS_RGBA_PICTURE) || (type == IS_RGB16_PICTURE) || (type == IS_RGBA16_PICTURE))
	{
		if (imr->n_oi >=  imr->m_oi)
		{
			imr->m_oi += MAX_DATA;
			imr->o_i = (O_i**)realloc(imr->o_i,imr->m_oi*sizeof(O_i*));
			if (imr->o_i == NULL)		return xvin_error(Out_Of_Memory);
		}
		imr->o_i[imr->n_oi] = (O_i*)stuff;
		imr->n_oi++;
	}
	else
		return (add_one_image(imr->one_i, type, stuff));
	return 0;
}

int remove_image (imreg *imr, int type, void *stuff)
{
	return remove_from_image (imr, type, stuff);
}
int remove_from_image (imreg *imr, int type, void *stuff)
{
	register int i, j = -1;
	if ( imr == NULL )	return xvin_error(Wrong_Argument);
	if ((type == 0) || (type == IS_CHAR_IMAGE) || (type == IS_INT_IMAGE)
	|| (type == IS_UINT_IMAGE) || (type == IS_LINT_IMAGE)
	|| (type == IS_FLOAT_IMAGE) || (type == IS_COMPLEX_IMAGE)
	|| (type == IS_DOUBLE_IMAGE) || (type == IS_COMPLEX_DOUBLE_IMAGE)
	|| (type == IS_RGB_PICTURE) || (type == IS_ONE_IMAGE)
	|| (type == IS_RGBA_PICTURE) || (type == IS_RGB16_PICTURE) || (type == IS_RGBA16_PICTURE))
	{
		for (i=0, j = -1 ; (i< imr->n_oi) && (j < 0) ; i++)
		{
			if ( (void*)(imr->o_i[i]) == stuff)	j = i;
		}
		if ( j >= 0 && imr->n_oi > 1)
		{
			free_one_image(imr->o_i[j]);
			imr->n_oi--;
			for (i=j ; i< imr->n_oi ; i++)
				imr->o_i[i] = imr->o_i[i+1];
			if (imr->cur_oi >= j && imr->cur_oi > 0)imr->cur_oi--;
			imr->one_i = imr->o_i[imr->cur_oi];
			imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
			return 0;
		}
		else return 1;
	}
	else	return (remove_one_image(imr->one_i, type, stuff));
	return 1;
}


O_p	*find_im_cur_op(imreg *imr)
{
	if (imr == NULL || imr->one_i == NULL || imr->one_i->cur_op < 0
	|| imr->one_i->cur_op >= imr->one_i->n_op) 		return NULL;
	else	return (imr->one_i->o_p[imr->one_i->cur_op]);
}


int	x_imdata_2_imr(imreg *imr, int x)
{
	int pw;
	if (imr == NULL || imr->one_i == NULL)	return -xvin_error(Wrong_Argument);
	pw = imr->one_i->im.nxe - imr->one_i->im.nxs;
	if (imr->s_nx == 0 || pw == 0) return 0;
	if (x > imr->one_i->im.nxe) return (imr->x_off + imr->s_nx);
	if (x < imr->one_i->im.nxs) return imr->x_off;
	return (imr->x_off + ((x - imr->one_i->im.nxs) * imr->s_nx)/pw);
}
int	y_imdata_2_imr(imreg *imr, int y)
{
	int ph;
	if (imr == NULL || imr->one_i == NULL)	return -xvin_error(Wrong_Argument);
	ph = imr->one_i->im.nye - imr->one_i->im.nys;
	if (imr->s_ny == 0 || ph == 0) return 0;
	if (y > imr->one_i->im.nye) return (imr->y_off - imr->s_ny);
	if (y < imr->one_i->im.nys) return imr->y_off;
	return (imr->y_off - ((y - imr->one_i->im.nys) * imr->s_ny)/ph);
}
float	x_imr_2_imdata(imreg *imr, int x)
{
	int pw;
	DIALOG *d = NULL;

	if (imr == NULL || imr->one_i == NULL)	  return -xvin_error(Wrong_Argument);
	d = find_dialog_associated_to_imr(imr, NULL);
	if (d == NULL) return -xvin_error(Wrong_Argument);
	pw  = imr->one_i->im.nxe - imr->one_i->im.nxs;
	if (imr->s_nx == 0 || pw == 0) return 0;
	return((float)imr->one_i->im.nxs + ((float)((x - d->x - imr->x_off)*pw)/imr->s_nx));

}
float	y_imr_2_imdata(imreg *imr, int y)
{
	int ph;
	DIALOG *d = NULL;
	float yd;

	if (imr == NULL || imr->one_i == NULL)	  return -xvin_error(Wrong_Argument);
	d = find_dialog_associated_to_imr(imr, NULL);
	if (d == NULL) return -xvin_error(Wrong_Argument);
	ph = imr->one_i->im.nye - imr->one_i->im.nys;
	if (imr->s_ny == 0 || ph == 0) return 0;
	yd  = ((float)imr->one_i->im.nys + ((float)((imr->y_off - y+d->y)*ph)/imr->s_ny));
	//draw_bubble(screen, B_LEFT, 100, 100, "yoff %d s_ny %d ph %d yvp %d d->y %d s %d y-> %g"
	//  ,imr->y_off,imr->s_ny,ph,y,d->y,(imr->y_off - y+d->y),yd);

	return yd;
}
float	x_imr_2_im_user_data(imreg *imr, int x)
{
	float  xf;
	int pw;
	DIALOG *d = NULL;

	if (imr == NULL || imr->one_i == NULL)	  return -xvin_error(Wrong_Argument);
	d = find_dialog_associated_to_imr(imr, NULL);
	if (d == NULL) return -xvin_error(Wrong_Argument);
	pw  = imr->one_i->im.nxe - imr->one_i->im.nxs;
	if (imr->s_nx == 0 || pw == 0) return 0;
	xf = (float)imr->one_i->im.nxs + ((float)((x - imr->x_off)*pw)/imr->s_nx);
	return (imr->one_i->ax + imr->one_i->dx * xf);
}
float	y_imr_2_im_user_data(imreg *imr, int y)
{
	float yf;
	int ph;
	DIALOG *d = NULL;

	if (imr == NULL || imr->one_i == NULL)	  return -xvin_error(Wrong_Argument);
	d = find_dialog_associated_to_imr(imr, NULL);
	if (d == NULL) return -xvin_error(Wrong_Argument);
	ph = imr->one_i->im.nye - imr->one_i->im.nys;
	if (imr->s_ny == 0 || ph == 0) return 0;
	yf = (float)imr->one_i->im.nys + ((float)((imr->y_off - y)*ph)/imr->s_ny);
	return (imr->one_i->ay + imr->one_i->dy * yf);
}
int	x_pltdata_2_imr(imreg *imr, float x)
{
	float pw;
	O_p *op = NULL;

	if (imr == NULL || imr->one_i == NULL) return -xvin_error(Wrong_Argument);
	op = find_im_cur_op(imr);
	if (op == NULL)	return (x_imdata_2_imr(imr, (int)x));
	pw = op->x_hi - op->x_lo;
	if (imr->s_nx == 0 || pw == 0) return 0;
	x = imr->s_nx * (x - op->x_lo) / pw;
	return (imr->x_off + (int)x);
}
int	y_pltdata_2_imr(imreg *imr, float y)
{
	float ph;
	O_p *op = NULL;

	if (imr == NULL || imr->one_i == NULL) return -xvin_error(Wrong_Argument);
	op = find_im_cur_op(imr);
	if (op == NULL)	return (y_imdata_2_imr(imr, (int)y));
	ph = op->y_hi - op->y_lo;
	if (imr->s_ny == 0 || ph == 0) return 0;
	y = imr->s_ny * (y - op->y_lo) / ph;
	return (imr->y_off - (int)y);
}
float	x_imr_2_pltdata(imreg *imr, int yvp)
{
	O_p *op= find_im_cur_op(imr);

	if (op == NULL)		return -xvin_error(Wrong_Argument);
	if (!(op->type & IM_SAME_Y_AXIS))		return -2;
	if (yvp < 0 || yvp > op->dat[op->cur_dat]->nx)	return -3;
	return 	(op->ax + op->dx * op->dat[op->cur_dat]->xd[yvp]);
}
float	y_imr_2_pltdata(imreg *imr, int xvp)
{
	O_p *op= find_im_cur_op(imr);

	if (op == NULL)		return -xvin_error(Wrong_Argument);
	if (!(op->type & IM_SAME_X_AXIS))		return -2;
	if (xvp < 0 || xvp > op->dat[op->cur_dat]->ny)	return -3;
	return 	(op->ay + op->dy * op->dat[op->cur_dat]->yd[xvp]);
}


int 	set_im_x_unit_set(imreg *imr, int n)
{
  //register int i;

        if (imr == NULL)      return 1;
	//i = n & NOT_TO_DISPLAY;
	n &= 0xff;
	set_oi_x_unit_set(imr->one_i, n);
/*	if (i == 0)	refresh_im_plot(imr, UNCHANGED);
	update_menu(cur_menu);	*/
	return (0);
}

int 	set_im_y_unit_set(imreg *imr, int n)
{
  //register int i;

        if (imr == NULL)      return 1;
	//i = n & NOT_TO_DISPLAY;
	n &= 0xff;
	set_oi_y_unit_set(imr->one_i, n);
/*	if (i == 0)	refresh_im_plot(imr, UNCHANGED);
	update_menu(cur_menu); */
	return (0);
}

int 	set_im_z_unit_set(imreg *imr, int n)
{
        if (imr == NULL)      return 1;
	set_oi_z_unit_set(imr->one_i, n);
/*	if (imr->one_i->cur_op >= 0)	refresh_im_plot(imr, UNCHANGED);
	update_menu(cur_menu);	
	return (change_contrast(0));	*/
	return 0;
}


O_i *find_source_specific_oi_in_imr(imreg *imr, char *source, int *n_oi)
{
  int i, icp, l;
  char *c = NULL;

  if (imr == NULL || source == NULL)	      return NULL;
  l = strlen(source);
  if (l < 1) return NULL;
  for (i = 0, icp = -1; i < imr->n_oi && icp == -1; i++)
    {
      if (imr->o_i[i] != NULL)
	{
	  c = imr->o_i[i]->im.source;
	  icp = ((c != NULL) && strncmp(c,source,l) == 0) ? i : -1;
	}
    }
  if ((icp >= 0) && (n_oi != NULL)) *n_oi = icp;
  return (icp == -1) ? NULL : imr->o_i[icp];
}


/*	do_one_image (O_i *oi, struct box *q_b);
 *	DESCRIPTION	(O_i *oi, struct box *qbo, struct box **plt)
 *
 *	RETURNS		0 on success, 1
 */
int do_one_image(imreg *imr)
{
	register int i, j;
	int iopt, pxs, pxe, pys, pye, min = 0;
	O_i *oi = NULL;
	O_p *op = NULL;
	d_s *ds = NULL;
	struct plot_label *p_p_l;
	char s[64] = {0};
	float xd[2] = {0}, yd[2] = {0}, xe[2] = {0}, ye[2] = {0}, tmp = 0;
	int x2, x3, y2, y3, fgd = fine_grid;

	if (imr == NULL)		return xvin_error(Wrong_Argument);
	oi = imr->one_i;
	if (oi == NULL)		return xvin_error(Wrong_Argument);

	if (imr->one_i->data_changing || imr->one_i->transfering_data_to_box)   return D_O_K;
	if (imr->stack_in_use || imr->blitting_stack)   return D_O_K;

	imr->one_i->transfering_data_to_box = 1;
	imr->stack_in_use = 1;



	if (oi->cur_op >= 0 && oi->cur_op < oi->n_op)	op = oi->o_p[oi->cur_op];
	fine_grid = 0;
	qterm(&imr->stack);
	free_qkd_box();
	init_box(&imr->stack);
	imr->stack.color = Lightgreen;
	qterm(&imr->stack);

	if (oi->width != 1 || oi->height != 1)
		qdset_abs(oi->width,0,oi->height,0);
	qdset_ticklen(oi->tick_len);
	qkylabl_set((oi->iopt2 & Y_NUM));
	qkxlabl_set((oi->iopt2 & X_NUM));
	if (oi->iopt2 & IM_USR_COOR_LB)
	{
		qkyunit(oi->y_prefix,oi->y_unit);
		qkxunit(oi->x_prefix,oi->x_unit);
	}
	iopt = (oi->iopt & NOAXES) ? NOAXES+DOT : TRIM+DOT;
	pxs = xd[0] = oi->im.nxs - ((oi->im.nxe - oi->im.nxs)*3)/(oi->width*512); // was *3)/(oi->
	pxe = xd[1] = oi->im.nxe + ((oi->im.nxe - oi->im.nxs)*6)/(oi->width*512);
	pys = yd[0] = oi->im.nys - ((oi->im.nye - oi->im.nys)*3)/(oi->height*512);
	pye = yd[1] = oi->im.nye + ((oi->im.nye - oi->im.nys)*6)/(oi->height*512);
	if (oi->iopt2 & IM_USR_COOR_LB)
	{
		xd[0] = oi->dx * pxs + oi->ax;
		xd[1] = oi->dx * pxe + oi->ax;
		yd[0] = oi->dy * pys + oi->ay;
		yd[1] = oi->dy * pye + oi->ay;
	}
	qkdraw(2,xd,yd,iopt,xd,xd+1,yd,yd+1);	/* NOAXES+DOT */
	imr->plt = last_q_b();
	if (oi->x_title != NULL) 	qxlabl(oi->x_title);
	if (oi->y_title != NULL)	qylabl(oi->y_title);
/* test
	xd[0] = pxs;
	xd[1] = pxe;
	yd[0] = pys;
	yd[1] = pye;
	qkdraw(2,xd,yd,iopt,xd,xd+1,yd,yd+1);
	for (i=0 ; i< oi->n_lab ; i++)
	{
		qkd_lab_color(Lightred);
		p_p_l = oi->lab[i];
		if (p_p_l->type == USR_COORD)
			xlabel(p_p_l->text,p_p_l->xla,p_p_l->yla);
		else
			xlabel(p_p_l->text, oi->x_lo + (oi->x_hi - oi->x_lo)
		* p_p_l->xla, oi->y_lo + (oi->y_hi - oi->y_lo) * p_p_l->yla);
	}
 end test */
	qdset_ticklen(oi->tick_len);
	if (op == NULL || oi->cur_op < 0)    /* right and top related to image */
	{
		xd[0] = pxs;	xd[1] = pxe;
		yd[0] = pys;	yd[1] = pye;
		if (oi->iopt2 & IM_USR_COOR_RT)
		{
			xd[0] = oi->dx * pxs + oi->ax;
			xd[1] = oi->dx * pxe + oi->ax;
			yd[0] = oi->dy * pys + oi->ay;
			yd[1] = oi->dy * pye + oi->ay;
		}
		qkylabl_set(Y_NUM);
		qkxlabl_set(X_NUM);
		qkyunit(oi->y_prefix, oi->y_unit);
		qkxunit(oi->x_prefix, oi->x_unit);
		iopt = (oi->iopt & NOAXES) ? NOAXES+DOT : AXES_PRIME+DOT;
		qkd_color(White,Yellow);
		qkdraw(2,xd,yd,iopt,xd,xd+1,yd,yd+1);
		if (oi->y_prime_title != NULL)	qylabl(oi->y_prime_title);
		if (oi->x_prime_title != NULL)	qxlabl(oi->x_prime_title);
		if (oi->title != NULL)		qdtitl(oi->title);
		qterm(&imr->stack2);
		free_qkd_box();
		init_box(&imr->stack2);
		imr->stack.color = Lightgreen;
		qterm(&imr->stack2);
			if (oi->width != 1 || oi->height != 1)
				qdset_abs(oi->width,0,oi->height,0);
		iopt |= NOAXES;
		qkdraw(2,xd,yd,iopt,xd,xd+1,yd,yd+1);
	}
	else	/* right and top related to one_plot */
	{
		xe[0] = xd[0] = pxs;	xe[1] = xd[1] = pxe;
		ye[0] = yd[0] = pys;	ye[1] = yd[1] = pye;
		if (!(op->iopt2 & X_LIM))
		{
			op->x_lo = xd[0];	op->x_hi = xd[1];
		}
		if (!(op->iopt2 & Y_LIM))
		{
			op->y_lo = yd[0];	op->y_hi = yd[1];
		}
		if (oi->iopt2 & IM_USR_COOR_RT)	/* RT instead of LB */
		{
			xd[0] = oi->dx * pxs + oi->ax;
			xd[1] = oi->dx * pxe + oi->ax;
			yd[0] = oi->dy * pys + oi->ay;
			yd[1] = oi->dy * pye + oi->ay;
		}
		if (op->type & IM_SAME_X_AXIS)
		{
			op->x_lo = xe[0] = xd[0] = oi->dx * pxs + oi->ax; /* pxs;*/
			op->x_hi = xe[1] = xd[1] = oi->dx * pxe + oi->ax; /* pxe;*/
			op->x_lo = xd[0] = pxs;
			op->x_hi = xd[1] = pxe;
			op->iopt2 |= X_LIM;
		}
		else
		{
			if (!(op->iopt2 & X_LIM))	find_x_limits(op);
			xd[0] = xe[0] = op->dx * op->x_lo + op->ax;/* oi->ax => op->ax */
			xd[1] = xe[1] = op->dx * op->x_hi + op->ax;
		}
		if (op->type & IM_SAME_Y_AXIS)
		{
			op->y_lo = ye[0] = yd[0] = oi->dy * pys + oi->ay; /* pys;*/
			op->y_hi = ye[1] = yd[1] = oi->dy * pye + oi->ay; /* pye;*/
			op->y_lo = yd[0] = pys;
			op->y_hi = yd[1] = pye;
			op->iopt2 |= Y_LIM;
		}
		else
		{
			if (!(op->iopt2 & Y_LIM))	find_y_limits(op);
			yd[0] = ye[0] = op->dy * op->y_lo + op->ay;
			yd[1] = ye[1] = op->dy * op->y_hi + op->ay;
		}
		qkylabl_set((op->iopt2 & Y_NUM));
		qkxlabl_set((op->iopt2 & X_NUM));
		if (op->type & IM_SAME_Y_AXIS)		qkyunit(oi->y_prefix, oi->y_unit);
		else 			qkyunit(op->y_prefix, op->y_unit);
		if (op->type & IM_SAME_X_AXIS)		qkxunit(oi->x_prefix, oi->x_unit);
		else			qkxunit(op->x_prefix, op->x_unit);
		qkd_color(Yellow,Yellow);
		iopt = (op->iopt & NOAXES) ? NOAXES | DOT : AXES_PRIME | DOT;
		qkdraw(2,xe,ye,iopt,xe,xe+1,ye,ye+1);
		if (op->y_title != NULL)	qylabl(op->y_title);
		if (op->x_title != NULL)	qxlabl(op->x_title);
		if (op->title != NULL)		qdtitl(op->title);
/*
		if (op->type & IM_SAME_Y_AXIS)
		{
			xe[0] = pxs;	xe[1] = pxe;
			ye[0] = pys;	ye[1] = pye;
		}
*/
		qterm(&imr->stack2);
		free_qkd_box();
		init_box(&imr->stack2);
		imr->stack.color = Lightgreen;
		qterm(&imr->stack2);
			if (oi->width != 1 || oi->height != 1)
				qdset_abs(oi->width,0,oi->height,0);
		iopt |= NOAXES;
		iopt &= ~DOT;
		for (i=0 ; i< op->n_dat ; i++)
		{
		  if (op->display_single_ds && i != op->cur_dat) continue;
		  ds = op->dat[i];
		  qkd_color(White,ds->color);
		  qdbox(ds->symb);
		  if (ds->m == 2 )	iopt |= DOT; else 	iopt &= ~DOT;
		  if (ds->m == 0 )	iopt |= DASH; else 	iopt &= ~DASH;
		  min = (ds->nx > ds->ny)	? ds->ny : ds->nx;
		  qkdraw(min,ds->xd,ds->yd,iopt,&op->x_lo,&op->x_hi,&op->y_lo,&op->y_hi);
		}
	}

	/* new we set the stack2 position */
	if (locate_box(&imr->stack,QKD_B_PLOT, &x2, &x3, &y2, &y3, FIRST) != NULL)
	{
		imr->stack2.xc += x2;
		imr->stack2.yc += y2;
	}
	/* new we set the stack2 position */

	xd[0] = pxs;
	xd[1] = pxe;
	yd[0] = pys;
	yd[1] = pye;
	iopt = DOT | NOAXES;
	qdbox(NULL);
	qkdraw(2,xd,yd,iopt,xd,xd+1,yd,yd+1);
	for (i=0 ; i< oi->n_lab ; i++)
	{
		qkd_lab_color(Lightred);
		p_p_l = oi->lab[i];
		if (p_p_l->text == NULL)	continue;
		if(strncmp(p_p_l->text,"\\scale{",7) == 0)
		{
			j = sscanf(p_p_l->text + 7,"%f",&tmp);
			if (j != 1)
			{
				fine_grid = fgd;
				imr->one_i->transfering_data_to_box = 0;
				imr->stack_in_use = 0;
				return win_printf("wrong \\\\ scale command \n %s",p_p_l->text );				}
			if ((p_p_l->type & 0x03) == USR_COORD  || (p_p_l->type& 0x03) == ABS_COORD)
			{
				if	(p_p_l->type & WHITE_LABEL)		qkd_lab_color(OVER_WHITE);
				else							qkd_lab_color(Lightred);
				snprintf(s,64,"|\\leftarrow{%g %s}\\rightarrow |",tmp,(oi->x_unit != NULL) ? oi->x_unit : " ");
				tmp /= oi->dx * (1 + ((float)1)/512);// was 3)/512
				j = 7252 * oi->width * tmp / (oi->im.nxe - oi->im.nxs);
				if ((p_p_l->type & 0x03) == USR_COORD)
					p_p_l->b = xlabel(s,p_p_l->xla,p_p_l->yla);
				else if ((p_p_l->type & 0x03) == ABS_COORD)
				p_p_l->b = xlabel(s,oi->im.nxs +(oi->im.nxe - oi->im.nxs)
				* p_p_l->xla, oi->im.nys + (oi->im.nye - oi->im.nys)
				* p_p_l->yla);
				if (p_p_l->b == NULL || p_p_l->b->child == NULL || p_p_l->b->n_box != 5)
				{
					fine_grid = fgd;
					imr->one_i->transfering_data_to_box = 0;
					imr->stack_in_use = 0;
					return win_printf("wrong scale");
				}

				p_p_l->b->child[4]->xc = j;
				p_p_l->b->child[3]->xc = j - p_p_l->b->child[3]->w;
				p_p_l->b->child[2]->xc = (j - p_p_l->b->child[2]->w)/2;
				p_p_l->b->child[1]->xc = p_p_l->b->child[0]->w;
				p_p_l->b->child[0]->xc = 0;
				p_p_l->b->w = j;
				if	(p_p_l->type & WHITE_LABEL)		p_p_l->b->color = (short int)OVER_WHITE;
			}
			else
			{
				if	(p_p_l->type & WHITE_LABEL)		qkd_lab_color(OVER_WHITE);
				else							qkd_lab_color(Lightred);
				snprintf(s,64,"|\\leftarrow{%g %s}\\rightarrow |",tmp,(oi->y_unit != NULL) ? oi->y_unit : " ");
				tmp /= oi->dy * (1 + ((float)1)/512);// was 3)/512
				j = 7252 * oi->height * tmp / (oi->im.nye - oi->im.nys);
				if ((p_p_l->type & 0x03) ==  VERT_LABEL_USR)
					p_p_l->b = ylabel(s,p_p_l->xla,p_p_l->yla);
				else	if ((p_p_l->type & 0x03) ==  VERT_LABEL_ABS)
					p_p_l->b = ylabel(s,
					oi->im.nxs +(oi->im.nxe - oi->im.nxs)* p_p_l->xla,
					oi->im.nys + (oi->im.nye - oi->im.nys) * p_p_l->yla);
				if (p_p_l->b == NULL || p_p_l->b->child == NULL
				|| p_p_l->b->child[0]->n_box != 5)
				{
					fine_grid = fgd;
					imr->one_i->transfering_data_to_box = 0;
					imr->stack_in_use = 0;
					return win_printf("wrong vertical scale");
				}
				p_p_l->b->child[0]->child[4]->xc = j;
				p_p_l->b->child[0]->child[3]->xc = j - p_p_l->b->child[0]->child[3]->w;
				p_p_l->b->child[0]->child[2]->xc = (j - p_p_l->b->child[0]->child[2]->w)/2;
				p_p_l->b->child[0]->child[1]->xc = p_p_l->b->child[0]->child[0]->w;
				p_p_l->b->child[0]->child[0]->xc = 0;
				p_p_l->b->child[0]->w = j;
				if	(p_p_l->type & WHITE_LABEL)		p_p_l->b->child[0]->color = (short int)OVER_WHITE;
			}


		}
		else
		{
			if	(p_p_l->type & WHITE_LABEL)		qkd_lab_color(OVER_WHITE);
			else							qkd_lab_color(Lightred);
			if ((p_p_l->type & 0x03) == USR_COORD)
				p_p_l->b = xlabel(p_p_l->text,p_p_l->xla,p_p_l->yla);
			else if ((p_p_l->type & 0x03) == ABS_COORD)
				p_p_l->b = xlabel(p_p_l->text,
				oi->im.nxs +(oi->im.nxe - oi->im.nxs) * p_p_l->xla,
				oi->im.nys + (oi->im.nye - oi->im.nys) * p_p_l->yla);
			else	if ((p_p_l->type & 0x03) ==  VERT_LABEL_USR)
				p_p_l->b = ylabel(p_p_l->text,p_p_l->xla,p_p_l->yla);
			else	if ((p_p_l->type & 0x03) ==  VERT_LABEL_ABS)
				p_p_l->b = ylabel(p_p_l->text,
				oi->im.nxs +(oi->im.nxe - oi->im.nxs)* p_p_l->xla,
				oi->im.nys + (oi->im.nye - oi->im.nys) * p_p_l->yla);
			if	(p_p_l->type & WHITE_LABEL)		p_p_l->b->color = (short int)OVER_WHITE;
		}
	}
	fine_grid = fgd;
	imr->one_i->transfering_data_to_box = 0;
	imr->stack_in_use = 0;
	return 0;
}

imreg	*create_imreg_with_movie(int nx, int ny, int data_type, int n_frames)
{
  imreg *imr;
  O_i *oi = NULL;

  imr = create_and_register_new_image_project(0, 16, 900, 700);
  if (imr == NULL) return NULL;
  if (imr->one_i != NULL) free_one_image(imr->one_i);
  oi = create_one_movie(nx, ny, data_type, n_frames);
  if (oi == NULL) return NULL;
  imr->one_i = oi;
  imr->o_i[0] = oi;
  return imr;
 }
int select_image_of_imreg(imreg *imr, int n)
{
  if ((imr == NULL) || (n < 0) || (n > imr->n_oi))
    return 1;
	imr->cur_oi = n;
	imr->one_i = imr->o_i[imr->cur_oi];
	imr->one_i->need_to_refresh |= PLOTS_NEED_REFRESH;
	return 0;
}
int select_image_of_imreg_and_display(imreg *imr, int n)
{
  if (select_image_of_imreg(imr, n))  return 1;
  return broadcast_dialog_message(MSG_DRAW,0);
}
#endif
