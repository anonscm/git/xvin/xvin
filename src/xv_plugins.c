#ifndef _XV_PLUGIN_C_
#define _XV_PLUGIN_C_
#include <dlfcn.h>

#include "config.h"

#ifdef BUILDING_DLL // if we build the plug'ins version (unix), we need this file



#include "ctype.h"
#include "allegro.h"
#include "allegro/internal/aintern.h"

#include "xv_plugins.h" // for inclusions of libraries that control dynamic loading of libraries.

#include "xvin.h"
#include "xv_main.h"

#ifdef XV_MAC
#include "CoreFoundation/CFBundle.h" // to access bundle info, like path and resources...
#endif

#ifdef XV_GTK_DIALOG
#include "form_builder.h"
#endif

#include <dirent.h>
plugins_info plg_in[32];
int plg_in_n = 0;

int useprogdir = 0;



char *local_strdup(char *src)
{
    int i;
    char *c = NULL;

    if (src == NULL)
    {
        return NULL;
    }

    i = strlen(src);
    c = (char *)calloc(i + 1, sizeof(char));

    if (c == NULL)
    {
        return NULL;
    }

    for (; i >= 0; i--)
    {
        c[i] = src[i];
    }

    return (c);
}




char **get_arg(char *input, int *argc)
{
    int i, j;
    int len = 0;
    char **list = NULL, *templ = NULL, ch = 0;

    if ((input == NULL) || ((len = strlen(input)) == 0))
    {
        *argc = 0;
        return (NULL);
    }

    for (i = 0, *argc = 0; i < len;)
    {
        for (j = 0; ((i < len) && (j == 0));)
        {
            j = (isspace(input[i])) ? 0 : 1;
            i += (j == 0) ? 1 : 0;
        }

        if (i < len)
        {
            (*argc)++;
            list = (char **)realloc(list, (*argc + 1) * sizeof(char *));

            if (list == NULL)
            {
                return NULL;
            }

            templ = input + i;
        }

        for (j = 0; ((i < len) && (j == 0));)
        {
            j = (isspace(input[i])) ? 1 : 0;
            i += (j == 0) ? 1 : 0;
        }

        if (i < len)
        {
            ch = input[i];
            input[i] = 0;
        }

        if (list != NULL)
        {
            list[(*argc) - 1] = local_strdup(templ);    /* strdup(temp);*/
        }

        if (i < len)
        {
            input[i++] = ch;
        }
    }

    if (list != NULL)
    {
        list[*argc] = NULL;
    }

    return list;
}






int record_plug_ins(char *name, void *hist)
{
    if (plg_in_n >= 31)
    {
        return 1;
    }

    plg_in[plg_in_n].name = strdup(name);
    plg_in[plg_in_n++].hinstLib = hist;
    return 0;
}




int grep_plug_ins(char *name)
{
    int i;

    if (name == NULL)
    {
        return -1;
    }

    for (i = 0; i < plg_in_n; i++)
    {
        if (strcmp(plg_in[i].name, name) == 0)
        {
            return i;
        }
    }

    return -1;
}

int add_plug_ins_in_config(char *name)
{
    int np = get_config_int("PLUGINS-AUTO", "plugins_n", 0);
    char pl_num[64] = {0};
    snprintf(pl_num, 64, "plugins_%04d", np);
    set_config_int("PLUGINS-AUTO", "plugins_n", ++np);
    set_config_string("PLUGINS-AUTO", pl_num, name);
    return 0;
}

int grep_plug_ins_in_config(char *name)
{
    int np = get_config_int("PLUGINS-AUTO", "plugins_n", 0);
    char pl_num[64] = {0};
    const char *cur_plug_name = NULL;

    if (name == NULL)
    {
        return -1;
    }

    for (int i = 0; i < np; i++)
    {
        snprintf(pl_num, sizeof(pl_num), "plugins_%04d", i);
        cur_plug_name = get_config_string("PLUGINS-AUTO", pl_num, "");

        //printf("%s != %s ?\n", cur_plug_name, name);
        if (strcmp(cur_plug_name, name) == 0)
        {
            return i;
        }
    }

    return -1;
}
int remove_plug_ins_in_config(int n)
{
    int np = get_config_int("PLUGINS-AUTO", "plugins_n", 0);
    char next_pl_num[64] = {0};
    char cur_pl_num[64] = {0};
    const char *next_plug_name = NULL;

    if (n > (np - 1) || n < 0)
    {
        return -1;
    }

    for (int i = n; i < np - 1; i++)
    {
        snprintf(cur_pl_num, sizeof(cur_pl_num), "plugins_%04d", i);
        snprintf(next_pl_num, sizeof(next_pl_num), "plugins_%04d", i + 1);
        next_plug_name = get_config_string("PLUGINS-AUTO", next_pl_num, "");
        set_config_string("PLUGINS-AUTO", cur_pl_num, next_plug_name);
    }

    if (np > 0)
    {
        snprintf(next_pl_num, sizeof(next_pl_num), "plugins_%04d", np - 1);
        set_config_string("PLUGINS-AUTO", next_pl_num, NULL);
        set_config_int("PLUGINS-AUTO", "plugins_n", np - 1);
    }

    return 0;
}


void *grep_plug_ins_hinstLib(char *name)
{
    int i;
    i = grep_plug_ins(name);

    if (i < 0)
    {
        return NULL;
    }

    return plg_in[i].hinstLib;
}




int remove_plug_ins_record(int n)
{
    int i;

    for (i = n; i < plg_in_n && i < 31; i++)
    {
        plg_in[i].name = plg_in[i + 1].name;
        plg_in[i].hinstLib = plg_in[i + 1].hinstLib;
    }

    if (plg_in_n > 0)
    {
        plg_in_n--;
    }

    return 0;
}
char *extract_plugin_name(const char *plugin_file)
{
  char plugin_name[128] = {0}, name[128] = {0};
    int i = 0;
    extract_file_name(plugin_name, sizeof(plugin_name), plugin_file);
    i = strlen(plugin_name);

    while (i > 0 && plugin_name[i] != '.')
    {
        --i;
    }

    if (i >= 0)
    {
        plugin_name[i] = '\0';
    }

#ifndef XV_WIN32
    //sscanf(plugin_name, "lib%s", plugin_file);
    //strcpy(plugin_name, plugin_file);
    sscanf(plugin_name, "lib%s", name);
#else
    //sscanf(plugin_name, "%s", plugin_file);
    //strcpy(plugin_name, plugin_file);
    sscanf(plugin_name, "%s", name);
#endif
    return strdup(name); // plugin_name);
}

void *open_lib(char *plugin_name)
{
//#ifndef XV_WIN32
    char    filename[4096] = {'\0'};
    char plugins_dir[4096] = "./";

#ifdef XV_WIN32
    strcpy(plugins_dir, prog_dir);
#endif

    if (useprogdir)
      strcpy(plugins_dir, prog_dir);

//#endif
    void   *hinstLib;
#ifdef XV_MAC_OLD
    CFBundleRef CFBundleGetMainBundle(void);
    int len;
    strcpy(plugins_dir, "./"); // directory where the plugins are
    CFBundleRef mainBundle = CFBundleGetMainBundle();

    if (mainBundle)
    {
        CFURLRef url = CFBundleCopyBuiltInPlugInsURL(mainBundle);

        if (url)
        {
            if (CFURLGetFileSystemRepresentation(url, true, (unsigned char *)plugins_dir, sizeof(plugins_dir)))
            {
                len = strlen(plugins_dir);
                plugins_dir[len++] = '/';
                plugins_dir[len] = 0;
            }

            CFRelease(url);
        }
    }

#endif

//#ifdef XV_WIN32
//   return LoadLibrary(plugin_name);
//#else
#ifdef XV_WIN32
    snprintf(filename, 256, "%s%s.%s", plugins_dir, plugin_name, PLUGIN_EXT);
#else
    snprintf(filename, 256, "%slib%s.%s", plugins_dir, plugin_name, PLUGIN_EXT);
#endif

    hinstLib = dlopen(filename, RTLD_LAZY); // possible options : RTLD_LAZY or RTLD_NOW or RTLD_GLOBAL
//#endif
    return hinstLib;
}

PL_MAIN get_lib_main(void *lib, const char *plugin_name)
{
    PL_MAIN pmain = NULL;
    char    main_name[128] = "";
    snprintf(main_name, 128, "%s_main", plugin_name);
//#ifdef XV_WIN32
//    pmain = (PL_MAIN) GetProcAddress(lib, main_name);
//#else
    pmain = (PL_MAIN) dlsym(lib, main_name);
//#endif
    return pmain;
}

bool is_a_plugin(void *lib, const char *plugin_name)
{
    return get_lib_main(lib, plugin_name) != NULL;
}

void free_lib(void *lib)
{
//#ifdef XV_WIN32
//    FreeLibrary(lib);
//#else
    dlclose(lib);
//#endif
}

int autoload_plugins(void)
{
    int  argcp;
    char **argvp;
    char pl_name[64] = {0};
    int np = get_config_int("PLUGINS-AUTO", "plugins_n", 0);

    for (int i = 0; i < np; i++)
    {
        snprintf(pl_name, 64, "plugins_%04d", i);
        argvp = get_config_argv("PLUGINS-AUTO", pl_name, &argcp);

        if (argcp > 0)
        {
            if (load_plugin(argcp, argvp) != 0)
            {
                win_printf_OK("error when auto-loading plugin %s", argvp[0]);    // error : we exit
            }
        }
    }
    return 0;
}
/****************************************************************/
/* this function loads a plug'in and execute plugin_main()      */
/* it also record the plug'in                                   */
/* it returns 0 on success                                      */
/****************************************************************/
int load_plugin(int argcp, char *argvp[])
{
    char    *error = NULL;
    char    *plugin_name = argvp[0];
    void   *hinstLib = NULL;
    PL_MAIN ProcAdd = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    hinstLib = open_lib(plugin_name);

    if (hinstLib == NULL)
    {
//#ifndef XV_WIN32
        error = dlerror();
//#endif
# ifdef XV_WIN32
        show_mouse(screen);
#endif
        //printf("%s\n", error);
        win_printf("Could not load plugin %s "/*from file %s*/ "\nError:%s", plugin_name/*, filename*/, error);
        return -1;
    }

    ProcAdd = get_lib_main(hinstLib, plugin_name);

    if (ProcAdd == NULL)
    {
        char    main_name[128] = "";
//#ifndef XV_WIN32
        error = dlerror();
//#endif
        snprintf(main_name, 128, "%s_main", plugin_name);
        win_printf("Could not execute %s from plug'in %s\n%s\n"
                   "this may be due to a dependence problem", main_name, plugin_name, error);
        return (-2);
    }

    // we execute the main :
    (ProcAdd)(argcp, argvp);
    // we record the plug'in:
    record_plug_ins(plugin_name, hinstLib);
    return (0); // retruns 0 on success only
}

#if defined(XV_GTK_DIALOG) && !defined(XV_WIN32)
int list_plugin(void)
{
  //char    *error = NULL;
    void   *lib;
    PL_MAIN ProcAdd;
    DIR *dp = NULL;
    struct dirent entry = {0};
    struct dirent *ep = NULL;
    void *plug_ins[128] = {NULL};
    char *plug_ins_name[128] = {NULL};
    bool plug_ins_state[128] = {false};
    bool plug_ins_new_state[128] = {false};
    bool plug_ins_autoload_state[128] = {false};
    bool plug_ins_new_autoload_state[128] = {false};
    int cur_plug = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    form_window_t *form = form_create("Plugin List");
    form_push_label(form, "Load\t\t\tAutoLoad\t\t\tName");
    form_newline(form);

    char plugins_dir[4096] = "./";

#ifdef XV_WIN32
    strcpy(plugins_dir, prog_dir);
#endif
    dp = opendir(plugins_dir);

    if (dp != NULL)
    {
        while (cur_plug < 128)
        {
            readdir_r(dp, &entry, &ep);

            if (ep == NULL)
            {
                break;
            }

            plug_ins_name[cur_plug] = extract_plugin_name(ep->d_name);
            lib = open_lib(plug_ins_name[cur_plug]);

            if (lib && is_a_plugin(lib, plug_ins_name[cur_plug]))
            {
                void *loaded_lib = grep_plug_ins_hinstLib(plug_ins_name[cur_plug]);
                int autoloaded_lib = grep_plug_ins_in_config(plug_ins_name[cur_plug]);
                plug_ins_state[cur_plug] = plug_ins_new_state[cur_plug] = loaded_lib != NULL;
                plug_ins[cur_plug] = lib;
                plug_ins_autoload_state[cur_plug] = plug_ins_new_autoload_state[cur_plug] = autoloaded_lib >= 0;
                form_push_bool(form, &(plug_ins_new_state[cur_plug]));
                form_push_bool(form, &(plug_ins_new_autoload_state[cur_plug]));
                form_push_label(form, "\t%s", plug_ins_name[cur_plug]);
                form_newline(form);
                //free_lib(lib);
                cur_plug++;
            }
        }

        (void) closedir(dp);
    }
    else
    {
        perror("Couldn't open the directory");
    }

    form_run(form);

    for (int i = 0; i < cur_plug; ++i)
    {
        if (plug_ins_state[i] != plug_ins_new_state[i])
        {
            if (plug_ins_new_state[i])
            {
                ProcAdd = get_lib_main(plug_ins[i], plug_ins_name[i]);
                (ProcAdd)(0, NULL); // FIXME Arguments
                // we record the plug'in:
                record_plug_ins(plug_ins_name[i], plug_ins[i]);
            }
            else
            {
                remove_plug_ins_record(grep_plug_ins(plug_ins_name[i]));
            }
        }
        else if (plug_ins_state[i] == false)
        {
            free_lib(plug_ins[i]);
        }

        if (plug_ins_autoload_state[i] != plug_ins_new_autoload_state[i])
        {
            if (plug_ins_new_autoload_state[i])
            {
                add_plug_ins_in_config(plug_ins_name[i]);
            }
            else
            {
                remove_plug_ins_in_config(grep_plug_ins_in_config(plug_ins_name[i]));
            }
        }
    }

    return (0); // returns 0 on success only
}
#endif

/****************************************************************/
/* this function is called from the file menu.                  */
/* it asks for a plug'in name, and loads it.                    */
/****************************************************************/
int run_module(void)
{
    int i = 0;
//  void* hinstLib;
    int argc = 0, np, autoloaded = 0;
    char **argv = NULL;
    char line[512] = {0};
    char pl_name[64] = {0};
    const char *pg = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    line[0] = 0;
    pg = (const char *)get_config_string("PLUGINS", "last_loaded", NULL);

    if (pg != NULL)
    {
        strcpy(line, pg);
    }

    i = win_scanf("enter name of plugins (followed by eventual options):\n%32ls\n"
                  "%b load plug'in automatically (if plugins is successfully loaded, of course)\n"
		  "%b find plugin in executable birectory\n",
                  &line, &autoloaded,&useprogdir);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    argv = get_arg(line, &argc);

    if (load_plugin(argc, argv) == 0)
    {
        // win_printf("plugin %s loaded successfully !", argv[0]);
        set_config_string("PLUGINS", "last_loaded", argv[0]);
    }
    else return (win_printf_OK("could not exec plugins %s.dll\n"
                                   "this may be due to a dependence problem", pl_name));

    do_update_menu();
    np = get_config_int("PLUGINS-AUTO", "plugins_n", 0);
    snprintf(pl_name, 64, "plugins_%04d", np);

    if (autoloaded == 1)
    {
        set_config_int("PLUGINS-AUTO", "plugins_n", ++np);
        set_config_string("PLUGINS-AUTO", pl_name, line);
    }

    if (argv != NULL)
    {
        for (i = 0; i < argc; i++)
        {
            if (argv[i] != NULL)
            {
                free(argv[i]);
            }
        }

        free(argv);
        argv = NULL;
    }

    return (D_O_K);
}// end of 'run_module'






int remove_module(void)
{
    int i;
    int n;
    PL_MAIN ProcAdd;
    int argc = 0;
    char **argv, line[512], pl_name[64];
    const char  *pg = NULL;
//#ifndef XV_WIN32
    char *dl_error = NULL;
//#endif

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    line[0] = 0;
    pg = (const char *)get_config_string("PLUGINS", "last_loaded", NULL);

    if (pg != NULL)
    {
        strcpy(line, pg);
    }

    i = win_scanf("enter name of plugins to be unloaded %s", line);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    argv = get_arg(line, &argc);
    //win_printf("bef grep");
    n = grep_plug_ins(argv[0]);

    if (n < 0)
    {
        return win_printf_OK("could not recover plugins %s.dll", argv[0]);
    }

    snprintf(pl_name, 64, "%s_unload", argv[0]);
    // If the handle is valid, try to get the function address.
//#ifdef XV_WIN32
//    ProcAdd = (PL_MAIN)GetProcAddress(plg_in[n].hinstLib, pl_name);
//#else
    ProcAdd = (PL_MAIN)dlsym(plg_in[n].hinstLib, pl_name);

    if ((dl_error = dlerror()) != NULL)
    {
        win_printf("error :\n%s", dl_error);
    }

//#endif

    // If the function address is valid, call the function.

    if (ProcAdd != NULL)
    {
        //      win_printf("bef proc");
        (ProcAdd)(argc, argv);
        //win_printf("after proc");
        set_config_string("PLUGINS", "last_loaded", argv[0]);
        //win_printf("bef free");
//#ifdef XV_WIN32
//
//        if (FreeLibrary(plg_in[n].hinstLib) == 0)
//        {
//            return win_printf_OK("could not remove plugins %s.dll\n maybe this test has to inversed...", argv[0]);
//        }
//
//#else

        if (dlclose(plg_in[n].hinstLib) != 0)
        {
            return (win_printf_OK("could not remove plugins %s.%s\n(maybe it is still in use ?)", argv[0], PLUGIN_EXT));
        }

//#endif

        //win_printf("bef remove");
        if (remove_plug_ins_record(n))
        {
            return win_printf_OK("could not remove plugins %s from recorded list", argv[0]);
        }
    }
    else
    {
        win_printf("could not find unload function for plugins %s.dll\n", pl_name);
        return D_O_K;
    }

    //win_printf("bef update");
    do_update_menu();

    if (argv != NULL)
    {
        for (i = 0; i < argc; i++)
        {
            if (argv[i] != NULL)
            {
                free(argv[i]);
            }
        }

        free(argv);
        argv = NULL;
    }

    return D_O_K;
}// end of 'run_module'



#endif // fin de BUILDING_DLL

#endif // fin du fichier XV_PLUGINS_C
