# ifndef _BOX2DEV_C_
# define _BOX2DEV_C_

# include "xvin/platform.h"
# include "box_def.h"
# include "dev_def.h"
# include "xvinerr.h"
# include "box2alfn.h"
# include <stdlib.h>
# include <string.h>
# include "dev_pcx.h"


int 	x_b=0, y_b=0;
short int   /*	*temp, */ last_pt_size=7;
int 	last_color = 1;


extern int	ttf_enable;


//extern int time_debug;
//extern unsigned long ucl_box, ucl_poly, ucl_char, ucl_box_start, ucl_poly_start, ucl_char_start;
//extern double seg_poly;
//unsigned long my_uclock();



int display_box(struct box *bo, int screen_ratio, int color,int mode)
{
	pc_init1();
	pc_fen = screen_ratio;
	pc_color(color,mode);
	return (wr_box(bo));
}
int set_box_origin(struct box *bo, int x0, int y0, int screen_ratio)
{
	bo->xc = (x0 * screen_ratio) / 2;
	bo->yc = (y0 * screen_ratio) / 2;
	return 0;
}

int wr_box(struct box *b0)
{
	int hic_start;
	struct box *ba = NULL;

/*	temp = (short int*)font;*/
	//	if (time_debug)	    ucl_box_start = my_uclock();
	if (b0 == NULL) return 1;
	if (pc_fen <= 0) pc_fen = 1; // avoid dividin by zero
	ba = b0;
	ba->i_box = 0;
	hic_start = ba->n_hic;
	if ( hic_start == 0)
	{
		x_b=0;
		y_b=0;
	}
	(*q_dev.move)((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	if (ba->n_box < 0)
	{
	  //      if (time_debug)	    ucl_box += my_uclock() - ucl_box_start;
		if (ba->n_box == TURN_90)	wr_box_90(ba->child[0]);
		else if (ba->n_box == CHAR_BOX)	wr_char(ba);
		else if (ba->n_box == RE_POLYGONE_BOX
			|| ba->n_box == POLYGONE_BOX
			|| ba->n_box == CLIPED_RE_POLYGONE_BOX
			|| ba->n_box == CLIPED_POLYGONE_BOX)
			wr_polygone(ba);
		//if (time_debug)	    ucl_box_start = my_uclock();
	}
	x_b += ba->xc;
	y_b += ba->yc;

	while(ba->n_hic > hic_start || ba->i_box < ba->n_box )
	{
	        if (ba->i_box < 0)                ba->i_box = 0;
		else if (ba->i_box > ba->n_box)   ba->i_box = ba->n_box;
	        else if (ba->i_box == ba->n_box
			 || (ba->child != NULL
			     && ba->child[ba->i_box] == NULL))	/* all childs printed */
		  { // || ba->child[ba->i_box] == NULL added 31-5-07
			x_b -= ba->xc;
			y_b -= ba->yc;
			ba= ba->parent;
			ba->i_box++;
		}
		else if (ba->child == NULL) return 1;
		else if (ba->child[ba->i_box]->n_box >= 0 )	/* one step down */
		{
			ba = ba->child[ba->i_box];
			ba->i_box = 0;
			x_b += ba->xc;
			y_b += ba->yc;
			(*q_dev.move)((2*x_b)/pc_fen, (2*y_b)/pc_fen);
		}
		else if (ba->child[ba->i_box]->n_box == TURN_90 )
		{
			x_b += ba->child[ba->i_box]->xc;
			y_b += ba->child[ba->i_box]->yc;
			//if (time_debug)	    ucl_box += my_uclock() - ucl_box_start;
			wr_box_90(ba->child[ba->i_box]->child[0]);
			//if (time_debug)	    ucl_box_start = my_uclock();
			x_b -= ba->child[ba->i_box]->xc;
			y_b -= ba->child[ba->i_box]->yc;
			ba->i_box++;
		}
		else if (ba->child[ba->i_box]->n_box == CHAR_BOX )
		{
		  //if (time_debug)	    ucl_box += my_uclock() - ucl_box_start;
			wr_char(ba->child[ba->i_box]);
			//if (time_debug)	    ucl_box_start = my_uclock();
			ba->i_box++;
		}
		else if ( ba->child[ba->i_box]->n_box == RE_POLYGONE_BOX
			|| ba->child[ba->i_box]->n_box == POLYGONE_BOX
			|| ba->child[ba->i_box]->n_box == CLIPED_RE_POLYGONE_BOX
			|| ba->child[ba->i_box]->n_box == CLIPED_POLYGONE_BOX)
		{
		  //if (time_debug)	    ucl_box += my_uclock() - ucl_box_start;
			wr_polygone(ba->child[ba->i_box]);
			//if (time_debug)	    ucl_box_start = my_uclock();
			ba->i_box++;
		}
	}
	x_b -= ba->xc;
	y_b -= ba->yc;
	(*q_dev.move)((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	(*q_dev.line_size)(default_line_size);
	last_pt_size = default_line_size;//7;
	//if (time_debug)	    ucl_box += my_uclock() - ucl_box_start;
	return 0;
}
int wr_char(struct box *b0)
{
	register int i;
	int xm = 0, ym = 0, sx, sy, nc, xt=0, yt=0, xr, yr, xsl, xp=0, yp=0;
	char ch;
	int ltemp;
	char  *bt = NULL;
	unsigned char  *uc = NULL, uch;

	if (b0 == NULL) return 1;
	//if (time_debug)	    ucl_char_start = my_uclock();
	if (ttf_enable && b0->char_val != 0)
	  {
	    if (q_dev.draw_char_90 != NULL) return q_dev.draw_char(b0);
	    else return	draw_ttf_char(b0);
	  }

	bt = box_font.ch + b0->n_char;
	x_b += b0->xc;
	y_b += b0->yc;
	(*q_dev.move)((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	sx = b0->pt;
	sy = b0->pt;
	if ( sx != last_pt_size )
	{
		(*q_dev.line_size)(1+sx/2);
		last_pt_size = 1+sx/2;
	}
	if ( b0->color != last_color )
	{
		(*q_dev.color)((int)b0->color,0);
		last_color = b0->color;
	}
	uc= (unsigned char*)bt-1;
	nc = (int)uc[4];
	xsl = (int)box_font.ch[b0->n_char+2];
	for ( i = 5; i< 5 +2*nc; i+=2)
	{
		uch = uc[i]&0xFE;
		xr = (int)uch;
		ch = bt[i]&0xFE;
		yr = (int)ch;
		if (i>5)
		{
			uch = uc[i-2]&0xFE;
			xr -= (int)uch;
			ch = bt[i-2]&0xFE;
			yr -= (int)ch;
		}
		if ( (uc[1] & 0x01) &&  (bt[i] & 0x01) )
		{
			ltemp = b0->w;
			ltemp *= 16;
			ltemp /= 3;
			ltemp *= xr;
			ltemp /= (((int)(uc[1]&0xFE))*b0->pt);
			xr = ltemp;
		}
		xt += xr;
		yt += yr;
		xp = 2*xm/pc_fen;
		yp = 2*ym/pc_fen;
		ltemp = ((xsl+yt))*3*b0->slx;
		ltemp = (ltemp*sx)/b0->sly;
		ltemp += xt*sx*3;
		ltemp =  (ltemp + 24)/16;
		xm = ltemp;
/*		xm = (xt*sx*3 + 24 + ((xsl+yt)*3*b0->slx*sx)/b0->sly)/16;*/
		ltemp = (yt*sy)*3;
		ltemp = (ltemp + 24)/16;
		ym = ltemp;
/*		ym = (yt*sy*3 + 24)/16;*/

		if ( uc[i] & 0x01 )
			q_dev.rline(((2*(xm))/pc_fen)-xp, ((2*(ym))/pc_fen)-yp);
		else
			q_dev.rmove(((2*(xm))/pc_fen)-xp, ((2*(ym))/pc_fen)-yp);
	}
	x_b -= b0->xc;
	y_b -= b0->yc;
	q_dev.move((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	//if (time_debug)	    ucl_char += my_uclock() - ucl_char_start;
	return 0;
}

int wr_polygone(struct box *b0)
{
	register int i;
	BX_AR  *x = NULL, *y = NULL;
	BX_AR xs, ys;

	//if (time_debug)	    ucl_poly_start = my_uclock();
	if (b0 == NULL) return 1;
	x_b += b0->xc;
	y_b += b0->yc;
	q_dev.move((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	if ( b0->pt != last_pt_size )
	{
		q_dev.line_size(1+b0->pt/2);
		last_pt_size = 1+b0->pt/2;
	}
	if ( b0->color != last_color )
	{
		q_dev.color((int)b0->color,0);
		last_color = b0->color;
	}
	if (b0->child == NULL) return 1;
	x = (BX_AR *)b0->child[0];
	y = (BX_AR *)b0->child[1];
	if (x == NULL || y == NULL) return 1;
	if ( b0->n_box == CLIPED_POLYGONE_BOX || b0->n_box == CLIPED_RE_POLYGONE_BOX)
	  q_dev.clip((2*x_b)/pc_fen, (2*y_b)/pc_fen, (2*(x_b + b0->w))/pc_fen, (2*(y_b + b0->h))/pc_fen);
	if ( b0->n_box == POLYGONE_BOX || b0->n_box == CLIPED_POLYGONE_BOX)
	{
		for (i=0 ; i< b0->n_char && x+i != NULL && y+i != NULL ; i++)
		{
		  if (x[i] & 0x0001)  q_dev.line(((2*x_b)+x[i])/pc_fen, ((2*y_b)+y[i])/pc_fen);
		  else
		    {
		      if (y[i] & 0x0001)	q_dev.dot(((2*x_b)+x[i])/pc_fen, ((2*y_b)+y[i])/pc_fen);
		      q_dev.move(((2*x_b)+x[i])/pc_fen, ((2*y_b)+y[i])/pc_fen);
		    }
		}
		//seg_poly += b0->n_char;
	}
	else if ( b0->n_box == RE_POLYGONE_BOX || b0->n_box == CLIPED_RE_POLYGONE_BOX)
	{
		for (i=0 ; i< b0->n_char && x+i != NULL && y+i != NULL ; i++)
		{
			if (x[i] & 0x0001)		q_dev.rline(x[i]/pc_fen, y[i]/pc_fen);
			else				q_dev.rmove(x[i]/pc_fen, y[i]/pc_fen);
		}
		//seg_poly += b0->n_char;
	}
	if (b0->child != NULL && b0->child[2] != NULL)
	{
		xs = x_b;
		ys = y_b;
		q_dev.move((2*x_b)/pc_fen, (2*y_b)/pc_fen);
		for (i=0 ; i< b0->n_char  && x+i != NULL && y+i != NULL ; i++)
		{
			if ( b0->n_box == RE_POLYGONE_BOX || b0->n_box == CLIPED_RE_POLYGONE_BOX)
			{
/*				q_dev.rmove(x[i]/pc_fen, y[i]/pc_fen);*/
				x_b += x[i]/2;
				y_b += y[i]/2;
			}
			else if ( b0->n_box == POLYGONE_BOX || b0->n_box == CLIPED_POLYGONE_BOX)
			{
/*				q_dev.move(((2*x_b)+x[i])/pc_fen, ((2*y_b)+y[i])/pc_fen);*/
				x_b = xs + x[i]/2;
				y_b = ys + y[i]/2;
			}
			//if (time_debug)	    ucl_poly += my_uclock() - ucl_poly_start;
			wr_box(b0->child[2]);
			//if (time_debug)	    ucl_poly_start = my_uclock();
		}
		x_b = xs;
		y_b = ys;
	}
	if ( b0->n_box == CLIPED_POLYGONE_BOX || b0->n_box == CLIPED_RE_POLYGONE_BOX)
			q_dev.clip_reset();
	x_b -= b0->xc;
	y_b -= b0->yc;
	q_dev.move((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	//if (time_debug)	    ucl_poly += my_uclock() - ucl_poly_start;
	return 0;
}
int wr_polygone_90(struct box *b0)
{
	register int i;
	BX_AR  *x = NULL, *y = NULL;

	if (b0 == NULL) return 1;
	x_b -= b0->yc;
	y_b += b0->xc;
	q_dev.move((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	if ( b0->pt != last_pt_size )
	{
		q_dev.line_size(1+b0->pt/2);
		last_pt_size = 1+b0->pt/2;
	}
	if ( b0->color != last_color )
	{
		q_dev.color((int)b0->color,0);
		last_color = b0->color;
	}
	x = (BX_AR *)b0->child[0];
	y = (BX_AR *)b0->child[1];
	if ( b0->n_box == CLIPED_POLYGONE_BOX
		|| b0->n_box == CLIPED_RE_POLYGONE_BOX)
			q_dev.clip((2*x_b)/pc_fen, (2*y_b)/pc_fen, (2*(x_b - b0->h))/pc_fen, (2*(y_b + b0->w))/pc_fen);
	if ( b0->n_box == POLYGONE_BOX || b0->n_box == CLIPED_POLYGONE_BOX)
	{
		for (i=0 ; i< b0->n_char ; i++)
		{
			if ( x[i] & 0x0001 )
				q_dev.line(((2*x_b)-y[i])/pc_fen, ((2*y_b)+x[i])/pc_fen);
			else
			{
				if ( y[i] & 0x0001 )
					q_dev.dot(((2*x_b)-y[i])/pc_fen, ((2*y_b)+x[i])/pc_fen);
				q_dev.move(((2*x_b)-y[i])/pc_fen, ((2*y_b)+x[i])/pc_fen);
			}
		}
	}
	else if ( b0->n_box == RE_POLYGONE_BOX || b0->n_box == CLIPED_RE_POLYGONE_BOX)
	{
		for (i=0 ; i< b0->n_char ; i++)
		{
			if ( x[i] & 0x0001 )
				q_dev.rline(-y[i]/pc_fen, x[i]/pc_fen);
			else
				q_dev.rmove(-y[i]/pc_fen, x[i]/pc_fen);
		}
	}
	if (b0->child[2] != NULL)
	{
		q_dev.move((2*x_b)/pc_fen, (2*y_b)/pc_fen);
		for (i=0 ; i< b0->n_char ; i++)
		{
			if ( b0->n_box == RE_POLYGONE_BOX ||  b0->n_box == CLIPED_RE_POLYGONE_BOX)
				q_dev.rmove(-y[i]/pc_fen, x[i]/pc_fen);
			else if (b0->n_box == POLYGONE_BOX || b0->n_box == CLIPED_POLYGONE_BOX)
				q_dev.move(((2*x_b)-y[i])/pc_fen, ((2*y_b)+x[i])/pc_fen);
			wr_box_90(b0->child[2]);
		}
	}
	if ( b0->n_box == CLIPED_POLYGONE_BOX || b0->n_box == CLIPED_RE_POLYGONE_BOX)
			q_dev.clip_reset();
	x_b += b0->yc;
	y_b -= b0->xc;
	q_dev.move((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	return 0;
}
int wr_box_90(struct box *b0)
{
	int hic_start;
	struct box *ba = NULL;


	if (b0 == NULL) return 1;
	ba = b0;
	ba->i_box = 0;
	hic_start = ba->n_hic;
	if ( hic_start == 0)
	{
		x_b=0;
		y_b=0;
	}
	q_dev.move((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	if (ba->n_box < 0)
	{
		if ( ba->n_box == CHAR_BOX ) 		wr_char_90(ba);
		else if ( ba->n_box == RE_POLYGONE_BOX
			|| ba->n_box == POLYGONE_BOX
			|| ba->n_box == CLIPED_RE_POLYGONE_BOX
			|| ba->n_box == CLIPED_POLYGONE_BOX )
			wr_polygone_90(ba);
	}
	x_b += ba->xc;
	y_b += ba->yc;

	while(ba->n_hic > hic_start || ba->i_box < ba->n_box)
	{
		if ( ba->i_box == ba->n_box )	/* all childs printed */
		{
			x_b += ba->yc;
			y_b -= ba->xc;
			ba= ba->parent;
			ba->i_box++;
		}
		else if ( ba->child[ba->i_box]->n_box >= 0 )	/* one step down */
		{
			ba = ba->child[ba->i_box];
			ba->i_box = 0;
			x_b -= ba->yc;
			y_b += ba->xc;
			q_dev.move((2*x_b)/pc_fen, (2*y_b)/pc_fen);
		}
		else if ( ba->child[ba->i_box]->n_box == CHAR_BOX )
		{
			wr_char_90(ba->child[ba->i_box]);
			ba->i_box++;
		}
		else if ( ba->child[ba->i_box]->n_box == RE_POLYGONE_BOX
			|| ba->child[ba->i_box]->n_box == POLYGONE_BOX
			|| ba->child[ba->i_box]->n_box == CLIPED_RE_POLYGONE_BOX
			|| ba->child[ba->i_box]->n_box == CLIPED_POLYGONE_BOX )
		{
			wr_polygone_90(ba->child[ba->i_box]);
			ba->i_box++;
		}
	}
	x_b -= ba->xc;
	y_b -= ba->yc;
	q_dev.move((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	q_dev.line_size(default_line_size);
	last_pt_size = default_line_size; //7;
	return 0;
}
int wr_char_90(struct box *b0)
{
	register int i;
	int xm=0, ym=0, sx, sy, nc, xt=0, yt=0, xr, yr, xsl, xp=0, yp=0;
	char ch;
	int ltemp;
	char  *bt = NULL;
	unsigned char  *uc = NULL, uch;

	if (b0 == NULL) return 1;
	if (ttf_enable && b0->char_val != 0)
	  {
	    if (q_dev.draw_char_90 != NULL) return q_dev.draw_char_90(b0);
	    else return	draw_ttf_char_90(b0);
	  }

	bt = box_font.ch + b0->n_char;
	x_b -= b0->yc;
	y_b += b0->xc;
	q_dev.move((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	sx = b0->pt;
	sy = b0->pt;
	if ( sx != last_pt_size )
	{
		q_dev.line_size(1+sx/2);
		last_pt_size = 1+sx/2;
	}
	if ( b0->color != last_color )
	{
		q_dev.color((int)b0->color,0);
		last_color = b0->color;
	}
	uc= (unsigned char*)bt-1;
	nc = (int)uc[4];
	xsl = (int)box_font.ch[b0->n_char+2];
	for ( i = 5; i< 5 +2*nc; i+=2)
	{
		uch = uc[i]&0xFE;
		xr = (int)uch;
		ch = bt[i]&0xFE;
		yr = (int)ch;
		if (i>5)
		{
			uch = uc[i-2]&0xFE;
			xr -= (int)uch;
			ch = bt[i-2]&0xFE;
			yr -= (int)ch;
		}
		if ( (uc[1] & 0x01) &&  (bt[i] & 0x01) )
		{
			ltemp = b0->w;
			ltemp *= 16;
			ltemp /= 3;
			ltemp *= xr;
			ltemp /= (((int)(uc[1]&0xFE))*b0->pt);
			xr = ltemp;
		}
		xt += xr;
		yt += yr;
		xp = -2*ym/pc_fen;
		yp = 2*xm/pc_fen;
		ltemp = ((xsl+yt))*3*b0->slx;
		ltemp = (ltemp*sx)/b0->sly;
		ltemp += xt*sx*3;
		ltemp =  (ltemp + 24)/16;
		xm = (int)ltemp;
/*		xm = (xt*sx*3 + 24 + ((xsl+yt)*3*b0->slx*sx)/b0->sly)/16;*/
		ltemp = (yt*sy)*3;
		ltemp = (ltemp + 24)/16;
		ym = (int)ltemp;
/*		ym = (yt*sy*3 + 24)/16;*/
		if ( uc[i] & 0x01 )
			q_dev.rline(((2*(-ym))/pc_fen)-xp, ((2*(xm))/pc_fen)-yp);
		else
			q_dev.rmove(((2*(-ym))/pc_fen)-xp, ((2*(xm))/pc_fen)-yp);
	}
	x_b += b0->yc;
	y_b -= b0->xc;
	q_dev.move((2*x_b)/pc_fen, (2*y_b)/pc_fen);
	return 0;
}

# endif
