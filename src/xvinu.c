#ifndef _XVINU_C_
#define _XVINU_C_

# include "xvin/platform.h"
# include "util.h"
# include "color.h"
# include "plot_ds.h"
# include "xvin.h"
# include "plot_mn.h"
# include "dev_pcx.h"

# include <allegro.h>
# include <stdlib.h>
# include <string.h>


char 	*last_answer = NULL;		/* answer string */

int 	menu_x0, menu_y1;		/* menu area */
int 	icone_w;	 			/* the width of an icone */
int 	nx_icone = 3;			/* the nuber of icone along x */
int 	grp_v_sp = 3;			/* the distance of two menu group */
int 	Lift_W;					/* the width of a lift */
int 	max_x, max_y;			/* the scrren dimension */
int 	text_scale;				/* the scaling of box */
int 	screen_to_box;			/* the factor from box to screen */
int 	init_state = 0;
int 	ac_color;				/* the active region color */
int 	menu_color;				/* the menu region color */

int		mn_index;
int  	last_popup_index;
int   screen_restoring = 0;
int screen_acquired = 0;
/*
 int    Black;
 int    Blue;
 int    Green;
 int    Cyan;
 int    Red;
 int    Magenta;
 int    Brown;
 int    Lightgray;
 int    Darkgray;
 int    Lightblue;
 int    Lightgreen;
 int    Lightcyan;
 int    Lightred;
 int    Lightmagenta;
 int    Yellow;
 int    White;
*/
# ifdef XV_WIN32
_XV_DLL DIALOG* cur_ac_reg = NULL;
# endif
# ifdef XV_UNIX
DIALOG* cur_ac_reg = NULL;
# endif

# ifdef XV_MAC
DIALOG* cur_ac_reg = NULL;
# endif

int	full_screen = 0;


/*	int init_wasisdas (void);
 *	DESCRIPTION	iniit all buffers
 *
 *
 *
 *	RETURNS		0 on success, 1 if the box was not active
 *
 */
int init_wasisdas (void)
{
	max_data_color = 8;
	if ( data_color == NULL)
	{
		data_color = (int *)calloc(max_data_color,sizeof(int));
		if (data_color == NULL)
		{
			fprintf(stderr,"calloc error \n");
			exit (1);
		}
	}
	data_color[0] = Yellow;
	data_color[1] = Lightgreen;
	data_color[2] = Lightred;
	data_color[3] = Lightblue;
	data_color[4] = Lightmagenta;
	data_color[5] = Lightgray;
 	data_color[6] = Lightcyan;
	data_color[7] = Brown;


	max_data_symbols = 10;
	if ( data_symbols == NULL)
	{
		data_symbols = (char **)calloc(max_data_symbols,sizeof(char*));
		if (data_symbols == NULL)
		{
			fprintf(stderr,"calloc error \n");
			exit (1);
		}
	}
	data_symbols[0] = strdup("\\pl");
	data_symbols[1] = strdup("\\cr");
	data_symbols[2] = strdup("\\di");
	data_symbols[3] = strdup("\\sq");
	data_symbols[4] = strdup("\\oc");
	data_symbols[5] = strdup("\\fd");
	data_symbols[6] = strdup("\\fs");
	data_symbols[7] = strdup("\\fx");
	data_symbols[8] = strdup("\\fp");
	data_symbols[9] = strdup("\\bu");


 	ac_color = Darkgray;		/* the active region color */
 	menu_color = Lightgray;		/* the menu region color */

	menu_x0 = ( max_x * 52 )/ 64;
	icone_w = (max_x - menu_x0)/nx_icone;
	menu_x0 = max_x - nx_icone * icone_w;
	menu_y1 = ( max_y * 42 )/ 64;
	text_scale = (30 * 1024)/max_x;
	Lift_W = (20 * max_x)/1024;
	grp_v_sp = (3 * max_y) / 768;
	if ( grp_v_sp == 0) grp_v_sp = 1;
	screen_to_box = text_scale /2;
	init_state = 1;
/*
	fill_region(0,0,menu_x0-1,max_y-1,ac_color);
	fill_region(menu_x0,0,max_x-1,max_y-1,menu_color);
*/
	return 0;
}


int	start_xvin(int color_depth, int gfx_mode, int w, int h)
{
	int i = demgr(color_depth, gfx_mode, w, h);
	pc_init();
	init_wasisdas ();
	init_plot_treat_menu();		// changed 2003/11/26, NG
	init_plot_file_menu();		// added 2004/02/09, NG
	init_image_menu(); 		// added 2003/11/23, NG
	return i;
}


/*	return OK to overwrite, WIN_CANCEL otherwise */
// if path is not null, the file name is build from file and path
// if path is null, file is expected to contains both file and path
int	do_you_want_to_overwrite(const char *file, const char *path, const char *mes)
{
	int i;
	FILE *fp = NULL;
	char *file1 = NULL, dir[512] = {0};
	unsigned long long size;
	time_t fti;

	if (path != NULL)
	  {
	    strncpy(dir, path, 510);
	    put_backslash(dir);
	    i = 511 - strlen(dir);
	    strncat(dir,file,i);
	  }
	else strncpy(dir, file,512);


	fp = fopen (dir, "r");
	if (fp == NULL)		return WIN_OK;
	fclose (fp);

#ifndef XV_UNIX
	size = file_size_ex(dir);
#else
    size = 0;
#endif
	fti = file_time(dir);

	file1 = strdup(dir);
#ifdef XV_UNIX
	i = win_printf ("File %s \nalready exists\ndate %s \nsize %llu \n %s\n"
					"ready to overwrite?...",backslash_to_slash(file1),
					ctime(&fti),size,(mes==NULL)?" ":mes);
#else
	i = win_printf ("File %s \nalready exists\ndate %s \nsize %I64u \n %s\n"
					"ready to overwrite?...",backslash_to_slash(file1),
					ctime(&fti),size,(mes==NULL)?" ":mes);
#endif
	return i;
}




/*	ac_grep(acreg *ac, char *format, ...)
 *	DESCRIPTION	find stuff in an acreg using a format input
 *			and attach pointer to it
 *			%im -> imreg, %pr -> pltreg, %oi -> O_i
 *			%oic -> char O_i, %oii -> short int O_i
 *			%oif -> float O_i, %oiz -> complex int O_i
 *			%oiop -> the current one_plot of an image
 *			%op -> O_p, %ds -> data-set, %ti -> title_button
 *			%pk -> pick_list
 *			%tx -> text-region, %tb -> text_bufer
 *	RETURNS		the number of item read on success, -1 otherwise
 *
 */
int 	ac_grep(DIALOG *di, char *fmt, ...)
{
	register int i, j;
	char c[32] = {0}, *p = NULL;
	int n = 0, not_done = 1;
	va_list ap;
	void *vp = NULL;
	imreg *imr = NULL;
	pltreg *pr = NULL;
/*	Txreg *tr = NULL;*/
	O_p *op = NULL;

	if (di == NULL)
		di = active_dialog;
	va_start(ap, fmt);
	for (p = fmt, i = 0, c[0] = 0; not_done ; p++)
	{
		not_done = (*p == 0) ? 0 : 1;
		if ( *p == '%' || *p == 0)
		{
			if (i != 0)
			{
				i = 0;
				if (strncmp(c,"im",2) == 0)
				{
					vp = va_arg(ap, imreg**);
					*((imreg**)vp) = imr = find_imr_in_current_dialog(di);
					if (imr == NULL)
					{
					  //win_printf("can't find image_region");
						va_end(ap);
						return -1;
					}
					n++;
				}
				else if (strncmp(c,"pr",2) == 0)
				{
					vp = va_arg(ap, pltreg**);
					*((pltreg**)vp) = pr = find_pr_in_current_dialog(di);
					if (pr == NULL)
					{
					  //win_printf("can't find plot_region");
						va_end(ap);
						return -1;
					}
					n++;
				}
				else if ((j = strncmp(c,"oi",2)) == 0)
				{
					vp = va_arg(ap, O_i**);
					if (imr == NULL)
					{
						imr = find_imr_in_current_dialog(di);
						if (imr == NULL)
						{
							win_printf("can't find image_region");
							va_end(ap);
							return -1;
						}
					}
					*((O_i**)vp) = imr->one_i;
					if ((strncmp(c,"oic",3) == 0) && (imr->one_i->im.data_type != IS_CHAR_IMAGE))
					{
						win_printf("can't find a CHAR image");
						va_end(ap);
						return -1;
					}
					if ((strncmp(c,"oii",3) == 0) && (imr->one_i->im.data_type != IS_INT_IMAGE))
					{
						win_printf("can't find a INT image");
						va_end(ap);
						return -1;
					}
					if ((strncmp(c,"oif",3) == 0) && (imr->one_i->im.data_type != IS_FLOAT_IMAGE))
					{
						win_printf("can't find a FLOAT image");
						va_end(ap);
						return -1;
					}
					if ((strncmp(c,"oiz",3) == 0) && (imr->one_i->im.data_type != IS_COMPLEX_IMAGE))
					{
						win_printf("can't find a COMPLEX image");
						va_end(ap);
						return -1;
					}
					if (strncmp(c,"oiop",4) == 0)
					{
						if (imr->one_i->cur_op < 0)
						{
							win_printf("can't find plot in image");
							va_end(ap);
							return -1;
						}
						*((O_p**)vp) = imr->one_i->o_p[imr->one_i->cur_op];
					}
					n++;
				}
				else if (strncmp(c,"op",2) == 0)
				{
					vp = va_arg(ap, O_p**);
					if (pr == NULL)
					{
						pr = find_pr_in_current_dialog(di);
						if (pr == NULL)
						{
							win_printf("can't find plot_region");
							va_end(ap);
							return -1;
						}
					}
					*((O_p**)vp) = op = pr->one_p;
					n++;
				}
				else if (strncmp(c,"ds",2) == 0)
				{
					vp = va_arg(ap, d_s**);
					if (pr == NULL)
					{
						pr = find_pr_in_current_dialog(di);
						if (pr == NULL)
						{
							win_printf("can't find plot_region");
							va_end(ap);
							return -1;
						}
					}
					op = pr->one_p;
					if (op->n_dat)
						*((d_s**)vp) = op->dat[op->cur_dat];
					else
					{
						win_printf("can't find data set");
						va_end(ap);
						return -1;
					}
					n++;
				}
			}
		}
		else if (i < 32)
		{
			c[i++] = *p;
			c[i] = 0;
		}
	}
	va_end(ap);
	return n;
}



# endif
