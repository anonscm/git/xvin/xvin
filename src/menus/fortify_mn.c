#include "fortify_mn.h"
#include "xvin.h"

MENU fortify_menu[32] = {NULL};

#ifdef FORTIFY

int check_all(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    Fortify_CheckAllMemory();
    return D_O_K;
}
int out_all(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    Fortify_OutputAllMemory();
    return D_O_K;
}
int enter_scope(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    return win_printf_OK("Scope number %d",Fortify_EnterScope());
}
int leave_scope(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    Fortify_LeaveScope();
    return D_O_K;
}
int dump_scope(void)
{
    int scope = 0;

    if(updating_menu_state != 0)	return D_O_K;
    if (win_scanf("scope number %d",&scope) == WIN_CANCEL)	return D_O_K; 
    Fortify_DumpAllMemory(scope);
    return D_O_K;
}



void fortify_error(const char *mes)
{
    static char *file = NULL;
    static int mesid = 0, respond_type = OK;
    FILE *fp = NULL;

    if (file == NULL)
    {
        file = my_sprintf(file,"%sfortify.log",prog_dir);
        fp = fopen(file,"w");
        if (fp != NULL)
        {
            fprintf(fp,"Starting\n");
            fclose(fp);
        }
    }
    if (mes != NULL)
    {
        fp = fopen(file,"a");
        if (fp != NULL)
        {
            fprintf(fp,"%d : %s\n",mesid++,mes);
            fclose(fp);
        }
    }
    if (respond_type == WIN_CANCEL && mes != NULL) return;
    if (mes) respond_type = win_printf("%s\n to get next message press OK to store them Cancel",mes);
    return;
}


int display_fortify_message(void)
{
    if(updating_menu_state != 0)	return D_O_K;
    fortify_error(NULL);
    return 0;  
}

int set_fortify_next(void)
{
    char all[256] = {0};
    FILE *fp = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(all,256,"%s%s",prog_dir,"fortify-on.txt");
    fp = fopen(all,"w");
    if (fp) 
    {
        fprintf(fp,"On\n");
        fclose(fp);
        return win_printf_OK("Your have activated the Fortify memory\nchecker for your next Xvin Session ");
    }
    else return win_printf_OK("Could not open file %s",backslash_to_slash(all));

}
int disable_fortify_next(void)
{
    char all[256] = {0};
    if(updating_menu_state != 0)	return D_O_K;
    snprintf(all,256,"%s%s",prog_dir,"fortify-on.txt");
    if (delete_file(all))
        return win_printf_OK("Could not delete file %s",backslash_to_slash(all));
    return win_printf_OK("Your have disabled the Fortify memory\nchecker for your next Xvin Session ");
}

fortify_menu[32] =
{
    {"Enter scope",			enter_scope,  	NULL,	0,	NULL},	
        {"Leave scope",	                leave_scope, 	NULL,0,	NULL},
        {"Display messages",            display_fortify_message, 	NULL,0,	NULL},
        {"Dump Scope",		        dump_scope,  	NULL,	0,	NULL},	
        {"Check all",			check_all,  	NULL,	0,	NULL},	
        {"Dump all",	                out_all, 	NULL,0,	NULL},
        {"Disable Fortify",	        disable_fortify_next, 	NULL,0,	NULL},
        {NULL,                      NULL,       NULL,   0, NULL }
};
#endif	
