# include <stdlib.h>
# include <string.h>

# include "allegro.h"
# include "xvin.h"
# include "plot_op.h"
# include "gr_file.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "plot_mn.h"
# include "plot_reg_gui.h"



int do_reinit_keyboard(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)      return 0;

    remove_keyboard();
    install_keyboard();
    remove_mouse();
    install_mouse();
# ifdef XV_WIN32
    show_mouse(screen);
#endif
    return 0;
}


// the following function ocntains instructions that are common to all graphics modes
int set_XVin_resolution(int size, int height, int is_full_screen)
{
    pltreg *pr = NULL;
    destroy_bitmap(plt_buffer);
    plt_buffer = create_bitmap(size,height);//(SCREEN_W, SCREEN_H);
    switch_to_memory_buffer(screen);
    full_screen = 0;
    the_dialog[0].w = size;//SCREEN_W;
    the_dialog[0].h = height;//SCREEN_H;
    the_dialog[2].w = size;//SCREEN_W;
    the_dialog[2].h = height;//SCREEN_H - main_menu_height; 
    the_dialog[2].y = main_menu_height;
    ac_grep(cur_ac_reg,"%pr", &pr);
    if (pr && pr->one_p)
        pr->one_p->need_to_refresh = 1;
    broadcast_dialog_message(MSG_DRAW, 0);
# ifdef XV_WIN32
    show_mouse(screen);
#endif
    set_config_int("WINDOW_MODE", "size",   size);
    set_config_int("WINDOW_MODE", "height", height);
    set_config_int("WINDOW_MODE", "full-screen", is_full_screen);


    flush_config_file();

    return (0);
}

int do_win_choose_size(void)
{
    if (updating_menu_state != 0)
    {
        return D_O_K;
    }
    int w = win_screen_w - 20;
    int h = win_screen_h - 50;

    win_scanf("Choose screen size :\n Width %d\n Height %d\n", &w, &h);
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, w, h) != 0)
    {
        return 1;
    }

    set_XVin_resolution(w, h, 0);
    return 0;

}
int do_win_choose_max_size(void)
{
    if (updating_menu_state != 0)
    {
        return D_O_K;
    }
    int w = win_screen_w - 20;
    int h = win_screen_h - 50;

    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 900, 700, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, w, h) != 0)
    {
        return 1;
    }

    set_XVin_resolution(w, h, 0);
    return 0;
}
int do_win_l(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 900 || win_screen_h < 700)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 900 && SCREEN_H == 700)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 900 || win_screen_h < 700) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 900, 700, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 900, 700) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(900, 700, 0);
    return 0;
}
int do_win_1000(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 1000 || win_screen_h < 718)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 1000 && SCREEN_H == 718)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 1000 || win_screen_h < 718) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 1024, 768, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 1000, 718) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(1000, 718, 0);
    return 0;
}
int do_win_1240(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 1240 || win_screen_h < 768)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 1240 && SCREEN_H == 768)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 1240 || win_screen_h < 768) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 1280, 1024, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 1240, 768) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(1240, 768, 0);
    return 0;
}

int do_win_1260(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 1260 || win_screen_h < 974)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 1260 && SCREEN_H == 974)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 1260 || win_screen_h < 974) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 1280, 1024, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 1260, 974) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(1260, 974, 0);
    return 0;
}
int do_win_1660(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 1660 || win_screen_h < 1160)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 1660 && SCREEN_H == 1160)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 1660 || win_screen_h < 1160) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 1280, 1024, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 1660, 1160) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(1660, 1160, 0);
    return 0;
}
int do_win_1660t(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 1660 || win_screen_h < 1020)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 1660 && SCREEN_H == 1020)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 1660 || win_screen_h < 1020) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 1280, 1024, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 1600, 1020) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(1660, 1020, 0);
    return 0;
}
int do_win_1420(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 1420 || win_screen_h < 850)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 1420 && SCREEN_H == 850)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 1420 || win_screen_h < 850) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 1280, 1024, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 1420, 850) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(1420, 850, 0);
    return 0;
}


int do_win_1900(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 1900 || win_screen_h < 1050)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 1900 && SCREEN_H == 1050)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 1900 || win_screen_h < 1050) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif 
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 1280, 1024, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 1900, 1050) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(1900, 1050, 0);
    return 0;
}


int do_win_2450(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 2560 || win_screen_h < 1600)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 2560 && SCREEN_H == 1600)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 2560 || win_screen_h < 1600) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 1280, 1024, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 2450, 1550) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(2450, 1550, 0);
    return 0;
}


int do_win_2400(void)
{
  // extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 2400 || win_screen_h < 1550)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 2400 && SCREEN_H == 1550)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 2400 || win_screen_h < 1550) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 1280, 1024, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 2400, 1550) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(2400, 1550, 0);
    return 0;
}

int do_win_2500(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 2500 || win_screen_h < 1400)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 2500 && SCREEN_H == 1400)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 2500 || win_screen_h < 1400) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 1280, 1024, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 2500, 1400) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(2500, 1400, 0);
    return 0;
}
int do_win_3600(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 3600 || win_screen_h < 2000)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 3600 && SCREEN_H == 2000)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 3600 || win_screen_h < 2000) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 1280, 1024, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 3600, 2000) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(3600, 2000, 0);
    return 0;
}


int do_win_3100(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 3100 || win_screen_h < 1700)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 3100 && SCREEN_H == 1700)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 3100 || win_screen_h < 1700) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 1280, 1024, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 3100, 1700) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(3100, 1700, 0);
    return 0;
}

int do_win_3800(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 3800 || win_screen_h < 2100)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 3800 && SCREEN_H == 2100)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 3800 || win_screen_h < 2100) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 1280, 1024, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 3800, 2100) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(3800, 2100, 0);
    return 0;
}




int do_full_screen_1280(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 1280 || win_screen_h < 1024)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 1280 && SCREEN_H == 1024)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 1280 || win_screen_h < 1024) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
    //    if (set_gfx_mode(GFX_AUTODETECT_FULLSCREEN, 1280, 1024, 0, 0) != 0)  {

#ifdef XV_WIN32

    if (start_xvin(32, GFX_DIRECTX_OVL, 1280, 1024) != 0)
#endif
    {
        if (start_xvin(32, GFX_AUTODETECT_FULLSCREEN, 1280, 1024) != 0)
        {
            set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
            allegro_message("Error setting graphics mode\n%s\n", allegro_error);
            return D_O_K;
        }
    }

    set_XVin_resolution(1280, 1024, 1);
    return 0;
}


int do_full_screen_1600(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 1600 || win_screen_h < 1200)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 1600 && SCREEN_H == 1200)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 1600 || win_screen_h < 1200) return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_FULLSCREEN, 1600, 1200, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_FULLSCREEN, 1600, 1200) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(1600, 1200, 1);
    return 0;
}

int do_full_screen(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 1024 || win_screen_h < 768)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 1024 && SCREEN_H == 778)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 1024 || win_screen_h < 768)   return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //if (set_gfx_mode(GFX_AUTODETECT_FULLSCREEN, 1024, 768, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_FULLSCREEN, 1024, 768) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(1024, 768, 1);
    return 0;
}


int do_full_screen_over(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 1024 || win_screen_h < 768)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 1024 && SCREEN_H == 778)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 1024 || win_screen_h < 768)   return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif 
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
    //if (set_gfx_mode(GFX_AUTODETECT_FULLSCREEN, 1024, 768, 0, 0) != 0)  {
#ifdef XV_WIN32

    if (start_xvin(32, GFX_DIRECTX_OVL, 1024, 768) != 0)
#endif
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(1024, 768, 1);
    return 0;
}


int do_full_screen_direct(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 1024 || win_screen_h < 768)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 1024 && SCREEN_H == 778)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 1024 || win_screen_h < 768)   return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif  
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
    //if (set_gfx_mode(GFX_AUTODETECT_FULLSCREEN, 1024, 768, 0, 0) != 0)  {
#ifdef XV_WIN32

    if (start_xvin(32, GFX_DIRECTX, 1024, 768) != 0)
#endif
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(1024, 768, 1);
    return 0;
}


int do_win_s(void)
{
  //extern int   switch_to_memory_buffer(BITMAP * buf);

    if (updating_menu_state != 0)
    {
        if (win_screen_w < 640 || win_screen_h < 480)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        if (SCREEN_W == 640 && SCREEN_H == 480)
            active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    if (win_screen_w < 640 || win_screen_h < 480)   return D_O_K;

    //scare_mouse();
# ifdef XV_WIN32
    show_mouse(NULL);
#endif
    set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);

    //    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0) != 0)  {
    if (start_xvin(32, GFX_AUTODETECT_WINDOWED, 640, 480) != 0)
    {
        set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
        allegro_message("Error setting graphics mode\n%s\n", allegro_error);
        return D_O_K;
    }

    set_XVin_resolution(640, 480, 1);
    return 0;
}

int select_ttf(void)
{
    if (updating_menu_state != 0)
    {
        if (ttf_enable)   active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    ttf_enable = (ttf_enable == 0) ? 1 : 0;
    //  win_printf("ttf enable %d",ttf_enable);
    return D_O_K;
}

int do_change_font(void)
{
    if (updating_menu_state != 0)
    {
        if (ttf_enable)   active_menu->flags |=  D_SELECTED;
        else active_menu->flags &= ~D_SELECTED;

        return D_O_K;
    }

    const char *datafilefullpath = get_config_string("FONT", "DATAFILE", NULL);
    char *choosen_file = open_one_file("Choose a font file", datafilefullpath, "Fonts files\0*.dat\0\0");
    if (choosen_file)
    {
        init_fonts(choosen_file);
        main_menu_height = normalsize_font_height + 10;
    }
    free (choosen_file);
    set_XVin_resolution(SCREEN_W, SCREEN_H, full_screen);

    return D_O_K;
}

MENU plot_display_menu[32] =
{

    { "Choose arbritrary size",      do_win_choose_size,    NULL,       0, NULL  },
    { "Choose maximum size",      do_win_choose_max_size,    NULL,       0, NULL  },
    { "Win 640x480",      do_win_s,    NULL,       0, NULL  },
    { "Win 900x700",      do_win_l,        NULL,       0, NULL  },
    { "Win 1000x718",     do_win_1000,        NULL,       0, NULL  },
    { "Win 1240x768",    do_win_1240,        NULL,       0, NULL  },
    { "Win 1260x974",    do_win_1260,        NULL,       0, NULL  },
    { "Win 1420x850",    do_win_1420,        NULL,       0, NULL  },
    { "Win 1660x1160",    do_win_1660,        NULL,       0, NULL  },
    { "Win 1660x1020",    do_win_1660t,        NULL,       0, NULL  },
    { "Win 1900x1050",    do_win_1900,        NULL,       0, NULL  },
    { "Win 2400x1550",    do_win_2400,        NULL,       0, NULL  },
    { "Win 2450x1550",    do_win_2450,        NULL,       0, NULL  },
    { "Win 2500x1400",    do_win_2500,        NULL,       0, NULL  },
    { "Win 3100x1700",    do_win_3100,        NULL,       0, NULL  },
    { "Win 3600x2000",    do_win_3600,        NULL,       0, NULL  },
    { "Win 3800x2100",    do_win_3800,        NULL,       0, NULL  },


    //{ "\0",      NULL,    NULL,       0, NULL  },
    //{ "Full screen 1600x1200",       do_full_screen_1600,   NULL,       0, NULL  },
    //{ "Full screen 1280x1024 over",  do_full_screen_1280,   NULL,   0, NULL  },
    //{ "Full screen 1024x768",        do_full_screen,        NULL,       0, NULL  },
//#ifdef XV_WIN32
    //{ "Full screen 1024x768 over",   do_full_screen_over,        NULL,       0, NULL  },
    //{ "Full screen 1024x768 direct", do_full_screen_direct,        NULL,       0, NULL  },
//#endif
    //{ "Full screen 1600x1200",       do_full_screen_1600,   NULL,       0, NULL  },
    { "\0",      NULL,    NULL,       0, NULL  },
    {"TTF enable/disable",          select_ttf, NULL,   0,  NULL},
    {"Change font data file", do_change_font, NULL, 0, NULL},
    { "Restart keyboard and Mouse",       do_reinit_keyboard,   NULL,       0, NULL  },
    { NULL,                          NULL,             NULL,       0, NULL  }
};
