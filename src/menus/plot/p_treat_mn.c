# include <stdlib.h>
# include <string.h>

# include "allegro.h"
# include "xvin.h"
# include "xv_tools_lib.h"
# include "fftw3_lib.h"
# include "plot_op.h"
# include "gr_file.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "plot_reg_gui.h"
# include "plot_mn.h"


#include "treat/p_treat_basic.h"
#include "treat/p_treat_math.h"
#include "treat/p_treat_fftw.h"
#include "treat/p_treat_p2im.h"

MENU    *plot_n_ds_menu(void);

MENU    *basic_all_ds_menu(void);
int average_all_ds_of_op_by_index(void);
int average_all_ds_of_op_by_index_by_histo(void);
int average_all_ds_of_op_by_index_by_histo_2(void);
int find_peaks_in_Y_of_equally_spaced_ds_all_ds(void);

MENU plot_treat_menu[32] =
{
    /*
       {"Remove Visible points",        cheatting,    NULL,       0, "VISIBLE"  },
       {"Remove Invisible points",       cheatting,    NULL,       0, "INVISIBLE"  },
       {"ops.  on 1 dataset" , NULL, basic_ds_mn, 0, NULL},
       {"math. on 1 dataset" , NULL, plot_1ds_mn, 0, NULL},
       {"math. on 2 datasets", NULL, plot_2ds_mn, 0, NULL},
    */
    { NULL,                          NULL,             NULL,       0, NULL  }
};





/**************************************************************************************************/
int	add_plot_treat_menu_item(char *text, int (*proc)(void), struct MENU *child, int flags, void *dp)
{
    int i;

    for (i = 0; i < 32 && plot_treat_menu[i].text != NULL; i++);

    if (i >= 30)
    {
        return -1;
    }

    plot_treat_menu[i].text = text;
    plot_treat_menu[i].proc = proc;
    plot_treat_menu[i].child = child;
    plot_treat_menu[i].flags = flags;
    plot_treat_menu[i].dp = dp;
    plot_treat_menu[i + 1].text = NULL;
    return 0;
}



/**************************************************************************************************/
int init_plot_treat_menu(void)
{
    static MENU *mn = NULL;
    /*	MENU* interpol_plot_menu();*/

    if (mn != NULL)
    {
        return 1;
    }

    mn = basic_ds_menu();
    add_plot_treat_menu_item("ops. on 1 dataset",  NULL, mn, 0, NULL);
    mn = basic_all_ds_menu();
    add_plot_treat_menu_item("ops. on all dataset",  NULL, mn, 0, NULL);
    mn = dataset_to_values_menu();
    add_plot_treat_menu_item("dataset measurements", NULL, mn, 0, NULL);
    add_plot_treat_menu_item("Linear fit Y = aX",        do_least_square_fit_on_ds, NULL, MENU_INDEX(1), NULL);
    add_plot_treat_menu_item("Linear fit Y = aX +b",        do_least_square_fit_on_ds, NULL, 0, NULL);
    mn = plot_1ds_menu();
    add_plot_treat_menu_item("math. on 1 dataset", NULL, mn,  0, NULL);
    mn = plot_2ds_menu();
    add_plot_treat_menu_item("math. on 2 datasets", NULL, mn,  0, NULL);
    mn = plot_allds_menu();
    add_plot_treat_menu_item("math. on all datasets", NULL, mn, 0, NULL);
    add_plot_treat_menu_item("\0",              	NULL, NULL, 0, NULL);
    mn = interpol_plot_menu();
    add_plot_treat_menu_item("&Interpolation",   	NULL, mn,  0, NULL);
    mn = matrice_plot_menu();
    add_plot_treat_menu_item("Polynome fit",      	NULL, mn, 0, NULL);
    return 0;
}




/************************************************************************************************/

int do_y_of_y(void);
int do_x_of_x(void);

MENU    *basic_ds_menu(void)
{
  static MENU mn[32] = {0};

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "Swap x and y",		    do_swap_x_y,		NULL,	0, 		NULL),
                                      add_item_to_menu(mn, "Remove Visible points",	cheatting,		NULL,	DS_ONE, (char *) "VISIBLE"),
                                      add_item_to_menu(mn, "Remove Invisible points",	cheatting,		NULL,	DS_ONE, (char *) "INVISIBLE"),
                                      add_item_to_menu(mn, "Set Y of Visible points",	changing_value,		NULL,	DS_ONE, (char *) "VISIBLE"),
                                      add_item_to_menu(mn, "Set Y of Invisible points",	changing_value,		NULL,	DS_ONE, (char *) "INVISIBLE"),
                                      add_item_to_menu(mn, "\0",              		NULL,                   NULL,   0,          	NULL);
    add_item_to_menu(mn, "Sort along x",    		do_sort_ds_along_x,     NULL,   0,          	NULL);
    add_item_to_menu(mn, "Sort backward along x",	do_sort_ds_backward_along_x,     NULL,   0,          	NULL);
    add_item_to_menu(mn, "Quicksort > in X",		ds_sort,		NULL,	ASCENDING, 	NULL);
    add_item_to_menu(mn, "Quicksort < in X",		ds_sort,		NULL,	DESCENDING, 	NULL);
    add_item_to_menu(mn, "Normalize data set",	ds_normalized_max_to_1,	NULL,	0, 	NULL);
    add_item_to_menu(mn, "\0",              		NULL,                   NULL,   0,          	NULL);
    add_item_to_menu(mn, "y(x)",            		opp_on_two_ds,          NULL,   Y_OF_X_YY,  	NULL);
    add_item_to_menu(mn, "x(t)",            		do_x_of_t_versus_index,          NULL,  0,	NULL);
    add_item_to_menu(mn, "y(t)",            		do_y_of_t_versus_index,          NULL,  0,	NULL);
    add_item_to_menu(mn, "x(x)",            		do_x_of_x,          NULL,  0,	NULL);
    add_item_to_menu(mn, "y(y)",            		do_y_of_y,          NULL,  0,	NULL);
    //    add_item_to_menu(mn,"x(t)",            		opp_on_one_ds,          NULL,   MENU_INDEX(DS_EXTRACT_X_T),	NULL);
    // add_item_to_menu(mn,"y(t)",            		opp_on_one_ds,          NULL,   MENU_INDEX(DS_EXTRACT_Y_T),	NULL);
    add_item_to_menu(mn, "\0",              		NULL,                   NULL,   0,         	NULL);
    add_item_to_menu(mn, "Merge 2 datasets",		ds_merge,		NULL,	0, 		NULL);
    add_item_to_menu(mn, "Merge all ds", 		do_merge_all_ds_in_op,	NULL,	0,		NULL);
    add_item_to_menu(mn, "Split dataset",		    ds_split_ds,		NULL,	0, 		NULL);

    add_item_to_menu(mn, "Reduce in x",		    ds_reduce,		NULL,	0, 		NULL);
    add_item_to_menu(mn, "Decime in x",		    ds_decime,		NULL,	0, 		NULL);
    add_item_to_menu(mn, "Average along x",	    ds_decime_avg,		NULL,	0, 		NULL);
    add_item_to_menu(mn,"Average all points with same x and compute error",ds_same_x_average,NULL,0,NULL);
    add_item_to_menu(mn, "Avg. periodic sig.",	    ds_periodic_avg,		NULL,	0, 		NULL);
    add_item_to_menu(mn, "Avg. non-periodic sig.",	    do_wrap_y_in_x_using_one_ds,		NULL,	0, 		NULL);
    add_item_to_menu(mn, "\0",              		NULL,                   NULL,   0,         	NULL);
    add_item_to_menu(mn, "Remove Visible points (all ds)",	cheatting,	NULL,	DS_ALL, (char *) "VISIBLE"),
                         add_item_to_menu(mn, "Remove Invisible points (all ds)",	cheatting,	NULL,	DS_ALL, (char *) "INVISIBLE"),
                         add_item_to_menu(mn, "plot->image",		    do_plot_to_im,		NULL,	0, 		NULL);
    return mn;
}


MENU    *more_basic_all_ds_menu(void)
{
  static MENU mn[32] = {0};

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "Merge all ds", 		do_merge_all_ds_in_op,	NULL,	0,		NULL);
    add_item_to_menu(mn, "average ds by index", 		average_all_ds_of_op_by_index,	NULL,	0,		NULL);
    add_item_to_menu(mn, "average ds by histo along index", 		average_all_ds_of_op_by_index_by_histo,	NULL,	0,		NULL);
    add_item_to_menu(mn, "average ds by histo along index 2", 		average_all_ds_of_op_by_index_by_histo_2,	NULL,	0,		NULL);
    add_item_to_menu(mn, "find correlated drift", 	follow_slow_correlated_drift_in_all_ds,	NULL,	0,		NULL);
    add_item_to_menu(mn, "\0",              		NULL,                   NULL,   0,         	NULL);
    add_item_to_menu(mn, "plot->image",		    do_plot_to_im,		NULL,	0, 		NULL);
    add_item_to_menu(mn, "Erase points of all ds given a range of value of one ds",do_erase_all_from_one_ds,NULL,0,NULL);
    add_item_to_menu(mn, "Split dataset",		    ds_split_ds,		NULL,	DS_ALL, 		NULL);

    return mn;
}


MENU    *basic_all_ds_menu(void)
{
  static MENU mn[32] = {0};

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "Remove Visible points (all ds)",	cheatting,	NULL,	DS_ALL, (char *) "VISIBLE");
    add_item_to_menu(mn, "Remove Invisible points (all ds)",	cheatting,	NULL,	DS_ALL, (char *) "INVISIBLE");
    add_item_to_menu(mn, "Set Y of Visible points",	changing_value,		NULL,	DS_ALL, (char *) "VISIBLE");
    add_item_to_menu(mn, "Set Y of Invisible points",	changing_value,		NULL,	DS_ALL, (char *) "INVISIBLE");
    add_item_to_menu(mn, "Kill ds having Visible points",	remove_ds_with_invisible_points,	NULL,	DS_ALL,
                     (char *) "VISIBLE");
    add_item_to_menu(mn, "Kill ds having Invisible points",	remove_ds_with_invisible_points,	NULL,	DS_ALL,
                     (char *) "INVISIBLE");
    add_item_to_menu(mn, "Kill ds having all points Visible",	remove_ds_with_all_invisible_points,	NULL,	DS_ALL,
                     (char *) "VISIBLE");
    add_item_to_menu(mn, "Kill ds having all points Invisible",	remove_ds_with_all_invisible_points,	NULL,	DS_ALL,
                     (char *) "INVISIBLE");
    add_item_to_menu(mn, "Kill ds having large derivative",	remove_ds_with_un_or_visible_points_with_large_derivative,	NULL,
                     0, NULL);
    add_item_to_menu(mn, "Change color of ds having Visible points",	remove_ds_with_invisible_points,	NULL,	DS_ALL,
                     (char *) "VISIBLE|COLOR");
    add_item_to_menu(mn, "Change color of ds having Invisible points",	remove_ds_with_invisible_points,	NULL,	DS_ALL,
                     (char *) "INVISIBLE|COLOR");
    add_item_to_menu(mn, "Remove ds with a specific color",	remove_ds_with_color,	NULL,	DS_ALL, (char *) "VISIBLE");
    add_item_to_menu(mn, "Remove ds without a specific color",	remove_ds_with_color,	NULL,	DS_ALL, (char *) "INVISIBLE");

    add_item_to_menu(mn, "\0",              		NULL,                   NULL,   0,          	NULL);
    add_item_to_menu(mn, "x(t)",            		do_x_of_t_versus_index_all,          NULL,  0,	NULL);
    add_item_to_menu(mn, "y(t)",            		do_y_of_t_versus_index_all,          NULL,  0,	NULL);
    add_item_to_menu(mn, "MAX(x)-MIN(x)",            	x_or_y_range_of_all_ds_same_op,          NULL,   MENU_INDEX(X_AXIS),
                     NULL);

    add_item_to_menu(mn, "MAX(Y)-MIN(Y)",            	x_or_y_range_of_all_ds_same_op,          NULL,   MENU_INDEX(Y_AXIS),
                     NULL);
                     add_item_to_menu(mn, "Distance between ds",    distance_between_datasets,          NULL, 0,
                                                       NULL);
    add_item_to_menu(mn, "Number of Pts. in ds",        	x_or_y_range_of_all_ds_same_op,          NULL,
                     MENU_INDEX(DS_N_PTS),	NULL);
    add_item_to_menu(mn, "<X>,<Y> all Pts. in ds",       x_or_y_range_of_all_ds_same_op,          NULL,
                     MENU_INDEX(DS_MEAN_XY),	NULL);
    add_item_to_menu(mn, "MAX(x)-MIN(x) visible pts.",   x_or_y_range_of_visible_pts_all_ds_same_op,          NULL,
                     MENU_INDEX(X_AXIS),	NULL);
    add_item_to_menu(mn, "MAX(Y)-MIN(Y) visible pts",    x_or_y_range_of_visible_pts_all_ds_same_op,          NULL,
                     MENU_INDEX(Y_AXIS),	NULL);
    add_item_to_menu(mn, "Slidding hiso",    slidding_histogramm_ds_same_op,          NULL,   0,	NULL);
    add_item_to_menu(mn, "Aggregate dataset per source pattern",    do_aggregate_ds_per_source, NULL, 0,	NULL);
    add_item_to_menu(mn, "Aggregate dataset per source pattern with range",    do_aggregate_ds_per_source_range, NULL, 0,	NULL);
    add_item_to_menu(mn, "Apply color of to dataset per source pattern",    do_apply_color_ds_per_source_pattern, NULL, 0,	NULL);
    add_item_to_menu(mn, "Apply color of to dataset per source pattern with range", do_apply_color_ds_per_source_pattern_by_range, NULL, 0,	NULL);
    add_item_to_menu(mn, "\0",              		NULL,                   NULL,   0,         	NULL);
    add_item_to_menu(mn, "More all ds function",    NULL,          more_basic_all_ds_menu(),	0,		NULL);


    add_item_to_menu(mn, "Merge all ds", 		do_merge_all_ds_in_op,	NULL,	0,		NULL);

    add_item_to_menu(mn, "average ds by index", 		average_all_ds_of_op_by_index,	NULL,	0,		NULL);
    add_item_to_menu(mn, "average ds by histo along index", 		average_all_ds_of_op_by_index_by_histo,	NULL,	0,		NULL);
    add_item_to_menu(mn, "find correlated drift", 	follow_slow_correlated_drift_in_all_ds,	NULL,	0,		NULL);
    add_item_to_menu(mn, "\0",              		NULL,                   NULL,   0,         	NULL);
    add_item_to_menu(mn, "plot->image",		    do_plot_to_im,		NULL,	0, 		NULL);
    return mn;
}


/************************************************************************************************/
MENU    *dataset_to_values_menu(void)
{
  static MENU mn[32] = {0};

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "Mean <y>, no window",	mean_ds,		NULL,   WINDOWING_NO,	NULL);
    add_item_to_menu(mn, "Mean <y>, Hanning",	mean_ds,		NULL,   WINDOWING_YES,	NULL);
    add_item_to_menu(mn, "Mean <y> on all ds, no window",	mean_visible_pts_all_ds,	NULL,   WINDOWING_NO,	NULL);
    add_item_to_menu(mn, "\0",             		NULL,                   NULL,   0,          	NULL);
    add_item_to_menu(mn, "Find X barycenter", 	       compute_barycenter_of_visible_points,		NULL,   0,          	NULL);
    add_item_to_menu(mn, "Find extrema", 		ds_find_max,		NULL,   0,          	NULL);
    add_item_to_menu(mn, "Find extrema + width", ds_find_halfmax,	NULL,   0,          	NULL);
    add_item_to_menu(mn, "Find peaks", find_peaks_in_Y_of_equally_spaced_ds_all_ds,	NULL,   0,          	NULL);
    add_item_to_menu(mn, "Draw running variance", draw_running_variance,NULL,0,NULL);
    return mn;
}


MENU    *plot_1ds_fft_menu(void)
{
  static MENU mn[32] = {0};

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "PSD (energy)",	    do_psd,            	    NULL,   PSD_ENERGY | DS_ONE,	NULL);
    add_item_to_menu(mn, "PSD (amplitude)",	do_psd,            	    NULL,   PSD_AMP | DS_ONE,	NULL);
    add_item_to_menu(mn, "FT (project on one mode)",	do_one_mode,            	    NULL,   0,	NULL);
    add_item_to_menu(mn, "auto-correlation",	do_correlation,         NULL,   ONE_DS,		NULL);
    add_item_to_menu(mn, "cross-correlation", do_correlation,         NULL,   TWO_DS,		NULL);
    add_item_to_menu(mn, "plot Hamming window", do_display_Hamming,   NULL,   0,		NULL);
    return mn;
}

MENU    *plot_1ds_more_math_menu(void)
{
  static MENU mn[32] = {0};

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "y -> arctan(y)",	opp_on_one_ds,		    NULL,	MENU_INDEX(DS_ATAN),	NULL);
    add_item_to_menu(mn, "y -> atan2(y,x)",	opp_on_one_ds,		    NULL,	MENU_INDEX(DS_ATAN2),	NULL);
    add_item_to_menu(mn, "y -> unwrap(y)",	opp_on_one_ds,		    NULL,	MENU_INDEX(DS_UNWRAP),	NULL);
    add_item_to_menu(mn, "y -> tanh(y)",	opp_on_one_ds,		    NULL,	MENU_INDEX(DS_TANH),	NULL);
    add_item_to_menu(mn, "y -> 10^(y)",      opp_on_one_ds,          NULL,   MENU_INDEX(DS_EXPO_10),	NULL);
    add_item_to_menu(mn, "y -> log_2(y)",      opp_on_one_ds,          NULL,   MENU_INDEX(DS_LOG2),	NULL);
    add_item_to_menu(mn, "y -> 2^(y)",      opp_on_one_ds,          NULL,   MENU_INDEX(DS_EXPO_2),	NULL);
    add_item_to_menu(mn, "y -> log(y)",      opp_on_one_ds,          NULL,   MENU_INDEX(DS_LOG10),	NULL);
    return mn;
}




/************************************************************************************************/
MENU    *plot_1ds_menu(void)
{
  static MENU mn[32] = {0};

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "x -> ax+b",        opp_on_one_ds,          NULL,   MENU_INDEX(X_AXIS),	NULL);
    add_item_to_menu(mn, "y -> ay+b",        opp_on_one_ds,          NULL,   MENU_INDEX(Y_AXIS),	NULL);
    add_item_to_menu(mn, "Rescale in X",        rescale_ds_according_lo_hi,          NULL,   MENU_INDEX(X_AXIS),	NULL);
    add_item_to_menu(mn, "Rescale in Y",        rescale_ds_according_lo_hi,          NULL,   MENU_INDEX(Y_AXIS),	NULL);
    add_item_to_menu(mn, "y -> y^2",		    opp_on_one_ds,		    NULL,	MENU_INDEX(DS_SQR),	NULL);
    add_item_to_menu(mn, "y -> sqrt{y}",     opp_on_one_ds,          NULL,   MENU_INDEX(DS_SQRT),	NULL);
    add_item_to_menu(mn, "y -> 1/y",		    opp_on_one_ds,		    NULL,   MENU_INDEX(DS_INVERSE),	NULL);
    add_item_to_menu(mn, "y -> |y|",		    opp_on_one_ds,		    NULL,	MENU_INDEX(DS_ABS),	NULL);
    add_item_to_menu(mn, "y -> exp(y)",      opp_on_one_ds,          NULL,   MENU_INDEX(DS_EXPO),	NULL);
    add_item_to_menu(mn, "y -> ln(y)",       opp_on_one_ds,          NULL,   MENU_INDEX(DS_LOG),	NULL);
    add_item_to_menu(mn, "More math", NULL, plot_1ds_more_math_menu(),  0, NULL);
//  add_item_to_menu(mn,"expand \\times 4",     opp_on_one_ds,		NULL,   MENU_INDEX(DS_EXPAND),  NULL);
    add_item_to_menu(mn, "\0",               NULL,                   NULL,   0,		NULL);
    add_item_to_menu(mn, "Differentiate in Y",  do_y_differentiate,   NULL,   0,		NULL);
    add_item_to_menu(mn, "True derivate",    do_true_y_derivative,   NULL,   0,		NULL);
    add_item_to_menu(mn, "Avg derivative",    do_true_y_avg_derivative,   NULL,   0,		NULL);
    add_item_to_menu(mn, "Integrate",        do_integrate_ds,        NULL,   0,		NULL);
    add_item_to_menu(mn, "True integrate",   do_true_integrate_ds,   NULL,   0,		NULL);
    add_item_to_menu(mn, "\0",              	NULL,                   NULL,   0,		NULL);
//  add_item_to_menu(mn,"Power Spectrum",   do_spectrum,            NULL,   0,		NULL);
    add_item_to_menu(mn, "FFT stuff", NULL, plot_1ds_fft_menu(),  0, NULL);
    add_item_to_menu(mn, "\0",              	NULL,                   NULL,   0,		NULL);
    add_item_to_menu(mn, "Filter (FIR)",	    do_fir_filtering,       NULL,   0,		NULL);
    add_item_to_menu(mn,"Gaussian convolution",do_gaussian_convolution,NULL,0,NULL);
    add_item_to_menu(mn, "Filter (Fourier)",	do_filter_ds,    	    NULL,   0,     		NULL);
    add_item_to_menu(mn, "Non linear Filter for flat area",	do_non_linear_plateau_filtering,    	    NULL,   0,     		NULL);
    add_item_to_menu(mn, "Non linear Filter for slopes",   do_filter_nl_ex_slope_auto_r_math,    	    NULL,   0,     		NULL);

    add_item_to_menu(mn, "Hilbert demodulation", do_Hilbert_demodulation, NULL,  0,     		NULL);
    add_item_to_menu(mn, "View Filter",	    do_display_current_filter, NULL, 0, 		NULL);
    add_item_to_menu(mn, "Histogramm Gaussian",	    do_build_histo_with_exponential_convolution, NULL, 0, 		NULL);
    add_item_to_menu(mn, "Histogramm Gaussian (norm 1 ds)",
                     do_build_histo_with_exponential_convolution_norm_for_each_ds, NULL, 0, 		NULL);
    add_item_to_menu(mn, "Histogramm: stretch visibles peaks to a value", do_stretch_histograms, NULL, 0, NULL);
    add_item_to_menu(mn, "Histogramm: shift visibles peaks to a value", do_shift_histograms, NULL, 0, NULL);
    return mn;
}



/************************************************************************************************/
MENU    *plot_2ds_menu(void)
{
  static MENU mn[32] = {0};

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "Add Y on same X",              do_add_on_2_ds,  NULL, ADD,          NULL);
    add_item_to_menu(mn, "Substract Y on same X",        opp_on_two_ds,  NULL, SUBSTRACT,    NULL);
    add_item_to_menu(mn, "Multiply Y on same X",         opp_on_two_ds,  NULL, MULTIPLY,     NULL);
    add_item_to_menu(mn, "Divide Y on same X",           opp_on_two_ds,  NULL, DIVIDE,       NULL);
    add_item_to_menu(mn, "\0",               NULL,           NULL, 0,            NULL);
    add_item_to_menu(mn, "Substract by index",        sub_on_2_ds_by_index,  NULL, 0,    NULL);
    add_item_to_menu(mn, "\0",               NULL,           NULL, 0,            NULL);
    add_item_to_menu(mn, "y(x)",             opp_on_two_ds,  NULL, Y_OF_X_YY,    NULL);
    add_item_to_menu(mn,"y1(y2)",y1_y2_same_x,NULL, 0,     NULL);
    return mn;
}


/************************************************************************************************/
MENU    *plot_allds_menu(void)
{
  static MENU mn[32] = {0};

    if (mn[0].text != NULL)
    {
        return mn;
    }

    add_item_to_menu(mn, "Adjust y",        adjust_ds_mean_y_according_to_visible_points,          NULL,   0,	  NULL);
    add_item_to_menu(mn, "Adjust sparse signal",     adjust_ds_mean_y_for_sparse_signal,          NULL,   0,	  NULL);
    add_item_to_menu(mn, "x -> ax+b",        opp_on_all_ds,          NULL,   MENU_INDEX(X_AXIS),	  NULL);
    add_item_to_menu(mn, "y -> ay+b",        opp_on_all_ds,          NULL,   MENU_INDEX(Y_AXIS),	  NULL);
    add_item_to_menu(mn, "y -> y^2",		    opp_on_all_ds,		    NULL,	MENU_INDEX(DS_SQR),	  NULL);
    add_item_to_menu(mn, "y -> sqrt{y}",     opp_on_all_ds,          NULL,   MENU_INDEX(DS_SQRT),	NULL);
    add_item_to_menu(mn, "y -> 1/y",		    opp_on_all_ds,		    NULL,   MENU_INDEX(DS_INVERSE),	NULL);
    add_item_to_menu(mn, "y -> exp(y)",      opp_on_all_ds,          NULL,   MENU_INDEX(DS_EXPO),	NULL);
    add_item_to_menu(mn, "y -> log(y)",      opp_on_all_ds,          NULL,   MENU_INDEX(DS_LOG10),	NULL);
    add_item_to_menu(mn, "y -> ln(y)",       opp_on_all_ds,          NULL,   MENU_INDEX(DS_LOG),	  NULL);
    add_item_to_menu(mn, "y -> tanh(y)",       opp_on_all_ds,          NULL,   MENU_INDEX(DS_TANH),	  NULL);
    add_item_to_menu(mn, "y -> |y|",		    opp_on_all_ds,		    NULL,	MENU_INDEX(DS_ABS),	  NULL);
    add_item_to_menu(mn, "y -> arctan(y)",	opp_on_all_ds,		    NULL,	MENU_INDEX(DS_ATAN), NULL);
    add_item_to_menu(mn, "\0",               NULL,                   NULL, 0,            NULL);
    add_item_to_menu(mn, "PSD (energy)",	    do_psd,            	    NULL,   PSD_ENERGY | DS_ALL,	NULL);
    add_item_to_menu(mn, "PSD (amplitude)",	do_psd,            	    NULL,   PSD_AMP | DS_ALL,	NULL);
    add_item_to_menu(mn, "View Filter",	    do_display_current_filter, NULL, 0, 		NULL);
    add_item_to_menu(mn, "\0",               NULL,                   NULL, 0,            NULL);
    add_item_to_menu(mn, "Add all ds + error", do_add_all_ds,        NULL, 0,            NULL);
    add_item_to_menu(mn,"Average all ds",do_average_all_ds,NULL,0,NULL);
    add_item_to_menu(mn, "Dump sigma HF od all ds", do_dump_sigma_HF_of_all_ds,        NULL, 0,            NULL);
    add_item_to_menu(mn,"Dump std of all ds",do_dump_std_of_all_ds,NULL,0,NULL);

    add_item_to_menu(mn, "Substract one ds to all", substract_on_ds_to_all_other_by_index,        NULL, 0,            NULL);
    add_item_to_menu(mn, "Get 1 point in each ds", do_pick_points_having_same_x_on_add_all_ds,        NULL, 0,
                     NULL);
    return mn;
}
