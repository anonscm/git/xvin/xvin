# include <stdlib.h>
# include <string.h>

# include "allegro.h"
# include "dev_def.h"
# include "xvin.h"
# include "plot_op.h"
# include "gr_file.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "plot_reg_gui.h"
# include "plot_mn.h"

int	set_plt_opt(void);
int	set_line_size(void);
int animate_plot_scope(void);
int animate_plot_scope2(void);

MENU grid_style_menu[32] =
{
	{"No Grid",	set_plt_opt, NULL, MENU_INDEX(0), (char *) "0"},
	{"Grid on",	set_plt_opt, NULL, MENU_INDEX(1), (char *) "1"},
	{"No fine Grid",	set_plt_opt, NULL, MENU_INDEX(6), (char *) "6"},
	{"Fine Grid on",	set_plt_opt, NULL, MENU_INDEX(7), (char *) "7"},
	{"Set line size",	set_line_size, NULL, 0, (char *) NULL},
        { "\0", 			NULL,   	NULL,       0, NULL  },
	{"X axis no numbering",	set_plt_opt, NULL, MENU_INDEX(2), (char *) "2"},
	{"X axis numbering",	set_plt_opt, NULL, MENU_INDEX(3), (char *) "3"},
        { "\0", 			NULL,   	NULL,       0, NULL  },
	{"Y axis no numbering",	set_plt_opt, NULL, MENU_INDEX(4), (char *) "4"},
	{"Y axis numbering",	set_plt_opt, NULL, MENU_INDEX(5), (char *) "5"},
/*	{"choose X log display",	set_2_5_display, NULL, 0, (char *) "X"},
	{"choose Y log display",	set_2_5_display, NULL, 0, (char *) "Y"},	*/
	{ NULL,          NULL,             NULL,       0, NULL  }
};



MENU plot_options_menu[32] =
{
	{ "data set",    	NULL,	ds_menu,    0, NULL  },
	{ "Plot stuff",   NULL,	op_menu,      0, NULL  },
	{ "axis options",   NULL,	plot_axis_menu,      0, NULL  },
	{ "grid options",   NULL,	grid_style_menu,      0, NULL  },
	{ "animate scope",		animate_plot_scope, NULL, 0, NULL },
	{ "animate scope 2",	animate_plot_scope2, NULL, 0, NULL },
	{ NULL,          NULL,             NULL,       0, NULL  }
};

int	add_plot_options_menu_item( char* text, int (*proc)(void), struct MENU* child, int flags, void* dp)
{
	int i;
	for (i = 0; i < 32 && plot_options_menu[i].text != NULL; i++);
	if (i >= 30)	return -1;
	plot_options_menu[i].text = text;
	plot_options_menu[i].proc = proc;
	plot_options_menu[i].child = child;
	plot_options_menu[i].flags = flags;
	plot_options_menu[i].dp = dp;
	plot_options_menu[i+1].text = NULL;
	return 0;
}





# ifdef AF
int	check_plt_opt(void)
{
	O_p	*op = NULL;
	pltreg *pr = NULL;

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;


	{
		if (mn_index == 0)	return ((op->iopt & NOAXES) ? ON : OFF);
		else if(mn_index == 1) 	return ((op->iopt & NOAXES) ? OFF : ON);
		else if(mn_index == 2)	return ((op->iopt2 & X_NUM) ? OFF : ON);
		else if(mn_index == 3)	return ((op->iopt2 & X_NUM) ? ON : OFF);
		else if(mn_index == 4)	return ((op->iopt2 & Y_NUM) ? OFF : ON);
		else if(mn_index == 5)	return ((op->iopt2 & Y_NUM) ? ON : OFF);				}
	return D_O_K;
}
# endif



int	set_line_size(void)
{
	int i, ls = default_line_size;

	if(updating_menu_state != 0)	return 0;
	i = win_scanf("Define line size %4d\n",&ls);
	if (i == WIN_CANCEL) return 0;
	default_line_size = ls;
	return 0;
}



int	set_plt_opt(void)
{
	O_p	*op = NULL;
	pltreg *pr = NULL;
	int index;

	if(updating_menu_state != 0)
	{
		index = RETRIEVE_MENU_INDEX;
		if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
		{
			active_menu->flags |=  D_DISABLED;
			return D_O_K;
		}
		if (index == 0)
		  {
		    if (op->iopt & NOAXES) active_menu->flags |= D_SELECTED;
		    else active_menu->flags &= ~D_SELECTED;
		  }
		else if(index == 1)
		  {
		    if (op->iopt & NOAXES) active_menu->flags &= ~D_SELECTED;
		    else active_menu->flags |= D_SELECTED;
		  }
		else if(index == 3)
		  {
		    if (op->iopt2 & X_NUM) active_menu->flags |= D_SELECTED;
		    else active_menu->flags &= ~D_SELECTED;
		  }
		else if(index == 2)
		  {
		    if (op->iopt2 & X_NUM) active_menu->flags &= ~D_SELECTED;
		    else active_menu->flags |= D_SELECTED;
		  }
		else if(index == 5)
		  {
		    if (op->iopt2 & Y_NUM) active_menu->flags |= D_SELECTED;
		    else active_menu->flags &= ~D_SELECTED;
		  }
		else if(index == 4)
		  {
		    if (op->iopt2 & Y_NUM) active_menu->flags &= ~D_SELECTED;
		    else active_menu->flags |= D_SELECTED;
		  }
		else if(index == 6)
		  {
		    if (op->iopt & NOAXES)
		      {
			active_menu->flags |=  D_DISABLED;
		      }
		    else
		      {
			active_menu->flags &=  ~D_DISABLED;
			if (fine_grid == 0) active_menu->flags |= D_SELECTED;
			else active_menu->flags &= ~D_SELECTED;
		      }
		  }
		else if(index == 7)
		  {
		    if (op->iopt & NOAXES)
		      {
			active_menu->flags |=  D_DISABLED;
		      }
		    else
		      {
			active_menu->flags &=  ~D_DISABLED;
			if (fine_grid == 1) active_menu->flags |= D_SELECTED;
			else active_menu->flags &= ~D_SELECTED;
		      }
		  }
		return D_O_K;
	}

	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)	return D_O_K;
	index = RETRIEVE_MENU_INDEX;
	/*
	if (active_menu->dp == NULL)	return D_O_K;
	i = sscanf((char*)active_menu->dp,"%d",&index);
	if (i != 1)		return D_O_K;*/
	if (index == 0)		op->iopt |= NOAXES;
	else if (index == 1)		op->iopt &= ~NOAXES;
	else if (index == 2)		op->iopt2 &= ~X_NUM;
	else if (index == 3)		op->iopt2 |= X_NUM;
	else if (index == 4)		op->iopt2 &= ~Y_NUM;
	else if (index == 5)		op->iopt2 |= Y_NUM;
	else if (index == 6)		fine_grid = 0;
	else if (index == 7)		fine_grid = 1;
	op->need_to_refresh = 1;
	refresh_plot(pr, UNCHANGED);
	return 0;
}
int animate_plot_scope(void)
{
	int j;
	O_p	*op = NULL;
	pltreg *pr = NULL;
	d_s *ds;
	int i;
	clock_t dtm, t, t0 = 0, tms, start;
	static float  ms = 20, w  = 0.1, inc = 0.02;
	float  x_lo, x_hi, xd;
	char question[1024] = {0};

	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return D_O_K;

	for ( j = 0, x_lo = x_hi = ds->xd[0]; j < ds->nx && j < ds->ny; j++)
	{
		if (ds->xd[j] > x_hi) x_hi = ds->xd[j];
		if (ds->xd[j] < x_lo) x_lo = ds->xd[j];
	}
	sprintf(question,"Show plot in scope mode\n"
		"data set %d extends from %f to %f\n"
		"select extend of window along x %%f\n"
		"select extend fraction to move %%f\n"
		"time lapse to span this width %%f (ms)",op->cur_dat,x_lo,x_hi);

	i = win_scanf(question,&w,&inc,&ms);

	tms = (CLOCKS_PER_SEC*ms)/1000;



	for (start = clock(), dtm = 0, xd = x_lo, i = 0; xd < x_hi - w; i++, xd += w*inc)
	{
		t0 = clock();
		op->x_lo = xd;
		op->x_hi = xd + w;
		do_one_plot (pr);
		write_and_blit_box(pr->stack, pr->screen_scale, plt_buffer, the_dialog+2, pr);

		for (t = clock() - t0, dtm += t; t < tms; t = clock() - t0);
	}
	start = clock() - start;
	win_printf("Animation last %g sec, %d steps\nmean waiting per cycle %g (ms)",
		((float)start)/CLOCKS_PER_SEC, i, 1000*(float)(dtm)/(i*CLOCKS_PER_SEC));
	return 0;

}
int animate_plot_scope2(void)
{
	int j,  i0, im;
	O_p	*op = NULL;
	pltreg *pr = NULL;
	d_s *ds = NULL;
	int i, npts, dxn, dxwn;
	clock_t dtm, t, t0 = 0, tms, start;
	static float  ms = 20, w  = 0.1, inc = 0.02;
	static int bm = 0;
	float  x_lo, x_hi,  *xd0 = NULL, *yd0;
	char question[1024] = {0};
	BITMAP *p_b0 = NULL, *p_b = NULL;

	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)	return D_O_K;

	for ( j = 0, x_lo = x_hi = ds->xd[0]; j < ds->nx && j < ds->ny; j++)
	{
		if (ds->xd[j] > x_hi) x_hi = ds->xd[j];
		if (ds->xd[j] < x_lo) x_lo = ds->xd[j];
	}
	sprintf(question,"Show time series plot in scope mode\n"
		"data set %d contains %d points\n"
		"select extend of scope as a fraction of total data %%f\n"
		"select fractionnal increment %%f\n"
		"time lapse to span this width %%f (ms)"
		"using video bitmap yes ->1, no ->0%%d",op->cur_dat,ds->nx);

	i = win_scanf(question,&w,&inc,&ms,&bm);

	tms = (CLOCKS_PER_SEC*ms)/1000;
	npts = ds->nx;
	xd0 = ds->xd;
	yd0 = ds->yd;
	dxn = (int)(w * ds->nx);
	dxn = (dxn <= 0) ? 1 : dxn;
	dxwn = (int)(inc * dxn);
	dxwn = (dxn <= 0) ? 1 : dxwn;


	p_b = p_b0 = plt_buffer;
	if (bm)
		p_b = create_video_bitmap(SCREEN_W, SCREEN_H);


	for (start = clock(), i0 = 0, im = dxn, i = 0, dtm = 0; im < npts-1; i++, i0 += dxwn)
	{
		t0 = clock();
		im = i0 + dxn;
		im = (im < npts-1) ? im : npts-1;
		op->x_lo = xd0[i0];
		op->x_hi = xd0[im];
		ds->xd = xd0 + i0;
		ds->yd = yd0 + i0;
		ds->nx = ds->ny = dxn;
		do_one_plot (pr);
		write_and_blit_box(pr->stack, pr->screen_scale, p_b, the_dialog+2, pr);

		for (t = clock() - t0, dtm += t; t < tms; t = clock() - t0);
	}
	start = clock() - start;
	win_printf("Animation last %g sec, %d steps, scope plot of %d points\n"
				"mean execution time per cycle %g (ms)\n"
				"mean time per cycle %g (ms)\n",
		((float)start)/CLOCKS_PER_SEC, i, dxn,
		1000*(float)(dtm)/(i*CLOCKS_PER_SEC),
		1000*(float)(start)/(i*CLOCKS_PER_SEC));
	ds->nx = ds->ny = npts;
	ds->xd = xd0;
	ds->yd = yd0;
	plt_buffer = p_b0;
	if (bm) destroy_bitmap(p_b);
	return 0;

}
