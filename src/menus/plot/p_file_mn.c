
#ifndef _P_FILE_MN_C_
#define _P_FILE_MN_C_

#include "config.h"

#include "file_picker_gui.h"

# include "ctype.h"
# include "allegro.h"
# include "allegro/internal/aintern.h"

#ifdef BUILDING_DLL
#include "xv_plugins.h"
#endif

# include "xvin.h"
# include "plot_op.h"
# include "gr_file.h"
# include "plot_reg.h"
# include "im_reg.h"
# include "plot_gr.h"
# include "xv_main.h"
# include "im_reg.h"
# include "plot_mn.h"
# include "im_mn.h"
# include "plot_reg_gui.h"
# include "dev_ps.h"
# include "dev_pcx.h"
# include "plot2box.h"
# include "plot_mn.h"
// extern int load_jpeg_file_in_imreg(imreg *imr, char *file, char *path);

XV_VAR(int, color_enable);
XV_VAR(int, eps_file_flag);

#define BIG_FONT                         0        /* FONT */
#define COLOR   128
/*
   extern pltreg *project[];
   extern int   project_cur, project_n, project_m;
   extern DIALOG the_dialog[];

   extern DATAFILE *datafile;
   */

XV_VAR(FONT*, original_font);
XV_VAR(FONT*, big_font);
//extern FONT *original_font, *big_font;

XV_ARRAY(MENU, plot_project_menu);
//extern  MENU plot_project_menu[];

MENU plot_recently_loaded[32] = {0};
char re_file[16][256] = {0};
char re_path[16][256] = {0};
int  n_re = 0;

MENU path_recently_visited[32] = {0};
char vi_path[16][256] = {0};
int  n_vi = 0;

//# define FULLFILE_SIZE 16384

char fullfile[FULLFILE_SIZE] = {0}, *fu = NULL;  // space for multiple files

int   get_path_drive(char *path)
{
    if (path == NULL || strlen(path) < 2)
    {
        return -1;
    }

    if (path[1] != ':')
    {
        return -1;
    }

    if ((path[0] >= 'a') && (path[0] < 'a' + 26))
    {
        return (path[0] - 'a');
    }

    if ((path[0] >= 'A') && (path[0] < 'a' + 26))
    {
        return (path[0] - 'A');
    }

    return -1;
}
int path_is_accessible(char *path)
{
#ifdef XV_MAC
    return 111;
#endif
#ifdef XV_UNIX
    return 111;
#endif
    return _al_drive_exists(get_path_drive(path));
}

/*   select a file with a file grabber using config info
 *   char *fpath            a string where to save the selected file path, it may contain a starting path to search
 *      if the first character is set to zero, search will be done from last save file_type_cfg
 *   int fpath_size         the size of fpath
 *   char *file_type_cfg    the file type save in config file example GR-FILE, TRACKING-FILE
 *   char *extensions        "gr", "trk" or "xv,txt"
 *   char *loading_description  the help message
 *   int loading if 1 loading otherwise saving
 *   return 0 if everything OK
 */

int do_select_file(char *fpath, int fpath_size, char *file_type_cfg, char *extensions, char *loading_description,
                   int loading)
{
    char exts[512] = {0};
    char filename[512] = {0};
    char *ful = NULL;

    if (fpath == NULL || file_type_cfg == NULL || extensions == NULL)
    {
        return 1;
    }

    if (loading)
    {
        snprintf(exts, sizeof(exts), "Load file (%s)", extensions);
        unsigned int len = strlen(exts) + 1;
        snprintf(exts + len , sizeof(exts) - len, "%s", extensions);
        len = len + strlen(exts + len) + 1;
        snprintf(exts + len , sizeof(exts) - len, "All file (*.*)");
        len = len + strlen(exts + len) + 1;
        snprintf(exts + len , sizeof(exts) - len, "*.*");
        len = len + strlen(exts + len) + 1;
        len = len < sizeof(exts) - 1 ? len : sizeof(exts) - 1;
        exts[len] = '\0';
        ful = open_one_file_config(loading_description, fpath[0] != 0 ? fpath : NULL, exts, file_type_cfg, "last_loaded");
    }
    else
    {
        snprintf(exts, sizeof(exts), "Save file (%s)", extensions);
        unsigned int len = strlen(exts) + 1;
        snprintf(exts + len , sizeof(exts) - len, "%s", extensions);
        len = len + strlen(exts + len) + 1;
        snprintf(exts + len , sizeof(exts) - len, "All file (*.*)");
        len = len + strlen(exts + len) + 1;
        snprintf(exts + len , sizeof(exts) - len, "*.*");
        len = len + strlen(exts + len) + 1;
        len = len < sizeof(exts) - 1 ? len : sizeof(exts) - 1;
        exts[len] = '\0';

        if (fpath)
        {
            extract_file_name(filename, sizeof(filename), fpath);
        }

        ful = save_one_file_config(loading_description, fpath[0] != 0 ? fpath : NULL, filename, exts, file_type_cfg,
                                  "last_saved");
    }

    if (ful)
    {
        strncpy(fpath, ful, fpath_size);
        reorder_visited_path_list(fpath);
        free(ful);
        return 0;
    }

    return 1;
}


int do_load_plot_from_recently_visited_path(void)
{
    pltreg *pr = NULL;
    imreg *imr = NULL;
    //O_p *op;
    //int j;
    //extern char fullfilei[], *fui;
    //int do_load_im(void);

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (active_menu->text == NULL)
    {
        return D_O_K;
    }

    /*


       op = create_plot_from_gr_file(file, path);
       if (op == NULL) win_printf_OK("Could not loaded %s\n from %s",
       backslash_to_slash(file), backslash_to_slash(path));

       pr = find_pr_in_current_dialog(NULL);
       if (pr == NULL)
       pr = create_and_register_new_plot_project(0,   32,  900,  668);

       if (pr == NULL)
       {
       free_one_plot(op);
       win_printf_OK("Could not find or allocte plot region!");
       }
       j = pr->n_op;
       add_data_to_pltreg (pr, IS_ONE_PLOT, (void *)op);
       if (j == 0) remove_data_from_pltreg (pr, IS_ONE_PLOT, (void *)pr->o_p[j]);


    */
    pr = find_pr_in_current_dialog(NULL);

    if (pr != NULL)
    {
        strcpy(fullfile, active_menu->text);
        fu = fullfile;
        return do_load();
    }

    imr = find_imr_in_current_dialog(NULL);

    if (imr != NULL)
    {
        strcpy(fullfilei, active_menu->text);
        fui = fullfilei;
        return do_load_im();
    }

    return D_O_K;
}

int reorder_visited_path_list(char *path)
{
    int i;
    int n, i_l, nn;
    char last_n[128] = {0},  cp_path[256] = {0};

    if (n_vi == 0)
    {
        n = get_config_int("GR-FILE-PATH", "last_visited_n", 0);
    }
    else
    {
        n = n_vi;
    }

    path_recently_visited[n].text = NULL;

    if (n_vi == 0)
    {
        int i_v = 0;
        for (i = 0, i_v = 0; i < n; i++)
        {
	  sprintf(last_n, "last_visited_%d", i);
	  const char *last_visited_item = get_config_string("GR-FILE-PATH", last_n, "");
	    if (last_visited_item != NULL && strlen(last_visited_item) > 2)
	      {
		strcpy(vi_path[i_v], last_visited_item);
		path_recently_visited[i_v].text = vi_path[i_v];
		path_recently_visited[i_v].proc = do_load_plot_from_recently_visited_path;

		if (path_is_accessible(vi_path[i_v]))
		  {
		    path_recently_visited[i_v].flags &= ~D_DISABLED;
		  }
		else
		  {
		    path_recently_visited[i_v].flags |= D_DISABLED;
		  }
		i_v++;
	      }
        }
	n = i_v;
    }
    for (i = 0, i_l = -1; i < n; i++)
    {
        if ((path != NULL) && (strcmp(vi_path[i], path) == 0))
        {
            i_l = i;
        }
    }

    if (path == NULL)
    {
        return 0;
    }

    strncpy(cp_path, path, 255);
    nn = (i_l == -1 && n < 16) ? n + 1 : n;
    set_config_int("GR-FILE-PATH", "last_visited_n", nn);
    n = nn;
    path_recently_visited[nn].text = NULL;
    nn = (i_l == -1) ? nn - 1 : i_l;

    for (i = nn; i > 0; i--)
    {
        strncpy(vi_path[i], vi_path[i - 1], 255);
        path_recently_visited[i].text = vi_path[i];
        path_recently_visited[i].proc = do_load_plot_from_recently_visited_path;

        if (path_is_accessible(vi_path[i]))
        {
            path_recently_visited[i].flags &= ~D_DISABLED;
        }
        else
        {
            path_recently_visited[i].flags |= D_DISABLED;
        }
    }

    strcpy(vi_path[0], cp_path);
    sprintf(last_n, "last_visited_0");
    path_recently_visited[0].text = vi_path[0];
    path_recently_visited[0].proc = do_load_plot_from_recently_visited_path;

    if (path_is_accessible(vi_path[0]))
    {
        path_recently_visited[0].flags &= ~D_DISABLED;
    }
    else
    {
        path_recently_visited[0].flags |= D_DISABLED;
    }

    for (i = 0; i < n; i++)
    {
        sprintf(last_n, "last_visited_%d", i);
        set_config_string("GR-FILE-PATH", last_n, vi_path[i]);
    }

    flush_config_file();
    return 0;
}




int do_quit(void)
{
    int i;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    i = win_printf("Do you really want to quit ? :'(");

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    flush_config_file();

    if (general_end_action)
    {
        general_end_action(NULL);
    }

    return D_CLOSE;
}


int do_load_recently_loaded_plot(void)
{
    pltreg *pr = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (active_menu->text == NULL || active_menu->dp == NULL)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        pr = create_and_register_new_plot_project(0,   32,  900,  668);
    }

    if (pr == NULL)
    {
        return 0;
    }

    load_plt_file_in_pltreg(pr, active_menu->text, (char *)active_menu->dp);
    reorder_loaded_plot_file_list(active_menu->text, (char *)active_menu->dp);
    return D_REDRAW;
}

int reorder_loaded_plot_file_list(char *last, char *path)
{
    int i;
    int n, i_l, nn;
    char last_n[128] = {0}, cp_last[256] = {0}, cp_path[256] = {0}; //, *f[32], *p[32];

    if (n_re == 0)
    {
        n = get_config_int("PLOT-GR-FILE", "last_loaded_n", 0);
    }
    else
    {
        n = n_re;
    }

    plot_recently_loaded[n].text = NULL;

    // win_printf("n loaded %d file %s",n,(last)?last:"Null");
    if (n_re == 0)
    {
        for (i = 0; i < n; i++)
        {
            sprintf(last_n, "last_loaded_%d", i);
            strcpy(re_file[i], get_config_string("PLOT-GR-FILE", last_n, ""));
            sprintf(last_n, "last_loaded_path_%d", i);
            strcpy(re_path[i], get_config_string("PLOT-GR-FILE", last_n, ""));
            plot_recently_loaded[i].text = re_file[i];
            plot_recently_loaded[i].dp = (void *)re_path[i];
            plot_recently_loaded[i].proc = do_load_recently_loaded_plot;

            if (path_is_accessible(re_path[i]))
            {
                plot_recently_loaded[i].flags &= ~D_DISABLED;
            }
            else
            {
                plot_recently_loaded[i].flags |= D_DISABLED;
            }

            //win_printf("menu %d at %s",i,plot_recently_loaded[i].text);
        }
    }

    for (i = 0, i_l = -1; i < n; i++)
    {
        if (last != NULL && path != NULL
                && (strcmp(re_file[i], last) == 0) && (strcmp(re_path[i], path) == 0))
        {
            i_l = i;
        }
    }

    if (last == NULL || path == NULL)
    {
        return 0;
    }

    strncpy(cp_last, last, 255);
    strncpy(cp_path, path, 255);
    //if (i_l >= 0)   win_printf("sel %d -> %s last %s",i_l,re_file[i_l],last);
    nn = (i_l == -1 && n < 16) ? n + 1 : n;
    set_config_int("PLOT-GR-FILE", "last_loaded_n", nn);
    n = nn;
    plot_recently_loaded[nn].text = NULL;
    nn = (i_l == -1) ? nn - 1 : i_l;

    //win_printf("last %s",last);
    for (i = nn; i > 0; i--)
    {
        //win_printf("setting %d replacing %s\nby %s\nlast %s",i,re_file[i],re_file[i-1],last);
        strncpy(re_file[i], re_file[i - 1], 255);
        //sprintf(last_n,"last_loaded_%d",i);
        //set_config_string("PLOT-GR-FILE",last_n,re_file[i]);
        strncpy(re_path[i], re_path[i - 1], 255);
        //sprintf(last_n,"last_loaded_path_%d",i);
        //set_config_string("PLOT-GR-FILE",last_n,re_path[i]);
        plot_recently_loaded[i].text = re_file[i];
        plot_recently_loaded[i].dp = (void *)re_path[i];
        plot_recently_loaded[i].proc = do_load_recently_loaded_plot;

        if (path_is_accessible(re_path[i]))
        {
            plot_recently_loaded[i].flags &= ~D_DISABLED;
        }
        else
        {
            plot_recently_loaded[i].flags |= D_DISABLED;
        }
    }

    //win_printf("last %s",cp_last);
    strcpy(re_file[0], cp_last);
    strcpy(re_path[0], cp_path);
    //sprintf(last_n,"last_loaded_0");
    //set_config_string("PLOT-GR-FILE",last_n,re_file[0]);
    //sprintf(last_n,"last_loaded_path_0");
    //set_config_string("PLOT-GR-FILE",last_n,re_path[0]);
    plot_recently_loaded[0].dp = (void *)re_path[0];
    sprintf(last_n, "last_loaded_0");
    plot_recently_loaded[0].text = re_file[0];
    //win_printf("i 0 last %s menu %s",last,plot_recently_loaded[0].text);
    plot_recently_loaded[0].proc = do_load_recently_loaded_plot;

    if (path_is_accessible(re_path[0]))
    {
        plot_recently_loaded[0].flags &= ~D_DISABLED;
    }
    else
    {
        plot_recently_loaded[0].flags |= D_DISABLED;
    }

    for (i = 0; i < n; i++)
    {
        sprintf(last_n, "last_loaded_%d", i);
        set_config_string("PLOT-GR-FILE", last_n, re_file[i]);
        sprintf(last_n, "last_loaded_path_%d", i);
        set_config_string("PLOT-GR-FILE", last_n, re_path[i]);
    }

    flush_config_file();
    return 0;
}

int do_load(void)
{
    int j;
    char path[512] = {0};
    char file[512] = {0};
    pltreg *pr = NULL;
    O_p *op;
    int erase_first_plot = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    file_list_t *file_list = open_files_config("Load plot (*.gr)", fu, "Plot Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0",
                             "PLOT-GR-FILE", "last_loaded");
    file_list_t *cursor = file_list;

    while (cursor != NULL)
    {
        set_config_string("PLOT-GR-FILE", "last_loaded", cursor->data);
        extract_file_name(file, sizeof(file), cursor->data);
        extract_file_path(path, sizeof(path), cursor->data);
        //printf("%s\n",cursor->data);
        //printf("%s\n",file);
        //printf("%s\n",path);
        op = create_plot_from_gr_file(file, path);

        if (op == NULL) win_printf_OK("Could not loaded %s\n from %s",
                                          cursor->data, backslash_to_slash(path));

        //else win_printf("loaded \n%s",backslash_to_slash(fullfile));
        pr = (pr == NULL) ? find_pr_in_current_dialog(NULL) : pr;

        if (pr == NULL)
        {
            pr = create_and_register_new_plot_project(0,   32,  900,  668);
            erase_first_plot = 1;
        }

        if (pr == NULL)
        {
            free_one_plot(op);
            return(win_printf_OK("Could not find or allocate plot region!"));
        }

        j = pr->n_op;
        add_data_to_pltreg(pr, IS_ONE_PLOT, (void *)op);

        if (j == 0)
        {
            remove_data_from_pltreg(pr, IS_ONE_PLOT, (void *)pr->o_p[j]);
        }

        if (erase_first_plot && (pr->n_op > 1))
        {
            remove_data_from_pltreg(pr, IS_ONE_PLOT, (void *)pr->o_p[0]);
        }

        switch_plot(pr, pr->n_op - 1);
        reorder_loaded_plot_file_list(file, path);
        reorder_visited_path_list(path);
        path[0] = '\0';
        refresh_plot(pr, pr->n_op - 1);
        cursor = cursor->next;
    }

    free_file_list(file_list);
    //switch_allegro_font(0);
    //broadcast_dialog_message(MSG_DRAW,0);
    //return 0;
    return D_REDRAW;
}

int do_load_in_new_project(void)
{
    char path[512] = {0};
    char fname[512] = {0};
    char *file = NULL;
    pltreg *pr = NULL;
    O_p *op = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    file = open_one_file_config("Load plot (*.gr) in a new project", fu,
                                "Plot Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0", "PLOT-GR-FILE", "last_loaded");

    if (file)
    {
        extract_file_path(path, sizeof(path), file);
	extract_file_name(fname, sizeof(fname), file);
        op = create_plot_from_gr_file(fname, path);

        //win_printf("%s",op);
        if (op == NULL) win_printf_OK("Could not loaded %s\n from %s",
                                          backslash_to_slash(fname), backslash_to_slash(path));

        pr = create_and_register_new_plot_project(0,   32,  900,  668);

        if (pr == NULL)
        {
            free_one_plot(op);
            win_printf_OK("Could not find or allocte plot region!");
        }

        add_data_to_pltreg(pr, IS_ONE_PLOT, (void *)op);
        remove_data_from_pltreg(pr, IS_ONE_PLOT, (void *)pr->o_p[0]);
        reorder_loaded_plot_file_list(file, path);
        reorder_visited_path_list(path);
        switch_plot(pr, pr->n_op - 1);
        free(file);
    }

    return D_REDRAW;
}





int do_save(void)
{
    char path[512] = {0};
    char file[512] = {0};
    char *fullfile1 = NULL;
    pltreg *pr = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        win_printf("cannot find Pr");
        return D_O_K;
    }
    if (pr->one_p->dir == NULL || sizeof(pr->one_p->dir) < 1)
      {
	if (strlen(first_data_dir) > 0)
	  pr->one_p->dir = strdup(first_data_dir);
      }


    fullfile1 = save_one_file_config("Save plot (*.gr) in ASCII format", pr->one_p->dir, pr->one_p->filename,
                                     "Plot Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0", "PLOT-GR-FILE", "last_saved");

    if (fullfile1)
    {
        extract_file_name(file, 256, fullfile1);
        extract_file_path(path, 512, fullfile1);
        save_one_plot(pr->one_p, fullfile1);
        set_op_filename(pr->one_p, "%s",file);
        set_op_path(pr->one_p, "%s", path);
        free(fullfile1);
    }

    broadcast_dialog_message(MSG_DRAW, 0);
    return D_O_K;
}




int do_save_plot_bitmap(void)
{
    //int i;
    //char path[512] = {0};
    char file[256] = "Untitled.bmp";
    char *fullfile1 = NULL;
    //const char *pa;
    pltreg *pr = NULL;
    BITMAP *bmp;
    PALETTE pal;
    DIALOG *di;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    switch_allegro_font(1);
    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return win_printf_OK("cannot find Pr");
    }

    di = find_dialog_associated_to_pr(pr, NULL);

    if (di == NULL)
    {
        return win_printf_OK("cannot find Dialog");
    }

    if (pr->one_p->filename)
    {
        strncpy(file, pr->one_p->filename, sizeof(file));
        replace_extension(file, file, "bmp", sizeof(file));
    }

    fullfile1 = save_one_file_config("Save plot region as a bitmap", pr->one_p->dir, file,
                                     "Bitmap Files (*.bmp)\0*.bmp;pcx;tga\0All Files (*.*)\0*.*\0\0", "PLOT-BMP-FILE", "last_saved");

    if (fullfile1)
    {
#ifdef XV_WIN32
        scare_mouse();
#endif
        vsync();
        get_palette(pal);
	for(;screen_acquired;); // we wait for screen_acquired back to 0;
	screen_acquired = 1;

        acquire_bitmap(screen);
        bmp = create_sub_bitmap(screen, di->x, di->y, di->w, di->h);
        release_bitmap(screen);
	screen_acquired = 0;
        save_bitmap(fullfile1, bmp, pal);
#ifdef XV_WIN32
        unscare_mouse();
#endif
        destroy_bitmap(bmp);
        free(fullfile1);
    }

    switch_allegro_font(0);
    broadcast_dialog_message(MSG_DRAW, 0);
    return D_O_K;
}


int do_save_bin(void)
{
    pltreg *pr = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)
    {
        return (win_printf_OK("cannot find plot region"));
    }

    if (pr->one_p == NULL)
    {
        return (win_printf_OK("cannot find plot"));
    }

    if (pr->one_p->dir == NULL || sizeof(pr->one_p->dir) < 1)
      {
	if (strlen(first_data_dir) > 0)
	  pr->one_p->dir = strdup(first_data_dir);
      }

    save_one_plot_data_bin(pr->one_p);
    broadcast_dialog_message(MSG_DRAW, 0);
    return D_O_K;
}

int save_one_plot_bin_auto(O_p *op)
{
  char fullfilel[2048] = {0};

  if (op == NULL) return 1;
  if ((op->filename == NULL) || (op->dir == NULL))
    return save_one_plot_data_bin(op);
  if (append_filename(fullfilel, op->dir, op->filename, sizeof(fullfile)) == NULL)
    return 2;
  return  save_one_plot_bin(op, fullfilel);
}



int save_one_plot_data_bin(O_p *op)
{
    char path[512] = {0};
    char file[256] = {0};
    char *fullfile1 = NULL;
    fullfile1 = save_one_file_config("Save plot (*.gr) in Bin format", op->dir, op->filename,
                                     "Plot Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0", "PLOT-GR-FILE", "last_saved");

    if (fullfile1)
    {
        extract_file_name(file, 256, fullfile1);
        extract_file_path(path, 512, fullfile1);
        save_one_plot_bin(op, fullfile1);
        set_op_filename(op, "%s", file);
        set_op_path(op, "%s", path);
        free(fullfile1);
    }

    return D_O_K;
}

# include"dev_def.h"

int spit_box_to_ps(pltreg *pr, char *file)
{
    struct box *bo = NULL;
    int x0, y0, i;
    static int col = 1, fg, fgk, paysage = 0, autos = 0;
    static float sca = 1;
    float xb1, yb1, xb0, yb0, sc; /* tmp */
    O_p *op = NULL;
    d_s *ds = NULL;
    //extern FILE *qd_fp;
    char question[512] = {0};
    //extern int  ps_init1(void);
    //extern int  pc_init1(void);
    //extern int  ps_start_bounding_box_2(char *file, char *infile, char *inpath,
    //                                    time_t *timer, float x0, float w, float y0, float h, float sc);
    //extern int  ps_close(void);

    /*  color_enable = 1;*/
    if (pr == NULL)
    {
        return 1;
    }

    bo = pr->stack;
    op = pr->one_p;
    ds = op->dat[0];
    x0 = bo->xc;
    y0 = bo->yc;
    /*
        if (bo->w > 9600 || (bo->d+bo->h) > 13200)
        {
        sc = (bo->w > 9600) ? ((float)(9600))/bo->w : 1;
        tmp = ((float)(13200))/(bo->d+bo->h);
        sc = (tmp < sc) ? tmp : sc;
        i = win_scanf("Plot bigger than the A4 page! scale it to ? %f",&sc);
        if (i == WIN_CANCEL)    return 0;
        sc *= .15;
        }
        */
    //col = color_enable;
    fgk = fg = fine_grid;
    snprintf(question, 512, "Using scale = 1, the plot width is %g mm \n"
             "and height %g mm\nChoose your scale %%f"
             "Or select automatic scale %%b\n"
             "Orientation: Portrait %%R Landscape %%r\n"
             "Type of EPS: Black and White %%R Color %%r Gray level%%r\n"
             "Fine grid: Off %%R On %%r\n",
             (25.4 * bo->w) / 1200, (25.4 * (bo->d + bo->h)) / 1200);
    i = win_scanf(question, &sca, &autos, &paysage, &col, &fg);

    if (i == WIN_CANCEL)
    {
        return 0;
    }

    if (fg != fine_grid)
    {
        fine_grid = fg;
        pr->one_p->need_to_refresh = 1;
        refresh_plot(pr, UNCHANGED);
    }

    if (autos)
    {
        if (paysage)
        {
            sca = ((float)bo->w / 1200) / 11;

            if (((float)(bo->d + bo->h) / 1200) / 8 < sca)
            {
                sca = ((float)(bo->d + bo->h) / 1200) / 8;
            }
        }
        else
        {
            sca = ((float)bo->w / 1200) / 8;

            if (((float)(bo->d + bo->h) / 1200) / 11 < sca)
            {
                sca = ((float)(bo->d + bo->h) / 1200) / 11;
            }
        }

        sc = sca * 0.15;
        xb1 = sc * ((float)(2 * bo->w)) / 5;
        yb1 = sc * ((float)(2 * (bo->d + bo->h))) / 5;
        xb0 = (8 * 72.72 - xb1) / 2;
        yb0 = (11 * 72.72 - yb1) / 2;
    }
    else
    {
        sc = sca * 0.15;
        xb1 = sc * ((float)(2 * bo->w)) / 5;
        yb1 = sc * ((float)(2 * (bo->d + bo->h))) / 5;
        xb0 = (8 * 72.72 - xb1) / 2;
        yb0 = (11 * 72.72 - yb1) / 2;
    }

    //  ps_init1();

    if (paysage)
    {
        if (ps_start_bounding_box_2(file, (op->filename != NULL) ? op->filename : strdup("NULL"),
                                    (op->dir != NULL) ? op->dir : strdup("NULL"), &(ds->time) , (8 * 72.72 - yb1) / 2,
                                    yb1 + 5, (11 * 72.72 - xb1) / 2, xb1 + 5, sc) == 1)
        {
            return win_printf_OK("Error cannot open %s", file);
        }

        fprintf(qd_fp, "%d 0 translate\n90 rotate\n", (int)((yb1 + 5.5) / sc));
    }
    else
    {
        if (ps_start_bounding_box_2(file, (op->filename != NULL) ? op->filename : strdup("NULL"),
                                    (op->dir != NULL) ? op->dir : strdup("NULL"), &(ds->time) , xb0, xb1 + 5, yb0, yb1 + 5, sc) == 1)
        {
            return win_printf_OK("Error cannot open %s", file);
        }
    }

    bo->xc = 0;
    bo->yc = 0;
    ps_init1();

    /*  set_cursor_glass();*/
    if (col == 1)
    {
        ps_color(Black, TRUE_COLOR_WHITE_TO_BLACK);
        color_enable = 1;
    }
    else if (col == 2)
    {
        ps_color(Black, GRAY_WHITE_TO_BLACK);
        color_enable = 1;
    }
    else
    {
        ps_color(Black, BW_WHITE_TO_BLACK);
        color_enable = 0;
    }

    wr_box(bo);
    ps_close();
    bo->xc = x0;
    bo->yc = y0;
    /*  reset_cursor();*/
    pc_init1();

    if (q_dev.draw_char != NULL)
    {
        win_printf("Draw char not Null");
    }

    if (fgk != fine_grid)
    {
        fine_grid = fgk;
        pr->one_p->need_to_refresh = 1;
        refresh_plot(pr, UNCHANGED);
    }

    return D_O_K;
}



int print2psfile(void)
{
    int i;
    char path[512] = {0};
    char file[512] = {0};
    char *fullfilel = NULL;
    pltreg *pr = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL || pr->one_p == NULL)
    {
        win_printf("cannot find Pr");
        return D_O_K;
    }

    if (pr->one_p->dir == NULL || sizeof(pr->one_p->dir) < 1)
      {
          if (strlen(first_data_dir) > 0)
              pr->one_p->dir = strdup(first_data_dir);
      }


    snprintf(file,sizeof(file),"Untiteled.eps");
    if (pr->one_p->filename)
    {
        strncpy(file, pr->one_p->filename, sizeof(file));
        replace_extension(file, file, "eps", sizeof(file));
    }

    eps_file_flag = 1;
    color_enable = 1;
    fullfilel = save_one_file_config("Save plot region as a eps", pr->one_p->dir, file,
                                    "PostScript Files (*.eps)\0*.eps\0All Files (*.*)\0*.*\0\0", "PLOT-EPS-FILE", "last_saved");

    if (fullfilel)
    {
        extract_file_name(file, 256, fullfilel);
        replace_extension(file, file, "eps", sizeof(file));
        extract_file_path(path, 512, fullfilel);
        i = spit_box_to_ps(pr, fullfilel);

        if (i != D_O_K)
        {
            win_printf("Cannot open\n%s !...", backslash_to_slash(fullfilel));
        }
    }

    eps_file_flag = 0;
    color_enable = 0;
    return D_O_K;
}

int create_new_plot(void)
{
    int i;
    static int new_pr = 0, nf = 16, grab = 1, new_grab_opds = 2;
    static char title[1024] = {0}, xtitle[1024]  = {0}, ytitle[1024]  = {0}, src[1024] = {0};
    pltreg *pr = NULL;
    static O_p *op = NULL;
    d_s *ds = NULL;

    if (updating_menu_state != 0)
    {
        return 0;
    }

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
    {
        return OFF;
    }

    if (op_grabbing != NULL)
    {
        i = win_scanf("Do you want to: %RCreate a new plot not for grabbing purpose\n"
                      "or %r a new grabbing plot\n"
                      "or %r new dataset in the grabbing plot\n"
                      "or %r to stop the grabbing process in this plot\n"
                      , &new_grab_opds);

        if (i == WIN_CANCEL)
        {
            return 0;
        }

        if (new_grab_opds > 2)
        {
            ds_grabbing = NULL;
            op_grabbing = NULL;
            return 0;
        }
        else if (new_grab_opds < 2)
        {
            i = win_scanf("Do you want to create a new plot not\n"
                          "In the curent projet %R or in a new one %r\n"
                          "Title %lsTitle in X%ls title in Y%ls\n"
                          "Source of the data set %ls"
                          , &new_pr, title, xtitle, ytitle, src, &grab);

            if (i == WIN_CANCEL)
            {
                return 0;
            }

            grab = 1;
        }
        else if (new_grab_opds == 2)
        {
            i = win_scanf("Specify the Source of the data set %ls", src);

            if (i == WIN_CANCEL)
            {
                return 0;
            }

            ds_grabbing = create_and_attach_one_ds(op_grabbing, nf, nf, 0);

            if (ds_grabbing == NULL)
            {
                return win_printf_OK("could not create ds");
            }

            ds_grabbing->nx = ds_grabbing->ny = 0;

            if (strlen(src) > 0)
            {
	      set_ds_source(ds_grabbing, "%s", src);
            }

            return 0;
        }
    }
    else
    {
        i = win_scanf("Creating a new plot in the curent projet %R\n"
                      "or %r a new plot in a new plot region\n"
                      "or %r adding a new data set to the current plot\n"
                      "Title %lsTitle in X%ls title in Y%ls"
                      "Source of the data set %ls"
                      "Make this plot a grabbing plot %b\n",
                      &new_pr, title, xtitle, ytitle, src, &grab);

        if (i == WIN_CANCEL)
        {
            return 0;
        }

        if (op == NULL && new_pr == 2)
        {
            return win_printf_OK("could not create ds not plot defined");
        }
        else if (op != NULL && new_pr == 2)
        {
            ds = create_and_attach_one_ds(op_grabbing, nf, nf, 0);

            if (ds_grabbing == NULL)
            {
                return win_printf_OK("could not create ds");
            }

            ds_grabbing->nx = ds_grabbing->ny = 0;

            if (strlen(src) > 0)
            {
	      set_ds_source(ds_grabbing, "%s", src);
            }

            return 0;
        }
    }

    if (new_pr)
    {
        pr = create_pltreg_with_op(&op, nf, nf, 0);

        if (pr == NULL || op == NULL)
        {
            return win_printf_OK("could not create plot region");
        }
    }
    else
    {
        op = create_and_attach_one_plot(pr, nf, nf, 0);
    }

    if (op == NULL)
    {
        return win_printf_OK("could not create plot");
    }

    ds = op->dat[0];
    ds->nx = ds->ny = 0;

    if (grab)
    {
        ds_grabbing = ds;
        op_grabbing = op;
    }

    if (strlen(title) > 0)
    {
      set_plot_title(op, "%s", title);
    }

    if (strlen(xtitle) > 0)
    {
      set_plot_x_title(op, "%s",xtitle);
    }

    if (strlen(ytitle) > 0)
    {
      set_plot_y_title(op, "%s", ytitle);
    }

    if (strlen(src) > 0)
    {
      set_ds_source(ds_grabbing, "%s", src);
    }

    return refresh_plot(pr, pr->n_op - 1);
}


int create_bug_plot(void)
{
    static int nf = 1024;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;

    if (updating_menu_state != 0)
    {
        return 0;
    }

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
    {
        return OFF;
    }

    op = create_and_attach_one_plot(pr, nf, nf, 0);

    if (op == NULL)
    {
        return win_printf_OK("could not create plot");
    }

    ds = op->dat[0];
    /*
       op->x_lo = -0.01;
       op->y_lo = -0.01;
       op->x_hi = 0.01;
       op->y_hi = 0.01;
       */
    //  ds->xd[1] = ds->yd[1] = 0.1;
    ds->nx = ds->ny = 2;
    return refresh_plot(pr, pr->n_op - 1);
}

int do_sleep(void)
{
    BITMAP *mask = NULL;
    BITMAP *bmp_old = NULL;
    BITMAP *paused_message = NULL;
    BITMAP *scr = NULL;
    int screen_hh, screen_ww;
    char    message_filename[255] = {0};

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    scr = screen;
    //  scr = the_dialog[2].dp;
    screen_hh = scr->h;
    screen_ww = scr->w;
    //  win_printf("w = %d, h = %d", screen_ww, screen_hh);
    sprintf(message_filename, "%spaused.pcx", prog_dir);
    paused_message = load_bitmap(message_filename, default_palette);
    mask    = create_bitmap(screen_ww, screen_hh);
    bmp_old = create_bitmap(screen_ww, screen_hh);
#ifdef XV_WIN32
    scare_mouse();
#endif
    for(;screen_acquired;); // we wait for screen_acquired back to 0;
    screen_acquired = 1;
    acquire_bitmap(screen);
    blit(scr, bmp_old, 0, 0, 0, 0, screen_ww, screen_hh);
    floodfill(mask, 1, 1, Lightgray);
    set_trans_blender(0, 0, 0, 150); // nnn/255 : mask thickness
    draw_trans_sprite(scr, mask, 0, 0);
    blit(paused_message, scr, 0, 0, screen_ww / 2 - 200, screen_hh / 2 - 100 , 400,
         200); // 400x200 is the message bitmap size
    release_bitmap(screen);
    screen_acquired = 0;
#ifdef XV_WIN32
    unscare_mouse();
#endif
    //  object_message(&the_dialog[2], MSG_DRAW, 0);
    //  broadcast_dialog_message(MSG_DRAW,0);
    readkey(); // this pauses the system, for real!
#ifdef XV_WIN32
    scare_mouse();
#endif
    for(;screen_acquired;); // we wait for screen_acquired back to 0;
    screen_acquired = 1;
    acquire_bitmap(screen);
    blit(bmp_old, scr, 0, 0, 0, 0, screen_ww, screen_hh);
    release_bitmap(screen);
    screen_acquired = 0;
#ifdef XV_WIN32
    unscare_mouse();
#endif
    destroy_bitmap(mask);
    destroy_bitmap(bmp_old);
    destroy_bitmap(paused_message);
    return (D_O_K);
}

int do_dump_plot_in_csv_file(void)
{
  int i, j;
  O_p *op = NULL;
  int nf, len;
  d_s  *dsi;
  pltreg *pr = NULL;
  char question[2048] = {0}, titlex[64] = {0}, titley[64] = {0}, unit[64] = {0}, filename[512] = {0}, file[512] = {0};
  FILE *fp = NULL;
  static int decicoma = 0, allds = 0;


  if(updating_menu_state != 0)	return D_O_K;

  /* display routine action if SHIFT is pressed */
  if (key[KEY_LSHIFT])
  {
    return win_printf_OK("This routine multiply the y coordinate"
  "of a data set by a number");
  }
  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return win_printf_OK("cannot find data");
  nf = dsi->nx;	/* this is the number of points in the data set */

  if (op->x_unit != NULL)
      snprintf(unit,sizeof(unit),"(%s)",op->x_unit);
  else unit[0] = 0;
  snprintf(titlex,sizeof(titlex),"DataSet%d-X%s",op->cur_dat,unit);
  if (op->y_unit != NULL)
      snprintf(unit,sizeof(unit),"(%s)",op->y_unit);
  else unit[0] = 0;
  snprintf(titley,sizeof(titley),"DataSet%d-Y%s",op->cur_dat,unit);
  snprintf(question,sizeof(question)
           ,"I am going to write this plot data in a csv file\n"
           "Your can either %%R->convert only the selected dataset %d\n"
           "or %%r->convert all datasets\n"
           //"X column title:\n%%32s\n"
           //"y column title:\n%%32s\n"
           "%%b->replace decimal point by coma (Fr)",op->cur_dat);
  i = win_scanf(question,&allds,&decicoma);
  if (i == WIN_CANCEL)	return OFF;
  for (j = ((op->filename) ? strlen(op->filename):0) - 1; j > 0 &&  op->filename[j] != '.'; j--);
  for(i = 0; i < j && i < (int)sizeof(file); i++)
      file[i] = op->filename[i];
  file[i] = 0;
  if (allds == 0)
      snprintf(filename,sizeof(filename),"%s-DS%d",file,op->cur_dat);
  else snprintf(filename,sizeof(filename),"%s-DS[0-%d[",file,op->n_dat);
  build_full_file_name(filename,sizeof(filename), op->dir, filename);
  replace_extension(filename, filename, "csv", sizeof(filename));
  if (do_select_file(filename, sizeof(filename), "Save csv-FILE", "csv", "Save csv", 0))
    return win_printf_OK("Cannot select ouput file");

  fp = fopen(filename, "wb");
  if (fp == NULL)
      {
          win_printf("Cannot open file:\n%s", backslash_to_slash(filename));
          return 0;
      }
  int nfm = 0, sel = op->cur_dat, k;
  for (i = 0; i < op->n_dat; i++)
      {
          j = op->dat[i]->nx;
          j = (j < op->dat[i]->ny) ? j : op->dat[i]->ny;
          nfm = (j < nfm) ? nfm : j;
      }
  nfm = (allds) ? nfm : nf;
  for (i = 0, k = 0; i < op->n_dat; i++)
      {
          if (allds == 0 && i != sel) continue;
          if (op->x_unit != NULL)
              snprintf(unit,sizeof(unit),"(%s)",op->x_unit);
          else unit[0] = 0;
          snprintf(titlex,sizeof(titlex),"DS%d-X%s",i,unit);
          if (op->y_unit != NULL)
              snprintf(unit,sizeof(unit),"(%s)",op->y_unit);
          else unit[0] = 0;
          snprintf(titley,sizeof(titley),"DS%d-Y%s",i,unit);
          if (k == 0)       fprintf(fp,"%s;%s",titlex,titley);
          else        fprintf(fp,";%s;%s",titlex,titley);


          if(op->dat[i]->xe)
          {
              if (op->x_unit != NULL)
              snprintf(unit,sizeof(unit),"(%s)",op->x_unit);
          else unit[0] = 0;
          snprintf(titlex,sizeof(titlex),"DS%d-XError%s",i,unit);
          fprintf(fp,";%s",titlex);
        }

          if(op->dat[i]->ye)
          {
              if (op->y_unit != NULL)
              snprintf(unit,sizeof(unit),"(%s)",op->y_unit);
          else unit[0] = 0;
          snprintf(titley,sizeof(titley),"DS%d-YError%s",i,unit);
          fprintf(fp,";%s",titley);
        }

          k++;
      }
  fprintf(fp,"\n");
  float tmpx, tmpy;
  int id;
  for (j = 0; j < nfm; j++)
      {
          for (id = 0, k = 0; id < op->n_dat; id++)
              {
                  if (allds == 0 && id != sel) continue;
                  dsi = op->dat[id];
                  if (j < op->dat[id]->nx)
                      {
                          tmpx = op->ax+op->dx*dsi->xd[j];
                          tmpy = op->ay+op->dy*dsi->yd[j];
                          snprintf(question,sizeof(question),"%s%g;%g",(k)?";":"",tmpx,tmpy);
                          len = strlen(question);
                          for (i = 0;decicoma > 0 && i < len; i++)
                              question[i] = (question[i] == '.') ? ',' : question[i];
                          fprintf(fp,"%s",question);

                          if (op->dat[id]->xe)
                          {
                          tmpx = op->dx*dsi->xe[j];
                          snprintf(question,sizeof(question),"%s%g",";",tmpx);
                          len = strlen(question);
                          for (i = 0;decicoma > 0 && i < len; i++)
                              question[i] = (question[i] == '.') ? ',' : question[i];
                          fprintf(fp,"%s",question);
                        }

                        if (op->dat[id]->ye)
                        {
                        tmpx = op->dy*dsi->ye[j];
                        snprintf(question,sizeof(question),"%s%g",";",tmpx);
                        len = strlen(question);
                        for (i = 0;decicoma > 0 && i < len; i++)
                            question[i] = (question[i] == '.') ? ',' : question[i];
                        fprintf(fp,"%s",question);
                      }

                      }
                  else
                  {
                      fprintf(fp,"%s;",(k)?";":"");
                      if (op->dat[id]->ye)    fprintf(fp,";");
                      if (op->dat[id]->xe)    fprintf(fp,";");
                    }

                  k++;
              }
          fprintf(fp,"\n");
      }
  fclose(fp);
  /* refisplay the entire plot */
  refresh_plot(pr, UNCHANGED);
  return D_O_K;
}


/**************************************************************************************************/
MENU plot_file_menu[32] =
{
    { NULL,                          NULL,             NULL,       0, NULL  }
};
/**************************************************************************************************/
MENU plot_file_import_menu[32] =
{
    { NULL,                          NULL,             NULL,       0, NULL  }
};
/**************************************************************************************************/
MENU plot_file_export_menu[32] =
{
    { NULL,                          NULL,             NULL,       0, NULL  }
};


/**************************************************************************************************/


MENU plot_file_load_image_menu[32] =
{
    {"Load Gr image",       do_load_im, NULL,   0, NULL},
    {"Recently Loaded images",          NULL,   im_recently_loaded, 0, NULL},
    {"Import image" ,       NULL,   image_file_import_menu,     0, NULL}
};




int init_plot_file_menu(void)
{
    static MENU *mn = NULL;
    char mess[64] = {0};

    if (mn != NULL)
    {
        return 1;
    }

    reorder_loaded_plot_file_list(NULL, NULL);
    reorder_loaded_image_file_list(NULL, NULL);
    reorder_visited_path_list(NULL);
    add_item_to_menu(plot_file_menu, "New plot",     create_new_plot,        NULL,   0, NULL);
    //    add_item_to_menu(plot_file_menu, "bug plot",     create_bug_plot,        NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "\0",                       NULL,           NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "&Load Gr plot\tCtrl-L",        do_load,        NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "Recently Loaded ",           NULL,                 plot_recently_loaded, 0, NULL);
    add_item_to_menu(plot_file_menu, "Recently visited ",              NULL,                 path_recently_visited, 0,
                     NULL);
    add_item_to_menu(plot_file_menu, "L&oad Gr plot in new project\tCtrl-O", do_load_in_new_project, NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "\0",                       NULL,           NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "&Save plot (binary)\tCtrl-S",  do_save_bin,        NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "Save plot (&Ascii)\tCtrl-A",   do_save,        NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "Save plot in CSV file",   do_dump_plot_in_csv_file,        NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "\0",                       NULL,           NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "Load &Cgr project",    load_cgr_multi_plots,   NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "Save Cgr project",     do_save_all_plots_from_project, NULL, 0, NULL);
    add_item_to_menu(plot_file_menu, "\0",                       NULL,       NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "Load image",               NULL,       plot_file_load_image_menu,  0, NULL);
    add_item_to_menu(plot_file_menu, "\0",                       NULL,       NULL,   0, NULL);
    add_item_to_menu(plot_file_import_menu, ".gr plot",          do_load,        NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "Import" ,                      NULL,       plot_file_import_menu,  0, NULL);
    add_item_to_menu(plot_file_export_menu, ".gr plot (binary)",     do_save_bin,    NULL,   0, NULL);
    add_item_to_menu(plot_file_export_menu, ".gr plot (ascii)",      do_save,        NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "Export" ,                      NULL,       plot_file_export_menu,  0, NULL);
    add_item_to_menu(plot_file_menu, "\0",                       NULL,           NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "Print &Eps\tCtrl-E",           print2psfile,       NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "Dump bitmap",              do_save_plot_bitmap, NULL,  0, NULL);
# if defined(BUILDING_DLL) //&& !defined(XVIN_STATICLINK)
    add_item_to_menu(plot_file_menu, "\0",                       NULL,           NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "Load plug'in",             run_module,     NULL,   0, NULL);
    add_item_to_menu(plot_file_menu, "Unload plug'in",               remove_module,      NULL,   0, NULL);
#if defined(XV_GTK_DIALOG) && !defined(XV_WIN32)
    add_item_to_menu(plot_file_menu, "List plugins",             list_plugin,        NULL,   0, NULL);
#endif
    add_item_to_menu(plot_file_menu, "\0",                       NULL,           NULL,   0, NULL);
# endif
    //  add_item_to_menu(plot_file_menu,"&Sleep\tCtrl-X",           do_sleep,       NULL,   0, NULL);
    sprintf(mess, "Quit\t%c", (char)27);
    add_item_to_menu(plot_file_menu, strdup(mess),               do_quit,        NULL,   0, NULL);
    return 0;
}
# endif
