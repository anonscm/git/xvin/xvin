#pragma once
/********************************************************************/
/* p_treat_math.h												*/
/* mathematical data treatments on dataset(s)						*/
/********************************************************************/
#include "plot_ds.h"
#include "plot_op.h"
// definitions for operations on 1 dataset: above D_USER = 256
#define DS_SQRT          0x0600 /* 2058 */
#define DS_SQR           0x1600
#define DS_INVERSE       0x2600
#define DS_ABS           0x3600
#define DS_ATAN          0x4600
#define DS_ATAN2         0x8600
#define DS_UNWRAP        0x5600
#define DS_TANH          0x6600

#define DS_EXPAND        0x0700 /* 2059 */
#define DS_EXPO          0x2900 /* 2060 */ /* colision with Y_AXIS */
#define DS_EXPO_10       0x3900 /* 2060 */ /* colision with Y_AXIS */
#define DS_EXPO_2        0x4900 /* 2060 */ /* colision with Y_AXIS */
#define DS_LOG           0x1900
#define DS_LOG10         0x1A00
#define DS_LOG2          0x2A00
#define DS_EXTRACT_X_T   0x0B00 /* 2063 */
#define DS_EXTRACT_Y_T   0x0C00 /* 2064 */

// definitions for operations on 2 datasets: above D_USER = 256
#define ADD        0x0200 /* 2054 */
#define SUBSTRACT  0x0300 /* 2055 */
#define MULTIPLY   0x0400 /* 2056 */
#define DIVIDE     0x0500 /* 2057 */
#define Y_OF_X_YY  0x0D00 /* 2065 */
#define DS_N_PTS   0x0E00
#define DS_MEAN_XY 0x0F00

// definitions for windowing or not: (used as a menu option for now)
#define WINDOWING_NO     	0x0000
#define WINDOWING_YES		0x1000



int	opp_on_one_ds(void);
			// basic operations on 1 dataset
int	opp_on_two_ds(void);			// basic operations on 2 datasets
int	opp_on_all_ds(void);			// basic operations on all datasets
int do_add_all_ds(void);            // addition of all datasets, and correct construction of error bars
int do_add_on_2_ds(void);
int do_average_all_ds(void);

int do_true_y_derivative(void);			// computes derivative
int do_integrate_ds(void);			// computes integral
int do_true_integrate_ds(void);			// computes integral, taking into account irregularly spaced points
int mean_ds(void);					// computes the mean value of a dataset
int ds_means(void);					// computes the mean value of a dataset, idem on visible points
d_s *find_peak(d_s *src, int mode, int nstart, int nend, int pwidth, float thres, d_s *peaks);
int find_peaks(char ch);
int find_zero_around(float *x, int nx, int nzer, float *Zero_pos,  float *Zero_deriv);

int do_fir_filtering(void);			// sliding average, moved from interpol.c
int do_least_square_fit_on_ds(void);
int  sub_on_2_ds_by_index(void);
int rescale_ds_according_lo_hi(void);
XV_FUNC(int, do_build_histo_with_exponential_convolution,(void));
XV_FUNC(int, adjust_ds_mean_y_according_to_visible_points,(void));
int x_or_y_range_of_all_ds_same_op(void);
int x_or_y_range_of_visible_pts_all_ds_same_op(void);
int slidding_histogramm_ds_same_op(void);
int mean_visible_pts_all_ds(void);
int do_pick_points_having_same_x_on_add_all_ds(void);
int  substract_on_ds_to_all_other_by_index(void);
int adjust_ds_mean_y_for_sparse_signal(void);
int do_non_linear_plateau_filtering(void);
int do_erase_all_from_one_ds(void);
int follow_slow_correlated_drift_in_all_ds(void);
int do_build_histo_with_exponential_convolution_norm_for_each_ds(void);
int y1_y2_same_x(void);
int do_x_of_t_versus_index(void);
int do_y_of_t_versus_index(void);
int do_x_of_t_versus_index_all(void);
int do_y_of_t_versus_index_all(void);
int do_y_differentiate(void);
int do_wrap_y_in_x_using_one_ds(void);
int find_max_of_distribution_by_histo(d_s *dst, O_p *ops, float biny, float *Max);
int do_true_y_avg_derivative(void);
int compute_barycenter_of_visible_points(void);
d_s *true_y_avg_derivative_of_ds_in_op(O_p *op, int ids, int idx);
XV_FUNC(int, find_peaks_in_Y_of_equally_spaced_ds_all_ds,(void));
int do_gaussian_convolution(void);
int average_all_ds_of_op_by_index_by_histo_2(void);
d_s *average_all_ds_of_op_by_max_of_bounded_histo(O_p *ops, float biny, int multi_peak, float thres, float y_min, float y_max);
int draw_running_variance(void);
int compute_running_variance(d_s *dsi,d_s *dso, int window, int shift);
int distance_between_datasets(void);

int do_filter_nl_ex_slope_auto_r_math(void);
int do_dump_sigma_HF_of_all_ds(void);
int do_dump_std_of_all_ds(void);
