/************************************************************************/
/* plot_treat_fftw.h							*/
/* mathematical data treatments on dataset(s), using fftw v3.0.1	*/
/************************************************************************/

// definitions for computing the autocorrelation of a dataset, or the cross-correlation of 2 datasets:
#define ONE_DS		0x0100 	/* work on one dataset: autocorrelation				   */
#define TWO_DS		0x0200 	/* work on two datasets: cross-correlation			   */

int do_spectrum(void); 			// computes the power spectrum of a dataset using fftw
int do_psd(void); 				// computes the psd of a dataset using fftw
int do_correlation(void);		// computes the cross-correlation (or auto-correlation) using fftw
int do_filter_ds(void);			// filters a dataset using fftw
int do_Hilbert_demodulation(void);	 // filters a dataset using fftw to demodule it.
int do_display_current_filter(void); // display current filter (if it exists) as a new plot
int do_display_Hamming(void);		 // display Hamming window
int do_one_mode(void);                 // Project on One Fourier mode




