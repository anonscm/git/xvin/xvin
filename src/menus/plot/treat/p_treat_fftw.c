/********************************************************************/
/* plot_treat_fftw.c												*/
/* mathematical data treatments on dataset(s), using fftw v3.0.1	*/
/********************************************************************/


#include "platform.h"
#include "xvin.h"
#include "p_treat_basic.h" // for definition of "DS_ONE and DS_ALL"
#include "p_treat_fftw.h"

#include "fft_filter_lib.h"	// mathematical functions operating on filters (no front-end)
#include "fftw3_lib.h"		// (fftw3 is also included from here), mathematical functions (no front-end)
#include "xv_tools_lib.h"

#include <fftw3.h>		// include the fftw v3.0.1 or v3.1.1 library for computing spectra:
#include <gsl/gsl_statistics.h>
# include <stdlib.h>
# include <string.h>



/************************************************************************/
/* computes the poser spectral density, 								*/
/* with more options then the regular do_spectrum        				*/
/************************************************************************/
int do_psd(void)
{
    int i, j;
    pltreg *pr = NULL;         // plot region
    O_p *op = NULL, *opd = NULL;      // initial and destination plots
    d_s *dsi = NULL, *dsd = NULL;     // initial and destination datasets
    int     nx, n2=1;
    char message[512] = {0};
    static	float	f_acq=-1.0, percent_overlap=0.8;
    static	int		bool_remember_values=1, bool_overlap_type=1, bool_hanning=1, n_fft, n_overlap;
    int	     index, n_ds=1, k_ds, *ds_index=NULL;

	if (updating_menu_state != 0)	return D_O_K;
	index = (active_menu->flags & 0xFFFFFF00);
	if ( !( (index & PSD_ENERGY) || (index & PSD_AMP) ) ) return D_O_K;
	if ( !( (index & DS_ONE) || (index & DS_ALL) ) ) return D_O_K;

    if (key[KEY_LSHIFT])   return win_printf_OK("This routine computes the Power Spectrum Density (PSD)\n"
        		"of the selected dataset\nA power of 2 is not required (but is faster)\n"
				"A new plot is created (log-log display)");

   	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return(win_printf_OK("Could not find plot data"));

    nx = dsi->nx; if (nx<2) return(win_printf_OK("not enough points in dataset!"));
    for (n2 = 1; n2 < nx; n2 <<= 1);
    n2>>=1;	// n2 is the closest power of 2 lower than or equal to nx

    if (index & DS_ONE) // PSD of all datasets of plot
    {  n_ds = 1;
       ds_index = (int*)calloc(1,sizeof(int));
       ds_index[0] = op->cur_dat;
    }
    if (index & DS_ALL) // PSD of all datasets of plot
    {  n_ds = op->n_dat;
       ds_index = (int*)calloc(op->n_dat, sizeof(int));
       for (i=0; i<op->n_dat; i++) ds_index[i]=i;
    }
    if ( (ds_index==NULL) || (n_ds<1) )	return(win_printf_OK("something wrong with datasets indices !"));

	if ( (bool_remember_values==0) || (f_acq<=0) )
	{	n_fft     = (n2>8192) ? 4096 : n2;
		percent_overlap=0.8;
		f_acq = find_sampling_frequency(dsi->xd, nx);
	}
	if (bool_overlap_type==1)	n_overlap = (int)((float)100.*percent_overlap);
	else 						n_overlap = (int)((float)n_fft*percent_overlap);
	sprintf(message, "{\\pt14\\color{yellow}Power Spectral Density (PSD)}\n\n"
			"dataset has %d points. lower or equal power of 2 is %d\n"
			"- number of points of fft window : %%8d\n"
			"- overlap between windows       : %%8d\n"
			"     overlap in %%R points or %%r in percents\n"
			"- acquisition frequency (in Hz) : %%8f\n"
			"     (for normalization)\n\n"
			"%%b use Hanning window ?\n"
			"%%b remember values",nx, n2);
	if (win_scanf(message,&n_fft, &n_overlap, &bool_overlap_type, &f_acq, &bool_hanning, &bool_remember_values) == WIN_CANCEL)
		return D_O_K;
	percent_overlap = (float)n_overlap/((float)(100.));
	if (bool_overlap_type==1)	n_overlap       =(int)(percent_overlap*(float)n_fft);
	else						percent_overlap =(float)n_overlap/(float)n_fft;

	if ( (n_overlap>=n_fft) || (percent_overlap>=1.) )
	return(win_printf_OK("overlap is too large compared to n-fft\n"
					"in points    : %d>=%d\n"
					"in percents : %d>=100", n_overlap, n_fft, (int)((float)100.*percent_overlap)));

	for (j=0; j<n_ds; j++)
	{	k_ds = ds_index[j];

        if (j==0)
        {  opd = create_and_attach_one_plot(pr, (n_fft/2)+1, (n_fft/2)+1, 0);
           if (opd == NULL)        return win_printf_OK("Cannot allocate plot");
           dsd = opd->dat[0];
        }
        else dsd = create_and_attach_one_ds(opd, (n_fft/2)+1, (n_fft/2)+1, 0);
        if (dsd == NULL)    	return win_printf_OK("Cannot allocate dataset");

	    i=compute_psd_of_float_data(op->dat[k_ds]->yd, dsd->yd, dsd->xd, f_acq, op->dat[k_ds]->nx, n_fft, n_overlap, bool_hanning);
	    if (i==0) return win_printf_OK("nothing to do with your values !");

	    if (index & PSD_AMP)		// here, user asked for a psd in amplitude units instead of energy units
	    {   for (i=0 ; i<= n_fft/2 ; i++) dsd->yd[i] = (float)sqrt(dsd->yd[i]);
    	}

    	inherit_from_ds_to_ds(dsd, op->dat[k_ds]);
    	set_formated_string(&dsd->treatement,"P.S.D. (%s) over %d points, f_{acq}=%gHz, %s",
			(index & PSD_AMP) ? "amplitude" : "energy", n_fft, f_acq,
			(bool_hanning==1) ? "Hanning window" : "no windowing");


//	uns_op_2_op(opd, op);					// unitsets are inherited, but not correctly translated (t -> 1/t...)
/*	if (op->n_xu>1) // to do: inherit correctly
	for (i=1; i<op->n_xu; i++)
	{	opd->xu[i] =
	}
	win_printf("destination plot has %d x unitsets\nm=% c=%d cp=%d",opd->n_xu, opd->m_xu, opd->c_xu, opd->c_xu_p);
*/

//    	opd->un = build_unit_set(IS_RAW_U, 0 /* shift */, 1 /* multiplier */, 0, 0, "" /* e.g. : "V"*/ );


	}

	opd->iopt |= XLOG;
    opd->iopt |= YLOG;

    opd->filename = Transfer_filename(op->filename);
    opd->dir = Mystrdup(op->dir);
	set_plot_title(opd, "psd");
    set_plot_x_title(opd, "frequency (Hz)");
	if (index & PSD_AMP) set_plot_y_title(opd, "V/\\sqrt{Hz}");
	else		 		set_plot_y_title(opd, "V^2/Hz");

    free(ds_index);
 	return refresh_plot(pr, UNCHANGED);
}
/* end of do_dsp */



/************************************************************************/
/* project the y component on one Fourier mode (cosine and sine) */
/************************************************************************/
int do_one_mode(void)
{
    int i;
    pltreg *pr = NULL;         // plot region
    O_p *op = NULL;      // initial plot
    d_s *dsi = NULL, *dsdr = NULL, *dsdi = NULL, *dsdp = NULL, *dsdv = NULL;     // initial and destination datasets
    int     nx;
    double co, si, tmp, A;
    static	int		kx = 1, drag = 0;
    static  float dt = 1;

	if (updating_menu_state != 0)	return D_O_K;
	if (key[KEY_LSHIFT])   return win_printf_OK("This routine project the present dataset one one Fourier mode\n"
				"Two datasets (cosine and sineare created");

   	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return(win_printf_OK("Could not find plot data"));

	nx = dsi->nx; if (nx<2) return(win_printf_OK("not enough points in dataset!"));

	i = win_scanf("On which mode do you want to project data %d",&kx);
	if (i == WIN_CANCEL) return D_O_K;
	if (kx < 0 || kx > nx/2) return(win_printf_OK("Mode out of range!"));
	dsdr = create_and_attach_one_ds(op, nx, nx, 0);
        if (dsdr == NULL)    	return win_printf_OK("Cannot allocate dataset");
    	inherit_from_ds_to_ds(dsdr, dsi);
	if (kx > 0 && kx < nx/2)
	  {
	    dsdi = create_and_attach_one_ds(op, nx, nx, 0);
	    if (dsdi == NULL)    	return win_printf_OK("Cannot allocate dataset");
	    inherit_from_ds_to_ds(dsdi, dsi);
	  }
	for (i = 0, co = si = 0; i < nx; i++)
	  {
	    co += dsi->yd[i] * cos((M_PI * 2 * i * kx)/nx);
	    si += dsi->yd[i] * sin((M_PI * 2 * i * kx)/nx);
	  }
	co /= nx; si /= nx;
	if (kx > 0 && kx < nx/2)
	  {
	    co *= 2;
	    si *= 2;
	  }
	for (i = 0; i < nx; i++)
	  {
	    dsdr->yd[i] =  co * cos((M_PI * 2 * i * kx)/nx);
	    dsdr->xd[i] = dsi->xd[i];
	  }
	for (i = 0; dsdi != NULL && i < nx; i++)
	  {
	    dsdi->yd[i] =  si * sin((M_PI * 2 * i * kx)/nx);
	    dsdi->xd[i] = dsi->xd[i];
	  }
    	set_formated_string(&dsdr->treatement,"Cosine projection of mode %d = %g phi %g radians",kx,co,atan2(si,co));
	if (dsdi != NULL)
	  set_formated_string(&dsdi->treatement,"Sine projection of mode %d = %g phi %g degrees",kx,si,(atan2(si,co)*180)/M_PI);


	i = win_scanf("Compute viscous drag No %R yes %r\ndt between points (seconds) %8f\n",&drag,&dt);
	if (i == WIN_CANCEL || drag == 0) return 0;
	if (dt <= 0) return win_printf_OK("Impossible value for dt %g",dt);

	dsdp = create_and_attach_one_ds(op, nx, nx, 0);
        if (dsdp == NULL)    	return win_printf_OK("Cannot allocate dataset");
    	inherit_from_ds_to_ds(dsdp, dsi);
	if (si != 0)
	  {
	    tmp = co*co/si;
	    tmp *= tmp;
	    A = sqrt(tmp);
	    tmp += co*co;
	    tmp = sqrt(tmp);
	    A += si;
	  }
	else A = tmp = 0;
	for (i = 0; i < nx; i++)
	  {
	    dsdp->yd[i] =  co * co * sin((M_PI * 2 * i * kx)/nx);
	    dsdp->yd[i] -=  co * si * cos((M_PI * 2 * i * kx)/nx);
	    if (si != 0) dsdp->yd[i] /=  si;
	    dsdp->xd[i] = dsi->xd[i];
	  }

    	set_formated_string(&dsdp->treatement,"orthogonal component of mode %d = %g phi %g radians (%g degrees) total amp = %g",kx,tmp,atan2(si,co),(atan2(si,co)*180)/M_PI,A);

	dsdv = create_and_attach_one_ds(op, nx, nx, 0);
        if (dsdv == NULL)    	return win_printf_OK("Cannot allocate dataset");
    	inherit_from_ds_to_ds(dsdv, dsi);
	tmp = (M_PI * 2 * kx)/(dt*nx);
	for (i = 0; i < nx; i++)
	  {
	    dsdv->yd[i] =  tmp * si * cos((M_PI * 2 * i * kx)/nx);
	    dsdv->yd[i] -=  tmp * co * sin((M_PI * 2 * i * kx)/nx);
	    dsdv->xd[i] = dsi->xd[i];
	  }
	A = tmp * sqrt(co * co + si * si);
	set_ds_treatement(dsdv,"Velocity component of mode %d = %g phi %g radians (%g degrees) dt %g s",kx,A,atan2(si,co),(atan2(si,co)*180)/M_PI,dt);
 	return refresh_plot(pr, UNCHANGED);
}

/********************************************************************/
/* computes the power spectrum of a dataset using fftw         	    */
/* this is an old function  (before 2002)                           */
/* its units are non dimensional in X and Y, use do_psd() instead!  */
/*                                                                  */
/* I removed access to it from the menu on 2007/06/05, N.G.         */
/* but the code is still there for comparison, just in case         */
/********************************************************************/
int do_spectrum(void)
{
    int i;
    pltreg *pr = NULL;         // plot region
    O_p *op = NULL, *opd = NULL;      // initial and destination plots
    d_s *dsi = NULL, *dsd = NULL;     // initial and destination datasets
    int     nx;
    //float f_acq;	// to build a X-unit set with Hz
    double  *in=NULL, *out=NULL;
    double  *power_spectrum=NULL;
    fftw_plan plan;     // fft plan for fftw3

	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
        	return win_printf_OK("This routine computes the power spectrum\n"
	        "of the selected dataset\n"
        	"a new plot is created");
	}
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("Could not find plot data");

	nx = dsi->nx;
	if (nx<2) return(win_printf_OK("not enough points (%d) in current dataset!",nx));

	//f_acq = find_sampling_frequency(dsi->xd, nx);


    in             =(double *) fftw_malloc(nx*sizeof(double));
    out            =(double *) fftw_malloc(nx*sizeof(double));
    power_spectrum =(double *) fftw_malloc((nx/2+1)*sizeof(double));
    plan           = fftw_plan_r2r_1d(nx, in, out, FFTW_R2HC, FFTW_ESTIMATE);   // the new fftw3 version!

    for (i=0 ; i< nx ; i++)     in[i] = dsi->yd[i];

    fftw_execute(plan); // fftw3 version!
    power_spectrum[0]=out[0]*out[0];    // DC component

    for (i=0; i<(nx+1)/2; ++i)          // k < nx/2 rounded up
        power_spectrum[i] = out[i]*out[i] + out[nx-i]*out[nx-i];
    if (nx % 2 ==0)                     // nx is even
        power_spectrum[nx/2] = out[nx/2]*out[nx/2]; // Nyquist frequency

    opd = create_and_attach_one_plot(pr, (nx/2)+1, (nx/2)+1, 0);
    if (opd == NULL)        return win_printf_OK("Cannot allocate plot");
    dsd = opd->dat[0];
    if (dsd == NULL)     {  fftw_destroy_plan(plan);
                            return D_O_K;
                         }
    for (i=0 ; i<= nx/2 ; i++)
    {
        dsd->xd[i] = i;		// mode number
        dsd->yd[i] = (float)power_spectrum[i]/nx;	// on 2004/01/16, added the normalisation by 1/nx
    }
    dsd->ny = 1+(nx/2);
    dsd->nx = 1+(nx/2);
    pr->one_p->iopt |= YLOG;

    fftw_free(in); fftw_free(out); fftw_free(power_spectrum);
    fftw_destroy_plan(plan);

    set_plot_title(opd, "spectrum");
    opd->filename = Transfer_filename(op->filename);
    opd->dir = Mystrdup(op->dir);

    uns_op_2_op(opd, op);
	// to do : add a unit set with the frequency. the ratio is : f_acq/nx;
    inherit_from_ds_to_ds(dsd, dsi);
    set_formated_string(&dsd->treatement,"spectrum");

    return refresh_plot(pr, UNCHANGED);
}



/****************************************************************/
/* the function below computes the cross-correlation of x and y	*/
/* and gives the result in 'cor_f' 				*/
/* correct frequencies for the x-axis are retruned in 'freq'	*/
/*								*/
/* 'cor_f' and 'freq' must be already allocated !		*/
/* (size=n_fft/2) if half of points,  				*/
/* (size=n_fft) if all points					*/
/****************************************************************/
int compute_correlation_of_float_data(float *x, float *y, float *cor_f, float *time,
				      float f_acq, int nx, int n_fft, int n_overlap,
				      int bool_hanning, int bool_half_points, int bool_normalize)
{
    int i,j,k;
    double  		*in1=NULL, *in2=NULL, *back=NULL, *cor_d=NULL;
    fftw_complex	*out1=NULL, *out2=NULL, *out3=NULL;
    double moy1, moy2, factor;
    fftw_plan plan1, plan2, plan_back;     // fft plan for fftw3

  in1  = (double *) fftw_malloc( n_fft     *sizeof(double));
  in2  = (double *) fftw_malloc( n_fft     *sizeof(double));
  out1 = (fftw_complex *) fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));
  out2 = (fftw_complex *) fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));
  out3 = (fftw_complex *) fftw_malloc((n_fft/2+1)*sizeof(fftw_complex));
  back = (double *) fftw_malloc( n_fft	  *sizeof(double));
  cor_d= (double *) fftw_malloc( n_fft	  *sizeof(double));

  for (i=0; i<n_fft; i++) cor_d[i]=0;

  plan1     = fftw_plan_dft_r2c_1d(n_fft,  in1,  out1, FFTW_ESTIMATE);
  plan2     = fftw_plan_dft_r2c_1d(n_fft,  in2,  out2, FFTW_ESTIMATE);
  plan_back = fftw_plan_dft_c2r_1d(n_fft, out3,  back, FFTW_ESTIMATE);

  k=0;
  for (j=0; j<(nx-n_fft)+1; j+=(n_fft-n_overlap) )
    {	// ci-dessous, on calcule les valeurs moyennes:
      moy1 = 0.;	moy2 = 0.;
      for (i=0 ; i< n_fft ; i++)
    	{   moy1 += (double)x[j+i];  	moy2 += (double)y[j+i];
	}
      moy1 /= (double)n_fft;	moy2 /= (double)n_fft;

      // ici, on calcule le carre des variables, sans valeur moyenne:
      for (i=0; i<n_fft ; i++)
	{	in1[i] = (double)x[j+i]-moy1;
	  in2[i] = (double)y[j+i]-moy2;
	}

      // fenetrage eventuel:
      if (bool_hanning==1)
	{	fft_window_real(n_fft, in1, moy1*1e-2);
	  //		fft_window_real(n_fft, in2, moy2*1e-2);
	}

      // ci-dessous, transformees de fourier directes:
      fftw_execute(plan1);
      fftw_execute(plan2);

      // ci-dessous, multiplication de out1 par le complexe conjugue de out2:
      for (i=1; i<n_fft/2+1; i++)
    	{   out3[i][0] = out1[i][0] * out2[i][0] + out1[i][1] * out2[i][1];
	  out3[i][1] = out1[i][1] * out2[i][0] - out1[i][0] * out2[i][1];
    	}
      out3[0][0] = 0.;
      out3[0][1] = 0.;

      // ci-dessous, transformee de fourier inverse:
      fftw_execute(plan_back);

      // defenetrage eventuel:
      if (bool_hanning==1)
	{//	fft_unwindow_real(n_fft, back, moy1*1e-2);
	  //	fft_unwindow_real(n_fft, back, moy2*1e-2);
	}

      for (i=0 ; i < n_fft/2 ; i++)
    	{   cor_d[i+n_fft/2] += back[i];
	  cor_d[i]         += back[i+n_fft/2];
    	}

      k++;
    } // at the end of the loop, we have averaged $k$ spectra

  if (k==0) return(k);

  if (bool_normalize==1)	factor = 1./cor_d[n_fft/2];
  else					factor=(double)1./((double)k*(double)n_fft*(double)n_fft);

  if (bool_half_points==1)
    for (i=0 ; i < n_fft/2 ; i++)
      {   cor_f[i] = (float)(cor_d[i+n_fft/2] * factor);
        time[i]  = (float)i/f_acq;
      }
  else // all n_fft points, ie, negative values of time are also output
    for (i=0 ; i < n_fft/2 ; i++)
      {   cor_f[i+n_fft/2] = (float)(cor_d[i+n_fft/2]         * factor);
        cor_f[i]         = (float)(cor_d[i] * factor);
        time[i]          = (float)(i-n_fft/2.)/f_acq;		// note : 1/f_acq = dt
        time[i+n_fft/2]  = (float)i/f_acq;
      }

  fftw_free(in1); 	fftw_free(in2);
  fftw_free(out1); 	fftw_free(out2); 	fftw_free(out3);
  fftw_free(back);	fftw_free(cor_d);
  fftw_destroy_plan(plan1);
  fftw_destroy_plan(plan2);
  fftw_destroy_plan(plan_back);

  return(k); // we return the number of windows operated on
}




/********************************************************************/
/* computes the cross-correlation (or auto-correlation) :           */
/* uses fftw v3.0.1                                                 */
/********************************************************************/
int do_correlation(void)
{
    int	index=0, n2, k;
    pltreg	*pr = NULL;
    O_p	*op,   *opd = NULL;
    d_s	*dsi1=NULL, *dsi2=NULL, *dsd=NULL;
    int	nx;
    static float	f_acq=1.;
    char	title[128] = {0};
    static int bool_hanning=1, bool_plot_half=1, bool_normalize=0;
    static int n_fft=4096, n_overlap;
    char	message[512] = {0};
    float   moyenne=0, m2=0, variance=0;


  if(updating_menu_state != 0)	return D_O_K;
  index = (active_menu->flags & 0xFFFFFF00);

  if (key[KEY_LSHIFT])
    {	if (index==ONE_DS)	return win_printf_OK("This routine computes the auto-correlation\n"
						     "of the selected dataset\n\n"
						     "a new plot is created\n");
      else if (index==TWO_DS) return win_printf_OK("This routine computes the cross-correlation\n"
						   "of the selected dataset and the one just before (N-1) on the same plot\n\n"
						   "a new plot is created\n");
    }
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi1) != 3)
    return win_printf_OK("Could not find plot data");

  if (index==ONE_DS)        	// autocorrelation
    {   dsi2 = dsi1;
      strcpy(title,"Auto-correlation");
    }
  else if (index==TWO_DS)		// cross-correlation
    {	if (op->n_dat < 2 )   	return(win_printf_OK("not enough datasets!\ntry auto-correlation instead"));
      dsi2 = op->dat[(pr->one_p->cur_dat + op->n_dat - 1) % op->n_dat];
      strcpy(title,"Cross-correlation");
    }
  if ( dsi1->nx != dsi2->nx)	return win_printf_OK("The number of points in the two datasets differs\n"
						     "(%d != %d)!\nCan't correlate...", dsi1->nx, dsi2->nx);
  nx = dsi1->nx;

  if (nx<2) return(win_printf_OK("not enough points in dataset!"));
  for (n2 = 1; n2 < nx; n2 <<= 1);
  n2>>=1;	// n2 is the closest power of 2 lower than or equal to nx
  n_fft     = (n2>8192) ? 4096 : n2;
  n_overlap = n_fft*0.8;
  f_acq = find_sampling_frequency(dsi1->xd, nx);

  sprintf(message, "dataset has %d points. lower or equal power of 2 is %d\n"
	  "number of points of fft window ? %%d"
	  "overlap (in points) between windows ? %%d"
	  "acquisition frequency (in Hz, for normalization) ?%%f"
	  "%%b use Hanning window\n"
	  "%%R plot all points %%r plot half of points (positive \\tau)\n"
	  "%%b normalize autocorrelation function (divide by C_{xx}(\\tau=0))",nx, n2);
  if (win_scanf(message,&n_fft,&n_overlap,&f_acq,&bool_hanning, &bool_plot_half, &bool_normalize) == WIN_CANCEL)
    return(D_O_K);
  if (n_overlap>=n_fft)
    return(win_printf("overlap is too large compared to n\\_fft (%d>=%d)", n_overlap, n_fft));
  if (n_fft%2!=0)
    return(win_printf_OK("window size should be odd.\naborting"));

  if (bool_plot_half==1) opd = create_and_attach_one_plot(pr, n_fft/2, n_fft/2, 0);
  else				   opd = create_and_attach_one_plot(pr,   n_fft,   n_fft, 0);
  if (opd == NULL)	return win_printf_OK("Cannot allocate plot");
  dsd = opd->dat[0];
  if (dsd == NULL)	return D_O_K;

  k = compute_correlation_of_float_data(dsi1->yd, dsi2->yd, dsd->yd, dsd->xd,
					f_acq, nx, n_fft, n_overlap, bool_hanning, bool_plot_half, bool_normalize);
  if (k==0) return(win_printf_OK("no window operated on!"));

  inherit_from_ds_to_ds(dsd, dsi1);
  set_formated_string(&dsd->treatement, "%s", title);

  set_plot_title(opd, "%s (%d windows)", title, k);
  opd->filename = Transfer_filename(op->filename);
  opd->dir = Mystrdup(op->dir);

  if ( (index==ONE_DS) && (bool_normalize==0) )
    {	if (win_printf("Normalize autocorrelation by variance ?\n\nOK to do so, WIN_CANCEL not to do so")!=WIN_CANCEL)
	{
#ifdef USE_GSL
	  variance = gsl_stats_float_variance(dsi1->yd, 1, nx);
#else
	  moyenne=0.;
	  for (k=0; k<nx; k++)	moyenne += dsi1->yd[k];
	  moyenne /= nx;
	  variance=0.;
	  for (k=0; k<nx; k++)	variance += (dsi1->yd[k]-moyenne)*(dsi1->yd[k]-moyenne);
	  variance /= nx;
#endif

	  for (k=0; k<n_fft/(1+bool_plot_half); k++)
	    {	dsd->yd[k] /= variance;
	    }
        }
    }
  if ( (index==TWO_DS) && (bool_normalize==0) )
    {	if (win_printf("Normalize cross-correlation by covariance ?\n\nOK to do so, WIN_CANCEL not to do so")!=WIN_CANCEL)
	{
#ifdef USE_GSL
	  variance = gsl_stats_float_covariance(dsi1->yd, 1, dsi2->yd, 1, nx);
#else
	  moyenne  =0.;
	  m2       =0.;
	  for (k=0; k<nx; k++)	{ moyenne += dsi1->yd[k]; m2 += dsi2->yd[k]; }
	  moyenne /= nx;
	  m2      /= nx;
	  variance =0.;
	  for (k=0; k<nx; k++)	variance += (dsi1->yd[k]-moyenne)*(dsi2->yd[k]-m2);
	  variance /= nx;
#endif

	  for (k=0; k<n_fft/(1+bool_plot_half); k++)
	    {	dsd->yd[k] /= variance;
	    }
        }
    }
  return refresh_plot(pr, UNCHANGED);
}




/****************************************************************/
/* filter a dataset, input is real, and so is output		*/
/****************************************************************/
int do_filter_ds(void)
{
    int 	i, j;
    int 		no;
    int 		nx, nx_2, nout;
    //    	char  		c2[128];
    pltreg 	*pr = NULL;
    O_p 		*op = NULL;
    d_s 		*dsi = NULL, *dsf = NULL;
    double	*in = NULL;
    fftw_complex 	*out = NULL;
    fftw_plan 	plan;
    static int    rmdc = 1, win = 1;
    static float  smp = 0.05;
    double        mean;


  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine filters a dataset.\n"
			 "a new dataset is created.");
  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
    return(win_printf_OK("can't find data set"));

  nx = dsi->ny;
  nout = nx;
  nx_2 = nx/2;
  /*
    sprintf(c2, "initial dataset has %d points\n"
    "number of points in filtered dataset ? %%d",nx);
    if (win_scanf(c2, &nout) == WIN_CANCEL)    return OFF;
    if ( (nout>nx) || (nout<=0) )
    {	nout=nx;
    win_printf_OK("number of points will be the same (%d)",nout);
    }
  */
  current_filter=construct_filter(nx, (REAL_INPUT | REAL_OUTPUT | SYM),op->xu[op->c_xu]->dx ); // nx /* nx_2+1 */

  if (current_filter==NULL)	return win_printf_OK("filter is NULL!!");
  if (current_filter->n==0) 	return win_printf_OK("filter has 0 points!!");
  if (current_filter->f==NULL)	return win_printf_OK("filter data is NULL!!");
  // win_printf("filter is tested");
  in  = (double *)fftw_malloc((nx+(nx%2))*sizeof(double));		// to store initial data
  out = (fftw_complex *) fftw_malloc((nx_2+1)   *sizeof(fftw_complex));	// to store the complex Fourier transform
  if (in==NULL || out==NULL)  return win_printf_OK("malloc error");


  i = win_scanf("Remove DC component %b\n"
		"apply a non vanishing Hamming window %b\n"
		"with a minimum value of %10f",&rmdc,&win,&smp);
  if (i == WIN_CANCEL) return 0;
  if (smp <= 0)  return win_printf_OK("smp %g must be > 0",smp);
  for (i=0, mean = 0; i<nx ; i++)
    {
      in[i] = (double)(dsi->yd[i]);
      mean += in[i];
    }
  for (i=0, mean = (nx > 0) ? mean/nx : 0; i<nx && rmdc; i++)
      in[i] -= mean;

  for (i=0; i<nx && win; i++)
    in[i] *= (1+smp - cos((2*M_PI*i)/nx));



  plan = fftw_plan_dft_r2c_1d(nx, in, out, FFTW_ESTIMATE);
  fftw_execute(plan);
  fftw_destroy_plan(plan);
  // win_printf("fft is performed");
  apply_filter_complex(current_filter, out, nx_2+1);	// the fft has nx/2+1 complex points
  // win_printf("filter is applied");
  plan = fftw_plan_dft_c2r_1d(nx, out, in, FFTW_ESTIMATE);
  fftw_execute(plan);
  fftw_destroy_plan(plan);
  // win_printf("fft-1 is performed");
  dsf = build_data_set(nout, nout);
  if (dsf == NULL) return win_printf_OK("can't create data set");

  // we invert hanning window
  for (i=0; i<nx && win; i++)
    in[i] /= (1+smp - cos((2*M_PI*i)/nx));

  // we restore DC
  //win_printf("rmdc %d mean %g",rmdc,mean);
  for (i=0, mean *= nx; i<nx && rmdc; i++)
    in[i] += current_filter->f[0]*mean;

  no=1;	// we keep all the points. we can use ds_decime() to eliminate non informative points
  /*	no = (nout != 0) ? (nx/nout) : 1; */
  for (i=0; i<nx ; i++)
    {	j = i/no;
      if ( (i%no == 0) && (j<nout) )
	{	dsf->yd[j] = (float)(in[i]/nx);
	  dsf->xd[j] = dsi->xd[i];
	}
    }

  fftw_free(in); fftw_free(out);
  dsf->ny = nout;
  dsf->nx = nout;
  add_one_plot_data (op, IS_DATA_SET, (void *)dsf);

  inherit_from_ds_to_ds(dsf,dsi);
  dsf->treatement = my_sprintf(NULL, "filtred %s", (dsi->source != NULL) ? dsi->source : "no source");

  return refresh_plot(pr, UNCHANGED);
}




/****************************************************************/
/* filter a dataset, input is real, and so is output		*/
/****************************************************************/
int do_Hilbert_demodulation(void)
{
    int j, k;
    int 		nx, nout, no;
    //    	char  		c2[128];
    pltreg 		*pr = NULL;
    O_p 		*op = NULL;
    d_s 		*dsi = NULL, *ds_amplitude=NULL, *ds_reel=NULL, *ds_freq=NULL;
    double      norm=1., vec=0., sca=0.;
    static float    cl, wl, ch, wh;
    float       f_acq;
    fftw_complex *inc = NULL;
    static int       bool_Hanning=1, bool_amplitude=1, bool_reel=1, bool_freq=0;

	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
			return win_printf_OK("This routine performs a Hilbert transform on a dataset.\n"
        		"a new dataset is created with the amplitude and eventually another one with the phase.");
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
			return(win_printf_OK("can't find data set"));

	nx   = dsi->ny;
	nout = nx;
	f_acq = find_sampling_frequency_dialog(dsi->xd, dsi->nx);

	if (win_scanf("{\\pt14\\color{yellow}Hilbert transform for demodulation}\n\n"
	              "{\\pt14\\color{yellow}with normal bandpass filter}\n\n"
                  " %7f : low cutoff\n %7f : width\n"
				  " %7f : high cutoff\n %7f : width\n\n"
				  "sampling frequency is %7f ({\\color{lightred}important!})\n"
                  "%b apply Hanning window\n"
                  "%b plot local amplitude\n"
                  "%b plot real part (filtered data)\n"
                  "%b plot local frequency",
                  &cl, &wl, &ch, &wh, &f_acq,
                  &bool_Hanning, &bool_amplitude, &bool_reel, &bool_freq)==WIN_CANCEL) return(D_O_K);
	if ( (bool_amplitude + bool_reel + bool_freq) < 1) return(win_printf_OK("Nothing to do!"));

	// following function does the mathematical job. it also allocates "inc" do we don't have to do it.
	inc = Hilbert_transform(dsi->yd, nx, f_acq, cl, wl, ch, wh, bool_Hanning);
	if (inc==NULL) return(win_printf_OK("error during Hilbert transform"));

    if (bool_amplitude==1)
	{ ds_amplitude = duplicate_data_set(dsi,NULL);
	  if (ds_amplitude == NULL) return win_printf_OK("can't create data set");

  	  no=1;	// we keep all the points. we can use ds_decime() to eliminate non informative points
 	  for (k=0; k<nx ; k++)
      {	    j = k/no;
        	if ( (k%no == 0) && (j<nout) )
        	{	ds_amplitude->yd[j] = (float)sqrt(inc[k][0]*inc[k][0] + inc[k][1]*inc[k][1]);
        	}
        //	if (ds_amplitude->ye)  ds_amplitude->ye[j] = dsi->ye[k] / no;
      }
      inherit_from_ds_to_ds(ds_amplitude, dsi);
      set_formated_string(&ds_amplitude->treatement,"Hilbert demodulation, local amplitude");
   	  add_one_plot_data (op, IS_DATA_SET, (void *)ds_amplitude);
    }

    if (bool_reel==1)
	{ ds_reel = duplicate_data_set(dsi,NULL);
	  if (ds_reel == NULL) return win_printf_OK("can't create data set");

  	  no=1;	// we keep all the points. we can use ds_decime() to eliminate non informative points
 	  for (k=0; k<nx ; k++)
      {	    j = k/no;
        	if ( (k%no == 0) && (j<nout) )
        	{	ds_reel->yd[j] = (float)inc[k][0];
        	}
        	if (ds_reel->ye)  ds_reel->ye[j] = dsi->ye[k] / no;
      }
      inherit_from_ds_to_ds(ds_reel, dsi);
      set_formated_string(&ds_reel->treatement,"Hilbert demodulation, real part");
   	  add_one_plot_data (op, IS_DATA_SET, (void *)ds_reel);
    }

    if (bool_freq==1)
	{ ds_freq = duplicate_data_set(dsi,NULL);
	  if (ds_freq == NULL) return win_printf_OK("can't create data set");

      norm = (float)nx/(2*M_PI);

      for (j=0; j<nx ; j++)
      {	  k = j+1 % nout;
		  vec = inc[j][0]*inc[k][1] - inc[j][1]*inc[k][0];
		  sca = inc[j][0]*inc[k][0] + inc[j][1]*inc[k][1];

          if ( vec == 0.0 && sca == 0.0)	ds_freq->yd[j] = 0.;
		  else ds_freq->yd[j] = norm*(float)atan2 ( vec, sca );
      }
      if (bool_Hanning==1)     ds_freq->yd[nx-1] = 0.0;

      inherit_from_ds_to_ds(ds_freq, dsi);
      set_formated_string(&ds_freq->treatement,"Hilbert demodulation, local frequency");
   	  add_one_plot_data (op, IS_DATA_SET, (void *)ds_freq);

    }

	fftw_free(inc);

	return refresh_plot(pr, UNCHANGED);
}





int do_display_current_filter(void)
{

	if(updating_menu_state != 0)
	{	if (current_filter==NULL)   	active_menu->flags |=  D_DISABLED;
		else
		{ if (current_filter->n==0)	active_menu->flags |=  D_DISABLED;
		  else	 			active_menu->flags &= ~D_DISABLED;
		 }
    		return D_O_K;
	}

	if (current_filter!=NULL) plot_filter_in_pltreg(current_filter);
	else win_printf_OK("no filter in use for now");
	return 0;
}



int do_display_Hamming(void)
{
    int i;
    int	nx, n2, n_fft;
    pltreg	*pr = NULL;
    O_p	*op = NULL;
    d_s	*dsi = NULL, *dsd = NULL;
    float	smp=0.01;
    char	message[512] = {0};

	if(updating_menu_state != 0)	return D_O_K;

   	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("Could not find plot data");

    	nx = dsi->nx; if (nx<2) return(win_printf_OK("not enough points in dataset!"));
    	for (n2 = 1; n2 <= nx; n2 <<= 1);
	n2>>=1;	// n2 is the closest power of 2 lower than or equal to nx
	n_fft     = (n2>8192) ? 4096 : n2;
	sprintf(message, "dataset has %d points. lower or equal power of 2 is %d\n"
			"number of points of fft window ? %%d"
			"value for Hamming bounds ? %%f",nx, n2);
	if (win_scanf(message,&n_fft,&smp) == WIN_CANCEL) 		return D_O_K;

    	dsd = create_and_attach_one_ds(op, n_fft, n_fft, 0);
    	for (i=0 ; i<n_fft; i++)
    	{   dsd->xd[i] = dsi->xd[i];
        	dsd->yd[i] = (float)(Hamming(n_fft, i, smp));
    	}
    	inherit_from_ds_to_ds(dsd, dsi);
	dsd->treatement = my_sprintf(NULL, "Hamming window with %f", smp);

	return(refresh_plot(pr, UNCHANGED));
}
