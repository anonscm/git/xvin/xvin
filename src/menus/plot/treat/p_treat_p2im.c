/********************************************************************/
/* p_treat_p2im.h												    */
/* operations transforming plots into images            		    */
/********************************************************************/

#include "platform.h"
#include "xvin.h"
#include "xv_tools_lib.h"

#include "p_treat_p2im.h"
#include "p_treat_basic.h"
# include <stdlib.h>
# include <string.h>



/********************************************************************/
/* transformation of a plot with N datasets into an image 	        */
/* 	NG	03.04.2000		(in pl2.c) 		                            */
/*  adapted 30.05.2006 for Allegro version                          */
/********************************************************************/
int do_plot_to_im(void)
{
  register int i, j=1, k;           	/* intermediaire compteurs de boucle    */
/*	Bto 	*ild;         */       	/* bouton clique lors de l'appel        */
	imreg 	*imr;			/* image region 			*/
	O_i 	*oi;		    	/* one image source and destination 	*/
	union 	pix *pd;   		/* data des images (matrice) 	       	*/
	int	nx, ny;		       	/* dimensions en x, y			*/
/*	char  	c1[128];      */	/* chaine pour l'info et l'heritage     */
	O_p 	*op;
	d_s 	*ds;	 		/* ds is part of the source		*/
	pltreg 	*pr;
static int bool_update_unitsets=1, bool_yunit=0, bool_take_log=0;
    char *message=NULL;
static char parameter_name[128]="tau";
    char parameter_string[128]="tau=";
    int  p1=-1, p2=-1;
    d_s *dsh = NULL;
    int nmds = 0;

    if (updating_menu_state != 0)	return(D_O_K);	
	if (key[KEY_LSHIFT]) return(win_printf_OK("This function builds an image from\n"
				"the datasets of a plot.\n\n"
				"All datasets must have the same number of points n_x, same x-axis is not required\n"
				"If there are n_y datasets, the result is an image with size n_x x n_y"));

    if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
		return(win_printf_OK("I cannot find plot data !"));

	ny = op->n_dat;				/* nombre de datasets 		*/
	if (ny<4) return(win_printf_OK("Not enough datasets!\nI want 4 minimum, if not, the image will be too thin."));


	float dxmean = 0;
	for (i = k = 0, dxmean = 0; i < ny; i++)
	{
	  for (j=1; j< op->dat[i]->nx; j++)
	    {
	      dxmean += fabs(op->dat[i]->xd[j] - op->dat[i]->xd[j-1]);
	      k++;
	    }
	}
	dxmean /= (k) ? k : 1; 
	
	dsh = build_data_set(16,16);
	if (dsh == NULL) return(win_printf_OK("I cannot create tmp ds !"));
	dsh->nx = dsh->ny = 0;

	for (i=0, nmds = 0; i < ny; i++)
	{
	  nmds = (op->dat[i]->nx > nmds) ? op->dat[i]->nx : nmds;
	  for (j=0; j< op->dat[i]->nx; j++)
	    {
	      for (k = 0 ; k < dsh->nx; k++)
		if (fabs(dsh->xd[k] - op->dat[i]->xd[j]) <= dxmean/2) break;
	      if (k >= dsh->nx)
		  add_new_point_to_ds(dsh, op->dat[i]->xd[j], 1);
	      else
		dsh->yd[k] += 1;
	    }
	  //win_printf("ds %d total size %d",i, dsh->nx);
	}
	sort_ds_along_x(dsh);
	if (dsh->nx > 2 * nmds)
	  return(win_printf_OK("Dataset have too different number of points!\n"
			       " %d common points ",dsh->nx));
	/*
	nx=op->dat[0]->nx;
	for (i=1; i<ny; i++)
	{  if (op->dat[i]->nx != nx) 
		return(win_printf_OK("Dataset %d have not the same number of points as dataset 0 ! ",i));
	}
	*/
	for (k = 2 ; k < dsh->nx; k++)
	  if ((dsh->xd[k] - dsh->xd[k-1]) != (dsh->xd[1] - dsh->xd[0])) break;
	//if (k < dsh->nx)
	//return(win_printf_OK("points are not equally spaced !\n"
	//		       "at %d",k));
	nx = dsh->nx;
	
	message=my_sprintf(message, "I'm about to build an image with the %d datasets of the current plot.\n"
                                "all datasets have %d points\n\n"
                                "so image will be {\\color{yellow}%d x %d}\n\n"
                                "%%b use x-values of dataset 0 to build x-unitset of image\n"
                                "%%b use a parameter from ds->treatment or ds->history to build y-unitset\n"
                                "       parameter : %%10s\n"
                                "%%b take log(y) as z instead of y", 
                                ny, nx, nx, ny);
	i=win_scanf(message, &bool_update_unitsets, &bool_yunit, &parameter_name, &bool_take_log);
	free(message);

	imr = create_and_register_new_image_project( 0,   32,  900,  668);
	if (imr == NULL) return(win_printf_OK("could not create imreg"));	
	oi = create_and_attach_oi_to_imr(imr, dsh->nx, ny, IS_FLOAT_IMAGE);
	remove_from_image (imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
	                        select_image_of_imreg(imr, imr->n_oi -1);

	pd  = oi->im.pixel;

	for (i=0; i<ny; i++)	/* one dataset is one line of the image */
	{
	  ds = op->dat[i];
	  for (j=0; j < dsh->nx; j++)
	    {
	      for (k = 0 ; k < ds->nx; k++)
		if (fabs(dsh->xd[j] - op->dat[i]->xd[k]) <= dxmean/2) break;
	      if (k < ds->nx)
		{
		  if (bool_take_log==1) 
		    {
		      pd[i].fl[k] = (ds->yd[k]>0) ? logf(fabsf(ds->yd[k])) : 0.;
		    }
		  else
		    {
		      pd[i].fl[j] = ds->yd[k];
		    }
		}
	    }
	}

    uns_op_2_oi (op, IS_X_UNIT_SET, oi, IS_X_UNIT_SET);
    //uns_op_2_oi (op, IS_Z_UNIT_SET, oi, IS_Y_UNIT_SET);
    
    if (bool_update_unitsets==1)
    {  
      set_unit_offset(oi->xu[0], op->dat[0]->xd[0]);
      if (op->dat[0]->nx-1 > 0)
	set_unit_increment(oi->xu[0], (op->dat[0]->xd[op->dat[0]->nx-1]-op->dat[0]->xd[1])/(op->dat[0]->nx-1));
      else
	set_unit_increment(oi->xu[0], (op->dat[0]->xd[1] - op->dat[0]->xd[0]));
       set_oi_x_unit_set (oi, 0);
    }
    
    if (bool_yunit==1)
    {   strcpy(parameter_string, parameter_name);
        ds=op->dat[0];
        p1 = grep_int_in_string(ds->treatement, parameter_string);
		if (p1==-1) 	p1 = grep_int_in_string(ds->history, parameter_string);
		if (p1==-1) 	
        {  strcat(parameter_string, "=");
           p1 = grep_int_in_string(ds->treatement, parameter_string);
           if (p1==-1)  p1 = grep_int_in_string(ds->history, parameter_string);
        }
        if (p1==-1) 	
        {  strcpy(parameter_string, parameter_name);
           strcat(parameter_string, " ");
           p1 = grep_int_in_string(ds->treatement, parameter_string);
           if (p1==-1)  p1 = grep_int_in_string(ds->history, parameter_string);
        }
        
        strcpy(parameter_string, parameter_name);
        ds=op->dat[1];
        p2 = grep_int_in_string(ds->treatement, parameter_string);
		if (p2==-1) 	p2 = grep_int_in_string(ds->history, parameter_string);
		if (p2==-1) 	
        {  strcat(parameter_string, "=");
           p2 = grep_int_in_string(ds->treatement, parameter_string);
           if (p2==-1)  p2 = grep_int_in_string(ds->history, parameter_string);
        }
        if (p2==-1) 	
        {  strcpy(parameter_string, parameter_name);
           strcat(parameter_string, " ");
           p2 = grep_int_in_string(ds->treatement, parameter_string);
           if (p2==-1)  p2 = grep_int_in_string(ds->history, parameter_string);
        }
        
        if ( (p1<0) || (p2<0) ) win_printf("cannot find %s in datasets infos of 0 and 1", parameter_name);
        else if (p1==p2)        win_printf("%s is the same (=%d) in datasets infos of 0 and 1", parameter_name, p1);
        else
        {       set_unit_offset(oi->yu[0],    (float)p1);
                set_unit_increment(oi->yu[0], (float)(p2-p1) );
                set_oi_y_unit_set (oi, 0);
        }
         
        set_im_y_title(oi, "%s", parameter_name); 
    }
    

	oi->im.source = Mystrdup(op->dat[0]->source);
    set_im_title(oi, "image of %s", (op->title!=NULL) ? op->title : op->y_title);
    set_im_x_title(oi, "%s", op->x_title);
    
	find_zmin_zmax(oi);			/* recherche du meilleur contraste pour l'affichage 	*/
	do_one_image(imr);

	return(D_REDRAW);		/* retour et rafraichissement video */

} /* end of the "im_phase_multiplication" function */

