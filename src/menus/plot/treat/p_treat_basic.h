/********************************************************************/
/* plot_treat_basic.h												*/
/* basic data manipulation and treatments for dataset(s)			*/
/********************************************************************/
#include "platform.h"
#include "plot_ds.h"
#include <limits.h>
// definitions for cheating (removing visible or unvisible points):
#define VISIBLE		0x00000100
#define INVISIBLE	0x00001000
#define DS_ONE      0x00010000
#define DS_ALL      0x00100000
#define DS_COLOR    0x01000000

// definitions for sorting a dataset usoin the QuickSort algorithm:
#define ASCENDING   0x0200 	/* to sort in ascending order 	*/
#define DESCENDING  0x0400 	/* to sort in the reverse order	*/

// functions defined in plot_treat_basic.c:
XV_FUNC(int, do_swap_x_y, (void));			// to swap X and Y
XV_FUNC(int, cheatting, (void));			// remove visible or unvisible points
XV_FUNC(int, changing_value, (void));
XV_FUNC(int, QuickSort_double, (float *xd, float *yd, int l, int r)); // QuickSort, used by ds_sort()
XV_FUNC(int, ds_sort, (void));			/* sort a data set in increasing or decreasing X values	*/
XV_FUNC(int, sort_ds_along_x,(d_s *ds));		// used by 'do_sort_ds_along_x'
XV_FUNC(int, sort_ds_along_y,(d_s *ds));		// used by 'do_sort_ds_along_x'
XV_FUNC(int,	do_sort_ds_along_x,(void));	// to sort a dataset according to increasing x-values
XV_FUNC(int,	do_sort_ds_backward_along_x,(void));	// to sort a dataset according to decreasing x-values
XV_FUNC(int, ds_reduce,(void));			/* reduce a data set with duplicated X values	*/
XV_FUNC(int, ds_decime,(void));			/* decime a data set (1 point on N kept)	*/
XV_FUNC(int, ds_same_x_average,(void));
XV_FUNC(int, ds_merge,(void));				/* merge 2 datasets in one			*/
XV_FUNC(int, do_merge_all_ds_in_op,(void));	// from interpol.c
XV_FUNC(int, ds_decime_avg,(void));		/* average a data set (1 point on N kept)	*/
XV_FUNC(int, ds_periodic_avg,(void));
XV_FUNC(int, ds_find_max,(void));			/* find maximum and minimum of a dataset	*/
XV_FUNC(int, ds_normalized_max_to_1,(void));
XV_FUNC(int, ds_find_halfmax,(void)); 		/* find half heigth and thickness 		*/
XV_FUNC(int, ds_find_all_extrema_data,(d_s *ds, int start, int end)); // used by ds_find_halfmax
XV_FUNC(int, remove_ds_with_invisible_points,(void));
XV_FUNC(int, remove_ds_with_all_invisible_points,(void));
XV_FUNC(int, remove_ds_with_un_or_visible_points_with_large_derivative,(void));
XV_FUNC(float, interpolate_point_by_poly_4_in_array,(float *x, float *y, int nx, float xp));
XV_FUNC(float, interpolate_point_by_poly_3_in_array,(float *x, float *y, int nx, float xp));
XV_FUNC(float, interpolate_point_by_poly_2_in_array,(float *x, float *y, int nx, float xp));
XV_FUNC(int, remove_point_using_range, (O_p *src_op, d_s **src_ds_ptr,
                             float range_x_min, float range_x_max,
                             float range_y_min, float range_y_max,
                             bool remove_inside));
XV_FUNC(int, remove_point_using_x_range, (O_p *src_op, d_s **src_ds_ptr,
                                                          float range_x_min, float range_x_max,
                                                                                          bool remove_inside));
XV_FUNC(int, remove_ds_with_color, (void));
XV_FUNC(int, do_aggregate_ds_per_source, (void));
XV_FUNC(int, do_aggregate_ds_per_source_range, (void));
XV_FUNC(int, do_stretch_histograms, (void));
XV_FUNC(int, do_shift_histograms, (void));
XV_FUNC(int, do_apply_color_ds_per_source_pattern, (void));
XV_FUNC(int, do_apply_color_ds_per_source_pattern_by_range, (void));
XV_FUNC(int,ds_split_ds,(void));
XV_FUNC(int, split_ds_of_op,(O_p *op, int ds_all, int cd, float delta_x));

//int offseting_value(void);
