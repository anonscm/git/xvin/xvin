
# include "xvin.h"
# include "allegro.h"

# include "fftl32n.h"
# include "fillib.h"

# include "plot_mn.h"

#include "p_treat_basic.h"

# include <stdlib.h>
# include <string.h>

#include "interpol.h"


float interpolate_point_by_poly_4_in_array(float *x, float *y, int nx, float xp);
float interpolate_point_by_poly_3_in_array(float *x, float *y, int nx, float xp);
float interpolate_point_by_poly_2_in_array(float *x, float *y, int nx, float xp);

int QuickSort_double(float *xd, float *yd, int l, int r); // defined in p_treat_basic.c, declared in p_treat_basic.h

/*
float gasdev(long *idum);
float ran1(long *idum);
*/


float y_pulse[] =
{1, 0.989963, 0.960337, 0.912539, 0.848826, 0.772148, 0.685955,
0.593978, 0.5, 0.407632, 0.320112, 0.240142, 0.169765, 0.110307,
0.06236, 0.025825, 3.72529e-09, -0.016305, -0.024624, -0.026682, -0.024252,
-0.019018, -0.012472, -0.005831, 2.79397e-09, 0.004447, 0.007242, 0.008386,
0.008084, 0.006682, 0.004595, 0.002243, -3.72529e-09, -0.001844, -0.003104,
-0.003705, -0.003675, -0.003118, -0.002198, -0.001098, -5.58794e-09, 0.000941,
0.001614, 0.001962, 0.001979, 0.001706, 0.001221, 0.000619, -3.72529e-09,
-0.000545, -0.000946, -0.001164, -0.001187, -0.001035, -0.000748, -0.000383,
7.45058e-09, 0.000344, 0.000602, 0.000747, 0.000768, 0.000675, 0.000492,
0.000253, 6.51926e-09, -0.000231, -0.000407, -0.000508, -0.000526, -0.000465,
-0.00034, -0.000176, -1.86265e-09, 0.000162, 0.000288, 0.000361, 0.000375,
0.000333, 0.000245, 0.000128, 6.51926e-09, -0.000118, -0.000211, -0.000266,
-0.000277, -0.000247, -0.000183, -9.54811e-05, 4.65661e-09, 8.91602e-05, 0.000159,
0.000201, 0.000211, 0.000189, 0.00014, 7.32243e-05, 2.79397e-09, -6.87726e-05,
-0.000123, -0.000156, -0.000164, -0.000147, -0.000109, -5.73881e-05, -1.86265e-09,
5.41424e-05, 9.72347e-05, 0.000123, 0.00013, 0.000117, 8.69622e-05, 4.57959e-05,
8.3819e-09, -4.34015e-05, -7.80858e-05, -9.93684e-05, -0.000105, -9.4343e-05, -7.03726e-05,
-3.71337e-05, 1.86265e-09, 3.53232e-05, 6.36652e-05, 8.11443e-05, 8.56975e-05, 7.72988e-05,
5.77718e-05, 3.0552e-05, 0, -2.90982e-05, -5.25471e-05, -6.70888e-05, -7.09891e-05,
-6.41085e-05, -4.79612e-05, -2.53897e-05, 1.86265e-09, 2.42842e-05, 4.39007e-05, 5.6114e-05,
5.94389e-05, 5.37541e-05, 4.02695e-05, 2.13375e-05, 0, -2.04686e-05, -3.7035e-05,
-4.74006e-05, -5.02653e-05, -4.55119e-05, -3.4133e-05, -1.81049e-05, -1.86265e-09, 1.73971e-05,
3.15215e-05, 4.03887e-05, 4.28762e-05, 3.88455e-05, 2.91746e-05, 1.54823e-05, -1.86265e-09,
-1.49217e-05, -2.70475e-05, -3.46936e-05, -3.68636e-05, -3.34419e-05, -2.51289e-05, -1.33533e-05,
1.86265e-09, 1.28802e-05, 2.33781e-05, 3.00081e-05, 3.19164e-05, 2.89697e-05, 2.17948e-05,
1.15884e-05, -1.86265e-09, -1.12038e-05, -2.03438e-05, -2.6131e-05, -2.78093e-05, -2.52593e-05,
-1.90102e-05, -1.01104e-05, 5.58794e-09, 9.79565e-06, 1.78032e-05, 2.28845e-05, 2.4383e-05,
2.21636e-05, 1.66893e-05, 8.89134e-06, -5.58794e-09, -8.61473e-06, -1.56574e-05, -2.01408e-05,
-2.14651e-05, -1.95242e-05, -1.47223e-05, -7.83801e-06, 5.58794e-09, 7.61449e-06, 1.38534e-05,
1.78264e-05, 1.8998e-05, 1.72965e-05, 1.30376e-05, 6.95605e-06, -1.86265e-09, -6.75768e-06,
-1.23046e-05, -1.58418e-05, -1.68998e-05, -1.53854e-05, -1.16117e-05, -6.18771e-06, 2.79397e-09,
6.01821e-06, 1.09617e-05, 1.41338e-05, 1.50781e-05, 1.37417e-05, 1.03731e-05, 5.53951e-06,
9.31323e-10, -5.38491e-06, -9.81987e-06, -1.26511e-05, -1.35079e-05, -1.23121e-05, -9.3095e-06,
-4.95836e-06, -5.58794e-09, 4.82984e-06, 8.80752e-06, 1.13593e-05, 1.21417e-05, 1.10669e-05,
8.36235e-06, 4.46104e-06, -7.45058e-09, -4.35486e-06, -7.95349e-06, -1.0239e-05, -1.09542e-05,
-9.97819e-06, -7.54744e-06, -4.02145e-06, -7.45058e-09, 3.9218e-06, 7.18981e-06, 9.25269e-06,
9.9102e-06, 9.02452e-06, 6.83218e-06, 3.65078e-06, 0, -3.54648e-06, -6.50622e-06,
-8.38935e-06, -8.9556e-06, -8.20123e-06, -6.20261e-06, -3.31923e-06, -5.58794e-09, 3.23448e-06,
5.89341e-06, 7.61449e-06, 8.14442e-06, 7.4422e-06, 5.63078e-06, 3.00072e-06, 4.65661e-09,
-2.94298e-06, -5.36814e-06, -6.94208e-06, -7.41892e-06, -6.77817e-06, -5.12227e-06, -2.7325e-06,
3.72529e-09, 2.68687e-06, 4.88851e-06, 6.33113e-06, 6.77258e-06, 6.18584e-06, 4.68083e-06,
2.50898e-06, 4.65661e-09, -2.4382e-06, -4.47594e-06, -5.76861e-06, -6.18212e-06, -5.64754e-06,
-4.27477e-06, -2.28919e-06, -3.72529e-09, 2.23238e-06, 4.09409e-06, 5.28432e-06, 5.65685e-06,
5.15953e-06, 3.91807e-06, 2.09454e-06, -1.86265e-09, -2.05077e-06, -3.75323e-06, -4.85033e-06,
-5.18188e-06, -4.74043e-06, -3.58745e-06, -1.93156e-06, -7.45058e-09, 1.87755e-06, 3.42913e-06,
4.43589e-06, 4.74136e-06, 4.33624e-06, 3.29781e-06, 1.76206e-06, 5.58794e-09, -1.72667e-06,
-3.15532e-06, -4.0736e-06, -4.37535e-06, -3.98606e-06, -3.03052e-06, -1.6205e-06, 0,
1.5879e-06, 2.88617e-06, 3.74485e-06, 3.99724e-06, 3.66755e-06, 2.76975e-06, 1.48918e-06,
-5.58794e-09, -1.44541e-06, -2.65241e-06, -3.42913e-06, -3.68059e-06, -3.35835e-06, -2.54624e-06,
-1.36159e-06, -1.86265e-09, 1.33645e-06, 2.43355e-06, 3.1665e-06, 3.37698e-06, 3.09199e-06,
2.33576e-06, 1.25542e-06, -1.86265e-09, -1.22562e-06, -2.24262e-06, -2.89455e-06, -3.1013e-06,
-2.83867e-06, -2.15136e-06, -1.15298e-06, 0, 1.11852e-06, 2.05357e-06, 2.65054e-06,
2.8424e-06, 2.5956e-06, 1.97161e-06, 1.05146e-06, -3.72529e-09, -1.03004e-06, -1.88313e-06,
-2.44752e-06, -2.60957e-06, -2.38232e-06, -1.81235e-06, -9.74163e-07, -5.58794e-09, 9.49018e-07,
1.73226e-06, 2.23145e-06, 2.39536e-06, 2.18488e-06, 1.66707e-06, 8.88482e-07, 0,
-8.67993e-07, -1.56462e-06, -2.03773e-06, -2.19047e-06, -2.00979e-06, -1.52178e-06, -8.06525e-07,
-7.45058e-09, 7.79517e-07, 1.44355e-06, 1.86265e-06, 1.99862e-06, 1.81794e-06, 1.38488e-06,
7.34814e-07, 2.79397e-09, -7.07805e-07, -1.30385e-06, -1.69128e-06, -1.80863e-06, -1.64658e-06,
-1.24983e-06, -6.59376e-07, 3.72529e-09, 6.64033e-07, 1.19396e-06, 1.54041e-06, 1.6503e-06,
1.51061e-06, 1.12504e-06, 6.10948e-07, 0, -5.94184e-07, -1.07102e-06, -1.38395e-06,
-1.4808e-06, -1.35042e-06, -1.01514e-06, -5.47618e-07, 7.45058e-09, 5.27129e-07, 9.66713e-07,
1.24704e-06, 1.329e-06, 1.20979e-06, 9.12696e-07, 4.8615e-07, 0, -4.71249e-07,
-8.60542e-07, -1.10641e-06, -1.17905e-06, -1.07847e-06, -8.10251e-07, -4.24683e-07, 0,
4.19095e-07, 7.72066e-07, 1.00117e-06, 1.04122e-06, 9.61125e-07, 7.07805e-07, 3.83705e-07,
-5.58794e-09, -3.68804e-07, -6.6869e-07, -8.47504e-07, -9.12696e-07, -8.15839e-07, -6.18398e-07,
-3.31551e-07, 1.86265e-09, 3.1665e-07, 5.65313e-07, 7.33882e-07, 7.73929e-07, 7.00355e-07,
5.22472e-07, 2.75671e-07, -7.45058e-09, -2.71946e-07, -4.88013e-07, -6.18398e-07, -6.55651e-07,
-5.90459e-07, -4.41447e-07, -2.36556e-07, -1.86265e-09, 2.19792e-07, 3.89293e-07, 4.94532e-07,
5.27129e-07, 4.67524e-07, 3.48315e-07, 1.75089e-07, 0, -1.71363e-07, -3.20375e-07,
-3.85568e-07, -4.06057e-07, -3.63216e-07, -2.70084e-07, -1.37836e-07, -5.58794e-09, 1.31316e-07,
2.20723e-07, 2.81259e-07, 2.89641e-07, 2.61702e-07, 1.80677e-07, 9.96515e-08, -1.86265e-09,
-8.00937e-08, -1.43424e-07, -1.65775e-07, -1.73226e-07, -1.39698e-07, -9.31323e-08, -3.72529e-08,
-1.30385e-08, 2.04891e-08, 8.47504e-08, 3.44589e-08, 5.96046e-08, -1.49012e-08, 1.86265e-08,
0};


# define N1 	8
int max_x_pulse = 512;





double poly_4_in_x_1(float *x, float *y, float pos_x)
{
	double tmp;

	tmp = (x[0] - x[1]) * (x[0] - x[2]) * (x[0] - x[3]);
	tmp = (tmp != 0) ? 1/tmp : 1;
	tmp *= y[0] * (pos_x - x[1]) * (pos_x - x[2]) * (pos_x - x[3]);
	return tmp;
}

double poly_4_in_x0(float *x, float *y, float pos_x)
{
	double tmp;

	tmp = (x[1] - x[0]) * (x[1] - x[2]) * (x[1] - x[3]);
	tmp = (tmp != 0) ? 1/tmp : 1;
	tmp *= y[1] * (pos_x - x[0]) * (pos_x - x[2]) * (pos_x - x[3]);
	return tmp;
}

double poly_4_in_x1(float *x, float *y, float pos_x)
{
	double tmp;

	tmp = (x[2] - x[0]) * (x[2] - x[1]) * (x[2] - x[3]);
	tmp = (tmp != 0) ? 1/tmp : 1;
	tmp *= y[2] * (pos_x - x[0]) * (pos_x - x[1]) * (pos_x - x[3]);
	return tmp;
}

double poly_4_in_x2(float *x, float *y, float pos_x)
{
	double tmp;


	tmp = (x[3] - x[0]) * (x[3] - x[1]) * (x[3] - x[2]);
	tmp = (tmp != 0) ? 1/tmp : 1;
	tmp *= y[3] * (pos_x - x[0]) * (pos_x - x[1]) * (pos_x - x[2]);
	return tmp;
}



double poly_3_in_x_1(float *x, float *y, float pos_x)
{
	double tmp;


	tmp = (x[0] - x[1]) * (x[0] - x[2]);
	tmp = (tmp != 0) ? 1/tmp : 1;
	tmp *= y[0] * (pos_x - x[1]) * (pos_x - x[2]);
	return tmp;
}

double poly_3_in_x0(float *x, float *y, float pos_x)
{
	double tmp;

	tmp = (x[1] - x[0]) * (x[1] - x[2]);
	tmp = (tmp != 0) ? 1/tmp : 1;
	tmp *= y[1] * (pos_x - x[0]) * (pos_x - x[2]);
	return tmp;

}

double poly_3_in_x1(float *x, float *y, float pos_x)
{
	double tmp;

	tmp = (x[2] - x[0]) * (x[2] - x[1]);
	tmp = (tmp != 0) ? 1/tmp : 1;
	tmp *= y[2] * (pos_x - x[0]) * (pos_x - x[1]);
	return tmp;
}

double poly_3_integrate_in_x_1(float *x, float *y, float pos_x)
{
	double tmp, tmp2, yp;


	tmp = (x[0] - x[1]) * (x[0] - x[2]);
	tmp = (tmp != 0) ? 1/tmp : 1;
	tmp2 = pos_x;
	yp = tmp2*x[1]*x[2];
	yp -= tmp2*tmp2*(x[1]+x[2])/2;
	yp += tmp2*tmp2*tmp2/3;
	yp *= y[0]*tmp;
	return yp;
}

double poly_3_integrate_in_x0(float *x, float *y, float pos_x)
{
	double tmp, tmp2, yp;

	tmp = (x[1] - x[0]) * (x[1] - x[2]);
	tmp = (tmp != 0) ? 1/tmp : 1;
	tmp2 = pos_x;
	yp = tmp2*x[0]*x[2];
	yp -= tmp2*tmp2*(x[0]+x[2])/2;
	yp += tmp2*tmp2*tmp2/3;
	yp *= y[1]*tmp;
	return yp;

}

double poly_3_integrate_in_x1(float *x, float *y, float pos_x)
{
	double tmp, tmp2, yp;

	tmp = (x[2] - x[0]) * (x[2] - x[1]);
	tmp = (tmp != 0) ? 1/tmp : 1;
	tmp2 = pos_x;
	yp = tmp2*x[0]*x[1];
	yp -= tmp2*tmp2*(x[0]+x[1])/2;
	yp += tmp2*tmp2*tmp2/3;
	yp *= y[2]*tmp;
	return yp;
}



double poly_2_in_x0(float *x, float *y, float pos_x)
{
	double tmp;


	tmp = (x[0] - x[1]);
	tmp = (tmp != 0) ? 1/tmp : 1;
	tmp *= y[0] * (pos_x - x[1]);
	return tmp;
}

double poly_2_in_x1(float *x, float *y, float pos_x)
{
	double tmp;

	tmp = (x[1] - x[0]);
	tmp = (tmp != 0) ? 1/tmp : 1;
	tmp *= y[1] * (pos_x - x[0]);
	return tmp;
}







float	pulse_half_size(float accuracy)
{
	register int i;

	if (accuracy < 0)	accuracy = -accuracy;
	for (i = N1/2; i < max_x_pulse && accuracy < fabs(y_pulse[i]); i += N1);
	return ((float)i)/N1;
}



float	pulse(float x)
{
	int ix;
	float y, dx;

	x = fabs(x);
	x *= N1;
	ix = (int)x;
	if (ix >= max_x_pulse-2)	return 0;
	dx = x - ix;
	y = -y_pulse[(ix-1) < 0 ? 1 : ix - 1]*(dx * (dx - 1) * (dx - 2))/6;
	y += y_pulse[ix]*((dx + 1) * (dx - 1) * (dx - 2))/2;
	y += -y_pulse[ix+1]*((dx + 1) * dx * (dx - 2))/2;
	y += y_pulse[ix+2]*((dx + 1) * dx * (dx - 1))/6;
	return y;

}

int find_index_in_ordered_array(float *x, int nx, float xp)
{
	register int i, ib, it;


	if (xp < x[0])		return 0;
	if (xp >= x[nx-1])	return nx-1;
	for (i = ib = 0, it = nx-1; it - ib > 1; )
	{
		i = (ib+it)/2;
		if (xp == x[i])
		{
			ib = i;
			break;
		}
		if (xp > x[i])		ib = i;
		else			it = i;
	}
	return ib;
}

/*	interpolate data point in a non-equally spaced array, the method uses
 *	a pulse response of width w_avg with a define accuracy. the w_arg
 *	parameter shoulb be at least two times the maximum points
 *	distance in the array. The array shoul be ordered in x.
 */

float interpolate_point_in_array(float *x, float *y, int nx, float xp, float w_avg, float accuracy)

{
	register int i, imin, imax;
	float extend, df, yp, wp, dx, p;


	if (x == NULL || nx <= 0)	return 0;
	if (w_avg <= 0)			return 0;

	df = 1/w_avg;
	extend = pulse_half_size(accuracy)*w_avg;
	imin = find_index_in_ordered_array(x, nx, xp-extend);
	imax = find_index_in_ordered_array(x, nx, xp+extend);

	for (i = imin, wp = yp = 0; i <= imax; i++)
	{
		dx = (x[i] - xp)*df;
		p = pulse(dx);
		yp += y[i]*p;
		wp += p;
	}
	yp = (wp != 0) ? yp/wp : yp;
	return yp;
}

float	find_max_width_between_succesive_point(float *x, int nx)
{

	register int i;
	float dx, d;

	if (x == NULL || nx <= 1)	return 0;
	for (i = 0, dx = fabs(x[1] - x[0]); i < nx - 1; i++)
	{
		d = x[i+1] - x[i];
		d = (d < 0) ? -d : d;
		dx = (d > dx) ? d : dx;
	}
	return dx;
}



int	interpol_point(void)
{
	register int i;
	pltreg *pr;
	O_p *op;
	d_s *dsi;
	float w, x = 0, y, y2, y4, y3;
	static float accu = 1e-3;

	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("Could not find plot data");

	w = find_max_width_between_succesive_point(dsi->xd, dsi->nx);


	i = win_scanf("polynomes/filtering interpolation of point at\nx value %f"
	"for filtering, smoothing of a width of %fwith an accuracy %f",&x,&w,&accu);

	if (i == WIN_CANCEL)	return 0;

	y = interpolate_point_in_array(dsi->xd, dsi->yd, dsi->nx, x, w, accu);
	y4 = interpolate_point_by_poly_4_in_array(dsi->xd, dsi->yd, dsi->nx, x);
	y3 = interpolate_point_by_poly_3_in_array(dsi->xd, dsi->yd, dsi->nx, x);
	y2 = interpolate_point_by_poly_2_in_array(dsi->xd, dsi->yd, dsi->nx, x);
	win_printf("Interpolation of point at x value = %g \n"
	"2^{nd} order polynome y = %g\n"
	"3^{nd} order polynome y = %g\n"
	"4^{nd} order polynome y = %g\n"
	"Using filtering leads y = %g\n"
	"width value %g accuracy %g",x,y2,y3,y4,y,w,accu);
	return D_O_K;
}

# ifdef EN_COURS
float interpolate_by_poly_3_and_integrate_in_array(float *x, float *y, int nx, float x0, float x1)
{
	register int  imin;
	float  yp;

	if (x == NULL || nx <= 0)	return 0;
	imin = find_index_in_ordered_array(x, nx, x0);


	imin = (imin <= 0) ? 0 : imin;
	imin = (imin > nx-4) ? nx-4 : imin;

	yp = poly_3_integrate_in_x_1(x + imin, y + imin, x[imin]);

	yp = poly_4_in_x_1(x + imin, y + imin, xp);
	yp += poly_4_in_x0(x + imin, y + imin, xp);
	yp += poly_4_in_x1(x + imin, y + imin, xp);
	yp += poly_4_in_x2(x + imin, y + imin, xp);

	return yp;
}


int	integrate_curve_between_2_points(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL;
	float w, x = 0, y, x0r, x1r;
	static float x0 = 0, x1 = 0;
	double I = 0;

	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("Could not find plot data");


	i = win_scanf("Integration with interpolation by a second order polynome "
		      "between point %12f and point %12f\n(in plot units)\n",&x0,&x1);

	if (i == WIN_CANCEL)	return 0;
	x0r = x0/op->dx;
	x1r = x1/op->dx;

	for (i = 1, x = 0; i < ds->nx && x >= 0; i++)
	  x = ds->xd[i] - dsi->xd[i-1];
	if (x < 0)  return win_printf_OK("Your data set is not sorted in x!");


	imin = find_index_in_ordered_array(dsi->xd, dsi->nx, x0r);
	imax = find_index_in_ordered_array(dsi->xd, dsi->nx, x1r);



	y = interpolate_point_in_array(dsi->xd, dsi->yd, dsi->nx, x, w, accu);
	y4 = interpolate_point_by_poly_4_in_array(dsi->xd, dsi->yd, dsi->nx, x);
	y3 = interpolate_point_by_poly_3_in_array(dsi->xd, dsi->yd, dsi->nx, x);
	y2 = interpolate_point_by_poly_2_in_array(dsi->xd, dsi->yd, dsi->nx, x);
	win_printf("Interpolation of point at x value = %g \n"
	"2^{nd} order polynome y = %g\n"
	"3^{nd} order polynome y = %g\n"
	"4^{nd} order polynome y = %g\n"
	"Using filtering leads y = %g\n"
	"width value %g accuracy %g",x,y2,y3,y4,y,w,accu);
	return D_O_K;
}

# endif
int	interpol_ds(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsic = NULL, *dsd = NULL;
	float w, maxx, minx, y, dx, x;
	int nx;
	static float accu = 1e-3;

	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("Could not find plot data");

	dsic = duplicate_data_set(dsi,NULL);
	sort_ds_along_x(dsic);

	w = find_max_width_between_succesive_point(dsic->xd, dsic->nx);
	maxx = dsic->xd[dsic->nx-1];
	minx = dsic->xd[0];
	nx = (int)(2*(maxx-minx)/w);
	i = win_scanf("interpolate points by filtering over the range\n"
	"min x value %f max x value %fresampling number of points %d"
	"smoothing width value %faccuracy %f",&minx,&maxx,&nx,&w,&accu);

	if (i == WIN_CANCEL)
	  {
	    free_data_set(dsic);
	    return 0;
	  }


	if ((dsd = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
		return win_printf("cannot create data set");

	for (i = 0, dx = (maxx-minx)/(nx-1), x = minx ; i < dsd->nx; i++, x += dx)
	{
		y = interpolate_point_in_array(dsic->xd, dsic->yd, dsic->nx, x, w, accu);
		dsd->xd[i] = x;
		dsd->yd[i] = y;
	}
	free_data_set(dsic);
	return refresh_plot(pr,pr->cur_op);
}
int	interpol_and_average_all_ds(void)
{
  register int i, k;
  pltreg *pr = NULL;
  O_p *op = NULL, *opd = NULL;
  d_s *dsi = NULL, *dsic = NULL, *dsd = NULL;
  float w, maxx, minx, y, dx, x, wm, maxmaxx, minminx;
  int nx;
  static float accu = 1e-3;

  if(updating_menu_state != 0)
    {
      if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      if (!op || op->n_dat < 2)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if(updating_menu_state != 0)	return D_O_K;

  if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
    return win_printf_OK("Could not find plot data");

  for (i = 0; i < op->n_dat; i++)
    {
      dsi = op->dat[i];
      dsic = duplicate_data_set(dsi,NULL);
      sort_ds_along_x(dsic);

      w = find_max_width_between_succesive_point(dsic->xd, dsic->nx);
      maxx = dsic->xd[dsic->nx-1];
      minx = dsic->xd[0];
      nx = (int)(2*(maxx-minx)/w);
      if (i == 0)
	{
	  maxmaxx = 0;
	  minminx = 0;
	  wm = 0;
	}
      maxmaxx += maxx;
      minminx += minx;
      wm += w;
      free_data_set(dsic);
    }
  if (i) wm /= i;
  if (i) maxmaxx /= i;
  if (i) minminx /= i;
  nx = (int)(2*(maxmaxx-minminx)/wm);

  i = win_scanf("Add all datasets after interpolating points by filtering.\n"
		"Achieve this over the range in x [%8f,  %8f]\n"
		"With a resampling number of points %8d\n"
		"smoothing in x over a width of %8f\n"
		"and an accuracy %8f",&minminx,&maxmaxx,&nx,&wm,&accu);

  if (i == WIN_CANCEL)	      return 0;


  if ((opd = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
    return win_printf("cannot create data set");
  dsd = opd->dat[0];

  for (k = 0; k < op->n_dat; k++)
    {
      dsi = op->dat[k];
      dsic = duplicate_data_set(dsi,NULL);
      sort_ds_along_x(dsic);
      for (i = 0, dx = (maxmaxx-minminx)/(nx-1), x = minminx ; i < dsd->nx; i++, x += dx)
	{
	  y = interpolate_point_in_array(dsic->xd, dsic->yd, dsic->nx, x, wm, accu);
	  dsd->yd[i] = y;
	}
      free_data_set(dsic);
      for (i = 0; i < dsd->nx; i++) dsd->xd[i] += dsd->yd[i];
    }
  for (i = 0, dx = (maxmaxx-minminx)/(nx-1), x = minminx ; i < dsd->nx; i++, x += dx)
    {
      dsd->yd[i] = dsd->xd[i] / op->n_dat;
      dsd->xd[i] = x;
    }
  set_ds_source(dsd,"Average %d data sets after smoothing over %g",op->n_dat,wm);
  opd->need_to_refresh = 1;

  opd->filename = Transfer_filename(op->filename);
  uns_op_2_op(opd, op);
  opd->dir = Mystrdup(op->dir);

  if (op->title != NULL) set_plot_title(opd, "%s", op->title);
  if (op->x_title != NULL) set_plot_x_title(opd, "%s", op->x_title);
  if (op->y_title != NULL) set_plot_y_title(opd, "%s", op->y_title);

  return refresh_plot(pr,pr->cur_op);
}

int do_FIR_and_resample_partial_dataset(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *ds = NULL;
	int start =0;
	int end =0;

	int rec_fir = 15;
	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("Could not find plot data");
	end = ds->nx;

	i = win_scanf("Choose starting %d and ending %d point rec_fir %d\n",&start,&end,&rec_fir);
    if (i == WIN_CANCEL) return 0;
	create_and_attach_one_ds(op,ds->nx,ds->ny,0);
	dsi = op->dat[op->n_dat-1];
	op->dat[op->n_dat-1] = FIR_and_resample_partial_dataset(ds,start,end,rec_fir);
	free(dsi);
	return 0;
}

d_s *FIR_and_resample_partial_dataset(d_s *dsz,int start,int end,int rec_fir)
{
	int i,j,k;
	if (dsz == NULL) return 0;
	d_s *dsztemp = NULL, *dszr = NULL;
	dsztemp = build_data_set(end-start+1,end-start+1);
	dszr = build_data_set(end-start+1,end-start+1);


	for (i = start, j = 0, k = 0, dsztemp->nx = dsztemp->ny = 0, add_new_point_to_ds(dsztemp, 0, 0); i < end; i++)
			{
					dsztemp->yd[j] += dsz->yd[i];
					dsztemp->xd[j] += dsz->xd[i];
					k++;

					if (k >= rec_fir)
							{
									dsztemp->yd[j] /= rec_fir;
									dsztemp->xd[j] /= rec_fir;
									j++;
									add_new_point_to_ds(dsztemp, 0, 0);
									//dsztemp->yd[j] = dsztemp->xd[j] = 0;
									k = 0;
							}
			}

	if (dsztemp->nx > 0)
			{
					dsztemp->nx = dsztemp->ny = dsztemp->nx - 1;
			}
	dszr = resample_by_poly_2_ds_no_verbose(dsztemp,dsz,start,end);
	free_data_set(dsztemp);
  return dszr;
}

d_s *remove_resampled_FIR(d_s *dsz, int start, int end, int rec_fir)
{
	d_s * dszr = NULL;
	dszr = FIR_and_resample_partial_dataset(dsz,start,end,rec_fir);
	if ((dszr->nx != dsz->nx) || (dszr->ny != dsz->ny))
	{
		win_printf("problem length with resampling");
	return NULL;
}
	for (int i = 0;i<dsz->nx;i++) 	dszr->yd[i] = dsz->yd[i] - dszr->yd[i];
	return dszr;
}


int do_remove_resample_fir(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *ds = NULL;
	int start =0;
	int end =0;

	int rec_fir = 15;
	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&ds) != 3)
		return win_printf_OK("Could not find plot data");
	end = ds->nx;

	i = win_scanf("Choose starting %d and ending %d point rec_fir %d\n",&start,&end,&rec_fir);
    if (i == WIN_CANCEL) return 0;
	create_and_attach_one_ds(op,ds->nx,ds->ny,0);
	dsi = op->dat[op->n_dat-1];
	op->dat[op->n_dat-1] = remove_resampled_FIR(ds,start,end,rec_fir);
	if (op->dat[op->n_dat-1] == NULL) op->dat[op->n_dat-1] = dsi;
	else{
			inherit_from_ds_to_ds(op->dat[op->n_dat-1],ds);
			op->dat[op->n_dat-1]->color = ds->color;
			free(dsi);
}
	return 0;
}

float interpolate_point_by_poly_4_in_array(float *x, float *y, int nx, float xp)
{
	register int  imin;
	float  yp;

	if (x == NULL || nx <= 0)	return 0;
	imin = find_index_in_ordered_array(x, nx, xp);

/*	imin--;*/
	imin = (imin <= 0) ? 0 : imin;
	imin = (imin > nx-4) ? nx-4 : imin;
	yp = poly_4_in_x_1(x + imin, y + imin, xp);
	yp += poly_4_in_x0(x + imin, y + imin, xp);
	yp += poly_4_in_x1(x + imin, y + imin, xp);
	yp += poly_4_in_x2(x + imin, y + imin, xp);
	return yp;
}

float interpolate_point_by_poly_3_in_array(float *x, float *y, int nx, float xp)
{
	register int  imin;
	float  yp;

	if (x == NULL || nx <= 0)	return 0;
	imin = find_index_in_ordered_array(x, nx, xp);
	imin = (imin <= 0) ? 0 : imin;
	imin = (imin > nx-3) ? nx-3 : imin;
	yp = poly_3_in_x_1(x + imin, y + imin, xp);
	yp += poly_3_in_x0(x + imin, y + imin, xp);
	yp += poly_3_in_x1(x + imin, y + imin, xp);
	return yp;
}

float interpolate_point_by_poly_2_in_array(float *x, float *y, int nx, float xp)
{
	register int  imin;
	float  yp;

	if (x == NULL || nx <= 0)	return 0;
	imin = find_index_in_ordered_array(x, nx, xp);
/*	imin--;*/
	imin = (imin <= 0) ? 0 : imin;
	imin = (imin > nx-2) ? nx-2 : imin;
	yp = poly_2_in_x0(x + imin, y + imin, xp);
	yp += poly_2_in_x1(x + imin, y + imin, xp);
	return yp;
}

int interpolate_array_by_poly_4_in_array(float *xi, float *yi, int nxi, float *xo, float *yo, int nxo, float xstart, float step)
{
	register int i;

	if (xo == NULL)		xo = (float*)calloc(nxo,sizeof(float));
	if (yo == NULL)		yo = (float*)calloc(nxo,sizeof(float));
	if (xi == NULL || yi == NULL || xo == NULL || yo == NULL)		return 1;
	for (i = 0; i < nxo; i++)
	{
		xo[i] = xstart + i * step;
		yo[i] = interpolate_point_by_poly_4_in_array(xi, yi, nxi, xo[i]);
	}
	return 0;
}

int	interpol_poly_4_ds(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsic = NULL, *dsd = NULL;
	float maxx, minx, y, x; // , dx
	int nx;

	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("Could not find plot data");

	dsic = duplicate_data_set(dsi,NULL);
	sort_ds_along_x(dsic);


	maxx = dsic->xd[dsic->nx-1];
	minx = dsic->xd[0];
	nx = dsic->nx;

	i = win_scanf("interpolate point\nmin x value %f max x value %f"
			"umber of points %d",&minx,&maxx,&nx);

	if (i == WIN_CANCEL)
	  {
	    free_data_set(dsic);
	    return 0;
	  }

	if ((dsd = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
		return win_printf("cannot create data set");

	for (i = 0, x = minx ; i < dsd->nx; i++, x = minx + (i*(maxx-minx))/(nx-1))	// , dx = (maxx-minx)/(nx-1)
	{
		y = interpolate_point_by_poly_4_in_array(dsic->xd, dsic->yd, dsic->nx, x);
		dsd->xd[i] = x;
		dsd->yd[i] = y;
	}
	free_data_set(dsic);
	return refresh_plot(pr,pr->cur_op);
}

int	interpol_poly_3_ds(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsic = NULL, *dsd = NULL;
	float maxx, minx, y, dx, x;
	int nx;

	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsic) != 3)
		return win_printf_OK("Could not find plot data");

	dsi = duplicate_data_set(dsic,NULL);
	sort_ds_along_x(dsi);

	maxx = dsi->xd[dsi->nx-1];
	minx = dsi->xd[0];
	nx = dsi->nx;
	i = win_scanf("interpolate point\nmin x value %f max x value %f"
					"number of points %d",&minx,&maxx,&nx);

	if (i == WIN_CANCEL)
	  {
	    free_data_set(dsi);
	    return 0;
	  }

	if ((dsd = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
		return win_printf("cannot create data set");

	for (i = 0, dx = (maxx-minx)/(nx-1), x = minx ; i < dsd->nx; i++, x += dx)
	{
		y = interpolate_point_by_poly_3_in_array(dsi->xd, dsi->yd, dsi->nx, x);
		dsd->xd[i] = x;
		dsd->yd[i] = y;
	}
	free_data_set(dsi);
	return refresh_plot(pr,pr->cur_op);
}

int	interpol_poly_2_ds(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsic = NULL, *dsd = NULL;
	float maxx, minx, y, dx, x;
	int nx;

	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsic) != 3)
		return win_printf_OK("Could not find plot data");

	dsi = duplicate_data_set(dsic,NULL);
	sort_ds_along_x(dsi);

	maxx = dsi->xd[dsi->nx-1];
	minx = dsi->xd[0];
	nx = dsi->nx;
	i = win_scanf("interpolate point\nmin x value %f max x value %f"
					"number of points %d",&minx,&maxx,&nx);
	if (i == WIN_CANCEL)
	  {
	    free_data_set(dsi);
	    return 0;
	  }

	if ((dsd = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
		return win_printf("cannot create data set");

	for (i = 0, dx = (maxx-minx)/(nx-1), x = minx ; i < dsd->nx; i++, x += dx)
	{
		y = interpolate_point_by_poly_2_in_array(dsi->xd, dsi->yd, dsi->nx, x);
		dsd->xd[i] = x;
		dsd->yd[i] = y;
	}
	free_data_set(dsi);
	return refresh_plot(pr,pr->cur_op);
}
# ifdef OLD
int	resample_by_poly_2_ds(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsic = NULL, *dsa = NULL, *dsd = NULL;
	int ndsi, ndsa;

	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("Could not find plot data");


	ndsi = op->cur_dat;
	ndsa = op->cur_dat + 1;
	ndsa = (ndsa >= op->n_dat) ? 0 : ndsa;

	i = win_scanf("resampling points of the current data set %d\n"
		"matching the absciss of a second data set %d",&ndsi,&ndsa);
	if (i == WIN_CANCEL)	return 0;
	if (ndsi >= op->n_dat || ndsi < 0) return win_printf("cannot create reach data set");
	if (ndsa >= op->n_dat || ndsa < 0) return win_printf("cannot create reach data set");
	dsa = op->dat[ndsa];
	dsic = op->dat[ndsi];

	if ((dsd = create_and_attach_one_ds(op, dsa->nx, dsa->nx, 0)) == NULL)
		return win_printf("cannot create data set");

	dsi = duplicate_data_set(dsic,NULL);
	sort_ds_along_x(dsi);

	for (i = 0; i < dsd->nx; i++)
	  dsd->yd[i] = interpolate_point_by_poly_2_in_array(dsi->xd, dsi->yd, dsi->nx, dsd->xd[i] = dsa->xd[i]);
	free_data_set(dsi);
	return refresh_plot(pr,pr->cur_op);
}


# endif

int	resample_by_poly_2_ds(void)
{
    register int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsic = NULL, *dsa = NULL, *dsd = NULL;
    static int ndsi = 0, ndsa = 1;
    static int idss = 0, idse = 1;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
      return win_printf_OK("Could not find plot data");

    ndsi = op->cur_dat;
    ndsa = op->cur_dat + 1;
    ndsa = (ndsa >= op->n_dat) ? 0 : ndsa;

    i = win_scanf("resampling points of data set in [%4d,%4d]\n"
		  "matching the absciss of a second data set %4d",&idss,&idse,&ndsa);
    if (i == WIN_CANCEL)	return 0;

    for(ndsi = idss; ndsi <= idse; ndsi++)
      {
          if (ndsi >= op->n_dat || ndsi < 0)
              return win_printf("cannot create reach data set");
          if (ndsa >= op->n_dat || ndsa < 0)
              return win_printf("cannot create reach data set");
          dsa = op->dat[ndsa];
          dsic = op->dat[ndsi];

          if ((dsd = create_and_attach_one_ds(op, dsa->nx, dsa->nx, 0)) == NULL)
              return win_printf("cannot create data set");
          dsi = duplicate_data_set(dsic,NULL);
          sort_ds_along_x(dsi);
          for (i = 0; i < dsd->nx; i++)
              dsd->yd[i] = interpolate_point_by_poly_2_in_array(dsi->xd, dsi->yd, dsi->nx, dsd->xd[i] = dsa->xd[i]);
          free_data_set(dsi);
      }
    return refresh_plot(pr,pr->cur_op);
}
d_s *resample_by_poly_2_ds_no_verbose(d_s *dsic,d_s *dsa,int start, int end)
{
	register int i;
	d_s *dsi = NULL, *dsd = NULL;




	if ((dsd = build_data_set( dsa->nx, dsa->nx )) == NULL)
		return win_printf_ptr("cannot create data set");
	dsi = duplicate_data_set(dsic,NULL);
	sort_ds_along_x(dsi);
	dsd->nx=dsd->ny=0;
	for (i = start; i < end; i++)
	  add_new_point_to_ds(dsd,dsa->xd[i],interpolate_point_by_poly_2_in_array(dsi->xd, dsi->yd, dsi->nx, dsd->xd[i] = dsa->xd[i]));
	free_data_set(dsi);
	return dsd;
}
int	resample_by_poly_3_ds(void)
{
    register int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsic = NULL, *dsa = NULL, *dsd = NULL;
    static int ndsi = 0, ndsa = 1;
    static int idss = 0, idse = 1;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
      return win_printf_OK("Could not find plot data");

    ndsi = op->cur_dat;
    ndsa = op->cur_dat + 1;
    ndsa = (ndsa >= op->n_dat) ? 0 : ndsa;

    i = win_scanf("resampling points of data set in [%4d,%4d]\n"
		  "matching the absciss of a second data set %4d",&idss,&idse,&ndsa);
    if (i == WIN_CANCEL)	return 0;

    for(ndsi = idss; ndsi <= idse; ndsi++)
      {
	if (ndsi >= op->n_dat || ndsi < 0)
	  return win_printf("cannot create reach data set");
	if (ndsa >= op->n_dat || ndsa < 0)
	  return win_printf("cannot create reach data set");
	dsa = op->dat[ndsa];
	dsic = op->dat[ndsi];

	if ((dsd = create_and_attach_one_ds(op, dsa->nx, dsa->nx, 0)) == NULL)
	  return win_printf("cannot create data set");

	dsi = duplicate_data_set(dsic,NULL);
	sort_ds_along_x(dsi);
	for (i = 0; i < dsd->nx; i++)
	  dsd->yd[i] = interpolate_point_by_poly_3_in_array(dsi->xd, dsi->yd, dsi->nx, dsd->xd[i] = dsa->xd[i]);
	free_data_set(dsi);
      }
    return refresh_plot(pr,pr->cur_op);
}
int	resample_by_poly_4_ds(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsic = NULL, *dsa = NULL, *dsd = NULL;
	int ndsi, ndsa;


	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("Could not find plot data");


	ndsi = op->cur_dat;
	ndsa = op->cur_dat + 1;
	ndsa = (ndsa >= op->n_dat) ? 0 : ndsa;

	i = win_scanf("resampling points of the current data set %d\n"
		"matching the absciss of a second data set %d",&ndsi,&ndsa);
	if (i == WIN_CANCEL)	return 0;
	if (ndsi >= op->n_dat || ndsi < 0) return win_printf("cannot create reach data set");
	if (ndsa >= op->n_dat || ndsa < 0) return win_printf("cannot create reach data set");
	dsa = op->dat[ndsa];
	dsic = op->dat[ndsi];

	if ((dsd = create_and_attach_one_ds(op, dsa->nx, dsa->nx, 0)) == NULL)
		return win_printf("cannot create data set");

	dsi = duplicate_data_set(dsic,NULL);
	sort_ds_along_x(dsi);
	for (i = 0; i < dsd->nx; i++)
	  dsd->yd[i] = interpolate_point_by_poly_4_in_array(dsi->xd, dsi->yd, dsi->nx, dsd->xd[i] = dsa->xd[i]);
	free_data_set(dsi);
	return refresh_plot(pr,pr->cur_op);
}




int  normalize_all_ds_by_one_ertrapolated(void)
{
  register int i, j;
  pltreg *pr = NULL;
  O_p *op = NULL, *opd = NULL;
  d_s *dsi1 = NULL, *dsi2 = NULL;
  char question[1024] = {0};
  int ids1, ids2;
  float x, y;
  static int interp = 0;

  if(updating_menu_state != 0)
    {
      if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      if (!op || op->n_dat < 2)
	active_menu->flags |=  D_DISABLED;
      else active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine performs a normalisation operation\n"
			 "of all datasets by the selected dataset of the plot.\n"
			 "Points Y coordinate is nemormalized by extrapolation.\n");


  if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi1) != 3)
    return win_printf_OK("cannot find data!");
  if (op->n_dat < 2)
    return (win_printf_OK("No enought data set!\n2 datasets are required!"));


  ids2 = op->cur_dat;
  if (op->n_dat > 24)
    {
      i = win_scanf("This routine performs a normalisation operation\n"
		    "of all datasets by the selected dataset of the plot.\n"
		    "Points Y coordinate is nemormalized by extrapolation.\n"
		    "The results is placed in a new plot\n"
		    "Do interpolation by a polynome of order %R1, %r2, %r2\n"
		    "Specify the Data set to substract %8d\n",&interp,&ids2);
      if (i == WIN_CANCEL) return D_O_K;
    }
  else
    {
      snprintf(question,sizeof(question),"This routine performs a normalisation operation\n"
	       "of all datasets by the selected dataset of the plot.\n"
	       "Points Y coordinate is nemormalized by extrapolation.\n"
	       "The results is placed in a new plot\n"
	       "Do interpolation by a polynome of order %%R1, %%r2, %%r2\n"
	       "Choose Data set to substract:\n");
      for(i = 0; i < op->n_dat; i++)
	{
	  if (i == 0)    snprintf(question+strlen(question),sizeof(question)-strlen(question),"%d->%%R ",i);
	  else           snprintf(question+strlen(question),sizeof(question)-strlen(question),"%d->%%r ",i);
	  if (i%10 == 9) snprintf(question+strlen(question),sizeof(question)-strlen(question),"\n");
	}
      i = win_scanf(question,&interp,&ids2);
      if (i == WIN_CANCEL)       return D_O_K;
    }
  if (ids2 < 0 || ids2 >= op->n_dat)
    return win_printf_OK("Data set 2 index %d out of range [0, %d[!",ids2,op->n_dat);

  opd = duplicate_plot_data(op, NULL);
  if (opd == NULL) return win_printf_OK("cannot duplicate plot!");
  add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opd);
  for (ids1 = 0, dsi2 = opd->dat[ids2]; ids1 < opd->n_dat; ids1++)
    {
      dsi1 = opd->dat[ids1];
      if (ids1 == ids2) continue;
      dsi2 = opd->dat[ids2];
      for (j = 0; j < dsi1->nx && j < dsi2->nx; j++)
	{
	  x = dsi1->xd[j];
	  if (interp == 0)       y = interpolate_point_by_poly_2_in_array(dsi2->xd, dsi2->yd, dsi2->nx, x);
	  else if (interp == 1)  y = interpolate_point_by_poly_3_in_array(dsi2->xd, dsi2->yd, dsi2->nx, x);
	  else if (interp == 2)  y = interpolate_point_by_poly_4_in_array(dsi2->xd, dsi2->yd, dsi2->nx, x);
	  else y = 0;
	  dsi1->yd[j] = (y != 0) ? dsi1->yd[j]/y : dsi1->yd[j];
	  if (dsi1->ye != NULL && dsi2->ye != NULL)
	      dsi1->ye[j] = sqrt(dsi1->ye[j]*dsi1->ye[j] + dsi2->ye[j]*dsi2->ye[j]);
	  set_ds_treatement(dsi1,"Normalized data set %d -> %s",ids2,(dsi2->source)?dsi2->source:"No source");
	}
      dsi1->nx = dsi1->ny = j;
    }
  opd->need_to_refresh = 1;
  remove_ds_from_op(opd, dsi2);
  return refresh_plot(pr, pr->n_op-1);
}





int	resample_ds(void)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsa = NULL, *dsd = NULL, *dst = NULL;
	float w;
	static float accu = 1e-3;
	static int xory = 0;
	int  ndsi, ndsa;

	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf_OK("Could not find plot data");


	ndsi = op->cur_dat;
	ndsa = op->cur_dat + 1;
	ndsa = (ndsa >= op->n_dat) ? 0 : ndsa;

	i = win_scanf("resampling points of the current data set %4d\n"
		"matching the %R absciss or %r ordonate \nof a second data set %4d",&ndsi,&xory, &ndsa);
	if (i == WIN_CANCEL)	return 0;
	if (ndsi >= op->n_dat || ndsi < 0) return win_printf("cannot create reach data set");
	if (ndsa >= op->n_dat || ndsa < 0) return win_printf("cannot create reach data set");
	dsa = op->dat[ndsa];
	dsi = op->dat[ndsi];
	dst  = duplicate_data_set(dsi, NULL);
	if (xory == 1)
	  {
	    QuickSort_double(dst->yd, dst->xd, 0, dst->nx-1);
	    w = find_max_width_between_succesive_point(dst->yd, dst->nx);
	  }
	else
	  {
	    QuickSort_double(dst->xd, dst->yd, 0, dst->nx-1);
	    w = find_max_width_between_succesive_point(dst->xd, dst->nx);
	  }

	i = win_scanf("interpolate points by filtering \n"
	"smoothing width value %faccuracy %f",&w,&accu);
	if (i == WIN_CANCEL)	return 0;

	if ((dsd = create_and_attach_one_ds(op, dsa->nx, dsa->nx, 0)) == NULL)
		return win_printf("cannot create data set");

	if (xory == 1)
	  {
	    for (i = 0; i < dsd->nx; i++)
	      dsd->xd[i] = interpolate_point_in_array(dst->yd, dst->xd, dst->nx, dsd->yd[i] = dsa->yd[i],w, accu);
	  }
	else
	  {
	    for (i = 0; i < dsd->nx; i++)
	      dsd->yd[i] = interpolate_point_in_array(dst->xd, dst->yd, dst->nx, dsd->xd[i] = dsa->xd[i],w, accu);
	  }
	free_data_set(dst);
	return refresh_plot(pr,pr->cur_op);
}







#define IA 16807
#define IM1 2147483647
#define AM (1.0/IM1)
#define IQ 127773
#define IR 2836
#define NTAB 32
#define NDIV (1+(IM1-1)/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)


# ifdef EXE

float ran1(int *idum)
{
	int j;
	long k;
	static long iy=0;
	static long iv[NTAB] = {0};
	float temp;

	if (*idum <= 0 || !iy) {
		if (-(*idum) < 1) *idum=1;
		else *idum = -(*idum);
		for (j=NTAB+7;j>=0;j--) {
			k=(*idum)/IQ;
			*idum=IA*(*idum-k*IQ)-IR*k;
			if (*idum < 0) *idum += IM1;
			if (j < NTAB) iv[j] = *idum;
		}
		iy=iv[0];
	}
	k=(*idum)/IQ;
	*idum=IA*(*idum-k*IQ)-IR*k;
	if (*idum < 0) *idum += IM1;
	j=iy/NDIV;
	iy=iv[j];
	iv[j] = *idum;
	if ((temp=AM*iy) > RNMX) return RNMX;
	else return temp;
}
# endif

#undef IA
#undef IM1
#undef AM
#undef IQ
#undef IR
#undef NTAB
#undef NDIV
#undef EPS
#undef RNMX

# ifdef EXE
float gasdev(int *idum)
{
	float ran1(int *idum);
	static int iset=0;
	static float gset;
	float fac,rsq,v1,v2;

	if  (iset == 0) {
		do {
			v1=2.0*ran1(idum)-1.0;
			v2=2.0*ran1(idum)-1.0;
			rsq=v1*v1+v2*v2;
		} while (rsq >= 1.0 || rsq == 0.0);
		fac=sqrt(-2.0*log(rsq)/rsq);
		gset=v1*fac;
		iset=1;
		return v2*fac;
	} else {
		iset=0;
		return gset;
	}
}

int do_add_gaussian_noise(char ch)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	int  nx;
	static float amp = .1;
	static long idum = 0;

	if (ch != CR) 		return 0;
	if (last_m_event.kbstat & GR_KB_SHIFT)
	{
		win_justified_printf("This routine add a random gaussian noise to an existing data set, this data set is thus modified");

	}
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
	if ( i != 3)		return win_printf("cannot find data");

	nx = dsi->nx;
	i = win_scanf("Gaussian noise added with amplitude %fseed %dnumber of points in data set %d",&amp,&idum,&nx);
	if (i == WIN_CANCEL)	return 0;


	dsd = create_and_attach_one_ds(op, nx,nx, 0);
	if (dsd == NULL)		return win_printf("Cannot allocate plot");


	for (i = 0; i < nx; i++)
	{
		dsd->xd[i] = (i < dsi->nx) ? dsi->xd[i] : i;
		dsd->yd[i] = ((i < dsi->nx) ? dsi->yd[i] : 0 ) + amp*gasdev(&idum);
	}
	dsd->treatement = my_sprintf(dsd->treatement,"gaussian noise added %g amp",amp);
	refresh_plot(pr, UNCHANGED);
	switch_plot(0);
	switch_data_set(0);
	return 0;
}
int do_sin(char ch)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	int  nx;

	if (ch != CR) 		return 0;
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
	if ( i != 3)		return win_printf("cannot find data");




	nx = dsi->nx;
	dsd = create_and_attach_one_ds(op, nx,nx, 0);
	if (dsd == NULL)		return win_printf("Cannot allocate plot");


	for (i = 0; i < nx; i++)
	{
		dsd->xd[i] = dsi->xd[i];
		dsd->yd[i] = sin(dsi->yd[i]);
	}
	dsd->treatement = my_sprintf(dsd->treatement,"sinus taken");
	refresh_plot(pr, UNCHANGED);
	switch_plot(0);
	switch_data_set(0);
	return 0;
}
int do_cos(char ch)
{
	register int i;
	pltreg *pr = NULL;
	O_p *op = NULL;
	d_s *dsi = NULL, *dsd = NULL;
	int  nx;

	if (ch != CR) 		return 0;
	i = ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi);
	if ( i != 3)		return win_printf("cannot find data");


	nx = dsi->nx;
	dsd = create_and_attach_one_ds(op, nx,nx, 0);
	if (dsd == NULL)		return win_printf("Cannot allocate plot");


	for (i = 0; i < nx; i++)
	{
		dsd->xd[i] = dsi->xd[i];
		dsd->yd[i] = cos(dsi->yd[i]);
	}
	dsd->treatement = my_sprintf(dsd->treatement,"cosinus taken");
	refresh_plot(pr, UNCHANGED);
	switch_plot(0);
	switch_data_set(0);
	return 0;
}
# endif







MENU *interpol_plot_menu(void)
{
	static MENU mn[32];


	if (mn[0].text != NULL)	return mn;

	add_item_to_menu(mn,"single point", interpol_point,NULL,0,NULL);
	add_item_to_menu(mn,"data set smoothing", interpol_ds,NULL,0,NULL);
	add_item_to_menu(mn,"average and smooth datasets", interpol_and_average_all_ds,NULL,0,NULL);
	add_item_to_menu(mn,"Normalize all ds by one", normalize_all_ds_by_one_ertrapolated,NULL,0,NULL);
	add_item_to_menu(mn,"data set by 3rd. poly.", interpol_poly_4_ds,NULL,0,NULL);
	add_item_to_menu(mn,"data set by 2rd. poly.", interpol_poly_3_ds,NULL,0,NULL);
	add_item_to_menu(mn,"data set by 1rd. poly.", interpol_poly_2_ds,NULL,0,NULL);
	add_item_to_menu(mn,"resampling smoothing", resample_ds,NULL,0,NULL);
	add_item_to_menu(mn,"resampling by 3rd. poly.", resample_by_poly_4_ds,NULL,0,NULL);
	add_item_to_menu(mn,"resampling by 2rd. poly.", resample_by_poly_3_ds,NULL,0,NULL);
	add_item_to_menu(mn,"resampling by 1rd. poly.", resample_by_poly_2_ds,NULL,0,NULL);
  add_item_to_menu(mn,"FIR and resample", do_FIR_and_resample_partial_dataset,NULL,0,NULL);
	add_item_to_menu(mn,"Remove resampled fir",do_remove_resample_fir,NULL,0,NULL);


# ifdef NOT_HERE

	add_menu_item(ac, "noise added", do_add_gaussian_noise, ONE_BOUNCE,0);
	add_menu_item(ac, "sin(y)", do_sin, ONE_BOUNCE,0);
	add_menu_item(ac, "cos(y)", do_cos, ONE_BOUNCE,0);


	add_sub_menu(ac_pl,"Interpolation",ac);
# endif

	return mn;
}

int		interpol_main(void)
{
	MENU *mn = NULL;
	mn = interpol_plot_menu();
	add_plot_treat_menu_item("Interpolation", NULL, mn, 0, NULL);
	return WIN_OK;
}
