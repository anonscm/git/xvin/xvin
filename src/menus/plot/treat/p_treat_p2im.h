/********************************************************************/
/* p_treat_p2im.h										        	*/
/* operations transforming plots into images            		    */
/********************************************************************/


// functions defined in p_treat_p2im.c:
int do_plot_to_im(void);				/* merge all datasets into an image	*/
