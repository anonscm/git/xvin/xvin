/********************************************************************/
/* plot_treat_basic.h                                               */
/* basic data manipulation and treatments for dataset(s)            */
/********************************************************************/

# include <stdlib.h>
# include <string.h>
#include <float.h> //DB sandard from c lib
#include "platform.h"
#include "xvin.h"
#include "p_treat_basic.h"



/************************************************************************************************/
int do_swap_x_y(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return D_O_K;
    }

    if (swap_x_y_in_plot(op) != 0)
    {
        return (win_printf_OK("problem !\nIcannot swap x and y in this plot!"));
    }

    op->need_to_refresh = 1;
    return (refresh_plot(pr, UNCHANGED));
}


/************************************************************************************************/
int cheatting(void)
{
    int k;
    int  index, n_start, n_end;
    pltreg *pr = NULL;
    d_s *ds = NULL;
    O_p *op = NULL;
    bool remove_inside = false;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (active_menu->dp == NULL)
    {
        return D_O_K;
    }

    index = active_menu->flags & 0xFFFFFF00; // one ds or all ds ?

    if (strstr((const char *) active_menu->dp, "INVISIBLE") != NULL)
    {
        remove_inside = false;
    }
    else if (strstr((const char *) active_menu->dp, "VISIBLE") != NULL)
    {
        remove_inside = true;
    }
    else
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)
    {
        return D_O_K;
    }

    if (index & DS_ONE)  // we work on active dataset
    {
        n_start = op->cur_dat;
        n_end   = op->cur_dat + 1;
    }
    else if (index & DS_ALL)  // we work on all datasets
    {
        n_start = 0;
        n_end   = op->n_dat;
    }
    else
    {
        return (D_O_K);
    }

// win_printf("index = %d = %x\ni will work on datasets in [%d , %d[", index, index, n_start, n_end);

    for (k = n_end - 1; k >= n_start; k--)
    {
        ds = op->dat[k]; // we work on dataset k
        remove_point_using_range(op, &ds, op->x_lo, op->x_hi, op->y_lo, op->y_hi, remove_inside);
    }

    if (remove_inside && (index & DS_ALL))
    {
        auto_x_limit();
        auto_y_limit();

    }
    op->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
}

/*
// Those function should be inlined by the compilator
bool is_point_inside_range(float x, float y, float range_x_min, float range_x_max,
                             float range_y_min, float range_y_max)
{
    return (x >= range_x_max || x < range_x_min || y >= range_y_max || y < range_y_min);
}

bool is_point_outside_range(float x, float y, float range_x_min, float range_x_max,
                             float range_y_min, float range_y_max)
{
    return !is_point_inside_range(x, y, range_x_min, range_x_max, range_y_min, range_y_max);
}
*/

int remove_point_using_range(O_p *src_op, d_s **src_ds_ptr,
                             float range_x_min, float range_x_max,
                             float range_y_min, float range_y_max,
                             bool remove_inside)
{
    d_s *src_ds = *src_ds_ptr;
    int min = (src_ds->nx > src_ds->ny) ? src_ds->nx : src_ds->ny;
    int n_keep = 0;
    float *x = NULL;
    float *y = NULL;
    float *xe = NULL;
    float *ye = NULL;
    float *xed = NULL;
    float *yed = NULL;
    float *xbd = NULL;
    float *xbu = NULL;
    float *ybd = NULL;
    float *ybu = NULL;

    if (remove_inside)
    {
        for (int i = min - 1 ; i >= 0 ; i--)
        {
            if (src_ds->xd[i] >= range_x_max || src_ds->xd[i] < range_x_min
                    || src_ds->yd[i] >= range_y_max || src_ds->yd[i] < range_y_min)
            {
                n_keep++;
            }
        }
    }
    else
    {
        for (int i = min - 1 ; i >= 0 ; i--)
        {
            if (src_ds->xd[i] < range_x_max && src_ds->xd[i] >= range_x_min
                    && src_ds->yd[i] < range_y_max && src_ds->yd[i] >= range_y_min)
            {
                n_keep++;
            }
        }
    }

// win_printf("on dataset %d, i will keep %d points", k, n_keep);
    if (n_keep <= 0)
    {
        remove_one_plot_data(src_op, IS_DATA_SET, (void *)src_ds);
        *src_ds_ptr = NULL;
        //  n_end--; // there is one dataset less in the plot
    }
    else
    {
        x = (float *)calloc(n_keep, sizeof(float));
        y = (float *)calloc(n_keep, sizeof(float));

        if (src_ds->xe != NULL)
        {
            xe = (float *)calloc(n_keep, sizeof(float));
        }
        if (src_ds->xed != NULL)
        {
            xed = (float *)calloc(n_keep, sizeof(float));
        }
        if (src_ds->xbd != NULL)
        {
            xbd = (float *)calloc(n_keep, sizeof(float));
        }
        if (src_ds->xbu != NULL)
        {
            xbu = (float *)calloc(n_keep, sizeof(float));
        }

        if (src_ds->ye != NULL)
        {
            ye = (float *)calloc(n_keep, sizeof(float));
        }
        if (src_ds->yed != NULL)
        {
            yed = (float *)calloc(n_keep, sizeof(float));
        }
        if (src_ds->ybd != NULL)
        {
            ybd = (float *)calloc(n_keep, sizeof(float));
        }
        if (src_ds->ybu != NULL)
        {
            ybu = (float *)calloc(n_keep, sizeof(float));
        }

        if (remove_inside)
        {
            for (int i = min - 1, k = n_keep - 1 ; i >= 0 ; i--)
            {
                if (src_ds->xd[i] >= range_x_max || src_ds->xd[i] < range_x_min
                        || src_ds->yd[i] >= range_y_max || src_ds->yd[i] < range_y_min)
                {
                    x[k] = src_ds->xd[i];

                    if (src_ds->xe != NULL)
                    {
                        xe[k] = src_ds->xe[i];
                    }
                    if (src_ds->xed != NULL)
                    {
                        xed[k] = src_ds->xed[i];
                    }
                    if (src_ds->xbd != NULL)
                    {
                        xbd[k] = src_ds->xbd[i];
                    }
                    if (src_ds->xbu != NULL)
                    {
                        xbu[k] = src_ds->xbu[i];
                    }


                    if (src_ds->ye != NULL)
                    {
                        ye[k] = src_ds->ye[i];
                    }
                    if (src_ds->yed != NULL)
                    {
                        yed[k] = src_ds->yed[i];
                    }
                    if (src_ds->ybd != NULL)
                    {
                        ybd[k] = src_ds->ybd[i];
                    }
                    if (src_ds->xbu != NULL)
                    {
                        ybu[k] = src_ds->ybu[i];
                    }

                    y[k--] = src_ds->yd[i];
                }
            }
        }
        else
        {
            for (int i = min - 1, k = n_keep - 1  ; i >= 0 ; i--)
            {
                if (src_ds->xd[i] < range_x_max && src_ds->xd[i] >= range_x_min
                        && src_ds->yd[i] < range_y_max && src_ds->yd[i] >= range_y_min)
                {
                    x[k] = src_ds->xd[i];

                    if (src_ds->xe != NULL)
                    {
                        xe[k] = src_ds->xe[i];
                    }
                    if (src_ds->xed != NULL)
                    {
                        xed[k] = src_ds->xed[i];
                    }
                    if (src_ds->xbd != NULL)
                    {
                        xbd[k] = src_ds->xbd[i];
                    }
                    if (src_ds->xbu != NULL)
                    {
                        xbu[k] = src_ds->xbu[i];
                    }

                    if (src_ds->ye != NULL)
                    {
                        ye[k] = src_ds->ye[i];
                    }
                    if (src_ds->yed != NULL)
                    {
                        yed[k] = src_ds->yed[i];
                    }
                    if (src_ds->ybd != NULL)
                    {
                        ybd[k] = src_ds->ybd[i];
                    }
                    if (src_ds->ybu != NULL)
                    {
                        ybu[k] = src_ds->ybu[i];
                    }

                    y[k--] = src_ds->yd[i];
                }
            }
        }

        if (src_ds->xd)
        {
            free(src_ds->xd);
        }

        if (src_ds->yd)
        {
            free(src_ds->yd);
        }

        src_ds->xd = x;
        src_ds->yd = y;

        if (src_ds->xe != NULL)
        {
            free(src_ds->xe);
            src_ds->xe = xe;
        }
        if (src_ds->xed != NULL)
        {
            free(src_ds->xed);
            src_ds->xed = xed;
        }
        if (src_ds->xbd != NULL)
        {
            free(src_ds->xbd);
            src_ds->xbd = xbd;
        }
        if (src_ds->xbu != NULL)
        {
            free(src_ds->xbu);
            src_ds->xbu = xbu;
        }


        if (src_ds->ye != NULL)
        {
            free(src_ds->ye);
            src_ds->ye = ye;
        }
        if (src_ds->yed != NULL)
        {
            free(src_ds->yed);
            src_ds->yed = yed;
        }
        if (src_ds->ybd != NULL)
        {
            free(src_ds->ybd);
            src_ds->ybd = ybd;
        }
        if (src_ds->ybu != NULL)
        {
            free(src_ds->ybu);
            src_ds->ybu = ybu;
        }

        src_ds->mx = src_ds->my = src_ds->nx = src_ds->ny = n_keep;
    }

     src_op->need_to_refresh = 1;
    return D_O_K;
}

int remove_point_using_x_range(O_p *src_op, d_s **src_ds_ptr,
                             float range_x_min, float range_x_max,
                             bool remove_inside)
{
    d_s *src_ds = *src_ds_ptr;
    int min = (src_ds->nx > src_ds->ny) ? src_ds->nx : src_ds->ny;
    int n_keep = 0;
    float *x = NULL;
    float *y = NULL;
    float *xe = NULL;
    float *ye = NULL;
    float *xed = NULL;
    float *yed = NULL;
    float *xbd = NULL;
    float *xbu = NULL;
    float *ybd = NULL;
    float *ybu = NULL;

    if (remove_inside)
    {
        for (int i = min - 1 ; i >= 0 ; i--)
        {
            if (src_ds->xd[i] >= range_x_max || src_ds->xd[i] < range_x_min)
            {
                n_keep++;
            }
        }
    }
    else
    {
        for (int i = min - 1 ; i >= 0 ; i--)
        {
            if (src_ds->xd[i] < range_x_max && src_ds->xd[i] >= range_x_min)
            {
                n_keep++;
            }
        }
    }

// win_printf("on dataset %d, i will keep %d points", k, n_keep);
    if (n_keep <= 0)
    {
        remove_one_plot_data(src_op, IS_DATA_SET, (void *)src_ds);
        *src_ds_ptr = NULL;
        //  n_end--; // there is one dataset less in the plot
    }
    else
    {
        x = (float *)calloc(n_keep, sizeof(float));
        y = (float *)calloc(n_keep, sizeof(float));

        if (src_ds->xe != NULL)
        {
            xe = (float *)calloc(n_keep, sizeof(float));
        }
        if (src_ds->xed != NULL)
        {
            xed = (float *)calloc(n_keep, sizeof(float));
        }
        if (src_ds->xbd != NULL)
        {
            xbd = (float *)calloc(n_keep, sizeof(float));
        }
        if (src_ds->xbu != NULL)
        {
            xbu = (float *)calloc(n_keep, sizeof(float));
        }

        if (src_ds->ye != NULL)
        {
            ye = (float *)calloc(n_keep, sizeof(float));
        }
        if (src_ds->yed != NULL)
        {
            yed = (float *)calloc(n_keep, sizeof(float));
        }
        if (src_ds->ybd != NULL)
        {
            ybd = (float *)calloc(n_keep, sizeof(float));
        }
        if (src_ds->ybu != NULL)
        {
            ybu = (float *)calloc(n_keep, sizeof(float));
        }

        if (remove_inside)
        {
            for (int i = min - 1, k = n_keep - 1 ; i >= 0 ; i--)
            {
                if (src_ds->xd[i] >= range_x_max || src_ds->xd[i] < range_x_min)
                {
                    x[k] = src_ds->xd[i];

                    if (src_ds->xe != NULL)
                    {
                        xe[k] = src_ds->xe[i];
                    }
                    if (src_ds->xed != NULL)
                    {
                        xed[k] = src_ds->xed[i];
                    }
                    if (src_ds->xbd != NULL)
                    {
                        xbd[k] = src_ds->xbd[i];
                    }
                    if (src_ds->xbu != NULL)
                    {
                        xbu[k] = src_ds->xbu[i];
                    }


                    if (src_ds->ye != NULL)
                    {
                        ye[k] = src_ds->ye[i];
                    }
                    if (src_ds->yed != NULL)
                    {
                        yed[k] = src_ds->yed[i];
                    }
                    if (src_ds->ybd != NULL)
                    {
                        ybd[k] = src_ds->ybd[i];
                    }
                    if (src_ds->xbu != NULL)
                    {
                        ybu[k] = src_ds->ybu[i];
                    }

                    y[k--] = src_ds->yd[i];
                }
            }
        }
        else
        {
            for (int i = min - 1, k = n_keep - 1  ; i >= 0 ; i--)
            {
                if (src_ds->xd[i] < range_x_max && src_ds->xd[i] >= range_x_min)
                {
                    x[k] = src_ds->xd[i];

                    if (src_ds->xe != NULL)
                    {
                        xe[k] = src_ds->xe[i];
                    }
                    if (src_ds->xed != NULL)
                    {
                        xed[k] = src_ds->xed[i];
                    }
                    if (src_ds->xbd != NULL)
                    {
                        xbd[k] = src_ds->xbd[i];
                    }
                    if (src_ds->xbu != NULL)
                    {
                        xbu[k] = src_ds->xbu[i];
                    }

                    if (src_ds->ye != NULL)
                    {
                        ye[k] = src_ds->ye[i];
                    }
                    if (src_ds->yed != NULL)
                    {
                        yed[k] = src_ds->yed[i];
                    }
                    if (src_ds->ybd != NULL)
                    {
                        ybd[k] = src_ds->ybd[i];
                    }
                    if (src_ds->ybu != NULL)
                    {
                        ybu[k] = src_ds->ybu[i];
                    }

                    y[k--] = src_ds->yd[i];
                }
            }
        }

        if (src_ds->xd)
        {
            free(src_ds->xd);
        }

        if (src_ds->yd)
        {
            free(src_ds->yd);
        }

        src_ds->xd = x;
        src_ds->yd = y;

        if (src_ds->xe != NULL)
        {
            free(src_ds->xe);
            src_ds->xe = xe;
        }
        if (src_ds->xed != NULL)
        {
            free(src_ds->xed);
            src_ds->xed = xed;
        }
        if (src_ds->xbd != NULL)
        {
            free(src_ds->xbd);
            src_ds->xbd = xbd;
        }
        if (src_ds->xbu != NULL)
        {
            free(src_ds->xbu);
            src_ds->xbu = xbu;
        }


        if (src_ds->ye != NULL)
        {
            free(src_ds->ye);
            src_ds->ye = ye;
        }
        if (src_ds->yed != NULL)
        {
            free(src_ds->yed);
            src_ds->yed = yed;
        }
        if (src_ds->ybd != NULL)
        {
            free(src_ds->ybd);
            src_ds->ybd = ybd;
        }
        if (src_ds->ybu != NULL)
        {
            free(src_ds->ybu);
            src_ds->ybu = ybu;
        }

        src_ds->mx = src_ds->my = src_ds->nx = src_ds->ny = n_keep;
    }

     src_op->need_to_refresh = 1;
    return D_O_K;
}



int remove_ds_with_color(void)
{
    pltreg *pr = NULL;
    d_s *ds = NULL;
    O_p *op = NULL;
    int index = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (active_menu->dp == NULL)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return D_O_K;
    }

    if (strstr((const char *) active_menu->dp, "INVISIBLE") != NULL)
    {
        index |= INVISIBLE;
    }
    else if (strstr((const char *) active_menu->dp, "VISIBLE") != NULL)
    {
        index |= VISIBLE;
    }
    else
    {
        return D_O_K;
    }

    int color = color_chooser();
    int i = 0;

    while (i < op->n_dat)
    {
        ds = op->dat[i];    // we work on dataset k

        if ((index & VISIBLE && ds->color == color) || (index & INVISIBLE && ds->color != color))
        {
            if (op->n_dat > 1)
            {
                remove_one_plot_data(op, IS_DATA_SET, (void *)ds);
            }
            else
            {
                remove_data(pr, IS_ONE_PLOT, (void *)op);
                op = NULL;
                break;
            }
        }
        else
        {
            ++i;
        }
    }

    if (op)
    {
        op->need_to_refresh = 1;
        return refresh_plot(pr, UNCHANGED);
    }
    else
    {
        return refresh_plot(pr, 0);
    }
}

int remove_ds_with_invisible_points(void)
{
    int i,  k;
    int min, n_out, index, n_start, n_end, nds_to_rm;
    pltreg *pr = NULL;
    d_s *ds = NULL;
    O_p *op = NULL;
    int color = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (active_menu->dp == NULL)
    {
        return D_O_K;
    }

    index = active_menu->flags & 0xFFFFFF00; // one ds or all ds ?

    if (strstr((const char *) active_menu->dp, "INVISIBLE") != NULL)
    {
        index |= INVISIBLE;
    }
    else if (strstr((const char *) active_menu->dp, "VISIBLE") != NULL)
    {
        index |= VISIBLE;
    }
    else
    {
        return D_O_K;
    }

    if (strstr((const char *) active_menu->dp, "COLOR") != NULL)
    {
        index |= DS_COLOR;
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return D_O_K;
    }

    if (index & DS_ONE)  // we work on active dataset
    {
        n_start = op->cur_dat;
        n_end   = op->cur_dat + 1;
    }
    else if (index & DS_ALL)  // we work on all datasets
    {
        n_start = 0;
        n_end   = op->n_dat;
    }
    else
    {
        return (D_O_K);
    }

    if (index & DS_COLOR)
    {
        color = color_chooser();
    }

    for (k = n_end - 1, nds_to_rm = 0; k >= n_start; k--)
    {
        ds = op->dat[k]; // we work on dataset k
        min = (ds->nx > ds->ny) ? ds->nx : ds->ny;
        n_out = 0;

        if (index & INVISIBLE)
        {
            for (i = min - 1 ; i >= 0 && n_out == 0; i--)
            {
                if (ds->xd[i] >= op->x_hi || ds->xd[i] < op->x_lo
                        || ds->yd[i] >= op->y_hi || ds->yd[i] < op->y_lo)
                {
                    n_out++;
                }
            }
        }
        else if (index & VISIBLE)
        {
            for (i = min - 1 ; i >= 0 ; i--)
            {
                if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo
                        && ds->yd[i] < op->y_hi && ds->yd[i] >= op->y_lo)
                {
                    n_out++;
                }
            }
        }
        else
        {
            return (OFF);
        }

        if (n_out > 0 && op->n_dat > 1)
        {
            nds_to_rm++;
        }
    }

    if ((op->n_dat - nds_to_rm) < 1)
    {
        if (!(index & DS_COLOR))
        {
            i = win_printf("You will remove all data sets of the current plot\nDo you really want to do so ?");
	    if (i == WIN_CANCEL)
	      {
		return D_O_K;
	      }
        }


    }

    for (k = n_end - 1; k >= n_start; k--)
    {
        ds = op->dat[k]; // we work on dataset k
        min = (ds->nx > ds->ny) ? ds->nx : ds->ny;
        n_out = 0;

        if (index & INVISIBLE)
        {
            for (i = min - 1 ; i >= 0 && n_out == 0; i--)
            {
                if (ds->xd[i] >= op->x_hi || ds->xd[i] < op->x_lo
                        || ds->yd[i] >= op->y_hi || ds->yd[i] < op->y_lo)
                {
                    n_out++;
                }
            }
        }
        else if (index & VISIBLE)
        {
            for (i = min - 1 ; i >= 0 ; i--)
            {
                if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo
                        && ds->yd[i] < op->y_hi && ds->yd[i] >= op->y_lo)
                {
                    n_out++;
                }
            }
        }
        else
        {
            return (OFF);
        }

        if (n_out > 0 && op->n_dat > 1)
        {
            if (index & DS_COLOR)
            {
                ds->color = color;
            }
            else
            {
                remove_one_plot_data(op, IS_DATA_SET, (void *)ds);
            }
        }
        else if (n_out > 0 && op->n_dat == 1)
        {
            if (index & DS_COLOR)
            {
                ds->color = color;
            }

            {
                i = win_printf("You will remove all data sets that is\nyou will remove the plot");

                if (i != WIN_CANCEL)
                {
                    remove_data(pr, IS_ONE_PLOT, (void *)op);
                }
            }
        }
    }

    op->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
}

int remove_ds_with_un_or_visible_points_with_large_derivative(void)
{
    int i,  k;
    int min, n_out, n_start, n_end, nds_to_rm;
    pltreg *pr = NULL;
    d_s *ds = NULL;
    O_p *op = NULL;
    static int all = 0, unvisi = 0;
    static float thres = 1, thresu = 1;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return D_O_K;
    }

    i = win_scanf("Remove from plot data set(s) with visible/invisible points\n"
                  "having a large derivative\n"
                  "Select Visible->%R or Invisible %r\n"
                  "Treat the current data set->%R or all->%r\n"
                  "Define the absolute value of the threshold above witch\n"
                  "high deivative is detected %8f\n", &unvisi, &all, &thresu);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    if (all == 0)  // we work on active dataset
    {
        n_start = op->cur_dat;
        n_end   = op->cur_dat + 1;
    }
    else   // we work on all datasets
    {
        n_start = 0;
        n_end   = op->n_dat;
    }

    thres = (op->dy != 0) ? thresu / op->dy : thresu;

    for (k = n_end - 1, nds_to_rm = 0; k >= n_start; k--)
    {
        ds = op->dat[k]; // we work on dataset k
        min = (ds->nx > ds->ny) ? ds->nx : ds->ny;
        n_out = 0;

        if (unvisi)
        {
            for (i = min - 2 ; i >= 0 && n_out == 0; i--)
            {
                if ((ds->xd[i] >= op->x_hi || ds->xd[i] < op->x_lo || ds->yd[i] >= op->y_hi
                        || ds->yd[i] < op->y_lo) && (fabs(ds->yd[i + 1] - ds->yd[i]) > thres))
                {
                    n_out++;
                }
            }
        }
        else
        {
            for (i = min - 2 ; i >= 0 ; i--)
            {
                if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo && ds->yd[i] < op->y_hi
                        && ds->yd[i] >= op->y_lo && (fabs(ds->yd[i + 1] - ds->yd[i]) > thres))
                {
                    n_out++;
                }
            }
        }

        if (n_out > 0 && op->n_dat > 1)
        {
            nds_to_rm++;
        }
    }

    if ((op->n_dat - nds_to_rm) < 1)
    {
        i = win_printf("You will remove all data sets of the current plot\nDo you really want to do so ?");

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }
    }

    for (k = n_end - 1; k >= n_start; k--)
    {
        ds = op->dat[k]; // we work on dataset k
        min = (ds->nx > ds->ny) ? ds->nx : ds->ny;
        n_out = 0;

        if (unvisi)
        {
            for (i = min - 2 ; i >= 0 && n_out == 0; i--)
            {
                if ((ds->xd[i] >= op->x_hi || ds->xd[i] < op->x_lo || ds->yd[i] >= op->y_hi
                        || ds->yd[i] < op->y_lo) && (fabs(ds->yd[i + 1] - ds->yd[i]) > thres))
                {
                    n_out++;
                }
            }
        }
        else
        {
            for (i = min - 2 ; i >= 0 ; i--)
            {
                if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo && ds->yd[i] < op->y_hi
                        && ds->yd[i] >= op->y_lo && (fabs(ds->yd[i + 1] - ds->yd[i]) > thres))
                {
                    n_out++;
                }
            }
        }

        if (n_out > 0 && op->n_dat > 1)
        {
            remove_one_plot_data(op, IS_DATA_SET, (void *)ds);
        }
        else if (n_out > 0 && op->n_dat == 1)
        {
            i = win_printf("You will remove all data sets that is\nyou will remove the plot");

            if (i != WIN_CANCEL)
            {
                remove_data(pr, IS_ONE_PLOT, (void *)op);
            }
        }
    }

    op->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
}
int remove_ds_with_all_invisible_points(void)
{
    int i, k;
    int min, n_out, index, n_start, n_end, nds_to_rm = 0;
    pltreg *pr = NULL;
    d_s *ds = NULL;
    O_p *op = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (active_menu->dp == NULL)
    {
        return D_O_K;
    }

    index = active_menu->flags & 0xFFFFFF00; // one ds or all ds ?

    if (strstr((const char *) active_menu->dp, "INVISIBLE") != NULL)
    {
        index |= INVISIBLE;
    }
    else if (strstr((const char *) active_menu->dp, "VISIBLE") != NULL)
    {
        index |= VISIBLE;
    }
    else
    {
        return D_O_K;
    }

    //win_printf ("entering dp %s index %x",active_menu->dp,index);
    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return D_O_K;
    }

    if (index & DS_ONE)  // we work on active dataset
    {
        n_start = op->cur_dat;
        n_end   = op->cur_dat + 1;
    }
    else if (index & DS_ALL)  // we work on all datasets
    {
        n_start = 0;
        n_end   = op->n_dat;
    }
    else
    {
        return win_printf_OK("not all ds nor one");
    }

// win_printf("index = %d = %x\ni will work on datasets in [%d , %d[", index, index, n_start, n_end);
    op->need_to_refresh = 1;

    for (k = n_end - 1, nds_to_rm = 0; k >= n_start; k--)
    {
        ds = op->dat[k]; // we work on dataset k
        min = (ds->nx > ds->ny) ? ds->nx : ds->ny;
        n_out = 0;

        if (index & INVISIBLE)
        {
            for (i = min - 1 ; i >= 0; i--)
            {
                if (ds->xd[i] >= op->x_hi || ds->xd[i] < op->x_lo
                        || ds->yd[i] >= op->y_hi || ds->yd[i] < op->y_lo)
                {
                    n_out++;
                }
            }
        }
        else if (index & VISIBLE)
        {
            for (i = min - 1 ; i >= 0 ; i--)
            {
                if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo
                        && ds->yd[i] < op->y_hi && ds->yd[i] >= op->y_lo)
                {
                    n_out++;
                }
            }
        }
        else
        {
            return (OFF);
        }

        if (n_out == min && op->n_dat > 1)
        {
            nds_to_rm++;
        }
    }

    if ((op->n_dat - nds_to_rm) < 1)
    {
        i = win_printf("You will remove all data sets of the current plot\nDo you really want to do so ?");

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }
    }

    for (k = n_end - 1; k >= n_start; k--)
    {
        ds = op->dat[k]; // we work on dataset k
        min = (ds->nx > ds->ny) ? ds->nx : ds->ny;
        n_out = 0;

        if (index & INVISIBLE)
        {
            for (i = min - 1 ; i >= 0; i--)
            {
                if (ds->xd[i] >= op->x_hi || ds->xd[i] < op->x_lo
                        || ds->yd[i] >= op->y_hi || ds->yd[i] < op->y_lo)
                {
                    n_out++;
                }
            }
        }
        else if (index & VISIBLE)
        {
            for (i = min - 1 ; i >= 0 ; i--)
            {
                if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo
                        && ds->yd[i] < op->y_hi && ds->yd[i] >= op->y_lo)
                {
                    n_out++;
                }
            }
        }
        else
        {
            return (OFF);
        }

        if (n_out == min && op->n_dat > 1)
        {
            remove_one_plot_data(op, IS_DATA_SET, (void *)ds);
        }
        else if (n_out == min && op->n_dat == 1)
        {
            i = win_printf("You will remove all data sets that is\nyou will remove the plot");

            if (i != WIN_CANCEL)
            {
                remove_data(pr, IS_ONE_PLOT, (void *)op);
            }
        }
    }

    return refresh_plot(pr, UNCHANGED);
}
/************************************************************************************************/
int changing_value(void)
{
    int i, k;
    int min, index, n_start, n_end;
    pltreg *pr = NULL;
    d_s *ds = NULL, *dsd = NULL;
    O_p *op = NULL;
    float vald; // val in dataset raw format
    static float val = 0;
    static int mode = 0;
    char question[2048] = {0};

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (active_menu->dp == NULL)
    {
        return D_O_K;
    }

    index = active_menu->flags & 0xFFFFFF00; // one ds or all ds ?
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)
    {
        return D_O_K;
    }
    if (strstr((const char *) active_menu->dp, "INVISIBLE") != NULL)
    {
        index |= INVISIBLE;
	snprintf(question,sizeof(question),"Modifying the value of Invisible points\n"
		 "Specify if you want to:\n"
		 "%%R replace the Y values or \n"
		 "%%radd an offet to y values\n"
		 "of invisible points\n"
		 "That is points with Y values out of [%g,%g[\n"
		 "Specify this value or offet %%8f\n"
		 ,op->ay+op->dy*op->y_lo,op->ay+op->dy*op->y_hi);
        i = win_scanf(question, &mode, &val);
        if (i == WIN_CANCEL)
        {
            return 0;
        }
    }
    else if (strstr((const char *) active_menu->dp, "VISIBLE") != NULL)
    {
        index |= VISIBLE;
	snprintf(question,sizeof(question),"Modifying the value of Visible points\n"
		 "Specify if you want to:\n"
		 "%%R replace the Y values or \n"
		 "%%radd an offet to y values\n"
		 "of invisible points\n"
		 "That is points with Y values in of [%g,%g[\n"
		 "Specify this value or offet %%8f\n"
		 ,op->ay+op->dy*op->y_lo,op->ay+op->dy*op->y_hi);
        i = win_scanf(question, &mode, &val);
        if (i == WIN_CANCEL)
        {
            return 0;
        }
    }
    else
    {
        return D_O_K;
    }

    vald = (op->dy != 0) ? (val - op->ay)/op->dy : (val - op->ay);

    if (index & DS_ONE)  // we work on active dataset
    {
        n_start = op->cur_dat;
        n_end   = op->cur_dat + 1;
    }
    else if (index & DS_ALL)  // we work on all datasets
    {
        n_start = 0;
        n_end   = op->n_dat;
    }
    else
    {
        return (D_O_K);
    }

    for (k = n_start; k < n_end; k++)
    {
        ds = op->dat[k]; // we work on dataset k
        min = (ds->nx > ds->ny) ? ds->nx : ds->ny;

        if ((dsd = create_and_attach_one_ds(op, min, min, 0)) == NULL)
        {
            return win_printf_OK("cannot create plot !");
        }
        duplicate_data_set(ds, dsd);

        if (index & INVISIBLE)
        {
            for (i = min - 1 ; i >= 0 ; i--)
            {
                if (ds->xd[i] >= op->x_hi || ds->xd[i] < op->x_lo
                        || ds->yd[i] >= op->y_hi || ds->yd[i] < op->y_lo)
                {
                    dsd->yd[i] = (mode) ? dsd->yd[i] + vald : vald;
                }
            }
        }
        else if (index & VISIBLE)
        {
            for (i = min - 1; i >= 0 ; i--)
            {
                if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo
                        && ds->yd[i] < op->y_hi && ds->yd[i] >= op->y_lo)
                {
                    dsd->yd[i] = (mode) ? dsd->yd[i] + vald : vald;
                }
            }
        }
    }
    op->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
}
# ifdef NOTUSED
int offseting_value(void)
{
    int i, k;
    int min, index, n_start, n_end;
    pltreg *pr = NULL;
    d_s *ds = NULL, *dsd = NULL;
    O_p *op = NULL;
    static float val = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (active_menu->dp == NULL)
    {
        return D_O_K;
    }

    index = active_menu->flags & 0xFFFFFF00; // one ds or all ds ?

    if (strstr(active_menu->dp, "INVISIBLE") != NULL)
    {
        index |= INVISIBLE;
        i = win_scanf("what Y value do you want to add\nInvisible points %20f\n", &val);

        if (i == WIN_CANCEL)
        {
            return 0;
        }
    }
    else if (strstr(active_menu->dp, "VISIBLE") != NULL)
    {
        index |= VISIBLE;
        i = win_scanf("what Y value do you want to add to\nVisible points %20f\n", &val);

        if (i == WIN_CANCEL)
        {
            return 0;
        }
    }
    else
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)
    {
        return D_O_K;
    }

    if (index & DS_ONE)  // we work on active dataset
    {
        n_start = op->cur_dat;
        n_end   = op->cur_dat + 1;
    }
    else if (index & DS_ALL)  // we work on all datasets
    {
        n_start = 0;
        n_end   = op->n_dat;
    }
    else
    {
        return (D_O_K);
    }

    for (k = n_start; k < n_end; k++)
    {
        ds = op->dat[k]; // we work on dataset k
        min = (ds->nx > ds->ny) ? ds->nx : ds->ny;

        if ((dsd = create_and_attach_one_ds(op, min, min, 0)) == NULL)
        {
            return win_printf_OK("cannot create plot !");
        }

        duplicate_data_set(ds, dsd);

        if (index & INVISIBLE)
        {
            for (i = min - 1 ; i >= 0 ; i--)
            {
                if (ds->xd[i] >= op->x_hi || ds->xd[i] < op->x_lo
                        || ds->yd[i] >= op->y_hi || ds->yd[i] < op->y_lo)
                {
                    dsd->yd[i] += val;
                }
            }
        }
        else if (index & VISIBLE)
        {
            for (i = min - 1; i >= 0 ; i--)
            {
                if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo
                        && ds->yd[i] < op->y_hi && ds->yd[i] >= op->y_lo)
                {
                    dsd->yd[i] += val;
                }
            }
        }
    }

    op->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
}
# endif
/****************************************************************/
/* quicksort algorithm :                        */
/* provides an extremely efficient method of sorting arrays in  */
/* memory.                                                      */
/*                                                              */
/* QUICKSORT sorts elements in the array A with indices between */
/* LO and HI (both inclusive). Note that the QUICKSORT          */
/* procedure executes itself recursively.                       */
/*                                                              */
/* how to call it :                                             */
/*      QuickSort(a,Lo,Hi);                                     */
/****************************************************************/
int QuickSort(float *a, int l, int r)
{
    int  i, j;
    float         x, y;
    i = l;
    j = r;
    x = a[(int)((l + r) / 2) ];

    while (i < j)
    {
        while (a[i] < x)
        {
            i = i + 1;
        }

        while (x < a[j])
        {
            j = j - 1;
        }

        if (i <= j)
        {
            y = a[i];
            a[i] = a[j];
            a[j] = y;
            i = i + 1;
            j = j - 1;
        }
    }/*while*/

    if (l < j)
    {
        QuickSort(a, l, j);
    }

    if (i < r)
    {
        QuickSort(a, i, r);
    }

    return 0;
}
/****************************************************************/
/* this function operates on a double array,                    */
/* sorting with the first one                                   */
/*                                                              */
/****************************************************************/
int QuickSort_double(float *xd, float *yd, int l, int r)
{
    int  i, j;
    float         x, y;
    i = l;
    j = r;
    x = xd[(int)((l + r) / 2) ];

    while (i < j)
    {
        while (xd[i] < x)
        {
            i += 1;
        }

        while (x < xd[j])
        {
            j -= 1;
        }

        if (i <= j)
        {
            y = xd[i];
            xd[i] = xd[j];
            xd[j] = y;
            y = yd[i];
            yd[i] = yd[j];
            yd[j] = y;
            i += 1;
            j -= 1;
        }
    }/*while*/

    if (l < j)
    {
        QuickSort_double(xd, yd, l, j);
    }

    if (i < r)
    {
        QuickSort_double(xd, yd, i, r);
    }

    return 0;
} /* end of the "QuickSort_double" function */
/****************************************************************/
/* this function operates on a triple array,                    */
/* sorting with the first one                                   */
/*                                                              */
/****************************************************************/
int QuickSort_triple(float *xd, float *yd, float *ye, int l, int r)
{
    int  i, j;
    float         x, y;
    i = l;
    j = r;
    x = xd[(int)((l + r) / 2) ];

    while (i < j)
    {
        while (xd[i] < x)
        {
            i += 1;
        }

        while (x < xd[j])
        {
            j -= 1;
        }

        if (i <= j)
        {
            y = xd[i];
            xd[i] = xd[j];
            xd[j] = y;
            y = yd[i];
            yd[i] = yd[j];
            yd[j] = y;
            y = ye[i];
            ye[i] = ye[j];
            ye[j] = y;
            i += 1;
            j -= 1;
        }
    }/*while*/

    if (l < j)
    {
        QuickSort_triple(xd, yd, ye, l, j);
    }

    if (i < r)
    {
        QuickSort_triple(xd, yd, ye, i, r);
    }

    return 0;
} /* end of the "QuickSort_triple" function */
/****************************************************************/
/* to sort a data set                                           */
/****************************************************************/
int ds_sort(void)
{
    int j;
    pltreg      *pr = NULL;
    O_p         *op = NULL;
    d_s         *ds = NULL, *dsi = NULL; /* dsi is the source     */
    /* ds is the destination     */
    int         npts;
    int         index;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine sorts a data set.\n"
                             " \">\" means ascending order\n"
                             " \"<\" means reverse order\n"
                             "A new dataset is created.");
    }

    index = (active_menu->flags & 0xFFFFFF00);

    /* we first find the data that we need to transform */
    if ((index != ASCENDING) && (index != DESCENDING))
    {
        return win_printf_OK("Function called in a bad way !");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf_OK("cannot find data");
    }

    npts = dsi->nx;     /* the number of points in the data set */

    if ((ds = create_and_attach_one_ds(op, npts, npts, 0)) == NULL)
    {
        return win_printf("I can't create dataset !");
    }

    if (dsi->ye != NULL)
        if ((ds = alloc_data_set_y_error(ds)) == NULL)
        {
            return win_printf("I can't allocate y error!");
        }
    if (dsi->yed != NULL)
        if ((ds = alloc_data_set_y_down_error(ds)) == NULL)
        {
            return win_printf("I can't allocate y error!");
        }
    if (dsi->ybu != NULL)
        if ((ds = alloc_data_set_y_box_up(ds)) == NULL)
        {
            return win_printf("I can't allocate y error!");
        }
    if (dsi->ybd != NULL)
        if ((ds = alloc_data_set_y_box_dwn(ds)) == NULL)
        {
            return win_printf("I can't allocate y error!");
        }


    if (dsi->xe != NULL)
        if ((ds = alloc_data_set_x_error(ds)) == NULL)
        {
            return win_printf("I can't allocate x error!");
        }
    if (dsi->xed != NULL)
        if ((ds = alloc_data_set_x_down_error(ds)) == NULL)
        {
            return win_printf("I can't allocate x error!");
        }
    if (dsi->xbu != NULL)
        if ((ds = alloc_data_set_x_box_up(ds)) == NULL)
        {
            return win_printf("I can't allocate x error!");
        }
    if (dsi->xbd != NULL)
        if ((ds = alloc_data_set_x_box_dwn(ds)) == NULL)
        {
            return win_printf("I can't allocate x error!");
        }


    if (index == ASCENDING)   for (j = 0; j < npts; j++)
        {
            ds->yd[j] = dsi->yd[j];
            ds->xd[j] = dsi->xd[j];

            if (ds->ye != NULL)
            {
                ds->ye[j] = dsi->ye[j];
            }
            if (ds->yed != NULL)
            {
                ds->yed[j] = dsi->yed[j];
            }
            if (ds->ybu != NULL)
            {
                ds->ybu[j] = dsi->ybu[j];
            }
            if (ds->ybd != NULL)
            {
                ds->ybd[j] = dsi->ybd[j];
            }

            if (ds->xe != NULL)
            {
                ds->xe[j] = dsi->xe[j];
            }
            if (ds->xed != NULL)
            {
                ds->xed[j] = dsi->xed[j];
            }
            if (ds->xbu != NULL)
            {
                ds->xbu[j] = dsi->xbu[j];
            }
            if (ds->xbd != NULL)
            {
                ds->xbd[j] = dsi->xbd[j];
            }
        }
    else            for (j = 0; j < npts; j++)
        {
            ds->yd[j] = dsi->yd[j];
            ds->xd[j] = -dsi->xd[j];

            if (ds->ye != NULL)
            {
                ds->ye[j] = dsi->ye[j];
            }
            if (ds->yed != NULL)
            {
                ds->yed[j] = dsi->yed[j];
            }
            if (ds->ybu != NULL)
            {
                ds->ybu[j] = dsi->ybu[j];
            }
            if (ds->ybd != NULL)
            {
                ds->ybd[j] = dsi->ybd[j];
            }


            if (ds->xe != NULL)
            {
                ds->xe[j] = dsi->xe[j];
            }
            if (ds->xed != NULL)
            {
                ds->xed[j] = dsi->xed[j];
            }
            if (ds->xbu != NULL)
            {
                ds->xbu[j] = dsi->xbu[j];
            }
            if (ds->xbd != NULL)
            {
                ds->xbd[j] = dsi->xbd[j];
            }

        }

    if (ds->ye != NULL)
      {   // need to treat other errors
        QuickSort_triple(ds->xd, ds->yd, ds->ye, 0, npts - 1);
    }
    else
    {
        QuickSort_double(ds->xd, ds->yd, 0, npts - 1);
    }

    if (index == DESCENDING)  for (j = 0; j < npts; j++)
        {
            ds->xd[j] = -dsi->xd[j];
        }

    /* now we must do some house keeping */
    inherit_from_ds_to_ds(ds, dsi);
    set_ds_treatement(ds, "sorted");
    op->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
} /* end of the "ds_sort" function */
/************************************************************************************************/
// definitions for sorting a dataset:
#define NRANSI
#define SWAP(a,b) temp1=(a);(a)=(b);(b)=temp1;
#define M 7
#define NSTACK 256
/************************************************************************************************/
int     sort_ds_along_x(d_s *ds)
{
    unsigned long n;
    float *arr = NULL,  *brr = NULL, *earr = NULL, *ebrr = NULL;
    unsigned long i, ir, j, k, l = 1;
    int *is = NULL, *istack = NULL, jstack = 0;
    float a, b, ea = 0, eb = 0, temp1;

    if (ds->nx < 1 || ds->ny < 1)
    {
        return 1;
    }

    is = (int *)calloc(NSTACK, sizeof(int));
    istack = is - 1;
    arr = ds->xd - 1;

    if (ds->xe != NULL)
    {
        earr = ds->xe - 1;
    }

    brr = ds->yd - 1;

    if (ds->ye != NULL)
    {
        ebrr = ds->ye - 1;
    }

    n = ds->nx;
    ir = n;
    l = 1;

    for (;;)
    {
        if (ir - l < M)
        {
            for (j = l + 1; j <= ir; j++)
            {
                a = arr[j];
                b = brr[j];
                ea = (earr != NULL) ? earr[j] : 0;
                eb = (ebrr != NULL) ? ebrr[j] : 0;

                for (i = j - 1; i >= 1; i--)
                {
                    if (arr[i] <= a)
                    {
                        break;
                    }

                    arr[i + 1] = arr[i];

                    if (earr != NULL)
                    {
                        earr[i + 1] = earr[i];
                    }

                    brr[i + 1] = brr[i];

                    if (ebrr != NULL)
                    {
                        ebrr[i + 1] = ebrr[i];
                    }
                }

                arr[i + 1] = a;

                if (earr != NULL)
                {
                    earr[i + 1] = ea;
                }

                brr[i + 1] = b;

                if (ebrr != NULL)
                {
                    ebrr[i + 1] = eb;
                }
            }

            if (!jstack)
            {
                if (is)
                {
                    free(is);
                }

                return 0;
            }

            ir = istack[jstack];
            l = istack[jstack - 1];
            jstack -= 2;
        }
        else
        {
            k = (l + ir) >> 1;
            SWAP(arr[k], arr[l + 1]);

            if (earr != NULL)
            {
                SWAP(earr[k], earr[l + 1])
            }

            SWAP(brr[k], brr[l + 1]);

            if (ebrr != NULL)
            {
                SWAP(ebrr[k], ebrr[l + 1])
            }

            if (arr[l + 1] > arr[ir])
            {
                SWAP(arr[l + 1], arr[ir]);

                if (earr != NULL)
                {
                    SWAP(earr[l + 1], earr[ir])
                }

                SWAP(brr[l + 1], brr[ir]);

                if (ebrr != NULL)
                {
                    SWAP(ebrr[l + 1], ebrr[ir])
                }
            }

            if (arr[l] > arr[ir])
            {
                SWAP(arr[l], arr[ir]);

                if (earr != NULL)
                {
                    SWAP(earr[l], earr[ir])
                }

                SWAP(brr[l], brr[ir]);

                if (ebrr != NULL)
                {
                    SWAP(ebrr[l], ebrr[ir])
                }
            }

            if (arr[l + 1] > arr[l])
            {
                SWAP(arr[l + 1], arr[l]);

                if (earr != NULL)
                {
                    SWAP(earr[l + 1], earr[l])
                }

                SWAP(brr[l + 1], brr[l]);

                if (ebrr != NULL)
                {
                    SWAP(ebrr[l + 1], ebrr[l])
                }
            }

            i = l + 1;
            j = ir;
            a = arr[l];
            b = brr[l];
            ea = (earr) ? earr[l] : 0;
            eb = (ebrr) ? ebrr[l] : 0;

            for (;;)
            {
                do
                {
                    i++;
                }
                while (arr[i] < a);

                do
                {
                    j--;
                }
                while (arr[j] > a);

                if (j < i)
                {
                    break;
                }

                SWAP(arr[i], arr[j]);

                if (earr != NULL)
                {
                    earr[i] = earr[j];
                }

                SWAP(brr[i], brr[j]);

                if (ebrr != NULL)
                {
                    ebrr[i] = ebrr[j];
                }
            }

            arr[l] = arr[j];

            if (earr != NULL)
            {
                earr[l] = earr[j];
            }

            arr[j] = a;

            if (earr != NULL)
            {
                earr[j] = ea;
            }

            brr[l] = brr[j];

            if (ebrr != NULL)
            {
                ebrr[l] = ebrr[j];
            }

            brr[j] = b;

            if (ebrr != NULL)
            {
                ebrr[j] = eb;
            }

            jstack += 2;

            if (jstack > NSTACK)
            {
                win_printf_OK("NSTACK too small in sort2.");
            }

            if (ir - i + 1 >= j - l)
            {
                istack[jstack] = ir;
                istack[jstack - 1] = i;
                ir = j - 1;
            }
            else
            {
                istack[jstack] = j - 1;
                istack[jstack - 1] = l;
                l = i;
            }
        }
    }
}


int     sort_ds_along_y(d_s *ds)
{
    unsigned long n;
    float *arr = NULL,  *brr, *earr = NULL, *ebrr = NULL;
    unsigned long i, ir, j, k, l = 1;
    int *is = NULL, *istack = NULL, jstack = 0;
    float a, b, ea = 0, eb = 0, temp1;

    if (ds->nx < 1 || ds->ny < 1)
    {
        return 1;
    }

    is = (int *)calloc(NSTACK, sizeof(int));
    istack = is - 1;
    arr = ds->yd - 1;

    if (ds->ye != NULL)
    {
        earr = ds->ye - 1;
    }

    brr = ds->xd - 1;

    if (ds->xe != NULL)
    {
        ebrr = ds->xe - 1;
    }

    n = ds->ny;
    ir = n;
    l = 1;

    for (;;)
    {
        if (ir - l < M)
        {
            for (j = l + 1; j <= ir; j++)
            {
                a = arr[j];
                b = brr[j];
                ea = (earr != NULL) ? earr[j] : 0;
                eb = (ebrr != NULL) ? ebrr[j] : 0;

                for (i = j - 1; i >= 1; i--)
                {
                    if (arr[i] <= a)
                    {
                        break;
                    }

                    arr[i + 1] = arr[i];

                    if (earr != NULL)
                    {
                        earr[i + 1] = earr[i];
                    }

                    brr[i + 1] = brr[i];

                    if (ebrr != NULL)
                    {
                        ebrr[i + 1] = ebrr[i];
                    }
                }

                arr[i + 1] = a;

                if (earr != NULL)
                {
                    earr[i + 1] = ea;
                }

                brr[i + 1] = b;

                if (ebrr != NULL)
                {
                    ebrr[i + 1] = eb;
                }
            }

            if (!jstack)
            {
                if (is)
                {
                    free(is);
                }

                return 0;
            }

            ir = istack[jstack];
            l = istack[jstack - 1];
            jstack -= 2;
        }
        else
        {
            k = (l + ir) >> 1;
            SWAP(arr[k], arr[l + 1]);

            if (earr != NULL)
            {
                SWAP(earr[k], earr[l + 1])
            }

            SWAP(brr[k], brr[l + 1]);

            if (ebrr != NULL)
            {
                SWAP(ebrr[k], ebrr[l + 1])
            }

            if (arr[l + 1] > arr[ir])
            {
                SWAP(arr[l + 1], arr[ir]);

                if (earr != NULL)
                {
                    SWAP(earr[l + 1], earr[ir])
                }

                SWAP(brr[l + 1], brr[ir]);

                if (ebrr != NULL)
                {
                    SWAP(ebrr[l + 1], ebrr[ir])
                }
            }

            if (arr[l] > arr[ir])
            {
                SWAP(arr[l], arr[ir]);

                if (earr != NULL)
                {
                    SWAP(earr[l], earr[ir])
                }

                SWAP(brr[l], brr[ir]);

                if (ebrr != NULL)
                {
                    SWAP(ebrr[l], ebrr[ir])
                }
            }

            if (arr[l + 1] > arr[l])
            {
                SWAP(arr[l + 1], arr[l]);

                if (earr != NULL)
                {
                    SWAP(earr[l + 1], earr[l])
                }

                SWAP(brr[l + 1], brr[l]);

                if (ebrr != NULL)
                {
                    SWAP(ebrr[l + 1], ebrr[l])
                }
            }

            i = l + 1;
            j = ir;
            a = arr[l];
            b = brr[l];
            ea = (earr) ? earr[l] : 0;
            eb = (ebrr) ? ebrr[l] : 0;

            for (;;)
            {
                do
                {
                    i++;
                }
                while (arr[i] < a);

                do
                {
                    j--;
                }
                while (arr[j] > a);

                if (j < i)
                {
                    break;
                }

                SWAP(arr[i], arr[j]);

                if (earr != NULL)
                {
                    earr[i] = earr[j];
                }

                SWAP(brr[i], brr[j]);

                if (ebrr != NULL)
                {
                    ebrr[i] = ebrr[j];
                }
            }

            arr[l] = arr[j];

            if (earr != NULL)
            {
                earr[l] = earr[j];
            }

            arr[j] = a;

            if (earr != NULL)
            {
                earr[j] = ea;
            }

            brr[l] = brr[j];

            if (ebrr != NULL)
            {
                ebrr[l] = ebrr[j];
            }

            brr[j] = b;

            if (ebrr != NULL)
            {
                ebrr[j] = eb;
            }

            jstack += 2;

            if (jstack > NSTACK)
            {
                win_printf_OK("NSTACK too small in sort2.");
            }

            if (ir - i + 1 >= j - l)
            {
                istack[jstack] = ir;
                istack[jstack - 1] = i;
                ir = j - 1;
            }
            else
            {
                istack[jstack] = j - 1;
                istack[jstack - 1] = l;
                l = i;
            }
        }
    }
}


/************************************************************************************************/
#undef M
#undef NSTACK
#undef SWAP
#undef NRANSI
/************************************************************************************************/
int do_sort_ds_along_x(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine sort a single data set according\n"
                             "to the X axis order, this routine does not keep\n"
                             " the original data set !");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return D_O_K;
    }

    sort_ds_along_x(dsi);
    pr->one_p->need_to_refresh = 1;
    return refresh_plot(pr, pr->cur_op);
}
int do_sort_ds_backward_along_x(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL;
    int i;
    float x, y, ex = 0, ey = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine sort a single data set according\n"
                             "to the inverse X axis order, this routine does not keep\n"
                             " the original data set !");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return D_O_K;
    }

    sort_ds_along_x(dsi);

    for (i = 0; i < dsi->nx / 2; i++)
    {
        x = dsi->xd[dsi->nx - i - 1];

        if (dsi->xe)
        {
            ex = dsi->xe[dsi->nx - i - 1];
        }

        y = dsi->yd[dsi->nx - i - 1];

        if (dsi->ye)
        {
            ey = dsi->ye[dsi->nx - i - 1];
        }

        dsi->xd[dsi->nx - i - 1] = dsi->xd[i];
        dsi->yd[dsi->nx - i - 1] = dsi->yd[i];

        if (dsi->xe)
        {
            dsi->xe[dsi->nx - i - 1] = dsi->xe[i];
        }

        if (dsi->ye)
        {
            dsi->ye[dsi->nx - i - 1] = dsi->ye[i];
        }

        dsi->xd[i] = x;

        if (dsi->xe)
        {
            dsi->xe[i] = ex;
        }

        dsi->yd[i] = y;

        if (dsi->ye)
        {
            dsi->ye[i] = ey;
        }
    }

    return refresh_plot(pr, pr->cur_op);
}
/****************************************************************/
/* to reduce by averaging a data set                */
/****************************************************************/
int ds_reduce(void)
{
    int     i, j, k;
    O_p     *op = NULL;
    d_s     *ds = NULL, *dsi = NULL, /* dsi is the source and ds is the destination */
            *dstmp = NULL;    /* dstmp is a temporary dataset      */
    pltreg  *pr = NULL;
    int     npts;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT]) return win_printf_OK("This routine reduces a data set.\n"
                                    "by averaging the points with same X\n"
                                    "The function \"sort ds >\" is called\n"
                                    "(so be careful if < order wanted...)."
                                    "A new dataset is created.");

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf("cannot find data");
    }

    npts = dsi->nx;         /* this is the number of points in the data set */

    if ((dstmp = build_data_set(npts, npts)) == NULL)
    {
        return win_printf("I can't create plot !");
    }

    for (j = 0; j < npts; j++)
    {
        dstmp->yd[j] = dsi->yd[j];
        dstmp->xd[j] = dsi->xd[j];
    }

    QuickSort_double(dstmp->xd, dstmp->yd, 0, npts - 1);
    j = 0;
    k = 0;

    while (j < npts - 1)
    {
        i = 1;

        while (dstmp->xd[j] == dstmp->xd[j + i])
        {
            dstmp->yd[j] += dstmp->yd[j + i];
            i = i + 1;
        }

        dstmp->xd[k] = dstmp->xd[j];
        dstmp->yd[k] = dstmp->yd[j] / i;
        j = j + i;
        k = k + 1;
    }

    if (dstmp->xd[npts - 2] != dstmp->xd[npts - 1])
    {
        dstmp->xd[k] = dstmp->xd[npts - 1];
        dstmp->yd[k] = dstmp->yd[npts - 1];
        k = k + 1;
    }

    win_printf("Info :\n%d points in source dataset.\n%d different ones.", npts, k);

    if ((ds = create_and_attach_one_ds(op, k, k, 0)) == NULL)
    {
        return win_printf("I can't create plot !");
    }

    for (j = 0; j < k; j++)
    {
        ds->yd[j] = dstmp->yd[j];
        ds->xd[j] = dstmp->xd[j];
    }

    free_data_set(dstmp);
    inherit_from_ds_to_ds(ds, dsi);
    ds->treatement = my_sprintf(ds->treatement, " averaged-reduced");
    return refresh_plot(pr, UNCHANGED);
} /* end of the "ds_reduce" function */
/****************************************************************/
/* to decime a data set                     */
/***************************************************************/
int ds_same_x_average_data_set_sorted(void)
{
  int i, nr = 0;
  static float off = 10;
  float mean = 0;
  float meansq = 0;
  float xtemp = 0;
  float thresh = 0;
  O_p   *op = NULL;
  d_s   *ds = NULL, *dsi = NULL, *dsr = NULL; /* dsi is the source and ds is the destination */
  pltreg    *pr = NULL;
  char  s[128] = {0};
  int   npts_in;//, npts_out;

  if (updating_menu_state != 0)
  {
      return D_O_K;
  }

  if (key[KEY_LSHIFT]) return win_printf_OK("This routine reduces a data set well sorted in x\n"
                                  "by keeping only 1 point for each x.\n"
                                  "Errors are computed as the square root of the number of points\n"
                                  "A new dataset is created.");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
  {
      return win_printf("cannot find data!");
  }

  npts_in = dsi->nx;            /* this is the number of points in the data set */
  sprintf(s, "There are %d points in the source dataset.\n"
          "Consider same x if x value lie within %%f porcent from each other\n", npts_in);
  i = win_scanf(s, &off);

  ds = duplicate_data_set(dsi,NULL);

  if ((dsr = create_and_attach_one_ds(op,16, 16, 0)) == NULL)
    {
      return win_printf("I can't create plot !");
    }
  dsr->nx = dsr->ny = 0;
  alloc_data_set_y_error(dsr);

  xtemp = ds->xd[0];
  thresh = fabs(xtemp * off / 100);
  mean = 0;
  meansq = 0;
  nr = 0;

  for (int ii = 0;ii<ds->nx;ii++)
  {

    if (fabs(xtemp-ds->xd[ii]) < thresh)
    {
    mean += ds->yd[ii];
    meansq += ds->yd[ii] * ds->yd[ii];
    nr+=1;
  }
  else
  {
    add_new_point_to_ds(dsr,xtemp,mean/nr);
    dsr->ye[dsr->nx - 1] = (mean/nr) / sqrt(nr);
    xtemp = ds->xd[ii];
    thresh = fabs(xtemp * off / 100);
    mean = ds->yd[ii];
    meansq = ds->yd[ii]*ds->yd[ii];
    nr = 1;
  }

  }
  add_new_point_to_ds(dsr,xtemp,mean/nr);
  dsr->ye[dsr->nx - 1] = (mean/nr) /sqrt(nr);



  free_data_set(ds);
  return 0;


}



int ds_same_x_average(void)
{
  int i, nr = 0;
  static float off = 10;
  float mean = 0;
  float meansq = 0;
  float xtemp = 0;
  float thresh = 0;
  O_p   *op = NULL;
  d_s   *ds = NULL, *dsi = NULL, *dsr = NULL; /* dsi is the source and ds is the destination */
  pltreg    *pr = NULL;
  char  s[128] = {0};
  int   npts_in;//, npts_out;
  static int issorted = 0;

  if (updating_menu_state != 0)
  {
      return D_O_K;
  }

  if (key[KEY_LSHIFT]) return win_printf_OK("This routine reduces a data set\n"
                                  "by keeping only 1 point for each x.\n"
                                  "Errors are computed as the square root of the number of points\n"
                                  "A new dataset is created.");

  /* we first find the data that we need to transform */
  if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
  {
      return win_printf("cannot find data!");
  }
  i = win_scanf("Is your dataset sorted in x ? Yes -> %R No ->%r\n",&issorted);
  if (i == WIN_CANCEL) return 0;
  if (issorted == 0) return ds_same_x_average_data_set_sorted();
  npts_in = dsi->nx;            /* this is the number of points in the data set */
  sprintf(s, "There are %d points in the source dataset.\n"
          "Consider same x if x value lie within %%f porcent from each other\n", npts_in);
  i = win_scanf(s, &off);
  if (i == WIN_CANCEL) return 0;


  ds = duplicate_data_set(dsi,NULL);

  if ((dsr = create_and_attach_one_ds(op,16, 16, 0)) == NULL)
    {
      return win_printf("I can't create plot !");
    }
  dsr->nx = dsr->ny = 0;
  alloc_data_set_y_error(dsr);

  while (ds->nx != 0)
  {
    xtemp = ds->xd[0];
    thresh = xtemp * off / 100;
    mean = 0;
    nr = 0;
    meansq = 0;
    for (i =0;i<ds->nx - nr;)
    {
      if (fabs(xtemp-ds->xd[i]) < thresh)
      {
      mean += ds->yd[i];
      meansq += ds->yd[i] * ds->yd[i];
      nr+=1;
      remove_point_from_data_set(ds,i);
    }
    else i += 1;

    }
    mean /= nr;
    meansq /= nr;
    add_new_point_to_ds(dsr,xtemp,mean);
    dsr->ye[dsr->nx - 1] = sqrt((meansq-mean*mean)/nr);
  }

  free_data_set(ds);
  return 0;


}

int split_ds_of_op(O_p *op, int ds_all, int cd, float delta_x)
{

  d_s *dsi = NULL, *dscur = NULL;
  int j = 0;
  int k = 0;
  for (j = 0,k=0;j<op->n_dat;j++)
  {
  dsi = op->dat[k];
  if (ds_all == 0 && j!= cd)
  {
    k++;
    continue;
  }
  dscur = create_and_attach_one_ds(op,16,16,0);
  dscur->nx = dscur->ny = 0;

  for (int i = 0;i<dsi->nx;i++)
  {
    add_new_point_to_ds(dscur,dsi->xd[i],dsi->yd[i]);
    if (i != dsi->nx-1)
    {
    if (dsi->xd[i+1] - dsi->xd[i] > delta_x)
    {
      dscur = create_and_attach_one_ds(op,16,16,0);
      dscur->nx = dscur->ny = 0;


    }
  }
  }
  remove_ds_from_op(op,dsi);
}
return 0;

}



int ds_split_ds(void)
{

      int i;
      O_p   *op = NULL;
      d_s   *dsi = NULL; /* dsi is the source and ds is the destination */
      pltreg    *pr = NULL;
      d_s *dscur = NULL;
      static float delta_x = 5;

      if (updating_menu_state != 0)
      {
          return D_O_K;
      }

      if (key[KEY_LSHIFT]) return win_printf_OK("This routine split one ds in several smaller ones\n");

      /* we first find the data that we need to transform */
      if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
      {
          return win_printf("cannot find data!");
      }

      i = win_scanf("Split selected dataset if delta x (no units)) > %f\n",&delta_x);
      int index = 0;
      int ds_all = 0;
      int cd = 0;

      if (i == WIN_CANCEL)
      {
          return OFF;
      }
      cd = op->cur_dat;
      index = active_menu->flags & 0xFFFFFF00;
      if (index & DS_ALL) ds_all = 1;
      split_ds_of_op(op, ds_all, cd, delta_x);

      return 0;

}

int ds_decime(void)
{
    int i, j;
    static int n = 2, off = 0, offe = 1;
    O_p   *op = NULL;
    d_s   *ds = NULL, *dsi = NULL; /* dsi is the source and ds is the destination */
    pltreg    *pr = NULL;
    char  s[128] = {0};
    int   npts_in;//, npts_out;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT]) return win_printf_OK("This routine reduces a data set\n"
                                    "by keeping only 1 point each N ones.\n"
                                    "NO sorting is performed !\n"
                                    "A new dataset is created.");

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf("cannot find data!");
    }

    npts_in = dsi->nx;            /* this is the number of points in the data set */
    sprintf(s, "There are %d points in the source dataset.\n"
            "keep 1 point each %%6d points\n Starting at point in range [%%5d,%%5d[\n", npts_in);
    i = win_scanf(s, &n, &off, &offe);

    if (i == WIN_CANCEL)
    {
        return OFF;
    }

    off = (off < 0) ? 0 : off;

    if (n <= 0)
    {
        return (win_printf("N=%d is prohibited !!!", n));
    }

    //npts_out=(int)((npts_in-off) - ((npts_in) % n))/n;
    //i=win_printf("The will be %d points in the destination dataset.",npts_out);
    //if (i==WIN_CANCEL) return OFF;

    for (int ids = off; ids < offe; ids++)
      {
	if ((ds = create_and_attach_one_ds(op, npts_in/n, npts_in/n, 0)) == NULL)
	  {
	    return win_printf("I can't create plot !");
	  }
	ds->nx = ds->ny = 0;

	if (dsi->xe != NULL)	    alloc_data_set_x_error(ds);
	if (dsi->xed != NULL)	    alloc_data_set_x_down_error(ds);
	if (dsi->xbu != NULL)       alloc_data_set_x_box_up(ds);
	if (dsi->xbd != NULL)       alloc_data_set_x_box_dwn(ds);
	if (dsi->ye != NULL)	    alloc_data_set_y_error(ds);
	if (dsi->yed != NULL)	    alloc_data_set_y_down_error(ds);
	if (dsi->ybu != NULL)       alloc_data_set_y_box_up(ds);
	if (dsi->ybd != NULL)       alloc_data_set_y_box_dwn(ds);


	for (i = 0, j = ids; j < dsi->nx; j += n, i++)
	  {

	    if (dsi->xe != NULL && dsi->ye != NULL)
	      {
		add_new_point_with_xy_error_to_ds(ds, dsi->xd[j], dsi->xe[j], dsi->yd[j], dsi->ye[j]);
	      }
	    else if (dsi->xe != NULL)
	      {
		add_new_point_with_x_error_to_ds(ds, dsi->xd[j], dsi->xe[j], dsi->yd[j]);
	      }
	    else if (dsi->ye != NULL)
	      {
		add_new_point_with_y_error_to_ds(ds, dsi->xd[j], dsi->yd[j], dsi->ye[j]);
	      }
	    else
	      {
		add_new_point_to_ds(ds, dsi->xd[j], dsi->yd[j]);
	      }
	    if (dsi->xed != NULL && ds->xed != NULL)	ds->xed[i] = dsi->xed[j];
	    if (dsi->xbu != NULL && ds->xbu != NULL)    ds->xbu[i] = dsi->xbu[j];
	    if (dsi->xbd != NULL && ds->xbd != NULL)    ds->xbd[i] = dsi->xbd[j];
	    if (dsi->yed != NULL && ds->yed != NULL)	ds->yed[i] = dsi->yed[j];
	    if (dsi->ybu != NULL && ds->ybu != NULL)    ds->ybu[i] = dsi->ybu[j];
	    if (dsi->ybd != NULL && ds->ybd != NULL)    ds->ybd[i] = dsi->ybd[j];

	  }

	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement, "decimed by %d offeted by %d", n, ids);
      }
    return refresh_plot(pr, UNCHANGED);
} /* end of the "ds_decime" function */
/****************************************************************/
/* to decime_avg a data set                     */
/****************************************************************/
int do_ds_decime_average(O_p *op, d_s *dsi, int n, int starting, int period, int replace)
{
    int i, j, k, ii;
    d_s     *ds = NULL; /* dsi is the source and ds is the destination */
    int     npts_in, npts_out, itmp;
    double  sy, sy2, sx;
    npts_in = dsi->nx;          /* this is the number of points in the data set */
    npts_out = (npts_in - starting) / period; //(int)npts_in / n;
    npts_out += (npts_in - starting - (npts_out * period) >= n) ? 1 : 0;
    itmp = npts_out;

    if (replace)
    {
        for (i = 0, itmp = 0; i < npts_out; i++)
        {
            k = starting + i * period;

            for (j = 0; j < n && (k + j) < npts_in; j++)
            {
                itmp++;
            }
        }

        //
    }

    //npts_out = (int)npts_in / n;
    //npts_out += (npts_in % n) ? 1 : 0;

    if ((ds = create_and_attach_one_ds(op, itmp, itmp, 0)) == NULL)
    {
        return win_printf_OK("I can't create plot !");
    }

    ds->ny = ds->nx;

    if (replace == 0)
    {
        if ((alloc_data_set_y_error(ds) == NULL) || (ds->ye == NULL))
        {
            return win_printf_OK("I can't create errors !");
        }
    }

    for (i = 0, ii = 0; i < npts_out; i++)
    {
        k = starting + i * period;

        for (j = 0, sx = sy = 0; j < n && (k + j) < npts_in; j++)
        {
            sx += dsi->xd[k + j];
            sy += dsi->yd[k + j];
        }

        if (j != 0)
        {
            sx /= j;
            sy /= j;
        }

        if (replace == 0)
        {
            ds->xd[i] = sx;
            ds->yd[i] = sy;

            for (j = 0, sy2 = 0; j < n && (k + j) < npts_in; j++)
            {
                sy2 += (dsi->yd[k + j] - sy) * (dsi->yd[k + j] - sy);
            }

            k = j;
            k = (k > 1) ? k - 1 : k;
            sy2 = (k != 0 && j != 0) ? sy2 / (k * j) : sy2;
            sy2 = sqrt(sy2);
            ds->ye[i] = sy2;
        }
        else if (replace == 1)
        {
            for (j = 0; j < n && (k + j) < npts_in && ii < ds->nx; j++)
            {
                ds->xd[ii] = dsi->xd[k + j];
                ds->yd[ii++] = sy;
            }
        }
        else if (replace == 2)
        {
            for (j = 0; j < n && (k + j) < npts_in && ii < ds->nx; j++)
            {
                ds->xd[ii] = dsi->xd[k + j];
                ds->yd[ii++] = dsi->yd[k + j] - sy;
            }
        }
    }

    inherit_from_ds_to_ds(ds, dsi);
    ds->treatement = my_sprintf(ds->treatement, "Averaged over %d pts starting at %d with period %d", n, starting, period);
    return D_O_K;
} /* end of the "do_ds_decime" function */
int ds_decime_avg(void)
{
    int i;
    static int n = 2, period = 4, starting = 1, replace = 0;
    O_p     *op = NULL;
    d_s     *dsi = NULL; /* dsi is the source and ds is the destination */
    pltreg  *pr = NULL;
    char    s[512] = {0};
    int     npts_in, npts_out;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT]) return win_printf_OK("\\stack{\\paragraph{8000}{"
                                    "This routine reduces a data set by averaging over {\\color{yellow}n} "
                                    "points. With {X_i = \\Sigma_{j=i*n}^{j<i*(n+1)} x_j} and "
                                    "{Y_i = \\Sigma_{j=i*n}^{j<i*(n+1)} y_j}. The number of points of "
                                    "the new data set is the number of points of the input one divide by n. "
                                    "The error \\delta Y_i is calculated assuming that statistical errors "
                                    "are uncorrelated  by:} \\centerparagraph{8000}{ "
                                    "{\\delta Y_i = \\sqrt{\\Sigma_{j=i*n}^{j<i*(n+1)} "
                                    "\\frac{(y_j - Y_i)^2}{n*(n-1)}}}}}");

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf("cannot find data!");
    }

    npts_in = dsi->nx;          /* this is the number of points in the data set */
    snprintf(s, sizeof(s), "There are %d points in the source dataset.\n"
             "This routine will average over %%8d points\n"
             "starting at point %%8d with a period of %%8d\n"
             "%%Rplace one point per average\n"
             "%%rreplace points average by their average\n"
             "%%rreplace points average by their differece from average\n"
             , npts_in);
    i = win_scanf(s, &n, &starting, &period, &replace);

    if (i == WIN_CANCEL)
    {
        return OFF;
    }

    if (n <= 0)
    {
        return win_printf_OK("N=%d is prohibited !!!", n);
    }

    npts_out = (npts_in - starting) / period; //(int)npts_in / n;
    npts_out += (npts_in - starting - (npts_out * period) >= n) ? 1 : 0;
    i = win_printf("There will be %d points in the destination dataset.", npts_out);

    if (i == WIN_CANCEL)
    {
        return OFF;
    }

    if (do_ds_decime_average(op, dsi, n, starting, period, replace))
    {
        return win_printf("averaging error");
    }

    return refresh_plot(pr, UNCHANGED);
} /* end of the "ds_decime" function */
/****************************************************************/
/* to periodic_avg a data set                       */
/****************************************************************/
int ds_periodic_avg(void)
{
    int i, j, k;
    int n_end;
    O_p     *op = NULL;
    d_s     *ds = NULL, *dsi = NULL; /* dsi is the source and ds is the destination */
    pltreg  *pr = NULL;
    char    s[4096] = {0};
    static int  npts_in, npts_out = 128, n_start = 0, n_per = 8, stop_after_N = 0, with_er = 1;
    static int doavg = 1;
    double  sy, sy2;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT]) return win_printf_OK("\\stack{\\paragraph{8000}{"
                                    "This routine reduces a data set by averaging a periodic signal  over {\\color{yellow}n} "
                                    "points. Starting at point of index n_{s} and ending at point of index n_e. "
                                    "With {Y_j = \\Sigma_{k=j}^{k<+i*n)} y_k}. "
                                    "The number of points of "
                                    "the new data set is the number of points of the input one divide by n. "
                                    "The error \\delta Y_i is calculated assuming that statistical errors "
                                    "are uncorrelated  by:} \\centerparagraph{8000}{ "
                                    "{\\delta Y_i = \\sqrt{\\Sigma_{j=i*n}^{j<i*(n+1)} "
                                    "\\frac{(y_j - Y_i)^2}{n*(n-1)}}}}}");

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf("cannot find data!");
    }

    npts_in = dsi->nx;          /* this is the number of points in the data set */
    sprintf(s, "There are %d points in the source dataset.\n"
            "the routine will %%R->extract cycles or %%r->average a periodic signal\n"
            "starting at point of index %%8d\n"
            "one period extending over %%8d points\n"
            "process signal until the end of dataset %%R or stops after N averaging periods %%r\n"
            "If you select N periods, indicate the N value %%6d (number of full periods)\n "
            "Do you want statistical error bars? %%R->No %%r->Yes\n", npts_in);
    i = win_scanf(s, &doavg, &n_start, &npts_out, &stop_after_N, &n_per, &with_er);

    if (i == WIN_CANCEL)
    {
        return OFF;
    }

    if (npts_out <= 0)
    {
        return win_printf_OK("N=%d is prohibited !!!", npts_out);
    }

    if (doavg)
      {
	if ((ds = create_and_attach_one_ds(op, npts_out, npts_out, 0)) == NULL)
	  {
	    return win_printf_OK("I can't create plot !");
	  }

	ds->ny = npts_out;

	if (with_er)
	  {
	    if ((alloc_data_set_y_error(ds) == NULL) || (ds->ye == NULL))
	      {
		return win_printf_OK("I can't create errors !");
	      }
	  }

	n_end = (stop_after_N) ? n_start + n_per * npts_out : npts_in;
	n_end = (n_end < npts_in) ? n_end : npts_in;

	//win_printf("n_end %d nx %d err %d nout %d",n_end,npts_in,with_er,ds->nx);
	for (i = 0; i < npts_out; i++)
	  {
	    for (k = 0, j = i + n_start, sy = 0; j < n_end; j += npts_out, k++)
	      {
		sy += dsi->yd[j];
	      }

	    if (k != 0)
	      {
		sy /= k;
	      }

	    ds->xd[i] = i;
	    ds->yd[i] = sy;

	    if (with_er)
	      {
		for (k = 0, j = i + n_start, sy2 = 0; j < n_end; j += npts_out, k++)
		  {
		    sy2 += (dsi->yd[j] - sy) * (dsi->yd[j] - sy);
		  }

		k = (k > 1) ? k - 1 : k;
		sy2 = (k != 0) ? sy2 / (k) : sy2;
		sy2 = sqrt(sy2);
		ds->ye[i] = sy2;
	      }
	  }
	inherit_from_ds_to_ds(ds, dsi);
	ds->treatement = my_sprintf(ds->treatement, "Periodic averaged starting at index %d "
				    "period size %d number of averaging %d", n_start, npts_out, (n_end - n_start) / npts_out);
      }
    else //extract cycles
      {
	n_end = (stop_after_N) ? n_start + n_per * npts_out : npts_in;
	n_end = (n_end < npts_in) ? n_end : npts_in;
	for (k = 0, i = 0, j = n_start; j < n_end; j++)
	  {
	    if (ds == NULL)
	      {
		if ((ds = create_and_attach_one_ds(op, npts_out, npts_out, 0)) == NULL)
		  {
		    return win_printf_OK("I can't create ds for cycle %d !",i);
		  }
		inherit_from_ds_to_ds(ds, dsi);
		ds->treatement = my_sprintf(ds->treatement, "Periodic signal starting at index %d "
					    "period size %d number cycle %d", n_start, npts_out, i);
	      }
	    ds->xd[k] = k;
	    ds->yd[k] = dsi->yd[j];
	    k++;
	    if (k >= npts_out)
	      {
		ds = NULL;
		k = 0;
		i++;
	      }
	  }
      }
    return refresh_plot(pr, UNCHANGED);
} /* end of the "ds_decime" function */
/****************************************************************/
/* to merge 2 datatsets in 1                    */
/****************************************************************/
int ds_merge(void)
{
    int j;
    O_p     *op = NULL;
    d_s     *ds1 = NULL, *ds2 = NULL,     /* sources  */
            *ds = NULL;        /* destination  */
    pltreg  *pr = NULL;
    int     npts;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT]) return win_printf_OK("his routine merge 2 datasets in one.\n"
                                    "A new dataset is created.\n"
                                    "Properties of active dataset are kept.");

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return win_printf("cannot find data!");
    }

    if (op == NULL || pr->one_p->n_dat < 2)
    {
        return (win_printf("Not enought data sets !"));
    }

    j = pr->one_p->cur_dat;
    ds1 = pr->one_p->dat[j];
    j = (j + 1 >= pr->one_p->n_dat) ? 0 : j + 1;
    ds2 = pr->one_p->dat[j];

    if (ds1 == NULL || ds2 == NULL)
    {
        return (win_printf("I can't find data set !"));
    }

    npts = ds1->nx + ds2->nx;

    if ((ds = create_and_attach_one_ds(op, npts, npts, 0)) == NULL)
    {
        return win_printf("I can't create plot !");
    }

    if (ds1->ye != NULL || ds2->ye != NULL)
        if ((ds = alloc_data_set_y_error(ds)) == NULL)
        {
            return win_printf("I can't allocate y error!");
        }

    if (ds1->xe != NULL || ds2->xe != NULL)
        if ((ds = alloc_data_set_x_error(ds)) == NULL)
        {
            return win_printf("I can't allocate x error!");
        }

    for (j = 0; j < ds1->nx; j++)
    {
        ds->yd[j] = ds1->yd[j];
        ds->xd[j] = ds1->xd[j];

        if (ds1->xe != NULL)
        {
            ds->xe[j] = ds1->xe[j];
        }

        if (ds1->ye != NULL)
        {
            ds->ye[j] = ds1->ye[j];
        }
    }

    for (j = 0; j < ds2->nx; j++)
    {
        ds->yd[j + ds1->nx] = ds2->yd[j];
        ds->xd[j + ds1->nx] = ds2->xd[j];

        if (ds2->xe != NULL)
        {
            ds->xe[j + ds1->nx] = ds2->xe[j];
        }

        if (ds2->ye != NULL)
        {
            ds->ye[j + ds1->nx] = ds2->ye[j];
        }
    }

    inherit_from_ds_to_ds(ds, ds1);
    ds->treatement = my_sprintf(ds->treatement, " merged datasets");
    return  refresh_plot(pr, UNCHANGED);
} /* end of the "ds_merge" function */
O_p *merge_all_ds_in_op(pltreg *pr, O_p *op)
{
    int i, j, k;
    O_p *opn = NULL;
    d_s *ds = NULL;
    int nf, ni, has_erx = 0, has_ery;

    if (op == NULL || op->n_dat < 1)
    {
        return NULL;
    }

    for (i = nf = 0, has_erx = has_ery = 0; i < op->n_dat; i++)
    {
        ni = (op->dat[i]->nx < op->dat[i]->ny) ? op->dat[i]->nx : op->dat[i]->ny;
        nf += ni;

        if (ni > 0 && op->dat[i]->xe != NULL)
        {
            has_erx = 1;
        }

        if (ni > 0 && op->dat[i]->ye != NULL)
        {
            has_ery = 1;
        }
    }

    if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)
    {
        return NULL;
    }

    ds = opn->dat[0];

    if ((has_ery) && ((ds = alloc_data_set_y_error(ds)) == NULL))
    {
        return (O_p *) win_printf_ptr("I can't allocate y error!");
    }

    if ((has_erx) && ((ds = alloc_data_set_x_error(ds)) == NULL))
    {
        return (O_p *) win_printf_ptr("I can't allocate x error!");
    }

    for (i = k = 0; i < op->n_dat; i++)
    {
        for (j = 0; j < op->dat[i]->nx && j < op->dat[i]->ny; j++)
        {
            ds->yd[k] = op->dat[i]->yd[j];
            ds->xd[k] = op->dat[i]->xd[j];

            if (ds->xe != NULL)
            {
                ds->xe[k] = (op->dat[i]->xe != NULL) ? op->dat[i]->xe[j] : 0;
            }

            if (ds->ye != NULL)
            {
                ds->ye[k] = (op->dat[i]->ye != NULL) ? op->dat[i]->ye[j] : 0;
            }

            k++;
        }
    }

    sort_ds_along_x(ds);
    /* now we must do some house keeping */
    inherit_from_ds_to_ds(ds, op->dat[op->cur_dat]);
    ds->treatement = my_sprintf(ds->treatement,
                                "data set merged from %d data sets", op->n_dat);
    set_plot_title(opn, "data set merged from %d data sets", op->n_dat);

    if (op->x_title != NULL)
    {
      set_plot_x_title(opn, "%s", op->x_title);
    }

    if (op->y_title != NULL)
    {
      set_plot_y_title(opn, "%s", op->y_title);
    }

    opn->filename = Transfer_filename(op->filename);
    uns_op_2_op(opn, op);
    return opn;
}
/****************************************************************/
/* moved from interpol.c on 26/11/2003              */
/****************************************************************/
int do_merge_all_ds_in_op(void)
{
    O_p *op = NULL, *opn = NULL;
    d_s *dsi = NULL;
    pltreg *pr = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])  /* display routine action if SHIFT is pressed */
    {
        return win_printf("This routine merge all the data sets\n"
                          "of one plot and place them in a new plot");
    }

    /* we first find the data that we need to transform */

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf("cannot find data");
    }

    if ((opn = merge_all_ds_in_op(pr, op)) == NULL)
    {
        return win_printf("cannot create plot !");
    }

    /* refisplay the entire plot */
    return refresh_plot(pr, UNCHANGED);
}


O_p *aggregate_ds_per_source(char *commonstr)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    O_p *opres = NULL;
    d_s *ds = NULL;
    d_s *dd = NULL;
    bool first_copied = true;

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
    {
        win_printf("cannot find data");
        return NULL;
    }

    if ((opres = create_and_attach_one_plot(pr, 1, 1, 0)) == NULL)
    {
        return NULL;
    }

    opres->data_changing = 1;

    for (int i = 0; i < pr->n_op; ++i)
    {
        op = pr->o_p[i];

        if (op != opres)
        {
            for (int j = 0; j < op->n_dat; ++j)
            {
                ds = op->dat[j];

                if (ds->source != NULL && strlen(ds->source) > 0 && strstr(ds->source, commonstr) != NULL)
                {
                    dd = duplicate_data_set(ds, NULL);

                    if (add_one_plot_data(opres, IS_DATA_SET, (void *)dd) == 0)
                    {
                        if (first_copied)
                        {
                            uns_op_2_op(opres, op);
                            set_op_x_unit_set(opres, op->n_xu - 1);
                            set_op_y_unit_set(opres, op->n_yu - 1);
                            set_op_t_unit_set(opres, op->n_tu - 1);
                            first_copied = false;
                        }
                    }
                }
            }
        }
    }

    opres->title = strdup(commonstr);
    opres->filename = strdup(commonstr);
    opres->data_changing = 0;
    return opres;
}
int apply_color_ds_per_source_pattern(char *commonstr, int color, O_p *src_op)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int affected_ds = 0;

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
    {
        win_printf("cannot find data");
        return 0;
    }

    for (int i = 0; i < pr->n_op; ++i)
    {
        op = pr->o_p[i];

        if (op == src_op)
        {
            continue;
        }

        for (int j = 0; j < op->n_dat; ++j)
        {
            ds = op->dat[j];

            if (ds->source != NULL && strlen(ds->source) > 0 && strstr(ds->source, commonstr) != NULL)
            {
                ds->color = color;
                affected_ds++;
            }
        }
    }

    return affected_ds;
}

int do_apply_color_ds_per_source_pattern(void)
{
    static char buf[1024] = {0};
    int res = 0;
    d_s *ds = NULL;
    static int pattern_source = 0;
    static int color_source = 0;
    static int color = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%ds", &ds) != 1)
    {
        win_printf("cannot find data");
        return 0;
    }

    res = win_scanf("Warning: this function will affect all plot in this project\n"
                    "%R Use current ds source %r use pattern: %s\n"
                    "%R Use current ds color %r choose color", &pattern_source, buf, &color_source);

    if (res != WIN_OK)
    {
        return OFF;
    }

    if (pattern_source == 0)
    {
        snprintf(buf, sizeof(buf), "%s", ds->source);
    }
    else
    {
        if (strlen(buf) < 1)
        {
            error_message("You can't use a empty pattern");
            return OFF;
        }
    }

    if (color_source == 0)
    {
        color = ds->color;
    }
    else
    {
        color = color_chooser();

        if (color == -1)
        {
            return OFF;
        }
    }

    int affected_ds = apply_color_ds_per_source_pattern(buf, color, NULL);
    win_printf("%d Affected ds", affected_ds);
    return 0;
}

int do_apply_color_ds_per_source_pattern_by_range(void)
{
    static char buf[1024] = {0};
    int res = 0;
    O_p *op = NULL;
    d_s *ds = NULL;
    static char prefix[1024] = "Bead";
    static char suffix[1024] = " ";
    static int begin = 0;
    static int end = 1;
    static int color_source = 0;
    static int color = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%op", &op) != 1)
    {
        win_printf("cannot find data");
        return 0;
    }

    res = win_scanf("Warning: this function will affect all plot in this project\n"
                    "This function will read all dataset of thet current plot that respond to a pattern\n"
                    "and apply the color of this dataset to all dataset of\n"
                    "the project that respond to the same pattern\n"
                    "Prefix: %s\n range [%d to %d[\n Suffix %s\n"
                    "%R Use source ds color %r choose color for each element",
                    prefix, &begin, &end, suffix, &color_source);

    if (res != WIN_OK)
    {
        return OFF;
    }

    if (end < begin)
    {
        return 0;
    }

    for (int i = begin; i < end; ++i)
    {
        snprintf(buf, sizeof(buf), "%s%d%s", prefix, i, suffix);

        for (int j = 0; j < op->n_dat; ++j)
        {
            ds = op->dat[j];

            if (ds->source != NULL && strlen(ds->source) > 0 && strstr(ds->source, buf) != NULL)
            {
                if (color_source == 0)
                {
                    color = ds->color;
                }
                else
                {
                    color = color_chooser();

                    if (color == -1)
                    {
                        return OFF;
                    }
                }

                //int affected_ds =
		apply_color_ds_per_source_pattern(buf, color, op);
            }
        }
    }

    return 0;
}

int do_aggregate_ds_per_source(void)
{
    static char buf[1024] = {0};
    int res = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    res = win_scanf("Warning: this function will affect all plot in this project\n"
                    "Aggregation string: %s", buf);

    if (res == WIN_OK)
    {
        if (strlen(buf) < 1)
        {
            error_message("You can't use a empty pattern");
            return OFF;
        }

        aggregate_ds_per_source(buf);
    }

    return 0;
}

int do_aggregate_ds_per_source_range(void)
{
    char prefix[1024] = "Bead";
    char suffix[1024] = " ";
    char buf[4096] = {0};
    int begin = 0;
    int end = 1;
    int res = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    res = win_scanf("Warning: this function will affect all plot in this project\n"
                    "Prefix: %s\n range [%d to %d[\n Suffix %s\n", prefix, &begin, &end, suffix);

    if (res == WIN_OK)
    {
        if (end < begin)
        {
            return 0;
        }

        for (int i = begin; i < end; ++i)
        {
            snprintf(buf, sizeof(buf), "%s%d%s", prefix, i, suffix);
            aggregate_ds_per_source(buf);
        }
    }

    return 0;
}

/* end of "do_merge_all_ds_in_op" */
/****************************************************************/
/* to find the first global maximum and minimum of a data set   */
/****************************************************************/
int ds_find_max(void)
{
    int i;
    pltreg  *pr = NULL;
    O_p     *op = NULL;
    d_s     *ds = NULL;
    float   max_X, max_Y, min_x, min_y;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT]) return win_printf_OK("This routine gives the first global\n"
                                    "maximum and minimum of a data set.");

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)
    {
        return win_printf("cannot find data");
    }

    max_X = ds->xd[0];
    min_x = ds->xd[0];
    max_Y = ds->yd[0];
    min_y = ds->yd[0];

    for (i = 1; i < ds->nx; i++)
    {
        if (ds->yd[i] > max_Y)
        {
            max_Y = ds->yd[i];
            max_X = ds->xd[i];
        }

        if (ds->yd[i] < min_y)
        {
            min_y = ds->yd[i];
            min_x = ds->xd[i];
        }
    }

    win_printf("for data set n� %d :\n"
               "the point of maximum y \nis (%f,%f)\n"
               "the point of minimum y \nis (%f,%f)\n",
               op->cur_dat, max_X, max_Y, min_x, min_y);
    return D_O_K;
} /* end of the "ds_find_max" function */
/****************************************************************/
/* to find the first global maximum and minimum of a data set   */
/****************************************************************/
int ds_normalized_max_to_1(void)
{
    int i;
    pltreg  *pr = NULL;
    O_p     *op = NULL;
    d_s     *ds = NULL;
    float    max_Y;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT]) return win_printf_OK("This routine find the first global\n"
                                    "maximum and normalize it so that this maximum = 1.");

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)
    {
        return win_printf("cannot find data");
    }

    //max_x=ds->xd[0];
    max_Y = ds->yd[0];

    for (i = 1; i < ds->nx; i++)
    {
        if (ds->yd[i] > max_Y)
        {
            max_Y = ds->yd[i];
            //max_x = ds->xd[i];
        }
    }

    for (i = 1; i < ds->nx && max_Y != 0; i++)
    {
        ds->yd[i] /= max_Y;
    }

    op->need_to_refresh = 1;
    return refresh_plot(pr, pr->cur_op);
} /* end of the "ds_normalized_max_to_1" function */
/****************************************************************/
/* to find the first local maximum and minimum of a data set    */
/*  and the position of the half maxima         */
/****************************************************************/
int ds_find_halfmax(void)
{
    int i;
    pltreg  *pr = NULL;
    O_p     *op = NULL;
    d_s     *ds = NULL;
    int     min, start, end;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT]) return win_printf_OK("This routine gives the maximum of \n"
                                    "the visible points of the active dataset.\n"
                                    "it also gives the coordinates of the points\n"
                                    "where half max is reached.");

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)
    {
        return win_printf("I cannot find data");
    }

    /* ci-dessous, on cherche quels sont les points dans la zone du plot affichee : */
    min = (ds->nx > ds->ny) ? ds->nx : ds->ny;

    for (start = i = min - 1; i >= 0 ; i--)
    {
        if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo)
        {
            start = (i < start) ? i : start;
        }
    }

    for (end = i = 0; i < min ; i++)
    {
        if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo)
        {
            end = (i > end) ? i : end;
        }
    }

    end += 1;

    if ((end - start) == 0)
    {
        return win_printf("No points to work on !");
    }

    return ds_find_all_extrema_data(ds, start, end);
} /* end of the "ds_find_halfmax" function */

float stretch_histogram_max_visible_peak_to_pos(O_p *src_op, d_s *pattern_ds, int k_min, int k_max, float min_value,
        float ref_x, float ref_x_offset)
{
  //float max_pos = -1;
    float y_val = -FLT_MAX;
    float max_y_val = -FLT_MAX;
    int max_index = -1;
    float pos = 0;
    int find_max_ret = 0;
    char buf[1024] = {0};
    k_min = k_min > 0 ? k_min : 1;
    float drx = 1;
    float off;

    if (k_max < k_min)
    {
        return 1;
    }

    for (int k = k_min; k < k_max; ++k)
    {
        if ((pattern_ds->yd[k] > pattern_ds->yd[k - 1]) && (pattern_ds->yd[k] >= pattern_ds->yd[k + 1])
                && (pattern_ds->yd[k] > min_value))
        {
            find_max_ret = find_max_around(pattern_ds->yd, pattern_ds->nx, k, &pos, &y_val, NULL);

            if (find_max_ret == 0 && y_val > max_y_val)
            {
                max_y_val = y_val;
                //max_pos = pos;
                max_index = k;
            }
        }
    }

    if (max_index >= 0)
    {
        // ax = offset, dx = multi, rx = div
        float ax =  0;
        float dx = ref_x - ref_x_offset;
        float rx = (pattern_ds->xd[max_index]) * (src_op->dx == 0 ? 1 : src_op->dx) - ref_x_offset;
        float tmp;
        drx = dx / rx;
        tmp = (src_op->dx != 0) ? ax / src_op->dx : ax;

        for (int i = 0; i < pattern_ds->nx ; i++)
        {
            pattern_ds->xd[i] =  tmp + drx * (pattern_ds->xd[i]) - dx / (src_op->dx == 0 ? 1 : src_op->dx);

            if (pattern_ds->xe)
            {
                pattern_ds->xe[i] = drx * (pattern_ds->xe[i]) - dx / (src_op->dx == 0 ? 1 : src_op->dx);
            }
        }
        off = ref_x / src_op->dx - pattern_ds->xd[max_index];
        for (int i = 0; i < pattern_ds->nx ; i++)
        {
            pattern_ds->xd[i] =  pattern_ds->xd[i] + off;

            if (pattern_ds->xe)
            {
                pattern_ds->xe[i] = pattern_ds->xe[i] + off;
            }
        }

        printf("source; stretch; ref_x; ref_x_off\n\"%s\"; %.3f; %.3f; %.3f\n", pattern_ds->source, drx, ref_x, ref_x_offset);
        snprintf(buf, sizeof(buf), "Bp->Nm: %.3f - %s", 1. / drx, pattern_ds->source);
        free(pattern_ds->source);
        pattern_ds->source = strdup(buf);
    }

    return drx;
}

float offset_histogram_max_visible_peak_to_pos(O_p *src_op, d_s *pattern_ds, int k_min, int k_max, float min_value,
        float ref_x)
{
  //float max_pos = -1;
    float y_val = -FLT_MAX;
    float max_y_val = -FLT_MAX;
    int max_index = -1;
    float pos = 0;
    int find_max_ret = 0;
    char buf[1024] = {0};
    k_min = k_min > 0 ? k_min : 1;
    float off = 0;

    if (k_max < k_min)
    {
        return 1;
    }

    for (int k = k_min; k < k_max; ++k)
    {
        if ((pattern_ds->yd[k] > pattern_ds->yd[k - 1]) && (pattern_ds->yd[k] >= pattern_ds->yd[k + 1])
                && (pattern_ds->yd[k] > min_value))
        {
            find_max_ret = find_max_around(pattern_ds->yd, pattern_ds->ny, k, &pos, &y_val, NULL);

            if (find_max_ret == 0 && y_val > max_y_val)
            {
                max_y_val = y_val;
                //max_pos = pos;
                max_index = k;
            }
        }
    }

    if (max_index >= 0)
    {
        off = ref_x / src_op->dx - pattern_ds->xd[max_index];

        for (int i = 0; i < pattern_ds->nx ; i++)
        {
            pattern_ds->xd[i] =  pattern_ds->xd[i] + off;

            if (pattern_ds->xe)
            {
                pattern_ds->xe[i] = pattern_ds->xe[i] + off;
            }
        }

        printf("source; offset; ref_x\n\"%s\"; %.3f; %.3f\n", pattern_ds->source, off * src_op->dx, ref_x);
        snprintf(buf, sizeof(buf), "offset: %.3f - %s", off * src_op->dx, pattern_ds->source);
        free(pattern_ds->source);
        pattern_ds->source = strdup(buf);
    }

    return off;
}
int apply_stretch_ds_per_source_pattern(char *commonstr, float stretch, O_p *src_op)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    int affected_ds = 0;

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)
    {
        win_printf("cannot find data");
        return 0;
    }

    for (int i = 0; i < pr->n_op; ++i)
    {
        op = pr->o_p[i];

        if (op == src_op)
        {
            continue;
        }

        for (int j = 0; j < op->n_dat; ++j)
        {
            ds = op->dat[j];
            printf("common str |%s| \n", commonstr);

            if (ds->source != NULL && strlen(ds->source) > 0 && strstr(ds->source, commonstr) != NULL)
            {
                printf("enter stretch %f\n", stretch);
                // ax = offset, dx = multi, rx = div
                float ax =  0;
                float tmp;
                tmp = (op->dx != 0) ? ax / op->dx : ax;

                for (int k = 0; k < ds->nx ; k++)
                {
                    ds->xd[k] =  tmp + stretch * ds->xd[k];

                    if (ds->xe)
                    {
                        ds->xe[k] = stretch * ds->xe[k];
                    }
                }

                affected_ds++;
            }
        }
    }

    return affected_ds;
}
int do_shift_histograms(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    static float ref_x = 0;
    int allegro_return = WIN_OK;
    int k_min = -1;
    int k_max = -1;
    static int specific_color = 0;
    static int color = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return win_printf_OK("cannot find data");
    }

    allegro_return = win_scanf("Shift Value ref x value %5f\n %b only a specified color\n",
                               &ref_x, &specific_color);

    if (allegro_return == WIN_CANCEL)
    {
        return OFF;
    }

    if (specific_color)
    {
        color = color_chooser();
    }

    if (color == -1)
    {
        return OFF;
    }

    for (int i = 0; i < op->n_dat; ++i)
    {
        ds = op->dat[i];

        if (!specific_color || ds->color == color)
        {
            k_min = -1;
            k_max = -1;

            for (int j = 0; j  < ds->nx; ++j)
            {
                if (k_min == -1 && ds->xd[j] > op->x_lo)
                {
                    k_min = j;
                }

                if (k_max == -1 && ds->xd[j] > op->x_hi)
                {
                    k_max = j - 1;
                }
            }

            if (k_min == -1)
            {
                continue;
            }

            if (k_max == -1)
            {
                k_max = ds->nx - 1;
            }

            //float offset =
	    offset_histogram_max_visible_peak_to_pos(op, ds, k_min, k_max, op->y_lo, ref_x);
        }
    }

    op->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    return 0;
}

int do_stretch_histograms(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    static float ref_x = 0;
    static float ref_x_offset = 0;
    int allegro_return = WIN_OK;
    int k_min = -1;
    int k_max = -1;
    static int specific_color = 0;
    static int color = 0;
    static int do_propagation = 0;
    static int pattern_type = 1;
    static char form_text[1024] = "";
    static char singlepattern[1024] = "";
    static char prefix[1024] = "Bead";
    static char suffix[1024] = " ";
    static int begin = 0;
    static int end = 1;
    char **patternlist = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return win_printf_OK("cannot find data");
    }

    allegro_return = win_scanf("ref x value %5f\n ref x for offset %5f\n %b only a specified color\n"
                               "%b Propagate to all beads by dataset source\n"
                               "%R Use Pattern define for each dataset\n"
                               "%r Use a range of dataset source pattern\n"
                               "Prefix: %s\n range [%d to %d[\n Suffix %s\n"
                               "X List of pattern (not implemented yet)"
                               "X Use dataset index (not implemented yet)",
                               &ref_x,&ref_x_offset, &specific_color, &do_propagation,
                               &pattern_type, prefix, &begin, &end, suffix);

    if (allegro_return == WIN_CANCEL)
    {
        return OFF;
    }

    if (specific_color)
    {
        color = color_chooser();
    }

    if (color == -1)
    {
        return OFF;
    }

    if (do_propagation && pattern_type == 1)
    {
        if (end <= begin)
        {
            error_message("Invalid range");
            return OFF;
        }

        patternlist = (char **) calloc(end - begin, sizeof(char *));

        for (int i = begin; i < end; ++i)
        {
            snprintf(singlepattern, sizeof(singlepattern), "%s%d%s", prefix, i, suffix);
            patternlist[i - begin] = strdup(singlepattern);
        }
    }

    for (int i = 0; i < op->n_dat; ++i)
    {
        ds = op->dat[i];
        k_min = -1;
        k_max = -1;

        if (!specific_color || ds->color == color)
        {
            for (int j = 0; j  < ds->nx; ++j)
            {
                if (k_min == -1 && ds->xd[j] > op->x_lo)
                {
                    k_min = j;
                }

                if (k_max == -1 && ds->xd[j] > op->x_hi)
                {
                    k_max = j - 1;
                }
            }

            if (k_min == -1)
            {
                continue;
            }

            if (k_max == -1)
            {
                k_max = ds->nx - 1;
            }

            float stretch = stretch_histogram_max_visible_peak_to_pos(op, ds, k_min, k_max, op->y_lo, ref_x, ref_x_offset);

            if (do_propagation)
            {
                if (pattern_type == 0)
                {
                    snprintf(form_text, sizeof(form_text), "Source of current dataset\n\n %s \n\n Pattern %%s", ds->source);
                    int ret = win_scanf(form_text, singlepattern);

                    if (ret != WIN_OK)
                    {
                        return OFF;
                    }

                    apply_stretch_ds_per_source_pattern(singlepattern, stretch, op);
                }
                else if (pattern_type == 1)
                {
                    for (int ii = 0; ii < (end - begin); ++ii)
                    {
                        if (ds->source != NULL && strlen(ds->source) > 0 && strstr(ds->source, patternlist[ii]) != NULL)
                        {
                            apply_stretch_ds_per_source_pattern(singlepattern, stretch, op);
                        }
                    }
                }
            }
        }
    }

    if (do_propagation && patternlist)
    {
        for (int i = 0; i < (end - begin); ++i)
        {
            free(patternlist[i]);
            patternlist[i] = NULL;
        }

        free(patternlist);
    }

    op->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    return 0;
}

/****************************************************************/
/* to find the first local maximum and minimum of a data set    */
/*  and the position of the half maxima         */
/****************************************************************/
int ds_find_all_extrema_data(d_s *ds, int start, int end)
{
    int i;
    float   max_X, max_Y, min_x, min_y;
    float   half_y, px, py, a, b;
    float   log_half_y, db1 = 0, db2 = 0, dc1 = 0, dc2 = 0;
    int     nx;
    char    b1[256] = {0}, b2[256] = {0}, c1[256] = {0}, c2[256] = {0}, bb[256] = {0}, cc[256] = {0};
    nx = end - start;

    if (nx == 0)
    {
        return win_printf("in ds_find_all_extrema_data : \nNo points to work on !");
    }

    max_X = ds->xd[start];
    min_x = ds->xd[start];
    max_Y = ds->yd[start];
    min_y = ds->yd[start];

    for (i = start; i < end; i++)
    {
        if (ds->yd[i] > max_Y)
        {
            max_Y = ds->yd[i];
            max_X = ds->xd[i];
        }

        if (ds->yd[i] < min_y)
        {
            min_y = ds->yd[i];
            min_x = ds->xd[i];
        }
    }

    half_y = (max_Y + min_y) / 2;

    if (min_y > 0)
    {
        log_half_y = (log(max_Y) + log(min_y)) / 2;
    }
    else
    {
        log_half_y = 0;
    }

    /* search for the linear half point before : */
    for (i = start; i < (max_X); i++)
    {
        if (((ds->yd[i] >= half_y) && (ds->yd[i + 1] <= half_y))
                || ((ds->yd[i] <= half_y) && (ds->yd[i + 1] >= half_y)))
        {
            if ((ds->xd[i + 1] - ds->xd[i]) * (ds->yd[i + 1] - ds->yd[i]) != 0)
            {
                a  = (ds->yd[i + 1] - ds->yd[i]) / (ds->xd[i + 1] - ds->xd[i]);
                b  = (ds->yd[i] * ds->xd[i + 1] - ds->yd[i + 1] * ds->xd[i]) / (ds->xd[i + 1] - ds->xd[i]);
                py = half_y;
                px = (py - b) / a;
            }
            else
            {
                px = ds->xd[i];
                py = ds->yd[i];
            }

            sprintf(b1, "lower lin. half is at (%f,%f)", px, py);
            db1 = px;
        }
    }

    /* search for the log half point before : */
    if (log_half_y != 0)
    {
        for (i = start; i < (max_X); i++)
        {
            if ((ds->yd[i] > 0) && (ds->yd[i + 1] > 0))
            {
                if (((log(ds->yd[i]) >= log_half_y) && (log(ds->yd[i + 1]) <= log_half_y))
                        || ((log(ds->yd[i]) <= log_half_y) && (log(ds->yd[i + 1]) >= log_half_y)))
                {
                    if ((ds->xd[i + 1] - ds->xd[i]) * (log(ds->yd[i + 1]) - log(ds->yd[i])) != 0)
                    {
                        a  = (log(ds->yd[i + 1]) - log(ds->yd[i])) / (ds->xd[i + 1] - ds->xd[i]);
                        b  = (log(ds->yd[i]) * ds->xd[i + 1] - log(ds->yd[i + 1]) * ds->xd[i]) / (ds->xd[i + 1] - ds->xd[i]);
                        py = log_half_y;
                        px = (py - b) / a;
                    }
                    else
                    {
                        px = ds->xd[i];
                        py = log(ds->yd[i]);
                    }

                    sprintf(c1, "lower log half is at (%f,%f)", px, py);
                    dc1 = px;
                }
            }
            else
            {
                i = max_X - 1;
            }
        }
    }
    else
    {
        strcpy(c1, "");
    }

    /* search for the lin half point after : */
    for (i = (end - 2); i >= max_X; i--)
    {
        if (((ds->yd[i] >= half_y) && (ds->yd[i + 1] <= half_y))
                || ((ds->yd[i] <= half_y) && (ds->yd[i + 1] >= half_y)))
        {
            if ((ds->xd[i + 1] - ds->xd[i]) * (ds->yd[i + 1] - ds->yd[i]) != 0)
            {
                a  = (ds->yd[i + 1] - ds->yd[i]) / (ds->xd[i + 1] - ds->xd[i]);
                b  = (ds->yd[i] * ds->xd[i + 1] - ds->yd[i + 1] * ds->xd[i]) / (ds->xd[i + 1] - ds->xd[i]);
                py = half_y;
                px = (py - b) / a;
            }
            else
            {
                px = ds->xd[i];
                py = ds->yd[i];
            }

            sprintf(b2, "upper lin. half is at (%f,%f)", px, py);
            db2 = px;
        }
    }

    /* search for the log half point after : */
    if (log_half_y != 0)
    {
        for (i = (end - 2); i >= max_X; i--)
        {
            if ((ds->yd[i] > 0) && (ds->yd[i + 1] > 0))
            {
                if (((log(ds->yd[i]) >= log_half_y) && (log(ds->yd[i + 1]) <= log_half_y))
                        || ((log(ds->yd[i]) <= log_half_y) && (log(ds->yd[i + 1]) >= log_half_y)))
                {
                    if ((ds->xd[i + 1] - ds->xd[i]) * (log(ds->yd[i + 1]) - log(ds->yd[i])) != 0)
                    {
                        a  = (log(ds->yd[i + 1]) - log(ds->yd[i])) / (ds->xd[i + 1] - ds->xd[i]);
                        b  = (log(ds->yd[i]) * ds->xd[i + 1] - log(ds->yd[i + 1]) * ds->xd[i]) / (ds->xd[i + 1] - ds->xd[i]);
                        py = log_half_y;
                        px = (py - b) / a;
                    }
                    else
                    {
                        px = ds->xd[i];
                        py = log(ds->yd[i]);
                    }

                    sprintf(c2, "upper log. half is at (%f,%f)", px, py);
                    dc2 = px;
                }
            }
            else
            {
                i = max_X;
            }
        }
    }
    else
    {
        strcpy(c2, "");
    }

    if ((db1 * db2) != 0)
    {
        sprintf(bb, "=> lin. width = %f", db2 - db1);
    }
    else
    {
        strcpy(bb, "");
    }

    if ((dc1 * dc2) != 0)
    {
        sprintf(cc, "=> log. width = %f", dc2 - dc1);
    }
    else
    {
        strcpy(cc, "");
    }

    win_printf("for visible points (%d,%d) of current data set :\n"
               "the point of maximum y is (%f,%f)\n"
               "the point of minimum y is (%f,%f)\n\n"
               "%s \n"
               "%s \n"
               "%s \n\n"
               "%s \n"
               "%s \n"
               "%s \n\n",
               start, end, max_X, max_Y, min_x, min_y, b1, b2, bb, c1, c2, cc);
    return (D_O_K);
} /* end of the "ds_find_all_extrema_data" function */
