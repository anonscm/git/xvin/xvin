# include <stdlib.h>
# include <string.h>

# include "xvin.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

# include "plot_mn.h"
# include "matrice.h"

double chi2_in_fit_ds_to_xn_polynome(d_s *ds, int n, float x_lo, float x_hi, float y_lo, float y_hi, double *a,
                                     int *nf);


int     fit_poly(void)
{
    int i, j, k;
    int icp, nx, np;
    static int n = 2;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dss = NULL;
    double *a, dx, x, y, x1, yt;
    float er;

    if (updating_menu_state != 0)  return D_O_K;

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)
    {
        win_printf("cannot find data");
        return D_O_K;
    }

    i = win_scanf("y = \\Sigma_{i=0}^{i<=n} a_i x^i \n degree of polynome n %d", &n);

    if (i == WIN_CANCEL)  return OFF;

    nx = fit_ds_to_xn_polynome(dsi, n + 1, op->x_lo, op->x_hi, op->y_lo, op->y_hi, &a);

    if (nx < 0)
    {
        win_printf("something went wrong!");
        return D_O_K;
    }

    for (i = 0, icp = -1; i < op->n_dat && icp == -1; i++)
    {
        if (op->dat[i] != NULL)
        {
            dss = op->dat[i];
            icp = ((dss->treatement != NULL) && strncmp(dss->treatement, "least square fit \\Sigma_0^na_nx^n", 25) == 0) ? i : -1;
        }
    }

    nx = (nx < 512) ? 512 : nx;

    if (icp == -1)
    {
        i = op->cur_dat;
        dss = create_and_attach_one_ds(op, nx, nx, 0);
        dss->treatement = my_sprintf(dss->treatement, "least square fit \\Sigma_0^na_nx^n");
        op->cur_dat = i;
    }

    if (dss == NULL)
    {
        win_printf("can't create data set");
        return D_O_K;
    }

    if (nx != dss->nx || nx != dss->ny)
    {
        win_printf("data set has not the right size");
        return D_O_K;
    }

    if (nx == 512)
    {
        for (i = 0, dx = (op->x_hi - op->x_lo) / nx ; i < nx ; i++)
        {
            x = op->x_lo + dx * i;
            dss->xd[i] = (float)x;
            yt = 0;

            for (j = 0; j <= n; j++)
            {
                for (np = 0, x1 = a[j]; np < j; np++)
                    x1 *= x;

                yt += x1;
            }

            dss->yd[i] = (float)yt;
        }
    }
    else
    {
        for (i = 0, j = 0 ; i < nx && j < dsi->nx ; j++)
        {
            x = dsi->xd[j];
            y = dsi->yd[j];

            if (x < op->x_hi && x >= op->x_lo && y < op->y_hi && y >= op->y_lo)
            {
                dss->xd[i] = (float)x;
                yt = 0;

                for (k = 0; k <= n; k++)
                {
                    for (np = 0, x1 = a[k]; np < k; np++)
                        x1 *= x;

                    yt += x1;
                }

                dss->yd[i] = (float)yt;
                i++;
            }
        }
    }

    er = error_in_fit_ds_to_xn_polynome(dsi, n + 1, op->x_lo, op->x_hi, op->y_lo, op->y_hi, a);
    add_plot_label_for_poly_coeff(op, a, n, er);
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

int     rm_poly(void)
{
    int i, j, k;
    int icp, nx, np;
    static int n = 2;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dss = NULL;
    double *a = NULL, x, y, x1, yt;

    if (updating_menu_state != 0)  return D_O_K;

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)
    {
        win_printf("cannot find data");
        return D_O_K;
    }

    i = win_scanf("y = \\Sigma_{i=0}^{i<=n} a_i x^i \n degree of polynome n %d", &n);

    if (i == WIN_CANCEL)  return OFF;

    nx = fit_ds_to_xn_polynome(dsi, n + 1, op->x_lo, op->x_hi, op->y_lo, op->y_hi, &a);

    if (nx < 0)
    {
        win_printf("something went wrong!");
        return D_O_K;
    }

    for (i = 0, icp = -1; i < op->n_dat && icp == -1; i++)
    {
        if (op->dat[i] != NULL)
        {
            dss = op->dat[i];
            icp = ((dss->treatement != NULL) &&
                   strncmp(dss->treatement, "data - least square fit \\Sigma_0^na_nx^n", 25) == 0) ? i : -1;
        }
    }

    if (icp == -1)
    {
        i = op->cur_dat;
        dss = create_and_attach_one_ds(op, nx, nx, 0);
        set_ds_treatement(dss, "data - least square fit \\Sigma_0^na_nx^n");
        op->cur_dat = i;
    }

    if (dss == NULL)
    {
        win_printf("can't create data set");
        return D_O_K;
    }

    if (nx != dss->nx || nx != dss->ny)
    {
        win_printf("data set has not the right size");
        return D_O_K;
    }

    for (i = 0, j = 0 ; i < nx && j < dsi->nx ; j++)
    {
        x = dsi->xd[j];
        y = dsi->yd[j];

        if (x < op->x_hi && x >= op->x_lo && y < op->y_hi && y >= op->y_lo)
        {
            dss->xd[i] = (float)x;
            yt = 0;

            for (k = 0; k <= n; k++)
            {
                for (np = 0, x1 = a[k]; np < k; np++)
                    x1 *= x;

                yt -= x1;
            }

            dss->yd[i] = (float)(y + yt);
            i++;
        }
    }

    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}
int add_plot_label_for_order_1_poly_old(O_p *op, double *a, float er)
{
  char st[1024] = {0};

    snprintf(st, 1024, "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
             "{a_0 =  %e}{a_1 = %e}{er = %e}}}", 1, a[0], a[1], er);
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo) / 4,
                    op->y_hi - (op->y_hi - op->y_lo) / 4, st, USR_COORD);
    return 0;
}
int add_plot_label_for_order_1_poly(O_p *op, double *a, float er)
{
# ifdef OLD
  char st[1024] = {0};

    snprintf(st, 1024, "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
             "{a_0 =  %e}{a_1 = %e}{er = %e}{in plot unit %s / %s}{a_0 =  %e}"
             "{a_1 = %e}{er = %e}}}", 1, a[0], a[1], er, (op->x_unit) ? op->x_unit : "no name",
             (op->y_unit) ? op->y_unit : "no name", op->ay + (a[0]*op->dy),
             a[1]*op->dy / op->dx, er * op->dy);
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo) / 4,
                    op->y_hi - (op->y_hi - op->y_lo) / 4, st, USR_COORD);

# endif
    set_ds_plot_label(op->dat[op->n_dat - 1], op->x_lo + (op->x_hi - op->x_lo) / 4,
                      op->y_hi - (op->y_hi - op->y_lo) / 4, USR_COORD,
                      "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
                      "{a_0 =  %e}{a_1 = %e}{er = %e}{in plot unit %s / %s}{a_0 =  %e}"
                      "{a_1 = %e}{er = %e}}}", 1, a[0], a[1], er, (op->x_unit) ? op->x_unit : "no name",
                      (op->y_unit) ? op->y_unit : "no name", op->ay + (a[0]*op->dy),
                      a[1]*op->dy / op->dx, er * op->dy);
    return 0;
}

int add_plot_label_for_order_2_poly(O_p *op, double *a, float er)
{
# ifdef OLD
  char st[1024] = {0};

    snprintf(st, 1024, "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
             "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{er = %e}}}", 2, a[0], a[1], a[2], er);
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo) / 4,
                    op->y_hi - (op->y_hi - op->y_lo) / 4, st, USR_COORD);
# endif
    set_ds_plot_label(op->dat[op->n_dat - 1], op->x_lo + (op->x_hi - op->x_lo) / 4,
                      op->y_hi - (op->y_hi - op->y_lo) / 4, USR_COORD,
                      "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
                      "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{er = %e}}}", 2, a[0], a[1], a[2], er);
    return 0;
}
int add_plot_label_for_order_3_poly(O_p *op, double *a, float er)
{
# ifdef OLD
    char st[1024];

    snprintf(st, 1024, "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
             "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{er = %e}}}", 3, a[0], a[1], a[2],
             a[3], er);
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo) / 4,
                    op->y_hi - (op->y_hi - op->y_lo) / 4, st, USR_COORD);
# endif
    set_ds_plot_label(op->dat[op->n_dat - 1], op->x_lo + (op->x_hi - op->x_lo) / 4,
                      op->y_hi - (op->y_hi - op->y_lo) / 4, USR_COORD,
                      "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
                      "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{er = %e}}}", 3, a[0], a[1], a[2],
                      a[3], er);
    return 0;
}
int add_plot_label_for_order_4_poly(O_p *op, double *a,  float er)
{
# ifdef OLD
    char st[1024];

    snprintf(st, 1024, "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
             "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{a_4 = %e}{er = %e}}}", 4, a[0], a[1],
             a[2], a[3], a[4], er);
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo) / 4,
                    op->y_hi - (op->y_hi - op->y_lo) / 4, st, USR_COORD);
# endif
    set_ds_plot_label(op->dat[op->n_dat - 1], op->x_lo + (op->x_hi - op->x_lo) / 4,
                      op->y_hi - (op->y_hi - op->y_lo) / 4, USR_COORD,
                      "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
                      "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{a_4 = %e}{er = %e}}}", 4, a[0], a[1],
                      a[2], a[3], a[4], er);
    return 0;
}
int add_plot_label_for_order_5_poly(O_p *op, double *a, float er)
{
# ifdef OLD
    char st[1024];

    snprintf(st, 1024, "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
             "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{a_4 = %e}{a_5 = %e}{er = %e}}}"
             , 5, a[0], a[1], a[2], a[3], a[4], a[5], er);
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo) / 4,
                    op->y_hi - (op->y_hi - op->y_lo) / 4, st, USR_COORD);
# endif
    set_ds_plot_label(op->dat[op->n_dat - 1], op->x_lo + (op->x_hi - op->x_lo) / 4,
                      op->y_hi - (op->y_hi - op->y_lo) / 4, USR_COORD,
                      "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
                      "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{a_4 = %e}{a_5 = %e}{er = %e}}}"
                      , 5, a[0], a[1], a[2], a[3], a[4], a[5], er);
    return 0;
}
int add_plot_label_for_order_6_poly(O_p *op, double *a, float er)
{
# ifdef OLD
    char st[1024];

    snprintf(st, 1024, "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
             "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{a_4 = %e}{a_5 = %e}"
             "{a_6 = %e}{er = %e}}}", 6, a[0], a[1], a[2], a[3], a[4], a[5], a[6], er);
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo) / 4,
                    op->y_hi - (op->y_hi - op->y_lo) / 4, st, USR_COORD);
# endif
    set_ds_plot_label(op->dat[op->n_dat - 1], op->x_lo + (op->x_hi - op->x_lo) / 4,
                      op->y_hi - (op->y_hi - op->y_lo) / 4, USR_COORD,
                      "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
                      "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{a_4 = %e}{a_5 = %e}"
                      "{a_6 = %e}{er = %e}}}", 6, a[0], a[1], a[2], a[3], a[4], a[5], a[6], er);
    return 0;
}
int add_plot_label_for_order_7_poly(O_p *op, double *a, float er)
{
# ifdef OLD
    char st[1024];

    snprintf(st, 1024, "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
             "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{a_4 = %e}{a_5 = %e}{a_6 = %e}"
             "{a_7 = %e}{er = %e}}}", 7, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], er);
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo) / 4,
                    op->y_hi - (op->y_hi - op->y_lo) / 4, st, USR_COORD);
# endif
    set_ds_plot_label(op->dat[op->n_dat - 1], op->x_lo + (op->x_hi - op->x_lo) / 4,
                      op->y_hi - (op->y_hi - op->y_lo) / 4, USR_COORD,
                      "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
                      "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{a_4 = %e}{a_5 = %e}{a_6 = %e}"
                      "{a_7 = %e}{er = %e}}}", 7, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], er);
    return 0;
}
int add_plot_label_for_order_8_poly(O_p *op, double *a, int n, float er)
{
# ifdef OLD
    char st[1024];

    snprintf(st, 1024, "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
             "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{a_4 = %e}{a_5 = %e}{a_6 = %e}"
             "{a_7 = %e}{a_8 = %e}{er = %e}}}", n, a[0], a[1], a[2], a[3], a[4], a[5],
             a[6], a[7], a[8], er);
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo) / 4,
                    op->y_hi - (op->y_hi - op->y_lo) / 4, st, USR_COORD);
# endif
    set_ds_plot_label(op->dat[op->n_dat - 1], op->x_lo + (op->x_hi - op->x_lo) / 4,
                      op->y_hi - (op->y_hi - op->y_lo) / 4, USR_COORD,
                      "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }"
                      "{a_0 =  %e}{a_1 = %e}{a_2 = %e}{a_3 = %e}{a_4 = %e}{a_5 = %e}{a_6 = %e}"
                      "{a_7 = %e}{a_8 = %e}{er = %e}}}", n, a[0], a[1], a[2], a[3], a[4], a[5],
                      a[6], a[7], a[8], er);
    return 0;
}
int add_plot_label_for_poly_coeff(O_p *op, double *a, int n, float er)
{

    if (n == 1)   return add_plot_label_for_order_1_poly(op, a, er);
    else if (n == 2)  return add_plot_label_for_order_2_poly(op, a, er);
    else if (n == 3)  return add_plot_label_for_order_3_poly(op, a, er);
    else if (n == 4)  return add_plot_label_for_order_4_poly(op, a, er);
    else if (n == 5)  return add_plot_label_for_order_5_poly(op, a, er);
    else if (n == 6)  return add_plot_label_for_order_6_poly(op, a, er);
    else if (n == 7)  return add_plot_label_for_order_7_poly(op, a, er);
    else if (n >= 8)  return add_plot_label_for_order_8_poly(op, a, n, er);
    else return 1;
}


int add_plot_label_poly_chi2(O_p *op, double *a, int n, double chi2, int nptfit)
{
    int i, l, rl;
    char st[2048] = {0};

    snprintf(st, sizeof(st), "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<=%d} a_i x^i }", n);

    for (i = 0; i <= n; i++)
    {
        l = strlen(st);
        rl = sizeof(st) - l;
        snprintf(st + l, rl, "{a_%d =  %g}", i, a[i]);
        //win_printf("i %d l %d \n st %s",i,l,st);
    }

    l = strlen(st);
    rl = sizeof(st) - l;
    snprintf(st + l, rl, "{\\chi^2 =  %g}{n = %d}}}", chi2, nptfit - n);
    //win_printf(st);
    set_ds_plot_label(op->dat[op->n_dat - 1], op->x_lo + (op->x_hi - op->x_lo) / 4,
                      op->y_hi - (op->y_hi - op->y_lo) / 4, USR_COORD, "%s",st);

    return 0;
}

int fit_all_ds_to_xn_polynome(d_s *ds, int n, double **a)
{
    int i, j, k;
    int npt, id, nx = 0, np;
    int status, ret;
    double *m = NULL, *l = NULL, *b = NULL, *x = NULL, ax, ay, x1;

    if (ds == NULL)       return -1;

    npt = n * (n + 1) / 2;

    m = (double *) calloc(npt, sizeof(double));
    l = (double *) calloc(npt, sizeof(double));
    b = (double *) calloc(n, sizeof(double));
    x = (double *) calloc(n, sizeof(double));

    for (k = 0, i = 0, j = 0 ; k < npt ; k++)
    {
        for (id = nx = 0, m[k] = 0; id < ds->nx; id++)
        {
            ax = ds->xd[id];
            ay = ds->yd[id];
            x1 = 1;

            for (np = 0; np < (j + i); np++)
                x1 *= ax;

            m[k] += x1;
        }

        j++;

        if (j == n) j = ++i;
    }

    for (i = 0 ; i < n ; i++)
    {
        for (id = nx = 0, b[i] = 0; id < ds->nx; id++)
        {
            ax = ds->xd[id];
            x1 = ay = ds->yd[id];

            for (np = 0; np < i; np++)
                x1 *= ax;

            nx++;
            b[i] += x1;
        }
    }

    ret = (nx < n) ? -2 : nx;
    status = sym_matrix_to_triang(n, m, l);
    ret = (status ==  ERR_NEGATIVE_SQRT) ? -3 : nx;
    sym_matrix_solve(n, l, b, x);
    ret = (status ==  NULL_DENOMINATOR) ? -3 : nx;
    sym_matrix_improv(n, l, b, x);
    ret = (status ==  NULL_DENOMINATOR) ? -3 : nx;
    *a = x;

    if (ret < 0 && x != NULL)  free(x);

    if (l) free(l);

    if (m) free(m);

    if (b) free(b);

    return ret;
}
// to fit to a polynome of order p set n tp p + 1 
int fit_ds_to_xn_polynome(d_s *ds, int n, float x_lo, float x_hi, float y_lo, float y_hi, double **a)
{
    int i, j, k;
    int npt, id, nx = 0, np;
    int status, ret;
    double *m = NULL, *l = NULL, *b = NULL, *x = NULL, ax, ay, x1;

    if (ds == NULL)       return -1;

    npt = n * (n + 1) / 2;

    m = (double *) calloc(npt, sizeof(double));
    l = (double *) calloc(npt, sizeof(double));
    b = (double *) calloc(n, sizeof(double));
    x = (double *) calloc(n, sizeof(double));

    for (k = 0, i = 0, j = 0 ; k < npt ; k++)
    {
        for (id = nx = 0, m[k] = 0; id < ds->nx; id++)
        {
            ax = ds->xd[id];
            ay = ds->yd[id];
            x1 = 1;

            if (ax < x_hi && ax >= x_lo && ay < y_hi && ay >= y_lo)
            {
                for (np = 0; np < (j + i); np++)
                    x1 *= ax;

                m[k] += x1;
            }
        }

        j++;

        if (j == n) j = ++i;
    }

    for (i = 0 ; i < n ; i++)
    {
        for (id = nx = 0, b[i] = 0; id < ds->nx; id++)
        {
            ax = ds->xd[id];
            x1 = ay = ds->yd[id];

            if (ax < x_hi && ax >= x_lo && ay < y_hi && ay >= y_lo)
            {
                for (np = 0; np < i; np++)
                    x1 *= ax;

                nx++;
                b[i] += x1;
            }
        }
    }

    /*    for (i=0 ; i<npt ; i++) printf("m(%d) = %g\n",i,m[i]);
    for (i=0 ; i<n ; i++) printf("b(%d) = %g\n",i,b[i]);*/
    ret = (nx < n) ? -2 : nx;
    status = sym_matrix_to_triang(n, m, l);
    ret = (status ==  ERR_NEGATIVE_SQRT) ? -3 : nx;
    sym_matrix_solve(n, l, b, x);
    ret = (status ==  NULL_DENOMINATOR) ? -3 : nx;
    sym_matrix_improv(n, l, b, x);
    ret = (status ==  NULL_DENOMINATOR) ? -3 : nx;
    /*    for (i=0 ; i<n ; i++)   printf("x(%d) = %g\n",i,x[i]);*/
    *a = x;

    if (ret < 0 && x != NULL)  free(x);

    if (l) free(l);

    if (m) free(m);

    if (b) free(b);

    return ret;
}


int fit_ds_to_xn_polynome_er(d_s *ds, int n, float x_lo, float x_hi, float y_lo, float y_hi, double **a)
{
    int i, j, k;
    int npt, id, nx = 0, np;
    int status, ret;
    double *m = NULL, *l = NULL, *b = NULL, *x = NULL, ax, ay, x1, er, sig2_1;

    if (ds == NULL)       return -1;

    npt = n * (n + 1) / 2;

    m = (double *) calloc(npt, sizeof(double));
    l = (double *) calloc(npt, sizeof(double));
    b = (double *) calloc(n, sizeof(double));
    x = (double *) calloc(n, sizeof(double));

    for (k = 0, i = 0, j = 0 ; k < npt ; k++)
    {
        for (id = nx = 0, m[k] = 0; id < ds->nx; id++)
        {
            ax = ds->xd[id];
            ay = ds->yd[id];
            er = (ds->ye == NULL) ? 1 : ds->ye[id];
            sig2_1 = (er >= 0) ? (double)1 / (er * er) : 1;
            x1 = sig2_1;

            if (ax < x_hi && ax >= x_lo && ay < y_hi && ay >= y_lo)
            {
                for (np = 0; np < (j + i); np++)
                    x1 *= ax;

                m[k] += x1;
            }
        }

        j++;

        if (j == n) j = ++i;
    }

    for (i = 0 ; i < n ; i++)
    {
        for (id = nx = 0, b[i] = 0; id < ds->nx; id++)
        {
            ax = ds->xd[id];
            x1 = ay = ds->yd[id];
            er = (ds->ye == NULL) ? 1 : ds->ye[id];
            sig2_1 = (er >= 0) ? (double)1 / (er * er) : 1;
            x1 *= sig2_1;

            if (ax < x_hi && ax >= x_lo && ay < y_hi && ay >= y_lo)
            {
                for (np = 0; np < i; np++)
                    x1 *= ax;

                nx++;
                b[i] += x1;
            }
        }
    }

    /*    for (i=0 ; i<npt ; i++) printf("m(%d) = %g\n",i,m[i]);
    for (i=0 ; i<n ; i++) printf("b(%d) = %g\n",i,b[i]);*/
    ret = (nx < n) ? -2 : nx;
    status = sym_matrix_to_triang(n, m, l);
    ret = (status ==  ERR_NEGATIVE_SQRT) ? -3 : nx;
    sym_matrix_solve(n, l, b, x);
    ret = (status ==  NULL_DENOMINATOR) ? -3 : nx;
    sym_matrix_improv(n, l, b, x);
    ret = (status ==  NULL_DENOMINATOR) ? -3 : nx;
    /*    for (i=0 ; i<n ; i++)   printf("x(%d) = %g\n",i,x[i]);*/
    *a = x;

    if (ret < 0 && x != NULL)  free(x);

    if (l) free(l);

    if (m) free(m);

    if (b) free(b);

    return ret;
}


int fit_poly_er(void)
{
    int i, j, k;
    int icp, nx, np;
    static int n = 2, nf = 0;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dss = NULL;
    double *a = NULL, dx, x, y, x1, yt;
    double er;

    if (updating_menu_state != 0)  return D_O_K;

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)
    {
        win_printf("cannot find data");
        return D_O_K;
    }

    i = win_scanf("y = \\Sigma_{i=0}^{i<=n} a_i x^i \n degree of polynome n %d", &n);

    if (i == WIN_CANCEL)  return OFF;

    nx = fit_ds_to_xn_polynome_er(dsi, n + 1, op->x_lo, op->x_hi, op->y_lo, op->y_hi, &a);

    if (nx < 0)
    {
        win_printf("something went wrong!");
        return D_O_K;
    }

    for (i = 0, icp = -1; i < op->n_dat && icp == -1; i++)
    {
        if (op->dat[i] != NULL)
        {
            dss = op->dat[i];
            icp = ((dss->treatement != NULL) && strncmp(dss->treatement, "least square fit \\Sigma_0^na_nx^n", 25) == 0) ? i : -1;
        }
    }

    nx = (nx < 512) ? 512 : nx;

    if (icp == -1)
    {
        i = op->cur_dat;
        dss = create_and_attach_one_ds(op, nx, nx, 0);
        dss->treatement = my_sprintf(dss->treatement, "least square fit \\Sigma_0^na_nx^n");
        op->cur_dat = i;
    }

    if (dss == NULL)
    {
        win_printf("can't create data set");
        return D_O_K;
    }

    if (nx != dss->nx || nx != dss->ny)
    {
        win_printf("data set has not the right size");
        return D_O_K;
    }

    if (nx == 512)
    {
        for (i = 0, dx = (op->x_hi - op->x_lo) / nx ; i < nx ; i++)
        {
            x = op->x_lo + dx * i;
            dss->xd[i] = (float)x;
            yt = 0;

            for (j = 0; j <= n; j++)
            {
                for (np = 0, x1 = a[j]; np < j; np++)
                    x1 *= x;

                yt += x1;
            }

            dss->yd[i] = (float)yt;
        }
    }
    else
    {
        for (i = 0, j = 0 ; i < nx && j < dsi->nx ; j++)
        {
            x = dsi->xd[j];
            y = dsi->yd[j];

            if (x < op->x_hi && x >= op->x_lo && y < op->y_hi && y >= op->y_lo)
            {
                dss->xd[i] = (float)x;
                yt = 0;

                for (k = 0; k <= n; k++)
                {
                    for (np = 0, x1 = a[k]; np < k; np++)
                        x1 *= x;

                    yt += x1;
                }

                dss->yd[i] = (float)yt;
                i++;
            }
        }
    }

    er = chi2_in_fit_ds_to_xn_polynome(dsi, n + 1, op->x_lo, op->x_hi, op->y_lo, op->y_hi, a, &nf);
    add_plot_label_poly_chi2(op, a, n, er, nf);
    //add_plot_label_for_poly_coeff(op, a, n,er);
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}



int fit_ds_to_xn_polynome_recenter(d_s *ds, int n, float x_lo, float x_hi, float y_lo, float y_hi, double **a,
                                   float centerx)
{
    int i, j, k;
    int npt, id, nx = 0, np;
    int status, ret;
    double *m = NULL, *l = NULL, *b = NULL, *x = NULL, ax, ay, x1;

    if (ds == NULL)       return -1;

    npt = n * (n + 1) / 2;

    m = (double *) calloc(npt, sizeof(double));
    l = (double *) calloc(npt, sizeof(double));
    b = (double *) calloc(n, sizeof(double));
    x = (double *) calloc(n, sizeof(double));

    for (k = 0, i = 0, j = 0 ; k < npt ; k++)
    {
        for (id = nx = 0, m[k] = 0; id < ds->nx; id++)
        {
            ax = ds->xd[id];
            ay = ds->yd[id];
            x1 = 1;

            if (ax < x_hi && ax >= x_lo && ay < y_hi && ay >= y_lo)
            {
                for (np = 0; np < (j + i); np++)
                    x1 *= ax - centerx;

                m[k] += x1;
            }
        }

        j++;

        if (j == n) j = ++i;
    }

    for (i = 0 ; i < n ; i++)
    {
        for (id = nx = 0, b[i] = 0; id < ds->nx; id++)
        {
            ax = ds->xd[id];
            x1 = ay = ds->yd[id];

            if (ax < x_hi && ax >= x_lo && ay < y_hi && ay >= y_lo)
            {
                for (np = 0; np < i; np++)
                    x1 *= ax - centerx;

                nx++;
                b[i] += x1;
            }
        }
    }

    /*    for (i=0 ; i<npt ; i++) printf("m(%d) = %g\n",i,m[i]);
    for (i=0 ; i<n ; i++) printf("b(%d) = %g\n",i,b[i]);*/
    ret = (nx < n) ? -2 : nx;
    status = sym_matrix_to_triang(n, m, l);
    ret = (status ==  ERR_NEGATIVE_SQRT) ? -3 : nx;
    sym_matrix_solve(n, l, b, x);
    ret = (status ==  NULL_DENOMINATOR) ? -3 : nx;
    sym_matrix_improv(n, l, b, x);
    ret = (status ==  NULL_DENOMINATOR) ? -3 : nx;
    /*    for (i=0 ; i<n ; i++)   printf("x(%d) = %g\n",i,x[i]);*/
    *a = x;

    if (ret < 0 && x != NULL)  free(x);

    if (l) free(l);

    if (m) free(m);

    if (b) free(b);

    return ret;
}
float error_in_fit_ds_to_xn_polynome(d_s *ds, int n, float x_lo, float x_hi, float y_lo, float y_hi, double *a)
{
    int id, j, np;
    int  nx = 0;
    float ax, ay;
    double  x1, er, yt;

    if (ds == NULL)       return -1;

    for (id = nx = 0, er = 0; id < ds->nx; id++)
    {
        ax = ds->xd[id];
        ay = ds->yd[id];

        if (ax < x_hi && ax >= x_lo && ay < y_hi && ay >= y_lo)
        {
            for (j = 0, yt = 0; j < n; j++)
            {
                for (np = 0, x1 = a[j]; np < j; np++)
                    x1 *= ax;

                yt += x1;
            }

            er += ((double)ay - yt) * ((double)ay - yt);
            nx++;
        }
    }

    return (nx > 0) ? (float)sqrt(er / nx) : 0;
}

double chi2_in_fit_ds_to_xn_polynome(d_s *ds, int n, float x_lo, float x_hi, float y_lo, float y_hi, double *a, int *nf)
{
    int id, j, np;
    int  nx = 0;
    float ax, ay;
    double  x1, er, yt, err, sig2_1;

    if (ds == NULL)       return -1;

    for (id = nx = 0, er = 0; id < ds->nx; id++)
    {
        ax = ds->xd[id];
        ay = ds->yd[id];
        err = (ds->ye == NULL) ? 1 : ds->ye[id];
        sig2_1 = (err > 0) ? (double)1 / (err * err) : 1;

        if (ax < x_hi && ax >= x_lo && ay < y_hi && ay >= y_lo)
        {
            for (j = 0, yt = 0; j < n; j++)
            {
                for (np = 0, x1 = a[j]; np < j; np++)
                    x1 *= ax;

                yt += x1;
            }

            er += sig2_1 * ((double)ay - yt) * ((double)ay - yt);
            nx++;
        }
    }

    if (nf) *nf = nx;

    return er;
}

int     fit_poly_local(void)
{
    int i;
    float zmin;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL;
    static float wfit = 10;
    static int n = 5;

    if (updating_menu_state != 0)  return D_O_K;

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)
    {
        win_printf("cannot find data");
        return D_O_K;
    }

    i = win_scanf("y = \\Sigma_{i=0}^{i<=n} a_i x^i \n"
                  "degree of polynome n %dWidth of fit %f", &n, &wfit);

    if (i == WIN_CANCEL)  return OFF;

    fit_poly_local_op(op, op->cur_dat, wfit, n + 1, &zmin);
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

int     fit_poly_local_op(O_p *op, int n_data_set, float wfit, int n, float *zmin)
{
    int i, j, k;
    int icp, nx, nx0, imin, np, nxd;
    float min;
    d_s *dsi = NULL, *dss = NULL;
    float xminfit, xmaxfit;
    double *a = NULL, dx, x, y, x1, yt, xmin = 0;

    char st[256];

    if (op == NULL || n_data_set > op->n_dat) return 1;

    dsi = op->dat[n_data_set];

    for (i = 0, min = dsi->yd[0], imin = 0; i < dsi->nx && i < dsi->ny; i++)
    {
        if (dsi->yd[i] < min)
        {
            min = dsi->yd[i];
            imin = i;
        }
    }

    xminfit = dsi->xd[imin] - wfit / 2;
    xmaxfit = dsi->xd[imin] + wfit / 2;
    /*    win_printf("%g < min %g < %g",xminfit,dsi->xd[imin],xmaxfit); */
    nx0 = nx = fit_ds_to_xn_polynome_recenter(dsi, n, xminfit, xmaxfit, -1e34, 1e34, &a, dsi->xd[imin]);

    if (nx < 0)       return win_printf("Null Determinant!");

    for (i = 0, icp = -1; i < op->n_dat && icp == -1; i++)
    {
        if (op->dat[i] != NULL)
        {
            dss = op->dat[i];
            icp = ((dss->treatement != NULL) && strncmp(dss->treatement,
                    "least square fit \\Sigma_0^na_nx^n", 25) == 0) ? i : -1;
        }
    }

    nxd = (nx < 512) ? 512 : nx;

    if (icp == -1)
    {
        i = op->cur_dat;
        dss = create_and_attach_one_ds(op, nxd, nxd, 0);
        set_ds_treatement(dss, "least square fit \\Sigma_0^na_nx^n");
        op->cur_dat = i;
    }

    if (dss == NULL)
    {
        win_printf("can't create data set");
        return D_O_K;
    }

    if (nxd != dss->nx || nxd != dss->ny)
    {
        win_printf("data set has not the right size");
        return D_O_K;
    }

    if (nxd == 512)
    {
        for (i = 0, dx = (xmaxfit - xminfit) / nxd ; i < nxd ; i++)
        {
            x = xminfit + dx * i;
            dss->xd[i] = (float)x;
            yt = 0;

            for (j = 0; j < n; j++)
            {
                for (np = 0, x1 = a[j]; np < j; np++)
                    x1 *= x - dsi->xd[imin];

                yt += x1;
            }

            dss->yd[i] = (float)yt;

            if (i == 0 || (float)yt < min)
            {
                min = (float)yt;
                xmin = x;
            }
        }
    }
    else
    {
        for (i = 0, j = 0 ; i < nxd && j < dsi->nx ; j++)
        {
            x = dsi->xd[j];
            y = dsi->yd[j];

            if (x < xmaxfit && x >= xminfit && y < op->y_hi && y >= op->y_lo)
            {
                dss->xd[i] = (float)x;
                yt = 0;

                for (k = 0; k < n; k++)
                {
                    for (np = 0, x1 = a[k]; np < k; np++)
                        x1 *= (x - dsi->xd[imin]);

                    yt += x1;
                }

                dss->yd[i] = (float)yt;

                if (i == 0 || (float)yt < min)
                {
                    min = (float)yt;
                    xmin = x;
                }

                i++;
            }
        }
    }

    xminfit = (float)xmin - wfit / 2;
    xmaxfit = (float)xmin + wfit / 2;
    /*    win_printf("%g < min = %g < %g",xminfit,xmin,xmaxfit); */
    /*
    nx0 = nx = fit_ds_to_xn_polynome(dsi, n, xminfit,xmaxfit, -1e34, 1e34, &a);
    if (nx < 0)
    {
    win_printf("Null Determinant!");
    return D_O_K;
    }
    if (nxd == 512)
    {
    for (i=0, dx = (xmaxfit - xminfit) / nxd ; i< nxd ; i++)
    {
    x = xminfit + dx * i;
    dss->xd[i] = (float)x;
    yt = 0;
    for (j = 0; j < n; j++)
    {
    for (np = 0, x1 = a[j]; np < j; np++)
    x1 *= x;
    yt += x1;
    }
    dss->yd[i] = (float)yt;
    if (i == 0 || (float)yt < min)
    {
    min = (float)yt;
    xmin = x;
    }
    }
    }
    else
    {
    for (i=0, j = 0 ; i< nxd && j < dsi->nx ; j++)
    {
    x = dsi->xd[j];
    y = dsi->yd[j];
    if (x < xmaxfit && x >= xminfit && y < op->y_hi && y >= op->y_lo)
    {
    dss->xd[i] = (float)x;
    yt = 0;
    for (k = 0; k < n; k++)
    {
    for (np = 0, x1 = a[k]; np < k; np++)
    x1 *= x;
    yt += x1;
    }
    dss->yd[i] = (float)yt;
    if (i == 0 || (float)yt < min)
    {
    min = (float)yt;
    xmin = x;
    }
    i++;
    }
    }
    }
    */
    snprintf(st, 256, "\\fbox{\\stack{{\\sl y = \\Sigma_{i=0}^{i<%d} a_i x^i }"
             "{nx =  %d}{Z_{min/max} = %g -> %g %s}{width = %g}}}", n, nx0, xmin, op->ax + op->dx * xmin,
             (op->x_unit) ? op->x_unit : " ", wfit);
    push_plot_label(op, op->x_lo + (op->x_hi - op->x_lo) / 4,
                    op->y_hi - (op->y_hi - op->y_lo) / 4, st, USR_COORD);

    if (a != NULL) free(a);

    *zmin = xmin;
    return 0;
}

int find_local_min(d_s *dsi, int n, int imin, int wfit, float *xmin, float *ymin)
{
    int i, j;
    float xminfit, xmaxfit;
    int nx, nxd, np;
    double *a = NULL, dx, x, yt, x1;

    xminfit = (imin - wfit / 2 < 0) ? dsi->xd[0] : dsi->xd[imin - wfit / 2];
    xmaxfit = (imin + wfit / 2 >= dsi->nx) ? dsi->xd[dsi->nx - 1] :
              dsi->xd[imin + wfit / 2];

    nx = fit_ds_to_xn_polynome_recenter(dsi, n, xminfit, xmaxfit, -1e34, 1e34,
                                        &a, dsi->xd[imin]);

    if (nx < 0)       return 1;

    nxd = 512;

    for (i = 0, dx = (xmaxfit - xminfit) / nxd ; i < nxd ; i++)
    {
        x = xminfit + dx * i;
        yt = 0;

        for (j = 0; j < n; j++)
        {
            for (np = 0, x1 = a[j]; np < j; np++)
                x1 *= x - dsi->xd[imin];

            yt += x1;
        }

        if (i == 0 || (float)yt < *ymin)
        {
            *ymin = yt;
            *xmin = x;
        }
    }

    if (a != NULL) free(a);

    return 0;
}




d_s     *fit_poly_multi_local_op(O_p *op, int n_data_set, float wfit, int n, float threshold)
{
    int i;
    int  imin, bip;
    float xmin = 0, ymin = 0, min;
    d_s *dsi = NULL, *dss = NULL;


    if (op == NULL || n_data_set > op->n_dat) return NULL;

    dsi = op->dat[n_data_set];
    i = op->cur_dat;
    dss = create_and_attach_one_ds(op, 128, 128, 0);
    set_ds_treatement(dss, "minima by least square fit \\Sigma_0^na_nx^n");
    op->cur_dat = i;
    dss->nx = 0;
    set_ds_dot_line(dss);
    set_ds_point_symbol(dss, "\\oc");

    for (i = 0, min = dsi->yd[0], imin = 0, bip = (dsi->yd[0] > threshold) ? 0 : 1
            ; i < dsi->nx && i < dsi->ny; i++)
    {
        if ((bip == 1) && (dsi->yd[i] > threshold))
        {
            if (find_local_min(dsi, n, imin, wfit, &xmin, &ymin) == 0)
            {
                dss->xd[dss->nx] = xmin;
                dss->yd[dss->nx++] = ymin;

                if (dss->nx >= dss->mx)
                    dss = build_adjust_data_set(dss, dss->mx + 128, dss->mx + 128);
            }

            bip = 0;
        }
        else if (bip == 1)
        {
            if (dsi->yd[i] < min)
            {
                min = dsi->yd[i];
                imin = i;
            }
        }
        else if ((bip == 0) && (dsi->yd[i] < threshold))
        {
            min = dsi->yd[i];
            imin = i;

        }

        bip = (dsi->yd[i] > threshold) ? 0 : 1;
    }

    if (bip == 1)
    {
        if (find_local_min(dsi, n, imin, wfit, &xmin, &ymin) == 0)
        {
            dss->xd[dss->nx] = xmin;
            dss->yd[dss->nx++] = ymin;
        }
    }

    return dss;
}

int     fit_n_poly_local(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL;
    static float wfit = 10, thres = 0;
    static int n = 5;

    if (updating_menu_state != 0)  return D_O_K;

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)
    {
        win_printf("cannot find data");
        return D_O_K;
    }

    i = win_scanf("Fit y = \\Sigma_{i=0}^{i<=n} a_i x^i \n"
                  "to part of data below a threshold y_t\n"
                  "degree of polynome n %dWidth of fit %fy_t %f", &n, &wfit, &thres);

    if (i == WIN_CANCEL)  return OFF;

    fit_poly_multi_local_op(op, op->cur_dat, wfit, n + 1, thres);
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


/***********************************************************/
/*                                                         */
/*        name: sym_matrix_to_triang                       */
/*                                                         */
/*                                                         */
/* description: given a definite positive symmetric        */
/*              M of size n compute the triangular         */
/*              matrix L such that M = LtL                 */
/*                                                         */
/*       input: n, M stored in float *m                    */
/*      output: L stored in float *l                       */
/*      return: OK if no problem, err otherwise            */
/*                                                         */
/*        note: data are stored in a 1d array in the order */
/*              m00, m01, m02,..., m0n, m11, m12, ..., mnn */
/*                                                         */
/*              arrays are assumed to have been allocated  */
/*                                                         */
/*       date: 26 april 1995                               */
/*                                                         */
/***********************************************************/
int sym_matrix_to_triang(int n, double *m, double *l)
{
    int i, j, k;
    double tmp;
    double *pi = NULL, *pj = NULL, *pl = NULL;

    pl = l;

    for (i = 0 ; i < n ; i++)
    {
        j = i;
        tmp = *m++;

        for (pi = pl + i, pj = pl + j, k = n ; pi < l - j + i ; --k,
                pi += k, pj += k)
            tmp -= *pi **pj;

        if (tmp <= 0) return ERR_NEGATIVE_SQRT;

        *l++ = tmp = sqrt(tmp);
        tmp = 1.0 / tmp;

        for (j++ ; j < n ; j++)
        {
            *l = *m++;

            for (pi = pl + i, pj = pl + j, k = n ; pi < l - j + i ; --k, pi += k, pj += k)
                * l -= *pi **pj;

            *l++ *= tmp;
        }
    }

    return D_O_K;
}

/***********************************************************/
/*                                                         */
/*        name: sym_matrix_solve                           */
/*                                                         */
/*                                                         */
/* description: given a definite positive triangular       */
/*              matrix L of size n, compute X such         */
/*              that LtLX = Y
 */
/*                                                         */
/*       input: n, L, Y stored in double *l, *y             */
/*      output: X stored in double *x                       */
/*      return: OK if no problem, err otherwise            */
/*                                                         */
/*        note: data are stored in a 1d array in the order */
/*              m00, m01, m02,..., m0n, m11, m12, ..., mnn */
/*                                                         */
/*              arrays are assumed to have been allocated  */
/*                                                         */
/*       date: 26 april 1995                               */
/*                                                         */
/***********************************************************/
int sym_matrix_solve(int n, double *l, double *y, double *x)
{
    int i, j;
    double *x0, *x1;
    double tmp, tmp1;

    x0 = x;

    for (i = 0 ; i < n ; i++) *x++ = *y++;

    for (i = 0 ; i < n ; i++)
    {
        x = x0++;

        if (*l == 0)  return NULL_DENOMINATOR;

        *x /= *l++;

        for (tmp = *x++, j = i + 1 ; j < n ; j++) *x++ -= *l++ * tmp;
    }

    for (x1 = --x, i = n; i > 0 ; i--)
    {
        for (x0 = x1, j = i ; j < n ; j++) *x -= *--l **x0--;

        tmp1 = *--l;

        if (tmp1 == 0)    return NULL_DENOMINATOR;

        *x-- /= tmp1;
    }

    return D_O_K;
}

/***********************************************************/
/*                                                         */
/*        name: sym_matrix_improv                          */
/*                                                         */
/*                                                         */
/* description: given a definite positive triangular       */
/*              matrix L of size n, and a solution X such  */
/*              that LtLX = Y, improve that solution.      */
/*              cf Num. Recipes 2.7                        */
/*                                                         */
/*       input: n, L, Y, X stored in double *l, *y, *x      */
/*      output: better X stored in double *x                */
/*      return: OK if no problem, err otherwise            */
/*                                                         */
/*        note: data are stored in a 1d array in the order */
/*              m00, m01, m02,..., m0n, m11, m12, ..., mnn */
/*                                                         */
/*              arrays are assumed to have been allocated  */
/*                                                         */
/*       date: 3 may 1995                                  */
/*                                                         */
/***********************************************************/
int sym_matrix_improv(int n, double *l, double *y, double *x)
{
    int i, j;
    double *r = NULL, *r0 = NULL, *l0 = NULL, *x0 = NULL, *x1 = NULL, *dif = NULL, *dif0 = NULL;
    double *xx = NULL, *xx0 = NULL, *xx1 = NULL, tmp;

    r0 = r = (double *) malloc(n * sizeof(double));
    dif0 = dif = (double *) malloc(n * sizeof(double));
    xx0 = xx = (double *) malloc(n * sizeof(double));

    if (!r || !xx || !dif) return 0;

    l0 = l;
    x0 = x1 = x;

    for (i = 0 ; i < n ; i++, xx++)   /* calculate M*X */
        for (*xx = *x1++ **l++, x = x0 + i + 1, j = i + 1; j < n ; j++)
            *xx += *l++ **x++;

    for (xx--; i > 0 ; i--)
    {
        tmp = *xx;

        for (xx1 = xx0 + n - 1, j = n ; j > i ; j--) *xx1-- += tmp **--l;

        *xx-- *= *--l;
    }

    for (i = 0 ; i < n ; i++) *r++ = *++xx - *y++; /* calculate diff */

    sym_matrix_solve(n, l0, r0, dif); /* solve for the diff */

    for (x = x0, i = 0 ; i < n ; i++) *x++ -= *dif++;

    if (r0)   free(r0);

    if (dif0) free(dif0);

    if (xx0)  free(xx0);

    return D_O_K;
}

MENU    *matrice_plot_menu(void)
{
  static MENU mn[32] = {0};

    if (mn[0].text != NULL)   return mn;

    add_item_to_menu(mn, "Fit to a polynome", fit_poly, NULL, 0, NULL);
    add_item_to_menu(mn, "Fit to a polynome with error", fit_poly_er, NULL, 0, NULL);
    add_item_to_menu(mn, "Substract a polynome", rm_poly, NULL, 0, NULL);
    add_item_to_menu(mn, "Fit locally a polynome", fit_poly_local, NULL, 0, NULL);
    add_item_to_menu(mn, "Fit local minima", fit_n_poly_local, NULL, 0, NULL);

    return mn;
}

int     matrice_main(void)
{
    MENU *mn = NULL;
    mn = matrice_plot_menu();
    add_plot_treat_menu_item("Polynome fit", NULL, mn, 0, NULL);
    return D_O_K;
}
