/********************************************************************/
/* plot_treat_math.c                                                */
/* mathematical data treatments on dataset(s)                       */
/********************************************************************/

# include "float.h"

#include "platform.h"
#include "xvin.h"
#include "xv_tools_lib.h"
#include "p_treat_math.h"
#include "p_treat_basic.h"

# include <stdlib.h>
# include <string.h>
# include <math.h>
# include <assert.h>

d_s	*find_cur_data_set_in_op(O_p *ops)
{
    d_s *ds = NULL;

    if (ops == NULL || ops->n_dat <= 0 || ops->cur_dat >= ops->n_dat)
    {
        return NULL;
    }

    ds = ops->dat[ops->cur_dat];
    return (ds);
}

int mean_y2_on_array(float *yd, int nx, int win_flag, float *meany, float *my2, float *my4)
{
    int i;
    double tmp, dx, y2, mean, y4;

    if (win_flag)
    {
        dx = 2 * M_PI / nx;

        for (i = 0 , mean = 0; i < nx ; i++)
        {
            mean += (1 - cos(i * dx)) * yd[i];
        }
    }
    else
    {
        for (i = 0 , mean = 0; i < nx ; i++)
        {
            mean += yd[i];
        }
    }

    mean /= nx;

    if (win_flag)
    {
        dx = 2 * M_PI / nx;

        for (i = 0 , y4 = y2 = 0; i < nx ; i++)
        {
            tmp = yd[i] - mean;
            y2 += (1 - cos(i * dx)) * tmp * tmp;
            y4 += (1 - cos(i * dx)) * tmp * tmp * tmp * tmp;
        }
    }
    else
    {
        for (i = 0 , y4 = y2 = 0; i < nx ; i++)
        {
            tmp = yd[i] - mean;
            y2 += tmp * tmp;
            y4 +=  tmp * tmp * tmp * tmp;
        }
    }

    y2 /= nx;
    y4 /= nx;

    if (meany != NULL)
    {
        *meany = mean;
    }

    if (my2 != NULL)
    {
        *my2 = y2;
    }

    if (my4 != NULL)
    {
        *my4 = y4;
    }

    return 0;
}


int do_dump_sigma_HF_of_all_ds(void)
{
  int i;
  pltreg *pr = NULL;
  O_p *op = NULL, *opd = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  double tmp;

  if (updating_menu_state != 0)
  {
      return D_O_K;
  }

  if (key[KEY_LSHIFT])
  {
      return win_printf_OK("This routine create a new plot containing\n"
                           "the sigma HF of all datasets");
  }

  i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);
  if (i != 3)
  {
      return win_printf_OK("cannot find data");
  }

  opd = create_and_attach_one_plot(pr,16,16,0);

  dsd = opd->dat[0];
  set_plot_symb(dsd, "\\pt5\\di");
  set_ds_source(dsd, "sigma HF of respective dataset");
  set_plot_title(opd, "Sigma HF of %s", (op->title != NULL) ? op->title : "");
  set_plot_y_title(opd, "Sigma HF");
  set_plot_x_title(opd, "dataset index");
  dsd->nx = dsd->ny = 0;
  for (i = 0;i<op->n_dat;i++)
  {
    tmp = -1;
    get_sigma_of_derivative_of_partial_ds(op->dat[i], 0, op->dat[i]->nx, DBL_MAX,&tmp);
    add_new_point_to_ds(dsd,i,(float)tmp);
  }
  opd->need_to_refresh = 1;

  return refresh_plot(pr, pr->n_op - 1 );

}

int do_dump_std_of_all_ds(void)
{
  int i;
  pltreg *pr = NULL;
  O_p *op = NULL, *opd = NULL;
  d_s *dsi = NULL, *dsd = NULL;
  double tmp;
  float meansq = 0;
  float mean = 0;
  d_s *dsc = NULL;

  if (updating_menu_state != 0)
  {
      return D_O_K;
  }

  if (key[KEY_LSHIFT])
  {
      return win_printf_OK("This routine create a new plot containing\n"
                           "the sigma HF of all datasets");
  }

  i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);
  if (i != 3)
  {
      return win_printf_OK("cannot find data");
  }

  opd = create_and_attach_one_plot(pr,16,16,0);

  dsd = opd->dat[0];
  set_plot_symb(dsd, "\\pt5\\di");
  set_ds_source(dsd, "std of respective dataset");
  set_plot_title(opd, "Std of %s", (op->title != NULL) ? op->title : "");
  set_plot_y_title(opd, "Std");
  set_plot_x_title(opd, "dataset index");
  uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opd, IS_Y_UNIT_SET);
  dsd->nx = dsd->ny = 0;
  meansq = 0;
  mean = 0;
  for (i = 0;i<op->n_dat;i++)
  {
    dsc = op->dat[i];
    meansq = 0;
    mean = 0;
    for (int j = 0;j<dsc->nx;j++)
    {
      meansq += dsc->yd[j]*dsc->yd[j];
      mean += dsc->yd[j];
    }
    meansq /= dsc->nx;
    mean /= dsc->nx;
    add_new_point_to_ds(dsd,i,sqrt(meansq-mean*mean));
  }
  opd->need_to_refresh = 1;

  return refresh_plot(pr, pr->n_op - 1 );

}


/*	this routine compute the mean value, the second and four momentum
 *	of the y component of the point of a data set in the view area
 *	a cosine windowing is possible
 *
 */

int mean_y2_on_op(O_p *op, int win_flag, float *meany, float *my2, float *my4, float *kxu, int *nxeff)
{
    int i;
    d_s *dsi = NULL;
    int  nx, start, end;
    double tmp, dx, y2, mean, y4, y2u, y4u, kx;
    un_s *un = NULL;
    dsi = find_cur_data_set_in_op(op);

    if (dsi == NULL)
    {
        win_printf("bad data set");
        return 1;
    }

    nx = dsi->nx;
    un = op->yu[op->c_yu];

    if (un->type != IS_METER || un->decade != IS_MICRO)
        win_printf("warning the y unit is not \\mu m !\n"
                   "numerical value will be wrong");

    un = op->xu[op->c_xu];

    if (un->type != IS_SECOND || un->decade != 0)
        win_printf("warning the x unit is not seconds !\n"
                   "numerical value will be wrong");

    start = 0;
    end = nx;

    if (win_flag)
    {
        dx = 2 * M_PI / nx;

        for (i = start , mean = 0; i < end ; i++)
        {
            mean += (1 - cos(i * dx)) * dsi->yd[i];
        }
    }
    else
    {
        for (i = start , mean = 0; i < end ; i++)
        {
            mean += dsi->yd[i];
        }
    }

    mean /= nx;

    if (win_flag)
    {
        dx = 2 * M_PI / nx;

        for (i = start , y4 = y2 = 0; i < end ; i++)
        {
            tmp = dsi->yd[i] - mean;
            y2 += (1 - cos(i * dx)) * tmp * tmp;
            y4 += (1 - cos(i * dx)) * tmp * tmp * tmp * tmp;
        }
    }
    else
    {
        for (i = start , y4 = y2 = 0; i < end ; i++)
        {
            tmp = dsi->yd[i] - mean;
            y2 += tmp * tmp;
            y4 +=  tmp * tmp * tmp * tmp;
        }
    }

    y2 /= nx;
    y4 /= nx;
    y2u = y2 * op->dy * op->dy;
    kx = (1.38e-11 * 298) / y2u;
    y4u = y4 * op->dy * op->dy * op->dy * op->dy;

    if (meany != NULL)
    {
        *meany = mean * op->dy;
    }

    if (my2 != NULL)
    {
        *my2 = y2u;
    }

    if (my4 != NULL)
    {
        *my4 = y4u;
    }

    if (kxu != NULL)
    {
        *kxu = kx;
    }

    if (nxeff != NULL)
    {
        *nxeff = nx;
    }

    return 0;
}



/********************************************************************/

/*		find anisotropy of a brownian motion, return a new ds with
 *		x axis corresponding to the maximum of variance
 *		anisitropy in %, angle in degrees
 */
 void find_rotation_angle_ds(d_s *ds1, float *angle,int win_flag)
 {
     register int i;
     float the, yx = 1, co, si;
     int  nx;
     d_s *ds2=NULL;

     float xm = 0, ym = 0, x2 = 0, y2 = 0, x21, y21;

     if (ds1 == NULL)
     {
        win_printf("No good ds on which to compute angle\n");
         return;
     }

     if (ds1->nx != ds1->ny)
     {
       win_printf("Partial dataset don't have the same length\n");
     }
     nx = ds1->nx;

     ds2 = build_data_set(nx, nx);

     if (ds2 == NULL)
     {
         win_printf("can't create ds");
         return;
     }

     ds2->nx = ds2->ny = nx;

     mean_y2_on_array(ds1->xd, ds1->nx, win_flag, &xm, &x2, NULL);
     mean_y2_on_array(ds1->yd, ds1->ny, win_flag, &ym, &y2, NULL);
     the = M_PI / 4;
     co = cos(the);
     si = sin(the);

     for (i = 0 ; i < nx ; i++)
     {
         ds2->xd[i] = co * (ds1->xd[i] - xm) + si * yx * (ds1->yd[i] - ym);
         ds2->yd[i] =  -si * (ds1->xd[i] - xm) + co * yx * (ds1->yd[i] - ym);
     }

     mean_y2_on_array(ds2->xd, ds2->nx, win_flag, NULL, &x21, NULL);
     mean_y2_on_array(ds2->yd, ds2->ny, win_flag, NULL, &y21, NULL);
     co = (x2 - y2) / 2;
     si = (x21 - y21) / 2;
     x21 = (x2 + y2) / 2;

     if (x21 != 0)
     {
         y21 = sqrt(co * co + si * si) / x21;
     }

     the = (float) atan2(si, co) / 2;
     *angle =the;
     co = cos(the);
     si = sin(the);

     free_data_set(ds2);


     return;
 }


d_s *find_locale_rotate_ds(O_p *ops, int nds, float *anisotropy, float *angle, int win_flag)
{
    register int i;
    float the, yx = 1, co, si;
    d_s *dsi = NULL, *dss = NULL;
    int  nx;
    float xm = 0, ym = 0, x2 = 0, y2 = 0, x21, y21;

    if (ops == NULL)
    {
        return NULL;
    }

    dsi = ops->dat[(nds < ops->n_dat) ? nds : ops->cur_dat];
    nx = dsi->nx;

    if (ops->dx == 0)
    {
        win_printf("unit factor null !\ncannot treate");
        return NULL;
    }

    if (ops->dx != ops->dy)
    {
        win_printf("unit factor differs betwwen x and y!\nconverting both so that they correspond to the x one");
        yx = ops->dy / ops->dx;
    }

    dss = build_data_set(nx, nx);

    if (dss == NULL)
    {
        win_printf("can't create ds");
        return NULL;
    }

    dss->nx = dss->ny = nx;
    mean_y2_on_array(dsi->xd, dsi->nx, win_flag, &xm, &x2, NULL);
    mean_y2_on_array(dsi->yd, dsi->ny, win_flag, &ym, &y2, NULL);
    the = M_PI / 4;
    co = cos(the);
    si = sin(the);

    for (i = 0 ; i < nx ; i++)
    {
        dss->xd[i] = co * (dsi->xd[i] - xm) + si * yx * (dsi->yd[i] - ym);
        dss->yd[i] =  -si * (dsi->xd[i] - xm) + co * yx * (dsi->yd[i] - ym);
    }

    mean_y2_on_array(dss->xd, dss->nx, win_flag, NULL, &x21, NULL);
    mean_y2_on_array(dss->yd, dss->ny, win_flag, NULL, &y21, NULL);
    co = (x2 - y2) / 2;
    si = (x21 - y21) / 2;
    x21 = (x2 + y2) / 2;

    if (x21 != 0)
    {
        y21 = sqrt(co * co + si * si) / x21;
    }

    the = atan2(si, co) / 2;
    *anisotropy = 100 * y21;
    *angle = 180 * the / M_PI;
    co = cos(the);
    si = sin(the);

    for (i = 0 ; i < nx ; i++)
    {
        dss->xd[i] = xm + co * (dsi->xd[i] - xm) + si * yx * (dsi->yd[i] - ym);
        dss->yd[i] = ym - si * (dsi->xd[i] - xm) + co * yx * (dsi->yd[i] - ym);
    }

    inherit_from_ds_to_ds(dss, dsi);
    return (dss);
}
d_s *true_y_avg_derivative_of_ds_in_op(O_p *op, int ids, int idx)
{
  int j, k, nf;
  d_s *dsi = NULL, *dsd = NULL;
  float tmp;

  if (op == NULL || ids < 0 || ids >= op->n_dat) return NULL;
  dsi = op->dat[ids];
  nf = dsi->nx;
  nf -= 2*idx;
  if (nf <= 0) return NULL;
     dsd =build_data_set(nf, nf);
  if (dsd == NULL) return win_printf_ptr("cannot create ds");
  for (j = 0; j < nf; j++)
    {
      for(k = 1, tmp = 0; k <= idx; k++)
	{
	  dsd->yd[j] += dsi->yd[idx+j+k] - dsi->yd[idx+j-k];
	  tmp += dsi->xd[idx+j+k] - dsi->xd[idx+j-k];
	  dsd->xd[j] += dsi->xd[idx+j+k] + dsi->xd[idx+j-k];
	}
      dsd->xd[j] /= 2*idx;
      tmp *= op->dx;
      dsd->yd[j] = (tmp != 0) ? dsd->yd[j]/tmp : dsd->yd[j];
    }
  inherit_from_ds_to_ds(dsd, dsi);
  set_ds_treatement(dsd, "Averaged derivative over %d pts",idx);
  return dsd;
}


int do_true_y_avg_derivative(void)
{
    int i, j, k, nf;
    pltreg *pr = NULL;
    O_p *op = NULL, *opd = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    static int all = 0, jk = 2;
    float tmp;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine create a new plot containing\n"
                             "the two points average derivative of the selected data set");
    }

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);
    if (i != 3)
    {
        return win_printf_OK("cannot find data");
    }

    i = win_scanf("This routine create a new plot containing\n"
                  "the two points average derivative of the selected data set \\{x_n=x(t_n)\\} \n\n"
                  "it is defined and computed at \\Sigma_{j=1}^{j<=k}(t_{n+j}-t_{n+j}/k as:\n\n"
		  "define k = %4d\n"
                  "Do you want to apply to the selected data-sets %R or to all%r\n", &jk,&all);

    if (i == WIN_CANCEL) return D_O_K;
    if (jk < 1) return win_printf_OK("averaging must be greater than 1");
    for (i = 0; i < op->n_dat; i++)
      {
	if (all == 0 && i != op->cur_dat) continue;
	dsi = op->dat[i];
	nf = dsi->nx;
	nf -= 2*jk;
	if (nf <= 0) continue;
	if (opd == NULL)
	  {
	    opd = create_and_attach_one_plot(pr, nf, nf, 0);
	    if (opd == NULL) return win_printf_OK("cannot create plot");
	    dsd = opd->dat[0];
	  }
	if (dsd == NULL)
	  dsd = create_and_attach_one_ds(opd, nf, nf, 0);
	if (dsd == NULL) return win_printf_OK("cannot create ds");
	for (j = 0; j < nf; j++)
	  {
	    for(k = 1, tmp = 0; k <= jk; k++)
	      {
		dsd->yd[j] += dsi->yd[jk+j+k] - dsi->yd[jk+j-k];
		tmp += dsi->xd[jk+j+k] - dsi->xd[jk+j-k];
		dsd->xd[j] += dsi->xd[jk+j+k] + dsi->xd[jk+j-k];
	      }
	    dsd->xd[j] /= 2*jk;
	    //dsd->yd[j] /= jk*jk;
	    tmp *= op->dx;
	    dsd->yd[j] = (tmp != 0) ? dsd->yd[j]/tmp : dsd->yd[j];
	    //dsd->yd[j] *= op->dy;
	  }
	inherit_from_ds_to_ds(dsd, dsi);
	set_ds_treatement(dsd, "Averaged derivative over %d pts",jk);
	dsd = NULL;
      }
    set_plot_title(opd, "Averaged derivate");
    opd->filename = Transfer_filename(op->filename);
    uns_op_2_op(opd, op);

    for (i = 0; i < opd->n_yu; i++)  // derivative remove offset
    {
        opd->yu[i]->ax = 0;
    }
    opd->dir = Mystrdup(op->dir);

    return refresh_plot(pr, UNCHANGED);
}


int do_true_y_derivative(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    static int all = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine create a new plot containing\n"
                             "the simple two points derivative of the selected data set \\{x_n=x(t_n)\\} \n\n"
                             "it is defined and computed at the points \\frac{t_n+t_{n+1}}{2} as:\n\n"
                             "y(\\frac{t_n+t_{n+1}}{2}) = \\frac{x_{n+1}-x_n}{t_{n+1}-t_{n}}");
    }

    i = ac_grep(cur_ac_reg, "%pr%op", &pr, &op);

    if (i != 2)
    {
        return win_printf_OK("cannot find data");
    }

    i = win_scanf("This routine create a new plot containing\n"
                  "the two points derivative of the selected data set \\{x_n=x(t_n)\\} \n\n"
                  "it is defined and computed at the points \\frac{t_n+t_{n+1}}{2} as:\n\n"
                  "y(\\frac{t_n+t_{n+1}}{2}) = \\frac{x_{n+1}-x_n}{t_{n+1}-t_{n}}"
                  "Do you want to apply to the selected data-sets %R or to all%r\n", &all);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    true_y_derivative(op, all ? -1 : op->cur_dat, NULL);
    return refresh_plot(pr, UNCHANGED);
}

d_s *true_y_derivative_one_ds(const d_s *src_ds, float dx, float dy)
{
    d_s *dest_ds = NULL;
    int nx = 0;
    float tmp = .0;
    assert(src_ds);
    nx = src_ds->nx;
    dest_ds = build_data_set(nx - 1, nx - 1);
    inherit_from_ds_to_ds(dest_ds, src_ds);
    set_ds_treatement(dest_ds, "derivative");

    if (src_ds->source)
    {
        dest_ds->source = strdup(src_ds->source);
    }

    for (int i = 0; i < nx - 1; i++)
    {
        dest_ds->xd[i] = (src_ds->xd[i] + src_ds->xd[i + 1]) / 2;
        tmp = dx * (src_ds->xd[i + 1] - src_ds->xd[i]);
        dest_ds->yd[i] = (tmp != 0) ? (src_ds->yd[i + 1] - src_ds->yd[i]) / tmp : src_ds->yd[i + 1] -
                         src_ds->yd[i];
        dest_ds->yd[i] *= dy;

        if (tmp == 0)
        {
            warning_message("zero divisor in derivate at %d pts", i);
        }
    }

    return dest_ds;
}


int do_y_differentiate(void)
{
    int i, k;
    pltreg *pr = NULL;
    O_p *op = NULL, *opd = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    static int delta = 1, xofidelta = 0, all_ds = 0;
    int  nx;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine create a new plot containing\n"
                             "the simple two points derivative of the selected data set \\{x_n=x(t_n)\\} \n\n"
                             "it is defined and computed at the points \\frac{t_n+t_{n+1}}{2} as:\n\n"
                             "y(\\frac{t_n+t_{n+1}}{2}) = \\frac{x_{n+1}-x_n}{t_{n+1}-t_{n}}");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf_OK("cannot get plot data\n");
    }

    i = win_scanf("Differentiate Y by index = y(i+\\delta ) - y(i)\n"
                  "Specify \\delta  %8d\n"
                  "For X use: %R x(i) or %r (x(i+\\delta) + x(i))/2\n"
                  "Apply to the selected dataset %R or to %r all datasets\n", &delta, &xofidelta, &all_ds);

    if (i == WIN_CANCEL)
    {
        return OFF;
    }

    if (all_ds)
    {
        for (k = 0; k < op->n_dat; k++)
        {
            if (delta < 0 || delta >= op->dat[k]->nx)
            {
                return win_printf_OK("\\delta %d is out of range for ds %d", delta,k);
            }
        }
    }
    else
    {
        nx = dsi->nx;

        if (delta < 0 || delta >= nx)
        {
            return win_printf_OK("\\delta %d is out of range", delta);
        }
    }

    for (k = 0;  k < op->n_dat; k++)
    {
        dsi = (all_ds) ? op->dat[k] : dsi;
        nx = dsi->nx;

        if (opd == NULL)
        {
            opd = create_and_attach_one_plot(pr, nx - delta, nx - delta, 0);
            dsd = opd->dat[0];
        }
        else
        {
            dsd = create_and_attach_one_ds(opd, nx - delta, nx - delta, 0);
        }

        if (opd == NULL)
        {
            return win_printf_OK("Cannot allocate plot");
        }

        if (dsd == NULL)
        {
            return D_O_K;
        }

        inherit_from_ds_to_ds(dsd, dsi);
        set_ds_treatement(dsd, "Differentiate over %d", delta);

        for (i = 0; i < nx - delta; i++)
        {
            dsd->xd[i] = (xofidelta == 0) ? dsi->xd[i] : (dsi->xd[i + delta] + dsi->xd[i]) / 2;
            dsd->yd[i] = dsi->yd[i + delta] - dsi->yd[i];
        }

        if (all_ds == 0)
        {
            break;
        }
    }

    set_plot_title(opd, "Differentiate over %d", delta);
    opd->filename = Transfer_filename(op->filename);
    uns_op_2_op(opd, op);
    cancel_offset_in_op_unit_set(opd, IS_Y_UNIT_SET); // derivative remove offset
    //  for (i = 0; i < opd->n_yu; i++)      opd->yu[i]->ax = 0;
    opd->dir = Mystrdup(op->dir);
    return refresh_plot(pr, UNCHANGED);
}

O_p *true_y_derivative(const O_p *src_op, int ds_idx, O_p *dest_op)
{
    pltreg *pr = NULL;
    const d_s *cur_src_ds = NULL;
    d_s *cur_dest_ds = NULL;
    //int  nx = 0;
    assert(src_op);
    ac_grep(cur_ac_reg, "%pr", &pr);

    if (dest_op == NULL)
    {
        dest_op = create_and_attach_one_plot(pr, src_op->dat[0]->nx - 1, src_op->dat[0]->ny - 1, 0);
    }

    for (int j = 0; j < src_op->n_dat; j++)
    {
        if (ds_idx >= 0 && j != ds_idx)
        {
            continue;
        }

        cur_src_ds = src_op->dat[j];
        //nx = cur_src_ds->nx;
        cur_dest_ds = true_y_derivative_one_ds(cur_src_ds, src_op->dx, src_op->dy);

        if (cur_dest_ds == NULL)
        {
            return D_O_K;
        }

        add_one_plot_data(dest_op, IS_DATA_SET, (void *)cur_dest_ds);
    }

    remove_ds_from_op(dest_op, dest_op->dat[0]);
    set_plot_title(dest_op, "true derivate");
    dest_op->filename = Transfer_filename(src_op->filename);
    uns_op_2_op(dest_op, src_op);

    for (int i = 0; i < dest_op->n_yu; i++)  // derivative remove offset
    {
        dest_op->yu[i]->ax = 0;
    }

    dest_op->dir = Mystrdup(src_op->dir);
    return dest_op;
}


int do_integrate_ds(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    int  nx;
    float tmp;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine integrates a dataset along the x-axis\n"
                             "this is the simple cumulative sum of y-values\n"
                             "a new dataset is created");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf_OK("Could not find plot data");
    }

    nx = dsi->nx;
    dsd = build_data_set(nx, nx);

    if (dsd == NULL)
    {
        return (win_printf_OK("can't create data set"));
    }

    for (i = 0, tmp = 0; i < nx ; i++)
    {
        dsd->xd[i] = dsi->xd[i];
        tmp += dsi->yd[i];
        dsd->yd[i] = tmp;
    }

    dsd->ny = nx;
    dsd->nx = nx;
    add_one_plot_data(op, IS_DATA_SET, (void *)dsd);
    inherit_from_ds_to_ds(dsd, dsi);
    set_ds_treatement(dsd, "Integrated ds");
    return refresh_plot(pr, UNCHANGED);
}





int do_true_integrate_ds(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    int  nx;
    static int trapeze = 0, backward = 0;
    //float tmp, dx;
    double tmpd, dxdo = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine integrates a dataset along the x-axis\n"
                             "normalisation by x-steps is performed\n"
                             "a new dataset is created");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf_OK("Could not find plot data");
    }

    i = win_scanf("Dicrete data integretion\n"
                  "Which integration method do you want\n"
                  "Rectangle %R or trapeze %r\n"
                  "Note that if you have derivate data and want\n"
                  "to recover them by integration (to a constant)\n"
                  "you should use the rectangle method\n"
                  "You can integrate in the forward %R direction\nor backward%r\n"
                  , &trapeze, &backward);
    nx = dsi->nx;
    dsd = build_data_set(nx + 1, nx + 1);

    if (dsd == NULL)
    {
        return (win_printf_OK("can't create data set"));
    }

    if (trapeze == 0 && backward == 0)
    {
        dsd->xd[0] = dsi->xd[0] - (dsi->xd[1] - dsi->xd[0]) / 2;
        dsd->yd[0] = 0;
        tmpd = 0;

        for (i = 1 , tmpd = 0; i < nx ; i++)
        {
            dsd->xd[i] = (dsi->xd[i] + dsi->xd[i - 1]) / 2;
            dxdo  =  dsi->xd[i] - dsi->xd[i - 1];
            //if (dx!=0)
            tmpd += (dsi->yd[i - 1] * dxdo);
            //else        tmp += (dsi->yd[i]+dsi->yd[i-1])/2;
            dsd->yd[i] = (float)tmpd;
        }

        dsd->xd[i] = dsi->xd[i - 1] + dxdo / 2;
        //if (dx!=0)
        tmpd += (dsi->yd[i - 1] * dxdo);
        //else        tmp += (dsi->yd[i]+dsi->yd[i-1])/2;
        dsd->yd[i] = (float)tmpd;
        dsd->ny = nx + 1;
        dsd->nx = nx + 1;
        add_one_plot_data(op, IS_DATA_SET, (void *)dsd);
        inherit_from_ds_to_ds(dsd, dsi);
        set_ds_treatement(dsd, "True Integrated rectangle forward ds");
    }
    else if (trapeze == 0 && backward == 1)
    {
        dsd->xd[0] = dsi->xd[nx - 1] - (dsi->xd[nx - 2] - dsi->xd[nx - 1]) / 2;
        dsd->yd[0] = 0;
        tmpd = 0;

        for (i = 1 , tmpd = 0; i < nx ; i++)
        {
            dsd->xd[i] = (dsi->xd[nx - i - 1] + dsi->xd[nx - i]) / 2;
            dxdo  =  dsi->xd[nx - i - 1] - dsi->xd[nx - i];
            //if (dx!=0)
            tmpd += (dsi->yd[nx - i] * dxdo);
            //else        tmp += (dsi->yd[i]+dsi->yd[i-1])/2;
            dsd->yd[i] = (float)tmpd;
        }

        dsd->xd[i] = dsi->xd[nx - i] + dxdo / 2;
        //if (dx!=0)
        tmpd += (dsi->yd[nx - i] * dxdo);
        //else        tmp += (dsi->yd[i]+dsi->yd[i-1])/2;
        dsd->yd[i] = (float)tmpd;
        dsd->ny = nx + 1;
        dsd->nx = nx + 1;
        add_one_plot_data(op, IS_DATA_SET, (void *)dsd);
        inherit_from_ds_to_ds(dsd, dsi);
        set_ds_treatement(dsd, "True Integrated rectangle backward ds");
    }

    if (trapeze == 1 && backward == 0)
    {
        dsd->xd[0] = dsi->xd[0] - (dsi->xd[1] - dsi->xd[0]) / 2;
        dsd->yd[0] = 0;
        tmpd = 0;
        // fist int is rectangle
        dsd->xd[1] = (dsi->xd[1] + dsi->xd[0]) / 2;
        dxdo  =  dsi->xd[1] - dsi->xd[0];
        tmpd += (double)(dsi->yd[0] * dxdo);
        dsd->yd[1] = (float)tmpd;

        for (i = 2 , tmpd = 0; i < nx ; i++)
        {
            tmpd += dxdo * ((((double)dsi->yd[i - 1] + (double)dsi->yd[i - 2]) / 2) + (double)dsi->yd[i - 1]) / 4;
            dsd->xd[i] = (dsi->xd[i] + dsi->xd[i - 1]) / 2;
            dxdo  = (double)dsi->xd[i] - (double)dsi->xd[i - 1];
            tmpd += dxdo * ((double)dsi->yd[i - 1] + (((double)dsi->yd[i - 1] + (double)dsi->yd[i]) / 2)) / 4;
            dsd->yd[i] = (float)tmpd;
        }

        dsd->xd[i] = dsi->xd[i - 1] + dxdo / 2;
        tmpd += ((double)dsi->yd[i - 1] * dxdo);
        dsd->yd[i] = (float)tmpd;
        dsd->ny = nx + 1;
        dsd->nx = nx + 1;
        add_one_plot_data(op, IS_DATA_SET, (void *)dsd);
        inherit_from_ds_to_ds(dsd, dsi);
        set_ds_treatement(dsd, "True Integrated trapeze forward ds");
    }
    else if (trapeze == 1 && backward == 1)
    {
        dsd->xd[0] = dsi->xd[nx - 1] - (dsi->xd[nx - 2] - dsi->xd[nx - 1]) / 2;
        dsd->yd[0] = 0;
        tmpd = 0;
        // fist int is rectangle
        dsd->xd[1] = (dsi->xd[nx - 2] + dsi->xd[nx - 1]) / 2;
        dxdo  =  dsi->xd[nx - 2] - dsi->xd[nx - 1];
        tmpd += (dsi->yd[nx - 1] * dxdo);
        dsd->yd[1] = (float)tmpd;

        for (i = 2 , tmpd = 0; i < nx ; i++)
        {
            tmpd += dxdo * ((((double)dsi->yd[nx - i] + (double)dsi->yd[nx - i + 1]) / 2) + (double)dsi->yd[nx - i]) / 4;
            dsd->xd[i] = (dsi->xd[nx - i - 1] + dsi->xd[nx - i]) / 2;
            dxdo  = (double)dsi->xd[nx - i - 1] - (double)dsi->xd[nx - i];
            tmpd += dxdo * ((double)dsi->yd[nx - i] + (((double)dsi->yd[nx - i] + (double)dsi->yd[nx - i - 1]) / 2)) / 4;
            dsd->yd[i] = (float)tmpd;
        }

        dsd->xd[i] = dsi->xd[nx - i] + dxdo / 2;
        tmpd += (dsi->yd[nx - i] * dxdo);
        dsd->yd[i] = (float)tmpd;
        dsd->ny = nx + 1;
        dsd->nx = nx + 1;
        add_one_plot_data(op, IS_DATA_SET, (void *)dsd);
        inherit_from_ds_to_ds(dsd, dsi);
        set_ds_treatement(dsd, "True Integrated trapeze backward ds");
    }

    return refresh_plot(pr, UNCHANGED);
}


int distance_between_datasets(void)
{
  int i,j;
  pltreg *pr = NULL;
  O_p *op = NULL, *opr = NULL, *opsorted = NULL;
  d_s *dsi = NULL, *dsr = NULL, *dscur = NULL;
  //float tmp, dx;

  if (updating_menu_state != 0)
  {
      return D_O_K;
  }

  if (key[KEY_LSHIFT])
  {
      return win_printf_OK("This routine compute X distance between  sucessive datasets\n");
  }

  if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
  {
      return win_printf_OK("Could not find plot data");
  }
  opr = create_and_attach_one_plot(pr,op->n_dat,op->n_dat,0);
  dsr = opr->dat[0];
  dsr->nx = dsr->ny = 0;
  float dsmin = 0;
  float dsmax = 0;
  float last_max = 0;
  opsorted = create_and_attach_one_plot(pr,16,16,0);
  float min = 0;
  int minds = 0;
  while (op->n_dat > 0)
  {
    min = FLT_MAX;
    for (i = 0;i<op->n_dat;i++)
    {
      dscur = op->dat[i];
      if (dscur->xd[0] < min)
      {
        min = dscur->xd[0];
        minds = i;
      }
    }

   add_one_plot_data(opsorted, IS_DATA_SET, (void *) op->dat[minds]);
   for (i=minds;i<op->n_dat-1;i++)
   {
     op->dat[i] = op->dat[i+1];
   }
   op->n_dat -= 1;
 }
 uns_op_2_op_by_type(op, IS_X_UNIT_SET, opr, IS_Y_UNIT_SET);
 uns_op_2_op_by_type(op, IS_X_UNIT_SET, opr, IS_X_UNIT_SET);
 remove_data(pr, IS_ONE_PLOT, (void *)op);
 remove_ds_from_op(opsorted,opsorted->dat[0]);


 op = opsorted;

  for (i = 0;i<op->n_dat;i++)
  {
    dscur = op->dat[i];
    dsmin = FLT_MAX;
    dsmax = FLT_MIN;
    for (j=0;j<dscur->nx;j++)
    {
      if (dscur->xd[j] < dsmin) dsmin = dscur->xd[j] ;
      if (dscur->xd[j] > dsmax) dsmax = dscur->xd[j] ;
    }
    if (i>0) add_new_point_to_ds(dsr,(dsmin+last_max)/2,dsmin-last_max);
    last_max = dsmax;
  }


  set_plot_symb(dsr, "\\pt5\\di");
  set_ds_source(dsr, "X distance between successive ds");
  set_plot_title(opr, "X distance of %s", (op->title != NULL) ? op->title : "");
  set_plot_y_title(opr, "X distance of all ds");
  set_plot_x_title(opr, "dataset mean X");

return 0;

}

int get_sigma_of_derivative_of_partial_ds(d_s *dsi, int start, int end, double uperlimit, double *sigHF)
{
    int i;
    double tmp, sigma2, sigest2;
    int nf, ng;

    if (dsi == NULL)
    {
        return -1;
    }

    nf = dsi->nx; /* this is the number of points in the data set */

    if (nf < 2)
    {
        return -1;
    }

    start = (start < 0) ? 0 : start;
    start = (start < nf) ? start : nf;
    end = (end < 0) ? 0 : end;
    end = (end < nf) ? end : nf;

    if (start > end)
    {
        return -1;
    }

    sigest2 = 4 * uperlimit * uperlimit;

    for (i = start, ng = 0, sigma2 = 0; i < end - 1; i++)
    {
        tmp = dsi->yd[i + 1] - dsi->yd[i];
        tmp *= tmp;

        if (tmp < sigest2)
        {
            sigma2 += tmp;
            ng++;
        }
    }

    if (ng < 1)
    {
        return -1;
    }

    sigma2 /= ng;
    sigest2 = 4 * sigma2;

    for (i = start, ng = 0, sigma2 = 0; i < end - 1; i++)
    {
        tmp = dsi->yd[i + 1] - dsi->yd[i];
        tmp *= tmp;

        if (tmp < sigest2)
        {
            sigma2 += tmp;
            ng++;
        }
    }

    if (ng < 1)
    {
        return -1;
    }

    sigma2 /= ng;

    if (sigHF)
    {
        *sigHF = sqrt(sigma2);
    }

    return 0;
}
int compute_barycenter_of_visible_points(void)
{
  int i, k;
  double yw = 0, x_yw = 0;
  float min = 0, max = 0;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi = NULL;


   if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine computes the mean Y-value of a dataset\n");
    }

     if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf_OK("cannot find data!");
    }


     for (i = 0, k = 0, yw = x_yw = 0; i < dsi->nx; i++)
       {
	 if (dsi->xd[i] < op->x_hi && dsi->xd[i] >= op->x_lo &&
	     dsi->yd[i] < op->y_hi && dsi->yd[i] >= op->y_lo)
	   {
	     if (k == 0) min = max = dsi->xd[i];
	     min = (dsi->xd[i] < min) ? dsi->xd[i] : min;
	     max = (dsi->xd[i] > max) ? dsi->xd[i] : max;
	     yw += dsi->yd[i];
	     x_yw += dsi->yd[i] * dsi->xd[i];
	     k++;
	   }
       }

     if (k == 0 || yw == 0.0)
       return win_printf_OK("no data in view!");
     return win_printf_OK("For dataset %d with %d points in view in [%g, %g[ raw\n"
			  "Or [%g, %g[ in %s"
			  "The barycenter in X is located at %g (raw)\n"
			  "Or %g %s\n"
			  ,op->cur_dat,k,min,max,op->ax+min*op->dx,op->ax+max*op->dx
			  ,(op->x_unit != NULL) ? op->x_unit : "raw"
			  ,x_yw/yw,op->ax+op->dx*x_yw/yw
			  ,(op->x_unit != NULL) ? op->x_unit : "raw");

}


/********************************************************************/
/* averages a dataset                           */
/********************************************************************/
int mean_ds(void)
{
    int i;
    char s[128] = {0}, u[128] = {0}, u1[128] = {0}, u4[128] = {0}, error_weighted_stat[512] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL;
    int  nx, min, start, end, nv, nx_er = 0, nxv_er = 0;
    double tmp, dx, y2, y3, y2v, y3v, mean, meanv, y4, y4v, r, y2u, y3u, y4u, sigHF, r3;
    double si_er, yi_si2, si2_1, dy2_si2, yiv_si2, si2v_1, dy2v_si2, dy2_si2u, dy2v_si2u;
    double median = 0;
    int index, win_flag;
    char mess[4096] = {0};
    static int val_sel = 0;
    int nti = 0;
    float *y = NULL;




    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine computes the mean Y-value of a dataset\n");
    }

     if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    index = active_menu->flags & 0xFFFFFF00;
    win_flag = (index == WINDOWING_YES) ? 1 : 0;
    nx = dsi->nx;
    min = (dsi->nx > dsi->ny) ? dsi->nx : dsi->ny;

    for (start = i = min - 1; i >= 0 ; i--)
    {
        if (dsi->xd[i] < op->x_hi && dsi->xd[i] >= op->x_lo)
        {
            start = (i < start) ? i : start;
        }
    }

    for (end = i = 0; i < min ; i++)
    {
        if (dsi->xd[i] < op->x_hi && dsi->xd[i] >= op->x_lo)
        {
            end = (i > end) ? i : end;
        }
    }

    end += 1;
    nx = end - start;

    if (nx == 0)
    {
        return win_printf_OK("No points to average !");
    }

    if (win_flag)
    {
        dx = 2 * M_PI / nx;

        for (i = start, nx_er = 0, mean = 0, yi_si2 = si2_1 = 0, nti = 0; i < end ; i++)
        {
            mean += (1 - cos(i * dx)) * dsi->yd[i];
            if (dsi->ye != NULL)
            {
                si_er = dsi->ye[i];

                if (si_er > 0)
                {
                    si_er = (double)1 / si_er;
                    si_er *= si_er;
                    yi_si2 += si_er * dsi->yd[i];
                    si2_1 += si_er;
                    nx_er++;
                }
            }
        }
    }
    else
    {
        for (i = start, nx_er = 0, mean = 0, yi_si2 = si2_1 = 0; i < end ; i++)
        {
            mean += dsi->yd[i];
            if (dsi->ye != NULL)
            {
                si_er = dsi->ye[i];

                if (si_er > 0)
                {
                    si_er = (double)1 / si_er;
                    si_er *= si_er;
                    yi_si2 += si_er * dsi->yd[i];
                    si2_1 += si_er;
                    nx_er++;
                }
            }
        }
    }

    mean /= nx;

    if (nx_er > 0)
    {
        yi_si2 /= si2_1;
    }

    for (i = nv = nxv_er = 0 , meanv = 0, yiv_si2 = si2v_1 = 0; i < min ; i++)
    {
        if (dsi->xd[i] < op->x_hi && dsi->xd[i] >= op->x_lo
                && dsi->yd[i] < op->y_hi && dsi->yd[i] >= op->y_lo)
        {
            meanv += dsi->yd[i];
            nv++;

            if (dsi->ye != NULL)
            {
                si_er = dsi->ye[i];

                if (si_er > 0)
                {
                    si_er = (double)1 / si_er;
                    si_er *= si_er;
                    yiv_si2 += si_er * dsi->yd[i];
                    si2v_1 += si_er;
                    nxv_er++;
                }
            }
        }
    }

    if (nv)
    {
        meanv /= nv;
        y = (float*)calloc(nv, sizeof(float));

    }

    if (nxv_er > 0)
    {
        yiv_si2 /= si2v_1;
    }

    for (i = 0, y4v = y2v = y3v = dy2v_si2 = 0; i < min ; i++)
    {
        if (dsi->xd[i] < op->x_hi && dsi->xd[i] >= op->x_lo
                && dsi->yd[i] < op->y_hi && dsi->yd[i] >= op->y_lo)
        {
            if (y != NULL) y[nti] = dsi->yd[i];
            nti = (nti < nv) ? nti+1 : nv;
            tmp = dsi->yd[i] - meanv;
            y2v += tmp * tmp;
            y3v += tmp * tmp * tmp;
            y4v +=  tmp * tmp * tmp * tmp;

            if (dsi->ye != NULL)
            {
                tmp = dsi->yd[i] - yiv_si2;
                si_er = dsi->ye[i];

                if (si_er > 0)
                {
                    si_er = (double)1 / si_er;
                    si_er *= si_er;
                    dy2v_si2 += si_er * tmp * tmp;
                    si2v_1 += si_er;
                }
            }
        }
    }

    y2v /= (nv > 1) ? nv - 1 : 1;
    y3v /= (nv > 1) ? nv - 1 : 1;
    y4v /= (nv > 1) ? nv - 1 : 1;
    if (y != NULL)
        {
            QuickSort(y, 0, nv-1);
            median = (nv%2 == 0) ? (y[nv/2] + y[1+(nv/2)])/2 : y[nv/2];
            if (y) free(y);
        }
    y = NULL;

    if (nxv_er > 0)
    {
        dy2v_si2 /= si2v_1;
    }

    if (win_flag)
    {
        dx = 2 * M_PI / nx;
        snprintf(s, sizeof(s), "Using a Hanning window\n");

        for (i = start , y4 = y2 = y3 = dy2_si2 = 0; i < end ; i++)
        {
            tmp = dsi->yd[i] - mean;
            y2 += (1 - cos(i * dx)) * tmp * tmp;
            y3 += (1 - cos(i * dx)) * tmp * tmp * tmp;
            y4 += (1 - cos(i * dx)) * tmp * tmp * tmp * tmp;

            if (dsi->ye != NULL)
            {
                tmp = dsi->yd[i] - yi_si2;
                si_er = dsi->ye[i];

                if (si_er > 0)
                {
                    si_er = (double)1 / si_er;
                    si_er *= si_er;
                    dy2_si2 += si_er * tmp * tmp;
                    si2_1 += si_er;
                }
            }
        }
    }
    else
    {
        snprintf(s, sizeof(s), "Using no window\n");

        for (i = start , y4 = y2 = y3 = dy2_si2 = 0; i < end ; i++)
        {
            tmp = dsi->yd[i] - mean;
            y2 += tmp * tmp;
            y3 += tmp * tmp * tmp;
            y4 +=  tmp * tmp * tmp * tmp;

            if (dsi->ye != NULL)
            {
                tmp = dsi->yd[i] - yi_si2;
                si_er = dsi->ye[i];

                if (si_er > 0)
                {
                    si_er = (double)1 / si_er;
                    si_er *= si_er;
                    dy2_si2 += si_er * tmp * tmp;
                    si2_1 += si_er;
                }
            }
        }
    }

    y2 /= (nx > 1) ? nx - 1 : 1;
    y3 /= (nx > 1) ? nx - 1 : 1;
    y4 /= (nx > 1) ? nx - 1 : 1;

    if (nx_er > 0)
    {
        dy2_si2 /= si2_1;
    }

    u[0] = 0;

    if (op->y_unit != NULL)
    {
        snprintf(u, sizeof(u), "(%s)^2", op->y_unit);
    }

    u4[0] = 0;

    if (op->y_unit != NULL)
    {
        snprintf(u4, sizeof(u4), "(%s)^4", op->y_unit);
    }

    strcpy(u1, (op->y_unit != NULL) ? op->y_unit : "pixel");
    get_sigma_of_derivative_of_partial_ds(dsi, start, end, 4 * sqrt(y2), &sigHF);
    y2u = y2 * op->dy * op->dy;
    y3u = y3 * op->dy * op->dy * op->dy;
    y4u = y4 * op->dy * op->dy * op->dy * op->dy;
    dy2_si2u = dy2_si2 * op->dy * op->dy;
    dy2v_si2u = dy2v_si2 * op->dy * op->dy;
    r = (y2 != 0) ? y4 / (y2 * y2) : 0;
    r3 = (y2 > 0) ? y3 / (y2 * sqrt(y2)) : 0;

    if (nv == end - start)
        {
            if (nx_er > 0)
                snprintf(error_weighted_stat, sizeof(error_weighted_stat),
                         "Error weigted statistic over %d points:\n Mean_{er} = %g (raw) = %g %s\n"
                         "\\sigma_{er}^2 = %g (raw) = {\\color{yellow}%g %s}\n"
                         "{\\color{yellow}\\sigma_{er}}  = %g (raw) = {\\color{yellow}%g %s}\n"
                         , nx_er, yi_si2, yi_si2 * op->dy, u1, dy2_si2, dy2_si2u, u, sqrt(dy2_si2)
                         , sqrt(dy2_si2u), u1);
            snprintf(mess,sizeof(mess),"Data set %d with %d points total.\n\n"
                     "Analyzing y component between point %d and %d \n(= %d points). "
                     "(raw refers to data without units).\n"
                     "Do not save any value %%R or select the value you want to save\n"
                     "{\\color{yellow} Mean value} <y>= \\Sigma_{i = n_0 = %d}^{i < n_1 = %d} y_i/(n_1-n_0)\n"
                     "<y> = %g (raw) %%r = {\\color{yellow}%g %s} %%r\n"
                     "Median in y %g (raw) %%r {\\color{yellow}%g %s} %%r\n"
                     "\nMean square fluctuations:"
                     "\\sigma^2 = \\Sigma_{i = n_0 = %d}^{i < n_1 = %d} \\frac{(y_i - <y>)^2}{(n_1 - n_0 - 1)}\n\n"
                     "\\sigma^2 = %g (raw^2) %%r = {\\color{yellow}%g %s} %%r\n"
                     "\\sigma^2/<y> = %g (raw) %%r = {\\color{yellow}%g %s} %%r\n"
                     "{\\color{yellow}\\sigma}  = %g (raw) %%r = {\\color{yellow}%g %s} %%r\n"
                     "{\\color{yellow}\\sigma_{HF}} = %g (raw) %%r {\\color{yellow}%g %s} %%r\n"
                     "(\\sigma_{HF} is computed from signal derivative)\n\n"
                     "{\\color{yellow}Fourth moment}"
                     "\\gamma^4 = \\Sigma_{i = n_0 = %d}^{i < n_1 = %d} \\frac{(y_i - <y>)^4}{(n_1 - n_0 -1)}\n\n"
                     "\\gamma^4 = %g (raw^4) %%r = {\\color{yellow}%g %s} %%r\n"
                     "\\gamma^4/\\sigma^4 = %g %%r \n%s"
                     "\\gamma^3 = %g (raw^3) %%r = {\\color{yellow}%g} %%r\nSkewness %g %%r\n%s"
                     ,pr->one_p->cur_dat, nx, start, end, end - start, start, end
                     ,mean, op->ay + mean * op->dy, u1, median, op->ay + median * op->dy, u1, start, end
                     ,y2, y2u, u,((mean > 0) ? (y2 / mean) : y2)
                     , ((mean > 0) ? (y2u / (mean * op->dy)) : y2u), u1
                     ,sqrtf(y2), sqrtf(y2u), (op->y_unit != NULL) ? op->y_unit : " "
                     , sigHF, sigHF * op->dy, (op->y_unit != NULL) ? op->y_unit : " "
                     , start, end, y4, y4u, u4, r, s, y3, y3u, r3, error_weighted_stat);
            i = win_scanf(mess,&val_sel);
    }
    else
    {
        get_sigma_of_derivative_of_partial_ds(dsi, start, end, 4 * sqrt(y2v), &sigHF);
        y2u = y2v * op->dy * op->dy;
        y4u = y4v * op->dy * op->dy * op->dy * op->dy;
        r = (y2v != 0) ? y4v / (y2v * y2v) : 0;

        if (nxv_er > 0)
            snprintf(error_weighted_stat, sizeof(error_weighted_stat),
                     "Error weigted statistic over %d points:\n Mean_{er} = %g (raw) = %g %s\n"
                     "\\sigma_{er}^2 = %g (raw) = {\\color{yellow}%g %s}\n"
                     "{\\color{yellow}\\sigma_{er}}  = %g (raw) = {\\color{yellow}%g %s}\n"
                     , nxv_er, yiv_si2, yiv_si2 * op->dy, u1, dy2v_si2, dy2v_si2u, u, sqrt(dy2v_si2)
                     , sqrt(dy2v_si2u), u1);

        win_printf_OK("Data set %d with %d points total (flat window).\n\n"
                      "Analyzing y component of visible points\n(= %d points). " //nv
                      "(raw refers to data without units).\n"
                      "{\\color{yellow} Mean value} \n<y> = %g (raw) = {\\color{yellow}%g %s}\n\n"// meanv, meanv, u
                      "Mean square fluctuations \n\n\n"
                      "\\sigma^2 = \\Sigma_{visible} \\frac{(y_i - <y>)^2}{(nv)}\n"
                      "\\sigma^2 = %g (raw^2) = {\\color{yellow}%g %s}\n" //y2v y2u u
                      "\\sigma^2/<y> = %g (raw) = {\\color{yellow}%g %s}\n" // y2v/meanv  y2v/meanv u
                      "{\\color{yellow}\\sigma}  = %g (raw) = {\\color{yellow}%g %s}\n"  //sig, sigu, u
                      "{\\color{yellow}Fourth moment}\n\n"
                      "\\gamma^4 = \\Sigma_{visible} \\frac{(y_i - <y>)^4}{(nv)}\n"
                      "\\gamma^4 = %g (raw^4) = {\\color{yellow}%g %s}\n"  //y4v y4u u
                      "\\gamma^4/\\sigma^4 = %g\n\n\n"  //r
                      "\\gamma^3 = %g (raw^3) = {\\color{yellow}%g}\nSkewness %g\n%s",
                      pr->one_p->cur_dat, nx, nv,
                      meanv, op->ay + meanv * op->dy, u1,
                      y2v, y2u, u,
                      ((meanv > 0) ? (y2v / meanv) : y2v), ((meanv > 0) ? (y2u / (meanv * op->dy)) : y2u), u1,
                      sqrtf(y2v), sqrtf(y2u), (op->y_unit != NULL) ? op->y_unit : " "
                      , y4v, y4u, u4, r, y3, y3u, r3, error_weighted_stat);
    }

    return D_O_K;
}




/********************************************************************/
/* averages a dataset                           */
/********************************************************************/

int compute_running_variance(d_s *dsi,d_s *dso, int window, int shift)
{
  int ndso = (dsi->nx - window) / shift;
  if (ndso <=0 )return 0;
  float mean = 0, meansq = 0, meanx = 0;

  dso = build_adjust_data_set(dso,ndso,ndso);

  for (int i = 0; i < ndso ; i++)
  {
    mean = 0;
    meansq = 0;
    meanx = 0;
    for (int j = 0;j<window;j++)
    {
      if (i*shift+j >= dsi->nx) break;
      mean += dsi->yd[i*shift+j];
      meansq += dsi->yd[i*shift+j]*dsi->yd[i*shift+j];
      meanx += dsi->xd[i*shift+j];
    }
    mean /= window;
    meansq/= window;
    meanx /= window;
    dso->xd[i] = meanx;
    dso->yd[i] = sqrt(meansq - mean*mean);
  }

  return 0;
}

int draw_running_variance(void)
{
  static int shift=32;
  static int window=128;
  O_p *op = NULL;
  d_s *dsi = NULL;
  pltreg *pr = NULL;

  if (updating_menu_state != 0)
  {
      return D_O_K;
  }

  if (key[KEY_LSHIFT])
  {
      return win_printf_OK("This routine computes running variance in y\n");
  }

  if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
  {
      return win_printf_OK("cannot find data!");
  }

  int i = win_scanf("Precise window %6d and shift %6d for the running variance\n",&window,&shift);
  if (i==WIN_CANCEL) return 0;

  int ndso = (dsi->nx - window) / shift;


  d_s *dso = create_and_attach_one_ds(op,ndso,ndso,0);
  compute_running_variance(dsi,dso,  window,  shift);
  return 0;


}

int mean_visible_pts_all_ds(void)
{
    int i, j, k;
    char s[128] = {0}, u[128] = {0}, u1[128] = {0}, u4[128] = {0};
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL;
    int  nx, nv, nds_visi, *ds_visi = NULL, nvl;
    double tmp, y2v, meanv, y4v, r, y2vu, y4vu, *mean_ds = NULL, *mean_si = NULL, y2vl, y2vlu;
    double median = 0;
    int nti = 0;
    float *y = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine computes the mean Y-value\nof all visible points in all datasets\n");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    if (op->n_dat > 0)
    {
        mean_ds = (double *)calloc(op->n_dat, sizeof(double));
        mean_si = (double *)calloc(op->n_dat, sizeof(double));
        ds_visi = (int *)calloc(op->n_dat, sizeof(int));

        if (mean_ds == NULL || mean_si == NULL || ds_visi == NULL)
        {
            return win_printf_OK("cannot allocate memory!");
        }
    }

    for (j = nv = nx = nds_visi = 0 , meanv = 0; j < op->n_dat; j++)
    {
        dsi = op->dat[j];
        nx += dsi->nx;

        for (i = k = 0; i < dsi->nx ; i++)
        {
            if (dsi->xd[i] < op->x_hi && dsi->xd[i] >= op->x_lo
                    && dsi->yd[i] < op->y_hi && dsi->yd[i] >= op->y_lo)
            {
                mean_ds[j] += dsi->yd[i];
                meanv += dsi->yd[i];
                ds_visi[j]++;
                nv++;
                k++;
            }
        }

        if (k)
        {
            nds_visi++;

            if (ds_visi[j])
            {
                mean_ds[j] /= ds_visi[j];
            }
        }
    }

    if (nv)
    {
        meanv /= nv;
        y = (float*)calloc(nv, sizeof(float));
    }

    for (j = nvl = 0, y4v = y2v = y2vl = 0; j < op->n_dat; j++)
    {
        dsi = op->dat[j];

        for (i = 0; i < dsi->nx ; i++)
        {
            if (dsi->xd[i] < op->x_hi && dsi->xd[i] >= op->x_lo
                    && dsi->yd[i] < op->y_hi && dsi->yd[i] >= op->y_lo)
            {
                if (y != NULL) y[nti] = dsi->yd[i];
                nti = (nti < nv) ? nti+1 : nv;
                tmp = dsi->yd[i] - meanv;
                y2v += tmp * tmp;
                y4v +=  tmp * tmp * tmp * tmp;
                tmp = dsi->yd[i] - mean_ds[j];
                mean_si[j] += tmp * tmp;
            }
        }

        if (ds_visi[j] > 1)
        {
            //mean_si[j]/(ds_visi[j] -1);
            y2vl += mean_si[j];
            nvl += ds_visi[j] - 1;
        }
    }

    y2v /= (nv > 1) ? nv - 1 : 1;
    y4v /= (nv > 1) ? nv - 1 : 1;
    y2vl /= (nvl > 0) ? nvl : 1;
    if (y != NULL)
        {
            QuickSort(y, 0, nv-1);
            median = (nv%2 == 0) ? (y[nv/2] + y[1+(nv/2)])/2 : y[nv/2];
            if (y) free(y);
        }
    y = NULL;

    if (mean_ds != NULL)
    {
        free(mean_ds);
    }

    if (mean_si != NULL)
    {
        free(mean_si);
    }

    if (ds_visi != NULL)
    {
        free(ds_visi);
    }

    u[0] = 0;

    if (op->y_unit != NULL)
    {
        snprintf(u, sizeof(u), "(%s)^2", op->y_unit);
    }

    u4[0] = 0;

    if (op->y_unit != NULL)
    {
        snprintf(u4, sizeof(u4), "(%s)^4", op->y_unit);
    }

    strcpy(u1, (op->y_unit != NULL) ? op->y_unit : "pixel");
    y2vu = y2v * op->dy * op->dy;
    y2vlu = y2vl * op->dy * op->dy;
    y4vu = y4v * op->dy * op->dy * op->dy * op->dy;
    r = (y2v != 0) ? y4v / (y2v * y2v) : 0;

    if (nv == nx)
    {
        i = win_printf_OK("Analyzing Y of all %d datasets with a total of %d points.\n\n"
                          "found %d data sets with visible points\n"
                          "(raw refers to data without units).\n"
                          "{\\color{yellow} Mean value} \n<y> = %g (raw) = {\\color{yellow}%g %s}\n\n"
                          "{\\color{yellow} Median value of y} = %g (raw) = {\\color{yellow}%g %s}\n\n"
                          "Mean square fluctuations \n\n\n"
                          "\\sigma^2 = \\Sigma_{i = n0 = %d}^{i < n1 = %d} \\frac{(y_i - <y>)^2}{(n1 - n0 - 1)}\n"
                          "\\sigma^2 = %g (raw^2) = {\\color{yellow}%g %s}\n"
                          "<\\sigma^2> (with mean value of each ds) = %g (raw^2) = {\\color{yellow}%g %s}\n"
                          "\\sigma^2/<y> = %g (raw) = {\\color{yellow}%g %s}\n"
                          "{\\color{yellow}\\sigma}  = %g (raw) = {\\color{yellow}%g %s}\n"
                          "<\\sigma> (with <y> of each ds) = %g (raw) = {\\color{yellow}%g %s}\n"
                          "{\\color{yellow}Fourth moment}\n\n"
                          "\\gamma^4 = \\Sigma_{i = n0 = %d}^{i < n1 = %d} \\frac{(y_i - <y>)^4}{(n1 - n0 -1)}\n"
                          "\\gamma^4 = %g (raw^4) = {\\color{yellow}%g %s}\n"
                          "\\gamma^4/\\sigma^4 = %g\n\n\n%s",
                          pr->one_p->n_dat, nv, nds_visi,
                          meanv, op->ay + meanv * op->dy, u1,median, op->ay + median * op->dy, u1, 0, nx,
                          y2v, y2vu, u,
                          y2vl, y2vlu, u,
                          ((meanv > 0) ? (y2v / meanv) : y2v), ((meanv > 0) ? (y2vu / (meanv * op->dy)) : y2vu), u1,
                          sqrtf(y2v), sqrtf(y2vu), (op->y_unit != NULL) ? op->y_unit : " "
                          , sqrtf(y2vl), sqrtf(y2vlu), (op->y_unit != NULL) ? op->y_unit : " "
                          , 0, nv, y4v, y4vu, u4, r, s);
    }
    else
    {
        i = win_printf_OK("Analyzing Y of visible points of all %d datasets\nwith a total of %d points.\n\n"
                          "found %d data sets with visible points\n"
                          "(raw refers to data without units).\n"
                          "{\\color{yellow} Mean value} \n<y> = %g (raw) = {\\color{yellow}%g %s}\n\n"
                          "{\\color{yellow} Median value of y} = %g (raw) = {\\color{yellow}%g %s}\n\n"
                          "Mean square fluctuations \n\n\n"
                          "\\sigma^2 = \\Sigma_{i = n0 = %d}^{i < n1 = %d} \\frac{(y_i - <y>)^2}{(n1 - n0 - 1)}\n"
                          "\\sigma^2 = %g (raw^2) = {\\color{yellow}%g %s}\n"
                          "<\\sigma^2> (with <y> of each ds) = %g (raw^2) = {\\color{yellow}%g %s}\n"
                          "\\sigma^2/<y> = %g (raw) = {\\color{yellow}%g %s}\n"
                          "{\\color{yellow}\\sigma}  = %g (raw) = {\\color{yellow}%g %s}\n"
                          "<\\sigma> (with <y> of each ds) = %g (raw) = {\\color{yellow}%g %s}\n"
                          "{\\color{yellow}Fourth moment}\n\n"
                          "\\gamma^4 = \\Sigma_{i = n0 = %d}^{i < n1 = %d} \\frac{(y_i - <y>)^4}{(n1 - n0 -1)}\n"
                          "\\gamma^4 = %g (raw^4) = {\\color{yellow}%g %s}\n"
                          "\\gamma^4/\\sigma^4 = %g\n\n\n%s",
                          pr->one_p->n_dat, nv, nds_visi,
                          meanv, op->ay + meanv * op->dy, u1,median, op->ay + median * op->dy, u1, 0, nx,
                          y2v, y2vu, u,
                          y2vl, y2vlu, u,
                          ((meanv > 0) ? (y2v / meanv) : y2v), ((meanv > 0) ? (y2vu / (meanv * op->dy)) : y2vu), u1,
                          sqrtf(y2v), sqrtf(y2vu), (op->y_unit != NULL) ? op->y_unit : " "
                          , sqrtf(y2vl), sqrtf(y2vlu), (op->y_unit != NULL) ? op->y_unit : " "
                          , 0, nv, y4v, y4vu, u4, r, s);
    }

    return D_O_K;
}








/********************************************************************/
/* operations on two data-sets                                      */
/********************************************************************/

int y1_y2_same_x(void)
{
    int i;//, j;, k;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi1 = NULL, *dsi2 = NULL, *dss = NULL;
    int   ids1, ids2; // , index, nxn
    float x_cur;
    //static int doindex = 0;
    //float dx;
    char question[1024] = {0};

    if (updating_menu_state != 0)
    {
        if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        if (!op || op->n_dat < 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Draw a dataset as a function of another if they have same x_coordinates");
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return (win_printf_OK("cannot find data!"));
    }

    if (op->n_dat < 2)
    {
        return (win_printf_OK("No enought data set!\n2 datasets are required!"));
    }

    ids1 = op->cur_dat;
    ids2 = (ids1 + 1 < op->n_dat) ? ids1 + 1 : 0;

    if (op->n_dat > 24)
    {
        i = win_scanf("Operation on two data sets\nData set 1 %8d data set 2 %8d\n", &ids1, &ids2);

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }
    }
    else
    {
        snprintf(question, sizeof(question), "Operation on two data sets\n"
                 "First Data set %d\nchoose Data set 2:\n", ids1);

        for (i = 0; i < op->n_dat; i++)
        {
            if (i == 0)
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%R ", i);
            }
            else
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%r ", i);
            }

            if (i % 10 == 9)
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "\n");
            }
        }
        snprintf(question + strlen(question), sizeof(question) - strlen(question), "\n");
        i = win_scanf(question, &ids2);

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }
    }

    if (ids1 < 0 || ids1 >= op->n_dat)
    {
        return win_printf_OK("Data set 1 index %d out of range [0, %d[!", ids1, op->n_dat);
    }

    if (ids2 < 0 || ids2 >= op->n_dat)
    {
        return win_printf_OK("Data set 2 index %d out of range [0, %d[!", ids2, op->n_dat);
    }

    dsi1 = op->dat[ids1];
    dsi2 = op->dat[ids2];

    if (dsi1 == NULL || dsi2 == NULL)
    {
        return (win_printf_OK("can't find 2 datasets"));
    }

    //nx1 = dsi1->nx;
    //nx2 = dsi2->nx;
    //nxn = (nx1 > nx2) ? nx2 : nx1;
    //bou c'est pas beau
    dss = build_data_set(64,64);
    dss->nx = dss->ny = 0;

    for (int i1 = 0;i1<dsi1->nx;i1++)
    {
      x_cur = dsi1->xd[i1];
      for (int j1 = 0;j1<dsi2->nx;j1++)
      {
        if (dsi2->xd[j1]==x_cur) add_new_point_to_ds(dss,dsi1->yd[i1],dsi2->yd[j1]);
      }
    }
    add_one_plot_data(pr->one_p, IS_DATA_SET, (void *)dss);
    return refresh_plot(pr, UNCHANGED);
}






int opp_on_two_ds(void)
{
    int i, j, k;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi1 = NULL, *dsi2 = NULL, *dss = NULL;
    int  nx1, nx2, nxn, index, ids1, ids2;
    static int doindex = 0;
    float dx;
    char question[1024] = {0};

    if (updating_menu_state != 0)
    {
        if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        if (!op || op->n_dat < 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Arithmetical operations on two datasets\n"
                             "a new dataset is created");
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return (win_printf_OK("cannot find data!"));
    }

    if (op->n_dat < 2)
    {
        return (win_printf_OK("No enought data set!\n2 datasets are required!"));
    }

    ids1 = op->cur_dat;
    ids2 = (ids1 + 1 < op->n_dat) ? ids1 + 1 : 0;

    if (op->n_dat > 24)
    {
        i = win_scanf("Operation on two data sets\nData set 1 %8d data set 2 %8d\n", &ids1, &ids2);

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }
    }
    else
    {
        snprintf(question, sizeof(question), "Operation on two data sets\n"
                 "First Data set %d\nchoose Data set 2:\n", ids1);

        for (i = 0; i < op->n_dat; i++)
        {
            if (i == 0)
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%R ", i);
            }
            else
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%r ", i);
            }

            if (i % 10 == 9)
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "\n");
            }
        }
        snprintf(question + strlen(question), sizeof(question) - strlen(question), "\n");
        i = win_scanf(question, &ids2);

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }
    }

    if (ids1 < 0 || ids1 >= op->n_dat)
    {
        return win_printf_OK("Data set 1 index %d out of range [0, %d[!", ids1, op->n_dat);
    }

    if (ids2 < 0 || ids2 >= op->n_dat)
    {
        return win_printf_OK("Data set 2 index %d out of range [0, %d[!", ids2, op->n_dat);
    }

    dsi1 = op->dat[ids1];
    dsi2 = op->dat[ids2];

    if (dsi1 == NULL || dsi2 == NULL)
    {
        return (win_printf_OK("can't find 2 datasets"));
    }

    nx1 = dsi1->nx;
    nx2 = dsi2->nx;
    nxn = (nx1 > nx2) ? nx2 : nx1;

    if (nx1 != nx2)
        i =  win_printf_OK("data sets have different size %d  and %d!\n"
                           "I propose to add them over %d", nx1, nx2, nxn);

    if (i == WIN_CANCEL)
    {
        return (D_O_K);
    }

    for (i = 0 , j = 0; i < nxn ; i++)
    {
        k = i + 1;
        k = (k < nxn) ? k : nxn - 1;
        k = (k < 0) ? 0 : k;

        if (i != k)
        {
            dx = dsi1->xd[i] - dsi1->xd[k];
            dx = (dx == 0) ? 0.00001 : fabs(dx * 0.00001);
            j += (fabs(dsi1->xd[i] - dsi2->xd[i]) > dx) ? 1 : 0;

            if (j == 1)  win_printf("Point at index %d \n"
				    "x1 = %g x2 = %g\n"
				    "dx = %g diff = %g!"
				    , i, dsi1->xd[i], dsi2->xd[i], dx
				    , dsi1->xd[i]- dsi2->xd[i]);
        }
    }

    if (j)
    {
        win_scanf("the X axis differ by more than 10^{-5} relative\n"
                  "to the interval to the next point.\n"
                  "I propose to treat points having the same index and \n"
                  "averaging theis absisca only if you click here %b \n"
                  "Be carefull the result might be different from what you expected!", &doindex);

        if (doindex == 0)
        {
            return 0;
        }
    }

    dss = duplicate_data_set(dsi1, NULL);
    if (dss == NULL)
    {
        return (win_printf_OK("can't create data set"));
    }

    index = active_menu->flags & 0xFFFFFF00;

    //I'm combining errors as if statistically independent
    if (index == ADD)
    {
        for (i = 0 ; i < nxn ; i++)
        {
            if (doindex)
            {
                dss->xd[i] = (dss->xd[i] + dsi2->xd[i]) / 2;
            }

            dss->yd[i] += dsi2->yd[i];

            if (dss->ye != NULL && dsi2->ye != NULL)
            {
                dss->ye[i] = sqrt(dss->ye[i] * dss->ye[i] + dsi2->ye[i] * dsi2->ye[i]);
            }
            else if (dss->ye != NULL && dsi2->ye == NULL)
            {
                dss->ye[i] = fabsf(dss->ye[i] * dsi2->yd[i]);
            }
            else if (dss->ye != NULL && dsi2->ye != NULL)
            {
                dss->ye[i] = fabsf(dss->yd[i] * dsi2->ye[i]);
            }
        }

        inherit_from_ds_to_ds(dss, dsi1);

        if (doindex)
        {
            set_ds_treatement(dss, "added Y by index in X with dataset %s", dsi2->source);
        }
        else
        {
            set_ds_treatement(dss, "added Y with dataset %s", dsi2->source);
        }
    }
    else if (index == SUBSTRACT)
    {
        for (i = 0 ; i < nxn ; i++)
        {
            if (doindex)
            {
                dss->xd[i] = (dss->xd[i] + dsi2->xd[i]) / 2;
            }

            dss->yd[i] -= dsi2->yd[i];

            if (dss->ye != NULL && dsi2->ye != NULL)
            {
                dss->ye[i] = sqrt(dss->ye[i] * dss->ye[i] + dsi2->ye[i] * dsi2->ye[i]);
            }
            else if (dss->ye != NULL && dsi2->ye == NULL)
            {
                dss->ye[i] = fabsf(dss->ye[i] * dsi2->yd[i]);
            }
            else if (dss->ye != NULL && dsi2->ye != NULL)
            {
                dss->ye[i] = fabsf(dss->yd[i] * dsi2->ye[i]);
            }
        }

        inherit_from_ds_to_ds(dss, dsi1);

        if (doindex)
        {
            set_ds_treatement(dss, "substracted Y by index in X with dataset %s", dsi2->source);
        }
        else
        {
            set_ds_treatement(dss, "substracted Y data from dataset %s", dsi2->source);
        }
    }
    else if (index == MULTIPLY)
    {
        free(dss);
        dss = multiply_on_two_ds_by_index(dsi1, dsi2, doindex);
    }
    else if (index == Y_OF_X_YY)
    {
      if (dsi1->ye != NULL)          alloc_data_set_x_error(dss);
      else if (dss->xe != NULL)      free_data_set_x_error(dss);
      if (dsi1->yed != NULL)         alloc_data_set_x_down_error(dss);
      else if (dss->xed != NULL)     free_data_set_x_down_error(dss);
      if (dsi1->ybu != NULL)         alloc_data_set_x_box_up(dss);
      else if (dss->xbu != NULL)     free_data_set_x_box_up(dss);
      if (dsi1->ybd != NULL)         alloc_data_set_x_box_dwn(dss);
      else if (dss->xbd != NULL)     free_data_set_x_box_dwn(dss);


      if (dsi2->ye != NULL)          alloc_data_set_y_error(dss);
      else if (dss->ye != NULL)      free_data_set_y_error(dss);
      if (dsi2->yed != NULL)         alloc_data_set_y_down_error(dss);
      else if (dss->yed != NULL)     free_data_set_y_down_error(dss);
      if (dsi1->ybu != NULL)         alloc_data_set_y_box_up(dss);
      else if (dss->ybu != NULL)     free_data_set_y_box_up(dss);
      if (dsi1->ybd != NULL)         alloc_data_set_y_box_dwn(dss);
      else if (dss->ybd != NULL)     free_data_set_y_box_dwn(dss);

        for (i = 0 ; i < nxn ; i++)
        {
            dss->xd[i] = dsi1->yd[i];
            dss->yd[i] = dsi2->yd[i];

            if (dss->xe != NULL && dsi1->ye != NULL)
            {
                dss->xe[i] = dsi1->ye[i];
            }
            if (dss->xed != NULL && dsi1->yed != NULL)
            {
                dss->xed[i] = dsi1->yed[i];
            }
            if (dss->xbu != NULL && dsi1->ybu != NULL)
            {
                dss->xbu[i] = dsi1->ybu[i];
            }
            if (dss->xbd != NULL && dsi1->ybd != NULL)
            {
                dss->xbd[i] = dsi1->ybd[i];
            }

            if (dss->ye != NULL && dsi2->ye != NULL)
            {
                dss->ye[i] = dsi2->ye[i];
            }
            if (dss->yed != NULL && dsi2->yed != NULL)
            {
                dss->yed[i] = dsi2->yed[i];
            }
            if (dss->ybu != NULL && dsi2->ybu != NULL)
            {
                dss->ybu[i] = dsi2->ybu[i];
            }
            if (dss->ybd != NULL && dsi2->ybd != NULL)
            {
                dss->ybd[i] = dsi2->ybd[i];
            }
        }

        inherit_from_ds_to_ds(dss, dsi1);
        set_ds_treatement(dss, "Y(X) with Y from %s", dsi2->source);
    }
    else if (index == DIVIDE)
    {
        for (i = 0 ; i < nxn ; i++)
        {
            if (doindex)
            {
                dss->xd[i] = (dss->xd[i] + dsi2->xd[i]) / 2;
            }

            if (dsi2->yd[i] != 0)
            {
                dss->yd[i] /= dsi2->yd[i];
            }
            else
            {
                win_printf_OK("Division by Zero at point %d", i);
            }

            if (dss->ye != NULL && dsi2->ye != NULL)
                dss->ye[i] = dss->yd[i] * sqrt((dss->ye[i] * dss->ye[i]) / (dss->yd[i] * dss->yd[i]) + (dsi2->ye[i] * dsi2->ye[i]) /
                                               (dsi2->yd[i] * dsi2->yd[i]));
            else if (dss->ye != NULL && dsi2->ye == NULL)
            {
                dss->ye[i] = fabsf(dss->ye[i] * dsi2->yd[i]);
            }
            else if (dss->ye == NULL && dsi2->ye != NULL)
            {
                // fx !!!!!!!!!!!!!!!
                //dss->ye[i] = fabsf(dss->yd[i] * dsi2->ye[i]);
            }
        }

        inherit_from_ds_to_ds(dss, dsi1);

        if (doindex)
        {
            set_ds_treatement(dss, "divided Y by index in X with dataset %s", dsi2->source);
        }
        else
        {
            set_ds_treatement(dss, "divided Y with dataset %s", dsi2->source);
        }
    }
    else
    {
        free_data_set(dss);
        return (win_printf_OK("wrong operation between data sets!"));
    }

    add_one_plot_data(pr->one_p, IS_DATA_SET, (void *)dss);
    return refresh_plot(pr, UNCHANGED);
}
d_s *add_on_2_ds_by_index(const d_s *ds1, const d_s *ds2, bool doindex)
{
    return add_on_2_ds_by_index_with_offset(ds1, ds2, doindex, 0);
}
d_s *add_on_2_ds_by_index_with_offset(const d_s *ds1, const d_s *ds2, bool doindex, int ds2_offset)
{
    d_s *res_ds = duplicate_data_set(ds1, NULL);
    int nxn = my_min(ds1->nx, ds2->nx - abs(ds2_offset));
    float tmp = 0;

    if (!res_ds->ye && ds2->ye)
    {
        alloc_data_set_y_error(res_ds);
    }

    if (res_ds->ye)
    {
        memset(res_ds->ye, 0, res_ds->ny * sizeof(float));
    }

    for (int i = ds2_offset < 0 ? abs(ds2_offset) : 0; i < nxn ; i++)
    {
        if (doindex)
        {
            res_ds->xd[i] += ds2->xd[i + ds2_offset];
            res_ds->xd[i] /= 2;
        }

        res_ds->yd[i] += ds2->yd[i + ds2_offset];

        if (ds1->ye != NULL)
        {
            tmp = ds1->yd[i];
            tmp *= tmp;

            if (tmp != 0)
            {
                res_ds->ye[i] += (ds1->ye[i] * ds1->ye[i]) / tmp;
            }
        }

        if (ds2->ye != NULL)
        {
            tmp = ds2->yd[i];
            tmp *= tmp;

            if (tmp != 0)
            {
                res_ds->ye[i] += (ds2->ye[i + ds2_offset] * ds2->ye[i + ds2_offset]) / tmp;
            }
        }
    }

    inherit_from_ds_to_ds(res_ds, ds1);

    if (doindex)
    {
        set_ds_treatement(res_ds, "Added Y by index in X with dataset %s", ds2->source);
    }
    else
    {
        set_ds_treatement(res_ds, "Added Y with dataset %s", ds2->source);
    }

    return res_ds;
}


d_s *multiply_on_two_ds_by_index_with_offset(const d_s *ds1, const d_s *ds2, bool doindex, int ds2_offset)
{
    d_s *res_ds = duplicate_data_set(ds1, NULL);

    if (ds1 == NULL || ds2 == NULL)
    {
        return NULL;
    }

    int nxn = my_min(ds1->nx, ds2->nx - abs(ds2_offset));

    for (int i = ds2_offset < 0 ? abs(ds2_offset) : 0; i < nxn ; i++)
    {
        if (doindex)
        {
            res_ds->xd[i] += ds2->xd[i + ds2_offset];
            res_ds->xd[i] /= 2;
        }

        res_ds->yd[i] *= ds2->yd[i + ds2_offset];

        if (res_ds->ye != NULL && ds2->ye != NULL)
        {
            res_ds->ye[i] = res_ds->yd[i] * sqrt((res_ds->ye[i] * res_ds->ye[i]) / (res_ds->yd[i] * res_ds->yd[i]) +
                                                 (ds2->ye[i + ds2_offset] * ds2->ye[i + ds2_offset]) / (ds2->yd[i + ds2_offset] * ds2->yd[i + ds2_offset]));
        }
        else if (res_ds->ye != NULL && ds2->ye == NULL)
        {
            res_ds->ye[i] = fabsf(res_ds->ye[i] * ds2->yd[i + ds2_offset]);
        }
        else if (res_ds->ye != NULL && ds2->ye != NULL)
        {
            res_ds->ye[i] = fabsf(res_ds->yd[i] * ds2->ye[i + ds2_offset]);
        }
    }

    inherit_from_ds_to_ds(res_ds, ds1);

    if (doindex)
    {
        set_ds_treatement(res_ds, "Multiply Y by index in X with dataset %s", ds2->source);
    }
    else
    {
        set_ds_treatement(res_ds, "multiplied Y with dataset %s", ds2->source);
    }

    return res_ds;
}

d_s *multiply_on_two_ds_by_index(const d_s *ds1, const d_s *ds2, bool doindex)
{
    return multiply_on_two_ds_by_index_with_offset(ds1, ds2, doindex, 0);
}
int  sub_on_2_ds_by_index(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi1 = NULL;
    d_s *dsi2 = NULL;
    d_s *dsd = NULL;
    int  nx1, nx2,  npx;

    if (updating_menu_state != 0)
    {
        if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        if (!op || op->n_dat < 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("These routines perform an substration operations\n"
                             "on 2 the datasets of a plot on both x an y for points\n"
                             "having the same index.");

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi1) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    if (op->n_dat < 2)
    {
        return (win_printf_OK("No enought data set!\n2 datasets are required!"));
    }

    i = op->cur_dat;
    i = (i + 1 >= op->n_dat) ? 0 : i + 1;
    dsi2 = op->dat[i];

    if (dsi1 == NULL || dsi2 == NULL)
    {
        return (win_printf_OK("can't find data set"));
    }

    nx1 = dsi1->nx;
    nx2 = dsi2->nx;
    npx = (nx1 < nx2) ? nx1 : nx2;
    /*
    ner = 0;
    if (dsi1->ye != NULL) ner++;
    if (dsi2->ye != NULL) ner++;
    */
    dsd = create_and_attach_one_ds(op, npx, npx, 0);

    if (dsd == NULL)
    {
        return win_printf_OK("cannot create plot !");
    }

    /*
    if (ner != 0)
      {
    if((alloc_data_set_y_error(dsd) == NULL) || (dsd->ye == NULL))
        win_printf_OK("I can't create errors 4!");
      }
    */
    for (i = 0 ; i < npx ; i++)
    {
        dsd->xd[i] = dsi1->xd[i] - dsi2->xd[i];
        dsd->yd[i] = dsi1->yd[i] - dsi2->yd[i];
        /*
        if (dsi1->ye != NULL)
          {
            tmp = dsi1->yd[k];
            tmp *= tmp;
            if (tmp != 0)     dsd->ye[i] += (dsi1->ye[k]*dsi1->ye[k])/tmp;
            else win_printf("You had a divide by zero in error calculation\n"
                    "at ds1 and point %d",i);
          }
        */
    }

    /*
    for (i=0 ; i< nx1 ; i++)
      {
    nd = 1;
    nde = (dsi1->ye != NULL) ? 1 : 0;
    for (k = 0; (k < dsi2->nx) && (dsi1->xd[i] != dsi2->xd[k]); k++);
    if (k == dsi2->nx) continue;
    dsd->yd[i] += dsi2->yd[k];
    nd++;
    if (dsi2->ye != NULL)
      {
        tmp = dsi2->yd[k];
        tmp *= tmp;
        if (tmp != 0)
          {
        dsd->ye[i] += (dsi2->ye[k]*dsi2->ye[k])/tmp;
        nde++;
          }
        else win_printf("You had a divide by zero in error calculation\n"
                "at ds2 and point %d",i);
      }
    //dsd->yd[i] /= nd; modif vc
    if (dsd->ye != NULL && nde != 0)
      dsd->ye[i] = dsd->yd[i]*sqrtf(dsd->ye[i]/nde);
      }
    */
    set_ds_treatement(dsd, "substract of 2 ds by index");
    dsd->ny = npx;
    dsd->nx = npx;
    inherit_from_ds_to_ds(dsd, dsi1);
    dsd->m = dsi1->m;
    set_ds_point_symbol(dsd, dsi1->symb);
    return refresh_plot(pr, UNCHANGED);
}


int  substract_on_ds_to_all_other_by_index(void)
{
    int i, j;
    pltreg *pr = NULL;
    O_p *op = NULL;
    O_p *opd = NULL;
    d_s *dsi1 = NULL;
    d_s *dsi2 = NULL;
    char question[1024] = {0};
    int ids1, ids2;

    if (updating_menu_state != 0)
    {
        if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        if (!op || op->n_dat < 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("This routine performs an substration operations\n"
                             "of one dataset to all the other of the plot.\n"
                             "Substraction in Y occurs for points having the same index.\n");

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi1) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    if (op->n_dat < 2)
    {
        return (win_printf_OK("No enought data set!\n2 datasets are required!"));
    }

    ids2 = op->cur_dat;

    if (op->n_dat > 24)
    {
        i = win_scanf("This routine performs an substration operations\n"
                      "of one dataset to all the other of the plot.\n"
                      "Substraction in Y occurs for points having the same index.\n"
                      "Specify the Data set to substract %8d\n", &ids2);

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }
    }
    else
    {
        snprintf(question, sizeof(question), "This routine performs a substration operation\n"
                 "of one dataset to all the other of the plot.\n"
                 "Substraction in Y occurs for points having the same index.\n"
                 "Choose Data set to substract:\n");

        for (i = 0; i < op->n_dat; i++)
        {
            if (i == 0)
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%R ", i);
            }
            else
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%r ", i);
            }

            if (i % 10 == 9)
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "\n");
            }
        }

        i = win_scanf(question, &ids2);

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }
    }

    if (ids2 < 0 || ids2 >= op->n_dat)
    {
        return win_printf_OK("Data set 2 index %d out of range [0, %d[!", ids2, op->n_dat);
    }

    opd = duplicate_plot_data(op, NULL);

    if (opd == NULL)
    {
        return win_printf_OK("cannot duplicate plot!");
    }

    add_data_to_pltreg(pr, IS_ONE_PLOT, (void *)opd);

    for (ids1 = 0, dsi2 = opd->dat[ids2]; ids1 < opd->n_dat; ids1++)
    {
        dsi1 = opd->dat[ids1];

        if (ids1 == ids2)
        {
            continue;
        }

        dsi2 = opd->dat[ids2];

        for (j = 0; j < dsi1->nx && j < dsi2->nx; j++)
        {
            dsi1->yd[j] -= dsi2->yd[j];

            if (dsi1->ye != NULL && dsi2->ye != NULL)
            {
                dsi1->ye[j] = sqrt(dsi1->ye[j] * dsi1->ye[j] + dsi2->ye[j] * dsi2->ye[j]);
            }

            set_ds_treatement(dsi1, "Substrat data set %d -> %s", ids2, (dsi2->source) ? dsi2->source : "No source");
        }

        dsi1->nx = dsi1->ny = j;
    }

    opd->need_to_refresh = 1;
    remove_ds_from_op(opd, dsi2);
    return refresh_plot(pr, pr->n_op - 1);
}


int  do_add_on_2_ds(void)
{
    int i, ids1, ids2;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi1 = NULL;
    d_s *dsi2 = NULL;
    char question[2048] = {0};

    if (updating_menu_state != 0)
    {
        if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        if (!op || op->n_dat < 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("These routines perform an addition operations\n"
                             "on 2 the datasets of a plot, the selected ds\n"
                             "is the reference for x position a new plot is created");

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi1) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    if (op->n_dat < 2)
    {
        return (win_printf_OK("No enought data set!\n2 datasets are required!"));
    }



    ids1 = op->cur_dat;
    ids2 = (ids1 + 1 < op->n_dat) ? ids1 + 1 : 0;

    if (op->n_dat > 24)
    {
        i = win_scanf("Addition of two data sets\nData set 1 %8d data set 2 %8d\n", &ids1, &ids2);

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }
    }
    else
    {
        snprintf(question, sizeof(question), "Addition of two data sets\n"
                 "First Data set %d\nchoose Data set 2:\n", ids1);

        for (i = 0; i < op->n_dat; i++)
        {
            if (i == 0)
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%R ", i);
            }
            else
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%r ", i);
            }

            if (i % 10 == 9)
            {
                snprintf(question + strlen(question), sizeof(question) - strlen(question), "\n");
            }
        }
        snprintf(question + strlen(question), sizeof(question) - strlen(question), "\n");
        i = win_scanf(question, &ids2);

        if (i == WIN_CANCEL)
        {
            return D_O_K;
        }
    }

    if (ids1 < 0 || ids1 >= op->n_dat)
    {
        return win_printf_OK("Data set 1 index %d out of range [0, %d[!", ids1, op->n_dat);
    }

    if (ids2 < 0 || ids2 >= op->n_dat)
    {
        return win_printf_OK("Data set 2 index %d out of range [0, %d[!", ids2, op->n_dat);
    }

    dsi1 = op->dat[ids1];
    dsi2 = op->dat[ids2];

    if (dsi1 == NULL || dsi2 == NULL)
    {
        return (win_printf_OK("can't find 2 datasets"));
    }


    add_one_plot_data(op, IS_DATA_SET, add_on_2_ds_same_x(dsi1, dsi2));
    return refresh_plot(pr, UNCHANGED);
}

int do_erase_all_from_one_ds(void)
{
  int i, j;
  pltreg *pr = NULL;
  O_p *op = NULL;
  d_s *dsi1 = NULL;
  d_s *dsi2 = NULL;
  d_s *dscleared = NULL;
  float low_r;
  float high_r;
  char question[1024] = {0};
  int  ids2;

  if (updating_menu_state != 0)
  {
      if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
      {
          active_menu->flags |=  D_DISABLED;
      }
      else
      {
          active_menu->flags &= ~D_DISABLED;
      }

      if (!op || op->n_dat < 2)
      {
          active_menu->flags |=  D_DISABLED;
      }
      else
      {
          active_menu->flags &= ~D_DISABLED;
      }

      return D_O_K;
  }



  if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi1) != 3)
  {
      return win_printf_OK("cannot find data!");
  }

  if (op->n_dat < 2)
  {
      return (win_printf_OK("No enought data set!\n2 datasets are required!"));
  }

  ids2 = op->cur_dat;

  if (op->n_dat > 24)
  {
      i = win_scanf("This routine removes point if their x corresponds to a x of the selected dataset that has\n"
                    "y outside the range [%6f,%6f]\n"
                    "Specify the Data set to base on %8d\n", &low_r,&high_r,&ids2);

      if (i == WIN_CANCEL)
      {
          return D_O_K;
      }
  }
  else
  {
      snprintf(question, sizeof(question), "This routine removes point if their x corresponds to a x of the selected dataset that has\n"
                    "y outside the range [%%6f,%%6f]\n"
                    "Select the dataset :\n");
      for (i = 0; i < op->n_dat; i++)
      {
          if (i == 0)
          {
              snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%R ", i);
          }
          else
          {
              snprintf(question + strlen(question), sizeof(question) - strlen(question), "%d->%%r ", i);
          }

          if (i % 10 == 9)
          {
              snprintf(question + strlen(question), sizeof(question) - strlen(question), "\n");
          }
      }

      i = win_scanf(question, &low_r,&high_r,&ids2);

      if (i == WIN_CANCEL)
      {
          return D_O_K;
      }
  }

  dsi1 = op->dat[ids2];

  int current_ndat = op->n_dat;
  for (i = 0;i<current_ndat;i++)
  {
    if (i==ids2) continue;
    dsi2 = op->dat[i];
    dscleared = create_and_attach_one_ds(op,16,16,0);
    dscleared->nx = dscleared->ny=0;
    inherit_from_ds_to_ds(dscleared,dsi2);

    for (j = 0;j<dsi2->nx;j++)
    {

              if (dsi1->yd[j] > low_r && dsi1->yd[j] < high_r)
              {
                add_new_point_to_ds(dscleared,dsi2->xd[j],dsi2->yd[j]);
                }

  }
}


  dsi2 = dsi1;
  dscleared = create_and_attach_one_ds(op,16,16,0);
  dscleared->nx = dscleared->ny=0;
  inherit_from_ds_to_ds(dscleared,dsi2);

  for (j = 0;j<dsi2->nx;j++)
  {

            if (dsi1->yd[j] > low_r && dsi1->yd[j] < high_r)
            {
              add_new_point_to_ds(dscleared,dsi2->xd[j],dsi2->yd[j]);
              }

}



return 0;
}

d_s *add_on_2_ds_same_x(const d_s *src_ds1, const d_s *src_ds2)
{
    int nx1 = 0;
    int nx2 = 0;
    int npx = 0;
    int ner = 0;
    int nd = 0;
    int nde = 0;
    int last_ds2_pt_in_ds1 = 0;
    float tmp = 0.;
    const d_s *tmp_ds = NULL;
    d_s *dest_ds = NULL;

    if (src_ds1 == NULL || src_ds2 == NULL)
    {
        (win_printf_OK("can't find data set"));
        return NULL;
    }

    nx1 = src_ds1->nx;
    nx2 = src_ds2->nx;

    // we swap dataset if needed to have the biggest first.
    if (nx1 < nx2)
    {
        tmp_ds = src_ds2;
        src_ds2 = src_ds1;
        src_ds1 = tmp_ds;
        nx1 = nx2;
        nx2 = src_ds2->nx;
    }

    // check that all ds hve same x coordinates

    for (last_ds2_pt_in_ds1 = 0, npx = 0; last_ds2_pt_in_ds1 < src_ds2->nx && npx == 0; last_ds2_pt_in_ds1++)
    {
        int i = 0;

        while ((i < src_ds1->nx) && (src_ds1->xd[i] != src_ds2->xd[last_ds2_pt_in_ds1]))
        {
            i++;
        }

        npx = (i == src_ds1->nx);

        if (npx)
        {
            win_printf("points %d x -> %g of ds2 has no correspondant", last_ds2_pt_in_ds1, src_ds2->xd[last_ds2_pt_in_ds1]);
        }
    }

    if (npx != 0)
    {
        win_printf_OK("your datasets have differents x values\n"
                      "cannot process them\n");
        return NULL;
    }

    ner = !!src_ds1->ye + !!src_ds2->ye;
    dest_ds = build_data_set(nx1, nx1);

    if (dest_ds == NULL)
    {
        win_printf_OK("cannot create plot !");
        return NULL;
    }

    if (ner != 0)
    {
        if ((alloc_data_set_y_error(dest_ds) == NULL) || (dest_ds->ye == NULL))
        {
            win_printf_OK("I can't create errors 4!");
        }
    }

    for (int i = 0 ; i < nx1 ; i++)
    {
        dest_ds->xd[i] = src_ds1->xd[i];
        dest_ds->yd[i] = src_ds1->yd[i];

        if (src_ds1->ye != NULL)
        {
            tmp = src_ds1->yd[i];
            tmp *= tmp;

            if (tmp != 0)
            {
                dest_ds->ye[i] += (src_ds1->ye[i] * src_ds1->ye[i]) / tmp;
            }
            else win_printf("You had a divide by zero in error calculation\n"
                                "at ds1 and point %d", i);
        }
    }

    for (int i = 0 ; i < nx1 ; i++)
    {
        nd = 1;
        nde = (src_ds1->ye != NULL) ? 1 : 0;
        int k = 0;

        while ((k < src_ds2->nx) && (src_ds1->xd[i] != src_ds2->xd[k]))
        {
            ++k;
        }

        if (k == src_ds2->nx)
        {
            continue;
        }

        dest_ds->yd[i] += src_ds2->yd[k];
        nd++;

        if (src_ds2->ye != NULL)
        {
            tmp = src_ds2->yd[k];
            tmp *= tmp;

            if (tmp != 0)
            {
                dest_ds->ye[i] += (src_ds2->ye[k] * src_ds2->ye[k]) / tmp;
                nde++;
            }
            else win_printf("You had a divide by zero in error calculation\n"
                                "at ds2 and point %d", i);
        }

        //dest_ds->yd[i] /= nd; modif vc
        if (dest_ds->ye != NULL && nde != 0)
        {
            dest_ds->ye[i] = dest_ds->yd[i] * sqrtf(dest_ds->ye[i] / nde);
        }
    }

    set_ds_treatement(dest_ds, "addition of 2 ds");
    dest_ds->ny = nx1;
    dest_ds->nx = nx1;
    inherit_from_ds_to_ds(dest_ds, src_ds1);
    dest_ds->m = src_ds1->m;
    set_ds_point_symbol(dest_ds, src_ds1->symb);
    return dest_ds;
}

int do_pick_points_having_same_x_on_add_all_ds(void)
{
    int i, j, k;
    pltreg *pr = NULL;
    O_p *op = NULL;
    O_p *opd = NULL;
    d_s *dsi1 = NULL;
    d_s *dsd = NULL;
    int  ery = 0, dsok = 1;
    float tmp, maxx = -FLT_MAX;
    static int x_sel = 0, xrep = 0, repeat = 0;
    static float xval = 0, xrange = 0.01, incx = 1.0;

    if (updating_menu_state != 0)
    {
        if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        if (!op || op->n_dat < 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("These routines perform arithmetical operations\n"
                             "on all the datasets of a plot, it takes one point in\n"
                             "each ds having a specific x value and plot (ds index,y)");

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi1) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    if (op->n_dat < 2)
    {
        return win_printf_OK("No enought data set!\n2 datasets are required!");
    }

    // check that all ds hve same x coordinates
    i = win_scanf("These routines perform arithmetical operations\n"
                  "on all the datasets of a plot, it takes one point in\n"
                  "each ds having a specific x value and plot (x,y)"
                  "Define the selection method:\n"
                  "\\oc  %R->point index\n\\oc  %r->first visible points\n"
                  "\\oc  %r->X value exact or \\oc  %r->X value +/- range\n"
                  "Define the x value of the point using:\n"
                  "\\di  %R->dataset index\n"
                  "\\di  %r->Numeric parameter from source occuring after = sign\n"
                  "Specify the X index or value wanted %8f \n"
                  "and eventually the range %8f\n"
                  "Repeat the operation %R->No\n"
                  "%r->yes by incrementing in x by:\n"
                  "Increment %8f\n"
                  , &x_sel, &xrep, &xval, &xrange, &repeat, &incx);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    //win_printf("x sel %d xrep %d ",x_sel,xrep);
    if (xrep == 1)
    {
        for (j = 0, dsok = 1, ery = 0, maxx = -FLT_MAX; j < op->n_dat && dsok > 0; j++)
        {
            dsi1 = op->dat[j];

            if (dsi1->ye)
            {
                ery = 1;
            }

            for (i = 0; i < dsi1->nx; i++)
            {
                maxx = (maxx < dsi1->xd[i]) ? dsi1->xd[i] : maxx;
            }

            if (dsi1->source == NULL)
            {
                dsok = 0;
                win_printf_OK("No source in data set %d!", j);
                break;
            }

            for (i = 0; i < (int) strlen(dsi1->source) && dsi1->source[i] != '='; i++);

            if (i >= (int) strlen(dsi1->source))
            {
                dsok = 0;
                win_printf_OK("bad source in data set %d!\n%s", j, dsi1->source);
                break;
            }
            else if (sscanf(dsi1->source + i + 1, "%f", &tmp) != 1)
            {
                dsok = 0;
                win_printf_OK("bad number source in data set %d!\nsouce: %s\n->%s"
                              , j, dsi1->source, dsi1->source + i + 1);
                break;
            }
        }
    }

    if (dsok == 0)
    {
        return win_printf_OK("cannot retrieve data from source !");
    }

    opd = create_and_attach_one_plot(pr, 16, 16, 0);

    if (opd == NULL)
    {
        return win_printf_OK("cannot create plot !");
    }

    dsd = (opd)->dat[0];
    dsd->nx = dsd->ny = 0;

    if (ery && (alloc_data_set_y_error(dsd) == NULL))
    {
        win_printf_OK("I can't create errors in y!");
    }

    for (k = -1; (repeat >= k) && (xval <= maxx); xval += incx, k = 1)
    {
        if (dsd == NULL)
        {
            dsd = create_and_attach_one_ds(opd, 16, 16, 0);
        }

        if (dsd == NULL)
        {
            return win_printf_OK("cannot create ds !");
        }

        dsd->nx = dsd->ny = 0;

        for (j = 0; j < op->n_dat; j++)
        {
            dsi1 = op->dat[j];
            tmp = j;

            if (xrep == 1)
            {
                for (i = 0; i < (int) strlen(dsi1->source) && dsi1->source[i] != '='; i++);

                sscanf(dsi1->source + i + 1, "%f", &tmp);
                //win_printf_OK(" number source in data set %d!\nsouce: %s\n->%g"
                //           ,j,dsi1->source,tmp);
            }

            if (x_sel == 0)
            {
                i = (int)xval;

                if (i < dsi1->nx && i >= 0)
                {
                    if (dsi1->ye != NULL)
                    {
                        add_new_point_with_y_error_to_ds(dsd, tmp, dsi1->yd[i], dsi1->ye[i]);
                    }
                    else
                    {
                        add_new_point_to_ds(dsd, tmp, dsi1->yd[i]);
                    }
                }
            }
            else if (x_sel == 1)
            {
                for (i = 0; i < dsi1->nx; i++)
                    if (dsi1->xd[i] < op->x_hi && dsi1->xd[i] >= op->x_lo
                            && dsi1->yd[i] < op->y_hi && dsi1->yd[i] >= op->y_lo)
                    {
                        break;
                    }

                if (i < dsi1->nx)
                {
                    if (dsi1->ye != NULL)
                    {
                        add_new_point_with_y_error_to_ds(dsd, tmp, dsi1->yd[i], dsi1->ye[i]);
                    }
                    else
                    {
                        add_new_point_to_ds(dsd, tmp, dsi1->yd[i]);
                    }
                }
            }
            else if (x_sel == 2)
            {
                for (i = 0; i < dsi1->nx; i++)
                    if (dsi1->xd[i] == xval)
                    {
                        break;
                    }

                if (i < dsi1->nx)
                {
                    if (dsi1->ye != NULL)
                    {
                        add_new_point_with_y_error_to_ds(dsd, tmp, dsi1->yd[i], dsi1->ye[i]);
                    }
                    else
                    {
                        add_new_point_to_ds(dsd, tmp, dsi1->yd[i]);
                    }
                }
            }
            else if (x_sel == 3)
            {
                for (i = 0; i < dsi1->nx; i++)
                    if (fabs(dsi1->xd[i] - xval) < fabs(xrange))
                    {
                        break;
                    }

                if (i < dsi1->nx)
                {
                    if (dsi1->ye != NULL)
                    {
                        add_new_point_with_y_error_to_ds(dsd, tmp, dsi1->yd[i], dsi1->ye[i]);
                    }
                    else
                    {
                        add_new_point_to_ds(dsd, tmp, dsi1->yd[i]);
                    }
                }
            }
        }

        set_ds_source(dsd, "points having same x = %g on all ds", xval);
        dsd = NULL;
    }

    uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opd, IS_Y_UNIT_SET);
    opd->need_to_refresh = 1;

    if (op->title != NULL)
    {
      set_plot_title(opd, "%s",op->title);
    }

    if (op->x_title != NULL)
    {
      set_plot_x_title(opd, "%s",op->x_title);
    }

    if (op->y_title != NULL)
    {
      set_plot_y_title(opd, "%s", op->y_title);
    }

    opd->filename = Transfer_filename(op->filename);
    opd->dir = Mystrdup(op->dir);
    return refresh_plot(pr, UNCHANGED);
}


int do_x_of_t_versus_index(void)
{
    int i;
    pltreg *pr = NULL;         // plot region
    O_p *op = NULL, *opn = NULL;      // initial plot
    d_s *dsi = NULL, *dsn = NULL;     // initial and destination datasets
    int     nx;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine extract x(point index)\n");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return (win_printf_OK("Could not find plot data"));
    }

    nx = dsi->nx;

    if (nx < 1)
    {
        return (win_printf_OK("not enough points in dataset!"));
    }

    opn = create_and_attach_one_plot(pr, nx, nx, 0);

    if (opn == NULL)
    {
        return win_printf_OK("cannot create plot !");
    }

    dsn = opn->dat[0];
    inherit_from_ds_to_ds(dsn, dsi);
    set_ds_treatement(dsn, "X (index) of ds %d", op->cur_dat);

    if (dsi->xe != NULL)
    {
        alloc_data_set_y_error(dsn);
    }
    if (dsi->xed != NULL)
    {
        alloc_data_set_y_down_error(dsn);
    }
    if (dsi->xbu != NULL)
    {
      alloc_data_set_y_box_up(dsn);
    }
    if (dsi->xbd != NULL)
    {
      alloc_data_set_y_box_dwn(dsn);
    }

    for (i = 0; i < nx; i++)
    {
        dsn->yd[i] = dsi->xd[i];
        dsn->xd[i] = i;

        if (dsi->xe != NULL && dsn->ye != NULL)
        {
            dsn->ye[i] = dsi->xe[i];
        }
        if (dsi->xed != NULL && dsn->yed != NULL)
        {
            dsn->yed[i] = dsi->xed[i];
        }
        if (dsi->xbu != NULL && dsn->ybu != NULL)
        {
            dsn->ybu[i] = dsi->xbu[i];
        }
        if (dsi->xbd != NULL && dsn->ybd != NULL)
        {
            dsn->ybd[i] = dsi->xbd[i];
        }

    }

    uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_Y_UNIT_SET);
    uns_op_2_op_by_type(op, IS_T_UNIT_SET, opn, IS_X_UNIT_SET);
    set_plot_title(opn, "X(t) %s", (op->title != NULL) ? op->title : "");

    if (op->x_title != NULL && strlen(op->x_title) > 0)
    {
      set_plot_y_title(opn, "%s", op->x_title);
    }

    set_plot_x_title(opn, "Index");
    return refresh_plot(pr, pr->n_op-1);
}


int do_x_of_x(void)
{
    int i;
    pltreg *pr = NULL;         // plot region
    O_p *op = NULL;      // initial plot
    d_s *dsi = NULL, *dsn = NULL;     // initial and destination datasets
    int     nx;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine extract x(point index)\n");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return (win_printf_OK("Could not find plot data"));
    }

    nx = dsi->nx;

    if (nx < 1)
    {
        return (win_printf_OK("not enough points in dataset!"));
    }

    dsn = create_and_attach_one_ds(op, nx, nx, 0);

    if (dsn == NULL)
    {
        return win_printf_OK("cannot create plot !");
    }

    inherit_from_ds_to_ds(dsn, dsi);
    set_ds_treatement(dsn, "X vs X  of ds %d", op->cur_dat);

    if (dsi->xe != NULL)
    {
        alloc_data_set_x_error(dsn);
        alloc_data_set_y_error(dsn);
    }
    if (dsi->xed != NULL)
    {
        alloc_data_set_x_down_error(dsn);
        alloc_data_set_y_down_error(dsn);
    }
    if (dsi->xbu != NULL)
    {
        alloc_data_set_x_box_up(dsn);
        alloc_data_set_y_box_up(dsn);
    }
    if (dsi->xbd != NULL)
    {
        alloc_data_set_x_box_dwn(dsn);
        alloc_data_set_y_box_dwn(dsn);
    }



    for (i = 0; i < nx; i++)
    {
        dsn->yd[i] = dsi->xd[i];
        dsn->xd[i] = dsi->xd[i];

        if (dsi->xe != NULL && dsn->ye != NULL)
        {
            dsn->ye[i] = dsi->xe[i];
        }
        if (dsi->xe != NULL && dsn->xe != NULL)
        {
            dsn->xe[i] = dsi->xe[i];
        }
        if (dsi->xed != NULL && dsn->yed != NULL)
        {
            dsn->yed[i] = dsi->xed[i];
        }
        if (dsi->xed != NULL && dsn->xed != NULL)
        {
            dsn->xed[i] = dsi->xed[i];
        }
        if (dsi->xbu != NULL && dsn->ybu != NULL)
        {
            dsn->ybu[i] = dsi->xbu[i];
        }
        if (dsi->xbd != NULL && dsn->xbd != NULL)
        {
            dsn->xbd[i] = dsi->xbd[i];
        }



    }
    return refresh_plot(pr, UNCHANGED);
}

int do_y_of_y(void)
{
    int i;
    pltreg *pr = NULL;         // plot region
    O_p *op = NULL;      // initial plot
    d_s *dsi = NULL, *dsn = NULL;     // initial and destination datasets
    int     nx;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine extract x(point index)\n");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return (win_printf_OK("Could not find plot data"));
    }

    nx = dsi->nx;

    if (nx < 1)
    {
        return (win_printf_OK("not enough points in dataset!"));
    }

    dsn = create_and_attach_one_ds(op, nx, nx, 0);

    if (dsn == NULL)
    {
        return win_printf_OK("cannot create plot !");
    }

    inherit_from_ds_to_ds(dsn, dsi);
    set_ds_treatement(dsn, "Y vs Y  of ds %d", op->cur_dat);

    if (dsi->ye != NULL)
    {
        alloc_data_set_x_error(dsn);
        alloc_data_set_y_error(dsn);
    }
    if (dsi->yed != NULL)
    {
        alloc_data_set_x_down_error(dsn);
        alloc_data_set_y_down_error(dsn);
    }
    if (dsi->ybu != NULL)
    {
        alloc_data_set_x_box_up(dsn);
        alloc_data_set_y_box_up(dsn);
    }
    if (dsi->ybd != NULL)
    {
        alloc_data_set_x_box_dwn(dsn);
        alloc_data_set_y_box_dwn(dsn);
    }



    for (i = 0; i < nx; i++)
    {
        dsn->yd[i] = dsi->yd[i];
        dsn->xd[i] = dsi->yd[i];

        if (dsi->ye != NULL && dsn->ye != NULL)
        {
            dsn->ye[i] = dsi->ye[i];
        }
        if (dsi->ye != NULL && dsn->xe != NULL)
        {
            dsn->xe[i] = dsi->ye[i];
        }
        if (dsi->yed != NULL && dsn->yed != NULL)
        {
            dsn->yed[i] = dsi->yed[i];
        }
        if (dsi->yed != NULL && dsn->xed != NULL)
        {
            dsn->xed[i] = dsi->yed[i];
        }
        if (dsi->ybu != NULL && dsn->ybu != NULL)
        {
            dsn->ybu[i] = dsi->ybu[i];
        }
        if (dsi->ybd != NULL && dsn->xbd != NULL)
        {
            dsn->xbd[i] = dsi->ybd[i];
        }

    }
    return refresh_plot(pr, UNCHANGED);
}


int do_x_of_t_versus_index_all(void)
{
    int i, j;
    pltreg *pr = NULL;         // plot region
    O_p *op = NULL, *opn = NULL;      // initial plot
    d_s *dsi = NULL, *dsn = NULL;     // initial and destination datasets
    int     nx;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine extract x(point index)\n");
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return (win_printf_OK("Could not find plot data"));
    }

    for (j = 0; j < op->n_dat; j++)
    {
        dsi = op->dat[j];
        nx = dsi->nx;

        if (nx < 1)
        {
            return (win_printf_OK("not enough points in dataset!"));
        }

        if (j == 0)
        {
            opn = create_and_attach_one_plot(pr, nx, nx, 0);

            if (opn == NULL)
            {
                return win_printf_OK("cannot create plot !");
            }

            dsn = opn->dat[0];
            uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_Y_UNIT_SET);
            uns_op_2_op_by_type(op, IS_T_UNIT_SET, opn, IS_X_UNIT_SET);
            set_plot_title(opn, "X(t) %s", (op->title != NULL) ? op->title : "");

            if (op->x_title != NULL && strlen(op->x_title) > 0)
            {
	      set_plot_y_title(opn, "%s", op->x_title);
            }

            set_plot_x_title(opn, "Index");
        }
        else
        {
            dsn = create_and_attach_one_ds(opn, nx, nx, 0);

            if (dsn == NULL)
            {
                return win_printf_OK("cannot create ds !");
            }
        }

        inherit_from_ds_to_ds(dsn, dsi);
        set_ds_treatement(dsn, "X (index) of ds %d", op->cur_dat);

	if (dsi->xe != NULL)
	  {
	    alloc_data_set_y_error(dsn);
	  }
	if (dsi->xed != NULL)
	  {
	    alloc_data_set_y_down_error(dsn);
	  }
	if (dsi->xbu != NULL)
	  {
	    alloc_data_set_y_box_up(dsn);
	  }
	if (dsi->xbd != NULL)
	  {
	    alloc_data_set_y_box_dwn(dsn);
	  }

        for (i = 0; i < nx; i++)
        {
            dsn->yd[i] = dsi->xd[i];
            dsn->xd[i] = i;
	    if (dsi->xe != NULL && dsn->ye != NULL)
	      {
		dsn->ye[i] = dsi->xe[i];
	      }
	    if (dsi->xed != NULL && dsn->yed != NULL)
	      {
		dsn->yed[i] = dsi->xed[i];
	      }
	    if (dsi->xbu != NULL && dsn->ybu != NULL)
	      {
		dsn->xbu[i] = dsi->xbu[i];
	      }
	    if (dsi->xbd != NULL && dsn->ybd != NULL)
	      {
		dsn->ybd[i] = dsi->xbd[i];
	      }

        }
    }

    return refresh_plot(pr, UNCHANGED);
}

int do_y_of_t_versus_index_all(void)
{
    int i, j;
    pltreg *pr = NULL;         // plot region
    O_p *op = NULL, *opn = NULL;      // initial plot
    d_s *dsi = NULL, *dsn = NULL;     // initial and destination datasets
    int     nx;
    static int mod = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine extract x(point index)\n");
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return (win_printf_OK("Could not find plot data"));
    }
    i = win_scanf("Do you want to:\n%R->create a new plot\n%r->modify the plot\n",&mod);
    if (i == WIN_CANCEL) return 0;

    for (j = 0; j < op->n_dat; j++)
    {
        dsi = op->dat[j];
        nx = dsi->nx;

        if (nx < 1)
        {
            return (win_printf_OK("not enough points in dataset!"));
        }

	if (mod == 0)
	  {
	    if (j == 0)
	      {
		opn = create_and_attach_one_plot(pr, nx, nx, 0);

		if (opn == NULL)
		  {
		    return win_printf_OK("cannot create plot !");
		  }

		dsn = opn->dat[0];
		uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opn, IS_Y_UNIT_SET);
		uns_op_2_op_by_type(op, IS_T_UNIT_SET, opn, IS_X_UNIT_SET);
		set_plot_title(opn, "Y(t) %s", (op->title != NULL) ? op->title : "");

		if (op->x_title != NULL && strlen(op->x_title) > 0)
		  {
		    set_plot_y_title(opn, "%s", op->x_title);
		  }

		set_plot_x_title(opn, "Index");
	      }
	    else
	      {
		dsn = create_and_attach_one_ds(opn, nx, nx, 0);

		if (dsn == NULL)
		  {
		    return win_printf_OK("cannot create ds !");
		  }
	      }

	    inherit_from_ds_to_ds(dsn, dsi);
	  }
	else dsn = dsi;
        set_ds_treatement(dsn, "Y (index) of ds %d", op->cur_dat);

	if (dsi->ye != NULL)
	  {
	    alloc_data_set_y_error(dsn);
	  }
	if (dsi->yed != NULL)
	  {
	    alloc_data_set_y_down_error(dsn);
	  }
	if (dsi->ybu != NULL)
	  {
	    alloc_data_set_y_box_up(dsn);
	  }
	if (dsi->ybd != NULL)
	  {
	    alloc_data_set_y_box_dwn(dsn);
	  }


        for (i = 0; i < nx; i++)
        {
            dsn->yd[i] = dsi->yd[i];
            dsn->xd[i] = i;
	    if (dsi->ye != NULL && dsn->ye != NULL)
	      {
		dsn->ye[i] = dsi->ye[i];
	      }
	    if (dsi->yed != NULL && dsn->yed != NULL)
	      {
		dsn->yed[i] = dsi->yed[i];
	      }
	    if (dsi->ybu != NULL && dsn->ybu != NULL)
	      {
		dsn->ybu[i] = dsi->ybu[i];
	      }
	    if (dsi->ybd != NULL && dsn->ybd != NULL)
	      {
		dsn->ybd[i] = dsi->ybd[i];
	      }
        }
    }
    if (mod == 0) opn->filename = Transfer_filename(op->filename);
    return refresh_plot(pr, UNCHANGED);
}

int do_y_of_t_versus_index(void)
{
    int i;
    pltreg *pr = NULL;         // plot region
    O_p *op = NULL, *opn = NULL;      // initial plot
    d_s *dsi = NULL, *dsn = NULL;     // initial and destination datasets
    int     nx;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine extract x(point index)\n");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return (win_printf_OK("Could not find plot data"));
    }

    nx = dsi->nx;

    if (nx < 1)
    {
        return (win_printf_OK("not enough points in dataset!"));
    }

    opn = create_and_attach_one_plot(pr, nx, nx, 0);

    if (opn == NULL)
    {
        return win_printf_OK("cannot create plot !");
    }

    dsn = opn->dat[0];
    inherit_from_ds_to_ds(dsn, dsi);
    set_ds_treatement(dsn, "Y (index) of ds %d", op->cur_dat);

    if (dsi->ye != NULL)
    {
        alloc_data_set_y_error(dsn);
    }
    if (dsi->yed != NULL)
    {
        alloc_data_set_y_down_error(dsn);
    }
    if (dsi->ybu != NULL)
    {
      alloc_data_set_y_box_up(dsn);
    }
    if (dsi->ybd != NULL)
    {
      alloc_data_set_y_box_dwn(dsn);
    }


    for (i = 0; i < nx; i++)
    {
        dsn->yd[i] = dsi->yd[i];
        dsn->xd[i] = i;

        if (dsi->ye != NULL && dsn->ye != NULL)
        {
            dsn->ye[i] = dsi->ye[i];
        }
        if (dsi->yed != NULL && dsn->yed != NULL)
        {
            dsn->yed[i] = dsi->yed[i];
        }
        if (dsi->ybu != NULL && dsn->ybu != NULL)
        {
            dsn->ybu[i] = dsi->ybu[i];
        }
        if (dsi->ybd != NULL && dsn->ybd != NULL)
        {
            dsn->ybd[i] = dsi->ybd[i];
        }


    }

    uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opn, IS_Y_UNIT_SET);
    uns_op_2_op_by_type(op, IS_T_UNIT_SET, opn, IS_X_UNIT_SET);
    set_plot_title(opn, "Y(t) %s", (op->title != NULL) ? op->title : "");

    if (op->y_title != NULL && strlen(op->y_title) > 0)
    {
      set_plot_y_title(opn, "%s", op->y_title);
    }
    opn->filename = Transfer_filename(op->filename);
    set_plot_x_title(opn, "Index");
    return refresh_plot(pr, UNCHANGED);
}



int do_wrap_y_in_x_using_one_ds(void)
{
    int i, j;
    pltreg *pr = NULL;         // plot region
    O_p *op = NULL, *opn = NULL;      // initial plot
    d_s *dsi = NULL, *dsi2 = NULL, *dsn = NULL;     // initial and destination datasets
    //int     nx;
    float min, max, tmp;
    static int i_ds = 0;
    static float dx = -5, ax = 5;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine extract x(point index)\n");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi2) != 3)
    {
        return (win_printf_OK("Could not find plot data"));
    }

    i = win_scanf("This function recenter bursts on one ds using a\n"
                  "second ds as a time reference to average non periodic bursts\n"
                  "Specify the dataset to wrapp by its number %8d\n"
                  "Specify an offset before the synchronization ds %8f\n"
                  "Specify a trailing period after the burst ds %8f\n"
                  , &i_ds, &dx, &ax);

    if (i == WIN_CANCEL)
    {
        return OFF;
    }

    if (i_ds < 0 || i_ds >= op->n_dat)
    {
        return (win_printf_OK("dataset out of range!"));
    }

    dsi = op->dat[i_ds];
    opn = create_and_attach_one_plot(pr, 16, 16, 0);

    if (opn == NULL)
    {
        return win_printf_OK("cannot create plot !");
    }

    dsn = opn->dat[0];
    dsn->nx = dsn->ny;
    uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_X_UNIT_SET);
    uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opn, IS_Y_UNIT_SET);
    set_plot_title(opn, "Wrapped data %s", (op->title != NULL) ? op->title : "");
    opn->filename = Transfer_filename(op->filename);
    if (op->x_title != NULL && strlen(op->x_title) > 0)
    {
      set_plot_y_title(opn, "%s",op->x_title);
    }

    if (op->y_title != NULL && strlen(op->y_title) > 0)
    {
      set_plot_x_title(opn, "%s", op->y_title);
    }

    for (j = 0; j < dsi2->nx; j++)
    {
        if (dsi->xe != NULL)
        {
            alloc_data_set_x_error(dsn);
        }

        if (dsi->ye != NULL)
        {
            alloc_data_set_y_error(dsn);
        }

        min = dx + op->ax + op->dx * dsi2->xd[j];

        if (j < dsi2->nx)
        {
            max = ax + op->ax + op->dx * dsi2->xd[j + 1];
        }
        else
        {
            max = FLT_MAX;
        }

        if (dsn == NULL)
        {
            dsn = create_and_attach_one_ds(opn, 16, 16, 0);

            if (dsn == NULL)
            {
                return win_printf_OK("cannot create ds !");
            }

            dsn->nx = dsn->ny;
        }

        inherit_from_ds_to_ds(dsn, dsi);
        set_ds_treatement(dsn, "ds %d wrapped between %g and %g", i_ds, min, max);

        for (i = 0; i < dsi->nx; i++)
        {
            tmp = op->ax + op->dx * dsi->xd[i];

            if (tmp >= min && tmp < max)
            {
                if (dsi->xe != NULL && dsi->ye != NULL)
                {
                    add_new_point_with_xy_error_to_ds(dsn, dsi->xd[i] - dsi2->xd[j], dsi->xe[i], dsi->yd[i], dsi->ye[i]);
                }
                else if (dsi->xe != NULL)
                {
                    add_new_point_with_x_error_to_ds(dsn, dsi->xd[i] - dsi2->xd[j], dsi->xe[i], dsi->yd[i]);
                }
                else if (dsi->ye != NULL)
                {
                    add_new_point_with_y_error_to_ds(dsn, dsi->xd[i] - dsi2->xd[j], dsi->yd[i], dsi->ye[i]);
                }
                else
                {
                    add_new_point_to_ds(dsn, dsi->xd[i] - dsi2->xd[j], dsi->yd[i]);
                }
            }
        }

        dsn = NULL;
    }

    return refresh_plot(pr, UNCHANGED);
}

int do_average_all_ds(void)
{

  int i, j;
  pltreg *pr = NULL;
  O_p *op = NULL, *opd = NULL;
  d_s *dsi1 = NULL, *dsd =  NULL, *dsmean = NULL; // , *dsi2
   float xtemp = 0, ytemp = 0;

  if (updating_menu_state != 0)
  {
      if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
      {
          active_menu->flags |=  D_DISABLED;
      }
      else
      {
          active_menu->flags &= ~D_DISABLED;
      }

      if (!op || op->n_dat < 2)
      {
          active_menu->flags |=  D_DISABLED;
      }
      else
      {
          active_menu->flags &= ~D_DISABLED;
      }

      return D_O_K;
  }



  if (key[KEY_LSHIFT])
      return win_printf_OK("These routines perform arithmetical operations\n"
                           "on all the datasets of a plot, the selected ds\n"
                           "is the reference for x position a new plot is created");

  if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi1) != 3)
  {
      return win_printf_OK("cannot find data!");
  }

  if (op->n_dat < 2)
  {
      return (win_printf_OK("No enought data set!\n2 datasets are required!"));
  }

  opd = create_and_attach_one_plot(pr, op->dat[op->cur_dat]->nx,op->dat[op->cur_dat]->nx , 0);

  if (opd == NULL)
  {
      return win_printf_OK("cannot create plot !");
  }

  dsmean = (opd)->dat[0];
  dsmean->nx = dsmean-> ny =0;

  dsi1 = op->dat[0];

  for ( i = 0;i< dsi1->nx;i++)
  {
    xtemp = dsi1->xd[i];
    ytemp = 0;
    for (j = 0; j < op->n_dat;j++)
    {
        dsd = op->dat[j];
        if (i >= dsd->nx ) return(win_printf_OK("Datasets don't have the same length \n"));
        if (xtemp != dsd->xd[i] ) return(win_printf_OK("X do not agree between data set \n"));
        ytemp += dsd->yd[i]/op->n_dat;
    }
    add_new_point_to_ds(dsmean,xtemp,ytemp);
  }

  set_ds_treatement(dsmean, "average of %d ds", op->n_dat);

  if (op->title != NULL)
  {
    set_plot_title(opd, "%s", op->title);
  }

  if (op->x_title != NULL)
  {
      set_plot_x_title(opd, "%s", op->x_title);
  }

  if (op->y_title != NULL)
  {
      set_plot_y_title(opd, "%s", op->y_title);
  }

  opd->filename = Transfer_filename(op->filename);
  uns_op_2_op(opd, op);
  inherit_from_ds_to_ds(dsd, dsi1);
  dsd->m = dsi1->m;
  set_ds_point_symbol(dsd, dsi1->symb);
  opd->dir = Mystrdup(op->dir);
  pr->cur_op = pr->n_op - 1;
  pr->one_p = pr->o_p[pr->cur_op];
  return refresh_plot(pr, pr->n_op - 1);

}



int do_add_all_ds(void)
{
    int i, j, k;
    pltreg *pr = NULL;
    O_p *op = NULL, *opd = NULL;
    d_s *dsi1 = NULL, *dsd =  NULL, *dstmp = NULL; // , *dsi2
    int  nx1, nt, singlem, all_no_error = 0, all_error = 0; // , warning = 0, nd, nde, npx, ner
    float mavg;
    static float pt_error = 0.03;
    static int thres = 5, no_error = 1;
    char question[4096] = {0};

    if (updating_menu_state != 0)
    {
        if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        if (!op || op->n_dat < 2)
        {
            active_menu->flags |=  D_DISABLED;
        }
        else
        {
            active_menu->flags &= ~D_DISABLED;
        }

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("These routines perform arithmetical operations\n"
                             "on all the datasets of a plot, the selected ds\n"
                             "is the reference for x position a new plot is created");

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi1) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    if (op->n_dat < 2)
    {
        return (win_printf_OK("No enought data set!\n2 datasets are required!"));
    }

    // check that all ds hve same x coordinates
    dstmp = build_data_set(op->dat[0]->nx, op->dat[0]->nx);

    if (dstmp == NULL)
    {
        return win_printf_OK("cannot create temporary ds !");
    }

    dstmp->nx = dstmp->ny = 0;
    win_printf("bef scan all ds");

    for (j = nx1 = nt = 0, all_error = 1, all_no_error = 1; j < op->n_dat; j++)
    {
        dsi1 = op->dat[j];
        all_error = (dsi1->ye == NULL) ? 0 : all_error;
        all_no_error = (dsi1->ye != NULL) ? 0 : all_no_error;

        if (dsi1->nx > nx1)
        {
            nx1 = dsi1->nx;
        }

        nt += dsi1->nx;

        for (k = 0; k < dsi1->nx; k++)
        {
            for (i = 0; i < dstmp->nx; i++)
            {
                if (dsi1->xd[k] == dstmp->xd[i])
                {
                    dstmp->yd[i] += 1;
                    break;
                }
            }

            if (i >= dstmp->nx)
            {
                add_new_point_to_ds(dstmp, dsi1->xd[k], 1);
            }
        }
    }

    win_printf("bef sort ds");
    sort_ds_along_x(dstmp);
    win_printf("after sort ds");

    for (i = 0, singlem = 0, mavg = 0; i < dstmp->nx; i++)
    {
        singlem += (dstmp->yd[i] <= 1) ? 1 : 0;
        mavg += dstmp->yd[i];
    }

    if (dstmp->nx > 0)
    {
        mavg /= dstmp->nx;
    }

    snprintf(question, sizeof(question), "You are going to average %d datasets having at most %d points\n"
             "this routine averages points of different datasets having strickly the same X\n"
             "I have found %d points having a unique X value, otherwise points will be\n"
             "averaged by %g on average. You can choose to skip points with small averaging\n"
             "number, define this theshold value %%6d \n", op->n_dat, nx1, singlem, mavg);

    if (all_no_error == 0 && all_error == 0)
    {
        no_error = 1;
        snprintf(question + strlen(question), sizeof(question) - strlen(question),
                 "Your datasets are not all equivalents in terms of error:\n"
                 "some have error bars other don't! in this condition, averaging\n"
                 "will not set errors\n");
        i = win_scanf(question, &thres);

        if (i == WIN_CANCEL)
        {
            free_data_set(dstmp);
            return D_O_K;
        }
    }

    if (all_no_error)
    {
        snprintf(question + strlen(question), sizeof(question) - strlen(question),
                 "All your datasets heve no error bars, click here %%R to generate an error\n"
                 "on each point by computing the variance of the averaged points for that X\n"
                 "or here %%r no produce no error bars Or here %%r to generate an error by \n"
                 "dividing the point error %%6f by the square root of number of averaging\n");
        i = win_scanf(question, &thres, &no_error, &pt_error);

        if (i == WIN_CANCEL)
        {
            free_data_set(dstmp);
            return D_O_K;
        }
    }

    if (all_error)
    {
        snprintf(question + strlen(question), sizeof(question) - strlen(question),
                 "All your datasets heve an error bars, click here %%R to generate an error\n"
                 "on each point by computing the geometrical average of point errors for that X\n"
                 "or here %%r no produce no error bars\n");
        i = win_scanf(question, &thres, &no_error);

        if (i == WIN_CANCEL)
        {
            free_data_set(dstmp);
            return D_O_K;
        }
    }

    /*


    for (j = npx = 0; npx == 0 && j < op->n_dat; j++)
      {
    dsi2 = op->dat[j];
    for (k = 0; k < dsi2->nx; k++)
      {
        for (i = 0; (i < dsi1->nx) && (dsi1->xd[i] != dsi2->xd[k]); i++);
        npx = (i == dsi1->nx) ? 1 : npx;
        if (npx && warning != WIN_CANCEL )
          warning = win_printf("points %d x -> %g of ds %d has no correspondant",k,dsi2->xd[k],j);
      }
      }
    if (npx != 0)   return win_printf_OK("your datasets have differents x values\n"
                       "cannot process them\n");

    for (j = 0, ner = 0; j < op->n_dat; j++)
      ner += (op->dat[j]->ye != NULL) ? 1 : 0;

    */

    for (i = nx1 = 0; i < dstmp->nx; i++)
    {
        nx1 += (dstmp->yd[i] > thres) ? 1 : 0;
    }

    if (nx1 <= 0)
    {
        free_data_set(dstmp);
        return win_printf_OK("No points have averaging greater than %d !",thres);
    }

    opd = create_and_attach_one_plot(pr, nx1, nx1, 0);

    if (opd == NULL)
    {
        return win_printf_OK("cannot create plot !");
    }

    dsd = (opd)->dat[0];

    if (no_error != 1)
    {
        if ((alloc_data_set_y_error(dsd) == NULL) || (dsd->ye == NULL))
        {
            win_printf_OK("I can't create errors 4!");
        }
    }

    for (i = 0, j = 0; i < dstmp->nx && j < dsd->nx; i++)
        if (dstmp->yd[i] > thres)
        {
            dsd->xd[j++] = dstmp->xd[i];
        }

    for (j = 0; j < op->n_dat; j++)
    {
        dsi1 = op->dat[j]; // we add on all ds

        for (k = 0; k < dsi1->nx; k++)
        {
            for (i = 0; i < dsd->nx; i++)
            {
                if (dsi1->xd[k] == dsd->xd[i]) // points with same X
                {
                    dsd->yd[i] += dsi1->yd[k];
                }
            }
        }
    }

    for (i = 0, j = 0; i < dstmp->nx && j < dsd->nx; i++) // we normalize
        if (dstmp->yd[i] > thres)
        {
            dsd->yd[j++] /= (dstmp->yd[i] > 0) ? dstmp->yd[i] : 1;
        }

    if (no_error == 0)
    {
        // we compute error
        if (all_error && dsd->ye != NULL)
        {
            for (j = 0; j < op->n_dat; j++)
            {
                dsi1 = op->dat[j]; // we add on all ds

                for (k = 0; k < dsi1->nx && dsi1->ye != NULL; k++)
                {
                    for (i = 0; i < dsd->nx; i++)
                    {
                        if (dsi1->xd[k] == dsd->xd[i]) // points with same X
                        {
                            dsd->ye[i] += dsi1->ye[k] * dsi1->ye[k];
                        }
                    }
                }
            }

            for (i = 0, j = 0; i < dstmp->nx && j < dsd->nx; i++) // we normalize
            {
                if (dstmp->yd[i] > thres)
                {
                    dsd->ye[j] = sqrt(dsd->ye[j]); // to be checked
                    dsd->ye[j] /= (dstmp->yd[i] > 0) ? dstmp->yd[i] : 1;
                    j++;
                }
            }
        }

        if (all_no_error)
        {
            for (j = 0; j < op->n_dat; j++)
            {
                dsi1 = op->dat[j]; // we add on all ds

                for (k = 0; k < dsi1->nx; k++)
                {
                    for (i = 0; i < dsd->nx; i++)
                    {
                        if (dsi1->xd[k] == dsd->xd[i]) // points with same X
                        {
                            dsd->ye[i] += (dsi1->yd[k] - dsd->yd[i]) * (dsi1->yd[k] - dsd->yd[i]);
                        }
                    }
                }
            }

            for (i = 0, j = 0; i < dstmp->nx && j < dsd->nx; i++) // we normalize
            {
                if (dstmp->yd[i] > thres)
                {
                    dsd->ye[j] /= (dstmp->yd[i] > 1) ? (dstmp->yd[i] - 1) : 1;
                    dsd->ye[j] = sqrt(dsd->ye[j]); // to be checked
                    j++;
                }
            }
        }
    }

    if (no_error == 2)
    {
        // we compute error
        if (all_no_error)
        {
            for (i = 0, j = 0; i < dstmp->nx && j < dsd->nx; i++) // we normalize
            {
                if (dstmp->yd[i] > thres)
                {
                    dsd->ye[j] = pt_error;
                    dsd->ye[j] /= (dstmp->yd[i] > 1) ? sqrt(dstmp->yd[i] - 1) : 1;
                    j++;
                }
            }
        }
    }

    set_ds_treatement(dsd, "addition of %d ds", op->n_dat);

    if (op->title != NULL)
    {
      set_plot_title(opd, "%s", op->title);
    }

    if (op->x_title != NULL)
    {
        set_plot_x_title(opd, "%s", op->x_title);
    }

    if (op->y_title != NULL)
    {
        set_plot_y_title(opd, "%s", op->y_title);
    }

    opd->filename = Transfer_filename(op->filename);
    uns_op_2_op(opd, op);
    inherit_from_ds_to_ds(dsd, dsi1);
    dsd->m = dsi1->m;
    set_ds_point_symbol(dsd, dsi1->symb);
    opd->dir = Mystrdup(op->dir);
    return refresh_plot(pr, UNCHANGED);
}

int rescale_ds_according_lo_hi(void)
{
    int i, j;
    pltreg  *pr = NULL;
    O_p     *op = NULL;
    d_s     *dsi1 = NULL, *dss = NULL;
    int  index, n_ds_start, n_ds_end,  ds_in;
    static float alo =  0, ahi = 1;
    static int do_all = 0, ds_keep = 0;
    float drx, tmplo, tmphi;
    char message[2048] = {0};

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Rescale datasets according\n"
                             "to the Y boundaries");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi1) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    if (dsi1 == NULL)
    {
        return (win_printf_OK("can't find data set"));
    }

    if ((op->y_hi - op->y_lo) <= 0)
    {
        return win_printf_OK("The Y boundaries of your plot are not set properly!");
    }

    //index = active_menu->flags & 0xFFFFFF00;
    index = RETRIEVE_MENU_INDEX;
    //nx1 = dsi1->nx;

    if (index == X_AXIS)
    {
        ds_in = n_ds_start = n_ds_end = op->cur_dat;
        snprintf(message, sizeof(message), "Transformation along X according to plot specified boundaries:\n"
                 "new X =  (old X - %g) \\times [ {\\it \\frac{X_{max} - X_{min}}{(%g - %g}}]\n\n"
                 "Specify the value that the maximum of the display will take : X_{max} = %%8f\n"
                 "Specify the value that the minimum of the display will take : X_{min} = %%8f\n"
                 "You can either create a new data set %%R or replace existing one %%r\n"
                 "You can perform this transformation simultaneously on several\n"
                 "data sets.\nStarting at data set %%6d\n"
                 "ending at data set %%6d (included) or %%b->do on all ds\n", op->dx * op->x_lo, op->dx * op->x_hi, op->dx * op->x_lo);
        i = win_scanf(message, &ahi, &alo, &ds_keep, &n_ds_start, &n_ds_end, &do_all);

        if (i == WIN_CANCEL)
        {
            return OFF;    // index = 0;
        }

        if (do_all)
        {
            n_ds_start = 0;
            n_ds_end = op->n_dat - 1;
        }

        if (n_ds_end < n_ds_start || n_ds_start < 0 || n_ds_end >= op->n_dat)
        {
            return win_printf_OK("data set range %d to %d out of range!", n_ds_start, n_ds_end);
        }

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
            dsi1 = op->dat[j];

            if (ds_keep == 0)
            {
                dss = duplicate_data_set(dsi1, NULL);

                if (dss != NULL)
                {
                    inherit_from_ds_to_ds(dss, dsi1);
                    add_one_plot_data(op, IS_DATA_SET, (void *)dss);
                }
            }
            else
            {
                dss = op->dat[j];
            }

            if (dss == NULL)
            {
                return (win_printf_OK("can't create data set"));
            }

            tmplo = (op->dx != 0) ? alo / op->dx : alo;
            tmphi = (op->dx != 0) ? ahi / op->dx : ahi;
            drx = (tmphi - tmplo) / (op->x_hi - op->x_lo);

            for (i = 0; i < dss->nx ; i++)
            {
                dss->xd[i] = tmplo + drx * (dsi1->xd[i] - op->x_lo);

                if (dss->xe)
                {
                    dss->xe[i] = drx * dsi1->xe[i];
                }
            }

            set_ds_treatement(dss, "X axis rescaled: multiplied by %g, and offseted by %g", drx, tmplo - drx * op->x_lo);
        }

        op->cur_dat = ds_in;
        dss = NULL; // dataset already added
        op->x_lo = tmplo;
        op->x_hi = tmphi;
        op->need_to_refresh = 1;
    }
    else if (index == Y_AXIS)
    {
        ds_in = n_ds_start = n_ds_end = op->cur_dat;
        snprintf(message, sizeof(message), "Transformation along Y according to plot specified boundaries:\n"
                 "new Y =  (old Y - %g) \\times [ {\\it \\frac{Y_{max} - Y_{min}}{(%g - %g}}]\n\n"
                 "Specify the value that the maximum of the display will take : Y_{max} = %%8f\n"
                 "Specify the value that the minimum of the display will take : Y_{min} = %%8f\n"
                 "You can either create a new data set %%R or replace existing one %%r\n"
                 "You can perform this transformation simultaneously on several\n"
                 "data sets.\nStarting at data set %%6d\n"
                 "ending at data set %%6d (included) or %%b->do on all ds\n", op->dy * op->y_lo, op->dy * op->y_hi, op->dy * op->y_lo);
        i = win_scanf(message, &ahi, &alo, &ds_keep, &n_ds_start, &n_ds_end, &do_all);

        if (i == WIN_CANCEL)
        {
            return OFF;    // index = 0;
        }

        if (do_all)
        {
            n_ds_start = 0;
            n_ds_end = op->n_dat - 1;
        }

        if (n_ds_end < n_ds_start || n_ds_start < 0 || n_ds_end >= op->n_dat)
        {
            return win_printf_OK("data set range %d to %d out of range!", n_ds_start, n_ds_end);
        }

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
            dsi1 = op->dat[j];

            if (ds_keep == 0)
            {
                dss = duplicate_data_set(dsi1, NULL);

                if (dss != NULL)
                {
                    inherit_from_ds_to_ds(dss, dsi1);
                    add_one_plot_data(op, IS_DATA_SET, (void *)dss);
                }
            }
            else
            {
                dss = op->dat[j];
            }

            if (dss == NULL)
            {
                return (win_printf_OK("can't create data set"));
            }

            tmplo = (op->dy != 0) ? alo / op->dy : alo;
            tmphi = (op->dy != 0) ? ahi / op->dy : ahi;
            drx = (tmphi - tmplo) / (op->y_hi - op->y_lo);

            for (i = 0; i < dss->nx ; i++)
            {
                dss->yd[i] = tmplo + drx * (dsi1->yd[i] - op->y_lo);

                if (dss->ye)
                {
                    dss->ye[i] = drx * dsi1->ye[i];
                }
            }

            set_ds_treatement(dss, "Y axis rescaled: multiplied by %g, and offseted by %g", drx, tmplo - drx * op->y_lo);
        }

        op->cur_dat = ds_in;
        dss = NULL; // dataset already added
        op->y_lo = tmplo;
        op->y_hi = tmphi;
        op->need_to_refresh = 1;
    }

    if (dss != NULL)
    {
        add_one_plot_data(op, IS_DATA_SET, (void *)dss);
    }

    return refresh_plot(pr, UNCHANGED);
}


/********************************************************************/
/* simple operations on 1 dataset                                   */
/********************************************************************/
int opp_on_one_ds(void)
{
    int i, j;
    pltreg  *pr = NULL;
    O_p     *op = NULL;
//   float   tmp;
    d_s     *dsi1 = NULL, *dss = NULL;
    int  nx1, index, n_ds_start, n_ds_end,  ds_in;
    static float ax =  0, dx = 1, rx = 1;
    static float axn[8] = {0,0,0,0,0,0,0,0};
    static float dxn[8] = {1,1,1,1,1,1,1,1};
    static float rxn[8] = {1,1,1,1,1,1,1,1};
    static int do_all = 0, ds_keep = 0, treat_label = 0, naff = 0, iaff = 3;
    float drx, tmp, ftmp;
    float uax, udx, uay,udy; // x and y unit affine coeff

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Arithmetical operations on one dataset\n"
                             "a new dataset is created in the same plot");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi1) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    if (dsi1 == NULL)
    {
        return (win_printf_OK("can't find data set"));
    }

    uax = op->ax;
    udx = op->dx;
    uay = op->ay;
    udy = op->dy;
    //index = active_menu->flags & 0xFFFFFF00;
    index = RETRIEVE_MENU_INDEX;
    nx1 = dsi1->nx;

    if (index == X_AXIS)
    {
      int k, l;
      float mean = 0;
      char question[2048] = {0};
      for (k = l = 0; k < dsi1->nx; k++)
	{
	  if (dsi1->xd[k] < op->x_hi && dsi1->xd[k] >= op->x_lo &&
	      dsi1->yd[k] < op->y_hi && dsi1->yd[k] >= op->y_lo)
	    {
	      mean += dsi1->xd[k];
	      l++;
	    }
	}
      mean = (l > 0) ? mean/l : mean;
      ds_in = n_ds_start = n_ds_end = op->cur_dat;
      snprintf(question,sizeof(question),"Transformation affine along X:"
	       "new X = [ old X \\times {\\it \\frac{n}{d}}] + {\\it b}\n\n"
	       "%%R Remove mean of visible points %g\n"
	       "%%r Normalize ds by mean of visible points %g\n"
	       "%%r Invert along X: \n"
	       "-----------------------------------------------------------------------------\n",mean,mean);
      for (k = 0; k < naff && k < 7; k++)
	snprintf(question+strlen(question),sizeof(question)-strlen(question),
		 "%%r set %d offset %g multiplier {\\it n} = %g divisor {\\it d} = %g\n"
		 ,k,axn[k],dxn[k],rxn[k]);

      snprintf(question+strlen(question),sizeof(question)-strlen(question),
	       "%%r new set %d offset {\\it b} = %%5f multiplier {\\it n} = %%5f divisor {\\it d} %%5f\n"
	       "-----------------------------------------------------------------------------\n"
	       "You can either create a new data set %%R or replace existing one %%r\n"
	       "You can perform this transformation simultaneously on several data sets.\n"
	       "Starting at data set %%4d ending at %%4d (included) or %%b->do on all ds\n"
	       "Apply to labels %%b\n"
	       ,naff);
      if (rx == 0) rx = 1;
        i = win_scanf(question, &iaff, &ax, &dx, &rx, &ds_keep, &n_ds_start, &n_ds_end, &do_all, &treat_label);

        if (i == WIN_CANCEL)
        {
            return OFF;    // index = 0;
        }

	if (iaff == 0)
	  {
	    ax = -mean;
	    dx = rx = 1;
	  }
	else if (iaff == 1)
	  {
	    ax = 0;
	    dx = 1;
	    rx = mean;
	  }
	else if (iaff == 2)
	  {
	    ax = 0;
	    dx = -1;
	    rx = 1;
	  }
	else if (iaff == naff + 3)
	  {
	    if (rx == 0)
	      {
		rx = 1;
		return (win_printf_OK("can't divide by 0!"));
	      }
	    for (k = 7; k > 0; k--)
	      {
		axn[k] = axn[k-1];
		dxn[k] = dxn[k-1];
		rxn[k] = rxn[k-1];
	      }
	    axn[0] = ax;
	    dxn[0] = dx;
	    rxn[0] = rx;
	    naff = (naff < 7) ? naff + 1: 7;
	    iaff = 3;
	  }
	else if (iaff >= 3 && iaff < naff + 3)
	  {
	    ax = axn[iaff-3];
	    dx = dxn[iaff-3];
	    rx = rxn[iaff-3];
	    for (k = iaff-3; k > 0; k--)
	      {
		axn[k] = axn[k-1];
		dxn[k] = dxn[k-1];
		rxn[k] = rxn[k-1];
	      }
	    axn[0] = ax;
	    dxn[0] = dx;
	    rxn[0] = rx;
	    iaff = 3;
	  }
        if (rx == 0)
        {
	    rx = 1;
            return (win_printf_OK("can't divide by 0!"));
        }

        if (do_all)
        {
            n_ds_start = 0;
            n_ds_end = op->n_dat - 1;
        }

        if (n_ds_end < n_ds_start || n_ds_start < 0 || n_ds_end >= op->n_dat)
        {
            return win_printf_OK("data set range %d to %d out of range!", n_ds_start, n_ds_end);
        }

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
            dsi1 = op->dat[j];

            if (ds_keep == 0)
            {
                dss = duplicate_data_set(dsi1, NULL);

                if (dss != NULL)
                {
                    inherit_from_ds_to_ds(dss, dsi1);
                    add_one_plot_data(op, IS_DATA_SET, (void *)dss);
                }
            }
            else
            {
                dss = op->dat[j];
            }

            if (dss == NULL)
            {
                return (win_printf_OK("can't create data set"));
            }

            for (i = 0, drx = dx / rx, tmp = (udx != 0) ? ax / udx : ax; i < dss->nx ; i++)
            {
              if (udx != 0)
	      {
		ftmp = dsi1->xd[i];
	        dss->xd[i] = ((drx-1)*uax/udx) + tmp;
		dss->xd[i] += drx * ftmp;
	      }
	      else   dss->xd[i] =  tmp + drx * dsi1->xd[i];
	      if (dss->xe)
                {
		  dss->xe[i] = drx * dsi1->xe[i];
                }
	      if (dss->xed)
                {
		  dss->xed[i] = drx * dsi1->xed[i];
                }
	      if (dss->xbd)
                {
		  dss->xbd[i] = drx * dsi1->xbd[i];
                }
	      if (dss->xbu)
                {
		  dss->xbu[i] = drx * dsi1->xbu[i];
                }

            }
            update_ds_treatement(dss, "X axis multiplied by %g, divided by %g "
				 " and offseted by %g with unit %s (ax = %g, dx = %g)"
				 ,dx, rx, ax, (op->xu[op->c_xu]->name) ? op->xu[op->c_xu]->name : "raw",uax,udx);

        }
        for (j = 0; treat_label > 0 && j < op->n_lab; j++)
        {
	  if (udx != 0)
	    {
	      ftmp = op->lab[j]->xla;
	      op->lab[j]->xla = ((drx-1)*uax/udx) + tmp;
	      op->lab[j]->xla += drx * ftmp;
	    }
	  else  op->lab[j]->xla = tmp + drx * op->lab[j]->xla;
        }

        op->cur_dat = ds_in;
        dss = NULL; // dataset already added
        op->need_to_refresh = 1;

# ifdef OLD

        ds_in = n_ds_start = n_ds_end = op->cur_dat;
        i = win_scanf("Transformation along X:"
                      "new X = [ old X \\times {\\it \\frac{n}{d}}] + {\\it b}\n\n"
                      "offset {\\it b} = %8f \n"
                      "multiplier {\\it n} = %8f divisor {\\it d} %8f\n"
                      "You can either create a new data set %R or replace existing one %r\n"
                      "You can perform this transformation simultaneously on several\n"
                      "data sets.\nStarting at data set %6d\n"
                      "ending at data set %6d (included) or %b->do on all ds\n"
                      "Apply to labels %b\n"
                      , &ax, &dx, &rx, &ds_keep, &n_ds_start, &n_ds_end, &do_all, &treat_label);

        if (i == WIN_CANCEL)
        {
            return OFF;    // index = 0;
        }

        if (rx == 0)
        {
            return (win_printf_OK("can't divide by 0!"));
        }

        if (do_all)
        {
            n_ds_start = 0;
            n_ds_end = op->n_dat - 1;
        }

        if (n_ds_end < n_ds_start || n_ds_start < 0 || n_ds_end >= op->n_dat)
        {
            return win_printf_OK("data set range %d to %d out of range!", n_ds_start, n_ds_end);
        }

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
            dsi1 = op->dat[j];

            if (ds_keep == 0)
            {
                dss = duplicate_data_set(dsi1, NULL);

                if (dss != NULL)
                {
                    inherit_from_ds_to_ds(dss, dsi1);
                    add_one_plot_data(op, IS_DATA_SET, (void *)dss);
                }
            }
            else
            {
                dss = op->dat[j];
            }

            if (dss == NULL)
            {
                return (win_printf_OK("can't create data set"));
            }

            for (i = 0, drx = dx / rx, tmp = (udx != 0) ? ax / udx : ax; i < dss->nx ; i++)
            {
	      if (udx != 0)
	      {
	        dss->xd[i] = (drx-1)*uax/udx + tmp;
		dss->xd[i] += drx * dsi1->xd[i];
	      }
	      else    dss->xd[i] =  tmp + drx * dsi1->xd[i];

                if (dss->xe)
                {
                    dss->xe[i] = drx * dsi1->xe[i];
                }
                if (dss->xed)
                {
                    dss->xed[i] = drx * dsi1->xed[i];
                }
                if (dss->xbd)
                {
                    dss->xbd[i] = drx * dsi1->xbd[i];
                }
                if (dss->xbu)
                {
                    dss->xbu[i] = drx * dsi1->xbu[i];
                }

            }

            update_ds_treatement(dss, "X axis multiplied by %g, divided by %g "
				 " and offseted by %g with unit %s (ax = %g, dx = %g)"
				 ,dx, rx, ax, (op->xu[op->c_xu]->name) ? op->xu[op->c_xu]->name : "raw",uax,udx);
        }

        for (j = 0; treat_label > 0 && j < op->n_lab; j++)
        {
	    if (udx != 0)
	      {
	        op->lab[j]->xla = (drx-1)*uax/udx + tmp;
		op->lab[j]->xla += drx * op->lab[j]->xla;
	      }
	    else op->lab[j]->xla = tmp + drx * op->lab[j]->xla;
        }

        op->cur_dat = ds_in;
        dss = NULL; // dataset already added
        op->need_to_refresh = 1;
# endif
    }
    else if (index == Y_AXIS)
    {
       int k, l;
      float mean = 0;
      char question[2048] = {0};
      for (k = l = 0; k < dsi1->nx; k++)
	{
	  if (dsi1->xd[k] < op->x_hi && dsi1->xd[k] >= op->x_lo &&
	      dsi1->yd[k] < op->y_hi && dsi1->yd[k] >= op->y_lo)
	    {
	      mean += dsi1->yd[k];
	      l++;
	    }
	}
      mean = (l > 0) ? mean/l : mean;
      ds_in = n_ds_start = n_ds_end = op->cur_dat;
      snprintf(question,sizeof(question),"Transformation affine along Y:"
	       "new Y = [ old Y \\times {\\it \\frac{n}{d}}] + {\\it b}\n\n"
	       "%%R Remove mean of visible points %g\n"
	       "%%r Normalize ds by mean of visible points %g\n"
	       "%%r Invert along Y: \n"
	       "-----------------------------------------------------------------------------\n",mean,mean);
      for (k = 0; k < naff && k < 7; k++)
	snprintf(question+strlen(question),sizeof(question)-strlen(question),
		 "%%r set %d offset %g multiplier {\\it n} = %g divisor {\\it d} = %g\n"
		 ,k,axn[k],dxn[k],rxn[k]);

      snprintf(question+strlen(question),sizeof(question)-strlen(question),
	       "%%r new set %d offset {\\it b} = %%5f multiplier {\\it n} = %%5f divisor {\\it d} %%5f\n"
	       "-----------------------------------------------------------------------------\n"
	       "You can either create a new data set %%R or replace existing one %%r\n"
	       "You can perform this transformation simultaneously on several data sets.\n"
	       "Starting at data set %%4d ending at %%4d (included) or %%b->do on all ds\n"
	       "Apply to labels %%b\n"
	       ,naff);
      if (rx == 0) rx = 1;
        i = win_scanf(question, &iaff, &ax, &dx, &rx, &ds_keep, &n_ds_start, &n_ds_end, &do_all, &treat_label);

        if (i == WIN_CANCEL)
        {
            return OFF;    // index = 0;
        }

	if (iaff == 0)
	  {
	    ax = -mean;
	    dx = rx = 1;
	  }
	else if (iaff == 1)
	  {
	    ax = 0;
	    dx = 1;
	    rx = mean;
	  }
	else if (iaff == 2)
	  {
	    ax = 0;
	    dx = -1;
	    rx = 1;
	  }
	else if (iaff == naff + 3)
	  {
	    if (rx == 0)
	      {
		rx = 1;
		return (win_printf_OK("can't divide by 0!"));
	      }
	    for (k = 7; k > 0; k--)
	      {
		axn[k] = axn[k-1];
		dxn[k] = dxn[k-1];
		rxn[k] = rxn[k-1];
	      }
	    axn[0] = ax;
	    dxn[0] = dx;
	    rxn[0] = rx;
	    naff = (naff < 7) ? naff + 1: 7;
	    iaff = 3;
	  }
	else if (iaff >= 3 && iaff < naff + 3)
	  {
	    ax = axn[iaff-3];
	    dx = dxn[iaff-3];
	    rx = rxn[iaff-3];
	    for (k = iaff-3; k > 0; k--)
	      {
		axn[k] = axn[k-1];
		dxn[k] = dxn[k-1];
		rxn[k] = rxn[k-1];
	      }
	    axn[0] = ax;
	    dxn[0] = dx;
	    rxn[0] = rx;
	    iaff = 3;
	  }
        if (rx == 0)
        {
	    rx = 1;
            return (win_printf_OK("can't divide by 0!"));
        }

        if (do_all)
        {
            n_ds_start = 0;
            n_ds_end = op->n_dat - 1;
        }

        if (n_ds_end < n_ds_start || n_ds_start < 0 || n_ds_end >= op->n_dat)
        {
            return win_printf_OK("data set range %d to %d out of range!", n_ds_start, n_ds_end);
        }

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
            dsi1 = op->dat[j];

            if (ds_keep == 0)
            {
                dss = duplicate_data_set(dsi1, NULL);

                if (dss != NULL)
                {
                    inherit_from_ds_to_ds(dss, dsi1);
                    add_one_plot_data(op, IS_DATA_SET, (void *)dss);
                }
            }
            else
            {
                dss = op->dat[j];
            }

            if (dss == NULL)
            {
                return (win_printf_OK("can't create data set"));
            }

            for (i = 0, drx = dx / rx, tmp = (udy != 0) ? ax / udy : ax ; i < dss->nx ; i++)
            {
  	        //y = uay + udy * y
	      //new y = ax + drx ( uay + udy * y)
	      //new y = ax + drx * uay + ydy * drx * y
	      //new y = drx * uay  + udy * ((ax/dy) + drx * y)
	      //new y = uay  + udy * ((uay*(drx-1))/udy + tmp + drx * y)
	      // ay = 0 => y =  dy * (tmp + drx * y)
	      if (udy != 0)
	      {
		ftmp = dsi1->yd[i];
	        dss->yd[i] = ((drx-1)*uay/udy) + tmp;
		dss->yd[i] += drx * ftmp;
	      }
	      else dss->yd[i] = tmp + drx * dsi1->yd[i];

	      if (dss->ye)
                {
		  dss->ye[i] = drx * dsi1->ye[i];
                }
	      if (dss->yed)
                {
		  dss->yed[i] = drx * dsi1->yed[i];
                }
	      if (dss->ybd)
                {
		  dss->ybd[i] = drx * dsi1->ybd[i];
                }
	      if (dss->ybu)
                {
		  dss->ybu[i] = drx * dsi1->ybu[i];
                }

            }
            update_ds_treatement(dss, "Y axis multiplied by %g, divided by %g "
				 " and offseted by %g with unit %s (ax = %g, dx = %g)"
				 ,dx, rx, ax, (op->yu[op->c_yu]->name) ? op->yu[op->c_yu]->name : "raw",uay,udy);

        }

        for (j = 0; treat_label > 0 && j < op->n_lab; j++)
        {
	      if (udy != 0)
	      {
		ftmp = op->lab[j]->yla;
	        op->lab[j]->yla = (drx-1)*uay/udy + tmp;
		op->lab[j]->yla += drx * ftmp;
	      }
	      else op->lab[j]->yla = tmp + drx * op->lab[j]->yla;
        }

        op->cur_dat = ds_in;
        dss = NULL; // dataset already added
        op->need_to_refresh = 1;
    }
    else if (index == DS_INVERSE)
    {
      dss = ds_1_over_y(dsi1);
    }
    else if (index == DS_SQRT)
    {
	dss = ds_squareroot(dsi1);
    }
    else if (index == DS_SQR)
    {
	dss = ds_square(dsi1);
    }
    else if (index == DS_ABS)
    {
        dss = ds_abs(dsi1);
    }
    else if (index == DS_LOG)
    {
       dss = ds_log(dsi1);
    }
    else if (index == DS_LOG10)
    {
      dss = ds_log10(dsi1);
    }
    else if (index == DS_LOG2)
    {
      dss = ds_log2(dsi1);
    }
    else if (index == DS_EXPO)
    {
      dss = ds_exp(dsi1);
    }
    else if (index == DS_EXPO_10)
    {
      dss = ds_exp10(dsi1);
    }
    else if (index == DS_EXPO_2)
    {
      dss = ds_exp2(dsi1);
    }
    else if (index == DS_ATAN)
    {
        dss = duplicate_data_set(dsi1, NULL);

        if (dss == NULL)
        {
            return (win_printf_OK("can't create data set"));
        }

        for (i = 0 ; i < nx1 ; i++)
        {
            dss->yd[i] = (float)atan(dsi1->yd[i]);
        }

        inherit_from_ds_to_ds(dss, dsi1);
        update_ds_treatement(dss, "arctan(ds)");
    }
    else if (index == DS_TANH)
    {
        dss = duplicate_data_set(dsi1, NULL);

        if (dss == NULL)
        {
            return (win_printf_OK("can't create data set"));
        }

        for (i = 0 ; i < nx1 ; i++)
        {
            dss->yd[i] = (float)tanh(dsi1->yd[i]);
        }

        inherit_from_ds_to_ds(dss, dsi1);
        update_ds_treatement(dss, "tanh(ds)");
    }
    else if (index == DS_ATAN2)
    {
        dss = duplicate_data_set(dsi1, NULL);

        if (dss == NULL)
        {
            return (win_printf_OK("can't create data set"));
        }

        for (i = 0 ; i < nx1 ; i++)
        {
            dss->xd[i] = i;
            dss->yd[i] = (float)atan2(dsi1->yd[i], dsi1->xd[i]);
        }

        inherit_from_ds_to_ds(dss, dsi1);
        update_ds_treatement(dss, "arctan2(ds)");
    }
    else if (index == DS_EXTRACT_X_T)
    {
        dss = build_data_set(nx1, nx1);

        if (dss == NULL)
        {
            return (win_printf_OK("can't create data set"));
        }

        dss->ny = nx1;
        dss->nx = nx1;

        if (dsi1->xe)
        {
            alloc_data_set_y_error(dss);
        }

        for (i = 0 ; i < nx1 ; i++)
        {
            dss->xd[i] = i;
            dss->yd[i] = dsi1->xd[i];

            if (dss->ye)
            {
                dss->ye[i] = dsi1->xe[i];
            }
        }

        inherit_from_ds_to_ds(dss, dsi1);
        update_ds_treatement(dss, "ds extract x serie");
    }
    else if (index == DS_EXTRACT_Y_T)
    {
        dss = build_data_set(nx1, nx1);

        if (dss == NULL)
        {
            return (win_printf_OK("can't create data set"));
        }

        dss->ny = nx1;
        dss->nx = nx1;

        if (dsi1->ye)
        {
            alloc_data_set_y_error(dss);
        }

        for (i = 0 ; i < nx1 ; i++)
        {
            dss->xd[i] = i;
            dss->yd[i] = dsi1->yd[i];

            if (dss->ye)
            {
                dss->ye[i] = dsi1->ye[i];
            }
        }

        inherit_from_ds_to_ds(dss, dsi1);
        update_ds_treatement(dss, "ds extract y serie");
    }
    /*    else if (index == DS_EXPAND)
        {
            dss = build_data_set(4*nx1, 4*nx1);
            if (dss == NULL)    return (win_printf_OK("can't create data set"));
            dss->ny = 4*nx1;
            dss->nx = 4*nx1;
            for (i=0 ; i< nx1 ; i++)    dss->yd[i] = dsi1->yd[i];
            tmp = (dsi1->xd[1] - dsi1->xd[0])/4;
            for (i=0 ; i< 4*nx1 ; i++)
                dss->xd[i] = dsi1->xd[i/4] + tmp * (i%4);
            if (fft_init(nx1)) return (win_printf_OK("Can't initialize fft"));
            if ( win_flag)      fftwindow1(nx1, dss->yd, 0.05);
            realtr1(nx1, dss->yd);
            fft(nx1, dss->yd, 1);
            realtr2(nx1, dss->yd, 1);
            if (fft_init(4*nx1)) return (win_printf_OK("Can't initialize fft"));
            dss->yd[nx1] = dss->yd[1];
            dss->yd[1] = 0;
            realtr2(4*nx1, dss->yd, -1);
            fft(4*nx1, dss->yd,- 1);
            realtr1(4*nx1, dss->yd);

            if ( win_flag)      defftwindow1(4*nx1, dss->yd, 0.05);
            dss->treatement = my_sprintf(dss->treatement,"expanded by 4");
        }       */
    else if (index == DS_UNWRAP)
    {
        dss = duplicate_data_set(dsi1, NULL);

        if (dss == NULL)
        {
            return (win_printf_OK(" "));
        }

        unwrap(dss->yd, dss->ny); // declared in xv_tools_lib.c
        inherit_from_ds_to_ds(dss, dsi1);
        update_ds_treatement(dss, "unwrap(ds)");
    }
    else
    {
        return D_O_K;
    }

    if (dss != NULL)
    {
        add_one_plot_data(op, IS_DATA_SET, (void *)dss);
    }

    return refresh_plot(pr, UNCHANGED);
}


float get_sigma_of_derivative_auto(d_s *dsi, float fraction)
{
    int i;
    float tmp, sigma2;
    int nf, ng;
    d_s *dst = NULL;

    if (dsi == NULL)
    {
        return -1;
    }

    nf = dsi->nx; /* this is the number of points in the data set */

    if (nf < 2)
    {
        return -2;
    }

    dst = build_data_set(nf - 1, nf - 1);

    if (dst == NULL)
    {
        return -3;
    }

    for (i = 0; i < nf - 1; i++)
    {
        tmp = dsi->yd[i + 1] - dsi->yd[i];
        tmp *= tmp;
        dst->xd[i] = tmp;
        dst->yd[i] = i;
    }

    sort_ds_along_x(dst);
    ng = (int)(fraction * dst->nx);

    if (ng < 1)
    {
        free_data_set(dst);
        return -4;
    }

    for (i = 0, sigma2 = 0; i < ng; i++)
    {
        sigma2 += dst->xd[i];
    }

    sigma2 /= ng;
    free_data_set(dst);
    return sqrt(sigma2);
}

d_s    *jumpfit_vc_2_exclude_edge(d_s *dsi, d_s *ds, int repeat, int max, int ex_size, float sigm, float p,
                                  double *chi2t)
{
    int i, j, k, l;
    int nf, width = 16;
    double y0, chi2, chi2n, tmp, totalchi = 0, tmp2, weight = 0;
    static double *collx = NULL, *colly = NULL;
    static int n_size = 0;

    if (dsi == NULL)
    {
        return NULL;
    }

    nf = dsi->nx; /* this is the number of points in the data set */
    if (nf < 1) return NULL;
    if (ds == NULL || ds->mx < nf || ds->my < nf)
    {
        ds = build_adjust_data_set(ds, nf, nf);
    }

    ds->nx = ds->ny = nf;

    if (ds == NULL)
    {
        return NULL;
    }

    if (nf > n_size)
    {
        if (collx)
        {
            free(collx);
        }

        if (colly)
        {
            free(colly);
        }

        collx = (double *) calloc(nf, sizeof(double));
        colly = (double *) calloc(nf, sizeof(double));

        if (collx == NULL || colly == NULL)
        {
            return NULL;
        }

        n_size = nf;
    }

    for (i = 0; i < nf; i++)
    {
        collx[i] =  colly[i] = 0;
        ds->xd[i] = 0;
    }

    for (l = 0; l < repeat; l++)
    {
        for (k = 0; k <= max; k++)
        {
            for (j = 0, width = 2 * k + 1; j < nf - width; j++)
            {
                for (i = 0, y0 = 0; i < width; i++)
                {
                    y0 += dsi->yd[j + i];
                }

                y0 /= width;

                for (i = 0, chi2 = 0; i < width; i++)
                {
                    tmp = y0 - dsi->yd[j + i];
                    chi2 += tmp * tmp;
                }

                if (width <= 1)
                {
                    chi2n = 4;
                }
                else
                {
                    chi2n = chi2 / (sigm * sigm);
                    chi2n += 2 * sqrt(width);
                    chi2n = chi2n / (width - 1);
                }

                if (chi2n < 1)
                {
                    chi2n = 1;    // we correct for fluctuation
                }

                if (chi2n != 0)
                {
                    weight = pow(chi2n, (-p));

                    if (weight > 1e300)
                    {
                        weight = 1e300;
                    }

                    if (weight < 1e-300)
                    {
                        weight = 1e-300;
                    }
                }
                else
                {
                    weight = 1;
                }

                colly[j] += weight * y0;
                collx[j] += weight;

                if (width > 1)
                {
                    colly[j + width - 1] += weight * y0;
                    collx[j + width - 1] += weight;
                }
            }
        }

        for (j = 0, tmp2 = 0; j < nf; j++)
        {
            if (collx[j] != 0)
            {
                colly[j] /= collx[j];
            }

            tmp2 += (dsi->yd[j] - colly[j]) * (dsi->yd[j] - colly[j]);
        }

        totalchi = sqrt(tmp2 / nf);
    }

    for (i = 0; i < nf; i++)
    {
        ds->xd[i] = 1;
    }

    sigm *= 4 * M_SQRT2;

    for (i = 0; i < nf - 1; i++)
    {
        collx[i] =  colly[i + 1] - colly[i];

        if (fabs(collx[i]) > sigm)
        {
            for (k = i - ex_size + 1; k <= i + ex_size; k++)
                if (k >= 0 && k < nf)
                {
                    ds->xd[k] = 0;
                }
        }
    }

    for (i = 0, k = 0; i < nf; i++)
    {
        if (ds->xd[i])
        {
            ds->yd[k] = colly[i];
            ds->xd[k++] = dsi->xd[i];
        }
    }

    ds->nx = ds->ny = k;

    if (chi2t)
    {
        *chi2t = totalchi;
    }

    inherit_from_ds_to_ds(ds, dsi);
    update_ds_treatement(ds, "Non-linear filter: repeat = %d, max = %d, ex_size = %d, sigma = %g, weight = %g "
                      , repeat, max, ex_size, sigm, p);
    return ds;
}




int do_non_linear_plateau_filtering(void)
{
    int i, k;
    pltreg *pr = NULL;
    O_p *op = NULL, *opd = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    int  n_dat, c_dat;
    static int all_ds = 0, new_plot = 0, repeat = 1, max = 64, ex_size = 1, sigm_type = 0;
    static float p = 13, sigmu = 0.003;
    float sigm;
    double chi2;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf("This routine creates a new data set containing\n"
                          "a non linear jump filter\n"
                          "of the of the selected data set");
    }

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)
    {
        return win_printf_OK("cannot find data");
    }

    i = win_scanf("Non linear filter expecting flat events."
                  "Weighting by  (\\chi^2/\\sigma^2)^{-p} using forward/backward algorithm.\n"
                  "Apply how many times? %8d. "
                  "Define the bounds on the # points used per fit w= (2m + 1):\n"
                  "maxixum size for averaging m = %8d  \n"
                  "You need to define the noise level by its \\sigma\n"
                  "%R=>Use a fix value specify below or %r=>Compute sigma from derivative of data\n"
                  "the sigma of noise= %8f; "
                  "the weighting power p= %8f\n"
                  "Exclude points with averaging smaller than %8d\n"
                  "Apply to all datasets %b place result in a new plot %b\n"
                  , &repeat, &max, &sigm_type, &sigmu, &p, &ex_size, &all_ds, &new_plot); /* don't forget the &*/

    if (i == WIN_CANCEL)
    {
        return 0;
    }

    if (new_plot)
    {
        i = op->n_dat;
        op->n_dat = 0;
        opd = duplicate_plot_data(op, NULL);
        op->n_dat = i;

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        add_data_to_pltreg(pr, IS_ONE_PLOT, (void *)opd);
        opd->need_to_refresh = 1;
    }
    else
    {
        opd = op;
    }

    //win_printf("bef start");
    n_dat = op->n_dat;
    c_dat = op->cur_dat;

    for (k = 0; k < n_dat; k++)
    {
        if ((all_ds == 0) && (k != c_dat))
        {
            continue;
        }

        dsi = op->dat[k];

        if (sigm_type)
        {
            sigm = get_sigma_of_derivative_auto(dsi, 0.8);
            //win_printf("sigma %g",sigm);
        }
        else
        {
            sigm = sigmu;
        }

        //win_printf("bef jump");
        dsd = jumpfit_vc_2_exclude_edge(dsi, NULL, repeat, max, ex_size, sigm, p, &chi2);

        if (dsd == NULL)
        {
            return win_printf("Cannot allocate plot");
        }

        if (add_one_plot_data(opd, IS_DATA_SET, (void *)dsd))
        {
            return win_printf_OK("cannot find data");
        }

        inherit_from_ds_to_ds(dsd, dsi);
    }

    return refresh_plot(pr, UNCHANGED);
}


d_s    *nl_fit_step_and_slope_vc_2_r_math(d_s *dsi,   // data set of data
				     d_s *ds, // data set to save filtrerd data compatible with dsi
				     d_s *dsdz,  // pointer to derivative data produced
				     int max, int min,  // size to perform averaging
				     int size_to_evaluate_noise,  // extend of data on which you will evaluate noise
				     float p,     // weigthing power
				     double *chi2t,  // chi2 returned by the function
				     float mul, //Multiplicative factor favoring long average (1.2 typically)
				     float noise_mul) // Multiplicative factor on noise (1.1 typical)
{
    printf("nl parameters p=%f mul=%f noise_mul=%f\n",p,mul,noise_mul);
    register int i, j, k;
    int nf, width = 16, i_sighf = 0, cx, nx0;
    float sigm, meany, my2;
    double y0, chi2, chi2n, tmp, totalchi = 0, tmp2, weight = 0, sx2, sxy, tmpx, tmpy, a, sx, sy;
    static double *collx = NULL, *colly = NULL, *collsx = NULL, *collsy = NULL;
    static double *collnx = NULL, *collny = NULL, *sigHF = NULL;
    static int n_size = 0, n_sigHF = 0;

    if (dsi == NULL) return NULL;

    nf = dsi->nx;	/* this is the number of points in the data set */
    if (ds == NULL || ds->mx < nf || ds->my < nf)
      ds = build_adjust_data_set(ds, nf, nf);
    ds->nx = ds->ny = nf;
    if (ds == NULL) return NULL;

    if (dsdz == NULL || dsdz->mx < nf || dsdz->my < nf)
    dsdz = NULL;

    if (nf > n_size)
      {
	if (collx) free(collx);
	if (colly) free(colly);
	if (collsx) free(collsx);
	if (collsy) free(collsy);
	if (collnx) free(collnx);
	if (collny) free(collny);
	collx = (double *) calloc (nf,sizeof(double));
	colly = (double *) calloc (nf,sizeof(double));
	collsx = (double *) calloc (nf,sizeof(double));
	collsy = (double *) calloc (nf,sizeof(double));
	collnx = (double *) calloc (nf,sizeof(double));
	collny = (double *) calloc (nf,sizeof(double));
	if (collx == NULL || colly == NULL) return NULL;
	if (collsx == NULL || collsy == NULL) return NULL;
	if (collnx == NULL || collny == NULL) return NULL;
	n_size = nf;
      }
    cx = 2*(nf - size_to_evaluate_noise);
    nx0 = cx/size_to_evaluate_noise;
    nx0 = (nx0 < 0) ? 0 : nx0;
    nx0 += (cx/size_to_evaluate_noise) ? 2 : 1;
    if (nx0 > n_sigHF)
      {
	sigHF = (double*)realloc(sigHF,nx0*sizeof(double));
	n_sigHF = nx0;
      }
    for (i = 0; i < nf; i++)
      {
	collx[i] =  colly[i] = collsx[i] =  collsy[i] = collnx[i] =  collny[i] = 0;
	ds->xd[i] = 0;
      }


    mean_y2_on_array(dsi->yd, dsi->nx, 0, &meany, &my2, 0);
    for (i_sighf = 0; i_sighf < nx0; i_sighf++)
      {
	j = (nx0 > 1) ? (cx * i_sighf)/(2 * (nx0-1)) : 1;
	get_sigma_of_derivative_of_partial_ds(dsi, j, j + size_to_evaluate_noise, 4 * sqrt(my2), sigHF+i_sighf);
      }



    for (k = min; k <= 2 * max + 1; k++)
      {
	for (j = 0, width = k; j < nf - width + 1; j++)
	  {  // we do step analyze
	    i_sighf = (cx > 0) ?((2 * (nx0-1))*j)/cx : 0;
	    i_sighf = (i_sighf < 0) ? 0 : i_sighf;
	    i_sighf = (i_sighf < n_sigHF) ? i_sighf : n_sigHF-1;
	    sigm = noise_mul*sigHF[i_sighf];
	    for (i = 0, y0 = 0; i < width; i++)
	      y0 += dsi->yd[j+i];
	    y0 /= width;
	    for (i = 0, chi2 = 0; i < width; i++)
	      {
		tmp = y0 - dsi->yd[j+i];
		chi2 += tmp * tmp;
	    }
	    if (width <= 1) chi2n = 4;
	    else
	      {
		chi2n = chi2 / (sigm*sigm);
		chi2n += 2*sqrt(width);
		chi2n = chi2n / (mul*(width-1));
	      }
	    if (chi2n < 1) chi2n = 1;    // we correct for fluctuation
	    if (chi2n != 0)
	      {
		weight = width * pow(chi2n,(-p));
		if (weight > 1e300) weight=1e300;
		if (weight < 1e-300) weight=1e-300;
	      }
	    else	  weight = width;
	    for (i = 0; i < width; i++)
	      {
		colly[j+i] += weight*y0;
		collx[j+i] += weight;
	      }
	    // we do slop analyze
	    if (width < 3) continue;
	    for (i = 0, sx = sy = sx2 = sxy = 0; i < width; i++)
	    {
	      tmpx = dsi->xd[j+i] - dsi->xd[j];
	      tmpy = dsi->yd[j+i] - dsi->yd[j];;
	      sx += tmpx;
	      sy += tmpy;
	      sx2 += tmpx * tmpx;
	      sxy += tmpx * tmpy;
	    }
	    y0 = (sx2 * width) - sx * sx;
	    if (y0 == 0) continue;
	    a = sxy * width - sx * sy;
	    a /= y0;
	    y0 = (sx2 * sy - sx * sxy) / y0;
	    for (i = 0, chi2 = 0; i < width; i++)
	      {
		tmp = (y0 + a * (dsi->xd[j+i] - dsi->xd[j])) - dsi->yd[j+i] + dsi->yd[j];
	      chi2 += tmp * tmp;
	      }
	    if (width <= 2) chi2n = 4;
	    else
	      {
		chi2n = chi2 / (sigm*sigm);
		chi2n += 2*sqrt(width);
		chi2n = chi2n /(mul*(width-2));
	      }
	    if (chi2n < 1) chi2n = 1;    // we correct for fluctuation
	    if (chi2n != 0)
	      {
		weight = width *pow(chi2n,(-p));
		if (weight > 1e300) weight=1e300;
		if (weight < 1e-300) weight=1e-300;
	      }
	    else	  weight = width;
	    for (i = 0; i < width; i++)
	      {
		tmp = dsi->yd[j] + (y0 + a * (dsi->xd[j+i] - dsi->xd[j]));
		collsy[j+i] += weight*tmp;
		collsx[j+i] += weight;
		collnx[j+i] += width*weight;
		collny[j+i] += a*weight;
	      }
	  }
      }
    for (j = 0, tmp2=0; j < nf; j++)
      {
	if (dsdz != NULL)
	  {
	    dsdz->yd[j] = (collx[j] > collsx[j]) ? 1 : 2;
	  dsdz->xd[j] = dsi->xd[j];
	  //dsdz->yd[j] = (collsx[j] > 0) ? collnx[j]/collsx[j] : 0;
	  dsdz->yd[j] = (collsx[j] > 0) ? collny[j]/collsx[j] : 0;
	  i_sighf = (cx > 0) ?((2 * (nx0-1))*j)/cx : 0;
	  i_sighf = (i_sighf < 0) ? 0 : i_sighf;
	  i_sighf = (i_sighf < n_sigHF) ? i_sighf : n_sigHF-1;
	  //dsdz->xd[j] = sigHF[i_sighf];
	  //dsdz->yd[j] = collsx[j];
	  //dsdz->xd[j] = collx[j];
	  }
	if ((collx[j] + collsx[j]) != 0)
	  {
	  colly[j] = (colly[j] + collsy[j])/(collx[j] + collsx[j]);
	  }
	tmp2 += (dsi->yd[j] - colly[j]) * (dsi->yd[j] - colly[j]);
      }
    totalchi = sqrt(tmp2/nf);
    for (i = 0; i < nf; i++)       ds->xd[i] = 1;
    //sigm *= 4*M_SQRT2;
    for (i = 0; i < nf-1; i++)
      {
	i_sighf = (cx > 0) ?((2 * (nx0-1))*i)/cx : 0;
	i_sighf = (i_sighf < 0) ? 0 : i_sighf;
	i_sighf = (i_sighf < n_sigHF) ? i_sighf : n_sigHF-1;
	sigm = noise_mul*sigHF[i_sighf];
	sigm *= 3;
	if (fabs(colly[i+1] - colly[i]) > sigm)
	  {  // we look for big jump and correct the inapropriate estimate
	    if (fabs(dsi->yd[i] - colly[i]) > sigm)
	    {
	      colly[i] = dsi->yd[i];
	      if (dsdz != NULL && i < dsdz->mx)
		{
		  if (i > 0) dsdz->yd[i-1] = colly[i] - colly[i-1];
		  dsdz->yd[i] = colly[i+1] - colly[i];
		}
	    }
	    else if (dsdz != NULL && i < dsdz->mx)
	      dsdz->yd[i] = colly[i+1] - colly[i];
	  }
      }

    for (i = 0, k = 0; i < nf; i++)
      {
	if (ds->xd[i])
	  {
	  ds->yd[k] = colly[i];
	  ds->xd[k++] = dsi->xd[i];
	  }
      }
    ds->nx = ds->ny = k;
    if (chi2t) *chi2t = totalchi;
    //inherit_from_ds_to_ds(ds, dsi);
    return ds;
}


int do_filter_nl_ex_slope_auto_r_math(void)
{
    O_p *op = NULL, *opnd = NULL;
    d_s *ds = NULL, *dsi = NULL, *dstype = NULL;
    pltreg *pr;
    int i, ds_nb = 0, i_ds, nd;
    double chi2;
    static int max = 32, dydx, dsmin = 32, newop = 0;
    static int min =1, n_size = 256, all = 0;
    static float p = 10, mul = 1.2, n_mul = 1.1;

    if(updating_menu_state != 0)	return D_O_K;

    if (key[KEY_LSHIFT])    return win_printf_OK("This routine creates a new data set containing\n"
					"a non linear filter explecting flat and sloopy phase\n"
					"the data set");
    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)
    {
        return win_printf_OK("cannot find data");
    }
    ds_nb = op->cur_dat;
    i = win_scanf("Non linear filter expecting flat and slope events."
		  "Weighting by  (\\chi^2/\\sigma^2)^{-p} using forward/backward algorithm.\n"
		  "Define the bounds on the # points used per fit w= (2m + 1):\n"
		  "max(m) = %4d min(m) m=%4d\n"
		  "the sigma of noise is compute automatically over %4d points\n"
		  "the weighting power p= %10f\n"
		  "Multiplicative factor favoring long average %4f\n"
		  "Multiplicative factor on noise %4f\n"
		  "%b Produce a derivative plot\n"
		  "Proceed %R->current dataset or %r->all\n"
		  "exclude ds smaller than %5d;\n"
		  "add neew dataset(s) to %R-> the current plot %r a new plot\n"
		  ,&max,&min,&n_size,&p,&mul,&n_mul,&dydx,&all,&dsmin,&newop); /* don't forget the &*/

    if (i == WIN_CANCEL)	return D_O_K;


    if (min > max)
      {
          return win_printf("YOU SCREWED UP: Min width > Max width!!!");
      }
    for (i_ds = 0, nd = op->n_dat; i_ds < nd; i_ds++)
        {
            if (all == 0 && i_ds != ds_nb) continue;
            dsi = op->dat[i_ds];
            if (dsi->nx < 1) continue;
            dstype = build_data_set(dsi->nx,dsi->nx);
            if (dstype == NULL) { return win_printf_OK("cannot create dataset"); }
            ds = nl_fit_step_and_slope_vc_2_r_math(dsi, NULL, dstype,  max, min, n_size, p, &chi2, mul, n_mul);
            if (ds == NULL)    { return win_printf_OK("cannot create plot !"); }
            if (newop == 1)
                {
                    if (opnd == NULL)
                        {
                            //opnd = create_one_empty_plot();
                            opnd = create_and_attach_one_empty_plot(pr, 0);
                            if (opnd == NULL) { return win_printf_OK("cannot create plot"); }
                            //add_data_to_pltreg(pr, IS_ONE_PLOT, (void*)opnd);
                            uns_op_2_op_by_type(op, IS_X_UNIT_SET, opnd, IS_X_UNIT_SET);
                            uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opnd, IS_Y_UNIT_SET);
                        }
                    if (add_one_plot_data(opnd, IS_DATA_SET, (void*)ds))
                        { return win_printf_OK("cannot find data"); }
                }
            else
                {
                    if (add_one_plot_data(op, IS_DATA_SET, (void*)ds))
                        { return win_printf_OK("cannot find data"); }
                }
            if (dydx)
                {
                    for(i = 0; op->dx > 0 && i < dstype->nx; i++)
                        dstype->yd[i] /= op->dx;
                    if (newop == 1)
                        {
                            if (add_one_plot_data(opnd, IS_DATA_SET, (void*)dstype))
                                { return win_printf_OK("cannot find data"); }
                        }
                    else
                        {
                            if (add_one_plot_data(op, IS_DATA_SET, (void*)dstype))
                                { return win_printf_OK("cannot find data"); }
                        }
                }
        }
    if (newop == 1) refresh_plot(pr, pr->n_op - 1);
    else refresh_plot(pr, UNCHANGED);
    return 0;
}


int slidding_histogramm_ds_same_op(void)
{
    int i, j, k;
    pltreg  *pr = NULL;
    O_p     *op = NULL, *opn = NULL, *opt = NULL;
    d_s     *dsi = NULL, *dsn = NULL, *dsp = NULL, *dst = NULL;
    int  maxi, mini, nxd, no_peak_yet, itmp, maxnx;
    float min, max, tmp, toff, maxn;
    static int fpeaks = 0;
    static float wsize = 10, dx = 1, intx = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("Build a plot with the x or y range of each dataset\n"
                             "versus index\n");

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    i = win_scanf("Slidding histogramm along y, move an integretaing window\n"
                  "of size %8f by steps of %8f\n%R->Nb. of points %r->integrate X\n"
		  "Find peaks %b\n"
		  , &wsize, &dx,&intx,&fpeaks);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    if (dx <= 0)
    {
        return win_printf_OK("dx must be > 0!");
    }

    for (mini = maxi = i = 0, min = max = dsi->yd[i] ; i < dsi->nx ; i++)
    {
        min = (dsi->yd[i] < min) ? dsi->yd[mini = i] : min;
        max = (dsi->yd[i] > max) ? dsi->yd[maxi = i] : max;
    }

    nxd = (int)(0.5 + (max - min) / dx);
    opn = create_and_attach_one_plot(pr, nxd, nxd, 0);
    if (opn == NULL)
    {
        return win_printf_OK("cannot create plot!");
    }

    dsn = opn->dat[0];
    opn->need_to_refresh = 1;

    if (fpeaks)
      {
	dsp = create_and_attach_one_ds(opn, 16, 16, 0);
	if (dsp == NULL)
	    return win_printf_OK("cannot create plot!");
	alloc_data_set_x_error(dsp);
	set_ds_dot_line(dsp);
	set_plot_symb(dsp, "\\oc");
	dsp->nx = dsp->ny = 0;
	opt = create_and_attach_one_plot(pr, 16, 16, 0);
	if (opt == NULL)
	  return win_printf_OK("cannot create plot!");
	dst = opt->dat[0];
	alloc_data_set_y_error(dst);
	set_ds_dot_line(dst);
	dst->nx = dst->ny = 0;
	set_plot_y_title(opt, "Avg of X");
	uns_op_2_op_by_type(op, IS_X_UNIT_SET, opt, IS_Y_UNIT_SET);
	uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opt, IS_X_UNIT_SET);
	set_plot_symb(dst, "\\di");
	set_ds_source(dst, "window_histo w %g dx %g", wsize, dx);
	set_plot_title(opt, "window histo of %s", (op->title != NULL) ? op->title : "");
	set_plot_x_title(opt, "value");
      }

    if (intx == 0)
      {
	for (i = 0, maxn = 0, maxnx = 0, no_peak_yet = 1; i < nxd; i++)
	  {
	    tmp = min + i * dx;
	    for (k = 0; k < dsi->nx; k++)
	      {
		if (fabs(tmp - dsi->yd[k]) < wsize / 2)
		  {
		    dsn->yd[i] += 1;
		    dsn->xd[i] += dsi->yd[k];
		  }
	      }
	    dsn->xd[i] /= (dsn->yd[i] > 0) ? dsn->yd[i] : 1;
	    if (fpeaks)
	      {
		if (dsn->yd[i] > dsn->yd[(i)?i-1:0])
		  {
		    if (no_peak_yet == 0)
		      {
			maxn = dsn->yd[i];
			maxnx = i;
		      }
		    no_peak_yet = 1;
		    if (i == 0 || dsn->yd[i] > maxn)
		      {
			maxn = dsn->yd[i];
			maxnx = i;
		      }
		  }
		if (dsn->yd[i] < maxn && no_peak_yet > 0)
		  {
		    k = (maxnx + i - 1)/2;
		    no_peak_yet = 0;
		    for (j = 0, itmp = 0, toff = 0; j < dsi->nx; j++)
		      {
			if (fabs(tmp - dsi->yd[j]) < wsize / 2)
			  {
			    itmp += 1;
			    toff += (dsi->yd[j] - dsn->xd[k])*(dsi->yd[j] - dsn->xd[k]);
			  }
		      }
		    toff /= (itmp > 1) ? itmp-1 : 1;
		    add_new_point_with_x_error_to_ds(dsp, dsn->xd[k], sqrt(toff), dsn->yd[k]);
		    tmp = min + k * dx;
		    for (j = 0, itmp = 0, toff = 0; j < dsi->nx; j++)
		      {
			if (fabs(tmp - dsi->yd[j]) < wsize / 2)
			  {
			    itmp += 1;
			    toff += dsi->xd[j];
			  }
		      }
		    toff /= (itmp > 0) ? itmp : 1;
		    add_new_point_with_y_error_to_ds(dst, dsn->xd[k], toff, (itmp) ? toff/sqrt(itmp): toff);
		    //add_new_point_to_ds(dst, dsn->xd[k], toff);
		  }
	      }
	  }
	for (i = 0; i < nxd; i++)
	  dsn->xd[i] = min + i * dx;
	set_plot_y_title(opn, "# of points");
      }
    else
     {
	for (i = 0; i < nxd; i++)
	  {
	    tmp =  min + i * dx;
	    for (k = 0; k < dsi->nx; k++)
	      {
		dsn->yd[i] += (fabs(tmp - dsi->yd[k]) < wsize / 2) ? dsi->xd[k] : 0;
		dsn->xd[i] += (fabs(tmp - dsi->yd[k]) < wsize / 2) ? 1 : 0;
	      }
	  }
	for (i = 0; i < nxd; i++)
	  {
	    dsn->yd[i] /= (dsn->xd[i] > 0) ? dsn->xd[i] : 1;
	    dsn->xd[i] = min + i * dx;
	  }
	set_plot_y_title(opn, "Avg of X");
	uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_Y_UNIT_SET);
      }


    uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opn, IS_X_UNIT_SET);
    set_plot_symb(dsn, "\\pt5\\di");
    set_ds_source(dsn, "window_histo w %g dx %g", wsize, dx);
    set_plot_title(opn, "window histo of %s", (op->title != NULL) ? op->title : "");
    set_plot_x_title(opn, "value");
    return refresh_plot(pr, UNCHANGED);
}

int x_or_y_range_of_all_ds_same_op(void)
{
    int i, j, k;
    pltreg  *pr = NULL;
    O_p     *op = NULL, *opn = NULL;
    d_s     *dsi = NULL, *dsn = NULL;
    int  index, maxi, mini;
    float min, max;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf_OK("Build a plot with the x or y range of each dataset\n"
                             "versus index\n");

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return win_printf_OK("cannot find data!");
    }

    index = RETRIEVE_MENU_INDEX;

    if (op->n_dat < 1)
    {
        return win_printf_OK("no data set!");
    }

    opn = create_and_attach_one_plot(pr, op->n_dat, op->n_dat, 0);

    if (opn == NULL)
    {
        return win_printf_OK("cannot create plot!");
    }

    dsn = opn->dat[0];
    opn->need_to_refresh = 1;

    if (index == X_AXIS)
    {
        for (j = 0, k = 0; j < op->n_dat; j++)
        {
            dsi = op->dat[j];

            if (dsi->nx < 2 || dsi->ny < 2)
            {
                continue;
            }

            if (dsi->xe && dsn->ye == NULL)
            {
                alloc_data_set_y_error(dsn);
            }

            for (mini = maxi = i = 0, min = max = dsi->xd[i] ; i < dsi->nx ; i++)
            {
                min = (dsi->xd[i] < min) ? dsi->xd[mini = i] : min;
                max = (dsi->xd[i] > max) ? dsi->xd[maxi = i] : max;
            }

            dsn->yd[k] = max - min;
            dsn->xd[k] = j;

            if (dsi->xe)
            {
                dsn->ye[k] = sqrt(dsi->xe[mini] * dsi->xe[mini] + dsi->xe[maxi] * dsi->xe[maxi]);
            }

            k++;
        }

        dsn->nx = dsn->ny = k;
        uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_Y_UNIT_SET);
        set_plot_symb(dsn, "\\pt5\\di");
        set_ds_source(dsn, "X range of all ds");
        set_plot_title(opn, "X range of %s", (op->title != NULL) ? op->title : "");
        set_plot_y_title(opn, "X range of all ds");
        set_plot_x_title(opn, "dataset index");
    }
    else if (index == Y_AXIS)
    {
        for (j = k = 0; j < op->n_dat; j++)
        {
            dsi = op->dat[j];

            if (dsi->nx < 2 || dsi->ny < 2)
            {
                continue;
            }

            if (dsi->ye && dsn->ye == NULL)
            {
                alloc_data_set_y_error(dsn);
            }

            for (mini = maxi = i = 0, min = max = dsi->yd[i] ; i < dsi->nx ; i++)
            {
                min = (dsi->yd[i] < min) ? dsi->yd[mini = i] : min;
                max = (dsi->yd[i] > max) ? dsi->yd[maxi = i] : max;
            }

            dsn->yd[k] = max - min;
            dsn->xd[k] = j;

            if (dsi->ye)
            {
                dsn->ye[k] = sqrt(dsi->ye[mini] * dsi->ye[mini] + dsi->ye[maxi] * dsi->ye[maxi]);
            }

            k++;
        }

        uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opn, IS_Y_UNIT_SET);
        set_ds_source(dsn, "Y range of all ds");
        set_plot_symb(dsn, "\\pt5\\di");
        set_plot_title(opn, "Y range of %s", (op->title != NULL) ? op->title : "");
        set_plot_y_title(opn, "Y range of all ds");
        set_plot_x_title(opn, "dataset index");
    }
    else if (index == DS_N_PTS)
    {
        for (j = 0; j < op->n_dat; j++)
        {
            dsi = op->dat[j];
            dsn->yd[j] = dsi->nx;
            dsn->xd[j] = j;
        }

        set_ds_source(dsn, "Number of points of all ds");
        set_plot_symb(dsn, "\\pt5\\di");
        set_plot_title(opn, "# pts. of %s", (op->title != NULL) ? op->title : "");
        set_plot_y_title(opn, "Number of points of all ds");
        set_plot_x_title(opn, "dataset index");
    }
    else if (index == DS_MEAN_XY)
    {
        for (j = 0; j < op->n_dat; j++)
        {
            dsi = op->dat[j];

            for (k = 0; k < dsi->nx; k++)
            {
                dsn->yd[j] += dsi->yd[k];
                dsn->xd[j] += dsi->xd[k];
            }

            if (dsi->nx)
            {
                dsn->yd[j] /= dsi->nx;
                dsn->xd[j] /= dsi->nx;
            }
        }

        set_ds_source(dsn, "mean value of all points in ds");
        set_plot_symb(dsn, "\\pt5\\di");
        set_plot_title(opn, "Mean value of %s", (op->title != NULL) ? op->title : "");
        set_plot_y_title(opn, "mean value in Y of all points in ds");
        set_plot_x_title(opn, "mean value in X of all points in ds");
    }
    else
    {
        return D_O_K;
    }

    return refresh_plot(pr, UNCHANGED);
}


O_p *x_extend_of_pts_in_y_range_all_ds_same_op(pltreg  *pr, O_p  *op, float y_lo, float y_hi)
{
    int i, j, k;
    O_p   *opn = NULL;
    d_s   *dsi = NULL, *dsn = NULL;
    int   maxi, mini;
    float min, max, x, dx;

    if (op->n_dat < 1)
    {
        return NULL;
    }

    opn = create_and_attach_one_plot(pr, op->n_dat, op->n_dat, 0);

    if (opn == NULL)
    {
        return (O_p *) win_printf_ptr("cannot create plot!");
    }

    dsn = opn->dat[0];
    opn->need_to_refresh = 1;

    for (j = 0, k = 0; j < op->n_dat; j++)
    {
        dsi = op->dat[j];

        if (dsi->nx < 2 || dsi->ny < 2)
        {
            continue;
        }

        if (dsi->xe && dsn->ye == NULL)
        {
            alloc_data_set_y_error(dsn);
        }

        dx = dsi->xd[dsi->nx - 1] - dsi->xd[0];

        if (dsi->nx > 1)
        {
            dx /= (dsi->nx - 1);
        }

        for (mini = maxi = -1, i = 1, min = FLT_MAX, max = -FLT_MAX; i < dsi->nx ; i++)
        {
            if ((dsi->yd[i] < y_hi) && (dsi->yd[i - 1] > y_hi))
            {
                x = dsi->xd[i - 1];

                if ((dsi->yd[i] - dsi->yd[i - 1]) != 0)
                {
                    x += (dsi->xd[i] - dsi->xd[i - 1]) * (y_hi - dsi->yd[i - 1]) / (dsi->yd[i] - dsi->yd[i - 1]);
                }

                if (x < min)
                {
                    mini = i;
                    min = x;
                }
            }

            if ((dsi->yd[i] < y_lo) && (dsi->yd[i - 1] > y_lo))
            {
                x = dsi->xd[i - 1];

                if ((dsi->yd[i] - dsi->yd[i - 1]) != 0)
                {
                    x += (dsi->xd[i] - dsi->xd[i - 1]) * (y_hi - dsi->yd[i - 1]) / (dsi->yd[i] - dsi->yd[i - 1]);
                }

                if (x > max)
                {
                    maxi = i;
                    max = x;
                }
            }
        }

        /*

        for (mini = maxi = -1, i = 0, min = FLT_MAX, max = FLT_MIN; i < dsi->nx ; i++)
        {
          if (dsi->yd[i] >= y_hi || dsi->yd[i] < y_lo) continue;
          min = (mini < 0 || dsi->xd[i] < min) ? dsi->xd[mini = i] : min;
          max = (maxi < 0 || dsi->xd[i] > max) ? dsi->xd[maxi = i] : max;
        }

             for (mini = maxi = -1, i = 1, min = FLT_MAX, max = FLT_MIN; i < dsi->nx ; i++)
        {
          //if (dsi->yd[i] >= y_hi || dsi->yd[i] < y_lo) continue;
          if ((dsi->yd[i] - y_lo)*(dsi->yd[i-1] - y_lo) > 0) continue;
          x = dsi->xd[i-1];
          if ((dsi->yd[i]-(dsi->yd[i-1]) != 0)
              x+= (dsi->yd[i]-(dsi->yd[i-1])*(y_lo - dsi->yd[i-1])/(dsi->yd[i]-(dsi->yd[i-1]);

          if ((dsi->yd[i] - y_hi)*(dsi->yd[i-1] - y_hi) > 0) continue;
          min = (mini < 0 || dsi->xd[i] < min) ? dsi->xd[mini = i] : min;
          max = (maxi < 0 || dsi->xd[i] > max) ? dsi->xd[maxi = i] : max;
        }
             */
        if (mini < 0 || maxi < 0)
        {
            continue;
        }

        if (max - min <= 0.001 * dx)
        {
            continue;
        }

        dsn->yd[k] = max - min;
        dsn->xd[k] = j;

        if (dsi->xe)
        {
            dsn->ye[k] = sqrt(dsi->xe[mini] * dsi->xe[mini] + dsi->xe[maxi] * dsi->xe[maxi]);
        }

        k++;
    }

    dsn->nx = dsn->ny = k;
    uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_Y_UNIT_SET);
    set_plot_symb(dsn, "\\pt5\\di");
    set_ds_source(dsn, "X range of pts in all ds with y in [%g,%g[", y_lo, y_hi);
    set_plot_title(opn, "X range of %s", (op->title != NULL) ? op->title : "");
    set_plot_y_title(opn, "X range of pts in all ds with y in [%g,%g[", y_lo, y_hi);
    set_plot_x_title(opn, "dataset index");
    return opn;
}



int x_or_y_range_of_visible_pts_all_ds_same_op(void)
{
    int i, j, k;
    pltreg  *pr = NULL;
    O_p     *op = NULL, *opn = NULL;
    d_s     *dsi = NULL, *dsn = NULL;
    int  index, maxi, mini, index_t;
    float min, max;
    static int index_type = 0;
    char question[1024] = {0};

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
        return win_printf("Build a plot with the x or y range of each dataset\n"
                          "versus index\n");

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2)
    {
        return win_printf_OK("cannot find data!");
    }

    index = RETRIEVE_MENU_INDEX;

    if (op->n_dat < 1)
    {
        return win_printf_OK("no data set!");
    }

    dsi = op->dat[0];
    index_t = 0;

    if (dsi->src_parameter_type[0] != NULL)
    {
        for (i = 0; dsi->src_parameter_type[i] != NULL && i < 8 ; i++);

        index_t = index_type;
        snprintf(question, sizeof(question), "You can choose to plot Max - Min vesus:\n"
                 "%%R->point index\n");

        for (j = 0; j < i ; j++)
        {
            snprintf(question + strlen(question), sizeof(question) - strlen(question),
                     "%%r->%s\n", dsi->src_parameter_type[j]);
        }

        j = win_scanf(question, &index_t);

        if (j == WIN_CANCEL)
        {
            return D_O_K;
        }

        index_type = index_t;
    }

    opn = create_and_attach_one_plot(pr, op->n_dat, op->n_dat, 0);

    if (opn == NULL)
    {
        return win_printf_OK("cannot create plot!");
    }

    dsn = opn->dat[0];
    opn->need_to_refresh = 1;

    if (index == X_AXIS)
    {
        for (j = 0, k = 0; j < op->n_dat; j++)
        {
            dsi = op->dat[j];

            if (dsi->nx < 2 || dsi->ny < 2)
            {
                continue;
            }

            if (dsi->xe && dsn->ye == NULL)
            {
                alloc_data_set_y_error(dsn);
            }

            for (mini = maxi = -1, i = 0, min = FLT_MAX, max = -FLT_MAX; i < dsi->nx ; i++)
            {
                if (dsi->xd[i] >= op->x_hi || dsi->xd[i] < op->x_lo
                        || dsi->yd[i] >= op->y_hi || dsi->yd[i] < op->y_lo)
                {
                    continue;
                }

                min = (mini < 0 || dsi->xd[i] < min) ? dsi->xd[mini = i] : min;
                max = (maxi < 0 || dsi->xd[i] > max) ? dsi->xd[maxi = i] : max;
            }

            if (mini < 0 || maxi < 0)
            {
                continue;
            }

            if (max - min <= 0)
            {
                continue;
            }

            dsn->yd[k] = max - min;

            if (index_t == 0)
            {
                dsn->xd[k] = j;
            }
            else if (index_t < 8)
            {
                dsn->xd[k] = dsi->src_parameter[index_t - 1];
            }

            if (dsi->xe)
            {
                dsn->ye[k] = sqrt(dsi->xe[mini] * dsi->xe[mini] + dsi->xe[maxi] * dsi->xe[maxi]);
            }

            k++;
        }

        dsn->nx = dsn->ny = k;
        uns_op_2_op_by_type(op, IS_X_UNIT_SET, opn, IS_Y_UNIT_SET);
        set_plot_symb(dsn, "\\pt5\\di");
        set_ds_source(dsn, "X range of visible pts in all ds");
        set_plot_title(opn, "X range of %s", (op->title != NULL) ? op->title : "");
        set_plot_y_title(opn, "X range of visible pts in all ds");

        if (index_t == 0)
        {
            set_plot_x_title(opn, "dataset index");
        }
        else if (index_t < 8)
        {
	  set_plot_x_title(opn, "%s", op->dat[0]->src_parameter_type[index_t - 1]);
        }
    }
    else if (index == Y_AXIS)
    {
        for (j = k = 0; j < op->n_dat; j++)
        {
            dsi = op->dat[j];

            if (dsi->nx < 2 || dsi->ny < 2)
            {
                continue;
            }

            if (dsi->ye && dsn->ye == NULL)
            {
                alloc_data_set_y_error(dsn);
            }

            for (mini = maxi = -1, i = 0, min = FLT_MAX, max = -FLT_MAX; i < dsi->nx ; i++)
            {
                if (dsi->xd[i] >= op->x_hi || dsi->xd[i] < op->x_lo
                        || dsi->yd[i] >= op->y_hi || dsi->yd[i] < op->y_lo)
                {
                    continue;
                }

                min = (mini < 0 || dsi->yd[i] < min) ? dsi->yd[mini = i] : min;
                max = (maxi < 0 || dsi->yd[i] > max) ? dsi->yd[maxi = i] : max;
            }

            if (mini < 0 || maxi < 0)
            {
                continue;
            }

            if (max - min <= 0)
            {
                continue;
            }

            dsn->yd[k] = max - min;

            if (index_t == 0)
            {
                dsn->xd[k] = j;
            }
            else if (index_t < 8)
            {
                dsn->xd[k] = dsi->src_parameter[index_t - 1];
            }

            if (dsi->ye)
            {
                dsn->ye[k] = sqrt(dsi->ye[mini] * dsi->ye[mini] + dsi->ye[maxi] * dsi->ye[maxi]);
            }

            k++;
        }

        uns_op_2_op_by_type(op, IS_Y_UNIT_SET, opn, IS_Y_UNIT_SET);
        set_ds_source(dsn, "Y range of all ds");
        set_plot_symb(dsn, "\\pt5\\di");
        set_plot_title(opn, "Y range of %s", (op->title != NULL) ? op->title : "");
        set_plot_y_title(opn, "Y range of all ds");

        if (index_t == 0)
        {
            set_plot_x_title(opn, "dataset index");
        }
        else if (index_t < 8)
        {
	  set_plot_x_title(opn, "%s", op->dat[0]->src_parameter_type[index_t - 1]);
        }
    }
    else if (index == DS_N_PTS)
    {
        for (j = 0; j < op->n_dat; j++)
        {
            dsi = op->dat[j];
            dsn->yd[j] = dsi->nx;

            if (index_t == 0)
            {
                dsn->xd[j] = j;
            }
            else if (index_t < 8)
            {
                dsn->xd[j] = dsi->src_parameter[index_t - 1];
            }
        }

        set_ds_source(dsn, "Number of points of all ds");
        set_plot_symb(dsn, "\\pt5\\di");
        set_plot_y_title(opn, "Number of points of all ds");

        if (index_t == 0)
        {
            set_plot_x_title(opn, "dataset index");
        }
        else if (index_t < 8)
        {
	  set_plot_x_title(opn, "%s", op->dat[0]->src_parameter_type[index_t - 1]);
        }
    }
    else
    {
        return D_O_K;
    }

    return refresh_plot(pr, UNCHANGED);
}



/********************************************************************/
/* simple operations on all datasets simultaneously                 */
/********************************************************************/
int opp_on_all_ds_same_op(void)
{
    int i, j;
    pltreg  *pr = NULL;
    O_p     *op = NULL;
//   float   tmp;
    d_s     *dsi1 = NULL, *dss = NULL;
    int  nx1, index, n_ds_start, n_ds_end, ds_keep, ds_in;
    static float ax =  0, dx = 1, rx = 1;
    float drx;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Arithmetical operations on oall dataset\n"
                             "a new dataset is created in the same plot");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi1) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    if (dsi1 == NULL)
    {
        return (win_printf_OK("can't find data set"));
    }

    //index = active_menu->flags & 0xFFFFFF00;
    index = RETRIEVE_MENU_INDEX;
    nx1 = dsi1->nx;

    if (index == X_AXIS)
    {
        ds_in = n_ds_start = n_ds_end = op->cur_dat;
        ds_keep = 0;
        i = win_scanf("Transformation on all data sets along X:"
                      "new X = [ old X \\times {\\it \\frac{n}{d}}] + {\\it b}\n\n"
                      "offset {\\it b} = %8f \n"
                      "multiplier {\\it n} = %8f divisor {\\it d} %8f\n"
                      "You can either create a new data set %R or replace existing one %r\n"
                      , &ax, &dx, &rx, &ds_keep);

        if (i == WIN_CANCEL)
        {
            return OFF;    // index = 0;
        }

        if (rx == 0)
        {
            return (win_printf_OK("can't divide by 0!"));
        }

        n_ds_start = 0;
        n_ds_end = op->n_dat;

        if (n_ds_end < n_ds_start || n_ds_start < 0 || n_ds_end >= op->n_dat)
        {
            return win_printf_OK("data set range %d to %d out of range!", n_ds_start, n_ds_end);
        }

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
            dsi1 = op->dat[j];

            if (ds_keep == 0)
            {
                dss = duplicate_data_set(dsi1, NULL);

                if (dss != NULL)
                {
                    inherit_from_ds_to_ds(dss, dsi1);
                    add_one_plot_data(op, IS_DATA_SET, (void *)dss);
                }
            }
            else
            {
                dss = op->dat[j];
            }

            if (dss == NULL)
            {
                return (win_printf_OK("can't create data set"));
            }

            for (i = 0, drx = dx / rx ; i < dss->nx ; i++)
            {
                dss->xd[i] = ax + drx * dsi1->xd[i];

                if (dss->xe)
                {
                    dss->xe[i] = drx * dsi1->xe[i];
                }
                if (dss->xed)
                {
                    dss->xed[i] = drx * dsi1->xed[i];
                }
                if (dss->xbd)
                {
                    dss->xbd[i] = drx * dsi1->xbd[i];
                }
                if (dss->xbu)
                {
                    dss->xbu[i] = drx * dsi1->xbu[i];
                }

            }

            update_ds_treatement(dss, "X axis multiplied by %g, divided by %g and offseted by %g", dx, rx, ax);
        }

        op->cur_dat = ds_in;
        dss = NULL; // dataset already added
        op->need_to_refresh = 1;
    }
    else if (index == Y_AXIS)
    {
        ds_in = n_ds_start = n_ds_end = op->cur_dat;
        ds_keep = 0;
        i = win_scanf("Transformation on all data sets along Y:"
                      "new Y = [ old Y \\times {\\it \\frac{n}{d}}] + {\\it b}\n\n"
                      "offset {\\it b} = %8f \n"
                      "multiplier {\\it n} = %8f divisor {\\it d} %8f\n"
                      "You can either create a new data set %R or replace existing one %r\n"
                      , &ax, &dx, &rx, &ds_keep);

        if (i == WIN_CANCEL)
        {
            return OFF;    // index = 0;
        }

        if (rx == 0)
        {
            return (win_printf_OK("can't divide by 0!"));
        }

        n_ds_start = 0;
        n_ds_end = op->n_dat;

        if (n_ds_end < n_ds_start || n_ds_start < 0 || n_ds_end >= op->n_dat)
        {
            return win_printf_OK("data set range %d to %d out of range!", n_ds_start, n_ds_end);
        }

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
            dsi1 = op->dat[j];

            if (ds_keep == 0)
            {
                dss = duplicate_data_set(dsi1, NULL);

                if (dss != NULL)
                {
                    inherit_from_ds_to_ds(dss, dsi1);
                    add_one_plot_data(op, IS_DATA_SET, (void *)dss);
                }
            }
            else
            {
                dss = op->dat[j];
            }

            if (dss == NULL)
            {
                return (win_printf_OK("can't create data set"));
            }

            for (i = 0, drx = dx / rx ; i < dss->nx ; i++)
            {
                dss->yd[i] = ax + drx * dsi1->yd[i];

                if (dss->ye)
                {
                    dss->ye[i] = drx * dsi1->ye[i];
                }
                if (dss->yed)
                {
                    dss->yed[i] = drx * dsi1->yed[i];
                }
                if (dss->ybd)
                {
                    dss->ybd[i] = drx * dsi1->ybd[i];
                }
                if (dss->ybu)
                {
                    dss->ybu[i] = drx * dsi1->ybu[i];
                }


            }

            update_ds_treatement(dss, "Y axis multiplied by %g, divided by %g and offseted by %g", dx, rx, ax);
        }

        op->cur_dat = ds_in;
        dss = NULL; // dataset already added
        op->need_to_refresh = 1;
    }
    else if (index == DS_INVERSE)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	    dss = ds_1_over_y(op->dat[j]);
            if (dss != NULL)
            {
                add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_SQRT)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	    dsi1 = op->dat[j];
	    dss = ds_squareroot(dsi1);
            if (dss != NULL)
            {
                add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_SQR)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	    dss = ds_square(op->dat[j]);
            if (dss != NULL)
            {
                add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_ABS)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
            dss = ds_abs(op->dat[j]);

            if (dss != NULL)
            {
                add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_LOG)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	  dss = ds_log10(op->dat[j]);
	  if (dss != NULL)
            {
	      add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_LOG10)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	  dss = ds_log10(op->dat[j]);
	  if (dss != NULL)
            {
	      add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_LOG2)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	  dss = ds_log2(op->dat[j]);
	  if (dss != NULL)
            {
	      add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_EXPO)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	  dss = ds_exp(op->dat[j]);
	  if (dss != NULL)
            {
	      add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_EXPO_10)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	  dss = ds_exp10(op->dat[j]);
	  if (dss != NULL)
            {
	      add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_EXPO_2)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	  dss = ds_exp2(op->dat[j]);
	  if (dss != NULL)
            {
	      add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_ATAN)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	    dsi1 = op->dat[j];
            dss = duplicate_data_set(dsi1, NULL);

            if (dss == NULL)
            {
                return (win_printf_OK("can't create data set"));
            }

            for (i = 0 ; i < nx1 ; i++)
            {
                dss->yd[i] = (float)atan(dsi1->yd[i]);
            }

            inherit_from_ds_to_ds(dss, dsi1);
            update_ds_treatement(dss, "arctan(ds)");

            if (dss != NULL)
            {
                add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_TANH)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	    dsi1 = op->dat[j];
            dss = duplicate_data_set(dsi1, NULL);

            if (dss == NULL)
            {
                return (win_printf_OK("can't create data set"));
            }

            for (i = 0 ; i < nx1 ; i++)
            {
                dss->yd[i] = (float)tanh(dsi1->yd[i]);
            }

            inherit_from_ds_to_ds(dss, dsi1);
            update_ds_treatement(dss, "tanh(ds)");

            if (dss != NULL)
            {
                add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_ATAN2)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	    dsi1 = op->dat[j];
            dss = duplicate_data_set(dsi1, NULL);

            if (dss == NULL)
            {
                return (win_printf_OK("can't create data set"));
            }

            for (i = 0 ; i < nx1 ; i++)
            {
                dss->xd[i] = i;
                dss->yd[i] = (float)atan2(dsi1->yd[i], dsi1->xd[i]);
            }

            inherit_from_ds_to_ds(dss, dsi1);
            update_ds_treatement(dss, "arctan2(ds)");

            if (dss != NULL)
            {
                add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_EXTRACT_X_T)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	    dsi1 = op->dat[j];
            dss = build_data_set(nx1, nx1);

            if (dss == NULL)
            {
                return (win_printf_OK("can't create data set"));
            }

            dss->ny = nx1;
            dss->nx = nx1;

            if (dsi1->xe)
            {
                alloc_data_set_y_error(dss);
            }

            for (i = 0 ; i < nx1 ; i++)
            {
                dss->xd[i] = i;
                dss->yd[i] = dsi1->xd[i];

                if (dss->ye)
                {
                    dss->ye[i] = dsi1->xe[i];
                }
            }

            inherit_from_ds_to_ds(dss, dsi1);
            update_ds_treatement(dss, "ds extract x serie");

            if (dss != NULL)
            {
                add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_EXTRACT_Y_T)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	    dsi1 = op->dat[j];
            dss = build_data_set(nx1, nx1);

            if (dss == NULL)
            {
                return (win_printf_OK("can't create data set"));
            }

            dss->ny = nx1;
            dss->nx = nx1;

            if (dsi1->ye)
            {
                alloc_data_set_y_error(dss);
            }

            for (i = 0 ; i < nx1 ; i++)
            {
                dss->xd[i] = i;
                dss->yd[i] = dsi1->yd[i];

                if (dss->ye)
                {
                    dss->ye[i] = dsi1->ye[i];
                }
            }

            inherit_from_ds_to_ds(dss, dsi1);
            update_ds_treatement(dss, "ds extract y serie");

            if (dss != NULL)
            {
                add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else if (index == DS_UNWRAP)
    {
        n_ds_start = 0;
        n_ds_end = op->n_dat;

        for (j = n_ds_start; j <= n_ds_end; j++)
        {
	    dsi1 = op->dat[j];
            dss = duplicate_data_set(dsi1, NULL);

            if (dss == NULL)
            {
                return (win_printf_OK(" "));
            }

            unwrap(dss->yd, dss->ny); // declared in xv_tools_lib.c
            inherit_from_ds_to_ds(dss, dsi1);
            update_ds_treatement(dss, "unwrap(ds)");

            if (dss != NULL)
            {
                add_one_plot_data(op, IS_DATA_SET, (void *)dss);
            }
        }
    }
    else
    {
        return D_O_K;
    }

    //if (dss != NULL)    add_one_plot_data (op, IS_DATA_SET, (void *)dss);
    return refresh_plot(pr, UNCHANGED);
}

int average_all_ds_of_op_by_index(void)
{
    int i, j, k;
    pltreg  *pr = NULL;
    O_p     *ops = NULL;
    d_s     *dsd = NULL, *dsi = NULL;
    int      max, nds;

    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine performs averaging oy x and y of all data sets\n"
                             "having the same point index");
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &ops) != 2)
    {
        return win_printf_OK("cannot find data!");
    }

    nds = ops->n_dat;

    for (k = 0, max = 0; k < nds; k++)
    {
        dsd = ops->dat[k];

        if (dsd->nx > max)
        {
            max = dsd->nx;
        }

        if (dsd->ny > max)
        {
            max = dsd->ny;
        }
    }

    dsd = create_and_attach_one_ds(ops, max, max, 0);

    if (dsd == NULL)
    {
        return win_printf("Cannot allocate plot");
    }
    if (nds > 2)     alloc_data_set_y_error(dsd);
    for (i = 0; i < dsd->nx; i++)
    {
        for (k = j = 0; k < nds; k++)
        {
            dsi = ops->dat[k];

            if (i < dsi->nx)
            {
                dsd->xd[i] += dsi->xd[i];
                dsd->yd[i] += dsi->yd[i];
                j++;
            }
        }
        if (j > 0)
        {
            dsd->xd[i] /= j;
            dsd->yd[i] /= j;
        }
    }

    for (i = 0; dsd->ye != NULL && i < dsd->nx; i++)
    {
        for (k = j = 0; k < nds; k++)
        {
            dsi = ops->dat[k];

            if (i < dsi->nx)
            {
	      dsd->ye[i] += (dsi->yd[i] - dsd->yd[i])*(dsi->yd[i] - dsd->yd[i]);
	      j++;
            }
        }
        if (j > 1)
        {
	  dsd->ye[i] /= (j-1);
	  dsd->ye[i] = sqrt(dsd->ye[i]);
        }
    }


    set_ds_source(dsd, "Average data sets over %d data sets", ops->n_dat);
    ops->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
}
# define    INDEX_OUT_OF_RANGE  3
# define    PB_WITH_EXTREMUM    2
# define    TOO_MUCH_NOISE      4
# define    GETTING_REF_PROFILE 1


/*  Find maximum position in a 1d array, the array is supposed periodic
 *  return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int find_max_around(float *x, int nx, int nmax, float *Max_pos, float *Max_val, float *Max_deriv)
{
    int   ret = 0, delta;
    double a, b, c, d, f_2, f_1, f0, f1, f2, xm = 0;

    if (nmax >= nx)
    {
        return INDEX_OUT_OF_RANGE;
    }

    f_2 = x[(nx + nmax - 2) % nx];
    f_1 = x[(nx + nmax - 1) % nx];
    f0 = x[nmax];
    f1 = x[(nmax + 1) % nx];
    f2 = x[(nmax + 2) % nx];

    if (f_1 < f1)
    {
        a = -f_1 / 6 + f0 / 2 - f1 / 2 + f2 / 6;
        b = f_1 / 2 - f0 + f1 / 2;
        c = -f_1 / 3 - f0 / 2 + f1 - f2 / 6;
        d = f0;
        delta = 0;
    }
    else
    {
        a = b = c = 0;
        a = -f_2 / 6 + f_1 / 2 - f0 / 2 + f1 / 6;
        b = f_2 / 2 - f_1 + f0 / 2;
        c = -f_2 / 3 - f_1 / 2 + f0 - f1 / 6;
        d = f_1;
        delta = -1;
    }

    if (fabs(a) < 1e-8)
    {
        if (b != 0)
        {
            xm =  - c / (2 * b) - (3 * a * c * c) / (4 * b * b * b);
        }
        else
        {
            xm = 0;
            ret |= PB_WITH_EXTREMUM;
        }
    }
    else if ((b * b - 3 * a * c) < 0)
    {
        ret |= PB_WITH_EXTREMUM;
        xm = 0;
    }
    else
    {
        xm = (-b - sqrt(b * b - 3 * a * c)) / (3 * a);
    }

    *Max_pos = (float)(xm + nmax + delta);
    *Max_val = (float)((a * xm * xm * xm) + (b * xm * xm) + (c * xm) + d);

    if (Max_deriv)
    {
        *Max_deriv = (float)((6 * a * xm) + (2 * b));
    }

    return ret;
}

/*  Find maximum position in a 1d array, the array is supposed periodic
 *  return 0 -> everything fine, TOO_MUCH_NOISE or PB_WITH_EXTREMUM
 *
 */
int find_zero_around(float *x, int nx, int nzer, float *Zero_pos,  float *Zero_deriv)
{
    int   ret = 0;//, first = 1;
    double a, b, c, f_1, f0, f1, xm = 0, x1, x2, delta;

    if (nzer >= nx)
    {
        return INDEX_OUT_OF_RANGE;
    }

    f_1 = x[(nx + nzer - 1) % nx] / 2;
    f0 = x[nzer];
    f1 = x[(nzer + 1) % nx] / 2;
    //if (first == 1) win_printf("x[%d] = %g; x[%d] = %g; x[%d] = %g\n",
    //(nx + nzer - 1) % nx,2*f_1,nzer,f0,(nx + nzer + 1) % nx,2*f1);
    //f_1*x*(x-1)/2 - f0*(x+1)*(x-1) + f1*x*(x+1)/2;
    //(f_1/2)*[x2-x] -f0*[x2-1] +(f1/2)*[x2+x] = 0;
    //x2*[f_1/2 -f0 + f1/2] + x*[f1/2-f_1/2] +f0
    a = f_1 - f0  + f1;
    b = f1 - f_1;
    c =  f0;
    delta = 0;
    delta = b * b - 4 * a * c;

    if (fabs(a) < 1e-8)
    {
        if (b != 0)
        {
            xm = -c / b;
        }
        else
        {
            xm = 0;
            ret |= PB_WITH_EXTREMUM;
        }
    }

    if (delta < 0)
    {
        ret |= PB_WITH_EXTREMUM;
        xm = 0;
    }
    else
    {
        x1 = (-b + sqrt(delta)) / (2 * a);
        x2 = (-b - sqrt(delta)) / (2 * a);
        //if (first == 1) win_printf("x1 %g et x2 %g",x1,x2);
        xm = (fabs(x1) < fabs(x2)) ? x1 : x2;
    }

    *Zero_pos = (float)(xm + nzer);

    if (Zero_deriv)
    {
        *Zero_deriv = (float)((2 * a * xm) + b);
    }

    //first = 0;
    return ret;
}
d_s *find_max_peaks(d_s *dsi, float mthres)
{
   d_s *dsp = NULL;
   int k, i1, i, ret;
   float Max_pos, Max_val, p, tmpx;

   if (dsi == NULL || dsi->nx <= 1) return NULL;
   dsp = build_data_set(16, 16);
   if (dsp == NULL) return NULL;
   dsp->nx = dsp->ny = 0;
   set_ds_dot_line(dsp);
   set_plot_symb(dsp, "\\pt5\\oc ");
   inherit_from_ds_to_ds(dsp, dsi);
   set_ds_source(dsp, "Peaks found with threshold %g  for %s"
                 ,mthres, ((dsp->source != NULL) ? dsp->source : " "));

    for (k = 1; k < dsi->nx - 1; k++)
    {
        if ((dsi->yd[k] > dsi->yd[k - 1]) && (dsi->yd[k] >= dsi->yd[k + 1])
                && (dsi->yd[k] > mthres))
        {
            ret = find_max_around(dsi->yd, dsi->nx, k, &Max_pos, &Max_val, NULL);

            if (ret == 0)
            {
                i = (int)Max_pos;
                i1 = i + 1;
                p = (float)i1 - Max_pos;
                i = (i < 0) ? 0 : i;
                i = (i < dsi->nx) ? i : dsi->nx - 1;
                i1 = (i1 < 0) ? 0 : i1;
                i1 = (i1 < dsi->nx) ? i1 : dsi->nx - 1;
                tmpx = p * dsi->xd[i] + (1 - p) * dsi->xd[i1];
                add_new_point_to_ds(dsp, tmpx, Max_val);
            }
        }
    }
    return dsp;
}

int find_peaks_in_Y_of_equally_spaced_ds_all_ds(void)
{
    int i, k, i1;
    pltreg  *pr = NULL;
    O_p     *ops = NULL;
    d_s     *dsi = NULL, *dsp = NULL;
    int      ret, ii;
    static int mode = 1, multids = 0, ndss = 0, ndse = 1;
    static float thres = 0.01, dods = 1, dolab = 0, dumpcsv = 0, bigger = 1;
    float ymin, ymax, Max_pos, Max_val, tmpx, p, mthres;
    char question[512] = {0};
    FILE *fp = NULL;
    char cfilename[1024] = {0};

    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine performs averaging oy x and y of all data sets\n"
                             "having the same point index");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &ops, &dsi) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    for (k = 0, ymin = ymax = dsi->yd[0]; k < dsi->nx; k++)
    {
        if (dsi->yd[k] > ymax)
        {
            ymax = dsi->yd[k];
        }

        if (dsi->yd[k] < ymin)
        {
            ymin = dsi->yd[k];
        }
    }
    ndss = (ndss < ops->n_dat) ? ndss : ops->n_dat;
    ndse = (ndse <= ops->n_dat) ? ndse : ops->n_dat;
    snprintf(question, sizeof(question), "Find peaks of equally spaced data in ds %d in [0,%d[\n"
             "Min = %g max = %g\n"
             "Keep peaks if :\n"
             "%%R they are bigger than %g x a relative threshold R\n"
             "%%r they are bigger than a value V\n"
             "%%r they are inside the graph boudary\n"
             "  in first case define the relative factor R = %%12f\n"
             "  in second case define the minimum value V  = %%12f\n"
	     "Proceed %%R->selected dataset %%r->or multiple in [%%4d,%%4d[\n"
             "Add a dataset with peaks %%b"
             "Add labels %%b \nDump peaks in a CSV file %%b\n", ops->cur_dat,ops->n_dat,ymin, ymax, ymax);
    i = win_scanf(question, &mode, &thres, &bigger, &multids,&ndss,&ndse,&dods, &dolab, &dumpcsv);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    if (dods == 0 && dolab == 0)
    {
        return win_printf_OK("Nothing to do!");
    }
    if (mode == 0)    mthres = ymax * thres;
    else  if (mode == 1)    mthres = bigger;
    else mthres = ops->ax + ops->y_lo * ops->dx;
    int ids = 0, curds = ops->cur_dat;
    for (curds = ops->cur_dat, ids = (multids) ? ndss : curds; ids < ((multids) ? ndse : curds+1); ids++)
      {
	dsi = ops->dat[ids];
	if (dods)
	  {
	    dsp = create_and_attach_one_ds(ops, 16, 16, 0);

	    if (dsp != NULL)
	      {
		dsp->nx = dsp->ny = 0;
		set_ds_dot_line(dsp);
		set_plot_symb(dsp, "\\pt5\\oc ");
		inherit_from_ds_to_ds(dsp, dsi);
		set_ds_source(dsp, "Peaks found with relative threshold %g leading to actual threshold = %g for %s"
			      , thres, mthres, ((dsp->source != NULL) ? dsp->source : " "));
	      }
	  }

	if (dumpcsv)
	  {
	    if (fp == NULL)
	      {
		if (ops->dir && ops->filename)
		  {
		    append_filename(cfilename, ops->dir, ops->filename, 1024);
		    replace_extension(cfilename, cfilename, "csv", sizeof(cfilename));
		  }

		if (do_select_file(cfilename, sizeof(cfilename), "CSV-FILE", "*.csv", "File to save peaks", 0))
		  {
		    return win_printf_OK("Cannot select input file");
		  }

		fp = fopen(cfilename, "w");
		if (fp == NULL)
		  {
		    win_printf("Cannot open file:\n%s", backslash_to_slash(cfilename));
		    return -1;
		  }

		fprintf(fp, "Peak_Nb\t");
		fprintf(fp, "index\t");
		fprintf(fp, "X(%s)\t", (ops->x_unit != NULL) ? ops->x_unit : "");
		fprintf(fp, "Amplitude\n");
	      }
	  }

	for (k = 1, ii = 0; k < dsi->nx - 1; k++)
	  {
	    if (mode == 2 && (dsi->xd[k] < ops->x_lo || dsi->xd[k] > ops->x_hi))
	      continue;
	    if (mode == 2 && (dsi->yd[k] < ops->y_lo || dsi->yd[k] > ops->y_hi))
	      continue;
	    if ((dsi->yd[k] > dsi->yd[k - 1]) && (dsi->yd[k] >= dsi->yd[k + 1])
		&& (dsi->yd[k] > mthres))
	      {
		//win_printf("k = %d y %g",k,dsi->yd[k]);
		ret = find_max_around(dsi->yd, dsi->nx, k, &Max_pos, &Max_val, NULL);

		//win_printf("k = %d y %g\n ret %d pos %g val %g",k,dsi->yd[k],ret,Max_pos,Max_val);
		if (ret == 0)
		  {
		    i = (int)Max_pos;
		    i1 = i + 1;
		    p = (float)i1 - Max_pos;
		    i = (i < 0) ? 0 : i;
		    i = (i < dsi->nx) ? i : dsi->nx - 1;
		    i1 = (i1 < 0) ? 0 : i1;
		    i1 = (i1 < dsi->nx) ? i1 : dsi->nx - 1;
		    tmpx = p * dsi->xd[i] + (1 - p) * dsi->xd[i1];

		    if (dolab)
		      {
			snprintf(question, sizeof(question), "\\pt7\\center{\\stack{{index = %g}{x = %g %s}{y = %g}{\\oc}}}"
				 , Max_pos, ops->ax + ops->dx * tmpx, (ops->x_unit != NULL) ? ops->x_unit : "", ops->ay + ops->dy * Max_val);
			set_ds_plot_label(dsi, tmpx, Max_val, USR_COORD, "%s",question);
		      }

		    if (dsp != NULL)
		      {
			add_new_point_to_ds(dsp, tmpx, Max_val);
		      }

		    if (dumpcsv && (fp != NULL))
		      {
			fprintf(fp, "%d\t", ii++);
			fprintf(fp, "%g\t", Max_pos);
			fprintf(fp, "%g\t", ops->ax + ops->dx * tmpx);
			fprintf(fp, "%g\n", Max_val);
		      }
		  }
	      }
	  }
      }
    if (dumpcsv && (fp != NULL))
    {
        fclose(fp);
    }

    ops->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
}




O_p *build_histo_with_exponential_convolution_2(pltreg *pr, O_p *ops, d_s *dsi, float biny)
{
    int  j, k;
    int       maxh, iy, np;
    d_s *dsh = NULL;
    O_p *oph = NULL;
    float ymin, ymax, tmp;

    if (dsi == NULL)
    {
        return NULL;
    }

    for (k = 0, ymin = ymax = dsi->yd[0]; k < dsi->nx; k++)
    {
        if (dsi->yd[k] > ymax)
        {
            ymax = dsi->yd[k];
        }

        if (dsi->yd[k] < ymin)
        {
            ymin = dsi->yd[k];
        }
    }

    maxh = (int)(0.5 + ((ymax - ymin) * 5) / biny);
    maxh += 50;

    if (pr != NULL)
    {
        oph = create_and_attach_one_plot(pr, maxh, maxh, 0);
    }
    else
    {
        oph = create_one_plot(maxh, maxh, 0);
    }

    if (oph == NULL)
    {
        return NULL;
    }

    //dsh = create_and_attach_one_ds(ops, maxh, maxh, 0);
    dsh = oph->dat[0];

    if (dsh == NULL)
    {
        return NULL;
    }

    for (k = 0, np = 0; k < dsi->nx; k++)
    {
        iy = (int)(0.5 + ((dsi->yd[k] - ymin) * 5) / biny);
        iy += 25;

        for (j = iy - 25; j <= iy + 25; j++)
        {
            if (j >= 0 && j < dsh->nx)
            {
                tmp = ymin + ((j - 25) * biny / 5) - dsi->yd[k];
                tmp /= biny;
                tmp = exp(-tmp * tmp);
                dsh->yd[j] += tmp;
                np++;
            }
        }
    }

    for (k = 0; np > 0 && k < dsh->nx; k++)
    {
            //dsh->yd[k] *= (float)51 / np;
        dsh->xd[k] = ymin + ((k - 25) * biny / 5);
    }

    inherit_from_ds_to_ds(dsh, dsi);

    if (ops != NULL)
    {
        uns_op_2_op_by_type(ops, IS_Y_UNIT_SET, oph, IS_X_UNIT_SET);
    }

    set_ds_source(dsh, "Histogramm by exponential convolution \\sigma = %g", biny);
    oph->need_to_refresh = 1;
    return oph;
}
// if dsi != NULL, treat a single data set
// if dsi == NULL and ops != NULL treat all ds of ops
// y_min et y_max limits of the histo
O_p *build_histo_with_exponential_convolution_with_y_lim(pltreg *pr, O_p *ops, d_s *dsi, float biny, float y_min, float y_max)
{
    int  j, k;
    int       maxh, iy, np, iy_1, allds, ids, nds;
    d_s *dsh = NULL, *dst = NULL;
    O_p *oph = NULL;
    float ymin = 0, ymax = 0, tmp, fiy, aiy, aiy_1;
    static float fexp[51] = {0};
    static int fexp_init = 0;

    if (dsi == NULL && ops == NULL)
    {
            //win_printf("build_histo_with_exponential_convolution\nops null and dsi null!\n");
        return NULL;
    }

    allds = (dsi == NULL && ops != NULL) ? 1 : 0;

    if (fexp_init == 0)
    {
        for (j = 0; j <= 50; j++)
        {
            tmp = ((float)(j - 25)) / 5;
            fexp[j] = exp(-tmp * tmp);
        }

        fexp_init = 1;
    }

    nds = (allds) ? ops->n_dat : 1;


    for (ids = 0; ids < nds; ids++)
      {
	dst = (allds) ? ops->dat[ids] : dsi;

	if (ids == 0)
	  {
	    ymin = ymax = dst->yd[0];
	  }

	for (k = 0; k < dst->nx; k++)
	  {
	    if (dst->yd[k] > ymax)
	      {
		ymax = dst->yd[k];
	      }

	    if (dst->yd[k] < ymin)
	      {
		ymin = dst->yd[k];
	      }
	  }
      }
    if (y_min >= y_max)
      {
	ymin = (y_min <= ymin) ? y_min : ymin;
	ymax = (y_max >= ymax) ? y_max : ymax;
	//win_printf("using local extremum!");
      }

    maxh = (int)(0.5 + ((ymax - ymin) * 5) / biny);
    maxh += 50;

    if (maxh < 0 || maxh > (10 * 2e6))
    {
        int ret = win_printf("you are trying to create a very big histogram %d points, the process will be abort unless you hit Cancel\n", maxh);
        if (ret != WIN_CANCEL)
            return NULL;
    }

    if (pr != NULL)
    {
        oph = create_and_attach_one_plot(pr, maxh, maxh, 0);
    }
    else
    {
        oph = create_one_plot(maxh, maxh, 0);
    }

    if (oph == NULL)
    {
            //win_printf("build_histo_with_exponential_convolution\ncannot create oph with %d pts!\nYmin %g Ymax %g biny %g\n", maxh,
            //     ymax, ymin, biny);
        return NULL;
    }

    //dsh = create_and_attach_one_ds(ops, maxh, maxh, 0);
    dsh = oph->dat[0];

    if (dsh == NULL)
    {
            //win_printf("build_histo_with_exponential_convolution\ndsh null in oph with %d pts!\n", maxh);
        return NULL;
    }

    for (ids = 0, np = 0; ids < nds; ids++)
    {
        dst = (allds) ? ops->dat[ids] : dsi;

        if (ids == 0)
        {
            inherit_from_ds_to_ds(dsh, dst);
        }

        for (k = 0; k < dst->nx; k++)
        {
            fiy = ((dst->yd[k] - ymin) * 5) / biny;
            iy_1 = (int)fiy;
            iy = iy_1 + 1;
            aiy = fabs(fiy - iy_1);
            aiy_1 = 1 - aiy;
            iy_1 += 25;

            for (j = iy_1 - 25; j <= iy_1 + 25; j++)
            {
                if (j >= 0 && j < dsh->nx)
                {
                    dsh->yd[j] += aiy_1 * fexp[j - iy_1 + 25];
                    dsh->xd[j] += aiy_1;
                }
            }

            iy += 25;

            for (j = iy - 25; j <= iy + 25; j++)
            {
                if (j >= 0 && j < dsh->nx)
                {
                    dsh->yd[j] += aiy * fexp[j - iy + 25];
                    dsh->xd[j] += aiy;
                    np++;
                }
            }
        }
    }

    for (k = 0; np > 0 && k < dsh->nx; k++)
    {
       //dsh->yd[k] *= (float)51 / np;
        dsh->xd[k] = ymin + ((k - 25) * biny / 5);
    }

    if (ops != NULL)
    {
        uns_op_2_op_by_type(ops, IS_Y_UNIT_SET, oph, IS_X_UNIT_SET);
    }

    set_plot_title(oph, "Histogram smooth of %s", (ops != NULL && ops->title != NULL) ? ops->title : "");
    set_plot_y_title(oph, "Probability");
    set_plot_x_title(oph, "%s", (ops != NULL && ops->y_title != NULL) ? ops->y_title : "y extend");

    if (allds == 0)
    {
        update_ds_treatement(dsh, "Histogramm by exponential convolution \\sigma = %g ver 2 of one ds", biny);
    }
    else
    {
        update_ds_treatement(dsh, "Histogramm by exponential convolution \\sigma = %g ver 2 of all ds", biny);
    }

    oph->need_to_refresh = 1;
    return oph;
}

// if dsi != NULL, treat a single data set
// if dsi == NULL and ops != NULL treat all ds of ops
O_p *build_histo_with_exponential_convolution(pltreg *pr, O_p *ops, d_s *dsi, float biny)
{
  return build_histo_with_exponential_convolution_with_y_lim(pr, ops, dsi, biny, 1.0, 0.0);
}



// treat a single data set
d_s *build_histo_ds_with_gaussian_convolution(d_s *dsi, float biny)
{
    int  j, k;
    int  maxh, iy, np, iy_1;
    d_s *dsh = NULL;
    float ymin = 0, ymax = 0, tmp, fiy, aiy, aiy_1;
    static float fexp[51] = {0};
    static int fexp_init = 0;

    if (dsi == NULL || dsi->nx == 0)
    {
            //win_printf("build_histo_with_exponential_convolution\ndsi null!\n");
        return NULL;
    }
    if (fexp_init == 0)
    {
        for (j = 0; j <= 50; j++)
        {
            tmp = ((float)(j - 25)) / 5;
            fexp[j] = exp(-tmp * tmp);
        }
        fexp_init = 1;
    }
    ymin = ymax = dsi->yd[0];

    for (k = 0; k < dsi->nx; k++)
      {
	ymax = (dsi->yd[k] > ymax) ? dsi->yd[k] : ymax;
	ymin = (dsi->yd[k] < ymin) ? dsi->yd[k] : ymin;
      }
    maxh = (int)(0.5 + ((ymax - ymin) * 5) / biny);
    maxh += 50;

    if (maxh < 0 || maxh > (10 * 2e6))
    {
        int ret = win_printf("you are trying to create a very big histogram %d points,"
			     " the process will be abort unless you hit Cancel\n", maxh);
        if (ret != WIN_CANCEL)
            return NULL;
    }
    dsh = build_data_set(maxh, maxh);

    if (dsh == NULL)
    {
            //win_printf("build_histo_with_exponential_convolution\n"
            //	   "dsh null in oph with %d pts!\n", maxh);
        return NULL;
    }
    inherit_from_ds_to_ds(dsh, dsi);
    for (k = 0, np = 0; k < dsi->nx; k++)
      {
	fiy = ((dsi->yd[k] - ymin) * 5) / biny;
	iy_1 = (int)fiy;
	iy = iy_1 + 1;
	aiy = fabs(fiy - iy_1);
	aiy_1 = 1 - aiy;
	iy_1 += 25;

	for (j = iy_1 - 25; j <= iy_1 + 25; j++)
	  {
	    if (j >= 0 && j < dsh->nx)
	      {
		dsh->yd[j] += aiy_1 * fexp[j - iy_1 + 25];
		dsh->xd[j] += aiy_1;
	      }
	  }
	iy += 25;
	for (j = iy - 25; j <= iy + 25; j++)
	  {
	    if (j >= 0 && j < dsh->nx)
	      {
		dsh->yd[j] += aiy * fexp[j - iy + 25];
		dsh->xd[j] += aiy;
		np++;
	      }
	  }
      }
    for (k = 0; np > 0 && k < dsh->nx; k++)
      {
              //dsh->yd[k] *= (float)51/ np;
        dsh->xd[k] = ymin + ((k - 25) * biny / 5);
      }
    update_ds_treatement(dsh, "Histogramm by gaussian convolution \\sigma = %g "
		      "ver 2 of all ds", biny);
    return dsh;
}

int find_max_of_histo_of_ds_with_gaussian_convolution(d_s *dsi, float biny, float *ypos, float *height)
{
    d_s *dsh = NULL;
    int j, jj;
    float xavg, yavg, tmp, zh = 0;

    if (dsi == NULL) return 1;
    dsh = build_histo_ds_with_gaussian_convolution(dsi, biny);
    if (dsh == NULL) return 2;
    if (dsh->nx <= 2)
       {
          free_data_set(dsh);
          return 3;
       }

    for (j = jj = 0, tmp = dsh->yd[j]; j < dsh->nx; j++)
            tmp = (tmp < dsh->yd[j]) ? dsh->yd[jj = j] : tmp;
    int ret = find_max_around(dsh->yd, dsh->nx, jj, &yavg, &zh, NULL);
    /*
    for (jj = dsh->nx - 1; jj > 0; jj--)
      {
         if ((dsh->yd[jj] > dsh->yd[jj - 1]) && (dsh->yd[jj] > dsh->yd[jj + 1])
             && (dsh->yd[jj] > tmp/20))
            {// we have some maximum
               int ret = find_max_around(dsh->yd, dsh->nx, jj, &yavg, &zh, NULL);
               if (ret == 0) break;
            }
      }
    */
    xavg = (dsh->xd[dsh->nx-1] - dsh->xd[0]);
    yavg = dsh->xd[0] + (xavg * yavg)/(dsh->nx-1);
    *ypos = yavg;
    if (height) *height = zh;
    free_data_set(dsh);
    return ret;
}
int find_y_offset_between_2_ds_by_histo(d_s *ds1, d_s *ds2, float biny, float *dy12, float *extend)
{
  int  k;
  int  nf, ki;
  float minx = 0, maxx = 0, yavg, val;
  d_s *ds = NULL;


  if (ds1 == NULL || ds1->nx <= 0) return 1;
  if (ds2 == NULL || ds2->nx <= 0) return 1;

  minx = ds1->xd[0];
  maxx = ds1->xd[ds1->nx-1];
  minx = (ds2->xd[0] < minx) ? ds2->xd[0] : minx;
  maxx = (ds2->xd[ds2->nx-1] > maxx) ? ds2->xd[ds2->nx-1] : maxx;

  nf = (int)(maxx - minx + 1.5);
  if ((ds = build_data_set(nf, nf)) == NULL)
          return 1;
  ds->nx = ds->ny = 0;
  for (k = 0, ki = 0; k < ds1->nx; k++)
    {
       for ( ; ki < ds2->nx && ds2->xd[ki] < ds1->xd[k]; ki++);
       if (ki >= 0 && ki < ds2->nx && (fabs(ds2->xd[ki] - ds1->xd[k]) < 0.5))
          {
             add_new_point_to_ds(ds, ds->nx, ds1->yd[k] - ds2->yd[ki]);
          }
    }
  k = find_max_of_histo_of_ds_with_gaussian_convolution(ds, biny, &yavg, &val);
  if (dy12) *dy12 = yavg;
  if (extend) *extend = val;
  free_data_set(ds);
  return k;
}

// treat a single data set
d_s *build_gaussian_convolution(d_s *dsi, float biny)
{
    int j, k;
    int maxh, iy, np, iy_1;
    d_s *dsh = NULL;
    float ymin = 0, ymax = 0, tmp, fiy, aiy, aiy_1;
    static float fexp[51] = {0};
    static int fexp_init = 0;

    if (dsi == NULL)
    {
        win_printf("build_gaussian_convolution\ndsi null!\n");
        return NULL;
    }
    if (fexp_init == 0)
    {
        for (j = 0; j <= 50; j++)
        {
            tmp = ((float)(j - 25)) / 5;
            fexp[j] = exp(-tmp * tmp);
        }
        fexp_init = 1;
    }
    ymin = ymax = dsi->xd[0];
    for (k = 0; k < dsi->nx; k++)
      {
	ymax = (dsi->xd[k] > ymax) ? dsi->xd[k] : ymax;
	ymin = (dsi->xd[k] < ymin) ? dsi->xd[k] : ymin;
      }
    maxh = (int)(0.5 + ((ymax - ymin) * 5) / biny);
    maxh += 50;
    if (maxh < 0 || maxh > (10 * 2e6))
    {
        int ret = win_printf("you are trying to create a very big histogram %d points,\n"
			     " the process will be abort unless you hit Cancel\n", maxh);
        if (ret != WIN_CANCEL)
            return NULL;
    }
    dsh = build_data_set(maxh, maxh);
    if (dsh == NULL)
    {
        win_printf("build_gaussian_convolution\ncannot create dsh with %d pts!\n"
		   "Ymin %g Ymax %g biny %g\n", maxh, ymax, ymin, biny);
        return NULL;
    }
    inherit_from_ds_to_ds(dsh, dsi);
    for (k = 0, np = 0; k < dsi->nx; k++)
      {
	fiy = ((dsi->xd[k] - ymin) * 5) / biny;
	iy_1 = (int)fiy;
	iy = iy_1 + 1;
	aiy = fabs(fiy - iy_1);
	aiy_1 = 1 - aiy;
	iy_1 += 25;
	for (j = iy_1 - 25; j <= iy_1 + 25; j++)
	  {
	    if (j >= 0 && j < dsh->nx)
	      {
		dsh->yd[j] += aiy_1 * dsi->yd[k] * fexp[j - iy_1 + 25];
		dsh->xd[j] += aiy_1;
	      }
	  }
	iy += 25;
	for (j = iy - 25; j <= iy + 25; j++)
	  {
	    if (j >= 0 && j < dsh->nx)
	      {
		dsh->yd[j] += aiy * dsi->yd[k] * fexp[j - iy + 25];
		dsh->xd[j] += aiy;
		np++;
	      }
	  }
      }
    for (k = 0; np > 0 && k < dsh->nx; k++)
    {
            //dsh->yd[k] *= (float)51 / np;
        dsh->xd[k] = ymin + ((k - 25) * biny / 5);
    }

    update_ds_treatement(dsh, "Histogramm by exponential convolution \\sigma = %g "
		      "ver 2 of all ds", biny);
    return dsh;
}


O_p *build_histo_with_exponential_convolution_norm_for_each_ds(pltreg *pr, O_p *ops, float biny)
{
    int  j, k;
    int       maxh, iy, np, iy_1, ids, nds;
    d_s *dsh = NULL, *dst = NULL;
    O_p *oph = NULL;
    float ymin = 0, ymax = 0, tmp, fiy, aiy, aiy_1;
    static float fexp[51] = {0};
    static int fexp_init = 0;

    if (ops == NULL)
    {
        return NULL;
    }

    if (fexp_init == 0)
    {
        for (j = 0; j <= 50; j++)
        {
            tmp = ((float)(j - 25)) / 5;
            fexp[j] = exp(-tmp * tmp);
        }

        fexp_init = 1;
    }

    nds = ops->n_dat;

    for (ids = 0; ids < nds; ids++)
    {
        dst = ops->dat[ids];

        if (ids == 0)
        {
            ymin = ymax = dst->yd[0];
        }

        for (k = 0; k < dst->nx; k++)
        {
            ymax = (dst->yd[k] > ymax) ? dst->yd[k] : ymax;
            ymin = (dst->yd[k] < ymin) ? dst->yd[k] : ymin;
        }
    }

    maxh = (int)(0.5 + ((ymax - ymin) * 5) / biny);
    maxh += 50;
    oph = (pr != NULL) ? create_and_attach_one_plot(pr, maxh, maxh, 0) : create_one_plot(maxh, maxh, 0);

    if (oph == NULL)
    {
        return NULL;
    }

    //dsh = create_and_attach_one_ds(ops, maxh, maxh, 0);
    dsh = oph->dat[0];

    if (dsh == NULL)
    {
        return NULL;
    }

    for (ids = 0, np = 0; ids < nds; ids++)
    {
        dst = ops->dat[ids];

        if (ids == 0)
        {
            inherit_from_ds_to_ds(dsh, dst);
        }

        for (k = 0; k < dst->nx; k++)
        {
            fiy = ((dst->yd[k] - ymin) * 5) / biny;
            iy_1 = (int)fiy;
            iy = iy_1 + 1;
            aiy = fabs(fiy - iy_1);
            aiy_1 = 1 - aiy;
            aiy /= (dst->nx > 0) ? dst->nx : 1;
            aiy_1 /= (dst->nx > 0) ? dst->nx : 1;
            iy_1 += 25;

            for (j = iy_1 - 25; j <= iy_1 + 25; j++)
            {
                if (j >= 0 && j < dsh->nx)
                {
                    dsh->yd[j] += aiy_1 * fexp[j - iy_1 + 25];
                    dsh->xd[j] += aiy_1;
                }
            }

            iy += 25;

            for (j = iy - 25; j <= iy + 25; j++)
            {
                if (j >= 0 && j < dsh->nx)
                {
                    dsh->yd[j] += aiy * fexp[j - iy + 25];
                    dsh->xd[j] += aiy;
                    np++;
                }
            }
        }
    }

    for (k = 0; np > 0 && k < dsh->nx; k++)
    {
        //dsh->yd[k] *= (float)51 / np;
        dsh->xd[k] = ymin + ((k - 25) * biny / 5);
    }

    if (ops != NULL)
    {
        uns_op_2_op_by_type(ops, IS_Y_UNIT_SET, oph, IS_X_UNIT_SET);
    }

    set_plot_title(oph, "Histogram smooth of %s", (ops != NULL && ops->title != NULL) ? ops->title : "");
    set_plot_y_title(oph, "Probability");
    set_plot_x_title(oph, "%s", (ops != NULL && ops->y_title != NULL) ? ops->y_title : "y extend");
    update_ds_treatement(dsh, "Histogramm by exponential convolution \\sigma = %g ver 2 of all ds, each ds normalized to 1",
                      biny);
    oph->need_to_refresh = 1;
    return oph;
}


# ifdef PB
// if dsi != NULL, treat a single data set
// if dsi == NULL and ops != NULL treat all ds of ops
// float the gaussian is computed from error bars if they exist or from sig otherwise
O_p *build_histo_with_gaussian_convolution_from_error(pltreg *pr, O_p *ops, d_s *dsi, float sig)
{
    int  j, k;
    int       maxh, iy, np, iy_1, allds, ids, nds;
    d_s *dsh = NULL, *dst = NULL;
    O_p *oph = NULL;
    float ymin = 0, ymax = 0, tmp, fiy, aiy, aiy_1, biny, max_er, amp;
    static float fexp[51] = {0};
    static int fexp_init = 0;

    if (dsi == NULL && ops == NULL)
    {
        return NULL;
    }

    allds = (dsi == NULL && ops != NULL) ? 1 : 0;

    if (fexp_init == 0)
    {
        for (j = 0; j <= 50; j++)
        {
            tmp = ((float)(j - 25)) / 5;
            fexp[j] = exp(-tmp * tmp);
        }

        fexp_init = 1;
    }

    nds = (allds) ? ops->n_dat : 1;

    for (ids = 0, max_er = sig; ids < nds; ids++)
    {
        dst = (allds) ? ops->dat[ids] : dsi;

        if (ids == 0)
        {
            ymin = ymax = dst->yd[0];
        }

        for (k = 0; k < dst->nx; k++)
        {
            if (dst->yd[k] > ymax)
            {
                ymax = dst->yd[k];
            }

            if (dst->yd[k] < ymin)
            {
                ymin = dst->yd[k];
            }

            if (dst->ye != NULL)
            {
                max_er = (dst->ye[k] > max_er) ? (dst->ye[k]) : max_er;
            }
        }
    }

    maxh = (int)(0.5 + ((ymax - ymin) * 5) / max_er);
    maxh += 50;

    if (pr != NULL)
    {
        oph = create_and_attach_one_plot(pr, maxh, maxh, 0);
    }
    else
    {
        oph = create_one_plot(maxh, maxh, 0);
    }

    if (oph == NULL)
    {
        return NULL;
    }

    //dsh = create_and_attach_one_ds(ops, maxh, maxh, 0);
    dsh = oph->dat[0];

    if (dsh == NULL)
    {
        return NULL;
    }

    for (ids = 0, np = 0; ids < nds; ids++)
    {
        dst = (allds) ? ops->dat[ids] : dsi;

        if (ids == 0)
        {
            inherit_from_ds_to_ds(dsh, dst);
        }

        for (k = 0; k < dst->nx; k++)
        {
            biny = (dst->ye != NULL && dst->ye[k] > 0) ?  dst->ye[k] : sig;
            amp = (float) / biny;
            fiy = ((dst->yd[k] - ymin) * 5) / biny;
            iy_1 = (int)fiy;
            iy = iy_1 + 1;
            aiy = fabs(fiy - iy_1);
            aiy_1 = 1 - aiy;
            iy_1 += 25;

            for (j = iy_1 - 25; j <= iy_1 + 25; j++)
            {
                if (j >= 0 && j < dsh->nx)
                {
                    dsh->yd[j] += amp * aiy_1 * fexp[j - iy_1 + 25];
                    dsh->xd[j] += aiy_1;
                }
            }

            iy += 25;

            for (j = iy - 25; j <= iy + 25; j++)
            {
                if (j >= 0 && j < dsh->nx)
                {
                    dsh->yd[j] += amp * aiy * fexp[j - iy + 25];
                    dsh->xd[j] += aiy;
                    np++;
                }
            }
        }
    }// PBBBBBBB !

    for (k = 0; np > 0 && k < dsh->nx; k++)
    {
        dsh->yd[k] *= (float)51 / np;
        dsh->xd[k] = ymin + ((k - 25) * biny / 5);
    }

    if (ops != NULL)
    {
        uns_op_2_op_by_type(ops, IS_Y_UNIT_SET, oph, IS_X_UNIT_SET);
    }

    set_plot_title(oph, "Histogram smooth of %s", (ops != NULL && ops->title != NULL) ? ops->title : "");
    set_plot_y_title(oph, "Probability");
    set_plot_x_title(oph, "%s", (ops != NULL && ops->y_title != NULL) ? ops->y_title : "y extend");

    if (allds == 0)
    {
        update_ds_treatement(dsh, "Histogramm by exponential convolution \\sigma = %g ver 2 of one ds", biny);
    }
    else
    {
        update_ds_treatement(dsh, "Histogramm by exponential convolution \\sigma = %g ver 2 of all ds", biny);
    }

    oph->need_to_refresh = 1;
    return oph;
}

# endif


int do_build_histo_with_exponential_convolution_norm_for_each_ds(void)
{
    int i;
    pltreg  *pr = NULL;
    O_p     *ops = NULL;
    static float biny = 1;
    char question[512] = {0};

    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine compute an histogram in Y of the current dataset\n"
                             "Each point is convoluted by a Gaussian");
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &ops) != 2)
    {
        return win_printf_OK("cannot find data!");
    }

    snprintf(question, sizeof(question), "Histogramm by Gaussian convolution\n"
             "Define \\sigma of exponential %%12f\n"
             "Treating all datasets normalizing each to max = 1");
    i = win_scanf(question, &biny);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    build_histo_with_exponential_convolution_norm_for_each_ds(pr, ops, biny);
    return refresh_plot(pr, UNCHANGED);
}



int do_build_histo_with_exponential_convolution(void)
{
    int i;
    pltreg  *pr = NULL;
    O_p     *ops = NULL;
    d_s     *dsi = NULL;
    static float biny = 1;
    static int Nds = 25, overl = 12, all = 0;
    char question[512] = {0};
    //static unsigned long t_c, to = 0;

    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine compute an histogram in Y of the current dataset\n"
                             "Each point is convoluted by a Gaussian");
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &ops, &dsi) != 3)
    {
        return win_printf_OK("cannot find data!");
    }

    snprintf(question, sizeof(question), "Histogramm by Gaussian convolution\n"
             "Define \\sigma of exponential %%12f\n"
             "%%R->Treat current dataset\n%%r-> or all of them in one histo\n"
	     "%%r->or all datasets in a slidding window of N in one histo\n"
	     "then specify the window size N = %%5d and the overlap %%4d\n");
    i = win_scanf(question, &biny, &all,&Nds,&overl);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    //t_c = my_uclock();
    //build_histo_with_exponential_convolution_2(pr, ops, dsi, biny);
    //t_c = my_uclock() - t_c;
    //to = my_uclock();
    O_p *oph = NULL, *oph0 = NULL;
    d_s **orgds = NULL, *dsh = NULL, *dst = NULL;
    int totds = 0, j, ids, k;
    float ymin = 0, ymax = 0, tmp;

    for (ids = 0; ids < ops->n_dat; ids++)
      {
	dst = ops->dat[ids];
	if (ids == 0)
	  {
	    ymin = ymax = dst->yd[0];
	  }
	for (k = 0; k < dst->nx; k++)
	  {
	    if (dst->yd[k] > ymax)
	      {
		ymax = dst->yd[k];
	      }

	    if (dst->yd[k] < ymin)
	      {
		ymin = dst->yd[k];
	      }
	  }
      }
    tmp = ymax - ymin;
    ymin -= tmp/50;
    ymax += tmp/50;


    if (all < 2)
      {
	build_histo_with_exponential_convolution(pr, ops, ((all) ? NULL : dsi), biny);
      }
    else
      {
	//win_printf("sliding win");
	totds = ops->n_dat;
	orgds = ops->dat;
	ops->n_dat = Nds;
	for (j = 0; j < totds - Nds; j += overl)
	  {
	    ops->dat = orgds + j;
	    if (j == 0)
	      {
		oph0 = build_histo_with_exponential_convolution_with_y_lim(pr, ops, ((all) ? NULL : dsi), biny, ymin, ymax);
	      }
	    else
	      {
		//win_printf("treating %d to %d");
		oph = build_histo_with_exponential_convolution_with_y_lim(NULL, ops, ((all) ? NULL : dsi), biny, ymin, ymax);
		if (oph != NULL)
		  {
		    //win_printf("after treating %d to %d");
		    dsh = duplicate_data_set(oph->dat[0],NULL);
		    if (add_data_to_one_plot(oph0, IS_DATA_SET, (void*)dsh))
		      return win_printf("Could not add data set");
		    free_one_plot(oph);
		    oph = NULL;
		  }
	      }
	  }
	ops->n_dat = totds;
	ops->dat = orgds;
      }

    //to = my_uclock() - to;
    //win_printf("hiso expo %g ms, histo 2 %g ms", 1000*(double)t_c/get_my_uclocks_per_sec()
    //         , 1000*(double)to/get_my_uclocks_per_sec());
    return refresh_plot(pr, UNCHANGED);
}

int adjust_ds_mean_y_according_to_visible_points(void)
{
    int i, j, k;
    int min, n_start, n_end;
    pltreg *pr = NULL;
    d_s *ds = NULL;
    O_p *op = NULL;
    static double pos = 0, tmp, tmpu, tmp2;
    char question[1024] = {0};
    static int adj_type = 0, ds_type = 0, ndss = 0, ndse = 1;
    double highest_visible_pts_ds_mean = -DBL_MAX;
    double highest_visible_pts_ds_meanU = -DBL_MAX;
    d_s *curds = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)
    {
        return D_O_K;
    }

    min = (ds->nx > ds->ny) ? ds->nx : ds->ny;

    for (i = min - 1, j = 0, tmp = 0; i >= 0 ; i--)
    {
        if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo
                && ds->yd[i] < op->y_hi && ds->yd[i] >= op->y_lo)
        {
            tmp += ds->yd[i];
            j++;
        }
    }

    if (j)
    {
        tmp /= j;
    }
    double meanall = 0, meanallU = 0;
    int nmeanall = 0;
    for (int l = 0; l < op->n_dat; ++l)
    {
        int npts = 0;
        double curvalsum = 0;
        curds = op->dat[l];
        min = (curds->nx > curds->ny) ? curds->nx : curds->ny;
        for (i = min - 1, j = 0, curvalsum = 0; i >= 0 ; i--)
        {
            if (curds->xd[i] < op->x_hi && curds->xd[i] >= op->x_lo
                    && curds->yd[i] < op->y_hi && curds->yd[i] >= op->y_lo)
            {
                curvalsum += curds->yd[i];
                npts++;
            }
        }
        nmeanall += npts;
        meanall += curvalsum;
        if (npts)
        {
            curvalsum /= npts;
            if (curvalsum > highest_visible_pts_ds_mean)
            {
                highest_visible_pts_ds_mean = curvalsum;
            }
        }
    }
    meanall = (nmeanall > 0) ? meanall/nmeanall : meanall;
    highest_visible_pts_ds_meanU =  op->ay + highest_visible_pts_ds_mean * op->dy;
    tmpu = op->ay + tmp * op->dy;
    meanallU = op->ay + meanall * op->dy;
    snprintf(question, sizeof(question), "I propose to shift dataset(s) along Y in such\n"
             "a way that the mean value in Y of their {\\color{yellow}visible points}\n"
             "equals to:\n%%R the value %8g of the current data set (%d),\n"
             "%%r the value %8g of the highest visible dataset point mean,\n"
             "%%r the value %8g of the mean of all visible dataset points,\n"
             "%%r 0,\n%%r this value %%8lf,\n"
             "Apply the process to:\n%%R all ds,\n%%r only the current one,\n"
             "%%r ds in the range: [ %%8d,%%8d[ (max %d)\n"
             "(Datasets with no visible points are not affected)."
             , tmpu, op->cur_dat, highest_visible_pts_ds_meanU, meanallU, op->n_dat);
    i = win_scanf(question, &adj_type, &pos, &ds_type, &ndss, &ndse);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

// win_printf("index = %d = %x\ni will work on datasets in [%d , %d[", index, index, n_start, n_end);
    op->need_to_refresh = 1;

    if (ds_type == 0)
    {
        n_start = 0;
        n_end = op->n_dat;
    }
    else if (ds_type == 1)
    {
        n_start = op->cur_dat;
        n_end = op->cur_dat + 1;
    }
    else if (ds_type == 2)
    {
        n_start = (ndss < 0) ? 0 : ndse;
        n_end = (ndse > op->n_dat) ? op->n_dat : ndse;

        if (n_start > n_end)
        {
            return win_printf_OK("Wrong dataset selection\nmin %d max %d not in [0,%d[!", ndss, ndse, op->n_dat);
        }
    }
    else
    {
        return win_printf_OK("Wrong dataset selection option! ds_type = %d", ds_type);
    }

    if (adj_type == 0)
    {
        tmpu = 0;
    }
    else if (adj_type == 1)
    {
        tmp = highest_visible_pts_ds_mean;
    }
    else if (adj_type == 2)
    {
        tmp = meanallU;
    }
    else if (adj_type == 4)
    {
        tmp = pos - op->ay;

        if (op->dy > 0)
        {
            tmp /= op->dy;
        }
    }
    else if (adj_type == 3)
    {
        tmp = 0;
    }
    else
    {
        return win_printf_OK("Wrong adjusting type  selection! adj_type = %d", adj_type);
    }

    for (k = n_end - 1; k >= n_start; k--)
    {
        ds = op->dat[k]; // we work on dataset k
        min = (ds->nx > ds->ny) ? ds->nx : ds->ny;

        for (i = min - 1, j = 0, tmp2 = 0 ; i >= 0 ; i--)
        {
            if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo
                    && ds->yd[i] < op->y_hi && ds->yd[i] >= op->y_lo)
            {
                tmp2 += ds->yd[i];
                j++;
            }
        }

        if (j)  // there are visible points
        {
            tmp2 /= j;

            for (i = min - 1; i >= 0 ; i--)
            {
                ds->yd[i] += tmp - tmp2;
            }

            update_ds_treatement(ds, "Data shifted in Y by %g", tmp - tmp2);
            //win_printf("Data set %d shifted in Y by %g",k,tmp-tmp2);
        }
    }

    return refresh_plot(pr, UNCHANGED);
}

d_s *compute_difference_in_Y_for_ds_by_index(O_p *opd, d_s *dsi1, d_s *dsi2)
{
    int i;
    int nx;
    d_s *dsd = NULL;

    if (dsi1 == NULL || dsi2 == NULL)
    {
        return NULL;
    }

    nx = (dsi1->nx > dsi2->nx) ? dsi2->nx : dsi1->nx;

    if (opd != NULL)
    {
        dsd = create_and_attach_one_ds(opd, nx, nx, 0);
    }
    else
    {
        dsd = build_data_set(nx, nx);
    }

    if (dsd == NULL)
    {
        return NULL;
    }

    for (i = 0; i < dsd->nx; i++)
    {
        dsd->xd[i] = (dsi1->xd[i] + dsi2->xd[i]) / 2;
        dsd->yd[i] = dsi1->yd[i] - dsi2->yd[i];
    }

    inherit_from_ds_to_ds(dsd, dsi1);
    update_ds_treatement(dsd, "diference between data set");
    return dsd;
}

d_s *compute_difference_in_Y_for_ds_with_ordered_integer_x(O_p *opd, d_s *dsi1, d_s *dsi2)
{
    int  j, k;
    int nx;
    d_s *dsd = NULL;
    float x;

    if (dsi1 == NULL || dsi2 == NULL)
    {
        return NULL;
    }

    nx = (dsi1->nx > dsi2->nx) ? dsi2->nx : dsi1->nx;

    if (opd != NULL)
    {
        dsd = create_and_attach_one_ds(opd, nx, nx, 0);
    }
    else
    {
        dsd = build_data_set(nx, nx);
    }

    if (dsd == NULL)
    {
        return NULL;
    }

    dsd->nx = dsd->ny = 0;
    x = (dsi1->xd[0] < dsi2->xd[0]) ? dsi2->xd[0] : dsi1->xd[0];

    for (j = k = 0; j < dsi1->nx && k < dsi2->nx;)
    {
        for (; j < dsi1->nx && (dsi1->xd[j] < x); j++);

        if (j >= dsi1->nx)
        {
            break;
        }

        for (; k < dsi1->nx && (dsi2->xd[k] < x); k++);

        if (k >= dsi2->nx)
        {
            break;
        }

        if (dsi1->xd[j] == dsi2->xd[k])
        {
            add_new_point_to_ds(dsd, x = dsi1->xd[j], dsi1->yd[j] - dsi2->yd[k]);
        }

        if (x == dsi1->xd[j])
        {
            j++;

            if (j >= dsi1->nx)
            {
                break;
            }
        }

        if (x == dsi2->xd[k])
        {
            k++;

            if (k >= dsi2->nx)
            {
                break;
            }
        }

        x = (dsi1->xd[j] < dsi2->xd[k]) ? dsi1->xd[j] : dsi2->xd[k];
    }

    inherit_from_ds_to_ds(dsd, dsi1);
    update_ds_treatement(dsd, "difference between data set at same x on ordered x axis");
    return dsd;
}
int find_index_of_max_in_y(d_s *dsi)
{
    int i, imax;
    float max;

    if (dsi == NULL || dsi->yd == NULL)
    {
        return -1;
    }

    for (i = imax = 0, max = dsi->yd[0]; i < dsi->nx; i++)
    {
        max = (dsi->yd[i] > max) ? dsi->yd[imax = i] : max;
    }

    return imax;
}

int find_index_of_max_in_y_in_x_range(O_p *op, d_s *dsi, float low, float hi)
{
    int i, imax;
    float max;

    if (dsi == NULL || dsi->yd == NULL)
    {
        return -1;
    }

    if (op != NULL)
    {
        low -= op->ax;

        if (op->dx != 0)
        {
            low /= op->dx;
        }

        hi -= op->ax;

        if (op->dx != 0)
        {
            hi /= op->dx;
        }
    }

    for (i = 0, imax = -1; i < dsi->nx; i++)
    {
        if (dsi->xd[i] >= low  && dsi->xd[i] < hi)
        {
            if (imax < 0)
            {
                max = dsi->yd[imax = i];
            }

            max = (dsi->yd[i] > max) ? dsi->yd[imax = i] : max;
        }
    }

    return imax;
}

int find_max_in_y_in_x_range(O_p *op, d_s *dsi, float low, float hi, float *Max_pos, float *Max_val, float *width)
{
    int imax, nxh, ret;//, debug = 0;
    float tmpo, tmp, tmpv, tmpd, w;
    char st[1024] = {0};
    imax = find_index_of_max_in_y_in_x_range(op, dsi, low, hi);

    if (imax < 0)
    {
        return -1;
    }

    nxh = dsi->nx;
    ret = find_max_around(dsi->yd, nxh, imax, &tmp, &tmpv, &tmpd);

    if (ret != 0)
    {
        return -1;
    }

    tmpo = tmp;
    w = tmpv;

    if (tmpd != 0)
    {
        w /= tmpd;
    }

    w = sqrt(fabs(w));
    //if (debug++ < 5) win_printf("Amp %g deriv %g w %g",tmpv,tmpd,w);
    tmp = dsi->xd[0] + (tmp * (dsi->xd[nxh - 1] - dsi->xd[0]) / (nxh - 1));

    if (op != NULL)
    {
        tmp = op->ax + tmp * op->dx;
    }

    if (Max_pos)
    {
        *Max_pos = tmp;
    }

    if (Max_val)
    {
        *Max_val = tmpv;
    }

    w = w * (dsi->xd[nxh - 1] - dsi->xd[0]) / (nxh - 1);

    if (op != NULL)
    {
        w = w * op->dx;
    }

    if (width)
    {
        *width = w;
    }

    snprintf(st, sizeof(st), "\\fbox{\\pt8\\stack{{looking for max in [%g,%g]}"
            "{imax =  %d, nxh = %d}{max = %g (%g)}{ret = %d}}}", low, hi, imax, nxh, tmpo, tmp, ret);
    set_ds_plot_label(dsi, 1, 1, USR_COORD, "%s",st);
    return 0;
}



int adjust_ds_mean_y_for_sparse_signal(void)
{
    int i, j, k;
    int min, n_start, n_end, ids1, imax, ret, nxh;
    pltreg *pr = NULL;
    d_s *ds = NULL, *dsi = NULL, *dst = NULL;
    O_p *op = NULL, *oph = NULL;
    static float biny = 0.1;
    static double tmp, tmpu;
    float Max_pos, Max_val;
    char question[1024] = {0};
    static int  ds_type = 0, ndss = 0, ndse = 1, verbose = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi) != 3)
    {
        return D_O_K;
    }

    min = (dsi->nx > dsi->ny) ? dsi->nx : dsi->ny;

    for (i = min - 1, j = 0, tmp = 0; i >= 0 ; i--)
    {
        tmp += dsi->yd[i];
        j++;
    }

    if (j)
    {
        tmp /= j;
    }

    tmpu = op->ay + tmp * op->dy;
    snprintf(question, sizeof(question), "I propose to shift dataset(s) along Y in such\n"
             "a way that the most probable value in Y \n"
             "equals to:\n the value %8g of the current data set (%d),\n"
             "Apply the process to:\n%%R all other ds,\n"
             "%%r to ds in the range: [ %%8d,%%8d[ (max %d)\n"
             "Specify the bin size for the histogram %%8f\n"
             "Verbose %%b\n"
             , tmpu, op->cur_dat, op->n_dat);
    i = win_scanf(question, &ds_type, &ndss, &ndse, &biny, &verbose);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    ids1 = op->cur_dat;
// win_printf("index = %d = %x\ni will work on datasets in [%d , %d[", index, index, n_start, n_end);
    op->need_to_refresh = 1;

    if (ds_type == 0)
    {
        n_end = op->n_dat;
        n_start = 0;
    }
    else if (ds_type == 1)
    {
        n_end = (ndse > op->n_dat) ? op->n_dat : ndse;
        n_start = (ndss < 0) ? 0 : ndse;

        if (n_start > n_end)
        {
            return win_printf_OK("Wrong dataset selection\nmin %d max %d not in [0,%d[!", ndss, ndse, op->n_dat);
        }
    }
    else
    {
        return win_printf_OK("Wrong dataset selection option!");
    }

    for (k = n_end - 1; k >= n_start; k--)
    {
        ds = op->dat[k]; // we work on dataset k

        if (k == ids1)
        {
            continue;
        }

        dst = compute_difference_in_Y_for_ds_with_ordered_integer_x(NULL, ds, dsi);

        if (verbose)
        {
            verbose = (win_printf("Diff compute for ds %d", k) == WIN_CANCEL) ? 0 : 1;
        }

        if (dst == NULL)
        {
            continue;
        }

        oph = build_histo_with_exponential_convolution(NULL, op, dst, biny);

        if (verbose)
        {
            verbose = (win_printf("Gaussian hist compute for ds %d", k) == WIN_CANCEL) ? 0 : 1;
        }

        if (oph == NULL)
        {
            continue;
        }

        imax = find_index_of_max_in_y(oph->dat[0]);

        if (verbose)
        {
            verbose = (win_printf("Max index of histo found for ds %d", k) == WIN_CANCEL) ? 0 : 1;
        }

        if (imax < 0)
        {
            continue;
        }

        nxh = oph->dat[0]->nx;
        ret = find_max_around(oph->dat[0]->yd, nxh, imax, &Max_pos, &Max_val, NULL);

        if (verbose)
        {
            verbose = (win_printf("Max of histo found for ds %d", k) == WIN_CANCEL) ? 0 : 1;
        }

        if (ret != 0)
        {
            continue;
        }

        tmp = oph->dat[0]->xd[0] + (Max_pos * (oph->dat[0]->xd[nxh - 1] - oph->dat[0]->xd[0]) / (nxh - 1));

        for (i = 0; i < ds->nx; i++)
        {
            ds->yd[i] -= tmp;
        }

        update_ds_treatement(ds, "Data shifted in Y by %g", -tmp);

        if (verbose)
        {
            verbose = (win_printf("Data shifted by %g for ds %d", -tmp, k) == WIN_CANCEL) ? 0 : 1;
        }

        free_one_plot(oph);
        free_data_set(dst);
    }

    return refresh_plot(pr, UNCHANGED);
}

int find_max_of_distribution_by_histo(d_s *dst, O_p *ops, float biny, float *Max)
{
    O_p *oph = NULL;
    int imax, nxh;
    float Max_pos = 0, Max_val = 0, tmp = 0;
    oph = build_histo_with_exponential_convolution(NULL, ops, dst, biny);

    if (oph == NULL)
    {
        return 1;
    }

    imax = find_index_of_max_in_y(oph->dat[0]);

    if (imax < 0)
    {
        free_one_plot(oph);
        return 2;
    }

    nxh = oph->dat[0]->nx;

    if (find_max_around(oph->dat[0]->yd, nxh, imax, &Max_pos, &Max_val, NULL))
    {
        free_one_plot(oph);
        return 3;
    }

    tmp = oph->dat[0]->xd[0] + (Max_pos * (oph->dat[0]->xd[nxh - 1] - oph->dat[0]->xd[0]) / (nxh - 1));

    if (Max)
    {
        *Max = tmp;
    }

    free_one_plot(oph);
    return 0;
}

int find_ds_median_in_y(d_s *dst, float *Max)
{
    int i, nt;
    float *y = NULL;
    if (dst == NULL || Max == NULL || dst->nx < 1 || dst->ny < 1) return 1;
    nt = (dst->nx < dst->ny) ? dst->nx : dst->ny;
    y = (float*)calloc(nt, sizeof(float));
    if (y == NULL) return 2;
    for (i = 0; i < nt; i++) y[i] = dst->yd[i];
    QuickSort(y, 0, nt-1);
    *Max = (nt%2 == 0) ? (y[nt/2] + y[1+(nt/2)])/2 : y[nt/2];
    if (y) free(y);
    return 0;
}
int find_ds_median_in_x(d_s *dst, float *Max)
{
    int i, nt;
    float *y = NULL;
    if (dst == NULL || Max == NULL || dst->nx < 1 || dst->ny < 1) return 1;
    nt = (dst->nx < dst->ny) ? dst->nx : dst->ny;
    y = (float*)calloc(nt, sizeof(float));
    if (y == NULL) return 2;
    for (i = 0; i < nt; i++) y[i] = dst->xd[i];
    QuickSort(y, 0, nt-1);
    *Max = (nt%2 == 0) ? (y[nt/2] + y[1+(nt/2)])/2 : y[nt/2];
    if (y) free(y);
    return 0;
}


int find_max_of_distribution_by_median(d_s *dst, O_p *ops, float *Max)
{
    d_s *dso = NULL;

    if (dst == NULL || Max == NULL) return 1;
    dso = duplicate_data_set(dst, NULL);
    if (dso == NULL) return 2;
    sort_ds_along_y(dso);
    *Max = (dso->nx%2 == 0) ? (dso->yd[dso->nx/2] + dso->yd[1+(dso->nx/2)])/2 : dso->yd[dso->nx/2];
    if (ops) *Max = ops->ay + *Max * ops->dy;
    if (dso) free_data_set(dso);
    return 0;
}




# ifdef ENCOURS
int adjust_ds_mean_y_according_to_visible_points_by_scaling(void)
{
    int i, j, k;
    int min, n_start, n_end;
    pltreg *pr = NULL;
    d_s *ds = NULL;
    O_p *op = NULL;
    static double pos = 0, tmp, tmpu, tmp2;
    char question[1024] = {0};
    static int adj_type = 0, ds_type = 0, ndss = 0, ndse = 1;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)
    {
        return D_O_K;
    }

    min = (ds->nx > ds->ny) ? ds->nx : ds->ny;

    for (i = min - 1, j = 0, tmp = 0; i >= 0 ; i--)
    {
        if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo
                && ds->yd[i] < op->y_hi && ds->yd[i] >= op->y_lo)
        {
            tmp += ds->yd[i];
            j++;
        }
    }

    if (j)
    {
        tmp /= j;
    }

    tmpu = op->ay + tmp * op->dy;
    snprintf(question, sizeof(question), "I propose to scale dataset(s) in Y in such\n"
             "a way that the mean value in Y of their {\\color{yellow}visible points}\n"
             "equals to:\n%%R the value %8g of the current data set (%d),\n"
             "%%r this value %%8lf\n"
             "Apply the process to:\n%%R all ds,\n%%r only the cuurent one,\n"
             "%%r ds in the range: [ %%8d,%%8d[ (max %d)\n"
             "(Datasets with no visible points are not affected).", tmpu, op->cur_dat, op->n_dat);
    i = win_scanf(question, &adj_type, &pos, &ds_type, &ndss, &ndse);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

// win_printf("index = %d = %x\ni will work on datasets in [%d , %d[", index, index, n_start, n_end);
    op->need_to_refresh = 1;

    if (ds_type == 0)
    {
        n_end = op->n_dat;
        n_start = 0;
    }
    else if (ds_type == 1)
    {
        n_end = op->cur_dat + 1;
        n_start = op->cur_dat;
    }
    else if (ds_type == 2)
    {
        n_end = (ndse > op->n_dat) ? op->n_dat : ndse;
        n_start = (ndss < 0) ? 0 : ndse;

        if (n_start > n_end)
        {
            return win_printf_OK("Wrong dataset selection\nmin %d max %d not in [0,%d[!", ndss, ndse, op->n_dat);
        }
    }
    else
    {
        return win_printf_OK("Wrong dataset selection option!");
    }

    if (adj_type == 0)
    {
        tmpu = 0;
    }
    else if (adj_type == 1)
    {
        tmp = pos - op->ay;

        if (op->dy > 0)
        {
            tmp /= op->dy;
        }
    }
    else
    {
        return win_printf_OK("Wrong adjusting type  selection!");
    }

    for (k = n_end - 1; k >= n_start; k--)
    {
        ds = op->dat[k]; // we work on dataset k
        min = (ds->nx > ds->ny) ? ds->nx : ds->ny;

        for (i = min - 1, j = 0, tmp2 = 0 ; i >= 0 ; i--)
        {
            if (ds->xd[i] < op->x_hi && ds->xd[i] >= op->x_lo
                    && ds->yd[i] < op->y_hi && ds->yd[i] >= op->y_lo)
            {
                tmp2 += ds->yd[i];
                j++;
            }
        }

        if (j)  // there are visible points
        {
            tmp2 /= j;

            for (i = min - 1; i >= 0 ; i--)
            {
                ds->yd[i] += tmp - tmp2;
            }

            update_ds_treatement(ds, "Data shifted in Y by %g", tmp - tmp2);
            //win_printf("Data set %d shifted in Y by %g",k,tmp-tmp2);
        }
    }

    return refresh_plot(pr, UNCHANGED);
}
# endif

# ifdef ENCOURS

/*  compute correlation of two arrays in Y over a restricted extend in index
    starting at xcor_start and ending at xcor_start + xcor_size of the first array

 */

int find_nearest_autocorrelation_maximum_in_Y_by_index(d_s *ds1, int xcor_start, int xcor_size, d_s *ds2, int no_dc,
        int periodic)
{
    int i, j;
    int min1, min2, xs1, xe1, xs2, xe2, nxc;
    float m1, m2, max;
    static float *cor = NULL;
    static int cor_size = -1;
    min1 = ds1->nx;
    min1 = (ds1->ny < min1) ? ds1->ny : min1;
    min2 = ds2->nx;
    min2 = (ds2->ny < min2) ? ds2->ny : min2;

    for (j = 0, m1 = 0; j < min1 && no_dc > 0; j++)
    {
        m1 += ds1->yd[j];
    }

    if (min1 > 0)
    {
        m1 /= min1;
    }

    for (j = 0, m2 = 0; j < min2 && no_dc > 0; j++)
    {
        m2 += ds2->yd[j];
    }

    if (min2 > 0)
    {
        m2 /= min2;
    }

    xs1 = xcor_start;
    xs1 = (xs1 > min1) ? min1 : xs1;
    xe1 = min1 - xcor_start - xcor_size;
    xe1 = (xe1 > xcor_start) ? xcor_start : xe1;
    xe2 = min2 - xcor_start - xcor_size;
    xe2 = (xe2 > xcor_start) ? xcor_start : xe2;
    xe1 = (xe2 < xe1) ? xe2 : xe1;

    for (j = 1; j < (xs1 + xe1); j *= 2);

    if (cor == NULL || cor_size < j)
    {
        cor = (float)calloc(j, sizeof(float));

        if (cor == NULL)
        {
            return 1;
        }

        cor_size = j;
    }

    for (i = -xs1; i < xe1; i++)
    {
        xs2 = xcor_start + i;
        xs2 = (xs2 < 0) ? 0 : xs2
              xs2 = (xs2 > min2) ? min2 : xs2;
        xe2 = xcor_start + xcor_size + i;
        xe2 = (xe2 < 0) ? 0 : xe2
              xe2 = (xe2 > min2) ? min2 : xe2;

        for (j = xs2; j < xe2; j++)
        {
            cor[i + xcor_start] += (dsi->yd[j - i] - m1) * (dsi2->yd[j] - m2) ;
        }
    }

    nxc = xe1 + xs1;

    for (j = 0, i = 0, max = cor[0]; j < nxc; j++)
    {
        if (cor[j] > max)
        {
            max = cor[i = j];
        }
    }

    if ((cor[j] > cor[j - 1]) && (cor[k] > cor[j + 1]) && (cor[j] > thres))
    {
        ret = find_max_around(cor, nxc, j, &Max_pos, &Max_val, NULL);

        if (ret == 0)
        {
            tmp = (dsh->yd[k] > 0) ? dsh->xd[k] / dsh->yd[k] : dsh->xd[k];
            add_new_point_to_ds(dsd, tmp, ymin + (Max_pos - 25)*biny / 5);
        }
    }
}




for (i = nf - 1; i >= 0 && norm > 0 && ds->yd[0] > 0; i--)
{
    ds->yd[i] /= ds->yd[0];
}




}


# endif




d_s *compute_correlated_drift_in_Y_for_ds_with_ordered_integer_x(O_p *opi, O_p *opd, float biny)
{
    int i, j;
    int nx, *i_1 = NULL, *i0 = NULL, imin, nds, nxh, imax, ret;
    O_p *oph = NULL;
    d_s *dsi = NULL, *dsd = NULL, *dst = NULL;
    float xi_1 = 0, xi0 = 0, tmp;
    float Max_pos, Max_val;

    if (opi == NULL)
    {
        return NULL;
    }

    nds = opi->n_dat;

    for (i = 0, nx = 0; i < nds; i++) // typical data set size
    {
        nx = (opi->dat[i]->nx > nx) ? opi->dat[i]->nx : nx;
    }

    if (nx <= 0)
    {
        return NULL;
    }

    for (i = 0, imin = -1; i < nds; i++)
    {
        if (imin < 0 && opi->dat[i]->nx > 0)
        {
            xi_1 = opi->dat[imin = i]->xd[0];
        }

        if (opi->dat[i]->nx > 0 && xi_1 > opi->dat[i]->xd[0])
        {
            xi_1 = opi->dat[imin = i]->xd[0];
        }
    }

    if (imin < 0)
    {
        return NULL;
    }

    if (opd != NULL)
    {
        dsd = create_and_attach_one_ds(opd, nx, nx, 0);
    }
    else
    {
        dsd = build_data_set(nx, nx);
    }

    if (dsd == NULL)
    {
        return NULL;
    }

    dsd->nx = dsd->ny = 0;
    i_1 = (int *) calloc(nds, sizeof(int));
    i0 = (int *) calloc(nds, sizeof(int));

    if (i_1 == NULL || i0 == NULL)
    {
        return NULL;
    }

    dst = build_data_set(nds, nds);

    if (dst == NULL)
    {
        return NULL;
    }

    dst->nx = dst->ny = 0;

    for (i = 0; i < nds; i++)
    {
        i_1[i] = i0[i] = 0;
    }

    while (1)
    {
        for (i = 0, imin = -1; i < nds; i++)
        {
            dsi = opi->dat[i];

            for (; (i_1[i] < dsi->nx) && (dsi->xd[i_1[i]] < xi_1); i_1[i]++);

            // dsi->xd[i_1[i]] is equal or greater than xi_1
            for (i0[i] = i_1[i]; (i0[i] < dsi->nx) && (dsi->xd[i0[i]] <= xi_1); i0[i]++);

            // dsi->xd[i0[i]] is greater than xi_1
            if ((i0[i] < dsi->nx) && (imin < 0))
            {
                xi0 = dsi->xd[i0[imin = i]];
            }

            if ((i0[i] < dsi->nx) && (dsi->xd[i0[i]] < xi0))
            {
                xi0 = dsi->xd[i0[imin = i]];
            }
        }

        for (i = dst->nx = dst->ny = 0; i < nds; i++)
        {
            dsi = opi->dat[i];

            if (i_1[i] >= dsi->nx)
            {
                continue;
            }

            if (i0[i] >= dsi->nx)
            {
                continue;
            }

            if ((dsi->xd[i_1[i]] == xi_1) && (dsi->xd[i0[i]] == xi0))
            {
                tmp = dsi->yd[i0[i]] - dsi->yd[i_1[i]];

                if (fabs(tmp) < (12 * biny))  // we assume small variation
                {
                    dst->xd[dst->nx] = (xi_1 + xi0) / 2;
                    dst->yd[dst->nx] = tmp;
                    dst->nx++;
                    dst->ny = dst->nx;
                }
            }
        }

        if (dst->nx > 1)
        {
            oph = build_histo_with_exponential_convolution(NULL, NULL, dst, biny);

            if (oph == NULL)
            {
                continue;
            }

            imax = find_index_of_max_in_y(oph->dat[0]);

            if (imax < 0)
            {
                continue;
            }

            nxh = oph->dat[0]->nx;
            ret = find_max_around(oph->dat[0]->yd, nxh, imax, &Max_pos, &Max_val, NULL);

            if (ret != 0)
            {
                continue;
            }

            tmp = oph->dat[0]->xd[0] + (Max_pos * (oph->dat[0]->xd[nxh - 1] - oph->dat[0]->xd[0]) / (nxh - 1));
            add_new_point_to_ds(dsd, (xi_1 + xi0) / 2, tmp);
            free_one_plot(oph);
        }

        for (i = 0, xi_1 = xi0, j = 0; i < nds; i++)
        {
            dsi = opi->dat[i];
            i_1[i] = i0[i];

            if (i0[i] < dsi->nx)
            {
                j++;
            }
        }

        if (j < 2)
        {
            break;
        }
    }

    inherit_from_ds_to_ds(dsd, opi->dat[0]);
    set_ds_source(dsd, "drift correlation tracking between data set at same x on ordered x axis");
    free_data_set(dst);
    free(i0);
    free(i_1);
    return dsd;
}



int follow_slow_correlated_drift_in_all_ds(void)
{
    int i;
    pltreg  *pr = NULL;
    O_p     *ops = NULL;
    static float biny = 0.1;

    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine looks for correlated drift in ds\n"
                             "having the same point index");
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &ops) != 2)
    {
        return win_printf_OK("cannot find data!");
    }

    i = win_scanf("Follow correlated drifts in a bunch of data sets \n"
                  "presenting sparse signal.\n"
                  "Bin size in Y %8f for the histogramm\n"
                  , &biny);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    compute_correlated_drift_in_Y_for_ds_with_ordered_integer_x(ops, ops, biny);
    ops->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
}


int average_all_ds_of_op_by_index_by_histo(void)
{
    int i, j, k, ki;
    pltreg  *pr = NULL;
    O_p     *ops = NULL, *opd = NULL;
    d_s     *dsd = NULL, *dsi = NULL, *dsh = NULL;
    int      max, min = 0, nds, maxh, np, iy, verbose = WIN_CANCEL, ret, maxk, npt, ix, nf;
    static float biny = 0.1, thres = 0.5;
    static int keeph = 0, multi_peak = 0;
    float ymin, ymax, tmp, Max_pos, Max_val, tmpx, maxmax;

    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine performs averaging oy x and y of all data sets\n"
                             "having the same point index");
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &ops) != 2)
    {
        return win_printf_OK("cannot find data!");
    }

    i = win_scanf("Average all data sets of a plot at the same integer point value\n"
                  "using an histogram along Y to average and peak maximum value.\n"
                  "This routine assumes X values to integers in rising order\n"
                  "Bin size in Y %8f for the histogramm. Select:\n"
                  "\\oc  %R a single point corresponding to the maximum of the histogram\n"
                  "\\oc  %r all maxima greater than a threshold value\n"
                  "\\oc  %r the combination of both\n"
                  "Define the treshold value %12f\n"
                  "keep histogram plot %b\n"
                  , &biny, &multi_peak, &thres, &keeph);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    nds = ops->n_dat;

    for (k = 0, max = 0, ymin = ymax = ops->dat[0]->yd[0]; k < nds; k++)
    {
        dsi = ops->dat[k];
        ix = (int)(dsi->xd[dsi->nx - 1] + 0.5);
        max = (k == 0 || ix > max) ? ix : max;
        ix = (int)(dsi->xd[0] + 0.5);
        min = (k == 0 || ix < min) ? ix : min;

        for (i = 0; i < dsi->nx; i++)
        {
            ymax = (dsi->yd[i] > ymax) ? dsi->yd[i] : ymax;
            ymin = (dsi->yd[i] < ymin) ? dsi->yd[i] : ymin;
        }
    }

    maxh = (int)(0.5 + ((ymax - ymin) * 5) / biny);
    maxh += 50;

    //win_printf("min = %g max = %g n = %d",ymin,ymax,maxh);
    if (keeph)
    {
        opd = create_and_attach_one_plot(pr, maxh, maxh, 0);

        if (opd == NULL)
        {
            return win_printf("Cannot allocate plot");
        }

        dsh = opd->dat[0];
        uns_op_2_op_by_type(ops, IS_Y_UNIT_SET, opd, IS_X_UNIT_SET);
    }
    else
    {
        dsh = build_data_set(maxh, maxh);

        if (dsh == NULL)
        {
            return win_printf("Cannot allocate dsh");
        }
    }

    /*
    dsh = build_data_set(maxh,maxh);
    if (dsh == NULL)    return win_printf("Cannot allocate ds");
    */
    nf = max - min + 1;
    dsd = create_and_attach_one_ds(ops, nf, nf, 0);

    if (dsd == NULL)
    {
        return win_printf("Cannot allocate plot");
    }

    //win_printf("treating %d pts",nf);
    set_ds_dot_line(dsd);
    set_plot_symb(dsd, "\\pt5\\oc ");
    dsd->nx = dsd->ny = 0;

    for (i = 0; i < nf; i++) // was max
    {
        // we scan all points by index
        ix = min + i;

        if (dsh == NULL)
        {
            if (keeph && opd != NULL)
            {
                dsh = create_and_attach_one_ds(opd, maxh, maxh, 0);
            }
            else
            {
                dsh = build_data_set(maxh, maxh);
            }

            if (dsh == NULL)
            {
                return win_printf("Cannot allocate plot");
            }
        }

        for (k = 0; k < dsh->nx; k++)
        {
            dsh->yd[k] = dsh->xd[k] = 0;    // we reset histo
        }

        for (k = np = 0; k < nds; k++)
        {
            // for one index we scan data sets
            dsi = ops->dat[k];

            for (ki = 0 ; ki < dsi->nx && dsi->xd[ki] < (float)ix; ki++);

            if (ki < dsi->nx && ((int)(0.5 + dsi->xd[ki]) == ix))
            {
                iy = (int)(0.5 + ((dsi->yd[ki] - ymin) * 5) / biny);
                iy += 25;

                for (j = iy - 25; j <= iy + 25; j++)
                {
                    if (j >= 0 && j < dsh->nx)
                    {
                        tmp = ymin + ((j - 25) * biny / 5) - dsi->yd[ki];
                        tmp /= biny;
                        tmp = exp(-tmp * tmp);

                        if (verbose != WIN_CANCEL)
                            verbose = win_printf("i = %d k = %d j = %d dj %d \n y = %g tmp %g"
                                                 , i, k, j, j - iy, (ymin + ((j - 25) * biny / 5) - dsi->yd[ki]) / biny, tmp);

                        dsh->yd[j] += tmp;
                        dsh->xd[j] += dsi->xd[ki] * tmp;
                        np++;
                    }
                }
            }

            //else win_printf("At pt %d, nds %d ki = %d out of range %d\nxd %g xi %d"
            //		    ,i,k,ki,dsi->nx,dsi->xd[dsi->nx-1],ix);
        }

        for (k = 0; np > 0 && k < dsh->nx; k++)
        {
                //dsh->yd[k] *= (float)51 / np;
            dsh->xd[k] *= (float)51 / np;
        }

        for (k = 1, maxmax = dsh->yd[maxk = 0], npt = 0; k < dsh->nx - 1; k++)
        {
            if ((dsh->yd[k] > dsh->yd[k - 1]) && (dsh->yd[k] > dsh->yd[k + 1]))
            {
                // we have some maximum
                ret = find_max_around(dsh->yd, dsh->nx, k, &Max_pos, &Max_val, NULL);

                if (ret == 0)
                {
                    if (Max_val > maxmax)
                    {
                        maxk = k;
                        maxmax = Max_val;
                    }

                    if (multi_peak > 0)
                    {
                        if (dsh->yd[k] > thres)
                        {
                            // we keep max greater than threshold
                            tmp = (dsh->yd[k] > 0) ? dsh->xd[k] / dsh->yd[k] : dsh->xd[k];
                            add_new_point_to_ds(dsd, tmp, ymin + (Max_pos - 25)*biny / 5);
                            npt++;
                        }
                    }
                }
            }
        }

        if ((multi_peak == 0) || ((multi_peak == 2) && (npt == 0)))
        {
            find_max_around(dsh->yd, dsh->nx, maxk, &Max_pos, &Max_val, NULL);
            tmp = (dsh->yd[maxk] > 0) ? dsh->xd[maxk] / dsh->yd[maxk] : dsh->xd[maxk];
            add_new_point_to_ds(dsd, tmp, ymin + (Max_pos - 25)*biny / 5);
        }

        for (k = 0, tmp = tmpx = 0; k < dsh->nx; k++)
        {
            tmp += dsh->yd[k];
            tmpx += dsh->xd[k];
        }

        tmpx = (tmp > 0) ? tmpx / tmp : tmpx;
        set_ds_source(dsh, "Histogram at %d -> %g expo-convolution \\sigma = %g", i, tmpx, biny);

        for (k = 0; k < dsh->nx; k++)
        {
            dsh->xd[k] = ymin + ((k - 25) * biny / 5);    // we reset histo
        }

        //  if (i == 0)
        //win_printf("min = %g max = %g n = %d\n0-> %g %d->%g",ymin,ymax,maxh,dsh->xd[0],dsh->nx-1,dsh->xd[dsh->nx-1]);
        if (keeph == 0)
        {
            free_data_set(dsh);
        }

        dsh = NULL;
    }

    set_ds_source(dsd, "Average data sets over %d data sets", ops->n_dat);
    ops->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
}


//Average all data sets of a plot at the same integer point value
//using an histogram along Y to average and peak maximum value.
//This routine assumes X values to integers in rising order
//biny : Bin size in Y for the histogramm.
//multi_peak :
// 0 a single point corresponding to the maximum of the histogram\n"
// 1 all maxima greater than a threshold value\n"
// 2  the combination of both\n"
// thres : the treshold value

d_s *average_all_ds_of_op_by_max_of_histo(O_p *ops, float biny, int multi_peak, float thres)
{
    return average_all_ds_of_op_by_max_of_bounded_histo(ops, biny, multi_peak, thres, 1, -1);
}


d_s *average_all_ds_of_op_by_max_of_bounded_histo(O_p *ops, float biny, int multi_peak, float thres, float y_min, float y_max)
{
    int i, j, k, ki;
    d_s     *dsd = NULL, *dsi = NULL, *dsh = NULL;
    int      max, min = 0, nds, maxh, np, iy, ret, maxk, npt, ix, nf;
    float ymin, ymax, tmp, Max_pos, Max_val, tmpx, maxmax;

    if (ops == NULL) return NULL;

    nds = ops->n_dat;

    for (k = 0, max = 0, ymin = ymax = ops->dat[0]->yd[0]; k < nds; k++)
    {
        dsi = ops->dat[k];
        ix = (int)(dsi->xd[dsi->nx - 1] + 0.5);
        max = (k == 0 || ix > max) ? ix : max;
        ix = (int)(dsi->xd[0] + 0.5);
        min = (k == 0 || ix < min) ? ix : min;

        for (i = 0; i < dsi->nx; i++)
        {
            ymax = (dsi->yd[i] > ymax) ? dsi->yd[i] : ymax;
            ymin = (dsi->yd[i] < ymin) ? dsi->yd[i] : ymin;
        }
    }
    if (y_max >= y_min)
        {
            ymax = y_max;
            ymin = y_min;
        }
    maxh = (int)(0.5 + ((ymax - ymin) * 5) / biny);
    maxh += 50;

    dsh = build_data_set(maxh, maxh);
    if (dsh == NULL) return NULL; //win_printf("Cannot allocate dsh");

    nf = max - min + 1;
    dsd = build_data_set(nf, nf);
    if (dsd == NULL)         return NULL;//win_printf("Cannot allocate plot");

    set_ds_dot_line(dsd);
    set_plot_symb(dsd, "\\pt5\\oc ");
    dsd->nx = dsd->ny = 0;

    for (i = 0; i < nf; i++) // was max
    {
        // we scan all points by index
        ix = min + i;

        if (dsh == NULL)
        {
            dsh = build_data_set(maxh, maxh);
            if (dsh == NULL)
            {
                    return NULL;//win_printf("Cannot allocate plot");
            }
        }

        for (k = 0; k < dsh->nx; k++)
        {
            dsh->yd[k] = dsh->xd[k] = 0;    // we reset histo
        }

        for (k = np = 0; k < nds; k++)
        {
            // for one index we scan data sets
            dsi = ops->dat[k];

            for (ki = 0 ; ki < dsi->nx && dsi->xd[ki] < (float)ix; ki++);

            if (ki < dsi->nx && ((int)(0.5 + dsi->xd[ki]) == ix))
            {
                iy = (int)(0.5 + ((dsi->yd[ki] - ymin) * 5) / biny);
                iy += 25;

                for (j = iy - 25; j <= iy + 25; j++)
                {
                    if (j >= 0 && j < dsh->nx)
                    {
                        tmp = ymin + ((j - 25) * biny / 5) - dsi->yd[ki];
                        tmp /= biny;
                        tmp = exp(-tmp * tmp);
                        dsh->yd[j] += tmp;
                        dsh->xd[j] += dsi->xd[ki] * tmp;
                        np++;
                    }
                }
            }
        }

        for (k = 0; np > 0 && k < dsh->nx; k++)
        {
            dsh->yd[k] *= (float)51 / np;
            dsh->xd[k] *= (float)51 / np;
        }

        for (k = 1, maxmax = dsh->yd[maxk = 0], npt = 0; k < dsh->nx - 1; k++)
        {
            if ((dsh->yd[k] >= dsh->yd[k - 1]) && (dsh->yd[k] >= dsh->yd[k + 1]))
            {
                // we have some maximum
                ret = find_max_around(dsh->yd, dsh->nx, k, &Max_pos, &Max_val, NULL);

                if (ret == 0)
                {
                    if (Max_val > maxmax)
                    {
                        maxk = k;
                        maxmax = Max_val;
                    }

                    if (multi_peak > 0)
                    {
                        if (dsh->yd[k] > thres)
                        {
                            // we keep max greater than threshold
                            tmp = (dsh->yd[k] > 0) ? dsh->xd[k] / dsh->yd[k] : dsh->xd[k];
                            add_new_point_to_ds(dsd, tmp, ymin + (Max_pos - 25)*biny / 5);
                            npt++;
                        }
                    }
                }
            }
        }

        if ((multi_peak == 0) || ((multi_peak == 2) && (npt == 0)))
        {
            find_max_around(dsh->yd, dsh->nx, maxk, &Max_pos, &Max_val, NULL);
            tmp = (dsh->yd[maxk] > 0) ? dsh->xd[maxk] / dsh->yd[maxk] : dsh->xd[maxk];
            add_new_point_to_ds(dsd, tmp, ymin + (Max_pos - 25)*biny / 5);
        }

        for (k = 0, tmp = tmpx = 0; k < dsh->nx; k++)
        {
            tmp += dsh->yd[k];
            tmpx += dsh->xd[k];
        }

        tmpx = (tmp > 0) ? tmpx / tmp : tmpx;
        set_ds_source(dsh, "Histogram at %d -> %g expo-convolution \\sigma = %g", i, tmpx, biny);

        for (k = 0; k < dsh->nx; k++)
        {
            dsh->xd[k] = ymin + ((k - 25) * biny / 5);    // we reset histo
        }

        free_data_set(dsh);
        dsh = NULL;
    }
    set_ds_source(dsd, "Average data sets over %d data sets", ops->n_dat);
    return dsd;
}




int average_all_ds_of_op_by_index_by_histo_2(void)
{
    int i;
    pltreg  *pr = NULL;
    O_p     *ops = NULL;
    d_s     *dsd = NULL;
    static float biny = 0.1, thres = 0.5;
    static int multi_peak = 0;

    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine performs averaging oy x and y of all data sets\n"
                             "having the same point index");
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &ops) != 2)
    {
        return win_printf_OK("cannot find data!");
    }

    i = win_scanf("Average all data sets of a plot at the same integer point value\n"
                  "using an histogram along Y to average and peak maximum value.\n"
                  "This routine assumes X values to integers in rising order\n"
                  "Bin size in Y %8f for the histogramm. Select:\n"
                  "\\oc  %R a single point corresponding to the maximum of the histogram\n"
                  "\\oc  %r all maxima greater than a threshold value\n"
                  "\\oc  %r the combination of both\n"
                  "Define the treshold value %12f\n"
                  , &biny, &multi_peak, &thres);

    if (i == WIN_CANCEL)
    {
        return D_O_K;
    }

    dsd = average_all_ds_of_op_by_max_of_histo(ops, biny, multi_peak, thres);
    add_data_to_one_plot(ops, IS_DATA_SET, dsd);
    ops->need_to_refresh = 1;
    return refresh_plot(pr, UNCHANGED);
}


/********************************************************************/
/* simple operations on all datasets simultaneously                 */
/********************************************************************/
int opp_on_all_ds(void)
{
    int i, j, k;
    pltreg  *pr = NULL;
    O_p     *ops = NULL, *opd = NULL;
    float   tmp;
    d_s     *dsd = NULL, *dsi = NULL;
    int     index, n_ds_end, n_ds_start;
    static float ax =  0, dx = 1, rx = 1;
    float drx;

    if (updating_menu_state != 0)
    {
        return (D_O_K);
    }

    if (key[KEY_LSHIFT])
    {
        return (win_printf_OK("This routine performs arithmetical operations\n"
                              "on all datasets of the current plot\n"
                              "a new plot is created"));
    }

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &ops) != 2)
    {
        return (win_printf_OK("cannot find data!"));
    }

    //index = active_menu->flags & 0xFFFFFF00;
    index = RETRIEVE_MENU_INDEX;

    if (index == X_AXIS)
    {
        i = win_scanf("{\\color{lightgreen}Transformation along X}\n"
                      "new X = old X \\times {\\it \\frac{n}{d}} + {\\it b}\n"
                      "offset {\\it b} = %f multiplier {\\it n} = %f divisor {\\it d} %f", &ax, &dx, &rx);

        if (i == WIN_CANCEL)
        {
            return (OFF);    // index = 0;
        }

        if (rx == 0)
        {
            return (win_printf_OK("can't divide by 0!"));
        }

        drx = dx / rx;
        opd = duplicate_plot_data(ops, opd);

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        for (k = 0; k < opd->n_dat; k++)
        {
            dsd = opd->dat[k];

            for (i = 0; i < dsd->nx ; i++)
            {
                dsd->xd[i] *= drx;
                dsd->xd[i] += ax;

                if (dsd->xe)
                {
                    dsd->xe[i] *= drx;
                }
            }

            inherit_from_ds_to_ds(dsd, ops->dat[k]);
            update_ds_treatement(dsd, "X axis multiplied by %g, divided by %g and offseted by %g", dx, rx, ax);
        }
    }
    else if (index == Y_AXIS)
    {
        i = win_scanf("{\\color{lightgreen}Transformation along Y}\n"
                      "new Y = old Y \\times {\\it \\frac{n}{d}} + {\\it b}\n"
                      "offset {\\it b} = %f multiplier {\\it n} = %f divisor {\\it d} %f", &ax, &dx, &rx);

        if (i == WIN_CANCEL)
        {
            return (OFF);    // index = 0;
        }

        if (rx == 0)
        {
            return (win_printf_OK("can't divide by 0!"));
        }

        drx = dx / rx;
        opd = duplicate_plot_data(ops, opd);

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        for (k = 0; k < opd->n_dat; k++)
        {
            dsd = opd->dat[k];

            for (i = 0; i < dsd->ny ; i++)
            {
                dsd->yd[i] *= drx;
                dsd->yd[i] += ax;

                if (dsd->ye)
                {
                    dsd->ye[i] *= drx;
                }
            }

            inherit_from_ds_to_ds(dsd, ops->dat[k]);
            update_ds_treatement(dsd, "Y axis multiplied by %g, divided by %g and offseted by %g", dx, rx, ax);
        }
    }
    else if (index == DS_INVERSE)
    {
        opd = duplicate_plot_data(ops, opd);

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        for (k = 0; k < opd->n_dat; k++)
        {
            dsd = opd->dat[k];

            for (i = 0; i < dsd->ny ; i++)
            {
                if (dsd->yd[i] != 0)
                {
                    dsd->yd[i] = 1. / dsd->yd[i];
                }
                else
                {
                    dsd->yd[i] = 0.;
                }

                if (dsd->ye)
                {
                    dsd->ye[i] *= (dsd->yd[i] * dsd->yd[i]);
                }
            }

            inherit_from_ds_to_ds(dsd, ops->dat[k]);
            update_ds_treatement(dsd, "inversed 1/y");
        }
    }
    else if (index == DS_SQRT)
    {
        opd = duplicate_plot_data(ops, opd);

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        for (k = 0; k < opd->n_dat; k++)
        {
            dsd = opd->dat[k];

            for (i = 0; i < dsd->ny ; i++)
            {
                if (dsd->yd[i] >= 0)
                {
                    dsd->yd[i] = sqrtf(dsd->yd[i]);
                }
                else
                {
                    dsd->yd[i] = -1.;
                }

                if (dsd->ye)
                {
                    if (dsd->yd[i] != 0)
                    {
                        dsd->ye[i] /= 2.*dsd->yd[i];
                    }
                    else
                    {
                        dsd->ye[i] =  0.;
                    }
                }
            }

            inherit_from_ds_to_ds(dsd, ops->dat[k]);
            update_ds_treatement(dsd, "ds square-rooted");
        }
    }
    else if (index == DS_SQR)
    {
        opd = duplicate_plot_data(ops, opd);

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        for (k = 0; k < opd->n_dat; k++)
        {
            dsd = opd->dat[k];

            for (i = 0; i < dsd->ny ; i++)
            {
                if (dsd->ye)
                {
                    dsd->ye[i] *= 2.*dsd->yd[i];
                }

                dsd->yd[i] = dsd->yd[i] * dsd->yd[i];
            }

            inherit_from_ds_to_ds(dsd, ops->dat[k]);
            update_ds_treatement(dsd, "ds squared");
        }
    }
    else if (index == DS_ABS)
    {
        opd = duplicate_plot_data(ops, opd);

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        for (k = 0; k < opd->n_dat; k++)
        {
            dsd = opd->dat[k];

            for (i = 0; i < dsd->ny ; i++)
            {
                dsd->yd[i] = fabsf(dsd->yd[i]);
            }

            inherit_from_ds_to_ds(dsd, ops->dat[k]);
            update_ds_treatement(dsd, "abs(ds)");
        }
    }
    else if (index == DS_LOG)
    {
        opd = duplicate_plot_data(ops, opd);

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        for (k = 0; k < opd->n_dat; k++)
        {
            dsd = opd->dat[k];

            for (i = 0; i < dsd->ny ; i++)
            {
                tmp = dsd->yd[i];

                if (dsd->yd[i] > 0)
                {
                    dsd->yd[i] = logf(dsd->yd[i]);
                }
                else
                {
                    dsd->yd[i] = -20.;
                }

                if (dsd->ye)
                {
                    if (tmp != 0)
                    {
                        dsd->ye[i] *= dsd->yd[i] / tmp;
                    }
                    else
                    {
                        dsd->ye[i] =  0.;
                    }
                }
            }

            inherit_from_ds_to_ds(dsd, ops->dat[k]);
            update_ds_treatement(dsd, "ds log (ln) in y");
        }
    }
    else if (index == DS_LOG10)
    {
        opd = duplicate_plot_data(ops, opd);

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        for (k = 0; k < opd->n_dat; k++)
        {
            dsd = opd->dat[k];

            for (i = 0; i < dsd->ny ; i++)
            {
                tmp = dsd->yd[i];

                if (dsd->yd[i] > 0)
                {
                    dsd->yd[i] = log10f(dsd->yd[i]);
                }
                else
                {
                    dsd->yd[i] = -20.;
                }

                if (dsd->ye)
                {
                    if (tmp != 0)
                    {
                        dsd->ye[i] *= dsd->yd[i] / tmp;
                    }
                    else
                    {
                        dsd->ye[i] =  0.;
                    }
                }
            }

            inherit_from_ds_to_ds(dsd, ops->dat[k]);
            update_ds_treatement(dsd, "ds log10 in y");
        }
    }
    else if (index == DS_EXPO)
    {
        opd = duplicate_plot_data(ops, opd);

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        for (k = 0; k < opd->n_dat; k++)
        {
            dsd = opd->dat[k];

            for (i = 0; i < dsd->ny ; i++)
            {
                tmp = dsd->yd[i];

                if (dsd->yd[i] >= 88)
                {
                    dsd->yd[i] = 1e35;
                }
                else if (dsd->yd[i] <= -88)
                {
                    dsd->yd[i] = 1e-35;
                }
                else
                {
                    dsd->yd[i] = expf(dsd->yd[i]);
                }

                if (dsd->ye)
                {
                    if (tmp != 0)
                    {
                        dsd->ye[i] *= dsd->yd[i] / tmp;
                    }
                    else
                    {
                        dsd->ye[i] =  0.;
                    }
                }
            }

            inherit_from_ds_to_ds(dsd, ops->dat[k]);
            update_ds_treatement(dsd, "ds exp in y");
        }
    }
    else if (index == DS_ATAN)
    {
        opd = duplicate_plot_data(ops, opd);

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        for (k = 0; k < opd->n_dat; k++)
        {
            dsd = opd->dat[k];

            for (i = 0; i < dsd->ny ; i++)
            {
                tmp = dsd->yd[i];
                dsd->yd[i] = atanf(dsd->yd[i]);

                if (dsd->ye)
                {
                    dsd->ye[i] *= 1. / (tmp * tmp);
                }
            }

            inherit_from_ds_to_ds(dsd, ops->dat[k]);
            update_ds_treatement(dsd, "arctan(ds)");
        }
    }
    else if (index == DS_TANH)
    {
        opd = duplicate_plot_data(ops, opd);

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        for (k = 0; k < opd->n_dat; k++)
        {
            dsd = opd->dat[k];

            for (i = 0; i < dsd->ny ; i++)
            {
                tmp = dsd->yd[i];
                dsd->yd[i] = tanh(dsd->yd[i]);

                if (dsd->ye)
                {
                    dsd->ye[i] *= 1. / (tmp * tmp);    // need work !
                }
            }

            inherit_from_ds_to_ds(dsd, ops->dat[k]);
            update_ds_treatement(dsd, "tanh(ds)");
        }
    }
    else if (index == DS_EXTRACT_X_T)
    {
        opd = duplicate_plot_data(ops, NULL);

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        n_ds_start = 0;
        n_ds_end = opd->n_dat;

        for (j = n_ds_start; j < n_ds_end; j++)
        {
            dsi = opd->dat[j];

            if (dsi->xe)
            {
                alloc_data_set_y_error(dsi);
            }

            for (i = 0 ; i < dsi->nx ; i++)
            {
                dsi->yd[i] = dsi->xd[i];

                if (dsi->xe)
                {
                    dsi->ye[i] = dsi->xe[i];
                }
            }

            if (dsi->xe)
            {
                free_data_set_x_error(dsi);
            }

            for (i = 0 ; i < dsi->nx ; i++)
            {
                dsi->xd[i] = i;
            }

            update_ds_treatement(dsi, "ds extract x serie");
        }
    }
    else if (index == DS_EXTRACT_Y_T)
    {
        opd = duplicate_plot_data(ops, NULL);

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        n_ds_start = 0;
        n_ds_end = opd->n_dat;

        for (j = n_ds_start; j < n_ds_end; j++)
        {
            dsi = opd->dat[j];

            if (dsi->xe)
            {
                free_data_set_x_error(dsi);
            }

            for (i = 0 ; i < dsi->nx ; i++)
            {
                dsi->xd[i] = i;
            }

            update_ds_treatement(dsi, "ds extract y serie");
        }
    }
    else
    {
        return (D_O_K);
    }

    if (opd != NULL)
    {
        add_data_to_pltreg(pr, IS_ONE_PLOT, (void *)opd);
        opd->need_to_refresh = 1;
    }

    pr->cur_op = pr->n_op - 1;
    pr->one_p = pr->o_p[pr->cur_op];
    return refresh_plot(pr, UNCHANGED);
} // end of function "opp_on_all_ds"



/********************************************************/
/* taken in interpol.c (26/11/2003)         */
/* Modified by Hugo Roussille (19/07/2016)              */
/********************************************************/
int do_fir_filtering(void)
{
  int i, j, k, l;
    pltreg *pr = NULL;
    O_p *op = NULL, *opd = NULL;
    d_s *dsi = NULL, *dsd = NULL;
    int  nx, n_dat;
    static int nfir = 8, all_ds = 0, new_plot = 0, axes=2;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf("This routine creates a new data set containing\n"
                          "the simple Finite Inpulse Response filtering\n"
                          "of the of the selected data set. This filtering\n"
                          "corresponds to sliding averaging over a finite\n"
                          "number of points");
    }

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)
    {
        return win_printf("cannot find data");
    }

    i = win_scanf("Finite Impulse Response filtering with a rectangular\n"
                  "window over %8d points\n"
                  "Apply to all datasets %b place result in a new plot %b\n"
						"Do the average on %R x axis %r y axis %r both axes"
                  , &nfir, &all_ds, &new_plot, &axes);

    if (i == WIN_CANCEL)
    {
        return 0;
    }

    if (new_plot)
    {
        i = op->n_dat;
        op->n_dat = 0;
        opd = duplicate_plot_data(op, NULL);
        op->n_dat = i;

        if (opd == NULL)
        {
            return (win_printf_OK("can't create plot"));
        }

        add_data_to_pltreg(pr, IS_ONE_PLOT, (void *)opd);
        opd->need_to_refresh = 1;
    }
    else
    {
        opd = op;
    }

    n_dat = op->n_dat;

    for (k = 0; k < n_dat; k++)
    {
        if ((all_ds == 0) && (op->dat[k] != dsi))
        {
            continue;
        }

        dsi = op->dat[k];
        nx = dsi->nx;

        if (nx - nfir <= 0)
        {
            continue;
        }

		  dsd = create_and_attach_one_ds(opd, nx + 1 - nfir, nx + 1 - nfir, 0);

		  if (dsd == NULL)
		  {
				return win_printf("Cannot allocate plot");
		  }

		  if (axes == 2)
		  {
			  for (i = 0; i < nx + 1 - nfir; i++)
			  {
					for (j = 0, dsd->xd[i] = dsd->yd[i] = 0; j < nfir; j++)
					{
				l = i + j;
				l = (l < dsi->nx) ? l : dsi->nx - 1;
				dsd->xd[i] += dsi->xd[l];
				dsd->yd[i] += dsi->yd[l];
					}

					dsd->xd[i] /= nfir;
					dsd->yd[i] /= nfir;
			  }
		  }
		  else if (axes == 1)
		  {
			  for (i = 0; i < nx + 1 - nfir; i++)
			  {
					for (j = 0, dsd->xd[i] = dsd->yd[i] = 0; j < nfir; j++)
					{
				l = i + j;
				l = (l < dsi->nx) ? l : dsi->nx - 1;
				dsd->yd[i] += dsi->yd[l];
					}

					dsd->xd[i] = dsi->xd[i + nfir/2];
					dsd->yd[i] /= nfir;
			  }
		  }
		  else if (axes == 0)
		  {
			  for (i = 0; i < nx + 1 - nfir; i++)
			  {
					for (j = 0, dsd->xd[i] = dsd->yd[i] = 0; j < nfir; j++)
					{
				l = i + j;
				l = (l < dsi->nx) ? l : dsi->nx - 1;
				dsd->xd[i] += dsi->xd[l];
					}

					dsd->yd[i] = dsi->yd[i + nfir/2];
					dsd->xd[i] /= nfir;
			  }
		  }
		  inherit_from_ds_to_ds(dsd, dsi);
		  update_ds_treatement(dsd, "FIR filter over %d points", nfir);

    }

    return refresh_plot(pr, (new_plot) ? pr->n_op -  1 : UNCHANGED);
}
/* end of "do_fir_filtering" */

//   fit parameter a in y = a*z(x), y and z ds must have same x
//   by minimisation of chi^2

int	find_best_a_of_2_ds(d_s *dsi,    // input data set
			    d_s *dsm,    // the model function
			    int verbose, // issue explicit error message
			    double *a,   // the best a value
			    double *E)   // the chi^2
{
    int i, j;
    double fy, f, y, fx, er, la = 0; //, x


    if (dsi == NULL || dsm == NULL || dsm->nx != dsi->nx)
    {
        if (verbose) win_printf("No valid data \n");
        return 1;
    }
    if (dsi->ye == NULL)
    {
        if (verbose) win_printf("No valid data error in Y \n");
        return 2;
    }
    for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
        er = dsi->ye[i];
        //x = dsi->xd[i];
        y = dsi->yd[i];
        fx = dsm->yd[i];
        if (er > 0)
        {
            fy += y*fx/(er*er);
            f += (fx*fx)/(er*er);
        }
    }
    if (f != 0)    la = fy/f;
    if (a) *a = la;
    else return 2;
    for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
        //x = dsi->xd[i];
        y = dsi->yd[i];
        fx = la*dsm->yd[i];
        er = dsi->ye[i];
        f = y - fx;
        if (er > 0)
        {
            *E += (f*f)/(er*er);
            j++;
        }
    }
    return 0;
}

//   fit parameter a in y = a*z(x) + b, y and z ds must have same x
//   by minimisation of chi^2


int	find_best_a_and_b_of_2_ds(d_s *dsi,    // input data set t
				 d_s *dsm,    // the model function
				 int verbose, // issue explicit error message
				 int error_type, // the type of error in data
				 double *a,   // the best a value
				 double *b,   // the best b value
				 double *E)   // the chi^2
{
    int i, j;
    double fy, f, y, fx, er, la, lb; // , x
    double sy = 0, sy2 = 0, sfx = 0, sfx2 = 0, sfxy = 0, sig_2, ny = 0;

    if (dsi == NULL || dsm == NULL || dsm->nx != dsi->nx)
    {
        if (verbose) win_printf("No valid data \n");
        return 1;
    }
    for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
        //x = dsi->xd[i];
        y = dsi->yd[i];
        fx = dsm->yd[i];
        if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
        else if (error_type == 1) er = fx;
        else er = 1;
        if (er > 0)
        {
            sig_2 = (double)1/(er*er);
            sy += y*sig_2;
            sy2 += y*y*sig_2;
            sfx += fx*sig_2;
            sfx2 += fx*fx*sig_2;
            sfxy += y*fx*sig_2;
            ny += sig_2;
        }
    }
    lb = (sfx2 * ny) - sfx * sfx;
    if (lb == 0)
    {
        if (verbose) win_printf_OK("\\Delta  = 0 \n can't fit that!");
        return 3;
    }
    la = sfxy * ny - sfx * sy;
    la /= lb;
    lb = (sfx2 * sy - sfx * sfxy)/lb;
    if (a) *a = la;
    if (b) *b = lb;
    for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
        //x = dsi->xd[i];
        y = dsi->yd[i];
        fx = la*dsm->yd[i] + lb;
        if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
        else if (error_type == 1) er = fx;
        else er = 1;
        f = y - fx;
        if (er > 0)
        {
            *E += (f*f)/(er*er);
            j++;
        }
    }
    return 0;
}





int do_least_square_fit_on_ds(void)
{
    int i;
    pltreg *pr = NULL;
    O_p   *op = NULL;
    d_s *dsi1 = NULL;
    d_s *dest_ds = NULL;
    d_s *dslog = NULL;
    int do_log = 0, do_log_x = 0, do_log_y = 0;
    int  bool_not_affine = 0;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return (win_printf_OK("{\\color{yellow}Linear fit}\n"
                              "It searches for the linear fit Y=aX+b of ALL points of the active dataset.\n"
                              "If the left <CTRL> key is pressed, b is forced to be zero, i.e. it performs\n"
                              "the fit Y=aX.\n\n"
                              "a new dataset is created, with 2 points"));
    }

    // by default, we compute an affine fit y=ax+b; but if ALT is pressed, the fit is linear y=ax
    if (key[KEY_LCONTROL])
    {
        bool_not_affine = 1;
    }

    if (RETRIEVE_MENU_INDEX)
    {
        bool_not_affine = 1;
    }

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi1);

    if ((i != 3) || (dsi1 == NULL) || dsi1->nx == 0)
    {
        return (win_printf_OK("can't find dataset"));
    }

    if (is_plot_x_log(op))
    {
      dslog = duplicate_data_set(dsi1,NULL);
      if (dslog == NULL) return(win_printf_OK("no dataset available\n"));
      for (int k = 0;k < dslog->nx;k++)
      {
          dslog->xd[k] = log10(dsi1->xd[k]);
      }
      do_log = 1;
      do_log_x = 1;
    }

    if (is_plot_y_log(op))
    {
      if (dslog == NULL) dslog = duplicate_data_set(dsi1,NULL);
      if (dslog == NULL) return(win_printf_OK("no dataset available\n"));
      for (int k = 0;k < dslog->ny;k++)
      {
          dslog->yd[k] = log10(dsi1->yd[k]);
      }
      do_log = 1;
      do_log_y =1;
    }


    if (do_log) dest_ds = least_square_fit_on_ds(dslog, !bool_not_affine, NULL, NULL);
    else dest_ds = least_square_fit_on_ds(dsi1, !bool_not_affine, NULL, NULL);
    if (dest_ds)
    {
        add_data_to_one_plot(op, IS_DATA_SET, dest_ds);
        if (do_log_x)
        {
          for (int k = 0;k<dest_ds->nx;k++)
          {
          dest_ds->xd[k] = pow(10,dest_ds->xd[k]);
        }
        for (int k = 0;k<dest_ds->n_lab;k++)
        {
        dest_ds->lab[k]->xla = 0.5*dsi1->xd[0] + 0.5*dsi1->xd[dsi1->nx-1];
      }

        }
        if (do_log_y)
        {
          for (int k = 0;k<dest_ds->nx;k++)
          {
          dest_ds->yd[k] = pow(10,dest_ds->yd[k]);

        }
        for (int k = 0;k<dest_ds->n_lab;k++)
        {
        dest_ds->lab[k]->yla = 0.5*dsi1->yd[0] + 0.5*dsi1->yd[dsi1->nx-1];
      }


        }
    }
    if (dslog) free_data_set(dslog);
    return refresh_plot(pr, UNCHANGED);
}
d_s *least_square_fit_on_ds(const d_s *src_ds, bool is_affine_fit, double *out_a, double *out_b)
{
    return least_square_fit_on_ds_between_indexes(src_ds, is_affine_fit, -1, -1, out_a, out_b);
}
d_s *least_square_fit_on_ds_between_indexes(const d_s *src_ds, bool is_affine_fit, int min_idx, int max_idx,
        double *out_a, double *out_b)
{
    d_s *dest_ds = NULL;
    int nx;
    bool has_good_error_bars = true;
    float minx = FLT_MAX;
    float maxx = -FLT_MAX;
    float miny = FLT_MAX;
    float maxy = -FLT_MAX;
    float tmp = 0;
    double sy2 = 0, sy = 0, sx = 0, sx2 = 0, sxy = 0, sig2 = 0, sig_2 = 0, chi2 = 0, ny = 0;
    double a = 0, b = 0;
    char  c[256] = {0};
    assert(src_ds);

    if (min_idx < 0)
    {
        min_idx = 0;
    }

    if (max_idx < 0)
    {
        max_idx = src_ds->nx;
    }

    nx = max_idx - min_idx;

    if (max_idx - min_idx < 2)
    {
        (win_printf_OK("no fit on less than 2 points"));
        return NULL;
    }

    if (src_ds->ye == NULL)
    {
        has_good_error_bars = false;
    }
    else
    {
        has_good_error_bars = true;

        for (int i = min_idx; i < max_idx && has_good_error_bars == 1; i++)
        {
            if (src_ds->ye[i] == 0)
            {
                has_good_error_bars = 0;
            }
        }
    }

    if (has_good_error_bars == 0)
    {
        minx = FLT_MAX;
        maxx = -FLT_MAX;
        miny = FLT_MAX;
        maxy = -FLT_MAX;

        for (int i = min_idx; i < max_idx ; i++)
        {
            sy += src_ds->yd[i];
            sy2 += src_ds->yd[i] * src_ds->yd[i];
            sx += src_ds->xd[i];
            sx2 += src_ds->xd[i] * src_ds->xd[i];
            sxy += src_ds->xd[i] * src_ds->yd[i];
            minx = (src_ds->xd[i] < minx) ? src_ds->xd[i] : minx;
            maxx = (src_ds->xd[i] > maxx) ? src_ds->xd[i] : maxx;
            miny = (src_ds->yd[i] < miny) ? src_ds->yd[i] : miny;
            maxy = (src_ds->yd[i] > maxy) ? src_ds->yd[i] : maxy;
        }

        if (!is_affine_fit)
        {
            if (sx2 == 0)
            {
                win_printf_OK("\\Sigma x_i^2 = 0   => cannot perform fit.");
                return NULL;
            }

            a = (sxy / sx2);
            b = 0.;
        }
        else
        {
            b = (sx2 * nx) - sx * sx;

            if (b == 0)
            {
                win_printf_OK("\\Delta  = 0 \n can't fit that!");
                return NULL;
            }

            a = sxy * nx - sx * sy;
            a /= b;
            b = (sx2 * sy - sx * sxy) / b;
        }

        dest_ds = build_data_set(2, 2); //create_and_attach_one_ds(dest_op, 2, 2, 0);

        if (dest_ds == NULL)
        {
            win_printf_OK("can't create data set");
            return  NULL;
        }

        tmp = maxx - minx;
        minx = minx - (tmp * .05);
        maxx = maxx + (tmp * .05);
        dest_ds->xd[0] = minx;
        dest_ds->xd[1] = maxx;
        dest_ds->yd[0] = a * minx + b;
        dest_ds->yd[1] = a * maxx + b;
        inherit_from_ds_to_ds(dest_ds, src_ds);

        if (is_affine_fit)
        {
            update_ds_treatement(dest_ds, "least square fit y=ax+b, a = %g and b = %g", a, b);
            snprintf(c, sizeof(c),"\\fbox{\\stack{{\\sl y = a x + b}{a = %g}{b = %g}}}", a, b);
        }
        else
        {
            update_ds_treatement(dest_ds, "least square fit y=ax, a = %g", a);
            snprintf(c, sizeof(c), "\\fbox{\\stack{{\\sl y = a x}{a = %g}}}", a);
        }

        //    the following line is to created a label not attached to the datset
        //        push_plot_label(pr->one_p, (maxx+minx)/2, a*(maxx+minx)/2 + b, c, USR_COORD);
        //      now, we attach the label to the dataset:
        set_ds_plot_label(dest_ds, (maxx + minx) / 2, a * (maxx + minx) / 2 + b, USR_COORD, "%s",c);
    }
    else  // with error bars in y
    {

        minx = FLT_MAX;
        maxx = -FLT_MAX;
        miny = FLT_MAX;
        maxy = -FLT_MAX;

        for (int i = 0; i < nx ; i++)
        {
            sig2 = src_ds->ye[i] * src_ds->ye[i];
            sig_2 = (double)1 / sig2;
            sy += src_ds->yd[i] * sig_2;
            sy2 += src_ds->yd[i] * src_ds->yd[i] * sig_2;
            sx += src_ds->xd[i] * sig_2;
            sx2 += src_ds->xd[i] * src_ds->xd[i] * sig_2;
            sxy += src_ds->xd[i] * src_ds->yd[i] * sig_2;
            ny += sig_2;
            minx = (src_ds->xd[i] < minx) ? src_ds->xd[i] : minx;
            maxx = (src_ds->xd[i] > maxx) ? src_ds->xd[i] : maxx;
            miny = (src_ds->yd[i] < miny) ? src_ds->yd[i] : miny;
            maxy = (src_ds->yd[i] > maxy) ? src_ds->yd[i] : maxy;
        }

        if (!is_affine_fit)
        {
            if (sx2 == 0)
            {
                win_printf_OK("\\Sigma x_i^2 = 0   => cannot perform fit.");
                return NULL;
            }

            a = (sxy / sx2);
            b = 0.;
        }
        else
        {
            b = (sx2 * ny) - sx * sx;

            if (b == 0)
            {
                win_printf_OK("\\Delta  = 0 \n can't fit that!");
                return NULL;
            }

            a = sxy * ny - sx * sy;
            a /= b;
            b = (sx2 * sy - sx * sxy) / b;
        }

        dest_ds = build_data_set(nx, nx);

        if (dest_ds == NULL)
        {
            win_printf_OK("can't create data set");
            return NULL;
        }

        chi2 = 0;

        for (int i = 0; i < nx; i++)
        {
            dest_ds->xd[i] = src_ds->xd[i];
            dest_ds->yd[i] = a * dest_ds->xd[i] + b;
            sig2 = src_ds->ye[i] * src_ds->ye[i];
            sig_2 = (double)1 / sig2;
            chi2 += (a * dest_ds->xd[i] + b - src_ds->yd[i]) * (a * dest_ds->xd[i] + b - src_ds->yd[i]) * sig_2;
        }

        inherit_from_ds_to_ds(dest_ds, src_ds);

        if (is_affine_fit)
        {
            update_ds_treatement(dest_ds, "least square fit y=ax+b, a = %g and b = %g "
                              "\\chi^2 = %g, n = %d", a, b, chi2, nx - 2);
            snprintf(c, sizeof(c), "\\fbox{\\stack{{\\sl y = a x + b}{a = %g}{b = %g}{\\chi^2 = %g}{n = %d}}}"
                    , a, b, chi2, nx - 2);
        }
        else
        {
            update_ds_treatement(dest_ds, "least square fit y=ax, a = %g "
                              "\\chi^2 = %g, n = %d", a, chi2, nx - 2);
            snprintf(c, sizeof(c), "\\fbox{\\stack{{\\sl y = a x}{a = %g}{\\chi^2 = %g}{n = %d}}}", a, chi2, nx - 1);
        }

        set_ds_plot_label(dest_ds, (maxx + minx) / 2, a * (maxx + minx) / 2 + b, USR_COORD, "%s", c);
    }

    if (out_a)
    {
        *out_a = a;
    }

    if (out_b)
    {
        *out_b = b;
    }

    return dest_ds;
}

int compute_least_square_fit_on_ds(const d_s *src_ds, bool is_affine_fit, double *out_a, double *out_b)
{
    return compute_least_square_fit_on_ds_between_indexes(src_ds, is_affine_fit, -1, -1, out_a, out_b);
}


int compute_least_square_fit_on_ds_between_indexes(const d_s *src_ds, bool is_affine_fit, int min_idx, int max_idx,
        double *out_a, double *out_b)
{
    int nx;
    bool has_good_error_bars = true;
    float minx = FLT_MAX;
    float maxx = -FLT_MAX;
    float miny = FLT_MAX;
    float maxy = -FLT_MAX;
    double sy2 = 0, sy = 0, sx = 0, sx2 = 0, sxy = 0, sig2 = 0, sig_2 = 0, ny = 0;
    double a = 0, b = 0;
    double yt = 0, xt = 0;

    assert(src_ds);

    if (min_idx < 0)
    {
        min_idx = 0;
    }

    if (max_idx < 0)
    {
        max_idx = src_ds->nx;
    }

    nx = max_idx - min_idx;

    if (max_idx - min_idx < 2)
    {
        (win_printf_OK("no fit on less than 2 points"));
        return 1;
    }

    if (src_ds->ye == NULL)
    {
        has_good_error_bars = false;
    }
    else
    {
        has_good_error_bars = true;

        for (int i = min_idx; i < max_idx && has_good_error_bars == 1; i++)
        {
            if (src_ds->ye[i] == 0)
            {
                has_good_error_bars = 0;
            }
        }
    }

    if (has_good_error_bars == 0)
    {
        minx = FLT_MAX;
        maxx = -FLT_MAX;
        miny = FLT_MAX;
        maxy = -FLT_MAX;

        for (int i = min_idx; i < max_idx ; i++)
        {
            yt = (double) src_ds->yd[i];
            xt = (double) src_ds->xd[i];

            sy += yt;
            sy2 += yt*yt;
            sx += xt;
            sx2 += xt*xt;
            sxy += xt*yt;
            minx = (xt < minx) ? xt : minx;
            maxx = (xt > maxx) ? xt : maxx;
            miny = (yt < miny) ? yt : miny;
            maxy = (yt > maxy) ? yt : maxy;
        }

        if (!is_affine_fit)
        {
            if (sx2 == 0)
            {
                win_printf_OK("\\Sigma x_i^2 = 0   => cannot perform fit.");
                return 1;
            }

            a = (sxy / sx2);
            b = 0.;
        }
        else
        {
            b = (sx2 * nx) - sx * sx;

            if (b == 0)
            {
                win_printf_OK("\\Delta  = 0 sx2 %lf sx %lf \n can't fit that!",sx2,sx);
                for (int i = min_idx; i < max_idx ; i++)
                {
                  win_printf_OK("xd %d %f",i,src_ds->xd[i]);

                }
                return 1;
            }

            a = sxy * nx - sx * sy;
            a /= b;
            b = (sx2 * sy - sx * sxy) / b;
        }
    }
    else  // with error bars in y
    {
        minx = FLT_MAX;
        maxx = -FLT_MAX;
        miny = FLT_MAX;
        maxy = -FLT_MAX;

        for (int i = min_idx; i < max_idx ; i++)
        {
            sig2 = src_ds->ye[i] * src_ds->ye[i];
            sig_2 = (double)1 / sig2;
            sy += src_ds->yd[i] * sig_2;
            sy2 += src_ds->yd[i] * src_ds->yd[i] * sig_2;
            sx += src_ds->xd[i] * sig_2;
            sx2 += src_ds->xd[i] * src_ds->xd[i] * sig_2;
            sxy += src_ds->xd[i] * src_ds->yd[i] * sig_2;
            ny += sig_2;
            minx = (src_ds->xd[i] < minx) ? src_ds->xd[i] : minx;
            maxx = (src_ds->xd[i] > maxx) ? src_ds->xd[i] : maxx;
            miny = (src_ds->yd[i] < miny) ? src_ds->yd[i] : miny;
            maxy = (src_ds->yd[i] > maxy) ? src_ds->yd[i] : maxy;
        }

        if (!is_affine_fit)
        {
            if (sx2 == 0)
            {
                win_printf_OK("\\Sigma x_i^2 = 0   => cannot perform fit.");
                return 1;
            }

            a = (sxy / sx2);
            b = 0.;
        }
        else
        {
            b = (sx2 * ny) - sx * sx;

            if (b == 0)
            {
                win_printf_OK("\\Delta  = 0 \n can't fit that!");
                return 1;
            }

            a = sxy * ny - sx * sy;
            a /= b;
            b = (sx2 * sy - sx * sxy) / b;
        }

    }

    if (out_a)
    {
        *out_a = a;
    }

    if (out_b)
    {
        *out_b = b;
    }

    return 0;
}


int	find_best_a_an_b_of_exp_fit(d_s *dsi,    // input data set
                                int verbose, // issue explicit error message
                                int error_type, // the type of error in data
                                double tau,  // the imposed tau value
                                double *a,   // the best a value
                                double *b,   // the best b value
                                double *E)   // the chi^2
{
    int i, j;
    double fy, f, x, y, fx, er, la, lb;
    double sy = 0, sy2 = 0, sfx = 0, sfx2 = 0, sfxy = 0, sig_2, ny = 0;

    if (dsi == NULL)
    {
        if (verbose) win_printf("No valid data \n");
        return 1;
    }
    for (i = 0, f = fy = 0; i < dsi->nx; i++)
    {
        x = dsi->xd[i];
        y = dsi->yd[i];
        fx = exp(-x/tau);
        if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
        else if (error_type == 1) er = fx;
        else er = 1;
        if (er > 0)
        {
            sig_2 = (double)1/(er*er);
            sy += y*sig_2;
            sy2 += y*y*sig_2;
            sfx += fx*sig_2;
            sfx2 += fx*fx*sig_2;
            sfxy += y*fx*sig_2;
            ny += sig_2;
        }
    }
    lb = (sfx2 * ny) - sfx * sfx;
    if (lb == 0)
    {
        if (verbose) win_printf_OK("\\Delta  = 0 \n can't fit that!");
        return 3;
    }
    la = sfxy * ny - sfx * sy;
    la /= lb;
    lb = (sfx2 * sy - sfx * sfxy)/lb;
    if (a) *a = la;
    if (b) *b = lb;
    for (i = 0, j = 0, *E = 0; i < dsi->nx; i++)
    {
        x = dsi->xd[i];
        y = dsi->yd[i];
        fx = la*exp(-x/tau) + lb;
        if (error_type == 0 && dsi->ye != NULL)   er = dsi->ye[i];
        else if (error_type == 1) er = fx;
        else er = 1;
        f = y - fx;
        if (er > 0)
        {
            *E += (f*f)/(er*er);
            j++;
        }
    }
    return 0;
}

int MR_cross_product(float* v1, float* v2, float *vout)
{
  vout[0]=v1[1]*v2[2]-v2[1]*v1[2];
  vout[1]=v1[2]*v2[0]-v2[2]*v1[0];
  vout[2]=v1[0]*v2[1]-v2[0]*v1[1];
  return 0;
}

float **MR_smat(float **mat1, float **mat2, int l1, int l2)
{
    int i,j;
    float **out_mat;
    out_mat = calloc(l1 , sizeof(float*));
    for (i=0; i<l1; i++)
    {
      out_mat[i] = calloc(l2 , sizeof(float));
    }
    for (i=0;i<l1;i++)
    {
      for (j=0;j<l2;j++)
    {
      out_mat[i][j]=mat1[i][j]+mat2[i][j];
    }
    }
    return out_mat;
  }

  float  **MR_pmat(float **mat1, float **mat2, int lm11, int lm12, int lm21, int lm22)
  {
    int k,i,j;
    float **out_mat;
    out_mat = calloc(lm11 , sizeof(float*));
    for (i=0; i<lm11; i++)
    {
      out_mat[i] = calloc(lm22 , sizeof(float));
    }
    if (lm12!=lm21) win_printf("It's not possible to multiply theses matrices - incompatible size\n");
    for (i=0;i<lm11;i++)
    {
      for (j=0;j<lm22;j++)
      {
        out_mat[i][j]=0;
        for (k=0;k<lm12;k++) out_mat[i][j]+=mat1[i][k]*mat2[k][j];
      }
    }
    return out_mat;
  }

float MR_vnorm(float *v1, int l)
{
  float n2=0;
  int i;

  for (i=0;i<l;i++) n2+=v1[i]*v1[i];
  return sqrt(n2);
}

float **MR_eye_matrix(int l)
{
  float **r;
  r = calloc(l , sizeof(float*));
  for (int i=0; i<l; i++)
  {
    r[i] = calloc(l , sizeof(float));
  }
  for (int i=0;i<l;i++)
  {
    for (int j=0;j<l;j++)
    r[i][j]=0;
  }
  for (int i=0;i<l;i++) r[i][i]=1;
  return r;
}

float **MR_constant_multiplier_matrix(float** m, float lambda, int l1, int l2)
{
  int i,j;
  float **r;
  r = calloc(l1, sizeof(float*));
  for (i=0; i<l1; i++)
  {
    r[i] = calloc(l2 , sizeof(float));
  }
  for (i=0;i<l1;i++)
  {
    for (j=0;j<l2;j++)
    {
      r[i][j]=m[i][j]*lambda;
    }
  }
  return r;
}


int MR_skew_product(float* v1,  float** out_mat)
{
  out_mat[0][0]=0;
  out_mat[1][1]=0;
  out_mat[2][2]=0;
  out_mat[0][1]=-v1[2];
  out_mat[0][2]=v1[1];
  out_mat[1][0]=v1[2];
  out_mat[1][2]=-v1[0];
  out_mat[2][0]=-v1[1];
  out_mat[2][1]=v1[0];
  return 0;
}


float MR_dot_product(float *v1, float *v2, int l)
{
  float r=0;
  int i;
  for (i=0;i<l;i++) r+=v1[i]*v2[i];
  return r;
}


float **compute_rotation_matrix_bt_2_vectors(float *v1, float *v2)
{
  //FUITES ATTENTION
    float w[3]={0},nw=0;
    float **w_hat;
    float **out_mat,**out_mat_temp,**mattp1,**mattp2;
    w_hat = calloc(3 , sizeof(float*));
    for (int i=0; i<3; i++)
    {
      w_hat[i] = calloc(3 , sizeof(float));
    }
    float co;
    float v1n=MR_vnorm(v1,3);
    float v2n=MR_vnorm(v2,3);
    for (int i=0;i<3;i++)
    {
    v1[i]=v1[i]/v1n;
    v2[i]=v2[i]/v2n;
    }
    MR_cross_product(v1,v2,w);
    nw=MR_vnorm(w,3);
    for (int i=0;i<3;i++) w[i]=w[i]/nw;
    MR_skew_product(w,w_hat);
    co=MR_dot_product(v1,v2,3);
    mattp1=MR_eye_matrix(3);
    mattp2=MR_constant_multiplier_matrix(w_hat,nw,3,3);
    out_mat_temp=MR_smat(mattp1,mattp2,3,3);
    MR_free_matrix(mattp1,3);MR_free_matrix(mattp2,3);
    mattp1=MR_pmat(w_hat,w_hat,3,3,3,3);
    mattp2=MR_constant_multiplier_matrix(mattp1,(1-co),3,3);
    out_mat=MR_smat(out_mat_temp,mattp2,3,3);
    MR_free_matrix(mattp1,3);MR_free_matrix(mattp2,3);MR_free_matrix(w_hat,3);MR_free_matrix(out_mat_temp,3);
    return out_mat;
}

int MR_free_matrix(float **m, int l1)
{
  for (int i=0;i<l1;i++)
  {
    free(m[i]);
  }
  free(m);
  return 0;
}

int do_gaussian_convolution(void)
{
    int i;
    static float biny = 1;
    O_p *op = NULL;
    d_s *dsi = NULL, *ds = NULL;
    pltreg *pr = NULL;

   if(updating_menu_state != 0)	return D_O_K;

    /* display routine action if SHIFT is pressed */
    if (key[KEY_LSHIFT])
      return win_printf_OK("This routine cuts datasets keeping only flat part");

    /* we first find the data that we need to transform */
    if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
      return win_printf_OK("cannot find data");

    i = win_scanf("Size of the gaussian convolution:\n %4f\n",&biny);
    if (i == WIN_CANCEL) return 0;
    ds = build_gaussian_convolution(dsi, biny);
    if (ds == NULL)
      return win_printf_OK("cannot create dataset");
    add_one_plot_data(op, IS_DATA_SET, (void*)ds);

    return refresh_plot(pr, UNCHANGED);
}
