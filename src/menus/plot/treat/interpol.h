/********************************************************************/
/* interpol.h												*/
/* basic data manipulation and treatments for dataset(s)			*/
/********************************************************************/
#include "platform.h"
#include "plot_ds.h"
#include <limits.h>

XV_FUNC(float, interpolate_point_in_array, (float *x, float *y, int nx, float xp, float w_avg, float accuracy));
XV_FUNC(float,	find_max_width_between_succesive_point,(float *x, int nx));
XV_FUNC(d_s *,FIR_and_resample_partial_dataset,(d_s *dsz,int start,int end,int rec_fir));
XV_FUNC(d_s *,resample_by_poly_2_ds_no_verbose,(d_s *dsic,d_s *dsa,int start, int end));
XV_FUNC(d_s *,remove_resampled_FIR,(d_s *dsz, int start, int end, int rec_fir));
