

# include "allegro.h"
# include "float.h"
# include "xvin.h"
# include "plot_op.h"
# include "gr_file.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "plot_reg_gui.h"
# include "imr_reg_gui.h"
# include "plot_mn.h"

# include <stdlib.h>
# include <stdio.h>
# include <malloc.h>
# include <string.h>
int 	do_select_marker_snap(void);
int		do_zoom_in_x_on_op(void);
int		do_unzoom_in_x_on_op(void);
int		do_adjust_y_lim_of_cur_ds_visi(void);
int		do_move_in_x_on_op_2(int index);
int		do_move_in_x_right(void);
int		do_move_in_x_left(void);
int		do_move_in_x_right2(void);
int		do_move_in_x_left2(void);
int		do_adjust_limits_to_cur_ds(void);

int		do_mouse_wheel_zoom_in_on_op(void);
int		do_mouse_wheel_zoom_out_on_op(void);

int do_copy_plot_screen_to_image(void);

#ifdef XV_WIN32
int 	do_check_heap(void)
{
  if(updating_menu_state != 0)	 return 0;
   win_printf("heap check disabled! \n");
   //win_printf("heap status %d (-2 =>OK)\n",_heapchk());
   return 0;
}
#endif




MENU plot_tools_menu[32] =
{
	{ "select limits",    	do_select_pr_area,    	NULL,       0, 			NULL},
	{ "Snap to markers",   	do_select_marker_snap,  NULL,       0, 			NULL},
	{ "zoom in X",			do_zoom_in_x_on_op,		NULL,		0,			NULL},
	{ "zoom out X",			do_unzoom_in_x_on_op,   NULL,       0, 			NULL},
	{ "<< x <<",			do_move_in_x_left2,     NULL,	MENU_INDEX(2),	NULL},
	{ "< x <",				do_move_in_x_left,		NULL, 	MENU_INDEX(1),	NULL},
	{ "> x >",				do_move_in_x_right,		NULL, 	MENU_INDEX(1),	NULL},
	{ ">> x >>",			do_move_in_x_right2,	NULL, 	MENU_INDEX(2),	NULL},
	{ "adj. y lim.",		do_adjust_y_lim_of_cur_ds_visi,	NULL,	0,		NULL},
	{ "adj. ds lim.",		do_adjust_limits_to_cur_ds,	NULL,	0,		NULL},
#ifdef XV_WIN32
	{ "Check memory.",		do_check_heap,	NULL,	0,		NULL},
#endif
	{ "Copy screen area to Copied image",		do_copy_plot_screen_to_image,	NULL,	0,		NULL},


	{ NULL,                          NULL,             NULL,       0, 		NULL}
};

int	add_plot_tools_menu_item( char* text, int (*proc)(void), struct MENU* child, int flags, void* dp)
{
	int i;
	for (i = 0; i < 32 && plot_tools_menu[i].text != NULL; i++);
	if (i >= 30)	return -1;
	plot_tools_menu[i].text = text;
	plot_tools_menu[i].proc = proc;
	plot_tools_menu[i].child = child;
	plot_tools_menu[i].flags = flags;
	plot_tools_menu[i].dp = dp;
	plot_tools_menu[i+1].text = NULL;
	return 0;
}

int	find_nearest_points_in_x_in_ds(d_s *ds, float x, int* nearest_smaller, int* nearest_larger, float min_y, float max_yl)
{
	int i, j, k;
	float x0, x1, xt, yt;

	if (ds == NULL)	return 1;
	for (i = j = k = 0, x0 = x1 = ds->xd[0]; i < ds->nx; i++)
	{
		xt = ds->xd[i];
		yt = ds->yd[i];
		if (yt < min_y || yt > max_yl)	continue;
		if ((x - xt) >= 0 && (x - xt < fabs(x - x0)))
		{
			x0 = xt;
			j = i;
		}
		if ((xt - x) >= 0 && (xt - x < fabs(x1 - x)))
		{
			x1 = xt;
			k = i;
		}
	}
	*nearest_smaller = j;
	*nearest_larger = k;
	return 0;
}



int	find_nearest_points_in_y_in_ds(d_s *ds, float y, int* nearest_smaller, int* nearest_larger, float min_x, float max_xl)
{
	int i, j, k;
	float y0, y1, yt, xt;

	if (ds == NULL)	return 1;
	for (i = j = k = 0, y0 = y1 = ds->yd[0]; i < ds->nx; i++)
	{
		yt = ds->yd[i];
		xt = ds->xd[i];
		if (xt < min_x || xt > max_xl)	continue;
		if ((y - yt) >= 0 && (y - yt < fabs(y - y0)))
		{
			y0 = yt;
			j = i;
		}
		if ((yt - y) >= 0 && (yt - y < fabs(y1 - y)))
		{
			y1 = yt;
			k = i;
		}
	}
	*nearest_smaller = j;
	*nearest_larger = k;
	return 0;
}



int	find_nearest_points_in_in_ds(d_s *ds, float x, float y, int* nearest_in_x, int* nearest_in_y)
{
	int i, j, k;
	float y0, x0, yt, xt;
	clock_t time = 0;

	if (ds == NULL)	return 1;
	for (i = j = k = 0, y0 = ds->yd[0], x0 = ds->xd[0], time = clock(); i < ds->nx; i++)
	{
	  if (8*(clock() - time) > CLOCKS_PER_SEC)  return 1;
	  xt = ds->xd[i];
	  yt = ds->yd[i];
	  if (fabs(yt - y) < fabs(y0 - y))
	    {
	      y0 = yt;
	      j = i;
	    }
	  if (fabs(xt - x) < fabs(x0 - x))
	    {
	      x0 = xt;
	      k = i;
	    }
	}
	*nearest_in_y = j;
	*nearest_in_x = k;
	return 0;
}



/* To crop an image to the visible portion of it that is visible on the screen */
int do_copy_plot_screen_to_image(void)
{
    int i;
    static int x0 = 0, y0 = 0, x1 = 256, y1 = 256;
    O_i  *oid = NULL;
    pltreg *pr = NULL;

    if (updating_menu_state != 0)  return D_O_K;
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("To convert a screen piece in  an image \n\n"
                             "A new image is created");
    }

    if (ac_grep(cur_ac_reg, "%pr", &pr) != 1)      return OFF;

    if (pr == NULL)                  return win_printf_OK("I cannot find any plot !");


    i = win_scanf("Define the Scrren area to copy:\n"
                  "X0 = %4d Y0 = %4d\n"
                  "X1 = %4d Y1 = %4d\n",&x0, &y0, &x1, &y1);
    if (i == WIN_CANCEL) return 0;

    x0 = (x0 < 0) ? 0 : x0;
    y0 = (y0 < 0) ? 0 : y0;
    x1 = (x1 <= screen->w) ? x1 : screen->w;
    y1 = (y1 <= screen->h) ? y1 : screen->h;

    oid = convert_bitmap_to_oi(screen, NULL, x0, y0, x1, y1);
    if (oid == NULL)      return (win_printf("Can't create dest image"));
    win_printf("Screen was copied 0");
    smart_map_pixel_ratio_of_image_and_screen(oid);
    set_zmin_zmax_values(oid, 0, 255);
    set_z_black_z_white_values(oid, 0, 255);

    set_im_title(oid, "Screen copy (%d,%d,%d,%d)",x0, y0, x1, y1);
    win_printf("Screen was copied");
    if (oi_copied != NULL) free_one_image(oi_copied);
    oi_copied = oid;

    return 0;
}



int	find_nearest_points_in_plot(O_p *op, float xm, float ym, int *nearest_ds, int *nearest_point, int time_out)
{
  int i;
  int ids, idsn, index;
  float  yt, xt;
  double d, d0, tmp;
  d_s *ds = NULL;
  clock_t time = 0;

  if ((op == NULL) || ((op->x_hi - op->x_lo) == 0) || ((op->y_hi - op->y_lo) == 0)) return 1;
  xm = xm - op->ax;
  if (op->dx != 0) xm /= op->dx;
  ym = ym - op->ay;
  if (op->dy != 0) ym /= op->dy;
  for (ids = 0, idsn = -1, index = -1, d0 = -1, time = clock(); ids < op->n_dat; ids++)
    {
      ds = op->dat[ids];
      if (ds->invisible) continue;
      if ((time_out > 0) && (8*(clock() - time) > CLOCKS_PER_SEC))  return 1;
      for (i = 0; i < ds->nx; i++)
	{
	  yt = ds->yd[i];
	  xt = ds->xd[i];
	  if (op->iopt & XLOG)
	    {
	      if (op->x_lo != 0 && xm != 0 && xt > 0)
		tmp = log10(xt/xm)/log10(op->x_hi/op->x_lo);
	      else tmp = 100000;
	    }
	  else tmp = (xt - xm) / (op->x_hi - op->x_lo);
	  d = tmp * tmp;
	  if (op->iopt & YLOG)
	    {
	      if (op->y_lo != 0 && ym != 0 && yt > 0)
		tmp = log10(yt/ym)/log10(op->y_hi/op->y_lo);
	      else tmp = 100000;
	    }
	  else tmp = (yt - ym) / (op->y_hi - op->y_lo);
	  d += tmp * tmp;
	  if (d0 < 0)
	    {
	      index = i;
	      idsn = ids;
	      d0 = d;
	    }
	  if (d < d0)
	    {
	      index = i;
	      idsn = ids;
	      d0 = d;
	    }
	}
    }
  *nearest_ds = idsn;
  *nearest_point = index;
  return 0;
}





int find_selected_pr_area(pltreg *pr, int x_0, int y_0, DIALOG *d)
{
  int   dx = 0, dy = 0, ldx, ldy, color;//, sc;
	float x, y, xa, ya;
	//	struct box *b;
	O_p *op = NULL;


	if (pr->one_p == NULL)		return 1;
	op = pr->one_p;
	//sc = pr->screen_scale;
	//b = pr->stack;
	y = ya = y_pr_2_pltdata_raw(pr, y_0);// - d->y);
	x = xa = x_pr_2_pltdata_raw(pr, x_0);// - d->x);
	ldy = 0;
	ldx = 0;
	color = makecol(255, 64, 64);
	while (mouse_b & 0x03)
	{
		dx = mouse_x - x_0;
		dy = mouse_y - y_0;
		if  (ldy != dy || dx != ldx)
		{
		  blit_plot_reg(d);
		  for(;screen_acquired;); // we wait for screen_acquired back to 0;     
		  screen_acquired = 1;					  
		  acquire_bitmap(screen);
		  rect(screen,x_0,y_0,x_0 + dx,y_0 + dy,color);
		  release_bitmap(screen);
		  screen_acquired = 0;					  
		  draw_bubble(screen, B_CENTER, x_0, y_0-40, "resizing plot (ESC to abort)");
		  draw_bubble(screen, B_LEFT, x_0, y_0-20, "x0 = %g y0 %g",
			      x_pr_2_pltdata(pr,x_0),y_pr_2_pltdata(pr,y_0));
		  draw_bubble(screen, B_RIGHT, x_0 + dx, y_0 + dy + 20, "x1 = %g y2 %g",
			      x_pr_2_pltdata(pr,x_0 + dx),y_pr_2_pltdata(pr,y_0 + dy));
		  y = y_pr_2_pltdata_raw(pr, y_0 + dy);
		  x = x_pr_2_pltdata_raw(pr, x_0 + dx);
		  ldx = dx;
		  ldy = dy;
		}
		if (key[KEY_ESC])
		{
			clear_keybuf();
			return D_REDRAWME;
		}

	}
	if ( abs(dy) > 5 && abs(dx) > 5)
	{
		if (y > ya)
		{
			op->y_hi = y;
			op->y_lo = ya;
		}
		else
		{
			op->y_hi = ya;
			op->y_lo = y;
		}
		if (x > xa)
		{
			op->x_hi = x;
			op->x_lo = xa;
		}
		else
		{
			op->x_hi = xa;
			op->x_lo = x;
		}
		op->iopt2 |= Y_LIM;
		op->iopt2 |= X_LIM;
		op->need_to_refresh = 1;
		return D_REDRAWME;
	}
	return D_O_K;
}
int 	do_select_pr_area(void)
{
	if(updating_menu_state != 0)
	{

		if (general_pr_action == find_selected_pr_area)
			active_menu->flags |= D_SELECTED;
		else active_menu->flags &= ~D_SELECTED;
		return D_O_K;
	}
	prev_general_pr_action = general_pr_action;
	general_pr_action = find_selected_pr_area;
	return D_O_K;
}

int		display_nearest_points_in_x(pltreg *pr, O_p *op, float x, DIALOG *d)
{
	int i0 = 0, i1 = 0;
	float y0, y1, x0, x1;
	d_s *ds = NULL;
	
	(void)d;
	ds = op->dat[op->cur_dat];
	find_nearest_points_in_x_in_ds(ds, x, &i0, &i1, op->y_lo, op->y_hi);
	y0 = ds->yd[i0];
	y0 = (y0 < op->y_lo) ? op->y_lo : y0;
	y0 = (y0 > op->y_hi) ? op->y_hi : y0;
	y1 = ds->yd[i1];
	y1 = (y1 < op->y_lo) ? op->y_lo : y1;
	y1 = (y1 > op->y_hi) ? op->y_hi : y1;
	x0 = ds->xd[i0];
	x0 = (x0 < op->x_lo) ? op->x_lo : x0;
	x0 = (x0 > op->x_hi) ? op->x_hi : x0;
	x1 = ds->xd[i1];
	x1 = (x1 < op->x_lo) ? op->x_lo : x1;
	x1 = (x1 > op->x_hi) ? op->x_hi : x1;
	draw_bubble(screen,B_LEFT,x_pltdata_2_pr(pr,x1)+10,y_pltdata_2_pr(pr,y1),
	"x[%d] %g y[%d] %g",i1,ds->xd[i1],i1,ds->yd[i1]);
	draw_bubble(screen,B_RIGHT,x_pltdata_2_pr(pr,x0)-10,y_pltdata_2_pr(pr,y0),
	"x[%d] %g y[%d] %g",i0,ds->xd[i0],i0,ds->yd[i0]);

	return D_O_K;
}


int		display_nearest_points_in_y(pltreg *pr, O_p *op, float y, DIALOG *d)
{
	int i0 = 0, i1 = 0;
	float x0, x1, y0, y1;
	d_s *ds = NULL;
	
	(void)d;
	ds = op->dat[op->cur_dat];
	find_nearest_points_in_y_in_ds(ds, y, &i0, &i1,op->x_lo, op->x_hi);
	y0 = ds->yd[i0];
	y0 = (y0 < op->y_lo) ? op->y_lo : y0;
	y0 = (y0 > op->y_hi) ? op->y_hi : y0;
	y1 = ds->yd[i1];
	y1 = (y1 < op->y_lo) ? op->y_lo : y1;
	y1 = (y1 > op->y_hi) ? op->y_hi : y1;
	x0 = ds->xd[i0];
	x0 = (x0 < op->x_lo) ? op->x_lo : x0;
	x0 = (x0 > op->x_hi) ? op->x_hi : x0;
	x1 = ds->xd[i1];
	x1 = (x1 < op->x_lo) ? op->x_lo : x1;
	x1 = (x1 > op->x_hi) ? op->x_hi : x1;
/*	draw_bubble(screen,0,500,64,"y = %g y_{off} %d y_{s_ny} %d mouse y %d d->y %d",y,pr->y_off,pr->s_ny,mouse_y, d->y);	*/
	draw_bubble(screen,B_LEFT,x_pltdata_2_pr(pr,x1),y_pltdata_2_pr(pr,y1)-10,
	"x[%d] %g y[%d] %g",i1,ds->xd[i1],i1,ds->yd[i1]);
	draw_bubble(screen,B_RIGHT,x_pltdata_2_pr(pr,x0),y_pltdata_2_pr(pr,y0)+10,
	"x[%d] %g y[%d] %g",i0,ds->xd[i0],i0,ds->yd[i0]);

	return D_O_K;
}

int 	do_select_marker_snap(void)
{

	if(updating_menu_state != 0)
	{

		if ((horz_marker_plt_drag_action == display_nearest_points_in_x) &&
			(vert_marker_plt_drag_action == display_nearest_points_in_y))
			active_menu->flags |= D_SELECTED;
		else active_menu->flags &= ~D_SELECTED;
		return D_O_K;
	}

	horz_marker_plt_drag_action = display_nearest_points_in_x;
	vert_marker_plt_drag_action = display_nearest_points_in_y;
	return D_O_K;
}


int		do_zoom_in_x_on_op(void)
{
	int imin, imax, j;
	O_p *op = NULL;
	d_s *dsi = NULL;
	pltreg *pr = NULL;
	int nf;
	static float factor = 2;
	float min, max, lo, hi;

	if(updating_menu_state != 0)
	{
	    add_keyboard_short_cut(0, KEY_UP, 0, do_zoom_in_x_on_op);
		return D_O_K;
	}
    if (key[KEY_LSHIFT])   return win_printf_OK(
		"This routine zoom in the x direction of a plot");
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf("cannot find data");

	nf = dsi->nx;	/* this is the number of points in the data set */

	lo = (op->x_lo + op->x_hi)/2 - (op->x_hi - op->x_lo)/(2*factor);
	hi = (op->x_lo + op->x_hi)/2 + (op->x_hi - op->x_lo)/(2*factor);
/*	win_printf("lo %g hi %g -> lo %g hi %g",op->x_lo,op->x_hi,lo,hi);*/
	op->x_lo = lo;
	op->x_hi = hi;
	min = op->y_lo;
	max = op->y_hi;
	for (imin = imax = -1, j = 0; j < nf; j++)
	{
		if (dsi->xd[j] <= hi && dsi->xd[j] >= lo)
		{
			if (imin == -1 || dsi->yd[j] < min)
			{
				min = dsi->yd[j];
				imin = 0;
			}
			if (imax == -1 || dsi->yd[j] > max)
			{
				max = dsi->yd[j];
				imax = 0;
			}
		}
	}
	op->iopt2 |= X_LIM;
	lo = (min + max)/2;
	hi = (max - min)/2;
	min = lo - 1.05 * hi;
	max = lo + 1.05 * hi;
/*	win_printf("lo %g hi %g -> max %g min %g",op->x_lo,op->x_hi,max,min);	*/
	if (imin == 0 && imax == 0)
	{
		if (min < op->y_lo || max > op->y_hi)
		{
			if (max-min > op->y_hi - op->y_lo)
			{
				op->y_lo = min;
				op->y_hi = max;
			}
			else
			{
				lo = (min+max)/2 - (op->y_hi - op->y_lo)/2;
				hi = (min+max)/2 - (op->y_hi + op->y_lo)/2;
				op->y_lo = lo;
				op->y_hi = hi;
			}
			op->iopt2 |= Y_LIM;
/*			win_printf("Y lo %g hi %g",op->y_lo,op->y_hi);	*/

		}
	}
	op->need_to_refresh = 1;
	refresh_plot(pr, UNCHANGED);
	return 0;
}

int		do_unzoom_in_x_on_op(void)
{
	int imin, imax, j;
	O_p *op = NULL;
	d_s *dsi = NULL;
	pltreg *pr = NULL;
	int nf;
	static float factor = 2;
	float min, max, lo, hi;

	if(updating_menu_state != 0)
	{
	    add_keyboard_short_cut(0, KEY_DOWN, 0, do_unzoom_in_x_on_op);
		return D_O_K;
	}
	if (key[KEY_LSHIFT])   return win_printf_OK(
		"This routine zoom in the x direction of a plot");
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf("cannot find data");

	nf = dsi->nx;	/* this is the number of points in the data set */

	lo = ((fabs(op->x_lo) < FLT_MAX/4) && (fabs(op->x_hi) < FLT_MAX/4))
		? (op->x_lo + op->x_hi)/2 : FLT_MAX/4;
	hi = ((fabs(op->x_lo) < FLT_MAX/4) && (fabs(op->x_hi) < FLT_MAX/4))
		? (op->x_hi - op->x_lo)*factor/2 : FLT_MAX/4;

	op->x_lo = lo - hi;
	op->x_hi = lo + hi;
	min = op->y_lo;
	max = op->y_hi;
	for (imin = imax = -1, j = 0; j < nf; j++)
	{
		if (dsi->xd[j] <= hi && dsi->xd[j] >= lo)
		{
			if (imin == -1 || dsi->yd[j] < min)
			{
				min = dsi->yd[j];
				imin = 0;
			}
			if (imax == -1 || dsi->yd[j] > max)
			{
				max = dsi->yd[j];
				imax = 0;
			}
		}
	}
	op->iopt2 |= X_LIM;
	lo = (min + max)/2;
	hi = (max - min)/2;
	min = lo - 1.05 * hi;
	max = lo + 1.05 * hi;
	if (imin == 0 && imax == 0)
	{
		if (min < op->y_lo || max > op->y_hi)
		{
			if (max-min > op->y_hi - op->y_lo)
			{
				op->y_lo = min;
				op->y_hi = max;
			}
			else
			{
				lo = (min+max)/2 - (op->y_hi - op->y_lo)/2;
				hi = (min+max)/2 - (op->y_hi + op->y_lo)/2;
				op->y_lo = lo;
				op->y_hi = hi;
			}
			op->iopt2 |= Y_LIM;
		}
	}
	op->need_to_refresh = 1;
	refresh_plot(pr, UNCHANGED);
	return 0;
}


int		do_mouse_wheel_zoom_in_on_op(void)
{
  //int imin, imax, j;
	O_p *op = NULL;
	d_s *dsi = NULL;
	pltreg *pr = NULL;
	//int nf;
	static float factor = 1.05;
	float xm, ym, lo, hi;

	if(updating_menu_state != 0)
	{
	    add_keyboard_short_cut(0, KEY_UP, 0, do_mouse_wheel_zoom_in_on_op);
		return D_O_K;
	}
	//	if (key[KEY_LSHIFT])   return win_printf_OK(
	//"This routine zoom in the x direction of a plot");
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf("cannot find data");

	//nf = dsi->nx;	/* this is the number of points in the data set */



	xm = x_pr_2_pltdata(pr, mouse_x);
	ym = y_pr_2_pltdata(pr, mouse_y);
	if (key[KEY_LCONTROL] == 0)
	  {
	    lo = xm - (xm - (op->ax + op->dx * op->x_lo))/factor;
	    hi = xm + ((op->ax + op->dx * op->x_hi) - xm)/factor;

	    lo -= op->ax;
	    op->x_lo = (op->dx != 0) ? lo/op->dx: lo;
	    hi -= op->ax;
	    op->x_hi = (op->dx != 0) ? hi/op->dx: hi;

/*	win_printf("lo %g hi %g -> lo %g hi %g",op->x_lo,op->x_hi,lo,hi);*/
	    //op->x_lo = lo;
	    //op->x_hi = hi;
	  }
	if (key[KEY_LSHIFT] == 0)
	  {
	    lo = ym - (ym - (op->ay + op->dy * op->y_lo))/factor;
	    hi = ym + ((op->ay + op->dy * op->y_hi) - ym)/factor;

	    lo -= op->ay;
	    op->y_lo = (op->dy != 0) ? lo/op->dy: lo;
	    hi -= op->ay;
	    op->y_hi = (op->dy != 0) ? hi/op->dy: hi;

	    //op->y_lo = lo;
	    //op->y_hi = hi;
	  }
	op->iopt2 |= X_LIM | Y_LIM;
	op->need_to_refresh = 1;
	refresh_plot(pr, UNCHANGED);
	return 0;
}

int		do_mouse_wheel_zoom_out_on_op(void)
{
  //int imin, imax, j;
	O_p *op = NULL;
	d_s *dsi = NULL;
	pltreg *pr = NULL;
	//int nf;
	static float factor = 1.05;
	float xm, ym, lo, hi;

	if(updating_menu_state != 0)
	{
	    add_keyboard_short_cut(0, KEY_DOWN, 0, do_mouse_wheel_zoom_out_on_op);
		return D_O_K;
	}

	  /*
	  return win_printf_OK(
		"This routine zoom in the x direction of a plot");
	  */
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf("cannot find data");

	//nf = dsi->nx;	/* this is the number of points in the data set */


	xm = x_pr_2_pltdata(pr, mouse_x);
	ym = y_pr_2_pltdata(pr, mouse_y);
	if (key[KEY_LCONTROL] == 0)
	  {
	    lo = xm - factor * (xm - (op->ax + op->dx * op->x_lo));
	    hi = xm + factor * ((op->ax + op->dx * op->x_hi) - xm);
	    lo -= op->ax;
	    op->x_lo = (op->dx != 0) ? lo/op->dx: lo;
	    hi -= op->ax;
	    op->x_hi = (op->dx != 0) ? hi/op->dx: hi;

	    //op->x_lo = lo;
	    //op->x_hi = hi;
	  }
	if (key[KEY_LSHIFT] == 0)
	  {
	    lo = ym - factor * (ym - (op->ay + op->dy * op->y_lo));
	    hi = ym + factor * ((op->ay + op->dy * op->y_hi) - ym);

	    lo -= op->ay;
	    op->y_lo = (op->dy != 0) ? lo/op->dy: lo;
	    hi -= op->ay;
	    op->y_hi = (op->dy != 0) ? hi/op->dy: hi;

	    //op->y_lo = lo;
	    //op->y_hi = hi;
	  }
	op->iopt2 |= X_LIM | Y_LIM;
	op->need_to_refresh = 1;
	refresh_plot(pr, UNCHANGED);
	return 0;
}


int		do_move_in_x_on_op_2(int index)
{
	int imin, imax, j;
	O_p *op = NULL;
	d_s *dsi = NULL;
	pltreg *pr = NULL;
	int nf;
	float min, max, lo, hi;

	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf("cannot find data");


	nf = dsi->nx;	/* this is the number of points in the data set */
	lo = (op->x_lo + op->x_hi)/2;
	hi = (op->x_hi - op->x_lo)/2;
	if (index == 2)
	{
		op->x_lo = lo;
		op->x_hi = lo + 2*hi;
	}
	else if (index == -2)
	{
		op->x_lo = lo - 2*hi;
		op->x_hi = lo;
	}
	else if (index == -1)
	{
		op->x_lo = lo - hi - hi/2;
		op->x_hi = lo + hi - hi/2;
	}
	else if (index == 1)
	{
		op->x_lo = lo - hi + hi/2;
		op->x_hi = lo + hi + hi/2;
	}
	else return OFF;
	lo = op->x_lo;
	hi = op->x_hi;
	min = op->y_lo;
	max = op->y_hi;

	for (imin = imax = -1, j = 0; j < nf; j++)
	{
		if (dsi->xd[j] <= hi && dsi->xd[j] >= lo)
		{
			if (imin == -1 || dsi->yd[j] < min)
			{
				min = dsi->yd[j];
				imin = 0;
			}
			if (imax == -1 || dsi->yd[j] > max)
			{
				max = dsi->yd[j];
				imax = 0;
			}
		}
	}
	op->iopt2 |= X_LIM;
	lo = (min + max)/2;
	hi = (max - min)/2;
	min = lo - 1.05 * hi;
	max = lo + 1.05 * hi;
/*	win_printf("lo %g hi %g -> max %g min %g",op->x_lo,op->x_hi,max,min);	*/
	if (imin == 0 && imax == 0)
	{
		if (min < op->y_lo || max > op->y_hi)
		{
			if (max-min > op->y_hi - op->y_lo)
			{
				op->y_lo = min;
				op->y_hi = max;
			}
			else
			{
				lo = (min+max)/2 - (op->y_hi - op->y_lo)/2;
				hi = (min+max)/2 + (op->y_hi - op->y_lo)/2;
				op->y_lo = lo;
				op->y_hi = hi;
			}
			op->iopt2 |= Y_LIM;
			display_title_message("Y changed lo %g hi %g",op->y_lo,op->y_hi);

		}
	}

	op->need_to_refresh = 1;
 	refresh_plot(pr, UNCHANGED);
// 	display_title_message("dialog size %d alloc %d",number_of_item_from_dialog(),m_the_dialog);

	return 0;
}

int   select_plot_wheel_action(pltreg *pr, O_p *op, DIALOG *d, int ticks)
{
  (void)pr;
  (void)d;
  (void)op;
    if (ticks > 0)
      {
	for( ; ticks > 0; ticks--)  //do_move_in_x_left();
	  do_mouse_wheel_zoom_in_on_op();
      }
    else if (ticks < 0)
      {
	for(ticks = -ticks ; ticks > 0; ticks--)  //do_move_in_x_right();
	  do_mouse_wheel_zoom_out_on_op();
      }
    return 0;
}

int		do_move_in_x_right(void)
{
	if(updating_menu_state != 0)
	{
	    add_keyboard_short_cut(0, KEY_RIGHT, 0, do_move_in_x_right);
		if (plot_wheel_action == NULL)
			plot_wheel_action = select_plot_wheel_action;
		return D_O_K;
	}

    if (key[KEY_LSHIFT])   return win_printf_OK(
		"This routine move in the x direction of a plot");
	if (key[KEY_RSHIFT])	return do_move_in_x_on_op_2(2);
	return do_move_in_x_on_op_2(1);
}

int		do_move_in_x_left(void)
{
	if(updating_menu_state != 0)
	{
	    add_keyboard_short_cut(0, KEY_LEFT, 0, do_move_in_x_left);
		return D_O_K;
	}
    if (key[KEY_LSHIFT])   return win_printf_OK(
		"This routine move in the x direction of a plot");
	if (key[KEY_RSHIFT])	return do_move_in_x_on_op_2(-2);
	return do_move_in_x_on_op_2(-1);
}

int		do_move_in_x_right2(void)
{
	if(updating_menu_state != 0)
	{
	  //add_keyboard_short_cut(0, KEY_F2, 0, do_move_in_x_right2);
		return D_O_K;
	}
    if (key[KEY_LSHIFT])   return win_printf_OK(
		"This routine move in the x direction of a plot");
	return do_move_in_x_on_op_2(2);
}

int		do_move_in_x_left2(void)
{
	if(updating_menu_state != 0)
	{
	  //add_keyboard_short_cut(0, KEY_F1, 0, do_move_in_x_left2);
		return D_O_K;
	}
    if (key[KEY_LSHIFT])   return win_printf_OK(
		"This routine move in the x direction of a plot");
	return do_move_in_x_on_op_2(-2);
}

int		do_adjust_y_lim_of_cur_ds_visi(void)
{
	int imin, imax, j;
	O_p *op = NULL;
	d_s *dsi = NULL;
	pltreg *pr = NULL;
	int nf;
	float min, max, lo, hi;

	if(updating_menu_state != 0)	return D_O_K;
    if (key[KEY_LSHIFT])   return win_printf_OK(
	"This routine adjust y limits of a plot \n"
	"according to the current data set");
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op%ds",&pr,&op,&dsi) != 3)
		return win_printf("cannot find data");

	nf = dsi->nx;	/* this is the number of points in the data set */

	lo = op->x_lo;
	hi = op->x_hi;
	min = op->y_lo;
	max = op->y_hi;
	for (imin = imax = -1, j = 0; j < nf; j++)
	{
		if (dsi->xd[j] <= hi && dsi->xd[j] >= lo)
		{
			if (imin == -1 || dsi->yd[j] < min)
			{
				min = dsi->yd[j];
				imin = 0;
			}
			if (imax == -1 || dsi->yd[j] > max)
			{
				max = dsi->yd[j];
				imax = 0;
			}
		}
	}
	op->iopt2 |= X_LIM;
	lo = (min + max)/2;
	hi = (max - min)/2;
	min = lo - 1.05 * hi;
	max = lo + 1.05 * hi;
/*	win_printf("lo %g hi %g -> max %g min %g",op->x_lo,op->x_hi,max,min);	*/
	if (imin == 0 && imax == 0)
	{
		op->y_lo = min;
		op->y_hi = max;
		op->iopt2 |= Y_LIM;
		display_title_message("Y changed lo %g hi %g",op->y_lo,op->y_hi);
	}
	op->need_to_refresh = 1;
	refresh_plot(pr, UNCHANGED);
	return 0;
}


int		do_adjust_limits_to_cur_ds(void)
{
	pltreg *pr = NULL;
	O_p *op = NULL;


	if(updating_menu_state != 0)
	{
	    add_keyboard_short_cut(0, KEY_HOME, 0, do_adjust_limits_to_cur_ds);
		return D_O_K;
	}

	if (key[KEY_LSHIFT])   return win_printf_OK(
	"This routine adjust x and y limits of a plot \n"
	"according to the current data set");
	/* we first find the data that we need to transform */
	if (ac_grep(cur_ac_reg,"%pr%op",&pr,&op) != 2)
		return win_printf("cannot find data");

    adjust_limits_on_ds_range(op, op->cur_dat);

	return 0;
}











