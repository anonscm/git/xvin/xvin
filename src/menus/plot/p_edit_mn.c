# include "xvin.h"
# include "allegro.h"
# include "plot_op.h"
# include "gr_file.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "plot_reg_gui.h"
# include "plot_mn.h"

# include <stdlib.h>
# include <string.h>
#ifndef XV_WIN32
# include <sys/stat.h>
# else
AL_FUNC(int, file_exists, (AL_CONST char *filename, int attrib, int *aret));
#endif




int add_error_bars_in_x(void);
int add_error_bars_in_y(void);
int remove_error_bars_in_x(void);
int remove_error_bars_in_y(void);
int convert_error_bars_in_x_in_ds(void);
int convert_error_bars_in_y_in_ds(void);
int set_label(void);
int generate_plot_plugin_template(void);
int do_menu_scan(void);
int     paste_plot(void);
int     paste_ds_as_plot(void);
int     paste_ds(void);
int     add_ds_in_copied(void);
int     generate_description(void);
char *set_to_white(char *text);
int convert_to_printable_colors(void);
int     ds_copy(void);
int     op_copy1(void);
int     set_source(void);
int     set_treatement(void);
int     paste_all_ds_from_op_copied(void);
int     add_all_ds_to_copied_op(void);

// /!\ Last string argument of a MENU should not be a const string , but an allocated char *, stack overflow
// could append depending on allegro internal implementation  --> const is discarded by explicite
// cast
MENU plot_label_menu[32] =
{
    {"Title",                       set_label,  NULL,   0, (char *)  "0"},
    {"Title in X",                  set_label,  NULL,   0, (char *)  "1"},
    {"Title in Y",                  set_label,  NULL,   0, (char *)  "2"},
    {"Top axis Title",              set_label,  NULL,   0, (char *)  "6"},
    {"Right Title",                 set_label,  NULL,   0, (char *)  "7"},
    { "\0",                         NULL,       NULL,   0, NULL},
    {"Edit source", set_source,  NULL,   0, (char *)  "3"},
    {"Edit treatement", set_treatement,  NULL,   0, (char *)  "3"},
    {"label in absolute coordinate", set_label,  NULL,   0, (char *)  "3"},
    {"label in plot coordinate",    set_label,  NULL,   0, (char *)  "4"},
    {"Vertical label",              set_label,  NULL,   0, (char *)  "5"},
    { "\0",                         NULL,       NULL,   0, NULL},
    {"Generate description",     generate_description,  NULL,   0, (char *)  "3"},
    {"Convert to printable colors", convert_to_printable_colors, NULL, 0, (char *) "8"},
    {NULL,                          NULL,       NULL,   0, NULL }
};

MENU plot_error_menu[32] =
{
    {"Add errors in X",         add_error_bars_in_x,    NULL,   0,  NULL},
    {"Convert errors in X in ds",   convert_error_bars_in_x_in_ds,  NULL, 0, NULL},
    {"Remove errors in X",      remove_error_bars_in_x,     NULL,   0,  NULL},
    {"Add errors in Y",         add_error_bars_in_y,    NULL,   0,  NULL},
    {"Convert errors in Y in ds",   convert_error_bars_in_y_in_ds,  NULL, 0, NULL},
    {"remove errors in Y",      remove_error_bars_in_y,     NULL,   0,  NULL},
    {NULL,                      NULL,       NULL,   0, NULL }
};

int do_add_point_to_plot(pltreg *pr, O_p *op, d_s *ds)
{
    int i;
    static float x, y;
    float xt, yt;
    static int go_on = 1, x_auto = 0;
    char question[1024] = {0};

    if (pr == NULL || op == NULL || ds == NULL) return 0;

    if (x_auto)      x = op->ax + op->dx * ds->nx;

    sprintf(question, "Adding point %d to plot\n"
            "Do you want X axis automatic  %%b\n"
            "Otherwise X axis value %%12f\n\n"
            "  Specify Y axis value %%12f\n\n"
            "Have you finished entring point series\n"
            "yes %%R no %%r", ds->nx);
    i = win_scanf(question, &x_auto, &x, &y, &go_on);

    if (i == WIN_CANCEL) return 0;

    if (x_auto)      xt = ds->nx;
    else      xt = (op->dx != 0) ? (x - op->ax) / op->dx : x;

    yt = (op->dy != 0) ? (y - op->ay) / op->dy : y;
    add_new_point_to_ds(ds, xt, yt);
    op->need_to_refresh = 1;
    refresh_plot(pr, pr->n_op - 1);
    return go_on;
}

int add_point_to_plot(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;

    if (updating_menu_state != 0)      return 0;

    if (ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &ds) != 3)   return OFF;

    if (ds->symb == NULL) set_plot_symb(ds, "\\pt6\\oc");

    while (do_add_point_to_plot(pr, op, ds));

    return 0;
}


MENU plot_edit_menu[32] =
{
    {"Add point to plot",    add_point_to_plot,    NULL,       0, NULL  },
    {"\0",           NULL,          NULL,   0, NULL},
    {"Dataset &Copy\tAlt+C",    ds_copy,    NULL,       0, NULL  },
    {"Paste ds \tAlt+&V",        paste_ds,   NULL,   0,  NULL},
    {"copy ds to copied plot (shift multi)",      add_ds_in_copied,   NULL,   0,  NULL},
    {"copy all ds to copied plot (shift multi)",      add_all_ds_to_copied_op,    NULL,   0,  NULL},

    {"Paste all ds from last copied plot",         paste_all_ds_from_op_copied,     NULL,   0,  NULL},

    {"Kill dataset\tCtrl-K",    ds_menu_kill_ds,       NULL,       0, NULL  },
    {"Data set handling",       NULL,   ds_menu,    0, (char *)  "1"},
    {"\0",           NULL,          NULL,   0, NULL},
    {"Copy plot\tCtrl-C",    op_copy1,       NULL,       0, NULL  },
    {"Paste plot\tCtrl-V",      paste_plot,     NULL,   0,  NULL},
    {"Kill plot\tCtrl-X",    op_menu_kill_op,       NULL,       0, NULL  },
    {"Kill plot to the right",    op_menu_kill_op_to_right,       NULL,       0, NULL  },
    {"Paste ds as plot\tCtrl-P",        paste_ds_as_plot,   NULL,   0,  NULL},
    {"Plot handling",           NULL,   op_menu,    0, (char *)  "0"},
    {"\0",           NULL,          NULL,   0, NULL},
    {"Labelling",               NULL,   plot_label_menu,    0, (char *)  "2"},
    {"Error bars",              NULL,   plot_error_menu,   0, NULL},
    {"generate plugin",         generate_plot_plugin_template,  NULL,   0,  NULL},
    {"scan menu",               do_menu_scan,   NULL,   0,  NULL},
    {NULL,                      NULL,       NULL,   0, NULL }
};




int generate_description(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    char *buf = NULL;

    if (updating_menu_state != 0)          return D_O_K;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) return D_O_K;

    buf = create_description_label(op, 8);
    set_op_plot_label(op, op->x_lo + (op->x_hi - op->x_lo) / 4,
                      op->y_hi - (op->y_hi - op->y_lo) / 4, USR_COORD, "%s", buf);

    if (buf) free(buf);

    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


char *set_to_white(char *text)
{
    char *tmp = NULL;

    if (text == NULL) return (text);

    tmp = my_sprintf(tmp, "%s", text);
    free(text);
    text = NULL;
    text = my_sprintf(text, "\\color{white}%s", tmp);
    free(tmp);
    tmp = NULL;

    return (text);
}

int convert_to_printable_colors(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    int i, j;

    if (updating_menu_state != 0)          return D_O_K;

    if (ac_grep(cur_ac_reg, "%pr%op", &pr, &op) != 2) return D_O_K;

    if (op->title != NULL) op->title = set_to_white(op->title);

    if (op->x_title != NULL) op->x_title = set_to_white(op->x_title);

    if (op->y_title != NULL) op->y_title = set_to_white(op->y_title);

    for (i = 0; i < op->n_lab; i++)
    {
        if (op->lab[i]->text != NULL)
            op->lab[i]->text = set_to_white(op->lab[i]->text);
    }

    for (i = 0; i < op->n_dat; i++)
    {
        if (op->dat[i]->color == Lightcyan)  op->dat[i]->color = Cyan;

        if (op->dat[i]->color == Lightgray)  op->dat[i]->color = Darkgray;

        if (op->dat[i]->color == Lightblue)  op->dat[i]->color = Blue;

        if (op->dat[i]->color == Lightgreen) op->dat[i]->color = Green;

        if (op->dat[i]->color == Lightred)    op->dat[i]->color = Red;

        if (op->dat[i]->color == Lightmagenta) op->dat[i]->color = Magenta;

        if (op->dat[i]->color == Yellow)      op->dat[i]->color = White;

        for (j = 0; j < op->dat[i]->n_lab; j++)
        {
            if (op->dat[i]->lab[j]->text != NULL)
                op->dat[i]->lab[j]->text = set_to_white(op->dat[i]->lab[j]->text);
        }

    }

    op->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}


int op_copy1(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;

    if (updating_menu_state != 0)    return D_O_K;

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL)    return win_printf_OK("plot not found");

    op = pr->one_p;

    if (op == NULL)   return D_O_K;

    if (op_copied != NULL)
    {
        free_one_plot(op_copied);
        op_copied = NULL;
    }

    if ((op_copied = duplicate_plot_data(op, NULL)) == NULL)
        return D_O_K;

    refresh_plot(pr, UNCHANGED);
    return 0;
}


int     paste_plot(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    char buf[1024]  = {0};

    if (updating_menu_state != 0)
    {
        if (op_copied == NULL)   active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL || op_copied == NULL)    return win_printf_OK("plot not found");

    op = duplicate_plot_data(op_copied, NULL);

    if (op == NULL)     return D_O_K;

    add_data(pr, IS_ONE_PLOT, (void *)op);
    pr->cur_op = pr->n_op - 1;
    pr->one_p = pr->o_p[pr->cur_op];
    pr->stack = &(pr->one_p->bplt);
    pr->one_p->need_to_refresh = 1;
    sprintf(buf, "%s %s", pr->one_p->filename, pr->one_p->dir);
    //  if (os_type == OSTYPE_WIN2000 || os_type == OSTYPE_WINXP)
    my_set_window_title("%s", buf);
    refresh_plot(pr, UNCHANGED);
    return 0;
}

int     paste_ds_as_plot(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    char buf[1024]  = {0};

    if (updating_menu_state != 0)
    {
        if (opds_copied == NULL)   active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL || opds_copied == NULL)  return win_printf_OK("plot not found");

    op = duplicate_plot_data(opds_copied, NULL);

    if (op == NULL)     return D_O_K;

    add_data(pr, IS_ONE_PLOT, (void *)op);
    pr->cur_op = pr->n_op - 1;
    pr->one_p = pr->o_p[pr->cur_op];
    pr->stack = &(pr->one_p->bplt);
    pr->one_p->need_to_refresh = 1;
    sprintf(buf, "%s %s", pr->one_p->filename, pr->one_p->dir);
    //  if (os_type == OSTYPE_WIN2000 || os_type == OSTYPE_WINXP)
    my_set_window_title("%s", buf);
    refresh_plot(pr, UNCHANGED);
    return 0;
}




int     add_ds_in_copied(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s  *ds = NULL;
    int i, ids, multi = 0, iop = 0;
    static int iops = 0, iope = 1;
    char question[1024] = {0};

    if (updating_menu_state != 0)
    {
        if (op_copied == NULL)   active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL || op_copied == NULL)
        return win_printf_OK("plot not found");

    op = pr->one_p;

    if (op == NULL)   return D_O_K;

    ids = op->cur_dat;
    if (key[KEY_LSHIFT])
    {
      snprintf(question,sizeof(question),"I propose to copy dataset %d of a series of plots\n"
	       "in range [%%4d, %%4d]"
	       "in the last copied plot",ids);
      i = win_scanf(question,&iops, &iope);
      if (i == WIN_CANCEL) return 0;
      multi = 1;
    }
    int k = 0;
    for (iop = iops; iop <= iope; iop++)
      {
	if (multi == 0 && iop != pr->cur_op) continue;
	if (iop < 0 || iop >= pr->n_op) continue;
	ds = pr->o_p[iop]->dat[ids]; //op->dat[op->cur_dat];

	if (ds == NULL)   return D_O_K;

	i = copy_ds_from_one_op_to_anotherone(ds, pr->o_p[iop], op_copied);

	if (i) win_printf("data set was not copied !");
	k++;
      }
    if (multi) win_printf("%d datasets copied !",k);
    return 0;
}



int     add_all_ds_to_copied_op(void)
{
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s  *ds = NULL;
    int i, j, multi = 0, iop = 0;
    static int iops = 0, iope = 1;


    if (updating_menu_state != 0)
    {
        if (op_copied == NULL)   active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL || op_copied == NULL)
        return win_printf_OK("plot not found");


    if (key[KEY_LSHIFT])
    {
      i = win_scanf("I propose to copy all datasets of a series of plots\n"
	       "in the range [%4d, %4d]"
	       "in the last copied plot",&iops, &iope);
      if (i == WIN_CANCEL) return 0;
      multi = 1;
    }
    for (iop = (multi) ? iops : pr->cur_op; iop <= ((multi) ? iope : pr->cur_op+1); iop++)
      {
	if (multi == 0 && iop != pr->cur_op) continue;
	if (iop < 0 || iop >= pr->n_op) continue;

	op = pr->o_p[iop];

	if (op == NULL)   return D_O_K;


	for (j = 0; j < op->n_dat; j++)
	  {
	    ds = op->dat[j];
	    if (ds == NULL)   return D_O_K;

	    i = copy_ds_from_one_op_to_anotherone(ds, op, op_copied);

	    if (i) win_printf("data set was not copied !");
	  }
      }

    return 0;
}

int     paste_ds(void)
{
    pltreg *pr = NULL;
    d_s  *dds = NULL;
    int i;
    un_s *unss, *unsd;
    char *units, *unitd;
    float dxs = 1, axs = 1, dxd = 1, axd = 0, dys = 1, ays = 0, dyd = 1, ayd = 0;
    float d_xs = 1, d_xd = 1, d_ys = 1, d_yd = 1;
    int do_for_next = 0, do_it = 1;
    char message[512] = {0};

    if (updating_menu_state != 0)
    {
        if (opds_copied == NULL && ds_copied == NULL)   active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL || opds_copied == NULL || ds_copied == NULL)
        return win_printf_OK("plot not found");

    //win_printf("debug data ok");

    if (opds_copied == pr->one_p)
    {
        dds = duplicate_data_set(ds_copied, NULL);

        if (dds == NULL)    return win_printf_OK("could not duplicate data set");

        add_data(pr, IS_DATA_SET, (void *)dds);
        pr->one_p->need_to_refresh = 1;
        return D_REDRAWME;
    }
    else
    {
        unss = opds_copied->xu[opds_copied->c_xu];
        unsd = pr->one_p->xu[pr->one_p->c_xu];

        if (unss->type != unsd->type)
        {
            units = generate_units(unss->type, unss->decade);
            unitd = generate_units(unsd->type, unsd->decade);
            snprintf(message, sizeof(message),
                     "You are adding data sets with different x units\n"
                     "souce %s \n destination %s\n"
                     "do you really want to transfer !\n"
                     "If you click OK you will probably get garbage \n"
                     " you are warn ...! To apply the same treatment \n"
                     "for all the other data sets click here %%b"
                     , (units) ? units : "no_name",
                     (unitd) ? unitd : "no_name");
            i = win_scanf(message, &do_for_next);
            do_it = (i == WIN_CANCEL)   ? 0 : 1;

            if (units)
            {
                free(units);
                units = NULL;
            }

            if (unitd)
            {
                free(unitd);
                unitd = NULL;
            }

            if (do_it)
            {
                dxs = dxd = 1;
                axs = axd = 0;
                d_xs = d_xd = 1;
            }
        }
        else
        {
            dxs = unss->dx;
            axs = unss->ax;
            d_xs = 1;

            for (i = 0; i < unss->decade; i++, d_xs *= 10);

            for (i = 0; i > unss->decade; i--, d_xs *= .1);

            dxd = unsd->dx;
            axd = unsd->ax;
            d_xd = 1;

            for (i = 0; i < unsd->decade; i++, d_xd *= 10);

            for (i = 0; i > unsd->decade; i--, d_xd *= .1);
        }

        unss = opds_copied->yu[opds_copied->c_yu];
        unsd = pr->one_p->yu[pr->one_p->c_yu];

        if (unss->type != unsd->type)
        {
            units = generate_units(unss->type, unss->decade);
            unitd = generate_units(unsd->type, unsd->decade);
            snprintf(message, sizeof(message),
                     "You are adding data sets with different x units\n"
                     "souce %s \n destination %s\n"
                     "do you really want to transfer !\n"
                     "If you click OK you will probably get garbage \n"
                     " you are warn ...! To apply the same treatment \n"
                     "for all the other data sets click here %%b"
                     , (units) ? units : "no_name",
                     (unitd) ? unitd : "no_name");
            i = win_scanf(message, &do_for_next);
            do_it = (i == WIN_CANCEL)   ? 0 : 1;

            if (units)
            {
                free(units);
                units = NULL;
            }

            if (unitd)
            {
                free(unitd);
                unitd = NULL;
            }

            if (do_it)
            {
                dys = dyd = 1;
                ays = ayd = 0;
                d_ys = d_yd = 1;
            }
        }
        else
        {
            dys = unss->dx;
            ays = unss->ax;
            d_ys = 1;

            for (i = 0; i < unss->decade; i++, d_ys *= 10);

            for (i = 0; i > unss->decade; i--, d_ys *= .1);

            dyd = unsd->dx;
            ayd = unsd->ax;
            d_yd = 1;

            for (i = 0; i < unsd->decade; i++, d_yd *= 10);

            for (i = 0; i > unsd->decade; i--, d_yd *= .1);
        }

        if (do_it)
        {
            dds = duplicate_data_set(ds_copied, NULL);

            if (dds == NULL)    return D_O_K;

            if (dxs != dxd || axs != axd || d_xs != d_xd)
            {
                for (i = 0; (dxd != 0) && (i < dds->nx) && (i < dds->ny); i++)
                    dds->xd[i] = ((((axs + (dxs * dds->xd[i])) * d_xs) / d_xd) - axd) / dxd;
            }

            if (dys != dyd || ays != ayd || d_ys != d_yd)
            {
                for (i = 0; (dyd != 0) && (i < dds->nx) && (i < dds->ny); i++)
                    dds->yd[i] = ((((ays + (dys * dds->yd[i])) * d_ys) / d_yd) - ayd) / dyd;
            }

            add_data(pr, IS_DATA_SET, (void *)dds);
        }

        pr->one_p->need_to_refresh = 1;
    }

    refresh_plot(pr, UNCHANGED);
    return 0;
}

int     paste_all_ds_from_op_copied(void)
{
    pltreg *pr = NULL;
    d_s  *dds = NULL, *dsi = NULL;
    int i, j;
    int nds, do_for_next = 0, do_it = 1;
    un_s *unss = NULL, *unsd = NULL;
    char *units = NULL, *unitd = NULL;
    float dxs = 1, axs = 0, dxd = 1, axd = 0, dys = 1, ays = 0, dyd = 1, ayd = 0;
    float d_xs = 1, d_xd = 1, d_ys = 1, d_yd = 1;
    char message[512]  = {0};


    if (updating_menu_state != 0)
    {
        if (op_copied == NULL)   active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL || op_copied == NULL)
        return win_printf_OK("plot not found");

    nds = op_copied->n_dat;

    for (j = 0, do_it = 1; j < nds; j++)
    {
        dsi = op_copied->dat[j];

        if (op_copied == pr->one_p)
        {
            dds = duplicate_data_set(dsi, NULL);

            if (dds == NULL)    return D_O_K;

            add_data(pr, IS_DATA_SET, (void *)dds);
            pr->one_p->need_to_refresh = 1;
        }
        else  // on different plot
        {
            unss = op_copied->xu[op_copied->c_xu];
            unsd = pr->one_p->xu[pr->one_p->c_xu];

            if (unss->type != unsd->type)
            {
                if (do_for_next == 0)
                {
                    units = generate_units(unss->type, unss->decade);
                    unitd = generate_units(unsd->type, unsd->decade);
                    snprintf(message, sizeof(message),
                             "You are adding data sets with different x units\n"
                             "souce %s \n destination %s\n"
                             "do you really want to transfer !\n"
                             "If you click OK you will probably get garbage \n"
                             " you are warn ...! To apply the same treatment \n"
                             "for all the other data sets click here %%b"
                             , (units) ? units : "no_name",
                             (unitd) ? unitd : "no_name");
                    i = win_scanf(message, &do_for_next);
                    do_it = (i == WIN_CANCEL)   ? 0 : 1;

                    if (units)
                    {
                        free(units);
                        units = NULL;
                    }

                    if (unitd)
                    {
                        free(unitd);
                        unitd = NULL;
                    }
                }

                if (do_it)
                {
                    dxs = dxd = 1;
                    axs = axd = 0;
                    d_xs = d_xd = 1;
                }
            }
            else  // same unit type
            {
                dxs = unss->dx;
                axs = unss->ax;
                d_xs = 1;

                for (i = 0; i < unss->decade; i++, d_xs *= 10);

                for (i = 0; i > unss->decade; i--, d_xs *= .1);

                dxd = unsd->dx;
                axd = unsd->ax;
                d_xd = 1;

                for (i = 0; i < unsd->decade; i++, d_xd *= 10);

                for (i = 0; i > unsd->decade; i--, d_xd *= .1);
            }

            unss = op_copied->yu[op_copied->c_yu];
            unsd = pr->one_p->yu[pr->one_p->c_yu];

            if (do_it)
            {
                dds = duplicate_data_set(dsi, NULL);

                if (dds == NULL)    return D_O_K;

                if (dxs != dxd || axs != axd || d_xs != d_xd)
                {
                    for (i = 0; (dxd != 0) && (i < dds->nx) && (i < dds->ny); i++)
                        dds->xd[i] = ((((axs + (dxs * dds->xd[i])) * d_xs) / d_xd) - axd) / dxd;
                }

                if (dys != dyd || ays != ayd || d_ys != d_yd)
                {
                    for (i = 0; (dyd != 0) && (i < dds->nx) && (i < dds->ny); i++)
                        dds->yd[i] = ((((ays + (dys * dds->yd[i])) * d_ys) / d_yd) - ayd) / dyd;
                }

                add_data(pr, IS_DATA_SET, (void *)dds);
            } // end on different plot


            /*
               if (unss->type != unsd->type)
               {
               if (do_for_next == 0)
               {
               units = generate_units(unss->type, unss->decade);
               unitd = generate_units(unsd->type, unsd->decade);
               snprintf(message,sizeof(message),
               "You are adding data sets with different x units\n"
               "souce %s \n destination %s\n"
               "do you really want to transfer !\n"
               "If you click OK you will probably get garbage \n"
               " you are warn ...! To apply the same treatment \n"
               "for all the other data sets click here %%b"
               ,(units)?units:"no_name",
               (unitd)?unitd:"no_name");
               i = win_scanf(message,&do_for_next);
               do_it = (i == WIN_CANCEL)    ? 0 : 1;
               if (units)  {free(units); units = NULL;}
               if (unitd)  {free(unitd); unitd = NULL;}
               if (do_it)
               {
               dys = dyd = 1;
               ays = ayd = 0;
               d_ys = d_yd = 1;
               }
               }
               else
               {
               dys = unss->dx;  ays = unss->ax; d_ys = 1;
               for (i = 0; i < unss->decade; i++, d_ys *= 10);
               for (i = 0; i > unss->decade; i--, d_ys *= .1);
               dyd = unsd->dx;  ayd = unsd->ax; d_yd = 1;
               for (i = 0; i < unsd->decade; i++, d_yd *= 10);
               for (i = 0; i > unsd->decade; i--, d_yd *= .1);
               }
               if (do_it)
               {
               dds = duplicate_data_set(dsi, NULL);
               if (dds == NULL)     return D_O_K;
               if (dxs != dxd || axs != axd || d_xs != d_xd)
               {
               for (i = 0; (dxd != 0) && (i < dds->nx) && (i < dds->ny); i++)
               dds->xd[i] = ((((axs + (dxs * dds->xd[i]))*d_xs)/d_xd)-axd)/dxd;
               }
               if (dys != dyd || ays != ayd || d_ys != d_yd)
               {
               for (i = 0; (dyd != 0) && (i < dds->nx) && (i < dds->ny); i++)
               dds->yd[i] = ((((ays + (dys * dds->yd[i]))*d_ys)/d_yd)-ayd)/dyd;
               }
               add_data(pr, IS_DATA_SET, (void*)dds);
               }
               }
               */
            pr->one_p->need_to_refresh = 1;

        }
    }

    refresh_plot(pr, UNCHANGED);
    return 0;
}


XV_FUNC(int, do_save_bin, (void));
XV_FUNC(int, do_load, (void));
int do_menu_scan(void)
{

    if (updating_menu_state != 0)    return D_O_K;

    /*  i = win_scanf("Do you want to update menu");
        if (i == WIN_CANCEL)    return D_O_K;   */

    /*  win_printf("number of dialog %d",number_of_item_from_dialog());
        i = retrieve_item_from_dialog(d_keyboard_proc, do_load);
        allegro_message("do load in %d",i);     */
    remove_all_keyboard_proc();
    /*  win_printf("number of dialog %d",number_of_item_from_dialog());*/
    scan_and_update(plot_menu);
    /*  win_printf("number of dialog %d",number_of_item_from_dialog());
        i = retrieve_item_from_dialog(d_keyboard_proc, do_save_bin);
        allegro_message("do save bin in %d",i);
        i = retrieve_item_from_dialog(d_keyboard_proc, do_load);
        allegro_message("do load in %d",i);     */



    return D_O_K;
}
int add_plot_edit_menu_item(char *text, int (*proc)(void), struct MENU *child, int flags, void *dp)
{
    int i;

    for (i = 0; i < 32 && plot_edit_menu[i].text != NULL; i++);

    if (i >= 30)    return -1;

    plot_edit_menu[i].text = text;
    plot_edit_menu[i].proc = proc;
    plot_edit_menu[i].child = child;
    plot_edit_menu[i].flags = flags;
    plot_edit_menu[i].dp = dp;
    plot_edit_menu[i + 1].text = NULL;
    return 0;
}
int add_error_bars_in_y(void)
{
    int i;
    pltreg *pr = NULL;
    static float a = 0, b = 0.05;
    static int n_dser = -1, sqr = 0, ertype = 0;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsr = NULL;
    int  nx;
    float tmp;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT]) return win_printf_OK(
                                        "This routine add error bars in Y to a data set, these errors either\n"
                                        "come from:\n - another data set having the same number of points and "
                                        "the same X axis\n - the error may be set to a fix value for each points\n"
                                        "- or the error may be set to a ratio of the Y component\n"
                                        "- or to a combination of both");

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)     return win_printf_OK("cannot find data");

    if (dsi->ye != NULL)
    {
        i = win_printf("This data set already has error bars, persuing will\n"
                       "replace the present values by new one.\n Do you want to go on ?");

        if (i == WIN_CANCEL)    return D_O_K;
    }

    nx = dsi->nx;

    if (n_dser == op->cur_dat) n_dser++;

    i = win_scanf(
            "This routine add error bars in Y to a data set, these errors either\n"
            "come from:\n - another data set having the same number of points and "
            "the same X axis\n - the error may be set to a fix value for each points\n"
            "- or the error may be set to a ratio of the Y component\n"
            "- or to a combination of both\n"
            "if errors are contained in a extra data set enter its number\n"
            "Or -1 if you want to define this error otherwise %4d\n"
            "If you have enter -1 for the error data set, you can define errors as:\n"
            "%R->\\delta y = a + b.abs(y) or %r->\\delta y = a + b.\\sqrt(|y|)\n"
            "a = %6f b = %6f\n"
	    "Add error as %R->Std. Err. %r->Down Std. Err. %r->Up Box Err. %r->Down Box Err.\n"
	    , &n_dser, &sqr, &a, &b, &ertype);

    if (i == WIN_CANCEL)    return D_O_K;

    a = fabs(a);

    if (ertype == 0 && dsi->ye == NULL)
    {
        if ((alloc_data_set_y_error(dsi) == NULL) || (dsi->ye == NULL))
            return win_printf_OK("I can't create errors !");
    }
    else if (ertype == 1 && dsi->yed == NULL)
    {
        if ((alloc_data_set_y_down_error(dsi) == NULL) || (dsi->yed == NULL))
            return win_printf_OK("I can't create errors !");
    }
    else if (ertype == 2 && dsi->ybu == NULL)
    {
        if ((alloc_data_set_y_box_up(dsi) == NULL) || (dsi->ybu == NULL))
            return win_printf_OK("I can't create errors !");
    }
    else if (ertype == 3 && dsi->ybd == NULL)
    {
        if ((alloc_data_set_y_box_dwn(dsi) == NULL) || (dsi->ybd == NULL))
            return win_printf_OK("I can't create errors !");
    }

    if (n_dser >= 0)
    {
        dsr = op->dat[n_dser % op->n_dat];

        if (dsi->nx != dsr->nx)
            return win_printf_OK("data set have different size !");

        for (i = 0; i < nx; i++)
        {
            if (dsi->xd[i] != dsr->xd[i])
                return win_printf_OK("pts at index %d differ in X\n"
                                     "Data X = %g error X %g!", i, dsi->xd[i], dsr->xd[i]);
        }

        for (i = 0; i < nx; i++)
	  {
	    if (ertype == 0) dsi->ye[i] = dsr->yd[i];
	    else if (ertype == 1) dsi->yed[i] = dsr->yd[i];
	    else if (ertype == 2) dsi->ybu[i] = dsr->yd[i];
	    else if (ertype == 3) dsi->ybd[i] = dsr->yd[i];
	  }
    }
    else
    {
        for (i = 0; i < nx; i++)
	  {
	    tmp = (sqr) ? a + b * sqrt(fabs(dsi->yd[i])) : a + b * fabs(dsi->yd[i]);
	    if (ertype == 0) dsi->ye[i] = tmp;
	    else if (ertype == 1) dsi->yed[i] = tmp;
	    else if (ertype == 2) dsi->ybu[i] = tmp;
	    else if (ertype == 3) dsi->ybd[i] = tmp;
	  }
    }

    op->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    return 0;
}

int convert_error_bars_in_y_in_ds(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL, *opn = NULL;
    d_s *dsi = NULL, *dsr = NULL;
    int  nx;
    static int all = 0, ndss = 0, ndse = 0, newplot = 0, ertype = 0;

    if (updating_menu_state != 0)
    {
        i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

        if (i != 3) active_menu->flags |=  D_DISABLED;
        else if (dsi->ye == NULL) active_menu->flags |=  D_DISABLED;
        else if (dsi->ye != NULL) active_menu->flags &=  ~D_DISABLED;

        return D_O_K;
    }


    if (key[KEY_LSHIFT]) return win_printf_OK("This routine convert error bars in Y to a new data set");

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)     return win_printf_OK("cannot find data");
    ndse = (ndse > op->n_dat) ? op->n_dat : ndse;
    ndss = (ndss > op->n_dat) ? 0 : ndss;

    if (dsi->ye == NULL)
        return win_printf_OK("This data does not contains error bars. Sorry");
    i = win_scanf("Concerting error bars in datasets\nDo you want to proceed:\n"
		  "%R->just the selected dataset\n"
		  "%r->all datasets of the plot\n"
		  "%r->A set of datasets in the range[%4d,%4d[\n"
		  "Place result in a new plot%b\n"
		  "%R->std. error %r->down error %r->box up error %r->box down error\n"
		  ,&all,&ndss,&ndse,&newplot,&ertype);
    if (i == WIN_CANCEL) return 0;

    if (ertype == 1 && dsi->yed == NULL)
      return win_printf_OK("This error type is not present !");
    if (ertype == 2 && dsi->ybu == NULL)
      return win_printf_OK("This error type is not present !");
    if (ertype == 3 && dsi->ybd == NULL)
      return win_printf_OK("This error type is not present !");

    if (all <= 1)
      {
	ndss = 0;
	ndse = op->n_dat;
      }
    int ids;
    for (ids = ndss; ids < ndse; ids++)
      {
	if (all == 0 && ids != op->cur_dat) continue;
	dsi = op->dat[ids];
	nx = dsi->nx;
	if (newplot && opn == NULL)
	  {
	    if ((opn = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	      return win_printf_OK("I can't create plot !");
	    dsr = opn->dat[0];
	    opn->need_to_refresh = 1;
	  }
	else if ((dsr = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	  return win_printf_OK("I can't create plot !");
	inherit_from_ds_to_ds(dsr, dsi);
	if (ertype == 0)      set_ds_treatement(dsr, "error bar values in Y");
	else if (ertype == 1) set_ds_treatement(dsr, "error down bar values in Y");
	else if (ertype == 2) set_ds_treatement(dsr, "error box up bar values in Y");
	else if (ertype == 3) set_ds_treatement(dsr, "error box down bar values in Y");

	for (i = 0; i < nx; i++)
	  {
	    dsr->xd[i] = dsi->xd[i];
	    if (ertype == 0) dsr->yd[i] = dsi->ye[i];
	    else if (ertype == 1) dsr->yd[i] = dsi->yed[i];
	    else if (ertype == 2) dsr->yd[i] = dsi->ybu[i];
	    else if (ertype == 3) dsr->yd[i] = dsi->ybd[i];
	  }
      }

    op->need_to_refresh = 1;
    if (newplot) refresh_plot(pr, pr->n_op-1);
    else refresh_plot(pr, UNCHANGED);
    return 0;
}

int convert_error_bars_in_x_in_ds(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL, *opn = NULL;
    d_s *dsi = NULL, *dsr = NULL;
    int  nx;
    static int all = 0, ndss = 0, ndse = 0, newplot = 0, ertype = 0;

    if (updating_menu_state != 0)
    {
        i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

        if (i != 3) active_menu->flags |=  D_DISABLED;
        else if (dsi->xe == NULL) active_menu->flags |=  D_DISABLED;
        else if (dsi->xe != NULL) active_menu->flags &=  ~D_DISABLED;

        return D_O_K;
    }


    if (key[KEY_LSHIFT]) return win_printf_OK("This routine convert error bars in X to a new data set");

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)     return win_printf_OK("cannot find data");
    ndse = (ndse > op->n_dat) ? op->n_dat : ndse;
    ndss = (ndss > op->n_dat) ? 0 : ndss;

    if (dsi->xe == NULL)
        return win_printf_OK("This data does not contains error bars. Sorry");
    i = win_scanf("Concerting error bars in datasets\nDo you want to proceed:\n"
		  "%R->just the selected dataset\n"
		  "%r->all datasets of the plot\n"
		  "%r->A set of datasets in the range[%4d,%4d[\n"
		  "Place result in a new plot%b\n"
		  "%R->std. error %r->down error %r->box up error %r->box down error\n"
		  ,&all,&ndss,&ndse,&newplot,&ertype);
    if (i == WIN_CANCEL) return 0;

    if (ertype == 1 && dsi->xed == NULL)
      return win_printf_OK("This error type is not present !");
    if (ertype == 2 && dsi->xbu == NULL)
      return win_printf_OK("This error type is not present !");
    if (ertype == 3 && dsi->xbd == NULL)
      return win_printf_OK("This error type is not present !");

    if (all <= 1)
      {
	ndss = 0;
	ndse = op->n_dat;
      }
    int ids;
    for (ids = ndss; ids < ndse; ids++)
      {
	if (all == 0 && ids != op->cur_dat) continue;
	dsi = op->dat[ids];
	nx = dsi->nx;
	if (newplot && opn == NULL)
	  {
	    if ((opn = create_and_attach_one_plot(pr, nx, nx, 0)) == NULL)
	      return win_printf_OK("I can't create plot !");
	    dsr = opn->dat[0];
	    opn->need_to_refresh = 1;
	  }
	else if ((dsr = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
	  return win_printf_OK("I can't create plot !");
	inherit_from_ds_to_ds(dsr, dsi);
	if (ertype == 0)      set_ds_treatement(dsr, "error bar values in X");
	else if (ertype == 1) set_ds_treatement(dsr, "error down bar values in X");
	else if (ertype == 2) set_ds_treatement(dsr, "error box up bar values in X");
	else if (ertype == 3) set_ds_treatement(dsr, "error box down bar values in X");

	for (i = 0; i < nx; i++)
	  {
	    dsr->xd[i] = dsi->xd[i];
	    if (ertype == 0) dsr->yd[i] = dsi->xe[i];
	    else if (ertype == 1) dsr->yd[i] = dsi->xed[i];
	    else if (ertype == 2) dsr->yd[i] = dsi->xbu[i];
	    else if (ertype == 3) dsr->yd[i] = dsi->xbd[i];
	  }
      }

    op->need_to_refresh = 1;
    if (newplot) refresh_plot(pr, pr->n_op-1);
    else refresh_plot(pr, UNCHANGED);
    return 0;
}

# ifdef KEEP

int convert_error_bars_in_x_in_ds(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsr = NULL;
    int  nx;

    if (updating_menu_state != 0)
    {
        i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

        if (i != 3) active_menu->flags |=  D_DISABLED;
        else if (dsi->xe == NULL) active_menu->flags |=  D_DISABLED;
        else if (dsi->xe != NULL) active_menu->flags &=  ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT]) return win_printf_OK(
                                        "This routine convert error bars in X to a new data set");

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)     return win_printf_OK("cannot find data");

    if (dsi->xe == NULL)
        return win_printf_OK("This data does not contains error bars. Sorry");

    nx = dsi->nx;

    if ((dsr = create_and_attach_one_ds(op, nx, nx, 0)) == NULL)
        return win_printf_OK("I can't create plot !");

    for (i = 0; i < nx; i++)
    {
        dsr->yd[i] = dsi->xe[i];
        dsr->xd[i] = dsi->xd[i];
    }

    inherit_from_ds_to_ds(dsr, dsi);
    set_ds_treatement(dsr, "error bar values in X");

    op->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    return 0;
}
# endif


int add_error_bars_in_x(void)
{
    int i;
    pltreg *pr;
    static float a = 0, b = 0.05;
    static int n_dser = -1, sqr = 0, ertype = 0;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsr = NULL;
    int  nx;
    float tmp;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (key[KEY_LSHIFT]) return win_printf_OK(
                                        "This routine add error bars in X to a data set, these errors either\n"
                                        "come from:\n - another data set having the same number of points and "
                                        "the same X axis\n - the error may be set to a fix value for each points\n"
                                        "- or the error may be set to a ratio of the Y component\n"
                                        "- or to a combination of both");

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)     return win_printf_OK("cannot find data");

    if (dsi->xe != NULL)
    {
        i = win_printf("This data set already has error bars, persuing will\n"
                       "replace the present values by new one.\n Do you want to go on ?");

        if (i == WIN_CANCEL)    return D_O_K;
    }

    nx = dsi->nx;

    if (n_dser == op->cur_dat) n_dser++;

    i = win_scanf(
            "This routine add error bars in X to a data set, these errors either\n"
            "come from:\n - another data set having the same number of points and "
            "the same X axis\n - the error may be set to a fix value for each points\n"
            "- or the error may be set to a ratio of the Y component\n"
            "- or to a combination of both\n"
            "if errors are contained in a extra data set enter its number\n"
            "Or -1 if you want to define this error otherwise %4d\n"
            "If you have enter -1 for the error data set, you can define errors as:\n"
            "%R->\\delta y = a + b.abs(y) or %r->\\delta y = a + b.\\sqrt(|y|)\n"
            "a = %6f b = %6f\n"
	    "Add error as %R->Std. Err. %r->Down Std. Err. %r->Up Box Err. %r->Down Box Err.\n"
	    , &n_dser, &sqr, &a, &b, &ertype);

    if (i == WIN_CANCEL)    return D_O_K;

    a = fabs(a);

    if (ertype == 0 && dsi->xe == NULL)
    {
        if ((alloc_data_set_x_error(dsi) == NULL) || (dsi->xe == NULL))
            return win_printf_OK("I can't create errors !");
    }
    else if (ertype == 1 && dsi->xed == NULL)
    {
        if ((alloc_data_set_x_down_error(dsi) == NULL) || (dsi->xed == NULL))
            return win_printf_OK("I can't create errors !");
    }
    else if (ertype == 2 && dsi->xbu == NULL)
    {
        if ((alloc_data_set_x_box_up(dsi) == NULL) || (dsi->xbu == NULL))
            return win_printf_OK("I can't create errors !");
    }
    else if (ertype == 3 && dsi->xbd == NULL)
    {
        if ((alloc_data_set_x_box_dwn(dsi) == NULL) || (dsi->xbd == NULL))
            return win_printf_OK("I can't create errors !");
    }

    if (n_dser >= 0)
    {
        dsr = op->dat[n_dser % op->n_dat];

        if (dsi->nx != dsr->nx)
            return win_printf_OK("data set have different size !");

        for (i = 0; i < nx; i++)
        {
            if (dsi->xd[i] != dsr->xd[i])
                return win_printf_OK("pts at index %d differ in X\n"
                                     "Data X = %g error X %g!", i, dsi->xd[i], dsr->xd[i]);
        }

        for (i = 0; i < nx; i++)
	  {
	    if (ertype == 0) dsi->xe[i] = dsr->yd[i];
	    else if (ertype == 1) dsi->xed[i] = dsr->yd[i];
	    else if (ertype == 2) dsi->xbu[i] = dsr->yd[i];
	    else if (ertype == 3) dsi->xbd[i] = dsr->yd[i];
	  }
    }
    else
    {
        for (i = 0; i < nx; i++)
	  {
	    tmp = (sqr) ? a + b * sqrt(fabs(dsi->yd[i])) : a + b * fabs(dsi->yd[i]);
	    if (ertype == 0) dsi->xe[i] = tmp;
	    else if (ertype == 1) dsi->xed[i] = tmp;
	    else if (ertype == 2) dsi->xbu[i] = tmp;
	    else if (ertype == 3) dsi->xbd[i] = tmp;
	  }
    }

    op->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    return 0;
}

# ifdef KEEP

int add_error_bars_in_x(void)
{
    int i;
    pltreg *pr;
    static float a = 0, b = 0.05;
    static int n_dser = -1;
    O_p *op = NULL;
    d_s *dsi = NULL, *dsr = NULL;
    int  nx, test = 1, test2 = 1, test3 = 0;

    if (updating_menu_state != 0)    return D_O_K;

    if (key[KEY_LSHIFT]) return win_printf_OK(
                                        "This routine add error bars in X to a data set, these errors either\n"
                                        "come from:\n - another data set having the same number of points and "
                                        "the same Y axis\n - the error may be set to a fix value for each points\n"
                                        "- or the error may be set to a ratio of the X component\n"
                                        "- or to a combination of both");

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)     return win_printf_OK("cannot find data");

    if (dsi->xe != NULL)
    {
        i = win_printf("This data set already has error bars, persuing will\n"
                       "replace the present values by new one.\n Do you want to go on ?");

        if (i == WIN_CANCEL)    return D_O_K;
    }

    nx = dsi->nx;

    if (n_dser == op->cur_dat) n_dser++;

    i = win_scanf(
            "This routine add error bars in X to a data set, these errors either\n"
            "come from:\n - another data set having the same number of points and "
            "the same Y axis\n - the error may be set to a fix value for each points\n"
            "- or the error may be set to a ratio of the Y component\n"
            "- or to a combination of both\n"
            "if errors are contained in a extra data set enter its number\n"
            "Or -1 if you want to define this error otherwise %3d"
            "If you \nhave enter -1 for the error data set, you can define errors as:\n"
            "\\delta x = a + b x abs(x)\n"
            "a = %f b = %fclik me %b\n toto\n oui %R non %r bof %r\na %R b %r", &n_dser, &a, &b, &test, &test2, &test3);

    if (i == WIN_CANCEL)    return D_O_K;

    win_printf("test %d %d", test, test2);
    a = fabs(a);

    if (dsi->xe == NULL)
    {
        if ((alloc_data_set_x_error(dsi) == NULL) || (dsi->xe == NULL))
            return win_printf_OK("I can't create errors !");
    }

    if (n_dser >= 0)
    {
        dsr = op->dat[n_dser % op->n_dat];

        if (dsi->nx != dsr->nx)
            return win_printf_OK("data set have different size !");

        for (i = 0; i < nx; i++)
        {
            if (dsi->yd[i] != dsr->yd[i])
                return win_printf_OK("pts at index %d differ in X\n"
                                     "Data X = %g error X %g!", i, dsi->yd[i], dsr->yd[i]);
        }

        for (i = 0; i < nx; i++) dsi->xe[i] = dsr->xd[i];
    }
    else
    {
        for (i = 0; i < nx; i++) dsi->xe[i] = a + b * fabs(dsi->xd[i]);
    }

    op->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    return 0;
}

# endif

int remove_error_bars_in_x(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL;
    static int rxe = 1, rxed = 0, rxbd = 0, rxbu = 0;
    char question[2048] = {0};

    if (updating_menu_state != 0)
    {
        i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

        if (i != 3) active_menu->flags |=  D_DISABLED;
        else if (dsi->xe == NULL) active_menu->flags |=  D_DISABLED;
        else if (dsi->xe != NULL) active_menu->flags &=  ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
      return win_printf_OK("This routine suppressd error bars in X to a data set");

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)     return win_printf_OK("cannot find data");

    if (dsi->xe == NULL && dsi->xed == NULL && dsi->xbd == NULL && dsi->xbu == NULL)
    {
        i = win_printf("This data set has no error bars, cannot remove them");

        if (i == WIN_CANCEL)    return D_O_K;
    }
    snprintf(question,sizeof(question),
	     "Specify the error bars thay you want to remove in X among:\n"
	     "%s%s%s%s\n%%b->Std.Er.\n%%b->Bottom.Std.Er.\n%%b->Box.Er.\n"
	     "%%b->Box.Bottom.Er.\n"
	     ,(dsi->xe == NULL)?"":"-Std.Er. "
	     ,(dsi->xed == NULL)?"":"-Bottom.Std.Er. "
	     ,(dsi->xbu == NULL)?"":"-Box.Er. "
	     ,(dsi->xbd == NULL)?"":"-Box.Bottom.Er. ");

    i = win_scanf(question,&rxe, &rxed, &rxbd, &rxbu);
    if (i == WIN_CANCEL)    return D_O_K;
    if (dsi->xe != NULL && rxe > 0)
      free_data_set_x_error(dsi);
    if (dsi->xed != NULL && rxed > 0)
      free_data_set_x_down_error(dsi);
    if (dsi->xbu != NULL && rxbu > 0)
      free_data_set_x_box_up(dsi);
    if (dsi->xbd != NULL && rxbd > 0)
      free_data_set_x_box(dsi);

    op->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    return 0;
}
int remove_error_bars_in_y(void)
{
    int i;
    pltreg *pr = NULL;
    O_p *op = NULL;
    d_s *dsi = NULL;
    static int rye = 1, ryed = 0, rybd = 0, rybu = 0;
    char question[2048] = {0};

    if (updating_menu_state != 0)
    {
        i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

        if (i != 3) active_menu->flags |=  D_DISABLED;
        else if (dsi->ye == NULL) active_menu->flags |=  D_DISABLED;
        else if (dsi->ye != NULL) active_menu->flags &=  ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT]) return win_printf_OK(
                                        "This routine suppressd error bars in Y to a data set");

    i = ac_grep(cur_ac_reg, "%pr%op%ds", &pr, &op, &dsi);

    if (i != 3)     return win_printf_OK("cannot find data");


    if (dsi->ye == NULL && dsi->yed == NULL && dsi->ybd == NULL && dsi->ybu == NULL)
    {
        i = win_printf("This data set has no error bars, cannot remove them");

        if (i == WIN_CANCEL)    return D_O_K;
    }
    snprintf(question,sizeof(question),
	     "Specify the error bars thay you want to remove in Y among:\n"
	     "%s%s%s%s\n%%b->Std.Er.\n%%b->Bottom.Std.Er.\n%%b->Box.Er.\n"
	     "%%b->Box.Bottom.Er.\n"
	     ,(dsi->ye == NULL)?"":"-Std.Er. "
	     ,(dsi->yed == NULL)?"":"-Bottom.Std.Er. "
	     ,(dsi->ybu == NULL)?"":"-Box.Er. "
	     ,(dsi->ybd == NULL)?"":"-Box.Bottom.Er. ");

    i = win_scanf(question,&rye, &ryed, &rybd, &rybu);
    if (i == WIN_CANCEL)    return D_O_K;
    if (dsi->ye != NULL && rye > 0)
      free_data_set_y_error(dsi);
    if (dsi->yed != NULL && ryed > 0)
      free_data_set_y_down_error(dsi);
    if (dsi->ybu != NULL && rybu > 0)
      free_data_set_y_box_up(dsi);
    if (dsi->ybd != NULL && rybd > 0)
      free_data_set_y_box(dsi);

    op->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    return 0;
}




int set_label(void)
{
    int i;
    char *s = NULL, **lbl = NULL;
    int index;
    p_l *dp = NULL;
    pltreg *pr = NULL;

    if (updating_menu_state != 0)    return D_O_K;

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL || pr->one_p == NULL) return D_O_K;

    if (active_menu->dp == NULL)    return D_O_K;

    i = sscanf((char *)active_menu->dp, "%d", &index);

    if (i != 1) return D_O_K;

    if (index == 0)     lbl = &pr->one_p->title;
    else if (index == 1)    lbl = &pr->one_p->x_title;
    else if (index == 2)    lbl = &pr->one_p->y_title;
    else if (index == 6)    lbl = &pr->one_p->x_prime_title;
    else if (index == 7)    lbl = &pr->one_p->y_prime_title;

    if (lbl != NULL && *lbl != NULL)
    {
        TeX_modify(*lbl);

        if (*lbl != NULL) free(*lbl);

        *lbl = NULL;
    }
    else        TeX_modify(NULL);

    if (last_answer == NULL || strlen(last_answer) == 0)    s = NULL;
    else                s = strdup(last_answer);


    if (index == 0) pr->one_p->title = s;

    if (index == 1) pr->one_p->x_title = s;

    if (index == 2) pr->one_p->y_title = s;

    if (index == 6) pr->one_p->x_prime_title = s;

    if (index == 7) pr->one_p->y_prime_title = s;

    if ((index == 3 || index == 4 || index == 5) && (s != NULL))
    {
        dp = (p_l *)calloc(1, sizeof(p_l));

        if (dp == NULL)        return D_O_K;

        dp->text = s;
        dp->xla = (pr->one_p->iopt & XLOG) ? pr->one_p->x_lo :
                  (pr->one_p->x_hi + pr->one_p->x_lo) / 2;
        dp->yla = (pr->one_p->iopt & YLOG) ? pr->one_p->y_lo :
                  (pr->one_p->y_hi + pr->one_p->y_lo) / 2;

        if (index == 3)     dp->type = ABS_COORD;
        else if (index == 4)    dp->type = USR_COORD;
        else if (index == 5)    dp->type = VERT_LABEL_USR;

        add_data_to_one_plot(pr->one_p, IS_PLOT_LABEL, (void *)dp);
    }

    pr->one_p->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}




int set_source(void)
{
    char *s = NULL, **lbl = NULL;
    pltreg *pr = NULL;

    if (updating_menu_state != 0)    return D_O_K;

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL || pr->one_p == NULL) return D_O_K;

    lbl = &(pr->one_p->dat[pr->one_p->cur_dat]->source);

    if (lbl != NULL && *lbl != NULL)
    {
        TeX_modify(*lbl);

        if (*lbl != NULL) free(*lbl);

        *lbl = NULL;
    }
    else        TeX_modify(NULL);

    if (last_answer == NULL || strlen(last_answer) == 0)    s = NULL;
    else                s = strdup(last_answer);

    pr->one_p->dat[pr->one_p->cur_dat]->source = s;
    pr->one_p->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}

int set_treatement(void)
{
    char **lbl = NULL;
    pltreg *pr = NULL;
    O_p *op;
    d_s *ds;

    if (updating_menu_state != 0)    return D_O_K;

    pr = find_pr_in_current_dialog(NULL);

    if (pr == NULL || pr->one_p == NULL) return D_O_K;
    op = pr->one_p;
    ds = op->dat[op->cur_dat];
    lbl = &(ds->treatement);

    if (lbl != NULL && *lbl != NULL)
    {
        TeX_modify(*lbl);

        if (*lbl != NULL) free(*lbl);

        *lbl = NULL;
    }
    else        TeX_modify(NULL);

    if (last_answer != NULL && strlen(last_answer) > 0)
      update_ds_treatement(ds,"%s",last_answer);
    op->need_to_refresh = 1;
    refresh_plot(pr, UNCHANGED);
    return D_O_K;
}



int generate_plot_plugin_template(void)
{
    int i;
    char app_name[256] = {0}, path[256] = {0}, filename[512] = {0}, *s = NULL, APP_NAME[256] = {0};
    FILE *fp = NULL;
    int aret; //, hello = 0, create = 0, rescale = 1, anim = 0;

    if (updating_menu_state != 0)    return D_O_K;

    app_name[0] = 0;
    i = win_scanf("I am going to prepare the set of files necessary\n"
                  "to write a basic starting template providing a minimum\n"
                  "add on program attaching a new treatement button to the\n"
                  "plot menu and giving the canva for a plot treatement\n"
                  "function. Give the name of the plugin\n"
                  "(avoid space, punctuation minus sign ...  characters) %s",
                  app_name);

    if (i == WIN_CANCEL)    return D_O_K;

    for (i = 0; app_name[i] != 0; i++)
    {
        if (app_name[i] == ' ' || app_name[i] == '\t' || app_name[i] == '-'
                || app_name[i] == '\\' || app_name[i] == '/' || app_name[i] == '|'
                || app_name[i] == '<' || app_name[i] == '*' || app_name[i] == '?'
                || app_name[i] == ';' || app_name[i] == ':' || app_name[i] == '>')
            return win_printf_OK("You have incorporated a space a tab or a minus sign\n"
                                 "a / a | a ; ... which is forbidden in an application name");
    }

    strcpy(APP_NAME, app_name);
    strupr(APP_NAME);
    strcpy(path, prog_dir);
    backslash_to_slash(path);
    s = strstr(path, "/bin");

    if (s == NULL)  s = strstr(path, "/BIN");

    if (s == NULL)  s = strstr(path, "/build");

    if (s == NULL)  s = strstr(path, "/BUILD");

    if (s == NULL)
    {
        win_printf("I cannot retrieve the path of Xvin in %s\n", path);
        return D_O_K;
    }

    s[0] = 0;
    get_os_specific_path(path);

    if (file_exists(path, FA_DIREC, &aret) == 0)
    {
        win_printf("I cannot find Xvin path!\n%s", backslash_to_slash(path));
        return D_O_K;
    }

    put_path_separator(path, sizeof(path));
    strcat(path, "plug-ins-src");

    if (file_exists(path, FA_DIREC, &aret) == 0)
    {
#ifdef XV_WIN32

        if (mkdir(path) != 0)
#else
        if (mkdir(path, 0777) != 0)
#endif
        {
            win_printf("I cannot create path!\n%s", backslash_to_slash(path));
            return D_O_K;
        }
    }

    if (file_exists(path, FA_DIREC, &aret) == 0)
    {
        win_printf("I cannot find plug-in path!\n%s", backslash_to_slash(path));
        return D_O_K;
    }

    put_path_separator(path, sizeof(path));
    strcat(path, app_name);
    get_os_specific_path(path);

    if (file_exists(path, FA_DIREC, &aret) == 0)
    {
#ifdef XV_WIN32

        if (mkdir(path) != 0)
#else
        if (mkdir(path, 0777) != 0)
#endif
        {
            win_printf("I cannot create path!\n%s", backslash_to_slash(path));
            return D_O_K;
        }
    }
    else
    {
        win_printf("path %s already exists! presuably %s plug-ins already exist\n"
                   , backslash_to_slash(path), app_name);
        return D_O_K;

    }

    put_path_separator(path, sizeof(path));
    strcpy(filename, path);
    strcat(filename, "Makefile");

    fp = fopen(filename, "w");

    if (fp == NULL)
    {
        win_printf("I cannot create file!\n%s", backslash_to_slash(filename));
        return D_O_K;
    }

    //fprintf(fp,"CFLAGS = -c  -Wall -O2 -march=pentium -mdll -DXV_WIN32  -I../../include \\\n"
    //  " -I../../include/xvin  -I../../include/plug-ins \\\n"
    //  "-I\"c:\\Program Files\\GnuWin32\\include\"\n\n");




    fprintf(fp, "# name of the plug'in\n");
    fprintf(fp, "NAME	    		= %s\n", app_name);
    fprintf(fp, "# specific plugin flags\n");
    fprintf(fp, "LCFLAGS 		= \n");
    fprintf(fp, "# extra file to zip \n");
    fprintf(fp, "ZIPALSO			= \n");
    fprintf(fp, "# name of additional source files (.c and .h are already assumed) \n");
    fprintf(fp, "OTHER_FILES 		=  \n");
    fprintf(fp, "# name of libraries this plug'in depends on \n");
    fprintf(fp, "LIB_DEPS    		= \n");
    fprintf(fp, "# name of the dll to convert in lib.a \n");
    fprintf(fp, "OTHER_LIB_FROM_DLL 	=  	\n");
    fprintf(fp, "XVIN_DIR    		=../../\n");
    fprintf(fp, "################################################################################################## \n");
    fprintf(fp, "include $(XVIN_DIR)platform.mk		# platform specific definitions\n");
    fprintf(fp, "include $(XVIN_DIR)plugin.mk 		# plugin construction rules\n");





    /*
       fprintf(fp,"include ../../platform.mk\n\n");
       fprintf(fp,"all : ../../bin/%s.dll\n\n",app_name);
       fprintf(fp,"clean :\n\trm ../../bin/%s.dll %s.o ../../lib/lib%s.a\n\n",
       app_name,app_name,app_name);
       fprintf(fp,"zip  : %s.c %s.h makefile\n"
       "\tcd ../..; $(ZIPEXE) a -tzip plug-ins-src/%s/%s.zip \\\n"
       "\tplug-ins-src/%s/%s.c plug-ins-src/%s/%s.h \\\n"
       "\tplug-ins-src/%s/makefile ; cd plug-ins-src/%s \n\n",
       app_name,app_name,app_name,app_name,app_name,app_name,
       app_name,app_name,app_name,app_name);

       fprintf(fp,"../../bin/%s.dll : %s.o\n"
       "\tgcc  -o ../../bin/%s.dll -mwindows -shared $(PLUGINSLIB) %s.o  \\\n"
       "-lalleg -lxvin  -Wl,--export-all-symbols  -Wl,--enable-auto-import \\\n"
       "-Wl,--out-implib=../../lib/lib%s.a \n",app_name,app_name,app_name,app_name,app_name);
       fprintf(fp,"\n\n%s.o : %s.c %s.h\n\tgcc $(PCFLAGS) -o %s.o %s.c\n",
       app_name,app_name,app_name,app_name,app_name);
       */
    fclose(fp);





    strcpy(filename, path);
    strcat(filename, app_name);
    strcat(filename, ".c");
    fp = fopen(filename, "w");

    if (fp == NULL)
    {
        win_printf("I cannot create file!\n%s", backslash_to_slash(filename));
        return D_O_K;
    }

    fprintf(fp, "/*\n*    Plug-in program for plot treatement in Xvin.\n"
            " *\n *    V. Croquette\n  */\n");
    fprintf(fp, "#ifndef _%s_C_\n#define _%s_C_\n\n", APP_NAME, APP_NAME);
    fprintf(fp, "#include <allegro.h>\n"
            "# include \"xvin.h\"\n\n"
            "/* If you include other regular header do it here*/ "
            "\n\n\n/* But not below this define */\n"
            "# define BUILDING_PLUGINS_DLL\n # include \"%s.h\"\n\n", app_name);


    fprintf(fp, "\nint do_%s_hello(void)\n{\n"
            "  int i;\n", app_name);
    fprintf(fp, "\n  if(updating_menu_state != 0)	return D_O_K;\n");
    fprintf(fp, "\n  i = win_printf(\"Hello from %s\");", app_name);
    fprintf(fp, "\n  if (i == WIN_CANCEL) win_printf_OK(\"you have press WIN_CANCEL\");");
    fprintf(fp, "\n  else win_printf_OK(\"you have press OK\");\n  return 0;\n}\n");



    fprintf(fp, "\nint do_%s_rescale_data_set(void)\n{\n"
            "  int i, j;\n"
            "  O_p *op = NULL;\n"
            "  int nf;\n  static float factor = 1;\n"
            "  d_s *ds, *dsi;\n  pltreg *pr = NULL;\n\n", app_name);

    fprintf(fp, "\n  if(updating_menu_state != 0)	return D_O_K;\n\n");

    fprintf(fp, "  /* display routine action if SHIFT is pressed */\n"
            "  if (key[KEY_LSHIFT])\n"
            "  {\n"
            "    return win_printf_OK(\"This routine multiply the y coordinate\"\n"
            "  \"of a data set by a number\");\n"
            "  }\n");



    fprintf(fp, "  /* we first find the data that we need to transform */\n"
            "  if (ac_grep(cur_ac_reg,\"%%pr%%op%%ds\",&pr,&op,&dsi) != 3)\n"
            "    return win_printf_OK(\"cannot find data\");\n");

    fprintf(fp, "  nf = dsi->nx;	/* this is the number of points in the data set */\n"
            "  i = win_scanf(\"By what factor do you want to multiply y %%f\",&factor);\n"
            "  if (i == WIN_CANCEL)	return OFF;\n\n"
            "  if ((ds = create_and_attach_one_ds(op, nf, nf, 0)) == NULL)\n"
            "    return win_printf_OK(\"cannot create plot !\");\n\n"
            "  for (j = 0; j < nf; j++)\n"
            "  {\n"
            "    ds->yd[j] = factor * dsi->yd[j];\n"
            "    ds->xd[j] = dsi->xd[j];\n"
            "  }\n\n"
            "  /* now we must do some house keeping */\n"
            "  inherit_from_ds_to_ds(ds, dsi);\n"
            "  set_ds_treatement(ds,\"y multiplied by %%f\",factor);\n"
            "  /* refisplay the entire plot */\n"
            "  refresh_plot(pr, UNCHANGED);\n"
            "  return D_O_K;\n"
            "}\n");


    fprintf(fp, "\nint do_%s_rescale_plot(void)\n{\n"
            "  int i, j;\n"
            "  O_p *op = NULL, *opn = NULL;\n"
            "  int nf;\n  static float factor = 1;\n"
            "  d_s *ds, *dsi;\n  pltreg *pr = NULL;\n\n", app_name);

    fprintf(fp, "\n  if(updating_menu_state != 0)	return D_O_K;\n\n");

    fprintf(fp, "  /* display routine action if SHIFT is pressed */\n"
            "  if (key[KEY_LSHIFT])\n"
            "  {\n"
            "    return win_printf_OK(\"This routine multiply the y coordinate\"\n"
            "  \"of a data set by a number and place it in a new plot\");\n"
            "  }\n");



    fprintf(fp, "  /* we first find the data that we need to transform */\n"
            "  if (ac_grep(cur_ac_reg,\"%%pr%%op%%ds\",&pr,&op,&dsi) != 3)\n"
            "    return win_printf_OK(\"cannot find data\");\n");


    fprintf(fp, "  nf = dsi->nx;	/* this is the number of points in the data set */\n"
            "  i = win_scanf(\"By what factor do you want to multiply y %%f\",&factor);\n"
            "  if (i == WIN_CANCEL)	return OFF;\n\n");

    fprintf(fp, "  if ((opn = create_and_attach_one_plot(pr, nf, nf, 0)) == NULL)\n"
            "    return win_printf_OK(\"cannot create plot !\");\n"
            "  ds = opn->dat[0];\n\n"
            "  for (j = 0; j < nf; j++)\n"
            "  {\n"
            "    ds->yd[j] = factor * dsi->yd[j];\n"
            "    ds->xd[j] = dsi->xd[j];\n"
            "  }\n\n"
            "  /* now we must do some house keeping */\n"
            "  inherit_from_ds_to_ds(ds, dsi);\n"
            "  set_ds_treatement(ds,\"y multiplied by %%f\",factor);\n"
            "  set_plot_title(opn, \"Multiply by %%f\",factor);\n"
            "  if (op->x_title != NULL) set_plot_x_title(opn, op->x_title);\n"
            "  if (op->y_title != NULL) set_plot_y_title(opn, op->y_title);\n"
            "  opn->filename = Transfer_filename(op->filename);\n"
            "  uns_op_2_op(opn, op);\n"
            "  /* refisplay the entire plot */\n"
            "  refresh_plot(pr, UNCHANGED);\n"
            "  return D_O_K;\n}\n");



    fprintf(fp, "\nMENU *%s_plot_menu(void)\n"
            "{\n"
            "  static MENU mn[32];\n\n"
            "  if (mn[0].text != NULL)	return mn;\n"
            "  add_item_to_menu(mn,\"Hello example\", do_%s_hello,"
            "NULL,0,NULL);\n"
            "  add_item_to_menu(mn,\"data set rescale in Y\", do_%s_rescale_data_set,"
            "NULL,0,NULL);\n"
            "  add_item_to_menu(mn,\"plot rescale in Y\", "
            "do_%s_rescale_plot,NULL,0,NULL);\n"
            "  return mn;\n"
            "}\n", app_name, app_name, app_name, app_name);


    fprintf(fp, "\nint	%s_main(int argc, char **argv)\n{\n"
	    "  (void)argc;"
	    "  (void)argv;"
            "  add_plot_treat_menu_item ( \"%s\", NULL, %s_plot_menu(), 0, NULL);\n"
            "  return D_O_K;\n}\n", app_name, app_name, app_name);


    fprintf(fp, "\nint	%s_unload(int argc, char **argv)\n{\n"
	    "  (void)argc;"
	    "  (void)argv;"
            "  remove_item_to_menu(plot_treat_menu, \"%s\", NULL, NULL);\n"
            "  return D_O_K;\n}\n", app_name, app_name);


    fprintf(fp, "#endif\n\n");

    fclose(fp);

    strcpy(filename, path);
    strcat(filename, app_name);
    strcat(filename, ".h");
    fp = fopen(filename, "w");

    if (fp == NULL)
    {
        win_printf("I cannot create file!\n%s", backslash_to_slash(filename));
        return D_O_K;
    }

    fprintf(fp, "#pragma once\n");
    fprintf(fp, "PXV_FUNC(int, do_%s_rescale_plot, (void));\n", app_name);
    fprintf(fp, "PXV_FUNC(MENU*, %s_plot_menu, (void));\n", app_name);
    fprintf(fp, "PXV_FUNC(int, do_%s_rescale_data_set, (void));\n", app_name);
    fprintf(fp, "PXV_FUNC(int, %s_main, (int argc, char **argv));\n", app_name);

    fclose(fp);
    win_printf("directory %s has been created\n"
               "containing files : Makefile, %s.c %s.h\n"
               "modify the source file and compile by running make\n",
               backslash_to_slash(path), app_name, app_name);

    return D_O_K;

}
