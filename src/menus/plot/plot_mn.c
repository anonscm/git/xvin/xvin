# include <stdlib.h>
# include <string.h>

# ifndef _PLOT_MN_C_
# define _PLOT_MN_C_

#include "allegro.h"

#ifdef XV_WIN32
  #include "winalleg.h"
  #include "process.h"
#endif

#include "xvin.h"
#include "plot_op.h"
#include "gr_file.h"
#include "plot_reg.h"
#include "plot_gr.h"
#include "plot_mn.h"


XV_ARRAY(MENU,  project_menu);
XV_ARRAY(MENU,  fortify_menu);
XV_ARRAY(MENU,  help_menu);

int	do_quit(void);

MENU plot_menu[32] =
{
   { "&File",            NULL,             plot_file_menu,  0, NULL  },
   { "&Edit",	         NULL,             plot_edit_menu,  0, NULL  },
   { "&Project",         NULL,             project_menu,  0, NULL  },
   { "O&ptions",         NULL,             plot_options_menu,   0, NULL  },
   { "&Tools",           NULL,             plot_tools_menu,  0, NULL  },
   { "&Display",         NULL,             plot_display_menu,  0, NULL  },
   { "&Treatement",      NULL,             plot_treat_menu,  0, NULL  },
#ifdef FORTIFY
   { "&Fortify",         NULL,             fortify_menu,  0, NULL  },
#endif
   { "&Help",            NULL,             help_menu,  0, NULL  },
   { "&Quit\tQ",         do_quit,          NULL,       0, NULL  },
   { NULL,               NULL,             NULL,       0, NULL  },
   { NULL,               NULL,             NULL,       0, NULL  },
   { NULL,               NULL,             NULL,       0, NULL  },
   { NULL,               NULL,             NULL,       0, NULL  },
   { NULL,               NULL,             NULL,       0, NULL  },
   { NULL,               NULL,             NULL,       0, NULL  },

};


int	add_item_to_menu(MENU* mn, char* text, int (*proc)(void), struct MENU* child, int flags, void* dp)
{
	int i, j;
	for (i = j = 0; i < 32 && mn[i].text != NULL && j == 0; i++)
	{ //we do not add menu if it already exists except if it is a line separator!
	    if ((mn[i].text == text) && (mn[i].proc == proc) && (mn[i].child == child)
		&& (mn[i].flags == flags) && (mn[i].dp == dp) && (text[0] != 0)) j++;
	}
	if (i >= 30 || j != 0)	return -1;
	mn[i].text = text;
	mn[i].proc = proc;
	mn[i].child = child;
	mn[i].flags = flags;
	mn[i].dp = dp;
	mn[i+1].text = NULL;
	return i;
}

int	insert_item_to_menu_at_line(MENU* mn, int line, char* text, int (*proc)(void), struct MENU* child, int flags, void* dp)
{
  int i, last;
  //we find last line
  if (line < 0 || line > 30) return -1;
  for (last = 0; last < 32 && mn[last].text != NULL; last++);
  if (last >= 30) return -2;
  for (i = last - 1; i >= 0  && i >= line && mn[i].text != NULL; i--)
    { //we move stuff
	mn[i+1] = mn[i];
    }
  if (last+2 < 32) mn[last+2].text = NULL;
  mn[line].text = text;
  mn[line].proc = proc;
  mn[line].child = child;
  mn[line].flags = flags;
  mn[line].dp = dp;
  return line;
}

int	remove_item_to_menu(MENU* mn, char* text, int (*proc)(void), struct MENU* child)
{
	int i, j, k;

	if (mn == NULL || text == NULL)  return 1;
	//	if (proc == NULL && child == NULL)  return 1;
	for (i = 0; i < 32 && mn[i].text != NULL; i++);
	//win_printf("rem bef loop");
	for (j = 0, k = -1; j < i && k == -1; j++)
	{
	    if (strcmp(mn[j].text,text) == 0)
	      {
		if (proc != NULL)
		  {
		    if (mn[j].proc == proc) k = j;
		  }
		else if (child != NULL)
		  {
		    if (mn[j].child == child) k = j;
		  }
		else k = j;
	      }
	}
	//win_printf("rem after loop %d",k);
	if (k < 0) return 1;
	for (j = k; j < i; j++)
	{
	  mn[j].text = mn[j+1].text;
	  mn[j].proc = mn[j+1].proc;
	  mn[j].child = mn[j+1].child;
	  mn[j].flags = mn[j+1].flags;
	  mn[j].dp = mn[j+1].dp;
	}
	mn[i-1].text = NULL;
	//	win_printf("reduced to %d",i-1);
	return 0;
}

# endif
