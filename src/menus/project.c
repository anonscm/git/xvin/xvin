


# include "xvin.h"
# include "plot_op.h"
# include "gr_file.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "im_oi.h"
# include "im_reg.h"
# include "im_gr.h"
/*# include "project.h"*/
# include "plot_reg_gui.h"
# include "imr_reg_gui.h"
# include "xv_main.h"
# include "plot_mn.h"
# include "im_mn.h"

# include <allegro.h>
# include <stdlib.h>
# include <string.h>

pltreg *project[32];
int	project_cur = 0, project_n = 0, project_m = 32;

imreg *im_project[32];
int	im_project_cur = 0, im_project_n = 0, im_project_m = 32;

/* the menu filling */
int	proj_mn_cur = 1, proj_mn_n = 0, proj_mn_m = 32;

int switch_project(void);

MENU project_menu[32] =
  {
    { "Plot Proj. 1",       switch_project,    NULL,       0, (char *) "plot 0"  },
    { NULL,                          NULL,             NULL,       0, NULL  }
  };



int switch_project(void)
{
  int i;
  int n = 0;
  pltreg *pr = NULL, *prc = NULL;
  imreg *imr = NULL, *imrc = NULL;
  DIALOG *di_mn = NULL;
  //	char buf[1024];


			

  prc = find_pr_in_current_dialog(NULL);		
  imrc = find_imr_in_current_dialog(NULL);
			
  if (active_menu == NULL || active_menu->dp == NULL)	return D_O_K;
  //	win_printf("dp %s \n menu %s",(char*)active_menu->dp,(char*)active_menu->text);
  i = sscanf((char*)active_menu->dp,"plot %d",&n);


  if (i == 1)
    {
      if (n >= 0 && n <= project_n)
	{
	  pr = project[n];
	  if(updating_menu_state != 0)	
	    {
	      if (prc != NULL && pr == prc)	active_menu->flags |= D_SELECTED;
	      else active_menu->flags &= ~D_SELECTED;
	      return D_O_K;
	    }
	  //win_printf("plot n %d max %d",n,project_n);
	  project_cur = n;
	  the_dialog[2].dp = (void*)pr;	
	  the_dialog[2].proc = d_draw_Op_proc;
	  the_dialog[2].fg = makecol(0, 0, 0);
	  the_dialog[2].bg = makecol(255, 255, 255);					
	  di_mn = the_dialog + 1;
	  for(;screen_acquired;); // we wait for screen_acquired back to 0;     
	  screen_acquired = 1;			
	  acquire_bitmap(screen);
	  rectfill(screen,di_mn->x,di_mn->y,di_mn->x+di_mn->w-1,di_mn->y+di_mn->h-1, 
		   makecol(255,255,255));
	  release_bitmap(screen);
	  screen_acquired = 0;				  
	  di_mn->w = 0; 	   di_mn->h = 0;

	  xvin_d_menu_proc(MSG_END,the_dialog + 1,0);
			
	  the_dialog[1].dp = plot_menu;
	  xvin_d_menu_proc(MSG_START,the_dialog + 1,0);
	  pr->one_p->need_to_refresh = 1;
	  //d_menu_proc(MSG_DRAW,the_dialog + 1,0);	//								
	  /*			sprintf(buf,"%s %s",pr->one_p->filename, pr->one_p->dir);
				my_set_window_title(buf);*/
	  //win_printf("end switch plot %d in %d points %d",pr->cur_op,pr->n_op,pr->one_p->dat[0]->nx);
	  //do_one_plot(pr);
	  //win_printf("after do one plot");
	}
    }
  i = sscanf((char*)active_menu->dp,"image %d",&n);
  if (i == 1)
    {
      //		win_printf("image n %d max %d",n,im_project_n);
      if (n >= 0 && n <= im_project_n)
	{
	  imr = im_project[n];
	  if(updating_menu_state != 0)	
	    {
	      if (imrc != NULL && imr == imrc)	
		active_menu->flags |= D_SELECTED;
	      else active_menu->flags &= ~D_SELECTED;
	      return D_O_K;
	    }			
	  im_project_cur = n;
	  the_dialog[2].dp = (void*)imr;	
	  the_dialog[2].proc = d_draw_Im_proc;
	  the_dialog[2].fg = makecol(0, 0, 0);
	  the_dialog[2].bg = makecol(128, 128, 128);	
	  di_mn = the_dialog + 1;
	  for(;screen_acquired;); // we wait for screen_acquired back to 0;     
	  screen_acquired = 1;			
	  acquire_bitmap(screen);
	  rectfill(screen,di_mn->x,di_mn->y,di_mn->x+di_mn->w-1,di_mn->y+di_mn->h-1, 
		   makecol(255,255,255));
	  release_bitmap(screen);
	  screen_acquired = 0;				  
	  di_mn->w = 0; 	   di_mn->h = 0;
	  xvin_d_menu_proc(MSG_END,the_dialog + 1,0);
	  the_dialog[1].dp = image_menu;	
	  xvin_d_menu_proc(MSG_START,the_dialog + 1,0);	
	  //d_menu_proc(MSG_DRAW,the_dialog + 1,0);	//							
	  /*			sprintf(buf,"%s %s",imr->one_i->filename, imr->one_i->dir);
				my_set_window_title(buf);				*/
	}
    }
  //	broadcast_dialog_message(MSG_DRAW,0);	//
  return D_REDRAW; 
  //	return 0; //
}

int switch_project_to_this_pltreg(pltreg *pr)
{
  int i;
  pltreg  *prc = NULL;
  //imreg *imrc = NULL;
  DIALOG *di_mn = NULL;
			
  if (pr == NULL) return 1;
  prc = find_pr_in_current_dialog(NULL);	
  //imrc = find_imr_in_current_dialog(NULL);	
  if (prc == pr) return 0;
  for (i = 0; i <= project_n; i++)
    {
      prc = project[i];
      if (pr == prc)	
	{
	  project_cur = i;
	  the_dialog[2].dp = (void*)pr;	
	  the_dialog[2].proc = d_draw_Op_proc;
	  the_dialog[2].fg = makecol(0, 0, 0);
	  the_dialog[2].bg = makecol(255, 255, 255);					
	  di_mn = the_dialog + 1;
	  for(;screen_acquired;); // we wait for screen_acquired back to 0;     
	  screen_acquired = 1;			
	  
	  acquire_bitmap(screen);
	  rectfill(screen,di_mn->x,di_mn->y,di_mn->x+di_mn->w-1,di_mn->y+di_mn->h-1, 
		   makecol(255,255,255));
	  release_bitmap(screen);
	  screen_acquired = 0;				  
	  di_mn->w = 0; 	   di_mn->h = 0;
	  xvin_d_menu_proc(MSG_END,the_dialog + 1,0);
	  the_dialog[1].dp = plot_menu;
	  xvin_d_menu_proc(MSG_START,the_dialog + 1,0); 
	  xvin_d_menu_proc(MSG_DRAW,the_dialog + 1,0);
	  return 0;
	}
    }
  return 1;
}
int switch_project_to_this_imreg(imreg *imr)
{
  int i;
  imreg *imrc = NULL;
  DIALOG *di_mn = NULL;
	
  if (imr == NULL) return 1;
  imrc = find_imr_in_current_dialog(NULL);
  if (imrc == imr) return 0;
  for (i = 0; i <= im_project_n; i++)
    {
      imrc = im_project[i];
      //win_printf("scanning %d of %d im",i,im_project_n);
      if (imr == imrc)	
	{
	  im_project_cur = i;
	  the_dialog[2].dp = (void*)imr;	
	  the_dialog[2].proc = d_draw_Im_proc;
	  the_dialog[2].fg = makecol(0, 0, 0);
	  the_dialog[2].bg = makecol(128, 128, 128);	
	  di_mn = the_dialog + 1;
	  for(;screen_acquired;); // we wait for screen_acquired back to 0;     
	  screen_acquired = 1;
	  acquire_bitmap(screen);
	  rectfill(screen,di_mn->x,di_mn->y,di_mn->x+di_mn->w-1,di_mn->y+di_mn->h-1, 
		   makecol(255,255,255));
	  release_bitmap(screen);
	  screen_acquired = 0;				  
	  di_mn->w = 0; 	   di_mn->h = 0;
	  xvin_d_menu_proc(MSG_END,the_dialog + 1,0);
	  the_dialog[1].dp = image_menu;	
	  xvin_d_menu_proc(MSG_START,the_dialog + 1,0);
	  xvin_d_menu_proc(MSG_DRAW,the_dialog + 1,0);	
	  //d_menu_proc(MSG_DRAW,the_dialog + 1,0); 
	  //win_printf("found right stuff %d",i);
	  return 0; 
	}
    }
  return 1;
}


int change_menu_name_of_current_project(char *text)
{
  if (proj_mn_cur < 0 || proj_mn_cur > proj_mn_n) return 1;
  if (text == NULL || strlen(text) < 1) return 1;
  if (project_menu[proj_mn_cur].text != NULL)    
    {
      free(project_menu[proj_mn_cur].text);
      project_menu[proj_mn_cur].text = NULL;
    }
  project_menu[proj_mn_cur].text = strdup(text);
  return 0;
}

int what_is_current_project(MENU  *mn, int *pltreg_nb, int *imreg_nb)
{
  int i;

  if (proj_mn_cur < 0 || proj_mn_cur > proj_mn_n) return -1;
  if (mn) mn = project_menu + proj_mn_cur;
  if (the_dialog[2].proc == d_draw_Im_proc) // project_menu[proj_mn_cur].proc
    {
      for(i = 0; i <= im_project_n; i++)
	{
	  if (im_project[i] == the_dialog[2].dp) //project_menu[proj_mn_cur].dp)
	    {
	      if (pltreg_nb) *pltreg_nb = -1;
	      if (imreg_nb) *imreg_nb = i;
	      return proj_mn_cur;
	    }
	}
    }
  else
    {
      for(i = 0; i <= project_n; i++)
	{
	  if (project[i] == the_dialog[2].dp) //project_menu[proj_mn_cur].dp)
	    {
	      if (pltreg_nb) *pltreg_nb = i;
	      if (imreg_nb) *imreg_nb = -1;
	      return proj_mn_cur;
	    }
	}
    }
  return proj_mn_cur;
}
pltreg	*create_and_register_new_plot_project(int x, int y, int w, int h)
{
  pltreg *pr = NULL;
  DIALOG *di_mn = NULL;
	
  pr = build_plot_region(x, y, w, h);
  if (pr == NULL)	return NULL;		
  pr->auto_scale = 1;	

  if (project_n < project_m)
    {
      project_n++;
      project_cur = project_n;
      project[project_n] = pr;
      proj_mn_n++;
      proj_mn_cur = proj_mn_n;
      project_menu[proj_mn_n].text = my_sprintf(NULL,"Plot Proj. %d",project_n+1);
      project_menu[proj_mn_n].dp = my_sprintf(NULL,"plot %d",project_n);
      project_menu[proj_mn_n].proc = switch_project;		
      project_menu[proj_mn_n+1].text = NULL;
      the_dialog[2].dp = (void*)pr;	
      the_dialog[2].proc = d_draw_Op_proc;	
      di_mn = the_dialog + 1;
      for(;screen_acquired;); // we wait for screen_acquired back to 0;     
      screen_acquired = 1;			
      acquire_bitmap(screen);
      rectfill(screen,di_mn->x,di_mn->y,di_mn->x+di_mn->w-1,di_mn->y+di_mn->h-1, 
	       makecol(255,255,255));
      release_bitmap(screen);
      screen_acquired = 0;			      
      di_mn->w = 0; 	   di_mn->h = 0;
      xvin_d_menu_proc(MSG_END,the_dialog + 1,0);		
      the_dialog[1].dp = plot_menu;	
      xvin_d_menu_proc(MSG_START,the_dialog + 1,0);	
      xvin_d_menu_proc(MSG_DRAW,the_dialog + 1,0);				
    }
  return pr;
}



pltreg	*create_and_register_hidden_new_plot_project(int x, int y, int w, int h, char *name)
{
  pltreg *pr = NULL;
	
  pr = build_plot_region(x, y, w, h);
  if (pr == NULL)	return NULL;		
  pr->auto_scale = 1;	

  if (project_n < project_m)
    {
      project_n++;
      project_cur = project_n;
      project[project_n] = pr;
      proj_mn_n++;
      proj_mn_cur = proj_mn_n;
      if (name == NULL || strlen(name) < 1)
	project_menu[proj_mn_n].text = my_sprintf(NULL,"Plot Proj. %d",project_n+1);
      else project_menu[proj_mn_n].text = strdup(name);
      project_menu[proj_mn_n].dp = my_sprintf(NULL,"plot %d",project_n);
      project_menu[proj_mn_n].proc = switch_project;		
      project_menu[proj_mn_n+1].text = NULL;

    }
  return pr;
}



imreg	*create_and_register_new_image_project(int x, int y, int w, int h)
{
  imreg *imr = NULL;
  DIALOG *di_mn = NULL;
	
  imr = build_image_region(x, y, w, h, 0);
  if (imr == NULL)	return NULL;		

  if (im_project_n < im_project_m)
    {
      im_project_n++;
      im_project_cur = im_project_n;
      im_project[im_project_n] = imr;
      proj_mn_n++;
      proj_mn_cur = proj_mn_n;
      project_menu[proj_mn_n].text = my_sprintf(NULL,"Image Proj. %d",im_project_n+1);
      project_menu[proj_mn_n].dp = my_sprintf(NULL,"image %d",im_project_n);
      project_menu[proj_mn_n].proc = switch_project;		
      project_menu[proj_mn_n+1].text = NULL;
      the_dialog[2].dp = (void*)imr;		
      the_dialog[2].proc = d_draw_Im_proc;
      di_mn = the_dialog + 1;
      for(;screen_acquired;); // we wait for screen_acquired back to 0;     
      screen_acquired = 1;			
      
      acquire_bitmap(screen);
      rectfill(screen,di_mn->x,di_mn->y,di_mn->x+di_mn->w-1,di_mn->y+di_mn->h-1, 
	       makecol(128,255,128));
      rectfill(plt_buffer,di_mn->x,di_mn->y,di_mn->x+di_mn->w-1,di_mn->y+di_mn->h-1, 
	       makecol(128,128,255));
      release_bitmap(screen);
      screen_acquired = 0;			      
      di_mn->w = 0; 	   di_mn->h = 0;
      //		win_printf("changing menu");
      xvin_d_menu_proc(MSG_END,the_dialog + 1,0);		
      the_dialog[1].dp = image_menu;		
      xvin_d_menu_proc(MSG_START,the_dialog + 1,0);			
      xvin_d_menu_proc(MSG_DRAW,the_dialog + 1,0);	
      //win_printf("changing menu 2");
    }
  return imr;
}



imreg	*create_and_register_new_empty_image_project(int x, int y, int w, int h)
{
  imreg *imr = NULL;
  DIALOG *di_mn = NULL;
	
  imr = build_an_empty_image_region(x, y, w, h);
  if (imr == NULL)	return NULL;		

  if (im_project_n < im_project_m)
    {
      im_project_n++;
      im_project_cur = im_project_n;
      im_project[im_project_n] = imr;
      proj_mn_n++;
      proj_mn_cur = proj_mn_n;
      project_menu[proj_mn_n].text = my_sprintf(NULL,"Image Proj. %d",im_project_n+1);
      project_menu[proj_mn_n].dp = my_sprintf(NULL,"image %d",im_project_n);
      project_menu[proj_mn_n].proc = switch_project;		
      project_menu[proj_mn_n+1].text = NULL;
      the_dialog[2].dp = (void*)imr;		
      the_dialog[2].proc = d_draw_Im_proc;
      di_mn = the_dialog + 1;
      for(;screen_acquired;); // we wait for screen_acquired back to 0;     
      screen_acquired = 1;			
      acquire_bitmap(screen);
      rectfill(screen,di_mn->x,di_mn->y,di_mn->x+di_mn->w-1,di_mn->y+di_mn->h-1, 
	       makecol(128,255,128));
      rectfill(plt_buffer,di_mn->x,di_mn->y,di_mn->x+di_mn->w-1,di_mn->y+di_mn->h-1, 
	       makecol(128,128,255));
      release_bitmap(screen);
      screen_acquired = 0;			
      di_mn->w = 0; 	   di_mn->h = 0;
      //		win_printf("changing menu");
      xvin_d_menu_proc(MSG_END,the_dialog + 1,0);		
      the_dialog[1].dp = image_menu;		
      xvin_d_menu_proc(MSG_START,the_dialog + 1,0);			
      xvin_d_menu_proc(MSG_DRAW,the_dialog + 1,0);	
      //win_printf("changing menu 2");
    }
  return imr;
}

imreg	*create_and_register_hidden_new_image_project(int x, int y, int w, int h, char *name)
{
  imreg *imr = NULL;
	
  imr = build_image_region(x, y, w, h, 0);
  if (imr == NULL)	return NULL;		

  if (im_project_n < im_project_m)
    {
      im_project_n++;
      im_project_cur = im_project_n;
      im_project[im_project_n] = imr;
      proj_mn_n++;
      proj_mn_cur = proj_mn_n;
      if (name == NULL || strlen(name) < 1)
	project_menu[proj_mn_n].text = my_sprintf(NULL,"Image Proj. %d",im_project_n+1);
      else project_menu[proj_mn_n].text = strdup(name);;
      project_menu[proj_mn_n].dp = my_sprintf(NULL,"image %d",im_project_n);
      project_menu[proj_mn_n].proc = switch_project;		
      project_menu[proj_mn_n+1].text = NULL;

    }
  return imr;
}


DIALOG *find_dialog_associated_to_menu(DIALOG *di, int *di_n)
{
  int i;

  if (di == NULL) di = active_dialog;
  for (i = 0; (di[i].proc != NULL) && (i < 256); i++)
    {
      if (di[i].proc == xvin_d_menu_proc)
        {
	  if (di_n != NULL)  *di_n = i;
          return di+i;
        }
    }
  if (i >= 256)   allegro_message("Hi dialog index %d",i);
  return NULL;
}




# ifdef TO_DO

int	destroy_project(int n)
{


  if (project_n < project_m)
    {
      project_n++;
      project_cur = project_n;
      project[project_n] = pr;
      proj_mn_n++;
      proj_mn_cur = proj_mn_n;
      project_menu[proj_mn_n].text = my_sprintf(NULL,"Plot Proj. %d",project_n+1);
      project_menu[proj_mn_n].dp = my_sprintf(NULL,"plot %d",project_n);
      project_menu[proj_mn_n].proc = switch_project;		
      project_menu[proj_mn_n+1].text = NULL;
      the_dialog[2].dp = pr;	
      the_dialog[2].proc = d_draw_Op_proc;	
      the_dialog[1].dp = plot_menu;		
    }
  return pr;
}



int switch_to_previous_project(void)
{
  int i;
  int n = 0;
  pltreg *pr = NULL;
  imreg *imr = NULL;
  //	char buf[1024];

	
  if(updating_menu_state != 0)	return D_O_K;		
			
		
  if (active_menu->dp == NULL)	return D_O_K;
  //	win_printf("dp %s \n menu %s",(char*)active_menu->dp,(char*)active_menu->text);
  i = sscanf((char*)active_menu->dp,"plot %d",&n);
  if (i == 1)
    {
      //		win_printf("plot n %d max %d",n,project_n);
      if (n >= 0 && n <= project_n)
	{
	  pr = project[n];
	  project_cur = n;
	  the_dialog[2].dp = pr;	
	  the_dialog[2].proc = d_draw_Op_proc;
	  the_dialog[2].fg = makecol(0, 0, 0);
	  the_dialog[2].bg = makecol(255, 255, 255);					
	  the_dialog[1].dp = plot_menu;
	  /*			sprintf(buf,"%s %s",pr->one_p->filename, pr->one_p->dir);
				my_set_window_title(buf);*/
	}
    }
  i = sscanf((char*)active_menu->dp,"image %d",&n);
  if (i == 1)
    {
      //		win_printf("image n %d max %d",n,im_project_n);
      if (n >= 0 && n <= im_project_n)
	{
	  imr = im_project[n];
	  im_project_cur = n;
	  the_dialog[2].dp = imr;	
	  the_dialog[2].proc = d_draw_Im_proc;
	  the_dialog[2].fg = makecol(0, 0, 0);
	  the_dialog[2].bg = makecol(128, 128, 128);	
	  the_dialog[1].dp = image_menu;	
	  /*			sprintf(buf,"%s %s",imr->one_i->filename, imr->one_i->dir);
				my_set_window_title(buf);				*/
	}
    }
  broadcast_dialog_message(MSG_DRAW,0);	
  return 0;
}

# endif












