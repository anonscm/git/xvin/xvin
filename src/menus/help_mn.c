#include "help_mn.h"
#include "xvin.h"
#ifdef XV_UNIX
#include <sys/wait.h>
#endif
#include "gitinfo.h"

//MENU help_menu[32] = {NULL};


#ifdef XV_UNIX


int help_me_xv(void)
{
    char htmlhelp[512] = {0}, *c = NULL; 

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"firefox %s",prog_dir);

    c = strstr(htmlhelp,"build");
    if (c == NULL)     c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;    
    my_strncat(htmlhelp,"docs/html/xvin.html &",sizeof(htmlhelp));
    system(htmlhelp);
    return D_O_K;
}
int help_me_xv_chm(void)
{
    static char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"xchm %s",prog_dir);
    c = strstr(htmlhelp,"build");
    if (c == NULL)  c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;    
    my_strncat(htmlhelp,"docs/chm/xvin.chm &",sizeof(htmlhelp));
    system(htmlhelp);
    return D_O_K;
}

int help_me_pico_html(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"firefox %s",prog_dir);
    c = strstr(htmlhelp,"build");
    if (c == NULL)    c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;    
    my_strncat(htmlhelp,"docs/html/picotw.html &",sizeof(htmlhelp));
    system(htmlhelp);
    return D_O_K;
}
int help_me_pico_chm(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"xchm %s",prog_dir);
    c = strstr(htmlhelp,"build");
    if (c == NULL)    c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;    
    my_strncat(htmlhelp,"docs/chm/picotw.chm &",sizeof(htmlhelp));
    system(htmlhelp);
    return D_O_K;
}

int help_me_pico_device_html(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"firefox %s",prog_dir);
    c = strstr(htmlhelp,"build");
    if (c == NULL) c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;    
    my_strncat(htmlhelp,"docs/html/picodv.html &",sizeof(htmlhelp));
    system(htmlhelp);
    return D_O_K;
}
int help_me_pico_device_chm(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"xchm %s",prog_dir);
    c = strstr(htmlhelp,"build");
    if (c == NULL) c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;    
    my_strncat(htmlhelp,"docs/chm/picodv.chm &",sizeof(htmlhelp));
    system(htmlhelp);
    return D_O_K;
}

int help_me_pico_bio_html(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"firefox %s",prog_dir);
    c = strstr(htmlhelp,"build");
    if (c == NULL) c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;
    my_strncat(htmlhelp,"docs/html/picobi.html &",sizeof(htmlhelp));
    system(htmlhelp);
    return D_O_K;
}
int help_me_pico_bio_chm(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"xchm %s &",prog_dir);
    c = strstr(htmlhelp,"build");
    if (c == NULL) c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;
    my_strncat(htmlhelp,"docs/chm/picobi.chm &",sizeof(htmlhelp));
    system(htmlhelp);
    return D_O_K;
}

int help_me_pico_pias_html(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"firefox %s",prog_dir);
    c = strstr(htmlhelp,"build");
    if (c == NULL) c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0; 
    my_strncat(htmlhelp,"docs/html/pias.html &",sizeof(htmlhelp));
    system(htmlhelp);
    return D_O_K;	
}
int help_me_pico_pias_chm(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"xchm %s",prog_dir);
    //win_printf("prog dir %s\n",prog_dir);
    //#ifdef W_PICO_INCLUDE_CONFIG_H_WAF
    c = strstr(htmlhelp,"build");
    //#else
    if (c == NULL) c = strstr(htmlhelp,"bin");
    //#endif    
    if (c != NULL) c[0] = 0;
    else win_printf("Could not find either build or bin in %s",htmlhelp);

    my_strncat(htmlhelp,"docs/chm/piashlp.chm &",sizeof(htmlhelp));
    //win_printf("file %s\n",htmlhelp);
    system(htmlhelp);
    return D_O_K;
}


# endif


#ifdef XV_WIN32 

int help_me_xv(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"%s",prog_dir);
    c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;
    my_strncat(htmlhelp,"docs\\html\\xvin.html",sizeof(htmlhelp));
    //spawnlp(P_NOWAIT,"c:\\Program Files\\Internet Explorer\\iexplore.exe","iexplore.exe",htmlhelp,NULL);
    //win_printf("file %s",backslash_to_slash(htmlhelp));

    ShellExecute(
                 HWND_DESKTOP, //Parent window
                 "open",       //Operation to perform
                 htmlhelp,     //Path to program
                 NULL,         //Parameters
                 NULL,         //Default directory
                 SW_SHOW);     //How to open

    return D_O_K;
}
int help_me_al(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"%s",prog_dir);    
    c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;
    my_strncat(htmlhelp,"docs\\allegro\\html\\allegro.html",sizeof(htmlhelp));
    //  spawnlp(P_NOWAIT,"c:\\Program Files\\Internet Explorer\\iexplore.exe","iexplore.exe",htmlhelp,NULL);

    ShellExecute(
                 HWND_DESKTOP, //Parent window
                 "open",       //Operation to perform
                 htmlhelp,     //Path to program
                 NULL,         //Parameters
                 NULL,         //Default directory
                 SW_SHOW);     //How to open

    return D_O_K;
}

int help_me_xv_chm(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"%s",prog_dir);        
    c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;
    my_strncat(htmlhelp,"docs\\chm\\xvin.chm",sizeof(htmlhelp));
    //spawnlp(P_NOWAIT,htmlhelp,htmlhelp,NULL);
    //win_printf("file %s",backslash_to_slash(htmlhelp));

    ShellExecute(
                 HWND_DESKTOP, //Parent window
                 "open",       //Operation to perform
                 htmlhelp,     //Path to program
                 NULL,         //Parameters
                 NULL,         //Default directory
                 SW_SHOW);     //How to open

    return D_O_K;
}
int help_me_al_chm(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"%s",prog_dir);            
    c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;
    my_strncat(htmlhelp,"docs\\allegro\\chm\\allegro.chm",sizeof(htmlhelp));
    //spawnlp(P_NOWAIT,htmlhelp,htmlhelp,NULL);

    ShellExecute(
                 HWND_DESKTOP, //Parent window
                 "open",       //Operation to perform
                 htmlhelp,     //Path to program
                 NULL,         //Parameters
                 NULL,         //Default directory
                 SW_SHOW);     //How to open

    return D_O_K;
}

int help_me_pico(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"%s",prog_dir);
    c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;
    my_strncat(htmlhelp,"docs\\html\\picotw.html",sizeof(htmlhelp));

    ShellExecute(
                 HWND_DESKTOP, //Parent window
                 "open",       //Operation to perform
                 htmlhelp,     //Path to program
                 NULL,         //Parameters
                 NULL,         //Default directory
                 SW_SHOW);     //How to open

    return D_O_K;
}

int help_me_pico_chm(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"%s",prog_dir);    
    c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;
    my_strncat(htmlhelp,"docs\\chm\\picotw.chm",sizeof(htmlhelp));

    ShellExecute(
                 HWND_DESKTOP, //Parent window
                 "open",       //Operation to perform
                 htmlhelp,     //Path to program
                 NULL,         //Parameters
                 NULL,         //Default directory
                 SW_SHOW);     //How to open

    return D_O_K;
}

int help_me_pico_device_chm(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"%s",prog_dir);        
    c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;
    my_strncat(htmlhelp,"docs\\chm\\picodv.chm",sizeof(htmlhelp));

    ShellExecute(
                 HWND_DESKTOP, //Parent window
                 "open",       //Operation to perform
                 htmlhelp,     //Path to program
                 NULL,         //Parameters
                 NULL,         //Default directory
                 SW_SHOW);     //How to open

    return D_O_K;
}

int help_me_pico_bio_chm(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"%s",prog_dir);
    c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;
    my_strncat(htmlhelp,"docs\\chm\\picobi.chm",sizeof(htmlhelp));

    ShellExecute(
                 HWND_DESKTOP, //Parent window
                 "open",       //Operation to perform
                 htmlhelp,     //Path to program
                 NULL,         //Parameters
                 NULL,         //Default directory
                 SW_SHOW);     //How to open

    return D_O_K;
}
int help_me_pico_pias_chm(void)
{
    char htmlhelp[512] = {0}, *c = NULL;

    if(updating_menu_state != 0)	return D_O_K;
    snprintf(htmlhelp,sizeof(htmlhelp),"%s",prog_dir);
    c = strstr(htmlhelp,"bin");
    if (c) c[0] = 0;
    my_strncat(htmlhelp,"docs\\chm\\pias.chm",sizeof(htmlhelp));

    ShellExecute(
                 HWND_DESKTOP, //Parent window
                 "open",       //Operation to perform
                 htmlhelp,     //Path to program
                 NULL,         //Parameters
                 NULL,         //Default directory
                 SW_SHOW);     //How to open

    return D_O_K;
}

#endif

int help_apropos(void)
{
    //int svn_nb = 3341;

    char program[128] = "Unknown";
    char sdk_version[128] = "Sim";
    char focus[128] = "None";
    char platform[128] = "Unknown";

    if(updating_menu_state != 0)	return D_O_K;
#ifdef PLAYITSAM
    snprintf(program, sizeof(program), "PlayItSam");
#elif defined(PIAS)
    snprintf(program, sizeof(program), "PlayItAgainSam");
#elif defined(MOVIE)
    snprintf(program, sizeof(program), "Pico Movie");
#elif defined(GAME)
    snprintf(program, sizeof(program), "Pico Simulation");
#elif defined (GIGE)
    snprintf(program, sizeof(program), "PicoJai");
# ifdef SDK_1_2_1
    snprintf(sdk_version, sizeof(sdk_version), "JAI SDK 1.2.1");
# else //defined(SDK_1_2_5)                                        
    snprintf(sdk_version, sizeof(sdk_version), "JAI SDK 1.2.5");
# endif
#elif defined (UEYE)                                    
    snprintf(sdk_version, sizeof(sdk_version), "uEye");
#else
    snprintf(program, sizeof(program), "Xvin");
#endif
#if defined (MAD_CITY_LAB)
    snprintf(focus, sizeof(focus), "Mad City Piezo");
#elif defined (PIFOC_DAS1602)
    snprintf(focus, sizeof(focus), "Pifoc TS");
#endif
#ifdef XV_WIN32
    snprintf(platform, sizeof(platform), "Windows");
#elif defined(XV_UNIX)
    snprintf(platform, sizeof(platform), "Linux");
#elif defined(XV_MACOS)
    snprintf(platform, sizeof(platform), "Apple");
#endif

    win_printf("%s\n"
               "Compiled for: %s\n"
               "Camera: %s\n"
               "Piezo: %s\n"
               "Git:\n"
               /*
               "\t- Commit:  %s\n"
               "\t- Date:    %s\n"
               "\t- Author:  %s\n"
               "\t- Version: %s\n",
               "\t- Branch:  %s" */
               ,program, platform, sdk_version, focus);
               //, GIT_COMMIT_HASH, GIT_COMMIT_DATE, GIT_COMMIT_AUTHOR, GIT_COMMIT_VERSION, GIT_COMMIT_BRANCH);
    return D_O_K;
}

MENU help_menu[32] = 
{ 
    { "A propos",	   help_apropos,    NULL,  0, NULL  },
#if defined(XV_UNIX) // && !defined(NO_STD_HELP)    
    { "Software help (chm)",	   help_me_pico_chm,    NULL,  0, NULL  },
    { "Software help (html)",	   help_me_pico_html,    NULL,  0, NULL  },    
    { "Hardware help (chm)",	   help_me_pico_device_chm,    NULL,  0, NULL  },
    { "Hardware help (html)",	   help_me_pico_device_html,    NULL,  0, NULL  },    
    { "Wet stuff help (chm)",	   help_me_pico_bio_chm,    NULL,  0, NULL  },
    { "Wet stuff help (html)",	   help_me_pico_bio_html,    NULL,  0, NULL  },    
    { "PlayItAgainSam help (chm)",	   help_me_pico_pias_chm,    NULL,  0, NULL  },
    { "PlayItAgainSam help (html)",	   help_me_pico_pias_html,    NULL,  0, NULL  },    
    { "Xvin (chm)",          help_me_xv_chm,        NULL,  0, NULL  },
    { "Xvin (Html)",          help_me_xv,        NULL,  0, NULL  },    

    
#endif
#if defined(XV_WIN32) && !defined(NO_STD_HELP)
        { "Software help (CHM)",	   help_me_pico_chm,    NULL,  0, NULL  },
        { "Hardware help (CHM)",	   help_me_pico_device_chm,    NULL,  0, NULL  },
        { "Wet stuff help (CHM)",	   help_me_pico_bio_chm,    NULL,  0, NULL  },
        { "PlayItAgainSam help (CHM)",	   help_me_pico_pias_chm,    NULL,  0, NULL  },
        { "Xvin Html",          help_me_xv,        NULL,  0, NULL  },
        { "Xvin CHM",	   help_me_xv_chm,    NULL,  0, NULL  },
        { "Allegro Html",       help_me_al,        NULL,  0, NULL  },
        { "Allegro CHM",        help_me_al_chm,    NULL,   0, NULL  },
#endif
        { NULL,               NULL,                NULL,       0, NULL  }
};



