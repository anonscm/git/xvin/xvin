# ifndef _I_FILE_MN_C_
# define _I_FILE_MN_C_
#include "config.h"

# include "file_picker_gui.h"

# include "ctype.h"
# include "allegro.h"
# include "allegro/internal/aintern.h"

#ifdef BUILDING_DLL
#include "xv_plugins.h"
#endif

# include "xvin.h"
# include "plot_op.h"
# include "gr_file.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "xv_main.h"
# include "im_reg.h"
# include "imr_reg_gui.h"
# include "im_mn.h"
# include "plot_mn.h"
# include "dev_pcx.h"
# include "dev_ps.h"
# include "plot2box.h"


// commented 2004/02/07, unused since plugin:
// extern int load_jpeg_file_in_imreg(imreg *imr, char *file, char *path);

XV_VAR(int,  color_enable);
XV_VAR(int,  eps_file_flag);

XV_FUNC(int, do_quit, (void));
XV_FUNC(int, do_load, (void));
XV_FUNC(int, do_load_in_new_project, (void));

XV_FUNC(int, load_cgr_multi_plots, (void));
XV_FUNC(int, do_save_all_plots_from_project, (void));
XV_FUNC(int, load_im_file_in_imreg, (imreg *imr, char *file, char *path));
XV_FUNC(imreg *, create_and_register_new_image_project, (int x, int y, int w, int h));



#define BIG_FONT                         0        /* FONT */
int	do_load_in_new_project(void);
int	run_module(void);
int path_is_accessible(char *path);

extern 	MENU im_project_menu[];

MENU im_recently_loaded[32]  = {0};
char re_ifile[16][256] = {0};
char re_ipath[16][256] = {0};
int  n_ire = 0;

//char  *fui = NULL;  // space for multiple files

int	reorder_loaded_image_file_list(char *last, char *path);

MENU image_file_import_menu[32] =
{
    { "nothing?",                          NULL,             NULL,       0, NULL  },
    { NULL,                          	NULL,             NULL,       0, NULL  }
};
MENU image_file_export_menu[32] =
{
    { "nothing?",                          NULL,             NULL,       0, NULL  },
    { NULL,                          	NULL,             NULL,       0, NULL  }
};





int	do_load_recently_loaded_image(void)
{
    int j = 0;
    imreg *imr = NULL;
    O_i *oi = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    if (active_menu->text == NULL || active_menu->dp == NULL)
    {
        return D_O_K;
    }

    imr = find_imr_in_current_dialog(NULL);
    oi = load_im_file_in_oi(active_menu->text, (char *)active_menu->dp);

    if (oi == NULL)
        return win_printf_OK("could not load image\n%s\n"
                             "from path %s", active_menu->text,
                             backslash_to_slash((char *)active_menu->dp));

    if (imr == NULL)
    {
        imr = create_and_register_new_image_project(0,   32,  900,  668);
        j = 1;
    }

    if (imr == NULL)
    {
        return 0;
    }

    add_image(imr, oi->im.data_type, (void *)oi);

    if (j == 1)
    {
        remove_from_image(imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
    }

    //load_im_file_in_imreg(imr, active_menu->text, (char*)active_menu->dp);
    select_image_of_imreg(imr, imr->n_oi - 1);
    reorder_loaded_image_file_list(active_menu->text, (char *)active_menu->dp);
    //win_printf("bef redraw 2");
    return D_REDRAW;
}

int	reorder_loaded_image_file_list(char *last, char *path)
{
    int i;
    int n, i_l, nn;
    char last_n[128] = {0}, cp_last[256] = {0}, cp_path[256] = {0}; //, *f[32], *p[32];

    if (n_ire == 0)
    {
        n = get_config_int("IMAGE-GR-FILE", "last_loaded_n", 0);
    }
    else
    {
        n = n_ire;
    }

    im_recently_loaded[n].text = NULL;

    if (n_ire == 0)
    {
        for (i = 0; i < n; i++)
        {
            snprintf(last_n, 128, "last_loaded_%d", i);
            strncpy(re_ifile[i], get_config_string("IMAGE-GR-FILE", last_n, ""), 256);
            snprintf(last_n, 128, "last_loaded_path_%d", i);
            strncpy(re_ipath[i], get_config_string("IMAGE-GR-FILE", last_n, ""), 256);
            im_recently_loaded[i].text = re_ifile[i];
            im_recently_loaded[i].dp = (void *)re_ipath[i];
            im_recently_loaded[i].proc = do_load_recently_loaded_image;

            if (path_is_accessible(re_ipath[i]))
            {
                im_recently_loaded[i].flags &= ~D_DISABLED;
            }
            else
            {
                im_recently_loaded[i].flags |= D_DISABLED;
            }
        }
    }

    for (i = 0, i_l = -1; i < n; i++)
    {
        if (last != NULL && path != NULL
                && (strcmp(re_ifile[i], last) == 0) && (strcmp(re_ipath[i], path) == 0))
        {
            i_l = i;
        }
    }

    if (last == NULL || path == NULL)
    {
        return 0;
    }

    strncpy(cp_last, last, 255);
    strncpy(cp_path, path, 255);
    nn = (i_l == -1 && n < 16) ? n + 1 : n;
    set_config_int("IMAGE-GR-FILE", "last_loaded_n", nn);
    //    win_printf("%d recent",nn);
    n = nn;
    im_recently_loaded[nn].text = NULL;
    nn = (i_l == -1) ? nn - 1 : i_l;

    for (i = nn; i > 0; i--)
    {
        strncpy(re_ifile[i], re_ifile[i - 1], 255);
        strncpy(re_ipath[i], re_ipath[i - 1], 255);
        im_recently_loaded[i].text = re_ifile[i];
        im_recently_loaded[i].dp = (void *)re_ipath[i];
        im_recently_loaded[i].proc = do_load_recently_loaded_image;

        if (path_is_accessible(re_ipath[i]))
        {
            im_recently_loaded[i].flags &= ~D_DISABLED;
        }
        else
        {
            im_recently_loaded[i].flags |= D_DISABLED;
        }
    }

    strcpy(re_ifile[0], cp_last);
    strcpy(re_ipath[0], cp_path);
    im_recently_loaded[0].dp = (void *)re_ipath[0];
    snprintf(last_n, 128, "last_loaded_0");
    im_recently_loaded[0].text = re_ifile[0];
    im_recently_loaded[0].proc = do_load_recently_loaded_image;

    if (path_is_accessible(re_ipath[0]))
    {
        im_recently_loaded[0].flags &= ~D_DISABLED;
    }
    else
    {
        im_recently_loaded[0].flags |= D_DISABLED;
    }

    for (i = 0; i < n; i++)
    {
        snprintf(last_n, 128, "last_loaded_%d", i);
        set_config_string("IMAGE-GR-FILE", last_n, re_ifile[i]);
        snprintf(last_n, 128, "last_loaded_path_%d", i);
        set_config_string("IMAGE-GR-FILE", last_n, re_ipath[i]);
    }

    flush_config_file();
    return 0;
}



int	do_save_image(void)
{
    char path[512] = {0};
    char file[256] = {0};
    char *fullfile = NULL;
    imreg *imr = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    imr = find_imr_in_current_dialog(NULL);

    if (imr == NULL)
    {
        return win_printf_OK("cannot find Imr");
    }

    fullfile = save_one_file_config("Save image (*.gr)", imr->one_i->dir, imr->one_i->filename,
                                    "Image Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0", "IMAGE-GR-FILE", "last_saved");

    if (fullfile)
    {
        extract_file_name(file, 256, fullfile);
        extract_file_path(path, 512, fullfile);
        save_one_image(imr->one_i, fullfile);
        set_oi_filename(imr->one_i, file);
        set_oi_path(imr->one_i, path);
        reorder_loaded_image_file_list(file, path);
        reorder_visited_path_list(path);
    }

    broadcast_dialog_message(MSG_DRAW, 0);
    return D_O_K;
}








int	do_load_im(void)
{
    int j = 0;
    char path[512] = {0};
    char file[256] = {0};
    imreg *imr = NULL;
    O_i *oi = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    file_list_t *file_list = open_files_config("Load plot image(*.gr)", fui,
                             "Plot Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0", "IMAGE-GR-FILE", "last_loaded");
    file_list_t *cursor = file_list;

    while (cursor != NULL)
    {
        set_config_string("IMAGE-GR-FILE", "last_loaded", cursor->data);
        extract_file_path(path, sizeof(path), cursor->data);
        extract_file_name(file, sizeof(file), cursor->data);
        oi = load_im_file_in_oi(file, path);

        if (oi == NULL)
            return win_printf_OK("could not load image\n%s\n"
                                 "from path %s", file, backslash_to_slash(path));

        imr = (imr == NULL) ? find_imr_in_current_dialog(NULL) : imr;

        if (imr == NULL)
        {
            imr = create_and_register_new_image_project(0,   32,  900,  668);
            j = 1;
        }
        else
        {
            j = 0;
        }

        if (imr == NULL)
        {
            win_printf_OK("could not create imreg");
        }

        add_image(imr, oi->im.data_type, (void *)oi);

        if (j == 1)
        {
            remove_from_image(imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
        }

        select_image_of_imreg(imr, imr->n_oi - 1);
        reorder_loaded_image_file_list(file, path);
        extract_file_path(path, sizeof(path), cursor->data);
        reorder_visited_path_list(path);
        cursor = cursor->next;
    }

    flush_config_file();
    free_file_list(file_list);
    return D_REDRAW;
}

int	do_load_movie_on_disk(void)
{
    int j;
    char path[512] = {0};
    char file[256] = {0};
    imreg *imr = NULL;
    O_i *oi = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    movie_buffer_on_disk = 1;
    file_list_t *file_list = open_files_config("Load plot (*.gr)", fui, "Plot Files (*.gr)\0*.gr\0All Files (*.*)\0*.*\0\0",
                             "PLOT-GR-FILE", "last_loaded");
    file_list_t *cursor = file_list;

    while (cursor != NULL)
    {
        set_config_string("IMAGE-GR-FILE", "last_loaded", cursor->data);
        oi = load_im_file_in_oi(cursor->data, path);

        if (oi == NULL)
            return win_printf_OK("could not load image\n%s\n"
                                 "from path %s", file, backslash_to_slash(path));

        imr = (imr == NULL) ? find_imr_in_current_dialog(NULL) : imr;

        if (imr == NULL)
        {
            imr = create_and_register_new_image_project(0,   32,  900,  668);
            j = 1;
        }
        else
        {
            j = 0;
        }

        if (imr == NULL)
        {
            win_printf_OK("could not create imreg");
        }

        add_image(imr, oi->im.data_type, (void *)oi);

        if (j == 1)
        {
            remove_from_image(imr, imr->o_i[0]->im.data_type, (void *)imr->o_i[0]);
        }

        select_image_of_imreg(imr, imr->n_oi - 1);
        reorder_loaded_image_file_list(file, path);
        extract_file_path(path, sizeof(path), cursor->data);
        reorder_visited_path_list(path);
        cursor = cursor->next;
    }

    free_file_list(file_list);
    flush_config_file();
    movie_buffer_on_disk = 0;
    return D_REDRAW;
}

int add_im_in_eps(imreg *imr)
{
    int i, j;
    char line[128] = {0},  cho;
    struct box  *bo = NULL;
    O_i *oi = imr->one_i;
    int x0, y0, xs, ys, nx, ny, c, k, color, color_n;
    int x2 = 0, y2 = 0, x3, y3;
    float sca, z, zg, z1;

    color_n = (oi->im.data_type == IS_RGB_PICTURE) ? 3 : 1;
    bo = &imr->stack;
    x0 = bo->xc;
    y0 = bo->yc;
    bo->xc = 0;
    bo->yc = 0;
    wr_box(bo);
    xs = bo->xc + imr->plt->xc + 3 * 14;
    ys = bo->yc + imr->plt->yc + 3 * 14;
    xs = (xs * 2) / 5;
    ys = (ys * 2) / 5;
    sca = (float)(512 * 28) / 5;
    nx = oi->im.nxe - oi->im.nxs;
    ny = oi->im.nye - oi->im.nys;
    zg = 256;

    if ((oi->z_white - oi->z_black) != 0)
    {
        zg = 256 / (oi->z_white - oi->z_black);
    }

    fprintf(qd_fp, "gsave\n%d %d translate\n", xs, ys);

    if (oi->im.data_type == IS_RGB_PICTURE)
    {
        fprintf(qd_fp, "/scanline %d 3 mul string def\n", nx);
    }
    else
    {
        fprintf(qd_fp, "/imline    %d string def\n", nx);
    }

    fprintf(qd_fp, "%d %d scale\n", (int)(sca * oi->width), (int)(sca * oi->height));
    fprintf(qd_fp, "%d %d 8 [ %d 0 0 %d 0 0]\n", nx, ny, nx, ny);

    if (oi->im.data_type == IS_RGB_PICTURE)
    {
        fprintf(qd_fp, "{ currentfile scanline readhexstring pop } false 3\ncolorimage\n	");
    }
    else
    {
        fprintf(qd_fp, "{currentfile imline readhexstring pop} image\n");
    }

    for (i = oi->im.nys; i < oi->im.nye ; i++)
    {
        for (j = oi->im.nxs, k = 0, color = 0; j < oi->im.nxe ; color++, j = (color >= color_n) ? j + 1 : j)
        {
            color %= color_n;

            if (oi->im.data_type == IS_FLOAT_IMAGE)
            {
                z = oi->im.pixel[i].fl[j];
            }
            else if (oi->im.data_type == IS_COMPLEX_IMAGE)
            {
                if (oi->im.mode == RE)
                {
                    z = oi->im.pixel[i].fl[2 * j];
                }
                else if (oi->im.mode == IM)
                {
                    z = oi->im.pixel[i].fl[2 * j + 1];
                }
                else if (oi->im.mode == AMP)
                {
                    z = oi->im.pixel[i].fl[2 * j];
                    z1 = oi->im.pixel[i].fl[2 * j + 1];
                    z = sqrt(z * z + z1 * z1);
                }
                else if (oi->im.mode == AMP_2)
                {
                    z = oi->im.pixel[i].fl[2 * j];
                    z1 = oi->im.pixel[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                }
                else if (oi->im.mode == LOG_AMP)
                {
                    z = oi->im.pixel[i].fl[2 * j];
                    z1 = oi->im.pixel[i].fl[2 * j + 1];
                    z = z * z + z1 * z1;
                    z = (z > 0) ? log10(z) : -40.0;
                }
                else
                {
                    return (win_printf("Unknown mode for complex image"));
                }
            }
            else if (oi->im.data_type == IS_RGB_PICTURE)
            {
                z = oi->im.pixel[i].ch[3 * j + color];
            }
            else if (oi->im.data_type == IS_INT_IMAGE)
            {
                z = oi->az + oi->dz * oi->im.pixel[i].in[j];
            }
            else if (oi->im.data_type == IS_UINT_IMAGE)
            {
                z = oi->az + oi->dz * oi->im.pixel[i].ui[j];
            }
            else if (oi->im.data_type == IS_LINT_IMAGE)
            {
                z = oi->az + oi->dz * oi->im.pixel[i].li[j];
            }
            else if (oi->im.data_type == IS_CHAR_IMAGE)
            {
                z = oi->az + oi->dz * oi->im.pixel[i].ch[j];
            }
            else
            {
                return (win_printf("Unknown image type"));
            }

            z = (z -  oi->z_black) * zg;
            z = (z > 255) ? 255 : (int)z;
            c = (z < 0) ? 0 : (int)z;
            cho = (c & 0XF0)	>> 4;
            line[(k % 32) << 1] = (cho > 9) ? '7' + cho : '0' + cho;
            cho = (c & 0X0F);
            line[1 + ((k % 32) << 1)] = (cho > 9) ? '7' + cho : '0' + cho;
            k++;

            if (!(k % 32))
            {
                line[64] = 0;
                fprintf(qd_fp, "%s\n", line);
            }
        }

        if ((k % 32) != 0)
        {
            line[2 * (k % 32)] = 0;
            fprintf(qd_fp, "%s\n", line);
        }
    }

    fprintf(qd_fp, "grestore\n");

    if (locate_box(&imr->stack, QKD_B_PLOT, &x2, &x3, &y2, &y3, FIRST) == NULL)
    {
        return D_O_K;
    }

    x3 = imr->stack2.xc;
    y3 = imr->stack2.yc;
    imr->stack2.xc = x2;// was +=
    imr->stack2.yc = y2;// was +=
    wr_box(&imr->stack2);
    imr->stack2.xc = x3;
    imr->stack2.yc = y3;
    bo->xc = x0;
    bo->yc = y0;
    return 0;
}


int	spit_box_and_im_to_ps(imreg *imr, char *file)
{
    struct box *bo = NULL;
    static int col = 1, paysage = 0, autos = 0;
    static float sca = 1;
    int  x0, y0, i;
    float xb1, yb1, xb0, yb0, sc = .15; // tmp;
    O_i *oi = NULL;
    //extern FILE *qd_fp;
    char question[512] = {0};
    
    //int 	ps_init1(void);
    //int 	ps_start_bounding_box_2(char *file, char *infile, char *inpath,
    //                                time_t *timer, float x0, float w, float y0, float h, float sc);
//int 	ps_close(void);
//int ps_color(int color, int mode);
    //	color_enable = 1;
    bo = &imr->stack;
    oi = imr->one_i;
    x0 = bo->xc;
    y0 = bo->yc;
    /*
        if (bo->w > 9600 || (bo->d+bo->h) > 13200)
        {
        sc = (bo->w > 9600) ? ((float)(9600))/bo->w : 1;
        tmp = ((float)(13200))/(bo->d+bo->h);
        sc = (tmp < sc) ? tmp : sc;
        i = win_scanf("Plot bigger than the A4 page! scale it to ? %f",&sc);
        if (i == WIN_CANCEL)	return 0;
        sc *= .15;
        }
        */
    snprintf(question, 512, "Using scale = 1, the image width is %g mm \n"
             "and height %g mm\nChoose your scale %%f"
             "Or select automatic scale %%b\n"
             "Orientation: Portrait %%R Landscape %%r\n"
             "Type of EPS: Black and White %%R Color %%r Gray level%%r\n",
             (25.4 * bo->w) / 1200, (25.4 * (bo->d + bo->h)) / 1200);
    i = win_scanf(question, &sca, &autos, &paysage, &col);

    if (i == WIN_CANCEL)
    {
        return 0;
    }

    if (autos)
    {
        if (paysage)
        {
            sca = ((float)bo->w / 1200) / 11;

            if (((float)(bo->d + bo->h) / 1200) / 8 < sca)
            {
                sca = ((float)(bo->d + bo->h) / 1200) / 8;
            }
        }
        else
        {
            sca = ((float)bo->w / 1200) / 8;

            if (((float)(bo->d + bo->h) / 1200) / 11 < sca)
            {
                sca = ((float)(bo->d + bo->h) / 1200) / 11;
            }
        }

        sc = sca * 0.15;
        xb1 = sc * ((float)(2 * bo->w)) / 5;
        yb1 = sc * ((float)(2 * (bo->d + bo->h))) / 5;
        xb0 = (8 * 72.72 - xb1) / 2;
        yb0 = (11 * 72.72 - yb1) / 2;
    }
    else
    {
        sc = sca * 0.15;
        xb1 = sc * ((float)(2 * bo->w)) / 5;
        yb1 = sc * ((float)(2 * (bo->d + bo->h))) / 5;
        xb0 = (8 * 72.72 - xb1) / 2;
        yb0 = (11 * 72.72 - yb1) / 2;
    }

    win_printf("scale %g", sc);

    if (paysage)
    {
        if (ps_start_bounding_box_2(file, (oi->filename != NULL) ? oi->filename : (char *) strdup("NULL"),
                                    (oi->dir != NULL) ? oi->dir : strdup("NULL"), &(oi->im.time),
                                    (8 * 72.72 - yb1) / 2, yb1 + 5, (11 * 72.72 - xb1) / 2, xb1 + 5, sc) == 1)
        {
	  return win_printf_OK("Error cannot open %s", backslash_to_slash(file));
        }

        fprintf(qd_fp, "%d 0 translate\n90 rotate\n", (int)((yb1 + 5.5) / sc));
    }
    else
    {
        if (ps_start_bounding_box_2(file, (oi->filename != NULL) ? oi->filename : strdup("NULL"),
                                    (oi->dir != NULL) ? oi->dir : strdup("NULL"), &(oi->im.time) , xb0, xb1 + 5, yb0, yb1 + 5, sc) == 1)
        {
	  return win_printf_OK("Error cannot open %s", backslash_to_slash(file));
        }
    }

    ps_init1();
    bo->xc = 0;
    bo->yc = 0;

    if (col == 1)
    {
        ps_color(Black, TRUE_COLOR_WHITE_TO_BLACK);
        color_enable = 1;
    }
    else if (col == 2)
    {
        ps_color(Black, GRAY_WHITE_TO_BLACK);
        color_enable = 1;
    }
    else
    {
        ps_color(Black, BW_WHITE_TO_BLACK);
        color_enable = 0;
    }

    //	ps_color(Black, TRUE_COLOR_WHITE_TO_BLACK);
    add_im_in_eps(imr);
    ps_close();
    eps_file_flag = 0;
    bo->xc = x0;
    bo->yc = y0;
    pc_init1();
    return D_O_K;
}


int	im_to_epsfile(void)
{
    int i;
    char s[256] = {0};
    char c_dir[256] = {0};
    char c[128] = {0};
    char fullfileil[512] = {0};
    int no_file = 1, ins;
    imreg *imr = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    imr = find_imr_in_current_dialog(NULL);

    if (imr == NULL || imr->one_i == NULL)
    {
        win_printf("cannot find image region");
        return D_O_K;
    }

    if (imr->one_i->filename != NULL)
    {
        replace_extension(c, imr->one_i->filename, ".eps", sizeof(c));
    }
    else
    {
        strcpy(c, "untitle.eps");
    }

    if (imr->one_i->dir != NULL)
    {
        strcpy(c_dir, imr->one_i->dir);
    }
    else
    {
        my_getcwd(c_dir, sizeof(c_dir));
    }

    put_path_separator(c_dir, sizeof(c_dir));
    strcpy(fullfileil, c_dir);
    strcat(fullfileil, c);

    while (no_file)
    {
        eps_file_flag = 1;
        color_enable = 1;
        ins = do_select_file(fullfileil, sizeof(fullfileil), "IMAGE-EPS-FILE\0", "*.eps;*.ps", "saving in EPS format", 0);

        //switch_allegro_font(1);
        //ins = file_select_ex("Image to postscript", fullfilei, "eps", 512, 0, 0);
        //switch_allegro_font(0);
        if (ins == 0)
        {
            //extract_file_name(c, sizeof(c), fullfilei);
            //extract_file_path(c_dir, sizeof(c_dir), fullfilei);
            //put_path_separator(c_dir, sizeof(c_dir));

            //sprintf(s, "%s%s", c_dir, c);
            //i =	do_you_want_to_overwrite(s, NULL, NULL);
            //if (i == WIN_CANCEL) return OFF;
            i = spit_box_and_im_to_ps(imr, fullfilei);

            if (i)
            {
                win_printf("Cannot open\n%s !...", backslash_to_slash(s));
            }
            else
            {
                no_file = 0;
            }
        }
        else
        {
            no_file = 0;
        }

        eps_file_flag = 0;
        color_enable = 0;
    }

    return D_O_K;
}




int	do_save_image_bitmap(void)
{
    int i;
    char path[512] = {0}, file[256] = {0}, fullfile[512] = {0};
    const char *pa = NULL;
    imreg *imr = NULL;
    BITMAP *bmp = NULL;
    PALETTE pal;
    DIALOG *di = NULL;

    if (updating_menu_state != 0)
    {
        return D_O_K;
    }

    imr = find_imr_in_current_dialog(NULL);
    di = find_dialog_associated_to_imr(imr, NULL);

    if (imr == NULL)
    {
        win_printf("cannot find imr");
        return D_O_K;
    }

    if (imr->one_i->dir == NULL)
    {
        pa = get_config_string("IMAGE-BMP-FILE", "last_saved", NULL);

        if (pa != NULL)
        {
            extract_file_path(fullfile, 512, pa);
        }
        else
        {
            my_getcwd(fullfile, 512);
        }

# ifdef XV_WIN32
        strcat(fullfile, "\\");
#else
        strcat(fullfile, "/");
#endif
    }
    else
    {
        strcpy(fullfile, imr->one_i->dir);
# ifdef XV_WIN32
        strcat(fullfile, "\\");
#else
        strcat(fullfile, "/");
#endif
    }

    if (imr->one_i->filename == NULL)
    {
        strcat(fullfile, "Untitled.bmp");
    }
    else
    {
        strcat(fullfile, imr->one_i->filename);
    }

    replace_extension(fullfile, fullfile, "bmp", sizeof(fullfile));
    //switch_allegro_font(1);
    //i = file_select_ex("Save image region as a bitmap", fullfile, "bmp;pcx;tga", 512, 0, 0);
    //switch_allegro_font(0);
    i = do_select_file(fullfile, sizeof(fullfile), "IMAGE-BMP_FILE\0", "bmp;pcx;tga", "Exporting image\0", 0);

    if (i == 0)
    {
        set_config_string("IMAGE-BMP-FILE", "last_saved", fullfile);
        extract_file_name(file, sizeof(file), fullfile);
        extract_file_path(path, sizeof(path), fullfile);
# ifdef XV_WIN32
        strcat(path, "\\");
#else
        strcat(path, "/");
#endif

        if (do_you_want_to_overwrite(fullfile, NULL, "Saving image Bitmap") == WIN_OK)
        {
            scare_mouse();
            vsync();
	    for(;screen_acquired;); // we wait for screen_acquired back to 0;     
	    screen_acquired = 1;				    
            acquire_bitmap(screen);
            get_palette(pal);
            bmp = create_sub_bitmap(screen, di->x, di->y, di->w, di->h);
            release_bitmap(screen);
	    screen_acquired = 0;				    
            save_bitmap(fullfile, bmp, pal);
            unscare_mouse();
            destroy_bitmap(bmp);
        }
    }

    broadcast_dialog_message(MSG_DRAW, 0);
    return D_O_K;
}





MENU im_file_load_plot_menu[32] =
{
    { "L&oad Gr in new project\t(Ctrl-L)",	do_load, 	NULL,	0, NULL}, // _in_new_project
    {"Recently Loaded plot",	        NULL,   plot_recently_loaded, 0, NULL},
    {"Import plot" ,  		NULL,	plot_file_import_menu, 	0, NULL}
};




MENU image_file_menu[32] =
{
    { "Load Image\t(Ctrl-I)",        		do_load_im,			NULL,	0, NULL},
    { "Load movie on disk",        		do_load_movie_on_disk,			NULL,	0, NULL},



    { "Recently Loaded ",	                NULL,	                       im_recently_loaded, 0, NULL},
    { "Recently visited ",	                NULL,	                path_recently_visited, 0, NULL},
    { "&Save image\t(Ctrl-S)",           	do_save_image,			NULL,	0, NULL},
    { "Print &Eps\t(Ctrl-E)",			im_to_epsfile,			NULL,	0, NULL},
    { "Dump Bitmap",      			do_save_image_bitmap,		NULL,	0, NULL},


    { "\0",       				NULL,        			NULL,	0, NULL},

    { "L&oad plot in new project\t(Ctrl-L)",	NULL, 	im_file_load_plot_menu,	0, NULL},
    //   { "L&oad Gr in new project\t(Ctrl-L)",	do_load_in_new_project, 	NULL,	0, NULL},
    { "Load &Cgr project.\t(Ctrl-C)",		load_cgr_multi_plots,		NULL,	0, NULL},
    { "\0",       				NULL,        			NULL,	0, NULL},
    {"Import" ,  				NULL,	image_file_import_menu, 	0, NULL},
    {"Export" ,  				NULL,	image_file_export_menu, 	0, NULL},
    { "\0",       				NULL,        			NULL,	0, NULL},
# ifdef BUILDING_DLL
    { "load plugin",				run_module,			NULL,	0, NULL},
    { "Unload plugin",				remove_module,			NULL,	0, NULL},
#if defined(XV_GTK_DIALOG) && !defined(XV_WIN32)
    { "List plugins",				list_plugin,			NULL,	0, NULL},
#endif
# endif
    { "&Quit\t(Ctrl-Q)",				do_quit,	          	NULL,	0, NULL},
    { NULL,                      		NULL,           		NULL,	0, NULL}
};



int	set_file_image_menu_text(int index, char *text)
{
    if (index < 0 || index >= 32)
    {
        return 1;
    }

    image_file_menu[index].text = text;
    return 0;
}

int	set_file_image_menu_proc(int index, int (*proc)(void))
{
    if (index < 0 || index >= 32)
    {
        return 1;
    }

    image_file_menu[index].proc = proc;
    return 0;
}

int	add_file_image_menu_item(char *text, int (*proc)(void), struct MENU *child, int flags, void *dp)
{
    int i;

    for (i = 0; i < 32 && image_file_menu[i].text != NULL; i++);

    if (i >= 30)
    {
        return -1;
    }

    image_file_menu[i].text = text;
    image_file_menu[i].proc = proc;
    image_file_menu[i].child = child;
    image_file_menu[i].flags = flags;
    image_file_menu[i].dp = dp;
    image_file_menu[i + 1].text = NULL;
    return 0;
}
# endif
