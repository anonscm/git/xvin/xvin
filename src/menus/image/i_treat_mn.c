/****************************************************************/
/* i_treat_mn.c							*/
/*								*/
/* operations on the image/treatment menu			*/
/*								*/
/* note: the operations launched by the menu are under the 	*/
/*       subdirectory treat/					*/
/****************************************************************/

#include "i_treat_mn.h"

# include <stdlib.h>
# include <string.h>


#include "allegro.h"
#include "xvin.h"

#include "treat/i_treat_im2plot.h"	// image to plot operations
#include "treat/i_treat_filter.h"	// filtering images, using fftlib32
#include "treat/i_treat_basic.h"	// basic tools for images
#include "treat/i_treat_im2im.h"	// math. tools for images
#include "treat/i_treat_fftw.h"		// operations on images, using fftw3
#include "treat/i_treat_measures.h"		// some measurements on images

#include "fft_filter_lib.h"

int do_z_profile_of_movie(void);
int do_set_pixels_to_value_and_keep_visible_image(void);
int do_im_of_max_z_profile_of_movie(void);
int do_movie_average_along_y_and_in_t(void);
int do_copy_screen_to_image(void);
int  do_convert_16bits_image_to_8bits(void);

MENU image_treat_menu[32] =
{
	{NULL,                          	NULL,       NULL,   0, NULL }
};




int	add_image_treat_menu_item( char* text, int (*proc)(void), struct MENU* child, int flags, void* dp)
{
	int i;
	for (i = 0; i < 32 && image_treat_menu[i].text != NULL; i++);
	if (i >= 30)	return -1;
	image_treat_menu[i].text = text;
	image_treat_menu[i].proc = proc;
	image_treat_menu[i].child = child;
	image_treat_menu[i].flags = flags;
	image_treat_menu[i].dp = dp;
	image_treat_menu[i+1].text = NULL;
	return 0;
}




/************************************************************************************************/
MENU    *image_basic_treat_menu(void)
{   static MENU mn[32] = {0};

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn,"Swap X and Y",			do_im_swap_X_Y,		NULL, 0,	NULL);
    add_item_to_menu(mn,"Upside down",                  do_upside_down_image,NULL,0,NULL);
    add_item_to_menu(mn,"Upside down, right to left",   do_upside_down_image_right_to_left,NULL,0,NULL);
    add_item_to_menu(mn,"Convert float to unsigned 16 bits", do_convert_float_image_to_16bits ,NULL,0,NULL);
    add_item_to_menu(mn,"Convert float to signed 16 bits", do_convert_float_image_to_signed_16bits ,NULL,0,NULL);
    add_item_to_menu(mn,"Convert 16bits to 8bits",  do_convert_16bits_image_to_8bits,NULL,0,NULL);
    add_item_to_menu(mn,"Cut images in small ones",  do_cut_image_in_slice_to_movie,NULL,0,NULL);



    add_item_to_menu(mn,"\0",				NULL,			NULL, 0,	NULL);
    add_item_to_menu(mn,"Crop to visible points",	do_keep_visible_image,	NULL, 0,	NULL);
    add_item_to_menu(mn,"Copy screen to image",	        do_copy_screen_to_image,	NULL, 0,	NULL);
    add_item_to_menu(mn,"Set invisible pixels value",	do_set_pixels_to_value_and_keep_visible_image,	NULL, 0,	NULL);
    add_item_to_menu(mn,"Contact-sheet",                assemble_multiple_small_images_from_movie ,NULL,0,NULL);
    return mn;
}



/************************************************************************************************/
MENU    *image_math_treat_menu(void)
{   static MENU mn[32]  = {0};

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn,"Resample in X",		do_resample_image,	NULL, X_DIRECTION,	NULL);
    add_item_to_menu(mn,"Resample in Y",		do_resample_image,	NULL, Y_DIRECTION,	NULL);
    add_item_to_menu(mn,"\0",				NULL,			NULL, 0,	NULL);
    add_item_to_menu(mn,"Normalize with X, new image",	do_normalize,	  	NULL, 0,	NULL);
    add_item_to_menu(mn,"Normalize with X, old image",	do_normalize_optimized,	NULL, 0,	NULL);
    add_item_to_menu(mn,"\0",				NULL,			NULL, 0,	NULL);
    add_item_to_menu(mn,"Phase multiplication",		do_im_phase_multiplication, NULL, 0,	NULL);
    add_item_to_menu(mn,"Change referential",		do_im_decalage,		NULL, 0,	NULL);
    add_item_to_menu(mn,"\0",				NULL,			NULL, 0,	NULL);
    add_item_to_menu(mn,"Binarize",			do_binarize,		NULL, 0,	NULL);
    add_item_to_menu(mn,"Derivate in X",		do_im_2pts_deriv_in_x,		NULL, 0,	NULL);
    add_item_to_menu(mn,"Derivate in Y",		do_im_2pts_deriv_in_y,		NULL, 0,	NULL);
    add_item_to_menu(mn,"\0",				NULL,			NULL, 0,	NULL);
    add_item_to_menu(mn,"Re( Im )^2",			do_amp,			NULL, DO_AMP_RE,	NULL);
    add_item_to_menu(mn,"Im( Im )^2",			do_amp,			NULL, DO_AMP_IM,	NULL);
    add_item_to_menu(mn,"Squared modulus",		do_amp,			NULL, DO_AMP_AMP_2,	NULL);
    add_item_to_menu(mn,"Modulus",			do_amp,			NULL, DO_AMP_AMP,	NULL);
    add_item_to_menu(mn,"Phase",			do_im_phi,			NULL, 0,	NULL);
    add_item_to_menu(mn,"d phi / dx",			do_im_kx,		NULL, 0,	NULL);
    add_item_to_menu(mn,"d phi / dy",			do_im_ky,		NULL, 0,	NULL);
    add_item_to_menu(mn,"\0",				NULL,			NULL, 0,	NULL);
    add_item_to_menu(mn,"Image of X-spectra (lin)",	do_im_x_spectrum,	NULL, 0,	(char *) "LIN");
    add_item_to_menu(mn,"Image of X-spectra (log)",	do_im_x_spectrum,	NULL, 0,	(char *) "LOG");
    add_item_to_menu(mn,"Image of Y-spectra (lin)",	do_im_y_spectrum,	NULL, 0,	(char *) "LIN");
    add_item_to_menu(mn,"Image of Y-spectra (log)",	do_im_y_spectrum,	NULL, 0,	(char *) "LOG");
    add_item_to_menu(mn,"\0",				NULL,			NULL, 0,	NULL);
    add_item_to_menu(mn,"2d-FFT, Forward",		do_fft_2d,		NULL, FFT2D_FORWARD,	NULL);
    add_item_to_menu(mn,"build + apply 2d filter",	do_build_and_apply_2d_filter,	NULL, 0,	NULL);
    add_item_to_menu(mn,"2d-FFT, Backward",		do_fft_2d,		NULL, FFT2D_BACKWARD,	NULL);

    return mn;
}



/************************************************************************************************/
MENU    *image_2images_treat_menu(void)
{   static MENU mn[32]  = {0};

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn,"Add",				do_opp_on_two_images,	NULL, IM_ADD,		NULL);
    add_item_to_menu(mn,"Substract",			do_opp_on_two_images,	NULL, IM_SUBSTRACT,	NULL);
    add_item_to_menu(mn,"Multiply",			do_opp_on_two_images, 	NULL, IM_MULTIPLY,	NULL);
    add_item_to_menu(mn,"Divide",			do_opp_on_two_images,	NULL, IM_DIVIDE,	NULL);

    return mn;
}



/************************************************************************************************/
MENU    *image_measures_treat_menu(void)
{   static MENU mn[32] = {0};

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn,"mean values",			do_image_cst_measure,	NULL, 0, NULL);

    return mn;
}



/************************************************************************************************/
MENU    *image_to_plot_treat_menu(void)
{   static MENU mn[32]  = {0};

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn,"X-averaged Y-profile\t(ctrl+i)",  do_average_along_x,     NULL, 0, 		NULL);
    add_item_to_menu(mn,"Y-averaged X-profile\t(ctrl+i)",  do_average_along_y,     NULL, 0, 		NULL);
    add_item_to_menu(mn,"Multiple line profiles",	do_im_multi_lines_profile_to_plot,	NULL, 0,		NULL);
    add_item_to_menu(mn,"Multiple row profiles",	do_im_multi_rows_profile_to_plot,	NULL, 0,		NULL);
    add_item_to_menu(mn,"\0",				NULL,			NULL, 0,		NULL);
    add_item_to_menu(mn,"X-averaged Y-spectrum",	do_y_spectrum_average_along_x,NULL, 0, 		NULL);
    add_item_to_menu(mn,"Y-averaged X-spectrum",	do_x_spectrum_average_along_y,NULL, 0, 		NULL);
    add_item_to_menu(mn,"\0",				NULL,			NULL, 0,		NULL);
    add_item_to_menu(mn,"Z profile from movie",		do_z_profile_of_movie,	NULL, 0,		NULL);
    add_item_to_menu(mn,"Follow max position from movie",  do_find_max_frame_value_in_movie,	NULL, 0,		NULL);
    add_item_to_menu(mn,"Move frames from movie following ds", do_move_frame_from_ds_in_movie ,	NULL, 0,		NULL);

    add_item_to_menu(mn,"Flatten level in movie",	 do_flatten_movie_by_histo_on_Z_profile,	NULL, 0,		NULL);




    add_item_to_menu(mn,"\0",				NULL,			NULL, 0,		NULL);
    add_item_to_menu(mn,"histogram of Z",		do_image_histogram,	NULL, 0,		NULL);
    add_item_to_menu(mn,"Y(X)",				do_XY_im_to_plot,	NULL, 0,		NULL);
    add_item_to_menu(mn,"\0",				NULL,			NULL, 0,		NULL);
    add_item_to_menu(mn,"Follow image extremum along Y",		do_followMax_along_y	,	NULL, 0,		NULL);
    add_item_to_menu(mn,"Follow image Zero along Y",		do_followZero_along_y	,	NULL, 0,		NULL);


    return mn;
}


/************************************************************************************************/
MENU    *movie_to_image_treat_menu(void)
{   static MENU mn[32]  = {0};

    if (mn[0].text != NULL) return mn;




    add_item_to_menu(mn,"Averaged partial movie", do_average_partial_movie,     NULL, 0, 		NULL);
     add_item_to_menu(mn,"Cut movie",do_submovie ,     NULL, 0, 		NULL);
    add_item_to_menu(mn,"\0",				NULL,			NULL,	0,	NULL);
    add_item_to_menu(mn,"X-averaged Y-profile",  do_movie_average_along_x,     NULL, 0, 		NULL);
    add_item_to_menu(mn,"Y-averaged X-profile",  do_movie_average_along_y,     NULL, 0, 		NULL);
    add_item_to_menu(mn,"Y-averaged X-profile avg in t",  do_movie_average_along_y_and_in_t,     NULL, 0, 		NULL);
    add_item_to_menu(mn,"Extremum in Z profile", do_im_of_max_z_profile_of_movie ,     NULL, 0, 		NULL);
    add_item_to_menu(mn,"\0",				NULL,			NULL,	0,	NULL);
    add_item_to_menu(mn,"Substract Z profile to movie",  substract_z_profile_from_movie,     NULL, 0, 		NULL);
    add_item_to_menu(mn,"\0",				NULL,			NULL, 0,		NULL);

    return mn;
}


/************************************************************************************************/
MENU    *image_filter_treat_menu(void)
{   static MENU mn[32]  = {0};

    if (mn[0].text != NULL) return mn;

    add_item_to_menu(mn,"Average along Y (FIR filter)",	do_sliding_average_y,	NULL,	0,	NULL);
    add_item_to_menu(mn,"\0",				NULL,			NULL,	0,	NULL);
    add_item_to_menu(mn,"Filter image in X (fftlib32)",	filter_1d,    		NULL,	0, 	(char *) "P_LINE");
    add_item_to_menu(mn,"Filter image in Y (fftlib32)",	filter_1d,		NULL,   0, 	(char *) "P_COL");
    add_item_to_menu(mn,"\0",				NULL,			NULL,	0,	NULL);
    add_item_to_menu(mn,"Filter image in X (fftw)",  	do_filter_image,  	NULL,	REAL_OUTPUT, 	(char *) "P_LINE");
    add_item_to_menu(mn,"Filter image in Y (fftw)",	do_filter_image,	NULL,   REAL_OUTPUT, 	(char *) "P_COL");
    add_item_to_menu(mn,"Filter image in X (fftw) R->C",do_filter_image,  	NULL,	COMPLEX_OUTPUT, (char *) "P_LINE");
    add_item_to_menu(mn,"Filter image in Y (fftw) R->C",do_filter_image,	NULL,   COMPLEX_OUTPUT, (char *) "P_COL");

    return mn;
}
