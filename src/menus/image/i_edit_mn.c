
# include "xvin.h"
# include "allegro.h"
# include "plot_op.h"
# include "gr_file.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "plot_reg_gui.h"
# include "im_mn.h"
# include "imr_reg_gui.h"

# include <stdlib.h>
# include <string.h>
#ifndef XV_WIN32
# include <sys/stat.h>
# else
AL_FUNC(int, file_exists, (AL_CONST char *filename, int attrib, int *aret));
#endif



int set_image_label(void);
int image_show(void);
int	generate_image_plugin_template(void);

int     paste_plot_in_im(void);
int     paste_ds_as_plot_in_im(void);
int     imop_copy(void);
int     paste_image(void);
int     oi_copy(void);
int 	do_check_heap(void);
int     imop_ds_copy(void);
int     do_TrkMax_unit_x(void);
int     do_TrkMax_unit_y(void);
int     do_TrkMax_unit_z(void);
int     do_TrkMax_unit_t(void);
int     set_im_treatement(void);
int     set_im_source(void);

// /!\ Last string argument of a MENU should not be a const string , but an allocated char *, stack overflow
// could append depending on allegro internal implementation  --> const is discarded by explicite
// cast
MENU image_label_menu[32] =
{
	{"Title", 						set_image_label,	NULL,	0,(char *)	"0"},
	{"Bottom title in X",			set_image_label,	NULL,	0,(char *)	"1"},
	{"Top title in X",				set_image_label,  	NULL,	0,(char *)	 "2"},
	{"Right title in Y",			set_image_label,	NULL,	0,(char *)	 "3"},
	{"Left title in Y",				set_image_label,	NULL,	0,(char *)	 "4"},
	{"Edit source", set_im_source,  NULL,   0, (char *)  "3"},
	{"Edit treatement", set_im_treatement,  NULL,   0, (char *)  "3"},


	{ "\0",  	     				NULL,      			NULL,   0, NULL},
	{"Horizontal label",			set_image_label,	NULL,	0,(char *)	"5"},
	{"White label",					set_image_label,	NULL,	0,(char *)	"6"},
	{"label in absolute coordinate",set_image_label,	NULL,	0,(char *)	"7"},
    { "\0",  	     				NULL,      			NULL,   0, NULL},
	{"Vertical label",				set_image_label,	NULL,	0,(char *)	"8"},
	{"Vertical white label",		set_image_label,	NULL,	0,(char *)	"9"},				{NULL,                          NULL,       NULL,   0, NULL }
};

MENU image_unit_menu[32] =
{
	{"Add X unit to movie",			do_TrkMax_unit_x,	NULL,	0,(char *)	"0"},
	{"Add Y unit to movie",			do_TrkMax_unit_y,	NULL,	0,(char *)	"0"},
	{"Add Z unit to movie",			do_TrkMax_unit_z,	NULL,	0,(char *)	"0"},
	{"Add T unit to movie",			do_TrkMax_unit_t,	NULL,	0,(char *)	"0"},
	{NULL,                                  NULL,       NULL,   0, NULL }
};

MENU image_edit_menu[32] =
{


	{"Copy image",		oi_copy,  	NULL,	0,	NULL},
	{"Paste image",		paste_image,  	NULL,	0,	NULL},
	{"Copy plot",		imop_copy,  	NULL,	0,	NULL},
	{"Copy data set",	imop_ds_copy,  	NULL,	0,	NULL},
	{"Paste plot",		paste_plot_in_im,  	NULL,	0,	NULL},
	{"Paste ds as plot",	paste_ds_as_plot_in_im,  	NULL,	0,	NULL},

	{"Image handling", 			NULL,	oi_menu,	0,(char *)	"0"},
	{"Plot in image",			NULL,	imop_menu,	0,(char *)	"1"},
	{"Labelling",				NULL,  	image_label_menu,	0,(char *)	"2"},
	{"Units",				NULL,  	image_unit_menu,	0,(char *)	"2"},
	{"plugin template",			generate_image_plugin_template,  	NULL,	0,	NULL},
#ifdef XV_WIN32
	{ "Check memory.",		do_check_heap,	NULL,	0,		NULL},
# endif
	{NULL,                      NULL,       NULL,   0, NULL }
};


int	add_image_edit_menu_item( char* text, int (*proc)(void), struct MENU* child, int flags, void* dp)
{
	register int i;
	for (i = 0; i < 32 && image_edit_menu[i].text != NULL; i++);
	if (i >= 30)	return -1;
	image_edit_menu[i].text = text;
	image_edit_menu[i].proc = proc;
	image_edit_menu[i].child = child;
	image_edit_menu[i].flags = flags;
	image_edit_menu[i].dp = dp;
	image_edit_menu[i+1].text = NULL;
	return 0;
}

int do_TrkMax_unit_x(void)
{
  O_i *ois = NULL;
  imreg *imr = NULL;

  if(updating_menu_state != 0)    return D_O_K;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
  mn_index = X_AXIS;
  add_new_units();
  return 0;
}



int do_TrkMax_unit_y(void)
{
  O_i *ois = NULL;
  imreg *imr = NULL;

  if(updating_menu_state != 0)    return D_O_K;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
  mn_index = Y_AXIS;
  add_new_units();
  return 0;
}


int do_TrkMax_unit_z(void)
{
  O_i *ois = NULL;
  imreg *imr = NULL;
  int nfi;


  if(updating_menu_state != 0)    return D_O_K;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  mn_index = Z_AXIS;
  add_new_units();
  win_printf("T unit set added\n# of T unit set %d current %d\n"
	     "Offset %g increment %g\n"
             "name %s\n", ois->n_zu,ois->c_zu
	     ,ois->zu[ois->c_zu]->ax
	     ,ois->zu[ois->c_zu]->dx
	     ,((ois->zu[ois->c_zu]->name)?ois->zu[ois->c_zu]->name:""));


//ois->c_zu = ois->n_zu -1;
  return 0;
}


int do_TrkMax_unit_t(void)
{
  O_i *ois = NULL;
  imreg *imr = NULL;
  int nfi;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) < 1)	return OFF;
  nfi = abs(ois->im.n_f);
  nfi = (nfi == 0) ? 1 : nfi;
  if(updating_menu_state != 0)
    {
      if (nfi <= 1) 	 active_menu->flags |=  D_DISABLED;
      else               active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }
  mn_index = T_AXIS;
  add_new_units();

  win_printf("T unit set added\n# of T unit set %d current %d\n"
	     "Offset %g increment %g\n"
             "name %s\n", ois->n_tu,ois->c_tu
	     ,ois->tu[ois->c_tu]->ax
	     ,ois->tu[ois->c_tu]->dx
	     ,((ois->tu[ois->c_tu]->name)?ois->tu[ois->c_tu]->name:""));


//ois->c_zu = ois->n_zu -1;
  return 0;
}



int oi_copy(void)
{
    imreg *imr = NULL;
    O_i* oi = NULL;

    if(updating_menu_state != 0)    return D_O_K;

    imr = find_imr_in_current_dialog(NULL);
    if (imr == NULL)    return win_printf_OK("image not found");
    oi = imr->one_i;
    if (oi == NULL)   return D_O_K;

    if (oi_copied != NULL)
      {
        free_one_image(oi_copied);
        oi_copied = NULL;
      }
    if ((oi_copied = duplicate_image(oi, NULL)) == NULL)         return D_O_K;
    return D_O_K;
}

int     paste_image(void)
{
	imreg *imr = NULL;
	O_i* oi = NULL;
	char buf[1024] = {0};

	if(updating_menu_state != 0)
	  {
	    if (oi_copied == NULL)   active_menu->flags |=  D_DISABLED;
	    else active_menu->flags &= ~D_DISABLED;
	    return D_O_K;
	  }

	imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL || oi_copied == NULL)	return win_printf_OK("image not found");

	oi = duplicate_image(oi_copied, NULL);
	if (oi == NULL) 	return D_O_K;
	add_image(imr, oi->im.data_type, (void*)oi);
	imr->cur_oi = imr->n_oi-1;
	imr->one_i = imr->o_i[imr->cur_oi];
	//pr->stack = &(pr->one_p->bplt);
	imr->one_i->need_to_refresh = ALL_NEED_REFRESH;;
	snprintf(buf,1024,"%s %s",oi->filename, oi->dir);
//	if (os_type == OSTYPE_WIN2000 || os_type == OSTYPE_WINXP)
	my_set_window_title("%s",buf);
	return (refresh_image(imr, imr->n_oi-1));
}


int imop_copy(void)
{
    imreg *imr = NULL;
    O_p* op = NULL;

    imr = find_imr_in_current_dialog(NULL);
    op = find_im_cur_op(imr);

    if(updating_menu_state != 0)
      {
	if (op == NULL)   active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }
    if (op == NULL)   return D_O_K;

    if (op_copied != NULL)
      {
        free_one_plot(op_copied);
        op_copied = NULL;
      }
    if ((op_copied = duplicate_plot_data(op, NULL)) == NULL)
        return D_O_K;
    return D_O_K;
}


int imop_ds_copy(void)
{
    imreg *imr = NULL;
    O_p* op = NULL;
    d_s *ds = NULL;
    int i, i_ds;

    imr = find_imr_in_current_dialog(NULL);
    op = find_im_cur_op(imr);

    if(updating_menu_state != 0)
      {
	if (op == NULL)   active_menu->flags |=  D_DISABLED;
	else active_menu->flags &= ~D_DISABLED;
	return D_O_K;
      }
    if (op == NULL)   return D_O_K;
    if (op->n_dat < 2) ds = op->dat[0];
    else
      {
	i_ds = op->cur_dat;
	i = win_scanf("Which data set do you want to copy ?\n%10d\n",&i_ds);
	if (i == WIN_CANCEL) return D_O_K;
	if (i_ds < 0 || i_ds >= op->n_dat)
	  return win_printf_OK("Data set %d out of range !",i_ds);
	ds = op->dat[i_ds];
      }

    if (opds_copied != NULL)
      {
        free_one_plot(opds_copied);
        opds_copied = NULL;
        ds_copied = NULL;
      }
    i = op->n_dat;    op->n_dat = 0;
    if ((opds_copied = duplicate_plot_data(op, NULL)) == NULL)
        return D_O_K;
    ds_copied = duplicate_data_set(ds, NULL);
    if (ds_copied == NULL)     return D_O_K;
    if (add_one_plot_data(opds_copied, IS_DATA_SET, (void*)ds_copied))
        return D_O_K;
    op->n_dat = i;
    remove_all_keyboard_proc();
    return D_O_K;
}




int     paste_plot_in_im(void)
{
	imreg *imr = NULL;
	O_p* op = NULL;

	if(updating_menu_state != 0)
	  {
	    if (op_copied == NULL)   active_menu->flags |=  D_DISABLED;
	    else active_menu->flags &= ~D_DISABLED;
	    return D_O_K;
	  }

	imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL || op_copied == NULL)	return win_printf_OK("data not found");

	op = duplicate_plot_data(op_copied, NULL);
	if (op == NULL) 	return D_O_K;
	add_one_image (imr->one_i, IS_ONE_PLOT, (void *)op);
	return (refresh_image(imr, UNCHANGED));
}

int     paste_ds_as_plot_in_im(void)
{
	imreg *imr = NULL;
	O_p* op = NULL;

	if(updating_menu_state != 0)
	  {
	    if (opds_copied == NULL)   active_menu->flags |=  D_DISABLED;
	    else active_menu->flags &= ~D_DISABLED;
	    return D_O_K;
	  }

	imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL || opds_copied == NULL)	return win_printf_OK("plot not found");

	op = duplicate_plot_data(opds_copied, NULL);
	if (op == NULL) 	return D_O_K;
	add_one_image (imr->one_i, IS_ONE_PLOT, (void *)op);
	return (refresh_image(imr, UNCHANGED));
}


int set_image_label(void)
{
	register int i;
	char *s = NULL, **lbl = NULL;
	imreg *imr = NULL;
	O_i *oi = NULL;
	p_l *dp = NULL;
	int index;

	if(updating_menu_state != 0)	return D_O_K;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return D_O_K;
	if (active_menu->dp == NULL)	return D_O_K;

	i = sscanf((char*)active_menu->dp,"%d",&index);
	if (i != 1)	return D_O_K;

	if ( index == 0)	lbl = &oi->title;
	else	if ( index == 1)	lbl = &oi->x_title;
	else	if ( index == 2)	lbl = &oi->x_prime_title;
	else	if ( index == 3)	lbl = &oi->y_prime_title;
	else	if ( index == 4)	lbl = &oi->y_title;
	if (lbl != NULL && *lbl != NULL)
	{
		TeX_modify (*lbl);
		if(*lbl != NULL)  free(*lbl);
		*lbl = NULL;
	}
	else				TeX_modify (NULL);
	if (last_answer == NULL || strlen(last_answer) == 0)	s = NULL;
	else 				s = strdup(last_answer);


	if ( index == 0)	oi->title = s;
	if ( index == 1)	oi->x_title = s;
	if ( index == 2)	oi->x_prime_title = s;
	if ( index == 3)	oi->y_prime_title = s;
	if ( index == 4)	oi->y_title = s;
	if ( index == 5 || index == 6 || index == 7 || index == 8 || index == 9)
	{
		dp = (p_l*)calloc(1,sizeof(p_l));
		if ( dp == NULL )		return D_O_K;
		dp->text = s;
		dp->xla = (oi->im.nxe + oi->im.nxs) /2;
		dp->yla = (oi->im.nye + oi->im.nys) /2;
		if (index == 5)			dp->type = USR_COORD;
		else if (index == 6)	dp->type = USR_COORD + WHITE_LABEL;
		else if (index == 7)	dp->type = ABS_COORD;
		else if (index == 8)	dp->type = VERT_LABEL_USR;
		else if (index == 9)	dp->type = VERT_LABEL_USR + WHITE_LABEL;
		add_to_one_image(oi,IS_PLOT_LABEL,(void*)dp);
	}
	oi->need_to_refresh |= PLOTS_NEED_REFRESH;
	return (refresh_image(imr, UNCHANGED));
}



int set_im_source(void)
{
    char **lbl = NULL;
    O_i *oi = NULL;
    imreg *imr = NULL;

    if (updating_menu_state != 0)    return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return D_O_K;

    lbl = &(oi->im.source);

    if (lbl != NULL && *lbl != NULL)
    {
        TeX_modify(*lbl);

        if (*lbl != NULL) free(*lbl);

        *lbl = NULL;
    }
    else        TeX_modify(NULL);

    if (last_answer != NULL && strlen(last_answer) > 0)
      set_oi_source(oi,"%s", last_answer);

    oi->need_to_refresh |= PLOTS_NEED_REFRESH;
    return (refresh_image(imr, UNCHANGED));
}

int set_im_treatement(void)
{
    char **lbl = NULL;
    O_i *oi = NULL;
    imreg *imr = NULL;


    if (updating_menu_state != 0)    return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return D_O_K;

    lbl = &(oi->im.treatement);
    if (lbl != NULL && *lbl != NULL)
    {
        TeX_modify(*lbl);

        if (*lbl != NULL) free(*lbl);

        *lbl = NULL;
    }
    else        TeX_modify(NULL);

    if (last_answer != NULL && strlen(last_answer) > 0)
      update_oi_treatment(oi,"%s",last_answer);
    oi->need_to_refresh |= PLOTS_NEED_REFRESH;
    return (refresh_image(imr, UNCHANGED));
}


int image_show(void)
{
  char s[128] = {0},  sp[128] = {0}, mv[128] = {0}, user[2048] = {0};
    imreg *imr = NULL;
    O_i *oi = NULL;
    //	O_p *op = NULL;
    //	char current_op[32];
    time_t timer;
    float size = 0;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return D_O_K;

    size = fabs(oi->im.m_f);
    if (size == 0) size = 1;
    size *= oi->im.nx*oi->im.ny;

    if (oi->im.user_ispare[oi->im.c_f][0] != 0 || oi->im.user_ispare[oi->im.c_f][1] != 0
	|| oi->im.user_ispare[oi->im.c_f][2] != 0 || oi->im.user_ispare[oi->im.c_f][3] != 0
	|| oi->im.user_fspare[oi->im.c_f][0] != 0 || oi->im.user_fspare[oi->im.c_f][1] != 0
	|| oi->im.user_fspare[oi->im.c_f][2] != 0 || oi->im.user_fspare[oi->im.c_f][3] != 0)
      {
	snprintf(user,sizeof(user),"ispare [0] = %d; [1] = %d; [2] = %d; [3] = %d\n"
		 "fspare [0] = %g; [1] = %g; [2] = %g; [3] = %g\n"
		 ,oi->im.user_ispare[oi->im.c_f][0],oi->im.user_ispare[oi->im.c_f][1]
		 ,oi->im.user_ispare[oi->im.c_f][2],oi->im.user_ispare[oi->im.c_f][3]
		 ,oi->im.user_fspare[oi->im.c_f][0],oi->im.user_fspare[oi->im.c_f][1]
		 ,oi->im.user_fspare[oi->im.c_f][2],oi->im.user_fspare[oi->im.c_f][3]);

      }


    if (oi->im.n_f > 1)
      snprintf(mv,128,"movie %d/%d images (%d allocated)",oi->im.c_f,oi->im.n_f,oi->im.m_f);
    else if (oi->im.n_f < 0)
      snprintf(mv,128,"disk movie %d/%d images (%d allocated)",oi->im.c_f,-oi->im.n_f,oi->im.m_f);
    else	snprintf(mv,128,"Single image");

    if (oi->im.data_type == IS_CHAR_IMAGE)
      {
	snprintf(s,128,"%s of unsigned char",mv);
	size *= 1;
      }
    else if (oi->im.data_type == IS_INT_IMAGE)
      {
	snprintf(s,128,"%s of short int",mv);
	size *= 2;
      }
    else if (oi->im.data_type == IS_UINT_IMAGE)
      {
	snprintf(s,128,"%s of unsigned short int",mv);
	size *= 2;
      }
    else if (oi->im.data_type == IS_LINT_IMAGE)
      {
	snprintf(s,128,"%s of int (32 bits)",mv);
	size *= 4;
      }
    else if (oi->im.data_type == IS_FLOAT_IMAGE)
      {
	snprintf(s,128,"%s of float",mv);
	size *= 4;
      }
    else if (oi->im.data_type == IS_DOUBLE_IMAGE)
      {
	snprintf(s,128,"%s of double",mv);
	size *= 8;
      }
    else if (oi->im.data_type == IS_RGB_PICTURE)
      {
	snprintf(s,128,"%s a RGB picture",mv);
	size *= 3;
      }
    else if (oi->im.data_type == IS_RGBA_PICTURE)
      {
	snprintf(s,128,"%s a RGBA picture",mv);
	size *= 4;
      }
    else if (oi->im.data_type == IS_RGB16_PICTURE)
      {
	snprintf(s,128,"%s a RGB16 picture",mv);
	size *= 6;
      }
    else if (oi->im.data_type == IS_RGBA16_PICTURE)
      {
	snprintf(s,128,"%s a RGBA16 picture",mv);
	size *= 8;
      }
    else if (oi->im.data_type == IS_COMPLEX_IMAGE)
      {
	size *= 8;
	if (oi->im.mode == LOG_AMP)
	  snprintf(s,128,"%s of complex, z = log[A^2]",mv);
	else if (oi->im.mode == AMP)
	  snprintf(s,128,"%s of complex, z = A",mv);
	else if (oi->im.mode == AMP_2)
	  snprintf(s,128,"%s of complex, z = A^2",mv);
	else if (oi->im.mode == RE)
	  snprintf(s,128,"%s of complex, z = Re",mv);
	else if (oi->im.mode == IM)
	  snprintf(s,128,"%s of complex, z = Im",mv);
	else	snprintf(s,128,"%s of complex, z = Unknown !",mv);
	}
    else if (oi->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
      {
	size *= 16;
	if (oi->im.mode == LOG_AMP)
	  snprintf(s,128,"%s of complex double, z = log[A^2]",mv);
	else if (oi->im.mode == AMP)
	  snprintf(s,128,"%s of complex double, z = A",mv);
	else if (oi->im.mode == AMP_2)
	  snprintf(s,128,"%s of complex double, z = A^2",mv);
	else if (oi->im.mode == RE)
	  snprintf(s,128,"%s of complex double, z = Re",mv);
	else if (oi->im.mode == IM)
	  snprintf(s,128,"%s of complex double, z = Im",mv);
	else	snprintf(s,128,"%s of complex double, z = Unknown !",mv);
      }
    else return (win_printf ("Unknown image type"));
    if ( oi->im.win_flag & X_PER && oi->im.win_flag & Y_PER)
      snprintf(sp,128,"periodic in X and Y");
    else if ( oi->im.win_flag & X_PER )
      snprintf(sp,128,"periodic in X but not in Y");
    else if ( oi->im.win_flag & Y_PER )
      snprintf(sp,128,"periodic in Y but not in X");
    else	snprintf(sp,128,"not periodic in X, not periodic in Y");
    if (oi->im.time == 0) 	time(&timer);
    else 			timer = oi->im.time;

    win_printf("{\\color{yellow}Image %d} -> type %s\n"
	       "nx %d ny %d\n%g MegaBytes\n%s\n{\\color{yellow}source }%s\n"
	       "\n{\\color{yellow}treatement} %s\n"
	       "\n{\\color{yellow}history} %s\n{\\color{yellow}Filename} %s\n"
	       "\n{\\color{yellow}path} %s\n Time %s"
	       "\n%s"
	       ,imr->cur_oi,s,oi->im.nx,oi->im.ny,size/(1024*1024),sp,
	       (oi->im.source != NULL) ? backslash_to_slash(oi->im.source) : "no source",
	       (oi->im.treatement != NULL) ? backslash_to_slash(oi->im.treatement)
	       : "no treatement",
	       (oi->im.history != NULL) ? backslash_to_slash(oi->im.history) :
	       "no history",
	       (oi->filename != NULL) ? oi->filename : "Untitled.gr",
	       (oi->dir != NULL) ? backslash_to_slash(oi->dir) : "Not define"
	       ,ctime(&timer),user);
    return D_O_K;
}

int	generate_image_plugin_template(void)
{
    register int i;
    char app_name[256] = {0}, path[256] = {0}, filename[512] = {0}, *s = NULL, APP_NAME[256] = {0};
    FILE *fp = NULL;
    int aret;

	if(updating_menu_state != 0)	return D_O_K;

	app_name[0] = 0;
	i = win_scanf("I am going to prepare the set of files necessary\n"
		"to write a basic starting template providing a minimum\n"
		"add on program attaching a new treatement button to the\n"
		"image menu and giving the canva for a image treatement\n"
		"function. Give the name of the plugin\n"
		"(avoid space, punctuation ...  characters) %s",
		app_name);
	if (i == WIN_CANCEL)	return D_O_K;
	strncpy(APP_NAME,app_name,255);
	strupr(APP_NAME);
	strncpy(path,prog_dir,255);

	backslash_to_slash(path);
	s = strstr(path,"/bin");
	if (s == NULL)	s = strstr(path,"/BIN");
	if (s == NULL)
	{
		win_printf("I cannot retrieve the path of Xvin\n");
		return D_O_K;
	}
	s[0] = 0;
	slash_to_backslash(path);
	if (file_exists(path,FA_DIREC,&aret) == 0)
	{
		win_printf("I cannot find Xvin path!\n%s",backslash_to_slash(path));
		return D_O_K;
	}

	put_backslash(path);
	strcat (path,"plug-ins-src");
	if (file_exists(path,FA_DIREC,&aret) == 0)
	{
#ifdef XV_WIN32
		if (mkdir(path) != 0)
#else
		if (mkdir(path, 0777) != 0)
#endif
		{
			win_printf("I cannot create path!\n%s",backslash_to_slash(path));
			return D_O_K;
		}
	}
	if (file_exists(path,FA_DIREC,&aret) == 0)
	{
		win_printf("I cannot find plug-in path!\n%s",backslash_to_slash(path));
		return D_O_K;
	}
	put_backslash(path);
	strcat (path,app_name);
	slash_to_backslash(path);
	if (file_exists(path,FA_DIREC,&aret) == 0)
	{
#ifdef XV_WIN32
		if (mkdir(path) != 0)
#else
		if (mkdir(path, 0777) != 0)
#endif
		{
			win_printf("I cannot create path!\n%s",backslash_to_slash(path));
			return D_O_K;
		}
	}
	else
	{
		win_printf("path %s already exists! presuably %s plug-ins already exist\n"
			,backslash_to_slash(path),app_name);
		return D_O_K;

	}
	put_backslash(path);
	strcpy(filename,path);
	strcat(filename,"Makefile");

	fp = fopen(filename,"w");
	if (fp == NULL)
	{
		win_printf("I cannot create file!\n%s",backslash_to_slash(filename));
		return D_O_K;
	}
	//fprintf(fp,"CFLAGS = -c  -Wall -O2 -march=pentium -mdll -DXV_WIN32  -I../../include \\\n"
	//	" -I../../include/xvin  -I../../include/plug-ins \\\n"
	//	"-I\"c:\\Program Files\\GnuWin32\\include\"\n\n");





	fprintf(fp,"# name of the plug'in\n");
	fprintf(fp,"NAME	    		= %s\n",app_name);
	fprintf(fp,"# specific plugin flags\n");
	fprintf(fp,"LCFLAGS 		= \n");
	fprintf(fp,"# extra file to zip\n");
	fprintf(fp,"ZIPALSO			= \n");
	fprintf(fp,"# name of additional source files (.c and .h are already assumed)\n");
	fprintf(fp,"OTHER_FILES 		= \n");
	fprintf(fp,"# name of libraries this plug'in depends on\n");
	fprintf(fp,"LIB_DEPS    		= \n");
	fprintf(fp,"# name of the dll to convert in lib.a     \n");
	fprintf(fp,"OTHER_LIB_FROM_DLL 	=  	\n");
	fprintf(fp,"XVIN_DIR    		=../../\n");
	fprintf(fp,"##################################################################################################\n");
	fprintf(fp,"include $(XVIN_DIR)platform.mk		# platform specific definitions\n");
	fprintf(fp,"include $(XVIN_DIR)plugin.mk 		# plugin construction rules\n");

	/*


	fprintf(fp,"include ../../platform.mk\n\n");
	fprintf(fp,"all : ../../bin/%s.dll\n\n",app_name);
	fprintf(fp,"clean :\n\trm ../../bin/%s.dll %s.o ../../lib/lib%s.a \n\n",
		app_name,app_name,app_name);
	fprintf(fp,"zip	: %s.c %s.h makefile\n"
	"\tcd ../..; $(ZIPEXE) a -tzip plug-ins-src/%s/%s.zip \\\n"
	"\tplug-ins-src/%s/%s.c plug-ins-src/%s/%s.h \\\n"
	"\tplug-ins-src/%s/makefile ; cd plug-ins-src/%s \n\n",
	app_name,app_name,app_name,app_name,app_name,app_name,app_name,
	app_name,app_name,app_name);

	fprintf(fp,"../../bin/%s.dll : %s.o\n"
	"\tgcc  -o ../../bin/%s.dll -mwindows -shared $(PLUGINSLIB) %s.o  \\\n"
		"-lalleg -lxvin  -Wl,--export-all-symbols  -Wl,--enable-auto-import \\\n"
	"-Wl,--out-implib=../../lib/lib%s.a \n",app_name,app_name,app_name,app_name,app_name);
	fprintf(fp,"\n\n%s.o : %s.c %s.h\n\tgcc $(PCFLAGS) -o %s.o %s.c\n",
		app_name,app_name,app_name,app_name,app_name);
	*/
	fclose(fp);

	strcpy(filename,path);
	strcat(filename,app_name);
	strcat(filename,".c");
	fp = fopen(filename,"w");
	if (fp == NULL)
	{
		win_printf("I cannot create file!\n%s",backslash_to_slash(filename));
		return D_O_K;
	}
	fprintf(fp,"/*\n*    Plug-in program for image treatement in Xvin.\n"
			" *\n *    V. Croquette\n  */\n");
	fprintf(fp,"#ifndef _%s_C_\n#define _%s_C_\n\n",APP_NAME,APP_NAME);

	fprintf(fp,"# include \"allegro.h\"\n"
	"# include \"xvin.h\"\n\n"
	"/* If you include other regular header do it here*/ "
	"\n\n\n/* But not below this define */\n"
		"# define BUILDING_PLUGINS_DLL\n");


	fprintf(fp,"//place here all headers from plugins already compiled\n\n\n"
		"#undef _PXV_DLL\n"
		"#define _PXV_DLL   __declspec(dllexport)\n"
		"# include \"%s.h\"\n"
		"//place here other headers of this plugin \n",app_name);



	fprintf(fp,"\nint do_%s_average_along_y(void)\n{\n"
		"\tregister int i, j;\n\tint l_s, l_e;\n"
		"\tO_i *ois = NULL;\n\tO_p *op = NULL;\n"
		"\td_s *ds = NULL;\n\timreg *imr = NULL;\n\n",app_name);

	fprintf(fp,"\n\tif(updating_menu_state != 0)	return D_O_K;\n\n");

	fprintf(fp,"\tif (key[KEY_LSHIFT])\n"
		"\t{\n"
		"\t\treturn win_printf_OK(\"This routine average the image intensity\"\n"
		"\t\t\t\"of several lines of an image\");\n"
		"\t}\n");


	fprintf(fp,"\tif (ac_grep(cur_ac_reg,\"%%im%%oi\",&imr,&ois) != 2)\n"
		"\treturn D_O_K;\n"
		"\t/* we grap the data that we need, the image and the screen region*/\n"
		"\tl_s = 0; l_e = ois->im.ny;\n"
		"\t/* we ask for the limit of averaging*/\n"
		"\ti = win_scanf(\"Average Between lines%%d and %%d\",&l_s,&l_e);\n"
		"\tif (i == WIN_CANCEL)	return D_O_K;\n"
		"\t/* we create a plot to hold the profile*/\n"
		"\top = create_one_plot(ois->im.nx,ois->im.nx,0);\n"
		"\tif (op == NULL) return (win_printf_OK(\"cant create plot!\"));\n");

	fprintf(fp,"\tds = op->dat[0]; /* we grab the data set */\n"
		"\top->y_lo = ois->z_black;	op->y_hi = ois->z_white;\n"
		"\top->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;\n"
		"\tinherit_from_im_to_ds(ds, ois);\n"
		"\top->filename = Transfer_filename(ois->filename);\n"
		"\tset_plot_title(op,\"%s averaged profile from line "
		"%%d to %%d\",l_s, l_e);\n",app_name);

	fprintf(fp,"\tset_formated_string(&ds->treatement,\"%%s "
	"averaged profile from line %%d to %%d\",l_s, l_e);\n"
	"\tuns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);\n"
	"\tuns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);\n"
	"\tfor (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;\n"
	"\tfor (i=l_s ; i< l_e ; i++)\n"
	"\t{\n"
		"\t\textract_raw_line(ois, i, ds->xd);\n"
		"\t\tfor (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];\n"
	"\t}\n");

	fprintf(fp,"\tfor (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)\n"
	"\t{\n"
		"\t\tds->xd[i] = i;\n"
		"\t\tds->yd[i] /= j;\n"
	"\t}\n"
	"\tadd_one_image (ois, IS_ONE_PLOT, (void *)op);\n"
	"\tois->cur_op = ois->n_op - 1;\n"
	"\tbroadcast_dialog_message(MSG_DRAW,0);\n"
	"\treturn D_REDRAWME;\n"
	"}\n");



	fprintf(fp,"\nO_i	*%s_image_multiply_by_a_scalar(O_i *ois, float factor)\n"
	"{\n"
	"\tregister int i, j;\n"
	"\tO_i *oid = NULL;\n\tfloat tmp;\n"
	"\tint onx, ony, data_type;\n"
	"\tunion pix *ps = NULL, *pd = NULL;\n\n",app_name);


	fprintf(fp,"\tonx = ois->im.nx;\tony = ois->im.ny;\tdata_type = ois->im.data_type;\n"
	"\toid =  create_one_image(onx, ony, data_type);\n"
	"\tif (oid == NULL)\n"
	"\t{\n"
		"\t\twin_printf(\"Can't create dest image\");\n"
		"\t\treturn NULL;\n"
	"\t}\n");

	fprintf(fp,"\tif (data_type == IS_CHAR_IMAGE)\n"
	"\t{\n"
		"\t\tfor (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)\n"
		"\t\t{\n"
			"\t\t\tfor (j=0; j< onx; j++)\n"
			"\t\t\t{\n"
				"\t\t\t\ttmp = factor * ps[i].ch[j];\n"
				"\t\t\t\tpd[i].ch[j] = (tmp < 0) ? 0 :\n"
					"\t\t\t\t\t((tmp > 255) ? 255 : (unsigned char)tmp);\n"
			"\t\t\t}\n"
		"\t\t}\n"
	"\t}\n"
	"\telse if (data_type == IS_INT_IMAGE)\n"
	"\t{\n"
		"\t\tfor (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)\n"
		"\t\t{\n"
			"\t\t\tfor (j=0; j< onx; j++)\n"
			"\t\t\t{\n"
				"\t\t\t\ttmp = factor * ps[i].in[j];\n"
				"\t\t\t\tpd[i].in[j] = (tmp < -32768) ? -32768 :\n"
					"\t\t\t\t\t((tmp > 32767) ? 32767 : (short int)tmp);\n"
			"\t\t\t}\n"
		"\t\t}\n"
	"\t}\n"
	"\telse if (data_type == IS_UINT_IMAGE)\n"
	"\t{\n"
		"\t\tfor (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)\n"
		"\t\t{\n"
			"\t\t\tfor (j=0; j< onx; j++)\n"
			"\t\t\t{\n"
				"\t\t\t\ttmp = factor * ps[i].ui[j];\n"
				"\t\t\t\tpd[i].ui[j] = (tmp < 0) ? 0 :\n"
					"\t\t\t\t\t((tmp > 65535) ? 65535 : (unsigned short int)tmp);\n"
			"\t\t\t}\n"
		"\t\t}\n"
	"\t}\n"
	"\telse if (data_type == IS_LINT_IMAGE)\n"
	"\t{\n"
		"\t\tfor (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)\n"
		"\t\t{\n"
			"\t\t\tfor (j=0; j< onx; j++)\n"
			"\t\t\t{\n"
				"\t\t\t\ttmp = factor * ps[i].li[j];\n"
				"\t\t\t\tpd[i].li[j] = (tmp < 0x10000000) ? 0x10000000 :\n"
					"\t\t\t\t\t((tmp > 0x7FFFFFFF) ? 0x7FFFFFFF : (int)tmp);\n"
			"\t\t\t}\n"
		"\t\t}\n"
	"\t}\n"
	"\telse if (data_type == IS_FLOAT_IMAGE)\n"
	"\t{\n"
		"\t\tfor (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)\n"
		"\t\t{\n"
			"\t\t\tfor (j=0; j< onx; j++)\n"
			"\t\t\t{\n"
				"\t\t\t\tpd[i].fl[j] = factor * ps[i].fl[j];\n"
			"\t\t\t}\n"
		"\t\t}\n"
	"\t}\n"
	"\telse if (data_type == IS_DOUBLE_IMAGE)\n"
	"\t{\n"
		"\t\tfor (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)\n"
		"\t\t{\n"
			"\t\t\tfor (j=0; j< onx; j++)\n"
			"\t\t\t{\n"
				"\t\t\t\tpd[i].db[j] = factor * ps[i].db[j];\n"
			"\t\t\t}\n"
		"\t\t}\n"
	"\t}\n"
	"\telse if (data_type == IS_COMPLEX_IMAGE)\n"
	"\t{\n"
		"\t\tfor (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)\n"
		"\t\t{\n"
			"\t\t\tfor (j=0; j< onx; j++)\n"
			"\t\t\t{\n"
				"\t\t\t\tpd[i].fl[2*j] = factor * ps[i].fl[2*j];\n"
				"\t\t\t\tpd[i].fl[2*j+1] = factor * ps[i].fl[2*j+1];\n"
			"\t\t\t}\n"
		"\t\t}\n"
	"\t}\n"
	"\telse if (data_type == IS_COMPLEX_DOUBLE_IMAGE)\n"
	"\t{\n"
		"\t\tfor (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)\n"
		"\t\t{\n"
			"\t\t\tfor (j=0; j< onx; j++)\n"
			"\t\t\t{\n"
				"\t\t\t\tpd[i].db[2*j] = factor * ps[i].db[2*j];\n"
				"\t\t\t\tpd[i].db[2*j+1] = factor * ps[i].db[2*j+1];\n"
			"\t\t\t}\n"
		"\t\t}\n"
	"\t}\n"
	"\telse if (data_type == IS_RGB_PICTURE)\n"
	"\t{\n"
		"\t\tfor (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)\n"
		"\t\t{\n"
			"\t\t\tfor (j=0; j< 3*onx; j++)\n"
			"\t\t\t{\n"
				"\t\t\t\ttmp = factor * ps[i].ch[j];\n"
				"\t\t\t\tpd[i].ch[j] = (tmp < 0) ? 0 :\n"
					"\t\t\t\t\t((tmp > 255) ? 255 : (unsigned char)tmp);\n"
			"\t\t\t}\n"
		"\t\t}\n"
	"\t}\n"
	"\telse if (data_type == IS_RGBA_PICTURE)\n"
	"\t{\n"
		"\t\tfor (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)\n"
		"\t\t{\n"
			"\t\t\tfor (j=0; j< 4*onx; j++)\n"
			"\t\t\t{\n"
				"\t\t\t\ttmp = factor * ps[i].ch[j];\n"
				"\t\t\t\tpd[i].ch[j] = (tmp < 0) ? 0 :\n"
					"\t\t\t\t\t((tmp > 255) ? 255 : (unsigned char)tmp);\n"
			"\t\t\t}\n"
		"\t\t}\n"
	"\t}\n");

	fprintf(fp,"\tinherit_from_im_to_im(oid,ois);\n"
		"\tuns_oi_2_oi(oid,ois);\n"
		"\toid->im.win_flag = ois->im.win_flag;\n"
		"\tif (ois->title != NULL)	set_im_title(oid, \"%%s rescaled by %%g\",\n"
		"\t\t ois->title,factor);\n"
		"\tset_formated_string(&oid->im.treatement,\n"
		"\t\t\"Image %%s rescaled by %%g\", ois->filename,factor);\n"
		"\treturn oid;\n"
		"}\n");

	fprintf(fp,"\nint do_%s_image_multiply_by_a_scalar(void)\n"
		"{\n"
		"\tO_i *ois = NULL, *oid = NULL;\n"
		"\timreg *imr = NULL;\n\tint i;\n"
		"\tstatic float factor = 2.0;\n\n",app_name);

	fprintf(fp,"\n\tif(updating_menu_state != 0)	return D_O_K;\n\n");

	fprintf(fp,"\tif (key[KEY_LSHIFT])\n"
		"\t{\n"
		"\t\treturn win_printf_OK(\"This routine multiply the image intensity\"\n"
		"\t\t\"by a user defined scalar factor\");\n"
		"\t}\n");

	fprintf(fp,"\tif (ac_grep(cur_ac_reg,\"%%im%%oi\",&imr,&ois) != 2)\n"
		"\t{\n"
		"\t\twin_printf(\"Cannot find image\");\n"
		"\t\treturn D_O_K;\n"
		"\t}\n"
		"\ti = win_scanf(\"define the factor of mutiplication %%f\",&factor);\n"
		"\tif (i == WIN_CANCEL)	return D_O_K;\n"
		"\toid = %s_image_multiply_by_a_scalar(ois,factor);\n"
		"\tif (oid == NULL)	return win_printf_OK(\"unable to create image!\");\n"
		"\tadd_image(imr, oid->type, (void*)oid);\n"
		"\tfind_zmin_zmax(oid);\n"
		"\treturn (refresh_image(imr, imr->n_oi - 1));\n"
		"}\n\n",app_name);




	fprintf(fp,"\nMENU *%s_image_menu(void)\n"
	"{\n"
	"\tstatic MENU mn[32];\n\n"
	"\tif (mn[0].text != NULL)	return mn;\n"
	"\tadd_item_to_menu(mn,\"average profile\", do_%s_average_along_y,"
	"NULL,0,NULL);\n"
	"\tadd_item_to_menu(mn,\"image z rescaled\", "
		"do_%s_image_multiply_by_a_scalar,NULL,0,NULL);\n"
	"\treturn mn;\n"
	"}\n",app_name,app_name,app_name);





	fprintf(fp,"\nint	%s_main(int argc, char **argv)\n{\n"
		"\t(void)argc;\n\t(void)argv;\n"
		"\tadd_image_treat_menu_item ( \"%s\", NULL, %s_image_menu(), 0, NULL);\n"
		"\treturn D_O_K;\n}\n",app_name,app_name,app_name);


	fprintf(fp,"\nint	%s_unload(int argc, char **argv)\n{\n"
		"\t(void)argc;\n\t(void)argv;\n"
		"\tremove_item_to_menu(image_treat_menu, \"%s\", NULL, NULL);\n"
		"\treturn D_O_K;\n}\n",app_name,app_name);



	fprintf(fp,"#endif\n\n");

	fclose(fp);

	strcpy(filename,path);
	strcat(filename,app_name);
	strcat(filename,".h");
	fp = fopen(filename,"w");
	if (fp == NULL)
	{
		win_printf("I cannot create file!\n%s",backslash_to_slash(filename));
		return D_O_K;
	}
	fprintf(fp,"#ifndef _%s_H_\n#define _%s_H_\n\n",APP_NAME,APP_NAME);
	fprintf(fp,"PXV_FUNC(int, do_%s_average_along_y, (void));\n",app_name);
	fprintf(fp,"PXV_FUNC(MENU*, %s_image_menu, (void));\n",app_name);
	fprintf(fp,"PXV_FUNC(int, %s_main, (int argc, char **argv));\n",app_name);
	fprintf(fp,"#endif\n\n");

	fclose(fp);
	win_printf("directory %s has been created\n"
			"containing files : Makefile, %s.c %s.h\n"
			"modify the source file and compile by running make\n",
			backslash_to_slash(path),app_name,app_name);

	return D_O_K;

}
