

# include "xvin.h"
# include "plot_op.h"
# include "gr_file.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "xv_main.h"
# include "im_reg.h"
# include "im_mn.h"
# include "imr_reg_gui.h"

# include "allegro.h"
# ifdef XV_WIN32
# include "winalleg.h"
# endif



# include <stdlib.h>
# include <string.h>
int	play_movie(void);
int set_complex_im_mode(void);
int	set_image_periodicity(void);
int check_image_complex_opt(void);
int	check_image_periodicity_opt(void);
int set_z_min_max(void);
int	next_frame(void);
int	previous_frame(void);
int	goto_frame(void);
int set_z_max_range(void);
int change_interpollation_display(void);
int set_rgb_im_mode(void);
int map_pixel_to_screen(void);
int set_image_width_heigh(void);
int get_complex_x_phase(void);
int get_complex_x_zero_phase_lines(void);


int change_roi(void);
# define 	AUTO		8192
# define 	INVERSE		4096

MENU image_scale[32] =
{
   { "4.0 (slow)",    change_image_scale,    NULL, MENU_INDEX(7),  NULL  },
   { "2.8 (slow)",    change_image_scale,    NULL, MENU_INDEX(10), NULL  },
   { "2.0 (fast)",    change_image_scale,    NULL, MENU_INDEX(14), NULL  },
   { "1.4 (slow)",    change_image_scale,    NULL, MENU_INDEX(20), NULL  },
   { "1.0 (fast)",    change_image_scale,    NULL, MENU_INDEX(28), NULL  },
   { "0.875 (slow)",  change_image_scale,    NULL, MENU_INDEX(32), NULL  },
   { "0.707 (slow)",  change_image_scale,    NULL, MENU_INDEX(40), NULL  },
   { "0.5 (fast)",    change_image_scale,    NULL, MENU_INDEX(56), NULL  },
   { "0.35 (slow)",   change_image_scale,    NULL, MENU_INDEX(80), NULL  },
   { "0.25 (slow)",   change_image_scale,    NULL, MENU_INDEX(112), NULL  },
   { "0.17 (slow)",   change_image_scale,    NULL, MENU_INDEX(160), NULL  },
   { "\0",       				   NULL,   		     NULL,            0, NULL  },
   { "Display raw",   change_interpollation_display,    NULL, MENU_INDEX(0), NULL  },
   { "Display linear interpolation",   change_interpollation_display,    NULL, MENU_INDEX(1), NULL  },

   { NULL,                 NULL,             NULL,              0, NULL  }
};


MENU image_opts[32] =
{ // Discarding const operator --> unsafe cast
   { "No Grid",       		   change_image_opt, NULL, MENU_INDEX(0), (char *) "0"  },
   { "Grid on",       		   change_image_opt, NULL, MENU_INDEX(1), (char *) "1"  },
   { "\0",       				   NULL,   		     NULL,            0, NULL  },
   { "pixel and usr coord.",       change_image_opt, NULL, MENU_INDEX(2),(char *) "2"  },
   { "Pixel only",       	   change_image_opt, NULL, MENU_INDEX(3),(char *) "3"  },
   { "user coord. only",           change_image_opt, NULL, MENU_INDEX(4),(char *) "4"  },
   { "\0",       				   NULL,             NULL,            0, NULL  },
   { "Numbering",     		   change_image_opt, NULL, MENU_INDEX(5),(char *) "5"  },
   { "No Numb.",       		   change_image_opt, NULL, MENU_INDEX(6),(char *) "6"  },
   { "\0",       				   NULL,        	 NULL,            0, NULL  },
   { "Region of interest",     	   change_roi, NULL, MENU_INDEX(5),(char *) "5"  },
   { "\0",       				   NULL,        	 NULL,            0, NULL  },
   { "Plot and Image independent", change_image_opt, NULL, MENU_INDEX(7),(char *) "7"  },
   { "Plot and Image Same X", 	   change_image_opt, NULL, MENU_INDEX(8),(char *) "8"  },
   { "Plot and Image Same Y", 	   change_image_opt, NULL, MENU_INDEX(9),(char *) "9"  },
   { NULL,                         NULL,             NULL,            0, NULL  }
};


MENU complex_image_menu[32] =
{
	{"Amplitude",			set_complex_im_mode,	NULL,	MENU_INDEX(1),(char *) "1"},
	{"Amplitude square	",	set_complex_im_mode,	NULL,	MENU_INDEX(2),(char *) "2"},
	{"Log10	 A^2",			set_complex_im_mode,	NULL,	MENU_INDEX(3),(char *) "3"},
	{"Reel part", 			set_complex_im_mode,	NULL,	MENU_INDEX(4),(char *) "4"},
	{"Imaginary part", 		set_complex_im_mode,	NULL,	MENU_INDEX(5),(char *) "5"},
	{ "\0",       			NULL,        	        NULL,   0,          NULL  },
	{"Extract Phase along X", 	get_complex_x_phase,	NULL,	0,               0},
	{"Extract Phase X phase lines",	get_complex_x_zero_phase_lines,	NULL,	0,               0},
	{NULL,      			NULL,			NULL,	0, 	NULL}
};


MENU rgb_image_menu[32] =
{
	{"Grey level",			set_rgb_im_mode,	NULL,	MENU_INDEX(1),(char *) "1"},
	{"Red -> grey",	                set_rgb_im_mode,	NULL,	MENU_INDEX(2),(char *) "2"},
	{"Red only",	                set_rgb_im_mode,	NULL,	MENU_INDEX(7),(char *) "7"},
	{"Green -> grey",		set_rgb_im_mode,	NULL,	MENU_INDEX(3),(char *) "3"},
	{"Green only",			set_rgb_im_mode,	NULL,	MENU_INDEX(8),(char *) "8"},
	{"Blue -> grey",		set_rgb_im_mode,	NULL,	MENU_INDEX(4),(char *) "4"},
	{"Blue only", 			set_rgb_im_mode,	NULL,	MENU_INDEX(9),(char *) "9"},
	{"Alpha channel", 		set_rgb_im_mode,	NULL,	MENU_INDEX(5),(char *) "5"},
	{"RGB", 		set_rgb_im_mode,	NULL,	MENU_INDEX(6),(char *) "6"},
	{NULL,      			NULL,					NULL,	0, 	NULL}
};


MENU image_periodicity_menu[32] =
{
	{"Periodic in X",		set_image_periodicity,	NULL,	MENU_INDEX(1),(char *) "1"},
	{"Not Periodic in X",	set_image_periodicity,	NULL,	MENU_INDEX(2),(char *) "2"},
    { "\0",  			     NULL,             	    NULL,            0, NULL  },
	{"Periodic in Y",		set_image_periodicity,	NULL,	MENU_INDEX(3),(char *) "3"},
	{"Not Periodic in Y", 	set_image_periodicity,	NULL,	MENU_INDEX(4),(char *) "4"},
    { "\0",  			     NULL,             	    NULL,            0, NULL  },		{"Periodic in X and Y",	set_image_periodicity,	NULL,	MENU_INDEX(5), NULL},
	{NULL,      			NULL,					NULL,	0, 	NULL}
};

MENU image_zminmax_menu[32] =
{
	{"Z limits",		set_z_min_max,	NULL,	0, NULL},
	{"Z limits auto",	set_z_min_max,	NULL,	MENU_INDEX(AUTO), NULL},
	{"Z limits max",	set_z_max_range,	NULL,	0, NULL},
	{"Z auto inverse",	set_z_min_max,	NULL,	MENU_INDEX(AUTO|INVERSE), NULL},


	{NULL,      		NULL,					NULL,	0, 	NULL}
};



MENU pixel_ratio[32] =
{
   { "4 image px = 1 screen px",    map_pixel_to_screen,    NULL, MENU_INDEX(32),  NULL  },
   { "2 image px = 1 screen px",    map_pixel_to_screen,    NULL, MENU_INDEX(16),  NULL  },
   { "1 image px = 1 screen px",    map_pixel_to_screen,    NULL, MENU_INDEX(8),  NULL  },
   { "1 image px = 2 screen px",    map_pixel_to_screen,    NULL, MENU_INDEX(4),  NULL  },
   { "1 image px = 4 screen px",    map_pixel_to_screen,    NULL, MENU_INDEX(2),  NULL  },
   { "image size",    set_image_width_heigh,    NULL, 0,  NULL  },
   { NULL,                 NULL,             NULL,              0, NULL  }
};



MENU image_option_menu[32] =
{
  { "Z limits",    NULL,        image_zminmax_menu,  0, NULL  },
  { "Scale",       NULL,        image_scale,         0, NULL  },
  { "Pixel ratio",       NULL,        pixel_ratio,         0, NULL  },
  { "Rulers",      NULL,        image_opts,          0, NULL  },
   { "Complex image mode",      NULL,    complex_image_menu,       0, NULL  },
   { "RGB image mode",      NULL,    rgb_image_menu,       0, NULL  },
   { "image periodicity",      NULL,     image_periodicity_menu,   0, NULL  },
  { "Play movie",  play_movie,  NULL,                0, NULL  },
  { "Next Frame",  next_frame,  NULL,                0, NULL  },
  { "previous Frame",  previous_frame,  NULL,                0, NULL  },
  { "goto Frame\tCtrl-G",  goto_frame,  NULL,                0, NULL  },
   { NULL,          NULL,       NULL,       0, NULL  }
};


int change_roi(void)
{
  imreg *imr = NULL;
  int xs, xe, ys, ye, i;
  char message[1024] = {0};

  if(updating_menu_state != 0)	return D_O_K;
  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL || imr->one_i == NULL || imr->one_i->im.pixel == NULL)
    return D_O_K;
  xs = imr->one_i->im.nxs;
  xe = imr->one_i->im.nxe;
  ys = imr->one_i->im.nys;
  ye = imr->one_i->im.nye;
  snprintf(message,sizeof(message),"Define region of interest\n"
           "Xs %%8d Xe %%8d in [0,%d]\n"
           "Ys %%8d Ye %%8d in [0,%d]\n",imr->one_i->im.nx,imr->one_i->im.ny);
  i = win_scanf(message,&xs,&xe,&ys,&ye);
  if (i == WIN_CANCEL) return 0;
  imr->one_i->im.nxs = xs;
  imr->one_i->im.nxe = xe;
  imr->one_i->im.nys = ys;
  imr->one_i->im.nye = ye;
  imr->one_i->need_to_refresh |= ALL_NEED_REFRESH;
  return D_REDRAW;
}

int map_pixel_to_screen(void)
{
	imreg *imr = NULL;
	int index;

	if(updating_menu_state != 0)	return D_O_K;

	imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL || imr->one_i == NULL || imr->one_i->im.pixel == NULL)
		return D_O_K;
	index = RETRIEVE_MENU_INDEX;


	map_pixel_ratio_of_image_and_screen(imr->one_i, ((float)index)/8, ((float)index)/8);
	//find_zmin_zmax(imr->one_i);
	//imr->one_i->need_to_refresh |= BITMAP_NEED_REFRESH | PLOTS_NEED_REFRESH;

	//return refresh_image(imr, UNCHANGED);

	return D_REDRAW;
}


int set_image_width_heigh(void)
{
  imreg *imr = NULL;
  static float w = 1, h = 1;

  if(updating_menu_state != 0)	return D_O_K;

  imr = find_imr_in_current_dialog(NULL);
  if (imr == NULL || imr->one_i == NULL || imr->one_i->im.pixel == NULL)
    return D_O_K;
  w = imr->one_i->width;
  h = imr->one_i->height;
  if ((win_scanf("Image size \nwidth %6f\n heigh %6f\n",&w,&h)) == WIN_CANCEL)
    return D_O_K;

  set_oi_horizontal_extend(imr->one_i, w);
  set_oi_vertical_extend(imr->one_i, h);

  //find_zmin_zmax(imr->one_i);
  //return refresh_image(imr, UNCHANGED);

  return D_REDRAW;
}



int set_z_min_max(void)
{
	int i = WIN_OK;
	imreg *imr = NULL;
	int index;
	static int inverse = 0;

	if(updating_menu_state != 0)	return D_O_K;

	imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL || imr->one_i == NULL || imr->one_i->im.pixel == NULL)
		return D_O_K;
	index = RETRIEVE_MENU_INDEX;

	if ( index == AUTO)		find_zmin_zmax(imr->one_i);
	//else if ( index == AUTO_INV)	find_zmin_zmax(imr->one_i);
	else
		i = win_scanf("Minimum Z value%12f\nMaximum Z value%12f\nInverse scale %b\n",
			      &(imr->one_i->z_min),&(imr->one_i->z_max),&inverse);
	if (i == WIN_CANCEL)	return 0;
	imr->one_i->z_black = imr->one_i->z_min;
	imr->one_i->z_white = imr->one_i->z_max;
	if ( index == INVERSE || inverse)
	{
		i = imr->one_i->z_black;
		imr->one_i->z_black = imr->one_i->z_white;
		imr->one_i->z_white = i;
	}
	imr->one_i->need_to_refresh |= BITMAP_NEED_REFRESH;
	return D_REDRAW;
}
int set_z_max_range(void)
{
	imreg *imr = NULL;

	imr = find_imr_in_current_dialog(NULL);
	if(updating_menu_state != 0)
	  {
		if (imr == NULL || imr->one_i->im.data_type == IS_FLOAT_IMAGE
		    || imr->one_i->im.data_type == IS_DOUBLE_IMAGE
		    || imr->one_i->im.data_type == IS_COMPLEX_IMAGE
		    || imr->one_i->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
		{
			active_menu->flags |=  D_DISABLED;
			return D_O_K;
		}
		else  active_menu->flags &= ~D_DISABLED;
		return D_O_K;
	  }
	if (imr == NULL || imr->one_i == NULL || imr->one_i->im.pixel == NULL)
		return D_O_K;

	switch (imr->one_i->im.data_type)
	{
		case  IS_CHAR_IMAGE:
		case  IS_RGB_PICTURE:
		case  IS_RGBA_PICTURE:
		  imr->one_i->z_black = imr->one_i->z_min = 0;
		  imr->one_i->z_white = imr->one_i->z_max = 255;
		break;
		case  IS_INT_IMAGE:
		  imr->one_i->z_black = imr->one_i->z_min = -32768;
		  imr->one_i->z_white = imr->one_i->z_max = 32767;
		break;
		case  IS_UINT_IMAGE:
		  imr->one_i->z_black = imr->one_i->z_min = 0;
		  imr->one_i->z_white = imr->one_i->z_max = 65535;
		break;
		case  IS_LINT_IMAGE:
		  imr->one_i->z_black = imr->one_i->z_min = 0x10000000;
		  imr->one_i->z_white = imr->one_i->z_max = 0x0FFFFFFF;
		break;
		case  IS_FLOAT_IMAGE:
		case  IS_DOUBLE_IMAGE:
		case  IS_COMPLEX_IMAGE:
		case  IS_COMPLEX_DOUBLE_IMAGE:
	        default: return D_O_K;
	}
	return D_REDRAW;
}
int change_interpollation_display(void)
{
  imreg *imr = NULL;
  int  index;

  index = RETRIEVE_MENU_INDEX;
  imr = find_imr_in_current_dialog(NULL);
  if(updating_menu_state != 0)
    {
      if (imr == NULL)
	{
	  active_menu->flags |=  D_DISABLED;
	  return D_O_K;
	}
      else active_menu->flags &= ~D_DISABLED;
      if (smooth_interpol == index)
	active_menu->flags |=  D_SELECTED;
      else active_menu->flags &= ~D_SELECTED;
      return D_O_K;
    }
  if (smooth_interpol == index) return D_O_K;
  smooth_interpol = index;
  if (imr != NULL)  imr->one_i->need_to_refresh |= BITMAP_NEED_REFRESH;
  return D_REDRAW;
}


int change_image_scale(void)
{
	imreg *imr = NULL;
	int  index;

	imr = find_imr_in_current_dialog(NULL);
	index = RETRIEVE_MENU_INDEX;
	if(updating_menu_state != 0)
	{
		if (imr == NULL)
		{
			active_menu->flags |=  D_DISABLED;
			return D_O_K;
		}
		else active_menu->flags &= ~D_DISABLED;
		if (imr->screen_scale == index)
			active_menu->flags |=  D_SELECTED;
		else active_menu->flags &= ~D_SELECTED;
		return D_O_K;
	}


	if (imr == NULL)		return D_O_K;

	if (index <=imr->max_scale && index >= imr->min_scale && imr->screen_scale != index)
	{
		imr->screen_scale = index;
		imr->one_i->need_to_refresh |= BITMAP_NEED_REFRESH;
	}
/*	return broadcast_dialog_message(MSG_DRAW,0);	*/
	return D_REDRAW;
}
# ifdef OLD
int check_image_scale(void)
{
	imreg *imr = NULL;
	int i, j, n = -1;

	if(updating_menu_state != 0)	return D_O_K;

	imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)		return D_O_K;

	for (j = 0; image_scale[j].text != NULL; j++)
	{
		if (image_scale[j].dp != NULL)
		{
			i = sscanf((char*)image_scale[j].dp,"%d",&n);
			if (i == 1 && n == imr->screen_scale)
				image_scale[j].flags |= D_SELECTED;
			else image_scale[j].flags &= !D_SELECTED;
		}
	}
	return D_O_K;
}


int check_image_opt(void)
{
	register int iopt, iopt2;
	O_i *oi = NULL;
	O_p *op = NULL;
	imreg *imr = NULL;
	int i, j, n, f;

	if(updating_menu_state != 0)	return D_O_K;

	imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)		return D_O_K;
	oi = imr->one_i;
	iopt = oi->iopt;
	iopt2 = oi->iopt2;
	op = find_oi_cur_op(oi);

	for (j = 0; image_opts[j].text != NULL; j++)
	{
		if (image_opts[j].dp != NULL)
		{
			f = 0;
			i = sscanf((char*)image_opts[j].dp,"%d",&n);
			if (i == 1)
			{
				if (n == 0)
					f = (iopt & NOAXES) ? 1 : 0;
				else if(n == 1)
					f = (iopt & NOAXES) ? 0 : 1;
				else if (n == 2)
					f = (iopt2 & IM_USR_COOR_RT) ? 1 : 0;
				else if (n == 3)
					f = (iopt2 & (IM_USR_COOR_RT | IM_USR_COOR_LB)) ? 0 : 1;
				else if (n == 4)
					f = ((iopt2&IM_USR_COOR_LB)&&(iopt2&IM_USR_COOR_RT)) ? 1 : 0;
				else if (n == 5)
					f = ((iopt2 & X_NUM) && (iopt2 & Y_NUM)) ? 1 : 0;
				else if (n == 6)
					f = ((iopt2 & X_NUM) && (oi->iopt2 & Y_NUM)) ? 0 : 1;
				else if (n == 7)
				{
					if (op == NULL)		image_opts[j].flags |=  D_DISABLED;
					else
					{
						f = (op->type & (IM_SAME_X_AXIS|IM_SAME_Y_AXIS)) ? 0 : 1;
						image_opts[j].flags &=  ~D_DISABLED;
					}
				}
				else if (n == 8)
				{
					if (op == NULL)		image_opts[j].flags |=  D_DISABLED;
					else
					{
						f = (op->type & IM_SAME_X_AXIS) ? 1 : 0;
						image_opts[j].flags &=  ~D_DISABLED;
					}
				}
				else if (n == 9)
				{
					if (op == NULL)		image_opts[j].flags |=  D_DISABLED;
					else
					{
						f = (op->type & IM_SAME_Y_AXIS) ? 1 : 0;
						image_opts[j].flags &=  ~D_DISABLED;
					}
				}
				if (f == 1)	image_opts[j].flags |= D_SELECTED;
				else image_opts[j].flags &= ~D_SELECTED;

			}
		}
	}
	return D_O_K;
}

# endif
int change_image_opt(void)
{
	register int iopt, iopt2;
	int redraw = 0;
	O_i *oi = NULL;
	O_p *op = NULL;
	imreg *imr = NULL;
	int  index, f;

	imr = find_imr_in_current_dialog(NULL);
	index = RETRIEVE_MENU_INDEX;
	if(updating_menu_state != 0)
	{
		if (imr == NULL)
		{
			active_menu->flags |=  D_DISABLED;
			return D_O_K;
		}
		else active_menu->flags &= ~D_DISABLED;
		oi = imr->one_i;
		iopt = oi->iopt;
		iopt2 = oi->iopt2;
		op = find_oi_cur_op(oi);
		f = 0;
		if (index == 0)		 f = (iopt & NOAXES) ? 1 : 0;
		else if(index == 1)  f = (iopt & NOAXES) ? 0 : 1;
		else if (index == 2) f = (iopt2 & IM_USR_COOR_RT) ? 1 : 0;
		else if (index == 3) f = (iopt2 & (IM_USR_COOR_RT | IM_USR_COOR_LB)) ? 0 : 1;
		else if (index == 4) f = ((iopt2&IM_USR_COOR_LB)&&(iopt2&IM_USR_COOR_RT)) ? 1 : 0;
		else if (index == 5) f = ((iopt2 & X_NUM) && (iopt2 & Y_NUM)) ? 1 : 0;
		else if (index == 6) f = ((iopt2 & X_NUM) && (oi->iopt2 & Y_NUM)) ? 0 : 1;
		else if (index == 7)
		{
			if (op == NULL)		active_menu->flags |=  D_DISABLED;
			else
			{
				f = (op->type & (IM_SAME_X_AXIS|IM_SAME_Y_AXIS)) ? 0 : 1;
				active_menu->flags &=  ~D_DISABLED;
			}
		}
		else if (index == 8)
		{
			if (op == NULL)		active_menu->flags |=  D_DISABLED;
			else
			{
				f = (op->type & IM_SAME_X_AXIS) ? 1 : 0;
				active_menu->flags &=  ~D_DISABLED;
			}
		}
		else if (index == 9)
		{
			if (op == NULL)		active_menu->flags |=  D_DISABLED;
			else
			{
				f = (op->type & IM_SAME_Y_AXIS) ? 1 : 0;
				active_menu->flags &=  ~D_DISABLED;
			}
		}
		if (f == 1)	active_menu->flags |= D_SELECTED;
		else active_menu->flags &= ~D_SELECTED;
		return D_O_K;
	}


	if (imr == NULL)		return D_O_K;

	oi = imr->one_i;
	iopt = oi->iopt;
	iopt2 = oi->iopt2;
	op = find_oi_cur_op(oi);
	if (index == 0)
	{
		redraw = (iopt & NOAXES) ? 0 : ALL_NEED_REFRESH;
		oi->iopt |= NOAXES;
	}
	else if (index == 1)
	{
		redraw = (iopt & NOAXES) ? ALL_NEED_REFRESH : 0;
		oi->iopt &= ~NOAXES;
	}
	else if (index == 2)
	{
		oi->iopt2 &= ~IM_USR_COOR_LB;
		oi->iopt2 |= IM_USR_COOR_RT;
		redraw = PLOTS_NEED_REFRESH;
	}
	else if (index == 3)
	{
		oi->iopt2 &= ~IM_USR_COOR_LB;
		oi->iopt2 &= ~IM_USR_COOR_RT;
		redraw = PLOTS_NEED_REFRESH;
	}
	else if (index == 4)
	{
		oi->iopt2 |= IM_USR_COOR_LB | IM_USR_COOR_RT;
		redraw = PLOTS_NEED_REFRESH;
	}
	else if (index == 5)
	{
		redraw = (iopt2 & X_NUM) ? ((iopt2 & Y_NUM) ? 0 : ALL_NEED_REFRESH)
			: ALL_NEED_REFRESH;
		oi->iopt2 |= X_NUM;
		oi->iopt2 |= Y_NUM;
	}
	else if (index == 6)
	{
		redraw = (iopt2 & X_NUM) ? ALL_NEED_REFRESH :
			((iopt2 & Y_NUM) ? ALL_NEED_REFRESH : 0);
		oi->iopt2 &= ~X_NUM;
		oi->iopt2 &= ~Y_NUM;
	}
	else if (index == 7)
	{
		if (op == NULL)		return D_O_K;
		else op->type &= ~(IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
		redraw = PLOTS_NEED_REFRESH;
	}
	else if (index == 8)
	{
		if (op == NULL)		return D_O_K;
		else op->type |= IM_SAME_X_AXIS;
		redraw = PLOTS_NEED_REFRESH;
	}
	else if (index == 9)
	{
		if (op == NULL)		return D_O_K;
		else op->type |= IM_SAME_Y_AXIS;
		redraw = PLOTS_NEED_REFRESH;
	}

/*	check_image_opt();
	check_image_complex_opt();
	check_image_periodicity_opt();	*/
	oi->need_to_refresh = redraw;
	return D_REDRAW;
}

# ifdef OLD

int check_image_complex_opt(void)
{
	O_i *oi = NULL;
	imreg *imr = NULL;
	int i, j, n;

	if(updating_menu_state != 0)	return D_O_K;

	imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)		return D_O_K;
	oi = imr->one_i;

	for (j = 0; complex_image_menu[j].text != NULL; j++)
	{
		if (complex_image_menu[j].dp != NULL)
		{
			i = sscanf((char*)complex_image_menu[j].dp,"%d",&n);
			if (i == 1)
			{
				if (oi->im.data_type != IS_COMPLEX_IMAGE)
				{
					complex_image_menu[j].flags |=  D_DISABLED;
				}
				else
				{
					if (n == 1 && oi->im.mode == AMP)
						complex_image_menu[j].flags |=  D_SELECTED;
					else if (n == 2 && oi->im.mode == AMP_2)
						complex_image_menu[j].flags |=  D_SELECTED;
					else if (n == 3 && oi->im.mode == LOG_AMP)
						complex_image_menu[j].flags |=  D_SELECTED;
					else if (n == 4 && oi->im.mode == RE)
						complex_image_menu[j].flags |=  D_SELECTED;
					else if (n == 5 && oi->im.mode == IM)
						complex_image_menu[j].flags |=  D_SELECTED;
				}
			}
		}
	}
	return D_O_K;
}
# endif
int set_rgb_im_mode(void)
{
	register int mode;
	int index;
	O_i *oi = NULL;
	imreg *imr = NULL;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;
	}

	index = RETRIEVE_MENU_INDEX;

	if(updating_menu_state != 0)
	{
		if (oi->im.data_type != IS_RGB_PICTURE && oi->im.data_type != IS_RGBA_PICTURE
		    && oi->im.data_type != IS_RGB16_PICTURE && oi->im.data_type != IS_RGBA16_PICTURE)
		{
			active_menu->flags |=  D_DISABLED;
		}
		else
		{
			active_menu->flags &=  ~D_DISABLED;
			active_menu->flags &=  ~D_SELECTED;
			if (index == 1 && oi->im.mode == GREY_LEVEL)
				active_menu->flags |=  D_SELECTED;
			else if (index == 2 && oi->im.mode == R_LEVEL)
				active_menu->flags |=  D_SELECTED;
			else if (index == 3 && oi->im.mode == G_LEVEL)
				active_menu->flags |=  D_SELECTED;
			else if (index == 4 && oi->im.mode == B_LEVEL)
				active_menu->flags |=  D_SELECTED;
			else if (index == 5 && oi->im.mode == ALPHA_LEVEL)
				active_menu->flags |=  D_SELECTED;
			else if (index == 6 && oi->im.mode == TRUE_RGB)
				active_menu->flags |=  D_SELECTED;
			else if (index == 7 && oi->im.mode == RED_ONLY)
				active_menu->flags |=  D_SELECTED;
			else if (index == 8 && oi->im.mode == GREEN_ONLY)
				active_menu->flags |=  D_SELECTED;
			else if (index == 9 && oi->im.mode == BLUE_ONLY)
				active_menu->flags |=  D_SELECTED;
		}
		return D_O_K;
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return D_O_K;
	mode = oi->im.mode;
	if (oi->im.data_type != IS_RGB_PICTURE && oi->im.data_type != IS_RGBA_PICTURE
	    && oi->im.data_type != IS_RGB16_PICTURE && oi->im.data_type != IS_RGBA16_PICTURE)
	  return D_O_K;
	if (index == 1)	oi->im.mode = GREY_LEVEL;
	else if (index == 2)	oi->im.mode = R_LEVEL;
	else if (index == 3)	oi->im.mode = G_LEVEL;
	else if (index == 4)	oi->im.mode = B_LEVEL;
	else if (index == 5)	oi->im.mode = ALPHA_LEVEL;
	else if (index == 6)	oi->im.mode = TRUE_RGB;
	else if (index == 7)	oi->im.mode = RED_ONLY;
	else if (index == 8)	oi->im.mode = GREEN_ONLY;
	else if (index == 9)	oi->im.mode = BLUE_ONLY;
	if (mode != oi->im.mode)
	{
		find_zmin_zmax(oi);
		refresh_image(imr, UNCHANGED);
	}
	return D_O_K;
}
int get_complex_x_phase(void)
{
	register int i, j;
	O_i *ois = NULL, *oid = NULL;
	imreg *imr = NULL;
	union pix *ps, *pd;     /* data des images (matrice) 	       */
	static int pok; // n_ref = 0,
	static float ampratio = 0.05;
	float phiref = 0, reref = 0, imref = 0, re, im, phin, *amp, ampmax;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;
	}
	if(updating_menu_state != 0)
	  {
	    if (ois->im.data_type != IS_COMPLEX_IMAGE)
	      active_menu->flags |=  D_DISABLED;
	    else  active_menu->flags &=  ~D_DISABLED;
	    return D_O_K;
	  }
	i = win_scanf("This routine extract the phase of a complex image\n"
		      "along X provide le signal amplitude is strong enough\n"
		      "specify the amplitude ratio below which phase is not computed \n"
		      "%f",&ampratio);
	if (i == WIN_CANCEL) return D_O_K;

	//n_ref = 0;
	oid = create_and_attach_oi_to_imr(imr, ois->im.nx, ois->im.ny, IS_FLOAT_IMAGE);
	amp = (float*) calloc(ois->im.nx,sizeof(float));
	if (oid == NULL || amp == NULL)	return win_printf_OK("cannot create phase image !");
	oid->filename = my_sprintf(oid->filename, "Phase-%s.gr",ois->filename);
	pd  = oid->im.pixel;
	ps  = ois->im.pixel;

	for (i = 0; i < ois->im.ny; i++)
	  {
	    for (j = 0; j < ois->im.nx; j++)
	      {
		re = ps[i].fl[2*j];
		im = ps[i].fl[2*j+1];
		pd[i].fl[j] = (im != 0 && re != 0) ? atan2(im,re) : 0;
		amp[j] += re * re + im * im;
	      }
	  }
	for (j = 0, ampmax = 0; j < ois->im.nx; j++)
	  ampmax = (amp[j] > ampmax) ? amp[j] : ampmax;

	/*
	for (j = 0; j < ois->im.nx; j++)
	  amp[j] = (amp[j] > ampmax*ampratio) ? 1 : 0;
	*/


	pok = 0;
	i = 0;
	for (j = 0; j < ois->im.nx; j++)
	  {
	    if (amp[j] > ampmax*ampratio)
	      {
		if (pok == 0)
		  {
		    pok = 1;
		    phiref = pd[i].fl[j];
		    reref = ps[i].fl[2*j];
		    imref = ps[i].fl[2*j+1];
		  }
		else
		  {
		    re = reref * ps[i].fl[2*j] + imref * ps[i].fl[2*j+1];
		    im = reref * ps[i].fl[2*j+1] - imref * ps[i].fl[2*j];
		    phin = phiref + atan2(im,re);
		    for (;phin - pd[i].fl[j] > M_PI_2; pd[i].fl[j] += M_PI);
		    for (;pd[i].fl[j] - phin > M_PI_2; pd[i].fl[j] -= M_PI);
		    phiref = pd[i].fl[j];
		    reref = ps[i].fl[2*j];
		    imref = ps[i].fl[2*j+1];
		  }
	      }
	    else
	      {
		pd[i].fl[j] = -M_PI * 2;
		pok = 0;
	      }
	  }





	for (j = 0; j < ois->im.nx; j++)
	  {
	    if (amp[j] > ampmax*ampratio)
	      {
		phiref = pd[0].fl[j];
		reref = ps[0].fl[2*j];
		imref = ps[0].fl[2*j+1];
		for (i = 1; i < ois->im.ny; i++)
		  {
		    re = reref * ps[i].fl[2*j] + imref * ps[i].fl[2*j+1];
		    im = reref * ps[i].fl[2*j+1] - imref * ps[i].fl[2*j];
		    phin = phiref + atan2(im,re);
		    for (;phin - pd[i].fl[j] > M_PI_2; pd[i].fl[j] += M_PI);
		    for (;pd[i].fl[j] - phin > M_PI_2; pd[i].fl[j] -= M_PI);
		    phiref = pd[i].fl[j];
		    reref = ps[i].fl[2*j];
		    imref = ps[i].fl[2*j+1];
		  }
	      }
	    else
	      {
		for (i = 1; i < ois->im.ny; i++)
		  pd[i].fl[j] = -M_PI * 2;
	      }
	  }
	inherit_from_im_to_im(oid,ois);		/* heritage global des proprietes de l'image 1 		*/
	uns_oi_2_oi(oid,ois);				/* heritage des unites de l'image 1 			*/
	oid->im.win_flag = ois->im.win_flag;		/* heritage des boundary condition de l'image 1 	*/
	find_zmin_zmax(oid);
	select_image_of_imreg(imr, imr->n_oi-1);
	oid->need_to_refresh = ALL_NEED_REFRESH;
	free(amp);
	return D_REDRAWME;
}


float find_zero_of_3_points_polynome1(float y_1, float y0, float y1)
{
	double a, b, c, x = 0, delta;

	a = ((y_1+y1)/2) - y0;
	b = (y1-y_1)/2;
	c = y0;
	delta = b*b - 4*a*c;
	if (a == 0)
	{
		x = (b != 0) ? -c/b : 0;
	}
	else if (delta >=0)
	{
		delta = sqrt(delta);
		x = (fabs(-b + delta) < fabs(-b - delta)) ? -b + delta : -b - delta ;
		x /= 2*a;
	}
	return (float)x;
}



int get_complex_x_zero_phase_lines(void)
{
	register int i, j, k;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds = NULL;
	imreg *imr = NULL;
	union pix *ps;     /* data des images (matrice) 	       */
	static int pok; // n_ref = 0,
	static float ampratio = 0.05;
	float phiref = 0, reref = 0, imref = 0, re, im, phin, *amp, ampmax, *l1, *l2, tmp;
	int n_line = 0, l = 0;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;
	}
	if(updating_menu_state != 0)
	  {
	    if (ois->im.data_type != IS_COMPLEX_IMAGE)
	      active_menu->flags |=  D_DISABLED;
	    else  active_menu->flags &=  ~D_DISABLED;
	    return D_O_K;
	  }
	i = win_scanf("This routine extract the phase of a complex image\n"
		      "along X provide le signal amplitude is strong enough\n"
		      "specify the amplitude ratio below which phase is not computed \n"
		      "%f",&ampratio);
	if (i == WIN_CANCEL) return D_O_K;

	//n_ref = 0;
	amp = (float*) calloc(ois->im.nx,sizeof(float));
	l1 = (float*) calloc(ois->im.nx,sizeof(float));
	l2 = (float*) calloc(ois->im.nx,sizeof(float));
	if (l1 == NULL || l2 == NULL || amp == NULL)	return win_printf_OK("cannot create phase image !");
	ps  = ois->im.pixel;

	// we look for average amplitude
	for (i = 0; i < ois->im.ny; i++)
	  {
	    for (j = 0; j < ois->im.nx; j++)
	      {
		re = ps[i].fl[2*j];
		im = ps[i].fl[2*j+1];
		amp[j] += re * re + im * im;
	      }
	  }
	// we find the max average amplitude
	for (j = 0, ampmax = 0; j < ois->im.nx; j++)
	  ampmax = (amp[j] > ampmax) ? amp[j] : ampmax;
	//win_printf("Ampmax %g",ampmax);

	// we compute phase of the first line if amplitude is large enough
	i = 0;
	for (j = 0, pok = -1; j < ois->im.nx; j++)
	  {
	    if (amp[j] > ampmax*ampratio)
	      {
		if (pok == -1)   // first point with significant amplitude: ref phase
		  {
		    pok = 1;
		    reref = re = ps[0].fl[2*j];
		    imref = im = ps[0].fl[2*j+1];
		    phiref = l1[j] = (im != 0 && re != 0) ? atan2(im,re) : 0;
		  }
		else
		  {
		    re = ps[0].fl[2*j];
		    im = ps[0].fl[2*j+1];
		    l1[j] = (im != 0 && re != 0) ? atan2(im,re) : 0; // phase between -pi and pi
		    // we find right phase with low accuracy
		    phin = phiref + atan2(reref * im - imref * re, reref * re + imref * im);
		    // we adjust accurate phase to be exact
		    for (;phin - l1[j] > M_PI_2; l1[j] += M_PI);
		    for (;l1[j] - phin > M_PI_2; l1[j] -= M_PI);
		    // we keep last good point
		    phiref = l1[j];
		    reref = re;
		    imref = im;
		  }
	      }
	    else
	      {
		l1[j] = -M_PI * 2;
		pok = (pok < 0) ? pok : 0;
	      }
	  }



	n_line = (int)(phiref/(2*M_PI));// we compute the number oof data set of phase lines

	//win_printf("n lines %d",n_line);

	for (i = 0; i < n_line; i++)
	  {  // allocation of the phase lines
	    if (i == 0)
	      {
		op = create_and_attach_op_to_oi(ois, ois->im.ny, ois->im.ny, 0, IM_SAME_X_AXIS | IM_SAME_Y_AXIS);
		ds = op->dat[0];
	      }
	    else
	      ds = create_and_attach_one_ds(op, ois->im.ny, ois->im.ny, 0);
	    //ds = create_and_attach_ds_to_oi(ois, ois->im.ny, ois->im.ny, 0);

	    inherit_from_im_to_ds(ds, ois);
	    set_formated_string(&ds->treatement,"Phase line %d \\pi",i);

	    // we record the first points of each phase lines
	    for (j = 1, k = 0, tmp = 2 * M_PI; j < ois->im.nx-1; j++)
	      {  // we find the point index of phase line
		if (l1[j-1] > - M_PI && l1[j] > - M_PI && l1[j] > - M_PI)
		  {
		    if (fabs (((l1[j-1] + l1[j] + l1[j+1])/3) - 2 * i * M_PI) < tmp)
		      {
			tmp = fabs (((l1[j-1] + l1[j] + l1[j+1])/3) - 2 * i * M_PI);
			k = j;
		      }
		  }
	      }
	    //win_printf("phase %d found at %d",i,k);
	    ds = op->dat[i];
	    if (k > 0)
	      {  // we extrapolate the phase line position
		ds->yd[0] = 0;
		ds->xd[0] = find_zero_of_3_points_polynome1(l1[k-1]- 2 * i * M_PI,
							    l1[k]- 2 * i * M_PI, l1[k+1]- 2 * i * M_PI) + k;
		//ds->xd[0] = k;
	      }
	    else
	      {
		ds->yd[0] = 0;
		ds->xd[0] = 0;
	      }
	  }
	//win_printf("%d phase lines",n_line);
	for (i = 1; i < ois->im.ny; i++)
	  { // we scaan all remaining lines of image
	    display_title_message ("Line %d",i);
	    for (j = 0; j < ois->im.nx; j++)
	      {
		if (amp[j] > ampmax*ampratio)
		  {
		    phiref = l1[j]; // the reference phase comes from previous line
		    reref = ps[i-1].fl[2*j];
		    imref = ps[i-1].fl[2*j+1];
		    // we find right phase with low accuracy
		    re = reref * ps[i].fl[2*j] + imref * ps[i].fl[2*j+1];
		    im = reref * ps[i].fl[2*j+1] - imref * ps[i].fl[2*j];
		    phin = phiref + atan2(im,re);
		    // phase between -pi and pi
		    re = ps[i].fl[2*j];
		    im = ps[i].fl[2*j+1];
		    l2[j] = (im != 0 && re != 0) ? atan2(im,re) : 0;
		    // we adjust accurate phase to be exact
		    for (;phin - l2[j] > M_PI_2; l2[j] += M_PI);
		    for (;l2[j] - phin > M_PI_2; l2[j] -= M_PI);
		  }
		else
		  {
		    l2[j] = -M_PI * 2;
		  }
	      }
	    for (l = 0; l < n_line; l++)
	      {
		for (j = 1, k = 0, tmp = 2 * M_PI; j < ois->im.nx-1; j++)
		  {   // we find the point index of phase line
		    if (l2[j-1] > - M_PI && l2[j] > - M_PI && l2[j] > - M_PI)
		      {
			if (fabs (((l2[j-1] + l2[j] + l2[j+1])/3) - 2 * l * M_PI) < tmp)
			  {
			    tmp = fabs (((l2[j-1] + l2[j] + l2[j+1])/3) - 2 * l * M_PI);
			    k = j;
			  }
		      }
		  }
		ds = op->dat[l];// we extrapolate the phase line position
		if (k > 0)
		  {
		    ds->yd[i] = i;
		    ds->xd[i] = find_zero_of_3_points_polynome1(l2[k-1]- 2 * l * M_PI,
								l2[k]- 2 * l * M_PI, l2[k+1]- 2 * l * M_PI) + k;
		    //ds->xd[i] = k;
		  }
		else
		  {
		    ds->yd[i] = i;
		    ds->xd[i] = 0;
		  }
	      }
	    for (j = 0; j < ois->im.nx; j++)
	      l1[j] = l2[j]; // we swap phase info from one line to the next
	  }
	free(amp);
	free(l1);
	free(l2);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"Phase lines");
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	ois->cur_op = ois->n_op - 1;
	ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}

int set_complex_im_mode(void)
{
	register int mode;
	int index;
	O_i *oi = NULL;
	imreg *imr = NULL;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;
	}

	index = RETRIEVE_MENU_INDEX;

	if(updating_menu_state != 0)
	{
		if (oi->im.data_type != IS_COMPLEX_IMAGE)
		{
			active_menu->flags |=  D_DISABLED;
		}
		else
		{
			active_menu->flags &=  ~D_DISABLED;
			if (index == 1 && oi->im.mode == AMP)
				active_menu->flags |=  D_SELECTED;
			else if (index == 2 && oi->im.mode == AMP_2)
				active_menu->flags |=  D_SELECTED;
			else if (index == 3 && oi->im.mode == LOG_AMP)
				active_menu->flags |=  D_SELECTED;
			else if (index == 4 && oi->im.mode == RE)
				active_menu->flags |=  D_SELECTED;
			else if (index == 5 && oi->im.mode == IM)
				active_menu->flags |=  D_SELECTED;
			else active_menu->flags &=  ~D_SELECTED;
		}
		return D_O_K;
	}
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return D_O_K;
	mode = oi->im.mode;
/*	if (ch == 0)		return ((mn_index == mode) ? ON : OFF);*/
	if (oi->im.data_type != IS_COMPLEX_IMAGE)	return D_O_K;
/*
	if (active_menu->dp == NULL)	return D_O_K;
	i = sscanf((char*)active_menu->dp,"%d",&index);
	if (i != 1)		return D_O_K;*/
	if (index == 1)	oi->im.mode = AMP;
	else if (index == 2)	oi->im.mode = AMP_2;
	else if (index == 3)	oi->im.mode = LOG_AMP;
	else if (index == 4)	oi->im.mode = RE;
	else if (index == 5)	oi->im.mode = IM;
	if (mode != oi->im.mode)
	{
		find_zmin_zmax(oi);
		refresh_image(imr, UNCHANGED);
	}
	return D_O_K;
}
int check_image_periodicity_opt(void)
{
	O_i *oi = NULL;
	imreg *imr = NULL;
	int i, j, n;

	if(updating_menu_state != 0)	return D_O_K;

	imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)		return D_O_K;
	oi = imr->one_i;

	for (j = 0; image_periodicity_menu[j].text != NULL; j++)
	{
		if (image_periodicity_menu[j].dp != NULL)
		{
			n = 0;
			i = sscanf((char*)image_periodicity_menu[j].dp,"%d",&n);
			if (i == 1)
			{
				if ((n == 1) && (oi->im.win_flag == X_PER))
					image_periodicity_menu[j].flags |=  D_SELECTED;
				else if ((n == 2) && !(oi->im.win_flag == X_PER))
					image_periodicity_menu[j].flags |= D_SELECTED;
				else if ((n == 3) && (oi->im.win_flag == Y_PER))
					image_periodicity_menu[j].flags |=  D_SELECTED;
				else if ((n == 4) && !(oi->im.win_flag == Y_PER))
					image_periodicity_menu[j].flags |= D_SELECTED;
			}
		}
	}
	return D_O_K;
}

int set_image_periodicity(void)
{
	int index, w_flag;
	O_i *oi = NULL;
	imreg *imr = NULL;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;
	}
	active_menu->flags &=  ~D_DISABLED;
	index = RETRIEVE_MENU_INDEX;
	w_flag = oi->im.win_flag;
	if(updating_menu_state != 0)
	{
		if (index == 1 && (w_flag & X_PER))
			active_menu->flags |=  D_SELECTED;
		else if (index == 2 && !(w_flag & X_PER))
			active_menu->flags |=  D_SELECTED;
		else if (index == 3 && (w_flag & Y_PER))
			active_menu->flags |=  D_SELECTED;
		else if (index == 4 && !(w_flag & Y_PER))
			active_menu->flags |=  D_SELECTED;
		else if (index == 5 && (w_flag & X_PER) && (w_flag & Y_PER))
			active_menu->flags |=  D_SELECTED;
		else 	active_menu->flags &=  ~D_SELECTED;
		return D_O_K;
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return D_O_K;
/*
	if (active_menu->dp == NULL)	return D_O_K;
	i = sscanf((char*)active_menu->dp,"%d",&index);
	if (i != 1)		return D_O_K;*/
	switch( index )
	{
		case 1:	oi->im.win_flag |= X_PER;		break;
		case 3:	oi->im.win_flag |= Y_PER;		break;
		case 2: oi->im.win_flag &= X_NOT_PER;	break;
		case 4:	oi->im.win_flag &= Y_NOT_PER;	break;
		case 5:	oi->im.win_flag |= X_PER|Y_PER;	break;
		default:		break;
	}
	remove_all_keyboard_proc();
	scan_and_update(image_menu);
	return D_O_K;
}


int	play_movie(void)
{
	register int i;
	O_i *oi = NULL;
	int nf = 0;
	imreg *imr;
	clock_t t, t0 = 0, tms, td;
	static float  ms = 20;
	//BITMAP *imb = NULL;
	//DIALOG *d = NULL;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;
	}

	if(updating_menu_state != 0)
	{
		if (oi->im.n_f == 1 || oi->im.n_f == 0)
			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
		return D_O_K;
	}
	nf = abs(oi->im.n_f);
	if (nf <= 0)
	{
		win_printf("This is not a movie");
		return D_O_K;
	}
	i = oi->im.c_f;

	i = win_scanf("Display frame time lapse %f (ms)",&ms);
	//d = the_dialog + 2;
	//d = find_dialog_associated_to_imr(imr, NULL);
	tms = (CLOCKS_PER_SEC*ms)/1000;
	//	win_printf("tms raw = %d",(int)tms);
	for (i = 0, td = clock(); i < nf; i++)
	{
		t0 = i*tms;
		switch_frame(oi,i);
		oi->need_to_refresh |= ALL_NEED_REFRESH;
		refresh_image(imr, UNCHANGED);
		/*
		oi->need_to_refresh |= BITMAP_NEED_REFRESH;
		display_image_stuff_16M(imr,d);
		if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL))
		{
		  imb = (BITMAP*)oi->bmp.stuff;
# ifdef XV_WIN32
		  if (win_get_window() == GetForegroundWindow())
		    {
# endif
		      acquire_bitmap(screen);
		      blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off -
			   imb->h + d->y, imb->w, imb->h);
		      release_bitmap(screen);
# ifdef XV_WIN32
		    }
		  draw_im_screen_label(imr, oi, oi->im.c_f);
# endif
		}


		*/
		//		write_and_blit_im_box( plt_buffer, &(the_dialog[2]), imr);
		for (t = clock() - td; t < t0; t = clock() - td);
	}
	td = clock() - td;
	display_title_message("%d im in %g s -> %g im/s",i,((float)td)/CLOCKS_PER_SEC,
			     ((float)i*CLOCKS_PER_SEC)/(float)td);
	return D_REDRAWME;

}

int     shift_ROI_right(void)
{
  O_i *oi = NULL;
  int dx, nv;
  imreg *imr = NULL;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_RIGHT, 0, next_frame);
      return D_O_K;
    }
  if (oi->im.nxs == 0 && oi->im.nxe == oi->im.nx)
    return 0;
  nv = (oi->im.nxe - oi->im.nxs)/2;
  dx = (oi->im.nxe + nv < oi->im.nx) ? nv : oi->im.nx - oi->im.nxe;
  oi->im.nxe += dx;
  oi->im.nxs += dx;
  oi->need_to_refresh = ALL_NEED_REFRESH;
  return refresh_image(imr, UNCHANGED);
}


int	next_frame(void)
{
	register int i;
	O_i *oi = NULL;
	//O_p *op = NULL;
	//d_s *ds = NULL;
	int nf = 0;//, n_line;
	imreg *imr = NULL;
	//BITMAP *imb = NULL;
	//DIALOG *d = NULL;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;
	}
	//d = find_dialog_associated_to_imr(imr, NULL);
	if(updating_menu_state != 0)
	{
		if (oi->im.n_f == 1 || oi->im.n_f == 0)
			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
	        add_keyboard_short_cut(0, KEY_RIGHT, 0, next_frame);
		return D_O_K;
	}
	nf = abs(oi->im.n_f);
  if (oi->im.movie_on_disk == 0 && nf<=0)
{
	  return shift_ROI_right();
	  //win_printf("This is not a movie");
	  //return D_O_K;
	}
	i = oi->im.c_f + 1;
  if (oi->im.movie_on_disk == 0)i %= nf;

	//	d = the_dialog + 2;
	switch_frame(oi,i);
	oi->need_to_refresh |= ALL_NEED_REFRESH;
	/*
	oi->need_to_refresh |= BITMAP_NEED_REFRESH;
	display_image_stuff_16M(imr,d);
	if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL))
	{
	  imb = (BITMAP*)oi->bmp.stuff;
# ifdef XV_WIN32
    if (win_get_window() == GetForegroundWindow())
      {
# endif
	  acquire_bitmap(screen);
	  blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off -
		imb->h + d->y, imb->w, imb->h);
	  release_bitmap(screen);
# ifdef XV_WIN32
      }
# endif
	}
	draw_im_screen_label(imr, oi, oi->im.c_f);
	op = find_oi_cur_op(oi);
	if (op)
	  {
	    ds = find_treatement_specific_ds_in_op(op, "line profile");
	    if (ds != NULL)
	      {
		if (sscanf (ds->treatement,"line profile at %d",&n_line) >= 1)
		  do_line_profile(oi, n_line, d);
		}
	    ds = find_treatement_specific_ds_in_op(op, "row profile");
	    if (ds != NULL)
	      {
		if (sscanf (ds->treatement,"row profile at %d",&n_line) >= 1)
		  do_row_profile(oi, n_line, d);
	      }
	    oi->need_to_refresh |= PLOTS_NEED_REFRESH;
	  }
	*/
	  refresh_image(imr, UNCHANGED);
	display_title_message("%05d im",i);
	return D_REDRAWME;

}

int     shift_ROI_left(void)
{
    O_i *oi = NULL;
    int dx, nv;
    imreg *imr = NULL;

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
    {
      active_menu->flags |=  D_DISABLED;
      return D_O_K;
    }
  if(updating_menu_state != 0)
    {
      add_keyboard_short_cut(0, KEY_RIGHT, 0, next_frame);
      return D_O_K;
    }
  if (oi->im.nxs == 0 && oi->im.nxe == oi->im.nx)
    return 0;
  nv = (oi->im.nxe - oi->im.nxs)/2;
  dx = (oi->im.nxs - nv >= 0) ? nv : oi->im.nxs;
  oi->im.nxe -= dx;
  oi->im.nxs -= dx;
  oi->need_to_refresh = ALL_NEED_REFRESH;
  return refresh_image(imr, UNCHANGED);
}

int	previous_frame(void)
{
   register int i;
   O_i *oi = NULL;
   //O_p *op = NULL;
   //d_s *ds = NULL;
   int nf = 0;//, n_line;
   imreg *imr = NULL;
   //BITMAP *imb = NULL;
   //DIALOG *d = NULL;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;
	}
	//d = find_dialog_associated_to_imr(imr, NULL);
	if(updating_menu_state != 0)
	{
		if (oi->im.n_f == 1 || oi->im.n_f == 0)
			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
	        add_keyboard_short_cut(0, KEY_LEFT, 0, previous_frame);
		return D_O_K;
	}


	nf = abs(oi->im.n_f);
	if (nf <= 0)
	{
	  return shift_ROI_left();
	  //win_printf("This is not a movie");
	  //	return D_O_K;
	}
	i = oi->im.c_f - 1;
	i = (i < 0) ? i + nf : i;
	i %= nf;
	//d = the_dialog + 2;
	switch_frame(oi,i);

	oi->need_to_refresh |= ALL_NEED_REFRESH;
	/*
	oi->need_to_refresh |= BITMAP_NEED_REFRESH;
	display_image_stuff_16M(imr,d);
	if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL))
	{
	  imb = (BITMAP*)oi->bmp.stuff;
# ifdef XV_WIN32
	  if (win_get_window() == GetForegroundWindow())
	    {
# endif
	      acquire_bitmap(screen);
	      blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off -
		   imb->h + d->y, imb->w, imb->h);
	      release_bitmap(screen);
# ifdef XV_WIN32
	    }
# endif
	}
	draw_im_screen_label(imr, oi, oi->im.c_f);
	op = find_oi_cur_op(oi);
	if (op)
	  {
	    ds = find_treatement_specific_ds_in_op(op, "line profile");
	    if (ds != NULL)
	      {
		if (sscanf (ds->treatement,"line profile at %d",&n_line) >= 1)
		  do_line_profile(oi, n_line, d);
		}
	    ds = find_treatement_specific_ds_in_op(op, "row profile");
	    if (ds != NULL)
	      {
		if (sscanf (ds->treatement,"row profile at %d",&n_line) >= 1)
		  do_row_profile(oi, n_line, d);
	      }
	    oi->need_to_refresh |= PLOTS_NEED_REFRESH;
	  }
	*/
	refresh_image(imr, UNCHANGED);
	display_title_message("%05d im",i);
	return D_REDRAWME;

}



int	goto_frame(void)
{
    register int i;
    O_i *oi = NULL;
    //O_p *op = NULL;
    //d_s *ds = NULL;
    int nf = 0, cf;//, n_line;
    imreg *imr = NULL;
    //BITMAP *imb = NULL;
    //DIALOG *d = NULL;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;
	}
	//d = find_dialog_associated_to_imr(imr, NULL);
	if(updating_menu_state != 0)
	{
		if (oi->im.n_f == 1 || oi->im.n_f == 0)
			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
		return D_O_K;
	}
	nf = abs(oi->im.n_f);
	if (nf <= 0)
	{
		win_printf("This is not a movie");
		return D_O_K;
	}
	cf = i = oi->im.c_f;
	i = win_scanf("Goto frame %d",&cf);
	if (i == WIN_CANCEL)   return OFF;

	i = (cf < 0) ? nf-1 : cf;
	i %= nf;

	//d = the_dialog + 2;
	switch_frame(oi,i);
	oi->need_to_refresh |= ALL_NEED_REFRESH;
	/*
	oi->need_to_refresh |= BITMAP_NEED_REFRESH;
	display_image_stuff_16M(imr,d);
	if ((oi->bmp.to == IS_BITMAP) &&  (oi->bmp.stuff != NULL))
	{
	  imb = (BITMAP*)oi->bmp.stuff;
# ifdef XV_WIN32
	  if (win_get_window() == GetForegroundWindow())
	    {
# endif
	      acquire_bitmap(screen);
	      blit(imb,screen,0,0,imr->x_off + d->x, imr->y_off -
		   imb->h + d->y, imb->w, imb->h);
	      release_bitmap(screen);
# ifdef XV_WIN32
	    }
# endif
	}
	draw_im_screen_label(imr, oi, oi->im.c_f);
	op = find_oi_cur_op(oi);
	if (op)
	  {
	    ds = find_treatement_specific_ds_in_op(op, "line profile");
	    if (ds != NULL)
	      {
		if (sscanf (ds->treatement,"line profile at %d",&n_line) >= 1)
		  do_line_profile(oi, n_line, d);
		}
	    ds = find_treatement_specific_ds_in_op(op, "row profile");
	    if (ds != NULL)
	      {
		if (sscanf (ds->treatement,"row profile at %d",&n_line) >= 1)
		  do_row_profile(oi, n_line, d);
	      }
	  }
	*/
	refresh_image(imr, UNCHANGED);
	display_title_message("%d im",i);
	return D_REDRAWME;

}



int		do_mouse_wheel_zoom_in_on_oi(imreg *imr, O_i *oi)
{
    float pxl, pyl, rx, ry;
    int inxef, inyef, snx, sny, x0, x1, y0, y1, sc, inx, iny;

    pxl = x_imr_2_imdata(imr, mouse_x);
    pyl = y_imr_2_imdata(imr, mouse_y);
    sc = imr->screen_scale;
    // size of the displayed image
    inxef = oi->im.nxe - oi->im.nxs;
    inyef = oi->im.nye - oi->im.nys;
    // size of image on the screen
    snx = (int)(PIXEL_1 * 512 * oi->width +.5);
    sny = (int)(PIXEL_1 * 512 * oi->height +.5);

    rx = (float)snx/(sc*inxef);
    ry = (float)sny/(sc*inyef);
    for (inx = oi->im.nx; inx >= inxef; inx /= 2);
    for (iny = oi->im.ny; iny >= inyef; iny /= 2);
    my_set_window_title("zooming in rx %g  ry %g inx %d/%d iny %d/%d",rx,ry,inx,oi->im.nx,iny,oi->im.ny);
    if (rx < 16.0 && ry < 16.0)
       {
           x0 = (int)(0.5 + pxl - (pxl - oi->im.nxs)/2);
           x0 = (x0 < 0) ? 0 : x0;
           y0 = (int)(0.5 + pyl - (pyl - oi->im.nys)/2);
           y0 = (y0 < 0) ? 0 : y0;
           x1 = x0 + inx;
           if (x1 > oi->im.nx)
               {
                   x1 = oi->im.nx;
                   x0 = x1 - inx;
                   x0 = (x0 < 0) ? 0 : x0;
               }
           y1 = y0 + iny;
           if (y1 > oi->im.ny)
               {
                   y1 = oi->im.ny;
                   y0 = y1 - iny;
                   y0 = (y0 < 0) ? 0 : y0;
               }
           my_set_window_title("ROI -> x0 = %d y0 %d x1 %d y1 %d (%dx%d)",x0, y0, x1, y1,x1-x0,y1-y0);
           set_oi_region_of_interest_in_pixel(oi, x0, y0, x1, y1);
           return refresh_image(imr, UNCHANGED);
       }
    return 0;
}

int		do_mouse_wheel_zoom_out_on_oi(imreg *imr, O_i *oi)
{
    float pxl, pyl, rx, ry, rxm, rym;
    int inxef, inyef, snx, sny, x0, x1, y0, y1, sc, inx, iny;

    pxl = x_imr_2_imdata(imr, mouse_x);
    pyl = y_imr_2_imdata(imr, mouse_y);
    sc = imr->screen_scale;
    // size of the displayed image
    inxef = oi->im.nxe - oi->im.nxs;
    inyef = oi->im.nye - oi->im.nys;
    // size of image on the screen
    snx = (int)(PIXEL_1 * 512 * oi->width +.5);
    sny = (int)(PIXEL_1 * 512 * oi->height +.5);

    rxm = (float)snx/(oi->im.nx*sc);
    rym = (float)sny/(oi->im.ny*sc);

    rx = (float)snx/(inxef*sc);
    ry = (float)sny/(inyef*sc);
    for (inx = oi->im.nx; inx >= inxef; inx /= 2);
    inx *= 2;
    inx = (inx > oi->im.nx) ? oi->im.nx : inx;
    for (iny = oi->im.ny; iny >= inyef; iny /= 2);
    iny *= 2;
    iny = (iny > oi->im.ny) ? oi->im.ny : iny;
    my_set_window_title("zooming out rx %g rxm %g ry %g rym %g",rx,rxm,ry,rym);
    if (rx >= rxm && ry >= rym)
       {
           x0 = pxl - 2*(pxl - oi->im.nxs);
           x0 = (x0 < 0) ? 0 : x0;
           y0 = pyl - 2*(pyl - oi->im.nys);
           y0 = (y0 < 0) ? 0 : y0;
           x1 = x0 + 2*inx;
           if (x1 > oi->im.nx)
               {
                   x1 = oi->im.nx;
                   x0 = x1 - 2*inx;
                   x0 = (x0 < 0) ? 0 : x0;
               }
           y1 = y0 + 2*iny;
           if (y1 > oi->im.ny)
               {
                   y1 = oi->im.ny;
                   y0 = y1 - 2*iny;
                   y0 = (y0 < 0) ? 0 : y0;
               }
           //win_printf("ROI -> x0 = %d y0 %d x1 %d y1 %d",x0, y0, x1, y1);
           my_set_window_title("ROI -> x0 = %d y0 %d x1 %d y1 %d (%dx%d)",x0, y0, x1, y1,x1-x0,y1-y0);
           set_oi_region_of_interest_in_pixel(oi, x0, y0, x1, y1);
           return refresh_image(imr, UNCHANGED);
       }
    return 0;
}




int   select_image_wheel_action(imreg *imr, O_i *oi, DIALOG *d, int ticks)
{
  (void)d;
  //win_printf("wheel select");
    if (ticks > 0)
      {
	for( ; ticks > 0; ticks--)
                do_mouse_wheel_zoom_in_on_oi(imr, oi);
      }
    else if (ticks < 0)
      {
	for(ticks = -ticks ; ticks > 0; ticks--)
                do_mouse_wheel_zoom_out_on_oi(imr, oi);
      }
    return 0;
}
