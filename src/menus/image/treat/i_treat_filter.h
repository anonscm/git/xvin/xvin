/****************************************************************/
/* i_treat_filter.h						*/
/* header for i_treat_filter.c					*/
/*								*/
/* operations on images related to filtering 			*/
/* and using fftlib						*/
/****************************************************************/


extern int filter_mode; // defined and initialized in i_treat_filter.c

int filter_1d(void);
int fft_fil_inr_outc(float *x, int nx, int w_flag, int f, int w);
int fft_fil_inr_outr(float *x, int nx, int w_flag, int f, int w);
int fft_fil_inc_outc(float *x, int nx, int w_flag, int f, int w);

