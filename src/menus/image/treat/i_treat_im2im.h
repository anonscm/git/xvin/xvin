/********************************************************************************/
/* Math. operations on one or two images					*/
/********************************************************************************/
/* Nicolas Garnier	nicolas.garnier@ens-lyon.fr				*/
/********************************************************************************/

#include "xvin.h"


// definitions for operatinos on 2 images:
#define IM_ADD			0x0100
#define IM_SUBSTRACT		0x0200
#define IM_MULTIPLY		0x0300
#define IM_DIVIDE		0x0400

int 	do_opp_on_two_images(void);
int 	do_sliding_average_y(void);

