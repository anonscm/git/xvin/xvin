/****************************************************************/
/* i_treat_im2plot.h						*/
/* header for i_treat_im2plot.c					*/
/*								*/
/* operations on images realizing something that results	*/
/* in a (new) plot or dataset					*/
/****************************************************************/

int do_image_cst_measure(void);
int image_cst_measure_engine(O_i *oi);
