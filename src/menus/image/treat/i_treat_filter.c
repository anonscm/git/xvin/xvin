/****************************************************************/
/* i_treat_filter.c						*/
/*								*/
/* operations on images related to filtering 			*/
/* and using fftlib						*/
/****************************************************************/

# include <stdlib.h>
# include <string.h>

#include "allegro.h"
#include "xvin.h"

#include "fftl32n.h"
#include "fillib.h"

#include "i_treat_filter.h"
int filter_mode = REAL_MODE + LOW_PASS;


// leads to nx/2 complex points
int fft_fil_inr_outc(float *x, int nx, int w_flag, int f, int w)
{
  register int j;
  float  moy, smp = 0.05;

  for (j = 0, moy = 0.0; j < nx ; j++)	moy += x[j];
  for (j = 0, moy /= nx; j < nx ; j++)	x[j] -= moy;
  if (w_flag)		fftwindow1(nx, x, smp);
  realtr1(nx, x);		fft(nx, x, 1);	realtr2(nx, x, 1);
  if (filter_mode & BAND_PASS)	  bandpass_smooth_half (nx, x, f, w);
  else if (filter_mode & LOW_PASS)  lowpass_smooth_half (nx, x, f);
  else if (filter_mode & HIGH_PASS) hipass_smooth_half (nx, x, f);
  else if (filter_mode & BRICK_WALL)
    for (j = 2*f, x[1] = 0; j < nx ; j++)	x[j] = 0;	
  fft(nx, x, -1);
  if (w_flag)		defftwc1(nx, x, smp);
  if (filter_mode & LOW_PASS)	
    for (j = 0; j<nx ; j++, j++)	x[j] += moy;
  return 0;	
}


// leads to nx complex points
int fft_fil_inr_outc_no_sampling_reduction(float *x, float *im, int nx, int w_flag, int f, int w)
{
  register int j;
  float  moy, smp = 0.05;

  for (j = 0, moy = 0.0; j < nx ; j++)	moy += x[j];
  for (j = 0, moy /= nx; j < nx ; j++)	x[j] -= moy;
  if (w_flag)		fftwindow1(nx, x, smp);
  realtr1(nx, x);		fft(nx, x, 1);	realtr2(nx, x, 1);
  if (filter_mode & BAND_PASS)	  bandpass_smooth_half (nx, x, f, w);
  else if (filter_mode & LOW_PASS)  lowpass_smooth_half (nx, x, f);
  else if (filter_mode & HIGH_PASS) hipass_smooth_half (nx, x, f);
  else if (filter_mode & BRICK_WALL)
    for (j = 2*f, x[1] = 0; j < nx ; j++)	x[j] = 0;
  for (j = 2, im[1] = im[0] = 0; j < nx; j+=2) 
    {
      im[j+1] = x[j];
      im[j] = -x[j+1];
    }
  realtr2(nx, x, -1);	fft(nx, x, -1);	 realtr1(nx, x);
  realtr2(nx, im, -1);	fft(nx, im, -1); realtr1(nx, im);  
  if (w_flag)	defftwindow1(nx, x, smp);
  if (w_flag)	defftwindow1(nx, im, smp);
  if (filter_mode & LOW_PASS)	
    for (j = 0; j<nx ; j++, j++)	x[j] += moy;
  return 0;	
}



int fft_fil_inr_outr(float *x, int nx, int w_flag, int f, int w)
{
  register int j;
  float  moy, smp = 0.05;

  for (j = 0, moy = 0.0; j < nx ; j++)	moy += x[j];
  for (j = 0, moy /= nx; j < nx ; j++)	x[j] -= moy;
  if (w_flag)		fftwindow1(nx, x, smp);
  realtr1(nx, x);
  fft(nx, x, 1);
  realtr2(nx, x, 1);
  if (filter_mode & BAND_PASS)	  bandpass_smooth_half (nx, x, f, w);
  else if (filter_mode & LOW_PASS)  lowpass_smooth_half (nx, x, f);
  else if (filter_mode & HIGH_PASS) hipass_smooth_half (nx, x, f);
  else if (filter_mode & BRICK_WALL)
    for (j = 2*f, x[1] = 0; j < nx ; j++)	x[j] = 0;
  realtr2(nx, x, -1);	fft(nx, x, -1);	realtr1(nx, x);
  if (w_flag)	defftwindow1(nx, x, smp);
  if ((filter_mode & LOW_PASS)|| (filter_mode & BRICK_WALL))
    for (j = 0 ; j<nx ; j++)	x[j] += moy;
  return 0;
}
int fft_fil_inc_outc(float *x, int nx, int w_flag, int f, int w)
{
  register int j, ret = 0;
  float  moyr, moyi, smp = 0.05;

  for (j = 0, moyr = 0.0; j < nx ; j++, j++)	moyr += x[j];
  for (j = 1, moyi = 0.0; j < nx ; j++, j++)	moyi += x[j];	
  for (j = 0, moyr /= nx, moyr *= 2; j < nx ; j++, j++)	x[j] -= moyr;
  for (j = 1, moyi /= nx, moyi *= 2; j < nx ; j++, j++)	x[j] -= moyi;
  if (w_flag)		fftwc1(nx, x, smp);
  fft(nx, x, 1);
  if (filter_mode & SYM)
    {
      f = (f < 0) ? -f : f;
      if (filter_mode & BAND_PASS)	  ret = bandpass_smooth_sym (nx, x, f, w);
      else if (filter_mode & LOW_PASS)  ret = lowpass_smooth_sym (nx, x, f);
      else if (filter_mode & HIGH_PASS) ret = hipass_smooth_sym (nx, x, f);
      else if (filter_mode & BRICK_WALL)
	for (j = 2*abs(f); j < nx - 2*abs(f); j++)	x[j] = 0;
    }
  else if (filter_mode & DI_SYM)
    {
      if (filter_mode & BAND_PASS)	  ret = bandpass_smooth_dissym (nx, x, f, w);
      else if (filter_mode & LOW_PASS)  ret = lowpass_smooth_dissym (nx, x, f);
      else if (filter_mode & HIGH_PASS) ret = hipass_smooth_dissym (nx, x, f);
      else if (filter_mode & BRICK_WALL)
	{
	  if (f >= 0)	for (j = 2*f; j < nx; j++)	x[j] = 0;
	  else 		for (j = 2; j < nx+2*f; j++)	x[j] = 0;
	}
    }	
  fft(nx, x, -1);
  if ( w_flag)	defftwc1(nx, x, smp);
  if ((filter_mode & LOW_PASS)|| (filter_mode & BRICK_WALL))
    {
      for (j = 0; j < nx ; j++, j++)	x[j] += moyr;
      for (j = 1; j < nx ; j++, j++)	x[j] += moyi;
    }
  return ret;
}



int filter_1d(void)
{
  register int i, j;
  static int f_c = 0, width = 0, nout = 0, fil_type = 1, d_type = 0;
  int onx, ony, w_flag, no, no1, nout1 = 0;
  char *c1 = NULL, c2[128] = {0};
  int index = 0, periodicity, flag_org;
  float *tmp = NULL, *im = NULL;
  union pix *pd = NULL;
  O_i *ois = NULL, *oid = NULL;
  imreg *imr = NULL;

  if(updating_menu_state != 0)	return D_O_K;	
		
  if (active_menu->dp == NULL)	return D_O_K;
  if (strcmp((const char *) active_menu->dp,"P_LINE") == 0) index = P_LINE;
  else if (strcmp((const char *) active_menu->dp,"P_COL") == 0) index = P_COL;
  else return D_O_K;
		
  if (index != P_LINE && index != P_COL) return D_O_K;
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  /*	if (ois->im.data_type == IS_COMPLEX_IMAGE)	return filter_1d_c(CR);*/
  if (ois->im.data_type == IS_COMPLEX_IMAGE)	return D_O_K;	
  if (nout == 0 || f_c == 0)
    {
      nout = (index == P_LINE) ? ois->im.nx :  ois->im.ny;
      f_c = nout/4;
      width = nout/8;
    }	
  if (index == P_COL && nout > ois->im.ny)	nout = ois->im.ny;
  if (index == P_LINE && nout > ois->im.nx)	nout = ois->im.nx;

  flag_org = ois->im.win_flag;
  periodicity = (index == P_LINE) ? ois->im.win_flag & X_PER : ois->im.win_flag & Y_PER;
  periodicity = (periodicity) ? 1 : 0;

  i = win_scanf("Select the filter type\n"
		"low pass->%R band pass->%r high pass->%r low pass brick wall->%r\n"
		"Output data type Real->%R Complex(nx/2 pts)->%r Complex(nx pts)->%r\n"
		"f_c (in mode) cutoff%6d width (for band pass)%6d\nn_{out}%6d\n" 
		"Boundary contions: Non periodic->%R periodic %r\n"
		,&fil_type,&d_type,&f_c, &width, &nout,&periodicity);
  if (i == WIN_CANCEL)	return OFF;	
  if (periodicity) ois->im.win_flag |= (index == P_LINE) ? X_PER : Y_PER; 
  else ois->im.win_flag &= (index == P_LINE) ? X_NOT_PER :  Y_NOT_PER;
  
  filter_mode &= ~(LOW_PASS|BAND_PASS|HIGH_PASS|BRICK_WALL);
	
  if (fil_type == 0) filter_mode |= LOW_PASS;
  else if (fil_type == 3) filter_mode |= BRICK_WALL;	
  else if (fil_type == 1) filter_mode |= BAND_PASS;	
  else if (fil_type == 2) filter_mode |= HIGH_PASS;			
  else return D_O_K;
	
  filter_mode &= ~(REAL_MODE+COMPLEX_MODE);

  if (d_type == 0) filter_mode |= REAL_MODE;
  else if (d_type == 1) filter_mode |= COMPLEX_MODE;	
  else if (d_type == 2) filter_mode |= COMPLEX_MODE;	
  else return D_O_K;
	
	
  if (index == P_LINE)
    {
      onx = ((d_type == 1) && (nout > ois->im.nx/2)) // filter_mode & COMPLEX_MODE 
	? ois->im.nx/2 : nout;
      ony = ois->im.ny;
      tmp = (float*)calloc(2*ois->im.nx,sizeof(float));
      im = tmp + ois->im.nx;
    }
  else
    {
      ony = ((d_type == 1) && (nout > ois->im.ny/2)) 
	? ois->im.ny/2 : nout;
      onx = ois->im.nx;
      tmp = (float*)calloc(2*ois->im.ny,sizeof(float));
      im = tmp + ois->im.ny;
    }
  if (filter_mode & REAL_MODE) oid = create_one_image(onx, ony, IS_FLOAT_IMAGE);
  else 	oid = create_one_image(onx, ony, IS_COMPLEX_IMAGE);
  if ( oid == NULL )	return (win_printf_OK ("Can't create dest image"));
  pd = oid->im.pixel;	

  if (index == P_LINE)
    {
      if (fft_init(ois->im.nx) || filter_init(ois->im.nx)) 
	return (win_printf_OK("Can't initialize fft"));
      no = ois->im.nx/onx;
      w_flag = (!(ois->im.win_flag & X_PER)) ? 1 : 0;
      for (j=0; j< ois->im.ny; j++)
	{
	  no1 = onx * j;
	  if (no1 >= nout1 + 16384)
	    {
	      draw_bubble(screen,0, 10, 64, "fft line %d",j);
	      nout1 = no1;
	    }
	  extract_raw_line (ois, j, tmp);
	  if (d_type == 0)  // filter_mode & REAL_MODE
	    {
	      fft_fil_inr_outr(tmp, ois->im.nx,w_flag,f_c,width);
	      for (i = 0; i< onx ; i++) pd[j].fl[i] = tmp[i*no];
	    }
	  else 	if (d_type == 1)
	    {
	      fft_fil_inr_outc(tmp, ois->im.nx,w_flag,f_c,width);
	      for (i = 0; i< onx ; i++)
		{
		  pd[j].fl[2*i] = tmp[i*no];
		  pd[j].fl[2*i+1] = tmp[i*no+1];
		}
	    }
	  else 	if (d_type == 2)
	    {
	      fft_fil_inr_outc_no_sampling_reduction(tmp, im,ois->im.nx,w_flag,f_c,width);
	      for (i = 0; i< onx ; i++)
		{
		  pd[j].fl[2*i] = tmp[i*no];
		  pd[j].fl[2*i+1] = im[i*no];
		}
	    }
	}
    }
  else
    {
      if (fft_init(ois->im.ny) || filter_init(ois->im.ny)) 
	return (win_printf_OK("Can't initialize fft"));
      no = ois->im.ny/ony;
      w_flag = (!(ois->im.win_flag & Y_PER)) ? 1 : 0;
      for (j=0; j< ois->im.nx; j++)
	{
	  no1 = ony * j;
	  if (no1 >= nout1 + 16384)
	    {
	      draw_bubble(screen,0, 10, 64, "fft row %d",j);			
	      nout1 = no1;
	    }
	  extract_raw_row (ois, j, tmp);
	  if (d_type == 0)  // filter_mode & REAL_MODE
	    {
	      fft_fil_inr_outr(tmp, ois->im.ny,w_flag,f_c,width);
	      for (i = 0; i< ony ; i++) pd[i].fl[j] = tmp[i*no];
	    }
	  else 	if (d_type == 1)
	    {
	      fft_fil_inr_outc(tmp, ois->im.ny,w_flag,f_c,width);
	      for (i = 0; i< ony ; i++)
		{
		  pd[i].fl[2*j] = tmp[i*no];
		  pd[i].fl[2*j+1] = tmp[i*no+1];
		}
	    }
	  else 	if (d_type == 2)
	    {
	      fft_fil_inr_outc_no_sampling_reduction(tmp,im,ois->im.ny,w_flag,f_c,width);
	      for (i = 0; i< ony ; i++)
		{
		  pd[i].fl[2*j] = tmp[i*no];
		  pd[i].fl[2*j+1] = im[i*no];
		}
	    }
	}
    }


  if (tmp) free(tmp);	

  ois->im.win_flag = flag_org;
			
  c1 = (ois->title != NULL) ? ois->title : strdup("no title");
  if (filter_mode & LOW_PASS)		sprintf(c2,"low-pass %d",f_c);
  else if (filter_mode & HIGH_PASS)	sprintf(c2,"high-pass %d",f_c);
  else if (filter_mode & BAND_PASS)	sprintf(c2,"band-pass %d %d",f_c, width);

  set_im_title(oid, "\\stack{{filtred image}{%s in %s}{%s}}", c1, 
	       (index == P_LINE)? "X" : "Y", c2);
	

  set_oi_treatement(oid,"filtred image %s in %s type %s", c1, 
		      (index == P_LINE)? "X" : "Y", c2);	


  inherit_from_im_to_im(oid,ois);
  uns_oi_2_oi(oid,ois);	
  if (index == P_LINE)
    {
      no = ois->im.nx/onx;
      for (i=0; i < oid->n_xu; i++) oid->xu[i]->dx *= no;
    }
  else
    {
      no = ois->im.ny/ony;
      for (i=0; i < oid->n_yu; i++) oid->yu[i]->dx *= no;
    }
  set_oi_x_unit_set(oid, oid->c_xu);
  set_oi_y_unit_set(oid, oid->c_yu);		
  oid->im.win_flag = ois->im.win_flag;
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  imr->cur_oi = imr->n_oi - 1;
  oid->need_to_refresh = ALL_NEED_REFRESH;
  return refresh_image(imr, imr->n_oi - 1);				
}


