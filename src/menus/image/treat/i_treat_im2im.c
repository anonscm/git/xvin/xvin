/********************************************************************************/
/* Math. operations on one or two images					*/
/********************************************************************************/
/* Nicolas Garnier	nicolas.garnier@ens-lyon.fr				*/
/********************************************************************************/
#include "i_treat_im2im.h"
#include "xvin.h"

# include <stdlib.h>
# include <string.h>

#ifndef MAX_TITLE_STRING_SIZE
#define MAX_TITLE_STRING_SIZE 1024
#endif

/********************************************************************************/
/* operations arithmetiques simples sur deux images 				*/
/*	01.1998									*/
/*	11.2003 for Allegro version						*/
/********************************************************************************/
int 	do_opp_on_two_images(void)
{
	register int i, j;             /* intermediaire compteurs de boucle    */
        imreg *imr = NULL;		       /* image region 			       */
	O_i 	*ois1 = NULL, *ois2 = NULL, *oid = NULL;    /* one image source (x2) et destination */
	union pix *ps1 = NULL, *ps2 = NULL, *pd = NULL;     /* data des images (matrice) 	       */
	int	nx1, nx2, nxn;         /* dimensions en x 		       */
	int 	ny1, ny2, nyn;	       /* dimensions en y 		       */
	char  	c1[MAX_TITLE_STRING_SIZE] = {0}, 
	  c2[MAX_TITLE_STRING_SIZE] = {0}; /* chaine pour l'info et l'heritage */
	int 	operation, erreur;
	float	modulus;	

	if(updating_menu_state != 0)	
	{
    	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois1) != 2)  
	  {active_menu->flags |=  D_DISABLED; 		return D_O_K;	}
		else active_menu->flags &= ~D_DISABLED;
    	if (imr->n_oi < 2 || ois1->im.data_type == IS_COMPLEX_IMAGE) 
    		active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
		return D_O_K;	
	} 
	
	operation = active_menu->flags & 0xFFFFFF00;
	
	if (key[KEY_LSHIFT])
   	{	return win_printf_OK("Simple arithmetic on two images.\n\n"
        			"A new image is created.");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois1) != 2)	return win_printf_OK("cannot find data!");
	if (ois1 == NULL || ois1->im.data_type == IS_COMPLEX_IMAGE)		return win_printf_OK("cannot treat complex image");
	if (imr->n_oi < 2)					return win_printf_OK("No enought images to perform that !");

	i = imr->cur_oi; 		/* on recupere le numero de l'image active 	*/
	ps1 = ois1->im.pixel;
	i = ( (i-1 < 0) ? (imr->n_oi-1) : (i-1) );
	ois2 = imr->o_i[i]; 		/* on recupere la deuxieme image 		*/
	ps2 = imr->o_i[i]->im.pixel;	/* on recupere les data de l'image n�i-1 	*/

	if (ps1 == NULL || ps2 == NULL)				return (win_printf("I cannot find any data in image"));

	nx1 = ois1->im.nx;  ny1 = ois1->im.ny; /* dimensions de l'image 1 		*/
	nx2 = ois2->im.nx;  ny2 = ois2->im.ny; /* dimensions de l'image 2 		*/
	nxn = (nx1 > nx2) ? nx2 : nx1;  			/* dimensions minimales (x) 		*/
	nyn = (ny1 > ny2) ? ny2 : ny1;			/* idem 						(y) 		*/

	if (ois1->im.data_type != ois2->im.data_type)
		return win_printf("Images are of different type !\n"
					" This version of the procedure \n cannot operate with them.");

	if (nx1 != nx2)
		i = win_printf("images have different number of colomns %d and %d !\n"
						" I propose to add them over the %d first ones",nx1,nx2,nxn);
	if (i == WIN_CANCEL) return D_O_K;   /* abandon par l'utilisateur */
	if (ny1 != ny2)
		i = win_printf("images have different number of lines %d and %d !\n"
						" I propose to add them over the %d first ones",ny1,ny2,nyn);
	if (i == WIN_CANCEL)	return D_O_K;  /* abandon par l'utilisateur */

	if (operation!=IM_DIVIDE)  { i= (ois1->im.data_type == IS_CHAR_IMAGE) ? IS_INT_IMAGE : ois1->im.data_type; }
		/* si l'image est de type char (entier non signe),
			alors la destination est de type int (entier signe) car
			la soustraction peut faire apparaitre des signes negatifs
			sinon, le type de l'image de destination est le meme que
			celui de l'image de depart */
	else			{ i= (ois1->im.data_type == IS_COMPLEX_IMAGE) ? IS_COMPLEX_IMAGE : IS_FLOAT_IMAGE; }
		/* si l'on procede a une division, le resultat sera en float, sauf si les 
			images de depart sont complexes. */
	oid = create_one_image(nxn ,nyn, i);	/* on cree une image de ce type i 	*/
	pd  = oid->im.pixel;			/* affectations des datas de l 'image 	*/

	if (operation == IM_ADD)           	/* calcul : addition 			*/
	{
		if (ois1->im.data_type == IS_FLOAT_IMAGE)
		{	win_printf("addition of reals. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].fl[j] = ps1[i].fl[j] + ps2[i].fl[j];
				}
		}/* end of addition of reals */
		else if (ois1->im.data_type == IS_COMPLEX_IMAGE)
		{	win_printf("addition of complexes. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{	pd[i].fl[2*j] = ps1[i].fl[2*j] + ps2[i].fl[2*j];
										/* partie reelle */
					pd[i].fl[2*j+1] = ps1[i].fl[2*j+1] + ps2[i].fl[2*j+1];
										/* partie imaginaire */
				}
		}/* end of addition of complexes */
		else if (ois1->im.data_type == IS_INT_IMAGE)
		{	win_printf("addition of integers. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].in[j] = ps1[i].in[j] + ps2[i].in[j];
				}
		}/* end of addition of integers */
		else if (ois1->im.data_type == IS_UINT_IMAGE)
		{	win_printf("addition of integers. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].ui[j] = ps1[i].ui[j] + ps2[i].ui[j];
				}
		}/* end of addition of unsigned integers */
		else if (ois1->im.data_type == IS_LINT_IMAGE)
		{	win_printf("addition of integers. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].li[j] = ps1[i].li[j] + ps2[i].li[j];
				}
		}/* end of addition of long integers */
		else if (ois1->im.data_type == IS_CHAR_IMAGE)
		{	win_printf("addition of chars. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].in[j] = ps1[i].ch[j] + ps2[i].ch[j];
				}
		}/* addition of chars */
	} /* addition case
																		*/
	else if (operation == IM_SUBSTRACT) 	/* soustraction 								*/
	{
		if (ois1->im.data_type == IS_FLOAT_IMAGE)
		{	win_printf("substraction of reals. \n"
							"result will be reals. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].fl[j] = ps1[i].fl[j] - ps2[i].fl[j];
				}
		}/* substraction of reals */
		else if (ois1->im.data_type == IS_COMPLEX_IMAGE)
		{	win_printf("substraction of complexes. \n"
							"result will be complexes. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{	pd[i].fl[2*j] = ps1[i].fl[2*j] - ps2[i].fl[2*j];
										/* partie reelle */
					pd[i].fl[2*j+1] = ps1[i].fl[2*j+1] - ps2[i].fl[2*j+1];
										/* partie imaginaire */
				}
		}/* substraction of complexes */
		else if (ois1->im.data_type == IS_INT_IMAGE)
		{	win_printf("substraction of integers. \n"
							"result will be integers. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].in[j] = ps1[i].in[j] - ps2[i].in[j];
				}
		}/* substraction of integers */
		else if (ois1->im.data_type == IS_UINT_IMAGE)
		{	win_printf("substraction of integers. \n"
							"result will be integers. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].ui[j] = ps1[i].ui[j] - ps2[i].ui[j];
				}
		}/* substraction of unsigned integers */
		else if (ois1->im.data_type == IS_LINT_IMAGE)
		{	win_printf("substraction of integers. \n"
							"result will be integers. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].li[j] = ps1[i].li[j] - ps2[i].li[j];
				}
		}/* substraction of long integers */
		else if (ois1->im.data_type == IS_CHAR_IMAGE)
		{	win_printf("substraction of unsigned chars. \n"
							"result will be integers \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].in[j] = ps1[i].ch[j] - ps2[i].ch[j];
				}
		}/* substraction of chars */
	}/* substraction case */
	else if (operation == IM_MULTIPLY)							/* multiplication */
	{
		if (ois1->im.data_type == IS_FLOAT_IMAGE)
		{	win_printf("multiplication of reals. \n"
							"result will be reals. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].fl[j] = ps1[i].fl[j] * ps2[i].fl[j];
				}
		}/* multiplication of reals */
		else if (ois1->im.data_type == IS_COMPLEX_IMAGE)
		{	win_printf("multiplication of complexes. \n"
							"result will be complexes. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{	pd[i].fl[2*j]   = (ps1[i].fl[2*j]   * ps2[i].fl[2*j])
										 - (ps1[i].fl[2*j+1] * ps2[i].fl[2*j+1]);
											/* partie reelle */
					pd[i].fl[2*j+1] = (ps1[i].fl[2*j]   * ps2[i].fl[2*j+1])
										 + (ps1[i].fl[2*j+1] * ps2[i].fl[2*j]);
											/* partie imaginaire */
				}
		}/* multiplication of complexes */
		else if (ois1->im.data_type == IS_INT_IMAGE)
		{	win_printf("multiplication of integers. \n"
							"result will be integers. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].in[j] = ps1[i].in[j] * ps2[i].in[j];
				}
		}/* multiplication of integers */
		else if (ois1->im.data_type == IS_UINT_IMAGE)
		{	win_printf("multiplication of integers. \n"
							"result will be integers. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].ui[j] = ps1[i].ui[j] * ps2[i].ui[j];
				}
		}/* multiplication of integers */
		else if (ois1->im.data_type == IS_LINT_IMAGE)
		{	win_printf("multiplication of integers. \n"
							"result will be integers. \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].li[j] = ps1[i].li[j] * ps2[i].li[j];
				}
		}/* multiplication of long integers */
		else if (ois1->im.data_type == IS_CHAR_IMAGE)
		{	win_printf("multiplication of unsigned chars. \n"
							"result will be integers \n ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	pd[i].in[j] = ps1[i].ch[j] * ps2[i].ch[j];
				}
		}/* multiplication of chars */
	}

	else if (operation == IM_DIVIDE)   	/* division */
	{  	erreur=0;
		if (ois1->im.data_type == IS_FLOAT_IMAGE)
		{	win_printf("You asked for a division of reals.\n"
				"result will be reals, but division by zero leads to a zero value.\n"
				"ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	if (ps2[i].fl[j] != 0)  { pd[i].fl[j] = ps1[i].fl[j] / ps2[i].fl[j]; }
					else			{ pd[i].fl[j] = 0;
								  erreur++; }
				}
		}/* division of reals */
		else if (ois1->im.data_type == IS_COMPLEX_IMAGE)
		{	win_printf("You asked for a division of complexes.\n"
				"result will be complexes,\n"
				"ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{	modulus = ps2[i].fl[2*j]*ps2[i].fl[2*j] + ps2[i].fl[2*j+1]*ps2[i].fl[2*j+1];
					if (modulus != 0)	
					{ pd[i].fl[2*j]   = (ps1[i].fl[2*j]*ps2[i].fl[2*j] + ps1[i].fl[2*j+1]*ps2[i].fl[2*j+1]) /modulus;
					  pd[i].fl[2*j+1] = (ps1[i].fl[2*j+1]*ps2[i].fl[2*j] - ps1[i].fl[2*j]*ps2[i].fl[2*j+1]) /modulus;
					}
					else 			
					{ pd[i].fl[2*j]   = 0;
					  pd[i].fl[2*j+1] = 0;
					  erreur++; 
					}
				}
		}/* division of complexes */
		else if (ois1->im.data_type == IS_INT_IMAGE)
		{	win_printf("You asked for a division of integers.\n"
				"result will be reals, but division by zero leads to a zero value.\n"
				"ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	if (ps2[i].in[j] != 0)  { pd[i].fl[j] = ps1[i].in[j] / ps2[i].in[j]; }
					else			{ pd[i].fl[j] = 0;
								  erreur++; }
				}
		}/* division of integers */
		else if (ois1->im.data_type == IS_UINT_IMAGE)
		{	win_printf("You asked for a division of integers.\n"
				"result will be reals, but division by zero leads to a zero value.\n"
				"ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	if (ps2[i].ui[j] != 0)  { pd[i].fl[j] = ps1[i].ui[j] / ps2[i].ui[j]; }
					else			{ pd[i].fl[j] = 0;
								  erreur++; }
				}
		}/* division of unsigned integers */
		else if (ois1->im.data_type == IS_LINT_IMAGE)
		{	win_printf("You asked for a division of integers.\n"
				"result will be reals, but division by zero leads to a zero value.\n"
				"ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	if (ps2[i].li[j] != 0)  { pd[i].fl[j] = ps1[i].li[j] / ps2[i].li[j]; }
					else			{ pd[i].fl[j] = 0;
								  erreur++; }
				}
		}/* division of long integers */
		else if (ois1->im.data_type == IS_CHAR_IMAGE)
		{	win_printf("You asked for a division of chars.\n"
				"result will be reals, but division by zero leads to a zero value.\n"
				"ok to perform ?");
			for (i=0; i<nyn; i++)
				for (j=0; j<nxn; j++)
				{ 	if (ps2[i].ch[j] != 0)  { pd[i].fl[j] = ps1[i].ch[j] / ps2[i].ch[j]; }
					else			{ pd[i].fl[j] = 0;
								  erreur++; }
				}
		}/* multiplication of chars */
		if (erreur != 0) { win_printf("Division by zero occurs %d times !",erreur); }
	}

	else
	{	free_one_image(oid);
		return win_printf("Wrong operation between the images !");
	}
	/* win_printf("Operation completed."); */

	if (ois1->im.data_type==IS_CHAR_IMAGE || ois1->im.data_type==IS_INT_IMAGE
	    || ois1->im.data_type==IS_UINT_IMAGE || ois1->im.data_type==IS_LINT_IMAGE)
	{  if ((operation == IM_SUBSTRACT) || (operation == IM_ADD) )
		{	oid->ax = ois1->ax;  /*factor*/
			oid->dx = ois1->dx;  /*offset*/
			oid->ay = ois1->ay;  /*factor*/
			oid->dy = ois1->dy;  /*offset*/
			oid->az = ois1->az;  /*factor*/
			oid->dz = ois1->dz;  /*offset*/
		}
		else if (mn_index == IM_MULTIPLY)
		{	/* idem ? */
		}
	} 	/* dans le cas d'une image d'entiers (signe ou non) on herite
			des facteurs de conversion entre pixels et dimensions */

	/* getting the title of the source image 1, and protection against too long names */
	if ( (ois1->title != NULL) && (strlen(ois1->title) < MAX_TITLE_STRING_SIZE/2) )
	{	strcpy(c1, ois1->title);	}
	else {	if (ois1->im.source != NULL) 
			strcpy(c1, ois1->im.source);
		else	c1[0] = 0;	}
	/* getting the title of the source image 2, and protection against too long names */
	if ( (ois2->title != NULL) && (strlen(ois2->title) < MAX_TITLE_STRING_SIZE/2) )
	{	strcpy(c2, ois2->title);	}
	else {	if (ois2->im.source != NULL) 
			strcpy(c2, ois2->im.source);
		else	c2[0] = 0;	}

	if (operation == IM_ADD)
	{	set_im_title(oid,"\\stack{{{Added images}{%s and %s}}}", c1, c2);
		set_oi_treatement(oid,"Added images %s and %s", c1, c2);
	}
	else if (operation == IM_SUBSTRACT)
	{  set_im_title(oid,"\\stack{ {{Substacted images}{%s by %s}} }", c1, c2);
		set_oi_treatement(oid,"Substacted images %s by %s", c1, c2);
	}
	else if (operation == IM_MULTIPLY)
	{  set_im_title(oid,"\\stack{{Multiplied images}{%s and %s}}", c1, c2);
		set_oi_treatement(oid,"Multiplied images %s and %s", c1, c2);
	}
	else if (operation == IM_DIVIDE)
	{  set_im_title(oid,"\\stack{{Divided images}{%s by %s}}", c1, c2);
		set_oi_treatement(oid,"Divided images %s by %s", c1, c2);
	}
		/* titre et commentaire suivant l'operation effectuee */

	inherit_from_im_to_im(oid,ois1);		/* heritage global des proprietes de l'image 1 		*/
	uns_oi_2_oi(oid,ois1);				/* heritage des unites de l'image 1 			*/
	oid->im.win_flag = ois1->im.win_flag;		/* heritage des boundary condition de l'image 1 	*/
	/* win_printf("inheriting completed."); */

	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	imr->cur_oi = imr->n_oi - 1;
	oid->need_to_refresh = ALL_NEED_REFRESH;				
	broadcast_dialog_message(MSG_DRAW,0);	
	return D_REDRAWME;		

} /* end of the "op_on_two_images" function */









/********************************************************************************/
/* sliding average on one image			 				*/
/*	24.10.2001								*/
/********************************************************************************/
int 	do_sliding_average_y(void)
{
	register int i, j, k;	/* intermediaire compteurs de boucle    */
	imreg 	*imr = NULL;		/* image region 			*/
	O_i 	*ois = NULL, *oid = NULL; 	/* one image source (x2) et destination */
	union pix *ps = NULL, *pd = NULL;     /* data des images (matrice)		*/
	int	nx,ny,Nm=3;
	char  	c1[MAX_TITLE_STRING_SIZE] = {0};

	if(updating_menu_state != 0)	return D_O_K;	
	
	if (key[KEY_LSHIFT])
   	{	return win_printf_OK("Sliding average (filter)\n\n"
        			"A new image is created.");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return win_printf_OK("cannot find data!");
	if (ois == NULL) 	return D_O_K;	
	if ( (ois->im.data_type != IS_FLOAT_IMAGE) && (ois->im.data_type != IS_CHAR_IMAGE) )
								return(win_printf("image can be char or float only !"));
	ps  = ois->im.pixel;	if (ps == NULL)			return (win_printf("I cannot find any data in image"));
	nx = ois->im.nx;  ny = ois->im.ny;

	i=win_scanf("sliding average in the Y direction\n average on %d lines",&Nm);
	if (i==WIN_CANCEL) return(OFF);

	oid = create_one_image(nx ,ny, IS_FLOAT_IMAGE);	pd  = oid->im.pixel;		

	if (ois->im.data_type == IS_CHAR_IMAGE)
	{ for (i=0; i<(int)((Nm-1)/2); i++)
	  {	for (j=0; j<nx; j++) { pd[i].fl[j] = ps[i].ch[j]; } }

	  for (i=(int)((Nm-1)/2); i<ny-(int)((Nm-1)/2); i++)
	  {	for (j=0; j<nx; j++) { pd[i].fl[j] =0;
				       for (k=-(int)(Nm-1)/2; k<=(int)(Nm-1)/2; k++) { pd[i].fl[j] += ps[i+k].ch[j]; }
				       pd[i].fl[j] /= Nm; }
	  }
	  for (i=ny-(int)((Nm-1)/2); i<ny; i++)
	  {	for (j=0; j<nx; j++) { pd[i].fl[j] = ps[i].ch[j]; } }
	}
	else if (ois->im.data_type == IS_FLOAT_IMAGE)
	{ for (i=0; i<(int)((Nm-1)/2); i++)
	  {	for (j=0; j<nx; j++) { pd[i].fl[j] = ps[i].fl[j]; } }

	  for (i=(int)((Nm-1)/2); i<ny-(int)((Nm-1)/2); i++)
	  {	for (j=0; j<nx; j++) { pd[i].fl[j] =0;
				       for (k=-(int)(Nm-1)/2; k<=(int)(Nm-1)/2; k++) { pd[i].fl[j] += ps[i+k].fl[j]; }
				       pd[i].fl[j] /= Nm; }
	  }
	  for (i=ny-(int)((Nm-1)/2); i<ny; i++)
	  {	for (j=0; j<nx; j++) { pd[i].fl[j] = ps[i].fl[j]; } }
	}

		
	/* getting the title of the source image 1, and protection against too long names */
	if ( (ois->title != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE) )
	{	strcpy(c1, ois->title);	}
	else {	if (ois->im.source != NULL) 
			strcpy(c1, ois->im.source);
		else	c1[0] = 0;	}

	set_im_title(oid,"Slid. aver. of %s", c1);
	set_oi_treatement(oid,"Sliding average");


	inherit_from_im_to_im(oid,ois);
	uns_oi_2_oi(oid,ois);	
/*	if (index == P_LINE)
	{
		no = ois->im.nx/onx;
		for (i=0; i < oid->n_xu; i++) oid->xu[i]->dx *= no;
	}
	else if (index == P_COL)
	{
		no = ois->im.ny/ony;
		for (i=0; i < oid->n_yu; i++) oid->yu[i]->dx *= no;
	}
*/	set_oi_x_unit_set(oid, oid->c_xu);
	set_oi_y_unit_set(oid, oid->c_yu);		
	oid->im.win_flag = ois->im.win_flag;

	add_image(imr, oid->type, (void*)oid);
	find_zmin_zmax(oid);
	imr->cur_oi = imr->n_oi - 1;
	oid->need_to_refresh = ALL_NEED_REFRESH;				
	broadcast_dialog_message(MSG_DRAW,0);	
	return D_REDRAWME;		
} /* end of the "sliding_average_y" function */



