/********************************************************************************/
/* functions to perform basic operations on images              */
/********************************************************************************/
/* Vincent Croquette, Nicolas Garnier                       */
/*  nicolas.garnier@ens-lyon.fr                     */
/********************************************************************************/



#include "xvin.h"
#include "i_treat_basic.h"
//#include "../../plot/treat/p_treat_basic.h"

# include <stdlib.h>
# include <string.h>

#ifndef MAX_TITLE_STRING_SIZE
#define MAX_TITLE_STRING_SIZE 1024
#endif

/************************************************************************/
/* procedure used by the procedure "resample_image"             */
/************************************************************************/
int extrapolate_array(float *in, int nxi, float *out, int nxo)
{
    int i;
    float x, y, dx;
    int ix;

    if (in == NULL || nxi <= 0 || nxo <= 0)       return 1;

    if (out == NULL)
    {
        out = (float *)calloc(nxo, sizeof(float));

        if (out == NULL)  return 1;
    }

    for (i = 0; i < nxo; i++)
    {
        x = ((float)(i * nxi)) / nxo;
        ix = (int)x;
        ix = (ix <= 0) ? 1 : ix;
        ix = (ix < nxi - 2) ? ix : nxi - 3;
        dx = x - ix;
        y = -  dx * (dx - 1) * (dx - 2) * in[ix - 1] / 6;
        y += (dx + 1) * (dx - 1) * (dx - 2) * in[ix] / 2;
        y -= (dx + 1) * dx * (dx - 2) * in[ix + 1] / 2;
        y += (dx + 1) * dx * (dx - 1) * in[ix + 2] / 6;
        out[i] = y;
    }

    return 0;
} /* end of the procedure "extrapolate_array" */







/************************************************************************/
/* procedure used by the procedure "resample_image"             */
/************************************************************************/
int fast_resample_array(float *in, int nxi, float *out, int nxo)
{
    int i, j;
    float x, dx;
    static int nxi_old = 0, nxo_old = 0, *ixi = NULL;
    static float *a_1 = NULL, *a0 = NULL, *a1 = NULL, *a2 = NULL;
    int ix;

    if (nxi <= 0 || nxo <= 0)     return 1;

    if (nxo != nxo_old)
    {
        ixi = (int *)realloc(ixi, nxo * sizeof(int));
        a_1 = (float *)realloc(a_1, nxo * sizeof(float));
        a0 = (float *)realloc(a0, nxo * sizeof(float));
        a1 = (float *)realloc(a1, nxo * sizeof(float));
        a2 = (float *)realloc(a2, nxo * sizeof(float));

        if (ixi == NULL || a_1  == NULL || a0 == NULL || a1 == NULL || a2 == NULL)
            return 1;
    }

    if (nxo != nxo_old || nxi != nxi_old)
    {
        for (i = 0; i < nxo; i++)
        {
            x = ((float)(i * nxi)) / nxo;
            ix = (int)x;
            ix = (ix <= 0) ? 1 : ix;
            ix = (ix < nxi - 2) ? ix : nxi - 3;
            ixi[i] = ix;
            dx = x - ix;
            a_1[i] = -(dx * (dx - 1) * (dx - 2)) / 6;
            a0[i] = ((dx + 1) * (dx - 1) * (dx - 2)) / 2;
            a1[i] = -((dx + 1) * dx * (dx - 2)) / 2;
            a2[i] = ((dx + 1) * dx * (dx - 1)) / 6;
        }

        nxi_old = nxi;
        nxo_old = nxo;
    }

    if (in == NULL)       return 0;

    if (out == NULL)
    {
        out = (float *)calloc(nxo, sizeof(float));

        if (out == NULL)  return 1;
    }

    for (i = 0; i < nxo; i++)
    {
        j = ixi[i];
        out[i] = a_1[i] * in[j - 1] + a0[i] * in[j] + a1[i] * in[j + 1] + a2[i] * in[j + 2];
    }

    return 0;
}/* end of the procedure "fast_resample_array" */







/************************************************************************/
/* to resample an image                         */
/************************************************************************/
int do_resample_image(void)
{
    int i, j;
    int   onx, ony;
    char  c[MAX_TITLE_STRING_SIZE], c1[MAX_TITLE_STRING_SIZE];
    int   index;
    float     *tmp = NULL, *tmp1 = NULL;
    union pix *pd = NULL;
    O_i   *ois = NULL, *oid = NULL;
    imreg     *imr = NULL;

    if (updating_menu_state != 0)  return D_O_K;

    index = active_menu->flags & 0xFFFFFF00;

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Resampling an image in X or Y\n\n"
                             "This uses a polynomial extrapolation of each line/row of the image\n"
                             "and changes its number of representative points.\n\n"
                             "The source image is conserved.");
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)  return win_printf_OK("cannot find data!");

    if (ois->im.data_type == IS_COMPLEX_IMAGE)        return win_printf_OK("cannot treat complex image");

    if (index == X_DIRECTION)
    {
        for (i = 2; i < ois->im.nx; i *= 2);

        onx = i;
        ony = ois->im.ny;
        sprintf(c, "resampling along X,\n image has %d points \n new size %%d", ois->im.nx);

        i = win_scanf(c, &onx);

        if (i == WIN_CANCEL)  return OFF;

        tmp = (float *)calloc(ois->im.nx, sizeof(float));

    }
    else if (index == Y_DIRECTION)
    {
        for (i = 2; i < ois->im.ny; i *= 2);

        ony = i;
        onx = ois->im.nx;
        sprintf(c, "resampling along Y,\n image has %d points \n new size %%d", ois->im.ny);

        i = win_scanf(c, &ony);

        if (i == WIN_CANCEL)  return OFF;

        tmp = (float *)calloc(ois->im.ny, sizeof(float));
    }
    else return win_printf_OK("wrong mode!\nthis function is called incorrectly!");

    tmp1 = (float *)calloc(ony, sizeof(float));

    if (tmp == NULL || tmp1 == NULL)
        return win_printf("cannot create tmp array!");

    oid = create_and_attach_oi_to_imr(imr, onx, ony, IS_FLOAT_IMAGE);

    if (oid == NULL)  return win_printf("cannot create destination image!");

    pd = oid->im.pixel;

    if (index == X_DIRECTION)
    {
        for (j = 0; j < ois->im.ny; j++)
        {
            extract_raw_line(ois, j, tmp);
            /*            extrapolate_array(tmp, ois->im.nx, pd[j].fl, onx);  */
            fast_resample_array(tmp, ois->im.nx, pd[j].fl, onx);
        }
    }
    else
    {
        for (j = 0; j < ois->im.nx; j++)
        {
            extract_raw_row(ois, j, tmp);
            /*            extrapolate_array(tmp, ois->im.ny, tmp1, ony);  */
            fast_resample_array(tmp, ois->im.ny, tmp1, ony);

            for (i = 0; i < ony ; i++) pd[i].fl[j] = tmp1[i];
        }
    }

    if (tmp) free(tmp);

    if (tmp1 != NULL) free(tmp1);

    /* getting the title of the source image, and protection against too long names */
    if ((ois->title != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE))
    {
        strcpy(c1, ois->title);
    }
    else
    {
        if ((ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE))
            strcpy(c1, ois->im.source);
        else    c1[0] = 0;
    }



    set_im_title(oid, "\\stack{{resampled image}{%s in %s}}", c1, (index == X_DIRECTION) ? "X" : "Y");
    set_oi_treatement(oid, "resampled image in %s", (index == X_DIRECTION) ? "X" : "Y");

    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);

    if (index == X_DIRECTION)
    {
        for (i = 0; i < oid->n_xu; i++)
            oid->xu[i]->dx *= ((float)ois->im.nx) / onx;
    }
    else
    {
        for (i = 0; i < oid->n_yu; i++)
            oid->yu[i]->dx *= ((float)ois->im.ny) / ony;
    }

    set_oi_x_unit_set(oid, oid->c_xu);
    set_oi_y_unit_set(oid, oid->c_yu);
    oid->im.win_flag = ois->im.win_flag;
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
} /* end of the procedure "do_resample_image" */







/************************************************************************************************/
/* "normalisation" au sens large d'une image en 1 passage                   */
/*  l'image de depart n'est pas modifiee et une deuxieme image est cree         */
/*  25.01.1999                                      */
/************************************************************************************************/
int do_normalize(void)
{
    int i, j;     /* compteurs                        */
    union pix *ps = NULL,  *pd = NULL;      /* peut etre est-ce meiux en float ...          */
    imreg   *imr  = NULL;
    O_i     *ois    = NULL,   /* image source                         */
             *oid    = NULL;     /* image destination                    */
    d_s     *ds_amp = NULL;      /* data set with amplitude profile          */
    int   l_s, l_e;       /* limites de la zone de travail en y           */
    int   nx, ny;         /* intermediares pour la taille de l'image source   */
    float   amp_moy, amp_min, amp_max; /* amplitude moyenne sur l'image          */
    //    char    c1[MAX_TITLE_STRING_SIZE]; /* titre de l'image source               */
    float offset;         /* offset : niveau de gris zero de la camera        */
    static    int bool_divide = 0;    /* should we perform an optional division ?     */
    int   bool_zero = 0;      /* is there a point with zero mean profile ?        */

    if (updating_menu_state != 0)  return D_O_K;

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Calculate the averaged X-profile between \n"
                             "user-supplied lines and substract it from all \n"
                             "the lines of the source image.\n\n"
                             "The resulting image can also be divided by the \n"
                             "average profile (optional).\n\n"
                             "The destination image is of float type.\n\n"
                             "The source image is conserved.");
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)
    {
        return OFF;
    }

    if (imr == NULL)  return (win_printf("I cannot find any image !"));

    if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        return (win_printf("The image is complex !\n I cannot proceed."));
    }

    nx = ois->im.nx;
    ny = ois->im.ny;

    l_s = 0;
    l_e = ny - 1;
    offset = 0;
    i = win_scanf("{\\pt14\\color{yellow}Image normalization}\n\n"
                  "Use lines between %6d and %6d\n"
                  "and a constant offset %8f\n\n"
                  "%b also perform a division", &l_s, &l_e, &offset, &bool_divide);

    if (i == WIN_CANCEL)  return OFF;

    if (l_e <= l_s)   return win_printf("%d <= %d", l_e, l_s);

    ps = ois->im.pixel;   /* on recupere les datas (matrice d'entiers) de l'image */

    oid = create_one_image(nx , ny, IS_FLOAT_IMAGE);

    if (oid == NULL)  return win_printf("I cannot create the destination image!");

    pd  = oid->im.pixel;

    /*
      tmpy = (float*)calloc(ois->im.nx,sizeof(float));
      if ( oid == NULL || tmpy == NULL ) return (win_printf ("Can't create dest image"));
    */
    /* step 1 : calcul du profil moyen en selon x */

    /* ci dessous, constuction du dataset pour le profil d'amplitude moyen selon x */
    ds_amp = build_data_set(nx, nx);

    if (ds_amp == NULL) return (win_printf("Can't create temporary dataset for average X-profile !"));

    ds_amp->nx = nx;
    ds_amp->ny = nx;
    inherit_from_im_to_ds(ds_amp, ois);

    amp_min = 255;
    amp_max = 0;

    /* ci dessous, prise de moyenne mathematique : (image -> plot) pour l'amplitude de l'image : */
    for (i = 0 ; i < nx ; i++)    ds_amp->yd[i] = 0;  /* initialisation a zero */

    for (i = l_s ; i < l_e ; i++)
    {
        extract_raw_line(ois, i, ds_amp->xd);       /* la premiere "colonne" du dataset (qui en a 2) sert de tampon */

        for (j = 0 ; j < ds_amp->nx ; j++)
        {
            ds_amp->yd[j] += ds_amp->xd[j];

            if (ds_amp->xd[j] < amp_min) amp_min = ds_amp->xd[j];

            if (ds_amp->xd[j] > amp_max) amp_max = ds_amp->xd[j];
        }
    }

    for (i = 0, j = l_e - l_s ; i < ds_amp->nx && j != 0 ; i++)
    {
        ds_amp->xd[i] = i;              /* on reconstruit une 1ere colonne potable */
        ds_amp->yd[i] /= j;

        if (ds_amp->yd[i] == 0) bool_zero++;
    }

    if (bool_zero != 0) win_printf_OK("I will {\\color{lightred}NOT} divide by the profile\n"
                                          "(there are %d point(s) with zero amplitude)", bool_zero);

    amp_moy = 0;

    for (i = 0 ; i < nx ; i++)
    {
        amp_moy += ds_amp->yd[i];
    }

    amp_moy /= nx;

    /* step 2 : creation and computation of the destination image */

    for (j = 0; j < ny; j++)
    {
        if (ois->im.data_type == IS_CHAR_IMAGE)
            for (i = 0 ; i < ois->im.nx ; i++)
                pd[j].fl[i] = ps[j].ch[i] - ds_amp->yd[i] + offset;
        else if (ois->im.data_type == IS_INT_IMAGE)
            for (i = 0 ; i < ois->im.nx ; i++)
                pd[j].fl[i] = ps[j].in[i] - ds_amp->yd[i] + offset;
        else if (ois->im.data_type == IS_UINT_IMAGE)
            for (i = 0 ; i < ois->im.nx ; i++)
                pd[j].fl[i] = ps[j].ui[i] - ds_amp->yd[i] + offset;
        else if (ois->im.data_type == IS_LINT_IMAGE)
            for (i = 0 ; i < ois->im.nx ; i++)
                pd[j].fl[i] = ps[j].li[i] - ds_amp->yd[i] + offset;
        else if (ois->im.data_type == IS_FLOAT_IMAGE)
            for (i = 0 ; i < ois->im.nx ; i++)
                pd[j].fl[i] = ps[j].fl[i] - ds_amp->yd[i] + offset;
    }

    if ((bool_divide == 1) && (bool_zero == 0))
    {
        for (j = 0; j < ny; j++)
            for (i = 0; i < nx; i++)
                pd[j].fl[i] /= (ds_amp->yd[i] + offset);
    }

    /* dans le cas d'une image d'entiers (signe ou non) on doit heriter
       des facteurs de conversion entre pixels et dimensions */

    if (ds_amp) free_data_set(ds_amp);        /* on libere les ds intermediaires */

    if (ois->im.data_type == IS_CHAR_IMAGE || ois->im.data_type == IS_INT_IMAGE)
    {
        oid->az = ois->az;
        oid->dz = ois->dz;
    }

    /* step 3  : presentation of the results */
    if (ois->title != NULL) set_im_title(oid, "%s",ois->title);

    set_oi_treatement(oid, "normalized, using lines %d - %d", l_s, l_e);
    inherit_from_im_to_im(oid, ois);          /* heritage global des proprietes de l'image source */
    uns_oi_2_oi(oid, ois);            /* heritage des unites de l'image source */
    oid->im.win_flag = ois->im.win_flag;      /* heritage des boundary condition (periodic or not) de l'image source */
    /*win_printf("inheriting completed."); */
    add_image(imr, oid->type, (void *)oid);       /* on place la nouvelle image dans l'image region */
    find_zmin_zmax(oid);              /* recherche du meilleur contraste pour l'affichage */
    i = refresh_image(imr, imr->n_oi - 1);    /* retour et rafraichissement video */

    win_printf(" minimum amplitude was %f\n maximum amplitude was %f\n averaged amplitude was %f.",
               amp_min, amp_max, amp_moy);

    return (i);

}/* end of "do_normalize" */




/************************************************************************************************/
/* "normalisation" au sens large d'une image en 1 passage                   */
/*  l'image de depart est modifiee, aucune image est cree : optimisation de la memoire) */
/*  04.02.1999                                      */
/*  NG, AC.                                         */
/*                                              */
/************************************************************************************************/
int do_normalize_optimized(void)
{
    int i, j;     /* compteurs                        */
    union pix *ps = NULL;            /* peut etre est-ce meiux en float ...          */
    imreg   *imr  = NULL;
    O_i     *ois    = NULL;   /* image source                         */
    d_s     *ds_amp = NULL;      /* data set with amplitude profile          */
    int   l_s, l_e;       /* limites de la zone de travail en y           */
    int   nx, ny;         /* intermediares pour la taille de l'image source   */
    float   amp_moy, amp_min, amp_max; /* amplitude moyenne sur l'image          */
    char  c1[128] = {0};        /* titre de l'image source              */
    float offset;         /* offset : niveau de gris zero de la camera        */
    int   image_type;     /* type de l'image de depart (char, float ...)      */
    int   image_number;       /* numero de l'image dans l'ir              */
    int   data_len = 4;   /* longueur d'un char/int/float             */
    char *buf = NULL;               /* intermediaire de conversion/realloc            */

    if (updating_menu_state != 0)  return D_O_K;

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Calculate the averaged X-profile between \n"
                             "user-supplied lines and substract it from all \n"
                             "the lines of the source image.\n\n"
                             "The source image is replaced.\n\n"
                             "as of 2003/11/23: this function is buggy... realloc problem ???\n"
                             "my advice : do not use it!");
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)
        return OFF;

    if (imr == NULL)  return (win_printf("I cannot find any image !"));

    if (ois->im.data_type == IS_COMPLEX_IMAGE)
        return (win_printf("The image is complex !\n I cannot proceed."));

    nx = ois->im.nx;
    ny = ois->im.ny;

    l_s    = 0;
    l_e    = ny;
    offset = 0;
    {
        i = win_scanf("Use lines between %d and %d and constant offset %f", &l_s, &l_e, &offset);

        if (i == WIN_CANCEL)    return OFF;

        if (l_e <= l_s)     return win_printf("%d <= %d", l_e, l_s);
    }

    image_type   = ois->im.data_type; /* on recupere le type de l'image active    */
    image_number = imr->cur_oi;       /* on recupere le numero de l'image active  */


    /* step 1 : calcul du profil moyen en selon x, recherche de l'amplitude min et max        */

    /* ci dessous, constuction du dataset pour le profil d'amplitude moyen selon x */
    ds_amp = build_data_set(nx, nx);

    if (ds_amp == NULL) return (win_printf("Can't create temporary dataset for average X-profile !"));

    ds_amp->nx = nx;
    ds_amp->ny = nx;
    inherit_from_im_to_ds(ds_amp, ois);

    amp_min = 255;
    amp_max = 0;

    /* ci dessous, prise de moyenne mathematique : (image -> plot) pour l'amplitude de l'image : */
    for (i = 0 ; i < nx ; i++)    ds_amp->yd[i] = 0;  /* initialisation a zero */

    for (i = l_s ; i < l_e ; i++)
    {
        extract_raw_line(ois, i, ds_amp->xd);       /* la premiere "colonne" du dataset (qui en a 2) sert de tampon */

        for (j = 0 ; j < ds_amp->nx ; j++)
        {
            ds_amp->yd[j] += ds_amp->xd[j];

            if (ds_amp->xd[j] < amp_min) amp_min = ds_amp->xd[j]; /* recherche min */

            if (ds_amp->xd[j] > amp_max) amp_max = ds_amp->xd[j]; /* recherche max */
        }
    }

    for (i = 0, j = l_e - l_s ; i < ds_amp->nx && j != 0 ; i++)
    {
        ds_amp->xd[i] = i;              /* on reconstruit une 1ere colonne potable */
        ds_amp->yd[i] /= j;
    }

    amp_moy = 0;

    for (i = 0 ; i < nx ; i++)
    {
        amp_moy += ds_amp->yd[i];
    }

    amp_moy /= nx;

    /* step 2 : creation and computation of the destination image */
    if (image_type == IS_CHAR_IMAGE)
    {
        data_len = 1;
        buf = (char *) ois->im.pixel[0].ch;
    }
    else if (image_type == IS_INT_IMAGE)
    {
        data_len = 2;
        buf = (char *) ois->im.pixel[0].in;
    }
    else if (image_type == IS_UINT_IMAGE)
    {
        data_len = 2;
        buf = (char *) ois->im.pixel[0].ui;
    }
    else if (image_type == IS_LINT_IMAGE)
    {
        data_len = 4;
        buf = (char *) ois->im.pixel[0].li;
    }
    else if (image_type == IS_FLOAT_IMAGE)
    {
        data_len = 4;
        buf = (char *) ois->im.pixel[0].fl;
    }

    buf = (char *) realloc(buf, nx * ny * sizeof(float));

    if (buf == NULL)
    {
        free_data_set(ds_amp);       /* on libere les ds intermediaires */
        return (win_printf("ERROR : I cannot reallocate buffer !"));
    }

    ps = ois->im.pixel;   /* on recupere les datas (matrice d'entiers) de l'image */
    ois->im.data_type = IS_FLOAT_IMAGE;     /* l'image va etre une image de complexes */

    for ((j = 0); (j < ny); (j++))
    {
        ois->im.pixel[j].fl = (float *)buf;
        buf = buf + nx * 4;
    }

    buf = buf - ny * nx * 4;
    buf = buf + data_len * nx * ny - 1 ; /* le 10 fevrier 1999
                      rajout de ce 1 pour eviter un decalage dans l'indexation de buf
                      en effet, une difference entre les deux procedures de soustraction
                      etait visible. */


    for ((j = ny - 1); (j >= 0); (j--))
    {
        if (image_type == IS_CHAR_IMAGE)
        {
            for ((i = nx - 1); (i >= 0); (i--))
            {
                ps[j].fl[i] = (float) * ((unsigned char *)buf) - ds_amp->yd[i] + offset;
                buf--;
            }
        }
        else if (image_type == IS_INT_IMAGE)
        {
            for ((i = nx - 1); (i >= 0); (i--))
            {
                ps[j].fl[i] = (float) * ((short int *)buf);
                buf -= data_len;              /* with data_len = 2 ! */
            }
        }
        else if (image_type == IS_UINT_IMAGE)
        {
            for ((i = nx - 1); (i >= 0); (i--))
            {
                ps[j].fl[i] = (float) * ((unsigned short int *)buf);
                buf -= data_len;
            }
        }
        else if (image_type == IS_LINT_IMAGE)
        {
            for ((i = nx - 1); (i >= 0); (i--))
            {
                ps[j].fl[i] = (float) * ((int *)buf);
                buf -= data_len;
            }
        }
        else if (image_type == IS_FLOAT_IMAGE)
            for ((i = nx - 1); (i >= 0); (i--))
                ps[j].fl[i] = ps[j].fl[i] - ds_amp->yd[i] + offset;
    }

    free_data_set(ds_amp);        /* on libere les ds intermediaires */


    /* step 3  : presentation of the results */
    /* getting the title of the source image, and protection against too long names */
    if ((ois->title != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE))
    {
        strcpy(c1, ois->title);
    }
    else
    {
        if ((ois->im.source != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE))
            strcpy(c1, ois->im.source);
        else    c1[0] = 0;
    }

    set_im_title(ois, "\\stack{{image %s}{normalized auto with lines %d to %d}}", c1, l_s, l_e);
    set_oi_treatement(ois, "normalized auto lines %d - %d", l_s, l_e);

    find_zmin_zmax(ois);          /* recherche du meilleur contraste pour l'affichage */
    i = refresh_image(imr, image_number); /* retour et rafraichissement video */
    win_printf(" minimum amplitude was %f\n maximum amplitude was %f\n "
               "averaged amplitude was %f.", amp_min, amp_max, amp_moy);
    return (i);
}
/* end of "do_normalize_optimized" */




/********************************************************************************/
/* multiplication of an entire image by a phase exp( i (kx*x + ky*y))       */
/*  where x and y are the image coordinates (space and time)        */
/*  and kx, ky are user-supplied                        */
/*  NG  25.02.1999                          */
/********************************************************************************/
int     do_im_phase_multiplication(void)
{
    int i, j;             /* intermediaire compteurs de boucle    */
    imreg     *imr = NULL;           /* image region             */
    O_i   *ois = NULL,  *oid = NULL;        /* one image source and destination     */
    union     pix *ps = NULL, *pd = NULL;       /* data des images (matrice)            */
    int   nx, ny;             /* dimensions en x, y           */
    char      c1[128] = {0};            /* chaine pour l'info et l'heritage     */
    float phase, kx = 0, ky = 0; /* the local phase factor */

    if (updating_menu_state != 0)  return D_O_K;

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine multiplies an image by a phase\n"
                             "exp( i (k_x x + k_y y) )\n"
                             "with user-supplied k_x and k_y\n\n"
                             "A new image is created.");
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)
        return OFF;

    ps = ois->im.pixel;   /* getting the image n�i data           */

    if (ps == NULL) return (win_printf("I cannot find any data in image"));

    nx = ois->im.nx;
    ny = ois->im.ny; /* image dimensions                */

    if (ois->im.data_type != IS_COMPLEX_IMAGE)
        return (win_printf("I need a complex image !"));

    i = win_scanf("Multiplication by a local phase \\phi (x,y) = k_x x + k_y y\n  k_x = %f  k_y = %f (k_x and k_y may be reals)",
                  &kx, &ky);

    if (i == WIN_CANCEL)  return OFF;

    oid = create_one_image(nx , ny, IS_COMPLEX_IMAGE); /* creating the destination image */

    if (oid == NULL)  return win_printf("I cannot create the destination image!");

    pd  = oid->im.pixel;

    /* mathematical part : */
    if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        for (i = 0; i < ny; i++)
            for (j = 0; j < nx; j++)
            {
                phase = 2 * M_PI * ((kx * j) / nx + (ky * i) / ny);
                pd[i].fl[2 * j]   = ps[i].fl[2 * j] * cos(phase)  -  ps[i].fl[2 * j + 1] * sin(phase);
                /* partie reelle */
                pd[i].fl[2 * j + 1] = ps[i].fl[2 * j] * sin(phase)  +  ps[i].fl[2 * j + 1] * cos(phase);
                /* partie imaginaire */
            }
    }
    else
    {
        for (i = 0; i < ny; i++)
            for (j = 0; j < nx; j++)
            {
                phase = (kx * j) / nx + (ky * i) / ny;
                pd[i].fl[2 * j]   = ps[i].fl[j] * cos(phase);
                /* partie reelle */
                pd[i].fl[2 * j + 1] = ps[i].fl[j] * sin(phase);
                /* partie imaginaire */
            }
    }

    /* end of mathematical part */

    /* getting the title of the source image, and protection against too long names */
    if ((ois->title != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE))
    {
        strcpy(c1, ois->title);
    }
    else
    {
        if ((ois->im.source != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE))
            strcpy(c1, ois->im.source);
        else    c1[0] = 0;
    }

    set_im_title(oid, "multiplied by phase with (kx=%f, ky=%f)", kx, ky);
    set_oi_treatement(oid, "multiplied by phase with (kx=%f, ky=%f)", kx, ky);

    inherit_from_im_to_im(oid, ois);      // transfer the filename and directory
    set_oi_x_unit_set(oid, oid->c_xu);
    set_oi_y_unit_set(oid, oid->c_yu);
    oid->im.win_flag = ois->im.win_flag;
    add_image(imr, oid->type, (void *)oid);
    find_zmin_zmax(oid);
    imr->cur_oi = imr->n_oi - 1;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    broadcast_dialog_message(MSG_DRAW, 0);
    return D_REDRAWME;

} /* end of the "do_im_phase_multiplication" function */





/********************************************************************************/
/* decalage des lignes d'une image pour se placer dans un autre referentiel */
/*      25.02.1999                          */
/*  aucune image n'est cree : on travaille avec the old one         */
/********************************************************************************/
int     do_im_decalage(void)
{
    int i = 0, j;            /* intermediaire compteurs de boucle    */
    imreg     *imr = NULL;               /* image region             */
    O_i   *ois = NULL;           /* one image source and destination     */
    union     pix *ps = NULL;        /* data des images (matrice)            */
    int   nx, ny;             /* dimensions en x, y           */
    char      c1[MAX_TITLE_STRING_SIZE] = {0}; /* chaine pour l'info et l'heritage     */
    float *tmp = NULL;           /* the local phase factor */
    float nd = 0, nd_tmp;
    int   dec_i, k,
          offset = 0, offset_tmp; /* intermediaires   */

    if (updating_menu_state != 0)  return D_O_K;

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routines changes the referential of a spatio-temporal diagram\n"
                             "by shifting lines accordingly\n\n"
                             "User gives the number of lines needed for 2 \\pi -shift.\n"
                             "A constant shift (offset) is possible.\n"
                             "The source image is worked on.");
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)
        return OFF;

    ps = imr->o_i[i]->im.pixel;   /* getting the image n�i data           */

    if (ps == NULL) return (win_printf("I cannot find any data in image"));

    nx = ois->im.nx;
    ny = ois->im.ny; /* image dimensions                */


    offset_tmp = offset;
    nd_tmp = nd;

    i = win_scanf("Referential change\n Enter the number of lines for a 2 \\pi rotation\n n = %f Enter the offset %d",
                  &nd_tmp, &offset_tmp);

    if (i == WIN_CANCEL)  return OFF;

    offset = offset_tmp - offset;

    if (nd_tmp == 0)     nd = -nd;                /* reste ou retourne � l'�tat d'origine */
    else if (nd == 0) nd = nd_tmp;            /* premier d�calage */
    else          nd = 1 / (1 / (nd_tmp) - 1 / (nd)); /* cas g�n�ral */

    if (ois->im.data_type == IS_CHAR_IMAGE)
    {
        tmp = (float *)calloc(nx, sizeof(char));
    }
    else if (ois->im.data_type == IS_INT_IMAGE)
    {
        tmp = (float *)calloc(nx, sizeof(int));
    }
    else if (ois->im.data_type == IS_UINT_IMAGE)
    {
        tmp = (float *)calloc(nx, sizeof(int));
    }
    else if (ois->im.data_type == IS_LINT_IMAGE)
    {
        tmp = (float *)calloc(nx, sizeof(int));
    }
    else if (ois->im.data_type == IS_FLOAT_IMAGE)
    {
        tmp = (float *)calloc(nx, sizeof(float));
    }
    else if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        tmp = (float *)calloc(2 * nx, sizeof(float));
    }
    else return (win_printf("bad image type ?"));

    /* mathematical part : */
    if (ois->im.data_type == IS_CHAR_IMAGE)
    {
        for (i = 0; i < ny; i++)
        {
            if (nd != 0)
            {
                dec_i  = (int)(i * nx / nd) + offset;
            }
            else
            {
                dec_i  = offset;
            }

            while (dec_i >= nx)
            {
                dec_i -= nx;
            }

            while (dec_i < 0)
            {
                dec_i += nx;
            }

            for (j = 0; j < nx; j++) tmp[j] = ps[i].ch[j];

            k = 0;

            for (j = 0; j < nx; j++)
            {
                if (j + dec_i >= nx)
                {
                    k = 1;
                }
                else
                {
                    k = 0;
                }

                ps[i].ch[j + dec_i - k * nx] = tmp[j];
            }
        }
    }

    if (ois->im.data_type == IS_INT_IMAGE)
    {
        for (i = 0; i < ny; i++)
        {
            if (nd != 0)
            {
                dec_i  = (int)(i * nx / nd) + offset;
            }
            else
            {
                dec_i  = offset;
            }

            while (dec_i >= nx)
            {
                dec_i -= nx;
            }

            while (dec_i < 0)
            {
                dec_i += nx;
            }

            for (j = 0; j < nx; j++) tmp[j] = ps[i].in[j];

            k = 0;

            for (j = 0; j < nx; j++)
            {
                if (j + dec_i >= nx)
                {
                    k = 1;
                }
                else
                {
                    k = 0;
                }

                ps[i].in[j + dec_i - k * nx] = tmp[j];
            }
        }
    }

    if (ois->im.data_type == IS_UINT_IMAGE)
    {
        for (i = 0; i < ny; i++)
        {
            if (nd != 0)
            {
                dec_i  = (int)(i * nx / nd) + offset;
            }
            else
            {
                dec_i  = offset;
            }

            while (dec_i >= nx)
            {
                dec_i -= nx;
            }

            while (dec_i < 0)
            {
                dec_i += nx;
            }

            for (j = 0; j < nx; j++) tmp[j] = ps[i].ui[j];

            k = 0;

            for (j = 0; j < nx; j++)
            {
                if (j + dec_i >= nx)
                {
                    k = 1;
                }
                else
                {
                    k = 0;
                }

                ps[i].ui[j + dec_i - k * nx] = tmp[j];
            }
        }
    }

    if (ois->im.data_type == IS_LINT_IMAGE)
    {
        for (i = 0; i < ny; i++)
        {
            if (nd != 0)
            {
                dec_i  = (int)(i * nx / nd) + offset;
            }
            else
            {
                dec_i  = offset;
            }

            while (dec_i >= nx)
            {
                dec_i -= nx;
            }

            while (dec_i < 0)
            {
                dec_i += nx;
            }

            for (j = 0; j < nx; j++) tmp[j] = ps[i].li[j];

            k = 0;

            for (j = 0; j < nx; j++)
            {
                if (j + dec_i >= nx)
                {
                    k = 1;
                }
                else
                {
                    k = 0;
                }

                ps[i].li[j + dec_i - k * nx] = tmp[j];
            }
        }
    }

    if (ois->im.data_type == IS_FLOAT_IMAGE)
    {
        for (i = 0; i < ny; i++)
        {
            if (nd != 0)
            {
                dec_i  = (int)(i * nx / nd) + offset;
            }
            else
            {
                dec_i  = offset;
            }

            while (dec_i >= nx)
            {
                dec_i -= nx;
            }

            while (dec_i < 0)
            {
                dec_i += nx;
            }

            for (j = 0; j < nx; j++) tmp[j] = ps[i].fl[j];

            k = 0;

            for (j = 0; j < nx; j++)
            {
                if (j + dec_i >= nx)
                {
                    k = 1;
                }
                else
                {
                    k = 0;
                }

                ps[i].fl[j + dec_i - k * nx] = tmp[j];
            }
        }
    }

    if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        for (i = 0; i < ny; i++)
        {
            if (nd != 0)
            {
                dec_i  = (int)(i * nx / nd) + offset;
            }
            else
            {
                dec_i  = offset;
            }

            while (dec_i >= nx)
            {
                dec_i -= nx;
            }

            while (dec_i < 0)
            {
                dec_i += nx;
            }

            for (j = 0; j < 2 * nx; j++) tmp[j] = ps[i].fl[j];

            k = 0;

            for (j = 0; j < nx; j++)
            {
                if (j + dec_i >= nx)
                {
                    k = 1;
                }
                else
                {
                    k = 0;
                }

                ps[i].fl[2 * (j + dec_i - k * nx)]   = tmp[2 * j];
                ps[i].fl[2 * (j + dec_i - k * nx) + 1] = tmp[2 * j + 1];
            }
        }
    }

    /* end of mathematical part */

    if (tmp) free(tmp);

    /* getting the title of the source image, and protection against too long names */
    if ((ois->title != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE))
    {
        strcpy(c1, ois->title);
    }
    else
    {
        if ((ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE))
            strcpy(c1, ois->im.source);
        else    c1[0] = 0;
    }

    set_im_title(ois, "%s shifted with %.1f and %d", c1, nd_tmp, offset_tmp);
    ois->im.treatement = my_sprintf(ois->im.treatement, "shifted with %.1f and %d", nd_tmp, offset_tmp);


    ois->need_to_refresh = ALL_NEED_REFRESH;
    broadcast_dialog_message(MSG_DRAW, 0);
    return D_REDRAWME;
} /* end of the "do_im_decalage" function */



O_i *cut_image_in_slice_to_movie(O_i *ois, int sizex, int sizey, int xfirst)
{
    int i, Nx, Ny, ix, iy, nf;
    O_i *oid = NULL;

    if (ois == NULL || sizex < 0 || sizex > ois->im.nx || sizey < 0 || sizey > ois->im.ny)
        return NULL;
    Nx = ois->im.nx/sizex;
    Nx += (ois->im.nx%sizex) ? 1 : 0;
    Ny = ois->im.ny/sizey;
    Ny += (ois->im.ny%sizey) ? 1 : 0;
    nf = Nx * Ny;
    oid = create_one_movie(sizex, sizey, ois->im.data_type, nf);
    if (oid == NULL) return NULL;
    for (i = 0; i < nf; i++)
        {
            switch_frame(oid,i);
            ix = (xfirst) ? i%Nx : i/Ny;
            iy = (xfirst) ? i/Nx : i%Ny;
            copy_one_subset_of_image_in_a_bigger_one(oid, 0, 0, ois, ix*sizex, iy*sizey, sizex, sizey);
        }
    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);
    return oid;
}


int do_cut_image_in_slice_to_movie(void)
{
    int i;
    imreg *imr = NULL;
    O_i *ois = NULL, *oid = NULL;
    static int sizex = 128, sizey = 128, xfirst = 1;

    if (updating_menu_state != 0)  return D_O_K;

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Calculate an over simplified version of the source image\n"
                             "by binarizing it according to user-specified thresholds\n\n"
                             "The destination image is of integer type !");
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return OFF;
    if (imr == NULL)                  return (win_printf("I cannot find any image !"));

    i = win_scanf("Cut image in small ones and dump them in a movie\n"
                  "Size in x %4d; size in y %4d; %b->scan X first\n"
                  , &sizex, &sizey, &xfirst);

    if (i == WIN_CANCEL)  return OFF;
    oid = cut_image_in_slice_to_movie(ois, sizex, sizey, xfirst);
    if (oid == NULL) return 0;
    add_image(imr, oid->type, (void *)oid);
    find_zmin_zmax(oid);
    imr->cur_oi = imr->n_oi - 1;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    broadcast_dialog_message(MSG_DRAW, 0);
    return D_REDRAWME;
}

/************************************************************************************************/
/* "binarisation" d'une image                                   */
/*  l'image de depart n'est pas modifiee et une deuxieme image est creee            */
/*  07.07.2000                                      */
/************************************************************************************************/
int do_binarize(void)
{
    int i, j;     /* compteurs                        */
    union pix *ps = NULL, *pd = NULL;
    float *x = NULL;         /* tmp array if image is complex            */
    imreg   *imr  = NULL;
    O_i     *ois    = NULL,   /* image source                         */
             *oid    = NULL;     /* image destination                    */
    static float v_min = 1, v_max = 1; /* limites de la zone de travail en z           */
    int   d = 0, what = 0; /* what : boolean                   */
    int   nx, ny;         /* intermediaires pour la taille de l'image source  */
    char  c1[MAX_TITLE_STRING_SIZE] = {0}; /* titre de l'image source               */

    if (updating_menu_state != 0)  return D_O_K;

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("Calculate an over simplified version of the source image\n"
                             "by binarizing it according to user-specified thresholds\n\n"
                             "The destination image is of integer type !");
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return OFF;

    if (imr == NULL)                  return (win_printf("I cannot find any image !"));

    if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        i = win_scanf("The image is complex !\n proceed on displayed part (0)\n"
                      " or on modulus (1) ? %d I will create a new image (integers)", &what);

        if (i == WIN_CANCEL) return (i);
    }

    nx = ois->im.nx;
    ny = ois->im.ny;

    i = win_scanf("set to +1 points with value greater than %f and set to %d points with value lower than %f"
                  "all other points will be set to 0", &v_max, &d, &v_min);

    if (i == WIN_CANCEL)  return OFF;

    if (v_max < v_min)    return win_printf("%f < %f", v_max, v_min);

    x  = (float *)calloc(ois->im.nx, sizeof(float));

    if (x == NULL)      return win_printf("I cannot create temporary array !");

    ps = ois->im.pixel;
    oid = create_one_image(nx , ny, IS_INT_IMAGE);

    if (oid == NULL)  return win_printf("I cannot create the destination image!");

    pd  = oid->im.pixel;


    /* step 2 : creation and computation of the destination image */

    for (j = 0; j < ny; j++)
    {
        if (ois->im.data_type == IS_CHAR_IMAGE)
            for (i = 0 ; i < ois->im.nx ; i++)
            {
                if (ps[j].ch[i] >= v_max) pd[j].in[i] = +1;
                else    if (ps[j].ch[i] < v_min) pd[j].in[i] = d;
                else pd[j].in[i] = 0;
            }
        else if (ois->im.data_type == IS_INT_IMAGE)
            for (i = 0 ; i < ois->im.nx ; i++)
            {
                if (ps[j].in[i] >= v_max) pd[j].in[i] = +1;
                else    if (ps[j].in[i] < v_min) pd[j].in[i] = d;
                else pd[j].in[i] = 0;
            }
        else if (ois->im.data_type == IS_UINT_IMAGE)
            for (i = 0 ; i < ois->im.nx ; i++)
            {
                if (ps[j].ui[i] >= v_max) pd[j].in[i] = +1;
                else    if (ps[j].ui[i] < v_min) pd[j].in[i] = d;
                else pd[j].in[i] = 0;
            }
        else if (ois->im.data_type == IS_LINT_IMAGE)
            for (i = 0 ; i < ois->im.nx ; i++)
            {
                if (ps[j].li[i] >= v_max) pd[j].in[i] = +1;
                else    if (ps[j].li[i] < v_min) pd[j].in[i] = d;
                else pd[j].in[i] = 0;
            }
        else if (ois->im.data_type == IS_FLOAT_IMAGE)
            for (i = 0 ; i < ois->im.nx ; i++)
            {
                if (ps[j].fl[i] >= v_max) pd[j].in[i] = +1;
                else    if (ps[j].fl[i] < v_min) pd[j].in[i] = d;
                else pd[j].in[i] = 0;
            }
        else if (ois->im.data_type == IS_COMPLEX_IMAGE)
        {
            if (what == 0) extract_raw_line(ois, j, x);
            else for (i = 0; i < ois->im.nx; i++)
                    x[i] = sqrt(ps[j].fl[2 * i] * ps[j].fl[2 * i] + ps[j].fl[2 * i + 1] * ps[j].fl[2 * i + 1]);

            for (i = 0 ; i < ois->im.nx ; i++)
            {
                if (x[i] >= v_max) pd[j].in[i] = +1;
                else  if (x[i] < v_min) pd[j].in[i] = d;
                else pd[j].in[i] = 0;
            }
        }
    }

    if (ois->im.data_type == IS_CHAR_IMAGE || ois->im.data_type == IS_INT_IMAGE
            || ois->im.data_type == IS_UINT_IMAGE || ois->im.data_type == IS_LINT_IMAGE)
    {
        oid->az = ois->az;
        oid->dz = ois->dz;
    }

    if (ois->im.data_type == IS_COMPLEX_IMAGE && x != NULL) free(x);

    /* step 3  : presentation of the results */
    /* getting the title of the source image, and protection against too long names */
    if ((ois->title != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE))
    {
        strcpy(c1, ois->title);
    }
    else
    {
        if ((ois->im.source != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE))
            strcpy(c1, ois->im.source);
        else    c1[0] = 0;
    }

    set_im_title(oid, "%s threshold at %g", c1, v_max);
    set_oi_treatement(oid, "thresheld");

    inherit_from_im_to_im(oid, ois);      // transfer the filename and directory
    set_oi_x_unit_set(oid, oid->c_xu);
    set_oi_y_unit_set(oid, oid->c_yu);
    oid->im.win_flag = ois->im.win_flag;

    add_image(imr, oid->type, (void *)oid);
    find_zmin_zmax(oid);
    imr->cur_oi = imr->n_oi - 1;
    oid->need_to_refresh = ALL_NEED_REFRESH;
    broadcast_dialog_message(MSG_DRAW, 0);
    return D_REDRAWME;
}/* end of "do_binarize" */






/* To crop an image to the visible portion of it that is visible on the screen */
int do_copy_screen_to_image(void)
{
    int i;
    static int x0 = 0, y0 = 0, x1 = 256, y1 = 256;
    O_i  *oid = NULL;
    imreg *imr = NULL;

    if (updating_menu_state != 0)  return D_O_K;
    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("To convert a screen piece in  an image \n\n"
                             "A new image is created");
    }

    if (ac_grep(cur_ac_reg, "%im", &imr) != 1)      return OFF;

    if (imr == NULL)                  return win_printf_OK("I cannot find any image !");


    i = win_scanf("Define the Scrren area to copy:\n"
                  "X0 = %4d Y0 = %4d\n"
                  "X1 = %4d Y1 = %4d\n",&x0, &y0, &x1, &y1);
    if (i == WIN_CANCEL) return 0;

    x0 = (x0 < 0) ? 0 : x0;
    y0 = (y0 < 0) ? 0 : y0;
    x1 = (x1 <= screen->w) ? x1 : screen->w;
    y1 = (y1 <= screen->h) ? y1 : screen->h;

    oid = convert_bitmap_to_oi(screen, NULL, x0, y0, x1, y1);
    if (oid == NULL)      return (win_printf("Can't create dest image"));

    smart_map_pixel_ratio_of_image_and_screen(oid);
    set_zmin_zmax_values(oid, 0, 255);
    set_z_black_z_white_values(oid, 0, 255);

    set_im_title(oid, "Screen copy (%d,%d,%d,%d)",x0, y0, x1, y1);
    add_image(imr, oid->type, (void *)oid);
    return (refresh_image(imr, imr->n_oi - 1));
}





/* To crop an image to the visible portion of it that is visible on the screen */
int do_keep_visible_image(void)
{
    int i, j, im;
    un_s *un = NULL;
    int onx, ony, xs, ys, nfi;
    union pix *ps = NULL, *pd = NULL;
    O_i *ois = NULL, *oid = NULL;
    imreg *imr = NULL;

    if (updating_menu_state != 0)  return D_O_K;

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("To crop an image to the visible portion of it that is visible on the screen\n\n"
                             "A new image is created");
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return OFF;

    if (imr == NULL)                  return win_printf_OK("I cannot find any image !");

    onx = ois->im.nxe - ois->im.nxs;
    xs  = ois->im.nxs;
    ony = ois->im.nye - ois->im.nys;
    ys  = ois->im.nys;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;

    if (onx <= 0 || ony <= 0) return win_printf_OK("empty image ??");

    if (nfi < 2) oid = create_one_image(onx, ony, ois->im.data_type);
    else         oid = create_one_movie(onx, ony, ois->im.data_type, nfi);

    if (oid == NULL)      return (win_printf("Can't create dest image"));

    for (im = 0; im < nfi; im++)
    {
        if (nfi > 1)
        {
            switch_frame(ois, im);
            switch_frame(oid, im);
        }

        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < oid->im.nx ; i++)
        {
            if (ois->im.data_type == IS_CHAR_IMAGE)
                for (j = 0; j < oid->im.ny; j++)
                    pd[j].ch[i] = ps[ys + j].ch[xs + i];
            else if (ois->im.data_type == IS_INT_IMAGE)
                for (j = 0; j < oid->im.ny; j++)
                    pd[j].in[i] = ps[ys + j].in[xs + i];
            else if (ois->im.data_type == IS_UINT_IMAGE)
                for (j = 0; j < oid->im.ny; j++)
                    pd[j].ui[i] = ps[ys + j].ui[xs + i];
            else if (ois->im.data_type == IS_LINT_IMAGE)
                for (j = 0; j < oid->im.ny; j++)
                    pd[j].li[i] = ps[ys + j].li[xs + i];
            else if (ois->im.data_type == IS_FLOAT_IMAGE)
                for (j = 0; j < oid->im.ny; j++) pd[j].fl[i] = ps[ys + j].fl[xs + i];
            else if (ois->im.data_type == IS_COMPLEX_IMAGE)
            {
                for (j = 0; j < oid->im.ny; j++)
                {
                    pd[j].fl[2 * i] = ps[ys + j].fl[2 * (xs + i)];
                    pd[j].fl[2 * i + 1] = ps[ys + j].fl[2 * (xs + i) + 1];
                }
            }
        }
    }

    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);

    for (i = 0 ; i < oid->n_xu ; i++)
        oid->xu[i]->ax += xs * oid->xu[i]->dx;

    for (i = 0 ; i < oid->n_yu ; i++)
        oid->yu[i]->ax += ys * oid->yu[i]->dx;

    un = build_unit_set(0, xs, 1, 0, 0, NULL);

    if (un == NULL)       return D_O_K;

    add_to_one_image(oid, IS_X_UNIT_SET, (void *)un);
    un = build_unit_set(0, ys, 1, 0, 0, NULL);

    if (un == NULL)       return D_O_K;

    add_to_one_image(oid, IS_Y_UNIT_SET, (void *)un);
    oid->c_xu = oid->n_xu - 1;
    oid->c_yu = oid->n_yu - 1;
    oid->im.win_flag = ois->im.win_flag;

    if (ois->title != NULL)   set_im_title(oid, "%s", ois->title);

    set_oi_treatement(oid, "cropped image");
    add_image(imr, oid->type, (void *)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}




/* To crop an image to the visible portion of it that is visible on the screen */
int do_set_pixels_to_value_and_keep_visible_image(void)
{
    int i, j, im;
    int onx, ony, xs, ys, nfi;
    static int single = 1;
    static float val = 10;
    union pix *ps = NULL, *pd = NULL;
    O_i *ois = NULL, *oid = NULL;
    imreg *imr = NULL;
    char question[256] = {0};

    if (updating_menu_state != 0)  return D_O_K;

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("To crop an image to the visible portion of it that is visible on the screen\n\n"
                             "A new image is created");
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return OFF;

    if (imr == NULL)                  return win_printf_OK("I cannot find any image !");


    onx = ois->im.nxe - ois->im.nxs;
    xs  = ois->im.nxs;
    ony = ois->im.nye - ois->im.nys;
    ys  = ois->im.nys;
    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;

    if (onx == ois->im.nx && ony == ois->im.ny)
        return win_printf_OK("The ROI is equal to the entire image nothing to be done ??");

    if (onx <= 0 || ony <= 0) return win_printf_OK("empty image ??");

    if (nfi > 1)
    {
        snprintf(question, sizeof(question),
                 "Do you want to treat all the %d frames\n"
                 "of the movie->%%R or just the current frame->%%r (%d)", nfi, ois->im.c_f);
        i = win_scanf(question, &val, &single);

        if (i == WIN_CANCEL) return D_O_K;
    }
    else
    {
        i = win_scanf("Specify the value to set to invisible pixels %6f\n", &val);

        if (i == WIN_CANCEL) return D_O_K;
    }

    if (single || nfi < 2) oid = create_one_image(ois->im.nx, ois->im.ny, ois->im.data_type);
    else                   oid = create_one_movie(ois->im.nx, ois->im.ny, ois->im.data_type, nfi);

    if (oid == NULL)      return (win_printf("Can't create dest image"));

    for (im = 0; im < nfi; im++)
    {
        if (single == 0 && nfi > 1)
        {
            switch_frame(ois, im);
            switch_frame(oid, im);
        }

        set_all_pixel_to_color(oid, (int)(val + 0.5), (double)val);

        for (i = xs, ps = ois->im.pixel, pd = oid->im.pixel; i < ois->im.nxe ; i++)
        {
            if (ois->im.data_type == IS_CHAR_IMAGE)
                for (j = ys; j < ois->im.nye; j++)
                    pd[j].ch[i] = ps[j].ch[i];
            else if (ois->im.data_type == IS_INT_IMAGE)
                for (j = ys; j < ois->im.nye; j++)
                    pd[j].in[i] = ps[j].in[i];
            else if (ois->im.data_type == IS_UINT_IMAGE)
                for (j = ys; j < ois->im.nye; j++)
                    pd[j].ui[i] = ps[j].ui[i];
            else if (ois->im.data_type == IS_LINT_IMAGE)
                for (j = ys; j < ois->im.nye; j++)
                    pd[j].li[i] = ps[j].li[i];
            else if (ois->im.data_type == IS_FLOAT_IMAGE)
                for (j = ys; j < ois->im.nye; j++)
                    pd[j].fl[i] = ps[j].fl[i];
            else if (ois->im.data_type == IS_COMPLEX_IMAGE)
                for (j = ys; j < ois->im.nye; j++)
                    pd[j].cp[i] = ps[j].cp[i];
            else if (ois->im.data_type == IS_DOUBLE_IMAGE)
                for (j = ys; j < ois->im.nye; j++)
                    pd[j].db[i] = ps[j].db[i];
            else if (ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
                for (j = ys; j < ois->im.nye; j++)
                    pd[j].dcp[i] = ps[j].dcp[i];
            else if (ois->im.data_type == IS_RGB16_PICTURE)
                for (j = ys; j < ois->im.nye; j++)
                    pd[j].rgb16[i] = ps[j].rgb16[i];
            else if (ois->im.data_type == IS_RGBA16_PICTURE)
                for (j = ys; j < ois->im.nye; j++)
                    pd[j].rgba16[i] = ps[j].rgba16[i];
            else if (ois->im.data_type == IS_RGB_PICTURE)
                for (j = ys; j < ois->im.nye; j++)
                    pd[j].rgb[i] = ps[j].rgb[i];
            else if (ois->im.data_type == IS_RGBA_PICTURE)
                for (j = ys; j < ois->im.nye; j++)
                    pd[j].rgba[i] = ps[j].rgba[i];
        }
    }

    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);
    oid->im.win_flag = ois->im.win_flag;

    if (ois->title != NULL)   set_im_title(oid, "%s", ois->title);

    set_oi_treatement(oid, "pixel set to %f image", val);
    add_image(imr, oid->type, (void *)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}





int do_im_2pts_deriv_in_x(void)
{
    int i, j;
    float *kx = NULL;
    char c1[MAX_TITLE_STRING_SIZE] = {0};
    imreg *imr = NULL;
    O_i *ois = NULL, *oid = NULL;
    double val1, val2;


    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return OFF;

    if (updating_menu_state != 0)
    {
        if (ois->im.data_type != IS_COMPLEX_IMAGE)
            active_menu->flags |=  D_DISABLED;
        else               active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("To compute the local derivative along X using 2 points difference\n\n"
                             "A new image is created.");
    }

    if (ois->im.data_type == IS_COMPLEX_IMAGE)        return win_printf_OK("complex image !...");

    if (ois->im.win_flag & X_PER) oid = create_one_image(ois->im.nx, ois->im.ny, IS_FLOAT_IMAGE);
    else oid = create_one_image(ois->im.nx - 1, ois->im.ny, IS_FLOAT_IMAGE);

    if (oid == NULL) return win_printf_OK("Can't create dest image");

    for (i = 0 ; i < ois->im.ny ; i++)
    {
        kx = oid->im.pixel[i].fl;

        for (j = 0 ; j < ois->im.nx - 1 ; j++)
        {
            get_raw_pixel_value(ois, j, i, &val1);
            get_raw_pixel_value(ois, j + 1, i, &val2);
            kx[j] = (float)(val2 - val1);
        }

        if (ois->im.win_flag & X_PER)
        {
            j = ois->im.nx - 1;
            get_raw_pixel_value(ois, j, i, &val1);
            get_raw_pixel_value(ois, 0, i, &val2);
            kx[j] = (float)(val2 - val1);
        }
    }

    if ((ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE))
        strcpy(c1, ois->im.source);
    else                  c1[0] = 0;

    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);
    oid->im.win_flag = ois->im.win_flag;
    set_im_title(oid, "\\stack{{{\\partial  \\over \\partial x}}{%s}}", c1);
    set_oi_treatement(oid, "{\\partial \\over \\partial x}");
    add_image(imr, oid->type, (void *)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}

int do_im_2pts_deriv_in_y(void)
{
    int i, j;
    float *z = NULL;
    char c1[MAX_TITLE_STRING_SIZE] = {0};
    imreg *imr = NULL;
    O_i *ois = NULL, *oid = NULL;
    double val1, val2;


    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return OFF;

    if (updating_menu_state != 0)
    {
        if (ois->im.data_type != IS_COMPLEX_IMAGE)
            active_menu->flags |=  D_DISABLED;
        else               active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("To compute the local derivative along X using 2 points difference\n\n"
                             "A new image is created.");
    }

    if (ois->im.data_type == IS_COMPLEX_IMAGE)        return win_printf_OK("complex image !...");

    if (ois->im.win_flag & Y_PER) oid = create_one_image(ois->im.nx, ois->im.ny, IS_FLOAT_IMAGE);
    else oid = create_one_image(ois->im.nx, ois->im.ny - 1, IS_FLOAT_IMAGE);

    if (oid == NULL) return win_printf_OK("Can't create dest image");

    for (i = 0 ; i < ois->im.ny - 1 ; i++)
    {
        z = oid->im.pixel[i].fl;

        for (j = 0 ; j < ois->im.nx ; j++)
        {
            get_raw_pixel_value(ois, j, i, &val1);
            get_raw_pixel_value(ois, j, i + 1, &val2);
            z[j] = (float)(val2 - val1);
        }

        if (ois->im.win_flag & Y_PER)
        {
            z = oid->im.pixel[ois->im.ny - 1].fl;

            for (j = 0 ; j < ois->im.nx ; j++)
            {
                j = ois->im.nx - 1;
                get_raw_pixel_value(ois, j, i, &val1);
                get_raw_pixel_value(ois, j, 0, &val2);
                z[j] = (float)(val2 - val1);
            }
        }
    }

    if ((ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE))
        strcpy(c1, ois->im.source);
    else                  c1[0] = 0;

    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);
    oid->im.win_flag = ois->im.win_flag;
    set_im_title(oid, "\\stack{{{\\partial  \\over \\partial y}}{%s}}", c1);
    set_oi_treatement(oid, "{\\partial \\over \\partial y}");
    add_image(imr, oid->type, (void *)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}





/* computes the phase derivative along the X direction  */
int do_im_kx(void)
{
    int i, j, k;
    float *z = NULL, *kx = NULL;
    float vec, sca, norm;
    char c1[MAX_TITLE_STRING_SIZE] = {0};
    imreg *imr = NULL;
    O_i *ois = NULL, *oid = NULL;

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return OFF;

    if (updating_menu_state != 0)
    {
        if (ois->im.data_type != IS_COMPLEX_IMAGE)
            active_menu->flags |=  D_DISABLED;
        else               active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("To compute the phase derivative with respect to the X direction\n\n"
                             "A new image is created.");
    }

    if (ois->im.data_type != IS_COMPLEX_IMAGE)        return win_printf_OK("I need a complex image...");

    oid = create_one_image(ois->im.nx, ois->im.ny, IS_FLOAT_IMAGE);

    if (oid == NULL) return win_printf_OK("Can't create dest image");

    norm = (float)ois->im.nx / (2 * M_PI);

    for (i = 0 ; i < ois->im.ny ; i++)
    {
        z = ois->im.pixel[i].fl;
        kx = oid->im.pixel[i].fl;

        for (j = 0 ; j < 2 * ois->im.nx ; j += 2)
        {
            k = (j + 2) % (2 * ois->im.nx);
            vec = z[j] * z[k + 1] - z[j + 1] * z[k];
            sca = z[j] * z[k] + z[j + 1] * z[k + 1];

            if (vec == 0.0 && sca == 0.0) kx[j / 2] = 0;
            else  kx[j / 2] = norm * (float)atan2(vec, sca);
        }

        if (!(ois->im.win_flag & X_PER)) kx[ois->im.nx - 1] = 0.0;
    }

    if ((ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE))
        strcpy(c1, ois->im.source);
    else                  c1[0] = 0;

    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);
    oid->im.win_flag = ois->im.win_flag;
    set_im_title(oid, "\\stack{{{\\partial \\Phi \\over \\partial x}}{%s}}", c1);
    set_oi_treatement(oid, "{\\partial \\Phi \\over \\partial x}");
    add_image(imr, oid->type, (void *)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}



int do_im_ky(void)
{
    int i, j, k;
    union pix *z = NULL, *ky = NULL;
    float vec, sca, norm;
    char c1[MAX_TITLE_STRING_SIZE] = {0};
    imreg *imr = NULL;
    O_i *ois = NULL, *oid = NULL;

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return OFF;

    if (updating_menu_state != 0)
    {
        if (ois->im.data_type != IS_COMPLEX_IMAGE)
            active_menu->flags |=  D_DISABLED;
        else               active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("To compute the phase derivative with respect to the Y direction\n\n"
                             "A new image is created.");
    }

    if (ois->im.data_type != IS_COMPLEX_IMAGE)        return win_printf_OK("I need a complex image...");

    oid = create_one_image(ois->im.nx, ois->im.ny, IS_FLOAT_IMAGE);

    if (oid == NULL) return win_printf_OK("Can't create dest image");

    norm = (float)ois->im.ny / (2 * M_PI);
    z = ois->im.pixel;
    ky = oid->im.pixel;

    for (i = 0 ; i < 2 * ois->im.nx ; i += 2)
    {
        for (j = 0 ; j < ois->im.ny ; j++)
        {
            k = (j + 1) % ois->im.ny;
            vec = z[j].fl[i] * z[k].fl[i + 1] - z[k].fl[i] * z[j].fl[i + 1];
            sca = z[j].fl[i] * z[k].fl[i] + z[j].fl[i + 1] * z[k].fl[i + 1];

            if (vec == 0.0 && sca == 0.0)   ky[j].fl[i / 2] = 0;
            else  ky[j].fl[i / 2] = norm * (float)atan2(vec, sca);
        }

        if (!(ois->im.win_flag & Y_PER))  ky[ois->im.ny - 1].fl[i / 2] = 0.0;
    }

    if ((ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE))
        strcpy(c1, ois->im.source);
    else                  c1[0] = 0;

    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);
    oid->im.win_flag = ois->im.win_flag;
    set_im_title(oid, "\\stack{{{\\partial \\Phi \\over \\partial y}}{%s}}", c1);
    set_oi_treatement(oid, "{\\partial \\Phi \\over \\partial y}");
    add_image(imr, oid->type, (void *)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}



/* computes the phase derivative along the X direction  */
int do_im_phi(void)
{
    int i, j, k, p0;
    float *z = NULL, *ph = NULL;
    float vec, sca, max, mar;
    char c1[MAX_TITLE_STRING_SIZE] = {0};
    imreg *imr = NULL;
    O_i *ois = NULL, *oid = NULL;
    d_s *ds = NULL, *dsp = NULL;
    static float minampratio = 0.05, kx = 0;
    static int complete_phase_seg = 1;

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return OFF;

    if (updating_menu_state != 0)
    {
        if (ois->im.data_type != IS_COMPLEX_IMAGE)
            active_menu->flags |=  D_DISABLED;
        else               active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("To compute the phase of a complex\n\n"
                             "A new image is created.");
    }

    if (ois->im.data_type != IS_COMPLEX_IMAGE)
        return win_printf_OK("I need a complex image...");

    i = win_scanf("Compute phase of imaginary image (substract -2\\pi k_x.i/Nx; k_x %6f)\n"
                  "when relative amplitude is greater than\n"
                  "%8f complete phase segment %b\n", &kx, &minampratio, &complete_phase_seg);
    oid = create_one_image(ois->im.nx, ois->im.ny, IS_FLOAT_IMAGE);

    if (oid == NULL) return win_printf_OK("Can't create dest image");

    ds = build_data_set(ois->im.nx, ois->im.nx);
    dsp = build_data_set(ois->im.nx, ois->im.nx);

    if (ds == NULL || dsp == NULL)  return win_printf_OK("Can't create temp dataset");

    mar = minampratio * minampratio;

    for (i = 0 ; i < ois->im.ny ; i++)
    {
        z = ois->im.pixel[i].fl;
        ph = oid->im.pixel[i].fl;

        for (j = 0, max = 0 ; j < 2 * ois->im.nx ; j += 2)
        {
            k = j / 2;
            vec = z[j + 1];
            sca = z[j];
            ds->xd[k] = vec * vec + sca * sca;
            max = (ds->xd[k] > max) ? ds->xd[k] : max;
            ds->yd[k] = (vec == 0.0 && sca == 0.0)  ?  0 : atan2(vec, sca);
        }

        if (complete_phase_seg == 0)
        {
            for (k = 0; k < ds->nx; k++)
                ph[k] = (ds->xd[k] > (max * mar)) ? ds->yd[k] : 0;
        }
        else
        {
            for (k = p0 = 0; k < ds->nx; k++)
            {
                if (((k > 0) && fabs(ds->yd[k] - ds->yd[k - 1]) > M_PI) || (k == ds->nx - 1))
                {
                    // a phase jump
                    if ((ds->xd[k] > (max * mar)) && (ds->xd[p0] > (max * mar)))
                    {
                        for (j = p0; j < k; j++)
                            ph[j] = ds->yd[j];
                    }

                    p0 = k;
                }
            }
        }
    }

    if ((ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE))
        strcpy(c1, ois->im.source);
    else                  c1[0] = 0;

    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);
    oid->im.win_flag = ois->im.win_flag;
    set_im_title(oid, "\\stack{{{Phi }}{%s}}", c1);
    set_oi_treatement(oid, "{\\Phi(Re,Im)}");
    add_image(imr, oid->type, (void *)oid);
    find_zmin_zmax(oid);
    free_data_set(ds);
    free_data_set(dsp);
    return (refresh_image(imr, imr->n_oi - 1));
}





int do_amp(void)
{
    int i, j;
    union pix *z = NULL, *amp = NULL;
    float ampt = 0;
    char  c[MAX_TITLE_STRING_SIZE] = {0}, c1[MAX_TITLE_STRING_SIZE] = {0};
    imreg *imr = NULL;
    O_i   *ois = NULL, *oid = NULL;
    int   index;


    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return OFF;

    if (updating_menu_state != 0)
    {
        if (ois->im.data_type != IS_COMPLEX_IMAGE)
            active_menu->flags |=  D_DISABLED;
        else               active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("To compute %s on a complex image\n\n"
                             "A new image is created.", c);
    }



    index = active_menu->flags & 0xFFFFFF00;

    switch (index)
    {
    case DO_AMP_RE :
        sprintf(c, "Re^2");
        break;

    case DO_AMP_IM :
        sprintf(c, "Im^2");
        break;

    case DO_AMP_AMP_2:
        sprintf(c, "(Re^2 + Im^2)");
        break;

    case DO_AMP_AMP:
        sprintf(c, "\\sqrt{Re^2 + Im^2}");
        break;

    default :
        return OFF;
        break;
    }

    if (ois->im.data_type != IS_COMPLEX_IMAGE)        return win_printf_OK("I need a complex image...");

    oid = create_one_image(ois->im.nx, ois->im.ny, IS_FLOAT_IMAGE);

    if (oid == NULL) return win_printf_OK("Can't create dest image");

    z = ois->im.pixel;
    amp = oid->im.pixel;

    for (i = 0 ; i < 2 * ois->im.nx ; i += 2)
    {
        for (j = 0 ; j < ois->im.ny ; j++)
        {
            switch (mn_index)
            {
            case DO_AMP_RE :
                ampt = z[j].fl[i] * z[j].fl[i];
                break;

            case DO_AMP_IM :
                ampt = z[j].fl[i + 1] * z[j].fl[i + 1];
                break;

            case DO_AMP_AMP_2:
                ampt = z[j].fl[i] * z[j].fl[i];
                ampt += z[j].fl[i + 1] * z[j].fl[i + 1];
                break;

            case DO_AMP_AMP:
                ampt = z[j].fl[i] * z[j].fl[i];
                ampt += z[j].fl[i + 1] * z[j].fl[i + 1];
                ampt = sqrt(ampt);
                break;
            };

            amp[j].fl[i / 2] = ampt;
        }
    }

    if ((ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE))
        strcpy(c1, ois->im.source);
    else                  c1[0] = 0;

    set_im_title(oid, "%s of %s", c, c1);
    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);
    oid->im.win_flag = ois->im.win_flag;
    oid->im.treatement = Mystrdupre(oid->im.history, c);
    add_image(imr, oid->type, (void *)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}


/*  O_i *duplicate_image(O_i *src, O_i *dest);
 *  DESCRIPTION Copy the data of src one_image to dest,
 *          if dest == NULL allocate space for the new image
 *
 *
 *  RETURNS     the dest one_image on success, NULL otherwise
 *
 */
O_i *duplicate_and_swap_image(O_i *src, O_i *dest)
{
    register int i, j;
    int ii, jj, n_frames = 1;
    union pix *pd = NULL, *ps = NULL;
    p_l *sp = NULL, *dp = NULL;
    un_s *us = NULL;

    if (src == NULL)        return (O_i *) xvin_ptr_error(Wrong_Argument);

    if (dest == NULL)
    {
        if ((dest = (O_i *)calloc(1, sizeof(O_i))) == NULL)
            return (O_i *) xvin_ptr_error(Out_Of_Memory);

        init_one_image(dest, src->im.data_type);
    }

    dest->width = src->height;
    dest->height = src->width;
    dest->right = src->up;
    dest->up = src->right;
    dest->iopt = src->iopt;
    dest->iopt2 = src->iopt2;
    dest->tick_len = src->tick_len;
    dest->x_lo = src->y_lo;
    dest->x_hi = src->y_hi;
    dest->y_lo = src->x_lo;
    dest->y_hi = src->x_hi;
    dest->z_black = src->z_black;
    dest->z_white = src->z_white;
    dest->z_min = src->z_min;
    dest->z_max = src->z_max;
    dest->ax = src->ay;
    dest->dx = src->dy;
    dest->ay = src->ax;
    dest->dy = src->dx;
    dest->az = src->az;
    dest->dz = src->dz;
    dest->at = src->at;
    dest->dt = src->dt;
    dest->im.time = src->im.time;

    if (get_oi_periodicity_in_x(src))     set_oi_periodic_in_y(dest);

    if (get_oi_periodicity_in_y(src))     set_oi_periodic_in_x(dest);

    dest->title = Mystrdup(src->title);
    dest->x_title = Mystrdup(src->y_title);
    dest->y_title = Mystrdup(src->x_title);
    dest->x_prime_title = Mystrdup(src->y_prime_title);
    dest->y_prime_title = Mystrdup(src->x_prime_title);
    dest->x_prefix = Mystrdup(src->y_prefix);
    dest->y_prefix = Mystrdup(src->x_prefix);
    dest->x_unit = Mystrdup(src->y_unit);
    dest->y_unit = Mystrdup(src->x_unit);
    dest->z_prefix = Mystrdup(src->z_prefix);
    dest->t_prefix = Mystrdup(src->t_prefix);
    dest->z_unit = Mystrdup(src->z_unit);
    dest->t_unit = Mystrdup(src->t_unit);
    dest->im.source = Mystrdup(src->im.source);
    dest->im.history = Mystrdup(src->im.history);
    dest->im.treatement = Mystrdup(src->im.treatement);
    dest->im.record_duration = src->im.record_duration;

    if (dest->im.special != NULL)
        remove_from_one_image(dest, ALL_SPECIAL, dest->im.special);

    if (src->im.special != NULL)
        for (i = 0 ; i < src->im.n_special ; i++)
            add_to_one_image(dest, IS_SPECIAL, src->im.special[i]);

    if (src->im.n_f > 0)
    {
        i = win_printf("You are duplicating a movie with %d images\nif you want to duplicate the entire movie press OK\nif you want to copy a single frame press WIN_CANCEL",
                       src->im.n_f);

        if (i == WIN_OK)    n_frames = dest->im.n_f = src->im.n_f;
    }

    alloc_one_image(dest, src->im.ny, src->im.nx, src->im.data_type);
    pd = dest->im.pixel;
    ps = (n_frames > 1) ? src->im.pxl[0] : src->im.pixel;

    for (i = 0, ii = 0 ; i < n_frames * src->im.ny ; i++, ii++)
    {
        for (j = 0, jj = 0 ; j < src->im.nx ; j++, jj++)
        {
            switch (src->im.data_type)
            {
            case  IS_CHAR_IMAGE:
                pd[jj].ch[ii] = ps[i].ch[j];
                break;

            case  IS_RGB_PICTURE:
                pd[jj].ch[3 * ii] = ps[i].ch[3 * j];
                pd[jj].ch[(3 * ii) + 1] = ps[i].ch[(3 * j) + 1];
                pd[jj].ch[(3 * ii) + 2] = ps[i].ch[(3 * j) + 2];
                break;

            case  IS_RGBA_PICTURE:
                pd[jj].ch[3 * ii] = ps[i].ch[3 * j];
                pd[jj].ch[(3 * ii) + 1] = ps[i].ch[(3 * j) + 1];
                pd[jj].ch[(3 * ii) + 2] = ps[i].ch[(3 * j) + 2];
                pd[jj].ch[(3 * ii) + 3] = ps[i].ch[(3 * j) + 3];
                break;

            case  IS_INT_IMAGE:
                pd[jj].in[ii] = ps[i].in[j];
                break;

            case  IS_UINT_IMAGE:
                pd[jj].ui[ii] = ps[i].ui[j];
                break;

            case  IS_LINT_IMAGE:
                pd[jj].li[ii] = ps[i].li[j];
                break;

            case  IS_FLOAT_IMAGE:
                pd[jj].fl[ii] = ps[i].fl[j];
                break;

            case  IS_COMPLEX_IMAGE:
                pd[jj].fl[2 * ii] = ps[i].fl[2 * j];
                pd[jj].fl[(2 * ii) + 1] = ps[i].fl[(2 * j) + 1];
                break;

            case  IS_DOUBLE_IMAGE:
                pd[jj].db[ii] = ps[i].db[j];
                break;

            case  IS_COMPLEX_DOUBLE_IMAGE:
                pd[jj].db[(2 * ii)] = ps[i].db[2 * j];
                pd[jj].db[(2 * ii) + 1] = ps[i].db[(2 * j) + 1];
                break;

            }
        }
    }

    dest->im.nxs = src->im.nys;
    dest->im.nxe = src->im.nye;
    dest->im.nys = src->im.nxs;
    dest->im.nye = src->im.nxe;
    dest->im.mode = src->im.mode;

    for (i = 0 ; i < src->n_lab ; i++)
    {
        sp = src->lab[i];
        dp = (p_l *)calloc(1, sizeof(p_l));

        if (dp == NULL)        return (O_i *) xvin_ptr_error(Out_Of_Memory);

        *dp = *sp;

        if (sp->text != NULL)       dp->text = strdup(sp->text);

        dest->lab[i] = dp;
    }

    while (dest->n_yu > 0)
    {
        remove_from_one_image(dest, IS_Y_UNIT_SET, (void *)dest->yu[dest->n_yu - 1]);
    };

    if (src->xu != NULL && src->n_xu > 0)
    {
        for (i = 0 ; i < src->n_xu ; i++)
        {
            us = duplicate_unit_set(src->xu[i], NULL);
            add_to_one_image(dest, IS_Y_UNIT_SET, (void *)us);
        }
    }

    dest->c_yu = src->c_xu;

    while (dest->n_xu > 0)
    {
        remove_from_one_image(dest, IS_X_UNIT_SET, (void *)dest->xu[dest->n_xu - 1]);
    };

    if (src->yu != NULL && src->n_yu > 0)
    {
        for (i = 0 ; i < src->n_yu ; i++)
        {
            us = duplicate_unit_set(src->yu[i], NULL);
            add_to_one_image(dest, IS_X_UNIT_SET, (void *)us);
        }
    }

    dest->c_xu = src->c_yu;

    while (dest->n_zu > 0)
    {
        remove_from_one_image(dest, IS_Z_UNIT_SET, (void *)dest->zu[dest->n_zu - 1]);
    };

    if (src->zu != NULL && src->n_zu > 0)
    {
        for (i = 0 ; i < src->n_zu ; i++)
        {
            us = duplicate_unit_set(src->zu[i], NULL);
            add_to_one_image(dest, IS_Z_UNIT_SET, (void *)us);
        }
    }

    dest->c_zu = src->c_zu;

    while (dest->n_tu > 0)
    {
        remove_from_one_image(dest, IS_T_UNIT_SET, (void *)dest->tu[dest->n_tu - 1]);
    };

    if (src->tu != NULL && src->n_tu > 0)
    {
        for (i = 0 ; i < src->n_tu ; i++)
        {
            us = duplicate_unit_set(src->tu[i], NULL);
            add_to_one_image(dest, IS_T_UNIT_SET, (void *)us);
        }
    }

    dest->c_tu = src->c_tu;

    dest->filename = Transfer_filename(src->filename);
    dest->dir = Mystrdup(src->dir);

    if (image_duplicate_use != NULL)    image_duplicate_use(dest, src);

    dest->need_to_refresh |= ALL_NEED_REFRESH;
    return dest;
}



int do_im_swap_X_Y(void)
{
    O_i *ois = NULL, *oid = NULL;
    imreg *imr = NULL;

    if (updating_menu_state != 0)
        return D_O_K;

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("To swap X and Y directions of an image.");
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)      return D_O_K;

    if (imr == NULL)
        return win_printf_OK("I cannot find any image !");

    oid = duplicate_and_swap_image(ois, NULL);
    add_image(imr, oid->type, (void *)oid);
    return (refresh_image(imr, imr->n_oi - 1));
}


O_i *upside_down_image(O_i *ois)
{
    register int i, j;
    O_i *oid = NULL;
    int onx, ony, data_type;
    union pix *ps = NULL, *pd = NULL;

    onx = ois->im.nx;
    ony = ois->im.ny;
    data_type = ois->im.data_type;
    oid =  create_one_image(onx, ony, data_type);

    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return NULL;
    }

    if (data_type == IS_CHAR_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].ch[onx - 1 - j] = ps[i].ch[j];
        }
    }
    else if (data_type == IS_INT_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].in[onx - 1 - j] = ps[i].in[j];
        }
    }
    else if (data_type == IS_UINT_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].ui[onx - 1 - j] = ps[i].ui[j];
        }
    }
    else if (data_type == IS_LINT_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].li[onx - 1 - j] = ps[i].li[j];
        }
    }
    else if (data_type == IS_FLOAT_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].fl[onx - 1 - j] = ps[i].fl[j];
        }
    }
    else if (data_type == IS_DOUBLE_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].db[onx - 1 - j] = ps[i].db[j];

        }
    }
    else if (data_type == IS_COMPLEX_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
            {
                pd[ony - 1 - i].fl[2 * (onx - 1 - j)] = ps[i].fl[2 * j];
                pd[ony - 1 - i].fl[2 * (onx - 1 - j) + 1] = ps[i].fl[2 * j + 1];
            }
        }
    }
    else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
            {
                pd[ony - 1 - i].db[2 * (onx - 1 - j)] = ps[i].db[2 * j];
                pd[ony - 1 - i].db[2 * (onx - 1 - j) + 1] = ps[i].db[2 * j + 1];
            }
        }
    }
    else if (data_type == IS_RGB_PICTURE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].rgb[j] = ps[i].rgb[j];
        }
    }
    else if (data_type == IS_RGBA_PICTURE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].rgba[j] = ps[i].rgba[j];
        }
    }

    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);
    oid->im.win_flag = ois->im.win_flag;

    if (ois->title != NULL)   set_im_title(oid, "%s Upside down", ois->title);

    set_oi_treatement(oid, "Image %s upside down", ois->filename);
    return oid;
}

int do_upside_down_image(void)
{
    O_i *ois = NULL, *oid = NULL;
    imreg *imr = NULL;


    if (updating_menu_state != 0)    return D_O_K;

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine turns an image"
                             " upside down");
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)
    {
        win_printf("Cannot find image");
        return D_O_K;
    }

    oid = upside_down_image(ois);

    if (oid == NULL)    return win_printf_OK("unable to create image!");

    add_image(imr, oid->type, (void *)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}



O_i *upside_down_image_right_to_left(O_i *ois)
{
    register int i, j;
    O_i *oid = NULL;
    int onx, ony, data_type;
    union pix *ps = NULL, *pd = NULL;

    onx = ois->im.nx;
    ony = ois->im.ny;
    data_type = ois->im.data_type;
    oid =  create_one_image(onx, ony, data_type);

    if (oid == NULL)
    {
        win_printf("Can't create dest image");
        return NULL;
    }

    if (data_type == IS_CHAR_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].ch[onx - 1 - j] = ps[i].ch[j];
        }
    }
    else if (data_type == IS_INT_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].in[onx - 1 - j] = ps[i].in[j];
        }
    }
    else if (data_type == IS_UINT_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].ui[onx - 1 - j] = ps[i].ui[j];
        }
    }
    else if (data_type == IS_LINT_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].li[onx - 1 - j] = ps[i].li[j];
        }
    }
    else if (data_type == IS_FLOAT_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].fl[onx - 1 - j] = ps[i].fl[j];
        }
    }
    else if (data_type == IS_DOUBLE_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].db[onx - 1 - j] = ps[i].db[j];

        }
    }
    else if (data_type == IS_COMPLEX_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
            {
                pd[ony - 1 - i].fl[2 * (onx - 1 - j)] = ps[i].fl[2 * j];
                pd[ony - 1 - i].fl[2 * (onx - 1 - j) + 1] = ps[i].fl[2 * j + 1];
            }
        }
    }
    else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
            {
                pd[ony - 1 - i].db[2 * (onx - 1 - j)] = ps[i].db[2 * j];
                pd[ony - 1 - i].db[2 * (onx - 1 - j) + 1] = ps[i].db[2 * j + 1];
            }
        }
    }
    else if (data_type == IS_RGB_PICTURE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].rgb[j] = ps[i].rgb[j];
        }
    }
    else if (data_type == IS_RGBA_PICTURE)
    {
        for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
        {
            for (j = 0; j < onx; j++)
                pd[ony - 1 - i].rgba[j] = ps[i].rgba[j];
        }
    }

    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);
    oid->im.win_flag = ois->im.win_flag;

    if (ois->title != NULL) set_im_title(oid, "%s Upside down", ois->title);

    set_oi_treatement(oid, "Image %s upside down", ois->filename);
    return oid;
}


int do_upside_down_image_right_to_left(void)
{
    O_i *ois = NULL, *oid = NULL;
    imreg *imr = NULL;


    if (updating_menu_state != 0)  return D_O_K;

    if (key[KEY_LSHIFT])
    {
        return win_printf_OK("This routine turns an image"
                             " upside down and right to left");
    }

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)
    {
        win_printf("Cannot find image");
        return D_O_K;
    }

    oid = upside_down_image_right_to_left(ois);

    if (oid == NULL)  return win_printf_OK("unable to create image!");

    add_image(imr, oid->type, (void *)oid);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}

int do_im_of_max_z_profile_of_movie(void)
{
    register int i, j;
    static int start = -1, end = -1, min = 0, filter = 0, fir = 16, maxval = 0;
    O_i *ois = NULL, *oid = NULL;
    int im, nf;
    d_s *ds = NULL;
    imreg *imr = NULL;
    int onx, ony, ix, iy;
    union pix *ps = NULL, *pd = NULL;
    float ypos = 0, Max_val = 0;


    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) != 2)
    {
        active_menu->flags |=  D_DISABLED;
        return D_O_K;
    }

    if (updating_menu_state != 0)
    {
        if (ois->im.n_f == 1 || ois->im.n_f == 0)
            active_menu->flags |=  D_DISABLED;
        else active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    nf = abs(ois->im.n_f);
    ony = ois->im.ny;
    onx = ois->im.nx;

    if (nf <= 0)
    {
        win_printf("This is not a movie");
        return D_O_K;
    }

    start = (start == -1) ? 0 : start;
    end = (end == -1 || nf < end) ? nf : end;
    i = win_scanf("I shall build an image with each pixel corresponding to:\n"
                  "%Rthe maximum or %rthe minimum of the movie Z profile\n"
                  "extracted at that pixel position\n"
                  "from frames starting at %6d ending at %6d\n"
		  "%b->instead of max value, give the frame number\n"
		  "using a FIR filter, if yes averaging %4d points\n"
		  "%bAdd max value"
                  , &min, &start, &end,&filter,&fir,&maxval);

    if (i == WIN_CANCEL)  return D_O_K;

    start = (start < 0) ? 0 : start;
    end = (nf < end) ? nf : end;

    if (filter == 0)
      oid = create_and_attach_oi_to_imr(imr, onx, ony, ois->im.data_type);
    else
      {
	if (maxval == 0)
	  oid = create_and_attach_oi_to_imr(imr, onx, ony, IS_FLOAT_IMAGE);
	else
	  oid = create_and_attach_movie_to_imr(imr, onx, ony, IS_FLOAT_IMAGE,2);
      }
    if (oid == NULL)  return win_printf_OK("cannot create profile !");


    set_im_title(oid, "%s taken in frame [%d,%d[", ((min) ? "Min" : "Max"), start, end);
    set_oi_treatement(oid, "%s taken in frame [%d,%d[", ((min) ? "Min" : "Max"), start, end);
    inherit_from_im_to_im(oid, ois);
    uns_oi_2_oi(oid, ois);

    if (filter == 0)
      {
	for (im = start; im < end; im++)
	  {
	    switch_frame(ois, im);
	    ps = ois->im.pixel, pd = oid->im.pixel;

	    for (iy = 0; iy < ony; iy++)
	      {
		for (ix = 0; ix < onx; ix++)
		  {

		    switch (oid->im.data_type)
		      {
		      case  IS_CHAR_IMAGE:
			{
			  if (im == start)
			    pd[iy].ch[ix] = ps[iy].ch[ix];

			  if (min == 0 && (ps[iy].ch[ix] > pd[iy].ch[ix]))
			    pd[iy].ch[ix] = ps[iy].ch[ix];

			  if (min == 1 && (ps[iy].ch[ix] < pd[iy].ch[ix]))
			    pd[iy].ch[ix] = ps[iy].ch[ix];
			}
			break;

		      case  IS_RGB_PICTURE:
			{
			  if (im == start)
			    pd[iy].rgb[ix].r = ps[iy].rgb[ix].r;

			  if (min == 0 && (ps[iy].rgb[ix].r > pd[iy].rgb[ix].r))
			    pd[iy].rgb[ix].r = ps[iy].rgb[ix].r;

			  if (min == 1 && (ps[iy].rgb[ix].r < pd[iy].rgb[ix].r))
			    pd[iy].rgb[ix].r = ps[iy].rgb[ix].r;

			  if (im == start)
			    pd[iy].rgb[ix].g = ps[iy].rgb[ix].g;

			  if (min == 0 && (ps[iy].rgb[ix].g > pd[iy].rgb[ix].g))
			    pd[iy].rgb[ix].g = ps[iy].rgb[ix].g;

			  if (min == 1 && (ps[iy].rgb[ix].g < pd[iy].rgb[ix].g))
			    pd[iy].rgb[ix].g = ps[iy].rgb[ix].g;

			  if (im == start)
			    pd[iy].rgb[ix].b = ps[iy].rgb[ix].b;

			  if (min == 0 && (ps[iy].rgb[ix].b > pd[iy].rgb[ix].b))
			    pd[iy].rgb[ix].b = ps[iy].rgb[ix].b;

			  if (min == 1 && (ps[iy].rgb[ix].b < pd[iy].rgb[ix].b))
			    pd[iy].rgb[ix].b = ps[iy].rgb[ix].b;

			}
			break;

		      case  IS_RGBA_PICTURE:
			{
			  if (im == start)
			    pd[iy].rgba[ix].r = ps[iy].rgba[ix].r;

			  if (min == 0 && (ps[iy].rgba[ix].r > pd[iy].rgba[ix].r))
			    pd[iy].rgba[ix].r = ps[iy].rgba[ix].r;

			  if (min == 1 && (ps[iy].rgba[ix].r < pd[iy].rgba[ix].r))
			    pd[iy].rgba[ix].r = ps[iy].rgba[ix].r;

			  if (im == start)
			    pd[iy].rgba[ix].g = ps[iy].rgba[ix].g;

			  if (min == 0 && (ps[iy].rgba[ix].g > pd[iy].rgba[ix].g))
			    pd[iy].rgba[ix].g = ps[iy].rgba[ix].g;

			  if (min == 1 && (ps[iy].rgba[ix].g < pd[iy].rgba[ix].g))
			    pd[iy].rgba[ix].g = ps[iy].rgba[ix].g;

			  if (im == start)
			    pd[iy].rgba[ix].b = ps[iy].rgba[ix].b;

			  if (min == 0 && (ps[iy].rgba[ix].b > pd[iy].rgba[ix].b))
			    pd[iy].rgba[ix].b = ps[iy].rgba[ix].b;

			  if (min == 1 && (ps[iy].rgba[ix].b < pd[iy].rgba[ix].b))
			    pd[iy].rgba[ix].b = ps[iy].rgba[ix].b;

			  if (im == start)
			    pd[iy].rgba[ix].a = ps[iy].rgba[ix].a;

			  if (min == 0 && (ps[iy].rgba[ix].a > pd[iy].rgba[ix].a))
			    pd[iy].rgba[ix].a = ps[iy].rgba[ix].a;

			  if (min == 1 && (ps[iy].rgba[ix].a < pd[iy].rgba[ix].a))
			    pd[iy].rgba[ix].a = ps[iy].rgba[ix].a;

			}
			break;

		      case  IS_RGB16_PICTURE:
			{

			  if (im == start)
			    pd[iy].rgb16[ix].r = ps[iy].rgb16[ix].r;

			  if (min == 0 && (ps[iy].rgb16[ix].r > pd[iy].rgb16[ix].r))
			    pd[iy].rgb16[ix].r = ps[iy].rgb16[ix].r;

			  if (min == 1 && (ps[iy].rgb16[ix].r < pd[iy].rgb16[ix].r))
			    pd[iy].rgb16[ix].r = ps[iy].rgb16[ix].r;

			  if (im == start)
			    pd[iy].rgb16[ix].g = ps[iy].rgb16[ix].g;

			  if (min == 0 && (ps[iy].rgb16[ix].g > pd[iy].rgb16[ix].g))
			    pd[iy].rgb16[ix].g = ps[iy].rgb16[ix].g;

			  if (min == 1 && (ps[iy].rgb16[ix].g < pd[iy].rgb16[ix].g))
			    pd[iy].rgb16[ix].g = ps[iy].rgb16[ix].g;

			  if (im == start)
			    pd[iy].rgb16[ix].b = ps[iy].rgb16[ix].b;

			  if (min == 0 && (ps[iy].rgb16[ix].b > pd[iy].rgb16[ix].b))
			    pd[iy].rgb16[ix].b = ps[iy].rgb16[ix].b;

			  if (min == 1 && (ps[iy].rgb16[ix].b < pd[iy].rgb16[ix].b))
			    pd[iy].rgb16[ix].b = ps[iy].rgb16[ix].b;

			}
			break;

		      case  IS_RGBA16_PICTURE:
			{
			  if (im == start)
			    pd[iy].rgba16[ix].r = ps[iy].rgba16[ix].r;

			  if (min == 0 && (ps[iy].rgba16[ix].r > pd[iy].rgba16[ix].r))
			    pd[iy].rgba16[ix].r = ps[iy].rgba16[ix].r;

			  if (min == 1 && (ps[iy].rgba16[ix].r < pd[iy].rgba16[ix].r))
			    pd[iy].rgba16[ix].r = ps[iy].rgba16[ix].r;

			  if (im == start)
			    pd[iy].rgba16[ix].g = ps[iy].rgba16[ix].g;

			  if (min == 0 && (ps[iy].rgba16[ix].g > pd[iy].rgba16[ix].g))
			    pd[iy].rgba16[ix].g = ps[iy].rgba16[ix].g;

			  if (min == 1 && (ps[iy].rgba16[ix].g < pd[iy].rgba16[ix].g))
			    pd[iy].rgba16[ix].g = ps[iy].rgba16[ix].g;

			  if (im == start)
			    pd[iy].rgba16[ix].b = ps[iy].rgba16[ix].b;

			  if (min == 0 && (ps[iy].rgba16[ix].b > pd[iy].rgba16[ix].b))
			    pd[iy].rgba16[ix].b = ps[iy].rgba16[ix].b;

			  if (min == 1 && (ps[iy].rgba16[ix].b < pd[iy].rgba16[ix].b))
			    pd[iy].rgba16[ix].b = ps[iy].rgba16[ix].b;

			  if (im == start)
			    pd[iy].rgba16[ix].a = ps[iy].rgba16[ix].a;

			  if (min == 0 && (ps[iy].rgba16[ix].a > pd[iy].rgba16[ix].a))
			    pd[iy].rgba16[ix].a = ps[iy].rgba16[ix].a;

			  if (min == 1 && (ps[iy].rgba16[ix].a < pd[iy].rgba16[ix].a))
			    pd[iy].rgba16[ix].a = ps[iy].rgba16[ix].a;
			}
			break;

		      case  IS_INT_IMAGE:
			{
			  if (im == start)
			    pd[iy].in[ix] = ps[iy].in[ix];

			  if (min == 0 && (ps[iy].in[ix] > pd[iy].in[ix]))
			    pd[iy].in[ix] = ps[iy].in[ix];

			  if (min == 1 && (ps[iy].in[ix] < pd[iy].in[ix]))
			    pd[iy].in[ix] = ps[iy].in[ix];
			}
			break;

		      case  IS_UINT_IMAGE:
			{
			  if (im == start)
			    pd[iy].ui[ix] = ps[iy].ui[ix];

			  if (min == 0 && (ps[iy].ui[ix] > pd[iy].ui[ix]))
			    pd[iy].ui[ix] = ps[iy].ui[ix];

			  if (min == 1 && (ps[iy].ui[ix] < pd[iy].ui[ix]))
			    pd[iy].ui[ix] = ps[iy].ui[ix];
			}
			break;

		      case  IS_LINT_IMAGE:
			{
			  if (im == start)
			    pd[iy].li[ix] = ps[iy].li[ix];

			  if (min == 0 && (ps[iy].li[ix] > pd[iy].li[ix]))
			    pd[iy].li[ix] = ps[iy].li[ix];

			  if (min == 1 && (ps[iy].li[ix] < pd[iy].li[ix]))
			    pd[iy].li[ix] = ps[iy].li[ix];
			}
			break;

		      case  IS_FLOAT_IMAGE:
			{
			  if (im == start)
			    pd[iy].fl[ix] = ps[iy].fl[ix];

			  if (min == 0 && (ps[iy].fl[ix] > pd[iy].fl[ix]))
			    pd[iy].fl[ix] = ps[iy].fl[ix];

			  if (min == 1 && (ps[iy].fl[ix] < pd[iy].fl[ix]))
			    pd[iy].fl[ix] = ps[iy].fl[ix];
			}
			break;

		      case  IS_DOUBLE_IMAGE:
			{
			  if (im == start)
			    pd[iy].db[ix] = ps[iy].db[ix];

			  if (min == 0 && (ps[iy].db[ix] > pd[iy].db[ix]))
			    pd[iy].db[ix] = ps[iy].db[ix];

			  if (min == 1 && (ps[iy].db[ix] < pd[iy].db[ix]))
			    pd[iy].db[ix] = ps[iy].db[ix];
			}
			break;

		      case  IS_COMPLEX_IMAGE:
			{
			  if (im == start)
			    pd[iy].cp[ix].re = ps[iy].cp[ix].re;

			  if (min == 0 && (ps[iy].cp[ix].re > pd[iy].cp[ix].re))
			    pd[iy].cp[ix].re = ps[iy].cp[ix].re;

			  if (min == 1 && (ps[iy].cp[ix].re < pd[iy].cp[ix].re))
			    pd[iy].cp[ix].re = ps[iy].cp[ix].re;

			  if (im == start)
			    pd[iy].cp[ix].im = ps[iy].cp[ix].im;

			  if (min == 0 && (ps[iy].cp[ix].im > pd[iy].cp[ix].im))
			    pd[iy].cp[ix].im = ps[iy].cp[ix].im;

			  if (min == 1 && (ps[iy].cp[ix].im < pd[iy].cp[ix].im))
			    pd[iy].cp[ix].im = ps[iy].cp[ix].im;
			}
			break;

		      case  IS_COMPLEX_DOUBLE_IMAGE:
			{
			  if (im == start)
			    pd[iy].dcp[ix].dre = ps[iy].dcp[ix].dre;

			  if (min == 0 && (ps[iy].dcp[ix].dre > pd[iy].dcp[ix].dre))
			    pd[iy].dcp[ix].dre = ps[iy].dcp[ix].dre;

			  if (min == 1 && (ps[iy].dcp[ix].dre < pd[iy].dcp[ix].dre))
			    pd[iy].dcp[ix].dre = ps[iy].dcp[ix].dre;

			  if (im == start)
			    pd[iy].dcp[ix].dim = ps[iy].dcp[ix].dim;

			  if (min == 0 && (ps[iy].dcp[ix].dim > pd[iy].dcp[ix].dim))
			    pd[iy].dcp[ix].dim = ps[iy].dcp[ix].dim;

			  if (min == 1 && (ps[iy].dcp[ix].dim < pd[iy].dcp[ix].dim))
			    pd[iy].dcp[ix].dim = ps[iy].dcp[ix].dim;
			}
			break;

		      default:
			return 1;
		      }
		  }
	      }
	  }
      }
    else
      {
	ds = build_data_set(nf,nf);
	pd = oid->im.pixel;
	for (ix = 0; ix < nf; ix++) ds->xd[ix] = ix;
	for (iy = 0; iy < ony; iy++)
	  {
	    for (ix = 0; ix < onx; ix++)
	      {
		extract_z_profile_from_movie(ois, ix, iy, ds->yd);
		for(i = 0; i < ds->nx-fir; i++)
		  {
		    ds->xd[i] = 0;
		    for(j = 0; j < fir; j++)
		      {
			ds->xd[i] += ds->yd[i+j];
		      }
		  }
		for(i = j = 0; i < ds->nx-fir; i++)
		  {
		    if (i == 0 || ds->xd[i] > ypos)
		      {
			j = i;
			ypos = ds->xd[i];
		      }
		  }
		find_max_around(ds->xd, ds->nx-fir, j, &ypos, &Max_val, NULL);
		pd[iy].fl[ix] = ypos + (fir/2);
		if (maxval == 1)
		  {
		    oid->im.pxl[1][iy].fl[ix] = Max_val/fir;
		  }
	      }
	  }
      }
    if (ds != NULL) free_data_set(ds);
    map_pixel_ratio_of_image_and_screen(oid, 1, 1);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}


int substract_z_profile_from_movie(void)
{
    int i;
    int  nfi;
    O_i *ois = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    imreg *imr;
    static float offset = 0;
    static int  start = -1, end = -1;
    char question[1024] = {0};
    union pix *pd = NULL;
    int  j, im, donx, dony, color, itmp;
    float min, max, mean, fcolor;
    double dcolor;



    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) < 2)   return OFF;

    if (ois->o_p == NULL || ois->n_op <= 0)   return OFF;

    op = find_oi_cur_op(ois);//ois->o_p[ois->cur_op];
    nfi = (ois != NULL) ? abs(ois->im.n_f) : 0;
    nfi = (nfi == 0) ? 1 : nfi;

    if (updating_menu_state != 0)
    {
        if (nfi <= 1 || op == NULL)// || op->dat[op->cur_dat]->nx != nfi)
            active_menu->flags |=  D_DISABLED;
        else               active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }

    if (nfi <= 1 || op == NULL)
        return win_printf_OK("This is not a movie or you need to have a plot with the profile attached!");

    if (op->dat[op->cur_dat]->nx != nfi)
        return win_printf_OK("The plot point number does not match the number of frames in the movie!");

    end = nfi;
    ds = op->dat[op->cur_dat];

    for (i = 0, min = max = ds->yd[0], mean = 0; i < nfi && i < ds->nx; i++)
    {
        min = (min > ds->yd[i]) ? ds->yd[i] : min;
        max = (max < ds->yd[i]) ? ds->yd[i] : max;
        mean += ds->yd[i];
    }

    mean = (i > 0) ? mean / i : mean;

    start = (start == -1) ? 0 : start;
    end = (end == -1 || nfi < end) ? nfi : end;
    snprintf(question, sizeof(question), "I shall modify your movie by substracting a specifc value each frame\n"
             "this value is taken from a plot having at least one point per frame\n"
             "Your plot averaged over %d frames has a mean value of %g\n"
             "with a min of %g and a max of %g\n"
             "for restricted dynamics image format you may add an offset %%6f\n"
             "from frames starting at %%6d ending at %%6d"
             , i, mean, min, max);
    i = win_scanf(question, &offset, &start, &end);

    if (i == WIN_CANCEL)  return D_O_K;

    start = (start < 0) ? 0 : start;
    end = (nfi < end) ? nfi : end;

    donx = ois->im.nx;
    dony = ois->im.ny;

    if (ois->im.data_type == IS_CHAR_IMAGE)
    {
        for (im = start; im < end; im++)
        {
            switch_frame(ois, im);
            pd = ois->im.pixel;
            color = (int)(0.5 - ds->yd[im] + offset);

            for (i = 0; i < dony; i++)
            {
                for (j = 0; j < donx ; j++)
                {
                    itmp = color + pd[i].ch[j];

                    if (itmp < 0) itmp = 0;
                    else if (itmp > 255) itmp = 255;

                    pd[i].ch[j] = itmp;
                }
            }
        }
    }
    else if (ois->im.data_type == IS_RGB_PICTURE)
    {
        for (im = start; im < end; im++)
        {
            switch_frame(ois, im);
            pd = ois->im.pixel;
            color = (int)(0.5 - ds->yd[im] + offset);

            for (i = 0; i < dony; i++)
            {
                for (j = 0; j < 3 * donx ; j++)
                {
                    itmp = color + pd[i].ch[j];

                    if (itmp < 0) itmp = 0;
                    else if (itmp > 255) itmp = 255;

                    pd[i].ch[j] = itmp;
                }
            }
        }
    }
    else if (ois->im.data_type == IS_RGBA_PICTURE)
    {
        for (im = start; im < end; im++)
        {
            switch_frame(ois, im);
            pd = ois->im.pixel;
            color = (int)(0.5 - ds->yd[im] + offset);

            for (i = 0; i < dony; i++)
            {
                for (j = 0; j < 4 * donx ; j++)
                {
                    itmp = color + pd[i].ch[j];

                    if (itmp < 0) itmp = 0;
                    else if (itmp > 255) itmp = 255;

                    pd[i].ch[j] = itmp;
                }
            }
        }
    }
    else if (ois->im.data_type == IS_RGB16_PICTURE)
    {
        for (im = start; im < end; im++)
        {
            switch_frame(ois, im);
            pd = ois->im.pixel;
            color = (int)(0.5 - ds->yd[im] + offset);

            for (i = 0; i < dony; i++)
            {
                for (j = 0; j < 3 * donx ; j++)
                {
                    itmp = color + pd[i].ui[j];

                    if (itmp < 0) itmp = 0;
                    else if (itmp > 65535) itmp = 65535;

                    pd[i].ui[j] = itmp;
                }
            }
        }
    }
    else if (ois->im.data_type == IS_RGBA16_PICTURE)
    {
        for (im = start; im < end; im++)
        {
            switch_frame(ois, im);
            pd = ois->im.pixel;
            color = (int)(0.5 - ds->yd[im] + offset);

            for (i = 0; i < dony; i++)
            {
                for (j = 0; j < 4 * donx ; j++)
                {
                    itmp = color + pd[i].ui[j];

                    if (itmp < 0) itmp = 0;
                    else if (itmp > 65535) itmp = 65535;

                    pd[i].ui[j] = itmp;
                }
            }
        }
    }
    else if (ois->im.data_type == IS_INT_IMAGE)
    {
        for (im = start; im < end; im++)
        {
            switch_frame(ois, im);
            pd = ois->im.pixel;
            color = (int)(0.5 - ds->yd[im] + offset);

            for (i = 0; i < dony; i++)
            {
                for (j = 0; j < donx ; j++)
                {
                    itmp = color + pd[i].in[j];

                    if (itmp < -32768) itmp = -32768;
                    else if (itmp > 32767) itmp = 32767;

                    pd[i].ch[j] = itmp;
                }
            }
        }
    }
    else if (ois->im.data_type == IS_UINT_IMAGE)
    {
        for (im = start; im < end; im++)
        {
            switch_frame(ois, im);
            pd = ois->im.pixel;
            color = (int)(0.5 - ds->yd[im] + offset);

            for (i = 0; i < dony; i++)
            {
                for (j = 0; j < donx ; j++)
                {
                    itmp = color + pd[i].ui[j];

                    if (itmp < 0) itmp = 0;
                    else if (itmp > 65535) itmp = 65535;

                    pd[i].ui[j] = itmp;
                }
            }
        }
    }
    else if (ois->im.data_type == IS_LINT_IMAGE)
    {
        for (im = start; im < end; im++)
        {
            switch_frame(ois, im);
            pd = ois->im.pixel;
            color = (int)(0.5 - ds->yd[im] + offset);

            for (i = 0; i < dony; i++)
            {
                for (j = 0; j < donx ; j++)
                    pd[i].li[j] += color;
            }
        }
    }
    else if (ois->im.data_type == IS_FLOAT_IMAGE)
    {
        for (im = start; im < end; im++)
        {
            switch_frame(ois, im);
            pd = ois->im.pixel;
            fcolor = offset - ds->yd[im];

            for (i = 0; i < dony; i++)
            {
                for (j = 0; j < donx ; j++)
                    pd[i].fl[j] += fcolor;
            }
        }
    }
    else if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {
        for (im = start; im < end; im++)
        {
            switch_frame(ois, im);
            pd = ois->im.pixel;
            fcolor = offset - ds->yd[im];

            for (i = 0; i < dony; i++)
            {
                for (j = 0; j < 2 * donx ; j++)
                    pd[i].fl[j] += fcolor;
            }
        }
    }
    else if (ois->im.data_type == IS_DOUBLE_IMAGE)
    {
        for (im = start; im < end; im++)
        {
            switch_frame(ois, im);
            pd = ois->im.pixel;
            dcolor = offset - ds->yd[im];

            for (i = 0; i < dony; i++)
            {
                for (j = 0; j < donx ; j++)
                    pd[i].db[j] += dcolor;
            }
        }
    }
    else if (ois->im.data_type == IS_COMPLEX_DOUBLE_IMAGE)
    {
        for (im = start; im < end; im++)
        {
            switch_frame(ois, im);
            pd = ois->im.pixel;
            dcolor = offset - ds->yd[im];

            for (i = 0; i < dony; i++)
            {
                for (j = 0; j < 2 * donx ; j++)
                    pd[i].db[j] += dcolor;
            }
        }
    }

    ois->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->cur_oi));
}


int assemble_multiple_small_images_from_movie(void)
{
    int i;
    //int snx = 0, sny = 0;
    int  onx, ony, nfi, posx, posy, type, ROI_set, lx0, lnx, ly0, lny;
    O_i   *oid = NULL, *ois = NULL;
    p_l *pl = NULL;
    imreg *imr = NULL;
    static int x0 = 0, nx = 64, y0 = 0, ny = 32, bd = 5, bdc = 0, col = 0;
    static int nfx = 4, nfy = 3, start = 0, every = 3, end = 1, sw = 0;
    char color[64] = {0};

    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) < 1)   return OFF;

    nfi = abs(ois->im.n_f);
    nfi = (nfi == 0) ? 1 : nfi;

    if (updating_menu_state != 0)
    {
        if (nfi <= 1)      active_menu->flags |=  D_DISABLED;
        else               active_menu->flags &= ~D_DISABLED;

        return D_O_K;
    }


    if (ois != NULL)
    {
        //snx = ois->im.nx;
        //sny = ois->im.ny;
        type = ois->im.data_type;
    }

    end = nfi;
    ROI_set = (ois->im.nxs > 0 || ois->im.nys > 0 || ois->im.nxe < ois->im.nx || ois->im.nye < ois->im.ny) ? 1 : 0;

    if (ROI_set)
    {
        lx0 = ois->im.nxs;
        lnx = ois->im.nxe - ois->im.nxs;
        ly0 = ois->im.nys;
        lny = ois->im.nye - ois->im.nys;
    }
    else
    {
        lx0 = x0;
        lnx = nx;
        ly0 = y0;
        lny = ny;
    }

    i = win_scanf("Construction of a composite image from partial image taken from different movie frames\n"
                  "Specify the origin and size of the sub image in the movie:\n"
                  "X0 %6d size in X %6d Y0 %6d size in Y %6d\n"
                  "Normal%R or swap XY%r \nBorder size %6d Grey level %6d\n"
                  "How many sub-images in line %4d How many sub-images in row %4d\n"
                  "Grab sub images starting at frame %6d Every %6d frames ending at %6d\n"
                  "Label color White->%R Black->%r Red->%r Blue->%r Green->%r\n"
                  , &lx0, &lnx, &ly0, &lny, &sw, &bd, &bdc, &nfx, &nfy, &start, &every, &end, &col);

    if (i == WIN_CANCEL) return  D_O_K;

    onx = (sw) ? nfx * (lny + bd + bd) : nfx * (lnx + bd + bd);
    ony = (sw) ? nfy * (lnx + bd + bd) : nfy * (lny + bd + bd);
    ony += bd;
    oid = create_and_attach_oi_to_imr(imr, onx, ony, type);

    if (oid == NULL)  return win_printf_OK("cannot create profile !");

    if (col == 0) snprintf(color, sizeof(color), "white");
    else if (col == 1) snprintf(color, sizeof(color), "black");
    else if (col == 2) snprintf(color, sizeof(color), "lightred");
    else if (col == 3) snprintf(color, sizeof(color), "lightblue");
    else if (col == 4) snprintf(color, sizeof(color), "lightgreen");
    else snprintf(color, sizeof(color), "Lightred");

    set_all_pixel_to_color(oid, bdc, 0.0);

    for (i = start; i < end; i += every)
    {
        switch_frame(ois, i);

        if (sw == 0)
        {
            posx = bd + (bd + lnx + bd) * ((((i - start) / every)) % nfx);
            posy = bd + (bd + lny + bd) * ((((i - start) / every)) / nfx);
            posy = ony - posy - lny;
            copy_one_subset_of_image_in_a_bigger_one(oid, posx, posy, ois, lx0, ly0, lnx, lny);

            //create_and_attach_label_to_oi(oid, posx + lnx/2, posy -bd - lny, USR_COORD, "{\\pt5 %d}", i);
            if (ois->n_tu == 0)     pl = create_plot_label(USR_COORD, posx + lnx / 2, posy - bd ,
                                             "\\color{%s}\\center{\\pt5 %d}", color, i);
            else pl = create_plot_label(USR_COORD, posx + lnx / 2, posy - bd
                                            , "\\color{%s}\\center{\\pt5 %g %s}", color,
                                            ois->tu[ois->c_tu]->ax + ois->tu[ois->c_tu]->dx * i
                                            , ((ois->tu[ois->c_tu]->name) ? ois->tu[ois->c_tu]->name : ""));

            add_to_one_image(oid, IS_PLOT_LABEL, (void *)pl);
        }
        else
        {
            posx = bd + (bd + lny + bd) * ((((i - start) / every)) % nfx);
            posy = bd + (bd + lnx + bd) * ((((i - start) / every)) / nfx);
            posy = ony - posy - lnx;
            copy_and_swap_one_subset_of_image_in_a_bigger_one(oid, posx, posy, ois, lx0, ly0, lnx, lny);

            //create_and_attach_label_to_oi(oid, posx + lny/2, posy -bd - lnx, USR_COORD, "{\\pt5 %d}", i);
            if (ois->n_tu == 0)     pl = create_plot_label(USR_COORD, posx + lny / 2, posy - bd
                                             , "\\color{%s}\\center{\\pt5 %d}", color, i);
            else pl = create_plot_label(USR_COORD, posx + lny / 2, posy - bd
                                            , "\\color{%s}\\center{\\pt5 %g %s}", color,
                                            ois->tu[ois->c_tu]->ax + ois->tu[ois->c_tu]->dx * i
                                            , ((ois->tu[ois->c_tu]->name) ? ois->tu[ois->c_tu]->name : ""));

            add_to_one_image(oid, IS_PLOT_LABEL, (void *)pl);
        }
    }

    if (ROI_set == 0)
    {
        x0 = lx0;
        nx = lnx;
        y0 = ly0;
        ny = lny;
    }


    //find_zmin_zmax(oid);
    oid->z_black = ois->z_black;
    oid->z_white = ois->z_white;
    oid->z_min = ois->z_min;
    oid->z_max = ois->z_max;

    if (sw == 0)
    {
        uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid, IS_X_UNIT_SET);
        uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid, IS_Y_UNIT_SET);
    }
    else
    {
        uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid, IS_Y_UNIT_SET);
        uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid, IS_X_UNIT_SET);
    }

    inherit_from_im_to_im(oid, ois);
    map_pixel_ratio_of_image_and_screen(oid, 1, 1);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}




int do_average_partial_movie(void)
{
    int i;
    int  nfi;
    O_i *ois = NULL, *oid = NULL;
    imreg *imr = NULL;
    static int  start = -1, nf = 32, end = -1;


    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) < 2)   return OFF;

    nfi = (ois != NULL) ? abs(ois->im.n_f) : 0;
    nfi = (nfi == 0) ? 1 : nfi;

    if (updating_menu_state != 0)
    {
        if (nfi <= 1)      active_menu->flags |=  D_DISABLED;
        else               active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    end = nfi;
    start = (start == -1) ? 0 : start;
    end = (end == -1 || nfi < end) ? nfi : end;
    i = win_scanf("Average movie starting at frame %4d over %4d\n", &start, &end);

    if (i == WIN_CANCEL)  return D_O_K;
    end = start + nf;
    start = (start < 0) ? 0 : start;
    end = (nfi < end) ? nfi : end;
    nf = end - start;

    oid = average_partial_movie(ois, NULL, start, nf);
    if (oid == NULL)  return D_O_K;
    add_image(imr, oid->type, (void*)oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
    
}


int	do_flatten_movie_by_histo_on_Z_profile(void)
{
  int i, j, nf, k;
  static int wl = 1;
  static float level = 127;//, biny = 2;
  O_i   *ois = NULL, *oid = NULL, *oid2 = NULL;
  d_s  *dst = NULL;
  imreg *imr = NULL;
  //float mean, ypos;
	
  if(updating_menu_state != 0)	return D_O_K;		

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	
    return win_printf_OK("This routine project z profiles\n");

  nf = abs(ois->im.n_f);		
  if (nf <= 0)	
    {
      win_printf("This is not a movie");
      return D_O_K;
    }
  if (ois->im.data_type != IS_CHAR_IMAGE)
    return win_printf_OK("This is not a 8bits image!");
  i = win_scanf("Flatten %Rblack level %ror while level\n"
		"Define this level value %5f\n"
		,&wl,&level);
  if (i == WIN_CANCEL)   return 0;

  oid = create_and_attach_movie_to_imr(imr, ois->im.nx, ois->im.ny, ois->im.data_type, nf);
  if (oid == NULL)	return win_printf_OK("cannot create profile !");

  oid2 = create_and_attach_oi_to_imr(imr, ois->im.nx, ois->im.ny, ois->im.data_type);
  if (oid2 == NULL)	return win_printf_OK("cannot create profile !");

  dst = build_data_set(256,256);
  if (dst == NULL)	return win_printf_OK("cannot create profile !");
  int imax = 0;
  unsigned char val, umin = 0, umax = 0;
  float max, tmp;
  d_s *dst2 = build_data_set(nf,nf);
  if (dst2 == NULL)	return win_printf_OK("cannot create profile !");

  for(i = 0; i < ois->im.ny; i++)
    {
      for(j = 0; j < ois->im.nx; j++)
	{
	  for (k = 0; k < dst->nx; k++) dst->xd[k] = dst->yd[k] = 0;
	  for (k = 0; k < nf; k++)
	    {
	      switch_frame(ois, k);
	      val = ois->im.pixel[i].ch[j];
	      dst->yd[val]++;
	      dst2->xd[k] = val;
	      umin = (k == 0 || val < umin) ? val : umin;
	      umax = (k == 0 || val > umax) ? val : umax;
	    }
	  for (k = (wl) ? umax : umin, max = 0; k < dst->nx && k >= 0; k = (wl) ? k-1 : k+1)
	    {
	      max += dst->yd[imax = k];
	      if (max > nf/20) break;
	    }
	  //extract_z_profile_from_movie(ois, j, i, dst->yd);
	  //find_max_of_histo_of_ds_with_gaussian_convolution(dst, biny, &ypos, NULL);
	  /*
	  for(k = 0; k < nf; k++)
	    {
	      if (wl) dst->yd[i] = (ypos > 0) ? level*(dst->yd[k] - ypos)/ypos : 0;
	      else dst->yd[i] = (dst->yd[k] - ypos);
	      dst->yd[i] += level;
	    }
	  set_z_profile_from_movie(oid, j, i, dst->xd, nf);
	  */
	  oid2->im.pixel[i].ch[j] = (unsigned char)imax;
	  for(k = 0; k < nf; k++)
	    {
	      if (wl) tmp = (imax > 0) ? level*(dst2->xd[k] - imax)/imax : 0;
	      else tmp = (dst2->xd[k] - imax);
	      tmp += level;
	      tmp = (tmp < 0) ? 0 : tmp;
	      tmp = (tmp < 256) ? tmp : 255;
	      switch_frame(oid, k);
	      oid->im.pixel[i].ch[j] = (unsigned char)tmp;
	    }	  
	}
      my_set_window_title("Processing line %d",i);
    }

  inherit_from_im_to_im(oid, ois);	
  oid->filename = Transfer_filename(ois->filename);	
  find_zmin_zmax(oid);
  uns_oi_2_oi(oid,ois);	/* heritage des unites de l'image 1 			*/
  oid->im.win_flag = ois->im.win_flag;	
  oid->need_to_refresh = ALL_NEED_REFRESH;
  free_data_set(dst);
  free_data_set(dst2);
  return (refresh_image(imr, imr->n_oi - 1));
}




/*
 *		dest = ois1 - ois2
 *
 */
O_i 	*convert_float_image_to_16bits(O_i *dest, O_i *ois1)
{
    int i, j, im;
    int  onx, ony, itmp, nf, nfd;
    int	dest_type = 0;
    float tmp;
    union pix *pd = NULL, *ps1 = NULL;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    nf = ois1->im.n_f;
    if (nf < 1) nf = 1;
    if (ois1->im.data_type != IS_FLOAT_IMAGE)
    {
        win_printf ("the image is not of float type!\n"
                    "Cannot transform!");
        return(NULL);
    }

    if (dest != NULL)
    {
        if (dest->im.data_type != IS_UINT_IMAGE)
        {
            win_printf ("The destination image is not of unsigned char type\n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        if (ony != dest->im.ny || onx != dest->im.nx)
        {
            win_printf ("The destination size differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        nfd = dest->im.n_f;
        if (nfd < 1) nfd = 1;
        if (nf != nfd)
        {
            win_printf ("The destination nb of frame differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }

    }
    else
        dest_type = IS_UINT_IMAGE;


    float zmin = ois1->z_min, zmax = ois1->z_max;
    
    i = win_scanf("Converting Z value to [0,2^{16}[\n"
		  "O => zmin = %6f\n"
		  "2^{16} => zmax = %6f\n",&zmin,&zmax);
    if (i == WIN_CANCEL) return 0;    

    
    if (nf == 1)
        dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
    else dest = (dest != NULL) ? dest : create_one_movie(onx, ony, dest_type, nf);
    if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }

    for (im = 0; im < nf; im++)
    {
        switch_frame(ois1,im);
        switch_frame(dest,im);
        pd = dest->im.pixel;	ps1 = ois1->im.pixel;
        for (i=0; i< ony; i++)
        {
            for (j=0; j<onx; j++)
            {
                tmp = ps1[i].fl[j] - zmin;
                tmp /= zmax - zmin;
                tmp *= 65536;
                itmp = (int)(tmp+0.5);
                itmp = (itmp < 0) ? 0 : itmp;
                itmp = (itmp > 65535) ? 65535 : itmp;
                pd[i].ui[j] = (unsigned short int)itmp;
            }
        }
    }
    inherit_from_im_to_im(dest,ois1);
    uns_oi_2_oi(dest,ois1);
    update_oi_treatment(dest, "Float image converted to unsigned short int :"
			" Zui = (Zf - %f)/(%f - %f)", zmin, zmax, zmin);
    dest->im.win_flag = ois1->im.win_flag;
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}



/*
 *		dest = ois1 - ois2
 *
 */
O_i 	*convert_float_image_to_signed_16bits(O_i *dest, O_i *ois1)
{
    int i, j, im;
    int  onx, ony, itmp, nf, nfd;
    int	dest_type = 0;
    float tmp;
    union pix *pd = NULL, *ps1 = NULL;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    nf = ois1->im.n_f;
    if (nf < 1) nf = 1;
    if (ois1->im.data_type != IS_FLOAT_IMAGE)
    {
        win_printf ("the image is not of float type!\n"
                    "Cannot transform!");
        return(NULL);
    }

    if (dest != NULL)
    {
        if (dest->im.data_type != IS_INT_IMAGE)
        {
            win_printf ("The destination image is not of unsigned char type\n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        if (ony != dest->im.ny || onx != dest->im.nx)
        {
            win_printf ("The destination size differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        nfd = dest->im.n_f;
        if (nfd < 1) nfd = 1;
        if (nf != nfd)
        {
            win_printf ("The destination nb of frame differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }

    }
    else
        dest_type = IS_INT_IMAGE;


    float zmin = ois1->z_min, zmax = ois1->z_max;
    
    i = win_scanf("Converting Z value to [0,2^{16}[\n"
		  "O => zmin = %6f\n"
		  "2^{16} => zmax = %6f\n",&zmin,&zmax);
    if (i == WIN_CANCEL) return 0;    

    
    if (nf == 1)
        dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
    else dest = (dest != NULL) ? dest : create_one_movie(onx, ony, dest_type, nf);
    if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }

    for (im = 0; im < nf; im++)
    {
        switch_frame(ois1,im);
        switch_frame(dest,im);
        pd = dest->im.pixel;	ps1 = ois1->im.pixel;
        for (i=0; i< ony; i++)
        {
            for (j=0; j<onx; j++)
            {
                tmp = ps1[i].fl[j] - zmin;
                tmp /= zmax - zmin;
                tmp *= 65536;
		tmp -= 32768;
                itmp = (int)(tmp+0.5);
                itmp = (itmp < -32768) ? -32768 : itmp;
                itmp = (itmp > 32767) ? 32767 : itmp;
                pd[i].in[j] = (short int)itmp;
            }
        }
    }
    inherit_from_im_to_im(dest,ois1);
    uns_oi_2_oi(dest,ois1);
    update_oi_treatment(dest, "Float image converted to signed short int :"
			" Zui = (Zf - %f)/(%f - %f)", zmin, zmax, zmin);
    dest->im.win_flag = ois1->im.win_flag;
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}


int		do_convert_float_image_to_16bits(void)
{
    O_i   *oid = NULL, *ois = NULL;
    imreg *imr = NULL;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    oid = convert_float_image_to_16bits(NULL, ois);
    if (oid == NULL)	return D_O_K;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}


int		do_convert_float_image_to_signed_16bits(void)
{
    O_i   *oid = NULL, *ois = NULL;
    imreg *imr = NULL;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    oid = convert_float_image_to_signed_16bits(NULL, ois);
    if (oid == NULL)	return D_O_K;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}


/*
 *		dest = ois1 - ois2
 *
 */
O_i 	*convert_16bits_image_to_8bits(O_i *dest, O_i *ois1)
{
    int i, j, im;
    int  onx, ony, itmp, nf, nfd;
    int	dest_type = 0;
    float tmp;
    union pix *pd = NULL, *ps1 = NULL;

    if (ois1 == NULL || ois1->im.pixel == NULL)
    {
        win_printf("No image found");
        return (NULL);
    }
    ony = ois1->im.ny;	onx = ois1->im.nx;
    nf = ois1->im.n_f;
    if (nf < 1) nf = 1;
    if (ois1->im.data_type != IS_UINT_IMAGE)
    {
        win_printf ("the image is not of 16bits type!\n"
                    "Cannot transform!");
        return(NULL);
    }

    if (dest != NULL)
    {
        if (dest->im.data_type != IS_CHAR_IMAGE)
        {
            win_printf ("The destination image is not of unsigned char type\n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        if (ony != dest->im.ny || onx != dest->im.nx)
        {
            win_printf ("The destination size differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }
        nfd = dest->im.n_f;
        if (nfd < 1) nfd = 1;
        if (nf != nfd)
        {
            win_printf ("The destination nb of frame differs from the source one \n"
                        "Cannot perform conversion!");
            return(NULL);
        }

    }
    else
        dest_type = IS_CHAR_IMAGE;


    float zmin = ois1->z_min, zmax = ois1->z_max;
    
    i = win_scanf("Converting Z value to [0,2^{16}[\n"
		  "O => zmin = %6f\n"
		  "2^{16} => zmax = %6f\n",&zmin,&zmax);
    if (i == WIN_CANCEL) return 0;    

    
    if (nf == 1)
        dest = (dest != NULL) ? dest : create_one_image(onx, ony, dest_type);
    else dest = (dest != NULL) ? dest : create_one_movie(onx, ony, dest_type, nf);
    if (dest == NULL)
    {
        win_printf("Can't create dest image");
        return(NULL);
    }

    for (im = 0; im < nf; im++)
    {
        switch_frame(ois1,im);
        switch_frame(dest,im);
        pd = dest->im.pixel;	ps1 = ois1->im.pixel;
        for (i=0; i< ony; i++)
        {
            for (j=0; j<onx; j++)
            {
	      tmp = (float)ps1[i].ui[j] - zmin;
	      tmp /= zmax - zmin;
	      tmp *= 256;
	      itmp = (int)(tmp+0.5);
	      itmp = (itmp < 0) ? 0 : itmp;
	      itmp = (itmp > 256) ? 256 : itmp;
	      pd[i].ch[j] = (unsigned char)itmp;
            }
        }
    }
    inherit_from_im_to_im(dest,ois1);
    uns_oi_2_oi(dest,ois1);
    update_oi_treatment(dest, "16 bits image converted to 8 bits :"
			" Zui = (Zf - %f)/(%f - %f)", zmin, zmax, zmin);
    dest->im.win_flag = ois1->im.win_flag;
    dest->need_to_refresh = ALL_NEED_REFRESH;
    return dest;
}

int  do_convert_16bits_image_to_8bits(void)
{
    O_i   *oid = NULL, *ois = NULL;
    imreg *imr = NULL;

    if(updating_menu_state != 0)	return D_O_K;

    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
    oid = convert_16bits_image_to_8bits(NULL, ois);
    if (oid == NULL)	return D_O_K;
    add_image(imr, oid->type, (void*)oid);
    find_zmin_zmax(oid);
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, imr->n_oi - 1));
}



O_i	*cut_submovie(O_i *ois, int  dlx, int dly, int urx, int ury, int start, int nfo)
{
  register int i, j, k=0;
  O_i *oid;
  int onx, ony, data_type;
  int nf;
  union pix *ps, *pd;

  onx = urx - dlx;
  ony = ury - dly;
  data_type = ois->im.data_type;
  nf =ois->im.n_f;

  nfo = (nfo < nf) ? nfo : nf;
  oid =  create_one_movie(onx, ony, data_type, nfo);


  if (oid == NULL)
    {
      win_printf("Can't create dest image");
      return NULL;
    }
  for(k = 0; (start+k) < nf && k <nfo; k++)
    {
      switch_frame(ois,k+start);
      switch_frame(oid,k);
      if (data_type == IS_CHAR_IMAGE)
	{
	  for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	    {
	      for (j=0; j< onx; j++)
		{
		  pd[i].ch[j] = ps[i+dly].ch[j+dlx];
		}
	    }
	}
      else if (data_type == IS_INT_IMAGE)
	{
	  for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	    {
	      for (j=0; j< onx; j++)
		{
		  pd[i].in[j] = ps[i+dly].in[j+dlx];
		}
	    }
	}
      else if (data_type == IS_UINT_IMAGE)
	{
	  for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	    {
	      for (j=0; j< onx; j++)
		{
		  pd[i].ui[j] = ps[i+dly].ui[j+dlx];
		}
	    }
	}
      else if (data_type == IS_LINT_IMAGE)
	{
	  for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	    {
	      for (j=0; j< onx; j++)
		{
		  pd[i].li[j] = ps[i+dly].li[j+dlx];
		}
	    }
      }
      else if (data_type == IS_FLOAT_IMAGE)
	{
	  for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	    {
	      for (j=0; j< onx; j++)
		{
		  pd[i].fl[j] = ps[i+dly].fl[j+dlx];
		}
	    }
	}
      else if (data_type == IS_DOUBLE_IMAGE)
	{
	  for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	    {
	      for (j=0; j< onx; j++)
		{
		  pd[i].db[j] = ps[i+dly].db[j+dlx];
	      }
	    }
	}
      else if (data_type == IS_COMPLEX_IMAGE) 
	{ 
	  for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++) 
	    { 
	      for (j=0; j< onx; j++) 
		{
		  pd[i].cp[j] = ps[i+dly].cp[j+dlx];
		}
	    }
	}
      else if (data_type == IS_COMPLEX_DOUBLE_IMAGE)
	{
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j=0; j< onx; j++)
	      {
		pd[i].dcp[j] = ps[i+dly].dcp[j+dlx];		
	      }
	  }
      }
    else if (data_type == IS_RGB_PICTURE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j=0; j< 3*onx; j++)
	      {
		pd[i].rgb[j] = ps[i+dly].rgb[j+dlx];
	      }
	  }
      }
    else if (data_type == IS_RGBA_PICTURE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j=0; j< 4*onx; j++)
	      {
		pd[i].rgba[j] = ps[i+dly].rgba[j+dlx];
	      }
	  }
      }
    else if (data_type == IS_RGB16_PICTURE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j=0; j< 3*onx; j++)
	      {
		pd[i].rgb16[j] = ps[i+dly].rgb16[j+dlx];
	      }
	  }
      }
    else if (data_type == IS_RGBA16_PICTURE)
      {
	for (i = 0, ps = ois->im.pixel, pd = oid->im.pixel; i < ony ; i++)
	  {
	    for (j=0; j< 4*onx; j++)
	      {
		pd[i].rgba16[j] = ps[i+dly].rgba16[j+dlx];
	      }
	  }
      }
    else win_printf("not implemented");
  }
  inherit_from_im_to_im(oid,ois);
  oid->width = ois->width*onx/ois->im.nx;
  oid->width = ois->height*ony/ois->im.ny;
  uns_oi_2_oi(oid,ois);
  oid->im.win_flag = ois->im.win_flag;
  if (ois->title != NULL)	set_im_title(oid, "%s",
  					     ois->title);
  //set_formated_string(&oid->im.treatement,
  //	"Image %s", ois->filename);
  return oid;
}

int do_submovie(void)
{
    O_i *ois=NULL, *oid=NULL;
    imreg *imr;
    int i;
    static int onx = 256, ony = 256, x0 = 0, y0 = 0, center = 1, nfo = 256, start = 0;
    int dlx =200, dly=200, urx=400, ury=400;
    
    if(updating_menu_state != 0)	return D_O_K;
    
    if (key[KEY_LSHIFT])
      {
	return win_printf_OK("This routine cuts a movie");
      }
    if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
      {
	win_printf("Cannot find image");
	return D_O_K;
      }
    //map_pixel_ratio_of_image_and_screen(ois, 1, 1);
    i = win_scanf("Set position and size in X and Y\n"
		  "Define new size in X = %4d and in Y = %4d\n"
		  "Use %R->Bottom/left corner or %r->center\n"
		  "x_0 %4d,y_0 %4d\n"
		  "Number of images to keep %4d\n"
		  "Starting at image %d\n"
		  ,&onx,&ony,&center,&x0,&y0,&nfo,&start);
    //,&dlx,&dly,&urx,&ury);
    if (i == WIN_CANCEL)	return D_O_K;
    if (center)
      {
	dlx = x0-(onx/2);
	dly = y0-(ony/2);
	urx = x0+(onx/2);
	ury = y0+(ony/2);
      }
    else
      {
	dlx = x0;
	dly = y0;
	urx = x0+onx;
	ury = y0+ony;	
      }
    oid = cut_submovie(ois,dlx,dly,urx,ury,start,nfo);
    if (oid == NULL)	return win_printf_OK("unable to create image!");
    add_image(imr, oid->type, (void*)oid);
    map_pixel_ratio_of_image_and_screen(oid, 1, 1);
    find_zmin_zmax(oid);
    return (refresh_image(imr, imr->n_oi - 1));
}

