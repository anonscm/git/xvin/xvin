/****************************************************************/
/* i_treat_measures.c						*/
/*								*/
/* operations on images giving numbers (measurements)		*/
/****************************************************************/

# include <stdlib.h>
# include <string.h>


#include "allegro.h"
#include "xvin.h"

#include "i_treat_measures.h"


/************************************************************************************************/
/* do_image_cst_measure										*/
/* 2003/02/02											*/
/************************************************************************************************/
/* calcul en un passage (A,omega,k) moyenne et moyenne avec ponderation par A 			*/
/* pour une image filtree 									*/
/************************************************************************************************/
/* this function can be AUTOmatized								*/
/************************************************************************************************/
int do_image_cst_measure(void)
/* inspire par do_average_along_y */
{
    imreg   *imr = NULL;
    O_i	*ois = NULL;

    ac_grep(cur_ac_reg,"%im%oi",&imr,&ois);  
    
    if(updating_menu_state != 0)	
      {
	if (imr != NULL && ois != NULL)
	  {
	    if (imr->n_oi < 1 || ois->im.data_type != IS_COMPLEX_IMAGE) 
	      active_menu->flags |=  D_DISABLED;
	    else active_menu->flags &= ~D_DISABLED;
	  }
	return D_O_K;	
      } 
	
    if (key[KEY_LSHIFT])	/* display routine action if SHIFT is pressed 	*/
      return win_printf_OK("For an image of complex numbers {\\pt14\\color{yellow}A = |A | e^{i \\phi}}\n\n"
			   "this routine computes:\n- the amplitude {\\color{lightred}|A |},\n"
			   "- the phase derivatives {\\color{lightgreen}k_x = {\\frac{\\partial \\phi}{\\partial x}}},"
			   " {\\color{lightgreen}k_y = {\\frac{\\partial \\phi}{\\partial y}} = \\omega}\n\n"
			   "Those quantities are averaged over a user-specified portion\n"
			   "of the image; k_x and k_y are also ponderated by the amplitude.");

    if (ois == NULL)	
      return (win_printf("I cannot access the image !"));
    
    return(image_cst_measure_engine(ois));	
}/* end of the "do_image_cst_measure" function */





/************************************************************************************************/
/* calcul en un passage (A,omega,k) moyenne et moyenne avec ponderation par A 			*/
/* pour une image filtree 									*/
/************************************************************************************************/
/* this is the core of the function "ng_averaging", new version of 25.08.1999			*/
/* this function can be AUTOmatized								*/
/************************************************************************************************/
int image_cst_measure_engine(O_i *oi)
{
    register int i,j,k;			/* compteurs 						*/
    float   ampt=0, kxt=0, kyt=0;	/* reel temporaire 					*/
    int     l_s, l_e, r_s, r_e;	/* limites de la zone de travail en y et en x		*/
    int	N;			/* number of points used				*/
    float   vec, sca, 
      norm_X, norm_Y;		/* intermediaires pour calculer la derivee de la phase 	*/
    int	nx,ny;			/* intermediares pour la taille de l'image source 	*/
    double	amp_moy,		/* amplitude moyenne sur l'image 			*/
      kx_moy,ky_moy,		/* k_x et k_y moyenne sur l'image 			*/
      kx_moy_pond,ky_moy_pond;/* k_x et k_y moyenne, pondere sur l'image 		*/
    char	c[128];			/* string for title update				*/
    union pix *z = NULL;			/* peut etre est-ce mieux en float ...			*/
    float   *z_i = NULL;
    int	total;			/* total number of lines to count percents done		*/
    
    
    /* step 1 : initialization and preliminary tests */
    if (oi->im.data_type != IS_COMPLEX_IMAGE)	
      return win_printf_OK("The image is not a complex one !");
    
    nx=oi->im.nx;
    ny=oi->im.ny; 
    
    l_s=0; l_e=ny-1; r_s=0; r_e=nx;
    if (!(oi->im.win_flag & X_PER)) r_e =nx-1;
    if (!(oi->im.win_flag & Y_PER)) 
      {	l_s = (int)(ny*0.05);
	l_e = (int)(ny*0.95);
      }
    i=win_scanf("\\pt14\\color{yellow} |A | and k_x=\\phi_x, k_y=\\phi_y for complexes |A |e^{i\\phi}\n\n"
		"Proceed between lines%d and %d and between rows%d and %d"
		"\\pt8 note : last line should not be included if not Y-periodic\n"
		"\\pt8 and last colomn should not be included if not X-periodic.",
		&l_s,&l_e,&r_s,&r_e);
    if (i==WIN_CANCEL)	return D_O_K;
    
    if (l_s >= l_e) 	return win_printf_OK("domain problem : \nl_e=%d <= l_s=%d",l_e,l_s);
    if (l_e > ny) 		return win_printf_OK("domain problem : \nl_e=%d > ny=%d",l_e,ny);
    if (l_s < 0) 		return win_printf_OK("domain problem : \nl_s=%d < 0",l_s);
    if (r_s >= r_e) 	return win_printf_OK("domain problem : \nr_e%d <= r_s%d",r_e,r_s);
    if (r_e > nx) 		return win_printf_OK("domain problem : \nr_e=%d > nx=%d",l_e,nx);
    if (r_s < 0) 		return win_printf_OK("domain problem : \nr_s%d < 0",r_s);
    
    
    /* step 2 : calculus */
    z = oi->im.pixel;
    amp_moy = 0;
    kx_moy  = 0;	kx_moy_pond=0;
    ky_moy  = 0;	ky_moy_pond=0;
    N = (l_e-l_s)*(r_e-r_s);
    norm_X = (float)oi->im.nx/(2*M_PI);	/* intermediaire */
    norm_Y = (float)oi->im.ny/(2*M_PI);
    total = l_e-l_s;
    for (i=l_s ; i<l_e ; i++)
      {	z_i = oi->im.pixel[i].fl;
	if ((i%10)==0) 
	  {	sprintf(c, "%d %% done",(int)(100*(i-l_s)/total));	
	    //			if (ti != NULL)	modify_title(ti,c);
	  }
	for (j=r_s; j<r_e ; j++)
	  { 	ampt = sqrt( z[i].fl[2*j] * z[i].fl[2*j]  +  z[i].fl[2*j+1] * z[i].fl[2*j+1] );
	    
	    k = (j+1) % (oi->im.nx);
	    vec = z_i[2*j]*z_i[2*k+1] - z_i[2*j+1]*z_i[2*k];
	    sca = z_i[2*j]*z_i[2*k] + z_i[2*j+1]*z_i[2*k+1];
	    if ( vec==0.0 && sca==0.0)	kxt=0;
	    else 	kxt= norm_X*(float)atan2 ( vec, sca );
	    
	    k = (i+1) % oi->im.ny;
	    vec = z[i].fl[2*j]*z[k].fl[2*j+1]-z[k].fl[2*j]*z[i].fl[2*j+1];
	    sca = z[i].fl[2*j]*z[k].fl[2*j]+z[i].fl[2*j+1]*z[k].fl[2*j+1];
	    if ( vec==0.0 && sca==0.0)	kyt=0;
	    else 	kyt= norm_Y*(float)atan2 (vec, sca );
	    
	    amp_moy += ampt;
	    kx_moy  += kxt;
	    ky_moy  += kyt;
	    kx_moy_pond += kxt*ampt;
	    ky_moy_pond += kyt*ampt;
	  }	
      }
    amp_moy /= N;
    kx_moy  /= N; 
    ky_moy  /= N;
    if (amp_moy==0) { kx_moy_pond=0; ky_moy_pond=0; }
    else		{ kx_moy_pond /= (N*amp_moy); 
      ky_moy_pond /= (N*amp_moy); 
    }
    
    /* step 3  : presentation of the results */
    if (amp_moy==0)
      {	win_printf_OK("for (x,y) in [%d,%d[ * [%d,%d[\n"
		      "amplitude {\\color{lightred}|A |} vanishes!\nno ponderation performed.\n\n"
		      "averaged <k_x> = %3.3f\naveraged <k_y> = \\omega = %3.3f",
		      r_s,r_e,l_s,l_e,
		      (float)kx_moy, (float)ky_moy);
      }
    else
      {	win_printf_OK("Writing the image as {\\pt14\\color{yellow}A(x,y) = |A | e^{i\\phi}}\n\n"
		      "then for (x,y) in [%d, %d [ * [%d, %d [\n"
		      "averaged quantities are :\n\n"
		      " {\\color{lightred}|A | = %3.3f} (amplitude)\n"
		      " <k_x> = %3.3f\n"
		      " <k_y> = %3.3f\n"
		      " {\\color{lightgreen}<k_x>^{pond} = %3.3f} (ponderated)\n"
		      " {\\color{lightgreen}<k_y>^{pond} = %3.3f} (ponderated)",
		      r_s,r_e,l_s,l_e,
		      (float)amp_moy,(float)kx_moy,(float)ky_moy,(float)kx_moy_pond,(float)ky_moy_pond);
      }
    return(D_O_K);	
}/* end of the "image_cst_measure" function */



