/********************************************************************************/
/* functions to perform basic operations on images				*/
/********************************************************************************/
/* Vincent Croquette, Nicolas Garnier						*/
/*	nicolas.garnier@ens-lyon.fr						*/
/********************************************************************************/

// definitions for indicating X or Y direction when calling a function from a menu:
#define X_DIRECTION	0x0100
#define Y_DIRECTION	0x0200

// definitions for do_amp:
#define DO_AMP_RE	0x0100
#define DO_AMP_IM	0x0200
#define DO_AMP_AMP_2	0x0300
#define DO_AMP_AMP	0x0400


int	extrapolate_array(float *in, int nxi, float *out, int nxo);	// used by resample_image
int	fast_resample_array(float *in, int nxi, float *out, int nxo);	// used by resample_image
int 	do_resample_image(void);		// resample an image in x or y, a new image is created

int 	do_normalize(void);			// normalization / a new image is created
int 	do_normalize_optimized(void);		// normalization / the image is modified

int 	do_im_phase_multiplication(void);	// multiplication of an image by a phase exp( i (kx*x + ky*y))
int 	do_im_decalage(void); 			// shift of lines (ie, change of referential for space-time plots), old image is modified
int 	do_binarize(void);			// "binarization" of an image, a new one is created

int 	do_keep_visible_image(void);		// crop an image to the visible portion of it visible on the screen

int 	do_im_kx(void);				// phase derivative along x
int 	do_im_ky(void);				// 
int 	do_amp(void);				// keep a projection of a complex image
int     do_im_phi(void);
int     do_im_2pts_deriv_in_x(void);
int     do_im_2pts_deriv_in_y(void);

int 	do_im_swap_X_Y(void);			// swap X and Y for an image
int     do_upside_down_image_right_to_left(void);
int     do_upside_down_image(void);
int	assemble_multiple_small_images_from_movie(void);
int	substract_z_profile_from_movie(void);
int     do_average_partial_movie(void);
int	do_flatten_movie_by_histo_on_Z_profile(void);
int		do_convert_float_image_to_16bits(void);
int		do_convert_float_image_to_signed_16bits(void);
int do_submovie(void);
int do_cut_image_in_slice_to_movie(void);
