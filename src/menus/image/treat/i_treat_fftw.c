/********************************************************************************/
/* FT and Filtering, using FFTW3						*/
/********************************************************************************/
/* Nicolas Garnier	nicolas.garnier@ens-lyon.fr				*/
/********************************************************************************/

# include <stdlib.h>
# include <string.h>

#include "xvin.h"
#include "i_treat_fftw.h"

#include <fftw3.h>		// include the fftw v3.0.1 library for computing spectra:
#include "fft_filter_lib.h"	// (fftw3 is also included from here)



int find_selected_imr_area(imreg *imr, int x_0, int y_0, DIALOG *d);


#ifndef MAX_TITLE_STRING_SIZE
#define MAX_TITLE_STRING_SIZE 1024
#endif



/********************************************************************************/
/* x-averaged y-spectrum 							*/
/* ie: spectrum for rows, averaged over rows from x_s to x_e 			*/
/* 	a partir de la fonction do_average_along_x de XVINIMMN.C 		*/
/* 	Attention : a priori, l'image doit etre reelle...			*/
/* 	cas complexe non traite...						*/
/* 	ie le spectre s'effectue alors sur la partie reelle presente a l'ecran  */
/*										*/
/* this function can be AUTOmatized						*/
/********************************************************************************/
int do_y_spectrum_average_along_x(void)
{
  register int i, j;	/* for counting in loops 			*/
  int r_s=0, r_e=0;   	/* starting and ending rows 			*/
  O_i 	*ois = NULL;       	/* source image 				*/
  O_p 	*op = NULL;		/* destination plot 				*/
  d_s 	*ds = NULL;		/* data of destination plot 			*/
  imreg 	*imr = NULL;		/* image region where to work 			*/
  int 	ny;		/* number of points for each fft 		*/
  float 	*y = NULL; 		/* temp data  (a row)				*/
  double	*in = NULL, *out = NULL, *power_spectrum = NULL;
  fftw_plan plan;
  char  	c1[MAX_TITLE_STRING_SIZE] = {0};	/* string for title too				*/

  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT])
    {	return win_printf_OK("X-averaged Y-spectrum extract each row of \n"
			     "the source image, calculates its spectrum \n"
			     "and makes a plot of the averaged spectrums.\n"
			     "If the image is complex, it operates on the \n"
			     "real part plotted on the screen (Re,Im,A...). ");
    }
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  r_s=0;
  r_e=ois->im.nx; 		/* initialisation of r_e and r_s */

  if (!(ois->im.win_flag & Y_PER))	/* if image non periodic in Y 	*/
    sprintf(c1,"image is NOT periodic in Y.\n"
	    "Average spectrum (FFT) between rows %%d and %%d");
  else	sprintf(c1,"image is periodic in Y.\n"
		"Average spectrum (FFT) between rows %%d and %%d");

  if (win_scanf(c1,&r_s,&r_e) == WIN_CANCEL) return OFF;
  if ( (r_e<=r_s) || (r_e>ois->im.nx) || (r_s<0) )  return (win_printf("bad rows limits ! "));

  /* ci dessous, initialisation de la fft sur un nb de pts puissance de 2 : */
  ny = ois->im.ny;

  y  	       = (float *) fftw_malloc(ny*sizeof(float)); /* complete row 			*/
  in             = (double *) fftw_malloc(ny*sizeof(double));
  out            = (double *) fftw_malloc(ny*sizeof(double));
  power_spectrum = (double *) fftw_malloc((ny/2+1)*sizeof(double));
  plan           = fftw_plan_r2r_1d(ny, in, out, FFTW_R2HC, FFTW_ESTIMATE);   // the new fftw3 version!

  op = create_one_plot( (ny/2)+1 , (ny/2)+1, 0); 	/* resulting plot */
  if (op == NULL) return(win_printf("cant create spectrum plot!"));
  ds = op->dat[0];	/* the resulting data ds will be the first data set of the plot op	*/

  for (j=0; j<(ny/2+1); j++) power_spectrum[j]=0;
  for (i=r_s ; i< r_e ; i++)                 /* start the averaging loop 		*/
    {
      extract_raw_row(ois, i, y);/* extraction of the i-th row from ois to y 	*/
      for (j=0 ; j<ny ; j++)	in[j] = y[j];

      if (!(ois->im.win_flag & Y_PER))	/* if image non periodic in Y 	*/
	fft_window_real(ny, in, 0.001);

      fftw_execute(plan); // fftw3 version!
      power_spectrum[0] += out[0]*out[0];    // DC component
      for (j=0; j<(ny+1)/2; ++j)          // k < nx/2 rounded up
	power_spectrum[j] 	+= out[j]*out[j] + out[ny-j]*out[ny-j];
      if (ny % 2 ==0)                     // nx is even
	power_spectrum[ny/2] 	+= out[ny/2]*out[ny/2]; // Nyquist frequency

    }
  for (j=0 ; j<(ny/2+1) ; j++)
    {	ds->xd[j] = j;
      ds->yd[j] = power_spectrum[j]/(r_e-r_s);
    }

  fftw_free(y); fftw_free(in); fftw_free(out); fftw_free(power_spectrum);
  fftw_destroy_plan(plan);


  /* getting the title of the source image, and protection against too long names */
  if ( (ois->title != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE) )
    {	strcpy(c1, ois->title);	}
  else {	if ( (ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE) )
      strcpy(c1, ois->im.source);
    else	c1[0] = 0;	}


  set_plot_title(op,"\\stack{{Averaged spectrum for row %d to %d}{%s}}",r_s, r_e, c1);
  set_plot_x_title(op,"frequency (2\\pi/L_y)");
  op->iopt |= YLOG;				/* logarithmic units */
  set_ds_source(ds, "%s", c1);
  set_ds_treatement(ds,"Averaged spectrum for row %d to %d",r_s, r_e);
  inherit_from_im_to_ds(ds, ois);				/* ok */
  uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);	/* modifie le 09.09.98 */
  op->filename = Transfer_filename(ois->filename); 	/* ok */

  add_one_image (ois, IS_ONE_PLOT, (void *)op);
  ois->cur_op = ois->n_op - 1;
  broadcast_dialog_message(MSG_DRAW,0);
  return D_REDRAWME;
}/* end of the procedure "do_y_spectrum_average_along_x" */







/********************************************************************************/
/* y-averaged x-spectrum 							*/
/* ie: spectrum for lines, averaged over lines from y_s to y_e 			*/
/* 	a partir de la fonction do_average_along_x de XVINIMMN.C 		*/
/* 	Attention : a priori, l'image doit etre reelle...			*/
/* 	cas complexe non traite...						*/
/* 	ie le spectre s'effectue alors sur la partie reelle presente a l'ecran  */
/*										*/
/********************************************************************************/
int do_x_spectrum_average_along_y(void)
{
  register int i, j;		/* for counting in loops 		*/
  int l_s=0, l_e=0;     		/* starting and ending lines 		*/
  O_i 	*ois = NULL;       		/* source image 			*/
  O_p 	*op = NULL;			/* destination plot 			*/
  d_s 	*ds = NULL;			/* data of destination plot 		*/
  imreg *imr = NULL;			/* image region where to work 		*/
  int 	nx;			/* number of points for each fft 	*/
  float  *x = NULL;			/* temp data  (a line)			*/
  double	*in = NULL, *out = NULL, *power_spectrum = NULL;
  fftw_plan plan;
  char  c1[MAX_TITLE_STRING_SIZE] = {0};			/* string for title too			*/
  if(updating_menu_state != 0)	return D_O_K;

  if (key[KEY_LSHIFT]) return win_printf_OK("Y-averaged X-spectrum extract each line of \n"
					    "the source image, calculates its spectrum \n"
					    "and makes a plot of the averaged spectrums.\n"
					    "If the image is complex, it operates on the \n"
					    "real part plotted on the screen (Re,Im,A...).\n");
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  l_e=ois->im.ny; 	/* initialisation of l_s and l_e */
  l_s=0;
  if (!(ois->im.win_flag & X_PER))	/* if image non periodic in X 	*/
    sprintf(c1,"image is NOT periodic in X.\n"
	    "Average spectrum (FFT) between lines %%d and %%d");
  else	sprintf(c1,"image is periodic in X.\n"
		"Average spectrum (FFT) between lines %%d and %%d");
  if ( win_scanf(c1,&l_s,&l_e) == WIN_CANCEL) 		return OFF;
  if ( (l_e<=l_s) || (l_e>ois->im.ny) || (l_s<0) )	return (win_printf("bad rows limits ! "));

  /* ci dessous, initialisation de la fft sur un nb de pts puissance de 2 : */
  nx = ois->im.nx;

  x  	       = (float *) fftw_malloc(nx * sizeof(float)); /* complete row 			*/
  in             = (double *)fftw_malloc(nx * sizeof(double));
  out            = (double *)fftw_malloc(nx * sizeof(double));
  power_spectrum = (double *) fftw_malloc((nx / 2 + 1) * sizeof(double));
  plan           = fftw_plan_r2r_1d(nx, in, out, FFTW_R2HC, FFTW_ESTIMATE);   // the new fftw3 version!

  op = create_one_plot( (nx/2)+1 , (nx/2)+1, 0); 		/* resulting plot 	*/
  if (op == NULL) return (win_printf("cant create spectrum plot!"));
  ds = op->dat[0];	/* the resulting data ds will be the first data set of the plot op	*/

  for (j=0; j<(nx/2+1); j++) power_spectrum[j]=0;
  for (i=l_s ; i< l_e ; i++)                 /* start the averaging loop 		*/
    {
      extract_raw_line(ois, i, x);/* extraction of the i-th row from ois to y 	*/
      for (j=0 ; j<nx ; j++)	in[j] = x[j];

      if (!(ois->im.win_flag & X_PER))	/* if image non periodic in Y 	*/
	fft_window_real(nx, in, 0.001);

      fftw_execute(plan); // fftw3 version!
      power_spectrum[0] += out[0]*out[0];    // DC component
      for (j=0; j<(nx+1)/2; ++j)          // k < nx/2 rounded up
	power_spectrum[j] 	+= out[j]*out[j] + out[nx-j]*out[nx-j];
      if (nx % 2 ==0)                     // nx is even
	power_spectrum[nx/2] 	+= out[nx/2]*out[nx/2]; // Nyquist frequency

    }
  for (j=0 ; j<(nx/2+1) ; j++)
    {	ds->xd[j] = j;
      ds->yd[j] = power_spectrum[j]/(l_e-l_s);
    }

  fftw_free(x); fftw_free(in); fftw_free(out); fftw_free(power_spectrum);
  fftw_destroy_plan(plan);

  /* getting the title of the source image, and protection against too long names */
  if ( (ois->title != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE) )
    {	strcpy(c1, ois->title);	}
  else {	if ( (ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE) )
      strcpy(c1, ois->im.source);
    else	c1[0] = 0;	}

  set_plot_title(op,"\\stack{{Averaged spectrum for lines %d to %d}{%s}}",l_s, l_e, c1);
  set_plot_x_title(op,"frequency (2\\pi/L_x)");
  op->iopt |= YLOG;
  set_ds_source(ds, "%s", c1);			/* logarithmic units */
  set_ds_treatement(ds,"Averaged spectrum for lines %d to %d",l_s, l_e);
  inherit_from_im_to_ds(ds, ois);									/* ok */
  uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);     /* modifie le 09.09.98 */
  op->filename = Transfer_filename(ois->filename); 	/* ok */

  add_one_image (ois, IS_ONE_PLOT, (void *)op);
  ois->cur_op = ois->n_op - 1;
  broadcast_dialog_message(MSG_DRAW,0);
  return D_REDRAWME;
}/* end of the procedure "do_y_spectrum_average_along_x" */









/********************************************************************************/
/* image of y-spectrum 								*/
/* ie: spectrum for every row, placed together so as to make a new image	*/
/* 	NB : this looks like a 1D fft of the image in the Y direction,		*/
/* 	but there is only the modulus of the fft used here (spectrum...)	*/
/* inspiration in "do_x_averaged_y_spectrum" 					*/
/* 	Attention : the source image should be real...				*/
/* 	if it is complex, only the real projection on the screen 		*/
/*	(re, im, modulus, ...) is considered...					*/
/********************************************************************************/
int do_im_y_spectrum(void)
{
  int i, j;	/* for counting in loops	 		*/
  int 	operation;	/* selection lin/log by menu		*/
  O_i 	*ois = NULL, *oid = NULL;     /* source and destination image			*/
  union pix *pd = NULL;		/* data of the images				*/
  imreg 	*imr = NULL;		/* image region where to work 			*/
  int 	nx, ny;		/* number of points for each fft 		*/
  float 	*y = NULL; 		/* temp data  (a row)				*/
  double	*in = NULL, *out = NULL;
  fftw_plan plan;
  char  	c1[MAX_TITLE_STRING_SIZE] = {0};	/* string for title too				*/

  if(updating_menu_state != 0)	return D_O_K;

  if (active_menu->dp == NULL)	return D_O_K;
  if (strcmp((const char *) active_menu->dp, "LIN") == 0) 	operation = LIN_SPEC;
  else if (strcmp((const char *) active_menu->dp,"LOG") == 0) 	operation = LOG_SPEC;
  else return D_O_K;

  if (key[KEY_LSHIFT]) return win_printf_OK("Construct an image of spectra\n"
					    "Each row of the new image is the spectrum of the row of the source image.\n"
					    "If the image is complex, it operates on the \n"
					    "real part plotted on the screen (Re,Im,A...).\n\n"
					    "The source image is conserved.\n\n"
					    "You can choose a LIN or LOG spectrum image.");

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  nx = ois->im.nx;
  ny = ois->im.ny;

  y  	       =(float *) fftw_malloc(ny*sizeof(float)); /* complete row 			*/
  in             = (double *) fftw_malloc(ny*sizeof(double));
  out            = (double *) fftw_malloc(ny*sizeof(double));
  plan           = fftw_plan_r2r_1d(ny, in, out, FFTW_R2HC, FFTW_ESTIMATE);   // the new fftw3 version!

  oid = create_one_image( nx, (ny/2)+1, IS_FLOAT_IMAGE);
  if (oid == NULL)	return win_printf("I cannot create the destination image!");
  pd = oid->im.pixel;
  if (pd == NULL) 	return (win_printf("I can't create spectrum image!"));

  for (i=0 ; i< ois->im.nx ; i++)
    {
      extract_raw_row(ois, i, y);/* extraction of the i-th row from ois to y 	*/
      for (j=0 ; j<ny ; j++)	in[j] = y[j];
      if (!(ois->im.win_flag & Y_PER))	/* if image non periodic in Y 	*/
	fft_window_real(ny, in, 0.001);	/* then windowing for fft	*/

      fftw_execute(plan); // fftw3 version!


      if (operation==LIN_SPEC)
	{	pd[0].fl[i] = (float)(out[0]*out[0]);    // DC component
	  for (j=0; j<(ny+1)/2; ++j)          // k < nx/2 rounded up
	    pd[j].fl[i] = (float)(out[j]*out[j] + out[ny-j]*out[ny-j]);
	  if (ny % 2 ==0)                     // nx is even
	    pd[ny/2].fl[i] 	= (float)(out[ny/2]*out[ny/2]); // Nyquist frequency
	}
      else
	{	pd[0].fl[i] = (float)(log(out[0]*out[0]));    // DC component
	  for (j=0; j<(ny+1)/2; ++j)          // k < nx/2 rounded up
	    pd[j].fl[i] 	= (float)(log(out[j]*out[j] + out[ny-j]*out[ny-j]));
	  if (ny % 2 ==0)                     // nx is even
	    pd[ny/2].fl[i] 	= (float)(log(out[ny/2]*out[ny/2])); // Nyquist frequency
	}
    }

  fftw_free(y); fftw_free(in); fftw_free(out);
  fftw_destroy_plan(plan);

  /* getting the title of the source image, and protection against too long names */
  if ( (ois->title != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE) )
    {	strcpy(c1, ois->title);	}
  else {	if ( (ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE) )
      strcpy(c1, ois->im.source);
    else	c1[0] = 0;	}

  if (operation==LIN_SPEC) { set_im_title(oid,"\\stack{{Y LIN spectrum image}{for %s}}", c1);
    set_oi_treatement(oid,"Y LIN spectrum image");
  }
  else 			 { set_im_title(oid,"\\stack{{Y LOG spectrum image}{for %s}}", c1);
    set_oi_treatement(oid,"Y LOG spectrum image");
  }
  set_im_y_title(oid, "frequency (2\\pi/L_y)");
  inherit_from_im_to_im(oid, ois);
  oid->im.win_flag = ois->im.win_flag;
  uns_oi_2_oi(oid,ois);

  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  if (operation==LOG_SPEC) oid->z_min=-abs(oid->z_max);
  return (refresh_image(imr, (imr->n_oi - 1) ));

}/* end of the procedure "do_im_y_spectrum" */





/********************************************************************************/
/* image of x-spectrum 								*/
/* ie: spectrum for every line, placed together so as to make a new image	*/
/* 	NB : this looks like a 1D fft of the image in the X direction,		*/
/* 	but there is only the modulus of the fft used here (spectrum...)	*/
/* inspiration in "do_y-averaged_x-spectrum" 					*/
/* 	Attention : the source image should be real...				*/
/* 	if it is complex, only the real projection on the screen 		*/
/*	(re, im, modulus, ...) is considered...					*/
/********************************************************************************/
int do_im_x_spectrum(void)
{
  int i, j;	/* for counting in loops	 		*/
  int 	operation;	/* choose lin or log10 output		*/
  O_i 	*ois = NULL, *oid = NULL;     /* source and destination image			*/
  union pix *pd = NULL;		/* data of the images				*/
  imreg 	*imr = NULL;		/* image region where to work 			*/
  int 	nx, ny;	/* number of points for each fft 		*/
  double	*in = NULL, *out = NULL;
  fftw_plan	plan;
  float 	*x = NULL; 	/* temp data  (a line)				*/
  char  	c1[MAX_TITLE_STRING_SIZE] = {0};	/* string for title too				*/

  if(updating_menu_state != 0)	return D_O_K;

  if (active_menu->dp == NULL)	return D_O_K;
  if (strcmp((const char *) active_menu->dp,"LIN") == 0) 	operation = LIN_SPEC;
  else if (strcmp((const char *) active_menu->dp,"LOG") == 0) 	operation = LOG_SPEC;
  else return D_O_K;

  if (key[KEY_LSHIFT])	return win_printf_OK("Construct an image of spectra\n"
					     "Each line of the new image is the spectrum of the line of the source image.\n"
					     "If the image is complex, it operates on the \n"
					     "real part plotted on the screen (Re,Im,A...).\n\n"
					     "The source image is conserved.\n\n"
					     "You can choose a LIN or LOG spectrum image.");

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  nx = ois->im.nx;
  ny = ois->im.ny;

  x  	       = (float *) fftw_malloc(nx * sizeof(float)); /* complete row 			*/
  in             = (double *) fftw_malloc(nx * sizeof(double));
  out            = (double *) fftw_malloc(nx * sizeof(double));
  plan           = fftw_plan_r2r_1d(nx, in, out, FFTW_R2HC, FFTW_ESTIMATE);   // the new fftw3 version!

  oid = create_one_image( (nx/2)+1, ny, IS_FLOAT_IMAGE);
  if (oid == NULL)	return win_printf("I cannot create the destination image!");
  pd = oid->im.pixel;
  if (pd == NULL) 	return win_printf("I can't create spectrum image!");

  for (i=0 ; i< ny ; i++)
    {
      extract_raw_line(ois, i, x);/* extraction of the i-th line from ois to x */
      for (j=0 ; j<nx ; j++)	in[j] = x[j];
      if (!(ois->im.win_flag & X_PER))	/* if image non periodic in Y 	*/
	fft_window_real(nx, in, 0.001);	/* then windowing for fft	*/

      fftw_execute(plan); // fftw3 version!

      if (operation==LIN_SPEC)
	{	pd[i].fl[0] = (float)(out[0]*out[0]);    // DC component
	  for (j=0; j<(nx+1)/2; ++j)          // k < nx/2 rounded up
	    pd[i].fl[j] = (float)(out[j]*out[j] + out[nx-j]*out[nx-j]);
	  if (nx % 2 ==0)                     // nx is even
	    pd[i].fl[nx/2] 	= (float)(out[nx/2]*out[nx/2]); // Nyquist frequency
	}
      else
	{	pd[i].fl[0] = (float)(log(out[0]*out[0]));    // DC component
	  for (j=0; j<(nx+1)/2; ++j)          // k < nx/2 rounded up
	    pd[i].fl[j] 	= (float)(log(out[j]*out[j] + out[nx-j]*out[nx-j]));
	  if (nx % 2 ==0)                     // nx is even
	    pd[i].fl[nx/2] 	= (float)(log(out[nx/2]*out[nx/2])); // Nyquist frequency
	}
    }

  fftw_free(x); fftw_free(in); fftw_free(out);
  fftw_destroy_plan(plan);

  /* getting the title of the source image, and protection against too long names */
  if ( (ois->title != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE) )
    {	strcpy(c1, ois->title);	}
  else {	if ( (ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE) )
      strcpy(c1, ois->im.source);
    else	c1[0] = 0;	}

  if (operation==LIN_SPEC) { set_im_title(oid,"\\stack{{X LIN spectrum image}{for %s}}", c1);
    set_oi_treatement(oid,"X LIN spectrum image");
  }
  else 			 { set_im_title(oid,"\\stack{{X LOG10 spectrum image}{for %s}}", c1);
    set_oi_treatement(oid,"X LOG10 spectrum image");
  }
  set_im_x_title(oid, "frequency (2\\pi/L_x)");
  inherit_from_im_to_im(oid, ois);
  oid->im.win_flag = ois->im.win_flag;
  uns_oi_2_oi(oid,ois);

  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  if (operation==LOG_SPEC) oid->z_min=-abs(oid->z_max);	// to prevent the log from imposing minus infinity as z_min
  return (refresh_image(imr, (imr->n_oi - 1) ));

}/* end of the procedure "do_im_x_spectrum" */






int do_filter_image(void)
{
  register int 	i, j;
  int		index, mode=0;
  static int	symetry=0;
  int 		nx, ny, nout, n_2, n2;
  imreg 		*imr = NULL;
  union pix 	*pd = NULL;			// data of destination image
  O_i 		*ois = NULL, *oid = NULL;
  float		*tmp=NULL;
  double		*in=NULL;		// the input and/or output data if it is real
  fftw_complex 	*inc=NULL, 		// the input and/or output data if it is complex
    *out=NULL;		// the Fourier transform of the data: it is complex.
  fftw_plan 	plan_direct=NULL, plan_inverse=NULL;
  char		c1[MAX_TITLE_STRING_SIZE] = {0};

  if(updating_menu_state != 0)	return D_O_K;

  index = active_menu->flags;
  if 	(index & COMPLEX_OUTPUT) 	{mode |= COMPLEX_OUTPUT; }
  else if (index & REAL_OUTPUT)	 	{mode |= REAL_OUTPUT; }
  else return win_printf_OK("type of output not specified");

  if (active_menu->dp == NULL)			return D_O_K;
  if (strcmp((const char *) active_menu->dp,"P_LINE") == 0) 	index = P_LINE;
  else if (strcmp((const char *) active_menu->dp,"P_COL") == 0) 	index = P_COL;
  else return D_O_K;


  if (key[KEY_LSHIFT])
    return win_printf_OK("This routine filters an image, using Fourier Transform.\n"
			 "A demodulation can be operated with Hilbert transform.\n"
			 "The fftw v3.01 library is used, allowing to filter images of any size.\n\n"
			 "a new image is created.");

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  nx = ois->im.nx;
  ny = ois->im.ny;

  //	nout : number of logical points
  // 	n_2 :  nout/2, rounded down
  //	n2 :   nout if nout is odd, nout+1 if nout is even.
  if (index==P_LINE) { nout = nx;		n_2 = (nx%2==0) ? nx/2 : (nx-1)/2; 	n2 = nx+(nx%2); }
  else		   { nout = ny;		n_2 = (ny%2==0) ? ny/2 : (ny-1)/2; 	n2 = ny+(ny%2); }

  if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {	mode |= COMPLEX_INPUT;
      if (mode & REAL_OUTPUT)
	{	mode &= ~REAL_OUTPUT;
	  mode |=  COMPLEX_OUTPUT;
	}
    }
  else
    {	mode |= REAL_INPUT;
      if (ois->im.data_type != IS_FLOAT_IMAGE)
	{	if (win_scanf("image is neither real nor complex...\n I will operate as if it was real")==WIN_CANCEL)
	    {	return D_O_K;
	    }
	}
    }
  if (current_filter!=NULL)	free_filter(current_filter);
  current_filter=construct_filter(n_2+1, (mode | SYM) ,1);
  if (current_filter==NULL)	return win_printf_OK("Filter is NULL");

  if ( (mode & REAL_INPUT) && (mode & REAL_OUTPUT) )
    {	//win_printf_OK("Real input, real output");
      in  = (double *) fftw_malloc(  n2 * sizeof(double));
      out = (fftw_complex *) malloc( (n_2 + 1) * sizeof(fftw_complex));
      plan_direct  = fftw_plan_dft_r2c_1d(nout, in,  out, FFTW_ESTIMATE);
      plan_inverse = fftw_plan_dft_c2r_1d(nout, out, in,  FFTW_ESTIMATE);
    }
  else
    if ( (mode & COMPLEX_INPUT) && (mode & COMPLEX_OUTPUT) )
      {	//win_printf_OK("Complex input, complex output");
	win_scanf("\\pt14 \\color{yellow}Filtering of complex data\n\\color{white}Select symmetry\n"
		  "%R remove negative frequencies, filter positive ones\n"
		  "%r symetrical filtering\n"
		  "%r remove positive frequencies, filter negative ones",&symetry);
	symetry--;
	if (symetrize_filter(current_filter, nout, symetry) != nout) return(win_printf("filter cannot be symmetrized!"));
	symetry++;
	inc = (fftw_complex *) fftw_malloc( nout   *sizeof(fftw_complex));
	out = (fftw_complex *) fftw_malloc( nout   *sizeof(fftw_complex));
	plan_direct  = fftw_plan_dft_1d    (nout, inc, out, FFTW_FORWARD, FFTW_ESTIMATE);
	plan_inverse = fftw_plan_dft_1d    (nout, out, inc, FFTW_BACKWARD, FFTW_ESTIMATE);
      }
    else
      if ( (mode & REAL_INPUT) && (mode & COMPLEX_OUTPUT) )
	{	win_printf_OK("Real input, Complex output");
	  in  = (double *) fftw_malloc(  nout  *sizeof(double));
	  out = (fftw_complex *) fftw_malloc(  nout  *sizeof(fftw_complex));
	  inc = (fftw_complex *) fftw_malloc(  nout  *sizeof(fftw_complex));	// to store initial data
	  plan_direct  = fftw_plan_dft_r2c_1d(nout, in,  out, FFTW_ESTIMATE);
	  plan_inverse = fftw_plan_dft_1d    (nout, out, inc, FFTW_BACKWARD, FFTW_ESTIMATE);
	}
      else return(win_printf_OK("Complex input and real output ? No sense for me, I'm quiting."));

  if (mode & COMPLEX_OUTPUT)	oid = create_one_image( nx, ny, IS_COMPLEX_IMAGE);
  else if (mode & REAL_OUTPUT)	oid = create_one_image( nx, ny, IS_FLOAT_IMAGE);
  else return win_printf("mode error !\n output image is neither real nor complex!");
  if (oid == NULL)	return win_printf_OK("I cannot create the destination image!");
  pd = oid->im.pixel;
  if (pd == NULL) 	return win_printf_OK("I can't access destination data!");

  //        if (!(mode & REAL_INPUT)) return win_printf_OK("input is not real!\n complex filtering not fully implemented yet!");

  tmp = (float *) fftw_malloc( nout*sizeof(float));


  if (index==P_LINE) // filter along the X direction
    { 	if (mode & REAL_OUTPUT)
	for (i=0; i<ny; i++)
	  {	extract_raw_line(ois, i, tmp);
	    for (j=0; j<nx ; j++)      in[j] = (double)tmp[j];
	    if (!(ois->im.win_flag & X_PER))	fft_window_real(nx, in, 0.001);

	    fftw_execute(plan_direct);
	    apply_filter_complex(current_filter, out, n_2+1);	// the fft has n/2+1 complex points
	    fftw_execute(plan_inverse);

	    if (!(ois->im.win_flag & X_PER))	fft_unwindow_real(nx, in, 0.001);
	    for (j=0; j<nx ; j++)	pd[i].fl[j] = (float)(in[j]/nout);
	  }
      else // complex output
	if (mode & REAL_INPUT)
	  for (i=0; i<ny; i++)
	    {	extract_raw_line(ois, i, tmp);
	      for (j=0; j<nx ; j++)   in[j] = (double)tmp[j];
	      if (!(ois->im.win_flag & X_PER))	fft_window_real(nx, in, 0.001);

	      fftw_execute(plan_direct);
	      apply_filter_complex(current_filter, out, n_2+1);	// the fft has n/2+1 complex points
	      fftw_execute(plan_inverse);

	      if (!(ois->im.win_flag & X_PER))	fft_unwindow_complex(nx, inc, 0.001);
	      for (j=0; j<nx ; j++)
		{	pd[i].fl[2*j]   = (float)(inc[j][0]/nout);
		  pd[i].fl[2*j+1] = (float)(inc[j][1]/nout);
		}
	    }
	else if (mode & COMPLEX_INPUT)
	  for (i=0; i<ny; i++)
	    {	for (j=0; j<nx ; j++)
		{	inc[j][0] = (double)ois->im.pixel[i].fl[2*j];
		  inc[j][1] = (double)ois->im.pixel[i].fl[2*j+1];
		}
	      if (!(ois->im.win_flag & X_PER))	fft_window_complex(nx, inc, 0.001);

	      fftw_execute(plan_direct);
	      apply_filter_complex(current_filter, out, nout);	// the fft has nout complex points
	      fftw_execute(plan_inverse);

	      if (!(ois->im.win_flag & X_PER))	fft_unwindow_complex(nx, inc, 0.001);
	      for (j=0; j<nx ; j++)
		{	pd[i].fl[2*j]   = (float)(inc[j][0]/nout);
		  pd[i].fl[2*j+1] = (float)(inc[j][1]/nout);
		}
	    }
    }
  else // filter along the Y direction:
    { 	if (mode & REAL_OUTPUT)
	for (j=0; j<nx; j++)
	  {	extract_raw_row (ois, j, tmp);
	    for (i=0; i<ny ; i++)      in[i] = (double)tmp[i];
	    if (!(ois->im.win_flag & Y_PER))	fft_window_real(ny, in, 0.001);

	    fftw_execute(plan_direct);
	    apply_filter_complex(current_filter, out, n_2+1);	// the fft has n/2+1 complex points
	    fftw_execute(plan_inverse);

	    if (!(ois->im.win_flag & Y_PER))	fft_unwindow_real(ny, in, 0.001);
	    for (i=0; i<ny ; i++)	pd[i].fl[j] = (float)(in[i]/nout);
	  }
      else // complex output
	if (mode & REAL_INPUT)
	  for (j=0; j<nx; j++)
	    {	extract_raw_row (ois, j, tmp);
	      for (i=0; i<ny ; i++)      in[i] = (double)tmp[i];
	      if (!(ois->im.win_flag & Y_PER))	fft_window_real(ny, in, 0.001);

	      fftw_execute(plan_direct);
	      apply_filter_complex(current_filter, out, n_2+1);	// the fft has n/2+1 complex points
	      fftw_execute(plan_inverse);

	      if (!(ois->im.win_flag & Y_PER))	fft_unwindow_complex(ny, inc, 0.001);
	      for (i=0; i<ny ; i++)
		{	pd[i].fl[2*j]   = (float)(inc[i][0]/nout);
		  pd[i].fl[2*j+1] = (float)(inc[i][1]/nout);
		}
	    }
	else if (mode & COMPLEX_INPUT)
	  for (j=0; j<nx; j++)
	    {	for (i=0; i<ny ; i++)
		{	inc[i][0] = (double)ois->im.pixel[i].fl[2*j];
		  inc[i][1] = (double)ois->im.pixel[i].fl[2*j+1];
		}
	      if (!(ois->im.win_flag & Y_PER))	fft_window_complex(ny, inc, 0.001);

	      fftw_execute(plan_direct);
	      apply_filter_complex(current_filter, out, n_2+1);	// the fft has n/2+1 complex points
	      fftw_execute(plan_inverse);

	      if (!(ois->im.win_flag & Y_PER))	fft_unwindow_complex(ny, inc, 0.001);
	      for (i=0; i<ny ; i++)
		{	pd[i].fl[2*j]   = (float)(inc[i][0]/nout);
		  pd[i].fl[2*j+1] = (float)(inc[i][1]/nout);
		}
	    }
    }

  fftw_destroy_plan(plan_direct);
  fftw_destroy_plan(plan_inverse);

  // if Hilbert transform, then half the amplitude is missing
  // because negative frequencies have been removed
  if ( (mode & REAL_INPUT) && (mode & COMPLEX_OUTPUT) )
    {	for (j=0; j<nx; j++)
	{ for (i=0; i<ny ; i++)
	    {	pd[i].fl[2*j]   *= 2;
	      pd[i].fl[2*j+1] *= 2;
	    }
	}
      set_oi_treatement(oid,"R->C ");
    }

  fftw_free(tmp); fftw_free(out);
  if (mode & REAL_INPUT) 				   	 fftw_free(in);
  if ( (mode & COMPLEX_INPUT) || (mode & COMPLEX_OUTPUT) ) fftw_free(inc);

  inherit_from_im_to_im(oid, ois);
  uns_oi_2_oi(oid,ois);

  /* getting the title of the source image, and protection against too long names */
  if ( (ois->title != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE) )
    {	strcpy(c1, ois->title);	}
  else {	if ( (ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE) )
      strcpy(c1, ois->im.source);
    else	c1[0] = 0;	}

  set_im_title(oid, "%s filtred in %s", c1, (index == P_LINE)? "X" : "Y");
  set_oi_treatement(oid,"%s filtred in %s", c1, (index == P_LINE)? "X" : "Y");

  /*	if (index == P_LINE)
	{	no = ois->im.nx/onx;
	for (i=0; i < oid->n_xu; i++) oid->xu[i]->dx *= no;
	}
	else
	{	no = ois->im.ny/ony;
	for (i=0; i < oid->n_yu; i++) oid->yu[i]->dx *= no;
	}
  */
  set_oi_x_unit_set(oid, oid->c_xu);
  set_oi_y_unit_set(oid, oid->c_yu);
  oid->im.win_flag = ois->im.win_flag;
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  imr->cur_oi = imr->n_oi - 1;

  /* 3 lines:
     oid->need_to_refresh = ALL_NEED_REFRESH;
     broadcast_dialog_message(MSG_DRAW,0);
     return D_REDRAWME;
  */
  /* or just 1 line ? : */
  return (refresh_image(imr, (imr->n_oi - 1) ));


} /* end of function "do_filter_image" */












/********************************************************************************/
/* fft2d of an image								*/
/********************************************************************************/
int do_fft_2d(void)
{
  int i, j;	/* for counting in loops	 		*/
  int 	operation;	/* choose lin or log10 output			*/
  O_i 	*ois = NULL, *oid=NULL;/* source and destination image			*/
  union pix *ps = NULL, *pd = NULL;	/* data of the images				*/
  imreg 	*imr = NULL;		/* image region where to work 			*/
  int 	nx, ny;		/* number of points for each fft 		*/
  fftw_complex	*in = NULL, *out = NULL;
  fftw_plan	plan;
  char  	c1[MAX_TITLE_STRING_SIZE] = {0};	/* string for title too				*/
  double	normalisation=1;
#ifndef XV_UNIX
  static unsigned long t_c, to = 0;
#endif

  operation = active_menu->flags & 0xFFFFFF00;
  if ( (operation!=FFT2D_FORWARD) && (operation!=FFT2D_BACKWARD) ) return D_O_K;

  if (updating_menu_state != 0)
    {
      if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
	  active_menu->flags |=  D_DISABLED;
	  return D_O_K;
	}
      else 		active_menu->flags &= ~D_DISABLED;
      if (operation==FFT2D_BACKWARD)
	{	if (ois->im.data_type!=IS_COMPLEX_IMAGE)
	    {
	      active_menu->flags |=  D_DISABLED;
	    }
	  else 	active_menu->flags &= ~D_DISABLED;
	}
      return D_O_K;
    }

  if (key[KEY_LSHIFT])	return win_printf_OK("Construct an image of the 2-d\n"
					     "discrete Fourier transform (DFT) using FFTw3\n\n"
					     "Images of any size can be operated\n\n"
					     "a normalization factor \\frac{1}{\\sqrt{ n_x  n_y}} is applied");

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return OFF;
  ps = ois->im.pixel;
  if (ps == NULL) 	return win_printf("I cannot access destination data!");

  nx = ois->im.nx;
  ny = ois->im.ny;

  in             =(fftw_complex *) fftw_malloc(nx*ny*sizeof(fftw_complex));
  out            = (fftw_complex *) fftw_malloc(nx*ny*sizeof(fftw_complex));

  if (operation==FFT2D_FORWARD)
    plan           = fftw_plan_dft_2d(nx, ny, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  else
    plan           = fftw_plan_dft_2d(nx, ny, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);

  oid = create_one_image(nx, ny, IS_COMPLEX_IMAGE);
  if (oid == NULL)	return win_printf("I cannot create the destination image!");
  pd = oid->im.pixel;
  if (pd == NULL) 	return win_printf("I cannot access destination data!");

  if (ois->im.data_type == IS_CHAR_IMAGE)
    {	for (i=0; i<nx; i++)
	for (j=0; j<ny; j++)
	  {	in[j*nx+i][0]=(double)ps[j].ch[i];
	    in[j*nx+i][1]=0;
	  }
    }
  else if (ois->im.data_type == IS_INT_IMAGE)
    {	for (i=0; i<nx; i++)
	for (j=0; j<ny; j++)
	  {	in[j*nx+i][0]=(double)ps[j].in[i];
	    in[j*nx+i][1]=0;
	  }
    }
  else if (ois->im.data_type == IS_UINT_IMAGE)
    {	for (i=0; i<nx; i++)
	for (j=0; j<ny; j++)
	  {	in[j*nx+i][0]=(double)ps[j].ui[i];
	    in[j*nx+i][1]=0;
	  }
    }
  else if (ois->im.data_type == IS_LINT_IMAGE)
    {	for (i=0; i<nx; i++)
	for (j=0; j<ny; j++)
	  {	in[j*nx+i][0]=(double)ps[j].li[i];
	    in[j*nx+i][1]=0;
	  }
    }
  else if (ois->im.data_type == IS_FLOAT_IMAGE)
    {	for (i=0; i<nx; i++)
	for (j=0; j<ny; j++)
	  {	in[j*nx+i][0]=(double)ps[j].fl[i];
	    in[j*nx+i][1]=0;
	  }
    }
  else if (ois->im.data_type == IS_COMPLEX_IMAGE)
    {	for (i=0; i<nx; i++)
	for (j=0; j<ny; j++)
	  {	in[j*nx+i][0]=ps[j].fl[2*i];
	    in[j*nx+i][1]=ps[j].fl[2*i+1];
	  }
    }
  else return win_printf("Image is of unknown type !!!");
#ifndef XV_MAC
#ifndef XV_UNIX
  t_c = my_uclock();
#endif
#endif
  if (operation==FFT2D_FORWARD)
    { if (!(ois->im.win_flag & X_PER))	fft_window_complex_2d_x(nx, ny, in, 0.001);
      if (!(ois->im.win_flag & Y_PER))	fft_window_complex_2d_y(nx, ny, in, 0.001);
    }

  fftw_execute(plan);

  if (operation==FFT2D_BACKWARD)
    { if (!(ois->im.win_flag & X_PER))	fft_unwindow_complex_2d_x(nx, ny, in, 0.001);
      if (!(ois->im.win_flag & Y_PER))	fft_unwindow_complex_2d_y(nx, ny, in, 0.001);
    }

  normalisation=1/sqrt(nx*ny);	// this is an arbitrary choice, resulting of the fftw3 convention !!!
  for (i=0; i<nx; i++)
    for (j=0; j<ny; j++)
      {	pd[j].fl[2*i]   = out[j*nx+i][0] * normalisation;
	pd[j].fl[2*i+1] = out[j*nx+i][1] * normalisation;
      }

#ifndef XV_UNIX
#ifndef XV_MAC
  t_c = my_uclock() - t_c;
  if (to == 0) to = MY_UCLOCKS_PER_SEC;
  win_printf("dt = %g ms",(double)(t_c*1000)/to);
#endif
#endif
  fftw_free(in); fftw_free(out);
  fftw_destroy_plan(plan);

  /* getting the title of the source image, and protection against too long names */
  if ( (ois->title != NULL) && (strlen(ois->title) < MAX_TITLE_STRING_SIZE) )
    {	strcpy(c1, ois->title);	}
  else {	if ( (ois->im.source != NULL) && (strlen(ois->im.source) < MAX_TITLE_STRING_SIZE) )
      strcpy(c1, ois->im.source);
    else	c1[0] = 0;	}

  set_im_title(oid,"FFT2d %s", c1);
  set_oi_treatement(oid,"FFT-2d");

  if (operation==FFT2D_FORWARD)
    {	set_im_x_title(oid, "frequency (2\\pi/L_x)");
      set_im_y_title(oid, "frequency (2\\pi/L_y)");
    }
  inherit_from_im_to_im(oid, ois);
  oid->im.win_flag = ois->im.win_flag;
  //	uns_oi_2_oi(oid,ois);

  if (operation==FFT2D_FORWARD) 	oid->im.mode = LOG_AMP;
  else				oid->im.mode = RE;
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  return (refresh_image(imr, (imr->n_oi - 1) ));

}/* end of the procedure "do_fft_2d" */





int do_build_and_apply_2d_filter(void)
{	register int i, j;	/* for counting in loops	 		*/
  O_i 	*ois = NULL;
  O_i	*oid=NULL	;/* source and destination image		*/
  union pix *ps = NULL, *pd = NULL;	/* data of the images				*/
  imreg 	*imr = NULL;		/* image region where to work 			*/
  int 	nx, ny;		/* number of points for each fft 		*/
  static int fx_min=0, fx_max=0, fy_min=0, fy_max=0;
  static int width_x=0, width_y=0;
  struct fft_filter *tmp_filter = NULL;


  if (updating_menu_state != 0)
    {
      if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
	  active_menu->flags |=  D_DISABLED;
	  return D_O_K;
	}
      else 		active_menu->flags &= ~D_DISABLED;
      if (ois->im.data_type!=IS_COMPLEX_IMAGE)
	active_menu->flags |=  D_DISABLED;
      else	 	active_menu->flags &= ~D_DISABLED;
      return D_O_K;
    }

  if (key[KEY_LSHIFT])	return win_printf_OK("Construct an image of a 2-d filter\n"
					     "to be applied on a 2d-FFT\n\n"
					     "To apply it, use image multiplication");

  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2) return D_O_K;
  ps = ois->im.pixel;
  if (ps == NULL) 	return win_printf("I cannot access source data!");

  /*
    prev_general_im_action = general_im_action;
    general_im_action = find_selected_imr_area;
  */
  nx = ois->im.nx;
  ny = ois->im.ny;

  if (fx_max==0) fx_max=nx;
  if (fy_max==0) fy_max=ny;

  i=win_scanf("\\color{yellow}Band Pass Filter\\color{white}\nselect frequencies\n"
	      "%4d cutoff_X^{min}   \n"
	      "%4d cutoff_X^{max}   %4d width\n"
	      "%4d cutoff_Y^{min}   \n"
	      "%4d cutoff_Y^{max}   %4d width\n",
	      &fx_min, &fx_max, &width_x, &fy_min, &fy_max, &width_y);
  if (i==WIN_CANCEL) return D_O_K;
  if ( (fx_max<fx_min) || (fx_min<0) || (fx_max>nx) ) return win_printf_OK("error with frequencies in x");
  if ( (fy_max<fy_min) || (fy_min<0) || (fy_max>ny) ) return win_printf_OK("error with frequencies in y");

  oid = create_one_image(nx, ny, IS_COMPLEX_IMAGE);
  if (oid == NULL)	return win_printf("I cannot create the destination image!");
  pd = oid->im.pixel;
  if (pd == NULL) 	return win_printf("I cannot access destination data!");

  tmp_filter = build_bandpass_filter  (nx, fx_min, width_x, fx_max, width_x);
  for (i=0; i<nx; i++)
    for (j=0; j<ny; j++)
      {	pd[j].fl[2*i]   = ps[j].fl[2*i]  *tmp_filter->f[i];
	pd[j].fl[2*i+1] = ps[j].fl[2*i+1]*tmp_filter->f[i];
      }
  free_filter(tmp_filter);
  tmp_filter = build_bandpass_filter  (ny, fy_min, width_y, fy_max, width_y);
  for (i=0; i<nx; i++)
    for (j=0; j<ny; j++)
      {	pd[j].fl[2*i]   *= tmp_filter->f[j];
	pd[j].fl[2*i+1] *= tmp_filter->f[j];
      }
  free_filter(tmp_filter);

  /*	for (i=fx_min; i<fx_max; i++)
	for (j=fy_min; j<fy_max; j++)
	{	pd[j].fl[2*i]   = ps[j].fl[2*i];
	pd[j].fl[2*i+1] = ps[j].fl[2*i+1];
	}*/

  set_im_title(oid,"filtered FFT2d");
  set_oi_treatement(oid,"filtered FT");
  inherit_from_im_to_im(oid, ois);
  uns_oi_2_oi(oid,ois);
  oid->im.mode = LOG_AMP;
  add_image(imr, oid->type, (void*)oid);
  find_zmin_zmax(oid);
  return(refresh_image(imr, (imr->n_oi - 1) ));
}


int find_selected_imr_area(imreg *imr, int x_0, int y_0, DIALOG *d)
{
  int   dx = 0, dy = 0, ldx, ldy, color; //, sc;
  // float x, y, xa, ya;
  //	struct box *b;
  //	O_i *oi;


  if (imr->o_i == NULL)		return 1;
  //	oi = imr->one_i;
  //sc = imr->screen_scale;
  //	b = imr->stack;
  //y = ya = y_imr_2_imdata(imr, y_0);
  //x = xa = x_imr_2_imdata(imr, x_0);
  ldy = 0;
  ldx = 0;
  color = makecol(255, 64, 64);
  while (mouse_b & 0x03)
    {
      dx = mouse_x - x_0;
      dy = mouse_y - y_0;
      if  (ldy != dy || dx != ldx)
	{
	  blit_plot_reg(d);
	  acquire_bitmap(screen);
	  rect(screen,x_0,y_0,x_0 + dx,y_0 + dy,color);
	  release_bitmap(screen);
	  draw_bubble(screen, B_CENTER, x_0, y_0-40, "resizing plot (ESC to abort)");
	  draw_bubble(screen, B_LEFT, x_0, y_0-20, "x0 = %g y0 %g",
		      x_imr_2_imdata(imr,x_0),y_imr_2_imdata(imr,y_0));
	  draw_bubble(screen, B_RIGHT, x_0 + dx, y_0 + dy + 20, "x1 = %g y2 %g",
		      x_imr_2_imdata(imr,x_0 + dx),	y_imr_2_imdata(imr,y_0 + dy));
	  //y = y_imr_2_pltdata(imr, y_0 + dy);
	  //x = x_imr_2_pltdata(imr, x_0 + dx);
	}
      if (key[KEY_ESC])
	{
	  clear_keybuf();
	  return D_REDRAWME;
	}
      ldx = dx;
      ldy = dy;
    }

  // here : do something with values, or just return them.
  // values are xa, x, ya, y

  //		oi->need_to_refresh = 1;
  //		return D_REDRAWME;

  return D_O_K;
}

/*
  int drag_rectangle(DIALOG *d)
  {
  int ox, oy, x, y;
  BITMAP *mask;
  BITMAP *bmp_old;

  mask = create_bitmap(d->w, d->h);
  bmp_old = create_bitmap(d->w, d->h);
  blit(d->dp, bmp_old, 0, 0, 0, 0, d->w, d->h);

  // workspace-coordinates
  ox = x = mouse_x - d->x;
  oy = y = mouse_y - d->y;

  // pointer in workspace?
  if(ox < 0 || ox > d->w-1 || oy <0 || oy > d->h-1)
  return D_O_K;

  //set_mouse_sprite(resize_pointer_up);
  xor_mode(TRUE);

  scare_mouse(); // virer la souris
  draw_region(ox, oy, x, y, d->dp, d->w, d->h); // au debut = 1 pt
  object_message(d, MSG_DRAW, 0); // tracer par dessus la bitmap de l'objet
  unscare_mouse(); // souris revient

  while (mouse_b) { // tant que c'est clike

  if (mouse_x-d->x != x || mouse_y-d->y != y) { // si souris a bouge

  scare_mouse(); // cacher souris

  draw_region(ox, oy, x, y, d->dp, d->w, d->h); // xor mode => cela efface
  object_message(d, MSG_DRAW, 0); // sans doute inutile ?

  x = mouse_x-d->x; //nouvelle coordnoon�es souris
  y = mouse_y-d->y;

  draw_region(ox, oy, x, y, d->dp, d->w, d->h);
  object_message(d, MSG_DRAW, 0);
  unscare_mouse();
  }
  broadcast_dialog_message(MSG_IDLE, 0); //
  }

  scare_mouse();
  draw_region(ox, oy, x, y, d->dp, d->w, d->h);
  object_message(d, MSG_DRAW, 0);
  unscare_mouse();

  xor_mode(FALSE);

  if (mouse_x-d->x != ox || mouse_y-d->y != oy) {
  clear_to_color(mask, Darkgray);
  draw_region(ox, oy, x, y, mask, mask->w, mask->h);
  floodfill(mask, (ox+x)/2, (oy+y)/2, Lightmagenta); // c'est la couleur transparente !
  set_trans_blender(0, 0, 0, 196); // la 4eme valeur code la durete du masque de 0 a 255
  draw_trans_sprite(d->dp, mask, 0, 0);
  scare_mouse();
  object_message(d, MSG_DRAW, 0);

  unscare_mouse();
  if(alert("ZOOM?", NULL, NULL, "&Yes", "&No", 'y', 'n')!=1){
  scare_mouse();
  blit(bmp_old, d->dp, 0, 0, 0, 0, d->w, d->h);
  object_message(d, MSG_DRAW, 0);
  unscare_mouse();
  }
  }

  destroy_bitmap(mask);
  destroy_bitmap(bmp_old);
  set_mouse_sprite(default_pointer);

  return D_O_K;
  }


  void draw_region(int ox, int oy, int x, int y , BITMAP *bmp,  int h, int w)
  {
  int xx, yy, oxx, oyy;
  fixed angle_a, angle_b;
  fixed radius_a, radius_b;
  fixed tmp1, tmp2;

  tmp1 = fixatan2(itofix(h-1-oy), itofix(ox));
  tmp2 = fixatan2(itofix(h-1-y), itofix(x));

  angle_a = (fixtof(tmp1)<fixtof(tmp2)) ? tmp1 : tmp2;
  angle_b = (fixtof(tmp1)<fixtof(tmp2)) ? tmp2 : tmp1;

  radius_a =fixhypot(itofix(ox), itofix(h-1-oy));
  radius_b =fixhypot(itofix(x), itofix(h-1-y));

  arc(bmp, 0, h-1, angle_a, angle_b, fixtoi(radius_a), XOR_COLOR);
  arc(bmp, 0, h-1, angle_a, angle_b, fixtoi(radius_b), XOR_COLOR);

  xx = fixtoi(fixmul(radius_b, fixcos(tmp1)));
  yy = h-1-fixtoi(fixmul(radius_b, fixsin(tmp1)));

  oxx = fixtoi(fixmul(radius_a, fixcos(tmp1)));
  oyy = h-1-fixtoi(fixmul(radius_a, fixsin(tmp1)));

  line(bmp, oxx, oyy, xx, yy, XOR_COLOR);

  xx = fixtoi(fixmul(radius_a, fixcos(tmp2)));
  yy = h-1-fixtoi(fixmul(radius_a, fixsin(tmp2)));

  oxx = fixtoi(fixmul(radius_b, fixcos(tmp2)));
  oyy = h-1-fixtoi(fixmul(radius_b, fixsin(tmp2)));

  line(bmp, oxx, oyy, xx, yy, XOR_COLOR);

  return;
  }
*/
