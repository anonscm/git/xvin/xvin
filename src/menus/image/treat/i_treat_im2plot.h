/****************************************************************/
/* i_treat_im2plot.h						*/
/* header for i_treat_im2plot.c					*/
/*								*/
/* operations on images realizing something that results	*/
/* in a (new) plot or dataset					*/
/****************************************************************/

int do_average_along_x(void);
int do_average_along_y(void);

int do_movie_average_along_x(void);
int do_movie_average_along_y(void);

int do_image_histogram(void);
int do_XY_im_to_plot(void);
int 	do_im_multi_lines_profile_to_plot(void);
int 	do_im_multi_rows_profile_to_plot(void);
int do_followMax_along_y(void);
int do_followZero_along_y(void);
int do_find_max_frame_value_in_movie(void);
int do_move_frame_from_ds_in_movie(void);
