/****************************************************************/
/* i_treat_im2plot.c						*/
/*								*/
/* operations on images realizing something that results	*/
/* in a (new) plot or dataset					*/
/****************************************************************/

#include "xvin.h"
#include "allegro.h"

#include "i_treat_im2plot.h"
#include "../../plot/treat/p_treat_math.h"

# include <stdlib.h>
# include <string.h>
# include <float.h>


int do_average_along_x(void)
{
	register int i, j;
	static int r_s, r_e;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds = NULL;	
	imreg *imr = NULL;
	
	if(updating_menu_state != 0)	return D_O_K;	
	
	imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)	return 0;
	ois = imr->one_i;	
	r_s = 0; r_e = ois->im.nx;
	i = win_scanf("Average Between row%d and %d",&r_s,&r_e);
	if (i == WIN_CANCEL) return D_O_K;
	op = create_one_plot(ois->im.ny,ois->im.ny,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0];
	op->x_lo = ois->z_min;	op->x_hi = ois->z_max;	
	op->iopt2 |= X_LIM;		op->type = IM_SAME_Y_AXIS;
	set_plot_title(op,"Averaged row profile (%d to %d)",r_s, r_e);
	set_ds_treatement(ds,"Averaged row profile (%d to %d)",r_s, r_e);
	inherit_from_im_to_ds(ds, ois);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_X_UNIT_SET);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	op->filename = strdup(ois->filename);		
	for (i=0 ; i< ds->ny ; i++)	ds->xd[i] = 0;
	for (i=r_s ; i< r_e ; i++)
	{
		extract_raw_row(ois, i, ds->yd);
		for (j=0 ; j< ds->ny ; j++)	ds->xd[j] += ds->yd[j];	
	}
	for (i=0, j = r_e - r_s ; i< ds->ny && j != 0 ; i++)
	{
		ds->yd[i] = i;
		ds->xd[i] /= j;		
	}
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);	
	return D_REDRAWME;		
}

int do_average_along_y(void)
{
	register int i, j;
	static int l_s = 0, l_e = 128, add_var = 0;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds = NULL;	
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;	
	
	imr = find_imr_in_current_dialog(NULL);
	if (imr == NULL)	return 0;
	ois = imr->one_i;	
	l_s = 0; l_e = ois->im.ny;
	i = win_scanf("Average Between lines%d and %d"
		      "%b Add root mean square",&l_s,&l_e,&add_var);
	if (i == WIN_CANCEL)	return D_O_K;
	op = create_one_plot(ois->im.nx,ois->im.nx,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0];
/*	op->y_lo = ois->z_min;	op->y_hi = ois->z_max;	*/
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	op->iopt2 |= Y_LIM;	op->type = IM_SAME_X_AXIS;
	inherit_from_im_to_ds(ds, ois);	
	op->filename = Transfer_filename(ois->filename);	
	set_plot_title(op,"Averaged profile from line %d to %d",l_s, l_e);
	set_ds_treatement(ds,"Averaged profile from line %d to %d",l_s, l_e);
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	if (add_var) alloc_data_set_y_error(ds);
	for (i=0 ; i< ds->nx ; i++)	ds->yd[i] = 0;
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, ds->xd);
		for (j=0 ; j< ds->nx ; j++)	ds->yd[j] += ds->xd[j];	
	}
	for (i=0, j = l_e - l_s ; i< ds->nx && j !=0 ; i++)
	{
		ds->xd[i] = i;
		ds->yd[i] /= j;		
	}
	if (add_var)
	  {
	    float tmp;
	    for (i=l_s ; i< l_e && ds->ye != NULL; i++)
	      {
		extract_raw_line(ois, i, ds->xd);
		for (j=0 ; j< ds->nx ; j++)
		  {
		    tmp = ds->xd[j] - ds->yd[j];
		    ds->ye[j] += tmp *tmp;
		  }
	      }
	    for (i=0, j = l_e - l_s ; i< ds->nx && j !=0  && ds->ye != NULL; i++)
	      {
		ds->xd[i] = i;
		ds->ye[i] /= (j > 1) ? j-1 :1;
		ds->ye[i] = sqrt(ds->ye[i]);
		
	      }
	    
	  }
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	broadcast_dialog_message(MSG_DRAW,0);	
	return D_REDRAWME;		
}


int do_movie_average_along_x(void)
{
	register int i, j, k;
	static int l_s = -1, l_e = -1;
	O_i *ois = NULL, *oid = NULL;
	float *tmp = NULL;
	int nf;
	union pix *pd = NULL;
	imreg *imr = NULL;


	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;	
	}		
	if(updating_menu_state != 0)	
	{
		if (ois->im.n_f == 1 || ois->im.n_f == 0)	
			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
		return D_O_K;	
	}	
	nf = abs(ois->im.n_f);		
	if (nf <= 0)	
	{
		win_printf("This is not a movie");
		return D_O_K;
	}
	i = ois->im.c_f;

	if (l_s == -1) l_s = 0; 
	if (l_e == -1) l_e = ois->im.nx;
	i = win_scanf("Average Between row %d and %d",&l_s,&l_e);
	if (i == WIN_CANCEL)	return D_O_K;

	oid = create_and_attach_oi_to_imr(imr, ois->im.ny ,nf, IS_FLOAT_IMAGE);	
	if (oid == NULL) return (win_printf_OK("cant create image!"));

	tmp = (float*)calloc(ois->im.ny,sizeof(float));
	if (tmp == NULL) return (win_printf_OK("cant create tmp array!"));
	pd  = oid->im.pixel;		

	set_im_title(oid,"Average row profile. (%d, %d)", l_s,l_e);
	set_oi_treatement(oid,"Average row profile. (%d, %d)", l_s,l_e);

	inherit_from_im_to_im(oid,ois);

	uns_oi_2_oi_by_type(ois, IS_Y_UNIT_SET, oid, IS_X_UNIT_SET);
	uns_oi_2_oi_by_type(ois, IS_T_UNIT_SET, oid, IS_Y_UNIT_SET);
	for (k = 0; k < nf; k++)
	  {
	    switch_frame(ois,k);
	    display_title_message("im %d",k);
	    for (i=0 ; i< ois->im.ny ; i++)	tmp[i] = 0;
	    for (i = (l_s>= 0) ? l_s : 0; i< l_e && i < ois->im.nx; i++)
	      {
		extract_raw_row(ois, i, tmp);
		for (j=0 ; j< ois->im.ny ; j++)	pd[k].fl[j] += tmp[j];	
	      }
	    for (i=0, j = l_e - l_s ; i < ois->im.ny && j !=0 ; i++)
	      {
		pd[k].fl[i] /= j;		
	      }
	  }
	free(tmp);
	find_zmin_zmax(oid);
	oid->need_to_refresh = ALL_NEED_REFRESH;
	set_oi_horizontal_extend(oid, 1);
	set_oi_vertical_extend(oid, 1);
	select_image_of_imreg(imr, imr->n_oi-1);
	broadcast_dialog_message(MSG_DRAW,0);	
	return D_REDRAWME;		
}


int do_movie_average_along_y(void)
{
	register int i, j, k;
	static int l_s = -1, l_e = -1;
	O_i *ois = NULL, *oid = NULL;
	int nf;
	union pix *pd = NULL;
	float *tmp = NULL;
	imreg *imr = NULL;


	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;	
	}		
	if(updating_menu_state != 0)	
	{
		if (ois->im.n_f == 1 || ois->im.n_f == 0)	
			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
		return D_O_K;	
	}	
	nf = abs(ois->im.n_f);		
	if (nf <= 0)	
	{
		win_printf("This is not a movie");
		return D_O_K;
	}
	i = ois->im.c_f;

	if (l_s == -1) l_s = 0; 
	if (l_e == -1) l_e = ois->im.ny;
	i = win_scanf("Average Between lines %8d and %8d\n",&l_s,&l_e);
	if (i == WIN_CANCEL)	return D_O_K;

	oid = create_and_attach_oi_to_imr(imr, ois->im.nx ,nf, IS_FLOAT_IMAGE);	
	if (oid == NULL) return (win_printf_OK("cant create image!"));

	tmp = (float*)calloc(ois->im.nx,sizeof(float));
	if (tmp == NULL) return (win_printf_OK("cant create tmp array!"));
	pd  = oid->im.pixel;		

	set_im_title(oid,"Average lines profile. (%d, %d)", l_s,l_e);
	set_oi_treatement(oid,"Average line profile. (%d, %d)", l_s,l_e);

	inherit_from_im_to_im(oid,ois);

	uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid, IS_X_UNIT_SET);
	uns_oi_2_oi_by_type(ois, IS_T_UNIT_SET, oid, IS_Y_UNIT_SET);
	for (k = 0; k < nf; k++)
	  {
	    switch_frame(ois,k);
	    for (i=0 ; i< ois->im.nx ; i++)	tmp[i] = 0;
	    for (i=l_s ; i< l_e ; i++)
	      {
		extract_raw_line(ois, i, tmp);
		for (j=0 ; j< ois->im.nx ; j++)	pd[k].fl[j] += tmp[j];	
	      }
	    for (i=0, j = l_e - l_s ; i < ois->im.nx && j !=0 ; i++)
	      {
		pd[k].fl[i] /= j;		
	      }
	  }
	free(tmp);
	find_zmin_zmax(oid);
	oid->need_to_refresh = ALL_NEED_REFRESH;
        set_oi_horizontal_extend(oid, 1);
	set_oi_vertical_extend(oid, 1);
	select_image_of_imreg(imr, imr->n_oi-1);
	broadcast_dialog_message(MSG_DRAW,0);	
	return D_REDRAWME;		
}



int do_movie_average_along_y_and_in_t(void)
{
	register int i, j, k;
	static int l_s = -1, l_e = -1, starting = 0, avgt = 1, period = 1;
	O_i *ois = NULL, *oid = NULL;
	int nf, ny;
	union pix *pd = NULL;
	float *tmp = NULL;
	imreg *imr = NULL;


	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;	
	}		
	if(updating_menu_state != 0)	
	{
		if (ois->im.n_f == 1 || ois->im.n_f == 0)	
			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
		return D_O_K;	
	}	
	nf = abs(ois->im.n_f);		
	if (nf <= 0)	
	{
		win_printf("This is not a movie");
		return D_O_K;
	}
	i = ois->im.c_f;

	if (l_s == -1) l_s = 0; 
	if (l_e == -1) l_e = ois->im.ny;
	i = win_scanf("Average Between lines %8d and %8d\n"
		      "Average in time starting at frame %8d\n"
		      "over %8d frames repeat averaging over a period %8d\n"
		      ,&l_s,&l_e,&starting,&avgt,&period);
	if (i == WIN_CANCEL)	return D_O_K;
	for (ny = 0, k = starting; k < nf; ny++, k += period);

	oid = create_and_attach_oi_to_imr(imr, ois->im.nx ,ny, IS_FLOAT_IMAGE);	
	if (oid == NULL) return (win_printf_OK("cant create image!"));

	tmp = (float*)calloc(ois->im.nx,sizeof(float));
	if (tmp == NULL) return (win_printf_OK("cant create tmp array!"));
	pd  = oid->im.pixel;		

	set_im_title(oid,"Average lines profile. (%d, %d) in time staring at %d over %d with a period %d", l_s,l_e,starting,avgt,period);
	set_oi_treatement(oid,"Average line profile. (%d, %d) in time staring at %d over %d with a period %d", l_s,l_e,starting,avgt,period);

	inherit_from_im_to_im(oid,ois);

	uns_oi_2_oi_by_type(ois, IS_X_UNIT_SET, oid, IS_X_UNIT_SET);
	uns_oi_2_oi_by_type(ois, IS_T_UNIT_SET, oid, IS_Y_UNIT_SET);
	for (k = starting; k < nf; k++)
	  {
	    ny = (k - starting)/period;
	    if (ny < 0) continue;
	    if (k > ((ny*period)+avgt)) continue;
	    switch_frame(ois,k);
	    for (i=0 ; i< ois->im.nx ; i++)	tmp[i] = 0;
	    for (i=l_s ; i< l_e ; i++)
	      {
		extract_raw_line(ois, i, tmp);
		for (j=0 ; j< ois->im.nx ; j++)	pd[ny].fl[j] += tmp[j];	
	      }
	  }
	for (k = 0; k < oid->im.ny; k++)
	  {
	    for (i=0, j = (l_e - l_s)*avgt ; i < ois->im.nx && j !=0 ; i++)
	      {
		pd[k].fl[i] /= j;		
	      }
	  }
	free(tmp);
	find_zmin_zmax(oid);
	oid->need_to_refresh = ALL_NEED_REFRESH;
        set_oi_horizontal_extend(oid, 1);
	set_oi_vertical_extend(oid, 1);
	select_image_of_imreg(imr, imr->n_oi-1);
	broadcast_dialog_message(MSG_DRAW,0);	
	return D_REDRAWME;		
}




int do_z_profile_of_movie(void)
{
	register int i;
	static int xc = 0, yc = 0;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds = NULL;	
	int nf;
	imreg *imr = NULL;
	int sx, sy, onx, ony, ix, iy, nm;

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	{
		active_menu->flags |=  D_DISABLED;
		return D_O_K;	
	}		
	if(updating_menu_state != 0)	
	{
		if (ois->im.n_f == 1 || ois->im.n_f == 0)	
			active_menu->flags |=  D_DISABLED;
		else active_menu->flags &= ~D_DISABLED;
		return D_O_K;	
	}	
	sx = z_x_avg_profile_size;
	sy = z_y_avg_profile_size;
	nf = abs(ois->im.n_f);		
	ony = ois->im.ny;	
	onx = ois->im.nx;	

	if (nf <= 0)	
	{
		win_printf("This is not a movie");
		return D_O_K;
	}

	i = win_scanf("I shall coupute an averaged profile over\n"
		      "a rectangular area centered on one point\n"
		      "xc %8d yc %8d\n"
		      "extending over 1 + 2 x Nx along x: Nx %8d (0,32)\n"
		      "extending over 1 + 2 x Ny along y: Ny %8d (0,32)\n"
		      ,&xc,&yc,&sx,&sy);

	if (i == WIN_CANCEL)	return D_O_K;

	if (xc < sx) win_printf_OK("xc (%d) cannot be < Nx (%d)",xc,sx);
	if (yc < sy) win_printf_OK("yc (%d) cannot be < Ny (%d)",yc,sy);
	if (xc > onx - sx) win_printf_OK("xc (%d) cannot be > image x size - Nx (%d)",xc,onx-sx);
	if (yc > ony - sy) win_printf_OK("yc (%d) cannot be > image y size - Ny (%d)",yc,ony-sy);

	//win_printf("xc %d yc %d sx %d sy %d",xc,yc,sx,sy);

	op = create_one_plot(nf,nf,0);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0];

	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	inherit_from_im_to_ds(ds, ois);	
	op->filename = Transfer_filename(ois->filename);	

	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_T_UNIT_SET, op, IS_X_UNIT_SET);
	//for (i=0 ; i< ds->nx ; i++)	ds->xd[i] = i;

	for (i = 0; i < nf; i++) ds->xd[i] = 0;
	for (iy = (yc - sy), nm = 0; iy <= (yc + sy); iy++)
	    { 
	      for (ix = (xc - sx); ix <= (xc + sx); ix++)
		{
		  if (ix >= 0 && iy >= 0 && ix < onx && iy < ony)
		    {
		      extract_z_profile_from_movie(ois, ix, iy, ds->yd);
		      for (i = 0; i < nf; i++) ds->xd[i] += ds->yd[i];
		      nm++;
		    }
		}
	    }
	  for (i = 0; i < nf; i++) 
	    {
	      ds->yd[i] = ds->xd[i];
	      if (nm > 0) ds->yd[i] /= nm;
	      ds->xd[i] = i;
	    }
	if (sx == 0 && sy == 0)
	  {
	    set_plot_title(op,"Movie profile xc %d yc %d",xc, yc);
	    set_ds_treatement(ds,"Movie Z profile at xc %d yc %d",xc, yc);
	  }
	else
	  {
	    set_plot_title(op,"\\stack{{Movie avg profile}"
			   "{\\pt8 xc %d avg %d yc %d avg %d}"
			   "{\\pt8 %d effective avarage}}"
			   ,xc ,1 + 2 * sx, yc ,1 + 2 * sy, nm);
	    set_ds_treatement(ds,"Movie Z avg profile at xc %d over %d "
				"yc %d over %d effective averaging over %d profiles"
				,xc ,1 + 2 * sx
				,yc ,1 + 2 * sy, nm);
	  }


	  //	extract_z_profile_from_movie(ois, xc, yc, ds->yd);
	add_one_image (ois, IS_ONE_PLOT, (void *)op);
	ois->cur_op = ois->n_op - 1;
	ois->need_to_refresh |= PLOT_NEED_REFRESH  | EXTRA_PLOT_NEED_REFRESH;	
	//broadcast_dialog_message(MSG_DRAW,0);	
	return D_REDRAWME;		
}






/********************************************************************************/
/* histogram for an image.				 			*/
/* previously "im_hist"								*/
/********************************************************************************/
int do_image_histogram(void)
{
	register int i, j;	/* for counting in loops 			*/
	int 	r_s=0, r_e=0;   /* starting and ending rows 			*/
	int 	l_s=0, l_e=0;   /* starting and ending lines 			*/
	int	N;		/* total number of usefull points in the image	*/
	int	Nn;		/* number of points really used in the histogram*/
	O_i 	*ois = NULL;       	/* source image 				*/
	O_p 	*op = NULL;		/* destination plot 				*/
	d_s 	*ds = NULL;		/* data of destination plot 			*/
	imreg   *imr = NULL;		/* image region where to work 			*/
	int 	nx, n2;		/* number of points 				*/
	float   *y = NULL;		/* tmp data					*/
	float   zmin, zmax, dz; /* tmp data 					*/
	int	pp;		/* tmp data 					*/
	char    s[1024] = {0};        /* string for title 				*/
	int     nfi = 0;        /* nb of frames */
	int     nfs, nfe, im;
	
	if(updating_menu_state != 0)	return D_O_K;	
		
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)	return D_O_K;

	nfi = abs(ois->im.n_f); 
	nfi = (nfi == 0) ? 1 : nfi;
	nfe = nfi;
	nfs = 0;

	if (key[KEY_LSHIFT]) 	return win_printf_OK("Histogram of a user-specified region of an image \n"
				"result is a plot.\n"
				"If the image is complex, operation is on the real projection\n"
				"plotted on the screen (Re,Im,|A|...)");
	if (r_e==0)  		r_e=ois->im.nx; /* initialisation of r_e */
	if (r_e>ois->im.nx)  	r_e=ois->im.nx; /* initialisation of r_e */
	if (l_e==0)  		l_e=ois->im.ny; /* initialisation of l_e */
	if (l_e>ois->im.ny)  	l_e=ois->im.ny; /* initialisation of l_e */

	if (nfi == 1)
	  {
	    i = win_scanf("{\\pt14\\color{yellow} Histogram of z-data from an image}\n\n"
			  "select region:\n"
			  "between %4d and %4d colomns\n"
			  "between %4d and %4d lines",
			  &r_s,&r_e,&l_s,&l_e);
	  }
	else
	  {
	    i = win_scanf("{\\pt14\\color{yellow} Histogram of z-data from an image}\n\n"
			  "select region:\n"
			  "between %4d and %4d colomns\n"
			  "between %4d and %4d lines\n"
			  "Frames from %3d to %3d\n"
			  ,&r_s,&r_e,&l_s,&l_e,&nfs,&nfe);
	  }
	if (i == WIN_CANCEL) return D_O_K;
	if (r_e<=r_s)    return (win_printf("bad rows limits ! "));
	if (l_e<=l_s)    return (win_printf("bad lines limits ! "));
	
	N = (r_e-r_s)*(l_e-l_s);
	for (n2 = 1; n2 < (N/100); n2 <<= 1); 
	/* a ce stade, n2 est la puissance de 2 la plus proche de N/100 par valeurs inferieures */

	y  = (float*)calloc(ois->im.ny,sizeof(float)); /* complete row 			*/
	if (y == NULL) { free(y); return win_printf("I cannot allocate memory !"); }
	for (im = nfs; im < nfe; im++)
	  {
	    switch_frame(ois,im);
	    for (i=r_s; i<r_e; i++)
	      {	extract_raw_row(ois, i, y);/* extraction of the i-th row from ois to y 	*/
		if (i==r_s && im == nfs) {zmin=zmax=y[l_s];}
		for (j=l_s ; j<l_e ; j++)
		  {	if (y[j]<zmin) {zmin=y[j];}
		    if (y[j]>zmax) {zmax=y[j];}
		  }
	      }
	  }
	nx=n2;

	sprintf(s,"There are %d points in the selected region\n and for these points z is in [%.2f,%.2f]\n\n"
			"select the interval in z you want for the histogram :\n%%8f z_{min}\n%%8f z_{max}"
			"and the number of bins N %%3d",N,zmin,zmax);
	i=win_scanf(s,&zmin,&zmax,&nx); 
	if (i==WIN_CANCEL) 
	{	
	  if (y) free(y); 
	  return D_O_K;
	} 	
	
	dz = (zmax-zmin)/(nx-1); /* au lieu de (nx-1)     NG 28.07.1999 */

	op = create_one_plot( nx, nx, 0); 	/* resulting plot */
	if (op == NULL) return(win_printf("I cannot create hist plot!"));
	ds = op->dat[0];	/* the resulting data ds will be the first data set of the plot op	*/

	Nn=0; /* initialisation */
	for (im = nfs; im < nfe; im++)
	  {
	    switch_frame(ois,im);
	    for (i=r_s ; i<r_e ; i++)     		/* start the loop 				*/
	      {	extract_raw_row(ois, i, y);	/* extraction of the i-th row from ois to y 	*/
		for (j=l_s; j<l_e ; j++)
		  if ( (y[j]>=zmin) && (y[j]<=zmax) )
		    {	pp=(int)( (y[j]-zmin)/dz );
		      ds->yd[pp]++; 	/* line histogram normalised 1/1		*/
		      Nn++; 			/* one more point in the histogram 		*/
		    }			
	      }
	  }
	if (y) free(y); 	/* no more usefull */

	for (i=0; i<(nx); i++)
	{	ds->xd[i]     = zmin+i*dz; 
	}

	sprintf(s,"\\fbox{\\stack{{%d points in histogram}{%d points in image}}}",Nn,N);
	push_plot_label(op,  zmin + (zmin-zmax)/4, Nn/nx, s, USR_COORD);

	set_plot_title(op, "histogram for row %d to %d and line %d to %d",r_s, r_e, l_s, l_e);
	set_ds_treatement(ds, "histogram for row %d to %d and line %d to %d",r_s, r_e,l_s,l_e);
	inherit_from_im_to_ds(ds, ois);				/* ok */
	uns_oi_2_op(ois, IS_Z_UNIT_SET, op, IS_X_UNIT_SET);	/* modifie le 31.05.99 */
	
	op->filename  = Transfer_filename(ois->filename); 	

	add_one_image (ois, IS_ONE_PLOT, (void *)op);      	/* ok */
	return (refresh_im_plot(imr, ois->n_op - 1));		/* ok */

}/* end of the procedure "im_hist" */




/************************************************************************/
/* two images to plot operations	 				*/
/*	28.07.1999 previous version "two_im_to_plot"			*/
/* 	25.02.2004 Allegro port						*/
/************************************************************************/
int 	do_XY_im_to_plot(void)
{
	register int i, j, k;		/* intermediaire compteurs de boucle    */
	imreg 	*imr = NULL;			/* image region 			*/
	O_i 	*oisx = NULL, *oisy = NULL;  		/* one image source (x2) et destination */
	union pix *psx = NULL, *psy = NULL;		/* data des images (matrice)		*/
	int 	r_s=0, r_e=0;   	/* starting and ending rows 		*/
	int 	l_s=0, l_e=0;   	/* starting and ending lines 		*/
	int	nx1, nx2;		/* dimensions en x 		  	*/
	int 	ny1, ny2;		/* dimensions en y			*/
	int 	N;			/* total number of usefull points	*/
	float   *x = NULL;			/* tmp data				*/
	float   *y = NULL;			/* tmp data				*/
	pltreg 	*pr = NULL;			/* for plot region creation		*/
	O_p	*op = NULL;			/* for plot creation			*/
	d_s	*ds = NULL;			/* for dataset creation			*/

	if(updating_menu_state != 0)	return D_O_K;	
		
	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oisy) != 2)	return D_O_K;
	if (imr->n_oi<2)					return D_O_K;

	i = imr->cur_oi;			
	i = ( (i-1 < 0) ? (imr->n_oi-1) : (i-1) );	/* number i-1 */
	oisx = imr->o_i[i]; 	
	psx = imr->o_i[i]->im.pixel;
	psy = imr->o_i[i]->im.pixel;

	if ( (psx==NULL) || (psy==NULL) )
			return (win_printf("I cannot find any data in image"));

	nx1 = oisy->im.nx;  ny1 = oisy->im.ny;  /* dimensions de l'image 1 (y) */
	nx2 = oisx->im.nx;  ny2 = oisx->im.ny;  /* dimensions de l'image 2 (x) */
	if (nx1 != nx2)	return(win_printf_OK("Images have different number of colomns %d and %d !\n"
				"so I cannot work.",nx1,nx2) );
	if (ny1 != ny2)	return(win_printf_OK("images have different number of lines %d and %d !\n"
				"so I cannot work.",ny1,ny2) );

	if (r_e==0) 	r_e=nx1; /* initialisation of r_e */
	if (r_e>nx1)  	r_e=nx1; /* initialisation of r_e */
	if (l_e==0) 	l_e=ny1; /* initialisation of l_e */
	if (l_e>ny1)  	l_e=ny1; /* initialisation of l_e */
 
 	i = win_scanf("Building a plot {\\pt14\\color{yellow}y(x)}\n"
			"with y from current image\n" 
			" and x from previous image.\n" 
			"Use points between colomns %4d and %4d\nand between lines %4d and %4d\n\n"
			"\\pt8note: for images of complex numbers, visible part will be operated on",
			&r_s,&r_e,&l_s,&l_e);
	if (i==WIN_CANCEL)	return D_O_K;		/* user stop 				*/
	if (r_e<=r_s)   return (win_printf_OK("bad colomns limits ! "));
	if (l_e<=l_s)   return (win_printf_OK("bad lines limits ! "));

	N = (r_e-r_s)*(l_e-l_s);

	pr = create_and_register_new_plot_project(0,   32,  900,  668);
	if (pr == NULL)	return(win_printf_OK("Could not find or allocate plot region!"));	

	op = (O_p *)calloc(1,sizeof(O_p));
	if (op == NULL)	return 1;
	init_one_plot(op);
	add_data( pr, IS_ONE_PLOT, (void*)op);	

	pr->cur_op = pr->n_op - 1;
	pr->one_p = pr->o_p[pr->cur_op];

	ds = create_and_attach_one_ds (op, N, N, IS_FLOAT);

	pr->one_p->filename  = Transfer_filename(oisy->filename); 	
//	pr->one_p->dir = Mystrdup(path);
	pr->one_p->dat[0]->source = Transfer_filename(oisy->filename);

/*	else // if error
	{	remove_one_plot_data (pr->one_p, IS_DATA_SET, (void*)ds);
		return 1;
	}					
*/

	x  = (float*)calloc(oisx->im.ny,sizeof(float));
	y  = (float*)calloc(oisy->im.ny,sizeof(float));

	k=0;
	for (i=r_s ; i<r_e ; i++)
	{	extract_raw_row(oisx, i, x);
		extract_raw_row(oisy, i, y);
		for (j=l_s; j<l_e ; j++)
		{	ds->xd[k] = x[j]; 
			ds->yd[k] = y[j];
			k++;
		}			
	}
	if (x) free(x);
	if (y) free(y);

	set_plot_title(op,"y(x) from images");
	set_plot_x_title(op, "x");
	set_plot_y_title(op, "y");	

	do_one_plot(pr);

	broadcast_dialog_message(MSG_DRAW,0);	

	return D_REDRAW;	

} /* end of the "do_XY_im_to_plot" function */


int 	do_im_multi_lines_profile_to_plot(void)
{
  static int start= 0, end = 1, step = 1;
  int i, j, k;
  imreg 	*imr = NULL;			/* image region 			*/
  O_i 	*oi = NULL;  		
  O_p *op = NULL;
  d_s *ds = NULL;
  char question[1024] = {0};

  if(updating_menu_state != 0)	return D_O_K;	
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return D_O_K;
  
  sprintf(question,"This rourine extract a set of line profiles [0, %d[\n"
		"Define the starting profile index %%6d\n"
	  "Define the ending profile index (included)%%6d\n"
	  "grabing every one profile every %%6d",oi->im.ny);
  i = win_scanf(question,&start,&end,&step);
  if (i == WIN_CANCEL)	  return D_O_K;
  if (start < 0 || start >= oi->im.ny)
    return win_printf_OK("Starting index %d must be > 0 and < %d !",start,oi->im.ny);
  if (end < 0 || end >= oi->im.ny)
    return win_printf_OK("Ending index %d must be > 0 and < %d !",end,oi->im.ny);
  if (start > end)
    return win_printf_OK("Starting index %d must be <= to ending one %d!",start,end);

  op = create_and_attach_op_to_oi(oi, oi->im.nx, oi->im.nx, 0, IM_SAME_X_AXIS);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  for (i = start+step; i <= end; i += step)
    {
      if ((ds = create_and_attach_one_ds(op, oi->im.nx, oi->im.nx, 0)) == NULL)
	return win_printf("I can't create ds !");
    }
  op->y_lo = oi->z_black;
  op->y_hi = oi->z_white;    
  op->iopt2 |= Y_LIM;
  op->type = IM_SAME_X_AXIS;
  op->x_title = Mystrdupre(op->x_title, "Position along line");
  op->filename = Transfer_filename(oi->filename);
  uns_oi_2_op(oi, IS_Z_UNIT_SET, op, IS_Y_UNIT_SET);
  uns_oi_2_op(oi, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
  set_plot_title(op,"line profiles from %d to %d",start,end);
  for (i = start, k = 0; i <= end; i += step, k++)
    {
      ds = op->dat[k];
      for (j=0 ; j< ds->nx ; j++)    ds->xd[j] = j;
      extract_raw_line(oi, i, ds->yd);        
      //set_ds_line_color(ds, Lightred);
      if (oi->n_yu > 0)
	set_ds_treatement(ds,"Multi lines profile at %d -> %g %s",
		       i,oi->ay+oi->dy*i,(oi->yu[oi->c_yu]->name != NULL) ? 
		       oi->yu[oi->c_yu]->name : " ");
      else set_ds_treatement(ds,"Multi line profile at %d",i);
      ds->source = Mystrdupre(ds->source, oi->im.source);
    }
  oi->need_to_refresh |= PLOTS_NEED_REFRESH;
  return refresh_im_plot(imr, oi->cur_op);
}

int 	do_im_multi_rows_profile_to_plot(void)
{
  static int start= 0, end = 1, step = 1;
  int i, j, k;
  imreg 	*imr = NULL;			/* image region 			*/
  O_i 	*oi = NULL;  		
  O_p *op = NULL;
  d_s *ds = NULL;
  char question[1024] = {0};

  if(updating_menu_state != 0)	return D_O_K;	
  
  if (ac_grep(cur_ac_reg,"%im%oi",&imr,&oi) != 2)	return D_O_K;
  
  sprintf(question,"This rourine extract a set of row profiles [0, %d[\n"
		"Define the starting profile index %%6d\n"
	  "Define the ending profile index (included)%%6d\n"
	  "grabing every one profile every %%6d",oi->im.nx);
  i = win_scanf(question,&start,&end,&step);
  if (i == WIN_CANCEL)	  return D_O_K;
  if (start < 0 || start >= oi->im.nx)
    return win_printf_OK("Starting index %d must be > 0 and < %d !",start,oi->im.nx);
  if (end < 0 || end >= oi->im.nx)
    return win_printf_OK("Ending index %d must be > 0 and < %d !",end,oi->im.nx);
  if (start > end)
    return win_printf_OK("Starting index %d must be <= to ending one %d!",start,end);

  op = create_and_attach_op_to_oi(oi, oi->im.ny, oi->im.ny, 0, IM_SAME_X_AXIS);
  if (op == NULL)	return win_printf_OK("Could not create plot data");
  for (i = start+step; i <= end; i += step  )
    {
      if ((ds = create_and_attach_one_ds(op, oi->im.ny, oi->im.ny, 0)) == NULL)
	return win_printf("I can't create ds !");
    }
  op->y_lo = oi->z_black;
  op->y_hi = oi->z_white;    
  op->iopt2 |= X_LIM;
  op->type = IM_SAME_Y_AXIS;
  op->y_title = Mystrdupre(op->y_title, "Position along row");
  op->filename = Transfer_filename(oi->filename);
  uns_oi_2_op(oi, IS_Z_UNIT_SET, op, IS_X_UNIT_SET);
  uns_oi_2_op(oi, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
  set_plot_title(op,"row profiles from %d to %d",start,end);
  for (i = start, k = 0; i <= end; i += step, k++)
    {
      ds = op->dat[k];
      for (j=0 ; j< ds->nx ; j++)    ds->yd[j] = j;
      extract_raw_row(oi, i, ds->xd);        
      //set_ds_line_color(ds, Lightred);

      if (oi->n_xu > 0)
	set_ds_treatement(ds,"Multi rows profile at %d -> %g %s",
		       i,oi->ax+oi->dx*i,(oi->xu[oi->c_xu]->name != NULL) ? 
		       oi->xu[oi->c_xu]->name : " ");
      
      else set_ds_treatement(ds,"Multi row profile at %d",i);
      ds->source = Mystrdupre(ds->source, oi->im.source);
    }
  oi->need_to_refresh |= PLOTS_NEED_REFRESH;
  return refresh_im_plot(imr, oi->cur_op);


} /* end of the "do_XY_im_to_plot" function */

int do_followMax_along_y(void)
{
	register int i, j;
	static int l_s = -1, l_e = -1, posi = 25, wmax = 10, ismax;
	int maxi = 0;
	float max, Max_pos, Max_val, Max_deriv;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds = NULL, *dst = NULL;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average the image intensity"
			"of several lines of an image");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	return D_O_K;
	/* we grab the data that we need, the image and the screen region*/
	if (l_s < 0) l_s = 0; 
	if (l_e < 0) l_e = ois->im.ny;
	/* we ask for the limit of averaging*/
	i = win_scanf("Follow extremum Between lines%6d and %6d\n"
		      "using a 3rd order polynom passing by 4 points of the Max\n"
		      "%R->Min %r->Max\n"
		      "Define the starting Max position \n"
		      "in pixel %6d extend of search %6d\n",&l_s,&l_e,&ismax,&posi,&wmax);
	if (i == WIN_CANCEL)	return D_O_K;

	/* we create a plot to hold the profile*/
	dst = build_data_set(ois->im.nx,ois->im.nx);
	if (dst == NULL) return (win_printf_OK("cant create plot!"));
	op = create_and_attach_op_to_oi(ois,ois->im.ny,ois->im.ny,0,IM_SAME_Y_AXIS|IM_SAME_X_AXIS);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0]; /* we grab the data set */
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	inherit_from_im_to_ds(ds, ois);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"follow Max from line %d to %d start at %d",l_s, l_e,posi);
	set_ds_treatement(ds,"follow Max from line %d to %d start at %d",l_s, l_e,posi);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, dst->yd);
		if (ismax == 0)
		  for (j = 0; j < dst->nx; j++) dst->yd[j] *= -1; 
		for(j = posi - wmax, max = -FLT_MAX; j <= posi + wmax && j < ois->im.nx; j++)
		  {
		    if (j < 0) continue;
		    if (dst->yd[j] > max) max = dst->yd[maxi = j];
		  } 
		if (max != -FLT_MAX)
		  {
		    find_max_around(dst->yd, dst->nx, maxi, &Max_pos, &Max_val, &Max_deriv);
		    ds->xd[i] = Max_pos;
		    ds->yd[i] = i;
		    posi = (int)(0.5 + Max_pos);
		  }
	}
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}



int do_followZero_along_y(void)
{
	register int i, j;
	static int l_s = -1, l_e = -1, posi = 25, wzer = 10;
	int zeri = 0;
	float zer, Zer_pos, Zer_deriv;
	O_i *ois = NULL;
	O_p *op = NULL;
	d_s *ds = NULL, *dst = NULL;
	imreg *imr = NULL;


	if(updating_menu_state != 0)	return D_O_K;

	if (key[KEY_LSHIFT])
	{
		return win_printf_OK("This routine average the image intensity"
			"of several lines of an image");
	}

	if (ac_grep(cur_ac_reg,"%im%oi",&imr,&ois) != 2)
	return D_O_K;
	/* we grab the data that we need, the image and the screen region*/
	if (l_s < 0) l_s = 0; 
	if (l_e < 0) l_e = ois->im.ny;
	/* we ask for the limit of averaging*/
	i = win_scanf("Follow a zero Between lines%6d and %6d\n"
		      "using a 2rd order polynom passing by 3 points near zero\n"
		      "Define the starting Zer position \n"
		      "in pixel %6d extend of search %6d\n",&l_s,&l_e,&posi,&wzer);
	if (i == WIN_CANCEL)	return D_O_K;

	/* we create a plot to hold the profile*/
	dst = build_data_set(ois->im.nx,ois->im.nx);
	if (dst == NULL) return (win_printf_OK("cant create plot!"));
	op = create_and_attach_op_to_oi(ois,ois->im.ny,ois->im.ny,0,IM_SAME_Y_AXIS|IM_SAME_X_AXIS);
	if (op == NULL) return (win_printf_OK("cant create plot!"));
	ds = op->dat[0]; /* we grab the data set */
	op->y_lo = ois->z_black;	op->y_hi = ois->z_white;
	inherit_from_im_to_ds(ds, ois);
	op->filename = Transfer_filename(ois->filename);
	set_plot_title(op,"follow Zero from line %d to %d start at %d",l_s, l_e,posi);
	set_ds_treatement(ds,"follow Zero from line %d to %d start at %d",l_s, l_e,posi);
	uns_oi_2_op(ois, IS_Y_UNIT_SET, op, IS_Y_UNIT_SET);
	uns_oi_2_op(ois, IS_X_UNIT_SET, op, IS_X_UNIT_SET);
	for (i=l_s ; i< l_e ; i++)
	{
		extract_raw_line(ois, i, dst->yd);
		for(j = posi - wzer, zer = FLT_MAX; j <= posi + wzer && j < ois->im.nx; j++)
		  {
		    if (j < 0) continue;
		    if (fabs(dst->yd[j]) < fabs(zer)) zer = dst->yd[zeri = j];
		  } 
		if (zer != FLT_MAX)
		  {
		    find_zero_around(dst->yd, dst->nx, zeri, &Zer_pos,  &Zer_deriv);
		    ds->xd[i] = Zer_pos;
		    ds->yd[i] = i;
		    posi = (int)(0.5 + Zer_pos);
		  }
	}
	broadcast_dialog_message(MSG_DRAW,0);
	return D_REDRAWME;
}

int do_find_max_frame_value_in_movie(void)
{
    int im;
    int  nfi;
    O_i *ois = NULL;
    O_p *op = NULL;
    d_s *ds = NULL, *dsz = NULL, *dszout = NULL, *dsz2 = NULL;
    imreg *imr = NULL;
    float max_val;


    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) < 2)   return OFF;

    nfi = (ois != NULL) ? abs(ois->im.n_f) : 0;
    nfi = (nfi == 0) ? 1 : nfi;

    if (updating_menu_state != 0)
    {
        if (nfi <= 1)      active_menu->flags |=  D_DISABLED;
        else               active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }

    op = create_and_attach_op_to_oi(ois, nfi, nfi, 0, IM_SAME_X_AXIS|IM_SAME_Y_AXIS);
    if (op == NULL)	return win_printf_OK("Could not create plot data");
    ds = op->dat[0];
    dsz = create_and_attach_one_ds(op, nfi, nfi, 0);
    if (dsz == NULL)	return win_printf_OK("Could not create dsz");
    dszout = create_and_attach_one_ds(op, nfi, nfi, 0);
    if (dszout == NULL)	return win_printf_OK("Could not create dsz");
    dsz2 = create_and_attach_one_ds(op, nfi, nfi, 0);
    if (dsz2 == NULL)	return win_printf_OK("Could not create dsz");            
    inherit_from_im_to_ds(ds, ois);
    inherit_from_im_to_ds(dsz, ois);
    inherit_from_im_to_ds(dszout, ois);
    inherit_from_im_to_ds(dsz2, ois);            
    update_ds_treatement(ds, "Position of maximum");
    update_ds_treatement(dsz, "Maximum vs frame number");
    update_ds_treatement(dszout, "Maximum of backgound vs frame number");
    update_ds_treatement(dsz2, "Variance of backgound vs frame number");            
    for (im = 0; im < nfi; im++)
      {
	switch_frame(ois,im);
	find_zmax_and_pos_interpolate(ois, &max_val, ds->xd+im, ds->yd+im);
	find_zmax_excluding_an_area(ois, ds->xd[im], ois->im.nx/4, ds->yd[im], ois->im.ny/4,dszout->yd+im, dsz2->yd+im);
	dsz->yd[im] = max_val;
	dsz->xd[im] = im;
	dszout->xd[im] = im;
	dsz2->xd[im] = im;
      }
    ois->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, UNCHANGED));
    
}

int do_move_frame_from_ds_in_movie(void)
{
    int im, i, j;
    int  nfi;
    O_i *ois = NULL, *oid = NULL;
    O_p *op = NULL;
    d_s *ds = NULL;
    imreg *imr = NULL;


    if (ac_grep(cur_ac_reg, "%im%oi", &imr, &ois) < 2)   return OFF;
    op = find_oi_cur_op(ois);
    nfi = (ois != NULL) ? abs(ois->im.n_f) : 0;
    nfi = (nfi == 0) ? 1 : nfi;

    if (updating_menu_state != 0)
    {
        if (op == NULL || nfi <= 1)      active_menu->flags |=  D_DISABLED;
        else               active_menu->flags &= ~D_DISABLED;
        return D_O_K;
    }
    ds = op->dat[0];
    if (ds->nx != nfi)
      return win_printf_OK("Plot does not have the same number of points\n"
			   "than the movie has frames!");
    if (ois->im.data_type != IS_CHAR_IMAGE)
      return win_printf_OK("This works for char image only");
    oid = create_and_attach_movie_to_imr(imr,ois->im.nx, ois->im.ny, IS_CHAR_IMAGE,nfi);
    if (oid == NULL)
      return win_printf_OK("cannot create movie!");
    for (im = 0; im < nfi; im++)
      {
	switch_frame(ois,im);
	switch_frame(oid,im);	
	for (i = 0; i < ois->im.ny; i++)
	  {
	    for (j = 0; j < ois->im.nx; j++)
	      {
		oid->im.pixel[i].ch[j] = (unsigned char)(fast_interpolate_image_point(ois, ds->xd[im]+j, ds->yd[im]+i, 127) >> 8);
	      }
	  }
      }
    oid->need_to_refresh = ALL_NEED_REFRESH;
    return (refresh_image(imr, UNCHANGED));
    
}

