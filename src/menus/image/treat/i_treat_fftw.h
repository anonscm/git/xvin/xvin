/********************************************************************************/
/* FT and Filtering, using FFTW3						*/
/********************************************************************************/
/* Nicolas Garnier	nicolas.garnier@ens-lyon.fr				*/
/********************************************************************************/

// definitions for computing the spectrum
#define LIN_SPEC	0x0100
#define LOG_SPEC	0x0200

#define FFT2D_FORWARD	0x0100
#define FFT2D_BACKWARD	0x0200

int 	do_y_spectrum_average_along_x(void);
int	do_x_spectrum_average_along_y(void);
int	do_im_y_spectrum(void);
int	do_im_x_spectrum(void);
int	do_filter_image(void);
int 	do_fft_2d(void);
int	do_build_and_apply_2d_filter(void);

