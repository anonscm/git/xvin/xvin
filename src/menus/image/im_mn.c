
# ifndef _IM_MN_C_
# define _IM_MN_C_

# include <stdlib.h>
# include <string.h>

# include "allegro.h"
# include "xvin.h"
# include "plot_op.h"
# include "gr_file.h"
# include "plot_reg.h"
# include "plot_gr.h"
# include "im_reg.h"
# include "im_gr.h"
# include "i_treat_mn.h"
# include "im_mn.h"

XV_ARRAY(MENU,  project_menu);
XV_ARRAY(MENU,  fortify_menu);
XV_ARRAY(MENU,  help_menu);
XV_FUNC(int, do_quit, (void));
XV_FUNC(int, help_me, (void));





MENU image_menu[32] = 
{ 
   { "&File",            NULL,             image_file_menu,  0, NULL  },
   { "&Edit",	         NULL,             image_edit_menu,  0, NULL  },
   { "&Project",         NULL,             project_menu,  0, NULL  },
   { "O&ptions",         NULL,             image_option_menu,   0, NULL  },
   { "&Display",         NULL,             plot_display_menu,  0, NULL  },
   { "&Treatement",      NULL,             image_treat_menu,  0, NULL  },
#ifdef FORTIFY
   { "&Fortify",         NULL,             fortify_menu,  0, NULL  },
#endif
   { "&Help",            NULL,             help_menu,  0, NULL  },
   { "&Quit\tQ",         do_quit,          NULL,       0, NULL  },
   { NULL,               NULL,             NULL,       0, NULL  }
};

# ifdef OLD
int check_image_menu(void)
{
	check_image_opt();
	check_image_scale();
	return 0;
}
# endif

int init_image_menu(void)
{
    static MENU *mn = NULL;


	if (mn != NULL)	return 1;

    	mn = image_basic_treat_menu();
    		add_image_treat_menu_item("Basic operations", 		NULL, mn, 0, NULL);

	mn = image_measures_treat_menu();
    		add_image_treat_menu_item("Measurements", 		NULL, mn, 0, NULL);

	mn = image_to_plot_treat_menu();
    		add_image_treat_menu_item("Image to plot", 		NULL, mn, 0, NULL);

	mn = movie_to_image_treat_menu();
    		add_image_treat_menu_item("Movie to image", 		NULL, mn, 0, NULL);

	mn = image_math_treat_menu();
    		add_image_treat_menu_item("Math. on 1 image", 		NULL, mn, 0, NULL);

	mn = image_2images_treat_menu();
    		add_image_treat_menu_item("Math. on 2 images" , 	NULL, mn, 0, NULL);

	mn = image_filter_treat_menu();
    		add_image_treat_menu_item("Filtering" , 		NULL, mn, 0, NULL);
	return 0;
}

int	set_image_menu_text(int index, char* text)
{
	if (index < 0 || index >= 32)	return 1;
	image_menu[index].text = text;
	return 0;
}

int	set_image_menu_proc(int index, int (*proc)(void))
{
	if (index < 0 || index >= 32)	return 1;
	image_menu[index].proc = proc;
	return 0;
}




int	add_image_menu_item( char* text, int (*proc)(void), struct MENU* child, int flags, void* dp)
{
	register int i;
	for (i = 0; i < 32 && image_menu[i].text != NULL; i++);
	if (i >= 30)	return -1;
	image_menu[i].text = text;
	image_menu[i].proc = proc;
	image_menu[i].child = child;
	image_menu[i].flags = flags;
	image_menu[i].dp = dp;			
	image_menu[i+1].text = NULL;		
	return 0;	 
}


# endif


