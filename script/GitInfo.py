#!/usr/bin/env python
# encoding: utf-8
import subprocess
def _cmd(*args):
    return str(subprocess.check_output(('git',)+tuple(args)).strip())

def version():
    return _cmd('describe', '--dirty=+', '--always')

def lastCommitHash():
    return _cmd('log', '-n', '1', '--pretty=format:%H')

def lastCommitDate():
    return _cmd('log', '-n', '1', '--pretty=format:%cD')

def lastCommitAuthor():
    return _cmd('log', '-n', '1', '--pretty=format:%an')

def currentBranch():
    return _cmd('rev-parse', '--abbrev-ref', 'HEAD')

def isDirty():
    return version()[-1] == '+'
