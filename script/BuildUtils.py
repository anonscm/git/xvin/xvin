#! /usr/bin/env python
# encoding: utf-8

import sys
from waflib.Configure import conf
from waflib.Errors import ConfigurationError


### WAF FIX ###
def bld_command(*k):
    fun = k[0]
    name = fun.__name__
    from waflib.Build import BuildContext
    class tmp(BuildContext):
        cmd = name
        fun = name
    return fun
### END WAF FIX ###

def get_platform(conf):
    if hasattr(conf, "options") and conf.options.platform:
        return conf.options.platform

    else:
        print(sys.platform)
        if "darwin" in sys.platform:
            return "macos"
        elif "win" in sys.platform or "msys" in sys.platform:
            return "win"
        else:
            return "linux"

OPENMP_CODE = '''
#ifndef _OPENMP
 choke me
#endif
#include <omp.h>
int main () { return omp_get_num_threads (); }
'''

@conf
def check_openmp_cflags(self, **kw):
    self.start_msg('Checking for $CC option to support OpenMP')
    kw.update({'fragment': OPENMP_CODE})
    def _test(flag):
        fragment = """
                   #include <omp.h>

                   int main(int, char **)
                   {
                       auto _ = omp_get_num_threads();
                       float x[100];
                       for(int i = 0; i < _; ++i)
                           x[i] = i;

                   #   pragma omp parallel for
                       for(int i = 0; i < 100; ++i)
                           x[i] = i*i;
                       return 0;
                   }
                   """
        return self.check_cxx(fragment  = fragment,
                             msg       = 'checking for openmp',
                             cxxflags  = [flag],
                             linkflags = [flag],
                             execute   = False,
                             mandatory = False)
    for flag in ('-fopenmp', '-xopenmp', '-openmp', '-mp', '-omp', '-qsmp=omp'):
        if _test(flag):
            self.env.append_unique('CFLAGS',    flag)
            self.env.append_unique('CXXFLAGS',  flag)
            self.env.append_unique('LINKFLAGS', flag)
            break
    else:
        self.fatal("No OPENMP found")
