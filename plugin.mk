##########################################################
# generic rules to build a XVin plugin
##########################################################
#
# this makefile contains some generic definitions first,
# and then generic rules. it should not be edited.
#
# if you have specific definitions or rules for a given plug'in,
# they should be written at the begining of the makefile located
# in the source directory of the given plug'in
#
# if you want to include platform specific definitions or rules,
# then edit the file "platform.mk"
##########################################################



# below are generic definitions (carefull with spaces!):
##########################################################
PL_NAME			=$(strip $(NAME))
OTHER_FILES		:=$(strip $(OTHER_FILES))
OTHER_LIB_FROM_DLL	:=$(strip $(OTHER_LIB_FROM_DLL))
LIB_DEPS   		:=$(strip $(LIB_DEPS))
XVIN_DIR   		:=$(strip $(XVIN_DIR))
PL_OBJS			=$(PL_NAME).o
PL_OUT 			=$(XVIN_DIR)$(LIB_PREFIX)$(PL_NAME).$(LIB_EXT)
O_DIR           :=$(strip $(O_DIR))
C_DIR           :=$(strip $(C_DIR))

ifneq "$(strip $(OTHER_FILES))" ""
OTHER_O    = $(addsuffix .o,$(strip $(OTHER_FILES)))
PL_OBJS   += $(OTHER_O)
endif

ifneq "$(O_DIR)" ""
FULL_OBJS := $(addprefix $(O_DIR),$(PL_OBJS))
$(FULL_OBJS) :: $(PL_OBJS)
else
FULL_OBJS := $(PL_OBJS)
endif

vpath %.c src/ ./ base/ $(C_DIR)
vpath %.h include/
vpath %.o obj/ $(O_DIR)

ifneq "$(strip $(OTHER_LIB_FROM_DLL))" ""
OTHER_LIBA = $(addsuffix .a,$(strip $(OTHER_LIB_FROM_DLL)))
OTHER_LIB_DEF = $(addsuffix .def,$(strip $(OTHER_LIB_FROM_DLL)))
OTHER_LIB_LIB = $(addprefix $(XVIN_DIR)lib/lib,$(addsuffix .a,$(strip $(OTHER_LIB_FROM_DLL))))
OTHER_LIB_DLL = $(addprefix $(XVIN_DIR)bin/,$(addsuffix .dll,$(strip $(OTHER_LIB_FROM_DLL))))

ifneq "$(SYSTEM)" "WIN32"
$(error building a library out of a dll file can only be done on win32 systems...)
endif
endif

ifneq "$(strip $(PL_NAME))" "alfont"
LIB_DEPS  += PlayItAgainSam
endif



# below are the rules, they differ whether makefile was invocated
# directly in the plug'in directory, or from the main xvin makefile
# (in that case, the variable $(XVIN_MAIN_MAKEFILE) is not defined)
##########################################################

ifndef $(XVIN_MAIN_MAKEFILE)
include $(XVIN_DIR)platform.mk	# platform specific definitions
ifeq ($(CXX_MODULE), 1)
    CC = $(CXX)
endif
.PHONY	: all clean zip global_all global_clean global_zip
all	: global_all
clean 	:: global_clean
zip 	:: local_zip
else
.PHONY  : global_all global_clean global_zip
endif
.SUFFIXES = .o .c .cc

global_all : $(PL_OUT)

global_clean ::
	rm -f $(XVIN_DIR)$(LIB_PREFIX)$(PL_NAME).$(LIB_EXT) $(FULL_OBJS) ; \
	rm -f $(XVIN_DIR)lib/lib$(PL_NAME).a ; \
	rm -f $(OTHER_LIB_LIB) $(OTHER_LIBA) $(OTHER_LIB_DLL)

local_zip :: $(PL_NAME).c $(PL_NAME).h $(patsubst %.o,%.c,$(OTHER_O)) $(patsubst %.o,%.h,$(OTHER_O)) $(ZIPALSO) Makefile
	cd ../..; \
	$(ZIPEXE) plug-ins-src/$(PL_NAME)/$(PL_NAME)$(ZIP_EXT) $(addprefix plug-ins-src/$(PL_NAME)/, $^); \
	cd plug-ins-src/$(PL_NAME)

global_zip :: $(PL_NAME).c $(PL_NAME).h $(patsubst %.o,%.c,$(OTHER_O)) $(patsubst %.o,%.h,$(OTHER_O)) $(ZIPALSO) Makefile
	cd ../..; \
	$(ZIPEXE) $(ZIPALL)$(ZIP_EXT) $(addprefix plug-ins-src/$(PL_NAME)/, $^); \
	cd plug-ins-src/$(PL_NAME)

#-fstack-protector-all

ifndef OTHER_LIBA
$(XVIN_DIR)bin/$(PL_NAME).dll :: $(FULL_OBJS) Makefile
	$(CC) $(PLATFORM_FLAGS) $(LCFLAGS) -o $@ -mwindows -shared \
    -L$(XVIN_DIR)lib  $(addprefix -L,$(addsuffix /lib,$(strip $(GNU_LIBS_DIR)))) $(FULL_OBJS) \
	 $(addprefix -l,$(LIB_DEPS)) -lalleg44.dll -Wl,--export-all-symbols \
	-Wl,--enable-auto-import -Wl,--out-implib=$(XVIN_DIR)lib/lib$(PL_NAME).a
endif

#-fstack-protector-all

ifdef OTHER_LIBA
# $(addprefix $(XVIN_DIR)lib/lib,$(OTHER_LIBA))
$(XVIN_DIR)bin/$(PL_NAME).dll :: $(FULL_OBJS)  $(OTHER_LIB_DEF) $(OTHER_LIB_LIB) $(OTHER_LIB_DLL) Makefile
	$(CC) $(PLATFORM_FLAGS)  $(LCFLAGS) -o $@ -mwindows -shared \
    -L$(XVIN_DIR)lib $(addprefix -L,$(addsuffix /lib,$(strip $(GNU_LIBS_DIR)))) $(FULL_OBJS) \
     $(addprefix -l,$(LIB_DEPS)) $(addprefix -l,$(OTHER_LIB_FROM_DLL)) -lalleg44.dll -Wl,--export-all-symbols \
	-Wl,--enable-auto-import -Wl,--out-implib=$(XVIN_DIR)lib/lib$(PL_NAME).a



$(OTHER_LIB_DLL) :: $(OTHER_LIBA:.a=.dll)
	cp -uvp $(patsubst $(XVIN_DIR)bin/%, %, $@)  $@

#$(XVIN_DIR)lib/lib$(OTHER_LIBA)
$(OTHER_LIB_LIB) :: $(OTHER_LIBA)
	cp -uvp $(patsubst $(XVIN_DIR)lib/lib%, %, $@)  $@

$(OTHER_LIBA)  ::  $(OTHER_LIBA:.a=.dll) $(OTHER_LIBA:.a=.def)
	dlltool --def $(patsubst %.a,%.def,$@) -k --dllname $(patsubst %.a,%.dll,$@) --output-lib $@

$(OTHER_LIB_DEF) :: $(OTHER_LIBA:.a=.dll)
	gendef $(patsubst %.def,%.dll,$@)
#	pexports $(patsubst %.def,%.dll,$@) | sed 's/^_//' > $@

endif


$(XVIN_DIR)bin/lib$(PL_NAME).so :: $(FULL_OBJS) Makefile
	$(CC) $(PLATFORM_FLAGS) -o $@ -shared -L$(XVIN_DIR)lib $(PLUG_INS_LIB) \
	$(FULL_OBJS) $(addprefix -l,$(LIB_DEPS)) -Wl,-rpath,$(HOME)/xvin/bin  $(ALLEGRO_LIB) -L$(XVIN_DIR)bin

$(XVIN_DIR)bin/lib$(PL_NAME).dylib :: $(FULL_OBJS) Makefile
	$(CC) $(PLATFORM_FLAGS) -o $@ -dynamiclib -Wl,-dynamic -L$(XVIN_DIR)lib $(PLUG_INS_LIB) \
	$(FULL_OBJS) $(addprefix -l,$(LIB_DEPS)) $(ALLEGRO_LIB) -L$(XVIN_DIR)bin; \
	$(foreach toto,$(LIB_DEPS), \
		install_name_tool -change ../../bin/lib$(toto).dylib \
			@executable_path/../PlugIns/lib$(toto).dylib \
			$(XVIN_DIR)bin/lib$(PL_NAME).dylib; \
	)

$(XVIN_DIR)lib/lib$(PL_NAME).a : $(FULL_OBJS) Makefile
	$(CC) $(PLATFORM_FLAGS) -o $@ $(PLUG_INS_LIB) \
	$(FULL_OBJS) $(addprefix -l,$(LIB_DEPS)) $(ALLEGRO_LIB)

ifeq ($(CXX_MODULE), 1)
$(PL_OBJS) :: %.o: %.cc
	$(CXX) $(CXX_FLAGS) $(LCFLAGS) $(PCFLAGS) -I./plug-ins-src/$(PL_NAME) $(PLUG_INS_INC) \
    -o $(O_DIR)$@ $(C_DIR)$(notdir $<)
else

$(PL_OBJS) :: %.o: %.c
	$(CC) $(LCFLAGS) $(PCFLAGS) -I. -o $(O_DIR)$@ $(C_DIR)$(notdir $<)
endif
# -I./plug-ins-src/$(PL_NAME) $(PLUG_INS_INC) \
