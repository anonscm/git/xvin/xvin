
#################################################################
# main makefile for XVin                                        #
#                                                               #
# this file should NOT be modified                              #
# edit "platform.mk" to select your system and choose target    #
#                                                               #
# note: all XXX.mk should only contain definitions, no rules!   #
#################################################################
XVIN_MAIN_MAKEFILE = yes

include platform.mk


#################################################
# below are definitions of lists of files:      #
#################################################

#NEEDED_DLL = $(ldd bin/xvin.exe | grep -v /c/ | grep -v '???' | cut -f -1 -d ' ' | cut -f 2-)
NEEDED_DLL = alleg44.dll libgcc_s_dw2-1.dll libwinpthread-1.dll libfreetype-6.dll libbz2-1.dll libharfbuzz-0.dll libglib-2.0-0.dll \
libintl-8.dll libiconv-2.dll libpng16-16.dll zlib1.dll libfftw3-3.dll libstdc++-6.dll

DEV_O = obj/dev_pcx.o obj/dev_ps.o obj/color.o
DEV_H = include/xvin/dev_def.h include/xvin/color.h include/xvin/plot_opt.h 
DEV_S = src/dev_pcx.c src/color.c src/dev_ps.c

BOX_O = obj/box_def.o obj/box_eqs.o obj/box2dev.o obj/box2alfn.o
BOX_H = include/xvin/box_def.h include/xvin/box2alfn.h include/xvin/fops2.h include/generated/config.h
BOX_S = src/box_def.c src/box_eqs.c src/box2dev.c src/box2alfn.c

PLOT_O = obj/unitset.o obj/plot_ds.o obj/plot_op.o obj/plot_gr.o obj/plot2box.o obj/op2box.o 
PLOT_H = include/xvin/unitset.h include/xvin/plot_ds.h include/xvin/plot_op.h\
		 include/xvin/plot2box.h include/xvin/plot_gr.h include/xvin/op2box.h
PLOT_S = src/unitset.c src/plot_ds.c src/plot_op.c src/plot2box.c src/op2box.c src/plot_gr.c 

GUI_O = obj/unit_gui.o obj/imr_reg_gui.o obj/file_picker_gui.o
ifdef XV_GTK_DIALOG
	GUI_O += obj/ds_viewer.o obj/form_builder.o obj/progress_builder.o
endif
GUI_H = include/xvin/imr_reg_gui.h include/xvin/file_picker_gui.h include/xvin/ds_viewer.h include/xvin/form_builder.h include/xvin/progress_builder.h
GUI_S = src/gui/unit_gui.c src/gui/imr_reg_gui.c src/gui/file_picker_gui.c src/gui/ds_viewer.c src/gui/gtk_helper/form_builder.c src/gui/gtk_helper/progress_builder.c


OI_O = obj/im_oi.o obj/im_reg.o obj/im_gr.o 
OI_H = include/xvin/im_oi.h include/xvin/im_reg.h include/xvin/im_gr.h 
OI_S = src/im_oi.c src/im_reg.c src/im_gr.c 

PRO = obj/plot_reg.o obj/plot_reg_gui.o obj/project.o obj/plot_proj.o
PRH = include/xvin/plot_reg.h include/xvin/plot_reg_gui.h
PRS = src/plot_reg.c src/gui/plot_reg_gui.c src/menus/project.c src/plot_proj.c

UTILO = obj/xvinerr.o obj/util.o obj/gui_def.o obj/win_gui.o obj/xv_main.o \
 obj/log_file.o obj/win_file.o obj/xv_tools_lib.o obj/global_vars.o 
UTILH = include/xvin/xvinerr.h include/xvin/util.h include/xvin/gui_def.h include/xvin/xv_main.h\
 include/xvin/fonts.h include/xvin/platform.h include/xvin/log_file.h \
 include/xvin/xv_tools_lib.h include/xvin/xv_plugins.h 
UTILS = src/xvinerr.c src/util.c src/gui/gui_def.c src/gui/win_gui.c \
 src/xv_main.c src/log_file.c src/gui/win_file.c src/lib/xv_tools_lib.c \
 src/xv_plugins.c src/global_vars.c 

FFTO = obj/fillib.o obj/fftl32n.o obj/fft_filter_lib.o obj/fftw3_lib.o
FFTH = include/xvin/fillib.h include/xvin/fftl32n.h include/xvin/fft_filter_lib.h include/xvin/fftw3_lib.h
FFTS = src/fillib.c src/fftl32n.c src/lib/fft_filter_lib.c src/lib/fftw3_lib.c

GRO = obj/gr_file.o 
GRH = include/xvin/gr_file.h
GRS = src/gr_file.c

XVINO = obj/xvinu.o
XVINH = include/xvin.h
XVINS = src/xvinu.c 

PL_MN_O = obj/p_file_mn.o obj/p_edit_mn.o obj/p_disp_mn.o obj/p_tools_mn.o \
 obj/p_treat_mn.o obj/plot_mn.o obj/p_options_mn.o 
PL_MN_H = include/xvin/plot_mn.h 
#\   src/menus/plot/treat/p_treat_math.h \
 #include/xvin/matrice.h src/menus/plot/treat/p_treat_basic.h src/menus/plot/treat/p_treat_fftw.h
PL_MN_S = src/menus/plot/p_file_mn.c src/menus/plot/p_edit_mn.c\
 src/menus/plot/p_disp_mn.c src/menus/plot/p_tools_mn.c src/menus/plot/plot_mn.c\
 src/menus/plot/p_treat_mn.c src/menus/plot/p_options_mn.c 

PLTR_MN_O = obj/interpol.o obj/matrice.o \
 obj/p_treat_basic.o obj/p_treat_math.o obj/p_treat_fftw.o obj/p_treat_p2im.o 
PLTR_MN_H = src/menus/plot/treat/p_treat_basic.h src/menus/plot/treat/p_treat_math.h \
 src/menus/plot/treat/p_treat_fftw.h src/menus/plot/treat/p_treat_p2im.h include/xvin/matrice.h
PLTR_MN_S = src/menus/plot/treat/interpol.c src/menus/plot/treat/matrice.c \
 src/menus/plot/treat/p_treat_basic.c src/menus/plot/treat/p_treat_math.c \
 src/menus/plot/treat/p_treat_fftw.c src/menus/plot/treat/p_treat_p2im.c

IM_MN_O = obj/i_file_mn.o obj/i_edit_mn.o obj/im_mn.o obj/i_option_mn.o obj/i_treat_mn.o 
IM_MN_H = include/xvin/im_mn.h 
IM_MN_S = src/menus/image/i_file_mn.c src/menus/image/i_edit_mn.c\
 src/menus/image/im_mn.c src/menus/image/i_option_mn.c \
 src/menus/image/i_treat_mn.c 

IMTR_MN_O = obj/i_treat_im2plot.o obj/i_treat_filter.o \
 obj/i_treat_basic.o obj/i_treat_im2im.o \
 obj/i_treat_fftw.o obj/i_treat_measures.o
IMTR_MN_H = src/menus/image/treat/i_treat_im2plot.h src/menus/image/treat/i_treat_filter.h \
 src/menus/image/treat/i_treat_basic.h src/menus/image/treat/i_treat_im2im.h \
 src/menus/image/treat/i_treat_fftw.h src/menus/image/treat/i_treat_measures.h
IMTR_MN_S = src/menus/image/treat/i_treat_im2plot.c src/menus/image/treat/i_treat_filter.c \
 src/menus/image/treat/i_treat_basic.c src/menus/image/treat/i_treat_im2im.c \
 src/menus/image/treat/i_treat_fftw.c src/menus/image/treat/i_treat_measures.c

HELP_MN_O = obj/help_mn.o
HELP_MN_H = include/xvin/help_mn.h
HELP_MN_S = src/menus/help_mn.c

ifeq ($(MEMCHECK),FORTIFY)
FORTIO = obj/fortify.o obj/fortify_mn.o
FORTIH = include/fortify.h include/fortify_mn.h
FORTIS = src/fortify.c src/menus/fortify_mn.c
endif

OBJS  = $(BOX_O) $(PLOT_O) $(UTILO) $(GUI_O) $(FFTO) $(DEV_O) $(GRO) $(XVINO) $(PRO) 
OBJS += $(PL_MN_O) $(PLTR_MN_O) $(OI_O) $(IM_MN_O) $(IMTR_MN_O) $(HELP_MN_O)


ifeq ($(MEMCHECK),FORTIFY)
OBJS += $(FORTIO)
endif 

ifeq ($(LINKING),DYNAMIC)
OBJS += obj/xv_plugins.o
endif

HEADER  = $(DEV_H) $(BOX_H) $(PLOT_H) $(UTILH) $(GUI_H) $(FFTH) $(GRH) $(XVINH) $(PRH) 
HEADER += $(PL_MN_H) $(PLTR_MN_H) $(OI_H) $(IM_MN_H) $(IMTR_MN_H) $(FORTIH) $(HELP_MN_H)
SRC     = $(DEV_S) $(BOX_S) $(PLOT_S) $(UTILS) $(GUI_S) $(FFTS) $(GRS) $(XVINS) $(PRS) 
SRC    += $(PL_MN_S) $(PLTR_MN_S) $(OI_S) $(IM_MN_S) $(IMTR_MN_S) $(FORTIS) $(HELP_MN_S)

MAKEFILES = makefile platform_template.mk platform.mk \
            linux.mk win32.mk macos.mk defines.mk \
            plugin.mk plugin-list-ens-lps.mk plugin-list-ens-lyon.mk


#################################################
# below are allfont definitions:                #
#################################################
ALF_DIR = plug-ins-src/allfont/
#ALF_OUT = $(LIB_PREFIX)alfont.$(LIB_EXT)  # name of the allegrofont library (name is platform dependant)
ALF_STUFF = $(ALF_OUT) include/alfont.h include/alfontdll.h
#ALF_S   = $(addprefix $(ALF_DIR),"Makefile alfont.c alfont.h alfontdll.h")
ALF_S   = $(ALF_DIR)Makefile $(ALF_DIR)alfont.c $(ALF_DIR)alfont.h $(ALF_DIR)alfontdll.h
ALF_FLAGS = -DALFONT_DLL -DALFONT_DLL_EXPORTS $(FREETYPE_INC)

ifeq ($(LINKING),DYNAMIC)
ALFONT_DLL=1
ALFONT_DLL_EXPORTS=1
ALF_FLAGS += $(LINKING_OPTION)
endif


#################################################
# below are fftw3 definitions:                  #
#################################################
FFTWFLAGS  = -lfftw3
# common to all systems/platforms
ifeq ($(SYSTEM),WIN32)
FFTW3_STUFF = bin/fftw3.dll lib/libfftw3.a include/fftw3.h
# usefull on Win32 only
endif
FFTW3_DIR = plug-ins-src/fftw-3.1.2/
# location of the win32 dll
FFTW3_S = $(FFTW3_DIR)fftw3.dll $(FFTW3_DIR)fftw3.h $(FFTW3_DIR)fftw3.def $(FFTW3_DIR)makefile


# above are the definitions
##################################################################################################################
##################################################################################################################
##################################################################################################################
# below are the rules


all : platform.mk alfont fftw3 $(XVIN)
	@echo for $(SYSTEM)/$(PLATFORM), the $(LINKING) version has been build.

.PHONY : all installeur zip plugins plgclean plsrczip doc clean help \
       alfont fftw3 include/generated/gitinfo.h include/generated/gitinfo.hh

platform.mk : platform_template.mk
	@-if [ ! -e platform.mk ]; then cp platform_template.mk platform.mk; \
	echo A new platform.mk has been generated from the template. \
	please edit this file before running make again. ; \
	else touch platform.mk; \
    echo Your platform.mk was kept, but it may be older than the one from the SVN. \
    You should check it. ; fi;
	exit 1


#################################################
# below are fftw3 rules:                        #
#################################################
fftw3 : $(FFTW3_STUFF)

bin/fftw3.dll : $(FFTW3_DIR)fftw3.dll
	cp -uvp $(FFTW3_DIR)fftw3.dll bin/fftw3.dll

include/fftw3.h : $(FFTW3_DIR)fftw3.h
	cp -uvp $(FFTW3_DIR)fftw3.h include/fftw3.h

lib/libfftw3.a : $(FFTW3_DIR)fftw3.dll $(FFTW3_DIR)fftw3.def
	cd $(FFTW3_DIR); dlltool --def fftw3.def -k --dllname fftw3.dll \
 --output-lib ../../lib/libfftw3.a; cd ../..

bin/libfftw3.so :                # note : this is unused, but just in case...
	ln -s bin/libfftw3.so $(word 1,$(GNU_LIBS_DIR))/lib/libfftw3.so

bin/libfftw3.dylib :             # note : this is unused, but just in case...
	ln -s bin/libfftw3.dylib $(word 1,$(GNU_LIBS_DIR))/lib/libfftw3.dylib

#################################################
# below are alfont rules:                       #
#################################################
alfont : $(ALF_STUFF)

$(ALF_OUT) :
	cd $(ALF_DIR); make global_all;

include/alfont.h : $(ALF_DIR)/alfont.h
	cp -vp $(ALF_DIR)/alfont.h include/alfont.h

include/alfontdll.h : $(ALF_DIR)/alfontdll.h
	cp -vp $(ALF_DIR)/alfontdll.h include/alfontdll.h



##############################################
# plug'ins stuff:                            #
##############################################

# the rules about plugins:
plugins : $(XVIN)
	$(foreach toto,$(PLUGINS),cd plug-ins-src/$(toto); make global_all; cd ../..;)

plsrczip : 
	$(foreach toto,$(PLUGINS),cd plug-ins-src/$(toto); make global_zip; cd ../..;) 

plgclean : 
	$(foreach toto,$(PLUGINS),cd plug-ins-src/$(toto); make global_clean; cd ../..; ) 



#################################################################
# below are the rules to produce xvin objects:                  #
#################################################################

obj/fillib.o	: src/fillib.c include/xvin/fillib.h include/xvin/util.h
	$(CC) $(CFLAGS) -o obj/fillib.o -c src/fillib.c 

obj/fftl32n.o	: src/fftl32n.c include/xvin/fftl32n.h 
	$(CC) $(CFLAGS) -o obj/fftl32n.o -c src/fftl32n.c   

$(DEV_O) : $(DEV_S) $(DEV_H)	
	$(CC) $(CFLAGS) src/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(BOX_O) : $(BOX_S) $(BOX_H)	
	$(CC) $(CFLAGS) src/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(PLOT_O) : $(PLOT_S) $(PLOT_H)	
	$(CC) $(CFLAGS) src/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(GUI_O) : $(GUI_S) $(GUI_H) include/xvin/im_reg.h
	$(CC) $(CFLAGS) src/gui/$(notdir $(patsubst %.o,%.c,$@)) -o $@

obj/xvinerr.o	: include/xvin/xvinerr.h src/xvinerr.c
	$(CC) $(CFLAGS) -o obj/xvinerr.o src/xvinerr.c

obj/util.o	: include/xvin/util.h src/util.c
	$(CC) $(CFLAGS) -o obj/util.o src/util.c

obj/log_file.o	: include/xvin/util.h include/xvin/log_file.h src/log_file.c
	$(CC) $(CFLAGS) -o obj/log_file.o src/log_file.c

obj/gr_file.o	: include/xvin/gr_file.h src/gr_file.c
	$(CC) $(CFLAGS) -o obj/gr_file.o src/gr_file.c

obj/xvinu.o	: include/xvin.h src/xvinu.c
	$(CC) $(CFLAGS) -o obj/xvinu.o src/xvinu.c

obj/plot_reg.o : include/xvin/plot_reg.h src/plot_reg.c
	$(CC) $(CFLAGS) -o obj/plot_reg.o src/plot_reg.c

obj/plot_proj.o : include/xvin/plot_reg.h include/xvin/plot_ds.h include/xvin/plot_op.h\
 src/plot_proj.c
	$(CC) $(CFLAGS) -o obj/plot_proj.o src/plot_proj.c

obj/plot_reg_gui.o : include/xvin/plot_reg.h src/gui/plot_reg_gui.c include/xvin/plot_reg_gui.h
	$(CC) $(CFLAGS) -o obj/plot_reg_gui.o src/gui/plot_reg_gui.c

obj/gui_def.o : include/xvin/gui_def.h src/gui/gui_def.c
	$(CC) $(CFLAGS) -o obj/gui_def.o src/gui/gui_def.c      	

obj/win_file.o : include/xvin/util.h src/gui/win_file.c
	$(CC) $(CFLAGS) -o obj/win_file.o src/gui/win_file.c      	

obj/win_gui.o : src/gui/win_gui.c include/xvin.h
	$(CC) $(CFLAGS) -o obj/win_gui.o src/gui/win_gui.c     	

$(PL_MN_O) : $(PL_MN_S) $(PL_MN_H)	
	$(CC) $(CFLAGS) src/menus/plot/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(PLTR_MN_O) : $(PLTR_MN_S) $(PL_MN_H)	
	$(CC) $(CFLAGS) src/menus/plot/treat/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(IM_MN_O) : $(IM_MN_S) $(IM_MN_H)	
	$(CC) $(CFLAGS) src/menus/image/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(IMTR_MN_O) : $(IMTR_MN_S) $(IMTR_MN_H)	
	$(CC) $(CFLAGS) src/menus/image/treat/$(notdir $(patsubst %.o,%.c,$@)) -o $@

obj/project.o : src/menus/project.c
	$(CC) $(CFLAGS) -o obj/project.o src/menus/project.c

obj/help_mn.o : src/menus/help_mn.c include/generated/gitinfo.h  include/generated/gitinfo.hh
	$(CC) $(CFLAGS) -Isrc -Iinclude/generated -o $@ src/menus/help_mn.c

obj/xv_main.o : src/xv_main.c include/xvin/xv_main.h
	$(CC) $(CFLAGS) -o obj/xv_main.o src/xv_main.c

$(OI_O) : $(OI_S) $(OI_H)
	$(CC) $(CFLAGS) src/$(notdir $(patsubst %.o,%.c,$@)) -o $@

obj/fftw3_lib.o: src/lib/fftw3_lib.c include/xvin/fftw3_lib.h
	$(CC) $(CFLAGS) -o $@ src/lib/fftw3_lib.c

obj/fft_filter_lib.o: src/lib/fft_filter_lib.c include/xvin/fft_filter_lib.h
	$(CC) $(CFLAGS) -o $@ src/lib/fft_filter_lib.c

obj/xv_tools_lib.o: src/lib/xv_tools_lib.c include/xvin/xv_tools_lib.h
	$(CC) $(CFLAGS) -o $@ src/lib/xv_tools_lib.c

obj/fortify.o : src/fortify.c include/fortify.h
	$(CC) $(CFLAGS) -DFORTIFY -o $@ src/fortify.c

obj/xv_plugins.o : src/xv_plugins.c include/xvin/xv_plugins.h
	$(CC) $(CFLAGS) -o $@ src/xv_plugins.c

obj/global_vars.o : src/global_vars.c
	$(CC) $(CFLAGS) -o $@ src/global_vars.c

obj/form_builder.o : src/gui/gtk_helper/form_builder.c include/xvin/form_builder.h
	$(CC) $(CFLAGS) -o $@ $<
    
obj/progress_builder.o : src/gui/gtk_helper/progress_builder.c include/xvin/progress_builder.h
	$(CC) $(CFLAGS) -o $@ $<

    
src/svn.h: src/svn.in
	@echo "#define SVN_REVISION \"$(svn info | grep 'Last Changed Rev: '  | sed 's/Last Changed Rev: //g')\"" > src/svn.h
	@echo "#define SVN_DATE \"$(svn info | grep 'Last Changed Date: '  | sed 's/Last Changed Date: //g')\"" >> src/svn.h
	@echo "#define SVN_MODIFICATION \"$(svn info | grep 'Last Changed Author: '  | sed 's/Last Changed Author: //g')\"" >> src/svn.h





##############################################
# icon  (same code for all win32 versions)	 #
##############################################

ifeq ($(SYSTEM),WIN32)
ICON = obj/icon.o
obj/icon.o: icons/gr.ico icons/xvin.rc
	windres -I rc -O coff -i icons/xvin.rc -o obj/icon.o
endif	

ifeq ($(SYSTEM),LINUX)
ICON = # obj/icon.o

# following 2 lines are required if and only if file X11_icon.c needs to be rebuilt
#icons/X11_icon.c: icons/gr.ico
#	icons/xfixicon.sh icons/gr.ico -o icons/X11_icon.c
obj/icon.o: icons/X11_icon.c 
	$(CC) $(CFLAGS) -o $@ icons/X11_icon.c # -DALLEGRO_WITH_XWINDOWS -DALLEGRO_USE_CONTRUCTOR 
endif



##################################################################
# xvin executable, win32 systems       					         #
##################################################################
ifeq ($(SYSTEM),WIN32)

bin/$(NEEDED_DLL):
	cp -uvp /mingw32/bin/$@ bin/$@;




ifeq ($(LINKING),STATIC)
bin/xvin.exe : $(OBJS) $(ICON) makefile include/xvin.h src/main.c bin/fftw3.dll bin/$(NEEDED_DLL)
	$(CC) $(DEBUG_FLAGS) $(WARNING_FLAGS) -mwindows $(ICON) $(CPU_FLAGS) -D$(XV_TARGET) -DXVIN_STATICLINK  $(INC_FLAGS) $(PLATFORM_FLAGS) \
$(LINKING_OPTION) $(MEMCHECK_FLAGS)  \
 -I./include -I./include/xvin $(OBJS)  -o $@ src/main.c  -L./lib -L/usr/local/lib -lalleg44 -lfftw3 -lalfont -lkernel32 -luser32 -lgdi32 -lcomdlg32 -lole32 -ldinput -lddraw -ldxguid -lwinmm -ldsound $(GTK_LIB) ; mkdir bin/share; mkdir bin/share/glib-2.0; mkdir bin/share/icons; mkdir bin/share/glib-2.0/schemas;cp -uvp /mingw32/share/glib-2.0/schemas/* bin/share/glib-2.0/schemas; cp -Ruvp /mingw32/share/icons bin/share/ 
# -lalfontdll 
endif



ifeq ($(LINKING),DYNAMIC)
ifeq ($(PLATFORM),MINGW)
bin/xvin.exe :  $(FFTW3_STUFF) bin/alfont.dll bin/xvin.dll lib/libxvin.a $(ICON) makefile include/xvin.h src/main.c bin/$(NEEDED_DLL)
	$(CC) $(DEBUG_FLAGS) $(WARNING_FLAGS) -mwindows $(PLATFORM_FLAGS) $(ICON) $(CPU_FLAGS) -D$(XV_TARGET)  \
 -I./include -I./include/xvin -o bin/xvin.exe src/main.c -L./lib $(INC_FLAGS) -L/usr/local/lib -lalleg44.dll  -lxvin  -luser32 -lgdi32 -lcomdlg32 -lole32 -ldinput -lddraw -ldxguid -lwinmm -ldsound $(GTK_LIB)
 endif
#-fsanitize=address removed -lkernel32 

ifeq ($(PLATFORM),CLANG)
bin/xvin.exe :  $(FFTW3_STUFF) bin/alfont.dll bin/xvin.dll lib/libxvin.a $(ICON) makefile include/xvin.h src/main.c bin/$(NEEDED_DLL)
	$(CC) $(DEBUG_FLAGS) $(WARNING_FLAGS) -mwindows $(PLATFORM_FLAGS) $(ICON) $(CPU_FLAGS) -D$(XV_TARGET)  -DXV_GTK_DIALOG \
 -I./include -I./include/xvin -o bin/xvin.exe src/main.c -L./lib    $(INC_FLAGS) -L/usr/local/lib -lalleg44.dll -lxvin -lkernel32 -luser32 -lgdi32 -lcomdlg32 -lole32 -ldinput -lddraw -ldxguid -lwinmm -ldsound $(GTK_LIB)
 endif

ifeq ($(PLATFORM),CYGWIN)
bin/xvin.exe : bin/xvin.dll lib/libxvin.a $(ICON) makefile include/xvin.h src/main.c bin/fftw3.dll bin/$(NEEDED_DLL)
	$(CC) $(DEBUG_FLAGS) $(WARNING_FLAGS) -mwindows $(PLATFORM_FLAGS) $(ICON) $(CPU_FLAGS) -D$(XV_TARGET) $(INC_FLAGS)  \
 -I./include -I./include/xvin -o bin/xvin.exe src/main.c -L./lib -lalleg44 -lxvin -ldl 
endif
endif

endif # end of the SYSTEM=WIN32 case



##################################################################
# xvin executable, linux systems                                 #
##################################################################
ifeq ($(SYSTEM),LINUX)
ifeq ($(LINKING),STATIC)
bin/xvin : lib/libxvin.a  makefile include/xvin.h $(ICON) src/main.c
	$(CC) -o main.o $(CFLAGS) -DXVIN_STATICLINK -DXV_GTK_DIALOG src/main.c  
	$(CC) main.o $(ICON) $(LD_FLAGS) -o $@ -L./lib -lxvin \
	plug-ins-src/allfont/alfont.o $(ALLEGRO_LIB) $(FREETYPE_LIB) $(GTK_LIB)    $(FFTWFLAGS) -lm -fPIC   
endif # end of the case LINKING=STATIC

ifeq ($(LINKING),DYNAMIC)
bin/xvin : bin/libxvin.so include/xvin.h src/main.c $(ICON) makefile
	$(CC) -c $(DEBUG_FLAGS) $(CPU_FLAGS) $(INC_FLAGS) $(PLATFORM_FLAGS) -DXV_GTK_DIALOG \
               $(LINKING_OPTION) $(MEMCHECK_FLAGS) src/main.c -o $(XVIN_DIR)obj/main.o; \
        cd bin/; \
	$(CC) $(DEBUG_FLAGS) -DXV_GTK_DIALOG $(LD_FLAGS) -o xvin ../obj/main.o \
	$(LIB_OPTS) $(FFTWFLAGS) $(FREETYPE_LIB) $(ALLEGRO_LIB) \
	-Wl,-rpath,$(HOME)/xvin/bin  -L.  -lxvin -lalfont -lm -fPIC 

endif # end of the case LINKING=DYNAMIC
endif # end of the case SYSTEM=LINUX
#$(CFLAGS) 


##################################################################
# xvin executable, MacOSX systems                                #
##################################################################
ifeq ($(SYSTEM),MACOS)
ifeq ($(LINKING),STATIC)
bin/xvin : lib/libxvin.a  makefile include/xvin.h src/main.c 
	$(CC) -c $(DEBUG_FLAGS) $(WARNING_FLAGS) $(CPU_FLAGS) -D$(XV_TARGET) -I./include -o main.o -I./include/xvin src/main.c 
	$(CC) -s -L/usr/X11R6/lib -Wl,-dynamic  -o bin/xvin main.o -framework cocoa\
	-L./lib $(ALLEGRO_LIB) -lxvin -lfreetype -lz -lalfont -lfftw3 -lm; \
	$(ALLEGRO_BIN)/fixbundle $@ icons/gr.bmp
endif # end of the case LINKING=STATIC

ifeq ($(LINKING),DYNAMIC)
bin/xvin : bin/libxvin.dylib include/xvin.h src/main.c makefile
	$(CC) $(CFLAGS) src/main.c -o main.o # -I./include -I./include/xvin
	cd bin/; \
    $(CC) $(LD_FLAGS) -o xvin ../main.o libxvin.dylib libalfont.dylib \
	$(LIB_OPTS) $(FFTWFLAGS) \
	$(FREETYPE_LIB) $(ALLEGRO_LIB) \
	--rpath=$(HOME)/xvin/bin;	\
#	-Wl,-rpath,$(HOME)/xvin/bin	
	$(ALLEGRO_BIN)/fixbundle $@ icons/gr.bmp
endif # end of the case LINKING=DYNAMIC
endif # end of the case SYSTEM=MACOS 



#################################################################
# libxvin.a library (same code for all platforms)               #
#################################################################
lib/libxvin.a : $(OBJS) $(MAKEFILES)
	ar $(AR_FLAGS) lib/libxvin.a $(OBJS) 



#################################################################
# xvin.dll (same code for all win32 platforms)                  #
#################################################################
bin/xvin.dll : $(OBJS) $(MAKEFILES)
	$(CC) -o $@ $(OBJS) $(DEBUG_FLAGS) $(WARNING_FLAGS) $(PLATFORM_FLAGS) $(CPU_FLAGS) $(DLL_FLAGS) \
	-L./lib $(addprefix -L,$(addsuffix /lib/,$(GNU_LIBS_DIR))) \
    $(FFTWFLAGS) -lalfont $(ALLEGRO_LIB)  $(GTK_LIB)  -ldl    -Wl,--export-all-symbols \
    -Wl,--enable-auto-import -Wl,--out-implib=lib/libxvin.a 

#-fsanitize=address c:\msys64\mingw32\lib\clang\3.5.1\lib\windows\clang_rt.asan_dynamic-i386.lib

#################################################################
# xvin.so (same code for all linux platforms)                   #
#################################################################
bin/libxvin.so : $(OBJS) $(MAKEFILES)
	$(CC) $(DEBUG_FLAGS) $(WARNING_FLAGS) $(CPU_FLAGS) -o $@ $(OBJS) -shared $(LIB_OPTS)  \
	$(LD_FLAGS) $(FFTWFLAGS) $(FREETYPE_LIB) $(ALLEGRO_LIB) $(GTK_LIB)
# \
#-Wl,-export-dynamic 


#################################################################
# xvin.dylib (Mac OS X platform)                                #
#################################################################
bin/libxvin.dylib : $(OBJS) $(MAKEFILES) 
	$(CC) -o $@ $(OBJS) -dynamiclib $(LIB_OPTS) -L$(XVIN_DIR)bin \
	$(LD_FLAGS) $(FFTWFLAGS) $(FREETYPE_LIB) $(ALLEGRO_LIB) \
	-lalfont -Wl,-dynamic


#################################################################
# below are the rules to produce the documentation    			#
#################################################################

DOCS = docs/html/xvin.html docs/rtf/xvin.rtf docs/txt/xvin.txt docs/txi/xvin.txi docs/chm/xvin.chm docs/chm/xvin.html

doc : $(DOCS)

docs/html/xvin.html : docs/src/xvin._tx
	bin/makedoc.exe -html docs/html/xvin.html docs/src/xvin._tx

docs/rtf/xvin.rtf : docs/src/xvin._tx
	bin/makedoc.exe -rtf docs/rtf/xvin.rtf docs/src/xvin._tx

docs/txt/xvin.txt : docs/src/xvin._tx
	bin/makedoc.exe -ascii  docs/txt/xvin.txt docs/src/xvin._tx	

docs/txi/xvin.txi : docs/src/xvin._tx
	bin/makedoc.exe -txi docs/txi/xvin.txi docs/src/xvin._tx

docs/chm/xvin.html : docs/src/xvin._tx
	bin/makedoc.exe -ochm -html docs/chm/xvin.html docs/src/xvin._tx 

docs/chm/xvin.chm : docs/src/xvin._tx docs/chm/xvin.html
	bin/makedoc.exe -chm docs/chm/xvin.html docs/src/xvin._tx; \
	cd docs/chm; /c/'Program Files/HTML Help Workshop'/hhc.exe xvin.hhp; cd ../..







#################################################
# cleaning (common to all platforms)			#
#################################################
clean : 
	rm -f $(OBJS) 
	rm -f lib/libxvin.a
	rm -f bin/xvin.exe
	rm -f bin/xvin.dll bin/xvin.so bin/xvin.dylib
	rm -f bin/xvin
	rm -f $(ALF_STUFF) $(ALF_DIR)*.o
	rm -f $(FFTW3_STUFF)





##############################################
# installeur								 #
##############################################
installeur : installer/Xvin_script_modern.nsi
	$(NSIS_BIN) installer/XVin_script_modern.nsi
	cp -uvp installer/XVin_setup.exe .




#################################################
# archives										#
#################################################

zip	: $(HEADER) $(SRC) makefile src/main.c # docs/src/xvin._tx 
	$(ZIPEXE) xvinsrc$(ZIP_EXT) $(HEADER) $(SRC) \
  $(MAKEFILES) src/main.c \
 icons/gr.ico icons/gr.bmp icons/X11_icon.c icons/xfixicon.sh icons/xvin.rc \
 obj/remove.me lib/remove.me bin/fonts.dat \
 bin/allegro.cfg bin/arial.ttf  bin/keyboard.dat bin/language.dat \
 bin/keyconf.txt  \
 $(FFTW3_S) $(ALF_S) \
 docs/src/xvin._tx docs/html/*.* docs/rtf/*.* docs/txi/*.* docs/txt/*.* docs/chm/*.* \
# include/plug-ins/fftw3.h


#################################################
# help                                          #
#################################################
help :
	@echo "Makefile for XVin. possible options are :"
	@echo "[]/[all]   build XVin"
	@echo "[help]     print this help message"
	@echo "[clean]    clean the XVin objects and binaries"
	@echo "[zip]      zip the XVin sources"
	@echo "[doc]      build the XVin docs"
	@echo " "
	@echo "[plugins]  build all the plugins from the plugins list"
	@echo "[plsrczip] scan all plugins and archive all the sources"
	@echo "[plgclean] scan all the plugins and clean them"
	@echo " "
	@echo "the plugins list is (from the main makefile):"
	@echo $(PLUGINS);

include common.mk
