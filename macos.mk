#################################################
# makefile rules for MacOS X					#
#################################################

XV_TARGET    = XV_MAC 	# the define to be passed to all XVin source files:

# below we add a MacOS specific option:
CPU_FLAGS    += -fno-common


# the following will be used outside:
AR_FLAGS     = rcs
LD_FLAGS     = # -s -L/usr/X11R6/lib # -Wl,--export-dynamic 


# library type (static or dynamic):
ifeq ($(LINKING),DYNAMIC)
	LINKING_OPTION = -DBUILDING_DLL # -dynamic
    PLUGIN_LINKING_OPTION = # -dynamic
	# extension for filenames of dynamic libraries:
	LIB_EXT    =dylib
	LIB_PREFIX =bin/lib
else
ifeq ($(LINKING),STATIC)
	LINKING_OPTION = -DXVIN_STATICLINK
	# extension for filenames of dynamic libraries:
	LIB_EXT    =a
	LIB_PREFIX =lib/lib
else 
	@echo "Library type (static or dynamic) not defined... aborting."
	return 1
endif
endif



# command line zip archiver, and archive file extension:
ZIPEXE = zip 
ZIP_EXT = .zip



