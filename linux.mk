#################################################
# makefile rules for Linux                      #
#################################################

XV_TARGET    = XV_UNIX  # the define to be passed to all xvin source files:


# the following will be used outside:
AR_FLAGS     = rc
#-Wl,--export-dynamic -s

# library type (static or dynamic):
ifeq ($(LINKING),DYNAMIC)
	LINKING_OPTION = -DBUILDING_DLL -shared
	PLUGIN_LINKING_OPTION =
	LD_FLAGS     = -L/usr/X11R6/lib -Wl,-Bstatic -Wl,-Bdynamic
    # extension for filenames of dynamic libraries:
	LIB_EXT    =so
	LIB_PREFIX =bin/lib
else
ifeq ($(LINKING),STATIC)
	LINKING_OPTION = -DXVIN_STATICLINK
	LD_FLAGS     = -g -ggdb -L/usr/X11R6/lib  
    # extension for filenames of dynamic libraries:
	LIB_EXT    =a
	LIB_PREFIX =lib/lib
else 
	@echo "Library type (static or dynamic) not defined... aborting."
	return 1
endif
endif


# command line zip archiver, and archive file extension:
ZIPEXE = tar -czvf 
ZIP_EXT = .tgz
