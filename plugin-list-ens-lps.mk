# this is the list of plug'ins that are used in Paris
#
# the present file (ens-lps.mk) only defines the variable $(PLUGINS) to be
# used in the main XVin makefile to compile/zip/clean all plug'ins at the same time

PLUGINS = xvplot nrutil deconv random cardinal fft2d stat fjca inout hist lequalz skew ssdna \
	stores talbot testim trmov xvjpeg xvtiff wlc ohy lotus tilted spheric calpiezzo \
	diffusion igor frommovie evolive select thorlabs_apt_api
