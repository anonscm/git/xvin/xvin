##################################################################
# main makefile for picocvb                                      #
#                                                                #
# this file should NOT be modified                               #
# edit "platform_pico.mk" to select your system and choose target#
#                                                                #
# note: all XXX.mk should only contain definitions, no rules!    #
##################################################################
XVIN_MAIN_MAKEFILE = yes

# -DJF_DEBUG
include platform_pico.mk
LSCFLAGS =  -DGIGE   -DUEYE -D$(UEYE_SDK) -DZMAG_APT  -DZMAG_FACTOR -DDUMMY_FOCUS
PROG_NAME = picoueye-Hua

#################################################
# below are definitions of lists of files:      #
#################################################
DEV_O = objp/dev_pcx.o objp/dev_ps.o objp/color.o
DEV_H = include/xvin/dev_def.h include/xvin/color.h include/xvin/plot_opt.h
DEV_S = src/dev_pcx.c src/color.c src/dev_ps.c

BOX_O = objp/box_def.o objp/box_eqs.o objp/box2dev.o objp/box2alfn.o
BOX_H = include/xvin/box_def.h include/xvin/box2alfn.h include/xvin/fops2.h
BOX_S = src/box_def.c src/box_eqs.c src/box2dev.c src/box2alfn.c

PLOT_O = objp/unitset.o objp/plot_ds.o objp/plot_op.o objp/plot_gr.o objp/plot2box.o objp/op2box.o
PLOT_H = include/xvin/unitset.h include/xvin/plot_ds.h include/xvin/plot_op.h\
		 include/xvin/plot2box.h include/xvin/plot_gr.h include/xvin/op2box.h
PLOT_S = src/unitset.c src/plot_ds.c src/plot_op.c src/plot2box.c src/op2box.c src/plot_gr.c

GUI_O = objp/unit_gui.o objp/imr_reg_gui.o obj/file_picker_gui.o
GUI_H = include/xvin/imr_reg_gui.h include/xvin/file_picker_gui.h
GUI_S = src/gui/unit_gui.c src/gui/imr_reg_gui.c src/gui/file_picker_gui.c

OI_O = objp/im_oi.o objp/im_reg.o objp/im_gr.o
OI_H = include/xvin/im_oi.h include/xvin/im_reg.h include/xvin/im_gr.h
OI_S = src/im_oi.c src/im_reg.c src/im_gr.c

PRO = objp/plot_reg.o objp/plot_reg_gui.o objp/project.o objp/plot_proj.o
PRH = include/xvin/plot_reg.h include/xvin/plot_reg_gui.h
PRS = src/plot_reg.c src/gui/plot_reg_gui.c src/menus/project.c src/plot_proj.c

UTILO = objp/xvinerr.o objp/util.o objp/gui_def.o objp/win_gui.o objp/xv_pico.o \
 objp/log_file.o objp/win_file.o objp/xv_tools_lib.o objp/global_vars.o
UTILH = include/xvin/xvinerr.h include/xvin/util.h include/xvin/gui_def.h\
 include/xvin/fonts.h include/xvin/platform.h include/xvin/log_file.h include/xvin/xv_tools_lib.h include/xvin/xv_plugins.h
UTILS = src/xvinerr.c src/util.c src/gui/gui_def.c src/gui/win_gui.c \
 src/xv_pico.c src/log_file.c src/gui/win_file.c src/lib/xv_tools_lib.c src/xv_plugins.c src/global_vars.c

FFTO = objp/fillib.o objp/fftl32n.o objp/fft_filter_lib.o objp/fftw3_lib.o
FFTH = include/xvin/fillib.h include/xvin/fftl32n.h include/xvin/fft_filter_lib.h \
 include/xvin/fftw3_lib.h
FFTS = src/fillib.c src/fftl32n.c src/lib/fft_filter_lib.c src/lib/fftw3_lib.c

GRO = objp/gr_file.o
GRH = include/xvin/gr_file.h
GRS = src/gr_file.c

XVINO = objp/xvinu.o
XVINH = include/xvin.h
XVINS = src/xvinu.c

PL_MN_O = objp/p_file_mn.o objp/p_edit_mn.o objp/p_disp_mn.o objp/p_tools_mn.o \
 objp/p_treat_mn.o objp/plot_mn.o objp/p_options_mn.o
PL_MN_H = include/xvin/plot_mn.h
#\   src/menus/plot/treat/p_treat_math.h \
 #include/xvin/matrice.h src/menus/plot/treat/p_treat_basic.h src/menus/plot/treat/p_treat_fftw.h
PL_MN_S = src/menus/plot/p_file_mn.c src/menus/plot/p_edit_mn.c\
 src/menus/plot/p_disp_mn.c src/menus/plot/p_tools_mn.c src/menus/plot/plot_mn.c\
 src/menus/plot/p_treat_mn.c src/menus/plot/p_options_mn.c

PLTR_MN_O = objp/interpol.o objp/matrice.o \
 objp/p_treat_basic.o objp/p_treat_math.o objp/p_treat_fftw.o objp/p_treat_p2im.o
PLTR_MN_H = src/menus/plot/treat/p_treat_basic.h src/menus/plot/treat/p_treat_math.h \
 src/menus/plot/treat/p_treat_fftw.h src/menus/plot/treat/p_treat_p2im.h include/xvin/matrice.h
PLTR_MN_S = src/menus/plot/treat/interpol.c src/menus/plot/treat/matrice.c \
 src/menus/plot/treat/p_treat_basic.c src/menus/plot/treat/p_treat_math.c \
 src/menus/plot/treat/p_treat_fftw.c src/menus/plot/treat/p_treat_p2im.c

IM_MN_O = objp/i_file_mn.o objp/i_edit_mn.o objp/im_mn.o objp/i_option_mn.o objp/i_treat_mn.o
IM_MN_H = include/xvin/im_mn.h
IM_MN_S = src/menus/image/i_file_mn.c src/menus/image/i_edit_mn.c\
 src/menus/image/im_mn.c src/menus/image/i_option_mn.c \
 src/menus/image/i_treat_mn.c

IMTR_MN_O = objp/i_treat_im2plot.o objp/i_treat_filter.o \
 objp/i_treat_basic.o objp/i_treat_im2im.o \
 objp/i_treat_fftw.o objp/i_treat_measures.o
IMTR_MN_H = src/menus/image/treat/i_treat_im2plot.h src/menus/image/treat/i_treat_filter.h \
 src/menus/image/treat/i_treat_basic.h src/menus/image/treat/i_treat_im2im.h \
 src/menus/image/treat/i_treat_fftw.h src/menus/image/treat/i_treat_measures.h
IMTR_MN_S = src/menus/image/treat/i_treat_im2plot.c src/menus/image/treat/i_treat_filter.c \
 src/menus/image/treat/i_treat_basic.c src/menus/image/treat/i_treat_im2im.c \
 src/menus/image/treat/i_treat_fftw.c src/menus/image/treat/i_treat_measures.c



ifeq ($(MEMCHECK),FORTIFY)
FORTIO = objp/fortify.o
FORTIH = include/fortify.h
FORTIS = src/fortify.c
endif


HELP_MN_O = obj/help_mn.o
HELP_MN_H = include/xvin/help_mn.h
HELP_MN_S = src/menus/help_mn.c


OBJS  = $(BOX_O) $(PLOT_O) $(UTILO) $(GUI_O) $(FFTO) $(DEV_O) $(GRO) $(XVINO) $(PRO)
OBJS += $(PL_MN_O) $(PLTR_MN_O) $(OI_O) $(IM_MN_O) $(IMTR_MN_O) $(HELP_MN_O)

ifeq ($(MEMCHECK),FORTIFY)
OBJS += $(FORTIO)
endif

ifeq ($(THREADING),USE_OPENMP)
CFLAGS += -DUSE_OPENMP -fopenmp
LFLAGS = -lgomp
endif


#ifeq ($(LINKING),DYNAMIC)
OBJS += objp/xv_plugins.o
#endif

HEADER  = $(DEV_H) $(BOX_H) $(PLOT_H) $(UTILH) $(GUI_H) $(FFTH) $(GRH) $(XVINH) $(PRH)
HEADER += $(PL_MN_H) $(PLTR_MN_H) $(OI_H) $(IM_MN_H) $(IMTR_MN_H) $(FORTIH)

SRC     = $(DEV_S) $(BOX_S) $(PLOT_S) $(UTILS) $(GUI_S) $(FFTS) $(GRS) $(XVINS) $(PRS)
SRC    += $(PL_MN_S) $(PLTR_MN_S) $(OI_S) $(IM_MN_S) $(IMTR_MN_S) $(FORTIS)

MAKEFILES = makefile platform_template.mk platform.mk \
            linux.mk win32.mk macos.mk defines.mk \
            plugin.mk plugin-list-ens-lps.mk plugin-list-ens-lyon.mk


#################################################
# below are allfont definitions:                #
#################################################
ALF_DIR = plug-ins-src/allfont/
ALF_OUT = libp/libalfont.$(LIB_EXT)  # name of the allegrofont library (name is platform dependant)
ALF_STUFF = $(ALF_OUT) include/alfont.h include/alfontdll.h
#ALF_S   = $(addprefix $(ALF_DIR),"Makefile alfont.c alfont.h alfontdll.h")
ALF_S   = $(ALF_DIR)Makefile $(ALF_DIR)alfont.c $(ALF_DIR)alfont.h $(ALF_DIR)alfontdll.h
ALF_FLAGS = -DALFONT_DLL -DALFONT_DLL_EXPORTS $(FREETYPE_INC)

ifeq ($(LINKING),DYNAMIC)
ALFONT_DLL=1
ALFONT_DLL_EXPORTS=1
ALF_FLAGS += $(LINKING_OPTION)
endif


#################################################
# below are fftw3 definitions:                  #
#################################################
FFTWFLAGS  = -lfftw3
# common to all systems/platforms
ifeq ($(SYSTEM),WIN32)
FFTW3_STUFF = bin/fftw3.dll lib/libfftw3.a include/fftw3.h
# usefull on Win32 only
endif
FFTW3_DIR = plug-ins-src/fftw-3.1.2/
# location of the win32 dll
FFTW3_S = $(FFTW3_DIR)fftw3.dll $(FFTW3_DIR)fftw3.h $(FFTW3_DIR)fftw3.def $(FFTW3_DIR)makefile



#################################################
# below are cfg_file definitions:                #
#################################################
CFG_DIR = plug-ins-src/cfg_file/
CFG_O = objp/cfg_file.o objp/microscope.o
CFG_S   = $(CFG_DIR)cfg_file.c $(CFG_DIR)microscope.c
CFG_H   = $(CFG_DIR)Pico_cfg.h $(CFG_DIR)microscope.h

OBJS += $(CFG_O)
SRC += $(CFG_S)
HEADER += $(CFG_H)

NRU_DIR = plug-ins-src/nrutil/

NRU_O = objp/covsrt.o objp/gammln.o objp/gammq.o objp/gaussj.o objp/gcf.o \
objp/gser.o objp/mrqcof.o objp/mrqmin.o objp/svdfit.o objp/svdcmp.o \
objp/svdvar.o objp/svbksb.o objp/pythag.o objp/lfit.o objp/ran1.o objp/nrutil.o

NRU_S = $(NRU_DIR)covsrt.c $(NRU_DIR)gammln.c $(NRU_DIR)gammq.c $(NRU_DIR)gaussj.c \
$(NRU_DIR)gcf.c $(NRU_DIR)gser.c $(NRU_DIR)mrqcof.c $(NRU_DIR)mrqmin.c \
$(NRU_DIR)svdfit.c $(NRU_DIR)svdcmp.c $(NRU_DIR)svdvar.c $(NRU_DIR)svbksb.c \
$(NRU_DIR)pythag.c $(NRU_DIR)lfit.c $(NRU_DIR)ran1.c $(NRU_DIR)nrutil.c

NRU_H = $(NRU_DIR)covsrt.h $(NRU_DIR)gammln.h $(NRU_DIR)gammq.h $(NRU_DIR)gaussj.h \
$(NRU_DIR)gcf.h $(NRU_DIR)gser.h $(NRU_DIR)mrqcof.h $(NRU_DIR)mrqmin.h \
$(NRU_DIR)svdfit.h $(NRU_DIR)svdcmp.h $(NRU_DIR)svdvar.h $(NRU_DIR)svbksb.h \
$(NRU_DIR)pythag.h $(NRU_DIR)lfit.h $(NRU_DIR)ran1.h $(NRU_DIR)nrutil.h

OBJS += $(NRU_O)
SRC += $(NRU_S)
HEADER += $(NRU_H)

OBJS += objp/wlc.o

OBJS += objp/xvplot.o

OBJS += objp/cardinal.o objp/sftl32n.o

OBJS += objp/stat.o

OBJS += objp/cordrift.o

OBJS += objp/fft2d.o objp/fftbtl32n.o

#################################################
# below are trackBead definitions:                #
#################################################
TRK_DIR = plug-ins-src/trackBead/

TRK_O = objp/brown_util.o objp/uEye_source.o  objp/track_util.o objp/focus.o \
objp/magnetscontrol.o  objp/scan_zmag.o objp/action.o objp/calibration.o \
objp/record.o objp/hat.o objp/force.o objp/save_movie_util.o objp/trackBead.o \
objp/zdet.o objp/zdet_topo.o objp/fillibbt.o objp/timer_thread.o objp/gui.o

TRK_S = $(TRK_DIR)brown_util.c $(TRK_DIR)uEye_source.c  $(TRK_DIR)track_util.c \
$(TRK_DIR)focus.c $(TRK_DIR)magnetscontrol.c  $(TRK_DIR)scan_zmag.c $(TRK_DIR)action.c \
$(TRK_DIR)calibration.c $(TRK_DIR)hat.c $(TRK_DIR)force.c $(TRK_DIR)save_movie_util.c \
$(TRK_DIR)record.c $(TRK_DIR)zdet.c $(TRK_DIR)trackBead.c $(TRK_DIR)zdet_topo.c \
$(TRK_DIR)fillibbt.c $(TRK_DIR)timer_thread.c $(TRK_DIR)gui.c

TRK_H = $(TRK_DIR)brown_util.h $(TRK_DIR)uEye_source.h $(TRK_DIR)track_util.h \
$(TRK_DIR)focus.h $(TRK_DIR)magnetscontrol.h  $(TRK_DIR)scan_zmag.h $(TRK_DIR)action.h \
$(TRK_DIR)calibration.h $(TRK_DIR)hat.h $(TRK_DIR)force.h $(TRK_DIR)save_movie_util.h \
$(TRK_DIR)record.h $(TRK_DIR)zdet.h $(TRK_DIR)trackBead.h $(TRK_DIR)zdet_topo.h \
$(TRK_DIR)fillibbt.h $(TRK_DIR)timer_thread.h $(TRK_DIR)gui.h

OBJS += $(TRK_O)

SRC += $(TRK_S)
HEADER += $(TRK_H)

#SRI_DIR = plug-ins-src/trackBead/pico_serial/
#SRI_O = objp/pico_serial.o
#SRI_S = $(SRI_DIR)pico_serial.c
#SRI_S = $(SRI_DIR)pico_serial.h

#OBJS += $(SRI_O)
#SRC += $(SRI_S)
#HEADER += $(SRI_H)


OBJS += $(SRF_O)
SRC += $(SRF_S)
HEADER += $(SRF_H)

#tv : ZMAG_CONTROL
#ifdef ZMAG_APT
SRM_DIR = plug-ins-src/trackBead/thorlabs/
SRM_O = objp/apt_magnets.o
SRM_S = $(SRM_DIR)apt_magnets.c
SRM_S = $(SRM_DIR)apt_magnets.h
LSCFLAGS += -DZMAG_APT
#endif

OBJS += $(SRM_O)
SRC += $(SRM_S)
HEADER += $(SRM_H)


#SRT_DIR = plug-ins-src/trackBead/pico_temperature/
#SRT_O = objp/pico_temperature.o
#SRT_S = $(SRT_DIR)pico_temperature.c
#SRT_S = $(SRT_DIR)pico_temperature.h

#OBJS += $(SRT_O)
#SRC += $(SRT_S)
#HEADER += $(SRT_H)

ifeq ($(FLOW_CONTROL),THORLABS)
FLC_DIR = plug-ins-src/trackBead/thorlabs/
FLC_O = objp/apt_flow_control.o
FLC_H = $(FLC_DIR)apt_flow_control.h $(FLC_DIR)APTAPI.h
FLC_S = $(FLC_DIR)apt_flow_control.c
FLC_A = lib/libAPT.a
FLC_L = -lAPT
OBJS += $(FLC_O)
SRC += $(FLC_S)
HEADER += $(FLC_H)
LSCFLAGS += -DFLOW_CONTROL
endif

ifeq ($(FLOW_CONTROL),DUMMY)
FLC_DIR = plug-ins-src/trackBead/
FLC_O = objp/flow_control.o
FLC_H = $(FLC_DIR)flow_control.h
FLC_S = $(FLC_DIR)flow_control.c
OBJS += $(FLC_O)
SRC += $(FLC_S)
HEADER += $(FLC_H)
LSCFLAGS += -DFLOW_CONTROL_DUMMY
endif


THM_DIR = plug-ins-src/trackBead/thermal_z/
THM_O = objp/thermal_z.o
THM_S = $(THM_DIR)thermal_z.c
THM_S = $(THM_DIR)thermal_z.h

OBJS += $(THM_O)
SRC += $(THM_S)
HEADER += $(THM_H)


#add of sdi files
SDI_DIR = plug-ins-src/trackBead/SDITracking_v2/
SDI_O_P = objp/sdi_v2_parameter.o
SDI_S_P = $(SDI_DIR)sdi_v2_parameter.c
SDI_S_P = $(SDI_DIR)sdi_v2_parameter.h
OBJS += $(SDI_O_P)
SRC += $(SDI_S_P)
HEADER += $(SDI_H_P)

SDI_O_B = objp/sdi_v2_basic.o
SDI_S_B = $(SDI_DIR)sdi_v2_basic.c
SDI_S_B = $(SDI_DIR)sdi_v2_basic.h
OBJS += $(SDI_O_B)
SRC += $(SDI_S_B)
HEADER += $(SDI_H_B)

SDI_O_M = objp/sdi_v2_movie.o
SDI_S_M = $(SDI_DIR)sdi_v2_movie.c
SDI_S_M = $(SDI_DIR)sdi_v2_movie.h
OBJS += $(SDI_O_M)
SRC += $(SDI_S_M)
HEADER += $(SDI_H_M)

SDI_O_F = objp/sdi_v2_fringes.o
SDI_S_F = $(SDI_DIR)sdi_v2_fringes.c
SDI_S_F = $(SDI_DIR)sdi_v2_fringes.h
OBJS += $(SDI_O_F)
SRC += $(SDI_S_F)
HEADER += $(SDI_H_F)

SDI_O_Y = objp/sdi_v2_y_profile.o
SDI_S_Y = $(SDI_DIR)sdi_v2_y_profile.c
SDI_S_Y = $(SDI_DIR)sdi_v2_y_profile.h
OBJS += $(SDI_O_Y)
SRC += $(SDI_S_Y)
HEADER += $(SDI_H_Y)

SDI_O_BD = objp/sdi_v2_bead.o
SDI_S_BD = $(SDI_DIR)sdi_v2_bead.c
SDI_S_BD = $(SDI_DIR)sdi_v2_bead.h
OBJS += $(SDI_O_BD)
SRC += $(SDI_S_BD)
HEADER += $(SDI_H_BD)

SDI_O_FA = objp/sdi_v2_fov_analysis.o
SDI_S_FA = $(SDI_DIR)sdi_v2_fov_analysis.c
SDI_S_FA = $(SDI_DIR)sdi_v2_fov_analysis.h
OBJS += $(SDI_O_FA)
SRC += $(SDI_S_FA)
HEADER += $(SDI_H_FA)

SDI_O_G = objp/sdi_v2_graphic.o
SDI_S_G = $(SDI_DIR)sdi_v2_graphic.c
SDI_S_G = $(SDI_DIR)sdi_v2_graphic.h
OBJS += $(SDI_O_G)
SRC += $(SDI_S_G)
HEADER += $(SDI_H_G)

SDI_O_FOV = objp/sdi_v2_general.o
SDI_S_FOV = $(SDI_DIR)sdi_v2_general.c
SDI_S_FOV = $(SDI_DIR)sdi_v2_general.h
OBJS += $(SDI_O_FOV)
SRC += $(SDI_S_FOV)
HEADER += $(SDI_H_FOV)


ifeq ($(wildcard $(TRK_DIR)uEye/$(UEYE_SDK)/uEye_api.lib),)
	UEYE_LIB = lib/libuEye_api.a lib/libuEye_tools.a
else
	UEYE_LIB = lib/uEye_api.lib lib/uEye_tools.lib
endif



# above are the definitions
##################################################################################################################
##################################################################################################################
##################################################################################################################
# below are the rules


all : platform.mk alfont fftw3  bin/picoueye-Hua.exe
	@echo for $(SYSTEM)/$(PLATFORM), the $(LINKING) version has been build.

.PHONY : all installeur zip plugins plgclean plsrczip doc clean help \
       alfont fftw3

platform.mk : platform_template.mk
	@-if [ ! -e platform.mk ]; then cp platform_template.mk platform.mk; \
	echo A new platform.mk has been generated from the template. \
	please edit this file before running make again. ; \
	else touch platform.mk; \
    echo Your platform.mk was kept, but it may be older than the one from the SVN. \
    You should check it. ; fi;
	exit 1



#################################################
# below are TRK rules:                          #
#################################################

$(TRK_O) : $(TRK_S) $(TRK_H)
	$(CC) $(LSCFLAGS) $(CFLAGS) $(TRK_DIR)/$(notdir $(patsubst %.o,%.c,$@)) -o $@


#################################################
# below are CFG rules:                          #
#################################################

$(CFG_O) : $(CFG_S) $(CFG_H)
	$(CC) $(LSCFLAGS) $(CFLAGS) $(CFG_DIR)/$(notdir $(patsubst %.o,%.c,$@)) -o $@


$(NRU_O) : $(NRU_S) $(NRU_H)
	$(CC) $(CFLAGS) $(NRU_DIR)/$(notdir $(patsubst %.o,%.c,$@)) -o $@

objp/wlc.o : plug-ins-src/wlc/wlc.c plug-ins-src/wlc/wlc.h
	$(CC) $(CFLAGS) -o objp/wlc.o -c plug-ins-src/wlc/wlc.c

$(SRI_O) : $(SRI_S) $(SRI_S)
	$(CC) $(LSCFLAGS) $(CFLAGS) $(SRI_DIR)/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(SRF_O) : $(SRF_S) $(SRF_S)
	$(CC) $(LSCFLAGS) $(CFLAGS) $(SRF_DIR)/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(SRM_O) : $(SRM_S) $(SRM_S)
	$(CC) $(LSCFLAGS) $(CFLAGS) $(SRM_DIR)/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(SRT_O) : $(SRT_S) $(SRT_S)
	$(CC) $(LSCFLAGS) $(CFLAGS) $(SRT_DIR)/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(THM_O) : $(THM_S) $(THM_S)
	$(CC) $(LSCFLAGS) $(CFLAGS) $(THM_DIR)/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(FLC_O) : $(FLC_S) $(FLC_H)
	$(CC) $(LSCFLAGS) $(CFLAGS) $(FLC_DIR)/$(notdir $(patsubst %.o,%.c,$@)) -o $@



objp/xvplot.o : plug-ins-src/xvplot/xvplot.c plug-ins-src/xvplot/xvplot.h
	$(CC) $(CFLAGS) -o objp/xvplot.o -c plug-ins-src/xvplot/xvplot.c



objp/cardinal.o : plug-ins-src/cardinal/cardinal.c plug-ins-src/cardinal/cardinal.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/cardinal.o -c plug-ins-src/cardinal/cardinal.c


objp/sftl32n.o : plug-ins-src/cardinal/sftl32n.c plug-ins-src/cardinal/sftl32n.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/sftl32n.o -c plug-ins-src/cardinal/sftl32n.c


objp/stat.o : plug-ins-src/stat/stat.c plug-ins-src/stat/stat.h
	$(CC) $(CFLAGS) -o objp/stat.o -c plug-ins-src/stat/stat.c

objp/cordrift.o : plug-ins-src/cordrift/cordrift.c plug-ins-src/cordrift/cordrift.h
	$(CC) $(CFLAGS) -o objp/cordrift.o -c plug-ins-src/cordrift/cordrift.c


objp/fft2d.o objp/fftbtl32n.o: plug-ins-src/fft2d/fft2d.c plug-ins-src/fft2d/fft2d.h
	$(CC) $(CFLAGS) -o objp/fft2d.o -c plug-ins-src/fft2d/fft2d.c
	$(CC) $(CFLAGS) -o objp/fftbtl32n.o -c plug-ins-src/fft2d/fftbtl32n.c

#bon : il y a plus elegant mais bon..
objp/sdi_v2_parameter.o : plug-ins-src/trackBead/SDITracking_v2/sdi_v2_parameter.c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_parameter.h
	$(CC) $(CFLAGS) -o objp/sdi_v2_parameter.o -c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_parameter.c

objp/sdi_v2_basic.o : plug-ins-src/trackBead/SDITracking_v2/sdi_v2_basic.c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_basic.h
	$(CC) $(CFLAGS) -o objp/sdi_v2_basic.o -c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_basic.c

objp/sdi_v2_fringes.o : plug-ins-src/trackBead/SDITracking_v2/sdi_v2_fringes.c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_fringes.h
	$(CC) $(CFLAGS) -o objp/sdi_v2_fringes.o -c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_fringes.c

objp/sdi_v2_y_profile.o : plug-ins-src/trackBead/SDITracking_v2/sdi_v2_y_profile.c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_y_profile.h
	$(CC) $(CFLAGS) -o objp/sdi_v2_y_profile.o -c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_y_profile.c

objp/sdi_v2_bead.o : plug-ins-src/trackBead/SDITracking_v2/sdi_v2_bead.c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_bead.h
	$(CC) $(CFLAGS) -o objp/sdi_v2_bead.o -c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_bead.c

objp/sdi_v2_fov_analysis.o : plug-ins-src/trackBead/SDITracking_v2/sdi_v2_fov_analysis.c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_fov_analysis.h
	$(CC) $(CFLAGS) -o objp/sdi_v2_fov_analysis.o -c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_fov_analysis.c

objp/sdi_v2_graphic.o : plug-ins-src/trackBead/SDITracking_v2/sdi_v2_graphic.c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_graphic.h
		$(CC) $(CFLAGS) -o objp/sdi_v2_graphic.o -c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_graphic.c

objp/sdi_v2_general.o : plug-ins-src/trackBead/SDITracking_v2/sdi_v2_general.c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_general.h
		$(CC) $(CFLAGS) -o objp/sdi_v2_general.o -c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_general.c

objp/sdi_v2_movie.o : plug-ins-src/trackBead/SDITracking_v2/sdi_v2_movie.c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_movie.h
		$(CC) $(CFLAGS) -o objp/sdi_v2_movie.o -c plug-ins-src/trackBead/SDItracking_v2/sdi_v2_movie.c

#################################################
# below are fftw3 rules:                        #
#################################################
fftw3 : $(FFTW3_STUFF)

bin/fftw3.dll : $(FFTW3_DIR)fftw3.dll
	cp -uvp $(FFTW3_DIR)fftw3.dll bin/fftw3.dll

include/fftw3.h : $(FFTW3_DIR)fftw3.h
	cp -uvp $(FFTW3_DIR)fftw3.h include/fftw3.h

lib/libfftw3.a : $(FFTW3_DIR)fftw3.dll $(FFTW3_DIR)fftw3.def
	cd $(FFTW3_DIR); dlltool --def fftw3.def -k --dllname fftw3.dll \
 --output-lib ../../lib/libfftw3.a; cd ../..

bin/libfftw3.so :                # note : this is unused, but just in case...
	ln -s bin/libfftw3.so $(word 1,$(GNU_LIBS_DIR))/lib/libfftw3.so

bin/libfftw3.dylib :             # note : this is unused, but just in case...
	ln -s bin/libfftw3.dylib $(word 1,$(GNU_LIBS_DIR))/lib/libfftw3.dylib




$(TRK_DIR)uEye/$(UEYE_SDK)/uEye_api.def : $(TRK_DIR)uEye/$(UEYE_SDK)/uEye_api.dll
	cd $(TRK_DIR)uEye/$(UEYE_SDK); gendef uEye_api.dll \
	; cd ../../../..

#	cd $(TRK_DIR)uEye/$(UEYE_SDK); pexports uEye_api.dll  | sed 's/^_//' > uEye_api.def \
#	; cd ../../../..



lib/libuEye_api.a : $(TRK_DIR)uEye/$(UEYE_SDK)/uEye_api.dll  $(TRK_DIR)uEye/$(UEYE_SDK)/uEye_api.def
	cd $(TRK_DIR)uEye/$(UEYE_SDK); cp -v uEye_api.dll ../../../../bin;\
     dlltool --def uEye_api.def -k --dllname uEye_api.dll \
 --output-lib ../../../../lib/libuEye_api.a; cd ../../../..

lib/uEye_api.lib : $(TRK_DIR)uEye/$(UEYE_SDK)/uEye_api.lib
	cp $(TRK_DIR)uEye/$(UEYE_SDK)/uEye_api.lib lib/uEye_api.lib;

lib/uEye_tools.lib : $(TRK_DIR)uEye/$(UEYE_SDK)/uEye_api.lib
	cp $(TRK_DIR)uEye/$(UEYE_SDK)/uEye_tools.lib lib/uEye_tools.lib;

$(TRK_DIR)uEye/$(UEYE_SDK)/uEye_tools.def : $(TRK_DIR)uEye/$(UEYE_SDK)/uEye_tools.dll
	cd $(TRK_DIR)uEye/$(UEYE_SDK); gendef uEye_tools.dll  \
	; cd ../../../..

#	cd $(TRK_DIR)uEye/$(UEYE_SDK); pexports uEye_tools.dll  | sed 's/^_//' > uEye_tools.def \
#	; cd ../../../..

lib/libuEye_tools.a : $(TRK_DIR)uEye/$(UEYE_SDK)/uEye_tools.dll  $(TRK_DIR)uEye/$(UEYE_SDK)/uEye_tools.def
	cd $(TRK_DIR)uEye/$(UEYE_SDK); cp -v uEye_tools.dll ../../../../bin;\
     dlltool --def uEye_tools.def -k --dllname uEye_tools.dll \
 --output-lib ../../../../lib/libuEye_tools.a; cd ../../../..


ifeq ($(FLOW_CONTROL),THORLABS)
$(FLC_DIR)APT.def : $(FLC_DIR)APT.dll
	cd $(FLC_DIR); pexports APT.dll  | sed 's/^_//' > APT.def \
	; cd ../../../..

lib/libAPT.a : $(FLC_DIR)APT.dll $(FLC_DIR)APT.def
	cd $(FLC_DIR); cp -v Jai_APT.dll ../../../../bin;\
     dlltool --def APT.def -k --dllname APT.dll \
 --output-lib ../../../../lib/libAPT.a; cd ../../../..
endif



#################################################
# below are MADlib.dll rules:              #
#################################################




bin/PI_GCS2_DLL.dll : $(SRF_DIR)PI_GCS2_DLL.dll
	cp -uvp $(SRF_DIR)PI_GCS2_DLL.dll bin/PI_GCS2_DLL.dll

lib/libPI_GCS2_DLL.a : $(SRF_DIR)PI_GCS2_DLL.dll $(SRF_DIR)PI_GCS2_DLL.def
	cd $(SRF_DIR); dlltool --def PI_GCS2_DLL.def -k --dllname PI_GCS2_DLL.dll \
 --output-lib ../../../lib/libPI_GCS2_DLL.a; cd ../..



#################################################
# below are alfont rules:                       #
#################################################
alfont : $(ALF_STUFF)

$(ALF_OUT) :
	cd $(ALF_DIR); make global_all;

include/alfont.h : $(ALF_DIR)/alfont.h
	cp -vp $(ALF_DIR)/alfont.h include/alfont.h

include/alfontdll.h : $(ALF_DIR)/alfontdll.h
	cp -vp $(ALF_DIR)/alfontdll.h include/alfontdll.h



##############################################
# plug'ins stuff:                            #
##############################################

# the rules about plugins:
plugins : $(XVIN)
	$(foreach toto,$(PLUGINS),cd plug-ins-src/$(toto); make global_all; cd ../..;)

plsrczip :
	$(foreach toto,$(PLUGINS),cd plug-ins-src/$(toto); make global_zip; cd ../..;)

plgclean :
	$(foreach toto,$(PLUGINS),cd plug-ins-src/$(toto); make global_clean; cd ../..; )



#################################################################
# below are the rules to produce xvin objects:                  #
#################################################################

objp/fillib.o	: src/fillib.c include/xvin/fillib.h include/xvin/util.h
	$(CC) $(CFLAGS) -o objp/fillib.o -c src/fillib.c

objp/fftl32n.o	: src/fftl32n.c include/xvin/fftl32n.h
	$(CC) $(CFLAGS) -o objp/fftl32n.o -c src/fftl32n.c

$(DEV_O) : $(DEV_S) $(DEV_H)
	$(CC) $(CFLAGS) src/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(BOX_O) : $(BOX_S) $(BOX_H)
	$(CC) $(CFLAGS) src/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(PLOT_O) : $(PLOT_S) $(PLOT_H)
	$(CC) $(CFLAGS) src/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(GUI_O) : $(GUI_S) $(GUI_H) include/xvin/im_reg.h
	$(CC) $(CFLAGS) src/gui/$(notdir $(patsubst %.o,%.c,$@)) -o $@

objp/xvinerr.o	: include/xvin/xvinerr.h src/xvinerr.c
	$(CC) $(CFLAGS) -o objp/xvinerr.o src/xvinerr.c

objp/util.o	: include/xvin/util.h src/util.c
	$(CC) $(CFLAGS) -o objp/util.o src/util.c

objp/log_file.o	: include/xvin/util.h include/xvin/log_file.h src/log_file.c
	$(CC) $(CFLAGS) -o objp/log_file.o src/log_file.c

objp/gr_file.o	: include/xvin/gr_file.h src/gr_file.c
	$(CC) $(CFLAGS) -o objp/gr_file.o src/gr_file.c

objp/xvinu.o	: include/xvin.h src/xvinu.c
	$(CC) $(CFLAGS) -o objp/xvinu.o src/xvinu.c

objp/plot_reg.o : include/xvin/plot_reg.h src/plot_reg.c
	$(CC) $(CFLAGS) -o objp/plot_reg.o src/plot_reg.c

objp/plot_proj.o : include/xvin/plot_reg.h include/xvin/plot_ds.h include/xvin/plot_op.h\
 src/plot_proj.c
	$(CC) $(CFLAGS) -o objp/plot_proj.o src/plot_proj.c

objp/plot_reg_gui.o : include/xvin/plot_reg.h src/gui/plot_reg_gui.c include/xvin/plot_reg_gui.h
	$(CC) $(CFLAGS) -o objp/plot_reg_gui.o src/gui/plot_reg_gui.c

objp/gui_def.o : include/xvin/gui_def.h src/gui/gui_def.c
	$(CC) $(CFLAGS) -o objp/gui_def.o src/gui/gui_def.c

objp/win_file.o : include/xvin/util.h src/gui/win_file.c
	$(CC) $(CFLAGS) -o objp/win_file.o src/gui/win_file.c

objp/win_gui.o : src/gui/win_gui.c include/xvin.h
	$(CC) $(CFLAGS) -o objp/win_gui.o src/gui/win_gui.c

$(PL_MN_O) : $(PL_MN_S) $(PL_MN_H)
	$(CC) $(CFLAGS) src/menus/plot/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(PLTR_MN_O) : $(PLTR_MN_S) $(PL_MN_H)
	$(CC) $(CFLAGS) src/menus/plot/treat/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(IM_MN_O) : $(IM_MN_S) $(IM_MN_H)
	$(CC) $(CFLAGS) src/menus/image/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(IMTR_MN_O) : $(IMTR_MN_S) $(IMTR_MN_H)
	$(CC) $(CFLAGS) src/menus/image/treat/$(notdir $(patsubst %.o,%.c,$@)) -o $@

objp/project.o : src/menus/project.c
	$(CC) $(CFLAGS) -o objp/project.o src/menus/project.c

objp/help_mn.o : src/menus/help_mn.c src/svn.h
	$(CC) $(CFLAGS) -Isrc -o obj/help_mn.o src/menus/help_mn.c

objp/xv_pico.o : src/xv_pico.c include/xvin/xv_main.h src/svn.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/xv_pico.o src/xv_pico.c

$(OI_O) : $(OI_S) $(OI_H)
	$(CC) $(CFLAGS) src/$(notdir $(patsubst %.o,%.c,$@)) -o $@

objp/fft_filter_lib.o: src/lib/fft_filter_lib.c include/xvin/fft_filter_lib.h
	$(CC) $(CFLAGS) -o $@ src/lib/fft_filter_lib.c

objp/fftw3_lib.o: src/lib/fftw3_lib.c include/xvin/fftw3_lib.h
	$(CC) $(CFLAGS) -o $@ src/lib/fftw3_lib.c

objp/xv_tools_lib.o: src/lib/xv_tools_lib.c include/xvin/xv_tools_lib.h
	$(CC) $(CFLAGS) -o $@ src/lib/xv_tools_lib.c

objp/fortify.o : src/fortify.c include/fortify.h
	$(CC) $(CFLAGS) -DFORTIFY -o $@ src/fortify.c

objp/xv_plugins.o : src/xv_plugins.c include/xvin/xv_plugins.h
	$(CC) $(CFLAGS) -DBUILDING_DLL  -o $@ src/xv_plugins.c

objp/global_vars.o : src/global_vars.c
	$(CC) $(CFLAGS) -o $@ src/global_vars.c


src/svn.h: src/svn.in
	@echo "#define SVN_REVISION \"$(svn info | grep 'Last Changed Rev: '  | sed 's/Last Changed Rev: //g')\"" > src/svn.h
	@echo "#define SVN_DATE \"$(svn info | grep 'Last Changed Date: '  | sed 's/Last Changed Date: //g')\"" >> src/svn.h
	@echo "#define SVN_MODIFICATION \"$(svn info | grep 'Last Changed Author: '  | sed 's/Last Changed Author: //g')\"" >> src/svn.h



##############################################
# icon  (same code for all win32 versions)	 #
##############################################

ifeq ($(SYSTEM),WIN32)
ICON = objp/icon.o
objp/icon.o: icons/gr.ico icons/xvin.rc
	windres -I rc -O coff -i icons/xvin.rc -o objp/icon.o
endif

ifeq ($(SYSTEM),LINUX)
ICON = # objp/icon.o

# following 2 lines are required if and only if file X11_icon.c needs to be rebuilt
#icons/X11_icon.c: icons/gr.ico
#	icons/xfixicon.sh icons/gr.ico -o icons/X11_icon.c
objp/icon.o: icons/X11_icon.c
	$(CC) $(CFLAGS) -o $@ icons/X11_icon.c # -DALLEGRO_WITH_XWINDOWS -DALLEGRO_USE_CONTRUCTOR
endif



##################################################################
# xvin executable, win32 systems       					         #
##################################################################
ifeq ($(SYSTEM),WIN32)
ifeq ($(LINKING),STATIC)
bin/picoueye-Hua.exe : $(SRC) $(HEADER)$(OBJS) $(ICON) makefile include/xvin.h src/main_pico.c bin/fftw3.dll
	$(CC) -Wall -mwindows $(ICON) $(CPU_FLAGS) -D$(XV_TARGET) -DXVIN_STATICLINK \
 -I./include -I./include/xvin $(OBJS) -o $@ src/main_pico.c -L /usr/local/lib  -L./lib \
 $(GTK_LIB) -lfftw3 -lalfont -lalleg44 $(LFLAGS) -luEye_api -luEye_tools  -lAPT $(FLC_L) -lkernel32 -luser32 -lgdi32 -lcomdlg32 -lole32 -ldinput -lddraw -ldxguid -lwinmm -ldsound -ldl;
# -lalfontdll -lalleg
endif


ifeq ($(LINKING),DYNAMIC)
ifeq ($(PLATFORM),MINGW)
bin/picoueye-Hua.exe :  $(FFTW3_STUFF) bin/alfont.dll bin/picoueye-mad.dll lib/libpicoueye-mad.a $(ICON) makefile include/xvin.h src/main_pico.c
	$(CC) -Wall -mwindows $(PLATFORM_FLAGS) $(ICON) $(CPU_FLAGS) -D$(XV_TARGET) \
 -I./include -I./include/xvin -o bin/picoueye-mad.exe src/main_pico.c -L./lib -lalleg -lxvin
endif
ifeq ($(PLATFORM),CYGWIN)
bin/picoueye-mad.exe : bin/xvin.dll lib/libxvin.a $(ICON) makefile include/xvin.h src/main_pico.c bin/fftw3.dll
	$(CC) -Wall -mwindows $(PLATFORM_FLAGS) $(ICON) $(CPU_FLAGS) -D$(XV_TARGET) $(INC_FLAGS) \
 -I./include -I./include/xvin -o bin/picoueye-mad.exe src/main_pico.c -L./lib -lalleg -lxvin
endif
endif

endif # end of the SYSTEM=WIN32 case



##################################################################
# xvin executable, linux systems                                 #
##################################################################
ifeq ($(SYSTEM),LINUX)
ifeq ($(LINKING),STATIC)
bin/xvin : lib/libxvin.a  makefile include/xvin.h $(ICON) src/main_pico.c
	$(CC) -o main_pico.o $(CFLAGS) -DXVIN_STATICLINK src/main_pico.c
	$(CC) -s main_pico.o $(ICON) $(LD_FLAGS) -o $@ -L./lib -lxvin \
	$(FREETYPE_LIB) $(ALLEGRO_LIB) -lxvin -lalfont $(FFTWFLAGS)
endif # end of the case LINKING=STATIC

ifeq ($(LINKING),DYNAMIC)
bin/xvin : bin/libxvin.so include/xvin.h src/main_pico.c $(ICON) makefile
	$(CC) $(CFLAGS) src/main_pico.c -o main_pico.o; \
        cd bin/; $(CC) $(LD_FLAGS) -o xvin ../main_pico.o libpico.so libalfont.so \
	$(LIB_OPTS) $(FFTWFLAGS) $(FREETYPE_LIB) $(ALLEGRO_LIB) \
	-Wl,-rpath,$(HOME)/xvin/bin

endif # end of the case LINKING=DYNAMIC
endif # end of the case SYSTEM=LINUX



##################################################################
# xvin executable, MacOSX systems                                #
##################################################################
ifeq ($(SYSTEM),MACOS)
ifeq ($(LINKING),STATIC)
bin/xvin : lib/libxvin.a  makefile include/xvin.h src/main_pico.c
	$(CC) -c $(CPU_FLAGS) -D$(XV_TARGET) -I./include -o main_pico.o -I./include/xvin src/main_pico.c
	$(CC) -s -L/usr/X11R6/lib -Wl,-dynamic  -o bin/xvin main_pico.o -framework cocoa\
	-L./lib $(ALLEGRO_LIB) -lpico -lfreetype -lz -lalfont -lfftw3 -lm; \
	$(ALLEGRO_BIN)/fixbundle $@ icons/gr.bmp
endif # end of the case LINKING=STATIC

ifeq ($(LINKING),DYNAMIC)
bin/xvin : bin/libpico.dylib include/xvin.h src/main_pico.c makefile
	$(CC) $(CFLAGS) src/main_pico.c -o main_pico.o # -I./include -I./include/xvin
	cd bin/; \
    $(CC) $(LD_FLAGS) -o xvin ../main_pico.o libxpico.dylib libalfont.dylib \
	$(LIB_OPTS) $(FFTWFLAGS) \
	$(FREETYPE_LIB) $(ALLEGRO_LIB) \
	--rpath=$(HOME)/xvin/bin;	\
#	-Wl,-rpath,$(HOME)/xvin/bin
	$(ALLEGRO_BIN)/fixbundle $@ gr.bmp
endif # end of the case LINKING=DYNAMIC
endif # end of the case SYSTEM=MACOS



#################################################################
# libxvin.a library (same code for all platforms)               #
#################################################################
lib/libpicoueye-Hua.a : $(OBJS) $(MAKEFILES)
	ar $(AR_FLAGS) lib/libpicoueye-Hua.a $(OBJS)



#################################################################
# xvin.dll (same code for all win32 platforms)                  #
#################################################################
bin/picoueye-Hua.dll : $(OBJS) $(MAKEFILES)
	$(CC) -o $@ $(OBJS) $(PLATFORM_FLAGS) $(DLL_FLAGS) \
	-L./lib $(addprefix -L,$(addsuffix /lib/,$(GNU_LIBS_DIR))) \
    $(FFTWFLAGS) -lalfont -lalleg -Wl,--export-all-symbols \
    -Wl,--enable-auto-import -Wl,--out-implib=lib/libpicoueye-mad.a


#################################################################
# xvin.so (same code for all linux platforms)                   #
#################################################################
bin/libpicoueye-Hua.so : $(OBJS) $(MAKEFILES)
	$(CC) -o $@ $(OBJS) -shared $(LIB_OPTS)  \
	$(LD_FLAGS) $(FFTWFLAGS) $(FREETYPE_LIB) $(ALLEGRO_LIB) \
	-Wl,-export-dynamic


#################################################################
# xvin.dylib (Mac OS X platform)                                #
#################################################################
bin/libpicoueye-Hua.dylib : $(OBJS) $(MAKEFILES)
	$(CC) -o $@ $(OBJS) -dynamiclib $(LIB_OPTS) -L$(XVIN_DIR)bin \
	$(LD_FLAGS) $(FFTWFLAGS) $(FREETYPE_LIB) $(ALLEGRO_LIB) \
	-lalfont -Wl,-dynamic


#################################################################
# below are the rules to produce the documentation    			#
#################################################################

DOCS = docs/html/xvin.html docs/rtf/xvin.rtf docs/txt/xvin.txt docs/txi/xvin.txi docs/chm/xvin.chm docs/chm/xvin.html

doc : $(DOCS)

docs/html/xvin.html : docs/src/xvin._tx
	bin/makedoc.exe -html docs/html/xvin.html docs/src/xvin._tx

docs/rtf/xvin.rtf : docs/src/xvin._tx
	bin/makedoc.exe -rtf docs/rtf/xvin.rtf docs/src/xvin._tx

docs/txt/xvin.txt : docs/src/xvin._tx
	bin/makedoc.exe -ascii  docs/txt/xvin.txt docs/src/xvin._tx

docs/txi/xvin.txi : docs/src/xvin._tx
	bin/makedoc.exe -txi docs/txi/xvin.txi docs/src/xvin._tx

docs/chm/xvin.html : docs/src/xvin._tx
	bin/makedoc.exe -ochm -html docs/chm/xvin.html docs/src/xvin._tx

docs/chm/xvin.chm : docs/src/xvin._tx docs/chm/xvin.html
	bin/makedoc.exe -chm docs/chm/xvin.html docs/src/xvin._tx; \
	cd docs/chm; /c/'Program Files/HTML Help Workshop'/hhc.exe xvin.hhp; cd ../..







#################################################
# cleaning (common to all platforms)			#
#################################################
clean :
	rm -f $(OBJS)
	rm -f lib/libpicoueye-Hua.a
	rm -f bin/picoueye-Hua.exe
	rm -f bin/picoueye-Hua.dll bin/picoueye-Hua.so bin/picoueye-Hua.dylib
	rm -f bin/picoueye-Hua
	rm -f $(ALF_STUFF) $(ALF_DIR)*.o
	rm -f $(FFTW3_STUFF)





##############################################
# installeur								 #
##############################################
installeur : installer/Picoueye_script_modern.nsi
	C:/Program\ Files/NSIS/makensis.exe installer/Picoueye_script_modern.nsi
	cp -uvp installer/picoueye_setup.exe .




#################################################
# archives										#
#################################################

zip	: $(HEADER) $(SRC) makefile src/main_pico.c # docs/src/xvin._tx
	$(ZIPEXE) xvinsrc$(ZIP_EXT) $(HEADER) $(SRC) \
  $(MAKEFILES) src/main_pico.c \
 icons/gr.ico icons/gr.bmp icons/X11_icon.c icons/xfixicon.sh icons/xvin.rc \
 objp/remove.me lib/remove.me bin/example.dat \
 bin/allegro.cfg bin/arial.ttf  bin/keyboard.dat bin/language.dat \
 bin/keyconf.txt  \
 $(FFTW3_S) $(ALF_S) \
 docs/src/xvin._tx docs/html/*.* docs/rtf/*.* docs/txi/*.* docs/txt/*.* docs/chm/*.* \
# include/plug-ins/fftw3.h


#################################################
# help                                          #
#################################################
help :
	@echo "Makefile for XVin. possible options are :"
	@echo "[]/[all]   build XVin"
	@echo "[help]     print this help message"
	@echo "[clean]    clean the XVin objects and binaries"
	@echo "[zip]      zip the XVin sources"
	@echo "[doc]      build the XVin docs"
	@echo " "
	@echo "[plugins]  build all the plugins from the plugins list"
	@echo "[plsrczip] scan all plugins and archive all the sources"
	@echo "[plgclean] scan all the plugins and clean them"
	@echo " "
	@echo "the plugins list is (from the main makefile):"
	@echo $(PLUGINS);
