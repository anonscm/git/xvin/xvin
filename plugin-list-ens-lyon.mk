# this is the list of plug'ins that are used in Lyon (Nicolas G.).
#
# the present file (ens-lyon.mk) only defines the variable $(PLUGINS) to be
# used in the main XVin makefile to compile/zip/clean all plug'ins at the same time

PLUGINS = inout diff find response hist GC resistance graphs turbulence \
	pendule	cycles Langevin random function_generator fits Jarzynski \
	statistics corde debug diskop irreversible stc 


