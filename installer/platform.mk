# below is the system used:
SYSTEM = WIN32
# SYSTEM = LINUX
# SYSTEM = MACOS

# below is the platform (used only on win32 machines):
PLATFORM = MINGW
# PLATFORM = CYGWIN

# below is the linking type:
#LINKING = STATIC
LINKING = DYNAMIC

# below we eventually define a memory checker (comment it if none is used)
# MEMCHECK = FORTIFY

# below are the CPU flags, and eventually optimization flags:
CPU_FLAGS      = -O2 # -march=pentium4 -msse2 -mfpmath=sse
#CPU_FLAGS      = -O2 -march=nocona -msse2 -mfpmath=sse

# below is the C compiler name (e.g. : gcc-3.0 on debian/woody, gcc on most systems)
CC			 = gcc

# following line indicates where are the libraries required to build XVin
# those libraries are : Allegro/Freetype/FFTW/GSL
# you can write several directories, separated with spaces, but it is
# recommended to use only one directory, if you can.
#GNU_LIBS_DIR = $(HOME)/local            # typical for linux when no root
#GNU_LIBS_DIR = /usr/local               # most typical 
GNU_LIBS_DIR = /c/PROGRA~1/GnuWin32               # most typical on Win32 Mingw systems


# below are lines to tell where your libraries are, if they are in different 
# directories then the one specified above. If you don't know, or if libs are
# in the directory above, please comment those definitions.
#ALLEGRO_DIR=
#FREETYPE_DIR=
#GSL_DIR=


# below is a computation of the libraries include path
MY_INC = $(addprefix -I,$(addsuffix /include,$(strip $(GNU_LIBS_DIR))))
# following line may be usefull on mingw only, but I recommend using
#    GNU_LIBS_DIR = $(XVIN_DIR) 
# instead.
# MY_INC = -I./include/allegro -I./include/allfont -I../../include/pthread



# We write the plug'in names in a compact way:
#include $(XVIN_DIR)plugin-list-ens-lyon.mk
include $(XVIN_DIR)plugin-list-ens-lps.mk
# another possible way of giving a plugins list:
#PLUGINS = inout random hist



#############################################################################
# do not edit below this line
#############################################################################
include $(XVIN_DIR)defines.mk
