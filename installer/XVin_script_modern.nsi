# XVin installer 
# 2004-02-10

# let's use the modern style, it is more user-friendly:
!include "MUI.nsh"
# let's include some function to register file extension into windows explorer:
!include "fileassoc.nsh"


# The name of the installer
Name "XVin"
Caption "XVin installer for Win32"

# The file to create
OutFile "XVin_setup.exe"
!define MUI_ICON "..\icons\GR2.ICO"
!define MUI_UNICON "..\icons\GR2.ICO"
!define MUI_HEADERIMAGE  # Display an image on the header of the page.
!define MUI_HEADERIMAGE_BITMAP xvin_installer.bmp
!define MUI_HEADERIMAGE_UNBITMAP "xvin_installer.bmp"
!define MUI_BGCOLOR "FFFFFF"

XPStyle on
!define MUI_ABORTWARNING
!define MUI_FINISHPAGE_NOAUTOCLOSE	# wait after files copying is finished for user to be able to look at the log
!define MUI_UNFINISHPAGE_NOAUTOCLOSE	# idem, when not succesfull


; The default installation directory
#InstallDir $PROGRAMFILES\XVin
InstallDir c:\msys64\home\XVin

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\XVin" "Install_Dir"


;--------------------------------
LicenseText "XVin license"
LicenseData "license.rtf"

;--------------------------------

; Pages

# !define MUI_PAGE_HEADER_TEXT "XVin"
# !define MUI_PAGE_HEADER_SUBTEXT "The Physicist's Friend" 
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "license.rtf"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

;--------------------------------
; installer sections

Section "XVin core" section_core

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR\bin
  SetFileAttributes $INSTDIR\bin FILE_ATTRIBUTE_NORMAL

  ; Put file there
  File "..\bin\arial.ttf"
  File "..\bin\fonts.dat"
  File "..\bin\keyboard.dat"
  File "..\bin\language.dat"
  File "..\bin\xvin.exe"
  File "..\bin\xvin.dll"
  
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\XVin "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin" "DisplayName" "XVin"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin" "NoRepair" 1

  WriteUninstaller "uninstall.exe"
  
SectionEnd


Section "Allegro libraries" section_allegro_lib
  SetOutPath $INSTDIR\bin
  File "..\bin\alleg44.dll"
  File "..\bin\alfont.dll"
  File "..\bin\libfreetype-6.dll"
  File "d:\msys64\mingw32\bin\libgomp-1.dll"
  File "d:\msys64\mingw32\bin\libgcc_s_dw2-1.dll" 
  File "d:\msys64\mingw32\bin\libwinpthread-1.dll" 
  File "d:\msys64\mingw32\bin\libfreetype-6.dll" 
  File "d:\msys64\mingw32\bin\libbz2-1.dll" 
  File "d:\msys64\mingw32\bin\libharfbuzz-0.dll" 
  File "d:\msys64\mingw32\bin\libglib-2.0-0.dll" 
  File "d:\msys64\mingw32\bin\libintl-8.dll" 
  File "d:\msys64\mingw32\bin\libiconv-2.dll" 
  File "d:\msys64\mingw32\bin\libpng16-16.dll" 
  File "d:\msys64\mingw32\bin\zlib1.dll" 
  File "d:\msys64\mingw32\bin\libfftw3-3.dll" 
  File "d:\msys64\mingw32\bin\libstdc++-6.dll"
  File "d:\msys64\mingw32\bin\libatk-1.0-0.dll"
  File "d:\msys64\mingw32\bin\libcairo-2.dll"
  File "d:\msys64\mingw32\bin\libcairo-gobject-2.dll"
  File "d:\msys64\mingw32\bin\libexpat-1.dll"
  File "d:\msys64\mingw32\bin\libffi-6.dll"
  File "d:\msys64\mingw32\bin\libfontconfig-1.dll"
  File "d:\msys64\mingw32\bin\libgdk-3-0.dll"
  File "d:\msys64\mingw32\bin\libgdk_pixbuf-2.0-0.dll"
  File "d:\msys64\mingw32\bin\libgio-2.0-0.dll"
  File "d:\msys64\mingw32\bin\libgmodule-2.0-0.dll"
  File "d:\msys64\mingw32\bin\libgobject-2.0-0.dll"
  File "d:\msys64\mingw32\bin\libgtk-3-0.dll"
  File "d:\msys64\mingw32\bin\libpango-1.0-0.dll"
  File "d:\msys64\mingw32\bin\libpangocairo-1.0-0.dll"
  File "d:\msys64\mingw32\bin\libpangoft2-1.0-0.dll"
  File "d:\msys64\mingw32\bin\libpangowin32-1.0-0.dll"
  File "d:\msys64\mingw32\bin\libpixman-1-0.dll"
  File "d:\msys64\mingw32\bin\libepoxy-0.dll"
  File "d:\msys64\mingw32\bin\libpcre-1.dll"
  File "d:\msys64\mingw32\bin\libdl.dll"
  File "d:\msys64\mingw32\bin\libgraphite2.dll"
  File "d:\msys64\mingw32\bin\libssp-0.dll"	
  File "d:\msys64\mingw32\bin\libgsl-23.dll"
  File "d:\msys64\mingw32\bin\libgslcblas-0.dll"


SectionEnd

Section "Allegro configuration files" section_allegro_config
  SetOutPath $INSTDIR\bin
  File "..\bin\setup.exe"
#  File "..\bin\keyconf.exe"
  File "..\bin\keyconf.txt"
  File "..\bin\allegro.cfg"
  File "..\misc\default_config\xvin.cfg"  
SectionEnd

SubSection "Plug'ins" section_plugins

  Section "random numbers" subsec_random
    SetOutPath $INSTDIR\bin
    File "..\bin\random.dll"
  SectionEnd

  Section "histograms" subsec_hist
    SetOutPath $INSTDIR\bin
    File "..\bin\hist.dll"
  SectionEnd

  Section "inport/export data" subsec_inout
    SetOutPath $INSTDIR\bin
    File "..\bin\inout.dll"
  SectionEnd


  Section "Old numerical reciepe" subsec_nrutil
    SetOutPath $INSTDIR\bin
  File "..\bin\nrutil.dll"    
  SectionEnd

  Section "Worm Like Chain model" subsec_wlc
    SetOutPath $INSTDIR\bin
  File "..\bin\wlc.dll"
  SectionEnd

  Section "JPEG Image support" subsec_jpeg
    SetOutPath $INSTDIR\bin
  #File "..\bin\jpeg62.dll"   
  File "..\bin\libjpeg-8.dll"
  File "..\bin\xvjpeg.dll"
  SectionEnd

  Section "Burst selection" subsec_select
    SetOutPath $INSTDIR\bin
  File "..\bin\select.dll" 
  SectionEnd

  Section "Camera coorection" subsec_cardinal
    SetOutPath $INSTDIR\bin 
  File "..\bin\cardinal.dll"  
  SectionEnd


  Section "Old fft2d support" subsec_fft2d
    SetOutPath $INSTDIR\bin
  File "..\bin\fft2d.dll"     
  SectionEnd

  Section "shift_data" subsec_shift_data
    SetOutPath $INSTDIR\bin
  File "..\bin\shift_data.dll"     
  SectionEnd

  Section "Statistical functions" subsec_stat
    SetOutPath $INSTDIR\bin
  File "..\bin\stat.dll"    
  SectionEnd

  Section "Spreadsheet plots" subsec_xvplot
    SetOutPath $INSTDIR\bin
  File "..\bin\xvplot.dll"
  SectionEnd

  Section "Fatest Fourier in the West" subsec_fftw3
    SetOutPath $INSTDIR\bin
  File "..\bin\fftw3.dll"     
  SectionEnd

  Section "Tiff image support" subsec_tiff
    SetOutPath $INSTDIR\bin
  File "..\bin\libtiff-5.dll"  
  File "..\bin\xvtiff.dll"
  File "..\bin\zlib1.dll"
  #File "..\bin\jpeg62.dll" 
  SectionEnd

  Section "Pofile tracking" subsec_tilted
    SetOutPath $INSTDIR\bin
  File "..\bin\tilted.dll" 
  SectionEnd

  Section "1D deconvolution" subsec_deconv
    SetOutPath $INSTDIR\bin
  File "..\bin\deconv.dll" 
  SectionEnd

SubSectionEnd

Section "Documentation" section_doc
  SetOutPath $INSTDIR\docs\html
  File "..\docs\html\xvin*.*" 
  File "..\docs\html\*.css"	
  SetOutPath $INSTDIR\docs\rtf
  File "..\docs\rtf\xvin*.*"
  SetOutPath $INSTDIR\docs\txt
  File "..\docs\txt\xvin*.*"
  SetOutPath $INSTDIR\docs\chm
  File "..\docs\chm\xvin*.*"
  File "..\docs\chm\*.css"	
SectionEnd


Section "All lib and headers" section_all_lib_h
  SetOutPath $INSTDIR\lib
  File "..\lib\*.a"
  File "d:\msys64\mingw32\lib\liballeg*.a"
  SetOutPath $INSTDIR\include
  File "..\include\*.h"
  File "d:\msys64\mingw32\include\allegro.h"
  File "d:\msys64\mingw32\include\winalleg.h"
  SetOutPath $INSTDIR\include\xvin
  File "..\include\xvin\*.h"
  #SetOutPath $INSTDIR\include\plug-ins
  #File "..\include\plug-ins\*.h"
  SetOutPath $INSTDIR\include\allegro
  File "d:\msys64\mingw32\include\allegro\*.h"
  SetOutPath $INSTDIR\include\allegro\platform
  File "d:\msys64\mingw32\include\allegro\platform\*.h"
  SetOutPath $INSTDIR\include\allegro\internal
  File "d:\msys64\mingw32\include\allegro\internal\*.h"
  SetOutPath $INSTDIR\include\allegro\inline
  File "d:\msys64\mingw32\include\allegro\inline\*.inl"
  SetOutPath $INSTDIR
  File "..\makefile"
  SetOutPath $INSTDIR
  File "..\win32.mk"
  File "..\linux.mk"
  File "..\macos.mk"
  File "..\plugin.mk"
  File "..\defines.mk"
  File "..\plugin-list-ens-lps.mk"
  File "..\installer\platform.mk"
  File "..\platform_template.mk"
SectionEnd


Section "Start Menu Shortcuts" section_shortcuts
  CreateDirectory "$SMPROGRAMS\XVin"
  CreateShortCut "$SMPROGRAMS\XVin\XVin (test version).lnk" "$INSTDIR\bin\xvin.exe" "" "$INSTDIR\bin\xvin.exe" 0  
  CreateShortCut "$SMPROGRAMS\XVin\XVin Help.lnk" "$INSTDIR\docs\html\xvin.html" "" "$INSTDIR\docs\html\xvin.html" 0
  CreateShortCut "$SMPROGRAMS\XVin\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
SectionEnd



Section "Associate file extensions" section_gr
  !insertmacro APP_ASSOCIATE "gr" "XVin.GRfile" "XVin gr file" "$INSTDIR\bin\xvin.exe,0" "Open with XVin" "$INSTDIR\bin\xvin.exe %1"
SectionEnd


;--------------------------------
;Descriptions

;Language strings
LangString DESC_section_core ${LANG_ENGLISH} \
 "XVin core binaries and configuration files (required)"
LangString DESC_section_allegro_lib ${LANG_ENGLISH} \
 "Allegro and Allegro Fonts dynamic libraries. They are required unless you have compiled them yourself"
LangString DESC_section_allegro_config ${LANG_ENGLISH} \
 "Allegro configuration files, usefull only if you want to tune the graphics and input (optional)"
LangString DESC_section_plugins ${LANG_ENGLISH} \
 "Installs selected plug'ins to add specialized functions to XVin"
LangString DESC_section_doc ${LANG_ENGLISH} \
 "Install XVin documentation in html, rtf, and txt formats"
LangString DESC_section_shortcuts ${LANG_ENGLISH} \
 "Add shortcurts to the Windows Start Menu"
LangString DESC_section_gr ${LANG_ENGLISH} \
 "Associate .gr files with XVin in Windows explorer"

LangString DESC_subsec_inout ${LANG_ENGLISH} \
 "(inout.dll) To import/export binary and ascii files"
LangString DESC_subsec_random ${LANG_ENGLISH} \
 "(random.dll) To generate random number, follwing given distributions"
LangString DESC_subsec_hist ${LANG_ENGLISH} \
 "(hist.dll) To create histograms and extract information from them"
LangString DESC_subsec_nrutil ${LANG_ENGLISH} \
 "(nrutil.dll) Old numerical reciepe functions"
LangString DESC_subsec_wlc ${LANG_ENGLISH} \
 "(wlc.dll) Set of polymer elasticity models such as Worm Like Chain"
LangString DESC_subsec_jpeg ${LANG_ENGLISH} \
 "(wvjpeg.dll, jpeg-62.dll) allow import and export of Jpeg images"
LangString DESC_subsec_select ${LANG_ENGLISH} \
 "(select.dll) Selection of experimental bursts using the mouse"
LangString DESC_subsec_cardinal ${LANG_ENGLISH} \
 "(cardinal.dll) set of functions to analyze brownian motion and correct camera filtering"
LangString DESC_subsec_fft2d ${LANG_ENGLISH} \
 "(fft2d.dll) set of functions performing fft 2D and related functions"
LangString DESC_subsec_shift_data ${LANG_ENGLISH} \
 "(shift_data.dll) set of functions performing fft 1D convolution and correlation"
LangString DESC_subsec_stat ${LANG_ENGLISH} \
 "(stat.dll) old set of statistical functions"
LangString DESC_subsec_xvplot ${LANG_ENGLISH} \
 "(xvplot.dll) some support of speadsheet data"
LangString DESC_subsec_fftw3 ${LANG_ENGLISH} \
 "(fftw3.dll) the fastest fourier transform in the west"
LangString DESC_subsec_tilted ${LANG_ENGLISH} \
 "(tilted.dll) Profile tracking functions"
LangString DESC_subsec_tiff ${LANG_ENGLISH} \
 "(xvtiff.dll, zlib1.dll, libtif-5.dll, jpeg62.dll) Inport support for TIFF images"
LangString DESC_subsec_deconv ${LANG_ENGLISH} \
 "(deconv.dll) 1D deconvolution routines"


  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${section_core}	$(DESC_section_core)
    !insertmacro MUI_DESCRIPTION_TEXT ${section_allegro_lib} $(DESC_section_allegro_lib)
    !insertmacro MUI_DESCRIPTION_TEXT ${section_allegro_config} $(DESC_section_allegro_config)

    !insertmacro MUI_DESCRIPTION_TEXT ${section_plugins} $(DESC_section_plugins)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_inout} $(DESC_subsec_inout)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_random} $(DESC_subsec_random)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_hist} $(DESC_subsec_hist)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_tiff} $(DESC_subsec_tiff)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_jpeg} $(DESC_subsec_jpeg)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_stat} $(DESC_subsec_stat)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_wlc} $(DESC_subsec_wlc)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_select} $(DESC_subsec_select)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_nrutil} $(DESC_subsec_nrutil)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_cardinal} $(DESC_subsec_cardinal)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_fft2d} $(DESC_subsec_fft2d)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_xvplot} $(DESC_subsec_xvplot)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_fftw3} $(DESC_subsec_fftw3)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_tilted} $(DESC_subsec_tilted)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_deconv} $(DESC_subsec_deconv)

    !insertmacro MUI_DESCRIPTION_TEXT ${section_doc} $(DESC_section_doc)
    !insertmacro MUI_DESCRIPTION_TEXT ${section_all_lib_h} $(DESC_section_all_lib_h)
    !insertmacro MUI_DESCRIPTION_TEXT ${section_shortcuts} $(DESC_section_shortcuts)
    !insertmacro MUI_DESCRIPTION_TEXT ${section_gr}	$(DESC_section_gr)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END



;--------------------------------
; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin"
  DeleteRegKey HKLM SOFTWARE\XVin

  ; Remove files and uninstaller
  Delete $INSTDIR\bin\arial.ttf
  Delete $INSTDIR\bin\fonts.dat
  Delete $INSTDIR\bin\keyboard.dat
  Delete $INSTDIR\bin\language.dat
  Delete $INSTDIR\bin\xvin.exe
  Delete $INSTDIR\bin\xvin.dll
  Delete $INSTDIR\bin\xvin.cfg

  Delete $INSTDIR\bin\alfont.dll

  Delete $INSTDIR\bin\setup.exe
#  Delete $INSTDIR\bin\keyconf.exe
  Delete $INSTDIR\bin\keyconf.txt
  Delete $INSTDIR\bin\allegro.cfg

  Delete $INSTDIR\bin\random.dll
  Delete $INSTDIR\bin\hist.dll
  Delete $INSTDIR\bin\inout.dll
  Delete $INSTDIR\bin\jpeg-62.dll"   
  Delete $INSTDIR\bin\select.dll"  
  Delete $INSTDIR\bin\cardinal.dll"  
  Delete $INSTDIR\bin\xvjpeg.dll"
  Delete $INSTDIR\bin\fft2d.dll"     
  Delete $INSTDIR\bin\jpeg62.dll"    
  Delete $INSTDIR\bin\stat.dll"    
  Delete $INSTDIR\bin\xvplot.dll"
  Delete $INSTDIR\bin\fftw3.dll"     
  Delete $INSTDIR\bin\libtiff-5.dll"  
  Delete $INSTDIR\bin\xvtiff.dll"
  Delete $INSTDIR\bin\hist.dll"
  Delete $INSTDIR\bin\nrutil.dll"    
  Delete $INSTDIR\bin\tilted.dll"    
  Delete $INSTDIR\bin\zlib1.dll"
  Delete $INSTDIR\bin\deconv.dll"
  Delete $INSTDIR\bin\wlc.dll"

  Delete $INSTDIR\docs\html\*.* 
  Delete $INSTDIR\docs\rtf\*.*
  Delete $INSTDIR\docs\txt\*.*

  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\XVin\*.*"

  ; Remove directories used
  RMDir "$SMPROGRAMS\XVin"
  RMDir "$INSTDIR\bin"
  RMDir "$INSTDIR\docs\html"
  RMDir "$INSTDIR\docs\rtf"
  RMDir "$INSTDIR\docs\txt"
  RMDir "$INSTDIR\docs"
  RMDir "$INSTDIR"

 ;start of restore script
  !insertmacro APP_UNASSOCIATE "gr" "XVin.GRfile"

SectionEnd







