# XVin plug'ins installer 
# 2005-11-03

# let's use the modern style, it is more user-friendly:
!include "MUI.nsh"

# The name of the installer
Name "XVin Plug'ins"
Caption "XVin Plug'ins installer for Win32"

# The file to create
OutFile "Xvin_plugins_setup.exe"
!define MUI_ICON "..\icons\gr.ico"
!define MUI_UNICON "..\icons\gr.ico"
!define MUI_HEADERIMAGE  # Display an image on the header of the page.
!define MUI_HEADERIMAGE_BITMAP xvin_installer.bmp
!define MUI_HEADERIMAGE_UNBITMAP "xvin_installer.bmp"
!define MUI_BGCOLOR "FFFFFF"

XPStyle on
!define MUI_ABORTWARNING
!define MUI_FINISHPAGE_NOAUTOCLOSE	# wait after files copying is finished for user to be able to look at the log
!define MUI_UNFINISHPAGE_NOAUTOCLOSE	# idem, when not succesfull


; The default installation directory
InstallDir $PROGRAMFILES\XVin

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\XVin" "Install_Dir"


;--------------------------------
LicenseText "XVin license"
LicenseData ".\license.rtf"

;--------------------------------

; Pages

# !define MUI_PAGE_HEADER_TEXT "XVin"
# !define MUI_PAGE_HEADER_SUBTEXT "The Physicist's Friend" 
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

# !insertmacro MUI_UNPAGE_CONFIRM
# !insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

;--------------------------------
; installer sections

SubSection "Plug'ins" section_plugins

  Section "Plugins source" subsec_plgsrc
    SetOutPath $INSTDIR
    File "..\plsrc.zip"
  SectionEnd

  Section "Old numerical recipes" subsec_nrutil
    SetOutPath $INSTDIR\bin
  File "..\bin\nrutil.dll"    
  SectionEnd

  Section "Worm Like Chain model" subsec_wlc
    SetOutPath $INSTDIR\bin
  File "..\bin\wlc.dll"
  SectionEnd

  Section "JPEG Image support" subsec_jpeg
    SetOutPath $INSTDIR\bin
  File "C:\Program Files\GnuWin32\bin\jpeg62.dll"   
  File "..\bin\xvjpeg.dll"
  SectionEnd

  Section "Burst selection" subsec_select
    SetOutPath $INSTDIR\bin
  File "..\bin\select.dll" 
  SectionEnd

  Section "Camera correction" subsec_cardinal
    SetOutPath $INSTDIR\bin 
  File "..\bin\cardinal.dll"  
  SectionEnd


  Section "Old fft2d support" subsec_fft2d
    SetOutPath $INSTDIR\bin
  File "..\bin\fft2d.dll"     
  SectionEnd

  Section "Statistical functions" subsec_stat
    SetOutPath $INSTDIR\bin
  File "..\bin\stat.dll"    
  SectionEnd

  Section "Spreadsheet plots" subsec_xvplot
    SetOutPath $INSTDIR\bin
  File "..\bin\xvplot.dll"
  SectionEnd

  Section "Fatest Fourier in the West" subsec_fftw3
    SetOutPath $INSTDIR\bin
  File "..\bin\fftw3.dll"     
  SectionEnd

  Section "Tiff image support" subsec_tiff
    SetOutPath $INSTDIR\bin
  File "C:\Program Files\GnuWin32\bin\libtiff3.dll"  
  File "..\bin\xvtiff.dll"
  File "C:\Program Files\GnuWin32\bin\zlib1.dll"
  File "C:\Program Files\GnuWin32\bin\jpeg62.dll" 
  SectionEnd

  Section "Pofile tracking" subsec_tilted
    SetOutPath $INSTDIR\bin
  File "..\bin\tilted.dll" 
  SectionEnd

  Section "1D deconvolution" subsec_deconv
    SetOutPath $INSTDIR\bin
  File "..\bin\deconv.dll" 
  SectionEnd


SubSectionEnd



;--------------------------------
;Descriptions

;Language strings
LangString DESC_section_plugins ${LANG_ENGLISH} \
 "Installs selected plug'ins to add specialized functions to XVin"

LangString DESC_subsec_nrutil ${LANG_ENGLISH} \
 "(nrutil.dll) Old numerical reciepe functions"
LangString DESC_subsec_wlc ${LANG_ENGLISH} \
 "(wlc.dll) Set of polymer elasticity models such as Worm Like Chain"
LangString DESC_subsec_jpeg ${LANG_ENGLISH} \
 "(wvjpeg.dll, jpeg-62.dll) allow import and export of Jpeg images"
LangString DESC_subsec_select ${LANG_ENGLISH} \
 "(select.dll) Selection of experimental bursts using the mouse"
LangString DESC_subsec_cardinal ${LANG_ENGLISH} \
 "(cardinal.dll) set of functions to analyze brownian motion and correct camera filtering"
LangString DESC_subsec_fft2d ${LANG_ENGLISH} \
 "(fft2d.dll) set of functions performing fft 2D and related functions"
LangString DESC_subsec_stat ${LANG_ENGLISH} \
 "(stat.dll) old set of statistical functions"
LangString DESC_subsec_xvplot ${LANG_ENGLISH} \
 "(xvplot.dll) some support of speadsheet data"
LangString DESC_subsec_fftw3 ${LANG_ENGLISH} \
 "(fftw3.dll) the fastest fourier transform in the west"
LangString DESC_subsec_tilted ${LANG_ENGLISH} \
 "(tilted.dll) Profile tracking functions"
LangString DESC_subsec_tiff ${LANG_ENGLISH} \
 "(xvtiff.dll, zlib1.dll, libtif3.dll, jpeg62.dll) Import support for TIFF images"
LangString DESC_subsec_deconv ${LANG_ENGLISH} \
 "(deconv.dll) 1D deconvolution routines"

;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN

    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_tiff} $(DESC_subsec_tiff)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_jpeg} $(DESC_subsec_jpeg)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_stat} $(DESC_subsec_stat)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_wlc} $(DESC_subsec_wlc)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_select} $(DESC_subsec_select)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_nrutil} $(DESC_subsec_nrutil)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_cardinal} $(DESC_subsec_cardinal)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_fft2d} $(DESC_subsec_fft2d)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_xvplot} $(DESC_subsec_xvplot)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_fftw3} $(DESC_subsec_fftw3)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_tilted} $(DESC_subsec_tilted)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_deconv} $(DESC_subsec_deconv)

  !insertmacro MUI_FUNCTION_DESCRIPTION_END

