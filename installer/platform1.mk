# below is the system used:
SYSTEM = WIN32
# SYSTEM = LINUX
# SYSTEM = MACOS

# below is the platform (used only on win32 machines):
PLATFORM = MINGW
#PLATFORM = CYGWIN

# below is the linking type:
# LINKING = STATIC
LINKING = DYNAMIC

# give the root directory
ROOTDIR = c:/Program\ Files/Xvin/

# archive name for all plugins source
ZIPALL = plsrc.zip

#################################################################
# do not edit below this line
#################################################################



#################################################################
# below we import definitions specific to each system			#
#################################################################
ifeq ($(SYSTEM),WIN32)
	XVIN = $(FFTW3STUFF) bin/xvin.exe
	include $(ROOTDIR)win32.mk
else
	XVIN = bin/xvin 
ifeq ($(SYSTEM),MACOS)
	include macos.mk
else 
ifeq ($(SYSTEM),LINUX)
	include linux.mk
else 
# 	return echo "no system defined"
endif
endif
endif

