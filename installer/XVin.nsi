# XVin binaries installer 
# 2007-02-08

# note !!!
# the source files shouldn't be installed with this installer
# maybe with another installer or with the SVN 

# let's use the modern style, it is more user-friendly:
!include "MUI.nsh"
# let's include some function to register file extension into windows explorer:
!include "fileassoc.nsh"


# The name of the installer
Name "XVin"
Caption "XVin installer for Win32"

# The file to create
OutFile "XVin_setup.exe"
!define MUI_ICON "..\icons\gr.ico"
!define MUI_UNICON "..\icons\gr.ico"
!define MUI_HEADERIMAGE  # Display an image on the header of the page.
!define MUI_HEADERIMAGE_BITMAP xvin_installer.bmp
!define MUI_HEADERIMAGE_UNBITMAP "xvin_installer.bmp"
!define MUI_BGCOLOR "FFFFFF"

XPStyle on
!define MUI_ABORTWARNING
!define MUI_FINISHPAGE_NOAUTOCLOSE	# wait after files copying is finished for user to be able to look at the log
!define MUI_UNFINISHPAGE_NOAUTOCLOSE	# idem, when not succesfull


; The default installation directory
InstallDir $PROGRAMFILES\XVin

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\XVin" "Install_Dir"


;--------------------------------
LicenseText "XVin license"
LicenseData "license.rtf"

;--------------------------------

; Pages

# !define MUI_PAGE_HEADER_TEXT "XVin"
# !define MUI_PAGE_HEADER_SUBTEXT "The Physicist's Friend" 
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "license.rtf"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

;--------------------------------
; installer sections

Section "XVin core" section_core

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR\bin
  
  ; Put file there
  File "..\bin\arial.ttf"
# File "..\bin\example.h"
  File "..\bin\fonts.dat"
#  File "..\bin\keyboard.dat"
  File "..\bin\language.dat"
  File "..\bin\alfont.dll"
  File "..\bin\xvin.exe"
  File "..\bin\xvin.dll"
  
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\XVin "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin" "DisplayName" "XVin"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin" "NoRepair" 1

  WriteUninstaller "uninstall.exe"
  
SectionEnd


Section "Allegro libraries" section_allegro_lib
  SetOutPath $INSTDIR\bin
  File "precompiled_libs\alleg42.dll"
#  File "..\bin\alleg42.dll"
# File "..\bin\freetype6.dll"
SectionEnd

Section "Allegro configuration files" section_allegro_config
  SetOutPath $INSTDIR\bin
  File "..\bin\setup.exe"
  File "..\bin\keyconf.exe"
# File "..\bin\keyconf.txt"
  File "..\bin\allegro.cfg"
SectionEnd

# Section "Xvin sources" section_xvin_src
#   SetOutPath $INSTDIR
#   File "..\xvinsrc.zip"
# SectionEnd

SubSection "Plug'ins" section_plugins

#   Section "Plugins source" subsec_plgsrc
#     SetOutPath $INSTDIR
#     File "..\plsrc.zip"
#   SectionEnd

  Section "random numbers" subsec_random
    SetOutPath $INSTDIR\bin
    File "..\bin\random.dll"
  SectionEnd

  Section "histograms" subsec_hist
    SetOutPath $INSTDIR\bin
    File "..\bin\hist.dll"
  SectionEnd

  Section "import/export data" subsec_inout
    SetOutPath $INSTDIR\bin
    File "..\bin\inout.dll"
  SectionEnd

SubSectionEnd

Section "Documentation" section_doc
  SetOutPath $INSTDIR\docs\html
  File "..\docs\html\*.*" 
  SetOutPath $INSTDIR\docs\rtf
  File "..\docs\rtf\*.*"
  SetOutPath $INSTDIR\docs\txt
  File "..\docs\txt\*.*"
  SetOutPath $INSTDIR\docs\chm
  File "..\docs\chm\*.*"
#  SetOutPath $INSTDIR\docs\allegro\chm
#  File "..\..\docs\chm\*.*"
#  SetOutPath $INSTDIR\docs\allegro\html
#  File "..\..\docs\html\*.*"

SectionEnd

# the following has been commented out, as Allegro should be ship in a different installer
# with the sources of XVin, not with the XVin executable

# Section "All lib and headers" section_all_lib_h
#   SetOutPath $INSTDIR\lib
#   File "..\lib\*.a"
#   SetOutPath $INSTDIR\include
#   File "..\include\*.h"
#   SetOutPath $INSTDIR\include\xvin
#   File "..\include\xvin\*.h"
#   SetOutPath $INSTDIR\include\allegro
#   File "..\include\allegro\*.h"
#   SetOutPath $INSTDIR\include\allegro\internal
#   File "..\include\allegro\internal\*.h"
#   SetOutPath $INSTDIR\include\allegro\inline
#   File "..\include\allegro\inline\*.*"
#   SetOutPath $INSTDIR\include\allegro\platform
#   File "..\include\allegro\platform\*.*"
#   SetOutPath $INSTDIR\include\plug-ins
#   File "..\include\plug-ins\*.h"
#   SetOutPath $INSTDIR
#   File "..\makefile"
#   SetOutPath $INSTDIR
#   File "..\win32.mk"
#   File "..\linux.mk"
#   File "..\macos.mk"
#   File "..\installer\platform.mk"
# SectionEnd


Section "Start Menu Shortcuts" section_shortcuts
  CreateDirectory "$SMPROGRAMS\XVin"
  CreateShortCut "$SMPROGRAMS\XVin\XVin (test version).lnk" "$INSTDIR\bin\xvin.exe" "" "$INSTDIR\bin\xvin.exe" 0  
  CreateShortCut "$SMPROGRAMS\XVin\XVin Help.lnk" "$INSTDIR\docs\html\xvin.html" "" "$INSTDIR\docs\html\xvin.html" 0
  CreateShortCut "$SMPROGRAMS\XVin\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
SectionEnd



Section "Associate file extensions" section_gr
  !insertmacro APP_ASSOCIATE "gr" "XVin.GRfile" "XVin gr file" "$INSTDIR\bin\xvin.exe,0" "Open with XVin" "$INSTDIR\bin\xvin.exe %1"
SectionEnd


;--------------------------------
;Descriptions

;Language strings
LangString DESC_section_core ${LANG_ENGLISH} \
 "XVin core binaries and configuration files (required)"
LangString DESC_section_allegro_lib ${LANG_ENGLISH} \
 "Allegro and Allegro Fonts dynamic libraries. They are required unless you have compiled them yourself"
LangString DESC_section_allegro_config ${LANG_ENGLISH} \
 "Allegro configuration files, usefull only if you want to tune the graphics and input (optional)"
LangString DESC_section_plugins ${LANG_ENGLISH} \
 "Installs selected plug'ins to add specialized functions to XVin"
LangString DESC_section_doc ${LANG_ENGLISH} \
 "Install XVin documentation in html, rtf, and txt formats"
LangString DESC_section_all_lib_h ${LANG_ENGLISH} \
 "Install Allegro 4.2, Allfont 1.0.2, Fftw3.01, Xvin libraries and header needed to compile plugins. They will be installed in ./lib and ./include"
LangString DESC_section_shortcuts ${LANG_ENGLISH} \
 "Add shortcurts to the Windows Start Menu"
LangString DESC_section_gr ${LANG_ENGLISH} \
 "Associate .gr files with XVin in Windows explorer"

LangString DESC_subsec_inout ${LANG_ENGLISH} \
 "(inout.dll) To import/export binary and ascii files"
LangString DESC_subsec_random ${LANG_ENGLISH} \
 "(random.dll) To generate random number, follwing given distributions"
LangString DESC_subsec_hist ${LANG_ENGLISH} \
 "(hist.dll) To create histograms and extract information from them"


;Assign language strings to sections
 !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${section_core}	$(DESC_section_core)
    !insertmacro MUI_DESCRIPTION_TEXT ${section_allegro_lib} $(DESC_section_allegro_lib)
    !insertmacro MUI_DESCRIPTION_TEXT ${section_allegro_config} $(DESC_section_allegro_config)

    !insertmacro MUI_DESCRIPTION_TEXT ${section_plugins} $(DESC_section_plugins)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_inout} $(DESC_subsec_inout)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_random} $(DESC_subsec_random)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_hist} $(DESC_subsec_hist)
 
    !insertmacro MUI_DESCRIPTION_TEXT ${section_doc} $(DESC_section_doc)
    !insertmacro MUI_DESCRIPTION_TEXT ${section_all_lib_h} $(DESC_section_all_lib_h)
    !insertmacro MUI_DESCRIPTION_TEXT ${section_shortcuts} $(DESC_section_shortcuts)
    !insertmacro MUI_DESCRIPTION_TEXT ${section_gr}	$(DESC_section_gr)
 !insertmacro MUI_FUNCTION_DESCRIPTION_END



;--------------------------------
; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin"
  DeleteRegKey HKLM SOFTWARE\XVin

  ; Remove files and uninstaller
  Delete $INSTDIR\bin\arial.ttf
  Delete $INSTDIR\bin\fonts.dat
  Delete $INSTDIR\bin\example.h
  Delete $INSTDIR\bin\keyboard.dat
  Delete $INSTDIR\bin\language.dat
  Delete $INSTDIR\bin\xvin.exe
  Delete $INSTDIR\bin\xvin.dll
  Delete $INSTDIR\bin\xvin.cfg

  Delete $INSTDIR\bin\alleg41.dll
  Delete $INSTDIR\bin\alfont.dll

  Delete $INSTDIR\bin\setup.exe
  Delete $INSTDIR\bin\keyconf.exe
  Delete $INSTDIR\bin\keyconf.txt
  Delete $INSTDIR\bin\allegro.cfg

  Delete $INSTDIR\bin\random.dll
  Delete $INSTDIR\bin\hist.dll
  Delete $INSTDIR\bin\inout.dll

  Delete $INSTDIR\docs\html\*.* 
  Delete $INSTDIR\docs\rtf\*.*
  Delete $INSTDIR\docs\txt\*.*

  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\XVin\*.*"

  ; Remove directories used
  RMDir "$SMPROGRAMS\XVin"
  RMDir "$INSTDIR\bin"
  RMDir "$INSTDIR\docs\html"
  RMDir "$INSTDIR\docs\rtf"
  RMDir "$INSTDIR\docs\txt"
  RMDir "$INSTDIR\docs"
  RMDir "$INSTDIR"

 ;start of restore script
  !insertmacro APP_UNASSOCIATE "gr" "XVin.GRfile"

SectionEnd







