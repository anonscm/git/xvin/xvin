# XVin installer 
# 2004-02-10

# let's use the modern style, it is more user-friendly:
!include "MUI.nsh"
# let's include some function to register file extension into windows explorer:
!include "fileassoc.nsh"


# The name of the installer
Name "PicoTwist CVB"
Caption "PicoTwist installer for Win32"

# The file to create
OutFile "Picocvb_setup.exe"
!define MUI_ICON "..\icons\GR2.ICO"
!define MUI_UNICON "..\icons\GR2.ICO"
!define MUI_HEADERIMAGE  # Display an image on the header of the page.
!define MUI_HEADERIMAGE_BITMAP xvin_installer.bmp
!define MUI_HEADERIMAGE_UNBITMAP "xvin_installer.bmp"
!define MUI_BGCOLOR "FFFFFF"

XPStyle on
!define MUI_ABORTWARNING
!define MUI_FINISHPAGE_NOAUTOCLOSE	# wait after files copying is finished for user to be able to look at the log
!define MUI_UNFINISHPAGE_NOAUTOCLOSE	# idem, when not succesfull


; The default installation directory
InstallDir $PROGRAMFILES\XVin

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\XVin" "Install_Dir"


;--------------------------------
LicenseText "XVin license"
LicenseData "license.rtf"

;--------------------------------

; Pages

# !define MUI_PAGE_HEADER_TEXT "XVin"
# !define MUI_PAGE_HEADER_SUBTEXT "The Physicist's Friend" 
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "license.rtf"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

;--------------------------------
; installer sections

Section "PicoTwist core" section_core

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR\bin
  
  ; Put file there
  File "..\bin\arial.ttf"
  File "..\bin\fonts.dat"
  File "..\bin\keyboard.dat"
  File "..\bin\language.dat"
  File "..\bin\picocvb.exe"
  File "..\bin\CVCImg.dll"
  File "..\bin\CVCUtilities.dll" 
  File "..\bin\pico.exe"
  File "..\bin\xvin.dll"
  File "..\bin\xvin.exe"
  File "..\bin\cardinal.dll"
  File "..\bin\wlc.dll"
  File "..\bin\xvplot.dll"
  File "..\bin\nrutil.dll"
  File "..\bin\stat.dll"
  File "..\bin\XVcfg.dll"
 
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\XVin "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin" "DisplayName" "XVin"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin" "NoRepair" 1

  WriteUninstaller "uninstall.exe"
  
SectionEnd


Section "Allegro libraries" section_allegro_lib
  SetOutPath $INSTDIR\bin
  File "c:\WINDOWS\system32\alleg42.dll"
  File "..\bin\alfont.dll"
  File "..\bin\fftw3.dll"
  File "..\..\GnuWin32\bin\freetype6.dll"
  File "..\..\GnuWin32\bin\zlib1.dll"
SectionEnd

Section "Allegro configuration files" section_allegro_config
  SetOutPath $INSTDIR\bin
  File "..\bin\setup.exe"
#  File "..\bin\keyconf.exe"
  File "..\bin\keyconf.txt"
  File "..\bin\allegro.cfg"
  File "..\bin\bead000quater.gr"
SectionEnd


Section "Documentation" section_doc
  SetOutPath $INSTDIR\docs\html
  File "..\docs\html\*.*" 
  SetOutPath $INSTDIR\docs\rtf
  File "..\docs\rtf\*.*"
  SetOutPath $INSTDIR\docs\txt
  File "..\docs\txt\*.*"
  SetOutPath $INSTDIR\docs\chm
  File "..\docs\chm\*.*"
SectionEnd


Section "All lib and headers" section_all_lib_h
  SetOutPath $INSTDIR\lib
  File "..\lib\*.a"
  SetOutPath $INSTDIR\include
  File "..\include\*.h"
  SetOutPath $INSTDIR\include\xvin
  File "..\include\xvin\*.h"
#  SetOutPath $INSTDIR\include\plug-ins
#  File "..\include\plug-ins\*.h"
  SetOutPath $INSTDIR
  File "..\makefile"
  SetOutPath $INSTDIR
  File "..\win32.mk"
  File "..\linux.mk"
  File "..\macos.mk"
  File "..\installer\platform.mk"
SectionEnd


Section "Start Menu Shortcuts" section_shortcuts
  CreateDirectory "$SMPROGRAMS\XVin"
  CreateShortCut "$SMPROGRAMS\XVin\Picocvb (test version).lnk" "$INSTDIR\bin\picocvb.exe" "" "$INSTDIR\bin\picocvb.exe" 0  
  CreateShortCut "$SMPROGRAMS\XVin\XVin Help.lnk" "$INSTDIR\docs\html\xvin.html" "" "$INSTDIR\docs\html\xvin.html" 0
  CreateShortCut "$SMPROGRAMS\XVin\Pico (simulation version).lnk" "$INSTDIR\bin\pico.exe" "" "$INSTDIR\bin\pico.exe" 0  
  CreateShortCut "$SMPROGRAMS\XVin\Pico Software Help.lnk" "$INSTDIR\docs\html\picotw.chm" "" "$INSTDIR\docs\chm\picotw.chm" 0
  CreateShortCut "$SMPROGRAMS\XVin\Pico Hardware Help.lnk" "$INSTDIR\docs\html\picodv.chm" "" "$INSTDIR\docs\chm\picodv.chm" 0
  CreateShortCut "$SMPROGRAMS\XVin\Pico Wel Help.lnk" "$INSTDIR\docs\html\picobi.chm" "" "$INSTDIR\docs\chm\picobi.chm" 0
  CreateShortCut "$SMPROGRAMS\XVin\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
SectionEnd



Section "Associate file extensions" section_gr
  !insertmacro APP_ASSOCIATE "gr" "XVin.GRfile" "XVin gr file" "$INSTDIR\bin\xvin.exe,0" "Open with XVin" "$INSTDIR\bin\xvin.exe %1"
SectionEnd


;--------------------------------
;Descriptions

;Language strings
LangString DESC_section_core ${LANG_ENGLISH} \
 "XVin core binaries and configuration files (required)"
LangString DESC_section_allegro_lib ${LANG_ENGLISH} \
 "Allegro and Allegro Fonts dynamic libraries. They are required unless you have compiled them yourself"
LangString DESC_section_allegro_config ${LANG_ENGLISH} \
 "Allegro configuration files, usefull only if you want to tune the graphics and input (optional)"
LangString DESC_section_plugins ${LANG_ENGLISH} \
 "Installs selected plug'ins to add specialized functions to XVin"
LangString DESC_section_doc ${LANG_ENGLISH} \
 "Install XVin documentation in html, rtf, and txt formats"
LangString DESC_section_shortcuts ${LANG_ENGLISH} \
 "Add shortcurts to the Windows Start Menu"
LangString DESC_section_gr ${LANG_ENGLISH} \
 "Associate .gr files with XVin in Windows explorer"


  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${section_core}	$(DESC_section_core)
    !insertmacro MUI_DESCRIPTION_TEXT ${section_allegro_lib} $(DESC_section_allegro_lib)
    !insertmacro MUI_DESCRIPTION_TEXT ${section_allegro_config} $(DESC_section_allegro_config)

    !insertmacro MUI_DESCRIPTION_TEXT ${section_doc} $(DESC_section_doc)

    !insertmacro MUI_DESCRIPTION_TEXT ${section_shortcuts} $(DESC_section_shortcuts)
    !insertmacro MUI_DESCRIPTION_TEXT ${section_gr}	$(DESC_section_gr)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END



;--------------------------------
; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\XVin"
  DeleteRegKey HKLM SOFTWARE\XVin

  ; Remove files and uninstaller
  Delete $INSTDIR\bin\arial.ttf
  Delete $INSTDIR\bin\fonts.dat
  Delete $INSTDIR\bin\keyboard.dat
  Delete $INSTDIR\bin\language.dat
  Delete $INSTDIR\bin\picocvb.exe
#  Delete $INSTDIR\bin\xvin.dll
  Delete $INSTDIR\bin\xvin.cfg
  Delete $INSTDIR\bin\bead000quater.gr
  Delete $INSTDIR\bin\CVCImg.dll"
  Delete $INSTDIR\bin\CVCUtilities.dll"

  Delete $INSTDIR\bin\alleg42.dll
  Delete $INSTDIR\bin\alfont.dll
  Delete $INSTDIR\bin\\fftw3.dll
  Delete $INSTDIR\bin\freetype6.dll
  Delete $INSTDIR\bin\\zlib1.dll


  Delete $INSTDIR\bin\alfont.dll

  Delete $INSTDIR\bin\setup.exe
#  Delete $INSTDIR\bin\keyconf.exe
  Delete $INSTDIR\bin\keyconf.txt
  Delete $INSTDIR\bin\allegro.cfg

  Delete $INSTDIR\include\*.* 
  Delete $INSTDIR\include\xvin\*.* 
  Delete $INSTDIR\include\plug-ins\*.* 

  Delete $INSTDIR\lib\*.* 

  Delete $INSTDIR\docs\html\*.* 
  Delete $INSTDIR\docs\rtf\*.*
  Delete $INSTDIR\docs\txt\*.*
  Delete $INSTDIR\docs\chm\*.*
  Delete $INSTDIR\*.*

  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\XVin\*.*"

  ; Remove directories used
  RMDir "$SMPROGRAMS\XVin"
  RMDir "$INSTDIR\bin"

  RMDir "$INSTDIR\include\xvin" 
  RMDir "$INSTDIR\include\plug-ins" 
  RMDir "$INSTDIR\include" 

  RMDir "$INSTDIR\lib\*.*" 


  RMDir "$INSTDIR\docs\html"
  RMDir "$INSTDIR\docs\rtf"
  RMDir "$INSTDIR\docs\txt"
  RMDir "$INSTDIR\docs\chm"
  RMDir "$INSTDIR\docs"
  RMDir "$INSTDIR"

 ;start of restore script
  !insertmacro APP_UNASSOCIATE "gr" "XVin.GRfile"

SectionEnd







