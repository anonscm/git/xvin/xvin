# XVin plug'ins installer 
# 2005-11-03

# let's use the modern style, it is more user-friendly:
!include "MUI.nsh"

# The name of the installer
Name "XVin Plug'ins"
Caption "XVin ENS-Lyon Plug'ins installer for Win32"

# The file to create
OutFile "Xvin_plugins_ENSLyon_setup.exe"
!define MUI_ICON "..\icons\gr.ico"
!define MUI_UNICON "..\icons\gr.ico"
!define MUI_HEADERIMAGE  # Display an image on the header of the page.
!define MUI_HEADERIMAGE_BITMAP xvin_installer.bmp
!define MUI_HEADERIMAGE_UNBITMAP "xvin_installer.bmp"
!define MUI_BGCOLOR "FFFFFF"

XPStyle on
!define MUI_ABORTWARNING
!define MUI_FINISHPAGE_NOAUTOCLOSE	# wait after files copying is finished for user to be able to look at the log
!define MUI_UNFINISHPAGE_NOAUTOCLOSE	# idem, when not succesfull


; The default installation directory
InstallDir $PROGRAMFILES\XVin

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\XVin" "Install_Dir"


;--------------------------------
LicenseText "XVin license"
LicenseData ".\license.rtf"

;--------------------------------

; Pages

# !define MUI_PAGE_HEADER_TEXT "XVin"
# !define MUI_PAGE_HEADER_SUBTEXT "The Physicist's Friend" 
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

# !insertmacro MUI_UNPAGE_CONFIRM
# !insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

;--------------------------------
; installer sections

SubSection "Plug'ins" section_plugins

  Section "diff" subsec_diff
    SetOutPath $INSTDIR\bin
    File "..\bin\diff.dll"
  SectionEnd

  Section "response" subsec_response
    SetOutPath $INSTDIR\bin
    File "..\bin\response.dll"
  SectionEnd

  Section "resistance" subsec_resistance
    SetOutPath $INSTDIR\bin
    File "..\bin\resistance.dll"
  SectionEnd

  Section "turbulence" subsec_turbulence
    SetOutPath $INSTDIR\bin
    File "..\bin\turbulence.dll"
  SectionEnd

  Section "pendule" subsec_pendule
    SetOutPath $INSTDIR\bin
    File "..\bin\pendule.dll"
  SectionEnd

  Section "Jarzynski" subsec_Jarzynski
    SetOutPath $INSTDIR\bin
    File "..\bin\Jarzynski.dll"
  SectionEnd
  
  Section "Langevin" subsec_Langevin
    SetOutPath $INSTDIR\bin
    File "..\bin\Langevin.dll"
  SectionEnd
  
  Section "fits" subsec_fits
    SetOutPath $INSTDIR\bin
    File "..\bin\fits.dll"
  SectionEnd
  
  Section "graphs" subsec_graphs
    SetOutPath $INSTDIR\bin
    File "..\bin\graphs.dll"
  SectionEnd

  Section "function_generator" subsec_function_generator
    SetOutPath $INSTDIR\bin
    File "..\bin\function_generator.dll"
  SectionEnd  

SubSectionEnd



;--------------------------------
;Descriptions

;Language strings
LangString DESC_section_plugins ${LANG_ENGLISH} \
 "Installs selected plug'ins to add specialized functions to XVin"

LangString DESC_subsec_response ${LANG_ENGLISH} \
 "(diff.dll) To compute/apply response functions. This is required by 'response' 'resistance' and 'turbulence'"
LangString DESC_subsec_response ${LANG_ENGLISH} \
 "(response.dll) To compute/apply response functions. This is required by 'resistance' and 'turbulence'"
LangString DESC_subsec_resistance ${LANG_ENGLISH} \
 "(resistance.dll) functions related to the RC circuit"
LangString DESC_subsec_turbulence ${LANG_ENGLISH} \
 "(turbulence.dll) functions related with turbulence experiments"
LangString DESC_subsec_pendule ${LANG_ENGLISH} \
 "(pendule.dll) functions related with torsion pendulum experiment"
LangString DESC_subsec_Jarzynski ${LANG_ENGLISH} \
 "(Jarzynski.dll) Jarzynski relation applications"
LangString DESC_subsec_Langevin ${LANG_ENGLISH} \
 "(Langevin.dll) Langevin equation integration"
LangString DESC_subsec_fits ${LANG_ENGLISH} \
 "(fits.dll) fits an exponential relaxation"
LangString DESC_subsec_graphs ${LANG_ENGLISH} \
 "(fits.dll) graphs some functions connected with experiments"
LangString DESC_subsec_function_generator ${LANG_ENGLISH} \
 "(fits.dll) generate signals for NI-FGEN or other applications"
;Assign language strings to sections

  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN

    !insertmacro MUI_DESCRIPTION_TEXT ${section_plugins} $(DESC_section_plugins)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_diff} $(DESC_subsec_diff)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_response} $(DESC_subsec_response)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_resistance} $(DESC_subsec_resistance)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_turbulence} $(DESC_subsec_turbulence)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_pendule} $(DESC_subsec_pendule)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_Jarzynski} $(DESC_subsec_Jarzynski)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_Langevin} $(DESC_subsec_Langevin)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_fits} $(DESC_subsec_fits)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_graphs} $(DESC_subsec_graphs)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_function_generator} $(DESC_subsec_function_generator)

  !insertmacro MUI_FUNCTION_DESCRIPTION_END

