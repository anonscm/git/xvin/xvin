# XVin plug'ins installer 
# 2005-11-03

# let's use the modern style, it is more user-friendly:
!include "MUI.nsh"

# The name of the installer
Name "XVin Plug'ins"
Caption "XVin (very old-) Plug'ins installer for Win32"

# The file to create
OutFile "Xvin_plugins_old_setup.exe"
!define MUI_ICON "..\icons\gr.ico"
!define MUI_UNICON "..\icons\gr.ico"
!define MUI_HEADERIMAGE  # Display an image on the header of the page.
!define MUI_HEADERIMAGE_BITMAP xvin_installer.bmp
!define MUI_HEADERIMAGE_UNBITMAP "xvin_installer.bmp"
!define MUI_BGCOLOR "FFFFFF"

XPStyle on
!define MUI_ABORTWARNING
!define MUI_FINISHPAGE_NOAUTOCLOSE	# wait after files copying is finished for user to be able to look at the log
!define MUI_UNFINISHPAGE_NOAUTOCLOSE	# idem, when not succesfull


; The default installation directory
InstallDir $PROGRAMFILES\XVin

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\XVin" "Install_Dir"


;--------------------------------
LicenseText "XVin license"
LicenseData ".\license.rtf"

;--------------------------------

; Pages

# !define MUI_PAGE_HEADER_TEXT "XVin"
# !define MUI_PAGE_HEADER_SUBTEXT "The Physicist's Friend" 
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

# !insertmacro MUI_UNPAGE_CONFIRM
# !insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

;--------------------------------
; installer sections

SubSection "Plug'ins" section_plugins

  Section "Old numerical recipes" subsec_nrutil
    SetOutPath $INSTDIR\bin
  File "..\bin\nrutil.dll"    
  SectionEnd

  Section "Lotus experiment" subsec_lotus
    SetOutPath $INSTDIR\bin
    File "..\bin\lotus.dll"
    File "..\bin\ohy.dll"
  SectionEnd

SubSectionEnd



;--------------------------------
;Descriptions

;Language strings
LangString DESC_section_plugins ${LANG_ENGLISH} \
 "Installs selected plug'ins to add specialized functions to XVin"

LangString DESC_subsec_nrutil ${LANG_ENGLISH} \
 "(nrutil.dll) Old numerical recipes functions"
LangString DESC_subsec_lotus ${LANG_ENGLISH} \
 "(lotus.dll) Specialized functions for hydrothermal waves, including specials"

;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN

    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_nrutil} $(DESC_subsec_nrutil)
    !insertmacro MUI_DESCRIPTION_TEXT ${subsec_lotus} $(DESC_subsec_lotus)

  !insertmacro MUI_FUNCTION_DESCRIPTION_END

