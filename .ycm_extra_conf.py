import os
import ycm_core

flags = [
'-W',
'-Wall',
'-Wextra',
'-Wno-write-strings',
'-Wunused',
'-Wmaybe-uninitialized', '-fno-common', '-Wformat=2', '-Winit-self', '-Winline', '-Wpacked',
'-Wpointer-arith', '-Wmissing-format-attribute',
'-Wmissing-noreturn','-Wnested-externs', '-Wold-style-definition',
'-Wmissing-declarations', '-Wmissing-prototypes',
'-Wswitch-enum', '-Wundef',
'-Wunreachable-code',
'-Wmissing-include-dirs',
'-Wshadow', '-Wformat-security', '-Wcast-qual', '-Wparentheses',
'-Wsequence-point',
'-std=c++17',
'-DXV_GTK_DIALOG',
'-DXV_UNIX',
'-DOPENCL',
'-I',
'/home/fx/xvin/include',
'-I',
'/home/fx/xvin/build/include',
'-I',
'/home/fx/xvin/include/generated',
'-I',
'/home/fx/xvin/build/include',
'-I',
'/home/fx/xvin/include/xvin',
'-I',
'/home/fx/xvin/src/menus/plot/treat',
'-pthread',
'-fms-extensions',
'-I/usr/include/libftdi1',
'-I/usr/include/gtk-3.0',
'-I/usr/include/glib-2.0/',
'-I/usr/lib/glib-2.0/include/',
'-I/usr/include/at-spi2-atk/2.0',
'-I/usr/include/gio-unix-2.0/',
'-I/usr/include/cairo',
'-I/usr/include/pango-1.0',
'-I/usr/include/harfbuzz',
'-I/usr/include/atk-1.0',
'-I/usr/include/pixman-1',
'-I/usr/include/freetype2',
'-I/usr/include/libdrm',
'-I/usr/include/libpng16',
'-I/usr/include/gdk-pixbuf-2.0',
'-I/opt/pleora/ebus_sdk/RHEL-6-x86_64/include',
'-I/usr/include/eigen3'
]
# Set this to the absolute path to the folder (NOT the file!) containing the
# compile_commands.json file to use that instead of 'flags'. See here for
# more details: http://clang.llvm.org/docs/JSONCompilationDatabase.html
#
# Most projects will NOT need to set this to anything; you can just change the
# 'flags' list of compilation flags. Notice that YCM itself uses that approach.
compilation_database_folder = ''

if compilation_database_folder:
  database = ycm_core.CompilationDatabase( compilation_database_folder )
else:
  database = None

SOURCE_EXTENSIONS = [  
    '.cc',
    '.c',
    '.cpp',
    '.cxx',
    '.m',
    '.mm'
]
HEADER_EXTENSIONS = [  
    '.h',
    '.hh',
    '.hxx',
    '.hpp',
]
def IsHeaderFile(filename):  
    extension = os.path.splitext(filename)[1]
    return extension in HEADER_EXTENSIONS

def DirectoryOfThisScript():
  return os.path.dirname( os.path.abspath( __file__ ) )

def GetCompilationInfoForFile(database, filename):  
    if IsHeaderFile(filename):
        basename = os.path.splitext(filename)[0]
        for extension in SOURCE_EXTENSIONS:
            replacement_file = basename + extension
            if os.path.exists(replacement_file):
                compilation_info = database.GetCompilationInfoForFile(replacement_file)
                if compilation_info.compiler_flags_:
                    return compilation_info
        return None
    return database.GetCompilationInfoForFile(filename)

def MakeRelativePathsInFlagsAbsolute( flags, working_directory ):
  if not working_directory:
    return list( flags )
  new_flags = []
  make_next_absolute = False
  path_flags = [ '-isystem', '-I', '-iquote', '--sysroot=' ]
  for flag in flags:
    new_flag = flag

    if make_next_absolute:
      make_next_absolute = False
      if not flag.startswith( '/' ):
        new_flag = os.path.join( working_directory, flag )

    for path_flag in path_flags:
      if flag == path_flag:
        make_next_absolute = True
        break

      if flag.startswith( path_flag ):
        path = flag[ len( path_flag ): ]
        new_flag = path_flag + os.path.join( working_directory, path )
        break

    if new_flag:
      new_flags.append( new_flag )
  return new_flags

def GetIncludeTemplateDeclaration( filename ):
    root, ext = os.path.splitext( filename )
    if ext == '.hxx':
        return [ '-include', root + '.hh' ]
    return []

def FlagsForFile( filename ):
  if database:
    # Bear in mind that compilation_info.compiler_flags_ does NOT return a
    # python list, but a "list-like" StringVec object
    compilation_info = database.GetCompilationInfoForFile( filename )
    final_flags = MakeRelativePathsInFlagsAbsolute(
      compilation_info.compiler_flags_,
      compilation_info.compiler_working_dir_ )

    # NOTE: This is just for YouCompleteMe; it's highly likely that your project
    # does NOT need to remove the stdlib flag. DO NOT USE THIS IN YOUR
    # ycm_extra_conf IF YOU'RE NOT 100% YOU NEED IT.
    try:
      final_flags.remove( '-stdlib=libc++' )
    except ValueError:
      pass
  else:
    relative_to = DirectoryOfThisScript()
    final_flags = MakeRelativePathsInFlagsAbsolute( flags, relative_to )

  final_flags.extend( GetIncludeTemplateDeclaration( filename ) )

  return {
    'flags': final_flags,
    'do_cache': True
  }
