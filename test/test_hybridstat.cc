#include <sstream>
#include <boost/math/distributions/normal.hpp>
#include "cordrift_core.h"
#include "hybridstat_cluster.h"
#include "hybridstat_core.h"
#include "xvin/plot_ds.h"
#include <vector>
#include <numeric>

#define BOOST_TEST_MODULE hybridstat
#include <boost/test/included/unit_test.hpp>
#include <numeric>

// Have to make it a macro so that it reports exact line numbers when checks fail.
#define CHECK_CLOSE_COLLECTION(_a,ae,_b,be,tolerance)                    \
    {                                                                    \
        auto a = _a; auto b = _b;                                        \
        BOOST_REQUIRE_EQUAL(std::distance(a, ae), std::distance(b, be)); \
        for(; a != ae; ++a, ++b)                                         \
        { BOOST_CHECK_CLOSE(*a, *b, tolerance); }                        \
    }
using data_t = std::remove_pointer<decltype(data_set::xd)>::type;
namespace bm = boost::math;

BOOST_AUTO_TEST_SUITE(test_intervals)
    BOOST_AUTO_TEST_CASE(simple)
    {
        using namespace cordrift;
        intervals::Args cf;
        cf.bfilter      = 0;
        cf.dn           = 1;
        cf.uncertainty  = .5;
        cf.confidence   = bm::cdf(bm::normal(0., .5), -std::sqrt(1.5))*2.;
        cf.nei          = 0;
        cf.nmin         = 3;
        data_t vect[] = { 100., 100., 100., 100., 100.,         // 5
                          99., 98., 97., 96., 95., 94., 93.,    // 2
                          90., 90., 90., 90., 90., 90., 90.,    // 9
                          80., 80., 80., 80., 80.,              // 24
                          79., 78., 77., 76., 75., 74., 73.,    // 31
                          74., 74.5, 74.7, 74.8, 73., 73.,      // 37
                          50., 50., 50., 50.
                       };
        Data ds;
        ds.nx = sizeof(vect)/sizeof(float);
        ds.yd = vect;
        auto ans = intervals::run(cf, std::numeric_limits<size_t>::max()
                                    , std::numeric_limits<size_t>::max()
                                    , ds);


        intervals::rng_t truth[] = {{0,5}, {12,19}, {19,24}, {31,35}, {37, 40}};
        const size_t sz = sizeof(truth)/sizeof(intervals::rng_t);

        BOOST_CHECK_EQUAL(sz, ans.size());
        if(ans.size() == sz)
            for(auto i = 0u; i < ans.size(); ++i)
            {
                BOOST_CHECK_EQUAL(truth[i].first,  ans[i].first);
                BOOST_CHECK_EQUAL(truth[i].second, ans[i].second);
            }
    }

    BOOST_AUTO_TEST_CASE(merge)
    {
        using namespace cordrift;
        intervals::Args cf;
        cf.bfilter      = 0;
        cf.dn           = 1;
        cf.uncertainty  = .1;
        cf.confidence   = bm::cdf(bm::normal(0., .1), -std::sqrt(1.5))*2.;
        cf.nei          = 0;
        cf.nmin         = 3;
        data_t vect[] = { 100., 100., 100., 100., 100.,         // 5
                          99., 98., 97., 96., 95., 94., 93.,    // 2
                          90., 90., 90., 90., 90., 90., 90.,    // 12
                          88.1, 90., 90., 90., 90.,             // 17
                          50., 50., 50., 50.
                       };
        Data ds;
        ds.nx = sizeof(vect)/sizeof(float);
        ds.yd = vect;
        auto ans = intervals::run(cf, std::numeric_limits<size_t>::max()
                                    , std::numeric_limits<size_t>::max()
                                    , ds);

        intervals::rng_t truth[] = {{0,5}, {12,24}, {24,27}};
        const size_t sz = sizeof(truth)/sizeof(intervals::rng_t);

        BOOST_CHECK_EQUAL(sz, ans.size());
        if(ans.size() == sz)
            for(auto i = 0u; i < ans.size(); ++i)
            {
                BOOST_CHECK_EQUAL(truth[i].first,  ans[i].first);
                BOOST_CHECK_EQUAL(truth[i].second, ans[i].second);
            }
    }

    BOOST_AUTO_TEST_CASE(clipping)
    {
        using namespace cordrift;
        intervals::Args cf;
        cf.bfilter      = 0;
        cf.confidence   = bm::cdf(bm::normal(0., .1), -std::sqrt(1.5))*2.;
        cf.dn           = 1;
        cf.uncertainty  = .1;
        cf.nei          = 0;
        cf.nmin         = 3;
        data_t vect[] = { 100., 100., 100., 100., 100.,         // 5
                          99., 98., 97., 96., 95., 94., 93.,    // 2
                          89.5, 89.7, 89.9, 90., 90., 90., 90., // 12
                          90, 90., 89.9, 89.7, 89.5,            // 17
                          50., 50., 50., 50.
                       };
        Data ds;
        ds.nx = sizeof(vect)/sizeof(float);
        ds.yd = vect;
        auto ans = intervals::run(cf, std::numeric_limits<size_t>::max()
                                    , std::numeric_limits<size_t>::max()
                                    , ds);

        intervals::rng_t truth[] = {{0,5}, {14,22}, {24,27}};
        const size_t sz = sizeof(truth)/sizeof(intervals::rng_t);

        BOOST_CHECK_EQUAL(sz, ans.size());
        if(ans.size() == sz)
            for(auto i = 0u; i < ans.size(); ++i)
            {
                BOOST_CHECK_EQUAL(truth[i].first,  ans[i].first);
                BOOST_CHECK_EQUAL(truth[i].second, ans[i].second);
            }
    }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(test_collapse)
    BOOST_AUTO_TEST_CASE(horizontal)
    {
        using namespace cordrift;
        collapse::Args cf;
        cf.nsmear  = 0;
        cf.nmin    = 1;

        data_t ind[10]; std::iota(ind,ind+10, 0);
        data_t ys[] = { 20., 20., 20., 20., 20.,                // 5
                        10., 10., 10., 10., 10., 10.,  10.,     // 19
                        1.,  1.,  1.,  1.,  1.,  1.,           // 31
                      };

        DataSet ds(3);
        ds[0].xd = ind+2;  ds[1].xd = ind;  ds[2].xd = ind+3;
        ds[0].yd = ys;     ds[1].yd = ys+5; ds[2].yd = ys+12;
        ds[0].nx = 5;      ds[1].nx = 7;    ds[2].nx = 6;

        auto res = collapse::run(cf, ds);
        {
            decltype(res.profile) truth(0., 9);
            BOOST_CHECK_EQUAL_COLLECTIONS(std::begin(truth),       std::end(truth),
                                          std::begin(res.profile), std::end(res.profile));
        }
        {
            decltype(res.counts) truth = {1,1,2,3,3,3,3,1,1};
            BOOST_CHECK_EQUAL_COLLECTIONS(std::begin(truth),      std::end(truth),
                                          std::begin(res.counts), std::end(res.counts));
        }
    }

    BOOST_AUTO_TEST_CASE(slanted)
    {
        using namespace cordrift;
        collapse::Args cf;
        cf.nsmear  = 0;
        cf.nmin    = 1;

        size_t const nds = 5;
        data_t ind[25]; std::iota(ind,ind+25, 0);
        data_t ys[10*nds];
        DataSet ds(5);

        ds[0].xd = ind+5;  ds[1].xd = ind;    ds[2].xd = ind+2;  ds[3].xd = ind+6;
        ds[0].yd = ys;     ds[1].yd = ys+10;  ds[2].yd = ys+20;  ds[3].yd = ys+30;
        ds[0].nx = 5;      ds[1].nx = 4;      ds[2].nx = 7;      ds[3].nx = 6;

        ds[4].xd = ind+13;
        ds[4].yd = ys+40;
        ds[4].nx = 3;

        for(size_t k = 0u; k < nds; ++k)
            for(size_t i = 0; i < ds[k].nx; ++i)
                ds[k].yd[i] = k*10.+100.-i*5;

        auto res = collapse::run(cf, ds);

        {
            decltype(res.profile) truth = {65,  60., 55., 50., 45., 40.,
                                           35., 30., 25., 20., 15., 10.,
                                           10., 10.,  5.,  0.};
            BOOST_CHECK_EQUAL_COLLECTIONS(std::begin(truth),       std::end(truth),
                                          std::begin(res.profile), std::end(res.profile));
        }
        {
            decltype(res.counts) truth = {1,1,2,2,1,2,3,3,3,2,1,1,
                                          0,1,1,1};
            BOOST_CHECK_EQUAL_COLLECTIONS(std::begin(truth),      std::end(truth),
                                          std::begin(res.counts), std::end(res.counts));
        }
    }
BOOST_AUTO_TEST_SUITE_END()

using namespace hybridstat;
namespace
{
    using _v = std::vector<std::pair<float, float>>;

    auto _inp(_v && x)
    {
        hybridstat::theoretical::Peaks y;
        static_cast<_v&>(y) = x;
        return y;
    }
}

namespace std
{
    template <typename T0>
    inline decltype(auto) operator << (T0 & stream, zeroclip::Results const & res)
    {
        char tmp [2048] = "";
        snprintf(tmp, sizeof(tmp), "{zero = {%f, %f}, max = {%f, %f}}",
                 res.zero.first, res.zero.second,
                 res.max.first, res.max.second);
        return (stream << tmp);
    }

    template <typename T>
    inline std::ostream & operator << (std::ostream & stream, std::pair<T,T> const & res)
    {
        std::ostringstream s;
        s << "{"  <<  res.first << ", " << res.second << "}";
        return (stream << s.str());
    }

    bool equal(zeroclip::Results const & a, zeroclip::Results const & b)
    { return a.zero == b.zero && a.max == b.max; }
}

BOOST_AUTO_TEST_SUITE(test_histogram)
    BOOST_AUTO_TEST_CASE(simple)
    {
        float yd[] = {20,20,20,20,20,-20,-20,-20};
        d_s items[2];
        items[0].nx = 5;  items[1].nx = 3;
        items[0].ny = 5;  items[1].ny = 3;
        items[0].yd = yd; items[1].yd = yd+5;

        std::vector<d_s const *> dt = {items, items+1};
        auto cf  = hybridstat::histogram::square(1.); cf.maxabs = 1e5;
        auto res = hybridstat::histogram::run(cf, dt);
        BOOST_CHECK_CLOSE(-20., res.ymin, 1e-5);
        BOOST_CHECK_CLOSE( 20., res.ymax, 1e-5);

        std::vector<float> truth(40*5+1);
        for(size_t i = 0lu; i < 11lu; ++i)
            truth[i] = truth[truth.size()-i-1] = 1;
        BOOST_CHECK_EQUAL_COLLECTIONS(truth.begin(),    truth.end(),
                                      res.data.begin(), res.data.end());

        cf  = hybridstat::histogram::timesquare(1.); cf.maxabs = 1e5;
        res = hybridstat::histogram::run(cf, dt);
        for(size_t i = 0lu; i < 11lu; ++i)
        { truth[i] = 3.; truth[truth.size()-i-1] = 5; }
        BOOST_CHECK_EQUAL_COLLECTIONS(truth.begin(),    truth.end(),
                                      res.data.begin(), res.data.end());
    }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(test_zeroclip)
    BOOST_AUTO_TEST_CASE(two_max_in_zero_space)
    {
        float            ys [] = { 1., 1., 1., 1., 2., 1., 3., 1., 1., 1., 1., 1., 1.5, 1.};
        constexpr size_t n     = sizeof(ys)/sizeof(decltype(ys[0]));
        float            xs [n];
        std::iota(xs, xs+n, -4.);

        auto          cf    = zeroclip::Args{1.,3.}; cf.ninc = 1;
        auto          res   = zeroclip::compute(cf, n, xs, ys);
        decltype(res) truth = {{3.,2.},{2.,0.}};
        BOOST_CHECK_EQUAL(truth, res);

        std::vector<float> ytruth (ys, ys+n); ytruth[6] = 2.f*1.1f;
        cf = zeroclip::args(1.); cf.ninc = 1;
        BOOST_CHECK(zeroclip::apply(cf, res, n, ys));
        CHECK_CLOSE_COLLECTION(ytruth.begin(), ytruth.end(), ys, ys+n, 1e-5);
    }

    BOOST_AUTO_TEST_CASE(max_out_of_zero_space)
    {
        float            ys [] = { 1., 1., 1., 1., 2., 1., 3., 1., 1., 1., 1., 1., 2.91, 1.};
        constexpr size_t n     = sizeof(ys)/sizeof(decltype(ys[0]));
        float            xs [n];
        std::iota(xs, xs+n, -4.);

        auto          cf    = zeroclip::Args{1.,3.}; cf.ninc = 1;
        auto          res   = zeroclip::compute(cf, n, xs, ys);
        decltype(res) truth = {{3.,2.},{2.91,xs[n-2]}};
        BOOST_CHECK_EQUAL(truth, res);

        std::vector<float> ytruth (ys, ys+n);
        cf = zeroclip::args(1.); cf.ninc = 1;
        BOOST_CHECK(!zeroclip::apply(cf, res, n, ys));
        BOOST_CHECK_EQUAL_COLLECTIONS(ytruth.begin(), ytruth.end(), ys, ys+n);
    }

    BOOST_AUTO_TEST_CASE(default_case)
    {
        float            ys [] = {1., 4., 1., 1., 1., 5., 1., 1., 1., 1.5, 1., 1., 1., 2., 1.};
        constexpr size_t n     = sizeof(ys)/sizeof(decltype(ys[0]));
        float            xs [n];
        std::iota(xs, xs+n, -5.);

        auto cf  = zeroclip::Args{1.,3.}; cf.ninc = 1;
        auto res = zeroclip::compute(cf, n, xs, ys);
        decltype(res) truth = {{5.,0.},{4.,xs[1]}};
        BOOST_CHECK_EQUAL(truth, res);

        std::vector<float> ytruth (ys, ys+n); ytruth[5] = 4.f*1.1f;
        cf = zeroclip::args(1.); cf.ninc = 1;
        BOOST_CHECK(zeroclip::apply(cf, res, n, ys));
        CHECK_CLOSE_COLLECTION(ytruth.begin(), ytruth.end(), ys, ys+n, 1e-5);
    }
BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(test_probability)
    BOOST_AUTO_TEST_CASE(simple)
    {
        using namespace hybridstat::probability;
        Probability prob(10);
        for(auto i = 0; i < 111; ++i) prob(i, i >= 111);
        for(auto i = 111; i < 121; ++i) prob(111, i >= 111);

        BOOST_CHECK_EQUAL(true, prob.good());
        auto p = (1.-10./111.)/(1.-10/111.-10.+7170./111.);
        BOOST_CHECK_CLOSE(p, prob.probability(), 1e-4);
        BOOST_CHECK_CLOSE(p/(1.-10./111.)*std::sqrt(1.-p), prob.stddev(), 1e-4);
    }

    BOOST_AUTO_TEST_CASE(peaks)
    {
        using namespace hybridstat::probability;
        PeakProbabilities prob(0, 1.);
        prob.peak(10., 14.);
        prob.peak(20., 24.);
        prob.peak(30., 34.);
        prob.peak(40., 44.);

        for(auto i = 10; i < 20; ++i)
            for(auto j = 20; j < 60; j += 10)
            {
                std::vector<float> ys(3*i, j);
                for(auto k = 0; k <= 2*i; ++k)
                    ys[i] = -30;
                ys[ys.size()-1] = 300;
                prob.event(&ys[0], 2*i, ys.size(), 100);
            }

        auto res = prob.results(1.);
        BOOST_CHECK_EQUAL(4, res.size());
        BOOST_CHECK_CLOSE(10., res[0].xp, 1e-4);
        BOOST_CHECK_CLOSE(20., res[1].xp, 1e-4);
        BOOST_CHECK_CLOSE(30., res[2].xp, 1e-4);
        BOOST_CHECK_CLOSE(40., res[3].xp, 1e-4);
        BOOST_CHECK_CLOSE(14., res[0].yp, 1e-4);
        BOOST_CHECK_CLOSE(24., res[1].yp, 1e-4);
        BOOST_CHECK_CLOSE(34., res[2].yp, 1e-4);
        BOOST_CHECK_CLOSE(44., res[3].yp, 1e-4);
        BOOST_CHECK_EQUAL(false, res[0].good);
        BOOST_CHECK_EQUAL(true,  res[1].good);
        BOOST_CHECK_EQUAL(true,  res[2].good);
        BOOST_CHECK_EQUAL(true,  res[3].good);
        BOOST_CHECK_EQUAL(0,     res[0].events.size());
        BOOST_CHECK_EQUAL(10,    res[1].events.size());
        BOOST_CHECK_EQUAL(10,    res[2].events.size());
        BOOST_CHECK_EQUAL(10,    res[3].events.size());

        BOOST_CHECK_EQUAL(20,    res[1].events[0].xp);
        BOOST_CHECK_EQUAL(10,    res[1].events[0].yp);
        BOOST_CHECK_EQUAL(20,    res[1].events[9].xp);
        BOOST_CHECK_EQUAL(19,    res[1].events[9].yp);
    }
BOOST_AUTO_TEST_SUITE_END()

struct ClusterData: public std::map<int, hybridstat::distance::Input>
{
    ClusterData()
    {
        using namespace hybridstat::distance;
        emplace(1, _inp(_v{{0.00134628,31.},{0.0551621,27.},{0.0954151,36.},{0.180882,37.},{0.349378,2.},{0.405596,63.},{0.496554,3.},{0.548055,86.},{0.597451,67.1321},{0.693351,22.},{0.742138,2.},{0.793773,76.},{0.923189,6.}}));
        emplace(4, _inp(_v{{0.00659454,106.},{0.115112,63.},{0.19915,3.47009},{0.234921,15.},{0.315051,2.},{0.370023,45.},{0.496568,38.},{0.594551,37.},{0.70085,83.},{0.753951,16.},{0.81067,63.7699},{0.927225,6.}}));
        emplace(5, _inp(_v{{0.00356164,97.},{0.12087,57.4036},{0.225997,82.},{0.31503,55.},{0.391744,2.},{0.469678,66.},{0.511931,2.},{0.569605,80.},{0.706712,83.},{0.78473,65.}}));
        emplace(6, _inp(_v{{0.00129597,113.},{0.104107,67.},{0.227358,19.},{0.334623,30.},{0.402953,10.},{0.613097,13.},{0.647758,4.},{0.755808,16.},{0.800184,32.},{0.842907,6.}}));
        emplace(7, _inp(_v{{0.0021277,81.},{0.0564805,16.},{0.0958018,74.},{0.178815,47.848},{0.410139,93.49},{0.540599,33.},{0.604999,83.},{0.704434,15.},{0.805278,77.},{0.932458,6.}}));
        emplace(8, _inp(_v{{0.00186084,72.},{0.0555447,2.},{0.13349,48.},{0.242248,62.},{0.34201,45.1582},{0.427251,2.},{0.502688,55.},{0.607989,65.},{0.737686,71.},{0.820134,55.},{0.935178,5.}}));
        emplace(11, _inp(_v{{0.00327968,108.},{0.106427,51.},{0.240545,4.},{0.36922,34.},{0.502796,6.},{0.604492,17.},{0.705018,73.},{0.796038,59.},{0.938958,6.}}));
        emplace(13, _inp(_v{{0.00182014,55.},{0.0958406,42.},{0.20257,8.},{0.284441,24.},{0.359054,3.},{0.558117,10.},{0.710961,12.},{0.756203,35.}}));
        emplace(14, _inp(_v{{0.00249372,108.523},{0.125048,66.},{0.243705,20.},{0.388132,46.6581},{0.448386,3.},{0.513449,34.},{0.616577,39.},{0.718515,85.},{0.768813,16.},{0.826614,66.},{0.935621,6.}}));
        emplace(16, _inp(_v{{0.0054161,85.},{0.0610615,4.},{0.130641,59.},{0.234331,80.},{0.329453,52.},{0.388557,101.},{0.492095,73.},{0.604392,83.},{0.739185,75.},{0.825917,70.},{0.937245,6.}}));
        emplace(17, _inp(_v{{0.00169885,109.},{0.113714,63.},{0.234529,34.3956},{0.373873,47.},{0.506946,36.},{0.607097,48.1116},{0.720877,74.},{0.772828,23.},{0.826164,71.},{0.960036,6.}}));
        emplace(19, _inp(_v{{0.00275975,81.},{0.113334,52.},{0.225044,71.},{0.319919,52.},{0.396948,3.},{0.487665,57.},{0.597929,78.},{0.73433,76.6339},{0.82032,57.},{0.935721,5.}}));
        emplace(22, _inp(_v{{0.00231307,88.},{0.060716,2.},{0.144735,52.},{0.190046,3.},{0.268918,87.},{0.379238,68.},{0.479289,2.},{0.546819,59.2732},{0.653209,64.9185},{0.777145,68.046},{0.851059,49.},{0.951652,6.}}));
        emplace(24, _inp(_v{{0.00324372,107.},{0.0338479,63.2837},{0.0810352,91.},{0.203261,8.},{0.27899,24.},{0.557882,13.},{0.698312,14.},{0.736035,54.5482},{0.883505,6.}}));
        emplace(25, _inp(_v{{0.0025451,83.},{0.114416,74.},{0.190588,49.},{0.405093,87.},{0.616622,78.},{0.711711,12.},{0.829067,73.},{0.963881,6.}}));
        emplace(26, _inp(_v{{0.00267714,63.},{0.115532,40.},{0.249018,104.},{0.327798,58.7168},{0.383369,4.},{0.402067,3.},{0.503353,60.},{0.622005,80.},{0.679214,3.},{0.773045,79.},{0.854424,57.},{0.980193,6.}}));
        emplace(28, _inp(_v{{0.00442414,86.},{0.116225,632.513},{0.236948,39.1932},{0.275223,16.6689},{0.34582,71.6292},{0.419477,17.},{0.505807,3.},{0.555689,17.5637},{0.638827,53.8672},{0.700267,2.},{0.792158,43.7983},{0.83491,74.},{0.944578,7.}}));
        emplace(31, _inp(_v{{0.00358592,64.},{0.129347,40.},{0.213033,2.},{0.254978,12.},{0.418196,40.9744},{0.55395,33.},{0.654914,19.3846},{0.764435,57.},{0.819521,21.},{0.87455,53.}}));
        emplace(34, _inp(_v{{0.00238618,56.},{0.116337,54.7158},{0.187083,33.},{0.415457,61.},{0.612405,51.},{0.699726,4.},{0.806521,34.}}));
        emplace(35, _inp(_v{{0.00160969,106.},{0.099906,95.},{0.240064,23.},{0.330126,50.},{0.418707,5.},{0.635054,28.},{0.793753,57.},{0.826339,68.4511}}));
        emplace(39, _inp(_v{{-0.0625927,7.},{0.00282907,98.},{0.0519014,4.},{0.12342,43.0649},{0.165767,4.},{0.227162,17.1936},{0.324605,7.},{0.36333,26.1096},{0.393218,29.1656},{0.502967,16.2911},{0.532505,25.},{0.624998,26.7244},{0.651012,25.},{0.755233,29.},{0.784226,43.6615},{0.852459,17.},{0.889052,29.},{0.916449,39.1004},{1.03004,5.75653}}));
        emplace(41, _inp(_v{{0.00717058,195.006},{0.107817,77.4671},{0.241852,17.4846},{0.341975,41.},{0.396009,2.},{0.502925,2.},{0.556951,2.},{0.625252,26.1846},{0.78467,26.},{0.831159,53.},{0.987629,6.}}));
        emplace(45, _inp(_v{{0.00244674,105.},{0.0608742,2.},{0.111305,100.},{0.245149,25.},{0.348581,61.},{0.429599,12.3107},{0.652396,34.},{0.687881,2.},{0.795433,37.},{0.839696,70.},{0.956265,6.}}));
        emplace(47, _inp(_v{{0.00221417,101.},{0.113109,97.},{0.244944,24.},{0.284248,4.},{0.340147,56.},{0.425562,6.},{0.551535,3.},{0.644361,32.},{0.785734,33.},{0.830119,75.},{0.947961,6.}}));
        emplace(48, _inp(_v{{0.00386486,23.7119},{0.105384,19.},{0.207075,39.},{0.28695,16.},{0.44838,28.},{0.56252,22.},{0.721527,24.},{0.814878,36.}}));
        emplace(49, _inp(_v{{0.00261692,112.},{0.0589087,3.},{0.102523,63.},{0.229407,27.},{0.301301,3.},{0.374629,46.},{0.436888,3.},{0.528615,35.},{0.644766,34.},{0.770665,75.4257},{0.824209,15.},{0.885049,67.},{1.02709,6.}}));
        emplace(53, _inp(_v{{0.00162538,84.},{0.114476,51.},{0.238069,8.},{0.362071,32.},{0.481691,16.},{0.588816,21.},{0.701255,66.},{0.798098,49.}}));
        emplace(54, _inp(_v{{0.00152182,106.},{0.0928946,62.},{0.216385,7.},{0.340735,42.},{0.412121,2.},{0.459561,19.},{0.567154,18.},{0.673516,80.},{0.779438,62.},{0.90377,6.}}));
        emplace(55, _inp(_v{{0.0104066,177.31},{0.0457879,126.893},{0.110682,53.5175},{0.151806,25.0448},{0.215921,72.},{0.243155,63.2809},{0.289416,51.1505},{0.38952,10.},{0.44504,34.3385},{0.497043,22.},{0.548024,51.8463},{0.611166,24.2752},{0.686097,44.7603},{0.774354,42.6813},{0.85621,11.},{0.907927,14.},{0.972876,2.},{1.01679,3.}}));
    }
};

BOOST_AUTO_TEST_SUITE(test_clustering)
    BOOST_AUTO_TEST_CASE(measure)
    {
        using namespace hybridstat::distance;
        auto beads = ClusterData();

        auto r = convolve(0.015, 1., 0., beads[1], beads[1]);
        BOOST_CHECK_SMALL(r.val,     1e-4);
        BOOST_CHECK_SMALL(r.grad[0], 1e-4);
        BOOST_CHECK_SMALL(r.grad[1], 1e-4);

        auto r1 = convolve(0.015, .5,      .55555, beads[1], beads[1]);
        auto r2 = convolve(0.015, .5-.001, .55555, beads[1], beads[1]);
        auto r3 = convolve(0.015, .5,      .55555-.001, beads[1], beads[1]);
        BOOST_CHECK_CLOSE(r1.val-r2.val, .5e-3*(r1.grad[0]+r2.grad[0]), 1e-2);
        BOOST_CHECK_CLOSE(r1.val-r3.val, .5e-3*(r1.grad[1]+r3.grad[1]), 1e0);

        r = convolve(0.015, 1., 0., beads[31], beads[31]);
        BOOST_CHECK_SMALL(r.val,     1e-4);
        BOOST_CHECK_SMALL(r.grad[0], 1e-4);
        BOOST_CHECK_SMALL(r.grad[1], 1e-4);
    }

    BOOST_AUTO_TEST_CASE(distance)
    {
        using namespace hybridstat::distance;
        auto beads = ClusterData();
        Args args;

        auto r     = hybridstat::distance::run(args, beads[1], beads[1]);
        BOOST_CHECK_SMALL(r.dist, 1e-5f);
        BOOST_CHECK_CLOSE(1.f, r.dyn,  1e-5f);
        BOOST_CHECK_SMALL(r.bias, 1e-5f);

        r     = hybridstat::distance::run(args, beads[31], beads[31]);
        BOOST_CHECK_SMALL(r.dist, 1e-5f);
        BOOST_CHECK_CLOSE(1.f, r.dyn,  1e-5f);
        BOOST_CHECK_SMALL(r.bias, 1e-5f);

        r       = hybridstat::distance::run(args, beads[53], beads[31]);
        auto r2 = hybridstat::distance::run(args, beads[31], beads[53]);
        auto r3 = hybridstat::distance::convert(53, 31, r2);
        auto r4 = hybridstat::distance::convert(31, 53, r2);
        BOOST_CHECK_CLOSE(r2.dist, r4.dist, 1e-5f);
        BOOST_CHECK_CLOSE(r2.dyn,  r4.dyn,  1e-2f);
        BOOST_CHECK_CLOSE(r2.bias, r4.bias, 1e-2f);
        BOOST_CHECK_CLOSE(r.dist,  r3.dist, 1e-5f);
        BOOST_CHECK_CLOSE(r.dyn,   r3.dyn,  1e-2f);
        BOOST_CHECK_CLOSE(r.bias,  r3.bias, 1e-2f);

        hybridstat::distance::Input b1,b2;
        for(auto const & v: beads[31])
        {
            b1.push_back({v.first*1.05-0.005, v.second});
            b2.push_back({v.first*.95+0.005,  v.second});
        }
        r  = hybridstat::distance::run(args, beads[31], beads[31]);
        r2 = hybridstat::distance::run(args, b1, b2);
        BOOST_CHECK_SMALL(r2.dist, 1e-3f);
        BOOST_CHECK_CLOSE(1.05/.95,  r2.dyn, 1e-3f);
        BOOST_CHECK_CLOSE(-2.*.01/.95, r2.bias, 1e-2f);
    }

    BOOST_AUTO_TEST_CASE(cluster)
    {
        using namespace hybridstat::cluster;
        using hybridstat::distance::Result;
        Input dist;
        float square [5][5] = {{ 1.,  2., .03,  4.,  4.},
                               { 2.,  1., .02,  4.,  4.},
                               {.03, .02, .01, .04, .05},
                               { 4.,  4., .04,  1.,  4.},
                               { 4.,  4., .05,  4.,  1.}};

        for(size_t i = 0;  i < 15; ++i)
        {
            size_t e = i-(i%5)+5;
            for(size_t j = i;  j < e; ++j)
                dist.emplace(std::make_pair(i,j),
                             Result{square[i%5][j%5], i*1.f, j*1.f});
            for(size_t j = e;  j < 15; ++j)
                dist.emplace(std::make_pair(i,j),
                             Result{100.f, i*1.f, j*1.f});
        }

        Args args = {3};
        auto r = run(args, dist);
        BOOST_CHECK_EQUAL(3,  r.groups.size());
        BOOST_CHECK_EQUAL(2,  r.groups[0].first);
        BOOST_CHECK_EQUAL(7,  r.groups[1].first);
        BOOST_CHECK_EQUAL(12, r.groups[2].first);
        BOOST_CHECK_EQUAL(5,  r.groups[0].second.size());
        BOOST_CHECK_EQUAL(5,  r.groups[1].second.size());
        BOOST_CHECK_EQUAL(5,  r.groups[2].second.size());

        std::vector<size_t> truth = {2,1,0,3,4};
        std::vector<size_t> vals;
        for(auto const & x: r.groups[0].second) vals.push_back(x.id);
        BOOST_CHECK_EQUAL_COLLECTIONS(truth.begin(), truth.end(),
                                      vals.begin(),  vals.end());

        for(size_t i = 0;  i < 15; ++i)
        {
            size_t e = i-(i%5)+5;
            for(size_t j = e;  j < 15; ++j)
                dist[std::make_pair(i,j)].dist = 5.f;
            dist[std::make_pair(i,i)].dist = 0.f;
        }
        r = run(args, dist);
        BOOST_CHECK_EQUAL(3,  r.groups.size());
        BOOST_CHECK_EQUAL(2,  r.groups[0].first);
        BOOST_CHECK_EQUAL(7,  r.groups[1].first);
        BOOST_CHECK_EQUAL(12, r.groups[2].first);
        BOOST_CHECK_EQUAL(5,  r.groups[0].second.size());
        BOOST_CHECK_EQUAL(5,  r.groups[1].second.size());
        BOOST_CHECK_EQUAL(5,  r.groups[2].second.size());

        vals.resize(0);
        for(auto const & x: r.groups[0].second) vals.push_back(x.id);
        BOOST_CHECK_EQUAL_COLLECTIONS(truth.begin(), truth.end(),
                                      vals.begin(),  vals.end());
    }

    BOOST_AUTO_TEST_CASE(theoretical)
    {
        using namespace hybridstat::theoretical;
        std::istringstream stream;
        stream.str("# blah blah\n"
                   "   # blah blah\n"
                   ">first seq line 1\n"
                   "first SEQ line 2\n"
                   "   # blah blah\n"
                   "first seq line 3\n"
                   "  >   second seq line 1\n"
                   "  SECOND SEQ LINE 2\n"
                   "   # blah blah\n"
                   " second seq line 3   \n"
                   "\t>third seq line 1\n"
                   "third seq line 2\n"
                   ">FOURTH seq line 1\n"
                   "FOURTH seq line 2\n"
                  );

        auto res = read(stream);
        BOOST_CHECK_EQUAL(4, res.size());
        BOOST_CHECK_EQUAL("first seq line 1",                   res[0].name);
        BOOST_CHECK_EQUAL("first seq line 2first seq line 3",   res[0].seq);
        BOOST_CHECK_EQUAL("second seq line 1",                  res[1].name);
        BOOST_CHECK_EQUAL("second seq line 2second seq line 3", res[1].seq);
        BOOST_CHECK_EQUAL("third seq line 1",                   res[2].name);
        BOOST_CHECK_EQUAL("third seq line 2",                   res[2].seq);
        BOOST_CHECK_EQUAL("FOURTH seq line 1",                  res[3].name);
        BOOST_CHECK_EQUAL("fourth seq line 2",                  res[3].seq);

        Sequences th = {{"A", "aaaaGGGaaatttCCCaaa", {}}, {"B", "CCCaaaCCC", {}}};
        Oligos    ol = {"GGG", "CC"};
        auto      x  = find(th, ol);

        decltype(x[0].peaks) out[2];
        out[0] = _inp(_v{{8,1.}, {17.,1.}, {18., 1.}});
        out[1] = _inp(_v{{3,1.}, {4., 1.}, {9., 1.}, {10., 1.}});

        BOOST_CHECK_EQUAL(2, x.size());
        BOOST_CHECK_EQUAL_COLLECTIONS(out[0].begin(), out[0].end(),
                                      x[0].peaks.begin(), x[0].peaks.end());
        BOOST_CHECK_EQUAL_COLLECTIONS(out[1].begin(), out[1].end(),
                                      x[1].peaks.begin(), x[1].peaks.end());
    }

    BOOST_AUTO_TEST_CASE(bestmatch)
    {
        using namespace hybridstat::bestmatch;
        Args cf;
        cf.ext5 = .1f;
        Hairpins refs = {{"A", _inp(_v{{10., 1.}, {15., 1.}, {20., 1.}}), {}},
                         {"B", _inp(_v{{5.,  1.}, {15., 1.}, {20., 1.}}), {}}};
        Input bead = _inp(_v{{0., 1.}, {10./20.+.5, 1.}, {15/20.+.5, 1.}, {20./20.+.5, 1.}});
        auto  res  = run(cf, refs, bead);
        BOOST_CHECK_EQUAL(0, res.id);

        bead = _inp(_v{{0., 1.}, {1.048, 1.}, {1.550, 1.}, {2.050, 1.}});
        res  = run(cf, refs, bead);
        BOOST_CHECK_EQUAL(0, res.id);

        bead = _inp(_v{{0., 1.}, {.550, 1.}, {1.050, .1}, {1.550, 1.}, {2.050, 1.}});
        res  = run(cf, refs, bead);
        BOOST_CHECK_EQUAL(1, res.id);

        bead = _inp(_v{{0., 1.}, {.550, .01}, {1.050, 1.}, {1.550, 1.}, {2.050, 1.}});
        res  = run(cf, refs, bead);
        BOOST_CHECK_EQUAL(0, res.id);
    }

    BOOST_AUTO_TEST_CASE(matchedpeaks)
    {
        using namespace hybridstat::distance;
        Result r  = {0., .9, .1};
        Input  b1 = _inp(_v{{1.01, 1.}, {1.51, 1.}, {2.01, 1.}});
        Input  b2 = _inp(_v{{1.,   1.}, {1.5, 1.},  {2., 1.}});
        auto ch = [&](auto &b) { for(auto & x:b) x.first = (x.first-r.bias)/r.dyn; };

        ch(b2);
        auto          res   = matched(.1f, r, b2, b1);
        decltype(res) truth = {{0, 0}, {1, 1}, {2, 2}};
        BOOST_CHECK_EQUAL_COLLECTIONS(truth.begin(), truth.end(),
                                      res  .begin(), res  .end());

        b1 = _inp(_v{{1.01, 1.}, {1.51, 1.}, {2.01, 1.}});
        b2 = _inp(_v{{.95,  1.}, {1.,   1.}, {1.05, 1.}, {1.5, 1.},   {2.0, 1.}});

        ch(b2);
        res   = matched(.05f, r, b2, b1);
        truth = {{1, 0}, {3, 1}, {4, 2}};
        BOOST_CHECK_EQUAL_COLLECTIONS(truth.begin(), truth.end(),
                                      res  .begin(), res  .end());

        b1 = _inp(_v{{1.01, 1.}, {1.51, 1.}, {2.01, 1.}});
        b2 = _inp(_v{{.95,  1.}, {1.01, 1.}, {1.05, 1.}, {1.39, 1.}, {1.61, 1.},  {2., 1.}});

        ch(b2);
        res   = matched(.05f, r, b2, b1);
        truth = {{1, 0}, {5, 2}};
        BOOST_CHECK_EQUAL_COLLECTIONS(truth.begin(), truth.end(),
                                      res  .begin(), res  .end());

        b1 = _inp(_v{{1.,     1.}, {1.001,   1.}, {1.002,  1.}, {1.5, 1.}});
        b2 = _inp(_v{{1.0005, 1.}, {1.00101, 1.}, {1.0021, 1.}, {1.7, 1.}});

        ch(b2);
        res   = matched(.05f, r, b2, b1);
        truth = {{0, 0}, {1, 1}, {2, 2}};
        BOOST_CHECK_EQUAL_COLLECTIONS(truth.begin(), truth.end(),
                                      res  .begin(), res  .end());
    }
BOOST_AUTO_TEST_SUITE_END()
