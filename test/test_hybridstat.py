u""" Tests hybridstat's xlsx output"""
import unittest
from hybridstat.report import _run
from hybridstat._type  import Hairpin, Probability, Position, Peak, Key, Distance, Bead, Group
from hybridstat.identification import read, write

class Clustering(unittest.TestCase):
    u""" tests hybridstat's xlsx output """
    @classmethod
    def _create_args(cls, ext):
        def _create_grp(i):
            i = i*5
            def _create_bead(j:int):
                def _create_peak(k:int):
                    return Peak(Position   (float(k+i), float(k+j), True),
                                Probability(k/10., k/5., k/50.),
                                None if k % 2 == 0 else k,
                                tuple(Position(float(l),float(l), True) for l in range(5)))
                return Bead(Key     (1,j), j+10, 1., j/100.,
                            tuple(_create_peak(k) for k in range(5)),
                            Distance(j, 1.+j/100.,j/100.))

            return Group(Key(1,i), tuple(_create_bead(j) for j in range(i,i+5)))
        return dict(git      = dict(BRANCH   = "branch",
                                    HASHTAG  = "hashtag",
                                    VERSION  = "version",
                                    AUTHOR   = "author",
                                    DATE     = "date",
                                    CLEAN    = False),
                    config   = dict(help     = "CONFIG", firstphase = 4,
                                    ext5     = 1., precision = 1.),
                    hairpins = tuple(),
                    oligos   = ("OLIGO1", "OLIGO2"),
                    filename = "/tmp/"+cls.__name__+"."+ext,
                    groups   = tuple(_create_grp(i) for i in range(5)))

    def test_xlsx(self):
        u""" create one xlsx output """
        _run(self._create_args('xlsx'))
    def test_csv(self):
        u""" create one csv output """
        _run(self._create_args('csv'))

class BestMatch(unittest.TestCase):
    u""" tests hybridstat's xlsx output """
    @classmethod
    def _create_args(cls, ext):
        def _create_grp(i):
            i = i*5
            def _create_bead(j:int):
                def _create_peak(k:int):
                    return Peak(Position   (float(k+i), float(k+j), True),
                                Probability(k/10., k/5., k/50.),
                                None if k % 2 == 0 else k-1,
                                tuple(Position(float(l),float(l), True) for l in range(5)))
                return Bead(Key     (1,j), j+10, 1., j/100.,
                            tuple(_create_peak(k) for k in range(5)),
                            Distance(j, 1.+j/100.,j/100.))

            return Group(Key(1,i), tuple(_create_bead(j) for j in range(i,i+5)))
        return dict(git      = dict(BRANCH   = "branch",
                                    HASHTAG  = "hashtag",
                                    VERSION  = "version",
                                    AUTHOR   = "author",
                                    DATE     = "date",
                                    CLEAN    = False),
                    config   = dict(help     = "CONFIG", firstphase = 3,
                                    ext5     = 1., precision = 1.),
                    hairpins = (Hairpin('HP1', 'abcdefghijklmnopqrstuvwxyz0123456789', []),
                                Hairpin('HP2', 'cccccccggggggggggggggggggggggggggggg', []),
                                Hairpin('HP3', 'zyxwvutsrqpomnlkjihgfedcba0123456789', [3]),
                                Hairpin('HP4', '0123456789zyxwvutsrqpomnlkjihgfedcba', []),
                                Hairpin('HP5', '+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-', [21])),
                    oligos   = ("atcg", "cccc"),
                    filename = "/tmp/"+cls.__name__+"."+ext,
                    groups   = tuple(_create_grp(i) for i in range(5)))

    def test_xlsx(self):
        u""" create one xlsx output """
        _run(self._create_args('xlsx'))

    def test_csv(self):
        u""" create one csv output """
        _run(self._create_args('csv'))

class IdentificationTest(unittest.TestCase):
    u"testing identification related python"
    def test_extractsummary(self):
        u""" extracts info from best match test"""
        res   = read("/tmp/BestMatch.xlsx")
        truth = (('T1B5',  (5, 6, 7, 8, 9)),
                 ('T1B15', (15, 16, 17, 18, 19)),
                 ('T1B20', (20, 21, 22, 23, 24)),
                 ('T1B0',  (0, 1, 2, 3, 4)),
                 ('T1B10', (10, 11, 12, 13, 14)))
        self.assertEqual(dict(res), dict(truth))

    def test_identification(self):
        u""" extracts info from best match test"""
        truth = (('T1B5',  (5, 6, 7, 9)),
                 ('T1B15', (15, 18, 19)),
                 ('T1B20', (20, 21, 22, 23, 24)),
                 ('T1B0',  (0, 3, 4)),
                 ('T1B10', (10, 11, 12, 13, 14)))

        write("/tmp/Identification.xlsx", truth)
        res = read("/tmp/Identification.xlsx")
        self.assertEqual(dict(res), dict(truth))

if __name__ == '__main__':
    unittest.main()
