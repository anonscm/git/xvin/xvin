#include "accumulators.h"
#include "filter_core.h"
#include "filter_core.cc"

#define BOOST_TEST_MODULE filters
#include <boost/test/included/unit_test.hpp>
#include <numeric>

BOOST_AUTO_TEST_CASE(random_access_rolling)
{
    using namespace stats;

    double vals[] = { 1., 2., 3., 4., 5. };
    double wgts[] = { 2., 2., 2., 2., 2.,};

    {
        RARolling<bat::mean> mean(1);
        for(size_t i = 0; i < sizeof(vals)/sizeof(double); ++i)
        {
            mean(vals+i, wgts+i);
            BOOST_CHECK_EQUAL(vals[i], ba::mean(mean));
        }
    }

    {
        RARolling<bat::mean> mean(2);
        mean(vals, wgts);
        for(size_t i = 1; i < sizeof(vals)/sizeof(double); ++i)
        {
            mean(vals+i, wgts+i);
            BOOST_CHECK_CLOSE((vals[i]+vals[i-1])*.5, ba::mean(mean), 1e-10);
        }
    }

    {
        RARolling<bat::mean> mean(2);
        mean(vals, vals);
        for(size_t i = 1; i < sizeof(vals)/sizeof(double); ++i)
        {
            mean(vals+i, vals+i);
            auto x = (vals[i]*vals[i]+vals[i-1]*vals[i-1])/(vals[i]+vals[i-1]);
            BOOST_CHECK_CLOSE(x, ba::mean(mean), 1e-10);
        }
    }
}

BOOST_AUTO_TEST_CASE(buffered_rolling)
{
    using namespace stats;

    std::vector<double> vals = { 1., 2., 3., 4., 5. };
    std::vector<double> wgts = { 2., 2., 2., 2., 2.,};

    {
        Rolling<bat::mean> mean(1);
        for(size_t i = 0; i < vals.size(); ++i)
        {
            mean(vals[i], wgts[i]);
            BOOST_CHECK_EQUAL(vals[i], ba::mean(mean));
        }
    }

    {
        Rolling<bat::mean> mean(2);
        mean(vals[0], wgts[0]);
        for(size_t i = 1; i < vals.size(); ++i)
        {
            mean(vals[i], wgts[i]);
            BOOST_CHECK_CLOSE((vals[i]+vals[i-1])*.5, ba::mean(mean), 1e-10);
        }
    }

    {
        Rolling<bat::mean> mean(2);
        mean(vals[0], vals[0]);
        for(size_t i = 1; i < vals.size(); ++i)
        {
            mean(vals[i], vals[i]);
            auto x = (vals[i]*vals[i]+vals[i-1]*vals[i-1])/(vals[i]+vals[i-1]);
            BOOST_CHECK_CLOSE(x, ba::mean(mean), 1e-10);
        }
    }
}

BOOST_AUTO_TEST_CASE(forwardbackward)
{
    using namespace filter::forwardbackward;

    Args cf;
    cf.normalize  = false;
    cf.estimators = {2};
    cf.precision  = 1;
    cf.power      = 1;
    cf.window     = 2;

    std::valarray<float> data  = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    std::valarray<float> truth(1., data.size());
    cf.derivate = true;
    run(cf, data);
    for(size_t i = 1; i < data.size()-1; ++i)
        BOOST_CHECK_EQUAL(truth[i], data[i]);

    cf.derivate = false;
    data  = {1, 2, 3, 4, 5, 6, 7, 8};
    truth = {1, (1.5*2+2.5)/3., 3, 4, 5, 6, (6.5+7.5*2)/3., 8};
    run(cf, data);
    BOOST_CHECK_EQUAL_COLLECTIONS(std::begin(truth), std::end(truth),
                                  std::begin(data),  std::end(data));

    data = std::valarray<float>(1., 100);
    cf   = Args();
    run(cf, data);

    truth = std::valarray<float>(1., 100);
    BOOST_CHECK_EQUAL_COLLECTIONS(std::begin(truth), std::end(truth),
                                  std::begin(data),  std::end(data));

}

BOOST_AUTO_TEST_CASE(xvnonlin)
{
    using namespace filter::xvnonlin;

    Args cf;
    cf.estimators = {2};
    cf.precision  = 1;
    cf.power      = 1;

    std::valarray<float> data  = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    std::valarray<float> truth(1., data.size());
    cf.derivate = true;
    run(cf, data);
    for(size_t i = 1; i < data.size()-1; ++i)
        BOOST_CHECK_EQUAL(truth[i], data[i]);

    cf.derivate = false;
    data  = {1, 2, 3, 4, 5};
    truth = {1.22969747, 2., 3., 4., 4.5};
    run(cf, data);
    BOOST_CHECK_EQUAL_COLLECTIONS(std::begin(truth), std::end(truth),
                                  std::begin(data),  std::end(data));

    data = std::valarray<float>(1., 100);
    cf   = Args();
    run(cf, data);

    truth = std::valarray<float>(1., 100);
    BOOST_CHECK_EQUAL_COLLECTIONS(std::begin(truth), std::end(truth),
                                  std::begin(data),  std::end(data));
}
