import pyxvin as lp

print("Current PR: {}".format(lp.PltReg.current()))
print("Current OP: {}".format(lp.OnePlot.current()))
print("Current DS: {}".format(lp.DataSet.current()))
print("DS lengths:", [(len(x.xd), len(x.yd)) for x in lp.OnePlot.current()])

ds = lp.OnePlot.current()[0]
print("OP[0]: {}".format(ds))
print("min(xd): {}, max(xd) {}, mean(xd) {}, std(xd){}" \
        .format(ds.xd.min(), ds.xd.max(), ds.xd.mean(), ds.xd.std()))
print("min(yd): {}, max(yd) {}, mean(yd) {}, std(yd){}" \
        .format(ds.yd.min(), ds.yd.max(), ds.yd.mean(), ds.yd.std()))

ds = lp.PltReg.current().newitem(10)[0]
ds.xd = range(10)
ds.yd = range(10,20)
op = lp.OnePlot.current()
lp.refresh_plot()

ds = op.newitem(15)
ds.xd = range(8,8+15)
ds.yd = range(10,20)

ds.yd = range(20,5,-1)
lp.refresh_plot()
