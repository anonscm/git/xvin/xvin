#include <allegro.h>
/* XPM */
static const char *allegico_xpm[] = {
/* columns rows colors chars-per-pixel */
"32 32 6 1",
"  c opaque",
". c navy",
"X c blue",
"o c red",
"O c yellow",
"+ c None",
/* pixels */
"     +++++        ++++++++     +",
" XXXX +++ XXXX.XX ++++++++ XXX +",
" .XXX +++ XXXX..XX ++++++ XXXX +",
"+ .XXX + XXXX  .XX ++++++ XXX. +",
"++ .XXX XXXX + ..XX ++++ XXXX ++",
"++ ..XXXXXXX ++ .XX ++++ XXX +++",
"+++ ..XXXXX +++ ..XX ++ XXXX +++",
"++++ ..XXX +++++ .XX ++ XXX ++++",
"++++ XXXXX +++++ ..XX  XXXX ++++",
"+++ XXX.XXX +++++ .XX  XXX +++++",
"++ XXXX..XXX ++++ .XXXXXXX +++++",
"++ XXXX ..XX ++++ ..XXXXXX +++++",
"+ .XXX + ..XX ++++ .XXXX. ++++++",
" .... +++ .... +++ ...... ++++++",
" .... +++ .... ++++ .... +++++++",
"     +++++     ++++      +++++++",
"oooooooooooooooooooooooooooooooo",
"                                ",
"                                ",
"                                ",
"      OOOO             OOOOO    ",
"     OOOOOO           OOOOOOO   ",
"    OOOOOOOO         OOOOOOOOO  ",
"    OOOOOOOOO       OOOO   OOOO ",
"   OOOO   OOOO     OOOO     OOOO",
"  OOOO     OOO     OOO       OOO",
" OOOO       OOO   OOO        OOO",
" OOOO       OOOO  OOO         OO",
"OOOO         OOOOOOO           O",
"OOO           OOOOOO            ",
"OOO            OOOO             ",
"                                "
};

#if defined ALLEGRO_WITH_XWINDOWS && defined ALLEGRO_USE_CONSTRUCTOR
extern void *allegro_icon;
CONSTRUCTOR_FUNCTION(static void _set_allegro_icon(void));
static void _set_allegro_icon(void)
{
    allegro_icon = allegico_xpm;
}
#endif
