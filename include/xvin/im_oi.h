#ifndef _IM_OI_H_
#define _IM_OI_H_

# include "platform.h"
# include "unitset.h"
# include "xvinerr.h"
# include "util.h"
# include "box_def.h"
# include "plot_ds.h"
# include "plot_op.h"
# include "plot_opt.h"
# include "gui_def.h"
# include "plot2box.h"
# include "gr_file.h"


/*-------------------------------------------------------------------------
  IMAGE STUFF
  ---------------------------------------------------------------------------*/

# define    IS_INT_IMAGE        128
# define    IS_CHAR_IMAGE       256
# define    IS_FLOAT_IMAGE      512
# define    IS_COMPLEX_IMAGE    64
# define    IS_RGB_PICTURE      16384   // 0x4000
# define    IS_BW_PICTURE       32768  // 0x8000
# define    IS_UINT_IMAGE       131072  // 0x20000
# define    IS_LINT_IMAGE       262144  // 0x40000
# define    IS_RGBA_PICTURE     524288  // 0x80000
# define    IS_DOUBLE_IMAGE     0x200000
# define    IS_COMPLEX_DOUBLE_IMAGE 0x400000
# define    IS_RGB16_PICTURE    0x800000
# define    IS_RGBA16_PICTURE   0x100000

# define    IS_BITMAP           65636


# define    IS_INT          256
# define    IS_CHAR         128
# define    IS_FLOAT        512

# define    PLOT_NEED_REFRESH       0x1
# define    EXTRA_PLOT_NEED_REFRESH         0x2
# define    PLOTS_NEED_REFRESH      0x3
# define    BITMAP_NEED_REFRESH     0x4
# define    INTERNAL_BITMAP_NEED_REFRESH    0x8
# define    ALL_NEED_REFRESH        0xF


typedef struct _mcomplex
{
    float re, im;
} mcomplex;

typedef struct _mdcomplex
{
    double dre, dim;
} mdcomplex;


typedef struct _rgb
{
    unsigned char r, g, b;
} rgb_t;

typedef struct _rgba
{
    unsigned char r, g, b, a;
} rgba_t;

typedef struct _rgb16
{
    short int r, g, b;
} rgb16_t;

typedef struct _rgba16
{
    short int r, g, b, a;
} rgba16_t;

union pix                   /* data pointer to pixel */
{
    unsigned char *ch;
    short int *in;
    unsigned short int *ui;
    int *li;
    float *fl;
    double *db;
    mcomplex *cp;
    mdcomplex *dcp;
    rgb_t *rgb;
    rgba_t *rgba;
    rgb16_t *rgb16;
    rgba16_t *rgba16;
};





struct image_data
{
    int data_type, mode;            /* char, int or float */
    int nx, nxs, nxe;           /* nb of x pixels */
    int ny , nys, nye;          /* nb of y pixels */
    union pix *pixel;           /* the image data */
    union pix **pxl;            /* the array for movie */
    void **mem;                             /* the array containing the starting point of memory of each image */
    int n_f, m_f, c_f;          /* the number of images */
    unsigned char **over;           /* overwrite plane 1 bit */
    time_t time;                    /* date of creation */
    char *source;
    char *history;
    char *treatement;
    char **special;             /* strings for special infos*/
    int n_special, m_special;
    struct hook use;            /* usage of the image */
    unsigned char win_flag;         /* boundary conditions */
    int movie_on_disk;                      /* specify if in memory or on disk */
    double record_duration;                 /* duration of data acquisition in micro seconds */
    long long record_start;                 /* start of duration of data */
    struct screen_label ** *s_l;             /* an array of screen object with first index
                                               related to image number in a movie */
    int *n_sl, *m_sl;

    int **user_ispare;                      // integer paramters specific for each image
    float **user_fspare;                      // float paramters specific for each image
    int user_nipar;                         // nb integer paramters specific for each image
    int user_nfpar;                         // nb float paramters specific for each image
    int multi_page;                        // if set memory is cut one chunck per image for movie
    int has_sqaure_pixel;
    float src_parameter[8];        // numerical value of parameter characterizing data (ex. temperature ...)
    char *src_parameter_type[8];   // the description of these parameters
  int save_using_this_frame_as_first;   // for image rolling buffer

    /*
       int *iparam;                            // integer paramter specific for each image
       float *fparam;                          // float paramter specific for each image
       int *i1param;                           // integer paramter specific for each image
       int *i2param;                           // integer paramter specific for each image
       int *i3param;                           // integer paramter specific for each image
       */
};




/* possible mode values */
# define    LOG_AMP     8
# define    AMP_2       16
# define    AMP     32
# define    RE      64
# define    IM      128
# define    KX      256
# define    KY      512
# define    PHI     1024
# define    GREY_LEVEL  0
# define    R_LEVEL     2048
# define    RED_ONLY    2049
# define    G_LEVEL     4096
# define    GREEN_ONLY  4097
# define    B_LEVEL     8192
# define    BLUE_ONLY   8193
# define    ALPHA_LEVEL 16384
# define        TRUE_RGB        32768

/* data treatement */
# define    LOW_PASS    32
# define    BAND_PASS   64
# define    HIGH_PASS   128
# define    BRICK_WALL  8192
# define    REAL_MODE   512
# define    COMPLEX_MODE    1024

/* define boundary conditions */
# define    X_PER       1
# define    Y_PER       2
# define    X_NOT_PER   ~X_PER
# define    Y_NOT_PER   ~Y_PER

# define    DATA_IN_USE 1
# define    BITMAP_IN_USE   2

typedef struct one_image O_i;

 struct one_image
{
    int type;
    char *filename, *dir, *title;   /* The strings defining the titles */
    char *x_title, *y_title, *x_prime_title, *y_prime_title;
    char *x_prefix, *y_prefix, *x_unit, *y_unit;
    char *z_prefix, *t_prefix, *z_unit, *t_unit;
    float width, height, right, up;
    int iopt, iopt2;
    float tick_len;
    float x_lo, x_hi, y_lo, y_hi;
    float z_black, z_white;     /* grey-scale def */
    float z_min, z_max;     /* grey-scale limit */
    float z_Rmin, z_Rmax;     /* Red scale limit */
    float z_Gmin, z_Gmax;     /* Green scale limit */
    float z_Bmin, z_Bmax;     /* Blue scale limit */
    float ax, dx;           /* conversion factor and offset */
    float ay, dy;           /* between pixels and user dim */
    float az, dz;           /* between pixels and user dim */
    float at, dt;           /* between frames and time */
    struct image_data im;
    struct plot_label **lab;    /* labelling in the plot */
    int n_lab, m_lab;
    O_p **o_p;          /* the series of plot */
    int n_op, m_op;         /* the number of plot */
    int cur_op;         /* the current one */
    un_s    **xu;           /* x unit set */
    int n_xu, m_xu, c_xu;
    un_s    **yu;           /* y unit set */
    int n_yu, m_yu, c_yu;
    un_s    **zu;           /* z unit set */
    int n_zu, m_zu, c_zu;
    un_s    **tu;           /* t unit set (for movies) */
    int n_tu, m_tu, c_tu;
    struct hook bmp;        /* use to attach a BITMAP */
    int need_to_refresh;
    int rm_background; //activate backgound removal
    O_i *oi_bg;  // image background to remove
    int buisy_in_thread;
    int data_changing;
    int transfering_data_to_box;
    int filename_is;
    char *fileext;
    int (*oi_idle_action)(struct one_image *oi, DIALOG *d);
    int (*oi_got_mouse)(struct  one_image *oi, int xm_s, int ym_s, int mode);
    int (*oi_lost_mouse)(struct  one_image *oi, int xm_s, int ym_s, int mode);
    int (*oi_end_action)(struct one_image *oi, DIALOG *d);
    int (*oi_mouse_action)(struct one_image *oi, int x0, int y0, int mode, DIALOG *d);
    int (*oi_post_display)(struct one_image *oi, DIALOG *d); /* used to add graphis element just before blit*/
};


typedef struct screen_label
{
    int type;           /* absolute or user defined */
    float xla, yla;     /* the position of label */
    char *text;         /* the text of it */
    float user_val;

    struct hook use;            /* usage of the image */
    int (*draw_shape)(struct screen_label *s_l, struct one_image *oi, void *imr);
    int (*shape_action)(struct screen_label *s_l, struct one_image *oi, void *imr);
} S_l;



typedef struct im_max
{
    int x0, x1, y0, y1, xm, ym, np, inb;            // a rectangle containing the max in pixels
    double xpos, ypos, weight, zmax, dnb, chi2, sigma;      // trap size
} im_ext;


typedef struct im_max_array
{
    struct im_max *ie;
    unsigned int n_ie, m_ie, c_ie;
} im_ext_ar;


XV_FUNC(int, set_oi_mode, (O_i *oi, int mode));
XV_FUNC(int, get_oi_mode, (O_i *oi));
XV_FUNC(int, set_oi_type, (O_i *oi, int type));
XV_FUNC(int, get_oi_type, (O_i *oi));


# define    SET_DATA_IN_USE(oi)         (oi)->buisy_in_thread |= DATA_IN_USE
# define    SET_BITMAP_IN_USE(oi)           (oi)->buisy_in_thread |= BITMAP_IN_USE
# define    SET_DATA_NO_MORE_IN_USE(oi) (oi)->buisy_in_thread &= ~DATA_IN_USE
# define    SET_BITMAP_NO_MORE_IN_USE(oi)   (oi)->buisy_in_thread &= ~BITMAP_IN_USE

# define    IS_DATA_IN_USE(oi)          ((oi)->buisy_in_thread & DATA_IN_USE)
# define    IS_BITMAP_IN_USE(oi)            ((oi)->buisy_in_thread & BITMAP_IN_USE)



# define    SET_PLOT_NEED_REFRESH(oi)          (oi)->need_to_refresh |= PLOT_NEED_REFRESH
# define    SET_EXTRA_PLOT_NEED_REFRESH(oi)        (oi)->need_to_refresh |= EXTRA_PLOT_NEED_REFRESH
# define    SET_PLOTS_NEED_REFRESH(oi)         (oi)->need_to_refresh |= PLOTS_NEED_REFRESH
# define    SET_BITMAP_NEED_REFRESH(oi)        (oi)->need_to_refresh |= BITMAP_NEED_REFRESH
# define    SET_INTERNAL_BITMAP_NEED_REFRESH(oi)   (oi)->need_to_refresh |= INTERNALBITMAP_NEED_REFRESH
# define    SET_ALL_NEED_REFRESH(oi)           (oi)->need_to_refresh |= ALL_NEED_REFRESH

# define    UNSET_PLOT_NEED_REFRESH(oi)        (oi)->need_to_refresh &= ~PLOT_NEED_REFRESH
# define    UNSET_EXTRA_PLOT_NEED_REFRESH(oi)      (oi)->need_to_refresh &= ~EXTRA_PLOT_NEED_REFRESH
# define    UNSET_PLOTS_NEED_REFRESH(oi)           (oi)->need_to_refresh &= ~PLOTS_NEED_REFRESH
# define    UNSET_BITMAP_NEED_REFRESH(oi)          (oi)->need_to_refresh &= ~BITMAP_NEED_REFRESH
# define    UNSET_INTERNAL_BITMAP_NEED_REFRESH(oi) (oi)->need_to_refresh &= ~INTERNALBITMAP_NEED_REFRESH
# define    UNSET_ALL_NEED_REFRESH(oi)         (oi)->need_to_refresh &= ~ALL_NEED_REFRESH



# define    OI_TYPE_IS_UNSIGNED_CHAR(oi)        (get_oi_type(oi) == \
                                                 IS_CHAR_IMAGE)) ? 1 : 0;
# define    OI_TYPE_IS_RGB(oi)              (get_oi_type(oi) == \
                                             IS_RGB_PICTURE)) ? 1 : 0;
# define    OI_TYPE_IS_RGBA(oi)             (get_oi_type(oi) == \
                                             IS_RGBA_PICTURE)) ? 1 : 0;
# define    OI_TYPE_IS_RGB16(oi)                (get_oi_type(oi) == \
                                                 IS_RGB16_PICTURE)) ? 1 : 0;
# define    OI_TYPE_IS_RGBA16(oi)               (get_oi_type(oi) == \
                                                 IS_RGBA16_PICTURE)) ? 1 : 0;
# define    OI_TYPE_IS_INT(oi)          (get_oi_type(oi) == \
                                         IS_INT_IMAGE)) ? 1 : 0;
# define    OI_TYPE_IS_UNSIGNED_SHORT_INT(oi)   (get_oi_type(oi) == \
                                                 IS_UINT_IMAGE)) ? 1 : 0;
# define    OI_TYPE_IS_LONG_INT(oi)     (get_oi_type(oi) == \
                                         IS_LINT_IMAGE)) ? 1 : 0;
# define    OI_TYPE_IS_FLOAT(oi)            (get_oi_type(oi) == \
                                             IS_FLOAT_IMAGE)) ? 1 : 0;
# define    OI_TYPE_IS_DOUBLE(oi)       (get_oi_type(oi) == \
                                         IS_DOUBLE_IMAGE)) ? 1 : 0;
# define    OI_TYPE_IS_COMPLEX(oi)      (get_oi_type(oi) == \
                                         IS_COMPLEX_IMAGE)) ? 1 : 0;
# define    OI_TYPE_IS_COMPLEX_DOUBLE(oi)   (get_oi_type(oi) == \
                                             IS_COMPLEX_DOUBLE_IMAGE)) ? 1 : 0;


# define    IS_INT_IMAGE        128
# define    IS_CHAR_IMAGE       256
# define    IS_FLOAT_IMAGE      512
# define    IS_COMPLEX_IMAGE    64
# define    IS_RGB_PICTURE      16384
# define    IS_BW_PICTURE       32768
# define    IS_UINT_IMAGE       131072
# define    IS_LINT_IMAGE       262144
# define    IS_RGBA_PICTURE     524288
# define    IS_DOUBLE_IMAGE     0x200000
# define    IS_COMPLEX_DOUBLE_IMAGE 0x400000
# define    IS_RGB16_PICTURE    0x800000
# define    IS_RGBA16_PICTURE   0x100000

# define        UCHAR_LINE_PTR(oi,line)     (oi)->im.pixel[(line)].ch
# define        SINT_LINE_PTR(oi,line)      (oi)->im.pixel[(line)].in
# define        UINT_LINE_PTR(oi,line)      (oi)->im.pixel[(line)].ui
# define        LINT_LINE_PTR(oi,line)      (oi)->im.pixel[(line)].li
# define        FLT_LINE_PTR(oi,line)       (oi)->im.pixel[(line)].fl
# define        DBL_LINE_PTR(oi,line)       (oi)->im.pixel[(line)].db
# define        RGB_LINE_PTR(oi,line)       (oi)->im.pixel[(line)].rgb
# define        RGBA_LINE_PTR(oi,line)      (oi)->im.pixel[(line)].rgba
# define        RGB16_LINE_PTR(oi,line)     (oi)->im.pixel[(line)].rgb16
# define        RGBA16_LINE_PTR(oi,line)    (oi)->im.pixel[(line)].rgba16
# define        CFLT_LINE_PTR(oi,line)      (oi)->im.pixel[(line)].mcomplex
# define        CDBL_LINE_PTR(oi,line)      (oi)->im.pixel[(line)].mdcomplex


# define        UCHAR_PIXEL(oi,line,col)     (oi)->im.pixel[(line)].ch[(col)]
# define        SINT_PIXEL(oi,line,col)      (oi)->im.pixel[(line)].in[(col)]
# define        UINT_PIXEL(oi,line,col)      (oi)->im.pixel[(line)].ui[(col)]
# define        LINT_PIXEL(oi,line,col)      (oi)->im.pixel[(line)].li[(col)]
# define        FLT_PIXEL(oi,line,col)       (oi)->im.pixel[(line)].fl[(col)]
# define        DBL_PIXEL(oi,line,col)       (oi)->im.pixel[(line)].db[(col)]
# define        RGB_PIXEL(oi,line,col)       (oi)->im.pixel[(line)].rgb[(col)]
# define        RGBA_PIXEL(oi,line,col)      (oi)->im.pixel[(line)].rgba[(col)]
# define        RGB16_PIXEL(oi,line,col)     (oi)->im.pixel[(line)].rgb16[(col)]
# define        RGBA16_PIXEL(oi,line,col)    (oi)->im.pixel[(line)].rgba16[(col)]
# define        CFLT_PIXEL(oi,line,col)      (oi)->im.pixel[(line)].mcomplex[(col)]
# define        CDBL_PIXEL(oi,line,col)      (oi)->im.pixel[(line)].mdcomplex[(col)]

XV_FUNC(unsigned char, get_uchar_pixel, (O_i *oi, int line, int col));
XV_FUNC(short int, get_sint_pixel, (O_i *oi, int line, int col));
XV_FUNC(unsigned short int, get_uint_pixel, (O_i *oi, int line, int col));
XV_FUNC(int, get_lint_pixel, (O_i *oi, int line, int col));
XV_FUNC(float, get_flt_pixel, (O_i *oi, int line, int col));
XV_FUNC(double, get_dbl_pixel, (O_i *oi, int line, int col));
XV_FUNC(rgb_t, get_rgb_pixel, (O_i *oi, int line, int col));
XV_FUNC(rgba_t, get_rgba_pixel, (O_i *oi, int line, int col));
XV_FUNC(rgb16_t, get_rgb16_pixel, (O_i *oi, int line, int col));
XV_FUNC(rgba16_t, get_rgba16_pixel, (O_i *oi, int line, int col));
XV_FUNC(mcomplex, get_cflt_pixel, (O_i *oi, int line, int col));
XV_FUNC(mdcomplex, get_cdbl_pixel, (O_i *oi, int line, int col));

XV_FUNC(unsigned char *, uchar_line_ptr, (O_i *oi, int line));
XV_FUNC(short int *, sint_line_ptr, (O_i *oi, int line));
XV_FUNC(unsigned short int *, uint_line_ptr, (O_i *oi, int line));
XV_FUNC(int *, lint_line_ptr, (O_i *oi, int line));
XV_FUNC(float *, flt_line_ptr, (O_i *oi, int line));
XV_FUNC(double *, dbl_line_ptr, (O_i *oi, int line));
XV_FUNC(rgb_t *, rgb_line_ptr, (O_i *oi, int line));
XV_FUNC(rgba_t *, rgba_line_ptr, (O_i *oi, int line));
XV_FUNC(rgb16_t *, rgb16_line_ptr, (O_i *oi, int line));
XV_FUNC(rgba16_t *, rgba16_line_ptr, (O_i *oi, int line));
XV_FUNC(mcomplex *, cflt_line_ptr, (O_i *oi, int line));
XV_FUNC(mdcomplex *, cdbl_line_ptr, (O_i *oi, int line));


XV_FUNC(int, get_raw_pixel_value, (O_i *oi, int ix, int iy, double *val));
XV_FUNC(int, set_raw_pixel_value, (O_i *oi, int ix, int iy, double *val));

XV_FUNC(int, set_oi_region_of_interest_in_pixel,
        (O_i *oi, int x0, int y0, int x1, int y1));
XV_FUNC(int, get_oi_region_of_interest_in_pixel,
        (O_i *oi, int *x0, int *y0, int *x1, int *y1));
XV_FUNC(int, set_oi_source, (O_i *oi, char *format, ...)  __attribute__((format(printf, 2, 3))));
XV_FUNC(char *, get_oi_source, (O_i *oi));
XV_FUNC(int, set_oi_history, (O_i *oi, char *format, ...)  __attribute__((format(printf, 2, 3))));
XV_FUNC(char *, get_oi_history, (O_i *oi));
XV_FUNC(int, set_oi_treatement, (O_i *oi, char *format, ...)  __attribute__((format(printf, 2, 3))));
XV_FUNC(char *, get_oi_treatement, (O_i *oi));
XV_FUNC(int, update_oi_treatment, (O_i *oi, char *format, ...)  __attribute__((format(printf, 2, 3))));

# define    set_oi_one_special(oi,text) if ((oi) != NULL) \
                                                    add_to_one_image ((oi), \
                                                                      IS_SPECIAL, (void *)(text))


XV_FUNC(int, is_oi_a_movie, (O_i *ois));
XV_FUNC(int, nb_of_frames_in_movie, (O_i *ois));
XV_FUNC(int, is_oi_a_color_image, (O_i *ois));
XV_FUNC(int, is_oi_a_complex_image, (O_i *ois));


XV_FUNC(int, set_oi_periodic_in_x, (O_i *oi));
XV_FUNC(int, set_oi_periodic_in_y, (O_i *oi));
XV_FUNC(int, set_oi_periodic_in_x_and_y, (O_i *oi));
XV_FUNC(int, set_oi_not_periodic_in_x, (O_i *oi));
XV_FUNC(int, set_oi_not_periodic_in_y, (O_i *oi));
XV_FUNC(int, set_oi_not_periodic, (O_i *oi));
XV_FUNC(int, get_oi_periodicity_in_x, (O_i *oi));
XV_FUNC(int, get_oi_periodicity_in_y, (O_i *oi));

/* public functions */


/*  Enables to create an image  to
 ** destroy it. An "imreg" may contain several images "O_i", when created
 ** only one O_i is created which has been initialize but has no
 ** "image_data". When the number of pixels and image type is known
 ** it may be allocated (using alloc_one_image) and filled with the data.
 ** To add another image, you have to create it, to initialize it, to
 ** alloc it, and then to add it.
 */




XV_FUNC(int, set_oi_filename, (O_i *oi, char *file));
XV_FUNC(int, set_oi_path, (O_i *oi, char *dir));
XV_FUNC(char *, get_oi_filename, (O_i *oi));
XV_FUNC(char *, get_oi_path, (O_i *oi));

# define    set_oi_title            set_im_title
# define    set_oi_x_title          set_im_x_title
# define    set_oi_y_title          set_im_y_title
# define    set_oi_x_top_title      set_im_x_top_title
# define    set_oi_y_right_title        set_im_y_right_title

XV_FUNC(int, set_im_title, (O_i *oi, char *format, ...)  __attribute__((format(printf, 2, 3))));
XV_FUNC(int, set_im_x_title, (O_i *oi, char *format, ...)  __attribute__((format(printf, 2, 3))));
XV_FUNC(int, set_im_y_title, (O_i *oi, char *format, ...)  __attribute__((format(printf, 2, 3))));
XV_FUNC(int, set_im_x_top_title, (O_i *oi, char *format, ...)  __attribute__((format(printf, 2, 3))));
XV_FUNC(int, set_im_y_right_title, (O_i *oi, char *format, ...)  __attribute__((format(printf, 2, 3))));

XV_FUNC(char *, get_oi_title, (O_i *oi));
XV_FUNC(char *, get_oi_x_title, (O_i *oi));
XV_FUNC(char *, get_oi_y_title, (O_i *oi));
XV_FUNC(char *, get_oi_x_top_title, (O_i *oi));
XV_FUNC(char *, get_oi_y_right_title, (O_i *oi));


XV_FUNC(O_i *, create_one_image, (int nx, int ny, int data_type));
XV_FUNC(O_i *, duplicate_image, (O_i *src, O_i *dest));
XV_FUNC(O_i *, duplicate_image_or_movie, (O_i *src, O_i *dest, int ask));
XV_FUNC(O_i *, create_one_movie, (int nx, int ny, int data_type, int frames));
XV_FUNC(O_i *, create_one_continuous_movie, (int nx, int ny, int data_type, int n_frames));

XV_FUNC(int, free_one_image, (O_i *oi));

/* not to be used */
XV_FUNC(int, init_one_image, (O_i *oi, int type));
XV_FUNC(int, alloc_one_image, (O_i *oi, int nx, int ny, int type));

XV_FUNC(int, add_one_image, (O_i *oi, int type, void *stuff));
XV_FUNC(int, add_to_one_image, (O_i *oi, int type, void *stuff));
XV_FUNC(int, remove_one_image, (O_i *oi, int type, void *stuff));
XV_FUNC(int, remove_from_one_image, (O_i *oi, int type, void *stuff));

XV_FUNC(int, alloc_over_plane, (O_i *oi));
XV_FUNC(int, reset_full_over_plane, (O_i *oi, int val));
XV_FUNC(int, set_over, (O_i *oi, int x, int y));

XV_FUNC(int, extract_line, (O_i *oi, int n_line, float *z));
XV_FUNC(int, extract_row, (O_i *oi, int n_row, float *z));
XV_FUNC(int, extract_raw_line, (O_i *oi, int n_line, float *z));
XV_FUNC(int, extract_raw_row, (O_i *oi, int n_row, float *z));
XV_FUNC(int, extract_raw_partial_line, (O_i *oi, int n_line, float *z, int start, int size));
XV_FUNC(int, extract_raw_partial_row, (O_i *oi, int n_row, float *z, int start, int size));
XV_FUNC(float *, extract_tilted_profile, (O_i *oi, float *z, int nz, float x0, float y0, float x1, float y1,
        int *npts));
XV_FUNC(float *, extract_raw_tilted_profile, (O_i *oi, float *z, int nz, float x0, float y0, float x1, float y1,
        int *npts));

XV_FUNC(float *, extract_z_profile_from_movie, (O_i *ois, int xc, int yc, float *z));
XV_FUNC(int,extract_z_partial_profile_from_movie,(O_i *ois, int xc, int yc, float *z, int fbeg, int fend));

XV_FUNC(int, set_z_profile_from_movie, (O_i *ois, int xc, int yc, float *z, int nz));
XV_FUNC(int, switch_frame, (O_i *oi, int n));

XV_FUNC(int, set_oi_src_parameter, (O_i *oi, int param_nb, float value, char *type));
/* width and height = 1 means 512 screen pixels (for screen_scale = PIXEL_0) */

XV_FUNC(int, set_oi_horizontal_extend, (O_i *oi, float width));
XV_FUNC(int, set_oi_vertical_extend, (O_i *oi, float height));
# define    get_oi_horizontal_extend(oi) ((oi) == NULL) ? \
                                                xvin_error(Wrong_Argument) : (oi)->width
# define    get_oi_vertical_extend(oi) ((oi) == NULL) ? \
                                              xvin_error(Wrong_Argument) : (oi)->height

XV_FUNC(int, map_pixel_ratio_of_image_and_screen, (O_i *oi, float ratiox, float ratioy));
XV_FUNC(int, what_is_pixel_ratio_of_image_and_screen, (O_i *oi, float *ratiox, float *ratioy));
XV_FUNC(int, smart_map_pixel_ratio_of_image_and_screen, (O_i *oi));

# define    get_oi_x_user_coordinate(x) (oi)->ax + (oi)->dx * (x)
# define    get_oi_y_user_coordinate(y) (oi)->ay + (oi)->dy * (y)
# define    get_oi_z_user_coordinate(z) (oi)->az + (oi)->dz * (z)

XV_FUNC(char *, get_oi_x_unit_name, (O_i *oi));
XV_FUNC(char *, get_oi_y_unit_name, (O_i *oi));
XV_FUNC(char *, get_oi_z_unit_name, (O_i *oi));

# define    set_oi_grid(oi,on_off)      (oi)->iopt = ((on_off) == ON)\
                                                     ? (oi)->iopt & ~NOAXES :\
                                                                            (oi)->iopt | NOAXES
# define    get_oi_grid(oi)         (((oi)->opt & NOAXES) \
                                     ? OFF : ON)



# define    USER        0
# define    PIXEL       1
# define    NO_NUMBER   2




XV_FUNC(int, set_oi_bottom_left_axes, (O_i *oi, int mode));
XV_FUNC(int, is_oi_bottom_left_axes_in_pixel, (O_i *oi));
XV_FUNC(int, is_oi_bottom_left_axes_numbered, (O_i *oi));
XV_FUNC(int, set_oi_top_right_axes, (O_i *oi, int pixel_user_nonumber_none));
XV_FUNC(int, is_oi_top_right_axes_in_pixel, (O_i *oi));
XV_FUNC(int, is_oi_top_right_axes_numbered, (O_i *oi));
XV_FUNC(int, set_oi_plot_axes, (O_i *oi, int mode));
XV_FUNC(int, get_oi_plot_axes, (O_i *oi));
XV_FUNC(int, is_oi_x_axes_same_as_plot, (O_i *oi));
XV_FUNC(int, is_oi_y_axes_same_as_plot, (O_i *oi));

/*
   set_oi_region_of_interest(oi,pixel_user,x0,y0,x1,y1)
   get_oi_region_of_interest(oi,pixel_user,*x0,*y0,*x1,*y1)


*/

XV_FUNC(int, set_zmin_zmax_values, (O_i *oi, float zmin, float zmax));
XV_FUNC(int, set_RGB_zmin_zmax_values, (O_i *oi, float Rzmin, float Rzmax, float Gzmin, float Gzmax, float Bzmin, float Bzmax));
XV_FUNC(int, set_z_black_z_white_values, (O_i *oi, float zb, float zw));
XV_FUNC(int, get_zmin_zmax_values, (O_i *oi, float *zmin, float *zmax));
XV_FUNC(int, get_z_black_z_white_values, (O_i *oi, float *zb, float *zw));
XV_FUNC(int, get_RGB_zmin_zmax_values, (O_i *oi, float *Rzmin, float *Rzmax, float *Gzmin, float *Gzmax, float *Bzmin, float *Bzmax));
XV_FUNC(O_p *, create_and_attach_op_to_oi, (O_i *oi, int nx, int ny, int type, int axes_type));
XV_FUNC(d_s *, create_and_attach_ds_to_oi, (O_i *oi, int nx, int ny, int type));
XV_FUNC(O_p *, find_oi_cur_op, (O_i *oi));
XV_FUNC(p_l *, create_and_attach_label_to_oi, (O_i *oi, float x, float y, int type, const char *format, ...)) __attribute__((format(printf, 5, 6)));
XV_FUNC(un_s *, create_and_attach_unit_set_to_oi, (O_i *oi, int type, float ax, float dx, char decade, char mode,
        char *name, int axis));
/* for movies */
/*
#define create_attach_select_x_un_to_oi(oi, type, ax, dx, decade, mode, name)\
create_and_attach_unit_set_to_oi((oi), (type), (ax), (dx), (decade), (mode), (name), IS_X_UNIT_SET)

#define create_attach_select_y_un_to_oi(oi, type, ax, dx, decade, mode, name)\
create_and_attach_unit_set_to_oi((oi), (type), (ax), (dx), (decade), (mode), (name), IS_Y_UNIT_SET)
*/
XV_FUNC(un_s *, create_attach_select_x_un_to_oi, (O_i *oi, int type, float ax, float dx, char decade, char mode,
        char *name));
XV_FUNC(un_s *, create_attach_select_y_un_to_oi, (O_i *oi, int type, float ax, float dx, char decade, char mode,
        char *name));


XV_FUNC(int, push_image_label, (O_i *oi, float tx, float ty, char *label, int type));

# define    remove_cur_op_from_oi(oi)   remove_from_one_image ((oi),\
                                                               IS_ONE_PLOT, \
                                                               (void*)  find_oi_cur_op(oi))
# define    remove_op_from_oi(oi,op)    remove_from_one_image ((oi),\
                                                               IS_ONE_PLOT, (void*)(op))

# define    remove_label_from_oi(oi,pl) remove_from_one_image ((oi),\
                                                               IS_PLOT_LABEL, (void*)(pl))
# define    remove_x_unit_set_from_oi(oi,un)    \
                                                                            remove_from_one_image ((oi),\
                                                                                                   IS_X_UNIT_SET, (void*)(un))
# define    remove_y_unit_set_from_oi(oi,un)    \
                                                                            remove_from_one_image ((oi),\
                                                                                                   IS_Y_UNIT_SET, (void*)(un))
# define    remove_z_unit_set_from_oi(oi,un)    \
                                                                            remove_from_one_image ((oi),\
                                                                                                   IS_Z_UNIT_SET, (void*)(un))


# define    get_oi_float_dat(oi, row, line)     (oi)->im.pixel[\
                                                                            (line)].fl[(row)]
# define    set_oi_float_dat(oi, row, line, val)    (oi)->im.pixel[(line)]\
                                                                            .fl[(row)] = val
# define    get_oi_int_dat(oi, row, line)       (oi)->im.pixel[(line)]\
                                                                            .in[(row)]
# define    set_oi_int_dat(oi, row, line, val)  (oi)->im.pixel[(line)]\
                                                                            .in[(row)] = val
# define    get_oi_char_dat(oi, row, line)      (oi)->im.pixel[(line)]\
                                                                            .ch[(row)]
# define    set_oi_char_dat(oi, row, line, val)     (oi)->im.pixel[(line)]\
                                                                            .ch[(row)] = val

XV_FUNC(unsigned char *, get_oi_char_line_ptr, (O_i *oi, int line));
XV_FUNC(short int *, get_oi_int_line_ptr, (O_i *oi, int line));
XV_FUNC(float *, get_oi_float_line_ptr, (O_i *oi, int line));
XV_FUNC(float *, get_oi_complex_line_ptr, (O_i *oi, int line));


XV_FUNCPTR(int, image_special_2_use, (O_i *oi));
XV_FUNCPTR(int, image_use_2_special, (O_i *oi));
XV_FUNCPTR(int, image_free_use, (O_i *oi));
XV_FUNCPTR(int, image_duplicate_use, (O_i *oid, O_i *ois));


XV_FUNC(int, set_all_pixel_to_color, (O_i *dest, int bw, double flt_dbl_val));
XV_FUNC(int, copy_and_swap_one_subset_of_image_in_a_bigger_one, (O_i *dest, int posx, int posy, O_i *ois, int x_start,
        int y_start, int nx, int ny));
XV_FUNC(int, copy_one_subset_of_image_in_a_bigger_one, (O_i *dest, int posx, int posy, O_i *ois, int x_start,
        int y_start, int nx, int ny));
XV_FUNC(int, copy_or_add_one_subset_of_image_in_a_bigger_one, (O_i *dest, int posx, int posy, O_i *ois, int x_start, int y_start,  int nx, int ny, int add));
XV_FUNC(int, find_zmin_zmax, (O_i *oi));
XV_FUNC(int, find_zmin_zmax_and_pos, (O_i *oi, int *x_min, int *x_max, int *y_min, int *y_max));
XV_FUNC(int, save_one_image, (O_i *oi, char *file));
XV_FUNC(int, save_one_image_to_fp, (O_i *oi, FILE *fp));
XV_FUNC(int, decribe_image, (O_i *oi, char *des, int size_des));

XV_FUNC(int, inherit_from_im_to_im, (O_i *dest, O_i *src));
XV_FUNC(int, inherit_from_im_to_ds, (d_s *dest, O_i *src));
XV_FUNC(int, inherit_from_ds_to_ds, (d_s *dest, const d_s *src));

# define    inherit_from_oi_to_oi       inherit_from_im_to_im
# define    inherit_from_oi_to_ds       inherit_from_im_to_ds

XV_FUNCPTR(int,(switch_frame_of_disk_movie_c), (O_i *oi, int n));


XV_FUNC(int, fast_interpolate_image_point, (O_i *ois, float x, float y, int out_color));
XV_FUNC(float, interpolate_image_point, (O_i *ois, float x, float y, float *zr, float *zi));
XV_FUNC(int, interpolate_RGB_picture_point, (O_i *ois, float x, float y, float *zr, float *zg, float *zb, float *za));

XV_FUNC(int, set_oi_x_unit_set, (O_i *oi, int n));
XV_FUNC(int, set_oi_y_unit_set, (O_i *oi, int n));
XV_FUNC(int, set_oi_z_unit_set, (O_i *oi, int n));
XV_FUNC(int, set_oi_t_unit_set, (O_i *oi, int n));


XV_FUNC(int, uns_oi_2_op, (O_i *ois, int oi_type, O_p *op, int op_type));
XV_FUNC(int, uns_oi_2_oi, (O_i *dest, O_i *src));
XV_FUNC(int, uns_op_2_oi, (O_p *op, int op_type, O_i *oid, int oi_type));
XV_FUNC(int, uns_oi_2_oi_by_type, (O_i *ois, int ois_type, O_i *oid, int oid_type));
XV_FUNC(int, affine_transform_all_image_unitset, (O_i *oi, int type, float offset, float multiplier));

XV_FUNC(int, find_zmax_and_pos_interpolate, (O_i *oi, float *max_val, float *x_fmax, float *y_fmax));
XV_FUNC(int, find_zmax_excluding_an_area,(O_i *oi, int xc, int dx, int yc, int dy, float *max_val, float *z2acg));
XV_FUNC(O_i*, average_partial_movie, (O_i *ois1, O_i *dest, int start, int n_frames));

XV_FUNC(int, swap_image, (O_i *oi));

XV_FUNC(int, find_op_nb_of_source_specific_ds_in_oi, (O_i *oi, char *source));
XV_FUNC(int, find_op_nb_of_treatement_specific_ds_in_oi, (O_i *oi, char *treatement));

/* type may be  IS_X_UNIT_SET IS_Y_UNIT_SET IS_Z_UNIT_SET IS_T_UNIT_SET */


# define PIXEL_0    14
# define PIXEL_1    28
# define PIXEL_2    56
# define Z_MIN      128
# define Z_MAX      256


# define    IM_USR_COOR_LB  0x4000  /* uses im units not pixels(left,bot)*/
# define    IM_USR_COOR_RT  0x2000  /* uses im units not pixels (rigt,top)*/

# define    IM_SAME_X_AXIS  8
# define    IM_SAME_Y_AXIS  16
# define    IM_Z_SAME_AS_X_PLOT_AXIS    32
# define    IM_Z_SAME_AS_Y_PLOT_AXIS    64
# define    DS_OF_OP_SYNCHRONIZE_WITH_MOVIE 128
//# undef PATTERN
//# define    PATTERN 8


XV_FUNC(int, set_image_starting_time, (O_i *oi));
XV_FUNC(int, set_image_ending_time, (O_i *oi));

XV_FUNC(S_l *, add_screen_label, (O_i *oi, float x, float y, int type, char *text,
                                  int (*draw_shape)(S_l *s_l, O_i *oi, void *imr)));
XV_FUNC(int, remove_all_screen_label_from_movie, (O_i *oi));
XV_FUNC(int, remove_one_screen_label, (O_i *oi, struct screen_label *s_l));
XV_FUNC(int, remove_all_screen_label_from_one_frame, (O_i *oi, int im));
XV_FUNC(int, alloc_im_ext_array, (im_ext_ar *iea, int size));
XV_FUNC(int, free_im_ext_array, (im_ext_ar *iea));
XV_FUNC(int, find_in_im_extremum, (im_ext_ar *iea, int x, int y, int tolerence));
XV_FUNC(int, add_pt_to_im_extremum, (im_ext_ar *iea, int i_ie, int x, int y, double data));
XV_FUNC(int, finalize_im_extremum, (im_ext_ar *iea));
XV_FUNC(im_ext_ar *, find_maxima_position, (O_i *oi, double threshold,  int tolerence, int max_number, int useROI));




#endif
