
# ifndef _GR_FILE_H_
# define _GR_FILE_H_

# include "platform.h"
# include <stdarg.h>
# include <stdio.h>

struct input_file
{
	int n_line;
	char *filename;
	FILE *fpi;
};


# define L_SIZE 	32
# define B_LINE 	65536
# define I_F_SIZE 	8
# define MAX_ERROR 	20
# define OP_SIZE 	32
# define CRT_Z 		26
# define CMID		27
# define SWAP_XY	32
# define GR_SIZE	16384

# ifndef _GR_FILE_C_
XV_ARRAY(struct input_file, i_f);
XV_VAR(char*, lferror);
XV_VAR(int, n_error);
XV_VAR(int, cur_i_f);
XV_ARRAY(char, f_in);
# else
struct input_file i_f[I_F_SIZE];
char 	*lferror;
// char 	data_path[256];
int 	n_error=0;
int 	cur_i_f=0;
char 	f_in[256];
# endif
XV_FUNC(int, get_label, (char **c1, char **c2, char *line));
XV_FUNC(char*, get_next_line, (char *line));
XV_FUNC(int, error_in_file, ( char *fmt, ...) __attribute__((format(printf, 1, 2))));
XV_FUNC(int, gr_numbi, (int *np, int *argcp, char ***argvp));
XV_FUNC(int, gr_numb, (float *np, int *argcp, char ***argvp));

# endif




