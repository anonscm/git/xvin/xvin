#ifndef _PLATEFORM_H_
#define _PLATEFORM_H_

#ifndef XV_WIN32
# include "config.h"
#endif

#ifdef DMALLOC
#include "dmalloc.h"
#endif

#ifdef FORTIFY
#include "fortify.h"
#endif

#ifdef XV_WIN32
/* describe how function prototypes look in win32 DLL */
#if (defined XVIN_STATICLINK) 
#define _XV_DLL
#elif (defined BUILDING_DLL)
#define _XV_DLL   __declspec(dllexport)
#else
#define _XV_DLL   __declspec(dllimport)
#endif

/* 	the same, for plug-ins compilation */
#if (defined XVIN_STATICLINK) 
#define _PXV_DLL
#elif (defined BUILDING_PLUGINS_DLL)
#define _PXV_DLL   __declspec(dllexport)
#elif (defined BUILDING_DLL)
#define _PXV_DLL   __declspec(dllexport)
#else
#define _PXV_DLL   __declspec(dllimport)
#endif
#endif /* XV_WIN32  */


/* defines for the Linux version:*/
#ifdef XV_UNIX 
#define  _XV_DLL // no need to define those guys
#define _PXV_DLL
#endif         /* XV_UNIX  */


/* defines for the Mac OSX version:*/
#ifdef XV_MAC 
#define  _XV_DLL // no need to define those guys
#define _PXV_DLL
#endif         /* XV_MAC  */


/* below are common to all platforms : */
#ifdef XV_WIN32
#define XV_VAR(type, name)                   extern _XV_DLL type name
#define XV_ARRAY(type, name)                 extern _XV_DLL type name[]
#ifdef __cplusplus
#define XV_FUNC(type, name, args)            extern "C" type name args
#else
#define XV_FUNC(type, name, args)            extern type name args
#endif
#define XV_METHOD(type, name, args)          type (*name) args
#define XV_FUNCPTR(type, name, args)         extern _XV_DLL type (*name) args

#define PXV_VAR(type, name)                   extern _PXV_DLL type name
#define PXV_ARRAY(type, name)                 extern _PXV_DLL type name[]
#ifdef __cplusplus
#define PXV_FUNC(type, name, args)            extern "C" type name args
#else
#define PXV_FUNC(type, name, args)            extern type name args
#endif
#define PXV_METHOD(type, name, args)          type (*name) args
#define PXV_FUNCPTR(type, name, args)         extern _PXV_DLL type (*name) args
#else
#ifdef __cplusplus
#define XV_FUNC(type, name, args)            extern "C" type name args
#else
#define XV_FUNC(type, name, args)            extern type name args
#endif
#define XV_VAR(type, name)                   extern type name
#define XV_ARRAY(type, name)                 extern type name[]
#define XV_METHOD(type, name, args)          type (*name) args
#define XV_FUNCPTR(type, name, args)         extern type (*name) args

#define PXV_VAR(type, name)                   extern  type name
#define PXV_ARRAY(type, name)                 extern  type name[]

#ifdef __cplusplus
#define PXV_FUNC(type, name, args)            extern "C" type name args
#else
#define PXV_FUNC(type, name, args)            extern type name args
#endif

#define PXV_METHOD(type, name, args)          type (*name) args
#define PXV_FUNCPTR(type, name, args)         extern type (*name) args

#endif
// #ifndef XV_FUNC // this was the linux/macos definition, curiously different ?
//    #define XV_FUNC(type, name, args)               type name args
// #endif

#ifndef XV_PRINTFUNC
#define XV_PRINTFUNC(type, name, args, a, b)    XV_FUNC(type, name, args)
#endif

#ifndef XV_FUNCPTRARRAY
#define XV_FUNCPTRARRAY(type, name, args)       extern type (*name[]) args
#endif

#endif 
