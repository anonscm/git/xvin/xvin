# ifndef _IM_GR_H_
# define _IM_GR_H_



# include "platform.h"
# include "unitset.h"
# include "xvinerr.h"
# include "util.h"
# include "color.h"
# include "gr_file.h"
# include "box_def.h"
# include "plot_ds.h"
# include "plot_op.h"
# include "plot_opt.h"
# include "gui_def.h"
# include "plot2box.h"
# include "gr_file.h"
# include "im_oi.h"
# include "im_reg.h"


# ifndef _IM_GR_C_
XV_VAR(char*, data_path);
XV_VAR(int,	swap_mode);
XV_VAR(int,	movie_buffer_on_disk);

# endif

XV_FUNC(int, load_im_file_in_imreg, (imreg *imr, char *file, char *path));
XV_FUNC(O_i*, load_im_file_in_oi, (char *file, char *path));
XV_FUNC(O_i*, load_im_fullfile_in_oi, (char *fullfile));
XV_FUNC(int, imreadfile, (O_i *oi, char *file_name));
XV_FUNC(int, check_if_gr_file_is_an_image, (char *file_name));
XV_FUNC(int, set_image_opts, (O_i *oi, int argc, char **argv));
XV_FUNC(int, imxlimread, (O_i *oi, int *argcp, char ***argvp));
XV_FUNC(int, imylimread, (O_i *oi, int *argcp, char ***argvp));
XV_FUNC(int, imzlimread, (O_i *oi, int *argcp, char ***argvp));
XV_FUNC(int, push_image, (O_i *oi, char *filename, int type, int mode));
XV_FUNC(int, push_swap_image, (O_i *oi, char *filename, int type, int mode));

XV_FUNC(int, switch_frame_of_disk_movie, (O_i *oi, int f));
XV_FUNC(int, find_CTRL_Z_offset, (O_i *oi, FILE *fp));
XV_FUNC(int, save_image_header, (O_i* oi, char *head, int size));
XV_FUNC(int, close_movie_on_disk, (O_i *oi, int nf));

XV_FUNC(char* ,read_image_header, (O_i* oi, int *size));

#endif 









