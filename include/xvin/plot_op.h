/*  This series of functions handles the one plot structure
 *
 */


#ifndef _PLOT_OP_H_
#define _PLOT_OP_H_

# include "platform.h"
# include "plot_ds.h"
# include "plot_opt.h"
# include "gui_def.h"


typedef struct one_plot
{
    int type;
    char *filename, *dir, *title, *x_title, *y_title;
    char *x_prime_title, *y_prime_title;
    char *x_prefix, *y_prefix, *t_prefix, *x_unit, *y_unit, *t_unit;
    float width, height, right, up;
    float ax, dx, ay, dy, at, dt;
    int iopt, iopt2;
    float tick_len, tick_len_x, tick_len_y;
    float x_lo, x_hi, y_lo, y_hi;
    struct data_set **dat;      /* the real data */
    int n_dat, m_dat;       /* the number of data set*/
    int cur_dat;            /* the current one */
    int display_single_ds;
    struct plot_label **lab;    /* labelling in the plot */
    int n_lab, m_lab;       /* the last and size*/
    un_s    **xu;           /* x unit set */
    int n_xu, m_xu, c_xu,  c_xu_p;
    un_s    **yu;           /* y unit set */
    int n_yu, m_yu, c_yu,  c_yu_p;
    un_s    **tu;           /* t unit set (associated with index) */
    int n_tu, m_tu, c_tu, c_tu_p;
    struct box  bplt;       /* the associated box plot */
    int need_to_refresh;
    int data_changing;
    int transfering_data_to_box;
    int user_id;                  /* a user identifier */
    bool read_only; /* can't be removed by user interface */
    int user_ispare[16];
    float user_fspare[16];
    void* user_vspare[16];
    int (*op_idle_action)(struct one_plot *op, DIALOG *d); //  DIALOG *d
    int (*op_got_mouse)(struct one_plot *op, int xm_s, int ym_s, int mode);
    int (*op_lost_mouse)(struct one_plot *op, int xm_s, int ym_s, int mode);
    int (*op_end_action)(struct one_plot *op, DIALOG *d);
    int (*op_post_display)(struct one_plot *op, DIALOG *d); /* used to add graphis element just before blit*/
    char *(*op_idle_point_add_display)(struct one_plot *op, DIALOG *d, int nearest_ds,
                                       int nearest_point); /* used to add graphis element just before blit*/

} O_p;

# define    X_AXIS      2304    /* 2048 + 256 */
# define    Y_AXIS      2048
# define    Z_AXIS      2560    /* 2048 + 512 */
# define    T_AXIS      3072    /* 2048 + 1024 */

# define     INSERT_HERE  8192

XV_FUNC(O_p *, create_one_plot, (int nx, int ny, int data_type));
XV_FUNC(O_p*,  create_one_empty_plot, (void));
XV_FUNC(d_s *, create_and_attach_one_ds, (O_p *op, int nx, int ny, int data_type));
XV_FUNC(int, create_and_attach_n_ds,(int nop, O_p *op,int nx, int ny, int data_type));
XV_FUNC(int, free_one_plot, (O_p *op));
XV_FUNC(O_p *, duplicate_plot_data, (O_p *src, O_p *dest));
XV_FUNC(int, add_one_plot_data, (O_p *op, int type, void *stuff));
XV_FUNC(int, add_data_to_one_plot, (O_p *op, int type, void *stuff));
XV_FUNC(int, remove_data_from_one_plot, (O_p *op, int type, void *stuff));
# define remove_data_from_one_plot (op, type, stuff) remove_one_plot_data ((op), (type), (stuff))
XV_FUNC(int, remove_one_plot_data, (O_p *op, int type, void *stuff));
XV_FUNC(int, init_one_plot, (O_p *op));
XV_FUNC(int, do_it_one_plot_op, (O_p *o_p));
XV_FUNC(int, swap_x_y_in_plot, (O_p *op));
XV_FUNC(int, set_op_x_unit_set, (O_p *op, int n));
XV_FUNC(int, set_op_y_unit_set, (O_p *op, int n));
XV_FUNC(int, set_op_t_unit_set, (O_p *op, int n));
XV_FUNC(int, find_x_limits, (O_p *op));
XV_FUNC(int, find_y_limits, (O_p *op));
XV_FUNC(int, set_plot_title, (O_p *op, const char *format, ...) __attribute__ ((format (printf, 2, 3))));
XV_FUNC(int, set_plot_x_title, (O_p *op, const char *format, ...) __attribute__ ((format (printf, 2, 3))));
XV_FUNC(int, set_plot_y_title, (O_p *op, const char *format, ...) __attribute__ ((format (printf, 2, 3))));
XV_FUNC(int, set_plot_x_prime_title, (O_p *op, const char *format, ...) __attribute__ ((format (printf, 2, 3))));
XV_FUNC(int, set_plot_y_prime_title, (O_p *op, const char *format, ...) __attribute__ ((format (printf, 2, 3))));
XV_FUNC(int, set_plot_y_log, (O_p *op));
XV_FUNC(int, set_plot_x_log, (O_p *op));
XV_FUNC(int, is_plot_y_log, (O_p *op));
XV_FUNC(int, is_plot_x_log, (O_p *op));
XV_FUNC(int, set_plot_y_lin, (O_p *op));
XV_FUNC(int, set_plot_x_lin, (O_p *op));
XV_FUNC(int, set_plot_x_fixed_range, (O_p *op));
XV_FUNC(int, set_plot_x_auto_range, (O_p *op));
XV_FUNC(int, is_plot_x_auto_range, (O_p *op));
XV_FUNC(int, set_plot_y_fixed_range, (O_p *op));
XV_FUNC(int, set_plot_y_auto_range, (O_p *op));
XV_FUNC(int, is_plot_y_auto_range, (O_p *op));
XV_FUNC(int, adjust_limits_on_ds_range, (O_p *op, int dsidx));
# define set_plot_filename set_op_filename
# define set_plot_file set_op_filename
# define set_plot_path set_op_path

XV_FUNC(int, set_op_filename, (O_p *op, const char *format, ...) __attribute__ ((format (printf, 2, 3))));
XV_FUNC(int, set_op_path, (O_p *op, const char *format, ...) __attribute__ ((format (printf, 2, 3))));
XV_FUNC(char *, get_op_filename, (O_p *op));
XV_FUNC(char *, get_op_path, (O_p *op));

XV_FUNC(int, push_plot_label, (O_p *op, float tx, float ty, char *label, int type));
XV_FUNC(int, set_op_plot_label, (O_p *op, float tx, float ty, int type, char *format, ...) __attribute__ ((format (printf, 5, 6))));
XV_FUNC(int, set_op_width, (O_p *op, float width));
XV_FUNC(int, set_op_height, (O_p *op, float height));
XV_FUNC(float, get_op_width, (O_p *op));
XV_FUNC(float, get_op_height, (O_p *op));
XV_FUNC(d_s *, get_op_cur_ds, (O_p *op));
XV_FUNC(int, get_ds_number_in_op, (O_p *op, d_s *ds));
XV_FUNC(int, create_attach_select_x_un_to_op, (O_p *op, int type, float ax, float dx, char decade, char mode,
        char *name));
XV_FUNC(int, create_attach_select_y_un_to_op, (O_p *op, int type, float ax, float dx, char decade, char mode,
        char *name));
XV_FUNC(int, create_attach_select_t_un_to_op, (O_p *op, int type, float ax, float dx, char decade, char mode,
        char *name));
XV_FUNC(int, add_ds_to_op, (O_p *op, d_s *ds));
XV_FUNC(int, add_plot_label_to_op, (O_p *op, p_l *pl));
XV_FUNC(int, add_x_unit_set_to_op, (O_p *op, un_s *un));
XV_FUNC(int, add_y_unit_set_to_op, (O_p *op, un_s *un));
XV_FUNC(int, remove_ds_from_op, (O_p *op, d_s *ds));
XV_FUNC(int, remove_ds_n_from_op, (O_p *op, int n));
XV_FUNC(int, remove_plot_label_from_op, (O_p *op, p_l *pl));
XV_FUNC(int, remove_x_unit_set_from_op, (O_p *op, un_s *un));
XV_FUNC(int, remove_y_unit_set_from_op, (O_p *op, un_s *un));
XV_FUNC(int, cancel_offset_in_op_unit_set, (O_p *op, int type));

XV_FUNC(d_s *, find_source_specific_ds_in_op, (O_p *op, char *source));
XV_FUNC(d_s *, find_treatement_specific_ds_in_op, (O_p *op, char *treatement));

XV_FUNC(int, affine_transform_all_plot_unitset, (O_p *op, int axis, float offset, float multiplier));
XV_FUNC(int, get_afine_param_from_op, (O_p *op, int axis, float *ax, float *dx));
XV_FUNC(int, switch_plot_data_set, (O_p *op, int n_dat));

XV_FUNC(int, uns_op_2_op_by_type, (const O_p *ops, int ops_type, O_p *opd, int opd_type));
XV_FUNC(int, uns_op_2_op, (O_p *dest, const O_p *src));

XV_FUNC(char *, create_description_label, (O_p *op, int n_max_ds));
XV_FUNC(int, add_new_point_to_op_and_ds, (O_p *op, d_s *ds, float x, float y));

XV_FUNC(int, find_nearest_points_in_plot, (O_p *op, float x, float y, int *nearest_ds, int *nearest_point,
        int time_out));

# endif
