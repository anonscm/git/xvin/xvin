
/*	This series of functions handles the plot_region
 *
 *
 *
 */


#ifndef _PLOT_DS_H_
#define _PLOT_DS_H_

# include "platform.h"
# include "xvinerr.h"
# include "unitset.h"
# include "util.h"
# include "box_def.h"

# define MAX_DATA 		16

# define 	IS_SPECIAL		32
# define 	ALL_SPECIAL		33
# define 	IS_ONE_PLOT		1024
# define 	IS_ONE_IMAGE		8192
# define 	IS_DATA_SET		2048
# define 	IS_PLOT_LABEL		4096
# define 	IS_X_UNIT_SET		16
# define 	IS_Y_UNIT_SET		17
# define 	IS_Z_UNIT_SET		18
# define 	IS_T_UNIT_SET		19
# define 	IS_UNIT_SET		20

# define 	ABS_COORD		0
# define 	USR_COORD		1
# define 	VERT_LABEL_USR		2
# define 	VERT_LABEL_ABS		3
# define 	WHITE_LABEL		16
# define 	OVER_WHITE 		32

#ifndef _PLOT_DS_C_
XV_VAR(int*, data_color);
XV_VAR(int,	max_data_color);
XV_VAR(char**, data_symbols);
XV_VAR(int,	max_data_symbols);
XV_VAR(bool, keep_color);
# endif

typedef struct plot_label
{
	int type;			/* absolute or user defined */
	float xla, yla;			/* the position of label */
	char *text;			/* the text of it */
	struct box *b;			/* the plot box */
} p_l;

# define	get_pl_type(pl)		((pl) == NULL) ? 0 : (pl)->type
# define	set_pl_type(pl,type)	if ((pl) != NULL) (pl)->type = (type)
# define	get_pl_x(pl)		((pl) == NULL) ? 0 : (pl)->xla
# define	set_pl_x(pl,x)		if ((pl) != NULL) (pl)->xla = xla
# define	get_pl_y(pl)		((pl) == NULL) ? 0 : (pl)->yla
# define	set_pl_y(pl,y)		if ((pl) != NULL) (pl)->yla = yla
# define	get_pl_text(pl)		((pl) == NULL) ? NULL : (pl)->text

XV_FUNC(p_l*, create_plot_label, (int type, float x, float y, char *format,...) __attribute__ ((format (printf, 4, 5))));
XV_FUNC(p_l*, duplicate_plot_label, (p_l *pls));
XV_FUNC(int, free_plot_label, (p_l *pl));

typedef struct data_set
{
  int nx, ny, type;		/* number of x and y data */
  int mx, my;			/* size alloc for x and y */
  float *xd, *yd;			/* the x and y data */
  float *xe, *ye;			/* the x and y error on data, symetric if xeb and yev are not defined, error above xd otherwise, (max value for boxplot)*/
  // TODO EXPORT DATA IN GR
  float *xed, *yed;       /* the x and y error on data, below the point (min value for boxplot)*/
  float *xbu, *ybu;   /* the x and y upperside of the box for boxplot, if xbd and ybd is not define, the box will be symetric */
  float *xbd, *ybd;    /* the x and y downside of the box for boxplot */
  float boxplot_width;
  // END TODO
  char *symb;			/* the symbol to plot */
  int m;				/* the line style */
  int color;			/* the drawing color */
  time_t time;			/* date of creation */ /* long int changed into time_t 2006/11/10, NG */
  char *source;
  char *history;
  char *treatement;
  char **special;
  int n_special, m_special;
  struct plot_label **lab;	/* labelling in the plot attached to ds */
  int n_lab, m_lab;		/* the last and size*/
  struct hook use;		/* usage of the data set */
  int need_to_refresh;
  double record_duration;         /* duration of data acquisition in micro seconds */
  long long record_start;         /* start of duration of data */
  int invisible;                  /* if set make the dataset invisible, but data is still there */
  int user_ispare[16];
  float user_fspare[16];
  float src_parameter[8];        // numerical value of parameter characterizing data (ex. temperature ...)
  char *src_parameter_type[8];   // the description of these parameters
} d_s;


XV_FUNC(d_s*, build_data_set, (int nx, int ny));
XV_FUNC(d_s*, build_adjust_data_set, (d_s *ds, int nx, int ny));
XV_FUNC(d_s*, duplicate_data_set, (const d_s *src, d_s *dest));
XV_FUNC(d_s*, duplicate_partial_data_set, (const d_s *src, d_s *dest, int istart, int size));
XV_FUNC(int, free_data_set, (d_s *ds));
XV_FUNC(int, add_new_point_to_ds, (d_s *ds, float x, float y));
XV_FUNC(int, add_new_point_with_y_error_to_ds, (d_s *ds, float x, float y, float dy));
XV_FUNC(int, add_new_point_with_x_error_to_ds, (d_s *ds, float x, float dx, float y));
XV_FUNC(int, add_new_point_with_xy_error_to_ds, (d_s *ds, float x, float dx, float y, float dy));
XV_FUNC(int, insert_new_point_in_ds, (d_s *ds, int pt_index, float x, float y, float dx, float dy));


# define	get_ds_nx(ds)		((ds) == NULL) ? 0 : (ds)->nx
# define	get_ds_ny(ds)		((ds) == NULL) ? 0 : (ds)->ny
# define 	set_ds_x_point(ds,x,i)	\
	if ((ds) != NULL && (i) >= 0 && (i) < (ds)->nx) (ds)->xd[(i)] = (x)
# define 	set_ds_y_point(ds,y,i)	\
	if ((ds) != NULL && (i) >= 0 && (i) < (ds)->ny) (ds)->yd[(i)] = (y)
# define 	set_ds_point(ds,x,y,i)	\
	if ((ds) != NULL && (i) >= 0 && (i) < (ds)->nx && (i) < (ds)->ny) \
	{ (ds)->xd[(i)] = (x); (ds)->yd[(i)] = (y) }
# define 	get_ds_x_point(ds,i)	\
	((ds) != NULL && (i) >= 0 && (i) < (ds)->nx) ? (ds)->xd[(i)] : 0
# define 	get_ds_y_point(ds,i)	\
	((ds) != NULL && (i) >= 0 && (i) < (ds)->ny) ? (ds)->yd[(i)] : 0

XV_FUNC(float*, get_ds_x_ptr, (d_s *ds));
XV_FUNC(float*, get_ds_y_ptr, (d_s *ds));

# define 	set_dash_line		set_ds_dash_line
# define 	set_dot_line		set_ds_dot_line
# define 	set_plain_line		set_ds_plain_line
# define 	set_line_color		set_ds_line_color
# define 	set_plot_symb		set_ds_point_symbol

XV_FUNC(int, set_ds_dash_line, (d_s *ds));
XV_FUNC(int, set_ds_dot_line, (d_s *ds));
XV_FUNC(int, set_ds_plain_line, (d_s *ds));

XV_FUNC(int, set_ds_line_color, (d_s *ds, int color));
# define	get_ds_line_color(ds)	((ds) == NULL) ? 0 : (ds)->color

XV_FUNC(int, set_ds_point_symbol, (d_s *ds, char *symb));
# define	get_ds_point_symbol(ds)	((ds) == NULL) ? NULL : (ds)->symb

XV_FUNC(int, inherit_from_ds_to_ds, (d_s *dest, const d_s *src));

XV_FUNC(int, set_ds_source, (d_s *ds, char *format, ...)  __attribute__ ((format (printf, 2, 3))));
# define	get_ds_source(ds)	((ds) == NULL) ? NULL : (ds)->source

XV_FUNC(int, set_ds_history, (d_s *ds, char *format, ...) __attribute__ ((format (printf, 2, 3))));
# define	get_ds_history(ds)	((ds) == NULL) ? NULL : (ds)->history

XV_FUNC(int, set_ds_treatement, (d_s *ds, char *format, ...) __attribute__ ((format (printf, 2, 3))));
XV_FUNC(int, update_ds_treatement, (d_s *ds, char *format, ...) __attribute__ ((format (printf, 2, 3))));
# define	get_ds_treatement(ds)	((ds) == NULL) ? NULL : (ds)->treatement
XV_FUNC(int, add_to_data_set, (d_s *ds, int type, void *stuff));
XV_FUNC(int, remove_from_data_set, (d_s *ds, int type, void *stuff));
XV_FUNC(int, remove_point_from_data_set, (d_s *src, int n_one));
XV_FUNC(int, remove_all_point_from_data_set, (d_s *src));
XV_FUNC(int, remove_plot_label_from_ds, (d_s *ds, p_l *pl));
XV_FUNC(int, remove_all_plot_label_from_ds, (d_s *ds));
XV_FUNC(int, set_ds_plot_label, (d_s *ds, float tx, float ty, int type,
	const char *format, ...) __attribute__ ((format (printf, 5, 6))));
XV_FUNC(int, push_plot_label_in_ds, (d_s *ds, float tx, float ty, char *label,
	int type));

XV_FUNC(d_s*, alloc_data_set_x_error, (d_s *ds));
XV_FUNC(d_s*, alloc_data_set_x_down_error, (d_s *ds));
XV_FUNC(d_s*, alloc_data_set_x_box, (d_s *ds));
XV_FUNC(d_s*, alloc_data_set_x_box_up, (d_s *ds));
XV_FUNC(d_s*, alloc_data_set_x_box_dwn, (d_s *ds));
XV_FUNC(d_s*, alloc_data_set_y_error, (d_s *ds));
XV_FUNC(d_s*, alloc_data_set_y_down_error, (d_s *ds));
XV_FUNC(d_s*, alloc_data_set_y_box, (d_s *ds));
XV_FUNC(d_s*, alloc_data_set_y_box_up, (d_s *ds));
XV_FUNC(d_s*, alloc_data_set_y_box_dwn, (d_s *ds));
XV_FUNC(int, free_data_set_x_error, (d_s *ds));
XV_FUNC(int, free_data_set_x_down_error, (d_s *ds));
XV_FUNC(int, free_data_set_x_box, (d_s *ds));
XV_FUNC(int, free_data_set_x_box_up, (d_s *ds));
XV_FUNC(int, free_data_set_x_box_dwn, (d_s *ds));
XV_FUNC(int, free_data_set_y_error, (d_s *ds));
XV_FUNC(int, free_data_set_y_down_error, (d_s *ds));
XV_FUNC(int, free_data_set_y_box, (d_s *ds));
XV_FUNC(int, free_data_set_y_box_up, (d_s *ds));
XV_FUNC(int, free_data_set_y_box_dwn, (d_s *ds));
XV_FUNC(int, read_bead_number_from_ds_source,(d_s *src,int *bead_nbr));


XV_FUNC(int, set_src_parameter, (d_s *ds, int param_nb, float value, char *type));

XV_FUNC(int, set_ds_starting_time, (d_s *ds));
XV_FUNC(int, set_ds_ending_time, (d_s *ds));

XV_FUNC(int, find_nearest_points_in_in_ds, (d_s *ds, float x, float y, int* nearest_in_x, int* nearest_in_y));
XV_FUNC(int,	find_nearest_points_in_x_in_ds,(d_s *ds, float x, int* nearest_smaller, int* nearest_larger, float min_y, float max_yl));

XV_FUNC(int, swap_ds_x_y, (d_s *ds));

XV_FUNC(d_s *, ds_log10, (d_s *dsi));
XV_FUNC(d_s *, ds_log, (d_s *dsi));
XV_FUNC(d_s *, ds_log2, (d_s *dsi));
XV_FUNC(d_s *, ds_exp, (d_s *dsi));
XV_FUNC(d_s *, ds_exp2, (d_s *dsi));
XV_FUNC(d_s *, ds_exp10, (d_s *dsi));
XV_FUNC(d_s *, ds_squareroot, (d_s *dsi));
XV_FUNC(d_s *, ds_abs, (d_s *dsi));
XV_FUNC(d_s *, ds_square, (d_s *dsi));
XV_FUNC(d_s *, ds_1_over_y, (d_s *dsi));

XV_FUNCPTR(int,	ds_special_2_use, (d_s *ds));
XV_FUNCPTR(int,	ds_use_2_special, (d_s *ds));
XV_FUNCPTR(int,	ds_free_use,(d_s *ds));
XV_FUNCPTR(int,	ds_duplicate_use, (d_s *dsd, const d_s *dss));



# endif
