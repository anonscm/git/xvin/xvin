#pragma once

#ifndef XV_WIN32
# include "config.h"
#endif

#include "platform.h"
#include <xvin.h>
#include <stdbool.h>
#ifdef XV_GTK_DIALOG
#include <gtk/gtk.h>
#endif
#include <allegro.h>
#include <string.h>
#include <stdio.h>

typedef struct file_list_s {
char *data;
struct file_list_s *next;
} file_list_t;

XV_FUNC(file_list_t, *open_files, (const char *dialog_title, const char *current_uri, const char *filter));
XV_FUNC(file_list_t, *open_files_config, (const char *dialog_title, const char *current_uri, const char *filter, const char *config_cat, const char *config_var));
XV_FUNC(char, *open_one_file, (const char *dialog_title, const char *current_uri, const char *filter));
XV_FUNC(char, *open_one_file_config, (const char *dialog_title, const char *current_uri, const char *filter, const char *config_cat, const char *config_var));
XV_FUNC(char, *save_one_file, (const char *dialog_title, const char *current_dir, const char *filename, const char *filter));
XV_FUNC(char, *save_one_file_config, (const char *dialog_title, const char *current_dir, const char *filename, const char *filter, const char *config_cat, const char *config_var));

XV_FUNC(char, *select_folder, (const char *dialog_title, const char *current_uri));
XV_FUNC(char, *select_folder_config, (const char *dialog_title, const char *current_dir, const char *config_cat, const char *config_var));

#ifndef XV_GTK_DIALOG
XV_FUNC(file_list_t, *win_string_to_file_list, (char *files_str));
#endif
#ifdef XV_GTK_DIALOG
XV_FUNC(void, add_win_filters_to_gtk_choose_dialog, (GtkFileChooser *dialog, const char *filters));
XV_FUNC(file_list_t, *GSList_to_filelist, (GSList *gsl));
#endif
XV_FUNC(void, free_file_list, (file_list_t *file_list));
XV_FUNC(char, *get_default_extension, (const char *filters));
