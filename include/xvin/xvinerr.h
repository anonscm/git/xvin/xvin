/*
 *		Error handling module
 *		At any time you can check wether some error occuerd using :
 *			first_xvin_error(); if an error occured this function
 *	 	will describe it with the filename and the line of code
 *		where it happens; otherwise the fuction return NULL.
 *		Multiple errors may have occured they may be describe
 *		using	next_xvin_error(), at the end of the list, this
 *		function return NULL. When all errors have been treated	 
 *		the error list may be cleared by	 reset_xvin_error();
 *
 *		ex: for(er = first_xvin_error(); er != NULL; er = next_xvin_error())
 *				puts(er);   (char *er) will print all errors	 
 *
 *		to record the occurence of an error use :
 *		int xvin_error(int ntype); or void *xvin_ptr_error(int ntype);
 *		depending of the context. "ntype" may be an already registered
 *		error type like Out_Of_Memory or  it may correspond to the index
 *		returned by register_error(char *newtype);
 *
 *		ex: int New_error = register_error("the new error correspond to ...");
 *		xvin_error(New_error);
*/


/*	error handling routines */

#ifndef _XVINERR_H_
#define _XVINERR_H_

# include "platform.h"

# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <string.h>
# ifdef XV_WIN32   //NG, 2005/07/05
#   include <dos.h>
#   ifndef MVSC
#       include <io.h>
#   endif
# endif

# include <time.h>
# include <stdarg.h>
# include <malloc.h>

typedef struct _xv_err
{
	char	**type;
	int		n_err, m_err, c_err;
} xv_err;

/* error history */

typedef struct _list_xv_err
{
	int		*nerr;
	char 	**file;
	int		*n_line;
	int		n_err, m_err, c_err;
} list_xv_err;

# ifndef _XVINERR_C_
XV_VAR(xv_err, xvin_errors);
XV_VAR(list_xv_err,	last_xvin_errors);
XV_VAR(int, No_error);
XV_VAR(int, Wrong_Argument);
XV_VAR(int, Out_Of_Memory);
XV_VAR(int, Fft_Not_Supported);
XV_VAR(int, Wrong_Option);
XV_VAR(int, No_Data);
# endif

XV_FUNC(int, register_error, (const char *newtype));

/*	define "newtype" as a new error, return an associated unique number for
	this error	*/
	


/*		this is the first error returning 0 which corresponds to no error */

XV_FUNC(char*, describe_error, (int ntype));

XV_FUNC(int, xvin_error_occured, (int ntype, int line, char *file));
XV_FUNC(void*, xvin_ptr_error_occured, (int ntype, int line, char *file));

# define	xvin_error(ntype)	xvin_error_occured((ntype), __LINE__, __FILE__)
# define	xvin_ptr_error(ntype)	\
	xvin_ptr_error_occured((ntype), __LINE__, __FILE__)

# define 	reset_xvin_error()	\
	last_xvin_error.n_err = last_xvin_error.c_err = 0
	
XV_FUNC(char*, first_xvin_error, (void));
XV_FUNC(char*, next_xvin_error, (void));

XV_FUNC(int, dump_to_xv_log_file_with_date_and_time, (const char *fmt, ...) __attribute__ ((format (printf, 1, 2))));
XV_FUNC(int, dump_to_xv_log_file_with_time, (const char *fmt, ...) __attribute__ ((format (printf, 1, 2))));
XV_FUNC(int, dump_to_xv_log_file_only, (const char *fmt, ...) __attribute__ ((format (printf, 1, 2))));

# endif
