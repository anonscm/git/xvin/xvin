
/*	This series of functions handles the plot_region
 *	
 *	
 *
 */

#ifndef _UNITSET_H_
#define _UNITSET_H_

# include "platform.h"

# define 	IS_X_UNIT_SET		16
# define 	IS_Y_UNIT_SET		17
# define 	IS_Z_UNIT_SET		18
# define 	IS_T_UNIT_SET		19
# define 	IS_UNIT_SET		20

# define 	IS_YOTTA		12
# define 	IS_ZETTA		21
# define 	IS_EXA    		18
# define 	IS_PETA   		15
# define 	IS_TERRA		12
# define 	IS_GIGA			9
# define 	IS_MEGA			6
# define 	IS_KILO			3
# define 	IS_MILLI		-3
# define 	IS_MICRO		-6
# define 	IS_NANO			-9
# define 	IS_PICO			-12
# define 	IS_FEMTO		-15
# define 	IS_ATTO		    -18
# define 	IS_ZEPTO	    -21
# define 	IS_YOCTO	   -24

# define 	IS_VOLT			256
# define 	IS_AMPERE		512
# define 	IS_METER		1024
# define 	IS_NEWTON		2048
# define 	IS_SECOND		4096
# define 	IS_GRAMME		8192
# define 	IS_GAUSS		16384
# define 	IS_OHM			32768
# define 	IS_RAW_U		-128 		/* raw data */



typedef struct unit_set
{
	int type, sub_type, axis;	/* identifier */
	float ax, dx;			/* the offset and increment */
	char	decade;			/* the decade value*/
	char	mode;			/* log or a^2 */
	char *name;			/* the text of it */
} un_s;

	/* creator destructor */
XV_FUNC(un_s*, build_unit_set, (int t, float ax, float dx, char d, char m, char *name));
XV_FUNC(int, free_unit_set,(un_s *uns));
XV_FUNC(un_s*, duplicate_unit_set,(un_s *dest, un_s *src));

	/* direct access, encapsulation	*/
# define 	set_unit_offset(un,off)		(un)->ax = (float)(off)
# define 	get_unit_offset(un)		(un)->ax
# define 	set_unit_increment(un,inc)	(un)->dx = (float)(inc)
# define 	get_unit_increment(un)		(un)->dx	
# define 	set_unit_type(un,type)		(un)->type = (int)(type)
# define 	get_unit_type(un)		(un)->type	
# define 	set_unit_sub_type(un,stype)	(un)->sub_type = (int)(stype)
# define 	get_unit_sub_type(un)		(un)->sub_type	
# define 	set_unit_axis(un,axis)		(un)->axis = (int)(axis)
# define 	get_unit_axis(un)		(un)->axis	
# define 	set_unit_decade(un,dec)		(un)->decade = (int)(dec)
# define 	get_unit_decade(un)		(un)->decade	
# define 	set_unit_mode(un,mod)		(un)->mode = (int)(mod)
# define 	get_unit_mode(un)		(un)->mode	
# define 	set_unit_name(un,name)		(un)->name = (char*)(name)
# define 	get_unit_name(un)		(un)->name	

	/* filling units, type, decade	*/
	/* construct a string "units" from the entry "type" and "decade" 
 ex: generate_units(IS_VOLT, IS_MICRO); will produce the string "\mu V"*/
XV_FUNC(char*, generate_units, (int type, int decade));
	/* do the opposite: parse units to determine type and decade */
XV_FUNC(int, unit_to_type, (char *unit, int *type, int *decade));

	/* change decade, determine corresponding units and increment */
XV_FUNC(int, change_decade, (un_s *un, int dec));
	
XV_FUNC(int, compare_units, (un_s *a,un_s *b));


XV_FUNC(int, modify_un_decade, (void));
XV_FUNC(int, modify_units, (void));
XV_FUNC(int, modify_units_prime, (void));
XV_FUNC(int, add_new_units, (void));
XV_FUNC(int, switch_un_ty, (void));
XV_FUNC(int, switch_un_dec, (void));

XV_FUNC(int,	get_afine_param_from_unit, (un_s *un, float *ax, float *dx));
XV_FUNC(int,	set_afine_param_for_unit_with_decade, (un_s *un, float ax, float dx, int decade));

# endif

