#pragma once
#include <gtk/gtk.h>
#include "platform.h"
#include <stdbool.h>

typedef struct
{
    GtkDialog *dialog;
    GtkProgressBar *bar;
    GtkLabel *info_label;
    bool canceled;
} progress_window_t;

XV_FUNC(progress_window_t *, progress_create, (const char *title));
XV_FUNC(int, progress_run, (progress_window_t *progress));
XV_FUNC(int, progress_free, (progress_window_t *progress));

XV_FUNC(int, progress_set_fraction, (progress_window_t *progress, double fraction));
XV_FUNC(double, progress_get_fraction, (progress_window_t *progress));

XV_FUNC(int, progress_pulse, (progress_window_t *progress));
XV_FUNC(int, progress_is_canceled, (progress_window_t *progress));
XV_FUNC(int, progress_update_window, (progress_window_t *progress));

XV_FUNC(int, progress_set_info_text, (progress_window_t *progress, const char *text));
