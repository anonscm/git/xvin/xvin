
#ifndef _GUI_DEF_H_
#define _GUI_DEF_H_

# include "platform.h"
# include "allegro.h"

# define 	YES 		1
# define 	NO 		0
# define OK _Pragma ("GCC warning \"'OK' macro is deprecated, use 'WIN_OK' instead\"") 128
# define	WIN_OK		128
# define	CANCEL _Pragma ("GCC warning \"'CANCEL' macro is deprecated use 'WIN_CANCEL' instead\"") 129
# define	WIN_CANCEL		129


/*	This series of functions handles the most basic events :
 *	the screen contains active boxes which will activate actions
 *	this boxes are in screen coordinates. Three kinds of object have
 *	boxes directly on screen : menus, icone, and acreg (active region). 
 *	These boxes are refered to "area". Menu, acreg and icone, contains
 *	secondary objects which also have dybox with units relative to area.
 *	To each box is attach a specific action.
 */

typedef struct dynamic_box
{
	int x0, y0;		/* position of the box on the screen */
	int x1, y1;		/* in pixel units (absolute) */
	int flag;		/* flag indicating wether the box is mem*/
	BITMAP *b;		/* the context of the box */
	int act;		/* flag specifying the action or fucntion */
	int (*action)(char); 	/* the pointer to the action to be done */
	void *par;		/* parent of the box */
} dybox;

/* flag state may be : */

# define MEMORIZED 	1	
# define NOT_MEMORIZED	~MEMORIZED 	
# define ACTIVATED	2
# define NOT_ACTIVATED	~ACTIVATED




/* 	The rect structure contains the definition of object in acreg 
 *	this definion allow resizeable objets (using RIGHT_SIDE, BOTTOM_LINE,
 *	VFILL_2 and HFILL_2, the displayed size is given by the dybox of the
 *	object. The determination of this dybox is done by 
 * 	int 		evaluate_dybox(dybox* area, dybox* d_b, rec* def);
 *	area is the size of the acreg, def is the definition of the object
 *	d_b is the effective size of the object.
 */

typedef struct rect
{
	int x0;			/* position of the box on the screen */
	int y0;			/* in pixel units (absolute) */
	int w;			/* width */
	int h;			/* height */
} rec;



# ifndef _GUI_DEF_C_
XV_VAR(DIALOG*, the_dialog);
XV_VAR(int, m_the_dialog);
XV_VAR(int, updating_menu_state);
# else
DIALOG *the_dialog = NULL;
int m_the_dialog = 0;
int	updating_menu_state = 0;
# endif


XV_FUNC(int, setsize_dybox, (dybox *db, int x0, int y0, int x1, int y1));
XV_FUNC(int, setsize_rec, (rec* def, int x0, int y0, int width, int height));

XV_FUNC(DIALOG*, attach_new_item_to_dialog, (int (*proc)(int, DIALOG *, int), 
	int key, int flags, int d1, int d2));
XV_FUNC(int, set_dialog_size_and_color, (DIALOG* di, int x, int y, int w, int h, 
	int fg, int bg));
XV_FUNC(int, set_dialog_properties, (DIALOG* di, void* dp, void* dp2, void* *dp3));
XV_FUNC(int, remove_item_from_dialog, (int (*proc)(int, DIALOG *, int), void* dp));
XV_FUNC(int, remove_all_keyboard_proc, (void));
XV_FUNC(int, remove_short_cut_from_dialog, (int key, int scan_code));
XV_FUNC(int, retrieve_item_from_dialog, (int (*proc)(int, DIALOG *, int), void* dp));
XV_FUNC(DIALOG*, retrive_short_cut_from_dialog, (int key, int scan_code));
XV_FUNC(int, retrieve_item_from_dialog, (int (*proc)(int, DIALOG *, int), void* dp));
XV_FUNC(int, retrive_short_cut_index_from_dialog, (int key, int d1));
XV_FUNC(int, retrieve_index_of_menu_from_dialog,(void));
XV_FUNC(int, add_keyboard_short_cut, (int key, int d1, int d2, int (*menu_proc)(void)));
XV_FUNC(int, scan_and_update, (MENU* mn));
XV_FUNC(int, number_of_item_from_dialog, (void));
XV_FUNC(int, do_update_menu,(void));
XV_FUNC(int, xvin_d_menu_proc, (int msg, DIALOG *d, int c));
XV_FUNCPTR(int, before_menu_proc, (int msg, DIALOG *d, int c));
XV_FUNCPTR(int, after_menu_proc, (int msg, DIALOG *d, int c));
XV_FUNC(DIALOG, *get_the_dialog, (void));
XV_FUNC(int, count_keyboard_short_cut, (void));
XV_FUNC(int, add_keyboard_short_cut_verbose, (int key, int d1, int d2, int (*menu_proc)(void)));
# endif  /* _GUI_DEF_H_ */









