
#ifndef _MATRICE_H_
#define _MATRICE_H_


#define ERR_NEGATIVE_SQRT 	3000
#define NULL_DENOMINATOR	3001

XV_FUNC(int, 	sym_matrix_to_triang, (int n, double *m, double *l));
XV_FUNC(int, 	sym_matrix_solve, (int n, double *l, double *y, double *x));
XV_FUNC(int, 	sym_matrix_improv, (int n, double *l, double *y, double *x));
XV_FUNC(int, 	fit_ds_to_parabola, (d_s *ds));
XV_FUNC(int,    fit_all_ds_to_xn_polynome, (d_s *ds, int n, double **a));
XV_FUNC(int, 	fit_ds_to_xn_polynome, (d_s *ds, int n, float x_lo, float x_hi,
				float y_lo, float y_hi, double **a));
				XV_FUNC(int, 	fit_ds_to_xn_polynome_er, (d_s *ds, int n, float x_lo, float x_hi,
								float y_lo, float y_hi, double **a));
XV_FUNC(int,	fit_poly_local_op, (O_p *op, int n_data_set, float wfit, int n,
				float *zmin));
XV_FUNC(float, 	error_in_fit_ds_to_xn_polynome, (d_s *ds, int n, float x_lo,
				float x_hi, float y_lo, float y_hi, double *a));
XV_FUNC(double, chi2_in_fit_ds_to_xn_polynome,(d_s *ds, int n, float x_lo, float x_hi, float y_lo, float y_hi, double *a, int *nf)
);

XV_FUNC(int,	add_plot_label_for_poly_coeff, (O_p *op, double *a, int n,
				float er));
XV_FUNC(MENU*,	matrice_plot_menu, (void));
XV_FUNC(int,	fit_poly, (void));
XV_FUNC(int,	rm_poly, (void));
XV_FUNC(int,	fit_poly_local, (void));

XV_FUNC(int, add_plot_label_for_order_1_poly, (O_p *op, double *a, float er));
XV_FUNC(int, add_plot_label_for_order_2_poly, (O_p *op, double *a, float er));
XV_FUNC(int, add_plot_label_for_order_3_poly, (O_p *op, double *a, float er));
XV_FUNC(int, add_plot_label_for_order_4_poly, (O_p *op, double *a, float er));
XV_FUNC(int, add_plot_label_for_order_5_poly, (O_p *op, double *a, float er));
# endif
