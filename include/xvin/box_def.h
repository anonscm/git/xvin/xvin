
# ifndef _BOX_DEF_H_ 
# define _BOX_DEF_H_

# include "platform.h"

# define MAX_IN 16384
# define BSIZE 128
# define AR_SIZE 384
# define FT_SIZE 256
# define SUBSCRIPT 2
# define EXPONENT 4
# define OVER 8
# define VEC 16
# define SQRT 32
# define STACK 64
# define CENTER 128
# define LEFT 256
# define TOP 512
# define BOTTOM 1024
# define SUM 2048
# define FBOX 4096
# define FRAC 32768
# define PARAGRAPH 65536
# define CENTERPARAGRAPH 0x20000
# define LEFTPARAGRAPH 0x40000
# define RIGHTPARAGRAPH 0x80000
# define TILDE 0x100000

# define NORMAL_CHAR 256
# define SPACE_CHAR 128


# define CHAR_BOX -1
# define RE_POLYGONE_BOX -2
# define POLYGONE_BOX -3
# define CLIPED_RE_POLYGONE_BOX -4
# define CLIPED_POLYGONE_BOX -5
# define FILL_RE_POLYGONE_BOX -6
# define FILL_POLYGONE_BOX -7
# define FILL_CLIPED_RE_POLYGONE_BOX -8
# define FILL_CLIPED_POLYGONE_BOX -9


# define TURN_90 -8

# define FIRST 1
# define NEXT 2

# define BX_AR	int

struct _box_font
{
	char 			*ch;
	unsigned char 	*uch;
	short int 		*new_t; 
	int w_white;
	int Max_d;
	int Max_h;
};
XV_VAR(int, ttf_enable);
# ifndef _BOX_DEF_C_
/*
extern	char 		*box_font;
extern short int 	*new_t;
*/
XV_VAR(long int, d_off);
XV_ARRAY(char, eqs_err);		/* error message */
XV_VAR(short int*, temp);
XV_VAR(struct _box_font, box_font);
# else
/*
char 		*box_font=NULL;
short int 	*new_t;
*/
long int 	d_off=0;
char 		eqs_err[128];		/* error message */
short int   *temp;
struct _box_font box_font;
# endif

struct box
{
	int 	xc;	/*position of the box relative to her parent*/
	int 	yc;
	int 	w;	/* size of the box, 1200 units correspond to*/
	int 	h;	/* 1 inch , h(eight) above base line */
	int 	d;	/* d(epth) below base line */
	short int 	pt;	/* poshort int size of char	*/
	unsigned char	slx;	/* slant angle is atan(sly/slx), allow slant and */
	unsigned char	sly;	/* italic */
	int 	color; 	/* color of the line */		
	int 	n_box;	/* if >0 number of childs, -1 indicate a char box 
					other negative value indicate a polygone*/
	int 	i_box;	/* general index */
	int 	n_char;	/* position in the font of the char desciption */
	short int 	n_hic;	/* level of hierarchy */
	short int 	id;	/* identifier flag */	
	struct box 	**child;/* pointer to the array of child */
	struct box 	*parent;/* pointer to its parent */
	int		char_val; /* the ascii code of the char when n_box = CHAR_BOX*/
};

XV_FUNC(int, init_font, (void));
XV_FUNC(struct box*, mknewbox, (struct box *ba));
XV_FUNC(struct box*, mkpolybox, (struct box *b0, int nx));
XV_FUNC(int, init_box, (struct box *b0));
XV_FUNC(int, copy_box_param, (struct box *dest, struct box *src));
XV_FUNC(struct box*, duplicate_box, (struct box *dest, struct box *src));
XV_FUNC(int, free_box, (struct box *b0));
XV_FUNC(struct box*, attach_child, (struct box *parent, struct box *child));
XV_FUNC(struct box*, locate_box, (struct box *b0, int id, int *x0, int *x1, int *y0, int *y1, int mode));
XV_FUNC(struct box*, locate_box_90, (struct box *b0, int id, int *x0, int *x1, int *y0, int *y1, int mode));
XV_FUNC(int, where_is_box, (struct box *b0, int id, int *x0, int *x1, int *y0, int *y1));
XV_FUNC(int, where_is_box_90, (struct box *b0, int id, int *x0, int *x1, int *y0, int *y1));
XV_FUNC(short int, find_char, (char *ch));
XV_FUNC(int, rearrange_hic, (struct box *b0));
XV_FUNC(int, set_box_size, (struct box *bc, struct box *ba));
XV_FUNC(struct box*, attach_ch, (char *ch, struct box *ba));
XV_FUNC(struct box*, set_origin, (struct box *ba));
XV_FUNC(int, adjust_width, (struct box *ba));
XV_FUNC(int, pr_b, (struct box *b));
XV_FUNC(struct box*, catch_last_box, (struct box *ba));
XV_FUNC(int, wr_char, (struct box *b0));
XV_FUNC(int, wr_char_90, (struct box *b0));
XV_FUNC(int, wr_box, (struct box *b0));
XV_FUNC(int, wr_box_90, (struct box *b0));
XV_FUNC(int, string_to_box, (char *st,struct box *b0));
XV_FUNC(int, wr_polygone, (struct box *b0));
XV_FUNC(int, display_box, (struct box *bo, int screen_ratio, int color, int mode));
XV_FUNC(int, set_box_origin, (struct box *bo, int x0, int y0, int screen_ratio));
				
				
				
				
				


# endif
