

# ifndef _PLOT_MN_H_
# define _PLOT_MN_H_

# include "platform.h"

XV_VAR(BITMAP*, plt_buffer);


XV_ARRAY(pltreg*, project);
XV_VAR(int,	project_cur);
XV_VAR(int,	project_n);
XV_VAR(int,	project_m);

XV_VAR(int,	proj_mn_cur);
XV_VAR(int,	proj_mn_n);
XV_VAR(int,	proj_mn_m);

# define FULLFILE_SIZE 16384

#ifndef _P_FILE_MN_C_
XV_ARRAY(MENU, path_recently_visited);
XV_VAR(char** ,vi_path);
XV_VAR(int, n_vi);
#endif


XV_ARRAY(MENU, plot_file_menu);
XV_ARRAY(MENU, plot_file_import_menu);			// added 2004-02-09, NG
XV_ARRAY(MENU, plot_file_export_menu);			// added 2004-02-09, NG
XV_ARRAY(MENU, plot_display_menu);
XV_ARRAY(MENU, plot_project_menu);
XV_ARRAY(MENU, plot_tools_menu);
XV_ARRAY(MENU, plot_treat_menu);
XV_ARRAY(MENU, plot_edit_menu);
XV_ARRAY(MENU, plot_options_menu);
XV_ARRAY(MENU, plot_recently_loaded);

XV_FUNC(MENU*, interpol_plot_menu, (void));
XV_FUNC(MENU*, matrice_plot_menu, (void));

XV_FUNC(MENU*, basic_ds_menu, (void));
XV_FUNC(MENU*, plot_1ds_menu, (void));
XV_FUNC(MENU*, plot_2ds_menu, (void));
XV_FUNC(MENU*, plot_allds_menu, (void));
XV_FUNC(MENU*, dataset_to_values_menu, (void));
XV_FUNC(int, do_least_square_fit_on_ds, (void));	// this line is required because this function appears in plot/treatment/menu

XV_FUNC(int, do_quit, (void));
XV_FUNC(int, do_load, (void));
XV_FUNC(int, do_load_im, (void));
XV_FUNC(int, do_load_recently_loaded_plot, (void));
XV_FUNC(int, reorder_loaded_plot_file_list, (char *last, char *path));
XV_FUNC(int, reorder_visited_path_list, (char *path));
XV_FUNC(int, do_select_file, (char *fpath, int fpath_size, char *file_type_cfg, char *extensions, char *loading_description, int loading));
XV_FUNC(int, do_select_pr_area, (void));
XV_FUNC(int, find_selected_pr_area, (pltreg *pr, int x_0, int y_0, DIALOG *d));

XV_FUNC(int, add_plot_tools_menu_item,   (char* text, int (*proc)(void), struct MENU* child, int flags, void* dp));
XV_FUNC(int, add_plot_edit_menu_item,    (char* text, int (*proc)(void), struct MENU* child, int flags, void* dp));
XV_FUNC(int, add_plot_treat_menu_item,   (char* text, int (*proc)(void), struct MENU* child, int flags, void* dp));
XV_FUNC(int, add_plot_options_menu_item, (char* text, int (*proc)(void), struct MENU* child, int flags, void* dp));

XV_FUNC(int, add_item_to_menu, (MENU* mn, char* text, int (*proc)(void), struct MENU* child, int flags, void* dp));
XV_FUNC(int, remove_item_to_menu, (MENU* mn, char* text, int (*proc)(void), struct MENU* child));

XV_FUNC(int, insert_item_to_menu_at_line, (MENU* mn, int line, char* text, int (*proc)(void), struct MENU* child, int flags, void* dp));
XV_FUNC(int, interpol_main, (void));
XV_FUNC(int, matrice_main, (void));
XV_FUNC(int, init_plot_treat_menu, (void));		// implemented in p_treat_mn.c

XV_FUNC(int, save_one_plot_data_bin, (O_p *op));

XV_FUNC(O_p *, build_histo_with_exponential_convolution,(pltreg *pr, O_p *ops, d_s *dsi, float biny));
XV_FUNC(O_p *, build_histo_with_exponential_convolution_2,(pltreg *pr, O_p *ops, d_s *dsi, float biny));

XV_FUNC(O_p *, build_histo_with_exponential_convolution_norm_for_each_ds, (pltreg *pr, O_p *ops, float biny));
XV_FUNC(d_s *, compute_correlated_drift_in_Y_for_ds_with_ordered_integer_x,(O_p *opi, O_p *opd, float biny));
XV_FUNC(d_s *, average_all_ds_of_op_by_max_of_histo, (O_p *ops, float biny, int multi_peak, float thres));
XV_FUNC(d_s *, average_all_ds_of_op_by_max_of_bounded_histo, (O_p *ops, float biny, int multi_peak, float thres, float y_min, float y_max));
XV_FUNC(int, find_index_of_max_in_y,(d_s *dsi));
XV_FUNC(int, find_max_around,(float *x, int nx, int nmax, float *Max_pos, float *Max_val, float *Max_deriv));


XV_FUNC(int, get_sigma_of_derivative_of_partial_ds, (d_s *dsi, int start, int end, double uperlimit, double *sigHF));

XV_FUNC(int, find_max_around, (float *x, int nx, int nmax, float *Max_pos, float *Max_val, float *Max_deriv));
XV_FUNC (d_s, *add_on_2_ds_same_x, (const d_s *src_ds1, const d_s *src_ds2));

XV_FUNC (d_s, *add_on_2_ds_by_index, (const d_s *src_ds1, const d_s *src_ds2, bool doindex));
XV_FUNC(d_s, *multiply_on_two_ds_by_index, (const d_s *ds1, const d_s *ds2, bool doindex));

XV_FUNC (d_s, *add_on_2_ds_by_index_with_offset, (const d_s *src_ds1, const d_s *src_ds2, bool doindex, int ds2_offset));
XV_FUNC(d_s, *multiply_on_two_ds_by_index_with_offset, (const d_s *ds1, const d_s *ds2, bool doindex, int ds2_offset));

XV_FUNC(O_p, *true_y_derivative, (const O_p *src_op, int ds_idx, O_p *dest_op));

XV_FUNC(d_s*, find_max_peaks, (d_s *dsi, float mthres));
XV_FUNC(d_s, *least_square_fit_on_ds, (const d_s *src_ds, bool is_affine_fit, double *a, double *b));
XV_FUNC(d_s, *least_square_fit_on_ds_between_indexes, (const d_s *src_ds, bool is_affine_fit, int min_idx, int max_idx,
        double *out_a, double *out_b));


XV_FUNC(int, compute_least_square_fit_on_ds,(const d_s *src_ds, bool is_affine_fit, double *out_a, double *out_b));
XV_FUNC(int, compute_least_square_fit_on_ds_between_indexes, (const d_s *src_ds, bool is_affine_fit, int min_idx, int max_idx, double *out_a, double *out_b));

XV_FUNC(d_s, *true_y_derivative_one_ds, (const d_s *src_ds, float dx, float dy));

XV_FUNC(int, mean_y2_on_op,(O_p *op, int win_flag, float *meany, float *my2, float *my4, float *kxu, int *nxeff));

XV_FUNC(int, mean_y2_on_array,(float *yd, int nx, int win_flag, float *meany, float *my2, float *my4));

XV_FUNC(int, find_max_of_distribution_by_histo, (d_s *dst, O_p *ops, float biny, float *Max));


XV_FUNC(int, QuickSort_double, (float *xd, float *yd, int l, int r));
XV_FUNC(int, QuickSort, (float *a, int l, int r));
XV_FUNC(int, sort_ds_along_x, (d_s *ds));
XV_FUNC(int, sort_ds_along_y, (d_s *ds));

XV_FUNC(d_s *, build_gaussian_convolution, (d_s *dsi, float biny));
XV_FUNC(d_s *, build_histo_ds_with_gaussian_convolution, (d_s *dsi, float biny));
XV_FUNC(int, find_max_of_histo_of_ds_with_gaussian_convolution, (d_s *dsi, float biny, float *ypos, float *heigh));

XV_FUNC(int, find_y_offset_between_2_ds_by_histo, (d_s *ds1, d_s *ds2, float biny, float *dy12, float *extend));

XV_FUNC(int, find_max_in_y_in_x_range, (O_p *op, d_s *dsi, float low, float hi, float *Max_pos, float *Max_val, float *width));
XV_FUNC(O_p *, x_extend_of_pts_in_y_range_all_ds_same_op, (pltreg  *pr, O_p  *op, float y_lo, float y_hi));
XV_FUNC(O_p *, merge_all_ds_in_op, (pltreg *pr, O_p *op));
XV_FUNC(d_s *,    jumpfit_vc_2_exclude_edge,        (d_s *dsi, d_s *ds, int repeat, int max, int ex_size, float sigm, float p, double *chi2t));

XV_FUNC(int, load_cgr_multi_plots, (void));
XV_FUNC(int, do_save_all_plots_from_project, (void));
XV_FUNC(int, init_plot_treat_menu, (void));	// defined in "p_treat_mn.c"
XV_FUNC(int, init_plot_file_menu, (void));	// defined in "p_file_mn.c"
XV_FUNC(int, init_image_menu, (void));

XV_FUNC(int,find_best_a_an_b_of_exp_fit,(d_s *dsi, int verbose, int error_type, double tau,  double *a, double *b,  double *E));


XV_FUNC(float, interpolate_point_by_poly_4_in_array,(float *x, float *y, int nx, float xp));
XV_FUNC(float, interpolate_point_by_poly_3_in_array,(float *x, float *y, int nx, float xp));
XV_FUNC(float, interpolate_point_by_poly_2_in_array,(float *x, float *y, int nx, float xp));

XV_FUNC(int, find_best_a_and_b_of_2_ds, (d_s *dsi, d_s *dsm, int verbose, int error_type, double *a, double *b, double *E));
XV_FUNC(int, find_best_a_of_2_ds, (d_s *dsi, d_s *dsm, int verbose, double *a, double *E));


XV_FUNC(int,MR_cross_product,(float *v1, float *v2, float *vout));
XV_FUNC(float **,MR_smat,(float **mat1, float **mat2, int l1, int l2));
XV_FUNC(float **,MR_pmat,(float **mat1, float **mat2, int lm11, int lm12, int lm21, int lm22));
XV_FUNC(float,MR_vnorm,(float *v1, int l));
XV_FUNC(float **,MR_eye_matrix,(int l));
XV_FUNC(int,MR_skew_product,(float *v1,  float **out_mat));
XV_FUNC(float,MR_dot_product,(float *v1, float *v2, int l));
XV_FUNC(float **,compute_rotation_matrix_bt_2_vectors,(float *v1, float *v2));
XV_FUNC(int,MR_free_matrix,(float **m, int l1));

XV_FUNC(int, find_max_of_distribution_by_median, (d_s *dst, O_p *ops, float *Max));

XV_FUNC(int, find_ds_median_in_y, (d_s *dst, float *Max));
XV_FUNC(int, find_ds_median_in_x, (d_s *dst, float *Max));

PXV_FUNC(d_s*, find_locale_rotate_ds, (O_p *ops, int nds, float *anisotropy, float *angle, int win_flag));
PXV_FUNC(void, find_rotation_angle_ds,(d_s *ds1, float *angle, int win_flag));



# ifndef _PLOT_MN_C_
XV_ARRAY(MENU, plot_menu);
XV_VAR(double,last_save_values_Y);
XV_VAR(double, last_save_values_X);
XV_VAR(double,last_save_values_dY);
XV_VAR(double, last_save_values_dX);
# else
double last_save_values_Y = 0;
double last_save_values_X = 0;
double last_save_values_dY = 0;
double last_save_values_dX = 0;
# endif

# endif
