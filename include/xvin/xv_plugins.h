#ifndef _XV_PLUGIN_H_
#define _XV_PLUGIN_H_

#ifdef XV_UNIX
#include <dlfcn.h>
#define PLUGIN_EXT "so"
#endif

#ifdef XV_MAC
#include <dlfcn.h>
#define PLUGIN_EXT "dylib"
#endif

#ifdef XV_WIN32
#include <allegro.h>
#include "winalleg.h"
#define PLUGIN_EXT "dll"
#endif

#include "platform.h"


// taken from util.h : 
typedef struct _plugins_info
{
  char *name;
  void* hinstLib;
} plugins_info;


// taken from p_file_mn.c and xv_main.c : 
typedef int (*PL_MAIN)(int, char**); 

#ifdef _XV_PLUGIN_C_
// local functions, used only in xv_plugins.c : 
int	record_plug_ins(char *name, void *hist);
int     grep_plug_ins(char *name);
int	remove_plug_ins_record(int n);
#endif

// function called by xv_main to load a plugin:
XV_FUNC(int, autoload_plugins,(void));
XV_FUNC(int,	load_plugin,	(int argcp, char *argvp[]));
XV_FUNC(int, list_plugin, (void));

// functions called from menu, in p_file_mn.c AND i_file_mn.c :
XV_FUNC(int,	run_module, 	(void));
XV_FUNC(int,	remove_module,	(void));

XV_FUNC(void*, grep_plug_ins_hinstLib, (char *name));
#endif

