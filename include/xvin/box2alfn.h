# ifndef _BOX2ALFN_H_ 
# define _BOX2ALFN_H_ 

# include "box_def.h"
# include "dev_def.h"
# include "xvinerr.h"
# include <allegro.h>
# include <alfont.h>

# define TTFFONT "c:\\windows\\fonts\\times.ttf"
# define TTFFONT1 "c:\\winnt\\fonts\\arial.ttf"


# ifndef _BOX2ALFN_C_
XV_VAR(int,	ttf_enable);
XV_VAR(int, ttf_init_statetate);
XV_VAR(ALFONT_FONT*, user_font);
XV_VAR(char*, debug_ttf);
# endif


XV_FUNC(int, ttf_init, (void));
XV_FUNC(int, draw_ttf_char, (struct box *b0));
XV_FUNC(int, draw_ttf_char_90, (struct box *b0));
XV_FUNC(int, give_ttf_char_width, (struct box *b0));
XV_FUNC(int, give_ttf_char_space, (struct box *b0));
XV_FUNC(int, ps_draw_char, (struct box *b0));
XV_FUNC(int, ps_draw_char_90, (struct box *b0));
# endif
