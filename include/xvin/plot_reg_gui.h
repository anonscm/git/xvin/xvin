#ifndef _PLOT_REG_GUI_H_
#define _PLOT_REG_GUI_H_

# include "platform.h"
# include "plot_reg.h"
# include "plot2box.h"

#ifndef _PLOT_REG_GUI_C_
XV_ARRAY(MENU, plot_axis_menu);
XV_ARRAY(MENU, line_style_menu);
XV_ARRAY(MENU, line_color_menu);
XV_ARRAY(MENU, point_style_menu);
XV_ARRAY(MENU, ds_menu);
XV_ARRAY(MENU, op_menu);
XV_ARRAY(MENU, y_axis_label_menu);
XV_ARRAY(MENU, x_axis_label_menu);
XV_VAR(int, line_style_selected);
XV_VAR(d_s*, ds_copied);
XV_VAR(O_p*, opds_copied);
XV_VAR(O_p*, op_copied);
XV_VAR(p_l, p_l_copied);
XV_VAR(d_s*, ds_grabbing);
XV_VAR(O_p*, op_grabbing);
XV_VAR(int, last_data_mouse_x);
XV_VAR(int, last_data_mouse_y);
#endif

# define B_CENTER 0
# define B_RIGHT	1
# define B_LEFT	2


XV_FUNC(int, do_it_one_plot_op_pr, (O_p *o_p, pltreg *pr));
XV_FUNC(int, do_linlin, (void));
XV_FUNC(int, do_loglog, (void));
XV_FUNC(int, do_linlog, (void));
XV_FUNC(int, do_loglin, (void));
XV_FUNC(int, set_y_limit, (void));
XV_FUNC(int, set_x_limit, (void));
XV_FUNC(int, auto_x_limit, (void));
XV_FUNC(int, auto_y_limit, (void));
XV_FUNC(int, auto_x_y_limit,(void));
XV_FUNC(int, plot_size, (int mn_index, float size));
XV_FUNC(int, set_plot_height, (void));
XV_FUNC(int, set_plot_width, (void));

XV_FUNC(int, do_plot_axis_menu, (int item));

XV_FUNC(int, def_line_style_plain, (void));
XV_FUNC(int, def_line_style_dash, (void));
XV_FUNC(int, def_line_style_dot, (void));

XV_FUNC(int, def_line_color, (void));
XV_FUNC(int, ds_menu_kill_ds, (void));
XV_FUNC(int, ds_show, (void));

XV_FUNC(int, do_remove_nearest_ds, (bool confirm_rm));

XV_FUNC(int, do_ds_menu, (int item));
XV_FUNC(int, op_menu_kill_op_ask, (int));
XV_FUNC(int, op_menu_kill_op, (void));
XV_FUNC(int, op_menu_kill_op_to_right_ask, (int doask));
XV_FUNC(int, op_menu_kill_op_to_right, (void));
XV_FUNC(int, all_plt, (void));

XV_FUNC(int, do_op_menu, (int item));

XV_FUNC(int, do_y_axis_label_menu, (int item));
XV_FUNC(int, build_and_display_y_axis_menu, (pltreg *pr, O_p *op));
XV_FUNC(int, build_and_display_y_axis_prime_menu, (pltreg *pr, O_p *op));

XV_FUNC(int, do_x_axis_label_menu, (int item));
XV_FUNC(int, build_and_display_x_axis_menu, (pltreg *pr, O_p *op));
XV_FUNC(int, build_and_display_x_axis_prime_menu, (pltreg *pr, O_p *op));
XV_FUNC(int, switch_to_memory_buffer, (BITMAP *buf));
XV_FUNC(int, mk_ds_mark, (O_p *op, int n_ds, int x, int y));
XV_FUNC(int, write_and_blit_box, (struct box *b, int scale, BITMAP *plt_buffer, DIALOG *d, pltreg *pr));
XV_FUNC(int, blit_plot_reg, (DIALOG *d));
XV_FUNC(int, move_label, (struct box *xt, struct box *b, int sc, BITMAP *plt_buffer, DIALOG *d, pltreg *pr, int xm_s, int ym_s, int x, int y));
XV_FUNC(int, edit_xlabel, (O_p *op, int c_lab));
XV_FUNC(int, edit_ylabel, (O_p *op, int c_lab));
XV_FUNC(int, d_draw_Op_proc, (int msg, DIALOG *d, int c));
XV_FUNC(pltreg*, find_pr_in_current_dialog, (DIALOG *di));
XV_FUNC(DIALOG*, find_dialog_associated_to_pr, (pltreg *pr, DIALOG *di));
XV_FUNC(int, do_one_plot, (pltreg *pr));
XV_FUNC(int, do_it_one_plot, (pltreg *pr));

XV_FUNC(int, draw_horz_marker, (BITMAP *plt_buffer, DIALOG *d, int pos, int mode));XV_FUNC(int, draw_vert_marker, (BITMAP *plt_buffer, DIALOG *d, int pos, int mode));

XV_FUNC(int, do_it_one_plot_op_pr, (O_p *o_p, pltreg *pr));
XV_FUNC(int, find_box_scale, (struct box *bo, dybox *db));
XV_FUNC(int, prepare_all_plots, (pltreg *pr, int n_col));
XV_FUNC(int, plots_in_one_column, (struct box *bo));
XV_FUNC(int, plots_in_two_columns, (struct box *bo));
XV_FUNC(int, plots_in_three_columns, (struct box *bo));
XV_FUNC(int, draw_bubble, (BITMAP* b, int mode, int x, int y, char *format, ...) __attribute__((format(printf, 5, 6))));

XV_FUNC(int, refresh_plot, (pltreg *pr, int n_plot));
XV_FUNC(DIALOG *, attach_new_plot_region_to_dialog, (int key, int flags, int d1, int d2));
XV_FUNC(int, re_attach_plot_region_to_dialog, (DIALOG *di, pltreg *pr));
XV_FUNC(int, copy_ds_from_one_op_to_anotherone, (d_s *dsc, O_p *src, O_p *dst));
XV_FUNCPTR(int, prev_general_pr_action, (pltreg *pr, int x0, int y0, DIALOG *d));
XV_FUNCPTR(int, general_pr_action, (pltreg *pr, int x0, int y0, DIALOG *d));			
XV_FUNCPTR(int, vert_marker_plt_action, (pltreg *pr, O_p *op, float y, DIALOG *d));
XV_FUNCPTR(int, horz_marker_plt_action, (pltreg *pr, O_p *op, float x, DIALOG *d));
XV_FUNCPTR(int, vert_marker_plt_drag_action, (pltreg *pr, O_p *op, float y, DIALOG *d));
XV_FUNCPTR(int, horz_marker_plt_drag_action, (pltreg *pr, O_p *op, float x, DIALOG *d));
	
XV_FUNCPTR(int, plot_wheel_action, (pltreg *pr, O_p *op, DIALOG *d, int ticks));
XV_FUNCPTR(int, plot_idle_action, (pltreg *pr, O_p *op, DIALOG *d));


# endif





























