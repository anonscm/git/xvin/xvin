# ifndef _OP2BOX_H_
# define _OP2BOX_H_

# include "plot_opt.h"
# include "dev_def.h"
# include "box_def.h"
# include "color.h"
# include "plot_op.h"
# include "plot2box.h"


XV_VAR(int, PlotAxesColor);

# ifndef _OP2BOX_C_
XV_VAR(struct plot_label, selected_point);
XV_VAR(int, selected_point_enable);
# endif


XV_FUNC(int, draw_pr_box, (struct box *b, int dx, int dy, int scale, int color));
XV_FUNC(int, one_plot_to_box, (O_p *o_p, struct box *b));
XV_FUNC(int, do_it_one_plot_op, (O_p *o_p));
XV_FUNC(int, find_x_limits, (O_p *op));
XV_FUNC(int, find_y_limits, (O_p *op));


# endif

