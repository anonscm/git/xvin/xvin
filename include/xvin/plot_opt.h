# define GCC_COMPIL

# define DEFAULT	0000000
# define NOCLEAR	0000001
# define NOAXES		0000002
# define DOT		0000010
# define DASH		0000020
# define BIGCHAR	0000100
# define SMALLCHAR	0000200
# define AXES_PRIME	0000400
# define TRIM		0001000
# define XLOG		0002000
# define YLOG		0004000
# define CROSS		0020000

# define GRID		TRIM + NOAXES + AXES_PRIME


/* for iopt2 */

# define X_NUM		0000001
# define Y_NUM		0000002
# define X_LIM		0000004			/* axis limit is not imposed */
# define Y_LIM		0000010




# define TEK 0 
/* # define PLOTTER 1 */
/*# define ATTPC 2 */
//# define PS 3
# define PLOT 4
#include "xvin/platform.h"
XV_FUNC(int, ps_start_file,(char *filename));
XV_FUNC(int, ps_close, (void));
