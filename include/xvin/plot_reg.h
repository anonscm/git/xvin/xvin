/*	This series of functions handles the one plot structure
 *
 */


#ifndef _PLOT_REG_H_
#define _PLOT_REG_H_

# include "platform.h"
# include "box_def.h"
# include "gui_def.h"
# include "color.h"
# include "plot_op.h"
# include "plot_ds.h"

# define MAX_ONE_PLOT 4
# define MAX_ACTIVE_DB 		32

#ifndef _PLOT_REG_C_
XV_VAR(int, PlotBackgroundColor);
XV_VAR(int, PlotAxesColor);
# else
int PlotBackgroundColor = 0;
int PlotAxesColor = 16;
# endif


typedef struct plot_region			/* view of a graph */
{
	dybox 		db;			/* boundary of it */
	rec 		def;			/* def of position */	
	struct box 	*stack;			/* box translation of plot */
	struct box 	*plt;			/* box of plot */
	int 		x_off, y_off;		/* its bottom left corner */
	int 		s_nx, s_ny;		/* the plot size on screen */
	int 		screen_scale, max_scale, min_scale, auto_scale;	
		/* ratio from box to screen */
	float 		mag_step;		/* magnification fac.*/
	int 		x0, y0;			/* x & y offset to box */
	dybox 		*l_db;			/* list of active box */
	int 		n_db, m_db;		/* number, alloced */
	O_p 		*one_p;			/* active = o_p[cur_op] */
	O_p 		**o_p;			/* the series of plot */
	int 		n_op, m_op;		/* the number of plot */
	int 		cur_op;			/* the current one */
	struct box	all;			/* the combination of plot */
	int			display_mode;	/* normal or all plots */
	int			*c_op;			/* an array for plot ordering */		
	char 		*filename;		/* the name of the project file */	
	char 		*path;			/* the name of the project directory */		
	int			mark_v,mark_h;	/* the positions of markers */
	struct hook use;		        /* allow to attach more data */	
        int             stack_in_use;           /* to prevent pb in multi thread */
        int             blitting_stack;         /* to prevent pb in multi thread */

/*	struct hook dlg;	*/		/* use to attach a Dialog */		
        int (*plot_end_action)(struct plot_region *pr, DIALOG *d);        
} pltreg;

# define 	IS_ONE_PLOT		1024
# define	ALL_PLOTS	1025

# define	MAX_SCALE	8192
# define	MIN_SCALE	4096
# define	DEC_SCALE	2048
# define	INC_SCALE	1024
# define	AUTO_SCALE	512

/* public functions */
XV_FUNC(pltreg*, build_plot_region, (int x0, int y0, int w, int h));
XV_FUNC(O_p*, create_and_attach_one_plot, (pltreg *p,int x,int y,int type));
XV_FUNC(O_p*, create_and_attach_one_empty_plot, (pltreg *pr, int insert_here));
XV_FUNC(int, add_data, (pltreg *pr, int type, void *stuff));
XV_FUNC(int, add_data_to_pltreg, (pltreg *pr, int type,void *stuff));
/**
 * @brief  remove some data (currently only Op) from a plot region
 *
 * @param pltreg 
 * @param type currently only IS_ONE_PLOT
 * @param stuff pointer to the data to remove
 */
XV_FUNC(int, remove_data, (pltreg *pr, int type, void *stuff));

# define 	remove_data_from_pltreg remove_data
/*XV_FUNC(int, remove_data_from_pltreg, (pltreg *pr, int type, void *stuff));*/
XV_FUNC(int, free_plot_region, (pltreg* pr));
XV_FUNC(int, do_one_plot, (pltreg *pr));
XV_FUNC(int, do_it_one_plot, (pltreg *pr));


XV_FUNC(int, switch_plot, (pltreg *pr, int n_plot));
XV_FUNC(int, get_op_number_in_pr, (pltreg *pr, O_p *op));
XV_FUNC(int, display_plot_region, (pltreg *pr));
XV_FUNC(int, erase_box, (void));
XV_FUNC(int, draw_box, (int dx, int dy, int scale, int color));
XV_FUNC(int, spit_box_to_ps, (pltreg *pr, char *file));
XV_FUNC(int, refresh_plot, (pltreg *pr, int n_plot));

XV_FUNC(int, update_just_plot, (pltreg *pr));
XV_FUNC(int, update_plot_db, (pltreg *pr));
XV_FUNC(int, run_pr_db, (char ch));
XV_FUNC(dybox*, next_pr_db, (pltreg *pr));

/* pltreg 			*find_plot_reg(acreg *ac); */
XV_FUNC(d_s*, find_cur_data_set, (pltreg *plt));

XV_FUNC(int, shift_horz, (char ch));
XV_FUNC(int, shift_vert, (char ch));
XV_FUNC(int, drag_label, (char ch));
XV_FUNC(int, change_label, (char ch));
XV_FUNC(int, show_peak, (char ch));


XV_FUNC(int, x_pltdata_2_pr, (pltreg *pr, float x));
XV_FUNC(int, y_pltdata_2_pr, (pltreg *pr, float y));
XV_FUNC(float, x_pr_2_pltdata, (pltreg *pr, int xvp));
XV_FUNC(float, y_pr_2_pltdata, (pltreg *pr, int yvp));
XV_FUNC(float, x_pr_2_pltdata_raw, (pltreg *pr, int xvp));
XV_FUNC(float, y_pr_2_pltdata_raw, (pltreg *pr, int yvp));

XV_FUNC(dybox*, make_box_active, (pltreg *pr, int type, int (*action)(char), int mode));


/* local functions */
XV_FUNC(int, find_box_scale, (struct box *bo, dybox *db));

XV_FUNC(float, interp, (float p,float fp,float f0,float f1,float f2));
XV_FUNC(int, set_plt_x_unit_set, (pltreg *pr, int n));
XV_FUNC(int, set_plt_y_unit_set, (pltreg *pr, int n));


XV_FUNC(int, prepare_all_plots, (pltreg *pr, int n_col));
XV_FUNC(int, plots_in_three_columns, (struct box *bo));
XV_FUNC(int, plots_in_two_columns, (struct box *bo));
XV_FUNC(int, plots_in_one_column, (struct box *bo));

XV_FUNC(int, fft_fil_inr_outr, (float *x, int nx, int w_flag, int f, int w));
XV_FUNC(int, fft_fil_inr_outc, (float *x, int nx, int w_flag, int f, int w));
XV_FUNC(int, fft_fil_inc_outc, (float *x, int nx, int w_flag, int f, int w));
//XV_FUNC(int, least_square_fit_on_ds, (char ch));
//XV_FUNC(int, remove_plot,(char ch));
XV_FUNC(int, load_plt_file_in_pltreg, (pltreg *pr, char *file, char *path));
XV_FUNC(pltreg*, find_pr_in_current_dialog, (DIALOG *di));
XV_FUNC(int, set_plt_x_unit_set, (pltreg *pr, int n));
XV_FUNC(int, set_plt_y_unit_set, (pltreg *pr, int n));

XV_FUNC(d_s*, find_source_specific_ds_in_pr, (pltreg *pr, char *source));

XV_FUNC(int, find_op_nb_of_source_specific_ds_in_pr, (pltreg *pr, char *source));
XV_FUNC(int, find_op_nb_of_treatement_specific_ds_in_pr, (pltreg *pr, char *treatement));

XV_FUNC(pltreg*, load_cgr_file_in_pltreg, (pltreg *pr, char *file_path, char *file));
XV_FUNC(pltreg*, load_cgrfile_in_pltreg, (pltreg *pr, char *fullfile));
XV_FUNC(pltreg, *create_pltreg_with_op, (O_p **op, int nx, int ny, int data_type));
XV_FUNC(pltreg, *create_hidden_pltreg_with_op, (O_p **op, int nx, int ny, int data_type, char *name));


# endif  /* _PLOT_REG_H_ */
