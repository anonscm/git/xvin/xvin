/************************************************************************/
/* fft_filter_lib.h							*/
/* filtering llibrary for use with fftw					*/
/*	DESCRIPTION	Collection of functions performing : 		*
 *				lowpass, 	smooth, 		*
 *				highpass,	normal,			*
 *				bandpass,	brickwall,		*
 *									*
 * cutoff, center, width characterize the filter shape, their units 	*
 * are in FFT mode number (the same as in the spectrum plot).		*
 *                                                                      *
 * NOTE: all the functions here are FFTW3 independent and can be used   *
 * with any fft routine...                                              *
 * functions that are specific to FFTW3 are in another library named    *
 * "fftw3_lib.c"                                                        *
 * 									*/
/************************************************************************/

#include <fftw3.h>

/************************************************************************/
/* values for the fft_filter_mode integer : 				*/
/************************************************************************/
#define REAL_INPUT	1	/* new ! 		*/
#define COMPLEX_INPUT	2	/* new ! 		*/
#define SMOOTH_F 	4	/* new ! 		*/
#define NORMAL_F 	8	/* new ! 		*/
#define BRICKW_F 	16	/* new ! 		*/
#define BUTTERWORTH_F 	8192	/* new ! 		*/
#define LOW_PASS 	32	/* idem as in xvin.h 	*/
#define BAND_PASS 	64	/* idem as in xvin.h 	*/
#define HIGH_PASS 	128	/* idem as in xvin.h 	*/
#define REJECTOR 	256	/* new ! 		*/
#define REAL_OUTPUT	512	/* */
#define COMPLEX_OUTPUT	1024	/* */
#define SYM     	2048 	/* idem as in xvinimmn 	*/
#define DI_SYM		4096 	/* idem as in xvinimmn 	*/

#define FILTER_N0	4096	/* default nber of points in the filter for first initialisation */


/************************************************************************/
typedef struct fft_filter
/* this structure contains the complete information on a filter *********/
{
	int mode;	/*kind of filter */
	int n;		/*size of the filter array in use*/
	int hp;		/*high-pass freq.*/
	int wh;		/*high-pass width*/
	int lp;		/*low-pass freq.*/
	int wl;		/*low-pass width*/
	float *f;  	/*pointer to the array of filter */
} fftf;



/************************************************************************/
/* variables :								*/
/************************************************************************/
extern struct fft_filter *current_filter;
	// contains the filter used the last time (e.g., to plot it)
	// it is defined for good in fft_filer_lib.c


/************************************************************************/
/* functions :								*/
/************************************************************************/

// following function initialize a filter:
fftf*	init_filter(int npts);
void 	free_filter(fftf *filter);

// following functions returns a window (Hamming type, with sines):
double Hanning			(int npts, int i);
double Hamming			(int npts, int i, float smp);

// following functions apply a window on a 1d signal to make it periodic:
int 	fft_window_real     	(int npts, double *x,	float smp);
int 	fft_unwindow_real   	(int npts, double *x,	float smp);
int 	fft_window_complex  	(int npts, fftw_complex *x, float smp);
int 	fft_unwindow_complex	(int npts, fftw_complex *x, float smp);

// following functions apply a window on a 2d image, to make it periodic:
int	fft_window_complex_2d_x  (int nx, int ny, fftw_complex *x, float smp);
int	fft_window_complex_2d_y  (int nx, int ny, fftw_complex *x, float smp);
int	fft_unwindow_complex_2d_x(int nx, int ny, fftw_complex *x, float smp);
int	fft_unwindow_complex_2d_y(int nx, int ny, fftw_complex *x, float smp);

// following functions apply a filter on a Fourier transform:
int	apply_filter_real   	(struct fft_filter *fil, double       *x, int n_2);
int	apply_filter_complex	(struct fft_filter *fil, fftw_complex *x, int n_2);

// following function create a plot to "show" the current filter
int 	plot_filter_in_pltreg	(struct fft_filter *filter);

// following function makes a filter symetric
// (because a filter is not symetric if created by the following next 4 functions):
int	symetrize_filter	(struct fft_filter *filter, int npts, int symetry);

// following functions build assymetric filters:
fftf	*build_lowpass_filter   (int npts, int cutoff, int width);
fftf	*build_highpass_filter  (int npts, int cutoff, int width);
fftf	*build_bandpass_filter  (int npts, int cutoffl, int widthl, int cutoffh, int widthh);
fftf	*build_rejector_filter  (int npts, int cutoffl, int widthl, int cutoffh, int widthh);

// following functions build assymetric Butterworth filters:
fftf	*build_lowpass_butter_filter   (int npts, int cutoff, int order);
fftf	*build_highpass_butter_filter  (int npts, int cutoff, int order);
fftf	*build_bandpass_butter_filter  (int npts, int cutoffl, int orderl, int cutoffh, int orderh);
fftf	*build_rejector_butter_filter  (int npts, int cutoffl, int orderl, int cutoffh, int orderh);


// following functions is the one to invoke; it has a user interface to set the parameters:
fftf	*construct_filter  	(int npts, int mode, float dx);

// following functions convert frequencies into modes (integers) and the ooposite:
int 	freq_to_mode		(float f, float f_acq, int N);
float 	mode_to_freq_to_mode	(int k,   float f_acq, int N);
