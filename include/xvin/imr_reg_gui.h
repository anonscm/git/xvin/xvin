#ifndef _IM_REG_GUI_H_
#define _IM_REG_GUI_H_

# include "platform.h"
# include "unitset.h"
# include "xvinerr.h"
# include "util.h"
# include "box_def.h"
# include "plot_ds.h"
# include "plot_op.h"
# include "plot_opt.h"
# include "gui_def.h"
# include "plot2box.h"
# include "im_oi.h"
# include "im_reg.h"



# define 	Y_MARKER	4546
# define 	X_MARKER	4547
# define 	P_LINE		16
# define 	P_COL		17
# define 	P_TILTED	18
# define	SYM			2048
# define	DI_SYM		4096

#ifndef _IM_REG_GUI_C_
XV_ARRAY(MENU, imop_menu);
XV_ARRAY(MENU, oi_menu);
XV_ARRAY(MENU, im_y_axis_label_menu);
XV_ARRAY(MENU, im_x_axis_label_menu);
XV_VAR(O_i*, oi_copied);
XV_VAR(int, smooth_interpol);
XV_VAR(int, z_x_avg_profile_size);
XV_VAR(int, z_y_avg_profile_size);

#endif

XV_FUNC(imreg*, find_imr_in_current_dialog, (DIALOG *di));
XV_FUNC(DIALOG*, find_dialog_associated_to_imr, (imreg *imr, DIALOG *di));
XV_FUNC(int, find_imr_index_in_current_dialog, (DIALOG *di));

XV_FUNC(int, im_menu_kill_op, (void));
XV_FUNC(int, imop_show, (void));

XV_FUNC(int, do_imop_menu, (int item));
XV_FUNC(int, oi_menu_kill_im, (void));

XV_FUNC(int, do_oi_menu, (int item));

XV_FUNC(int, do_im_y_axis_label_menu, (int item));
XV_FUNC(int, build_and_display_im_y_axis_menu, (imreg *imr, O_i *oi));

XV_FUNC(int, do_im_x_axis_label_menu, (int item));
XV_FUNC(int, build_and_display_im_x_axis_menu, (imreg *imr, O_i *oi));
XV_FUNC(int, edit_im_xlabel, (O_i *oi, int c_lab));
XV_FUNC(int, edit_im_ylabel, (O_i *oi, int c_lab));
XV_FUNC(int, move_im_label, (struct box *xt, struct box *b, int sc, BITMAP *plt_buffer, DIALOG *d, imreg *imr, int xm_s, int ym_s, int x, int y));
XV_FUNC(int, d_draw_Im_proc, (int msg, DIALOG *d, int c));

XV_FUNC(O_i*, convert_bitmap_to_oi, (BITMAP *bmp, O_i *dest, int x0, int y0, int x1, int y1));
XV_FUNC(int, write_and_blit_im_box, (BITMAP *plt_buffer, DIALOG *d, imreg *imr));
XV_FUNC(int, adj_image_pos, (imreg *imr, DIALOG *d));
XV_FUNC(int, display_image_stuff_16M, (imreg *imr,  DIALOG *d));
XV_FUNC(DIALOG *, attach_new_image_region_to_dialog, (int key, int flags, int d1, int d2));
XV_FUNC(int, re_attach_image_region_to_dialog, (DIALOG *di, imreg *imr));
XV_FUNCPTR(int, image_special_display, (imreg *imr));
XV_FUNCPTR(int, vert_marker_action, (O_i *oi, int n_line, DIALOG *d));
XV_FUNCPTR(int, horz_marker_action, (O_i *oi, int n_row, DIALOG *d));
XV_FUNCPTR(int, vert_marker_drag_action, (O_i *oi, int n_line, DIALOG *d));
XV_FUNCPTR(int, horz_marker_drag_action, (O_i *oi, int n_row, DIALOG *d));

XV_FUNCPTR(int, prev_general_im_action, (imreg *imr, int x_0, int y_0, int mode));
XV_FUNCPTR(int, general_im_action, (imreg *imr, int x0, int y0, int mode));

XV_FUNC(int, do_line_profile, (O_i *oi, int n_line, DIALOG *d));
XV_FUNC(int, do_row_profile, (O_i *oi, int n_row, DIALOG *d));
XV_FUNC(int, check_profile, (MENU* mn));
XV_FUNC(int, set_profile, (void));
XV_FUNC(int, draw_im_screen_label, (imreg *imr, O_i *oi, int cur_f));
XV_FUNC(int, refresh_image, (imreg *imr, int n_im));

XV_FUNC(int, restore_image_parameter, (imreg *imr,  DIALOG *d));
#endif 
