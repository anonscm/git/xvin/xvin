/********************************************************************************/
/*	fftw3_lib.h							*/
/********************************************************************************/
/*	DESCRIPTION	Collection of functions performing : 
 *			- PSD computation
 *                      - filtering
 *                      - Hilbert transform
 *										*
 *	using the fftw3 library
 *                                                                              *
 *      these functions are not accessible from the XVin GUI, but are intended      
 *      to be used by other functions or plugins. they provide a higher level 
 *      interface to the FFTW3 library.
 *
 *      front-end functions (accessible from the GUI) are in the source file
 *      "menus/plot/treat/p_treat_fftw.c"
 */
/********************************************************************************/
/* Vincent Croquette, Nicolas Garnier	nicolas.garnier@ens-lyon.fr		*/
/********************************************************************************/

// definitions for computing the psd of a dataset:
#define PSD_ENERGY	0x0200 	/* the psd is given in energy (squared amplitude), like a spectrum */
#define PSD_AMP		0x0400 	/* the psd is given in amplitude (square root of a spectrum)	   */

#include <fftw3.h> 


/************************************************************************/
/* functions :								*/
/************************************************************************/

int compute_psd_of_float_data(float *y, float *psd_f, float *freq, float f_acq, int nx, int n_fft, int n_overlap, int bool_hanning);
					// used by 'do_psd' and any function that needs it (contains just math)
int compute_cross_psd_of_float_data(float *x, float *y, float *psd_f_real, float *psd_f_imag, float *freq, float f_acq, int nx, int n_fft, int n_overlap, int bool_hanning);
					// used by any function that needs it (contains just math)

float *Fourier_filter_float       (float *y, int nx, float f_acq, float fl, float dfl, float fh, float dfh, int bool_Hanning);
				// depending on the values of fl and fh, a lowpass, or highpass or bandpass filter is applied to *y
 				// negative or invalid fh implies a LP filter
 				// negative fl implies a HP filter
fftw_complex *Hilbert_transform   (float *y, int nx, float f_acq, float fl, float dfl, float fh, float dfh, int bool_Hanning);

