# ifndef _DEV_PCX_H_
# define _DEV_PCX_H_

# include "xvin/platform.h"
//#pragma once
# include "allegro.h"
# ifdef XV_WIN32
# include "winalleg.h"
# endif


XV_FUNC(int, pc_move, (float ix, float iy));
XV_FUNC(int, pc_rmove, (float ix, float iy));
XV_FUNC(int, pc_color, (int cl, int mode));
XV_FUNC(int, pc_line_size, (float size));
XV_FUNC(int, pc_mode, (int arg));
XV_FUNC(int, pc_dot, (float ix, float iy));
XV_FUNC(int, pc_line, (float x2, float y2));
XV_FUNC(int, pc_rline, (float x2, float y2));
XV_FUNC(int, pc_size, (int arg));
XV_FUNC(int, pc_clip, (float x0, float y0, float x1, float y1));
XV_FUNC(int, pc_clip_reset, ());
XV_FUNC(int, pc_init, (void));
XV_FUNC(int, pc_init1, (void));
XV_FUNC(int, pc_catch1, (float x, float y));
XV_FUNC(int, pc_catch2, (float x, float y));
XV_FUNC(int, demgr, (int color_depth, int gfx_mode, int w, int h));
XV_FUNC(int, gris, (int zmin, int zmax));

# ifdef _DEV_PCX_C_
BITMAP *dev_bitmap = NULL;
int y_offset;
#else
XV_VAR(BITMAP*, dev_bitmap);
XV_VAR(int, y_offset);
#endif


# endif
