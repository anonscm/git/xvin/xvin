# ifndef _DEV_DEF_H_
# define _DEV_DEF_H_

/*
	Defines the graphics drivers functions needed to display
	a box structure. The first driver is the display screen
	the other correspond to other format PS, PDF, SVG...
*/

# include "platform.h"
# include "box_def.h"

# define GRAPHICS 3
# define ALPHA 4
# define CLEAR 5
# define DONE 6
# define SMALL 7
# define BIG 8
# define ERASE 9
# define GRAPH 1
# define LINE 2

# ifdef _BOX2DEV_C_ 
int default_line_size = 7;
# else
XV_VAR(int, default_line_size);
# endif
struct device
{
	int (*move)(float,float);
	int (*rmove)(float,float);	
	int (*mode)(int);
	int (*dot)(float,float);
	int (*line)(float,float);
	int (*rline)(float,float);	
	int (*size)(int);
	int (*line_size)(float);	
	int (*color)(int, int);		
	int (*clip)(float,float,float,float);			
	int (*clip_reset)(void);
        int (*draw_char)(struct box *b0);  
        int (*draw_char_90)(struct box *b0);
};
# ifndef _DEV_PCX_C_
XV_VAR(struct device, q_dev);
XV_VAR(int, pc_fen);
XV_VAR(int, dev_color);
# endif


# endif
