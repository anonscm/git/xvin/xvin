# ifndef _UTIL_H_
# define _UTIL_H_

# include "platform.h"

# include <stdarg.h>
# include <string.h>
# include <stdbool.h>
#ifndef XV_MAC
#define M_E		2.7182818284590452354
#define M_LOG2E		1.4426950408889634074
#define M_LOG10E	0.43429448190325182765
#define M_LN2		0.69314718055994530942
#define M_LN10		2.30258509299404568402
#define M_PI		3.14159265358979323846
#define M_PI_2		1.57079632679489661923
#define M_PI_4		0.78539816339744830962
#define M_1_PI		0.31830988618379067154
#define M_2_PI		0.63661977236758134308
#define M_2_SQRTPI	1.12837916709551257390
#define M_SQRT2		1.41421356237309504880
#define M_SQRT1_2	0.70710678118654752440
#define PI		M_PI
#define PI2		M_PI_2
#endif

#ifndef my_max
#define my_max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })
#endif

#ifndef my_min
#define my_min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })
#endif

# define	DIR_LEN		256




# define 	NOT_TO_DISPLAY	0x8000	/* prevent to display an image */

struct hook
{
	int to;			/* identifier */
	void *stuff;		/* the object */
	int sub_type;
};

#ifdef XV_MAC
#define CLOCK_MONOTONIC 0
struct timespec;
int clock_gettime(int, struct timespec*);
#endif



/* this goes in whatever header defines my_strdup */
XV_FUNC(char, *my_strdup,(const char *s));
XV_FUNC(char, *my_strupr, (char *s));

#ifndef strdup
#define strdup(x) my_strdup(x)
#endif

#ifndef strupr
#define strupr(x) my_strupr(x)
#endif

# define Mystrdup(src)	((src) == NULL) ? NULL : strdup((src))

XV_FUNC(char, *get_os_specific_path, (char *path));
XV_FUNC(bool, is_path_separator, (char c));
XV_FUNC(char, get_path_separator, ());
XV_FUNC(void, put_path_separator, (char * path, int n_path));
XV_FUNC(char*, Mystrdupre, (char *dest, const char *src));
XV_FUNC(int, my_set_window_title, (char *format, ...) __attribute__ ((format (printf, 1, 2))));
XV_FUNC(int, set_formated_string, (char **dest, char *format, ...) __attribute__ ((format (printf, 2, 3))));
XV_FUNC(int, vset_formated_string, (char **dest, char *format, va_list ap)__attribute__((format(printf, 2, 0))));
XV_FUNC(char*, my_sprintf, (char *dest, char *format, ...) __attribute__ ((format (printf, 2, 3))));
XV_FUNC(char*, my_strncat, (char *dest, const char *src, size_t dest_size));
XV_FUNC(char*, slash_to_backslash, (char *path));
XV_FUNC(char*, backslash_to_slash,(char *path));
XV_FUNC(char*, build_full_file_name, (char *name, int n_name, const char *path, const char *file));
XV_FUNC(char*, extract_file_path, (char *path, int n_path, const char *full_name));
XV_FUNC(char*, extract_file_name, (char *file, int n_file, const char *full_name));
XV_FUNC(char*, my_getcwd, (char *path, int size));
XV_FUNC(char*, Transfer_filename, (char *src));
XV_FUNC(int, do_you_want_to_overwrite, (const char *file, const char *path, const char *mes));
XV_FUNC(int, switch_allegro_font, (int mode));
XV_FUNC(char, *protect_string, (const char *str, int size));
XV_FUNC(bool, nearly_equal, (float a, float b, float epsilon));
XV_FUNC(int, my_cp, (const char *to, const char *from));
//XV_FUNC(bool, file_exists, (const char *filename));


/* compatibility with old menu_index using flags in allegro menu */

# define MENU_INDEX(n) ((n)<<8)
# define RETRIEVE_MENU_INDEX   (active_menu == NULL) ? 0 : ((active_menu->flags & 0x80000000) ? \
		(((active_menu->flags & 0xFFFFFF00)>>8) | 0xFF000000) \
		: ((active_menu->flags & 0xFFFFFF00)>>8))

/* my_uclock() try to reproduce the u_clock() from unix under WIN32 using
QueryPerformanceCounter(&t0); and QueryPerformanceFrequency(&freq); Windows
functions. This fuction count ticks from a fast counter ~3.5 MHz.
my_uclock() cast the 64bits in 32 after dividing the count number by 16
thus it provide a granularity of ~4 micro-second.
my_ulclock() is a 64 bits version with no division.

typical use

unsigned long t;

       t = mu_uclock();
       ...
       some function to measure execution time
       ...
       t = mu_uclock() - t;
       win_printf("execution time is %g", (double)t/MY_UCLOCKS_PER_SEC);

*/



# ifdef XV_WIN32

XV_FUNC(char*, DoFileOpen, (const char *FileTitle, const char *initialdir,  char *szFileName, int szFileNameSize, const char *ext_filters, const char *extension));

XV_FUNC(char*, DoFileSave, (const char *FileTitle, const char *initialdir, char *szFileName, int szFileNameSize, const char *ext_filters, const char *extension));

# endif /* end of the win32 case */



/* following moved from the win32 section for use with other version (linux / MacOs): */
/* NG, 2009/05/05 */
XV_FUNC(unsigned int, my_uclock, (void));
XV_FUNC(unsigned int, get_my_uclocks_per_sec, (void));
XV_FUNC(long long, my_ulclock, (void));
XV_FUNC(long long, get_my_ulclocks_per_sec, (void));

# ifndef _UTIL_C_
XV_VAR(unsigned int, my_uclocks_per_sec);
XV_VAR(long long, my_ulclocks_per_sec);
# endif

# define MY_UCLOCKS_PER_SEC (my_uclocks_per_sec) ? my_uclocks_per_sec : \
             get_my_uclocks_per_sec()

# define MY_ULCLOCKS_PER_SEC (my_ulclocks_per_sec) ? my_ulclocks_per_sec : \
             get_my_ulclocks_per_sec()


/* following moved from the win32 section for use with other version (linux / MacOs): */
/* NG, 2007/08/29 */
# ifndef _UTIL_C_
XV_VAR(char, win_title[]);
XV_VAR(int, win_title_used);
# else
char win_title[512];
int win_title_used = 0;
# endif
# endif
