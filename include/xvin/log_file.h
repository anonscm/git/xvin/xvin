#ifndef _LOG_FILE_H_
#define _LOG_FILE_H_

XV_FUNC(int, select_log_file, (void));
XV_FUNC(char*, grab_specific_log_file, (void));
XV_FUNC(char*, grab_specific_sub_log_file, (char *main_logfile));

XV_FUNC(int, dump_to_log_file_with_date_and_time, (const char *fmt, ...)  __attribute__((format(printf, 1, 2))));
XV_FUNC(int, dump_to_log_file_with_time, (const char *fmt, ...)  __attribute__((format(printf, 1, 2))));
XV_FUNC(int, dump_to_log_file_only, (const char *fmt, ...)  __attribute__((format(printf, 1, 2))));

XV_FUNC(int, dump_to_specific_log_file_with_date_and_time, (char *file,	const char *fmt, ...)  __attribute__((format(printf, 2, 3))));
XV_FUNC(int, dump_to_specific_log_file_with_time, (char *file, const char *fmt, ...)  __attribute__((format(printf, 2, 3))));
XV_FUNC(int, dump_to_specific_log_file_only, (char *file, const char *fmt, ...)  __attribute__((format(printf, 2, 3))));

# ifndef  _LOG_FILE_C_
XV_ARRAY(char, sample);
XV_ARRAY(char, author);
XV_VAR(char*, log_filename);
# endif

# endif

