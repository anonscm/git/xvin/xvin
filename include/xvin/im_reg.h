#ifndef _IM_REG_H_
#define _IM_REG_H_

# include "platform.h"
# include "unitset.h"
# include "xvinerr.h"
# include "util.h"
# include "box_def.h"
# include "plot_ds.h"
# include "plot_op.h"
# include "plot_opt.h"
# include "gui_def.h"
# include "plot2box.h"
# include "gr_file.h"
# include "im_oi.h"



typedef struct image_region			/* view of an image */
{
  dybox db;				/* boundary of it */
  rec def;				/* def of position */	
  struct box stack, stack2;		/* box translation of plot */
  struct box *plt;			/* box of plot */
  int x_off, y_off;			/* the image bot. l. corner */
  int s_nx, s_ny;				/* the image size on screen */
  int screen_scale, max_scale, min_scale;	/* ratio from box to screen */
  float mag_step;				/* magnification fac.*/
  int x0, y0;				/* x & y offset to box */
  dybox *l_db;				/* list of active box */
  int n_db, m_db;				/* number, alloced */
  O_i *one_i; 				/* the im def */
  O_i **o_i;				/* the series of image */
  int n_oi, m_oi;				/* the number of image */
  int cur_oi;				/* the current one */
  int mark_v,mark_h;	                /* the positions of markers */	
  int stack_in_use;                     /* to prevent pb in multi thread */
  int blitting_stack;                   /* to prevent pb in multi thread */
  struct hook use;		        /* allow to attach more data */	
  int (*image_got_mouse)(struct image_region *imr, int xm_s, int ym_s, int mode);        
  int (*image_lost_mouse)(struct image_region *imr, int xm_s, int ym_s, int mode);        
  int (*image_end_action)(struct image_region *imr, DIALOG *d);        
  int (*image_mouse_action)(struct image_region *imr, int x0, int y0, int mode, DIALOG *d);
} imreg;

#ifndef im_REG_C
XV_VAR(imreg*, last_selected_imr);;
# endif

XV_FUNC(imreg*, build_image_region, (int x0, int y0, int width, int height, int type));
XV_FUNC(imreg*, build_an_empty_image_region, (int x0, int y0, int width, int height));

XV_FUNC(int, free_image_region, (imreg* imr));

XV_FUNC(O_i*, create_and_attach_oi_to_imr, (imreg *imr, int nx, int ny, int type));

XV_FUNC(int, alloc_image, (imreg *imr, int nx, int ny, int type));

XV_FUNC(imreg,	*create_imreg_with_movie, (int nx, int ny, int data_type, int n_frames));

XV_FUNC(O_i*,  create_and_attach_movie_to_imr, (imreg *imr, int nx, int ny, int type, int nf));

XV_FUNC(O_i*, create_and_attach_continuous_movie_to_imr, (imreg *imr, int nx, int ny, int type, int nf));
XV_FUNC(int, select_image_of_imreg, (imreg *imr, int n));
XV_FUNC(int, select_image_of_imreg_and_display, (imreg *imr, int n));


XV_FUNC(int, add_image, (imreg *imr, int type, void *stuff));
XV_FUNC(int, add_to_image, (imreg *imr, int type, void *stuff));
XV_FUNC(int, remove_image, (imreg *imr, int type, void *stuff));
XV_FUNC(int, remove_from_image, (imreg *imr, int type, void *stuff));

# define find_im_cur_oi(imr)	(imr)->one_i 

XV_FUNC(O_p*, find_im_cur_op, (imreg *imr));

XV_FUNC(int, x_imdata_2_imr, (imreg *imr, int x));
XV_FUNC(int, y_imdata_2_imr, (imreg *imr, int y));
XV_FUNC(float, x_imr_2_imdata, (imreg *imr, int x));
XV_FUNC(float, y_imr_2_imdata, (imreg *imr, int y));

XV_FUNC(float, x_imr_2_im_user_data, (imreg *imr, int x));
XV_FUNC(float, y_imr_2_im_user_data, (imreg *imr, int y));

XV_FUNC(int, x_pltdata_2_imr, (imreg *imr, float x));
XV_FUNC(int, y_pltdata_2_imr, (imreg *imr, float y));
XV_FUNC(float, x_imr_2_pltdata, (imreg *imr, int yvp));
XV_FUNC(float, y_imr_2_pltdata, (imreg *imr, int xvp));

XV_FUNC(int, set_im_x_unit_set, (imreg *imr, int n));
XV_FUNC(int, set_im_y_unit_set, (imreg *imr, int n));
XV_FUNC(int, set_im_z_unit_set, (imreg *imr, int n));
XV_FUNC(O_i*, find_source_specific_oi_in_imr, (imreg *imr, char *source, int *n_oi));

XV_FUNC(int, do_one_image, (imreg *imr));

XV_FUNC(int, draw_box_im, (int dx, int dy, int scale, int color));
XV_FUNC(int, erase_imr_box, (imreg *imr));
XV_FUNC(int, draw_box_imr_plot, (imreg *imr, int color));

XV_FUNC(int, refresh_im_plot, (imreg *imr, int n_plot));
XV_FUNC(int, refresh_image, (imreg *imr, int n_im));

XV_FUNC(imreg*, find_imr_in_current_dialog, (DIALOG *di));

# endif 





