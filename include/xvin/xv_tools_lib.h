#pragma once


// function to convert a string into an array of integers (for imput of indexes):
XV_FUNC(int,           *str_to_index,           (char *str, int *N) );
XV_FUNC(float,         *str_to_float,           (char *str, int *N) ); // same with floats (july 2006)
int do_try_str_to_index(void);

// functions to grep a parameter value in a string
// (example : grep a numerical value in ds->treatment, for using it as a parameter) 

// functions to find points lying in a given interval, from an array
XV_FUNC(int,       grep_int_in_string,          (char *string, char *parameter) );
XV_FUNC(float,     grep_float_in_string,        (char *string, char *parameter) );
XV_FUNC(int,       *find_interval_float,        (float  *x, int n_x, float  x_min, float  x_max, int *N));
XV_FUNC(int,       *find_interval_double,       (double *x, int n_x, double x_min, double x_max, int *N));
XV_FUNC(int,       *find_and_keep_firsts_int,   (int *index, int n_in, int *n_out));
XV_FUNC(int,       *find_and_keep_lasts_int,    (int *index, int n_in, int *n_out));
XV_FUNC(int,       *find_and_keep_middles_int,  (int *index, int n_in, int *n_out));

// following function is to convert from little-endian to big-endian, and vice-versa:
XV_FUNC(void,      swap_bytes,	                (void *a, int n, int nb) );

// following function unwraps a phase, ie let it be continous and span away from [-PI; +PI[ :
XV_FUNC(int,       unwrap,	                	(float *phaxe, int nx) ); 

// following functions detects the sampling rate and the average or initial dt 
// of a time series :
XV_FUNC(float,     find_dt_average_from_time_array, (float *t, int n) );
XV_FUNC(float,     find_dt_start_from_time_array,   (float *t, int n) );
XV_FUNC(float,     find_sampling_frequency,         (float *t, int n) );
XV_FUNC(float,     find_sampling_frequency_dialog,  (float *t, int n) );
XV_FUNC(d_s *,    jumpfit_vc_2_exclude_edge,        (d_s *dsi, d_s *ds, int repeat, int max, int ex_size, float sigm, float p, double *chi2t));

XV_FUNC(float,    get_sigma_of_derivative_auto, (d_s *dsi, float fraction));
