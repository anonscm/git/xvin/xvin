# ifndef _PLOT2BOX_H_
# define _PLOT2BOX_H_

# include <stdio.h>
# include "platform.h"

# define GRAPHICS 3
# define ALPHA 4
# define CLEAR 5
# define DONE 6
# define SMALL 7
# define BIG 8
# define ERASE 9
# define GRAPH 1
# define LINE 2

# define 	QKD_F		4097
# define 	QKD_B		4098
# define 	QKD_B_PLOT	4099
# define 	QKD_B_XLABL	4100
# define 	QKD_B_YLABL	4101
# define 	QKD_B_XTK_T	4102
# define 	QKD_B_XTK_B	4103
# define 	QKD_B_YTK_L	4104
# define 	QKD_B_YTK_R	4105
# define 	QKD_B_XTITL	4106
# define 	QKD_B_YTITL	4107
# define 	QKD_B_TITL	4108
# define 	QKD_B_SYMB	4109
# define 	QKD_B_XLABEL	4110
# define 	QKD_B_YLABEL	4111
# define 	QKD_B_IM	4112
# define 	QKD_B_XTITL_PRIME	4113
# define 	QKD_B_YTITL_PRIME	4114

# ifndef _PLOT2BOX_C_
XV_VAR(int, q_logx);
XV_VAR(int, q_logy);
XV_VAR(int, trim);

XV_VAR(int, q_xsave);
XV_VAR(int, q_ysave);
XV_VAR(int, q_nxtick);
XV_VAR(int, q_nytick);

XV_VAR(char, q_symb);
XV_VAR(char, q_cross);

XV_VAR(int, q_xmark);
XV_VAR(int, q_ymark);
XV_VAR(int, q_minx);
XV_VAR(int, q_maxx);
XV_VAR(int, q_miny);
XV_VAR(int, q_maxy);
XV_VAR(float, q_scx);
XV_VAR(float, q_scy);

XV_VAR(float, q_x1);
XV_VAR(float, q_x2);
XV_VAR(float, q_y1);
XV_VAR(float, q_y2);

XV_VAR(float, q_ax);
XV_VAR(float, q_bx);
XV_VAR(float, q_ay);
XV_VAR(float, q_by);

XV_VAR(int, q_pxxmin);
XV_VAR(int, q_pxxmax);
XV_VAR(int, q_pxymin);
XV_VAR(int, q_pxymax);

XV_VAR(float, q_pxdx);
XV_VAR(int, q_chxsiz);
XV_VAR(int, q_chysiz);
XV_VAR(int, q_halfch);
XV_VAR(int, q_xshift);
XV_VAR(int, q_yshift);

XV_ARRAY(double, q_xtick);
XV_ARRAY(double, q_ytick);

XV_ARRAY(double, q_xltick);
XV_ARRAY(double, q_yltick);

XV_ARRAY(int, q_xltick_t);
XV_ARRAY(int, q_yltick_t);

XV_VAR(FILE, *qd_fp);


XV_VAR(struct box*, q_f);
XV_VAR(struct box*, q_b);
XV_VAR(struct box*, q_b_plot);
XV_VAR(struct box*, q_b_xlabl);
XV_VAR(struct box*, q_b_ylabl);
XV_VAR(struct box*, q_b_xtk_t);
XV_VAR(struct box*, q_b_xtk_b);
XV_VAR(struct box*, q_b_ytk_l);
XV_VAR(struct box*, q_b_ytk_r);
XV_VAR(struct box*, q_b_xtitl);
XV_VAR(struct box*, q_b_ytitl);
XV_VAR(struct box*, q_b_titl);
XV_VAR(struct box*, q_b_symb);



XV_VAR(char*, q_pre_xlabl);
XV_VAR(char*, q_unit_xlabl);
XV_VAR(char*, q_pre_ylabl);
XV_VAR(char*, q_unit_ylabl);

XV_VAR(int, pc_fen);

XV_VAR(int, q_pxxmin_sv);
XV_VAR(int, q_pxxmax_sv);
XV_VAR(int, q_pxymin_sv);
XV_VAR(int, q_pxymax_sv);

XV_VAR(int, qkxlabl_flag);
XV_VAR(int, qkylabl_flag);
XV_VAR(int, qkgr_color);
XV_VAR(int, qkax_color);
XV_VAR(int, qklab_color);
XV_VAR(float, tk_len);
XV_VAR(float, tk_len_x);
XV_VAR(float, tk_len_y);



XV_VAR(int, title_vspace);
XV_VAR(int, xtitle_vspace);
XV_VAR(int, ytitle_hspace);

XV_VAR(int, fine_grid);
# else
int q_logx = 0;
int q_logy = 0;

int q_nxtick, q_nytick;
int trim = 0;

char q_symb = 0;
char q_cross = 0;

int q_xmark;
int q_ymark;

int q_minx = 0;			/* device limits */
int q_maxx = 1023;
int q_miny = 0;
int q_maxy = 780;

float q_scx = 1.0;
float q_scy = 1.0;

float q_x1, q_x2, q_y1, q_y2;	/* axis values (user coordinates) */
float q_ax, q_bx, q_ay, q_by;	/* user to device coordinate transformation */
int q_pxxmin = 150;		/* default axes (device coordinates) */
int q_pxxmax = 1000;
int q_pxymin = 45;
int q_pxymax = 750;

float q_pxdx = 0.01956;		/* device scale factor */

int q_chxsiz = 14;
int q_chysiz = 28;
int q_halfch = 7;
int q_xshift = 5;
int q_yshift = 7;

double q_xtick[64], q_ytick[64];
double q_xltick[256], q_yltick[256];
int q_xltick_t[256], q_yltick_t[256];
int q_xsave, q_ysave;

FILE *qd_fp = NULL;


struct box *q_f = NULL;		/* outside frame */
struct box *q_b = NULL;
struct box *q_b_plot = NULL;
struct box *q_b_xlabl = NULL;
struct box *q_b_ylabl = NULL;
struct box *q_b_xtk_t = NULL;
struct box *q_b_xtk_b = NULL;
struct box *q_b_ytk_l = NULL;
struct box *q_b_ytk_r = NULL;
struct box *q_b_xtitl = NULL;
struct box *q_b_ytitl = NULL;
struct box *q_b_titl = NULL;
struct box *q_b_symb = NULL;

char *q_pre_xlabl = NULL;
char *q_unit_xlabl = NULL;
char *q_pre_ylabl = NULL;
char *q_unit_ylabl = NULL;

int q_pxxmin_sv = 150;
int q_pxxmax_sv = 1000;
int q_pxymin_sv = 45;
int q_pxymax_sv = 750;

int qkxlabl_flag = 1;
int qkylabl_flag = 1;
int qkgr_color;
int qkax_color;
int qklab_color;
float tk_len = .15;
float tk_len_x = .15;
float tk_len_y = .15;

int title_vspace = 0;
int xtitle_vspace = 0;
int ytitle_hspace = 0;

#endif



XV_FUNC(struct box*, last_plot_box, (void));
XV_FUNC(struct box*, last_q_b, (void));
XV_FUNC(int, erase_redraw, (int npts, float *x, float *y, int iopt));
XV_FUNC(int, draw, (int npts, float *x, float *y, int idraw));
XV_FUNC(int, qdchar, (char charac));
XV_FUNC(int, qdylab, (int trim));
XV_FUNC(int, qdxlab, (int trim));
XV_FUNC(int, decide_format, (float vmin, float vmax));
XV_FUNC(int, qdcode, (float value, char *text, char *format));
XV_FUNC(int, qdone, (void));
XV_FUNC(int, qdset, (float axmin, float axmax, float aymin, float aymax));
XV_FUNC(int, qdset_abs, (float sizex, float offsetx, float sizey, float offsety));
XV_FUNC(int, set_size, (float vmin, float vmax, double *rmin, double *rstep, float size));
XV_FUNC(int, qdtitl, (char *string));
XV_FUNC(int, qterm, (struct box *b));
XV_FUNC(int, qxlabl, (char *string));
XV_FUNC(int, qdset_ticklen, (float len));
XV_FUNC(int, qdset_ticklen_x, (float len));
XV_FUNC(int, qdset_ticklen_y, (float len));
XV_FUNC(int, qkxlabl_set, (int flag));
XV_FUNC(int, qkylabl_set, (int flag));
XV_FUNC(int, qkxunit, (char *prefix, char *unit));
XV_FUNC(int, qkyunit, (char *prefix, char *unit));
XV_FUNC(int, qkmax, (int x, int y));
XV_FUNC(int, qkmin, (int x, int y));
XV_FUNC(int, qylabl, (char *string));
XV_FUNC(int, qdxtk, (int trim));
XV_FUNC(int, qdytk, (int trim));
XV_FUNC(int, qdfile, (char *fname));
XV_FUNC(struct box*, xlabel, (char *string, float xpos, float ypos));
XV_FUNC(struct box*, ylabel, (char *string, float xpos, float ypos));
XV_FUNC(int, xlogtk, (int trim));
XV_FUNC(int, xloglab, (int trim));
XV_FUNC(int, ylogtk, (int trim));
XV_FUNC(int, yloglab, (int trim));
XV_FUNC(int, qkdraw, (int npts, float *x, float *y, int opt, float *xmin, float *xmax, float *ymin, float *ymax));
XV_FUNC(int, free_qkd_box , (void));
XV_FUNC(BX_AR*, duplicate_realloc, (BX_AR *xl, int k));
XV_FUNC(int, dev_box_init, (struct box *bc));
XV_FUNC(int, push_poly_point, (BX_AR *x, BX_AR *y, BX_AR xp, BX_AR yp, int flag));
XV_FUNC(int, qkd_lab_color, (int color));
XV_FUNC(int, qkd_color, (int ax_color, int gr_color));
XV_FUNC(int, qdbox, (char *ch));


XV_FUNC(struct box*, qkdim, (int opt,float xmin,float xmax, float ymin, float ymax));

# endif
