#ifndef _FFTL32N_H_
#define _FFTL32N_H_
/*	fftl32n.h  
 *	
 *	5 fev 1992	JMF
 *
 *	header for fftl32n.c
 *		set of routines for fast Fourier transforms
 *		in 32 bits with the DOS EXTENDER
 */

# include "platform.h"

# ifndef _FFTL32N_C_
XV_ARRAY(float, fftsin);
XV_ARRAY(int, mix);
# endif

XV_FUNC(int, fft_init, (int npts));
XV_FUNC(int, fftmixing, (int npts, float *x));
XV_FUNC(int, fft, (int npts, float *x, int df));
XV_FUNC(void, realtr1, (int npts, float *x));
XV_FUNC(void, realtr2, (int npts, float *x, int df));
XV_FUNC(int, fftwindow, (int npts, float *x));
XV_FUNC(int, fftwindow1, (int npts, float *x, float smp));
XV_FUNC(int, defftwindow1, (int npts, float *x, float smp));
XV_FUNC(int, fftwc, (int npts, float *x));
XV_FUNC(int, fftwc1, (int npts, float *x, float smp));
XV_FUNC(int, defftwc, (int npts, float *x));
XV_FUNC(int, defftwc1, (int npts, float *x, float smp));
XV_FUNC(void, spec_real, (int npts, float *x, float *spe));
XV_FUNC(void, spec_comp, (int npts, float *x, float *spe));
XV_FUNC(int, demodulate, (int npts, float *x, int freq));
XV_FUNC(void, derive_real, (int npts, float *x, float *y));
XV_FUNC(void, amp, (int npts, float *x, float *y));

# endif
