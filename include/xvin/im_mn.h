

# ifndef _IM_MN_H_
# define _IM_MN_H_

# include "platform.h"


XV_VAR(BITMAP*, plt_buffer);

XV_ARRAY(MENU, image_file_menu);
XV_ARRAY(MENU, plot_display_menu);
XV_ARRAY(MENU, plot_project_menu);
XV_ARRAY(MENU, image_edit_menu);
XV_ARRAY(MENU, image_treat_menu);
XV_ARRAY(MENU, image_option_menu);

XV_VAR(int, im_project_cur);
XV_VAR(int, im_project_n);
XV_VAR(int, im_project_m);


XV_FUNC(int, change_image_scale, (void));
XV_FUNC(int, check_image_scale,  (void));
XV_FUNC(int, change_image_opt,   (void));
XV_FUNC(int, check_image_opt,    (void));
XV_FUNC(int, check_image_menu,   (void));

XV_FUNC(int, set_image_menu_text, (int index, char* text));
XV_FUNC(int, set_image_menu_proc, (int index, int (*proc)(void)));

XV_FUNC(int, add_image_menu_item, 	(char* text, int (*proc)(void), struct MENU* child, int flags, void* dp));
XV_FUNC(int, add_image_edit_menu_item, 	(char* text, int (*proc)(void), struct MENU* child, int flags, void* dp));
XV_FUNC(int, add_image_treat_menu_item, (char* text, int (*proc)(void), struct MENU* child, int flags, void* dp));	

XV_FUNC(MENU*, image_basic_treat_menu,   (void));
XV_FUNC(MENU*, image_measures_treat_menu, (void));
XV_FUNC(MENU*, image_math_treat_menu,	 (void));
XV_FUNC(MENU*, image_2images_treat_menu, (void));
XV_FUNC(MENU*, image_to_plot_treat_menu, (void));
XV_FUNC(MENU*, image_filter_treat_menu,  (void));

XV_FUNC(int, reorder_loaded_image_file_list, (char *last, char *path));

# ifndef _IM_MN_C_
XV_ARRAY(MENU, image_menu);
XV_ARRAY(MENU, image_file_import_menu);
XV_ARRAY(MENU, image_file_export_menu);
XV_ARRAY(MENU, im_recently_loaded);
XV_ARRAY(char, fullfilei);
# endif

# ifndef _I_FILE_MN_C_
XV_VAR(char*, fui);
# else
char fullfilei[4096] = {0}, *fui = NULL;
# endif

# endif


