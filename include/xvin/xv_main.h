

# ifndef _XV_MAIN_H_
# define _XV_MAIN_H_

# include "platform.h"


# ifndef _XV_MAIN_C_
XV_VAR(DIALOG*, the_dialog);
XV_VAR(int, m_the_dialog);
XV_VAR(DATAFILE*, datafile);
XV_VAR(BITMAP*, plt_buffer);
XV_VAR(int, plt_buffer_used);
XV_VAR(int, screen_used);
XV_VAR(int,	eps_file_flag);
XV_ARRAY(char, prog_dir);
XV_ARRAY(char, config_dir); // where are config file xvin.cfg and log files

XV_VAR(int,  win_color_depth);
XV_VAR(int,  win_screen_w);
XV_VAR(int,  win_screen_h);
# endif

XV_ARRAY(char, first_data_dir);
XV_VAR(FONT*, small_font);
XV_VAR(FONT*, normalsize_font);
XV_VAR(FONT*, large_font);
XV_VAR(int, small_font_height);
XV_VAR(int, normalsize_font_height);
XV_VAR(int, large_font_height);
XV_VAR(int, small_font_width);
XV_VAR(int, normalsize_font_width);
XV_VAR(int, large_font_width);
XV_VAR(int, main_menu_height);


XV_FUNC(int, d_myclear_proc, (int msg, DIALOG *d, int c));
XV_FUNC(int, xvin_main, (int argc, char *argv[]));
XV_FUNC(int, xvin_pico, (int argc, char *argv[]));
XV_FUNC(DIALOG*, get_the_dialog, (void));
XV_FUNC(BITMAP*, get_plt_buffer, (void));
XV_FUNC(DIALOG*, get_cur_ac_reg, (void));
XV_FUNC(float*, get_fftsin, (void));
XV_FUNC(DIALOG*, find_dialog_associated_to_menu, (DIALOG *di, int *di_n));
XV_FUNCPTR(int, general_idle_action, (DIALOG *d));
XV_FUNCPTR(int, general_end_action, (DIALOG *d));
XV_FUNC(int, general_idle, (int msg, DIALOG *d, int c));
#endif

