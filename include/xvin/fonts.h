/* Allegro datafile object indexes, produced by grabber v4.4.2, Unix */
/* Datafile: /home/fx/xvin/misc/font_normal_screen.dat */
/* Date: Tue Dec 30 11:24:53 2014 */
/* Do not hand edit! */

#define LARGE_FONT                       0        /* FONT */
#define NORMALSIZE_FONT                  1        /* FONT */
#define SMALL_FONT                       2        /* FONT */
#define THE_PALETTE                      3        /* PAL  */

