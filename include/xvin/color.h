# ifndef _COLOR_H_
# define _COLOR_H_

# include "platform.h"

# define BUILD_COLOR(r,g,b)		(((b)&0xff)<<16)+(((g)&0xff)<<8)+((r)&0xff)
# define EXTRACT_R(color)		((color)&0xff)
# define EXTRACT_G(color)		(((color)>>8)&0xff)
# define EXTRACT_B(color)		(((color)>>16)&0xff)

#ifndef _COLOR_C_
XV_VAR(int, Black);
XV_VAR(int, Blue);
XV_VAR(int, Green);
XV_VAR(int, Cyan);
XV_VAR(int, Red);
XV_VAR(int, Magenta);
XV_VAR(int, Brown);
XV_VAR(int, Lightgray);
XV_VAR(int, Darkgray);
XV_VAR(int, Lightblue);
XV_VAR(int, Lightgreen);
XV_VAR(int, Lightcyan);
XV_VAR(int, Lightred);
XV_VAR(int, Lightmagenta);
XV_VAR(int, Yellow);
XV_VAR(int, White);

XV_VAR(int, aliceblue);
XV_VAR(int, antiquewhite);
XV_VAR(int, aqua);
XV_VAR(int, aquamarine);
XV_VAR(int, azure);
XV_VAR(int, beige);
XV_VAR(int, bisque);
XV_VAR(int, black);
XV_VAR(int, blanchedalmond);
XV_VAR(int, blue);
XV_VAR(int, blueviolet);
XV_VAR(int, brown);
XV_VAR(int, burlywood);
XV_VAR(int, cadetblue);
XV_VAR(int, chartreuse);
XV_VAR(int, chocolate);
XV_VAR(int, coral);
XV_VAR(int, cornflowerblue);
XV_VAR(int, cornsilk);
XV_VAR(int, crimson);
XV_VAR(int, cyan);
XV_VAR(int, darkblue);
XV_VAR(int, darkcyan);
XV_VAR(int, darkgoldenrod);
XV_VAR(int, darkgray);
XV_VAR(int, darkgreen);
XV_VAR(int, darkgrey);
XV_VAR(int, darkkhaki);
XV_VAR(int, darkmagenta);
XV_VAR(int, darkolivegreen);
XV_VAR(int, darkorange);
XV_VAR(int, darkorchid);
XV_VAR(int, darkred);
XV_VAR(int, darksalmon);
XV_VAR(int, darkseagreen);
XV_VAR(int, darkslateblue);
XV_VAR(int, darkslategray);
XV_VAR(int, darkslategrey);
XV_VAR(int, darkturquoise);
XV_VAR(int, darkviolet);
XV_VAR(int, deeppink);
XV_VAR(int, deepskyblue);
XV_VAR(int, dimgray);
XV_VAR(int, dimgrey);
XV_VAR(int, dodgerblue);
XV_VAR(int, firebrick);
XV_VAR(int, floralwhite);
XV_VAR(int, forestgreen);
XV_VAR(int, fuchsia);
XV_VAR(int, gainsboro);
XV_VAR(int, ghostwhite);
XV_VAR(int, gold);
XV_VAR(int, goldenrod);
XV_VAR(int, gray);
XV_VAR(int, grey);
XV_VAR(int, green);
XV_VAR(int, greenyellow);
XV_VAR(int, honeydew);
XV_VAR(int, hotpink);
XV_VAR(int, indianred);
XV_VAR(int, indigo);
XV_VAR(int, ivory);
XV_VAR(int, khaki);
XV_VAR(int, lavender);
XV_VAR(int, lavenderblush);
XV_VAR(int, lawngreen);
XV_VAR(int, lemonchiffon);
XV_VAR(int, lightblue);
XV_VAR(int, lightcoral);
XV_VAR(int, lightcyan);

/* lightgoldenrod*/
XV_VAR(int, yellow);
XV_VAR(int, lightgray);
XV_VAR(int, lightgreen);
XV_VAR(int, lightgrey);
XV_VAR(int, lightpink);
XV_VAR(int, lightsalmon);
XV_VAR(int, lightseagreen);
XV_VAR(int, lightskyblue);
XV_VAR(int, lightslategray);
XV_VAR(int, lightslategrey);
XV_VAR(int, lightsteelblue);
XV_VAR(int, lightyellow);
XV_VAR(int, lime);
XV_VAR(int, limegreen);
XV_VAR(int, linen);
XV_VAR(int, magenta);
XV_VAR(int, maroon);
XV_VAR(int, mediumaquamarine);
XV_VAR(int, mediumblue);
XV_VAR(int, mediumorchid);
XV_VAR(int, mediumpurple);
XV_VAR(int, mediumseagreen);
XV_VAR(int, mediumslateblue);
XV_VAR(int, mediumspringgreen);
XV_VAR(int, mediumturquoise);
XV_VAR(int, mediumvioletred);
XV_VAR(int, midnightblue);
XV_VAR(int, mintcream);
XV_VAR(int, mistyrose);
XV_VAR(int, moccasin);
XV_VAR(int, navajowhite);
XV_VAR(int, navy);
XV_VAR(int, oldlace);
XV_VAR(int, olive);
XV_VAR(int, olivedrab);
XV_VAR(int, orange);
XV_VAR(int, orangered);
XV_VAR(int, orchid);
XV_VAR(int, palegoldenrod);
XV_VAR(int, palegreen);
XV_VAR(int, paleturquoise);
XV_VAR(int, palevioletred);
XV_VAR(int, papayawhip);
XV_VAR(int, peachpuff);
XV_VAR(int, peru);
XV_VAR(int, pink);
XV_VAR(int, plum);
XV_VAR(int, powderblue);
XV_VAR(int, purple);
XV_VAR(int, red);
XV_VAR(int, rosybrown);
XV_VAR(int, royalblue);
XV_VAR(int, saddlebrown);
XV_VAR(int, salmon);
XV_VAR(int, sandybrown);
XV_VAR(int, seagreen);
XV_VAR(int, seashell);
XV_VAR(int, sienna);
XV_VAR(int, silver);
XV_VAR(int, skyblue);
XV_VAR(int, slateblue);
XV_VAR(int, slategray);
XV_VAR(int, slategrey);
XV_VAR(int, snow);
XV_VAR(int, springgreen);
XV_VAR(int, steelblue);
/*XV_VAR(int, tan);*/
XV_VAR(int, teal);
XV_VAR(int, thistle);
XV_VAR(int, tomato);
XV_VAR(int, turquoise);
XV_VAR(int, violet);
XV_VAR(int, wheat);
XV_VAR(int, white);
XV_VAR(int, whitesmoke);
XV_VAR(int, yellow);
XV_VAR(int, yellowgreen);

XV_FUNC(int, color_chooser, (void));

# endif

# define TRUE_COLOR 	0x1000000	/* normal box color	*/
# define FORCED_COLOR 	0x2000000	/* color imposed by operator and not by box */

# define TRUE_COLOR_WHITE_TO_BLACK 0x4000000
# define BW_WHITE_TO_BLACK 0x8000000
# define GRAY_WHITE_TO_BLACK 0x10000000


# define GREY_0 	64
# define RED_0 		128

# define XV2ALLEG(cl)		makecol(EXTRACT_R(cl), EXTRACT_G(cl), EXTRACT_B(cl))


# endif
