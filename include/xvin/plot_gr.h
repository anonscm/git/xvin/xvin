

/*	This series of functions handles the plot to gr files
 *	
 *	
 *
 */





#ifndef _PLOT_GR_H_
#define _PLOT_GR_H_

# include "xvin.h"


XV_FUNC(O_p*, create_plot_from_gr_file, (const char *file, const char *path));
XV_FUNC(int, load_plt_file_in_pltreg, (pltreg *pr, char *file, char *path));
XV_FUNC(int, pltreadfile, (O_p *op, char *file_name, int check));
XV_FUNC(int, push_plot_label, (O_p *op, float tx, float ty, char *label, int type));
XV_FUNC(int, save_one_plot, (O_p *op, const char *file));
XV_FUNC(int, save_one_plot_bin, (O_p *op, char *file));
XV_FUNC(int, save_one_plot_bin_auto, (O_p *op));



# endif /*  _PLOT_GR_H_ */

