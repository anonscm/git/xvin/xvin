# ifndef _DEV_PS_H_
# define _DEV_PS_H_

//#pragma once
# include "xvin/platform.h"
#include "time.h"

XV_FUNC(int 	,ps_close,(void));
XV_FUNC(int 	,ps_move,(float ix, float iy));
XV_FUNC(int 	,ps_rmove,(float ix, float iy));
XV_FUNC(int 	,ps_color,(int cl, int mode));
XV_FUNC(int 	,ps_line_size,(float size));
XV_FUNC(int 	,ps_mode,(int arg));
XV_FUNC(int 	,ps_dot,(float ix, float iy));
XV_FUNC(int 	,ps_line,(float x2, float y2));
XV_FUNC(int 	,ps_rline,(float x2, float y2));
XV_FUNC(int 	,ps_size,(int arg));
XV_FUNC(int 	,ps_clip_abs, (float x0, float y0, float x1, float y1)); 
XV_FUNC(int 	,ps_clip_abs_reset,(void)); /* cannot be override */
XV_FUNC(int 	,ps_clip_reset,());
XV_FUNC(int 	,ps_clip,(float x0, float y0, float x1, float y1));
XV_FUNC(int 	,ps_init,(void));
XV_FUNC(int 	,ps_init1,(void));
XV_FUNC(int 	,ps_catch1,(float x, float y));
XV_FUNC(int 	,ps_catch2,(float x, float y));
XV_FUNC(int 	,ps_start_bounding_box_2,(char *file, char *infile, char *inpath, 
			time_t *timer, float x0, float w, float y0, float h, float sc));

# endif
