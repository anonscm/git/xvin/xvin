# ifndef _XVIN_H_
# define _XVIN_H_

#ifndef XV_WIN32
# include "config.h"
#endif

# include "xvin/platform.h"
# include "xvin/color.h"
# include "xvin/gr_file.h"
# include "xvin/unitset.h"
# include "xvin/plot_ds.h"
# include "xvin/plot_op.h"
# include "xvin/plot_reg.h"
# include "xvin/plot_gr.h"
# include "xvin/util.h"
# include "xvin/plot2box.h"
# include "xvin/im_oi.h"
# include "xvin/im_reg.h"
# include "xvin/im_gr.h"
# include "xvin/plot_reg_gui.h"
# include "xvin/imr_reg_gui.h"
# include "xvin/xv_main.h"
# include "xvin/plot_mn.h"
# include "xvin/im_mn.h"
# include "xvin/matrice.h"
# include "xvin/log_file.h"
# include "xvin/file_picker_gui.h"

# include <stdlib.h>
# include <string.h>
#ifndef _XVINU_C_
XV_VAR(int, menu_x0);
XV_VAR(int, menu_y1);				/* menu area */
XV_VAR(int, icone_w);				/* the width of an icone */
XV_VAR(int, nx_icone);				/* the nuber of icone along x */
XV_VAR(int, grp_v_sp);				/* the distance of two menu group */
XV_VAR(int, Lift_W);				/* the width of a lift */
XV_VAR(int, max_x);
XV_VAR(int, max_y);					/* the scrren dimension */
XV_VAR(int, text_scale);			/* the scaling of box */
XV_VAR(int, screen_to_box);			/* the factor from box to screen */
XV_VAR(int, init_state);
XV_VAR(int, ac_color);				/* the active region color */
XV_VAR(int, menu_color);			/* the menu region color */
XV_VAR(int,	mn_index);
XV_VAR(int, last_popup_index);
XV_VAR(char*,last_answer);		/* answer string */
XV_VAR(DIALOG*, cur_ac_reg);	/* for compatibility */
XV_VAR(int, full_screen);
XV_VAR(int, screen_restoring);
XV_VAR(int, screen_acquired);
# endif

XV_FUNC(int, start_xvin, (int color_depth, int gfx_mode, int w, int h));


# include "xvin/box_def.h"
# include "xvin/color.h"

XV_FUNC(int, simple_string_to_box, (char *st,struct box *b0));
XV_FUNC(int, string_to_box,(char *st,struct box *b0));
XV_FUNC(char*, backslash_to_slash,(char *path));

XV_FUNC(int, warning_message, (const char *fmt, ...) __attribute__((format(printf, 1, 2))));
XV_FUNC(int, warning_message_log, (const char *log_file, const char *fmt, ...) __attribute__((format(printf, 2, 3))));
XV_FUNC(int, error_message, (const char *fmt, ...) __attribute__((format(printf, 1, 0))));
XV_FUNC(int, error_message_log, (const char *log_file, const char *fmt, ...) __attribute__((format(printf, 2, 3))));
XV_FUNC(int, debug_message, (const char *fmt, ...) __attribute__((format(printf, 1, 2))));
XV_FUNC(int, debug_message_log, (const char *log_file, const char *fmt, ...) __attribute__((format(printf, 2, 3))));

XV_VAR(int, IS_BATCH_SCRIPT);
XV_FUNC(int, win_scanf,(const char *fmt, ...));
XV_FUNC(int, win_scanf_in_list,(const char *fmt, int n_arg, void* arg_array));
XV_FUNC(int, win_vscanf, (const char *fmt, va_list ap));
XV_FUNC(int, win_printf,(const char *fmt, ...) __attribute__((format(printf, 1, 2))));
XV_FUNC(int, win_printf_OK,(const char *fmt, ...) __attribute__((format(printf, 1, 2))));
XV_FUNC(int, win_ti_vprintf,(const char *title, const char *fmt, va_list ap) __attribute__((format(printf, 2, 0))));
XV_FUNC(int, win_ti_printf,(const char *title, const char *fmt, ...) __attribute__((format(printf, 2, 3))));
XV_FUNC(void*, win_printf_ptr,(const char *fmt, ...) __attribute__((format(printf, 1, 2))));
XV_FUNC(int, win_simple_printf,(const char *fmt, ...) __attribute__((format(printf, 1, 2))));
XV_FUNC(int, win_simple_printf_OK,(const char *fmt, ...) __attribute__((format(printf, 1, 2))));
XV_FUNC(int, win_ti_simple_vprintf,(const char *title, const char *fmt, va_list ap) __attribute__((format(printf, 2, 0))));
XV_FUNC(int, win_ti_simple_printf,(const char *title, const char *fmt, ...) __attribute__((format(printf, 2, 3))));
XV_FUNC(void*, win_printf_simple_ptr,(const char *fmt, ...) __attribute__((format(printf, 1, 2))));

XV_FUNC(int, win_simple_printf,(const char *fmt, ...) __attribute__((format(printf, 1, 2))));
XV_FUNC(int, win_simple_printf_OK,(const char *fmt, ...) __attribute__((format(printf, 1, 2))));
XV_FUNC(int, win_ti_simple_vprintf,(const char *title, const char *fmt, va_list ap) __attribute__((format(printf, 2, 0))));
XV_FUNC(int, win_ti_simple_printf,(const char *title, const char *fmt, ...) __attribute__((format(printf, 2, 3))));
XV_FUNC(void*, win_simple_printf_ptr,(const char *fmt, ...) __attribute__((format(printf, 1, 2))));

XV_FUNC(int, set_XVin_resolution, (int size, int height, int is_full_screen));
XV_FUNC(int, TeX_modify, (const char *stuff));

XV_FUNC(int,	display_title_message, (const char *format, ...) __attribute__((format(printf, 1, 2))));
XV_FUNC(bool, init_fonts, (const char *datafilefullpath));

# define UNCHANGED	8192	/* leave it as it was */
# define OFF D_O_K

XV_FUNC(pltreg*, create_and_register_new_plot_project, (int x, int y, int w, int h));
XV_FUNC(pltreg*, create_and_register_hidden_new_plot_project, (int x, int y, int w, int h, char *name));
XV_FUNC(imreg*, create_and_register_new_image_project, (int x, int y, int w, int h));
XV_FUNC(imreg*,	create_and_register_hidden_new_image_project, (int x, int y, int w, int h, char *name));
XV_FUNC(imreg*,	create_and_register_new_empty_image_project, (int x, int y, int w, int h));
XV_FUNC(int, change_menu_name_of_current_project,(char *text));
XV_FUNC(int, what_is_current_project,(MENU  *mn, int *pltreg_nb, int *imreg_nb));
XV_FUNC(int, ac_grep, (DIALOG *di, char *fmt, ...) __attribute__((format(scanf, 2, 0))));

XV_FUNC(int, switch_project_to_this_imreg, (imreg *imr));
XV_FUNC(int, switch_project_to_this_pltreg, (pltreg *pr));


# endif
