#ifndef XV_MEMORIZED_WSCANF_HH
#   define XV_MEMORIZED_WSCANF_HH
#include "platform.h"
#ifndef XV_WIN32
# include "config.h"
#endif

#include <allegro/config.h>
#include <boost/preprocessor/comma_if.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/tuple/elem.hpp>
#ifdef __cplusplus
#   include <type_traits>
#   include <boost/algorithm/string.hpp>
#include <vector>
#endif

// following declarations are done without includes
// because of BITMAP conflict betweeen allegro and windows
#ifdef __cplusplus
extern "C" {
#endif
struct MENU;
#ifdef __cplusplus
}
#endif
XV_FUNC(int, win_scanf,(const char *fmt, ...));
XV_FUNC(int, add_item_to_menu, (MENU* mn, char* text, int (*proc)(void), struct MENU* child, int flags, void* dp));
#ifndef D_O_K
#   define D_O_K 0 // see allegro/gui.h
#endif
#ifndef OFF
#   define OFF  D_O_K // see xvin.h
#endif

#ifndef WIN_CANCEL
#   define WIN_CANCEL 129 // see xvin/gui_def.h
#endif

/* What it does: loads config / creates gui / saves new config
 * How to use it:
 *  # In the header, declare a sequence:
 *      > #define MY_SEQ (("KEY1", int, myvar1, 0))((, float, myvar2, _1.))
 *
 *     The first variable will be saved. The second will not as it's lacking a key.
 *
 *     The list of variables is a sequence of *doubled* parentheses
 *     ((..)) ((...)), containing each of them 4 different pieces of information
 *     ((key name, type, var name, default)).
 *
 *  # In the header, declare a structure:
 *      > CONFIG_DECLARE(MyStructName,MY_SEQ)
 *    or:
 *      > struct MyStructName
 *      > {
 *      >     CONFIG_STRUCT_DECLARE(MY_SEQ)
 *      >     .... // additional declarations
 *      > };
 *      > CONFIG_FUNC_DECLARE(MyStructName)
 *
 *
 *  # In the cc file, implement the calls:
 *      > CONFIG_IMPLEMENT(MyStructName,MY_SEQ,"write here the win_scanf string")
 *
 *  # In the gui file, declare a structure and use it:
 *      > MyStructName config; // values are initialzed to default
 *      > if(query(config, "MAIN KEY"))  return OFF; // returns true on CANCEL!
 *      > printf("myvar1 = %d\n", config.myvar1);
 *      > printf("myvar2 = %f\n", config.myvar2);
 */
#ifdef __cplusplus
namespace config
{
    inline bool is_good_key(char const * k)
    {
        if(k == nullptr)
            return false;

        for(size_t i = 0; k[i] != '\0'; ++k)
            if(k[i] != ' ') return true;
        return false;
    }

    inline bool is_good_key(char const * k1, char const * k2)
    { return is_good_key(k1) && is_good_key(k2); }

    template <typename T> T    get(char const * k1, char const * k2, T & d);
    template <typename T> T    set(char const * k1, char const * k2, T const & d);
#   define _C_DO(T,N)                                                       \
    template <> inline T get<T>(char const * k1, char const * k2, T & d)    \
    { if(is_good_key(k1,k2)) d = get_config_##N(k1,k2,d); return d; }       \
    template <> inline T set<T>(char const * k1, char const * k2,           \
                                   T const & d)                             \
    { if(is_good_key(k1,k2)) set_config_##N(k1,k2,d); return d; }

    template <typename T>
    inline std::vector<T>  & get(char const * k1, char const * k2, std::vector<T> &d)
    {
        char tmp[2048];
        for(size_t i = 0u, e = d.size(); i < e; ++i)
        {
            snprintf(tmp, sizeof(tmp), k2, i);
            get<T>(k1, tmp, d[i]);
        }
        return d;
    }

    template <typename T>
    inline std::vector<T>  & set(char const * k1, char const * k2, std::vector<T> &d)
    {
        char tmp[2048];
        for(size_t i = 0u, e = d.size(); i < e; ++i)
        {
            snprintf(tmp, sizeof(tmp), k2, i);
            set<T>(k1, tmp, d[i]);
        }
        return d;
    }

    _C_DO(int,int)
    _C_DO(float,float)
    _C_DO(char const *,string)

    template <>
    inline std::string get<std::string>(char const * k1, char const * k2, std::string & d)
    { char const *r = d.c_str(); r = get(k1, k2, r); return (d = r == nullptr ? "" : r); }

    template <>
    inline std::string set<std::string>(char const * k1, char const * k2, std::string const & d)
    { set(k1, k2, d.c_str()); return d;}

    template <>
    inline bool get<bool>(char const * k1, char const * k2, bool & d)
    { int k = d ? 1 : 0; get(k1, k2, k); d = k != 0; return d; }

    template <>
    inline bool set<bool>(char const * k1, char const * k2, bool const & d)
    { int k = d ? 1 : 0; set<int>(k1, k2, k); return d; }

#   undef _C_DO
    template <typename T>
    struct Save
    {
        template <typename T0, typename T1>
        Save(T0 k0, T1 k1, T & x, int & res) : _k0(k0), _k1(k1), _x(x), _r(res)
        { get<T>(_k0.c_str(), _k1.c_str(), _x);   }
        ~Save() { if(_r) set<T>(_k0.c_str(), _k1.c_str(), _x);   }

        constexpr T * operator*() { return &_x; }

        private:
            std::string _k0;
            std::string _k1;
            T   &       _x;
            int &       _r;
    };

    template <typename T0, typename T1, typename T>
    auto save(T0 k0, T1 k1, T & x, int & r) { return Save<T>(k0, k1, x, r); }

    template <typename T>
    struct Item
    {
        int         load   (char const * k1) { get(k1, title, addr); return 0; }
        int         save   (char const * k1) { set(k1, title, addr); return 0; }
        T  *        address    ()       { return &addr; }
        T const &   operator * () const { return addr; }

        char const * title;
        T          & addr;
    };

    template <>
    struct Item<bool>
    {
        char const * title;
        bool       & mem;
        int          addr = 0;

        int             load   (char const * k1)
        { addr = mem ? 1 : 0; get(k1, title, addr); mem = addr != 0; return 0; }
        int             save   (char const * k1)
        { mem = addr != 0; set(k1, title, addr); return 0; }
        int  *          address    () { return &addr; }
        bool const &    operator * () const { return mem; }
    };

    template <>
    struct Item<std::string>
    {
        char const  * title;
        std::string & mem;
        char          addr[2048] = "";

        int             load   (char const * k1)
        { get(k1, title, mem); snprintf(addr, sizeof(addr), "%s", mem.c_str()); return 0; }
        int             save   (char const * k1)
        { mem = addr; set(k1, title, mem); return 0; }
        char     *      address    ()       { return addr; }
        std::string &   operator * () const { return mem;  }
    };

    template <typename T>
    constexpr inline Item<T>        item(char const * a, T & b) { return Item<T>{a, b}; }
    template <typename T>
    constexpr inline Item<T>        item(T & b) { return Item<T>{nullptr, b}; }

    template <typename T>
    constexpr inline  typename std::enable_if<std::is_arithmetic<T>::value, Item<T>>::type
    item(T * b) { return Item<T>{nullptr, *b}; }

    template <typename T>
    constexpr inline Item<T> &      item(Item<T> & b) { return b; }
    template <typename T>
    constexpr inline Item<T const > item(char const * a, T const & b)
    { return Item<T const>({a, b}); }

    template <typename ...Args>
    inline void load(char const * k1, Item<Args>... args)
    { [](auto ...){}(args.load(k1)...); }

    template <typename ...Args>
    inline void save   (char const * k1, Item<Args> ... args)
    { [](auto ...){}(args.save(k1)...); }

    template <typename ...Args>
    inline bool query(char const * key, char const * msg, Args && ... args)
    {
        auto dummy = [](auto ...){};
        return [&](auto && ... items)
        {
            dummy(items.load(key)...);
            if(WIN_CANCEL == win_scanf(msg, items.address()...))
                return true;
            dummy(items.save(key)...);
            return false;
        }(item(args)...);
    }
}
#endif

#define _CAS_UNPACK(M,K,C)                                       \
    M(K, BOOST_PP_TUPLE_ELEM(4,0,C), BOOST_PP_TUPLE_ELEM(4,1,C), \
         BOOST_PP_TUPLE_ELEM(4,2,C), BOOST_PP_TUPLE_ELEM(4,3,C))

#define _CAS_DECLARE(A,K,C)                                     \
    BOOST_PP_TUPLE_ELEM(4,1,C) BOOST_PP_TUPLE_ELEM(4,2,C)       \
        = BOOST_PP_TUPLE_ELEM(4,3,C);
#define _CAS_LOAD_(K,N,T,V,_)   , config::item(BOOST_PP_STRINGIZE(N), self.V)
#define _CAS_LOAD(_,__,C)       _CAS_UNPACK(_CAS_LOAD_,_,C)
#define _CAS_SAVE(_,__,C)       _CAS_UNPACK(_CAS_LOAD_,_,C)
#define _CAS_CLS(_,ITEMS,C)                                     \
    BOOST_PP_TUPLE_ELEM(2,0,ITEMS).BOOST_PP_TUPLE_ELEM(4,2,C)   \
        = BOOST_PP_TUPLE_ELEM(2,1,ITEMS).BOOST_PP_TUPLE_ELEM(4,2,C);

#define CONFIG_STRUCT_DECLARE(C) BOOST_PP_SEQ_FOR_EACH(_CAS_DECLARE,_,C)
#define CONFIG_FUNC_DECLARE(S)                                                  \
    void load   (S       & self, char const * key);                             \
    void save   (S const & self, char const * key);                             \
    bool query  (S       & self, char const * key);

#define CONFIG_DECLARE(S,C)                                                     \
    struct S { CONFIG_STRUCT_DECLARE(C); };                                     \
    CONFIG_FUNC_DECLARE(S)

#define CONFIG_IMPLEMENT(S,C,MSG)                                               \
    void load   (S       & self, char const * key)                              \
    { config::load(key BOOST_PP_SEQ_FOR_EACH(_CAS_LOAD,S,C)); }                 \
    void save   (S const & self, char const * key)                              \
    { config::save(key BOOST_PP_SEQ_FOR_EACH(_CAS_LOAD,S,C)); }                 \
    bool query  (S       & self, char const * key)                              \
    { return config::query(key, MSG BOOST_PP_SEQ_FOR_EACH(_CAS_LOAD,S,C)); }

#define CONFIG_DECLARE_AND_IMPLEMENT(S,C,MSG)                                   \
    CONFIG_DECLARE(S,C); CONFIG_IMPLEMENT(S,C,MSG)

#define CONFIG_COPY(S1, S2, C) BOOST_PP_SEQ_FOR_EACH(_CAS_CLS,(S1,S2),C)
#define _CONFIG_COPY_ARG(A,B,C) args.BOOST_PP_TUPLE_ELEM(4,2,C),
#define CONFIG_COPY_FUNC(C,N,X)                                             \
    template <typename T> inline auto N(T const & args)                     \
    -> decltype(BOOST_PP_SEQ_FOR_EACH(_CONFIG_COPY_ARG,_,X)  C())           \
    { C cf; CONFIG_COPY(cf, args, X); return cf; }

/* What it does: declares & creates plugin menus which can optionally be linked.
 * How to use it:
 *  # In the header, declare a sequence:
 *      > #define MY_SEQ (false, name1, menu1)) ((true, name2, menu2) ((true, name3, menu3)
 *
 *     The 2 last menus are linked: completing menu2 succesfully launches menu3.
 *     The 1st menu is not linked: completing *does not* launch menu2.
 *
 *  # In the header, declare a structure:
 *      > MENU_DECLARE_ALL(pluginname,MY_SEQ)
 *
 *  # In the cc file (c++ is mandatory), implement the calls:
 *      > MENU_IMPL_ALL(pluginname,MY_SEQ)
 */
#ifdef __cplusplus
namespace menu
{
    typedef int (*fcn_t)();
    template <bool FLOW, fcn_t FCN>
    struct Item
    {
        constexpr static fcn_t          fcn   = FCN;
        constexpr static bool           flow  = FLOW;
    };

    inline int dummy() { return D_O_K; }

    template <typename ...FCNS>
    struct List
    {
        constexpr static size_t size()
        {
            if(sizeof...(FCNS) == 0u)
                return 0u;

            constexpr fcn_t fcns [] = { FCNS::fcn  ...};
            if(fcns[sizeof...(FCNS)-1u] == dummy)
                return sizeof...(FCNS)-1u;

            return sizeof...(FCNS);
        }

        template <size_t I>
        static int call()
        {
            constexpr fcn_t fcns [] = { FCNS::fcn  ...};
            constexpr bool  flows[] = { FCNS::flow ..., false};

            for(auto i = I; i < size() && flows[i]; ++i)
            {
                auto ans = (*fcns[i])();
                switch(ans)
                {
                    case WIN_CANCEL: return OFF;
                    case D_O_K:      break;
                    default:         return ans;
                }
            }
            return D_O_K;
        }

#       define _CALL_WRAP(_1,N,_2) static int call_##N() { return call<N>(); }
        BOOST_PP_REPEAT(101, _CALL_WRAP,)
#       undef _CALL_WRAP

        static MENU * create(std::initializer_list<char const *> args)
        {
            static MENU mn[sizeof...(FCNS)];
            static bool done = false;
            if(!done)
            {
                done = true;
#               define _CALL_WRAP(_1,N,_2) call_##N,
                constexpr fcn_t fcns [] = { BOOST_PP_REPEAT(100, _CALL_WRAP,) call_100 };
#               undef _CALL_WRAP
                constexpr fcn_t raws [] = { FCNS::fcn      ...};
                constexpr bool  flows[] = { FCNS::flow     ..., false};

                // convert char const * to char *
                constexpr auto nstr = 2048u;
                auto           j    = 0u;
                static    char titles[nstr*(size()+1)];
                for(auto t: args) { strcpy(titles+j*nstr, t); ++j; }

                for(size_t i = 0; i < size(); ++i)
                    add_item_to_menu(mn+i, titles+i*nstr,
                                     flows[i] && flows[i+1] ? fcns[i] : raws[i],
                                     nullptr,0,nullptr);
            }
            return mn;
        }
    };

    template <> struct List<> {}; // force a compiler failure upon use !!!
}
#endif

#define _XV_MENU_FCNS(N,_,C)                                                \
    menu::Item<BOOST_PP_TUPLE_ELEM(3, 0, C), BOOST_PP_TUPLE_ELEM(3, 2, C)>,
#define _XV_MENU_TITLES(N,_,C) BOOST_PP_TUPLE_ELEM(3, 1, C),
#define _XV_MENU_DEC(N,_,C) PXV_FUNC(int, BOOST_PP_TUPLE_ELEM(3, 2, C), (void));

#define MENU_IMPL(NAME,C)                                                   \
    MENU *NAME##_plot_menu(void)                                            \
    {                                                                       \
        return menu::List<                                                  \
            BOOST_PP_SEQ_FOR_EACH(_XV_MENU_FCNS,_,C)                        \
            menu::Item<false, menu::dummy>>::create                         \
        ({ BOOST_PP_SEQ_FOR_EACH(_XV_MENU_TITLES,_,C) ""});                 \
    }

#define MENU_IMPL_LOAD_UNLOAD(NAME,LCODE,UCODE,PLOT)                        \
    int    NAME##_main(int, char **)                                        \
    {                                                                       \
        LCODE;                                                              \
        static char n[] = #NAME;                                            \
        add_##PLOT##_treat_menu_item(n, nullptr, NAME##_plot_menu(),        \
                               0, nullptr);                                 \
        return D_O_K;                                                       \
    }                                                                       \
    int    NAME##_unload(int, char **)                                      \
    {                                                                       \
        UCODE;                                                              \
        char tmp[512] = #NAME;                                              \
        remove_item_to_menu(plot_treat_menu, tmp, nullptr, nullptr);        \
        return D_O_K;                                                       \
    }

#define MENU_IMPL_ALL(NAME,C)                                               \
    MENU_IMPL(NAME,C); MENU_IMPL_LOAD_UNLOAD(NAME,,,plot)
#define MENU_IMPL_CUSTOM(NAME,C,LCODE,UCODE)                                \
    MENU_IMPL(NAME,C); MENU_IMPL_LOAD_UNLOAD(NAME,LCODE,UCODE,plot)

#define IMG_MENU_IMPL_ALL(NAME,C)                                           \
    MENU_IMPL(NAME,C); MENU_IMPL_LOAD_UNLOAD(NAME,,,image)
#define IMG_MENU_IMPL_CUSTOM(NAME,C,LCODE,UCODE)                            \
    MENU_IMPL(NAME,C); MENU_IMPL_LOAD_UNLOAD(NAME,LCODE,UCODE,image)

#define MENU_DECLARE_ALL(NAME,C)                                            \
    PXV_FUNC(MENU *, NAME##_plot_menu, (void));                             \
    PXV_FUNC(int,    NAME##_main,      (int, char **));                     \
    PXV_FUNC(int,    NAME##_unload,    (int, char **));                     \
    BOOST_PP_SEQ_FOR_EACH(_XV_MENU_DEC,_,C)
#endif
