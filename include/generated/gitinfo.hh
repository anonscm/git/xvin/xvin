/* WARNING! All changes made to this file will be lost! */
#pragma once
namespace git
{
    constexpr char const * branch () { return "master";  } 
    constexpr char const * hashtag() { return "e9a8505dd67f59db9e83c4f3d748a8322ac820f7"; } 
    constexpr char const * version() { return "pico_ueye_v1.2.4-16-ge9a8"; }
    constexpr char const * author () { return "Mriv31";  }
    constexpr char const * date   () { return "Sun, 4 Mar 2018 23:02:56 +0100";    }
    constexpr bool         clean  () { return  false;    }
}
