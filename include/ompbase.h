#pragma once
#ifndef _OPENMP
#   define PRAGMA_OMP(X)
#else
#   include <omp.h>
#   define _PRAGMA_OMP(X) _Pragma(#X)
#   define PRAGMA_OMP(X) _PRAGMA_OMP(omp X)
#endif

namespace omp
{
    inline size_t max()
    {
#       ifndef _OPENMP
        return 1;
#       else
        return size_t(omp_get_max_threads());
#       endif
    }

    inline size_t size()
    {
#       ifndef _OPENMP
        return 1;
#       else
        return omp_get_num_threads();
#       endif
    }

    inline size_t id()
    {
#       ifndef _OPENMP
        return 0;
#       else
        return omp_get_thread_num();
#       endif
    }


    template <typename T, typename K>
    struct Reducer
    {
        Reducer(T & base, K && fcn)
          : _fcn (fcn)
          , _base(base)
          , _vals(base,  max()-1)
          , _used(false, max()-1)
        {}

        Reducer(K && fcn)
          : _fcn (fcn)
          , _vals(max()-1)
          , _used(false, max()-1)
        {}

        Reducer()                       = delete;
        Reducer(Reducer<T,K> const &)   = delete;
        Reducer(Reducer<T,K>       &&)  = default;

        ~Reducer() { reduce(); }

        T & operator *()
        {
            auto i = id();
            if(i == 0)
            {
                _bused = true;
                return _base;
            } else
            {
                _used[i-1] = true;
                return _vals[i-1];
            }
        }

        auto reduce() { return _reduce(_base, _fcn); }

        protected:
            size_t _move()
            {
                if(_red)
                    return _vals.size();
                _red = true;

                if(_bused)
                    return 0;

                size_t i = 0, e = _vals.size();
                for(; i < e && !_used[i]; ++i)
                    ;
                if(i < e)
                    _base = std::move(_vals[i]);
                return i+1;
            };

            template <typename B, typename F>
            auto _reduce(B & base, F & fcn) -> decltype(fcn(base, base), base)
            {
                auto i = _move();
                for(auto e = _vals.size(); i < e; ++i)
                    if(_used[i])
                        fcn(base, _vals[i]);
                return base;
            }

            template <typename B, typename F>
            auto _reduce(B & base, F & fcn) -> decltype(fcn(base), std::declval<void>())
            {
                auto i = _move();
                if(i <= _vals.size())
                    fcn(base);

                for(auto e = _vals.size(); i < e; ++i)
                    if(_used[i])
                        fcn(_vals[i]);
            }

            K &                 _fcn;
            T &                 _base;
            std::valarray<T>    _vals;
            std::valarray<bool> _used;
            bool                _bused = false;
            bool                _red   = false;
    };

    template <typename T, typename K>
    inline auto reducer(T & base, K && fcn)
    { return Reducer<T,K>(base, fcn); }

    template <typename T, typename K>
    inline auto reducer(K && fcn)
    { return Reducer<T,K>(fcn); }

    template <typename I, typename T, typename K1, typename K2>
    inline auto mapreduce(I & item, T & res, K1 map, K2 red)
    -> decltype(map(item, res[0]), reducer(item, red), item)
    {
        auto e   = int(res.size());
        auto all = reducer(item, red);

        PRAGMA_OMP(parallel)
        {
            auto & cur = *all;

            PRAGMA_OMP(for)
            for(int j = 0; j < e; ++j)
                map(cur, res[j]);
        }
        return item;
    };

    template <typename I, typename T, typename K1, typename K2>
    inline auto mapreduce(T & res, K1 map, K2 red)
    { I item; return reducer(item, res, map, red); }
}
