#pragma once
#include <utility>
#include <type_traits>
namespace menu
{
    template <typename Cf, typename T, typename ... Args>
    inline int _run_ac(T fcn, char const * formkey, char const * help,
                       char const * fmt, Args * ... args)
    {
        if(updating_menu_state != 0)    return D_O_K;

        /* display routine action if SHIFT is pressed */
        if (key[KEY_LSHIFT]) return win_printf_OK(help);

        //we assume data in xd on integer frame numbers
        /* we first find the data that we need to transform */
        char tmp[1024]; strcpy(tmp, fmt);
        if (ac_grep(cur_ac_reg, tmp, &args ...) != sizeof...(Args))
            return win_printf_OK("cannot find data");

        static Cf cf;
        if(query(cf, formkey)) return OFF;

        if(fcn(cf, args ...))
            return OFF;
        return D_O_K;
    }

    template <typename Cf, typename T>
    inline int run(T fcn, char const * formkey, char const * help)
    { return _run_ac<Cf, T, pltreg, O_p, d_s>
             (fcn, formkey, help, "%pr%op%ds", nullptr, nullptr, nullptr); }

    template <typename Cf, typename T>
    inline int run_op(T fcn, char const * formkey, char const * help)
    { return _run_ac<Cf, T, pltreg, O_p>
             (fcn, formkey, help, "%pr%op", nullptr, nullptr); }

    template <typename Cf, typename T>
    inline int run_pr(T fcn, char const * formkey, char const * help)
    { return _run_ac<Cf, T, pltreg>
             (fcn, formkey, help, "%pr", nullptr); }

    inline bool refresh(pltreg * pr, O_p *op)
    {
        if(op == nullptr)
            return false;
        op->need_to_refresh = 1;
        if(pr == nullptr)
            return false;
        for(auto i = 0;  i< pr->n_op; ++i)
            if(pr->o_p[i] == op)
            {
                refresh_plot(pr, i);
                break;
            }
        return false;
    };
}
