#ifndef _CARDINAL_H_
#define _CARDINAL_H_
#include "platform.h"
typedef struct _camera_cor
{
  int cam_fil;
  int frame;
  int w_flag;
  int init;
  float factor;
} cam_cor;

typedef struct _ref_position
{
    float bd_x0;
    float bd_y0;
    float bd_z0;
    float du_x0;
    float du_y0;
    float du_z0;    
} ref_pos;



PXV_FUNC(d_s*, find_cur_data_set_in_op, (O_p *ops));
PXV_FUNC(int, mean_y2_on_array, (float *yd, int nx, int win_flag, float *meany, 
			float *my2, float *my4));
PXV_FUNC(int, mean_y2_on_op, (O_p *op, int win_flag, float *meany, float *my2, 
			float *my4, float *kxu, int *nxeff));
PXV_FUNC(int, mean_y2, (void));
PXV_FUNC(O_p*, x_ou_y_de_t_on_op, (O_p *ops, int axis));
PXV_FUNC(int, y_de_t_et_x_t_new, (void));
PXV_FUNC(int, correct_simple_glitch, (void));
PXV_FUNC(float,	delta_f_de, (float fmin, float fmax, float fc));
PXV_FUNC(float, camera_response, (float freq_over_fech, int frame, float a));
PXV_FUNC(float, compute_r_for_acq_data, (int nx, int fb, int fm, float fc, 
		int frame, float opening, float *rap));
PXV_FUNC(int, alternate_lorentian_fit_acq_spe_with_error, (float *y, int ny, 
			float dy, int fb, int fm, int w_flag, int cam_fil, float factor, 
			int frame, float *fc, float *a, float *dfc, float *da, float *chisq, 
			float *dy0));
PXV_FUNC(int, invert_delta_f, (float fb, float fm, float r, float *fc));	
PXV_FUNC(int, invert_delta_f_for_acq_data, (int nx, int fb, int fm, float r, 
			float *fc, int frame, float opening, float *dfcdr));
PXV_FUNC(float*, compute_spe_for_acq_data, (int nx, float fc, int frame, 
			float opening, float *spe));	
PXV_FUNC(O_p, *do_fft_and_derivate_on_op_2, (O_p *op, int fb, int fd, int w_flag, 
		float fc, int frame, float factor, float *etar, float *detar));				
PXV_FUNC(int, draw_delta_de_fc_4, (void));
PXV_FUNC(int, do_lin_log_spectrum_avg, (void));

PXV_FUNC(int, find_anisotropy_and_rotate_plot, (void));
PXV_FUNC(int, do_full_new_treat_x_y_z_acq_spe, (void));


PXV_FUNC(int, do_cardinal_rescale_plot, (void));
PXV_FUNC(MENU*, cardinal_plot_menu, (void));
PXV_FUNC(int, do_cardinal_rescale_data_set, (void));
PXV_FUNC(int, cardinal_main, (int argc, char **argv));
#endif






