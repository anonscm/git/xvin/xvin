.PHONY: include/generated/gitinfo.h include/generated/gitinfo.hh

include/generated/gitinfo.h:
	echo "#pragma once" > $@
	echo "#define GIT_COMMIT_HASH \"`git log -n 1 --pretty=format:%H`\"" >> $@
	echo "#define GIT_COMMIT_DATE \"`git log -n 1 --pretty=format:%cD`\"" >> $@
	echo "#define GIT_COMMIT_AUTHOR \"`git log -n 1 --pretty=format:%an`\"" >> $@
	echo "#define GIT_COMMIT_BRANCH \"`git rev-parse --abbrev-ref HEAD`\"" >> $@

include/generated/config.h:
	touch $@

include/generated/gitinfo.hh:
	echo "/* WARNING! All changes made to this file will be lost! */" >  $@
	echo "#pragma once" >>  $@
	echo "namespace git" >>  $@
	echo "{" >>  $@
	echo "    constexpr char const * branch () { return \"`git rev-parse --abbrev-ref HEAD`\";  } " >>  $@
	echo "    constexpr char const * hashtag() { return \"`git log -n 1 --pretty=format:%H`\"; } " >>  $@
	echo "    constexpr char const * version() { return \"`git describe --abbrev=1 --tags`\"; }" >>  $@
	echo "    constexpr char const * author () { return \"`git log -n 1 --pretty=format:%an`\";  }" >>  $@
	echo "    constexpr char const * date   () { return \"`git log -n 1 --pretty=format:%cD`\";    }" >>  $@
	echo "    constexpr bool         clean  () { return  false;    }" >>  $@
	echo "}" >>  $@

include/generated/config.hh:
	touch $@
