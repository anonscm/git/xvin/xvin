How to build XVin on Mac Os X (l10.5 : leopard)

This just is one of the many possible methods. I used MacPorts, which creates a unix directory tree / environment. This method results in a XVin port that doesn't depend on X11.

First, install the libraries:

$ sudo port -v install freetype
$ sudo port -v install allegro 
$ sudo port -v install gsl

This installs freetype2, allegro 4.2.2, and gsl, as of 2008/02/08, but fftw3 isn't ported yet: "fftw" gives fftw 2.1.5). To get the fftw3 library, I compiled the sources from fftw-3.1.2.tar.gz shipped with the XVin sources, using regular commands but the MacPort directory tree:

$ cd fftw-3.1.2/
$ ./configure --prefix=/opt/local
$ make
$ sudo make install


Then, to compile XVin, one just needs to modify "platform.mk" to specify the MacOS target:

SYSTEM = MACOS

if the processor in the Mac is a Motorola, then an additional option is required . By default,  since 2008/01, Xvin assums a Mac with Intel. For the Motorola Macs, one should have:

CPU_FLAGS     = -O2 -DMAC_POWERPC # for MACOS with PowerPC/G4/G5

We also have to specify where MacPorts built its tree, which gives the required libraries XVin depends on. This is done by the following line: 

GNU_LIBS_DIR = /opt/local               # most typical 

After the full compilation of XVin, you may have to indicate to the system where to search for dynamical libraries. This is done by adding the following lines in the  ".bash_profile" file: 

export PATH=$PATH:$HOME/xvin/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/xvin/bin

then XVin is running...

