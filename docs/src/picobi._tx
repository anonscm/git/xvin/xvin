@#	    	
@# This is the source for the Pico docs, in a weird custom format.
@# Read makedoc.c for a description of what is going on...
@#
@# If you want to put everything in one big HTML file rather than splitting
@# it into sections, remove this 'multiplefiles' statement:
@#
@multiplefiles
@#
@# Uncomment the following statement to output css clean html files.
@#
@#ignore_css
@#
@external-css=allegro.css
@document_title=Pico Manual
@html_footer=Back to contents
@rtfh=Xvin - An image tracking software for magnetic tweezers 
@manh="version 0.1" "Pico" "Pico manual"
@mans=#include <xvin.h>
@$\input texinfo
@$@setfilename pico.inf
@$@settitle Pico Manual
@$@setchapternewpage odd
@$@paragraphindent 0
@$@setchapternewpage off
@# This should remain commented at the moment (dvi output) @$@finalout
@$
@$@ifinfo
@$@direntry
@$* Xvin: An image tracking software package for magnetic tweezers 
@$@end direntry
@$This is the Info version of the Pico manual
@$
@$By Vincent Croquette and Jean-Fran�ois Allemand
@$@end ifinfo
@$
@$@node Top, , (dir), (dir)
@titlepage
@<pre>
@!indent



       Wetlab protocols for preparation of samples and DNA for use with a Picotwist

         By Terence Strick, January, 2009.

        See the AUTHORS file for a
           complete list of contributors.
@indent
@</pre>


@!titlepage


@!text
@heading
Contents

@contents



@text
@heading
Overview

This manual provides protocols and guidelines for preparing DNA molecules and samples in view of single-molecule
experimentation using a PicoTwist apparatus.


@heading
Short description

The single-molecule DNA nanomanipulation made possible by a PicoTwist apparatus involves tethering a single DNA
molecule at one end to a glass surface and at the other end to a small superparamagnetic bead.  To accomplish
this, specially-prepared DNA molecules are first tethered to superparamagnetic beads. The resulting bead-DNA complexes
are incubated on the treated glass coverslip which forms the "floor" of the PicoTwist sample chamber.  After rinsing
out unbound beads, the user must identify an adequate bead-DNA complex and then calibrate its mechanical properties before
experiments can begin.

The manual covers

-DNA preparation

-Treatment of glass surfaces

-Assembly of DNA-bead complexes

-Injection of DNA-bead complexes into the sample chamber

-Incubation and rinsing procedures for eliminating excess, unbound beads

-Methods for screening bead-DNA complexes

-Various tips, tricks, and workarounds for debugging purposes

It is important to note that the protocols described here are only a staring point which will allow a novice
user to begin performing experiments.  It is assumed that the user is comfortable using and debugging
the standard molecular biology toolkit for DNA(PCR, restriction digestion, purification, cloning...). 
Some experiments may require more advanced strategies for preparing the various components (DNA, surfaces, beads...), 
and interested users can write to the PicoTwist support team for help in devising appropriate experimental strategies.

It is also important to note that, just as with the vast majority of surface sample preparations, there is
an intrinsic degree of variability in surface quality obtained with these protocols which may affect the yield
of usable bead-DNA complexes.

@heading
Preparation of the flow cell

Reagents required

Flow cell

Phosphate-buffered saline (1x)

Potassium phosphate buffer pH 8, 1M

BSA (Roche XXX); stock is 10 mg/ml in H_20; made fresh daily and resuspended at 37oC.

Tween-20 (Roche)

Polyglutamic acid, Mw 1,500�3,000 (Sigma); L and R forms are ok; larger Mw also; stock is 10 mg/ml in PBS

Sodium Azide 

Standard Buffer (SB) (10 mM Potassium Phosphate buffer pH 8; 0.1 mg/ml BSA; 0.1% Tween-20) 

Sheep polyclonal anti-digoxigenin (Roche 1 333 089); stock is 100 micrograms/ml in PBS



0. Make stock of antidig, aliquot and store at -20^{o}C

1. Clean glass coverslip in a nitric acid bath for 4 h, rinse extensively with ultrapure water and dry with a stream of clean argon.

2. Make hydrophobic.

3. Assemble flow cell.

4. Inject into the flow cell the polyclonal anti-digoxigenin solution. Place the capillary in a humid chamber and incubate at 37 ?C overnight.

5. Prepare a blocking solution of SB containing 10 mg/ml BSA, 3.3 mg/ml of polyglutamic acid and 3 mM sodium azide. Drain the capillary and inject 100 �l of the blocking solution into the capillary; place it in a humid chamber and incubate at 37 C for at least 8 h. 

The resulting blocked flow cell can be stored at 4o C for up to two weeks provided it is hermetically sealed and sterile 
working conditions are ensured.

@heading
DNA preparation

Three DNA fragments are ligated together to build these constructs:

1. The "target" DNA which will be extended and twisted using nanomanipulation, and which may eventually contain a specific sequence
of interest.  Depending on the application, the DNA is typically between 2 and 4 kb in length.

2. A ~1 kb biotin-bearing DNA fragment which is required to tether the construct to the biotin-labelled superparamagnetic bead.

3. A ~1 kb digoxigenin-bearing DNA fragment which is required to tether the construct to the digoxigenin-coated glass surface.

The target DNA can be constructed through a variety of molecular cloning strategies; the best one will depend
on the intended experiment.  The labelled DNA fragments are prepared by PCR-based incorporation of modified dNTPs.
Note that the protocols provided should serve as guidelines only, as changes in fragment length, GC-content, or other
parameters may require specific optimization.

Reagents required:

DNA polymerase, high fidelity, 3U/mirolitre and 10x reaction buffer (Roche)

dNTP mix (2.5 mM each) (Roche)

Biotin-16-deoxyuridine triphosphate (dUTP) (1 mM) (Roche)

Digoxigenin-11-dUTP, alkali stable (1 mM) (Roche)

T4 DNA ligase, 10 U/microlitre and 10x reaction buffer (New England Biolabs)

Forward PCR primers (10 �M) containing add-on restriction site "A"

Reverse PCR primers (10 �M) containing add-on restriction site "B"

Template DNA (10 pg/microlitre)


@hnode Synthesis and Purification of labelled DNA fragment

Set up two separate amplification reactions to label the 1-kb templates:
							




<pre>

DNA template                     5.0 �l                          5.0 �l

Forward PCR Primer	  	 7.5 �l				 7.5 �l

Reverse PCR Primer		 7.5 �l				 7.5 �l

dNTP mix			20.0 �l				20.0 �l

Biotin-16-dUTP			 5.0 �l

Digoxigenin-11-dUTP						 5.0 �l

10x reaction buffer		25.0 �l				25.0 �l

HiFi DNA Polymerase		 2.5 �l				 2.5 �l

Water				165  �l				165  �l
<pre>
Dispense each reaction into 50 �l aliquots, place in a thermal cycler and 
amplify each of the labeled products according to the generic PCR program:

Step number   	Denaturation    	Annealing	    	Polymerization 	
1		1 min @ 94 C			
2�31		1 min @ 94 C     	30 s at 60 C		1 min at 68 C	
Last	  				      			1 min at 68 C



Purify each of the amplified products using the QIAquick PCR Purification kit or phenol extraction followed by DNA precipitation.

Digest the product of the biotin-labeling reaction with restriction enzyme "A" and the product of the digoxigenin-labeling reaction 
with restriction enzyme "B" at 37 C for 1 h. Inactivate the enzymes by heating at 65 C for 20 min or phenol extraction followed by DNA precipitation.

Purify digestion products of the two reactions by electrophoresis through a 0.8% agarose gel containing 0.2 �g/ml ethidium bromide. Extract each of the products from the gel and purify the products using the QIAquick Gel Extraction kit. Typical yields are on the order of 2 ?g (~1 pmol). Alternatively,
digestion products can be purified using spin-column size-exclusion chromatography (GE Healthcare SHR-400 columns).  This step is required
to remove the small DNA fragment released by the restriction enzyme digestion of the PCR product.

Quantify the DNA, and adjust concentration to ~200 nM. Store the DNA at -80 C in 5 �l aliquots to retard degradation of the biotin and digoxigenin residues.  

@hnode Synthesis and Purification of a 2-kb target DNA fragment

Set up an amplification reaction as follows [note that the forward
and reverse PCR primers are not the same as for the previous reactions, although the forward primer should
contain add-on restriction site "A" and the reverse primer should contain add-on restriction site "B":

Reagents			

DNA template 			5.0 �l
Forward PCR Primer		7.5 �l
Reverse PCR Primer		7.5 �l
dNTP mix			20.0 �l
10x reaction buffer		25.0 �l
HiFi DNA Polymerase		2.5 �l
Water				165  �l

Dispense into 50 �l aliquots, place in a thermal cycler and amplify according to the generic PCR program:

Step number   	Denaturation    	Annealing	    	Polymerization 	
1		1 min @ 94 C			
2�31		1 min @ 94 C     	30 s at 60 C		4 min at 68 C	
Last	  				      			7 min at 68 C

[Note that the extension time and temperature are appropriate for amplification of a 4-kb DNA fragment of average GC content]

Purify each of the amplified products using the QIAquick PCR Purification kit or phenol extraction followed by DNA precipitation.

Digest the product of the biotin-labeling reaction with restriction enzyme "A" and the product of the digoxigenin-labeling reaction 
with restriction enzyme "B" at 37 C for 1 h. Inactivate the enzymes by heating at 65 C for 20 min or phenol extraction followed by DNA precipitation.

Purify digestion products of the two reactions by electrophoresis through a 0.8% agarose gel containing 0.2 �g/ml ethidium bromide. Extract each of the products from the gel and purify the products using the QIAquick Gel Extraction kit. Typical yields are on the order of 2 ?g (~1 pmol). Alternatively,
digestion products can be purified using spin-column size-exclusion chromatography (GE Healthcare SHR-400 columns).  This step is required
to remove the small DNA fragment released by the restriction enzyme digestion of the PCR product.

Quantify the DNA, and adjust concentration to ~50 nM. Store the DNA at -80 C in 5 �l aliquots to retard degradation of labels or nicking of the DNA.  These
aliquots can typically be stored for one to two months.  

@hnode Ligation of biotin-labelled and digoxigenin-labelled fragments to target DNA

Set up a ligation reaction by mixing the following:
			
Target DNA fragment (50 nM) 		1 �l
Digoxigenin-labeled fragment (200 nM) 	2.5 �l
1 kb biotin-labeled fragment (200 nM) 	2.5 �l
10x T4 DNA ligase buffer		1 �l
T4 DNA Ligase (10 U/�l)			1 �l
Water					2 �l

Incubate the reaction at 25 ?C for 3 h.  Inactivate the ligase by adding EDTA to 10 mM and incubating at 65 C for 10 min, or by
incubating at 65 C for 20 min. Dispense the ligation products into 2-�l aliquots and store at �80 C.

For binding to magnetic beads, dilute the DNA construct to ~50 pM with 10 mM Tris (pH 8) 10 mM EDTA. The dilute solution can be 
stored at 4 C for a month or so before the proportion of DNAs observed to be supercoiled under the microscope decays by about half. 
Note that this decay can be attributable not only to random nicking of the DNA but also to progressive degradation of biotin 
or digoxigenin labels.  Optimally the dilute solution should be stored at 4oC for only a week or two, and remade frequently from
frozen aliquoted stock.  Even so, it is a good idea to reprepare labelled DNA fragments every month or so. A good preparation 
of DNA should display ~50% of supercoilable molecules.


@heading
Anchoring of DNA to magnetic beads
<pre>
Reagents

DNA Ligation product (50 pM) 
Magnetic beads (10 mg/ml)
Freshly-prepared BSA (Roche, 10 mg/ml in water)
Tween-20 (ROche, 10%)
Phosphate buffered saline (PBS), 1x
Standard Buffer (SB) (10 mM Potassium phosphate pH 8 supplemented with 0.1 mg/ml BSA and 0.1% Tween-20)

Wash 5 �l of 1 �m magnetic beads in 200 �l PBS supplemented with 1 mg/ml BSA, and resuspend beads in 5 �l PBS supplemented with 1 mg/ml BSA.

Deposit a 0.5-�l drop of the DNA ligation product (50 pM) at the bottom of a small microfuge tube.

Load a wide-bore pipette tip with 90 �l of SB.

Deposit 5 �l of beads onto the drop of DNA and immediately dilute the reaction with the 90 �l SB. 
This should be done by gently depositing the SB onto the bead plus DNA solution.

Resuspend the beads to homogeneity by gently spinning the tube between thumb and forefinger or by 
tipping the tube upside down (without causing the liquid to drop) and allowing beads to sediment 
about 1/3 the height of the liquid column and repeating 4 or 5 times.

@heading
Anchoring of the DNA to the flow cell
<pre>
Before injecting bead-DNA mixture into the flow cell, move the magnets at least 2 cm away from the flow cell.

Inject 10 �l of the bead-DNA mixture into one of the plastic reservoirs connected to the capillary tube.

Inject another 20�50 �l of SB into the reservoir to ensure the majority of beads enter the capillary and are 
evenly distributed along the length of the capillary.  Beads should be seen under the microscope.

Allow the magnetic beads to sediment and incubate for 10-15 minutes. The majority of magnetic beads should 
be seen to move about on the surface and not appear immobile.

Establish a gentle flow of SB (150�200 �l/min, typically for 20 min) to remove unbound beads from the surface. 
(Buffer is injected into the input reservoir using a motorized syringe pump and the output reservoir is drained by gravity feed.) 

Approximately every 2 min slowly pass a rod with a small (2-mm-diameter) magnet at the end just over the capillary, to lift 
unbound beads off the surface and into the flow field.

Turn off flow and move the magnets as close as possible to the sample without coming into contact, 
causing DNA molecules to extend away from the surface.  Note the current rotational position of the 
magnets as the "magnet initial rotational state".

