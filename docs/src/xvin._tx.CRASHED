@#			
@# This is the source for the Xvin docs, in a weird custom format.
@# Read makedoc.c for a description of what is going on...
@#
@# If you want to put everything in one big HTML file rather than splitting
@# it into sections, remove this 'multiplefiles' statement:
@#
@multiplefiles
@#
@# Uncomment the following statement to output css clean html files.
@#
@#ignore_css
@#
@external-css=allegro.css
@document_title=Xv Manual
@html_footer=Back to contents
@rtfh=Xvin - a graphic and image library
@manh="version 0.2" "Xvin" "Xvin manual"
@mans=#include <xvin.h>
@$\input texinfo
@$@setfilename xvin.inf
@$@settitle Xvin Manual
@$@setchapternewpage odd
@$@paragraphindent 0
@$@setchapternewpage off
@# This should remain commented at the moment (dvi output) @$@finalout
@$
@$@ifinfo
@$@direntry
@$* Xvin: A graphical program to display plots and images.
@$@end direntry
@$This is the Info version of the Xvin manual
@$
@$By Vincent Croquette and Nicolas Garnier
@$@end ifinfo
@$
@$@node Top, , (dir), (dir)
@titlepage
@<pre>
@!indent



		A graphical program to display plots and images.

	     By Vincent Croquette and Nicolas Garnier, Nov, 2003.

		See the AUTHORS file for a
	       complete list of contributors.
@indent
@</pre>



#include &ltstd_disclaimer.h&gt
<blockquote class="text"><i>
   "We do not accept responsibility for any effects, adverse or otherwise, 
    that this code may have on you, your computer, your sanity, your dog, 
    and anything else that you can think of. Use it at your own risk."
</i></blockquote>
@!titlepage


@!text
@heading
Contents

@contents



@text
@heading
Using Xvin

Xvin is a general program to display plots and images. It is oriented to
present physical results. The goal is to provide a relatively simple
graphical tools allowing to handle scientific data and to perform common
numerical treatements. The emphasis is made in its ability to manipulate
large numbers of points (several millions) and to be has fast as possible.
The interface is on the other hand rather simplistic.



Xvin is also made to be easily expandable using plug-ins written in C and
loaded dynamically. Whithin Xvin you can automatically generate a project
directory MyProject holding a set of files: MyProject.h, MyProject.c the 
associated makefile which serves as a canvas to write simple applications
adding a new function attached to a menu button. 


@@int @xvin_main(int argc, char** argv);
   Start xvin passing the command line arguments




@@typedef struct plot_label @p_l;
@xref create_plot_label, duplicate_plot_label, free_plot_label
<codeblock>
	int type;		    type of label : in absolute or user defined coordinate
	float xla, yla;	    the x, y position of the label 
	char *text;         the string containing the text of the label
	struct box *b;      its internal representation
<endblock>
    Type may be one of those
<textblock>
	ABS_COORD	        0
 	USR_COORD		    1
 	VERT_LABEL_USR	    2
 	VERT_LABEL_ABS	    3
 	WHITE_LABEL		    16
 	OVER_WHITE 		    32
<endblock>





@@typedef struct data_set @d_s
@xref build_data_set, build_adjust_data_set, duplicate_data_set, free_data_set, add_new_point_to_ds
<codeblock>
	int nx, ny, type;		  the number of x and y data 
	int mx, my;                     the size allocated for x and y 	
	float *xd, *yd;    	   pointers to the x and y data 
	char *symb;			       the symbol to plot describe as a string
	int m;				       a flag defining the line style 
	int color;			       the drawing color 
	unsigned long time;	       the time and date of creation 
	char *source;              a string defining from where the data comes from
	char *history;             a string defining the previous treatement
	char *treatement;          a string defining the last treatement
	char **special;            a pointer to an array of user specific string
	int n_special, m_special;  the number of them and the size of the array
	struct hook use;		   a structure allowing to attach extra data
<endblock>


@@ @get_pl_x(pl)
@xref set_pl_x
   A macro grabbing the X coordinate of a plot label

@@ @set_pl_x(pl,x)
@xref get_pl_x
   A macro setting the X coordinate of a plot label

@@ @get_pl_y(pl)
@xref set_pl_y
   A macro grabbing the Y coordinate of a plot label

@@ @set_pl_y(pl,y)
@xref get_pl_y
   A macro setting the y coordinate of a plot label

@@ @get_pl_text(pl)
@xref get_pl_x, get_pl_y
   A macro grabbing the text string of a plot label

@@p_l* @create_plot_label(int type, float x, float y, char *format,...);
@xref free_plot_label, duplicate_plot_label
   Create a plot label by setting variables and allocating the text string using
   a printf format.
<codeblock>
      pl = create_plot_label (USR_COORD, 1, 2, "Nice point at %g",val);
<endblock>

@@p_l* @duplicate_plot_label(p_l *pls);
@xref free_plot_label, create_plot_label
   Duplicate a plot label 

@@int @free_plot_label(p_l *pl);
@xref duplicate_plot_label, create_plot_label
   Free both the text string and the structure.
       

@@d_s* @build_data_set(int nx, int ny);
@xref free_data_set, build_adjust_data_set
    Create the data set structure and allocate the x and y array to the 
    corresponding size.

@@d_s* @build_adjust_data_set(d_s *ds, int nx, int ny);
@xref free_data_set, build_data_set
    If the data set passed is null this function call build_data_set, otherwise
    it allow to reallocate the arrays to the new size.
    
@@d_s* @duplicate_data_set(d_s *src, d_s *dest);
@xref free_data_set
    Copy a data set and all its features
    
@@int @free_data_set(d_s *ds);
@xref build_data_set, build_adjust_data_set
    Tree everything allocated in the structue as well as the structure itself.
    If the function ds_free_use has been defined, this function is called.
    
@@int @add_new_point_to_ds(d_s *ds, float x, float y);
@xref build_data_set, build_adjust_data_set
    This function add a new point to a data set as its last points, it 
    automatically reallocate data arrays if needed.
    

@@	@get_ds_nx(ds)		
@xref d_s
	macro giving the number of points in x
	
@@	@get_ds_ny(ds)		
@xref d_s
	macro giving the number of points in y	
	

@@ @set_ds_x_point(ds,x,i)	
@xref d_s, get_ds_x_point 
	macro setting the x value of the point of index i, no check is made on array boundary  

@@ @set_ds_y_point(ds,y,i)	
@xref d_s, get_ds_y_point
	macro setting the y value of the point of index i, no check is made on array boundary  
  
@@ @set_ds_point(ds,x,y,i)	
@xref d_s
	macro setting both the x and y value of the point of index i, no check is made on array boundary   

@@ @get_ds_x_point(ds,i)	
@xref d_s, set_ds_x_point 
	macro getting the x value of the point of index i, no check is made on array boundary 

@@ @get_ds_y_point(ds,i)	
@xref d_s, set_ds_y_point
	macro getting the y value of the point of index i, no check is made on array boundary 




@headingnocontent
Conclusion

All good things must come to an end. Writing documentation is not a good 
thing, though, and that means it goes on for ever. There is always something 
I've forgotten to explain, or some essential detail I've left out, but for 
now you will have to make do with this. Feel free to ask if you can't figure 
something out.

Enjoy. I hope you find some of this stuff useful.


By Shawn Hargreaves.

<link>http://alleg.sourceforge.net/</a>



@!html
@!text
@$@ifinfo
@headingnocontent
Index

@index
@$@end ifinfo

@$@contents
@$@bye

@html
@text
