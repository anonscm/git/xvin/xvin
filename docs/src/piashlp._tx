@#	    	
@# This is the source for the Pico docs, in a weird custom format.
@# Read makedoc.c for a description of what is going on...
@#
@# If you want to put everything in one big HTML file rather than splitting
@# it into sections, remove this 'multiplefiles' statement:
@#
@multiplefiles
@#
@# Uncomment the following statement to output css clean html files.
@#
@#ignore_css
@#
@external-css=allegro.css
@document_title=PlayItAgainSam Manual
@html_footer=Back to contents
@rtfh=Xvin - The software to analyze data recorded with magnetic tweezers  
@manh="version 0.1" "PlayItAgainSam" "PlayItAgainSam manual"
@mans=#include <xvin.h>
@$\input texinfo 
@$@setfilename pias.inf
@$@settitle PlayItAgainSam Manual
@$@setchapternewpage odd
@$@paragraphindent 0
@$@setchapternewpage off
@# This should remain commented at the moment (dvi output) @$@finalout
@$
@$@ifinfo
@$@direntry
@$* PIAS - A signal viewer software for magnetic tweezers 
@$@end direntry
@$This is the Info version of the PlayItAgainSam manual
@$
@$By Vincent Croquette
@$@end ifinfo
@$
@$@node Top, , (dir), (dir)
@titlepage
@<pre>
@!indent



        A dedicate software to read and analyze TRK files
	produced by Picoxxx tracking software package for magnetic tweezers 

         By Vincent Croquette 

        See the AUTHORS file for a
           complete list of contributors.
@indent
@</pre>


@!titlepage


@!text
@heading
Contents

@contents



@text
@heading
PlayItAgainSam purpose

PlayItAgainSam is program used to read and analyze the data recorded
in real-time by the magnetic tweezers software saved in the TRK
format. PlayItAgainSam will load one or several TRK files and then
display their content in different manner. PlayItAgainSam also allow
the user to analyze these data recovering specific information on the
molecules studied in magnetic tweezers.

Before explaining the program functions, let us explain what the TRK
file format contains. Each time you record data in a TRK file using
either the simulation software Pico.exe or the experiment acquisition
software Picoueye.exe (for uEye cameras) and PicoJai.exe for (Jai
cameras), the software dumps exhaustive information in the TRK file
for latter treatment. The TRK file is composed of two parts: a header
part which consists of all the information on the experiment that you
were running when you start recording and the real data which
correspond to the beads coordinates at each frame. The data section
grows as long as the acquisition is running. The header part contains
a picture of the entire field of view taken at the beginning of the
acquisition, a copy of all the calibration images of all the beads and
a copy of the configuration file with the properties of the
instrument. The second data section contains all the information
concerning the experimental conditions in time: the position of the
magnets, the value that they were asked to move to their actual
position at each frame, the same data for the rotation position and
the focus position plus the status of the experiment and all the
timing information of each frame. For each bead, the trk file record
the x, y and z coordinates plus tracking quality information allowing
to determine if the z and x-y tracking were done properly at each
frame. The user has the option to record more information at each
frame, it is for instance possible to record a small image around each
bead (this significantly increases the size of the recording) or just
profiles of x or y or radial and orthoradial profiles. PlayItAgainSam
will read, display and dump to other format all these information. In
its normal use, TRK files are relatively compact, their size being of
the order of a few hundreds of megabyte hour long recording. One
should keep in mind that the time series recorded in TRK files may be
pretty large exceeding one million of points easily.

TRK format is mostly binary but uses ascii information in the header
being nearly readable and is described in annex.


@heading
Reading a TRK file 

The PIAS program should have been associated to TRK files so that
these files will automatically be open by PIAS. If you launch PIAS by
clicking on a TRK file or if you start first PIAS and then load a TRK
file (File menu => Load TRK file), the program will analyze the
header, load the information in memory and open two projects, one of
image type containing the starting field of view image and a plot
project displaying the time evolution of one bead. TRK files can be
pretty big and loading will then take some times, a message in the
title bar lets you now the progression status.

In principle the TRK files is self-contained and PIAS will recover all
information and only ask you what is your preferences for the scope
display. However, inconsistency may be detected in your TRK file and
PIAS will then ask you for option. On windows, PIAS is running in 32
bits which limits the size of the file that it can load safely, if you
load a TRK file bigger than 300Mb, PIAS will display an alert window
proposing different options to handle big recording. The message will
display the size of the file, the number of video frames in the TRK
file and the number of beads recorded. You can safely handle load 1Gb
file thus if your file is smaller than this limit, click on Ok, you
will load all frames of all beads. If your file exceeds the 1Gb limit,
you need to load only a subset of your data, you can choose to load a
subset of frames or a subset of beads at your convenience.


@heading
The scope window

This is the most convenient way to look at your data, it looks like this: 

@html_text_substitution=image_1|<img src="./pias-0001.png" height="897" width="1448" alt="PIAS scope" align="center"> 

image_1

This plot displays the Z signal of bead 22 versus time. You have the
possibility to visualize different beads and different time window by
using keyboard short-cuts.

The <b>arrows keys</b> allow to explore the time domain: <b>right and
left keys</b> move along time keeping the time window unchanged,
pressing the <b>SHIFT key</b> as you use the right and left arrow move
the time windows by smaller amount. The <b>up and down arows</b> will
zoom in and out on your signal. You can zoom in to observe individual
points of the signal. You can zoom out on a wide range, the window
will display up to 262144 points per traces representing typically one
hour of recording.

To switch from bead to bead use <b>Page Up and Page Down keys</b>, it
is common to have beads presenting no useful information, if you press
the <b>l key</b> you have the ability to declare that the present bead
is actually lost and should not be display anymore. Imagine you see
the signal of bead 2 and decide that it is indeed lost, the next time
you use <b>Page Up and Page Down keys</b> you will jump from bead 1 to
3 or the reverse skipping the lost bead. if you press <b>SHIFT and
Page Up or Page Down</b> you will see all beads even the lost ones
(you can then toggle back a bead switched to lost by pressing <b>l
key</b>.

The plot is composed of several data-sets representing the signal and
either the force or the position of the magnets. This parameter is
represented by a red data-set. Using the "record menu => scope menu"
you can select which experimental parameter you display together your
Z signal. You have the choice between force, zmag and rotation
depending on the experiment that you perform.

Three data-sets represent the Z signal.
@tallbullets
<ul>
<li>
The
data-set 0 represents all data points of the Z signal as long as the
XY tracking is fine<br> (the points of this data set are blue).

@tallbullets
<li> The data-set 1 represents a filtered version of the
Z signal. Single molecule experiments display a lot of Brownian noise,
the filtered data-set try to present good-looking data. <br> The
default filtering done is a non-linear filter that average data when
the signal remains constant and stops averaging when the signal
presents rapid variations. This last property avoids transient
rounding off. This non-linear filter does a pretty good job, its only
drawback is that it has a tendency to display steps in slowly varying
signal.

<li>
The color of the signal informs you on the quality of the
tracking. Although the z tracking is working fine, it is easy to have
this tracking out of range of the calibration image or spurious durst
can degrade the bead image quality. The z tracking algorithm also
provides a quality score of the tracking which is recorded in the
track file. The z racking quality degradation is a sign to consider
during an experiment, the color of the z signal will switch from
yellow (good tracking) to blue on poor tracking quality. A blue signal
is not always a bad signal but it is likely to become bad. Data-set 3
consists of all z points with good tracking quality. When the signal
becomes problematic, the points of this data-set are skip leaving the
blue signal visible.

<li>

Extra data-sets will display further information: when the TRK file
corresponds to a scanning in rotation or in force, phase of constant
signal are recorded to obtain accurate mean position of highly
fluctuating bead, these are averaging phases. One data-sets will
displays the mean position of these phases. Finally, it is possible to
measures position and speed in a Z traces which will appear as events
that we shall discuss later.

</ul>

@@ Changing the scope display

Using the "record menu => scope menu => change scope display" you can modify
the scope appearance.

@html_text_substitution=image_8|<img src="./pias-0008.png" height="450" width="787" alt="PIAS changing scope display" align="center"> 

image_8


@@ Substracting a fixed bead.

It is a good practice during recording is to select a few stuck bead
because they will reflect the drift of your experiment. Subtracting
the fixed bead signal to the z coordinate of an interesting bead is
usually very valuable.


@html_text_substitution=image_5|<img src="./pias-0005.png" height="890" width="1436" alt="PIAS scope signal of a bead" align="center"> 

image_5

Using the "record menu => scope menu => change bead to subtract" you can
select a bead to subtract

@html_text_substitution=image_6|<img src="./pias-0006.png" height="160" width="518" alt="PIAS scope dialog bead to subtract" align="center"> 

image_6


@html_text_substitution=image_7|<img src="./pias-0007.png" height="874" width="1439" alt="PIAS scope signal of a bead minus a fixed one" align="center"> 

image_7

The result as shown here illustrate that most of the drift has been
removed leaving a more stable trace. This process however increase the
noise level of the signal since the noise associated with the fixed
bead is not correlated with the bead of interest and increase the
total noise by a factor square root of 2. To be efficient this process
requires that the fixed bead signal is of good quality all along the
recording which is not always simple.

@@ Viewing numbers

@html_text_substitution=image_2|<img src="./pias-0002.png" height="890" width="1435" alt="PIAS scope display" align="center"> 

image_2

Below the menu bar you find the display parameter bar which reports
the value of the different experimental parameters of your
recording. The displayed values correspond to the frame selected by
the horizontal marker located at the bottom of the plot. You will be
able to read the magnets Z position, the rotation angle, the estimated
force, the focus position. At the end of the bar you find the z
position of the selected bead.

@heading
The image window

Aside a scope window, PIAS allows to recover an image of your field of
view taken when the recording started. To access this image use the
"Project" menu and select the image corresponding to your TRK file

@html_text_substitution=image_3|<img src="./pias-0003.png" height="132" width="1437" alt="PIAS scope" align="center"> 

image_3

Leading to:

@html_text_substitution=image_4|<img src="./pias-0004.jpg" height="887" width="1438" alt="PIAS image" align="center"> 

image_4

@heading
The recording configuration

Since the acquisition software records all the experimental parameters
you normally should not have to use the configuration menu in the
record menu, but you may have forgotten to update a parameter and it
is then possible to alter your TRK configuration so that it reflects
the real experiment. For instance you may have change your magnets and
forget to notify your acquisition software you can modify the magnets
parameters when using PIAS. If you do so, PIAS will create a
correction file in the same directory of your trk file having the same
filename but with a ".cor" extension. For instance, if you open the
trk file Zdet_025.trk again after loading it PIAS will look for a file
Zdet_025.cor, if this file exists the parameters contains in this file
will override those of the initila TRK file.

One critical problem with configuration that may occur is the
following: the acquisition program stores all configuration parameters
in the file PicoTwist.cfg. This file contains important information
such as the microscope calibration etc. This file is update by the
acquisition program that needs to update these parameters. In rare
conditions, it is possible that a software crash occurs while the
program is writing the configuration file erasing completely your
configuration parameters. In principle there is a PicoTwist.bkc file
that contains a copy of your initial configuration that will be copied
automatically in PicoTwist.cfg by the acquisition program that will
alert you of this issue. An alternative is to recover your
configuration file from the last trk recording with the right
configuration. Each trk file contains a copy of the PicoTwist.cfg
taken at the recording time. You can extract this configuration file
by using the following menu: "record menu => config menu => extract
config file from trk". The default name of this configuration is the
one of the trk file.


@heading
Measuring enzyme rates on trk

@hnode Selecting events from your trk files

Using the scope you can measure signal properties using the "select
events menu". Selecting events is done using the mouse to specify part
of the signal that you want to analyze. You have two tools that are
available for this purpose: "Multi segments line" and "Multi dash
line", each event is a series of segments following your signal that
you define by clicking the vertexes of these segments. In the
multi-line tool, all the segments are connected directly, in the dash
option the segments are separated. Let us show a simple example:

       
@html_text_substitution=image_9|<img src="./pias-0009.png" height="434" width="387" alt="PIAS selecting a simple segment" align="center"> 

image_9

You select the segment by left clicking on the vertex points, a right
click finises the segment series. You should focus on selecting the x
position of the vertex accurately, the software will adjust the y
coordinate to fit the bead position. The slope of your draw segment is
important, the software will recognize different kind of segments
according to their slope.

@tallbullets Horizontal segments are used to measure the mean position of a signal, we identify them as N.

@tallbullets rising segments as in the above images correspond typically to unzipping helicase signals identify by U.

@tallbullets decreasing segments correspond typically to translocation or a zipping helicase signals identify by U.

@tallbullets vertical signal will occur when enzymes detach from DNA for instance, this may be either H or R segments 

@html_text_substitution=image_seg|<img src="./pias-0010.png" height="434" width="470" alt="PIAS selecting a simple segment" align="center"> 

image_seg

@heading
Computing force curve, force scan or rotation scan

To characterize DNA elasticity three classical tests are made:   

@@ Force scan

@@ Rotation scan

@@ Force curves


Force curves is a basic function that will produce an accurate
force/extension curve of a DNA molecules, this will confirm that you
are indeed pulling on a single DNA molecule (provide the persistence
length is correct) and it is used to calibrate forces and magnets.

The force applied by the magnets depends on two parameters: the
magnets which are characterized by a set of parameters and the bead
which as a certain magnetization. When using MyOne bead, the
magnetization of the different beads is relatively homogenous but not
perfectly identical for all beads. Your apparatus and TRK files show
"estimated force" because of this variability. The estimated force is
the typical force that your magnets apply to a bead but the real one
might be different by typically 20%. If you measure the force using
the Brownian motion, you actually measure the real force applied by
the magnets. The estimated force and the real force are basically
proportional with a factor close to one. Depending on your goal you
may measure the real force applied to your beads at one or two force
and determine the factor Fr/Fe for each bead (all measured at the same
time) or you can compute a complete force curve to calibrate your
magnets for instance.

Measuring accurately the force is not difficult but many factors can
produce incorrect results and you must be aware of them to get good
results. The principle of the Force measurement by Brownian consists
in recording the horizontal Brownian fluctuations of the beads
[(x-[x])2] = [dx2] or [(y-[y])2] = [dy2] for some time will pulling at
constant force vertically. The stronger you pull, the faster and the
weaker the fluctuations following the relation F = kBT.[Z]/[dx2},
where [z] is the mean extension of the molecule and kBT is the
Boltzmann constant times the temperature (= 4.1 pN.nm at room
temperature). Your device will measure [z] and [dx2] so that you can
deduce the force directly. Error can occur both in [z] and [dx2] for
different reasons, [z] is simple to measure but might be off by an
offset, when you take a calibration image, it is easy to get a small
offset in the process, the reason is that if you suppress the magnetic
force to measure the position of the bead on the bottom of the cell,
the small bead still fluctuates and its mean position taken as a
reference is easily corrupted by this effect. [dx2] is also easy to
measure with your video camera but when you measure strong forces, the
fluctuations become very fast and sometimes too fast for the camera
which filter them leading to [dx2] smaller than they are really and
leading to overestimated measures. To guarantee the best result, PIAS
displays a lot of control curves which help to double check the
result.

After loading a TRK file corresponding to a Force curve, select
"record menu => redraw force curve" and accept the default settings
for your options.


@html_text_substitution=image_A|<img src="./pias-0011.png" height="894" width="1440" alt="PIAS force curve menu" align="center"> 

image_A


@html_text_substitution=image_B|<img src="./pias-0012.png" height="141" width="379" alt="PIAS force curve menu warning" align="center"> 

image_B

You are likely to see some warning like this one which tells you that
your force curve was stop before all points were measure, if you click
on CANCEL this message should not come back. The treatment will take
some time, the progression is visible in the title bar.

@html_text_substitution=image_C|<img src="./pias-0013.png" height="881" width="1436" alt="PIAS force curve project selection" align="center"> 

image_C


You have as many project as there were beads measured, each project
contains a large number of information: the first plots correspond at
each measuring points and fit to determine <dx2>, in particular the
frequency cutoff of the Brownian motion is computed. To insure good
results, this cutoff frequency should b well below the camera sampling
frequency.
       
@html_text_substitution=image_D|<img src="./pias-0016.png" height="882" width="1434" alt="PIAS single point measurement" align="center"> 

image_D

This is an example of the adjustment made for one data point made at
Zmag = -0.496 mm, the plot displays the x signal in yellow, the y in
green and the z in red during the recording period. Labels give you
the kBt/<dw2> which is the stiffness of the system. kx and ky are in
the range of 1.7 10^{-6} N/m, corresponding to a high force (~10pN),
the cutoff frequency is close to 20hz which is fine here since the
camera is running at 20Hz. The program also compute the bead radius by
two different method which mostly agree with the expected value of 0.5
microns (beads are often slightly bigger than 1 micron in
diameter). The Z fluctuations are stiffer and the cutoff frequency is
too high for the camera, this is not a real problem since we do not
use the fluctuations in z but just the mean value to compute the
force. The bead radius deduced from the z fluctuations is completely
useless.


@html_text_substitution=image_E|<img src="./pias-0014.png" height="882" width="1431" alt="PIAS single point measurement" align="center"> 

image_E

Fitting the force curve to the Worm Like Chain model.

@html_text_substitution=image_F|<img src="./pias-0015.png" height="879" width="1428" alt="PIAS single point measurement" align="center"> 

image_F



@!html
@!text
@$@ifinfo
@headingnocontent
Index

@index
@$@end ifinfo

@$@contents
@$@bye

@html
@text
