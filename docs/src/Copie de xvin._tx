@#			
@# This is the source for the Xvin docs, in a weird custom format.
@# Read makedoc.c for a description of what is going on...
@#
@# If you want to put everything in one big HTML file rather than splitting
@# it into sections, remove this 'multiplefiles' statement:
@#
@multiplefiles
@#
@# Uncomment the following statement to output css clean html files.
@#
@#ignore_css
@#
@external-css=allegro.css
@document_title=Xv Manual
@html_footer=Back to contents
@rtfh=Xvin - a graphic and image library
@manh="version 0.2" "Xvin" "Xvin manual"
@mans=#include <xvin.h>
@$\input texinfo
@$@setfilename xvin.inf
@$@settitle Xvin Manual
@$@setchapternewpage odd
@$@paragraphindent 0
@$@setchapternewpage off
@# This should remain commented at the moment (dvi output) @$@finalout
@$
@$@ifinfo
@$@direntry
@$* Xvin: A graphical program to display plots and images.
@$@end direntry
@$This is the Info version of the Xvin manual
@$
@$By Vincent Croquette and Nicolas Garnier
@$@end ifinfo
@$
@$@node Top, , (dir), (dir)
@titlepage
@<pre>
@!indent



		A graphical program to display plots and images.

	     By Vincent Croquette and Nicolas Garnier, Nov, 2003.

		See the AUTHORS file for a
	       complete list of contributors.
@indent
@</pre>



#include &ltstd_disclaimer.h&gt
<blockquote class="text"><i>
   "We do not accept responsibility for any effects, adverse or otherwise, 
    that this code may have on you, your computer, your sanity, your dog, 
    and anything else that you can think of. Use it at your own risk."
</i></blockquote>
@!titlepage


@!text
@heading
Contents

@contents



@text
@heading
Using Xvin

Xvin is a general program to display plots and images. It is oriented to
present physical results. The goal is to provide a relatively simple
graphical tools allowing to handle scientific data and to perform common
numerical treatements. The emphasis is made in its ability to manipulate
large numbers of points (several millions) and to be has fast as possible.
The interface is on the other hand rather simplistic.



Xvin is also made to be easily expandable using plug-ins written in C and
loaded dynamically. Whithin Xvin you can automatically generate a project
directory MyProject holding a set of files: MyProject.h, MyProject.c the 
associated makefile which serves as a canvas to write simple applications
adding a new function attached to a menu button. 


@@int @xvin_main(int argc, char** argv);
   Start xvin passing the command line arguments




@@typedef struct plot_label @p_l;
@xref create_plot_label, duplicate_plot_label, free_plot_label
<codeblock>
	int type;		    type of label : in absolute or user defined coordinate
	float xla, yla;	    the x, y position of the label 
	char *text;         the string containing the text of the label
	struct box *b;      its internal representation
<endblock>
    Type may be one of those
<textblock>
	ABS_COORD	        0
 	USR_COORD		    1
 	VERT_LABEL_USR	    2
 	VERT_LABEL_ABS	    3
 	WHITE_LABEL		    16
 	OVER_WHITE 		    32
<endblock>





@@typedef struct data_set @d_s
@xref build_data_set, build_adjust_data_set, duplicate_data_set, free_data_set, add_new_point_to_ds
<codeblock>
	int nx, ny, type;		  the number of x and y data 
	int mx, my;                     the size allocated for x and y 	
	float *xd, *yd;    	   pointers to the x and y data 
	char *symb;			       the symbol to plot describe as a string
	int m;				       a flag defining the line style 
	int color;			       the drawing color 
	unsigned long time;	       the time and date of creation 
	char *source;              a string defining from where the data comes from
	char *history;             a string defining the previous treatement
	char *treatement;          a string defining the last treatement
	char **special;            a pointer to an array of user specific string
	int n_special, m_special;  the number of them and the size of the array
	struct hook use;		   a structure allowing to attach extra data
<endblock>


@@ @get_pl_x(pl)
@xref set_pl_x
   A macro grabbing the X coordinate of a plot label

@@ @set_pl_x(pl,x)
@xref get_pl_x
   A macro setting the X coordinate of a plot label

@@ @get_pl_y(pl)
@xref set_pl_y
   A macro grabbing the Y coordinate of a plot label

@@ @set_pl_y(pl,y)
@xref get_pl_y
   A macro setting the y coordinate of a plot label

@@ @get_pl_text(pl)
@xref get_pl_x, get_pl_y
   A macro grabbing the text string of a plot label

@@p_l* @create_plot_label(int type, float x, float y, char *format,...);
@xref free_plot_label, duplicate_plot_label
   Create a plot label by setting variables and allocating the text string using
   a printf format.
<codeblock>
      pl = create_plot_label (USR_COORD, 1, 2, "Nice point at %g",val);
<endblock>

@@p_l* @duplicate_plot_label(p_l *pls);
@xref free_plot_label, create_plot_label
   Duplicate a plot label 

@@int @free_plot_label(p_l *pl);
@xref duplicate_plot_label, create_plot_label
   Free both the text string and the structure.
       

@@d_s* @build_data_set(int nx, int ny);
@xref free_data_set, build_adjust_data_set
    Create the data set structure and allocate the x and y array to the 
    corresponding size.

@@d_s* @build_adjust_data_set(d_s *ds, int nx, int ny);
@xref free_data_set, build_data_set
    If the data set passed is null this function call build_data_set, otherwise
    it allow to reallocate the arrays to the new size.
    
@@d_s* @duplicate_data_set(d_s *src, d_s *dest);
@xref free_data_set
    Copy a data set and all its features
    
@@int @free_data_set(d_s *ds);
@xref build_data_set, build_adjust_data_set
    Tree everything allocated in the structue as well as the structure itself.
    If the function ds_free_use has been defined, this function is called.
    
@@int @add_new_point_to_ds(d_s *ds, float x, float y);
@xref build_data_set, build_adjust_data_set
    This function add a new point to a data set as its last points, it 
    automatically reallocate data arrays if needed.
    

@@	@get_ds_nx(ds)		
@xref d_s
	macro giving the number of points in x
	
@@	@get_ds_ny(ds)		
@xref d_s
	macro giving the number of points in y	
	
@@ @set_ds_x_point(ds,x,i)	
@xref d_s, get_ds_x_point
	macro setting the x value of the point of index i, no check is made on 
	array boundary.  

@@ @set_ds_y_point(ds,y,i)	 
@xref d_s, get_ds_y_point
	macro setting the y value of the point of index i, no check is made on 
	array boundary.  
  
@@ @set_ds_point(ds,x,y,i)	
@xref d_s
	macro setting both the x and y value of the point of index i, no check 
	is made on array boundary   

@@ @get_ds_x_point(ds,i)	
@xref d_s, set_ds_x_point
	macro getting the x value of the point of index i, no check is made on 
	array boundary 

@@ @get_ds_y_point(ds,i)	
@xref d_s, set_ds_y_point
	macro getting the y value of the point of index i, no check is made on 
	array boundary 

@@float* @get_ds_x_ptr(d_s *ds);
@xref d_s
	Macro use to obtain the pointer to the float array of X coordinate.

@@float* @get_ds_y_ptr(d_s *ds);
@xref d_s
	Macro use to obtain the pointer to the float array of Y coordinate.
						
@@int @set_ds_dash_line(d_s *ds);
@xref d_s, set_ds_dot_line, set_ds_plain_line
	This function specify the data set properties.

@@int @set_ds_dot_line(d_s *ds);
@xref d_s, set_ds_dash_line, set_ds_plain_line
	This function specify the data set properties.
	
@@int @set_ds_plain_line(d_s *ds);
@xref d_s, set_ds_dot_line, set_ds_dash_line
	This function specify the data set properties.
	
@@int @set_ds_line_color(d_s *ds, int color);
@xref d_s, get_ds_line_color
	This function specify the color of the data set.
		
@@ @get_ds_line_color(ds)	
@xref d_s, set_ds_line_color
	This macro retrieve the color of the data set.

@@int @set_ds_point_symbol(d_s *ds, char *symb);
@xref d_s, get_ds_point_symbol
	This function specify the symbol to be drawn at each point.
<codeblock>
      set_ds_point_symbol(ds, "\\pt6\\gamma");
<endblock>
	place the greek symbol in six point size at each plotted point.
	
@@ @get_ds_point_symbol(ds)	
@xref d_s, set_ds_point_symbol
	Grap the pointer to the string describing the plot symbol.
	
@@int @inherit_from_ds_to_ds(d_s *dest, d_s *src);
@xref d_s
	Use to transfert all the property of one data set to a second one which
	correspond to a treatement done to the first one.
	
@@int @set_ds_source(d_s *ds, char *format, ...);
@xref d_s, get_ds_source
	Printf style function allowing to specify the source of the data set. The 
	string is automatically allocated to the required size.
<codeblock>
      set_ds_source(ds, "Original data set having number %d", n_data);
<endblock>	

@@ @get_ds_source(ds)	((ds) == NULL) ? NULL : (ds)->source
@xref d_s, set_ds_source
	Grap the pointer to the string describing the source of the data set.
	
@@int @set_ds_history (d_s *ds, char *format, ...);
@xref d_s, get_ds_history
	Printf style function allowing to specify the history of the data set. The 
	string is automatically allocated to the required size.
	
@@ @get_ds_history(ds)	((ds) == NULL) ? NULL : (ds)->history
@xref d_s, set_ds_history
	Grap the pointer to the string describing the history of the data set.

@@int @set_ds_treatement (d_s *ds, char *format, ...);
@xref d_s, get_ds_treatement
	Printf style function allowing to specify the treatement of the data set. The 
	string is automatically allocated to the required size.
	
@@ @get_ds_treatement(ds)	((ds) == NULL) ? NULL : (ds)->treatement
@xref d_s, set_ds_treatement
	Grap the pointer to the string describing the treatement of the data set.

@@int @add_to_data_set(d_s *ds, int type, void *stuff);
@xref d_s, remove_from_data_set
	General function used to attach a new thing to a data set. This may be a 
	special feature.
	
@@int @remove_from_data_set (d_s *ds, int type, void *stuff);
@xref d_s, add_to_data_set
	General function used to detach and free something to a data set. This 
	may be a 	special feature.
	
@@int @remove_point_from_data_set(d_s *src, int n_one);
@xref d_s
	

@@int @ds_special_2_use (d_s *ds);
@xref d_s, ds_use_2_special, ds_free_use, ds_duplicate_use

@@int @ds_use_2_special (d_s *ds);
@xref d_s, ds_special_2_use, ds_free_use, ds_duplicate_use

@@int @ds_free_use(d_s *ds);
@xref d_s, ds_use_2_special, ds_special_2_use, ds_duplicate_use

@@int @ds_duplicate_use (d_s *dsd, d_s *dss);
@xref d_s, ds_use_2_special, ds_free_use, ds_special_2_use


@heading
Units

Data in plots or images have one or several units which are define by a
structure specifying the type of unit and the affine transform which turn
the data numbers in physicals measure. A typical situation is for instance
to the data coming from an AD converter which are integer values ranging from
0 to 65535 for



@@typedef struct unit_set @un_s;
@xref build_unit_set, free_unit_set, duplicate_unit_set
<codeblock>
	int     type, sub_type, axis;	identifier 
	float   ax, dx;	                the offset and increment 
	char    decade;	                the decade value
	char    mode;                   log or a^2 
	char    *name;	                the text of it 
<endblock>

@@	@get_unit_type(un)		
@xref un_s, set_unit_type
	Macro allowing to acces the unit set type;

@@	@set_unit_type(un, type)	
@xref un_s, get_unit_type
	Macro defining the unit set type which may be one of the value below;
		

<codeblock>
	 	IS_VOLT			
	 	IS_AMPERE		
	 	IS_METER		
	 	IS_NEWTON		
	 	IS_SECOND		
	 	IS_GRAMME		
	 	IS_GAUSS		
	 	IS_OHM			
	 	IS_RAW_U		 		
<endblock>


@@	@get_unit_sub_type(un)		
@xref un_s, set_unit_sub_type
	Macro allowing to acces the unit set subtype;

@@	@set_unit_sub_type(un,stype)	
@xref un_s, get_unit_sub_type
	Macro defining the unit set subtype;


@@	@get_unit_axis(un)		
@xref un_s, set_unit_axis
	Macro allowing to acces the unit set axis;

@@	@set_unit_axis(un,axis)		
@xref un_s, get_unit_axis
	Macro defining the unit set axis which may be one of the value below;

<codeblock>
    	IS_X_UNIT_SET		
    	IS_Y_UNIT_SET		
    	IS_Z_UNIT_SET		
    	IS_T_UNIT_SET		
    	IS_UNIT_SET		
<endblock>

@@	@set_unit_decade(un,dec)	
@xref un_s, get_unit_decade
	Macro allowing to acces the unit set decade;
	
@@	@get_unit_decade(un)	
@xref un_s, set_unit_decade
	Macro defining the unit set decade which may be one of the value below;

<codeblock>
	 	IS_TERRA		12
	 	IS_GIGA			9
	 	IS_MEGA			6
	 	IS_KILO			3
	 	IS_MILLI		-3
	 	IS_MICRO		-6
	 	IS_NANO			-9
	 	IS_PICO			-12
	 	IS_FEMTO		-15
<endblock>


@@	@set_unit_offset(un,off)	
@xref un_s, get_unit_offset, get_unit_increment, set_unit_increment
	Macro defining the unit set offset of the affine transformation.
	
@@	@get_unit_offset(un)		
@xref un_s, set_unit_offset, get_unit_increment, set_unit_increment
	Macro allowing to acces the unit set offset of the affine transformation.

@@	@set_unit_increment(un,inc)	
@xref un_s, get_unit_increment, set_unit_offset, get_unit_offset
	Macro allowing to acces the unit set increment of the affine transformation.

@@	@get_unit_increment(un)		
@xref un_s, set_unit_increment, set_unit_offset, get_unit_offset
	Macro defining the unit set increment of the affine transformation.

@@	@set_unit_mode(un,mod)	
@xref un_s, get_unit_mode
	Macro defining the unit set mode.

@@	@get_unit_mode(un)	
@xref un_s, set_unit_mode
	Macro allowing to acces the unit set mode.

@@	@set_unit_name(un,name)	
@xref un_s, get_unit_name
	Macro defining the unit set name, name is a string that is attached to the 
 structure.

@@	@get_unit_name(un)	
@xref un_s, set_unit_name
	Macro allowing to acces the unit set name.


@@un_s* @build_unit_set (int t, float ax, float dx, char d, char m, char *name);
@xref un_s, free_unit_set, duplicate_unit_set
	Create the unitset structure in one step, the name is duplicated.
<codeblock>
	un = build_unit_set(IS_VOLT, -5, (float)10/16384, 0, 0, "V");
<endblock>
	This create a unit set which will convert from a 16 bits CAD to volts. 

@@int @free_unit_set(un_s *uns);
@xref un_s, build_unit_set, duplicate_unit_set
	Free the unit set.

@@un_s* @duplicate_unit_set(un_s *dest, un_s *src);
@xref un_s, build_unit_set, free_unit_set
	Copy one unit set in dest if dest is different from NULL, otherwise
 create a copy of src.

	


@@char* @generate_units (int type, int decade);
@xref un_s, unit_to_type
	Construct a string describing the actual unit.
<codeblock>
	char *name;
	name = generate_units (IS_VOLTS, IS_MICRO);
<endblock>	
	Will produce a string containing "\\mu V" (micro Volts).
	

@@int @unit_to_type (char *unit, int *type, int *decade);
@xref un_s, generate_units
	Just do the opposite: 
<codeblock>
	int type, decade;
	unit_to_type ("\\mu V",&type, &decade);
<endblock>	
	Will produce place IS_VOLTS in type and IS_MICRO in decade.

@@int @change_decade (un_s *un, int dec);
@xref un_s, compare_units
	Allow to switch from micro to nano for instance. The affine transformation
 increment and the unit name is change accordingly.
	
@@int @compare_units (un_s *a, un_s *b);
@xref un_s, change_decade
	Compare two unit sets and return 0 if they correspond to the same unit, 
 otherwise return 1. This fuction handle correctly the fact that the unit 
 sets are in different decade mode (you can compare microvolts and kilovolts) 

@@int @modify_un_decade (void);
@xref un_s, modify_units, modify_units_prime
	Menu function allowing to modify the decade.

@@int @modify_units (void);
@xref un_s, modify_un_decade, modify_units_prime
	Menu function allowing to modify the normal unit set of an image or a plot.

@@int @modify_units_prime (void);
@xref un_s, modify_un_decade, modify_units
	Menu function allowing to modify the unit set of a plot associated with the top and right axis.

@@int @add_new_units (void);
@xref un_s, switch_un_ty
	Menu function allowing to add a new unit set.
	
@@int @switch_un_ty (void);
@xref un_s, switch_un_dec, add_new_units
	Menu function allowing to switch type.

@@int @switch_un_dec (void);
@xref un_s, switch_un_ty, add_new_units
	Menu function allowing to switch decade.

@@int	@get_afine_param_from_unit (un_s *un, float *ax, float *dx);
@xref un_s, set_afine_param_for_unit_with_decade
	This function return the affine transform parameters corresponding to a 
 unit set in  the decade = 0 mode so that you can easily compare units.

@@int	@set_afine_param_for_unit_with_decade (un_s *un, float ax, float dx, int decade);
@xref un_s, get_afine_param_from_unit
	This function is used to set the affine transform parameters of a unit set 
 with the specify decade. ax and dx are give in zero decade mode the unitset is
 transform to the specify deacde.
<codeblock>
	set_afine_param_for_unit_with_decade (un, -5, (float)10/16384, IS_MILLI);
<endblock>	
	set the unit set for the AD converter in the -5000 mV to + 5000 mV range.  


@headingnocontent
Conclusion

All good things must come to an end. Writing documentation is not a good 
thing, though, and that means it goes on for ever. There is always something 
I've forgotten to explain, or some essential detail I've left out, but for 
now you will have to make do with this. Feel free to ask if you can't figure 
something out.

Enjoy. I hope you find some of this stuff useful.


By Shawn Hargreaves.

<link>http://alleg.sourceforge.net/</a>



@!html
@!text
@$@ifinfo
@headingnocontent
Index

@index
@$@end ifinfo

@$@contents
@$@bye

@html
@text
