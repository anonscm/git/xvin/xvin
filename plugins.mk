include platform.mk


# We write the plug'in names in a compact way:
PLUGINS = xvplot  nrutil deconv fft2d stat fjca inout hist lequalz skew ssdna \
stores talbot testim trmov xvjpeg xvtiff wlc ohy lotus tilted spheric calpiezzo diffusion \
igor frommovie evolive cardinal select

# and then give the rule :
plugins : $(addprefix bin/,$(addsuffix .dll,$(PLUGINS)))

%.dll : 
	cd $(filter-out "xvin.dll fftw3.dll", $(addprefix plug-ins-src/,$(basename $(notdir $@)))); make; cd ../..


plsrczip : 
	$(foreach toto,$(PLUGINS),cd plug-ins-src/$(toto); make zipall ;cd ../..;) 

plgclean : 
	$(foreach toto,$(PLUGINS),cd plug-ins-src/$(toto); make clean ;cd ../..;) 

help :
	@echo "all build all the plugins dll from the plugins list \
	plsrczip scan all plugins and archive all the sources \
	plgclean scan all the plugins and clean them \
	the plugins list is " $(PLUGINS);