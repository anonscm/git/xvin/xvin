##################################################################
# main makefile for PlayItAgainSam                                         #
#                                                                #
# this file should NOT be modified                               #
# edit "platform_pico.mk" to select your system and choose target#
#                                                                #
# note: all XXX.mk should only contain definitions, no rules!    #
##################################################################
XVIN_MAIN_MAKEFILE = yes

include platform_pico.mk

LSCFLAGS =  -DALLEGRO_STATICLINK  -DPIAS -DPROG_NAME="PlayItAgainSam" #-DXV_GTK_DIALOG


#include platform.mk
#LSCFLAGS =  -DNO_STD_HELP -DPIAS -I/c/PROGRA~1/allegro-4.2.2/include/

#################################################
# below are definitions of lists of files:      #
#################################################
DEV_O = objp/dev_pcx.o objp/dev_ps.o objp/color.o
DEV_H = include/xvin/dev_def.h include/xvin/color.h include/xvin/plot_opt.h
DEV_S = src/dev_pcx.c src/color.c src/dev_ps.c

BOX_O = objp/box_def.o objp/box_eqs.o objp/box2dev.o objp/box2alfn.o
BOX_H = include/xvin/box_def.h include/xvin/box2alfn.h include/xvin/fops2.h include/generated/config.h
BOX_S = src/box_def.c src/box_eqs.c src/box2dev.c src/box2alfn.c

PLOT_O = objp/unitset.o objp/plot_ds.o objp/plot_op.o objp/plot_gr.o objp/plot2box.o objp/op2box.o
PLOT_H = include/xvin/unitset.h include/xvin/plot_ds.h include/xvin/plot_op.h\
		 include/xvin/plot2box.h include/xvin/plot_gr.h include/xvin/op2box.h
PLOT_S = src/unitset.c src/plot_ds.c src/plot_op.c src/plot2box.c src/op2box.c src/plot_gr.c

GUI_O = objp/unit_gui.o objp/imr_reg_gui.o objp/file_picker_gui.o
GUI_H = include/xvin/imr_reg_gui.h include/xvin/file_picker_gui.h
GUI_S = src/gui/unit_gui.c src/gui/imr_reg_gui.c src/gui/file_picker_gui.c

OI_O = objp/im_oi.o objp/im_reg.o objp/im_gr.o
OI_H = include/xvin/im_oi.h include/xvin/im_reg.h include/xvin/im_gr.h
OI_S = src/im_oi.c src/im_reg.c src/im_gr.c

PRO = objp/plot_reg.o objp/plot_reg_gui.o objp/project.o objp/plot_proj.o
PRH = include/xvin/plot_reg.h include/xvin/plot_reg_gui.h
PRS = src/plot_reg.c src/gui/plot_reg_gui.c src/menus/project.c src/plot_proj.c

UTILO = objp/xvinerr.o objp/util.o objp/gui_def.o objp/win_gui.o objp/xv_pico.o \
 objp/log_file.o objp/win_file.o objp/xv_tools_lib.o objp/global_vars.o
UTILH = include/xvin/xvinerr.h include/xvin/util.h include/xvin/gui_def.h include/xvin/xv_pico.h\
 include/xvin/example.h include/xvin/platform.h include/xvin/log_file.h include/xvin/xv_tools_lib.h include/xvin/xv_plugins.h
UTILS = src/xvinerr.c src/util.c src/gui/gui_def.c src/gui/win_gui.c \
 src/xv_pico.c src/log_file.c src/gui/win_file.c src/lib/xv_tools_lib.c src/xv_plugins.c src/global_vars.c

FFTO = objp/fillib.o objp/fftl32n.o objp/fft_filter_lib.o objp/fftw3_lib.o
FFTH = include/xvin/fillib.h include/xvin/fftl32n.h include/xvin/fft_filter_lib.h include/xvin/fftw3_lib.h
FFTS = src/fillib.c src/fftl32n.c src/lib/fft_filter_lib.c src/lib/fftw3_lib.c

GRO = objp/gr_file.o
GRH = include/xvin/gr_file.h
GRS = src/gr_file.c

XVINO = objp/xvinu.o
XVINH = include/xvin.h
XVINS = src/xvinu.c

PL_MN_O = objp/p_file_mn.o objp/p_edit_mn.o objp/p_disp_mn.o objp/p_tools_mn.o \
 objp/p_treat_mn.o objp/plot_mn.o objp/p_options_mn.o
PL_MN_H = include/xvin/plot_mn.h
#\   src/menus/plot/treat/p_treat_math.h \
 #include/xvin/matrice.h src/menus/plot/treat/p_treat_basic.h src/menus/plot/treat/p_treat_fftw.h
PL_MN_S = src/menus/plot/p_file_mn.c src/menus/plot/p_edit_mn.c\
 src/menus/plot/p_disp_mn.c src/menus/plot/p_tools_mn.c src/menus/plot/plot_mn.c\
 src/menus/plot/p_treat_mn.c src/menus/plot/p_options_mn.c

PLTR_MN_O = objp/interpol.o objp/matrice.o \
 objp/p_treat_basic.o objp/p_treat_math.o objp/p_treat_fftw.o objp/p_treat_p2im.o
PLTR_MN_H = src/menus/plot/treat/p_treat_basic.h src/menus/plot/treat/p_treat_math.h \
 src/menus/plot/treat/p_treat_fftw.h src/menus/plot/treat/p_treat_p2im.h include/xvin/matrice.h
PLTR_MN_S = src/menus/plot/treat/interpol.c src/menus/plot/treat/matrice.c \
 src/menus/plot/treat/p_treat_basic.c src/menus/plot/treat/p_treat_math.c \
 src/menus/plot/treat/p_treat_fftw.c src/menus/plot/treat/p_treat_p2im.c

IM_MN_O = objp/i_file_mn.o objp/i_edit_mn.o objp/im_mn.o objp/i_option_mn.o objp/i_treat_mn.o
IM_MN_H = include/xvin/im_mn.h
IM_MN_S = src/menus/image/i_file_mn.c src/menus/image/i_edit_mn.c\
 src/menus/image/im_mn.c src/menus/image/i_option_mn.c \
 src/menus/image/i_treat_mn.c

IMTR_MN_O = objp/i_treat_im2plot.o objp/i_treat_filter.o \
 objp/i_treat_basic.o objp/i_treat_im2im.o \
 objp/i_treat_fftw.o objp/i_treat_measures.o
IMTR_MN_H = src/menus/image/treat/i_treat_im2plot.h src/menus/image/treat/i_treat_filter.h \
 src/menus/image/treat/i_treat_basic.h src/menus/image/treat/i_treat_im2im.h \
 src/menus/image/treat/i_treat_fftw.h src/menus/image/treat/i_treat_measures.h
IMTR_MN_S = src/menus/image/treat/i_treat_im2plot.c src/menus/image/treat/i_treat_filter.c \
 src/menus/image/treat/i_treat_basic.c src/menus/image/treat/i_treat_im2im.c \
 src/menus/image/treat/i_treat_fftw.c src/menus/image/treat/i_treat_measures.c

HELP_MN_O = objp/help_mn.o
HELP_MN_H = include/xvin/help_mn.h
HELP_MN_S = src/menu/help_mn.c

ifeq ($(MEMCHECK),FORTIFY)
FORTIO = objp/fortify.o
FORTIH = include/fortify.h
FORTIS = src/fortify.c
endif

OBJS  = $(BOX_O) $(PLOT_O) $(UTILO) $(GUI_O) $(FFTO) $(DEV_O) $(GRO) $(XVINO) $(PRO)
OBJS += $(PL_MN_O) $(PLTR_MN_O) $(OI_O) $(IM_MN_O) $(IMTR_MN_O)  $(HELP_MN_O)

ifeq ($(MEMCHECK),FORTIFY)
OBJS += $(FORTIO)
endif

#ifeq ($(LINKING),DYNAMIC)
OBJS += objp/xv_plugins.o
#endif

HEADER  = $(DEV_H) $(BOX_H) $(PLOT_H) $(UTILH) $(GUI_H) $(FFTH) $(GRH) $(XVINH) $(PRH)
HEADER += $(PL_MN_H) $(PLTR_MN_H) $(OI_H) $(IM_MN_H) $(IMTR_MN_H) $(FORTIH) $(HELP_MN_H)

SRC     = $(DEV_S) $(BOX_S) $(PLOT_S) $(UTILS) $(GUI_S) $(FFTS) $(GRS) $(XVINS) $(PRS)
SRC    += $(PL_MN_S) $(PLTR_MN_S) $(OI_S) $(IM_MN_S) $(IMTR_MN_S) $(FORTIS) $(HELP_MN_S)

MAKEFILES = makefile platform_template.mk platform_pico.mk \
            linux.mk win32.mk macos.mk defines.mk \
            plugin.mk plugin-list-ens-lps.mk plugin-list-ens-lyon.mk


#################################################
# below are allfont definitions:                #
#################################################
ALF_DIR = plug-ins-src/allfont/
ALF_OUT = libp/libalfont.$(LIB_EXT)  # name of the allegrofont library (name is platform dependant)
ALF_STUFF = $(ALF_OUT) include/alfont.h include/alfontdll.h
#ALF_S   = $(addprefix $(ALF_DIR),"Makefile alfont.c alfont.h alfontdll.h")
ALF_S   = $(ALF_DIR)Makefile $(ALF_DIR)alfont.c $(ALF_DIR)alfont.h $(ALF_DIR)alfontdll.h
ALF_FLAGS = -DALFONT_DLL -DALFONT_DLL_EXPORTS $(FREETYPE_INC)

ifeq ($(LINKING),DYNAMIC)
ALFONT_DLL=1
ALFONT_DLL_EXPORTS=1
ALF_FLAGS += $(LINKING_OPTION)
endif


#################################################
# below are fftw3 definitions:                  #
#################################################
FFTWFLAGS  = -lfftw3
# common to all systems/platforms
ifeq ($(SYSTEM),WIN32)
FFTW3_STUFF = bin/fftw3.dll lib/libfftw3.a include/fftw3.h
# usefull on Win32 only
endif
FFTW3_DIR = plug-ins-src/fftw-3.1.2/
# location of the win32 dll
FFTW3_S = $(FFTW3_DIR)fftw3.dll $(FFTW3_DIR)fftw3.h $(FFTW3_DIR)fftw3.def $(FFTW3_DIR)makefile



#################################################
# below are cfg_file definitions:                #
#################################################
CFG_DIR = plug-ins-src/cfg_file/
CFG_O = objp/cfg_file.o objp/microscope.o
CFG_S   = $(CFG_DIR)cfg_file.c $(CFG_DIR)microscope.c
CFG_H   = $(CFG_DIR)Pico_cfg.h $(CFG_DIR)microscope.h

OBJS += $(CFG_O)
SRC += $(CFG_S)
HEADER += $(CFG_H)


NRU_DIR = plug-ins-src/nrutil/

NRU_O = objp/covsrt.o objp/gammln.o objp/gammq.o objp/gaussj.o objp/gcf.o \
objp/gser.o objp/mrqcof.o objp/mrqmin.o objp/svdfit.o objp/svdcmp.o \
objp/svdvar.o objp/svbksb.o objp/pythag.o objp/lfit.o objp/ran1.o objp/nrutil.o

NRU_S = $(NRU_DIR)covsrt.c $(NRU_DIR)gammln.c $(NRU_DIR)gammq.c $(NRU_DIR)gaussj.c \
$(NRU_DIR)gcf.c $(NRU_DIR)gser.c $(NRU_DIR)mrqcof.c $(NRU_DIR)mrqmin.c \
$(NRU_DIR)svdfit.c $(NRU_DIR)svdcmp.c $(NRU_DIR)svdvar.c $(NRU_DIR)svbksb.c \
$(NRU_DIR)pythag.c $(NRU_DIR)lfit.c $(NRU_DIR)ran1.c $(NRU_DIR)nrutil.c

NRU_H = $(NRU_DIR)covsrt.h $(NRU_DIR)gammln.h $(NRU_DIR)gammq.h $(NRU_DIR)gaussj.h \
$(NRU_DIR)gcf.h $(NRU_DIR)gser.h $(NRU_DIR)mrqcof.h $(NRU_DIR)mrqmin.h \
$(NRU_DIR)svdfit.h $(NRU_DIR)svdcmp.h $(NRU_DIR)svdvar.h $(NRU_DIR)svbksb.h \
$(NRU_DIR)pythag.h $(NRU_DIR)lfit.h $(NRU_DIR)ran1.h $(NRU_DIR)nrutil.h

OBJS += $(NRU_O)

OBJS += objp/wlc.o

OBJS += objp/polymerWLC.o

OBJS += objp/xvplot.o

OBJS += objp/cardinal.o  objp/sftl32n.o

OBJS += objp/stat.o

OBJS += objp/cordrift.o
OBJS += objp/xvtiff.o
OBJS += objp/viewtiff.o


OBJS += objp/FindSteps.o


OBJS += objp/ImOliSeq.o

OBJS += objp/FitInterference.o

#OBJS += objp/brown_util.o

OBJS += objp/methylstat.o objp/met-gui.o

OBJS += objp/fft2d.o objp/fftbtl32n.o

#################################################
# below are trackBead definitions:                #
#################################################
TRK_DIR = plug-ins-src/trackBead/

TRK_O = objp/scan_zmag_rec.o objp/hat_rec.o objp/force_rec.o objp/select_rec.o objp/record.o \
 objp/fillibbt.o objp/PlayItAgainSam.o objp/brown_util.o objp/gui.o

TRK_S = $(TRK_DIR)scan_zmag_rec.c $(TRK_DIR)hat_rec.c $(TRK_DIR)record.c $(TRK_DIR)force_rec.c \
$(TRK_DIR)select_rec.c $(TRK_DIR)fillibbt.c $(TRK_DIR)PlayItAgainSam.c \
$(TRK_DIR)brown_util.c $(TRK_DIR)gui.c

TRK_H = $(TRK_DIR)scan_zmag_rec.h $(TRK_DIR)hat_rec.h $(TRK_DIR)force_rec.h $(TRK_DIR)select_rec.h \
$(TRK_DIR)record.h $(TRK_DIR)record.hh $(TRK_DIR)fillibbt.h $(TRK_DIR)brown_util.h $(TRK_DIR)gui.h



OBJS += $(TRK_O)
#OBJS += objp/filter_core.o

#SRC += plug-ins-src/filters/filter_core.cc
#HEADER += plug-ins-src/filters/filter_core.h

#THM_DIR = plug-ins-src/trackBead/thermal_z/
#THM_O = objp/thermal_z.o
#THM_S = $(THM_DIR)thermal_z.c
#THM_S = $(THM_DIR)thermal_z.h

OBJS += $(THM_O)
SRC += $(THM_S)
HEADER += $(THM_H)



#############################################################################
# below we import definitions specific to each system                       #
#############################################################################
ifeq ($(SYSTEM),WIN32)
	PIAS :=  bin/PlayItAgainSam.exe
	include $(XVIN_DIR)win32.mk
else
ifeq ($(SYSTEM),MACOS)
	PIAS := bin/PlayItAgainSam
	include $(XVIN_DIR)macos.mk
else
ifeq ($(SYSTEM),LINUX)
	PIAS := bin/PlayItAgainSam
	include $(XVIN_DIR)linux.mk
else
# 	return echo "no system defined"
endif
endif
endif


# above are the definitions
##################################################################################################################
##################################################################################################################
##################################################################################################################
# below are the rules


all : platform_pico.mk alfont fftw3 $(PIAS)
	@echo for $(SYSTEM)/$(PLATFORM), the $(LINKING) version has been build.

.PHONY : all installeur zip plugins plgclean plsrczip doc clean help \
       alfont fftw3

platform_pico.mk : platform_template.mk
	@-if [ ! -e platform_pico.mk ]; then cp platform_template.mk platform_pico.mk; \
	echo A new platform_pico.mk has been generated from the template. \
	please edit this file before running make again. ; \
	else touch platform_pico.mk; \
    echo Your platform_pico.mk was kept, but it may be older than the one from the SVN. \
    You should check it. ; fi;
	exit 1



#################################################
# below are TRK rules:                          #
#################################################

$(TRK_O) : $(TRK_S) $(TRK_H)
	$(CC) $(LSCFLAGS) $(CFLAGS) -DDUMMY $(TRK_DIR)/$(notdir $(patsubst %.o,%.c,$@)) -o $@

objp/filter_core.o : plug-ins-src/filters/filter_core.h plug-ins-src/filters/filter_core.cc
	$(CXX) $(LSCFLAGS) -D$(XV_TARGET) $(CXX_FLAGS) -I./include/ -I /mingw32/include/boost/  -c -o objp/filter_core.o plug-ins-src/filters/filter_core.cc


#################################################
# below are CFG rules:                          #
#################################################

#$(CFG_O) : $(CFG_S) $(CFG_H)
#	$(CC) $(LSCFLAGS) $(CFLAGS) $(CFG_DIR)/$(notdir $(patsubst %.o,%.c,$@)) -o $@


objp/cfg_file.o : plug-ins-src/cfg_file/cfg_file.c plug-ins-src/cfg_file/Pico_cfg.h plug-ins-src/cfg_file/microscope.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -I./plug-ins-src/cfg_file -o objp/cfg_file.o -c plug-ins-src/cfg_file/cfg_file.c

objp/microscope.o : plug-ins-src/cfg_file/microscope.c plug-ins-src/cfg_file/Pico_cfg.h plug-ins-src/cfg_file/microscope.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -I./plug-ins-src/cfg_file -o objp/microscope.o -c plug-ins-src/cfg_file/microscope.c


$(NRU_O) : $(NRU_S) $(NRU_H)
	$(CC) $(LSCFLAGS) $(CFLAGS) $(NRU_DIR)/$(notdir $(patsubst %.o,%.c,$@)) -o $@


objp/wlc.o : plug-ins-src/wlc/wlc.c plug-ins-src/wlc/wlc.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/wlc.o -c plug-ins-src/wlc/wlc.c


objp/polymerWLC.o : plug-ins-src/polymerWLC/polymerWLC.c plug-ins-src/polymerWLC/polymerWLC.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/polymerWLC.o -c plug-ins-src/polymerWLC/polymerWLC.c

$(THM_O) : $(THM_S) $(THM_S)
	$(CC) $(LSCFLAGS) $(CFLAGS) $(THM_DIR)/$(notdir $(patsubst %.o,%.c,$@)) -o $@


objp/xvplot.o : plug-ins-src/xvplot/xvplot.c plug-ins-src/xvplot/xvplot.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/xvplot.o -c plug-ins-src/xvplot/xvplot.c


objp/cardinal.o : plug-ins-src/cardinal/cardinal.c plug-ins-src/cardinal/cardinal.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/cardinal.o -c plug-ins-src/cardinal/cardinal.c

objp/sftl32n.o : plug-ins-src/cardinal/sftl32n.c plug-ins-src/cardinal/sftl32n.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/sftl32n.o -c plug-ins-src/cardinal/sftl32n.c

objp/stat.o : plug-ins-src/stat/stat.c plug-ins-src/stat/stat.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/stat.o -c plug-ins-src/stat/stat.c

objp/cordrift.o : plug-ins-src/cordrift/cordrift.c plug-ins-src/cordrift/cordrift.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/cordrift.o -c plug-ins-src/cordrift/cordrift.c

objp/xvtiff.o : plug-ins-src/xvtiff/xvtiff.c plug-ins-src/xvtiff/xvtiff.h
		$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/xvtiff.o -c plug-ins-src/xvtiff/xvtiff.c

objp/viewtiff.o : plug-ins-src/xvtiff/viewtiff.c plug-ins-src/xvtiff/viewtiff.h
				$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/viewtiff.o -c plug-ins-src/xvtiff/viewtiff.c

objp/FindSteps.o : plug-ins-src/FindSteps/FindSteps.c plug-ins-src/FindSteps/FindSteps.h
		$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/FindSteps.o -c plug-ins-src/FindSteps/FindSteps.c

objp/ImOliSeq.o : plug-ins-src/ImOliSeq/ImOliSeq.c plug-ins-src/ImOliSeq/ImOliSeq.h
	$(CC) $(CFLAGS) -o objp/ImOliSeq.o -c plug-ins-src/ImOliSeq/ImOliSeq.c

objp/FitInterference.o : plug-ins-src/FitInterference/FitInterference.c plug-ins-src/FitInterference/FitInterference.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/FitInterference.o -c plug-ins-src/FitInterference/FitInterference.c



objp/methylstat.o : plug-ins-src/methylstat/methylstat.cc plug-ins-src/methylstat/methylstat.hh
	cd plug-ins-src/methylstat; $(CXX) $(CXX_FLAGS) -I../../include -I../../include/generated -I../../include/xvin -I../../src/menus/plot/treat -Wno-write-strings -c methylstat.cc methylstat.hh gui.hh; cp -uvp methylstat.o ../../objp; cd ../..


objp/met-gui.o : plug-ins-src/methylstat/gui.cc plug-ins-src/methylstat/gui.hh
	cd plug-ins-src/methylstat; $(CXX) $(CXX_FLAGS) -I../../include -I../../include/generated -I../../include/xvin -Wno-write-strings -c gui.cc gui.hh ;cp -uvp gui.o ../../objp/met-gui.o; cd ../..


objp/brown_util.o : plug-ins-src/trackBead/brown_util.c plug-ins-src/trackBead/brown_util.h
	$(CC) -DPIAS $(CFLAGS) -o objp/brown_util.o -c plug-ins-src/trackBead/brown_util.c

objp/fft2d.o objp/fftbtl32n.o : plug-ins-src/fft2d/fft2d.c plug-ins-src/fft2d/fft2d.h plug-ins-src/fft2d/fftbtl32n.c plug-ins-src/fft2d/fftbtl32n.h
	$(CC) $(CFLAGS) -o objp/fft2d.o -c plug-ins-src/fft2d/fft2d.c
	$(CC) $(CFLAGS) -o objp/fftbtl32n.o -c plug-ins-src/fft2d/fftbtl32n.c

#################################################
# below are fftw3 rules:                        #
#################################################
fftw3 : $(FFTW3_STUFF)

bin/fftw3.dll : $(FFTW3_DIR)fftw3.dll
	cp -uvp $(FFTW3_DIR)fftw3.dll bin/fftw3.dll

include/fftw3.h : $(FFTW3_DIR)fftw3.h
	cp -uvp $(FFTW3_DIR)fftw3.h include/fftw3.h

lib/libfftw3.a : $(FFTW3_DIR)fftw3.dll $(FFTW3_DIR)fftw3.def
	cd $(FFTW3_DIR); dlltool --def fftw3.def -k --dllname fftw3.dll \
 --output-lib ../../libp/libfftw3.a; cd ../..

bin/libfftw3.so :                # note : this is unused, but just in case...
	ln -s bin/libfftw3.so $(word 1,$(GNU_LIBS_DIR))/lib/libfftw3.so

bin/libfftw3.dylib :             # note : this is unused, but just in case...
	ln -s bin/libfftw3.dylib $(word 1,$(GNU_LIBS_DIR))/lib/libfftw3.dylib

#################################################
# below are alfont rules:                       #
#################################################
alfont : $(ALF_STUFF)

$(ALF_OUT) :
	cd $(ALF_DIR); make -f Makefile global_all;

include/alfont.h : $(ALF_DIR)/alfont.h
	cp -vp $(ALF_DIR)/alfont.h include/alfont.h

include/alfontdll.h : $(ALF_DIR)/alfontdll.h
	cp -vp $(ALF_DIR)/alfontdll.h include/alfontdll.h



##############################################
# plug'ins stuff:                            #
##############################################

# the rules about plugins:
plugins : $(XVIN)
	$(foreach toto,$(PLUGINS),cd plug-ins-src/$(toto); make global_all; cd ../..;)

plsrczip :
	$(foreach toto,$(PLUGINS),cd plug-ins-src/$(toto); make global_zip; cd ../..;)

plgclean :
	$(foreach toto,$(PLUGINS),cd plug-ins-src/$(toto); make global_clean; cd ../..; )



#################################################################
# below are the rules to produce xvin objects:                  #
#################################################################

objp/fillib.o	: src/fillib.c include/xvin/fillib.h include/xvin/util.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/fillib.o -c src/fillib.c

objp/fftl32n.o	: src/fftl32n.c include/xvin/fftl32n.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/fftl32n.o -c src/fftl32n.c

$(DEV_O) : $(DEV_S) $(DEV_H)
	$(CC) $(LSCFLAGS) $(CFLAGS) src/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(BOX_O) : $(BOX_S) $(BOX_H)
	echo cflags :$(CFLAGS); echo fin; $(CC) $(LSCFLAGS) $(CFLAGS) src/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(PLOT_O) : $(PLOT_S) $(PLOT_H)
	$(CC) $(LSCFLAGS) $(CFLAGS) src/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(GUI_O) : $(GUI_S) $(GUI_H) include/xvin/im_reg.h
	$(CC) $(LSCFLAGS) $(CFLAGS) src/gui/$(notdir $(patsubst %.o,%.c,$@)) -o $@

objp/xvinerr.o	: include/xvin/xvinerr.h src/xvinerr.c
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/xvinerr.o src/xvinerr.c

objp/util.o	: include/xvin/util.h src/util.c
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/util.o src/util.c

objp/log_file.o	: include/xvin/util.h include/xvin/log_file.h src/log_file.c
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/log_file.o src/log_file.c

objp/gr_file.o	: include/xvin/gr_file.h src/gr_file.c
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/gr_file.o src/gr_file.c

objp/xvinu.o	: include/xvin.h src/xvinu.c
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/xvinu.o src/xvinu.c

objp/plot_reg.o : include/xvin/plot_reg.h src/plot_reg.c
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/plot_reg.o src/plot_reg.c

objp/plot_proj.o : include/xvin/plot_reg.h include/xvin/plot_ds.h include/xvin/plot_op.h\
 src/plot_proj.c
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/plot_proj.o src/plot_proj.c

objp/plot_reg_gui.o : include/xvin/plot_reg.h src/gui/plot_reg_gui.c include/xvin/plot_reg_gui.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/plot_reg_gui.o src/gui/plot_reg_gui.c

objp/gui_def.o : include/xvin/gui_def.h src/gui/gui_def.c
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/gui_def.o src/gui/gui_def.c

objp/win_file.o : include/xvin/util.h src/gui/win_file.c
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/win_file.o src/gui/win_file.c

objp/win_gui.o : src/gui/win_gui.c include/xvin.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/win_gui.o src/gui/win_gui.c

$(PL_MN_O) : $(PL_MN_S) $(PL_MN_H)
	$(CC) $(LSCFLAGS) $(CFLAGS) src/menus/plot/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(PLTR_MN_O) : $(PLTR_MN_S) $(PL_MN_H)
	$(CC) $(LSCFLAGS) $(CFLAGS) src/menus/plot/treat/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(IM_MN_O) : $(IM_MN_S) $(IM_MN_H)
	$(CC) $(LSCFLAGS) $(CFLAGS) src/menus/image/$(notdir $(patsubst %.o,%.c,$@)) -o $@

$(IMTR_MN_O) : $(IMTR_MN_S) $(IMTR_MN_H)
	$(CC) $(LSCFLAGS) $(CFLAGS) src/menus/image/treat/$(notdir $(patsubst %.o,%.c,$@)) -o $@

objp/project.o : src/menus/project.c
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/project.o src/menus/project.c

objp/xv_pico.o : src/xv_pico.c include/xvin/xv_main.h src/svn.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o objp/xv_pico.o src/xv_pico.c

$(OI_O) : $(OI_S) $(OI_H)
	$(CC) $(LSCFLAGS) $(CFLAGS) src/$(notdir $(patsubst %.o,%.c,$@)) -o $@

objp/fft_filter_lib.o: src/lib/fft_filter_lib.c include/xvin/fft_filter_lib.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o $@ src/lib/fft_filter_lib.c

objp/xv_tools_lib.o: src/lib/xv_tools_lib.c include/xvin/xv_tools_lib.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o $@ src/lib/xv_tools_lib.c

objp/help_mn.o : src/menus/help_mn.c include/generated/gitinfo.h
	$(CC) $(CFLAGS) -Isrc -Iinclude/generated -o objp/help_mn.o src/menus/help_mn.c

objp/fortify.o : src/fortify.c include/fortify.h
	$(CC) $(CFLAGS) -DFORTIFY -DWARN_ON_ZERO_MALLOC -DWARN_ON_FALSE_FAIL -DWARN_ON_SIZE_T_OVERFLOW -DWARN_ON_MALLOC_FAIL  -o $@ src/fortify.c
# -DCHECK_ALL_MEMORY_ON_FREE


objp/xv_plugins.o : src/xv_plugins.c include/xvin/xv_plugins.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -DBUILDING_DLL  -o $@ src/xv_plugins.c

objp/global_vars.o : src/global_vars.c
	$(CC) $(CFLAGS) -o $@ src/global_vars.c

objp/fftw3_lib.o: src/lib/fftw3_lib.c include/xvin/fftw3_lib.h
	$(CC) $(LSCFLAGS) $(CFLAGS) -o $@ src/lib/fftw3_lib.c


src/svn.h: src/svn.in
	@echo "#define SVN_REVISION \"$(svn info | grep 'Last Changed Rev: '  | sed 's/Last Changed Rev: //g')\"" > src/svn.h
	@echo "#define SVN_DATE \"$(svn info | grep 'Last Changed Date: '  | sed 's/Last Changed Date: //g')\"" >> src/svn.h
	@echo "#define SVN_MODIFICATION \"$(svn info | grep 'Last Changed Author: '  | sed 's/Last Changed Author: //g')\"" >> src/svn.h




##############################################
# icon  (same code for all win32 versions)	 #
##############################################

ifeq ($(SYSTEM),WIN32)
ICON = objp/icon.o
objp/icon.o: icons/gr.ico icons/xvin.rc
	windres -I rc -O coff -i icons/xvin.rc -o objp/icon.o
endif

ifeq ($(SYSTEM),LINUX)
ICON = # objp/icon.o

# following 2 lines are required if and only if file X11_icon.c needs to be rebuilt
#icons/X11_icon.c: icons/gr.ico
#	icons/xfixicon.sh icons/gr.ico -o icons/X11_icon.c
objp/icon.o: icons/X11_icon.c
	$(CC) $(LSCFLAGS) $(CFLAGS) -o $@ icons/X11_icon.c # -DALLEGRO_WITH_XWINDOWS -DALLEGRO_USE_CONTRUCTOR
endif



##################################################################
# xvin executable, win32 systems       					         #
##################################################################
ifeq ($(SYSTEM),WIN32)
ifeq ($(LINKING),STATIC)
bin/PlayItAgainSam.exe : $(OBJS) $(ICON) makefile include/xvin.h src/main_pico.c
	$(CXX) -Wall -mwindows $(ICON) $(CPU_FLAGS) -D$(XV_TARGET) -DXVIN_STATICLINK \
 -I./include -I./include/xvin $(OBJS) -o $@ src/main_pico.c  -L./lib -L/mingw32/lib -I/mingw32/include $(GTK_LIB) -ltiff -llibfftw3 -lfftw3  -lalleg44  -lalfont -lkernel32 -lgsl -luser32 -lgdi32 -lcomdlg32 -lole32 -ldinput -lddraw -ldxguid -lwinmm -ldsound -ldl -lstdc++ ; rm src/svn.h

#-lalleg -lfftw3 -lalfont -lkernel32 -luser32 -lgdi32 -lcomdlg32 -lole32 -Wl,-Bstatic -ldinput -lddraw -ldxguid -Wl,-Bdynamic -lwinmm -Wl,-Bstatic -ldsound -Wl,-Bdynamic -lkernel32 -luser32 -lgdi32 -lwinspool -lshell32 -lole32 -loleaut32 -luuid -lcomdlg32 -ladvapi32; rm src/svn.h

#-lalleg44.dll   -lfftw3 -lalfont -lkernel32 -luser32 -lgdi32 -lcomdlg32 -lole32 -ldinput -lddraw -ldxguid -lwinmm -ldsound; rm src/svn.h



# -lalfontdll -lalleg_s
endif


ifeq ($(LINKING),DYNAMIC)
ifeq ($(PLATFORM),MINGW)
bin/PlayItAgainSam.exe :  $(FFTW3_STUFF) bin/alfont.dll bin/PlayItAgainSam.dll lib/libPlayItAgainSam.a $(ICON) makefile include/xvin.h src/main_pico.c
	$(CC) -Wall -mwindows $(PLATFORM_FLAGS) $(ICON) $(CPU_FLAGS) -D$(XV_TARGET) \
 -I./include -I./include/xvin -o bin/PlayItAgainSam.exe src/main_pico.c -L./lib -lalleg44 -lPlayItAgainSam
endif
ifeq ($(PLATFORM),CYGWIN)
bin/PlayItAgainSam.exe : bin/xvin.dll lib/PlayItAgainSam.a $(ICON) makefile include/xvin.h src/main_pico.c bin/fftw3.dll
	$(CC) -Wall -mwindows $(PLATFORM_FLAGS) $(ICON) $(CPU_FLAGS) -D$(XV_TARGET) $(INC_FLAGS) \
 -I./include -I./include/xvin -o bin/PlayItAgainSam.exe src/main_pico.c -L./lib -lalleg44 -lPlayItAgainSam
endif
endif

endif # end of the SYSTEM=WIN32 case



##################################################################
# xvin executable, linux systems                                 #
##################################################################
ifeq ($(SYSTEM),LINUX)
ifeq ($(LINKING),STATIC)
bin/PlayItAgainSam : $(OBJS) $(ICON) makefile include/xvin.h src/main_pico.c
	$(CXX) -Wall  $(ICON) $(CPU_FLAGS) -D$(XV_TARGET) $(GTK_INC) -DXVIN_STATICLINK \
 -I./include -I./include/xvin -I./include/generated $(OBJS) plug-ins-src/allfont/alfont.o -o $@ src/main_pico.c -Wl,-rpath,$(HOME)/xvin/bin -L./bin -lxvin \
	$(FREETYPE_LIB) $(ALLEGRO_LIB) -lxvin  $(FFTWFLAGS) $(GTK_LIB) -lm -ldl
endif # end of the case LINKING=STATIC

ifeq ($(LINKING),DYNAMIC)
bin/PlayItAgainSam : bin/libxvin.so include/xvin.h src/main_pico.c $(ICON) makefile
	$(CC) $(LSCFLAGS) $(CFLAGS) src/main_pico.c -o main_pico.o; \
        cd bin/; $(CC) $(LD_FLAGS) -o PlayItAgainSam ../main_pico.o libpico.so libalfont.so \
	$(LIB_OPTS) $(FFTWFLAGS) $(FREETYPE_LIB) $(ALLEGRO_LIB) \
	-Wl,-rpath,$(HOME)/xvin/bin

endif # end of the case LINKING=DYNAMIC
endif # end of the case SYSTEM=LINUX



##################################################################
# xvin executable, MacOSX systems                                #
##################################################################
ifeq ($(SYSTEM),MACOS)
ifeq ($(LINKING),STATIC)
bin/xvin : lib/libxvin.a  makefile include/xvin.h src/main_pico.c
	$(CC) -c $(CPU_FLAGS) -D$(XV_TARGET) -I./include -o main_pico.o -I./include/xvin src/main_pico.c
	$(CC) -s -L/usr/X11R6/lib -Wl,-dynamic  -o bin/xvin main_pico.o -framework cocoa\
	-L./lib $(ALLEGRO_LIB) -lPlayItAgainSam -lfreetype -lz -lalfont -lfftw3 -lm; \
	$(ALLEGRO_BIN)/fixbundle $@ icons/gr.bmp
endif # end of the case LINKING=STATIC

ifeq ($(LINKING),DYNAMIC)
bin/xvin : bin/libPlayItAgainSam.dylib include/xvin.h src/main_pico.c makefile
	$(CC) $(LSCFLAGS) $(CFLAGS) src/main_pico.c -o main_pico.o # -I./include -I./include/xvin
	cd bin/; \
    $(CC) $(LD_FLAGS) -o xvin ../main_pico.o libxPlayItAgainSam.dylib libalfont.dylib \
	$(LIB_OPTS) $(FFTWFLAGS) \
	$(FREETYPE_LIB) $(ALLEGRO_LIB) \
	--rpath=$(HOME)/xvin/bin;	\
#	-Wl,-rpath,$(HOME)/xvin/bin
	$(ALLEGRO_BIN)/fixbundle $@ gr.bmp
endif # end of the case LINKING=DYNAMIC
endif # end of the case SYSTEM=MACOS



#################################################################
# libxvin.a library (same code for all platforms)               #
#################################################################
lib/libPlayItAgainSam.a : $(OBJS) $(MAKEFILES)
	ar $(AR_FLAGS) lib/libPlayItAgainSam.a $(OBJS)



#################################################################
# xvin.dll (same code for all win32 platforms)                  #
#################################################################
bin/PlayItAgainSam.dll : $(OBJS) $(MAKEFILES)
	$(CC) -o $@ $(OBJS) $(PLATFORM_FLAGS) $(DLL_FLAGS) \
	-L./lib $(addprefix -L,$(addsuffix /lib/,$(GNU_LIBS_DIR))) \
    $(FFTWFLAGS) -lalfont -lalleg44 -Wl,--export-all-symbols \
    -Wl,--enable-auto-import -Wl,--out-implib=lib/libPlayItAgainSam.a


#################################################################
# xvin.so (same code for all linux platforms)                   #
#################################################################
bin/libPlayItAgainSam.so : $(OBJS) $(MAKEFILES)
	$(CC) -o $@ $(OBJS) -shared $(LIB_OPTS)  \
	$(LD_FLAGS) $(FFTWFLAGS) $(FREETYPE_LIB) $(ALLEGRO_LIB) \
	-Wl,-export-dynamic


#################################################################
# xvin.dylib (Mac OS X platform)                                #
#################################################################
bin/libPlayItAgainSam.dylib : $(OBJS) $(MAKEFILES)
	$(CC) -o $@ $(OBJS) -dynamiclib $(LIB_OPTS) -L$(XVIN_DIR)bin \
	$(LD_FLAGS) $(FFTWFLAGS) $(FREETYPE_LIB) $(ALLEGRO_LIB) \
	-lalfont -Wl,-dynamic


#################################################################
# below are the rules to produce the documentation    		#
#################################################################

DOCS = docs/html/picobi.html docs/rtf/picobi.rtf docs/txt/picobi.txt docs/txi/picobi.txi docs/chm/picobi.chm docs/chm/picobi.html

DOCS += docs/html/picodv.html docs/rtf/picodv.rtf docs/txt/picodv.txt docs/txi/picodv.txi docs/chm/picodv.chm docs/chm/picodv.html

DOCS += docs/html/picotw.html docs/rtf/picotw.rtf docs/txt/picotw.txt docs/txi/picotw.txi docs/chm/picotw.chm docs/chm/picotw.html

DOCS += docs/html/piashlp.html docs/rtf/piashlp.rtf docs/txt/piashlp.txt docs/txi/piashlp.txi docs/chm/piashlp.chm docs/chm/piashlp.html

doc : $(DOCS)
	@echo for $(SYSTEM)/$(PLATFORM), the doc version has been build.

docs/html/piashlp.html : docs/src/piashlp._tx mkpias.mk
	bin/makedoc.exe -html docs/html/piashlp.html  docs/src/piashlp._tx

docs/rtf/piashlp.rtf : docs/src/piashlp._tx mkpias.mk
	bin/makedoc.exe -rtf docs/rtf/piashlp.rtf  docs/src/piashlp._tx

docs/txt/piashlp.txt : docs/src/piashlp._tx mkpias.mk
	bin/makedoc.exe -ascii  docs/txt/piashlp.txt  docs/src/piashlp._tx

docs/txi/piashlp.txi : docs/src/piashlp._tx mkpias.mk
	bin/makedoc.exe -txi docs/txi/piashlp.txi  docs/src/piashlp._tx

docs/chm/piashlp.html : docs/src/piashlp._tx mkpias.mk
	cp -vp docs/src/pias*.jpg docs/html; \
	cp -vp docs/src/pias*.png docs/html; \
	bin/makedoc.exe -ochm -html docs/chm/piashlp.html  docs/src/piashlp._tx

docs/chm/piashlp.chm : docs/src/piashlp._tx docs/chm/piashlp.html mkpias.mk
	cp -vp docs/src/pias*.jpg docs/chm; \
	cp -vp docs/src/pias*.png docs/chm; \
        bin/makedoc.exe -chm docs/chm/piashlp.html  docs/src/piashlp._tx; \
	cd docs/chm; /c/'Program Files (x86)/HTML Help Workshop'/hhc.exe piashlp.hhp; cd ../..


docs/html/picodv.html : docs/src/picodv._tx mkpias.mk
	bin/makedoc.exe -html docs/html/picodv.html  docs/src/picodv._tx

docs/rtf/picodv.rtf : docs/src/picodv._tx mkpias.mk
	bin/makedoc.exe -rtf docs/rtf/picodv.rtf  docs/src/picodv._tx

docs/txt/picodv.txt : docs/src/picodv._tx mkpias.mk
	bin/makedoc.exe -ascii  docs/txt/picodv.txt  docs/src/picodv._tx

docs/txi/picodv.txi : docs/src/picodv._tx mkpias.mk
	bin/makedoc.exe -txi docs/txi/picodv.txi  docs/src/picodv._tx

docs/chm/picodv.html : docs/src/picodv._tx mkpias.mk
	cp -vp docs/src/pico*.jpg docs/html; \
	cp -vp docs/src/pico*.png docs/html; \
	bin/makedoc.exe -ochm -html docs/chm/picodv.html  docs/src/picodv._tx

docs/chm/picodv.chm : docs/src/picodv._tx docs/chm/picodv.html mkpias.mk
	cp -vp docs/src/pico*.jpg docs/chm; \
	cp -vp docs/src/pico*.png docs/chm; \
        bin/makedoc.exe -chm docs/chm/picodv.html  docs/src/picodv._tx; \
	cd docs/chm; /c/'Program Files (x86)/HTML Help Workshop'/hhc.exe picodv.hhp; cd ../..



docs/html/picobi.html : docs/src/picobi._tx mkpias.mk
	bin/makedoc.exe -html docs/html/picobi.html  docs/src/picobi._tx

docs/rtf/picobi.rtf : docs/src/picobi._tx mkpias.mk
	bin/makedoc.exe -rtf docs/rtf/picobi.rtf  docs/src/picobi._tx

docs/txt/picobi.txt : docs/src/picobi._tx mkpias.mk
	bin/makedoc.exe -ascii  docs/txt/picobi.txt  docs/src/picobi._tx

docs/txi/picobi.txi : docs/src/picobi._tx mkpias.mk
	bin/makedoc.exe -txi docs/txi/picobi.txi  docs/src/picobi._tx

docs/chm/picobi.html : docs/src/picobi._tx mkpias.mk
	cp -vp docs/src/pico*.jpg docs/html; \
	cp -vp docs/src/pico*.png docs/html; \
	bin/makedoc.exe -ochm -html docs/chm/picobi.html  docs/src/picobi._tx

docs/chm/picobi.chm : docs/src/picobi._tx docs/chm/picobi.html mkpias.mk
	cp -vp docs/src/pico*.jpg docs/chm; \
	cp -vp docs/src/pico*.png docs/chm; \
        bin/makedoc.exe -chm docs/chm/picobi.html  docs/src/picobi._tx; \
	cd docs/chm; /c/'Program Files (x86)/HTML Help Workshop'/hhc.exe picobi.hhp; cd ../..



docs/html/picotw.html : docs/src/picotw._tx mkpias.mk
	bin/makedoc.exe -html docs/html/picotw.html  docs/src/picotw._tx

docs/rtf/picotw.rtf : docs/src/picotw._tx mkpias.mk
	bin/makedoc.exe -rtf docs/rtf/picotw.rtf  docs/src/picotw._tx

docs/txt/picotw.txt : docs/src/picotw._tx mkpias.mk
	bin/makedoc.exe -ascii  docs/txt/picotw.txt  docs/src/picotw._tx

docs/txi/picotw.txi : docs/src/picotw._tx mkpias.mk
	bin/makedoc.exe -txi docs/txi/picotw.txi  docs/src/picotw._tx

docs/chm/picotw.html : docs/src/picotw._tx mkpias.mk
	cp -vp docs/src/pico*.jpg docs/html; \
	cp -vp docs/src/pico*.png docs/html; \
	bin/makedoc.exe -ochm -html docs/chm/picotw.html  docs/src/picotw._tx

docs/chm/picotw.chm : docs/src/picotw._tx docs/chm/picotw.html mkpias.mk
	cp -vp docs/src/pico*.jpg docs/chm; \
	cp -vp docs/src/pico*.png docs/chm; \
        bin/makedoc.exe -chm docs/chm/picotw.html  docs/src/picotw._tx; \
	cd docs/chm; /c/'Program Files (x86)/HTML Help Workshop'/hhc.exe picotw.hhp; cd ../..







#################################################
# cleaning (common to all platforms)			#
#################################################
clean :
	rm -f $(OBJS)
	rm -f lib/libPlayItAgainSam.a
	rm -f bin/PlayItAgainSam.exe
	rm -f bin/PlayItAgainSam.dll bin/PlayItAgainSam.so bin/PlayItAgainSam.dylib
	rm -f bin/PlayItAgainSam
	rm -f $(ALF_STUFF) $(ALF_DIR)*.o
	rm -f $(FFTW3_STUFF)





##############################################
# installeur								 #
##############################################
installeur : installer/Pias_script_modern.nsi
	$(NSIS_BIN) installer/Pias_script_modern.nsi
	cp -uvp installer/Pias_setup.exe .




#################################################
# archives										#
#################################################

zip	: $(HEADER) $(SRC) makefile src/main_pico.c # docs/src/xvin._tx
	$(ZIPEXE) xvinsrc$(ZIP_EXT) $(HEADER) $(SRC) \
  $(MAKEFILES) src/main_pico.c \
 icons/gr.ico icons/gr.bmp icons/X11_icon.c icons/xfixicon.sh icons/xvin.rc \
 objp/remove.me lib/remove.me bin/fonts.dat \
 bin/allegro.cfg bin/arial.ttf  bin/keyboard.dat bin/language.dat \
 bin/keyconf.txt  \
 $(FFTW3_S) $(ALF_S) \
 docs/src/xvin._tx docs/html/*.* docs/rtf/*.* docs/txi/*.* docs/txt/*.* docs/chm/*.* \
# include/plug-ins/fftw3.h


#################################################
# help                                          #
#################################################
help :
	@echo "Makefile for XVin. possible options are :"
	@echo "[]/[all]   build XVin"
	@echo "[help]     print this help message"
	@echo "[clean]    clean the XVin objects and binaries"
	@echo "[zip]      zip the XVin sources"
	@echo "[doc]      build the XVin docs"
	@echo " "
	@echo "[plugins]  build all the plugins from the plugins list"
	@echo "[plsrczip] scan all plugins and archive all the sources"
	@echo "[plgclean] scan all the plugins and clean them"
	@echo " "
	@echo "the plugins list is (from the main makefile):"
	@echo $(PLUGINS);
