#! /bin/bash
LANG=C python3 waf build_xvin && \
cd build/xvin && LANG=C LD_LIBRARY_PATH="$PWD:/usr/local/lib" $1 ./xvin
