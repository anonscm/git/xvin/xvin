Prerequisite
============

## Clone xvin repository
To install git, refer to the section of your operating system

To clone the repository you will need an account in the lab server (git.pimprenelle.lps.ens.fr) or picoseq server (git.picoseq.org)

Note: Code should be pushed only from pimprenelle. don't use picoseq server to to that on this specific project.

### Clone from tig.phys.ens.fr

```bash
git clone https://tig.phys.ens.fr/ABCDLab/xvin/xvin.git

```
You need a username and a password.
You can write to vincent.croquette@lps.ens.fr for more information.


## Windows specifics

### Install MSys2
To compile xvin on windows, you need a gcc 32bit working build chain
Most user uses MSYS2.

You can find binary and installation tutorial [Here](https://msys2.github.io/)

### Msys2 launcher

To get a launcher for mingw32 install the package msys2-launcher

### Install GIT
git is an msys2 package, you can retrieve in an mingw32 console using:

```bash
pacman -Syu git
```
### Install xvin dependency

A simple script give the ability to install all xvin dependency at once (except allegro)

```bash
cd xvin
./misc/install_package_msys2.sh
```
### Install allegro and FTDI

Allegro4 is an old library which is not packaged with msys2.
FTDI is made to control rs232 over USB it is also not packaged with msys2
to install it, get the binary from

http://pimprenelle.lps.ens.fr/drupal6/sites/default/files/media/allegro-binary-msys2.zip



and extract it in the msys2/mingw32 folder (merge the folders if needed) but don't erase files from mingw32 folder

### Set default version of python

For **windows**, to get python3.8 as default version:

```bash
cd /mingw32/bin
rm python
rm python-config

ln -s python3.8 python
ln -s python3.8-config python-config
```

The installation script for windows also installs required python packages.

## Linux (checked on Ubuntu 18.04 and 20.04)

### Install xvin dependency

```bash
cd xvin
./misc/install_package_debian_ubuntu.sh
```
### Install uEye Camera
Download the latest SDK from uEye website (version 4.94) for USB cameras called "xxx.run" and run :
```bash
sudo bash "xxx.run"
```

### Install serial interface

In order to use serial interface with a standard user and avoid
> /usr/ttyS0 : permission denied
> for rawhid issue set plugdev group to username

You add you user to groups and configure ftdi driver :
```bash
sudo usermod -a -G dialout <username>
sudo usermod -a -G uucp <username>
sudo usermod -a -G input <username>
sudo usermod -a -G plugdev <username>
sudo ./misc/ftdi_config.sh
```

You need to reboot of your session to the new settings to take effect.

Build (Makefile Method)
=========

Before everything you must compile alfont, which is a plugin, whose dll is needed to build xvin. But this needs a platform.mk which is generated during the first call to build the xvin makefile then. In xvin directory :
```
make
```
You should have an error message telling that you miss allfont.dll but at least you have generated a platform.mk file. Next step :
```
cd plug-ins-src/alfont
```
And then run
```
make
```
You have now built allfont.dll. For the next step, an internal bug in the makefile of xvin fails to copy the alleg44.dll from mingw32/bin to xvin/bin so copy this dll by hand...
```
cp -uvp /mingw32/bin/alleg44.dll xvin/bin
```
Then go to xvin directory and run again :
```
make
```
Now xvin.exe is built (you may need to do the last make twice). But at that step if you run the .exe you get a "no font" error message.
```
cp -uvp xvin/misc/fonts.dat xvin/bin
```
Then xvin is built and sould work.

Now you can dream about building pico in his different versions (pico, picoueye, picoueye-PI709 etc, depending on peripherals config, game etc).
For all versions, at first, you need to create the folder xvin/objp to store the objects files of pico. Go to xvin directory and type :
```
mkdir objp
```
then for pico with ueye camera and pifoc directly linked to computer (not through microcontroler)
make -f mkpicoueye-PI709.mk

for game
make -f mkpico.mk
then copy misc/default_config/Pico.cfg to xvin/bin, and misc/bead000quater.gr to xvin/bin


Compilation (WAF Method)
=========

Configuration
------------

To launch the configuration process of xvin / pico / play it again sam you can launch

```
python3 waf configure
```
### Typical Configurations
Compile the programe with no opencl support, acquisition software using a uEye Camera and with both debugging flags and compiler optimization flags.
```
python3 waf configure --opencl --camera ueye --mode debug-opti --no-python
```

### Configuration arguments

All flags and description can be found using:
```
python3 waf --help
```

#### Build mode:

    -m MODE, --mode=MODE

* sanitize : No optimization, with debugger and sanitizer flags
* *debug* : No optimization with debugger flags
* debug-opti : optimized code with debugger flags
* release : optized code, no debugger flags

#### Platform

the platform in which will be compiled xvin, without this, the platform is automatically detected.

    -p PLATFORM, --platform=PLATFORM

* *linux*
* win

#### Camera
which camera driver to build with the software.

    -c CAMERA, --camera=CAMERA

* *sim*: Simulated image instead of a physical camera
* movie: Loop a .gr movie instead of a physical camera
* ueye: IDS uEye Camera
* jai: Jai camera

#### Serial support

    -s SERIAL, --serial=SERIAL

* True: Compile with serial support
* False: Dummy instrument interface.

#### Focus
Compile with special piezo objective stepper support:

    -z FOCUS, --focus=FOCUS

* *pifoc*: PI Piezo
* madcity: Madcity piezo


#### Other options

    --no-openmp           # disable OpenMP
    --opencl              # enable  OpenCL
    --no-warning          # disable Warning
    --no-gtkdialog        # disable all gtk dialogs
    --no-gtkprintf        # disable GTK for win_printf and win_scanf
    --cxx-only            # C++ compilation only
    --use-msvc            # On windows : use msvc compiler

### Toubleshooting

Some anti-virus blocks the configuration process,
To go through, you can disable the « Real-time scan »


###Build

After configure has succeeded, launch
```
python3 waf build
```
It will build PIAS, XVIN, and PICO.
If you want to only build one of them, you can launch
```
python3 waf build_pico
python3 waf build_pias
python3 waf build_xvin
```

###Install shortcuts

Finally, install shortcuts by launching from your xvin depository
```
sudo ./misc/install_shortcut_linux.sh
```

## Mac OS Building and Installing (PIAS only - 32 bits)

### Allegro

Allegro 4 is not supported any more on OSX. We thus need to download an older SDK to build it on OSX

1. Download the Xcode 3.1 installer (named xcode31_2199_developerdvd.dmg) which is available on the [Apple Developer site](https://developer.apple.com/download/more/).  Open the developer DVD image, then install the MacOSX10.4.Universal.pkg file in the Packages directory, which will place the SDK in /SDKs/MacOSX10.4u.sdk. You can delete the Xcode 3.1 image now, since we only need this one file.

2. Download allegro-4.4.2.zip from the [allegro web page](https://www.allegro.cc/files/?v=4.4)

3. Go to the unzipped allegro 4.4.2 folder, create a directory named build and go there with the terminal

4. Type in

```
export MACOSX_DEPLOYMENT_TARGET=10.4
export CFLAGS=-m32
cmake -DCMAKE_OSX_SYSROOT=/SDKs/MacOSX10.4u.sdk -DCMAKE_OSX_DEPLOYMENT_TARGET=10.4 ..
```
then
```
make install
```

5. The installation should work. Starting from El Capitan, you might get the following compilation error :
```
ld: in '/SDKs/MacOSX10.4u.sdk/System/Library/Frameworks//Accelerate.framework/Versions/A/Accelerate', malformed mach-o, symbol table not in __LINKEDIT
clang: error: linker command failed with exit code 1 (use -v to see invocation)
```
The only solution to get around this problem that we found so far is to uninstall the installed version to Apple Xcode and to download the version 7.2 from the Apple website. The compilation should then work.

### Dependencies

The dependencies are basically the same than listed in the Linux part. The only difference is that all dependencies should be also built in 32 bits given that Allegro 4 can not be built in 64 bits on OS X.

The simplest way to install dependencies with 32 bits symbol is to use the macports package manager, adding the "+universal" option, as in the example below. Indeed, homebrew does not allow easy install of cross compiled libraries any more.

```
sudo port install boost +universal
```

### Compilation of 'PIAS'.

1. Install gcc and g++ with macports :
```
sudo port install gcc7 +universal
```

2. Go to your git-cloned folder and type in :
```
 CC=/opt/local/bin/gcc-mp-7 CXX=/opt/local/bin/g++-mp-7 python3 waf configure --no-python
 ```

 Then
 ```
 CC=/opt/local/bin/gcc-mp-7 CXX=/opt/local/bin/g++-mp-7 python3 waf build_pias --no-warning
 ```


3. If you get an error of the type "linker was not able to find symbol for architectures i386 for the shared library xxx", you will need to reinstall the library xxx with macport using the option "+universal".
4. If you get an error concerning sched.h, erase the "include/pthread/sched.h" file and install pthread :
```
sudo port install pthread +universal
```

5. If you get an error "can not use auto within a function argument", it is because clang does not support this feature and that you didn't use gcc as mentionned in the step 2.

6. Building pias and xvin should work.



Compilation
=======

Just type :

```
python3 waf
```

All Programs will be compiled and available in the build folder.

    build/xvin # original XVin
    build/pias # SIMDEQ manipulation software
    build/pico # SIMDEQ acquisition software

to build programs one by one:

```
python3 waf build_xvin
python3 waf build_pico
python3 waf build_pias
```

## Execute

Execute one of the following command
```
./xvin.sh
./pico.sh
./pico-ueye.sh
./pias.sh
```

Installation Linux
============
On linux to make xvins program available on all system an make xvin available from menu :

```bash
cd misc
sudo ./install_shortcut_linux.sh /path/to/xvin
```



Upgrade xvin
============

1. Put aside local modifications
```bash
git stash
```

2. Get new modification from the server
```bash
git pull
```
3. Re-apply local modification
```bash
git stash pop
```

4. Configure xvin/pias/pico (exemple with a picotwist with ueye camera)
```bash
./waf configure -c ueye
```
5. Compile xvin/pias/pico
```bash
./waf
```
