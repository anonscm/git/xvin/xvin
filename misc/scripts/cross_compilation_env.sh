unset `env | \
    \grep -vi '^EDITOR=\|^HOME=\|^LANG=\|MXE\|^PATH=' | \
    \grep -vi 'PKG_CONFIG\|PROXY\|^PS1=\|^TERM=\|^PWD=' | \
     cut -d '=' -f1 | tr '\n' ' '`

CROSS=i686-w64-mingw32-
export PATH=/usr/i686-w64-mingw32/bin:$PATH
export LIBPATH=/usr/i686-w64-mingw32/lib
export INCLUDES=/usr/i686-w64-mingw32/include

export LIBPATH_boost=/usr/i686-w64-mingw32/lib
export INCLUDES_boost=/usr/i686-w64-mingw32/include

export PKG_CONFIG_PATH="/usr/i686-w64-mingw32/include/"
export CC=${CROSS}gcc
export CXX=${CROSS}g++
export LD=${CROSS}ld
export AR=${CROSS}ar
export WINRC=${CROSS}windres
export PKG_CONFIG=${CROSS}pkg-config



export PS1='\n[MinGW]\033[33m[\$?=$?]\033[32m\u@$HOSTNAME:$PWD   \033[34m[`date`]\033[0m\n42sh$>'
