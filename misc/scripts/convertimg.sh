
cd build;
for i in `seq 0 200`; do 
    if [ -f img$i.dat ]; then
        convert -verbose -size 1024x1024 -depth 8 gray:img$i.dat img$i.png;
    fi
    if [ -f prof$i-w128-h16.dat ]; then
        convert -verbose -size 128x16 -depth 8 gray:prof$i-w128-h16.dat prof$i-w128-h16.png;
    fi
    if [ -f prof$i-w128-h32.dat ]; then
        convert -verbose -size 128x32 -depth 8 gray:prof$i-w128-h32.dat prof$i-w128-h32.png;
    fi
    if [ -f prof$i-w128-h64.dat ]; then
        convert -verbose -size 128x64 -depth 8 gray:prof$i-w128-h64.dat prof$i-w128-h64.png;
    fi
    if [ -f prof$i-w128-h96.dat ]; then
        convert -verbose -size 128x96 -depth 8 gray:prof$i-w128-h96.dat prof$i-w128-h96.png;
    fi
done
cd -;
