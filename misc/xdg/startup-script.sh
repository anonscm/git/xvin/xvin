#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TMP=${DIR%/*}
NAME=${TMP##*/}
VERSION=${DIR##*/}
echo "${NAME}_${VERSION}"
export PATH="/home/biology/miniconda3/bin:$PATH"
source activate "${NAME}_${VERSION}"
export LD_LIBRARY_PATH="$HOME/miniconda3/envs/${NAME}_${VERSION}/lib:/usr/lib/x86_64-linux-gnu"
cd "$HOME/depixus/${NAME}/${VERSION}"
./pico-ueye $@ &
