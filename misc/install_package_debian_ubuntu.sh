sudo apt-get install fftw-dev build-essential libboost-all-dev libgsl0-dev liballegro4-dev libhdf5-dev python3-pip \
libgtk-3-dev libfftw3-dev git libeigen3-dev libnlopt-dev libftdi-dev libtiff-dev\

# optional:
# sudo apt-get install nvidia-current nvidia-opencl-dev nvidia-probe
# sudo apt-get libopencv-dev vim-gtk emacs
# sudo apt-get clang llvm lldb htop most libclang-dev libqt4-dev cgdb doxygen


# echo "installing defaul python packages"
pip3 install numpy xlsxwriter openpyxl pandas matplotlib pillow scipy h5py scikit-image
