pacman -S mingw-w64-i686-toolchain mingw-w64-i686-fftw mingw-w64-i686-gsl svn git \
mingw-w64-i686-pkg-config mingw-w64-i686-gtk3 mingw-w64-i686-opencv mingw-w64-i686-boost \
mingw-w64-i686-glade mingw-w64-i686-freetype doxygen make mingw-w64-i686-eigen3 mingw-w64-i686-nlopt \
mingw-w64-i686-python3 mingw-w64-i686-python3-pip mingw-w64-i686-python3-numpy msys2-launcher \
mingw-w64-i686-dlfcn mingw32/mingw-w64-i686-hdf5 mingw32/mingw-w64-i686-python3-h5py \
mingw-w64-i686-python3-xlsxwriter mingw-w64-i686-python3-openpyxl mingw-w64-i686-python3-pandas mingw-w64-i686-python3-matplotlib \
mingw-w64-i686-python3-scipy mingw-w64-i686-python3-pillow

pip3 install scikit-image
