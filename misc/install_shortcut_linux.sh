#! /bin/bash
XVIN_DIR=$1
MISC=$XVIN_DIR/misc
BIN=/usr/local/bin
APPS=/usr/share/applications
APPSICON=/usr/share/icons/hicolor/scalable/apps
MIMEICON=/usr/share/icons/hicolor/scalable/mimetypes
PIXMAPS=/usr/share/pixmaps
MIMETYPE=/usr/share/mime/packages
CACHEHICOLOR=/usr/share/icons/hicolor


if [ "$(id -u)" -ne 0 ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
if [ -z "$1" ]
  then
    echo "please supply xvin directory exemple : /home/ens/xvin"
    exit 1
fi

cd $BIN
# Creating link to binary in
cp -vf $MISC/launch_scripts/xvin .
cp -vf $MISC/launch_scripts/pias .
cp -vf $MISC/launch_scripts/pico .
cp -vf $MISC/launch_scripts/pico-ueye .

# Correct right

chmod 555 xvin
chmod 555 pias
chmod 555 pico
chmod 555 pico-ueye


#Install icon to the system
cd $APPSICON
cp -vf $MISC/xdg/xvin.svg .
cp -vf $MISC/xdg/pico.svg .
cp -vf $MISC/xdg/pias.svg .
#cp -vf $MISC/xdg/pico.svg .
cp -vf $MISC/xdg/pico-ueye.svg .

#Correct right
chmod 644 xvin.svg
chmod 644 pias.svg
chmod 644 pico.svg
# chmod 644 pias.svg
# chmod 644 pico.svg
chmod 644 pico-ueye.svg


cd $MIMEICON
cp -vf $MISC/xdg/xvin.svg application-x-xv-plot.svg
cp -vf $MISC/xdg/xvin.svg application-x-xv-project.svg
cp -vf $MISC/xdg/pias.svg application-x-xv-track.svg
#cp -vf $MISC/xdg/pico.svg .
#cp -vf $MISC/xdg/pico-ueye.svg .

#Correct right
chmod 644 application-x-xv-plot.svg
chmod 644 application-x-xv-project.svg
chmod 644 application-x-xv-track.svg



cd $PIXMAPS
cp -vf $MISC/xdg/xvin.svg .
cp -vf $MISC/xdg/pias.svg .
cp -vf $MISC/xdg/pico.svg .
cp -vf $MISC/xdg/pico-ueye.svg .

#Correct right
chmod 644 xvin.svg
chmod 644 pias.svg
chmod 644 pico.svg
chmod 644 pico-ueye.svg
#Install file extension to the system
cd $MIMETYPE
cp  $MISC/xdg/x-xv-plot.xml .
cp  $MISC/xdg/x-xv-project.xml .
cp  $MISC/xdg/x-xv-track.xml .

#Install application to the system
cd $APPS
cp -vf $MISC/xdg/xvin.desktop .
cp -vf $MISC/xdg/pias.desktop .
cp -vf $MISC/xdg/pico-ueye.desktop .
cp -vf $MISC/xdg/pico.desktop .

#Correct right
chmod 644 xvin.desktop
chmod 644 pias.desktop
chmod 644 pico.desktop
chmod 644 pico-ueye.desktop

update-mime-database /usr/share/mime
chmod ugo+r /usr/share/mime/mime.cache
update-desktop-database &>/dev/null || :
gtk-update-icon-cache $CACHEHICOLOR
xdg-desktop-menu forceupdate

USER="$(logname)"
HOMEDIR="$(eval echo ~$USER)"
CONFIG=$HOMEDIR/.config

if [ ! -d $CONFIG/xvin ]; then
mkdir $CONFIG/xvin
fi
if [ ! -f $CONFIG/xvin/Pico.cfg ]; then
    cp $MISC/default_config/Pico.cfg $CONFIG/xvin/Pico.cfg
fi
if [ ! -f $CONFIG/xvin/PlayItAgainSam.cfg ]; then
    cp $MISC/default_config/PlayItAgainSam.cfg $CONFIG/xvin/PlayItAgainSam.cfg
fi
if [ ! -f $CONFIG/xvin/Picotwist.cfg ]; then
    cp $MISC/default_config/Picotwist.cfg $CONFIG/xvin/Picotwist.cfg
fi

chown -R $USER:$USER $CONFIG/xvin
chmod -R 755 $CONFIG/xvin
