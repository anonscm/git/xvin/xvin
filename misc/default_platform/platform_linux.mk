# below is the system used:
#SYSTEM = WIN32
SYSTEM = LINUX
# SYSTEM = MACOS

# below is the platform (used only on win32 machines):
# PLATFORM = MVSC
PLATFORM = MINGW
# PLATFORM = CYGWIN

# below is the linking type:
#LINKING = STATIC
LINKING = DYNAMIC

# below the linking type of allegro:
#ALLEGRO_LINKING = STATIC
ALLEGRO_LINKING = DYNAMIC

UEYE_SDK = SDK_4_3_0
JAI_SDK = SDK_1_2_5
FLOW_CONTROL = THORLABS
XV_GTK_DIALOG = 1
#

# below we eventually define a memory checker (comment it if none is used)
# MEMCHECK = FORTIFY
CXX_FLAGS      = -std=c++11
WARNING_FLAGS  = -W -Wall -Wextra -Wunused -Wmaybe-uninitialized -fno-common -Wformat=2 -Winit-self -Winline -Wpacked -Wpointer-arith -Wmissing-format-attribute -Wmissing-noreturn -Wnested-externs -Wold-style-definition -Wswitch-enum -Wundef -Wunreachable-code -Wmissing-include-dirs -Wshadow -Wformat-security -Wcast-qual -Wparentheses -Wsequence-point
DEBUG_FLAGS = -g -ggdb

CPU_FLAGS      = -Werror=implicit-function-declaration -std=gnu11 -I/usr/local/include/  -Ic:\msys\1.0\local\include  -L/usr/local/lib/ -fPIC -DXV_GTK_DIALOG

CC	=	gcc
CXX = g++

GNU_LIBS_DIR = /usr               # most typical 


# below is a computation of the libraries include path
MY_INC = $(addprefix -I,$(addsuffix /include,$(strip $(GNU_LIBS_DIR))))
# following line may be usefull on mingw only, but I recommend using
#    GNU_LIBS_DIR = $(XVIN_DIR) 
# instead.
# MY_INC = -I./include/allegro -I./include/allfont -I../../include/pthread



# We write the plug'in names in a compact way:
#include $(XVIN_DIR)plugin-list-ens-lyon.mk
include $(XVIN_DIR)plugin-list-ens-lps.mk
# another possible way of giving a plugins list:
#PLUGINS = inout random hist



#############################################################################
# do not edit below this line
#############################################################################
include $(XVIN_DIR)defines.mk
