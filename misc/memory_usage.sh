#!/bin/bash
prev_mem_usage=0;
cur_mem_usage=0;

while true; do
    prev_mem_usage=$cur_mem_usage;
    cur_mem_usage=`smem --columns='uss' -P '^pico-ueye$' -H`;

    diff=`echo $cur_mem_usage - $prev_mem_usage | bc 2> /dev/null`;
    echo 'mem usage:' $cur_mem_usage 'diff:' $diff;
    sleep 0.5
done
