#ifndef _FX_H_
#define _FX_H_

PXV_FUNC(int, do_fx_rescale_plot, (void));
PXV_FUNC(MENU*, fx_plot_menu, (void));
PXV_FUNC(int, do_fx_rescale_data_set, (void));
PXV_FUNC(int, fx_main, (int argc, char **argv));
#endif

